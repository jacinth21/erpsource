CREATE OR REPLACE PACKAGE BODY gm_pkg_sm_robotics_ar_po_process IS

	/**********************************************************************
 	* Description : Procedure used to fetch Capital and metal PO Details
 	* Author      : saravanan M
	************************************************************************/
	PROCEDURE gm_fch_po_details(
		p_quote_id			IN			t5507_robot_quotation_category_details.c5506_robot_quotation_id%TYPE,
		p_account_type		IN			t5507_robot_quotation_category_details.c5507_em_quote_account_type%TYPE,
		p_out_po_dtl		OUT			CLOB
	)
	AS
	BEGIN
	SELECT 
		JSON_OBJECT(
			'strPONo' 			 VALUE C5514_CUSTOMER_PO,
			'strPOAmount' 		 VALUE C5514_PO_AMOUNT,
			'strPODate' 		 VALUE C5514_PO_DATE ,
			'strInvoiceId' 		 VALUE C503_INVOICE_ID ,
			'strDODtlsId' 		 VALUE C901_DO_DTLS,
			'strDODtlsName' 	 VALUE C901_DO_DTLS_NAME,
			'strPODtlsId' 		 VALUE C901_PO_DTLS,
			'strPODtlsName' 	 VALUE C901_PO_DTLS_NAME,
			'strInvoiceComment'  VALUE C5514_INVOICE_COMMENTS,
			'strInvoiceType' 	 VALUE C5514_EM_INVOICE_TYPE,
			'strInvoiceTemplate' VALUE C5514_EM_INVOICE_TEMPLATE,
			'strAccountId' 		 VALUE T5514.C704_ACCOUNT_ID,
			'strAccountName' 	 VALUE T5514.C704_ACCOUNT_NAME,
			'strAccountPay' 	 VALUE GET_CODE_NAME( get_account_pay_term(T5514.C704_ACCOUNT_ID))
		)
		INTO p_out_po_dtl
		FROM T5514_ROBOT_AR_INVOICE_DTLS T5514
		WHERE T5514.C5506_ROBOT_QUOTATION_ID = p_quote_id
		AND T5514.C5514_EM_QUOTE_ACCOUNT_TYPE = p_account_type  -- 'Capital' or 'Metal'
		AND T5514.C5514_VOID_FL IS NULL;
	
	END gm_fch_po_details;
	
	/**********************************************************************
 	* Description : Procedure used to save AR Invoice Type
 	* Author      : tmuthusamy
	************************************************************************/
	PROCEDURE gm_invoice_type_main(
		p_quote_id				IN			t5514_robot_ar_invoice_dtls.c5506_robot_quotation_id%TYPE,
		p_invoice_temp_name		IN			t5514_robot_ar_invoice_dtls.C5514_EM_INVOICE_TEMPLATE%TYPE,
		p_user_id				IN			t5514_robot_ar_invoice_dtls.C5514_LAST_UPDATED_BY%TYPE,
		p_invoice_type			IN			t5514_robot_ar_invoice_dtls.C5514_EM_INVOICE_TYPE%TYPE
	)
	AS
	BEGIN
	--If merge invoice type,update Type to T5506_robot_quotation table
	IF p_invoice_type = 'Merge Invoice' THEN
		gm_pkg_sm_robotics_ar_po_process.gm_upd_merge_invoice_type(p_quote_id,p_user_id,p_invoice_type);
	ELSE
		gm_pkg_sm_robotics_ar_po_process.gm_upd_invoice_type(p_quote_id,p_user_id,p_invoice_type);
	END IF;
	
	END gm_invoice_type_main;
	
	/**********************************************************************
 	* Description : Procedure used to save AR Invoice Template
 	* Author      : tmuthusamy
	************************************************************************/
	PROCEDURE gm_invoice_template_main(
		p_quote_id				IN			t5514_robot_ar_invoice_dtls.c5506_robot_quotation_id%TYPE,
		p_invoice_temp_name		IN			t5514_robot_ar_invoice_dtls.C5514_EM_INVOICE_TEMPLATE%TYPE,
		p_user_id				IN			t5514_robot_ar_invoice_dtls.C5514_LAST_UPDATED_BY%TYPE,
		p_invoice_type			IN			t5514_robot_ar_invoice_dtls.C5514_EM_QUOTE_ACCOUNT_TYPE%TYPE
	)
	AS
	BEGIN
	--If merge invoice type,update template to T5506_robot_quotation table
	IF p_invoice_type = 'Merge Invoice' THEN
		gm_pkg_sm_robotics_ar_po_process.gm_upd_merge_invoice_template(p_quote_id,p_invoice_temp_name,p_user_id);
	ELSE
		gm_pkg_sm_robotics_ar_po_process.gm_upd_invoice_template(p_quote_id,p_invoice_temp_name,p_user_id,p_invoice_type);
	END IF;
	END gm_invoice_template_main;
	
	
	/**********************************************************************
 	* Description : Procedure used to save AR Invoice Comments
 	* Author      : tmuthusamy
	************************************************************************/
	PROCEDURE gm_invoice_comments_main(
		p_quote_id				IN			t5514_robot_ar_invoice_dtls.c5506_robot_quotation_id%TYPE,
		p_invoice_comments		IN			t5514_robot_ar_invoice_dtls.C5514_INVOICE_COMMENTS%TYPE,
		p_user_id				IN			t5514_robot_ar_invoice_dtls.C5514_LAST_UPDATED_BY%TYPE,
		p_invoice_type			IN			t5514_robot_ar_invoice_dtls.C5514_EM_QUOTE_ACCOUNT_TYPE%TYPE
	)
	AS
	BEGIN
	
	--If merge invoice type,update comments to T5506_robot_quotation table
	IF p_invoice_type = 'Merge Invoice' THEN
		gm_pkg_sm_robotics_ar_po_process.gm_upd_merge_invoice_comments(p_quote_id,p_invoice_comments,p_user_id);
	ELSE
		gm_pkg_sm_robotics_ar_po_process.gm_upd_invoice_comments(p_quote_id,p_invoice_comments,p_user_id,p_invoice_type);
	END IF;
	
	END gm_invoice_comments_main;
	
	/**********************************************************************
 	* Description : Procedure used to save AR Invoice Template when Invoice type as merge
 	* Author      : tmuthusamy
	************************************************************************/
	PROCEDURE gm_upd_merge_invoice_template(
		p_quote_id				IN			t5514_robot_ar_invoice_dtls.c5506_robot_quotation_id%TYPE,
		p_invoice_temp_name		IN			t5514_robot_ar_invoice_dtls.C5514_EM_INVOICE_TEMPLATE%TYPE,
		p_user_id				IN			t5514_robot_ar_invoice_dtls.C5514_LAST_UPDATED_BY%TYPE
	)
	AS
	BEGIN
		UPDATE t5506_robot_quotation
		SET c5506_em_merge_invoice_template = p_invoice_temp_name,
		c5506_last_updated_by = p_user_id,
		c5506_last_updated_date = CURRENT_DATE
		WHERE c5506_robot_quotation_id = p_quote_id
		AND c5506_void_fl IS NULL;
	END gm_upd_merge_invoice_template;
	
	/**********************************************************************
 	* Description : Procedure used to save AR Invoice Template when Invoice type as capital or metal
 	* Author      : tmuthusamy
	************************************************************************/
	PROCEDURE gm_upd_invoice_template(
		p_quote_id				IN			t5514_robot_ar_invoice_dtls.c5506_robot_quotation_id%TYPE,
		p_invoice_temp_name		IN			t5514_robot_ar_invoice_dtls.C5514_EM_INVOICE_TEMPLATE%TYPE,
		p_user_id				IN			t5514_robot_ar_invoice_dtls.C5514_LAST_UPDATED_BY%TYPE,
		p_invoice_type			IN			t5514_robot_ar_invoice_dtls.C5514_EM_QUOTE_ACCOUNT_TYPE%TYPE
	)
	AS
	BEGIN
		UPDATE T5514_ROBOT_AR_INVOICE_DTLS
		SET C5514_EM_INVOICE_TEMPLATE = p_invoice_temp_name,
		C5514_LAST_UPDATED_BY = p_user_id,
		C5514_LAST_UPDATED_DATE = CURRENT_DATE
		WHERE C5506_ROBOT_QUOTATION_ID = p_quote_id
		AND C5514_EM_QUOTE_ACCOUNT_TYPE = p_invoice_type
		AND C5514_VOID_FL IS NULL;
	END gm_upd_invoice_template;
	
	/**********************************************************************
 	* Description : Procedure used to save AR Invoice Comments when Invoice type as merge
 	* Author      : tmuthusamy
	************************************************************************/
	PROCEDURE gm_upd_merge_invoice_comments(
		p_quote_id				IN			t5514_robot_ar_invoice_dtls.c5506_robot_quotation_id%TYPE,
		p_invoice_comments		IN			t5514_robot_ar_invoice_dtls.C5514_INVOICE_COMMENTS%TYPE,
		p_user_id				IN			t5514_robot_ar_invoice_dtls.C5514_LAST_UPDATED_BY%TYPE
	)
	AS
	BEGIN
		UPDATE t5506_robot_quotation
		SET c5506_merge_invoice_comments = p_invoice_comments,
		c5506_last_updated_by = p_user_id,
		c5506_last_updated_date = CURRENT_DATE
		WHERE c5506_robot_quotation_id = p_quote_id
		AND c5506_void_fl IS NULL;
	END gm_upd_merge_invoice_comments;
	
	/**********************************************************************
 	* Description : Procedure used to save AR Invoice Comments when Invoice type as capital or metal
 	* Author      : tmuthusamy
	************************************************************************/
	PROCEDURE gm_upd_invoice_comments(
		p_quote_id				IN			t5514_robot_ar_invoice_dtls.c5506_robot_quotation_id%TYPE,
		p_invoice_comments		IN			t5514_robot_ar_invoice_dtls.C5514_INVOICE_COMMENTS%TYPE,
		p_user_id				IN			t5514_robot_ar_invoice_dtls.C5514_LAST_UPDATED_BY%TYPE,
		p_invoice_type			IN			t5514_robot_ar_invoice_dtls.C5514_EM_QUOTE_ACCOUNT_TYPE%TYPE
	)
	AS
	BEGIN
		UPDATE T5514_ROBOT_AR_INVOICE_DTLS
		SET C5514_INVOICE_COMMENTS = p_invoice_comments,
		C5514_LAST_UPDATED_BY = p_user_id,
		C5514_LAST_UPDATED_DATE = CURRENT_DATE
		WHERE C5506_ROBOT_QUOTATION_ID = p_quote_id
		AND C5514_EM_QUOTE_ACCOUNT_TYPE = p_invoice_type
		AND C5514_VOID_FL IS NULL;
	END gm_upd_invoice_comments;
	
	/**********************************************************************
 	* Description : Procedure used to save AR Invoice Type when Invoice type as merge
 	* Author      : tmuthusamy
	************************************************************************/
	PROCEDURE gm_upd_merge_invoice_type(
		p_quote_id				IN			t5514_robot_ar_invoice_dtls.c5506_robot_quotation_id%TYPE,
		p_user_id				IN			t5514_robot_ar_invoice_dtls.C5514_LAST_UPDATED_BY%TYPE,
		p_invoice_type			IN			t5514_robot_ar_invoice_dtls.C5514_EM_INVOICE_TYPE%TYPE
	)
	AS
	BEGIN
		UPDATE t5506_robot_quotation
		SET C5506_EM_MERGE_INVOICE_TYPE = p_invoice_type,
		c5506_last_updated_by = p_user_id,
		c5506_last_updated_date = CURRENT_DATE
		WHERE c5506_robot_quotation_id = p_quote_id
		AND c5506_void_fl IS NULL;
	END gm_upd_merge_invoice_type;
	
	/**********************************************************************
 	* Description : Procedure used to save AR Invoice Type when Invoice type as capital or metal
 	* Author      : tmuthusamy
	************************************************************************/
	PROCEDURE gm_upd_invoice_type(
		p_quote_id				IN			t5514_robot_ar_invoice_dtls.c5506_robot_quotation_id%TYPE,
		p_user_id				IN			t5514_robot_ar_invoice_dtls.C5514_LAST_UPDATED_BY%TYPE,
		p_invoice_type			IN			t5514_robot_ar_invoice_dtls.C5514_EM_INVOICE_TYPE%TYPE
	)
	AS
	BEGIN
		UPDATE T5514_ROBOT_AR_INVOICE_DTLS
		SET C5514_EM_INVOICE_TYPE = p_invoice_type,
		C5514_LAST_UPDATED_BY = p_user_id,
		C5514_LAST_UPDATED_DATE = CURRENT_DATE
		WHERE C5506_ROBOT_QUOTATION_ID = p_quote_id
		AND C5514_EM_QUOTE_ACCOUNT_TYPE = p_invoice_type
		AND C5514_VOID_FL IS NULL;
	END gm_upd_invoice_type;
	
	/**********************************************************************
 	* Description : Procedure used to fetch Default Comment section sold Category list
 	* Author      : tmuthusamy
	************************************************************************/
	PROCEDURE gm_fch_invoice_comments_category_list(
		p_quote_id				IN			T5507_ROBOT_QUOTATION_CATEGORY_DETAILS.C5506_ROBOT_QUOTATION_ID%TYPE,
		p_invoice_comments		OUT			T5502_ROBOT_QUOTATION_CATEGORY_MASTER.C5502_CATEGORY_GROUP_NAME%TYPE
	)
	AS
	BEGIN
		BEGIN
		SELECT RTRIM (XMLAGG (XMLELEMENT (E, CATEGORY.CATEGORYNM || ', ')) .EXTRACT ('//text()'), ', ') INTO p_invoice_comments
		FROM (
		  SELECT T5502.C5502_CATEGORY_GROUP_NAME CATEGORYNM,
		         T5507.C5507_ORDER_CATEGORY_SEQ  ORDERCATSEQ
		    FROM T5502_ROBOT_QUOTATION_CATEGORY_MASTER T5502,
		         T5507_ROBOT_QUOTATION_CATEGORY_DETAILS T5507
		   WHERE T5502.C5502_ROBOT_QUOTATION_CATEGORY_MASTER_ID  = T5507.C5502_ROBOT_QUOTATION_CATEGORY_MASTER_ID(+)
		     AND T5507.C5506_ROBOT_QUOTATION_ID = p_quote_id
		     AND T5507.C5507_QTY > 0
		     AND T5502.C5502_VOID_FL IS NULL
		     AND T5507.C5507_VOID_FL IS NULL
		GROUP BY T5502.C5502_CATEGORY_GROUP_NAME,
		         T5507.C5507_ORDER_CATEGORY_SEQ
		ORDER BY T5507.C5507_ORDER_CATEGORY_SEQ) CATEGORY;
		
		EXCEPTION WHEN NO_DATA_FOUND
	    THEN
		   p_invoice_comments:= '';
	    END;
	END gm_fch_invoice_comments_category_list;
	
	/**********************************************************************
	 	* Description : Procedure used to save and update PO details for both capital and metal account
	 	* Author      : tmuthusamy
	************************************************************************/
    PROCEDURE gm_sav_quote_po_details(
        p_quote_id   					IN   T5514_ROBOT_AR_INVOICE_DTLS.C5506_ROBOT_QUOTATION_ID%TYPE,
        p_account_type					IN   T5514_ROBOT_AR_INVOICE_DTLS.C5514_EM_QUOTE_ACCOUNT_TYPE%TYPE,
		p_cust_po_number				IN   T5514_ROBOT_AR_INVOICE_DTLS.C5514_CUSTOMER_PO%TYPE,
		p_cust_po_amount				IN   T5514_ROBOT_AR_INVOICE_DTLS.C5514_PO_AMOUNT%TYPE,
		p_cust_po_date					IN   T5514_ROBOT_AR_INVOICE_DTLS.C5514_PO_DATE%TYPE,
		p_rev_rec_po_type				IN   T5514_ROBOT_AR_INVOICE_DTLS.C901_PO_DTLS%TYPE,
		p_rev_rec_po_type_name			IN   T5514_ROBOT_AR_INVOICE_DTLS.C901_PO_DTLS_NAME%TYPE,
		p_rev_rec_do_type				IN	 T5514_ROBOT_AR_INVOICE_DTLS.C901_DO_DTLS%TYPE,
		p_rev_rec_do_type_name			IN   T5514_ROBOT_AR_INVOICE_DTLS.C901_DO_DTLS_NAME%TYPE,
        p_user_id    					IN   T5514_ROBOT_AR_INVOICE_DTLS.C5514_LAST_UPDATED_BY%TYPE
    )
    AS
	    v_account_id					T5514_ROBOT_AR_INVOICE_DTLS.C704_ACCOUNT_ID%TYPE;
		v_account_name					T5514_ROBOT_AR_INVOICE_DTLS.C704_ACCOUNT_NAME%TYPE;
    BEGIN
    	v_account_id  := NULL;
		v_account_name:= NULL;
			
		
		IF (p_account_type = 'Capital') THEN
			BEGIN
				SELECT C704_CAPITAL_ACCOUNT_ID ,
				GET_ACCOUNT_NAME(C704_CAPITAL_ACCOUNT_ID)
				INTO 
				v_account_id,
				v_account_name
				FROM T5506_ROBOT_QUOTATION
				WHERE C5506_ROBOT_QUOTATION_ID = p_quote_id
				AND C5506_VOID_FL IS NULL;
			EXCEPTION WHEN NO_DATA_FOUND
			    THEN
			    v_account_id:= '';
			    v_account_name:= '';
		    END;
	    END IF;
		
		
		IF (p_account_type = 'Metal') THEN
			BEGIN
				SELECT C704_METAL_ACCOUNT_ID ,
				GET_ACCOUNT_NAME(C704_METAL_ACCOUNT_ID)
				INTO 
				v_account_id,
				v_account_name
				FROM T5506_ROBOT_QUOTATION
				WHERE C5506_ROBOT_QUOTATION_ID = p_quote_id
				AND C5506_VOID_FL IS NULL;
			EXCEPTION WHEN NO_DATA_FOUND
			    THEN
			    v_account_id:= '';
			    v_account_name:= '';
		    END;
	    END IF;
		
		-- Below query used for update the PO details for Quote
		UPDATE T5514_ROBOT_AR_INVOICE_DTLS
		SET C5514_CUSTOMER_PO   = p_cust_po_number,
		C5514_PO_AMOUNT         = p_cust_po_amount,
		C5514_PO_DATE           = p_cust_po_date,
		C901_PO_DTLS            = p_rev_rec_po_type,
		C901_PO_DTLS_NAME       = p_rev_rec_po_type_name,
		C901_DO_DTLS            = p_rev_rec_do_type,
		C901_DO_DTLS_NAME       = p_rev_rec_do_type_name,
		C704_ACCOUNT_ID         = v_account_id,
		C704_ACCOUNT_NAME       = v_account_name,
		C5514_LAST_UPDATED_BY   = p_user_id,
		C5514_LAST_UPDATED_DATE = CURRENT_DATE
		WHERE C5506_ROBOT_QUOTATION_ID = p_quote_id
		AND C5514_EM_QUOTE_ACCOUNT_TYPE = p_account_type
		AND C5514_VOID_FL IS NULL;
		
		IF (SQL%ROWCOUNT = 0) THEN
		INSERT
		INTO T5514_ROBOT_AR_INVOICE_DTLS
		(
			C5506_ROBOT_QUOTATION_ID, 
			C5514_EM_QUOTE_ACCOUNT_TYPE, 
			C704_ACCOUNT_ID, 
			C704_ACCOUNT_NAME, 
			C5514_CUSTOMER_PO,
			C5514_PO_AMOUNT, 
			C5514_PO_DATE, 
			C901_PO_DTLS, 
			C901_PO_DTLS_NAME, 
			C901_DO_DTLS, 
			C901_DO_DTLS_NAME,
			C5514_LAST_UPDATED_BY, 
			C5514_LAST_UPDATED_DATE
		)
		VALUES
		(
			p_quote_id,
			p_account_type,
			v_account_id,
			v_account_name, 
			p_cust_po_number,
			p_cust_po_amount,
			p_cust_po_date, 
			p_rev_rec_po_type,
			p_rev_rec_po_type_name,
			p_rev_rec_do_type,
			p_rev_rec_do_type_name,
			p_user_id, 
			CURRENT_DATE
		);
		END IF;
    END gm_sav_quote_po_details;
	
/**********************************************************************
 	* Description : Procedure used to update customer PO number in AR process PO Details
 	* Author      : saravanan M
	************************************************************************/
	PROCEDURE gm_upd_cust_po_number (
		p_quote_id       IN   t5507_robot_quotation_category_details.c5506_robot_quotation_id%TYPE,
		p_account_type   IN   t5507_robot_quotation_category_details.c5507_em_quote_account_type%TYPE,
		p_customer_po    IN   t5514_robot_ar_invoice_dtls.c5514_customer_po%TYPE,
		p_date_format    IN   VARCHAR2,
		p_user_id        IN   t5506_robot_quotation.c5506_last_updated_by%TYPE
	) AS
		v_cust_po_number   t501_order.c501_customer_po%TYPE;
		v_cust_po_amount   t501_order.c501_po_amount%TYPE;
		v_cust_po_date     t501_order.c501_customer_po_date%TYPE;
		v_account_id       t501_order.c704_account_id%TYPE;
	BEGIN
		--Updating the entered value in t5514_robot_ar_invoice_dtls
		UPDATE t5514_robot_ar_invoice_dtls
		SET c5514_customer_po = p_customer_po,
		    c5514_last_updated_by = p_user_id,
		    c5514_last_updated_date = current_date
		WHERE c5506_robot_quotation_id = p_quote_id --'GMQT-081621-1' 
		      AND c5514_em_quote_account_type = p_account_type --'Metal' or 'Capital'
		      AND c5514_void_fl IS NULL;
		--Fetching values from t5514_robot_ar_invoice_dtls to save into Order Table
		BEGIN
			SELECT c5514_customer_po,
			       c5514_po_amount,
			       c5514_po_date,
			       c704_account_id
			INTO
				v_cust_po_number,
				v_cust_po_amount,
				v_cust_po_date,
				v_account_id
			FROM t5514_robot_ar_invoice_dtls t5514
			WHERE t5514.c5506_robot_quotation_id = p_quote_id
			      AND t5514.c5514_em_quote_account_type = p_account_type --'capital' or 'Metal'
			      AND t5514.c5514_void_fl IS NULL;
		EXCEPTION
			WHEN no_data_found THEN
				v_cust_po_number := '';
				v_cust_po_amount := '';
				v_cust_po_date := '';
				v_account_id := '';
		END;
	      --To update PO details in Order Table
		gm_upd_po_details_for_orders (p_quote_id, v_account_id, v_cust_po_number, v_cust_po_amount, v_cust_po_date,
		                             p_date_format, p_user_id);
	END gm_upd_cust_po_number;
	
	 
	/**********************************************************************
 	* Description : Procedure used to update customer PO Amount in AR process PO Details
 	* Author      : saravanan M
	************************************************************************/
	PROCEDURE gm_upd_cust_po_amount (
		p_quote_id       IN   t5507_robot_quotation_category_details.c5506_robot_quotation_id%TYPE,
		p_account_type   IN   t5507_robot_quotation_category_details.c5507_em_quote_account_type%TYPE,
		p_po_amount      IN   t5514_robot_ar_invoice_dtls.c5514_po_amount%TYPE,
		p_date_format    IN   VARCHAR2,
		p_user_id        IN   t5506_robot_quotation.c5506_last_updated_by%TYPE
	) AS
		v_cust_po_number   t501_order.c501_customer_po%TYPE;
		v_cust_po_amount   t501_order.c501_po_amount%TYPE;
		v_cust_po_date     t501_order.c501_customer_po_date%TYPE;
		v_account_id       t501_order.c704_account_id%TYPE;
	BEGIN
		--Updating the entered value in t5514_robot_ar_invoice_dtls
		UPDATE t5514_robot_ar_invoice_dtls
		SET c5514_po_amount = p_po_amount,
		    c5514_last_updated_by = p_user_id,
		    c5514_last_updated_date = current_date
		WHERE c5506_robot_quotation_id = p_quote_id --'GMQT-081621-1' 
		      AND c5514_em_quote_account_type = p_account_type --'Metal' or 'Capital'
		      AND c5514_void_fl IS NULL;
		--Fetching values from t5514_robot_ar_invoice_dtls to save into Order Table
		BEGIN
			SELECT c5514_customer_po,
			       c5514_po_amount,
			       c5514_po_date,
			       c704_account_id
			INTO
				v_cust_po_number,
				v_cust_po_amount,
				v_cust_po_date,
				v_account_id
			FROM t5514_robot_ar_invoice_dtls t5514
			WHERE t5514.c5506_robot_quotation_id = p_quote_id
			      AND t5514.c5514_em_quote_account_type = p_account_type --'capital' or 'Metal'
			      AND t5514.c5514_void_fl IS NULL;
		EXCEPTION
			WHEN no_data_found THEN
				v_cust_po_number := '';
				v_cust_po_amount := '';
				v_cust_po_date := '';
				v_account_id := '';
		END;
		--To update PO details in Order Table
		gm_upd_po_details_for_orders (p_quote_id, v_account_id, v_cust_po_number, v_cust_po_amount, v_cust_po_date,
		                             p_date_format, p_user_id);
	END gm_upd_cust_po_amount;
	
	/**********************************************************************
 	* Description : Procedure used to update customer PO Date in AR process PO Details
 	* Author      : saravanan M
	************************************************************************/
	PROCEDURE gm_upd_cust_po_date (
		p_quote_id       IN   t5507_robot_quotation_category_details.c5506_robot_quotation_id%TYPE,
		p_account_type   IN   t5507_robot_quotation_category_details.c5507_em_quote_account_type%TYPE,
		p_po_date        IN   VARCHAR2,
		p_date_format    IN   VARCHAR2,
		p_user_id        IN   t5506_robot_quotation.c5506_last_updated_by%TYPE
	) AS
		v_cust_po_number   t501_order.c501_customer_po%TYPE;
		v_cust_po_amount   t501_order.c501_po_amount%TYPE;
		v_cust_po_date     t501_order.c501_customer_po_date%TYPE;
		v_account_id       t501_order.c704_account_id%TYPE;
	BEGIN
		--Updating the entered value in t5514_robot_ar_invoice_dtls
		UPDATE t5514_robot_ar_invoice_dtls
		SET c5514_po_date = to_date (p_po_date, p_date_format),
		    c5514_last_updated_by = p_user_id,
		    c5514_last_updated_date = current_date
		WHERE c5506_robot_quotation_id = p_quote_id --'GMQT-081621-1' 
		      AND c5514_em_quote_account_type = p_account_type --'Metal' or 'Capital'
		      AND c5514_void_fl IS NULL;
		--Fetching values from t5514_robot_ar_invoice_dtls to save into Order Table
		BEGIN
			SELECT c5514_customer_po,
			       c5514_po_amount,
			       c5514_po_date,
			       c704_account_id
			INTO
				v_cust_po_number,
				v_cust_po_amount,
				v_cust_po_date,
				v_account_id
			FROM t5514_robot_ar_invoice_dtls t5514
			WHERE t5514.c5506_robot_quotation_id = p_quote_id
			      AND t5514.c5514_em_quote_account_type = p_account_type --'capital' or 'Metal'
			      AND t5514.c5514_void_fl IS NULL;
		EXCEPTION
			WHEN no_data_found THEN
				v_cust_po_number := '';
				v_cust_po_amount := '';
				v_cust_po_date := '';
				v_account_id := '';
		END;
		--To update PO details in Order Table
		gm_upd_po_details_for_orders (p_quote_id, v_account_id, v_cust_po_number, v_cust_po_amount, v_cust_po_date,
		                             p_date_format, p_user_id);
	END gm_upd_cust_po_date;
	
	/**********************************************************************
 	* Description : Procedure used to update PO Details in t501_order
 	* Author      : saravanan M
	************************************************************************/
	PROCEDURE gm_upd_po_details_for_orders (
		p_quote_id         IN   t5507_robot_quotation_category_details.c5506_robot_quotation_id%TYPE,
		p_account_id       IN   t501_order.c704_account_id%TYPE,
		p_cust_po_number   IN   t501_order.c501_customer_po%TYPE,
		p_cust_po_amount   IN   t501_order.c501_po_amount%TYPE,
		p_cust_po_date     IN   DATE,
		p_date_format      IN   VARCHAR2,
		p_user_id          IN   t5506_robot_quotation.c5506_last_updated_by%TYPE
	) AS
	BEGIN
		UPDATE t501_order
		SET c501_customer_po = p_cust_po_number,
		    c501_customer_po_date = p_cust_po_date,
		    c501_po_amount = p_cust_po_amount,
		    c501_last_updated_by = p_user_id,
		    c501_last_updated_date = current_date
		WHERE c5506_robot_quotation_id = p_quote_id
		      AND c704_account_id = p_account_id
		      AND c501_void_fl IS NULL;
	END gm_upd_po_details_for_orders;
	
	
	/**********************************************************************
 	* Description : Procedure used to update revenue Recog PO Dtls based on Account Type
 	* Author      : saravanan M
	************************************************************************/
	PROCEDURE gm_upd_rev_rec_po_dtls (
		p_quote_id          IN   t5507_robot_quotation_category_details.c5506_robot_quotation_id%TYPE,
		p_account_type      IN   t5507_robot_quotation_category_details.c5507_em_quote_account_type%TYPE,
		p_rev_rec_po_type   IN   t5514_robot_ar_invoice_dtls.c901_po_dtls%TYPE,
		p_user_id           IN   t5506_robot_quotation.c5506_last_updated_by%TYPE
	) AS
		v_rev_rec_po_type_name   t5514_robot_ar_invoice_dtls.c901_po_dtls_name%TYPE;
		v_po_dtls                t5514_robot_ar_invoice_dtls.c901_po_dtls%TYPE;
		v_do_dtls                t5514_robot_ar_invoice_dtls.c901_do_dtls%TYPE;
		v_account_id             t501_order.c704_account_id%TYPE;
		
		CURSOR cur_order_id IS
		SELECT c501_order_id order_id
		FROM t501_order
		WHERE c5506_robot_quotation_id = p_quote_id
		      AND c704_account_id = v_account_id
		      AND c501_void_fl IS NULL;
	BEGIN
	--To get the entered revReco Type Name
	BEGIN
		SELECT get_code_name (p_rev_rec_po_type)
		INTO v_rev_rec_po_type_name
		FROM dual;
		EXCEPTION
			WHEN no_data_found THEN
				v_rev_rec_po_type_name := '';
		END;
		
		--Updating the entered value in t5514_robot_ar_invoice_dtls
		UPDATE t5514_robot_ar_invoice_dtls
		SET c901_po_dtls = p_rev_rec_po_type,
		    c901_po_dtls_name = v_rev_rec_po_type_name,
		    c5514_last_updated_by = p_user_id,
		    c5514_last_updated_date = current_date
		WHERE c5506_robot_quotation_id = p_quote_id
		      AND c5514_em_quote_account_type = p_account_type --'Capital' or 'Metal'
		      AND c5514_void_fl IS NULL;
		
		--Fetching values from t5514_robot_ar_invoice_dtls to save into revenue sampling
		BEGIN
			SELECT c901_po_dtls,
			       c901_do_dtls,
			       c704_account_id
			INTO
				v_po_dtls,
				v_do_dtls,
				v_account_id
			FROM t5514_robot_ar_invoice_dtls
			WHERE c5506_robot_quotation_id = p_quote_id
			      AND c5514_em_quote_account_type = p_account_type --'Capital' or 'Metal'
			      AND c5514_void_fl IS NULL;
		EXCEPTION
			WHEN no_data_found THEN
				v_po_dtls := '';
				v_do_dtls := '';
				v_account_id := '';
		END;
		
		FOR cur_order IN cur_order_id LOOP
		BEGIN
		--TO update the PO and DO Type into revenue sampling table
		IF(cur_order.order_id IS NOT NULL) THEN
		gm_pkg_ac_ar_revenue_sampling.gm_sav_revenue_sampling_dtls (cur_order.order_id, v_po_dtls, v_do_dtls, p_user_id);
		END IF;
		END;
		END LOOP;
	END gm_upd_rev_rec_po_dtls;
	
	/**********************************************************************
 	* Description : Procedure used to update revenue Recog DO Dtls based on Account Type
 	* Author      : saravanan M
	************************************************************************/
	PROCEDURE gm_upd_rev_rec_do_dtls (
		p_quote_id          IN   t5507_robot_quotation_category_details.c5506_robot_quotation_id%TYPE,
		p_account_type      IN   t5507_robot_quotation_category_details.c5507_em_quote_account_type%TYPE,
		p_rev_rec_do_type   IN   t5514_robot_ar_invoice_dtls.c901_po_dtls%TYPE,
		p_user_id           IN   t5506_robot_quotation.c5506_last_updated_by%TYPE
	) AS
		v_rev_rec_do_type_name   t5514_robot_ar_invoice_dtls.c901_po_dtls_name%TYPE;
		v_po_dtls                t5514_robot_ar_invoice_dtls.c901_po_dtls%TYPE;
		v_do_dtls                t5514_robot_ar_invoice_dtls.c901_do_dtls%TYPE;
		v_account_id             t501_order.c704_account_id%TYPE;
		
		CURSOR cur_order_id IS
		SELECT c501_order_id order_id
		FROM t501_order
		WHERE c5506_robot_quotation_id = p_quote_id
		      AND c704_account_id = v_account_id
		      AND c501_void_fl IS NULL;
	BEGIN
		--To get the entered revReco Type Name
		BEGIN
		SELECT get_code_name (p_rev_rec_do_type)
		INTO v_rev_rec_do_type_name
		FROM dual;
		EXCEPTION
			WHEN no_data_found THEN
				v_rev_rec_do_type_name := '';
		END;
		
		--Updating the entered value in t5514_robot_ar_invoice_dtls
		UPDATE t5514_robot_ar_invoice_dtls
		SET c901_do_dtls = p_rev_rec_do_type,
		    c901_do_dtls_name = v_rev_rec_do_type_name,
		    c5514_last_updated_by = p_user_id,
		    c5514_last_updated_date = current_date
		WHERE c5506_robot_quotation_id = p_quote_id
		      AND c5514_em_quote_account_type = p_account_type -- 'Capital' or 'metal'
		      AND c5514_void_fl IS NULL;
		      
		--Fetching values from t5514_robot_ar_invoice_dtls to save into revenue sampling
		BEGIN
			SELECT c901_po_dtls,
			       c901_do_dtls,
			       c704_account_id
			INTO
				v_po_dtls,
				v_do_dtls,
				v_account_id
			FROM t5514_robot_ar_invoice_dtls
			WHERE c5506_robot_quotation_id = p_quote_id
			      AND c5514_em_quote_account_type = p_account_type --'Capital' or 'Metal'
			      AND c5514_void_fl IS NULL;
		EXCEPTION
			WHEN no_data_found THEN
				v_po_dtls := '';
				v_do_dtls := '';
				v_account_id := '';
		END;
		
		FOR cur_order IN cur_order_id LOOP
		BEGIN
		--TO update the PO and DO Type into revenue sampling table
		IF(cur_order.order_id IS NOT NULL) THEN
		gm_pkg_ac_ar_revenue_sampling.gm_sav_revenue_sampling_dtls (cur_order.order_id, v_po_dtls, v_do_dtls, p_user_id);
		END IF;
		END;
		END LOOP;
	END gm_upd_rev_rec_do_dtls;
	
		/**********************************************************************
 	* Description : Procedure used to update PO Details in t501_order
 	* Author      : saravanan M
	************************************************************************/
	PROCEDURE gm_fch_quote_orders (
		p_quote_id			IN   t5507_robot_quotation_category_details.c5506_robot_quotation_id%TYPE,
		p_cursor_order_ids	OUT   TYPES.cursor_type
	) AS
	BEGIN

	OPEN p_cursor_order_ids  FOR
		select c501_order_id order_id 
		from t501_order
		WHERE 
		C5506_ROBOT_QUOTATION_ID = p_quote_id AND c501_void_fl IS NULL;
		
	END gm_fch_quote_orders;
	
	/**********************************************************************
 	* Description : Procedure used to save PO Comments
 	* Author      : saravananM
	************************************************************************/
	PROCEDURE gm_upd_po_comments(
		p_quote_id				IN			t5514_robot_ar_invoice_dtls.c5506_robot_quotation_id%TYPE,
		p_po_comments			IN			t5514_robot_ar_invoice_dtls.C5514_INVOICE_COMMENTS%TYPE,
		p_user_id				IN			t5514_robot_ar_invoice_dtls.C5514_LAST_UPDATED_BY%TYPE
	)
	AS
	BEGIN
		UPDATE T5514_ROBOT_AR_INVOICE_DTLS
		SET C5514_PO_COMMENTS = p_po_comments,
		C5514_LAST_UPDATED_BY = p_user_id,
		C5514_LAST_UPDATED_DATE = CURRENT_DATE
		WHERE C5506_ROBOT_QUOTATION_ID = p_quote_id
		AND C5514_VOID_FL IS NULL;
		
		UPDATE t501_order
		   SET c501_comments = p_po_comments
		 WHERE c5506_robot_quotation_id = p_quote_id
		   AND c501_void_fl IS NULL;
		 
	END gm_upd_po_comments;
	
	
	/**********************************************************************
 	* Description : Procedure used to fetch PO Comments
 	* Author      : saravananM
	************************************************************************/
	PROCEDURE gm_fetch_po_comments(
		p_quote_id				IN			t5514_robot_ar_invoice_dtls.c5506_robot_quotation_id%TYPE,
		p_po_comments			OUT			t5514_robot_ar_invoice_dtls.C5514_INVOICE_COMMENTS%TYPE
	)
	AS 
	BEGIN
	BEGIN
		SELECT c5514_po_comments
		INTO
		p_po_comments
		FROM t5514_robot_ar_invoice_dtls
		WHERE c5506_robot_quotation_id = p_quote_id
		      AND ROWNUM = 1
		      AND C5514_VOID_FL IS NULL;
		EXCEPTION
		WHEN no_data_found THEN
			p_po_comments := '';
		END;
		      
	END gm_fetch_po_comments;
	
	/**********************************************************************
 	* Description : Procedure used to fetch file List Ids
 	* Author      : saravananM
	************************************************************************/
	PROCEDURE gm_fetch_file_list_ids(
		p_file_name				IN			VARCHAR2,
		p_file_list_id			OUT			TYPES.cursor_type
	)
	AS 
	BEGIN
	OPEN p_file_list_id  FOR
	SELECT c903_upload_file_list file_list_id
	FROM t903_upload_file_list
	WHERE c903_file_name IN (p_file_name)
	      AND c903_delete_fl IS NULL;
		      
	END gm_fetch_file_list_ids;
END gm_pkg_sm_robotics_ar_po_process;
/