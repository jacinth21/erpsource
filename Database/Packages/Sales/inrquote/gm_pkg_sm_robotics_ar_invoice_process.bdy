CREATE OR REPLACE PACKAGE BODY gm_pkg_sm_robotics_ar_invoice_process IS

	/**********************************************************************
 	* Description : Procedure used to fetch Invoice Report
 	* Author      : saravanan M
	************************************************************************/
	PROCEDURE gm_fch_invoiced_report(
		p_quote_id					IN			t5507_robot_quotation_category_details.c5506_robot_quotation_id%TYPE,
		p_out_Invoice_rpt_dtl		OUT			CLOB
	)
	AS
	BEGIN
	
	BEGIN
		SELECT JSON_ARRAYAGG(JSON_OBJECT(  
		'strInvoiceId' VALUE T503.C503_INVOICE_ID,
		'strMergedInvoiceDtlsId' VALUE T503.C5033_MERGED_INVOICE_DTLS_ID,
		'strLastUpdatedBy' VALUE T503.C503_LAST_UPDATED_BY,
		'strLastUpdatedDate' VALUE T503.C503_LAST_UPDATED_DATE,
		'strMergedInvoiceId' VALUE T503.C5038_MERGED_INVOICE_ID,
		'strQuoteId' VALUE T503.C5506_ROBOT_QUOTATION_ID)) INTO p_out_Invoice_rpt_dtl -- New column on . update DDL
		FROM T503_INVOICE T503
		WHERE T503.C5506_ROBOT_QUOTATION_ID = p_quote_id
		AND T503.C503_VOID_FL IS NULL;
	 EXCEPTION
                    WHEN no_data_found THEN
                        p_out_Invoice_rpt_dtl := '';
        END;
	END gm_fch_invoiced_report;
END gm_pkg_sm_robotics_ar_invoice_process;
/