CREATE OR REPLACE PACKAGE BODY gm_pkg_sm_robotics_confim IS

	/**********************************************************************
 	* Description : Procedure used to Confirm robotic quote in INR
 	* Author      : Mahendran D
	************************************************************************/
    PROCEDURE gm_inr_confirm_quote (
        p_quote_id                    IN   t5506_robot_quotation.c5506_robot_quotation_id%TYPE,
        p_user_id                     IN   t5506_robot_quotation.c5506_last_updated_by%TYPE
    ) AS
    v_rfs_error_fl		VARCHAR2(1);

     BEGIN
	     
      	gm_upd_quote_capital_metal_acc_price (p_quote_id, p_user_id);
      	
      	gm_sav_quote_def_shipping_info(p_quote_id, p_user_id);
      
      	gm_pkg_sm_robotics_set_allocation.gm_remove_requested_set_list(p_quote_id, p_user_id);
      
        gm_pkg_sm_robotics_set_allocation.gm_sav_requested_set_list_bulk(p_quote_id,p_user_id);
     	
     	--To validate rfs quote	
		gm_validate_rfs_quote_main(p_quote_id,'Y','N','N',p_user_id,v_rfs_error_fl);
		
		--Insert record into t5514_robot_ar_invoice_dtls table for capital account AR Process details save and update
		gm_pkg_sm_robotics_ar_po_process.gm_sav_quote_po_details(p_quote_id,'Capital','','','','','','','',p_user_id);
		
		--Insert record into t5514_robot_ar_invoice_dtls table for Metal account AR Process details save and update
		gm_pkg_sm_robotics_ar_po_process.gm_sav_quote_po_details(p_quote_id,'Metal','','','','','','','',p_user_id);
		
     END gm_inr_confirm_quote;

 	
     /**********************************************************************
 	  * Description : Procedure used to Update Capital and Metal Account Price on Confirm Quote
 	  * Author      : Mahendran D
	 ************************************************************************/
     PROCEDURE gm_upd_quote_capital_metal_acc_price (
        p_quote_id    IN   t5506_robot_quotation.c5506_robot_quotation_id%TYPE,
        p_user_id     IN   t5506_robot_quotation.c5506_last_updated_by%TYPE
     ) AS
		v_metal_list_price	T5507_ROBOT_QUOTATION_CATEGORY_DETAILS.C5507_LIST_PRICE%TYPE;
		v_cap_list_price	T5507_ROBOT_QUOTATION_CATEGORY_DETAILS.C5507_LIST_PRICE%TYPE;
		v_capital_cat_type  T5507_ROBOT_QUOTATION_CATEGORY_DETAILS.C5507_EM_QUOTE_ACCOUNT_TYPE%TYPE := 'Capital';
		v_metal_cat_type    T5507_ROBOT_QUOTATION_CATEGORY_DETAILS.C5507_EM_QUOTE_ACCOUNT_TYPE%TYPE := 'Metal';
	 BEGIN
	   BEGIN
		SELECT SUM(C5507_QTY * (C5507_LIST_PRICE - ((C5507_LIST_PRICE/100)*NVL(C5507_DISCOUNT_PER,0))))
		  INTO v_metal_list_price
		  FROM T5507_ROBOT_QUOTATION_CATEGORY_DETAILS
		 WHERE C5506_ROBOT_QUOTATION_ID = p_quote_id
		   AND c5507_void_fl IS NULL
		   AND C5507_QTY > 0
		   AND C5507_EM_QUOTE_ACCOUNT_TYPE = v_metal_cat_type
	  GROUP BY C5507_EM_QUOTE_ACCOUNT_TYPE;
	  EXCEPTION WHEN NO_DATA_FOUND THEN
	  	v_metal_list_price := 0;
	  	END;
	  
	  
	  BEGIN
	  	SELECT SUM(C5507_QTY * (C5507_LIST_PRICE - ((C5507_LIST_PRICE/100)*NVL(C5507_DISCOUNT_PER,0))))
		  INTO v_cap_list_price
		  FROM T5507_ROBOT_QUOTATION_CATEGORY_DETAILS
		 WHERE C5506_ROBOT_QUOTATION_ID = p_quote_id
		   AND c5507_void_fl IS NULL
		   AND C5507_QTY > 0
		   AND C5507_EM_QUOTE_ACCOUNT_TYPE = v_capital_cat_type
		   AND c5502_robot_quotation_category_master_id not in
		   	   (SELECT c906_rule_value
				  FROM t906_rules
			     WHERE c906_rule_id = 'SKIP_SERVICE_PART'
				   AND c906_rule_grp_id='QUOTE_PARTS'
				   AND c906_void_fl is null)
			       AND c5507_ref_id not in (SELECT c906_rule_value FROM t906_rules
									   	     WHERE c906_rule_id = 'MANUAL_ADJ_PARTS' AND c906_rule_grp_id='MANUAL_ADJ_SKIP_TOT' AND c906_void_fl is null)
	  GROUP BY C5507_EM_QUOTE_ACCOUNT_TYPE;
	  EXCEPTION WHEN NO_DATA_FOUND THEN
	  	v_cap_list_price := 0;
	  	END;

		-- Script used for update capital and metal account price
		UPDATE T5506_ROBOT_QUOTATION
		   SET C5506_CAPITAL_ACCOUNT_TOTAL = v_cap_list_price,
		       C5506_METAL_ACCOUNT_TOTAL   = v_metal_list_price,
			   C5506_LAST_UPDATED_BY       = p_user_id,
			   C5506_LAST_UPDATED_DATE     = CURRENT_DATE,
			   C5506_QUOTE_CONFIRMED_BY    = p_user_id,
			   C5506_QUOTE_CONFIRMED_BY_NAME = get_user_name(p_user_id),
			   C5506_QUOTE_CONFIRMED_DATE  = CURRENT_DATE			   
		 WHERE C5506_ROBOT_QUOTATION_ID  = p_quote_id
		   AND C5506_VOID_FL IS NULL;

	   END gm_upd_quote_capital_metal_acc_price;

	/**********************************************************************
	* Description : Procedure used to validate rfs quote
	* Author      : Jeeva B
	************************************************************************/
	PROCEDURE gm_validate_rfs_quote_main (
		p_quote_id       	IN   t5506_robot_quotation.c5506_robot_quotation_id%TYPE,
		p_inr_confirm_fl   	IN   VARCHAR,
		p_set_confirm_fl    IN   VARCHAR,
		p_order_confirm_fl  IN   VARCHAR,
		p_user_id           IN   t5506_robot_quotation.c5506_last_updated_by%TYPE,
		p_rfs_error_fl		OUT	 VARCHAR2
	) AS
        v_ship_to_country   t704_account.c704_ship_country%TYPE;
        v_distributor_id   	t704_account.c704_account_id%TYPE;
		v_ref_id 			t5507_robot_quotation_category_details.c5507_ref_id%TYPE;
	BEGIN
		BEGIN
			--To get country id and distributor id
			SELECT t704.c704_ship_country,
    			get_account_dist_id(t704.c704_account_id)
			INTO  v_ship_to_country, v_distributor_id   			
			FROM
    			t5506_robot_quotation   t5506,
    			t704_account            t704
			WHERE
    			t704.c704_account_id = t5506.c704_metal_account_id
		    AND t704.c704_void_fl IS NULL
		    AND t5506.c5506_void_fl IS NULL
		    AND t5506.c5506_robot_quotation_id = p_quote_id
		    AND t704.c704_active_fl = 'Y';
		EXCEPTION WHEN NO_DATA_FOUND THEN
			v_ship_to_country := null;
			v_distributor_id := null;
    	END;
    	
    	-- To delete the temp table.
    	DELETE FROM TEMP_PART_QTY;
    	
    	-- To quote part items to the temp table 
		--IF(p_inr_confirm_fl = 'Y' OR p_order_confirm_fl = 'Y' ) THEN
			gm_ins_temp_quote_part_details (p_quote_id);
		--END IF;

		-- To quote set part items to temp table
		--IF(p_inr_confirm_fl = 'Y' OR p_set_confirm_fl = 'Y' ) THEN
			gm_ins_temp_quote_set_part_details (p_quote_id);
		--END IF;
		
		--This Procedure Minus the temp parts and RFS part
		gm_pkg_qa_rfs.gm_validate_rfs_by_country(v_ship_to_country);
		
		--update the unreleased part values in to quote table for other use 
		gm_upd_quote_rfs_dtls(p_quote_id, p_user_id,p_rfs_error_fl);
		
    	-- To delete the temp table.		
		DELETE FROM	my_temp_part_list;
		
	END gm_validate_rfs_quote_main;
	
	/**********************************************************************
	 * Description : Procedure used to insert temp quote part details.
	 * Author      : Jeeva B
	 ************************************************************************/
	PROCEDURE gm_ins_temp_quote_part_details (
		p_quote_id       IN   t5506_robot_quotation.c5506_robot_quotation_id%TYPE
	) AS
	BEGIN
		INSERT INTO TEMP_PART_QTY (PART)
			SELECT T5507.C5507_REF_ID
			FROM 
				T5507_ROBOT_QUOTATION_CATEGORY_DETAILS T5507
			WHERE
				T5507.C5506_ROBOT_QUOTATION_ID = p_quote_id
			AND T5507.C5507_QTY > 0
			AND T5507.C5507_VOID_FL IS NULL
			AND T5507.C901_REF_TYPE = '111120'; -- Part type
	END gm_ins_temp_quote_part_details;
	
	/**********************************************************************
	 * Description : Procedure used to insert temp quote part details.
	 * Author      : Jeeva B
	 ************************************************************************/
	PROCEDURE gm_ins_temp_quote_set_part_details (
		p_quote_id       IN   t5506_robot_quotation.c5506_robot_quotation_id%TYPE
	) AS
	BEGIN
		INSERT INTO TEMP_PART_QTY (PART)
			SELECT T5509.C205_PART_NUMBER_ID
			FROM 
				T5507_ROBOT_QUOTATION_CATEGORY_DETAILS T5507,
				T5509_ROBOT_QUOTATION_SET_DETAIL_PRICE T5509
			WHERE
				T5509.C5506_ROBOT_QUOTATION_ID = T5507.C5506_ROBOT_QUOTATION_ID
			AND T5507.C5507_REF_ID = T5509.C207_SET_ID
			AND T5507.C5506_ROBOT_QUOTATION_ID = p_quote_id
			AND T5509.C5509_REQ_QTY > 0
			AND T5507.C5507_QTY > 0
			AND T5507.C5507_VOID_FL IS NULL
			AND T5509.C5509_VOID_FL IS NULL
			AND T5507.C901_REF_TYPE = '111121'; --set type
	END gm_ins_temp_quote_set_part_details;
	
	/**********************************************************************
	 * Description : Procedure used to update the unreleased part values in to quote table for other use 
	 * Author      : Jeeva B
	 ************************************************************************/
	PROCEDURE gm_upd_quote_rfs_dtls (
		p_quote_id       IN   t5506_robot_quotation.c5506_robot_quotation_id%TYPE,
		p_user_id        IN   t5506_robot_quotation.c5506_last_updated_by%TYPE,
		p_rfs_error_fl	 OUT  VARCHAR2
	) AS
	v_part_number CLOB;
	BEGIN

		FOR i IN (SELECT c205_part_number_id FROM my_temp_part_list)
		LOOP
		v_part_number := v_part_number ||','|| i.c205_part_number_id;
		END LOOP;
		
		v_part_number := SUBSTR (v_part_number, INSTR (v_part_number, ',')    + 1) ;
		BEGIN
		UPDATE T5506_ROBOT_QUOTATION
		SET
			C5506_RFS_ERROR_DTLS = v_part_number,
			C5506_RFS_CHECK_FL = 'Y', 
			C5506_RFS_VALIDATE_DATE = CURRENT_DATE,
			C5506_LAST_UPDATED_BY = p_user_id,
			C5506_LAST_UPDATED_DATE = CURRENT_DATE
		WHERE C5506_ROBOT_QUOTATION_ID = p_quote_id
		AND C5506_VOID_FL IS NULL;
		END;
	
		IF (v_part_number IS NOT NULL) THEN
            p_rfs_error_fl := 'N';
		ELSE
            p_rfs_error_fl := 'Y';
        END IF;
		
	END gm_upd_quote_rfs_dtls;	   
	
	/**********************************************************************
 	* Description : Procedure used to sav the default shipping details
 	* Author      : Saravanan M
	************************************************************************/
	PROCEDURE gm_sav_quote_def_shipping_info(
	    p_quote_id                    IN   t5506_robot_quotation.c5506_robot_quotation_id%TYPE,
        p_user_id                     IN   t5506_robot_quotation.c5506_last_updated_by%TYPE
	) AS
		--cursor to get category details
		CURSOR robot_category_dtls IS
		SELECT 
			c5507_order_category_seq			order_category_seq_no,
		    c5507_em_quote_ship_to_type_name	order_shipto_name,
		    c5507_em_quote_account_type 		order_account_type
		FROM t5507_robot_quotation_category_details
		WHERE c5506_robot_quotation_id = p_quote_id
			AND c5507_void_fl IS NULL
		GROUP BY c5507_order_category_seq,
			c5507_em_quote_ship_to_type_name,c5507_em_quote_account_type;
	
		v_ship_ref_id				VARCHAR2(100);
		v_ship_to					VARCHAR2(20);
		v_ship_to_id				VARCHAR2(20);
		v_sales_rep_id				VARCHAR2(20);
		v_pref_carrier				VARCHAR2(20);
		v_capital_acc_id			t5506_robot_quotation.c704_capital_account_id%TYPE;
		v_metal_acc_id				t5506_robot_quotation.c704_metal_account_id%TYPE;
		v_capital_sales_rep_id		t5506_robot_quotation.c703_capital_sales_rep_id%TYPE;
		v_metal_sales_rep_id		t5506_robot_quotation.c703_metal_sales_rep_id%TYPE;
		v_ship_id					VARCHAR2(100);
		
	BEGIN
		--used to get Account ID for Quote country
		SELECT c704_capital_account_id	,
		       c704_metal_account_id	,
		       c703_capital_sales_rep_id,
		       c703_metal_sales_rep_id		
		INTO
				v_capital_acc_id, v_metal_acc_id, 
				v_capital_sales_rep_id, v_metal_sales_rep_id
		FROM t5506_robot_quotation
		WHERE c5506_robot_quotation_id = p_quote_id
		      AND c5506_void_fl IS NULL;

		FOR category_dtl IN robot_category_dtls LOOP
		--Exclude saving shipping details for bill only from sales consignment
		IF category_dtl.order_shipto_name <> 'N/A' THEN 
		v_ship_ref_id := category_dtl.order_category_seq_no||'-'||p_quote_id;

		IF category_dtl.order_shipto_name = 'Hospital' THEN
			v_ship_to := 4122;
			ELSE
			v_ship_to := '';
		END IF;
		
		IF category_dtl.order_account_type = 'Capital' THEN
			v_ship_to_id := v_capital_acc_id;
			v_sales_rep_id := v_capital_sales_rep_id;
		ELSE
			v_ship_to_id := v_metal_acc_id;
			v_sales_rep_id := v_metal_sales_rep_id;
		END IF;
		v_ship_id := '';
			--To fetch default carrier
			gm_pkg_cm_address.gm_fch_preferred_carrier(v_sales_rep_id,v_ship_to,0,v_ship_to_id,v_pref_carrier);
			--To save shipping details in t907
			gm_pkg_cm_shipping_trans.gm_sav_ship_info(v_ship_ref_id,'50180',v_ship_to,v_ship_to_id,v_pref_carrier,'5004','','','',p_user_id,v_ship_id,'','','',''); --(50180)order 5004-priority Overnight(default)
		
			--To update shipping status as completed(40) 
			gm_upd_quote_ship_sts(v_ship_ref_id,p_user_id);
		END IF;
		END LOOP;
	END gm_sav_quote_def_shipping_info;
	
    /*****************************************************************************************************************
 	* Description : Procedure used to update status fl as 40 based on catrgory id plus quote id like '1-GMQT-113021-1'
 	* Author      : Agilan Singaravel
	*****************************************************************************************************************/
	PROCEDURE gm_upd_quote_ship_sts(
		p_quote_id			IN		t5513_robot_set_allocation.c5506_robot_quotation_id%type,
		p_user_id			IN		t5000_order_consignment_mapping.c5000_last_updated_by%type
	)
	AS
	BEGIN
		
		UPDATE T907_SHIPPING_INFO
		   SET c907_status_fl = 40  --Completed
		     , c907_last_updated_by = p_user_id
		     , c907_last_updated_date = current_date
		 WHERE c907_ref_id = p_quote_id
		   AND c907_void_fl is null;
		 
	END gm_upd_quote_ship_sts;
END gm_pkg_sm_robotics_confim;
/