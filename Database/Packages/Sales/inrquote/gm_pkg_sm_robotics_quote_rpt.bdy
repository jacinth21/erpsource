CREATE OR REPLACE PACKAGE BODY gm_pkg_sm_robotics_quote_rpt IS

		/**********************************************************************
 	* Description : Procedure used to validate Quote ID
 	* Author      : saravanan M
	************************************************************************/
     PROCEDURE gm_validate_quote_id (
        p_quote_id      IN   t5506_robot_quotation.c5506_robot_quotation_id%TYPE,
        p_out			OUT	 NUMBER
        )
        AS
        BEGIN
        
        SELECT count(1) INTO p_out
		FROM T5506_ROBOT_QUOTATION
		WHERE C5506_ROBOT_QUOTATION_ID = p_quote_id
		AND C5506_VOID_FL IS NULL;
		
        END gm_validate_quote_id;

	/**********************************************************************
 	* Description : Procedure used to Fetch header details of Quote
 	* Author      : Jeeva B
	************************************************************************/
     PROCEDURE gm_fetch_header_details (
        p_quote_id      IN   t5506_robot_quotation.c5506_robot_quotation_id%TYPE,
        p_date_format	IN	 VARCHAR2,
        p_header_dtls 	OUT  TYPES.cursor_type
        )
     AS
       BEGIN
    	OPEN p_header_dtls FOR	        
		 SELECT T5506.C5506_ROBOT_QUOTATION_ID quoteId,
	         to_char(T5506.C5506_QUOTE_START_DATE, p_date_format) quoteStartDate,
	         to_char(T5506.C5506_EXPIRATION_DATE, p_date_format) quoteExpiryDate,
			 T5506.C5506_ROBOT_QUOTE_TOTAL quoteTotal,get_code_name(T5506.C901_QUOTE_STATUS) quoteStatus,
			 C704_CAPITAL_ACCOUNT_ID capitalAccId,
	         C704_METAL_ACCOUNT_ID metalAccId,
			 get_rep_name(c703_capital_sales_rep_id) capitalsalesname, 
	         get_account_name(t5506.c704_capital_account_id) capitalAccNm,
	         get_account_name(t5506.c704_metal_account_id) metalAccNm,
	         get_rep_name(t5506.c703_metal_sales_rep_id) salesrepName,
			 c5506_quote_confirmed_by_name      strconfirmedby,
			 get_user_name(c5506_last_updated_by)      strAllocationConfirmBy, 
			 to_char(c5506_quote_confirmed_date, p_date_format) strconfirmeddate,
	         c5506_capital_account_total   strcapitaltotal, 
			 c5506_metal_account_total     strmetaltotal, 
			 GET_SHIP_ADD (5021,c704_capital_account_id, '','') strshiptoaddress,
	         get_user_name(C5506_CAPITAL_VP_ID) zoneDirector 
	     FROM  T5506_ROBOT_QUOTATION T5506,T5500_ROBOT_MASTER T5500 
		 	WHERE T5500.C5500_ROBOT_MASTER_ID = T5506.C5500_ROBOT_MASTER_ID 
		 AND T5506.C5506_ROBOT_QUOTATION_ID= p_quote_id 
		 AND T5506.C5506_VOID_FL IS NULL 
		 AND T5500.C5500_VOID_FL IS NULL;
		 
	   END gm_fetch_header_details;
	   
	/**********************************************************************
 	* Description : Procedure used to fetch set part price in t5509 table
 	* Author      : Agilan Singaravel
	************************************************************************/
	FUNCTION get_quote_part_price (
	 	p_quote_id      IN   t5506_robot_quotation.c5506_robot_quotation_id%type,
        p_set_id    	IN	 t5509_robot_quotation_set_detail_price.c207_set_id%type,
        p_part_num		IN	 t5509_robot_quotation_set_detail_price.c205_part_number_id%type
    )
	RETURN 	NUMBER
	IS
	v_price  T5509_ROBOT_QUOTATION_SET_DETAIL_PRICE.c5509_list_price%type;
	BEGIN
 
	     BEGIN
	         SELECT nvl((c5509_list_price-((c5507_discount_per*c5509_list_price)/100)),c5509_list_price)
	         INTO v_price
	         FROM T5509_ROBOT_QUOTATION_SET_DETAIL_PRICE t5509,T5507_ROBOT_QUOTATION_CATEGORY_DETAILS t5507 
	        WHERE t5509.c5506_robot_quotation_id = t5507.c5506_robot_quotation_id
	          AND t5509.c207_set_id = t5507.c207_set_id
              AND c901_ref_type = 111121  --set
	          AND t5509.c5506_robot_quotation_id = p_quote_id
	          AND t5509.c207_set_id = p_set_id
	          AND t5509.c205_part_number_id = p_part_num
	          AND c5509_void_fl is null
              AND c5507_void_fl is null;
	     EXCEPTION WHEN NO_DATA_FOUND THEN
	     	v_price := '0';
	     END;
	     RETURN v_price;
	     
	  END get_quote_part_price;
	  
	/**********************************************************************
 	* Description : Procedure used to Fetch Quote Company ID
 	* Author      : Mahendran D
	************************************************************************/
     PROCEDURE gm_fch_quote_company_id (
        p_quote_id      IN   t5506_robot_quotation.c5506_robot_quotation_id%TYPE,
        p_out			OUT	 NUMBER
        )
        AS
        BEGIN
        	BEGIN
		        SELECT C1900_COMPANY_ID 
		          INTO p_out
				  FROM T5506_ROBOT_QUOTATION
				 WHERE C5506_ROBOT_QUOTATION_ID = p_quote_id
				   AND C5506_VOID_FL IS NULL;
			EXCEPTION WHEN NO_DATA_FOUND THEN
	     	     p_out := '0';
	     	END;
		
        END gm_fch_quote_company_id;	  
	  
END gm_pkg_sm_robotics_quote_rpt;
/