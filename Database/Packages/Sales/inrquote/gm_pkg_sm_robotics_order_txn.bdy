CREATE OR REPLACE PACKAGE BODY gm_pkg_sm_robotics_order_txn IS

	/*************************************************
 	* Description : Procedure used to create a order
 	* Author      : Agilan Singaravel
	*************************************************/
	PROCEDURE gm_create_order_main(
		p_quote_id			IN		t5513_robot_set_allocation.c5506_robot_quotation_id%type,
		p_order_cat_seq_id	IN		CLOB,
		p_comments			IN		VARCHAR2,		
		p_user_id			IN		t5513_robot_set_allocation.c5513_last_updated_by%type
	)
	AS
	BEGIN
		
		gm_upd_quote_order_process_fl(p_quote_id,p_order_cat_seq_id,'P',p_user_id);
		
	END gm_create_order_main;
	
	/*************************************************************************
 	* Description : Procedure used to update order process flag in t5507 table
 	* Author      : Agilan Singaravel
	*************************************************************************/
	PROCEDURE gm_upd_quote_order_process_fl(
		p_quote_id			IN		t5513_robot_set_allocation.c5506_robot_quotation_id%type,
		p_order_cat_seq_id	IN		CLOB,
		p_process_fl		IN		t5507_robot_quotation_category_details.c5507_quote_order_process_flag%type,		
		p_user_id			IN		t5513_robot_set_allocation.c5513_last_updated_by%type
	)
	AS
	BEGIN
		
		my_context.set_my_cloblist(p_order_cat_seq_id||',');
		
		UPDATE T5507_ROBOT_QUOTATION_CATEGORY_DETAILS
		   SET c5507_quote_order_process_flag = p_process_fl,
		   	   c5507_em_quote_order_category_status = DECODE(p_process_fl,'P','Order In-Progress','Y','Order Placed'),
			   c5507_last_updated_by = p_user_id,
			   c5507_last_updated_date = CURRENT_DATE
		 WHERE c5506_robot_quotation_id = p_quote_id
		   AND c5507_order_category_seq IN (SELECT token FROM v_clob_list WHERE token IS NOT NULL)
		   AND c5507_void_fl IS NULL;
		
	END gm_upd_quote_order_process_fl;
	
	/*************************************************************************************
 	* Description : Procedure used to validate order and create new order if no validation
 	* Author      : Agilan Singaravel
	**************************************************************************************/
	PROCEDURE gm_order_process_main(
		p_quote_id			IN		t5513_robot_set_allocation.c5506_robot_quotation_id%type,
		p_order_cat_seq_id	IN		CLOB,
		p_comments			IN		t501_order.c501_comments%type,
		p_user_id			IN		t5513_robot_set_allocation.c5513_last_updated_by%type
	)
	AS
	CURSOR fch_cat_seq
	IS
			SELECT token cat_seq_id
			  FROM v_clob_list 
			 WHERE token IS NOT NULL;
	BEGIN
		
		my_context.set_my_cloblist(p_order_cat_seq_id||',');
		
		FOR v_fch_cat_seq IN fch_cat_seq
		LOOP
		
			gm_validate_order_category(p_quote_id,v_fch_cat_seq.cat_seq_id,p_comments,p_user_id);
			
		END LOOP;
		  	
			gm_check_quote_orders_status(p_quote_id,p_user_id);
			
			--gm_upd_quote_ship_and_BO_sts(p_quote_id,p_user_id);
			
			gm_upd_quote_order_process_fl (p_quote_id,p_order_cat_seq_id,'Y',p_user_id);
			
	END gm_order_process_main;
	
	/***********************************************************************************************************
 	* Description : Procedure used to validate order and create new order if no validation and based on ref type
 	* Author      : Agilan Singaravel
	***********************************************************************************************************/
	PROCEDURE gm_validate_order_category(
		p_quote_id			IN		t5513_robot_set_allocation.c5506_robot_quotation_id%type,
		p_order_cat_seq		IN		t5507_robot_quotation_category_details.c5507_order_category_seq%type,
		p_comments			IN		t501_order.c501_comments%type,
		p_user_id			IN		t5513_robot_set_allocation.c5513_last_updated_by%type
	)
	AS
	v_order_count		NUMBER;
	v_ref_type			t5507_robot_quotation_category_details.c901_ref_type%type;
	BEGIN
			SELECT count(1)
		  	  INTO v_order_count
		  	  FROM T5507_ROBOT_QUOTATION_CATEGORY_DETAILS
		 	 WHERE c5507_order_category_seq = p_order_cat_seq
		   	   AND c5506_robot_quotation_id = p_quote_id
		   	   AND c501_order_id is not null
		   	   AND c5507_qty > 0
		   	   AND c5507_void_fl is null;
		   	   
		   	IF v_order_count = 0 THEN
		   	  BEGIN
		   		SELECT distinct c901_ref_type
		   		  INTO v_ref_type
		   		  FROM T5507_ROBOT_QUOTATION_CATEGORY_DETAILS
		   		 WHERE c5506_robot_quotation_id = p_quote_id
		   		   AND c5507_order_category_seq = p_order_cat_seq
                   AND c5507_qty > 0
		   		   AND c5507_void_fl is null;
		   	  EXCEPTION WHEN NO_DATA_FOUND THEN
		   	  		v_ref_type := null;
		   	  END;
		   	  
		   	  IF v_ref_type = 111120 THEN  --Part
		   	  	gm_sav_quote_order_by_part(p_quote_id,p_order_cat_seq,v_ref_type,p_comments,p_user_id);
		   	  ELSE --  111121   Set 
		   	  	gm_sav_quote_order_by_set(p_quote_id,p_order_cat_seq,v_ref_type,p_comments,p_user_id);
		   	  END IF;
		    END IF;
		   	   
	END gm_validate_order_category;
	
	/******************************************************************
 	* Description : Procedure used to create order if ref type as part 
 	* Author      : Agilan Singaravel
	********************************************************************/
	PROCEDURE gm_sav_quote_order_by_part(
		p_quote_id			IN		t5513_robot_set_allocation.c5506_robot_quotation_id%type,
		p_order_cat_seq		IN		t5507_robot_quotation_category_details.c5507_order_category_seq%type,
		p_ref_type			IN		t5507_robot_quotation_category_details.c901_ref_type%type,
		p_comments			IN		t501_order.c501_comments%type,
		p_user_id			IN		t5513_robot_set_allocation.c5513_last_updated_by%type
	)
	AS
	v_shipping_ref_id	VARCHAR2(1000);
	v_ship_add			VARCHAR2(1000);
	v_input_string		CLOB;
	v_part				t5507_robot_quotation_category_details.c5507_ref_id%type;
	v_qty				t5507_robot_quotation_category_details.c5507_qty%type;
	v_capital_acc		t5506_robot_quotation.c704_capital_account_id%type;
	v_metal_acc			t5506_robot_quotation.c704_capital_account_id%type;
	v_quote_company_id	t5506_robot_quotation.c1900_company_id%type;
	v_order_id 			VARCHAR2(20);
	v_ship_to			t907_shipping_info.c901_ship_to%type;
	v_ship_to_id		t907_shipping_info.c907_ship_to_id%type;
	v_carrier			t907_shipping_info.c901_delivery_carrier%type;
	v_mode				t907_shipping_info.c901_delivery_mode%type;
	v_order_mode		t501_order.c501_receive_mode%type;
	v_address_id		t907_shipping_info.c106_address_id%type;
	v_shipid			VARCHAR2(100);
	v_order_total		NUMBER;
	v_order_type		NUMBER;
	v_message			t902_log.c902_comments%TYPE;
	
	CURSOR fch_order_dtls
	IS
		--1.1 Get order type and plant id to create orders
		SELECT distinct c5507_em_quote_order_type ordertype, c5040_plant_id plantid
		  FROM T5507_ROBOT_QUOTATION_CATEGORY_DETAILS
		 WHERE c5506_robot_quotation_id = p_quote_id
		   AND c5507_order_category_seq = p_order_cat_seq
		   AND c5507_void_fl IS NULL
		   AND c5507_qty > 0;

	BEGIN
    
			v_shipping_ref_id := p_order_cat_seq ||'-'|| p_quote_id;
			
			--1.2 Get Account and Company Details
	 		BEGIN
	 			SELECT distinct DECODE(c5507_em_quote_account_type,'Capital',c704_capital_account_id,c704_metal_account_id),c1900_company_id
	 	  		  INTO v_capital_acc,v_quote_company_id
		  		  FROM T5506_ROBOT_QUOTATION t5506, T5507_ROBOT_QUOTATION_CATEGORY_DETAILS t5507
		 		 WHERE t5506.c5506_robot_quotation_id = p_quote_id
		   		   AND t5506.c5506_robot_quotation_id = t5507.c5506_robot_quotation_id
		   		   AND c5507_order_category_seq = p_order_cat_seq
		   		   AND c5506_void_fl IS NULL
		   		   AND c5507_void_fl IS NULL;
      	     EXCEPTION WHEN NO_DATA_FOUND THEN
				v_capital_acc := null;
				v_metal_acc := null;
				v_quote_company_id := null;
			END;
			
			--1.3 Get the Shipping details for Order_Cat_seq
			BEGIN
				SELECT c901_ship_to,c907_ship_to_id,c901_delivery_carrier,c901_delivery_mode,c106_address_id
			  	  INTO v_ship_to,v_ship_to_id,v_carrier,v_mode,v_address_id
			  	  FROM T907_SHIPPING_INFO
			     WHERE c907_ref_id = v_shipping_ref_id
			       AND c907_void_fl is null;
		 	EXCEPTION WHEN NO_DATA_FOUND THEN
		 		v_ship_to := 4122;	--Hospital
		 		v_ship_to_id := v_capital_acc;
		 		v_carrier := 5001;	--Fedex
		 		v_mode := 5004;    --Priority Overnight
		 		v_address_id := 0;
		 	END;
		 	
		 	v_order_mode 	:= 5016;  --Email type
		 	
			FOR v_fch_order_dtls IN fch_order_dtls
			LOOP
		 	-- 1.4 Form the input string for order
			BEGIN
		 		SELECT RTRIM (XMLAGG (XMLELEMENT (e, inputdtls || '|')) .EXTRACT ('//text()'), '|')
		   		  INTO v_input_string
		   		  FROM (
		 		SELECT t5507.c5507_ref_id ||',' || SUM (t5507.C5507_QTY) || ','|| ',50300,,,'|| 
		 					nvl((c5507_list_price-((c5507_discount_per*c5507_list_price)/100)),c5507_list_price) inputdtls
		   		  FROM T5507_ROBOT_QUOTATION_CATEGORY_DETAILS t5507
		  		 WHERE c5506_robot_quotation_id = p_quote_id
				   AND c901_ref_type = p_ref_type
		    	   AND c5507_qty > 0
		    	   AND c5507_void_fl IS NULL
		    	   AND c5507_order_category_seq = p_order_cat_seq
	  		  GROUP BY t5507.c5507_ref_id, t5507.c5507_list_price,c5507_discount_per);
	 		EXCEPTION WHEN NO_DATA_FOUND THEN
				v_input_string := null;
	 		END;
			  
			--1.5 save the order details
			gm_pkg_cor_client_context.gm_sav_client_context(v_quote_company_id,v_fch_order_dtls.plantid);
		
			v_order_id := GET_NEXT_ORDER_ID ('');
			
			IF (v_fch_order_dtls.ordertype = 'Direct') THEN
				v_order_type := '26240232';
			ELSIF (v_fch_order_dtls.ordertype = 'Bill Only - From Sales Consignments')  THEN
				v_order_type := '2532';
			ELSE --IF  (v_fch_order_dtls.ordertype = 'OUS Distributor')  THEN
				v_order_type := '102080';
			END IF;
		
			gm_save_item_order(v_order_id,v_capital_acc,v_order_mode,'',p_comments,'','0',p_user_id,v_input_string ||'|',v_order_type,'',
								'',v_ship_to,v_ship_to_id,v_carrier,v_mode,v_address_id,v_shipid);
								
			--to update comments in log table
		    gm_update_log(v_order_id,p_comments,1200,p_user_id,v_message);
			
		   	--Update unit price in t502_item_order table
			gm_upd_unit_price(v_order_id,p_user_id);

			--1.8 get order total and update into Order Table
			gm_update_quote_order_total(v_order_id,v_order_type,p_quote_id,p_comments,p_user_id);

			--1.9 UPDATE the order id and order status into the quote table
			gm_upd_order_id_by_quote_category(p_quote_id,v_order_id,p_order_cat_seq,null,'Order Placed',p_user_id);
		
		END LOOP;
		
	END gm_sav_quote_order_by_part;
	
	/***********************************************************************
 	* Description : Procedure used to update total order based on order id
 	* Author      : Agilan Singaravel
	************************************************************************/
	PROCEDURE gm_update_quote_order_total(
		p_order_id		IN		t501_order.c501_order_id%type,
		p_order_type	IN		t501_order.c901_order_type%type,
		p_quote_id		IN		t5513_robot_set_allocation.c5506_robot_quotation_id%type,
        p_comments		IN		t501_order.c501_comments%type,
		p_user_id		IN		t5513_robot_set_allocation.c5513_last_updated_by%type
	)
	AS
	v_total				NUMBER;
	BEGIN
		
	-- update quote id in order table
		UPDATE T501_ORDER
		   SET c5506_robot_quotation_id = p_quote_id,			  
			   c501_ship_cost = 0,
			   c501_last_updated_by = p_user_id,
			   c501_last_updated_date = CURRENT_DATE
		 WHERE (c501_order_id = p_order_id OR c501_parent_order_id = p_order_id)
		   AND c501_void_fl IS NULL; 
		   
	-- update total cost for Bill only from sales consignments in order table
	  IF p_order_type = 2532 THEN --Bill Only - From Sales Consignments
		SELECT SUM (NVL (TO_NUMBER (c502_item_qty) * TO_NUMBER (c502_item_price), 0))
	  	  INTO v_total
	  	  FROM t502_item_order
	 	 WHERE c501_order_id = p_order_id;
	 
		UPDATE T501_ORDER
		   SET c501_total_cost = nvl(v_total,0),
			   c501_last_updated_by = p_user_id,
			   c501_last_updated_date = CURRENT_DATE
		 WHERE c501_order_id = p_order_id
		   AND c901_order_type = 2532   --Bill Only - From Sales Consignments
		   AND c501_void_fl IS NULL;
	  END IF;

	END gm_update_quote_order_total;
	
	/***********************************************************************
 	* Description : Procedure used to update category status as order placed
 	* Author      : Agilan Singaravel
	************************************************************************/
	PROCEDURE gm_upd_order_id_by_quote_category(
		p_quote_id			IN		t5513_robot_set_allocation.c5506_robot_quotation_id%type,
		p_order_id			IN		t501_order.c501_order_id%type,
		p_order_cat_seq		IN		t5507_robot_quotation_category_details.c5507_order_category_seq%type,
		p_set_id			IN		t5507_robot_quotation_category_details.c207_set_id%type,
		p_order_status		IN		VARCHAR2,
		p_user_id			IN		t5513_robot_set_allocation.c5513_last_updated_by%type		
	)
	AS
	BEGIN

		UPDATE T5507_ROBOT_QUOTATION_CATEGORY_DETAILS
		   SET c501_order_id = p_order_id,
			   c5507_em_quote_order_category_status = p_order_status,
			   c5507_last_updated_by = p_user_id,
			   c5507_last_updated_date = CURRENT_DATE
		 WHERE c5506_robot_quotation_id = p_quote_id
		   AND c5507_order_category_seq = p_order_cat_seq
		   AND c5507_ref_id = nvl(p_set_id,c5507_ref_id)
		   AND c5507_qty > 0
		   AND c5507_void_fl IS NULL;
		   
	END gm_upd_order_id_by_quote_category;
	
	/********************************************************************************
 	* Description : Procedure used to create order of set based on ref type as 111121
 	* Author      : Agilan Singaravel
	*********************************************************************************/
	PROCEDURE gm_sav_quote_order_by_set(
		p_quote_id			IN		t5513_robot_set_allocation.c5506_robot_quotation_id%type,
		p_order_cat_seq		IN		t5507_robot_quotation_category_details.c5507_order_category_seq%type,
		p_ref_type			IN		t5507_robot_quotation_category_details.c901_ref_type%type,
		p_comments			IN		t501_order.c501_comments%type,
		p_user_id			IN		t5513_robot_set_allocation.c5513_last_updated_by%type
	)
	AS
	v_capital_acc		t5506_robot_quotation.c704_capital_account_id%type;
	v_metal_acc			t5506_robot_quotation.c704_capital_account_id%type;
	v_quote_company_id	t5506_robot_quotation.c1900_company_id%type;
	v_order_mode		t501_order.c501_receive_mode%type;
	v_order_type		varchar2(50);
	v_order_id 			VARCHAR2(20);
	v_ship_to			t907_shipping_info.c901_ship_to%type;
	v_ship_to_id		t907_shipping_info.c907_ship_to_id%type;
	v_carrier			t907_shipping_info.c901_delivery_carrier%type;
	v_mode				t907_shipping_info.c901_delivery_mode%type;
	v_address_id		t907_shipping_info.c106_address_id%type;
	v_shipid			VARCHAR2(100);
	v_order_total		NUMBER;
	v_cn_back_order_req_cnt	NUMBER;
	v_shipping_ref_id	VARCHAR2(1000);
	v_input_string		CLOB;
	v_consign_id		VARCHAR2(4000);
	v_request_id		VARCHAR2(100);
	v_back_order_id		VARCHAR2(20);
	v_message			t902_log.c902_comments%TYPE;
	v_master_req_id		VARCHAR2(4000);
	CURSOR fch_order_dtls
	IS
		--1.1 Get order type and plant id to create orders
		SELECT c5507_ref_id setid, c5040_plant_id plantid,c5507_em_quote_order_type ordertype
		  FROM T5507_ROBOT_QUOTATION_CATEGORY_DETAILS
		 WHERE c5506_robot_quotation_id = p_quote_id
		   AND c5507_order_category_seq = p_order_cat_seq
		   AND C901_REF_TYPE = p_ref_type -- Set
		   AND c5507_qty > 0 
		   AND C5507_VOID_FL IS NULL;
	 	 
	BEGIN
			--1.2 Get Account and Company Details
			BEGIN
				SELECT distinct DECODE(c5507_em_quote_account_type,'Capital',c704_capital_account_id,c704_metal_account_id) accnum,
				        c1900_company_id 
	 	  	  	  INTO v_capital_acc,v_quote_company_id
		  	      FROM T5506_ROBOT_QUOTATION t5506, T5507_ROBOT_QUOTATION_CATEGORY_DETAILS t5507
		 	     WHERE t5506.c5506_robot_quotation_id = p_quote_id
		   	   	   AND t5506.c5506_robot_quotation_id = t5507.c5506_robot_quotation_id
		   	   	   AND c5507_order_category_seq = p_order_cat_seq
		   	   	   AND c5506_void_fl IS NULL
		       	   AND c5507_void_fl IS NULL;		  
		  	EXCEPTION WHEN NO_DATA_FOUND THEN
				v_capital_acc := null;
				v_metal_acc := null;
				v_quote_company_id := null;
		  	END;
			--1.3 Get the Shipping details for Order_Cat_seq
			v_shipping_ref_id := p_order_cat_seq ||'-'|| p_quote_id;
			
			BEGIN
				SELECT c901_ship_to,c907_ship_to_id,c901_delivery_carrier,c901_delivery_mode,c106_address_id
			  	  INTO v_ship_to,v_ship_to_id,v_carrier,v_mode,v_address_id
			      FROM T907_SHIPPING_INFO
			 	 WHERE c907_ref_id = v_shipping_ref_id
			   	   AND c907_void_fl is null;
		 	EXCEPTION WHEN NO_DATA_FOUND THEN
		 		v_ship_to := 4122; --Hospital
		 		v_ship_to_id := v_capital_acc;
		 		v_carrier := 5001;  --Fedex
		 		v_mode := 5004;   --Priority Overnight
		 		v_address_id := 0;
		 	END;
		 	
			FOR v_fch_order_dtls IN fch_order_dtls
			LOOP
            
			v_order_mode := 5016; -- 5016 EMailORDMO
			v_order_type := '101260';   --ACK Order Type';
			
		 BEGIN
			SELECT RTRIM (XMLAGG (XMLELEMENT (e, c520_request_id || ',')) .EXTRACT ('//text()'), ',')
			  INTO v_master_req_id
              FROM T504_CONSIGNMENT 
             WHERE c207_set_id = v_fch_order_dtls.setid 
               AND c5506_robot_quotation_id = p_quote_id
               AND c504_void_fl is null;
         EXCEPTION WHEN NO_DATA_FOUND THEN
         	v_master_req_id := null;
         END;
         
            my_context.set_my_inlist_ctx (v_master_req_id);
		 
            --1.4 Form the input string for order
		 	SELECT RTRIM (XMLAGG (XMLELEMENT (e, inputdtls || '|')) .EXTRACT ('//text()'), '|')
		 	  INTO v_input_string
			  FROM (
			SELECT t505.c205_part_number_id ||',' || SUM (t505.c505_item_qty) || ','|| t505.c505_control_number ||
				   ',50300,,,'|| gm_pkg_sm_robotics_quote_rpt.get_quote_part_price(p_quote_id,v_fch_order_dtls.setid, t505.c205_part_number_id) 
				   inputdtls
			  FROM T505_ITEM_CONSIGNMENT t505
			 WHERE --t504.c504_consignment_id = t505.c504_consignment_id
--			   AND t504.c207_set_id = v_fch_order_dtls.setid
--			   AND t504.c5506_robot_quotation_id = p_quote_id
			   --AND 
			       t505.c504_consignment_id IN (SELECT c504_consignment_id 
			   			FROM T504_CONSIGNMENT 
			   		   WHERE c520_request_id IN (SELECT c520_request_id 
			   		    FROM t520_request WHERE (c520_request_id IN (SELECT token FROM v_in_list) OR 
			   		    						C520_MASTER_REQUEST_ID IN (SELECT token FROM v_in_list))
               	         AND c520_void_fl is null) AND c504_void_fl is null)
			  -- AND t504.c504_void_fl IS NULL
			   AND t505.c505_void_fl IS NULL
		  GROUP BY t505.c205_part_number_id, t505.c505_control_number);
		  
		  -- to set the company context
		  gm_pkg_cor_client_context.gm_sav_client_context(v_quote_company_id,v_fch_order_dtls.plantid);
		  
		  v_order_id := GET_NEXT_ORDER_ID ('');
		  
		  --exiting procedure to call place order
		  gm_save_item_order(v_order_id,v_capital_acc,v_order_mode,'',p_comments,'','0',p_user_id,v_input_string ||'|',v_order_type,'',
							'',v_ship_to,v_ship_to_id,v_carrier,v_mode,v_address_id,v_shipid);
							
		  --to update comments in log table
		  gm_update_log(v_order_id,p_comments,1200,p_user_id,v_message);

	      -- UPDATE the order id and order status into the quote table
		  gm_upd_order_id_by_quote_category(p_quote_id,v_order_id,p_order_cat_seq,v_fch_order_dtls.setid,'Order Placed',p_user_id);	
		  
		  -- Use below script to update the order type
		  gm_upd_quote_order_status(v_order_id,'2',v_fch_order_dtls.setid,p_user_id); --Controlled
		  
		  --Use below script to update the order type
		  IF (v_fch_order_dtls.ordertype = 'Direct') THEN
				v_order_type := '26240232';
		  ELSIF  (v_fch_order_dtls.ordertype = 'OUS Distributor')  THEN
				v_order_type := '102080';
		  END IF;
		
		  gm_upd_quote_order_type(v_order_id,v_order_type,p_user_id);  --26240232 - DirectODTYP
		  
		  -- Update the shipping status as Pending Shipping
		  gm_upd_quote_shipping_status(v_order_id,v_shipping_ref_id,30,p_user_id);  --30 - Pending Shipping

		  -- Get the back order request count for the CN
		  BEGIN
		  	SELECT RTRIM (XMLAGG (XMLELEMENT (e, c504_consignment_id || ',')) .EXTRACT ('//text()'), ',')
		      INTO v_consign_id
		      FROM t5513_robot_set_allocation
		     WHERE c207_set_id = v_fch_order_dtls.setid
		       AND c5506_robot_quotation_id = p_quote_id
		       AND c5513_void_fl is null;
		  EXCEPTION WHEN NO_DATA_FOUND THEN
		  	v_consign_id := null;
		  END;
			 
		 	-- Get CN Back order req items
				SELECT count(1)
				  INTO v_cn_back_order_req_cnt
				  FROM T520_REQUEST T520,T521_REQUEST_DETAIL T521,T504_CONSIGNMENT T504
				 WHERE T520.c520_master_request_id = T504.c520_request_id
				   AND T520.c520_request_id = T521.c520_request_id
				   AND T504.c504_consignment_id in (SELECT c504_consignment_id 
		      	  FROM t5513_robot_set_allocation
		         WHERE c207_set_id = v_fch_order_dtls.setid
		           AND c5506_robot_quotation_id = p_quote_id
		           AND c5513_void_fl is null)
				   AND T520.c520_void_fl IS NULL
				   AND T520.c520_delete_fl IS NULL
				   AND T504.c504_void_fl IS NULL;
		
				IF v_cn_back_order_req_cnt > 0 THEN
					SELECT RTRIM (XMLAGG (XMLELEMENT (e, inputdtls || '|')) .EXTRACT ('//text()'), '|')
					  INTO v_input_string
					  FROM (
				    SELECT t521.c205_part_number_id ||',' || SUM (t521.C521_QTY) || ',' || ',50300,,,'|| 
				    		gm_pkg_sm_robotics_quote_rpt.get_quote_part_price(p_quote_id,v_fch_order_dtls.setid, t521.c205_part_number_id) 
				               inputdtls
					  FROM T520_REQUEST t520, T521_REQUEST_DETAIL t521,T504_CONSIGNMENT T504,t5513_robot_set_allocation t5513
				     WHERE t520.c520_request_id = t521.c520_request_id
					   AND t520.c520_master_request_id = t504.c520_request_id
		   			   AND t520.c520_request_id = t521.c520_request_id
           			   AND T504.c5506_robot_quotation_id = t5513.c5506_robot_quotation_id
           			   AND t5513.c504_consignment_id = t504.c504_consignment_id
           			   AND t5513.c207_set_id = v_fch_order_dtls.setid
		       		   AND t5513.c5506_robot_quotation_id = p_quote_id
					   AND t520.c520_void_fl IS NULL
                       AND t5513.c5513_void_fl is null
                       AND t504.c504_void_fl is null
				  GROUP BY t521.c205_part_number_id);
				  
				   v_back_order_id := GET_NEXT_ORDER_ID ('');
				   v_order_type := '2525'; --Back Order
				   
				   gm_pkg_cor_client_context.gm_sav_client_context(v_quote_company_id,v_fch_order_dtls.plantid);
				   
				   gm_pkg_cm_shipping_trans.gm_sav_shipping(v_back_order_id,50180,v_ship_to,v_ship_to_id,v_carrier,v_mode,null,v_address_id,0,p_user_id,v_shipid);
				   
		  		   gm_save_item_order(v_back_order_id,v_capital_acc,v_order_mode,'',p_comments,'','0',p_user_id,v_input_string ||'|',v_order_type,'',
									v_order_id,v_ship_to,v_ship_to_id,v_carrier,v_mode,v_address_id,v_shipid);
	
				   gm_void_backorder_request(v_consign_id,p_user_id);
				   
				   gm_upd_bo_status_fl(v_back_order_id,p_user_id);

		  END IF;
		     --update unit price in t502_item_order table
		  	gm_upd_unit_price(v_order_id,p_user_id);

		  	--get order total and update into Order Table
		    gm_update_quote_order_total(v_order_id,v_order_type,p_quote_id,p_comments,p_user_id);
            
		    -- to update consignment detail based on order in t5000 table
            gm_sav_consignment_order_dtls(v_order_id,v_master_req_id,v_fch_order_dtls.setid,p_quote_id,v_quote_company_id,p_user_id);

	 	 END LOOP;
		
	END gm_sav_quote_order_by_set;
	
	/***********************************************************************
 	* Description : Procedure used to update order status fl in t501 table
 	* Author      : Agilan Singaravel
	************************************************************************/
	PROCEDURE gm_upd_quote_order_status(
		p_order_id		IN		t501_order.c501_order_id%type,
		p_status		IN		varchar2,
		p_set_id        IN      t501_order.c207_set_id%type,
		p_user_id		IN		t5513_robot_set_allocation.c5513_last_updated_by%type
	)
	AS
	BEGIN
		
		UPDATE T501_ORDER
		   SET c501_status_fl = p_status,
		   	   c207_set_id = p_set_id,
			   c501_last_updated_by = p_user_id,
			   c501_last_updated_date = CURRENT_DATE
		 WHERE c501_order_id = p_order_id
		   AND c501_void_fl IS NULL;
		   
	END gm_upd_quote_order_status;
	
	/***********************************************************************
 	* Description : Procedure used to update order type in t501 table
 	* Author      : Agilan Singaravel
	************************************************************************/
	PROCEDURE gm_upd_quote_order_type(
		p_order_id		IN		t501_order.c501_order_id%type,
		p_order_type	IN		NUMBER,
		p_user_id		IN		t5513_robot_set_allocation.c5513_last_updated_by%type
	)
	AS
	BEGIN
		
		UPDATE T501_ORDER
		   SET c901_order_type = p_order_type,
			   c501_last_updated_by = p_user_id,
			   c501_last_updated_date = CURRENT_DATE
		 WHERE c501_order_id = p_order_id
		   AND c501_void_fl IS NULL;
           
	END gm_upd_quote_order_type;
	
	/*********************************************************************************
 	* Description : Procedure used to update quote shipping status as pending shipping
 	* Author      : Agilan Singaravel
	**********************************************************************************/
	PROCEDURE gm_upd_quote_shipping_status(
        p_order_id      IN      T501_order.c501_order_id%type,
		p_ref_id		IN		t907_shipping_info.c907_ref_id%type,
		p_status_id		IN		NUMBER,
		p_user_id		IN		t5513_robot_set_allocation.c5513_last_updated_by%type
	)
	AS
	BEGIN

--		UPDATE T907_SHIPPING_INFO
--		   SET c907_status_fl = p_status_id,
--			   c907_last_updated_by = p_user_id,
--			   c907_last_updated_date = CURRENT_DATE
--		 WHERE c907_ref_id = p_ref_id
--		   AND c907_void_fl IS NULL;
           
        UPDATE T907_SHIPPING_INFO
		   SET c907_status_fl = p_status_id,
               c901_source = 50180, --orders
			   c907_last_updated_by = p_user_id,
			   c907_last_updated_date = CURRENT_DATE
		 WHERE c907_ref_id = p_order_id
		   AND c907_void_fl IS NULL;
           
	END gm_upd_quote_shipping_status;
	
	/*******************************************************
 	* Description : Procedure used to update void fl as Y
 	* Author      : Agilan Singaravel
	********************************************************/
	PROCEDURE gm_void_backorder_request(
		p_cons_id			IN		CLOB,
		p_user_id			IN		t5513_robot_set_allocation.c5513_last_updated_by%type
	)
	AS
	CURSOR fch_req_id
	IS
		SELECT DISTINCT t521.c520_request_id request_id
		  FROM T520_REQUEST T520,T521_REQUEST_DETAIL T521,T504_CONSIGNMENT T504
		 WHERE t520.c520_master_request_id = t504.c520_request_id
		   AND t520.c520_request_id = t521.c520_request_id
		   AND t504.c504_consignment_id in (SELECT token
		  FROM v_clob_list 
		 WHERE token IS NOT NULL)
		   AND t520.c520_void_fl IS NULL
		   AND t520.c520_delete_fl IS NULL
		   AND t504.c504_void_fl IS NULL;
	
	BEGIN
		my_context.set_my_cloblist(p_cons_id||',');
		FOR v_fch_req_id in fch_req_id
		LOOP
		UPDATE T253C_REQUEST_LOCK
		   SET c520_void_fl = 'Y',
			   c520_last_updated_by = p_user_id,
			   c520_last_updated_date = CURRENT_DATE
		 WHERE c520_request_id = v_fch_req_id.request_id
		   AND c520_void_fl IS NULL;

		UPDATE t253a_consignment_lock
		   SET c504_void_fl = 'Y',
			   c504_last_updated_by = p_user_id,
			   c504_last_updated_date = CURRENT_DATE
		 WHERE c520_request_id = v_fch_req_id.request_id;
		 
		UPDATE T520_REQUEST
		   SET c520_void_fl = 'Y'
		     , c520_last_updated_by = p_user_id
		     , c520_last_updated_date = current_date
		 WHERE c520_request_id = v_fch_req_id.request_id
		   AND c520_status_fl = 10 --Back Order
		   AND c520_void_fl is null;
		   
		 END LOOP;

	END gm_void_backorder_request;
	
	/*******************************************************************************************
 	* Description : Procedure used to save consignment details in t5000 order cons mapping table
 	* Author      : Agilan Singaravel
	********************************************************************************************/
	PROCEDURE gm_sav_consignment_order_dtls(
		p_order_id			IN		t5000_order_consignment_mapping.c501_order_id%type,
		p_request_id		IN		VARCHAR2,
		p_set_id			IN		t5000_order_consignment_mapping.c207_set_id%type,
		p_ref_id			IN		t5000_order_consignment_mapping.c5000_ref_id%type,
		p_company_id		IN		t5000_order_consignment_mapping.c1900_company_id%type,
		p_user_id			IN		t5000_order_consignment_mapping.c5000_last_updated_by%type
	)
	AS
	CURSOR fch_cn
	IS
		SELECT c504_consignment_id  cnid
		  FROM T504_CONSIGNMENT 
		 WHERE c520_request_id IN (
			   SELECT c520_request_id 
			   	 FROM t520_request 
			   	WHERE (c520_request_id IN (select token from v_in_list) OR C520_MASTER_REQUEST_ID IN (select token from v_in_list))
               	  AND c520_void_fl is null) 
           AND c504_void_fl is null;
	BEGIN
		--my_context.set_my_cloblist(p_consignment_id||',');
		my_context.set_my_inlist_ctx (p_request_id);
		FOR v_fch_cn IN fch_cn
		LOOP
		UPDATE T5000_ORDER_CONSIGNMENT_MAPPING
		   SET c207_set_id = p_set_id,
			   c5000_ref_id = p_ref_id,
			   c1900_company_id = p_company_id,
			   c5000_last_updated_by = p_user_id,
			   c5000_last_updated_by_name = get_user_name(p_user_id),
			   c5000_last_updated_date = CURRENT_DATE
		 WHERE c501_order_id = p_order_id
		   AND c504_consignment_id = v_fch_cn.cnid
		   AND c5000_void_fl IS NULL;

		IF (SQL%ROWCOUNT = 0) THEN

		INSERT INTO T5000_ORDER_CONSIGNMENT_MAPPING
					(c501_order_id,c504_consignment_id,c207_set_id,--c207_set_nm,
					 c5000_ref_id,c1900_company_id,c5000_last_updated_by,c5000_last_updated_by_name,c5000_last_updated_date)
			 VALUES (p_order_id,v_fch_cn.cnid,p_set_id,--get_set_name(p_set_id),
					 p_ref_id,p_company_id,p_user_id,get_user_name(p_user_id),CURRENT_DATE);
		END IF;
		
		--to update CN status as Pending shipping 
		--gm_upd_cn_status(v_fch_cn.cnid,'4',p_user_id);  --4 Completed
		
		--to update tag status as sold
		--gm_upd_tag_status_by_cn(p_order_id,v_fch_cn.cnid,p_ref_id,'26240343',p_user_id);  --26240343 Sold
		
		--to update quote in t504 table
		gm_pkg_sm_robotics_set_allocation.gm_upd_quote_info_to_consignment(v_fch_cn.cnid,p_ref_id,p_user_id);
		
		END LOOP;
	END gm_sav_consignment_order_dtls;
	
	/************************************************************
 	* Description : Procedure used to save status in t5506 table
 	* Author      : Agilan Singaravel
	*************************************************************/
	PROCEDURE gm_check_quote_orders_status(
		p_quote_id			IN		t5513_robot_set_allocation.c5506_robot_quotation_id%type,
		p_user_id			IN		t5000_order_consignment_mapping.c5000_last_updated_by%type
	)
	AS
	v_quote_order_count				NUMBER;
	v_order_category_seq_count		NUMBER;
	v_order_status					NUMBER;
	v_order_shipped_count			NUMBER;
	BEGIN
		-- Get the orders count for the quote
		SELECT COUNT(1) 
		  INTO v_quote_order_count
		  FROM T5507_ROBOT_QUOTATION_CATEGORY_DETAILS T5507
		 WHERE T5507.c5506_robot_quotation_id = p_quote_id
		   AND T5507.c5507_qty > 0
		   AND t5507.c5502_robot_quotation_category_master_id not in 
		   	   (SELECT c906_rule_value 
		   	      FROM t906_rules 
		   	     WHERE c906_rule_id = 'SKIP_SERVICE_PART' 
		   	       AND c906_rule_grp_id='QUOTE_PARTS' 
		   	       AND c906_void_fl is null)
		   AND T5507.c501_order_id IS NOT NULL
		   AND T5507.c5507_void_fl IS NULL;

-- Get the order category seq count for compare with number orders
		SELECT COUNT(1) 
		  INTO v_order_category_seq_count
		  FROM T5507_ROBOT_QUOTATION_CATEGORY_DETAILS T5507
		 WHERE T5507.C5506_ROBOT_QUOTATION_ID = p_quote_id	
		   AND T5507.C5507_QTY > 0
		   AND t5507.c5502_robot_quotation_category_master_id not in 
		   	   (SELECT c906_rule_value 
		   	      FROM t906_rules 
		   	     WHERE c906_rule_id = 'SKIP_SERVICE_PART' 
		   	       AND c906_rule_grp_id='QUOTE_PARTS' 
		   	       AND c906_void_fl is null)
		   AND T5507.C501_ORDER_ID IS NULL
		   AND T5507.C5507_VOID_FL IS NULL;
		   
		   
		 SELECT COUNT(1)
		   INTO v_order_shipped_count
		   FROM T501_ORDER
		  WHERE c5506_robot_quotation_id = p_quote_id
		    AND c501_status_fl < 3  --Shipped
		    AND c501_void_fl is null;

		IF v_quote_order_count > 0 THEN
			v_order_status := '110824'; -- 110824   Order Process In-Progress
		END IF;
		
		IF v_order_category_seq_count = 0 THEN
			v_order_status := '110826'; -- 110826  Pending Shipping  
		END IF;
		
		IF v_order_shipped_count = 0 AND v_order_category_seq_count = 0 THEN
			v_order_status := '110827'; -- 110826  Shipping Completed 
		END IF;

		IF v_order_status IS NOT NULL THEN
			-- update quote status as Order Process In-Progress
			gm_pkg_sm_robotics_quote_trans.gm_upd_quote_status(p_quote_id, v_order_status,p_user_id);
		END IF;
	END gm_check_quote_orders_status;
	
	/****************************************************************
 	* Description : Procedure used to update unit price in t502 table
 	* Author      : Agilan Singaravel
	*****************************************************************/
	PROCEDURE gm_upd_unit_price(
		p_order_id			IN		t501_order.c501_order_id%type,
		p_user_id			IN		t5000_order_consignment_mapping.c5000_last_updated_by%type
	)
	AS
	BEGIN
		
		UPDATE T502_ITEM_ORDER
		   SET c502_unit_price = c502_item_price 
		 WHERE c501_order_id = p_order_id 
		   AND c502_void_fl is null;
		   
		   UPDATE T502_ITEM_ORDER
		   SET c502_unit_price = c502_item_price
		 WHERE c501_order_id IN (select c501_order_id from t501_order where c501_parent_order_id = p_order_id and c501_void_fl is null)
		   AND c502_void_fl is null;

	END gm_upd_unit_price;
	
	/****************************************************************
 	* Description : Procedure used to update status fl in t501 table
 	* Author      : Agilan Singaravel
	*****************************************************************/
	PROCEDURE gm_upd_bo_status_fl(
		p_order_id		IN		t501_order.c501_order_id%type,
		p_user_id		IN		t501_order.c501_last_updated_by%type
	)
	AS
	v_total			NUMBER:=0;
	BEGIN
		
		SELECT sum(c502_item_qty*c502_item_price)
		  INTO v_total
		  FROM T502_ITEM_ORDER 
		 WHERE c501_order_id = p_order_id
		   AND c502_void_fl is null;
		
		UPDATE T501_ORDER
		   SET c501_status_fl = '0' --Back Order
		     , c501_total_cost = nvl(v_total,0)
		     , c501_last_updated_by = p_user_id
		     , c501_last_updated_date = current_date
		 WHERE c501_order_id = p_order_id
		   AND c901_order_type = '2525' -- Back order
           AND c501_status_fl = '1'
		   AND c501_void_fl is null;
		
	END gm_upd_bo_status_fl;
	
	/*********************************************************************
 	* Description : Procedure used to update status fl in t504 as shipped
 	* Author      : Agilan Singaravel
	**********************************************************************/
	PROCEDURE gm_upd_cn_status(
		p_order_id		IN		t501_order.c501_order_id%type,
		p_consign_id	IN		t504_consignment.c504_consignment_id%type,
		p_status		IN		t504_consignment.c504_status_fl%type,
		p_user_id		IN		t504_consignment.c504_created_by%type
	)
	AS
	v_dis_id		t501_order.c501_distributor_id%type;
	BEGIN
	  BEGIN
		SELECT c501_distributor_id
		  INTO v_dis_id
		  FROM T501_ORDER
		 WHERE c501_order_id = p_order_id
		   AND c501_void_fl is null;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      		v_dis_id := null;
      END;
		   
		-- Update the CN status shipped
		UPDATE T504_CONSIGNMENT
		   SET c504_status_fl = p_status,
		       c504_ship_date = current_date,
		       c701_distributor_id = v_dis_id,
			   c504_last_updated_by = p_user_id,
			   c504_last_updated_date = CURRENT_DATE
		 WHERE c504_consignment_id = p_consign_id
		   AND c504_void_fl IS NULL;
		
		UPDATE T907_SHIPPING_INFO
		   SET c907_status_fl = 40,
			   c907_last_updated_by = p_user_id,
			   c907_last_updated_date = CURRENT_DATE
		 WHERE c907_ref_id = p_consign_id
		   AND c907_void_fl IS NULL;
		   
		UPDATE t520_request
		   SET c520_status_fl = 40 --Completed
		     , C520_LAST_UPDATED_BY = p_user_id
		     , C520_LAST_UPDATED_DATE = current_date
		 WHERE c520_request_id IN (SELECT c520_request_id FROM T504_CONSIGNMENT 
		 						    WHERE c504_consignment_id = p_consign_id
		   							   AND c504_void_fl IS NULL)
	       AND c520_void_fl is null;
		   
	END gm_upd_cn_status;
	
	/*********************************************************************
 	* Description : Procedure used to update status fl in t5010 as sold
 	* Author      : Agilan Singaravel
	**********************************************************************/
	PROCEDURE gm_upd_tag_status_by_cn(
		p_order_id		IN		t501_order.c501_order_id%type,
		p_consign_id	IN		t504_consignment.c504_consignment_id%type,
		p_quote_id		IN		t5010_tag.c5506_robot_quotation_id%type,
		p_status		IN		t5010_tag.c901_status%type,
		p_user_id		IN		t504_consignment.c504_created_by%type
	)
	AS
	v_dis_id		t501_order.c501_distributor_id%type;
	
	BEGIN
	  BEGIN
		SELECT c501_distributor_id
		  INTO v_dis_id
		  FROM T501_ORDER
		 WHERE c501_order_id = p_order_id
		   AND c501_void_fl is null;
      EXCEPTION WHEN NO_DATA_FOUND THEN
      		v_dis_id := null;
      END;
		-- Update the tag status as Sold
		UPDATE T5010_TAG
		   SET c901_status = p_status  
		     , c901_location_type = 4120 --Distributor
		     , c5010_location_id = v_dis_id
		     , c5010_sub_location_comments = 'Sold on ' || p_order_id
		     , c5506_robot_quotation_id = p_quote_id
		     , c5010_last_updated_by = p_user_id
			 , c5010_last_updated_date = CURRENT_DATE
		 WHERE c5010_last_updated_trans_id = p_consign_id
		   AND c5010_void_fl IS NULL;
		   
	END gm_upd_tag_status_by_cn;
	
	/******************************************************
 	* Description : Procedure used to verify completed set
 	* Author      : Agilan Singaravel
	*******************************************************/
	PROCEDURE gm_upd_save_set_build(
		p_consign_id	IN		t504_consignment.c504_consignment_id%type,
		p_user_id		IN		t504_consignment.c504_created_by%type
	)
	AS
		v_input_string		CLOB;
		v_err_msg			VARCHAr2(4000);
	BEGIN
	   
		    SELECT RTRIM (XMLAGG (XMLELEMENT (e, inputdtls || '|')) .EXTRACT ('//text()'), '|')
		 	  INTO v_input_string
			  FROM (
			SELECT t505.c205_part_number_id ||'^' || SUM (t505.c505_item_qty) || '^'|| t505.c505_control_number ||
				   '^4110' inputdtls
			  FROM T505_ITEM_CONSIGNMENT t505
			 WHERE  t505.c504_consignment_id = p_consign_id
			   AND t505.c505_void_fl IS NULL
		  GROUP BY t505.c205_part_number_id, t505.c505_control_number);
		  
		  gm_save_set_build(p_consign_id,v_input_string,null,'1.20','1',p_user_id,v_err_msg);
		  
		  --to void t5050_invpick_assign_detail  table
		  UPDATE T5050_INVPICK_ASSIGN_DETAIL
		     SET c5050_void_fl = 'Y'
		       , c5050_last_updated_by = p_user_id
		       , c5050_last_updated_date = CURRENT_DATE
		   WHERE c5050_ref_id = p_consign_id
		     AND c5050_void_fl is null;
		  
	END gm_upd_save_set_build;
		
END gm_pkg_sm_robotics_order_txn;
/