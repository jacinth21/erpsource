CREATE OR REPLACE PACKAGE BODY gm_pkg_sm_robotics_copy_quote
 IS
 
  /**********************************************************************
 	* Description : Procedure used to copy the quote
 	* Author      : saravanan M
	************************************************************************/
	PROCEDURE gm_sav_copy_quote_dtls(
		p_current_quote_id	IN		T5506_ROBOT_QUOTATION.C5506_ROBOT_QUOTATION_ID%TYPE,
		p_user_id			IN		T5506_ROBOT_QUOTATION.C5506_LAST_UPDATED_BY%TYPE,
		p_new_quote_id		OUT		T5506_ROBOT_QUOTATION.C5506_ROBOT_QUOTATION_ID%TYPE
	)AS

	v_quote          t5506_robot_quotation.c5506_robot_quotation_id%TYPE;
	BEGIN
	--To Generate new Quote Id
	gm_pkg_sm_robotics_quote_trans.gm_generate_quote_id(v_quote);
	--To copy header details
	gm_pkg_sm_robotics_copy_quote.gm_sav_copy_quote_header_dtls(p_current_quote_id,p_user_id,v_quote);
	--To copy category details
	gm_pkg_sm_robotics_copy_quote.gm_sav_copy_quote_category_dtls(p_current_quote_id,p_user_id,v_quote);
	--To copy set details
	gm_pkg_sm_robotics_copy_quote.gm_sav_copy_quote_set_dtls(p_current_quote_id,p_user_id,v_quote);
	--To copy discount details
	gm_pkg_sm_robotics_copy_quote.gm_sav_copy_quote_discount_dtls(p_current_quote_id,p_user_id,v_quote);
	
	p_new_quote_id:= v_quote;
	
END gm_sav_copy_quote_dtls;

	 /**********************************************************************
 	* Description : Procedure used to copy the quote header details
 	* Author      : saravanan M
	************************************************************************/
PROCEDURE gm_sav_copy_quote_header_dtls(
		p_current_quote_id	IN		T5506_ROBOT_QUOTATION.C5506_ROBOT_QUOTATION_ID%TYPE,
		p_user_id			IN		T5506_ROBOT_QUOTATION.C5506_LAST_UPDATED_BY%TYPE,
		p_new_quote_id		IN		T5506_ROBOT_QUOTATION.C5506_ROBOT_QUOTATION_ID%TYPE
)
AS
 		v_expiry_dt         t5506_robot_quotation.c5506_expiration_date%TYPE;
        v_exp_days          t5501_robot_company_mapping.c901_quote_expiration_days%TYPE;
        v_robot_name        t5500_robot_master.c5500_robot_name%TYPE;

BEGIN
BEGIN
For i in (select * from t5506_robot_quotation where C5506_ROBOT_QUOTATION_ID=p_current_quote_id AND C5506_VOID_FL is null) 
LOOP
	--To get Expiry days
	SELECT
            c901_quote_expiration_days
        INTO v_exp_days
        FROM
            t5501_robot_company_mapping
        WHERE
            c1900_company_id = i.c1900_company_id
            AND c5500_robot_master_id = i.c5500_robot_master_id
            AND c5501_void_fl IS NULL;

        BEGIN
            IF v_exp_days = '110830' THEN --US
                SELECT
                    last_day(trunc(add_months(current_date, 0), 'Q') + 85)
                INTO v_expiry_dt
                FROM
                    dual;

            ELSIF v_exp_days = '110831' THEN -- other countries
                SELECT
                    current_date + 90
                INTO v_expiry_dt
                FROM
                    dual;

            END IF;

        END;
        INSERT INTO t5506_robot_quotation (
            c5506_robot_quotation_id,
            c5506_robot_quotation_name,
            c5500_robot_master_id,
            c1900_company_id,
            c5506_quote_start_date,
            c5506_expiration_date,
            c901_quote_status,
            c5506_capital_ad_id,
            c5506_capital_vp_id,
            c701_capital_distributor_id,
            c703_capital_sales_rep_id,
            c5506_tax_percentage,
            c5506_tax_amount,
            c5506_last_updated_by,
            c5506_last_updated_date,
            c5506_created_by,
            c5506_created_date
        ) VALUES (
            p_new_quote_id,
            i.c5506_robot_quotation_name,
            i.c5500_robot_master_id,
            i.c1900_company_id,
            current_date,
            v_expiry_dt,
            110813,
            i.c5506_capital_ad_id,
            i.c5506_capital_vp_id,
            i.c701_capital_distributor_id,
            i.c703_capital_sales_rep_id,
            i.c5506_tax_percentage,
            i.c5506_tax_amount,
            p_user_id,
            current_date,
            p_user_id,
            current_date
        ); -- 110813(initiated)
	
END LOOP;
END;
END gm_sav_copy_quote_header_dtls;

	 /**********************************************************************
 	* Description : Procedure used to copy the quote category details
 	* Author      : saravanan M
	************************************************************************/
PROCEDURE gm_sav_copy_quote_category_dtls(
		p_current_quote_id	IN		T5506_ROBOT_QUOTATION.C5506_ROBOT_QUOTATION_ID%TYPE,
		p_user_id			IN		T5506_ROBOT_QUOTATION.C5506_LAST_UPDATED_BY%TYPE,
		p_new_quote_id		IN		T5506_ROBOT_QUOTATION.C5506_ROBOT_QUOTATION_ID%TYPE
)
AS
BEGIN

BEGIN
For i in (select * from t5507_robot_quotation_category_details where C5506_ROBOT_QUOTATION_ID = p_current_quote_id AND C5507_VOID_FL IS NULL) 
LOOP
         INSERT INTO t5507_robot_quotation_category_details (
            c5506_robot_quotation_id,
            c5502_robot_quotation_category_master_id,
            C207_SET_ID,
            c5507_ref_id,
            c5507_ref_name,
            c901_ref_type,
            c5507_suggestion,
            c5507_default_qty,
            c5507_qty,
            c901_approval_category_section,
            c901_discount_approval,
            c5507_list_price,
            c5507_corporate_list_price,
            c5507_last_updated_by,
            c5507_last_updated_date
        ) VALUES (
            p_new_quote_id,
            i.c5502_robot_quotation_category_master_id,
            i.C207_SET_ID,
            i.c5507_ref_id,
            i.c5507_ref_name,
            i.c901_ref_type,
            i.c5507_suggestion,
            i.c5507_default_qty,
            i.c5507_qty,
            i.c901_approval_category_section,
            i.c901_discount_approval,
            i.c5507_list_price,
            i.c5507_corporate_list_price,
            p_user_id,
            current_date
        );
END LOOP;
END;

END gm_sav_copy_quote_category_dtls;

	 /**********************************************************************
 	* Description : Procedure used to copy the quote set details
 	* Author      : saravanan M
	************************************************************************/
PROCEDURE gm_sav_copy_quote_set_dtls(
		p_current_quote_id		IN		T5506_ROBOT_QUOTATION.C5506_ROBOT_QUOTATION_ID%TYPE,
		p_user_id				IN		T5506_ROBOT_QUOTATION.C5506_LAST_UPDATED_BY%TYPE,
		p_new_quote_id			IN		T5506_ROBOT_QUOTATION.C5506_ROBOT_QUOTATION_ID%TYPE
)
AS
		v_company_id 	t5506_robot_quotation.c1900_company_id%TYPE;
BEGIN
        SELECT NVL(get_compid_frm_cntx(),GET_RULE_VALUE('DEFAULT_COMPANY','ONEPORTAL')) into v_company_id from dual;

BEGIN
For i in (select * from t5508_robot_quotation_set_price where C5506_ROBOT_QUOTATION_ID=p_current_quote_id AND C5508_VOID_FL IS NULL) 
LOOP
          INSERT INTO t5508_robot_quotation_set_price (
            c5506_robot_quotation_id,
            c207_set_id,
            c901_txn_currency,
            c5508_set_price,
            c5508_corporate_set_price,
            c1900_company_id,
            c5508_last_updated_by,
            c5508_last_updated_date
        ) VALUES (
            p_new_quote_id,
            i.c207_set_id,
            i.c901_txn_currency,
            i.c5508_set_price,
            i.c5508_corporate_set_price,
            v_company_id,
            p_user_id,
            current_date
        );
         
END LOOP;
END;

BEGIN
For i in (select * from t5509_robot_quotation_set_detail_price where C5506_ROBOT_QUOTATION_ID=p_current_quote_id AND C5509_VOID_FL IS NULL) 
LOOP
     INSERT INTO t5509_robot_quotation_set_detail_price (
            c5506_robot_quotation_id,
            c207_set_id,
            c205_part_number_id,
            c901_txn_currency,
            c5509_list_price,
            c5509_corporate_price,
            c5509_default_qty,
            c5509_req_qty,
            c1900_company_id,
            c5509_last_updated_by,
            c5509_last_updated_date
        ) VALUES (
            p_new_quote_id,
            i.c207_set_id,
            i.c205_part_number_id,
            i.c901_txn_currency,
            i.c5509_list_price,
            i.c5509_corporate_price,
            nvl(i.c5509_default_qty,0),
            nvl(i.c5509_req_qty,0),
            v_company_id,
            p_user_id,
            current_date
        );
END LOOP;
END;

END gm_sav_copy_quote_set_dtls;

	 /**********************************************************************
 	* Description : Procedure used to copy the quote discount details
 	* Author      : saravanan M
	************************************************************************/
PROCEDURE gm_sav_copy_quote_discount_dtls(
		p_current_quote_id	IN		T5506_ROBOT_QUOTATION.C5506_ROBOT_QUOTATION_ID%TYPE,
		p_user_id			IN		T5506_ROBOT_QUOTATION.C5506_LAST_UPDATED_BY%TYPE,
		p_new_quote_id		IN		T5506_ROBOT_QUOTATION.C5506_ROBOT_QUOTATION_ID%TYPE
)
AS
	v_robot_id   	    t5500_robot_master.c5500_robot_master_id%TYPE;
	v_company_id 	t5506_robot_quotation.c1900_company_id%TYPE;
BEGIN
        SELECT NVL(get_compid_frm_cntx(),GET_RULE_VALUE('DEFAULT_COMPANY','ONEPORTAL')) into v_company_id from dual;
BEGIN
For i in (select * from t5510_robot_quotation_discount_approval_dtls where C5506_ROBOT_QUOTATION_ID=p_current_quote_id AND C5510_VOID_FL IS NULL) 
LOOP  
	INSERT INTO t5510_robot_quotation_discount_approval_dtls (
	            c5506_robot_quotation_id,
	            c901_approval_category_section,
	            c5510_approval_category_section_name,
	            c5510_category_sub_total,
	            c5510_category_corporate_sub_total,
	            c901_discount_approval,
	            c5510_discount_percentage,
	            c5510_discounted_sub_total, 
	            c5510_discounted_corportate_sub_total,
	            c5510_last_updated_by,
	            c5510_last_updated_date
	        ) VALUES (
	            p_new_quote_id,
	            i.c901_approval_category_section,
	            i.c5510_approval_category_section_name,
	            i.c5510_category_sub_total,
	            i.c5510_category_corporate_sub_total,
	            i.c901_discount_approval,
	            i.c5510_discount_percentage,
	            i.c5510_discounted_sub_total, 
	            i.c5510_discounted_corportate_sub_total,
	            p_user_id,
	            current_date
	        );
         
END LOOP;

BEGIN
		SELECT c5500_robot_master_id
		INTO v_robot_id
		FROM t5506_robot_quotation
		WHERE c5506_robot_quotation_id = p_new_quote_id
		      AND c5506_void_fl IS NULL;
      EXCEPTION
			WHEN no_data_found THEN v_robot_id := '';
		END;
		
	--To update the lprice
	gm_inr_quote_lprice_upd(p_new_quote_id,v_company_id,p_user_id);
	--to update overall quote total price
	gm_pkg_sm_robotics_quote_trans.gm_upd_robotic_quote_total(p_new_quote_id,v_robot_id,p_user_id);
	

END;
END gm_sav_copy_quote_discount_dtls;



/**********************************************************************
	 	* Description : Procedure used to update latest price to the line item
	 	* Author      : saravanan M
	************************************************************************/
    PROCEDURE gm_inr_quote_lprice_upd (
	p_quote_id     IN   t5506_robot_quotation.c5506_robot_quotation_id%TYPE,
	p_company_id   IN   t5506_robot_quotation.c1900_company_id%TYPE,
	p_user_id      IN   t5506_robot_quotation.c5506_last_updated_by%TYPE
) AS
	v_robot_id 		t5500_robot_master.c5500_robot_master_id%TYPE;
	v_list_price   	t5509_robot_quotation_set_detail_price.c5509_list_price%TYPE;
	v_corp_price   	t5509_robot_quotation_set_detail_price.c5509_corporate_price%TYPE;
		
    CURSOR robot_category_detail_cur IS
    SELECT c5507_robot_quotation_category_details_id 	category_id,
       c5506_robot_quotation_id							p_new_quote_id,
       c5502_robot_quotation_category_master_id			category_master_id,
       c5507_ref_id										ref_id,
       c5507_default_qty								default_qty,
       c5507_qty										req_qty,
       c901_ref_type									ref_type
	FROM t5507_robot_quotation_category_details
	WHERE c5506_robot_quotation_id = p_quote_id
      AND c5507_void_fl IS NULL;
      
	CURSOR discount_cur IS
	SELECT c901_code_id   disc_id,
	       c901_code_nm   disc_nm
	FROM t901_code_lookup
	WHERE c901_code_grp = 'ROQTSC'    --code grp to fetch category section id and name
	      AND c901_active_fl = '1'
	      AND c901_void_fl IS NULL
	ORDER BY c901_code_seq_no;
		
	CURSOR discount_approval_dtls_cur IS
	SELECT c5510_robot_quotation_discount_approval_dtls_id   disc_dtl_id,
		   c5510_discount_percentage                         disc_per,
		   c5510_approval_per                                approval_per
	FROM t5510_robot_quotation_discount_approval_dtls
	WHERE c5506_robot_quotation_id = p_quote_id
	 AND c5510_void_fl IS NULL;
    
		  
    BEGIN
    --loop category detail and updating the set/part price in t5507,t5508,t5509
    FOR category_dtls IN robot_category_detail_cur LOOP
    	SELECT c5500_robot_master_id into v_robot_id
		FROM t5502_robot_quotation_category_master
		WHERE c5502_robot_quotation_category_master_id = category_dtls.category_master_id
		AND C5502_VOID_FL IS NULL;
		
	gm_upd_robotics_quote_price_dtls(p_quote_id, category_dtls.ref_id, category_dtls.ref_type, category_dtls.category_master_id
            , p_company_id,p_user_id);
            
	END LOOP;
	
		--To save discount details in discount approval dtls table 
        --from t5507_robot_quotation_category_details by category_section  wise
       FOR disc_cur IN discount_cur LOOP
			BEGIN
				SELECT SUM (c5507_qty * c5507_list_price),
				       SUM (c5507_qty * c5507_corporate_list_price)
				INTO
					v_list_price,
					v_corp_price
				FROM t5507_robot_quotation_category_details t5507
				WHERE t5507.c901_approval_category_section = disc_cur.disc_id
				      AND t5507.c5507_void_fl IS NULL
				      AND t5507.c5506_robot_quotation_id = p_quote_id;
			EXCEPTION
				WHEN no_data_found THEN
					v_list_price := 0;
					v_corp_price := 0;
			END;
			
			--To save category section wise discount details
			UPDATE t5510_robot_quotation_discount_approval_dtls
			SET c5510_category_sub_total = v_list_price, 
			    c5510_category_corporate_sub_total = v_corp_price
			WHERE c5506_robot_quotation_id = p_quote_id
			      AND c901_approval_category_section = disc_cur.disc_id
			      AND c5510_void_fl IS NULL;
		END LOOP;
		
        FOR disc_appr_dtls IN discount_approval_dtls_cur LOOP
        
        IF disc_appr_dtls.disc_per IS NOT NULL THEN
        gm_pkg_sm_robotics_quote_discount.gm_sav_discount_request_percentage(p_quote_id, disc_appr_dtls.disc_dtl_id, disc_appr_dtls.disc_per,p_user_id);
        END IF;
        
        IF disc_appr_dtls.approval_per IS NOT NULL THEN
        gm_pkg_sm_robotics_quote_discount.gm_sav_approval_percentage (p_quote_id, disc_appr_dtls.disc_dtl_id
                                     ,disc_appr_dtls.approval_per, p_user_id);
		END IF;
        
        END LOOP;
        
        --To update overall quote and corporate total
		gm_pkg_sm_robotics_quote_trans.gm_upd_robotic_quote_total (p_quote_id, v_robot_id, p_user_id);
		
	END gm_inr_quote_lprice_upd;
	
    /**********************************************************************
 	* Description : Procedure used to update price details in set price and
 	* set detail price table
 	* Author      : saravanan M
	************************************************************************/
    PROCEDURE gm_upd_robotics_quote_price_dtls (
		p_quote_id      IN   t5507_robot_quotation_category_details.c5506_robot_quotation_id%TYPE,
		p_ref_id        IN   t5507_robot_quotation_category_details.c5507_ref_id%TYPE,
		p_ref_type      IN   t5507_robot_quotation_category_details.c901_ref_type%TYPE,
		p_category_id   IN   t5507_robot_quotation_category_details.c5502_robot_quotation_category_master_id%TYPE,
		p_company_id    IN   t5506_robot_quotation.c1900_company_id%TYPE,
		p_user_id       IN   t5507_robot_quotation_category_details.c5507_last_updated_by%TYPE
	) AS
		v_list_price              t2052_part_price_mapping.c2052_list_price%TYPE;
		v_comptxn_curr            t1900_company.c901_txn_currency%TYPE;
		v_corporate_price         t5507_robot_quotation_category_details.c5507_corporate_list_price%TYPE;
		v_part_num_id             t208_set_details.c205_part_number_id%TYPE;
		v_total_list_price        t5509_robot_quotation_set_detail_price.c5509_list_price%TYPE;
		v_total_corporate_price   t5509_robot_quotation_set_detail_price.c5509_corporate_price%TYPE;
		v_cnt                     NUMBER;
		v_part_qty                t208_set_details.c208_set_qty%TYPE;
        --cursor to get set details based on ref id
		CURSOR v_setlist IS
		SELECT c205_part_number_id part_num_id
		FROM t208_set_details t208
		WHERE c207_set_id = p_ref_id
		      AND c208_void_fl IS NULL;
	BEGIN

		--if the ref id is a part then fetch the part price and save it in t5507
		IF p_ref_type = '111120' THEN --Part(111120)
        	--To calculate ListPrice and CorporatePrice
			gm_pkg_sm_robotics_quote_trans.gm_robotics_quote_list_price_calc (p_ref_id, p_company_id, v_list_price, v_comptxn_curr, v_corporate_price);
            
            --To update t5507_robot_quotation_category_details table
			gm_upd_robotics_quote_list_price (p_quote_id, p_ref_id, v_list_price
			, v_corporate_price, p_category_id,p_user_id);
		END IF;
		--if the ref id is a set, save it in t5505,t5508 and t5509
		IF p_ref_type = '111121' THEN --Set(111121)
			
				FOR v_set IN v_setlist LOOP
                --To calculate ListPrice and CorporatePrice
					gm_pkg_sm_robotics_quote_trans.gm_robotics_quote_list_price_calc(v_set.part_num_id, p_company_id, v_list_price, v_comptxn_curr
					, v_corporate_price);
			
                    --To fetch part quantity 
					SELECT c208_set_qty
					INTO v_part_qty
					FROM t208_set_details t208
					WHERE c207_set_id = p_ref_id
					      AND c205_part_number_id = v_set.part_num_id
					      AND c208_void_fl IS NULL;
    
                    --To save set details in t5509_robot_quotation_set_detail_price table
					gm_upd_robotics_quote_set_details(p_quote_id, p_ref_id, v_set.part_num_id
					, v_list_price,v_corporate_price, p_user_id);
				END LOOP;
		
			
			BEGIN
				SELECT SUM (c5509_default_qty * c5509_list_price),
				       SUM (c5509_default_qty * c5509_corporate_price)
				INTO
					v_total_list_price,
					v_total_corporate_price
				FROM t5509_robot_quotation_set_detail_price
				WHERE c207_set_id = p_ref_id
				      AND c5506_robot_quotation_id = p_quote_id
				      AND c5509_void_fl IS NULL;
			EXCEPTION
				WHEN no_data_found THEN
					v_total_list_price := 0;
					v_total_corporate_price := 0;
			END;
            
			--To save set price details in t5508_robot_quotation_set_price table
			gm_pkg_sm_robotics_quote_master.gm_upd_robotics_quot_set_price (p_quote_id, p_ref_id, v_total_list_price
			, v_total_corporate_price, p_user_id);
            
            --To update list price and corporate price in t5507_robot_quotation_category_details
			gm_upd_robotics_quote_list_price (p_quote_id, p_ref_id, v_total_list_price
			, v_total_corporate_price, p_category_id,p_user_id);
		END IF;
	END gm_upd_robotics_quote_price_dtls;
	
	/**********************************************************************
 	* Description : Procedure used to update price details in set detail price table
 	* Author      : saravanan M
	************************************************************************/
	PROCEDURE gm_upd_robotics_quote_set_details(
	 	p_quote_id       IN   t5506_robot_quotation.c5506_robot_quotation_id%TYPE,
        p_set_id         IN   t5509_robot_quotation_set_detail_price.c207_set_id%TYPE,
        p_part_num       IN   t5509_robot_quotation_set_detail_price.c205_part_number_id%TYPE,
        p_list_price     IN   t5509_robot_quotation_set_detail_price.c5509_list_price%TYPE,
        p_corp_price     IN   t5509_robot_quotation_set_detail_price.c5509_corporate_price%TYPE,
        p_user_id        IN   t5509_robot_quotation_set_detail_price.c5509_last_updated_by%TYPE
	)
	AS 
	BEGIN
		UPDATE t5509_robot_quotation_set_detail_price
		SET c5509_list_price = p_list_price,
			c5509_corporate_price = p_corp_price,
			c5509_last_updated_by = p_user_id,
			c5509_last_updated_date = current_date
		WHERE c5506_robot_quotation_id = p_quote_id
			AND c207_set_id = p_set_id
			AND c205_part_number_id = p_part_num
			AND c5509_void_fl IS NULL;
    END gm_upd_robotics_quote_set_details;
    
    /**********************************************************************
 	* Description : Procedure used to update price details into category details
 	* Author      : saravanan M
	************************************************************************/
    PROCEDURE gm_upd_robotics_quote_list_price (
        p_quote_id          IN   t5506_robot_quotation.c5506_robot_quotation_id%TYPE,
        p_ref_id            IN   t5507_robot_quotation_category_details.c5507_ref_id%TYPE,
        p_list_price        IN   t5507_robot_quotation_category_details.c5507_list_price%TYPE,
        p_corporate_price   IN   t5507_robot_quotation_category_details.c5507_corporate_list_price%TYPE,
        p_category_id       IN   t5507_robot_quotation_category_details.c5502_robot_quotation_category_master_id%TYPE,
        p_user_id           IN   t5507_robot_quotation_category_details.c5507_last_updated_by%TYPE
    ) AS
    BEGIN	
        UPDATE t5507_robot_quotation_category_details
		SET c5507_list_price = p_list_price,
		    c5507_corporate_list_price = p_corporate_price,
		    c5507_list_price_updated_by = p_user_id,
		    c5507_list_price_updated_date = current_date,
		    c5507_last_updated_by = p_user_id,
		    c5507_last_updated_date = current_date
		WHERE c5506_robot_quotation_id = p_quote_id
		      AND c5502_robot_quotation_category_master_id = p_category_id
		      AND c5507_ref_id = p_ref_id;
    END gm_upd_robotics_quote_list_price;

END gm_pkg_sm_robotics_copy_quote;
/