CREATE OR REPLACE PACKAGE BODY gm_pkg_sm_robotics_notification IS

	/**********************************************************************
 	* Description : Procedure used to fetch RFS parts Details quote in INR
 	* Author      : Jeeva B
	************************************************************************/
    PROCEDURE gm_fch_rfs_parts_dtls (
        p_quote_id   	IN   	t5506_robot_quotation.c5506_robot_quotation_id%TYPE,
        p_out_parts		OUT	 	TYPES.cursor_type
    ) AS
    	v_part_string   VARCHAR2(4000);
    	v_country_name  VARCHAR(100);
     BEGIN
		BEGIN
	     -- Below query to get the Quote RFS Part
		SELECT C5506_RFS_ERROR_DTLS 
		   INTO v_part_string 
		   FROM T5506_ROBOT_QUOTATION T5506
		 WHERE T5506.C5506_ROBOT_QUOTATION_ID = p_quote_id
		   AND T5506.C5506_VOID_FL IS NULL;
		EXCEPTION WHEN NO_DATA_FOUND THEN
			v_part_string := NULL;
    	END;		
		
		BEGIN
		-- Query used for get RFS country
		SELECT get_code_name(T704.c704_bill_country) 
		   INTO v_country_name 
		   FROM T704_ACCOUNT T704, T5506_ROBOT_QUOTATION T5506
		 WHERE T704.C704_ACCOUNT_ID = T5506.C704_CAPITAL_ACCOUNT_ID
		   AND T5506.C5506_ROBOT_QUOTATION_ID = p_quote_id
		   AND T704.C704_VOID_FL   IS NULL
		   AND T5506.C5506_VOID_FL IS NULL;
		EXCEPTION WHEN NO_DATA_FOUND THEN
			v_country_name := NULL;
    	END;
		 
    	my_context.set_my_inlist_ctx (v_part_string);

		 -- below query used for get RFS details to include with emails
		OPEN p_out_parts
		FOR
		SELECT 	T205.C205_PART_NUMBER_ID PARTNUMBERID,
			   	T205.C205_PART_NUM_DESC PARTNUMDESC,
			   	T207.c207_set_id,
		       	T207.C207_SET_NM SETNM,
		       	T202.C202_PROJECT_ID PROJECTID,
		       	T202.C202_PROJECT_NM PROJECTNM,
		       	v_country_name COUNTRYNAME
		   FROM T205_PART_NUMBER T205,
		        T202_PROJECT T202,
				T207_SET_MASTER T207,
				T208_set_details T208
		WHERE 	T205.C202_PROJECT_ID = T202.C202_PROJECT_ID
			AND T205.C205_PART_NUMBER_ID = T208.c205_part_number_id
			AND T207.c207_set_id = T208.c207_set_id
			AND T207.c901_set_grp_type = 1600
			AND T205.C205_PART_NUMBER_ID IN (
	             SELECT token FROM v_in_list)
			AND T207.C207_VOID_FL IS NULL
			AND T208.C208_VOID_FL IS NULL
			AND T202.C202_VOID_FL IS NULL
			AND T205.C205_ACTIVE_FL != 'N';
		
     END gm_fch_rfs_parts_dtls;
   
END gm_pkg_sm_robotics_notification;
/