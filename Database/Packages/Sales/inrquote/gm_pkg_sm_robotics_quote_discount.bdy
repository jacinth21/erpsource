CREATE OR REPLACE PACKAGE BODY gm_pkg_sm_robotics_quote_discount
IS

   /**********************************************************************
 	* Description : Procedure used to update discount category details
 	* Author      : saravanan M
	************************************************************************/
	PROCEDURE gm_upd_discount_category(
	p_quote_id					IN		T5506_ROBOT_QUOTATION.C5506_ROBOT_QUOTATION_ID%TYPE,
	p_approval_category_section	IN     	t5502_robot_quotation_category_master.c901_approval_category_section%TYPE,
	p_user_id					IN		T5506_ROBOT_QUOTATION.C5506_LAST_UPDATED_BY%TYPE
	
)
AS
	v_cat_sub_total			T5510_ROBOT_QUOTATION_DISCOUNT_APPROVAL_DTLS.C5510_CATEGORY_SUB_TOTAL%TYPE;
	v_corp_sub_total		T5510_ROBOT_QUOTATION_DISCOUNT_APPROVAL_DTLS.C5510_CATEGORY_CORPORATE_SUB_TOTAL%TYPE;
	v_disc_per				T5510_ROBOT_QUOTATION_DISCOUNT_APPROVAL_DTLS.C5510_DISCOUNT_PERCENTAGE%TYPE;
	v_approval_per			T5510_ROBOT_QUOTATION_DISCOUNT_APPROVAL_DTLS.C5510_APPROVAL_PER%TYPE;
	v_cat_sub_total_per		T5510_ROBOT_QUOTATION_DISCOUNT_APPROVAL_DTLS.C5510_CATEGORY_SUB_TOTAL%TYPE;
	v_corp_sub_total_per	T5510_ROBOT_QUOTATION_DISCOUNT_APPROVAL_DTLS.C5510_CATEGORY_CORPORATE_SUB_TOTAL%TYPE;
BEGIN
	BEGIN
	--To get and calc catsubtotal and corpsubtotal
    select sum(T5507.C5507_QTY * T5507.C5507_LIST_PRICE ) , sum(T5507.C5507_QTY * C5507_CORPORATE_LIST_PRICE) 
    INTO v_cat_sub_total,v_corp_sub_total 
    from T5507_ROBOT_QUOTATION_CATEGORY_DETAILS T5507, T5502_ROBOT_QUOTATION_CATEGORY_MASTER T5502, T5506_ROBOT_QUOTATION T5506 
    where T5502.C5502_ROBOT_QUOTATION_CATEGORY_MASTER_ID = T5507.C5502_ROBOT_QUOTATION_CATEGORY_MASTER_ID
    AND T5506.C5506_ROBOT_QUOTATION_ID = T5507.C5506_ROBOT_QUOTATION_ID
    AND T5507.C5506_ROBOT_QUOTATION_ID = p_quote_id
    --999.891 service part should not include in quote total (PC-5162)
    AND T5507.C5507_REF_ID NOT IN(SELECT C906_RULE_VALUE from T906_RULES WHERE C906_RULE_ID = 'SERVICE_PARTS' AND C906_RULE_GRP_ID = 'INR_SKIP_PRICE_TOTAL')
    AND T5502.C901_APPROVAL_CATEGORY_SECTION = p_approval_category_section
    AND T5502.C5502_VOID_FL IS NULL
    AND T5506.C5506_VOID_FL IS NULL
    AND T5507.C5507_VOID_FL IS NULL;
	EXCEPTION
            	WHEN NO_DATA_FOUND THEN 	
            		v_cat_sub_total:= 0;
            		v_corp_sub_total:= 0;
	END;
	
	BEGIN
 	select C5510_DISCOUNT_PERCENTAGE,C5510_APPROVAL_PER INTO v_disc_per,v_approval_per
 	from T5510_ROBOT_QUOTATION_DISCOUNT_APPROVAL_DTLS 
 	where C5506_ROBOT_QUOTATION_ID = p_quote_id AND C901_APPROVAL_CATEGORY_SECTION= p_approval_category_section
	AND C5510_VOID_FL IS NULL;
	EXCEPTION
            	WHEN NO_DATA_FOUND THEN 	
            		v_disc_per:= '';
            		v_approval_per:= '';
	END;
	
	BEGIN
 		v_cat_sub_total_per := (v_cat_sub_total * nvl(v_disc_per,v_approval_per)) / 100 ;

    EXCEPTION WHEN NO_DATA_FOUND
    THEN
	    v_cat_sub_total_per:= 0;
    END;
    
	BEGIN
 		v_corp_sub_total_per := (v_corp_sub_total * nvl(v_disc_per,v_approval_per)) / 100;

    EXCEPTION WHEN NO_DATA_FOUND
    THEN
	    v_corp_sub_total_per:= 0;
    END;
	
	
    UPDATE T5510_ROBOT_QUOTATION_DISCOUNT_APPROVAL_DTLS 
    SET C5510_CATEGORY_SUB_TOTAL = v_cat_sub_total , C5510_CATEGORY_CORPORATE_SUB_TOTAL = v_corp_sub_total, 
    C5510_DISCOUNTED_SUB_TOTAL = v_cat_sub_total_per,C5510_DISCOUNTED_CORPORTATE_SUB_TOTAL = v_corp_sub_total_per,
    C5510_LAST_UPDATED_BY = p_user_id , C5510_LAST_UPDATED_DATE = current_date
    where C5506_ROBOT_QUOTATION_ID = p_quote_id AND C901_APPROVAL_CATEGORY_SECTION = p_approval_category_section AND C5510_VOID_FL IS NULL;


END gm_upd_discount_category;
	
	/**********************************************************************
 	* Description : Procedure used to update discount request percentage in robotics quote tool
 	* Author      : tmuthusamy
	************************************************************************/	
	PROCEDURE gm_sav_discount_request_percentage(
	p_quote_id				IN		T5510_ROBOT_QUOTATION_DISCOUNT_APPROVAL_DTLS.C5506_ROBOT_QUOTATION_ID%TYPE,
	p_discount_dtl_id		IN		T5510_ROBOT_QUOTATION_DISCOUNT_APPROVAL_DTLS.C5510_ROBOT_QUOTATION_DISCOUNT_APPROVAL_DTLS_ID%TYPE,
	p_discount_per			IN		T5510_ROBOT_QUOTATION_DISCOUNT_APPROVAL_DTLS.C5510_DISCOUNT_PERCENTAGE%TYPE,
	p_user_id				IN		T5510_ROBOT_QUOTATION_DISCOUNT_APPROVAL_DTLS.C5510_LAST_UPDATED_BY%TYPE
)
AS
	v_approval_authority T5510_ROBOT_QUOTATION_DISCOUNT_APPROVAL_DTLS.C5510_APPROVAL_AUTHORITY%TYPE;
	v_categoryid		 T5510_ROBOT_QUOTATION_DISCOUNT_APPROVAL_DTLS.C901_APPROVAL_CATEGORY_SECTION%TYPE;
	v_category_total 	 T5510_ROBOT_QUOTATION_DISCOUNT_APPROVAL_DTLS.C5510_CATEGORY_SUB_TOTAL%TYPE;
	v_discounted_total 	 T5510_ROBOT_QUOTATION_DISCOUNT_APPROVAL_DTLS.C5510_DISCOUNTED_SUB_TOTAL%TYPE;
	v_company_id	 	 T5506_robot_quotation.C1900_COMPANY_ID%TYPE;
    v_comptxn_curr       t1900_company.c901_txn_currency%TYPE;
    v_rept_curr          t1900_company.c901_reporting_currency%TYPE;
    v_currency_value     t5512_robot_currency_conver.c5512_currency_value%TYPE;
    v_corporate_price    t5507_robot_quotation_category_details.c5507_corporate_list_price%TYPE;
    v_robot_id			 T5506_robot_quotation.C5500_ROBOT_MASTER_ID%TYPE;
    v_refid        		 t5507_robot_quotation_category_details.C5507_REF_ID%TYPE;
    v_setid   			 t5507_robot_quotation_category_details.C207_SET_ID%TYPE;
    v_qty   			 t5507_robot_quotation_category_details.C5507_QTY%TYPE;
    v_list_price   		 t5507_robot_quotation_category_details.C5507_LIST_PRICE%TYPE;
	v_category_master_id t5507_robot_quotation_category_details.C5502_ROBOT_QUOTATION_CATEGORY_MASTER_ID%TYPE;
	v_approval_section   t5507_robot_quotation_category_details.C901_APPROVAL_CATEGORY_SECTION%TYPE;
    CURSOR robot_category_dtls_cur IS
    SELECT
    	C5507_ROBOT_QUOTATION_CATEGORY_DETAILS_ID  category_dtl_id
    FROM t5507_robot_quotation_category_details
    WHERE C5506_ROBOT_QUOTATION_ID = p_quote_id
    AND C901_APPROVAL_CATEGORY_SECTION = '110805'
    AND C5507_VOID_FL IS NULL;
BEGIN
	BEGIN
	SELECT
    t5505.c901_approval_authority,
    t5505.C901_APPROVAL_CATEGORY_SECTION,
    t5510.c5510_category_sub_total,
    t5506.C1900_COMPANY_ID,
    t5506.C5500_ROBOT_MASTER_ID
    INTO 
	v_approval_authority,v_categoryid,v_category_total,v_company_id,v_robot_id
	FROM
    t5505_robot_quotation_approval_config          t5505,
    t5510_robot_quotation_discount_approval_dtls   t5510,
    t5506_robot_quotation                          t5506
	WHERE
    t5505.C901_APPROVAL_CATEGORY_SECTION = t5510.C901_APPROVAL_CATEGORY_SECTION
    AND t5506.c5506_robot_quotation_id = t5510.C5506_ROBOT_QUOTATION_ID
    AND t5506.C5500_ROBOT_MASTER_ID = t5505.C5500_ROBOT_MASTER_ID
    AND t5505.C1900_COMPANY_ID = t5506.C1900_COMPANY_ID
    AND t5510.c5510_robot_quotation_discount_approval_dtls_id = p_discount_dtl_id
    AND p_discount_per BETWEEN c5505_min_discount AND c5505_max_discount
    AND t5505.C5505_VOID_FL IS NULL
    AND t5510.C5510_VOID_FL IS NULL
    AND t5506.C5506_VOID_FL IS NULL;
	
	EXCEPTION WHEN NO_DATA_FOUND
    THEN
	    v_approval_authority:='';
	    v_categoryid:= '';
	    v_category_total:= '';
	    v_robot_id:= '';
    END;
    
    BEGIN
	--Get company currency details from below query
    SELECT
        c901_txn_currency,
        c901_reporting_currency
    INTO
        v_comptxn_curr,
        v_rept_curr
    FROM
        t1900_company
    WHERE
        c1900_company_id = v_company_id
        AND c1900_void_fl IS NULL;

    EXCEPTION WHEN NO_DATA_FOUND
    THEN
        v_comptxn_curr := 0;
        v_rept_curr := 0;
    END;

    BEGIN
    --Get corporate price by below function
    v_currency_value  := gm_pkg_sm_robotics_quote_trans.get_robot_quote_currency_conversion(v_comptxn_curr, v_rept_curr);
    v_corporate_price := v_currency_value * v_category_total;
    EXCEPTION WHEN NO_DATA_FOUND
    THEN
        v_corporate_price := 0;
    END;
    
    BEGIN
    v_discounted_total:= (v_category_total * p_discount_per) / 100 ;
    
    EXCEPTION WHEN NO_DATA_FOUND
    THEN
	    v_discounted_total:= 0;
    END;
    
	UPDATE t5510_robot_quotation_discount_approval_dtls
	SET
	    c5510_approval_authority = v_approval_authority,
	    c5510_discount_percentage = p_discount_per,
	    c5510_discounted_sub_total = v_discounted_total,
	    c5510_discounted_corportate_sub_total = v_corporate_price,
	    c5510_last_updated_by = p_user_id,
	    c5510_last_updated_date = current_date
	WHERE
	    C5506_ROBOT_QUOTATION_ID = p_quote_id
	    AND C901_APPROVAL_CATEGORY_SECTION = v_categoryid;
	
	--update discount percentage and discounted total for discount amount
	UPDATE T5507_ROBOT_QUOTATION_CATEGORY_DETAILS
	SET
	    C5507_DISCOUNT_PER = p_discount_per,
	    C5507_DISCOUNT_PRICE = v_discounted_total,
	    C5507_CORPORATE_DISCOUNT_PRICE = v_corporate_price,
	    C5507_LAST_UPDATED_BY = p_user_id,
	    C5507_LAST_UPDATED_DATE = current_date
	WHERE
	    C5506_ROBOT_QUOTATION_ID = p_quote_id
	    AND C901_APPROVAL_CATEGORY_SECTION = v_categoryid;
	    
	--Ala carte Auto discount percentage calculation 
	FOR category_dtls IN robot_category_dtls_cur LOOP
	BEGIN
	SELECT 
		C5507_REF_ID,
    	C207_SET_ID,
    	C5507_QTY,
		C5507_LIST_PRICE,
		C5502_ROBOT_QUOTATION_CATEGORY_MASTER_ID,
		C901_APPROVAL_CATEGORY_SECTION
	INTO 
		v_refid,
		v_setid,
		v_qty,
		v_list_price,
		v_category_master_id,
		v_approval_section
	FROM T5507_ROBOT_QUOTATION_CATEGORY_DETAILS
	WHERE 
	    C5506_ROBOT_QUOTATION_ID = p_quote_id
	    AND C5507_ROBOT_QUOTATION_CATEGORY_DETAILS_ID = category_dtls.category_dtl_id;
	    
	EXCEPTION
    WHEN no_data_found THEN
        v_refid := '';
        v_setid := '';
        v_qty := 0;
        v_category_master_id := '';
        v_approval_section := '';
    END;
    gm_pkg_sm_robotics_quote_trans.gm_update_alacarte_discount_dtls(v_company_id,p_quote_id,v_category_master_id,v_approval_section,v_refid,v_setid,v_qty,v_list_price,p_user_id);
    END LOOP;
    
    --update quote total
    IF v_robot_id is not null AND p_quote_id is not null 
    THEN
    	gm_pkg_sm_robotics_quote_trans.gm_upd_robotic_quote_total(p_quote_id,v_robot_id,p_user_id);
    END IF;
	
END gm_sav_discount_request_percentage;

	/**********************************************************************
 	* Description : Procedure used to update discount approval percentage in robotics quote tool
 	* Author      : tmuthusamy
	************************************************************************/
	PROCEDURE gm_sav_approval_percentage(
	p_quote_id				IN		T5510_ROBOT_QUOTATION_DISCOUNT_APPROVAL_DTLS.C5506_ROBOT_QUOTATION_ID%TYPE,
	p_discount_dtl_id		IN		T5510_ROBOT_QUOTATION_DISCOUNT_APPROVAL_DTLS.C5510_ROBOT_QUOTATION_DISCOUNT_APPROVAL_DTLS_ID%TYPE,
	p_approval_per			IN		T5510_ROBOT_QUOTATION_DISCOUNT_APPROVAL_DTLS.C5510_APPROVAL_PER%TYPE,
	p_user_id				IN		T5510_ROBOT_QUOTATION_DISCOUNT_APPROVAL_DTLS.C5510_LAST_UPDATED_BY%TYPE
)
AS
	v_approval_authority T5510_ROBOT_QUOTATION_DISCOUNT_APPROVAL_DTLS.C5510_APPROVAL_AUTHORITY%TYPE;
	v_categoryid		 T5510_ROBOT_QUOTATION_DISCOUNT_APPROVAL_DTLS.C901_APPROVAL_CATEGORY_SECTION%TYPE;
	v_category_total 	 T5510_ROBOT_QUOTATION_DISCOUNT_APPROVAL_DTLS.C5510_CATEGORY_SUB_TOTAL%TYPE;
	v_approved_total 	 T5510_ROBOT_QUOTATION_DISCOUNT_APPROVAL_DTLS.C5510_DISCOUNTED_SUB_TOTAL%TYPE;
	v_company_id	 	 T5506_robot_quotation.C1900_COMPANY_ID%TYPE;
    v_comptxn_curr      t1900_company.c901_txn_currency%TYPE;
    v_rept_curr         t1900_company.c901_reporting_currency%TYPE;
    v_currency_value    t5512_robot_currency_conver.c5512_currency_value%TYPE;
    v_corporate_price   t5507_robot_quotation_category_details.c5507_corporate_list_price%TYPE;
    v_robot_id			 T5506_robot_quotation.C5500_ROBOT_MASTER_ID%TYPE;
    v_refid        		 t5507_robot_quotation_category_details.C5507_REF_ID%TYPE;
    v_setid   			 t5507_robot_quotation_category_details.C207_SET_ID%TYPE;
    v_qty   			 t5507_robot_quotation_category_details.C5507_QTY%TYPE;
    v_list_price   		 t5507_robot_quotation_category_details.C5507_LIST_PRICE%TYPE;
	v_category_master_id t5507_robot_quotation_category_details.C5502_ROBOT_QUOTATION_CATEGORY_MASTER_ID%TYPE;
	v_approval_section   t5507_robot_quotation_category_details.C901_APPROVAL_CATEGORY_SECTION%TYPE;
    CURSOR robot_category_dtls_cur IS
    SELECT
    	C5507_ROBOT_QUOTATION_CATEGORY_DETAILS_ID  category_dtl_id
    FROM t5507_robot_quotation_category_details
    WHERE C5506_ROBOT_QUOTATION_ID = p_quote_id
    AND C901_APPROVAL_CATEGORY_SECTION = '110805'
    AND C5507_VOID_FL IS NULL;
BEGIN
	BEGIN
	
	SELECT
    t5505.c901_approval_authority,
    t5505.C901_APPROVAL_CATEGORY_SECTION,
    c5510_category_sub_total,
    t5506.C5500_ROBOT_MASTER_ID
    INTO 
	v_approval_authority,v_categoryid,v_category_total,v_robot_id
	FROM
    t5505_robot_quotation_approval_config          t5505,
    t5510_robot_quotation_discount_approval_dtls   t5510,
    t5506_robot_quotation                          t5506
	WHERE
    t5505.C901_APPROVAL_CATEGORY_SECTION = t5510.C901_APPROVAL_CATEGORY_SECTION
    AND t5506.c5506_robot_quotation_id = t5510.C5506_ROBOT_QUOTATION_ID
    AND t5506.C5500_ROBOT_MASTER_ID = t5505.C5500_ROBOT_MASTER_ID
    AND t5505.C1900_COMPANY_ID = t5506.C1900_COMPANY_ID
    AND t5510.c5510_robot_quotation_discount_approval_dtls_id = p_discount_dtl_id
    AND p_approval_per BETWEEN c5505_min_discount AND c5505_max_discount
    AND t5505.C5505_VOID_FL IS NULL
    AND t5510.C5510_VOID_FL IS NULL
    AND t5506.C5506_VOID_FL IS NULL;
	
	EXCEPTION WHEN NO_DATA_FOUND
    THEN
	    v_approval_authority:='';
	    v_categoryid:= '';
	    v_category_total:= '';
	    v_robot_id:= '';
    END;
    
    BEGIN
	--Get company currency details from below query
    SELECT
        c901_txn_currency,
        c901_reporting_currency
    INTO
        v_comptxn_curr,
        v_rept_curr
    FROM
        t1900_company
    WHERE
        c1900_company_id = v_company_id
        AND c1900_void_fl IS NULL;

    EXCEPTION WHEN NO_DATA_FOUND
    THEN
        v_comptxn_curr := 0;
        v_rept_curr := 0;
    END;

    BEGIN
    --Get corporate price by below function	
    v_currency_value  := gm_pkg_sm_robotics_quote_trans.get_robot_quote_currency_conversion(v_comptxn_curr, v_rept_curr);
    v_corporate_price := v_currency_value * v_category_total;
    EXCEPTION WHEN NO_DATA_FOUND
    THEN
        v_corporate_price := 0;
    END;
    
    BEGIN
    v_approved_total:= (v_category_total * p_approval_per) / 100 ;
    
    EXCEPTION WHEN NO_DATA_FOUND
    THEN
	    v_approved_total:= 0;
    END;
    
	UPDATE t5510_robot_quotation_discount_approval_dtls
	SET
	    c5510_approval_per = p_approval_per,
	    c5510_approval_sub_total = v_approved_total,
	    c5510_approval_corporate_sub_total = v_corporate_price,
	    c5510_last_updated_by = p_user_id,
	    c5510_last_updated_date = current_date
	WHERE
	    C5506_ROBOT_QUOTATION_ID = p_quote_id
	    AND C901_APPROVAL_CATEGORY_SECTION = v_categoryid;
	
	--update approved percentage and discounted total for approved amount    
    UPDATE T5507_ROBOT_QUOTATION_CATEGORY_DETAILS
	SET
	    C5507_DISCOUNT_PER = p_approval_per,
	    C5507_DISCOUNT_PRICE = v_approved_total,
	    C5507_CORPORATE_DISCOUNT_PRICE = v_corporate_price,
	    C5507_LAST_UPDATED_BY = p_user_id,
	    C5507_LAST_UPDATED_DATE = current_date
	WHERE
	    C5506_ROBOT_QUOTATION_ID = p_quote_id
	    AND C901_APPROVAL_CATEGORY_SECTION = v_categoryid;
	    
	--Ala carte Auto discount percentage calculation    
	FOR category_dtls IN robot_category_dtls_cur LOOP
	BEGIN
	SELECT 
		C5507_REF_ID,
    	C207_SET_ID,
    	C5507_QTY,
		C5507_LIST_PRICE,
		C5502_ROBOT_QUOTATION_CATEGORY_MASTER_ID,
		C901_APPROVAL_CATEGORY_SECTION
	INTO 
		v_refid,
		v_setid,
		v_qty,
		v_list_price,
		v_category_master_id,
		v_approval_section
	FROM T5507_ROBOT_QUOTATION_CATEGORY_DETAILS
	WHERE 
	    C5506_ROBOT_QUOTATION_ID = p_quote_id
	    AND C5507_ROBOT_QUOTATION_CATEGORY_DETAILS_ID = category_dtls.category_dtl_id;
	    
	EXCEPTION
    WHEN no_data_found THEN
        v_refid := '';
        v_setid := '';
        v_qty := 0;
        v_category_master_id := '';
        v_approval_section := '';
    END;
    gm_pkg_sm_robotics_quote_trans.gm_update_alacarte_discount_dtls(v_company_id,p_quote_id,v_category_master_id,v_approval_section,v_refid,v_setid,v_qty,v_list_price,p_user_id);
    END LOOP;

    --update quote total
    IF v_robot_id is not null AND p_quote_id is not null 
    THEN
    
    	gm_pkg_sm_robotics_quote_trans.gm_upd_robotic_quote_total(p_quote_id,v_robot_id,p_user_id);
    END IF;
	
END gm_sav_approval_percentage;

	/**********************************************************************
 	* Description : Procedure used to update discount approval status in robotics quote tool
 	* Author      : tmuthusamy
	************************************************************************/
	PROCEDURE gm_upd_discount_status(
		p_discount_dtl_id		IN		T5510_ROBOT_QUOTATION_DISCOUNT_APPROVAL_DTLS.C5510_ROBOT_QUOTATION_DISCOUNT_APPROVAL_DTLS_ID%TYPE,
		p_status				IN		T5510_ROBOT_QUOTATION_DISCOUNT_APPROVAL_DTLS.C901_CATEGORY_STATUS%TYPE,
		p_user_id				IN		T5510_ROBOT_QUOTATION_DISCOUNT_APPROVAL_DTLS.C5510_LAST_UPDATED_BY%TYPE,
		p_out_approval_count	OUT		T5510_ROBOT_QUOTATION_DISCOUNT_APPROVAL_DTLS.C901_CATEGORY_STATUS%TYPE
)
AS
v_quoteid 			 			 T5510_ROBOT_QUOTATION_DISCOUNT_APPROVAL_DTLS.C5506_ROBOT_QUOTATION_ID%TYPE;
v_status_cnt 			 		 T5510_ROBOT_QUOTATION_DISCOUNT_APPROVAL_DTLS.C901_CATEGORY_STATUS%TYPE;
v_status_cnt_progress 		 	 T5510_ROBOT_QUOTATION_DISCOUNT_APPROVAL_DTLS.C901_CATEGORY_STATUS%TYPE;
BEGIN
	UPDATE t5510_robot_quotation_discount_approval_dtls
	SET
	    c901_category_status = p_status,
	    C101_CATEGORY_CHANGED_BY = p_user_id,
	    C5510_CATEGORY_CHANGED_DATE = current_date,
	    c5510_last_updated_by = p_user_id,
	    c5510_last_updated_date = current_date
	WHERE
	    c5510_robot_quotation_discount_approval_dtls_id = p_discount_dtl_id;
	  
	SELECT 
 	C5506_ROBOT_QUOTATION_ID
	INTO v_quoteid
	FROM
	    t5510_robot_quotation_discount_approval_dtls
	WHERE
	    c5510_robot_quotation_discount_approval_dtls_id = p_discount_dtl_id;
	    
    SELECT 
 	COUNT(*)
 	INTO v_status_cnt
	FROM
	    t5510_robot_quotation_discount_approval_dtls
	WHERE
	    C5506_ROBOT_QUOTATION_ID = v_quoteid
	    AND (C901_CATEGORY_STATUS IS NULL
	    OR C901_CATEGORY_STATUS != 110793)
	    AND C901_APPROVAL_CATEGORY_SECTION != 110807;
	    
    SELECT 
 	COUNT(*)
 	INTO v_status_cnt_progress
	FROM
	    t5510_robot_quotation_discount_approval_dtls
	WHERE
	    C5506_ROBOT_QUOTATION_ID = v_quoteid
	    AND C901_CATEGORY_STATUS IN ('110793','110794');
	    
    IF (v_status_cnt_progress > 0 AND v_status_cnt != 0) THEN
   		gm_pkg_sm_robotics_quote_trans.gm_upd_quote_status(v_quoteid,'110816',p_user_id);
    END IF;
	    
    IF v_status_cnt = 0 THEN
   		gm_pkg_sm_robotics_quote_trans.gm_upd_quote_status(v_quoteid,'110817',p_user_id);
    END IF;
    
    p_out_approval_count := v_status_cnt;
END gm_upd_discount_status;	

/**********************************************************************
 	* Description : Procedure used to fetch overall approved status count in robotics quote tool
 	* Author      : tmuthusamy
	************************************************************************/
	PROCEDURE gm_upd_overall_approve_status(
		p_quote_id			IN		T5510_ROBOT_QUOTATION_DISCOUNT_APPROVAL_DTLS.C5506_ROBOT_QUOTATION_ID%TYPE,
		p_out_approval_cnt	OUT		T5510_ROBOT_QUOTATION_DISCOUNT_APPROVAL_DTLS.C901_CATEGORY_STATUS%TYPE
)
AS
BEGIN

 	SELECT 
 	COUNT(c901_category_status)
	INTO p_out_approval_cnt
	FROM
	    t5510_robot_quotation_discount_approval_dtls
	WHERE
	    C5506_ROBOT_QUOTATION_ID = p_quote_id
	    AND c901_category_status = '110794';
    
END gm_upd_overall_approve_status;
	
END gm_pkg_sm_robotics_quote_discount;
/