-- @"C:\Database\Packages\Sales\gm_pkg_sm_acct_trans.pkg";
CREATE OR REPLACE
PACKAGE gm_pkg_sm_acct_trans
IS
    /****************************************************************
    * Description : Procedure used to fetch the rep account infromation
    * Author      : mmuthusamy
    *****************************************************************/
PROCEDURE gm_fch_rep_account_info (
        p_rep_account_id t704_account.c704_account_id%TYPE,
        p_out OUT Types.cursor_type) ;
    /****************************************************************
    * Description : Procedure used to fetch the parent account infromation
    * Author      : mmuthusamy
    *****************************************************************/
PROCEDURE gm_fch_parent_account_info (
        p_party_id t704_account.c101_party_id%TYPE,
        p_out OUT Types.cursor_type) ;
    /****************************************************************
    * Description : Procedure used to loop the rep account infromation
    * Author      : mmuthusamy
    *****************************************************************/
PROCEDURE gm_extract_rep_ac_param (
        p_rep_acc_input_str IN VARCHAR2,
        p_rep_acc_id OUT t704_account.c704_account_id%TYPE,
        p_rep_acc_name OUT t704_account.c704_account_nm%TYPE,
        p_rep_acc_name_en OUT t704_account.c704_account_nm_en%TYPE,
        p_comp_id OUT t704_account.c901_company_id%TYPE,
        p_rep_id OUT t704_account.c703_sales_rep_id%TYPE,
        p_rep_short_nm OUT t704_account.c704_account_sh_name%TYPE,
        p_rep_acc_active_fl OUT t704_account.c704_active_fl%TYPE,
        p_inception_dt OUT t704_account.C704_LAST_UPDATED_DATE%TYPE,
        p_currency OUT t704_account.C901_CURRENCY%TYPE,
        p_dealer_id OUT t704_account.c101_dealer_id%TYPE);
    /****************************************************************
    * Description : Procedure used to loop the parent account infromation
    * Author      : mmuthusamy
    *****************************************************************/
PROCEDURE gm_extract_parent_ac_param (
        p_acc_input_str IN VARCHAR2,
        p_parent_party_id OUT t704_account.c101_party_id%TYPE,
        p_parent_party_name OUT t704_account.c704_account_nm%TYPE,
        p_account_type OUT t704_account.c901_account_type%TYPE,
        p_contact_per OUT t704_account.c704_contact_person%TYPE,
        p_phone OUT t704_account.C704_PHONE%TYPE,
        p_fax OUT t704_account.C704_FAX%TYPE,
        p_acc_info OUT t704_account.C704_COMMENTS%TYPE,
        p_bill_name OUT t704_account.c704_bill_name%TYPE,
        p_bill_add1 OUT t704_account.c704_bill_add1%TYPE,
        p_bill_add2 OUT t704_account.c704_bill_add2%TYPE,
        p_bill_city OUT t704_account.c704_bill_city%TYPE,
        p_bill_state OUT t704_account.c704_bill_state%TYPE,
        p_bill_country OUT t704_account.c704_bill_country%TYPE,
        p_bill_zipcode OUT t704_account.c704_bill_zip_code%TYPE,
        p_ship_name OUT t704_account.c704_bill_name%TYPE,
        p_ship_add1 OUT t704_account.c704_bill_add1%TYPE,
        p_ship_add2 OUT t704_account.c704_bill_add2%TYPE,
        p_ship_city OUT t704_account.c704_bill_city%TYPE,
        p_ship_state OUT t704_account.c704_bill_state%TYPE,
        p_ship_country OUT t704_account.c704_bill_country%TYPE,
        p_ship_zipcode OUT t704_account.c704_bill_zip_code%TYPE,
        p_payment_term OUT t704_account.c704_payment_terms%TYPE,
        p_credit_rat OUT t704_account.c704_credit_rating%TYPE,
        p_internal_rep_type OUT VARCHAR2);
    /****************************************************************
    * Description : Procedure used to save rep account infromation
    * Author      : mmuthusamy
    *****************************************************************/
PROCEDURE gm_sav_acct_info (
        p_rep_acc_id        IN t704_account.c704_account_id%TYPE,
        p_rep_acc_name      IN t704_account.c704_account_nm%TYPE,
        p_rep_acc_name_en      IN t704_account.c704_account_nm_en%TYPE,
        p_rep_id            IN t704_account.c703_sales_rep_id%TYPE,
        p_rep_short_nm      IN t704_account.c704_account_sh_name%TYPE,
        p_inception_dt      IN t704_account.C704_LAST_UPDATED_DATE%TYPE,
        p_rep_acc_active_fl IN t704_account.c704_active_fl%TYPE,
        p_comp_id           IN t704_account.c901_company_id%TYPE,
        p_parent_party_id   IN OUT t704_account.c101_party_id%TYPE,
        p_parent_party_name IN t704_account.c704_account_nm%TYPE,
        p_account_type      IN t704_account.c901_account_type%TYPE,
        p_contact_per       IN t704_account.c704_contact_person%TYPE,
        p_phone             IN t704_account.C704_PHONE%TYPE,
        p_fax               IN t704_account.C704_FAX%TYPE,
        p_acc_info          IN t704_account.C704_COMMENTS%TYPE,
        p_bill_name         IN t704_account.c704_bill_name%TYPE,
        p_bill_add1         IN t704_account.c704_bill_add1%TYPE,
        p_bill_add2         IN t704_account.c704_bill_add2%TYPE,
        p_bill_city         IN t704_account.c704_bill_city%TYPE,
        p_bill_state        IN t704_account.c704_bill_state%TYPE,
        p_bill_country      IN t704_account.c704_bill_country%TYPE,
        p_bill_zipcode      IN t704_account.c704_bill_zip_code%TYPE,
        p_ship_name         IN t704_account.c704_bill_name%TYPE,
        p_ship_add1         IN t704_account.c704_bill_add1%TYPE,
        p_ship_add2         IN t704_account.c704_bill_add2%TYPE,
        p_ship_city         IN t704_account.c704_bill_city%TYPE,
        p_ship_state        IN t704_account.c704_bill_state%TYPE,
        p_ship_country      IN t704_account.c704_bill_country%TYPE,
        p_ship_zipcode      IN t704_account.c704_bill_zip_code%TYPE,
        p_payment_term      IN t704_account.c704_payment_terms%TYPE,
        p_credit_rat        IN t704_account.c704_credit_rating%TYPE,
        p_portal_comp_id    IN t704_account.C1900_COMPANY_ID%TYPE,
        p_currency 			IN t704_account.C901_CURRENCY%TYPE,
        p_user_id           IN t704_account.c704_created_by%TYPE,
        p_dealer_id 		IN t704_account.c101_dealer_id%TYPE,
        p_internal_rep_type IN t704_account.c101_internal_rep%TYPE DEFAULT NULL) ;
/****************************************************************
    * Description : Procedure used to update rep account information
    * Author      : mmuthusamy
    *****************************************************************/
PROCEDURE gm_upd_rep_acct_info (
        p_rep_acc_id        IN t704_account.c704_account_id%TYPE,
        p_rep_acc_name      IN t704_account.c704_account_nm%TYPE,
        p_rep_acc_name_en   IN t704_account.c704_account_nm_en%TYPE,
        p_rep_id            IN t704_account.c703_sales_rep_id%TYPE,
        p_rep_short_nm      IN t704_account.c704_account_sh_name%TYPE,
        p_inception_dt      IN t704_account.C704_LAST_UPDATED_DATE%TYPE,
        p_rep_acc_active_fl IN t704_account.c704_active_fl%TYPE,
        p_comp_id           IN t704_account.c901_company_id%TYPE,
        p_currency 			IN t704_account.C901_CURRENCY%TYPE,
        p_user_id           IN t704_account.c704_created_by%TYPE,
        p_dealer_id 		IN t704_account.c101_dealer_id%TYPE) ;        
    /****************************************************************
    * Description : Procedure used to update account information
    * Author      : mmuthusamy
    *****************************************************************/
PROCEDURE gm_upd_acct_info (
        p_rep_acc_id        IN t704_account.c704_account_id%TYPE,
        p_parent_party_id   IN t704_account.c101_party_id%TYPE,
        p_parent_party_name IN t704_account.c704_account_nm%TYPE,
        p_account_type      IN t704_account.c901_account_type%TYPE,
        p_contact_per       IN t704_account.c704_contact_person%TYPE,
        p_phone             IN t704_account.C704_PHONE%TYPE,
        p_fax               IN t704_account.C704_FAX%TYPE,
        p_acc_info          IN t704_account.C704_COMMENTS%TYPE,
        p_bill_name         IN t704_account.c704_bill_name%TYPE,
        p_bill_add1         IN t704_account.c704_bill_add1%TYPE,
        p_bill_add2         IN t704_account.c704_bill_add2%TYPE,
        p_bill_city         IN t704_account.c704_bill_city%TYPE,
        p_bill_state        IN t704_account.c704_bill_state%TYPE,
        p_bill_country      IN t704_account.c704_bill_country%TYPE,
        p_bill_zipcode      IN t704_account.c704_bill_zip_code%TYPE,
        p_ship_name         IN t704_account.c704_bill_name%TYPE,
        p_ship_add1         IN t704_account.c704_bill_add1%TYPE,
        p_ship_add2         IN t704_account.c704_bill_add2%TYPE,
        p_ship_city         IN t704_account.c704_bill_city%TYPE,
        p_ship_state        IN t704_account.c704_bill_state%TYPE,
        p_ship_country      IN t704_account.c704_bill_country%TYPE,
        p_ship_zipcode      IN t704_account.c704_bill_zip_code%TYPE,
        p_payment_term      IN t704_account.c704_payment_terms%TYPE,
        p_credit_rat        IN t704_account.c704_credit_rating%TYPE,
        p_portal_comp_id    IN t704_account.C1900_COMPANY_ID%TYPE,
        p_user_id           IN t704_account.c704_created_by%TYPE) ;
    /****************************************************************
    * Description : Procedure used to save the account attribute values
    * Author      : mmuthusamy
    *****************************************************************/
PROCEDURE gm_sav_acct_attr (
        p_accid        IN t704_account.c704_account_id%TYPE,
        p_acc_att_val  IN T704A_ACCOUNT_ATTRIBUTE.c704a_attribute_value%TYPE,
        p_acc_att_type IN T704A_ACCOUNT_ATTRIBUTE.c901_attribute_type%TYPE,
        p_userid       IN t704a_account_attribute.c704a_created_by%TYPE) ;
    /****************************************************************
    * Description : Procedure used to sync account attribute based on parent account details
    * Author      : mmuthusamy
    *****************************************************************/
PROCEDURE gm_sav_sync_acct_attr (
        p_accid  IN t704_account.c704_account_id%TYPE,
        p_acc_att_type IN VARCHAR2, 
        p_userid IN t704a_account_attribute.c704a_created_by%TYPE) ;

/****************************************************************
    * Description : Procedure used to sync parent account attribute based on account
    * Author      : mmuthusamy
    *****************************************************************/
PROCEDURE gm_sav_sync_parent_acct_attr (
        p_accid  IN t704_account.c704_account_id%TYPE,
        p_acc_att_type IN VARCHAR2, 
        p_userid IN t704a_account_attribute.c704a_created_by%TYPE) ;  
 /****************************************************************
* Description : Procedure used to sync GPO accounts based on parent account details from Pricing parameter Tab
* Author      : Mihir
*****************************************************************/
PROCEDURE gm_sav_sync_gpo_acct(
        p_accid        	IN t704_account.c704_account_id%TYPE,
        p_gpo_id	 	IN VARCHAR2,
        p_userid       	IN t704a_account_attribute.c704a_created_by%TYPE);
        
/****************************************************************
* Description : Procedure used to sync GPO accounts based on parent account details from basic info screen.
* Author      : Mihir
*****************************************************************/
PROCEDURE gm_sav_sync_parent_gpo_acct(	
		p_accid        IN t704_account.c704_account_id%TYPE,
        p_userid       IN t704a_account_attribute.c704a_created_by%TYPE);
/****************************************************************
* Description : Procedure used to fetch the new account data send to mail
* Author      : shanmugapriya
*****************************************************************/
PROCEDURE gm_sav_fch_rep_account_info (
        p_rep_account_id  	IN    t704_account.c704_account_id%TYPE,
        p_out_cursor      	OUT   Types.cursor_type,
        p_parentrepinfo_cur OUT   Types.cursor_type);
        
/*************************************************************************************************************
* Description : Procedure to Check input in required fields when marking XML file in Invoice Parameter
* Author      : Shiny
**************************************************************************************************************/
PROCEDURE gm_chk_invoice_xml_param(
    p_inputstr   IN VARCHAR2,
    p_account_id IN t704_account.c704_account_id%TYPE,
    p_comp_id    IN t704_account.c1900_company_id%TYPE);
/*************************************************************
* Description : Updates the account table with rep id
* Author      : Shiny
**************************************************************/
PROCEDURE gm_update_internal_rep(
    p_rep_account_id  	IN   t704_account.c704_account_id%TYPE,
    p_internal_rep_id   IN   varchar2,
    p_userid            IN   t704a_account_attribute.c704a_created_by%TYPE);
END gm_pkg_sm_acct_trans;
/