--@"C:\Database\Packages\Sales\gm_pkg_sm_pricing_rules.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_sm_pricing_rules
IS

PROCEDURE gm_sav_pricing_rules (
    p_request_id in t7001_group_part_pricing.c7000_account_price_request_id%type    
)
AS

v_comparator NUMBER;
v_target_price NUMBER;
v_source_price NUMBER;
raise_rule BOOLEAN;
v_account_id t7000_account_price_request.c704_account_id%type;
v_gpo_id t7000_account_price_request.C101_GPO_ID%TYPE;


--fetching  the rules from PRICING_RULES table based on the construct or group of request
    CURSOR  cur_pricing_rules  is
	   SELECT C7050_PRICING_RULE_ID rule_id,
		  C7050_PRICING_RULE_NM rule_nm,
		  C7050_SRC_RULE_REF_ID src_ref_id,
		  C901_PRICING_RULE_TYPE src_ref_type,
		  C7050_SRC_CONST_LVL src_const_lvl
		FROM T7050_PRICING_RULES
		WHERE C7050_SRC_RULE_REF_ID IN
		  (SELECT my_temp_txn_id FROM my_temp_list)
		OR C7050_SRC_RULE_REF_ID IN
		  (SELECT c7001_ref_id
		  FROM t7001_group_part_pricing
		  WHERE c7000_account_price_request_id=p_request_id
		  --AND c901_change_type ='52021'
      ) ;
    
BEGIN

--fetching construct ids and set in my temp table based on the price request.
    gm_fch_prc_construct_id(p_request_id);
	
	BEGIN
		SELECT C101_GPO_ID,c704_account_id INTO v_gpo_id,v_account_id
		FROM t7000_account_price_request WHERE C7000_ACCOUNT_PRICE_REQUEST_ID = p_request_id;
	EXCEPTION WHEN NO_DATA_FOUND THEN
		v_gpo_id:='';
		v_account_id := '';
	END;
	

FOR cur_price_rules IN cur_pricing_rules
LOOP
raise_rule := false;
--
    	
   --fetching the source amount using Source Ref ID,Source Ref Type 
     gm_fch_source_price(cur_price_rules.src_ref_id,cur_price_rules.src_ref_type,p_request_id,v_source_price);
   
   --fetching the comparing product amount using Ref ID, Ref Type 
	gm_fch_target_price(cur_price_rules.rule_id,v_account_id,v_gpo_id,v_target_price,v_comparator);
   
     
     
    -- checking for comparator values if available then do the comparision with source amount
    if  v_comparator = 50601 then  --'>'
    	 
        if v_target_price > v_source_price then 
         
           raise_rule := true;
        
        end if;
    
    elsif  v_comparator <> null and v_comparator = 50603 then --'<'
    
        if v_target_price < v_source_price then 
         
           raise_rule := true;
        
        end if;
    
    end if;
    
    
    delete from T7006_PRICING_REQUEST_RULES where C7050_PRICING_RULE_ID = cur_price_rules.rule_id 
    and C7000_ACCOUNT_PRICE_REQUEST_ID = p_request_id and C7050_RULE_REF_ID = cur_price_rules.src_ref_id ;
    
    --based on the comparision result raising the rules 
    if raise_rule = true then
   
    --gm_procedure_log('raise_rule ',cur_price_rules.rule_id||' , '||cur_price_rules.src_ref_id||','||cur_price_rules.src_ref_type||'m,'||v_source_price||','||v_target_price);
    gm_raise_pricing_rule(cur_price_rules.rule_id,p_request_id,v_source_price,v_target_price,cur_price_rules.src_ref_id,cur_price_rules.src_ref_type);
         
    end if;
    
    
    
    end loop;
--
END gm_sav_pricing_rules;
    
/* This procedure will get the source price for the request
*/

PROCEDURE gm_fch_source_price (
	 p_ref_id     IN   T7050_PRICING_RULES.C7050_SRC_RULE_REF_ID%TYPE,
     p_ref_type   in   varchar2,
     p_request_id in   t7001_group_part_pricing.c7000_account_price_request_id%type,
     p_price      OUT  NUMBER
     )
AS

BEGIN
 
--    
    if p_ref_type = 1006412 then  -- CONSTRUCT
    
    select NVL(sum(t7004.c7004_qty*t7001.c7001_adjusted_price),0) into p_price 
    from 
    	t7001_group_part_pricing t7001,
    	t7004_construct_mapping t7004 
    where 
    	t7004.c7003_system_construct_id=p_ref_id and 
    	to_char(t7004.c4010_group_id)=t7001.c7001_ref_id   and 
    	--t7001.c901_change_type='52021' and  -- OFFLIST
    	t7001.C7001_ADJUSTED_PRICE >0 and
    	t7001.C901_REF_TYPE = 52000 and    	
    	t7001.c7000_account_price_request_id=p_request_id; 
   
    elsif  p_ref_type = 1006413 then -- GROUP
    
    select NVL(sum(t7001.c7001_adjusted_price),0) into p_price 
    from
    	t7001_group_part_pricing t7001 
    where   
    	t7001.c7001_ref_id = p_ref_id  and 
    	--t7001.c901_change_type='52021' and  -- OFFLIST
    	t7001.C7001_ADJUSTED_PRICE >0 and
    	t7001.C901_REF_TYPE = 52000 and -- GROUP
    	t7001.c7000_account_price_request_id=p_request_id;  
    
    end if;
    
  --gm_procedure_log('source _price',p_price);
--
END gm_fch_source_price;

PROCEDURE gm_fch_target_price (
	p_pricing_rule_id IN T7051_PRICING_RULE_DETAIL.C7050_PRICING_RULE_ID%TYPE,
	p_account_id in t7000_account_price_request.c704_account_id%type,
    p_gpo_id in t7000_account_price_request.C101_GPO_ID%TYPE,
    p_tgt_price OUT NUMBER,
    p_comparator OUT NUMBER
)
AS
v_temp_price NUMBER;
v_percentage NUMBER;
v_account_id t7000_account_price_request.c704_account_id%type;

	cursor cur_tgt_pricing_rules  is
    
		SELECT C901_PRICING_RULE_TYPE tgt_rule_type ,
		  C7051_TGT_RULE_REF_ID tgt_ref_id ,
		  C7051_TGT_CONST_LVL tgt_const_lvl,
		  C7051_TGT_PRECENT_VALUE tgt_percentage,
		  C901_COMPARATOR_ID tgt_comparator
		FROM T7051_PRICING_RULE_DETAIL
		WHERE C7050_PRICING_RULE_ID = p_pricing_rule_id;

BEGIN

SELECT DECODE(p_gpo_id,'',p_account_id,'') into v_account_id from dual;

 
 p_tgt_price := 0;
FOR cur_tgt_price_rules IN cur_tgt_pricing_rules
LOOP
--    
    if cur_tgt_price_rules.tgt_rule_type = 1006412 then -- CONSTRUCT
    
	    select NVL(sum(t7004.c7004_qty*t7001.c7001_adjusted_price),0) into v_temp_price
	    from t7001_group_part_pricing t7001,
	    	 t7004_construct_mapping t7004,
	    	 t7000_account_price_request t7000 
	    where t7004.c7003_system_construct_id=cur_tgt_price_rules.tgt_ref_id and  
	    	  to_char(t7004.c4010_group_id)=t7001.c7001_ref_id   and 
	    	  t7001.C901_REF_TYPE='52000' and
	    	  t7001.c7000_account_price_request_id=t7000.c7000_account_price_request_id   and 
	    	  (NVL(t7000.c704_account_id,'-999')=NVL(v_account_id,'-999') AND 
	    	  NVL(C101_GPO_ID,-9999) = NVL(p_gpo_id,-9999)) AND 
	    	  t7000.C901_REQUEST_STATUS NOT IN (52120,52190) ; -- INITIATED,Approved Implemented 
    
    	-- If proposed price is not available, then get the current price that is approved.
    	IF v_temp_price=0 THEN
	    	select NVL(sum(t7004.c7004_qty*t7051.c7051_price),0) into v_temp_price
		    from t7051_account_group_pricing t7051,
		    	 t7004_construct_mapping t7004,
		    	 t7000_account_price_request t7000 
		    where t7004.c7003_system_construct_id=cur_tgt_price_rules.tgt_ref_id and  
		    	  to_char(t7004.c4010_group_id)=t7051.c7051_ref_id   and 
		    	  t7051.C901_REF_TYPE='52000' and
		    	  t7051.C7000_LAST_UPDATED_TXN_ID=t7000.c7000_account_price_request_id   and 
		    	  (NVL(t7000.c704_account_id,'-999')=NVL(v_account_id,'-999') AND 
		    	  NVL(t7051.C101_GPO_ID,-9999) = NVL(p_gpo_id,-9999)) AND 
		    	  t7000.C901_REQUEST_STATUS  IN (52190) ; -- Approved Implemented 
    	 END IF;

    elsif  cur_tgt_price_rules.tgt_rule_type = 1006413 then -- GROUP
    	BEGIN
		    select nvl(t7001.c7001_adjusted_price,0) into v_temp_price 
		    from t7001_group_part_pricing t7001,
		    	 t7000_account_price_request t7000 
		    where t7001.c7001_ref_id = cur_tgt_price_rules.tgt_ref_id  and 
		    	  t7001.c901_change_type='52021' and 
		    	  t7001.c7000_account_price_request_id=t7000.c7000_account_price_request_id and 
		    	  (NVL(t7000.c704_account_id,'-999')=NVL(v_account_id,'-999') AND 
		    	  NVL(C101_GPO_ID,-9999) = NVL(p_gpo_id,-9999)) AND 
		    	  t7000.C901_REQUEST_STATUS NOT IN (52120,52190) ; -- INITIATED,Approved Implemented 
	
			-- If proposed price is not available, then get the current price that is approved.
	    	EXCEPTION WHEN NO_DATA_FOUND THEN
		    	BEGIN
			    	select nvl(t7051.c7051_price,0) into v_temp_price
				    from t7051_account_group_pricing t7051,
				    	 t7000_account_price_request t7000, 
				    	 t7001_group_part_pricing t7001
				    where t7001.c7001_ref_id=t7051.c7051_ref_id   and 
                  t7051.c7051_ref_id  =cur_tgt_price_rules.tgt_ref_id and
				    --	  t7051.C901_REF_TYPE='52001' and
				    	  t7051.C7000_LAST_UPDATED_TXN_ID=t7000.c7000_account_price_request_id   and
				    	  t7000.c7000_account_price_request_id = t7001.c7000_account_price_request_id AND
				    	  (NVL(t7000.c704_account_id,'-999')=NVL(v_account_id,'-999') AND 
				    	  NVL(t7051.C101_GPO_ID,-9999) = NVL(p_gpo_id,-9999)) AND
				    	  t7051.C7051_ACTIVE_FL = 'Y' AND		    	   
				    	  t7000.C901_REQUEST_STATUS  IN (52190) ; -- Approved Implemented
				   EXCEPTION WHEN NO_DATA_FOUND THEN
				   		v_temp_price := 0;
				   END;
			  END; 
			
		END IF;
    
  
	
    p_tgt_price := p_tgt_price +v_temp_price;
    v_temp_price :=0;
    
    if(cur_tgt_price_rules.tgt_comparator IS NOT NULL)
    THEN
	p_comparator := cur_tgt_price_rules.tgt_comparator;
    END IF;
    
    if (cur_tgt_price_rules.tgt_percentage IS NOT NULL)
    THEN
	v_percentage := cur_tgt_price_rules.tgt_percentage;
    END IF;
	
  end loop;
  
  --gm_procedure_log('everythibng','v_percentage '||v_percentage||' p_comparator'||p_comparator ||'p_tgt_price'||p_tgt_price);

   -- checking for percentage values if available do the calculation and save the amount to v_target_price
    if v_percentage IS NOT NULL then
    
       -- p_tgt_price := p_tgt_price-(p_tgt_price *( v_percentage/100));
        p_tgt_price := p_tgt_price *( v_percentage/100);
    
    end if;
	

--
END gm_fch_target_price;


PROCEDURE gm_fch_prc_construct_id (
	p_request_id in t7001_group_part_pricing.c7000_account_price_request_id%type
)
AS

BEGIN
 --fetching construct ids based on the price request

INSERT INTO my_temp_list
            (my_temp_txn_id
            )
(	select distinct t7004.c7003_system_construct_id
 	from 
 		t7003_system_construct t7003 ,
 		t7004_construct_mapping t7004,
 		t7001_group_part_pricing t7001 
 	where 
	    t7003.c7003_system_construct_id = t7004.c7003_system_construct_id and 
	    t7004.c4010_group_id = t7001.c7001_ref_id AND 
	    t7001.c901_REF_TYPE='52000' and
	    t7001.C7001_ADJUSTED_PRICE > 0 and 
	    t7001.c7000_account_price_request_id=p_request_id);

END gm_fch_prc_construct_id;

PROCEDURE gm_raise_pricing_rule (
    p_rule_id IN T7050_PRICING_RULES.C7050_PRICING_RULE_ID%TYPE,
	p_request_id in t7001_group_part_pricing.c7000_account_price_request_id%type,
    p_source_price IN number,
    p_target_price IN number,
    p_rule_ref_id IN number,    
    p_rule_ref_type IN NUMBER    
)
AS

BEGIN

	delete from T7006_PRICING_REQUEST_RULES 
		where C7050_PRICING_RULE_ID = p_rule_id and 
			  C7000_ACCOUNT_PRICE_REQUEST_ID = p_request_id	and 
			  C7050_RULE_REF_ID = p_rule_ref_id; 

  insert into T7006_PRICING_REQUEST_RULES(C7006_PRICING_REQUETS_RULE_ID,C7050_PRICING_RULE_ID,C7000_ACCOUNT_PRICE_REQUEST_ID,C7006_REQUESTED_PRICE,C7006_TARGET_PRICE
  ,C7050_rule_ref_id,C901_rule_ref_type) values 
  (S7006_PRICING_REQUEST_RULES.nextval,p_rule_id,p_request_id,p_source_price,p_target_price,p_rule_ref_id,p_rule_ref_type);
--
END gm_raise_pricing_rule;

END gm_pkg_sm_pricing_rules;

/
