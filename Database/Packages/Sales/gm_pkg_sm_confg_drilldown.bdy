--@"C:\database\Packages\sales\gm_pkg_sm_confg_drilldown.bdy";
CREATE OR REPLACE
PACKAGE BODY gm_pkg_sm_confg_drilldown
IS
/******************************************************************************************************
  * This method will fetch the Column Name Which will be displayed in the drill down for Sales Dash Board.
********************************************************************************************************/
	PROCEDURE gm_fch_drilldown_grp(
    	p_out_data OUT TYPES.cursor_type 
    	)
		AS
		BEGIN
  		OPEN p_out_data FOR
  			SELECT DISTINCT( C901_DRILLDOWN_GRP_ID) ddgrpid,C901_CODE_NM ddcolumnnm
  			FROM T9152_SD_DRILLDOWN,T901_CODE_LOOKUP
  			WHERE c901_code_id=C901_DRILLDOWN_GRP_ID
  			AND c901_active_fl=1
  			ORDER BY C901_DRILLDOWN_GRP_ID;
		END gm_fch_drilldown_grp;
   
/**********************************************************************************************************************
  * This method will fetch the Details Of The Column Name Which will be displayed in the drill down for Sales Dash Board.
**************************************************************************************************************************/ 
   	PROCEDURE gm_fch_drilldown_dtl(
   		p_ddgrpid IN   t9152_sd_drilldown.c901_drilldown_grp_id%TYPE,
   		p_out_data OUT TYPES.cursor_type
   		)
   		AS
   		BEGIN
		OPEN p_out_data FOR
			SELECT 
  				   t9152.C901_DRILLDOWN_GRP_ID ddgrpid,
  				   t9152.C9152_SD_COLUMN_NM ddcolumnnm,
  				   t9152.C9152_SEQ seq_num,
  				   t9152.C9152_SD_COLUMN_SHRT_NM ddcolumnnmshrtnm,
  				   t9152.C9152_VOID_FL voidfl,
  				   t901.C901_CODE_NM ddgrpnm
			FROM T9152_SD_DRILLDOWN t9152,T901_CODE_LOOKUP t901
			WHERE t901.c901_code_id         = t9152.C901_DRILLDOWN_GRP_ID
			AND t9152.C901_DRILLDOWN_GRP_ID = NVL(p_ddgrpid,C901_DRILLDOWN_GRP_ID)
			AND c901_active_fl              =1
			ORDER BY C901_DRILLDOWN_GRP_ID,NVL(C9152_SEQ,'7');
   	END gm_fch_drilldown_dtl;
   	
END gm_pkg_sm_confg_drilldown ;
/