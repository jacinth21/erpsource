/* Formatted on 2011/10/26 17:13 (Formatter Plus v4.8.0) */
--@"C:\Database\Packages\Sales\invmanagement\gm_pkg_sm_tagset_rpt.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_sm_tagset_rpt
IS
--
/*********************************
   Purpose: This procedure is used to fetch tag current location Info.
**********************************/
--
	PROCEDURE gm_fch_tagset_inv_location(
		p_strtagids    IN		VARCHAR2
	  , p_att_type	   IN 		t5010a_tag_attribute.c901_attribute_type%TYPE
	  , p_out	   	   OUT		TYPES.cursor_type
	)
	AS
	BEGIN
		my_context.set_my_inlist_ctx (p_strtagids);
		
		OPEN p_out
		 FOR
		 	SELECT t5010.c5010_tag_id TAGID,
		 	       t5010.c703_sales_rep_id REPID, 
				   t5010.c901_sub_location_type CURLOCID,
				   DECODE(t5010.c901_sub_location_type,11301,t5010.c5010_sub_location_id,0) LOCKACCID,
				   NVL(DECODE(t5010.c901_sub_location_type,50221,t5010.c5010_sub_location_id,NULL),t5010.c5010_location_id) DISTID,
				   t5010.c106_address_id ADDRESSID,
				   t5010.c207_set_id TAGSETID,
                   get_set_name(t5010.c207_set_id) SETNAME,
                   t5010.c5010_sub_location_id SUBLOCID,
                   get_account_name(t5010.c704_account_id) ACCTNAME,
                   get_user_name(NVL(t5010a.c5010a_last_updated_by,t5010a.c5010a_created_by)) UPDATEDBY,
                   get_distributor_name(t5010.c5010_location_id) DISTNAME,
                   get_distributor_email(t5010.c5010_location_id) DISTEMAILID,
           		   get_cs_ship_email (4121, t5010.c703_sales_rep_id) REPEMAILID
			  FROM t5010_tag t5010, (SELECT c5010_tag_id, c5010a_last_updated_by, c5010a_created_by 
			                           FROM t5010a_tag_attribute  
			                          WHERE c901_attribute_type = p_att_type 
			                            AND c5010a_void_fl IS NULL) t5010a
  		     WHERE t5010.c5010_tag_id  IN (SELECT token FROM v_in_list)
  		       AND t5010.c5010_tag_id = t5010a.c5010_tag_id (+)
  		  	   AND t5010.c5010_void_fl IS NULL
  		  ORDER BY t5010.c703_sales_rep_id;
  		  	
	END gm_fch_tagset_inv_location;
--
/*********************************
   Purpose: This procedure is used to fetch tag comments log info.
**********************************/
--
	PROCEDURE gm_fch_comment_loginfo (
	    p_strtagids        IN		VARCHAR2
	  , p_cancel_type	   IN		t901_code_lookup.c902_code_nm_alt%TYPE
	  , p_loginfo		   OUT		TYPES.cursor_type
	)
	AS
		v_cancel_type t907_cancel_log.c901_type%TYPE;
	BEGIN
		
		my_context.set_my_inlist_ctx (p_strtagids);
		
		SELECT c901_code_id
		  INTO v_cancel_type
		  FROM t901_code_lookup
		 WHERE c902_code_nm_alt = p_cancel_type;

		OPEN p_loginfo
		 FOR
			 SELECT c907_cancel_log_id logid, c907_ref_id txnid, c907_comments comments
				  , get_code_name (c901_cancel_cd) cancelreason, get_code_name (c901_type) canceltype
				  , get_user_name (c907_created_by) createdby, TO_CHAR (c907_cancel_date, GET_RULE_VALUE('DATEFMT', 'DATEFORMAT')) canceldate
			   FROM t907_cancel_log
			  WHERE	c907_ref_id IN (SELECT token FROM v_in_list)
				AND c901_type = v_cancel_type
				AND c907_void_fl IS NULL
		   ORDER BY c907_cancel_date DESC,c907_cancel_log_id DESC;
		   
	END gm_fch_comment_loginfo;
	
/******************************************************************
 Description : Procedure to get the address of the specific distributor
****************************************************************/
	PROCEDURE gm_fch_dist_address (
		p_distid 	    IN		t701_distributor.c701_distributor_id%TYPE
	  , p_distinfocur   OUT		TYPES.cursor_type
	)
	AS
	BEGIN

		OPEN p_distinfocur
		 FOR
			 SELECT   c701_distributor_id ID
					, SUBSTR (C701_SHIP_ADD1 || ' ' ||  C701_SHIP_ADD2   || ' '
							  || C701_SHIP_CITY
							, 0
							, 50
							 ) NAME
				 FROM t701_distributor 
				WHERE c701_distributor_id = p_distid 
				  AND c701_active_fl = 'Y'
				  AND (C701_SHIP_ADD1 IS NOT NULL OR  C701_SHIP_ADD2  IS NOT NULL OR C701_SHIP_CITY IS NOT NULL)
				  AND c701_void_fl IS NULL;
	END gm_fch_dist_address;

/******************************************************************
 Description : Procedure to get the address of the specific account
****************************************************************/
	PROCEDURE gm_fch_acct_address (
		p_acctid 	    IN		t704_account.c704_account_id%TYPE
	  , p_acctinfocur   OUT		TYPES.cursor_type
	)
	AS
	BEGIN

		OPEN p_acctinfocur
		 FOR
        	SELECT	c704_account_id ID
                  , SUBSTR (c704_ship_add1 || ' ' ||  c704_ship_add2   || ' '
							  || c704_ship_city
							, 0
							, 50
							 ) NAME
			  FROM t704_account 
			 WHERE c704_account_id = p_acctid 
			   AND (c704_ship_add1 IS NOT NULL OR c704_ship_add2 IS NOT NULL OR c704_ship_city IS NOT NULL)
               AND c704_void_fl IS NULL;
	END gm_fch_acct_address;
END gm_pkg_sm_tagset_rpt;
/