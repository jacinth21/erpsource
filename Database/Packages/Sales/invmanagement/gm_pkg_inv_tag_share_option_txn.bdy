/* Formatted on 2011/11/18 11:27 (Formatter Plus v4.8.0) */
--@"C:\Database\Packages\Sales\invmanagement\gm_pkg_inv_tag_share_option_txn.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_inv_tag_share_option_txn
IS
--
/*********************************************************************************************************
Purpose: This procedure is used to update the tag share option from Tag share report in Inv mgmt module
Author: Karthik Somanathan
**********************************************************************************************************/

	PROCEDURE gm_upd_tag_share_options (
		p_tag_id	  		IN   T5010g_Tag_System_Locator.c5010_tag_id%TYPE
	  , p_share_option_id   IN   T5010g_Tag_System_Locator.C901_Tag_Share_Options%TYPE
	  , p_user_id   		IN   T5010g_Tag_System_Locator.C5010g_Updated_By%TYPE
	  , p_out_message   	OUT   VARCHAR2
	)
	AS
	BEGIN
		
		UPDATE T5010g_Tag_System_Locator Set
		 C901_Tag_Share_Options = p_share_option_id,
		 C5010g_Updated_By 	 = p_user_id,
		 C5010g_Updated_Date = CURRENT_DATE
		WHERE C5010_TAG_ID   = p_tag_id
		AND C5010G_VOID_FL IS NULL;
		
		p_out_message := 'Tag Share Option Updated';
	
	END gm_upd_tag_share_options;
--
END gm_pkg_inv_tag_share_option_txn;
/
