/**********************************************************************
  *Purpose: Score Card Consignment log
  Author: Yoga
  PC: 339
  Location: Database\Packages\Sales\invmanagementgm_pkg_sm_tag_consignment_history.pkg
  **********************************************************************/
  
CREATE OR REPLACE PACKAGE gm_pkg_sm_tag_consignment_history
AS
  /**********************************************************************
  *Purpose: Procedure to insert Tag transactions from Tag log to t504g_consignment_tag_history based on c5011_process_fl
  Author: Yoga
  PC: 339
  **********************************************************************/
  PROCEDURE gm_sc_tag_cons_hist_load_main;


/**********************************************************************
*Purpose: Procedure to update process flag
Author: Yoga
PC: 339
**********************************************************************/
PROCEDURE  gm_sc_upd_process_fl(
  p_log_id t5011_tag_log.c5010_tag_id%TYPE);

/**********************************************************************
*Purpose: Procedure  to update error info
Author: Yoga
PC: 339
**********************************************************************/
PROCEDURE  gm_sc_ins_error_info(
  p_error_msg t910_load_log_table.c910_status_details%TYPE);

  /**********************************************************************
  *Purpose: Function to get active distributor
  Author: Yoga
  PC: 339
  **********************************************************************/
  FUNCTION get_tag_distributor(
      p_tag_id t5011_tag_log.c5010_tag_id%TYPE )
    RETURN t701_distributor.c701_distributor_id%TYPE;
 /**********************************************************************
  *Purpose: Procedure to insert Tag transation
  Author: Yoga
  PC: 339
  **********************************************************************/
  PROCEDURE gm_sc_ins_tag_consignment(
      p_tag_id t5011_tag_log.c5010_tag_id%TYPE,
      p_set_id t207_set_master.c207_set_id%TYPE,
      p_part_number_id t205_part_number.c205_part_number_id%TYPE,
      p_created_date t5011_tag_log.c5011_created_date%TYPE,
      p_location_id t5011_tag_log.c5011_location_id%TYPE,
      p_sub_location_id t5011_tag_log.c5011_sub_location_id%TYPE,
      p_txn_id t5011_tag_log.c5011_last_updated_trans_id%TYPE,
      p_missing_fl t504g_consignment_tag_history.c504g_missing_fl%TYPE);
  /**********************************************************************
  *Purpose: Procedure to update Tag transations for return date (Return or Transfer)
  Author: Yoga
  PC: 339
  **********************************************************************/
  PROCEDURE gm_sc_upd_cons_tag_hist_return_date(
      p_tag_id t5011_tag_log.c5010_tag_id%TYPE,
      p_return_date DATE );
  /**********************************************************************
  *Purpose: Procedure to update Return date based on Return app table T5006_RETURNS_LOG new return entries
  Author: Yoga
  PC: 3034 
  **********************************************************************/
  PROCEDURE gm_sc_tag_retun_app_update;
  /**********************************************************************
  *Purpose: Procedure to update return date in monthly split
  Author: Yoga
  PC: 2781
  **********************************************************************/
  PROCEDURE gm_sc_cn_upd_mon_returns;

  /**********************************************************************
  *Purpose: Get source consignment data to be split by month
  Author: Yoga
  PC: 2781
  **********************************************************************/
  PROCEDURE gm_sc_cn_fetch_for_month_split;

  /**********************************************************************
  *Purpose: insert the monthwise data in t504h_cons_tag_month_history
  Author: Yoga
  PC: 2781
  **********************************************************************/
  PROCEDURE gm_sc_cn_ins_split_month(
    p_txn_id t504h_cons_tag_month_history.c504g_tag_transaction_id%TYPE,
    p_tag_id t5010_tag.c5010_tag_id%TYPE,
    p_set_id t207_set_master.c207_set_id%TYPE,
    p_distributor_id t701_distributor.c701_distributor_id%TYPE,
    p_start_date DATE,
    p_end_date DATE,
    p_split_start_Date DATE,
    p_split_end_date DATE ,
    p_days_kept NUMBER
  );

  /**********************************************************************
  *Purpose: Split the source table data to monthwise
  Author: Yoga
  PC: 2781
  **********************************************************************/
  PROCEDURE gm_sc_cn_ins_mon_split_usage(
    p_tag_id t504g_consignment_tag_history.C5010_TAG_ID%TYPE,
    p_dist_id t701_distributor.c701_distributor_id%TYPE,
    p_start_date t504g_consignment_tag_history.c504g_placement_date%TYPE,
    p_end_date t504g_consignment_tag_history.c504g_return_date%TYPE);

   /**********************************************************************
  *Purpose: Call this procedure from a job which runs every month 1st to insert current month data
  Author: Yoga
  PC: 2781
  **********************************************************************/
  PROCEDURE gm_sc_cn_mon_load_main;

  /**********************************************************************
  *Purpose: Function to check monthly record available or not. If count is greater than 0, record available
  Author: Yoga
  PC: 2781
  **********************************************************************/
  FUNCTION get_month_record_count(
    p_tag_id t504h_cons_tag_month_history.c5010_tag_id%TYPE,
    p_dist_id t701_distributor.c701_distributor_id%TYPE,
    p_start_date t504h_cons_tag_month_history.c504h_month_start%TYPE,
    p_end_date t504h_cons_tag_month_history.c504h_month_end%TYPE)
    RETURN NUMBER;

  /**********************************************************************
  *Purpose: Procedure to update exp usage and days kept
  Author: Yoga
  PC: 2781 
  **********************************************************************/
 PROCEDURE  gm_sc_cn_upd_mon_usage( 
    p_mon_hist_id T504H_CONS_TAG_MONTH_HISTORY.C504H_CONS_TAG_MONTH_HIST_ID%TYPE);

  /**********************************************************************
  *Purpose: Function to get number working days
  Author: Yoga
  PC: 342
  Code Lookup:
  **********************************************************************/
  FUNCTION get_number_of_days(
      p_start_date DATE,
      p_end_date   DATE )
    RETURN NUMBER;


    /*********************************
   Purpose: This procedure will update the Placement Date or Return date for the Consignment transaction.
   Author : Rajeshwaran
**********************************/
--
PROCEDURE gm_sc_cn_upd_adjustment(
	    p_cn_trans_id		IN  T504G_CONSIGNMENT_TAG_HISTORY.C504G_TAG_TRANSACTION_ID%TYPE,
	    p_old_plmnt_dt 		IN 	T504G_CONSIGNMENT_TAG_HISTORY.C504G_PLACEMENT_DATE%TYPE,
	    p_old_return_dt 	IN 	T504G_CONSIGNMENT_TAG_HISTORY.C504G_RETURN_DATE%TYPE,
	    p_new_plmnt_dt 		IN 	T504G_CONSIGNMENT_TAG_HISTORY.C504G_PLACEMENT_DATE%TYPE,
	    p_new_return_dt 	IN 	T504G_CONSIGNMENT_TAG_HISTORY.C504G_RETURN_DATE%TYPE,
	    p_last_updated_by 	IN 	T504G_CONSIGNMENT_TAG_HISTORY.C504G_LAST_UPDATED_BY%TYPE,
      p_comments          IN T504G_CONSIGNMENT_TAG_HISTORY.C504G_COMMENTS%TYPE,
      p_pathlink          IN t504g_consignment_tag_history.c504g_pathlink%TYPE
	);	


END gm_pkg_sm_tag_consignment_history;
/