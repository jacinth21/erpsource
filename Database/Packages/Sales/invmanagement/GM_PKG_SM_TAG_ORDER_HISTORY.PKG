create or replace PACKAGE GM_PKG_SM_TAG_ORDER_HISTORY
 AS
  /**********************************************************************
  *Purpose: Procedure to insert Tag transactions from Tag log to t501d_order_tag_usage_details based on c501d_owner_fl
  Author: Yoga
  PC: 342
  **********************************************************************/
  PROCEDURE gm_sc_tag_order_hist_load_main;
  /**********************************************************************
  *Purpose: Function to get order distributor
  Author: Yoga
  PC: 342
  **********************************************************************/
   FUNCTION get_tag_distributor(
    p_tag_id t5011_tag_log.c5010_tag_id%TYPE,
    p_order_date t501_order.c501_order_date%TYPE)
    RETURN t701_distributor.c701_distributor_id%TYPE;
  /**********************************************************************
  *Purpose: Procedure to update order update Order and Tag Owner
  Author: Yoga
  PC: 342
  **********************************************************************/
  PROCEDURE gm_sc_upd_tag_order_owner(
     p_tag_id t5011_tag_log.c5010_tag_id%TYPE,
     p_order_id t501_order.c501_order_id%TYPE,
     p_order_owner t701_distributor.c701_distributor_id%TYPE,
     p_tag_owner t701_distributor.c701_distributor_id%TYPE);
  /**********************************************************************
  *Purpose: Procedure to update Invald_share between to fieldsale for an order
  Author: Yoga
  PC: 342
  **********************************************************************/
  PROCEDURE gm_sc_upd_invalid_share(
    p_tag_id t5011_tag_log.c5010_tag_id%TYPE, 
    p_set_id t207_set_master.c207_set_id%TYPE,
    p_order_id t501_order.c501_order_id%TYPE,
      p_order_date t501_order.c501_order_date%TYPE,
    p_order_owner t701_distributor.c701_distributor_id%TYPE,
    p_tag_owner t701_distributor.c701_distributor_id%TYPE);
/**********************************************************************
  *Purpose: Function to get set count in a given order raised week
  Author: Yoga
  PC: 342
  Code Lookup:
  **********************************************************************/
  FUNCTION get_distributor_weekly_set_count(
    p_set_id t207_set_master.c207_set_id%TYPE,
    p_order_id t501_order.c501_order_id%TYPE,
    p_order_date t501_order.c501_order_date%TYPE,
    p_order_owner t701_distributor.c701_distributor_id%TYPE)
  RETURN NUMBER;
    
  /**********************************************************************
  *Purpose: Function to get set count in a given order raised week
  Author: Yoga
  PC: 342
  Code Lookup:
  **********************************************************************/
  FUNCTION get_distributor_weekly_order_count(
    p_order_date t501_order.c501_order_date%TYPE,
    p_order_owner t701_distributor.c701_distributor_id%TYPE,
    p_set_id t207_set_master.c207_set_id%TYPE
    )
    RETURN NUMBER;
  
  /**********************************************************************
  *Purpose: Function to get start day of given order date
  Author: Yoga
  PC: 342
  Code Lookup:
  **********************************************************************/
  FUNCTION get_order_week_start_date(
    p_order_date t501_order.c501_order_date%TYPE)
  RETURN DATE;
 
  /**********************************************************************
  *Purpose: Function to get n working days
  Author: Yoga
  PC: 342
  Code Lookup:
  **********************************************************************/
  FUNCTION get_last_nth_business_day ( 
    start_date DATE, working_days INTEGER
  ) RETURN DATE ;
    
  /**********************************************************************
  *Purpose: Function to get number working days
  Author: Yoga
  PC: 342
  Code Lookup:
  **********************************************************************/
  FUNCTION get_number_of_business_day ( 
    p_start_date DATE, p_end_date DATE
  ) RETURN NUMBER;


  /**********************************************************************
  *Purpose: Function to get number working days in a month
  Author: Yoga
  PC: 342
  Code Lookup:
  **********************************************************************/
  FUNCTION get_month_business_days(
      p_date   DATE )
    RETURN NUMBER;
  
  /**********************************************************************
  *Purpose: Procedure to get Bulk DOs and call  ovrloaded procedure to insert t501d_tag_order_usage_details table
  Author: Yoga
  PC: 3534
  **********************************************************************/
  PROCEDURE gm_sc_fetch_bulk_order;
  
  /**********************************************************************
  *Purpose: Procedure to mark buk DO is processed
  Author: Yoga
  PC: 3534
  **********************************************************************/
  PROCEDURE gm_sc_upd_order_for_bulk(
    p_order_id t501_order.c501_order_id%TYPE
  );
  
  /**********************************************************************
  *Purpose: Function to get system for given set id
  Author: Yoga
  PC: 342
  Code Lookup:
  **********************************************************************/
  FUNCTION get_system_id(
      p_set_id t207_set_master.c207_set_id%TYPE )
    RETURN VARCHAR2;

	/**********************************************************************
	*Purpose: Procedure to fetch all tracking implant orders and update bulk DO dtls
	Author:
	PC: 3845
	**********************************************************************/
	PROCEDURE gm_process_order_bulk_do_by_part (
        p_part_number_id IN t205_part_number.c205_part_number_id%TYPE,
        p_user_id        IN t501_order.c501_last_updated_by%TYPE) ;
        
	/**********************************************************************
	*Purpose: Procedure to update the order bulk DO process flag
	Author:
	PC: 3845
	**********************************************************************/
	PROCEDURE gm_upd_order_bulk_do_process_fl (
        p_order_id   IN t501_order.c501_order_id%TYPE,
        p_bulk_do_fl IN VARCHAR2,
        p_user_id    IN t501_order.c501_last_updated_by%TYPE) ;

	/**********************************************************************
	*Purpose: Procedure to fetch all baseline updated orders based on system
	* and update bulk DO dtls
	Author:
	PC: 3845
	**********************************************************************/
	PROCEDURE gm_process_order_bulk_do_by_system (
			p_system_id IN t207_set_master.c207_set_id%TYPE,
			p_user_id   IN t207_set_master.c207_last_updated_by%TYPE);
			

	/**********************************************************************
	*  When the Loaner/Consignment placement/return date is updated ,the Tag Owner should be updated to the distributor who
	* booked the sales order.
	* Author : Rajeshwaran
	**********************************************************************/
		PROCEDURE gm_sc_upd_actuals_credit (
    p_dist_id			IN	t504a_loaner_transaction.C504A_CONSIGNED_TO_ID%TYPE,
    p_tag_id			IN	t5010_tag.C5010_TAG_ID%TYPE,
    p_plmnt_dt 			IN 	T50401_loaner_tag_history.C50401_PLACEMENT_DATE%TYPE,
    p_return_dt 		IN 	T50401_loaner_tag_history.C50401_RETURN_DATE%TYPE,
    p_last_updated_by 	IN 	T50401_loaner_tag_history.C50401_LAST_UPDATED_BY%TYPE,
    p_comments          IN T50401_loaner_tag_history.C50401_COMMENTS%TYPE,
    p_pathlink          IN T50401_loaner_tag_history.c50401_pathlink%TYPE
    );
						
END GM_PKG_SM_TAG_ORDER_HISTORY;
/