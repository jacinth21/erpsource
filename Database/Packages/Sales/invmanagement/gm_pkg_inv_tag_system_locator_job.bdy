/*
FileName : gm_pkg_inv_tag_system_locator_job.bdy
Description : 
Author : karthik Somanathan
Date : 10/29/2020
Copyright : Globus Medical Inc 
*/

CREATE OR REPLACE
PACKAGE BODY gm_pkg_inv_tag_system_locator_job
IS
 /*********************************************************************************************************************
  * Description : Master Procedure to Fetch Tag details and save in System Locator Table
  * Author : Karthiks
  *********************************************************************************************************************/   
 PROCEDURE gm_process_tag_main
 
AS
v_last_sync_date t9300_job.C9300_START_DATE%TYPE:= NULL;
v_job_id   		 t9300_job.C9300_JOB_ID%TYPE:= 1055;
v_user_id  		 t9300_job.C9300_LAST_UPDATED_BY%TYPE:= 30301;

BEGIN
	  --fetch the last sync date and update startdate as currentdate of job.
	  gm_pkg_device_job_run_date.gm_update_job_details(v_job_id,v_user_id,v_last_sync_date);
	  
	  --Process the tag master details and save it in the system locator table.
	  gm_pkg_inv_system_locator_txn.gm_process_tag_master_dtls(v_last_sync_date,v_user_id);
	  
	  --Process the tag  details from Order (Find the Tag which is physically available in field) and save it in the system locator table.
	  gm_pkg_inv_system_locator_txn.gm_process_tag_missing_dtls(v_last_sync_date,v_user_id);
	  
	  --update job end date.
	  gm_pkg_device_tag_sync_master.gm_update_end_date_job(v_job_id,v_user_id);
  
 END gm_process_tag_main;

 /*********************************************************************************************************************
  * Description : Master Procedure to fetch Rep Email/Phone number from DO Classification/Tag usage 
  * 			  for the Tag and save in System Locator Table
  * Author : Karthiks
  *********************************************************************************************************************/   
  PROCEDURE gm_process_rep_contact_dtl_main
  
 AS
v_last_sync_date t9300_job.C9300_START_DATE%TYPE:= NULL;
v_job_id   		 t9300_job.C9300_JOB_ID%TYPE:= 1056;
v_user_id  		 t9300_job.C9300_LAST_UPDATED_BY%TYPE:= 30301;
 
 BEGIN
	  --fetch the last sync date and update startdate as currentdate of job.
	  gm_pkg_device_job_run_date.gm_update_job_details(v_job_id,v_user_id,v_last_sync_date);
	  
	  --Process the Rep Email/Phone number details from Order and save it in the system locator table.
	  gm_pkg_inv_system_locator_txn.gm_process_tag_rep_contact_dtls(v_last_sync_date,v_user_id);
	  
	  --update job end date.
	  gm_pkg_device_tag_sync_master.gm_update_end_date_job(v_job_id,v_user_id);

 END gm_process_rep_contact_dtl_main;
 
 /**********************************************************************************************************************************
  * Description : Master Procedure to group details from t5010g_tag_system_locator table and put into t5010h_system_locator_dtls
  * Author 		: Karthiks
  **********************************************************************************************************************************/   
  PROCEDURE gm_process_sys_loc_report_dtl
  
 AS
v_last_sync_date t9300_job.C9300_START_DATE%TYPE:= NULL;
v_job_id   		 t9300_job.C9300_JOB_ID%TYPE:= 1057;
v_user_id  		 t9300_job.C9300_LAST_UPDATED_BY%TYPE:= 30301;
 BEGIN
	  --fetch the last sync date and update startdate as currentdate of job.
	  gm_pkg_device_job_run_date.gm_update_job_details(v_job_id,v_user_id,v_last_sync_date);
	  
	  --group the details from t5010g_tag_system_locator table and put into t5010h_system_locator_dtls
	  gm_pkg_inv_system_locator_txn.gm_process_system_loc_report_details(v_last_sync_date,v_user_id);
	  
	  --update job end date.
	  gm_pkg_device_tag_sync_master.gm_update_end_date_job(v_job_id,v_user_id);

 END gm_process_sys_loc_report_dtl;
 
  /**********************************************************************************************************************************
  * Description : Master Procedure to update Share Options in  t5010h_system_locator_dtls table
  * Author 		: Karthiks
  **********************************************************************************************************************************/   
  PROCEDURE gm_process_tag_share_option_main
  
 AS
v_last_sync_date t9300_job.C9300_START_DATE%TYPE:= NULL;
v_job_id   		 t9300_job.C9300_JOB_ID%TYPE:= 1061;
v_user_id  		 t9300_job.C9300_LAST_UPDATED_BY%TYPE:= 30301;
 BEGIN
	  --fetch the last sync date and update startdate as currentdate of job.
	  gm_pkg_device_job_run_date.gm_update_job_details(v_job_id,v_user_id,v_last_sync_date);
	  
	  --group the details from t5010g_tag_system_locator table and put into t5010h_system_locator_dtls
	  gm_pkg_inv_system_locator_txn.gm_process_tag_share_options(v_last_sync_date,v_user_id);
	  
	  --update job end date.
	  gm_pkg_device_tag_sync_master.gm_update_end_date_job(v_job_id,v_user_id);

 END gm_process_tag_share_option_main;
	
END gm_pkg_inv_tag_system_locator_job;
/ 