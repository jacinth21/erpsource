/*
FileName : gm_pkg_inv_system_locator_txn.pkg
Description : 
Author : karthik Somanathan
Date : 10/29/2020
Copyright : Globus Medical Inc 
*/

CREATE OR REPLACE PACKAGE gm_pkg_inv_system_locator_txn
IS
 /*********************************************************************************************************************
  * Description :Procedure to Fetch Tag details and save in System Locator Table
  * Author : Karthiks
  *********************************************************************************************************************/   
  PROCEDURE gm_process_tag_master_dtls(
  		 p_last_sync_date IN t5010_tag.C5010_Last_Updated_Date%TYPE,
    	 p_user_id        IN VARCHAR2 	
  );
  
   /*********************************************************************************************************************
  * Description :Procedure to Fetch Tag details from order and save in System Locator Table
  * Author : Karthiks
  *********************************************************************************************************************/   
  PROCEDURE gm_process_tag_missing_dtls(
  		 p_last_sync_date IN t5010_tag.C5010_Last_Updated_Date%TYPE,
    	 p_user_id        IN VARCHAR2 	
  );
  
  /*********************************************************************************************************************
  * Description :Procedure to Fetch Tag rep details from DO classification / Order and save in System Locator Table
  * Author : Karthiks
  *********************************************************************************************************************/   
  PROCEDURE gm_process_tag_rep_contact_dtls(
  		 p_last_sync_date IN t5010_tag.C5010_Last_Updated_Date%TYPE,
    	 p_user_id        IN VARCHAR2 	
  ); 
  
   /*********************************************************************************************************************
  * Description :Procedure to Fetch Tag rep details from DO classification and save in System Locator Table
  * Author : Karthiks
  *********************************************************************************************************************/   
  PROCEDURE gm_process_tag_rep_dtls_from_do_clas(
  		 p_last_sync_date IN t5010_tag.C5010_Last_Updated_Date%TYPE,
    	 p_user_id        IN VARCHAR2 	
  ); 
  
 /*********************************************************************************************************************
  * Description :Procedure to Fetch Tag rep details from  Order and save in System Locator Table
  * Author : Karthiks
  *********************************************************************************************************************/   
  PROCEDURE gm_process_tag_rep_dtls_from_order(
  		 p_tag_id		  IN  T5010G_TAG_SYSTEM_LOCATOR.C5010_TAG_ID%TYPE,
    	 p_region_id      IN  T701_Distributor.C701_Region%TYPE,
    	 p_out_rep_email  OUT T5010G_TAG_SYSTEM_LOCATOR.C703_EMAIL_ID%TYPE,
    	 p_out_ph_no	  OUT T5010G_TAG_SYSTEM_LOCATOR.C703_PHONE_NUMBER%TYPE,
    	 p_out_email_type OUT T5010G_TAG_SYSTEM_LOCATOR.C703_PHONE_NUMBER%TYPE
  ); 

  /*********************************************************************************************************************
  * Description : Procedure to save details in t5010g_tag_system_locator table
  * Author : Karthiks
  *********************************************************************************************************************/   
  PROCEDURE gm_sav_system_locator_details(
  		 p_tag_id		  IN T5010G_TAG_SYSTEM_LOCATOR.C5010_TAG_ID%TYPE,
    	 p_set_id		  IN T5010G_TAG_SYSTEM_LOCATOR.C207_SET_ID%TYPE,
    	 p_system_id	  IN T5010G_TAG_SYSTEM_LOCATOR.C207_SET_SALES_SYSTEM_ID%TYPE,
    	 p_dist_id		  IN T5010G_TAG_SYSTEM_LOCATOR.C701_DISTRIBUTOR_ID%TYPE,
    	 p_rep_email_id	  IN T5010G_TAG_SYSTEM_LOCATOR.C703_EMAIL_ID%TYPE,
    	 p_rep_phone	  IN T5010G_TAG_SYSTEM_LOCATOR.C703_PHONE_NUMBER%TYPE,
    	 p_email_type	  IN T5010G_TAG_SYSTEM_LOCATOR.C901_EMAIL_TYPE%TYPE,
    	 p_user_id		  IN T5010G_TAG_SYSTEM_LOCATOR.C5010G_UPDATED_BY%TYPE
  );
 
  /*********************************************************************************************************************
  * Description :Procedure to group the details from t5010g_tag_system_locator table and put into t5010h_system_locator_dtls
  * Author : Karthiks
  *********************************************************************************************************************/   
  PROCEDURE gm_process_system_loc_report_details(
 		 p_last_sync_date IN T5010G_TAG_SYSTEM_LOCATOR.C5010G_UPDATED_DATE%TYPE,
    	 p_user_id        IN VARCHAR2 	
  ); 
  
  /*********************************************************************************************************************
  * Description : Procedure to save details in t5010h_system_locator_dtls table
  * Author : Karthiks
  *********************************************************************************************************************/   
  PROCEDURE gm_sav_sys_loc_dist_details(
      	 p_dist_id		  IN t5010h_system_locator_dtls.C701_DISTRIBUTOR_ID%TYPE,
    	 p_set_id		  IN t5010h_system_locator_dtls.C207_SET_ID%TYPE,
    	 p_system_id	  IN t5010h_system_locator_dtls.C207_SET_SALES_SYSTEM_ID%TYPE,
    	 p_rep_email_id	  IN t5010h_system_locator_dtls.C703_EMAIL_ID%TYPE,
    	 p_rep_phone	  IN t5010h_system_locator_dtls.C703_PHONE_NUMBER%TYPE,
    	 p_user_id		  IN t5010h_system_locator_dtls.C5010H_UPDATED_BY%TYPE
  );
  
  /*********************************************************************************************************************
  * Description :Procedure to update the tag share options in t5010h_system_locator_dtls table 
  * Author : Karthiks
  *********************************************************************************************************************/   
  PROCEDURE gm_process_tag_share_options(
 		 p_last_sync_date IN T5010G_TAG_SYSTEM_LOCATOR.C5010G_UPDATED_DATE%TYPE,
    	 p_user_id        IN VARCHAR2 	
  );
  
END gm_pkg_inv_system_locator_txn;
/