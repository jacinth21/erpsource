/*
FileName : gm_pkg_inv_system_locator_txn.bdy
Description : 
Author : karthik Somanathan
Date : 10/29/2020
Copyright : Globus Medical Inc 
*/

/*
 * Order to execute the Job
 * Exec Gm_Pkg_Inv_Tag_System_Locator_Job.Gm_Process_Tag_Main(); --Run 1st
 * Exec Gm_Pkg_Inv_Tag_System_Locator_Job.gm_process_rep_contact_dtl_main(); --Run 2nd (After Do classification Job / Tag Owner Job )
 * Exec Gm_Pkg_Inv_Tag_System_Locator_Job.gm_process_sys_loc_report_dtl(); --Run 3rd (After the above Job)
 * 
 * */

CREATE OR REPLACE
PACKAGE BODY gm_pkg_inv_system_locator_txn
IS
 /*********************************************************************************************************************
  * Description : Procedure to Fetch Tag details and save in System Locator Table
  * Author : Karthiks
  *********************************************************************************************************************/   
  PROCEDURE gm_process_tag_master_dtls(
  		 p_last_sync_date IN t5010_tag.C5010_Last_Updated_Date%TYPE,
    	 p_user_id        IN VARCHAR2 	
  )
AS
 v_set_id    T5010g_Tag_System_Locator.C207_SET_ID%TYPE;
 v_system_id T5010g_Tag_System_Locator.C207_SET_SALES_SYSTEM_ID%TYPE;
/*
 *51000 := Consignments
 *4120  := Distributor
 *10004 := Product Loaner
 */
CURSOR tag_from_field
  IS
    SELECT T5010.C5010_Tag_Id tagid,
    	   T5010.C5010_Location_Id distid,
      	   T5010.C207_SET_ID setid,
      	   T5010.C205_PART_NUMBER_ID pnum
    FROM T5010_TAG T5010 
    WHERE T5010.C5010_Last_Updated_Date >=  NVL(p_last_sync_date,C5010_Last_Updated_Date)
    AND t5010.C901_TRANS_TYPE          = 51000
    AND T5010.C901_LOCATION_TYPE       = 4120 
    AND T5010.C901_Inventory_Type NOT IN (10004)
    AND T5010.C5010_Void_Fl           IS NULL
    ORDER BY T5010.C5010_Tag_Id ASC;
    
BEGIN
 
 FOR tagdtl IN tag_from_field
  LOOP
    BEGIN
	 v_set_id := tagdtl.setid;
	 
	  --if the set id is null then get the set id based on the part number using critical set logic
	  IF tagdtl.pnum IS NOT NULL THEN
	  	v_set_id := NVL(tagdtl.setid,gm_pkg_common.get_part_primary_set_id(tagdtl.pnum));
	  END IF;
	
      --Get the system id for the set id from "tag_from_field" cursor
      BEGIN
        SELECT T207.C207_Set_Sales_System_Id systemid
          INTO v_system_id
        FROM T207_Set_Master T207
        WHERE T207.C207_Set_Id = v_set_id
        AND T207.C207_Void_Fl IS NULL;
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
        v_system_id := NULL;
      END;
     
      --Pass tagid,setid, system id and distributor id to save the details in T5010G_TAG_SYSTEM_LOCATOR table
      IF tagdtl.tagid IS NOT NULL AND v_set_id IS NOT NULL THEN
      	gm_pkg_inv_system_locator_txn.gm_sav_system_locator_details(tagdtl.tagid,v_set_id,v_system_id,
      		tagdtl.distid,NULL,NULL,NULL,p_user_id);
      END IF;
      
    END;
END LOOP;
  
END gm_process_tag_master_dtls;


/*********************************************************************************************************************
  * Description : Procedure to Fetch Tag details from order and save in System Locator Table
  * Author : Karthiks
  *********************************************************************************************************************/   
  PROCEDURE gm_process_tag_missing_dtls(
  		 p_last_sync_date IN t5010_tag.C5010_Last_Updated_Date%TYPE,
    	 p_user_id        IN VARCHAR2 	
  )
AS
 v_set_id    T5010g_Tag_System_Locator.C207_SET_ID%TYPE;
 v_system_id T5010g_Tag_System_Locator.C207_SET_SALES_SYSTEM_ID%TYPE;
 v_tag_id 	 T5010g_Tag_System_Locator.C5010_TAG_ID%TYPE;
 v_dist_id 	 T5010g_Tag_System_Locator.C701_DISTRIBUTOR_ID%TYPE;
 v_count 	 NUMBER;

 --Get the tag those are not shipped in the system but physically available in the field 
CURSOR tag_missing
  IS
    SELECT Tagid
		FROM
		  (SELECT T5010.C5010_Tag_Id Tagid ,
		    T5010.C5010_Last_Updated_Trans_Id Tagtxn,
		    T504.C504_Consignment_Id Consignmentid,
		    T504.C504_Void_Fl voidflag
		  FROM T5010_Tag T5010 ,
		    T504_Consignment T504
		  WHERE t5010.C5010_Void_Fl            IS NULL
		  AND T5010.C901_Status                IN (51012,51011,26240343) --Active,Released,Sold
		  AND T5010.C5010_Last_Updated_Trans_Id = T504.C504_Consignment_Id(+)
		  AND T504.C504_Void_Fl                 = 'Y'
		  AND T5010.C5010_Last_Updated_Date >=  NVL(p_last_sync_date,C5010_Last_Updated_Date)
		  UNION ALL
		  SELECT Tagid ,
		    Tagtxn ,
		    Consignmentid ,
		    voidflag
		  FROM
		    (SELECT T5010.C5010_Tag_Id Tagid ,
		      T5010.C5010_Last_Updated_Trans_Id Tagtxn,
		      T907.C907_Ref_Id Consignmentid,
		      T907.C907_Void_Fl voidflag
		    FROM T5010_Tag T5010 ,
		      T907_Shipping_Info t907
		    WHERE t5010.C5010_Void_Fl            IS NULL
		    AND T5010.C5010_Last_Updated_Trans_Id = T907.C907_Ref_Id(+)
		    AND T5010.C5010_Last_Updated_Date >=  NVL(p_last_sync_date,C5010_Last_Updated_Date)
		    )
		  WHERE Consignmentid IS NULL
		  AND Tagtxn          IS NOT NULL
		  AND Tagtxn LIKE 'GM-CN-%'
		  )
		GROUP BY Tagid;
    
BEGIN
 
 FOR tagdtl IN tag_missing
  LOOP
    BEGIN
	    
	    --Check the Tag is entered on any orders
	     BEGIN
	    	SELECT Tagid ,
			  Setid ,
			  Distid
			INTO v_tag_id,
			  v_set_id,
			  v_dist_id
			FROM
			  (SELECT T501d.C5010_Tag_Id Tagid,
			    T501d.C207_Set_Id Setid,
			    T703.C701_Distributor_Id Distid,
			    T501.C501_Order_Date
			  FROM T501_Order T501,
			    T501d_Order_Tag_Usage_Details T501d,
			    T703_Sales_Rep t703
			  WHERE T501.C501_Order_Id   = T501d.C501_Order_Id
			  AND T501.C703_Sales_Rep_Id = t703.C703_Sales_Rep_Id
			  AND T501.C501_VOID_FL     IS NULL
			  AND T501d.C501d_Void_Fl   IS NULL
			  AND T501d.C5010_Tag_Id    IS NOT NULL
			  AND T501d.C207_Set_Id     IS NOT NULL
			  AND T501d.C5010_Tag_Id     = tagdtl.tagid
			  ORDER BY T501.C501_Order_Date DESC
			  )
			WHERE rownum =1;
		  EXCEPTION
	      WHEN NO_DATA_FOUND THEN
	        v_tag_id  := NULL;
	        v_set_id  := NULL;
	        v_dist_id := NULL;
	      END;
			
	
      --Get the system id for the set id from "tag_missing" cursor
      BEGIN
        SELECT T207.C207_Set_Sales_System_Id systemid
          INTO v_system_id
        FROM T207_Set_Master T207
        WHERE T207.C207_Set_Id = v_set_id
        AND T207.C207_Void_Fl IS NULL;
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
        v_system_id := NULL;
      END;
      
      --CHeck the tag is already available in Tag system locator table , if it's available then no need to update ,we just need insert only.
      	SELECT COUNT(1)
	  	  INTO v_count
		FROM T5010g_Tag_System_Locator
		WHERE C5010_Tag_Id  = tagdtl.tagid
		AND C5010g_Void_Fl IS NULL;
     
      --Pass tagid,setid, system id and distributor id to save the details in T5010G_TAG_SYSTEM_LOCATOR table
       IF tagdtl.tagid IS NOT NULL AND v_set_id IS NOT NULL AND v_count = 0 THEN
	      	gm_pkg_inv_system_locator_txn.gm_sav_system_locator_details(v_tag_id,v_set_id,v_system_id,
	      		v_dist_id,NULL,NULL,NULL,p_user_id);
      	END IF;      	
      	
    END;
END LOOP;
  
END gm_process_tag_missing_dtls;


/*********************************************************************************************************************
  * Description : Procedure to Fetch rep email / Phone number details from Order and save in System Locator Table
  * Author 		: Karthiks
  *********************************************************************************************************************/   
  PROCEDURE gm_process_tag_rep_contact_dtls(
  		 p_last_sync_date IN t5010_tag.C5010_Last_Updated_Date%TYPE,
    	 p_user_id        IN VARCHAR2 	
  )
AS
BEGIN
	
 	gm_pkg_inv_system_locator_txn.gm_process_tag_rep_dtls_from_do_clas(p_last_sync_date,p_user_id);
  
END gm_process_tag_rep_contact_dtls;

 /*********************************************************************************************************************
  * Description :Procedure to Fetch Tag rep details from DO classification and save in System Locator Table
  * Author 		: Karthiks
  * Dependency 	: Needs to run after DO classification JOB(T501b_Order_By_System)
  *********************************************************************************************************************/  
  PROCEDURE gm_process_tag_rep_dtls_from_do_clas(
  		 p_last_sync_date IN t5010_tag.C5010_Last_Updated_Date%TYPE,
    	 p_user_id        IN VARCHAR2 	
  )
AS
v_rep_email_id     T5010G_TAG_SYSTEM_LOCATOR.C703_EMAIL_ID%TYPE;
v_rep_phone_number T5010G_TAG_SYSTEM_LOCATOR.C703_PHONE_NUMBER%TYPE;
v_dist_region 	   T701_Distributor.C701_Region%TYPE;
v_email_type t901_code_lookup.c901_code_id%TYPE := NULL;
v_from_dt 	 T501_Order.C501_ORDER_DATE%TYPE;
v_to_dt 	 T501_Order.C501_ORDER_DATE%TYPE;

/*
 *51000 := Consignments
 *4120  := Distributor
 *10004 := Product Loaner
 */
CURSOR tag_from_sys_locator
  IS
		SELECT T5010g.C5010_Tag_Id tagid ,
		  T5010g.C207_SET_ID setid,
		  T5010g.C207_Set_Sales_System_Id Systemid,
		  T5010g.C701_DISTRIBUTOR_ID distid
		FROM T5010g_Tag_System_Locator T5010g ,
		  T501_Order T501,
		  T703_Sales_Rep T703
		WHERE T5010g.C701_DISTRIBUTOR_ID = T703.C701_Distributor_Id
		AND T501.C703_Sales_Rep_Id       = T703.C703_Sales_Rep_Id
		AND T501.C501_Order_Date        >= NVL(p_last_sync_date,C501_Order_Date)
		AND T501.C501_Void_Fl           IS NULL
		AND T5010G.C5010G_VOID_FL       IS NULL
		GROUP BY T5010g.C5010_Tag_Id ,
				 T5010g.C207_Set_Id,
				 T5010g.C207_Set_Sales_System_Id,
				 T5010g.C701_DISTRIBUTOR_ID; 
    
BEGIN
 
 FOR tagdtl IN tag_from_sys_locator
  LOOP
    BEGIN
	      
	    BEGIN
		    SELECT C701_Region 
		       INTO v_dist_region 
		      FROM T701_Distributor 
		      WHERE C701_Distributor_Id = tagdtl.distid;
		    EXCEPTION
		WHEN No_Data_Found THEN
		  v_dist_region     := NULL;
		END;
		
		-- Get the First date of the month before 4 months
		SELECT '01-'||TO_CHAR(last_day(add_months(CURRENT_DATE,-(4))),'MON-YY')
			INTO v_from_dt
		FROM dual;

		-- Get the Last date of the Previous month.
		SELECT Last_Day(Add_Months(Current_Date,-1))
			INTO v_to_dt
		FROM dual;
		
	  -- Get the Rep email and Phone from  DO classification logic for the tag id for last 4 month
        BEGIN
	        SELECT Rtrim(Xmlagg(Xmlelement(E,emailid
		    || ',')).Extract('//text()').Getclobval(),',') emailid ,
		    Rtrim(Xmlagg(Xmlelement(E, repphone
		    || ',')).Extract('//text()').Getclobval(),',') repphone
		  INTO v_rep_email_id ,
		    v_rep_phone_number
		  FROM
		    (SELECT repid ,
				  Repphone,
				  emailid
				FROM
				  (SELECT Do_Main.Repid repid,
				    Do_Main.Repphone Repphone,
				    Do_Main.emailid emailid,
				    FLOOR(COUNT(Do_Clas.do_clas_ord_id)/2.5) cnt
				  FROM
				    (SELECT C501_Order_Id do_clas_ord_id
				    FROM T501b_Order_By_System
				    WHERE C207_System_Id = tagdtl.systemid
				    AND C501B_VOID_FL   IS NULL
				    ) Do_Clas,
				    (SELECT T501.C501_Order_Id Do_Ord_Id,
				      T501.C703_Sales_Rep_Id Repid,
				      T703.C703_Email_Id emailid ,
				      T703.C703_Phone_Number repphone,
				      T703.C701_Distributor_Id Distid,
				      TO_CHAR(T501.C501_Order_Date,'mm/yyyy') Orddate
				    FROM T501_Order T501 ,
				      T703_Sales_Rep T703
				    WHERE T703.C703_Sales_Rep_Id  = T501.C703_Sales_Rep_Id
				    AND T703.C701_Distributor_Id IN
				      (SELECT C701_Distributor_Id
				      FROM T701_Distributor
				      WHERE C701_Region           = v_dist_region
				      AND NVL(c701_active_fl,'Y') ='Y'
				      )
				    AND t501.C501_ORDER_DATE BETWEEN (v_from_dt) AND (v_to_dt)-- Exclude current month and get last 4 months
				    AND T501.C501_Void_Fl    IS NULL
				    ) Do_Main
				  WHERE Do_Clas.Do_Clas_Ord_Id =Do_Main.Do_Ord_Id
				  GROUP BY Do_Main.Repid,
				    Do_Main.Emailid,
				    Do_Main.Repphone
				  ORDER BY Cnt DESC
				  )
				WHERE cnt >1
			);
		EXCEPTION
		WHEN No_Data_Found THEN
		  v_rep_email_id     := NULL;
		  v_rep_phone_number := NULL;
		END;
		  
		IF v_rep_email_id IS NOT NULL THEN
			--110700 - FROM DO ClASSIFICATION
			v_email_type := 110700;
		END IF;
		
		IF (v_rep_email_id IS NULL OR v_rep_email_id = ' ') THEN
			gm_pkg_inv_system_locator_txn.gm_process_tag_rep_dtls_from_order(tagdtl.tagid,v_dist_region,v_rep_email_id,v_rep_phone_number,v_email_type);
		END IF;
		
      --Pass tagid,distid,distnm and dist email id to save the details in T5010G_TAG_SYSTEM_LOCATOR table
      	gm_pkg_inv_system_locator_txn.gm_sav_system_locator_details(tagdtl.tagid,NULL,NULL,NULL,
      		v_rep_email_id,v_rep_phone_number,v_email_type,p_user_id);
      	 
    END;
END LOOP;
  
END gm_process_tag_rep_dtls_from_do_clas;


/*********************************************************************************************************************
  * Description : Procedure to Fetch rep email / Phone number details from Order and save in System Locator Table
  * Author : Karthiks
  * Dependency : Needs to run after Tag owner Job (T501d_Order_Tag_Usage_Details)
  *********************************************************************************************************************/   
  PROCEDURE gm_process_tag_rep_dtls_from_order(
  		 p_tag_id		  IN  T5010G_TAG_SYSTEM_LOCATOR.C5010_TAG_ID%TYPE,
    	 p_region_id      IN  T701_Distributor.C701_Region%TYPE,
    	 p_out_rep_email  OUT T5010G_TAG_SYSTEM_LOCATOR.C703_EMAIL_ID%TYPE,
    	 p_out_ph_no	  OUT T5010G_TAG_SYSTEM_LOCATOR.C703_PHONE_NUMBER%TYPE,
    	 p_out_email_type OUT T5010G_TAG_SYSTEM_LOCATOR.C703_PHONE_NUMBER%TYPE
  )
AS
v_rep_email_id T5010G_TAG_SYSTEM_LOCATOR.C703_EMAIL_ID%TYPE;
v_rep_phone_number T5010G_TAG_SYSTEM_LOCATOR.C703_PHONE_NUMBER%TYPE;
v_from_dt 	T501_Order.C501_ORDER_DATE%TYPE;
v_to_dt 	T501_Order.C501_ORDER_DATE%TYPE;
v_email_type t901_code_lookup.c901_code_id%TYPE := NULL;

BEGIN
		-- Get the First date of the month before 2 months
		SELECT '01-'||TO_CHAR(last_day(add_months(CURRENT_DATE,-(2))),'MON-YY')
			INTO v_from_dt
		FROM dual;

		-- Get the Last date of the Previous month.
		SELECT Last_Day(Add_Months(Current_Date,-1))
			INTO v_to_dt
		FROM dual;
		
	  -- Get the Rep email and Phone from the tag recorded on the order for last 2 month
      BEGIN
		  SELECT Rtrim(Xmlagg(Xmlelement(E,emailid|| ',')).Extract('//text()').Getclobval(),',')  emailid ,
		         Rtrim(Xmlagg(Xmlelement(E,repphone|| ',')).Extract('//text()').Getclobval(),',') repphone
		           INTO v_rep_email_id ,
		                v_rep_phone_number
		 	 FROM
			    (
				   SELECT T703.C703_Email_Id emailid,
					  T703.C703_Phone_Number repphone
					FROM T501d_Order_Tag_Usage_Details T501d ,
					  T501_Order T501,
					  T703_Sales_Rep t703
					WHERE T501.C501_Order_Id      = T501d.C501_Order_Id
					AND T501.C703_Sales_Rep_Id    = T703.C703_Sales_Rep_Id
					AND T501d.C501d_Tag_Owner_Id IN
					  (SELECT C701_Distributor_Id
					  FROM T701_Distributor
					  WHERE C701_Region           = p_region_id
					  AND NVL(c701_active_fl,'Y') ='Y'
					  )
					AND T501d.C5010_Tag_Id           = p_tag_id
					AND t501.C501_ORDER_DATE BETWEEN (v_from_dt) AND (v_to_dt)-- Exclude current month and get last 2 months
					AND NVL(T703.C703_Active_Fl,'Y') = 'Y'
					AND T501.C501_Void_Fl           IS NULL
					AND T501d.C501d_Void_Fl         IS NULL
					GROUP BY T703.C703_Email_Id,
					  T703.C703_Phone_Number
			    );
		  EXCEPTION
		  WHEN NO_DATA_FOUND THEN
		    v_rep_email_id     := NULL;
		    v_rep_phone_number := NULL;
		  END;
		  
		  IF v_rep_email_id IS NOT NULL THEN
			--110701 -FROM Tag recording on DO
 	 		 v_email_type	:= 110701;
		  END IF;
		  
		  p_out_rep_email 	:= v_rep_email_id;
		  p_out_ph_no 		:= v_rep_phone_number;
		  p_out_email_type  := v_email_type;
      	
END gm_process_tag_rep_dtls_from_order;


  /*********************************************************************************************************************
  * Description : Procedure to save details in t5010g_tag_system_locator table
  * Author : Karthiks
  *********************************************************************************************************************/   
  PROCEDURE gm_sav_system_locator_details(
  		 p_tag_id		  IN T5010G_TAG_SYSTEM_LOCATOR.C5010_TAG_ID%TYPE,
    	 p_set_id		  IN T5010G_TAG_SYSTEM_LOCATOR.C207_SET_ID%TYPE,
    	 p_system_id	  IN T5010G_TAG_SYSTEM_LOCATOR.C207_SET_SALES_SYSTEM_ID%TYPE,
    	 p_dist_id		  IN T5010G_TAG_SYSTEM_LOCATOR.C701_DISTRIBUTOR_ID%TYPE,
    	 p_rep_email_id	  IN T5010G_TAG_SYSTEM_LOCATOR.C703_EMAIL_ID%TYPE,
    	 p_rep_phone	  IN T5010G_TAG_SYSTEM_LOCATOR.C703_PHONE_NUMBER%TYPE,
    	 p_email_type	  IN T5010G_TAG_SYSTEM_LOCATOR.C901_EMAIL_TYPE%TYPE,
    	 p_user_id		  IN T5010G_TAG_SYSTEM_LOCATOR.C5010G_UPDATED_BY%TYPE
  )
  AS
  BEGIN
	  
   UPDATE T5010G_TAG_SYSTEM_LOCATOR
	SET C207_SET_ID            = NVL(p_set_id,C207_SET_ID) ,
	  C207_SET_SALES_SYSTEM_ID = NVL(p_system_id,C207_SET_SALES_SYSTEM_ID),
	  C701_DISTRIBUTOR_ID      = NVL(p_dist_id,C701_DISTRIBUTOR_ID),
	  C703_EMAIL_ID            = NVL(p_rep_email_id,C703_EMAIL_ID),
	  C703_PHONE_NUMBER        = NVL(p_rep_phone,C703_PHONE_NUMBER),
	  C901_EMAIL_TYPE          = NVL(p_email_type,C901_EMAIL_TYPE),
	  C5010G_UPDATED_BY        = p_user_id,
	  C5010G_UPDATED_DATE      = CURRENT_DATE
	WHERE C5010_Tag_Id         = p_tag_id
	AND C5010G_VOID_FL        IS NULL;
	
	IF SQL%ROWCOUNT            = 0 THEN
	  INSERT
	  INTO T5010G_TAG_SYSTEM_LOCATOR
	    (
	      C5010_TAG_ID,
	      C207_SET_ID,
	      C207_SET_SALES_SYSTEM_ID,
	      C701_DISTRIBUTOR_ID,
	      C703_EMAIL_ID,
	      C703_PHONE_NUMBER,
	      C901_EMAIL_TYPE,
	      C5010G_UPDATED_BY,
	      C5010G_UPDATED_DATE
	    )
	    VALUES
	    (
	      p_tag_id ,
	      p_set_id ,
	      p_system_id,
	      p_dist_id,
	      p_rep_email_id,
	      p_rep_phone,
	      p_email_type,
	      p_user_id,
	      CURRENT_DATE
	    );
	END IF;

END gm_sav_system_locator_details ;

/*********************************************************************************************************************
  * Description : Procedure to group the details from t5010g_tag_system_locator table and put into t5010h_system_locator_dtls
  * Author : Karthiks
  *********************************************************************************************************************/   
  PROCEDURE gm_process_system_loc_report_details(
   		 p_last_sync_date IN T5010G_TAG_SYSTEM_LOCATOR.C5010G_UPDATED_DATE%TYPE,
    	 p_user_id        IN VARCHAR2 	
  )
AS
 v_set_id    T5010g_Tag_System_Locator.C207_SET_ID%TYPE;
 v_system_id T5010g_Tag_System_Locator.C207_SET_SALES_SYSTEM_ID%TYPE;
 v_rep_email T5010g_Tag_System_Locator.C703_EMAIL_ID%TYPE:= NULL;
 v_rep_phone T5010g_Tag_System_Locator.C703_PHONE_NUMBER%TYPE:= NULL;
 v_rep_em T5010g_Tag_System_Locator.C703_EMAIL_ID%TYPE := NULL;
 v_rep_ph T5010g_Tag_System_Locator.C703_PHONE_NUMBER%TYPE := NULL;
 
CURSOR sys_loc_master
  IS
   SELECT SETID,
		  SYSTEMID,
		  DISTID,
		  Rtrim(Xmlagg(Xmlelement(E,REPEMAILID
		  ||',')).Extract('//text()').Getclobval(),',') REPEMAILID ,
		  Rtrim(Xmlagg(Xmlelement(E, REPPHONE
		  ||',')).Extract('//text()').Getclobval(),',') REPPHONE
		FROM
		  (SELECT t5010g.C207_SET_ID SETID,
		    t5010g.C207_SET_SALES_SYSTEM_ID SYSTEMID,
		    t5010g.C701_DISTRIBUTOR_ID DISTID,
		    t5010g.C703_EMAIL_ID REPEMAILID,
		    t5010g.C703_PHONE_NUMBER REPPHONE
		  FROM T5010g_Tag_System_Locator t5010g
		  WHERE T5010g.C5010g_Updated_Date >= NVL(p_last_sync_date,C5010g_Updated_Date)
		  AND T5010g.C5010g_Void_Fl IS NULL
		  GROUP BY t5010g.C207_SET_ID ,
		    t5010g.C207_SET_SALES_SYSTEM_ID ,
		    t5010g.C701_DISTRIBUTOR_ID ,
		    T5010g.C703_Email_Id ,
		    T5010g.C703_Phone_Number
		  )
		GROUP BY SETID,
		  SYSTEMID,
		  DISTID;
    
BEGIN
 
 FOR distdetail IN sys_loc_master
  LOOP
    BEGIN

	    v_rep_em := distdetail.REPEMAILID;
	    v_rep_ph := distdetail.REPPHONE;
	    
		--TO Remove Duplicates Email  
		BEGIN
		SELECT Rtrim(Xmlagg(Xmlelement(E,Email
		  ||',')).Extract('//text()').Getclobval(),',')
		  	INTO v_rep_email
		FROM
		  ( SELECT DISTINCT Email
		  FROM
		    (SELECT Regexp_Substr(v_rep_em,'([^\,]+)', 1, Level) Email
		    FROM DUAL
		      CONNECT BY regexp_substr(v_rep_em,'([^\,]+)', 1, level) IS NOT NULL
		    )
	 	 );
	 	  EXCEPTION
		  WHEN NO_DATA_FOUND THEN
		    v_rep_email     := NULL;
		 END;
		 
		 --TO Remove Duplicates Phone numbers  
		 BEGIN
		SELECT Rtrim(Xmlagg(Xmlelement(E,phone
		  ||',')).Extract('//text()').Getclobval(),',')
		  	INTO v_rep_phone
		FROM
		  ( SELECT DISTINCT phone
		  FROM
		    (SELECT Regexp_Substr(v_rep_ph,'([^\,]+)', 1, Level) phone
		    FROM DUAL
		      CONNECT BY regexp_substr(v_rep_ph,'([^\,]+)', 1, level) IS NOT NULL
		    )
	 	 );
	 	  EXCEPTION
		  WHEN NO_DATA_FOUND THEN
		    v_rep_phone     := NULL;
		 END;
		 
		 
      --Pass setid, system id and distributor id ,email and phone numbers to save the details in t5010h_system_locator_dtls table
      IF distdetail.SETID IS NOT NULL AND distdetail.DISTID IS NOT NULL THEN
      	gm_pkg_inv_system_locator_txn.gm_sav_sys_loc_dist_details(distdetail.DISTID,distdetail.SETID,distdetail.SYSTEMID,
      		v_rep_email,v_rep_phone,p_user_id);
      END IF;
      
    END;
END LOOP;
  
END gm_process_system_loc_report_details;

/*********************************************************************************************************************
  * Description : Procedure to save details in t5010h_system_locator_dtls table
  * Author : Karthiks
  *********************************************************************************************************************/   
  PROCEDURE gm_sav_sys_loc_dist_details(
  		 p_dist_id		  IN t5010h_system_locator_dtls.C701_DISTRIBUTOR_ID%TYPE,
    	 p_set_id		  IN t5010h_system_locator_dtls.C207_SET_ID%TYPE,
    	 p_system_id	  IN t5010h_system_locator_dtls.C207_SET_SALES_SYSTEM_ID%TYPE,
    	 p_rep_email_id	  IN t5010h_system_locator_dtls.C703_EMAIL_ID%TYPE,
    	 p_rep_phone	  IN t5010h_system_locator_dtls.C703_PHONE_NUMBER%TYPE,
    	 p_user_id		  IN t5010h_system_locator_dtls.C5010H_UPDATED_BY%TYPE
  )
  AS
  BEGIN
	  
   UPDATE T5010H_SYSTEM_LOCATOR_DTLS
	SET C703_EMAIL_ID          = p_rep_email_id,
	  C703_PHONE_NUMBER        = p_rep_phone,
	  C5010H_UPDATED_BY        = p_user_id,
	  C5010H_UPDATED_DATE      = CURRENT_DATE
	WHERE C701_DISTRIBUTOR_ID  	 = p_dist_id
	AND C207_SET_ID 			 = p_set_id
	AND C207_SET_SALES_SYSTEM_ID = p_system_id
	AND C5010H_VOID_FL        IS NULL;
	
	IF SQL%ROWCOUNT            = 0 THEN
	  INSERT
	  INTO T5010H_SYSTEM_LOCATOR_DTLS
	    (
	      C701_DISTRIBUTOR_ID,
	      C207_SET_ID,
	      C207_SET_SALES_SYSTEM_ID,
	      C703_EMAIL_ID,
	      C703_PHONE_NUMBER,
	      C5010H_UPDATED_BY,
	      C5010H_UPDATED_DATE
	    )
	    VALUES
	    (
	      p_dist_id,
	      p_set_id ,
	      p_system_id,
	      p_rep_email_id,
	      p_rep_phone,
	      p_user_id,
	      CURRENT_DATE
	    );
	END IF;

END gm_sav_sys_loc_dist_details ;

/*********************************************************************************************************************
  * Description : Procedure to update the tag share options in t5010h_system_locator_dtls table 
  * Author : Karthiks
  *********************************************************************************************************************/   
  PROCEDURE gm_process_tag_share_options(
   		 p_last_sync_date IN T5010G_TAG_SYSTEM_LOCATOR.C5010G_UPDATED_DATE%TYPE,
    	 p_user_id        IN VARCHAR2 	
  )
AS
CURSOR sys_loc_master
  IS
   SELECT SETID,
		  SYSTEMID,
		  DISTID,
 		  MIN(TAGSHAREOPTION) TAGSHAREOPTIONID
		FROM
		  (SELECT t5010g.C207_SET_ID SETID,
		    t5010g.C207_SET_SALES_SYSTEM_ID SYSTEMID,
		    t5010g.C701_DISTRIBUTOR_ID DISTID,
		    t5010g.C703_EMAIL_ID REPEMAILID,
		    t5010g.C703_PHONE_NUMBER REPPHONE,
		    T5010g.C901_TAG_SHARE_OPTIONS TAGSHAREOPTION
		  FROM T5010g_Tag_System_Locator t5010g
		  WHERE T5010g.C5010g_Void_Fl IS NULL
		  GROUP BY t5010g.C207_SET_ID ,
		    t5010g.C207_SET_SALES_SYSTEM_ID ,
		    t5010g.C701_DISTRIBUTOR_ID ,
		    T5010g.C703_Email_Id ,
		    T5010g.C703_Phone_Number,
		    T5010g.C901_TAG_SHARE_OPTIONS
		  )
		GROUP BY SETID,
		  SYSTEMID,
		  DISTID;
    
BEGIN
 
 FOR distdetail IN sys_loc_master
  LOOP
    BEGIN
      --Pass setid, system id and distributor id and update the tag share option in t5010h_system_locator_dtls table
      IF distdetail.SETID IS NOT NULL AND distdetail.DISTID IS NOT NULL THEN
      	 UPDATE T5010H_SYSTEM_LOCATOR_DTLS
			SET C901_TAG_SHARE_OPTIONS = distdetail.TAGSHAREOPTIONID,
			  C5010H_UPDATED_BY        = p_user_id,
			  C5010H_UPDATED_DATE      = CURRENT_DATE
			WHERE C701_DISTRIBUTOR_ID  	 = distdetail.DISTID
			AND C207_SET_ID 			 = distdetail.SETID
			AND C207_SET_SALES_SYSTEM_ID = distdetail.SYSTEMID
			AND C5010H_VOID_FL        IS NULL;
      END IF;
    END;
END LOOP;
  
END gm_process_tag_share_options;

END gm_pkg_inv_system_locator_txn;
/ 