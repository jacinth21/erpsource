/* Formatted on 2011/11/18 11:27 (Formatter Plus v4.8.0) */
--@"C:\Database\Packages\Sales\invmanagement\gm_pkg_sm_tagset_txn.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_sm_tagset_txn
IS
--
/*********************************
   Purpose: This procedure is used to save tag current location.
**********************************/
--
	PROCEDURE gm_sav_tagset_inv_location (
		p_strtagids 	 IN   VARCHAR2
	  , p_sub_loc_type	 IN   t5010_tag.c901_sub_location_type%TYPE
	  , p_sub_loc_id	 IN   t5010_tag.c5010_sub_location_id%TYPE
	  , p_addressid 	 IN   t5010_tag.c106_address_id%TYPE
	  , p_accountid 	 IN   t5010_tag.c704_account_id%TYPE
	  , p_userid		 IN   t5010_tag.c5010_last_updated_by%TYPE
	)
	AS
		v_strtagids    VARCHAR2 (1000) := p_strtagids || ',';
		v_tag_id	   t5010a_tag_attribute.c5010_tag_id%TYPE;
		v_errtagids    VARCHAR2 (1000) := '';
		v_count        NUMBER := 0;
		
	BEGIN
		/*
		WHILE INSTR (v_strtagids, ',') <> 0
		LOOP
			v_tag_id	:= SUBSTR (v_strtagids, 1, INSTR (v_strtagids, ',') - 1);
			v_strtagids := SUBSTR (v_strtagids, INSTR (v_strtagids, ',') + 1);
				
				
					 SELECT COUNT (*) 
					   INTO v_count
		   			   FROM t5010_tag
		              WHERE c5010_tag_id = v_tag_id
		                AND c5010_void_fl IS NULL
		    			AND c5010_location_id IN
						    (
						         SELECT D_ID
						           FROM v700_territory_mapping_detail
						          WHERE DECODE (p_sub_loc_type, 50221, D_ID, 50222, REP_ID, 11301, AC_ID) = p_sub_loc_id
						    ) ;
						  
			IF v_count = 0 THEN
				v_errtagids := v_errtagids || v_tag_id || ',';
			END IF;
	
		END LOOP;
		
		--validation for set can transfer among the same distributor's Sales Reps only.	
		IF LENGTH (v_errtagids) > 0 THEN
			v_errtagids := SUBSTR (v_errtagids, 1, LENGTH (v_errtagids)-1);
			GM_RAISE_APPLICATION_ERROR('-20999','355',v_errtagids);		
			
		END IF;
		*/	
		
		my_context.set_my_inlist_ctx (p_strtagids);
		
		UPDATE t5010_tag
		   SET c901_sub_location_type = p_sub_loc_type
			 , c5010_sub_location_id = p_sub_loc_id
			 , c704_account_id = NVL (p_accountid,c704_account_id)
			 , c106_address_id =  DECODE (p_sub_loc_type, 50222, p_addressid, null)
			 , c5010_last_updated_by = p_userid
			 , c5010_last_updated_date = SYSDATE
		 WHERE c5010_tag_id IN (SELECT token FROM v_in_list) 
		   AND c5010_void_fl IS NULL;

	END gm_sav_tagset_inv_location;

/*********************************
Purpose: This procedure is used to save tag attribute.
**********************************/
--
	PROCEDURE gm_sav_tagset_attribute (
		p_strtagids    IN	VARCHAR2
	  , p_attr_type    IN	t5010a_tag_attribute.c901_attribute_type%TYPE
	  , p_attr_value   IN	t5010a_tag_attribute.c5010a_attribute_value%TYPE
	  , p_accountid    IN	t5010_tag.c5010_sub_location_id%TYPE
	  , p_canceltype   IN	t901_code_lookup.c902_code_nm_alt%TYPE
	  , p_reason	   IN	t907_cancel_log.c901_cancel_cd%TYPE
	  , p_comments	   IN	t907_cancel_log.c907_comments%TYPE
	  , p_userid	   IN	t5010a_tag_attribute.c5010a_last_updated_by%TYPE
	)
	AS
		v_strtagids    VARCHAR2 (1000) := p_strtagids || ',';
		v_tag_id	   t5010a_tag_attribute.c5010_tag_id%TYPE;
		v_cancelid	   NUMBER;
		v_company_id    t1900_company.c1900_company_id%TYPE;
	BEGIN
		SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL')) INTO v_company_id FROM dual; 
		WHILE INSTR (v_strtagids, ',') <> 0
		LOOP
			v_tag_id	:= SUBSTR (v_strtagids, 1, INSTR (v_strtagids, ',') - 1);
			v_strtagids := SUBSTR (v_strtagids, INSTR (v_strtagids, ',') + 1);

			UPDATE t5010a_tag_attribute
			   SET c5010a_attribute_value = p_attr_value
				 , c5010a_last_updated_by = p_userid
				 , c5010a_last_updated_date = SYSDATE
			 WHERE c5010_tag_id = v_tag_id AND c901_attribute_type = p_attr_type AND c5010a_void_fl IS NULL;

			IF (SQL%ROWCOUNT = 0)
			THEN
				INSERT INTO t5010a_tag_attribute
							(c5010a_tag_attribute_id, c5010_tag_id, c901_attribute_type, c5010a_attribute_value
						   , c5010a_created_by, c5010a_created_date
							)
					 VALUES (s5010a_tag_attribute.NEXTVAL, v_tag_id, p_attr_type, p_attr_value
						   , p_userid, SYSDATE
							);
			END IF;

			SELECT s907_cancel_log.NEXTVAL
			  INTO v_cancelid
			  FROM DUAL;

			INSERT INTO t907_cancel_log
						(c907_cancel_log_id, c907_ref_id, c907_comments, c901_cancel_cd, c901_type, c907_created_by
					   , c907_created_date, c907_cancel_date, c1900_company_id
						)
				 VALUES (v_cancelid, v_tag_id, p_comments, p_reason, p_attr_type, p_userid
					   , SYSDATE, TRUNC (SYSDATE), v_company_id
						);

			IF p_attr_type IN (11305, 11310) AND p_attr_value = 'Y'  --lock/decommission set
			THEN
				gm_sav_tag_dealloc (v_tag_id, p_attr_type, p_accountid);
			END IF;
		END LOOP;

		--update tag with account in t5010_tag table
		IF (p_attr_type = '11305' AND p_attr_value = 'Y')
		THEN
			gm_sav_tagset_inv_location (p_strtagids,'11301', p_accountid,'',p_accountid, p_userid);	-- 11301(account)- sub location type
		END IF;
	END gm_sav_tagset_attribute;

/*********************************
Purpose: This procedure is used to deallocat tag in T7104 table.
**********************************/
	PROCEDURE gm_sav_tag_dealloc (
		p_tag_id	  IN   t7104_case_set_information.c5010_tag_id%TYPE
	  , p_attr_type   IN   t5010a_tag_attribute.c901_attribute_type%TYPE
	  , p_accountid   IN   t5010_tag.c5010_sub_location_id%TYPE
	)
	AS
		CURSOR c_alloc_tags
		IS
			SELECT t7104.c7104_case_set_id casesetid
			  FROM t7104_case_set_information t7104
			 WHERE TRUNC (t7104.c7104_tag_lock_from_date) > TRUNC (SYSDATE)
			   AND get_case_acct_id (NULL, t7104.c7104_case_set_id) != DECODE (p_attr_type, 11305, p_accountid, '-999')
			   AND t7104.c5010_tag_id = p_tag_id;

		v_str		   VARCHAR2 (4000);
	BEGIN
		FOR v_rec IN c_alloc_tags
		LOOP
			v_str		:= v_str || v_rec.casesetid || ',';
		END LOOP;

		my_context.set_my_inlist_ctx (v_str);

		UPDATE t7104_case_set_information
		   SET c5010_tag_id = ''
			 , c7104_last_updated_by = 30301
			 , c7104_last_updated_date = SYSDATE
		 WHERE c7104_case_set_id IN (SELECT token
									   FROM v_in_list);
	END gm_sav_tag_dealloc;
--
END gm_pkg_sm_tagset_txn;
/
