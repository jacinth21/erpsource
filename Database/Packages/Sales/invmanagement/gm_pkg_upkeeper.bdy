--@"C:\Database\Packages\Sales\invmanagement\gm_pkg_upkeeper.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_upkeeper
AS
	/*********************************
	Purpose: This procedure is used to fetch loaner returned records from Loaner Transaction where Placement date is updated in upkeeper.
	Get last log data (t50402_loaner_tag_history_log) for the Loaner Transaction and update Return date with comments. Upkeeper Loaner History (t50401_loaner_tag_history) will be updated by the trigger automatically.
	**********************************/
	PROCEDURE gm_sc_upk_missing_return_load_main
	AS
		CURSOR cur_rtn IS
		SELECT t504a.c504a_return_dt, t50401.c50401_return_date , t504a.c504a_loaner_transaction_id
			FROM t50401_loaner_tag_history t50401,  t504a_loaner_transaction t504a
		WHERE t50401.c504a_loaner_transaction_id  = t504a.c504a_loaner_transaction_id
			AND t50401.c50401_return_date IS NULL
			AND t504a.c504a_return_dt IS NOT NULL
			AND t50401.c50401_void_fl IS NULL
			AND t504a.c504a_void_fl IS NULL;
		BEGIN
			FOR rtn IN cur_rtn
			LOOP 
				BEGIN

					INSERT INTO t50402_loaner_tag_history_log (
						c50402_loaner_tag_history_log_id,
						c504a_loaner_transaction_id,
						c701_distributor_id,
						c50402_orig_placement_date,
						c50402_orig_return_date,
						c50402_adj_placement_date,
						c50402_adj_return_date,
						c50402_remove_fl,
						c50402_disputed_fl,
						c50402_void_fl,
						c50402_pathlink,
						c50402_comments,
						c50402_update_group_desc,
						c50402_last_updated_by,
						c50402_last_updated_date,
						c50402_last_updated_date_utc,
						c50402_orig_pl_date_txt,
						c50402_orig_ret_date_txt,
						c50402_adj_pl_date_txt,
						c50402_adj_ret_date_txt,
						c50402_last_updated_date_txt
					)
					SELECT
						s50402_loaner_tag_history_log.NEXTVAL,
						c504a_loaner_transaction_id,
						c701_distributor_id,
						c50402_orig_placement_date,
						c50402_orig_return_date,
						c50402_adj_placement_date,
						rtn.c504a_return_dt,
						c50402_remove_fl,
						c50402_disputed_fl,
						c50402_void_fl,
						NULL,
						'Return date updated as '
						|| rtn.c504a_return_dt
						|| ' by Job',
						c50402_update_group_desc,
						30301,
						current_date,
						NULL,
						c50402_orig_pl_date_txt,
						c50402_orig_ret_date_txt,
						c50402_adj_pl_date_txt,
						TO_CHAR(rtn.c504a_return_dt, 'MM/dd/yyyy'),
						TO_CHAR(current_date, 'MM/dd/yyyy')
					FROM
						t50402_loaner_tag_history_log
					WHERE
						c50402_loaner_tag_history_log_id IN (
							SELECT
								MAX(c50402_loaner_tag_history_log_id)
							FROM
								t50402_loaner_tag_history_log
							WHERE
								c504a_loaner_transaction_id = rtn.c504a_loaner_transaction_id
								AND c50402_void_fl IS NULL
						)
						AND c50402_void_fl IS NULL;
				   
					--dbms_output.put_line('rtn.c504a_loaner_transaction_id='|| rtn.c504a_loaner_transaction_id);

				EXCEPTION
					WHEN OTHERS THEN
						--dbms_output.put_line('Return Date update Error'|| rtn.c504a_loaner_transaction_id);
						gm_ins_status_info('Error in executing procedute gm_sc_upk_missing_return_load_main, c504a_loaner_transaction_id:'
										   || rtn.c504a_loaner_transaction_id
										   || ',Error:'
										   || sqlerrm);
				END;
			END LOOP;
			COMMIT;
	END gm_sc_upk_missing_return_load_main;
	
	PROCEDURE gm_ins_status_info (
        p_error_msg t910_load_log_table.c910_status_details%TYPE
    ) AS
    BEGIN
        INSERT INTO t910_load_log_table (
            c910_load_log_id,
            c910_log_name,
            c910_state_dt,
            c910_end_dt,
            c910_status_details,
            c910_status_fl
        ) VALUES (
            s910_load_log.NEXTVAL,
            'gm_pkg_upkeeper',
            SYSDATE,
            SYSDATE,
            p_error_msg,
            'F'
        );

    END gm_ins_status_info;

END gm_pkg_upkeeper;
/