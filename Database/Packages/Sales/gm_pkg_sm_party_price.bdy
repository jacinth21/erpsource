--@"C:\oneportal\db\Packages\Sales\gm_pkg_sm_party_price.pkg";

CREATE OR REPLACE PACKAGE BODY gm_pkg_sm_party_price
IS
/******************************************************************
	  Description : This procedure is used update insert account price
	 ****************************************************************/
PROCEDURE gm_sav_acct_party_price (
	p_party_id   IN   t705_account_pricing.c101_party_id%TYPE,
	p_price       IN   t705_account_pricing.c705_unit_price%TYPE,
    p_discount     IN   t705_account_pricing.c705_discount_offered%TYPE,
    p_partnum   IN   t705_account_pricing.c205_part_number_id%TYPE,
    p_validate_pnum IN VARCHAR2 ,
    p_userid      IN   t705_account_pricing.c705_last_updated_by%TYPE
   )
   AS
   	v_projid	   t202_project.c202_project_id%TYPE;

   BEGIN
      
		UPDATE t705_account_pricing
		   SET c705_unit_price = p_price
			 , c705_discount_offered = p_discount
			 , c705_last_updated_by = p_userid
			 , c705_last_updated_date = CURRENT_DATE
			 , c705_price_exported_fl = NULL   --reset price exported flag to null
			 , c7501_account_price_req_dtl_id = NULL
			-- , c705_void_fl = NULL -- PC-5504 
		 WHERE c205_part_number_id = p_partnum AND c101_party_id = p_party_id
		   AND c705_void_fl IS NULL; 
		  -- PC-5504 - add void fl condition and removed void fl from update because if add price for voided part , new record with the price should be added
		 
	IF (SQL%ROWCOUNT = 0)
	THEN
	IF p_validate_pnum = 'YES' 
		THEN 
			SELECT get_project_id_from_part (p_partnum)
			  INTO v_projid
			  FROM DUAL;
	
			IF v_projid = ' '
			THEN
				raise_application_error (-20886, '');
			END IF;	
	END IF;
	INSERT INTO t705_account_pricing
						(c705_account_pricing_id, c101_party_id, c205_part_number_id, c705_unit_price
					   , c705_discount_offered, c202_project_id, c705_created_date, c705_created_by
					   , C705_PRICE_EXPORTED_FL 
						)
				 VALUES (s704_price_account.NEXTVAL, p_party_id, p_partnum, p_price
					   , p_discount, get_project_id_from_part (p_partnum), CURRENT_DATE, p_userid
					   ,null
						);
	
	END IF;
	END gm_sav_acct_party_price;
/******************************************************************
	  Description : This procedure is used fetch Parent Rep Info for AJAX Call
	 ****************************************************************/
PROCEDURE gm_fch_parent_rep_acct_info (
	p_accid 	  IN	   t705_account_pricing.c704_account_id%TYPE,
	p_parentrepinfo_cur   OUT	   TYPES.cursor_type
   )
   AS
   v_party_id        t705_account_pricing.c101_party_id%TYPE;
   v_company_id  t101_party.C1900_COMPANY_ID%TYPE;
  BEGIN
	  
	   SELECT NVL(get_compid_frm_cntx (),GET_RULE_VALUE('DEFAULT_COMPANY','ONEPORTAL'))
	     INTO v_company_id 
	     FROM DUAL;
	  
	  SELECT c101_party_id
	  	INTO v_party_id
	  FROM t704_account 
	  WHERE c704_account_id =p_accid
	  and c1900_company_id = v_company_id
	  AND c704_void_fl is NULL;
	  
	   OPEN p_parentrepinfo_cur
	 FOR
		SELECT t101.c101_party_nm parentacctnm
			    , t704.c704_account_nm repacnm
			    ,t704.c704_account_id repacid 
			    ,d_name dname
			    ,rep_name rpname
	    FROM v700_territory_mapping_detail v700,  t704_account t704,t101_party t101
	    WHERE  v700.ac_id = t704.c704_account_id 
	    AND  t704.c101_party_id  = t101.c101_party_id
	    AND t704.c704_void_fl IS NULL 
	    AND t704.c101_party_id   =v_party_id
	     AND t101.c101_void_fl IS NULL;
 END gm_fch_parent_rep_acct_info; 
 
/**************************************************
Description : Fetches the internal Rep List-BBA 
***************************************************/
PROCEDURE gm_fch_Internal_Act_Rep (
	p_internal_rep_type   IN	   t101_party.c101_party_nm%TYPE, 
	p_internal_rep_info   OUT	   TYPES.cursor_type
   ) 
AS
   v_rule_value t906_rules.c906_rule_value%type;
   v_company_id t1900_company.c1900_company_id%TYPE;
BEGIN 
	SELECT get_compid_frm_cntx() INTO v_company_id FROM dual;
	  OPEN p_internal_rep_info
	    FOR
           SELECT c906_rule_id CODEID, c906_rule_value CODENM  FROM t906_rules
               WHERE c906_rule_grp_id = 'BBA_INT_REP'          
               AND c1900_company_id   = v_company_id
               AND c906_void_fl       IS NULL;

END gm_fch_Internal_Act_Rep;
END gm_pkg_sm_party_price;
/
