/* Formatted on 2011/10/28 18:09 (Formatter Plus v4.8.0) */
-- @"C:\Database\Packages\Sales\gm_pkg_sm_eventbook_txn.bdy" ;

CREATE OR REPLACE PACKAGE BODY gm_pkg_sm_eventbook_txn
IS
	/******************************************************************************************
	  * Procedure to Save the New Event Information
	  * HReddi
	*******************************************************************************************/   
	PROCEDURE gm_sav_event_info (
		p_info_id				 IN 	  t7100_case_information.c7100_case_info_id%TYPE
	  , p_event_name 			 IN 	  t7103_schedule_info.c7103_name%TYPE
	  , p_pur_of_event 			 IN 	  t7103_schedule_info.c901_purpose%TYPE
	  , p_start_date 			 IN 	  t7103_schedule_info.c7103_start_date%TYPE
	  , p_end_date 				 IN 	  t7103_schedule_info.c7103_end_date%TYPE
	  , p_venue 				 IN 	  t7103_schedule_info.c7103_venue%TYPE
	  , p_loan_to 				 IN 	  t7103_schedule_info.c901_loan_to%TYPE
	  , p_loan_to_id 			 IN 	  t7103_schedule_info.c101_loan_to_id%TYPE
	  , p_material_req 			 IN 	  VARCHAR2
	  , p_favorite_id 			 IN 	  VARCHAR2	 
	  , p_user_id 				 IN 	  VARCHAR2
	  , p_out_event_info_id	 	 OUT	  t7100_case_information.c7100_case_info_id%TYPE 
	)
	AS
		v_event_id	   VARCHAR2 (100) := NULL;
		v_seq		   NUMBER;
		v_count		   NUMBER;
		v_con_to		t504a_loaner_transaction.c901_consigned_to%TYPE;
		v_req_for_id    t525_product_request.c525_request_for_id%TYPE;
		v_rep_id        t525_product_request.c703_sales_rep_id%TYPE;
		v_company_id  t1900_company.c1900_company_id%TYPE;
		v_plant_id T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
		v_loan_to_org  t7103_schedule_info.c901_loan_to_org%TYPE;
		v_loan_to     t7103_schedule_info.c901_loan_to%TYPE;
	BEGIN
		p_out_event_info_id := p_info_id;
		v_loan_to_org := p_loan_to;
		select get_compid_frm_cntx(),get_plantid_frm_cntx() into v_company_id,v_plant_id from dual;
		SELECT DECODE(p_loan_to,19523,19518,p_loan_to) INTO v_loan_to FROM dual;
		-- Validate Loan to ID (Check the sales rep id before create event)
		IF (v_loan_to = '19518') THEN
		BEGIN                                                          
			SELECT c703_sales_rep_id 
			  INTO v_rep_id 
			  FROM t703_sales_rep 
			 WHERE c703_sales_rep_id = p_loan_to_id 
			   AND c703_void_fl IS NULL 
			   AND c703_active_fl = 'Y';   
			EXCEPTION 
			  WHEN NO_DATA_FOUND
			  THEN
			    GM_RAISE_APPLICATION_ERROR('-20999','363','');				
			END;          
		END IF;
		
		-- Validate event before updating
		gm_validate_event(p_out_event_info_id,'saveevent');
		
		-- Get the Sequence for New Event Creation
		gm_fch_next_event_key (p_start_date, '1006504', v_event_id);  --1006504 Event type
			
        -- Updating event Info for existing case Info ID 
		UPDATE t7100_case_information
		   SET c7100_case_id = DECODE(p_out_event_info_id,null,v_event_id,c7100_case_id) 	
		     , c901_case_status = NVL(DECODE(p_material_req,',',19534,c901_case_status),19534) -- incomplete
		     , c901_type = 1006504	-- Event
		     , c7100_last_updated_by = p_user_id
		     , c7100_last_updated_date = CURRENT_DATE
		 WHERE c7100_case_info_id 	= p_out_event_info_id 
		   AND c7100_void_fl IS NULL AND c901_type = 1006504;
		 
		-- Inserting new record if no record updated from above.
		IF (SQL%ROWCOUNT = 0)
		THEN
			SELECT s7100_case_information.NEXTVAL
			  INTO p_out_event_info_id
			  FROM DUAL; 
			  
			INSERT INTO t7100_case_information
						(c7100_case_info_id, c7100_case_id 				    
					  , c7100_created_by, c7100_created_date, c901_case_status , c901_type,c1900_company_id
						)
				 VALUES (p_out_event_info_id, v_event_id					  
					   , p_user_id, CURRENT_DATE, 19534, 1006504,v_company_id
						);  
		END IF;		
		
		 -- Updating event Info for existing case Info ID.
		 -- Since t7103_schedule_info is an extension of t7100_case_information, Void flag check is not there
		UPDATE t7103_schedule_info 
		   SET c7103_name		  = p_event_name
		     , c7103_start_date   = DECODE(p_start_date,null,c7103_start_date,p_start_date)
		     , c7103_end_date	  = DECODE(p_end_date,null,c7103_end_date,p_end_date)
		     , c901_purpose		  = p_pur_of_event
		     , c7103_venue        = p_venue
		     , c901_loan_to		  = v_loan_to
		     , c101_loan_to_id	  = p_loan_to_id
		     , c901_loan_to_org   = v_loan_to_org
		 WHERE c7100_case_info_id = p_out_event_info_id;
		 
		-- Inserting new record if no record updated from above. 
		IF (SQL%ROWCOUNT = 0)
		THEN
		
		SELECT COUNT(*) INTO v_count
			FROM t7103_schedule_info t7103,
			  t7100_case_information t7100
			WHERE t7103.c7100_case_info_id = t7100.c7100_case_info_id
			AND t7100.c901_case_status  IN ('19524') -- Active
			AND t7103.c7103_name        = p_event_name
			AND t7103.c1900_company_id  = v_company_id
			AND t7103.c1900_company_id  = t7100.c1900_company_id
			AND t7100.c7100_void_fl     IS NULL;
		
			   IF v_count > 0 
	  		   THEN
	  		   		GM_RAISE_APPLICATION_ERROR('-20999','364','');
	  		   END IF;
		
		
			INSERT INTO t7103_schedule_info
						(c7100_case_info_id,c7103_name, c7103_start_date, c7103_end_date
					   , c901_purpose, c7103_venue, c901_loan_to, c901_loan_to_org
					   , c101_loan_to_id,c1900_company_id 
						)
				 VALUES (p_out_event_info_id,p_event_name, p_start_date,  p_end_date
					   , p_pur_of_event, p_venue, v_loan_to, v_loan_to_org
					   , p_loan_to_id,v_company_id
						);  
		END IF; 
		
		-- to update loaner transaction record when event edited 
		BEGIN
		 SELECT DECODE (t7103.c901_loan_to, 19518, 50170, 50172), DECODE (t7103.c901_loan_to, 19518, get_distributor_id (
		    t7103.c101_loan_to_id), t7103.c101_loan_to_id), DECODE (t7103.c901_loan_to, 19518, t7103.c101_loan_to_id, '')
		   INTO v_con_to, v_req_for_id, v_rep_id
		   FROM t7103_schedule_info t7103
		  WHERE t7103.c7100_case_info_id = p_out_event_info_id;
        EXCEPTION 
          WHEN NO_DATA_FOUND
          THEN
          v_con_to := NULL;
          v_req_for_id := NULL;
          v_rep_id := NULL;
        END;
  
		 UPDATE t504a_loaner_transaction
		SET c901_consigned_to                   = v_con_to, c504a_consigned_to_id = v_req_for_id, c703_sales_rep_id = v_rep_id,
		    c5040_plant_id =v_plant_id
		  WHERE c526_product_request_detail_id IN
		    (SELECT c526_product_request_detail_id
		       FROM t7104_case_set_information
		      WHERE c7100_case_info_id = p_out_event_info_id
		        AND c7104_void_fl     IS NULL
		    )
		    AND c504a_void_fl IS NULL; 
        -- end loaner transaction updation
  END gm_sav_event_info;
    
   /******************************************************************************************
	* Description: Logic to create an Event ID based on the start date of the event  
	* Author: VPrasath
	*******************************************************************************************/        
    PROCEDURE gm_fch_next_event_key(
      p_start_date IN  t7103_schedule_info.c7103_start_date%TYPE
    , p_type       IN  t7100_case_information.c901_type%TYPE      
    , p_event_id   OUT t7100_case_information.c7100_case_id%TYPE 
    )
    AS
    v_seq		NUMBER;
    BEGIN
	    -- Get the Sequence for New Event Creation
		SELECT  NVL (MAX(TO_NUMBER (SUBSTR (c7100_case_id,12))), 0) +1 			
		  INTO v_seq
		  FROM t7100_case_information
		 WHERE c7100_case_id LIKE '%-' || TO_CHAR (p_start_date, 'yyyymmdd') || '%' 
		   AND c7100_void_fl IS NULL 
		   AND c901_type = p_type;  	
		
		 -- Event ID Allotment
		p_event_id	:=
			   'E-'			
			|| TO_CHAR (p_start_date, 'yyyymmdd')
			|| '-'
			|| v_seq;
	END gm_fch_next_event_key;    

   /******************************************************************************************
	* Description: Validate Event   
	* Author: VPrasath
	*******************************************************************************************/        
    PROCEDURE gm_validate_event(
       p_info_id IN t7100_case_information.c7100_case_info_id%TYPE,
       p_status IN VARCHAR2
    )
    AS
   		v_status		t7100_case_information.c901_case_status%TYPE;
		v_void_fl		t7100_case_information.c7100_void_fl%TYPE;
		v_start_date	t7103_schedule_info.c7103_start_date%TYPE;
    BEGIN
	    --Validate before update 
		IF p_info_id IS NOT NULL THEN		
			 BEGIN
				SELECT c901_case_status, c7100_void_fl, t7103.c7103_start_date
				  INTO v_status, v_void_fl, v_start_date
				  FROM t7100_case_information t7100, t7103_schedule_info t7103
				 WHERE t7100.c7100_case_info_id = p_info_id
		  		   AND t7100.c901_type = 1006504 -- Event
		  		   AND t7100.c7100_case_info_id = t7103.c7100_case_info_id;
		  	 EXCEPTION WHEN NO_DATA_FOUND THEN
		  			v_status := NULL;
		  			v_void_fl := NULL;
		  			v_start_date := CURRENT_DATE;
	  		 END;
	  		   
	  		   -- if event is voided ( need to replace the error code from -20999)
	  		   IF v_void_fl IS NOT NULL
	  		   THEN
	  		   		GM_RAISE_APPLICATION_ERROR('-20999','365','');
	  		   END IF;
	  		   
	  		   -- If status is (19525)cancelled or (19527)closed ( need to replace the error code from -20999)
	  		   IF v_status = 19525 OR  v_status = 19527
	  		   THEN
	  		   		GM_RAISE_APPLICATION_ERROR('-20999','366','');
	  		   END IF;
	  		   
	  		   -- If start date is crossed ( need to replace the error code from -20999)
	  		   IF TRUNC(v_start_date) < TRUNC(CURRENT_DATE) AND p_status <> 'reschedule'
	  		   THEN
	  		   		GM_RAISE_APPLICATION_ERROR('-20999','367','');
	  		   END IF;
		END IF;
	END gm_validate_event;    
	
	/******************************************************************************************
	  * Get the Event ID that is currently active, and not rescheduled
	  * VPrasath
	*******************************************************************************************/	
	FUNCTION get_event_info_id(
		p_info_id	IN   t7100_case_information.c7100_case_info_id%TYPE
	)
	RETURN NUMBER
	IS
	v_info_id NUMBER;
	BEGIN
		BEGIN
			   SELECT t7100.c7100_case_info_id 
		         INTO v_info_id
		         FROM t7100_case_information t7100 
                WHERE t7100.c901_case_status != 19526 --  19526 - Rescheduled
                  AND t7100.c7100_void_fl IS NULL 
                  --AND t7100.c901_type = 1006504 
	       START WITH t7100.c7100_case_info_id = p_info_id
     CONNECT BY PRIOR t7100.c7100_case_info_id = t7100.c7100_parent_case_info_id;
     EXCEPTION WHEN OTHERS
     THEN
     	v_info_id := p_info_id;
     END;
     RETURN v_info_id;
	END get_event_info_id;	
   /******************************************************************************************
	  * Procedure to Fetch the Event Information
	  * HReddi
	*******************************************************************************************/   
    PROCEDURE  gm_fch_event_info (
		p_info_id				 IN   t7100_case_information.c7100_case_info_id%TYPE
	  , p_user_id 				 IN   VARCHAR2	
	  ,	p_event_info_cur		 OUT  TYPES.cursor_type
	)AS
		vcount					NUMBER;
		v_product_req_id		NUMBER;
		v_status_fl             NUMBER;
		v_date_fmt				VARCHAR2(20);
		v_info_id				t7100_case_information.c7100_case_info_id%TYPE;
		v_company_id   t1900_company.c1900_company_id%TYPE;
	BEGIN
		select get_compdtfmt_frm_cntx() into v_date_fmt from dual;
		 SELECT get_compid_frm_cntx()
            INTO v_company_id
            FROM DUAL;
         v_info_id := get_event_info_id(p_info_id);
		
		OPEN p_event_info_cur FOR 
			 SELECT t7100.c7100_case_id eventid
				 , t7103.c7103_name eventname
     			 , t7103.c901_purpose eventpurpose
     			 , get_code_name(t7103.c901_purpose) purpose
     			 , TO_CHAR(t7103.c7103_start_date, v_date_fmt) eventstartdate
     			 , TO_CHAR(t7103.c7103_end_date, v_date_fmt) eventenddate
     			 , TO_CHAR(get_business_day(t7103.c7103_end_date , 2), v_date_fmt) expreturndate
     			 , TO_CHAR(get_business_day(t7103.c7103_end_date , 2), v_date_fmt) expectedreturndt
     			 , t7103.c7103_venue eventvenue
     			 , t7103.c901_loan_to_org eventloanto
     			 , get_code_name(t7103.c901_loan_to_org) loantoType
     			 , t7103.c101_loan_to_id eventloantoname
     			 , DECODE(
     			     t7103.c901_loan_to
     			     ,'19518', get_rep_name(t7103.c101_loan_to_id)   			  
     			 			 , get_user_name(t7103.c101_loan_to_id)
     			 	) loantoname
     			 , DECODE(
     			     t7103.c901_loan_to
     			     ,'19518', get_rep_emailid(t7103.c101_loan_to_id)
     			    
     			 			 , get_user_emailid(t7103.c101_loan_to_id)
     			 	) loantoemailid
     			 , t7100.c7100_created_date createddt
     			 , get_user_emailid(p_user_id) fromuseremail
     			 , get_user_name(t7100.c7100_created_by) createdby
     			 , t7100.c7100_created_by createduser 
     			 , get_user_emailid(t7100.c7100_created_by) canuseremail
     			 , get_user_name(T7100.C7100_Last_Updated_By) cancelledby
     			 , get_case_attr_type('EVEMET', v_info_id) selectedcategories
     			 , get_code_name(t7100.c901_case_status) status  
     			 , t7100.c901_case_status statusId
  			  FROM t7103_schedule_info t7103, t7100_case_information t7100 
  			 WHERE t7100.c7100_case_info_id = t7103.c7100_case_info_id 
    		   AND t7103.c7100_case_info_id = v_info_id
    		   AND t7100.c7100_void_fl 	    IS NULL 
    		   AND t7100.c1900_company_id  = v_company_id
			   AND t7100.c1900_company_id  = t7103.c1900_company_id (+)
    		   AND t7100.c901_type = 1006504;

    END gm_fch_event_info;
 	/******************************************************************************************
	  * Procedure to save the Rescheduled Event Information
	  * HReddi
	*******************************************************************************************/   
    PROCEDURE gm_sav_reschedule_event(
		p_caseinfoid			 IN 	  t7100_case_information.c7100_case_info_id%TYPE
	  , p_start_date 			 IN 	  t7103_schedule_info.c7103_start_date%TYPE
	  , p_end_date 				 IN 	  t7103_schedule_info.c7103_end_date%TYPE
	  , p_plan_ship_dt           IN 	  DATE
	  , p_exp_return_dt			 IN       DATE
	  , p_reschedule_reason      IN       t7103_schedule_info.C901_reschedule_reason%TYPE
	  , p_user_id 				 IN 	  VARCHAR2
	  , p_out_event_info_id	 	 OUT	  t7100_case_information.c7100_case_info_id%TYPE 
	)
	AS
		v_case_id	   t7100_case_information.c7100_case_info_id%TYPE;
		v_event_id     t7100_case_information.c7100_case_id%TYPE;
   
        v_rep_id   			NUMBER;
        v_seq		   		NUMBER;
        v_event_status      VARCHAR2(5);
        v_status            NUMBER;
        v_info_id			t7100_case_information.c7100_case_info_id%TYPE;
        
        BEGIN

	      v_info_id :=  get_event_info_id(p_caseinfoid);
	        
	      -- Validate event before reschedule
	      gm_validate_event(v_info_id,'reschedule');
	      
	         
		-- Getting the status for checking the Event has Sets or not  
        gm_pkg_sm_eventbook_rpt.gm_fch_event_req_approved(v_info_id,v_event_status);
		
        -- if status retuns Y or N then we are setting the Event Status as 'ACTIVE' or else 'IN-COMPLETE'
		IF v_event_status <> 'NR' THEN
			v_status := '19524';  --ACTIVE
	    ELSE 
	    	v_status := '19534'; -- IN-COMPLETE
	    END IF;

	      -- check all the dates are entered ( need to replace the error code from -20999)
	      IF p_start_date IS NULL OR p_end_date IS NULL OR (v_event_status <> 'NR' AND (p_plan_ship_dt IS NULL OR p_exp_return_dt IS NULL))  
	      THEN
	      	GM_RAISE_APPLICATION_ERROR('-20999','368','');
	      END IF;
		
	    /* If event ship date is within 4 days then item requests are already allocated.
	    So,System will throw error message.*/
	    gm_validate_item_req(p_caseinfoid,p_user_id);  
	      
	    -- Update the request based on the reschedulng of the event
	    gm_sav_req_reschedule(v_info_id, p_plan_ship_dt, p_exp_return_dt, p_user_id);

		-- Get the Sequence for New Event Creation
		gm_fch_next_event_key (p_start_date, '1006504', v_event_id); --1006504 Event type
		
	    
	     SELECT s7100_case_information.NEXTVAL  
		   INTO p_out_event_info_id 
		   FROM dual ;
		   
	-- Inserting the new record by inserting the new Event ID, new Case id and Case Status
        INSERT INTO  t7100_case_information  
        	(c7100_case_info_id, c7100_case_id, c7100_created_by, c7100_created_date
        	,c901_case_status, c901_type, c7100_parent_case_info_id,c1900_company_id)
                 SELECT  p_out_event_info_id, v_event_id, p_user_id, CURRENT_DATE
                        ,v_status, 1006504, v_info_id,c1900_company_id
            	   FROM t7100_case_information 
            	  WHERE c7100_case_info_id = v_info_id;
          
    -- Updating the case Status to "RESCHEDULED" for existing Case Info ID.    	 
         UPDATE t7100_case_information 
            SET c901_case_status = 19526 -- Rescheduled
               , c7100_last_updated_by = p_user_id
		       , c7100_last_updated_date = CURRENT_DATE
          WHERE c7100_case_info_id = v_info_id 
            AND c7100_void_fl IS NULL AND c901_type = 1006504;
         
    -- Inserting the new record by assigning the old event information
         INSERT INTO t7103_schedule_info
						(c7100_case_info_id,c7103_name, c7103_start_date, c7103_end_date , c901_purpose, c7103_venue, c901_loan_to,c901_loan_to_org
					   , c101_loan_to_id, c901_reschedule_reason,c1900_company_id)
				SELECT p_out_event_info_id,c7103_name, p_start_date, p_end_date, c901_purpose, c7103_venue, c901_loan_to,c901_loan_to_org
				       , c101_loan_to_id, p_reschedule_reason,c1900_company_id 
				  FROM t7103_schedule_info 
				 WHERE c7100_case_info_id = v_info_id;  

	-- Inserting the Case Attribute Details.         
         INSERT INTO t7102_case_attribute
					(c7102_case_attribute_id, c7100_case_info_id, c7102_attribute_value, c901_attribute_type)
			   SELECT  s7102_case_attribute.NEXTVAL, p_out_event_info_id, c7102_attribute_value, c901_attribute_type
                 FROM t7102_case_attribute 
                WHERE c7100_case_info_id = v_info_id 
                  AND c7102_void_fl IS NULL;
      
        -- Inserting the Case Set Information from Old Case info id information to new Case Info ID          
        INSERT INTO t7104_case_set_information
					(c7104_case_set_id, c7100_case_info_id, c207_set_id, c5010_tag_id, c901_system_type, c7104_tag_lock_from_date
				   , c7104_tag_lock_to_date, c7104_created_date, c7104_created_by, c526_product_request_detail_id, c7104_shipped_del_fl 
				   , c205_part_number_id, c7104_item_qty)
                   SELECT s7104_case_set_information.NEXTVAL, p_out_event_info_id, c207_set_id, c5010_tag_id, c901_system_type, p_start_date  
                          ,p_end_date ,CURRENT_DATE ,p_user_id ,c526_product_request_detail_id ,c7104_shipped_del_fl  
                          ,c205_part_number_id , c7104_item_qty
                     FROM t7104_case_set_information 
                    WHERE c7100_case_info_id = v_info_id 
                      AND c7104_void_fl IS NULL;	 
  
                      
	END gm_sav_reschedule_event ;
	
	/******************************************************************************************
	  * Procedure to allocate or deallocate the requests and CN's based on the rescheduling.
	  * Refer the flow for this process in
	  * http://confluence.spineit.net/display/PRJ/IST_R004_Reschedule+Event
	  * 
	  * V Prasath
	*******************************************************************************************/	
	PROCEDURE gm_sav_req_reschedule (
	  p_caseinfoid 		 IN  t7100_case_information.c7100_case_info_id%TYPE
	, p_planned_shipdate IN  t526_product_request_detail.c526_required_date%TYPE  
	, p_exp_return_date	 IN	 t526_product_request_detail.c526_required_date%TYPE  
	, p_user_id    		 IN  t7100_case_information.c7100_created_by%TYPE	
	)
	AS
	 v_req_cnt		    NUMBER;
     v_no_of_days		NUMBER;
     v_ship_date		t526_product_request_detail.c526_required_date%TYPE;
     v_info_id			t7100_case_information.c7100_case_info_id%TYPE;
     
	  CURSOR v_req_cur IS
	  SELECT t526.c207_set_id set_id
	       , t526.c526_required_date old_planned_ship_date
		   , t526.c526_status_fl status_fl
		   , t504a.c504_consignment_id cn_id 
		   , t526.c526_product_request_detail_id req_det_id
	    FROM t526_product_request_detail t526
	       , t7104_case_set_information t7104
	       , t504a_loaner_transaction t504a
	   WHERE t7104.c526_product_request_detail_id = t526.c526_product_request_detail_id
	     AND t526.c526_product_request_detail_id = t504a.c526_product_request_detail_id (+)
	     AND t7104.c7100_case_info_id = v_info_id
	     AND t7104.c7104_void_fl is NULL
	     AND t526.c526_void_fl is NULL
	     AND t504a.c504a_void_fl(+) is NULL;
	BEGIN
		
		 v_info_id :=  get_event_info_id(p_caseinfoid);
		
		  SELECT COUNT(1) 
  		    INTO v_req_cnt
            FROM t7104_case_set_information t7104 
           WHERE c7100_case_info_id = v_info_id
             AND c526_product_request_detail_id is not NULL;
	          
	  IF v_req_cnt > 0 THEN
	      v_no_of_days := get_rule_value('4119','LOANERREQDAY'); -- 4119 - InHouse Loaner

	    FOR v_index in v_req_cur 
	     LOOP 
	            IF  v_index.old_planned_ship_date != p_planned_shipdate 
	           AND (p_planned_shipdate - CURRENT_DATE) >  v_no_of_days 
	           AND v_index.status_fl = 20 -- Allocated
	           AND v_index.cn_id IS NOT NULL 
	           AND v_index.set_id is NOT NULL THEN
	           
	            	 gm_pkg_op_loaner.gm_sav_deallocate(v_index.cn_id, p_user_id);
	            	 
	         ELSIF v_index.old_planned_ship_date != p_planned_shipdate 
	           AND (p_planned_shipdate - CURRENT_DATE) <=  v_no_of_days  
	           AND v_index.status_fl = 10 -- Open
	           AND v_index.set_id IS NOT NULL THEN
	           
	           		gm_pkg_op_loaner.gm_sav_allocate(v_index.set_id,'4119',p_user_id); -- 4119 - InHouse Loaner
	           		
	          END IF; 	
	          
	          UPDATE t526_product_request_detail t526
	             SET t526.c526_required_date = p_planned_shipdate
	               , t526.c526_exp_return_date = p_exp_return_date
	               , t526.c526_last_updated_by = p_user_id
	               , t526.c526_last_updated_date = CURRENT_DATE
	           WHERE t526.c526_product_request_detail_id = v_index.req_det_id
	             AND t526.c526_status_fl < 30; --  (30 - Closed , 40 - Rejected)
	          	
	     END LOOP;              
	  END IF;
              
    END   gm_sav_req_reschedule;       
	
	/******************************************************************************************
	  * Procedure to save the Event Request Details Information
	  * A Prasath
	*******************************************************************************************/	
	PROCEDURE gm_sav_event_request_details (
		 p_caseinfo_id	IN	 t7100_case_information.c7100_case_info_id%TYPE
	    ,p_userid		IN	 t7100_case_information.c7100_last_updated_by%TYPE
	)
	AS
				v_str		   VARCHAR2 (4000);
				v_casesetid    t7104_case_set_information.C7104_Case_Set_Id%TYPE;
				v_enddate      t7100_case_information.c7100_surgery_date%TYPE;
				v_req_id	   VARCHAR2 (20);
				v_shipid 	   VARCHAR2 (20);
				v_req_dtl_id   t7104_case_set_information.c526_product_request_detail_id%TYPE;	
				v_childshipid  VARCHAR2 (20);
				v_setid   	   t7104_case_set_information.c207_set_id%TYPE;
				v_addressid    t106_address.c106_address_id%TYPE;
				v_status       t526_product_request_detail.c526_status_fl%TYPE;
				v_source	   t907_shipping_info.c901_source%TYPE;
				v_flag		   CHAR(1) := 'Y';	
				v_info_id	   t7100_case_information.c7100_case_info_id%TYPE;
				v_casetype     t7100_case_information.c901_type%TYPE;
				v_req_date     VARCHAR2 (40);
				v_req_for_id   VARCHAR2 (40);
				v_rep_id 	   VARCHAR2 (40);
				v_acc_id 	   VARCHAR2 (40);
				v_req_by_type  t525_product_request.c901_request_by_type%TYPE;
				v_ass_rep_id NUMBER;
				v_temp_status  t526_product_request_detail.c526_status_fl%TYPE;
				v_dateformat  varchar2(100);
				v_loanto_org  t7103_schedule_info.c901_loan_to_org%TYPE;
				v_company_id  t1900_company.c1900_company_id%TYPE;
		CURSOR c_tagstr
		IS
			SELECT t7104.c7104_case_set_id casesetid, t7104.c7100_case_info_id caseinfoid,
			get_code_name_alt(t7104.c901_system_type) systype,t7100.c901_type casetype,
			decode(t7103.c901_loan_to,19518,4121,4123) loanto,t7103.c101_loan_to_id loantoid,			
				t7103.c7103_start_date startdate,t7103.c7103_end_date enddate
				 , t7104.c526_product_request_detail_id prdreqdtlid, t7104.c207_set_id setid
				 , t7104.c205_part_number_id pnum, nvl(t7104.c7104_item_qty,1) qty, t7100.c7100_surgery_date surdate
				 , t7100.c701_distributor_id distid,t7100.c703_sales_rep_id rep_id, t7100.c704_account_id accountid
				 , t7103.c901_loan_to_org loantoorg  
			  FROM t7104_case_set_information t7104, t7100_case_information t7100,t7103_schedule_info t7103
			 WHERE t7104.c7104_void_fl IS NULL
			   AND T7104.C7104_Shipped_Del_Fl IS NULL
			   AND t7104.c7100_case_info_id = v_info_id
			   AND t7104.c7100_case_info_id = t7100.c7100_case_info_id
               AND t7103.c7100_case_info_id(+) = t7100.c7100_case_info_id
			   AND t7100.c901_case_status = 19524	-- Active
			   AND t7100.c7100_void_fl IS NULL 
			  -- AND t7100.c901_type = 1006504 
			   AND t7104.c7104_case_set_id =  t7104.c7104_case_set_id 
			    ORDER BY t7104.c207_set_id,t7104.c205_part_number_id;
	BEGIN
		 select get_compdtfmt_frm_cntx(), get_compid_frm_cntx () into v_dateformat, v_company_id from dual;
		 v_info_id :=  get_event_info_id(p_caseinfo_id);
		 
		 -- Validate event before updating the request
	      gm_validate_event(v_info_id,'request');
	      
	     --fetching product request id for existing records 
	      BEGIN
		    SELECT req_id INTO v_req_id FROM
	       	(SELECT t526.c525_product_request_id req_id
			  FROM t7104_case_set_information t7104
			     , t7100_case_information t7100
			     , t526_product_request_detail t526 
			 WHERE t7104.c7104_void_fl IS NULL
			   AND t7104.c7104_shipped_del_fl is NULL
			   AND t7104.c7100_case_info_id = v_info_id
               AND t7104.c7100_case_info_id = t7100.c7100_case_info_id
               AND t7100.c901_case_status = 19524	-- Active 
              -- AND t7100.c901_type = 1006504 
			   AND t7100.c7100_void_fl is NULL
			   AND t7104.c7104_case_set_id =  t7104.c7104_case_set_id
               AND t526.c526_product_request_detail_id (+)= t7104.c526_product_request_detail_id
               AND t526.c526_void_fl is null
               AND t526.c525_product_request_id IS NOT NULL               
               ORDER BY t7104.c207_set_id,t7104.c205_part_number_id) 
               WHERE ROWNUM=1;
           EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               v_req_id := NULL;
        	 END;
		DELETE FROM my_temp_list;
		FOR v_rec IN c_tagstr
		LOOP
			v_casesetid := v_rec.casesetid;
			v_enddate := v_rec.enddate;					
			v_req_dtl_id := v_rec.prdreqdtlid;
			v_casetype := v_rec.casetype;
			v_status := '0';
			v_loanto_org := v_rec.loantoorg;
			--forming string to insert records in t526_product_request_detail
			--IF v_systype = 4127 OR  v_systype = 400087 OR v_systype = 400088 
			IF v_casetype = 1006505 OR  v_casetype = 1006506 OR v_casetype = 1006507 OR v_casetype = 107286  --Loaner Set, Set/Item consignment,107286 -Account Item Consignment 
			THEN
			    SELECT DECODE(v_casetype, '1006505', '0', '1') 
			   	 INTO v_status 
			    FROM DUAL;
				v_enddate := v_rec.surdate;	 
       			v_req_for_id := v_rec.distid;
				v_rep_id := v_rec.rep_id;
				v_acc_id := v_rec.accountid;
				IF v_casetype = 107286 --107286 Account Item Consignment
				THEN
				v_req_for_id := v_rec.accountid; -- setting account id as request for id 
				END IF;

				SELECT DECODE(v_casetype, 1006505, 50626, 4000670 ) INTO v_req_by_type FROM DUAL;
				--v_req_by_type  := 50626;
				BEGIN
				--For loaners, use planned ship date instead of required date(since it has no required date).
					SELECT to_char( DECODE(v_casetype, 1006505, c907_ship_to_dt, c526_required_date) , v_dateformat ) , C703_ASS_REP_ID
					  INTO v_req_date, v_ass_rep_id
            		  FROM my_temp_case_ship_det 
            		 WHERE c7104_case_set_id = v_rec.casesetid;
                EXCEPTION WHEN NO_DATA_FOUND
	           	THEN
	           	 	v_req_date := NULL;
		        END;
			ELSE 
				--PMT#49676-cns-linked-to-inhouse-event
				IF v_casetype = 1006504  AND v_rec.loanto = '4121' THEN   --case type is event and loan to is sales rep
					v_req_date := NULL;
					v_req_for_id := v_rec.loantoid;
					v_rep_id := v_rec.loantoid;
					v_acc_id := NULL;
					v_req_by_type  := 50625;
					v_ass_rep_id := NULL;
				ELSE
					--v_status := 5;
					v_req_date := NULL;
					v_req_for_id := v_rec.loantoid;
					v_rep_id := NULL;
					v_acc_id := NULL;
					v_req_by_type  := 50625;
					v_ass_rep_id := NULL;
			   END IF;	
				
				IF(NVL(get_rule_value_by_company(v_casetype, 'CHK_DIST_COMPANY', v_company_id), 'N') = 'Y') AND v_loanto_org = 19523  --19523 OUS Field Sales
				THEN
					v_req_for_id := GET_DISTRIBUTOR_ID(v_req_for_id);
				END IF;
		    END IF;
		    
		    -- if v_status is '0', then assign value '5' to temp variable to pass it to the string 
		    SELECT DECODE(v_status, '0', '5', v_status) INTO v_temp_status FROM DUAL;
		    
			v_str := v_rec.setid || '^'||v_rec.qty||'^' || v_req_date ||'^'|| v_temp_status ||'^'||v_rec.pnum||'|';    
				
					IF v_rec.pnum IS NOT NULL THEN
						v_source := 50186; -- InHouse Item
			    	ELSE
			    		v_source := 50182; -- Loaners	    		
			    	END IF;   
    	
	    	v_childshipid := 0;
			v_addressid :=  0;					
		--inserting records into t525_product_request table
		IF v_req_dtl_id IS NULL
		THEN
		
		gm_pkg_op_product_requests.gm_sav_product_requests (v_enddate
														  , v_rec.systype   --C901_REQUEST_FOR Product Loaner or inhouse loaner or item
														  , v_req_for_id    --C901_REQUEST_FOR_ID
														  , v_req_by_type	-- (employee) C901_REQUEST_BY_TYPE
														  , p_userid   --C525_REQUEST_BY_ID
														  , NULL   -- C901_SHIP_TO
														  , NULL   --C525_SHIP_TO_ID(sales rep id)p_ship_to_id
														  , p_userid
														  , v_str
														  , v_rep_id  --C703_SALES_REP_ID
														  , v_acc_id --C704_ACCOUNT_ID
														  , v_req_id
														  , '1006504' --EVENT C901_CODE_ID
														  , v_ass_rep_id
														   );
		
	 	
		
		 
			SELECT MAX(c526_product_request_detail_id)
			  INTO v_req_dtl_id
			  FROM t526_product_request_detail
			 WHERE c525_product_request_id = v_req_id 
			   AND c526_void_fl IS NULL;
			 --Shipping detail for product request detail id
			gm_pkg_cm_shipping_trans.gm_sav_ship_info (v_req_dtl_id	 --refid
													, v_source   --source
													, NULL	 -- (sales rep)p_ship_to
													, NULL	--(sales rep id)p_ship_to_id
													, 5001	 -- carrier
													, 5004	 -- delivery mode
													, NULL	 -- tracking_number
													, v_addressid	 -- address_id
													, 0   -- freight amount
													, p_userid
													, v_childshipid	 -- out shipid
													 );	
											 
			IF v_casetype = 1006505 OR  v_casetype = 1006506 OR v_casetype = 1006507 OR v_casetype = 107286  --Loaner Set, Set/Item consignment. 
			THEN
			    v_status := 1;
			ELSE 
				v_status := 5;
			END IF;
			--updating product request detail id in t7104_case_set_information
			 UPDATE t7104_case_set_information 
			    SET c526_product_request_detail_id=v_req_dtl_id 
			  WHERE c7104_case_set_id=v_casesetid 
			    AND c7100_case_info_id = v_info_id 
			    AND c7104_void_fl is null; 
			 
		END IF;
		
		--CREATING OR UPDATING SHIPPING RECORDS FOR PRODUCT REQUEST ID SHOULD RUN ONE TIME
        IF  v_req_id IS NOT NULL AND v_flag = 'Y' THEN		
			IF v_rec.loanto = '4121' THEN
			  BEGIN
				SELECT c106_address_id
				  INTO v_addressid
				  FROM t703_sales_rep t703, t106_address t106
	             WHERE t703.c101_party_id     = t106.c101_party_id            
				   AND t703.c703_sales_rep_id = v_rec.loantoid
	               AND t106.c106_primary_fl   = 'Y'
	               AND t106.c106_void_fl IS NULL ;
	           EXCEPTION WHEN NO_DATA_FOUND
	           	 THEN
	           	 	v_addressid := NULL;
	           	END;	           	
	         ELSIF v_rec.loanto = '4123' THEN     
	          BEGIN
	            SELECT c106_address_id
				  INTO v_addressid
				  FROM t101_user t101, t106_address t106
	             WHERE t101.c101_party_id     = t106.c101_party_id            
				   AND t101.c101_user_id = v_rec.loantoid
	               AND t106.c106_primary_fl   = 'Y'
	               AND t106.c106_void_fl IS NULL ;
	           EXCEPTION WHEN NO_DATA_FOUND
	           	 THEN
	           	 	v_addressid := NULL;
	           	END;	           	
        	END IF;
		
			BEGIN
            	 SELECT c907_shipping_id
	               INTO v_shipid
	               FROM t907_shipping_info
	              WHERE c907_ref_id = v_req_id
	                AND c901_source = 50185
	                AND c907_tracking_number IS NULL
	                AND c907_void_fl IS NULL;
         	EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               v_shipid := 0;
        	 END;	
			--Shipping detail for product request id
			gm_pkg_cm_shipping_trans.gm_sav_ship_info (v_req_id	 --refid
													, 50185   --source
													, v_rec.loanto	 -- loan_to
													, v_rec.loantoid	--loan_to_id
													, 5001	 -- carrier
													, 5004	 -- delivery mode
													, NULL	 -- tracking_number
													, v_addressid	 -- address_id
													, 0   -- freight amount
													, p_userid
													, v_shipid	 -- out shipid
													 );
													 
			v_flag := 'N';
		 END IF;
	--Commented as part of PMT-5840 change and status =1 has no significance since it is  used as and Condition
		IF v_status = '5' --AND v_status = '1'  --pending approval record should update dates
		THEN							      
			UPDATE t907_shipping_info 
			   SET c907_ship_to_dt = NULL
			     , c907_last_updated_by = p_userid
			     , c907_last_updated_date = CURRENT_DATE
			 WHERE c907_ref_id=TO_CHAR(v_req_dtl_id) 
			   AND c907_void_fl IS NULL 
			   AND c907_tracking_number IS NULL 
			   AND c907_status_fl <> 40;		   
		END IF;
							
	END LOOP;
	--find any obsolete sets are added,It will throw app error 
	--gm_pkg_op_product_requests.gm_validate_set_obsolete_sts(); PMT-29947
	IF v_req_id IS NOT NULL
	THEN
	   UPDATE t525_product_request 
	      SET c901_request_for = DECODE( v_casetype , 1006505,'4127', 1006506,'400087', 1006507,'400088',107286,'40050','4119') -- In House, 107286 - Account Item Consignment,40050 -Hospital Consignment
	        , c525_status_fl = '10'
	        , c525_last_updated_by = p_userid
	        , c525_last_updated_date = CURRENT_DATE 
	   WHERE c525_product_request_id = v_req_id 
	     AND C525_VOID_FL IS NULL;
	END IF; 
	
	END gm_sav_event_request_details;
	
	/******************************************************************************************
	  * Procedure to save the Event Shipping Details Information
	  * A Prasath
	*******************************************************************************************/
	PROCEDURE gm_sav_event_shipping_details (
		p_caseinfo_id		IN	 t7100_case_information.c7100_case_info_id%TYPE
	  , p_plan_ship_dt  	IN 	 DATE
	  , p_exp_return_dt		IN   DATE
	  , p_shipto	   		IN	 t907_shipping_info.c901_ship_to%TYPE
	  , p_shiptoid	   		IN	 t907_shipping_info.c907_ship_to_id%TYPE
	  , p_shipmode	   		IN   t907_shipping_info.c901_delivery_mode%TYPE
	  , p_shipcarr	   		IN	 t907_shipping_info.c901_delivery_carrier%TYPE
	  , p_addressid    		IN	 t907_shipping_info.c106_address_id%TYPE
	  , p_userid			IN	 t7100_case_information.c7100_last_updated_by%TYPE
	)
	AS
	 	v_info_id	   t7100_case_information.c7100_case_info_id%TYPE;
	 	
	    CURSOR v_cur IS
	    SELECT t526.c525_product_request_id req_id		   
		  FROM t7104_case_set_information T7104
		      ,t526_product_request_detail T526
		 WHERE t7104.c7100_case_info_id = v_info_id 
		   AND t526.c526_product_request_detail_id = t7104.c526_product_request_detail_id  
		   AND t526.c526_void_fl IS NULL
		   AND t7104.c7104_void_fl IS NULL 
		   AND t7104.c7104_shipped_del_fl IS NULL
	  GROUP BY t526.c525_product_request_id;
	
	BEGIN	
		 v_info_id :=  get_event_info_id(p_caseinfo_id);
		 
		 -- Validate event before updating the request
	      gm_validate_event(v_info_id,'shipping');
		
		FOR v_index IN v_cur
		LOOP 
			   
			UPDATE t526_product_request_detail 
			   SET c526_exp_return_date	= p_exp_return_dt
			     , c526_required_date =  p_plan_ship_dt
			     , c526_last_updated_by = p_userid
			     , c526_last_updated_date = CURRENT_DATE
			 WHERE c525_product_request_id 	=  v_index.req_id
			   AND c526_exp_return_date IS NULL
			   AND c526_void_fl	IS NULL;
			
			UPDATE t907_shipping_info
			   SET c907_ship_to_dt = p_plan_ship_dt
				 , c907_last_updated_by = p_userid
				 , c907_last_updated_date = CURRENT_DATE
			WHERE  c907_ref_id = v_index.req_id 
			  AND c907_void_fl IS NULL 
			  AND c901_source = 50185 
			  AND c907_ship_to_dt IS NULL 
			  AND c907_tracking_number IS NULL;	   
				   
			UPDATE t907_shipping_info
			   SET c901_ship_to = DECODE(p_shipto,0,NULL,p_shipto)
				 , c907_ship_to_id = DECODE(p_shiptoid,0,NULL,p_shiptoid)
				 , c907_ship_to_dt = p_plan_ship_dt
				 , c901_delivery_mode = DECODE(p_shipmode,0,NULL,p_shipmode)
				 , c901_delivery_carrier = DECODE(p_shipcarr,0,NULL,p_shipcarr)
				 , c106_address_id = p_addressid
				 , c907_last_updated_by = p_userid
				 , c907_last_updated_date = CURRENT_DATE
			WHERE  C525_Product_Request_Id = v_index.req_id  
			  AND c907_void_fl IS NULL 
			  AND c901_source IN(50182,50186) --50182-Loaners,50186-Inhouse Loaners 
			  AND c907_tracking_number IS NULL 
			  AND c907_ship_to_dt IS NULL 
			  AND c907_status_fl <> 40;
		  
		END LOOP;
	END gm_sav_event_shipping_details;
	
	/*******************************************************
	* Description : Procedure to Update event closed status job 
	* Author	  : Arockia Prasath
	*******************************************************/
	PROCEDURE  gm_sav_event_closed 
	AS
		v_company_id 		t1900_company.c1900_company_id%TYPE;
	BEGIN
		
		SELECT NVL(get_compid_frm_cntx(),GET_RULE_VALUE('DEFAULT_COMPANY','ONEPORTAL'))
          INTO v_company_id 
          FROM DUAL;
		
		UPDATE t7100_case_information
		   SET c901_case_status = 19527 --Closed
			 , c7100_last_updated_by = 30301 -- Manual
			 , c7100_last_updated_date = CURRENT_DATE
		 WHERE c7100_case_info_id in (SELECT c7100_case_info_id 
		                                FROM t7103_schedule_info  
		 							   WHERE TRUNC(c7103_end_date)+1=TRUNC(CURRENT_DATE)
		 							   AND C1900_COMPANY_ID = v_company_id) 
		   AND c901_case_status in (19524 , 19534) -- Active, In Complete
		   AND c7100_void_fl IS NULL
		   AND c901_type = 1006504;
		
	END gm_sav_event_closed;
	/*******************************************************
	* Description : Procedure to Cancel Open Request after the start date of the event 
	* Author	  : Arockia Prasath
	*******************************************************/
	PROCEDURE  gm_sav_event_open_req 
	AS
		v_company_id 		t1900_company.c1900_company_id%TYPE;
	BEGIN
		
		SELECT NVL(get_compid_frm_cntx(),GET_RULE_VALUE('DEFAULT_COMPANY','ONEPORTAL'))
          INTO v_company_id 
          FROM DUAL;
		
		UPDATE T526_Product_Request_Detail
		   SET c526_status_fl = '40' --Rejected
			 , c526_last_updated_by = 30301 -- Manual
			 , c526_last_updated_date  = CURRENT_DATE
		 WHERE c526_product_request_detail_id in (SELECT  t7104.C526_Product_Request_Detail_Id 
												    FROM t7104_case_set_information t7104
												       , t7100_case_information t7100
												       , t7103_schedule_info t7103
												   WHERE t7104.c7104_void_fl IS NULL
												     AND t7100.c7100_case_info_id = t7104.c7100_case_info_id
									                 AND t7100.c7100_case_info_id = t7103.c7100_case_info_id
												     --AND t7100.c901_case_status in (19524 , 19534)	-- Active, In Complete
												     AND TRUNC(t7103.c7103_start_date)+1=TRUNC(CURRENT_DATE)
												     AND t7100.c7100_void_fl is NULL 
												     AND t7104.c7104_case_set_id =  t7104.c7104_case_set_id
												     AND t7100.c901_type = 1006504 
                                                     AND t7104.c526_product_request_detail_id IS NOT NULL) 
           AND c526_void_fl IS NULL 
           AND C1900_COMPANY_ID = v_company_id
		   AND c526_status_fl IN ('5','10'); --10 - OPEN	 --5 - pending approval
		
	END gm_sav_event_open_req;
	
	/*******************************************************
	* Description : Procedure to Update the email flag for shipping records.
	* Author	  : Elango
	*******************************************************/
	PROCEDURE gm_sav_evnt_shpd_email_sts (
		p_caseinfoid   IN	t7104_case_set_information.C7100_Case_Info_Id%TYPE
	)
	AS
		v_info_id	   t7100_case_information.c7100_case_info_id%TYPE;
		v_req_id 	   t525_product_request.c525_product_request_id%TYPE;
		v_company_id   t1900_company.c1900_company_id%TYPE;
		CURSOR v_cur_ship
					IS
					 SELECT t907.c907_shipping_id shipping_id
			           FROM t526_product_request_detail t526
			              , t907_shipping_info t907
			              , t7104_case_set_information t7104
			          WHERE t7104.c7100_case_info_id = v_info_id			           
			            AND t526.c526_product_request_detail_id = t7104.c526_product_request_detail_id
			            AND t526.c526_void_fl IS NULL
			            AND t907.c907_void_fl IS NULL
			            AND t7104.c7104_void_fl IS NULL
			            AND t907.c907_status_fl IN (40)
			            AND t907.c907_email_update_fl IS NULL
			            AND t526.c205_part_number_id  IS NULL
			            AND t907.c907_tracking_number IS NOT NULL 
			            AND t526.c207_set_id IS NOT NULL 
			            AND t907.c525_product_request_id = t526.c525_product_request_id
			            AND t907.c907_ref_id =  DECODE (t526.c526_status_fl
					            									, '20', gm_pkg_op_product_requests.get_loanerreq_consignment_id (t526.c526_product_request_detail_id, '10')
					            									, '30', gm_pkg_op_product_requests.get_loanerreq_consignment_id (t526.c526_product_request_detail_id, NULL)
					            									, T526.C526_Product_Request_Detail_Id
					            									)
					    AND t907.c1900_company_id  = v_company_id
					    AND t907.c1900_company_id = t526.c1900_company_id      									
				  	UNION ALL
				  	/* Query to get IHLN shipped out records based on the master request (t525) */
					 SELECT t907.c907_shipping_id
					   FROM t504_consignment t504, t907_shipping_info t907
					  WHERE T504.C504_Consignment_Id   = T907.C907_Ref_Id
					    AND T907.C907_Status_Fl       IN (40) --shipped out
					    AND t504.c207_set_id          IS NULL
					    AND T907.C907_Void_Fl         IS NULL
					    AND t907.c907_email_update_fl IS NULL
					    AND T504.C504_Ref_Id           = TO_CHAR(v_req_id)
					    AND T504.C504_Type             = 9110 --IHLN
					    AND t907.c1900_company_id  = v_company_id
					    AND t907.c1900_company_id  = t504.c1900_company_id
					    AND T504.C504_Void_Fl         IS NULL; 	
	BEGIN
		v_info_id :=  get_event_info_id(p_caseinfoid);
		v_req_id  :=  gm_pkg_sm_eventbook_rpt.get_case_request_id(v_info_id);
		SELECT get_compid_frm_cntx()
            INTO v_company_id
            FROM DUAL;
            
		FOR v_cur IN v_cur_ship
		LOOP 
			UPDATE t907_shipping_info
	           SET c907_email_update_fl  = 'Y'
	             , c907_email_update_dt = TRUNC (CURRENT_DATE)
	             , c907_last_updated_by = 30301 -- Manual
				 , c907_last_updated_date = CURRENT_DATE
	         WHERE c907_shipping_id = v_cur.shipping_id
		    AND c907_email_update_fl IS NULL
		    AND c907_active_fl IS NULL
		    AND c907_status_fl = 40;
	    END LOOP;
	END gm_sav_evnt_shpd_email_sts;
	/*******************************************************
	* Description : Procedure to validate item request while rescheduling an event
	* Author	  : Gopinathan
	*******************************************************/
	PROCEDURE gm_validate_item_req (
		p_caseinfoid   IN	t7100_case_information.c7100_case_info_id%TYPE
	  , p_userid	   IN	t7100_case_information.c7100_created_by%TYPE
	)
	AS
		v_count NUMBER;
	BEGIN
		
		     SELECT COUNT(1) INTO v_count
	           FROM t526_product_request_detail t526, t7104_case_set_information t7104
	          WHERE t526.c526_product_request_detail_id = t7104.c526_product_request_detail_id
	            AND t7104.c7100_case_info_id = p_caseinfoid
	            AND t526.c526_status_fl IN ('20') -- Allocated,closed
	            AND t526.c526_void_fl IS NULL
	            AND t526.c205_part_number_id IS NOT NULL -- To avoid set requests
	            AND t7104.c7104_void_fl  IS NULL;
		
	    IF v_count > 0 THEN
	    	raise_application_error('-20587','');
		END IF;
			
	END gm_validate_item_req;	

	/*******************************************************
	* Description : Procedure to Cancel Event
	* Author      : Yoga
	*******************************************************/
	PROCEDURE gm_sav_cancel_event (
	    p_caseinfoid   IN	t7100_case_information.c7100_case_info_id%TYPE,
	    p_userid	   IN	t7100_case_information.c7100_created_by%TYPE
	)
	AS
	  v_info_id         t7100_case_information.c7100_case_info_id%TYPE;
	  v_txn_str         VARCHAR2(4000);
	  v_txn_temp_str         VARCHAR2(4000);
	  v_prod_req_id     t412_inhouse_transactions.c412_inhouse_trans_id%TYPE;
	  v_req_type        t525_product_request.c901_request_for%TYPE;
	BEGIN
	  BEGIN
	    SELECT t525.c525_product_request_id, t525.c901_request_for
	           INTO v_prod_req_id, v_req_type
	           FROM t525_product_request t525, t526_product_request_detail t526, t7104_case_set_information t7104
	          WHERE t525.c525_product_request_id =  t526.c525_product_request_id
	         AND t526.c526_product_request_detail_id = t7104.c526_product_request_detail_id
	            AND t7104.c7100_case_info_id = p_caseinfoid
	            AND t526.c526_void_fl IS NULL
	            AND t7104.c7104_void_fl  IS NULL
	            AND ROWNUM = 1 ;
	   EXCEPTION WHEN NO_DATA_FOUND THEN
	     v_prod_req_id := NULL;
		 v_req_type := NULL;
	  END;

	  -- Cancel the event
	  v_info_id :=  get_event_info_id(p_caseinfoid);

          -- Cancel t7100_case_information records
	  gm_pkg_sm_eventbook_txn.gm_sav_void_txn(v_info_id,'EVENT', p_userid);  



	  v_txn_temp_str:=v_txn_temp_str || v_info_id || ' Event successfully voided<BR>';
	  --gm_pkg_sm_eventbook_txn.fch_output_msg(v_txn_temp_str);
	  
	  --PC-2836 - Unable to Cancel event when event has no sets
	  IF v_prod_req_id IS NOT NULL THEN
	  FOR v_return_cur IN (
			 SELECT T526.C526_Product_Request_Detail_Id req_det_id,T526.C901_Request_Type req_type	          
	           FROM t525_product_request t525, t526_product_request_detail t526
	          WHERE t525.c525_product_request_id =  t526.c525_product_request_id
	             AND T526.C525_Product_Request_Id=v_prod_req_id
                  AND T526.C526_Status_Fl in (20)
	            AND T525.C525_Void_Fl Is NULL
	            )
    		LOOP
       		BEGIN 
	       		  gm_pkg_op_loaner.gm_sav_release_loaner (v_return_cur.req_det_id, v_return_cur.req_type, p_userid);
           END;
    	END LOOP;

          -- Reject t526 and Close t525 records
	  gm_pkg_sm_eventbook_txn.gm_sav_void_txn(v_prod_req_id,'REQ', p_userid);  
	  v_txn_temp_str:= v_txn_temp_str || v_prod_req_id || ' Request successfully voided<BR>';
	  --gm_pkg_sm_eventbook_txn.fch_output_msg(v_txn_temp_str);



	  -- Void t504 and t505 records for the event (with IHRQ child records)
	  gm_pkg_sm_eventbook_txn.gm_process_void_item_txn(v_prod_req_id, p_userid,v_txn_temp_str);

	 -- gm_pkg_sm_setmgmt_txn.gm_sav_cancel_tag (v_info_id, NULL, NULL, p_userid); -- Temporary comment
	 
	END IF;	

		
	END gm_sav_cancel_event; 

	/*******************************************************
	* Description     : Procedure to check for voiding child item Transactions
	* Author	  : Yoga
	*******************************************************/
	PROCEDURE gm_process_void_item_txn(
	  p_refid         IN  t525_product_request.c525_product_request_id%TYPE,
	  p_userid        IN  t7100_case_information.c7100_created_by%TYPE,
	  p_out_result    OUT VARCHAR2
	)
        AS
          CURSOR c_open_txn (v_txn_id VARCHAR2)
          IS
            SELECT t412.c412_inhouse_trans_id txn_id, t412.c412_type txn_type, t412.c412_status_fl status_fl, 'INHOUSE' txn_category
              FROM t412_inhouse_transactions t412, t413_inhouse_trans_items t413
             WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
                AND t413.c413_status_fl       IN ('R','Q')
                AND t412.c412_void_fl         is null
                AND t412.C412_Ref_Id          = TO_CHAR(v_txn_id)
            UNION ALL  -- 
            SELECT t504.c504_consignment_id txn_id, t504.c504_type txn_type, TO_CHAR(t907.c907_status_fl) status_fl, 'CONS' txn_category
              FROM  t504_consignment t504, T907_Shipping_Info T907
             WHERE T907.C907_Void_Fl          IS NULL
                 AND t907.c901_source         = '50186' -- Inhouse Loaner
                 AND T907.c907_ref_id         = t504.c504_consignment_id
                AND T504.C504_Void_Fl         IS NULL
                AND t504.c504_type            = '9110'-- IHLN
                AND T504.C504_Ref_Id          = TO_CHAR(v_txn_id);
               
          v_txn_id          t412_inhouse_transactions.c412_inhouse_trans_id%TYPE;
          v_txn_type        t525_product_request.c901_request_for%TYPE;
          v_parent_txn_type t412_inhouse_transactions.c901_ref_type%TYPE;
          v_txn_void_fl     t412_inhouse_transactions.c412_void_fl%TYPE;
          v_txn_catg        VARCHAR2(20);
          v_status_fl       VARCHAR2(2);
          v_txn_temp_str    VARCHAR2(4000);
        BEGIN
          v_txn_id := p_refid;
          v_txn_temp_str:= p_out_result;

          FOR c_temp IN c_open_txn (v_txn_id)
            LOOP
            BEGIN
              v_txn_id := c_temp.txn_id;
             
              v_txn_type := c_temp.txn_type;
              v_txn_catg := c_temp.txn_category;
              v_status_fl:= TO_CHAR(c_temp.status_fl);


              IF (v_txn_type=9110) THEN --IHLN
                IF (v_status_fl = 40) THEN -- Shipped
                 v_txn_temp_str:=v_txn_temp_str || v_txn_id || ' Shipped out. Cannot be voided<BR>';
                  --gm_pkg_sm_eventbook_txn.fch_output_msg(v_txn_temp_str);
                ELSE
                  
                  gm_pkg_sm_eventbook_txn.gm_sav_void_txn(v_txn_id,v_txn_catg, p_userid);  -- Void t504 records
                  v_txn_temp_str:=v_txn_temp_str || v_txn_id || ' successfully voided<BR>';
                  --gm_pkg_sm_eventbook_txn.fch_output_msg(v_txn_temp_str);
                  
                  gm_pkg_sm_eventbook_txn.gm_sav_void_txn(v_txn_id,'SHIP', p_userid);  -- Void t907 Ship records
                  v_txn_temp_str:=v_txn_temp_str || v_txn_id || ' Shipping successfully voided<BR>';
                  --gm_pkg_sm_eventbook_txn.fch_output_msg(v_txn_temp_str);
                  
                END  IF;

              ELSIF (v_txn_type=9106 OR v_txn_type=9104) THEN -- FGIH(9106),BLIH(9104)
                IF (v_status_fl < 4 ) THEN

                  -- If FGIH / BLIH is child txn of IHRQ which is not yet controlled, map the parts to the IHRQ then void it
                  IF (v_status_fl < 3) THEN  
                    v_parent_txn_type := get_parent_txn_type(v_txn_id);

                    IF (v_parent_txn_type = 1006483) THEN -- IHRQ
                      gm_pkg_op_ihloaner_item_txn.gm_sav_ihbo_map(v_txn_id,v_txn_type,p_refid,p_userid);
                    END IF;
                  END IF;

                  gm_pkg_sm_eventbook_txn.gm_sav_void_txn(v_txn_id,v_txn_catg, p_userid);  -- Void t412 records
                  v_txn_temp_str:=v_txn_temp_str || v_txn_id || ' successfully voided<BR>';
                  --gm_pkg_sm_eventbook_txn.fch_output_msg(v_txn_temp_str);;
                ELSE
                  v_txn_temp_str:=v_txn_temp_str || v_txn_id || ' is already verified. Cannot be voided<BR>';
                  --gm_pkg_sm_eventbook_txn.fch_output_msg(v_txn_temp_str);
                END IF;

              ELSIF (v_txn_type = 1006483) THEN -- IHRQ
                gm_pkg_sm_eventbook_txn.gm_process_void_item_txn(v_txn_id, p_userid, v_txn_temp_str); -- Call Recursively for IHRQ Child records

                -- Check the current status of IHRQ, might be changed if child transaction are mapped throueh 'gm_sav_ihbo_map' 
                BEGIN
                  SELECT c412_status_fl
                    INTO v_status_fl
                    FROM t412_inhouse_transactions
                   WHERE c412_inhouse_trans_id = v_txn_id;
                EXCEPTION WHEN OTHERS THEN
                  v_status_fl:=v_status_fl;
                END;

                IF (v_status_fl IN ('0','3')) THEN -- Open/Reviewed
                  gm_pkg_sm_eventbook_txn.gm_sav_void_txn(v_txn_id,v_txn_catg, p_userid);  -- Void t412 records
                  v_txn_temp_str:=v_txn_temp_str || v_txn_id || ' successfully voided<BR>';
                  --gm_pkg_sm_eventbook_txn.fch_output_msg(v_txn_temp_str);
                ELSE
                  v_txn_temp_str:=v_txn_temp_str || v_txn_id || ' Approved already<BR>';
                  --gm_pkg_sm_eventbook_txn.fch_output_msg(v_txn_temp_str);
                END IF;

              END IF;

            EXCEPTION WHEN OTHERS THEN 
              v_txn_temp_str:=v_txn_temp_str || 'Error :'|| SQLERRM || '<BR>';
              --gm_pkg_sm_eventbook_txn.fch_output_msg(v_txn_temp_str);
            END;
          END LOOP;

          p_out_result := v_txn_temp_str;

        END gm_process_void_item_txn; 

       /*******************************************************
        * Description : Procedure to void child item Transactions
        * Author      : Yoga
        *******************************************************/
        PROCEDURE gm_sav_void_txn(
          p_txn_id        IN  t525_product_request.c525_product_request_id%TYPE,
          p_txn_catg      IN  VARCHAR2,
          p_userid        IN  t7100_case_information.c7100_created_by%TYPE
        )
        AS
          v_cnt                   NUMBER;
          v_status                varchar2(4);

        BEGIN
          BEGIN
            IF (p_txn_catg = 'INHOUSE') THEN
             
              SELECT c412_status_fl
                INTO v_status
                FROM t412_inhouse_transactions
               WHERE c412_inhouse_trans_id = p_txn_id
              FOR UPDATE;

              UPDATE t412_inhouse_transactions
                SET c412_void_fl = 'Y'
                , c412_last_updated_date = CURRENT_DATE
                , c412_last_updated_by = p_userid
               WHERE c412_inhouse_trans_id = p_txn_id;

            ELSIF (p_txn_catg ='CONS') THEN
              SELECT c504_status_fl
                INTO v_status
                FROM t504_consignment
               WHERE c504_consignment_id = p_txn_id
                  AND  c504_void_fl IS NULL
              FOR UPDATE;   

              IF (v_status IN ('4')) THEN -- If Verified, Rollback IHLN and void it
                gm_pkg_op_ihloaner_item_txn.gm_sav_ihloaner_item_rollback(p_txn_id,p_userid);
              END IF;

              UPDATE t504_consignment
                SET c504_void_fl = 'Y'
                , c504_last_updated_date = CURRENT_DATE
                , c504_last_updated_by = p_userid
               WHERE c504_consignment_id = p_txn_id;

            ELSIF (p_txn_catg ='SHIP') THEN            
              SELECT 1
              INTO v_cnt
                FROM t907_shipping_info
               WHERE c907_ref_id = p_txn_id
              FOR UPDATE;

              UPDATE t907_shipping_info
                SET c907_void_fl = 'Y'
                , c907_last_updated_date = CURRENT_DATE
                , c907_last_updated_by = p_userid
               WHERE c907_ref_id = p_txn_id;

            ELSIF (p_txn_catg ='REQ') THEN
              SELECT 1
                INTO v_cnt
                FROM t525_product_request
               WHERE c525_product_request_id = p_txn_id
                  AND ROWNUM=1
               FOR UPDATE;

               UPDATE t525_product_request
                 SET c525_status_fl = '40' -- Closed
                , c525_last_updated_date = CURRENT_DATE
                , c525_last_updated_by = p_userid
               WHERE c525_product_request_id = p_txn_id;
             
              SELECT 1
                INTO v_cnt
                FROM t526_product_request_detail
               WHERE c525_product_request_id = p_txn_id
                  AND ROWNUM=1
               FOR UPDATE;
             
              -- Commented by VPrasath, since the request is getting voided,
              -- no need to update the status to rejected. 

              UPDATE t526_product_request_detail
                  SET c526_void_fl = 'Y'
                     , c526_last_updated_by = p_userid
                     , c526_last_updated_date = CURRENT_DATE
                    WHERE c525_product_request_id = p_txn_id
                       AND c526_status_fl NOT IN ('30','40')
                       AND c526_void_fl IS NULL;
            ELSIF (p_txn_catg ='EVENT') THEN
              SELECT 1
                INTO v_cnt
                FROM t7100_case_information
               WHERE c7100_case_info_id = p_txn_id
                AND c901_type = 1006504
                AND ROWNUM=1
              FOR UPDATE;

              UPDATE t7100_case_information
                SET c901_case_status = 19525 -- Cancelled
                , c7100_last_updated_by = p_userid
                , c7100_last_updated_date = CURRENT_DATE
               WHERE c7100_case_info_id = p_txn_id
                  AND c901_type = 1006504;
            END IF;
          EXCEPTION WHEN OTHERS THEN
            raise_application_error('-20999','Error while Cancelling Event.' || '<BR>Error :' || SQLERRM );
          END;

        END gm_sav_void_txn;

       /*******************************************************
        * Description : Procedure to handle appending output string
        * Author      : Yoga
        *******************************************************/
        PROCEDURE fch_output_msg(
          p_out_str      IN OUT  VARCHAR2
        )
        AS
          v_txn_temp_str    VARCHAR2(4000);
        BEGIN
          -- Handle if message exceeds its maximum limit
          BEGIN
            v_txn_temp_str:= v_txn_temp_str || p_out_str;

          EXCEPTION WHEN OTHERS THEN
            v_txn_temp_str := v_txn_temp_str || 'Error :'|| SQLERRM || '<BR>';
          END;

        END fch_output_msg;

        /******************************************************************************************
          * Description : Get the Event name for the given product request id
          * Author: Yoga
        *******************************************************************************************/	
        FUNCTION get_event_name(
          p_prod_req_id     IN     t526_product_request_detail.c525_product_request_id%TYPE
        )
        RETURN VARCHAR2
        IS
          v_event_name t7103_schedule_info.c7103_name%TYPE;
        BEGIN
          BEGIN
            SELECT t7103.c7103_name ename
              INTO v_event_name
              FROM t7103_schedule_info t7103, t7104_case_set_information t7104, 
                   t526_product_request_detail t526
             WHERE t7103.c7100_case_info_id = t7104.c7100_case_info_id
                AND t526.c526_product_request_detail_id = t7104.c526_product_request_detail_id
                AND t526.c525_product_request_id = p_prod_req_id
                AND t526.c526_void_fl is null
                AND t7104.c7104_void_fl is null
                AND ROWNUM =1;
          EXCEPTION WHEN NO_DATA_FOUND THEN
            v_event_name := NULL;
          END;
        
          RETURN v_event_name;
        END get_event_name;
        /******************************************************************************************
          * Description : Get the Event name for the given product request id
          * Author: Yoga
        *******************************************************************************************/	
        FUNCTION get_event_status(
          p_prod_req_id     IN     t526_product_request_detail.c525_product_request_id%TYPE
        )
        RETURN NUMBER
        IS
          v_event_status t7100_case_information.c901_case_status%TYPE;
        BEGIN
          BEGIN
            SELECT t7100.c901_case_status
              INTO v_event_status
              FROM t7103_schedule_info t7103, t7104_case_set_information t7104, t7100_case_information t7100, 
                   t526_product_request_detail t526
              WHERE t7103.c7100_case_info_id = t7104.c7100_case_info_id
                AND t526.c526_product_request_detail_id = t7104.c526_product_request_detail_id
                AND t7103.c7100_case_info_id = t7100.c7100_case_info_id
                AND t526.c525_product_request_id = p_prod_req_id
                AND t7104.c7104_void_fl is null
                AND ROWNUM =1;
          EXCEPTION WHEN NO_DATA_FOUND THEN
            v_event_status := NULL;
          END;
        
          RETURN v_event_status;
        END get_event_status;
        
    /********************************************************************
	* Description : Procedure to void the Favorite Set/Item Information
	* Author      : Hreddi
	*********************************************************************/
	PROCEDURE gm_sav_void_favorite (
	    p_favorite_id  IN	t7120_favourite_case.c7120_favourite_case_id%TYPE,
	    p_userid	   IN	t7120_favourite_case.c7120_created_by%TYPE
	)
	AS
	BEGIN
		UPDATE t7120_favourite_case 
		   SET c7120_void_fl           = 'Y'
		     , c7120_last_updated_date = CURRENT_DATE 
		     , c7120_last_updated_by   = p_userid 
		 WHERE c7120_favourite_case_id = p_favorite_id;
	END gm_sav_void_favorite;

	/********************************************************************
	* Description : Function to get type of parent request of an Inhouse transaction
	* Author      : Yoga
	*********************************************************************/
        FUNCTION get_parent_txn_type (
            p_txn_id IN t412_inhouse_transactions.c412_inhouse_trans_id%TYPE
        )
        RETURN NUMBER
        IS
          v_txn_type t412_inhouse_transactions.c901_ref_type%TYPE;
        BEGIN
          BEGIN
            SELECT t412.c901_ref_type
              INTO v_txn_type
              FROM t412_inhouse_transactions t412
             WHERE t412.c412_inhouse_trans_id = p_txn_id;
            
          EXCEPTION WHEN NO_DATA_FOUND THEN
            v_txn_type := NULL;
          END;
          return v_txn_type;
        END get_parent_txn_type;

END gm_pkg_sm_eventbook_txn;
/
