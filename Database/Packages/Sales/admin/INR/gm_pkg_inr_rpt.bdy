CREATE OR REPLACE PACKAGE BODY gm_pkg_inr_rpt
IS
/* *******************************************************************************
 * Author: Agilan singaravel
 * Procedure:gm_fch_acc_dtl
 * Description:Fetch account and sales details based on GPS ID
 ***********************************************************************************/
PROCEDURE  gm_fch_acc_dtl (
    p_gps_id	 		IN     t5050_fleet_info.c5050_egps_serial_no%type,
    p_part_num			IN	   VARCHAR2,
    p_out_cursor 		OUT    CLOB)
  AS
  	v_fleet_info_id			t5050_fleet_info.c5050_fleet_info_id%type;
  	v_acc_id				t704_account.c704_account_id%type;
	v_party_ids				CLOB;
	v_acc_ids				CLOB;
 	v_acc_details     		CLOB;
 	v_acc_sales_details		CLOB;
 	v_json_out		   	    json_object_t;
 	
  	CURSOR fetch_party_id
  	IS
  		SELECT c101_account_party_id party_id
  		  FROM T5051_FLEET_AFFILIATED_ACCOUNT_INFO
  		 WHERE c5050_fleet_info_id = v_fleet_info_id
  		   AND c5051_void_fl is null;
  		   
 	CURSOR fch_acc_id
 	IS
 		SELECT c704_account_id accid
 		  FROM T704_ACCOUNT
 		 WHERE c101_party_id in (select token from v_clob_list)
 		   AND c704_void_fl is null;
  BEGIN
	  
	  SELECT c5050_fleet_info_id,c704_account_id
	    INTO v_fleet_info_id,v_acc_id
	    FROM T5050_FLEET_INFO
	   WHERE c5050_egps_serial_no = p_gps_id
	     AND c5050_void_fl is null;
--	     
	 FOR v_fch_party_id IN fetch_party_id
     LOOP
     	 v_party_ids := v_party_ids || ',' ||v_fch_party_id.party_id;
     END LOOP;
     
     my_context.set_my_cloblist(v_party_ids||',');
--     
     FOR v_fch_acc_id IN fch_acc_id
     LOOP
     	 v_acc_ids := v_acc_ids || ',' ||v_fch_acc_id.accid;
     END LOOP;
     
     my_context.set_my_cloblist(v_acc_ids||',');
     
     SELECT JSON_ARRAYAGG (JSON_OBJECT(
     		'fleetid'	  VALUE		p_gps_id,
        	'accid'       VALUE 	c704_account_id,
        	'accname'     VALUE 	c704_account_nm,
        	'acccurr' 	  VALUE		c901_currency,
        	'repname'     VALUE 	get_rep_name(c703_sales_rep_id),
        	'shipaddr1'   VALUE 	c704_ship_add1,
        	'shipaddr2'   VALUE 	c704_ship_add2,
        	'shipcity'    VALUE     c704_ship_city,
        	'shipstate'   VALUE     get_code_name(c704_ship_state),
        	'shipcountry' VALUE     get_code_name(c704_ship_country),
        	'shipzipcode' VALUE     c704_ship_zip_code)
   ORDER BY c704_account_id ASC RETURNING CLOB) 
   	   INTO v_acc_details   
   	   FROM T704_ACCOUNT
      WHERE c704_account_id in (select token from v_clob_list)
        AND c704_void_fl is nulL; 
        
        my_context.set_my_inlist_ctx (p_part_num);
        
    SELECT JSON_ARRAYAGG (JSON_OBJECT(    
        	'accid'      VALUE  t501.c704_Account_id,
        	'pnum'       VALUE  t502.c205_part_number_id,
        	'pdesc'      VALUE  get_partnum_desc(t502.c205_part_number_id),
        	'pqty'       VALUE  c502_item_qty,
        	'orddate'    VALUE  c501_order_date,
        	'ordid'      VALUE  t501.c501_order_id,
        	'ordsalamt'	 VALUE	t501.c501_total_cost,
        	'ordshipdt'  VALUE  c501_shipping_date,
        	'ordfsname'  VALUE  get_distributor_name(t501.c501_distributor_id))
   ORDER BY t501.c704_account_id ASC RETURNING CLOB)           
       INTO v_acc_sales_details    
       FROM T704_ACCOUNT t704, T501_ORDER t501,T502_ITEM_ORDER T502
      WHERE t704.c704_account_id = t501.c704_account_id
        AND t501.c501_order_id = t502.c501_order_id
        AND t704.c704_account_id in (select token from v_clob_list)
        --AND t502.c205_part_number_id in (select c906_rule_value from t906_rules where c906_rule_id='PART' and c906_rule_grp_id='ESTREAMAPP' and c906_void_fl is null)
        AND t502.c205_part_number_id in (select token from v_in_list)
        AND c704_void_fl is null        
        AND c501_void_fl IS null; 
     
    v_json_out     := JSON_OBJECT_T.parse('{}');
    
    IF v_acc_details !=empty_clob() THEN 
         v_json_out.put('accdetails',JSON_ARRAY_T(v_acc_details));
    END IF;
    
    IF v_acc_sales_details !=empty_clob() THEN 
      	v_json_out.put('accsalesdetails',JSON_ARRAY_T(v_acc_sales_details));
    END IF;
    
    p_out_cursor := v_json_out.to_string;
	      
 END gm_fch_acc_dtl;
END gm_pkg_inr_rpt;
/