-- @"C:\Database\Packages\Sales\gm_pkg_sm_admin_dealer_txn.bdy";
CREATE OR REPLACE
PACKAGE BODY gm_pkg_sm_admin_dealer_txn
IS
    /****************************************************************
    * Description : This procedure used to save the Invoice details
    *               for the dealer
    * Author      : rdinesh
    *****************************************************************/
  PROCEDURE gm_sav_dealer_inv_details (
 
  p_dealer_id       		IN     	t101_party_invoice_details.C101_PARTY_ID%TYPE,
  p_inv_cust_type   		IN    	t101_party_invoice_details.C901_INVOICE_CUST_TYPE%TYPE,
  p_email_fl        		IN    	t101_party_invoice_details.C101_EMAIL_FL%TYPE,
  p_email_file_fmt			IN		t101_party_invoice_details.C901_EMAIL_FILE_FMT%TYPE,
  p_recipt_email			IN		t101_party_invoice_details.C101_RECIPIENTS_EMAIL%TYPE,
  p_csv_template_nm			IN		t101_party_invoice_details.C901_CSV_TEMPLATE_NM%TYPE,
  p_inv_layout				IN		t101_party_invoice_details.C901_INVOICE_LAYOUT%TYPE,
  p_vendor_id				IN		t101_party_invoice_details.C101_VENDOR_ID%TYPE,
  p_payment_terms			IN		t101_party_invoice_details.c901_payment_terms%TYPE,
  p_credit_rate				IN		t101_party_invoice_details.C101_CREDIT_RATING%TYPE,
  p_collector_id			IN		t101_party_invoice_details.C901_COLLECTOR_ID%TYPE,
  p_inv_close_dt			IN		t101_party_invoice_details.C901_INVOICE_CLOSING_DATE%TYPE,
  p_user_id					IN		t101_party_invoice_details.C101_CREATED_BY%TYPE ,
  p_consolidated_bill_flag	IN		t101_party_invoice_details.C101_CONSOLIDATED_BILL_FLAG%TYPE)
  
  
  AS
  
  v_dealer_inv_id   t101_party_invoice_details.C101_PARTY_INVOICE_DTLS_ID%TYPE;
  
  BEGIN
  
  		UPDATE t101_party_invoice_details
		  SET c901_invoice_cust_type    = p_inv_cust_type,
			  c101_email_fl            = p_email_fl,
			  c901_email_file_fmt       = p_email_file_fmt,
			  c101_recipients_email    = p_recipt_email,
			  c901_csv_template_nm      = p_csv_template_nm,
			  c901_invoice_layout       = p_inv_layout,
			  c101_vendor_id           = p_vendor_id,
			  c901_payment_terms       = p_payment_terms ,
			  c101_credit_rating       = p_credit_rate,
			  c901_collector_id         = p_collector_id ,
			  c901_invoice_closing_date = p_inv_close_dt,
			  c101_last_updated_by     = p_user_id ,
			  c101_last_updated_date   = CURRENT_DATE,
			  C101_CONSOLIDATED_BILL_FLAG = p_consolidated_bill_flag
		WHERE C101_party_id            = p_dealer_id ;
		
		
		IF (SQL%ROWCOUNT = 0)
        THEN
        
         SELECT s101_party_invocie_details.NEXTVAL
           INTO v_dealer_inv_id
           FROM DUAL;

         INSERT INTO t101_party_invoice_details
                    (
        			 C101_PARTY_INVOICE_DTLS_ID, C101_PARTY_ID, C901_INVOICE_CUST_TYPE,  c101_email_fl, C901_EMAIL_FILE_FMT,
        			 c101_recipients_email, C901_CSV_TEMPLATE_NM, C901_INVOICE_LAYOUT, c101_vendor_id, c901_payment_terms,
        			 c101_credit_rating, C901_COLLECTOR_ID, C901_INVOICE_CLOSING_DATE,
        			 C101_CREATED_BY, C101_CREATED_DATE,C101_CONSOLIDATED_BILL_FLAG
        			)
        	 VALUES (
        	 		 v_dealer_inv_id, p_dealer_id, p_inv_cust_type, p_email_fl, p_email_file_fmt,
        	 		 p_recipt_email, p_csv_template_nm, p_inv_layout, p_vendor_id, p_payment_terms,
        	 		 p_credit_rate, p_collector_id, p_inv_close_dt, 
        	 		 p_user_id,  CURRENT_DATE,p_consolidated_bill_flag
        	 		);
        
      	END IF;
   END gm_sav_dealer_inv_details;
     /****************************************************************
    * Description : This procedure used to sync values for Associated Accounts
    *               for the dealer
    * Author      : rdinesh
    *****************************************************************/   
   
 PROCEDURE gm_sync_dealer_info_to_account (
  	
  	p_dealer_id           IN  	t101_party_invoice_details.C101_PARTY_ID%TYPE,
  	p_account_id  		  IN    T704_ACCOUNT.C704_ACCOUNT_ID%TYPE,
  	p_user_id			  IN	t101_party_invoice_details.C101_CREATED_BY%TYPE 
  	
  	)
  	
  	AS
  	
  	v_accInputStr 				VARCHAR2(4000);
  	v_dealer_inv_info_out       Types.cursor_type;
  	v_dealer_address_info_out   Types.cursor_type;
  	
  	v_inv_cust_type             t101_party_invoice_details.C901_INVOICE_CUST_TYPE%TYPE;
    v_email_fl              	t101_party_invoice_details.c101_email_fl%TYPE;
  	v_email_file_fmt			t101_party_invoice_details.C901_EMAIL_FILE_FMT%TYPE;
  	v_recipt_email				t101_party_invoice_details.c101_recipients_email%TYPE;
  	v_csv_template_nm			t101_party_invoice_details.C901_CSV_TEMPLATE_NM%TYPE;
  	v_inv_layout				t101_party_invoice_details.C901_INVOICE_LAYOUT%TYPE;
  	v_vendor_id					t101_party_invoice_details.c101_vendor_id%TYPE;
  	v_payment_terms				t101_party_invoice_details.c901_payment_terms%TYPE;
  	v_credit_rate				t101_party_invoice_details.c101_credit_rating%TYPE;
  	v_collector_id				t101_party_invoice_details.C901_COLLECTOR_ID%TYPE;
  	v_consolidate_billing       t101_party_invoice_details.C101_CONSOLIDATED_BILL_FLAG%TYPE;
    v_inv_close_dt				t101_party_invoice_details.C901_INVOICE_CLOSING_DATE%TYPE;
  	
  	v_billname			        T106_ADDRESS.c106_BILL_NAME%TYPE;
  	v_addline1				 	T106_ADDRESS.C106_ADD1%TYPE;
	v_addline2				 	T106_ADDRESS.C106_ADD2%TYPE;
  	v_city					 	T106_ADDRESS.C106_CITY%TYPE;
  	v_state 				 	VARCHAR2(100);
  	v_country				 	VARCHAR2(100);
  	v_zipcode				 	T106_ADDRESS.C106_ZIP_CODE%TYPE;
	v_contact_person			T704_ACCOUNT.C704_CONTACT_PERSON%TYPE;
	v_phone						T704_ACCOUNT.C704_PHONE%TYPE;
	v_fax						T704_ACCOUNT.C704_FAX%TYPE;
	
	
	  	CURSOR c_dealer_acct_cur 
	  	IS
  			SELECT c704_account_id acct_id
  			  FROM t704_account
 			 WHERE c101_dealer_id = p_dealer_id
   			   AND c704_account_id  = NVL(p_account_id,c704_account_id)
   			   AND c704_void_fl    IS NULL;
   			   
   	    CURSOR c_dealer_contact_cur
   	    IS
   	    	SELECT   c901_mode cmodeid, c107_contact_value cvalue
				 FROM t107_contact
				WHERE c101_party_id = p_dealer_id AND c107_void_fl IS NULL;
	
  			
  	BEGIN
  			   
		BEGIN
		 SELECT c901_invoice_cust_type, c101_email_fl, c901_email_file_fmt,c101_recipients_email,
		        c901_csv_template_nm, c901_invoice_layout, c101_vendor_id, c901_payment_terms,
  				c101_credit_rating, c901_collector_id, c901_invoice_closing_date,C101_CONSOLIDATED_BILL_FLAG
  		   INTO v_inv_cust_type,v_email_fl,v_email_file_fmt,v_recipt_email,
  		        v_csv_template_nm,v_inv_layout,v_vendor_id,v_payment_terms,
  		        v_credit_rate,v_collector_id,v_inv_close_dt,v_consolidate_billing
  	 	   FROM t101_party_invoice_details
  		  WHERE C101_PARTY_ID = p_dealer_id 
  			AND c101_void_fl is null;
  		
  		 EXCEPTION WHEN NO_DATA_FOUND
  		THEN
  			v_inv_cust_type := '';
  		END;

  		-- Fetching dealer addresss info
  		 BEGIN
  		  SELECT c106_bill_name,c106_add1,c106_add2,c106_city, 
  		  		 c901_state, c901_country, c106_zip_code 
			INTO v_billname,v_addline1,v_addline2,v_city,
				 v_state,v_country,v_zipcode
				 FROM t106_address
				WHERE c101_party_id = p_dealer_id 
				  AND c106_void_fl IS NULL
				  AND c106_primary_fl = 'Y';
  		 EXCEPTION WHEN NO_DATA_FOUND
  		THEN
  			v_inv_cust_type := '';
  		END;
  		
  		--Fetching dealer contact info
  		FOR v_dealer_contact_cur IN c_dealer_contact_cur
  		LOOP
  		
  			IF v_dealer_contact_cur.cmodeid = '26230748' --contact peson
  			THEN 
  				v_contact_person := v_dealer_contact_cur.cvalue;
  			ELSIF v_dealer_contact_cur.cmodeid = '90450' --phone
  			THEN
  				v_phone := v_dealer_contact_cur.cvalue;
  			ELSIF v_dealer_contact_cur.cmodeid = '90454' --fax
  			THEN
  				v_fax := v_dealer_contact_cur.cvalue;
			END IF;
  		END LOOP;	
  		
  		
  		   UPDATE t704_account
			  SET c704_contact_person   = v_contact_person,
				  c704_phone            = v_phone,
				  c704_fax              = v_fax ,
				  C704_BILL_NAME        = v_billname,
				  c704_bill_add1        = v_addline1 ,
				  c704_bill_add2        = v_addline2 ,
				  c704_bill_city        = v_city ,
				  c704_bill_state       = v_state,
				  c704_bill_country     = v_country,
				  c704_bill_zip_code    = v_zipcode ,
				  c704_payment_terms    = v_payment_terms ,
				  c704_credit_rating    = v_credit_rate,
				  c704_last_updated_by  = p_user_id,
  				  c704_last_updated_date= CURRENT_DATE
				WHERE c101_dealer_id    = p_dealer_id
				AND c704_account_id     = NVL(p_account_id,c704_account_id)
				AND c704_void_fl    IS NULL; 
  		
  		
  		
  		v_accInputStr := '91983^' || v_email_fl || '|' || '91984^' || v_email_file_fmt || '|' || '91985^' || v_recipt_email || '|' || 
  		                 '91986^' || v_csv_template_nm || '|' || '10304532^' || v_inv_layout || '|' || '106642^' || v_vendor_id || '|' ||
  		                 '103050^' || v_collector_id || '|' || '106200^' || v_inv_cust_type || '|' || '106201^' || v_consolidate_billing ||  '|' || '106202^' || v_inv_close_dt || '|' ;
  		
  
  		FOR v_dealer_acct_id_cur IN c_dealer_acct_cur
  		LOOP
  				
			gm_save_account_attribute(v_dealer_acct_id_cur.acct_id,v_accInputStr,'',p_user_id);
  			
  		END LOOP;	
  	
  END gm_sync_dealer_info_to_account;

 END gm_pkg_sm_admin_dealer_txn;
/