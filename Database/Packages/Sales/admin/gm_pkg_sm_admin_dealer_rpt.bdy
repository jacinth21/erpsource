-- @"C:\Database\Packages\Sales\admin\gm_pkg_sm_Dealer_rpt.bdy";
CREATE OR REPLACE
PACKAGE BODY gm_pkg_sm_admin_dealer_rpt
IS
      /****************************************************************
    * Description : This procedure used to fetch Dealer Invoice parameter values
    *              
    * Author      : ckumar
    *****************************************************************/ 
   
 PROCEDURE gm_fch_dealer_inv_details (
 
  p_dealer_id    IN      t101_party_invoice_details.C101_party_id%TYPE,
  p_out          OUT     Types.cursor_type
 )
  
  AS 
  BEGIN
  
OPEN p_out
FOR
             select 
		      c901_invoice_cust_type    INVOICECUSTOMERTYPE,
			  c101_email_fl             ELECTRONICEMAIL,
			  c901_email_file_fmt       ELECRONICFILEFORMAT,
			  C101_recipients_email     RECEIPIENTSEMAIL,
			  c901_csv_template_nm      CSVTEMPLATTENAME,
			  c901_invoice_layout       INVOICELAYOUT,
			  c101_vendor_id            VENDOR,
			  c901_payment_terms        PAYMENTTERMS ,
			  c101_credit_rating        CREDITRATING,
			  c901_collector_id         COLLETORID ,
			  c901_invoice_closing_date INVOICECLOSINGDATE,
			  c101_last_updated_by      USERID ,
			  c101_last_updated_date    UPDATEDDATE,
			  NVL(t704.acctcnt,0) acctcount,
			  C101_CONSOLIDATED_BILL_FLAG CONSOLIDATEDBILLING
   FROM t101_party_invoice_details t101p, (
         SELECT c101_dealer_id, COUNT (1) acctcnt
           FROM t704_account
          WHERE c704_void_fl   IS NULL
            AND c101_dealer_id IS NOT NULL
       GROUP BY c101_dealer_id
    )
    t704
  WHERE t101p.C101_party_id = t704.c101_dealer_id(+)
    and t101p.c101_void_fl is null
    and t101p.C101_party_id = p_dealer_id ;
		

   END gm_fch_dealer_inv_details;
    /****************************************************************
    * Description : This procedure is used to fetch values for Dealer Accounts Mapping report
    * Author      : ckumar
    *****************************************************************/   
   PROCEDURE gm_fch_dealer_Acct_details (
 
  p_dealer_id    IN      t101_party_invoice_details.C101_PARTY_ID%TYPE,
  p_active_fl    IN      t101_party.c101_active_fl%TYPE,
  p_out          OUT     Types.cursor_type
 )
  
  AS 
   v_query  VARCHAR2(4000);
   V_COMPANY_ID NUMBER;
  BEGIN
  
     SELECT get_compid_frm_cntx() INTO V_COMPANY_ID   FROM dual;
		
	 v_query :=  'SELECT t101.c101_party_id DEALERID,get_party_name(t101.c101_party_id) DEALERNAME,
			  t106.C106_ADD1 BILLADDRESS,t101p.C101_CONSOLIDATED_BILL_FLAG COLSOLIDATEDBILLING,
			  t106.C106_CITY CITY,get_code_name(t101p.C901_INVOICE_CLOSING_DATE) INVCLOSEDATE,
			  t107.contactNumber CONTACTNUMBER,get_code_name(t106.C901_STATE) STATE,
			  T107.cperson CPERSON,t106.C106_ZIP_CODE ZIP,T107.fax,
			  get_code_name(t101p.C901_PAYMENT_TERMS) PAYMENTTERMS,get_dealer_address(t101.c101_party_id) DEALERADDRESS,
			  t101p.C901_INVOICE_CLOSING_DATE invClosingDate,c101_party_nm_en DEALERNAMEEN
			FROM t101_party t101,
			  t106_address t106,
			  t101_party_invoice_details t101p,
			 (select c101_party_id, MAX(decode(c901_mode,90450,C107_CONTACT_VALUE)) contactNumber,
                 MAX(decode(c901_mode,26230748,C107_CONTACT_VALUE)) cperson ,MAX(decode(c901_mode,90454,C107_CONTACT_VALUE)) fax from t107_contact where c107_void_fl  IS NULL GROUP BY c101_party_id) t107
			WHERE t101.c101_party_id = t106.c101_party_id(+)
			AND t101.c101_party_id   = t101p.c101_party_id(+)
		    AND t107.c101_party_id(+) = t101.c101_party_id
		    AND t101.C1900_COMPANY_ID = '||V_COMPANY_ID||'
			AND t106.C106_PRIMARY_FL(+) =  ''Y''
			AND t101.C901_PARTY_TYPE = ''26230725'''; --Dealer
			
	IF (p_active_fl !=0 AND p_active_fl='1006430' ) THEN
	
		v_query := v_query || ' AND NVL(t101.c101_active_fl,''Y'')=''Y''' ;
		
	ELSIF   (p_active_fl !=0 AND p_active_fl='1006431' ) THEN
	    
		v_query := v_query || ' AND t101.c101_active_fl = ''N''';
	
	END IF;
	

			
		v_query := v_query || '	AND t101.c101_party_id = decode('''|| p_dealer_id || ''','''',t101.c101_party_id,''0'',t101.c101_party_id,''' || p_dealer_id || ''')
								AND t101.c101_void_fl   IS NULL
								AND t106.c106_void_fl(+)   IS NULL';
			
								
			OPEN p_out  FOR v_query  ;
	
	   END gm_fch_dealer_Acct_details;	
	   
    /****************************************************************
    * Description : This procedure is used to fetch Associated accounts for Dealer
    * Author      : ckumar
    *****************************************************************/ 
	   
PROCEDURE gm_fch_dealer_Accounts (
 
  p_dealer_id    IN      t101_party_invoice_details.C101_PARTY_ID%TYPE,
  p_out          OUT     Types.cursor_type
 )
  
  AS 
 
  BEGIN
	  OPEN p_out
		FOR
  		
			SELECT c704_ACCOUNT_NM accountName
			      ,t704.c704_account_id accountid
				  ,get_party_name(t101.c101_party_id) dealername
			      ,d_name dname
			      ,rep_name rpname		
			      ,t704.c101_dealer_id dealerid
			  FROM t704_account t704,
					t101_party t101,
					v700_territory_mapping_detail v700
			 WHERE v700.ac_id = t704.c704_account_id
			   AND  t704.c101_party_id  = t101.c101_party_id
			   AND t704.c101_dealer_id=p_dealer_id
			   AND t704.C704_ACTIVE_FL ='Y'
   		       AND t704.c704_void_fl   IS NULL
   		       AND t101.c101_void_fl IS NULL;
   		      

 END gm_fch_dealer_Accounts;	

END gm_pkg_sm_admin_dealer_rpt;
/