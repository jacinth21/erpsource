CREATE OR REPLACE PACKAGE BODY gm_pkg_item_apprv_txn
IS
   /******************************************************************************
     * Description : Procedure to save approve and BO request from item consignment
     * Author       : smanimaran
    ******************************************************************************/
PROCEDURE gm_sav_request_frm_item(
		p_str_apprv_qty_string	  	IN	CLOB,
		p_str_bo_qty_string 	  	IN	CLOB,
		P_out_request_id	  		OUT	VARCHAR2,
		P_out_consgn_id	   	  		OUT	VARCHAR2,
		p_out_bo_id					OUT VARCHAR2
)
AS

BEGIN
IF p_str_bo_qty_string IS NOT NULL THEN
 gm_pkg_item_apprv_txn.gm_sav_bo_request(p_str_bo_qty_string,p_out_bo_id);
END IF;
IF p_str_apprv_qty_string IS NOT NULL THEN
gm_pkg_item_apprv_txn.gm_sav_apprv_request(p_str_apprv_qty_string,P_out_request_id,P_out_consgn_id);
END IF;

END gm_sav_request_frm_item;

/******************************************************************************
   * Description : Procedure to save the back order request from item consignment
   * Author       : smanimaran
******************************************************************************/
PROCEDURE gm_sav_bo_request(
		p_str_bo_qty_string 	  	IN	CLOB,
		p_out_bo_id					OUT VARCHAR2
	)

	AS 
		v_bo_json             	json_array_t;
		v_bo_item       	  	json_object_t;
		v_OUTPUT_REQUEST_ID		VARCHAR2(4000);
		v_OUT_REQ_ID			VARCHAR2(4000);
		v_request_id	 		VARCHAR2(1000);
        v_part_number_id	 	VARCHAR2(1000);
        v_bo_qty 		    	VARCHAR2(1000);
        v_date_format 			VARCHAR2(100);
        v_out_request_detail	VARCHAR2(4000);
        v_PAR_REQ_ID			T525_PRODUCT_REQUEST.C525_PRODUCT_REQUEST_ID%TYPE;
        v_REQ_FOR_ID			T525_PRODUCT_REQUEST.C525_REQUEST_FOR_ID%TYPE;
        v_REQUEST_FOR			T525_PRODUCT_REQUEST.C901_REQUEST_FOR%TYPE;
        v_RQ_STR				VARCHAR2(50);
        v_SHIP_TO_ID			T907_SHIPPING_INFO.C907_SHIP_TO_ID%TYPE;
        v_SHIP_TO				T907_SHIPPING_INFO.C901_SHIP_TO%TYPE;
        v_SHIP_CAR				T907_SHIPPING_INFO.C901_DELIVERY_CARRIER%TYPE;
        v_SHIP_MODE				T907_SHIPPING_INFO.C901_DELIVERY_MODE%TYPE;
        v_ADDR_ID				T907_SHIPPING_INFO.C106_ADDRESS_ID%TYPE;
        v_FRGT_AMT				T907_SHIPPING_INFO.C907_FRIEGHT_AMT%TYPE;
        v_REQ_BY_ID				T525_PRODUCT_REQUEST.C525_REQUEST_BY_ID%TYPE;
        v_REP_ID				T525_PRODUCT_REQUEST.C703_SALES_REP_ID%TYPE;
        v_REQUIRE_DATE			T526_PRODUCT_REQUEST_DETAIL.C526_REQUIRED_DATE%TYPE;
        v_USER_ID				T525_PRODUCT_REQUEST.C525_CREATED_BY%TYPE;
        v_ACCT_ID				T525_PRODUCT_REQUEST.C704_ACCOUNT_ID%TYPE;
        v_PLAN_SHIP_DT			T907_SHIPPING_INFO.C907_SHIP_TO_DT%TYPE;
        V_OUT_SHIP_ID 			VARCHAR2(4000);
        v_out_request_detail_id t521_request_detail.c521_request_detail_id%TYPE;
	BEGIN
	
	v_date_format := GET_RULE_VALUE('DATEFMT', 'DATEFORMAT');
	v_bo_json := json_array_t (p_str_bo_qty_string);
	
	FOR i IN 0..v_bo_json.get_size - 1 
        LOOP
        
        v_bo_item 					:=      TREAT(v_bo_json.get(i) AS json_object_t);
        v_request_id	 		   	:=		v_bo_item.get_string('strReqId');
        v_part_number_id	 		:=		v_bo_item.get_string('strPartNo');
        v_bo_qty 		    		:=		v_bo_item.get_string('strBoQty');
        
        SELECT T525.C525_PRODUCT_REQUEST_ID 	,
		T525.C525_REQUEST_FOR_ID 	,
		T525.C901_REQUEST_FOR 		,
		RTRIM (XMLAGG (xmlelement (E, T526.C205_PART_NUMBER_ID
		|| '^'
		||v_bo_qty
		||'^'
		||TO_CHAR(T526.C526_Required_date,v_date_format)
		||'^|')) .extract ('//text()'), ',') ,
		T907.C907_SHIP_TO_ID 		,
		T907.C901_SHIP_TO 			,
		T907.C901_DELIVERY_CARRIER 	,
		T907.C901_DELIVERY_MODE 	,
		T907.C106_ADDRESS_ID 		,
		T907.C907_FRIEGHT_AMT 		,
		T525.C525_REQUEST_BY_ID 	,
		T525.C703_SALES_REP_ID 		,
		T526.C526_REQUIRED_DATE 	,
		T525.C525_CREATED_BY 		,
		T525.C704_ACCOUNT_ID 		,
		T907.C907_SHIP_TO_DT 		
		INTO
		v_PAR_REQ_ID,v_REQ_FOR_ID,v_REQUEST_FOR,
		v_RQ_STR,v_SHIP_TO_ID,v_SHIP_TO,v_SHIP_CAR,
		v_SHIP_MODE,v_ADDR_ID,v_FRGT_AMT,v_REQ_BY_ID,
		v_REP_ID,v_REQUIRE_DATE,v_USER_ID,v_ACCT_ID,v_PLAN_SHIP_DT
		FROM T525_PRODUCT_REQUEST T525,
		T526_PRODUCT_REQUEST_DETAIL T526,
		T907_SHIPPING_INFO T907, t7100_case_information t7100,t7104_case_set_information T7104
		WHERE T525.C525_VOID_FL IS NULL
		AND T526.C526_VOID_FL IS NULL
		AND T907.C907_VOID_FL IS NULL
		AND T526.C205_PART_NUMBER_ID IS NOT NULL
		AND T525.C525_PRODUCT_REQUEST_ID = T526.C525_PRODUCT_REQUEST_ID
		AND T525.C525_PRODUCT_REQUEST_ID = T907.C525_PRODUCT_REQUEST_ID
		AND t7100.c7100_case_info_id = T7104.C7100_CASE_INFO_ID
		AND t7104.c526_product_request_detail_id = T526.C526_PRODUCT_REQUEST_DETAIL_ID
		AND t907.c907_ref_id = T526.C526_PRODUCT_REQUEST_DETAIL_ID
		AND T907.C907_SHIP_TO_ID IS NOT NULL
		AND T907.C901_SHIP_TO IS NOT NULL
		-- below condition is commented, since we dnt have any sales rep id for consignment items.
		--AND T525.C703_SALES_REP_ID IS NOT NULL
		AND T526.C526_STATUS_FL =10 --open
		AND T526.C526_PRODUCT_REQUEST_DETAIL_ID = v_request_id
		and T526.C205_PART_NUMBER_ID = v_part_number_id
		GROUP BY T525.C525_PRODUCT_REQUEST_ID,
		T525.C525_REQUEST_FOR_ID,
		T525.C703_SALES_REP_ID,
		T907.C907_SHIP_TO_ID,
		T907.C901_SHIP_TO,
		T907.C901_DELIVERY_CARRIER,
		T907.C901_DELIVERY_MODE,
		T907.C106_ADDRESS_ID,
		T907.C907_FRIEGHT_AMT,
		T525.C525_REQUEST_BY_ID ,
		T525.C525_CREATED_BY,
		T525.C704_ACCOUNT_ID,
		T907.C907_SHIP_TO_DT,
		T526.C526_Required_date,
		T525.C901_REQUEST_FOR
		ORDER BY T525.C525_PRODUCT_REQUEST_ID;

		--To save BO
		gm_pkg_op_request_master.gm_sav_request (v_request_id , v_REQUIRE_DATE ,v_PLAN_SHIP_DT, '50618', NULL, NULL, '40021', v_REQ_FOR_ID,
		'50625', v_REQ_BY_ID,v_SHIP_TO, v_SHIP_TO_ID, NULL , 10, v_USER_ID, '50060', v_OUT_REQ_ID, NULL);
		
		gm_pkg_op_request_master.gm_sav_request_detail (v_OUT_REQ_ID ,v_part_number_id,v_bo_qty,v_out_request_detail);
        
        UPDATE T520_request
			SET c520_ref_id       = v_PAR_REQ_ID
			WHERE C520_request_id = v_OUT_REQ_ID
			AND c520_void_fl IS NULL;
			
		IF v_OUT_REQ_ID IS NOT NULL
		    THEN
		gm_pkg_cm_shipping_trans.gm_sav_shipping(v_OUT_REQ_ID,'50184',v_SHIP_TO,v_SHIP_TO_ID,
		        	v_SHIP_CAR,v_SHIP_MODE,NULL,v_ADDR_ID,v_FRGT_AMT,v_USER_ID,V_OUT_SHIP_ID,NULL,NULL,NULL,NULL);
		END IF;
		
        v_OUTPUT_REQUEST_ID := v_OUTPUT_REQUEST_ID ||v_OUT_REQ_ID || ',';
        END LOOP;
        p_out_bo_id := v_OUTPUT_REQUEST_ID;

END gm_sav_bo_request;
	   /******************************************************************************
     * Description : Procedure to save the approve request from item consignment
     * Author       : smanimaran
    ******************************************************************************/
PROCEDURE gm_sav_apprv_request(
		p_str_apprv_qty_string	  	IN	CLOB,
		P_out_request_id	  		OUT	VARCHAR2,
		P_out_consgn_id	   	  		OUT	VARCHAR2
)

AS 
		v_apprv_request_json       	  	json_array_t;
		v_apprv_request_item       	  	json_object_t;
       	v_request_id	 		   		VARCHAR2(1000);
        v_part_number_id	 			VARCHAR2(1000);
        v_apprv_qty 		    		VARCHAR2(1000);
        V_OUT_SHIP_ID 					VARCHAR2(4000);
		v_date_format 					VARCHAR2(100);
		v_req_id 						VARCHAR2(50);
		v_OUTPUT_REQUEST_ID				VARCHAR2(4000);
		v_OUTPUT_CONSIGN_ID				VARCHAR2(4000);
		v_OUT_REQ_ID					VARCHAR2(1000);
		v_OUT_CONSIGN_ID				VARCHAR2(4000);
		
		v_PAR_REQ_ID			T525_PRODUCT_REQUEST.C525_PRODUCT_REQUEST_ID%TYPE;
        v_REQ_FOR				T525_PRODUCT_REQUEST.C525_REQUEST_FOR_ID%TYPE;
        v_REQUEST_FOR			T525_PRODUCT_REQUEST.C901_REQUEST_FOR%TYPE;
        v_RQ_STR				VARCHAR2(50);
        v_SHIP_TO_ID			T907_SHIPPING_INFO.C907_SHIP_TO_ID%TYPE;
        v_SHIP_TO				T907_SHIPPING_INFO.C901_SHIP_TO%TYPE;
        v_SHIP_CAR				T907_SHIPPING_INFO.C901_DELIVERY_CARRIER%TYPE;
        v_SHIP_MODE				T907_SHIPPING_INFO.C901_DELIVERY_MODE%TYPE;
        v_ADDR_ID				T907_SHIPPING_INFO.C106_ADDRESS_ID%TYPE;
        v_FRGT_AMT				T907_SHIPPING_INFO.C907_FRIEGHT_AMT%TYPE;
        v_REQ_BY_ID				T525_PRODUCT_REQUEST.C525_REQUEST_BY_ID%TYPE;
        v_REP_ID				T525_PRODUCT_REQUEST.C703_SALES_REP_ID%TYPE;
        v_REQUIRE_DATE			T526_PRODUCT_REQUEST_DETAIL.C526_REQUIRED_DATE%TYPE;
        v_USER_ID				T525_PRODUCT_REQUEST.C525_CREATED_BY%TYPE;
        v_ACCT_ID				T525_PRODUCT_REQUEST.C704_ACCOUNT_ID%TYPE;
        v_PLAN_SHIP_DT			T907_SHIPPING_INFO.C907_SHIP_TO_DT%TYPE;
CURSOR v_cur_ship
		 IS
			SELECT t520.c520_request_id reqid ,
			  t525.c525_product_request_id prod_req_id ,
			  t526.c526_product_request_detail_id req_dt_id,
			  shipping.c901_delivery_carrier shipcar,
			  shipping.c901_delivery_mode shipmode,
			  t525.c525_created_by userid
			FROM t520_request t520 ,
			  t504_consignment t504 ,
			  t505_item_consignment t505 ,
			  t525_product_request t525 ,
			  t526_product_request_detail t526 ,
			  (SELECT t907.c907_ref_id,
			    t907.c901_delivery_carrier,
			    t907.c901_delivery_mode
			  FROM t907_shipping_info t907
			  WHERE t907.c907_ref_id IN
			    (SELECT token FROM v_in_list WHERE token IS NOT NULL
			    )
			  AND t907.c907_void_fl IS NULL
			  ) shipping
			WHERE t520.c520_request_id               = t504.c520_request_id
			AND t504.c504_consignment_id             = t505.c504_consignment_id
			AND t520.c520_ref_id                     = t525.c525_product_request_id
			AND t525.c525_product_request_id         = t526.c525_product_request_id
			AND T526.C526_PRODUCT_REQUEST_DETAIL_ID IN
			  (SELECT token FROM v_in_list WHERE token IS NOT NULL
			  )
			AND t505.c205_part_number_id            = t526.c205_part_number_id
			AND t526.c526_product_request_detail_id = shipping.c907_ref_id
			AND t520.c520_void_fl                  IS NULL
			AND t504.c504_void_fl                  IS NULL
			AND t505.c505_void_fl                  IS NULL
			AND t526.c526_void_fl                  IS NULL
			AND t525.c525_void_fl                  IS NULL;
BEGIN
v_apprv_request_json := json_array_t (p_str_apprv_qty_string);

v_date_format := GET_RULE_VALUE('DATEFMT', 'DATEFORMAT');

FOR i IN 0..v_apprv_request_json.get_size - 1 
        LOOP
        v_apprv_request_item 		:=      TREAT(v_apprv_request_json.get(i) AS json_object_t);
        v_request_id	 		   	:=		v_apprv_request_item.get_string('strReqId');
        v_part_number_id	 		:=		v_apprv_request_item.get_string('strPartNo');
        v_apprv_qty 		    	:=		v_apprv_request_item.get_string('strApprvQty');
        

		SELECT T525.C525_PRODUCT_REQUEST_ID ,
		T525.C525_REQUEST_FOR_ID 			,
		T525.C901_REQUEST_FOR 				,
		RTRIM (XMLAGG (xmlelement (E, T526.C205_PART_NUMBER_ID
		|| '^'
		||v_apprv_qty
		||'^'
		||TO_CHAR(T526.C526_Required_date,v_date_format)
		||'^|')) .extract ('//text()'), ',') ,
		T907.C907_SHIP_TO_ID 			,
		T907.C901_SHIP_TO 				,
		T907.C901_DELIVERY_CARRIER 		,
		T907.C901_DELIVERY_MODE 		,
		T907.C106_ADDRESS_ID 			,
		T907.C907_FRIEGHT_AMT 			,
		T525.C525_REQUEST_BY_ID 		,
		T525.C703_SALES_REP_ID 			,
		T526.C526_REQUIRED_DATE 		,
		T525.C525_CREATED_BY 			,
		T525.C704_ACCOUNT_ID 			,
		T907.C907_SHIP_TO_DT 			
		INTO
		v_PAR_REQ_ID,v_REQ_FOR,v_REQUEST_FOR,
		v_RQ_STR,v_SHIP_TO_ID,v_SHIP_TO,v_SHIP_CAR,
		v_SHIP_MODE,v_ADDR_ID,v_FRGT_AMT,v_REQ_BY_ID,
		v_REP_ID,v_REQUIRE_DATE,v_USER_ID,v_ACCT_ID,v_PLAN_SHIP_DT
		FROM T525_PRODUCT_REQUEST T525,
		T526_PRODUCT_REQUEST_DETAIL T526,
		T907_SHIPPING_INFO T907, t7100_case_information t7100,t7104_case_set_information T7104
		WHERE T525.C525_VOID_FL IS NULL
		AND T526.C526_VOID_FL IS NULL
		AND T907.C907_VOID_FL IS NULL
		AND T526.C205_PART_NUMBER_ID IS NOT NULL
		AND T525.C525_PRODUCT_REQUEST_ID = T526.C525_PRODUCT_REQUEST_ID
		AND T525.C525_PRODUCT_REQUEST_ID = T907.C525_PRODUCT_REQUEST_ID
		AND t7100.c7100_case_info_id = T7104.C7100_CASE_INFO_ID
		AND t7104.c526_product_request_detail_id = T526.C526_PRODUCT_REQUEST_DETAIL_ID
		AND t907.c907_ref_id = T526.C526_PRODUCT_REQUEST_DETAIL_ID
		AND T907.C907_SHIP_TO_ID IS NOT NULL
		AND T907.C901_SHIP_TO IS NOT NULL
		-- below condition is commented, since we dnt have any sales rep id for consignment items.
		--AND T525.C703_SALES_REP_ID IS NOT NULL
		AND T526.C526_STATUS_FL =10 --open
		AND T526.C526_PRODUCT_REQUEST_DETAIL_ID = v_request_id
		AND T526.C205_PART_NUMBER_ID = v_part_number_id
		GROUP BY T525.C525_PRODUCT_REQUEST_ID,
		T525.C525_REQUEST_FOR_ID,
		T525.C703_SALES_REP_ID,
		T907.C907_SHIP_TO_ID,
		T907.C901_SHIP_TO,
		T907.C901_DELIVERY_CARRIER,
		T907.C901_DELIVERY_MODE,
		T907.C106_ADDRESS_ID,
		T907.C907_FRIEGHT_AMT,
		T525.C525_REQUEST_BY_ID ,
		T525.C525_CREATED_BY,
		T525.C704_ACCOUNT_ID,
		T907.C907_SHIP_TO_DT,
		T526.C526_Required_date,
		T525.C901_REQUEST_FOR
		ORDER BY T525.C525_PRODUCT_REQUEST_ID;
		
		
		-- putting req ids in vinlist. 
		--my_context.set_my_inlist_ctx (p_input_str) ;  
		--v_date_format := GET_RULE_VALUE('DATEFMT', 'DATEFORMAT');
		
	      --FOR v_cur IN v_cur_req
          --LOOP
           --if the request for type is 40050, then it will call the account item requests flow, otherwise call the existing prod_request flow
            IF v_REQUEST_FOR = '40050'
            THEN
            			 
			gm_pkg_op_account_request.gm_sav_initiate_request(v_request_id,v_REQUIRE_DATE,'50618',NULL,NULL,'40025',
				 v_REQ_FOR,'50625',v_REQ_BY_ID,v_SHIP_TO,v_SHIP_TO_ID,NULL,NULL,v_USER_ID,v_RQ_STR,'50060',
					 v_REP_ID,v_ACCT_ID,NULL,v_PLAN_SHIP_DT,NULL,v_OUT_REQ_ID,v_OUT_CONSIGN_ID,NULL);	
			ELSE	
					 
            -- here we are calling this procedure to get the RQ and CN by passing the info whatever the above cursor is returned.
			 gm_pkg_op_process_prod_request.gm_sav_initiate_request(v_request_id,v_REQUIRE_DATE,'50618',NULL,NULL,'40021',
					 v_REQ_FOR,'50626',v_REQ_BY_ID,v_SHIP_TO,v_SHIP_TO_ID,NULL,NULL,v_USER_ID,v_RQ_STR,'50060',
					 v_REP_ID,v_ACCT_ID,NULL,v_PLAN_SHIP_DT,NULL,v_OUT_REQ_ID,v_OUT_CONSIGN_ID,NULL);
			END IF;		 
	 
					
			UPDATE T520_request
			SET c520_ref_id       = v_PAR_REQ_ID
			WHERE C520_request_id = v_OUT_REQ_ID
			AND c520_void_fl IS NULL;
			
			IF v_OUT_REQ_ID IS NOT NULL
		    THEN
		    
		      gm_pkg_cm_shipping_trans.gm_sav_shipping(v_OUT_REQ_ID,'50184',v_SHIP_TO,v_SHIP_TO_ID,
		        	v_SHIP_CAR,v_SHIP_MODE,NULL,v_ADDR_ID,v_FRGT_AMT,v_USER_ID,V_OUT_SHIP_ID,NULL,NULL,NULL,NULL);
		    END IF;
			
			IF v_OUT_CONSIGN_ID IS NOT NULL
		    THEN
		      BEGIN
			    SELECT c520_request_id
				INTO v_req_id
				FROM t520_request
				WHERE c520_master_request_id = v_OUT_REQ_ID;
				
		      EXCEPTION
		    	WHEN NO_DATA_FOUND THEN
		        	v_req_id := NULL;
	    	  END;
				IF v_req_id IS NOT NULL 
				THEN
					UPDATE T520_request
					SET c520_ref_id       = v_PAR_REQ_ID
					WHERE C520_request_id = v_req_id;
				END IF;
				
		    END IF;
		    v_OUTPUT_REQUEST_ID := v_OUTPUT_REQUEST_ID ||v_OUT_REQ_ID || ',';
		    v_OUTPUT_CONSIGN_ID := v_OUTPUT_CONSIGN_ID ||v_OUT_CONSIGN_ID || ',';
			END LOOP;
				 
			SELECT SUBSTR(v_OUTPUT_REQUEST_ID,1,length(v_OUTPUT_REQUEST_ID)-1) 
				INTO v_OUTPUT_REQUEST_ID
			FROM dual;
			
			SELECT SUBSTR(v_OUTPUT_CONSIGN_ID,1,length(v_OUTPUT_CONSIGN_ID)-1) 
				INTO v_OUTPUT_CONSIGN_ID
			FROM dual;
			
			BEGIN
				FOR v_sh_cursor IN v_cur_ship
				LOOP
					 UPDATE T504_CONSIGNMENT
						SET c504_delivery_carrier = v_sh_cursor.shipcar,
						  c504_delivery_mode      = v_sh_cursor.shipmode,
						  c504_last_updated_date  = CURRENT_DATE,
						  c504_last_updated_by    = v_sh_cursor.userid
						WHERE c520_request_id     = v_sh_cursor.reqid
						AND c504_void_fl         IS NULL;
				END LOOP;
            END;
			
			P_out_request_id := v_OUTPUT_REQUEST_ID;
			P_out_consgn_id := v_OUTPUT_CONSIGN_ID;
			
END gm_sav_apprv_request;

END gm_pkg_item_apprv_txn;
/