create or replace
PACKAGE BODY gm_pkg_case_scheduler_calendar
IS 
     
/* *******************************************************************************
 * Author      : N Raja
 * Procedure   : gm_pkg_case_scheduler_calendar.bdy
 * Description : Procedure to fetch Calendar data
 ***********************************************************************************/              
	
PROCEDURE gm_fetch_calendar_data  (
	    p_FromDate      IN  VARCHAR2,
	    p_ToDate        IN  VARCHAR2,	
	    p_dateFormat    IN  VARCHAR2,
	    p_cur_cal_data  OUT   TYPES.cursor_type
      ) 
      AS
       v_company_id t1900_company.c1900_company_id%TYPE;
      BEGIN
	      
 		 SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL'))
   		   INTO v_company_id 
		   FROM dual;	
	      
         OPEN p_cur_cal_data
           FOR
             SELECT T7100.C7100_CASE_INFO_ID INFOID , T7100.C7100_CASE_ID CASE_ID,T7100.C901_CASE_STATUS STATUS,
                    TO_CHAR(T7100.C7100_SHIP_DATE, P_DATEFORMAT || ' HH:MM') START_DATE,
                    TO_CHAR(T7100.C7100_SURGERY_DATE,P_DATEFORMAT || ' HH:MM') SURGERY_DATE,
                    TO_CHAR(T7100.C7100_RETURN_DATE,P_DATEFORMAT || ' HH:MM') END_DATE,        
                    GET_DISTRIBUTOR_NAME (T7100.C701_DISTRIBUTOR_ID) DISTNM,
                    GET_ACCOUNT_NAME (T7100.C704_ACCOUNT_ID) ACCOUNTNM ,
                    GET_REP_NAME (T7100.C703_SALES_REP_ID) REPNM
               FROM T7100_CASE_INFORMATION T7100
              WHERE T7100.C7100_SURGERY_DATE BETWEEN TO_DATE(P_FROMDATE,P_DATEFORMAT)  AND TO_DATE(P_TODATE,P_DATEFORMAT)
                 AND T7100.C901_TYPE = '107285'
                 AND T7100.C901_CASE_STATUS != '107163'
                 AND T7100.C1900_COMPANY_ID = V_COMPANY_ID
                AND C7100_VOID_FL IS NULL ORDER BY T7100.C7100_CASE_INFO_ID;                            

     		    
END gm_fetch_calendar_data;

 END gm_pkg_case_scheduler_calendar;
    /
   