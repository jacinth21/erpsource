/* Formatted on 2011/11/04 11:52 (Formatter Plus v4.8.0) */

--@"C:\Database\Packages\sales\case_management\gm_pkg_sm_casebook_txn.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_sm_casebook_txn
IS
--

	/*******************************************************
	* Description : Procedure to save Case general info
	* Author		: Xun
	*******************************************************/
	PROCEDURE gm_sav_case_info (
		p_caseinfoid	  IN	   t7100_case_information.c7100_case_info_id%TYPE
	  , p_surgery_date	  IN	   t7100_case_information.c7100_surgery_date%TYPE
	  , p_surgery_hour	  IN	   t7100_case_information.c7100_created_by%TYPE
	  , p_surgery_min	  IN	   t7100_case_information.c7100_created_by%TYPE
	  , p_surgery_ampm	  IN	   t7100_case_information.c7100_created_by%TYPE
	  , p_rep_id		  IN	   t7100_case_information.c703_sales_rep_id%TYPE
	  , p_account_id	  IN	   t7100_case_information.c704_account_id%TYPE
	  , p_dist_id		  IN	   t7100_case_information.c701_distributor_id%TYPE
	  , p_personal_note   IN	   t7100_case_information.c7100_personal_notes%TYPE
	  , p_userid		  IN	   t7100_case_information.c7100_created_by%TYPE
	  , p_outcaseinfoid   OUT	   t7100_case_information.c7100_case_info_id%TYPE
	  , p_outcaseid 	  OUT	   t7100_case_information.c7100_case_id%TYPE
	)
	AS
		v_surgery_time VARCHAR2 (100) := NULL;
		v_surgery_hour VARCHAR2 (100) := NULL;
		v_case_id	   VARCHAR2 (100) := NULL;
		v_seq		   NUMBER;
		v_dateformat varchar2(100);
		v_company_id  t1900_company.c1900_company_id%TYPE;
	BEGIN
		SELECT  get_compdtfmt_frm_cntx(),get_compid_frm_cntx()
	INTO	 v_dateformat,v_company_id
	FROM	dual;
	
	
		p_outcaseinfoid := p_caseinfoid;
		v_surgery_hour := p_surgery_hour;

		IF (p_surgery_ampm = 'PM' AND TO_NUMBER (p_surgery_hour)!=12)
		THEN
			v_surgery_hour := TO_CHAR (TO_NUMBER (p_surgery_hour) + 12); 
		END IF;
		
		IF (p_surgery_ampm = 'AM' AND TO_NUMBER (p_surgery_hour)=12)
		THEN
			v_surgery_hour := '00'; 
		END IF;

		v_surgery_time := to_char(p_surgery_date, v_dateformat) || ' ' || v_surgery_hour || ':' || p_surgery_min;

		 SELECT  NVL (MAX(TO_NUMBER (SUBSTR (C7100_CASE_ID,INSTR(C7100_CASE_ID, '-', -1)+1))), 0) +1  			
		  INTO v_seq
		  FROM t7100_case_information
		 WHERE c7100_case_id LIKE '_-' || p_rep_id || '-' || TO_CHAR (p_surgery_date, 'yyyymmdd') || '%' AND c7100_void_fl IS NULL; 
		 
		v_case_id	:=
			   'D-'
			|| p_rep_id
			|| '-'
			|| TO_CHAR (p_surgery_date, 'yyyymmdd')
			|| '-'
			|| v_seq;	
	
		UPDATE t7100_case_information
		   SET c7100_surgery_date =  p_surgery_date
		   	 , c7100_case_id = decode(c7100_surgery_date, p_surgery_date, c7100_case_id, v_case_id)
			 , c7100_surgery_time = TO_DATE (v_surgery_time, v_dateformat||' hh24:mi')
			 , c701_distributor_id = p_dist_id
			 , c703_sales_rep_id = p_rep_id
			 , c704_account_id = DECODE (p_account_id, 0, NULL, p_account_id)
			 , c7100_last_updated_by = p_userid
			 , c7100_last_updated_date = CURRENT_DATE
			 , c7100_personal_notes = p_personal_note
		 WHERE c7100_case_info_id = p_outcaseinfoid AND c7100_void_fl IS NULL;
		  
		IF (SQL%ROWCOUNT = 0)
		THEN
			SELECT s7100_case_information.NEXTVAL
			  INTO p_outcaseinfoid
			  FROM DUAL; 
			

			INSERT INTO t7100_case_information
						(c7100_case_info_id, c7100_case_id, c7100_surgery_date
					   , c7100_surgery_time, c701_distributor_id, c703_sales_rep_id
					   , c704_account_id, c7100_created_by, c7100_created_date, c7100_personal_notes, c901_case_status, C901_Type,c1900_company_id
						)
				 VALUES (p_outcaseinfoid, v_case_id,  p_surgery_date
					   , TO_DATE (v_surgery_time, v_dateformat||' hh24:mi'), p_dist_id, p_rep_id
					   , DECODE (p_account_id, 0, NULL, p_account_id), p_userid, CURRENT_DATE, p_personal_note, 11090, 1006503,v_company_id
						);  
		END IF;
	 
		SELECT c7100_case_id
		  INTO p_outcaseid
		  FROM t7100_case_information
		 WHERE c7100_case_info_id = p_outcaseinfoid AND c7100_void_fl IS NULL; 
	END gm_sav_case_info;

	/*******************************************************
		* Description : Procedure to save Case additional info and attribute
		* Author		 : Xun
		*******************************************************/
	PROCEDURE gm_sav_case_builder_info (
		p_caseinfoid	  IN   t7101_case_additional_info.c7100_case_info_id%TYPE
	  , p_selectedcatg	  IN   VARCHAR2
	  , p_userid		  IN   t7100_case_information.c7100_created_by%TYPE
	)
	AS
		v_case_status  t7100_case_information.c901_case_status%TYPE;
		v_code_group  t901_code_lookup.c901_code_grp%TYPE;
		v_type  t7100_case_information.c901_type%TYPE;
	BEGIN
		DELETE FROM t7102_case_attribute
			  WHERE c7100_case_info_id = p_caseinfoid;
			  
		my_context.set_my_inlist_ctx (p_selectedcatg);

		INSERT INTO t7102_case_attribute
					(c7102_case_attribute_id, c7100_case_info_id, c7102_attribute_value, c901_attribute_type)
			SELECT s7102_case_attribute.NEXTVAL, p_caseinfoid, 'Y', v_in_list.token
			  FROM v_in_list
			 WHERE token IS NOT NULL;

		v_case_status := get_case_status(p_caseinfoid);
		v_type := get_type(p_caseinfoid);
		
		IF  v_type = '1006504' THEN --EVENT
			v_code_group := 'EVEMET';
		ELSE
			v_code_group := 'CASCAT';
		END IF;
		

		FOR cur IN (
		             SELECT c901_code_id delcatg 
		               FROM t901_code_lookup 
		              WHERE c901_code_grp=v_code_group
		                AND C901_ACTIVE_FL = 1 
		                AND c901_code_id NOT IN (SELECT token FROM v_in_list WHERE token IS NOT NULL)
		            )
		LOOP
			IF (v_case_status != 11091 AND v_type = '1006503') OR (v_case_status != 19524 AND v_case_status != 19534 AND v_type = '1006504')  
			THEN
				gm_case_del_allsets(p_caseinfoid,cur.delcatg); 
			ELSE
				gm_case_del_sets(p_caseinfoid, NULL, NULL, cur.delcatg, p_userid);
			END IF;
		END LOOP;
	END gm_sav_case_builder_info;

	 /******************************************************************
	* Description : Procedure to delete all Case Sets for specific type
	* Author		: Xun
	*******************************************************************/
	PROCEDURE gm_case_del_allsets (
		p_caseinfoid	IN	 t7100_case_information.c7100_case_info_id%TYPE 
	  , p_systype		IN	 t7104_case_set_information.c901_system_type%TYPE 
	)
	AS 
	BEGIN
		DELETE FROM t7104_case_set_information
				  WHERE c7100_case_info_id = p_caseinfoid AND c901_system_type = p_systype;
	END gm_case_del_allsets;	
	
	/******************************************************************
	* Description : Procedure to delete all favorite Case Sets for specific type
	* Author		: Xun
	*******************************************************************/
	PROCEDURE gm_case_del_fav_allsets (
		p_favcaseid		IN	 t7124_favourite_case_set_info.c7120_favourite_case_id%TYPE 
	  , p_systype		IN	 t7104_case_set_information.c901_system_type%TYPE 
	)
	AS 
	BEGIN
			DELETE FROM t7124_favourite_case_set_info
				  WHERE c7120_favourite_case_id = p_favcaseid AND c901_system_type = p_systype;
	END gm_case_del_fav_allsets;	

	 /*******************************************************
	* Description : Procedure to save Case Stes info
	* Author		: Xun
	*******************************************************/
	PROCEDURE gm_sav_case_sets_info (
		p_caseinfo_id	IN	 t7100_case_information.c7100_case_info_id%TYPE
	  , p_inputstr		IN	 VARCHAR2
	  , p_systype		IN	 t7104_case_set_information.c901_system_type%TYPE
	  , p_userid		IN	 t7100_case_information.c7100_created_by%TYPE
	  , p_det_inputstr  IN 	 CLOB DEFAULT NULL
	  , p_ship_inputstr IN 	 CLOB DEFAULT NULL
	)
	AS
		v_surgerydate  DATE;
		v_case_status  t7100_case_information.c901_case_status%TYPE;
		v_new_count	   NUMBER;
		v_old_count	   NUMBER;
		v_count	   NUMBER;
		v_type  t7100_case_information.c901_type%TYPE;
		v_startdate  DATE;
		v_enddate  DATE;
		v_case_set_id t7104_case_set_information.c7104_case_set_id%TYPE;
		cur TYPES.cursor_type;
		CURSOR c_del_all_sets 
		IS
			SELECT c207_set_id setid FROM t7104_case_set_information t7104 
			WHERE t7104.c7100_case_info_id = p_caseinfo_id 
			  AND t7104.c7104_shipped_del_fl IS NULL 
			  AND t7104.c7104_void_fl IS NULL 
			  AND c207_set_id NOT IN (SELECT token  FROM v_in_list WHERE token IS NOT NULL);
			  
		CURSOR c_del_sets (v_setid VARCHAR2)
		IS
			 SELECT DECODE (t7104.c901_set_location_type, 11375, 3, 11378,2, 11381, 1, 0) seq
			      , t7104.c7104_case_set_id case_setid
	           FROM t7104_case_set_information t7104
	          WHERE t7104.c7100_case_info_id = p_caseinfo_id
	            AND t7104.c207_set_id        = v_setid
	            AND t7104.c7104_shipped_del_fl IS NULL
		        AND t7104.c7104_void_fl IS NULL
	       ORDER BY seq;
	BEGIN
		my_context.set_my_inlist_ctx (p_inputstr);
		
		v_case_status := get_case_status(p_caseinfo_id);
		v_type := get_type(p_caseinfo_id);
	
		SELECT c7100_surgery_date
		  INTO v_surgerydate
		  FROM t7100_case_information
		 WHERE c7100_case_info_id = p_caseinfo_id AND c7100_void_fl IS NULL;
		 
		 SELECT t7100.C901_Type,t7100.c7100_surgery_date,T7103.C7103_Start_Date,T7103.C7103_End_Date 
			INTO v_type,v_surgerydate,v_startdate,v_enddate
		    FROM T7100_Case_Information T7100,T7103_Schedule_Info T7103
		    WHERE t7100.c7100_case_info_id = p_caseinfo_id AND t7100.c7100_void_fl IS NULL AND T7103.C7100_Case_Info_Id(+) = T7100.C7100_Case_Info_Id;

		IF (v_case_status != 11091 AND v_type = '1006503') OR (v_case_status != 19524 AND v_case_status != 19534 AND v_type = '1006504')  
	 	THEN
			UPDATE t7104_case_set_information
			   SET c7104_void_fl = 'Y'
			     , c7104_last_updated_by = p_userid
			     , c7104_last_updated_date = CURRENT_DATE
			 WHERE c7100_case_info_id = p_caseinfo_id 
			   AND c901_system_type = NVL (p_systype, -9999)
			   AND c7104_void_fl IS NULL;
	
			INSERT INTO t7104_case_set_information
						(c7104_case_set_id, c7100_case_info_id, c207_set_id, c901_system_type, c7104_tag_lock_from_date
					   , c7104_tag_lock_to_date, c7104_created_date, c7104_created_by)
				SELECT s7104_case_set_information.NEXTVAL, p_caseinfo_id, v_in_list.token, p_systype
					  , DECODE(v_type,'1006504',get_lock_date (v_surgerydate, 'LOCK_FROM_DATE'),v_startdate) 
				      , DECODE(v_type,'1006504',get_lock_date (v_surgerydate, 'LOCK_TO_DATE'),v_enddate)
					 , CURRENT_DATE, p_userid
				  FROM v_in_list
				 WHERE token IS NOT NULL;
		ELSE
			--not required If a type is Loaner/ consign Set request 
			IF p_systype <> 103642 AND p_systype <> 103643
			THEN
				FOR cur IN c_del_all_sets
				LOOP
					gm_case_del_sets(p_caseinfo_id,cur.setid,NULL,p_systype, p_userid);
				END LOOP;
			END IF;
			
			FOR cur IN (SELECT DISTINCT token v_setid FROM v_in_list WHERE token IS NOT NULL)
			LOOP
				SELECT count(1) 
				  INTO v_new_count
				  FROM v_in_list
				 WHERE token = cur.v_setid;
				--not required If a type is Loaner/ consign Set request 
	            IF p_systype <> 103642 AND p_systype <> 103643
				THEN
		             SELECT count(1)
		               INTO v_old_count
					   FROM t7104_case_set_information t7104
					  WHERE t7104.c7100_case_info_id = p_caseinfo_id
		                AND t7104.c207_set_id = cur.v_setid
					    AND t7104.c7104_shipped_del_fl IS NULL
					    AND t7104.c7104_void_fl IS NULL;
			    ELSE
			    	v_old_count := 0;
				END IF;			
				   
				 IF v_new_count > v_old_count 
				 THEN
				 	v_count := (v_new_count - v_old_count);
				 	WHILE(v_count > 0)
				 	LOOP
				 		gm_save_case_sets(p_caseinfo_id,cur.v_setid,null,null,p_systype,v_surgerydate,p_userid , v_case_set_id);
				 		v_count := v_count - 1;
				 	END LOOP;
				 ELSIF v_new_count < v_old_count
				 THEN
				 	v_count := (v_old_count - v_new_count);
				 	FOR v_rec IN c_del_sets(cur.v_setid)
					LOOP
						EXIT WHEN  v_count = 0 ;
				 		gm_case_del_sets(p_caseinfo_id,cur.v_setid,v_rec.case_setid,p_systype, p_userid);
				 		v_count:= v_count - 1;
				 	END LOOP;
				 END IF;
				
			END LOOP;
			--
		END IF;
		--save the shiping values into temp table
		IF p_systype = 103642 OR p_systype = 103643
		THEN
			gm_sav_temp_case_dtls(  v_case_set_id , p_det_inputstr, p_ship_inputstr);
		END IF;
		
		IF v_type = '1006504'  OR v_type = '1006505' OR v_type = '1006506' OR v_type = '1006507' OR v_type = '107286' 
		THEN
		UPDATE T7100_CASE_INFORMATION SET C901_CASE_STATUS='19524' ,C7100_LAST_UPDATED_BY=p_userid,C7100_LAST_UPDATED_DATE=CURRENT_DATE 
		WHERE c7100_case_info_id = p_caseinfo_id AND C7100_VOID_FL IS NULL;		
		END IF;
	END gm_sav_case_sets_info;

	  /***********************************************************
	* Description : Procedure to fetch all mapped System and Sets
	* Author	  : Xun
	*************************************************************/
	PROCEDURE gm_fch_mapped_sets (
		p_systype	   IN		t7104_case_set_information.c901_system_type%TYPE
	  , p_caseinfoid   IN		t7100_case_information.c7100_case_info_id%TYPE
	  , p_systemsets   OUT		TYPES.cursor_type
	)
	AS
	v_regular_exp  t906_rules.c906_rule_value%TYPE;
	v_company_id  t1900_company.c1900_company_id%TYPE;
		CURSOR sys_list
		IS
			SELECT t207c.c207_set_id sysid
			  FROM t207c_set_attribute t207c
				 , (SELECT t207c.c207_set_id,t207c.c901_attribute_type
					  FROM t207c_set_attribute t207c, t7102_case_attribute t7102, t901_code_lookup t901
					 WHERE t207c.c901_attribute_type = t7102.c901_attribute_type
					   AND t7102.c7100_case_info_id = p_caseinfoid
					   AND t7102.c7102_attribute_value = 'Y'
					   AND t7102.c7102_void_fl IS NULL
					   AND t7102.c901_attribute_type = t901.c901_code_id
					   AND t901.c901_code_grp = 'CASCAT') repapp   --(11180,11181,11182,11183,11184,11185,11186) categories
			 WHERE t207c.c901_attribute_type = p_systype
			   AND t207c.c901_attribute_type = repapp.c901_attribute_type
			   AND t207c.c207_set_id = repapp.c207_set_id
			   AND t207c.c207c_attribute_value = 'Y'
			   AND t207c.C207C_VOID_FL IS NULL;
	BEGIN
		select get_compid_frm_cntx() into v_company_id from dual;
		
		IF p_systype = '19531' THEN  --inhouseloaner
		
		OPEN p_systemsets
		 FOR
		 
			 SELECT b.c202_project_id systemid, get_project_name (b.c202_project_id) systemnm, b.c207_set_id setid
			  , get_set_name (b.c207_set_id) setnm, get_code_name (b.c207_category) family, '' stype
			  , '' hier
			   FROM t207_set_master b,T2080_SET_COMPANY_MAPPING t2080, (
			        SELECT DISTINCT t504.c207_set_id my_temp_txn_id
			           FROM t504_consignment t504, t504a_consignment_loaner t504a
			          WHERE t504.c504_consignment_id = t504a.c504_consignment_id
			            AND t504.c504_type           = 4119 -- In House Sets
			            AND t504.c504_void_fl       IS NULL
			            AND t504.c207_set_id        IS NOT NULL
			            AND t504a.c504a_status_fl   != 60 AND t504.c1900_company_id = v_company_id  --PC-1775 adding comp id because its driving all the set not based on comp id and also 9120.9903DE must display in demo set alone, not in inhouse sets
			    )
			    my_temp_list
			  WHERE b.c207_set_id       = my_temp_list.my_temp_txn_id
			    AND b.c207_obsolete_fl IS NULL
			    AND b.c207_void_fl     IS NULL
			    AND T2080.C207_SET_ID = b.C207_SET_ID
            	AND t2080.c2080_void_fl is null
            	AND T2080.C1900_COMPANY_ID =v_company_id
                AND NVL(b.c901_status_id,-999) NOT IN ('20369') -- Obsolete  
			ORDER BY Systemnm, Systemid, Setid;
			
		ELSIF p_systype = '19532' THEN --DEMO
		OPEN p_systemsets
		 FOR
			SELECT b.c202_project_id systemid, get_project_name (b.c202_project_id) systemnm, b.c207_set_id setid
		  , get_set_name (b.c207_set_id) setnm, get_code_name (b.c207_category) family, '' stype
		  , '' hier
		   FROM t207_set_master b,T2080_SET_COMPANY_MAPPING t2080, (
		        SELECT DISTINCT t504.c207_set_id my_temp_txn_id
		           FROM t504_consignment t504, t504a_consignment_loaner t504a
		          WHERE t504.c504_consignment_id = t504a.c504_consignment_id
		            -- AND t504.c504_type = 4119 -- In House Sets
		            AND t504.c504_void_fl     IS NULL
		            AND t504.c207_set_id      IS NOT NULL
		            AND t504a.c504a_status_fl != 60 AND t504.c1900_company_id = v_company_id --PC-1775
		    )
		    my_temp_list
		  	WHERE b.c207_set_id       = my_temp_list.my_temp_txn_id
		    AND b.c207_obsolete_fl IS NULL
		    AND b.c207_void_fl     IS NULL
		    AND b.c207_type         = 4078 -- Demo
		    AND T2080.C207_SET_ID = b.C207_SET_ID
            AND t2080.c2080_void_fl is null
            AND T2080.C1900_COMPANY_ID =v_company_id
            AND NVL(b.c901_status_id,-999) NOT IN ('20369') -- Obsolete  
			ORDER BY Systemnm, Systemid, Setid;
				 
		ELSE
		
		DELETE FROM my_temp_list;

		FOR sys_cur IN sys_list
		LOOP
			INSERT INTO my_temp_list
				 VALUES (sys_cur.sysid);
		END LOOP;
              v_regular_exp := get_rule_value('EURREGULAREXP','LN_LS_EUR_REG_EXP');
		OPEN p_systemsets
		 FOR
			  SELECT   a.c207_set_id systemid, get_set_name (a.c207_set_id) systemnm, a.c207_actual_set_id setid
					, get_set_name (a.c207_actual_set_id) setnm, get_code_name (b.c207_category) family
					, DECODE (a.c901_hierarchy, 20700, 'Standard', 20702, 'Additional') stype, a.c901_hierarchy hier
				 FROM v207a_set_consign_link a, t207_set_master b, my_temp_list ,T2080_SET_COMPANY_MAPPING t2080
				 WHERE a.c207_actual_set_id = b.c207_set_id
				  AND a.c207_set_id = my_temp_list.my_temp_txn_id
				  AND (a.c901_hierarchy = 20700 OR a.c901_hierarchy = 20702)
				  AND b.c207_obsolete_fl IS NULL
				  AND b.c207_void_fl IS NULL
			  	  AND NOT REGEXP_LIKE(a.c207_actual_set_id,'^'||v_regular_exp||'$')
			  	  AND T2080.C207_SET_ID = b.C207_SET_ID
            	  AND t2080.c2080_void_fl is null
            	  AND T2080.C1900_COMPANY_ID =v_company_id
            	  AND NVL(b.c901_status_id,-999) NOT IN ('20369') -- Obsolete  
             ORDER BY SUBSTR (systemnm, 1, 3), systemid, hier, setid;
		
		  
		END IF;
	END gm_fch_mapped_sets;

	  /***********************************************************
	* Description : Procedure to update the case when post
	*************************************************************/
	PROCEDURE gm_sav_case_post_info (
		p_caseinfoid   IN	t7100_case_information.c7100_case_info_id%TYPE
	  , p_userid	   IN	t7100_case_information.c7100_created_by%TYPE
	)
	AS
		v_status   			NUMBER;
	BEGIN
	
		SELECT c901_case_status 
			INTO v_status 
		FROM t7100_case_information
		WHERE c7100_case_info_id = p_caseinfoid AND c7100_void_fl IS NULL;
		 
		UPDATE t7100_case_information
		   SET c7100_case_id = REPLACE (c7100_case_id, 'D', 'C')
			 , c901_case_status = 11091
			 , c7100_last_updated_by = p_userid
			 , c7100_last_updated_date = CURRENT_DATE
		 WHERE c7100_case_info_id = p_caseinfoid AND c7100_void_fl IS NULL;

		--Comment for MNTTASK-7882 Auto Allocation
		--gm_pkg_sm_tag_allocation.gm_init_tagallocation (p_caseinfoid, p_userid);
 
	END gm_sav_case_post_info;
	
		
	/***********************************************************
	* Description : Procedure to save case book reschedule
	* Author	  : Dhana Reddy
	*************************************************************/
	PROCEDURE gm_save_resch_case(
        p_caseinfoid	  IN	t7100_case_information.c7100_case_info_id%TYPE
	  , p_surgery_date	  IN	t7100_case_information.c7100_surgery_date%TYPE
	  , p_surgery_hour	  IN	t7100_case_information.c7100_created_by%TYPE
	  , p_surgery_min	  IN	t7100_case_information.c7100_created_by%TYPE
	  , p_surgery_ampm	  IN	t7100_case_information.c7100_created_by%TYPE
      , p_userid		  IN	t7100_case_information.c7100_created_by%TYPE
      , p_outcaseinfoid   OUT	t7100_case_information.c7100_case_info_id%TYPE
	)
	AS
        v_surgery_time VARCHAR2 (100) := NULL;
		v_surgery_hour VARCHAR2 (100) := NULL;
		v_case_id	   VARCHAR2 (100) := NULL;
        v_caseinfoid   		NUMBER;
        v_parentcaseinfoid  NUMBER;
        v_rep_id   			NUMBER;
        v_seq		   		NUMBER;
        v_date_fmt				VARCHAR2(20);
	BEGIN
		
		select get_compdtfmt_frm_cntx() into v_date_fmt from dual;
		
        v_surgery_hour := p_surgery_hour;
        IF (p_surgery_ampm = 'PM' AND TO_NUMBER (p_surgery_hour)!=12)
		THEN
			v_surgery_hour := TO_CHAR (TO_NUMBER (p_surgery_hour) + 12); 
		END IF;
		
		IF (p_surgery_ampm = 'AM' AND TO_NUMBER (p_surgery_hour)=12)
		THEN
			v_surgery_hour := '00'; 
		END IF;
       
		v_surgery_time := to_char(p_surgery_date, GET_RULE_VALUE('DATEFMT', 'DATEFORMAT')) || ' ' || v_surgery_hour || ':' || p_surgery_min; 
	
        
        SELECT s7100_case_information.NEXTVAL  INTO v_caseinfoid FROM dual ;
        
        SELECT c703_sales_rep_id 
		  INTO v_rep_id 
		  FROM t7100_case_information 
		 WHERE c7100_case_info_id = p_caseinfoid
		  AND  c7100_void_fl IS NULL
		  FOR UPDATE;

        v_parentcaseinfoid :=p_caseinfoid; 
		  
        SELECT  NVL (MAX(TO_NUMBER (SUBSTR (SUBSTR (c7100_case_id, 2, LENGTH (c7100_case_id) - 1), LENGTH (v_rep_id) + 12))), 0) +1 			
		  INTO v_seq
		  FROM t7100_case_information
		 WHERE c7100_case_id LIKE '%-' || v_rep_id || '-' || TO_CHAR (p_surgery_date, 'yyyymmdd') || '%'; 
		 
		v_case_id	:=
			   'C-'
			|| v_rep_id
			|| '-'
			|| TO_CHAR (p_surgery_date, 'yyyymmdd')
			|| '-'
			|| v_seq;	
                                        
       p_outcaseinfoid := v_caseinfoid;
        -- t7100_case_information
        
        INSERT INTO  t7100_case_information  (c7100_case_info_id, c7100_case_id, c7100_surgery_date
                  , c7100_surgery_time, c701_distributor_id, c703_sales_rep_id
                  , c704_account_id, c7100_created_by, c7100_created_date, c7100_personal_notes, c901_case_status,c7100_parent_case_info_id,c901_type,c1900_company_id)
                 SELECT  v_caseinfoid, v_case_id, p_surgery_date
                  ,TO_DATE (v_surgery_time, v_date_fmt||' hh24:mi'), c701_distributor_id, c703_sales_rep_id
                  , c704_account_id, p_userid, CURRENT_DATE, c7100_personal_notes, c901_case_status,v_parentcaseinfoid,c901_type,c1900_company_id
            	 FROM t7100_case_information where c7100_case_info_id=p_caseinfoid;
          
         update t7100_case_information set c901_case_status=11092 where c7100_case_info_id=p_caseinfoid AND c7100_void_fl IS NULL;
         
         -- t7102_case_attribute
         
         INSERT INTO t7102_case_attribute
					(c7102_case_attribute_id, c7100_case_info_id, c7102_attribute_value, c901_attribute_type)
			   SELECT  s7102_case_attribute.NEXTVAL,v_caseinfoid, c7102_attribute_value,c901_attribute_type
               FROM t7102_case_attribute WHERE c7100_case_info_id=p_caseinfoid AND c7102_void_fl IS NULL;
      
        --t7104_case_set_information
          
        INSERT INTO t7104_case_set_information
					(c7104_case_set_id, c7100_case_info_id, c207_set_id, c901_system_type, c7104_tag_lock_from_date
				   , c7104_tag_lock_to_date, c7104_created_date, c7104_created_by)
                   SELECT s7104_case_set_information.NEXTVAL, v_caseinfoid, c207_set_id, c901_system_type,  get_lock_date (p_surgery_date, 'LOCK_FROM_DATE')
				   , get_lock_date (p_surgery_date, 'LOCK_TO_DATE') , CURRENT_DATE, p_userid 
                   FROM t7104_case_set_information WHERE c7100_case_info_id=p_caseinfoid AND c7104_void_fl IS NULL;
                   
         gm_pkg_sm_setmgmt_txn.gm_sav_reschedule_tag(v_caseinfoid, p_userid);          

    END   gm_save_resch_case;
    
    /***********************************************************
	* Description : Procedure to save favourite Case
	* Author	  : Dhana Reddy
	*************************************************************/     
    PROCEDURE gm_sav_favourite_case(
    p_caseinfoid	  IN	t7100_case_information.c7100_case_info_id%TYPE
   ,p_caseid	      IN	t7100_case_information.c7100_case_id%TYPE
   ,p_favourite_nm    IN	t7120_favourite_case.c7120_favourite_case_nm%TYPE
   ,p_userid		  IN	t7100_case_information.c7100_created_by%TYPE
    ) 
    AS
    v_favourite_case_id NUMBER;
    v_fav_cnt number;
    v_favourite_nm VARCHAR2 (100) := NULL;
    BEGIN
		
		BEGIN
		IF(get_type(p_caseinfoid)= '1006504') THEN  --FOR event			
			SELECT COUNT (c7120_favourite_case_nm)
		      INTO v_fav_cnt
			  FROM t7120_favourite_case t7120, t7100_case_information t7100
		 	 WHERE t7120.c7120_favourite_case_nm = p_favourite_nm 
		 	   AND t7120.C101_requestor_id       = t7100.c7100_created_by
		 	   AND T7100.c7100_case_info_id      = p_caseinfoid
		 	   AND t7120.c7120_void_fl           is null
		 	   AND t7100.c7100_void_fl           is null;
		ELSE 
    	SELECT COUNT (c7120_favourite_case_nm)
    	  INTO v_fav_cnt
  		  FROM t7120_favourite_case t7120
 		 WHERE t7120.c7120_favourite_case_nm = p_favourite_nm 
 		   AND get_case_rep_id (p_caseinfoid,'') = t7120.c703_sales_rep_id
 		   AND t7120.c7120_void_fl is null;
	 	END IF;
    	 EXCEPTION
    	WHEN NO_DATA_FOUND THEN
        	v_favourite_nm := '';
    	END;
    	
    	 IF v_fav_cnt > 0
    	 THEN
    	  GM_RAISE_APPLICATION_ERROR('-20999','361','');
    	 	
    	 END IF;
    	 
    	 -- from t7100 tO T7120 table
    	 SELECT S7120_FAVOURITE_CASE.NEXTVAL  INTO v_favourite_case_id FROM dual ;
    	 
		    INSERT INTO T7120_FAVOURITE_CASE  (c7120_favourite_case_id ,c7120_favourite_case_nm ,c703_sales_rep_id ,C101_requestor_id,c7120_created_by ,c7120_created_date,C1900_COMPANY_ID)   
         		SELECT v_favourite_case_id, p_favourite_nm, c703_sales_rep_id , c7100_created_by, p_userid ,CURRENT_DATE,C1900_COMPANY_ID 
                  FROM t7100_case_information 
                 WHERE c7100_case_info_id=p_caseinfoid AND c7100_void_fl IS NULL;
          
		--  from T7102 to T7122 table
        
		   INSERT INTO T7122_FAVOURITE_CASE_ATTRIBUTE (c7122_favourite_case_attr_id ,c7120_favourite_case_id, c901_attribute_type,c7122_attribute_value,c7122_created_by, c7122_created_date)
         		SELECT S7122_FAVOURITE_CASE_ATTRIBUTE.NEXTVAL ,v_favourite_case_id,c901_attribute_type ,c7102_attribute_value,p_userid,CURRENT_DATE 
         		  FROM T7102_CASE_ATTRIBUTE 
         		 WHERE c7100_case_info_id=p_caseinfoid AND c7102_void_fl IS NULL;
         		  
		-- From T7104 to T7124 table
 		
			INSERT INTO T7124_FAVOURITE_CASE_SET_INFO (c7124_favourite_case_set_id,c7120_favourite_case_id,c207_set_id ,c205_part_number_id,c7124_item_qty,c7124_created_by ,c7124_created_date, c901_system_type)
 				SELECT S7124_FAVOURITE_CASE_SET_INFO.NEXTVAL,v_favourite_case_id,c207_set_id,c205_part_number_id,c7104_item_qty,p_userid,CURRENT_DATE , c901_system_type
 				  FROM T7104_CASE_SET_INFORMATION 
 				 WHERE c7100_case_info_id=p_caseinfoid AND c7104_void_fl IS NULL;
    END gm_sav_favourite_case; 
    
    /*******************************************************
	* Description : Procedure to Cancel Case
	* Author	  : Xun
	*******************************************************/
	PROCEDURE gm_sav_cancel_case (
		p_caseinfoid   IN	t7100_case_information.c7100_case_info_id%TYPE
	  , p_userid	   IN	t7100_case_information.c7100_created_by%TYPE
	)
	AS
	BEGIN
		gm_pkg_sm_setmgmt_txn.gm_sav_cancel_tag (p_caseinfoid, NULL, NULL, p_userid);
		
		UPDATE t7100_case_information
		   SET c901_case_status = 11093
			 , c7100_last_updated_by = p_userid
			 , c7100_last_updated_date = CURRENT_DATE
		 WHERE c7100_case_info_id = p_caseinfoid AND c7100_void_fl IS NULL;
	END gm_sav_cancel_case;  
	
	
	/*******************************************************
	* Description : Procedure to Void Case
	* Author	  : Xun
	*******************************************************/
	PROCEDURE gm_sav_void_case (
		p_caseinfoid   IN	t7100_case_information.c7100_case_info_id%TYPE
	  , p_userid	   IN	t7100_case_information.c7100_created_by%TYPE
	)
	AS
	BEGIN 
	
		UPDATE t7100_case_information
		   SET c7100_void_fl = 'Y' 
			 , c7100_last_updated_by = p_userid
			 , c7100_last_updated_date = CURRENT_DATE
		 WHERE c7100_case_info_id = p_caseinfoid AND c7100_void_fl IS NULL;
	END gm_sav_void_case;  
	
	 
	  /***********************************************************
	* Description : Procedure to fetch all mapped System and Sets
	* for favorite case
	* Author	  : Xun
	*************************************************************/
	PROCEDURE gm_fch_fav_mapped_sets (
		p_systype	   IN		t7104_case_set_information.c901_system_type%TYPE
	  , p_favcaseid    IN		T7120_FAVOURITE_CASE.c7120_favourite_case_id%TYPE
	  , p_systemsets   OUT		TYPES.cursor_type
	)
	AS
	v_regular_exp  t906_rules.c906_rule_value%TYPE;
	v_company_id  t1900_company.c1900_company_id%TYPE;
		CURSOR sys_list
		IS
			SELECT t207c.c207_set_id sysid
			  FROM t207c_set_attribute t207c
				 , T2080_SET_COMPANY_MAPPING t2080,(SELECT t207c.c207_set_id,t207c.c901_attribute_type
					  FROM t207c_set_attribute t207c, T7122_FAVOURITE_CASE_ATTRIBUTE t7122, t901_code_lookup t901
					 WHERE t207c.c901_attribute_type = t7122.c901_attribute_type
					   AND t7122.c7120_favourite_case_id = p_favcaseid
					   AND t7122.c7122_attribute_value = 'Y'
					   AND t7122.c7122_void_fl IS NULL
					   AND t7122.c901_attribute_type = t901.c901_code_id
					   AND t901.c901_code_grp = 'CASCAT'   
					) repapp   --(11180,11181,11182,11183,11184,11185,11186) categories
			 WHERE t207c.c901_attribute_type = p_systype
			  AND t207c.c901_attribute_type = repapp.c901_attribute_type
			   AND t207c.c207_set_id = repapp.c207_set_id
			   AND t207c.c207c_attribute_value = 'Y'
			   AND t207c.C207C_VOID_FL IS NULL
			   AND T2080.C207_SET_ID = t207c.C207_SET_ID
               AND t2080.c2080_void_fl is null
               AND T2080.C1900_COMPANY_ID =v_company_id;
	BEGIN
		
		select get_compid_frm_cntx() into v_company_id from dual;
		DELETE FROM my_temp_list;

		FOR sys_cur IN sys_list
		LOOP
			INSERT INTO my_temp_list
				 VALUES (sys_cur.sysid);
		END LOOP;
              v_regular_exp := get_rule_value('EURREGULAREXP','LN_LS_EUR_REG_EXP');
		OPEN p_systemsets
		 FOR
			  SELECT   a.c207_set_id systemid, get_set_name (a.c207_set_id) systemnm, a.c207_actual_set_id setid
					, get_set_name (a.c207_actual_set_id) setnm, get_code_name (b.c207_category) family
					, DECODE (a.c901_hierarchy, 20700, 'Standard', 20702, 'Additional') stype, a.c901_hierarchy hier
				 FROM v207a_set_consign_link a, t207_set_master b, my_temp_list
				WHERE a.c207_set_id = b.c207_set_id
				  AND a.c207_set_id = my_temp_list.my_temp_txn_id
				  AND (a.c901_hierarchy = 20700 OR a.c901_hierarchy = 20702)
				  AND b.c207_obsolete_fl IS NULL
				  AND b.c207_void_fl IS NULL
			  AND NOT REGEXP_LIKE(a.c207_actual_set_id,'^'||v_regular_exp||'$')
			 ORDER BY SUBSTR (systemnm, 1, 3), systemid, hier, setid;
	END gm_fch_fav_mapped_sets;      
	
	/*******************************************************
	* Description : Procedure to overwrite case set info with
	* favorite case production selection
	* Author		: Xun
	*******************************************************/

	PROCEDURE gm_sav_fav_over_casesets (
		p_favcaseid    IN	t7120_favourite_case.c7120_favourite_case_id%TYPE
	  , p_caseinfoid   IN	t7100_case_information.c7100_case_info_id%TYPE
	  , p_userid	   IN	t7100_case_information.c7100_created_by%TYPE
	)
	AS
		v_surgerydate	DATE;
		v_startdate  DATE;
		v_enddate  DATE;
		v_type t7100_case_information.C901_Type%TYPE;
	BEGIN		
		
		SELECT t7100.C901_Type,t7100.c7100_surgery_date,T7103.C7103_Start_Date,T7103.C7103_End_Date 
			INTO v_type,v_surgerydate,v_startdate,v_enddate
		    FROM T7100_Case_Information T7100,T7103_Schedule_Info T7103
		    WHERE t7100.c7100_case_info_id = p_caseinfoid AND t7100.c7100_void_fl IS NULL AND T7103.C7100_Case_Info_Id(+) = T7100.C7100_Case_Info_Id;
		
	
		DELETE FROM t7104_case_set_information
			  WHERE c7100_case_info_id = p_caseinfoid;
	
		INSERT INTO t7104_case_set_information
					(c7104_case_set_id, c7100_case_info_id, c207_set_id,C205_Part_Number_Id,C7104_Item_Qty, c901_system_type, c7104_tag_lock_from_date
				   , c7104_tag_lock_to_date, c7104_created_date, c7104_created_by)
			SELECT s7104_case_set_information.NEXTVAL, p_caseinfoid, c207_set_id,C205_Part_Number_Id,C7124_Item_Qty,c901_system_type
				 , DECODE(v_type,'1006504',v_startdate,get_lock_date (v_surgerydate, 'LOCK_FROM_DATE')) 
				 , DECODE(v_type,'1006504',v_enddate,get_lock_date (v_surgerydate, 'LOCK_TO_DATE')), CURRENT_DATE
				 , p_userid
			  FROM t7124_favourite_case_set_info
			 WHERE c7120_favourite_case_id = p_favcaseid AND c7124_void_fl IS NULL;
			 
			 
		IF v_type = '1006504' THEN --event only
			-- Updating the case Status to "Active" for Case Info ID.    	 
         UPDATE t7100_case_information 
            SET c901_case_status = 19524 -- active
               , c7100_last_updated_by = p_userid
		       , c7100_last_updated_date = CURRENT_DATE
          WHERE c7100_case_info_id = p_caseinfoid 
            AND c7100_void_fl IS NULL;
		
		END IF;
		gm_pkg_sm_event_filter.gm_filter_data( p_caseinfoid, p_userid);
	END gm_sav_fav_over_casesets;
	
	 /*******************************************************
		* Description : Procedure to save Case profile Builder
		* Author		 : Xun
		*******************************************************/
	
	PROCEDURE gm_sav_profile_builder (
		p_favcaseid 	  IN	   t7120_favourite_case.c7120_favourite_case_id%TYPE
	  , p_favourite_nm	  IN	   t7120_favourite_case.c7120_favourite_case_nm%TYPE
	  , p_selectedcatg	  IN	   VARCHAR2
	  , p_userid		  IN	   t7100_case_information.c7100_created_by%TYPE
	  , p_outfavcaseid	  OUT	   t7120_favourite_case.c7120_favourite_case_id%TYPE
	)
	AS
		v_favourite_case_id NUMBER;
		v_favourite_nm VARCHAR2 (100) := NULL;
		v_company_id  t1900_company.c1900_company_id%TYPE;
	BEGIN 
		select get_compid_frm_cntx() into v_company_id from dual;
		IF (p_favcaseid = 0)
		THEN
			BEGIN
				SELECT c7120_favourite_case_nm
				  INTO v_favourite_nm
				  FROM t7120_favourite_case
				 WHERE c7120_favourite_case_nm = p_favourite_nm;
			EXCEPTION
				WHEN NO_DATA_FOUND
				THEN
					v_favourite_nm := '';
			END;

			IF v_favourite_nm = p_favourite_nm
			THEN
			    GM_RAISE_APPLICATION_ERROR('-20999','361','');
				
			END IF;
			SELECT s7120_favourite_case.NEXTVAL
		  	  INTO v_favourite_case_id
		      FROM DUAL;
		END IF;
    	 
		IF (p_favcaseid = 0)
		THEN
			p_outfavcaseid := v_favourite_case_id;
		ELSE
			p_outfavcaseid := p_favcaseid;
		END IF;
	 
		UPDATE t7120_favourite_case
		   SET c7120_favourite_case_nm = p_favourite_nm
			-- , c703_sales_rep_id = p_userid
			 , c7120_last_updated_by = p_userid
			 , c7120_last_updated_date = CURRENT_DATE
		 WHERE c7120_favourite_case_id = p_favcaseid AND c7120_void_fl IS NULL;
 
		IF (SQL%ROWCOUNT = 0)
		THEN
			INSERT INTO t7120_favourite_case
						(c7120_favourite_case_id, c7120_favourite_case_nm, c703_sales_rep_id, c7120_created_by
					   , c7120_created_date,C1900_COMPANY_ID
						)
				 VALUES (v_favourite_case_id, p_favourite_nm, p_userid, p_userid
					   , CURRENT_DATE,v_company_id
						); 
		END IF;
	
		my_context.set_my_inlist_ctx (p_selectedcatg);
	
		DELETE FROM t7122_favourite_case_attribute WHERE c7120_favourite_case_id = p_favcaseid;
		
		INSERT INTO t7122_favourite_case_attribute
					(c7122_favourite_case_attr_id, c7120_favourite_case_id, c7122_attribute_value, c901_attribute_type)
			SELECT s7122_favourite_case_attribute.NEXTVAL, p_outfavcaseid, 'Y', v_in_list.token
			  FROM v_in_list
			 WHERE token IS NOT NULL;
	
		FOR cur IN (
		            SELECT c901_code_id delcatg 
		              FROM t901_code_lookup 
		             WHERE c901_code_grp='CASCAT' 
		               AND C901_ACTIVE_FL = 1 
		               AND c901_code_id NOT IN (SELECT token FROM v_in_list)
		            )
		LOOP
			gm_case_del_fav_allsets(p_favcaseid,cur.delcatg); 
		END LOOP;
	
	END gm_sav_profile_builder;
	
	  /*******************************************************
		* Description : Procedure to save Case Profile Sets info
		* Author		: Xun
		*******************************************************/
	
	PROCEDURE gm_sav_profile_sets (
		p_favcaseid   IN   t7120_favourite_case.c7120_favourite_case_id%TYPE
	  , p_inputstr	  IN   VARCHAR2
	  , p_systype	  IN   t7104_case_set_information.c901_system_type%TYPE
	  , p_userid	  IN   t7100_case_information.c7100_created_by%TYPE
	)
	AS
	BEGIN
		my_context.set_my_inlist_ctx (p_inputstr);
	
		DELETE FROM t7124_favourite_case_set_info
			  WHERE c7120_favourite_case_id = p_favcaseid AND c901_system_type = NVL (p_systype, -9999);
	
		INSERT INTO t7124_favourite_case_set_info
					(c7124_favourite_case_set_id, c7120_favourite_case_id, c207_set_id, c7124_created_date
				   , c7124_created_by, c901_system_type)
			SELECT s7124_favourite_case_set_info.NEXTVAL, p_favcaseid, v_in_list.token, CURRENT_DATE, p_userid, p_systype
			  FROM v_in_list
			 WHERE token IS NOT NULL;
	END gm_sav_profile_sets;
	
	
	
PROCEDURE gm_check_and_close_case (
      p_caseinfoid   IN   t7100_case_information.c7100_case_info_id%TYPE
   )
   AS
      v_cnt   NUMBER := 0;
      c_areaset TYPES.cursor_type;
      CURSOR c_ord
	      IS
	        SELECT c501_customer_po cust_po, c503_invoice_id invoice_id
        	  FROM t501_order
             WHERE c7100_case_info_id = p_caseinfoid
               AND c501_void_fl IS NULL;
      v_inv_status t503_invoice.c503_status_fl%TYPE;
      v_flag CHAR(1):='';
   BEGIN
	  /*
	   * 1) If case without any DO raised
	   * 2) Single case may have mulitple orders,
	   *   If any one of them is without customer PO 
	   * 3) Invoice of the order is incompleted. 
	   *   these scenarios the case status will not be update to �closed�.
	   */
	  FOR v_rec IN c_ord
	  LOOP
	  	  v_flag := 'Y';
	      
	  	  IF v_rec.cust_po IS NULL OR v_rec.invoice_id IS NULL
	  	  THEN
	  	  	 RETURN;
	  	  END IF;
	  	  
	      BEGIN
		      SELECT c503_status_fl INTO v_inv_status
		        FROM t503_invoice
	           WHERE c503_invoice_id = v_rec.invoice_id;
           	EXCEPTION	
	           WHEN NO_DATA_FOUND
				THEN
					RETURN;
	      END;
	      
	      IF v_inv_status <> '2'
	      THEN
	     	 RETURN;
	      END IF;
	      
	  END LOOP;
	  --If case without DO then
	  IF v_flag != 'Y' THEN
	  	RETURN;
	  END IF;
	  
	  /*--Comment for MNTTASK-7882 Auto Allocation
      -- APrasath --> Add Your conditions here
      --11378 - areaset , 40-shipped , 11460 - Received will return 1 otherwise 0
      for c_areaset in (SELECT decode(t907.c907_status_fl,40,decode(t907.c901_accept_status,'11460',1,0),0) STATUS FROM t907_shipping_info t907 ,  
      t7104_case_set_information t7104 where t7104.c7100_case_info_id = p_caseinfoid and t7104.c901_set_location_type=11378 and
      t7104.c7104_void_fl is null  and  c7104_shipped_del_fl IS NULL and t907.c901_source=11388 and t907.c907_void_fl is null and  to_char(t7104.c7104_case_set_id)=t907.C907_REF_ID) loop
      
      IF c_areaset.STATUS = 0
      THEN
         RETURN;
      END IF;
       
      end loop;
       */     
      
      UPDATE t7100_case_information
         SET c901_case_status = 11094   -- close
       WHERE c7100_case_info_id = p_caseinfoid;
   END gm_check_and_close_case;
	/***********************************************************
	* Description : Procedure is used to save case sets
	* Author	  : Gopinathan
	*************************************************************/
	PROCEDURE gm_save_case_sets(
		p_caseinfo_id	IN	 t7100_case_information.c7100_case_info_id%TYPE
	  , p_setid			IN	 t7104_case_set_information.c207_set_id%TYPE
	  , p_pnum			IN	 t7104_case_set_information.c205_part_number_id%TYPE
	  , p_item_qty		IN	 t7104_case_set_information.c7104_item_qty%TYPE
	  , p_systype		IN	 t7104_case_set_information.c901_system_type%TYPE
	  , p_surgerydate	IN	 t7100_case_information.c7100_surgery_date%TYPE
	  , p_userid		IN	 t7100_case_information.c7100_created_by%TYPE
	)
	AS
	v_case_set_id    t7104_case_set_information.c7104_case_set_id%TYPE;
	BEGIN
	    --Overloading this procedure to return the  c7104_case_set_id.
		gm_save_case_sets(p_caseinfo_id, p_setid, p_pnum, p_item_qty, p_systype, p_surgerydate, p_userid, v_case_set_id);
	END gm_save_case_sets;
	/***********************************************************
	* Description : Procedure is used to save case sets
	* Author	  : Elango
	*************************************************************/
	PROCEDURE gm_save_case_sets(
		p_caseinfo_id	IN	 t7100_case_information.c7100_case_info_id%TYPE
	  , p_setid			IN	 t7104_case_set_information.c207_set_id%TYPE
	  , p_pnum			IN	 t7104_case_set_information.c205_part_number_id%TYPE
	  , p_item_qty		IN	 t7104_case_set_information.c7104_item_qty%TYPE
	  , p_systype		IN	 t7104_case_set_information.c901_system_type%TYPE
	  , p_surgerydate	IN	 t7100_case_information.c7100_surgery_date%TYPE
	  , p_userid		IN	 t7100_case_information.c7100_created_by%TYPE
	  , p_out_case_set_id OUT t7104_case_set_information.c7104_case_set_id%TYPE
	)
	AS
	v_case_set_id    t7104_case_set_information.c7104_case_set_id%TYPE;
	BEGIN
		SELECT s7104_case_set_information.NEXTVAL
			  INTO v_case_set_id
			  FROM DUAL; 
			  
		INSERT INTO t7104_case_set_information
						(c7104_case_set_id, c7100_case_info_id, c207_set_id,c205_part_number_id,c7104_item_qty, c901_system_type, c7104_tag_lock_from_date
					   , c7104_tag_lock_to_date, c7104_created_date, c7104_created_by)
			 VALUES     (v_case_set_id ,p_caseinfo_id,p_setid,p_pnum,p_item_qty,p_systype
			 		    ,get_lock_date (p_surgerydate, 'LOCK_FROM_DATE'), get_lock_date (p_surgerydate, 'LOCK_TO_DATE')
			 		    ,CURRENT_DATE,p_userid); 
			 		    
		 p_out_case_set_id :=   v_case_set_id;
			 		    
	END gm_save_case_sets;
 	  /*******************************************************
		* Description : Procedure is used to add/delete set in posted case
		* Author	  : Gopinathan
		*******************************************************/
   PROCEDURE gm_case_del_sets(
   	  p_caseinfoid   IN   t7100_case_information.c7100_case_info_id%TYPE
   	, p_setid 	  	 IN   t7104_case_set_information.c207_set_id%TYPE
   	, p_case_setid	 IN	  t7104_case_set_information.c7104_case_set_id%TYPE
   	, p_systype		 IN	  t7104_case_set_information.c901_system_type%TYPE 
    , p_userid		 IN   t7100_case_information.c7100_last_updated_by%TYPE
   )
   AS
   	CURSOR c_caseset
	    IS
		SELECT t7104.c7104_case_set_id case_setid, t7104.c207_set_id setid, t7104.c7100_case_info_id caseinfoid
			 , t7104.c526_product_request_detail_id req_dtl_id, t7104.c5010_tag_id tagid
		  FROM t7104_case_set_information t7104
		 WHERE t7104.c7100_case_info_id = p_caseinfoid
		   AND t7104.c7104_case_set_id = NVL (p_case_setid, t7104.c7104_case_set_id)
		   AND t7104.c207_set_id = NVL (p_setid, t7104.c207_set_id)
		   AND t7104.c901_system_type = NVL (p_systype, t7104.c901_system_type)
		   AND t7104.c7104_shipped_del_fl IS NULL
		   AND t7104.c7104_void_fl IS NULL; 
	v_reqstatusfl	t526_product_request_detail.c526_status_fl%TYPE;
	v_count 		NUMBER;
   BEGIN

	   FOR v_rec IN c_caseset
		LOOP
			IF v_rec.req_dtl_id IS NULL AND v_rec.tagid IS NOT NULL
			THEN
				/*--Comment for MNTTASK-7882 Auto Allocation
				SELECT COUNT (1)
				  INTO v_count
				  FROM t907_shipping_info
	             WHERE c907_ref_id IN (
	                       SELECT c907_ref_id FROM t907_shipping_info
	                       WHERE c907_ref_id = TO_CHAR (v_rec.case_setid)
	                            AND c901_source = 11385
	                            AND c907_status_fl = 40
	                            AND c907_void_fl IS NULL )
	               AND (NVL(c907_status_fl,0) < 40 OR c901_accept_status = 11462)	--Pending Receiving
	               AND c901_source = 11388
	               AND c907_void_fl IS NULL;
				   
				IF v_count > 0
				THEN
					UPDATE t7104_case_set_information
					   SET c7104_shipped_del_fl = 'Y'
						 , c7104_last_updated_by = p_userid
						 , c7104_last_updated_date = CURRENT_DATE
					 WHERE c7104_case_set_id = v_rec.case_setid
					   AND c7104_void_fl IS NULL;
				ELSE
					DELETE FROM t907_shipping_info 
					      WHERE c907_ref_id = TO_CHAR (v_rec.case_setid) AND c901_source IN (11385, 11388);
				*/	      
					UPDATE t7104_case_set_information
					   SET c7104_void_fl = 'Y'
					     , c7104_last_updated_by = p_userid
					     , c7104_last_updated_date = CURRENT_DATE
				     WHERE c7104_case_set_id = v_rec.case_setid
				       AND c7104_void_fl IS NULL;
				--END IF;
			ELSIF v_rec.req_dtl_id IS NOT NULL
			THEN
				SELECT NVL (c526_status_fl, '-999')
                  INTO v_reqstatusfl
                  FROM t526_product_request_detail
                 WHERE c526_product_request_detail_id = v_rec.req_dtl_id;

				IF v_reqstatusfl != '30'
				THEN
				
					UPDATE t7104_case_set_information
					   SET c7104_void_fl = 'Y'
						 , c7104_last_updated_by = p_userid
						 , c7104_last_updated_date = CURRENT_DATE
					 WHERE c7104_case_set_id = v_rec.case_setid
					   AND c7104_void_fl IS NULL;
					 
					--Void Loaner request and shipping details.
					gm_pkg_common_cancel.gm_cm_sav_cancelrow (v_rec.req_dtl_id
															, NULL--cancelreason
															, 'VDLON'
															, NULL--comments
															, p_userid
															 );
			
				ELSE
					UPDATE t7104_case_set_information
					   SET c7104_shipped_del_fl = 'Y'
						 , c7104_last_updated_by = p_userid
						 , c7104_last_updated_date = CURRENT_DATE
					 WHERE c7104_case_set_id = v_rec.case_setid
					   AND c7104_void_fl IS NULL;
				END IF;
			ELSIF  v_rec.tagid IS NULL
			THEN
            	UPDATE t7104_case_set_information
				   SET c7104_void_fl = 'Y'
					 , c7104_last_updated_by = p_userid
					 , c7104_last_updated_date = CURRENT_DATE 
                 WHERE c7104_case_set_id = v_rec.case_setid
                   AND c7104_void_fl IS NULL;
			END IF;
	END LOOP;
   END gm_case_del_sets;

    /*******************************************************
	* Description : Procedure to save item booking
	* Author	  : Gopinathan
	*******************************************************/
	PROCEDURE gm_sav_item_booking (
		p_caseinfo_id	IN	 t7100_case_information.c7100_case_info_id%TYPE
	  , p_pnums			IN	 VARCHAR2
	  , p_inputstr		IN	 VARCHAR2
	  , p_systype		IN	 t7104_case_set_information.c901_system_type%TYPE
	  , p_userid		IN	 t7100_case_information.c7100_created_by%TYPE
	  , p_det_inputstr  IN 	 CLOB DEFAULT NULL
	  , p_ship_inputstr IN 	 CLOB DEFAULT NULL
	)
	AS
		v_string	   VARCHAR2 (30000) := p_inputstr;
		v_substring    VARCHAR2 (1000);
		v_pnum		   VARCHAR2 (20);
		v_qty		   VARCHAR2 (20);
		v_surgerydate  DATE;
		v_case_status  t7100_case_information.c901_case_status%TYPE;
		v_type  t7100_case_information.c901_type%TYPE;
		v_startdate  DATE;
		v_enddate  DATE;
		v_prd_dtl_id t7104_case_set_information.c526_product_request_detail_id%TYPE; 
		v_pre_qty	 t7104_case_set_information.c7104_item_qty%TYPE; 
		v_case_set_id t7104_case_set_information.c7104_case_set_id%TYPE; 
		v_retn_flag  VARCHAR2(1);
		v_req_type   VARCHAR2(1) := '';
		
		v_cnt 			NUMBER;
		cur TYPES.cursor_type;
		CURSOR c_del_all_items 
		IS
			SELECT t7104.c205_part_number_id pnum, t7104.c526_product_request_detail_id req_dtl_id, t7104.c7104_item_qty qty FROM t7104_case_set_information t7104 
			WHERE t7104.c7100_case_info_id = p_caseinfo_id 
			  AND t7104.c7104_shipped_del_fl IS NULL 
			  AND t7104.c526_product_request_detail_id IS NOT NULL
			  AND t7104.c7104_void_fl IS NULL 
			  AND t7104.c205_part_number_id IS NOT NULL
			  AND t7104.c205_part_number_id NOT IN (SELECT token  FROM v_in_list WHERE token IS NOT NULL);
			  
		CURSOR c_del_items (v_pnum VARCHAR2)
		IS
			 SELECT t7104.c205_part_number_id pnum, t7104.c7104_item_qty qty, t7104.c526_product_request_detail_id req_dtl_id
	           FROM t7104_case_set_information t7104
	          WHERE t7104.c7100_case_info_id = p_caseinfo_id
	            AND t7104.c205_part_number_id        = v_pnum
	            AND t7104.c7104_shipped_del_fl IS NULL
	            AND t7104.c205_part_number_id IS NOT NULL
		        AND t7104.c7104_void_fl IS NULL;
		        
	BEGIN
		my_context.set_my_inlist_ctx (p_pnums);
		
		v_case_status := get_case_status(p_caseinfo_id);
		v_type := get_type(p_caseinfo_id);
	
		 
		  SELECT t7100.c901_type,t7100.c7100_surgery_date,t7103.c7103_start_date,t7103.c7103_end_date 
			INTO v_type,v_surgerydate,v_startdate,v_enddate
		    FROM t7100_case_information t7100,t7103_schedule_info t7103
		   WHERE t7100.c7100_case_info_id = p_caseinfo_id 
		     AND t7100.c7100_void_fl IS NULL 
		     AND t7100.C7100_Case_Info_Id=t7103.C7100_Case_Info_Id(+);
			
		  SELECT count(1)
	   	    INTO v_cnt
           FROM t7104_case_set_information t7104
          WHERE t7104.c7100_case_info_id = p_caseinfo_id
            AND t7104.c526_product_request_detail_id is not null
            AND t7104.c7104_shipped_del_fl IS NULL
	        AND t7104.c7104_void_fl IS NULL;
	       --to avoid code redundancy, assigning the req_type as 'Y'  
			IF v_type = '1006505' OR v_type = '1006506' OR v_type = '1006507' OR v_type = '107286'
			THEN
			v_req_type := 'Y';
			END IF;
		
	    --For Case requests(Loaner/ consignment ) we should not void the existing data.
	 	IF v_cnt = 0 AND v_req_type <> 'Y'
	 	THEN
	    	UPDATE t7104_case_set_information
			   SET c7104_void_fl         = 'Y'
			     , c7104_last_updated_by = p_userid
			     , c7104_last_updated_date = CURRENT_DATE
			 WHERE c7100_case_info_id = p_caseinfo_id 
			   AND c901_system_type = p_systype
			   AND c7104_void_fl    IS NULL;
	    END IF;
	       
		WHILE INSTR (v_string, '|') <> 0
		LOOP
			v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
			v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
			v_pnum		:= NULL;
			v_qty		:= NULL;
			V_retn_flag := NULL;
			
			v_pnum		:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_qty       := SUBSTR (v_substring, 1,INSTR (v_substring, ',') - 1); 
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_retn_flag := v_substring;
			
			
			BEGIN
				 SELECT t7104.c526_product_request_detail_id, t7104.c7104_item_qty
				 INTO v_prd_dtl_id, v_pre_qty
		           FROM t7104_case_set_information t7104
		          WHERE t7104.c7100_case_info_id = p_caseinfo_id
		            AND t7104.c205_part_number_id = v_pnum
		            AND t7104.c7104_shipped_del_fl IS NULL
			        AND t7104.c7104_void_fl IS NULL;
				EXCEPTION
		    	WHEN NO_DATA_FOUND THEN
		        	v_prd_dtl_id := NULL;
	    	END;
	    	
	    	IF v_prd_dtl_id IS NULL THEN

	    	--For Case requests(Loaner/ consignment ) we should not void the existing data.
		    		IF v_req_type <> 'Y'
					THEN
	 				   gm_sav_void_item(p_caseinfo_id,v_pnum,p_systype,p_userid);
	 				END IF;
				   gm_save_case_sets(p_caseinfo_id,null,v_pnum, v_qty,p_systype,v_surgerydate,p_userid, v_case_set_id);
				 
				   --CHANGED FOR PMT-6432		
			        UPDATE t7104_case_set_information 
                       SET c7104_retn_flag = v_retn_flag
                     WHERE c7100_case_info_id = p_caseinfo_id
                       AND c205_part_number_id = v_pnum
                       AND c7104_void_fl IS NULL;

                       ELSIF v_prd_dtl_id IS NOT NULL AND v_pre_qty != v_qty THEN
			
				gm_pkg_op_product_requests.gm_sav_req_item(v_prd_dtl_id,v_pnum,v_qty,p_userid);
				
				 UPDATE t7104_case_set_information
		            SET c7104_item_qty         = v_qty
		              , c7104_last_updated_by = p_userid
		              , c7104_last_updated_date = CURRENT_DATE
		          WHERE c7100_case_info_id = p_caseinfo_id 
				    AND c901_system_type = NVL (p_systype, -9999)
				    AND c205_part_number_id = v_pnum
		            AND c7104_void_fl    IS NULL;
			END IF;
			
			IF p_systype = 103644 OR p_systype = 107860  --For item consignment request
			THEN
				gm_sav_temp_case_dtls(  v_case_set_id , p_det_inputstr, p_ship_inputstr);
			END IF;			
		END LOOP;
			
			--For Case requests(Loaner/ consignment ) we should not void the existing data.
			IF v_req_type <> 'Y'
			THEN
				FOR cur IN c_del_all_items
				LOOP
					 --Void the removed part's request details.
					 gm_pkg_common_cancel.gm_cm_sav_cancelrow (cur.req_dtl_id
																, NULL--cancelreason
																, 'VDLON'
																, NULL--comments
																, p_userid
																 );
					  gm_sav_void_item(p_caseinfo_id,cur.pnum,p_systype,p_userid);
				END LOOP;
			END IF;
		
		
		IF v_type = '1006504'  OR v_req_type = 'Y'
		THEN
			UPDATE T7100_CASE_INFORMATION 
			   SET C901_CASE_STATUS='19524'  -- Active Event
			     , C7100_LAST_UPDATED_BY=p_userid
			     , C7100_LAST_UPDATED_DATE=CURRENT_DATE 
			WHERE c7100_case_info_id = p_caseinfo_id 
			  AND C7100_VOID_FL IS NULL;		
		END IF;
	END gm_sav_item_booking;
	
    /*******************************************************
	* Description : Procedure to void the booked item 
	* Author	  : Gopinathan
	*******************************************************/
	PROCEDURE gm_sav_void_item(
		p_caseinfo_id	IN	 t7100_case_information.c7100_case_info_id%TYPE
	  , p_pnum			IN	 t7104_case_set_information.c205_part_number_id%TYPE
	  , p_systype		IN	 t7104_case_set_information.c901_system_type%TYPE
	  , p_userid		IN	 t7100_case_information.c7100_created_by%TYPE
	)
	AS
	BEGIN
		UPDATE t7104_case_set_information
		   SET c7104_void_fl         = 'Y'
		     , c7104_last_updated_by = p_userid
		     , c7104_last_updated_date = CURRENT_DATE
		 WHERE c7100_case_info_id = p_caseinfo_id 
		   AND c901_system_type = p_systype
		   AND c205_part_number_id = p_pnum
		   AND c7104_void_fl    IS NULL;			
	END gm_sav_void_item;
	
/**************************************************************
* Description : Procedure to save Case attributes details
* Author  : Elango
***************************************************************/
PROCEDURE gm_sav_case_attr (
        p_caseinfo_id         IN t7102_case_attribute.c7100_case_info_id%TYPE,
        p_case_attrInputstr   IN CLOB,
        p_userid              IN t7102_case_attribute.c7102_created_by%TYPE)
AS
    v_strlen   	 NUMBER          := NVL (LENGTH (p_case_attrInputstr), 0) ;
    v_string     VARCHAR2 (4000) := p_case_attrInputstr;
    v_substring  VARCHAR2 (4000);
    v_case_attribute_id  t7102_case_attribute.c901_attribute_type%TYPE;
    v_case_info_id   t7102_case_attribute.c901_attribute_type%TYPE;
    v_attr_type      t7102_case_attribute.c901_attribute_type%TYPE;
    v_attr_value     t7102_case_attribute.c7102_attribute_value%TYPE;
    v_void_fl        t7102_case_attribute.C7102_VOID_FL%TYPE;
    v_caseinfoid	 t7102_case_attribute.c7100_case_info_id%TYPE;
BEGIN        
    --input str format ---::: case_attribute_id^case_info_id^attr_type^attr_value^void_fl
    WHILE INSTR (v_string, '|') <> 0
    LOOP
        v_attr_type  := NULL;
        v_attr_value := NULL;
        v_substring  := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
        v_string     := SUBSTR (v_string, INSTR (v_string, '|')  + 1) ;
        v_case_attribute_id  := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
        v_substring  := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        v_case_info_id := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
        v_substring  := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        v_attr_type := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
        v_substring  := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        v_attr_value := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
        v_substring  := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        v_void_fl := v_substring;
        
        --Void the attribute record for incoming attr type, case info id combination.
	     UPDATE t7102_case_attribute
	        SET C7102_VOID_FL            = 'Y', 
	            C7102_LAST_UPDATED_DATE = CURRENT_DATE,
	            C7102_LAST_UPDATED_BY = p_userid
	      WHERE C7102_CASE_ATTRIBUTE_ID = v_case_attribute_id
		       -- To avoid duplicate entry of attribute value in same type
	         OR (C7100_CASE_INFO_ID = v_case_info_id
		          AND C901_ATTRIBUTE_TYPE = v_attr_type
		          AND C7102_ATTRIBUTE_VALUE = v_attr_value
	            );
        
	    --If the void fl from input str is null, then we might need to update / insert the values for the same attr type.
        IF v_void_fl IS NULL
        THEN
        	 SELECT DECODE(v_case_attribute_id,NULL,NVL(v_case_info_id,p_caseinfo_id),v_case_info_id) INTO v_caseinfoid FROM DUAL;
       		 gm_pkg_sm_casebook_txn.gm_sav_case_attr_dtls (v_caseinfoid, v_case_attribute_id, v_attr_type, v_attr_value, p_userid) ;
       		 
       		 --Update Parent record
	       	 UPDATE t7100_case_information 
				SET c7100_last_updated_by=p_userid
				  , c7100_last_updated_date=CURRENT_DATE 
			  WHERE c7100_case_info_id = v_caseinfoid; 
        END IF;
        
    END LOOP;               
END gm_sav_case_attr;
/**************************************************************
* Description : Procedure to save set(System) attributes details
* Author  : Elango
***************************************************************/
PROCEDURE gm_sav_case_attr_dtls (
        p_case_info_id    IN t7102_case_attribute.C7100_CASE_INFO_ID%TYPE,
        p_case_attr_id    IN t7102_case_attribute.C7102_CASE_ATTRIBUTE_ID%TYPE,
        p_attr_type       IN t7102_case_attribute.C901_ATTRIBUTE_TYPE%TYPE,
        p_attr_value      IN t7102_case_attribute.C7102_ATTRIBUTE_VALUE%TYPE,
        p_userid          IN t7102_case_attribute.C7102_CREATED_BY%TYPE)
AS
BEGIN
	--Update the new chnaged attr values & void fl as null for the incoming attr type, case info id combination.
     UPDATE t7102_case_attribute
        SET C7102_ATTRIBUTE_VALUE   = p_attr_value,
        	C7102_VOID_FL           = NULL,
            C7102_LAST_UPDATED_DATE = CURRENT_DATE, 
            C7102_LAST_UPDATED_BY   = p_userid
      WHERE  C7102_CASE_ATTRIBUTE_ID = p_case_attr_id
      -- To avoid duplicate entry of attribute value in same type
         OR (C7100_CASE_INFO_ID = p_case_info_id
	          AND C901_ATTRIBUTE_TYPE = p_attr_type
	          AND C7102_ATTRIBUTE_VALUE = p_attr_value
            );
        
    IF (SQL%ROWCOUNT = 0) THEN
    
         INSERT
           INTO t7102_case_attribute
            (
                C7102_CASE_ATTRIBUTE_ID, C7100_CASE_INFO_ID, C901_ATTRIBUTE_TYPE
              , C7102_ATTRIBUTE_VALUE, C7102_VOID_FL, C7102_CREATED_BY
              , C7102_CREATED_DATE
            )
            VALUES
            (
                s7102_case_attribute.nextval, p_case_info_id, p_attr_type
              , p_attr_value, NULL, p_userid
              , CURRENT_DATE
            ) ;
            
    END IF;         
    
END gm_sav_case_attr_dtls;
    /****************************************************************
	* Description : Procedure is used to save temp table ship info's
	* Author	  : Elango
	*****************************************************************/
	PROCEDURE gm_sav_temp_case_dtls(
		p_case_set_info_id	IN	 t7104_case_set_information.c7104_case_set_id%TYPE
	  , p_det_inputstr		IN	 VARCHAR2
	  , p_ship_inutstr 		IN	 VARCHAR2
	)
	AS
	   v_det_str  VARCHAR2(200) := p_det_inputstr;
	   v_ship_str VARCHAR2(200) := p_ship_inutstr;
	   v_ass_repid 		NUMBER;
	   v_req_date		DATE;
	   v_ship_date 		DATE;
	   v_source 		NUMBER;
	   v_shipt_to 	    NUMBER;
	   v_shipt_to_id 	VARCHAR2(30);
	   v_carrier  	    NUMBER;
	   v_mode 	        NUMBER;
	   v_address_id     NUMBER;
	   v_attn_to		VARCHAR2(255);
	   v_date_fmt 		VARCHAR2(15);
	   v_ship_instruction 	VARCHAR2(4000);
	BEGIN		
		--change date format 
		--v_date_fmt := GET_RULE_VALUE('DATEFMT','DATEFORMAT') ;
		 SELECT get_compdtfmt_frm_cntx()
         INTO v_date_fmt
         FROM DUAL;
		IF v_det_str IS NOT NULL
		THEN
		    v_ass_repid     :=  SUBSTR (v_det_str, 0, INSTR (v_det_str, ',') - 1);
		    v_det_str 		:=  SUBSTR(v_det_str, INSTR (v_det_str, ',') + 1 );	
			v_req_date		:=  TO_DATE(NVL( SUBSTR(v_det_str, 0, INSTR (v_det_str, ',') - 1),NULL) , v_date_fmt );
			v_det_str 		:=  SUBSTR(v_det_str, INSTR (v_det_str, ',') + 1 );			
			v_ship_date 	:=  TO_DATE(NVL( v_det_str, NULL), v_date_fmt);
		END IF;
		
		IF v_ship_str IS NOT NULL
		THEN
			v_source		:=  SUBSTR (v_ship_str, 1, INSTR (v_ship_str, '^') - 1);
			v_ship_str  	:=  SUBSTR (v_ship_str, INSTR (v_ship_str, '^') + 1);
			v_shipt_to		:=  SUBSTR (v_ship_str, 1, INSTR (v_ship_str, '^') - 1);
			v_ship_str		:=  SUBSTR (v_ship_str, INSTR (v_ship_str, '^') + 1);
			v_shipt_to_id	:=  SUBSTR (v_ship_str, 1, INSTR (v_ship_str, '^') - 1);
			v_ship_str		:=  SUBSTR (v_ship_str, INSTR (v_ship_str, '^') + 1);
			v_carrier		:=  SUBSTR (v_ship_str, 1, INSTR (v_ship_str, '^') - 1);
			v_ship_str 		:=  SUBSTR (v_ship_str, INSTR (v_ship_str, '^') + 1);
			v_mode			:=  SUBSTR (v_ship_str, 1, INSTR (v_ship_str, '^') - 1);
			v_ship_str 		:=  SUBSTR (v_ship_str, INSTR (v_ship_str, '^') + 1);
			v_address_id	:=  SUBSTR (v_ship_str, 1, INSTR (v_ship_str, '^') - 1);
			v_ship_str 		:=  SUBSTR (v_ship_str, INSTR (v_ship_str, '^') + 1);
			v_attn_to   	:=  SUBSTR (v_ship_str, 1, INSTR (v_ship_str, '^') - 1);
			v_ship_str 		:=  SUBSTR (v_ship_str, INSTR (v_ship_str, '^') + 1);
			v_ship_instruction := v_ship_str;
		END IF;	
		
		gm_sav_temp_case(p_case_set_info_id, v_ass_repid, v_req_date, v_ship_date, v_source, v_shipt_to, v_shipt_to_id, v_carrier, v_mode, v_address_id, v_attn_to, v_ship_instruction);
		
	END gm_sav_temp_case_dtls;
    /***************************************************************
	* Description : Procedure is used to save temp table ship info's
	* Author	  : Elango
	****************************************************************/
	PROCEDURE gm_sav_temp_case(
		p_case_set_info_id	IN	 t7104_case_set_information.c7104_case_set_id%TYPE
	  , p_ass_repid 	    IN	 NUMBER
	  , p_req_date		    IN	 DATE
	  , p_ship_date 		IN	 DATE
	  , p_source 		    IN	 NUMBER
	  , p_shipt_to 		    IN	 NUMBER
	  , p_shipt_to_id 		IN	 VARCHAR2
	  , p_carrier 		    IN	 NUMBER
	  , p_mode  		    IN	 NUMBER
	  , p_address_id	    IN	 NUMBER
	  , p_attn_to		    IN	 VARCHAR2	
	  , p_ship_instruction	IN	 VARCHAR2
	)
	AS
	BEGIN	
	
		INSERT
		   INTO my_temp_case_ship_det
		    (
		        C7104_CASE_SET_ID, C526_REQUIRED_DATE, C907_SHIP_TO_DT, C703_ASS_REP_ID
		      , C901_SOURCE, C901_SHIP_TO, C907_SHIP_TO_ID
		      , C901_DELIVERY_CARRIER, C901_DELIVERY_MODE, C106_ADDRESS_ID
		      , C907_OVERRIDE_ATTN_TO, C907_SHIP_INSTRUCTION
		    )
		    VALUES
		    (
		       p_case_set_info_id , p_req_date, p_ship_date, p_ass_repid
		      , p_source , p_shipt_to ,  p_shipt_to_id
		      , p_carrier, p_mode , p_address_id
		      , p_attn_to, p_ship_instruction
		    ) ; 
		    
	END gm_sav_temp_case;
	--
	
	/*******************************************************
	   * Description : This procedure will get the required information that is used for sending out the email,
	    				when the Item Request is approved by PD.
	   * Author 	 : Anilkumar
	*******************************************************/
	PROCEDURE gm_sav_request_frm_case (
		p_input_str	  IN	VARCHAR2,
		P_out_request_id	  OUT	t520_request.c520_request_id%TYPE,
		P_out_consgn_id	   	  OUT	T504_CONSIGNMENT.C504_CONSIGNMENT_ID%TYPE
	)AS 
	v_OUT_CONSIGN_ID	VARCHAR2(1000);
	v_OUT_REQ_ID		VARCHAR2(1000);
	V_OUT_SHIP_ID 		VARCHAR2(1000);
	v_date_format 		VARCHAR2(100);
	v_req_id 			VARCHAR2(50);
	v_OUTPUT_REQUEST_ID		VARCHAR2(1000);
	v_OUTPUT_CONSIGN_ID		VARCHAR2(4000);
	-- Cursor is used to get the info of items that are approved PD, this info is used to create RQ and CN as well.
	CURSOR v_cur_req
		IS
		SELECT T525.C525_PRODUCT_REQUEST_ID PAR_REQ_ID,
		  T525.C525_REQUEST_FOR_ID REQ_FOR,
		  T525.C901_REQUEST_FOR REQUEST_FOR,
	       RTRIM (XMLAGG (xmlelement (E, T526.C205_PART_NUMBER_ID
	      || '^'
	      ||T526.C526_QTY_REQUESTED
	      ||'^'
	      ||TO_CHAR(T526.C526_Required_date,v_date_format)
	      ||'^|')) .extract ('//text()'), ',') RQ_STR,
		  T907.C907_SHIP_TO_ID SHIP_TO_ID,
		  T907.C901_SHIP_TO SHIP_TO,
		  T907.C901_DELIVERY_CARRIER SHIP_CAR,
		  T907.C901_DELIVERY_MODE SHIP_MODE,
	      T907.C106_ADDRESS_ID ADDR_ID,
	      T907.C907_FRIEGHT_AMT FRGT_AMT,
		  T525.C525_REQUEST_BY_ID REQ_BY_ID,
		  T525.C703_SALES_REP_ID REP_ID,
		  T526.C526_REQUIRED_DATE REQUIRE_DATE,
		  T525.C525_CREATED_BY USER_ID,
		  T525.C704_ACCOUNT_ID ACCT_ID,
		  T907.C907_SHIP_TO_DT PLAN_SHIP_DT
		FROM T525_PRODUCT_REQUEST T525,
		  T526_PRODUCT_REQUEST_DETAIL T526,
		  T907_SHIPPING_INFO T907, t7100_case_information t7100,t7104_case_set_information T7104
			WHERE T525.C525_VOID_FL                 IS NULL
					AND T526.C526_VOID_FL                   IS NULL
					AND T907.C907_VOID_FL                   IS NULL
					AND T526.C205_PART_NUMBER_ID            IS NOT NULL
					AND T525.C525_PRODUCT_REQUEST_ID         = T526.C525_PRODUCT_REQUEST_ID
					AND T525.C525_PRODUCT_REQUEST_ID         = T907.C525_PRODUCT_REQUEST_ID
				    AND t7100.c7100_case_info_id             = T7104.C7100_CASE_INFO_ID
		     	    AND t7104.c526_product_request_detail_id = T526.C526_PRODUCT_REQUEST_DETAIL_ID
				    AND t907.c907_ref_id                     = T526.C526_PRODUCT_REQUEST_DETAIL_ID
					AND T907.C907_SHIP_TO_ID                IS NOT NULL
					AND T907.C901_SHIP_TO                   IS NOT NULL
				-- below condition is commented, since we dnt have any sales rep id for consignment items.	
			--		AND T525.C703_SALES_REP_ID              IS NOT NULL
					AND T526.C526_STATUS_FL                  =10
					AND T526.C526_PRODUCT_REQUEST_DETAIL_ID IN
			  		(SELECT * FROM v_in_list)
					 GROUP BY T525.C525_PRODUCT_REQUEST_ID,
						  T525.C525_REQUEST_FOR_ID,
				      	  T525.C703_SALES_REP_ID,
						  T907.C907_SHIP_TO_ID,
						  T907.C901_SHIP_TO,
						  T907.C901_DELIVERY_CARRIER, 
						  T907.C901_DELIVERY_MODE,
						  T907.C106_ADDRESS_ID, 
						  T907.C907_FRIEGHT_AMT,
					      T525.C525_REQUEST_BY_ID ,
						  T525.C525_CREATED_BY,
						  T525.C704_ACCOUNT_ID,
						  T907.C907_SHIP_TO_DT,
						  T526.C526_Required_date,
						  T525.C901_REQUEST_FOR
			ORDER BY T525.C525_PRODUCT_REQUEST_ID;
		
		CURSOR v_cur_ship
		 IS
			SELECT t520.c520_request_id reqid ,
			  t525.c525_product_request_id prod_req_id ,
			  t526.c526_product_request_detail_id req_dt_id,
			  shipping.c901_delivery_carrier shipcar,
			  shipping.c901_delivery_mode shipmode,
			  t525.c525_created_by userid
			FROM t520_request t520 ,
			  t504_consignment t504 ,
			  t505_item_consignment t505 ,
			  t525_product_request t525 ,
			  t526_product_request_detail t526 ,
			  (SELECT t907.c907_ref_id,
			    t907.c901_delivery_carrier,
			    t907.c901_delivery_mode
			  FROM t907_shipping_info t907
			  WHERE t907.c907_ref_id IN
			    (SELECT token FROM v_in_list WHERE token IS NOT NULL
			    )
			  AND t907.c907_void_fl IS NULL
			  ) shipping
			WHERE t520.c520_request_id               = t504.c520_request_id
			AND t504.c504_consignment_id             = t505.c504_consignment_id
			AND t520.c520_ref_id                     = t525.c525_product_request_id
			AND t525.c525_product_request_id         = t526.c525_product_request_id
			AND T526.C526_PRODUCT_REQUEST_DETAIL_ID IN
			  (SELECT token FROM v_in_list WHERE token IS NOT NULL
			  )
			AND t505.c205_part_number_id            = t526.c205_part_number_id
			AND t526.c526_product_request_detail_id = shipping.c907_ref_id
			AND t520.c520_void_fl                  IS NULL
			AND t504.c504_void_fl                  IS NULL
			AND t505.c505_void_fl                  IS NULL
			AND t526.c526_void_fl                  IS NULL
			AND t525.c525_void_fl                  IS NULL;

           
	BEGIN
		-- putting req ids in vinlist. 
		my_context.set_my_inlist_ctx (p_input_str) ;  
		v_date_format := GET_RULE_VALUE('DATEFMT', 'DATEFORMAT');
		
	      FOR v_cur IN v_cur_req
            LOOP
           --if the request for type is 40050, then it will call the account item requests flow, otherwise call the existing prod_request flow
            IF v_cur.REQUEST_FOR = '40050'
            THEN
            					 
			gm_pkg_op_account_request.gm_sav_initiate_request(NULL,v_cur.REQUIRE_DATE,'50618',NULL,NULL,'40025',
					 v_cur.REQ_FOR,'50625',v_cur.REQ_BY_ID,v_cur.SHIP_TO,v_cur.SHIP_TO_ID,NULL,NULL,v_cur.USER_ID,v_cur.RQ_STR,'50060',
					 v_cur.REP_ID,v_cur.ACCT_ID,NULL,v_cur.PLAN_SHIP_DT,NULL,v_OUT_REQ_ID,v_OUT_CONSIGN_ID,NULL);	
			ELSE		 
            -- here we are calling this procedure to get the RQ and CN by passing the info whatever the above cursor is returned.
			 gm_pkg_op_process_prod_request.gm_sav_initiate_request(NULL,v_cur.REQUIRE_DATE,'50618',NULL,NULL,'40021',
					 v_cur.REQ_FOR,'50626',v_cur.REQ_BY_ID,v_cur.SHIP_TO,v_cur.SHIP_TO_ID,NULL,NULL,v_cur.USER_ID,v_cur.RQ_STR,'50060',
					 v_cur.REP_ID,v_cur.ACCT_ID,NULL,v_cur.PLAN_SHIP_DT,NULL,v_OUT_REQ_ID,v_OUT_CONSIGN_ID,NULL);
			END IF;		 
	 
					 
			UPDATE T520_request
			SET c520_ref_id       = v_cur.PAR_REQ_ID
			WHERE C520_request_id = v_OUT_REQ_ID
			AND c520_void_fl IS NULL;
			
			IF v_OUT_REQ_ID IS NOT NULL
		    THEN
		      gm_pkg_cm_shipping_trans.gm_sav_shipping(v_OUT_REQ_ID,'50184',v_cur.SHIP_TO,v_cur.SHIP_TO_ID,
		        	v_cur.SHIP_CAR,v_cur.SHIP_MODE,NULL,v_cur.ADDR_ID,v_cur.FRGT_AMT,v_cur.USER_ID,V_OUT_SHIP_ID,NULL,NULL,NULL,NULL);
		    END IF;
			
			IF v_OUT_CONSIGN_ID IS NOT NULL
		    THEN
		      BEGIN
			    SELECT c520_request_id
				INTO v_req_id
				FROM t520_request
				WHERE c520_master_request_id = v_OUT_REQ_ID;
		      EXCEPTION
		    	WHEN NO_DATA_FOUND THEN
		        	v_req_id := NULL;
	    	  END;
				IF v_req_id IS NOT NULL 
				THEN
					UPDATE T520_request
					SET c520_ref_id       = v_cur.PAR_REQ_ID
					WHERE C520_request_id = v_req_id;
				END IF;
				
		    END IF;
		    v_OUTPUT_REQUEST_ID := v_OUTPUT_REQUEST_ID ||v_OUT_REQ_ID || ',';
		    v_OUTPUT_CONSIGN_ID := v_OUTPUT_CONSIGN_ID ||v_OUT_CONSIGN_ID || ',';
			END LOOP;
			
			SELECT SUBSTR(v_OUTPUT_REQUEST_ID,1,length(v_OUTPUT_REQUEST_ID)-1) 
				INTO v_OUTPUT_REQUEST_ID
			FROM dual;
			
			SELECT SUBSTR(v_OUTPUT_CONSIGN_ID,1,length(v_OUTPUT_CONSIGN_ID)-1) 
				INTO v_OUTPUT_CONSIGN_ID
			FROM dual;
			
			BEGIN
				FOR v_sh_cursor IN v_cur_ship
				LOOP
					 UPDATE T504_CONSIGNMENT
						SET c504_delivery_carrier = v_sh_cursor.shipcar,
						  c504_delivery_mode      = v_sh_cursor.shipmode,
						  c504_last_updated_date  = CURRENT_DATE,
						  c504_last_updated_by    = v_sh_cursor.userid
						WHERE c520_request_id     = v_sh_cursor.reqid
						AND c504_void_fl         IS NULL;
				END LOOP;
            END;
			
			P_out_request_id := v_OUTPUT_REQUEST_ID;
			P_out_consgn_id := v_OUTPUT_CONSIGN_ID;
	END gm_sav_request_frm_case;
	/*******************************************************
	   * Description : This procedure will call by job. it will close past MCS cases.
	   * Author 	 : Gopinathan
	*******************************************************/	
	PROCEDURE gm_sav_mcs_case_close
	   AS
	      
	      CURSOR cur_past_case
		      IS
				 SELECT t7102.c7100_case_info_id caseinfoid
				   FROM t7100_case_information t7100, t7102_case_attribute t7102
				  WHERE t7100.c7100_case_info_id    = t7102.c7100_case_info_id
				    AND t7100.c901_case_status      = '11091' -- Active
				    AND t7100.c7100_surgery_date    < TRUNC (CURRENT_DATE)
				    AND t7100.c7100_void_fl        IS NULL
				    AND t7102.c7102_void_fl        IS NULL
				    AND t7102.c901_attribute_type   = '4000666' -- Source
				    AND t7102.c7102_attribute_value = '104420'; -- MCS APP
		v_message  VARCHAR2(4000);
	   BEGIN
	
		  FOR v_rec IN cur_past_case
		  LOOP
	
			  UPDATE t7100_case_information
		         SET c901_case_status = 11094   -- close
		       WHERE c7100_case_info_id = v_rec.caseinfoid;
	
		     --  save log for history
	 		 GM_UPDATE_LOG(v_rec.caseinfoid,'closed by job','4000665','30301',v_message); --4000665 - Case Status Log ,30301 -Manual Load 
		       
		END LOOP;
   END gm_sav_mcs_case_close;
	--   
END;
/
