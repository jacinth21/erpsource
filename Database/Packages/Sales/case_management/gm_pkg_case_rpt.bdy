CREATE OR REPLACE PACKAGE BODY gm_pkg_case_rpt
IS
/* *******************************************************************************
 * Author: Agilan singaravel
 * Procedure:gm_fch_acc_nm
 * Description:Fetch account list based on logged in rep
 ***********************************************************************************/ 
 PROCEDURE gm_fch_acc_nm (
 	  p_rep_id       IN    T703_SALES_REP.c703_sales_rep_id%TYPE
 	, p_acc_list     OUT   CLOB
  )
  AS
  BEGIN

       SELECT JSON_ARRAYAGG(JSON_OBJECT(
	   				'ID' VALUE AC_ID,
  				  	'NAME' VALUE NVL(V700.ac_name_en,V700.ac_name)
  			 )order by AC_ID RETURNING CLOB) INTO p_acc_list 
	    FROM V700_TERRITORY_MAPPING_DETAIL V700 , T704_ACCOUNT T704
	   WHERE V700.AC_ID = T704.C704_ACCOUNT_ID
         AND V700.REP_ID = p_rep_id
         AND NVL(T704.C704_ACTIVE_FL,'Y')='Y'
         AND  T704.C704_VOID_FL IS NULL
         AND V700.AC_ID IS NOT NULL;
	      
  END gm_fch_acc_nm;
  
/* *******************************************************************************
 * Author: Agilan singaravelan
 * Procedure:gm_fch_set_dtl_Sb
 * Description:Fetch set details from set bundle id
 ***********************************************************************************/ 
 PROCEDURE gm_fch_set_dtl_Sb (
 	  p_setbn_id       IN    T2076_SET_BUNDLE_DETAILS.C2075_SET_BUNDLE_ID%TYPE
 	, p_set_list       OUT   CLOB
  )
  AS
  BEGIN

	  SELECT JSON_ARRAYAGG(JSON_OBJECT(
	   				'SETID' VALUE c207_set_id,
  				  	'SETNAME' VALUE GET_SET_NAME(C207_set_id)
  			 )order by c207_set_id RETURNING CLOB) INTO p_set_list 
	    FROM T2076_SET_BUNDLE_DETAILS 
	   WHERE C2075_SET_BUNDLE_ID = p_setbn_id
	     AND C2076_VOID_FL IS NULL;	 
	     
   END gm_fch_set_dtl_Sb;
   
/* *******************************************************************************
 * Author: Agilan singaravelan
 * Procedure:gm_fch_validate_tag
 * Description:Validate tag number based on set id
 ***********************************************************************************/ 
 PROCEDURE gm_fch_validate_tag (
 	  p_set_id       IN    t207_set_master.c207_set_id%type
 	, p_tag_num      IN    t5010_tag.C5010_TAG_ID%type
 	, p_tag_val      OUT   VARCHAR2
  )
  AS
  v_count        NUMBER;
  v_tag_number   t5010_tag.C5010_TAG_ID%type;
  BEGIN

	    SELECT LPAD(p_tag_num,'7','0')  --LPAD will append zero before the tag number
	    INTO v_tag_number 
	    FROM dual;

	   
	  	SELECT count(*)
	  	  INTO v_count
	  	  FROM T5010_TAG 
	  	 WHERE C207_SET_ID = p_set_id 
	  	   AND c5010_tag_id = v_tag_number
	  	   AND C901_INVENTORY_TYPE = '10003'  -- Loose sales Consignment
	  	   AND C5010_VOID_FL IS NULL;
	   	   
	IF (v_count = 0) THEN
		p_tag_val := 'N';
	ELSE
		p_tag_val := 'Y';
    END IF;
     
   END gm_fch_validate_tag;  
   
/* *******************************************************************************
 * Author: Agilan singaravelan
 * Procedure:gm_gen_cash_id
 * Description:To generate the cash id based on rep id and surgery date 
 ***********************************************************************************/ 
 PROCEDURE gm_fch_case_id (
 	  p_surg_dt      IN    T7100_case_information.c7100_surgery_date%type
 	, p_rep_id       IN    t7100_case_information.c703_sales_rep_id%TYPE
 	, p_cash_id      OUT   VARCHAR2
  )   
  AS
  		  v_seq		   NUMBER;
  BEGIN
	 
   	    SELECT  NVL (MAX(TO_NUMBER (SUBSTR (C7100_CASE_ID,INSTR(C7100_CASE_ID, '-', -1)+1))), 0) +1  			
	  	  INTO  v_seq
	 	  FROM  t7100_case_information
	 	 WHERE  c7100_case_id LIKE 'CS-' || p_rep_id || '-' || TO_char (p_surg_dt, 'yyyymmdd') || '%' 
	 	   AND  c7100_void_fl IS NULL; 
	
	p_cash_id	:=
		   'CS-'
			|| p_rep_id
		    || '-'
			|| TO_char (p_surg_dt, 'yyyymmdd')
		    || '-'
			|| v_seq;	
			
  END gm_fch_case_id;

/* *******************************************************************************
 * Author: Parthiban
 * Procedure:gm_fch_case_dsh_data
 * Description:Fetch case scheduler dashboard data on party id
 ***********************************************************************************/ 
PROCEDURE gm_fch_case_dsh_data(			
		p_party_id     	IN     T7100_CASE_INFORMATION.C7100_OWNER_PARTY_ID%TYPE,
		p_date_fmt      IN     VARCHAR2,
		p_out_cursor 	OUT    CLOB
 )
 AS
	 v_lm_first_day VARCHAR2(50);
	 v_lm_last_day VARCHAR2(50);
	 v_tm_first_day VARCHAR2(50);
	 v_tm_last_day VARCHAR2(50);
	 v_nm_first_day VARCHAR2(50);
	 v_nm_last_day VARCHAR2(50);
	 v_json_out JSON_OBJECT_T;
	 -- v_dashboard_data VARCHAR2(4000);
	 
BEGIN
	
	  select to_char(Last_Day(ADD_MONTHS(current_date,-2))+1,p_date_fmt), to_char(Last_Day(ADD_MONTHS(current_date,-1)),p_date_fmt), 
	  		 to_char(Last_Day(ADD_MONTHS(current_date,-1))+1,p_date_fmt), to_char(Last_Day(ADD_MONTHS(current_date,+0)),p_date_fmt), 
	  		 to_char(Last_Day(ADD_MONTHS(current_date,+0))+1,p_date_fmt), to_char(Last_Day(ADD_MONTHS(current_date,+1)),p_date_fmt) 
	  	INTO v_lm_first_day,v_lm_last_day,v_tm_first_day,v_tm_last_day,v_nm_first_day,v_nm_last_day from dual;
	
	    
	    	   
		SELECT JSON_OBJECT( 'LM_DOBOOKED' VALUE (LM_DOBOOKED), 'TM_DOBOOKED' VALUE (TM_DOBOOKED), 'NM_DOBOOKED' VALUE (NM_DOBOOKED),
                                  'LM_DOPEND' VALUE (LM_DOPEND), 'TM_DOPEND' VALUE (TM_DOPEND), 'NM_DOPEND' VALUE (t.NM_DOPEND),
                                  'LM_CANCELEDCASE' VALUE (LM_CANCELEDCASE), 'TM_CANCELEDCASE' VALUE (TM_CANCELEDCASE), 'NM_CANCELEDCASE' VALUE (NM_CANCELEDCASE),
                                  'LM_LOANER' VALUE (LM_LOANER), 'TM_LOANER' VALUE (TM_LOANER), 'NM_LOANER' VALUE (NM_LOANER),
                                   'LM_NOLOANER' VALUE (LM_NOLOANER),  'TM_NOLOANER' VALUE (TM_NOLOANER),  'NM_NOLOANER' VALUE (NM_NOLOANER)
        	  	 ) INTO p_out_cursor 
        	FROM( 
	
	 	SELECT  
         SUM(CASE WHEN c7100_do_flag = 'Y' AND C7100_SURGERY_DATE >= to_date(v_lm_first_day,p_date_fmt) AND t7100.c7100_surgery_date <= to_date(v_lm_last_day,p_date_fmt) AND c901_case_status != '107163' THEN 1 ELSE 0 END) AS  LM_DOBOOKED,
         SUM(CASE WHEN c7100_do_flag = 'Y' AND C7100_SURGERY_DATE >= to_date(v_tm_first_day,p_date_fmt) AND  t7100.c7100_surgery_date <= CURRENT_DATE AND c901_case_status != '107163' THEN 1 ELSE 0 END)AS TM_DOBOOKED,
         SUM(CASE WHEN c7100_do_flag = 'Y' AND C7100_SURGERY_DATE >= to_date(v_nm_first_day,p_date_fmt) AND  t7100.c7100_surgery_date <= to_date(v_nm_last_day,p_date_fmt) AND c901_case_status != '107163' THEN 1 ELSE 0 END)AS NM_DOBOOKED,
         SUM(CASE WHEN c7100_do_flag = 'N' AND C7100_SURGERY_DATE >= to_date(v_lm_first_day,p_date_fmt) AND t7100.c7100_surgery_date <= to_date(v_lm_last_day,p_date_fmt) AND c901_case_status != '107163' THEN 1 ELSE 0 END)AS LM_DOPEND,
         SUM(CASE WHEN c7100_do_flag = 'N' AND C7100_SURGERY_DATE >= to_date(v_tm_first_day,p_date_fmt) AND  t7100.c7100_surgery_date <= CURRENT_DATE AND c901_case_status != '107163' THEN 1 ELSE 0 END) AS TM_DOPEND,
         SUM(CASE WHEN c7100_do_flag = 'N' AND C7100_SURGERY_DATE >= to_date(v_nm_first_day,p_date_fmt) AND  t7100.c7100_surgery_date <= to_date(v_nm_last_day,p_date_fmt) AND c901_case_status != '107163' THEN 1 ELSE 0 END)AS NM_DOPEND,
         SUM(CASE WHEN C7100_SURGERY_DATE >= to_date(v_lm_first_day,p_date_fmt) AND t7100.c7100_surgery_date <= to_date(v_lm_last_day,p_date_fmt)  AND c901_case_status = '107163' THEN 1 ELSE 0 END)AS LM_CANCELEDCASE,
         SUM(CASE WHEN c7100_surgery_date >= to_date(v_tm_first_day,p_date_fmt) AND  t7100.c7100_surgery_date <= to_date(v_tm_last_day,p_date_fmt) AND c901_case_status = '107163' THEN 1 ELSE 0 END)AS TM_CANCELEDCASE,
         SUM(CASE WHEN c7100_surgery_date >= to_date(v_nm_first_day,p_date_fmt) AND  t7100.c7100_surgery_date <= to_date(v_nm_last_day,p_date_fmt) AND c901_case_status = '107163' THEN 1 ELSE 0 END)AS NM_CANCELEDCASE,
         SUM(CASE WHEN  C7100_LOANER_FLAG = 'Y' AND C7100_SURGERY_DATE >= to_date(v_lm_first_day,p_date_fmt) AND t7100.c7100_surgery_date <= to_date(v_lm_last_day,p_date_fmt) AND c901_case_status != '107163' THEN 1 ELSE 0 END)AS LM_LOANER,
         SUM(CASE WHEN  C7100_LOANER_FLAG = 'Y' AND C7100_SURGERY_DATE > current_date AND  t7100.c7100_surgery_date <= to_date(v_tm_last_day,p_date_fmt) AND c901_case_status != '107163' THEN 1 ELSE 0 END)AS TM_LOANER,
         SUM(CASE WHEN  C7100_LOANER_FLAG = 'Y' AND C7100_SURGERY_DATE >= to_date(v_nm_first_day,p_date_fmt) AND  t7100.c7100_surgery_date <= to_date(v_nm_last_day,p_date_fmt) AND c901_case_status != '107163' THEN 1 ELSE 0 END)AS NM_LOANER,
         SUM(CASE WHEN C7100_LOANER_FLAG = 'N' AND C7100_SURGERY_DATE >= to_date(v_lm_first_day,p_date_fmt) AND t7100.c7100_surgery_date <= to_date(v_lm_last_day,p_date_fmt)  AND c901_case_status != '107163' THEN 1 ELSE 0 END)AS LM_NOLOANER,
         SUM(CASE WHEN C7100_LOANER_FLAG = 'N' AND C7100_SURGERY_DATE > current_date AND  t7100.c7100_surgery_date <= to_date(v_tm_last_day,p_date_fmt)  AND c901_case_status != '107163' THEN 1 ELSE 0 END)AS TM_NOLOANER,
         SUM(CASE WHEN C7100_LOANER_FLAG = 'N' AND C7100_SURGERY_DATE >= to_date(v_nm_first_day,p_date_fmt) AND  t7100.c7100_surgery_date <= to_date(v_nm_last_day,p_date_fmt)  AND c901_case_status != '107163' THEN 1 ELSE 0 END)AS NM_NOLOANER
    	FROM t7100_case_information T7100 WHERE c901_type  = '107287' AND  C7100_VOID_FL IS NULL AND C7100_OWNER_PARTY_ID = p_party_id )t;
 		    	        
   END gm_fch_case_dsh_data;
   
/* *******************************************************************************
 * Author: Parthiban
 * Procedure:gm_fch_case_shared_dtl
 * Description:Fetch case scheduler shared details based on party id
 ***********************************************************************************/ 
PROCEDURE gm_fch_case_shared_dtl(	
		p_party_id     	IN     T7100_CASE_INFORMATION.C7100_OWNER_PARTY_ID%TYPE,
		p_date_fmt      IN     VARCHAR2,
		p_out_cursor 	OUT    CLOB)
   AS 
		v_json_out JSON_OBJECT_T;
		v_lm_share  CLOB ;
		v_tm_share CLOB;
		v_nm_share CLOB;
		v_temp  CLOB;
		v_lm_first_day  VARCHAR2(50);
		v_lm_last_day   VARCHAR2(50);
		v_tm_first_day  VARCHAR2(50);
		v_tm_last_day   VARCHAR2(50);
		v_nm_first_day  VARCHAR2(50);
		v_nm_last_day   VARCHAR2(50);
		
   BEGIN
		
	   	SELECT JSON_OBJECT( 'date' VALUE current_date )
	   	  INTO v_temp
		  FROM dual ;
	   
	   
		SELECT to_char(Last_Day(ADD_MONTHS(current_date,-2))+1,p_date_fmt), to_char(Last_Day(ADD_MONTHS(current_date,-1)),p_date_fmt), 
	  	       to_char(Last_Day(ADD_MONTHS(current_date,-1))+1,p_date_fmt), to_char(Last_Day(ADD_MONTHS(current_date,+0)),p_date_fmt), 
	  		   to_char(Last_Day(ADD_MONTHS(current_date,+0))+1,p_date_fmt), to_char(Last_Day(ADD_MONTHS(current_date,+1)),p_date_fmt) 
	  	  INTO v_lm_first_day,v_lm_last_day,v_tm_first_day,v_tm_last_day,v_nm_first_day,v_nm_last_day FROM dual;
			
	    SELECT JSON_ARRAYAGG(JSON_OBJECT( 'caseid' VALUE T7100.C7100_CASE_INFO_ID,
		      	'surgerydt' VALUE TO_CHAR(T7100.C7100_SURGERY_DATE,p_date_fmt),
           		'surgerylevel' VALUE GET_CODE_NAME(C901_SURGERY_LEVEL),
           		'surgerytype' VALUE GET_CODE_NAME(C901_SURGERY_TYPE),
           		'repname' VALUE GET_REP_NAME(C703_SALES_REP_ID),
           		'accname' VALUE GET_ACCOUNT_NAME(C704_ACCOUNT_ID),
		       	'surgerytime' VALUE TO_CHAR (T7100.C7100_SURGERY_TIME,'HH12:MI AM'),
		      	'surgeryendtime' VALUE TO_CHAR ( T7100.C7100_SURGERY_END_TIME,'HH12:MI AM'),
           		'surgeonnm' VALUE GET_SURG_NAME(T7100.C7100_CASE_INFO_ID),
           		'casedotype' VALUE GET_DO_STATUS (T7100.C7100_CASE_INFO_ID),
          		'casetype' VALUE GET_CASE_STATUS (T7100.C7100_CASE_INFO_ID),
           		'month' VALUE 'tm'
		        ) ORDER BY T7100.C7100_SURGERY_DATE,T7100.C7100_SURGERY_TIME, T7100.C7100_SURGERY_END_TIME DESC RETURNING CLOB) INTO v_lm_share
		  FROM t7100_case_information t7100
		 WHERE t7100.c7100_surgery_date >= to_date(v_lm_first_day,p_date_fmt) 
		   AND t7100.c7100_surgery_date <= to_date(v_lm_last_day,p_date_fmt)
		   AND C7100_CASE_INFO_ID IN ( 
		SELECT DISTINCT C7100_CASE_INFO_ID FROM t7107_case_share WHERE  C7107_REP_PARTY_ID = p_party_id AND C7107_VOID_FL IS null)
		   AND  c901_type  = '107287'
		   AND C7100_VOID_FL is null;
		   			
					 
 		SELECT JSON_ARRAYAGG(JSON_OBJECT( 'caseid' VALUE T7100.C7100_CASE_INFO_ID,
		      'surgerydt' VALUE TO_CHAR(T7100.C7100_SURGERY_DATE,p_date_fmt),
              'surgerylevel' VALUE GET_CODE_NAME(C901_SURGERY_LEVEL),
              'surgerytype' VALUE GET_CODE_NAME(C901_SURGERY_TYPE),
              'repname' VALUE GET_REP_NAME(C703_SALES_REP_ID),
              'accname' VALUE GET_ACCOUNT_NAME(C704_ACCOUNT_ID),
              'surgerytime' VALUE TO_CHAR (T7100.C7100_SURGERY_TIME,'HH12:MI AM'),
              'surgeryendtime' VALUE TO_CHAR (T7100.C7100_SURGERY_END_TIME,'HH12:MI AM'),
              'surgeonnm' VALUE GET_SURG_NAME(T7100.C7100_CASE_INFO_ID),
              'casedotype' VALUE GET_DO_STATUS (T7100.C7100_CASE_INFO_ID),
              'casetype' VALUE GET_CASE_STATUS (T7100.C7100_CASE_INFO_ID),
              'month' VALUE 'tm'
		        )ORDER BY T7100.C7100_SURGERY_DATE, T7100.C7100_SURGERY_TIME, T7100.C7100_SURGERY_END_TIME DESC RETURNING CLOB) INTO v_tm_share
		  FROM t7100_case_information t7100
		 WHERE t7100.c7100_surgery_date >= to_date(v_tm_first_day,p_date_fmt) 
		   AND  t7100.c7100_surgery_date <= to_date(v_tm_last_day,p_date_fmt)
		   AND C7100_CASE_INFO_ID IN ( 
		SELECT DISTINCT C7100_CASE_INFO_ID FROM t7107_case_share WHERE  C7107_REP_PARTY_ID = p_party_id AND C7107_VOID_FL IS null)
		   AND   c901_type  = '107287'
		   AND  C7100_VOID_FL is null;
		
			
	   SELECT JSON_ARRAYAGG(JSON_OBJECT( 'caseid' VALUE T7100.C7100_CASE_INFO_ID,
	      	  'surgerydt' VALUE TO_CHAR(T7100.C7100_SURGERY_DATE,p_date_fmt),
              'surgerylevel' VALUE GET_CODE_NAME(C901_SURGERY_LEVEL),
              'surgerytype' VALUE GET_CODE_NAME(C901_SURGERY_TYPE),
              'repname' VALUE GET_REP_NAME(C703_SALES_REP_ID),
              'accname' VALUE GET_ACCOUNT_NAME(C704_ACCOUNT_ID),
              'surgerytime' VALUE TO_CHAR (T7100.C7100_SURGERY_TIME,'HH12:MI AM'),
              'surgeryendtime' VALUE TO_CHAR (T7100.C7100_SURGERY_END_TIME,'HH12:MI AM'),
              'surgeonnm' VALUE GET_SURG_NAME(T7100.C7100_CASE_INFO_ID),
              'casedotype' VALUE GET_DO_STATUS (T7100.C7100_CASE_INFO_ID),
              'casetype' VALUE GET_CASE_STATUS (T7100.C7100_CASE_INFO_ID),
              'month' VALUE 'nm'
		        ) ORDER BY T7100.C7100_SURGERY_DATE,T7100.C7100_SURGERY_TIME, T7100.C7100_SURGERY_END_TIME DESC RETURNING CLOB) INTO v_nm_share
		  FROM t7100_case_information t7100
	     WHERE t7100.c7100_surgery_date >= to_date(v_nm_first_day,p_date_fmt) 
	       AND  t7100.c7100_surgery_date <= to_date(v_nm_last_day,p_date_fmt)
	       AND C7100_CASE_INFO_ID IN ( 
	    SELECT DISTINCT C7100_CASE_INFO_ID FROM t7107_case_share WHERE  C7107_REP_PARTY_ID = p_party_id AND C7107_VOID_FL IS null)
		   AND   c901_type  = '107287'
		   AND  C7100_VOID_FL is null;
		  
			   
		    v_json_out := JSON_OBJECT_T.parse(v_temp);
								
			IF v_lm_share !=  empty_clob()  THEN
				v_json_out.put('arrlmshare',JSON_ARRAY_T(v_lm_share));		
			END IF;
	
			IF v_tm_share !=  empty_clob()  THEN
				v_json_out.put('arrtmshare',JSON_ARRAY_T(v_tm_share));
			END IF;

			IF v_nm_share !=  empty_clob()  THEN
				v_json_out.put('arrnmshare',JSON_ARRAY_T(v_nm_share));
			END IF	;
			 
			p_out_cursor := v_json_out.to_string;
		
		
 END gm_fch_case_shared_dtl;
 
/* *******************************************************************************
 * Author: Mahendrand
 * Procedure:gm_fch_case_dtl
 * Description:Fetch Case details for summary page
 ***********************************************************************************/  	  
 PROCEDURE  gm_fch_case_dtl (
    p_case_infoid 		IN     T7100_CASE_INFORMATION.C7100_CASE_INFO_ID%type,
    p_party_id          IN     T7100_CASE_INFORMATION.C7100_OWNER_PARTY_ID%TYPE,
    p_date_fmt    		IN     VARCHAR2,
    p_out_cursor 		OUT    CLOB)
  AS
   	v_json_out     JSON_OBJECT_T;
   	v_casedtl      VARCHAR2(4000);
 	v_casepartydtl VARCHAR2(4000);
 	v_casesetdtl   VARCHAR2(4000);
 	v_casepartdtl  VARCHAR2(4000);
 	v_casesharedtl VARCHAR2(4000);
  BEGIN
	   --Fetch Kit Details
	  
	   SELECT JSON_OBJECT(
	   				'caseinfoid'      VALUE t7100.C7100_CASE_INFO_ID,
  				   	'caseid'          VALUE t7100.C7100_CASE_ID,
  				   	'surgdt'          VALUE TO_CHAR(t7100.C7100_SURGERY_DATE,p_date_fmt),
  				   	'starttm'         VALUE TO_CHAR(t7100.C7100_SURGERY_TIME,'HH24:MI:SS'),
  				   	'endtm'           VALUE TO_CHAR(t7100.C7100_SURGERY_END_TIME,'HH24:MI:SS'),
  				   	'accid'           VALUE t7100.C704_ACCOUNT_ID,
  				   	'accnm'           VALUE get_account_name(t7100.C704_ACCOUNT_ID),
  				   	'surgtypeid'      VALUE t7100.C901_SURGERY_TYPE,
  				   	'surgtype'        VALUE get_code_name(t7100.C901_SURGERY_TYPE),
  				   	'surglvl'         VALUE get_code_name(t7100.C901_SURGERY_LEVEL),
  				   	'surglvlid'       VALUE t7100.C901_SURGERY_LEVEL,
  				   	'status'          VALUE get_code_name(t7100.C901_CASE_STATUS),
  				   	'statusid'        VALUE t7100.C901_CASE_STATUS,
  				   	'notes'           VALUE t7100.C7100_PERSONAL_NOTES,
  				   	'createddt'       VALUE TO_CHAR(t7100.C7100_CREATED_DATE,p_date_fmt),
  				   	'createdby'       VALUE get_user_name(t7100.C7100_CREATED_BY),
  				   	'updateddt'       VALUE TO_CHAR(t7100.C7100_LAST_UPDATED_DATE, p_date_fmt),
  				   	'updatedby'       VALUE get_user_name(t7100.C7100_LAST_UPDATED_BY),
 					'sharenm'         VALUE Decode(p_party_id,t7100.C7100_OWNER_PARTY_ID,'',get_party_name(t7100.C7100_OWNER_PARTY_ID))			   	
  			   )INTO v_casedtl 
	     FROM T7100_CASE_INFORMATION t7100
	    WHERE T7100.C7100_CASE_INFO_ID = p_case_infoid
	      AND T7100.C901_TYPE = 107287  --Case Schedular IPAD
	      AND T7100.C7100_VOID_FL IS NULL;
	      
	    --Fetch Set Details based on Case
	    SELECT JSON_ARRAYAGG (JSON_OBJECT( 'setid' VALUE T7104.C207_SET_ID,
                    'setdesc'      VALUE get_set_name(T7104.C207_SET_ID), 
                    'tagid'        VALUE T7104.C5010_TAG_ID,
                    'kitid'        VALUE T7104.C2078_KIT_MAP_ID, 
                    'kitnm'        VALUE gm_pkg_case_rpt.GET_KIT_NAME(T7104.C2078_KIT_MAP_ID),--T2078.c2078_kit_nm
                    'caseinfoid'   VALUE T7104.C7100_CASE_INFO_ID,
                    'voidfl'       VALUE T7104.C7104_VOID_FL)ORDER BY T7104.C207_SET_ID ASC RETURNING CLOB) INTO V_CASESETDTL
         FROM T7104_CASE_SET_INFORMATION T7104
        WHERE T7104.C7100_CASE_INFO_ID = p_case_infoid
          AND T7104.C7104_VOID_FL     IS NULL
          AND C207_SET_ID IS NOT NULL;
                                
	 
	   --Fetch part Details based on Case
	    SELECT JSON_ARRAYAGG(JSON_OBJECT(
	   				'partnum'      VALUE T7104.C205_PART_NUMBER_ID,
  				  	'partdesc'     VALUE gm_pkg_kit_mapping_rpt.GETPARTDESC(t205.c205_part_num_desc),  --REGEXP_REPLACE(t205.c205_part_num_desc,'[^ ,0-9A-Za-z]', ''),
  				   	'qty'          VALUE T7104.C7104_ITEM_QTY,
  				   	'kitid'        VALUE T7104.C2078_KIT_MAP_ID,
				    'kitnm'        VALUE gm_pkg_case_rpt.GET_KIT_NAME(T7104.C2078_KIT_MAP_ID),--T2078.c2078_kit_nm 
				    'caseinfoid'   VALUE T7104.C7100_CASE_INFO_ID,
  				   	'voidfl'       VALUE T7104.C7104_VOID_FL
  			   )order by T7104.C205_PART_NUMBER_ID RETURNING CLOB) INTO v_casepartdtl 
	     FROM T7104_CASE_SET_INFORMATION T7104,T205_PART_NUMBER T205
	    WHERE T7104.C7100_CASE_INFO_ID  = p_case_infoid
	      AND T7104.C205_PART_NUMBER_ID = T205.C205_PART_NUMBER_ID
	      AND T7104.C7104_VOID_FL IS NULL
	      AND T205.C205_ACTIVE_FL = 'Y'; 
	      	      
	    --Fetch Surgeon list based on Case
	      SELECT JSON_ARRAYAGG(JSON_OBJECT(
	   				'partyid'       VALUE T7106.C7106_SURGEON_PARTY_ID,
	   				'partynm'       VALUE T7106.C7106_SURGEON_NAME,	   				
	   				'npiid'         VALUE T7106.C6600_SURGEON_NPI_ID, 
  				   	'caseinfoid'    VALUE T7106.C7100_CASE_INFO_ID,
  				   	'voidfl'        VALUE T7106.C7106_VOID_FL 
  			   )order by  T7106.C7106_SURGEON_NAME RETURNING CLOB) INTO v_casepartydtl 
	     FROM T7106_CASE_SURGEON_MAP T7106
	    WHERE T7106.C7100_CASE_INFO_ID  = p_case_infoid
	      AND T7106.C7106_VOID_FL IS NULL;
	      
	      
	      --Fetch rep list based on shared by Case
	    SELECT JSON_ARRAYAGG(JSON_OBJECT(
	   				'repid'        VALUE T7107.C7107_REP_PARTY_ID,
	   				'repnm'        VALUE get_party_name(T7107.C7107_REP_PARTY_ID),
  				   	'caseinfoid'   VALUE T7107.C7100_CASE_INFO_ID,
  				   	'voidfl'       VALUE T7107.C7107_VOID_FL 
  			   )order by  get_rep_name(T7107.C7107_REP_PARTY_ID) RETURNING CLOB) INTO v_casesharedtl 
	      FROM T7107_CASE_SHARE T7107
	     WHERE T7107.C7100_CASE_INFO_ID  = p_case_infoid
	       AND T7107.C7107_VOID_FL IS NULL;
	
	    		v_json_out := JSON_OBJECT_T.parse(v_casedtl);
	  
			IF v_casesetdtl IS NOT NULL  THEN
				v_json_out.put('arrcasesetvo',JSON_ARRAY_T(v_casesetdtl));
			END IF;
			
			IF v_casepartdtl IS NOT NULL  THEN
				v_json_out.put('arrcasepartvo',JSON_ARRAY_T(v_casepartdtl));
			END IF;
			  
			IF v_casepartydtl IS NOT NULL  THEN
				v_json_out.put('arrcasepartyvo',JSON_ARRAY_T(v_casepartydtl));
			END IF;
			
			IF v_casesharedtl IS NOT NULL THEN
				v_json_out.put('arrcasesharevo',JSON_ARRAY_T(v_casesharedtl));
			END IF;
	
				p_out_cursor := v_json_out.to_string;
	      
 END gm_fch_case_dtl;
 
 /* *******************************************************************************
 * Author: Parthiban
 * Procedure:gm_fch_case_calendar_data
 * Description:Fetch Calendar Data for Calendar
 ***********************************************************************************/  	  
 PROCEDURE  gm_fch_case_calendar_data (
    p_party_id     	IN     T7100_CASE_INFORMATION.C7100_OWNER_PARTY_ID%TYPE,
    p_date_fmt      IN     VARCHAR2,
    p_out_cursor 		OUT    CLOB)
  AS
  		v_json_out JSON_OBJECT_T;
		v_cal_data  CLOB;
		v_from_day  VARCHAR2(50);
		v_to_day   VARCHAR2(50);
		v_tm_first_day VARCHAR2(50);
		v_tm_last_day VARCHAR2(50);
		v_temp CLOB;
		
		
   BEGIN
	   
	   
	   
	   	SELECT JSON_OBJECT( 'date' VALUE current_date )
	   	  INTO v_temp
		  FROM dual;
	   
	   SELECT to_char(Last_Day(ADD_MONTHS(current_date,-2))+1,p_date_fmt),
              to_char(Last_Day(ADD_MONTHS(current_date,+4)),p_date_fmt),
              to_char(Last_Day(ADD_MONTHS(current_date,-1))+1,p_date_fmt),
              to_char(Last_Day(ADD_MONTHS(current_date,+0)),p_date_fmt)
         INTO v_from_day, v_to_day, v_tm_first_day, v_tm_last_day 
	     FROM dual;
	  		 
	 
	   SELECT JSON_ARRAYAGG(JSON_OBJECT( 
			    'caseinfoid' VALUE  caseinfoid,
                'caseid' VALUE caseid ,
			    'accname' VALUE accname,
			    'surgeonnm' VALUE surgeonnm,
			    'surglevel' VALUE surglevel,
			    'surgerydt' VALUE surgerydt,
			    'surgerytime' VALUE surgerytime,
			    'surgeryendtime' VALUE surgeryendtime,
			    'dobooked' VALUE dobooked,
			    'pendingdo' VALUE pendingdo,
			    'loaner' VALUE loaner,
			    'noloaner' VALUE noloaner,
			    'cancelcase' VALUE cancelcase)
	  ORDER BY  surgerydt, surgerytime, surgeryendtime DESC
                RETURNING CLOB) INTO v_cal_data 
          FROM( 
		SELECT T7100.C7100_CASE_INFO_ID caseinfoid,
                  T7100.c7100_case_id caseid ,
			      GET_ACCOUNT_NAME(T7100.C704_ACCOUNT_ID) accname,
                  GM_PKG_CASE_RPT. GET_SURG_NAME(T7100.C7100_CASE_INFO_ID) surgeonnm,
                  GET_CODE_NAME(T7100.C901_SURGERY_LEVEL) surglevel,
                  TO_CHAR(T7100.C7100_SURGERY_DATE,'dd/mm/yyyy') surgerydt,
                  TO_CHAR (T7100.C7100_SURGERY_TIME,'HH12:MI AM') surgerytime,
                  TO_CHAR (T7100.C7100_SURGERY_END_TIME,'HH12:MI AM') surgeryendtime,
                  sum(CASE WHEN T7100.c7100_do_flag = 'Y' AND T7100.c901_case_status != '107163' THEN 1 ELSE 0 END) dobooked,
                  sum(CASE WHEN T7100.c7100_do_flag = 'N'AND T7100.c901_case_status != '107163' THEN 1 ELSE 0 END) pendingdo,
                  sum(CASE WHEN T7100.c7100_loaner_flag = 'Y' AND T7100.c901_case_status != '107163' THEN 1 ELSE 0 END) loaner,
                  sum(CASE WHEN T7100.c7100_loaner_flag = 'N'  AND T7100.c901_case_status != '107163' THEN 1 ELSE 0 END) noloaner,
                  sum(CASE WHEN  T7100.c901_case_status = '107163'  THEN 1 ELSE 0 END) cancelcase          
		  FROM t7100_case_information T7100
	     WHERE T7100.c901_type = '107287' 
	       AND T7100.c7100_void_fl IS NULL  
	       AND trunc(T7100.c7100_surgery_date) BETWEEN to_date(v_from_day,p_date_fmt) 
	       AND to_date(v_to_day,p_date_fmt)
	       AND (T7100.c7100_owner_party_id = p_party_id 
	        OR T7100.C7100_CASE_INFO_ID IN (SELECT C7100_CASE_INFO_ID FROM T7107_CASE_SHARE WHERE C7107_REP_PARTY_ID = p_party_id AND C7107_VOID_FL IS NULL))
	     GROUP BY T7100.c7100_case_info_id ,T7100.c7100_case_id, T7100.C704_ACCOUNT_ID,
	    	   T7100.C901_SURGERY_LEVEL, T7100.c7100_surgery_date,T7100.C7100_SURGERY_TIME,T7100.C7100_SURGERY_END_TIME);
--	 UNION ALL
--	    SELECT T7100.C7100_CASE_INFO_ID caseinfoid, T7100.c7100_case_id caseid ,
--			      GET_ACCOUNT_NAME(T7100.C704_ACCOUNT_ID) accname,
--                  GM_PKG_CASE_RPT. GET_SURG_NAME(T7100.C7100_CASE_INFO_ID) surgeonnm,
--                  GET_CODE_NAME(T7100.C901_SURGERY_LEVEL) surglevel,
--                  TO_CHAR(T7100.C7100_SURGERY_DATE,'dd/mm/yyyy') surgerydt,
--                  TO_CHAR (T7100.C7100_SURGERY_TIME,'HH12:MI AM') surgerytime,
--                  TO_CHAR (T7100.C7100_SURGERY_END_TIME,'HH12:MI AM') surgeryendtime,
--                  0 dobooked,0 pendingdo,0 loaner,1 noloaner, 0 cancelcase
--          FROM t7100_case_information T7100 WHERE T7100.c901_type = '107287' AND T7100.c901_case_status != '107163' 
--           AND T7100.c7100_void_fl IS NULL
--           AND trunc(T7100.c7100_surgery_date) BETWEEN to_date(v_from_day,p_date_fmt) 
--           AND to_date(v_to_day,p_date_fmt) 
--           AND trunc(T7100.c7100_surgery_date) NOT BETWEEN to_date(v_tm_first_day,p_date_fmt)
--	       AND to_date(v_tm_last_day,p_date_fmt)
--           AND (T7100.c7100_owner_party_id = p_party_id 
--	        OR T7100.C7100_CASE_INFO_ID IN (SELECT C7100_CASE_INFO_ID FROM T7107_CASE_SHARE WHERE C7107_REP_PARTY_ID = p_party_id AND C7107_VOID_FL IS NULL)));
	     
	      		 v_json_out := JSON_OBJECT_T.parse(v_temp);
									
				IF v_cal_data !=  empty_clob() THEN
					v_json_out.put('arrcaldata',JSON_ARRAY_T(v_cal_data));		
				END IF;
					p_out_cursor := v_json_out.to_string;
 
  END gm_fch_case_calendar_data;
  
  /* *******************************************************************************
 * Author: Parthiban
 * Procedure:gm_fch_case_dashborad
 * Description:Fetch case dashboard details based on party id
 ***********************************************************************************/ 
PROCEDURE gm_fch_case_dashborad(	
		p_party_id     	IN     T7100_CASE_INFORMATION.C7100_OWNER_PARTY_ID%TYPE,
		p_date_fmt      IN     VARCHAR2,
		p_out_cursor 	OUT    CLOB)
   AS 
		v_json_out JSON_OBJECT_T;
		v_lm_chart  CLOB;
		v_tm_chart CLOB;
		v_nm_chart CLOB;
		v_temp  CLOB;
		v_lm_first_day  VARCHAR2(50);
		v_lm_last_day   VARCHAR2(50);
		v_tm_first_day  VARCHAR2(50);
		v_tm_last_day   VARCHAR2(50);
		v_nm_first_day  VARCHAR2(50);
		v_nm_last_day   VARCHAR2(50);
		
   BEGIN
		
	   	SELECT JSON_OBJECT( 'date' VALUE current_date )
	   	  INTO v_temp
		  FROM dual ;
	   
		SELECT to_char(Last_Day(ADD_MONTHS(current_date,-2))+1,p_date_fmt), to_char(Last_Day(ADD_MONTHS(current_date,-1)),p_date_fmt), 
	  	       to_char(Last_Day(ADD_MONTHS(current_date,-1))+1,p_date_fmt), to_char(Last_Day(ADD_MONTHS(current_date,+0)),p_date_fmt), 
	  		   to_char(Last_Day(ADD_MONTHS(current_date,+0))+1,p_date_fmt), to_char(Last_Day(ADD_MONTHS(current_date,+1)),p_date_fmt) 
	  	  INTO v_lm_first_day,v_lm_last_day,v_tm_first_day,v_tm_last_day,v_nm_first_day,v_nm_last_day FROM dual;
	  	  
	  	  
	    SELECT JSON_ARRAYAGG(JSON_OBJECT( 'caseid' VALUE T7100.C7100_CASE_INFO_ID,
	      	  'surgerydt' VALUE TO_CHAR(T7100.C7100_SURGERY_DATE,p_date_fmt),
              'surgerylevel' VALUE GET_CODE_NAME(C901_SURGERY_LEVEL),
              'surgerytype' VALUE GET_CODE_NAME(C901_SURGERY_TYPE),
              'repname' VALUE GET_REP_NAME(C703_SALES_REP_ID),
              'surgeonnm' VALUE GET_SURG_NAME(T7100.C7100_CASE_INFO_ID),
              'accname' VALUE GET_ACCOUNT_NAME(C704_ACCOUNT_ID),
              'surgerytime' VALUE TO_CHAR (T7100.C7100_SURGERY_TIME,'HH12:MI AM'),
              'surgeryendtime' VALUE TO_CHAR (T7100.C7100_SURGERY_END_TIME,'HH12:MI AM'),
              'casedotype' VALUE GET_DO_STATUS (T7100.C7100_CASE_INFO_ID),
              'casetype' VALUE GET_CASE_STATUS (T7100.C7100_CASE_INFO_ID),
              'month' VALUE 'lm'
		        )ORDER BY  T7100.C7100_SURGERY_DATE, T7100.C7100_SURGERY_TIME, T7100.C7100_SURGERY_END_TIME DESC RETURNING CLOB) INTO v_lm_chart
		  FROM t7100_case_information t7100
		 WHERE  t7100.c7100_surgery_date >= to_date(v_lm_first_day,p_date_fmt) 
		   AND  t7100.c7100_surgery_date <= to_date(v_lm_last_day,p_date_fmt)
		   AND  C7100_VOID_FL is null
		   AND  C7100_OWNER_PARTY_ID = p_party_id;
		   
		   	    
		    
		SELECT JSON_ARRAYAGG(JSON_OBJECT( 'caseid' VALUE T7100.C7100_CASE_INFO_ID,
	      	  'surgerydt' VALUE TO_CHAR(T7100.C7100_SURGERY_DATE,p_date_fmt),
              'surgerylevel' VALUE GET_CODE_NAME(C901_SURGERY_LEVEL),
              'surgerytype' VALUE GET_CODE_NAME(C901_SURGERY_TYPE),
              'repname' VALUE GET_REP_NAME(C703_SALES_REP_ID),
              'surgeonnm' VALUE GET_SURG_NAME(T7100.C7100_CASE_INFO_ID),
              'accname' VALUE GET_ACCOUNT_NAME(C704_ACCOUNT_ID),
              'surgerytime' VALUE TO_CHAR (T7100.C7100_SURGERY_TIME,'HH12:MI AM'),
              'surgeryendtime' VALUE TO_CHAR (T7100.C7100_SURGERY_END_TIME,'HH12:MI AM'),
              'casedotype' VALUE GET_DO_STATUS (T7100.C7100_CASE_INFO_ID),
              'casetype' VALUE GET_CASE_STATUS (T7100.C7100_CASE_INFO_ID),
              'tmstatus' VALUE case WHEN c7100_surgery_date <= CURRENT_DATE AND c7100_do_flag = 'Y' AND  c901_case_status != '107163' THEN 'DOBOOKED'
                                  WHEN c7100_surgery_date <= CURRENT_DATE AND c7100_do_flag = 'N' AND  c901_case_status != '107163' THEN 'PENDINGDO' 
                                  WHEN c901_case_status = '107163' THEN 'CASECL'
                                  WHEN c7100_surgery_date > CURRENT_DATE AND c7100_loaner_flag = 'Y' AND  c901_case_status != '107163' THEN 'LOANER'
                                  WHEN c7100_surgery_date > CURRENT_DATE AND c7100_loaner_flag = 'N' AND  c901_case_status != '107163' THEN 'NOLOANER'
                                  ELSE ' '
                                  END
		        )ORDER BY  T7100.C7100_SURGERY_DATE, T7100.C7100_SURGERY_TIME, T7100.C7100_SURGERY_END_TIME DESC RETURNING CLOB) INTO v_tm_chart
		  FROM t7100_case_information t7100
		 WHERE  t7100.c7100_surgery_date >= to_date(v_tm_first_day,p_date_fmt) 
		   AND  t7100.c7100_surgery_date <= to_date(v_tm_last_day,p_date_fmt)
		   AND  C7100_VOID_FL is null
		   AND  C7100_OWNER_PARTY_ID = p_party_id;

		SELECT JSON_ARRAYAGG(JSON_OBJECT( 'caseid' VALUE T7100.C7100_CASE_INFO_ID,
	      	  'surgerydt' VALUE TO_CHAR(T7100.C7100_SURGERY_DATE,p_date_fmt),
              'surgerylevel' VALUE GET_CODE_NAME(C901_SURGERY_LEVEL),
              'surgerytype' VALUE GET_CODE_NAME(C901_SURGERY_TYPE),
              'repname' VALUE GET_REP_NAME(C703_SALES_REP_ID),
              'surgeonnm' VALUE GET_SURG_NAME(T7100.C7100_CASE_INFO_ID),
              'accname' VALUE GET_ACCOUNT_NAME(C704_ACCOUNT_ID),
              'surgerytime' VALUE TO_CHAR (T7100.C7100_SURGERY_TIME,'HH12:MI AM'),
              'surgeryendtime' VALUE TO_CHAR (T7100.C7100_SURGERY_END_TIME,'HH12:MI AM'),
              'casedotype' VALUE GET_DO_STATUS (T7100.C7100_CASE_INFO_ID),
              'casetype' VALUE GET_CASE_STATUS (T7100.C7100_CASE_INFO_ID),
              'month' VALUE 'nm'
		        )ORDER BY  T7100.C7100_SURGERY_DATE, T7100.C7100_SURGERY_TIME, T7100.C7100_SURGERY_END_TIME DESC RETURNING CLOB) INTO v_nm_chart
		  FROM t7100_case_information t7100
		 WHERE  t7100.c7100_surgery_date >= to_date(v_nm_first_day,p_date_fmt) 
		   AND  t7100.c7100_surgery_date <= to_date(v_nm_last_day,p_date_fmt)
		   AND  C7100_VOID_FL is null
		   AND  C7100_OWNER_PARTY_ID = p_party_id;

		    v_json_out := JSON_OBJECT_T.parse(v_temp);
								
			IF v_lm_chart !=  empty_clob() THEN
				v_json_out.put('arrlmchart',JSON_ARRAY_T(v_lm_chart));		
			END IF;
	
			IF v_tm_chart !=  empty_clob() THEN
				v_json_out.put('arrtmchart',JSON_ARRAY_T(v_tm_chart));
			END IF;

			IF v_nm_chart !=  empty_clob() THEN
				v_json_out.put('arrnmchart',JSON_ARRAY_T(v_nm_chart));
			END IF	;
			 
			p_out_cursor := v_json_out.to_string;
  
  END gm_fch_case_dashborad;

	/*******************************************************
   * Description : Function to fetch DO  STATUS GIVEN THE CODE_ID
   * Author      : Pdhanasekaran 
*******************************************************/
	FUNCTION GET_DO_STATUS
		(p_case_id  T7100_CASE_INFORMATION.C7100_CASE_INFO_ID%TYPE)
		 RETURN VARCHAR2
		IS
		v_count   NUMBER(10);
		v_result   VARCHAR2(100);
		 
		BEGIN
		  BEGIN
			SELECT count(*) INTO v_count 
		 	  FROM T7100_CASE_INFORMATION WHERE C7100_DO_FLAG = 'N' AND C901_CASE_STATUS != '107163' 
		 	   AND C901_TYPE  = '107287' and C7100_CASE_INFO_ID = p_case_id;
		 		
		 	    IF (v_count != 0) THEN   
		 		v_result:='PENDINGDO';
		 		END IF;
		 		 
		 	EXCEPTION
		      WHEN NO_DATA_FOUND
		    THEN
		      RETURN ' ';
		      END;
		     RETURN v_result;
		END GET_DO_STATUS;
		
		
	/*******************************************************
   * Description : Function to fetch CODE CASE STATUS GIVEN THE CODE_ID
   * Author      : Pdhanasekaran 
*******************************************************/
   FUNCTION get_case_status (
	 p_case_id	IN	 t7100_case_information.c7100_case_info_id%TYPE)
	 RETURN VARCHAR2
	IS
	v_count   NUMBER;
	BEGIN
		 BEGIN
 		SELECT count(*) INTO v_count   --DO BOOKED
 		FROM T7100_CASE_INFORMATION WHERE C7100_DO_FLAG = 'Y' 
 		AND C901_CASE_STATUS != '107163' AND C901_TYPE  = '107287' --Case Schedular IPAD
 		AND C7100_VOID_FL IS NULL
 		AND C7100_CASE_INFO_ID = p_case_id; 
 		
 		 IF (v_count != 0) THEN   		 		
 		 		
 		    return 'DOBOOKED';
 		    
 		ELSIF v_count = 0 THEN
 			SELECT count(*) INTO v_count   --CANCELED CASE
 			FROM T7100_CASE_INFORMATION WHERE  c901_type = '107287'
 			AND c901_case_status = '107163' --Canceled code
 			AND C7100_VOID_FL IS NULL
 			AND C7100_CASE_INFO_ID = p_case_id;
 		
 		IF (v_count != 0) THEN 
 			return 'CASECL';
 			END IF;
 
 			SELECT count(*) INTO v_count   --LOANER
 			FROM T7100_CASE_INFORMATION WHERE C7100_LOANER_FLAG = 'Y' 
 			AND c901_case_status != '107163' 
 			AND C7100_VOID_FL IS NULL
 			AND c901_type  = '107287' 
 			AND C7100_CASE_INFO_ID = p_case_id;
 			
 		IF (v_count != 0) THEN
 			return 'LOANER';
 			END IF;

 			SELECT count(*) INTO v_count   --NO LOANER
 			FROM T7100_CASE_INFORMATION WHERE C7100_LOANER_FLAG = 'N' 
 			AND  c901_case_status != '107163' 
 			AND C7100_VOID_FL IS NULL
 			AND c901_type  = '107287' 
 			AND C7100_CASE_INFO_ID = p_case_id;
 		
 		IF (v_count != 0) THEN
 			return 'NOLOANER';
 		    END IF;
 		    END IF;
 	
		END;
		RETURN v_count;
	END get_case_status;
	
/******************************************************************************************
       * Description :  This Function returns the kit name based on kit id.
*******************************************************************************************/
   FUNCTION GET_KIT_NAME
	 (p_kit_id           T2078_KIT_MAPPING.C2078_KIT_MAP_ID%TYPE)
	 RETURN VARCHAR2
	 IS
	
	       v_kit_nm     T2078_KIT_MAPPING.C2078_KIT_NM%type;
	 BEGIN
	        BEGIN
	        SELECT C2078_KIT_NM 
	        INTO v_kit_nm 
	        FROM T2078_KIT_MAPPING 
	        WHERE C2078_KIT_MAP_ID = p_kit_id 
	        AND C2078_VOID_FL IS NULL;
	          EXCEPTION
	          WHEN NO_DATA_FOUND
	        THEN
	         RETURN ' ';
	        END;
			 RETURN v_kit_nm;
	 END GET_KIT_NAME;
	
/******************************************************************************************
       * Description :  This Function returns the surgeon name based on case info id
*******************************************************************************************/

	 FUNCTION GET_SURG_NAME
		   (p_case_info_id IN varchar2 )
		   RETURN varchar2
		IS
		   v_surg_nm varchar2(4000) := null;
		
		   CURSOR surgeoncur
		   IS
		     SELECT C7106_SURGEON_NAME surgname from T7106_CASE_SURGEON_MAP where C7100_CASE_INFO_ID = p_case_info_id AND c7106_void_fl IS null;
		
		BEGIN
		    FOR surgeonnm IN surgeoncur 
		    LOOP 
		    
		        v_surg_nm :=  surgeonnm.surgname || ',' || v_surg_nm ;
		           
		  END LOOP; 
		    v_surg_nm := substr(v_surg_nm,0,length(v_surg_nm)-1);
		RETURN v_surg_nm;
		
		END GET_SURG_NAME;
		
 /* *******************************************************************************
 * Author:Agilan 
 * Procedure:gm_fch_case_book_dtl
 * Description:Fetch Case details for event notification
 ***********************************************************************************/  	  
 PROCEDURE  gm_fch_case_book_dtl (
    p_case_infoid 		IN     T7100_CASE_INFORMATION.C7100_CASE_INFO_ID%type,
    p_ref_id            IN	   T7100_CASE_INFORMATION.C703_SALES_REP_ID%type,
    p_date_fmt          IN     VARCHAR2,
    p_out_cursor 		OUT    TYPES.cursor_type)
  AS
  
  BEGIN
	  
	  OPEN p_out_cursor
	  FOR
	  	 --The purpose of date hardcoded is of getting GMT format to send mail request notification
		  SELECT TO_CHAR(T7100.C7100_SURGERY_DATE,'YYYY/MM/DD') SDATE, 
	  		     TO_CHAR(CAST(T7100.C7100_SURGERY_TIME AT TIME ZONE 'GMT' AS TIMESTAMP WITH TIME ZONE),'HH24:MI:SS') SRTTM,
	  		     TO_CHAR(CAST(T7100.C7100_SURGERY_END_TIME AT TIME ZONE 'GMT' AS TIMESTAMP WITH TIME ZONE),'HH24:MI:SS') ENDTM,
	  		 	 GET_ACCOUNT_NAME(T7100.C704_ACCOUNT_ID) LOC,
	  		 	 T7100.C704_ACCOUNT_ID ACCID,
         	     T7106.C7106_SURGEON_NAME SURNM,
         	     T7100.C7100_EVENT_ITEM_ID EVTITEMID,
         	     GET_REP_EMAILID(p_ref_id) REFEMAILID,
         	     GET_PARTY_NAME(T7100.C7100_OWNER_PARTY_ID) REPNAME
	  	    FROM T7100_CASE_INFORMATION  T7100, T7106_CASE_SURGEON_MAP T7106
	       WHERE T7100.C7100_CASE_INFO_ID = p_case_infoid --'51571' 
             AND T7100.C7100_CASE_INFO_ID = T7106.C7100_CASE_INFO_ID
	         AND t7100.c7100_void_fl IS NULL
	         AND T7106.c7106_void_fl IS NULL;

	  END gm_fch_case_book_dtl;
	  
/* *******************************************************************************
 * Author:Agilan 
 * Procedure:gm_fch_asso_rep_acc
 * Description:Fetch associate rep emailid based on account
 ***********************************************************************************/ 
  PROCEDURE  gm_fch_asso_rep_acc (
    p_acc_id 		    IN     T704_Account.C704_ACCOUNT_ID%type,
    p_out_cursor 		OUT    TYPES.cursor_type)
  AS
  BEGIN 
	   
  		OPEN p_out_cursor
  		FOR 
   
  		SELECT C703_SALES_REP_ID, GET_USER_EMAILID(C703_SALES_REP_ID) EMAILID
     	  FROM T704A_ACCOUNT_REP_MAPPING 
    	 WHERE C704_ACCOUNT_ID = p_acc_id   --'5387' 
      	   AND C704A_ACTIVE_FL='Y' 
           AND C704A_VOID_FL IS NULL;
      
  	END gm_fch_asso_rep_acc;

/******************************************************************************************
       * Description:Check rep catogory is direct sales rep or not
*******************************************************************************************/
  FUNCTION  gm_check_dir_rep (
    	p_rep_id 		    IN     T703_SALES_REP.C703_SALES_REP_ID%type)
  RETURN VARCHAR2
  IS
  
  	v_rep_cat        VARCHAR2(10);

  BEGIN 
	   
 		BEGIN
	       SELECT C703_REP_CATEGORY
	       	 INTO v_rep_cat    
	         FROM T703_SALES_REP 
	        WHERE C703_SALES_REP_ID = p_rep_id 
	          AND C703_VOID_FL IS NULL;
	   EXCEPTION
	      WHEN NO_DATA_FOUND
	   THEN
	      RETURN ' ';
	      END;
	RETURN v_rep_cat;
      
  	END gm_check_dir_rep;

/******************************************************************************************
       * Description:Check rep catogory is direct sales rep or not
*******************************************************************************************/
  PROCEDURE  gm_check_dir_rep (
    	p_rep_id 		    IN     T703_SALES_REP.C703_SALES_REP_ID%type,
        p_out               OUT    TYPES.cursor_type)
  AS
  
  	v_rep_cat        VARCHAR2(10);

  BEGIN 
	   
 		OPEN p_out
 		FOR
	       SELECT C703_REP_CATEGORY CAT,
	              GET_USER_EMAILID(p_rep_id) REPEMAILID
	       	-- INTO v_rep_cat    
	         FROM T703_SALES_REP 
	        WHERE C703_SALES_REP_ID = p_rep_id 
	          AND C703_VOID_FL IS NULL;
	          
  	END gm_check_dir_rep;

/******************************************************************************************
       * Description:Fetch case share details rep email id
*******************************************************************************************/   
  PROCEDURE  gm_fch_case_share_mail (
    	p_case_infoid 		IN     T7100_CASE_INFORMATION.C7100_CASE_INFO_ID%type,
        p_out               OUT    TYPES.cursor_type)  	
        AS
        BEGIN
	        
	        OPEN p_out
	        FOR
	        
	         SELECT T703.C703_SALES_REP_ID repid,
	         		T7107.C7107_REP_PARTY_ID partyid,
	         		GET_USER_EMAILID(T703.C703_SALES_REP_ID) shareEmail
	           FROM T703_SALES_REP t703,
	           		T7107_CASE_SHARE t7107 
	          WHERE T7107.C7107_REP_PARTY_ID = T703.C101_PARTY_ID 
	            AND T7107.C7100_CASE_INFO_ID=p_case_infoid
	            AND T7107.c7107_void_fl is null
	            AND t703.c703_void_fl is null; 
	        
	END gm_fch_case_share_mail;
END gm_pkg_case_rpt;
  /
