CREATE OR REPLACE
PACKAGE BODY gm_pkg_case_wrap
IS

/* *******************************************************************************
 * Author: Agilan singaravel
 * Procedure:gm_sav_case_dtl
 * Description:This Procedure is used to save case details into appropriate table
 * 			   by calling repective procedures.
 ***********************************************************************************/
  PROCEDURE gm_sav_case_dtl( 
	    p_input_string 		IN 		CLOB,
	    p_user_id      		IN 		T7100_CASE_INFORMATION.C7100_LAST_UPDATED_BY%TYPE,
	    p_case_info_id 	   	OUT 	VARCHAR2
  )
  AS
  		v_json 	 		json_object_t;
  		v_case_id		T7100_CASE_INFORMATION.C7100_CASE_ID%TYPE;
  		v_surg_dt   	varchar2(20);
  		v_start_time 	varchar2(20);
  		v_end_time 		varchar2(20);
  		v_acc_id		T7100_CASE_INFORMATION.C704_ACCOUNT_ID%TYPE;
  		v_surg_lvl      T7100_CASE_INFORMATION.C901_SURGERY_LEVEL%TYPE;
  		v_surg_type     T7100_CASE_INFORMATION.C901_SURGERY_TYPE%TYPE;
  		v_notes			T7100_CASE_INFORMATION.C7100_PERSONAL_NOTES%TYPE;
  		v_status		T7100_CASE_INFORMATION.C901_CASE_STATUS%TYPE;
  		v_party_id      T7100_CASE_INFORMATION.C7100_OWNER_PARTY_ID%TYPE;
  		v_case_info_id  T7100_CASE_INFORMATION.C7100_CASE_INFO_ID%TYPE;
  		v_comp_id       number(10);
  		v_comp_fmt		varchar2(20);
  		
  BEGIN
	  v_json      	:= json_object_t.parse(p_input_string);
	  v_surg_dt   	:= v_json.get_String('surgdt');
	  v_start_time  := v_json.get_String('starttm');
	 -- v_start_time 	:= TO_TIMESTAMP_TZ(v_surg_dt||' '||v_json.get_String('starttm'));
	  v_end_time 	:= v_json.get_String('endtm');
	 -- v_end_time    := TO_DATE(v_surg_dt||' '||v_json.get_String('endtm'),'yyyy/mm/dd hh24:mi');
	  v_acc_id      := v_json.get_String('accid');
	  v_surg_lvl    := v_json.get_String('surglvlid');
	  v_surg_type   := v_json.get_String('surgtypeid');
	  v_case_id		:= v_json.get_String('caseid');
	  v_notes		:= v_json.get_String('notes');
	  v_party_id    := v_json.get_String('partyid');
	  v_case_info_id := v_json.get_String('caseinfoid');
	  v_comp_id		:= v_json.get_String('cmpid');
	  v_comp_fmt	:= v_json.get_String('cmpdfmt');
	
	  --Save case details to t7100
	  gm_pkg_case_txn.gm_sav_case_dtl(v_case_id,v_surg_dt,v_start_time,
	  v_end_time,v_acc_id,v_surg_lvl,v_surg_type,v_notes,p_user_id,v_case_info_id,v_party_id,v_comp_id,v_comp_fmt);
	  
	  --Save surgeon details to t7106
	  gm_sav_case_surgeon_dtl(v_case_info_id,treat(v_json.get('arrcasepartyvo') AS json_array_t),p_user_id);
	  
	  --Save set details to t7104
	  gm_sav_case_set_dtl(v_case_info_id,treat(v_json.get('arrcasesetvo') AS json_array_t),p_user_id);
	  
	  --Save part details to t7104
	  gm_sav_case_part_dtl(v_case_info_id,treat(v_json.get('arrcasepartvo') AS json_array_t),p_user_id);
	  
	  --Save share deatils to t7107
	  gm_sav_case_rep_map(v_case_info_id,treat(v_json.get('arrcasesharevo') AS json_array_t),p_user_id);
	  
	  p_case_info_id := v_case_info_id;
	  
END gm_sav_case_dtl;

/* *******************************************************************************
 * Author: Agilan singaravel
 * Procedure:gm_sav_case_surgeon_dtl
 * Description:This Procedure is used to save surgeon details
 ***********************************************************************************/
	PROCEDURE gm_sav_case_surgeon_dtl(
		p_case_infoid       IN   OUT T7100_CASE_INFORMATION.C7100_CASE_INFO_ID%type,
		p_attr_array   		IN   json_array_t,
		p_user_id      		IN   T7100_CASE_INFORMATION.C7100_LAST_UPDATED_BY%TYPE
	  )  
	  AS	  
	  		v_set_attr_obj   json_object_t;
	  BEGIN
		  
		  FOR i IN 0 .. p_attr_array.get_size - 1
		 LOOP
		 
		    v_set_attr_obj := treat(p_attr_array.get(i) AS json_object_t);
		    
			gm_pkg_case_txn.gm_sav_case_surgeon_dtl (p_case_infoid,
									v_set_attr_obj.get_String('npiid'),
									v_set_attr_obj.get_String('partyid'),
									v_set_attr_obj.get_String('partynm'),
									v_set_attr_obj.get_String('voidfl'),
									p_user_id );
		 END LOOP;
		  
  END gm_sav_case_surgeon_dtl;
  
 /* *******************************************************************************
 * Author: Agilan singaravel
 * Procedure:gm_sav_case_set_dtl
 * Description:This Procedure is used to save set details 
 ***********************************************************************************/
	PROCEDURE gm_sav_case_set_dtl(
		p_case_infoid       IN   OUT T7100_CASE_INFORMATION.C7100_CASE_INFO_ID%type,
		p_attr_array   		IN   json_array_t,
		p_user_id      		IN   T7100_CASE_INFORMATION.C7100_LAST_UPDATED_BY%TYPE
	  )  
	  AS	  
	  		v_set_attr_obj   json_object_t;
	  BEGIN
		  
		  FOR i IN 0 .. p_attr_array.get_size - 1
		 LOOP
				
		    v_set_attr_obj := treat(p_attr_array.get(i) AS json_object_t);

			gm_pkg_case_txn.gm_sav_case_set_dtl (p_case_infoid,
									v_set_attr_obj.get_String('kitid'),
									v_set_attr_obj.get_String('setid'),
									v_set_attr_obj.get_String('tagid'),
									v_set_attr_obj.get_String('qty'),
									v_set_attr_obj.get_String('voidfl'),
									p_user_id );
		 END LOOP;
		  
  END gm_sav_case_set_dtl; 
  
/* *******************************************************************************
 * Author: Agilan singaravel
 * Procedure:gm_sav_case_part_dtl
 * Description:This Procedure is used to save part details 
 ***********************************************************************************/
	PROCEDURE gm_sav_case_part_dtl(
		p_case_infoid       IN   OUT T7100_CASE_INFORMATION.C7100_CASE_INFO_ID%type,
		p_attr_array   		IN   json_array_t,
		p_user_id      		IN   T7100_CASE_INFORMATION.C7100_LAST_UPDATED_BY%TYPE
	  )  
	  AS	  
	  		v_set_attr_obj   json_object_t;
	  BEGIN
		  
		  FOR i IN 0 .. p_attr_array.get_size - 1
		 LOOP
		 
		    v_set_attr_obj := treat(p_attr_array.get(i) AS json_object_t);
		    
			gm_pkg_case_txn.gm_sav_case_part_dtl (p_case_infoid,
									v_set_attr_obj.get_String('kitid'),
									v_set_attr_obj.get_String('partnum'),
									v_set_attr_obj.get_String('qty'),
									v_set_attr_obj.get_String('voidfl'),
									p_user_id );
		 END LOOP;
		  
  END gm_sav_case_part_dtl; 
/* *******************************************************************************
 * Author: Agilan singaravel
 * Procedure:gm_sav_case_rep_map
 * Description:This Procedure is used to save share details on case
 ***********************************************************************************/
	PROCEDURE gm_sav_case_rep_map(
		p_case_infoid       IN   OUT T7100_CASE_INFORMATION.C7100_CASE_INFO_ID%type,
		p_attr_array   		IN   json_array_t,
		p_user_id      		IN   T7100_CASE_INFORMATION.C7100_LAST_UPDATED_BY%TYPE
	  )  
	  AS	  
	  		v_set_attr_obj   json_object_t;
	  BEGIN
		  
		  FOR i IN 0 .. p_attr_array.get_size - 1
		 LOOP
		 
		    v_set_attr_obj := treat(p_attr_array.get(i) AS json_object_t);
		    
			gm_pkg_case_txn.gm_sav_case_rep_map (p_case_infoid,
									v_set_attr_obj.get_String('repid'),
									v_set_attr_obj.get_String('voidfl'),
									p_user_id );
		 END LOOP;
		  
  END gm_sav_case_rep_map;   
  
 END gm_pkg_case_wrap;
 /