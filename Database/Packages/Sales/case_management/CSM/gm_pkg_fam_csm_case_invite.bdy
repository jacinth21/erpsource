--@"C:\Database\Packages\gm_pkg_fam_csm_case_invite.bdy";
CREATE OR REPLACE
PACKAGE BODY GM_PKG_FAM_CSM_CASE_INVITE
IS
PROCEDURE gm_fch_case_receipients_emails (
	p_coveredbyrepid 	IN  VARCHAR2,
    p_sharedrepids 		IN  VARCHAR2,
    p_emailstr          OUT CLOB)
IS
	v_access_level t101_user.c101_access_level_id%TYPE;
	all_repids VARCHAR2(3000);
	v_associates_repids VARCHAR2(3000);
  v_assoc_rpt_repid 	t703_sales_rep.c703_sales_rep_id%TYPE;

	BEGIN 
		all_repids := p_sharedrepids;
		BEGIN	
		  	SELECT t101.c101_access_level_id 
		  	INTO v_access_level
		  	FROM t101_user t101,t703_sales_rep t703 
		  	WHERE t101.c101_party_id = t703.c101_party_id
		  	AND t703.c703_sales_rep_id = p_coveredbyrepid
		  	AND t703.c703_void_fl IS NULL;
			EXCEPTION
			WHEN NO_DATA_FOUND THEN
		  			v_access_level := NULL;
		END;
		all_repids := all_repids ||',' || p_coveredbyrepid;
		
		IF v_access_level = 7 THEN
     v_assoc_rpt_repid :=p_coveredbyrepid;
			all_repids := all_repids || ',' || v_assoc_rpt_repid;

  		BEGIN		
          SELECT listagg(repid,',') WITHIN GROUP(ORDER BY 1)
          INTO v_associates_repids
          FROM
            (SELECT DISTINCT c703_sales_rep_id repid
            FROM T704A_ACCOUNT_REP_MAPPING t704a,
              (SELECT DISTINCT ac_id
              FROM v700_territory_mapping_detail
              WHERE rep_id =p_coveredbyrepid
              OR rep_id    = v_assoc_rpt_repid
              ) v700
            WHERE t704a.C704_ACCOUNT_ID = v700.ac_id
            AND t704a.c704a_void_fl    IS NULL
            );
      EXCEPTION
          WHEN NO_DATA_FOUND THEN
                v_associates_repids := NULL;
      END;
			
      IF v_associates_repids IS NOT NULL THEN
        all_repids := all_repids || ',' || v_associates_repids;
      END IF;
		END IF;
	
		my_context.set_my_inlist_ctx(all_repids);
   
    BEGIN
      SELECT listagg(c107_contact_value,',') WITHIN GROUP( ORDER BY 1)
      INTO p_emailstr
      FROM t107_contact t107 ,
        t703_sales_rep t703
      WHERE t107.c101_party_id    = t703.c101_party_id
      AND t703.c703_void_fl      IS NULL
      AND t703.c703_sales_rep_id IN 
      (SELECT token FROM v_in_list
         WHERE token IS NOT NULL
        )
      AND c901_mode           = 90452 --Business
      AND c107_void_fl       IS NULL
      AND c107_primary_fl     = 'Y'
      AND t703.c703_active_fl ='Y'
      AND c107_inactive_fl   IS NULL;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
      p_emailstr := '';
    END;
    
  IF INSTR(p_emailstr,';') > 0 THEN
    p_emailstr := REPLACE(p_emailstr,';',',');
  END IF;
  
	END gm_fch_case_receipients_emails;
END GM_PKG_FAM_CSM_CASE_INVITE;
/



