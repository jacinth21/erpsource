
--@"C:\Database\Packages\sales\case_management\gm_pkg_case_schedular.bdy";
CREATE OR REPLACE PACKAGE BODY gm_pkg_case_schedular
IS
--

/* *******************************************************************************
 * Author: Agilan singaravelan
 * Procedure:gm_save_case_details
 * Description:Procedure to save Case schedular details
 ***********************************************************************************/ 
	PROCEDURE gm_save_case_details (
	     p_surgery_date	  IN	   t7100_case_information.c7100_surgery_date%TYPE
	   , p_ship_date	  IN	   t7100_case_information.c7100_ship_date%TYPE
	   , p_return_date	  IN	   t7100_case_information.c7100_return_date%TYPE
	   , p_division		  IN	   t7100_case_information.c1910_division_id%TYPE
	   , p_dist_id		  IN	   t7100_case_information.c701_distributor_id%TYPE
	   , p_rep_id		  IN	   t7100_case_information.c703_sales_rep_id%TYPE
	   , p_account_id	  IN	   t7100_case_information.c704_account_id%TYPE
	   , p_userid		  IN	   t7100_case_information.c7100_created_by%TYPE
       , p_status   	  IN	   t7100_case_information.C901_CASE_STATUS%TYPE
	   , p_outcaseid 	  IN OUT   t7100_case_information.c7100_case_id%TYPE
	   , p_company_id     IN       t1900_company.c1900_company_id%TYPE
	   , p_long_term      IN       t7100_case_information.C7100_LONG_TERM_FL%TYPE
	)
	AS
		v_seq		   NUMBER;
		v_case_id	   VARCHAR2 (100) := NULL;
		v_status       t7100_case_information.C901_CASE_STATUS%TYPE;
	BEGIN
		
		
		IF(p_status = 107161) THEN --Shipped Case Status
		
			SELECT C901_CASE_STATUS into v_status
		     FROM T7100_CASE_INFORMATION
		    WHERE C1900_COMPANY_ID = p_company_id
    	      AND C7100_CASE_ID =  p_outcaseid
     	      AND C7100_VOID_FL IS NULL;
     	   
     	    IF(v_status = 107161) THEN 
			    UPDATE T7100_CASE_INFORMATION 
				   SET C901_CASE_STATUS = p_status,
				       C7100_RETURN_DATE = p_return_date,
				       C7100_LONG_TERM_FL = p_long_term,
				   	   C7100_LAST_UPDATED_BY = p_userid,
				       C7100_LAST_UPDATED_DATE = CURRENT_DATE
	     		 WHERE C1900_COMPANY_ID = p_company_id
	    	       AND C7100_CASE_ID =  p_outcaseid
	     		   AND C7100_VOID_FL IS NULL;
     		ELSE
	     		UPDATE T7100_CASE_INFORMATION 
				   SET C901_CASE_STATUS = p_status,
				   	   C7100_SURGERY_DATE = p_surgery_date,
				   	   C7100_SHIP_DATE = p_ship_date,
				   	   C7100_RETURN_DATE = p_return_date,
				   	   C1910_DIVISION_ID = p_division,
				   	   C701_DISTRIBUTOR_ID = p_dist_id,
				   	   C703_SALES_REP_ID = p_rep_id,
				   	   C704_ACCOUNT_ID = p_account_id,
				   	   C7100_LONG_TERM_FL = p_long_term,
				       C7100_LAST_UPDATED_BY = p_userid,
				       C7100_LAST_UPDATED_DATE = CURRENT_DATE
	     		 WHERE C1900_COMPANY_ID = p_company_id
	    	       AND C7100_CASE_ID =  p_outcaseid
	     		   AND C7100_VOID_FL IS NULL;
     		END IF;
     		   
    	    UPDATE T2078_KIT_MAPPING 
	           SET C901_KIT_IN_OUT_STATUS = '107282', --Shipped for kit status
	          	   C2078_LAST_TRANS_UPDATED_BY = p_userid,
	          	   C2078_LAST_TRANS_UPDATED_DATE = CURRENT_DATE
	         WHERE C2078_LAST_UPDATED_TRANS_ID = p_outcaseid
	           AND C2078_VOID_FL is null
	           AND C1900_COMPANY_ID = p_company_id;
     
       ELSIF (p_status = 107162 OR p_status = 107163) THEN --107162 Returned and 107163-Canceled
            UPDATE T7100_CASE_INFORMATION 
			   SET C901_CASE_STATUS = p_status,  
			       C7100_RETURN_DATE = decode(p_status,107162,current_date,C7100_RETURN_DATE),
		 	       C7100_LAST_UPDATED_BY = p_userid,
			       C7100_LAST_UPDATED_DATE = CURRENT_DATE
     		 WHERE C1900_COMPANY_ID = p_company_id
    	       AND C7100_CASE_ID =  p_outcaseid
     		   AND C7100_VOID_FL IS NULL;
     		   
    	    UPDATE T2078_KIT_MAPPING 
	           SET C901_KIT_IN_OUT_STATUS = '107280', --Cretaed for kit status
	           	   C2078_LAST_UPDATED_TRANS_ID = null,
	          	   C2078_LAST_TRANS_UPDATED_BY = p_userid,
	          	   C2078_LAST_TRANS_UPDATED_DATE = CURRENT_DATE
	         WHERE C2078_LAST_UPDATED_TRANS_ID = p_outcaseid
	           AND C2078_VOID_FL is null
	           AND C1900_COMPANY_ID = p_company_id;
	          
	ELSE 
	        UPDATE T7100_CASE_INFORMATION 
			   SET C901_CASE_STATUS = p_status,
			   	   C7100_SURGERY_DATE = p_surgery_date,
			   	   C7100_SHIP_DATE = p_ship_date,
			   	   C7100_RETURN_DATE = p_return_date,
			   	   C1910_DIVISION_ID = p_division,
			   	   C701_DISTRIBUTOR_ID = p_dist_id,
			   	   C703_SALES_REP_ID = p_rep_id,
			   	   C704_ACCOUNT_ID = p_account_id,
			   	   C7100_LONG_TERM_FL = p_long_term,
			       C7100_LAST_UPDATED_BY = p_userid,
			       C7100_LAST_UPDATED_DATE = CURRENT_DATE
     		 WHERE C1900_COMPANY_ID = p_company_id
    	       AND C7100_CASE_ID =  p_outcaseid
     		   AND C7100_VOID_FL IS NULL;

     IF (SQL%ROWCOUNT = 0) THEN 
     		   		   
		SELECT  NVL (MAX(TO_NUMBER (SUBSTR (C7100_CASE_ID,INSTR(C7100_CASE_ID, '-', -1)+1))), 0) +1  			
	  	  INTO  v_seq
	 	  FROM  t7100_case_information
	 	 WHERE  c7100_case_id LIKE 'CS-' || p_rep_id || '-' || TO_char (p_surgery_date, 'yyyymmdd') || '%' 
	 	   AND  c7100_void_fl IS NULL; 

	p_outcaseid	:=
		   'CS-'
			|| p_rep_id
		    || '-'
			|| TO_char (p_surgery_date, 'yyyymmdd')
		    || '-'
			|| v_seq;	

		INSERT INTO t7100_case_information
						(c7100_case_info_id, c7100_case_id, c7100_surgery_date
					   , c7100_ship_date,c7100_return_date, c1910_division_id,c701_distributor_id, c703_sales_rep_id
					   , c704_account_id,C7100_LONG_TERM_FL, c7100_created_by, c7100_created_date,  c901_case_status, C901_Type,c1900_company_id
						)
				 VALUES ( s7100_case_information.NEXTVAL,p_outcaseid, p_surgery_date
					   ,p_ship_date, p_return_date,p_division, p_dist_id, p_rep_id
					   , DECODE (p_account_id, 0, NULL, p_account_id),p_long_term, p_userid, CURRENT_DATE, p_status, 107285, p_company_id
						);  		
	  END IF;
	 END IF;
	 p_outcaseid := p_outcaseid;	
END gm_save_case_details;			
				
/* *******************************************************************************
 * Author: Agilan singaravelan
 * Procedure:gm_fetch_kit_details
 * Description:Procedure to fetch kit details
 ***********************************************************************************/
 PROCEDURE gm_fetch_kit_details(
 	  p_kit_id   	IN  	varchar
 	, p_kit_data 	OUT 	TYPES.cursor_type
 	, p_kit_status  OUT 	varchar
 	, p_caseid      IN      t7100_case_information.c7100_case_id%TYPE
  )
   AS
   v_trans_id   		T2078_KIT_MAPPING.C2078_LAST_UPDATED_TRANS_ID%TYPE;
   v_ship_date_old      t7100_case_information.c7100_ship_date%TYPE;
   v_ship_date_new 	    t7100_case_information.c7100_ship_date%TYPE;
   v_return_date_old    t7100_case_information.c7100_return_date%TYPE;
   v_return_date_new    t7100_case_information.c7100_return_date%TYPE;
   v_kit_status_ship    varchar2(10);
   v_kit_status_return  varchar2(10);
   
   cursor check_case_id
   IS
		SELECT t2078a.c2078a_last_updated_trans_id
		  FROM t2078a_kit_mapping_log t2078a, t7100_case_information t7100, t7105_case_schedular_dtl t7105
		 WHERE t2078a.c2078_kit_map_id = p_kit_id -- 'KM-158'
		   AND t7105.c2078_kit_map_id = p_kit_id
		   AND t2078a.c2078a_last_updated_trans_id is not null
		   AND t7100.c7100_case_info_id = t7105.c7100_case_info_id
		   AND t2078a.c2078a_void_fl    IS NULL
		   AND t7100.c7100_case_id   = t2078a.c2078a_last_updated_trans_id
		   AND t7100.c901_case_status not in ('107163','107162')
		   AND t7100.c7100_void_fl is null
		   AND t7105.c7105_void_fl is null;
     BEGIN 
		BEGIN
         SELECT c7100_ship_date, C7100_RETURN_DATE
	     INTO v_ship_date_new,v_return_date_new
	     FROM t7100_case_information
	    WHERE c7100_case_id = p_caseid
	      AND c7100_void_fl is null;
  		END;
	    
  		
	    FOR check_overlap IN check_case_id
		LOOP
		   SELECT c7100_ship_date, C7100_RETURN_DATE
	     	 INTO v_ship_date_old,v_return_date_old
	     	 FROM t7100_case_information
	    	WHERE c7100_case_id = check_overlap.c2078a_last_updated_trans_id
	      	  AND c7100_void_fl is null;
		    
	      	 IF ((TRUNC(v_ship_date_new) < TRUNC(v_ship_date_old)) AND (TRUNC(v_return_date_new) > TRUNC(v_ship_date_old))) THEN   
	     			v_kit_status_ship := 'Y';
			 END IF;	
	     	  
			 IF ((TRUNC(v_ship_date_new) < TRUNC(v_return_date_old)) AND (TRUNC(v_return_date_new) > TRUNC(v_return_date_old))) THEN   
	     			v_kit_status_ship := 'Y';
			 END IF;
	     	  
			 IF ((TRUNC(v_ship_date_new) >= TRUNC(v_ship_date_old)) AND (TRUNC(v_return_date_new) <= TRUNC(v_return_date_old))) THEN   
	     			v_kit_status_ship := 'Y';
    		 END IF;  

	     	  IF(v_kit_status_ship = 'Y')THEN
	     	  		p_kit_status := 'N' || '^' || check_overlap.c2078a_last_updated_trans_id;
	     	  END IF;
	   END LOOP;

       
		OPEN p_kit_data
           FOR
           
         SELECT t2078.C2078_KIT_MAP_ID kitid,
          		t2078.C2078_KIT_NM kitnm,
  				t2078.C901_KIT_IN_OUT_STATUS STATUS,
  				t2078.C2078_ACTIVE_FL activefl, 
  				t2079.C207_SET_ID setid,
  				'1' QTY,
  				get_set_name(t2079.C207_SET_ID) setnm ,
  				p_kit_status kitsts,
  				t2079.C5010_TAG_ID tagid
  		   FROM T2078_KIT_MAPPING t2078,   T2079_KIT_MAP_DETAILS t2079
  		  WHERE t2078.C2078_KIT_MAP_ID = p_kit_id
  		    AND t2078.C2078_ACTIVE_FL = 'Y'
			AND t2078.C2078_KIT_MAP_ID   = t2079.C2078_KIT_MAP_ID(+)
			AND t2078.C2078_VOID_FL IS NULL
			AND t2079.C2079_VOID_FL IS NULL;

	  END  gm_fetch_kit_details;
	  
 /* *******************************************************************************
 * Author: Agilan singaravelan
 * Procedure:gm_save_inv_selection
 * Description:Procedure to save inventory selection
 ***********************************************************************************/
 PROCEDURE gm_save_inv_selection(
      p_case_id	  	IN	   t7100_case_information.c7100_case_id%TYPE
 	, p_kit_id    	IN     T2078_KIT_MAPPING.C2078_KIT_MAP_ID%TYPE
 	, p_userid	  	IN	   t7100_case_information.c7100_created_by%TYPE
 	, p_company_id  IN     t1900_company.c1900_company_id%TYPE
 	, p_status      OUT    t7100_case_information.c901_case_status%TYPE
  )
  AS
  	  v_case_info_id    t7100_case_information.c7100_case_info_id%TYPE; 
  	  v_ship_date	 	t7100_case_information.c7100_ship_date%TYPE;
  BEGIN
	    
	  BEGIN
	  SELECT c7100_case_info_id,c7100_ship_date
	    INTO v_case_info_id,v_ship_date
	    FROM t7100_case_information
	   WHERE c7100_case_id = p_case_id
	   	 AND c1900_company_id = p_company_id
	     AND c7100_void_fl is null;
	    
	   EXCEPTION WHEN NO_DATA_FOUND     -- NO DATA FOUND EXCEPTION
	   THEN
   		v_case_info_id := NULL;
   	   END; 

   	/*  Code commented. because first kit saved then status update shipped and not able to add other kit.
   	 * IF(TRUNC(v_ship_date) = TRUNC(CURRENT_DATE)) THEN
   	
   	  UPDATE t7100_case_information
   	     SET c901_case_status = '107161', -- Case status Shipped
   	  	     C7100_LAST_UPDATED_BY = p_userid,
	    	 C7100_LAST_UPDATED_DATE = CURRENT_DATE
	   WHERE C7100_CASE_ID = p_case_id
	     AND C7100_VOID_FL is null;
	     
	     gm_pkg_case_schedular.gm_save_set_details(p_case_id,p_kit_id,'107161',p_userid,p_company_id); -- Save the tag details.
   	  END IF;
   	  
   	  IF(TRUNC(v_ship_date) = TRUNC(CURRENT_DATE)) THEN
   	   		p_status := '107161';
   	  ELSE
   	  		p_status := '';
   	  END IF; */
   	  
   	p_status := '';
   	
	IF v_case_info_id IS NOT NULL THEN
	   	  
	  INSERT INTO T7105_CASE_SCHEDULAR_DTL 
	  			  (C7105_CASE_SCHEDULAR_DTL_ID, C7100_CASE_INFO_ID, C2078_KIT_MAP_ID, C7105_VOID_FL, C7105_LAST_UPDATED_BY, C7105_LAST_UPDATED_DATE, C7105_LAST_UPDATED_DATE_UTC)
	       VALUES (S7105_CASE_SCHEDULAR_DTL.nextval, v_case_info_id, p_kit_id, null, p_userid, CURRENT_DATE, CURRENT_DATE);
	       
	       UPDATE t2078_kit_mapping 
	          SET C901_KIT_IN_OUT_STATUS = '107281', --Allocated
	          	  C2078_LAST_UPDATED_TRANS_ID = p_case_id,
	          	  C2078_LAST_TRANS_UPDATED_BY = p_userid,
	          	  C2078_LAST_TRANS_UPDATED_DATE = CURRENT_DATE
	        WHERE C2078_KIT_MAP_ID = p_kit_id
	          AND C2078_void_fl is null;

	   END IF;       
					          
 END gm_save_inv_selection;

 /* *******************************************************************************
 * Author: Agilan singaravelan
 * Procedure:gm_save_case_notes
 * Description:Procedure to save case scdedular notes
 ***********************************************************************************/
  PROCEDURE gm_save_case_notes(
      p_case_id	  		IN	   t7100_case_information.c7100_case_id%TYPE
 	, p_userid	  		IN	   t7100_case_information.c7100_created_by%TYPE
 	, p_comments  		IN     t902_log.c902_comments%TYPE
 	, p_company_id      IN     t1900_company.c1900_company_id%TYPE
  )
 AS
 BEGIN
	 UPDATE  T7100_CASE_INFORMATION
	    SET  C7100_PERSONAL_NOTES = p_comments,
	    	 C7100_LAST_UPDATED_BY = p_userid,
	    	 C7100_LAST_UPDATED_DATE = CURRENT_DATE
	  WHERE  C7100_CASE_ID = p_case_id
	    AND	 c1900_company_id = p_company_id
	    AND  C7100_VOID_FL is null;
	    	    
END  gm_save_case_notes;

 /* *******************************************************************************
 * Author: Agilan singaravelan
 * Procedure:gm_fetch_case_details
 * Description:Procedure to fetch case scdedular details
 ***********************************************************************************/
  PROCEDURE gm_fetch_case_details(
      p_case_id           IN      t7100_case_information.c7100_case_id%TYPE
    , p_company_id        IN      t1900_company.c1900_company_id%TYPE
    , p_out               OUT     TYPES.cursor_type
  )
 AS
 v_datefmt VARCHAR2(50);
 BEGIN
    
    SELECT get_compdtfmt_frm_cntx()
      INTO v_datefmt 
      FROM DUAL;

     OPEN p_out
           FOR
     SELECT c7100_case_id CASEID,
            c7100_surgery_date SURGERYDT,
            c7100_ship_date SHIPDT,
            c7100_return_date RETDT,
            TO_char(c7100_surgery_date, v_datefmt) SURGERYDTFMT,            
            TO_char(c7100_ship_date,v_datefmt) SHIPDTFMT,
            TO_char(c7100_return_date,v_datefmt) RETDTFMT,
            c1910_division_id DIVISION,
            GET_DISTRIBUTOR_NAME(c701_distributor_id) SEARCHDISTRIBUTOR,
            GET_REP_NAME(c703_sales_rep_id) SEARCHSALESREP,
            GET_ACCOUNT_NAME(c704_account_id) SEARCHACCOUNT,
            c701_distributor_id DISTRIBUTOR,
            c703_sales_rep_id SALESREP,
            c704_account_id ACCOUNT,
            c901_case_status status,get_division_name(c1910_division_id) DIVNAME,
            C7100_LONG_TERM_FL LONGTREM
       FROM t7100_case_information
      WHERE c7100_case_id = p_case_id
        AND c7100_void_fl is null
        AND c1900_company_id = p_company_id;
END  gm_fetch_case_details;

/* *******************************************************************************
 * Author: Agilan singaravelan
 * Procedure:gm_fetch_inv_selection
 * Description:Procedure to fetch inv selection kit details
 ***********************************************************************************/
 PROCEDURE gm_fetch_inv_selection(
 	  p_case_id	  IN	   t7100_case_information.c7100_case_id%TYPE
 	, p_kit_data  OUT TYPES.cursor_type
  )
   AS
   	p_kit_id    T2078_KIT_MAPPING.C2078_KIT_MAP_ID%TYPE;
    v_case_info_id    t7100_case_information.c7100_case_info_id%TYPE; 
    
  BEGIN
	  
    OPEN p_kit_data
           FOR

     SELECT t2078.C2078_KIT_MAP_ID kitid,
    		t7105.C7100_case_info_id,
    		T7100.C7100_case_info_id ,
    		T7100.C901_case_status status,
  			t2078.C2078_KIT_NM kitnm,
  			t2079.C207_SET_ID SETID,
  			get_set_name(t2079.C207_SET_ID) SETNM,
  			'1' QTY,
  			t2079.C5010_TAG_ID TAGID
	   FROM t7100_case_information t7100, 
	  		T7105_CASE_SCHEDULAR_DTL t7105,       
  			t2078_kit_mapping t2078, 
  			t2079_kit_map_details t2079
	  WHERE T7100.C7100_case_info_id =  t7105.C7100_case_info_id
	    AND t2078.c2078_kit_map_id = t2079.c2078_kit_map_id
		AND t2078.c2078_kit_map_id = t7105.c2078_kit_map_id
		AND t7100.c7100_case_id  = p_case_id		
		AND t7100.c7100_void_fl IS NULL
		AND t7105.C7105_VOID_FL IS NULL
		AND t2078.c2078_void_fl IS NULL
		AND t2079.c2079_void_fl IS NULL;

	  END  gm_fetch_inv_selection;

 /* *******************************************************************************
 * Author: Agilan singaravelan
 * Procedure:gm_remove_kit
 * Description:Procedure to remove kit details from inventory selection tab
 ***********************************************************************************/
   PROCEDURE gm_remove_kit(
   	  p_kit_id    	IN     T2078_KIT_MAPPING.C2078_KIT_MAP_ID%TYPE
    , p_case_id	  	IN	   t7100_case_information.c7100_case_id%TYPE
    , p_company_id  IN     t1900_company.c1900_company_id%TYPE 
    , p_userid		IN	   t7100_case_information.c7100_created_by%TYPE
    , p_count       OUT    varchar2
    , p_check_date  OUT    varchar2
  )
  AS
      v_case_info_id    t7100_case_information.c7100_case_info_id%TYPE;
      v_count           varchar2(10);
      v_ship_date	 	t7100_case_information.c7100_ship_date%TYPE;
  BEGIN
	  
	  BEGIN
   	  SELECT c7100_case_info_id,c7100_ship_date
	    INTO v_case_info_id,v_ship_date
	    FROM t7100_case_information
	   WHERE c7100_case_id = p_case_id
	   	 AND c1900_company_id = p_company_id
	     AND c7100_void_fl is null;
  		EXCEPTION WHEN NO_DATA_FOUND
	       THEN
   		      v_case_info_id := NULL;
   		      v_ship_date := NULL;
   	       END;

   	SELECT COUNT(*)
	  INTO v_count 
	  FROM T7105_CASE_SCHEDULAR_DTL
	 WHERE c7100_case_info_id = v_case_info_id
	   AND C7105_void_fl IS NULL;

	  IF(TRUNC(v_ship_date) < TRUNC(CURRENT_DATE)) 
	  THEN
   	   		p_check_date := 'Y';
   	  ELSIF(TRUNC(v_ship_date) = TRUNC(CURRENT_DATE)) 
   	  THEN
   	   		gm_pkg_case_schedular.gm_save_set_details(p_case_id,p_kit_id,'107162',p_userid,p_company_id); -- Save the tag details. 		
   	  ELSE
   	  		p_check_date := '';
   	  END IF;
  
	   IF(v_count != '1' AND p_check_date is null) THEN
   	    UPDATE T7105_CASE_SCHEDULAR_DTL
   	       SET C7105_void_fl = 'Y',
   	           C7105_LAST_UPDATED_BY = p_userid,
   	           C7105_LAST_UPDATED_DATE = CURRENT_DATE
   	     WHERE c7100_case_info_id = v_case_info_id
   	       AND C2078_KIT_MAP_ID = p_kit_id
   	       AND C7105_void_fl is null;
   	    
   	    UPDATE T2078_KIT_MAPPING 
	       SET C901_KIT_IN_OUT_STATUS = '107280', --Cretaed for kit status
	           C2078_LAST_UPDATED_TRANS_ID = null,
	           C2078_LAST_TRANS_UPDATED_BY = p_userid,
	           C2078_LAST_TRANS_UPDATED_DATE = CURRENT_DATE
	     WHERE C2078_LAST_UPDATED_TRANS_ID = p_case_id
	       AND C2078_KIT_MAP_ID = p_kit_id
	       AND C2078_VOID_FL is null
	       AND C1900_COMPANY_ID = p_company_id;
 	  ELSE
	 	p_count := v_count;
	  END IF;
  END gm_remove_kit;
  
/* *******************************************************************************
 * Author: Agilan singaravelan
 * Procedure:gm_update_tag_details
 * Description:Procedure to save set details in tag report
 ***********************************************************************************/
   PROCEDURE gm_save_set_details(
      p_case_id	  		IN	   t7100_case_information.c7100_case_id%TYPE
 	, p_kit_id    		IN     T2078_KIT_MAPPING.C2078_KIT_MAP_ID%TYPE
 	, p_status    		IN     t7100_case_information.c901_case_status%TYPE
 	, p_userid	  		IN	   t7100_case_information.c7100_created_by%TYPE
 	, p_company_id  	IN     t1900_company.c1900_company_id%TYPE 
  )
  AS
  		v_acc_id 		  t7100_case_information.c704_account_id%TYPE;
  		v_case_info_id    t7100_case_information.c7100_case_info_id%TYPE; 
  		
  		   -- fetch kit id's   
   CURSOR case_details_kit
	   IS
   SELECT c2078_kit_map_id 
     FROM T7105_CASE_SCHEDULAR_DTL 
    WHERE c7100_case_info_id IN (SELECT c7100_case_info_id
	      						   FROM t7100_case_information
	     						  WHERE c7100_case_id = p_case_id
	       						    AND c7100_void_fl is null)		 --v_case_info_id
      AND c7105_void_fl is null;
      
           -- fetch tag details
  CURSOR kit_details(v_kit_id T2078_KIT_MAPPING.C2078_KIT_MAP_ID%TYPE)
	  IS
  SELECT c207_set_id,
		 c5010_tag_id
	FROM t2079_kit_map_details
   WHERE c2078_kit_map_id = v_kit_id
	 AND c2079_void_fl IS NULL;

 BEGIN  	       
  BEGIN   
	    SELECT c704_account_id into  v_acc_id
		  FROM t7100_case_information
		 WHERE c7100_case_id=p_case_id
		   AND c1900_company_id = p_company_id;
	    EXCEPTION WHEN NO_DATA_FOUND  THEN
   		     	 v_acc_id := NULL;	  
   	    END;    	             

   FOR case_details_kit1 IN case_details_kit
     LOOP        

	 FOR save_kit_detail IN kit_details(case_details_kit1.c2078_kit_map_id)
     LOOP
		
     UPDATE t5010_tag
		SET c901_sub_location_type = decode(p_status,'107161','11301',null),		
		    c704_account_id      = decode(p_status,'107161',v_acc_id,null),
		    c5010_last_updated_by = p_userid, 
		    c5010_last_updated_date = CURRENT_DATE
	  WHERE c207_set_id      = save_kit_detail.c207_set_id
	    AND c5010_tag_id       = save_kit_detail.c5010_tag_id
		AND c5010_void_fl     IS NULL;
	 END LOOP; 
  END LOOP; 
 
  END gm_save_set_details;
  
 /* *******************************************************************************
 * Author: Agilan singaravelan
 * Procedure:gm_check_case_status
 * Description:Procedure to check case status
 ***********************************************************************************/
   PROCEDURE gm_check_case_status(
      p_case_id	  		IN	   t7100_case_information.c7100_case_id%TYPE
 	, p_case_status     OUT    t7100_case_information.C901_CASE_STATUS%TYPE
 	, p_company_id  	IN     t1900_company.c1900_company_id%TYPE 
 	, p_check_inv       OUT    varchar2
 	, p_return_date     IN     t7100_case_information.c7100_return_date%TYPE
 	, p_return_out      OUT    varchar2
  )
  AS
  v_case_info_id    t7100_case_information.c7100_case_info_id%TYPE; 
  v_count           NUMBER;
  
  cursor check_kitid
  IS
       SELECT DISTINCT T7105.C2078_KIT_MAP_ID kitid
         FROM T7100_CASE_INFORMATION T7100  , T7105_CASE_SCHEDULAR_DTL T7105
        WHERE T7100.C7100_CASE_ID =   p_case_id    --'CS-1897-20190415-1'  
          AND T7105.C7100_CASE_INFO_ID = T7100.C7100_CASE_INFO_ID
          AND T7100.C901_CASE_STATUS not in ('107163','107162')
          AND C7100_VOID_FL IS NULL 
          AND C7105_VOID_FL IS NULL;
          
  cursor check_return_date (v_kit_id T2078_KIT_MAPPING.C2078_KIT_MAP_ID%TYPE)
  IS
     SELECT distinct T7100.C7100_CASE_ID caseid, T7105.C2078_KIT_MAP_ID kitmapid, T7100.C7100_SHIP_DATE shipdate,T2078.C2078_KIT_NM kitnm
      FROM T7100_CASE_INFORMATION T7100, T7105_CASE_SCHEDULAR_DTL T7105, T2078_KIT_MAPPING T2078
      WHERE T7105.C7100_CASE_INFO_ID = T7100.C7100_CASE_INFO_ID
      AND T7105.C2078_KIT_MAP_ID = T2078.C2078_KIT_MAP_ID
      AND T7105.C2078_KIT_MAP_ID = v_kit_id        
      AND T7100.C7100_CASE_ID != NVL(p_case_id,T7100.C7100_CASE_ID)  
      AND T7100.C901_CASE_STATUS not in ('107163','107162')
      AND T7100.C7100_VOID_FL IS NULL
      AND T7105.C7105_VOID_FL IS NULL
      AND T2078.C2078_VOID_FL IS NULL;
 
  BEGIN
	 BEGIN
	  	SELECT c7100_case_info_id
	      INTO v_case_info_id
	      FROM t7100_case_information
	     WHERE c7100_case_id = p_case_id
	       AND c7100_void_fl is null;
  		EXCEPTION WHEN NO_DATA_FOUND
	       THEN
   		      v_case_info_id := NULL;	  
   	       END;
	  BEGIN
			SELECT C901_CASE_STATUS
	  		  INTO p_case_status
	 		  FROM T7100_CASE_INFORMATION
	 		 WHERE c7100_case_info_id = v_case_info_id
	   		  -- AND c1900_company_id = p_company_id
	  		   AND C7100_VOID_FL is null;
	       EXCEPTION WHEN NO_DATA_FOUND  THEN
   		     	 p_case_status := NULL;	  
   	   END;  
   	     	   
   	   SELECT count(*)
   	     INTO v_count 
   	     FROM T7105_CASE_SCHEDULAR_DTL
   	    WHERE c7100_case_info_id = v_case_info_id
   	      AND c7105_void_fl is null;
   	      
   	    IF(v_count = '0') THEN
   	       p_check_inv := 'Y';
   	    ELSE
   	       p_check_inv := '';
   	    END IF;
   	    
     	FOR case_details_reurn IN check_kitid
        LOOP        

	      FOR compare_kit_detail IN check_return_date(case_details_reurn.kitid)
          LOOP
		
      		IF (TRUNC(p_return_date) > TRUNC(compare_kit_detail.shipdate)) THEN   
	     		p_return_out := compare_kit_detail.caseid || ',' || compare_kit_detail.kitnm;
	   		ELSE
	   			p_return_out := '';
      		END IF;  	    
     
	 END LOOP; 
  END LOOP; 
   	    
   END  gm_check_case_status;
	
 END;
/