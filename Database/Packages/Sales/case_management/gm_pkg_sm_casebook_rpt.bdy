
CREATE OR REPLACE PACKAGE BODY gm_pkg_sm_casebook_rpt
IS	
--
	/***********************************************************************************************************************
	   * Description : This procedure will get the required information that is used for sending out the email, 
	   					when the Item Request is approved by AD.
	   				   This proc is used for sending multiple Email @ different status.
	   				   2 -- Mail to AD Request notification
	   				   5,10 -- Mail to PD Request notification
	   * Author 	 : Anilkumar
	***********************************************************************************************************************/
	PROCEDURE gm_fch_ad_aprvl_request_info (
		p_input_str	  IN	VARCHAR2
	  , p_status      IN	VARCHAR2
	  , p_out_cur	  OUT 	TYPES.cursor_type
	)AS 
	BEGIN
		-- putting req ids in vinlist. 
		my_context.set_my_inlist_ctx (p_input_str) ;
		-- putting status ids in cloblist. 
		my_context.set_my_cloblist(p_status||',');
		
		OPEN p_out_cur 
		FOR 
		    SELECT DECODE (case.SHEET_OWNER, NULL, '', T101.C101_USER_F_NAME||' '||T101.C101_USER_L_NAME)
		    SHEET_OWNER_NAME, DECODE (case.SHEET_OWNER, NULL, '', T101.C101_EMAIL_ID) SHEET_OWNER_EMAIL,
		    case.ID,case.PAR_REQID,case.REQ_ID,case.REQ_QTY,case.APPROVED_QTY,case.PNUM,case.PDESC,case.SHEET_OWNER,
		    case.CREATED_BY,case.REP_EMAIL,case.REP_ID,case.APPROVED_BY,case.AD_ID,case.AD_EMAIL_ID		    
		    ,DECODE(get_rep_name(case.REP_ID),' ',get_user_name(case.REP_ID),get_rep_name(case.REP_ID)) REP_NAME,
		    get_latest_log_comments(case.id,'4000760') COMMENTS,get_user_emailid(case.last_updated_by)user_mailid
		    FROM
		    (
		         SELECT T7100.C7100_CASE_INFO_ID, T7100.C7100_CASE_ID ID, T525.C525_PRODUCT_REQUEST_ID PAR_REQID
		          , T526.C526_PRODUCT_REQUEST_DETAIL_ID REQ_ID, T7104.C7104_ITEM_QTY REQ_QTY, T526.C526_QTY_REQUESTED APPROVED_QTY
		          , T526.C205_PART_NUMBER_ID PNUM, T205.C205_PART_NUM_DESC PDESC, GET_DMD_SHEET_OWNER (T526.C205_PART_NUMBER_ID)
		            SHEET_OWNER, T7100.C7100_CREATED_BY CREATED_BY, DECODE (get_rep_id_from_user_id (T7100.C7100_CREATED_BY), NULL,
		            get_user_emailid (T7100.C7100_CREATED_BY), get_party_primary_contact (GET_REP_PARTY_ID (get_rep_id_from_user_id
		            (T7100.C7100_CREATED_BY)), 90452)) REP_EMAIL, DECODE (T7100.C703_SALES_REP_ID, NULL, T7100.C7100_CREATED_BY,
		            T7100.C703_SALES_REP_ID) REP_ID, GET_USER_NAME (T526.C526_APPROVE_REJECT_BY) APPROVED_BY, 
		            get_ad_rep_id ( get_rep_id_from_user_id (T7100.C7100_CREATED_BY)) AD_ID,
		            get_user_emailid (get_ad_rep_id ( get_rep_id_from_user_id (T7100.C7100_CREATED_BY))) AD_EMAIL_ID
		            ,T526.C526_LAST_UPDATED_BY last_updated_by
		           FROM T7104_CASE_SET_INFORMATION T7104, T7100_CASE_INFORMATION T7100, T525_PRODUCT_REQUEST T525
		          , T526_PRODUCT_REQUEST_DETAIL T526, T205_PART_NUMBER T205
		          WHERE T7100.C7100_CASE_INFO_ID             = T7104.C7100_CASE_INFO_ID
		            AND T7104.C526_PRODUCT_REQUEST_DETAIL_ID = T526.C526_PRODUCT_REQUEST_DETAIL_ID
		            AND T525.C525_PRODUCT_REQUEST_ID         = T526.C525_PRODUCT_REQUEST_ID
		            AND T205.C205_PART_NUMBER_ID             = T526.C205_PART_NUMBER_ID
		            AND T525.C901_REQUEST_FOR                = 400088
		            AND T526.C526_STATUS_FL                 IN
		            (
		                 SELECT TO_CHAR (token) FROM v_clob_list WHERE token IS NOT NULL
		            )
		            AND T7100.C7100_VOID_FL                 IS NULL
		            AND T7104.C7104_VOID_FL                 IS NULL
		            AND T526.C526_VOID_FL                   IS NULL
		            AND T526.C205_PART_NUMBER_ID            IS NOT NULL
		            AND T526.C526_PRODUCT_REQUEST_DETAIL_ID IN
		            (
		                 SELECT token FROM v_in_list WHERE token IS NOT NULL   --To avoid the NULL value from in_list.
		            )
		    )
		    CASE, T101_USER T101
		    WHERE T101.C101_USER_ID = NVL (CASE.SHEET_OWNER, CASE.CREATED_BY)
		    ORDER BY DECODE(p_status, '2', AD_ID,'' ),REP_ID; --When the status is 2,to send Ad notification order by AD_ID,REP_ID. else order by REP_ID.
	
	END gm_fch_ad_aprvl_request_info;
	--   
    /*******************************************************
	   * Description : This procedure will get the required information that is used for sending out the email, 
	   				   when the Item Request is approved by PD.
	   * Author 	 : Anilkumar
	*******************************************************/
	PROCEDURE gm_fch_pd_aprvl_request_info (
		p_input_str	  IN	VARCHAR2
	  , p_out_cur	  OUT 	TYPES.cursor_type
	)AS 
	BEGIN
		-- putting req ids in vinlist. 
		my_context.set_my_inlist_ctx (p_input_str) ;
		
		OPEN p_out_cur
		FOR
		 SELECT t520.c520_request_id RQID
		  -- DECODE(t520.c520_status_fl,'15','Pending Control',10,'Back Order','Shipped')STATUS,
		  ,DECODE(t520.c520_status_fl ,'15', 'Approved',gm_pkg_op_request_summary.get_request_status (t520.c520_status_fl)) status
		  , t520.c520_ref_id par_reqid,
		    t526.c526_product_request_detail_id req_id, t521.c521_qty splitaprovedqty, t526.c526_qty_requested approved_qty
		  , t521.c205_part_number_id pnum, C205_PART_NUM_DESC pdesc, DECODE (get_rep_id_from_user_id (t525.c525_request_by_id),
		    NULL, get_user_emailid (t525.c525_request_by_id), get_party_primary_contact (get_rep_party_id (
		    get_rep_id_from_user_id (t525.c525_request_by_id)), 90452)) rep_email, DECODE (t525.c703_sales_rep_id, NULL,
		    t525.c525_request_by_id, t525.c703_sales_rep_id) rep_id, GET_USER_NAME (T526.C526_APPROVE_REJECT_BY) approved_by,
		    t520.c520_last_updated_date lupdt, t526.c526_last_updated_date upddate, t520.c520_created_date crdt
		    ,get_latest_log_comments(get_req_case_id(t520.c520_ref_id),'4000760') comments,
		    get_user_emailid(t526.c526_last_updated_by)user_mailid
		-- ,t521.c205_part_number_id backordpnum
		   FROM t520_request t520, t521_request_detail t521, t525_product_request t525
		  , t526_product_request_detail t526, t205_part_number t205
		  WHERE t520.c520_ref_id             = t525.c525_product_request_id
		    AND t520.c520_request_id         = t521.c520_request_id
		    AND t521.c205_part_number_id     = t205.c205_part_number_id
		    AND t525.c525_product_request_id = t526.c525_product_request_id
		    AND t521.c205_part_number_id     = t526.c205_part_number_id
		 -- AND t526.c205_part_number_id = t205.c205_part_number_id
		    AND t520.c520_void_fl IS NULL
		    AND t525.c525_void_fl IS NULL
		    AND t526.c526_void_fl IS NULL
		    AND T526.C526_PRODUCT_REQUEST_DETAIL_ID IN (SELECT token FROM v_in_list WHERE token IS NOT NULL)
		  --AND t526.c526_last_updated_date         <= nvl(t520.c520_last_updated_date,t520.c520_created_date)
		  UNION
		 SELECT t520.c520_request_id RQID
		    -- DECODE(t520.c520_status_fl,'15','Pending Control',10,'Back Order','Shipped')STATUS,
		  , DECODE(t520.c520_status_fl ,'15', 'Approved',gm_pkg_op_request_summary.get_request_status (t520.c520_status_fl)) status
		  , t520.c520_ref_id par_reqid,
		    t526.c526_product_request_detail_id req_id, t505.C505_ITEM_QTY splitaprovedqty, t526.c526_qty_requested
		    approved_qty, t505.c205_part_number_id pnum, t205.C205_PART_NUM_DESC pdesc
		  , DECODE (get_rep_id_from_user_id (t525.c525_request_by_id), NULL, get_user_emailid (t525.c525_request_by_id),
		    get_party_primary_contact (get_rep_party_id (get_rep_id_from_user_id (t525.c525_request_by_id)), 90452)) rep_email,
		    DECODE (t525.c703_sales_rep_id, NULL, t525.c525_request_by_id, t525.c703_sales_rep_id) rep_id, GET_USER_NAME (
		    T526.C526_APPROVE_REJECT_BY) approved_by, t520.c520_last_updated_date lupdt, t526.c526_last_updated_date upddate
		  , t520.c520_created_date crdt, get_latest_log_comments(get_req_case_id(t520.c520_ref_id),'4000760') comments
		  ,get_user_emailid(t526.c526_last_updated_by)user_mailid
	   -- , t521.c205_part_number_id backordpnum
		   FROM t520_request t520, T504_CONSIGNMENT t504, T505_ITEM_CONSIGNMENT t505
		  , t525_product_request t525, t526_product_request_detail t526, t205_part_number t205
		  WHERE t520.c520_ref_id             = t525.c525_product_request_id
		    AND t520.c520_request_id         = t504.c520_request_id
		    AND t504.c504_consignment_id     = t504.c504_consignment_id
		    AND t504.c504_consignment_id     = t505.c504_consignment_id
		    AND t505.c205_part_number_id     = t526.c205_part_number_id
		    AND t505.c205_part_number_id     = t205.c205_part_number_id
		    AND t526.c205_part_number_id     = t205.c205_part_number_id
		    AND t525.c525_product_request_id = t526.c525_product_request_id
		 -- AND t526.c205_part_number_id = t205.c205_part_number_id
		    AND t504.c504_void_fl IS NULL
		    AND t505.C505_VOID_FL IS NULL
		    AND t520.c520_void_fl IS NULL
		    AND t525.c525_void_fl IS NULL
		    AND t526.c526_void_fl IS NULL
		 -- AND t205.c205_void_fl           is null
		    AND T526.C526_PRODUCT_REQUEST_DETAIL_ID IN (SELECT token FROM v_in_list WHERE token IS NOT NULL)
		 -- AND t526.c526_last_updated_date         <= nvl(t520.c520_last_updated_date,t520.c520_created_date)
		ORDER BY REP_ID;
	
	END gm_fch_pd_aprvl_request_info;
	-- 
	/******************************************************************
	* Description : Procedure to fetch the updated case with total count
	****************************************************************/	
	PROCEDURE gm_fch_case_sync_upd(
		p_token  		IN 		t101a_user_token.c101a_token_id%TYPE,
		p_lang_id    	IN  	T9151_DEVICE_SYNC_DTL.C901_LANGUAGE_ID%TYPE,
		p_uuid   		IN 		t9150_device_info.c9150_device_uuid%TYPE,
		p_user_id		IN 		t101_user.c101_user_id%TYPE,
		p_data_cur		OUT		TYPES.CURSOR_TYPE
	)AS	
		v_device_infoid  t9150_device_info.c9150_device_info_id%TYPE;
		v_case_sync_dt DATE;
		v_case_attr_sync_dt DATE;
	BEGIN
		
		v_device_infoid := get_device_id(p_token, p_uuid) ;
		--Get the  last sync date for case sync type.If last sync date is null then consider as master sync otherwise update sync
		v_case_sync_dt := gm_pkg_pdpc_prodcatrpt.get_last_sync_date(v_device_infoid,p_lang_id,'4000533'); -- Case Sync type
		v_case_attr_sync_dt := gm_pkg_pdpc_prodcatrpt.get_last_sync_date(v_device_infoid,p_lang_id,'4000538'); -- Case Attribute Sync type
		    
		  OPEN p_data_cur
			FOR
			SELECT COUNT(refid) totalsize, reftype FROM
				(
				 SELECT t7100.c7100_case_info_id refid, 4000533 reftype
				   FROM t7100_case_information t7100
				  WHERE t7100.c901_type = 1006503 --Case
				    AND t7100.c901_case_status IN (SELECT c906_rule_value
													 FROM t906_rules
												    WHERE c906_rule_id   = 'CASE_STATUS'
													  AND c906_rule_grp_id ='PROD_CAT_SYNC'
													  AND c906_void_fl IS NULL)
				    AND (C7100_CASE_INFO_ID IN (--Staff Covering/Watcher
		                 SELECT t7100.c7100_case_info_id 
						   FROM t7100_case_information t7100, t7102_case_attribute t7102
						  WHERE t7100.c7100_case_info_id  = t7102.c7100_case_info_id
						    AND T7102.C901_ATTRIBUTE_TYPE IN (103646,103647) --103646 - Staff Covering, 103647 - Watcher
						    AND T7102.C7102_ATTRIBUTE_VALUE = TO_CHAR(p_user_id))
				     OR T7100.C7100_CREATED_BY = p_user_id) --created by
				    AND (v_case_sync_dt IS NULL 
				     OR NVL (t7100.c7100_last_updated_date, t7100.c7100_created_date) > v_case_sync_dt)
              UNION ALL
                 SELECT  c7100_case_info_id refid, 4000538 reftype 
                   FROM T7102_CASE_ATTRIBUTE 
                  WHERE C7100_CASE_INFO_ID IN (
                         --fetch case by Staff Covering/Watcher
		                 SELECT t7100.c7100_case_info_id 
						   FROM t7100_case_information t7100, t7102_case_attribute t7102
						  WHERE t7100.c7100_case_info_id  = t7102.c7100_case_info_id
						    AND t7100.c901_type = 1006503 --Case
						    AND t7100.c901_case_status IN (SELECT c906_rule_value
														     FROM t906_rules
														     WHERE c906_rule_id   = 'CASE_STATUS'
														     AND c906_rule_grp_id ='PROD_CAT_SYNC'
														     AND c906_void_fl IS NULL)						    
						    AND T7102.C901_ATTRIBUTE_TYPE IN (103646,103647) --103646 - Staff Covering, 103647 - Watcher
						    AND T7102.C7102_ATTRIBUTE_VALUE = TO_CHAR(p_user_id)
						    AND (v_case_attr_sync_dt IS NULL 
				             OR NVL (t7100.c7100_last_updated_date, t7100.c7100_created_date) > v_case_attr_sync_dt)
				          UNION
				          -- fetch case by created by
			             SELECT t7100.c7100_case_info_id refid
						   FROM T7100_CASE_INFORMATION T7100
						  WHERE T7100.C7100_CREATED_BY = p_user_id
						    AND t7100.c901_type = 1006503 --Case
						    AND t7100.c901_case_status IN (SELECT c906_rule_value
														     FROM t906_rules
														     WHERE c906_rule_id   = 'CASE_STATUS'
														     AND c906_rule_grp_id ='PROD_CAT_SYNC'
														     AND c906_void_fl IS NULL)
				   			AND (v_case_attr_sync_dt IS NULL 
				             OR NVL (t7100.c7100_last_updated_date, t7100.c7100_created_date) > v_case_attr_sync_dt)          
				             )				     
			    )
			    GROUP BY reftype;
	
	END gm_fch_case_sync_upd;
	/*****************************************************************************************************
	 * Description :  This procedure used for web service which is used to fetch case info id from case information table 
	   where The web service userid is part of staff covering or watcher for that case.
	 ***************************************************************************************************/	
	PROCEDURE gm_fch_case_sync(
		p_user_id			IN 		t101_user.c101_user_id%TYPE,
		p_last_sync_date 	IN 		DATE
	)AS	
	BEGIN
		
		DELETE FROM my_temp_prod_cat_info;
		
		INSERT INTO my_temp_prod_cat_info(ref_id,void_fl)		    
			 SELECT t7100.c7100_case_info_id refid, t7102.c7102_void_fl voidfl
			   FROM t7100_case_information t7100, t7102_case_attribute t7102
			  WHERE t7100.c7100_case_info_id = t7102.c7100_case_info_id
			    AND t7100.c901_type = 1006503 --Case
				AND t7100.c901_case_status IN (SELECT c906_rule_value
													 FROM t906_rules
												    WHERE c906_rule_id   = 'CASE_STATUS'
													  AND c906_rule_grp_id ='PROD_CAT_SYNC'
													  AND c906_void_fl IS NULL)
			    AND t7102.c901_attribute_type IN (103646,103647) --103646 - Staff Covering, 103647 - Watcher
			    AND t7102.c7102_attribute_value = TO_CHAR(p_user_id)
			    AND (p_last_sync_date IS NULL OR NVL (t7100.c7100_last_updated_date, t7100.c7100_created_date) > p_last_sync_date)
			  UNION
             SELECT t7100.c7100_case_info_id refid, t7100.c7100_void_fl voidfl
			   FROM T7100_CASE_INFORMATION T7100
			  WHERE T7100.C7100_CREATED_BY = p_user_id
			    AND t7100.c901_type = 1006503 --Case
				AND t7100.c901_case_status IN (SELECT c906_rule_value
													 FROM t906_rules
												    WHERE c906_rule_id   = 'CASE_STATUS'
													  AND c906_rule_grp_id ='PROD_CAT_SYNC'
													  AND c906_void_fl IS NULL)
			    AND (p_last_sync_date IS NULL OR NVL (t7100.c7100_last_updated_date, t7100.c7100_created_date) > p_last_sync_date);
	
	END gm_fch_case_sync;
	/******************************************************************
	* Description : Procedure to fetch the case detail for the device
	****************************************************************/		
	PROCEDURE gm_fch_case_sync(
	    p_token_id    	 IN  	t101a_user_token.c101a_token_id%TYPE,
	    p_language_id	 IN  	t2001_master_data_updates.c901_language_id%TYPE,
	    p_page_no     	 IN  	NUMBER,
	    p_uuid        	 IN  	t9150_device_info.c9150_device_uuid%TYPE,
	    p_user_id		 IN 	t101_user.c101_user_id%TYPE,
	    p_case_cur 	     OUT 	TYPES.cursor_type 
	)AS
		v_device_infoid  t9150_device_info.c9150_device_info_id%TYPE;
		v_last_sync_date DATE;
	BEGIN
		
		v_device_infoid := get_device_id(p_token_id, p_uuid);
		--Get the  last sync date for case sync type.If last sync date is null then consider as master sync otherwise update sync
		v_last_sync_date := gm_pkg_pdpc_prodcatrpt.get_last_sync_date(v_device_infoid,p_language_id,'4000533'); -- Case Sync type
		--Updated case info ids are inserted into temp table for the device
		gm_fch_case_sync(p_user_id,v_last_sync_date);
	    
	    gm_fch_case_dtl(p_page_no,v_last_sync_date,p_case_cur); 
	    
	END gm_fch_case_sync;	
	/******************************************************************
	* Description : Procedure to fetch the case attribute detail for the device
	****************************************************************/		
	PROCEDURE gm_fch_case_attr_sync(
	    p_token_id    	 IN  	t101a_user_token.c101a_token_id%TYPE ,
	    p_language_id	 IN  	t2001_master_data_updates.c901_language_id%TYPE,
	    p_page_no     	 IN  	NUMBER ,
	    p_uuid        	 IN  	t9150_device_info.c9150_device_uuid%TYPE,
	    p_user_id		 IN 		t101_user.c101_user_id%TYPE,
	    p_case_attr_cur  OUT 	TYPES.cursor_type 
	)AS
		v_device_infoid  t9150_device_info.c9150_device_info_id%TYPE;
		v_last_sync_date DATE;
	BEGIN
		
		v_device_infoid := get_device_id(p_token_id, p_uuid);
		--Get the  last sync date for case Attribute sync type.If last sync date is null then consider as master sync otherwise update sync
		v_last_sync_date := gm_pkg_pdpc_prodcatrpt.get_last_sync_date(v_device_infoid,p_language_id,'4000538'); -- Case Attribute Sync type
		--Updated case info ids are inserted into temp table for the device
		gm_fch_case_sync(p_user_id,v_last_sync_date);
	    
	    gm_fch_case_attr_dtl(p_page_no,v_last_sync_date,p_case_attr_cur); 
	    
	END gm_fch_case_attr_sync;	
	/******************************************************************
	* Description : Procedure to fetch the case detail detail for the device
	****************************************************************/	
	PROCEDURE gm_fch_case_dtl(
		p_pageno        IN     NUMBER,
		p_last_sync_date IN 		DATE,
		p_case_cur		OUT		TYPES.CURSOR_TYPE
	)AS
		v_start          NUMBER;
		v_end            NUMBER;
		v_page_size      NUMBER;
		v_page_no        NUMBER;
		v_date_fmt		 VARCHAR2(20);
    BEGIN
	    v_page_no       := p_pageno;
	    v_date_fmt 		:= GET_RULE_VALUE('DATEFMT', 'DATEFORMAT');
	    
	    -- get the paging details
		gm_pkg_pdpc_prodcatrpt.gm_fch_paging_dtl('CASE','PAGESIZE',v_page_no,v_start,v_end,v_page_size);
		
		OPEN p_case_cur
			FOR	
			 SELECT RESULT_COUNT totalsize,  COUNT (1) OVER () pagesize, v_page_no pageno, rwnum, CEIL (RESULT_COUNT / v_page_size) totalpages, t7100.*  
			   FROM
			    (
			         SELECT t7100.*, ROWNUM rwnum, COUNT (1) OVER () result_count
			           FROM
			            (  			
						 SELECT t7100.c7100_case_info_id caseinfoid, t7100.c7100_case_id caseid
						  , TO_CHAR(t7100.c7100_surgery_date,v_date_fmt) surgerydt
						  , TO_CHAR(t7100.c7100_surgery_time,v_date_fmt||' HH24:MI:SS TZH:TZM') surgerytime
						  , t7100.c701_distributor_id distid, t7100.c703_sales_rep_id repid
						  , t7100.c704_account_id accountid, t7100.c7100_personal_notes pnotes, t7100.c901_type type
						  , t7100.c901_case_status status, t7100.c7100_parent_case_info_id prntcaseinfoid, t7100.c7100_created_by createdby
						  , NVL (temp.void_fl,t7100.c7100_void_fl) voidfl
						   FROM t7100_case_information t7100, my_temp_prod_cat_info temp
						  WHERE T7100.c7100_case_info_id = temp.ref_id
						    AND (p_last_sync_date IS NOT NULL OR (p_last_sync_date IS NULL AND t7100.c7100_void_fl IS NULL))
						)
			            t7100
			    )
			    t7100
			  WHERE rwnum BETWEEN v_start AND v_end;
			  
	END gm_fch_case_dtl;
	/******************************************************************
	* Description : Procedure to fetch the case attribute detail for the device
	****************************************************************/		
	PROCEDURE gm_fch_case_attr_dtl(
		p_pageno        IN     NUMBER,
		p_last_sync_date IN 		DATE,
		p_case_cur		OUT		TYPES.CURSOR_TYPE
	)AS
		v_start          NUMBER;
		v_end            NUMBER;
		v_page_size      NUMBER;
		v_page_no        NUMBER;
    BEGIN
	    v_page_no       := p_pageno;
	    
	    -- get the paging details
		gm_pkg_pdpc_prodcatrpt.gm_fch_paging_dtl('CASE_ATTR','PAGESIZE',v_page_no,v_start,v_end,v_page_size);
		
		OPEN p_case_cur
			FOR	
			 SELECT RESULT_COUNT totalsize,  COUNT (1) OVER () pagesize, v_page_no pageno, rwnum, CEIL (RESULT_COUNT / v_page_size) totalpages, t7102.*  
			   FROM
			    (
			         SELECT t7102.*, ROWNUM rwnum, COUNT (1) OVER () result_count
			           FROM
			            (  	
						 SELECT t7102.c7100_case_info_id caseinfoid, t7102.c7102_case_attribute_id caseattrid, t7102.c901_attribute_type
						    attrtype, t7102.c7102_attribute_value attrvalue, t7102.c7102_void_fl voidfl
						   FROM t7102_case_attribute t7102, my_temp_prod_cat_info temp
						  WHERE t7102.c7100_case_info_id = temp.ref_id 
						   AND (p_last_sync_date IS NOT NULL OR (p_last_sync_date IS NULL AND t7102.c7102_void_fl IS NULL))
						)
			            t7102
			    )
			    t7102
			  WHERE rwnum BETWEEN v_start AND v_end;
			  
	END gm_fch_case_attr_dtl;	
END gm_pkg_sm_casebook_rpt;
/
