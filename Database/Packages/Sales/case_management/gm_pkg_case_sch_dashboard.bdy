--@"C:\Database\Packages\sales\case_management\gm_pkg_case_sch_dashboard.bdy";
CREATE OR REPLACE PACKAGE BODY gm_pkg_case_sch_dashboard
IS
--
 
/* ***********************************************************************************************
 * Author: Agilan singaravelan
 * Procedure:gm_fetch_kit_ship_dashboard
 * Description:Procedure to fetch shipping details of overdue, today and tommorrow values for kit
 *************************************************************************************************/
   PROCEDURE gm_fetch_kit_ship_dashboard(
  		p_shipping_overdue   OUT  TYPES.cursor_type
  	  , p_shipping_today     OUT  TYPES.cursor_type
  	  , p_shipping_tomorrow  OUT  TYPES.cursor_type
  )
  AS
   v_datefmt      VARCHAR2(50);
   v_company_id   T1900_COMPANY.C1900_COMPANY_ID%TYPE;
  BEGIN
	 SELECT get_compdtfmt_frm_cntx(), get_compid_frm_cntx()
      INTO v_datefmt, v_company_id
      FROM DUAL;
 
	   OPEN p_shipping_overdue
           FOR
	  SELECT count(t7105.c2078_kit_map_id) ShipOverdueCnt, TO_char(CURRENT_DATE -90, v_datefmt) STARTDATE , TO_char(CURRENT_DATE-1, v_datefmt) ENDDATE , '107301' TYPE --t7100.c7100_case_info_id
		FROM t7100_case_information t7100, t7105_case_schedular_dtl t7105
		WHERE t7100.c7100_case_info_id = t7105.c7100_case_info_id  
		and t7100.c901_type        = '107285'
		and t7100.c901_case_status = '107160'  -- Initiated
		AND t7100.C7100_SHIP_DATE < trunc(CURRENT_DATE)
		AND t7100.c7100_void_fl     IS NULL
		AND t7105.c7105_void_fl     IS NULL
		AND t7100.c1900_company_id = v_company_id;
		
	  OPEN p_shipping_today
           FOR	
           SELECT count(t7105.c2078_kit_map_id) ShipOverdueCnt ,TO_char(CURRENT_DATE, v_datefmt) STARTDATE ,TO_char(CURRENT_DATE, v_datefmt) ENDDATE , '107301' TYPE --t7100.c7100_case_info_id
		FROM t7100_case_information t7100, t7105_case_schedular_dtl t7105
		WHERE t7100.c7100_case_info_id = t7105.c7100_case_info_id  
		and t7100.c901_type        = '107285'
		and t7100.c901_case_status = '107160'  -- Initiated
		AND t7100.C7100_SHIP_DATE = trunc(CURRENT_DATE)
		AND t7100.c7100_void_fl     IS NULL
		AND t7105.c7105_void_fl     IS NULL
		AND t7100.c1900_company_id = v_company_id;
	
		OPEN p_shipping_tomorrow
           FOR	
		SELECT count(t7105.c2078_kit_map_id) ShipOverdueCnt , TO_char(CURRENT_DATE+1, v_datefmt) STARTDATE , TO_char(CURRENT_DATE+1, v_datefmt) ENDDATE , '107301' TYPE --t7100.c7100_case_info_id
		FROM t7100_case_information t7100, t7105_case_schedular_dtl t7105
		WHERE t7100.c7100_case_info_id = t7105.c7100_case_info_id  
		and t7100.c901_type        = '107285'
		and t7100.c901_case_status = '107160' -- Initiated
		AND t7100.C7100_SHIP_DATE = trunc(CURRENT_DATE+1)
		AND t7100.c7100_void_fl     IS NULL
		AND t7105.c7105_void_fl     IS NULL
		AND t7100.c1900_company_id = v_company_id;

	 END gm_fetch_kit_ship_dashboard;
	 
 /* *********************************************************************************************
 * Author: Agilan singaravelan
 * Procedure:gm_fetch_kit_Returns_dashboard
 * Description:Procedure to fetch return details of overdue, today and tommorrow values for kit
 ************************************************************************************************/
   PROCEDURE gm_fetch_kit_Returns_dashboard(
  		p_return_overdue   OUT  TYPES.cursor_type
  	  , p_return_today     OUT  TYPES.cursor_type
  	  , p_return_tomorrow  OUT  TYPES.cursor_type
  )
  AS
   v_datefmt      VARCHAR2(50);
   v_company_id   T1900_COMPANY.C1900_COMPANY_ID%TYPE;
  BEGIN
	 SELECT get_compdtfmt_frm_cntx(), get_compid_frm_cntx()
      INTO v_datefmt, v_company_id
      FROM DUAL;
 
	  OPEN p_return_overdue
           FOR
	  SELECT count(t7105.c2078_kit_map_id) ReturnOverdueCnt,TO_char(CURRENT_DATE -90, v_datefmt) STARTDATE , TO_char(CURRENT_DATE-1 , v_datefmt) ENDDATE , '107301' TYPE --t7100.c7100_case_info_id
		FROM t7100_case_information t7100, t7105_case_schedular_dtl t7105
		WHERE t7100.c7100_case_info_id = t7105.c7100_case_info_id  
		and t7100.c901_type        = '107285'
		and t7100.c901_case_status = '107161' -- Shipped
		AND t7100.C7100_RETURN_DATE < trunc(CURRENT_DATE)
		AND t7100.c7100_void_fl     IS NULL
		AND t7105.c7105_void_fl     IS NULL
		AND t7100.c1900_company_id = v_company_id;
  
		OPEN p_return_today
           FOR
	  SELECT count(t7105.c2078_kit_map_id) ReturnTodayCnt,TO_char(CURRENT_DATE , v_datefmt) STARTDATE , TO_char(CURRENT_DATE, v_datefmt) ENDDATE , '107301' TYPE --t7100.c7100_case_info_id
		FROM t7100_case_information t7100, t7105_case_schedular_dtl t7105
		WHERE t7100.c7100_case_info_id = t7105.c7100_case_info_id  
		and t7100.c901_type        = '107285' 
		and t7100.c901_case_status = '107161' ---- Shipped
		AND t7100.C7100_RETURN_DATE = trunc(CURRENT_DATE)
		AND t7100.c7100_void_fl     IS NULL
		AND t7105.c7105_void_fl     IS NULL
		AND t7100.c1900_company_id = v_company_id;

		OPEN p_return_tomorrow
           FOR
		SELECT count(t7105.c2078_kit_map_id) ReturnTomorrowCnt,TO_char(CURRENT_DATE+1 , v_datefmt)  STARTDATE ,TO_char(CURRENT_DATE+1 , v_datefmt)  ENDDATE , '107301' TYPE --t7100.c7100_case_info_id
		FROM t7100_case_information t7100, t7105_case_schedular_dtl t7105
		WHERE t7100.c7100_case_info_id = t7105.c7100_case_info_id  
		and t7100.c901_type        = '107285'
		and t7100.c901_case_status = '107161' -- Shipped
		AND t7100.C7100_RETURN_DATE = trunc(CURRENT_DATE+1)
		AND t7100.c7100_void_fl     IS NULL
		AND t7105.c7105_void_fl     IS NULL
		AND t7100.c1900_company_id = v_company_id;

	  END gm_fetch_kit_Returns_dashboard;
	  
 /* *********************************************************************************************
 * Author: Agilan singaravelan
 * Procedure:gm_fetch_shipping_case_dashboard
 * Description:Procedure to fetch shipping details of overdue, today and tommorrow values for case
 ************************************************************************************************/
   PROCEDURE gm_fetch_shipping_case_dashboard(
  		p_shipping_overdue   OUT  TYPES.cursor_type
  	  , p_shipping_today     OUT  TYPES.cursor_type
  	  , p_shipping_tomorrow  OUT  TYPES.cursor_type
  )
  AS
   v_datefmt      VARCHAR2(50);
   v_company_id   T1900_COMPANY.C1900_COMPANY_ID%TYPE;
  BEGIN
	 SELECT get_compdtfmt_frm_cntx(), get_compid_frm_cntx()
      INTO v_datefmt, v_company_id
      FROM DUAL;
 
	  OPEN p_shipping_overdue
	  FOR
	  SELECT count(t7100.c7100_case_id) ShipOverdueCnt,TO_char(CURRENT_DATE -90 , v_datefmt)  STARTDATE ,TO_char(CURRENT_DATE-1 , v_datefmt)  ENDDATE , '107301' TYPE --t7100.c7100_case_info_id
		FROM t7100_case_information t7100
		WHERE t7100.c901_type        = '107285'
		and t7100.c901_case_status = '107160'  -- Initiated
		AND t7100.C7100_SHIP_DATE < trunc(CURRENT_DATE)
		AND t7100.c7100_void_fl     IS NULL
		AND t7100.c1900_company_id = v_company_id;
 
		OPEN p_shipping_today
	  FOR
		SELECT count(t7100.c7100_case_id) ShipOverdueCnt,TO_char(CURRENT_DATE, v_datefmt) STARTDATE , TO_char(CURRENT_DATE, v_datefmt) ENDDATE , '107301' TYPE --t7100.c7100_case_info_id
		FROM t7100_case_information t7100
		WHERE t7100.c901_type        = '107285'
		and t7100.c901_case_status = '107160'  -- Initiated
		AND t7100.C7100_SHIP_DATE = trunc(CURRENT_DATE)
		AND t7100.c7100_void_fl     IS NULL
		AND t7100.c1900_company_id = v_company_id;

		OPEN p_shipping_tomorrow
	  FOR
		SELECT count(t7100.c7100_case_id) ShipOverdueCnt,TO_char(CURRENT_DATE+1, v_datefmt) STARTDATE , TO_char(CURRENT_DATE+1, v_datefmt) ENDDATE , '107301' TYPE --t7100.c7100_case_info_id
		FROM t7100_case_information t7100
		WHERE t7100.c901_type        = '107285'
		and t7100.c901_case_status = '107160'  -- Initiated
		AND t7100.C7100_SHIP_DATE = trunc(CURRENT_DATE+1)
		AND t7100.c7100_void_fl     IS NULL
		AND t7100.c1900_company_id = v_company_id;
	

	  END gm_fetch_shipping_case_dashboard;
 /* *********************************************************************************************
 * Author: Agilan singaravelan
 * Procedure:gm_fetch_Returns_case_dashboard
 * Description:Procedure to fetch returns details of overdue, today and tommorrow values for case
 ************************************************************************************************/
   PROCEDURE gm_fetch_Returns_case_dashboard(
  		p_return_overdue   OUT  TYPES.cursor_type
  	  , p_return_today     OUT  TYPES.cursor_type
  	  , p_return_tomorrow  OUT  TYPES.cursor_type
  )
  AS
   v_datefmt      VARCHAR2(50);
   v_company_id   T1900_COMPANY.C1900_COMPANY_ID%TYPE;
  BEGIN
	 SELECT get_compdtfmt_frm_cntx(), get_compid_frm_cntx()
      INTO v_datefmt, v_company_id
      FROM DUAL;

	  OPEN p_return_overdue
	  FOR
	  SELECT  count(t7100.c7100_case_id) ReturnOverdueCnt,TO_char(CURRENT_DATE -90, v_datefmt) STARTDATE , TO_char(CURRENT_DATE -1, v_datefmt) ENDDATE , '107301' TYPE --t7100.c7100_case_info_id
		FROM t7100_case_information t7100
		WHERE t7100.c901_type        = '107285'
		and t7100.c901_case_status = '107161'  -- Shipped
		AND t7100.C7100_RETURN_DATE < trunc(CURRENT_DATE)
		AND t7100.c7100_void_fl     IS NULL
		AND t7100.c1900_company_id = v_company_id;

	  OPEN p_return_today
	  FOR
	  SELECT count(t7100.c7100_case_id) ReturnTodayCnt,TO_char(CURRENT_DATE, v_datefmt) STARTDATE , TO_char(CURRENT_DATE, v_datefmt) ENDDATE , '107301' TYPE --t7100.c7100_case_info_id
		FROM t7100_case_information t7100
		WHERE t7100.c901_type        = '107285'
		and t7100.c901_case_status = '107161'  -- Shipped
		AND t7100.C7100_RETURN_DATE = trunc(CURRENT_DATE)
		AND t7100.c7100_void_fl     IS NULL
		AND t7100.c1900_company_id = v_company_id;

		 OPEN p_return_tomorrow
	  FOR
	  SELECT count(t7100.c7100_case_id) ReturnTomorrowCnt, TO_char(CURRENT_DATE +1, v_datefmt) STARTDATE , TO_char(CURRENT_DATE +1, v_datefmt) ENDDATE , '107301' TYPE --t7100.c7100_case_info_id
		FROM t7100_case_information t7100
		WHERE t7100.c901_type        = '107285'
		and t7100.c901_case_status = '107161'  -- Shipped
		AND t7100.C7100_RETURN_DATE = trunc(CURRENT_DATE+1)
		AND t7100.c7100_void_fl     IS NULL
		AND t7100.c1900_company_id = v_company_id;

	  END gm_fetch_Returns_case_dashboard;
 /* **********************************************************************************************************
 * Author: Agilan singaravelan
 * Procedure:gm_fetch_kit_ship_upcoming
 * Description:Procedure to fetch shipping details of upcoming week, this week and this month values for kit
 *************************************************************************************************************/
   PROCEDURE gm_fetch_kit_ship_upcoming(
  		p_return_overdue   OUT  TYPES.cursor_type
  )
  AS
  v_datefmt      VARCHAR2(50);
  v_company_id   T1900_COMPANY.C1900_COMPANY_ID%TYPE;
  v_one_day		 DATE;
  v_seven_day    DATE;
  v_eight_day    DATE;
  v_fourteen_day DATE;
  v_fifteen_day  DATE;
  v_thirty_day   DATE;
  BEGIN
	 SELECT get_compdtfmt_frm_cntx(), get_compid_frm_cntx(), 
	 		gm_pkg_case_sch_dashboard.add_n_working_days(current_date,1),
	 		gm_pkg_case_sch_dashboard.add_n_working_days(current_date,7),
	 		gm_pkg_case_sch_dashboard.add_n_working_days(current_date,8),
	 		gm_pkg_case_sch_dashboard.add_n_working_days(current_date,14),
	 		gm_pkg_case_sch_dashboard.add_n_working_days(current_date,15),
	 		gm_pkg_case_sch_dashboard.add_n_working_days(current_date,30)
       INTO v_datefmt, v_company_id, v_one_day, v_seven_day, v_eight_day, v_fourteen_day, v_fifteen_day,v_thirty_day
       FROM DUAL;
          
    OPEN p_return_overdue
  FOR    
 select * from 
(SELECT count(t7105.c2078_kit_map_id) TW_CNT,TO_char(v_one_day, v_datefmt) TW_STARTDATE , TO_char(v_seven_day, v_datefmt) TW_ENDDATE , '107301' TYPE 
FROM t7100_case_information t7100, t7105_case_schedular_dtl t7105
WHERE t7100.c7100_case_info_id = t7105.c7100_case_info_id  
and t7100.c901_type        = '107285'
and t7100.c901_case_status = '107160'  -- Initiated
AND t7100.C7100_SHIP_DATE between TRUNC(v_one_day) and TRUNC(v_seven_day)
AND t7100.c7100_void_fl     IS NULL
AND t7105.c7105_void_fl     IS NULL
AND t7100.c1900_company_id = v_company_id
)this_week_ship,
(SELECT count(t7105.c2078_kit_map_id) NW_CNT,TO_char(v_eight_day, v_datefmt) NW_STARTDATE ,TO_char(v_fourteen_day, v_datefmt) NW_ENDDATE 
FROM t7100_case_information t7100, t7105_case_schedular_dtl t7105
WHERE t7100.c7100_case_info_id = t7105.c7100_case_info_id  
and t7100.c901_type        = '107285'
and t7100.c901_case_status = '107160'  -- Initiated
AND t7100.C7100_SHIP_DATE between TRUNC(v_eight_day) and TRUNC(v_fourteen_day)
AND t7100.c7100_void_fl     IS NULL
AND t7105.c7105_void_fl     IS NULL
AND t7100.c1900_company_id = v_company_id
)next_week_ship,
(SELECT count(t7105.c2078_kit_map_id) TM_CNT,TO_char(v_fifteen_day, v_datefmt) TM_STARTDATE , TO_char(v_thirty_day, v_datefmt) TM_ENDDATE 
FROM t7100_case_information t7100, t7105_case_schedular_dtl t7105
WHERE t7100.c7100_case_info_id = t7105.c7100_case_info_id  
and t7100.c901_type        = '107285'
and t7100.c901_case_status = '107160'  -- Initiated
AND t7100.C7100_SHIP_DATE between TRUNC(v_fifteen_day) and TRUNC(v_thirty_day)
AND t7100.c7100_void_fl     IS NULL
AND t7105.c7105_void_fl     IS NULL
AND t7100.c1900_company_id = v_company_id
)this_month_ship; 

  END gm_fetch_kit_ship_upcoming;
 /* **********************************************************************************************************
 * Author: Agilan singaravelan
 * Procedure:gm_fetch_kit_ret_upcoming
 * Description:Procedure to fetch returns details of upcoming week, this week and this month values for kit
 *************************************************************************************************************/
   PROCEDURE gm_fetch_kit_ret_upcoming(
  		p_return_overdue   OUT  TYPES.cursor_type
  )
  AS
  v_datefmt      VARCHAR2(50);
  v_company_id   T1900_COMPANY.C1900_COMPANY_ID%TYPE;
  v_one_day		 DATE;
  v_seven_day    DATE;
  v_eight_day    DATE;
  v_fourteen_day DATE;
  v_fifteen_day  DATE;
  v_thirty_day   DATE;
  BEGIN
	 SELECT get_compdtfmt_frm_cntx(), get_compid_frm_cntx(), 
	 		gm_pkg_case_sch_dashboard.add_n_working_days(current_date,1),
	 		gm_pkg_case_sch_dashboard.add_n_working_days(current_date,7),
	 		gm_pkg_case_sch_dashboard.add_n_working_days(current_date,8),
	 		gm_pkg_case_sch_dashboard.add_n_working_days(current_date,14),
	 		gm_pkg_case_sch_dashboard.add_n_working_days(current_date,15),
	 		gm_pkg_case_sch_dashboard.add_n_working_days(current_date,30)
       INTO v_datefmt, v_company_id, v_one_day, v_seven_day, v_eight_day, v_fourteen_day, v_fifteen_day,v_thirty_day
       FROM DUAL;
  OPEN p_return_overdue
  FOR
select * from 
(SELECT count(t7105.c2078_kit_map_id) TW_CNT,TO_char(v_one_day, v_datefmt) TW_STARTDATE , TO_char(v_seven_day, v_datefmt) TW_ENDDATE , '107302' TYPE 
FROM t7100_case_information t7100, t7105_case_schedular_dtl t7105
WHERE t7100.c7100_case_info_id = t7105.c7100_case_info_id  
and t7100.c901_type        = '107285'
and t7100.c901_case_status = '107161'  -- Shipped
AND t7100.C7100_RETURN_DATE between TRUNC(v_one_day) and TRUNC(v_seven_day)
AND t7100.c7100_void_fl     IS NULL
AND t7105.c7105_void_fl     IS NULL
AND t7100.c1900_company_id = v_company_id
)this_week_return,
(SELECT count(t7105.c2078_kit_map_id) NW_CNT,TO_char(v_eight_day, v_datefmt) NW_STARTDATE , TO_char(v_fourteen_day, v_datefmt) NW_ENDDATE 
FROM t7100_case_information t7100, t7105_case_schedular_dtl t7105
WHERE t7100.c7100_case_info_id = t7105.c7100_case_info_id  
and t7100.c901_type        = '107285'
and t7100.c901_case_status = '107161'  -- Shipped
AND t7100.C7100_RETURN_DATE between TRUNC(v_eight_day) and TRUNC(v_fourteen_day)
AND t7100.c7100_void_fl     IS NULL
AND t7105.c7105_void_fl     IS NULL
AND t7100.c1900_company_id = v_company_id
)next_week_return,
(SELECT count(t7105.c2078_kit_map_id) TM_CNT,TO_char(v_fifteen_day, v_datefmt) TM_STARTDATE , TO_char(v_thirty_day, v_datefmt) TM_ENDDATE 
FROM t7100_case_information t7100, t7105_case_schedular_dtl t7105
WHERE t7100.c7100_case_info_id = t7105.c7100_case_info_id  
and t7100.c901_type        = '107285'
and t7100.c901_case_status = '107161'  -- Shipped
AND t7100.C7100_RETURN_DATE between TRUNC(v_fifteen_day) and TRUNC(v_thirty_day)
AND t7100.c7100_void_fl     IS NULL
AND t7105.c7105_void_fl     IS NULL
AND t7100.c1900_company_id = v_company_id
)this_month_return; 

  END gm_fetch_kit_ret_upcoming;  
 /* **********************************************************************************************************
 * Author: Agilan singaravelan
 * Procedure:gm_fetch_case_ship_upcoming
 * Description:Procedure to fetch shipping details of upcoming week, this week and this month values for case
 *************************************************************************************************************/
   PROCEDURE gm_fetch_case_ship_upcoming(
  		p_return_overdue   OUT  TYPES.cursor_type
  )
  AS
  v_datefmt      VARCHAR2(50);
  v_company_id   T1900_COMPANY.C1900_COMPANY_ID%TYPE;
  v_one_day		 DATE;
  v_seven_day    DATE;
  v_eight_day    DATE;
  v_fourteen_day DATE;
  v_fifteen_day  DATE;
  v_thirty_day   DATE;
  BEGIN
	 SELECT get_compdtfmt_frm_cntx(), get_compid_frm_cntx(), 
	 		gm_pkg_case_sch_dashboard.add_n_working_days(current_date,1),
	 		gm_pkg_case_sch_dashboard.add_n_working_days(current_date,7),
	 		gm_pkg_case_sch_dashboard.add_n_working_days(current_date,8),
	 		gm_pkg_case_sch_dashboard.add_n_working_days(current_date,14),
	 		gm_pkg_case_sch_dashboard.add_n_working_days(current_date,15),
	 		gm_pkg_case_sch_dashboard.add_n_working_days(current_date,30)
       INTO v_datefmt, v_company_id, v_one_day, v_seven_day, v_eight_day, v_fourteen_day, v_fifteen_day,v_thirty_day
       FROM DUAL;
    OPEN p_return_overdue
  FOR    
  select * from 
(SELECT count(t7100.c7100_case_id) TW_CNT,TO_char(v_one_day, v_datefmt) TW_STARTDATE , TO_char(v_seven_day, v_datefmt) TW_ENDDATE , '107301' TYPE
FROM t7100_case_information t7100
WHERE t7100.c901_type        = '107285'
and t7100.c901_case_status = '107160'  -- Initiated
AND  t7100.C7100_SHIP_DATE between TRUNC(v_one_day) and TRUNC(v_seven_day)
AND t7100.c7100_void_fl     IS NULL
AND t7100.c1900_company_id = v_company_id
)this_week_ship,
(SELECT count(t7100.c7100_case_id) NW_CNT,TO_char(v_eight_day, v_datefmt) NW_STARTDATE ,TO_char(v_fourteen_day, v_datefmt)  NW_ENDDATE 
FROM t7100_case_information t7100
WHERE t7100.c901_type        = '107285'
and t7100.c901_case_status = '107160'  -- Initiated
AND  t7100.C7100_SHIP_DATE between TRUNC(v_eight_day) and TRUNC(v_fourteen_day)
AND t7100.c7100_void_fl     IS NULL
AND t7100.c1900_company_id = v_company_id
)next_week_ship,
(SELECT count(t7100.c7100_case_id) TM_CNT,TO_char(v_fifteen_day, v_datefmt) TM_STARTDATE , TO_char(v_thirty_day, v_datefmt) TM_ENDDATE 
FROM t7100_case_information t7100
WHERE t7100.c901_type        = '107285'
and t7100.c901_case_status = '107160'  -- Initiated
AND  t7100.C7100_SHIP_DATE between TRUNC(v_fifteen_day) and TRUNC(v_thirty_day)
AND t7100.c7100_void_fl     IS NULL
AND t7100.c1900_company_id = v_company_id
)this_month_ship;

  END gm_fetch_case_ship_upcoming;
 /* **********************************************************************************************************
 * Author: Agilan singaravelan
 * Procedure:gm_fetch_case_ret_upcoming
 * Description:Procedure to fetch returns details of upcoming week, this week and this month values for case
 *************************************************************************************************************/
   PROCEDURE gm_fetch_case_ret_upcoming(
  		p_return_overdue   OUT  TYPES.cursor_type
  )
  AS
  v_datefmt      VARCHAR2(50);
  v_company_id   T1900_COMPANY.C1900_COMPANY_ID%TYPE;
  v_one_day		 DATE;
  v_seven_day    DATE;
  v_eight_day    DATE;
  v_fourteen_day DATE;
  v_fifteen_day  DATE;
  v_thirty_day   DATE;
  BEGIN
	 SELECT get_compdtfmt_frm_cntx(), get_compid_frm_cntx(), 
	 		gm_pkg_case_sch_dashboard.add_n_working_days(current_date,1),
	 		gm_pkg_case_sch_dashboard.add_n_working_days(current_date,7),
	 		gm_pkg_case_sch_dashboard.add_n_working_days(current_date,8),
	 		gm_pkg_case_sch_dashboard.add_n_working_days(current_date,14),
	 		gm_pkg_case_sch_dashboard.add_n_working_days(current_date,15),
	 		gm_pkg_case_sch_dashboard.add_n_working_days(current_date,30)
       INTO v_datefmt, v_company_id, v_one_day, v_seven_day, v_eight_day, v_fourteen_day, v_fifteen_day,v_thirty_day
       FROM DUAL;
  OPEN p_return_overdue
  FOR
select * from 
(SELECT count(t7100.c7100_case_id) TW_CNT,TO_char(v_one_day, v_datefmt) TW_STARTDATE , TO_char(v_seven_day, v_datefmt) TW_ENDDATE , '107302' TYPE
FROM t7100_case_information t7100
WHERE t7100.c901_type        = '107285'
and t7100.c901_case_status = '107161'  -- Shipped
AND  t7100.C7100_RETURN_DATE between TRUNC(v_one_day) and TRUNC(v_seven_day)
AND t7100.c7100_void_fl     IS NULL
AND t7100.c1900_company_id = v_company_id
)this_week_return,
(SELECT count(t7100.c7100_case_id) NW_CNT,TO_char(v_eight_day, v_datefmt) NW_STARTDATE , TO_char(v_fourteen_day, v_datefmt) NW_ENDDATE 
FROM t7100_case_information t7100
WHERE t7100.c901_type        = '107285'
and t7100.c901_case_status = '107161'  -- Shipped
AND  t7100.C7100_RETURN_DATE between TRUNC(v_eight_day) and TRUNC(v_fourteen_day)
AND t7100.c7100_void_fl     IS NULL
AND t7100.c1900_company_id = v_company_id
)next_week_return,
(SELECT count(t7100.c7100_case_id) TM_CNT,TO_char(v_fifteen_day, v_datefmt) TM_STARTDATE ,TO_char(v_thirty_day, v_datefmt) TM_ENDDATE 
FROM t7100_case_information t7100
WHERE t7100.c901_type        = '107285'
and t7100.c901_case_status = '107161'  -- Shipped
AND  t7100.C7100_RETURN_DATE between TRUNC(v_fifteen_day) and TRUNC(v_thirty_day)
AND t7100.c7100_void_fl     IS NULL
AND t7100.c1900_company_id = v_company_id
)this_month_return;

END gm_fetch_case_ret_upcoming;
/******************************************************************************************
	* Description :  This Function returns the working day based on start_date and count.
*******************************************************************************************/
function add_n_working_days ( 
  start_date date, 
  working_days pls_integer
) return date 
as
  end_date       date;
  counter        pls_integer := 0;
  remaining_days pls_integer;
  weeks          pls_integer;
begin
   
  if working_days = 0 then
    end_date := start_date;
  elsif to_char(start_date, 'fmdy') in ('sat', 'sun') then
    if sign(working_days) = 1 then
      end_date := next_day(start_date, 'monday');
    else
      end_date := next_day(start_date-7, 'friday');
    end if;
  else
    end_date := start_date;
  end if;
   
  if abs(working_days) <= 5 then
    remaining_days := working_days;
  else
    weeks := floor ( abs(working_days) / 5 ) * sign(working_days);
    end_date := end_date + ( weeks * 7 );
    remaining_days := mod ( working_days, 5 );
  end if;
   
  while (counter < abs(remaining_days)) loop
    end_date := end_date + sign(working_days);
    if to_char(end_date, 'fmdy') not in ('sat', 'sun') then
      counter := counter + 1;
    end if;
  end loop;
 
  return end_date;
 
end add_n_working_days;


  END;
/