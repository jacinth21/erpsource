CREATE OR REPLACE
PACKAGE BODY gm_pkg_case_txn
IS

/* *******************************************************************************
 * Author: Agilan singaravelan
 * Procedure:gm_sav_case_dtl
 * Description:Procedure to save Case schedular details
 ***********************************************************************************/ 
	PROCEDURE gm_sav_case_dtl (
	
		 p_case_id 	  	  IN  	   t7100_case_information.c7100_case_id%TYPE	
	    ,p_surgery_dt	  IN	   varchar2
	 	,p_start_time 	  IN       varchar2
  		,p_end_time 	  IN	   varchar2
  		,p_account_id	  IN	   t7100_case_information.c704_account_id%TYPE
	    ,p_surg_lvl       IN	   T7100_CASE_INFORMATION.C901_SURGERY_LEVEL%TYPE
  		,p_surg_type      IN       T7100_CASE_INFORMATION.C901_SURGERY_TYPE%TYPE
  		,p_notes		  IN       T7100_CASE_INFORMATION.C7100_PERSONAL_NOTES%TYPE
  		,p_userid		  IN	   t7100_case_information.c7100_created_by%TYPE
  		,p_case_info_id   IN OUT   t7100_case_information.c7100_case_info_id%type
  		,p_party_id       IN       T7100_CASE_INFORMATION.C7100_OWNER_PARTY_ID%TYPE
  		,p_comp_id 		  IN       T7100_CASE_INFORMATION.C1900_COMPANY_ID%TYPE
  		,p_comp_fmt		  IN       varchar2
	)
	
	AS
	
	 v_company_id    t7100_case_information.C1900_COMPANY_ID%TYPE;
	 v_count         NUMBER;
	 	 
	BEGIN
		
	    UPDATE T7100_CASE_INFORMATION
		   SET C7100_CASE_ID = p_case_id,
		   	   C7100_SURGERY_DATE = TO_DATE(p_surgery_dt,p_comp_fmt),
		   	   C703_SALES_REP_ID = p_userid,
		   	   C7100_SURGERY_TIME = to_timestamp_tz(p_surgery_dt||' '||p_start_time,p_comp_fmt||'HH24:MI:SS TZH:TZM'),
		   	   C7100_SURGERY_END_TIME = to_timestamp_tz(p_surgery_dt||' '||p_end_time,p_comp_fmt||'HH24:MI:SS TZH:TZM'),
		   	   C901_SURGERY_LEVEL = p_surg_lvl,
		   	   C901_SURGERY_TYPE = p_surg_type,
		   	   C7100_PERSONAL_NOTES = p_notes,
		   	   C704_ACCOUNT_ID = DECODE (p_account_id, 0, NULL, p_account_id),
		   	   C901_CASE_STATUS = 107160,  --we need to change
		   	   C7100_LAST_UPDATED_BY = p_userid,
		   	   C7100_LAST_UPDATED_DATE = current_date
		 WHERE C7100_CASE_INFO_ID = p_case_info_id
		   AND C7100_VOID_FL is null;
	
	 IF (SQL%ROWCOUNT            = 0) THEN
	 
	   	p_case_info_id := s7100_case_information.NEXTVAL;
	   
		INSERT INTO T7100_CASE_INFORMATION (c7100_case_info_id, c7100_case_id, c7100_surgery_date,c703_sales_rep_id
					   , C7100_SURGERY_TIME,C7100_SURGERY_END_TIME, C901_SURGERY_LEVEL, C901_SURGERY_TYPE, C7100_PERSONAL_NOTES 
					   , c704_account_id, c7100_created_by, c7100_created_date,  c901_case_status, C901_Type,c1900_company_id
					   , c7100_do_flag,c7100_loaner_flag,c7100_owner_party_id
						)
				 VALUES (p_case_info_id, p_case_id, TO_DATE(p_surgery_dt,p_comp_fmt),p_userid
					   ,to_timestamp_tz(p_surgery_dt||' '||p_start_time,p_comp_fmt||'HH24:MI:SS TZH:TZM'), to_timestamp_tz(p_surgery_dt||' '||p_end_time,p_comp_fmt||'HH24:MI:SS TZH:TZM'), p_surg_lvl, p_surg_type
					   , p_notes , DECODE (p_account_id, 0, NULL, p_account_id), p_userid, CURRENT_DATE, '107160', 107287, p_comp_id
					   , 'N','N',p_party_id
						); 	
	   	   
	END IF;	   
		   
	END gm_sav_case_dtl;
	
/* *******************************************************************************
 * Author: Agilan singaravelan
 * Procedure:gm_sav_case_surgeon_dtl
 * Description:Procedure to save surgeon details for case based on case info id
 ***********************************************************************************/ 
	PROCEDURE gm_sav_case_surgeon_dtl (
		p_case_infoid    IN     t7100_case_information.c7100_case_info_id%type
	   ,p_npi_id		 IN     T7106_CASE_SURGEON_MAP.c6600_surgeon_npi_id%type
	   ,p_party_id		 IN		T7106_CASE_SURGEON_MAP.c7106_surgeon_party_id%type
	   ,p_surg_nm		 IN		T7106_CASE_SURGEON_MAP.c7106_surgeon_name%type
	   ,p_void_fl        IN     T7106_CASE_SURGEON_MAP.c7106_void_fl%type
	   ,p_user_id		 IN		T7106_CASE_SURGEON_MAP.C7106_LAST_UPDATED_BY%type	
	)
	AS
	BEGIN
		
		UPDATE T7106_CASE_SURGEON_MAP
		   SET C6600_SURGEON_NPI_ID = p_npi_id,
			   C7106_SURGEON_PARTY_ID = p_party_id,
			   C7106_SURGEON_NAME = p_surg_nm,
			   C7106_VOID_FL = p_void_fl,
			   C7106_LAST_UPDATED_BY = p_user_id,
			   C7106_LAST_UPDATED_DATE = current_date
		 WHERE c7100_case_info_id = p_case_infoid
		   AND C6600_SURGEON_NPI_ID = p_npi_id
		   AND C7106_VOID_FL is null;
		
	IF (SQL%ROWCOUNT = 0) THEN
	
		INSERT INTO T7106_CASE_SURGEON_MAP (C7106_CASE_SURGEON_MAP_ID,C7100_CASE_INFO_ID,C6600_SURGEON_NPI_ID,C7106_SURGEON_PARTY_ID,
											C7106_SURGEON_NAME,C7106_VOID_FL,C7106_LAST_UPDATED_BY,C7106_LAST_UPDATED_DATE)
		     VALUES (S7106_CASE_SURGEON_MAP.nextval,p_case_infoid,p_npi_id,p_party_id, 
		     		 p_surg_nm,p_void_fl,p_user_id,current_date);
	END IF;
		
		END gm_sav_case_surgeon_dtl;
		
/* *******************************************************************************
 * Author: Agilan singaravelan
 * Procedure:gm_sav_case_set_dtl
 * Description:Procedure to save set details for case based on case info id
 ***********************************************************************************/ 
	PROCEDURE gm_sav_case_set_dtl (
		p_case_infoid    IN     t7100_case_information.c7100_case_info_id%type
	   ,p_kit_id		 IN     T2078_KIT_MAPPING.C2078_KIT_MAP_ID%type
	   ,p_set_id		 IN		t207_set_master.c207_set_id%type
	   ,p_tag_id		 IN		T5010_tag.c5010_tag_id%type
	   ,p_qty			 IN		T7104_Case_Set_Information.C7104_ITEM_QTY%type
	   ,p_void_fl        IN     T7106_CASE_SURGEON_MAP.c7106_void_fl%type
	   ,p_user_id		 IN		T7106_CASE_SURGEON_MAP.C7106_LAST_UPDATED_BY%type	
	)		
	AS
	  v_tag_id		 	T5010_tag.c5010_tag_id%type;
	BEGIN
		v_tag_id := nvl(p_tag_id,'');
		
		UPDATE T7104_Case_Set_Information
		   SET C2078_KIT_MAP_ID = p_kit_id,
		       C207_SET_ID = p_set_id,
		       C5010_TAG_ID = v_tag_id,
		       C7104_ITEM_QTY = p_qty,
		       C7104_VOID_FL = p_void_fl,
		       C7104_LAST_UPDATED_BY = p_user_id,
		       C7104_LAST_UPDATED_DATE = current_date
		 WHERE C7100_CASE_INFO_ID = p_case_infoid
		   AND C207_SET_ID = p_set_id
		   AND (C5010_TAG_ID = p_tag_id OR C5010_TAG_ID is NULL)--p_tag_id
		   --AND C2078_KIT_MAP_ID = p_kit_id 
		   AND C7104_VOID_FL IS NULL;
		   
		   
	IF (SQL%ROWCOUNT = 0) THEN
	
		INSERT INTO T7104_Case_Set_Information (C7104_CASE_SET_ID,C7100_CASE_INFO_ID,C2078_KIT_MAP_ID,C207_SET_ID,C5010_TAG_ID,C7104_ITEM_QTY,C7104_VOID_FL,C7104_CREATED_BY,C7104_CREATED_DATE)
		VALUES (s7104_case_set_information.NEXTVAL,p_case_infoid,p_kit_id,p_set_id,v_tag_id,p_qty,p_void_fl,p_user_id,current_date);
		
	END IF;
	
	END gm_sav_case_set_dtl;
	
/* *******************************************************************************
 * Author: Agilan singaravelan
 * Procedure:gm_sav_case_part_dtl
 * Description:Procedure to save part details for case based on case info id
 ***********************************************************************************/ 
	PROCEDURE gm_sav_case_part_dtl (
		p_case_infoid    IN     t7100_case_information.c7100_case_info_id%type
	   ,p_kit_id		 IN     T2078_KIT_MAPPING.C2078_KIT_MAP_ID%type
	   ,p_part_num		 IN		t205_part_number.C205_PART_NUMBER_ID%type
	   ,p_qty			 IN		T7104_Case_Set_Information.C7104_ITEM_QTY%type
	   ,p_void_fl        IN     T7106_CASE_SURGEON_MAP.c7106_void_fl%type
	   ,p_user_id		 IN		T7106_CASE_SURGEON_MAP.C7106_LAST_UPDATED_BY%type	
	)		
	AS	
	BEGIN
		
		UPDATE T7104_Case_Set_Information
		   SET C2078_KIT_MAP_ID = p_kit_id,
		       C205_PART_NUMBER_ID = p_part_num,
		       C7104_ITEM_QTY = p_qty,
		       C7104_VOID_FL = p_void_fl,
		       C7104_LAST_UPDATED_BY = p_user_id,
		       C7104_LAST_UPDATED_DATE = current_date
		 WHERE C7100_CASE_INFO_ID = p_case_infoid
		   AND C205_PART_NUMBER_ID = p_part_num
		   AND C7104_VOID_FL IS NULL;
		   
	IF (SQL%ROWCOUNT = 0) THEN	
	
		INSERT INTO T7104_Case_Set_Information (C7104_CASE_SET_ID,C7100_CASE_INFO_ID,C2078_KIT_MAP_ID,C205_PART_NUMBER_ID,C7104_ITEM_QTY,C7104_VOID_FL,C7104_CREATED_BY,C7104_CREATED_DATE)
		VALUES (s7104_case_set_information.NEXTVAL,p_case_infoid,p_kit_id,p_part_num,p_qty,p_void_fl,p_user_id,current_date);
	
	END IF;
	
END gm_sav_case_part_dtl;	

/* *******************************************************************************
 * Author: Agilan singaravelan
 * Procedure:gm_sav_case_rep_map
 * Description:Procedure to save share details for case based on case info id
 ***********************************************************************************/ 
	PROCEDURE gm_sav_case_rep_map (
		p_case_infoid    IN     t7100_case_information.c7100_case_info_id%type
	   ,p_repparty_id	 IN     T101_party.c101_party_id%type
	   ,p_void_fl        IN     T7106_CASE_SURGEON_MAP.c7106_void_fl%type
	   ,p_user_id		 IN		T7106_CASE_SURGEON_MAP.C7106_LAST_UPDATED_BY%type	
	)
	AS
	BEGIN
	
		UPDATE T7107_CASE_SHARE
		   SET C7107_VOID_FL = p_void_fl,
		       C7107_LAST_UPDATED_BY = p_user_id,
		       C7107_LAST_UPDATED_DATE = current_date
		 WHERE C7100_CASE_INFO_ID = p_case_infoid
		   AND C7107_REP_PARTY_ID = p_repparty_id
		   AND C7107_VOID_FL IS NULL;
		   
	IF (SQL%ROWCOUNT = 0) THEN		 
		
		INSERT INTO T7107_CASE_SHARE (C7107_CASE_SHARE_ID,C7100_CASE_INFO_ID,C7107_REP_PARTY_ID,C7107_VOID_FL)
		     VALUES (S7107_CASE_SHARE.nextval,p_case_infoid,p_repparty_id,p_void_fl);
		     
	END IF;
	
	END gm_sav_case_rep_map;
/********************************************************************************
* Description : Procedure to update the event item id for the case booking.
* Author      : Agilan
*********************************************************************************/
PROCEDURE gm_sav_case_event_item_id(
    p_caseinfoid  IN 	t7100_case_information.c7100_case_info_id%type,
    p_itemid      IN 	t7100_case_information.c7100_event_item_id%TYPE,
    p_userid      IN 	t7100_case_information.c7100_created_by%TYPE )
AS
BEGIN
  	UPDATE t7100_case_information
  	   SET C7100_event_item_id   = p_itemid ,
    	   C7100_LAST_UPDATED_BY   = p_userid ,
           C7100_LAST_UPDATED_DATE = CURRENT_DATE
     WHERE C7100_CASE_INFO_ID   = p_caseinfoid;
     
  END gm_sav_case_event_item_id;
  
/********************************************************************************
* Description : Procedure to update the case status as cancelled.
* Author      : Agilan
*********************************************************************************/
PROCEDURE gm_sav_cancel_case (
		p_input_string 		IN 		CLOB,
	    p_user_id      		IN 		T7100_CASE_INFORMATION.C7100_LAST_UPDATED_BY%TYPE,
	    p_case_info_id 	   	OUT 	VARCHAR2)
AS
	v_json 	 		json_object_t;
	v_caseinfo_id   t7100_case_information.c7100_case_info_id%type;
BEGIN
	  v_json      	    := json_object_t.parse(p_input_string);
	  v_caseinfo_id		:= v_json.get_String('caseinfoid');
	  
	  UPDATE T7100_CASE_INFORMATION
	     SET C901_CASE_STATUS = '107163',
	         C7100_LAST_UPDATED_BY = p_user_id,
	         C7100_LAST_UPDATED_DATE = CURRENT_DATE
	   WHERE C7100_CASE_INFO_ID = v_caseinfo_id
	     AND C7100_VOID_FL is null;
	     
	  p_case_info_id := v_caseinfo_id;
END gm_sav_cancel_case;

END gm_pkg_case_txn;
/