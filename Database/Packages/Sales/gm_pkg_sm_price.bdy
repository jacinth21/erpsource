/* Formatted on 2011/10/28 18:05 (Formatter Plus v4.8.0) */
-- @"C:\Database\Packages\Sales\gm_pkg_sm_price.bdy" ;

CREATE OR REPLACE PACKAGE BODY gm_pkg_sm_price
IS
	/*******************************************************
	* Description : Load procedure to insert calculated price and must be executed prior to generating excels

	*******************************************************/
	PROCEDURE gm_sav_price_info
	AS
		v_gpo_id 	   T740_GPO_ACCOUNT_MAPPING.C101_PARTY_ID%TYPE;
		v_temp_gpo_id  T740_GPO_ACCOUNT_MAPPING.C101_PARTY_ID%TYPE;
		v_temp_acc 	   t704_account.c704_account_id%TYPE;
			
		CURSOR p_out_accids
		IS
			SELECT c704_account_id acids
			  FROM t705b_price_increase_exclude
			 WHERE TO_CHAR (c705b_price_year, 'YYYY') = '2014' 
             AND c704_account_id  in (
            --'6655','6538','4871','6665','5573','6352','7710','6689','7318','5013','5008','8173','4328','6063','6278','5845','5983','6196','5632','6718','5812','5835','5691','5806','8115',
			--'5810','5624','7608','5269','6545','6613','5973','5505','4868','7301','7305','5380','4276','4744','8213','6213','7637','7670','5163','5662','4670','4893','4636','8208','5519'
			--'7745','4455','6629','4468','4503','4901','6316','4685','5625','7216','4437','5222','5115','5751','5720','4398','5196','4759','4445','4275','4396','4382'
			--'5041','6510','4415','5803','4785','5631','5989','4845','6320','4649','5376','6748','5805','7731','7182','5568','6708','5815','6149','5322','6343','4583','6782','4545',
			--'4690','4831','6388','4564','5534','5084','7618','7610','4927','5807','6400','6177','6712','7511','6503','5533','6487','6346','5122','4908','5532','7729','5895','6376'
			--'5015','7483','4972','5509','8210','7201','6306','4370','7217','7207','7705','5606','4695','4372','6264','5819','5545','5799','5949','7659','5365','5733','7746','5656'
			--,'6615','7183','4663','4664','5717','8205','5445','8091','8231','7635','4379','5288','7666','5405','5223','7725','7605','7538','8206','7317','6492','7636','7634','7643'
			--'6569','6350','5987','7536','7743','7726','8315','5307','7701','5706','7603','5754','5149','6064','4375','5766','6323','7671','5164','6675','5304','4498','4886','5231'
			--,'4942','4469','5698','7730','6165','4753','5956','5547','6313','6061','7650','4723','5076','5594','5601','6449','6402','4917','7302','6522','5417','4571','6488','4936'
			--'4705','4579','6636','6207','5507','4826','6793','6544','7692','5191','6660','6670','6566','6533','8283','4883','6103','6752','6525'
			--Account part of GPO
			--5509,6533,7692,6660,6670,5322,4503,5591,4901,5819,6566,6615,4663,4664,5445,4886,5164,5231,4942,6323,5805,4468,6782,4545,4415,4883,6103,7637,7670,8208,7634,7643,6525,6793,6752,5380
			8205
             )
             AND c705b_exclude_fl IS NULL;

		CURSOR price_detail_ins_cur (
			v_ac_id   IN   VARCHAR
		)
		IS
		
			SELECT	 t205.c205_part_number_id pnum, v_ac_id account_id, 
					get_account_part_pricing(v_ac_id, t205.c205_part_number_id,v_gpo_id) uprice
				   , t705a.c207_set_sales_system_id sysid
				   , get_part_price_list (t205.c205_part_number_id, '') lprice
				   , t705a.c705a_price_increase per_inc
				FROM 
				     t205_part_number t205
				   , t208_set_details t208
				   , t207_set_master t207
				   , t705a_price_classification t705a
			   WHERE t205.c205_part_number_id = t208.c205_part_number_id
				 AND t208.c207_set_id = t207.c207_set_id
				 AND t207.c901_set_grp_type = 1608
				 AND t208.c205_part_number_id IS NOT NULL
				 AND t207.c207_set_id = t705a.c207_set_sales_system_id
				 AND TO_CHAR (t705a.c705a_price_year, 'YYYY') = '2014'
			--and (trunc(t705.c705_last_updated_date) < to_date('10/19/2010','mm/dd/yyyy')
			--or t705.c705_last_updated_date is null)
			;

		v_new_price    NUMBER := 0;
		v_price 	   NUMBER := 0;
		v_price_pc	   t705a_price_classification.c705a_price_increase%TYPE;
		v_ac_exist	   t704_account.c704_account_id%TYPE;
		v_ac_id 	   t704_account.c704_account_id%TYPE;
		v_set_exist    NUMBER;
		v_cnt 		   NUMBER;
		v_pi_cnt 		   NUMBER;
	BEGIN
		FOR account_id_ins IN p_out_accids LOOP
			v_ac_id 	:= account_id_ins.acids;
			v_temp_acc  := v_ac_id;
			BEGIN
				select C101_PARTY_ID into v_gpo_id FROM T740_GPO_ACCOUNT_MAPPING WHERE C740_VOID_FL IS NULL AND C704_ACCOUNT_ID =v_ac_id;
				EXCEPTION WHEN NO_DATA_FOUND THEN
				v_gpo_id :='';
			END;
			
			IF account_id_ins.acids IS NOT NULL THEN
				SELECT COUNT (1)
				  INTO v_ac_exist
				  FROM t704_account
				 WHERE c704_account_id = account_id_ins.acids;

				IF (v_ac_exist IS NULL) THEN
					raise_application_error ('-20214', '');
				END IF;
			END IF;
			
		FOR price_detail_ins IN price_detail_ins_cur (v_ac_id) LOOP
			
			v_temp_acc := v_ac_id;
			v_temp_gpo_id :='';
			v_pi_cnt := 0;
			-- When a part does not have a price, exclude it .
			IF price_detail_ins.uprice IS NOT NULL THEN
					v_new_price :=
							  -- Based on 11/06/13 meeting with Kathy , we will round the data as PUF is rounding it already.
							   ROUND((((price_detail_ins.per_inc / 100.00) * price_detail_ins.uprice) + price_detail_ins.uprice
							   ));
							   
					IF v_gpo_id IS NOT NULL
					THEN
					
					   --If a Part is having GPO price, then account ID should not be loaded,as GPO price takes precedence.	
					   SELECT COUNT(1) INTO v_cnt FROM T705_ACCOUNT_PRICING WHERE C205_PART_NUMBER_ID =price_detail_ins.pnum
					   AND c101_party_id = v_gpo_id AND C705_VOID_FL IS NULL;
					   
					   --If a Part is having GPO price, then account ID should not be loaded,as GPO price takes precedence.	
					   SELECT COUNT(1) INTO v_pi_cnt FROM t705c_price_increase WHERE C205_PART_NUMBER_ID =price_detail_ins.pnum
					   AND c101_party_id = v_gpo_id AND C705C_VOID_FL IS NULL AND c207_set_sales_system_id = price_detail_ins.sysid;
					   
					   
					   v_temp_gpo_id :=v_gpo_id ;
					   
					   IF v_cnt = 0
					   THEN
					   	v_temp_gpo_id :=''; -- GPO price is not available for this part.					   	
					   ELSIF v_cnt >0 THEN
					   	v_temp_acc := '';					   	
					   END IF;
				   END IF;
				    
				   
				   v_cnt := 0;
				   
				   IF v_pi_cnt = 0
				   THEN
						INSERT INTO t705c_price_increase
									(c705c_price_increase_id, c704_account_id, c207_set_sales_system_id
								   , c205_part_number_id, c705_unit_price, c705c_new_price, c705c_created_date, c705c_created_by
								   , c101_party_id
									)
							 VALUES (s705c_price_increase.NEXTVAL, v_temp_acc, price_detail_ins.sysid
								   , price_detail_ins.pnum, price_detail_ins.lprice, FLOOR (v_new_price), SYSDATE, '303510'
									,v_temp_gpo_id);
					END IF;
					END IF;
				END LOOP;	--END cursor price_detail_ins_cur
		END LOOP;
		commit;
	END gm_sav_price_info;

	/*******************************************************
	* Description : Procedure to fetch increased price information

	*******************************************************/
	PROCEDURE gm_fch_price_info (
		p_acid				 IN 	  t704_account.c704_account_id%TYPE
	  , p_user_id			 IN 	  t101_user.c101_user_id%TYPE
	  , p_out_price_detail	 OUT	  TYPES.cursor_type
	  , p_out_head_detail	 OUT	  TYPES.cursor_type
	)
	AS
		v_ac_exist	   t704_account.c704_account_id%TYPE;
		v_gpo_id 	   T740_GPO_ACCOUNT_MAPPING.C101_PARTY_ID%TYPE;   
	BEGIN
		IF p_acid IS NOT NULL THEN
			SELECT COUNT (1)
			  INTO v_ac_exist
			  FROM t704_account
			 WHERE c704_account_id = p_acid;

			IF (v_ac_exist IS NULL) THEN
				raise_application_error ('-20214', '');
			END IF;
		END IF;
		
		BEGIN
			select C101_PARTY_ID into v_gpo_id FROM T740_GPO_ACCOUNT_MAPPING WHERE C740_VOID_FL IS NULL AND C704_ACCOUNT_ID =p_acid;
			EXCEPTION WHEN NO_DATA_FOUND THEN
			v_gpo_id :='';
		END;
			
		OPEN p_out_head_detail
		 FOR
		 	-- As there are no duplicate/multiple records for an account in v700_territory_mapping_detail , distinct is not required
			SELECT  v700.region_id rid, v700.region_name rname, v700.ac_id accid
             ,NVL(get_rule_value(v700.region_id,'PRICEINCREASE'),v700.ad_id) adid
             ,DECODE(get_user_name(get_rule_value(v700.region_id,'PRICEINCREASE')),' ',v700.ad_name
            ,get_user_name(get_rule_value(v700.region_id,'PRICEINCREASE'))) adname
            ,get_user_emailid(NVL(get_rule_value(v700.region_id,'PRICEINCREASE'),v700.ad_id)) emailid
            ,get_user_phoneno(NVL(get_rule_value(v700.region_id,'PRICEINCREASE'),v700.ad_id)) phoneno
            ,get_user_title(NVL(get_rule_value(v700.region_id,'PRICEINCREASE'),v700.ad_id)) title
            ,AC_NAME acname, get_bill_add (v700.ac_id) acadd
            FROM v700_territory_mapping_detail v700
            WHERE v700.ac_id = p_acid;

		OPEN p_out_price_detail
		 FOR
			 SELECT   REPLACE (REPLACE (get_set_name (t705c.c207_set_sales_system_id), '&reg;', '')
							 , '&trade;'
							 , ''
							  ) pname
					, t705c.c205_part_number_id pnum, get_partnum_desc (t705c.c205_part_number_id) pdesc
					, t705c.c705_unit_price uprice -- Unit Price Not required in Excel.
					, t705c.c705c_new_price newprice
				 FROM  t705c_price_increase t705c
				WHERE (t705c.c704_account_id = 5698
					   OR 
					   t705c.c101_party_id = 5698
					   )
				  AND t705c.c705c_void_fl IS NULL
			 ORDER BY pname, pnum;
	END gm_fch_price_info;

	/*******************************************************
	   * Description : Load procedure to increase the list price.

	*******************************************************/
	PROCEDURE gm_sav_part_list_price
	AS
		CURSOR new_list_price_cur
		IS
			SELECT t207.c202_project_id projid, t207.c207_set_sales_system_id sysid, t208.c208_set_details_id setdetid
				 , t207.c207_set_id setid, t208.c205_part_number_id pnum, t705a.c705a_price_increase pricepc
				 , get_part_price_list (t208.c205_part_number_id, '') lprice
				 , (  ((t705a.c705a_price_increase / 100) * get_part_price_list (t208.c205_part_number_id, ''))
					+ get_part_price_list (t208.c205_part_number_id, '')
				   ) newlistprice
				 , t705a.c705a_created_by userid
			  FROM t207_set_master t207, t208_set_details t208, t705a_price_classification t705a
			 WHERE t207.c207_set_id = t705a.c207_set_sales_system_id
			   AND t207.c207_set_id = t208.c207_set_id
			   AND t207.c901_set_grp_type = 1608
			   AND TO_CHAR (t705a.c705a_price_year, 'YYYY') = '2014'
			   AND t207.c207_void_fl IS NULL
			   AND t208.c208_void_fl IS NULL;
	BEGIN
		FOR new_list_price IN new_list_price_cur LOOP
			UPDATE t205_part_number
			   SET c205_list_price = FLOOR(new_list_price.newlistprice)
				 , c205_last_updated_date = SYSDATE
				 , c205_last_updated_by = new_list_price.userid
			 WHERE c205_part_number_id = new_list_price.pnum;
		END LOOP;
	END gm_sav_part_list_price;

	/*******************************************************
	* Description : Procedure to fetch all Account IDS

	*******************************************************/
	PROCEDURE gm_fch_price_info_acids (
		p_out_accids   OUT	 TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_out_accids
		 FOR
          SELECT t704.c704_account_id acids
                   FROM t704_account t704, t705b_price_increase_exclude t705b, v700_territory_mapping_detail v700
                  WHERE t704.c704_account_id = t705b.c704_account_id
                    AND t705b.c705b_exclude_fl IS NULL
                    AND t704.c704_account_id = v700.ac_id
                    AND TO_CHAR (t705b.c705b_price_year, 'YYYY') = '2014'
                    AND t704.c704_account_id IN (
    5951,
    6023,
    4560,
    4452,
    5617,
    4343,
    4942,
    5148,
    4309,
    5984,
    5924,
    5031,
    5676,
    4863,
    4519,
    5018,
    5444,
    5014,
    6128,
    5028
    );
		/*	 SELECT t704.c704_account_id acids
			   FROM t704_account t704, t705b_price_increase_exclude t705b, v700_territory_mapping_detail v700
			  WHERE t704.c704_account_id = t705b.c704_account_id
				AND t705b.c705b_exclude_fl IS NULL
				AND t704.c704_account_id = v700.ac_id
				AND v700.region_id IN (4012)
				--AND t704.c704_account_id = 6431
				AND t704.c704_active_fl = 'Y'
				AND TO_CHAR (t705b.c705b_price_year, 'YYYY') = '2014';
	 SELECT t704.c704_account_id acids
	   FROM t704_account t704, t705b_price_increase_exclude t705b
	  WHERE t704.c704_account_id = t705b.c704_account_id
		  AND t705b.c705b_exclude_fl IS NULL
		  AND t704.c704_active_fl = 'Y';
	*/
	END gm_fch_price_info_acids;
END gm_pkg_sm_price;
/
