/* Formatted on 2011/11/07 15:52 (Formatter Plus v4.8.0) */
--@"C:\Database\Packages\Sales\invallocation\gm_pkg_sm_loaner_allocation.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_sm_loaner_allocation
IS
--
/*********************************
   Purpose: This procedure is used to initiate loaner allocation.
**********************************/
--
	PROCEDURE gm_init_loaner_job
	AS
		-- Cursor to fetch the data from t7104 where tag id is null to allocate loaner.
		CURSOR c_tags_tbd
		IS
			SELECT	 t7100.c7100_case_info_id caseinfoid, t7100.c7100_surgery_date surgerydt
				FROM t7100_case_information t7100
			   WHERE t7100.c7100_case_info_id IN (
						 SELECT DISTINCT t7104.c7100_case_info_id
									FROM t7104_case_set_information t7104
								   WHERE t7104.c7104_void_fl IS NULL
									 AND t7104.c5010_tag_id IS NULL
									 AND t7104.c7104_shipped_del_fl IS NULL
									 AND c526_product_request_detail_id IS NULL)
				 AND t7100.c901_case_status = 11091   -- Active
				 AND t7100.c901_type  = 1006503    -- CASE
				 AND t7100.c7100_void_fl IS NULL
				 AND TRUNC (CURRENT_DATE) >= TRUNC (get_lock_date (c7100_surgery_date, 'LOCK_FROM_DATE'))
				 AND TRUNC (CURRENT_DATE) <= TRUNC(c7100_surgery_date)  
			ORDER BY t7100.c7100_surgery_date;
		v_caseinfoid		 t7100_case_information.c7100_case_info_id%TYPE;	
	BEGIN
		FOR v_rec IN c_tags_tbd
		LOOP
			SELECT	   t7100.c7100_case_info_id
				  INTO v_caseinfoid
				  FROM t7100_case_information t7100
				 WHERE t7100.c7100_case_info_id = v_rec.caseinfoid
			FOR UPDATE;

			gm_init_loaner_req (v_rec.caseinfoid, CURRENT_DATE, NULL, 30301);
		END LOOP;
	END gm_init_loaner_job;

--
/*********************************
   Purpose: This procedure is used to initiate mutiple loaner requests.
**********************************/
--
	PROCEDURE gm_initiate_multiple_loaners (
		  p_caseinfo_id	IN	 t7100_case_information.c7100_case_info_id%TYPE
		, p_intputstr   IN   VARCHAR2
		, p_userid		IN	 t7100_case_information.c7100_last_updated_by%TYPE
	)
	AS
		v_inputstr    VARCHAR2 (4000) := p_intputstr;
		v_casesetid	  t7104_case_set_information.c7104_case_set_id%TYPE;
		v_loandt	  DATE;
	BEGIN
		DELETE FROM my_temp_list;
		WHILE INSTR (v_inputstr, ',') <> 0
		LOOP
			v_casesetid	:= TO_NUMBER(SUBSTR (v_inputstr, 1, INSTR (v_inputstr, ',') - 1));
			v_inputstr := SUBSTR (v_inputstr, INSTR (v_inputstr, ',') + 1);
			
			BEGIN
				SELECT TRUNC(t7104.c7104_tag_lock_from_date) 
				  INTO v_loandt
				  FROM t7104_case_set_information t7104
				 WHERE t7104.c7104_case_set_id = v_casesetid
				   AND t7104.c7104_void_fl IS NULL 
				   FOR UPDATE;
				EXCEPTION
    				WHEN NO_DATA_FOUND THEN
        				raise_application_error ('-20550', '');
			END;
			gm_init_loaner_req(p_caseinfo_id,v_loandt,v_casesetid,p_userid);
		END LOOP;
	   /* Code added for setting the NULL value to the C526_REQUEST_FLAG after calling second time.
		  that means After one case is booked if again need to add some more sets, then this flag will update the 'Y' value and in Jasper Print we show that second time set as **SET_ID
		  Added for PMT-254[Edit Loaner Requests Screen changes.]
	    */
		my_context.set_my_inlist_ctx (NULL) ;
		--find any obsolete sets are added,It will throw app error
		gm_pkg_op_product_requests.gm_validate_set_obsolete_sts();
	END gm_initiate_multiple_loaners;
--
/*********************************
   Purpose: This procedure is used to initiate loaner request.
**********************************/
--
	PROCEDURE gm_init_loaner_req (
		p_caseinfo_id	IN	 t7100_case_information.c7100_case_info_id%TYPE
	  , p_loandt		IN	 DATE
	  , p_casesetid 	IN	 t7104_case_set_information.c7104_case_set_id%TYPE
	  , p_userid		IN	 t7100_case_information.c7100_last_updated_by%TYPE
	)
	AS
		CURSOR c_tagstr
		IS
			SELECT t7104.c7104_case_set_id casesetid, t207b.c207_loaner_set_id loanersetid, t7104.c7100_case_info_id caseinfoid
				 , t7104.c7104_tag_lock_from_date lockfromdt, t7104.c7104_tag_lock_to_date locktodt
				 , t7100.c7100_surgery_date surgerydt, t7100.c701_distributor_id casedistid
				 , t7100.c703_sales_rep_id caserepid, t7100.c704_account_id caseacctid
				 , t7104.c526_product_request_detail_id prdreqdtlid, t7104.c207_set_id setid
			  FROM t7104_case_set_information t7104, t7100_case_information t7100,t207b_loaner_set_map t207b
			 WHERE t7104.c7104_void_fl IS NULL
			   AND t7104.c7104_shipped_del_fl IS NULL
			   AND t7104.c7100_case_info_id = p_caseinfo_id
			   AND t7104.c7100_case_info_id = t7100.c7100_case_info_id
			   AND t7100.c901_case_status = 11091	-- Active
			   AND t7100.c901_type  = 1006503    -- CASE
			   AND t207b.c207_consign_set_id = t7104.c207_set_id
			   AND t207b.c207b_void_fl IS NULL
			   AND t7100.c7100_void_fl IS NULL
			   AND t7104.c7104_case_set_id = NVL (p_casesetid, t7104.c7104_case_set_id);

		v_str		   VARCHAR2 (4000);
		v_caserepid    t7100_case_information.c703_sales_rep_id%TYPE;
		v_caseacctid   t7100_case_information.c704_account_id%TYPE;
		v_casedistid   t7100_case_information.c701_distributor_id%TYPE;
		v_surgerydt    t7100_case_information.c7100_surgery_date%TYPE;
		v_req_id	   VARCHAR2 (20);
		v_loandt	   DATE;
		v_ship_date	   DATE;
		v_shipid 	   VARCHAR2 (20);
		v_req_dtl_id   t7104_case_set_information.c526_product_request_detail_id%TYPE;	
		v_childshipid  VARCHAR2 (20);
		v_setid   	   t7104_case_set_information.c207_set_id%TYPE;
		v_addressid    t106_address.c106_address_id%TYPE;
		v_assoc_rep_id t525_product_request.c703_ass_rep_id%TYPE;
	BEGIN
		FOR v_rec IN c_tagstr
		LOOP
			v_surgerydt := v_rec.surgerydt;
			v_caserepid := v_rec.caserepid;
			v_caseacctid := v_rec.caseacctid;
			v_casedistid := v_rec.casedistid;
			v_ship_date := p_loandt;
			v_loandt := p_loandt;
			
			IF v_rec.prdreqdtlid IS NOT NULL THEN
			    GM_RAISE_APPLICATION_ERROR('-20999','353',v_rec.setid);		    
        		
        	END IF;
			
			IF v_ship_date IS NULL THEN -- IF it comes from front end then it will be null
				v_ship_date := TRUNC (get_lock_date (v_surgerydt, 'LOCK_FROM_DATE')-1);				
			END IF;
			--This will return whether passing date is buisness day then return same date otherwise return next buisness day
			v_loandt := get_next_working_day(v_ship_date);
			--validating ship date is buisness date
				IF TRUNC(v_ship_date) <> TRUNC(v_loandt) THEN
				--finding difference between surgery date and new ship date, if its less than 3 days then we move to previous work date
					IF TRUNC(v_surgerydt) - TRUNC(v_loandt) < 3 THEN
						--To find the last working day
						v_loandt := get_last_working_day(v_ship_date);
					END IF;
				END IF;
				--case book and rescheduled sets also need to check
				IF (v_loandt - CURRENT_DATE) < 0 THEN
					v_loandt := CURRENT_DATE;
				END IF;
			
			v_str		:= v_str || v_rec.loanersetid || '^1^' || TO_CHAR (TRUNC (v_loandt), 'MM/dd/yyyy') || '^5|';   --setid needs to be discussed.
		END LOOP;
		
        IF v_str IS NULL THEN --If cursor is not found validation then v_str will be null
        	raise_application_error ('-20550', '');
        END IF;
        
    	BEGIN
			SELECT	   t7104.c526_product_request_detail_id
				  INTO v_req_dtl_id
				  FROM t7104_case_set_information t7104
				 WHERE t7104.c526_product_request_detail_id IS NOT NULL
				   AND t7104.c7100_case_info_id = p_caseinfo_id
				   AND t7104.c7104_void_fl IS NULL
				   AND ROWNUM = 1;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				v_req_dtl_id := NULL;
		END; 

		IF v_req_dtl_id IS NOT NULL
		THEN
			SELECT c525_product_request_id
			  INTO v_req_id
			  FROM t526_product_request_detail
			 WHERE c526_product_request_detail_id = v_req_dtl_id AND c526_void_fl IS NULL;
			 
			 BEGIN
			 	SELECT c703_ass_rep_id
			  	INTO v_assoc_rep_id
			  	FROM t525_product_request
			 	WHERE c525_product_request_id = v_req_id AND c525_void_fl IS NULL;
			 EXCEPTION
				WHEN NO_DATA_FOUND 
					THEN
			 			v_assoc_rep_id :=null;
			 END;
			 
			 ---if the request is existed, update to open
			 UPDATE t525_product_request t525
               SET t525.c525_status_fl = '10',
                   c525_last_updated_by = p_userid,
                   c525_last_updated_date = CURRENT_DATE
             WHERE t525.c525_product_request_id = v_req_id
               AND c525_void_fl IS NULL;
		END IF;
        
		gm_pkg_op_product_requests.gm_sav_product_requests (v_surgerydt
														  , 4127   --Product Loaner
														  , v_casedistid
														  , 50626	-- (sales rep) p_req_by_type
														  , p_userid   --p_req_by_id
														  , 4121   -- (sales rep)p_ship_to
														  , v_caserepid   --(sales rep id)p_ship_to_id
														  , p_userid
														  , v_str
														  , v_caserepid
														  , v_caseacctid
														  , v_req_id
														  ,null
														  , v_assoc_rep_id);
		
											  
														  
														  v_childshipid := 0;
		v_addressid := NVL (get_address_id (v_caserepid), 0);												   
		IF v_req_dtl_id IS NULL
		THEN
			--Shipping detail
			gm_pkg_cm_shipping_trans.gm_sav_ship_info (v_req_id	 --refid
													, 50185   --source
													, 4121	 -- (sales rep)p_ship_to
													, v_caserepid	--(sales rep id)p_ship_to_id
													, 5001	 -- carrier
													, 5004	 -- delivery mode
													, NULL	 -- tracking_number
													, v_addressid	 -- address_id
													, 0   -- freight amount
													, p_userid
													, v_shipid	 -- out shipid
													 );
			SELECT c526_product_request_detail_id
			  INTO v_req_dtl_id
			  FROM t526_product_request_detail
			 WHERE c525_product_request_id = v_req_id AND c526_void_fl IS NULL;
			 										 
			gm_pkg_cm_shipping_trans.gm_sav_ship_info (v_req_dtl_id	 --refid
													, 50182   --source
													, 4121	 -- (sales rep)p_ship_to
													, v_caserepid	--(sales rep id)p_ship_to_id
													, 5001	 -- carrier
													, 5004	 -- delivery mode
													, NULL	 -- tracking_number
													, v_addressid	 -- address_id
													, 0   -- freight amount
													, p_userid
													, v_childshipid	 -- out shipid
													 );	
		ELSE
			
			SELECT MAX(c526_product_request_detail_id)
			  INTO v_req_dtl_id
			  FROM t526_product_request_detail
			 WHERE c525_product_request_id = v_req_id AND c526_void_fl IS NULL;
			 
			gm_pkg_cm_shipping_trans.gm_sav_ship_info (v_req_dtl_id	 --refid
													, 50182   --source
													, 4121	 -- (sales rep)p_ship_to
													, v_caserepid	--(sales rep id)p_ship_to_id
													, 5001	 -- carrier
													, 5004	 -- delivery mode
													, NULL	 -- tracking_number
													, v_addressid	 -- address_id
													, 0   -- freight amount
													, p_userid
													, v_childshipid	 -- out shipid
													 );	
												 
		END IF;
							
	--	gm_sav_loaner_tags (v_req_id, p_caseinfo_id, p_casesetid, p_userid);
		
		SELECT t7104.c207_set_id
		  INTO v_setid
		  FROM t7104_case_set_information t7104
		 WHERE t7104.c7104_case_set_id = p_casesetid 
		   AND t7104.c7104_shipped_del_fl IS NULL
		   AND t7104.c7104_void_fl IS NULL;
 
		gm_pkg_sm_setmgmt_txn.gm_sav_sets_tag_info (p_casesetid
												  , v_setid
												  , 11381	-- Loaner
												  , NULL
												  , v_req_dtl_id
												  , NULL
												  , p_userid
												   );
		
	END gm_init_loaner_req;

--
/*********************************
   Purpose: This procedure is used to save the loaner tag to T7104.
**********************************/
--
	PROCEDURE gm_sav_loaner_tags (
		p_requestid 	IN	 t525_product_request.c525_product_request_id%TYPE
	  , p_caseinfo_id	IN	 t7100_case_information.c7100_case_info_id%TYPE
	  , p_casesetid 	IN	 t7104_case_set_information.c7104_case_set_id%TYPE
	  , p_userid		IN	 t7100_case_information.c7100_last_updated_by%TYPE
	)
	AS
		CURSOR c_reqdtl
		IS
			SELECT t207b.c207_consign_set_id setid, t526.c526_product_request_detail_id reqdtlid
			  FROM t526_product_request_detail t526, t525_product_request t525,t207b_loaner_set_map t207b
			 WHERE t526.c525_product_request_id = p_requestid
			   AND t526.c526_void_fl IS NULL
			   AND t526.c525_product_request_id = t525.c525_product_request_id
			   AND t207b.c207_loaner_set_id = t526.c207_set_id
			   AND t207b.c207b_void_fl IS NULL
			   AND t525.c525_void_fl IS NULL;

		v_casesetid    t7104_case_set_information.c7104_case_set_id%TYPE;
	BEGIN
		FOR v_rec IN c_reqdtl
		LOOP
			v_casesetid := p_casesetid;

			BEGIN
				SELECT	   t7104.c7104_case_set_id
					  INTO v_casesetid
					  FROM t7104_case_set_information t7104
					 WHERE t7104.c526_product_request_detail_id IS NULL
					   AND t7104.c7104_shipped_del_fl IS NULL
					   AND t7104.c7104_void_fl IS NULL
					   AND t7104.c7100_case_info_id = p_caseinfo_id
					   AND t7104.c207_set_id = v_rec.setid 
					   AND t7104.c7104_case_set_id = NVL (p_casesetid, t7104.c7104_case_set_id)
					   AND ROWNUM < 2
				FOR UPDATE;
			EXCEPTION
				WHEN NO_DATA_FOUND
				THEN
					v_casesetid := NULL;
			END;

			gm_pkg_sm_setmgmt_txn.gm_sav_sets_tag_info (v_casesetid
													  , v_rec.setid
													  , 11381	-- Loaner
													  , NULL
													  , v_rec.reqdtlid
													  , NULL
													  , p_userid
													   );
		END LOOP;
	END gm_sav_loaner_tags;
/*********************************
   Purpose: This procedure is used to save loaner request status.
**********************************/
--
	PROCEDURE gm_sav_loaner_req_appv (
		p_lnreq_ids  IN   VARCHAR2
	  , p_userid	 IN   t526_product_request_detail.c526_last_updated_by%TYPE
	  , p_status_id  IN   VARCHAR2 
	  , p_reqid      OUT  VARCHAR2
	  , p_reqdet_litpnum OUT  VARCHAR2
	  , p_req_det_id OUT  VARCHAR2
	)
	AS
	v_count    NUMBER;
	v_req_id   NUMBER;
	v_ship_to  NUMBER;
	v_pfmly	   VARCHAR2(10);
	v_status    VARCHAR2(20);
	v_reqdet_litpnum_str   VARCHAR2(4000);
	v_req_det_id_str   VARCHAR2(4000);
	v_price  T205_PART_NUMBER.C205_CONSIGNMENT_PRICE%TYPE;
	v_company_id  VARCHAR2(20);
	CURSOR v_CUR
	IS
		SELECT  c526_product_request_detail_id proddet, c526_status_fl stat, c901_request_type, c205_part_number_id pnum ,c525_product_request_id prodreqid
        FROM t526_product_request_detail
        WHERE c526_product_request_detail_id IN (SELECT token FROM v_in_list)
   		AND c526_void_fl IS NULL
        FOR UPDATE;	
     
	BEGIN
		my_context.set_my_inlist_ctx (p_lnreq_ids);
		
		SELECT  get_compid_frm_cntx()
    	INTO    v_company_id
    	FROM    DUAL;
		   
    	FOR v_item IN v_cur
            LOOP
                IF v_item.stat <> 5 AND v_item.stat <> 2 AND v_item.stat <> 7
                THEN
					SELECT DECODE(v_item.stat,'40','Rejected', 'Approved') into v_status from dual;
					GM_RAISE_APPLICATION_ERROR('-20999','354',v_item.proddet||'#$#'||v_status);					
										                     
                END IF;
                
                --adding below validation to identify the viacell shipping location other than hospital
                 SELECT COUNT(1) INTO v_ship_to
					FROM t526_product_request_detail t526,
					  t907_shipping_info t907,
					  v205_parts_with_ext_vendor v205,
					  t106_address t106
					WHERE t526.c526_product_request_detail_id IN (v_item.proddet)
					AND t907.c907_ref_id                       = TO_CHAR(t526.c526_product_request_detail_id)
					AND t907.c907_void_fl                     IS NULL
					AND (t907.c901_ship_to                      <> 4122 --Hospital
                                        AND (t907.c901_ship_to = 4121 AND NVL(t106.c901_address_type,-999) <> 26240675))
                                                            AND  T907.C106_ADDRESS_ID = T106.C106_ADDRESS_ID (+)
                                        AND T106.C106_VOID_FL(+) IS NULL
					AND t526.c526_void_fl                     IS NULL
					AND t526.c205_part_number_id               = v_item.pnum
					AND t907.c907_status_fl <> 40
					AND v205.c205_part_number_id               = t526.c205_part_number_id;
					
					IF v_ship_to > 0 THEN
						GM_RAISE_APPLICATION_ERROR('-20999','410',v_item.prodreqid);										                     
                	END IF;
					
                
                IF v_item.pnum IS NOT NULL 
				THEN
				    
					SELECT t205.c205_product_family,get_part_price(v_item.pnum,'C')
					INTO v_pfmly, v_price
					FROM t205_part_number t205 
					WHERE t205.c205_part_number_id = v_item.pnum;
		
					IF v_pfmly = '4056'  --4056 - Literature Product family
	             	THEN         
	             	   --For Literature part, during AD Approval process, check for the consignment price.
	             	   --if it is 0 update the status as 5-Pending PD Approval.
	             	   --For PD approval, no need to check consignment price & we should update the status as 10.
	             	   IF  p_status_id = '5' 
	             	   THEN 
			             	IF v_price > 0 THEN
			             		v_status := '';
							    v_reqdet_litpnum_str := v_reqdet_litpnum_str || v_item.proddet ||',';
							ELSE
								v_status  := '5';
		              			v_req_det_id_str := v_req_det_id_str || v_item.proddet ||',';		
							END IF;
						ELSE 						
							v_status := '';
	              			v_req_det_id_str := v_req_det_id_str || v_item.proddet ||',';
						END IF;
	                ELSE
	              		v_status := p_status_id;
	              		v_req_det_id_str := v_req_det_id_str || v_item.proddet ||',';
	             	END IF;
             END IF;
            --Call common procedure to update the status, 10: open
             gm_pkg_op_loaner_allocation.gm_upd_request_status(v_item.proddet, NVL(v_status,'10'), p_userid);
	         UPDATE t526_product_request_detail
			   SET c526_approve_reject_by = p_userid,
			       c526_approve_reject_date = CURRENT_DATE
			 WHERE c526_product_request_detail_id =  v_item.proddet
			 AND c526_void_fl IS NULL;
			 
           END LOOP;
       		
	  	gm_sav_allocate_sets(p_lnreq_ids,p_userid);
		
		 SELECT COUNT ( *) INTO v_count
   		   FROM t525_product_request
          WHERE c525_product_request_id IN
    			(
         SELECT c525_product_request_id
           FROM t526_product_request_detail
          WHERE c526_product_request_detail_id IN (SELECT token FROM v_in_list)
          );
         
          IF v_count > 1
          THEN
           p_reqid :='';
          ELSE
         BEGIN
          SELECT c525_product_request_id INTO v_req_id
   		   FROM t525_product_request
          WHERE c525_product_request_id IN
    			(
         SELECT c525_product_request_id
           FROM t526_product_request_detail
          WHERE c526_product_request_detail_id IN (SELECT token FROM v_in_list));
         
        EXCEPTION
        	WHEN NO_DATA_FOUND
        	THEN
       		 v_req_id := '';
        END;
         SELECT count(1) INTO v_count
           FROM t526_product_request_detail
          WHERE c525_product_request_id = v_req_id
            AND c526_status_fl = DECODE(p_status_id,'5','2','5')
            AND c526_void_fl IS NULL;
         IF v_count > 0
          THEN 
          p_reqid :='';
          ELSE
          p_reqid :=v_req_id;
          END IF;
          END IF;
       
		p_reqdet_litpnum  := SUBSTR(v_reqdet_litpnum_str, 0, LENGTH(v_reqdet_litpnum_str)-1) ;
		p_req_det_id  := SUBSTR(v_req_det_id_str, 0, LENGTH(v_req_det_id_str)-1) ;
		
		-- Call the priority engine to prioritize the set
		--gm_pkg_op_ln_priority_engine.gm_sav_main_ln_set_priority(v_req_id,p_userid);
		
	END gm_sav_loaner_req_appv;

/*********************************
   Purpose: This procedure is used to allocate set to loaner request.
**********************************/
--
	PROCEDURE gm_sav_allocate_sets (
		p_lnreq_ids  IN   VARCHAR2
	  , p_userid	 IN   t526_product_request_detail.c526_last_updated_by%TYPE
	)
	AS
	v_lnreq_ids  	VARCHAR2 (3000) := p_lnreq_ids;
	v_req_dtl_id 	t526_product_request_detail.c526_product_request_detail_id%TYPE;
	v_setid   		t526_product_request_detail.c207_set_id%TYPE;
	v_reqtype       t526_product_request_detail.c901_request_type%TYPE;
	v_tissue_fl     t207_set_master.c207_tissue_fl%TYPE;
	v_company_id    VARCHAR2(20);
	v_company_tissue_fl    VARCHAR2(20);
	CURSOR cur_reqdtlid
		IS 
		SELECT c207_set_id setid ,c901_request_type reqtype, c526_product_request_detail_id reqdtlid, c205_part_number_id pnum,c525_product_request_id prodreqid
				  FROM t526_product_request_detail
			     WHERE c526_product_request_detail_id IN (SELECT token FROM v_in_list WHERE token IS NOT NULL)
			       AND c526_void_fl IS NULL 
			      -- AND c901_request_type = 4127
			       AND c526_status_fl NOT IN (20,40)
			       order by c526_product_request_detail_id;
	BEGIN   
		SELECT  get_compid_frm_cntx()
    	INTO    v_company_id
    	FROM    DUAL;
		
		v_company_tissue_fl := NVL(get_rule_value(v_company_id,'TISSUE_VALIDATION'),'N');
			FOR v_reqdtl_id IN cur_reqdtlid
			LOOP
			    v_setid := v_reqdtl_id.setid;
			    v_reqtype := v_reqdtl_id.reqtype;
			   
			    IF v_reqdtl_id.pnum IS NOT NULL AND v_reqtype = '4119' THEN -- Inhouse Loaner(4119)
			    	/* If the request required date is within 4days then this prc will allocate the items to the request.*/
			    	gm_pkg_sm_loaner_item_alloc.gm_sav_loaner_item_req(v_reqdtl_id.reqdtlid,v_reqtype,p_userid);
			    	
			    ELSIF v_reqtype = '4127' OR v_reqtype = '4119' THEN  --4127- Product Loaner  
			    --update the default address to hospital for tissue parts loaner sets PMT-41600
					BEGIN
			    	SELECT c207_tissue_fl INTO v_tissue_fl FROM t207_set_master WHERE c207_set_id=v_setid AND c207_void_fl IS NULL;
					EXCEPTION
					WHEN NO_DATA_FOUND THEN
					  v_tissue_fl := 'N';
					END;
			    	IF  v_tissue_fl = 'Y' AND v_reqtype = '4127' AND v_company_tissue_fl = 'Y'  THEN
			    	  gm_pkg_op_split.gm_update_loaner_shipping_info(v_reqdtl_id.prodreqid,v_reqdtl_id.reqdtlid,p_userid);
			    	END IF;
					gm_pkg_op_loaner.gm_sav_allocate (v_setid, v_reqtype, p_userid);
				END IF;
			END LOOP;   
	END gm_sav_allocate_sets;

/*********************************
   Purpose: This procedure is used to save loaner request reject.
**********************************/
--
	PROCEDURE gm_sav_loaner_req_rejt(
		p_lnreq_ids  IN   VARCHAR2
	  , p_userid	 IN   t526_product_request_detail.c526_last_updated_by%TYPE
	)
	AS
		CURSOR v_CUR
		IS
				SELECT  c526_product_request_detail_id proddet, c526_status_fl stat, c901_request_type reqtype, c525_product_request_id prodreqid
	            FROM t526_product_request_detail
	            WHERE c526_product_request_detail_id IN (SELECT token FROM v_in_list)
		   		AND c526_void_fl IS NULL
	            FOR UPDATE;
	    v_status	VARCHAR2(20);
	BEGIN
		my_context.set_my_inlist_ctx (p_lnreq_ids);
		
	      FOR v_item IN v_cur
            LOOP
           		IF v_item.stat <> 5 AND v_item.stat <> 2 AND v_item.stat <> 7
                THEN
                	SELECT DECODE(v_item.stat,'40','Rejected', 'Approved') into v_status from dual;
					GM_RAISE_APPLICATION_ERROR('-20999','354',v_item.proddet||'#$#'||v_status);	
									                     
                END IF;
                
                IF v_item.reqtype = 4127 AND v_item.stat <> 7
                THEN
                v_status := '7';
                ELSE
                v_status := '40';
                END IF;     
				
                -- reset allocated fl and planned ship date in t504a
				gm_pkg_op_loaner_allocation.gm_loaner_allocation_update(v_item.proddet, 'CLEAR', p_userid); 
				
           END LOOP;
		
		UPDATE t526_product_request_detail
		   SET c526_status_fl = v_status, --Cancelled
		       c526_last_updated_by = p_userid,
		       c526_last_updated_date = CURRENT_DATE,
		       c526_approve_reject_by = p_userid,
		       c526_approve_reject_date = CURRENT_DATE
		 WHERE c526_product_request_detail_id IN (SELECT token FROM v_in_list) 
		   AND c526_void_fl IS NULL; 	
	
		   FOR v_item IN v_cur
            LOOP
            -- call the priority engine 
				gm_pkg_op_ln_priority_engine.gm_sav_main_ln_set_priority(v_item.prodreqid,p_userid);
			END LOOP;
	END gm_sav_loaner_req_rejt;
--
/*********************************
	Purpose: This function is used to check set availability in loaner program.
**********************************/
	FUNCTION chk_loaner_set_avail(
		p_setid   t7104_case_set_information.C207_SET_ID%TYPE
	)
		RETURN VARCHAR2
	IS
		v_count		   NUMBER := 0;
	BEGIN
		BEGIN
			SELECT COUNT(1) INTO v_count 
			  FROM t207b_loaner_set_map 
			 WHERE c207_consign_set_id = p_setid 
			   AND C207B_VOID_FL IS NULL;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				RETURN 'N';
		END;

		IF v_count > 0 THEN
			RETURN 'Y';
		ElSE
			RETURN 'N';
		END IF;
		
	END chk_loaner_set_avail;
--
/*********************************
	Purpose: This Procedure is used to save the the approved quantity.
**********************************/
	PROCEDURE gm_sav_req_item_qty(
	p_input_string  IN VARCHAR2
  , p_userid	 	IN T526_PRODUCT_REQUEST_DETAIL.C526_LAST_UPDATED_BY%TYPE
	)AS
	v_string		VARCHAR2(2000) := p_input_string;
   	v_substring		VARCHAR2(2000);
    v_req_det_id	T526_PRODUCT_REQUEST_DETAIL.C526_PRODUCT_REQUEST_DETAIL_ID%TYPE;
	v_part_num		T526_PRODUCT_REQUEST_DETAIL.C205_PART_NUMBER_ID%TYPE;
	v_qty			T526_PRODUCT_REQUEST_DETAIL.C526_QTY_REQUESTED%TYPE;
	
   BEGIN
	   WHILE INSTR(v_string,'|') <> 0
	   LOOP
	   		v_substring := substr(v_string,0,INSTR(v_string,'|') - 1);
	   		v_string 	:= substr(v_string,INSTR(v_string,'|') + 1);
	   		
	   		v_req_det_id := null;
	   		v_part_num	 := null;
	   		v_qty		 := null;
	   		
	  		v_req_det_id := substr(v_substring,0,INSTR(v_substring,',') - 1); 
	   		v_substring := substr(v_substring,INSTR(v_substring,',') + 1);
	   		v_part_num	:= substr(v_substring,0,INSTR(v_substring,',') - 1);
	   		v_substring := substr(v_substring,INSTR(v_substring,',') + 1);
	   		v_qty		:= TO_NUMBER(v_substring);
	   		
	   	UPDATE T526_PRODUCT_REQUEST_DETAIL 
		   SET C526_QTY_REQUESTED	  = v_qty
		   	 , C526_LAST_UPDATED_BY   = p_userid
			 , C526_LAST_UPDATED_DATE = CURRENT_DATE
		 WHERE C526_PRODUCT_REQUEST_DETAIL_ID  = v_req_det_id
		 AND C526_VOID_FL IS NULL ;	
	   END LOOP;
	END gm_sav_req_item_qty;
--

END gm_pkg_sm_loaner_allocation;
/
