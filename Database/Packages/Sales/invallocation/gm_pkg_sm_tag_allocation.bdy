/* Formatted on 2011/12/19 12:09 (Formatter Plus v4.8.0) */
--@"C:\Database\Packages\Sales\invallocation\gm_pkg_sm_tag_allocation.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_sm_tag_allocation
IS
--
/*********************************
   Purpose: This procedure is used to initiate tag allocation job.
**********************************/
--
	PROCEDURE gm_init_tag_alloc_job
	AS
		-- Cursor to fetch the data from t7104 where tag id is null to allocate new tags.
		CURSOR c_tags_tbd
		IS
			SELECT	 t7100.c7100_case_info_id caseinfoid, t7100.c7100_surgery_date surgerydt
				FROM t7100_case_information t7100
			   WHERE t7100.c7100_case_info_id IN (
						 SELECT DISTINCT t7104.c7100_case_info_id
									FROM t7104_case_set_information t7104
								   WHERE t7104.c7104_void_fl IS NULL
									 AND t7104.c5010_tag_id IS NULL
									 AND c526_product_request_detail_id IS NULL
									 AND t7104.c7104_shipped_del_fl IS NULL)
				 AND t7100.c901_case_status = 11091   -- Active
				 AND t7100.c901_type     = 1006503    -- CASE
				 AND t7100.c7100_void_fl IS NULL
			ORDER BY t7100.c7100_surgery_date;

		v_caseinfoid   t7100_case_information.c7100_case_info_id%TYPE;
	BEGIN
		FOR v_rec IN c_tags_tbd
		LOOP
			SELECT	   t7100.c7100_case_info_id
				  INTO v_caseinfoid
				  FROM t7100_case_information t7100
				 WHERE t7100.c7100_case_info_id = v_rec.caseinfoid
			FOR UPDATE;

			gm_init_tagallocation (v_rec.caseinfoid, 30301);
		END LOOP;
	END gm_init_tag_alloc_job;

--

	/*********************************
   Purpose: This procedure is used to initiate tag allocation.
**********************************/
--
	PROCEDURE gm_init_tagallocation (
		p_caseinfo_id	IN	 t7100_case_information.c7100_case_info_id%TYPE
	  , p_userid		IN	 t7100_case_information.c7100_last_updated_by%TYPE
	)
	AS
		v_caseinfo	   TYPES.cursor_type;
	BEGIN
		gm_sav_settag_map (p_caseinfo_id, p_userid);
	END gm_init_tagallocation;

--
/*********************************
   Purpose: This procedure is used to save case set information.
**********************************/
--
	PROCEDURE gm_sav_settag_map (
		p_caseinfo_id	IN	 t7100_case_information.c7100_case_info_id%TYPE
	  , p_userid		IN	 t7100_case_information.c7100_last_updated_by%TYPE
	)
	AS
		CURSOR cur_sets
		IS
			SELECT t7104.c7104_case_set_id casesetid, t7104.c207_set_id setid, t7104.c7100_case_info_id caseinfoid
				 , t7104.c7104_tag_lock_from_date lockfromdt, t7104.c7104_tag_lock_to_date locktodt
			  FROM t7104_case_set_information t7104
			 WHERE t7104.c7100_case_info_id = p_caseinfo_id
			   AND t7104.c7104_void_fl IS NULL
			   AND t7104.c5010_tag_id IS NULL
			   AND t7104.c7104_shipped_del_fl IS NULL
			   AND c526_product_request_detail_id IS NULL;

		v_caserepid    t7100_case_information.c703_sales_rep_id%TYPE;
		v_caseacctid   t7100_case_information.c704_account_id%TYPE;
		v_casedistid   t7100_case_information.c701_distributor_id%TYPE;
		v_tagid 	   t7104_case_set_information.c5010_tag_id%TYPE;
		v_settype	   t7104_case_set_information.c901_set_location_type%TYPE;
		v_setid 	   t7104_case_set_information.c207_set_id%TYPE;
	BEGIN
		v_caserepid := get_case_rep_id (p_caseinfo_id, NULL);
		v_caseacctid := get_case_acct_id (p_caseinfo_id, NULL);
		v_casedistid := get_distributor_id (v_caserepid);

		FOR v_rec IN cur_sets
		LOOP
			SELECT	   t7104.c207_set_id casesetid
				  INTO v_setid
				  FROM t7104_case_set_information t7104
				 WHERE t7104.c7104_case_set_id = v_rec.casesetid AND t7104.c7104_void_fl IS NULL
				   AND t7104.c7104_shipped_del_fl IS NULL
			FOR UPDATE;

			gm_sav_self_tags_tbd (v_setid
								, v_rec.lockfromdt
								, v_rec.locktodt
								, v_caserepid
								, v_casedistid
								, v_caseacctid
								, v_tagid
								, v_settype
								 );

			IF v_tagid IS NOT NULL
			THEN
				gm_pkg_sm_setmgmt_txn.gm_sav_ship_dtl (v_rec.caseinfoid, v_rec.casesetid, v_tagid, v_settype, p_userid);
			END IF;

			IF v_tagid IS NULL
			THEN
				gm_rel_self_area_tags (v_rec.casesetid
									 , v_setid
									 , v_rec.lockfromdt
									 , v_rec.locktodt
									 , v_caserepid
									 , v_casedistid
									 , v_caseacctid
									 , p_userid
									 , v_tagid
									 , v_settype
									  );
			END IF;

			/*	IF v_tagid IS NULL
				THEN
					gm_sav_area_tags_tbd (v_rec.casesetid
										, v_setid
										, v_rec.lockfromdt
										, v_rec.locktodt
										, v_caserepid
										, v_casedistid
										, v_caseacctid
										, p_userid
										, v_tagid
										, v_settype
										 );
				END IF;
				*/
			IF v_tagid IS NOT NULL
			THEN
				gm_pkg_sm_setmgmt_txn.gm_sav_sets_tag_info (v_rec.casesetid, v_setid, v_settype, v_tagid, NULL, NULL, 30301);
			END IF;
		END LOOP;
	END gm_sav_settag_map;

--
/*********************************
   Purpose: This procedure is used to return self tagids.
**********************************/
--
	PROCEDURE gm_sav_self_tags_tbd (
		p_setid 	   IN		t7104_case_set_information.c207_set_id%TYPE
	  , p_lockfromdt   IN		t7104_case_set_information.c7104_tag_lock_from_date%TYPE
	  , p_locktodt	   IN		t7104_case_set_information.c7104_tag_lock_to_date%TYPE
	  , p_caserepid    IN		t7100_case_information.c703_sales_rep_id%TYPE
	  , p_casedistid   IN		t7100_case_information.c701_distributor_id%TYPE
	  , p_caseacctid   IN		t7100_case_information.c704_account_id%TYPE
	  , p_tagid 	   OUT		t7104_case_set_information.c5010_tag_id%TYPE
	  , p_settype	   OUT		t7104_case_set_information.c901_set_location_type%TYPE
	)
	AS
		--Cursor to fetch only the lock inventory for the respective account
		CURSOR c_locktags
		IS
			SELECT t5010.c5010_tag_id tagid
			  FROM t5010_tag t5010, v5010a_tag_attribute v5010a
			 WHERE t5010.c207_set_id = p_setid
			   AND t5010.c5010_void_fl IS NULL
			   AND t5010.c901_status = 51012   --Active
			   AND t5010.c901_location_type = 4120
			   AND t5010.c5010_tag_id = v5010a.c5010_tag_id
			   AND t5010.c704_account_id = p_caseacctid
			   AND v5010a.c5010_decommissioned = 'N'
			   AND v5010a.c5010_lockset = 'Y';

		--Cursor to fetch only those self tags which are with respective account
		CURSOR c_selfaccttags
		IS
			SELECT t5010.tagid, t5010.decommissioned
				 , t5010.lockset
			  FROM (SELECT t5010.c5010_tag_id tagid, t5010.c5010_location_id distid
						 , v5010a.c5010_decommissioned decommissioned, v5010a.c5010_lockset lockset
					  FROM t5010_tag t5010, v5010a_tag_attribute v5010a
					 WHERE t5010.c207_set_id = p_setid
					   AND t5010.c5010_void_fl IS NULL
					   AND t5010.c901_status = 51012   --Active
					   AND t5010.c5010_location_id = p_casedistid
					   AND t5010.c901_location_type = 4120
					   AND t5010.c5010_sub_location_id = p_caseacctid
					   AND t5010.c901_sub_location_type = 11301
					   AND t5010.c5010_tag_id = v5010a.c5010_tag_id(+)) t5010
			 WHERE NVL (t5010.decommissioned, 'N') = 'N' AND NVL (t5010.lockset, 'N') = 'N';

		--Cursor to fetch those tags which are with same distributor but not with same account to avoid extra checks.
		CURSOR c_selfareatags
		IS
			SELECT	 t5010.tagid, DECODE (t5010.sublocid, p_caserepid, 1, 2) locseq, t5010.decommissioned
				   , t5010.lockset,GET_DISTANCE (caserep_lat, caserep_lng, ship_lat, ship_long) ship_dist,
  					GET_DISTANCE (caserep_lat, caserep_lng, bill_lat, bill_long) bill_dist
				FROM (SELECT t5010.c5010_tag_id tagid, t5010.c5010_sub_location_id sublocid
						   , v5010a.c5010_decommissioned decommissioned, v5010a.c5010_lockset lockset
			               , DECODE (t5010.c901_sub_location_type, 50222, get_latitude (t5010.c106_address_id, 'ADDR', ''), 50221 ,get_latitude ( t5010.c5010_sub_location_id, 'DIST', ''), 11301, get_latitude (t5010.c5010_sub_location_id, 'ACC', '')) ship_lat 
			               , DECODE (t5010.c901_sub_location_type, 50222, get_latitude (t5010.c106_address_id , 'ADDR', '') , 50221, get_latitude (t5010.c5010_sub_location_id, 'DIST', 'BILL'), 11301, get_latitude (t5010.c5010_sub_location_id , 'ACC', 'BILL')) bill_lat
			               , DECODE (t5010.c901_sub_location_type, 50222,get_longitude (t5010.c106_address_id, 'ADDR', '') , 50221, get_longitude (t5010.c5010_sub_location_id,'DIST', '') , 11301, get_longitude ( t5010.c5010_sub_location_id, 'ACC', '')) ship_long
			               , DECODE (t5010.c901_sub_location_type, 50222, get_longitude (t5010.c106_address_id, 'ADDR', ''), 50221 , get_longitude (t5010.c5010_sub_location_id, 'DIST', 'BILL'), 11301 , get_longitude (t5010.c5010_sub_location_id, 'ACC', 'BILL')) bill_long
			               , get_latitude (get_address_id (p_caserepid) , 'ADDR', '') CASEREP_LAT
			               , get_longitude (get_address_id (p_caserepid), 'ADDR', '') CASEREP_LNG
               
						FROM t5010_tag t5010, v5010a_tag_attribute v5010a
					   	WHERE t5010.c207_set_id = p_setid
						 AND t5010.c5010_void_fl IS NULL
						 AND t5010.c901_status = 51012	 --Active
						 AND t5010.c5010_location_id = p_casedistid
						 AND t5010.c901_location_type = 4120
						 AND NVL (t5010.c5010_sub_location_id, -9999) != 
						 p_caseacctid	-- useful to avoid extra check for those tags which has been already fetch during above cursor.
						 AND t5010.c5010_tag_id = v5010a.c5010_tag_id(+)) t5010
			   WHERE NVL (t5010.decommissioned, 'N') = 'N' AND NVL (t5010.lockset, 'N') = 'N'
			ORDER BY locseq,nvl(ship_dist,bill_dist) ;

		v_tagid 	   t7104_case_set_information.c5010_tag_id%TYPE;
	BEGIN
		v_tagid 	:= NULL;
		p_settype	:= NULL;

		FOR v_rec IN c_locktags
		LOOP
			v_tagid 	:= v_rec.tagid;
			gm_chk_avail_tag (v_tagid, p_lockfromdt, p_locktodt);

			IF v_tagid IS NOT NULL
			THEN
				p_tagid 	:= v_tagid;
				p_settype	:= 11375;
				EXIT;
			END IF;
		END LOOP;

		IF p_tagid IS NULL
		THEN
			FOR v_rec IN c_selfaccttags
			LOOP
				v_tagid 	:= v_rec.tagid;
				gm_chk_avail_tag (v_tagid, p_lockfromdt, p_locktodt);

				IF v_tagid IS NOT NULL
				THEN
					p_tagid 	:= v_tagid;
					p_settype	:= 11375;
					EXIT;
				END IF;
			END LOOP;
		END IF;

		IF p_tagid IS NULL
		THEN
			FOR v_rec IN c_selfareatags
			LOOP
				v_tagid 	:= v_rec.tagid;
				gm_chk_avail_tag (v_tagid, p_lockfromdt, p_locktodt);

				IF v_tagid IS NOT NULL
				THEN
					p_tagid 	:= v_tagid;
					p_settype	:= 11375;
					EXIT;
				END IF;
			END LOOP;
		END IF;
	END gm_sav_self_tags_tbd;

--
/*********************************
   Purpose: This procedure is used to return self tagids which are allocated to area reps.
**********************************/
--
	PROCEDURE gm_rel_self_area_tags (
		p_casesetid    IN		t7104_case_set_information.c7104_case_set_id%TYPE
	  , p_setid 	   IN		t7104_case_set_information.c207_set_id%TYPE
	  , p_lockfromdt   IN		t7104_case_set_information.c7104_tag_lock_from_date%TYPE
	  , p_locktodt	   IN		t7104_case_set_information.c7104_tag_lock_to_date%TYPE
	  , p_caserepid    IN		t7100_case_information.c703_sales_rep_id%TYPE
	  , p_casedistid   IN		t7100_case_information.c701_distributor_id%TYPE
	  , p_caseacctid   IN		t7100_case_information.c704_account_id%TYPE
	  , p_userid	   IN		t7100_case_information.c7100_last_updated_by%TYPE
	  , p_tagid 	   OUT		t7104_case_set_information.c5010_tag_id%TYPE
	  , p_settype	   OUT		t7104_case_set_information.c901_set_location_type%TYPE
	)
	AS
		v_tagid 	   t5010_tag.c5010_tag_id%TYPE;

		--Cursor to fetch those tags which are with same distributor and check those if they are allocated to diff. rep.
		CURSOR c_selftag
		IS
			SELECT t5010.tagid, t5010.decommissioned, t5010.lockset,
        		GET_DISTANCE (caserep_lat, caserep_lng, ship_lat, ship_long) ship_dist,
        		GET_DISTANCE ( caserep_lat, caserep_lng, bill_lat, bill_long) bill_dist
			  FROM (SELECT t5010.c5010_tag_id tagid, t5010.c5010_sub_location_id sublocid
						  , v5010a.c5010_decommissioned decommissioned, v5010a.c5010_lockset lockset
			              , DECODE ( t5010.c901_sub_location_type, 50222, get_latitude (t5010.c106_address_id, 'ADDR', ''), 50221 ,get_latitude ( t5010.c5010_sub_location_id, 'DIST', ''), 11301, get_latitude (t5010.c5010_sub_location_id, 'ACC', '')) ship_lat 
			              , DECODE (t5010.c901_sub_location_type, 50222, get_latitude (t5010.c106_address_id , 'ADDR', '') , 50221, get_latitude (t5010.c5010_sub_location_id, 'DIST', 'BILL'), 11301, get_latitude (t5010.c5010_sub_location_id , 'ACC', 'BILL')) bill_lat
			              , DECODE (t5010.c901_sub_location_type, 50222,get_longitude (t5010.c106_address_id, 'ADDR', '') , 50221, get_longitude (t5010.c5010_sub_location_id,'DIST', '') , 11301, get_longitude ( t5010.c5010_sub_location_id, 'ACC', '')) ship_long
			              , DECODE ( t5010.c901_sub_location_type, 50222, get_longitude (t5010.c106_address_id, 'ADDR', ''), 50221 , get_longitude (t5010.c5010_sub_location_id, 'DIST', 'BILL'), 11301 , get_longitude (t5010.c5010_sub_location_id, 'ACC', 'BILL')) bill_long
			              , get_latitude (get_address_id (p_caserepid) , 'ADDR', '') CASEREP_LAT
			              , get_longitude (get_address_id (p_caserepid), 'ADDR', '') CASEREP_LNG
					  FROM t5010_tag t5010, v5010a_tag_attribute v5010a
					 WHERE t5010.c207_set_id = p_setid
					   AND t5010.c5010_void_fl IS NULL
					   AND t5010.c901_status = 51012   --Active
					   AND t5010.c5010_location_id = p_casedistid
					   AND t5010.c901_location_type = 4120
					   AND t5010.c5010_tag_id = v5010a.c5010_tag_id(+)) t5010
			 WHERE NVL (t5010.decommissioned, 'N') = 'N' AND NVL (t5010.lockset, 'N') = 'N'
       ORDER BY NVL(ship_dist,bill_dist) ;
	BEGIN
		p_tagid 	:= NULL;
		p_settype	:= NULL;

		FOR v_rec IN c_selftag
		LOOP
			v_tagid 	:= v_rec.tagid;
			gm_rel_area_tags (p_setid, p_lockfromdt, p_locktodt, p_casedistid, p_caseacctid, p_userid, v_tagid);

			IF v_tagid IS NOT NULL
			THEN
				p_tagid 	:= v_tagid;
				p_settype	:= 11375;
				EXIT;
			END IF;
		END LOOP;
	END gm_rel_self_area_tags;

/*********************************
   Purpose: This procedure is used to return self tagids which are allocated to area reps.
   Also it is useful to update the tags with null and voiding of shipping info.
**********************************/
--
	PROCEDURE gm_rel_area_tags (
		p_setid 	   IN		t7104_case_set_information.c207_set_id%TYPE
	  , p_lockfromdt   IN		t7104_case_set_information.c7104_tag_lock_from_date%TYPE
	  , p_locktodt	   IN		t7104_case_set_information.c7104_tag_lock_to_date%TYPE
	  , p_casedistid   IN		t7100_case_information.c701_distributor_id%TYPE
	  , p_caseacctid   IN		t7100_case_information.c704_account_id%TYPE
	  , p_userid	   IN		t7100_case_information.c7100_last_updated_by%TYPE
	  , p_tagid 	   IN OUT	t7104_case_set_information.c5010_tag_id%TYPE
	)
	AS
		v_casesetid    VARCHAR2 (20);
		v_shipout_fl   VARCHAR2 (1);
		v_tagid 	   t5010_tag.c5010_tag_id%TYPE;
	BEGIN
		v_tagid 	:= p_tagid;
		p_tagid 	:= NULL;

		SELECT	   t7104.c7104_case_set_id casesetid
			  INTO v_casesetid
			  FROM t7100_case_information t7100, t7104_case_set_information t7104
			 WHERE t7100.c901_case_status = 11091	--Active
			   AND t7100.c901_type    =  1006503  -- CASE
			   AND t7100.c7100_void_fl IS NULL
			   AND t7100.c7100_case_info_id = t7104.c7100_case_info_id
			   AND ((    TRUNC (t7104.c7104_tag_lock_to_date) >= TRUNC(p_lockfromdt)
					 AND TRUNC (t7104.c7104_tag_lock_to_date) <= TRUNC(p_locktodt)
				   )
				OR (	 TRUNC (t7104.c7104_tag_lock_from_date) >= TRUNC(p_lockfromdt)
					 AND TRUNC (t7104.c7104_tag_lock_from_date) <= TRUNC(p_locktodt)
				   )
			   )
			   AND t7104.c901_set_location_type = 11378   -- AreaSet
			   AND t7104.c207_set_id = p_setid
			   AND t7104.c5010_tag_id = v_tagid
			   AND t7104.c7104_shipped_del_fl IS NULL
			   AND t7104.c7104_void_fl IS NULL
			   AND ROWNUM = 1
		FOR UPDATE;

		v_shipout_fl := get_shipout_fl (v_casesetid);

		IF v_shipout_fl != 'Y'
		THEN
			--update gm_sav_settag_info
			gm_pkg_sm_setmgmt_txn.gm_sav_sets_tag_info (v_casesetid, p_setid, NULL, NULL, NULL, NULL, 30301);

			--void the shipping record
			UPDATE t907_shipping_info
			   SET c907_void_fl = 'Y'
				 , c907_last_updated_by = p_userid
				 , c907_last_updated_date = SYSDATE
			 WHERE c907_ref_id = v_casesetid 
			   AND NVL(c907_status_fl,0) < 40 
			   AND c901_source IN (11385,11388);

			p_tagid 	:= v_tagid;
		END IF;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			p_tagid 	:= NULL;
	END gm_rel_area_tags;

--
/*********************************
   Purpose: This procedure is used to return area tagids from area reps inventory.
**********************************/
--
	PROCEDURE gm_sav_area_tags_tbd (
		p_casesetid    IN		t7104_case_set_information.c7104_case_set_id%TYPE
	  , p_setid 	   IN		t7104_case_set_information.c207_set_id%TYPE
	  , p_lockfromdt   IN		t7104_case_set_information.c7104_tag_lock_from_date%TYPE
	  , p_locktodt	   IN		t7104_case_set_information.c7104_tag_lock_to_date%TYPE
	  , p_caserepid    IN		t7100_case_information.c703_sales_rep_id%TYPE
	  , p_casedistid   IN		t7100_case_information.c701_distributor_id%TYPE
	  , p_caseacctid   IN		t7100_case_information.c704_account_id%TYPE
	  , p_userid	   IN		t7100_case_information.c7100_last_updated_by%TYPE
	  , p_tagid 	   OUT		t7104_case_set_information.c5010_tag_id%TYPE
	  , p_settype	   OUT		t7104_case_set_information.c901_set_location_type%TYPE
	)
	AS
		CURSOR c_areatag
		IS
			SELECT t5010.tagid, t5010.currlcndistid, t5010.currlcn, t5010.decommissioned, t5010.lockset
				 , t5010.lcndistid, t5010.lcn
			  FROM (SELECT t5010.c5010_tag_id tagid
						 , NVL (t5010.c5010_sub_location_id, t5010.c5010_location_id) currlcndistid
						 , DECODE (t5010.c901_sub_location_type, 50222, 4121, 11301, 4122, 4120) currlcn
						 , v5010a.c5010_decommissioned decommissioned, v5010a.c5010_lockset lockset
						 , t5010.c5010_location_id lcndistid, t5010.c901_location_type lcn
					  FROM t5010_tag t5010, v5010a_tag_attribute v5010a
					 WHERE t5010.c207_set_id = p_setid
					   AND t5010.c5010_void_fl IS NULL
					   AND t5010.c901_status = 51012   --active
					   AND t5010.c5010_location_id IN (
							   SELECT c701_distributor_id
								 FROM t701_distributor
								WHERE c701_region = (SELECT c701_region
													   FROM t701_distributor
													  WHERE c701_distributor_id = p_casedistid)
								  AND c701_distributor_id != p_casedistid)
					   AND t5010.c901_location_type = 4120
					   AND t5010.c5010_tag_id = v5010a.c5010_tag_id(+)) t5010
			 WHERE NVL (t5010.decommissioned, 'N') = 'N' AND NVL (t5010.lockset, 'N') = 'N';

		v_tagid 	   t7104_case_set_information.c5010_tag_id%TYPE;
		v_currlcndistid VARCHAR2 (20);
		v_currlcn	   NUMBER;
		v_lcndistid    VARCHAR2 (20);
		v_lcn		   NUMBER;
		v_addressid    NUMBER;
		--v_toaddressid  NUMBER;
		v_shipid	   NUMBER;
	BEGIN
		p_tagid 	:= NULL;
		p_settype	:= NULL;

		FOR v_rec IN c_areatag
		LOOP
			v_tagid 	:= v_rec.tagid;
			gm_chk_avail_tag (v_tagid, p_lockfromdt, p_locktodt);

			IF v_tagid IS NOT NULL
			THEN
				--v_casesrepid := get_case_rep_id (NULL, p_casesetid);
				v_currlcndistid := v_rec.currlcndistid;
				v_currlcn	:= v_rec.currlcn;
				v_lcndistid := v_rec.lcndistid;
				v_lcn		:= v_rec.lcn;
				v_addressid := NVL (get_address_id (p_caserepid), 0);
				--v_toaddressid := NVL (get_address_id (v_lcndistid), 0);
				gm_pkg_sm_setmgmt_txn.gm_sav_case_shipinfo (p_casesetid
														  , 11385
														  , p_caserepid
														  , 4121
														  , v_currlcndistid
														  , v_currlcn
														  , '30'
														  , 30301
														  , v_addressid
														  , v_shipid
														   );
				gm_pkg_sm_setmgmt_txn.gm_sav_case_shipinfo
										(p_casesetid
									   , 11388
									   , v_lcndistid
									   , v_lcn
									   , p_caserepid
									   , 4121
									   , NULL
									   , 30301
									   , NULL	-- Shipback to owner and select the address from view while displaying it.
									   , v_shipid
										);
				p_tagid 	:= v_tagid;
				p_settype	:= 11378;
				EXIT;
			END IF;
		END LOOP;
	END gm_sav_area_tags_tbd;

--
/*********************************
   Purpose: This procedure is used to return area tagids from area reps inventory.
**********************************/
--
	PROCEDURE gm_chk_avail_tag (
		p_tagid 	   IN OUT	t7104_case_set_information.c5010_tag_id%TYPE
	  , p_lockfromdt   IN		t7104_case_set_information.c7104_tag_lock_from_date%TYPE
	  , p_locktodt	   IN		t7104_case_set_information.c7104_tag_lock_to_date%TYPE
	)
	AS
		v_count 	   NUMBER:=0;
	BEGIN
		SELECT COUNT (1)
		  INTO v_count
		  FROM t7100_case_information t7100, t7104_case_set_information t7104
		 WHERE t7100.c901_case_status = 11091	--Active
		   AND t7100.c901_type  = 1006503   -- CASE
		   AND t7100.c7100_void_fl IS NULL
		   AND t7100.c7100_case_info_id = t7104.c7100_case_info_id
		   AND t7104.c5010_tag_id = p_tagid
		   AND (   (	TRUNC (t7104.c7104_tag_lock_to_date) >= TRUNC(p_lockfromdt)
					AND TRUNC (t7104.c7104_tag_lock_to_date) <= TRUNC(p_locktodt)
				   )
				OR (	TRUNC (t7104.c7104_tag_lock_from_date) >= TRUNC(p_lockfromdt)
					AND TRUNC (t7104.c7104_tag_lock_from_date) <= TRUNC(p_locktodt)
				   )
			   )
		   AND t7104.c7104_void_fl IS NULL;

		IF v_count > 0
		THEN
			p_tagid 	:= NULL;
		ELSE
			p_tagid 	:= p_tagid;
		END IF;
	EXCEPTION
		WHEN OTHERS
		THEN
			p_tagid 	:= NULL;
	END gm_chk_avail_tag;

--
/*********************************
   Purpose: This procedure is used to return reps with case and case setid
**********************************/
--
	PROCEDURE gm_fch_all_caserep_dtl (
		p_out_caserepdtl   OUT	 TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_out_caserepdtl
		 FOR
			 SELECT   t7104.c7104_case_set_id casesetid, t7104.c207_set_id setid
					, get_set_name (t7104.c207_set_id) setname, t7100.c703_sales_rep_id caserepid
					, get_rep_name (t7100.c703_sales_rep_id) repname, t7100.c704_account_id caseacctid
					, get_account_name (t7100.c704_account_id) acctname
					, get_code_name (t7104.c901_set_location_type) lcntype, t7100.c7100_case_id caseid
					, get_cs_ship_email (4121, t7100.c703_sales_rep_id) repemail
				 FROM t7104_case_set_information t7104, t7100_case_information t7100
				WHERE t7104.c7104_void_fl IS NULL
				  AND t7104.c7104_shipped_del_fl IS NULL
				  AND t7104.c7100_case_info_id = t7100.c7100_case_info_id
				  AND t7100.c901_case_status = 11091   -- Active
				  AND t7100.c901_type  = 1006503     -- CASE
				  AND t7100.c7100_void_fl IS NULL
				  AND TRUNC (t7100.c7100_surgery_date) >= TRUNC (SYSDATE)
			 ORDER BY t7100.c703_sales_rep_id, t7100.c7100_surgery_date;
	END gm_fch_all_caserep_dtl;

--
/*********************************
   Purpose: This procedure is used to return the current and previous status of tags.
**********************************/
--
	PROCEDURE gm_fch_all_tagset_status (
		p_out_settagdtl   OUT	TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_out_settagdtl
		 FOR
		  SELECT curr_t7104a.infologid, curr_t7104a.casesetid, DECODE (curr_t7104a.currprodreqid, NULL, curr_t7104a.currtag,
		    DECODE (curretchid, ' ', 'LPA', curretchid)) currtag, DECODE (prev_t7104a.prevprodreqid, NULL,
		    prev_t7104a.prevtagid,prev_t7104a.prevetchid) prevtagid, prev_t7104a.prevprodreqid,
		    curr_t7104a.currprodreqid, curr_t7104a.currtype, curr_t7104a.currvalue
		   FROM
		    (
		         SELECT t7104a.c7104a_case_set_info_log_id infologid, t7104a.c7104_case_set_id casesetid, t7104a.c5010_tag_id
		            currtag, t7104a.c526_product_request_detail_id currprodreqid, t7104a.c901_type currtype
		          , t7104a.c7104a_value currvalue, get_cs_fch_loaner_etchid (t504a.c504_consignment_id) curretchid
		           FROM t7104a_case_set_info_log t7104a, t504a_loaner_transaction t504a, (
		                 SELECT MAX (t7104a.c7104a_case_set_info_log_id) infologid, t7104a.c7104_case_set_id
		                   FROM t7104a_case_set_info_log t7104a
		                  WHERE t7104a.c7104a_value = 'N'
		                   -- AND t7104a.c7104_case_set_id IN (2051, 2052)
		               GROUP BY t7104a.c7104_case_set_id
		            )
		            t7104a_max
		          WHERE t7104a.c7104a_case_set_info_log_id    = t7104a_max.infologid
		            AND t7104a.c526_product_request_detail_id = t504a.c526_product_request_detail_id (+)
		            AND t504a.c504a_void_fl IS NULL
		            AND t7104a.c7104a_value = 'N'
		    )
		    curr_t7104a,
		    (
		         SELECT t7104a.c7104a_case_set_info_log_id infologid, t7104a.c7104_case_set_id casesetid, t7104a.c5010_tag_id
		            prevtagid, t7104a.c526_product_request_detail_id prevprodreqid, t7104a.c901_type prevtype
		          , t7104a.c7104a_value prevvalue, get_cs_fch_loaner_etchid (t504a.c504_consignment_id) prevetchid
		           FROM t7104a_case_set_info_log t7104a, t504a_loaner_transaction t504a, (
		                  --below query fetch second max infologid for each casesetid
		                 SELECT MAX (t7104a.infologid) infologid, t7104a.casesetid
		                   FROM
		                    (
		                    --below query fetch all infologid with first max infologid for each casesetid
		                         SELECT t7104a.infologid, t7104a.casesetid, (
		                                 SELECT MAX (t7104a.c7104a_case_set_info_log_id)
		                                   FROM t7104a_case_set_info_log t7104a
		                                  WHERE t7104a.c7104_case_set_id = t7104a.casesetid
		                                    AND NVL(t7104a.c901_type,-9999) != 11396 --deallocated tags are not considering for previous tag
		                               GROUP BY t7104a.c7104_case_set_id
		                            )
		                            maxinfologid
		                           FROM
		                            (
		                                 SELECT t7104a.c7104a_case_set_info_log_id infologid, t7104a.c7104_case_set_id
		                                    casesetid
		                                   FROM t7104a_case_set_info_log t7104a
		                                  WHERE NVL(t7104a.c901_type,-9999) != 11396
		                                  --  AND t7104a.c7104_case_set_id IN (2051, 2052)
		                            )
		                            t7104a
		                    )
		                    t7104a
		                  WHERE t7104a.infologid != t7104a.maxinfologid
		               GROUP BY t7104a.casesetid
		            )
		            t7104a_2nd_max
		          WHERE t7104a.c7104a_case_set_info_log_id    = t7104a_2nd_max.infologid
		            AND t7104a.c526_product_request_detail_id = t504a.c526_product_request_detail_id (+)
		            AND NVL(t7104a.c901_type,-9999) != 11396 --deallocated tag
		            --AND t504a.c504a_void_fl IS NULL
		            --this query is used to get previous(deallocated) loaner which is voided one.
		            --So, we no need to check void flag for this.
		    )
		    prev_t7104a
		  WHERE curr_t7104a.casesetid = prev_t7104a.casesetid(+)
		    AND curr_t7104a.currvalue = 'N';
	END gm_fch_all_tagset_status;
--
END gm_pkg_sm_tag_allocation;
/