/* Formatted on 2011/12/16 14:14 (Formatter Plus v4.8.0) */
--@"C:\Database\Packages\Sales\invallocation\gm_pkg_sm_setmgmt_txn.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_sm_setmgmt_txn
IS
--
/*********************************
   Purpose: This procedure is used to initiate tag allocation.
**********************************/
--
	PROCEDURE gm_sav_case_shipinfo (
		p_refid 	   IN		t907_shipping_info.c907_ref_id%TYPE
	  , p_source	   IN		t907_shipping_info.c901_source%TYPE
	  , p_shiptoid	   IN		t907_shipping_info.c907_ship_to_id%TYPE
	  , p_shipto	   IN		t907_shipping_info.c901_ship_to%TYPE
	  , p_shipfromid   IN		t907_shipping_info.c907_ship_from_id%TYPE
	  , p_shipfrom	   IN		t907_shipping_info.c901_ship_from%TYPE
	  , p_status_fl    IN		t907_shipping_info.c907_status_fl%TYPE
	  , p_userid	   IN		t7100_case_information.c7100_last_updated_by%TYPE
	  , p_addressid    IN		t907_shipping_info.c106_address_id%TYPE
	  , p_shipid	   IN OUT	VARCHAR2
	)
	AS
	v_company_id    t907_shipping_info.c1900_company_id%TYPE;
	v_plant_id      t907_shipping_info.c5040_plant_id%TYPE;
	BEGIN
		-- Getting the company and plant id
	SELECT NVL(GET_COMPID_FRM_CNTX(),GET_RULE_VALUE('DEFAULT_COMPANY','ONEPORTAL')), NVL(GET_PLANTID_FRM_CNTX(),GET_RULE_VALUE('DEFAULT_PLANT','ONEPORTAL'))
	      INTO v_company_id, v_plant_id
	      FROM DUAL;
		--
		UPDATE t907_shipping_info
		   SET c901_ship_to = p_shipto
			 , c907_ship_to_id = p_shiptoid
			 , c106_address_id = p_addressid
			 , c907_ship_from_id = p_shipfromid
			 , c901_ship_from = p_shipfrom
			 , c907_status_fl = p_status_fl
			 , c703_sales_rep_id = DECODE(p_shipto,4121,p_shiptoid,c703_sales_rep_id)
			 , c907_last_updated_by = p_userid
			 , c907_last_updated_date = CURRENT_DATE
		 WHERE c907_ref_id = p_refid AND c901_source = p_source AND c907_void_fl IS NULL;

		IF (SQL%ROWCOUNT = 0)
		THEN			
			INSERT INTO t907_shipping_info
						(c907_shipping_id, c907_ref_id, c907_ship_to_id, c901_ship_to, c907_ship_from_id
					   , c901_ship_from, c901_source, c907_status_fl, c106_address_id, c907_created_by
					   , c907_created_date, c703_sales_rep_id, c1900_company_id
					   , c5040_plant_id
						)
				 VALUES (s907_ship_id.NEXTVAL, p_refid, p_shiptoid, p_shipto, p_shipfromid
					   , p_shipfrom, p_source, p_status_fl, p_addressid, p_userid
					   , CURRENT_DATE, p_shiptoid, v_company_id
					   , v_plant_id
						);
		END IF;
	END gm_sav_case_shipinfo;

--
/*********************************
   Purpose: This procedure is used to save tag information.
**********************************/
	PROCEDURE gm_sav_sets_tag_info (
		p_casesetid    IN	t7104_case_set_information.c7104_case_set_id%TYPE
	  , p_setid 	   IN	t7104_case_set_information.c207_set_id%TYPE
	  , p_setloctype   IN	t7104_case_set_information.c901_set_location_type%TYPE
	  , p_tagid 	   IN	t7104_case_set_information.c5010_tag_id%TYPE
	  , p_reqdtl_id    IN	t7104_case_set_information.c526_product_request_detail_id%TYPE
	  , p_ship_del_fl  IN	t7104_case_set_information.c7104_shipped_del_fl%TYPE
	  , p_userid	   IN	t7104_case_set_information.c7104_last_updated_by%TYPE
	)
	AS
	BEGIN
		UPDATE t7104_case_set_information
		   SET c901_set_location_type = p_setloctype
			 , c5010_tag_id = p_tagid
			 , c526_product_request_detail_id = p_reqdtl_id
			 , c7104_shipped_del_fl = p_ship_del_fl
			 , c7104_last_updated_by = p_userid
			 , c7104_last_updated_date = SYSDATE
		 WHERE c7104_case_set_id = p_casesetid AND c207_set_id = p_setid AND c7104_void_fl IS NULL;
	END gm_sav_sets_tag_info;

/*********************************
   Purpose: This procedure is used to cancel the tag information.
**********************************/
	PROCEDURE gm_sav_cancel_tag (
		p_caseinfoid	 IN   t7100_case_information.c7100_case_info_id%TYPE
	  , p_cancelreason	 IN   t907_cancel_log.c901_cancel_cd%TYPE
	  , p_comments		 IN   t907_cancel_log.c907_comments%TYPE
	  , p_userid		 IN   t7104_case_set_information.c7104_last_updated_by%TYPE
	)
	AS
		CURSOR cur_sets
		IS
			SELECT t7104.c7104_case_set_id casesetid, t7104.c207_set_id setid, t7104.c7100_case_info_id caseinfoid
			     , decode(t526.c901_request_type,'4119','VDIHLN','VDLON') casetype
				 , t7104.c526_product_request_detail_id prodreqid, t7104.c5010_tag_id tagid
			  FROM t7104_case_set_information t7104,t526_product_request_detail t526 
			 WHERE t7104.c7100_case_info_id = p_caseinfoid AND t7104.c7104_void_fl IS NULL
               AND t7104.c526_product_request_detail_id(+) = t526.c526_product_request_detail_id 
               AND t526.c526_void_fl IS NULL;

		v_reqstatusfl  t526_product_request_detail.c526_status_fl%TYPE;
		v_count 	   NUMBER;
	BEGIN
		FOR v_rec IN cur_sets
		LOOP
			
			IF v_rec.tagid IS NULL AND v_rec.prodreqid IS NOT NULL
			THEN
				--we cannot void loaner request which is created for CSM case. 
				--So,before voiding CSM loaner request, we have to remove request from t7104 table.
				gm_pkg_sm_setmgmt_txn.gm_sav_sets_tag_info (v_rec.casesetid, v_rec.setid, NULL, NULL, NULL, NULL, p_userid);
				

				
				
				SELECT NVL (c526_status_fl, '-999')
                  INTO v_reqstatusfl
                  FROM t526_product_request_detail
                 WHERE c526_product_request_detail_id = v_rec.prodreqid;

				IF v_reqstatusfl != '30'
				THEN
					--Void Loaner request and shipping details.
					gm_pkg_common_cancel.gm_cm_sav_cancelrow (v_rec.prodreqid
															, p_cancelreason
															, v_rec.casetype
															, p_comments
															, p_userid
															 );
				END IF;
				IF(get_type(p_caseinfoid)= '1006504')   --FOR event
				THEN
					gm_pkg_common_cancel.gm_cm_sav_cancelrow (v_rec.prodreqid
															, p_cancelreason
															, 'CANEVE'
															, p_comments
															, p_userid
															 );
				END IF;
				
			ELSIF v_rec.tagid IS NOT NULL AND v_rec.prodreqid IS NULL
			THEN
			
				SELECT COUNT(1) INTO v_count
	              FROM t907_shipping_info
	             WHERE c907_ref_id IN (
	                       SELECT c907_ref_id FROM t907_shipping_info
	                       WHERE c907_ref_id = TO_CHAR (v_rec.casesetid)
	                            AND c901_source = 11385
	                            AND c907_status_fl = 40
	                            AND c907_void_fl IS NULL )
	               AND (NVL(c907_status_fl,0) < 40 OR c901_accept_status = 11462)	--Pending Receiving
	               AND c901_source = 11388
	               AND c907_void_fl IS NULL;
	  
				IF v_count > 0
				THEN
					UPDATE t7104_case_set_information
					   SET c7104_shipped_del_fl = 'Y'
						 , c7104_last_updated_by = p_userid
						 , c7104_last_updated_date = SYSDATE
					 WHERE c7104_case_set_id = v_rec.casesetid
					   AND c7104_void_fl IS NULL;
				ELSE
					UPDATE t907_shipping_info
					   SET c907_void_fl = 'Y'
						 , c907_last_updated_by = p_userid
						 , c907_last_updated_date = SYSDATE
					 WHERE c907_ref_id = TO_CHAR (v_rec.casesetid)
					   AND c907_status_fl != 40
					   AND c901_source IN (11385, 11388);
					
					gm_pkg_sm_setmgmt_txn.gm_sav_sets_tag_info (v_rec.casesetid, v_rec.setid, NULL, NULL, NULL, NULL, p_userid);
				END IF;
			END IF;
		END LOOP;
	END gm_sav_cancel_tag;

--
/*********************************
   Purpose: This procedure is used to reschedule the tag information.
**********************************/
	PROCEDURE gm_sav_reschedule_tag (
		p_caseinfoid   IN	t7100_case_information.c7100_case_info_id%TYPE
	  , p_userid	   IN	t7104_case_set_information.c7104_last_updated_by%TYPE
	)
	AS
		v_pcaseinfo_id t7100_case_information.c7100_case_info_id%TYPE;
		v_psurgery_dt  t7100_case_information.c7100_surgery_date%TYPE;
		v_surgery_dt   t7100_case_information.c7100_surgery_date%TYPE;
	BEGIN
		SELECT c7100_surgery_date surgery_dt, t7100.c7100_parent_case_info_id pcaseinfoid
		  INTO v_surgery_dt, v_pcaseinfo_id
		  FROM t7100_case_information t7100
		 WHERE t7100.c7100_case_info_id = p_caseinfoid
		   AND t7100.c901_case_status = 11091	-- Active
		   AND t7100.c901_type = 1006503 -- CASE
		   AND t7100.c7100_void_fl IS NULL;

		SELECT c7100_surgery_date surgery_dt
		  INTO v_psurgery_dt
		  FROM t7100_case_information t7100
		 WHERE t7100.c7100_case_info_id = v_pcaseinfo_id
		   AND t7100.c901_case_status = 11092	-- Rescheduled
		   AND t7100.c901_type = 1006503 -- CASE
		   AND t7100.c7100_void_fl IS NULL;

		--Date function code needs to be added.Xun
		IF (v_surgery_dt >= (v_psurgery_dt + 7))
		THEN
		    gm_sav_newcase_tags (p_caseinfoid, v_pcaseinfo_id, p_userid); --shipped delete tag are removed from new case
			gm_sav_cancel_tag (v_pcaseinfo_id, NULL, NULL, p_userid);
			--Comment for MNTTASK-7882 Auto Allocation
			--gm_pkg_sm_tag_allocation.gm_init_tagallocation (p_caseinfoid, p_userid);
		ELSE
			gm_sav_prevcase_tags (p_caseinfoid, v_pcaseinfo_id, p_userid);
			gm_sav_cancel_tag (v_pcaseinfo_id, NULL, NULL, p_userid);
		END IF;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			v_pcaseinfo_id := NULL;
	END gm_sav_reschedule_tag;

/*********************************
   Purpose: This procedure is used to delete the new case tag information if tag remove in previous case.
**********************************/
	PROCEDURE gm_sav_newcase_tags (
		p_caseinfoid	IN	 t7100_case_information.c7100_case_info_id%TYPE
	  , p_pcaseinfoid	IN	 t7100_case_information.c7100_case_info_id%TYPE
	  , p_userid		IN	 t7104_case_set_information.c7104_last_updated_by%TYPE
	)
	AS
		v_newsetid	   t7104_case_set_information.c207_set_id%TYPE;
		v_setid 	   t7104_case_set_information.c207_set_id%TYPE;
		v_tagid 	   t7104_case_set_information.c5010_tag_id%TYPE;
		v_reqdtlid	   t7104_case_set_information.c526_product_request_detail_id%TYPE;
		v_settype	   t7104_case_set_information.c901_set_location_type%TYPE;
	 	v_ship_del_fl  t7104_case_set_information.c7104_shipped_del_fl%TYPE;
	 	v_casesetid    t7104_case_set_information.c7104_case_set_id%TYPE; 

		CURSOR c_sets 
		IS
			SELECT	 t7104.c7104_case_set_id casesetid, t7104.c207_set_id setid
				FROM t7104_case_set_information t7104
			   WHERE t7104.c7100_case_info_id = p_pcaseinfoid
				 AND t7104.c7104_void_fl IS NULL
				 AND t7104.c7104_shipped_del_fl = 'Y'
			ORDER BY t7104.c207_set_id;

	BEGIN
		FOR v_rec IN c_sets
		LOOP
			  SELECT t7104.c7104_case_set_id INTO v_casesetid
				FROM t7104_case_set_information t7104
			   WHERE t7104.c7100_case_info_id = p_caseinfoid 
			     AND t7104.c7104_void_fl IS NULL
			     AND t7104.c207_set_id = v_rec.casesetid
			     AND t7104.c7104_shipped_del_fl IS NULL AND ROWNUM=1;
			     
			  UPDATE t7104_case_set_information
			     SET c7104_void_fl = 'Y'
			       , c7104_last_updated_by = p_userid
			       , c7104_last_updated_date = SYSDATE
			   WHERE c7104_case_set_id = v_casesetid
			     AND c7104_void_fl IS NULL;
		END LOOP;
	END gm_sav_newcase_tags;	
/*********************************
   Purpose: This procedure is used to save the previous case tag information.
**********************************/
	PROCEDURE gm_sav_prevcase_tags (
		p_caseinfoid	IN	 t7100_case_information.c7100_case_info_id%TYPE
	  , p_pcaseinfoid	IN	 t7100_case_information.c7100_case_info_id%TYPE
	  , p_userid		IN	 t7104_case_set_information.c7104_last_updated_by%TYPE
	)
	AS
		v_newsetid	   t7104_case_set_information.c207_set_id%TYPE;
		v_setid 	   t7104_case_set_information.c207_set_id%TYPE;
		v_tagid 	   t7104_case_set_information.c5010_tag_id%TYPE;
		v_reqdtlid	   t7104_case_set_information.c526_product_request_detail_id%TYPE;
		v_settype	   t7104_case_set_information.c901_set_location_type%TYPE;
	 	v_ship_del_fl  t7104_case_set_information.c7104_shipped_del_fl%TYPE;
		CURSOR c_newtags
		IS
			SELECT	 t7104.c7104_case_set_id casesetid, t7104.c207_set_id setid
				   , t7104.c7104_tag_lock_from_date fromdate, t7104.c7104_tag_lock_to_date todate 
				FROM t7104_case_set_information t7104
			   WHERE t7104.c7100_case_info_id = p_caseinfoid AND t7104.c7104_void_fl IS NULL
			     AND t7104.c7104_shipped_del_fl IS NULL
			ORDER BY t7104.c207_set_id;

		CURSOR c_sets (
			v_tagset   VARCHAR2
		)
		IS
			SELECT	 t7104.c7104_case_set_id casesetid, t7104.c207_set_id setid, t7104.c5010_tag_id tagid, t7104.c526_product_request_detail_id reqdtlid
				   , t7104.c901_set_location_type loctype, t7104.c7104_shipped_del_fl shipdelfl
				FROM t7104_case_set_information t7104
			   WHERE t7104.c7100_case_info_id = p_pcaseinfoid
				 AND t7104.c207_set_id = v_tagset
				 AND t7104.c7104_void_fl IS NULL
			ORDER BY t7104.c207_set_id;

		v_lockfromdt   t7104_case_set_information.c7104_tag_lock_from_date%TYPE;
		v_locktodt	   t7104_case_set_information.c7104_tag_lock_to_date%TYPE;
		v_casesetid	   t7104_case_set_information.c7104_case_set_id%TYPE;
	BEGIN
		FOR v_rec IN c_newtags
		LOOP
			-- required to lock the new case set info record
				SELECT t7104.c207_set_id
				  INTO v_newsetid
				  FROM t7104_case_set_information t7104
				 WHERE t7104.c7104_case_set_id = v_rec.casesetid AND t7104.c7104_void_fl IS NULL
			FOR UPDATE;	
		
			FOR v_set IN c_sets (v_newsetid)
			LOOP
				v_casesetid := v_set.casesetid;
				v_setid 	:= v_set.setid;
				v_tagid 	:= v_set.tagid;
				v_reqdtlid	:= v_set.reqdtlid;
				v_settype	:= v_set.loctype;
				v_ship_del_fl := v_set.shipdelfl;
				--Comment for MNTTASK-7882 Auto Allocation
				--gm_pkg_sm_tag_allocation.gm_chk_avail_tag (v_tagid, v_rec.fromdate, v_rec.todate);

				IF v_tagid IS NOT NULL
				THEN
				/*--Comment for MNTTASK-7882 Auto Allocation
					IF v_ship_del_fl = 'Y'
					THEN
						UPDATE t907_shipping_info
			 			   SET c907_ref_id = TO_CHAR(v_rec.casesetid) 
							 , c907_last_updated_by = p_userid
							 , c907_last_updated_date = SYSDATE
						 WHERE c907_ref_id = TO_CHAR(v_set.casesetid) 
						   AND c907_void_fl IS NULL
						   AND c901_source IN(11385,11388);	
						   
						UPDATE t7104_case_set_information
					       SET c7104_void_fl = 'Y'
					         , c7104_last_updated_by = p_userid
					         , c7104_last_updated_date = SYSDATE
					     WHERE c7104_case_set_id = v_set.casesetid
					       AND c7104_void_fl IS NULL;

					ELSE
					-- shipping info
					gm_sav_ship_dtl_frm_prvcase(v_rec.casesetid,v_casesetid,p_userid);											
					END IF;
					*/
					--Tag info
					gm_pkg_sm_setmgmt_txn.gm_sav_sets_tag_info (v_rec.casesetid
															  , v_setid
															  , v_settype
															  , v_tagid
															  , v_reqdtlid
															  , v_ship_del_fl
															  , p_userid
															   );
				END IF;
			END LOOP;
		END LOOP;
	END gm_sav_prevcase_tags;

--
/*********************************
   Purpose: This procedure is used to save the rescheduled case information.
**********************************/
	PROCEDURE gm_sav_ship_dtl (
		p_caseinfoid   IN	t7100_case_information.c7100_case_info_id%TYPE
	  , p_casesetid    IN	t7104_case_set_information.c7104_case_set_id%TYPE
	  , p_tagid 	   IN	t7104_case_set_information.c5010_tag_id%TYPE
	  , p_settype	   IN	t7104_case_set_information.c901_set_location_type%TYPE
	  , p_userid	   IN	t7104_case_set_information.c7104_last_updated_by%TYPE
	)
	AS
		v_currlcndistid t5010_tag.c5010_sub_location_id%TYPE;
		v_currlcn	   t5010_tag.c901_sub_location_type%TYPE;
		v_toreptype	   t5010_tag.c901_sub_location_type%TYPE;
		v_lcndistid    t5010_tag.c5010_location_id%TYPE;
		v_lcn		   t5010_tag.c901_location_type%TYPE;
		v_caserepid    t7100_case_information.c703_sales_rep_id%TYPE;
		v_addressid    NUMBER;
		v_shipid	   NUMBER;
		v_casedistid   t7100_case_information.c701_distributor_id%TYPE;
		v_caseacctid   t7100_case_information.c704_account_id%TYPE;
		v_tagrepid	   t7100_case_information.c703_sales_rep_id%TYPE;
		v_tagacctid    t7100_case_information.c704_account_id%TYPE;
		v_torepid	   t7100_case_information.c703_sales_rep_id%TYPE;
		v_toaddressid  NUMBER;
	BEGIN
		SELECT t7100.c701_distributor_id casedistid, t7100.c703_sales_rep_id caserepid, t7100.c704_account_id acctid
		  INTO v_casedistid, v_caserepid, v_caseacctid
		  FROM t7100_case_information t7100
		 WHERE t7100.c7100_case_info_id = p_caseinfoid
		   AND t7100.c901_case_status = 11091	-- Active
		   AND t7100.c901_type = 1006503 -- CASE
		   AND t7100.c7100_void_fl IS NULL;
		   
		gm_fch_tag_detail(p_tagid,v_currlcndistid,v_currlcn, v_torepid, v_toreptype	, v_tagrepid, v_tagacctid);
		
		--self
		IF	  (p_settype = 11375 AND v_caserepid = v_currlcndistid)
		   OR (p_settype = 11375 AND v_caseacctid = v_currlcndistid)
		   OR (p_settype = 11375 AND v_caserepid = v_tagrepid)
		THEN
			v_caserepid:= NULL;
		ELSE
			v_addressid := NVL (get_address_id (v_caserepid), 0);
			v_toaddressid := NVL (get_address_id (v_torepid), 0);
			gm_pkg_sm_setmgmt_txn.gm_sav_case_shipinfo (p_casesetid
													  , 11385
													  , v_caserepid
													  , 4121
													  , v_torepid
													  , v_toreptype
													  , '30'
													  , 30301
													  , v_addressid
													  , v_shipid
													   );
			gm_pkg_sm_setmgmt_txn.gm_sav_case_shipinfo
										(p_casesetid
									   , 11388
									   , v_torepid
									   , v_toreptype
									   , v_caserepid
									   , 4121
									   , NULL
									   , 30301
									   , v_toaddressid	-- Shipback to owner and select the address from view while displaying it.
									   , v_shipid
										);
		END IF;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			v_caserepid:= NULL;
	END gm_sav_ship_dtl;

/*********************************
   Purpose: This procedure is used to fetch the tag detail.
**********************************/
	PROCEDURE gm_fch_tag_detail(
		p_tagid 	   		IN	t5010_tag.c5010_tag_id%TYPE
	  , p_currlcndistid		OUT t5010_tag.c5010_sub_location_id%TYPE
	  , p_currlcn 			OUT t5010_tag.c901_sub_location_type%TYPE
	  , p_torepid			OUT t5010_tag.c5010_sub_location_id%TYPE
	  , p_toreptype			OUT t5010_tag.c901_sub_location_type%TYPE
	  , p_tagrepid			OUT t5010_tag.c703_sales_rep_id%TYPE
	  , p_tagacctid 		OUT t5010_tag.c704_account_id%TYPE
	)
	AS
		v_sublcnid		 	 t5010_tag.c5010_sub_location_id%TYPE;
	    v_sublcn     		 t5010_tag.c901_sub_location_type%TYPE;
	    v_lcndistid    		 t5010_tag.c5010_location_id%TYPE;
		v_lcn		   		 t5010_tag.c901_location_type%TYPE;
		v_currlcndistid		 t5010_tag.c5010_sub_location_id%TYPE;
	    v_currlcn 			 t5010_tag.c901_sub_location_type%TYPE;
	    v_torepid			 t5010_tag.c5010_sub_location_id%TYPE;
	    v_toreptype			 t5010_tag.c901_sub_location_type%TYPE;
	    v_tagrepid			 t5010_tag.c703_sales_rep_id%TYPE;
	    v_tagreptype		 t5010_tag.c901_sub_location_type%TYPE;
	    v_tagacctid 		 t5010_tag.c704_account_id%TYPE;
	BEGIN
		BEGIN
		SELECT t5010.lcn, t5010.lcndistid, t5010.sublcn, t5010.sublcnid, t5010.currlcndistid
			 , t5010.currlcn, t5010.tagrepid, t5010.tagreptype, t5010.tagacctid
		  INTO v_lcn, v_lcndistid, v_sublcn, v_sublcnid, v_currlcndistid
		     , v_currlcn, v_tagrepid,v_tagreptype, v_tagacctid
		  FROM (SELECT t5010.c5010_sub_location_id currlcndistid
					 , DECODE (t5010.c901_sub_location_type,50222,4121,4120) currlcn
					 , NVL(t5010.c703_sales_rep_id,c5010_location_id) tagrepid,DECODE(t5010.c703_sales_rep_id,NULL,4120,4121) tagreptype
					 , t5010.c704_account_id tagacctid, t5010.c901_sub_location_type sublcn, t5010.c5010_sub_location_id sublcnid
					 , t5010.c901_location_type lcn, t5010.c5010_location_id lcndistid 
					 , v5010a.c5010_decommissioned decommissioned, v5010a.c5010_lockset lockset
				  FROM t5010_tag t5010, v5010a_tag_attribute v5010a
				 WHERE t5010.c5010_tag_id = p_tagid
				   AND t5010.c5010_void_fl IS NULL
				   AND t5010.c901_status = 51012   --active
				   AND t5010.c901_location_type = 4120
				   AND t5010.c5010_tag_id = v5010a.c5010_tag_id(+)) t5010
		 WHERE NVL (t5010.decommissioned, 'N') = 'N' AND NVL (t5010.lockset, 'N') = 'N';
		 
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
			    RETURN;
		END;
		
		IF NVL(v_sublcnid,'0') = '0' OR v_sublcn = 11301 THEN
		 	v_currlcndistid := v_tagrepid;
		 	v_currlcn   := v_tagreptype;
		END IF;
		 
		 p_currlcndistid  := v_currlcndistid;	
		 p_currlcn 	  	  := v_currlcn;		
		 p_torepid		  := v_tagrepid;		
		 p_toreptype	  := v_tagreptype;		
		 p_tagrepid	      := v_tagrepid;		
		 p_tagacctid 	  := v_tagacctid; 
	END gm_fch_tag_detail;
	
/*********************************
   Purpose: This procedure is used to override the tag information.
**********************************/
	PROCEDURE gm_sav_setoverride (
		p_casesetid   IN   t7104_case_set_information.c7104_case_set_id%TYPE
	  , p_newtagid	  IN   t7104_case_set_information.c5010_tag_id%TYPE
	  , p_tagdistid   IN   t7100_case_information.c701_distributor_id%TYPE
	  , p_userid	  IN   t7104_case_set_information.c7104_last_updated_by%TYPE
	)
	AS
		v_cnt		   NUMBER;
		v_setid 	   t7104_case_set_information.c207_set_id%TYPE;
		v_settype	   t7104_case_set_information.c901_set_location_type%TYPE;
		v_lockfromdt   t7104_case_set_information.c7104_tag_lock_from_date%TYPE;
		v_locktodt	   t7104_case_set_information.c7104_tag_lock_to_date%TYPE;
		v_caserepid    t7100_case_information.c703_sales_rep_id%TYPE;
		v_casedistid   t7100_case_information.c701_distributor_id%TYPE;
		v_newtagid	   t7104_case_set_information.c5010_tag_id%TYPE;
		v_caseinfoid   t7104_case_set_information.c7100_case_info_id%TYPE;
		v_prdtlid      t7104_case_set_information.c526_product_request_detail_id%TYPE;
		v_addressid    NUMBER;
		v_shipid	   NUMBER;
	BEGIN
		v_caserepid := get_case_rep_id (NULL, p_casesetid);
		v_casedistid := get_distributor_id (v_caserepid);
		v_settype	:= 11375;
		v_newtagid	:= p_newtagid;
		v_addressid := NVL (get_address_id (v_caserepid), 0);

		BEGIN
			SELECT t7104.c207_set_id setid, t7104.c7104_tag_lock_from_date lockfromdt
				 , t7104.c7104_tag_lock_to_date locktodt, t7104.c7100_case_info_id, t7104.c526_product_request_detail_id 
			  INTO v_setid, v_lockfromdt
				 , v_locktodt, v_caseinfoid, v_prdtlid
			  FROM t7104_case_set_information t7104
			 WHERE t7104.c7104_case_set_id = p_casesetid AND t7104.c7104_void_fl IS NULL
			   AND t7104.c7104_shipped_del_fl IS NULL;			
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				raise_application_error ('-20862', '');
		END;
			 
		
		IF v_prdtlid IS NOT NULL THEN
			--previous loaner request handling and voiding
			gm_cm_sav_prdt_req(v_prdtlid,p_casesetid,v_setid,p_userid); 
		END IF;
		
		IF p_tagdistid != v_casedistid
		THEN   -- AREASET
			v_newtagid	:= p_newtagid;
			--Comment for MNTTASK-7882 Auto Allocation
			--gm_pkg_sm_tag_allocation.gm_chk_avail_tag (v_newtagid, v_lockfromdt, v_locktodt);

			/*
			 * If tag is allocated to someone then it will throw a validation
			 * But if it should still do an override then, need to display that as part of rule
			 * and also here need to write a logic to fetch the caseset id of another case to deallocate from there
			 * and allocate it here. [This will also help in rescheduling a case scenario]
			 */
			IF v_newtagid IS NULL
			THEN
				--App Error from property file - Current tag is already allocated to some other case.
				raise_application_error ('-20519', '');
			END IF;

			v_settype	:= 11378;
		END IF;
		
        --Comment for MNTTASK-7882 Auto Allocation
        --gm_pkg_sm_setmgmt_txn.gm_sav_ship_dtl (v_caseinfoid, p_casesetid, v_newtagid, v_settype, p_userid);        
		
        gm_pkg_sm_setmgmt_txn.gm_sav_sets_tag_info (p_casesetid, v_setid, v_settype, v_newtagid, NULL, NULL, p_userid);
		
	END gm_sav_setoverride;

--
/*********************************
   Purpose: This procedure is used to prepare string for the email notification update.
**********************************/
	PROCEDURE gm_cm_sav_email_log (
		p_str		IN	 VARCHAR2
	  , p_attrval	IN	 t7104a_case_set_info_log.c7104a_value%TYPE
	)
	AS
	BEGIN
		my_context.set_my_inlist_ctx (p_str);

		UPDATE t7104a_case_set_info_log
		   SET c7104a_value = p_attrval
			 , c7104a_last_updated_by = 30301
			 , c7104a_last_updated_date = SYSDATE
		 WHERE c7104a_case_set_info_log_id IN (SELECT token
												 FROM v_in_list);
	END gm_cm_sav_email_log;

/*********************************
   Purpose: This procedure is used to log tag information.
**********************************/
	PROCEDURE gm_cm_sav_tag_log (
		p_loginfoid   IN   t7104a_case_set_info_log.c7104a_case_set_info_log_id%TYPE
	  , p_casesetid   IN   t7104_case_set_information.c7104_case_set_id%TYPE
	  , p_tagid 	  IN   t7104_case_set_information.c5010_tag_id%TYPE
	  , p_reqdtl_id   IN   t7104_case_set_information.c526_product_request_detail_id%TYPE
	  , p_type		  IN   t7104a_case_set_info_log.c901_type%TYPE
	  , p_attrval	  IN   t7104a_case_set_info_log.c7104a_value%TYPE
	  , p_userid	  IN   t7104_case_set_information.c7104_last_updated_by%TYPE
	)
	AS
	BEGIN
		UPDATE t7104a_case_set_info_log
		   SET c7104a_value = p_attrval
			 , c7104a_last_updated_by = p_userid
			 , c7104a_last_updated_date = SYSDATE
		 WHERE c7104a_case_set_info_log_id = p_loginfoid;

		IF (SQL%ROWCOUNT = 0)
		THEN
			INSERT INTO t7104a_case_set_info_log
						(c7104a_case_set_info_log_id, c7104_case_set_id, c5010_tag_id, c526_product_request_detail_id
					   , c901_type, c7104a_value, c7104a_last_updated_by, c7104a_last_updated_date
						)
				 VALUES (s7104a_case_set_info_log_id.NEXTVAL, p_casesetid, p_tagid, p_reqdtl_id
					   , p_type, p_attrval, p_userid, SYSDATE
						);
		END IF;
	END gm_cm_sav_tag_log;

	/*********************************
	   Purpose: This function is used to get next case date of the tag.
	**********************************/
	FUNCTION get_next_case_date (
		p_tagid   t7104_case_set_information.c5010_tag_id%TYPE
	)
		RETURN DATE
	IS
		v_date		   DATE;
	BEGIN
		BEGIN
			SELECT c7100_surgery_date
			  INTO v_date
			  FROM (SELECT	 t7100.c7100_surgery_date, MIN (t7104.c7104_tag_lock_from_date)
						FROM t7100_case_information t7100, t7104_case_set_information t7104
					   WHERE t7104.c7100_case_info_id = t7100.c7100_case_info_id
						 AND t7104.c5010_tag_id = p_tagid
						 AND t7100.c901_case_status NOT IN (11093, 11094)	--Cancelled, Closed
						 AND t7100.c901_type = 1006503 -- CASE
						 AND t7104.c7104_shipped_del_fl is null
						 AND TRUNC(t7100.c7100_surgery_date) >= TRUNC(SYSDATE)
					GROUP BY t7100.c7100_surgery_date
					ORDER BY t7100.c7100_surgery_date ASC)
			 WHERE ROWNUM = 1;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				RETURN NULL;
		END;

		RETURN v_date;
	END get_next_case_date;
--
/*********************************
   Purpose: This procedure is used to save loaner request while setoverride.
**********************************/
	PROCEDURE gm_cm_sav_prdt_req(
		p_reqdtl_id   IN   	t526_product_request_detail.c526_product_request_detail_id%TYPE
	  , p_casesetid   IN	t7104_case_set_information.c7104_case_set_id%TYPE
	  , p_setid 	  IN	t7104_case_set_information.c207_set_id%TYPE
	  , p_userid	  IN   	t526_product_request_detail.c526_last_updated_by%TYPE
	)
	AS
		v_req_status   t526_product_request_detail.c526_status_fl%TYPE;
		v_lnsetstatus  VARCHAR2(10);
	BEGIN
		
		BEGIN	 
			SELECT T526.c526_status_fl, gm_pkg_op_loaner.get_cs_fch_loaner_status (t504a.c504_consignment_id)
		  	  INTO v_req_status, v_lnsetstatus
			  FROM t526_product_request_detail T526, t504a_loaner_transaction T504a
			  WHERE T526.c526_product_request_detail_id = T504a.c526_product_request_detail_id(+)
				AND T526.c526_product_request_detail_id = p_reqdtl_id
				AND t526.c526_void_fl IS NULL
				AND t504a.c504a_void_fl IS NULL;
				
			EXCEPTION
				WHEN NO_DATA_FOUND
				THEN
					v_req_status   := NULL;
					v_lnsetstatus  := NULL;
		END;	
		
		--Loaner set (CN) status is  
		--      space - Loaner status function returns ''(space) for Pending Approval/Open requests,
		--          5 - Allocated,
		--         10 - pending shipping
		IF v_lnsetstatus NOT IN (' ','5','10') THEN 
			raise_application_error ('-20520', '');
		--Request status is Pending Approval(5)/Open(10)/ Allocated(20)		    	
		ELSIF v_req_status IN ('5','10','20') THEN
				--we cannot void loaner request which is created for CSM case. 
				--So,before voiding CSM loaner request, we have to remove request from t7104 table.
			gm_pkg_sm_setmgmt_txn.gm_sav_sets_tag_info (p_casesetid, p_setid, 11381, NULL, NULL, NULL, p_userid);
				--Void Loaner request and shipping details when request status is pending approval/open/allocated.
			gm_pkg_common_cancel.gm_cm_sav_cancelrow (p_reqdtl_id
													, NULL
													, 'VDLON'
													, 'Loaner Request Overridden'
													, p_userid
													);
		END IF;
	END gm_cm_sav_prdt_req;
	

/*********************************
	   Purpose: This function is used to get available tag.
	**********************************/
		FUNCTION get_chk_avail_tag ( 
				p_tagid   t7104_case_set_information.c5010_tag_id%TYPE
      			, p_lockfromdt IN   t7104_case_set_information.c7104_tag_lock_from_date%TYPE
                , p_locktodt   IN   t7104_case_set_information.c7104_tag_lock_to_date%TYPE
                )
                RETURN NUMBER
                IS
                v_count  NUMBER;
                BEGIN
                      BEGIN
                           SELECT COUNT (1)
                           INTO v_count
                           FROM t7100_case_information t7100, t7104_case_set_information t7104
                           WHERE t7100.c901_case_status = 11091               --Active
                           AND t7100.c7100_void_fl IS NULL
                           AND t7100.c901_type = 1006503 -- CASE
                           AND t7100.c7100_case_info_id = t7104.c7100_case_info_id
                           AND t7104.c5010_tag_id = p_tagid
                           AND (   (            TRUNC (t7104.c7104_tag_lock_to_date) >= TRUNC(p_lockfromdt)
                           AND TRUNC (t7104.c7104_tag_lock_to_date) <= TRUNC(p_locktodt) )
                           OR (       TRUNC (t7104.c7104_tag_lock_from_date) >= TRUNC (p_lockfromdt)
                           AND TRUNC (t7104.c7104_tag_lock_from_date) <= TRUNC(p_locktodt) ))
                           AND t7104.c7104_void_fl IS NULL;
                               
                           EXCEPTION
                                  WHEN NO_DATA_FOUND
                           THEN
                                   RETURN 0;
                           END;

                           RETURN v_count;
        END get_chk_avail_tag; 	
/*************************************************************************************************************
	   Purpose: This Procedere is used to retain the new shipin/shipout details after reschdule the Case.
 *************************************************************************************************************/
    PROCEDURE gm_sav_ship_dtl_frm_prvcase(
		p_casesetid   		IN	t7104_case_set_information.c7104_case_set_id%TYPE
	  , p_prvcasesetid 	  	IN	t7104_case_set_information.c7104_case_set_id%TYPE
	  , p_userid	  		IN  t907_shipping_info.c907_created_by%TYPE
	)
	AS    
	v_shipid varchar2(20):= '';
	v_company_id    t907_shipping_info.c1900_company_id%TYPE;
	v_plant_id      t907_shipping_info.c5040_plant_id%TYPE;
	BEGIN
		-- getting the company and plant details
	 	SELECT NVL (GET_COMPID_FRM_CNTX (), GET_RULE_VALUE ('DEFAULT_COMPANY', 'ONEPORTAL')), NVL (GET_PLANTID_FRM_CNTX (),
		    GET_RULE_VALUE ('DEFAULT_PLANT', 'ONEPORTAL'))
		   INTO v_company_id, v_plant_id
		   FROM DUAL;
	    -- 
		FOR c_ship IN (SELECT c907_ship_to_id, c901_ship_to, c907_ship_from_id
						     ,c901_ship_from,c901_source,c907_status_fl
						     ,c106_address_id, c703_sales_rep_id
					   FROM   t907_shipping_info 
                       WHERE  c907_ref_id = TO_CHAR(p_prvcasesetid) 
					   AND    c907_void_fl IS NULL
					   AND    c901_source IN(11385,11388))
		LOOP				
			-- we cannot use existing procedure "gm_sav_case_shipinfo". Because, the ship_to_id may be Account/Dist
			-- In gm_sav_case_shipinfo, the ship_to_id should be sales_rep_id while inserting records.
	  		INSERT INTO t907_shipping_info
					  ( c907_shipping_id, c907_ref_id, c907_ship_to_id, c901_ship_to, c907_ship_from_id,c901_ship_from, 
						c901_source, c907_status_fl, c106_address_id, c907_created_by,c907_created_date, c703_sales_rep_id
						, c1900_company_id, c5040_plant_id)
			   VALUES ( s907_ship_id.NEXTVAL, TO_CHAR(p_casesetid), c_ship.c907_ship_to_id,c_ship.c901_ship_to, 
						c_ship.c907_ship_from_id, c_ship.c901_ship_from, c_ship.c901_source, 
                   		c_ship.c907_status_fl, c_ship.c106_address_id, p_userid, CURRENT_DATE, c_ship.c703_sales_rep_id
                   		, v_company_id, v_plant_id);
		END LOOP;	
    END gm_sav_ship_dtl_frm_prvcase;
---
	
END gm_pkg_sm_setmgmt_txn;
/
