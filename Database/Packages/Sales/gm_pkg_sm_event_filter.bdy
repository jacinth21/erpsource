/* Formatted on 2016/18/04 01:00 (Formatter Plus v4.8.0) */
-- @"C:\Database\Packages\Sales\gm_pkg_sm_event_filter.bdy" ;
CREATE OR REPLACE
PACKAGE BODY gm_pkg_sm_event_filter
IS
/******************************************************************************************
  * Procedure to Filter the Set and Item Based on the Company ID
  * Manikandan Rajasekaran
*******************************************************************************************/
	PROCEDURE gm_filter_data(
    p_case_info_id IN t7104_case_set_information.C7100_CASE_INFO_ID%TYPE ,
    p_user_id      IN t7104_case_set_information.C7104_LAST_UPDATED_BY%TYPE )
	AS
	BEGIN
	
  	gm_filter_sets(p_case_info_id,p_user_id);
  	gm_filter_parts(p_case_info_id,p_user_id);
  
END gm_filter_data;
/******************************************************************************************
* Procedure to Filter the Sets which are Added as a Favourite.
* Manikandan Rajasekaran
*******************************************************************************************/
	PROCEDURE gm_filter_sets(
    p_case_info_id IN t7104_case_set_information.C7100_CASE_INFO_ID%TYPE ,
    p_user_id      IN t7104_case_set_information.C7104_LAST_UPDATED_BY%TYPE )
	AS
  	v_company_id t1900_company.c1900_company_id%TYPE;
	BEGIN
  		SELECT get_compid_frm_cntx() INTO v_company_id FROM dual;
  		UPDATE t7104_case_set_information
  		SET C7104_VOID_FL        ='Y' ,
    		C7104_LAST_UPDATED_BY  =p_user_id,
    		C7104_LAST_UPDATED_DATE=CURRENT_DATE
  		WHERE C7100_Case_Info_Id =p_case_info_id
  		AND C207_SET_ID         IN
    		(SELECT C207_SET_ID
    		 FROM t7104_case_set_information t7104
    		 WHERE C7100_Case_Info_Id    =p_case_info_id
    		 AND T7104.C901_SYSTEM_TYPE IN (19531,19532)
    		 AND C7104_Void_fl          IS NULL
    	MINUS
    	SELECT T7104.C207_SET_ID
    	FROM t7104_case_set_information t7104,
      		 T2080_SET_COMPANY_MAPPING t2080,
      		 T7100_CASE_INFORMATION T7100
    	WHERE T7104.C7100_Case_Info_Id=p_case_info_id
    	AND T7104.C901_SYSTEM_TYPE   IN (19531,19532)
    	AND T7100.C7100_CASE_INFO_ID  = T7104.C7100_CASE_INFO_ID
    	AND T7100.C1900_COMPANY_ID    = T2080.C1900_COMPANY_ID
    	AND T7104.C207_SET_ID         = T2080.C207_SET_ID
    	AND T2080.C1900_COMPANY_ID    =v_company_id
    	);
END gm_filter_sets;
/******************************************************************************************
* Procedure to Filter the Item which are Added as a Favourite.
* Manikandan Rajasekaran
*******************************************************************************************/
	PROCEDURE gm_filter_parts(
    p_case_info_id IN t7104_case_set_information.C7100_CASE_INFO_ID%TYPE ,
    p_user_id      IN t7104_case_set_information.C7104_LAST_UPDATED_BY%TYPE )
	AS
  	v_company_id t1900_company.c1900_company_id%TYPE;	
	BEGIN
  	SELECT get_compid_frm_cntx() INTO v_company_id FROM dual;
  	UPDATE t7104_case_set_information
  	SET C7104_VOID_FL        ='Y' ,
    	C7104_LAST_UPDATED_BY  =p_user_id,
    	C7104_LAST_UPDATED_DATE=CURRENT_DATE
  	WHERE C7100_Case_Info_Id =p_case_info_id
  	AND C207_SET_ID         IN
    	(SELECT C205_PART_NUMBER_ID
    	 FROM t7104_case_set_information t7104
    	 WHERE C7100_Case_Info_Id=p_case_info_id
    	 AND T7104.C207_SET_ID  IS NULL
    	 AND C7104_Void_fl      IS NULL
    MINUS
    SELECT T7104.C207_SET_ID
    FROM t7104_case_set_information t7104,
      T2080_SET_COMPANY_MAPPING t2080,
      T7100_CASE_INFORMATION T7100
    WHERE T7104.C7100_Case_Info_Id=p_case_info_id
    AND T7100.C7100_CASE_INFO_ID  = T7104.C7100_CASE_INFO_ID
    AND T7100.C1900_COMPANY_ID    = T2080.C1900_COMPANY_ID
    AND T7104.C207_SET_ID         = T2080.C207_SET_ID
    AND T2080.C1900_COMPANY_ID    =v_company_id
    );
END gm_filter_parts;
END gm_pkg_sm_event_filter;
/
