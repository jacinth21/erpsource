---@"c:\database\packages\Sales\gm_pkg_sm_eventbook_rpt.pkg";

CREATE OR REPLACE PACKAGE gm_pkg_sm_eventbook_rpt
IS

	/******************************************************************
    * Description	: Procedure to fetch Event Name
    * Author		: Jignesh Shah
    ****************************************************************/
	PROCEDURE gm_fch_event_name_list (
		p_evename	 OUT   TYPES.cursor_type
	);
	
	/********************************************************************
    * Description	: Procedure to fetch Event Name - without reschedule
    * Author		: Jignesh Shah
    *********************************************************************/
	PROCEDURE gm_fch_event_name_all_list (
		p_evename	 OUT   TYPES.cursor_type
	);

	/*********************************************************************
	* Description	: Procedure to fetch Even Report
	* Author		: Jignesh Shah
	**********************************************************************/

	PROCEDURE gm_fch_event_list_rpt (
        p_caseinfo_id  		IN  T7103_Schedule_Info.C7100_CASE_INFO_ID%TYPE,
        p_pur_of_event   	IN  T7103_Schedule_Info.C901_PURPOSE%TYPE,
        p_loan_to 		 	IN  T7103_Schedule_Info.C901_LOAN_TO%TYPE,
        p_loan_to_name		IN  T7103_Schedule_Info.C101_LOAN_TO_ID%TYPE,
        p_edtType			IN	VARCHAR2,
        p_start_date		IN  VARCHAR2,
        p_end_date			IN  VARCHAR2,
        p_out_event_list OUT TYPES.cursor_type
    );
        
	/***************************************************************************************
    * Description	: Procedure to fetch the Status regarding the atleaset one request might
    * 				  approved or not for an Event
    * Author		: HReddi
    ****************************************************************************************/
    PROCEDURE  gm_fch_event_req_approved(
        p_info_id           IN     t7100_case_information.c7100_case_info_id%TYPE ,
        p_out_status        OUT      VARCHAR2
    ); 
      
    /*****************************************************************************
    * Description	: Procedure to fetch the Event Set Details
    * Author		: APrasath
    ******************************************************************************/  
    PROCEDURE gm_fch_event_set_details (
		p_case_info_id		IN  T7104_Case_Set_Information.c7100_case_info_id%TYPE,
        p_out_set_details 	OUT TYPES.cursor_type
	);
	
	/*****************************************************************************
    * Description	: Procedure to fetch the Event Item Details
    * Author		: APrasath
    ******************************************************************************/  
	PROCEDURE gm_fch_event_item_details (
		p_case_info_id		IN  T7104_Case_Set_Information.c7100_case_info_id%TYPE,
        p_out_item_details 	OUT TYPES.cursor_type
	);

    /***************************************************************************
	* Description : Function to return the open txn for the request
	* Author      : Gopinathan
	***************************************************************************/
   FUNCTION get_open_txn (
  	  p_ref_id  IN t412_inhouse_transactions.C412_Ref_Id%TYPE
  	, p_partnum   			IN t413_inhouse_trans_items.c205_part_number_id%TYPE 
   ) 	
   	RETURN VARCHAR2;
	/******************************************************************
    * Description	: Procedure to fetch Booked Item
    * Author		: Gopinathan
    ****************************************************************/	
	PROCEDURE gm_fch_booked_items(
		p_case_info_id		IN  T7104_Case_Set_Information.c7100_case_info_id%TYPE,
        p_out_item_details 	OUT TYPES.cursor_type
	);
		
	/*********************************************************************
	* Description	: Procedure to fetch Event Set Rejected Report
	* Author		: Elango
	**********************************************************************/ 	
	 PROCEDURE  gm_fch_event_set_rejected_dtls(
            p_info_id          IN     VARCHAR2
        ,   p_type             IN     VARCHAR2
        ,   p_cancel_from      IN     VARCHAR2
        ,   p_out_status       OUT    TYPES.cursor_type
      );
      
   
    /***************************************************************************************
    * Description	: Procedure to fetch the Status regarding set is shipped out
    * 				 
    * Author		: Jignesh Shah
    ****************************************************************************************/
     PROCEDURE  gm_fch_event_shipout_info(
      	p_info_id          IN     t7100_case_information.c7100_case_info_id%TYPE
      , p_out_status       OUT    VARCHAR2
      );
     
    /***************************************************************************
	* Description : Function to return the t525_request for type
	* Author    : Elango
	***************************************************************************/
   
   FUNCTION get_request_type (
  	  p_product_request_id  IN   t525_product_request.c525_product_request_id%TYPE
   ) 
   RETURN VARCHAR2;
 /***************************************************************************
	* Description : Function to return the t525_request for type
	* Author    : Elango
	***************************************************************************/
   
   FUNCTION get_case_request_id (
  	  p_case_info_id  IN   t7104_case_set_information.C7100_CASE_INFO_ID%TYPE
   ) 
   RETURN VARCHAR2;
   
   /***************************************************************************
	* Description : Function to return the t525_request id for event
	* Author    : rshah
	***************************************************************************/
   
 FUNCTION get_event_request_id (
  	  p_case_info_id  IN   t7104_case_set_information.C7100_CASE_INFO_ID%TYPE
   ) 
   RETURN VARCHAR2;
   
    /***************************************************************************
	* Description : Function to return the consignment type
	* Author    : Elango
	***************************************************************************/
   
    FUNCTION get_consignment_type (
            p_consignment_id t504_consignment.c504_consignment_id%TYPE
    )
        RETURN VARCHAR2;
        
    /******************************************************************
    * Description	: Procedure to fetch shipping Information
    * Author		: Jignesh Shah
    ****************************************************************/
	PROCEDURE gm_fch_event_shipment_info (
		p_shipinfo	 OUT   TYPES.cursor_type
	);
	
	/******************************************************************
    * Description	: Procedure to fetch shipped sets Information
    * Author		: Jignesh Shah
    ****************************************************************/
	PROCEDURE gm_fch_event_shipped_sets (
		p_info_id          	IN     t7100_case_information.c7100_case_info_id%TYPE,		
		p_flag              IN     VARCHAR2,
		p_shipped_set_info	OUT   TYPES.cursor_type
	);
	
	/******************************************************************
    * Description	: Procedure to fetch shipped items Information
    * Author		: Elango
    ****************************************************************/
	PROCEDURE gm_fch_event_shipped_items (
		p_info_id          	IN     t7100_case_information.c7100_case_info_id%TYPE,
		p_shipped_item_info	OUT   TYPES.cursor_type
	);
	
	/******************************************************************
    * Description	: Procedure to Update shipped sets Information
    * Author		: Jignesh Shah
    ****************************************************************/
	PROCEDURE gm_sav_evnt_shpd_email_sts (
		p_info_id    IN     t7100_case_information.c7100_case_info_id%TYPE
	);
	
	/************************************************************************
    * Description	: Procedure to fetch Event Information based on End Date
    * Author		: Jignesh Shah
    *************************************************************************/
	PROCEDURE gm_fch_event_end_list (
		p_end_eventinfo	 OUT   TYPES.cursor_type
	);
	
	/*******************************************************
	* Description : Procedure to fetch details based of caseinfoid
	* Author	  : Jignesh Shah
	*******************************************************/
	PROCEDURE gm_fch_req_details (
		p_caseinfo_id	IN	 t7100_case_information.c7100_case_info_id%TYPE
	  , p_caseinfo		OUT      TYPES.cursor_type	
	);
	 /***************************************************************************************
    * Description	: Procedure to fetch the edit Status of event
    * 				 
    * Author		: APrasath
    ****************************************************************************************/
	FUNCTION  get_event_edit_status(
      	p_info_id          IN     t7100_case_information.c7100_case_info_id%TYPE    
    )
    RETURN VARCHAR2;
    /***************************************************************************************
    * Description	: FUNCTION to fetch the edit Status of event
    * 				 
    * Author		: APrasath
    ****************************************************************************************/
    FUNCTION  get_event_resource_add_status(
      	p_info_id          IN     t7100_case_information.c7100_case_info_id%TYPE    
    )
    RETURN VARCHAR2;
    /************************************************************************
    * Description	: Procedure to fetch missing part Event Information 
    * Author		: AROCKIA PRASATH
    *************************************************************************/
	PROCEDURE gm_fch_missing_part_event_list (
		p_missing_part_eventinfo	 OUT   TYPES.cursor_type
	);
   /************************************************************************
    * Description	: Procedure to fetch missing part Event Information 
    * Author		: AROCKIA PRASATH
    *************************************************************************/
	PROCEDURE gm_fch_missing_part_event_sets (
		p_req_id          	IN     t525_product_request.c525_product_request_id%TYPE,
		p_miss_part_set_info	OUT   TYPES.cursor_type
	);
	/******************************************************************
    * Description	: Procedure to fetch missing part Event Information 
    * Author		: AROCKIA PRASATH
    ****************************************************************/
	PROCEDURE gm_fch_event_missing_parts (
		p_req_id          	IN     t525_product_request.c525_product_request_id%TYPE,
		p_miss_parts	OUT   TYPES.cursor_type
	);
     /******************************************************************
    * Description	: Procedure to fetch missing part Event Information 
    * Author		: AROCKIA PRASATH
    ****************************************************************/
	PROCEDURE gm_fch_event_unreturn_sets (
		p_req_id          	IN     t525_product_request.c525_product_request_id%TYPE,
		p_miss_part_set_info	OUT   TYPES.cursor_type
	);
	/******************************************************************
    * Description	: Procedure to fetch missing part Event Information 
    * Author		: AROCKIA PRASATH
    ****************************************************************/
	PROCEDURE gm_fch_unreturn_events (
		p_unreturn_events	OUT   TYPES.cursor_type
	);
	/*****************************************************************
    * Description	: Function to fetch shipping id when event edited 
    * Author		: DHANA REDDY
    ****************************************************************/
	FUNCTION get_event_ship_add_id (
        p_info_id IN t7100_case_information.c7100_case_info_id%TYPE)
    RETURN NUMBER;
    /**********************************************************************************
    * Description	: Function to fetch planned shipping date when event is edited 
    * Author		: Elango
 	**********************************************************************************/
    FUNCTION get_event_planned_ship_date (
        p_info_id IN t7100_case_information.c7100_case_info_id%TYPE
    )
    RETURN VARCHAR2;
END gm_pkg_sm_eventbook_rpt;
/