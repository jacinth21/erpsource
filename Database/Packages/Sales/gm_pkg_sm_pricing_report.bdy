/* Formatted on 2010/06/11 16:36 (Formatter Plus v4.8.0) */

--@"C:\Database\Packages\Sales\gm_pkg_sm_pricing_report.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_sm_pricing_report
IS
--

	/******************************************************************
	  Description : This procedure is used to fetch acount group price report
	 ****************************************************************/
/*	PROCEDURE gm_fch_account_price_report (
		p_accountid    IN		t704_account.c704_account_id%TYPE
	  , p_groups	   IN		VARCHAR2
	  , p_pnum		   IN		t4011_group_detail.c205_part_number_id%TYPE
	  , p_setid 	   IN		t207_set_master.c207_set_id%TYPE
	  , p_outdetails   OUT		TYPES.cursor_type
	)
	AS
		v_count 	   NUMBER;
	BEGIN
		my_context.set_my_inlist_ctx (p_groups);

		OPEN p_outdetails
		 FOR
			 SELECT DISTINCT *
						FROM (SELECT t7051.c704_account_id accountid1
								   , get_account_name (t7051.c704_account_id) accountnm, get_set_name (p_setid) setnm
								   , p_setid setid, t4010.c4010_group_nm groupname
								   , TO_CHAR (t7051.c7051_created_date, get_rule_value ('DATEFMT', 'DATEFORMAT')) curr_createddate
								   , t4010.c4010_group_id groupid1
								FROM t7051_account_group_pricing t7051, t4010_group t4010
							   WHERE t7051.c704_account_id =
													 p_accountid   --NVL (t7051.c704_account_id, p_accountid) = p_accountid
								 AND t4010.c4010_group_id = t7051.c7051_ref_id(+)
								 AND t7051.c7051_void_fl IS NULL
								 AND NVL (t7051.c7051_effective_from_date, SYSDATE) <= SYSDATE
								 AND NVL (t7051.c7051_effective_to_date, SYSDATE) >= SYSDATE
								 AND t4010.c4010_group_id IN (SELECT *
																FROM v_in_list
															   WHERE token IS NOT NULL)) a
						   , (SELECT t7051.c704_account_id accountid2
								   , TO_NUMBER (get_account_part_pricing (t7051.c704_account_id
																		, t4011.c205_part_number_id
																		, NULL
																		 )
											   ) current_price
								   , t4011.c205_part_number_id partnum
								   , TO_NUMBER (DECODE (get_group_price ('', t4011.c4010_group_id, 52060)
													  , 0, ''
													  , get_group_price ('', t4011.c4010_group_id, 52060)
													   )
											   ) list_price
								   , TO_CHAR (t4011.c4011_created_date, get_rule_value ('DATEFMT', 'DATEFORMAT')) list_createddate
								   , get_partnum_desc (t4011.c205_part_number_id) pdesc, t4011.c4010_group_id groupid2
								FROM t7051_account_group_pricing t7051, t4011_group_detail t4011
							   WHERE t7051.c704_account_id = p_accountid
								 AND t4011.c4010_group_id = t7051.c7051_ref_id(+)
								 AND (	 regexp_like (t4011.c205_part_number_id
													, NVL (p_pnum, t4011.c205_part_number_id))
									  OR regexp_like (t7051.c7051_ref_id, NVL (p_pnum, t7051.c7051_ref_id))
									 )
								 AND t7051.c7051_void_fl IS NULL
								 AND t4011.c4010_group_id IN (SELECT *
																FROM v_in_list
															   WHERE token IS NOT NULL)) b
					   WHERE NVL (a.accountid1, 1) = NVL (b.accountid2, 1) AND a.groupid1 = b.groupid2
					   
					ORDER BY current_price DESC;
	END gm_fch_account_price_report;*/

	/******************************************************************
	  Description : This procedure is used to fetch request status report
	 ****************************************************************/
	PROCEDURE gm_fch_request_status_rpt (
		p_accountids   IN		VARCHAR2
	  , p_status	   IN		VARCHAR2
	  , p_userid		IN t101_user.c101_user_id%TYPE
	  , p_outdetail    OUT		TYPES.cursor_type
	)
	AS
	BEGIN
		my_context.set_my_inlist_ctx (p_accountids);
		my_context.set_my_double_inlist_ctx (p_status, '');

		OPEN p_outdetail
		 FOR
			 SELECT   c7000_account_price_request_id reqid, c704_account_id accountid
					, get_account_name (c704_account_id) accountnm
					, TO_CHAR (c7000_effective_date, get_rule_value ('DATEFMT', 'DATEFORMAT')) effectivedate
					, get_code_name (c901_request_status) status, get_user_name (c7000_created_by) createdby
					, TO_CHAR (c7000_created_date, get_rule_value ('DATEFMT', 'DATEFORMAT') || ' HH24:MI:SS') createddate , v700.adname adnm
				 FROM t7000_account_price_request, (select distinct REP_ID repid, ac_id accid , ad_name adname
from v700_territory_mapping_detail) v700
				WHERE c704_account_id IN (SELECT *
											FROM v_in_list
										   WHERE token IS NOT NULL)   --DECODE (p_accountid, 0, c704_account_id, p_accountid)
				  AND c901_request_status IN (SELECT token
												FROM v_double_in_list
											   WHERE token IS NOT NULL)   --DECODE (p_status, 0, c901_request_status, p_status)
				  AND c7000_void_fl IS NULL
				  AND c704_account_id = v700.accid
				  AND (decode(c901_request_status,52186,v700.repid,1) = decode(c901_request_status,52186,p_userid,1)
				   OR decode(c901_request_status,52186,c7000_created_by,1) = p_userid)
			 ORDER BY accountnm, c7000_effective_date DESC, c901_request_status;
	END gm_fch_request_status_rpt;

	/******************************************************************
	  Description : This procedure is used to fetch systems that doesnt have price for that account
	 ****************************************************************/
	PROCEDURE gm_fch_pend_sys_account_price (
		p_accountids		  IN	   VARCHAR2
	  , p_status			  IN	   VARCHAR2
	  , p_systemids 		  IN	   VARCHAR2
	  , p_outdetail 		  OUT	   TYPES.cursor_type
	  , p_out_priced_system   OUT	   NUMBER
	)
	AS
	BEGIN
		my_context.set_my_inlist_ctx (p_accountids);
		--gm_sav_temp_pend_grp_system (p_status, 'SYSTEM');
		my_context.set_my_double_inlist_ctx (p_systemids, '');

		-- Below sql to fetch unpriced system 
		OPEN p_outdetail
		 FOR
			SELECT DISTINCT t704.c704_account_id || '^' || t207.c207_set_id accsysid,
					'' reqid, t207.c207_set_id system_id,
					t207.c207_set_nm system_name, t704.c704_account_id ID,
					t704.c704_account_nm nm
				   FROM t704_account t704,
					t207_set_master t207,
					v7001_account_unpriced_system v7001
				  WHERE t704.c704_account_id IN (SELECT *
								   FROM v_in_list
								  WHERE token IS NOT NULL)
				    AND t207.c207_set_id = v7001.c207_set_id
				    AND (t704.c901_ext_country_id IS NULL
				    OR  t704.c901_ext_country_id  in (select country_id from v901_country_codes))
			       ORDER BY nm, UPPER (system_name);
		
		-- Below sql to fetch priced system [sql need to be revisited by richard]
		SELECT COUNT (1)
		  INTO p_out_priced_system		
		  FROM (SELECT DISTINCT c207_set_id
				   FROM v7000_pricing_request v7000
				  WHERE v7000.c704_account_id IN (SELECT *
								    FROM v_in_list
								   WHERE token IS NOT NULL)
			UNION
			SELECT DISTINCT c207_set_id
				   FROM t705_account_pricing_by_group t705
				  WHERE t705.c704_account_id IN (SELECT *
								   FROM v_in_list
								  WHERE token IS NOT NULL))
		 WHERE c207_set_id NOT IN (SELECT DISTINCT v7001.c207_set_id
						      FROM v7001_account_unpriced_system v7001);

	END gm_fch_pend_sys_account_price;

	
	/******************************************************************
	  Description : This procedure is used to fetch GPO systems that doesnt have price for that account
	 ****************************************************************/
	PROCEDURE gm_fch_gpo_pend_sys_account_pr (
		p_gpo				  IN	   t101_party.c101_party_id%TYPE
	  , p_status			  IN	   t7000_account_price_request.c901_request_status%TYPE
	  , p_systemids 		  IN	   VARCHAR2
	  , p_outdetail 		  OUT	   TYPES.cursor_type
	  , p_out_priced_system   OUT	   NUMBER
	)
	AS
	BEGIN
		my_context.set_my_inlist_ctx (p_gpo);
		my_context.set_my_double_inlist_ctx (p_systemids, '');
	
		-- SQL to fetch unpriced Group Account 
		OPEN p_outdetail
		 FOR
			SELECT DISTINCT t101.c101_party_id || '^' || t207.c207_set_id accsysid,
					'' reqid, t207.c207_set_id system_id,
					t207.c207_set_nm system_name, t101.c101_party_id ID,
					t101.c101_party_nm nm
				   FROM t101_party t101,
					t207_set_master t207,
					v7001_group_unpriced_system v7001
				  WHERE t207.c207_set_id = v7001.c207_set_id
				    AND t101.c901_party_type = 7003
				    AND v7001.c101_party_id = t101.c101_party_id
			       ORDER BY nm, system_name;

		-- Below sql to fetch priced system [sql need to be revisited by richard] 
		SELECT COUNT (1)
		  INTO p_out_priced_system		
		  FROM (SELECT DISTINCT c207_set_id
				   FROM v7000_pricing_request v7000
				  WHERE v7000.c4010_group_id IN (SELECT *
								    FROM v_in_list
								   WHERE token IS NOT NULL)
			UNION
			SELECT DISTINCT c207_set_id
				   FROM t705_account_pricing_by_group t705
				  WHERE t705.c101_party_id IN (SELECT *
								   FROM v_in_list
								  WHERE token IS NOT NULL))
		 WHERE c207_set_id NOT IN (SELECT DISTINCT v7001.c207_set_id
						      FROM v7001_group_unpriced_system v7001);
		  
	END gm_fch_gpo_pend_sys_account_pr;
	
	/******************************************************************
	  Description : This procedure is used to fetch Groups that doesnt have price for that account
	 ****************************************************************/
	PROCEDURE gm_fch_pend_grp_account_price (
		p_accountids		  IN	   VARCHAR2
	  , p_status			  IN	   VARCHAR2
	  , p_grpdaterange		  IN	   VARCHAR2
	  , p_outdetail 		  OUT	   TYPES.cursor_type
	  , p_out_priced_system   OUT	   NUMBER
	)
	AS
		v_startdate    DATE;
		v_enddate	   DATE;
	BEGIN
		my_context.set_my_inlist_ctx (p_accountids);
		gm_sav_temp_pend_grp_system (p_status, 'GROUP');

		-- Cursor to fetch all the unprices group by account 	
		OPEN p_outdetail
		 FOR
		SELECT DISTINCT   '^'
				|| t704.c704_account_id
				|| '^'
				|| t4010.c4010_group_id accgrpid,
				'' reqid,
				t4010.c207_set_id system_id, t4010.c4010_group_id GROUP_ID,
				t4010.c4010_group_nm group_name,
				get_set_name (t4010.c207_set_id) system_name,
				t704.c704_account_id ID, t704.c704_account_nm nm
			   FROM t704_account t704,
				t207_set_master t207,
				t4010_group t4010,
				v7000_pricing_request v7000p,
				v7001_account_unpriced_system v7001
			  WHERE t207.c207_set_id = t4010.c207_set_id
			    AND t4010.c4010_group_id = v7001.c4010_group_id
			    AND t704.c704_account_id = v7001.c704_account_id
			    AND t207.c207_set_id = v7001.c207_set_id
			    AND t704.c704_account_id = v7000p.c704_account_id(+)
			    AND (t704.c901_ext_country_id IS NULL
			    OR  t704.c901_ext_country_id  in (select country_id from v901_country_codes))
		       ORDER BY nm, UPPER (system_name), group_name;

		-- Get the count of all the price group [to be revisited by richard]
		SELECT COUNT (1)
		  INTO p_out_priced_system		
		  FROM (SELECT TO_CHAR(c4010_group_id) c4010_group_id
				   FROM v7000_pricing_request v7000
				  WHERE v7000.c704_account_id IN (SELECT *
								    FROM v_in_list
								   WHERE token IS NOT NULL)
			UNION
			SELECT c4010_group_id
				   FROM t705_account_pricing_by_group t705
				  WHERE t705.c704_account_id IN (SELECT *
								   FROM v_in_list
								  WHERE token IS NOT NULL))
		 WHERE c4010_group_id NOT IN (SELECT DISTINCT c4010_group_id
						      FROM v7001_account_unpriced_system v7001);
		
	END gm_fch_pend_grp_account_price;

	/******************************************************************
	  Description : This procedure is used to fetch Groups that doesnt have price for that account
	 ****************************************************************/
	PROCEDURE gm_fch_gpo_pend_grp_acc_pr (
		p_gpo			  IN	   VARCHAR2
	  , p_status			  IN	   VARCHAR2
	  , p_grpdaterange		  IN	   VARCHAR2
	  , p_outdetail 		  OUT	   TYPES.cursor_type
	  , p_out_priced_system   OUT	   NUMBER
	)
	AS
		v_startdate    DATE;
		v_enddate	   DATE;
	BEGIN
		my_context.set_my_inlist_ctx (p_gpo);
		gm_sav_temp_pend_grp_system (p_status, 'GROUP');


		OPEN p_outdetail
		 FOR
		SELECT DISTINCT    '^'
				|| t101.c101_party_id
				|| '^'
				|| t4010.c4010_group_id accgrpid,
				'' reqid,
				t4010.c207_set_id system_id, t4010.c4010_group_id GROUP_ID,
				t4010.c4010_group_nm group_name,
				get_set_name (t4010.c207_set_id) system_name,
				t101.c101_party_id ID, t101.c101_party_nm nm
			   FROM t101_party t101,
				t207_set_master t207,
				t4010_group t4010,
				v7000_pricing_request v7000p,
				v7001_group_unpriced_system v7001
			  WHERE t207.c207_set_id = t4010.c207_set_id
			    AND t101.c901_party_type = 7003
			    AND t4010.c4010_group_id = v7001.c4010_group_id
			    AND t101.c101_party_id = v7001.c101_party_id
			    AND t101.c101_party_id = v7000p.c101_gpo_id(+)
		       ORDER BY nm, system_name, group_name;     

		-- Get the count of all the price group [to be revisited by richard]
		SELECT COUNT (1)
		  INTO p_out_priced_system		
		  FROM (SELECT TO_CHAR(c4010_group_id) c4010_group_id
				   FROM v7000_pricing_request v7000
				  WHERE v7000.c4010_group_id IN (SELECT *
								    FROM v_in_list
								   WHERE token IS NOT NULL)
			UNION
			SELECT c4010_group_id
				   FROM t705_account_pricing_by_group t705
				  WHERE t705.c101_party_id IN (SELECT *
								   FROM v_in_list
								  WHERE token IS NOT NULL))
		 WHERE c4010_group_id NOT IN (SELECT DISTINCT c4010_group_id
						      FROM v7001_group_unpriced_system v7001);


	END gm_fch_gpo_pend_grp_acc_pr;
	
	/******************************************************************
	 Description : This procedure is used to save the set in temp table
	****************************************************************/
	PROCEDURE gm_sav_temp_pend_grp_system (
		p_status	IN	 VARCHAR2
	  , p_ref_val	IN	 VARCHAR2
	)
	AS
	BEGIN
		DELETE FROM my_temp_key_value;

		INSERT INTO my_temp_key_value
			(SELECT DISTINCT t7000.c7000_account_price_request_id, t7000.c704_account_id
						   , DECODE (p_ref_val, 'GROUP', t4010.c4010_group_id, t4010.c207_set_id)
						FROM t4010_group t4010, t7000_account_price_request t7000, t7001_group_part_pricing t7001
					   WHERE t7001.c7000_account_price_request_id = t7000.c7000_account_price_request_id
						 AND t4010.c4010_group_id = t7001.c7001_ref_id
						 AND t7001.c901_ref_type = 52000
						 AND t4010.c4010_void_fl IS NULL
						 AND t4010.c901_type = 40045
						 AND t7000.c704_account_id IN (SELECT *
														 FROM v_in_list
														WHERE token IS NOT NULL));
	END gm_sav_temp_pend_grp_system;

	/******************************************************************
	 Description : This procedure is used to get the Pricing Type, Part Group ID 
	****************************************************************/
	PROCEDURE gm_fch_grp_part_pricing_type (
		p_part_num		IN	T4011_GROUP_DETAIL.C205_PART_NUMBER_ID%TYPE
	  , p_outdetail		OUT	TYPES.cursor_type
	)
	AS
	BEGIN
	OPEN p_outdetail
		 FOR	
	SELECT t4011.c4010_group_id GROUPID,
  		   t4010.C901_PRICED_TYPE PRICEDTYPE 
	FROM t4011_group_detail t4011,
 		 t4010_group t4010
	WHERE   t4011.c205_part_number_id    = p_part_num
		AND t4011.c901_part_pricing_type = '52080' -- Primary Type part
		AND t4011.c4010_group_id         = t4010.c4010_group_id
		AND c901_type                    = '40045' -- Forecast/Demand Group
		AND t4010.c4010_void_fl         IS NULL;
		
	END gm_fch_grp_part_pricing_type;
END gm_pkg_sm_pricing_report;

/
