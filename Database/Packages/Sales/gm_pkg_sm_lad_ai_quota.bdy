/* Formatted on 2009/08/07 10:08 (Formatter Plus v4.8.0) */
-- @"C:\Database\Packages\Sales\gm_pkg_sm_lad_ai_quota.bdy" ;
-- exec gm_pkg_sm_lad_ai_quota.gm_sm_lad_ai_quota_main;

CREATE OR REPLACE PACKAGE BODY gm_pkg_sm_lad_ai_quota
IS
	/*
	* Main entry procedure
	*/
	PROCEDURE gm_sm_lad_ai_quota_main
	AS
	BEGIN
		gm_pkg_sm_lad_ai_quota.gm_sm_lad_budget_quota;
		gm_pkg_sm_lad_ai_quota.gm_sm_lad_budget_reclaim_quota;
		COMMIT;
		gm_pkg_sm_lad_ai_quota.gm_sm_validation_aiquota;
	--gm_pkg_sm_lad_ai_quota.gm_sm_validation_aiquota;
	END gm_sm_lad_ai_quota_main;

	/*
	* Procedure to load the budget quota. Iterates for Every distributor and AI set and calls gm_sm_lad_budget_quota_part
	*/
	PROCEDURE gm_sm_lad_budget_quota
	AS
		--
		v_d_id		   t504_consignment.c701_distributor_id%TYPE;
		v_set_id	   t207_set_master.c207_set_id%TYPE;

		CURSOR distributor_cursor
		IS
			SELECT c701_distributor_id
			  FROM t701_distributor;

		--WHERE c701_distributor_id IN (462);

		-- this will give all AI sets -> 965.902, AI.1234, AA.117
		CURSOR set_cursor
		IS
			SELECT	 v207a.c207_actual_set_id c207_set_id
				FROM v207a_set_consign_link v207a
			   WHERE v207a.c207a_type IN (20003, 20005) AND v207a.c901_custom_hierarchy <> 20701
			--AND v207a.c207_set_id = '300.009'
			ORDER BY v207a.c207_set_id;
--
	BEGIN
		-- Reset the update flag before load
		UPDATE t732_ai_quota t732
		   SET t732.c732_update_fl = NULL;

		--
		FOR distributor_val IN distributor_cursor
		LOOP
			--
			v_d_id		:= distributor_val.c701_distributor_id;

			FOR set_val IN set_cursor
			LOOP
				--
				v_set_id	:= set_val.c207_set_id;
				gm_pkg_sm_lad_ai_quota.gm_sm_lad_budget_quota_part (v_d_id, v_set_id);
			--
			END LOOP;
		--
		END LOOP;
	END gm_sm_lad_budget_quota;

	/*
	* Procedure to load the parts that were moved out of AI back to standard
	*/
	PROCEDURE gm_sm_lad_budget_reclaim_quota
	AS
		cur_quota_id   t732_ai_quota.c732_ai_quota_id%TYPE;

		CURSOR part_cursor
		IS
			SELECT t732.c732_ai_quota_id quotaid
			  FROM t732_ai_quota t732
			 WHERE t732.c732_update_fl IS NULL;
	--AND ROWNUM <=200;
	BEGIN
		FOR part_val IN part_cursor
		LOOP
			cur_quota_id := part_val.quotaid;

			UPDATE t732_ai_quota t732
			   SET t732.c732_qty = t732.c732_qty - t732.c732_qty
				 , t732.c732_update_fl = 'Y'
			 WHERE t732.c732_ai_quota_id = cur_quota_id;

			DELETE FROM t732_ai_quota t732
				  WHERE t732.c732_qty = 0;
		END LOOP;
	END gm_sm_lad_budget_reclaim_quota;

/*
	* Procedure to update / insert the delta qty for a dist and AI set
	*/
	PROCEDURE gm_sm_lad_budget_quota_part (
		p_d_id	   IN	t504_consignment.c701_distributor_id%TYPE
	  , p_set_id   IN	t207_set_master.c207_set_id%TYPE
	)
	AS
		v_partnum	   t731_virtual_consign_item.c205_part_number_id%TYPE;
		v_price 	   t732_ai_quota.c732_price%TYPE;
		v_qty		   t732_ai_quota.c732_qty%TYPE;
		v_cogs		   t732_ai_quota.c732_cogs_cost%TYPE;

		-- instead of joining T730 and T731, we can use a new denormalized table
		CURSOR part_cursor
		IS
			SELECT	 t731.c205_part_number_id pnum, SUM (t731.c731_item_price) / COUNT (t731.c731_item_price) price
				   , SUM (t731.c731_item_qty) qty
				   , SUM (t731.c731_item_cogs_price) / COUNT (t731.c731_item_cogs_price) cogs
				FROM t730_virtual_consignment t730, t731_virtual_consign_item t731
			   WHERE t730.c730_virtual_consignment_id = t731.c730_virtual_consignment_id
				 AND t730.c207_set_id = p_set_id
				 AND t730.c701_distributor_id = p_d_id
			--	 AND c730_lock_fl = 7	-- this will be removed when code goes to production
			GROUP BY t731.c205_part_number_id;
	BEGIN
		FOR part_val IN part_cursor
		LOOP
			v_partnum	:= part_val.pnum;
			v_price 	:= part_val.price;
			v_qty		:= part_val.qty;
			v_cogs		:= part_val.cogs;

			UPDATE t732_ai_quota t732
			   SET t732.c732_qty = v_qty
				 , t732.c732_price = v_price
				 , t732.c732_cogs_cost = v_cogs
				 , t732.c732_update_fl = 'Y'
			 WHERE t732.c205_part_number_id = v_partnum
			   AND t732.c207_set_id = p_set_id
			   AND t732.c701_distributor_id = p_d_id;

			IF (SQL%ROWCOUNT = 0)
			THEN
				gm_pkg_sm_lad_ai_quota.gm_sm_sav_ai_quota (p_d_id, p_set_id, v_partnum, v_qty, v_price, v_cogs);
			END IF;
		--
		END LOOP;
	END gm_sm_lad_budget_quota_part;

	/*
	* Used by Trigger to fetch the list of transactions for a specific part for a distributor
	*/
	PROCEDURE gm_sm_fch_daily_txn (
		p_d_id		IN		 t504_consignment.c701_distributor_id%TYPE
	  , p_partnum	IN		 t205_part_number.c205_part_number_id%TYPE
	  , p_txntype	IN		 t733_ai_quota_log.c901_transaction_type%TYPE
	  , p_out_txn	OUT 	 TYPES.cursor_type
	)
	AS
		--v_load_date	 t733_ai_quota_log.c733_load_date%TYPE := TRUNC (SYSDATE) - 1;	 --SELECT MAX(LOAD_DATE) FROm Master_Load_Table TO_DATE ('06/25/2009', 'MM/DD/YYYY'); --
		v_load_date    t733_ai_quota_log.c733_load_date%TYPE;
		v_txncnt	   NUMBER := 0;
		v_tsfcnt	   NUMBER := 0;
	BEGIN
		SELECT MAX (c9300_next_date) - 1
		  INTO v_load_date
		  FROM t9300_job
		 WHERE c9300_job_nm = 'CONSIGNMENTLOAD';

		-- load date shud be reduced by 1 for csg / ret
		IF (p_txntype = 20212)
		THEN
			SELECT COUNT (1)
			  INTO v_txncnt
			  FROM t506_returns t506, t507_returns_item t507
			 WHERE t506.c506_rma_id = t507.c506_rma_id
			   AND t506.c506_credit_date >= v_load_date
			   AND t507.c205_part_number_id = p_partnum
			   AND t506.c701_distributor_id = p_d_id
			   AND t506.c506_void_fl IS NULL
			   AND t506.c506_status_fl = 2;

			OPEN p_out_txn
			 FOR
				 SELECT   t506.c506_rma_id txnid, t506.c506_credit_date shipdate, t507.c507_item_qty qty, NULL tsfid
					 FROM t506_returns t506, t507_returns_item t507
					WHERE t506.c506_rma_id = t507.c506_rma_id
					  AND t506.c506_credit_date >= v_load_date
					  AND t507.c205_part_number_id = p_partnum
					  AND t506.c701_distributor_id = p_d_id
					  AND t506.c506_void_fl IS NULL
					  AND t506.c506_status_fl = 2
				 -- AND t506.c506_type IN (3301, 3302)	 -- CSG Set or Items return
				 ORDER BY t506.c506_rma_id;
		ELSIF (p_txntype = 20211)	-- CSG
		THEN
			SELECT COUNT (1)
			  INTO v_txncnt
			  FROM t504_consignment t504, t505_item_consignment t505
			 WHERE t504.c504_consignment_id = t505.c504_consignment_id
			   AND t504.c504_ship_date >= v_load_date
			   AND t505.c205_part_number_id = p_partnum
			   AND t504.c701_distributor_id = p_d_id
			   AND t504.c504_status_fl = 4
			   AND t504.c504_type = 4110
			   AND TRIM (t505.c505_control_number) IS NOT NULL
			   AND t504.c504_void_fl IS NULL;

			OPEN p_out_txn
			 FOR
				 SELECT   t504.c504_consignment_id txnid, t504.c504_ship_date shipdate, t505.c505_item_qty qty
						, NULL tsfid
					 FROM t504_consignment t504, t505_item_consignment t505
					WHERE t504.c504_consignment_id = t505.c504_consignment_id
					  AND t504.c504_ship_date >= v_load_date
					  AND t505.c205_part_number_id = p_partnum
					  AND t504.c701_distributor_id = p_d_id
					  AND t504.c504_status_fl = 4
					  AND t504.c504_type = 4110
					  AND TRIM (t505.c505_control_number) IS NOT NULL
					  AND t504.c504_void_fl IS NULL
				 ORDER BY t504.c504_consignment_id;
		END IF;

		IF (v_txncnt = 0)	-- If there are no CSG and no RET
		THEN
			SELECT COUNT (1)   -- check if there are any transfers
			  INTO v_tsfcnt
			  FROM (SELECT tsf.tsfid txnid, tsf.tsfdate shipdate, -999 qty
						 , 'Additional Inventory caused by Transfer transaction ' || t506.c506_rma_id comments
					  FROM t506_returns t506
						 , t507_returns_item t507
						 , (SELECT t921.c921_ref_id refid, t920.c920_transfer_id tsfid, t920.c920_transfer_date tsfdate
							  FROM t920_transfer t920, t921_transfer_detail t921
							 WHERE t920.c920_transfer_id = t921.c920_transfer_id
							   AND (t920.c920_from_ref_id = p_d_id OR t920.c920_to_ref_id = p_d_id)
							   AND t920.c901_transfer_mode = 90350
							   AND t920.c920_transfer_date >= v_load_date) tsf
					 WHERE t506.c506_rma_id = t507.c506_rma_id
					   AND tsf.refid = t506.c506_rma_id
					   AND c205_part_number_id = p_partnum
					UNION
					SELECT tsf.tsfid txnid, tsf.tsfdate shipdate, -999 qty
						 , 'Additional Inventory caused by Transfer transaction ' || t504.c504_consignment_id comments
					  FROM t504_consignment t504
						 , t505_item_consignment t505
						 , (SELECT t921.c921_ref_id refid, t920.c920_transfer_id tsfid, t920.c920_transfer_date tsfdate
							  FROM t920_transfer t920, t921_transfer_detail t921
							 WHERE t920.c920_transfer_id = t921.c920_transfer_id
							   AND (t920.c920_from_ref_id = p_d_id OR t920.c920_to_ref_id = p_d_id)
							   AND t920.c901_transfer_mode = 90350
							   AND t920.c920_transfer_date >= v_load_date) tsf
					 WHERE t504.c504_consignment_id = t505.c504_consignment_id
					   AND tsf.refid = t504.c504_consignment_id
					   AND c205_part_number_id = p_partnum);

			IF (v_tsfcnt = 0)	-- If there are no transfers, check for any Graphic case (critical part) shipment
			THEN
				OPEN p_out_txn
				 FOR
					 SELECT DISTINCT NULL txnid, tmpc.cdate shipdate, -999 qty	 --, tmpc.qty q
								   ,	'Additional Inventory caused by return of critical part '
									 || tmpc.pnum
									 || ' through '
									 || tmpc.csgid comments
								FROM t208_set_details t208p
								   , (SELECT   t504.c504_consignment_id csgid, t504.c504_ship_date cdate
											 , t504.c701_distributor_id distid, t208.c207_set_id setid
											 , t505.c505_item_qty qty, t505.c205_part_number_id pnum
										  FROM t504_consignment t504, t505_item_consignment t505, t208_set_details t208
										 WHERE t504.c504_consignment_id = t505.c504_consignment_id
										   AND t505.c205_part_number_id = t208.c205_part_number_id
										   AND t504.c504_ship_date >= v_load_date
										   AND t208.c208_critical_fl = 'Y'
										   AND t504.c701_distributor_id = p_d_id
										   AND t504.c504_status_fl = 4
										   AND t504.c504_type = 4110
										   AND TRIM (t505.c505_control_number) IS NOT NULL
										   AND t504.c504_void_fl IS NULL
									  ORDER BY qty DESC) tmpc
							   WHERE t208p.c207_set_id = tmpc.setid
								 AND t208p.c205_part_number_id = p_partnum
								 AND ROWNUM = 1
					 UNION ALL
					 SELECT NULL txnid, tmpr.cdate shipdate, -999 qty	--, 1 q
						  ,    'Additional Inventory caused by return of critical part '
							|| tmpr.pnum
							|| 'through '
							|| tmpr.rmaid comments
					   FROM t208_set_details t208p
						  , (SELECT   t208.c207_set_id setid, t506.c701_distributor_id distid, t506.c506_rma_id rmaid
									, t506.c506_created_date cdate, t507.c507_item_qty qty
									, t507.c205_part_number_id pnum
								 FROM t506_returns t506, t507_returns_item t507, t208_set_details t208
								WHERE t506.c701_distributor_id IS NOT NULL
								  AND t507.c205_part_number_id = t208.c205_part_number_id
								  AND t506.c506_rma_id = t507.c506_rma_id
								  AND t506.c506_credit_date >= v_load_date
								  AND t506.c506_status_fl = 2
								  AND t506.c506_rma_id = t507.c506_rma_id
								  AND t507.c507_status_fl IN ('C', 'W')
								  AND t506.c506_void_fl IS NULL
								  AND c701_distributor_id = p_d_id
								  AND t208.c208_critical_fl = 'Y'
							 ORDER BY qty DESC) tmpr
					  WHERE t208p.c207_set_id = tmpr.setid AND t208p.c205_part_number_id = p_partnum AND ROWNUM = 1;
			ELSE
				OPEN p_out_txn
				 FOR
					 SELECT tsf.tsfid txnid, tsf.tsfdate shipdate, -999 qty
						  , 'Additional Inventory caused by Transfer transaction ' || t506.c506_rma_id comments
					   FROM t506_returns t506
						  , t507_returns_item t507
						  , (SELECT t921.c921_ref_id refid, t920.c920_transfer_id tsfid
								  , t920.c920_transfer_date tsfdate
							   FROM t920_transfer t920, t921_transfer_detail t921
							  WHERE t920.c920_transfer_id = t921.c920_transfer_id
								AND (t920.c920_from_ref_id = p_d_id OR t920.c920_to_ref_id = p_d_id)
								AND t920.c901_transfer_mode = 90350
								AND t920.c920_transfer_date >= v_load_date) tsf
					  WHERE t506.c506_rma_id = t507.c506_rma_id
						AND tsf.refid = t506.c506_rma_id
						AND c205_part_number_id = p_partnum
					 UNION
					 SELECT tsf.tsfid txnid, tsf.tsfdate shipdate, -999 qty
						  , 'Additional Inventory caused by Transfer transaction ' || t504.c504_consignment_id comments
					   FROM t504_consignment t504
						  , t505_item_consignment t505
						  , (SELECT t921.c921_ref_id refid, t920.c920_transfer_id tsfid
								  , t920.c920_transfer_date tsfdate
							   FROM t920_transfer t920, t921_transfer_detail t921
							  WHERE t920.c920_transfer_id = t921.c920_transfer_id
								AND (t920.c920_from_ref_id = p_d_id OR t920.c920_to_ref_id = p_d_id)
								AND t920.c901_transfer_mode = 90350
								AND t920.c920_transfer_date >= v_load_date) tsf
					  WHERE t504.c504_consignment_id = t505.c504_consignment_id
						AND tsf.refid = t504.c504_consignment_id
						AND c205_part_number_id = p_partnum;
			END IF;
		END IF;
	END gm_sm_fch_daily_txn;

	/*
	* Used by Trigger to save the list of transactions for a specific part for a distributor
	*/
	PROCEDURE gm_sm_sav_daily_txn (
		p_txn_type	 IN   t733_ai_quota_log.c901_transaction_type%TYPE
	  , p_d_id		 IN   t504_consignment.c701_distributor_id%TYPE
	  , p_set_id	 IN   t732_ai_quota.c207_set_id%TYPE
	  , p_part_num	 IN   t205_part_number.c205_part_number_id%TYPE
	  , p_old_qty	 IN   t733_ai_quota_log.c733_original_qty%TYPE
	  , p_new_qty	 IN   t733_ai_quota_log.c733_new_qty%TYPE
	  , p_price 	 IN   t733_ai_quota_log.c733_price%TYPE
	  , p_cogscost	 IN   t733_ai_quota_log.c733_cogs_cost%TYPE
	)
	AS
		v_change_qty   t732_ai_quota.c732_qty%TYPE;
		v_abs_change_qty t732_ai_quota.c732_qty%TYPE;
		v_txn_tot_qty  t732_ai_quota.c732_qty%TYPE := 0;
		v_txn_qty	   t732_ai_quota.c732_qty%TYPE;
		v_old_qty	   t732_ai_quota.c732_qty%TYPE;
		v_new_qty	   t732_ai_quota.c732_qty%TYPE;
		v_txn_type	   t733_ai_quota_log.c901_transaction_type%TYPE;
		cur_txn_id	   t733_ai_quota_log.c733_transaction_id%TYPE;
		cur_txn_date   t733_ai_quota_log.c733_transaction_date%TYPE;
		cur_txn_qty    t732_ai_quota.c732_qty%TYPE;
		cur_comments   t733_ai_quota_log.c733_comments%TYPE;
		cursor_daily_txn TYPES.cursor_type;
		v_sign		   NUMBER := 1;
		v_comments	   t733_ai_quota_log.c733_comments%TYPE := NULL;
		v_tsfcnt	   NUMBER := 0;
	BEGIN
		v_old_qty	:= p_old_qty;
		v_abs_change_qty := (NVL (p_new_qty, 0) - NVL (p_old_qty, 0));

		-- Few Initialization
		IF (v_abs_change_qty <= 0 AND p_txn_type = 20211)
		THEN
			RETURN;
		ELSIF (v_abs_change_qty > 0 AND p_txn_type = 20211)
		THEN
			RETURN;
		ELSIF (v_abs_change_qty <= 0)
		THEN
			v_sign		:= -1;
		END IF;

		v_change_qty := v_abs_change_qty * v_sign;
		gm_pkg_sm_lad_ai_quota.gm_sm_fch_daily_txn (p_d_id, p_part_num, p_txn_type, cursor_daily_txn);

		SELECT COUNT (*)
		  INTO v_tsfcnt
		  FROM t733_ai_quota_log t733
		 WHERE t733.c205_part_number_id = p_part_num
		   AND t733.c207_set_id = p_set_id
		   AND t733.c701_distributor_id = p_d_id
		   AND t733.c920_transfer_id IS NOT NULL;

		LOOP
			FETCH cursor_daily_txn
			 INTO cur_txn_id, cur_txn_date, cur_txn_qty, cur_comments;

			-- this approach is a special case here and we dont have to use FETCH for getting data from cursor
			-- After the first FETCH, if the result set was empty, %FOUND yields FALSE, %NOTFOUND yields TRUE, and %ROWCOUNT yields 0.
			IF (cursor_daily_txn%NOTFOUND AND v_change_qty <> v_txn_tot_qty)
			THEN
				-- change > 0 then it shud be CSG or change < 0 then it shud be RET. else dont do anything
				IF (   (v_abs_change_qty > 0 AND p_txn_type = 20211 AND v_tsfcnt = 0)
					OR (v_abs_change_qty <= 0 AND p_txn_type = 20212 AND v_tsfcnt = 0)
				   )
				THEN
					v_change_qty := v_change_qty - v_txn_tot_qty;	-- If there are more than 1 data in cursor, we need this
					/*	gm_procedure_log ('p_txn_type '
										,	 p_txn_type
											|| ' - did '
											|| p_d_id
											|| ' - p_set_id '
											|| p_set_id
											|| ' -	pnum '
											|| p_part_num
										 );*/
					gm_pkg_sm_lad_ai_quota.gm_sm_sav_ai_quota_log (p_d_id
																 , p_set_id
																 , p_part_num
																 , v_old_qty
																 , (v_change_qty * v_sign)
																 , p_new_qty
																 , p_price
																 , p_cogscost
																 , p_txn_type
																 , NULL
																 , NULL
																 , NULL
																 , SYSDATE
																 , NULL
																  );
				END IF;
			END IF;

			EXIT WHEN cursor_daily_txn%NOTFOUND;

			IF (v_change_qty > v_txn_tot_qty)
			THEN
				IF (cur_txn_qty = -999
				   )   -- if the txn is from transfer or due to return / csg of graphic case, then transaction qty is the total change qty
				THEN
					v_txn_qty	:= v_change_qty;
					v_comments	:= cur_comments;
				ELSE
					v_txn_qty	:= cur_txn_qty;
				END IF;

				IF ((v_txn_tot_qty + v_txn_qty) > v_change_qty)
				THEN
					v_txn_qty	:= v_change_qty - v_txn_tot_qty;
					v_comments	:= 'A qty of ' || v_txn_qty || ' has been used from this txn ' || cur_txn_id;
				END IF;

				/*	gm_procedure_log ('p_txn_type '
									,	 p_txn_type
									  || ' - did '
									  || p_d_id
									  || ' - p_set_id '
									  || p_set_id
									  || ' -	pnum '
									  || p_part_num
									  || ' - qty '
									  || v_txn_qty
									  || ' - txnid '
									  || cur_txn_id
									  || ' - comments '
									  || cur_comments
									 );*/
				v_txn_tot_qty := v_txn_tot_qty + v_txn_qty;
				v_txn_qty	:= v_txn_qty * v_sign;
				v_new_qty	:= v_old_qty + v_txn_qty;
				gm_pkg_sm_lad_ai_quota.gm_sm_sav_ai_quota_log (p_d_id
															 , p_set_id
															 , p_part_num
															 , v_old_qty
															 , v_txn_qty
															 , v_new_qty
															 , p_price
															 , p_cogscost
															 , p_txn_type
															 , cur_txn_id
															 , cur_txn_date
															 , NULL
															 , SYSDATE
															 , v_comments
															  );
				v_old_qty	:= v_new_qty;
			ELSE
				EXIT;
			END IF;
		END LOOP;

		CLOSE cursor_daily_txn;
	END gm_sm_sav_daily_txn;

/*
	* Insert Procedure for t732_ai_quota
	*/
	PROCEDURE gm_sm_sav_ai_quota (
		p_distid	 IN   t732_ai_quota.c701_distributor_id%TYPE
	  , p_setid 	 IN   t732_ai_quota.c207_set_id%TYPE
	  , p_partnum	 IN   t732_ai_quota.c205_part_number_id%TYPE
	  , p_qty		 IN   t732_ai_quota.c732_qty%TYPE
	  , p_price 	 IN   t732_ai_quota.c732_price%TYPE
	  , p_cogscost	 IN   t732_ai_quota.c732_cogs_cost%TYPE
	)
	AS
	BEGIN
		INSERT INTO t732_ai_quota
					(c732_ai_quota_id, c701_distributor_id, c207_set_id, c205_part_number_id, c732_qty, c732_price
				   , c732_cogs_cost, c732_update_fl
					)
			 VALUES (s732_ai_quota_id.NEXTVAL, p_distid, p_setid, p_partnum, p_qty, p_price
				   , p_cogscost, 'Y'
					);
	END gm_sm_sav_ai_quota;

	/*
		* Insert Procedure for t733_ai_quota_log
		*/
	PROCEDURE gm_sm_sav_ai_quota_log (
		p_distid	  IN   t733_ai_quota_log.c701_distributor_id%TYPE
	  , p_setid 	  IN   t733_ai_quota_log.c207_set_id%TYPE
	  , p_partnum	  IN   t733_ai_quota_log.c205_part_number_id%TYPE
	  , p_org_qty	  IN   t733_ai_quota_log.c733_original_qty%TYPE
	  , p_txn_qty	  IN   t733_ai_quota_log.c733_transaction_qty%TYPE
	  , p_new_qty	  IN   t733_ai_quota_log.c733_new_qty%TYPE
	  , p_price 	  IN   t733_ai_quota_log.c733_price%TYPE
	  , p_cogscost	  IN   t733_ai_quota_log.c733_cogs_cost%TYPE
	  , p_txn_type	  IN   t733_ai_quota_log.c901_transaction_type%TYPE
	  , p_txn_id	  IN   t733_ai_quota_log.c733_transaction_id%TYPE
	  , p_txn_date	  IN   t733_ai_quota_log.c733_transaction_date%TYPE
	  , p_tsf_id	  IN   t733_ai_quota_log.c920_transfer_id%TYPE
	  , p_load_date   IN   t733_ai_quota_log.c733_load_date%TYPE
	  , p_comments	  IN   t733_ai_quota_log.c733_comments%TYPE
	)
	AS
	BEGIN
		INSERT INTO t733_ai_quota_log
					(c733_ai_quota_log_id, c701_distributor_id, c207_set_id, c205_part_number_id, c733_original_qty
				   , c733_transaction_qty, c733_new_qty, c733_price, c733_cogs_cost, c901_transaction_type
				   , c733_transaction_id, c733_transaction_date, c920_transfer_id, c733_load_date, c733_comments
					)
			 VALUES (s733_ai_quota_log_id.NEXTVAL, p_distid, p_setid, p_partnum, p_org_qty
				   , p_txn_qty, p_new_qty, p_price, p_cogscost, p_txn_type
				   , p_txn_id, p_txn_date, p_tsf_id, p_load_date, p_comments
					);
	END gm_sm_sav_ai_quota_log;

			 /*
	* Procedure for validations. Add any validation that will come up and email all txn with NULL txn id
	*/
	PROCEDURE gm_sm_validation_aiquota
	AS
	BEGIN
		gm_pkg_sm_lad_ai_quota.gm_sm_mail_aiquota;
	END gm_sm_validation_aiquota;

	/*
		* Procedure to email all validations like NULL in txn field etc.
		*/
	-- issue is that if we have more data (> size limit of varchar2), gm_com_send_email_prc might get screwed
	PROCEDURE gm_sm_mail_aiquota
	AS
		v_cnt		   NUMBER;
		subject 	   VARCHAR2 (100);
		to_mail 	   VARCHAR2 (200);
		mail_body	   VARCHAR2 (4000);
	BEGIN
		SELECT COUNT (*) cnt
		  INTO v_cnt
		  FROM t733_ai_quota_log t733
		 WHERE t733.c733_transaction_id IS NULL AND t733.c733_comments IS NULL;

		-- Email Area
		to_mail 	:= get_rule_value ('AIQUOTANULLTXN', 'EMAIL');
		subject 	:= ' AI Quota Null transaction ';
		mail_body	:= ' There are	' || v_cnt || ' transactions with NO Txn ID and COmments ';
		gm_com_send_email_prc (to_mail, subject, mail_body);
	END gm_sm_mail_aiquota;
END gm_pkg_sm_lad_ai_quota;
/
