--@"C:\PMT\db\Packages\Sales\pricing\gm_pkg_sm_rebate_report.pkg";
CREATE OR REPLACE PACKAGE gm_pkg_sm_rebate_report
IS
    /*********************************************************
    * Description : This procedure is used to fetch the rebate setup details. Based on Rebate Id or Party ID.
    *  		We are using the Auto complete changes - so, in setup screen we know only Party ID.
    * 
    *    	1. Based on Party id to get the Rebate id (primary key) and fetch the rebate details.
    *      	2. If No party id then, based on Rebate id (parameter values) to getch the rebate details.
    * 
    * Author: mmuthusamy
    *********************************************************/
    PROCEDURE gm_fch_rebate_management_dtls (
            p_rebate_id IN t7200_rebate_master.c7200_rebate_id%TYPE,
            p_party_id  IN t7200_rebate_master.c7200_ref_id%TYPE,
            p_out_rebate_dtls OUT TYPES.cursor_type) ;
    /*********************************************************
    * Description : This procedure is used to fetch Rebate part mapping details.
    *    Based on Rebate Id - to fetch all the parts number mapping details from T7201 table.
    * 
    * Author : mmuthusamy
    *********************************************************/
    PROCEDURE gm_fch_rebate_parts_mapping_dtls (
            p_rebate_id IN t7200_rebate_master.c7200_rebate_id%TYPE,
            p_out_rebate_part_dtls OUT TYPES.cursor_type);        


  /*******************************************************
	* Description : Procedure used to fetch the rebate audit trail details
	* Author   : mmuthusamy
	*******************************************************/
	--
	PROCEDURE gm_fch_rebate_log_dtls (
	        p_rebate_id IN t7202_rebate_log.c7200_rebate_id%TYPE,
	        p_type      IN t7202_rebate_log.c901_type%TYPE,
	        p_date_fmt  IN VARCHAR2,
	        p_out_log_cur OUT TYPES.cursor_type) ;       
            
	/*******************************************************
	* Description : Procedure used to fetch the rebate management report details 
	* Author   : mmuthusamy
	*******************************************************/
	--
    PROCEDURE gm_fch_rebate_management_report(
        p_rebate_acc_type     IN t7200_rebate_master.c901_grp_type%TYPE,
        p_party_id            IN t7200_rebate_master.c7200_ref_id%TYPE,
        p_rebate_type         IN t7200_rebate_master.c901_rebate_type%TYPE,
        p_rebate_category     IN t7200_rebate_master.c901_rebate_category%TYPE,
        p_rebate_from_rate    IN t7200_rebate_master.c7200_rebate_rate%TYPE,
        p_rebate_to_rate      IN t7200_rebate_master.c7200_rebate_rate%TYPE,
        p_rebate_part_type    IN t7200_rebate_master.c901_rebate_part_type%TYPE,
        p_date_Type         IN  VARCHAR2,
        p_rebate_frm_date     IN VARCHAR2,
        p_rebate_to_date      IN VARCHAR2,
        p_company_id         IN t7200_rebate_master.c1900_company_id%TYPE,
        p_out_rebate_report   OUT CLOB
   ) ;
   	/*******************************************************
	* Description : Procedure used to fetch the rebate Part List report details 
	* Author   : mmuthusamy
	*******************************************************/
	--
 PROCEDURE gm_fch_rebate_part_reports_dtls(
            p_rebate_acc_type     IN  t7200_rebate_master.c901_grp_type%TYPE,
            p_party_id            IN  t7200_rebate_master.c7200_ref_id%TYPE,
            p_date_Type           IN  VARCHAR2,
            p_rebate_frm_date     IN  VARCHAR2,
            p_rebate_to_date      IN  VARCHAR2,
            p_part_number         IN  T7201_REBATE_PARTS.c205_part_number_id%TYPE,
            p_company_id         IN t7200_rebate_master.c1900_company_id%TYPE,
            p_out_part_details    OUT CLOB
  ) ; 
  
    /*******************************************************
	* Description : Procedure used to fetch the rebate expiry details 
	* Author   : T.S Ramachandiran
	*******************************************************/
  
  PROCEDURE gm_fch_rebate_expiry_details(
		p_range                        IN  VARCHAR2,
		p_company_id                   IN  t7200_rebate_master.c1900_company_id%TYPE,
		p_out_rebate_expiry_details    OUT TYPES.cursor_type
);  
		
		  
END gm_pkg_sm_rebate_report;
/