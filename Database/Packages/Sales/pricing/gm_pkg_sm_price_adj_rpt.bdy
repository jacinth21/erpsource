/* Formatted on 2011/10/28 18:05 (Formatter Plus v4.8.0) */
-- @"C:\Database\Packages\Sales\pricing\gm_pkg_sm_price_adj_rpt.bdy" ;

CREATE OR REPLACE PACKAGE BODY gm_pkg_sm_price_adj_rpt
IS
	

	/**********************************************************************************************
	* Description : Procedure to fetch the header details to Part Price Adjustment Initiate screen
	* author : arajan
	*************************************************************************************************/
	PROCEDURE gm_fch_part_price_adj_header (
		p_acc_id		IN		 	t704_account.c704_account_id%TYPE
	  , p_type			IN			t901_code_lookup.c901_code_id%TYPE
	  , p_txn			IN 			t7020_part_price_adj_request.c7020_part_price_adj_req_id%TYPE
	  , p_dtls			OUT 	 	TYPES.cursor_type
	)
	AS
		v_party_id 	t7020_part_price_adj_request.c101_party_id%TYPE := p_acc_id;
		v_date_fmt	t906_rules.c906_rule_value%TYPE;
	BEGIN 
		
		IF p_type = 903110 -- Account
		THEN
			v_party_id	:= gm_pkg_sm_price_adj_rpt.get_party_id(p_acc_id);			
		END IF;
		
		OPEN p_dtls
		 FOR
		 	SELECT c7020_part_price_adj_req_id txnid,c101_party_id partyid, get_code_name(c901_request_status_cd) status, c901_request_status_cd statusid
		 		, get_user_name(c7020_initiated_by) initiatedby, c7020_initiated_date initdate
			  	, get_user_name(c7020_approved_by) implementedby, c7020_approved_date  impdate
			FROM t7020_part_price_adj_request
			WHERE c101_party_id = v_party_id
			AND c7020_part_price_adj_req_id = p_txn
			AND c7020_void_fl IS NULL;
			  
	END gm_fch_part_price_adj_header;
	
	/*******************************************************
	* Description : Function to get the party id
	* author : arajan
	*******************************************************/
	FUNCTION get_party_id (
		p_account_id   IN		t704_account.c704_account_id%TYPE
	)
	RETURN t704_account.c101_party_id%TYPE
	IS
	v_party_id t704_account.c101_party_id%TYPE;
	BEGIN
		BEGIN
			SELECT c101_party_id INTO v_party_id
			FROM t704_account
			WHERE c704_account_id = p_account_id
			AND c704_void_fl IS NULL;
		EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN NULL;
		END;
		
		RETURN v_party_id;
	END get_party_id;
	
	/*************************************************************************************
	* Description : Proceudre to fetch the Part Price Adjustment transaction details
	* author : arajan
	*************************************************************************************/
	PROCEDURE gm_fch_part_price_adj_dtls (
	  p_txn			IN 			t7020_part_price_adj_request.c7020_part_price_adj_req_id%TYPE
	  , p_dtls			OUT 	 	TYPES.cursor_type
	  , p_err_dtls		OUT 	 	TYPES.cursor_type
	)
	AS
	BEGIN
		
		OPEN p_dtls
		 FOR
		 	SELECT C7020_PART_PRICE_ADJ_REQ_ID TXNID, C7021_PART_PRICE_ADJ_DETAIL_ID DETAILID, C205_PART_NUMBER_ID PARTNUM
		 		, GET_PARTNUM_DESC(C205_PART_NUMBER_ID) PARTDESC,C901_ADJ_CODE ADJCODE
				, C7021_ADJ_VALUE ADJVALUE, C7021_UNIT_PRICE UNITPRICE, C705_NET_CAL_UNIT_PRICE NETPRICE, 0 ERRFL
			FROM T7021_PART_PRICE_ADJ_DETAILS
			WHERE C7020_PART_PRICE_ADJ_REQ_ID = p_txn
			AND C7021_VOID_FL IS NULL
			order by C7021_PART_PRICE_ADJ_DETAIL_ID,C205_PART_NUMBER_ID ;
			
		OPEN p_err_dtls
		 FOR
		 	SELECT C7020_PART_PRICE_ADJ_REQ_ID TXNID, C7021_PART_PRICE_ADJ_DETAIL_ID DETAILID, C205_PART_NUMBER_ID PARTNUM
		 		, GET_PARTNUM_DESC(C205_PART_NUMBER_ID) PARTDESC,C901_ADJ_CODE ADJCODE
				, C7021_ADJ_VALUE ADJVALUE, C7021_UNIT_PRICE UNITPRICE, C705_NET_CAL_UNIT_PRICE NETPRICE, C7021_ERROR_FL ERRFL
			FROM T7021_PART_PRICE_ADJ_DTLS_TEMP
			WHERE C7020_PART_PRICE_ADJ_REQ_ID = p_txn
			AND C7021_VOID_FL IS NULL
			order by C7021_PART_PRICE_ADJ_DETAIL_ID,C205_PART_NUMBER_ID;
			  
	END gm_fch_part_price_adj_dtls;
	
	/*******************************************************
	* Description : Function to get the party type
	* author : arajan
	*******************************************************/
	FUNCTION get_party_type (
		p_party_id   IN		T704_ACCOUNT.C101_PARTY_ID%TYPE
	)
	RETURN NUMBER
	IS
	v_count NUMBER;
	v_type  T901_CODE_LOOKUP.C901_CODE_ID%TYPE;
	BEGIN
		BEGIN
			SELECT count(1) INTO v_count
			FROM T704_ACCOUNT 
			WHERE C101_PARTY_ID = p_party_id
			and C704_VOID_FL is null;
		EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			v_type := 903111; --Group Account
		END;
		
		IF v_count > 0 THEN
			v_type := 903110; --Account
		ELSE
			v_type := 903111; --Group Account
		END IF;
		
		RETURN v_type;
	END get_party_type;

	/*******************************************************
	* Description : Function to get the account id
	* author : arajan
	*******************************************************/
	FUNCTION get_account_id (
		p_party_id   IN		T704_ACCOUNT.C101_PARTY_ID%TYPE
	)
	RETURN T704_ACCOUNT.c704_account_id%TYPE
	IS
	v_account_id  T704_ACCOUNT.c704_account_id%TYPE;
	BEGIN
		BEGIN
			SELECT c704_account_id INTO v_account_id
			FROM T704_ACCOUNT 
			WHERE C101_PARTY_ID = p_party_id
			AND ROWNUM = 1
			AND C704_VOID_FL is null;
		EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			v_account_id := p_party_id; --Group Account
		END;
		
		RETURN v_account_id;
	END get_account_id;
	
	/**********************************************************************************************
	* Description : Procedure to fetch the Histroy of Part Price Adjustment Report
	* author : arajan
	*************************************************************************************************/
	PROCEDURE gm_fch_part_price_adj_history (
		p_party_id			IN		 	t704_account.c101_party_id%TYPE
	  , p_part_num			IN		 	t7021_part_price_adj_details.c205_part_number_id%TYPE
	  , p_out_history		OUT 	 	TYPES.cursor_type
	)AS
	BEGIN
		
		OPEN p_out_history FOR 
		
		   SELECT t7021.c7020_part_price_adj_req_id transid
        		, t7021.c901_adj_code ADJCODEID 
        		, get_code_name(t7021.c901_adj_code) ADJCODENAME
        		, t7021.c7021_adj_value ADJVALUE        
        		, t7020.c7020_initiated_date initdate
        		, get_user_name(t7020.c7020_initiated_by) initBy
        		, t7020.c7020_approved_date  implemdate
        		, get_user_name(t7020.c7020_approved_by) implemby
        		, t7020.c101_party_id partyID
        		, t7021.c205_part_number_id PARTNUM 
                , t205.c205_part_num_desc PDESC
                , t7021.c7021_unit_price unitprice
                , t7021.c705_net_cal_unit_price netunitprice
   			 FROM t7020_part_price_adj_request t7020, t7021_part_price_adj_details t7021 , t205_part_number t205
   			WHERE t7020.c7020_part_price_adj_req_id = t7021.c7020_part_price_adj_req_id
   			  AND t7021.c205_part_number_id = t205.c205_part_number_id
   			  AND t7020.c7020_void_fl is null
   			  AND t7021.c7021_void_fl is null
   			  AND t7020.c901_request_status_cd = '105287'  -- Implemented
   			  AND t7020.c101_party_id = p_party_id
   			  AND t7021.c205_part_number_id = p_part_num;
   			  
		END gm_fch_part_price_adj_history;		
		
	/**************************************************************************************************
    * Description : Procedure to fetch the list users who have access to initiate the PADJ transaction
    * author : arajan
    ***************************************************************************************************/
   PROCEDURE gm_fch_initiated_users (
      p_outcursor   	OUT      TYPES.cursor_type
   )
   AS
   BEGIN
      OPEN p_outcursor
       FOR
          SELECT DISTINCT GET_USER_NAME(C101_MAPPED_PARTY_ID) NAME, C101_MAPPED_PARTY_ID ID
			FROM T1530_ACCESS T1530, T1500_GROUP T1500, T1501_GROUP_MAPPING T1501
			WHERE T1530.C1500_GROUP_ID = T1500.C1500_GROUP_ID(+)
				AND T1500.C1500_GROUP_ID = T1501.C1500_GROUP_ID(+)
				AND T1530.C1520_FUNCTION_ID LIKE 'PARTLVLADJCODEINIT' -- Funciton id of the left link Part Price Adjustment Initiate screen
				AND C1501_VOID_FL IS NULL
				AND T1500.C1500_VOID_FL IS NULL
				AND C101_MAPPED_PARTY_ID IS NOT NULL
			ORDER BY UPPER (NAME);
   END gm_fch_initiated_users;
   
  
	/***********************************************************************************************************
	* Description : Procedure to fetch the Detials of Part Price Adjustment Report based on the selected filters
	* Author: HReddi
	************************************************************************************************************/
	PROCEDURE gm_fch_part_price_adj_details (
		p_query_type    IN 			VARCHAR2
	 ,	p_query_ids		IN 			CLOB
	 ,	p_type			IN			VARCHAR2
	 ,	p_acc_party_id	IN			VARCHAR2
	 ,  p_part_number	IN			VARCHAR2
	 ,  p_out_details	OUT 	 	TYPES.cursor_type
	)
	AS
		v_query_string CLOB;
	BEGIN
		
		IF p_part_number IS NOT NULL THEN
			gm_temp_table_for_part(p_part_number);
		ELSIF p_query_ids IS NOT NULL THEN
			gm_temp_table_for_system(p_query_type,p_query_ids);
		END IF;
		
		IF p_type = 903110 THEN -- Account
    		gm_fch_adj_for_acct(p_acc_party_id,p_part_number,p_query_ids,p_out_details);
		ELSIF p_type = 903111 THEN-- Group
    		gm_fch_adj_for_grp(p_acc_party_id,p_part_number,p_query_ids,p_out_details);
		END IF;
		
	END gm_fch_part_price_adj_details;
	
	/******************************************************************************
     * Description : Procedure for saving the input part numbers in the Temp table.
     * Author: HReddi
    *******************************************************************************/
   PROCEDURE gm_temp_table_for_part (
     p_part_number IN	t205_part_number.c205_part_number_id%TYPE   
   )
   AS
   BEGIN
	    my_context.set_my_inlist_ctx(p_part_number);
	    DELETE FROM my_temp_part_list;
	    
	    INSERT INTO my_temp_part_list (
             SELECT c205_part_number_id , NULL
               FROM t205_part_number , v_in_list
              WHERE c205_part_number_id like v_in_list.token
		);	   
   END gm_temp_table_for_part;
   
   
 /*****************************************************************************************************
    * Description : Procedure to fetch the part details for the SYSTEM/PROJECT and Saving in TEMP table.
    * Author: HReddi
    *****************************************************************************************************/
   PROCEDURE gm_temp_table_for_system (
     	p_query_type    IN 			VARCHAR2
	  ,	p_query_ids		IN 			CLOB
   )
   AS   
   BEGIN	  
	   my_context.set_my_inlist_ctx(p_query_ids);
       DELETE FROM my_temp_part_list;
      
       IF p_query_type = 120001 THEN -- SYSTEM 
		      	INSERT INTO my_temp_part_list (
		      		 SELECT t208.c205_part_number_id , null
                       FROM t207_set_master t207
                          , t208_set_details t208
                          , v_in_list
                      WHERE t207.c901_set_grp_type = 1600
                        AND t207.c207_set_id = t208.c207_set_id
                        AND t207.c207_set_id   IN (v_in_list.token) 
                        AND t207.c207_void_fl is null
                        AND t208.c208_void_fl is null
		         	);		    
       ELSIF p_query_type = 120002 THEN -- PROJECT
	      		INSERT INTO my_temp_part_list(
	  				SELECT DISTINCT t205.c205_part_number_id, NULL
	   				  FROM t4011_group_detail t4011 , t205_part_number t205 , v_in_list
	  				 WHERE t4011.c205_part_number_id = t205.c205_part_number_id
	    			   AND t205.c202_project_id IN (v_in_list.token)
	    			   AND t205.c205_active_fl = 'Y'	    			                     
				);			    		
       	END IF;     	
   END gm_temp_table_for_system;
   
   /*****************************************************************************************************
    * Description : Procedure to Part Price Adjustment Details for the Selected Account Name.
    * Author: HReddi
    *****************************************************************************************************/
PROCEDURE gm_fch_adj_for_acct (
     	p_acc_party_id     	IN 			VARCHAR2
	 ,  p_part_number		IN 			CLOB
	 ,  p_query_ids			IN 			CLOB
	 ,  p_out_details		OUT 	 	TYPES.cursor_type
   )
   AS   
   		v_party_id   	t704_account.c101_party_id%TYPE;
   		v_gpo_id		t704_account.c101_party_id%TYPE;
   BEGIN
	   BEGIN	
		    SELECT t704.c101_party_id
       			 , t740.c101_party_id
       		 INTO v_party_id, v_gpo_id  	
	  		 FROM t704_account t704 
                , t740_gpo_account_mapping t740         
            WHERE t704.c704_account_id = p_acc_party_id        
              AND t704.c704_account_id = t740.c704_account_id (+)
	          AND t704.c704_void_fl IS NULL;
	   EXCEPTION WHEN NO_DATA_FOUND THEN
			v_party_id := null; --Group Account
	   END; 	   
	    
	    IF p_query_ids IS NOT NULL OR p_part_number IS NOT NULL THEN
	    
	    	OPEN p_out_details FOR 
	    			SELECT t705.c207_set_nm PJNAME
                         , t705.c205_part_number_id  PNUM
                         , t705.c205_part_num_desc PDESC
                         , '$' CURRENCY
                         , t705.c205_list_price PLPRICE
                         , t705.c705_unit_price PUPRICE
                         , GET_CODE_NAME(t7052.c901_discount_type) PADJCODENM
                         , t7052.c7052_discount_offered PADJVAL
                         , DECODE(t7052.c901_discount_type,'105283',(t705.c705_unit_price-t7052.c7052_discount_offered),'105284',(t705.c705_unit_price - (t705.c705_unit_price * (t7052.c7052_discount_offered / 100))),'0') PADJNETPRICE
                         , get_user_name(t705.c705_last_updated_by) IMPLBY
                         , t705.c705_last_updated_date IMPLDT 
                         , t7052.c7052_history_fl HISTORY
                         , t7052.c101_party_id PARTYID
                 		 , t705.c202_project_id PROJECTID
                		 , t705.c202_project_nm PROJECTNAME       
                	  FROM (SELECT v_party_id c101_party_id
                                   , t705.c205_part_number_id 
                                   , t205.c205_part_num_desc
                                   , t705.c202_project_id 
                                   , t202.c202_project_nm     
                                   , syst.c207_set_nm
                                   , t205.c205_list_price
                                   , gm_pkg_sm_adj_rpt.get_account_part_unitprice(t705.c704_account_id, t705.c205_part_number_id, v_gpo_id) c705_unit_price
                                   , NVL(t705.c705_last_updated_date,t705.c705_created_date ) c705_last_updated_date
                                   , NVL(t705.c705_last_updated_by,t705.c705_created_by) c705_last_updated_by
                                FROM t705_account_pricing t705
                                   , t202_project t202
                                   , t205_part_number t205
                                   , my_temp_part_list temp
                                   , (SELECT t208.c205_part_number_id, t207.c207_set_nm
                                                  FROM t207_set_master t207
                                                        , t208_set_details t208
                                                      -- , my_temp_part_list temp
                                                 WHERE t207.c901_set_grp_type = 1600
                                                   AND t207.c207_set_id = t208.c207_set_id
                                                   AND t207.c207_void_fl IS NULL
                                                   AND t208.c208_void_fl IS NULL
                                               --  AND temp.c205_part_number_id = t208.c205_part_number_id
                                     ) syst
                               WHERE t705.c704_account_id = p_acc_party_id
                                 AND t202.c202_project_id = t705.c202_project_id 
                                 AND t705.c205_part_number_id = temp.c205_part_number_id
                                 AND t205.c205_part_number_id = t705.c205_part_number_id
                                 AND t705.c205_part_number_id = syst.c205_part_number_id(+)
                                 AND t705.c705_void_fl IS NULL
                                 AND t202.c202_void_fl IS NULL
                              ) t705
                              , t7052_account_pricing_discount t7052
                          WHERE t705.c101_party_id = t7052.c101_party_id
                            AND t705.c205_part_number_id = t7052.c205_part_number_id (+)
                            AND t7052.c7052_void_fl IS NULL;
	    
	    ELSE
	    	OPEN p_out_details FOR 
	    		SELECT t705.c207_set_nm PJNAME
                     , t705.c205_part_number_id  PNUM
                     , t705.c205_part_num_desc PDESC
                     , '$' CURRENCY
                     , t705.c205_list_price PLPRICE
                     , t705.c705_unit_price PUPRICE
                     , GET_CODE_NAME(t7052.c901_discount_type) PADJCODENM
                     , t7052.c7052_discount_offered PADJVAL
                     , DECODE(t7052.c901_discount_type,'105283',(t705.c705_unit_price-t7052.c7052_discount_offered),'105284',(t705.c705_unit_price - (t705.c705_unit_price * (t7052.c7052_discount_offered / 100))),'0') PADJNETPRICE
                     , get_user_name(t705.c705_last_updated_by) IMPLBY
                     , t705.c705_last_updated_date IMPLDT 
                     , t7052.c7052_history_fl HISTORY
                     , t7052.c101_party_id PARTYID
                     , t705.c202_project_id PROJECTID
                     , t705.c202_project_nm PROJECTNAME       
                 FROM (SELECT v_party_id c101_party_id
                                   , t705.c205_part_number_id 
                                   , t205.c205_part_num_desc
                                   , t705.c202_project_id 
                                   , t202.c202_project_nm     
                                   , syst.c207_set_nm
                                   , t205.c205_list_price
                                   , gm_pkg_sm_adj_rpt.get_account_part_unitprice(t705.c704_account_id, t705.c205_part_number_id, v_gpo_id) c705_unit_price
                                   , NVL(t705.c705_last_updated_date,t705.c705_created_date ) c705_last_updated_date
                                   , NVL(t705.c705_last_updated_by,t705.c705_created_by) c705_last_updated_by
                                FROM t705_account_pricing t705
                                   , t202_project t202
                                   , t205_part_number t205
                                   , (SELECT t208.c205_part_number_id, t207.c207_set_nm
                                                  FROM t207_set_master t207
                                                        , t208_set_details t208
                                                      -- , my_temp_part_list temp
                                                 WHERE t207.c901_set_grp_type = 1600
                                                   AND t207.c207_set_id = t208.c207_set_id
                                                   AND t207.c207_void_fl IS NULL
                                                   AND t208.c208_void_fl IS NULL
                                               --  AND temp.c205_part_number_id = t208.c205_part_number_id
                                     ) syst
                               WHERE t705.c704_account_id = p_acc_party_id
                                 AND t202.c202_project_id = t705.c202_project_id                                      
                                 AND t205.c205_part_number_id = t705.c205_part_number_id
                                 AND t705.c205_part_number_id = syst.c205_part_number_id(+)
                                 AND t705.c705_void_fl IS NULL
                                 AND t202.c202_void_fl IS NULL
                          ) t705
                          , t7052_account_pricing_discount t7052
                      WHERE t705.c101_party_id = t7052.c101_party_id
                        AND t705.c205_part_number_id = t7052.c205_part_number_id (+)
                        AND t7052.c7052_void_fl IS NULL;	    
	    END IF;    
   END gm_fch_adj_for_acct;
   
    /*****************************************************************************************************
    * Description : Procedure to Part Price Adjustment Details for the Selected Group Price Book Name.
    * Author: HReddi
    *****************************************************************************************************/
   PROCEDURE gm_fch_adj_for_grp (
     	p_acc_party_id     	IN 			VARCHAR2
	 ,  p_part_number		IN 			CLOB
	 ,  p_query_ids			IN 			CLOB
	 ,  p_out_details		OUT 	 	TYPES.cursor_type
   )
   AS   
   		v_party_id   t704_account.c101_party_id%TYPE;
   BEGIN
	  IF p_query_ids IS NOT NULL OR p_part_number IS NOT NULL THEN
	    
	    	OPEN p_out_details FOR 
	    			SELECT t705.c207_set_nm PJNAME
                         , t705.c205_part_number_id  PNUM
                         , t705.c205_part_num_desc PDESC
                         , '$' CURRENCY
                         , t705.c205_list_price PLPRICE
                         , t705.c705_unit_price PUPRICE
                         , GET_CODE_NAME(t7052.c901_discount_type) PADJCODENM
                         , t7052.c7052_discount_offered PADJVAL
                         , DECODE(t7052.c901_discount_type,'105283',(t705.c705_unit_price-t7052.c7052_discount_offered),'105284',(t705.c705_unit_price - (t705.c705_unit_price * (t7052.c7052_discount_offered / 100))),'0') PADJNETPRICE
                         , get_user_name(t705.c705_last_updated_by) IMPLBY
                         , t705.c705_last_updated_date IMPLDT 
                         , t7052.c7052_history_fl HISTORY
                         , t7052.c101_party_id PARTYID
                 		 , t705.c202_project_id PROJECTID
                		 , t705.c202_project_nm PROJECTNAME       
                	  FROM (SELECT p_acc_party_id c101_party_id
                                   , t705.c205_part_number_id 
                                   , t205.c205_part_num_desc
                                   , t705.c202_project_id 
                                   , t202.c202_project_nm     
                                   , syst.c207_set_nm
                                   , t205.c205_list_price
                                   , gm_pkg_sm_adj_rpt.get_account_part_unitprice(NULL, t705.c205_part_number_id, p_acc_party_id) c705_unit_price
                                   , NVL(t705.c705_last_updated_date,t705.c705_created_date ) c705_last_updated_date
                                   , NVL(t705.c705_last_updated_by,t705.c705_created_by) c705_last_updated_by
                                FROM t705_account_pricing t705
                                   , t202_project t202
                                   , t205_part_number t205
                                   , my_temp_part_list temp
                                   , (SELECT t208.c205_part_number_id, t207.c207_set_nm
                                                  FROM t207_set_master t207
                                                        , t208_set_details t208
                                                      -- , my_temp_part_list temp
                                                 WHERE t207.c901_set_grp_type = 1600
                                                   AND t207.c207_set_id = t208.c207_set_id
                                                   AND t207.c207_void_fl IS NULL
                                                   AND t208.c208_void_fl IS NULL
                                               --  AND temp.c205_part_number_id = t208.c205_part_number_id
                                     ) syst
                               WHERE t705.c101_party_id = p_acc_party_id
                                 AND t202.c202_project_id = t705.c202_project_id 
                                 AND t705.c205_part_number_id = temp.c205_part_number_id
                                 AND t205.c205_part_number_id = t705.c205_part_number_id
                                 AND t705.c205_part_number_id = syst.c205_part_number_id(+)
                                 AND t705.c705_void_fl IS NULL
                                 AND t202.c202_void_fl IS NULL
                              ) t705
                              , t7052_account_pricing_discount t7052
                          WHERE t705.c101_party_id = t7052.c101_party_id
                            AND t705.c205_part_number_id = to_char(t7052.c205_part_number_id (+))
                            AND t7052.c7052_void_fl IS NULL;
	    
	    ELSE
	    	OPEN p_out_details FOR 
	    		SELECT t705.c207_set_nm PJNAME
                     , t705.c205_part_number_id  PNUM
                     , t705.c205_part_num_desc PDESC
                     , '$' CURRENCY
                     , t705.c205_list_price PLPRICE
                     , t705.c705_unit_price PUPRICE
                     , GET_CODE_NAME(t7052.c901_discount_type) PADJCODENM
                     , t7052.c7052_discount_offered PADJVAL
                     , DECODE(t7052.c901_discount_type,'105283',(t705.c705_unit_price-t7052.c7052_discount_offered),'105284',(t705.c705_unit_price - (t705.c705_unit_price * (t7052.c7052_discount_offered / 100))),'0') PADJNETPRICE
                     , get_user_name(t705.c705_last_updated_by) IMPLBY
                     , t705.c705_last_updated_date IMPLDT 
                     , t7052.c7052_history_fl HISTORY
                     , t7052.c101_party_id PARTYID
                     , t705.c202_project_id PROJECTID
                     , t705.c202_project_nm PROJECTNAME       
                 FROM (SELECT p_acc_party_id c101_party_id
                                   , t705.c205_part_number_id 
                                   , t205.c205_part_num_desc
                                   , t705.c202_project_id 
                                   , t202.c202_project_nm     
                                   , syst.c207_set_nm
                                   , t205.c205_list_price
                                   , gm_pkg_sm_adj_rpt.get_account_part_unitprice(NULL, t705.c205_part_number_id, p_acc_party_id) c705_unit_price
                                   , NVL(t705.c705_last_updated_date,t705.c705_created_date ) c705_last_updated_date
                                   , NVL(t705.c705_last_updated_by,t705.c705_created_by) c705_last_updated_by
                                FROM t705_account_pricing t705
                                   , t202_project t202
                                   , t205_part_number t205
                                   , (SELECT t208.c205_part_number_id, t207.c207_set_nm
                                                  FROM t207_set_master t207
                                                        , t208_set_details t208
                                                      -- , my_temp_part_list temp
                                                 WHERE t207.c901_set_grp_type = 1600
                                                   AND t207.c207_set_id = t208.c207_set_id
                                                   AND t207.c207_void_fl IS NULL
                                                   AND t208.c208_void_fl IS NULL
                                               --  AND temp.c205_part_number_id = t208.c205_part_number_id
                                     ) syst
                               WHERE t705.c101_party_id = p_acc_party_id
                                 AND t202.c202_project_id = t705.c202_project_id                                      
                                 AND t205.c205_part_number_id = t705.c205_part_number_id
                                 AND t705.c205_part_number_id = syst.c205_part_number_id(+)
                                 AND t705.c705_void_fl IS NULL
                                 AND t202.c202_void_fl IS NULL
                          ) t705
                          , t7052_account_pricing_discount t7052
                      WHERE t705.c101_party_id = t7052.c101_party_id
                        AND t705.c205_part_number_id = to_char(t7052.c205_part_number_id (+))
                        AND t7052.c7052_void_fl IS NULL;	    
	    END IF;    
   END gm_fch_adj_for_grp;
   
   /**********************************************************************************************************************
	* Description : Procedure to fetch the part price for the given part and account
	* author : VPrasath
	**********************************************************************************************************************/
	PROCEDURE gm_fch_acc_part_price (
	  	 p_account_id		IN			T704_ACCOUNT.c704_account_id%TYPE
	   , p_part_num			IN			T7052_ACCOUNT_PRICING_DISCOUNT.C205_PART_NUMBER_ID%TYPE
	   , p_out_details		OUT 	 	TYPES.cursor_type
	)
	 AS
	 v_party_id 	T704_ACCOUNT.c101_party_id%TYPE;
	 v_rec_count	NUMBER;
   	BEGIN
	   	BEGIN
			 SELECT c101_party_id
			  INTO v_party_id	
			  FROM t704_account
			 WHERE c704_account_id = p_account_id;
		  EXCEPTION  WHEN NO_DATA_FOUND
		THEN
			RETURN;
		END;	   
		
			INSERT INTO my_temp_acc_part_list 
			     VALUES (p_account_id, p_part_num);
		    
		    -- Get the count of record from the view to check whether record is available or not
			SELECT count(1) INTO v_rec_count
                    FROM v705_part_price_in_acc
                   WHERE c101_party_id = v_party_id
                     AND c205_part_number_id = p_part_num;     
			/*
			 * If there is record in v705_part_price_in_acc, get te price and adjustment details from the view
			 * Otherwise get the details from the T1706_PARTY_PRICE_ADJ table
			 * 107970: Waste; 107971: Revision
			*/
			IF v_rec_count > 0
			THEN
				OPEN p_out_details 
				FOR   
			   	SELECT c705_unit_price unitprice 
	                       , c7052_disc_offered_dollars unitadj   
	                       , c705_calculated_unit_price price                           
	                       , c1706_waste waste
	                       , c1706_revision revision
	                    FROM v705_part_price_in_acc
	                   WHERE c101_party_id = v_party_id
	                     AND c205_part_number_id = p_part_num;
	         ELSE            
	           	OPEN p_out_details 
				FOR 
				SELECT NULL unitprice 
	                       , NULL unitadj   
	                       , NULL price
						   , MAX(DECODE( c901_adj_type,107970,c1706_adj_value,NULL)) WASTE
						   , MAX(DECODE( c901_adj_type,107971,c1706_adj_value,NULL)) REVISION 
						   , C101_PARTY_ID partyid
						FROM T1706_PARTY_PRICE_ADJ
						WHERE C101_PARTY_ID = v_party_id
						GROUP BY c101_party_id;
	   		END IF;
	END gm_fch_acc_part_price;
	
   /**********************************************************************************************************************
       * Description : Procedure to fetch the list price for the given part and account
       * author : VPrasath
    **********************************************************************************************************************/
	PROCEDURE gm_fch_acc_part_list_price (
	      p_account_id        IN       T704_ACCOUNT.c704_account_id%TYPE
	    , p_part_num          IN       T7052_ACCOUNT_PRICING_DISCOUNT.C205_PART_NUMBER_ID%TYPE
	    , p_adj_type		  IN	   T1706_PARTY_PRICE_ADJ.C901_ADJ_TYPE%TYPE
	    , p_out_details       OUT      TYPES.cursor_type
	)
	AS
	v_party_id T704_ACCOUNT.c101_party_id%TYPE;
	v_company_id	T1900_COMPANY.C1900_COMPANY_ID%TYPE;
	v_rec_count	NUMBER;
	BEGIN         
	      
		SELECT get_compid_frm_cntx()
	   		INTO v_company_id 
		FROM DUAL;
       
		BEGIN
	        SELECT c101_party_id
	         INTO v_party_id    
	            FROM t704_account
	         WHERE c704_account_id = p_account_id;
             EXCEPTION  WHEN NO_DATA_FOUND
             THEN
                 RETURN;
		END;      
		INSERT INTO my_temp_acc_part_list 
           VALUES (p_account_id, p_part_num);
           
          -- Get the count of record from the view to check whether record is available or not
		SELECT count(1) INTO v_rec_count
                FROM v705_part_price_in_acc
               WHERE c101_party_id = v_party_id
                 AND c205_part_number_id = p_part_num;   
        /*
         * If there is no account price, the record from v705_part_price_in_acc will be null
         * In such case we need to get the list price if we are using the adjustment to the list price
         * For that, need to get the adjustment details from table t1706_party_price_adj and combine with t2052_part_price_mapping to get the list price
         *107970: Wasted; 107971: Revision; 105241: List price 
         * */      
		IF v_rec_count > 0
		THEN
	        OPEN p_out_details 
	        FOR   
			    SELECT (CASE 
			    		 WHEN ((p_adj_type = 107970 AND v705.c1706_waste_price_list = 105240) OR 
			    		      (p_adj_type = 107971 AND v705.c1706_revision_price_list = 105240)) 
			    		 THEN 
			    				c705_unit_price 
			            WHEN ((p_adj_type = 107970 AND v705.c1706_waste_price_list = 105241) OR 
					    	 (p_adj_type = 107971 AND v705.c1706_revision_price_list = 105241)) 
			            THEN 
			    				c205_list_price 
			    		 ELSE 
			    				c705_unit_price 
			    		 END) unitprice                    
			    FROM v705_part_price_in_acc v705
			    WHERE v705.c101_party_id = v_party_id
			    AND c205_part_number_id = p_part_num;
		ELSE
			OPEN p_out_details 
	        FOR
				SELECT (CASE 
			    		 
			            WHEN ((p_adj_type = 107970 AND t1706.c901_price_type = 105241) OR 
					    	  (p_adj_type = 107971 AND t1706.c901_price_type = 105241)) 
			            THEN 
			    				c2052_list_price 
			    		 END) unitprice                    
			    FROM t2052_part_price_mapping t2052, t1706_party_price_adj t1706
			    WHERE t1706.c101_party_id = v_party_id
			    AND t1706.c901_adj_type = p_adj_type
			    AND t2052.c1900_company_id = v_company_id
			    AND t1706.c1706_void_fl IS NULL
			    AND T2052.c205_part_number_id = p_part_num;
		END IF;
                
   END gm_fch_acc_part_list_price;
   
END gm_pkg_sm_price_adj_rpt;
/
