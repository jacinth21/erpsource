/** 
FileName : gm_pkg_sm_price_approval_txn.pkg
Description : 
Author : treddy 
Date : 
Copyright : Globus Medical Inc 
*/
CREATE OR REPLACE PACKAGE gm_pkg_sm_price_approval_txn
IS

/***********************************************************************
  * Author : treddy
  * Description : This method is used to update the price request status
 ************************************************************************/
        PROCEDURE gm_upd_price_req_approval(
	    p_acc_pri_req_id IN T7501_ACCOUNT_PRICE_REQ_DTL.C7500_ACCOUNT_PRICE_REQUEST_ID%TYPE,
	    p_user_id IN T101_USER.C101_USER_ID%TYPE
	);

/***********************************************************************
  * Author : treddy
  * Description : This method is used to get the party category type
 ************************************************************************/
  PROCEDURE gm_get_pary_category(
       p_account_id IN t7500_account_price_request.c704_account_id%TYPE,
       p_party_category OUT VARCHAR2
  );		


/***********************************************************************
  * Author : treddy
  * Description : This method is used to get the pricing request initiated 
                  by sales team or pricing team
 ************************************************************************/
        PROCEDURE gm_get_initiated_team(
	     p_user_id IN T101_USER.C101_USER_ID%TYPE,
	     p_company_id IN  t1501_group_mapping.C1900_COMPANY_ID%TYPE,
	     p_initiated_team OUT VARCHAR2
	);

/***********************************************************************
  * Author : treddy
  * Description : This method is used to update the price request status
 ************************************************************************/
        PROCEDURE gm_upd_approval_state(
	    p_acc_pri_req_id IN T7501_ACCOUNT_PRICE_REQ_DTL.C7500_ACCOUNT_PRICE_REQUEST_ID%TYPE,
	    p_party_category IN VARCHAR2,
	    p_initiated_team IN VARCHAR2,
	    p_user_id IN T101_USER.C101_USER_ID%TYPE
	);

/*******************************************************
   * Description : Procedure to fetch access permission
   * Author    : Tejeswara Reddy
   *******************************************************/
   FUNCTION gm_fch_user_grp_chk (
      p_partyid   IN   t1530_access.c101_party_id%TYPE,
      p_grp_id    IN   t1501_group_mapping.c1500_group_id%TYPE,
      p_company_id IN  t1501_group_mapping.C1900_COMPANY_ID%TYPE
   ) 
     RETURN NUMBER;
	
END gm_pkg_sm_price_approval_txn;

/