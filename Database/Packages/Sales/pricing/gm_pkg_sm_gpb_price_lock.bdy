/* Formatted on 2017/09/23 13:12 */
--@"C:\pmt\db\Packages\Sales\pricing\gm_pkg_sm_gpb_price_lock.bdy";
CREATE OR REPLACE PACKAGE BODY gm_pkg_sm_gpb_price_lock
IS
	/**********************************************************************
   * Description : Procedure to lock the GPB current price for 
   * GPB Price comparison.
   * Author 	 : Aprasath
  **********************************************************************/
	PROCEDURE gm_sav_initiate_main(	   
	    p_user_id 			IN T101_USER.C101_USER_ID%TYPE
	)
	AS
---
		v_gpb_id	   t7580_price_gpb_mapping.c101_gpb_id%TYPE;
		v_gpo_reference_id	   t7550_gpo_reference.c7550_gpo_reference_id%TYPE;
		v_gpo_reference_detail_id t7551_gpo_reference_detail.c7551_gpo_reference_detail_id%TYPE;
		v_company_id   t1900_company.c1900_company_id%TYPE;		
		v_user_id      NUMBER := 30301;
---

--Cursor to fetch the GPB ids to lock the current price
		CURSOR c_gpb_detail
		IS
			SELECT c101_gpb_id FROM t7580_price_gpb_mapping WHERE c7580_void_fl IS NULL;
    
	BEGIN
		
		SELECT  get_compid_frm_cntx()
			  INTO  v_company_id FROM DUAL;
			  
			  v_company_id := NVL(v_company_id,1000);
			  
			  v_gpo_reference_id := s7550_gpo_reference_id.nextval;
		
			 -- create the job id
			 INSERT INTO  t7550_gpo_reference(c7550_gpo_reference_id ,c7500_generated_date ,c1900_company_id ,c7550_created_by ,c7550_created_date) 
			     VALUES (v_gpo_reference_id,sysdate,v_company_id,NVL(p_user_id,v_user_id),sysdate);
		
				FOR c_dtl IN c_gpb_detail
				LOOP
				
				v_gpb_id := c_dtl.c101_gpb_id;
				
				v_gpo_reference_detail_id := s7551_gpo_reference_detail_id.nextval;
				
				--inserting reference details. This id will be used in pricing request master				
				 INSERT INTO  t7551_gpo_reference_detail(c7551_gpo_reference_detail_id,c7550_gpo_reference_id,c101_gpb_id) 
				     VALUES (v_gpo_reference_detail_id,v_gpo_reference_id,v_gpb_id);
				
				 --inserting current price for the GPB into reference master
				 INSERT INTO t7552_gpo_reference_price(
                              c7552_gpo_reference_price_id ,c7551_gpo_reference_detail_id ,c4010_group_id ,c901_price_type ,c205_part_number_id ,c7552_unit_price)
                              (
                              SELECT s7552_gpo_reference_price_id.nextval,
                                  v_gpo_reference_detail_id,c4010_group_id,c901_priced_type,c205_part_number_id,c7540_unit_price 
                                     FROM t7540_account_group_pricing 
                                      WHERE c101_party_id = v_gpb_id AND c7540_void_fl IS NULL AND c1900_company_id=v_company_id);					
					
				 END LOOP;
		
					-- LOG THE STATUS
					gm_pkg_cm_job.gm_cm_notify_load_info (CURRENT_DATE
													, CURRENT_DATE
													, 'S'
													, 'SUCCESS'
													, 'gm_pkg_sm_gpb_price_lock - Locked data for job id '||v_gpo_reference_id
													 );
					COMMIT;
				
				EXCEPTION
				WHEN OTHERS
				THEN
					ROLLBACK;
					--
					gm_pkg_cm_job.gm_cm_notify_load_info (CURRENT_DATE
														, CURRENT_DATE
														, 'E'
														, SQLERRM
														, 'gm_pkg_sm_gpb_price_lock - Locked data Error in '||v_gpo_reference_id
														 );
					COMMIT;			
	END gm_sav_initiate_main;
	--
	-- 
END gm_pkg_sm_gpb_price_lock;
/
