/** 
FileName : gm_pkg_sm_price_approval_txn.bdy
Description : 
Author : treddy 
Date : 
Copyright : Globus Medical Inc 
Confluence Link : http://confluence.spineit.net/display/PRJ/PRT_R401-Approve+a+price+request
*/

CREATE OR REPLACE PACKAGE BODY gm_pkg_sm_price_approval_txn
IS

/***********************************************************************
  * Author : treddy
  * Description : This method is used to update the price request
  *
  *  Party Catagiry Details
  *    GPB -- 0
  *    Non Contarct Account with GPB  -- 1
  *    Non Contarct Account --2
  *    Contarct Account National -- 3
  *    Contarct Account Local -- 4
  *
  *  Initiated Team Details
  *     Sales Team -- 5
  *     Pricing Team -- 6
  *
 ************************************************************************/
	PROCEDURE gm_upd_price_req_approval(
	    p_acc_pri_req_id 	IN T7501_ACCOUNT_PRICE_REQ_DTL.C7500_ACCOUNT_PRICE_REQUEST_ID%TYPE,
	    p_user_id 			IN T101_USER.C101_USER_ID%TYPE
	)
	AS
	   	v_party_category 	VARCHAR2(20);
      	v_initiated_team 	VARCHAR2(20);
	   	v_account_id 		t7500_account_price_request.c704_account_id%TYPE;
	   	v_company_id  		T7500_ACCOUNT_PRICE_REQUEST.C1900_COMPANY_ID%TYPE;
	BEGIN	
	     SELECT c704_account_id, c1900_company_id
	       INTO v_account_id, v_company_id	           
	       FROM t7500_account_price_request
	      WHERE c7500_account_price_request_id = p_acc_pri_req_id;
			
	      -- Procedure for getting the Party Category type
          gm_pkg_sm_price_approval_txn.gm_get_pary_category(v_account_id,v_party_category);
          
	      IF (v_party_category IS NOT NULL AND v_party_category != 'GACC') THEN
	      	 -- Procedure for getting the who initiated the Price Request
             gm_pkg_sm_price_approval_txn.gm_get_initiated_team(p_user_id,v_company_id,v_initiated_team);
             -- Procedure for Updating the Status on the T7501 table based on the above information
	         gm_pkg_sm_price_approval_txn.gm_upd_approval_state(p_acc_pri_req_id,v_party_category,v_initiated_team,p_user_id);
	         
	      END IF;
	END gm_upd_price_req_approval;	

/***********************************************************************
  * Author : treddy
  * Description : This method is used to get the party category type
 ************************************************************************/
	PROCEDURE gm_get_pary_category(
		p_account_id 		IN  t7500_account_price_request.c704_account_id%TYPE,
		p_party_category 	OUT VARCHAR2
    )
	AS	   
	    v_count 		NUMBER := 0;
	    v_contract_flg 	NUMBER := 0;
	    v_gpo_id 		NUMBER := 0;
	    v_party_id      t704_account.c101_party_id%TYPE;
	    v_contract_type t7041_party_price_contract.c901_contract_type%TYPE;
	    
	BEGIN						
		BEGIN
        	SELECT count(1)	INTO v_count
              FROM T704_ACCOUNT 
             WHERE C704_ACCOUNT_ID = p_account_id 
               AND c704_void_fl IS NULL;
         EXCEPTION
              WHEN NO_DATA_FOUND THEN
              	v_count := 0;
         END; 
		
		IF v_count = 0 THEN
			p_party_category := 'GACC' ;  -- Group Account
		ELSE
		  BEGIN
		      SELECT c901_contract INTO v_contract_flg  -- 903103 [Contract]
                FROM t704d_account_affln
		       WHERE c704_account_id = p_account_id
		         AND c704d_void_fl IS NULL; 
		  EXCEPTION
               WHEN NO_DATA_FOUND THEN
       			v_contract_flg := 0; 
          END; 
		   IF v_contract_flg = 0 OR v_contract_flg = 903101 THEN  -- No [903101]
		   		v_gpo_id := get_account_gpo_id(p_account_id);
		      	IF v_gpo_id > 0 THEN
		         	p_party_category := 'NCGAC';  -- Non Contract with GPB Account
		      	ELSE
		         	p_party_category := 'NCAC';  -- Non Contract Account
		      	END IF;
		   ELSE
		   	   SELECT c101_party_id	INTO v_party_id
	              FROM t704_account 
	             WHERE c704_account_id = p_account_id 
	               AND c704_void_fl IS NULL;
	               
	           BEGIN    
	            SELECT c901_contract_type	INTO v_contract_type
	              FROM t7041_party_price_contract 
	             WHERE c101_party_id = v_party_id 
	               AND c7041_void_fl IS NULL;
	           EXCEPTION
               WHEN NO_DATA_FOUND THEN
       			p_party_category := 'CACN';
          	   END; 
	               
		   		IF v_contract_type = '106020' THEN
		         	p_party_category := 'CACL';  --  Contarct account local(4)
		      	ELSE
		         	p_party_category := 'CACN';  -- Contarct account national(3)
		      	END IF;		       		
		   END IF;
		END IF;

	END gm_get_pary_category;

/********************************************************************************************************
  * Author : treddy
  * Description : This method is used to get the pricing request initiated by sales team or pricing team
 ********************************************************************************************************/
	PROCEDURE gm_get_initiated_team(
		p_user_id 			IN  T101_USER.C101_USER_ID%TYPE,
	    p_company_id 		IN  t1501_group_mapping.C1900_COMPANY_ID%TYPE,
	    p_initiated_team 	OUT VARCHAR2             
	)
	AS	   
	   	v_party_id 			t101_user.c101_party_id%type;
	   	v_pricing_grp_count number;
	BEGIN
        SELECT  C101_PARTY_ID INTO v_party_id
	      FROM T101_USER 
	     WHERE c101_user_id = p_user_id; 

        v_pricing_grp_count := gm_pkg_sm_price_approval_txn.gm_fch_user_grp_chk(v_party_id,'100005',p_company_id);  -- 100005[Per Contract]
	     
	    IF v_pricing_grp_count IS NOT NULL AND v_pricing_grp_count > 0 THEN
	       p_initiated_team := 'PRTM'; -- Pricing Team
	    ELSE
	       p_initiated_team := 'SATM'; -- Sales Team
	    END IF;
  			
	END gm_get_initiated_team;		

/***********************************************************************
  * Author : treddy
  * Description : This method is used to update the price request status
 ************************************************************************/
	PROCEDURE gm_upd_approval_state(
		p_acc_pri_req_id 	IN T7501_ACCOUNT_PRICE_REQ_DTL.C7500_ACCOUNT_PRICE_REQUEST_ID%TYPE,
	    p_party_category 	IN VARCHAR2,
	    p_initiated_team 	IN VARCHAR2,
	    p_user_id 			IN T101_USER.C101_USER_ID%TYPE
	)
	AS
		v_access_lvl 		VARCHAR2(50);
	CURSOR price_request
	IS
	SELECT c901_status
		  FROM t7501_account_price_req_dtl
		 WHERE c7500_account_price_request_id = p_acc_pri_req_id
		  AND c7501_void_fl IS NULL FOR UPDATE;
	BEGIN
		
		
		
		IF p_party_category = 'CACN' THEN
			UPDATE t7501_account_price_req_dtl		
		       SET c901_status = 52123  -- Pending PC Approval
		     WHERE c7500_account_price_request_id = p_acc_pri_req_id 
		       AND c901_status IN (52124)  -- Pending from PRQSTS
		       AND c7501_void_fl IS NULL;  
		ELSE 
			UPDATE t7501_account_price_req_dtl
		       SET c901_status = 52123  -- Pending PC Approval
		     WHERE c7500_account_price_request_id = p_acc_pri_req_id 
		       AND c7501_proposed_price < c7501_tripwire_price
		       AND c901_status IN (52124)
		       AND c7501_void_fl IS NULL; -- Initiated and Pending from PRQSTS

		    UPDATE t7501_account_price_req_dtl
		       SET c901_status = decode(p_initiated_team,'SATM',52125,'PRTM',52187),  -- 52125 [Approved from PRQSTS] 52187 [Pending AD Approval]
		           C7501_APPROVED_BY = decode(p_initiated_team,'SATM',p_user_id,'PRTM',c7501_approved_by),
			       C7501_APPROVED_DATE = decode(p_initiated_team,'SATM',current_date,'PRTM',c7501_approved_date)
		     WHERE c7500_account_price_request_id = p_acc_pri_req_id 
		       AND c7501_proposed_price >= c7501_tripwire_price
		       AND c901_status IN (52124,52123,52187)
		       AND c7501_void_fl IS NULL; -- 52124 [Pending from PRQSTS] 52123 [Pending PC Approval] , 52187 [Pending AD Approval]
		  END IF;
		  
		  SELECT c101_access_level_id INTO v_access_lvl FROM t101_user WHERE c101_user_id = p_user_id;
		  
		  --If AD Approved request from submitted request screen
		  IF v_access_lvl = '3' THEN 
	      	UPDATE t7501_account_price_req_dtl		
		       SET c901_status = 52123  -- Pending PC Approval
		     WHERE c7500_account_price_request_id = p_acc_pri_req_id 
		       AND c901_status IN (52187)  -- Pending AD Approval from PRQSTS
		       AND c7501_void_fl IS NULL;
	      END IF;

	END gm_upd_approval_state;
	
/*******************************************************
   * Description : Procedure to fetch access permission
   * Author    : Tejeswara Reddy
 *******************************************************/
   FUNCTION gm_fch_user_grp_chk (
      p_partyid   	IN   t1530_access.c101_party_id%TYPE,
      p_grp_id    	IN   t1501_group_mapping.c1500_group_id%TYPE,
      p_company_id 	IN  t1501_group_mapping.C1900_COMPANY_ID%TYPE
   )
      RETURN NUMBER
   IS
      v_status   NUMBER;
   BEGIN
      BEGIN
         SELECT TO_CHAR (COUNT (1))
           INTO v_status
           FROM t1501_group_mapping
          WHERE c101_mapped_party_id = p_partyid 
            AND c1500_group_id = p_grp_id 
            AND c1501_void_fl IS NULL;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            v_status := 0;
      END;
      RETURN v_status;
   END gm_fch_user_grp_chk;
    
END gm_pkg_sm_price_approval_txn;

/