/* Formatted on 2010/06/10 15:07 (Formatter Plus v4.8.0) */

--@"C:\Database\Packages\Sales\gm_pkg_sm_adj_txn.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_sm_adj_txn
IS
--
	/******************************************************************************************
	* Description : Procedure for updating the Adjustment details for the Sales Order
	* author : HReddi
	*********************************************************************************************/
	PROCEDURE gm_sav_adj_details (
		p_order_id   	IN t501_order.c501_order_id%TYPE,
		p_input_string 	IN CLOB ,
		p_user_id   	IN t501_order.c501_created_by%TYPE
	)
	AS
		v_strlen    NUMBER := NVL (LENGTH (p_input_string), 0) ;
	    v_string    CLOB := p_input_string;
	    v_substring VARCHAR2 (1000) ;
	    v_part_number t502_item_order.c205_part_number_id%TYPE;
	    v_unit_price  t502_item_order.c502_unit_price%TYPE;
	    v_unit_price_adj  t502_item_order.c502_unit_price_adj_value%TYPE;
	    v_adj_code  t502_item_order.c901_unit_price_adj_code%TYPE;
	    v_item_price t502_item_order.c502_unit_price%TYPE;
	   
	    v_party_id			t704_account.c101_party_id%TYPE;
	    v_account_id 		t704_account.c704_account_id%TYPE;
	    v_list_price 		t205_part_number.c205_list_price%TYPE;	    
	    v_discount_offered 	t502_item_order.c502_discount_offered%TYPE;
		v_discount_type 	t502_item_order.c901_discount_type%TYPE; 
		v_system_cal_unit_price t502_item_order.c502_system_cal_unit_price%TYPE;  		
		v_parent_order_id 	t501_order.c501_parent_order_id%TYPE;
		v_adj_code_per 		t1706_party_price_adj.c1706_adj_value%TYPE;
		v_do_unit_price 	t502_item_order.C502_DO_UNIT_PRICE%TYPE;
		v_adj_code_val		t502_item_order.C502_ADJ_CODE_VALUE%TYPE;
		
	BEGIN      
		BEGIN
			 SELECT t704.c101_party_id
			      , t704.c704_account_id 
			   INTO v_party_id	
			      , v_account_id
			   FROM t704_account t704
			      , t501_order t501
			  WHERE t704.c704_account_id = t501.c704_account_id
			    AND t501.c501_order_id = p_order_id;			 
		EXCEPTION  WHEN NO_DATA_FOUND
		THEN
			v_party_id := NULL;
			v_account_id := NULL;
		END;	   
		
		BEGIN
			SELECT NVL(t501.c501_parent_order_id, t501.c501_order_id)
			  INTO v_parent_order_id
  			  FROM t501_order t501 
  			 WHERE t501.c501_order_id = p_order_id;	            
  		EXCEPTION WHEN NO_DATA_FOUND
  		THEN
  		  	v_parent_order_id := p_order_id;
  		END;
  	
		
	    IF v_strlen > 0 THEN
	        WHILE INSTR (v_string, '|') <> 0
	        LOOP
	            --
	            v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
	            v_string := SUBSTR (v_string, INSTR (v_string, '|') + 1) ;
	            --
	            v_part_number := NULL;
	            v_unit_price := NULL;
	            v_unit_price_adj := NULL;
	            v_adj_code := NULL;
	            --
	            v_part_number := SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1) ;
	            v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1) ;
	            v_unit_price := TO_NUMBER (SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1)) ;
	            v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1) ;
	            v_unit_price_adj := TO_NUMBER (SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1)) ;
	            v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1) ;
	            v_adj_code := TO_NUMBER (SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1)) ;
	            v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1) ;
	            v_item_price := TO_NUMBER (v_substring);
	            
	            INSERT INTO my_temp_acc_part_list 
			     VALUES (v_account_id, v_part_number); 

			   BEGIN  
		            SELECT c901_discount_type
					     , c7052_discount_offered
					     , c705_calculated_unit_price
					     , NVL(TRIM(get_part_price_list ( v_part_number,'C')),0)
					     , NVL(DECODE(v_adj_code,'107970',c1706_waste,'107971',c1706_revision),0)
					  INTO v_discount_type
					  	 , v_discount_offered
					 	 , v_system_cal_unit_price
					     , v_list_price 
					     , v_adj_code_per
					  FROM v705_part_price_in_acc
					 WHERE c101_party_id = v_party_id
					   AND c205_part_number_id = v_part_number;
					   
				EXCEPTION WHEN NO_DATA_FOUND THEN
						 v_list_price := NULL;	    
			    		 v_discount_offered := NULL;
						 v_discount_type := NULL; 
						 v_system_cal_unit_price := NULL;
						 v_adj_code_per := NULL;
				END;			
			
				v_adj_code_val	:= ((NVL(v_unit_price,0)-NVL(v_unit_price_adj,0)) * NVL(v_adj_code_per,0)/100);
				v_do_unit_price := NVL(v_item_price,0) + NVL(v_unit_price_adj,0) + NVL(v_adj_code_val,0);
              	            
	            UPDATE t502_item_order 
		   		   SET c502_unit_price = NVL(v_do_unit_price,0)
		             , c502_unit_price_adj_value = NVL(v_unit_price_adj,0)
		             , c901_unit_price_adj_code = DECODE(v_adj_code,0,NULL,v_adj_code)
		             , c502_list_price = v_list_price		             
		             , c502_discount_offered = v_discount_offered
		             , c901_discount_type = v_discount_type
		             , c502_system_cal_unit_price = v_system_cal_unit_price
		             , C502_DO_UNIT_PRICE = NVL(v_unit_price,0)
		             , C502_ADJ_CODE_VALUE = NVL(v_adj_code_val,0)
		         WHERE  c501_order_id IN (SELECT c501_order_id 
		         						   FROM t501_order t501 
		         						  WHERE (t501.c501_order_id =  p_order_id OR t501.c501_parent_order_id = v_parent_order_id) 
		         						    AND t501.c501_void_fl is null
		         						    AND NVL(t501.c901_order_type,-9999) != 2525
		         						    ) 
		           AND c205_part_number_id = v_part_number
		           AND c502_item_price = v_item_price
		           AND c502_unit_price IS NULL
		           AND c502_void_fl IS NULL
		           AND ROWNUM = 1;
		           	           
		           UPDATE t502_item_order 
		   		   SET c502_unit_price = NVL(v_do_unit_price,0)
		             , c502_unit_price_adj_value = NVL(v_unit_price_adj,0)
		             , c901_unit_price_adj_code = DECODE(v_adj_code,0,NULL,v_adj_code)
		             , c502_list_price = v_list_price		             
		             , c502_discount_offered = v_discount_offered
		             , c901_discount_type = v_discount_type
		             , c502_system_cal_unit_price = v_system_cal_unit_price
		             , C502_DO_UNIT_PRICE = NVL(v_unit_price,0)
		             , C502_ADJ_CODE_VALUE = NVL(v_adj_code_val,0)
		         WHERE  c501_order_id IN (SELECT c501_order_id 
		         						   FROM t501_order t501 
		         						  WHERE (t501.c501_order_id =  p_order_id OR t501.c501_parent_order_id = v_parent_order_id) 
		         						    AND t501.c501_void_fl is null
		         						    AND t501.c901_order_type = 2525
		         						    ) 
		           AND c205_part_number_id = v_part_number
		           AND c502_item_price = v_item_price
		           AND c502_unit_price IS NULL
		           AND c502_void_fl IS NULL;
		           
	        END LOOP;
    END IF;	           
	END gm_sav_adj_details;
	
   /******************************************************************************************
	* Description : Procedure for updating the Adjustment details for the Sales Order.
	*               This Procedure should be called where ever the delete and insert for 
	*               Item Order is used.
	* Author : VPrasath
	******************************************************************************************/
	PROCEDURE gm_sav_ord_adj_details(
		p_item_order_id 		IN 	t502_item_order.c502_item_order_id%TYPE
	  , p_list_price 			IN 	t502_item_order.c502_list_price%TYPE
	  , p_unit_price 			IN 	t502_item_order.c502_unit_price%TYPE
	  , p_unit_price_adj_code 	IN 	t502_item_order.c901_unit_price_adj_code%TYPE
	  , p_unit_price_adj_value 	IN 	t502_item_order.c502_unit_price_adj_value%TYPE
	  , p_discount_offered 		IN 	t502_item_order.c502_discount_offered%TYPE
	  , p_discount_type 		IN 	t502_item_order.c901_discount_type%TYPE
	  , p_system_cal_unit_price IN 	t502_item_order.c502_system_cal_unit_price%TYPE
	  , p_account_price 		IN 	t502_item_order.C502_DO_UNIT_PRICE%TYPE
	  , p_adj_code_value		IN 	t502_item_order.C502_ADJ_CODE_VALUE%TYPE
	) 
	AS
	
	BEGIN
		UPDATE t502_item_order 
   		   SET c502_unit_price = p_unit_price
             , c502_unit_price_adj_value = NVL(p_unit_price_adj_value,0)
             , c901_unit_price_adj_code = DECODE(p_unit_price_adj_code,0,NULL,p_unit_price_adj_code)
             , c502_list_price = p_list_price             
             , c502_discount_offered = p_discount_offered
             , c901_discount_type = p_discount_type
             , c502_system_cal_unit_price = p_system_cal_unit_price
             , C502_DO_UNIT_PRICE = NVL(p_account_price,0)
		     , C502_ADJ_CODE_VALUE = NVL(p_adj_code_value,0)
         WHERE c502_item_order_id = p_item_order_id;		           
	END gm_sav_ord_adj_details; 
	
   /******************************************************************************************************
	* Description : Function to get updated Wasted/Revision for the selected Account and Adjuctment Code.
	* author : HReddi
	********************************************************************************************************/
	FUNCTION get_adj_code_value (
		p_order_id   	IN		t501_order.c501_order_id%TYPE,
		p_part_num		IN 		t502_item_order.c205_part_number_id%TYPE,
		p_item_ord_id   IN      t502_item_order.c502_item_order_id%TYPE,
		p_adj_code		IN 	    t502_item_order.c901_unit_price_adj_code%TYPE
	)
	RETURN t1706_party_price_adj.c1706_adj_value%TYPE
	IS
		v_party_id t704_account.c101_party_id%TYPE;
		v_account_id t704_account.c704_account_id%TYPE;
		v_adj_price VARCHAR2(10);
	BEGIN
		
		BEGIN
			SELECT t704.C101_PARTY_ID  INTO v_party_id
  			  FROM t501_order t501 , t704_account t704
             WHERE C501_ORDER_ID = p_order_id
             and T501.C704_ACCOUNT_ID = t704.C704_ACCOUNT_ID
               AND T501.C501_VOID_FL IS NULL 
               AND C704_VOID_FL IS NULL ;
		EXCEPTION
			WHEN NO_DATA_FOUND
		THEN
			v_party_id := NULL;
		END; 
               
		/* The following code is added for Getting the Wasted and Revision adjustments based on the Account Setup Screen or Based on the Group Setup Svreen.
			 * IF we have no Adjustment for the selected Account, then trying to get the Adjustemtn Details from Group Based Adjustments and vice versa.*/
				
		INSERT INTO my_temp_acc_part_list
			     SELECT t501.c704_account_id, p_part_num
		           FROM t501_order t501
		          WHERE t501.c501_order_id = p_order_id
					AND t501.c501_void_fl IS NULL;
           	
		BEGIN
           	 SELECT NVL(DECODE(p_adj_code,'107970',c1706_waste,'107971',c1706_revision),0)
           	   INTO v_adj_price
        	   FROM v705_part_price_in_acc
		      WHERE c101_party_id = v_party_id               
	            AND c205_part_number_id = p_part_num;
	            
   		EXCEPTION
			WHEN NO_DATA_FOUND
		THEN
			v_adj_price := 0;
		END;  
			
		RETURN v_adj_price;
	END get_adj_code_value;
	
	/******************************************************************************************
	* Description : Procedure for updating the Adjustment details for the BBA order
	* author : arajan
	*********************************************************************************************/
	PROCEDURE gm_sav_adj_details_bba (
		p_order_id   	IN t501_order.c501_order_id%TYPE,
		p_user_id   	IN t501_order.c501_created_by%TYPE
	)
	AS
		v_ack_order_id 		t501_order.c501_ref_id%TYPE;
		
		CURSOR cur_part_det
		IS
			SELECT T502.C205_PART_NUMBER_ID partnum, T502.C502_ITEM_PRICE itemprice, T502.C901_TYPE ptype, 
				  T502.C502_LIST_PRICE lprice, T502.C502_UNIT_PRICE uprice, T502.C901_UNIT_PRICE_ADJ_CODE adjcode,
				  T502.C502_UNIT_PRICE_ADJ_VALUE adjval, T502.C901_DISCOUNT_TYPE disctype, T502.C502_DISCOUNT_OFFERED discount,
				  T502.C502_SYSTEM_CAL_UNIT_PRICE calcuprice, t502.C502_DO_UNIT_PRICE douprice, T502.C502_ADJ_CODE_VALUE adjvalue
			FROM T501_ORDER T501, T502_ITEM_ORDER T502
			WHERE T501.C501_ORDER_ID = T502.C501_ORDER_ID
			and T501.C501_ORDER_ID = v_ack_order_id
			AND T501.C901_ORDER_TYPE = '101260' -- Acknowledgement Order
			AND T501.C501_VOID_FL IS NULL
			and T502.C502_VOID_FL is null;
	BEGIN
	-- Get the ACK order id
		BEGIN
			SELECT C501_REF_ID 
				INTO v_ack_order_id
				FROM T501_ORDER 
			WHERE C501_ORDER_ID = p_order_id
				AND C501_REF_ID IS NOT NULL
				AND C501_VOID_FL IS NULL;
		EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			v_ack_order_id := NULL;
		END;
		-- No need to update, if the ACK order id is not there
		IF v_ack_order_id IS NOT NULL THEN
			FOR v_part IN cur_part_det
			LOOP
				UPDATE t502_item_order 
		   		   SET c502_unit_price = NVL(v_part.uprice,0)
		             , c502_unit_price_adj_value = NVL(v_part.adjval,0)
		             , c901_unit_price_adj_code = DECODE(v_part.adjcode,0,NULL,v_part.adjcode)
		             , c502_list_price = v_part.lprice
		             , c502_discount_offered = v_part.discount
		             , c901_discount_type = v_part.disctype
		             , c502_system_cal_unit_price = v_part.calcuprice
		             , C502_DO_UNIT_PRICE = NVL(v_part.douprice,0)
		             , C502_ADJ_CODE_VALUE = NVL(v_part.adjvalue,0)
		         WHERE  c501_order_id = p_order_id
		           AND c205_part_number_id = v_part.partnum
		           AND c502_item_price = v_part.itemprice
		           AND c502_unit_price IS NULL
		           AND c502_void_fl IS NULL;
			END LOOP;
		END IF;
		
	END gm_sav_adj_details_bba;
/*****************************************************************************************
* Description : Procedure for used for validate the  required date is greater than equal to 45 day or not 
* author : tramasamy
*********************************************************************************************/
    PROCEDURE gm_ack_ord_reqdate_validation (
        p_order_id   IN   t501_order.c501_order_id%TYPE,
        p_req_date   IN   t501a_order_attribute.c501a_attribute_value%TYPE
    ) AS

        v_company_id      t1900_company.c1900_company_id%TYPE;
        v_order_date      t501_order.c501_order_date%TYPE;
        v_date_fmt        VARCHAR2(20);
        v_business_date   DATE;
        v_email_to t906_rules.c906_rule_value%type;
        v_email_msg t906_rules.c906_rule_value%type;
        v_email_sub t906_rules.c906_rule_value%type;
        v_str 				  CLOB;
        v_mail_body           CLOB;
        v_col_str			VARCHAR2 (1000);
        v_end_row_str		VARCHAR2 (1000);
        v_table_str	          CLOB;
        v_account_nm  t704_account.c704_account_nm%type;
        v_ack_business_days_count NUMBER;
    BEGIN
	    --get company id , date format
        SELECT
            get_compid_frm_cntx(),
            get_compdtfmt_frm_cntx()
        INTO
            v_company_id,
            v_date_fmt
        FROM
            dual;
      
      v_ack_business_days_count := NVL(get_rule_value('ACK_BUSINESS_DAYS','ACK_ORDER_DATE_VAL'),'0');
      
     BEGIN
        SELECT t501.c501_order_date,t704.c704_account_nm
          INTO v_order_date,v_account_nm
          FROM t501_order t501 ,t704_account t704
         WHERE c501_order_id = p_order_id
           AND t501.c704_account_id = t704.c704_account_id
           AND t501.c501_void_fl IS NULL
           AND t704.c704_void_fl  IS NULL;
     EXCEPTION
			WHEN OTHERS
			THEN
			RETURN;
			END;

 
        --To get business day only 
        v_business_date := get_business_day(v_order_date, v_ack_business_days_count);
 -- below string is the ACK order Required date  details
        v_col_str := '<TR><TD>';
        v_end_row_str := '</TD><TD>';
        v_str := v_str|| v_col_str || v_account_nm || v_end_row_str || p_order_id|| v_end_row_str ||p_req_date|| v_end_row_str|| to_char(v_order_date, v_date_fmt)|| '</TD></TR>';
        
        IF ((TO_DATE(p_req_date, v_date_fmt) < v_business_date) OR p_req_date IS NULL)  THEN 
            SELECT
                get_rule_value('ACK_ORDER_EMAIL_TO', 'ACK_ORDER_DATE_VAL')
            INTO v_email_to
            FROM
                dual;

            SELECT
                get_rule_value('ACK_ORDER_EMAIL_SUB', 'ACK_ORDER_DATE_VAL')
            INTO v_email_sub
            FROM
                dual;

            SELECT
                get_rule_value('ACK_ORDER_EMAIL_MSG', 'ACK_ORDER_DATE_VAL')
            INTO v_email_msg
            FROM
                dual;

            v_table_str := '<TABLE style="border: 1px solid  #676767">'
                           || '<TR><TD width=200><b>Account Name</b></TD><TD width=150><b>Ack Order</b></TD><TD width=150><b>Required Date</b></TD>'
                           || '<TD width=150><b>Created Date</b></TD></TR>';
            v_mail_body := '<style>TD{ FONT-SIZE: 11px; COLOR: #000000; FONT-FAMILY: verdana, arial, sans-serif;}</style><font face=arial size="2">';
            v_mail_body := v_mail_body
                           || v_email_msg
                           || '<br><br>'
                           || v_table_str
                           || v_str;
                  -- call the email procedure to send email to the user
            gm_com_send_html_email(v_email_to, v_email_sub, NULL, v_mail_body);
        END IF;

END gm_ack_ord_reqdate_validation;
END gm_pkg_sm_adj_txn;
/



