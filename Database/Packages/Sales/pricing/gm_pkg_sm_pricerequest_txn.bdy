/** 
FileName : gm_pkg_sm_pricerequest_txn.bdy
Description : 
Author : hreddy 
Date : 
Copyright : Globus Medical Inc 
*/
CREATE OR REPLACE PACKAGE BODY gm_pkg_sm_pricerequest_txn
IS
    
 /***********************************************************************
  * Author : HREDDY
  * Description : Save Pricing Request
  ************************************************************************/   
 PROCEDURE gm_sav_update_price_req_main (
 	p_input_string			IN	CLOB,
 	p_price_req_id  		IN  T7500_ACCOUNT_PRICE_REQUEST.C7500_ACCOUNT_PRICE_REQUEST_ID%TYPE,
 	p_price_req_status 		IN  T7500_ACCOUNT_PRICE_REQUEST.C901_REQUEST_STATUS%TYPE,
 	p_user_id               IN  T7500_ACCOUNT_PRICE_REQUEST.C7500_CREATED_BY%TYPE,
 	p_select_systems        IN  CLOB,
 	p_out_req_id  			OUT VARCHAR2
 )AS 
 	CURSOR price_request
	IS
	SELECT c901_request_type ,c901_request_status ,c1900_company_id , c7500_last_12months_sale ,
		  c7500_projected_12months_sale ,c7530_gpo_reference_id , c7531_gpo_reference_detail_id ,
		  c703_sales_rep_id ,c101_ad_id ,c101_vp_ad ,c7500_last_updated_by ,c7500_last_updated_date ,
		  c101_gpb_id ,c704_account_id
	FROM t7500_account_price_request
	WHERE c7500_account_price_request_id = p_price_req_id
	AND c7500_void_fl IS NULL FOR UPDATE;
	
	CURSOR price_request_group
	IS
	SELECT c7501_current_price ,c7501_list_price ,c7501_proposed_price ,c7501_tripwire_price ,
	  c7501_qty ,c901_status ,c901_construct_level ,c7501_implement_fl ,c7501_last_updated_date ,
	  c7501_last_updated_by ,c7003_principal_fl ,c4010_specl_group_fl ,c205_part_number_id ,
	  c901_group_type ,c901_change_type ,c7501_change_value ,c7003_system_construct_id ,
	  c4010_parent_group_id
	FROM t7501_account_price_req_dtl
	WHERE c7500_account_price_request_id = p_price_req_id
	--AND C901_STATUS NOT IN (52125,52190)  -- ignore approved and approved for implemented records
	AND c7501_void_fl IS NULL FOR UPDATE;
	
	CURSOR price_request_system
	IS
	SELECT c901_change_type ,c7502_change_value ,c7502_updated_by ,c7502_updated_date
	FROM t7502_account_price_req_sys
	WHERE c7500_account_price_req_id = p_price_req_id
	AND c7502_void_fl IS NULL FOR UPDATE;
	
	v_request_id 			t7500_account_price_request.c7500_account_price_request_id%TYPE;
	v_request_status        t7500_account_price_request.C901_REQUEST_STATUS%TYPE;
	v_created_by            t7500_account_price_request.C7500_CREATED_BY%TYPE;
    v_json_str              json;
	v_clob 					clob := ' ';
	v_status				t7500_account_price_request.C901_REQUEST_STATUS%TYPE;
    v_json_list 			json_list;

 BEGIN	
	--getting pricing request details
	gm_sav_upd_price_requset(p_input_string,p_user_id,v_request_id,v_request_status,v_created_by);	
	-- Getting the Group information details
	gm_sav_upd_price_req_details (p_input_string,p_select_systems,v_request_id, v_request_status, v_created_by );	
	-- Getting the System - Price Percentage information details
	gm_sav_upd_price_system_dtls (p_input_string,v_request_id,v_created_by );
	-- Geeting the Business Questions details
	gm_sav_upd_business_questions (p_input_string,v_request_id, v_created_by );
				
	-- VOID the unselected System from the saved PR-XXXXX.
			UPDATE t7501_account_price_req_dtl
			   SET c7501_void_fl  = 'Y'
			     , c7501_last_updated_by = v_created_by
			     , c7501_last_updated_date = SYSDATE
			 WHERE c7500_account_price_request_id = v_request_id
			   AND c207_set_id NOT IN (SELECT TOKEN FROM v_in_list)
			   AND c7501_void_fl IS NULL;
			
			-- VOID the unselected System -Price Percentage details from the saved PR-XXXXX.   
			UPDATE t7502_account_price_req_sys
			   SET c7502_void_fl  = 'Y'
			     , c7502_updated_by = v_created_by
			     , c7502_updated_date = SYSDATE
			 WHERE c7500_account_price_req_id = v_request_id
			   AND c207_set_id NOT IN (SELECT TOKEN FROM v_in_list)
			   AND c7502_void_fl IS NULL;
			    
	       IF v_request_status = 52124 THEN
       		UPDATE t7501_account_price_req_dtl
                        SET c901_status  = '52124' --Pending
                          , c7501_last_updated_by = v_created_by
                          , c7501_last_updated_date = SYSDATE
                     WHERE c7500_account_price_request_id = v_request_id
                        AND c207_set_id NOT IN (SELECT TOKEN FROM v_in_list)
                        and c901_status in ('52186')
                        AND c901_Status not in (52125,52190)  -- approved & Aprroved implemented
                        AND c7501_void_fl IS NULL;
       		  --To update t7501_account_price_req_dtl.c901_status as approved,denied,pending PC approval,pending AD approval
              gm_pkg_sm_price_approval_txn.gm_upd_price_req_approval(v_request_id,p_user_id);
         
              -- Calling the below procedure for updating the T7500 Request Status based on the Group Status.
    		  gm_pkg_sm_pricerequest_txn.gm_upd_price_req_status(v_request_id,p_user_id);
    		  
       END IF;
       SELECT c901_request_status INTO v_request_status
                  FROM T7500_ACCOUNT_PRICE_REQUEST 
                 WHERE c7500_account_price_request_id = v_request_id 
                   AND c7500_void_fl IS NULL;
                   
       p_out_req_id := v_request_id || '@' || v_request_status;

	
END gm_sav_update_price_req_main;

/***************************************************************************
 * Author : HREDDY
 * Description : Save Pricing Request into T7500_ACCOUNT_PRICE_REQUEST table
 ****************************************************************************/   
	PROCEDURE gm_sav_upd_price_requset (
      p_input_string		IN		CLOB,
      p_user_id				IN 		T101_USER.C101_USER_ID%TYPE,
	  p_request_id          OUT     T7500_ACCOUNT_PRICE_REQUEST.C7500_ACCOUNT_PRICE_REQUEST_ID%TYPE,
	  p_request_status      OUT     T7500_ACCOUNT_PRICE_REQUEST.C901_REQUEST_STATUS%TYPE,
	  p_created_by          OUT     T7500_ACCOUNT_PRICE_REQUEST.C7500_CREATED_BY%TYPE
     )
     AS         
	  v_request_type                 T7500_ACCOUNT_PRICE_REQUEST.C901_REQUEST_TYPE%TYPE;
      v_request_status               T7500_ACCOUNT_PRICE_REQUEST.C901_REQUEST_STATUS%TYPE;
	  v_company_id                   T7500_ACCOUNT_PRICE_REQUEST.C1900_COMPANY_ID%TYPE;
	  v_last_12months_sale           T7500_ACCOUNT_PRICE_REQUEST.C7500_LAST_12MONTHS_SALE%TYPE;
	  v_projected_12months_sale      T7500_ACCOUNT_PRICE_REQUEST.C7500_PROJECTED_12MONTHS_SALE%TYPE;
	  v_gpo_reference_id             T7500_ACCOUNT_PRICE_REQUEST.C7530_GPO_REFERENCE_ID%TYPE;
	  v_gpo_reference_detail_id      T7500_ACCOUNT_PRICE_REQUEST.C7531_GPO_REFERENCE_DETAIL_ID%TYPE;
	  v_sales_rep_id                 T7500_ACCOUNT_PRICE_REQUEST.C703_SALES_REP_ID%TYPE;
	  v_ad_id                        T7500_ACCOUNT_PRICE_REQUEST.C101_AD_ID%TYPE;
	  v_vp_ad_id                     T7500_ACCOUNT_PRICE_REQUEST.C101_VP_AD%TYPE;
	  v_created_by                   T7500_ACCOUNT_PRICE_REQUEST.C7500_CREATED_BY%TYPE;
	  v_request_id                   T7500_ACCOUNT_PRICE_REQUEST.C7500_ACCOUNT_PRICE_REQUEST_ID%TYPE;
	  v_party_id					 T7500_ACCOUNT_PRICE_REQUEST.C101_GPB_ID%TYPE;
	  v_account_id				 	 T704_ACCOUNT.C704_ACCOUNT_ID%TYPE;
	  v_save_prt_id                  T7500_ACCOUNT_PRICE_REQUEST.C7500_ACCOUNT_PRICE_REQUEST_ID%TYPE;
	  v_type_id		                 T901_CODE_LOOKUP.C901_CODE_ID%TYPE;
	  v_gpb_party_id   		         T101_USER.C101_PARTY_ID%TYPE;
	  v_initiated_team 				 VARCHAR2(20);
	  v_access_level				 T101_USER.C101_ACCESS_LEVEL_ID%TYPE;
	  v_out_req_id					 T7500_ACCOUNT_PRICE_REQUEST.C7500_ACCOUNT_PRICE_REQUEST_ID%TYPE;
  	  v_json 						 json_object_t;

      BEGIN
	      /*Oracle 12C has default API to get the VO object from the JSON input string*/
	      v_json := json_object_t.parse(p_input_string);
	      v_request_type := v_json.get_object('gmAccountPriceRequestVO').get_String('requestType');
	      v_request_status := v_json.get_object('gmAccountPriceRequestVO').get_String('requestStatus');
	      v_company_id := v_json.get_object('gmAccountPriceRequestVO').get_String('companyId');
	      v_last_12months_sale := v_json.get_object('gmAccountPriceRequestVO').get_String('last12MonthsSale');
	      v_projected_12months_sale := v_json.get_object('gmAccountPriceRequestVO').get_String('projected12MonthsSale');
	      v_gpo_reference_id := v_json.get_object('gmAccountPriceRequestVO').get_String('gpoReferenceId');
	      v_gpo_reference_detail_id := v_json.get_object('gmAccountPriceRequestVO').get_String('gpoReferenceDetailId');
	      v_sales_rep_id := v_json.get_object('gmAccountPriceRequestVO').get_String('salesRepId');
	      v_ad_id := v_json.get_object('gmAccountPriceRequestVO').get_String('adId');
	      v_vp_ad_id := v_json.get_object('gmAccountPriceRequestVO').get_String('vpAD');
	      v_created_by := v_json.get_object('gmAccountPriceRequestVO').get_String('createdBy');
	      v_account_id := v_json.get_object('gmAccountPriceRequestVO').get_String('partyid');
	      v_save_prt_id := v_json.get_object('gmAccountPriceRequestVO').get_String('pricerequestid');
	      v_type_id := v_json.get_object('gmAccountPriceRequestVO').get_String('typeid');

	      BEGIN
				SELECT C101_PARTY_ID ,C101_ACCESS_LEVEL_ID
					INTO v_gpb_party_id , v_access_level
					 FROM T101_USER 
					 WHERE C101_USER_ID = p_user_id;	
	   		EXCEPTION WHEN NO_DATA_FOUND THEN 
		    	v_party_id := NULL;
  	    	END;
         
  	    
	      IF v_type_id = '903107' THEN  -- Account
		      SELECT c101_party_id , c703_sales_rep_id INTO v_party_id   , v_sales_rep_id
		      FROM t704_account 
		      WHERE c704_account_id  = v_account_id 
		      AND c704_void_fl IS NULL;
		      
		      SELECT AD_ID, VP_ID INTO v_ad_id, v_vp_ad_id
		      FROM v700_territory_mapping_detail 
		      WHERE AC_ID = v_account_id;
		  ELSE
		  /*- Procedure for getting the who initiated the Price Request */
        	gm_pkg_sm_price_approval_txn.gm_get_initiated_team(p_user_id,v_company_id,v_initiated_team);	
  	  		  v_party_id := v_account_id;
  	  		  
  	    /* If the user access level is less than "3" and the user is from SATM then the salesrepid will get based on the party id,and get
  	     * the VP and AD id based on the salesrepid
  	     * If the user access level is >2 nd the user is from SATM then get the VP and AD id based on the user id 
  	     **/
			IF v_initiated_team = 'SATM' and v_access_level < '3' THEN
			  	  v_sales_rep_id := get_rep_id_from_party_id(v_gpb_party_id);
			  	  IF v_sales_rep_id IS NOT NULL THEN
				  	  SELECT AD_ID, VP_ID 
				  	 	INTO v_ad_id, v_vp_ad_id
				      FROM v700_territory_mapping_detail 
				      WHERE rep_id = v_sales_rep_id
				      AND ROWNUM=1;
				   END IF;
			ELSIF v_initiated_team = 'SATM' AND v_access_level > '2' THEN		
					 SELECT AD_ID, VP_ID 
				  	 	INTO v_ad_id, v_vp_ad_id
				      FROM v700_territory_mapping_detail 
				      WHERE (ad_id = p_user_id OR vp_id = p_user_id)
				      AND ROWNUM=1;		
			ELSE
			      v_sales_rep_id := NULL;		
			END IF;
		END IF;  
	      v_request_id := v_save_prt_id;
	     
	      /* common Procedure to save the details in T7500_ACCOUNT_PRICE_REQUEST table*/
	      gm_pkg_sm_pricerequest_txn.gm_sav_account_price_req(v_request_type,v_request_status,v_company_id,v_last_12months_sale,v_projected_12months_sale,
 							v_gpo_reference_id,v_gpo_reference_detail_id,v_sales_rep_id,v_ad_id,v_vp_ad_id,v_created_by,v_request_id,v_party_id,
 							v_account_id,v_type_id,v_out_req_id);
	     
 		  p_request_id	   := v_out_req_id;
		  p_request_status := v_request_status;
		  p_created_by := v_created_by;

      END gm_sav_upd_price_requset;
      
/***************************************************************************
 * Author : HREDDY
 * Description : Save Pricing Request into T7501_ACCOUNT_PRICE_REQ_DTL table
 ****************************************************************************/      
      PROCEDURE gm_sav_upd_price_req_details (
        p_input_string				IN	CLOB,
        p_select_systems            IN  CLOB, 
	  	p_account_price_request_id  IN  T7500_ACCOUNT_PRICE_REQUEST.C7500_ACCOUNT_PRICE_REQUEST_ID%TYPE,
	    p_status 					IN  T7501_ACCOUNT_PRICE_REQ_DTL.C901_STATUS%TYPE,              
	    p_created_by 				IN  T7501_ACCOUNT_PRICE_REQ_DTL.C7501_CREATED_BY%TYPE
      )
      AS         
		   v_current_price                          T7501_ACCOUNT_PRICE_REQ_DTL.C7501_CURRENT_PRICE%TYPE;
		   v_gpo_price                          T7501_ACCOUNT_PRICE_REQ_DTL.C7501_CURRENT_PRICE%TYPE;
	       v_list_price                             T7501_ACCOUNT_PRICE_REQ_DTL.C7501_LIST_PRICE%TYPE;
	       v_tripwire_price                         T7501_ACCOUNT_PRICE_REQ_DTL.C7501_TRIPWIRE_PRICE%TYPE;
	       v_proposed_price                         T7501_ACCOUNT_PRICE_REQ_DTL.C7501_PROPOSED_PRICE%TYPE;
	       v_status                                 T7501_ACCOUNT_PRICE_REQ_DTL.C901_STATUS%TYPE;
	       v_quantity                               T7501_ACCOUNT_PRICE_REQ_DTL.C7501_QTY%TYPE;        
	       v_implement_flg                          T7501_ACCOUNT_PRICE_REQ_DTL.C7501_IMPLEMENT_FL%TYPE;
	       v_approved_by                            T7501_ACCOUNT_PRICE_REQ_DTL.C7501_APPROVED_BY%TYPE;
	       v_created_by                             T7501_ACCOUNT_PRICE_REQ_DTL.C7501_CREATED_BY%TYPE;
	       v_last_updated_by                        T7501_ACCOUNT_PRICE_REQ_DTL.C7501_LAST_UPDATED_BY%TYPE;
	       v_group_id                               T7501_ACCOUNT_PRICE_REQ_DTL.C4010_GROUP_ID%TYPE;
	       v_set_id                                 T7501_ACCOUNT_PRICE_REQ_DTL.C207_SET_ID%TYPE;
	       v_construct_level                        T7501_ACCOUNT_PRICE_REQ_DTL.C901_CONSTRUCT_LEVEL%TYPE;
	       v_princlipal_fl                          T7501_ACCOUNT_PRICE_REQ_DTL.C7003_PRINCIPAL_FL%TYPE;
	       v_specialitygroup_fl						T7501_ACCOUNT_PRICE_REQ_DTL.C4010_SPECL_GROUP_FL%TYPE;
	       v_partnum								T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE;
	       v_group_type								T7501_ACCOUNT_PRICE_REQ_DTL.C901_GROUP_TYPE%TYPE;
	       v_change_type							T7501_ACCOUNT_PRICE_REQ_DTL.c901_change_type%TYPE;
	       v_change_value							T7501_ACCOUNT_PRICE_REQ_DTL.c7501_change_value%TYPE;
	       v_construct_id							T7501_ACCOUNT_PRICE_REQ_DTL.C7003_SYSTEM_CONSTRUCT_ID%TYPE;
	       v_parentgroupid							T7501_ACCOUNT_PRICE_REQ_DTL.C4010_PARENT_GROUP_ID%TYPE;
	       v_check_fl 								T7501_ACCOUNT_PRICE_REQ_DTL.C7501_CHECK_FL%TYPE;
	       v_individual_change_value				T7501_ACCOUNT_PRICE_REQ_DTL.c7501_change_value%TYPE;
		   v_group_array 							json_array_t;
  		   v_group_obj 								json_object_t;
  		   v_json 									json_object_t;

      BEGIN
	      /*Oracle 12C has default API to get the VO object from the JSON input string
	       * Get the  Array object from the JSON using json_array_t data type and get the JSON object from the array */
	      	v_json := json_object_t.parse(p_input_string);
	 		my_context.set_my_inlist_ctx (p_select_systems);
	 		v_group_array := treat(v_json.get('gmGroupVO') as json_array_t) ;
	 		
	 		FOR i IN 0 .. v_group_array.get_size - 1 LOOP
        		 	v_group_obj := treat(v_group_array.get(i) as json_object_t);
        		 	v_current_price := v_group_obj.get_String('currentprice');
        		 	v_gpo_price := v_group_obj.get_String('gpoprice');
        		 	v_list_price := v_group_obj.get_String('listprice');
        		 	v_tripwire_price := v_group_obj.get_String('tripwireunitprice');
        		 	v_proposed_price := v_group_obj.get_String('proposedunitprice');
        		 	v_quantity := v_group_obj.get_String('quantity');
        		 	v_group_id := v_group_obj.get_String('groupid');
        		 	v_set_id := v_group_obj.get_String('systemid');
        		 	v_construct_level := v_group_obj.get_String('constructlevel');
        		 	v_princlipal_fl := v_group_obj.get_String('principalflag');
        		 	v_specialitygroup_fl := v_group_obj.get_String('specialitygroup');
        		 	v_partnum := v_group_obj.get_String('partnum');
        		 	v_group_type := v_group_obj.get_String('grouptype');
        		 	v_change_type := v_group_obj.get_String('changetype');
        		 	v_change_value := v_group_obj.get_String('changevalue');
        		 	v_construct_id := v_group_obj.get_String('constructid');
        		 	v_parentgroupid := v_group_obj.get_String('parentgroupid');
        		 	v_check_fl := v_group_obj.get_String('checkfl');
        			--to update the current price based on GPB/Account mapping
					v_current_price:= NVL(v_gpo_price,v_current_price);
					v_individual_change_value := v_group_obj.get_String('individualgrpchangevalue');
					v_change_value := NVL(v_individual_change_value,v_change_value);
					
	    /* common Procedure to save the details in t7501_account_price_req_dtl table*/
	      gm_pkg_sm_pricerequest_txn.gm_sav_group_price_req_dtl(v_current_price,v_gpo_price,v_list_price,v_tripwire_price,v_proposed_price,p_status,
 							v_quantity,NULL,NULL,p_created_by,p_created_by,v_group_id,v_set_id,v_construct_level,v_princlipal_fl,v_specialitygroup_fl,v_partnum,v_group_type,
 							v_change_type,v_change_value,v_construct_id,v_parentgroupid,v_check_fl,p_account_price_request_id);
 							
        	END LOOP;
  
      END gm_sav_upd_price_req_details;     
      
      
/*********************************************************************************
 * Author : HREDDY
 * Description : Save Threshold Questions and answers into T9010_ANSWER_DATA table
 **********************************************************************************/   
	PROCEDURE gm_sav_upd_business_questions (
		p_input_string				IN	CLOB,
		p_account_price_request_id  IN  t9010_answer_data.c9010_ref_id%TYPE,
		p_created_by 				IN  T7501_ACCOUNT_PRICE_REQ_DTL.C7501_CREATED_BY%TYPE
	)
    AS         
	   v_question_id            t9010_answer_data.C9011_QUESTION_ID%TYPE;
	   v_qns_desc               t9010_answer_data.C9010_ANSWER_DESC%TYPE;
	   v_rep_id                 t9010_answer_data.C9010_CREATED_BY%TYPE;
	   v_drpdwn_id              t9010_answer_data.C901_CONSULTANT%TYPE;
	   v_json 					json_object_t;
	   v_business_que_array 	json_array_t;
       v_business_que_obj		json_object_t;
    BEGIN
	     /*Oracle 12C has default API to get the VO object from the JSON input string
	       * Get the  Array object from the JSON using json_array_t data type and get the JSON object from the array */
		v_json := json_object_t.parse(p_input_string);
	 	v_business_que_array := treat(v_json.get('gmThresholdQuestionVO') as json_array_t) ;
	 		
       	FOR i IN 0 .. v_business_que_array.get_size - 1 LOOP
       		    v_business_que_obj  := treat(v_business_que_array.get(i) as json_object_t);
	        		v_question_id 	:= v_business_que_obj.get_String('questionid');
					v_qns_desc 		:= v_business_que_obj.get_String('questionanswer');
				  	v_rep_id 		:= v_business_que_obj.get_String('userid');
				  	v_drpdwn_id 	:= v_business_que_obj.get_String('dropdownid');

			  	UPDATE T9010_ANSWER_DATA
				SET C9010_ANSWER_DESC = v_qns_desc,
					C901_CONSULTANT = v_drpdwn_id,
					C9010_LAST_UPDATED_BY = p_created_by,
					C9010_LAST_UPDATED_DATE = SYSDATE
			  WHERE C9011_QUESTION_ID = v_question_id
				AND c9010_ref_id = p_account_price_request_id;
	
			  IF(SQL%ROWCOUNT = 0)
				THEN				
					INSERT INTO T9010_ANSWER_DATA (
					            C9010_ANSWER_DATA_ID
					          , C9011_QUESTION_ID
					          ,	C9010_ANSWER_DESC
					          , C9010_CREATED_BY
					          ,	C9010_CREATED_DATE
					          ,	C901_CONSULTANT
					          , c9010_ref_id
					          , c901_ref_type)
					     VALUES(S9010_ANSWER_DATA.nextval
					          ,	v_question_id
					          , v_qns_desc
					          ,	p_created_by
					          ,	CURRENT_DATE
					          , v_drpdwn_id
					          , p_account_price_request_id
					          , '52130');		-- Account Request Pricing			
			  END IF;			     
			     
    END LOOP;
 	END gm_sav_upd_business_questions; 
      
      
/*******************************************************************************************
 * Author : HREDDY
 * Description : Procedure for saving the Price Percentage details againist the Each System
 * into t7502_account_price_req_sys table
 *******************************************************************************************/       
	PROCEDURE gm_sav_upd_price_system_dtls (
        p_input_string				IN	CLOB,
	  	p_account_price_request_id  IN  t9010_answer_data.c9010_ref_id%TYPE,
	    p_created_by 				IN  T7501_ACCOUNT_PRICE_REQ_DTL.C7501_CREATED_BY%TYPE
    )
    AS         
		   v_set_id            		t207_set_master.C207_SET_ID%TYPE;
	       v_price_type             t9010_answer_data.C9010_ANSWER_DESC%TYPE;
	       v_price_value            t9010_answer_data.C9010_CREATED_BY%TYPE;
	       v_json 					json_object_t;
 		   v_systems_array			json_array_t;
  		   v_systems_obj 			json_object_t;

    BEGIN
	     /*Oracle 12C has default API to get the VO object from the JSON input string
	       * Get the  Array object from the JSON using json_array_t data type and get the JSON object from the array */
	    v_json := json_object_t.parse(p_input_string);
	 	v_systems_array := treat(v_json.get('gmSystemVO') as json_array_t) ;
	 		
       	FOR i IN 0 .. v_systems_array.get_size - 1 LOOP
    		v_systems_obj := treat(v_systems_array.get(i) as json_object_t);
			v_set_id := v_systems_obj.get_String('systemId');
			v_price_type := v_systems_obj.get_String('changetype');
		  	v_price_value := v_systems_obj.get_String('changevalue');
			 
		  	 /* Common Procedure to save the details in t7502_account_price_req_sys table*/
	      gm_pkg_sm_pricerequest_txn.gm_sav_price_req_system(v_price_type,v_price_value,p_created_by,v_set_id,p_account_price_request_id);
				
    	END LOOP;
    END gm_sav_upd_price_system_dtls;
    
 /************************************************************************
  * Author : HReddi
  * Description : Saving the submitted price request id details
  ************************************************************************/
	PROCEDURE gm_upd_price_req_details (
		p_requestid         	IN 	T7500_ACCOUNT_PRICE_REQUEST.C7500_ACCOUNT_PRICE_REQUEST_ID%TYPE
	  , p_input_str         	IN 	CLOB
	  , p_user_id				IN  t7501_account_price_req_dtl.C7501_CREATED_BY%TYPE
	  , p_out_req_id            OUT T7500_ACCOUNT_PRICE_REQUEST.C7500_ACCOUNT_PRICE_REQUEST_ID%TYPE
	)
	AS
	    CURSOR price_request
	    IS
		SELECT c901_change_type ,c7501_change_value ,c901_status ,c7501_proposed_price ,
			  c7501_last_updated_by ,c7501_approved_by ,c7501_last_updated_date ,c7501_approved_date ,
			  c7501_implement_fl ,c7501_current_price
			FROM t7501_account_price_req_dtl
			WHERE c7500_account_price_request_id = p_requestid
			AND c7501_void_fl IS NULL FOR UPDATE;
	
	 	v_string    CLOB := p_input_str;
     	v_strlen    NUMBER          := NVL (LENGTH (p_input_str), 0) ;
    	v_substring VARCHAR2 (4000) ;
    	v_system_id  	t7501_account_price_req_dtl.C207_SET_ID%TYPE;    	
        v_group_id	 	t7501_account_price_req_dtl.C4010_GROUP_ID%TYPE;
        v_change_type   t7501_account_price_req_dtl.C901_CHANGE_TYPE%TYPE;
        v_change_value  t7501_account_price_req_dtl.C7501_CHANGE_VALUE%TYPE;
        v_prop_price    t7501_account_price_req_dtl.C7501_PROPOSED_PRICE%TYPE;
        v_status_id     t7501_account_price_req_dtl.C901_STATUS %TYPE;
        v_impl_fl       t7501_account_price_req_dtl.C7501_IMPLEMENT_FL %TYPE;
        v_prc_dtl_id    t7501_account_price_req_dtl.c7501_account_price_req_dtl_id %TYPE;
    	v_is_error varchar2(1);
	BEGIN
    	IF v_strlen                      > 0 THEN
			
        WHILE INSTR (v_string, '|') <> 0
        LOOP
            v_substring  	:= SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
            v_string     	:= SUBSTR (v_string, INSTR (v_string, '|')    + 1) ;
            v_system_id  	:= NULL;
            v_group_id	 	:= NULL;
            v_status_id    	:= NULL;
            v_change_type   := NULL;
            v_change_value  := NULL;
            v_prop_price    := NULL;
            v_impl_fl		:= NULL;
            v_prc_dtl_id    := NULL;
            
            v_system_id    	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            v_substring  	:= SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
            v_status_id 	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            v_substring  	:= SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
            v_change_type 	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            v_substring  	:= SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
            v_group_id 		:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            v_substring  	:= SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
            v_change_value 	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            v_substring  	:= SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;  
            v_prop_price 	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            v_substring  	:= SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
            v_impl_fl 	    := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            v_substring  	:= SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
            v_prc_dtl_id    := v_substring ;
            --Validating status
            IF v_impl_fl <> 'Y' THEN
	            v_is_error := validate_current_status(v_prc_dtl_id,v_status_id);
	            IF (v_is_error = 'Y') THEN
	      			  raise_application_error(-20999, 'Status cannot be rolled back. Please reload data and try again');
	   			END IF;
   			END IF;
            
              UPDATE t7501_account_price_req_dtl
                 SET c901_change_type = v_change_type
                   , c7501_change_value = v_change_value
                   , c901_status = v_status_id
                   , c7501_proposed_price = v_prop_price
                   , c7501_last_updated_by = p_user_id
                   , C7501_APPROVED_BY = p_user_id
                   , c7501_last_updated_date = CURRENT_DATE
                   , C7501_APPROVED_DATE = CURRENT_DATE
                   , C7501_IMPLEMENT_FL = v_impl_fl
                   , c7501_current_price = DECODE(v_impl_fl,'Y',v_prop_price,c7501_current_price)
               WHERE c207_set_id = v_system_id
                 AND c4010_group_id = v_group_id
                 AND c7500_account_price_request_id = p_requestid
                 AND c7501_account_price_req_dtl_id = v_prc_dtl_id
                 AND c7501_void_fl IS NULL;
                
        END LOOP;
    END IF;
    --To update t7501_account_price_req_dtl.c901_status as approved,denied,pending PC approval,pending AD approval
    --gm_pkg_sm_price_approval_txn.gm_upd_price_req_approval(p_requestid,p_user_id);
    -- Calling the below procedure for updating the T7500 Request Status based on the Group Status.
    gm_pkg_sm_pricerequest_txn.gm_upd_price_req_status(p_requestid,p_user_id);
    
    p_out_req_id := p_requestid;
    
	END gm_upd_price_req_details;
    
 /***************************************************************
  * Author : HReddi
  * Description : Saving the submitted price request id details
  ***************************************************************/
	PROCEDURE gm_upd_price_req_status (
		p_requestid         	IN 		T7500_ACCOUNT_PRICE_REQUEST.C7500_ACCOUNT_PRICE_REQUEST_ID%TYPE
	  , p_user_id				IN      t7501_account_price_req_dtl.C7501_CREATED_BY%TYPE	
	)
	AS
		v_total_count  		NUMBER;
		v_init_count   		NUMBER;
		v_pend_appr_count   NUMBER;
		v_appr_count        NUMBER;
		v_appr_deny_count   NUMBER;
		v_deny_count        NUMBER;
		v_status       		VARCHAR2(10);
		v_status_label      VARCHAR2(50);
		v_appr_deny_tot_count   NUMBER;
		v_impl_count        NUMBER;
		v_access_lvl		VARCHAR2(50);
	
	BEGIN
		 SELECT count(1) INTO v_total_count 
		   FROM t7501_account_price_req_dtl 
		  WHERE c7500_account_price_request_id = p_requestid
		    AND c7501_proposed_price IS NOT NULL
		    AND c7501_void_fl IS NULL;
		  
         SELECT count(1) INTO v_init_count 
           FROM t7501_account_price_req_dtl 
          WHERE c7500_account_price_request_id = p_requestid            
            AND c901_status = 52186 -- Initiated
            AND c7501_void_fl IS NULL;
                
          SELECT count(1) INTO v_pend_appr_count 
            FROM t7501_account_price_req_dtl 
           WHERE c7500_account_price_request_id = p_requestid  
             AND (c901_status = 52187 OR c901_status = 52123 OR c901_status = 52124);  -- Pending AD Approval Pending PC Approval
          
          SELECT count(1) INTO v_appr_count 
            FROM t7501_account_price_req_dtl 
           WHERE c7500_account_price_request_id = p_requestid  
             AND c901_status = 52125;  -- Approved
         
          SELECT count(1) INTO v_appr_deny_count 
            FROM t7501_account_price_req_dtl 
           WHERE c7500_account_price_request_id = p_requestid  
             AND c901_status IN (52125,52120);  -- Approved / Denied
         
         SELECT count(1) INTO v_deny_count 
            FROM t7501_account_price_req_dtl 
           WHERE c7500_account_price_request_id = p_requestid 
             AND c901_status IN (52120);  --  Denied
             
         SELECT count(1) INTO v_impl_count
           FROM t7501_account_price_req_dtl 
          WHERE c7500_account_price_request_id = p_requestid 
            AND c7501_implement_fl  = 'Y' 
            AND c901_status IN (52125)  -- Approved / Denied
            AND c7501_void_fl IS NULL;
		
         v_appr_deny_tot_count := v_deny_count + v_appr_count;
             
           IF (v_total_count = v_init_count ) THEN
                v_status := 52186; -- Initiated
           ELSIF(v_total_count = v_impl_count) THEN
           		 v_status := 52190;  -- Approved Implemented
           ELSIF(v_total_count = v_appr_count) THEN
           		 v_status := 52189;  -- Approved Not Implemented           
           ELSIF(v_total_count = v_deny_count) THEN
           		 v_status := 52120; -- Denied
           ELSIF((v_appr_count > 0) AND (v_deny_count > 0) AND (v_total_count = v_appr_deny_tot_count)) THEN
           		 v_status := 52188; -- Approved & Denied;
           ELSIF(v_pend_appr_count > 0) THEN
           		 v_status := 52121; -- Pending Approval
           ELSE
           		 v_status := 52121; -- Pending Approval;
           END IF;
          

          SELECT c101_access_level_id INTO v_access_lvl FROM t101_user WHERE c101_user_id = p_user_id;
		  
		  --If sales Rep status Pending AD Approval
		  --PC-844 7-Assoc Rep
		  IF v_access_lvl < '3' OR v_access_lvl = '7' THEN 
	      	v_status := '52187'; --Pending AD Approval
	      END IF;
           
           
		    UPDATE t7500_account_price_request 
               SET c901_request_status = to_number(v_status)
               ,c7500_submitted_date = decode(v_status,52120,NULL,52121,CURRENT_TIMESTAMP,52187,CURRENT_TIMESTAMP,c7500_submitted_date)
               ,C7500_REP_SUB_DATE = decode(v_status,52120,NULL,52187,CURRENT_TIMESTAMP,C7500_REP_SUB_DATE)
                 , c7500_last_updated_by = p_user_id
                 , c7500_last_updated_date = SYSDATE
             WHERE c7500_account_price_request_id = p_requestid
               AND c7500_void_fl IS NULL;              
        
	END gm_upd_price_req_status;
	
	/***************************************************************
  * Author : HReddi
  * Description : Removing the Selected System details from PR-XXXX
  ***************************************************************/
	PROCEDURE gm_upd_system_details (
		p_requestid         	IN 		T7500_ACCOUNT_PRICE_REQUEST.C7500_ACCOUNT_PRICE_REQUEST_ID%TYPE
	  ,	p_system_id         	IN 		t7501_account_price_req_dtl.c207_set_id%TYPE
	  , p_user_id				IN      t7501_account_price_req_dtl.C7501_CREATED_BY%TYPE	
	  , p_out_req_id            OUT 	T7500_ACCOUNT_PRICE_REQUEST.C7500_ACCOUNT_PRICE_REQUEST_ID%TYPE
	)
	AS			
	BEGIN
		  -- UPDATEing the void flag in system table before updating the same in Price Details Table.     		   
          UPDATE t7502_account_price_req_sys
             SET c7502_void_fl = 'Y'
               , c7502_updated_by = p_user_id
               , c7502_updated_date = SYSDATE
            WHERE c7500_account_price_req_id = p_requestid
             AND c207_set_id = p_system_id
             AND c7502_void_fl IS NULL;
          
          -- UPDATEing the void flag in Price Request Details table.
           UPDATE t7501_account_price_req_dtl
              SET c7501_void_fl  = 'Y'
                , c7501_last_updated_by = p_user_id
                , c7501_last_updated_date = SYSDATE
            WHERE c7500_account_price_request_id = p_requestid
              AND c207_set_id = p_system_id
              AND c7501_void_fl IS NULL;
              
    	p_out_req_id := p_requestid;
    	
	END gm_upd_system_details;
	
/*********************************************************************************************
  * Author : HReddi
  * Description : Voiding the Selected Pricing Request from Report Dashboard scrren from IPAD
  ********************************************************************************************/
	PROCEDURE gm_void_pricing_request (
		p_requestid         	IN 		T7500_ACCOUNT_PRICE_REQUEST.C7500_ACCOUNT_PRICE_REQUEST_ID%TYPE	
	  , p_user_id				IN      t7501_account_price_req_dtl.C7501_CREATED_BY%TYPE
	)
	AS
		v_cancelid   			NUMBER;
		v_canceltypecode 		t907_cancel_log.c901_type%TYPE;
	BEGIN				
	
		-- VOIDING THE RECORD FROM T9010_ANSWER_DATA TABLE
		
		 UPDATE T9010_ANSWER_DATA
            SET c9010_void_fl  = 'Y'
              , c9010_last_updated_by = p_user_id
              , c9010_last_updated_date = CURRENT_DATE
          WHERE c9010_ref_id = p_requestid
            AND c9010_void_fl IS NULL;
            
        -- VOIDING THE RECORD FROM t7502_account_price_req_sys TABLE
		 
         UPDATE t7502_account_price_req_sys
            SET c7502_void_fl  = 'Y'
              , c7502_updated_by = p_user_id
              , c7502_updated_date = CURRENT_DATE
          WHERE c7500_account_price_req_id = p_requestid
            AND c7502_void_fl IS NULL;
            
		-- VOIDING THE RECORD FROM t7501_account_price_req_dtl TABLE     
		
         UPDATE t7501_account_price_req_dtl
            SET c7501_void_fl  = 'Y'
              , c7501_last_updated_by = p_user_id
              , c7501_last_updated_date = CURRENT_DATE
          WHERE c7500_account_price_request_id = p_requestid
            AND c7501_void_fl IS NULL;
            
        -- VOIDING THE RECORD FROM t7502_account_price_req_sys TABLE     
         
          UPDATE t7500_account_price_request
            SET c7500_void_fl  = 'Y'
              , c7500_last_updated_by = p_user_id
              , c7500_last_updated_date = CURRENT_DATE
          WHERE c7500_account_price_request_id = p_requestid
            AND c7500_void_fl IS NULL;
            
	END gm_void_pricing_request;

/*********************************************************************************************
  * Author : Matt
  * Description : Validating current status to prevent rollback of status 
  ********************************************************************************************/		
FUNCTION validate_current_status (
  	  p_req_dtl_id  IN   T7501_ACCOUNT_PRICE_REQ_DTL.C7501_ACCOUNT_PRICE_REQ_DTL_ID%TYPE
  	  , p_new_status IN T7501_ACCOUNT_PRICE_REQ_DTL.C901_STATUS%TYPE
   )
   RETURN VARCHAR2
IS
	v_Is_Error	   VARCHAR2 (1);
	v_current_status NUMBER;
BEGIN
     BEGIN
       SELECT C901_STATUS 
       INTO v_current_status 
       FROM T7501_ACCOUNT_PRICE_REQ_DTL
       WHERE C7501_ACCOUNT_PRICE_REQ_DTL_ID = p_req_dtl_id
       AND C7501_VOID_FL is NULL;
 	EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_Is_Error := 'N';
    END;
    
        IF((p_new_status <> 52190 and p_new_status <> 52125) AND (v_current_status = 52125))  THEN  
         v_Is_Error := 'Y';    -- approved can only be changed to approved implemented 
       ELSIF((p_new_status = 52124) AND (v_current_status <> 52124)) THEN  
          v_Is_Error := 'Y'; -- Cannot change status back to pending  
	   ELSIF ((p_new_status = 52186) AND (v_current_status <> 52186)) THEN               
           v_Is_Error := 'Y'; -- Cannot change status back to initiated 
       ELSE
           v_Is_Error := 'N';
       END IF;

	RETURN v_Is_Error;

END validate_current_status;

 /***********************************************************************
  * Author : MATT B
  * Description : Rollback pending Approval status
  ************************************************************************/ 
PROCEDURE gm_rollback_pending_approval (
	p_requestid         	IN 		T7500_ACCOUNT_PRICE_REQUEST.C7500_ACCOUNT_PRICE_REQUEST_ID%TYPE	
  , p_user_id				IN      t7501_account_price_req_dtl.C7501_CREATED_BY%TYPE
)
AS
   	v_count 	  			NUMBER;
BEGIN	
	 BEGIN
		SELECT COUNT(1) INTO v_count
		FROM T7500_ACCOUNT_PRICE_REQUEST
		WHERE C901_REQUEST_STATUS          =  52121
		AND C7500_ACCOUNT_PRICE_REQUEST_ID = p_requestId
		AND C7500_VOID_FL IS NULL;
     EXCEPTION
	 WHEN NO_DATA_FOUND THEN
	     v_count := 0;
	 END;

	IF(v_count = 1) THEN
	   -- Update T7500 Table
	 UPDATE T7500_ACCOUNT_PRICE_REQUEST 
	   SET C901_REQUEST_STATUS = 52186     -- initiated
	   , C7500_SUBMITTED_DATE = NULL
	   , C7500_REP_SUB_DATE = NULL
	   ,C7500_LAST_UPDATED_BY = p_user_id
	   ,C7500_LAST_UPDATED_DATE = CURRENT_DATE
	   WHERE C7500_ACCOUNT_PRICE_REQUEST_ID = p_requestId
	   AND C901_REQUEST_STATUS          =  52121
	   AND C7500_VOID_FL IS NULL;
	   
	   -- Update T7501 Table where status not Approved/implemented
	   
	   UPDATE T7501_ACCOUNT_PRICE_REQ_DTL 
	   SET C901_STATUS  = 52186     -- initiated
	   ,C7501_LAST_UPDATED_BY = p_user_id
	   ,C7501_LAST_UPDATED_DATE = CURRENT_DATE
	   WHERE C7500_ACCOUNT_PRICE_REQUEST_ID = p_requestId	   
	   AND C901_STATUS NOT IN (52189) -- Approved and implemented
	   AND C7501_VOID_FL IS NULL;
	   
	   --DELETE GPB COMPARISON DATA FOR NEW COMPARISON 
	   DELETE FROM t7553_gpo_ref_price_req_map WHERE C7500_ACCOUNT_PRICE_REQUEST_ID = p_requestId;
	   --Delete systems and group price details based on request id
	   DELETE FROM t7503_compare_price_req_detail WHERE C7500_ACCOUNT_PRICE_REQUEST_ID = p_requestid;	   
	   DELETE FROM T7571_SYSTEM_REF_PRICE_REQ_MAP WHERE C7500_ACCOUNT_PRICE_REQUEST_ID = p_requestid;		   
	
    ELSE
       GM_RAISE_APPLICATION_ERROR('-20999','400','');
	END IF;
	
	END gm_rollback_pending_approval;
	
 /***********************************************************************
  * Author : Arockia Prasath
  * Description : To update the price request comparison gpb id
  ************************************************************************/ 
PROCEDURE gm_upd_price_request_gpb_map (
	p_requestid         	IN 		T7500_ACCOUNT_PRICE_REQUEST.C7500_ACCOUNT_PRICE_REQUEST_ID%TYPE	
  , p_user_id				IN      t7501_account_price_req_dtl.C7501_CREATED_BY%TYPE
)
AS
   	v_ref_id 	  			t7550_gpo_reference.c7550_gpo_reference_id%TYPE;
   	v_account_id            t7500_account_price_request.c704_account_id%TYPE;
   	v_party_id				t7500_account_price_request.c101_gpb_id%TYPE;
BEGIN	
	
		SELECT MAX(c7550_gpo_reference_id) INTO v_ref_id
		FROM t7550_gpo_reference
		WHERE c7550_void_fl    IS NULL;
		
		UPDATE t7500_account_price_request
		SET c7550_gpo_reference_id          = v_ref_id,
		  c7500_last_updated_by             =p_user_id,
		  c7500_last_updated_date           =sysdate
		WHERE c7500_account_price_request_id=p_requestid
		AND c7500_void_fl                  IS NULL;
		
		 SELECT c704_account_id,
			  c101_gpb_id
			INTO v_account_id,
			  v_party_id
			FROM t7500_account_price_request
			WHERE c7500_account_price_request_id=p_requestid
			AND c7500_void_fl                  IS NULL;
			
		DELETE FROM t7553_gpo_ref_price_req_map WHERE c7500_account_price_request_id = p_requestid;
		
		IF v_account_id IS NOT NULL THEN
		
		INSERT
		INTO t7553_gpo_ref_price_req_map
		  (
		    c7553_gpo_ref_price_req_map_id ,
		    c7551_gpo_reference_detail_id ,
		    c7500_account_price_request_id
		  )
		  (
		  SELECT s7553_gpo_ref_price_req_map_id.nextval,  t7551.* FROM (
		  SELECT t7551.c7551_gpo_reference_detail_id,
		      t7500.c7500_account_price_request_id
		    FROM t704d_account_affln t704d,
		      t7500_account_price_request t7500,
		      t7560_gpo_gpb_mapping t7560,
		      t7551_gpo_reference_detail t7551
		    WHERE t704d.c704d_void_fl              IS NULL
		    AND t7500.c7500_void_fl                IS NULL
		    AND t7560.c7560_void_fl                IS NULL
		    AND t7551.c7551_void_fl                IS NULL
		    AND t7500.c704_account_id               = t704d.c704_account_id
		    AND t7560.c101_gpo                      = t704d.c101_gpo
		    AND t7551.c101_gpb_id                   = t7560.c101_gpb
		    AND t7551.c7550_gpo_reference_id        = t7500.c7550_gpo_reference_id
		    AND t7500.c7500_account_price_request_id=p_requestid 
		    GROUP BY t7551.c7551_gpo_reference_detail_id,
		      t7500.c7500_account_price_request_id) t7551		    
		  ) ;
		  
		  ELSE
		  
		  INSERT
			INTO t7553_gpo_ref_price_req_map
		  (
		    c7553_gpo_ref_price_req_map_id ,
		    c7551_gpo_reference_detail_id ,
		    c7500_account_price_request_id
		  )(
		  SELECT s7553_gpo_ref_price_req_map_id.nextval,  t7551.* FROM (
		  SELECT t7551.c7551_gpo_reference_detail_id,
			  t7500.c7500_account_price_request_id
			FROM
			  (SELECT t704d.c101_gpo,
			    t740.c101_party_id
			  FROM t704d_account_affln t704d,
			    t740_gpo_account_mapping t740
			  WHERE t704d.c704_account_id = t740.c704_account_id
			  AND t740.c101_party_id      = v_party_id
			  AND t704d.c101_gpo IS NOT NULL
			  AND t740.c740_void_fl      IS NULL
			  AND t704d.c704d_void_fl    IS NULL
			  GROUP BY t704d.c101_gpo,
			    t740.c101_party_id
			  ) t704d,
			  t7500_account_price_request t7500,
			  t7560_gpo_gpb_mapping t7560,
			  t7551_gpo_reference_detail t7551
			WHERE t7500.c7500_void_fl              IS NULL
			AND t7560.c7560_void_fl                IS NULL
			AND t7551.c7551_void_fl                IS NULL
			AND t7500.c101_gpb_id                   = t704d.c101_party_id
			AND t7560.c101_gpo                      = t704d.c101_gpo
			AND t7551.c101_gpb_id                   = t7560.c101_gpb
			AND t7551.c7550_gpo_reference_id        = t7500.c7550_gpo_reference_id
			AND t7500.c7500_account_price_request_id = p_requestid
			GROUP BY t7551.c7551_gpo_reference_detail_id,
		      t7500.c7500_account_price_request_id) t7551
			);
			
			END IF; 
		  
END gm_upd_price_request_gpb_map;
/***********************************************************************
  * Author : Arockia Prasath
  * Description : To insert the additional systems for gpb comparison request
  ************************************************************************/ 
PROCEDURE gm_upd_price_request_sys_map (
	p_requestid         	IN 		T7500_ACCOUNT_PRICE_REQUEST.C7500_ACCOUNT_PRICE_REQUEST_ID%TYPE	
  , p_user_id				IN      t7501_account_price_req_dtl.C7501_CREATED_BY%TYPE
)
AS
    v_account_id            t7500_account_price_request.c704_account_id%TYPE;
   	v_party_id				t7500_account_price_request.c101_gpb_id%TYPE;
   	v_select_systems  CLOB;
BEGIN	
	   
		 SELECT c704_account_id,
			  c101_gpb_id
			INTO v_account_id,
			  v_party_id
			FROM t7500_account_price_request
			WHERE c7500_account_price_request_id=p_requestid
			AND c7500_void_fl                  IS NULL;
		 
		IF v_account_id IS NOT NULL THEN
		--setting account party id and gpb id in context
		  my_context.set_my_double_inlist_ctx (v_party_id,get_account_gpo_id(v_account_id)) ;
		ELSE
		--setting gpb id in context
		  my_context.set_my_double_inlist_ctx (v_party_id,NULL) ;
		END IF; 
		
		 SELECT RTRIM(XMLAGG(XMLELEMENT(e,c207_mapped_system_id
		  || ',')).EXTRACT('//text()').getclobval(),',')
		INTO v_select_systems
		FROM t7571_system_ref_price_req_map WHERE c7500_account_price_request_id = p_requestid;
		
		my_context.set_my_inlist_ctx (v_select_systems);
		
		DELETE FROM t7503_compare_price_req_detail WHERE C7500_ACCOUNT_PRICE_REQUEST_ID = p_requestid;
		-- inserting contruct and groups details into t7503_compare_price_req_detail for GPB comparison additional systems
		INSERT
			INTO t7503_compare_price_req_detail
			  (
			    C7503_compare_PRICE_REQ_DTL_ID , C7500_ACCOUNT_PRICE_REQUEST_ID , C207_SET_ID , C7003_SYSTEM_CONSTRUCT_ID , C901_CONSTRUCT_LEVEL ,
			    C4010_GROUP_ID ,  C205_PART_NUMBER_ID ,  C7503_QTY , C7503_LIST_PRICE ,  C7503_TRIPWIRE_PRICE ,   C7503_CURRENT_PRICE ,
			    C7003_PRINCIPAL_FL ,  C7503_CREATED_BY , C7503_CREATED_DATE
			  )
			SELECT s7503_compare_price_req_dtl_id.nextval, t7571.c7500_account_price_request_id, v_cons_grp.c207_set_id,v_cons_grp.c7003_system_construct_id,  v_cons_grp.c901_construct_level,
			  v_cons_grp.c4010_group_id, v_cons_grp.c205_part_number_id, v_cons_grp.c7004_qty , v_cons_grp.unitlistprice ,  v_cons_grp.unittripwire,  v_cons_grp.current_price ,
			  v_cons_grp.c7003_principal_fl ,  p_user_id, SYSDATE
			FROM v_gm_prt_construct_group v_cons_grp,
			  t7571_system_ref_price_req_map t7571
			WHERE (DECODE(c7571_all_group_flag, 'Y',1,v_cons_grp.c7003_system_construct_id ) > 0
			OR DECODE(c7571_all_group_flag, 'Y','Y',NVL(v_cons_grp.speclgrpflag,'-999') )    = 'Y')
			AND t7571.c207_mapped_system_id                                                  = v_cons_grp.c207_set_id
			AND t7571.c7500_account_price_request_id = p_requestid
			AND t7571.c7571_void_fl                                                         IS NULL ; 
END gm_upd_price_request_sys_map;

/***********************************************************************
  * Author : Jreddy
  * Description : To insert the additional systems for gpb comparison request
  ************************************************************************/ 
PROCEDURE gm_sav_upd_add_systems (
	p_requestid         	IN 		T7500_ACCOUNT_PRICE_REQUEST.C7500_ACCOUNT_PRICE_REQUEST_ID%TYPE	
  , p_input_str				IN 		CLOB
  , p_user_id				IN      t7501_account_price_req_dtl.C7501_CREATED_BY%TYPE
  , P_out_data				OUT		CLOB
)
AS
	 v_string    	CLOB := p_input_str;
     v_strlen    	NUMBER          := NVL (LENGTH (p_input_str), 0) ;
     v_substring 	VARCHAR2 (4000) ;
     v_system_id  	T7571_SYSTEM_REF_PRICE_REQ_MAP.C207_MAPPED_SYSTEM_ID%TYPE;
     v_grp_fl   	T7571_SYSTEM_REF_PRICE_REQ_MAP.C7571_ALL_GROUP_FLAG%TYPE;
     v_sav_systems  CLOB; 
        
	BEGIN
		--Delete systems based on request id
		DELETE FROM T7571_SYSTEM_REF_PRICE_REQ_MAP WHERE C7500_ACCOUNT_PRICE_REQUEST_ID = p_requestid;
		
		IF v_strlen                      > 0 THEN		
        WHILE INSTR (v_string, '|') <> 0
        LOOP
            v_substring  	:= SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
            v_string     	:= SUBSTR (v_string, INSTR (v_string, '|')    + 1) ;
            v_system_id  	:= NULL;
            v_grp_fl    	:= NULL;
            
            v_system_id    	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            v_substring  	:= SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
            v_grp_fl 		:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            --v_substring  	:= SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
            
            INSERT INTO T7571_SYSTEM_REF_PRICE_REQ_MAP(
            C7571_SYSTEM_REF_PRICE_MAP_ID
            ,C207_MAPPED_SYSTEM_ID
            ,C7500_ACCOUNT_PRICE_REQUEST_ID
            ,C7571_ALL_GROUP_FLAG)
			VALUES(
			S7571_SYSTEM_REF_PRICE_MAP_ID.NEXTVAL
			,v_system_id
			,p_requestid
			,v_grp_fl);
                
        END LOOP;
        
        gm_pkg_sm_pricerequest_txn.gm_upd_price_request_sys_map(p_requestid,p_user_id);
    END IF;
    --GET Saved details back to save the details
    SELECT RTRIM(XMLAGG(XMLELEMENT(e,c207_mapped_system_id 
		 || '^' ||c7571_all_group_flag || '^' || '|')).EXTRACT('//text()').getclobval(),',')
		 INTO v_sav_systems
		 FROM T7571_SYSTEM_REF_PRICE_REQ_MAP WHERE c7500_account_price_request_id = p_requestid;
		  	
	P_out_data := v_sav_systems;
END gm_sav_upd_add_systems;

/***********************************************************************
  * Author : Jreddy
  * Description : To insert the additional GPB's for requested PR ID
  ************************************************************************/ 
PROCEDURE gm_sav_upd_gpb_dtls (
	p_requestid         	IN 		T7500_ACCOUNT_PRICE_REQUEST.C7500_ACCOUNT_PRICE_REQUEST_ID%TYPE	
  , p_input_str				IN 		CLOB
  , p_user_id				IN      t7501_account_price_req_dtl.C7501_CREATED_BY%TYPE
)
AS
	 v_string    	CLOB := p_input_str;
     v_strlen    	NUMBER          := NVL (LENGTH (p_input_str), 0) ;
     v_substring 	VARCHAR2 (4000) ;
     v_grp_id  		T7571_SYSTEM_REF_PRICE_REQ_MAP.C207_MAPPED_SYSTEM_ID%TYPE;
        
	BEGIN
		--Delete systems based on request id
		DELETE FROM t7553_gpo_ref_price_req_map WHERE C7500_ACCOUNT_PRICE_REQUEST_ID = p_requestid;	
		
		IF v_strlen                      > 0 THEN    			
        WHILE INSTR (v_string, '|') <> 0
        LOOP
            v_substring  	:= SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
            v_string     	:= SUBSTR (v_string, INSTR (v_string, '|')    + 1) ;
            v_grp_id  		:= NULL;
            v_grp_id    	:= SUBSTR (v_substring, 1, INSTR (v_substring, '|') - 1) ;
            
            INSERT INTO t7553_gpo_ref_price_req_map(
            C7553_GPO_REF_PRICE_REQ_MAP_ID
            ,C7551_GPO_REFERENCE_DETAIL_ID
            ,C7500_ACCOUNT_PRICE_REQUEST_ID)
			VALUES(
			S7553_GPO_REF_PRICE_REQ_MAP_ID.NEXTVAL
			,v_substring
			,p_requestid);
                
        END LOOP;
    END IF;
END gm_sav_upd_gpb_dtls;

/***********************************************************************
  * Author : Karthik
  * Description : Save Pricing Request for Held Orders
  ************************************************************************/   
 PROCEDURE gm_sav_heldorder_price_req (
 	p_selected_orders       IN  CLOB,
 	p_user_id				IN 	T101_USER.C101_USER_ID%TYPE,
 	p_out_req_id  			OUT VARCHAR2
 )
 AS 
	  v_company_id                   T7500_ACCOUNT_PRICE_REQUEST.C1900_COMPANY_ID%TYPE;
	  v_sales_rep_id                 T7500_ACCOUNT_PRICE_REQUEST.C703_SALES_REP_ID%TYPE;
	  v_ad_id                        T7500_ACCOUNT_PRICE_REQUEST.C101_AD_ID%TYPE;
	  v_vp_ad_id                     T7500_ACCOUNT_PRICE_REQUEST.C101_VP_AD%TYPE;
	  v_created_by                   T7500_ACCOUNT_PRICE_REQUEST.C7500_CREATED_BY%TYPE;
	  v_request_id                   T7500_ACCOUNT_PRICE_REQUEST.C7500_ACCOUNT_PRICE_REQUEST_ID%TYPE;
	  v_party_id					 T7500_ACCOUNT_PRICE_REQUEST.C101_GPB_ID%TYPE;
	  v_account_id				 	 T704_ACCOUNT.C704_ACCOUNT_ID%TYPE;
	  v_save_prt_id                  T7500_ACCOUNT_PRICE_REQUEST.C7500_ACCOUNT_PRICE_REQUEST_ID%TYPE;
	  v_gpb_party_id   		         T101_USER.C101_PARTY_ID%TYPE;
	  v_initiated_team 				 VARCHAR2(20);
	  v_access_level				 T101_USER.C101_ACCESS_LEVEL_ID%TYPE;
	  v_order_ids					 VARCHAR2(1000) := p_selected_orders;
	  v_pnums						 CLOB;
	  v_set_id            			 t207_set_master.C207_SET_ID%TYPE;
	  v_out_req_id 					 t7500_account_price_request.c7500_account_price_request_id%TYPE;
	  v_set_id_str					 CLOB;
	  v_group_out					 TYPES.cursor_type;
	  v_string    					 CLOB := p_selected_orders;
      v_strlen    					 NUMBER := NVL (LENGTH (p_selected_orders), 0) ;
      v_substring 					 VARCHAR2 (4000) ;
	  v_unit_currentprice            VARCHAR2 (4000) ;
	  v_gpoprice                     VARCHAR2 (4000) ;
	  v_unit_listprice               v_gm_pricing_list_trip.unitlistprice%TYPE;
	  v_unit_tripwire                v_gm_pricing_list_trip.unittripwire%TYPE;
	  v_proposed_price               T7501_ACCOUNT_PRICE_REQ_DTL.C7501_PROPOSED_PRICE%TYPE DEFAULT NULL;
	  v_status                       T7501_ACCOUNT_PRICE_REQ_DTL.C901_STATUS%TYPE DEFAULT NULL;
	  v_qty                          v_gm_pricing_construct.c7004_qty%TYPE;
	  v_implement_flg                T7501_ACCOUNT_PRICE_REQ_DTL.C7501_IMPLEMENT_FL%TYPE DEFAULT NULL;
	  v_approved_by                  T7501_ACCOUNT_PRICE_REQ_DTL.C7501_APPROVED_BY%TYPE DEFAULT NULL;
	  v_group_id                     v_gm_pricing_construct.c4010_group_id%TYPE;
	  v_grp_set_id                   v_gm_pricing_construct.c207_set_id%TYPE;
	  v_construct_lvl                v_gm_pricing_construct.c901_construct_level%TYPE;
	  v_principal_fl                 v_gm_pricing_construct.c7003_principal_fl%TYPE;
	  v_speclgrpflag			     v_gm_pricing_construct.c4010_specl_group_fl%TYPE;
	  v_pnum						 T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE;
	  v_grouptype				     v_gm_pricing_construct.c901_group_type%TYPE;
	  v_change_type					 T7501_ACCOUNT_PRICE_REQ_DTL.c901_change_type%TYPE DEFAULT NULL;
	  v_change_value				 T7501_ACCOUNT_PRICE_REQ_DTL.c7501_change_value%TYPE DEFAULT NULL;
	  v_constructid					 v_gm_pricing_construct.c7003_system_construct_id%TYPE;
	  v_parent_grp_id				 v_gm_pricing_construct.c4010_parent_group_id%TYPE;
	  v_check_fl 					 T7501_ACCOUNT_PRICE_REQ_DTL.C7501_CHECK_FL%TYPE DEFAULT NULL;
	  v_grptypdesc					 VARCHAR2(50);
	  v_tripwireextensionprice		 T7010_GROUP_PRICE_DETAIL.C7010_PRICE%TYPE;
	  v_grp_set_nm					 v_gm_pricing_construct.c207_set_nm%TYPE;
	  v_construct_nm				 v_gm_pricing_construct.c7003_construct_name%TYPE;
	  v_parent_grp_nm				 v_gm_pricing_construct.c4010_parent_group_nm%TYPE;
	  v_group_nm				     v_gm_pricing_construct.c4010_group_nm%TYPE;
	  v_pdesc						 VARCHAR2(100);
	  v_pricetype					 v_gm_pricing_construct.c901_priced_type%TYPE;
	  v_order_id					 T501_ORDER.C501_ORDER_ID%TYPE;
	  v_price_type					 t4010_group.c901_priced_type%TYPE;
	  v_sys_division				 t207_set_master.c1910_division_id%TYPE;
 		
	  CURSOR v_system_details
		IS 
		  SELECT  PNUM ,
		  		  SETID , 
		  		  GROUPID ,
		  		  PRICEDTYPE, 
		  		  ITEMPRICE, 
		  		  CURRPRICE 
				FROM (
				  	SELECT t4011.c205_part_number_id pnum,t4010.c207_set_id setid,
			        t4010.c4010_group_id groupid , 
			        t4010.c901_priced_type pricedtype, 
			        NVL(NVL(t502.c502_ext_item_price, t502.c502_item_price), 0) ITEMPRICE ,
	    			NVL(get_account_part_pricing (t501.c704_account_id , t502.c205_part_number_id, get_account_gpo_id (t501.c704_account_id)), 0) CURRPRICE
			      FROM t4011_group_detail t4011, 
			        t4010_group t4010,
		            T501_ORDER T501,
		            T502_ITEM_ORDER t502,
		            t207_set_master t207
			      WHERE T501.C501_ORDER_ID  IN (SELECT TOKEN FROM v_in_list)
			      AND T501.C501_ORDER_ID = T502.C501_ORDER_ID
		          AND T502.C205_PART_NUMBER_ID = T4011.C205_PART_NUMBER_ID
		          AND T501.C501_VOID_FL IS NULL
		          AND T502.C502_VOID_FL IS NULL
			      AND t4010.c4010_group_id = t4011.c4010_group_id 
			      AND t4010.c4010_void_fl IS NULL 
			      AND t4010.c901_type = 40045 --Forecast/Demand Group
			      AND t4010.c901_priced_type in (52110, 52111) --Group,Individual 
			      AND t4010.c901_group_type IS NOT NULL 
			      AND t4010.c901_group_type NOT IN (52037,101222)--Instruments and Cases, Demo 
			      AND t4010.c4010_inactive_fl IS NULL
			      AND t207.c207_set_id                 = t4010.c207_set_id
			      AND (t4011.c901_part_pricing_type    = DECODE(NVL(t207.c1910_division_id,'-999'),2005,52080,52080)
                   OR t4011.c901_part_pricing_type     = DECODE(NVL(t207.c1910_division_id,'-999'),2005,52081,52080)) --52080 - primary ,52081 - secondary
			      AND t4010.c4010_publish_fl IS NOT NULL 
			      AND t207.c207_void_fl IS NULL
			      AND t502.c205_part_number_id NOT IN
				    (SELECT c906_rule_id
				    FROM t906_rules
				    WHERE c906_rule_grp_id = 'NONUSAGE'
				    AND c906_void_fl      IS NULL
				    )
			       ORDER BY t501.c501_created_date
		       )
		       WHERE (ITEMPRICE <> CURRPRICE) OR (ITEMPRICE = 0 AND CURRPRICE = 0)
		        OR ((ITEMPRICE < 0 AND CURRPRICE < 0) AND (ITEMPRICE <> CURRPRICE))
		       GROUP BY  PNUM , SETID , GROUPID ,PRICEDTYPE, ITEMPRICE,CURRPRICE;  
 BEGIN
	 my_context.set_my_inlist_ctx (p_selected_orders);
	 
  	SELECT RTRIM (XMLAGG (XMLELEMENT (E, PNUM
	    || ',')) .EXTRACT ('//text()'), ',')
  		|| ','  INTO v_pnums 
		  FROM(
		       SELECT T502.C205_PART_NUMBER_ID PNUM 
				FROM t501_order t501 ,
				  T502_ITEM_ORDER T502
				WHERE T501.C501_ORDER_ID  IN (SELECT TOKEN FROM v_in_list)
				AND T501.C501_ORDER_ID = T502.C501_ORDER_ID
				AND T501.C501_VOID_FL   IS NULL
				AND T502.C502_VOID_FL   IS NULL
			);
		
	     SELECT get_compid_frm_cntx() 
	    	 INTO  v_company_id
	     		FROM DUAL;
				 BEGIN
					SELECT C101_PARTY_ID ,
					  C101_ACCESS_LEVEL_ID
					INTO v_gpb_party_id ,
					  v_access_level
					FROM T101_USER
					WHERE C101_USER_ID = p_user_id;
					EXCEPTION
					WHEN NO_DATA_FOUND THEN
					  v_gpb_party_id := NULL;
		  	      END;
  	    	 
		  	      BEGIN
					SELECT T501.C704_ACCOUNT_ID
					INTO v_account_id
					FROM t501_order t501 ,
					  T502_ITEM_ORDER T502
					WHERE T501.C501_ORDER_ID IN
					  (SELECT TOKEN FROM v_in_list
					  )
					AND T501.C501_ORDER_ID = T502.C501_ORDER_ID
					AND T501.C501_VOID_FL IS NULL
					AND T502.C502_VOID_FL IS NULL
					GROUP BY T501.C704_ACCOUNT_ID;
				 END;
				 
				 BEGIN
					SELECT c101_party_id ,
					  c703_sales_rep_id
					INTO v_party_id ,
					  v_sales_rep_id
					FROM t704_account
					WHERE c704_account_id = v_account_id
					AND C704_VOID_FL     IS NULL;
				END;
				
				BEGIN
					SELECT AD_ID,
					  VP_ID
					INTO v_ad_id,
					  v_vp_ad_id
					FROM V700_TERRITORY_MAPPING_DETAIL
					WHERE AC_ID = v_account_id;
				END;

	       v_created_by := p_user_id;
	       --gm_procedure_log_local('for save data',v_created_by);
	       COMMIT;
	       
		 /* Procedure to save the details in t7500_account_price_request table*/ 
	     gm_pkg_sm_pricerequest_txn.gm_sav_account_price_req(NULL,'52186',v_company_id,NULL,NULL,NULL,NULL,v_sales_rep_id,v_ad_id,v_vp_ad_id,
	     v_created_by,NULL,v_party_id,v_account_id,'903107',v_out_req_id);
	     v_request_id := v_out_req_id;
	     
	     /* Procedure to save the details in t7502_account_price_req_sys table*/
	     FOR v_index IN v_system_details
			LOOP
				  v_set_id 		   := v_index.SETID;
		      gm_pkg_sm_pricerequest_txn.gm_sav_price_req_system(NULL,NULL,v_created_by,v_set_id,v_request_id);
		      v_set_id_str := v_set_id_str || v_set_id || ',';
		END LOOP;
		
		/* Procedure to save the details in t7501_account_price_req_dtl table*/
		gm_pkg_sm_pricerequest_rpt.gm_fch_construct_details(v_company_id,v_account_id,v_set_id_str,'903107',v_group_out);

		LOOP
    		FETCH v_group_out INTO v_grp_set_id, v_grp_set_nm,v_construct_nm,v_construct_lvl,v_group_id,v_parent_grp_id, v_parent_grp_nm,
    		v_principal_fl,v_group_nm,v_qty,v_pnum, v_pdesc, v_unit_listprice, v_unit_tripwire,v_grouptype,v_pricetype,v_speclgrpflag,
    		v_unit_currentprice,v_grptypdesc,v_tripwireextensionprice,v_constructid,v_gpoprice,v_sys_division;
   		 EXIT WHEN v_group_out%notfound;
   		 v_unit_currentprice := trim(NVL(v_gpoprice,v_unit_currentprice));
   		 
    	gm_pkg_sm_pricerequest_txn.gm_sav_group_price_req_dtl(trim(v_unit_currentprice),v_gpoprice,v_unit_listprice,v_unit_tripwire,NULL,NULL,
 							v_qty,NULL,NULL,v_created_by,v_created_by,v_group_id,v_grp_set_id,v_construct_lvl,v_principal_fl,v_speclgrpflag,v_pnum,v_grouptype,
 							NULL,NULL,v_constructid,v_parent_grp_id,NULL,v_request_id);		
    	 END LOOP;
    	 
    	 	IF v_group_out%isopen THEN
	  	 	CLOSE v_group_out;
	  	END IF;
  		
    	 	/*Procedure to Update Proposed Price for the Price request ID */
	       my_context.set_my_inlist_ctx (p_selected_orders);
    	  FOR v_index_price IN v_system_details
			LOOP
				  v_proposed_price  := v_index_price.ITEMPRICE;
				  v_group_id 		:= v_index_price.GROUPID;
				  v_set_id 			:= v_index_price.SETID;
				  v_pnum 			:= v_index_price.PNUM;
				  v_price_type		:= v_index_price.PRICEDTYPE;
	  			gm_pkg_sm_pricerequest_txn.gm_upd_price_req_prop_price(v_request_id,v_proposed_price,v_group_id,v_set_id,v_pnum,v_created_by,v_price_type);
		END LOOP;
    	 
	  	/*Procedure to save the PR and order details in T7503_ACCOUNT_PRICE_REQ_ORDER table*/
	  	BEGIN
		  	v_string := v_string || ',';
	    	IF v_strlen > 0 THEN
	        WHILE INSTR (v_string, ',') <> 0
		        LOOP
		            v_substring  	:= SUBSTR (v_string, 1, INSTR (v_string, ',') - 1) ;
		            v_string     	:= SUBSTR (v_string, INSTR (v_string, ',')    + 1) ;
		            v_order_id  	:= NULL;
		            
		            v_order_id    	:= v_substring;
  					gm_pkg_sm_pricerequest_txn.gm_sav_order_price_req(v_request_id,v_order_id,v_created_by);
		        END LOOP;
	    	END IF;
	    END;
  		
  	
	    p_out_req_id := v_request_id; 

END gm_sav_heldorder_price_req;

/***********************************************************************
  * Author : Karthik
  * Description : Save account price request details
  ************************************************************************/   
 PROCEDURE gm_sav_account_price_req (
 	  P_request_type                 IN T7500_ACCOUNT_PRICE_REQUEST.C901_REQUEST_TYPE%TYPE DEFAULT NULL,
      P_request_status               IN T7500_ACCOUNT_PRICE_REQUEST.C901_REQUEST_STATUS%TYPE DEFAULT NULL,
	  P_company_id                   IN T7500_ACCOUNT_PRICE_REQUEST.C1900_COMPANY_ID%TYPE,
	  P_last_12months_sale           IN T7500_ACCOUNT_PRICE_REQUEST.C7500_LAST_12MONTHS_SALE%TYPE DEFAULT NULL,
	  P_projected_12months_sale      IN T7500_ACCOUNT_PRICE_REQUEST.C7500_PROJECTED_12MONTHS_SALE%TYPE DEFAULT NULL,
	  P_gpo_reference_id             IN T7500_ACCOUNT_PRICE_REQUEST.C7530_GPO_REFERENCE_ID%TYPE DEFAULT NULL,
	  P_gpo_reference_detail_id      IN T7500_ACCOUNT_PRICE_REQUEST.C7531_GPO_REFERENCE_DETAIL_ID%TYPE DEFAULT NULL,
	  P_sales_rep_id                 IN T7500_ACCOUNT_PRICE_REQUEST.C703_SALES_REP_ID%TYPE,
	  P_ad_id                        IN T7500_ACCOUNT_PRICE_REQUEST.C101_AD_ID%TYPE,
	  P_vp_ad_id                     IN T7500_ACCOUNT_PRICE_REQUEST.C101_VP_AD%TYPE,
	  P_created_by                   IN T7500_ACCOUNT_PRICE_REQUEST.C7500_CREATED_BY%TYPE,
	  P_request_id                   IN T7500_ACCOUNT_PRICE_REQUEST.C7500_ACCOUNT_PRICE_REQUEST_ID%TYPE DEFAULT NULL,
	  P_party_id					 IN T7500_ACCOUNT_PRICE_REQUEST.C101_GPB_ID%TYPE,
	  P_account_id				 	 IN T704_ACCOUNT.C704_ACCOUNT_ID%TYPE ,
	  P_type_id		                 IN T901_CODE_LOOKUP.C901_CODE_ID%TYPE DEFAULT NULL,
	  p_out_request_id				 OUT T7500_ACCOUNT_PRICE_REQUEST.C7500_ACCOUNT_PRICE_REQUEST_ID%TYPE
 )
 AS 
 	  v_request_id     T7500_ACCOUNT_PRICE_REQUEST.C7500_ACCOUNT_PRICE_REQUEST_ID%TYPE;
 	  v_currency	   T7500_ACCOUNT_PRICE_REQUEST.C901_CURRENCY%TYPE;
 	  v_curr_value	   T7500_ACCOUNT_PRICE_REQUEST.C901_CUR_VALUE%TYPE;
 	  
 BEGIN
 IF P_company_id <> 1000 THEN
 begin
	SELECT T704.C901_CURRENCY 
	 INTO V_CURRENCY
	 FROM T704_ACCOUNT T704  
	 WHERE T704.C704_ACCOUNT_ID = P_ACCOUNT_ID;
	 EXCEPTION WHEN NO_DATA_FOUND THEN
	 V_CURRENCY := 1;
	 END;
	 --V_CURR_VALUE := GET_CURRENCY_CONVERSION(1, V_CURRENCY ,SYSDATE,1);
	 V_CURR_VALUE := NVL(GET_RULE_VALUE(V_CURRENCY,'PRICECONV'||EXTRACT(YEAR FROM SYSDATE)), 1 );
 ELSE
	 V_CURRENCY := 1;
	 V_CURR_VALUE := 1;
 END IF;
	 
 
			 UPDATE T7500_ACCOUNT_PRICE_REQUEST
			        SET C901_REQUEST_TYPE = P_request_type 
			          , C901_REQUEST_STATUS = P_request_status  -- Initiated
			          , C1900_COMPANY_ID = P_company_id
			          , C7500_LAST_12MONTHS_SALE = P_last_12months_sale
			          , C7500_PROJECTED_12MONTHS_SALE = P_projected_12months_sale
			          , C7530_GPO_REFERENCE_ID = DECODE(P_type_id,'903107',P_party_id,P_account_id)
			          , C7531_GPO_REFERENCE_DETAIL_ID = P_gpo_reference_detail_id
			          , C703_SALES_REP_ID = P_sales_rep_id
			          , C101_AD_ID = P_ad_id
			          , C101_VP_AD = P_vp_ad_id
			          , C7500_LAST_UPDATED_BY = P_created_by
			          , C7500_LAST_UPDATED_DATE = CURRENT_DATE
			          , c101_gpb_id = P_party_id
				      , c704_account_id = DECODE(P_type_id,'903107',P_account_id,NULL)
				      , C901_CURRENCY = v_currency
				      , C901_CUR_VALUE = V_CURR_VALUE
              ,C7500_SUBMITTED_DATE = decode(P_request_status,52124, CURRENT_TIMESTAMP, c7500_submitted_date)
			      WHERE C7500_ACCOUNT_PRICE_REQUEST_ID = P_request_id;

	     IF (SQL%ROWCOUNT = 0) THEN        
	        SELECT 'PR-' || S7500_REQUEST_ID.NEXTVAL INTO v_request_id FROM DUAL;	
	       
	        INSERT INTO T7500_ACCOUNT_PRICE_REQUEST (
	        			C7500_ACCOUNT_PRICE_REQUEST_ID
	        		  , C901_REQUEST_TYPE
	        		  , C901_REQUEST_STATUS
	        		  , C1900_COMPANY_ID
	        		  , C7500_LAST_12MONTHS_SALE
	        		  , C7500_PROJECTED_12MONTHS_SALE
	        		  , C7530_GPO_REFERENCE_ID
	        		  , C7531_GPO_REFERENCE_DETAIL_ID
	        		  , C703_SALES_REP_ID
	        		  , C101_AD_ID
	        		  , C101_VP_AD
	        		  , C7500_INITIATED_BY
	        		  , C7500_INITIATED_DATE
	        		  , C7500_CREATED_BY
	        		  , C7500_CREATED_DATE,c101_gpb_id
				  	  , c704_account_id
				  	  , C901_CURRENCY
				  	  , C901_CUR_VALUE)
	            VALUES(v_request_id
	                 , P_request_type -- Account Request Pricing
	                 , P_request_status -- Initiated
	                 , P_company_id
	                 , P_last_12months_sale
	                 , P_projected_12months_sale
	                 , DECODE(P_type_id,'903107',P_party_id,P_account_id)
	                 , P_gpo_reference_detail_id
	                 , P_sales_rep_id
	                 , P_ad_id
	                 , P_vp_ad_id
	                 , P_created_by
	                 , CURRENT_DATE
	                 , P_created_by
	                 , CURRENT_DATE
	                 , P_party_id
			 		 , DECODE(P_type_id,'903107',P_account_id,NULL)
			 		 ,v_currency
			 		 ,v_curr_value);
	      END IF; 
	        p_out_request_id := NVL(P_request_id,v_request_id);
 END gm_sav_account_price_req;
 
 /***********************************************************************
  * Author : Karthik
  * Description : Save system price request details
  ************************************************************************/   
 PROCEDURE gm_sav_price_req_system (
	       p_price_type             IN t9010_answer_data.C9010_ANSWER_DESC%TYPE DEFAULT NULL,
	       p_price_value            IN t9010_answer_data.C9010_CREATED_BY%TYPE DEFAULT NULL,
	       p_created_by 			IN  T7501_ACCOUNT_PRICE_REQ_DTL.C7501_CREATED_BY%TYPE,
	       p_set_id            		  IN t207_set_master.C207_SET_ID%TYPE,
	       p_account_price_request_id  IN  t9010_answer_data.c9010_ref_id%TYPE
 )
 AS 
 BEGIN
	 	IF p_account_price_request_id IS NOT NULL THEN
		 UPDATE t7502_account_price_req_sys
			   SET c901_change_type = p_price_type
			     , c7502_change_value =  DECODE(P_price_type,'52020',NULL,p_price_value)        
			     , c7502_updated_by = p_created_by
			     , c7502_updated_date = CURRENT_DATE
			 WHERE c7500_account_price_req_id = p_account_price_request_id
			   AND c207_set_id = p_set_id
			   AND c7502_void_fl IS NULL;
		END IF;
	
		  IF(SQL%ROWCOUNT = 0)
				THEN
				INSERT INTO t7502_account_price_req_sys
				           (c7502_account_price_req_sys_id 
				          , c207_set_id 
				          , c7502_change_value
				          , c901_change_type
				          , c7502_created_by 
				          , c7502_created_date 
				          , c7500_account_price_req_id)
                     VALUES(s7502_account_price_req_sys.nextval
                          , p_set_id 
                          , NVL(p_price_value,NULL) 
                          , p_price_type 
                          , p_created_by
                          , CURRENT_DATE 
                          , p_account_price_request_id);					
		  END IF;  
 END gm_sav_price_req_system;
 
  /***********************************************************************
  * Author : Karthik
  * Description : Save group price request details
  ************************************************************************/   
 PROCEDURE gm_sav_group_price_req_dtl (
	       p_current_price                         IN T7501_ACCOUNT_PRICE_REQ_DTL.C7501_CURRENT_PRICE%TYPE,
		   p_gpo_price                             IN T7501_ACCOUNT_PRICE_REQ_DTL.C7501_CURRENT_PRICE%TYPE,
	       p_list_price                            IN T7501_ACCOUNT_PRICE_REQ_DTL.C7501_LIST_PRICE%TYPE,
	       p_tripwire_price                        IN T7501_ACCOUNT_PRICE_REQ_DTL.C7501_TRIPWIRE_PRICE%TYPE,
	       p_proposed_price                        IN T7501_ACCOUNT_PRICE_REQ_DTL.C7501_PROPOSED_PRICE%TYPE DEFAULT NULL,
	       p_status                                IN T7501_ACCOUNT_PRICE_REQ_DTL.C901_STATUS%TYPE DEFAULT NULL,
	       p_quantity                              IN T7501_ACCOUNT_PRICE_REQ_DTL.C7501_QTY%TYPE,      
	       p_implement_flg                         IN T7501_ACCOUNT_PRICE_REQ_DTL.C7501_IMPLEMENT_FL%TYPE DEFAULT NULL,
	       p_approved_by                           IN T7501_ACCOUNT_PRICE_REQ_DTL.C7501_APPROVED_BY%TYPE DEFAULT NULL,
	       p_created_by                            IN T7501_ACCOUNT_PRICE_REQ_DTL.C7501_CREATED_BY%TYPE,
	       p_last_updated_by                       IN T7501_ACCOUNT_PRICE_REQ_DTL.C7501_LAST_UPDATED_BY%TYPE,
	       p_group_id                              IN T7501_ACCOUNT_PRICE_REQ_DTL.C4010_GROUP_ID%TYPE,
	       p_set_id                                IN T7501_ACCOUNT_PRICE_REQ_DTL.C207_SET_ID%TYPE,
	       p_construct_level                       IN T7501_ACCOUNT_PRICE_REQ_DTL.C901_CONSTRUCT_LEVEL%TYPE,
	       p_princlipal_fl                         IN T7501_ACCOUNT_PRICE_REQ_DTL.C7003_PRINCIPAL_FL%TYPE,
	       p_specialitygroup_fl					   IN T7501_ACCOUNT_PRICE_REQ_DTL.C4010_SPECL_GROUP_FL%TYPE,
	       p_partnum							   IN T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE,
	       p_group_type							   IN T7501_ACCOUNT_PRICE_REQ_DTL.C901_GROUP_TYPE%TYPE,
	       p_change_type						   IN T7501_ACCOUNT_PRICE_REQ_DTL.c901_change_type%TYPE DEFAULT NULL,
	       p_change_value						   IN  T7501_ACCOUNT_PRICE_REQ_DTL.c7501_change_value%TYPE DEFAULT NULL,
	       p_construct_id							IN T7501_ACCOUNT_PRICE_REQ_DTL.C7003_SYSTEM_CONSTRUCT_ID%TYPE,
	       p_parentgroupid							IN T7501_ACCOUNT_PRICE_REQ_DTL.C4010_PARENT_GROUP_ID%TYPE,
	       p_check_fl 								IN T7501_ACCOUNT_PRICE_REQ_DTL.C7501_CHECK_FL%TYPE DEFAULT NULL,
	       p_account_price_request_id   			IN  T7500_ACCOUNT_PRICE_REQUEST.C7500_ACCOUNT_PRICE_REQUEST_ID%TYPE
 )
 AS 
 BEGIN
	 	IF p_account_price_request_id IS NOT NULL THEN
	 		UPDATE t7501_account_price_req_dtl
				   SET c7501_current_price = NVL(p_current_price,c7501_current_price)
				     , c7501_list_price = NVL(p_list_price,c7501_list_price)
				     , c7501_proposed_price = p_proposed_price
				     , c7501_tripwire_price = NVL(p_tripwire_price,c7501_tripwire_price)
				     , c7501_qty = NVL(p_quantity,c7501_qty)
				     , c901_status = DECODE(p_proposed_price,null,null,p_status) -- status
				     , c901_construct_level = NVL(p_construct_level,c901_construct_level)
				     , c7501_implement_fl = NVL(p_implement_flg,c7501_implement_fl)
				     , c7501_last_updated_date = SYSDATE
				     , c7501_last_updated_by = p_created_by
				     , c7003_principal_fl = p_princlipal_fl
				     , C4010_SPECL_GROUP_FL = p_specialitygroup_fl
				     , C205_PART_NUMBER_ID = p_partnum
				     , C901_GROUP_TYPE = p_group_type
				     , c901_change_type = p_change_type
				     , c7501_change_value = p_change_value
				     , c7003_system_construct_id = p_construct_id
				     , c4010_parent_group_id = p_parentgroupid
				     , C7501_CHECK_FL = p_check_fl
				 WHERE c7500_account_price_request_id = p_account_price_request_id
				   AND c207_set_id = p_set_id
				   AND c4010_group_id = p_group_id
				   AND c901_construct_level = p_construct_level
				   AND c7003_system_construct_id = p_construct_id
				   AND NVL(c205_part_number_id,'-9999') = NVL(p_partnum,'-9999')
				   AND c7501_void_fl IS NULL ;
			END IF;
			
				IF (SQL%ROWCOUNT = 0) THEN	   
			        INSERT INTO T7501_ACCOUNT_PRICE_REQ_DTL(
			        			C7501_ACCOUNT_PRICE_REQ_DTL_ID
			        		  , C7500_ACCOUNT_PRICE_REQUEST_ID
			        		  , C7501_CURRENT_PRICE
			        		  , C7501_LIST_PRICE
			        		  , C7501_TRIPWIRE_PRICE
			        		  , C7501_PROPOSED_PRICE
			        		  , C901_STATUS
			        		  , C7501_QTY
			        		  , C7501_IMPLEMENT_FL
			        		  , C7501_CREATED_BY
			        		  , C7501_CREATED_DATE
			        		  , C4010_GROUP_ID
			        		  , C207_SET_ID
			        		  , C901_CONSTRUCT_LEVEL
						  	  , C7003_PRINCIPAL_FL
						  	  , C4010_SPECL_GROUP_FL
						  	  , C205_PART_NUMBER_ID
						  	  , C901_GROUP_TYPE
						  	  , C901_CHANGE_TYPE
						  	  , C7501_CHANGE_VALUE
						  	  , C7003_SYSTEM_CONSTRUCT_ID
						  	  , C4010_PARENT_GROUP_ID
						  	  , C7501_CHECK_FL)
			             VALUES(S7501_REQUEST_DTL_ID.NEXTVAL
			                  , p_account_price_request_id
			                  , p_current_price
			                  , p_list_price
			                  , p_tripwire_price
			                  , p_proposed_price
			                  , DECODE(p_proposed_price,null,null,p_status) --status
			                  , p_quantity
			                  , p_implement_flg
			                  , p_created_by
			                  , CURRENT_DATE
			                  , p_group_id
			                  , p_set_id
			                  , p_construct_level
					  		  , p_princlipal_fl
					  		  , p_specialitygroup_fl
					  		  , p_partnum
					  		  , p_group_type
					  		  , p_change_type
					  		  , p_change_value
					  		  , p_construct_id
					  		  , p_parentgroupid
					  		  , p_check_fl);
			     END IF; 
 END gm_sav_group_price_req_dtl;
 
 /***********************************************************************
  * Author : Karthik
  * Description : Save order with price request details
  ************************************************************************/   
 PROCEDURE gm_sav_order_price_req (
 	  p_request_id   		   IN  T7500_ACCOUNT_PRICE_REQUEST.C7500_ACCOUNT_PRICE_REQUEST_ID%TYPE,
      p_order_id               IN T501_ORDER.C501_ORDER_ID%TYPE,
	  p_created_by             IN T7501_ACCOUNT_PRICE_REQ_DTL.C7501_CREATED_BY%TYPE
	  )
	  AS
	  BEGIN
			INSERT INTO T7503_ACCOUNT_PRICE_REQ_ORDER
				  (
				    C7503_ACCOUNT_REQ_ORDER_ID,
				    C7500_ACCOUNT_PRICE_REQ_ID,
				    C501_ORDER_ID,
				    C7503_VOID_FL,
				    C7503_CREATED_BY,
				    C7503_CREATED_DATE,
				    C7503_LAST_UPDATED_BY,
				    C7503_LAST_UPDATED_DATE
				  )
				  VALUES
				  (
				    s7503_account_req_order_id.NEXTVAL,
				    p_request_id,
				    p_order_id,
				    NULL,
				    p_created_by,
				    CURRENT_DATE,
				    p_created_by,
				    CURRENT_DATE
				  );
  END gm_sav_order_price_req;
  
  /***********************************************************************
  * Author 		: Karthik
  * Description : update Proposed price for the PRice Request
  ************************************************************************/   
 PROCEDURE gm_upd_price_req_prop_price (
 		   p_account_price_request_id   		   IN  T7500_ACCOUNT_PRICE_REQUEST.C7500_ACCOUNT_PRICE_REQUEST_ID%TYPE,
	       p_proposed_price                        IN T7501_ACCOUNT_PRICE_REQ_DTL.C7501_PROPOSED_PRICE%TYPE,
	       p_group_id                              IN T7501_ACCOUNT_PRICE_REQ_DTL.C4010_GROUP_ID%TYPE,
	       p_set_id                                IN T7501_ACCOUNT_PRICE_REQ_DTL.C207_SET_ID%TYPE,
	       p_partnum							   IN T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE,
	       p_created_by							   IN T7501_ACCOUNT_PRICE_REQ_DTL.C7501_CREATED_BY%TYPE,
	       p_price_type							   IN  t4010_group.c901_priced_type%TYPE
 )
 AS 
 BEGIN
	 	IF p_account_price_request_id IS NOT NULL THEN
	 		UPDATE t7501_account_price_req_dtl
				   SET  c7501_proposed_price = p_proposed_price
				   	 , c7501_last_updated_date = SYSDATE
				     , c7501_last_updated_by = p_created_by
				 WHERE c7500_account_price_request_id = p_account_price_request_id
				   AND c207_set_id = p_set_id
				   AND c4010_group_id = p_group_id
				   AND NVL(c205_part_number_id,'-9999') = DECODE(p_price_type,'52111',p_partnum,'-9999') --Individual
				   AND c7501_void_fl IS NULL ;
			END IF;
 END gm_upd_price_req_prop_price;

END gm_pkg_sm_pricerequest_txn;

/ 