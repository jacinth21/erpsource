
--@"C:\Database\Packages\Sales\gm_pkg_sm_adj_rpt.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_sm_adj_rpt
IS
--

   /******************************************************************************************************
	* Description : Function to get Adjustment Net Unit Price for the selected Account and Adjuctment Code.
	* author : HReddi
	********************************************************************************************************/
	FUNCTION get_adj_net_unit_price (
		p_account_id   IN		t704_account.c704_account_id%TYPE,
		p_adj_code_id  IN 		t1706_party_price_adj.c901_adj_type%TYPE, 
		p_item_order_id	IN		t502_item_order.c502_item_order_id%TYPE
	)
	RETURN t1706_party_price_adj.c1706_adj_value%TYPE
	IS
		v_party_id t704_account.c101_party_id%TYPE;
		v_net_unit_price   VARCHAR2(50);
		v_adj_price VARCHAR2(10);
		v_unit_price varchar2(10);
		v_unit_price_adj varchar2(10);
		v_order_id t501_order.c501_order_id%TYPE;
		v_part_number t502_item_order.c205_part_number_id%TYPE;
		v_price_cur TYPES.cursor_type;
		
	BEGIN		
		BEGIN
			SELECT c101_party_id 
			  INTO v_party_id
			  FROM t704_account
			 WHERE c704_account_id = p_account_id
			  AND c704_void_fl IS NULL;
		EXCEPTION
			WHEN NO_DATA_FOUND
		THEN
			v_party_id := NULL;
		END;
		
		IF v_party_id IS NOT NULL THEN
		BEGIN
			/* The following code is added for Getting the Wasted and Revision adjustments based on the Account Setup Screen or Based on the Group Setup Svreen.
			 * IF we have no Adjustment for the selected Account, then trying to get the Adjustemtn Details from Group Based Adjustments and vice versa.*/
			SELECT c501_order_id,c205_part_number_id
			  INTO v_order_id, v_part_number
			  FROM t502_item_order
			 WHERE c502_item_order_id = p_item_order_id
			   AND c502_void_fl IS NULL;			   
			
			INSERT INTO my_temp_acc_part_list
			     SELECT t501.c704_account_id, t502.c205_part_number_id
		           FROM t501_order t501, t502_item_order t502
		          WHERE t501.c501_order_id = v_order_id
		            AND t501.c501_order_id = t502.c501_order_id
					AND t501.c501_void_fl IS NULL
           			AND t502.c502_void_fl IS NULL;
           			
           	 SELECT NVL(DECODE(p_adj_code_id,'107970',c1706_waste,'107971',c1706_revision),0)
           	   INTO v_adj_price
        	   FROM v705_part_price_in_acc
		      WHERE c101_party_id = v_party_id               
	            AND c205_part_number_id = v_part_number; 
	            
   		EXCEPTION
			WHEN NO_DATA_FOUND
		THEN
			v_adj_price := 0;
		END;  
		END IF;	 
		-- Get the unit price/ list price based on the pricing parameter selection in account setup screen
		gm_pkg_sm_price_adj_rpt.gm_fch_acc_part_list_price(p_account_id, v_part_number, p_adj_code_id, v_price_cur);
		LOOP
			FETCH v_price_cur
               INTO v_unit_price;
            EXIT
        		WHEN v_price_cur%NOTFOUND;
        END LOOP;   
        v_unit_price := NVL(v_unit_price,0);
        
        SELECT NVL(t502.c502_unit_price_adj_value,0)
	       INTO v_unit_price_adj
	       FROM t502_item_order t502
	      WHERE t502.c502_item_order_id = p_item_order_id
	    	AND t502.c502_void_fl IS NULL;
		 	
        v_net_unit_price := NVL(round((v_unit_price-v_unit_price_adj)-((v_unit_price-v_unit_price_adj)*v_adj_price/100),2),0);
		
		RETURN v_net_unit_price;
	END get_adj_net_unit_price;	
	
	/******************************************************************************************************
	* Description : This function returns price for a part for the specified Account
	* Parameters  : p_acc_id
 	* Parameters  : p_num
	********************************************************************************************************/
	FUNCTION get_account_part_unitprice (
		  p_acc_id   	t704_account.c704_account_id%TYPE
  		, p_num	    	t705_account_pricing.c205_part_number_id%TYPE
  		, p_gpoid    	t705_account_pricing.c101_party_id%TYPE
	)
		RETURN NUMBER
	IS
		v_price 	   t705_account_pricing.c705_unit_price%TYPE;
		v_gpoid t705_account_pricing.c101_party_id%TYPE;
	
BEGIN
		IF p_gpoid IS NULL THEN
			BEGIN
				select t740.c101_party_id
				  INTO v_gpoid
				  from t740_gpo_account_mapping t740 
				 where t740.c704_account_id = p_acc_id;       
			EXCEPTION WHEN NO_DATA_FOUND THEN
				v_gpoid := NULL;
			END;
		END IF;
		
	 SELECT c705_unit_price
	  INTO v_price  
	  FROM
	    (
	    /* 
	     *  Fetch the price details, 
	     *  for the given part and account, if group price and account price exists
	     *  fetch the group price. if the part does not have a group price
	     *  then fetch the account price
	     */
	      SELECT
	        c101_party_id ,
	        c205_part_number_id ,
	        c705_unit_price,
	        row_num
	      FROM
	        (
	          SELECT
	            c101_party_id ,
	            c205_part_number_id ,
	            c705_unit_price ,           
	            /*  
	             *  Following two columns help to decide to keep one of the two rows fetched for the same part
	             *  Example, part number: 124.454  unit price: $200 ( group price )
	             *           part number: 124.454  unit price: $250 ( account price )
	             *  we need to keep the 1st row and remove the second row.
	             */
	            row_number() over ( partition by c101_party_id, c205_part_number_id order by prc_type) row_num ,
	            COUNT(1) OVER ( PARTITION BY c101_party_id, c205_part_number_id) cnt
	          FROM
	            (
	              -- Fetch the account price for the given account(s) and part(s)
	           SELECT
	                t704.c101_party_id ,
	                t705.c205_part_number_id ,
	                t705.c705_unit_price ,                
	                1 prc_type
	              FROM
	                t705_account_pricing t705 ,
	                t704_account t704              
	              WHERE
	                t705.c101_party_id  = t704.c101_party_id
	              and t704.c704_account_id is not null
	              and t704.c704_account_id = p_acc_id
	              AND t705.c205_part_number_id = p_num
	              AND t704.c704_void_fl is null
	              AND t705.c705_void_fl IS NULL
	              union all
	              -- Fetch the group price for the given account(s) and part(s)
	              SELECT
	                t704.c101_party_id ,
	                t705.c205_part_number_id ,
	                t705.c705_unit_price ,
	                2 prc_type
	              FROM
	                t705_account_pricing t705 ,
	                t740_gpo_account_mapping t740,
	                t704_account t704
	              where
	                  t705.c101_party_id  = NVL(p_gpoid, v_gpoid)
	              AND t705.c101_party_id  = t740.c101_party_id
	              and t740.c704_account_id = t704.c704_account_id 
	              AND t704.c704_account_id = NVL(p_acc_id, t704.c704_account_id)
	              AND t705.c205_part_number_id = p_num             
	              AND t705.c705_void_fl IS NULL
	              AND t704.c704_void_fl IS NULL
	              and t740.c740_void_fl is null
	              AND ROWNUM = 1
	            )
	        )
	      WHERE
	        row_num = cnt
	    )
	    unit_prc ,
	    (
	    /* 
	     *  Fetch the part level discount details , 
	     *  for the given part and account, if discount price is set at group level and account level
	     *  fetch the discount at the account level. if the part does not have a account level discount
	     *  then fetch the group level discount
	     */
	      SELECT
	        c101_party_id ,
	        c205_part_number_id ,
	        c901_discount_type ,
	        c7052_discount_offered ,
	        row_num
	      FROM
	        (
	          SELECT
	            c101_party_id ,
	            c205_part_number_id ,
	            c901_discount_type ,
	            c7052_discount_offered ,
	            /*  
	             *  Following two columns help to decide to keep one of the two rows fetched for the same part
	             *  Example, part number: 124.454  discount: $100 ( group level discount )
	             *           part number: 124.454  discount: $250 ( account level discount )
	             *  we need to keep the 2nd row and remove the first row.
	             */
	            row_number() over ( partition BY c101_party_id, c205_part_number_id order by dsc_type DESC) row_num ,
	            COUNT(1) OVER ( PARTITION BY c101_party_id, c205_part_number_id) cnt
	          FROM
	            (
	              --Fetch Discount at the account level for the given account(s) and part(s)
	              SELECT
	                t704.c101_party_id ,
	                t7052.c205_part_number_id ,
	                t7052.c901_discount_type ,
	                t7052.c7052_discount_offered ,
	                1 dsc_type
	              FROM
	                t7052_account_pricing_discount t7052 ,
	                t704_account t704                 
	              WHERE
	                t7052.c101_party_id         = t704.c101_party_id
	              AND t7052.c7052_void_fl      IS NULL
	              AND t704.c704_void_fl        IS NULL
	              and t704.c704_account_id      = p_acc_id
	              AND t7052.c205_part_number_id = p_num 
	              union all
	              --Fetch Discount at the group level for the given account(s) and part(s)
	              SELECT
	                t704.c101_party_id ,
	                t7052.c205_part_number_id ,
	                t7052.c901_discount_type ,
	                t7052.c7052_discount_offered ,
	                2 dsc_type
	              FROM
	                t7052_account_pricing_discount t7052 ,
	                t740_gpo_account_mapping t740 ,
	                t704_account t704 
	              where
	                  t7052.c101_party_id  = NVL(p_gpoid, v_gpoid)
	              and t7052.c101_party_id = t740.c101_party_id
	              and t740.c704_account_id = t704.c704_account_id
	              AND t7052.c7052_void_fl IS NULL
	              and t740.c740_void_fl is null
	              and t704.c704_void_fl is null
	              AND t704.c704_account_id = NVL(p_acc_id, t704.c704_account_id)
	              AND t7052.c205_part_number_id = p_num 
	            )
	        )
	      WHERE
	        row_num = cnt
	    )
	    disc_prc
	  WHERE unit_prc.c101_party_id = disc_prc.c101_party_id(+)
	    AND unit_prc.c205_part_number_id = disc_prc.c205_part_number_id(+);
	
		RETURN v_price;
	--
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN '';
	--
	END get_account_part_unitprice;
	
END gm_pkg_sm_adj_rpt;
/
