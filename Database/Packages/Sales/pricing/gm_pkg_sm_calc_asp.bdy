/** 
FileName : gm_pkg_sm_calc_asp.bdy
Description : 
Author : karthiks 
Date : 12/11/2019
Copyright : Globus Medical Inc 
*/
CREATE OR REPLACE
PACKAGE BODY gm_pkg_sm_calc_asp
IS
	
 /*****************************************************************************************************************
  * Description : Get the Order details based on the input job id and calculate the AVG,Max and min selling price
  * 			  for part for all accounts and part for GPB's
  * Author 		: Karthiks
  ******************************************************************************************************************/   
  PROCEDURE gm_pkg_calc_price_by_part(
  		p_job_id     IN T7590_ASP_JOB_DTL.C7590_ASP_JOB_DTL_ID%TYPE,
  		p_company_id IN T1900_COMPANY.C1900_COMPANY_ID%TYPE
  )
  AS
  BEGIN
		  /*Get the Order details based on the input job id and calculate the AVG,Max and min selling price
	       *for part for all accounts
	       *26240800 := All Accounts
		   */
    		INSERT INTO T7593_PART_SELLING_PRICE (C7593_PART_SELLING_PRICE_ID,C7590_ASP_JOB_DTL_ID,C205_Part_Number_Id,
    		C7593_PART_SELLING_MIN_PRICE,C7593_PART_SELLING_MAX_PRICE,C7593_PART_SELLING_AVG_PRICE,C901_TYPE)
    		SELECT S7593_PART_SELLING_PRICE.NEXTVAL,
    		  p_job_id jbid,
			  price_dtl.pnum,
			  price_dtl.minprc,
			  price_dtl.maxprc,
			  price_dtl.avgprc,
			 '26240800' typ
			FROM
			  (SELECT t7591.c205_part_number_id pnum,
			    MIN(NVL (t7591.c502_item_price, 0)) minprc,
			    MAX(NVL (t7591.c502_item_price, 0)) maxprc,
			    AVG(NVL (t7591.c502_item_price, 0)) avgprc
			  FROM T7591_Asp_Order_Dtl T7591
			  WHERE T7591.c7590_asp_job_dtl_id = p_job_id
			  AND T7591.C7591_Void_Fl         IS NULL
			  GROUP BY t7591.c205_part_number_id
			  ) price_dtl;
		  	
		  /*Get the Order details based on the input job id and calculate the AVG,Max and min selling price
	       *for part for all GPB's available in the ASP_GPB_ID_FOR_CALC Rule group.
	       *26240801 := GPB
		   */
			my_context.set_my_inlist_ctx (get_rule_value_by_company('ASP_GPB', 'ASP_GPB_ID_FOR_CALC',p_company_id));
			
			INSERT INTO T7593_PART_SELLING_PRICE (C7593_PART_SELLING_PRICE_ID,C7590_Asp_Job_Dtl_Id,C205_Part_Number_Id,
			  C7593_PART_SELLING_MIN_PRICE,C7593_PART_SELLING_MAX_PRICE,C7593_PART_SELLING_AVG_PRICE,C901_TYPE,C7593_REF_ID)
				SELECT S7593_PART_SELLING_PRICE.NEXTVAL,
				  p_job_id jbid,
				  price_dtl.pnum,
				  price_dtl.minprc,
				  price_dtl.maxprc,
				  price_dtl.avgprc,
				  '26240801' Typ,
				  price_dtl.gpbid
				FROM
				  (SELECT T7591.C205_Part_Number_Id pnum,
				    MIN(NVL (T7591.C502_Item_Price, 0)) minprc,
				    MAX(NVL (T7591.C502_Item_Price, 0)) maxprc,
				    AVG(NVL (T7591.C502_Item_Price, 0)) avgprc,
				    T7591.C101_Gpb_Id gpbid
				  FROM T7591_Asp_Order_Dtl T7591
				  WHERE T7591.c7590_asp_job_dtl_id = p_job_id
				  AND T7591.C7591_Void_Fl         IS NULL
				  AND T7591.C101_Gpb_Id           IN
				    (SELECT v_list.token FROM v_in_list v_list
				    )
				  GROUP BY T7591.C205_Part_Number_Id,
				    T7591.C101_Gpb_Id
				  ) price_dtl;
    
	END gm_pkg_calc_price_by_part;
	
	
 /*****************************************************************************************************************
  * Description : Get the Order details based on the input job id and calculate the AVG,Max and min selling price
  * 			  for Group for all accounts and Group for GPB's
  * Author 		: Karthiks
  ******************************************************************************************************************/   
  PROCEDURE gm_pkg_calc_price_by_group(
  		p_job_id  IN T7590_ASP_JOB_DTL.C7590_ASP_JOB_DTL_ID%TYPE,
  		p_company_id IN T1900_COMPANY.C1900_COMPANY_ID%TYPE
  )
  AS  
  BEGIN
	       /*Get the Order details based on the input job id and calculate the AVG,Max and min selling price
	        *for Group for all accounts
	        *26240800 := All Accounts
		    */
		     INSERT INTO T7592_GROUP_SELLING_PRICE (C7592_GROUP_SELLING_PRICE_ID,C7590_ASP_JOB_DTL_ID,C4010_GROUP_ID,
				 C7592_GROUP_SELLING_MIN_PRICE,C7592_GROUP_SELLING_MAX_PRICE,C7592_GROUP_SELLING_AVG_PRICE,C901_TYPE)
					 SELECT S7592_GROUP_SELLING_PRICE.NEXTVAL,
					  p_job_id jbid,
					  price_dtl.grpid,
					  price_dtl.minprc,
					  price_dtl.maxprc,
					  price_dtl.avgprc,
					  '26240800' Typ
					FROM
					  (SELECT T4010.C4010_Group_Id grpid,
					    MIN (T7593.C7593_Part_Selling_Min_Price) minprc,
					    MAX (T7593.C7593_Part_Selling_Max_Price) maxprc,
					    AVG (T7593.C7593_Part_Selling_Avg_Price) avgprc
					  FROM t7593_part_selling_price t7593,
					    t4010_group t4010,
					    t4011_group_detail t4011
					  WHERE t4010.c4010_group_id     = t4011.c4010_group_id
					  AND t4010.c901_type = 40045--Forecast/Demand Group
					  AND T4010.C4010_Void_Fl       IS NULL
					  AND t4010.c901_group_type     IS NOT NULL
					  AND t4010.C4010_Inactive_Fl   IS NULL
					  AND t4010.c4010_publish_fl    IS NOT NULL
					  AND T7593.C7590_Asp_Job_Dtl_Id = p_job_id
					  AND T7593.C7593_Void_Fl       IS NULL
					  AND T4011.C205_Part_Number_Id  = T7593.C205_Part_Number_Id
					  GROUP BY T4010.C4010_Group_Id,
					    T4010.C4010_Group_Nm
					  )price_dtl;
		  
	      /*Get the Order details based on the input job id and calculate the AVG,Max and min selling price
	       *for Group for all GPB's available in the ASP_GPB_ID_FOR_CALC Rule group.
	       *26240801 := GPB
		   */  
		     my_context.set_my_inlist_ctx (get_rule_value_by_company('ASP_GPB', 'ASP_GPB_ID_FOR_CALC',p_company_id));
		     
		     INSERT INTO T7592_GROUP_SELLING_PRICE (C7592_GROUP_SELLING_PRICE_ID,C7590_ASP_JOB_DTL_ID,C4010_GROUP_ID,
				 C7592_GROUP_SELLING_MIN_PRICE,C7592_GROUP_SELLING_MAX_PRICE,C7592_GROUP_SELLING_AVG_PRICE,C901_TYPE,C7592_REF_ID)
				 	SELECT S7592_GROUP_SELLING_PRICE.NEXTVAL,
				 	  p_job_id jbid,
					  price_dtl.grpid,
					  price_dtl.minprc,
					  price_dtl.maxprc,
					  Price_Dtl.Avgprc,
					  '26240801' Typ,
					  Price_Dtl.gpbid
					FROM
					  (SELECT T4010.C4010_Group_Id grpid,
					    MIN (T7593.C7593_Part_Selling_Min_Price) minprc,
					    MAX (T7593.C7593_Part_Selling_Max_Price) maxprc,
					    AVG (T7593.C7593_Part_Selling_Avg_Price) Avgprc,
					    T7593.C7593_Ref_Id gpbid
					  FROM T7593_Part_Selling_Price T7593,
					    t4010_group t4010,
					    t4011_group_detail t4011
					  WHERE t4010.c4010_group_id     = t4011.c4010_group_id
					  AND t4010.c901_type = 40045--Forecast/Demand Group
					  AND T4010.C4010_Void_Fl       IS NULL
					  AND t4010.c901_group_type     IS NOT NULL
					  AND t4010.C4010_Inactive_Fl   IS NULL
					  AND t4010.c4010_publish_fl    IS NOT NULL
					  AND T4011.C205_Part_Number_Id  = T7593.C205_Part_Number_Id
					  AND T7593.C7590_Asp_Job_Dtl_Id = p_job_id
					  AND T7593.C7593_Void_Fl       IS NULL
					  AND T7593.C7593_Ref_Id        IN
					    (SELECT V_List.Token FROM V_In_List V_List
					    )
					  GROUP BY T4010.C4010_Group_Id,
					    T4010.C4010_Group_Nm,
					    T7593.C7593_Ref_Id
					  )price_dtl;
     
	END gm_pkg_calc_price_by_group;
	
END gm_pkg_sm_calc_asp;
/ 