/* Formatted on 2017/04/24 12:37 (Formatter Plus v4.8.8) */
--------------------------------------------------------
--  Package used to migrate data into Account Group Pricing Table (T7540)  
-- Its a Temp package for initial load 
--------------------------------------------------------
CREATE OR REPLACE PACKAGE BODY "TEMP_GM_PKG_BATCHUPDATE_T7540"
IS
    vcompany_id    NUMBER;


    PROCEDURE update_pricing_table (
        p_party_id   NUMBER
    )
    IS
    BEGIN
	    BEGIN
        SELECT DISTINCT c1900_company_id
                   INTO vcompany_id
                   FROM t704_account
                  WHERE c101_party_id = p_party_id;
        EXCEPTION WHEN NO_DATA_FOUND THEN
        	SELECT DISTINCT c1900_company_id
                   INTO vcompany_id
                   FROM t101_party
                  WHERE c101_party_id = p_party_id;
        END;

        update_pricing_by_group (p_party_id);
        update_pricing_by_individual (p_party_id);
    END;

    --*********************************************************
    -- To update Grouped priced groups for selected party
    --********************************************************* 
    PROCEDURE update_pricing_by_group (
        p_party_id   NUMBER
    )
    IS
    BEGIN
        MERGE INTO t7540_account_group_pricing t7540
            USING (
                    SELECT   t705.c101_party_id,  t4010.c4010_group_id
                          , MAX (t705.c705_unit_price) current_price, t4010.c901_priced_type
                       FROM (SELECT  t705.c101_party_id,t705.c205_part_number_id,MAX (t705.c705_unit_price) c705_unit_price,t4010.c4010_group_id,t4010.c4010_parent_group_id                           
                       FROM t4011_group_detail t4011
                          , t705_account_pricing t705
                          , t4010_group t4010
                      WHERE t4011.c205_part_number_id = t705.c205_part_number_id
                        AND t4010.c4010_group_id = t4011.c4010_group_id
                        AND t4010.c4010_void_fl IS NULL
                        AND t4010.c901_priced_type  in (52110)   -- priced type = GROUP                        
                        AND t705.c101_party_id = p_party_id
                   GROUP BY t705.c101_party_id,t705.c205_part_number_id,t705.c705_unit_price,t4010.c4010_group_id,c4010_parent_group_id) t705
                          , t4010_group t4010
                      WHERE NVL(t705.c4010_parent_group_id,t705.c4010_group_id)= NVL(t4010.c4010_parent_group_id,t4010.c4010_group_id) 
                        AND t4010.c4010_void_fl IS NULL
                        AND t4010.c901_priced_type  in (52110,52111)   -- priced type = GROUP,INDIVIDUAL
                        AND t705.c101_party_id = p_party_id
                   GROUP BY t4010.c4010_group_id, t705.c101_party_id, t4010.c901_priced_type
                   ) src
            ON (    t7540.c101_party_id = src.c101_party_id
                AND t7540.c4010_group_id = src.c4010_group_id)
            WHEN MATCHED THEN
                UPDATE
                   SET t7540.c901_priced_type = src.c901_priced_type, t7540.c1900_company_id = vcompany_id
                     , t7540.c7540_unit_price = src.current_price, t7540.c7540_last_updated_by   = 30301
          			 , t7540.c7540_last_updated_on   = CURRENT_DATE
            WHEN NOT MATCHED THEN
                INSERT (c7540_account_group_pricing_id, c101_party_id, c4010_group_id, c901_priced_type
                      , c1900_company_id, t7540.c7540_unit_price,t7540.c7540_last_updated_by,t7540.c7540_last_updated_on)
                VALUES (s7540_seq.NEXTVAL, src.c101_party_id, src.c4010_group_id, src.c901_priced_type
                      , vcompany_id, src.current_price, 30301,CURRENT_DATE);
    END;

    --*********************************************************
    --- To Update Individual priced groups for selected party 
    --*********************************************************
    PROCEDURE update_pricing_by_individual (
        p_party_id   NUMBER
    )
    IS
    BEGIN
        MERGE INTO t7540_account_group_pricing t7540
            USING (SELECT t705.c101_party_id, t4010.c4010_group_id, t4011.c205_part_number_id
                        , t705.c705_unit_price current_price, t4010.c901_priced_type FROM(
                   SELECT t705.c101_party_id, t705.c205_part_number_id
                        , t705.c705_unit_price , t4010.c901_priced_type,t4010.c4010_group_id,t4010.c4010_parent_group_id
                     FROM t4011_group_detail t4011, t705_account_pricing t705, t4010_group t4010
                    WHERE t4011.c205_part_number_id = t705.c205_part_number_id
                      AND t4010.c4010_group_id = t4011.c4010_group_id
                      AND t4010.c4010_void_fl IS NULL
                      AND t4010.c901_group_type IS NOT NULL
                      --AND t4010.c901_group_type NOT IN (52037)   -- instruments and cases
                      AND t705.c101_party_id = p_party_id
                      AND t4010.c901_priced_type = 52111 -- priced type = INDIVIDUAL
                      ) t705,  t4010_group t4010, t4011_group_detail t4011                          
                      WHERE NVL(t705.c4010_parent_group_id,t705.c4010_group_id)= NVL(t4010.c4010_parent_group_id,t4010.c4010_group_id) 
                      and t4010.c4010_group_id = t4011.c4010_group_id
                        AND t4011.c205_part_number_id = t705.c205_part_number_id
                        AND t4010.c4010_group_id = t4011.c4010_group_id
                        AND t4010.c4010_void_fl IS NULL
                        AND t4010.c901_priced_type  in (52110,52111)   -- priced type = GROUP,INDIVIDUAL
                        AND t705.c101_party_id = p_party_id 
                        GROUP BY t705.c101_party_id, t4010.c4010_group_id, t4011.c205_part_number_id , t705.c705_unit_price , t4010.c901_priced_type
                        ) src
            ON (    t7540.c101_party_id = src.c101_party_id
                AND t7540.c4010_group_id = src.c4010_group_id
                AND t7540.c205_part_number_id = src.c205_part_number_id)
            WHEN MATCHED THEN
                UPDATE
                   SET t7540.c901_priced_type = src.c901_priced_type, t7540.c1900_company_id = vcompany_id
                   , t7540.c7540_unit_price = src.current_price, t7540.c7540_last_updated_by   = 30301
          			 , t7540.c7540_last_updated_on   = CURRENT_DATE
            WHEN NOT MATCHED THEN
                INSERT (c7540_account_group_pricing_id, c101_party_id,  c4010_group_id, c901_priced_type
                      , c205_part_number_id, c1900_company_id, t7540.c7540_unit_price,t7540.c7540_last_updated_by,t7540.c7540_last_updated_on)
                VALUES (s7540_seq.NEXTVAL, src.c101_party_id, src.c4010_group_id, src.c901_priced_type
                      , src.c205_part_number_id, vcompany_id, src.current_price, 30301,CURRENT_DATE);
    END;
END temp_gm_pkg_batchupdate_t7540;
/