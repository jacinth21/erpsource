/** 
FileName : gm_pkg_sm_asp_txn.bdy
Description : 
Author : karthiks 
Date : 12/11/2019
Copyright : Globus Medical Inc 
*/

CREATE OR REPLACE
PACKAGE BODY gm_pkg_sm_asp_txn
IS
 /*********************************************************************************************************************
  * Description : Master Procedure to lock the data and calculate the Avg/Max and min Price for the Group/Part number
  * Author : Karthiks
  *********************************************************************************************************************/    
PROCEDURE gm_ld_asp_main(
	p_job_date IN DATE DEFAULT NULL
)
AS
v_out_job_id  T7590_ASP_JOB_DTL.C7590_ASP_JOB_DTL_ID%TYPE;
v_out_from_dt T7590_ASP_JOB_DTL.C7590_ASP_JOB_FROM_DT%TYPE;
v_out_to_dt   T7590_ASP_JOB_DTL.C7590_ASP_JOB_TO_DT%TYPE;
v_job_id  T7590_ASP_JOB_DTL.C7590_ASP_JOB_DTL_ID%TYPE;
v_from_dt T7590_ASP_JOB_DTL.C7590_ASP_JOB_FROM_DT%TYPE;
v_to_dt   T7590_ASP_JOB_DTL.C7590_ASP_JOB_TO_DT%TYPE;
v_job_date DATE;
v_company_id T1900_COMPANY.C1900_COMPANY_ID%TYPE;
BEGIN
	-- Get the company id from context
    SELECT NVL(GET_COMPID_FRM_CNTX(),GET_RULE_VALUE('DEFAULT_COMPANY','ONEPORTAL'))
    	INTO v_company_id 
    	FROM DUAL;
    	
	/*If the Input parameter has a date then we need to calcualte the ASP for previous month's from the passing date*/
	v_job_date := NVL(p_job_date,CURRENT_DATE);
	
	/*Get the Job calculation Period and Insert the record in the T7590_ASP_JOB_DTL table with the calculated From and To date*/
	gm_pkg_sm_asp_txn.gm_save_job_detail(v_job_date,v_company_id,v_out_job_id,v_out_from_dt,v_out_to_dt);
	v_job_id  := v_out_job_id;
	v_from_dt := v_out_from_dt;
	v_to_dt   := v_out_to_dt;
	--v_job_id  := 17;
	--v_from_dt := '01-JUN-19';
	--v_to_dt   := '30-NOV-19';
	
	/* Get the Order Transaction detail based on the From and To order date and Loop the data and insert in Lock Table*/
	gm_pkg_sm_asp_txn.gm_sav_asp_txn_dtl (v_job_id,v_company_id,v_from_dt,v_to_dt);
	
	/*Get the Order details based on the input job id and calculate the AVG,Max and min selling price 
	 *for part for all accounts and part for GPB's
	 */
	gm_pkg_sm_calc_asp.gm_pkg_calc_price_by_part(v_job_id,v_company_id);
	
	/*Get the Order details based on the input job id and calculate the AVG,Max and min selling price 
	 *for Group for all accounts and Group for GPB's
	 */
	gm_pkg_sm_calc_asp.gm_pkg_calc_price_by_group(v_job_id,v_company_id);
	
END gm_ld_asp_main;

  /*********************************************************************************************************************
  * Description : Get the Job calculation Period from the following Rule group "ASP_JOB_RUN_PERIOD" 
  *			      and calculate the From and To date
  *	 			  check the From and To date period is available in T7590_ASP_JOB_DTL table 
  *	 			  and if it's available then void the entry in T7590_ASP_JOB_DTL table.
  *	 			  If the record is not available then Insert the record in the T7590_ASP_JOB_DTL table 
  *	 			  with the calculated From and To date
  * Author      : Karthiks
  *********************************************************************************************************************/   
  PROCEDURE gm_save_job_detail(
  		p_job_date IN DATE DEFAULT NULL,
  		p_company_id IN T1900_COMPANY.C1900_COMPANY_ID%TYPE,
 		p_out_job_id OUT T7590_ASP_JOB_DTL.C7590_ASP_JOB_DTL_ID%TYPE,
 		p_out_from_dt OUT T7590_ASP_JOB_DTL.C7590_ASP_JOB_FROM_DT%TYPE,
  		p_out_to_dt OUT T7590_ASP_JOB_DTL.C7590_ASP_JOB_TO_DT%TYPE
  		
  )
  AS
  v_job_run_period T7590_ASP_JOB_DTL.C7590_ASP_JOB_RUN_PERIOD%TYPE;
  v_out_from_dt T7590_ASP_JOB_DTL.C7590_ASP_JOB_FROM_DT%TYPE;
  v_out_to_dt T7590_ASP_JOB_DTL.C7590_ASP_JOB_TO_DT%TYPE;
  v_from_dt T7590_ASP_JOB_DTL.C7590_ASP_JOB_FROM_DT%TYPE;
  v_to_dt T7590_ASP_JOB_DTL.C7590_ASP_JOB_TO_DT%TYPE;
  v_asp_job_id T7590_ASP_JOB_DTL.C7590_ASP_JOB_DTL_ID%TYPE;
  BEGIN
    
	-- Get the Job calculation Period
    SELECT GET_RULE_VALUE_BY_COMPANY ('ASP_JOB','ASP_JOB_RUN_PERIOD', p_company_id) 
    	INTO v_job_run_period  
    	FROM DUAL;
    
    --Calculate From and TO date based on the Job Period
    gm_pkg_sm_asp_txn.gm_calc_From_to_dt(p_job_date,v_job_run_period,v_out_from_dt,v_out_to_dt);
    v_from_dt := To_Date(v_out_from_dt);
    v_to_dt   := To_Date(v_out_to_dt);
    
    --Check the From and To date period is available in T7590_ASP_JOB_DTL table and if it's available then void the entry.
   -- gm_void_asp_job_dtl(v_from_dt,v_to_dt);
    
    --Insert the record in the T7590_ASP_JOB_DTL table with the calculated From and To date
    gm_pkg_sm_asp_txn.gm_sav_asp_job_dtl(v_job_run_period,v_from_dt,v_to_dt,p_company_id,v_asp_job_id);
    p_out_job_id  := v_asp_job_id;
    p_out_from_dt := v_from_dt;
    p_out_to_dt   := v_to_dt;
    
	END gm_save_job_detail;
	
  /*********************************************************************************************************************
  * Description : Calculate From and TO date based on the Job Period
  *     if the p_job_date is coming as CURRENT_DATE(12-DEC-19) and if the P_job_run_period is coming as "6"
  *     then the From date and to date will be like below
  *     p_out_from_dt := '01-JUN-19';
  *	    p_out_to_dt   := '30-NOV-19';
  * Author : Karthiks
  *********************************************************************************************************************/   
  PROCEDURE gm_calc_From_to_dt(
  		p_job_date IN DATE DEFAULT NULL,
 		P_job_run_period IN T7590_ASP_JOB_DTL.C7590_ASP_JOB_RUN_PERIOD%TYPE,
 		p_out_from_dt OUT T7590_ASP_JOB_DTL.C7590_ASP_JOB_FROM_DT%TYPE,
 		p_out_to_dt OUT T7590_ASP_JOB_DTL.C7590_ASP_JOB_TO_DT%TYPE
  )
  AS
  v_out_from_dt T7590_ASP_JOB_DTL.C7590_ASP_JOB_FROM_DT%TYPE;
  v_out_to_dt T7590_ASP_JOB_DTL.C7590_ASP_JOB_TO_DT%TYPE;
  BEGIN
	  
	-- Get the Last date of the Previous month
    SELECT LAST_DAY(ADD_MONTHS(p_job_date,-1))
    	INTO v_out_to_dt
    	FROM dual;
    
	-- Get the First date based on the input job period
    SELECT '01-'
  	||TO_CHAR(last_day(add_months(p_job_date,-(P_job_run_period))),'MON-YY')
  		INTO v_out_from_dt
		FROM dual;
    
	--Set the date value
	p_out_from_dt := v_out_from_dt;
	p_out_to_dt := v_out_to_dt;
	
	END gm_calc_From_to_dt;
	
 /*********************************************************************************************************************
  * Description : Insert the record in the T7590_ASP_JOB_DTL table with the calculated From and To date
  * Author 		: Karthiks
  *********************************************************************************************************************/   
  PROCEDURE gm_sav_asp_job_dtl(
 		P_job_run_period IN T7590_ASP_JOB_DTL.C7590_ASP_JOB_RUN_PERIOD%TYPE,
 		p_from_dt IN T7590_ASP_JOB_DTL.C7590_ASP_JOB_FROM_DT%TYPE,
 		p_to_dt IN T7590_ASP_JOB_DTL.C7590_ASP_JOB_TO_DT%TYPE,
 		p_company_id IN T1900_COMPANY.C1900_COMPANY_ID%TYPE,
 		p_asp_job_id OUT T7590_ASP_JOB_DTL.C7590_ASP_JOB_DTL_ID%TYPE
  )
  AS
   v_asp_job_id T7590_ASP_JOB_DTL.C7590_ASP_JOB_DTL_ID%TYPE;
  BEGIN
		
	    -- Void the job entry if the date month entry already available for the same period
	    UPDATE T7590_ASP_JOB_DTL
		SET C7590_VOID_FL           = 'Y'
		WHERE TRUNC(C7590_Asp_Job_From_Dt) = p_from_dt
		AND TRUNC(C7590_Asp_Job_To_Dt)     = p_to_dt
		AND C1900_COMPANY_ID        = p_company_id
		AND C7590_VOID_FL IS NULL; 
	
    	SELECT S7590_ASP_JOB_DTL.NEXTVAL 
    		INTO v_asp_job_id 
    		FROM DUAL;
    	
    	INSERT
		INTO T7590_ASP_JOB_DTL
		  (
		    C7590_ASP_JOB_DTL_ID,
		    C7590_ASP_JOB_FROM_DT,
		    C7590_ASP_JOB_TO_DT,
		    C7590_ASP_JOB_RUN_PERIOD,
		    C1900_COMPANY_ID,
		    C7590_VOID_FL
		  )
		  VALUES
		  (
		    v_asp_job_id,
		    p_from_dt,
		    p_to_dt,
		    P_job_run_period,
		    p_company_id,
		    NULL
		  );
		  
		 p_asp_job_id := v_asp_job_id;
		 
	END gm_sav_asp_job_dtl;
	
 /********************************************************************************************************************************
  * Description : Get the Order Transaction detail based on the From and To order date and Loop the data and insert in Lock Table
  * Author 		: Karthiks
  *********************************************************************************************************************************/   
  PROCEDURE gm_sav_asp_txn_dtl(
 		p_job_id  	 IN T7590_ASP_JOB_DTL.C7590_ASP_JOB_DTL_ID%TYPE,
 		p_company_id IN T1900_COMPANY.C1900_COMPANY_ID%TYPE,
 		p_from_dt    IN T7590_ASP_JOB_DTL.C7590_ASP_JOB_FROM_DT%TYPE,
		p_to_dt      IN T7590_ASP_JOB_DTL.C7590_ASP_JOB_TO_DT%TYPE
  )
  AS
  CURSOR order_dtl_cur
	IS
	  	SELECT T501.C501_Order_Id Orderid,
		  T501.C704_Account_Id Accountid,
		  T501.C703_Sales_Rep_Id Repid,
		  T501.C501_Ad_Id Ad_Id,
		  T501.C501_Vp_Id VP_Id,
		  T502.C205_Part_Number_Id Partnumber,
		  T502.C502_Item_Qty Qty,
		  T502.C502_Item_Price Price
		FROM T501_Order T501 ,
		  T502_Item_Order T502,
		  T205_Part_Number T205
		WHERE T501.C501_Order_Id     = T502.C501_Order_Id
		AND T502.C205_Part_Number_Id = T205.C205_Part_Number_Id
		AND T501.C501_Void_Fl       IS NULL
		AND T502.C502_Void_Fl       IS NULL
		AND T205.C205_Active_Fl      = 'Y'
		AND T501.C501_Hold_Fl       IS NULL
		AND T502.C502_Item_Price       > 0
		AND (T501.C501_Order_Date >= p_from_dt AND T501.C501_Order_Date <= p_to_dt)
		AND T501.C1900_company_id = p_company_id
		AND (NVL(t501.c901_order_type, -9999) NOT IN
		  (SELECT C906_Rule_Value
		  FROM T906_Rules
		  WHERE C906_Rule_Id   IN ('EXC_PRC_ORD_TYP')
		  AND C906_RULE_GRP_ID IN ('EXC_ASP_ORD_TYP')
		  AND C906_Void_Fl IS NULL
		  ))
		AND T205.C205_Product_Family NOT IN
		  (SELECT C906_Rule_Value
		  FROM T906_Rules
		  WHERE C906_Rule_Id   IN ('EXC_PART_TYP')
		  AND C906_RULE_GRP_ID IN ('EXC_ASP_PART_TYP')
		  AND C906_Void_Fl IS NULL
		  );
		--AND (NVL(t501.c901_order_type, -9999) NOT IN
		--  (102080,2533,2535,2520,101260,2528,2529,2523,2518,2524,102364,2531,2522))
		--AND T205.C205_Product_Family  NOT IN (26240096,26240467,4053,26240315,26240399);
			
  BEGIN
	  FOR order_dtl IN order_dtl_cur
      LOOP
      	 INSERT INTO T7591_Asp_Order_Dtl
			  (
			    C7591_Asp_Order_Dtl_Id,
			    C7590_Asp_Job_Dtl_Id,
			    C501_Order_Id,
				C704_Account_Id,
			    C703_Sales_Rep_Id,
			    C101_Ad_Id,
			    C101_Vp_Ad,
			    C205_Part_Number_Id,
			    C502_Item_Qty,
			    C502_Item_Price,
			    C7591_VOID_FL
			  )
			  VALUES
			  (
			    S7591_Asp_Order_Dtl.Nextval,
			    p_job_id,
			    order_dtl.Orderid,
			    order_dtl.Accountid,
			    order_dtl.Repid,
			    order_dtl.Ad_Id,
			    order_dtl.VP_Id,
			    order_dtl.Partnumber,
			    order_dtl.Qty,
			    order_dtl.Price,
			    NULL
			  );
      END LOOP;
      
    -- Get the GPB ID from the Mapping table and update the GPB in the Lock Table for the accounts
    UPDATE T7591_ASP_ORDER_DTL T7591
	SET T7591.C101_GPB_ID =
	  (SELECT T740.C101_PARTY_ID
	  FROM T740_GPO_ACCOUNT_MAPPING T740
	  WHERE T740.C704_ACCOUNT_ID = T7591.C704_ACCOUNT_ID
	  AND T740.C740_VOID_FL     IS NULL
	  )
	WHERE T7591.C7590_ASP_JOB_DTL_ID = p_job_id
	AND T7591.C7591_VOID_FL IS NULL;

	END gm_sav_asp_txn_dtl;
	
END gm_pkg_sm_asp_txn;
/ 