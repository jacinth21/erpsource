/**
@ PRT-288 - Load Group Pricing Information
@ Purpose: Package called by JMS to update current price T7540_Account_group_pricing table
@ Author: Matt B
*/

CREATE OR REPLACE
PACKAGE BODY gm_pkg_PRT_currentPrice_txn
IS
  /** ---------------------------------------------------------- 
  ----Main procedure that calls sub procedures based on batch type 
   ----------------------------------------------------------------**/
PROCEDURE gm_prt_currentPrice_main(
    p_BatchId   IN T9600_BATCH.C9600_BATCH_ID %TYPE
    ,p_BatchType IN T9600_BATCH.C901_BATCH_TYPE%TYPE
    ,p_Out_Refid OUT T9601_BATCH_DETAILS.C9601_REF_ID%TYPE)
AS
  vCompany_id T704_Account.C1900_COMPANY_ID%TYPE;
BEGIN
  /* query to retrieve ref_id from batch details table*/
      SELECT t9601.C9601_REF_ID
      INTO p_Out_Refid
      FROM T9601_BATCH_DETAILS T9601 ,
        T9600_BATCH T9600
      WHERE T9601.C9600_BATCH_ID = T9600.C9600_BATCH_ID
      AND T9600.C9600_BATCH_ID   = p_BatchId;
      
      IF (p_BatchType            = 18753) THEN
            SELECT DISTINCT c1900_company_id
            INTO vCompany_id
            FROM T101_party
            WHERE c101_party_id = p_Out_Refid;
            gm_prt_batchupdate(p_Out_Refid,vCompany_id);
      ELSE
            SELECT c1900_company_id
            INTO vCompany_id
            FROM t7500_account_price_request
            WHERE c7500_account_price_request_id = p_Out_Refid;
            gm_prt_pricingtoolUpdate(p_Out_Refid,vCompany_id);
      END IF;
END gm_prt_currentPrice_main;


	/** ---------------------------------------------------------- 
	----- Procedure called when new price added from pricing->Batch update 
	---------------------------------------------------------------**/
PROCEDURE gm_prt_batchupdate(
    p_Party_id  IN T9601_BATCH_DETAILS.C9601_REF_ID%TYPE ,
    vCompany_id IN T704_Account.C1900_COMPANY_ID%TYPE )
AS
  CURSOR batch_update_prices_cursor
  IS
    SELECT t705.c705_account_pricing_id,
      t705.c101_party_id Party_id,
      t4010.c207_set_id,
      T4010.C4010_Group_id,
      MAX(t705.c705_unit_price) current_price ,
      NULL part_number,
      t4010.c901_priced_type,
      NVL( t705.c705_last_updated_by,T705.C705_Created_BY) Updated_by
    FROM t705_account_pricing T705,
      t4010_group T4010,
      t4011_group_detail T4011
    WHERE t4011.c205_part_number_id   = t705.c205_part_number_id
    AND t4011.c4011_primary_part_lock_fl is not null  --only primary parts
    AND t4010.c4010_group_id          = t4011.c4010_group_id
    AND t4010.c207_set_id            IS NOT NULL
    AND t4010.c4010_void_fl IS NULL                        
    AND t705.c705_void_fl IS NULL
    AND t705.c101_party_id            = p_Party_id
    AND T705. c705_price_exported_fl IS NULL
    AND t4010.c901_priced_type        = 52110 -- pricetype = GROUP
    GROUP BY t4010.C4010_Group_id,
      t4010.c207_set_id,
      t705.c101_party_id,
      t4010.c901_priced_type,
      NVL( t705.c705_last_updated_by,T705.C705_Created_BY),
      t705.c705_account_pricing_id
  UNION ALL
  SELECT t705.c705_account_pricing_id,
    t705.c101_party_id,
    t4010.c207_set_id,
    T4010.C4010_Group_id,
    t705.c705_unit_price current_price ,
    t705.c205_part_number_id part_number,
    t4010.c901_priced_type,
    NVL( t705.c705_last_updated_by,T705.C705_Created_BY) Updated_by
  FROM t705_account_pricing T705,
    t4010_group T4010,
    t4011_group_detail T4011
  WHERE t4011.c205_part_number_id  = t705.c205_part_number_id
  AND t4010.c4010_group_id         = t4011.c4010_group_id
  AND t4010.c207_set_id           IS NOT NULL
  AND t4010.c4010_void_fl IS NULL                        
  AND t705.c705_void_fl IS NULL
  AND t705.c101_party_id           = p_Party_id
  AND T705.c705_price_exported_fl IS NULL
  AND t4010.c901_priced_type       = 52111; -- price type = Individual
BEGIN
      FOR batch_update_prices_val IN batch_update_prices_cursor
      LOOP
              gm_prt_UpdateData(batch_update_prices_val.Party_Id ,batch_update_prices_val.C207_Set_id ,batch_update_prices_val.C4010_group_id ,batch_update_prices_val.current_price ,batch_update_prices_val.part_number ,vCompany_id ,batch_update_prices_val.Updated_by);
      END LOOP;
  -- reset c705_price_exported_fl in T705 to 'Y'
  UPDATE t705_account_pricing
  SET c705_price_exported_fl  = 'Y'
  WHERE c101_party_id         = p_party_id
  AND c705_price_exported_fl IS NULL
  AND c705_void_fl IS NULL;
END gm_prt_batchupdate;


	/** ---------------------------------------------------------------------------
 	 ---	Procedure called when new price changes via pricing tool implement price 
 	 ------------------------------------------------------------------------------**/
PROCEDURE gm_prt_pricingtoolUpdate(
    p_priceRequest_id IN T9601_BATCH_DETAILS.C9601_REF_ID%TYPE ,
    vCompany_id       IN T704_Account.C1900_COMPANY_ID%TYPE )
AS
  CURSOR pricingTool_update_cursor
  IS
    SELECT T7500.C101_GPB_ID Party_Id,
      T7501.C207_Set_id,
      t4010.c4010_group_id ,
      t7501.c205_part_number_id part_number ,
      t7501.c7501_current_price current_price,
      T7500.c1900_company_id ,
      NVL(t7501.c7501_last_updated_by, t7501.c7501_created_by) Updated_by
    FROM t7500_account_price_request T7500,
      t7501_account_price_req_dtl T7501,
      t4010_group t4010
    WHERE t7501.c7500_account_price_request_id = t7500.c7500_account_price_request_id
    AND t7501.c7501_implement_fl               = 'Y'
    AND t7501.c7501_void_fl                   IS NULL
    AND t7500.c7500_account_price_request_id   = p_priceRequest_id
    --added to fetch all the groups which related to parent and update price
    AND NVL(t7501.c4010_parent_group_id,t7501.c4010_group_id)= NVL(t4010.c4010_parent_group_id,t4010.c4010_group_id) 
    AND t4010.c4010_void_fl                                 IS NULL
    AND t7500.c7500_void_fl  								IS NULL
    AND t7501.c7501_void_fl 								IS NULL
	AND t4010.c901_type                                      = 40045--Forecast/Demand Group
	AND t4010.c901_group_type                               IS NOT NULL
	AND t4010.c4010_inactive_fl                             IS NULL
	AND t4010.c4010_publish_fl                              IS NOT NULL;
BEGIN
        FOR pricingTool_update_val IN pricingTool_update_cursor
        LOOP
                gm_prt_UpdateData(pricingTool_update_val.Party_Id ,pricingTool_update_val.C207_Set_id ,pricingTool_update_val.C4010_group_id ,pricingTool_update_val.current_price ,pricingTool_update_val.part_number ,vCompany_id ,pricingTool_update_val.Updated_by);
        END LOOP;
        
        gm_prt_Part_Price_update(p_priceRequest_id);
        
END gm_prt_pricingtoolUpdate;


/** ---------------------------------------------------------
  ----------Common procedure that is used to update t7540 table 
 ----------------------------------------------------------------*/
PROCEDURE gm_prt_UpdateData(
    vPartyId       IN t7540_account_group_pricing.C101_Party_id%TYPE ,
    vSetId         IN t7540_account_group_pricing.C207_SET_ID%TYPE ,
    vGroupId       IN t7540_account_group_pricing.C4010_Group_ID%TYPE ,
    vCurrentPrice  IN t7540_account_group_pricing.C7540_UNIT_PRICE%TYPE ,
    vPartNumber    IN t7540_account_group_pricing.C205_PART_NUMBER_ID%TYPE ,
    vCompanyId     IN t7540_account_group_pricing.C1900_COMPANY_ID%TYPE ,
    vLastUpdatedBy IN t7540_account_group_pricing.C7540_Last_Updated_by%TYPE)
AS
BEGIN
        UPDATE t7540_account_group_pricing
        SET C7540_UNIT_PRICE      = vCurrentPrice ,
          C7540_Last_Updated_by   = vLastUpdatedBy ,
          C7540_Last_updated_on   = CURRENT_DATE
        WHERE C101_Party_id       = vPartyId
        --AND c207_set_id           =vSetId
        AND C4010_GROUP_ID        = vGroupId
        AND c7540_void_fl IS NULL
        AND NVL(c205_part_number_id,'-9999') = NVL(vPartNumber,'-9999');
        
        IF(SQL%ROWCOUNT           = 0) THEN
                INSERT
                INTO t7540_account_group_pricing
                  (
                    C7540_ACCOUNT_GROUP_PRICING_ID ,
                    C101_Party_id ,
                   -- C207_SET_ID ,
                   	c205_part_number_id,
                    C4010_GROUP_ID ,
                    C1900_COMPANY_ID ,
                    C7540_UNIT_PRICE ,
                    C7540_Last_Updated_BY ,
                    C7540_Last_Updated_On
                  )
                  VALUES
                  (
                    S7540_seq.nextval ,
                    vPartyId ,
                   -- vSetId ,
                    vPartNumber,
                    vGroupId ,
                    vCompanyId ,
                    vCurrentPrice ,
                    vLastUpdatedBy ,
                    CURRENT_DATE
            );
        END IF;
END gm_prt_UpdateData;

/** ---------------------------------------------------------
  ----------Procedure that is used to update t705 table 
 ----------------------------------------------------------------*/
PROCEDURE gm_prt_Part_Price_update(
  vRequest_id IN  t7500_account_price_request.c7500_account_price_request_id%TYPE
)
AS
  vParty_Id t7500_account_price_request.c101_gpb_id%TYPE;
  vproj_id t705_account_pricing.c202_project_id%TYPE;
  
     CURSOR part_price_update_cursor
     IS
      SELECT t4011.C205_part_number_id partnumber
        ,T7501.C7501_current_price currentPrice        
        ,T7501.C7501_approved_by approvedBy
        ,T7501.c7501_account_price_req_dtl_id prtreqdetailid
      FROM T7501_account_price_req_dtl T7501
        ,T4011_group_detail T4011
        ,T4010_group T4010
      WHERE T4010.c4010_group_id                 = T4011.C4010_group_id
        --AND C4011_primary_part_lock_fl           = 'Y'
        AND T7501.C7501_void_fl IS NULL
        AND t7501.c7501_implement_fl  			 =  'Y'
        AND T7501.C7500_Account_price_Request_id = vRequest_id
        --added to fetch all the groups which related to parent and update price
	    AND NVL(t7501.c4010_parent_group_id,t7501.c4010_group_id)= NVL(t4010.c4010_parent_group_id,t4010.c4010_group_id) 
	    AND NVL(t7501.c205_part_number_id,t4011.c205_part_number_id) = t4011.c205_part_number_id
	    AND t4010.c4010_void_fl                                 IS NULL
		AND t4010.c901_type                                      = 40045--Forecast/Demand Group
		AND t4010.c901_group_type                               IS NOT NULL
		AND t4010.c4010_inactive_fl                             IS NULL
		AND t4010.c4010_publish_fl                              IS NOT NULL
		GROUP BY t4011.C205_part_number_id ,T7501.C7501_current_price ,T7501.C7501_approved_by,T7501.c7501_account_price_req_dtl_id;
        
  
BEGIN
    SELECT C101_GPB_ID INTO vParty_Id  FROM T7500_account_price_request  WHERE C7500_account_price_request_id = vRequest_id;
    
    FOR part_price_update_val IN part_price_update_cursor
    LOOP
    
        UPDATE T705_ACCOUNT_PRICING
        SET C705_unit_price = part_price_update_val.currentPrice
        ,c7501_account_price_req_dtl_id = part_price_update_val.prtreqdetailid
        ,C705_last_updated_by = part_price_update_val.approvedBy
        ,C705_last_updated_date = Current_Date
        WHERE C101_party_id = vParty_Id
        AND C205_part_number_id = part_price_update_val.partnumber
        AND C705_void_fl IS  NULL;
        
           IF(SQL%ROWCOUNT = 0) THEN
           
             INSERT INTO T705_ACCOUNT_PRICING
              (C705_account_pricing_id 
              ,C101_party_id
              ,C205_part_number_id
              ,C705_unit_price
               ,C202_project_id
               ,C705_created_date
               ,C705_created_by
               ,C705_PRICE_EXPORTED_FL
               ,C705_LAST_UPDATED_DATE
               ,C705_LAST_UPDATED_BY
               ,c7501_account_price_req_dtl_id
              )
           VALUES (s704_price_account.NEXTVAL, vParty_Id, part_price_update_val.partnumber, part_price_update_val.currentPrice
               ,  get_project_id_from_part (part_price_update_val.partnumber), CURRENT_DATE, part_price_update_val.approvedBy
               ,'Y', CURRENT_DATE, part_price_update_val.approvedBy,part_price_update_val.prtreqdetailid
              );
           
           END IF;
    
     END LOOP;

END gm_prt_Part_Price_update;


/** ----------------------------------------------------------------------------------------
	Procedure to save the log from t705_account_pricing trigger( trg_acct_price_upd.trg ) 
------------------------------------------------------------------------------------------**/
PROCEDURE gm_sav_account_pricing_log(
	p_acc_pricing_id IN t705_account_pricing.c705_account_pricing_id%TYPE,
	p_curr_price IN  t705_account_pricing.c705_unit_price%TYPE,
	p_created_by IN t706_account_pricing_log.c706_created_by%TYPE,
	p_account_price_req_dtl_id IN t7501_account_price_req_dtl.c7501_account_price_req_dtl_id%TYPE,
    p_company_id       IN t704_account.c1900_company_id%TYPE)
AS
BEGIN
 INSERT INTO t706_account_pricing_log
					(c706_account_pricing_log_id, c705_account_pricing_id, c705_unit_price, c706_created_by,c706_created_date,
           c7501_account_price_req_dtl_id,c1900_company_id			    
					)
			 VALUES (s706_account_pricing_log.nextval, p_acc_pricing_id, p_curr_price, p_created_by, systimestamp,
                p_account_price_req_dtl_id,p_company_id				  
					);
END gm_sav_account_pricing_log;
END gm_pkg_PRT_currentPrice_txn;
/