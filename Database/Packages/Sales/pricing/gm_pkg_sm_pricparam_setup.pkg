--@"C:\pricing\db\Packages\Sales\pricing\gm_pkg_sm_pricparam_setup.pkg

CREATE OR REPLACE PACKAGE gm_pkg_sm_pricparam_setup
IS
/***********************************************************************************
* Description    : This procedure is called to SAVE PRICE ADJUSTMENT CODE VALUES 
* Parameters     : p_accid,p_party_id,p_prcAdjInpStr p_accInputStr, p_userid
*************************************************************************************/

	PROCEDURE gm_save_pricing_params (
        p_accid         IN t704_account.c704_account_id%TYPE,
        p_party_id      IN t704_account.c101_party_id%TYPE,
        p_prcAdjInpStr  IN VARCHAR2,
        p_gpo_id		IN VARCHAR2,
        p_userid        IN t704a_account_attribute.c704a_created_by%TYPE
    );
        
        
 /***********************************************************************************
* Description     : This procedure is called to Fetch Price Adjustment code values 
* Parameters      : p_accid,p_party_id
*************************************************************************************/

PROCEDURE gm_fch_pricing_params (
        p_accid       	 IN t704_account.c704_account_id%TYPE,
        p_party_id       IN VARCHAR2,
        p_out_cursor OUT TYPES.cursor_type);
        
        
  /*******************************************************
   * Description : Procedure to fetch required date history Info
   * Author 	 : Audit Trail ID and Audit Transaction id
   *******************************************************/
--
	PROCEDURE gm_fch_priceparam_history (
		p_ref_id	   IN		t941_audit_trail_log.c941_ref_id%TYPE
	  , p_audit_id	   IN		t940_audit_trail.c940_audit_trail_id%TYPE
	  , p_reqhistory   OUT		TYPES.cursor_type
	);
	
	/*******************************************************
   * Description : Procedure to fetch required date history Info
   * parameters 	 : Audit Trail ID and Audit Transaction id
   *******************************************************/
	PROCEDURE gm_sav_priceparam_history (
		p_ref_id	   		IN		t941_audit_trail_log.c941_ref_id%TYPE
	  , p_user_id	   		IN		t941_audit_trail_log.C941_UPDATED_BY%TYPE
	  , p_aatr_type	   		IN		VARCHAR2
	  , p_aatr_value   		IN		VARCHAR2
	  , p_aatr_price   		IN 		VARCHAR2
	  , p_old_aatr_value   	IN		VARCHAR2
	  , p_old_aatr_price   	IN 		VARCHAR2
	);
	
   /*********************************************************************
   * Description : Procedure to save the Group Account Mapping details
   * Author  	 : HReddi
   *********************************************************************/
	PROCEDURE gm_sav_grp_account_mapping (
		p_acc_id	 IN t704_account.c704_account_id%TYPE
	  , p_party_id	 IN t704_account.c101_party_id%TYPE
	  , p_gpo_id	 IN VARCHAR2
	  , p_user_id	 IN t740_gpo_account_mapping.C740_CREATED_BY %TYPE
	);
	
	
   /************************************************************************************************
   * Description : Procedure to split values for gm_sav_acct_affln procedure 
   * Author  	 : Mahavishnu
   ************************************************************************************************/
	PROCEDURE gm_sav_acct_affln_dtl(
	    p_acc_id    IN t704d_account_affln.C704_ACCOUNT_ID%TYPE ,
	    p_affln_str IN VARCHAR2 ,
	    p_userid    IN t704a_account_attribute.C704A_CREATED_BY%TYPE 
    );
	
	  /***********************************************************************************************
   * Description : Procedure to save the Account Affiliation Detail in t704d_account_affln table
   * Author  	 : Mahavishnu
   ***************************************************************************************************/
	PROCEDURE gm_sav_acct_affln(
	    p_accid        IN t704d_account_affln.C704_ACCOUNT_ID%TYPE,
	    p_gpo          IN t704d_account_affln.C101_GPO%TYPE,
	    p_idn          IN t704d_account_affln.C101_IDN%TYPE,
	    p_contract     IN t704d_account_affln.C901_CONTRACT%TYPE,
	  --p_admin_fee_fl IN t704d_account_affln.C901_ADMIN_FLAG%TYPE,
	  --p_admin_fee    IN t704d_account_affln.C704D_ADMIN_FEE%TYPE,
	    p_rpc          IN  t704d_account_affln.C101_RPC%TYPE,
	    p_hlthsystem   IN t704d_account_affln.c101_health_system%TYPE,
	    p_userid       IN t704a_account_attribute.C704A_CREATED_BY%TYPE,
	    p_gln		   IN t704d_account_affln.C704D_GLN%TYPE,
	    p_gpoid		   IN t704d_account_affln.C704D_GPOID%TYPE
    );
	
	
   /********************************************************************
    * Description : function to get Account Party ID for the Account ID
   *********************************************************************/
   FUNCTION get_acc_partyid (
      p_account_id    IN   t704_account.c704_account_id%TYPE
   )
   RETURN VARCHAR2;

	/*
	* Description : This procedure used to log the Account map details.
	* 				While change the Account GPB (Account setup - Pricing parameter) 
	* 				, at the time to track the Party and time information and stored to log table (T704a)
	*
	* Author: mmuthusamy
	* 
	*/
   
   PROCEDURE gm_sav_gpo_account_map_log (
	p_party_id IN t740a_gpo_account_map_log.c101_party_id%TYPE,
	p_account_id IN t740a_gpo_account_map_log.c704_account_id%TYPE,
	p_from_date IN t740a_gpo_account_map_log.c740a_from_dt%TYPE,
	p_to_date IN t740a_gpo_account_map_log.c740a_to_dt%TYPE,
	p_user_id IN t740a_gpo_account_map_log.c740a_last_updated_by%TYPE
	);
	
	/***********************************************************************************************
   * Description : Procedure Used to Release/unrelease System in Pricing TOOl
   * Author  	 : mselvamani
   ***************************************************************************************************/
	PROCEDURE gm_prt_add_release_system(
	p_sysid  IN t2080_set_company_mapping.c207_set_id%TYPE,
	p_segid  IN t207_set_master.c901_pricing_segment_id%TYPE,
	p_releasefl IN t2080_set_company_mapping.c2080_pricing_release_fl%TYPE,
	p_userid IN t2080_set_company_mapping.c2080_last_updated_by%TYPE
	);
	
	/***********************************************************************************************
   * Description : Procedure Used to get the Segment values
   * Author  	 : mselvamani
   ***************************************************************************************************/
	PROCEDURE gm_fch_segment_names(
	p_seg_out OUT TYPES.cursor_type
	);
   
END gm_pkg_sm_pricparam_setup;
/
