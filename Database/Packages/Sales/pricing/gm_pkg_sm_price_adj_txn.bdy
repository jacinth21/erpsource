/* Formatted on 2011/10/28 18:05 (Formatter Plus v4.8.0) */
-- @"C:\Database\Packages\Sales\gm_pkg_sm_price_adj_txn.bdy" ;

CREATE OR REPLACE PACKAGE BODY gm_pkg_sm_price_adj_txn
IS
	

	/******************************************************************************************
	* Description : Procedure is to save the details of Part Price Adjustment Initiate details
	* author : arajan
	*********************************************************************************************/
	PROCEDURE gm_sav_part_price_adj (
		p_party_id		IN		 	t7020_part_price_adj_request.c101_party_id%TYPE
	  , p_input_str		IN			CLOB
	  , p_rem_str		IN			CLOB
	  , p_user_id		IN			t7020_part_price_adj_request.c7020_initiated_by%TYPE
	  , p_txn			IN OUT		t7020_part_price_adj_request.c7020_part_price_adj_req_id%TYPE
	  , p_account_id   	IN			t704_account.c704_account_id%TYPE
	  , p_gpo_id	   	IN			t705_account_pricing.c101_party_id%TYPE
	  , p_comments      IN          VARCHAR2
	)
	AS
		v_status 		t7020_part_price_adj_request.c901_request_status_cd%TYPE;
		
	BEGIN
		
		gm_pkg_sm_price_adj_txn.gm_validate_part_price_details(p_txn); -- To validate the transaction
		
		-- to save the details
		gm_sav_wcopy_price_adj_dtls(p_party_id
										, p_input_str
										, p_rem_str
										, p_user_id
										, p_txn
										, p_account_id
										, p_gpo_id
										, p_comments);
		
	END gm_sav_part_price_adj;
	
	/***********************************************************************************************
	* Description : Procedure to save the part number details of Part Price Adjustment Initiate
	* author : arajan
	***************************************************************************************************/
	PROCEDURE gm_sav_wcopy_price_adj_dtls (
		p_party_id		IN		 	t7020_part_price_adj_request.c101_party_id%TYPE
	  , p_input_str		IN			CLOB
	  , p_rem_str		IN			CLOB
	  , p_user_id		IN			t7020_part_price_adj_request.c7020_initiated_by%TYPE
	  , p_txn			IN OUT		t7020_part_price_adj_request.c7020_part_price_adj_req_id%TYPE
	  , p_account_id   	IN			t704_account.c704_account_id%TYPE
	  , p_gpo_id	   	IN			t705_account_pricing.c101_party_id%TYPE
	  , p_comments      IN          VARCHAR2
	)
	AS
		v_old_txn_date	t7020_part_price_adj_request.c7020_last_updated_date%TYPE;
		v_txn			t7020_part_price_adj_request.c7020_part_price_adj_req_id%TYPE;
	BEGIN
			
			-- Get the transaction date before submitting the record
		BEGIN
			SELECT TRUNC(NVL(c7020_last_updated_date, c7020_initiated_date))
				INTO v_old_txn_date
			FROM t7020_part_price_adj_request 
			WHERE c7020_part_price_adj_req_id = p_txn
			AND c7020_void_fl IS NULL;
			EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				v_old_txn_date := CURRENT_DATE;
		END;
		
		gm_sav_part_price_master_t7020( p_party_id
										  , '105285' -- Initiated
										  , p_user_id
										  , p_txn
										  , p_comments);
		v_txn := p_txn;
		
		gm_sav_part_price_adj_dtls(p_party_id
									  , p_input_str
									  , p_rem_str
									  , p_user_id
									  , v_txn
									  , p_account_id
									  , p_gpo_id
									  , v_old_txn_date
									 );

    END gm_sav_wcopy_price_adj_dtls;
	
	/***********************************************************************************************
	* Description : Procedure to save the part number details of Part Price Adjustment Initiate
	* author : arajan
	***************************************************************************************************/
	PROCEDURE gm_validate_part_price_details (
		p_txn		IN	t7020_part_price_adj_request.c7020_part_price_adj_req_id%TYPE
	)
	AS
		v_void_fl		t7020_part_price_adj_request.c7020_void_fl%TYPE;
		v_old_status	t7020_part_price_adj_request.c901_request_status_cd%TYPE;
	BEGIN
		BEGIN
			SELECT C7020_VOID_FL
					, C901_REQUEST_STATUS_CD 
				INTO v_void_fl
					, v_old_status
			FROM T7020_PART_PRICE_ADJ_REQUEST 
			WHERE C7020_PART_PRICE_ADJ_REQ_ID = p_txn
			FOR UPDATE;
			EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				v_old_status := NULL;
				v_void_fl	 := NULL;
		END;
		
		IF p_txn IS NOT NULL AND v_void_fl = 'Y'
		THEN
		    GM_RAISE_APPLICATION_ERROR('-20999','357','');
			
		END IF;
		
		IF p_txn IS NOT NULL AND v_old_status != 105285 -- Initiated
		THEN
			raise_application_error('-20669','');
		END IF;

    END gm_validate_part_price_details;
    
    /**********************************************************************
	* Description : Procedure to submit the PADJ transaction for approval 
	* author : arajan
	************************************************************************/
    PROCEDURE gm_submit_part_price_adj ( 
	      p_party_id	IN		 	t7020_part_price_adj_request.c101_party_id%TYPE
	    , p_user_id		IN			t7020_part_price_adj_request.c7020_initiated_by%TYPE
	    , p_txn			IN OUT		t7020_part_price_adj_request.c7020_part_price_adj_req_id%TYPE
	    , p_comments    IN          VARCHAR2
    )
    AS
    BEGIN
		
	    gm_pkg_sm_price_adj_txn.gm_validate_part_price_details(p_txn); -- To validate the transaction
	    
		gm_sav_part_price_master_t7020( p_party_id
									  , '105286' -- Pending Approval
									  , p_user_id
									  , p_txn 
									  , p_comments);
	 	
	END gm_submit_part_price_adj;
    
    /***********************************************************************************************************
	* Description : Procedure to save the transaction details of Part Price Adjustment Initiate in master table
	* author : arajan
	************************************************************************************************************/
    PROCEDURE gm_sav_part_price_master_t7020 ( 
	      p_party_id	IN		 	t7020_part_price_adj_request.c101_party_id%TYPE
	    , p_status 		IN 			t7020_part_price_adj_request.c901_request_status_cd%TYPE
	    , p_user_id		IN			t7020_part_price_adj_request.c7020_initiated_by%TYPE
	    , p_txn			IN OUT		t7020_part_price_adj_request.c7020_part_price_adj_req_id%TYPE
	    , p_comments    IN          VARCHAR2
    )
    AS
    	v_message   	VARCHAR2(4000);
    BEGIN
    
		UPDATE t7020_part_price_adj_request
		   SET c901_request_status_cd = p_status
		     , c101_party_id = p_party_id
		     , c7020_last_updated_by = p_user_id
		     , c7020_last_updated_date = CURRENT_DATE
		 WHERE c7020_part_price_adj_req_id = p_txn			
		   AND c7020_void_fl IS NULL;
			
		IF (SQL%ROWCOUNT = 0)
		THEN
			SELECT 	'PADJ-'||s7020_part_price_adj_request.NEXTVAL
				INTO p_txn
			FROM DUAL;
		
			INSERT INTO t7020_part_price_adj_request
				(c7020_part_price_adj_req_id, c101_party_id, c901_request_status_cd, c7020_initiated_by, c7020_initiated_date)
			VALUES
				(p_txn, p_party_id, p_status, p_user_id, CURRENT_DATE);
		END IF;

		IF p_comments IS NOT NULL OR p_comments != ''
		THEN
			-- SAVING THE COMMNETS
	 		GM_UPDATE_LOG(p_txn,p_comments,4000898,p_user_id,v_message); --Part Price Adjustment Log
	 	END IF;
	END gm_sav_part_price_master_t7020;
	
	/***********************************************************************************************
	* Description : Procedure to save the part number details of Part Price Adjustment Initiate
	* author : arajan
	***************************************************************************************************/
	PROCEDURE gm_sav_part_price_adj_dtls (
		p_party_id			IN		 	t7020_part_price_adj_request.c101_party_id%TYPE
	  , p_input_str			IN			CLOB
	  , p_rem_str			IN			CLOB
	  , p_user_id			IN			t7020_part_price_adj_request.c7020_initiated_by%TYPE
	  , p_txn				IN 			t7020_part_price_adj_request.c7020_part_price_adj_req_id%TYPE
	  , p_account_id   		IN			t704_account.c704_account_id%TYPE
	  , p_gpo_id	   		IN			t705_account_pricing.c101_party_id%TYPE
	  , p_old_txn_date		IN			t7020_part_price_adj_request.c7020_last_updated_date%TYPE
	  , p_net_price_cal_fl	IN			VARCHAR2 DEFAULT NULL
	)
	AS
		v_strlen    	NUMBER          := NVL (LENGTH (p_input_str), 0) ;
		v_remstrlen    	NUMBER          := NVL (LENGTH (p_rem_str), 0) ;
	    v_string    	CLOB := p_input_str;
	    v_substring 	CLOB ;
	    v_pnum			t7021_part_price_adj_details.C205_PART_NUMBER_ID%TYPE;
	    v_adj_code  	t7021_part_price_adj_details.c901_adj_code%TYPE;
	    v_adj_val       t7021_part_price_adj_details.c7021_adj_value%TYPE;
	    v_unit_price    t7021_part_price_adj_details.c7021_unit_price%TYPE;
	    v_net_price     t7021_part_price_adj_details.C705_NET_CAL_UNIT_PRICE%TYPE;
	    v_adj_detail_id	t7021_part_price_adj_details.c7021_part_price_adj_detail_id%TYPE;
	    v_temp_pnum		VARCHAR2(4000);
	    v_temp_adj_code	VARCHAR2(4000);
	    v_cnt			NUMBER;
	    v_temp_adj_val	VARCHAR2(4000);
	    v_err_code		t7021_part_price_adj_dtls_temp.c7021_error_fl%TYPE := NULL;
	    v_party_type	t704_ACCOUNT.C101_PARTY_ID%TYPE;
	    v_account_id	t704_account.c704_account_id%TYPE;
	BEGIN
		
		v_party_type := gm_pkg_sm_price_adj_rpt.get_party_type(p_party_id);-- Get the party type
		
		my_context.set_my_inlist_ctx (p_rem_str);
		
		IF v_remstrlen > 0 THEN -- If any saved rows are deleted from the screen, void it in the table
			UPDATE T7021_PART_PRICE_ADJ_DETAILS
			SET C7021_VOID_FL = 'Y'
				, C7021_LAST_UPDATED_BY = p_user_id
				, C7021_LAST_UPDATED_DATE = CURRENT_DATE
			WHERE c7021_part_price_adj_detail_id IN (SELECT TOKEN FROM V_IN_LIST)
			AND c7021_void_fl IS NULL;
			
			-- To delete the record from the temporary table if it is removed from the screen
			DELETE FROM t7021_part_price_adj_dtls_temp
		    WHERE c7021_part_price_adj_detail_id IN (SELECT TOKEN FROM V_IN_LIST);
			
		END IF;
		
		-- If the party type is 903111 [Group Price Book] then the account id will be null
		IF v_party_type = 903111 THEN
			v_account_id := NULL;
		ELSE
			v_account_id := p_account_id;
		END IF;
		
		IF v_strlen > 0 THEN
	        WHILE INSTR (v_string, '|') <> 0
	        LOOP
	        	v_substring  	:= SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
    			v_string     	:= SUBSTR (v_string, INSTR (v_string, '|')    + 1);
	            v_adj_detail_id := NULL;
    			v_temp_pnum    	:= NULL;
			    v_adj_code  	:= '';
			    v_adj_val       := '';
			    v_temp_adj_code	:= '';
			    v_temp_adj_val  := '';
			    v_adj_detail_id := TO_NUMBER(SUBSTR (v_substring, 1, INSTR (v_substring, '^')      - 1));
			    v_substring  	:= SUBSTR (v_substring, INSTR (v_substring, '^')         + 1);
			    v_temp_pnum    	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^')      - 1);
			    v_substring  	:= SUBSTR (v_substring, INSTR (v_substring, '^')         + 1);
			    v_temp_adj_code	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^')      - 1);
			    v_substring  	:= SUBSTR (v_substring, INSTR (v_substring, '^')         + 1);
			    v_temp_adj_val	:= REPLACE(REPLACE(v_substring,CHR(10),''),CHR(13),'');

				-- Get the error code 
			    v_err_code := gm_pkg_sm_price_adj_txn.get_validated_adj_dlts(v_temp_pnum
			    															, v_temp_adj_code
			    															, v_temp_adj_val
			    															, p_txn
			    															, v_adj_detail_id
			    															, v_account_id
			    															, p_gpo_id);
			    
			    IF v_err_code IS NOT NULL -- save the wrong details to temporary table
			    THEN
			    	gm_pkg_sm_price_adj_txn.gm_sav_adj_dlts_temp(p_party_id
			    												, p_user_id
			    												, p_txn
			    												, v_temp_pnum
			    												, v_temp_adj_code
			    												, v_temp_adj_val
			    												, v_adj_detail_id
			    												, v_err_code
			    												, v_account_id
			    												, p_gpo_id);
			    ELSE
			    	gm_pkg_sm_price_adj_txn.gm_sav_adj_dtls_t7021(p_party_id
			    											  , v_temp_adj_code
			    											  , v_temp_adj_val
			    											  , v_temp_pnum
															  , p_user_id
															  , p_txn
															  , v_account_id
															  , p_gpo_id
															  , v_adj_detail_id);
			    	
				END IF;
			END LOOP;
		END IF;
		
		-- No need to calculate again, if it is already calculated
		IF p_net_price_cal_fl IS NULL
		THEN
			-- Need to update the net unit price if the unit price is edited for any part number
			gm_pkg_sm_price_adj_txn.gm_update_net_unit_price(p_txn,p_user_id,p_party_id,p_account_id,p_gpo_id, p_old_txn_date);
		END IF;
		
	END gm_sav_part_price_adj_dtls;
	
	/***********************************************************************************************
	* Description : Procedure to save the part number details of Part Price Adjustment Initiate
	* author : arajan
	***************************************************************************************************/
	PROCEDURE gm_sav_adj_dtls_t7021 (
		p_party_id			IN		 	t7020_part_price_adj_request.c101_party_id%TYPE
	  , p_adj_code			IN			t7021_part_price_adj_details.c901_adj_code%TYPE
	  , p_adj_val			IN			t7021_part_price_adj_details.c7021_adj_value%TYPE
	  , p_pnum				IN			t7021_part_price_adj_details.C205_PART_NUMBER_ID%TYPE
	  , p_user_id			IN			t7020_part_price_adj_request.c7020_initiated_by%TYPE
	  , p_txn				IN 			t7020_part_price_adj_request.c7020_part_price_adj_req_id%TYPE
	  , p_account_id   		IN			t704_account.c704_account_id%TYPE
	  , p_gpo_id	   		IN			t705_account_pricing.c101_party_id%TYPE
	  , p_adj_detail_id		IN			t7021_part_price_adj_details.c7021_part_price_adj_detail_id%TYPE
	)
	AS
	    v_unit_price    t7021_part_price_adj_details.c7021_unit_price%TYPE;
	    v_net_price     t7021_part_price_adj_details.C705_NET_CAL_UNIT_PRICE%TYPE;
	    v_adj_detail_id	t7021_part_price_adj_details.c7021_part_price_adj_detail_id%TYPE;
	    
	BEGIN
			v_adj_detail_id := p_adj_detail_id;
			
		    v_unit_price := gm_pkg_sm_adj_rpt.get_account_part_unitprice(p_account_id, p_pnum, p_gpo_id); -- get the price
	
			IF p_adj_code = '105283' AND NVL(v_unit_price,0) <> 0 -- Dollar
			THEN
				v_net_price := v_unit_price - p_adj_val;
			ELSIF p_adj_code = '105284' AND NVL(v_unit_price,0) <> 0 -- Percentage
			THEN
				v_net_price := v_unit_price - (v_unit_price * (p_adj_val / 100)); 
			ELSE
				v_net_price := 0;
			END IF;
	
			DELETE FROM t7021_part_price_adj_dtls_temp
		    WHERE c7021_part_price_adj_detail_id = v_adj_detail_id
		    	AND c7020_part_price_adj_req_id = p_txn;
		    
			UPDATE t7021_part_price_adj_details
			SET c205_part_number_id = p_pnum
			  , c901_adj_code = p_adj_code
			  , c7021_adj_value = p_adj_val
			  , c7021_unit_price = v_unit_price
			  , c705_net_cal_unit_price = v_net_price
			  , c7021_last_updated_by = p_user_id
			  , c7021_last_updated_date = CURRENT_DATE
			WHERE c7020_part_price_adj_req_id = p_txn
			AND c7021_part_price_adj_detail_id = v_adj_detail_id
			AND c7021_void_fl IS NULL;
				
			IF (SQL%ROWCOUNT = 0)
			THEN
				SELECT 	s7021_part_price_adj_details.NEXTVAL
					INTO v_adj_detail_id
				FROM DUAL;
			
				INSERT INTO t7021_part_price_adj_details
					(c7021_part_price_adj_detail_id, c7020_part_price_adj_req_id, C205_PART_NUMBER_ID, c901_adj_code
					, c7021_adj_value, c7021_unit_price, C705_NET_CAL_UNIT_PRICE, c7021_last_updated_by, c7021_last_updated_date)
				VALUES
					(v_adj_detail_id, p_txn, p_pnum, p_adj_code
					, p_adj_val, v_unit_price, v_net_price, p_user_id, CURRENT_DATE);
			END IF;

	END gm_sav_adj_dtls_t7021;
	
	/***********************************************************************************************
	* Description : Function to validate the values from the grid
	* author : arajan
	***************************************************************************************************/
	FUNCTION get_validated_adj_dlts (
	    p_pnum			IN 			t7021_part_price_adj_dtls_temp.C205_PART_NUMBER_ID%TYPE
	  , p_adj_code  	IN          t7021_part_price_adj_dtls_temp.c901_adj_code%TYPE
	  , p_adj_val       IN          t7021_part_price_adj_dtls_temp.c7021_adj_value%TYPE
	  , p_txn			IN 			t7020_part_price_adj_request.c7020_part_price_adj_req_id%TYPE
	  , p_adj_detail_id	IN 			t7021_part_price_adj_details.c7021_part_price_adj_detail_id%TYPE
	  , p_account_id   	IN			t704_account.c704_account_id%TYPE
	  , p_gpo_id	   	IN			t705_account_pricing.c101_party_id%TYPE
	)
	RETURN t7021_part_price_adj_dtls_temp.c7021_error_fl%TYPE
	IS
	    v_cnt			NUMBER;
	    v_err_code		t7021_part_price_adj_dtls_temp.c7021_error_fl%TYPE := NULL;
	    v_dup_cnt		NUMBER;
	    v_unit_price	t7021_part_price_adj_dtls_temp.c7021_unit_price%TYPE;
	    v_net_price		t7021_part_price_adj_dtls_temp.c705_net_cal_unit_price%TYPE;
	    v_per_diff		NUMBER;
	    v_adj_val_fl	NUMBER;
	BEGIN
		
		v_adj_val_fl := IS_NUMBER(p_adj_val);
		
		v_cnt := gm_pkg_sm_price_adj_txn.get_part_num_cnt(p_pnum);-- Check whether the part number is available or not
		 
		-- if the part number is not available, update the table with empty value
		IF v_cnt = 0
		THEN
			v_err_code := v_err_code || 1;
		ELSE
			v_dup_cnt := gm_pkg_sm_price_adj_txn.get_validate_dup_part(p_pnum, p_txn);
			IF v_dup_cnt >= 1 AND p_adj_detail_id IS NULL -- if the record is to insert for the first time
			THEN
				v_err_code := v_err_code || '1D'; --If the part number is duplicate
			ELSIF v_dup_cnt > 1 AND p_adj_detail_id IS NOT NULL -- if the record is to edit
			THEN
				v_err_code := v_err_code || '1D'; --If the part number is duplicate
			END IF;
		END IF;
		
		-- check whether the value is number or not	
		IF IS_NUMBER(p_adj_code) != 1 OR p_adj_code IS NULL
		THEN
			v_err_code := v_err_code || '^' || 2;
		ELSIF p_adj_code != 105283 AND p_adj_code != 105284 -- 105283: Dollar; 105284: Percentage
		THEN
			v_err_code := v_err_code || '^' || 2;
		END IF;
		
		-- check whether the value is number or not
		IF v_adj_val_fl != 1 OR p_adj_val IS NULL OR p_adj_val < 0
		THEN
			v_err_code := v_err_code || '^' || 3;
		ELSIF IS_NUMBER(p_adj_code) = 1  
		THEN
		
			IF p_adj_code = '105283' AND MOD(p_adj_val,1) != 0 --Dollar adjustment cannot have decimal values
			THEN
				v_err_code := v_err_code || '^' || 3;
			END IF;
		END IF;
		
		IF v_err_code IS NULL OR (v_err_code != '1' AND INSTR(v_err_code,'1^') = 0)
		THEN
			v_unit_price := gm_pkg_sm_adj_rpt.get_account_part_unitprice(p_account_id, p_pnum, p_gpo_id);
		END IF;
		
		IF v_unit_price IS NULL
		THEN
			v_err_code := v_err_code || '^' || '4' || '^' || '5';
			RETURN v_err_code;
		END IF;
		
		IF p_adj_code = '105283' AND NVL(v_unit_price,0) <> 0 -- Dollar
		THEN
			v_net_price := v_unit_price - p_adj_val;
			IF v_net_price <= 0 OR MOD(v_net_price,1) != 0
			THEN
				v_err_code := v_err_code || '^' || '5';
			--ELSE
				--v_per_diff := ((v_unit_price-v_net_price)/v_unit_price)*100; -- get the percentage difference
				--IF v_per_diff > 10 THEN
					--v_err_code := v_err_code || '^' || '5';
				--END IF;
			END IF; 
		ELSIF p_adj_code = '105284' AND NVL(v_unit_price,0) <> 0 AND v_adj_val_fl = 1 -- Percentage
		THEN
			v_net_price := v_unit_price - (v_unit_price * (p_adj_val / 100));
			IF v_net_price <= 0 
			THEN
				v_err_code := v_err_code || '^' || '5';
			--ELSE
				--v_per_diff := ((v_unit_price-v_net_price)/v_unit_price)*100; -- get the percentage difference
				--IF v_per_diff > 10 THEN
					--v_err_code := v_err_code || '^' || '5';
				--END IF;
			END IF;
		ELSE
			v_err_code := v_err_code || '^' || '5';
		END IF;

		RETURN v_err_code;
		
	END get_validated_adj_dlts;
	
	/***********************************************************************************************
	* Description : Procedure to save the wrong part number details of Part Price Adjustment Initiate to a temporary table
	* author : arajan
	***************************************************************************************************/
	PROCEDURE gm_sav_adj_dlts_temp (
		p_party_id		IN		 	t7020_part_price_adj_request.c101_party_id%TYPE
	  , p_user_id		IN			t7020_part_price_adj_request.c7020_initiated_by%TYPE
	  , p_txn			IN 			t7021_part_price_adj_dtls_temp.c7020_part_price_adj_req_id%TYPE
	  , p_pnum			IN 			t7021_part_price_adj_dtls_temp.C205_PART_NUMBER_ID%TYPE
	  , p_adj_code  	IN          t7021_part_price_adj_dtls_temp.c901_adj_code%TYPE
	  , p_adj_val       IN          t7021_part_price_adj_dtls_temp.c7021_adj_value%TYPE
	  , p_adj_detail_id	IN 			t7021_part_price_adj_dtls_temp.c7021_part_price_adj_detail_id%TYPE
	  , p_err_code		IN 			t7021_part_price_adj_dtls_temp.c7021_error_fl%TYPE
	  , p_account_id   	IN			t704_account.c704_account_id%TYPE
	  , p_gpo_id	   	IN			t705_account_pricing.c101_party_id%TYPE
	)
	AS
	    v_adj_detail_id		t7021_part_price_adj_dtls_temp.c7021_part_price_adj_detail_id%TYPE;
	    v_unit_price    	t7021_part_price_adj_dtls_temp.c7021_unit_price%TYPE;
	    v_net_price     	t7021_part_price_adj_dtls_temp.C705_NET_CAL_UNIT_PRICE%TYPE;
	    v_pnum			 	t7021_part_price_adj_dtls_temp.c205_part_number_id%TYPE;
	   	v_adj_code  	    t7021_part_price_adj_dtls_temp.c901_adj_code%TYPE;
	   	v_adj_val           t7021_part_price_adj_dtls_temp.c7021_adj_value%TYPE;
	BEGIN
		
		IF p_err_code IS NULL OR (p_err_code != '1' AND INSTR(p_err_code,'1^')  = 0)
		THEN
			v_unit_price := gm_pkg_sm_adj_rpt.get_account_part_unitprice(p_account_id, p_pnum, p_gpo_id);
		ELSE
			v_unit_price := NULL;
		END IF;
		
		IF p_adj_code = '105283' AND NVL(v_unit_price,0) <> 0 AND IS_NUMBER(p_adj_val) = 1 -- Dollar
		THEN
			v_net_price := v_unit_price - p_adj_val;
		ELSIF p_adj_code = '105284' AND NVL(v_unit_price,0) <> 0  AND IS_NUMBER(p_adj_val) = 1-- Percentage
		THEN
			v_net_price := v_unit_price - (v_unit_price * (p_adj_val / 100)); 
		ELSE
			v_net_price := 0;
		END IF;
		
		SELECT SUBSTR(p_pnum,1,GET_MAX_LENGTH('c205_part_number_id','t7021_part_price_adj_dtls_temp')) INTO v_pnum FROM DUAL;
		SELECT SUBSTR(p_adj_code,1,GET_MAX_LENGTH('c205_part_number_id','t7021_part_price_adj_dtls_temp')) INTO v_adj_code FROM DUAL;
		SELECT SUBSTR(p_adj_val,1,GET_MAX_LENGTH('c205_part_number_id','t7021_part_price_adj_dtls_temp')) INTO v_adj_val FROM DUAL;
		
		
		-- If the record is already updated in T7021_PART_PRICE_ADJ_DETAILS table, void it
		UPDATE T7021_PART_PRICE_ADJ_DETAILS
		SET C7021_VOID_FL = 'Y'
			, C7021_LAST_UPDATED_BY = p_user_id
			, C7021_LAST_UPDATED_DATE = CURRENT_DATE
		WHERE c7021_part_price_adj_detail_id = p_adj_detail_id
		AND c7021_void_fl IS NULL;
	    
		UPDATE t7021_part_price_adj_dtls_temp
		SET  c205_part_number_id = v_pnum
		  , c901_adj_code = v_adj_code
		  , c7021_adj_value = v_adj_val
		  , c7021_unit_price = v_unit_price
		  , c705_net_cal_unit_price = v_net_price
		  , c7021_last_updated_by = p_user_id
		  , c7021_last_updated_date = CURRENT_DATE
		  , C7021_ERROR_FL = p_err_code
		WHERE c7020_part_price_adj_req_id = p_txn
		AND c7021_part_price_adj_detail_id = p_adj_detail_id
		AND c7021_void_fl IS NULL;
			
		IF (SQL%ROWCOUNT = 0)
		THEN
			SELECT 	s7021_part_price_adj_details.NEXTVAL
				INTO v_adj_detail_id
			FROM DUAL;
		
			INSERT INTO t7021_part_price_adj_dtls_temp
				(c7021_part_price_adj_detail_id, c7020_part_price_adj_req_id, c205_part_number_id, c901_adj_code
				, c7021_adj_value, c7021_unit_price, c705_net_cal_unit_price, c7021_last_updated_by, c7021_last_updated_date, C7021_ERROR_FL)
			VALUES
				(v_adj_detail_id, p_txn, v_pnum, v_adj_code
				, v_adj_val, v_unit_price, v_net_price, p_user_id, CURRENT_DATE, p_err_code);
		END IF;
		
	END gm_sav_adj_dlts_temp;
	
	/****************************************************************************
	* Description : Procedure to void the Part Price Adjustment Initiate record
	* author : arajan
	***********************************************************************************/
	PROCEDURE gm_void_part_price_adj (
	  	p_txn			IN 		    t7020_part_price_adj_request.c7020_part_price_adj_req_id%TYPE
	  , p_user_id		IN			t7020_part_price_adj_request.c7020_initiated_by%TYPE
	)
	AS
		v_status 	t7020_part_price_adj_request.c901_request_status_cd%TYPE;
	BEGIN
		
		BEGIN
			SELECT c901_request_status_cd INTO v_status
				FROM t7020_part_price_adj_request
			WHERE c7020_part_price_adj_req_id = p_txn
			AND c7020_void_fl IS NULL;
			EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				raise_application_error('-20677',''); -- No data found
		END;
		
		IF v_status = 105287 --Implemented
		THEN
			raise_application_error('-20667',''); -- Implemented record cannot be voided
		END IF;
		
		UPDATE T7021_PART_PRICE_ADJ_DETAILS
		SET C7021_VOID_FL = 'Y'
			, C7021_LAST_UPDATED_BY = p_user_id
			, C7021_LAST_UPDATED_DATE = CURRENT_DATE
		WHERE C7020_PART_PRICE_ADJ_REQ_ID = p_txn
		AND c7021_void_fl IS NULL;
		
		-- Need to delete from the temporary table if there are any records
		DELETE FROM t7021_part_price_adj_dtls_temp
		WHERE c7020_part_price_adj_req_id = p_txn;
		    	
		UPDATE T7020_PART_PRICE_ADJ_REQUEST
		SET C7020_VOID_FL = 'Y'
			, C7020_LAST_UPDATED_BY = p_user_id
			, C7020_LAST_UPDATED_DATE = CURRENT_DATE
		WHERE C7020_PART_PRICE_ADJ_REQ_ID = p_txn
		AND C7020_VOID_FL IS NULL;

	END gm_void_part_price_adj;
	
		
	/*******************************************************
	* Description : Function to get the part details
	* author : arajan
	*******************************************************/
	FUNCTION is_number (p_string IN VARCHAR2)
	   RETURN INT
	IS
	   v_new_num NUMBER;
	BEGIN
	   v_new_num := TO_NUMBER(p_string);
	   RETURN 1;
	EXCEPTION
	WHEN VALUE_ERROR THEN
	   RETURN 0;
	END is_number;
	
	
	/*******************************************************
	* Description : Function to get the part count
	* author : arajan
	*******************************************************/
	FUNCTION get_part_num_cnt (
	  	p_part_num		IN		t205_part_number.c205_part_number_id%TYPE
	)
	RETURN NUMBER
	IS
	v_count	NUMBER := 0;
	BEGIN

		BEGIN
			select count(1)
				into v_count
				from T205_PART_NUMBER
			where C205_PART_NUMBER_ID = p_part_num
			and C205_ACTIVE_FL = 'Y'
			and C901_STATUS_ID != '20369'; --Obsolete
		EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN 0;
		END;
		
		RETURN v_count;
	END get_part_num_cnt;
	
	/*******************************************************
	* Description : Function to get the duplicate part count
	* author : arajan
	*******************************************************/
	FUNCTION get_validate_dup_part (
	  	p_part_num		IN		t205_part_number.c205_part_number_id%TYPE
	  	, p_txn			IN		t7020_part_price_adj_request.c7020_part_price_adj_req_id%TYPE
	)
	RETURN NUMBER
	IS
	v_count	NUMBER := 0;
	BEGIN

		BEGIN
			SELECT count(1)
				INTO v_count
				FROM T7021_PART_PRICE_ADJ_DETAILS
			WHERE C205_PART_NUMBER_ID = p_part_num
			AND C7020_PART_PRICE_ADJ_REQ_ID = p_txn
			AND C7021_VOID_FL is null;
		EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			v_count := 0;
		END;
		-- If there are no records in T7021_PART_PRICE_ADJ_DETAILS table check whether the record available in T7021_PART_PRICE_ADJ_DTLS_TEMP table or not
		IF v_count = 0
		THEN
			BEGIN
				SELECT count(1)
					INTO v_count
					FROM T7021_PART_PRICE_ADJ_DTLS_TEMP
				WHERE C205_PART_NUMBER_ID = p_part_num
				AND C7020_PART_PRICE_ADJ_REQ_ID = p_txn
				AND C7021_VOID_FL is null;
			EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				v_count := 0;
			END;
		END IF;
		
		RETURN v_count;
	END get_validate_dup_part;
	
	
	/*******************************************************
	* Description : To get the maximum length of a column
	* author : arajan
	*******************************************************/
	FUNCTION get_max_length (
	  	p_col_name	IN 		USER_TAB_COLUMNS.COLUMN_NAME%TYPE
	  	, p_tab_name	IN		USER_TAB_COLUMNS.TABLE_NAME%TYPE
	)
	RETURN NUMBER
	IS
	v_length	NUMBER := 0;
	BEGIN

		BEGIN
			SELECT NVL(CHAR_COL_DECL_LENGTH, DATA_LENGTH) 
				INTO v_length
				FROM USER_TAB_COLUMNS 
			WHERE TABLE_NAME = UPPER(p_tab_name)
			AND COLUMN_NAME = UPPER(p_col_name);
		EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			v_length := 0;
		END;
		
		RETURN v_length;
	END get_max_length;
	
	/****************************************************************************
	* Description : Procedure to update the net price
	* author : arajan
	***********************************************************************************/
	PROCEDURE gm_update_net_unit_price (
	  	p_txn			IN 		    t7020_part_price_adj_request.c7020_part_price_adj_req_id%TYPE
	  , p_user_id		IN			t7020_part_price_adj_request.c7020_initiated_by%TYPE
	  , p_party_id		IN		 	t7020_part_price_adj_request.c101_party_id%TYPE
	  , p_account_id	IN			t704_account.c704_account_id%TYPE
	  , p_gpo_id	   	IN			t705_account_pricing.c101_party_id%TYPE
	  , p_old_txn_date	IN			t7020_part_price_adj_request.c7020_last_updated_date%TYPE
	)
	AS
		v_party_type		t704_ACCOUNT.C101_PARTY_ID%TYPE;
		v_account_id		T704_ACCOUNT.c704_account_id%TYPE;
		v_part_str			CLOB;
		v_string    		CLOB ;
	    v_substring 		CLOB ;
	    v_part_num 			t7021_part_price_adj_details.C205_PART_NUMBER_ID%TYPE;
	    v_adj_code			t7021_part_price_adj_details.c901_adj_code%TYPE;
	    v_unit_price    	t7021_part_price_adj_details.c7021_unit_price%TYPE;
	    v_net_price     	t7021_part_price_adj_details.C705_NET_CAL_UNIT_PRICE%TYPE;
	    v_adj_val       	t7021_part_price_adj_details.c7021_adj_value%TYPE;
	    v_input_str			CLOB := '';
	    v_adj_dtl_id		t7021_part_price_adj_details.c7021_part_price_adj_detail_id%TYPE;
	BEGIN
		v_party_type := gm_pkg_sm_price_adj_rpt.get_party_type(p_party_id);
		
		BEGIN
			SELECT RTRIM (XMLAGG (XMLELEMENT (E, C205_PART_NUMBER_ID || ',')) .EXTRACT ('//text()').getclobval(), ',')
			INTO v_part_str
			FROM T705_ACCOUNT_PRICING  
			WHERE DECODE(v_party_type, '903110', C704_ACCOUNT_ID, C101_PARTY_ID)  = DECODE(v_party_type, '903110', p_account_id, p_party_id)
			AND C205_PART_NUMBER_ID IN (SELECT C205_PART_NUMBER_ID
					FROM T7021_PART_PRICE_ADJ_DETAILS 
					WHERE c7020_part_price_adj_req_id = p_txn
					AND c7021_void_fl IS NULL
					UNION ALL
					SELECT C205_PART_NUMBER_ID
					FROM T7021_PART_PRICE_ADJ_DTLS_TEMP
					WHERE C7020_PART_PRICE_ADJ_REQ_ID = p_txn
					AND C7021_VOID_FL IS NULL
					)
			AND TRUNC(NVL(C705_LAST_UPDATED_DATE,C705_CREATED_DATE)) >=  TRUNC(p_old_txn_date)
			AND c705_void_fl IS NULL;
			EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				return;
		END;
		
		IF v_part_str IS NOT NULL
		THEN
			v_string := v_part_str||'|';
			WHILE INSTR (v_string, '|') <> 0
	        LOOP
	        	V_SUBSTRING  	:= SUBSTR (V_STRING, 1, INSTR (V_STRING, '|') - 1);
    			V_PART_NUM     	:= SUBSTR (V_STRING, 1, INSTR (V_STRING, ',')    - 1);
                      
    			IF v_part_num IS NULL
    			THEN
    				v_part_num     	:= v_substring;
    				v_string		:= NULL;
    			ELSE
    				v_string 		:= SUBSTR (v_substring, INSTR (v_substring, ',')   +1)|| '|';	
    			END IF;
		
			    BEGIN
				    SELECT c7021_part_price_adj_detail_id, c901_adj_code, c7021_adj_value 
				    	INTO v_adj_dtl_id, v_adj_code, v_adj_val --Get the adjustment code and value to calculate the net price
				    FROM t7021_part_price_adj_details
				    WHERE C7020_PART_PRICE_ADJ_REQ_ID = p_txn
					AND C205_PART_NUMBER_ID = v_part_num
					AND C7021_VOID_FL IS NULL;
					EXCEPTION
					WHEN NO_DATA_FOUND
					THEN
						SELECT c7021_part_price_adj_detail_id, c901_adj_code, c7021_adj_value 
							INTO v_adj_dtl_id, v_adj_code, v_adj_val --Get the adjustment code and value to calculate the net price
					    FROM t7021_part_price_adj_dtls_temp
					    WHERE C7020_PART_PRICE_ADJ_REQ_ID = p_txn
						AND C205_PART_NUMBER_ID = v_part_num
						AND C7021_VOID_FL IS NULL
						AND ROWNUM = 1;
				END;
				
				v_input_str := v_input_str || v_adj_dtl_id || '^' ||v_part_num || '^' ||v_adj_code|| '^' || v_adj_val || '|';
				
    		END LOOP;
    		gm_sav_part_price_adj_dtls(p_party_id
    									, v_input_str
    									, ''
    									, p_user_id
    									, p_txn
    									, p_account_id
    									, p_gpo_id
    									, p_old_txn_date
    									, 'Y');
		END IF;
		
	END gm_update_net_unit_price;
	
	/****************************************************************************
	* Description : Procedure to void the Part Price Adjustment Initiate record
	* author : hreddy
	***********************************************************************************/
	PROCEDURE gm_deny_part_price_adj (
	  	p_txn_id		IN 		    t7020_part_price_adj_request.c7020_part_price_adj_req_id%TYPE
	  , p_user_id		IN			t7020_part_price_adj_request.c7020_initiated_by%TYPE
	  , p_comments      IN          VARCHAR2
	  , p_out_string    OUT         CLOB
	)
	AS
		v_status 	t7020_part_price_adj_request.c901_request_status_cd%TYPE;
		v_count		NUMBER;
		p_message   VARCHAR2(4000);
		v_party_name VARCHAR2(100);
	BEGIN		
		-- Getting the comma separated transactions which are not in Pending Approval Stage.
		BEGIN
		
			SELECT count(1)
			  INTO v_count
			FROM T7020_PART_PRICE_ADJ_REQUEST 
			WHERE C7020_PART_PRICE_ADJ_REQ_ID = p_txn_id  
			   AND c7020_void_fl IS NULL 
			   AND c901_request_status_cd <> '105286' ;  -- Pending Approval		
			EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				v_count := 0;
		END;
		
		IF v_count > 1 THEN
        	
        	GM_RAISE_APPLICATION_ERROR('-20999','358',p_txn_id);      	
   		END IF;			
   		
		UPDATE t7020_part_price_adj_request
			   SET c901_request_status_cd = '105285' --initiated
			     , c7020_last_updated_by = p_user_id
			     , c7020_last_updated_date = CURRENT_DATE
		WHERE c7020_part_price_adj_req_id = p_txn_id
		AND c7020_void_fl IS NULL;
				
		-- SAVING THE COMMNETS
		IF p_comments IS NOT NULL THEN
	 		GM_UPDATE_LOG(p_txn_id,p_comments,4000898,p_user_id,p_message); --Part Price Adjustment Log
	 	END IF;
	 	
		p_out_string := 'The transaction '||p_txn_id||' has been denied successfully and the status is changed to Initiated';		
			
	END gm_deny_part_price_adj;
	
	
	/****************************************************************************
	* Description : Procedure to void the Part Price Adjustment Initiate record
	* author : hreddy
	***********************************************************************************/
	PROCEDURE gm_approv_part_price_adj (
	  	p_txn_id		IN 		    t7020_part_price_adj_request.c7020_part_price_adj_req_id%TYPE
	  , p_user_id		IN			t7020_part_price_adj_request.c7020_initiated_by%TYPE
	  , p_comments      IN          VARCHAR2
	  , p_out_string    OUT         CLOB
	)
	AS
		v_status 		t7020_part_price_adj_request.c901_request_status_cd%TYPE;
		v_acc_id    	VARCHAR2(30);
		v_acc_type  	VARCHAR2(30) := 'Account'; 
		v_count			NUMBER;
		p_message   	VARCHAR2(4000);
		v_party_name 	VARCHAR2(100);
		v_formated_string CLOB;
		v_party_trans_id  CLOB;
		
		-- For Approval Process
		CURSOR cur_txn_dtls
		IS
			SELECT T7021.C205_PART_NUMBER_ID PARTNUM, T7021.C7021_ADJ_VALUE ADJVALUE, T7021.C901_ADJ_CODE ADJCODE
			  , T7021.C705_NET_CAL_UNIT_PRICE NETPRICE, T7021.C7021_UNIT_PRICE UNITPRICE
			  , T7020.C101_PARTY_ID PARTYID, T7020.C901_REQUEST_STATUS_CD STATUS , t7020.c7020_part_price_adj_req_id transid
			FROM T7021_PART_PRICE_ADJ_DETAILS T7021, T7020_PART_PRICE_ADJ_REQUEST T7020
			WHERE T7020.C7020_PART_PRICE_ADJ_REQ_ID = T7021.C7020_PART_PRICE_ADJ_REQ_ID
				AND T7020.C7020_PART_PRICE_ADJ_REQ_ID = p_txn_id
				AND T7021.C7021_VOID_FL IS NULL ;
				
	BEGIN
		
		-- Getting the transactions which are not in Pending Approval Stage.
		BEGIN
			SELECT count(1)
			  INTO v_count
			  FROM T7020_PART_PRICE_ADJ_REQUEST 
			  WHERE C7020_PART_PRICE_ADJ_REQ_ID = p_txn_id
			   AND c7020_void_fl IS NULL 
			   AND c901_request_status_cd <> '105286' ;  -- Pending Approval		 
			   EXCEPTION
			   WHEN NO_DATA_FOUND
			   THEN
			   		v_count := 0;
		END;
		  	  
		-- Getting the transactions which are not in Pending Approval Stage.
		IF v_count > 1 THEN        
        	GM_RAISE_APPLICATION_ERROR('-20999','358',p_txn_id);      	
   		END IF;
   		
   		UPDATE t7020_part_price_adj_request
		   SET c901_request_status_cd = '105287' --implemented
			 , c7020_approved_by = p_user_id
			 , c7020_approved_date = CURRENT_DATE
			 , c7020_last_updated_by = p_user_id
			 , c7020_last_updated_date = CURRENT_DATE
		 WHERE c7020_part_price_adj_req_id = p_txn_id
		   AND c7020_void_fl IS NULL;					   
		
		-- For Saving the Commnets
		    IF p_comments IS NOT NULL THEN
				GM_UPDATE_LOG(p_txn_id,p_comments,4000898,p_user_id,p_message);	--Part Price Adjustment Log
			END IF;	   
		FOR v_txn_dtls IN cur_txn_dtls
			LOOP	
				BEGIN
					SELECT c704_account_id INTO v_acc_id FROM t704_account WHERE c101_party_id = v_txn_dtls.partyid	AND c704_void_fl IS NULL;
				EXCEPTION
					WHEN NO_DATA_FOUND
				THEN
					v_acc_id := NULL;
				END;
				
				IF v_acc_id IS NULL THEN 
					v_acc_id := v_txn_dtls.partyid;
					v_acc_type := 'Group';
				END IF;
				-- Calling New Procedure fot updating the Discount Type and Discount Offered Values.
				gm_save_account_disciunt_data(v_txn_dtls.partyid,v_txn_dtls.adjcode,v_txn_dtls.adjvalue,p_user_id,v_txn_dtls.partnum);					
			END LOOP;
			p_out_string := 'The Transaction '||p_txn_id||' has been Implemented';
		
	END gm_approv_part_price_adj;
	
	/**********************************************************************************************************************
	* Description : Procedure to Update /Insert the Discount Offered for the Part in T7052_ACCOUNT_PRICING_DISCOUNT table.
	* author : hreddy
	**********************************************************************************************************************/
	PROCEDURE gm_save_account_disciunt_data (
	  	p_party_id		IN 		    T7052_ACCOUNT_PRICING_DISCOUNT.c101_party_id%TYPE
	  , P_discount_type IN 			T7052_ACCOUNT_PRICING_DISCOUNT.C901_DISCOUNT_TYPE%TYPE
	  , p_discount		IN			T7052_ACCOUNT_PRICING_DISCOUNT.C7052_DISCOUNT_OFFERED%TYPE
	  , p_user_id		IN			t7020_part_price_adj_request.c7020_initiated_by%TYPE
	  , p_part_num		IN			T7052_ACCOUNT_PRICING_DISCOUNT.C205_PART_NUMBER_ID%TYPE
	)
	AS
		v_sys_date DATE;
	BEGIN
		
		UPDATE T7052_ACCOUNT_PRICING_DISCOUNT
		   SET C901_DISCOUNT_TYPE = P_discount_type
			 , C7052_DISCOUNT_OFFERED = p_discount
			 , C7052_LAST_UPDATED_BY = p_user_id
			 , C7052_HISTORY_FL = 'Y'
			 , C7052_LAST_UPDATED_DATE = CURRENT_DATE			 
		 WHERE c101_party_id = p_party_id
		   AND C205_PART_NUMBER_ID = p_part_num
		   AND C7052_VOID_FL IS NULL;
		
		IF (SQL%ROWCOUNT = 0)THEN
		
			INSERT INTO T7052_ACCOUNT_PRICING_DISCOUNT
						(C7052_ACCOUNT_PRICE_DISC_ID, c101_party_id, C901_DISCOUNT_TYPE, C7052_DISCOUNT_OFFERED
					   , C7052_LAST_UPDATED_BY, C7052_LAST_UPDATED_DATE , C205_PART_NUMBER_ID , C7052_HISTORY_FL
			) VALUES (S7052_ACCOUNT_PRICING_DISCOUNT.NEXTVAL, p_party_id, P_discount_type, p_discount
					   , p_user_id, CURRENT_DATE , p_part_num , NULL
			);
		END IF;		
		
	END gm_save_account_disciunt_data;
	
	/**********************************************************************************************************************
	* Description : Procedure to Insert record in t705_account_pricing table
	* author : arajan
	**********************************************************************************************************************/
	PROCEDURE gm_sav_acc_partnum_price (
	  	  p_txn_id		IN 		    t7020_part_price_adj_request.c7020_part_price_adj_req_id%TYPE
	)
	AS
		v_acc_id   		t704_account.c704_account_id%TYPE;
		v_count			NUMBER := 0;
		v_unit_price	t705_account_pricing.c705_unit_price%TYPE;
		v_discount		t705_account_pricing.c705_discount_offered%TYPE;
		v_gpo_id		t740_gpo_account_mapping.c101_party_id%TYPE;
		
		CURSOR cur_txn_dtls
		IS
			SELECT T7021.C205_PART_NUMBER_ID PARTNUM, T7020.C7020_LAST_UPDATED_BY UPDATEDBY, T7020.C7020_LAST_UPDATED_DATE UPDDATE
				FROM T7021_PART_PRICE_ADJ_DETAILS T7021, T7020_PART_PRICE_ADJ_REQUEST T7020
			WHERE T7020.C7020_PART_PRICE_ADJ_REQ_ID = T7021.C7020_PART_PRICE_ADJ_REQ_ID
				AND T7020.C7020_PART_PRICE_ADJ_REQ_ID = p_txn_id
				AND T7020.C7020_VOID_FL IS NULL 
				AND T7021.C7021_VOID_FL IS NULL;
	BEGIN
		-- Get the account id using the party id
		BEGIN
			SELECT C704_ACCOUNT_ID 
				INTO v_acc_id
			FROM T704_ACCOUNT T704, T7020_PART_PRICE_ADJ_REQUEST T7020
			WHERE T704.C101_PARTY_ID = T7020.C101_PARTY_ID
				AND T7020.C7020_PART_PRICE_ADJ_REQ_ID = p_txn_id
				AND T704.C704_VOID_FL IS NULL
				AND t7020.C7020_VOID_FL IS NULL;
			EXCEPTION
			WHEN NO_DATA_FOUND THEN
				RETURN;
		END;
		
		-- get the gpo id
		BEGIN
			SELECT C101_PARTY_ID
				INTO v_gpo_id
				FROM T740_GPO_ACCOUNT_MAPPING 
			WHERE C704_ACCOUNT_ID = v_acc_id
				AND C740_VOID_FL IS NULL;
			EXCEPTION
			WHEN NO_DATA_FOUND THEN
				RETURN;
		END;
		
		FOR v_txn_dtls IN cur_txn_dtls
		LOOP
		
		-- Check whether the any existing record is available or not, if not insert into t705 table
			BEGIN
				SELECT count(1)
					INTO v_count
				FROM T705_ACCOUNT_PRICING 
				WHERE C704_ACCOUNT_ID = v_acc_id
					AND C205_PART_NUMBER_ID = v_txn_dtls.partnum 
					AND C705_VOID_FL IS NULL;
				EXCEPTION
				WHEN NO_DATA_FOUND THEN
					v_count := 0;
			END;
			
			IF v_count = 0
			THEN
				BEGIN
					SELECT c705_unit_price, c705_discount_offered
						INTO v_unit_price, v_discount
	             		FROM t705_account_pricing 
	             		WHERE c101_party_id = v_gpo_id
	             			AND c205_part_number_id = v_txn_dtls.partnum
	             			AND c705_void_fl IS NULL;
	            EXCEPTION
				WHEN NO_DATA_FOUND THEN
					RETURN;
				END;
             	
             	-- Call the procedure to insert record into t705_account_pricing	
				GM_SAVE_ACCOUNT_PARTNUM_PRICE(v_unit_price
											, v_discount
											, v_txn_dtls.partnum
											, v_acc_id
											, v_txn_dtls.updatedby
											, 'Account');
			END IF;
		END LOOP;
		
	END gm_sav_acc_partnum_price;
	
END gm_pkg_sm_price_adj_txn;
/
