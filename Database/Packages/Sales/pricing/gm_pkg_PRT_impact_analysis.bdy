/**
@ PRT-79 (PRT-R203 Generate and view impact analysis for a price request)
@ Purpose: Package used for impact analysis 
@ Author: Matt B
@ Created date : May/2017
*/

create or replace
PACKAGE BODY gm_pkg_PRT_impact_analysis
IS

 /******************************************************************************
  * Author : Matt B
  * Description : Procedure that is called to get a count of c7501_price-update_fl= 'Y'
  ******************************************************************************/	
PROCEDURE gm_prt_fch_priceupdate_fl(
   p_requestId IN  T7501_account_price_req_dtl.C7500_ACCOUNT_PRICE_REQUEST_ID%TYPE
  ,p_Out_flagCount OUT Number)
AS
BEGIN
 
     SELECT COUNT(1) INTO p_Out_flagCount
     FROM t7501_account_price_req_dtl	
     WHERE c7501_price_update_fl = 'Y' 
     AND C7501_VOID_FL IS NULL
     AND C7500_ACCOUNT_PRICE_REQUEST_ID = p_requestId;

END gm_prt_fch_priceupdate_fl;

 /******************************************************************************
  * Author : Matt B
  * Description : main procedure to update data  in T7520 and T7521 Impact analysis tables  
  ******************************************************************************/
PROCEDURE gm_prt_sav_impact_analysis(
    p_batch_id		IN		T9600_BATCH.C9600_BATCH_ID%TYPE
   ,p_user_id		IN		T7520_PRICE_IMPACT_ANALYSIS.C7520_CREATED_BY%TYPE
   ,p_out_refId     OUT     T9601_BATCH_DETAILS.C9601_REF_ID%TYPE
)
AS
   v_request_id 		T7520_PRICE_IMPACT_ANALYSIS.C7500_ACCOUNT_PRICE_REQUEST_ID%TYPE;
   v_company_id			T7520_PRICE_IMPACT_ANALYSIS.C1900_COMPANY_ID%TYPE;
   v_impact_anly_id     T7520_PRICE_IMPACT_ANALYSIS.C7520_PRICE_IMPACT_ANALYSIS_ID%TYPE;
   v_party_id			T7500_ACCOUNT_PRICE_REQUEST.C101_GPB_ID%TYPE;
   v_request_status     T7500_ACCOUNT_PRICE_REQUEST.C901_REQUEST_STATUS%TYPE;
BEGIN

    -- Get the request id from batch refid 
    SELECT t9601.C9601_REF_ID
      INTO p_Out_Refid
      FROM T9601_BATCH_DETAILS T9601 ,
        T9600_BATCH T9600
      WHERE T9601.C9600_BATCH_ID = T9600.C9600_BATCH_ID
      AND T9600.C9600_BATCH_ID   = p_batch_id;
      
    v_request_id  := p_Out_Refid;
    
    SELECT T7500.C101_GPB_ID,T7500.C901_REQUEST_STATUS 
    INTO v_party_id,v_request_status 
    FROM T7500_ACCOUNT_PRICE_REQUEST T7500 
    WHERE T7500.C7500_ACCOUNT_PRICE_REQUEST_ID = v_request_id AND T7500.C7500_VOID_FL IS NULL;
    
    SELECT C1900_COMPANY_ID INTO v_company_id FROM T101_PARTY WHERE C101_PARTY_ID = v_party_id AND C101_VOID_FL IS NULL;
    
    --Save the header info 
    gm_prt_sav_impact_anlys_head(v_request_id,p_user_id,v_company_id);
    
    -- retrieve the Impact_analysis Id from T7520 table 
    SELECT C7520_PRICE_IMPACT_ANALYSIS_ID INTO v_impact_anly_id FROM T7520_PRICE_IMPACT_ANALYSIS 
    WHERE C7500_ACCOUNT_PRICE_REQUEST_ID = v_request_id AND C7520_VOID_FL IS NULL;
    
    IF (v_impact_anly_id > 0) THEN
       gm_prt_sav_12months_data(v_request_id,p_user_id,v_impact_anly_id);
       gm_prt_sav_12months_sales_data(v_request_id,p_user_id,v_impact_anly_id);
    END IF; 
    
    --mapping the GPO comparison GPB to price request 
    IF v_request_status IS NOT NULL AND v_request_status = '52121' THEN -- PENDING APPROVAL
    	gm_pkg_sm_pricerequest_txn.gm_upd_price_request_gpb_map(v_request_id,p_user_id);
    END IF; 

END gm_prt_sav_impact_analysis;

 /******************************************************************************
  * Author : Matt B
  * Description : procedure to save impact in T7520_PRICE_IMPACT_ANALYSIS (header) table  
  ******************************************************************************/
PROCEDURE gm_prt_sav_impact_anlys_head(
  p_request_id 	IN	T7520_PRICE_IMPACT_ANALYSIS.C7500_ACCOUNT_PRICE_REQUEST_ID%TYPE
  ,p_created_by	IN	T7520_PRICE_IMPACT_ANALYSIS.C7520_CREATED_BY%TYPE
  ,p_company_id  IN  T7520_PRICE_IMPACT_ANALYSIS.C1900_COMPANY_ID%TYPE
)
AS
BEGIN
     
     UPDATE T7520_PRICE_IMPACT_ANALYSIS SET C7520_GENERATED_DATE = SYSDATE
     WHERE C7500_ACCOUNT_PRICE_REQUEST_ID = p_request_id 
     AND C7520_VOID_FL IS NULL;
     
     IF (SQL%ROWCOUNT = 0) THEN
     
        INSERT INTO T7520_PRICE_IMPACT_ANALYSIS
        (
           C7520_PRICE_IMPACT_ANALYSIS_ID,
           C7500_ACCOUNT_PRICE_REQUEST_ID,
           C7520_CREATED_BY,
           C7520_CREATED_DATE,
           C7520_GENERATED_DATE,
           C1900_COMPANY_ID
         )
         VALUES(S7520_PRICE_IMPACT_ANALYSIS_ID.nextval,
            p_request_id,
            p_created_by,
            SYSDATE,
            SYSDATE,
            p_company_id
         );
         
         -- Update C7500_IMPACT_ANALYSIS_FL 
         UPDATE T7500_ACCOUNT_PRICE_REQUEST SET C7500_IMPACT_ANALYSIS_FL = 'Y'
         ,C7500_LAST_UPDATED_BY = p_created_by
         ,C7500_LAST_UPDATED_DATE = SYSDATE
         WHERE C7500_ACCOUNT_PRICE_REQUEST_ID = p_request_id;
         
     END IF; 

END gm_prt_sav_impact_anlys_head;

 /******************************************************************************
  * Author : Matt B
  * Description : procedure to retrieve impact analysis data from order and proposed price
  *               and iterate to create data 
  ******************************************************************************/
PROCEDURE gm_prt_sav_12months_data(
   p_request_id 	IN	T7520_PRICE_IMPACT_ANALYSIS.C7500_ACCOUNT_PRICE_REQUEST_ID%TYPE
  ,p_created_by		IN	T7520_PRICE_IMPACT_ANALYSIS.C7520_CREATED_BY%TYPE
  ,p_impact_anly_id 	IN 	T7521_PRICE_IMPACT_ANALY_DTL.C7521_PRICE_IMPAC_ANLY_DTL_ID%TYPE
)
AS
  v_Request_typ 	T7500_ACCOUNT_PRICE_REQUEST.C901_REQUEST_TYPE%TYPE;
  v_party_id  		T7500_ACCOUNT_PRICE_REQUEST.C101_GPB_ID%TYPE;
  
  CURSOR impactAnly_update_cursor 
  IS
  SELECT T501.c704_account_id acct_id 
	   ,T205.c205_part_number_id partnum 
	   ,T207.c207_set_id set_id 
       ,NVL( SUM( T502.C502_CORP_ITEM_PRICE * T502.C502_ITEM_QTY),0) ORDER_AMOUNT 
       ,SUM(DECODE(T501.C901_ORDER_TYPE, 2524, 0, T502.C502_ITEM_QTY ) ) ORDER_QTY
	   ,proposed_price.c7501_proposed_price proposed_price
	   ,p_created_by
	   ,SYSDATE
      FROM t501_order t501, t502_item_order t502, t207_set_master t207, t208_set_details t208, t205_part_number t205
       ,(SELECT t4011.c205_part_number_id , t7501.c7501_proposed_price
           FROM t7500_account_price_request t7500, t7501_account_price_req_dtl t7501, t4011_group_detail t4011
          WHERE t4011.c4010_group_id = t7501.c4010_group_id
            AND T7501.C7500_ACCOUNT_PRICE_REQUEST_ID = T7500.C7500_ACCOUNT_PRICE_REQUEST_ID
            AND T4011.C4011_PRIMARY_PART_LOCK_FL = 'Y'   --Only Primary flag
            AND T7501.C7501_VOID_FL is NULL 
            AND t4011.c205_part_number_id = nvl(T7501.c205_part_number_id,t4011.c205_part_number_id)
            AND t7501.c7500_account_price_request_id = p_request_id 
            GROUP BY t4011.c205_part_number_id, t7501.c7501_proposed_price ) proposed_price
        WHERE t207.c207_set_id = t208.c207_set_id
  		  AND t208.c205_part_number_id = t502.c205_part_number_id
     	  AND t502.c501_order_id = t501.c501_order_id
     	  AND t205.c205_part_number_id = t208.c205_part_number_id
     	  AND NVL (T501.C901_ORDER_TYPE, -9999) NOT IN (SELECT T906.C906_RULE_VALUE
                                                                 FROM T906_RULES T906
                                                                WHERE t906.c906_rule_grp_id IN ('ORDTYPE','ORDERTYPE') AND c906_rule_id = 'EXCLUDE')
     	AND t501.c501_void_fl IS NULL
     	AND t502.c502_void_fl IS NULL
     	AND t501.c501_delete_fl IS NULL
     	AND t207.c207_skip_rpt_fl IS NULL
     	AND T207.C901_SET_GRP_TYPE = 1600
     	AND T501.C704_ACCOUNT_ID IN (SELECT MY_TEMP_TXN_ID FROM MY_TEMP_LIST  )
     	AND T501.C501_ORDER_DATE    >= ADD_MONTHS(TRUNC(CURRENT_DATE ,'MONTH') , -11)
      	AND T501.C501_ORDER_DATE    <= CURRENT_DATE
     	AND NVL(T501.C901_ORDER_TYPE,-9999) NOT IN ('2533') --Inter company sales(ICS)
     	AND t502.C205_PART_NUMBER_ID = proposed_price.c205_part_number_id (+)
		GROUP BY t207.c207_set_id
       		,T207.c207_set_nm
       		,T205.c205_part_number_id
       		,T205.c205_part_num_desc
       		,proposed_price.c7501_proposed_price
       		,T501.c704_account_id
		ORDER BY set_id, partnum;
 BEGIN  
  
  -- retrieve Request type and Party id from T7500 table
  SELECT C901_REQUEST_TYPE,C101_GPB_ID INTO v_Request_typ,v_party_id
  FROM T7500_ACCOUNT_PRICE_REQUEST T7500
  WHERE T7500.C7500_ACCOUNT_PRICE_REQUEST_ID = p_request_id;
  
   
  -- Delete the Temp table
  DELETE FROM MY_TEMP_LIST;
  
  IF V_REQUEST_TYP = '903108' THEN     --Group account 
 	 INSERT INTO MY_TEMP_LIST (MY_TEMP_TXN_ID) 
	 SELECT T740.C704_ACCOUNT_ID FROM T740_GPO_ACCOUNT_MAPPING T740
	 WHERE T740.C101_PARTY_ID =  v_party_id
	 AND T740.C740_VOID_FL IS NULL;
  ELSE  -- individual account
     INSERT INTO MY_TEMP_LIST (MY_TEMP_TXN_ID) 
	 SELECT T704.C704_ACCOUNT_ID  FROM T704_ACCOUNT  T704
	 WHERE T704.C101_PARTY_ID = v_party_id 
	 AND T704.C704_VOID_FL IS NULL;
   END IF;
  
 
       -- Delete existing records in detail table before inserting new records.
       DELETE FROM T7521_PRICE_IMPACT_ANALY_DTL WHERE C7520_PRICE_IMPACT_ANALYSIS_ID = p_impact_anly_id;

       FOR impactAnly_update_val IN impactAnly_update_cursor
       LOOP
                gm_prt_sav_impact_anlys_dtl(p_impact_anly_id,impactAnly_update_val.acct_id,impactAnly_update_val.partnum,impactAnly_update_val.set_id,impactAnly_update_val.order_amount
                   ,impactAnly_update_val.proposed_price,p_created_by,impactAnly_update_val.order_qty);
        END LOOP;

END gm_prt_sav_12months_data;


 /******************************************************************************
  * Author : Matt B
  * Description : procedure to save in T7521_PRICE_IMPACT_ANALY_DTL (details) table  
  ******************************************************************************/
PROCEDURE gm_prt_sav_impact_anlys_dtl(
   p_impact_anly_id    IN T7521_PRICE_IMPACT_ANALY_DTL.C7521_PRICE_IMPAC_ANLY_DTL_ID%TYPE
   ,p_account_id 	   IN T7521_PRICE_IMPACT_ANALY_DTL.C704_ACCOUNT_ID%TYPE
   ,p_partnum		   IN T7521_PRICE_IMPACT_ANALY_DTL.C205_PART_NUMBER_ID%TYPE
   ,p_set_id		   IN T7521_PRICE_IMPACT_ANALY_DTL.C207_SET_ID%TYPE
   ,p_order_amount	   IN T7521_PRICE_IMPACT_ANALY_DTL.C7521_LAST_12MONTH_SALE%TYPE
   ,p_prop_amount	   IN T7521_PRICE_IMPACT_ANALY_DTL.C7521_PROJECTED_12MONTH_SALE%TYPE
   ,p_created_by	   IN T7521_PRICE_IMPACT_ANALY_DTL.C7521_CREATED_BY%TYPE
   ,p_order_qty        IN T7521_PRICE_IMPACT_ANALY_DTL.C7521_LAST_12MONTH_QTY%TYPE
   ,p_month		       IN VARCHAR2 DEFAULT NULL
   ,p_year	           IN VARCHAR2 DEFAULT NULL
   ,p_segment_id       IN T207_SET_MASTER.C901_PRICING_SEGMENT_ID%TYPE DEFAULT NULL
   ,p_party_id         IN T7500_ACCOUNT_PRICE_REQUEST.C101_GPB_ID%TYPE DEFAULT NULL
   ,p_gpbid     	   IN T7500_ACCOUNT_PRICE_REQUEST.C101_GPB_ID%TYPE DEFAULT NULL
   ,p_sales_fl         IN VARCHAR2 DEFAULT NULL
)
AS
BEGIN
	INSERT INTO T7521_PRICE_IMPACT_ANALY_DTL (C7521_PRICE_IMPAC_ANLY_DTL_ID
	,C7520_PRICE_IMPACT_ANALYSIS_ID
	,C704_ACCOUNT_ID
	,C205_PART_NUMBER_ID
	,C207_SET_ID
	,C7521_LAST_12MONTH_SALE
	,C7521_PROJECTED_12MONTH_SALE
	,C7521_CREATED_BY
	,C7521_CREATED_DATE
	,C7521_LAST_12MONTH_QTY
	,C7521_MONTH
	,C7521_YEAR
	,C901_SEGMENT_ID,
	C101_ACCOUNT_PARTY_ID,
	C101_GPB_ID,
	C7521_SALES_FLAG)
	VALUES(S7521_PRICE_IMPAC_ANLY_DTL_ID.nextval
	   ,p_impact_anly_id
	   ,p_account_id
	   ,p_partnum
	   ,p_set_id
	   ,p_order_amount
	   ,p_prop_amount
	   ,p_created_by
	   ,sysdate
	   ,p_order_qty
	   ,p_month
	   ,p_year
	   ,p_segment_id
	   ,p_party_id
	   ,p_gpbid
	   ,p_sales_fl
	   );

END gm_prt_sav_impact_anlys_dtl; 

 /******************************************************************************
  * Author : Matt B
  * Description : procedure to fetch impact analysis data   
  ******************************************************************************/
PROCEDURE gm_prt_fch_impact_analysis(
  p_pricing_request_id 	IN 	T7520_PRICE_IMPACT_ANALYSIS.C7500_ACCOUNT_PRICE_REQUEST_ID%TYPE,
  p_user_id 			IN  T101_USER.C101_USER_ID%TYPE,
  p_impact_anly_data 	OUT	TYPES.cursor_type
)
AS
  v_rec_cnt NUMBER;
  v_ac_id T704_ACCOUNT.C704_ACCOUNT_ID%TYPE;
  v_gpb_id T7500_ACCOUNT_PRICE_REQUEST.C101_GPB_ID%TYPE; 
  v_ad_name V700_TERRITORY_MAPPING_DETAIL.AD_NAME%TYPE;
  v_ad_id T7500_ACCOUNT_PRICE_REQUEST.C101_AD_ID%TYPE;
  v_org_name varchar2(100);
  v_gen_date T7520_PRICE_IMPACT_ANALYSIS.C7520_GENERATED_DATE%TYPE;
  v_request_type T7500_ACCOUNT_PRICE_REQUEST.C901_REQUEST_TYPE%TYPE;
  v_currency T901_CODE_LOOKUP.C901_CODE_GRP%TYPE;
  v_initiated_team 	VARCHAR2(20);
  v_company_id  	T7500_ACCOUNT_PRICE_REQUEST.C1900_COMPANY_ID%TYPE;
  v_sales_rep_id    CLOB;
  v_access_level	T101_USER.C101_ACCESS_LEVEL_ID%TYPE;
  v_rep_id    		CLOB;
  v_party_id   		T101_USER.C101_PARTY_ID%TYPE;
  
BEGIN
   BEGIN
		SELECT COUNT(T7521.C207_SET_ID) INTO v_rec_cnt
		FROM T7521_PRICE_IMPACT_ANALY_DTL T7521,
	 		 T7520_PRICE_IMPACT_ANALYSIS T7520
		WHERE T7520.C7520_PRICE_IMPACT_ANALYSIS_ID = T7521.C7520_PRICE_IMPACT_ANALYSIS_ID
		AND C7521_VOID_FL                         IS NULL
		AND C7500_ACCOUNT_PRICE_REQUEST_ID         = p_pricing_request_id;
   EXCEPTION WHEN NO_DATA_FOUND THEN 
	    	v_rec_cnt := 0;
   END;
   
   IF (v_rec_cnt > 0) THEN
      
      SELECT T7500.C901_REQUEST_TYPE,T7500.C101_AD_ID, T7500.C704_ACCOUNT_ID, T7500.C101_GPB_ID, T7500.C1900_COMPANY_ID , TO_CLOB(T7500.C703_SALES_REP_ID)
      INTO v_request_type, v_ad_id,v_ac_id,v_gpb_id,v_company_id,v_sales_rep_id
      FROM T7500_ACCOUNT_PRICE_REQUEST T7500
      WHERE T7500.C7500_ACCOUNT_PRICE_REQUEST_ID = p_pricing_request_id;
      
       IF v_request_type = '903108' THEN  --Group account 
          v_ad_name := null;
          SELECT T101.C101_PARTY_NM,  T901.C901_CODE_NM
          INTO v_org_name, v_currency 
		  FROM T101_PARTY T101, T1900_COMPANY T1900, T901_CODE_LOOKUP T901
		  WHERE T1900.C1900_COMPANY_ID = T101.C1900_COMPANY_ID
		  AND T1900.C901_TXN_CURRENCY = T901.C901_CODE_ID AND T901.C901_CODE_GRP = 'CURRN'
		  AND T101.C101_PARTY_ID = v_gpb_id; 
       ELSE
          SELECT T704.C704_ACCOUNT_NM, T901.C901_CODE_NM,GET_USER_NAME(v_ad_id)
          INTO v_org_name,v_currency,v_ad_name
          FROM T704_ACCOUNT T704,T901_CODE_LOOKUP T901
          WHERE T901.C901_CODE_ID = T704.C901_CURRENCY
          AND T901.C901_CODE_GRP='CURRN'
          AND T704.C704_ACCOUNT_ID = v_ac_id;   
       END IF;
        
       /*- Procedure for getting the who initiated the Price Request */
        gm_pkg_sm_price_approval_txn.gm_get_initiated_team(p_user_id,v_company_id,v_initiated_team);
        
        BEGIN
			SELECT C101_ACCESS_LEVEL_ID,C101_PARTY_ID 
				INTO v_access_level,v_party_id
				FROM T101_USER 
				WHERE C101_USER_ID = p_user_id;	
   		EXCEPTION WHEN NO_DATA_FOUND THEN 
	    	v_access_level := NULL;
  	    END;
         
  	     BEGIN
			 SELECT decode(v_initiated_team,'SATM',v_sales_rep_id,'PRTM',NULL)
          	    INTO v_sales_rep_id
          	    FROM DUAL;	
   		EXCEPTION WHEN NO_DATA_FOUND THEN 
	    	v_sales_rep_id := NULL;
  	    END;
          	
  	    /* If the user access level is less than "3" then the salesrepid will be used to filter the account based on the rep
  	     * If the user access level is >2 and if the user is from SATM then the repid will be passed based on AD and VP
  	     **/
		IF v_access_level < '3' THEN
			v_rep_id := To_CLOB(NVL(v_sales_rep_id,get_rep_id_from_party_id(v_party_id)));
			my_context.set_my_cloblist(v_rep_id || ',');
		ELSIF v_initiated_team = 'SATM' AND v_access_level > '2' THEN		
				 SELECT RTRIM(XMLAGG(XMLELEMENT(e,REP_ID || ',')).EXTRACT('//text()').getclobval(),',') 
					INTO v_rep_id 
				FROM v700_territory_mapping_detail 
				WHERE (ad_id = p_user_id OR vp_id = p_user_id);
			my_context.set_my_cloblist (v_rep_id);
		ELSE
			v_rep_id := NULL;
			my_context.set_my_cloblist (v_rep_id);
		END IF;
		
   	   OPEN p_impact_anly_data FOR
              SELECT  SET_ID,
                ACCOUNT_ID,
                ACCOUNT_NM,
                SET_NAME,
                PARTNUM,
                GEN_DATE,
                ROUND(SUM(NVL((ACTUAL_SALE),0)),0) AS ACTUAL_SALE,
                ROUND(SUM(NVL((PROPOSED_SALE),0)),0) AS PROPOSED_SALE,
                ROUND(SUM(NVL((PROJ_IMPACT),0)),0) AS PROJ_IMPACT,
   				GROUPING_ID(SET_ID, ACCOUNT_ID,PARTNUM) AS grouping_id,
   				GROUP_ID() AS group_id,
   				v_ad_name AD_NAME,
                v_org_name ORG_NAME,
                v_currency AC_CURRENCY,
                p_pricing_request_id REQUEST_ID
    			FROM(
       				SELECT T7521.C207_SET_ID SET_ID,
                        T7521.C704_ACCOUNT_ID ACCOUNT_ID,
                        T704.C704_ACCOUNT_NM ACCOUNT_NM,
                        T207.C207_SET_NM SET_NAME,
                        T7521.C205_PART_NUMBER_ID PARTNUM,
                       NVL((T7521.C7521_LAST_12MONTH_SALE),0) ACTUAL_SALE ,
                        CASE 
                            WHEN (T7521.C7521_PROJECTED_12MONTH_SALE > 0 AND  T7521.C7521_LAST_12MONTH_QTY > 0)  THEN NVL(T7521.C7521_PROJECTED_12MONTH_SALE,0) * NVL(T7521.C7521_LAST_12MONTH_QTY,0) 
                            ELSE NVL((T7521.C7521_LAST_12MONTH_SALE),0)
                          END PROPOSED_SALE,
                        CASE
                           WHEN (T7521.C7521_PROJECTED_12MONTH_SALE > 0 AND  T7521.C7521_LAST_12MONTH_QTY > 0) THEN NVL((T7521.C7521_PROJECTED_12MONTH_SALE * T7521.C7521_LAST_12MONTH_QTY) ,0) - NVL(T7521.C7521_LAST_12MONTH_SALE,0) 
                           ELSE  NVL(T7521.C7521_LAST_12MONTH_SALE,0) - NVL(T7521.C7521_LAST_12MONTH_SALE,0)    
                        END  PROJ_IMPACT,
                        TO_CHAR (T7520.C7520_GENERATED_DATE, 'mm/dd/yyyy'||' HH24:MI:SS') GEN_DATE
  					FROM T7521_PRICE_IMPACT_ANALY_DTL T7521,
                        T7520_PRICE_IMPACT_ANALYSIS T7520,
                        T207_SET_MASTER T207,
                        T704_ACCOUNT T704
                	WHERE T7521.C7520_PRICE_IMPACT_ANALYSIS_ID = T7520.C7520_PRICE_IMPACT_ANALYSIS_ID
                        AND T207.C207_SET_ID                       = T7521.C207_SET_ID
                        AND T704.C704_ACCOUNT_ID                   = T7521.C704_ACCOUNT_ID
                        AND T7521.C7521_VOID_FL     IS NULL
                        AND T7521.C7521_SALES_FLAG 	IS NULL
                        AND (t704.c703_sales_rep_id IN (SELECT TO_CHAR(TOKEN) FROM v_clob_list WHERE TOKEN IS NOT NULL) OR v_initiated_team='PRTM')
    				AND T7520.C7500_ACCOUNT_PRICE_REQUEST_ID = p_pricing_request_id 
             		)
				GROUP BY GROUPING SETS(
				CUBE(ACCOUNT_ID, ACCOUNT_NM), 
				CUBE(SET_ID, SET_NAME),
				CUBE( SET_ID, SET_NAME,PARTNUM) ,
				CUBE( ACCOUNT_ID, ACCOUNT_NM,PARTNUM)  
				),GEN_DATE
				having GROUP_ID() =0 and ((ACCOUNT_NM is not null and PARTNUM is null and ACCOUNT_ID is not null and SET_NAME is  null and SET_ID is  null)
				or (ACCOUNT_NM is null and PARTNUM is not null and ACCOUNT_ID is null and SET_NAME is not null and SET_ID is not null)
				or (ACCOUNT_NM is null and PARTNUM is  null and ACCOUNT_ID is null and SET_NAME is not null and SET_ID is not null)
				or (ACCOUNT_NM is null and PARTNUM is  null and ACCOUNT_ID is null and SET_NAME is  null and SET_ID is  null)
				or (ACCOUNT_NM is not null and PARTNUM is not null and ACCOUNT_ID is not null and SET_NAME is null and SET_ID is null)
				)
				ORDER BY group_id,grouping_id,
				CASE WHEN PARTNUM is not null and SET_NAME is not null THEN SET_NAME     
				 WHEN PARTNUM is not null and ACCOUNT_NM is not null THEN ACCOUNT_NM    
				END,PROJ_IMPACT;
   ELSE
		raise_application_error('-20999','');
    END IF;
END gm_prt_fch_impact_analysis;

 /******************************************************************************
  * Author : Karthik
  * Description : procedure to Save the 12 month sales data
  ******************************************************************************/
PROCEDURE gm_prt_sav_12months_sales_data(
   p_request_id 	IN	T7520_PRICE_IMPACT_ANALYSIS.C7500_ACCOUNT_PRICE_REQUEST_ID%TYPE
  ,p_created_by		IN	T7520_PRICE_IMPACT_ANALYSIS.C7520_CREATED_BY%TYPE
  ,p_impact_anly_id IN 	T7521_PRICE_IMPACT_ANALY_DTL.C7521_PRICE_IMPAC_ANLY_DTL_ID%TYPE
)
AS
  v_Request_typ 	T7500_ACCOUNT_PRICE_REQUEST.C901_REQUEST_TYPE%TYPE;
  v_party_id  		T7500_ACCOUNT_PRICE_REQUEST.C101_GPB_ID%TYPE;
  v_acc_id			T7500_ACCOUNT_PRICE_REQUEST.C704_ACCOUNT_ID%TYPE;
  v_acc_party_id	T7500_ACCOUNT_PRICE_REQUEST.C101_GPB_ID%TYPE;
  
  CURSOR sales_analysis_cursor 
  IS
  SELECT T501.c704_account_id acct_id 
       ,T207.c207_set_id set_id 
       ,NVL( SUM( T502.C502_CORP_ITEM_PRICE * T502.C502_ITEM_QTY),0) ORDER_AMOUNT     
       , to_char(t501.c501_order_date,'YYYY') YEAR
       , to_char(t501.c501_order_date,'MM') MONTH
       ,v_party_id  ACCTPARTYID
       , T207.C901_PRICING_SEGMENT_ID SEGMENTID
       ,'Y' SALESANALYSISFLAG
       ,v_acc_party_id gpbid         
      FROM t501_order t501, t502_item_order t502, t207_set_master t207, t208_set_details t208, t205_part_number t205       
        WHERE t207.c207_set_id = t208.c207_set_id
        AND t208.c205_part_number_id = t502.c205_part_number_id
        AND t502.c501_order_id = t501.c501_order_id
        AND t205.c205_part_number_id = t208.c205_part_number_id
        AND NVL (T501.C901_ORDER_TYPE, -9999) NOT IN (SELECT T906.C906_RULE_VALUE
                                                                 FROM T906_RULES T906
                                                                WHERE t906.c906_rule_grp_id IN ('ORDTYPE','ORDERTYPE') AND c906_rule_id = 'EXCLUDE')
        AND t501.c501_void_fl IS NULL
        AND t502.c502_void_fl IS NULL
        AND t501.c501_delete_fl IS NULL
        AND t207.c207_skip_rpt_fl IS NULL
        AND T207.C901_SET_GRP_TYPE = 1600
        AND T501.C704_ACCOUNT_ID IN (SELECT MY_TEMP_TXN_ID FROM MY_TEMP_LIST)
        AND T501.C501_ORDER_DATE    >= ADD_MONTHS(TRUNC(CURRENT_DATE ,'MONTH') , -11)
        AND T501.C501_ORDER_DATE    <= CURRENT_DATE
        AND NVL(T501.C901_ORDER_TYPE,-9999) NOT IN ('2533') --Inter company sales(ICS)     
        GROUP BY t207.c207_set_id                                        
                 ,T501.c704_account_id
                 ,to_char(t501.c501_order_date,'YYYY')
                 ,to_char(t501.c501_order_date,'MM')
                 ,T207.C901_PRICING_SEGMENT_ID;
 BEGIN  
  
  -- retrieve Request type and Party id from T7500 table
  SELECT C901_REQUEST_TYPE,C101_GPB_ID,C704_ACCOUNT_ID INTO v_Request_typ,v_party_id,v_acc_id
  FROM T7500_ACCOUNT_PRICE_REQUEST T7500
  WHERE T7500.C7500_ACCOUNT_PRICE_REQUEST_ID = p_request_id;
  
   
  -- Delete the Temp table
  DELETE FROM MY_TEMP_LIST;

    IF V_REQUEST_TYP = '903108' THEN     --Group account 
	     INSERT INTO MY_TEMP_LIST (MY_TEMP_TXN_ID) 
	     SELECT T740.C704_ACCOUNT_ID FROM T740_GPO_ACCOUNT_MAPPING T740
	     WHERE T740.C101_PARTY_ID =  v_party_id
	     AND T740.C740_VOID_FL IS NULL;
  	ELSE  -- individual account
  	   	INSERT INTO MY_TEMP_LIST (MY_TEMP_TXN_ID) 
	   	SELECT T704.C704_ACCOUNT_ID  FROM T704_ACCOUNT  T704
	   	WHERE T704.C101_PARTY_ID = v_party_id 
	   	AND T704.C704_VOID_FL IS NULL;
		/*SELECT C101_PARTY_ID 
			INTO V_ACC_PARTY_ID 
			FROM T740_GPO_ACCOUNT_MAPPING
			WHERE C704_ACCOUNT_ID=v_acc_id;
			IF V_ACC_PARTY_ID IS NOT NULL THEN
				INSERT INTO MY_TEMP_LIST (MY_TEMP_TXN_ID) 
	            SELECT T740.C704_ACCOUNT_ID FROM T740_GPO_ACCOUNT_MAPPING T740
	            WHERE T740.C101_PARTY_ID =  v_acc_party_id
	            AND T740.C740_VOID_FL IS NULL;
			ELSE
		     	INSERT INTO MY_TEMP_LIST (MY_TEMP_TXN_ID) 
	         	SELECT T704.C704_ACCOUNT_ID  FROM T704_ACCOUNT  T704
	         	WHERE T704.C101_PARTY_ID = v_party_id 
	         	AND T704.C704_VOID_FL IS NULL;
			END IF;*/
    END IF;

       -- Delete existing records in detail table before inserting new records.
       DELETE FROM T7521_PRICE_IMPACT_ANALY_DTL WHERE C7520_PRICE_IMPACT_ANALYSIS_ID = p_impact_anly_id AND c7521_sales_flag IS NOT NULL;

       FOR sales_update_val IN sales_analysis_cursor
       LOOP
                    gm_prt_sav_impact_anlys_dtl(p_impact_anly_id,sales_update_val.acct_id,NULL,sales_update_val.set_id,sales_update_val.order_amount
                   ,NULL,p_created_by,NULL,sales_update_val.month,sales_update_val.year,sales_update_val.SEGMENTID,sales_update_val.acctpartyid
                   ,sales_update_val.gpbid,sales_update_val.salesanalysisflag);

        END LOOP;

END gm_prt_sav_12months_sales_data;

END GM_PKG_PRT_IMPACT_ANALYSIS;
/