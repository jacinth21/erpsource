--Database\Packages\Sales\pricing\gm_pkg_revert_account_part_pricing.bdy;

CREATE OR REPLACE PACKAGE BODY gm_pkg_revert_account_part_pricing IS
/******************************************************************************
  * Description : Procedure used to revert the part price from current price to previous price
  * Author : tramasamy
******************************************************************************/	
    PROCEDURE gm_upd_part_pricing (
        p_input_str  IN  CLOB,
        p_user_id    IN  t705_account_pricing.c101_party_id%TYPE
    ) AS

        v_strlen              NUMBER := nvl(
                              length(p_input_str),
                              0
                           );
        v_string              CLOB := p_input_str;
        v_substring           VARCHAR2(1000);
        v_account_pricing_id  t706_account_pricing_log.c705_account_pricing_id%TYPE;
        v_curr_price          t706_account_pricing_log.c705_unit_price%TYPE;
        v_new_price           t705_account_pricing.c705_unit_price%TYPE;
        v_txn_id              t705_account_pricing.c705_account_pricing_id%TYPE;
    BEGIN
        IF v_strlen > 0 THEN
            WHILE instr(v_string,',') <> 0 
             LOOP
	            --
	          v_substring := substr(v_string,1,instr( v_string, ',' ) - 1);
	          v_string := substr(v_string,instr(v_string,',') + 1);
              v_account_pricing_id := to_number(substr(v_substring,1,instr(v_substring,'|' ) - 1 ));
              v_substring := substr(v_substring,instr(v_substring,'|' ) + 1 );
              v_curr_price := to_number(v_substring);
                
              BEGIN
                SELECT
                    reqid,
                    rvalue
                INTO
                    v_txn_id,
                    v_new_price
                FROM
                    (
                        SELECT
                            t706.c705_account_pricing_id            reqid,
                            t706.c705_unit_price                    rvalue,
                            t7501.c7500_account_price_request_id    price_req_id
                        FROM
                            t706_account_pricing_log     t706,
                            t7501_account_price_req_dtl  t7501
                        WHERE
                                t706.c705_account_pricing_id = v_account_pricing_id
                            AND t706.c7501_account_price_req_dtl_id = t7501.c7501_account_price_req_dtl_id (+)
                            AND t706.c706_void_fl IS NULL
                            AND t7501.c7501_void_fl IS NULL
                            AND t706.c705_unit_price != v_curr_price
                        ORDER BY
                            t706.c706_created_date DESC
                    )
                WHERE
                    ROWNUM = 1;
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                  v_new_price :=v_curr_price;
                  v_txn_id:=v_account_pricing_id;
              END;
              
                UPDATE t705_account_pricing
                SET
                    c705_unit_price = v_new_price,
                    c705_last_updated_date = current_date,
                    c705_last_updated_by = p_user_id
                WHERE c705_account_pricing_id = v_txn_id
                  AND c705_void_fl IS NULL;

            END LOOP;

        END IF;
    END gm_upd_part_pricing;
/******************************************************************************
  * Description : Procedure used to Void the selected part price.
  * Author : tramasamy
******************************************************************************/	
    PROCEDURE gm_void_part_pricing (
        p_input_str  IN  CLOB,
        p_user_id    IN  t705_account_pricing.c101_party_id%TYPE
    ) AS

        v_strlen              NUMBER := nvl(length(p_input_str), 0);
        v_string              CLOB := p_input_str;
        v_substring           VARCHAR2(1000);
        v_account_pricing_id  t706_account_pricing_log.c705_account_pricing_id%TYPE;
    BEGIN
        IF v_strlen > 0 THEN
            WHILE instr( v_string, ',') <> 0 
            LOOP
	            --
	                v_substring := substr(v_string,1,instr(v_string,',') - 1);
                    v_string := substr(v_string,instr( v_string,',') + 1);
                    v_account_pricing_id := to_number(v_substring);
                
                UPDATE t705_account_pricing
                SET
                    c705_void_fl = 'Y',
                    c705_unit_price = NULL,
                    c705_last_updated_date = current_date,
                    c705_last_updated_by = p_user_id
                WHERE
                    c705_account_pricing_id = v_account_pricing_id;

            END LOOP;

        END IF;
    END gm_void_part_pricing;
/******************************************************************************
  * Description : Procedure used to update the Unit price of the part to the selected price.
  * Author : tramasamy
******************************************************************************/	
    PROCEDURE gm_upd_part_history_pricing (
        p_input_str  IN  CLOB,
        p_user_id    IN  t705_account_pricing.c101_party_id%TYPE
    ) AS

        v_strlen              NUMBER := nvl(length(p_input_str), 0);
        v_string              CLOB := p_input_str;
        v_substring           VARCHAR2(1000);
        v_account_pricing_id  t706_account_pricing_log.c705_account_pricing_id%TYPE;
        v_curr_price          t706_account_pricing_log.c705_unit_price%TYPE;
        v_new_price           t705_account_pricing.c705_unit_price%TYPE;
        v_txn_id              t705_account_pricing.c705_account_pricing_id%TYPE;
    BEGIN
        IF v_strlen > 0 THEN
            WHILE instr(v_string,',') <> 0
            LOOP
	            --
	            v_substring := substr(v_string,1,instr(v_string,',') - 1);
                v_string := substr(v_string,instr(v_string,',') + 1);
                v_account_pricing_id := to_number(substr(v_substring, 1,instr(v_substring,'|') - 1 ));
                v_substring := substr(v_substring,instr(v_substring,'|' ) + 1);
                v_curr_price := to_number(v_substring);
               
                UPDATE t705_account_pricing
                SET
                    c705_unit_price = v_curr_price,
                    c705_last_updated_date = current_date,
                    c705_last_updated_by = p_user_id
              WHERE c705_account_pricing_id = v_account_pricing_id
                AND c705_void_fl IS NULL;

            END LOOP;

        END IF;
    END gm_upd_part_history_pricing;

END gm_pkg_revert_account_part_pricing;
/
