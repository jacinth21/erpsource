create or replace
PACKAGE BODY gm_pkg_sm_pricerequest_rpt
IS

/***********************************************************************
  * Author : Jreddy
  * Description : Fetching threshold Questions and consultant DropDown Values
  ************************************************************************/
	PROCEDURE gm_fch_questionnair_details (
		p_requestid         IN 		t9010_answer_data.c9010_ref_id%TYPE
	   ,p_questiondetails   OUT		TYPES.cursor_type
	)
	AS
	BEGIN
		
		OPEN p_questiondetails
		 FOR
		SELECT t9011.c9011_question_id questionid
		, t9011.c9011_question_name questiondesc
		, t9011.c901_question_type questiontype
		, t9011.c9011_question_order questionNumber
		, t9010.c9010_answer_desc answerdesc
		, t9010.c901_consultant consultant
		, get_code_name(t9010.c901_consultant) consultantname
		FROM t9011_question t9011, t9010_answer_data t9010
   		WHERE t9011.c901_questionnair_type = 52130 -- Account Request Pricing
   		AND t9011.c9011_question_id = t9010.c9011_question_id (+)
    	AND t9010.c9010_ref_id(+) = UPPER(p_requestid)
    	AND t9010.c901_ref_type(+) = '52130'
    	AND t9010.c9010_void_fl(+) IS NULL
        AND t9011.c9011_void_fl IS NULL
    	ORDER BY questionNumber;
    	
		
	END gm_fch_questionnair_details;
	
	
/***********************************************************************
  * Author : Jreddy
  * Description : Fetching Group Part Map Details
  ************************************************************************/
	PROCEDURE gm_fch_group_part_details (
		 p_groupid 			  IN		VARCHAR2
	    ,p_grouppartdetails   OUT		TYPES.cursor_type
	)
	AS
		v_company_id NUMBER;
	BEGIN
		SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL'))   INTO  v_company_id FROM DUAL;
		
		OPEN p_grouppartdetails
		 FOR
		
		SELECT 0 GROUPID,
		  '' GROUPNAME,
		  T205.C205_PART_NUMBER_ID PNUM ,
		  C205_PART_NUM_DESC PDESC,
		  DECODE(get_part_pricing_type(T205.C205_PART_NUMBER_ID),0,0,52081) PRICING_TYPE ,
		  gm_pkg_pd_group_pricing.get_part_history_flag(T205.C205_PART_NUMBER_ID , p_groupid) historyfl,
		  get_part_pricing_type(T205.C205_PART_NUMBER_ID)+1 PNUM_CNT ,
		  gm_pkg_pd_group_pricing.get_part_primary_lock_flag(T205.C205_PART_NUMBER_ID) PART_PRIMARY_LOCK ,
		  'N' PRIMARY_LOCK_FL,
		  '0' GROUP_OWNER,
 		  TO_CHAR(GET_PART_PRICE(T205.C205_PART_NUMBER_ID,'L'),'999999.99') LISTPRICE,
		  to_char(get_part_price(t205.C205_PART_NUMBER_ID, 'E'),'999999.99') TRIPWIREPRICE
		FROM T205_PART_NUMBER T205
		WHERE REGEXP_LIKE(T205.C205_PART_NUMBER_ID ,'')
		AND T205.C205_PART_NUMBER_ID IN
		  (SELECT T2023.C205_PART_NUMBER_ID
		  FROM T2023_PART_COMPANY_MAPPING T2023
		  WHERE REGEXP_LIKE(T2023.C205_PART_NUMBER_ID,'')
		  AND T2023.C2023_VOID_FL          IS NULL
		  AND T2023.C1900_COMPANY_ID        = v_company_id
		  AND T2023.C901_PART_MAPPING_TYPE IN ( 105360,105361,10304547 )
		  )
		AND C205_SUB_COMPONENT_FL IS NULL
		MINUS
		SELECT 0 GROUPID,
		  '' GROUPNAME,
		  C205_PART_NUMBER_ID PNUM ,
		  GET_PARTNUM_DESC(C205_PART_NUMBER_ID) PDESC ,
		  DECODE(get_part_pricing_type(C205_PART_NUMBER_ID),0,0,52081) PRICING_TYPE ,
		  gm_pkg_pd_group_pricing.get_part_history_flag(C205_PART_NUMBER_ID,p_groupid) historyfl,
		  get_part_pricing_type(C205_PART_NUMBER_ID)+1 PNUM_CNT ,
		  gm_pkg_pd_group_pricing.get_part_primary_lock_flag(C205_PART_NUMBER_ID) PART_PRIMARY_LOCK,
		  'N' PRIMARY_LOCK_FL,
		  '0' GROUP_OWNER ,
		  NULL LISTPRICE,
		  NULL TRIPWIREPRICE
		FROM T4011_GROUP_DETAIL
		WHERE REGEXP_LIKE(C205_PART_NUMBER_ID ,'')
		AND C4010_GROUP_ID = p_groupid
		UNION
		SELECT C4010_GROUP_ID GROUPID,
		  GET_GROUP_NAME(C4010_GROUP_ID) GROUPNAME,
		  t4011.C205_PART_NUMBER_ID PNUM ,
		  GET_PARTNUM_DESC(t4011.C205_PART_NUMBER_ID) PDESC,
		  C901_PART_PRICING_TYPE PRICING_TYPE,
		  c4011_part_number_historyfl historyfl ,
		  get_part_pricing_type(t4011.C205_PART_NUMBER_ID) PNUM_CNT,
		  gm_pkg_pd_group_pricing.get_part_primary_lock_flag(t4011.C205_PART_NUMBER_ID) PART_PRIMARY_LOCK ,
		  NVL(c4011_primary_part_lock_fl,'N') PRIMARY_LOCK_FL,
		  gm_pkg_pd_group_pricing.get_group_primary_owner(C4010_GROUP_ID) GROUP_OWNER ,
		  TO_CHAR(GET_PART_PRICE(T205.C205_PART_NUMBER_ID,'L'),'999999.99') LISTPRICE,
		  to_char(get_part_price(t205.C205_PART_NUMBER_ID, 'E'),'999999.99') TRIPWIREPRICE
		FROM T4011_GROUP_DETAIL t4011,
		  t205_part_number t205
		WHERE C4010_GROUP_ID          = p_groupid
		AND T4011.C205_PART_NUMBER_ID = T205.C205_PART_NUMBER_ID
		ORDER BY PNUM;
			 
	END gm_fch_group_part_details;
	

	/*******************************************************
    * Description : This procedure used to get the all group details of a system
    * Author   :    Matt Balraj
    *******************************************************/
    PROCEDURE gm_fch_construct_details(
        p_company_id           IN     T704_ACCOUNT.C901_COMPANY_ID%TYPE,
        p_accparty_id          IN     t704_account.c704_Account_id%TYPE,
        p_select_systems       IN     CLOB,
        p_type_id              IN     VARCHAR2,
        p_group_cur            OUT    TYPES.cursor_type
    ) 
    AS
    -- v_party_id Number;
     v_row_count Number;
     v_id number;

     v_party_id number;
     v_company_id number;
    -- v_set_ids VARCHAR2 (50);
    BEGIN
	    IF  p_type_id = '903107' THEN
	    
		    SELECT c101_party_id INTO v_party_id
		      FROM t704_account
		     WHERE c704_account_id  = p_accparty_id 
		       AND c704_void_fl IS NULL;
		    
		    my_context.set_my_double_inlist_ctx (v_party_id,get_account_gpo_id(p_accparty_id)) ;
		ELSE
			v_party_id := p_accparty_id;
			my_context.set_my_double_inlist_ctx (v_party_id,NULL) ;
	    END IF;
	       
	    SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL'))   INTO  v_company_id FROM DUAL;
     
     --v_set_ids := '''300.151'',''300.024''';
     --v_set_ids := '300.151';
     v_company_id := p_company_id;
  my_context.set_my_inlist_ctx (p_select_systems);
  
   OPEN p_group_cur
    FOR	 
     /* Fetching constructor and realted groups details from  v_gm_pricing_construct and 
      * fetching list and trip price of each group which is in constructor from v_gm_pricing_list_trip and
      * fetching current price of each group which is in constructor from v_gm_pricing_current
     */
		SELECT v_construct.c207_set_id SETID ,
		  v_construct.c207_set_nm SETNAME ,
		  v_construct.c7003_construct_name CONSTRUCTNAME ,
		  v_construct.c901_construct_level CONSTRUCTLEVEL ,
		  v_construct.c4010_group_id GROUPID ,
		  v_construct.c4010_parent_group_id PARENTGROUPID,
		  v_construct.c4010_parent_group_nm PARENTGROUPNAME ,
		  v_construct.c7003_principal_fl PRINCIPALFLAG ,
		  v_construct.c4010_group_nm GROUPNAME ,
		  v_construct.c7004_qty QUANTITY,
		  '' PARTNUM,
		  '' PARTDESC ,
		  v_list_trip.unitlistprice UNITLISTPRICE ,
		  ROUND(v_list_trip.unittripwire,0) UNITTRIPWIRE ,
		  v_construct.c901_group_type GROUPTYPE ,
		  v_construct.c901_priced_type PRICETYPE ,
		  v_construct.c4010_specl_group_fl SPECLGRPFLAG ,
		  DECODE(v_current.current_price,NULL,' ',v_current.current_price) UNITCURRENTPRICE,
		  GET_CODE_NAME( v_construct.c901_group_type) GROUPTYPEDESC ,
		  ROUND(gm_pkg_sm_pricerequest_rpt.get_extension_price(v_list_trip.unittripwire,v_construct.c7004_qty),0) TRIPWIREEXTENSIONPRICE,
		  v_construct.c7003_system_construct_id CONSTRUCTID,
		  v_current.gpo_current_price GPOPRICE,
		  NULL SYSTEMDIVISIONID
		FROM v_gm_pricing_construct v_construct,
		  v_gm_pricing_list_trip v_list_trip,
		  v_gm_pricing_current v_current
		WHERE v_construct.c4010_group_id = v_current.c4010_group_id
		AND v_construct.c4010_group_id   = v_list_trip.c4010_group_id
		UNION ALL
	 /* Fetching groups details from  t4010_group and 
      * fetching list and trip price of each group which is in constructor from v_gm_pricing_list_trip and
      * fetching current price of each group which is in constructor from v_gm_pricing_current
     */
		SELECT t207.c207_set_id SETID ,
		  t207.c207_set_nm SETNAME ,
		  '' CONSTRUCTNAME ,
		  0 CONSTRUCTLEVEL ,
		  t4010.c4010_group_id GROUPID ,
		  NVL(t4010.c4010_parent_group_id,t4010.c4010_group_id) PARENTGROUPID,
		  get_group_name(NVL(t4010.c4010_parent_group_id,t4010.c4010_group_id)) PARENTGROUPNAME ,
		  '' PRINCIPALFLAG ,
		  t4010.c4010_group_nm GROUPNAME ,
		  1 QUANTITY ,
		  '' PARTNUM ,
		  '' PARTDESC,
		  v_list_trip.unitlistprice UNITLISTPRICE ,
		  ROUND(v_list_trip.unittripwire,0) UNITTRIPWIRE ,
		  t4010.c901_group_type GROUPTYPE ,
		  t4010.c901_priced_type PriceType ,
		  t4010.c4010_specl_group_fl SPECIALITYFL ,
		  DECODE(v_current.current_price,NULL,' ',v_current.current_price) UNITCURRENTPRICE,
		  GET_CODE_NAME( t4010.c901_group_type) GROUPTYPEDESC,
		  ROUND(gm_pkg_sm_pricerequest_rpt.GET_EXTENSION_PRICE(v_list_trip.unittripwire ,'1'),0) TRIPWIREEXTENSIONPRICE,
		  0 CONSTRUCTID,
		  v_current.gpo_current_price GPOPRICE,
		  T207.c1910_division_id SYSTEMDIVISIONID
		FROM t207_set_master T207,
		  t4010_group T4010,
		  v_gm_pricing_list_trip v_list_trip,
		  v_gm_pricing_current v_current
		WHERE t4010.c4010_group_id           = v_current.c4010_group_id
		AND t4010.c4010_group_id             = v_list_trip.c4010_group_id
		AND v_list_trip.c205_part_number_id IS NULL
		AND v_current.c205_part_number_id   IS NULL
		AND t4010.c207_set_id                = t207.c207_set_id
		AND NOT EXISTS
		  (SELECT v_construct.c4010_group_id
		  FROM v_gm_pricing_construct v_construct
		  WHERE v_construct.c4010_group_id=t4010.c4010_group_id
		  )
		AND t4010.c901_group_type     IS NOT NULL
		AND t4010.c4010_void_fl       IS NULL
		AND t4010.c4010_inactive_fl   IS NULL
		AND t4010.c4010_publish_fl    IS NOT NULL
		--AND t4010.c901_group_type NOT IN (52037)--Instruments and Cases
		AND NOT EXISTS (
                                SELECT c906_rule_value            
                                  FROM t906_rules
                                  WHERE c906_rule_id   = 'EXCLUDE'
                                  AND c906_rule_grp_id = 'GROUPTYPE'
                                  AND C1900_COMPANY_ID = v_company_id
                                  AND C906_VOID_FL    IS NULL
                                  AND t4010.c901_group_type = c906_rule_value
                                )--Instruments and Cases & Demo
		AND t4010.c901_type            = 40045--Forecast/Demand Group
		AND t4010.c901_priced_type     = 52110 --Group
		AND t207.c207_set_id          IN
		  (SELECT TOKEN FROM v_in_list
		  ) 
		UNION ALL
	 /* Fetching Individual groups details from  t4010_group,t4011_group_detail and 
      * fetching list and trip price of each group which is in constructor from v_gm_pricing_list_trip and
      * fetching current price of each group which is in constructor from v_gm_pricing_current
     */
		SELECT t207.c207_set_id SETID,
		  t207.c207_set_nm SETNAME,
		  '' CONSTRUCTNAME,
		  0 CONSTRUCTLEVEL,
		  t4010.c4010_group_id GROUPID ,
		  NVL(t4010.c4010_parent_group_id,t4010.c4010_group_id) PARENTGROUPID,
		  get_group_name(NVL(t4010.c4010_parent_group_id,t4010.c4010_group_id)) PARENTGROUPNAME,
		  '' PRINCIPALFLAG ,
		  t4010.c4010_group_nm GROUPNAME,
		  1 QUANTITY,
		  t205.c205_part_number_id PARTNUM,
		  t205.c205_part_num_desc PARTDESC,
		  v_list_trip.unitlistprice UNITLISTPRICE ,
		  ROUND(v_list_trip.unittripwire,0) UNITTRIPWIRE ,
		  t4010.c901_group_type GROUPTYPE,
		  t4010.c901_priced_type PriceType,
		  t4010.c4010_specl_group_fl SPECIALITYFL ,
		  DECODE(v_current.current_price,NULL,' ',v_current.current_price) UNITCURRENTPRICE,
		  GET_CODE_NAME( t4010.c901_group_type) GROUPTYPEDESC,
		  ROUND(gm_pkg_sm_pricerequest_rpt.get_extension_price(v_list_trip.unittripwire,1),0) TRIPWIREEXTENSIONPRICE,
		  0 CONSTRUCTID,
		  v_current.gpo_current_price GPOPRICE,
		  T207.c1910_division_id SYSTEMDIVISIONID
		FROM t4011_group_detail t4011,
		  t207_set_master t207,
		  t4010_group t4010,
		  t205_part_number t205,
		  v_gm_pricing_list_trip v_list_trip,
		  v_gm_pricing_current v_current
		WHERE t4010.c4010_group_id           = v_current.c4010_group_id
		AND t4010.c4010_group_id             = v_list_trip.c4010_group_id
		AND v_list_trip.c205_part_number_id IS NOT NULL
		AND v_current.c205_part_number_id   IS NOT NULL
		AND t207.c207_set_id                 = t4010.c207_set_id
		AND t4010.c4010_group_id             = t4011.c4010_group_id
	    AND (t4011.c901_part_pricing_type    = DECODE(NVL(T207.c1910_division_id,'-999'),2005,52080,52080 )
         OR t4011.c901_part_pricing_type     = DECODE(NVL(T207.c1910_division_id,'-999'),2005,52081,52080 )) --52080 - primary, 52081 - secondary
		AND t4011.c205_part_number_id        = t205.c205_part_number_id
		AND t4011.c205_part_number_id        = v_current.c205_part_number_id
		AND t4011.c205_part_number_id        = v_list_trip.c205_part_number_id
		AND t4010.c901_priced_type           = 52111--Individual
		--AND t4010.c901_group_type NOT       IN (52037)--Instruments and Cases
		AND NOT EXISTS (
                                SELECT c906_rule_value            
                                  FROM t906_rules
                                  WHERE c906_rule_id   = 'EXCLUDE'
                                  AND c906_rule_grp_id = 'GROUPTYPE'
                                  AND C1900_COMPANY_ID = v_company_id
                                  AND C906_VOID_FL    IS NULL
                                  AND t4010.c901_group_type = c906_rule_value
                                )--Instruments and Cases & Demo
		AND t4010.c4010_void_fl             IS NULL
		AND t4010.c901_type                  = 40045 --Forecast/Demand Group
		AND t207.c207_set_id                IN
		  (SELECT TOKEN FROM v_in_list
		  )
		AND t4010.c901_group_type   IS NOT NULL
		AND t4010.c4010_inactive_fl IS NULL
		AND t4010.c4010_publish_fl  IS NOT NULL
	ORDER BY PARTNUM, GROUPNAME, PARTDESC;
  
  END gm_fch_construct_details;
    
/***********************************************************************
  * Author : HReddi
  * Description :  Procedure for fetching the Price Request Dash Board details
  ************************************************************************/
	PROCEDURE gm_fch_price_report_details (
		 p_status_id 		  IN		CLOB
		,p_from_date		  IN		VARCHAR2
		,p_to_date			  IN		VARCHAR2
		,p_company_id		  IN		VARCHAR2
		,p_filter_reps		  IN	    CLOB
		,p_user_id            IN        VARCHAR2
	    ,p_pricereportdetails OUT		TYPES.cursor_type
	)
	AS
		v_date_fmt  VARCHAR2(20);
		v_pricing_grp_count NUMBER;
		v_query  CLOB;
		v_rep_query  CLOB;
		v_date_condition VARCHAR2(2000);
		v_sort_condition VARCHAR2(2000);
		v_gpb_party_id   T101_USER.C101_PARTY_ID%TYPE;
		v_sales_rep_id	CLOB;
	BEGIN
		SELECT get_company_dtfmt(p_company_id) INTO v_date_fmt FROM DUAL;		
		
		 BEGIN
				SELECT C101_PARTY_ID 
					INTO v_gpb_party_id
					 FROM T101_USER 
					 WHERE C101_USER_ID = p_user_id;	
	   		EXCEPTION WHEN NO_DATA_FOUND THEN 
		    	v_gpb_party_id := NULL;
  	    	END;
		
  	    	v_sales_rep_id := get_rep_id_from_party_id(v_gpb_party_id);
  	    	
		v_pricing_grp_count := gm_pkg_sm_price_approval_txn.gm_fch_user_grp_chk(p_user_id,'100005',p_company_id);
		v_sort_condition := ' ORDER BY status , TO_NUMBER (REPLACE (c7500_account_price_request_id,''PR-'','''')) DESC';
		
		v_query := 'SELECT c7500_account_price_request_id reqid
	                     , DECODE(c704_account_id , NULL , (''*''||get_party_name(c101_gpb_id)) , get_account_name(c704_account_id) ) groupAccName
                          ,(select Region_name from v700_territory_mapping_detail 
                          where AC_id = C704_ACCOUNT_ID and rep_id = c703_sales_rep_id ) regname
	                     , get_user_name(C7500_INITIATED_BY) initiatedby
	                     , to_char(C7500_INITIATED_DATE,'''||v_date_fmt||''') initiatedon
	  			         , get_code_name(c901_request_status) status
	  			         , c901_request_status statusid
	  			          ,to_char(c7500_submitted_date,'' mm/dd/yyyy hh24:mi'') statusupd
	                  FROM t7500_account_price_request 
	                 WHERE c7500_void_fl IS NULL
	                   AND c901_request_status IN (SELECT TOKEN FROM v_in_list WHERE TOKEN IS NOT NULL)
	                   AND c1900_company_id = '|| p_company_id ||'';
	                   
	    v_rep_query := 'SELECT c7500_account_price_request_id reqid
	                     , DECODE(c704_account_id , NULL , (''*''||get_party_name(c101_gpb_id)) , get_account_name(c704_account_id) ) groupAccName
                          ,(select Region_name from v700_territory_mapping_detail 
                          where AC_id = C704_ACCOUNT_ID and rep_id = c703_sales_rep_id ) regname
	                     , get_user_name(C7500_INITIATED_BY) initiatedby
	                     , to_char(C7500_INITIATED_DATE,'''||v_date_fmt||''') initiatedon
	  			         , get_code_name(c901_request_status) status
	  			         , c901_request_status statusid
	  			         ,to_char(c7500_submitted_date,'' mm/dd/yyyy hh24:mi'') statusupd
	                  FROM t7500_account_price_request 
	                 WHERE c7500_void_fl IS NULL
	                   AND c901_request_status IN (SELECT TOKEN FROM v_in_list WHERE TOKEN IS NOT NULL)
					   AND (c703_sales_rep_id IN (SELECT TOKEN FROM v_clob_list WHERE TOKEN IS NOT NULL) OR C7500_INITIATED_BY = '|| p_user_id ||' OR C101_AD_ID= '|| p_user_id ||' OR  C101_VP_AD= '|| p_user_id ||' )
	                   AND c1900_company_id = '|| p_company_id ||'';
	                   
	                   
	    IF( (p_from_date IS NOT NULL) AND (p_to_date IS NOT NULL)) THEN	     
	    	--v_date_condition := ' AND TRUNC(C7500_CREATED_DATE) >=  to_date('''|| p_from_date  ||''','''||v_date_fmt||''')   AND TRUNC(C7500_CREATED_DATE) <= to_date('''||p_to_date||''','''||v_date_fmt||''')  ';	 
        v_date_condition := ' AND TRUNC(C7500_CREATED_DATE) between  to_date('''|| p_from_date  ||''','''||v_date_fmt||''')   AND to_date('''||p_to_date||''','''||v_date_fmt||''')  ';	
	    END IF;
	  				
		IF v_pricing_grp_count > 0 THEN		
			my_context.set_my_inlist_ctx(p_status_id);		
			OPEN p_pricereportdetails
			 FOR
				v_query || v_date_condition || v_sort_condition;
	    ELSE
			my_context.set_my_cloblist (NVL(p_filter_reps,v_sales_rep_id));
			my_context.set_my_inlist_ctx(p_status_id);
			OPEN p_pricereportdetails
			 FOR		
				v_rep_query || v_date_condition || v_sort_condition;
	    END IF;
			 
	END gm_fch_price_report_details;

/***********************************************************************
  * Author : Tejeswara Reddy Chappidi
  * Description : Fetching the submitted price request id details
  ************************************************************************/
	PROCEDURE gm_fch_selected_request_dtls (
		 p_requestid         IN 	T7500_ACCOUNT_PRICE_REQUEST.C7500_ACCOUNT_PRICE_REQUEST_ID%TYPE
		,p_filter_reps		  IN	CLOB
		,p_user_id            IN    VARCHAR2
		,p_systemdetails 	 OUT	TYPES.cursor_type
		,p_groupdetails 	 OUT	TYPES.cursor_type
		,p_requestdetails 	 OUT	TYPES.cursor_type
	)
	AS
		v_rec_cnt NUMBER;
		v_company_id number;
		v_pricing_grp_count NUMBER;
	BEGIN
		BEGIN			
			SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL'))   INTO  v_company_id FROM DUAL;
			--validating logged in user is available in pricing group  
			v_pricing_grp_count := gm_pkg_sm_price_approval_txn.gm_fch_user_grp_chk(p_user_id,'100005',v_company_id);
			--setting sales rep ids in context
			my_context.set_my_cloblist (p_filter_reps);					
			SELECT count(1) 
			  INTO v_rec_cnt
	          FROM t7500_account_price_request 
	         WHERE c7500_account_price_request_id  = p_requestid 
	           AND (c703_sales_rep_id IN (SELECT TOKEN FROM v_clob_list WHERE TOKEN IS NOT NULL)
	           OR v_pricing_grp_count > 0)
	           AND c7500_void_fl IS NULL;
	    EXCEPTION WHEN NO_DATA_FOUND THEN 
	    	v_rec_cnt := 0;
	    END;
	    
	IF v_rec_cnt > 0 THEN
	
	  OPEN p_systemdetails FOR
	  
		 SELECT C207_SET_ID setid,
		        GET_SET_NAME(C207_SET_ID) setname,
		        C901_CHANGE_TYPE changetype,
		        DECODE(C901_CHANGE_TYPE, NULL , 'Choose one',Get_code_name(C901_CHANGE_TYPE)) changetypename,
                C7502_CHANGE_VALUE changevalue,
                REPLACE(C207_SET_ID,'.') ERRORSYSTEMID
		   FROM t7502_account_price_req_sys
		  WHERE C7500_ACCOUNT_PRICE_REQ_ID = p_requestid
		  AND C7502_VOID_FL IS NULL
		  ORDER BY setname;

      OPEN p_groupdetails FOR
      
		  SELECT t7501.C7501_ACCOUNT_PRICE_REQ_DTL_ID REQDTLID, t7501.C207_SET_ID SETID,
		         GET_SET_NAME(t7501.C207_SET_ID) SETNAME,
		         t7501.C901_CONSTRUCT_LEVEL CONSTRUCTLEVEL,
                 t7501.C4010_GROUP_ID GROUPID,
				 t4010.c4010_group_nm GROUPNAME,
				 t7501.C7501_QTY QUANTITY,
				 t7501.C7501_PROPOSED_PRICE PROPOSEDUNITPRICE,
				 t7501.C7501_CURRENT_PRICE UNITCURRENTPRICE,
				 t7501.C7501_TRIPWIRE_PRICE UNITTRIPWIRE,
				 t7501.C7501_LIST_PRICE UNITLISTPRICE,
				 gm_pkg_sm_pricerequest_rpt.GET_EXTENSION_PRICE(t7501.C7501_TRIPWIRE_PRICE,t7501.C7501_QTY) TRIPWIREEXTENSIONPRICE,
				 t7501.C7003_PRINCIPAL_FL principalflag,
	             t4010.c901_priced_type PriceType,
	             t7501.c4010_specl_group_fl SPECLGRPFLAG,
	             t7501.c205_part_number_id partnum,
                 GET_PARTNUM_DESC(t7501.c205_part_number_id) partdesc,
                 t7501.c901_group_type grouptype,
                 get_code_name(t7501.c901_group_type) grouptypedesc,
                 t7501.C7003_SYSTEM_CONSTRUCT_ID CONSTRUCTID,
                 t7003.C7003_CONSTRUCT_NAME CONSTRUCTNAME,
                 t7501.c901_change_type CHANGETYPENAME,
			     DECODE(t7501.c901_change_type,NULL,NULL,'52020',NULL,t7501.c7501_change_value) CHANGEVALUE,
			     DECODE(t7501.c901_change_type,NULL,NULL,'52020',NULL,t7501.c7501_change_value) INDIVIDUALGRPCHANGEVALUE,
			     get_code_name(t7501.C901_STATUS)  STATUSNAME,
			     t7501.C901_STATUS  STATUSID,
			     t7501.c4010_parent_group_id PARENTGROUPID,
			     get_group_name(t7501.c4010_parent_group_id) PARENTGROUPNAME,
			     t7501.C7501_CHECK_FL CHECKFL,
				 gm_pkg_sm_pricerequest_rpt.GET_SYSTEM_DIVISION(t7501.C207_SET_ID) SYSTEMDIVISIONID
			FROM T7501_ACCOUNT_PRICE_REQ_DTL t7501 , t4010_group t4010, t7003_system_construct t7003 
		   WHERE t7501.C7500_ACCOUNT_PRICE_REQUEST_ID = p_requestid
	         AND t7501.C4010_GROUP_ID =  t4010.C4010_GROUP_ID
	         AND t7501.c207_set_id = t7003.C207_SET_ID(+)
             AND t7501.c7003_system_construct_id = t7003.c7003_system_construct_id(+)
             AND t7003.c7003_void_fl(+) IS NULL
			 AND t7501.C7501_VOID_FL IS NULL
			 ORDER BY partnum, GROUPNAME, partdesc;
		 
      OPEN p_requestdetails FOR 
      
	      SELECT DECODE(C704_ACCOUNT_ID,null,C101_GPB_ID,C704_ACCOUNT_ID) ACCOUNTID,
		         DECODE(C704_ACCOUNT_ID,null,get_party_name(C101_GPB_ID),GET_ACCOUNT_NAME(C704_ACCOUNT_ID)) ACCOUNTNAME,
                 C7500_ACCOUNT_PRICE_REQUEST_ID PRICEREQUESTID,
                 C7500_PROJECTED_12MONTHS_SALE PROJ12MNTHSCALE,
                 DECODE(C704_ACCOUNT_ID,null,'903108','903107') typeid,
                 (SELECT count(1) FROM t9010_answer_data WHERE c9010_ref_id = p_requestid AND c9010_void_fl IS NULL) questioncnt,
                 get_code_name(C901_REQUEST_STATUS)  STATUSNAME,
			     C901_REQUEST_STATUS  STATUSID,
			     (SELECT COUNT(C901_REF_TYPE)  FROM T903_upload_file_list WHERE C903_REF_ID = p_requestid AND C901_TYPE  = '26230731' AND C903_DELETE_FL is NULL) documentcount,
			     (SELECT COUNT(T7520.C7520_PRICE_IMPACT_ANALYSIS_ID) FROM T7520_PRICE_IMPACT_ANALYSIS T7520, T7521_PRICE_IMPACT_ANALY_DTL T7521 
				   WHERE T7520.C7500_ACCOUNT_PRICE_REQUEST_ID = p_requestid AND T7520.C7520_PRICE_IMPACT_ANALYSIS_ID = T7521.C7520_PRICE_IMPACT_ANALYSIS_ID 
				   AND T7520.C7520_VOID_FL IS NULL AND T7521.C7521_VOID_FL IS NULL) impactcount
		    FROM T7500_ACCOUNT_PRICE_REQUEST
		   WHERE C7500_ACCOUNT_PRICE_REQUEST_ID = p_requestid
		     AND C7500_VOID_FL IS NULL;
	ELSE
		raise_application_error('-20999','');
	END IF;

	END gm_fch_selected_request_dtls;
	
/***********************************************************************
  * Author : Jreddy
  * Description : Fetching Account Name and Party Name
  ************************************************************************/
	PROCEDURE gm_fch_account_name (
		 p_acc_id 		  	IN		VARCHAR2
		,p_type_id 		  	IN		VARCHAR2
		,p_rep_id			IN      VARCHAR2
		,p_condition		IN      VARCHAR2
		,p_out_cursor	 	OUT		TYPES.cursor_type
	)
	AS
		v_query	 		CLOB;
		v_condition 	VARCHAR2(2000):= '';
	BEGIN	
		IF p_type_id = '903107' THEN -- Account
		
			v_query := ' SELECT AC_NAME ACCOUNTNAME
					     FROM v700_territory_mapping_detail v700,t704_account t704             
				         WHERE v700.ac_id = '''|| p_acc_id ||'''
					     AND v700.ac_id = t704.c704_account_id	 				
	                     AND c704_active_fl = ''Y''';
	                     
	        IF (v_condition IS NOT NULL) THEN 
	             v_condition :=  'AND '|| p_condition ;
	        END IF;
	        
		ELSE
			v_query := ' SELECT DECODE (c101_last_nm || c101_first_nm , NULL, c101_party_nm , c101_last_nm || '''' || c101_first_nm ) ACCOUNTNAME
					     FROM t101_party            
				         WHERE c101_party_id = '''|| p_acc_id ||'''
					     AND c101_delete_fl IS NULL	 				
	                     AND c901_party_type = ''7003''  AND c101_void_fl IS NULL ';
	    END IF;	    
		OPEN p_out_cursor FOR v_query || v_condition;

	END gm_fch_account_name;

/***********************************************************************
  * Author : treddy
  * Description : calculating tripwire EXTENSION PRICE
  ************************************************************************/	
FUNCTION GET_EXTENSION_PRICE
(p_unit_price IN T7010_GROUP_PRICE_DETAIL.C7010_PRICE%TYPE,
 p_qty IN NUMBER
)
RETURN VARCHAR2
IS
/*  Description     : THIS FUNCTION RETURNS EXTENSION PRICE
Parameters                       : p_code_id
*/
v_extension_price   VARCHAR2 (50);
BEGIN
     BEGIN
       IF p_unit_price IS NOT NULL AND p_unit_price > 0 AND p_qty IS NOT NULL AND p_qty >0 THEN
          v_extension_price := p_unit_price * p_qty;
       ELSE 
          v_extension_price := NULL;
       END IF;
     EXCEPTION
      WHEN OTHERS THEN
       RETURN NULL;
      END;
     RETURN v_extension_price;
END GET_EXTENSION_PRICE;

/***********************************************************************
  * Author : matthew
  * Description : get unit current price
  ************************************************************************/	
FUNCTION GET_GROUP_CURRENT_PRICE
(
   p_party_id IN T7540_account_group_pricing.c101_party_id%TYPE,
   p_set_Id IN T7540_account_group_pricing.c207_set_id%TYPE,
   p_group_id IN t7540_account_group_pricing.c4010_group_id%TYPE,
   p_part_number_id IN t7540_account_group_pricing.c205_part_number_id%TYPE
)
RETURN VARCHAR2 IS
     v_current_price   VARCHAR2 (50);
BEGIN
     BEGIN
       IF p_part_number_id IS NULL THEN
          SELECT t7540.c7540_unit_price INTO v_current_price
            FROM t7540_account_group_pricing T7540
           WHERE t7540.c207_set_id  =p_set_Id
            AND t7540.c4010_group_id = p_group_id
            AND t7540.c7540_void_fl IS NULL
            AND t7540.c101_party_id  = p_party_id;
       ELSE 
            SELECT t7540.c7540_unit_price INTO v_current_price
              FROM t7540_account_group_pricing T7540
             WHERE t7540.c207_set_id  =  p_set_Id
              AND t7540.c4010_group_id      =p_group_id
              AND t7540.c205_part_number_id = p_part_number_id
              AND t7540.c7540_void_fl      IS NULL
              AND t7540.c101_party_id = p_party_id;
       END IF;
     EXCEPTION
      WHEN OTHERS THEN
       RETURN '    ';
      END;
  RETURN v_current_price;
END GET_GROUP_CURRENT_PRICE;

/******************************************************************
* Author : Jreddy
* Description : This procedure is used to fetch System list
 ****************************************************************/
	PROCEDURE gm_fch_systems_list (
	p_searchtext	IN		 VARCHAR2
	,p_systemlist	OUT 	 TYPES.cursor_type
	)
	AS
	
		v_company_id t1900_company.C1900_COMPANY_ID%TYPE;
	
	BEGIN
	
	 	SELECT get_compid_frm_cntx()
    		INTO v_company_id
    		FROM DUAL;
    		
		OPEN p_systemlist
		 FOR
		 SELECT   t207.c207_set_id ID, t207.c207_set_nm NAME
		    FROM t207_set_master t207, t2080_set_company_mapping t2080
		   WHERE t207.c901_set_grp_type = 1600
		  -- AND t207.c207_release_for_sale = 'Y' (This is commented due to PMT-6291 Group Pricing Changes)
		     AND c207_void_fl IS NULL
		     AND c207_obsolete_fl IS NULL
		     AND t207.c207_set_id = t2080.c207_set_id
		     AND t2080.c1900_company_id = v_company_id
		     AND t2080.c2080_void_fl IS NULL
		     AND c2080_pricing_release_fl='Y'
		     AND  REGEXP_like(t207.c207_set_nm,p_searchtext,'i')
		ORDER BY UPPER (t207.c207_set_nm);
	END gm_fch_systems_list;

 /***********************************************************************
  * Author : HReddi
  * Description : Fetching the submitted price request id details
  ************************************************************************/
	PROCEDURE gm_fch_price_req_details (
		p_requestid         IN 	T7500_ACCOUNT_PRICE_REQUEST.C7500_ACCOUNT_PRICE_REQUEST_ID%TYPE
	  , p_rebatePrice       IN 	VARCHAR2
	  , p_user_id			IN  t7501_account_price_req_dtl.C7501_CREATED_BY%TYPE
	  , p_headerdetails 	OUT	TYPES.cursor_type
	  , p_constructdetails 	OUT	TYPES.cursor_type
	  , p_systemDetails		OUT	TYPES.cursor_type
	)AS
		v_company_id        T1900_COMPANY.C1900_COMPANY_ID%TYPE;
		v_date_fmt          VARCHAR2(20);
		v_rec_count			NUMBER;
		v_count			    NUMBER;
	BEGIN
		
		SELECT get_compid_frm_cntx()
	   		INTO v_company_id 
		FROM DUAL;
		
		SELECT get_company_dtfmt(v_company_id) INTO v_date_fmt FROM DUAL;
		
		SELECT count(1) INTO v_rec_count
		  FROM t7500_account_price_request
		 WHERE c7500_account_price_request_id = p_requestid
		   AND c901_request_status NOT IN ('52186','52187')  -- Initiated, Pending AD Approval
		   AND c7500_void_fl IS NULL;
		   
		--VALIDATING USERS FROM AD/VP TO VIEW ONLY THIER REQUEST   
		 SELECT count(1) INTO v_count
          FROM v700_territory_mapping_detail
         WHERE ad_id = p_user_id
           OR vp_id  = p_user_id;
           
           IF v_count > 0 THEN           
           	SELECT count(1) INTO v_rec_count
			  FROM t7500_account_price_request
			 WHERE c7500_account_price_request_id = p_requestid
			   AND c901_request_status NOT IN ('52186','52187')  -- Initiated, Pending AD Approval
			   AND (c101_ad_id = p_user_id
           		OR  c101_vp_ad  = p_user_id)
           	   AND c704_account_id IS NOT NULL
			   AND c7500_void_fl IS NULL;           
           END IF;
		
		IF v_rec_count > 0 THEN
		
				OPEN p_headerdetails FOR 		
					SELECT c7500_account_price_request_id priceRequestId 
		     		     , get_code_name(C901_REQUEST_STATUS)  STATUSNAME
					     , C901_REQUEST_STATUS  STATUSID
		     			 , get_user_name(c7500_initiated_by) initiatedBy
		     			 , to_char(c7500_initiated_date,v_date_fmt) initiatedDate
		     			 , gm_pkg_sm_pricerequest_rpt.get_acc_last12_month_sales(c704_account_id,c101_gpb_id) last12MonthSales
		     			 , c7500_projected_12months_sale Projected12MonthSales
		     			 , get_comp_curr_symbol(v_company_id) currency
		     			 , c704_account_id accountId
		     			 , DECODE(c704_account_id,'',C101_GPB_ID,c704_account_id) gpbid
                         , DECODE(c704_account_id,'',get_party_name(C101_GPB_ID),get_account_name(c704_account_id)) gpbname
		     			 , DECODE(p_user_id,c101_ad_id,'Y',c101_vp_ad,'Y','N') advpAccFl
		     			 ,decode(c901_currency,null,nvl(C901_CUR_VALUE,1),C901_CUR_VALUE) currval
		     			 , (select count(*) from t906_rules where c906_rule_grp_id = 'PRTIDN' and c906_rule_value = get_idn_no(C704_ACCOUNT_ID) and c906_rule_id = p_user_id) gtCount
		     			-- ,  GET_ACCOUNT_ATTRB_VALUE (v_acc_id,'1006433') groupPriceId  -- Healthcare Affiliation
					  FROM t7500_account_price_request 
					 WHERE c7500_account_price_request_id = p_requestid
					   AND c7500_void_fl IS NULL;
					   
				OPEN p_constructdetails FOR 		
					SELECT t7501.c7501_account_price_req_dtl_id prcdetlid,
					       t7501.C207_SET_ID SETID,
					       t7501.C7501_APPROVED_BY APPROVEDBY,
					       t207.c207_set_nm SET_NAME,
					       --(CASE WHEN t7501.C901_CONSTRUCT_LEVEL <> 0 THEN t207.c207_set_nm || ' '|| GET_CODE_NAME(t7501.C901_CONSTRUCT_LEVEL)|| ' Construct' WHEN  t4010.c4010_specl_group_fl = 'Y' THEN 'speciality Group' ELSE GET_CODE_NAME(t4010.C901_GROUP_TYPE) END) SETNAME,
					       (CASE WHEN t7501.C901_CONSTRUCT_LEVEL <> 0 THEN t7003.c7003_construct_name WHEN  t4010.c4010_specl_group_fl = 'Y' THEN 'Speciality Group' ELSE GET_CODE_NAME(t4010.C901_GROUP_TYPE) END) SETNAME,
					       t7501.C901_CONSTRUCT_LEVEL CONSTRUCTLEVEL,       
					       t7501.C4010_GROUP_ID GROUPID,
					       DECODE(t7501.c205_part_number_id, null,t4010.c4010_group_nm,(t7501.c205_part_number_id || ' - ' || t205.c205_part_num_desc)) GROUPNAME,
					       t4010.C901_GROUP_TYPE GROUPTYPE, 
					       GET_CODE_NAME(t4010.C901_GROUP_TYPE) GROUPTYPEDESC,
					       t7501.c205_part_number_id PARTNUM, 
					       t205.c205_part_num_desc PARTDESC, 
					       T4010.c901_priced_type PRICETYPE, 
					       get_code_name(T4010.c901_priced_type) PRICETYPEDES,
					       DECODE(t7501.C7501_QTY,'0','1',t7501.C7501_QTY) QUANTITY,					       t7501.C7501_LIST_PRICE UNITLISTPRICE,
					       t7501.C7501_TRIPWIRE_PRICE UNITTRIPWIRE,
					       gm_pkg_sm_pricerequest_rpt.GET_EXTENSION_PRICE(t7501.C7501_TRIPWIRE_PRICE,t7501.C7501_QTY) TRIPWIREEXTENSIONPRICE,
					       t7501.C7501_CURRENT_PRICE UNITCURRENTPRICE, 
					       t7501.C7501_PROPOSED_PRICE PROPOSEDUNITPRICE,                    
					       DECODE(t7501.C7003_PRINCIPAL_FL,'Y','YES','N','NO',null) PRINCIPALFLAG,       
					       t4010.c4010_specl_group_fl SPECLGRPFLAG,
					       get_code_name(t7501.c901_status)  STATUSNAME,
					       t7501.c901_status  STATUSID,
					       t7501.c901_change_type CHANGETYPENAME,
					       DECODE(t7501.c901_change_type,NULL,NULL,'52020',NULL,t7501.c7501_change_value) CHANGEVALUE,
					       t7501.c7500_account_price_request_id PRICEREQID,
					       DECODE(NVL(p_rebatePrice,'0'),'0','0',(t7501.C7501_PROPOSED_PRICE - (t7501.C7501_PROPOSED_PRICE * NVL(p_rebatePrice,'0') /100)))  rebateProposedprice,
					       DECODE(NVL(p_rebatePrice,'0'),'0',(DECODE(t7501.C7501_PROPOSED_PRICE,null,'0',ROUND((((t7501.C7501_LIST_PRICE - t7501.C7501_PROPOSED_PRICE) / t7501.C7501_LIST_PRICE) * 100),2))) ,					       
					       (DECODE(t7501.C7501_PROPOSED_PRICE,null,'0',(
					             round(((t7501.C7501_LIST_PRICE - 
					                    DECODE(NVL(p_rebatePrice,'0'),'0','0',(t7501.C7501_PROPOSED_PRICE - (t7501.C7501_PROPOSED_PRICE * NVL(p_rebatePrice,'0') /100))))/t7501.C7501_LIST_PRICE ) * 100,2) || ' %'))) )listPriceOffList
					      , t7501.C7501_IMPLEMENT_FL implFlag
					      , DECODE((SELECT count(1) FROM t902_log WHERE c902_ref_id = to_char(t7501.c4010_group_id || '@'||p_requestid )  AND c902_type = '1252' AND c1900_company_id = v_company_id AND c902_void_fl IS NULL ),'0','N','Y') LOGFL
					      , t207.c207_set_nm SETNM
		            	  ,(CASE WHEN t7501.C901_CONSTRUCT_LEVEL <> 0 THEN 1 WHEN  t4010.c4010_specl_group_fl = 'Y' THEN 2 ELSE 3 END) SETORDCNT
					  FROM T7501_ACCOUNT_PRICE_REQ_DTL t7501 , t4010_group t4010 , t205_part_number t205 , t207_set_master t207 , t7500_account_price_request t7500, t7003_system_construct T7003
					 WHERE t7501.c7500_account_price_request_id = t7500.c7500_account_price_request_id
                       AND t7501.C7500_ACCOUNT_PRICE_REQUEST_ID = p_requestid
					   AND t7501.C4010_GROUP_ID =  t4010.C4010_GROUP_ID
					   AND t7501.c207_set_id = t207.c207_set_id
					   AND t7500.c901_request_status <> '52186'  -- INITIATED
					   AND t7501.C7501_VOID_FL IS NULL
					   AND t4010.c4010_void_fl IS NULL
					   AND t7500.c7500_void_fl IS NULL
					   AND t7501.c207_set_id   = t7003.c207_set_id(+)
					   AND t7501.c7003_system_construct_id = t7003.c7003_system_construct_id (+)
					   AND t7003.c7003_void_fl is null
					   AND NVL (t7501.c205_part_number_id, '-999') = t205.c205_part_number_id(+)
					ORDER BY SETID, SETORDCNT,SETNAME,GROUPNAME;
					
				OPEN p_systemDetails FOR
				    SELECT c207_set_id SETID ,  GET_SET_NAME(C207_SET_ID) SETNAME
		            FROM t7501_account_price_req_dtl 
		            WHERE c7500_account_price_request_id = p_requestid
		            AND c7501_void_fl IS NULL
		            GROUP BY c207_set_id ;
		ELSE
		    -- GM_RAISE_APPLICATION_ERROR('-20999','356',p_requestid);
		    raise_application_error('-20999','The entered Pricing Request Id <B>'|| p_requestid || '</B> is not available for Approval process.');		    
			
		END IF;
		
	END gm_fch_price_req_details;
	
	 /***********************************************************************
  * Author : Matt
  * Description : Fetching the details for price request based on approved prices
  ************************************************************************/
	PROCEDURE gm_fch_generate_price_file (
		p_requestid         IN 	T7500_ACCOUNT_PRICE_REQUEST.C7500_ACCOUNT_PRICE_REQUEST_ID%TYPE
	  , p_pricedetails	 	OUT	TYPES.cursor_type
	)AS
		v_rec_count			NUMBER;   
	BEGIN
	
	   SELECT Count(1) INTO v_rec_count 
	   FROM t7501_account_price_req_dtl T7501
	   WHERE 
	    t7501.c7500_account_price_request_id = p_requestid;
	   
	   IF v_rec_count > 0 THEN
	   
	   OPEN p_pricedetails FOR 	
		 SELECT DISTINCT T207.c207_set_nm SystemName ,
		 	T7501.c205_part_number_id PartNum ,
 			T205.c205_part_num_desc PartDesc ,
		 	T7501.c7501_list_price LISTPRICE ,
  		 	T7501.c7501_proposed_price UnitPrice ,
  		 	ROUND(((T7501.c7501_list_price - t7501.c7501_proposed_price) / t7501.c7501_list_price) * 100,2) DISCOUNT ,
  		 	get_code_name(T7501.C901_status)  STATUS
	   	FROM t7501_account_price_req_dtl t7501,
  			T207_set_master T207,
  			T205_part_number T205
		WHERE T207.c207_set_id                   = t7501.c207_set_id
			AND T205.c205_part_number_id             = t7501.c205_part_number_id
			AND T7501.C901_STATUS NOT IN ('52120') -- Denied
			AND T7501.c205_part_number_id           IS NOT NULL
			AND T7501.c7500_account_price_request_id = p_requestid
	UNION ALL
		SELECT DISTINCT T207.c207_set_nm SystemName,
  			T4011.c205_part_number_id PartNum,
  			T205.c205_part_num_desc PartDesc,
  			T7501.c7501_list_price LISTPRICE,
  			T7501.c7501_proposed_price UnitPrice,
  			ROUND(((T7501.c7501_list_price - t7501.c7501_proposed_price) / t7501.c7501_list_price) * 100,2) DISCOUNT ,
  			get_code_name(T7501.C901_status)  STATUS
		FROM T7501_account_price_req_dtl T7501,
  			T207_set_master T207,
  			T4011_group_detail T4011,
  			T205_part_number T205
		WHERE T4011.c4010_group_id               = t7501.c4010_group_id
			AND T207.c207_set_id                     = t7501.c207_set_id
			AND T205.c205_part_number_id             = t4011.c205_part_number_id
			AND T7501.C901_STATUS NOT IN ('52120') -- Denied
			AND T7501.C7501_VOID_FL                 IS NULL
			AND T7501.c205_part_number_id           IS NULL
			AND T7501.c7500_account_price_request_id = p_requestid;
	   ELSE
	      GM_RAISE_APPLICATION_ERROR('-20999','398',p_requestid);
	   END IF;
	   
	
	END gm_fch_generate_price_file;
  /******************************************************************************
  * Author : APrasath
  * Description : Summing and populating the Last 12 Month sales of an Account - GmSalesYTDBean.java(reportYTDByAccountActual())
  ******************************************************************************/	
	FUNCTION get_acc_last12_month_sales(
		p_account_id IN T704_account.c704_account_id%TYPE
		,p_gpb_id IN T704_account.c101_party_id%TYPE
	)
	RETURN NUMBER
	IS
	/*  Description     : Summing and populating the Last 12 Month sales of an Account
		Parameters      : p_account_id
	*/
	v_total_sales   NUMBER;
	BEGIN
		
		SELECT   NVL( SUM( T502.C502_CORP_ITEM_PRICE * T502.C502_ITEM_QTY),0) INTO  v_total_sales
			FROM T501_ORDER T501 ,
			  T502_ITEM_ORDER T502 ,
			  T703_SALES_REP T703 ,
			  T701_DISTRIBUTOR T701 ,
			  T704_ACCOUNT T704
			WHERE T703.C703_SALES_REP_ID             = T501.C703_SALES_REP_ID
			AND NVL(T501.C901_ORDER_TYPE,-9999) NOT IN ('2533')
			AND NVL (c901_order_type,    -9999) NOT IN
			  (SELECT t906.c906_rule_value
			  FROM t906_rules t906
			  WHERE t906.c906_rule_grp_id = 'ORDTYPE'
			  AND c906_rule_id            = 'EXCLUDE'
			  )
			AND NVL (c901_order_type, -9999) NOT IN
			  (SELECT t906.c906_rule_value
			  FROM t906_rules t906
			  WHERE t906.c906_rule_grp_id = 'ORDERTYPE'
			  AND c906_rule_id            = 'EXCLUDE'
			  )
			AND T701.C701_DISTRIBUTOR_ID = T703.C701_DISTRIBUTOR_ID
			AND T704.C704_ACCOUNT_ID     = T501.C704_ACCOUNT_ID
			AND T502.C501_ORDER_ID       = T501.C501_ORDER_ID
			AND T501.C501_VOID_FL       IS NULL
			AND T502.C502_VOID_FL       IS NULL
			AND T501.C501_DELETE_FL     IS NULL
			AND T501.C501_ORDER_DATE    >= ADD_MONTHS(TRUNC(CURRENT_DATE ,'MONTH') , -11)
			AND T501.C501_ORDER_DATE    <= CURRENT_DATE
			AND T501.C703_SALES_REP_ID  IN
			  (SELECT DISTINCT REP_ID
			  FROM V700_TERRITORY_MAPPING_DETAIL
			  WHERE REP_COMPID NOT IN
			    (SELECT C906_RULE_ID
			    FROM T906_RULES
			    WHERE C906_RULE_GRP_ID = 'ADDNL_COMP_IN_SALES'
			    AND C906_VOID_FL      IS NULL
			    )
			  )
			AND (t501.c704_account_id = p_account_id OR			 			
			t501.c704_account_id IN (SELECT c704_account_id
				FROM t740_gpo_account_mapping
				WHERE c101_party_id  = p_gpb_id
				AND p_account_id IS NULL));
		
		RETURN v_total_sales;
	END get_acc_last12_month_sales;
	
	/******************************************************************************
  * Author : APrasath
  * Description : Summing and populating the Last 12 Month sales of an Parent Account  or GPB
  ******************************************************************************/	
	FUNCTION get_parent_acc_12_month_sales(
		 p_party_id IN T704_account.c101_party_id%TYPE DEFAULT NULL
		,p_gpb_id IN T704_account.c101_party_id%TYPE		
	)
	RETURN NUMBER
	IS
	/*  Description     : Summing and populating the Last 12 Month sales of an Account
		Parameters      : p_account_id
	*/
	v_total_sales   NUMBER;
	BEGIN
		
		SELECT   NVL( SUM( T502.C502_CORP_ITEM_PRICE * T502.C502_ITEM_QTY),0) INTO  v_total_sales
			FROM T501_ORDER T501 ,
			  T502_ITEM_ORDER T502 ,
			  T703_SALES_REP T703 ,
			  T701_DISTRIBUTOR T701 ,
			  T704_ACCOUNT T704
			WHERE T703.C703_SALES_REP_ID             = T501.C703_SALES_REP_ID
			AND NVL(T501.C901_ORDER_TYPE,-9999) NOT IN ('2533')
			AND NVL (c901_order_type,    -9999) NOT IN
			  (SELECT t906.c906_rule_value
			  FROM t906_rules t906
			  WHERE t906.c906_rule_grp_id = 'ORDTYPE'
			  AND c906_rule_id            = 'EXCLUDE'
			  )
			AND NVL (c901_order_type, -9999) NOT IN
			  (SELECT t906.c906_rule_value
			  FROM t906_rules t906
			  WHERE t906.c906_rule_grp_id = 'ORDERTYPE'
			  AND c906_rule_id            = 'EXCLUDE'
			  )
			AND T701.C701_DISTRIBUTOR_ID = T703.C701_DISTRIBUTOR_ID
			AND T704.C704_ACCOUNT_ID     = T501.C704_ACCOUNT_ID
			AND T502.C501_ORDER_ID       = T501.C501_ORDER_ID
			AND T501.C501_VOID_FL       IS NULL
			AND T502.C502_VOID_FL       IS NULL
			AND T501.C501_DELETE_FL     IS NULL
			AND T501.C501_ORDER_DATE    >= ADD_MONTHS(TRUNC(CURRENT_DATE ,'MONTH') , -11)
			AND T501.C501_ORDER_DATE    <= CURRENT_DATE
			AND T501.C703_SALES_REP_ID  IN
			  (SELECT DISTINCT REP_ID
			  FROM V700_TERRITORY_MAPPING_DETAIL
			  WHERE REP_COMPID NOT IN
			    (SELECT C906_RULE_ID
			    FROM T906_RULES
			    WHERE C906_RULE_GRP_ID = 'ADDNL_COMP_IN_SALES'
			    AND C906_VOID_FL      IS NULL
			    )
			  )
			AND (t501.c704_account_id IN (SELECT t704.c704_account_id  FROM t704_account  t704
	   			WHERE t704.c101_party_id = p_party_id 
	   			AND t704.c704_void_fl IS NULL) OR 			
			t501.c704_account_id IN (SELECT c704_account_id
				FROM t740_gpo_account_mapping
				WHERE c101_party_id  = p_gpb_id));
		
		RETURN v_total_sales;
	END get_parent_acc_12_month_sales;
 /***********************************************************************
  * Author : Jreddy
  * Description : Fetching Account Information details
  ************************************************************************/
	PROCEDURE gm_fch_account_info_details (
		p_requestid         IN 		t9010_answer_data.c9010_ref_id%TYPE
	   ,p_account_details   OUT		TYPES.cursor_type
	)
	AS
	 v_account_id VARCHAR2(20);
	 v_party_id VARCHAR2(20);
	 v_party_gpb_id VARCHAR2(20);
       BEGIN
	       BEGIN
             SELECT c704_account_id INTO v_account_id FROM t7500_account_price_request WHERE c7500_account_price_request_id= p_requestid;
            EXCEPTION WHEN NO_DATA_FOUND
			  THEN
			    raise_application_error ('-20999', 'The entered Pricing Request Id <B>' ||p_requestid|| '</B> is invalid');	
			END;
			
			IF v_account_id IS NULL THEN
				v_party_id := '';
			ELSE
              SELECT C101_PARTY_ID INTO v_party_id FROM t704_account WHERE c704_account_id = v_account_id;
            END IF;
            
              OPEN p_account_details
              FOR
                     SELECT ac_name straccountname, ad_name stradname, region_name strregname, d_name strdname
                             , ter_name strtername, rep_name strrepname, gp_name strgpname, vp_name strvpname
                             , GET_PRICING_REQUEST_ID(v_account_id,'','') strRequestId 
                             , t704d.c901_contract contractfl
                             , get_party_name(t704d.c101_idn) strGprAff -- 1006432 Group Pricing Affiliation
                             ,    get_party_name(t704d.c101_gpo) strHlcAff -- 1006433 Healthcare Affiliation 
                             , Decode(t704d.c901_contract,NULL,'No',get_code_name (t704d.c901_contract)) contractflname
                             , get_party_name(v_party_id) strParentAcc
                             , get_party_name(get_account_gpo_id(v_account_id)) strGpb
                       		 , get_code_name(t704.c901_currency) strCurrency
                       		 , v_account_id accid
                       		 , gm_pkg_sm_pricerequest_rpt.get_acc_last12_month_sales(v_account_id,NULL) last12MonthSales
                        FROM v700_territory_mapping_detail v700, t704_account t704, t704d_account_affln t704d
                       WHERE v700.ac_id = t704.c704_account_id AND v700.ac_id = v_account_id
                       	AND t704.c704_account_id = t704d.c704_account_id (+)
                       	AND t704d.c704d_void_fl (+) IS NULL;
		
	END gm_fch_account_info_details;
	
/******************************************************************
 *Author : Jreddy
 *Description : This procedure is used to fetch question, answer, answer group
 ****************************************************************/
	PROCEDURE gm_fch_question_answer (
		p_requestid 		  IN	   t7000_account_price_request.c7000_account_price_request_id%TYPE
	  , p_outquestion		  OUT	   TYPES.cursor_type
	  , p_outanswer 		  OUT	   TYPES.cursor_type
	  , p_outanswergroup	  OUT	   TYPES.cursor_type
	  , p_outquestionanswer   OUT	   TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_outquestion
		 FOR
		SELECT   c9011_question_order questionid, c9011_question_name questionname
				 FROM t9011_question
				WHERE c9011_void_fl IS NULL AND c901_questionnair_type='52130'    -- c901_ref_type = 52130	---pricing question
			 ORDER BY c9011_question_order;

		OPEN p_outanswer
		 FOR
		SELECT   c9011_question_order questionid, c901_question_type controltype
				 FROM t9011_question
				WHERE c9011_void_fl IS NULL AND c901_questionnair_type='52130'
			 ORDER BY c9011_question_order;

		OPEN p_outanswergroup
		 FOR
		SELECT c901_code_nm listname, c901_code_id listid, c901_code_grp groupname FROM t901_code_lookup WHERE c901_code_grp='YES/NO';

		OPEN p_outquestionanswer
		 FOR
		SELECT   c901_consultant listid, c9010_answer_desc answerdesc
				 FROM t9010_answer_data
				WHERE c9010_ref_id = p_requestid AND c901_ref_type = 52130 AND c9010_void_fl IS NULL;
	END gm_fch_question_answer;
	
/******************************************************************
 *Author : Jreddy
 *Description : This procedure is used to fetch Pricing History Log
 ****************************************************************/
	PROCEDURE gm_fch_price_log_history (
		p_ref_id	   IN		t941_audit_trail_log.c941_ref_id%TYPE
	  , p_reqhistory   OUT		TYPES.cursor_type
	)
	AS
	 
	v_comp_dt_fmt VARCHAR2 (20);
	BEGIN 
	     SELECT get_compdtfmt_frm_cntx() 
	       INTO v_comp_dt_fmt 
	       FROM dual;
	       	       
		OPEN p_reqhistory
		 FOR
			SELECT t706.C705_ACCOUNT_PRICING_ID reqid, t706.C705_UNIT_PRICE rvalue, get_user_name (t706.C706_CREATED_BY) updatedby
					, TO_CHAR (t706.C706_CREATED_DATE, 'mm/dd/yyyy'||' HH24:MI:SS') update_date, t7501.c7500_account_price_request_id price_req_id,t705.C705_VOID_FL void_fl
				 FROM T706_ACCOUNT_PRICING_LOG t706, t7501_account_price_req_dtl t7501,t705_account_pricing t705 -- PC-5504 - added t705_account_pricing table for check the void flag condition in history icon
				WHERE t706.C705_ACCOUNT_PRICING_ID = p_ref_id
        	AND t706.c7501_account_price_req_dtl_id = t7501.c7501_account_price_req_dtl_id(+)
				  AND t706.C706_VOID_FL IS NULL
				  AND t7501.C7501_VOID_FL IS NULL
				  AND t705.C705_ACCOUNT_PRICING_ID = t706.C705_ACCOUNT_PRICING_ID
			 ORDER BY t706.C706_CREATED_DATE desc;
	END gm_fch_price_log_history;
	
 /******************************************************************
 *Author : Jreddy
 *Description : This procedure is used to fetch GBB comparision details
 ****************************************************************/
	PROCEDURE gm_fch_gpb_compare_dtls (
		p_requestid 		  IN	   t7000_account_price_request.c7000_account_price_request_id%TYPE
	  , p_outgpbheader		  OUT	   TYPES.cursor_type
	  , p_outpricedtls 		  OUT	   TYPES.cursor_type
	  , p_outsystemdtls	  	  OUT	   TYPES.cursor_type
	)
	AS
	BEGIN
		--GPB Header Details
		OPEN p_outgpbheader
		 FOR
		SELECT t7551.c101_gpb_id gpbid,t101.c101_short_nm gpbname,
			t7580.c7580_color_code colorcode,t7580.c7580_check_fl checkflag
			FROM t7553_gpo_ref_price_req_map t7553,
		    t7551_gpo_reference_detail t7551,
		    t7580_price_gpb_mapping t7580,
        	t101_party t101
		    WHERE t7553.c7551_gpo_reference_detail_id = t7551.c7551_gpo_reference_detail_id
		    AND t7580.c101_gpb_id = t7551.c101_gpb_id
        	AND t101.c101_party_id = t7551.c101_gpb_id
        	AND t101.c101_party_id = t7580.c101_gpb_id
		    AND t7580.C7580_VOID_FL IS NULL
		    AND t7553.C7553_VOID_FL IS NULL
		    AND t7551.c7551_void_fl IS NULL
        	AND t101.c101_void_fl IS NULL
			AND t7553.c7500_account_price_request_id = p_requestid 
			ORDER BY t7580.c7580_display_order_id;
		
		--GPB price details Details
		OPEN p_outpricedtls
		 FOR
		SELECT t7551.c101_gpb_id gpbid,t7501.c7501_account_price_req_dtl_id reqdtlid,
		  t7552.c7552_unit_price unitprice,
		  t7552.c205_part_number_id pmumber,
		  t7552.c4010_group_id grpid,
		  t7580.c7580_color_code colorcd,
		  t7580.c7580_check_fl checkfl,
		  NVL(t7500.C901_CURRENCY,1) CURRENCY,
          nvl(t7500.C901_CUR_VALUE,1) CURR_VAL
		FROM t7500_account_price_request t7500,
		(SELECT t7501.c7501_account_price_req_dtl_id,t7501.C7500_ACCOUNT_PRICE_REQUEST_ID,t7501.c205_part_number_id,t7501.c4010_group_id, 1 pricereqsort from t7501_account_price_req_dtl t7501
		WHERE  T7501.C7501_VOID_FL IS NULL and t7501.c7500_account_price_request_id = p_requestid
		 UNION ALL
		SELECT t7503.c7503_compare_price_req_dtl_id,t7503.C7500_ACCOUNT_PRICE_REQUEST_ID,t7503.c205_part_number_id,t7503.c4010_group_id, 2 pricereqsort from t7503_compare_price_req_detail t7503
		WHERE  t7503.C7503_VOID_FL IS NULL and t7503.c7500_account_price_request_id = p_requestid ) t7501,
		   t7552_gpo_reference_price t7552,
		    t7553_gpo_ref_price_req_map t7553,
		    t7551_gpo_reference_detail t7551,
		    t7580_price_gpb_mapping t7580
		  WHERE t7553.c7551_gpo_reference_detail_id = t7552.c7551_gpo_reference_detail_id
		  AND t7552.c7551_gpo_reference_detail_id   = t7551.c7551_gpo_reference_detail_id  
		  AND t7553.c7500_account_price_request_id  = t7500.c7500_account_price_request_id 
		  AND t7501.C7500_ACCOUNT_PRICE_REQUEST_ID              = T7500.C7500_ACCOUNT_PRICE_REQUEST_ID
		  AND NVL(t7501.c205_part_number_id,t7501.c4010_group_id) = NVL(t7552.c205_part_number_id,t7552.c4010_group_id)
		  AND t7501.c4010_group_id = t7552.c4010_group_id
		  AND t7580.c101_gpb_id = t7551.c101_gpb_id
		  AND t7500.c7500_void_fl IS NULL
		  AND t7552.C7552_VOID_FL IS NULL
		  AND t7553.C7553_VOID_FL IS NULL
		  AND t7551.c7551_void_fl IS NULL
		  AND t7580.C7580_VOID_FL IS NULL
		AND t7500.c7500_account_price_request_id = p_requestid
		ORDER BY t7580.c7580_display_order_id,pricereqsort,t7501.c7501_account_price_req_dtl_id;

		--GPB adding new systems
		OPEN p_outsystemdtls
		 FOR
		SELECT t7501.c7501_account_price_req_dtl_id prcdetlid,
		  t7501.C207_SET_ID SETID,
		  t207.c207_set_nm SET_NAME,
		  (
		  CASE
		    WHEN t7501.C901_CONSTRUCT_LEVEL <> 0
		    THEN t7003.c7003_construct_name
		    WHEN t4010.c4010_specl_group_fl = 'Y'
		    THEN 'Speciality Group'
		    ELSE GET_CODE_NAME(t4010.C901_GROUP_TYPE)
		  END) SETNAME,
		  t7501.C901_CONSTRUCT_LEVEL CONSTRUCTLEVEL,
		  t7501.C4010_GROUP_ID GROUPID,
		  DECODE(t7501.c205_part_number_id, NULL,t4010.c4010_group_nm,(t7501.c205_part_number_id
		  || ' - '
		  || t205.c205_part_num_desc)) GROUPNAME,
		  t4010.C901_GROUP_TYPE GROUPTYPE,
		  GET_CODE_NAME(t4010.C901_GROUP_TYPE) GROUPTYPEDESC,
		  t7501.c205_part_number_id PARTNUM,
		  t205.c205_part_num_desc PARTDESC,
		  DECODE(t7501.C7501_QTY,'0','1',t7501.C7501_QTY) QUANTITY,
		  t7501.C7501_LIST_PRICE UNITLISTPRICE,
		  t7501.C7501_TRIPWIRE_PRICE UNITTRIPWIRE,
		  gm_pkg_sm_pricerequest_rpt.GET_EXTENSION_PRICE(t7501.C7501_TRIPWIRE_PRICE,t7501.C7501_QTY) TRIPWIREEXTENSIONPRICE,
		  t7501.C7501_CURRENT_PRICE UNITCURRENTPRICE,
		  t7501.C7501_PROPOSED_PRICE PROPOSEDUNITPRICE,
		  DECODE(t7501.C7003_PRINCIPAL_FL,'Y','YES','N','NO',NULL) PRINCIPALFLAG,
		  t4010.c4010_specl_group_fl SPECLGRPFLAG,
		  t7501.c7500_account_price_request_id PRICEREQID,
		  DECODE(NVL(0,'0'),'0',(DECODE(t7501.C7501_PROPOSED_PRICE,NULL,'0',ROUND((((t7501.C7501_LIST_PRICE - t7501.C7501_PROPOSED_PRICE) / t7501.C7501_LIST_PRICE) * 100),2))) , (DECODE(t7501.C7501_PROPOSED_PRICE,NULL,'0',( ROUND(((t7501.C7501_LIST_PRICE - DECODE(NVL(0,'0'),'0','0',(t7501.C7501_PROPOSED_PRICE - (t7501.C7501_PROPOSED_PRICE * NVL(0,'0') /100))))/t7501.C7501_LIST_PRICE ) * 100,2)
		  || ' %'))) )listPriceOffList
		  ,
		  t207.c207_set_nm SETNM ,
		  (
		  CASE
		    WHEN t7501.C901_CONSTRUCT_LEVEL <> 0
		    THEN 1
		    WHEN t4010.c4010_specl_group_fl = 'Y'
		    THEN 2
		    ELSE 3
		  END) SETORDCNT,
		  NVL(t7500.C901_CURRENCY,1) CURRENCY,
          nvl(t7500.C901_CUR_VALUE,1) CURR_VAL
		FROM
		  (SELECT t7501.c7501_account_price_req_dtl_id,
		    t7501.C7500_ACCOUNT_PRICE_REQUEST_ID,
		    t7501.c205_part_number_id,
		    t7501.c4010_group_id,
		    1 pricereqsort,
		    t7501.C207_SET_ID,
		    t7501.C901_CONSTRUCT_LEVEL,
		    t7501.C7501_QTY,
		    t7501.C7501_LIST_PRICE,
		    t7501.C7501_TRIPWIRE_PRICE,
		    t7501.C7501_CURRENT_PRICE,
		    t7501.C7501_PROPOSED_PRICE,
		    t7501.C7003_PRINCIPAL_FL,
		    t7501.c7003_system_construct_id
		  FROM t7501_account_price_req_dtl t7501
		  WHERE T7501.C7501_VOID_FL               IS NULL
		  AND t7501.c7500_account_price_request_id = p_requestid
		  UNION ALL
		  SELECT t7503.c7503_compare_price_req_dtl_id,
		    t7503.C7500_ACCOUNT_PRICE_REQUEST_ID,
		    t7503.c205_part_number_id,
		    t7503.c4010_group_id,
		    2 pricereqsort ,
		    t7503.C207_SET_ID,
		    t7503.C901_CONSTRUCT_LEVEL,
		    t7503.C7503_QTY,
		    t7503.C7503_LIST_PRICE,
		    t7503.C7503_TRIPWIRE_PRICE,
		    t7503.C7503_CURRENT_PRICE,
		    NULL C7503_PROPOSED_PRICE,
		    t7503.C7003_PRINCIPAL_FL,
		    t7503.c7003_system_construct_id
		  FROM t7503_compare_price_req_detail t7503
		  WHERE t7503.C7503_VOID_FL               IS NULL
		  AND t7503.c7500_account_price_request_id = p_requestid
		  ) t7501 ,
		  t4010_group t4010 ,
		  t205_part_number t205 ,
		  t207_set_master t207 ,
		  t7500_account_price_request t7500,
		  t7003_system_construct T7003
		WHERE t7501.c7500_account_price_request_id = t7500.c7500_account_price_request_id
		AND t7501.C7500_ACCOUNT_PRICE_REQUEST_ID   = p_requestid
		AND t7501.C4010_GROUP_ID                   = t4010.C4010_GROUP_ID
		AND t7501.c207_set_id                      = t207.c207_set_id
		AND t4010.c4010_void_fl                    IS NULL
		AND t7500.c7500_void_fl                    IS NULL
		AND t7501.c207_set_id                       = t7003.c207_set_id(+)
		AND t7501.c7003_system_construct_id         = t7003.c7003_system_construct_id (+)
		AND t7003.c7003_void_fl                    IS NULL
		AND NVL (t7501.c205_part_number_id, '-999') = t205.c205_part_number_id(+)
		ORDER BY pricereqsort,
		  SETID,
		  SETORDCNT,
		  SETNAME,
		  GROUPNAME;

	END gm_fch_gpb_compare_dtls;
	
/******************************************************************
* Author : Dsandeep
* Description : This procedure is used to fetch System list
 ****************************************************************/
	PROCEDURE gm_fch_systems_names (
	p_systemlist	OUT 	 TYPES.cursor_type
	)
	AS
	
		v_company_id t1900_company.C1900_COMPANY_ID%TYPE;
	
	BEGIN
	
	 	SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL'))   INTO  v_company_id FROM DUAL;
    	
		OPEN p_systemlist
		 FOR
		 SELECT   t207.c207_set_id ID, t207.c207_set_nm NAME
		    FROM t207_set_master t207, t2080_set_company_mapping t2080
		   WHERE t207.c901_set_grp_type = 1600
		  -- AND t207.c207_release_for_sale = 'Y' (This is commented due to PMT-6291 Group Pricing Changes)
		     AND c207_void_fl IS NULL
		     AND c207_obsolete_fl IS NULL
		     AND t207.c207_set_id = t2080.c207_set_id
		     AND t2080.c1900_company_id = v_company_id
		     AND t2080.c2080_void_fl IS NULL
		     AND c2080_pricing_release_fl='Y'
		ORDER BY UPPER (t207.c207_set_nm);
	END gm_fch_systems_names;
	
/******************************************************************
 *Author : Arockia prasath
 *Description : This procedure is used to fetch mapped pricing system 
 ****************************************************************/
	PROCEDURE gm_fch_price_mapped_system (
		p_requestid 		  IN	   t7000_account_price_request.c7000_account_price_request_id%TYPE
	  , p_req_map_systems     OUT	   TYPES.cursor_type
	  , p_req_systems  		  OUT 	   CLOB
	  , p_sav_systems  		  OUT 	   VARCHAR2
	  , p_req_gpb_dtls	      OUT	   TYPES.cursor_type
	)
	AS
	   v_req_systems  CLOB;
	BEGIN 
	    -- TO FETCH THE MAPPED SYSTEM   	       
		OPEN p_req_map_systems
		 FOR
		 SELECT t7571.c207_mapped_system_id sysid,
	        t207.c207_set_nm setname,
	        t7571.c7500_account_price_request_id pricereqid,
	        t7571.C7571_ALL_GROUP_FLAG grpfl
	      FROM t7571_system_ref_price_req_map t7571,
	        t207_set_master t207
	      WHERE c7500_account_price_request_id= p_requestid
	      AND c207_void_fl                   IS NULL
	      AND c207_obsolete_fl               IS NULL
	      AND t207.c207_set_id                = t7571.C207_MAPPED_SYSTEM_ID
	      AND C7571_VOID_FL                  IS NULL;
	      
	     OPEN p_req_gpb_dtls
		  FOR
		  SELECT t7551.c7551_gpo_reference_detail_id GPB_ID,
			  t101.c101_short_nm GPB_NAME,
			  DECODE(t7551.c7551_gpo_reference_detail_id,t7553.c7551_gpo_reference_detail_id,'Y','N') MAPPED_GPB
			FROM t7551_gpo_reference_detail t7551,
			  t7580_price_gpb_mapping t7580,
			  t7500_account_price_request t7500,
			  t101_party t101,
			  (SELECT c7551_gpo_reference_detail_id
			  FROM t7553_gpo_ref_price_req_map
			  WHERE c7500_account_price_request_id = p_requestid
			  AND c7553_void_fl                  IS NULL
			  ) t7553
			WHERE t7580.c101_gpb_id                 = t7551.c101_gpb_id
			AND t7500.c7550_gpo_reference_id        = t7551.c7550_gpo_reference_id
			AND t7500.c7500_account_price_request_id = p_requestid
			AND t7500.c7500_void_fl                IS NULL
			AND t7580.c101_gpb_id                   = t101.c101_party_id
			AND t7551.c7551_void_fl                IS NULL
			AND t7580.c7580_void_fl                IS NULL
			AND t101.c101_void_fl                  IS NULL
			AND t7551.c7551_gpo_reference_detail_id = t7553.c7551_gpo_reference_detail_id (+)
			ORDER BY t7580.c7580_display_order_id ;
	      
	      --USER SHOULD NOT ADD THE EXISTING SYSTEMS WHICH IN PRICING REQUEST
	      SELECT RTRIM(XMLAGG(XMLELEMENT(e,c207_set_id
		  || ',')).EXTRACT('//text()').getclobval(),',')
		   INTO v_req_systems
		    FROM t7502_account_price_req_sys WHERE c7500_account_price_req_id = p_requestid;
		    
		    p_req_systems := v_req_systems;
		    
	END gm_fch_price_mapped_system;
	

/******************************************************************
 *Author : Jreddy
 *Description : This procedure is used to send mail with price request details to AD, VP and sales rep
 ****************************************************************/
	PROCEDURE gm_fch_request_dtls (
		p_requestid 		  IN	   t7000_account_price_request.c7000_account_price_request_id%TYPE
	  , p_outdtls		  	  OUT	   TYPES.cursor_type
	  , p_rep_mail  		  OUT 	   VARCHAR2
	  , p_ad_mail  		      OUT      VARCHAR2
	  ,	p_vp_mail  		      OUT      VARCHAR2
	  , p_token_str			  OUT	   VARCHAR2
	  , p_status              OUT      VARCHAR2
	  , p_cc_email			  OUT      VARCHAR2
	)
	AS
		v_rep_id 	NUMBER;
		v_ad_id		NUMBER;
		v_vp_id		NUMBER;
		v_status 	NUMBER;
		v_rep_mail	VARCHAR2(200);
		v_ad_mail	VARCHAR2(200);
		v_vp_mail	VARCHAR2(200);	
		v_token_str	VARCHAR2(200);
	BEGIN
		
		SELECT c703_sales_rep_id, c101_ad_id, c101_vp_ad,c901_request_status  INTO v_rep_id, v_ad_id, v_vp_id,v_status FROM t7500_account_price_request WHERE c7500_account_price_request_id = p_requestid;
		OPEN p_outdtls
		 FOR
		SELECT c7500_account_price_request_id REQID, c704_account_id ACCID,
		    GET_ACCOUNT_NAME(c704_account_id) ACCNAME,
			GET_REP_NAME(c7500_initiated_by) INSTDBY,
			TO_CHAR (c7500_created_date, 'mm/dd/yyyy'||' HH24:MI:SS') INSTDDATE
			FROM t7500_account_price_request WHERE c7500_account_price_request_id = p_requestid;

			BEGIN
				SELECT c101_email_id INTO v_rep_mail
				FROM t101_user 
				WHERE c101_user_id = v_rep_id;
	   		EXCEPTION WHEN NO_DATA_FOUND THEN 
		    	v_rep_mail := NULL;
  	    	END;
  	    	BEGIN				
				SELECT c101_email_id INTO v_ad_mail 
				FROM t101_user 
				WHERE c101_user_id = v_ad_id;
	   		EXCEPTION WHEN NO_DATA_FOUND THEN 
		    	v_ad_mail := NULL;
  	    	END;
  	    	BEGIN				
				SELECT c101_email_id 
				INTO v_vp_mail 
				FROM t101_user 
				WHERE c101_user_id = v_vp_id;
	   		EXCEPTION WHEN NO_DATA_FOUND THEN 
		    	v_vp_mail := NULL;
  	    	END;
			p_rep_mail := v_rep_mail;
			p_ad_mail :=  v_ad_mail;
			p_vp_mail :=  v_vp_mail;
			p_cc_email := get_rule_value('PRT_APPR_CC','PRT_EMAIL');
			p_status := v_status;
			v_token_str := NULL;
			p_token_str := v_token_str;
	END gm_fch_request_dtls;
	

/******************************************************************
 *Author : Aprasath
 *Description : This procedure is used to send ipad notification to AD
 ****************************************************************/
	PROCEDURE gm_fch_req_notification_dtls (
		p_requestid 		  IN	   t7000_account_price_request.c7000_account_price_request_id%TYPE	  
	  , p_token_str			  OUT	   VARCHAR2
	)
	AS
		v_rep_id 	NUMBER;
		v_ad_id		NUMBER;
		v_vp_id		NUMBER;
		v_status 	NUMBER;	
		v_token_str	t9150_device_info.c9150_device_token%TYPE;
	BEGIN
		
		SELECT c703_sales_rep_id, c101_ad_id, c101_vp_ad,c901_request_status  
			INTO v_rep_id, v_ad_id, v_vp_id,v_status FROM t7500_account_price_request
				WHERE c7500_account_price_request_id = p_requestid;
		
			BEGIN
			--get v_token_str from t9150_device_info table based on AD id
		
	    	SELECT RTRIM (XMLAGG (XMLELEMENT (e, t9150.c9150_device_token || ',')) .EXTRACT ('//text()'), ',')
			INTO v_token_str
			FROM t9150_device_info t9150,
			  (SELECT t9150.c101_user_id userid,
			    MAX(t9150.c9150_last_updated_date) updateddate
			  FROM t101_user t101,
			    t9150_device_info t9150
			  WHERE t9150.c101_user_id        = t101.c101_user_id
		      AND t9150.c101_user_id = v_ad_id -- AD User ID
				  AND t9150.c9150_void_fl      IS NULL
				  GROUP BY t9150.c101_user_id
			   ) t101
				WHERE t9150.c101_user_id          = t101.userid
		      		AND t9150.c101_user_id = v_ad_id -- AD User ID
			  		AND t9150.c9150_last_updated_date = updateddate ;
	    	EXCEPTION WHEN NO_DATA_FOUND THEN 
			    	v_token_str := NULL;
  	    	END;
		
		p_token_str := v_token_str;
	END gm_fch_req_notification_dtls;
	
/***********************************************************************************
* Author : karthiks
* Description : This procedure is used to fetch the procedure based on access level
 ***********************************************************************************/
	PROCEDURE gm_fch_gpb_account_details (
	p_user_id	  IN 	   T101_USER.C101_USER_ID%TYPE
  ,	p_type		  IN	   t101_party.c901_party_type%TYPE
  , p_searchtext  IN	   VARCHAR2
  , p_gpb_acc_id   IN	t101_party.c101_party_id%TYPE
  , p_message	  OUT	   VARCHAR2
  , p_recordset   OUT	   TYPES.cursor_type
	)
	AS
		v_company_id t1900_company.C1900_COMPANY_ID%TYPE;
		v_initiated_team 	VARCHAR2(20);
		v_pkg_name 		VARCHAR2(2000);
		v_exec_string  		VARCHAR2(2000);
	BEGIN
	
	 	SELECT get_compid_frm_cntx()  
	 		INTO  v_company_id 
	 		FROM DUAL;

	 		/* Procedure for getting the team who initiated the Price Request
	 		 */
	 	gm_pkg_sm_price_approval_txn.gm_get_initiated_team(p_user_id,v_company_id,v_initiated_team);
	 	SELECT C906_RULE_VALUE 
	 			INTO v_pkg_name
	 		FROM T906_RULES 
	 		WHERE C906_RULE_ID = v_initiated_team
	 		AND C906_RULE_GRP_ID = 'PRT_GPB_ACCOUNT';

	 		IF v_pkg_name IS NULL
			THEN
				GM_RAISE_APPLICATION_ERROR('-20999','58',v_initiated_team);
			END IF;
			IF v_pkg_name IS NOT NULL
			THEN
				v_exec_string := 'BEGIN ' || v_pkg_name || '(:p_user_id,:p_type,:p_searchtext,:p_gpb_acc_id,:p_message,:p_recordset); END;'; 
				EXECUTE IMMEDIATE v_exec_string using p_user_id ,p_type,p_searchtext,p_gpb_acc_id,out p_message, out p_recordset;			
			END IF;
	
	END gm_fch_gpb_account_details;
	
/***********************************************************************************
* Author : karthiks
* Description : This procedure is used to fetch GPB based on access level
 ***********************************************************************************/
	PROCEDURE gm_fch_sales_gpb_account_dtl (
	p_user_id	  IN 	 T101_USER.C101_USER_ID%TYPE
  ,	p_type		  IN	   t101_party.c901_party_type%TYPE
  , p_searchtext  IN	   VARCHAR2
  , p_gpb_acc_id   IN	t101_party.c101_party_id%TYPE
  , p_message	  OUT	   VARCHAR2
  , p_recordset   OUT	   TYPES.cursor_type
	)
	AS
	v_portal_comp_id t101_party.c1900_company_id%TYPE;
	BEGIN

	SELECT get_compid_frm_cntx() 
		INTO v_portal_comp_id 
			FROM DUAL;
		
	OPEN p_recordset
		 FOR
		SELECT c101_party_id ID ,
		 	 DECODE (c101_last_nm
		     || c101_first_nm , NULL, c101_party_nm , c101_last_nm
		     || ' '
		     || c101_first_nm ) NAME,
		     c1900_company_id portal_company_id
		FROM t101_party
			WHERE c901_party_type = p_type
		AND c101_delete_fl   IS NULL
		AND c1900_company_id  = NVL(v_portal_comp_id, c1900_company_id)
		AND c101_void_fl     IS NULL
		AND (REGEXP_like(c101_party_nm,p_searchtext,'i') OR c101_party_id = p_gpb_acc_id)
		AND c101_active_fl IS NULL
		AND c101_party_id  IN
		  (SELECT t740.c101_party_id
		  FROM t703_sales_rep t703,
		    t101_user t101a,
		    v700_territory_mapping_detail v700,
		    t101_party t101,
		    t740_gpo_account_mapping t740
		  WHERE t703.c101_party_id  = t101a.c101_party_id
		  AND t703.c703_void_fl    IS NULL
		  AND t101a.c101_user_id    = p_user_id
		  AND t101a.c101_party_id   = t101.c101_party_id
		  AND (v700.rep_id          = t703.c703_sales_rep_id
		  OR v700.ad_id             = t101a.c101_user_id
		  OR v700.vp_id             = t101a.c101_user_id)
		  AND t740.c704_account_id  = v700.ac_id
		  AND t101.c1900_company_id = v_portal_comp_id
		  AND t101.c101_void_fl    IS NULL
		  AND t101.c101_active_fl  IS NULL
		  AND t740.c101_party_id   IS NOT NULL
		  GROUP BY t740.c101_party_id
		  );
  	EXCEPTION WHEN OTHERS
		THEN
			p_message	:= '1000';
   END gm_fch_sales_gpb_account_dtl;
   
    
/*************************************************************************************************
* Author : karthiks
* Description : This procedure is used to fetch the Price request salesrep id in Initiated Status
 *************************************************************************************************/
   
   PROCEDURE gm_fch_pr_pending_init_repid (
	p_out_initiated_req		OUT   TYPES.cursor_type
	)
	AS
	BEGIN
		
	OPEN p_out_initiated_req
		 FOR
		SELECT DISTINCT T7500.C703_SALES_REP_ID REPID,
        	DECODE(T7500.C7500_INITIATED_BY ,T7500.C101_AD_ID ,GET_USER_EMAIL_ADDRESS(T7500.C101_AD_ID),get_cs_ship_email('4121',T7500.C703_SALES_REP_ID)) REPEMAIL
		 FROM T7500_ACCOUNT_PRICE_REQUEST t7500
		WHERE C901_REQUEST_STATUS = '52186' --INITIATED
		AND C7500_VOID_FL IS NULL;
  
   END gm_fch_pr_pending_init_repid;
   
 
/**********************************************************************************************
* Author : karthiks
* Description : This procedure is used to fetch the Price request details in Initiated Status
 **********************************************************************************************/
   
   PROCEDURE gm_fch_pr_pending_init_req (
   	p_rep_id				IN   T7500_ACCOUNT_PRICE_REQUEST.C703_SALES_REP_ID%TYPE ,
	p_out_initiated_req		OUT   TYPES.cursor_type
	)
	AS
	v_company_id        T1900_COMPANY.C1900_COMPANY_ID%TYPE;
	v_date_fmt          VARCHAR2(20);
	BEGIN
		SELECT get_compid_frm_cntx()
	   		INTO v_company_id 
		FROM DUAL;
		
		SELECT get_company_dtfmt(v_company_id) 
			INTO v_date_fmt 
		FROM DUAL;
		
	OPEN p_out_initiated_req
		 FOR
		SELECT T7500.C7500_ACCOUNT_PRICE_REQUEST_ID PRID ,
			   DECODE(T7500.c704_account_id , NULL , (get_party_name(T7500.c101_gpb_id)) , get_account_name(T7500.c704_account_id) ) ACCOUNTNAME ,
        	    to_char(T7500.C7500_INITIATED_DATE,v_date_fmt) INITIATEDDATE
		 FROM T7500_ACCOUNT_PRICE_REQUEST t7500 
		WHERE C901_REQUEST_STATUS = '52186' --INITIATED
		AND T7500.C703_SALES_REP_ID = p_rep_id
		AND C7500_VOID_FL        IS NULL
		ORDER BY C7500_INITIATED_DATE DESC;
  
   END gm_fch_pr_pending_init_req;
   
 /*************************************************************************************************
* Author : karthiks
* Description : This procedure is used to fetch the Price request AD id in Pending AD Approval status
 ****************************************************************************************************/
   
   PROCEDURE gm_fch_pending_approval_ad_id (
	p_out_initiated_req		OUT   TYPES.cursor_type
	)
	AS
	BEGIN
		
	OPEN p_out_initiated_req
		 FOR
		SELECT DISTINCT NVL(T7500.C101_AD_ID ,T7500.C101_VP_AD) ADID,
  				NVL(GET_USER_EMAIL_ADDRESS(T7500.C101_AD_ID),GET_USER_EMAIL_ADDRESS(T7500.C101_VP_AD)) ADVPEMAIL
         FROM T7500_ACCOUNT_PRICE_REQUEST T7500 
		WHERE C901_REQUEST_STATUS = '52187' --PENDING AD APPROVAL
		 AND C7500_VOID_FL        IS NULL;
  
   END gm_fch_pending_approval_ad_id;
   
 /*************************************************************************************************
* Author : karthiks
* Description : This procedure is used to fetch the Price request in Pending AD Approval Status
 ****************************************************************************************************/
   
   PROCEDURE gm_fch_pr_pending_aprvl_req (
   	p_ad_id 				IN   T7500_ACCOUNT_PRICE_REQUEST.C101_AD_ID%TYPE ,
	p_out_initiated_req		OUT   TYPES.cursor_type
	)
	AS
	v_company_id        T1900_COMPANY.C1900_COMPANY_ID%TYPE;
	v_date_fmt          VARCHAR2(20);
	BEGIN
		
		SELECT get_compid_frm_cntx()
	   		INTO v_company_id 
		FROM DUAL;
		
		SELECT get_company_dtfmt(v_company_id) 
			INTO v_date_fmt 
		FROM DUAL;
		
	OPEN p_out_initiated_req
		 FOR
		SELECT  T7500.C7500_ACCOUNT_PRICE_REQUEST_ID PRID ,
				DECODE(T7500.c704_account_id , NULL , (get_party_name(T7500.c101_gpb_id)) , get_account_name(T7500.c704_account_id) ) ACCOUNTNAME ,
       	 		GET_USER_NAME(T7500.C7500_INITIATED_BY) INITIATEDBY,
       	    	to_char(T7500.C7500_INITIATED_DATE,v_date_fmt) INITIATEDDATE
		 FROM T7500_ACCOUNT_PRICE_REQUEST T7500
		WHERE C901_REQUEST_STATUS = '52187' --PENDING AD APPROVAL
		 AND (T7500.C101_AD_ID = p_ad_id OR (T7500.C101_VP_AD = p_ad_id AND T7500.C101_AD_ID IS NULL))
		 AND C7500_VOID_FL        IS NULL
		 ORDER BY C7500_INITIATED_DATE DESC;
  
   END gm_fch_pr_pending_aprvl_req;
   
/*************************************************************************************************
* Author : Suganthi
* Description : This procedure is used to fetch the Change Price request in Modify Order screen
 ****************************************************************************************************/   
PROCEDURE GM_FCH_ORDER_PRICE_REQ(
  P_ORD_ID     in  T7503_ACCOUNT_PRICE_REQ_ORDER.C7500_ACCOUNT_PRICE_REQ_ID%type,
  P_OUT_PR_CUR  OUT types.CURSOR_TYPE
  )
  AS
    v_company_id        T1900_COMPANY.C1900_COMPANY_ID%TYPE;
	v_date_fmt          VARCHAR2(20);
  BEGIN
  
	  SELECT get_compid_frm_cntx()
	   		INTO v_company_id 
		FROM DUAL;
		
		SELECT get_company_dtfmt(v_company_id) 
			INTO v_date_fmt 
		FROM DUAL;
		
  OPEN P_OUT_PR_CUR FOR
        SELECT C7500_ACCOUNT_PRICE_REQ_ID REQUESTID,
        TO_CHAR(C7503_CREATED_DATE,v_date_fmt) INITIATEDDATE,
        get_user_name(C7500_INITIATED_BY) INITIATEDBY,
        get_code_name(c901_request_status) STATUS,
        T7503.C501_ORDER_ID ORDERID
      FROM T7503_ACCOUNT_PRICE_REQ_ORDER T7503,
        T7500_ACCOUNT_PRICE_REQUEST T7500
      WHERE C7503_VOID_FL IS NULL
      AND T7500.C7500_ACCOUNT_PRICE_REQUEST_ID =  T7503.C7500_ACCOUNT_PRICE_REQ_ID
      AND T7503.C501_ORDER_ID = P_ORD_ID
      AND T7500.C7500_VOID_FL IS NULL
      ORDER BY T7503.C7500_ACCOUNT_PRICE_REQ_ID DESC;
  END GM_FCH_ORDER_PRICE_REQ;

/*************************************************************************************************
* Author : Karthik 
* Description : This function is used to get the system division
****************************************************************************************************/
FUNCTION GET_SYSTEM_DIVISION(
p_set_id IN t207_set_master.C207_SET_ID%TYPE
)
RETURN VARCHAR2
IS
/*  Description     : THIS FUNCTION RETURNS Set Division
Parameters                       : p_set_id
*/
v_div_id   VARCHAR2 (50);
BEGIN
     BEGIN
      SELECT c1910_division_id
      	 INTO v_div_id
		FROM t207_set_master
		WHERE c207_set_id = p_set_id
		AND c207_void_fl IS NULL;
     EXCEPTION
      WHEN OTHERS THEN
       RETURN 0;
      END;
     RETURN v_div_id;
	 
END GET_SYSTEM_DIVISION;
END gm_pkg_sm_pricerequest_rpt;
/