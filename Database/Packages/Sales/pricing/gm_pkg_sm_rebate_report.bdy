--@"C:\PMT\db\Packages\Sales\pricing\gm_pkg_sm_rebate_report.bdy";
CREATE OR REPLACE PACKAGE BODY gm_pkg_sm_rebate_report
IS
    /*********************************************************
    * Description : This procedure is used to fetch the rebate setup details. Based on Rebate Id or Party ID.
    *    We are using the Auto complete changes - so, in setup screen we know only Party ID.
    *
    *     1. Based on Party id to get the Rebate id (primary key) and fetch the rebate details.
    *       2. If No party id then, based on Rebate id (parameter values) to getch the rebate details.
    *
    * Author: mmuthusamy
    *********************************************************/

PROCEDURE gm_fch_rebate_management_dtls (
        p_rebate_id IN t7200_rebate_master.c7200_rebate_id%TYPE,
        p_party_id  IN t7200_rebate_master.c7200_ref_id%TYPE,
        p_out_rebate_dtls OUT TYPES.cursor_type)
AS
    v_rebate_id t7200_rebate_master.c7200_rebate_id%TYPE;
    v_date_fmt VARCHAR2(100);
BEGIN

	-- to assign the rebate id values to local variable
	
    v_rebate_id := p_rebate_id;
    
    -- checking the party id any values?
    SELECT get_compdtfmt_frm_cntx ()
       INTO v_date_fmt
       FROM dual;
      
    IF p_party_id IS NOT NULL THEN
        -- try to get the rebate id (based on party)
        BEGIN
             SELECT c7200_rebate_id
               INTO v_rebate_id
               FROM t7200_rebate_master
              WHERE c7200_ref_id = p_party_id
              AND C7200_VOID_FL IS NULL;
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_rebate_id := NULL;
        END;
        
    END IF;
    
    -- to get the rebate master details
    OPEN p_out_rebate_dtls FOR SELECT c7200_rebate_id rebateid,
    c901_grp_type grouptypeid,
    get_code_name (c901_grp_type) grouptypename,
    c7200_ref_id partyid,
    get_party_name (c7200_ref_id) searchpartyId,
    c901_rebate_part_type rebateparttypeid,
    get_code_name (c901_rebate_part_type) rebateparttypename,
    c7200_rebate_part_type_fl rebatepartfl,
    c901_rebate_category rebatecategoryid,
    get_code_name (c901_rebate_category) rebatecategoryname,
    c7200_rebate_category_fl rebatecategoryfl,
    c7200_rebate_rate rebaterate,
    c7200_rebate_rate_fl rebateratefl,
     TO_CHAR (c7200_reb_from_dt, v_date_fmt) rebatefromdate,
    TO_CHAR (c7200_reb_to_dt, v_date_fmt) rebatetodate,
   TO_CHAR (c7200_reb_eff_dt, v_date_fmt)  rebateeffectivedate,
    c7200_reb_eff_dt_fl rebateeffectivefl,
    c901_rebate_type rebatetypeid,
    get_code_name (c901_rebate_type) rebatetypename,
    c205_reb_part_number rebatepartnumber,
    c901_rebate_payment rebatepaymentid,
    get_code_name (c901_rebate_payment) rebatepaymentname,
    c7200_reb_contract_info rebatecontractinfo,
    c7200_reb_comments rebatecomments,
    c7200_tier1_percent tier1percent,
    c7200_tier1_from_amt tier1fromamt,
    c7200_tier1_to_amt tier1toamt,
    c7200_tier2_percent tier2percent,
    c7200_tier2_from_amt tier2fromamt,
    c7200_tier2_to_amt tier2toamt,
    c7200_tier3_percent tier3percent,
    c7200_tier3_from_amt tier3fromamt,
    c7200_tier3_to_amt tier3toamt,
    c7200_active_fl activefl,
    c7200_void_fl voidfl,
    c7200_last_updated_dt lastupdateddate,
    get_user_name (c7200_last_updated_by) lastupdatedby,
    c1900_company_id companyid 
    FROM t7200_rebate_master 
    WHERE c7200_rebate_id = v_rebate_id
      AND C7200_VOID_FL IS NULL;
    
    
END gm_fch_rebate_management_dtls;


/*********************************************************
* Description : This procedure is used to fetch Rebate part mapping details.
*    Based on Rebate Id - to fetch all the parts number mapping details from T7201 table.
*
* Author : mmuthusamy
*********************************************************/
PROCEDURE gm_fch_rebate_parts_mapping_dtls (
        p_rebate_id IN t7200_rebate_master.c7200_rebate_id%TYPE,
        p_out_rebate_part_dtls OUT TYPES.cursor_type)
AS
	v_date_fmt VARCHAR2 (20);
BEGIN
       
	SELECT get_compdtfmt_frm_cntx ()
       INTO v_date_fmt
       FROM dual;
    OPEN p_out_rebate_part_dtls FOR 
	 SELECT t7201.c205_part_number_id pnum,
	    t205.c205_part_num_desc part_desc,
	    t7201.c7201_effective_dt effectiveDate,
	    t101.C101_USER_F_NAME ||' '||t101.C101_USER_L_NAME  last_updated_by,
	    TO_CHAR ( t7201.c7201_last_updated_dt , v_date_fmt ||' HH:MI AM') last_updated_date
	    FROM 
	    t7201_rebate_parts t7201,
	    t205_part_number t205,t101_user t101 
	    WHERE t7201.c7200_rebate_id   = t7201.c7200_rebate_id
	    AND t205.c205_part_number_id = t7201.c205_part_number_id
        AND t101.c101_user_id = t7201.c7201_last_updated_by
	    AND t7201.c7200_rebate_id = p_rebate_id
	    AND t7201.c7201_void_fl IS NULL;
    
END gm_fch_rebate_parts_mapping_dtls;


	/*********************************************************
	* Description : Procedure used to fetch the rebate audit trail details
	*
	*Author : mmuthusamy
	*********************************************************/
	PROCEDURE gm_fch_rebate_log_dtls (
	        p_rebate_id IN t7202_rebate_log.c7200_rebate_id%TYPE,
	        p_type      IN t7202_rebate_log.c901_type%TYPE,
	        p_date_fmt  IN VARCHAR2,
	        p_out_log_cur OUT TYPES.cursor_type)
	        
	AS
	
	    v_comp_dt_fmt VARCHAR2 (20) ;
	    
	BEGIN
		
	    OPEN p_out_log_cur FOR
	    
	    SELECT t7202.c7200_rebate_id reqid,
		    t7202.c7202_rebate_value rvalue,
		    get_user_name (t7202.c7202_last_updated_by) updatedby,
		    TO_CHAR (t7202.c7202_last_updated_dt, p_date_fmt||' HH24:MI:SS AM') update_date
	    FROM t7202_rebate_log t7202
	    WHERE t7202.c7200_rebate_id = p_rebate_id
	    AND t7202.c901_type = p_type
	    AND t7202.c7202_void_fl IS NULL
	    ORDER BY t7202.c7202_last_updated_dt DESC;
	    
	END gm_fch_rebate_log_dtls;
	
    /*********************************************************
	* Description : Procedure used to fetch the rebate audit trail details
	*
	*Author : mmuthusamy
	*********************************************************/
   PROCEDURE gm_fch_rebate_management_report(
            p_rebate_acc_type     IN t7200_rebate_master.c901_grp_type%TYPE,
            p_party_id            IN t7200_rebate_master.c7200_ref_id%TYPE,
            p_rebate_type         IN t7200_rebate_master.c901_rebate_type%TYPE,
            p_rebate_category     IN t7200_rebate_master.c901_rebate_category%TYPE,
            p_rebate_from_rate    IN t7200_rebate_master.c7200_rebate_rate%TYPE,
            p_rebate_to_rate      IN t7200_rebate_master.c7200_rebate_rate%TYPE,
            p_rebate_part_type    IN t7200_rebate_master.c901_rebate_part_type%TYPE,
            p_date_Type           IN  VARCHAR2,
            p_rebate_frm_date     IN VARCHAR2,
            p_rebate_to_date      IN VARCHAR2,
            p_company_id          IN t7200_rebate_master.c1900_company_id%TYPE,
            p_out_rebate_report   OUT CLOB
           ) 
       AS
         v_date_fmt VARCHAR2(100);
         v_company_id    t1900_company.c1900_company_id%TYPE;
     BEGIN
		 SELECT get_compdtfmt_frm_cntx ()
		       INTO v_date_fmt
		       FROM dual;
	
 SELECT JSON_ARRAYAGG(JSON_OBJECT(
 
   'rebate_id' VALUE c7200_rebate_id,
   'grouptypeid' VALUE c901_grp_type ,
   'grouptypename' VALUE get_code_name (c901_grp_type) ,
   'partyid' VALUE c7200_ref_id ,
   'partyname' VALUE get_party_name (c7200_ref_id) , 
   'rebateparttypeid' VALUE c901_rebate_part_type ,
   'rebateparttypename' VALUE get_code_name (c901_rebate_part_type) ,
   'rebatepartfl' VALUE c7200_rebate_part_type_fl ,
   'rebatecategoryid' VALUE c901_rebate_category ,
   'rebatecategoryname' VALUE get_code_name (c901_rebate_category) ,
   'rebatecategoryfl' VALUE c7200_rebate_category_fl ,
   'rebaterate' VALUE c7200_rebate_rate ,
   'rebateratefl' VALUE c7200_rebate_rate_fl ,
   'rebatefromdate' VALUE TO_CHAR (c7200_reb_from_dt, v_date_fmt) ,
   'rebatetodate' VALUE TO_CHAR (c7200_reb_to_dt, v_date_fmt) ,
   'rebateeffectivedate' VALUE TO_CHAR (c7200_reb_eff_dt, v_date_fmt) , 
   'rebateeffectivefl' VALUE c7200_reb_eff_dt_fl , 
   'rebatetypeid' VALUE c901_rebate_type ,
  'rebatetypename' VALUE get_code_name (c901_rebate_type) ,
  'rebatepartnumber' VALUE c205_reb_part_number ,
  'rebatepaymentid' VALUE c901_rebate_payment ,
  'rebatepaymentname' VALUE get_code_name (c901_rebate_payment) ,
  'rebatecontractinfo' VALUE c7200_reb_contract_info ,
  'rebatecomments' VALUE c7200_reb_comments , 
  'tier1percent' VALUE to_char(c7200_tier1_percent) ,
  'tier1fromamt' VALUE c7200_tier1_from_amt , 
  'tier1toamt' VALUE c7200_tier1_to_amt ,
  'tier2percent' VALUE to_char(c7200_tier2_percent) ,
  'tier2fromamt' VALUE c7200_tier2_from_amt ,
  'tier2toamt' VALUE c7200_tier2_to_amt ,
  'tier3percent' VALUE to_char(c7200_tier3_percent) ,
  'tier3fromamt' VALUE c7200_tier3_from_amt ,
  'tier3toamt' VALUE c7200_tier3_to_amt ,
  'activefl' VALUE c7200_active_fl ,
  'lastupdateddate' VALUE TO_CHAR(c7200_last_updated_dt,v_date_fmt) ,
  'lastupdatedby' VALUE get_user_name (c7200_last_updated_by) ,
  'companyid' VALUE c1900_company_id,
  'activefl' VALUE  NVL(c7200_active_fl ,'N'))
  order by c901_grp_type, get_party_name (c7200_ref_id)
  RETURNING CLOB) 
  INTO p_out_rebate_report
   FROM t7200_rebate_master
  WHERE c1900_company_id = p_company_id
     AND c901_grp_type = DECODE (p_rebate_acc_type,0,c901_grp_type, p_rebate_acc_type) -- Account type
     AND c7200_ref_id = decode (p_party_id,null,c7200_ref_id, p_party_id)  -- party id
     AND c901_rebate_type =  decode (p_rebate_type,0,c901_rebate_type, p_rebate_type) -- rebate type
     AND c901_rebate_category = decode (p_rebate_category,0,c901_rebate_category, p_rebate_category) -- rebate catorgory
     AND c7200_rebate_rate between decode (p_rebate_from_rate,null,c7200_rebate_rate, p_rebate_from_rate) and decode
         (p_rebate_to_rate,null,c7200_ref_id, p_rebate_to_rate)
     AND c901_rebate_part_type =decode (p_rebate_part_type,0,c901_rebate_part_type, p_rebate_part_type) -- rebate part type
     AND decode(p_date_Type,'107720',c7200_reb_to_dt,'107721', c7200_reb_eff_dt, trunc(current_date)) >=  NVL(to_date(p_rebate_frm_date,v_date_fmt), trunc(CURRENT_DATE))
     AND decode(p_date_Type,'107720',c7200_reb_to_dt,'107721', c7200_reb_eff_dt, trunc(current_date)) <=  NVL(to_date(p_rebate_to_date,v_date_fmt), trunc(CURRENT_DATE))
     AND C7200_VOID_FL IS NULL;
END gm_fch_rebate_management_report;

	/*********************************************************
	* Description : Procedure used to fetch the rebate audit trail details
	*
	*Author : mmuthusamy
	*********************************************************/
 PROCEDURE gm_fch_rebate_part_reports_dtls(
            p_rebate_acc_type     IN  t7200_rebate_master.c901_grp_type%TYPE,
            p_party_id            IN  t7200_rebate_master.c7200_ref_id%TYPE,
            p_date_Type           IN  VARCHAR2,
            p_rebate_frm_date     IN  VARCHAR2,
            p_rebate_to_date      IN  VARCHAR2,
            p_part_number         IN  T7201_REBATE_PARTS.c205_part_number_id%TYPE,
            p_company_id         IN t7200_rebate_master.c1900_company_id%TYPE,
            p_out_part_details    OUT CLOB
  ) 
  AS
          v_date_fmt VARCHAR2(100);
      
        
   BEGIN
	 
	     SELECT get_compdtfmt_frm_cntx ()
		       INTO v_date_fmt
		       FROM dual;
		       
 SELECT JSON_ARRAYAGG(JSON_OBJECT(
 
   'rebate_id' VALUE t7200.c7200_rebate_id,
    'grouptypename' VALUE get_code_name (t7200.c901_grp_type) ,
    'partyname' VALUE get_party_name (t7200.c7200_ref_id) , 
    'rebateparttypename' VALUE get_code_name (t7200.c901_rebate_part_type) ,
    'rebatecategoryname' VALUE get_code_name (t7200.c901_rebate_category) ,
    'rebatetodate' VALUE TO_CHAR (t7200.c7200_reb_to_dt, v_date_fmt) ,
    'rebateeffectivedate' VALUE TO_CHAR (t7201.c7201_effective_dt, v_date_fmt) , 
    'rebatetypename' VALUE get_code_name (t7200.c901_rebate_type) ,
    'pnum' VALUE t7201.c205_part_number_id, 
    'lastupdateddate' VALUE  TO_CHAR(t7201.c7201_last_updated_dt,v_date_fmt),
    'lastupdatedby' VALUE get_user_name (t7201.c7201_last_updated_by)  )
     order by get_party_name (t7200.c7200_ref_id), t7201.C205_PART_NUMBER_ID
      RETURNING CLOB) 
     INTO p_out_part_details
		FROM t7200_rebate_master t7200, T7201_REBATE_PARTS t7201 
		WHERE t7200.C7200_REBATE_ID       = t7201.C7200_REBATE_ID
		    AND t7200.c1900_company_id          = p_company_id
		    AND t7200.c901_grp_type         = DECODE (p_rebate_acc_type,0,t7200.c901_grp_type, p_rebate_acc_type)
		    AND t7200.c7200_ref_id          = decode (p_party_id,null,t7200.c7200_ref_id, p_party_id)
		 AND REGEXP_LIKE(t7201.C205_PART_NUMBER_ID , CASE WHEN (p_part_number) IS NOT NULL THEN (REGEXP_REPLACE(p_part_number,'[+]','\+')) ELSE REGEXP_REPLACE(t7201.c205_part_number_id,'[+]','\+') END)
		    AND decode(p_date_Type,'107720',t7200.c7200_reb_to_dt,'107721', t7201.c7201_effective_dt,trunc(CURRENT_DATE)) >= NVL(to_date(p_rebate_frm_date,v_date_fmt), trunc(CURRENT_DATE))
            AND decode(p_date_Type,'107720',t7200.c7200_reb_to_dt,'107721', t7201.c7201_effective_dt,trunc(CURRENT_DATE)) <= NVL(to_date(p_rebate_to_date,v_date_fmt), trunc(CURRENT_DATE))
		    AND t7200.C7200_VOID_FL IS  NULL
		    AND t7201.C7201_VOID_FL IS  NULL;
		  
		
END gm_fch_rebate_part_reports_dtls;

/*********************************************************
	* Description : Procedure used to fetch the rebate expiry details
	*
	*Author : T.S. Ramachandiran
	*********************************************************/

PROCEDURE gm_fch_rebate_expiry_details(
		p_range                      IN  VARCHAR2,
		p_company_id                 IN  t7200_rebate_master.c1900_company_id%TYPE,
		p_out_rebate_expiry_details  OUT TYPES.cursor_type)
		
	AS
		v_date_fmt VARCHAR2 (20);
	    
	BEGIN
		
		SELECT get_compdtfmt_frm_cntx ()
       INTO v_date_fmt
       FROM dual;
		
		OPEN p_out_rebate_expiry_details FOR
		SELECT
			get_code_name (C901_grp_type) grouptypename,
			get_party_name (C7200_ref_id) partyname, 
			get_code_name (C901_rebate_type) rebatetypename,
			get_code_name (C901_rebate_part_type) rebateparttypename,
			get_code_name (C901_rebate_category) rebatecategoryname,
			C7200_rebate_rate rebaterate,
			TO_CHAR(C7200_reb_eff_dt, v_date_fmt)rebateeffectivedate,
			TO_CHAR(C7200_reb_from_Dt, v_date_fmt) rebatefromdate,
			TO_CHAR(c7200_reb_to_dt, v_date_fmt) rebatetodate
		FROM t7200_rebate_master
		WHERE C1900_company_id = p_company_id
		AND c7200_reb_to_dt   = TRUNC(CURRENT_DATE)+p_range
		AND C7200_Void_Fl Is Null
		AND c7200_active_fl='Y';
				
	END gm_fch_rebate_expiry_details;	




END gm_pkg_sm_rebate_report;
/