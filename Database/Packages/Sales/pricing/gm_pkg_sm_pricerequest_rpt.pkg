create or replace
PACKAGE gm_pkg_sm_pricerequest_rpt
IS

/***********************************************************************
  * Author : Jreddy
  * Description : Fetching threshold Question Details
  ************************************************************************/
	PROCEDURE gm_fch_questionnair_details (
		p_requestid         IN 		t9010_answer_data.c9010_ref_id%TYPE
	   ,p_questiondetails   OUT		TYPES.cursor_type
	);
	   
/***********************************************************************
  * Author : Jreddy
  * Description : Fetching Group Part Map Details
  ************************************************************************/	
PROCEDURE gm_fch_group_part_details (
		 p_groupid 			  IN		VARCHAR2
	    ,p_grouppartdetails   OUT		TYPES.cursor_type
	);

 /*******************************************************
    * Description : This procedure used to get the all group details of a system
    * Author   :    Tejeswara Reddy Chappidi
    *******************************************************/
   PROCEDURE gm_fch_construct_details(
        p_company_id           IN     T704_ACCOUNT.C901_COMPANY_ID%TYPE,
        p_accparty_id          IN     t704_account.c704_Account_id%TYPE,
        p_select_systems       IN     CLOB,
        p_type_id              IN     VARCHAR2,
        p_group_cur            OUT    TYPES.cursor_type
    );
    
  /***********************************************************************
  * Author : HReddi
  * Description : Procedure for fetching the Price Request Dash Board details
  ************************************************************************/
	PROCEDURE gm_fch_price_report_details (
		 p_status_id 		  IN		CLOB
		,p_from_date		  IN		VARCHAR2
		,p_to_date			  IN		VARCHAR2
		,p_company_id		  IN		VARCHAR2
		,p_filter_reps		  IN	    CLOB
		,p_user_id			  IN        VARCHAR2
	    ,p_pricereportdetails OUT		TYPES.cursor_type
	);
    
/***********************************************************************
  * Author : Tejeswara Reddy Chappidi
  * Description : Fetching the submitted price request id details
  ************************************************************************/
	PROCEDURE gm_fch_selected_request_dtls (
		p_requestid         IN 	T7500_ACCOUNT_PRICE_REQUEST.C7500_ACCOUNT_PRICE_REQUEST_ID%TYPE
		,p_filter_reps		  IN	    CLOB
		,p_user_id            IN        VARCHAR2
		,p_systemdetails OUT		TYPES.cursor_type
		,p_groupdetails OUT		TYPES.cursor_type
		,p_requestdetails OUT		TYPES.cursor_type
	);

/***********************************************************************
  * Author : Jreddy
  * Description : Fetching Account Name and Party Name
  ************************************************************************/
	PROCEDURE gm_fch_account_name (
		 p_acc_id 		  	IN		VARCHAR2
		,p_type_id 		  	IN		VARCHAR2
		,p_rep_id			IN      VARCHAR2
		,p_condition		IN      VARCHAR2
		,p_out_cursor	 	OUT		TYPES.cursor_type
	);

/***********************************************************************
  * Author : treddy
  * Description : calculating tripwire EXTENSION PRICE
  ************************************************************************/	
FUNCTION GET_EXTENSION_PRICE(
	p_unit_price IN T7010_GROUP_PRICE_DETAIL.C7010_PRICE%TYPE,
 	p_qty IN NUMBER
)
RETURN VARCHAR2;

/***********************************************************************
  * Author : matthew
  * Description : get unit current price
  ************************************************************************/	
FUNCTION GET_GROUP_CURRENT_PRICE
(
   p_party_id in T7540_account_group_pricing.c101_party_id%TYPE,
   p_set_Id in T7540_account_group_pricing.c207_set_id%TYPE,
   p_group_id in t7540_account_group_pricing.c4010_group_id%TYPE,
   p_part_number_id in t7540_account_group_pricing.c205_part_number_id%TYPE
)
RETURN VARCHAR2;

/******************************************************************
* Author : Jreddy
* Description : This procedure is used to fetch System list
 ****************************************************************/
	PROCEDURE gm_fch_systems_list (
		p_searchtext	IN		 VARCHAR2
	   ,p_systemlist	OUT 	 TYPES.cursor_type
	);
	
 /***********************************************************************
  * Author : HReddi
  * Description : Fetching the submitted price request id details
  ************************************************************************/
	PROCEDURE gm_fch_price_req_details (
		p_requestid         	IN 		T7500_ACCOUNT_PRICE_REQUEST.C7500_ACCOUNT_PRICE_REQUEST_ID%TYPE
	  , p_rebatePrice         	IN 		VARCHAR2
	  , p_user_id			    IN      t7501_account_price_req_dtl.C7501_CREATED_BY%TYPE
	  , p_headerdetails 		OUT		TYPES.cursor_type
	  , p_constructdetails 		OUT		TYPES.cursor_type
	  , p_systemDetails			OUT		TYPES.cursor_type
	);
	
 /***********************************************************************
  * Author : Matt
  * Description : Fetching the details for price request based on approved prices
  ************************************************************************/
	PROCEDURE gm_fch_generate_price_file (
		p_requestid         IN 	T7500_ACCOUNT_PRICE_REQUEST.C7500_ACCOUNT_PRICE_REQUEST_ID%TYPE
	  , p_pricedetails	 	OUT	TYPES.cursor_type
	);
 /******************************************************************************
  * Author : APrasath
  * Description : Summing and populating the Last 12 Month sales of an Account - GmSalesYTDBean.java(reportYTDByAccountActual())
  ******************************************************************************/	
	FUNCTION get_acc_last12_month_sales(
		p_account_id IN T704_account.c704_account_id%TYPE
		,p_gpb_id IN T704_account.c101_party_id%TYPE
	)
	RETURN NUMBER;
  /******************************************************************************
  * Author : APrasath
  * Description : Summing and populating the Last 12 Month sales of an Parent Account  or GPB
  ******************************************************************************/	
	FUNCTION get_parent_acc_12_month_sales(
		 p_party_id IN T704_account.c101_party_id%TYPE DEFAULT NULL
		,p_gpb_id IN T704_account.c101_party_id%TYPE		
	)
	RETURN NUMBER;

/***********************************************************************
  * Author : Jreddy
  * Description : Fetching Account Information details
  ************************************************************************/
	PROCEDURE gm_fch_account_info_details (
		p_requestid         IN 		t9010_answer_data.c9010_ref_id%TYPE
	   ,p_account_details   OUT		TYPES.cursor_type
	);

/******************************************************************
 *Author : Jreddy
 *Description : This procedure is used to fetch question, answer, answer group
 ****************************************************************/
	PROCEDURE gm_fch_question_answer (
		p_requestid 		  IN	   t7000_account_price_request.c7000_account_price_request_id%TYPE
	  , p_outquestion		  OUT	   TYPES.cursor_type
	  , p_outanswer 		  OUT	   TYPES.cursor_type
	  , p_outanswergroup	  OUT	   TYPES.cursor_type
	  , p_outquestionanswer   OUT	   TYPES.cursor_type
	);

/******************************************************************
 *Author : Jreddy
 *Description : This procedure is used to fetch Pricing History Log
 ****************************************************************/	
	PROCEDURE gm_fch_price_log_history (
		p_ref_id	   IN		t941_audit_trail_log.c941_ref_id%TYPE
	  , p_reqhistory   OUT		TYPES.cursor_type
	);
	
/******************************************************************
 *Author : Jreddy
 *Description : This procedure is used to fetch GBB comparision details
 ****************************************************************/
	PROCEDURE gm_fch_gpb_compare_dtls (
		p_requestid 		  IN	   t7000_account_price_request.c7000_account_price_request_id%TYPE
	  , p_outgpbheader		  OUT	   TYPES.cursor_type
	  , p_outpricedtls 		  OUT	   TYPES.cursor_type
	  , p_outsystemdtls	  	  OUT	   TYPES.cursor_type
	);
	
/******************************************************************
* Author : Dsandeep
* Description : This procedure is used to fetch System list
 ****************************************************************/
	PROCEDURE gm_fch_systems_names (
	p_systemlist	OUT 	 TYPES.cursor_type
	);
/******************************************************************
 *Author : Arockia prasath
 *Description : This procedure is used to fetch mapped pricing system 
 ****************************************************************/
	PROCEDURE gm_fch_price_mapped_system (
		p_requestid 		  IN	   t7000_account_price_request.c7000_account_price_request_id%TYPE
	  , p_req_map_systems     OUT	   TYPES.cursor_type
	  , p_req_systems         OUT      CLOB
	  , p_sav_systems         OUT      VARCHAR2
	  , p_req_gpb_dtls	      OUT	   TYPES.cursor_type
	);
	
/******************************************************************
 *Author : Jreddy
 *Description : This procedure is used to send mail with price request details to AD, VP and sales rep
 ****************************************************************/
	PROCEDURE gm_fch_request_dtls (
		p_requestid 		  IN	   t7000_account_price_request.c7000_account_price_request_id%TYPE
	  , p_outdtls		  	  OUT	   TYPES.cursor_type
	  , p_rep_mail  		  OUT 	   VARCHAR2
	  , p_ad_mail  			  OUT 	   VARCHAR2
	  ,	p_vp_mail  		      OUT      VARCHAR2
	  , p_token_str			  OUT	   VARCHAR2
	  , p_status              OUT      VARCHAR2
	  , p_cc_email			  OUT      VARCHAR2
	);	
	
/******************************************************************
 *Author : Aprasath
 *Description : This procedure is used to send ipad notification to AD
 ****************************************************************/
	PROCEDURE gm_fch_req_notification_dtls (
		p_requestid 		  IN	   t7000_account_price_request.c7000_account_price_request_id%TYPE	  
	  , p_token_str			  OUT	   VARCHAR2
	);
	
/***********************************************************************************
* Author : karthiks
* Description : This procedure is used to fetch the procedure based on access level
 ***********************************************************************************/
	PROCEDURE gm_fch_gpb_account_details (
	p_user_id	 IN 	 T101_USER.C101_USER_ID%TYPE
  ,	p_type		  IN	   t101_party.c901_party_type%TYPE
  , p_searchtext  IN	   VARCHAR2
  , p_gpb_acc_id   IN	t101_party.c101_party_id%TYPE
  , p_message	  OUT	   VARCHAR2
  , p_recordset   OUT	   TYPES.cursor_type
	);
	
/***********************************************************************************
* Author : karthiks
* Description : This procedure is used to fetch GPB based on access level
 ***********************************************************************************/
	PROCEDURE gm_fch_sales_gpb_account_dtl (
	p_user_id	  IN 	 T101_USER.C101_USER_ID%TYPE
  ,	p_type		  IN	   t101_party.c901_party_type%TYPE
  , p_searchtext  IN	   VARCHAR2
  , p_gpb_acc_id   IN	t101_party.c101_party_id%TYPE
  , p_message	  OUT	   VARCHAR2
  , p_recordset   OUT	   TYPES.cursor_type
	);
	
	    
/*************************************************************************************************
* Author : karthiks
* Description : This procedure is used to fetch the Price request salesrep id in Initiated Status
 *************************************************************************************************/
   
   PROCEDURE gm_fch_pr_pending_init_repid (
	p_out_initiated_req		OUT   TYPES.cursor_type
	);
   
 
/**********************************************************************************************
* Author : karthiks
* Description : This procedure is used to fetch the Price request details in Initiated Status
 **********************************************************************************************/
   
   PROCEDURE gm_fch_pr_pending_init_req (
   	p_rep_id				IN   T7500_ACCOUNT_PRICE_REQUEST.C703_SALES_REP_ID%TYPE ,
	p_out_initiated_req		OUT   TYPES.cursor_type
	);
   
 /*************************************************************************************************
* Author : karthiks
* Description : This procedure is used to fetch the Price request AD id in Pending AD Approval status
 ****************************************************************************************************/
   
   PROCEDURE gm_fch_pending_approval_ad_id (
	p_out_initiated_req		OUT   TYPES.cursor_type
	);
   
 /*************************************************************************************************
* Author : karthiks
* Description : This procedure is used to fetch the Price request in Pending AD Approval Status
 ****************************************************************************************************/
   
   PROCEDURE gm_fch_pr_pending_aprvl_req (
   	p_ad_id 				IN   T7500_ACCOUNT_PRICE_REQUEST.C101_AD_ID%TYPE ,
	p_out_initiated_req		OUT   TYPES.cursor_type
	);
	
 /*************************************************************************************************
* Author : Suganthi
* Description : This procedure is used to fetch the Change Price request in Modify Order screen
 ****************************************************************************************************/
 
  procedure GM_FCH_ORDER_PRICE_REQ(
  P_ORD_ID     in  T7503_ACCOUNT_PRICE_REQ_ORDER.C7500_ACCOUNT_PRICE_REQ_ID%type,
  P_OUT_PR_CUR  OUT types.CURSOR_TYPE
  );
  
/*************************************************************************************************
* Author : Karthik 
* Description : This function is used to get the system division
****************************************************************************************************/  
    FUNCTION GET_SYSTEM_DIVISION(
		p_set_id IN t207_set_master.C207_SET_ID%TYPE
	)
	RETURN VARCHAR2;
END gm_pkg_sm_pricerequest_rpt;
/