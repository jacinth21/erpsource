--@"C:\pricing\db\Packages\Sales\pricing\gm_pkg_sm_pricparam_setup.bdy

CREATE OR REPLACE PACKAGE BODY gm_pkg_sm_pricparam_setup
IS
  	/***********************************************************************************
	 * Description    : This procedure is called to SAVE PRICE ADJUSTMENT CODE VALUES 
	 * Parameters     : p_accid,p_party_id,p_prcAdjInpStr p_accInputStr, p_userid
	*************************************************************************************/
	PROCEDURE gm_save_pricing_params (
        p_accid         IN t704_account.c704_account_id%TYPE,
        p_party_id      IN t704_account.c101_party_id%TYPE,
        p_prcAdjInpStr  IN VARCHAR2,
        p_gpo_id		IN VARCHAR2,
        p_userid        IN t704a_account_attribute.c704a_created_by%TYPE
    )
    AS
	    v_strlen    NUMBER := NVL (LENGTH (p_prcAdjInpStr), 0) ;
	    v_string    VARCHAR2 (4000) := p_prcAdjInpStr;
	    v_substring VARCHAR2 (1000) ;
	    v_acc_attr_type t704a_account_attribute.c901_attribute_type%TYPE;
	    v_acc_attr_val t704a_account_attribute.c704a_attribute_value%TYPE;
	    v_acc_price_val VARCHAR2(30);
	    v_party_id  VARCHAR2(30);
	    v_prc_adj_id VARCHAR2(30);	    
	    v_adj_value VARCHAR2(30);
	    v_adj_price_val   VARCHAR2(20);
	BEGIN
		v_party_id := p_party_id;		
		-- Getting the Party id from the selec ted Account ID when we are comming from the Acoount Setup screen.
		IF v_party_id IS NULL THEN
			BEGIN
			 	SELECT C101_PARTY_ID 
			 	  INTO v_party_id 
			 	  FROM T704_ACCOUNT 
			 	 WHERE C704_ACCOUNT_ID = p_accid 
			 	   AND c704_void_fl IS NULL;
		 	EXCEPTION WHEN NO_DATA_FOUND  THEN
         			   v_party_id :=  null;
				END;
		END IF;		   
		
		-- Looping the Input string and getting the Attribute Value, Type, Price Values and Transaction Ref id.
    	IF v_strlen > 0 THEN
	        WHILE INSTR (v_string, '|') <> 0
	        LOOP
	            --
	            v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
	            v_string := SUBSTR (v_string, INSTR (v_string, '|') + 1) ;
	            --
	            v_acc_attr_type := NULL;
	            v_acc_attr_val := NULL;
	            v_acc_price_val := NULL;
	            v_prc_adj_id := NULL;
	            --
	            v_acc_attr_type := TO_NUMBER (SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1)) ;
	            v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1) ;
	            v_acc_attr_val := TO_NUMBER (SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1)) ;
	            v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1) ;
	            v_acc_price_val := TO_NUMBER (SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1)) ;
	            v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1) ;
	            v_prc_adj_id := v_substring;
	            
	            -- selecting the Adjustment Value and its Price Value for the transaction id for making the proper entries in History table.
	             BEGIN 
		             SELECT c1706_adj_value , C901_PRICE_TYPE INTO v_adj_value , v_adj_price_val
				       FROM t1706_party_price_adj 
				      WHERE c1706_party_price_adj_id = v_prc_adj_id 
				        AND c1706_void_fl is null 
				        AND c901_adj_type = v_acc_attr_type ;
			    EXCEPTION WHEN NO_DATA_FOUND  THEN
         			   v_adj_value :=  null;
         			   v_adj_price_val := null;
				END;
	            
				-- UPDATING the Modified Adj Type and Adj Price Type and its values in t1706 table..
                UPDATE t1706_party_price_adj
                   SET C901_ADJ_TYPE = v_acc_attr_type
                     , C1706_ADJ_VALUE = v_acc_attr_val
                     , C901_PRICE_TYPE = v_acc_price_val
                     , C1706_LAST_UPDATED_BY = p_userid
                     , C1706_LAST_UPDATED_DATE = CURRENT_DATE
                 WHERE c101_party_id = v_party_id  
                   AND C1706_PARTY_PRICE_ADJ_ID = v_prc_adj_id
                   AND C1706_void_fl IS NULL;                  
                              
	            -- IF no records update, the we are making the new records in t1706 table.
                IF (SQL%ROWCOUNT = 0) THEN
	                 SELECT S1706_party_price_adj.NEXTVAL INTO v_prc_adj_id FROM DUAL;                 
	               --  v_prc_adj_id := v_account_attr_id;                
	                 INSERT INTO t1706_party_price_adj(C1706_PARTY_PRICE_ADJ_ID,c101_party_id,C901_ADJ_TYPE
	                                                  ,C1706_ADJ_VALUE,C901_PRICE_TYPE,C1706_LAST_UPDATED_BY,C1706_LAST_UPDATED_DATE)
	                      VALUES(v_prc_adj_id,v_party_id,v_acc_attr_type,v_acc_attr_val,v_acc_price_val,p_userid,CURRENT_DATE) ;
	            END IF;
	            
	            -- Making the entries in t941 histroy log table for the Adjustment value modifications.
            	gm_sav_priceparam_history(v_prc_adj_id,p_userid,v_acc_attr_type,v_acc_attr_val,v_acc_price_val, v_adj_value , v_adj_price_val);           	
        	END LOOP;
        	-- for saving Group Account Mapping while comming from the Account Setup screen
            IF p_accid IS NOT NULL THEN
        	    gm_sav_grp_account_mapping(p_accid,p_party_id,p_gpo_id,p_userid);
        	    gm_pkg_sm_acct_trans.gm_sav_sync_gpo_acct(p_accid,p_gpo_id,p_userid);
        	END IF;
   		END IF;
	END gm_save_pricing_params;

 /***********************************************************************************
* Description     : This procedure is called to Fetch Price Adjustment code values 
* Parameters      : p_accid,p_party_id
*************************************************************************************/

	PROCEDURE gm_fch_pricing_params (
        p_accid       	 IN t704_account.c704_account_id%TYPE,
        p_party_id       IN VARCHAR2,
        p_out_cursor OUT TYPES.cursor_type)        
  	AS
		v_party_id VARCHAR2(30);
		v_rec_count NUMBER;
	BEGIN
		v_party_id := p_party_id;
	
	IF v_party_id IS NULL THEN
	BEGIN 
		SELECT C101_PARTY_ID 
		 	  INTO v_party_id 
		 	  FROM T704_ACCOUNT 
		 	 WHERE C704_ACCOUNT_ID = p_accid 
		 	   AND c704_void_fl IS NULL;
	EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         v_party_id :=  null;
	END;
	END IF;	
	OPEN p_out_cursor FOR	
	
	   SELECT t1706.c901_adj_type ADJTYPEID 
           , get_code_name(t1706.c901_adj_type) ADJTYPENM
           , t1706.c1706_adj_value ADJVALUE
           , t1706.c901_price_type PRICEVALUE
           , t1706.C1706_PARTY_PRICE_ADJ_ID pricAdjID
           , (SELECT count(1) 
                FROM t941_audit_trail_log 
               WHERE c940_audit_trail_id = '1234' 
                 AND c941_ref_id = t1706.c1706_party_price_adj_id) HisCount
        FROM t1706_party_price_adj t1706
       WHERE c101_party_id = v_party_id
         AND c1706_void_fl is null;
         
    END gm_fch_pricing_params;
    
    
  /*******************************************************
   * Description : Procedure to fetch required date history Info
   * parameters 	 : Audit Trail ID and Audit Transaction id
   *******************************************************/
--
	PROCEDURE gm_fch_priceparam_history (
		p_ref_id	   IN		t941_audit_trail_log.c941_ref_id%TYPE
	  , p_audit_id	   IN		t940_audit_trail.c940_audit_trail_id%TYPE
	  , p_reqhistory   OUT		TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_reqhistory FOR 
		 	 SELECT t941.c941_ref_id reqid                 
                    , regexp_substr(c941_value, '[^^]+', 1, 1)  rvalue              
                    , get_code_name(regexp_substr(c941_value, '[^^]+', 1, 2)) ADJCODE
                    , get_code_name(regexp_substr(c941_value, '[^^]+', 1, 3)) PRICEVAL
                    , get_user_name (t941.c941_updated_by) updatedby
					, TO_CHAR (t941.c941_updated_date
                    , SYS_CONTEXT('CLIENTCONTEXT','COMP_DATE_FMT')||' HH24:MI:SS') update_date
				 FROM t940_audit_trail t940, t941_audit_trail_log t941
				WHERE t941.c940_audit_trail_id = t940.c940_audit_trail_id
				  AND t941.c941_ref_id = p_ref_id				 
				  AND t940.c940_audit_trail_id = p_audit_id
				  AND t940.c940_void_fl IS NULL
			 ORDER BY t941.c941_updated_date;
			 
	END gm_fch_priceparam_history;
	
  /*****************************************************************************************
   * Description : Procedure to save the History over the Part Adjustment Code modifications
   * Author  	 : HReddi
   *****************************************************************************************/
--
	PROCEDURE gm_sav_priceparam_history (
		p_ref_id	   		IN		t941_audit_trail_log.c941_ref_id%TYPE
	  , p_user_id	   		IN		t941_audit_trail_log.C941_UPDATED_BY%TYPE
	  , p_aatr_type	   		IN		VARCHAR2
	  , p_aatr_value   		IN		VARCHAR2
	  , p_aatr_price   		IN 		VARCHAR2
	  , p_old_aatr_value   	IN		VARCHAR2
	  , p_old_aatr_price   	IN 		VARCHAR2
	)
	AS
	    v_rec_count NUMBER;
	    v_aatr_value  VARCHAR2(100);
	
	BEGIN
		-- Gettign the Records count from the Histroy Log table for the corresponding transaction ref id.
		-- the purpose is, if we tried to make the Adjustment value as empty for already saved value, we need to mantain the history for the same.
		SELECT count(1) 
          INTO v_rec_count 
          FROM t941_audit_trail_log 
         WHERE c940_audit_trail_id = '1234' 
           AND c941_ref_id = p_ref_id 
           AND c941_value like '%'|| p_aatr_type || '%';          
            
        -- If Adjustment Values are already saved and trying to removed the same, we need to maintain the history for the same.
        -- so for that empty adjustment value also we need History record.
        -- Record is available in Histro Log AND value is available for that AND now empty value is comming...    
        IF(v_rec_count > 0 AND p_old_aatr_value IS NOT NULL AND p_aatr_value IS NULL) THEN          		
	    	v_aatr_value:=   ' ' || '^' || p_aatr_type || '^' || p_aatr_price ;	           			
	        gm_pkg_cm_audit_trail.gm_cm_sav_audit_log(p_ref_id
	           		                                 ,v_aatr_value
	           		                                 ,'1234'  -- Audit Trail Log ID
	           		                                 , p_user_id
	           		                                 , CURRENT_DATE);
	    -- Tis will executes when the Attribute type alwas has the values...
	    -- we need to record the History as we are modifying the Price Value as well.       		                                         
        ELSIF (NVL(p_aatr_price,'-9999') <> p_old_aatr_price) OR (NVL(p_old_aatr_value,'-9999') <> p_aatr_value)   THEN            
            v_aatr_value:=  p_aatr_value || '^' || p_aatr_type || '^' || p_aatr_price ;
           	gm_pkg_cm_audit_trail.gm_cm_sav_audit_log(p_ref_id
           			                                 ,v_aatr_value
           			                                 ,'1234'  -- Audit Trail Log ID
           			                                 , p_user_id
           			                                 , CURRENT_DATE);
        END IF;		
	END gm_sav_priceparam_history;
	
  /*********************************************************************
   * Description : Procedure to save the Group Account Mapping details
   * Author  	 : HReddi
   *********************************************************************/
--
	PROCEDURE gm_sav_grp_account_mapping (
		p_acc_id	 IN t704_account.c704_account_id%TYPE
	  , p_party_id	 IN t704_account.c101_party_id%TYPE
	  , p_gpo_id	 IN VARCHAR2
	  , p_user_id	 IN t740_gpo_account_mapping.C740_CREATED_BY %TYPE
	)
	AS
	    v_acct_grp_id VARCHAR2 (20);
	    P_SAVEDACCNUM VARCHAR2(30);
	    v_count NUMBER;
	
	BEGIN	
	--Fetch to check if the account has already GPO mapping  Update/Insert want work for GPO [because of business logic]	
		v_acct_grp_id := p_acc_id;
	    p_savedaccnum := v_acct_grp_id; 
		
	   SELECT COUNT (1)
	     INTO v_count
	     FROM t740_gpo_account_mapping
	    WHERE c704_account_id = p_acc_id 
	      AND c740_void_fl IS NULL;
    
	IF v_count > 0 THEN 
	   UPDATE t740_gpo_account_mapping
          SET c101_party_id = DECODE(p_gpo_id, 0, NULL, p_gpo_id) 
            , c740_gpo_catalog_id = p_acc_id
            , c740_last_updated_by = p_user_id
            , c740_last_updated_date = CURRENT_DATE
        WHERE c704_account_id = p_acc_id;
    
    ELSE
		IF p_gpo_id <> 0  THEN
		        INSERT INTO t740_gpo_account_mapping
		        VALUES (s740_gpo_account.NEXTVAL, p_gpo_id, p_savedaccnum, p_savedaccnum, NULL, p_user_id, CURRENT_DATE, NULL, NULL);
		END IF; 
    END IF;
	dbms_mview.REFRESH ('v700_territory_mapping_detail');
	
	END gm_sav_grp_account_mapping; 
   /************************************************************************************************
   * Description : Procedure to split values for gm_sav_acct_affln procedure
   * Author  	 : mahavishnu
   ************************************************************************************************/
	PROCEDURE gm_sav_acct_affln_dtl(
	    p_acc_id    IN t704d_account_affln.C704_ACCOUNT_ID%TYPE,
	    p_affln_str IN VARCHAR2,
	    p_userid    IN t704a_account_attribute.C704A_CREATED_BY%TYPE 
    )
    AS
	    v_string    VARCHAR2 (4000) := p_affln_str;
	    v_substring VARCHAR2 (1000) ;
    	v_gpo          t704d_account_affln.C101_GPO%TYPE;
	    v_idn          t704d_account_affln.C101_IDN%TYPE;
	    v_contract     t704d_account_affln.C901_CONTRACT%TYPE;
	    v_party_id 	   t704_account.c101_party_id%TYPE;
	    v_gln		   t704d_account_affln.C704D_GLN%TYPE;
	    v_gpoid		   t704d_account_affln.C704D_GPOID%TYPE;
	    v_rpc           t704d_account_affln.C101_RPC%TYPE;
	    v_hlthsystem    t704d_account_affln.c101_health_system%TYPE;
	    
	    
	    CURSOR v_account_info
    		IS
         SELECT c704_account_id id
           FROM t704_account
          WHERE c101_party_id    = v_party_id
            AND c704_account_id <> p_acc_id
            AND c704_void_fl    IS NULL;
    BEGIN
	    -- iDn + '|' + gPo + '|' + contrVal + '|' + adminFeeFlag + '|' + adminFee + '|'
    	IF INSTR (v_string, '|') > 0 THEN
	            v_idn := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
	            v_string := SUBSTR (v_string, INSTR (v_string, '|') + 1) ;
	  			v_gpo := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
	            v_string := SUBSTR (v_string, INSTR (v_string, '|') + 1) ;
	  			v_contract := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ; 
	  			v_string := SUBSTR (v_string, INSTR (v_string, '|') + 1) ;
	  			v_rpc := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
	            v_string := SUBSTR (v_string, INSTR (v_string, '|') + 1) ;
	  			v_hlthsystem := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
	  			v_string := SUBSTR (v_string, INSTR (v_string, '|') + 1) ;
	  			v_gln := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
	  			v_string := SUBSTR (v_string, INSTR (v_string, '|') + 1) ;
	  			v_gpoid := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
	  			v_string := SUBSTR (v_string, INSTR (v_string, '|') + 1) ;
	  			--PC-2039 Add new fields on the Pricing Parameter tab in Account Setup screen (commenting the code related to admin fee and admin fee flag ) 
	  			gm_sav_acct_affln(p_acc_id,v_gpo,v_idn,v_contract,v_rpc,v_hlthsystem,p_userid,v_gln,v_gpoid);
	  			
	  			SELECT c101_party_id
		          INTO v_party_id
		          FROM t704_account
		         WHERE c704_account_id = p_acc_id 
		           AND c704_void_fl IS NULL;
		           
		        FOR v_acc_info IN v_account_info
    			LOOP
                   gm_sav_acct_affln(v_acc_info.id,v_gpo,v_idn,v_contract,v_rpc,v_hlthsystem,p_userid,v_gln,v_gpoid);
    			END LOOP;
	  	END IF;
	  	
	END gm_sav_acct_affln_dtl;
	
	  /***********************************************************************************************
   * Description : Procedure to save the Account Affiliation Detail in t704d_account_affln table
   * Author  	 : mahavishnu
   ***************************************************************************************************/
	PROCEDURE gm_sav_acct_affln(
	    p_accid        IN t704d_account_affln.C704_ACCOUNT_ID%TYPE,
	    p_gpo          IN t704d_account_affln.C101_GPO%TYPE,
	    p_idn          IN t704d_account_affln.C101_IDN%TYPE,
	    p_contract     IN t704d_account_affln.C901_CONTRACT%TYPE,
	    p_rpc          IN t704d_account_affln.C101_RPC%TYPE,
	    p_hlthsystem   IN t704d_account_affln.c101_health_system%TYPE,
	    p_userid       IN t704a_account_attribute.C704A_CREATED_BY%TYPE,
	    p_gln		   IN t704d_account_affln.C704D_GLN%TYPE ,
	    p_gpoid		   IN t704d_account_affln.C704D_GPOID%TYPE
    )
    AS
    BEGIN
    
   				--PC-2039 Add new fields on the Pricing Parameter tab in Account Setup screen (commenting the code related to admin fee and admin fee flag ) 
	  			
		UPDATE t704d_account_affln
		SET C101_GPO              = p_gpo,
		  C101_IDN                = p_idn,
		  C901_CONTRACT           = p_contract,
		  C101_RPC                = p_rpc,
		  c101_health_system      = p_hlthsystem,
		  C704D_LAST_UPDATED_BY   = p_userid ,
		  C704D_LAST_UPDATED_DATE = CURRENT_DATE,
		  C704D_GLN               = p_gln,
		  C704D_GPOID              = p_gpoid
		WHERE C704_ACCOUNT_ID     = p_accid
		AND C704D_VOID_FL        IS NULL;  
	    
	    IF (SQL%ROWCOUNT = 0) THEN
	    
			INSERT
			INTO t704d_account_affln
			  (
			    C704D_ACCOUNT_AFFLN_ID,
			    C704_ACCOUNT_ID,
			    C101_GPO,
			    C101_IDN,
			    C901_CONTRACT,
			    C101_RPC, 
			    c101_health_system,
			    C704D_LAST_UPDATED_BY,
			    C704D_LAST_UPDATED_DATE,
			    C704D_GLN,
			    C704D_GPOID
			  )
			  VALUES
			  (
			    s704d_account_affln.NEXTVAL,
			    p_accid,
			    p_gpo,
			    p_idn,
			    p_contract,
			    p_rpc,
			    p_hlthsystem,
			    p_userid,
			    CURRENT_DATE,
			    p_gln,
			    p_gpoid
			  );
	    END IF;
	END gm_sav_acct_affln;
  /*********************************************************************
    * Description : function to get Account Party ID for the Account ID
   *********************************************************************/
   FUNCTION get_acc_partyid (
      p_account_id    IN   t704_account.c704_account_id%TYPE
   )
      RETURN VARCHAR2
   IS
      v_acc_party_id   VARCHAR2 (150);
   BEGIN
      BEGIN
        SELECT c101_party_id INTO v_acc_party_id
          FROM t704_account
         WHERE c704_account_id = p_account_id
           AND c704_void_fl IS NULL;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            RETURN '';
      END;

    RETURN v_acc_party_id;  
	END get_acc_partyid;
	
	/*
	* Description : This procedure used to log the Account map details.
	* 				While change the Account GPB (Account setup - Pricing parameter) 
	* 				, at the time to track the Party and time information and stored to log table (T704a)
	*
	* Author: mmuthusamy
	* 
	*/
	
	PROCEDURE gm_sav_gpo_account_map_log (
	        p_party_id   IN t740a_gpo_account_map_log.c101_party_id%TYPE,
	        p_account_id IN t740a_gpo_account_map_log.c704_account_id%TYPE,
	        p_from_date  IN t740a_gpo_account_map_log.c740a_from_dt%TYPE,
	        p_to_date    IN t740a_gpo_account_map_log.c740a_to_dt%TYPE,
	        p_user_id    IN t740a_gpo_account_map_log.c740a_last_updated_by%TYPE)
	AS
	
	BEGIN
		--
		
	     INSERT
	       INTO T740A_GPO_ACCOUNT_MAP_LOG
	        (
	            C740A_ACC_MAP_LOG_ID, C101_PARTY_ID, C704_ACCOUNT_ID
	          , C740A_FROM_DT, C740A_TO_DT, C740A_LAST_UPDATED_BY
	          , C740A_LAST_UPDATED_DT
	        )
	        VALUES
	        (
	            s740A_ACC_MAP_LOG_ID.nextval, p_party_id, p_account_id
	          , p_from_date, p_to_date, p_user_id
	          , CURRENT_DATE
	        ) ;
	      --  
	END gm_sav_gpo_account_map_log;
	
	/*
	* Description : This procedure used to Release/Unrelease System in Pricing Tool.
	* Author: mselvamani
	* 
	*/
	PROCEDURE gm_prt_add_release_system(
    p_sysid     IN t2080_set_company_mapping.c207_set_id%TYPE,
    p_segid     IN t207_set_master.c901_pricing_segment_id%TYPE,
    p_releasefl IN t2080_set_company_mapping.c2080_pricing_release_fl%TYPE,
    p_userid    IN t2080_set_company_mapping.c2080_last_updated_by%TYPE )
AS
  v_company_id T1900_COMPANY.C1900_COMPANY_ID%TYPE;
BEGIN
  SELECT get_compid_frm_cntx() INTO v_company_id FROM DUAL;
  UPDATE t2080_set_company_mapping
  SET c2080_pricing_release_fl = p_releasefl,
    c2080_last_updated_by      = p_userid,
    c2080_last_updated_date    = sysdate
  WHERE c207_set_id            = p_sysid
  AND c1900_company_id        = v_company_id;
  IF(p_releasefl = 'Y')
  THEN
  UPDATE t207_set_master
  SET c901_pricing_segment_id = p_segid,
    c207_last_updated_by      = p_userid,
    c207_last_updated_date    = sysdate
  WHERE c207_set_id           = p_sysid
  AND c1900_company_id        = v_company_id;
  END IF;
END gm_prt_add_release_system;

/*
	* Description : This procedure used to get teh Segment Values.
	* Author: mselvamani
	* 
	*/
	PROCEDURE gm_fch_segment_names(
	    p_seg_out OUT TYPES.cursor_type )
	AS
	BEGIN
	  OPEN p_seg_out FOR 
	  SELECT c901_code_id CODEID,
	  c901_code_nm CODENM FROM t901_code_lookup WHERE c901_code_grp = 'SGMTTY' AND c901_void_fl IS NULL AND c901_active_fl = '1';
	END gm_fch_segment_names;
	

END gm_pkg_sm_pricparam_setup;
/
