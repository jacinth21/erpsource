--@"C:\PMT\db\Packages\Sales\pricing\gm_pkg_sm_rebate_setup.bdy";
CREATE OR REPLACE PACKAGE BODY gm_pkg_sm_rebate_setup
IS
    /*********************************************************
    * Description : This procedure is used to save the Rebate master information.
    *              Based on Rebate id update the data - if no data available - insert the new records to T7200 table.
    *
    * Author:
    *********************************************************/
PROCEDURE gm_sav_rebate_managment_dtls (
        p_rebate_id           IN OUT t7200_rebate_master.c7200_rebate_id%TYPE,
        p_rebate_acc_type     IN t7200_rebate_master.c901_grp_type%TYPE,
        p_party_id            IN t7200_rebate_master.c7200_ref_id%TYPE,
        p_rebate_part_type    IN t7200_rebate_master.c901_rebate_part_type%TYPE,
        p_rebate_category     IN t7200_rebate_master.c901_rebate_category%TYPE,
        p_rebate_rate         IN t7200_rebate_master.c7200_rebate_rate%TYPE,
        p_contract_frm_date   IN VARCHAR2,
        p_contract_to_date    IN VARCHAR2,
        p_effective_date      IN VARCHAR2,
        p_rebate_type         IN t7200_rebate_master.c901_rebate_type%TYPE,
        p_rebate_payment_type IN t7200_rebate_master.c901_rebate_payment%TYPE,
        p_contract_info       IN t7200_rebate_master.c7200_reb_contract_info%TYPE,
        p_rebate_comments     IN t7200_rebate_master.c7200_reb_comments%TYPE,
        p_tier1_percent       IN t7200_rebate_master.c7200_tier1_percent%TYPE,
        p_tier1_from_amount   IN t7200_rebate_master.c7200_tier1_from_amt%TYPE,
        p_tier1_to_amount     IN t7200_rebate_master.c7200_tier1_to_amt%TYPE,
        p_tier2_percent       IN t7200_rebate_master.c7200_tier2_percent%TYPE,
        p_tier2_from_amount   IN t7200_rebate_master.c7200_tier2_from_amt%TYPE,
        p_tier2_to_amount     IN t7200_rebate_master.c7200_tier2_to_amt%TYPE,
        p_tier3_percent       IN t7200_rebate_master.c7200_tier3_percent%TYPE,
        p_tier3_from_amount   IN t7200_rebate_master.c7200_tier3_from_amt%TYPE,
        p_tier3_to_amount     IN t7200_rebate_master.c7200_tier2_to_amt%TYPE,
        p_active_fl           IN t7200_rebate_master.c7200_active_fl%TYPE,
        p_user_id             IN t7200_rebate_master.c7200_last_updated_by%TYPE,
        p_company_id          IN t7200_rebate_master.c1900_company_id%TYPE
        )
AS

    v_rebate_id t7200_rebate_master.c7200_rebate_id%TYPE;
    v_date_fmt VARCHAR2 (20) ;
    v_rebateid_prefix VARCHAR2(10);
    
BEGIN

	-- to get the company and date formate from context
     SELECT get_compdtfmt_frm_cntx ()
       INTO v_date_fmt
       FROM dual;
    
       
     -- assign the rebate master id to local variable
    v_rebate_id := p_rebate_id;
    -- to update the rebate master details
    
     UPDATE t7200_rebate_master
    SET c901_grp_type                                = p_rebate_acc_type, 
    c7200_ref_id = p_party_id, c901_rebate_part_type = p_rebate_part_type,
    c901_rebate_category                             = p_rebate_category, 
    c7200_rebate_rate                                = p_rebate_rate,
    c7200_reb_from_dt 							     = to_date (p_contract_frm_date, v_date_fmt), 
    c7200_reb_to_dt                                  = to_date (p_contract_to_date, v_date_fmt),
    c7200_reb_eff_dt                                 = to_date (p_effective_date, v_date_fmt), 
    c901_rebate_type                                 = p_rebate_type, 
    c901_rebate_payment                              = p_rebate_payment_type, 
    c205_reb_part_number                             = get_rule_value(p_rebate_type, 'REBATE_PARTS'),
    c7200_reb_contract_info                          = p_contract_info, 
    c7200_reb_comments                               = p_rebate_comments, 
    c7200_tier1_percent                              = p_tier1_percent,
    c7200_tier1_from_amt                             = p_tier1_from_amount,
    c7200_tier1_to_amt                               = p_tier1_to_amount,
    c7200_tier2_percent                              = p_tier2_percent, 
    c7200_tier2_from_amt                             = p_tier2_from_amount,
    c7200_tier2_to_amt                               = p_tier2_to_amount, 
    c7200_tier3_percent                              = p_tier3_percent,
    c7200_tier3_from_amt                             = p_tier3_from_amount, 
    c7200_tier3_to_amt                               = p_tier3_to_amount,
    c7200_active_fl                                  = p_active_fl, 
    c7200_last_updated_by                            = p_user_id,
    c7200_last_updated_dt                            = CURRENT_DATE
    WHERE c7200_rebate_id                            = v_rebate_id
    AND C7200_VOID_FL       IS NULL;
      
      
    IF (SQL%ROWCOUNT                                      = 0) THEN
    --to get Rebate id Prefix
     SELECT GET_RULE_VALUE('REBATE_ID_PREFIX','REBATE') INTO v_rebateid_prefix  FROM DUAL;
        -- to get the seq no
         SELECT  v_rebateid_prefix || s7200_rebate_id.NEXTVAL
           INTO v_rebate_id
           FROM DUAL;
     
         INSERT
           INTO t7200_rebate_master
            (
                c7200_rebate_id, c901_grp_type, c7200_ref_id
              , c901_rebate_part_type, c901_rebate_category, c7200_rebate_rate
              , c7200_reb_from_dt, c7200_reb_to_dt, c7200_reb_eff_dt
              , c901_rebate_type, c205_reb_part_number, c901_rebate_payment
              , c7200_reb_contract_info, c7200_reb_comments, c7200_tier1_percent
              , c7200_tier1_from_amt, c7200_tier1_to_amt, c7200_tier2_percent
              , c7200_tier2_from_amt, c7200_tier2_to_amt, c7200_tier3_percent
              , c7200_tier3_from_amt, c7200_tier3_to_amt, c7200_active_fl
              , c7200_last_updated_dt, c7200_last_updated_by, c1900_company_id
            )
            VALUES
            (
                v_rebate_id, p_rebate_acc_type, p_party_id
              , p_rebate_part_type, p_rebate_category, p_rebate_rate
              , to_date (p_contract_frm_date, v_date_fmt), to_date (p_contract_to_date, v_date_fmt), to_date (
                p_effective_date, v_date_fmt), p_rebate_type, get_rule_value(p_rebate_type, 'REBATE_PARTS')
              , p_rebate_payment_type, p_contract_info, p_rebate_comments
              , p_tier1_percent, p_tier1_from_amount, p_tier1_to_amount
              , p_tier2_percent, p_tier2_from_amount, p_tier2_to_amount
              , p_tier3_percent, p_tier3_from_amount, p_tier3_to_amount
              , p_active_fl, CURRENT_DATE
              , p_user_id, p_company_id
            ) ;
            
    END IF;
    
    
    -- to assign the out parameter values
    p_rebate_id := v_rebate_id;
    
    
END gm_sav_rebate_managment_dtls;

/*********************************************************
* Description : This procedrue used to validate the part #. 
* If all the parts are valid then, Loop the part string
and call the part map procedure.
*
* Author:
*********************************************************/

PROCEDURE gm_sav_rebate_part_mapping_dtls
    (
        p_rebate_id      IN t7201_rebate_parts.c7200_rebate_id%TYPE,
        p_input_str      IN CLOB,
        p_effective_date IN VARCHAR2,
        p_user_id        IN t7201_rebate_parts.c7201_last_updated_by%TYPE,
        p_out_invalid_part OUT CLOB
    )
AS
    v_out_invalid_part CLOB;
    v_out_invalid_part_cmp CLOB;
    --
    v_string CLOB;
    v_substring VARCHAR2 (1000) ;
    v_part_no   VARCHAR2(4000);
    v_date_fmt VARCHAR2 (20) ;
BEGIN
	  -- to get the company date formate from context
     SELECT get_compdtfmt_frm_cntx ()
       INTO v_date_fmt
       FROM dual;
    -- to validate the part #
    gm_pkg_pd_partnumber.gm_validate_mfg_parts (p_input_str, v_out_invalid_part, v_out_invalid_part_cmp) ;
    
    -- If any parts number not valid then, return the error parts
    
    IF v_out_invalid_part  IS NOT NULL THEN
        p_out_invalid_part := v_out_invalid_part;
        RETURN;
    END IF;
    --
    v_string := p_input_str;
     WHILE INSTR (v_string, ',') <> 0
        LOOP
        	              v_substring := SUBSTR (v_string, 1, INSTR (v_string, ',') - 1) ;
            		      v_string    := SUBSTR (v_string, INSTR (v_string, ',')    + 1) ;	
            			  v_part_no   := v_substring ;
            		      IF v_part_no IS NOT NULL THEN
            			 	gm_pkg_sm_rebate_setup.gm_sav_rebate_part_map (p_rebate_id, v_part_no, to_date (p_effective_date, v_date_fmt), p_user_id);
						 END IF;
						 
      END LOOP; 
    --p_out_invalid_part := v_out_invalid_part;
END gm_sav_rebate_part_mapping_dtls;


/*********************************************************
* Description : This procedure used to update/Insert the rebate parts details to T7201 table.
*
*********************************************************/
PROCEDURE gm_sav_rebate_part_map
    (
        p_rebate_id      IN t7201_rebate_parts.c7200_rebate_id%TYPE,
        p_part_number_id IN t7201_rebate_parts.c205_part_number_id%TYPE,
        p_effective_date IN VARCHAR2,
        p_user_id        IN t7201_rebate_parts.c7201_last_updated_by%TYPE
    )
AS
    v_rebate_part_id t7201_rebate_parts.c7201_reb_part_id%TYPE;
 
BEGIN
  
       -- update the part mapping  
     UPDATE t7201_rebate_parts
        SET c7201_effective_dt      = p_effective_date, c7201_last_updated_by = p_user_id,
        c7201_last_updated_dt   = CURRENT_DATE
      WHERE c7200_rebate_id     = p_rebate_id
        AND c205_part_number_id = p_part_number_id
        AND c7201_void_fl      IS NULL;
        
      -- insert the new data
        
    IF (SQL%ROWCOUNT = 0) THEN  
          --   
         INSERT
           INTO t7201_rebate_parts
            (
                c7201_reb_part_id, c205_part_number_id, c7201_effective_dt
              , c7201_last_updated_dt, c7201_last_updated_by, c7200_rebate_id
            )
            VALUES
            (
                s7201_REB_PART_ID.NEXTVAL, p_part_number_id, p_effective_date
              , CURRENT_DATE, p_user_id, p_rebate_id
            ) ;
    END IF;
END gm_sav_rebate_part_map;

/*********************************************************
* Description : This procedure used to log the rebate master details
*
*********************************************************/
PROCEDURE gm_sav_rebate_master_log (
        p_rebate_id    IN t7202_rebate_log.c7200_rebate_id%TYPE,
        p_type         IN t7202_rebate_log.c901_type%TYPE,
        p_rebate_value IN t7202_rebate_log.c7202_rebate_value%TYPE,
        p_user_id      IN t7202_rebate_log.c7202_last_updated_by%TYPE,
        p_update_date  IN t7202_rebate_log.c7202_last_updated_dt%TYPE)
AS

BEGIN
	
     INSERT
       INTO t7202_rebate_log
        (
            c7202_rebate_log_id, c7200_rebate_id, c901_type
          , c7202_rebate_value, c7202_last_updated_by, c7202_last_updated_dt
        )
        VALUES
        (
            s7202_rebate_log.nextval, p_rebate_id, p_type
          , p_rebate_value, p_user_id, p_update_date
        ) ;
        
END gm_sav_rebate_master_log; 

END gm_pkg_sm_rebate_setup;
/