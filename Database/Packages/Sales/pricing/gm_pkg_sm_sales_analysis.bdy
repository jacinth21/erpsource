CREATE OR REPLACE
PACKAGE BODY gm_pkg_sm_sales_analysis
IS

 /******************************************************************************
  * Author : karthik
  * Description : procedure to retrieve sales analysis data from order 
  ******************************************************************************/
PROCEDURE gm_fch_12months_sales_data(
   p_request_id 	IN	T7520_PRICE_IMPACT_ANALYSIS.C7500_ACCOUNT_PRICE_REQUEST_ID%TYPE
  ,p_created_by		IN	T7520_PRICE_IMPACT_ANALYSIS.C7520_CREATED_BY%TYPE
  ,p_out 			OUT TYPES.cursor_type
  ,p_header_details OUT TYPES.cursor_type
)
AS
  v_Request_typ 	T7500_ACCOUNT_PRICE_REQUEST.C901_REQUEST_TYPE%TYPE;
  v_rec_cnt NUMBER;
  v_ac_id T704_ACCOUNT.C704_ACCOUNT_ID%TYPE;
  v_gpb_id T7500_ACCOUNT_PRICE_REQUEST.C101_GPB_ID%TYPE; 
  v_ad_name V700_TERRITORY_MAPPING_DETAIL.AD_NAME%TYPE;
  v_ad_id T7500_ACCOUNT_PRICE_REQUEST.C101_AD_ID%TYPE;
  v_org_name varchar2(100);
  v_request_type T7500_ACCOUNT_PRICE_REQUEST.C901_REQUEST_TYPE%TYPE;
  v_currency T901_CODE_LOOKUP.C901_CODE_GRP%TYPE;
  v_initiated_team 	VARCHAR2(20);
  v_company_id  	T7500_ACCOUNT_PRICE_REQUEST.C1900_COMPANY_ID%TYPE;
  v_sales_rep_id    CLOB;
  v_access_level	T101_USER.C101_ACCESS_LEVEL_ID%TYPE;
  v_rep_id    		CLOB;
  v_party_id   		T101_USER.C101_PARTY_ID%TYPE;
 BEGIN  
  
	 BEGIN
		SELECT COUNT(T7521.C207_SET_ID) INTO v_rec_cnt
		FROM T7521_PRICE_IMPACT_ANALY_DTL T7521,
	 		 T7520_PRICE_IMPACT_ANALYSIS T7520
		WHERE T7520.C7520_PRICE_IMPACT_ANALYSIS_ID = T7521.C7520_PRICE_IMPACT_ANALYSIS_ID
		AND C7521_VOID_FL                         IS NULL
		AND C7500_ACCOUNT_PRICE_REQUEST_ID         = p_request_id;
   EXCEPTION WHEN NO_DATA_FOUND THEN 
	    	v_rec_cnt := 0;
   END;
 
   IF (v_rec_cnt > 0) THEN
 SELECT T7500.C901_REQUEST_TYPE,T7500.C101_AD_ID, T7500.C704_ACCOUNT_ID, T7500.C101_GPB_ID, T7500.C1900_COMPANY_ID , TO_CLOB(T7500.C703_SALES_REP_ID)
      INTO v_request_type, v_ad_id,v_ac_id,v_gpb_id,v_company_id,v_sales_rep_id
      FROM T7500_ACCOUNT_PRICE_REQUEST T7500
      WHERE T7500.C7500_ACCOUNT_PRICE_REQUEST_ID = p_request_id;
      
       IF v_request_type = '903108' THEN  --Group account 
          v_ad_name := null;
          SELECT T101.C101_PARTY_NM,  T901.C901_CODE_NM
          INTO v_org_name, v_currency 
		  FROM T101_PARTY T101, T1900_COMPANY T1900, T901_CODE_LOOKUP T901
		  WHERE T1900.C1900_COMPANY_ID = T101.C1900_COMPANY_ID
		  AND T1900.C901_TXN_CURRENCY = T901.C901_CODE_ID AND T901.C901_CODE_GRP = 'CURRN'
		  AND T101.C101_PARTY_ID = v_gpb_id; 
       ELSE
          SELECT T704.C704_ACCOUNT_NM, T901.C901_CODE_NM,GET_AD_REP_NAME(v_ad_id)
          INTO v_org_name,v_currency,v_ad_name
          FROM T704_ACCOUNT T704,T901_CODE_LOOKUP T901
          WHERE T901.C901_CODE_ID = T704.C901_CURRENCY
          AND T901.C901_CODE_GRP='CURRN'
          AND T704.C704_ACCOUNT_ID = v_ac_id;   
       END IF;
        
       /*- Procedure for getting the who initiated the Price Request */
        gm_pkg_sm_price_approval_txn.gm_get_initiated_team(p_created_by,v_company_id,v_initiated_team);
        
        BEGIN
			SELECT C101_ACCESS_LEVEL_ID,C101_PARTY_ID 
				INTO v_access_level,v_party_id
				FROM T101_USER 
				WHERE C101_USER_ID = p_created_by;	
   		EXCEPTION WHEN NO_DATA_FOUND THEN 
	    	v_access_level := NULL;
  	    END;
         
  	     BEGIN
			 SELECT decode(v_initiated_team,'SATM',v_sales_rep_id,'PRTM',NULL)
          	    INTO v_sales_rep_id
          	    FROM DUAL;	
   		EXCEPTION WHEN NO_DATA_FOUND THEN 
	    	v_sales_rep_id := NULL;
  	    END;
          	
  	    /* If the user access level is less than "3" then the salesrepid will be used to filter the account based on the rep
  	     * If the user access level is >2 and if the user is from SATM then the repid will be passed based on AD and VP
  	     **/
		IF v_access_level < '3' THEN
			v_rep_id := To_CLOB(NVL(v_sales_rep_id,get_rep_id_from_party_id(v_party_id)));
			my_context.set_my_cloblist(v_rep_id || ',');
		ELSIF v_initiated_team = 'SATM' AND v_access_level > '2' THEN		
				 SELECT RTRIM(XMLAGG(XMLELEMENT(e,REP_ID || ',')).EXTRACT('//text()').getclobval(),',') 
					INTO v_rep_id 
				FROM v700_territory_mapping_detail 
				WHERE (ad_id = p_created_by OR vp_id = p_created_by);
			my_context.set_my_cloblist (v_rep_id);
		ELSE
			v_rep_id := NULL;
			my_context.set_my_cloblist (v_rep_id);
		END IF;
		
  OPEN p_out
	FOR
	SELECT * FROM (
			SELECT T7521.C704_ACCOUNT_ID ACCOUNTID,
			  T704.C704_ACCOUNT_NM ACCOUNTNM,
			  NVL(SUM(T7521.C7521_LAST_12MONTH_SALE),0) SALESAMOUNT ,
			  C7521_MONTH SALESMONTH,
			  TO_CHAR(TO_DATE(C7521_MONTH, 'MM'), 'MON')
			  ||' '
			  || C7521_YEAR MONTH_YEAR,
			  C7521_YEAR SALESYEAR,
			  T704.C101_PARTY_ID PARTYID,
			  GET_PARTY_NAME(T704.C101_PARTY_ID) PARTYNM
			FROM T7521_PRICE_IMPACT_ANALY_DTL T7521,
			  T7520_PRICE_IMPACT_ANALYSIS T7520,
			  T207_SET_MASTER T207,
			  T704_ACCOUNT T704
			WHERE T7521.C7520_PRICE_IMPACT_ANALYSIS_ID = T7520.C7520_PRICE_IMPACT_ANALYSIS_ID
			AND T207.C207_SET_ID                       = T7521.C207_SET_ID
			AND T704.C704_ACCOUNT_ID                   = T7521.C704_ACCOUNT_ID
			AND T7521.C7521_VOID_FL                   IS NULL
			AND (t704.c703_sales_rep_id               IN
			  (SELECT TO_CHAR(TOKEN) FROM v_clob_list WHERE TOKEN IS NOT NULL
			  )
			OR v_initiated_team   ='PRTM')
			AND T7520.C7500_ACCOUNT_PRICE_REQUEST_ID = p_request_id
			AND T7521.C7521_SALES_FLAG              IS NOT NULL
			GROUP BY T7521.C704_ACCOUNT_ID,
			  T704.C704_ACCOUNT_NM,
			  C7521_MONTH,
			  C7521_YEAR,
			  T704.C101_PARTY_ID
			UNION ALL
			SELECT '' ACCOUNTID,
			  '' ACCOUNTNM,
			  0 SALESAMOUNT,
			  Month_number SALESMONTH,
			  TO_CHAR(TO_DATE(Month_number, 'MM'), 'MON')
			  ||' '
			  || year_number MONTH_YEAR,
			  year_number SALESYEAR,
			  NULL PARTYID,
			  '' PARTYNM
			FROM date_dim
			WHERE TRUNC(CAL_DATE) >=ADD_MONTHS(TRUNC(CURRENT_DATE ,'MONTH') , -11)
			AND TRUNC(CAL_DATE)   <=TRUNC(SYSDATE)
			GROUP BY MONTH_NUMBER,
			  YEAR_NUMBER
		  )
		ORDER BY SALESYEAR, SALESMONTH;
		  
  OPEN p_header_details
    FOR
	SELECT T7500.c7500_account_price_request_id priceRequestId ,
	  (CASE WHEN (v_access_level < 3) THEN gm_pkg_sm_pricerequest_rpt.get_acc_last12_month_sales(C704_ACCOUNT_ID,c101_gpb_id) 
    ELSE gm_pkg_sm_pricerequest_rpt.get_parent_acc_12_month_sales(c101_gpb_id,c101_gpb_id) END ) last12MonthSales ,
	  GET_COMP_CURR_SYMBOL(v_company_id) AC_CURRENCY ,
	  DECODE(C704_ACCOUNT_ID,'',C101_GPB_ID,C704_ACCOUNT_ID) GPBID,
	   DECODE(C901_REQUEST_TYPE,'903108','',GET_PARTY_NAME(C101_GPB_ID)) PARTYNM ,
	  DECODE(C704_ACCOUNT_ID,'',GET_PARTY_NAME(C101_GPB_ID),GET_ACCOUNT_NAME(C704_ACCOUNT_ID)) ORG_NAME,
	  TO_CHAR (T7520.C7520_GENERATED_DATE, 'mm/dd/yyyy'||' HH24:MI:SS') GEN_DATE
	FROM T7500_ACCOUNT_PRICE_REQUEST T7500 ,T7520_PRICE_IMPACT_ANALYSIS T7520
	WHERE T7500.C7500_ACCOUNT_PRICE_REQUEST_ID = p_request_id
	AND T7500.C7500_ACCOUNT_PRICE_REQUEST_ID   = T7520.C7500_ACCOUNT_PRICE_REQUEST_ID
	AND T7500.C7500_VOID_FL                   IS NULL
	AND T7520.C7520_VOID_FL                   IS NULL;
		 
ELSE
	raise_application_error('-20999','');
   END IF;

END gm_fch_12months_sales_data;

/******************************************************************************
  * Author : karthik
  * Description : procedure to retrieve sales segment data from order 
  ******************************************************************************/
PROCEDURE gm_fch_sales_segment_data(
   p_request_id 	IN	T7520_PRICE_IMPACT_ANALYSIS.C7500_ACCOUNT_PRICE_REQUEST_ID%TYPE
  ,p_created_by		IN	T7520_PRICE_IMPACT_ANALYSIS.C7520_CREATED_BY%TYPE
  ,p_out 			OUT TYPES.cursor_type
  ,p_header_details OUT TYPES.cursor_type
)
AS
  v_Request_typ 	T7500_ACCOUNT_PRICE_REQUEST.C901_REQUEST_TYPE%TYPE;
  v_rec_cnt NUMBER;
  v_ac_id T704_ACCOUNT.C704_ACCOUNT_ID%TYPE;
  v_gpb_id T7500_ACCOUNT_PRICE_REQUEST.C101_GPB_ID%TYPE; 
  v_ad_name V700_TERRITORY_MAPPING_DETAIL.AD_NAME%TYPE;
  v_ad_id T7500_ACCOUNT_PRICE_REQUEST.C101_AD_ID%TYPE;
  v_org_name varchar2(100);
  v_request_type T7500_ACCOUNT_PRICE_REQUEST.C901_REQUEST_TYPE%TYPE;
  v_currency T901_CODE_LOOKUP.C901_CODE_GRP%TYPE;
  v_initiated_team 	VARCHAR2(20);
  v_company_id  	T7500_ACCOUNT_PRICE_REQUEST.C1900_COMPANY_ID%TYPE;
  v_sales_rep_id    CLOB;
  v_access_level	T101_USER.C101_ACCESS_LEVEL_ID%TYPE;
  v_rep_id    		CLOB;
  v_party_id   		T101_USER.C101_PARTY_ID%TYPE;
 BEGIN  
  
	 BEGIN
		SELECT COUNT(T7521.C207_SET_ID) INTO v_rec_cnt
		FROM T7521_PRICE_IMPACT_ANALY_DTL T7521,
	 		 T7520_PRICE_IMPACT_ANALYSIS T7520
		WHERE T7520.C7520_PRICE_IMPACT_ANALYSIS_ID = T7521.C7520_PRICE_IMPACT_ANALYSIS_ID
		AND C7521_VOID_FL                         IS NULL
		AND C7500_ACCOUNT_PRICE_REQUEST_ID         = p_request_id;
   EXCEPTION WHEN NO_DATA_FOUND THEN 
	    	v_rec_cnt := 0;
   END;
 
   IF (v_rec_cnt > 0) THEN
 SELECT T7500.C901_REQUEST_TYPE,T7500.C101_AD_ID, T7500.C704_ACCOUNT_ID, T7500.C101_GPB_ID, T7500.C1900_COMPANY_ID , TO_CLOB(T7500.C703_SALES_REP_ID)
      INTO v_request_type, v_ad_id,v_ac_id,v_gpb_id,v_company_id,v_sales_rep_id
      FROM T7500_ACCOUNT_PRICE_REQUEST T7500
      WHERE T7500.C7500_ACCOUNT_PRICE_REQUEST_ID = p_request_id;
      
       IF v_request_type = '903108' THEN  --Group account 
          v_ad_name := null;
          SELECT T101.C101_PARTY_NM,  T901.C901_CODE_NM
          INTO v_org_name, v_currency 
		  FROM T101_PARTY T101, T1900_COMPANY T1900, T901_CODE_LOOKUP T901
		  WHERE T1900.C1900_COMPANY_ID = T101.C1900_COMPANY_ID
		  AND T1900.C901_TXN_CURRENCY = T901.C901_CODE_ID AND T901.C901_CODE_GRP = 'CURRN'
		  AND T101.C101_PARTY_ID = v_gpb_id; 
       ELSE
          SELECT T704.C704_ACCOUNT_NM, T901.C901_CODE_NM,GET_AD_REP_NAME(v_ad_id)
          INTO v_org_name,v_currency,v_ad_name
          FROM T704_ACCOUNT T704,T901_CODE_LOOKUP T901
          WHERE T901.C901_CODE_ID = T704.C901_CURRENCY
          AND T901.C901_CODE_GRP='CURRN'
          AND T704.C704_ACCOUNT_ID = v_ac_id;   
       END IF;
        
       /*- Procedure for getting the who initiated the Price Request */
        gm_pkg_sm_price_approval_txn.gm_get_initiated_team(p_created_by,v_company_id,v_initiated_team);
        
        BEGIN
			SELECT C101_ACCESS_LEVEL_ID,C101_PARTY_ID 
				INTO v_access_level,v_party_id
				FROM T101_USER 
				WHERE C101_USER_ID = p_created_by;	
   		EXCEPTION WHEN NO_DATA_FOUND THEN 
	    	v_access_level := NULL;
  	    END;
         
  	     BEGIN
			 SELECT decode(v_initiated_team,'SATM',v_sales_rep_id,'PRTM',NULL)
          	    INTO v_sales_rep_id
          	    FROM DUAL;	
   		EXCEPTION WHEN NO_DATA_FOUND THEN 
	    	v_sales_rep_id := NULL;
  	    END;
          	
  	    /* If the user access level is less than "3" then the salesrepid will be used to filter the account based on the rep
  	     * If the user access level is >2 and if the user is from SATM then the repid will be passed based on AD and VP
  	     **/
		IF v_access_level < '3' THEN
			v_rep_id := To_CLOB(NVL(v_sales_rep_id,get_rep_id_from_party_id(v_party_id)));
			my_context.set_my_cloblist(v_rep_id || ',');
		ELSIF v_initiated_team = 'SATM' AND v_access_level > '2' THEN		
				 SELECT RTRIM(XMLAGG(XMLELEMENT(e,REP_ID || ',')).EXTRACT('//text()').getclobval(),',') 
					INTO v_rep_id 
				FROM v700_territory_mapping_detail 
				WHERE (ad_id = p_created_by OR vp_id = p_created_by);
			my_context.set_my_cloblist (v_rep_id);
		ELSE
			v_rep_id := NULL;
			my_context.set_my_cloblist (v_rep_id);
		END IF;
		
  OPEN p_out
	FOR
		SELECT T7521.C207_SET_ID SETID,
		    T7521.C704_ACCOUNT_ID ACCOUNTID,
		    T704.C704_ACCOUNT_NM ACCOUNTNM,
		    T207.C207_SET_NM SETNM,
		    NVL(SUM(T7521.C7521_LAST_12MONTH_SALE),0) SALESAMOUNT ,
		    T207.C901_PRICING_SEGMENT_ID SEGMENTID,
		     GET_CODE_NAME(T207.C901_PRICING_SEGMENT_ID) SEGMENTNM,
		  	T704.C101_PARTY_ID PARTYID,
		  	get_party_name(T704.C101_PARTY_ID) PARTYNM
		  FROM T7521_PRICE_IMPACT_ANALY_DTL T7521,
		    T7520_PRICE_IMPACT_ANALYSIS T7520,
		    T207_SET_MASTER T207,
		    T704_ACCOUNT T704
		  WHERE T7521.C7520_PRICE_IMPACT_ANALYSIS_ID = T7520.C7520_PRICE_IMPACT_ANALYSIS_ID
		  AND T207.C207_SET_ID                       = T7521.C207_SET_ID
		  AND T704.C704_ACCOUNT_ID                   = T7521.C704_ACCOUNT_ID
		  AND T7521.C7521_VOID_FL                   IS NULL
		  AND (t704.c703_sales_rep_id               IN
		    (SELECT TO_CHAR(TOKEN) FROM v_clob_list WHERE TOKEN IS NOT NULL
		    )
		  OR v_initiated_team                      ='PRTM')
		  AND T7520.C7500_ACCOUNT_PRICE_REQUEST_ID = p_request_id
		  AND T7521.C7521_SALES_FLAG 	IS NOT NULL
		  GROUP BY T7521.C207_SET_ID,
		    T7521.C704_ACCOUNT_ID,
		    T704.C704_ACCOUNT_NM,
		    T207.C207_SET_NM,
		    C901_PRICING_SEGMENT_ID,
		    T704.C101_PARTY_ID;
		    
OPEN p_header_details
    FOR
	SELECT T7500.c7500_account_price_request_id priceRequestId ,
	 (CASE WHEN (v_access_level < 3) THEN gm_pkg_sm_pricerequest_rpt.get_acc_last12_month_sales(C704_ACCOUNT_ID,c101_gpb_id) 
    ELSE gm_pkg_sm_pricerequest_rpt.get_parent_acc_12_month_sales(c101_gpb_id,c101_gpb_id) END ) LAST12MONTHSALES ,
	  GET_COMP_CURR_SYMBOL(v_company_id) AC_CURRENCY ,
	  DECODE(C704_ACCOUNT_ID,'',C101_GPB_ID,C704_ACCOUNT_ID) GPBID,
	  DECODE(C901_REQUEST_TYPE,'903108','',GET_PARTY_NAME(C101_GPB_ID)) PARTYNM ,
	  DECODE(C704_ACCOUNT_ID,'',GET_PARTY_NAME(C101_GPB_ID),GET_ACCOUNT_NAME(C704_ACCOUNT_ID)) ORG_NAME,
	  TO_CHAR (T7520.C7520_GENERATED_DATE, 'mm/dd/yyyy'||' HH24:MI:SS') GEN_DATE
	FROM T7500_ACCOUNT_PRICE_REQUEST T7500 ,T7520_PRICE_IMPACT_ANALYSIS T7520
	WHERE T7500.C7500_ACCOUNT_PRICE_REQUEST_ID = p_request_id
	AND T7500.C7500_ACCOUNT_PRICE_REQUEST_ID   = T7520.C7500_ACCOUNT_PRICE_REQUEST_ID
	AND T7500.C7500_VOID_FL                   IS NULL
	AND T7520.C7520_VOID_FL                   IS NULL;
		 
ELSE
	raise_application_error('-20999','');
   END IF;

END gm_fch_sales_segment_data;


END gm_pkg_sm_sales_analysis;
/