/* Formatted on 2010/10/25 19:22 (Formatter Plus v4.8.0) */
--@"C:\database\Packages\sales\GM_PKG_SM_MAPPING.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_sm_mapping
IS
   PROCEDURE gm_sav_asd_region_mapping (
      p_reg_id         IN OUT   t901_code_lookup.c901_code_id%TYPE,
      p_region_name    IN       t901_code_lookup.c901_code_nm%TYPE,
      p_ref_group      IN       t901_code_lookup.c901_code_grp%TYPE,
      p_zone_id   	   IN       t901_code_lookup.c902_code_nm_alt%TYPE,
      p_ad_id          IN       t101_user.c101_user_id%TYPE,
      p_vp_id          IN       t101_user.c101_user_id%TYPE,
      p_user_id        IN       t101_user.c101_user_id%TYPE,
      p_user_type      IN       t901_code_lookup.c901_code_id%TYPE,
      p_division_type  IN       t901_code_lookup.c901_code_id%TYPE,
      p_company_type   IN       t901_code_lookup.c901_code_id%TYPE,
      p_ad_plus_id     IN       t101_user.c101_user_id%TYPE,
      p_open_ad_user   IN       VARCHAR2,
      p_open_vp_user   IN       VARCHAR2,
      p_country_type   IN  		t901_code_lookup.c901_code_id%TYPE
   )
   AS
      v_new_ad_id         	t708_region_asd_mapping.c101_user_id%TYPE;
      v_old_ad_id         	t708_region_asd_mapping.c101_user_id%TYPE;
      v_old_ad_name         t101_user.c101_user_f_name%TYPE;
      v_old_vp_name         t101_user.c101_user_f_name%TYPE;
      v_new_vp_id          	t708_region_asd_mapping.c101_user_id%TYPE;
      v_old_vp_id          	t708_region_asd_mapping.c101_user_id%TYPE;
      v_old_zone_id		   	t901_code_lookup.c901_code_id%TYPE;
      v_reg_id     			t901_code_lookup.c901_code_id%TYPE;
      v_count      NUMBER;
      v_user_ad_count      NUMBER:= NULL;
      v_user_vp_count      NUMBER:= NULL;
      v_region_count  NUMBER;
      v_region_ad_count  NUMBER;
      v_vp_user_count   NUMBER;
      v_vp_count   NUMBER;
      v_ad_count   NUMBER;
      v_party_id        NUMBER:= NULL;
      v_new_user_email         VARCHAR2(100):= 'notification-prod@globusmedical.com';
      v_AD_access_level_id  NUMBER       :=3; -- AD
  	  v_VP_access_level_id  NUMBER       :=4; -- VP
  	  v_zone_name           t901_code_lookup.c901_code_nm%TYPE;
  	  v_old_zone_name           t901_code_lookup.c901_code_nm%TYPE;
  	  v_old_ad_zone_name           	t901_code_lookup.c901_code_nm%TYPE;
  	  v_old_vp_zone_name           	t901_code_lookup.c901_code_nm%TYPE;
  	  v_old_ad_region_name           t901_code_lookup.c901_code_nm%TYPE;
  	  v_old_vp_region_name           t901_code_lookup.c901_code_nm%TYPE;
  	  v_ad_user_f_name         t101_user.c101_user_f_name%TYPE;
      v_vp_user_f_name         t101_user.c101_user_f_name%TYPE;
      v_ad_user_l_name         t101_user.c101_user_l_name%TYPE := ' AD-TBH';
      v_vp_user_l_name         t101_user.c101_user_l_name%TYPE := ' VP-TBH';
   BEGIN
	   v_new_ad_id := p_ad_id;
	   v_new_vp_id := p_vp_id;
	      
      IF p_reg_id IS NULL   THEN
         SELECT MAX (t901.c901_code_seq_no) + 1
           INTO v_count
           FROM t901_code_lookup t901
          WHERE c901_code_grp = p_ref_group;
		
         -- Create a new region in code lookup with zone is an alt name
         gm_pkg_cm_code_lookup.gm_sav_code_lookup_master (NULL, p_region_name, p_ref_group, '1', v_count, p_zone_id, p_user_id);
         
         SELECT c901_code_id 
          INTO v_reg_id
           FROM t901_code_lookup
         WHERE c901_code_grp = p_ref_group 
         AND c901_code_seq_no = v_count;
         
         p_reg_id := v_reg_id;
       END IF;
         
		 UPDATE t901_code_lookup
		 SET c901_last_updated_date = CURRENT_DATE,
		    	c902_code_nm_alt = p_zone_id,
		        c901_last_updated_by = p_user_id
		 WHERE c901_code_id = p_reg_id;

		 -- Get New Zone Name 
		BEGIN
		 SELECT C901_CODE_NM INTO v_zone_name
           FROM t901_code_lookup
         WHERE c901_code_id = p_zone_id;
         EXCEPTION
			WHEN OTHERS THEN
		  v_zone_name := NULL;
		END;
		
		
--		Get OLD Zone Name
		BEGIN
			SELECT GET_CODE_NAME(C901_ZONE_ID) INTO v_old_zone_name
				FROM T710_SALES_HIERARCHY
			WHERE C901_AREA_ID  = p_reg_id
			AND C710_VOID_FL   IS NULL
			AND C710_ACTIVE_FL = 'Y';
			EXCEPTION
			WHEN OTHERS THEN
			  v_old_zone_name := NULL;
		END;      
		
		-- Region and Zone Mapping save 
        it_pkg_sm_sales_mapping.gm_sav_sales_hierarchy(p_division_type, p_zone_id, p_reg_id, p_company_type, p_country_type, p_user_id); --p_country_type param passed.(PC-5628  Country Dropdown - Region AD Setup) 
      
        -- Get Region and Zone Mapping count
       BEGIN
        SELECT count(1) INTO v_region_count
		FROM T710_SALES_HIERARCHY
		WHERE C710_VOID_FL IS NULL
		AND C710_ACTIVE_FL = 'Y'
		AND C901_AREA_ID    = p_reg_id;
      	EXCEPTION
		WHEN OTHERS THEN
		  v_region_count := 2;
		END;
  
		IF v_region_count > 1 THEN
			raise_application_error ('-9999', 'Region (' || p_region_name || ') already mapped to another Zone' || v_old_zone_name);
		END IF;
		
       -- New Open AD creation 
       IF p_open_ad_user = 'Y' THEN
       
       		SELECT COUNT(1) INTO v_user_ad_count
				FROM T101_USER
			WHERE C101_USER_F_NAME = p_region_name;
			
			IF v_user_ad_count > 0 THEN
				v_ad_user_l_name := v_ad_user_l_name || '-' || TO_CHAR(v_user_ad_count+1); 
			END IF;
			
	   		GM_PKG_IT_LOGIN.GM_SAVE_USER (NULL, p_region_name , p_region_name, v_ad_user_l_name, v_new_user_email,'2005', v_AD_access_level_id, p_user_id, '311', '300', '', '', '', '', v_party_id, v_new_ad_id) ;
	   		
	   END IF;
	   
	   -- New Open VP creation 
	   IF p_open_vp_user = 'Y' THEN
	   
	        SELECT COUNT(1) INTO v_user_vp_count
				FROM T101_USER
			WHERE C101_USER_F_NAME = v_zone_name;
			
			IF v_user_vp_count > 0 THEN
				v_vp_user_l_name := v_vp_user_l_name || '-' || TO_CHAR(v_user_vp_count+1); 
	   		END IF;
	   		
	   		GM_PKG_IT_LOGIN.GM_SAVE_USER (NULL, v_zone_name, v_zone_name, v_vp_user_l_name, v_new_user_email,'2005', v_VP_access_level_id, p_user_id, '311', '300', '', '', '', '', v_party_id, v_new_vp_id) ;
	   END IF;
       
	   		-- Get Current AD Details 
		BEGIN
			SELECT GET_USER_NAME(T708.C101_USER_ID) ,
			  GET_CODE_NAME(T708.C901_REGION_ID) ,
			  GET_CODE_NAME(GET_CODE_NAME_ALT ( T708.C901_REGION_ID)) 
			INTO v_old_ad_name, v_old_ad_region_name, v_old_ad_zone_name
			FROM T708_REGION_ASD_MAPPING T708
			WHERE C708_DELETE_FL   IS NULL
			AND C901_USER_ROLE_TYPE = 8000
			-- AND C708_INACTIVE_FL   IS NULL
			AND C901_REGION_ID      = p_reg_id;
		EXCEPTION
		WHEN OTHERS THEN
			  v_old_ad_name := NULL;
		END; 
		
		BEGIN
		IF v_old_ad_name IS NULL THEN
		    SELECT GET_USER_NAME(T708.C101_USER_ID) ,
			  GET_CODE_NAME(T708.C901_REGION_ID) ,
			  GET_CODE_NAME(GET_CODE_NAME_ALT ( T708.C901_REGION_ID)) 
			INTO v_old_ad_name, v_old_ad_region_name, v_old_ad_zone_name
			FROM T708_REGION_ASD_MAPPING T708
			WHERE C708_DELETE_FL   IS NULL
			AND C901_USER_ROLE_TYPE = 8000
			-- AND C708_INACTIVE_FL   IS NULL
			AND T708.C101_USER_ID      = v_new_ad_id;
		 END IF;
		EXCEPTION
		WHEN OTHERS THEN
			  v_old_ad_name := NULL;
		END; 
				
		-- Get Current VP Details 
		BEGIN 
			SELECT GET_USER_NAME(T708.C101_USER_ID) ,
			  GET_CODE_NAME(T708.C901_REGION_ID) ,
			  GET_CODE_NAME(GET_CODE_NAME_ALT ( T708.C901_REGION_ID)) 
			INTO v_old_vp_name, v_old_vp_region_name, v_old_vp_zone_name
			FROM T708_REGION_ASD_MAPPING T708
			WHERE C708_DELETE_FL   IS NULL
			AND C901_USER_ROLE_TYPE = 8001
			-- AND C708_INACTIVE_FL   IS NULL
			AND C901_REGION_ID      = p_reg_id;
		EXCEPTION
		WHEN OTHERS THEN
			  v_old_vp_name := NULL;
		END; 
		
		BEGIN
		IF v_old_vp_name IS NULL THEN
		    SELECT GET_USER_NAME(T708.C101_USER_ID) ,
			  GET_CODE_NAME(T708.C901_REGION_ID) ,
			  GET_CODE_NAME(GET_CODE_NAME_ALT ( T708.C901_REGION_ID)) 
			INTO v_old_vp_name, v_old_vp_region_name, v_old_vp_zone_name
			FROM T708_REGION_ASD_MAPPING T708
			WHERE C708_DELETE_FL   IS NULL
			AND C901_USER_ROLE_TYPE = 8001
			-- AND C708_INACTIVE_FL   IS NULL
			AND T708.C101_USER_ID      = v_new_vp_id;
		 END IF;
		EXCEPTION
		WHEN OTHERS THEN
			  v_old_vp_name := NULL;
		END;
		
        -- AD and VP Mappaing to region
		gm_sav_asd_mapping_master (v_new_ad_id,v_new_vp_id, p_ad_plus_id, p_reg_id,p_zone_id, p_user_type, p_user_id);
		
		-- Get AD, Region count by AD ID
		BEGIN
		SELECT COUNT(1) INTO v_ad_count  FROM (  	
		SELECT T708.C101_USER_ID,  T710.C901_AREA_ID
		FROM T710_SALES_HIERARCHY T710,
		  T708_REGION_ASD_MAPPING T708
		WHERE T710.C901_AREA_ID      = T708.C901_REGION_ID
		AND T708.C708_DELETE_FL     IS NULL
		-- AND T708.C708_INACTIVE_FL   IS NULL
		AND T710.C710_VOID_FL       IS NULL
		AND T710.C710_ACTIVE_FL      = 'Y'
		AND T708.C901_USER_ROLE_TYPE = 8000
		AND T708.C101_USER_ID       = v_new_ad_id
		GROUP BY T708.C101_USER_ID,
		  T710.C901_AREA_ID);
		END;
		IF v_ad_count > 1
	     THEN
	           raise_application_error('-20999','AD  <b>'|| v_old_ad_name ||' </b> already mapped on other Region <b> ' || v_old_ad_region_name ||'</b> Zone is <b>'|| v_old_ad_zone_name ||'</b>');
	     END IF;
	     
	     -- Get AD, Region count by Region ID
		BEGIN
		SELECT COUNT(1) INTO v_region_ad_count FROM (  	
		SELECT T708.C901_USER_ROLE_TYPE, T710.C901_AREA_ID
		FROM T710_SALES_HIERARCHY T710,
		  T708_REGION_ASD_MAPPING T708
		WHERE T710.C901_AREA_ID      = T708.C901_REGION_ID
		AND T708.C708_DELETE_FL     IS NULL
		-- AND T708.C708_INACTIVE_FL   IS NULL
		AND T710.C710_VOID_FL       IS NULL
		AND T710.C710_ACTIVE_FL      = 'Y'
		AND T708.C901_USER_ROLE_TYPE = 8000
		AND T710.C901_AREA_ID        = p_reg_id
		GROUP BY T708.C901_USER_ROLE_TYPE,
		  T710.C901_AREA_ID);
  		END;
		IF v_ad_count > 1
	     THEN
	           raise_application_error('-20999','AD <b>'|| v_old_ad_name ||'</b> already mapped on other Region <b> ' || v_old_ad_region_name ||'</b> Zone is <b>'|| v_old_ad_zone_name ||'</b>');
	     END IF;
	     
	     
	    -- Get VP, Zone count by VP ID 
		BEGIN
			SELECT COUNT(1) INTO v_vp_user_count
			FROM
			  (SELECT T708.C101_USER_ID,
			    T710.C901_ZONE_ID
			  FROM T710_SALES_HIERARCHY T710,
			    T708_REGION_ASD_MAPPING T708
			  WHERE T710.C901_AREA_ID      = T708.C901_REGION_ID
			  AND T708.C708_DELETE_FL     IS NULL
			  -- AND T708.C708_INACTIVE_FL   IS NULL
			  AND T710.C710_VOID_FL       IS NULL
			  AND T710.C710_ACTIVE_FL      = 'Y'
			  AND T708.C901_USER_ROLE_TYPE = 8001
			  AND T708.C101_USER_ID        = v_new_vp_id
			  GROUP BY T708.C101_USER_ID,
			    T710.C901_ZONE_ID
			  );
		END;

		IF v_vp_user_count > 1
	     THEN
	           raise_application_error('-20999','VP <b>'|| v_old_vp_name ||'</b> already mapped on other Zone <b>'|| v_old_vp_zone_name ||'</b>');
	     END IF;
		
	      -- Get VP, Zone count by Zone ID 
	     		BEGIN
			SELECT COUNT(1) INTO v_vp_count
			FROM
			  (SELECT T708.C101_USER_ID,
			    T710.C901_ZONE_ID
			  FROM T710_SALES_HIERARCHY T710,
			    T708_REGION_ASD_MAPPING T708
			  WHERE T710.C901_AREA_ID      = T708.C901_REGION_ID
			  AND T708.C708_DELETE_FL     IS NULL
			  -- AND T708.C708_INACTIVE_FL   IS NULL
			  AND T710.C710_VOID_FL       IS NULL
			  AND T710.C710_ACTIVE_FL      = 'Y'
			  AND T708.C901_USER_ROLE_TYPE = 8001
			  AND T710.C901_ZONE_ID        = p_zone_id
			  GROUP BY T708.C101_USER_ID,
			    T710.C901_ZONE_ID
			  );
		END;

		IF v_vp_count > 1
	     THEN
	           raise_application_error('-20999','VP <b>'|| v_old_vp_name ||'</b> already mapped on other Zone <b>'|| v_old_vp_zone_name ||'</b>');
	     END IF;
	     
	     
   END gm_sav_asd_region_mapping;

/*******************************************************
   * Description : PROCEDURE used to save AD, VP and AD Plus details. 
   * Author      : Jayaprasanth Gurunathan 
*******************************************************/      
   PROCEDURE gm_sav_asd_mapping_master (
      p_ad_id       IN   t708_region_asd_mapping.c101_user_id%TYPE,
      p_vp_id       IN   t708_region_asd_mapping.c101_user_id%TYPE,
      p_ad_plus_id  IN   t708_region_asd_mapping.c101_user_id%TYPE,
      p_reg_id      IN   t708_region_asd_mapping.c901_region_id%TYPE,
      p_zone_id      IN   t708_region_asd_mapping.c901_region_id%TYPE,
      p_user_type   IN   t708_region_asd_mapping.c901_user_role_type%TYPE,
      p_user_id     IN   t708_region_asd_mapping.c708_created_by%TYPE
   )
   AS
   
   		CURSOR C_VP_REGN IS
   		SELECT C901_AREA_ID reg_id
		FROM T710_SALES_HIERARCHY
		WHERE C710_VOID_FL IS NULL
		AND C710_ACTIVE_FL = 'Y'
		AND C901_ZONE_ID    = p_zone_id;
   
   BEGIN	   

	   -- Mapping Region and AD 
      gm_sav_ad_vp_mapping (p_ad_id, p_user_id, p_reg_id, 8000);
      
       -- Mapping Region and VP
--      gm_sav_ad_vp_mapping (p_vp_id, p_user_id, p_reg_id, 8001);
      
      IF p_vp_id IS NOT NULL THEN
      FOR vrec IN C_VP_REGN
			LOOP
				gm_sav_ad_vp_mapping (p_vp_id, p_user_id, vrec.reg_id, 8001);
			END LOOP;
      END IF;
--      
      -- Mapping Region and AD Plus 
      IF p_ad_plus_id IS NOT NULL  THEN
      		it_pkg_sm_sales_mapping.gm_sav_del_asd_map(p_ad_plus_id, NULL, p_reg_id, p_user_id);
        	it_pkg_sm_sales_mapping.gm_sav_asd_regn_map(p_ad_plus_id, 8002, NULL, p_reg_id, p_user_id);
      END IF;
      
   END gm_sav_asd_mapping_master;

/*******************************************************
   * Description : PROCEDURE used to save AD, VP and AD Plus details. 
   * Author      : Jayaprasanth Gurunathan 
*******************************************************/   
   PROCEDURE gm_sav_ad_vp_mapping (
      p_ad_vp_id    IN   t708_region_asd_mapping.c101_user_id%TYPE,
      p_user_id     IN   t708_region_asd_mapping.c708_created_by%TYPE,
      p_reg_id      IN   t708_region_asd_mapping.c901_region_id%TYPE,
      p_user_type   IN   t708_region_asd_mapping.c901_user_role_type%TYPE
   )
   AS
      v_delete_fl   VARCHAR2 (1) := NULL;
      v_inactive_fl   VARCHAR2 (1) := NULL;
      v_old_ad_id t708_region_asd_mapping.c101_user_id%TYPE;
      v_vp_id        t708_region_asd_mapping.c708_created_by%TYPE;
      v_zone_id        t708_region_asd_mapping.c901_region_id%TYPE;
      v_count NUMBER;
      
   BEGIN

      IF p_ad_vp_id IS NULL
      THEN
         v_delete_fl := 'Y';
         v_inactive_fl := 'Y';
      END IF;

      BEGIN
		SELECT C101_USER_ID 
			INTO v_old_ad_id
			 FROM T708_REGION_ASD_MAPPING T708
				WHERE T708.C708_DELETE_FL   IS NULL
				-- AND T708.C708_INACTIVE_FL IS NULL
				AND T708.C901_REGION_ID  = p_reg_id
				AND T708.C901_USER_ROLE_TYPE = p_user_type;
		EXCEPTION
			WHEN NO_DATA_FOUND THEN
			v_old_ad_id := NULL;
	   END;	
			-- p_user_type != 8002 AD Plus void restricted 
         IF v_old_ad_id IS NOT NULL AND v_old_ad_id != p_ad_vp_id AND p_user_type != 8002
         THEN
            it_pkg_sm_sales_mapping.gm_sav_del_asd_map(NULL, v_old_ad_id, p_reg_id, p_user_id);
         END IF;

         
      UPDATE t708_region_asd_mapping
         SET c101_user_id = p_ad_vp_id,
             c708_delete_fl = v_delete_fl,
             c708_inactive_fl = v_inactive_fl,
             c708_last_updated_by = p_user_id,
             c708_last_updated_date = CURRENT_DATE
       WHERE c901_region_id = p_reg_id
         AND c901_user_role_type = p_user_type
         AND c708_delete_fl IS NULL;

      IF SQL%ROWCOUNT = 0 AND p_ad_vp_id IS NOT NULL
      THEN
         INSERT INTO t708_region_asd_mapping
                     (c708_region_asd_id, c101_user_id, c901_region_id,
                      c708_start_dt, c901_user_role_type, c708_created_by,c708_created_date
                     )
              VALUES (s708_region_asd_mapping.NEXTVAL, p_ad_vp_id, p_reg_id,
                      CURRENT_DATE, p_user_type, p_user_id, CURRENT_DATE
                     );
      END IF;
      
   END gm_sav_ad_vp_mapping;
   
/*******************************************************
   * Description : PROCEDURE used to fetch region mapping report and one particular region details as well. 
   * Author      : Jayaprasanth Gurunathan 
*******************************************************/
   PROCEDURE gm_fch_region (
      p_reg_id       IN       t901_code_lookup.c901_code_id%TYPE,
      p_out_cursor   OUT      TYPES.cursor_type
   )
   AS
   v_date_fmt VARCHAR2 (20) ;
   BEGIN
   
		--
		SELECT get_compdtfmt_frm_cntx () INTO v_date_fmt FROM dual;
		--
       			
      OPEN p_out_cursor
       FOR
		  SELECT T710.C901_AREA_ID region_id,
		  GET_CODE_NAME(T710.C901_AREA_ID) region_name,
		  T710.C901_ZONE_ID zone_id,
		  get_code_name(T710.C901_ZONE_ID) zone_nm,
		  T710.C901_DIVISION_ID division_id,
		  T710.C901_COUNTRY_ID country_id,
		  T710.C901_COMPANY_ID comapany_id,
		  vp.vp_id vp_id,
		  get_user_name(vp.vp_id) vp_name,
		  ad.ad_id ad_id,
		  get_user_name(ad.ad_id) ad_name,
		  TO_CHAR ( T710.C710_CREATED_DATE, v_date_fmt ||' HH:MI:SS AM') created_date,
		  get_user_name ( T710.C710_CREATED_BY) created_by,
		  TO_CHAR (T710.C710_LAST_UPDATED_DATE, v_date_fmt ||' HH:MI:SS AM') last_updated_date,
		  get_user_name (T710.C710_LAST_UPDATED_BY) last_updated_by,
		  gm_pkg_sm_mapping.get_region_adplus_users(NVL(p_reg_id,T710.C901_AREA_ID)) ADPLUSUSERS
		FROM T710_SALES_HIERARCHY T710,
		  (SELECT C901_REGION_ID region_id,
		    T708.C101_USER_ID vp_id
		  FROM T708_REGION_ASD_MAPPING T708
		  WHERE C708_DELETE_FL   IS NULL
		  AND C901_USER_ROLE_TYPE = 8001
		  -- AND C708_INACTIVE_FL   IS NULL
		  AND C901_REGION_ID      = NVL(p_reg_id,C901_REGION_ID)
		  )vp,
		  (SELECT C901_REGION_ID region_id,
		    T708.C101_USER_ID ad_id
		  FROM T708_REGION_ASD_MAPPING T708
		  WHERE C708_DELETE_FL   IS NULL
		  AND C901_USER_ROLE_TYPE = 8000
		  -- AND C708_INACTIVE_FL   IS NULL
		  AND C901_REGION_ID      = NVL(p_reg_id,C901_REGION_ID)
		  )AD
		WHERE T710.C901_AREA_ID = vp.region_id
		AND T710.C901_AREA_ID   = ad.region_id
		AND T710.C710_VOID_FL  IS NULL; 
      
   END gm_fch_region;

   PROCEDURE gm_sav_void_region (
      p_txn_id   IN   t901_code_lookup.c901_code_id%TYPE,
      p_userid   IN   t901_code_lookup.c901_last_updated_by%TYPE
   )
   AS
      v_count   NUMBER;
   BEGIN
      SELECT COUNT (1)
        INTO v_count
        FROM t708_region_asd_mapping t708
       WHERE t708.c901_region_id = p_txn_id AND t708.c708_delete_fl IS NULL;

      IF v_count > 0
      THEN
         raise_application_error ('-20321', '');
      END IF;

      UPDATE t901_code_lookup t901
         SET t901.c901_active_fl = 0,
             t901.c901_last_updated_by = p_userid,
             t901.c901_last_updated_date = CURRENT_DATE
       WHERE c901_code_id = p_txn_id;

      dbms_mview.REFRESH ('v700_territory_mapping_detail');
   END gm_sav_void_region;

   PROCEDURE gm_fch_zone (
      p_zone_id         IN       t901_code_lookup.c901_code_id%TYPE,
      p_output_cursor   OUT      TYPES.cursor_type
   )
   AS
   BEGIN
      OPEN p_output_cursor
       FOR
          SELECT t901.c901_code_id zone_id, t901.c901_code_nm zone_name,
                 vp_id, get_user_name (vp_id) vp_name,
                 get_user_name (t901.c901_created_by) created_by,
                 TO_CHAR (t901.c901_created_date, GET_RULE_VALUE('DATEFMT', 'DATEFORMAT')) created_date,
                 get_user_name (t901.c901_last_updated_by) last_updated_by,
                 TO_CHAR (t901.c901_last_updated_date,
                          GET_RULE_VALUE('DATEFMT', 'DATEFORMAT')
                         ) last_updated_date
            FROM (SELECT   t708.c101_user_id vp_id,
                           t901.c902_code_nm_alt zone_id
                      FROM t708_region_asd_mapping t708,
                           t901_code_lookup t901
                     WHERE t901.c901_code_id = t708.c901_region_id
                       AND c901_user_role_type = 8001
                       AND t901.c901_code_grp = 'REGN'
                       AND t708.c708_delete_fl IS NULL
                  GROUP BY c101_user_id, t901.c902_code_nm_alt) t708,
                 t901_code_lookup t901
           WHERE t901.c901_code_grp = 'ZONE'
             AND t901.c901_code_id = t708.zone_id(+)
             AND t901.c901_active_fl = '1';
   END gm_fch_zone;

   PROCEDURE gm_sav_void_zone (
      p_txn_id    IN   t901_code_lookup.c901_code_id%TYPE,
      p_user_id   IN   t901_code_lookup.c901_code_nm%TYPE
   )
   AS
      v_count   NUMBER;
   BEGIN
      SELECT COUNT (1)
        INTO v_count
        FROM t901_code_lookup t901
       WHERE t901.c902_code_nm_alt = TO_CHAR (p_txn_id)
         AND t901.c901_active_fl = '1';

      IF v_count > 0
      THEN
         raise_application_error ('-20315', '');
      END IF;

      UPDATE t901_code_lookup t901
         SET t901.c901_active_fl = 0,
             t901.c901_last_updated_by = p_user_id,
             t901.c901_last_updated_date = CURRENT_DATE
       WHERE c901_code_id = p_txn_id;

      dbms_mview.REFRESH ('v700_territory_mapping_detail');
   END gm_sav_void_zone;
  /*****************************************************************
   * This method will fetch the associate spine specialist based on rep id or all who are mapped to accounts.
   ******************************************************/ 
   PROCEDURE gm_fch_AssocRepList_info(
   	p_out_data OUT TYPES.cursor_type,
   	p_acct_id  IN  t704_account.c704_account_id%TYPE
   )
   AS
		BEGIN
			OPEN p_out_data FOR
				SELECT assocrepnm, assocrepid, repid, repnm,
				GET_REP_EMAILID(assocrepid) ASSOCREPEMAIL
				  FROM
				  (
					SELECT distinct get_rep_name(t704a.c703_sales_rep_id) assocrepnm, t704a.c703_sales_rep_id assocrepid,  
						t703.c703_sales_rep_id repid, t703.c703_sales_rep_name repnm 
					FROM t704a_account_rep_mapping t704a, t704_account t704, t703_sales_rep t703  
					WHERE t704a.c704_account_id = t704.c704_account_id 
					AND t704.c704_account_id = NVL(p_acct_id,t704.c704_account_id)
					AND t703.c703_void_fl IS NULL 
					AND t704a.c704a_void_fl IS NULL 
					AND t704.c704_void_fl IS NULL 
					AND t703.c703_sales_rep_id= t703.c703_sales_rep_id			
					 AND t704.c703_sales_rep_id = t703.c703_sales_rep_id
				 )	 
				ORDER BY UPPER(assocrepnm);   		
   END gm_fch_AssocRepList_info;
   
   /*****************************************************************
   * Thisprocedure will be called to enable a map between rep and account.
	******************************************************/ 
   PROCEDURE  gm_sav_acct_rep_map(
   p_acct_id IN  t704_account.c704_account_id%TYPE,
   p_rep_id  IN  t704_account.c703_sales_rep_id%TYPE,
   p_user_id IN t704_account.c704_CREATED_BY%TYPE
   )
   AS
   
   BEGIN
	   
	UPDATE T704a_Account_Rep_Mapping 
	SET 
		C703_Sales_Rep_Id = p_rep_id,
		C704a_Last_Updated_By = p_user_id,
		C704a_Last_Updated_Date = CURRENT_DATE 
		
	WHERE C704_Account_Id = p_acct_id 
	AND   C703_Sales_Rep_Id = p_rep_id
	AND   c704a_void_fl is NULL;
    
	IF (SQL%ROWCOUNT = 0)
	THEN
	 INSERT INTO T704a_Account_Rep_Mapping 
	 			(C704a_Account_Rep_Map_Id, C704_Account_Id,  C703_Sales_Rep_Id,
	 			 C704a_Active_Fl,  C704a_Void_Fl,  C704a_Created_By, C704a_Created_Date,
	 			 C704a_Last_Updated_By, C704a_Last_Updated_Date )
	 VALUES     (s704a_Account_Rep_Mapping.nextval,p_acct_id,
	             p_rep_id,'Y', NULL,p_user_id,CURRENT_DATE, NULL, NULL);
			
	END IF;
END gm_sav_acct_rep_map;
/************************************************************************************************
   * This procedure will be called to void the rep mapped to an account.and 
   * if a sales rep is terminated this procedure will be called.
  
*************************************************************************************************/ 
   PROCEDURE  gm_sav_rem_acct_map(
   p_acct_id       IN  t704_account.c704_account_id%TYPE,
   p_assocrep_id   IN  VARCHAR2,
   p_party_id       IN t703_sales_rep.c101_party_id%TYPE,
   p_login_user_id IN t704_account.c704_CREATED_BY%TYPE
   )
   AS
   v_rep_id  t704_account.c703_sales_rep_id%TYPE := p_assocrep_id; 
   BEGIN
	
	IF p_party_id IS NOT NULL THEN
        v_rep_id:= get_rep_id_from_party_id(p_party_id);
    END IF;
    
   UPDATE t704a_account_rep_mapping
      SET c704a_void_fl = 'Y' , c704a_last_updated_by = p_login_user_id
          , C704A_LAST_UPDATED_DATE = CURRENT_DATE
   	WHERE c703_sales_rep_id = v_rep_id
     AND c704_account_id = NVL(p_acct_id, c704_account_id)
     AND c704a_void_fl is NULL;

	   
END gm_sav_rem_acct_map;	
/************************************************************************************************
   * This procedure will be called to enable an Associate rep map,here there are two input string
   * p_inputstring - data will be saved into t704a_account_rep_mapping
   * p_voidinputstring - void flag will be updated based on the account id and rep id. 
*************************************************************************************************/ 
	PROCEDURE gm_sav_assocrep_mapping(
	p_acct_id  IN  t704_account.c704_account_id%TYPE,
	p_inputstring IN  VARCHAR2,
	p_voidinputstring IN  VARCHAR2,
	p_user_id     IN t704_account.c704_CREATED_BY%TYPE
	)
AS
   v_string    VARCHAR2 (4000)  := p_inputstring;
   v_substring VARCHAR2 (4000);
   v_acct_id   t704_account.c704_account_id%TYPE;
   v_rep_id    t704_account.c703_sales_rep_id%TYPE;
   v_voidstring    VARCHAR2 (4000)  := p_voidinputstring;
BEGIN
	
	
         		WHILE INSTR (v_string, '|') <> 0
         		LOOP  
	         		 v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
	           		 v_string := SUBSTR (v_string, INSTR (v_string, '|') + 1);
	           		 v_acct_id := TRIM (SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1));
	           		 v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
	            	 v_rep_id :=v_substring;
	            	 gm_sav_acct_rep_map(v_acct_id,v_rep_id,p_user_id);
            	 
            	 END Loop;
	


         		WHILE INSTR (v_voidstring, '|') <> 0
         		LOOP  
	         		 v_substring := SUBSTR (v_voidstring, 1, INSTR (v_voidstring, '|') - 1);
	           		 v_voidstring := SUBSTR (v_voidstring, INSTR (v_voidstring, '|') + 1);
	           		 v_acct_id := TRIM (SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1));
	           		 v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
	            	 v_rep_id :=v_substring;
            	 
            	 	gm_sav_rem_acct_map(v_acct_id,v_rep_id,null,p_user_id);
            	 
            	 END LOOP;
    

END gm_sav_assocrep_mapping;
/************************************************************************************************
   * This procedure is used to get sales hierarchy email Ids based on Account/Rep.  
*************************************************************************************************/ 
PROCEDURE gm_fch_sales_hierarchy_emails (
        p_acct_id IN t704_account.c704_account_id%TYPE,
        p_rep_id  IN t704_account.c703_sales_rep_id%TYPE,
        p_out_data OUT TYPES.cursor_type)
AS
BEGIN
	
    OPEN p_out_data FOR
     SELECT GET_USER_EMAILID (VPID) VPEMAIL ,GET_USER_EMAILID (ADID) ADEMAIL, GET_REP_EMAILID (REPID) REPEMAIL,GET_USER_EMAILID (COLLECTORID) COLLTREMAIL
             ,GET_USER_NAME(COLLECTORID) COLLTRNAME
             from
             (SELECT DISTINCT  VP_ID VPID, AD_ID ADID
         , rep_id repid, gm_pkg_ar_accounts_rpt.get_collector_id(ac_id) collectorid
      
       FROM V700_TERRITORY_MAPPING_DETAIL
      WHERE AC_ID  = NVL (p_acct_id, AC_ID)
        AND rep_id = NVL (p_rep_id, rep_id)) ;
END gm_fch_sales_hierarchy_emails;

/*******************************************************
   * Description : Procedure to fetch sales rep and division mapping details
   * Author      : rdinesh
*******************************************************/
PROCEDURE gm_fch_salesrep_div_map(
   	p_rep_id IN t703_sales_rep.c703_sales_rep_id%TYPE,
   	p_out_cursor OUT Types.cursor_type
)	
  AS
	BEGIN
	  OPEN p_out_cursor
       	FOR
         	SELECT c901_rep_div_map_typ TYPE,t703a.c1910_division_id DIVISION
			  FROM t703a_salesrep_div_mapping t703a,t703_sales_rep t703
		   	 WHERE t703a.c703_sales_rep_id = p_rep_id
       		   AND t703a.c703_sales_rep_id = t703.c703_sales_rep_id
               AND t703.c703_void_fl IS NULL
			   AND t703a.c703a_void_fl IS NULL;
			 	   
END gm_fch_salesrep_div_map;

/*******************************************************
   * Description : Procedure to save the sales rep and division mapping details
   * Author      : rdinesh
*******************************************************/
   PROCEDURE gm_sav_salesrep_div_map (
   		p_inputstr IN CLOB,
   		p_rep_id IN t703_sales_rep.c703_sales_rep_id%TYPE,
   		p_userid IN VARCHAR2
   )
   AS
      v_div_id        t1910_division.c1910_division_id%TYPE;
      v_type   		  t901_code_lookup.c901_code_id%TYPE;
      v_strlen        NUMBER                  := NVL (LENGTH (p_inputstr), 0);
      v_string        CLOB                    := TRIM (p_inputstr);
      v_substring     CLOB;
    
   BEGIN
      IF v_strlen > 0
      THEN

         WHILE INSTR (v_string, '|') <> 0
         LOOP
            v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
            v_string := SUBSTR (v_string, INSTR (v_string, '|') + 1);
            v_div_id := NULL;
            v_type := NULL;
            v_div_id := TO_NUMBER (SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1));
            v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
            v_type := TO_NUMBER (v_substring);
            
          
	        UPDATE t703a_salesrep_div_mapping 
	   		   SET  c901_rep_div_map_typ = v_type
	         WHERE c703_sales_rep_id = p_rep_id
	           AND c1910_division_id = v_div_id;

		      IF (SQL%ROWCOUNT = 0)
		      THEN
		
		         INSERT INTO t703a_salesrep_div_mapping
		                     (
		                      c703a_rep_div_id, c703_sales_rep_id, c1910_division_id, c901_rep_div_map_typ,
		                      c703a_last_updated_by, c703a_last_updated_date
		                     )
		              VALUES (
		              		   S703A_SALESREP_DIV_MAPPING.NEXTVAL, p_rep_id, v_div_id, v_type,
		                       p_userid, CURRENT_DATE
		                     );
			  END IF;
	  
		   END LOOP;
      
       END IF;
	  
END gm_sav_salesrep_div_map;

/*******************************************************
   * Description : Procedure used to fetch unmapped AD list
   * Author      : JGURUNATHAN 
*******************************************************/
   PROCEDURE gm_fch_asd_users (
      p_region_id   		IN   		t901_code_lookup.c901_code_id%TYPE,
      p_dept_id         	IN       	t101_user.c901_dept_id%TYPE,
      p_accesslvl    		IN       	t101_user.c101_access_level_id%TYPE,
      p_asd_roll_type   	IN   		t901_code_lookup.c901_code_id%TYPE,
      p_output_cursor   	OUT      	TYPES.cursor_type
   )
   AS
   
   v_user_id 			t101_user.c101_user_id%TYPE;
   
   BEGIN
	     
 --       IF p_region_id IS NULL THEN
       OPEN p_output_cursor
       FOR
          SELECT T101.c101_user_id ID,
			  T101.c101_user_sh_name
			  || ' '
			  || c101_user_l_name NAME
			FROM t101_user T101
			WHERE T101.c901_dept_id       = p_dept_id
			AND T101.c901_user_status     = 311
			AND T101.c101_access_level_id = p_accesslvl
--			AND NOT EXISTS
--			  (SELECT DISTINCT AD_ID
--			  FROM V700_TERRITORY_MAPPING_DETAIL
--			  WHERE AD_ID = T101.C101_USER_ID
--			  )
			ORDER BY NAME ASC;
--		ELSE
--		
--		
--		BEGIN
--			SELECT C101_USER_ID INTO v_user_id
--				FROM T708_REGION_ASD_MAPPING
--			WHERE C901_REGION_ID    = p_region_id
--			AND C901_USER_ROLE_TYPE = p_asd_roll_type
--			AND C708_DELETE_FL     IS NULL
--			AND C708_INACTIVE_FL   IS NULL;
--			EXCEPTION
--			WHEN OTHERS THEN
--		  	v_user_id := NULL;
--		  	
--		END;
--		OPEN p_output_cursor
--		FOR
--		
--		SELECT T101.c101_user_id ID,
--		  T101.c101_user_sh_name
--		  || ' '
--		  || c101_user_l_name NAME
--		FROM t101_user T101
--		WHERE T101.c901_dept_id       = p_dept_id
--		AND T101.c901_user_status     = 311
--		AND T101.c101_access_level_id = p_accesslvl
--		AND NOT EXISTS
--		  (SELECT DISTINCT AD_ID
--		  FROM V700_TERRITORY_MAPPING_DETAIL
--		  WHERE AD_ID = T101.C101_USER_ID
--		  )
--		UNION
--		SELECT T101.c101_user_id ID,
--		  T101.c101_user_sh_name
--		  || ' '
--		  || c101_user_l_name NAME
--		FROM t101_user T101
--		WHERE T101.c901_dept_id       = p_dept_id
--		AND T101.c901_user_status     = 311
--		AND T101.c101_access_level_id = p_accesslvl
--		AND T101.c101_user_id         = v_user_id
--		ORDER BY NAME ASC;
--
--		END IF;	
   END gm_fch_asd_users;
   
/*******************************************************
   * Description : Procedure used to fetch unmapped AD list
   * Author      : JGURUNATHAN 
*******************************************************/
   PROCEDURE gm_fch_adplus_users (
      p_region_id   		IN   		t901_code_lookup.c901_code_id%TYPE,
      p_dept_id         	IN       	t101_user.c901_dept_id%TYPE,
      p_accesslvl    		IN       	t101_user.c101_access_level_id%TYPE,
      p_output_cursor   	OUT      	TYPES.cursor_type
   )
   AS
   
   v_user_id 			t101_user.c101_user_id%TYPE;
   
   BEGIN
		OPEN p_output_cursor
		FOR
		SELECT T101.c101_user_id ID,
		  T101.c101_user_sh_name
		  || ' '
		  || c101_user_l_name NAME
		FROM t101_user T101
		WHERE T101.c901_dept_id       = p_dept_id
		AND T101.c901_user_status     = 311
		AND T101.c101_access_level_id = p_accesslvl
		ORDER BY NAME ASC;

   END gm_fch_adplus_users;

/*******************************************************
   * Description : Function to fetch ad plus user list for region
   * Author      : Jayaprasanth Gurunathan 
*******************************************************/
FUNCTION get_region_adplus_users(
   	p_region_id   		IN   		t901_code_lookup.c901_code_id%TYPE)
RETURN CLOB
IS
v_outstr CLOB;
BEGIN
	BEGIN
	
	SELECT RTRIM (XMLAGG (XMLELEMENT (e, name || ',')) .EXTRACT ('//text()'), ',') INTO v_outstr
	FROM
	(
	SELECT GET_USER_NAME (T708.C101_USER_ID) name
	FROM T708_REGION_ASD_MAPPING T708
	WHERE C708_DELETE_FL IS NULL
	AND C901_USER_ROLE_TYPE = 8002
	-- AND C708_INACTIVE_FL IS NULL
	AND C901_REGION_ID = p_region_id
	ORDER BY name
	) ;
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
	v_outstr := NULL;
	END;	
 	RETURN v_outstr;
END get_region_adplus_users;

   
   
/*******************************************************
   * Description : Function to fetch sales rep and division mapping details
   * Author      : ARAJAN 
*******************************************************/
FUNCTION get_salesrep_division(
   	p_rep_id IN t703_sales_rep.c703_sales_rep_id%TYPE
)
RETURN 	t703a_salesrep_div_mapping.c901_rep_div_map_typ%TYPE
	
IS
v_division t703a_salesrep_div_mapping.c901_rep_div_map_typ%TYPE;
BEGIN
	BEGIN
		SELECT t703.c1910_division_id INTO v_division
			  FROM t703_sales_rep t703
		   	 WHERE t703.c703_sales_rep_id = p_rep_id
	           AND t703.c703_void_fl IS NULL;
		EXCEPTION
			WHEN NO_DATA_FOUND THEN
				v_division := NULL;
	END;	
	RETURN v_division;
END get_salesrep_division;

END gm_pkg_sm_mapping;
/
