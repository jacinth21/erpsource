--@"C:\Database\Packages\Sales\gm_pkg_sm_loaner_item_alloc.bdy";
CREATE OR REPLACE
PACKAGE BODY gm_pkg_sm_loaner_item_alloc
IS
--
/*********************************
   Purpose: This procedure is to fetch data from t525 to fetch all open request based on planned ship date.
**********************************/
--
PROCEDURE gm_fch_master_requests(
    p_type    		IN  t525_product_request.c901_request_for%TYPE,
    p_prod_dtl_id 	IN 	t526_product_request_detail.c526_product_request_detail_id%TYPE,
    p_out_req 		OUT TYPES.cursor_type
)
AS
BEGIN
  OPEN p_out_req 
    FOR
      SELECT 
        t525.C525_PRODUCT_REQUEST_ID, t526.c526_required_date
          FROM T525_PRODUCT_REQUEST t525,T526_PRODUCT_REQUEST_DETAIL t526
         WHERE t526.c525_product_request_id = TO_CHAR(t525.C525_PRODUCT_REQUEST_ID)
            AND (get_work_days_count(TO_CHAR(SYSDATE,get_rule_value('DATEFMT','DATEFORMAT')),TO_CHAR(t526.c526_required_date,get_rule_value('DATEFMT','DATEFORMAT')))<= 4
            AND TRUNC(t526.c526_required_date - (SYSDATE))>=0)  -- Should be 1 to 4 days from Planned ship date
            AND t525.c525_status_fl ='10' -- Parent Open
            AND t526.c526_status_fl ='10' -- Detail Open
            AND t525.c901_request_for = p_type
            AND t526.c526_product_request_detail_id = NVL(p_prod_dtl_id,c526_product_request_detail_id)
            AND t525.c525_delete_fl is null
            AND t525.c525_void_fl is null
            AND t526.c526_void_fl is null
            AND t526.c205_part_number_id is not null -- To avoid set requests
         GROUP BY t525.C525_PRODUCT_REQUEST_ID,t526.c526_required_date
         ORDER BY t525.C525_PRODUCT_REQUEST_ID;
END gm_fch_master_requests;
--

/*********************************
   Purpose: This procedure is to fetch data from t526 to fetch all open child request based on parent request.
**********************************/
--
PROCEDURE gm_fch_child_requests(
    p_refid         IN t525_product_request.c525_product_request_id%TYPE,
    p_prod_dtl_id 	IN 	t526_product_request_detail.c526_product_request_detail_id%TYPE,    
    p_out_req_det   OUT TYPES.cursor_type
  )
AS
BEGIN
  OPEN p_out_req_det 
    FOR
      SELECT t526.c205_part_NUMBER_id, t526.c526_qty_requested, t526.c901_request_type 
        FROM T526_PRODUCT_REQUEST_DETAIL t526
       WHERE t526.c525_product_request_id = p_refid
         AND t526.c526_product_request_detail_id = NVL(p_prod_dtl_id,c526_product_request_detail_id)
         AND t526.c526_void_fl is null
         AND t526.c205_part_number_id is not null
         AND t526.c526_status_fl in ('10'); -- Open  check => c526_status_fl, '5', 'Pending Approval', '10', 'Open', '20', 'Allocated', '30', 'Closed','40', 'Rejected'
END gm_fch_child_requests;
--

/*********************************
   Purpose: This procedure is called from Job, to run whole Inhouse process
**********************************/
--
  PROCEDURE gm_init_loaner_item_req
  AS
  BEGIN
  		gm_sav_loaner_item_req(NULL,4119,30301);
  END gm_init_loaner_item_req;
/*********************************
   Purpose: This procedure is called from screen while approve the request
**********************************/
--
  PROCEDURE gm_sav_loaner_item_req(
  	 p_prod_dtl_id 	IN 	t526_product_request_detail.c526_product_request_detail_id%TYPE,
  	 p_req_type 	IN 	t526_product_request_detail.c901_request_type%TYPE,
  	 p_user_id	 	IN  t526_product_request_detail.c526_last_updated_by%TYPE
  )
  AS
    v_cur_req     TYPES.cursor_type;
    v_cur_req_det TYPES.cursor_type;
    v_plnd_ship_dt t526_product_request_detail.c526_required_date%TYPE;
    v_ref_id      VARCHAR2(20);
    v_ref_qty     NUMBER;
    v_ihln_id     VARCHAR2(20);
    v_fgih_id     VARCHAR2(20);
    v_ihrq_id     VARCHAR2(20);
    v_req_type    t526_product_request_detail.C901_request_type%TYPE;
    v_part_num    t205_part_NUMBER.c205_part_NUMBER_id%TYPE;
    v_msg         clob;
BEGIN

  gm_pkg_sm_loaner_item_alloc.gm_fch_master_requests(p_req_type,p_prod_dtl_id,v_cur_req); -- Get Parent requests for Inhouse Loaner. Hardcode as of now

  LOOP
    FETCH v_cur_req
      INTO v_ref_id, v_plnd_ship_dt;

    EXIT WHEN v_cur_req%NOTFOUND;

    gm_pkg_sm_loaner_item_alloc.gm_fch_child_requests(v_ref_id,p_prod_dtl_id, v_cur_req_det); -- Get Child requests

    IF (v_cur_req_det IS NOT NULL) THEN
      LOOP  -- Loop each child request details
        FETCH v_cur_req_det
        INTO v_part_num, v_ref_qty, v_req_type;

        EXIT WHEN v_cur_req_det%NOTFOUND;

        BEGIN
          IF (v_ref_qty>0)
          THEN
            gm_pkg_sm_loaner_item_alloc.gm_sav_inhouse_shelf(v_ref_id,v_req_type, v_part_num, v_ihln_id, v_ref_qty);  -- for IHLN
          END IF;

        IF (v_ref_qty>0)
        THEN
          gm_pkg_sm_loaner_item_alloc.gm_sav_fg_shelf(v_ref_id, v_req_type, v_part_num, v_fgih_id, v_ref_qty);  -- for FGIH
        END IF;

        IF (v_ref_qty>0)
        THEN
          gm_pkg_sm_loaner_item_alloc.gm_sav_bo_req(v_ref_id, v_req_type , v_part_num, v_ihrq_id, v_ref_qty);  -- for IHRQ
        END IF;

        EXCEPTION WHEN OTHERS THEN  -- When any error happened append the below message and throw finally as a report
          v_msg := v_msg || 'In Request ' || v_ref_id || ', Part number  ' || v_part_num || ' is not transacted ' || 'Error Message:' || SQLERRM;
        END;

      END LOOP;
    END IF;

  END LOOP;
  
  IF v_cur_req%ISOPEN then
  	CLOSE v_cur_req;
  END IF;
  IF v_cur_req_det%ISOPEN THEN
  	CLOSE v_cur_req_det;
  END IF;

  IF (length(v_msg)>0)
  THEN
    raise_application_error (-20999, v_msg);
  END IF;

END gm_sav_loaner_item_req;
--


/*********************************
   Purpose: This procedure is to create or update IHLN
**********************************/
--
PROCEDURE gm_sav_inhouse_shelf(
    p_refid     IN     t525_product_request.c525_product_request_id%TYPE,
    p_reftype   IN     t526_product_request_detail.C901_request_type%TYPE,
    p_pnum      IN     t205_part_NUMBER.c205_part_NUMBER_id%TYPE,
    p_txn_id    IN OUT  VARCHAR2,
    p_bo_qty    IN OUT  NUMBER
)
AS
    v_rem_qty NUMBER ;
    v_ih_qty NUMBER;
    v_part_num t205_part_NUMBER.c205_part_NUMBER_id%TYPE;
BEGIN
    -- Get available quantities
    v_ih_qty:=GET_QTY_IN_INHOUSEINV(p_pnum);

    IF (v_ih_qty<0) THEN  -- If Negative make it zero
      v_ih_qty :=0;
    END IF;
   
    IF (v_ih_qty < p_bo_qty) -- If IH do not have req qty
    THEN
      p_bo_qty:= p_bo_qty - v_ih_qty;
    ELSE
      v_ih_qty:= p_bo_qty;
      p_bo_qty:=0;
    END IF;

    IF (v_ih_qty>0)
    THEN
      gm_pkg_sm_loaner_item_alloc.gm_chk_inhouse_txn(p_refid,p_reftype,p_pnum,9110, 30301, v_ih_qty,p_txn_id);-- 9110>IHLN  30301-Job User id
    END IF;

END gm_sav_inhouse_shelf;
--

/*********************************
   Purpose: This procedure is to create or update FGIH
**********************************/
--
PROCEDURE gm_sav_fg_shelf(
    p_refid     IN     t525_product_request.c525_product_request_id%TYPE,
    p_reftype   IN     t526_product_request_detail.C901_request_type%TYPE,
    p_pnum      IN     t205_part_NUMBER.c205_part_NUMBER_id%TYPE,
    p_txn_id    IN OUT  VARCHAR2,
    p_bo_qty    IN OUT  NUMBER
)
AS
    v_rem_qty NUMBER ;
    v_fg_qty NUMBER;
    v_fg_appr_qty NUMBER;
    v_part_num t205_part_NUMBER.c205_part_NUMBER_id%TYPE;
BEGIN
    -- Get available quantity
    v_fg_qty := get_fg_shelf_qty(p_pnum);
    v_fg_appr_qty := get_sales_forecast_sales(p_pnum);
    
    v_fg_qty := v_fg_qty - v_fg_appr_qty;

    IF (v_fg_qty<0) THEN  -- If Negative make it zero
      v_fg_qty :=0;
    END IF;
    
    IF (v_fg_qty < p_bo_qty) -- If IH do not have req qty
    THEN
      p_bo_qty:= p_bo_qty - v_fg_qty;
    ELSE
      v_fg_qty:= p_bo_qty;
      p_bo_qty:=0;
    END IF;

    IF (v_fg_qty>0)
    THEN
      gm_pkg_sm_loaner_item_alloc.gm_chk_inhouse_txn(p_refid,p_reftype, p_pnum, 9106, 30301, v_fg_qty, p_txn_id);-- 9106=>FGIH  30301-Job User id
    END IF;

END gm_sav_fg_shelf;
--

/*********************************
   Purpose: This procedure is to create or update IHRQ
**********************************/
--
PROCEDURE gm_sav_bo_req(
    p_refid     IN     t525_product_request.c525_product_request_id%TYPE,
    p_reftype   IN     t526_product_request_detail.C901_request_type%TYPE,
    p_pnum      IN     t205_part_NUMBER.c205_part_NUMBER_id%TYPE,
    p_txn_id    IN OUT  VARCHAR2,
    p_bo_qty    IN OUT  NUMBER
)
AS
BEGIN

  gm_pkg_sm_loaner_item_alloc.gm_chk_inhouse_txn(p_refid, p_reftype, p_pnum, 1006483, 30301, p_bo_qty,p_txn_id); -- 1006483=>IHRQ  30301-Job User id

END gm_sav_bo_req;
--

/*********************************
   Purpose: This procedure checks the any existing txn available or not
**********************************/
--
PROCEDURE gm_chk_inhouse_txn(
    p_refid      IN    VARCHAR2,
    p_reftype    IN    NUMBER,
    p_pnum       IN    t205_part_NUMBER.c205_part_NUMBER_id%TYPE,
    p_operation  IN    NUMBER,
    p_userid     IN    T412_INHOUSE_TRANSACTIONS.C412_CREATED_BY%TYPE,
    p_bo_qty     IN NUMBER,
    p_txn_id     IN OUT VARCHAR2      
)
AS
    v_price    T413_INHOUSE_TRANS_ITEMS.C413_ITEM_PRICE%TYPE;
    v_txnid    VARCHAR2(20);
    v_cnt      NUMBER;
    v_ref_dtl_id t505_item_consignment.c505_item_consignment_id%TYPE;
    v_status_fl	 T412_INHOUSE_TRANSACTIONS.C412_STATUS_FL%TYPE;
    v_ctrl_req 	 t906_rules.c906_rule_value%TYPE;
    v_ship_to 	 t907_shipping_info.c901_ship_to%TYPE;
    v_ship_to_id t907_shipping_info.c907_ship_to_id%TYPE;
    v_address_id t907_shipping_info.c106_address_id%TYPE;
    v_req_dtl_id t526_product_request_detail.c526_product_request_detail_id%TYPE;
    v_shipping_id t907_shipping_info.c907_shipping_id%TYPE;
    v_purpose     T412_INHOUSE_TRANSACTIONS.c412_inhouse_purpose%TYPE;
    v_detail_status  T413_INHOUSE_TRANS_ITEMS.c413_status_fl%TYPE;
    v_ship_mode  t907_shipping_info.C901_DELIVERY_MODE%TYPE;
    v_ship_carrier t907_shipping_info.c901_delivery_carrier%TYPE;
    v_ship_to_dt t907_shipping_info.c907_ship_to_dt%TYPE;
    v_user_id    t525_product_request.c525_created_by%TYPE;
    v_return_date t526_product_request_detail.c526_exp_return_date%TYPE;
    v_inv_type NUMBER;
BEGIN
  SELECT NVL(get_part_price (p_pnum, 'L'),0) 
  INTO v_price FROM dual;

  v_user_id :=p_userid;
  
  BEGIN
    IF (p_userid = 30301) THEN  -- If Manual load, get the user id of request creator
      SELECT t525.c525_created_by 
        INTO v_user_id
        FROM t525_product_request t525
       WHERE t525.c525_product_request_id = p_refid;
    END IF;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    v_user_id:=null;    
  END;

  v_txnid := p_txn_id;
  --Types will be FGIH(9106),BLIH(9104), IHRQ(1006483), IHLN(9110),VDIH(9117),BOIH(9116) 

  v_purpose   := get_rule_value ('TRANS_PURPOSE', p_operation);

  -- For IHRQ detail record status should be 'Q' and for VDIH -> 'V'. Rest will be 'R'
  SELECT (DECODE(p_operation,1006483,'Q',9117,'V','R'))
  INTO v_detail_status from dual;


  IF p_operation in (9106,9104,1006483,9116,9117,50150,1006570,1006573)
  THEN
  	v_ctrl_req  := get_rule_value ('CONTROL_REQUIRED', p_operation); 
  	
	IF  v_ctrl_req = 'Y'
	THEN
			v_status_fl	:= '2';
	ELSIF v_ctrl_req IS NOT NULL AND v_ctrl_req != 'N' -- For VDIH-9117,IHBO-9116 status is verified (4) 
	THEN
			v_status_fl := v_ctrl_req;
	ELSE
	  SELECT (DECODE(p_operation,1006483,'0','3')) -- For IHRQ parent status should be 0 for others 3
            INTO v_status_fl from dual;
	END IF;  
  
    BEGIN
      SELECT T412.C412_INHOUSE_TRANS_ID
        INTO v_txnid
        FROM T412_INHOUSE_TRANSACTIONS t412
       WHERE c412_ref_id= p_refid
         AND c412_type = p_operation
         AND T412.C412_VOID_FL IS NULL
         AND T412.C412_STATUS_FL = v_status_fl; -- pending control
    EXCEPTION WHEN NO_DATA_FOUND THEN
      v_txnid := null;
    END;
                
    IF v_txnid is null 
    THEN
    
       BEGIN
	     SELECT t907.c901_ship_to,c907_ship_to_id,c106_address_id ,t907.C901_DELIVERY_MODE,t907.c901_delivery_carrier, c907_ship_to_dt , t526.c526_exp_return_date
	       INTO v_ship_to,v_ship_to_id, v_address_id, v_ship_mode, v_ship_carrier, v_ship_to_dt , v_return_date
		   FROM t907_shipping_info t907, t526_product_request_detail t526
		  WHERE t907.c907_ref_id             = TO_CHAR(t526.c526_product_request_detail_id)
		    AND t526.c525_product_request_id = t907.c525_product_request_id
		    AND t907.c525_product_request_id = p_refid
		    AND t907.c901_source = 50186
		    AND t526.c205_part_number_id = p_pnum
		    AND t526.c526_status_fl IN(10,20) --Open, Allocated
		    AND t526.c526_void_fl IS NULL
		    AND t907.c907_void_fl IS NULL
		    AND ROWNUM = 1;
	    EXCEPTION WHEN NO_DATA_FOUND THEN
	      v_ship_to		:= NULL;
	      v_ship_to_id	:= NULL;
	      v_address_id	:= NULL;
	      v_ship_mode 	:= NULL;
	      v_ship_to_dt  := NULL;
	   END;
    
      gm_pkg_op_request_master.gm_sav_inhouse_txn(
        null,
        v_status_fl,                  -- Status flag
        v_purpose,                 -- purpose
        p_operation,          -- type
        p_refid,              -- refid
        p_reftype,
        v_ship_to,
        v_ship_to_id,
        v_user_id,            
        v_txnid
      );
    END IF;
      gm_pkg_op_request_master.gm_sav_inhouse_txn_dtl (
      v_txnid,
      p_pnum,
      p_bo_qty,
      NULL, -- control NUMBER
      v_price, --Price
      v_detail_status,    -- status
      v_user_id
      );
    
     --Whenever the detail is updated, the inhouse transactions table has to be updated ,so ETL can Sync it to GOP.
	 UPDATE t412_inhouse_transactions 
	   SET c412_last_updated_by = v_user_id
	     , c412_last_updated_date = SYSDATE
	 WHERE c412_inhouse_trans_id = v_txnid;	
 
      p_txn_id := v_txnid;

      -- Insert FGIH transaction for Device (in t5050)
      IF p_operation in (9106) THEN
        BEGIN
          SELECT c901_code_id
            INTO v_inv_type
            FROM t901_code_lookup t901
           WHERE t901.c901_code_grp = 'INVPT'
             AND t901.c902_code_nm_alt = p_operation -- In House Set
             AND t901.c901_active_fl = '1';

          gm_pkg_allocation.gm_ins_invpick_assign_detail (v_inv_type,
                                                         p_txn_id,
                                                         v_user_id
                                                        );
        EXCEPTION WHEN OTHERS THEN
          v_inv_type:=NULL;
        END;
      END IF;
  END IF;

    BEGIN 
      SELECT c526_product_request_detail_id 
        INTO v_req_dtl_id
        FROM t526_product_request_detail
       WHERE c525_product_request_id = p_refid
         AND c205_part_number_id = p_pnum
         AND c526_status_fl IN ('10','20') --Open,Allocated
         AND c526_void_fl IS NULL
         AND ROWNUM = 1;
      EXCEPTION WHEN NO_DATA_FOUND THEN
        v_req_dtl_id  := NULL;
    END;   
      
  IF p_operation in (9110)
  THEN
 	v_txnid := get_consignid_bsd_shipdtl(p_refid,p_reftype,p_pnum);
    
    IF v_txnid IS NULL
    THEN
    
    	BEGIN
	     SELECT t907.c901_ship_to,c907_ship_to_id,c106_address_id ,t907.C901_DELIVERY_MODE,t907.c901_delivery_carrier, t526.c526_required_date , t526.c526_exp_return_date
	       INTO v_ship_to,v_ship_to_id, v_address_id, v_ship_mode, v_ship_carrier, v_ship_to_dt , v_return_date
		   FROM t907_shipping_info t907, t526_product_request_detail t526
		  WHERE t907.c907_ref_id             = TO_CHAR(t526.c526_product_request_detail_id)
		    AND t526.c525_product_request_id = t907.c525_product_request_id
		    AND t907.c525_product_request_id = p_refid
		    AND t907.c901_source = 50186
		    AND t526.c205_part_number_id = p_pnum
		    AND t526.c526_status_fl IN(10,20) --Open, Allocated
		    AND t526.c526_void_fl IS NULL
		    AND t907.c907_void_fl IS NULL
		    AND ROWNUM = 1;
	    EXCEPTION WHEN NO_DATA_FOUND THEN
	      v_ship_to		:= NULL;
	      v_ship_to_id	:= NULL;
	      v_address_id	:= NULL;
	      v_ship_mode 	:= NULL;
	      v_ship_to_dt  := NULL;
	    END;
	    
      gm_pkg_op_request_master.gm_sav_consignment(
        null,   -- distributor_id       
        null,   -- v_ship_date  check
        v_return_date,   -- v_return_date
        gm_pkg_sm_eventbook_txn.get_event_name(p_refid),   -- comments    
        null,   -- total_cost
        '2',    -- Pending Control
        null,   -- void_fl            
        '01',   -- account_id
        v_ship_to,   -- v_ship_to Check
        null,   -- set_id    
        v_ship_to_id,   -- v_ship_to_id
        null,   -- final_comments
        v_purpose,   -- inhouse_purpose Check
        p_operation,  -- type to create IHLN
        5001,   -- delivery_carrier
        5004,   -- delivery_mode
        null,   -- tracking_number
        1,   -- ship_req_fl
        null,   -- verify_fl
        null,   -- verified_date
        null,   -- verified_by
        null,   -- update_inv_fl
        null,   -- reprocess_id
        null,   -- master_consignment_id
        null,   -- request_id 
        v_user_id,  -- v_created_by check
        p_refid,   -- refid
        p_reftype,  -- refType
	v_txnid -- out consignment_id
      );


      gm_pkg_cm_shipping_trans.gm_sav_ship_info (v_txnid
														 , 50186
														 , v_ship_to
														 , v_ship_to_id
														 , v_ship_carrier
														 , v_ship_mode
														 , NULL
														 , v_address_id
														 , 0
														 , v_user_id
														 , v_shipping_id
														  );
														  
	    UPDATE t907_shipping_info
		   SET c525_product_request_id = p_refid 
		     , c907_ship_to_dt = v_ship_to_dt
			 , c907_last_updated_by = v_user_id
			 , c907_last_updated_date = SYSDATE
		WHERE  c907_shipping_id = v_shipping_id AND c907_void_fl IS NULL;													  
    END IF;

    -- Insert in t505
    gm_pkg_op_request_master.gm_sav_consignment_detail (
        NULL,                                          -- p_item_consignment_id Check
        v_txnid,    --consignment_id
        p_pnum,    -- part_number_id
        null,     -- control_number                   -- Check
        p_bo_qty,  -- item_qty
        v_price,   -- item_price                      -- List Price
        NULL,     --  void_fl
        p_operation,  -- type
        p_refid,         -- ref_id
        v_ref_dtl_id  -- out_item_consigment_id       -- Check
    );
    
    --When ever the child is updated/Inserted, the consignment table has to be updated ,so ETL can Sync it to GOP.
	UPDATE t504_consignment
	   SET c504_last_updated_by   = v_user_id
	     , c504_last_updated_date = SYSDATE
	 WHERE c504_consignment_id = v_txnid;	

    IF v_req_dtl_id IS NOT NULL THEN-- UPDATE request to allocated status   
      gm_sav_void_ship_dtl(p_refid,v_req_dtl_id,v_user_id);   
    END IF;
    p_txn_id := v_txnid;
  END IF;  
  
   -- Update request detail to allocated state
    gm_pkg_sm_loaner_item_alloc.gm_sav_req_dtl(v_req_dtl_id);

END gm_chk_inhouse_txn;
--/*********************************

/*********************************
   Purpose: This procedure is to void all IHRQ on event start date.
   Author: Ritesh
**********************************/
--
PROCEDURE gm_void_ihloaner_req
AS 
cursor c_ihrq is
SELECT     ihrq_dtl.c412_inhouse_trans_id transid
    FROM
        (SELECT     t412.c412_ref_id, t412.c412_inhouse_trans_id
            FROM t412_inhouse_transactions t412
            WHERE t412.c412_status_fl IN ('0','3')
                AND t412.c412_type = 1006483
                AND t412.c412_void_fl IS NULL
        ) ihrq_dtl                          
    , (SELECT DISTINCT t7104.c7100_case_info_id, t525.c525_product_request_id
            FROM t525_product_request t525  , t526_product_request_detail t526, t7104_case_set_information t7104
              , t7100_case_information t7100, t7103_schedule_info t7103
            WHERE t525.c525_product_request_id = t526.c525_product_request_id
                AND t7104.c526_product_request_detail_id = t526.c526_product_request_detail_id
                AND t7104.c7100_case_info_id = t7100.c7100_case_info_id
                AND t7103.c7100_case_info_id = t7100.c7100_case_info_id
                AND t7100.c901_type = 1006504
                AND t7103.c7103_start_date = TRUNC (sysdate)
                AND t7100.c7100_void_fl IS NULL
                AND t525.c525_void_fl IS NULL
                AND t526.c526_void_fl IS NULL
        ) event_dtl
    WHERE ihrq_dtl.c412_ref_id = event_dtl.c525_product_request_id;

BEGIN 
                FOR v_rec IN c_ihrq
                Loop
                  --gm_pkg_common_cancel_op.gm_op_sav_void_inhousetxn(v_rec.transid,30301);
                  gm_pkg_sm_eventbook_txn.gm_sav_void_txn(v_rec.transid,'INHOUSE',30301);
                End loop;

END gm_void_ihloaner_req;
--
/*********************************
   Purpose: This procedure is used to update request status and void shipping record.
   Author: Gopinathan
**********************************/
PROCEDURE gm_sav_void_ship_dtl(
    p_req_id      IN    t526_product_request_detail.c525_product_request_id%TYPE,
    p_req_dtl_id  IN    t526_product_request_detail.C526_Product_Request_Detail_Id%TYPE,
    p_user_id     IN    t526_product_request_detail.c526_last_updated_by%TYPE
)
AS
BEGIN
  UPDATE t907_shipping_info
   SET c907_active_fl = 'N'  -- Inactive the record instead of void, as more than one IHLNs may be available  
     WHERE c907_ref_id = to_char(p_req_dtl_id)
       AND c525_product_request_id = p_req_id
       AND c901_source = 50186 
       AND c907_void_fl IS NULL;
END gm_sav_void_ship_dtl;

/*********************************
   Purpose: This procedure is used to update request details status
   Author: Yoga
**********************************/
PROCEDURE gm_sav_req_dtl(
    p_req_dtl_id  IN    t526_product_request_detail.C526_Product_Request_Detail_Id%TYPE
)
AS
BEGIN
	UPDATE t526_product_request_detail 
       SET c526_status_fl = 20 
     WHERE c526_product_request_detail_id = p_req_dtl_id
       AND c526_void_fl IS NULL;
  
END gm_sav_req_dtl;
/*********************************
   Purpose: This function is used to compare part req and existing IHLN shipping details
  			if both are same ship detail then return existing consignId(IHLN)
   Author: Gopinathan
**********************************/
FUNCTION get_consignid_bsd_shipdtl(
    p_ref_id    IN    t526_product_request_detail.c525_product_request_id%TYPE,
    p_ref_type  IN    t526_product_request_detail.c901_request_type%TYPE,    
    p_pnum      IN    t526_product_request_detail.c205_part_number_id%TYPE
)
RETURN VARCHAR2
IS
     	v_consign_id         VARCHAR2 (20);
BEGIN
  BEGIN
	SELECT txn.c504_consignment_id INTO v_consign_id 
	  FROM
	    (SELECT t907.c901_ship_to,c907_ship_to_id,DECODE(c106_address_id,0,null,c106_address_id) address_id ,t907.c901_delivery_mode,T907.C901_Delivery_Carrier, T526.C526_Required_Date ship_to_dt 
		   FROM t907_shipping_info t907, t526_product_request_detail t526
		  WHERE t907.c907_ref_id             = TO_CHAR(t526.c526_product_request_detail_id)
		    AND t526.c525_product_request_id = t907.c525_product_request_id
		    AND t907.c525_product_request_id = p_ref_id
		    AND t907.c901_source = DECODE(p_ref_type,4119,50186,c901_source)
		    AND t526.c205_part_number_id = p_pnum
		    AND t526.c205_part_number_id IS NOT NULL
		    AND t526.c526_void_fl IS NULL) Req,
		(SELECT t504.c504_consignment_id, t907.c901_ship_to,c907_ship_to_id,DECODE(c106_address_id,0,NULL,c106_address_id) address_id
		      , t907.c901_delivery_mode,t907.c901_delivery_carrier, c907_ship_to_dt ship_to_dt
           FROM t907_shipping_info t907,t504_consignment t504
          WHERE t907.c907_void_fl   IS NULL
            AND t907.c901_source     = DECODE(p_ref_type,4119,50186,c901_source) --50186- Loaner Items
    		AND t907.c907_status_fl IN (15)
    		AND t504.c504_ref_id= p_ref_id
            AND t504.c504_consignment_id = t907.c907_ref_id
            AND t504.c901_ref_type = p_ref_type
            AND t504.c504_status_fl = '2'
            AND t504.c504_void_fl IS NULL) txn
     WHERE NVL(txn.c901_ship_to,-999) = NVL(req.c901_ship_to,-999)
 	   AND NVL(txn.c907_ship_to_id,-999) = NVL(req.c907_ship_to_id,-999)
       AND NVL(txn.address_id,-999) = NVL(req.address_id,-999)
       AND NVL(txn.c901_delivery_mode,-999) = NVL(req.c901_delivery_mode,-999)
       AND NVL(txn.c901_delivery_carrier,-999) = NVL(req.c901_delivery_carrier,-999)
       AND TRUNC(txn.ship_to_dt) = TRUNC(req.ship_to_dt);
	 EXCEPTION
	     WHEN OTHERS
	     THEN
	          RETURN NULL;
	END;
     RETURN v_consign_id;
END get_consignid_bsd_shipdtl;
END gm_pkg_sm_loaner_item_alloc;
/