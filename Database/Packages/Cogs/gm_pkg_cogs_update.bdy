/* Formatted on 2009/10/22 13:20 (Formatter Plus v4.8.0) */
-- @"c:\database\packages\cogs\gm_pkg_cogs_update.bdy"
CREATE OR REPLACE
PACKAGE BODY gm_pkg_cogs_update
IS
    --
PROCEDURE gm_cs_sav_transfer_cogs (
        p_trns_id           IN t920_transfer.c920_transfer_id%TYPE,
        p_to_distributor_id IN t920_transfer.c920_to_ref_id%TYPE,
        p_user_id           IN t920_transfer.c920_created_by%TYPE,
        p_trns_mode         IN t920_transfer.c901_transfer_mode%TYPE)
AS
    /***************************************************************************************************
    * This Procedure is to accept the transfer already initiated, if the transfer type is inhouse to distributor
    this procedure will be called.
    * Author : vprasath
    * Date    : 02/25/06
    ***************************************************************************************************/
    --
    CURSOR cogs_transfer
    IS
         SELECT cons.partnum pnum, (NVL (consign, 0) - NVL (RETURN, 0)) qty
           FROM
            (
                 SELECT t505.c205_part_number_id partnum, SUM (t505.c505_item_qty) consign
                   FROM t505_item_consignment t505, t504_consignment t504
                  WHERE t505.c504_consignment_id IN
                    (
                         SELECT c921_ref_id
                           FROM t921_transfer_detail
                          WHERE c920_transfer_id = p_trns_id
                    )
                AND t505.c504_consignment_id         = t504.c504_consignment_id
                AND t504.c504_void_fl               IS NULL
                AND TRIM (t505.c505_control_number) IS NOT NULL
           GROUP BY t505.c205_part_number_id
            )
            cons, (
                 SELECT t507.c205_part_number_id partnum, SUM (t507.c507_item_qty) RETURN
                   FROM t507_returns_item t507, t506_returns t506
                  WHERE t506.c506_rma_id IN
                    (
                         SELECT c921_ref_id
                           FROM t921_transfer_detail
                          WHERE c920_transfer_id = p_trns_id
                            AND 1                = DECODE (p_trns_mode, 90350, 1, 0)
                    )
                    AND t507.c506_rma_id                 = t506.c506_rma_id
                    AND t506.c506_void_fl               IS NULL
                    AND TRIM (t507.c507_control_number) IS NOT NULL
                    AND t507.c507_status_fl              = 'C'
               GROUP BY t507.c205_part_number_id
            )
            ret
          WHERE cons.partnum                          = ret.partnum(+)
            AND (NVL (consign, 0) - NVL (RETURN, 0)) <> 0;
        --
        v_consign_id	t921_transfer_detail.c921_ref_id%TYPE;
        v_rma_id		t921_transfer_detail.c921_ref_id%TYPE;
        v_from_id	   t920_transfer.c920_from_ref_id%TYPE;
		v_to_id 	   t920_transfer.c920_to_ref_id%TYPE;
		v_txn_mode	   t920_transfer.c901_transfer_mode%TYPE;
    BEGIN
        --
        -- to fetch the dist ids
        	SELECT	c920_from_ref_id, c920_to_ref_id, c901_transfer_mode
					 INTO v_from_id, v_to_id, v_txn_mode
					 FROM t920_transfer
					WHERE c920_transfer_id = p_trns_id;
        	-- To fetch the consignment ID selected tranfer id
             SELECT t921.c921_ref_id
               INTO v_consign_id
               FROM t921_transfer_detail t921
              WHERE t921.c920_transfer_id = p_trns_id
                AND t921.c901_link_type   = 90360;
              -- To fetch the Retruns ID based on tranfer id
                 SELECT t921.c921_ref_id
               INTO v_rma_id
               FROM t921_transfer_detail t921
              WHERE t921.c920_transfer_id = p_trns_id
              AND t921.c901_link_type   = 90361;  

        FOR cogs_val IN cogs_transfer
        LOOP
            --
            IF cogs_val.qty > 0 THEN
                --
                gm_save_ledger_posting ('48062', CURRENT_DATE, cogs_val.pnum, p_to_distributor_id, p_trns_id, cogs_val.qty,
                NULL, p_user_id) ;
                --
            END IF;
            --
        END LOOP;
        --
        --If v_txn_mode is excess returns(90353) then do not update field sales warehouse since during excess reconciliation we donot create a TFCN transaction and during excess creation we update fs warehouse for all excess.
        IF v_txn_mode <> 90353 THEN
	        -- update the CN Qty
	        -- 4301 - Plus
	        -- 102900 - Consignment
			-- 102909 - Transfers
			   gm_pkg_op_inv_field_sales_tran.gm_update_fs_inv(v_consign_id, 'T505', v_to_id, 102900, 4301, 102909, p_user_id);
		END IF;	   
    END gm_cs_sav_transfer_cogs;
    --
    /***************************************************************************************************
    * This Procedure accepts the error id from "t822_costing_error_log" and calls "gm_save_ledger_posting"
    based on the posting_type. Currently there is no provision for updating "posting_type" in T822, this
    has to be done manually with an update statement. Currently this procedure is not being called from
    any of the code, it is purely to fix COGS error.
    * Author : Mihir
    ***************************************************************************************************/
PROCEDURE gm_cogs_correction (
        p_error_id IN t822_costing_error_log.c822_costing_error_log_id%TYPE -- Error ID
        ,
        p_user IN t822_costing_error_log.c822_created_by%TYPE,
        p_type IN t822_costing_error_log.c901_fix_type%TYPE -- Type C, F Cancel/Fix
    )
AS
    v_posting_type t822_costing_error_log.c901_posting_type%TYPE;
    v_party_id t822_costing_error_log.c810_party_id%TYPE;
    v_qty t822_costing_error_log.c822_qty%TYPE;
    v_partnum t822_costing_error_log.c205_part_number_id%TYPE;
    v_trans_id t822_costing_error_log.c822_txn_id%TYPE;
    v_error_type t822_costing_error_log.c901_error_type%TYPE;
    v_company_id t822_costing_error_log.c901_company_id%TYPE;
    v_country_id t822_costing_error_log.c901_country_id%TYPE;
    v_new_company_id t822_costing_error_log.c901_company_id%TYPE;
    v_new_country_id t822_costing_error_log.c901_country_id%TYPE;
    -- International posting changes
    v_owner_comp_id t822_costing_error_log.c1900_owner_company_id%TYPE;
    v_trans_comp_id t822_costing_error_log.c1900_txn_company_id%TYPE;
    v_destination_comp_id t822_costing_error_log.c1900_destination_company_id%TYPE;
    v_comp_id t822_costing_error_log.c1900_company_id%TYPE;
    v_plant_id t5040_plant_master.c5040_plant_id%TYPE;
    v_std_cost NUMBER;
    v_std_cost_type t901_code_lookup.c901_code_id%TYPE;
    --
    v_local_comp_cost t822_costing_error_log.c822_local_company_cost%TYPE;
    --
    CURSOR txtp_cursor
	IS
		SELECT	 c901_account_type_dr account_dr, NULL account_cr
			   , get_posting_table_ref (c901_account_type_dr) table_ref_nm, c901_posting_type p_type
			   , c901_from_costing_type f_type, c901_to_costing_type t_type, 1 seq_id, c804_posting_ref_id ref_id
			FROM t804_posting_ref
		   WHERE c901_transaction_type = v_posting_type AND c901_account_type_dr IS NOT NULL
		UNION
		SELECT	 NULL account_dr, c901_account_type_cr account_cr
			   , get_posting_table_ref (c901_account_type_cr) table_ref_nm, c901_posting_type p_type
			   , c901_from_costing_type v_posting_type, c901_to_costing_type t_type, 2 seq_id, c804_posting_ref_id ref_id
			FROM t804_posting_ref
		   WHERE c901_transaction_type = v_posting_type AND c901_account_type_cr IS NOT NULL
		ORDER BY ref_id, seq_id;
BEGIN
	SELECT c901_posting_type, c810_party_id, c822_qty
          , c205_part_number_id, c822_txn_id, c901_error_type
          , c901_company_id, c901_country_id
          , c1900_owner_company_id, NVL(c1900_txn_company_id, -999)
          , c1900_destination_company_id, c1900_company_id
          , get_plantid_frm_cntx ()
          -- PC-991: To get the local cost from DB and pass to posting
          , c822_local_company_cost
           INTO v_posting_type, v_party_id, v_qty
          , v_partnum, v_trans_id, v_error_type
          , v_company_id, v_country_id
          , v_owner_comp_id, v_trans_comp_id
          , v_destination_comp_id, v_comp_id
          , v_plant_id
          , v_local_comp_cost
           FROM t822_costing_error_log
          WHERE c822_costing_error_log_id = p_error_id FOR UPDATE;
  IF v_error_type = 102940 -- Costing Error
    THEN -- existing flow
	    IF p_type              = 53008 THEN
	        IF v_posting_type IS NULL THEN
	            raise_application_error ('-20549', 'Posting type cannot be blank for '|| v_trans_id) ;
	        END IF;
	        -- if check company id and TXN company id not equal then set the context value
	        IF v_comp_id <>  v_trans_comp_id
	        THEN
	        	gm_pkg_cor_client_context.gm_sav_client_context(v_trans_comp_id,v_plant_id);
	        END IF;
	        --
	        gm_save_ledger_posting (v_posting_type, CURRENT_DATE, v_partnum, v_party_id, v_trans_id, v_qty, NULL, p_user, v_destination_comp_id, v_owner_comp_id, v_local_comp_cost) ;
	    END IF; -- end p_type
   ELSIF v_error_type = 102941 -- Non Costing Error
    THEN
    IF p_type = 53008 THEN -- Repost
        -- get the Country and Company id
        v_new_company_id    := get_companyid_from_pnum (v_partnum) ;
        v_new_country_id    := get_countryid_from_partyid (v_posting_type, v_party_id) ;
        IF v_new_company_id IS NULL OR v_new_country_id IS NULL THEN
            raise_application_error ('-20999', 'Company or Country not found for the transaction - '|| v_trans_id) ;
        END IF;
        IF v_posting_type IS NULL THEN
	            raise_application_error ('-20549', 'Posting type cannot be blank for '|| v_trans_id) ;
	    END IF;
        FOR cur_in IN txtp_cursor
        LOOP
            gm_update_company_country_id (cur_in.table_ref_nm, v_posting_type, v_trans_id, v_partnum, v_new_company_id,
            v_new_country_id, p_user) ;
        END LOOP;
    END IF; -- end p_type
  ELSIF v_error_type = 26240362 -- Zero Costing Error
    THEN
    IF p_type = 53008 THEN -- Repost
    	-- to get the standard cost type - based on Vendor
    	v_std_cost_type := gm_pkg_pur_vendor_cost.get_vendor_cost_type (v_party_id);
    	IF NVL(v_std_cost_type, '-999') = '106561'
    	THEN
	    	-- to get the standard Cost
	    	v_std_cost := gm_pkg_pur_vendor_cost.get_vendor_std_cost (v_party_id, v_partnum);
    	ELSE
    		-- to get the FG costing
    		v_std_cost := get_ac_cogs_value (v_partnum, 4900);
    	END IF;
    	
    	-- to call the ledger posting
    	gm_save_ledger_posting (v_posting_type, CURRENT_DATE, v_partnum, v_party_id, v_trans_id, v_qty, v_std_cost, p_user, v_destination_comp_id, v_owner_comp_id, v_std_cost) ;
    END IF; -- end if p_type
  END IF; -- end v_error_type
	 UPDATE t822_costing_error_log
	SET c901_fix_type                 = decode(p_type,26241378,NULL,p_type), c822_fixed_by = p_user, c822_fixed_date = SYSDATE
	  WHERE c822_costing_error_log_id = p_error_id;
END gm_cogs_correction;
/*********************************************************
* Description : This procedure is used to corect COGS Error
*********************************************************/
PROCEDURE gm_sav_correct_cogs_err (
        p_inputstr VARCHAR2,
        p_user_id IN t101_user.c101_user_id%TYPE)
AS
    v_strlen    NUMBER           := NVL (LENGTH (p_inputstr), 0) ;
    v_string    VARCHAR2 (30000) := p_inputstr;
    v_substring VARCHAR2 (1000) ;
    v_errid t822_costing_error_log.c822_costing_error_log_id%TYPE;
    v_status t822_costing_error_log.c901_fix_type%TYPE;
    --
BEGIN
    WHILE INSTR (v_string, '|') <> 0
    LOOP
        v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
        v_string    := SUBSTR (v_string, INSTR (v_string, '|')    + 1) ;
        v_errid     := NULL;
        v_status    := NULL;
        v_errid     := SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1) ;
        v_substring := SUBSTR (v_substring, INSTR (v_substring, ',')    + 1) ;
        v_status    := v_substring;
        IF v_errid  IS NOT NULL AND v_status IS NOT NULL THEN
            gm_pkg_cogs_update.gm_cogs_correction (v_errid, p_user_id, v_status) ;
        END IF;
    END LOOP;
    --
END gm_sav_correct_cogs_err;

/*********************************************************
* Description : This procedure is used update the Company and Country id
*********************************************************/
PROCEDURE gm_update_company_country_id (
        p_table_ref  IN t805_posting_table_ref.c805_table_identifier%TYPE,
        p_txntype    IN t810_posting_txn.c901_account_txn_type%TYPE,
        p_txn_id     IN t810_posting_txn.c810_txn_id%TYPE,
        p_partnum    IN t810_posting_txn.c205_part_number_id%TYPE,
        p_company_id IN t810_posting_txn.c901_company_id%TYPE,
        p_country_id IN t810_posting_txn.c901_country_id%TYPE,
        p_user       IN t810_posting_txn.c810_created_by%TYPE)
AS
BEGIN
    -- to update company id and country id AP table
    UPDATE t810_posting_txn
        SET c901_company_id           = p_company_id, c901_country_id = p_country_id, c810_last_updated_by = p_user
          , c810_last_updated_date    = SYSDATE
          WHERE c901_account_txn_type = p_txntype
            AND c810_txn_id           = p_txn_id
            AND c205_part_number_id   = p_partnum
            AND c810_delete_fl        = 'N'
            AND (c901_company_id     IS NULL
    		OR c901_country_id       IS NULL) ;
END gm_update_company_country_id;
END gm_pkg_cogs_update;
/
