CREATE OR REPLACE PACKAGE BODY GM_PKG_PUR_WO_VENDOR_COMMIT  
IS
       
    /*********************************************************************
	Description : This function is used to save work order the WO attributes.
	Author: 
	*********************************************************************/
	PROCEDURE gm_sav_vendor_commit (
		p_po_id         IN T401_PURCHASE_ORDER.C401_PURCHASE_ORD_ID%TYPE,
	    p_input_str  	IN CLOB,
	    p_userid     	IN T402B_WO_VENDOR_COMMIT.C402B_LAST_UPDATED_BY%TYPE
	    )
	AS
	 v_string	   	  CLOB := p_input_str;
	   v_substring    VARCHAR2 (3000);
	   v_wo           T402B_WO_VENDOR_COMMIT.C402_WORK_ORDER_ID%TYPE;
	   v_commitqty     T402B_WO_VENDOR_COMMIT.C402B_COMMIT_QTY%TYPE;
	   v_commitdt   varchar2(100);
	   v_dateformat varchar2(20);
	   
	   v_company_id  T401_PURCHASE_ORDER.C1900_COMPANY_ID%TYPE;
	   v_dt_history_fl T402B_WO_VENDOR_COMMIT.C402B_COMMIT_DATE_HISTORY_FL%TYPE;
	   v_qty_history_fl T402B_WO_VENDOR_COMMIT.C402B_COMMIT_QTY_HISTORY_FL%TYPE;
	   v_vendor_commit_id T402B_WO_VENDOR_COMMIT.C402B_VENDOR_COMMIT_ID%TYPE;
	   v_part_num   T402_WORK_ORDER.C205_PART_NUMBER_ID%TYPE;
	BEGIN
	
SELECT C1900_COMPANY_ID, get_compdtfmt_frm_cntx()  INTO v_company_id, v_dateformat
			FROM T401_PURCHASE_ORDER
			WHERE  C401_PURCHASE_ORD_ID = p_po_id;
	
	-- to void the Vendor commit records (to handle spilt/merge issue)
	
	 UPDATE t402b_wo_vendor_commit
SET c402b_void_fl           = 'Y', c402b_last_updated_by = p_userid, c402b_last_updated_date = current_date
  WHERE c402_work_order_id in
    (
         SELECT c402_work_order_id
           FROM t402_work_order
          WHERE c401_purchase_ord_id = p_po_id
            AND c402_void_fl        IS NULL
    )
    AND c402b_void_fl IS NULL;
			
			
		WHILE INSTR (v_string, '|') <> 0
		LOOP
		v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
		v_string     := SUBSTR (v_string, INSTR (v_string, '|') + 1);
		v_vendor_commit_id         := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
	 	v_substring  := SUBSTR (v_substring, INSTR (v_substring, '^') + 1) ;
		v_wo         := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
	 	v_substring  := SUBSTR (v_substring, INSTR (v_substring, '^') + 1) ;
	 	v_commitdt         := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
	 	v_substring  := SUBSTR (v_substring, INSTR (v_substring, '^') + 1) ;
       	v_commitqty    := v_substring ;
       --
       --call Save work order qty procedure
       gm_pkg_pur_wo_vendor_commit.gm_sav_wo_vendor_commit (v_vendor_commit_id, v_wo, v_commitqty, to_date(v_commitdt,v_dateformat), p_userid);	
       
		END LOOP;
		-- to compare and save the remaing WO qty to Vendor Commit table
		
		 gm_pkg_pur_wo_vendor_commit.gm_sav_remain_commit_qty (p_po_id, p_userid);
		 
	 	-- to validate the WO qty
	 	
	 	gm_pkg_pur_wo_vendor_commit.gm_validate_commit_qty(p_po_id);
	 		 
END gm_sav_vendor_commit;


	 /*********************************************************************
	Description : This function is used to fetch work order details
	Author: 
	*********************************************************************/
 	PROCEDURE gm_fch_vendor_commit_dtls (
        p_po_id             IN   T401_PURCHASE_ORDER.C401_PURCHASE_ORD_ID%TYPE
       ,p_out         OUT   TYPES.cursor_type
    )
    AS
       v_company_id  T401_PURCHASE_ORDER.C1900_COMPANY_ID%TYPE;
 
    BEGIN
        
        	SELECT C1900_COMPANY_ID INTO v_company_id
			FROM T401_PURCHASE_ORDER
			WHERE  C401_PURCHASE_ORD_ID = p_po_id;
         OPEN p_out FOR 
                
                SELECT t402.c205_part_number_id pnum, t205.C205_PART_NUM_DESC pdesc ,t402.c402_work_order_id woid
                 , t402.C402_QTY_ORDERED wo_qty, t402b.C402B_COMMIT_DATE commit_date, t402b.C402B_COMMIT_DATE_HISTORY_FL commit_dt_fl
                 , t402b.C402B_COMMIT_QTY commit_qty, t402b.C402B_COMMIT_QTY_HISTORY_FL commit_qty_fl
                 ,t402b.c402b_vendor_commit_id vendor_commit_id
                 FROM t401_purchase_order t401, t402_work_order t402, t205_part_number t205
                 , t402b_wo_vendor_commit t402b
                 WHERE t401.c401_purchase_ord_id = t402.c401_purchase_ord_id
                      AND t402.c205_part_number_id  = t205.c205_part_number_id
                      AND t402.C402_WORK_ORDER_ID = t402b.c402_work_order_id (+)
                      AND T401.C1900_COMPANY_ID     = v_company_id
                      AND T402.c402_qty_ordered       > 0
                      AND T402.c402_status_fl      < 3
                      AND t402.c402_void_fl        IS NULL
                      AND t401.c401_void_fl        IS NULL
                      AND t402b.C402B_VOID_FL (+) IS NULL
                      AND t401.C401_PURCHASE_ORD_ID = p_po_id
                    order by t402.c205_part_number_id,t402b.c402b_vendor_commit_id;
 
                         
     END gm_fch_vendor_commit_dtls;
     
     /*********************************************************************
	Description : This function is used to validate the wo qty and commit qty
	Author: 
	*********************************************************************/
     PROCEDURE gm_validate_commit_qty (
      p_po_id             IN     T401_PURCHASE_ORDER.C401_PURCHASE_ORD_ID%TYPE
     )
	AS
		v_qty VARCHAR2(20);
		wo_id          T402B_WO_VENDOR_COMMIT.C402_WORK_ORDER_ID%TYPE;
	    v_partnum      VARCHAR2 (20) ;
	    v_wo_qty       VARCHAR2 (20) ;
	    v_vendor_commit_qty VARCHAR2 (20) ;
		wo_id_msg       CLOB;
	CURSOR vendor_commit_wo_cur
    IS
        -- to get the Vendor commit WO id
       
         SELECT t402b.C402_WORK_ORDER_ID wo_id
           FROM T401_PURCHASE_ORDER t401, T402_WORK_ORDER t402, T402B_WO_VENDOR_COMMIT t402b
          WHERE t401.C401_PURCHASE_ORD_ID = t402.C401_PURCHASE_ORD_ID
            AND t402.C402_WORK_ORDER_ID   = t402b.C402_WORK_ORDER_ID
            AND t401.C401_PURCHASE_ORD_ID = p_po_id
            AND T402.C402_QTY_ORDERED       > 0
            AND t402.C402_VOID_FL        IS NULL
            AND t401.C401_VOID_FL        IS NULL
            AND t402b.C402B_VOID_FL        IS NULL
       GROUP BY t402b.C402_WORK_ORDER_ID ;
	BEGIN
    -- loop the vendor commit WO and compare the qty
    FOR vendor_commit IN vendor_commit_wo_cur
    LOOP
        -- 1 get the WO qty
         SELECT C402_QTY_ORDERED
           INTO v_wo_qty
           FROM T402_WORK_ORDER
          WHERE C402_WORK_ORDER_ID = vendor_commit.wo_id
            AND c402_void_fl      IS NULL;
           
        -- 2. to get the sum of vendor commit qty
         SELECT SUM (C402B_COMMIT_QTY)
           INTO v_vendor_commit_qty
           FROM T402B_WO_VENDOR_COMMIT
          WHERE C402_WORK_ORDER_ID = vendor_commit.wo_id
            AND C402B_VOID_FL      IS NULL;
         -- 3 compate the Qty    
           IF v_wo_qty <> v_vendor_commit_qty THEN
            -- add the WO id to string
         	  wo_id_msg := wo_id_msg || vendor_commit.wo_id || ',';
           END IF;
    END LOOP;
    
  		IF wo_id_msg IS NOT NULL THEN
  			--
  			SELECT substr(wo_id_msg, 0, length(wo_id_msg) - 1) INTO wo_id_msg FROM DUAL;
  			--
            raise_application_error ('-20500', 'Sum of the Commit Qty should be equal to the WO Qty for ' || wo_id_msg ) ;
        END IF;
END gm_validate_commit_qty;

	/*********************************************************************
	Description : This function is used to update the vendor commit qty while update WO qty
	Author: 
	*********************************************************************/
	PROCEDURE gm_update_vendor_commit_qty (
		p_work_order_id IN T402_WORK_ORDER.C402_WORK_ORDER_ID%TYPE,
		p_new_work_order_id IN T402_WORK_ORDER.C402_WORK_ORDER_ID%TYPE,
		p_user_id IN T402_WORK_ORDER.C402_last_updated_by%TYPE)
	
	AS
	    v_wo_qty T402_WORK_ORDER.C402_QTY_ORDERED%TYPE;
	    v_vendor_commit_qty T402B_WO_VENDOR_COMMIT.C402B_COMMIT_QTY%TYPE;
	    v_cnt NUMBER;
	    v_vendor_commit_id T402B_WO_VENDOR_COMMIT.C402B_VENDOR_COMMIT_ID%TYPE;
	    v_tmp_work_order_id T402_WORK_ORDER.C402_WORK_ORDER_ID%TYPE;
	    v_commit_date DATE;
	  
	BEGIN
    -- loop the vendor commit WO and update/void
         SELECT COUNT(1) INTO v_cnt
           FROM T402B_WO_VENDOR_COMMIT
          WHERE C402_WORK_ORDER_ID = p_work_order_id
            AND C402B_VOID_FL      IS NULL;
          
          IF   v_cnt = 0 
          THEN
          RETURN;
          END IF;
   
        -- 1 get the WO qty
         SELECT c402_qty_ordered
           INTO v_wo_qty
           FROM T402_WORK_ORDER
          WHERE C402_WORK_ORDER_ID =  p_work_order_id
            AND c402_void_fl      IS NULL;
            
        -- 2. to get the sum of vendor commit qty
         SELECT SUM (C402B_COMMIT_QTY)
           INTO v_vendor_commit_qty
           FROM T402B_WO_VENDOR_COMMIT
          WHERE C402_WORK_ORDER_ID = p_work_order_id
            AND C402B_VOID_FL      IS NULL;
            
        -- 3 compate the Qty
        IF (v_wo_qty <> v_vendor_commit_qty OR p_new_work_order_id IS NOT NULL) THEN
        
            -- to get the first record
             SELECT MIN (C402B_VENDOR_COMMIT_ID)
               INTO v_vendor_commit_id
               FROM T402B_WO_VENDOR_COMMIT
              WHERE C402_WORK_ORDER_ID = p_work_order_id
                AND C402B_VOID_FL      IS NULL;
                
            -- to void the duplicate records
            
            
             UPDATE T402B_WO_VENDOR_COMMIT
            SET C402B_VOID_FL               = 'Y', C402B_LAST_UPDATED_BY = 30301, C402B_LAST_UPDATED_DATE = sysdate
              WHERE C402_WORK_ORDER_ID     = p_work_order_id
                AND C402B_VENDOR_COMMIT_ID <> v_vendor_commit_id
                AND C402B_VOID_FL          IS NULL;
                
            -- to update the commit qty
            
             UPDATE T402B_WO_VENDOR_COMMIT
            SET C402B_COMMIT_QTY           = v_wo_qty,
            C402B_LAST_UPDATED_BY = p_user_id, C402B_LAST_UPDATED_DATE = sysdate
              WHERE C402B_VENDOR_COMMIT_ID = v_vendor_commit_id
                AND C402B_VOID_FL         IS NULL;
                
  END IF;       
    --
  
  IF p_new_work_order_id IS NOT NULL
  THEN
   -- 1 get the WO qty
         SELECT c402_qty_ordered
           INTO v_wo_qty
           FROM T402_WORK_ORDER
          WHERE C402_WORK_ORDER_ID =  p_new_work_order_id
            AND c402_void_fl      IS NULL;
            
     -- 2 get the Vendor commit date
     SELECT C402B_COMMIT_DATE
               INTO v_commit_date
               FROM T402B_WO_VENDOR_COMMIT
              WHERE C402B_VENDOR_COMMIT_ID = v_vendor_commit_id
                AND C402B_VOID_FL         IS NULL;
           
    --Call save wo vendor commit procedure 
    gm_pkg_pur_wo_vendor_commit.gm_sav_wo_vendor_commit (NULL, p_new_work_order_id, v_wo_qty, v_commit_date, p_user_id);	
  
  END IF;
  
END gm_update_vendor_commit_qty;
/*********************************************************************
	Description : This function is used to fetch Vendor Commit report Details
	Author: 
	*********************************************************************/
PROCEDURE gm_fch_vendor_commit_report (
        p_vendorid          IN   T301_VENDOR.c301_vendor_id%TYPE
       ,p_projid            IN   T202_PROJECT.c202_project_id%TYPE
       ,p_partnum           IN   VARCHAR2
       ,p_closed_wo_fl      IN   t402_work_order.C402_STATUS_FL%TYPE
       ,p_commit_from_date  IN   VARCHAR2
       ,p_commit_to_date    IN   VARCHAR2
       ,p_out         OUT   TYPES.cursor_type
	)
	AS
		v_company_id    	T1900_COMPANY.C1900_COMPANY_ID%TYPE;
	    v_dateformat VARCHAR2(100);
   	BEGIN
		
		SELECT get_compid_frm_cntx(),get_compdtfmt_frm_cntx()
		  INTO v_company_id,v_dateformat
		  FROM dual;       
	   
	     OPEN p_out FOR 
				SELECT t402b.c205_part_number_id pnum, t205.C205_PART_NUM_DESC pdesc ,t402.c402_work_order_id woid,t401.C401_PURCHASE_ORD_ID POID
				 , t402.C402_QTY_ORDERED wo_qty, t402b.C402B_COMMIT_DATE commit_date, t402b.C402B_COMMIT_DATE_HISTORY_FL commit_date_fl
				 , t402b.C402B_COMMIT_QTY commit_qty, t402b.C402B_COMMIT_QTY_HISTORY_FL commit_qty_fl
				 , t202.C202_PROJECT_ID project_id, t202.C202_PROJECT_NM project_name
				 , t301.C301_VENDOR_NAME vendor_name, t401.C401_CREATED_DATE po_date
				 , t401.C401_DATE_REQUIRED Required_Date,ROUND (t402b.C402B_COMMIT_DATE - CURRENT_DATE) commit_due_days
				 , get_latest_log_comments (t402b.C402B_VENDOR_COMMIT_ID, 1500) lastcomments 
				 ,t402b.C402B_VENDOR_COMMIT_ID vendor_commit_id
				 FROM t401_purchase_order t401, t402_work_order t402, t205_part_number t205,
				 T202_PROJECT t202, T402B_WO_VENDOR_COMMIT t402b, T301_VENDOR t301
				 WHERE t401.c401_purchase_ord_id = t402.c401_purchase_ord_id
					  AND t402.c205_part_number_id  = t205.c205_part_number_id
					  AND t202.C202_PROJECT_ID = t205.C202_PROJECT_ID
					  AND t401.C301_VENDOR_ID = t301.C301_VENDOR_ID
					  AND t301.C301_VENDOR_ID = t402.C301_VENDOR_ID
					  AND t402.C402_WORK_ORDER_ID = t402b.C402_WORK_ORDER_ID
					  AND T401.C1900_COMPANY_ID     = v_company_id
					  AND t402.c402_void_fl        IS NULL
					  AND t401.c401_void_fl        IS NULL
					  AND t402b.C402B_VOID_FL  IS NULL
					  AND T402.c402_qty_ordered       > 0 --Get Ordered Qty greater than zero
				      AND t301.c301_vendor_id       = DECODE (p_vendorid, '', t301.c301_vendor_id, p_vendorid)
				      AND t202.c202_project_id      = DECODE (p_projid, '', t202.c202_project_id, p_projid)
				    AND regexp_like (t402b.c205_part_number_id, NVL (REGEXP_REPLACE(p_partnum,'[+]','\+'), REGEXP_REPLACE(t402b.c205_part_number_id,'[+]','\+')))   
				      AND T402.c402_status_fl <= DECODE(p_closed_wo_fl, 'Y', 3 , 2)
					  AND t402b.C402B_COMMIT_DATE 
				      BETWEEN  NVL(to_date(p_commit_from_date,v_dateformat),t402b.C402B_COMMIT_DATE) 
				      AND NVL(to_date(p_commit_to_date,v_dateformat),t402b.C402B_COMMIT_DATE)
				      order by t202.C202_PROJECT_NM, t402b.c205_part_number_id;					 
 	END gm_fch_vendor_commit_report;
  /*******************************************************************************
	Description : This function is used to fetch Vendor acknowledge report Details
	Author: asingaravel
  ********************************************************************************/
	PROCEDURE gm_fch_vendor_wo_ack_report (
        p_vendorid          IN   T301_VENDOR.c301_vendor_id%TYPE
       ,p_projid            IN   T202_PROJECT.c202_project_id%TYPE
       ,p_partnum           IN   VARCHAR2
       ,p_commit_from_date  IN   VARCHAR2
       ,p_commit_to_date    IN   VARCHAR2
       ,p_purchase_agent	IN	 t301_vendor.c301_purchasing_agent_id%type
       ,p_out         		OUT  TYPES.cursor_type
	)
	AS	
	v_company_id    	T1900_COMPANY.C1900_COMPANY_ID%TYPE;
	v_dateformat VARCHAR2(100);
   	BEGIN
		
		SELECT get_compid_frm_cntx(),get_compdtfmt_frm_cntx()
		  INTO v_company_id,v_dateformat
		  FROM dual;       
		  
 		OPEN p_out
 		FOR
 			SELECT  t202.c202_project_nm projectnm, 
		 			t205.c205_part_number_id pnum,
		 			t205.c205_part_num_desc pdesc,
         			t401.c401_purchase_ord_id poid,
		 			t402.c402_work_order_id woid,
         			t301.c301_vendor_name vennm,
		 			to_char(t401.c401_purchase_ord_date,v_dateformat) podate,
		 			to_char(t402.c402_wo_due_date,v_dateformat) woduedt,
         			t402.c402_qty_ordered wo_qty,
         			to_char(t4027.c4027_wo_ack_date,v_dateformat) woackdt,
		 			t4027.c4027_qty ackqty,
         			trunc(t4027.c4027_wo_ack_date)-trunc(current_date) ackdueday,
         			to_char(t402.C402_WO_LEAD_DATE,v_dateformat) leadtimedate,
         			t402.C402_QTY_ORDERED - totqty.totackqty woackshortqty,
         			t101.c101_user_f_name || ' ' ||t101.c101_user_l_name purchaseagentname,
         			trunc(t402.C402_WO_DUE_DATE) - trunc(t4027.c4027_wo_ack_date) woackleadtimelate,
         			tdhr.qtyreceived qtyreceived,
         			get_wo_pend_qty(t402.c402_work_order_id,t402.c402_qty_ordered) wopendqty
    		   FROM T401_PURCHASE_ORDER t401, T402_WORK_ORDER t402, T4027_WORK_ORDER_ACK_DATE t4027,
		 			T205_PART_NUMBER t205, T202_PROJECT t202, T301_VENDOR t301, t101_user t101,
		 			(select sum(t408.C408_QTY_RECEIVED) qtyreceived,t408.c402_work_order_id twoid
		 			from t408_dhr t408 where c408_void_fl is null
					GROUP BY t408.c402_work_order_id, t408.c402_work_order_id) tdhr,
                     (select sum(t4027.c4027_qty) totackqty,t4027.c402_work_order_id twoid 
		 			from T4027_WORK_ORDER_ACK_DATE t4027  where c4027_void_fl is null 
					GROUP BY t4027.c402_work_order_id) totqty
   			  WHERE t401.c401_purchase_ord_id = t402.c401_purchase_ord_id
	 			AND t402.c402_work_order_id = t4027.c402_work_order_id
	 			AND t205.c205_part_number_id = t402.c205_part_number_id
	 			AND t202.c202_project_id = t205.c202_project_id
	 			AND t301.c301_vendor_id = t402.c301_vendor_id
				AND t301.c301_vendor_id = t401.c301_vendor_id
				AND t101.C101_USER_ID = t301.C301_PURCHASING_AGENT_ID
				AND t402.C402_WORK_ORDER_ID = tdhr.twoid(+)
                AND t4027.c402_work_order_id = totqty.twoid
	 			AND t402.c301_vendor_id = nvl(p_vendorid,t402.c301_vendor_id)
	 			AND t202.c202_project_id = nvl(p_projid,t202.c202_project_id)
	 			AND regexp_like (t205.c205_part_number_id, NVL (REGEXP_REPLACE(p_partnum,'[+]','+'), REGEXP_REPLACE(t205.c205_part_number_id,'[+]','+')))
	 			AND t4027.c4027_wo_ack_date BETWEEN  NVL(to_date(p_commit_from_date,v_dateformat),t4027.c4027_wo_ack_date)
	 			AND NVL(to_date(p_commit_to_date,v_dateformat),t4027.c4027_wo_ack_date)
	 			AND NVL(C301_PURCHASING_AGENT_ID,'-999') = DECODE (p_purchase_agent, 0, NVL(c301_purchasing_agent_id,'-999'), p_purchase_agent)
	 			AND c402_void_fl is null
	 			AND c4027_void_fl is null
	 			AND c401_void_fl is null
	 			AND c202_void_fl is null
	 			ORDER BY t202.c202_project_nm,t205.c205_part_number_id,t301.c301_vendor_name,t402.c402_work_order_id,t4027.c4027_wo_ack_date;

  
	END gm_fch_vendor_wo_ack_report;
/*********************************************************************
	Description : This function is used to Save WO Qty
	Author: 
*********************************************************************/ 	
 	
 	PROCEDURE gm_sav_wo_vendor_commit(
    p_vendor_commit_id   IN t402b_wo_vendor_commit.c402b_vendor_commit_id%TYPE,
    p_work_ord_id        IN t402b_wo_vendor_commit.c402_work_order_id%TYPE,
    p_vendor_commit_qty  IN t402b_wo_vendor_commit.c402b_commit_qty%TYPE,
    p_vendor_commit_date IN t402b_wo_vendor_commit.c402b_commit_date%TYPE,
    p_userid             IN t402b_wo_vendor_commit.c402b_last_updated_by%TYPE )
AS
  v_part_num t402b_wo_vendor_commit.c205_part_number_id%TYPE;
BEGIN
	
  UPDATE T402B_WO_VENDOR_COMMIT
  SET C402B_COMMIT_DATE        = p_vendor_commit_date,
    C402B_COMMIT_QTY           =p_vendor_commit_qty,
    c402b_void_fl = NULL,
    C402B_LAST_UPDATED_BY      =p_userid,
    C402B_LAST_UPDATED_DATE    =CURRENT_DATE
  WHERE C402B_VENDOR_COMMIT_ID = p_vendor_commit_id;
  IF (SQL%ROWCOUNT             = 0) THEN
    -- to get the WO part number
    SELECT C205_PART_NUMBER_ID
    INTO v_part_num
    FROM T402_WORK_ORDER
    WHERE C402_WORK_ORDER_ID = p_work_ord_id;
    
		INSERT INTO T402B_WO_VENDOR_COMMIT 
				(C402B_VENDOR_COMMIT_ID, C402_WORK_ORDER_ID, C205_PART_NUMBER_ID, C402B_COMMIT_DATE, C402B_COMMIT_QTY,C402B_VOID_FL,
				C402B_LAST_UPDATED_BY, C402B_LAST_UPDATED_DATE
				)
 		 VALUES(
    			s402B_VENDOR_COMMIT_ID.NEXTVAL ,p_work_ord_id ,v_part_num ,p_vendor_commit_date ,p_vendor_commit_qty ,NULL ,
    			p_userid, CURRENT_DATE
    			);
  END IF;
END gm_sav_wo_vendor_commit;

/*********************************************************************
	Description : This function is used to save the remain commit qty
	Author: mmuthusamy
	*********************************************************************/
     PROCEDURE gm_sav_remain_commit_qty (
      p_po_id             IN     T401_PURCHASE_ORDER.C401_PURCHASE_ORD_ID%TYPE,
      p_user_id            IN t402b_wo_vendor_commit.c402b_last_updated_by%TYPE
     )
	AS
	
	CURSOR vendor_commit_remain_wo_cur
    IS
        -- to get the Vendor commit WO id
       
	   SELECT wo_id, remain_commit_qty
	   FROM
	    (
	         SELECT t402.C402_WORK_ORDER_ID wo_id, t402.C402_QTY_ORDERED - SUM (t402b.C402B_COMMIT_QTY) remain_commit_qty
	           FROM T401_PURCHASE_ORDER t401, T402_WORK_ORDER t402, T402B_WO_VENDOR_COMMIT t402b
	          WHERE t401.C401_PURCHASE_ORD_ID = t402.C401_PURCHASE_ORD_ID
	            AND t402.C402_WORK_ORDER_ID   = t402b.C402_WORK_ORDER_ID
	            AND t401.C401_PURCHASE_ORD_ID = p_po_id
	            AND T402.C402_QTY_ORDERED     > 0
	            AND t402.C402_VOID_FL        IS NULL
	            AND t401.C401_VOID_FL        IS NULL
	            AND t402b.C402B_VOID_FL      IS NULL
	       GROUP BY t402.C402_WORK_ORDER_ID, t402.C402_QTY_ORDERED
	    )
	  WHERE remain_commit_qty > 0;
  
	BEGIN
		
    -- loop the different WO and Vendor Commit Qty and insert empty commit Date
    
    FOR remain_vendor_commit IN vendor_commit_remain_wo_cur
    LOOP
        --Call save wo vendor commit procedure 
        
    	gm_pkg_pur_wo_vendor_commit.gm_sav_wo_vendor_commit (NULL, remain_vendor_commit.wo_id, remain_vendor_commit.remain_commit_qty, NULL, p_user_id);
    
    END LOOP;
    
    --
  		
END gm_sav_remain_commit_qty;


END GM_PKG_PUR_WO_VENDOR_COMMIT;
/
