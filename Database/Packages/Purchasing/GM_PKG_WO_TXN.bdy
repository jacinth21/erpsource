CREATE OR REPLACE PACKAGE BODY GM_PKG_WO_TXN  
IS
    /***********************************************************
	Description : This is used to save time  stamp the WO attributes.
	Auther: Elango
	************************************************************/
     PROCEDURE gm_sav_wo_part_info (
		  p_woid       IN     T402_WORK_ORDER.C402_WORK_ORDER_ID%TYPE,
          p_partnum    IN     T402_WORK_ORDER.C205_PART_NUMBER_ID%TYPE,
	      p_userid     IN   T402A_WO_ATTRIBUTE.C402A_CREATED_BY%TYPE
	) 
	AS	
	   	v_string               VARCHAR2 (2000);
	    
	  CURSOR part_attrib_val
   		  IS
		 SELECT t205.c205_part_drawing partdraw, t205.c205_material_spec mspec, t205.c205_material_cert_fl mcfl
			  , t205.c205_hard_test_fl hfl, t205.c205_compliance_cert_fl ccl, t205d.udietch udietch
			  , t205d.dietch dietch, t205d.pietch pietch 
			  ,t205.c205_part_number_id partnum,t402.c402_rev_num revnum
			  ,t402.c402_work_order_id woid
        	  ,nvl(gm_pkg_pd_rpt_udi.get_part_di_number(t205.c205_part_number_id),'')di
        	  ,t205.c205_part_num_desc description,t402.c301_vendor_id vendid,get_vendor_name(t402.c301_vendor_id) vendname
              ,gm_pkg_pd_rpt_udi.get_udi_format_prefix(t205.c205_part_number_id)  udiprefix
        	  ,gm_pkg_pd_rpt_udi.get_pi_config_nm(t205.c205_part_number_id) piconfnm
		 FROM t205_part_number t205,T402_Work_Order t402, (
			         SELECT c205_part_number_id partnum,
			            MAX (DECODE (c901_attribute_type, 103381, C205D_ATTRIBUTE_VALUE)) UDIETCH,
			            MAX (DECODE (c901_attribute_type, 103382, C205D_ATTRIBUTE_VALUE)) DIETCH,
			            MAX (DECODE (c901_attribute_type, 103383, C205D_ATTRIBUTE_VALUE)) PIETCH
			           FROM t205d_part_attribute
			          WHERE c205d_void_fl       IS NULL
			            AND c901_attribute_type IN ('103381', '103382', '103383')   --103381 UDIetch, 103382 DIETCH, 103383 PIETCH
			       GROUP BY c205_part_number_id
			    )
			    t205d
		  WHERE T205.C205_Part_Number_Id = T205d.Partnum (+)
      	  AND T205.C205_Part_Number_Id = p_partnum
          AND T402.C402_Work_Order_Id = p_woid
		  AND T205.C205_Active_Fl      = 'Y'
		  AND t402.c402_void_fl IS NULL ;
	    
	BEGIN
		--Loop through the cursor and form the input string as below:
		--103402^124.000|103403^40148|103404^Y|103405^0|103406^Y|103407^103363|103408^103366|103409^103384|
	   FOR each_val IN part_attrib_val
	   LOOP	   
	       v_string :=     '103402^'||each_val.partdraw||'|103403^'||each_val.mspec  ||
	                      '|103404^'||each_val.mcfl    ||'|103405^'||each_val.hfl    ||
	                      '|103406^'||each_val.ccl     ||'|103407^'||each_val.udietch||
	                      '|103408^'||each_val.dietch  ||'|103409^'||each_val.pietch ||	
	                      '|4000678^'||each_val.partnum||
	                      '|4000680^'||each_val.revnum ||'|4000681^'||each_val.Description    ||
	                      '|4000682^'||each_val.vendname ||'|4000683^'||each_val.di    ||
	                      '|4000684^'||each_val.udiprefix ||'|4000685^'||each_val.piconfnm    ||'|';
	   END LOOP;
	   --Call the below prc to update all the Wo attributes.
       IF v_string IS NOT NULL
	   THEN
	   		gm_sav_wo_attribute(p_woid, v_string, p_userid);
	   END IF;
	   
	END gm_sav_wo_part_info; 
	
    /******************************************************************
	Description : This is used to save WO attributes in bulk at a time.
	Auther: Elango
	*******************************************************************/	   
	PROCEDURE gm_sav_wo_attribute (
	      p_woid       IN   T402_WORK_ORDER.C402_WORK_ORDER_ID%TYPE,
	      p_string     IN   VARCHAR2,
	      p_userid     IN   T402A_WO_ATTRIBUTE.C402A_CREATED_BY%TYPE
	)
	AS 
	    v_string	   VARCHAR2 (3000) := p_string;
		v_substring    VARCHAR2 (3000);
		v_type		   VARCHAR2 (100);
		v_value 	   VARCHAR2 (500);
	   	
	BEGIN
		---v_string 103402^124.000|103403^40148|103404^Y|103405^0|103406^Y|103407^103363|103408^103366|103409^103384|
		WHILE INSTR (v_string, '|') <> 0
		LOOP
			v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
			v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
			v_type		:= NULL;
			v_value 	:= NULL;
			v_type		:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
			v_value 	:= v_substring;
			
			IF v_value IS NOT NULL
			THEN
				gm_sav_wo_attribute(p_woid, v_type, v_value, p_userid);
			END IF;
			
		END LOOP;
	END gm_sav_wo_attribute;
	
    /***************************************************************
	Description : This is used to save WO attributes once at a time
	Auther: Elango
	****************************************************************/	 	
	PROCEDURE gm_sav_wo_attribute (
	      p_woid       IN   T402_WORK_ORDER.c402_work_order_id%TYPE,
	      p_attrtype   IN   T402A_WO_ATTRIBUTE.C901_ATTRIBUTE_TYPE%TYPE,
	      p_attrval    IN   T402A_WO_ATTRIBUTE.C402A_ATTRIBUTE_VALUE%TYPE,
	      p_userid     IN   T402A_WO_ATTRIBUTE.C402A_CREATED_BY%TYPE
	) 
AS
   BEGIN	   
	   	   	   
      UPDATE T402A_WO_ATTRIBUTE
         SET C402A_ATTRIBUTE_VALUE = p_attrval,
             C402A_LAST_UPDATED_BY = p_userid,
             C402A_LAST_UPDATED_DATE = CURRENT_DATE
       WHERE C402_WORK_ORDER_ID  = p_woid 
         AND C901_ATTRIBUTE_TYPE = p_attrtype                 
         AND C402A_VOID_FL IS NULL;

      IF (SQL%ROWCOUNT = 0)
      THEN		        
		     
         INSERT   INTO T402A_WO_ATTRIBUTE   (
        		C402A_WO_ATTRIBUTE_ID, C402_WORK_ORDER_ID, 
        		C901_ATTRIBUTE_TYPE, C402A_ATTRIBUTE_VALUE, 
        		C402A_CREATED_BY, C402A_CREATED_DATE
    	  ) VALUES  (
        		S402A_WO_ATTRIBUTE.nextval, p_woid,
        		p_attrtype, p_attrval, 
        		p_userid, CURRENT_DATE
          ) ;
      END IF;
      
   END gm_sav_wo_attribute;
   
   /*********************************************************************
	Description : This is used to Fetch the WO details based on the PO id.
	Auther: Bala
	*********************************************************************/	   
	PROCEDURE gm_get_wo_list (
	     p_po_id       IN   T402_WORK_ORDER.C401_PURCHASE_ORD_ID%TYPE,
	     p_recordset   OUT  TYPES.cursor_type
	)AS
	v_po_id VARCHAR2(50);
	v_dateformat varchar2(100);
	v_company_id  t1900_company.c1900_company_id%TYPE;
	BEGIN
		SELECT	get_compid_frm_cntx(), get_compdtfmt_frm_cntx()
	INTO	v_company_id, v_dateformat
	FROM	dual;
	  
	   GM_PKG_DHR_PRIORITY.GM_INSERT_PART_TEMP(p_po_id,NULL);
	
		OPEN p_recordset FOR
		 SELECT C402_WORK_ORDER_ID WID
		      , t205.C205_PART_NUMBER_ID PNUM
		      , DECODE (GM_PKG_PD_RPT_UDI.GET_PART_DI_NUMBER(t205.C205_PART_NUMBER_ID), NULL, '', GM_PKG_PD_RPT_UDI.get_udi_format_prefix (t205.C205_PART_NUMBER_ID) || GM_PKG_PD_RPT_UDI.GET_PART_DI_NUMBER(t205.C205_PART_NUMBER_ID)) UDINO
		      , gm_pkg_op_dhr.get_di_no_from_vendor(t205.C205_PART_NUMBER_ID)SHOW_TXTBOX		     
		      , c402_split_cost split_cost
		      , get_part_attribute_value (t205.C205_PART_NUMBER_ID, 80134) CNUMFMT
		      , get_part_attribute_value (t205.C205_PART_NUMBER_ID, 80135) DUPCNUM
		      , NVL (get_rule_value (t205.C205_PART_NUMBER_ID, 'SKIPCONTROLNUMVAL'), DECODE (get_part_attribute_value(t205.C205_PART_NUMBER_ID, 80132), '80130', 'Y')) SKIPFL
		      , t205.C205_PRODUCT_MATERIAL MATERIAL
		      , t205.C205_PART_NUM_DESC PDESC
		      , GET_WO_WIP_QTY (C402_WORK_ORDER_ID) WIP
		      , C402_QTY_ORDERED QTY, C402_COST_PRICE RATE
		      , C402_CREATED_BY CUSER
		      , GET_WO_PEND_QTY (C402_WORK_ORDER_ID, C402_QTY_ORDERED) PEND
		      , C402_CRITICAL_FL CFL
		      , GET_DOC_FOOTER (TO_CHAR(C402_CREATED_DATE,v_dateformat), 'GM-G001') FOOTER
		      , C402_FAR_FL FARFL
		      , GET_WO_WIP_QTY (C402_WORK_ORDER_ID) RECQTY
		      , C402_STATUS_FL STATUSFL
		      , GET_WO_UOM_TYPE_QTY (C402_WORK_ORDER_ID) UOMANDQTY
		      , get_log_flag (t402.C402_WORK_ORDER_ID, 1232) WLOG
		      , C402_REV_NUM WOREV
		      , t205.C205_REV_NUM PARTREV
		      , TO_CHAR(t402.C402_WO_DUE_DATE,v_dateformat) WODUEDATE
		      , C402_QTY_HISTORY_FL QTYHISTFL
		      , C402_PRICE_HISTORY_FL PRICEHISTFL
		      , c205_product_class PRODCLASS
		      , DECODE (c402_validation_fl, 'Y', GET_RULE_VALUE ('WOCONTENT', 'PPPQ'), NULL) VALIDATIONFL
		      , DECODE (C402_FAR_FL, 'Y', 1, DECODE (c402_validation_fl, 'Y', 2, 3)) FLAGORD
		      , get_partnum_material_type(t205.C205_PART_NUMBER_ID) prodtype
		      , gm_pkg_pd_rpt_udi.get_part_udi_format (t205.C205_PART_NUMBER_ID) UDIFORMAT
		      , gm_pkg_pd_rpt_udi.get_part_di_number(t205.C205_PART_NUMBER_ID) DINUM
		      , chk_dhr_exist(C402_WORK_ORDER_ID) DHREXISTFL
		      , GET_PART_ATTR_VALUE_BY_COMP(t205.C205_PART_NUMBER_ID,106040,v_company_id) SERIALNUMNEEDFL-- Serial Number needed flag
              , get_code_name(gm_pkg_dhr_priority.gm_dhr_priority_calc_main(t205.C205_PART_NUMBER_ID)) DHR_PRIORITY

		   FROM T402_WORK_ORDER t402, T205_Part_number t205
		  WHERE t402.C401_PURCHASE_ORD_ID LIKE '%' || p_po_id
		    AND t402.C205_PART_NUMBER_ID  = t205.C205_PART_NUMBER_ID
		    AND C402_VOID_FL          IS NULL
		    AND C402_QTY_ORDERED       > 0
		    AND t402.c1900_company_id = v_company_id
		ORDER BY t402.C402_WO_DUE_DATE,t205.C205_PART_NUMBER_ID, FLAGORD, C402_WORK_ORDER_ID;		
	END gm_get_wo_list;   	
	 /*********************************************************************
	Description : This function is used to Return Y when the part contain open DHR. 
	Auther: Manoj.
	*********************************************************************/	   
	FUNCTION chk_dhr_exist (
	p_wo  VARCHAR2
    )
	RETURN VARCHAR2
	IS
	v_cnt		   NUMBER;
	v_dhr_fl       VARCHAR2(5);
	BEGIN

	     SELECT COUNT(1) INTO v_cnt
		   FROM t408_dhr a,t402_work_order b 
		  WHERE a.c402_work_order_id = b.c402_work_order_id (+)
		    AND b.c402_work_order_id = p_wo
            AND a.c408_void_fl IS NULL;
		
		IF v_cnt >0 THEN
			v_dhr_fl := 'Y';
		ELSE
			v_dhr_fl := 'N';
		END IF;
		RETURN 	v_dhr_fl;
	END chk_dhr_exist;
	
	/*********************************************************************
	Description : This function is used to getting Work order details ,part number to reload part number details
	Auther: 
	*********************************************************************/	
    PROCEDURE gm_upd_wo_part_info (
          p_string     IN   CLOB,
	      p_userid     IN   T402A_WO_ATTRIBUTE.C402A_CREATED_BY%TYPE
	)
	AS 
	   v_string	   	  CLOB := p_string;
	   v_substring    VARCHAR2 (3000);
	   v_wo           VARCHAR2 (20) ;
	   v_partnum      VARCHAR2 (20) ;
	   	
	BEGIN
		
		WHILE INSTR (v_string, '|') <> 0
		LOOP
		v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
		v_string     := SUBSTR (v_string, INSTR (v_string, '|') + 1);
		v_wo         := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
	 	v_substring  := SUBSTR (v_substring, INSTR (v_substring, '^') + 1) ;
       	v_partnum    := v_substring ;
      	
      	GM_PKG_WO_TXN.gm_sav_wo_part_info(v_wo,v_partnum,p_userid); 
      	GM_PKG_WO_TXN.gm_update_sub_WO(v_wo,v_partnum);
		
		END LOOP;
	END gm_upd_wo_part_info;
	
	/*********************************************************************
	Description : This function is used to void work order the WO attributes.
	Auther: 
	*********************************************************************/
    PROCEDURE gm_void_wo (
    	 p_txn_str  IN CLOB,
         p_user_id  IN t205d_part_attribute.c205d_last_updated_by%TYPE
	) 
	AS
	 v_string	   	  CLOB := p_txn_str;
	 v_wo_id 		  T402A_WO_ATTRIBUTE.C402_WORK_ORDER_ID%TYPE;
	BEGIN
		 WHILE INSTR (v_string, ',') <> 0
			  LOOP
			  v_wo_id := SUBSTR (v_string, 1, INSTR (v_string, ',') - 1);
			  v_string     := SUBSTR (v_string, INSTR (v_string, ',')    + 1);
			  
		 UPDATE t402_work_order
          SET c402_void_fl           = 'Y' ,
            c402_last_updated_date = CURRENT_DATE,
            c402_last_updated_by   = p_user_id
          WHERE c402_work_order_id = v_wo_id; 
		END LOOP;
	END gm_void_wo;
	
	PROCEDURE gm_update_sub_WO (
             p_woid       IN     T402_WORK_ORDER.C402_WORK_ORDER_ID%TYPE,
          p_partnum    IN     T402_WORK_ORDER.C205_PART_NUMBER_ID%TYPE
       )
       AS 
       
       CURSOR pop_val
       IS
      SELECT t205.c205_part_number_id subid, t205.c205_rev_num rev
        FROM t205a_part_mapping t205a, t205_part_number t205
       WHERE t205a.c205_from_part_number_id = p_partnum
         AND t205a.c205_to_part_number_id = t205.c205_part_number_id
         AND t205a.c205a_void_fl is null;  ---  added new column c205a_void_fl in t205a used to PMT-50777 - Create BOM and sub-component Mapping
       
       BEGIN
              
              DELETE FROM T403_SUB_WORK_ORDER
                      WHERE C402_WORK_ORDER_ID = p_woid;
              
              
              FOR rad_val IN pop_val
   LOOP
      --
      INSERT INTO t403_sub_work_order
                  (c403_sub_work_order_id, c402_work_order_id,
                   c206_sub_asmbly_id, c403_rev_num
                  )
           VALUES (s403_sub_work.NEXTVAL, p_woid,
                   rad_val.subid, rad_val.rev
                  );
   --
   END LOOP;              
END gm_update_sub_WO;
       
/*
 * PMT-32340 Work Order Revision Update Dashboard
 * When Doc Control updates the Drawing Revision for a Part and that Part is on open Work Orders, 
 * the Work Order should be displayed in a Dashboard with the following columns with the ability to update the work order revision or 
 * to ignore.
   There should also be a report logging all the actions performed from the Work Order Revision Dashboard
 */

/*********************************************************************
	Description : This procedure used to load the WO revision details 
				  and show the data to - Dashboard and report screen.
	      Author: Prabhu vigneshwaran M D 
*********************************************************************/
-- WO Dashboard screen - to pass status (Open)
-- WO Revision report screen - to pass status (In Progress, Failed, Ignored)

  PROCEDURE gm_fch_wo_revision_dtls(
	p_project_id        IN      T202_PROJECT.C202_PROJECT_ID%TYPE,
	p_part_number_id    IN      T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE,
	p_po_id             IN      T402_WORK_ORDER.C401_PURCHASE_ORD_ID%TYPE,
	p_action            IN      T402C_WORK_ORDER_REVISION.c901_action%TYPE,
	p_status            IN      VARCHAR2,
	p_from_date         IN      VARCHAR2,
	p_to_date           IN      VARCHAR2,
	P_OUT_WO_REV_DTLS   OUT    CLOB
    )
    AS
    v_date_fmt VARCHAR2(20);
    v_from_date DATE;
    v_to_date DATE;
     v_company_id T1900_company.c1900_company_id%TYPE;
    BEGIN  	     
	      -- to get the date format from context
         SELECT get_compdtfmt_frm_cntx (),get_compid_frm_cntx()
         INTO v_date_fmt,v_company_id
         FROM dual;   
         
         SELECT to_date(p_from_date , v_date_fmt),to_date(p_to_date , v_date_fmt)
         INTO v_from_date,v_to_date
         FROM dual;       
         
     --set the status in context and get the values 
	    my_context.set_my_inlist_ctx (p_status);	
	    
	    -- PC-1753: Add vendor and Agent name
	    
  		SELECT RTRIM(regexp_replace(xmlagg (xmlelement (e, P_OUT_WO_REV_DTLS || ',')) .extract('//text()').getClobVal(),'\&' ||'quot;', '"'), ',')
  		 INTO P_OUT_WO_REV_DTLS
   	     FROM
         (SELECT JSON_OBJECT 
          ('WOREVID'       VALUE  t402c.c402c_work_order_revision_id,
          'PART_NUM'       VALUE  t205.c205_part_number_id ,
    	  'PART_DESC'      VALUE  t205.c205_part_num_desc,
          'PO_ID'          VALUE  t402.c401_purchase_ord_id,
          'WO_ID'          VALUE  t402.c402_work_order_id,
          'REV_NUM'        VALUE  t402.c402_rev_num ,
          'PROJECTNAME'    VALUE  t202.c202_project_nm,
          'NEW_REV'        VALUE  t402c.C402C_WO_NEW_REV,
          'REVUPDDT'       VALUE  TO_CHAR(t402c.C402C_REVISION_DATE,v_date_fmt),
          'REVUPDBY'       VALUE  get_user_name(t402c.C402C_REVISION_BY),
          'WOQTY'          VALUE  t402.C402_QTY_ORDERED,
          'DHRQTY'         VALUE  get_wo_wip_qty (t402.c402_work_order_id),
          'ACTION'         VALUE  get_code_name(t402c.c901_action),
          'REASON'         VALUE  get_code_name(t402c.c901_reason) ,
          'COMMENTS'       VALUE  t402c.c402c_comments,
          'PERFORMED_BY'   VALUE  get_user_name(t402c.c402c_approved_by),
          'performed_date' VALUE  TO_CHAR(t402c.c402c_approved_date,v_date_fmt),
          'STATUS'         VALUE  get_code_name(t402c.C901_STATUS),
          'VENDOR'         VALUE  t301.c301_vendor_name,
          'AGENT'         VALUE  GET_USER_NAME(c301_purchasing_agent_id)
       )P_OUT_WO_REV_DTLS
       FROM 
        t205_part_number t205,
        t402_work_order t402
       ,T402C_WORK_ORDER_REVISION t402c
       ,t202_project t202
       , t301_vendor t301
       , t401_purchase_order t401
       WHERE T401.c401_purchase_ord_id = t402.c401_purchase_ord_id
       AND T301.c301_vendor_id = t401.c301_vendor_id
       AND T401.c401_void_Fl is null
       AND  t402.c205_part_number_id  = t205.c205_part_number_id
       AND T402.c402_work_order_id = t402c.c402_work_order_id
       AND t202.C202_PROJECT_ID = t205.C202_PROJECT_ID
       AND t402.c401_purchase_ord_id = DECODE (p_po_id,null,t402.c401_purchase_ord_id, p_po_id)
       AND REGEXP_LIKE(t402.c205_part_number_id , CASE WHEN REGEXP_REPLACE(p_part_number_id,'[+]','\+') IS NOT NULL THEN REGEXP_REPLACE(p_part_number_id,'[+]','\+') ELSE t402.c205_part_number_id END)
       AND t205.c202_project_id = DECODE (p_project_id,null,t205.c202_project_id, p_project_id)
       AND NVL(t402c.c901_action,'-9999') = DECODE (p_action,null,NVL(t402c.c901_action,'-9999'), p_action)
       AND t402c.C901_STATUS IN (SELECT token FROM v_in_list)
       AND DECODE(v_from_date, NULL,t402c.c402c_revision_date, t402c.c402c_approved_date) >= DECODE(v_from_date, NULL,t402c.c402c_revision_date, v_from_date)
       AND DECODE(v_to_date, NULL, t402c.c402c_revision_date, t402c.c402c_approved_date) <= DECODE(v_to_date, NULL, t402c.c402c_revision_date, v_to_date)
       AND t402.c1900_company_id = v_company_id
	   AND t402.c402_void_fl is null	
	   AND t402c.C402C_VOID_FL is null
	   -- Dashboard order by - Part number
	   -- Report screen order by -  c402c_approved_date, part number
	   ORDER BY DECODE(p_status,'108242','',t402c.c402c_approved_date),t205.c205_part_number_id, t402.c402_work_order_id  
	  );

END gm_fch_wo_revision_dtls;



/*********************************************************************
	Description : This procedure used to update the WO Revision status 
				  when status is open.
	      Author: Tamizhthangam
*********************************************************************/
 
 PROCEDURE gm_upd_bulk_wo_revision(
     p_input_str_wo_id   IN    CLOB,
     p_user_id           IN    T402_WORK_ORDER.C402_LAST_UPDATED_BY%TYPE,
     p_out_error_wo_ids  OUT   CLOB
    )
    AS
      v_work_id       VARCHAR2(4000);
      v_wo_cnt        NUMBER;
       CURSOR wo_id_cur
		IS
		SELECT c205_part_number_id work_orderId  FROM my_t_part_list;
   BEGIN
	-- set the clobe value to context
		my_context.set_my_cloblist(p_input_str_wo_id);	
		
		  FOR wo_id IN wo_id_cur
          LOOP 
       
			--Get Count from T402_WORK_ORDER  
            SELECT COUNT (1)
             INTO v_wo_cnt
             FROM T402_WORK_ORDER T402
             WHERE c402_work_order_id = wo_id.work_orderId
             AND  c402_status_fl <> 3
             AND c402_void_fl IS NULL;
    
           --If v_count not equal to zero then call gm_upd_wo_revision_dtls 
           IF v_wo_cnt <> 0
	      THEN                     
		    gm_sav_wo_revision (wo_id.work_orderId, NULL,NULL,NULL,108243,108240,NULL,NULL,p_user_id);
		  END IF;
		  --If v_count equal to zero then set workorderid to p_out_error_wo_ids
		  IF v_wo_cnt = 0 THEN
		    p_out_error_wo_ids := wo_id.work_orderId;
		  END IF;
	      END LOOP;         
 END gm_upd_bulk_wo_revision;
 

/*********************************************************************
	Description : This procedure used to Loop the WO and update the IGNORE 
				  status.
	      Author: Tamizhthangam
*********************************************************************/
PROCEDURE gm_ignore_wo_revision(
    p_input_str_wo_id  IN CLOB,
    p_reason           IN t907_cancel_log.c901_cancel_cd%TYPE,
    p_cancelType       IN t907_cancel_log.c907_cancel_log_id%TYPE,
    p_comments         IN t907_cancel_log.c907_comments%TYPE,
    p_user_id          IN t102_user_login.c102_user_login_id%TYPE
    )
   AS
      v_work_id       VARCHAR2(4000);
      v_wo_cnt        NUMBER;
      
      CURSOR wo_id_cur
		IS
		SELECT c205_part_number_id work_orderId  FROM my_t_part_list;
		
        BEGIN
    	-- set the clobe value to context
		my_context.set_my_cloblist(p_input_str_wo_id);	
		  FOR wo_id IN wo_id_cur
          LOOP   
           -- Based on WO ids to update the Status (Close), Action (ignore), reason, Comments
		  gm_sav_wo_revision (wo_id.work_orderId, NULL,NULL,NULL,108245,108241,p_reason,p_comments,p_user_id);
          END LOOP;  
END gm_ignore_wo_revision;
    
 /*********************************************************************
	      Description : This Procedure is used to get open wo details
	      Author: Prabhu vigneshwaran M D 
*********************************************************************/
	PROCEDURE gm_fch_open_wo_details(
		p_part_number_id IN T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE,
		p_product_class  IN T205_PART_NUMBER.C205_PRODUCT_CLASS%TYPE,
		p_mater_spec     IN T205_PART_NUMBER.C205_MATERIAL_SPEC%TYPE,
		p_part_drawing   IN T205_PART_NUMBER.C205_PART_DRAWING%TYPE,
		p_rev_num        IN T205_PART_NUMBER.C205_REV_NUM%TYPE,
	    p_out_wo_dtls    OUT  TYPES.cursor_type
    )
    AS
    v_part_upd_cnt NUMBER;
	v_curr_rev	   t205_part_number.c205_rev_num%TYPE;
	v_class 	   t205_part_number.c205_product_class%TYPE;
	v_spec		   t205_part_number.c205_material_spec%TYPE;
	v_drawnum	   t205_part_number.c205_part_drawing%TYPE;
	 v_company_id T1900_company.c1900_company_id%TYPE;
    BEGIN   
	     SELECT get_compid_frm_cntx()
       INTO v_company_id
       FROM dual;
	    	    
	--	Based on part # to check not equal to below values (Prod class, Material specification, Revision number, Drawing #) 
	--- return the count

	    	SELECT COUNT(1)
	        INTO v_part_upd_cnt
	        FROM T205_PART_NUMBER T205
	        WHERE T205.C205_PART_NUMBER_ID = P_PART_NUMBER_ID
	        AND (T205.C205_PRODUCT_CLASS <> P_PRODUCT_CLASS
	        OR T205.C205_MATERIAL_SPEC <> P_MATER_SPEC
	        OR  T205.C205_PART_DRAWING <> P_PART_DRAWING
	        OR  T205.C205_REV_NUM <> P_REV_NUM );
	       
	   IF v_part_upd_cnt <> 0 THEN
 			OPEN p_out_wo_dtls
 			 FOR
 				 SELECT t402.c402_work_order_id woid, t301.c301_vendor_name vname
 					  , t402.c401_purchase_ord_id poid, t402.c402_created_by cuser
 					  , DECODE (t205.c205_rev_num, p_rev_num, 'NC', t205.c205_rev_num) rev
 					  , DECODE (t402.c402_created_by, '', '', get_user_emailid (t402.c402_created_by)) email					
 					  , DECODE (t205.c205_product_class, p_product_class, 'NC', get_code_name (t205.c205_product_class)) pclass
 					  , DECODE (t205.c205_material_spec, p_mater_spec, 'NC', get_code_name (t205.c205_material_spec)) mspec
 					  , DECODE (t205.c205_part_drawing, p_part_drawing, 'NC', t205.c205_part_drawing) draw
 					  , t205.c205_part_number_id pnum
 				   FROM t205_part_number t205, t402_work_order t402, t301_vendor t301
 				  WHERE t205.c205_part_number_id = t402.c205_part_number_id
 				        AND t301.c301_vendor_id      = t402.c301_vendor_id
 						AND EXISTS (SELECT DISTINCT t402a.c402_work_order_id woid
				   								FROM t402_work_order t402a, t205a_part_mapping t205a
				  								WHERE (t205a.c205_to_part_number_id = p_part_number_id OR t402a.c205_part_number_id = p_part_number_id)
												  AND t402a.c205_part_number_id = t205a.c205_from_part_number_id(+)
												  AND t402.c402_work_order_id = t402a.c402_work_order_id
												  AND t402a.c402_status_fl < 3
												  AND t402a.c1900_company_id = v_company_id
												  AND t402a.c402_void_fl IS NULL  
												  AND t205a.c205a_void_fl is null  ---  added new column c205a_void_fl in t205a used to PMT-50777 - Create BOM and sub-component Mapping
												  )
						 AND t402.c402_status_fl < 3
						 AND t402.c1900_company_id = v_company_id
					     AND t402.c402_void_fl IS NULL
					     
 				 UNION
 				 SELECT t402.c402_work_order_id woid, t301.c301_vendor_name vname
 					  , t402.c401_purchase_ord_id poid, t402.c402_created_by cuser
 					  , DECODE (t205.c205_rev_num, p_rev_num, 'NC', t205.c205_rev_num) rev
 					  , DECODE (t402.c402_created_by, '', '', get_user_emailid (t402.c402_created_by)) email					
 					  , DECODE (t205.c205_product_class, p_product_class, 'NC', get_code_name (t205.c205_product_class)) pclass
 					  , DECODE (t205.c205_material_spec, p_mater_spec, 'NC', get_code_name (t205.c205_material_spec)) mspec
 					  , DECODE (t205.c205_part_drawing, p_part_drawing, 'NC', t205.c205_part_drawing) draw
 					  , t205.c205_part_number_id pnum
 				   FROM t205_part_number t205, t403_sub_work_order t403, t402_work_order t402, t301_vendor t301
 				  WHERE t205.c205_part_number_id = t403.c206_sub_asmbly_id(+)
 					AND t205.c205_part_number_id = t402.c205_part_number_id
 					AND t301.c301_vendor_id      = t402.c301_vendor_id
 					AND EXISTS(SELECT DISTINCT  t402a.c402_work_order_id woid
				   							FROM t402_work_order t402a, t205a_part_mapping t205a
				  							WHERE (t205a.c205_to_part_number_id = p_part_number_id OR t402a.c205_part_number_id = p_part_number_id)
											  AND t402a.c205_part_number_id = t205a.c205_from_part_number_id(+)
											  AND t402.c402_work_order_id = t402a.c402_work_order_id
					                          AND t402a.c402_status_fl < 3
					                          AND t402a.c1900_company_id = v_company_id
					                          AND t402a.c402_void_fl IS NULL
					                          AND t205a.c205a_void_fl is null   ---  added new column c205a_void_fl in t205a used to PMT-50777 - Create BOM and sub-component Mapping
					                          )
					 AND t402.c402_status_fl < 3
					 AND t402.c1900_company_id = v_company_id
					 AND t402.c402_void_fl IS NULL;  			
			--if part count as 0 then, cursor exist error will show - to avoid added the below statement.
		ELSE
		OPEN p_out_wo_dtls
 			 FOR
 				select * from dual;
      END IF;
     
 END gm_fch_open_wo_details;   

/*********************************************************************
	      Description : This procedure used to save the WO revision 
	      				to new table
	      		Author: prabhu vigneshwaran M D
*********************************************************************/
		PROCEDURE gm_sav_wo_revision_bulk_update(
		p_part_number_id IN T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE,
		p_user_id 		 IN t102_user_login.c102_user_login_id%TYPE
  )
  AS
  --based on part # to get all the open WO (T402, T403) - not equal part number revision <> WO revision 
	CURSOR work_order
	IS
	SELECT  T402.C402_WORK_ORDER_ID v_wo_id,
	 				T402.C205_PART_NUMBER_ID v_pnum,
	 				T402.C402_REV_NUM v_old_revision,
	 				T205.C205_REV_NUM v_new_revision 
	 	    FROM T402_WORK_ORDER T402,
         		 T205_PART_NUMBER T205
          WHERE T205.C205_REV_NUM <> T402.C402_REV_NUM
          AND T205.C205_PART_NUMBER_ID = T402.C205_PART_NUMBER_ID
          AND T402.C205_PART_NUMBER_ID = p_part_number_id
          AND T402.C402_STATUS_FL <> 3
          AND T402.C402_VOID_FL IS NULL;
	
      BEGIN
	      --loop the cursor.call the new procedure to save WO revision - new table. (gm_sav_wo_revision)
             FOR rad_val IN work_order
		          LOOP
               gm_sav_wo_revision (rad_val.v_wo_id, rad_val.v_pnum, rad_val.v_old_revision,rad_val.v_new_revision,'108242',null,null,null,p_user_id);
		      END LOOP;
    END gm_sav_wo_revision_bulk_update;     

     /*********************************************************
    * Description : This procedure used to save the WO revision to new table
    * Author:       prabhu vigneshwaran M D
    *********************************************************/		 	 
PROCEDURE gm_sav_wo_revision(
	 p_wo_id          IN   T402_WORK_ORDER.C402_WORK_ORDER_ID%TYPE,
     part_num         IN   T402_WORK_ORDER.c205_part_number_id%TYPE,
     p_old_revision   IN   T402_WORK_ORDER.c402_rev_num%TYPE,
     p_new_revision	  IN   t205_part_number.c205_rev_num%TYPE,
     p_status         IN   T402C_WORK_ORDER_REVISION.C901_STATUS%TYPE,
     p_action         IN   T402C_WORK_ORDER_REVISION.C901_ACTION%TYPE,
     p_reason         IN   T402C_WORK_ORDER_REVISION.C901_REASON%TYPE,
     p_comments       IN   T402C_WORK_ORDER_REVISION.C402C_COMMENTS%TYPE,
     p_user_id        IN   t102_user_login.c102_user_login_id%TYPE
    )
		AS
		BEGIN	       
			--based on WO Id and part number to update revision number - new table
			 UPDATE T402C_WORK_ORDER_REVISION 
                  SET C901_STATUS = p_status,
                      C901_ACTION = p_action,
                      C901_REASON = p_reason, 
                      C402C_COMMENTS = p_comments,
                      C402C_WO_CURR_REV = NVL(p_old_revision ,C402C_WO_CURR_REV),
                      C402C_WO_NEW_REV = NVL(p_new_revision,C402C_WO_NEW_REV),
                      C402C_VOID_FL = NULL,
                      C402C_LAST_UPDATED_BY = p_user_id, 
                      C402C_LAST_UPDATED_DATE = CURRENT_DATE ,
                      C402C_APPROVED_BY = DECODE(p_action, NULL, NULL, p_user_id), 
                      C402C_APPROVED_DATE = DECODE(p_action, NULL,NULL, TRUNC(CURRENT_DATE)) 
                    WHERE C402_WORK_ORDER_ID = p_wo_id;
            
  			--No record update then, insert the new records - new table (default value - Status (Open)).	
			IF (SQL%ROWCOUNT = 0) THEN
			-- 1. Create a new entry onto T402C_WORK_ORDER_REVISION
			INSERT INTO T402C_WORK_ORDER_REVISION
						(
						C402C_WORK_ORDER_REVISION_ID, 
						C402_WORK_ORDER_ID, 
						C205_PART_NUMBER_ID, 
						C402C_WO_CURR_REV, 
						C402C_WO_NEW_REV,
						C901_STATUS,
						C901_ACTION,
						C402C_REVISION_BY,
						C402C_REVISION_DATE,
						C402C_APPROVED_BY,
						C402C_APPROVED_DATE
						)
				 VALUES (
				 		S402C_WORK_ORDER_REVISION.NEXTVAL, 
				 		p_wo_id, 
				 		part_num, 
				 		p_old_revision, 
				 		p_new_revision,
					    p_status,
					    p_action,
					    p_user_id, 
					    CURRENT_DATE,
					    -- 26240599 Updated from WO edit
					    DECODE(p_action, 26240599, p_user_id, NULL),
					    DECODE(p_action, 26240599, TRUNC(CURRENT_DATE), NULL)
						);	
				END IF;					
END gm_sav_wo_revision;

/*********************************************************************
	      Description : This procedure used to Loop the WO and update 
	      				the CLOSE status
	      Author: prabhu vigneshwaran M D
*********************************************************************/	

PROCEDURE gm_wo_revision_update(  
		p_status  IN T402C_WORK_ORDER_REVISION.C901_STATUS%TYPE,
		p_user_id IN t102_user_login.c102_user_login_id%TYPE
  )
  AS
      v_clob_string  VARCHAR2(4000);
      v_wo_string VARCHAR2(4000);
      v_cnt NUMBER;
      v_out_msg VARCHAR2(4000);
      --
      CURSOR work_order_id
      IS
                SELECT t402c.c402_work_order_id v_wo , 
                     t402c.C402C_WO_NEW_REV v_rev,
                     t402.c402_qty_ordered v_pendqty, 
                    t402.c402_cost_price v_cost,
                     t402.c402_critical_fl v_criticalfl,
                     t402.c402_far_fl v_farfl,
                     t402.c402_validation_fl v_validfl,
                     'N' v_fl,
                     '0' v_action
              FROM T402C_WORK_ORDER_REVISION t402c , T402_WORK_ORDER t402
              WHERE t402c.c402_work_order_id = t402.c402_work_order_id
              and t402c.c901_status  = p_status
                AND t402c.c402c_void_fl IS NULL
                and t402.c402_void_fl is NULL;
      BEGIN  
	    
            FOR workorderdtls IN work_order_id
            LOOP
                  v_clob_string := workorderdtls.v_wo||'^'||
                  				   workorderdtls.v_pendqty||'^'||
                  				   workorderdtls.v_cost||'^'||
                  				   workorderdtls.v_fl||'^'||
                  				   workorderdtls.v_criticalfl||'^'||
                  				   workorderdtls.v_farfl||'^'||
                  				   workorderdtls.v_rev||'^'||
                  				   workorderdtls.v_validfl||'^'||
                  				   workorderdtls.v_action||'|';				   
                  
                  	  BEGIN
                  		gm_update_wo_oper(v_clob_string,p_user_id,v_out_msg);
                	   EXCEPTION WHEN OTHERS
                	   THEN
                		-- to update the status as "Failed"
                          gm_sav_wo_revision(workorderdtls.v_wo, NULL,NULL,NULL,108244,108240,NULL,NULL,p_user_id);
                       END;
             END LOOP;
      
END gm_wo_revision_update;


 /*********************************************************************
	      Description : This procedure used to void t402c based on partnumber
	      				and userID
	      Author: prabhu vigneshwaran M D
*********************************************************************/
  PROCEDURE gm_void_wo_revision(
		p_part_number_id IN T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE,
		p_user_id        IN t102_user_login.c102_user_login_id%TYPE
  )
  
  AS
  
  BEGIN
	 
	  	UPDATE T402C_WORK_ORDER_REVISION 
	       SET C402C_VOID_FL = 'Y',
	           C402C_LAST_UPDATED_BY = p_user_id, 
	           C402C_LAST_UPDATED_DATE = CURRENT_DATE
           WHERE c205_part_number_id = p_part_number_id 
             AND C901_STATUS ='108242' -- Open
             AND C402C_VOID_FL IS NULL;
END gm_void_wo_revision;  

END gm_pkg_wo_txn;
/
