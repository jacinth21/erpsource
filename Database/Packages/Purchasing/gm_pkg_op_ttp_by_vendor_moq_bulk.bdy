--@"Database\DMO\Packages\TTPVendor\gm_pkg_op_ttp_by_vendor_moq_bulk.bdy";
CREATE OR REPLACE PACKAGE BODY gm_pkg_op_ttp_by_vendor_moq_bulk IS
/********************************************************************
* Description :This Procedure used to Loop the pnum,qty,flag for Each vendor
			   and store into t3010_vendor_moq table
* Author	  : Prabhuvigneshwaran			
*************************************************************************/

    PROCEDURE gm_process_ttp_by_vendor_moq_bulk (
        p_vendor_id      IN    t3010_vendor_moq.c301_vendor_id%TYPE,
        p_partinputstr   IN    CLOB,
        p_inputstr       IN    CLOB,
        p_user_id        IN    t3010_vendor_moq.c3010_updated_by%TYPE,
        p_out_msg        OUT   CLOB
    ) AS

        v_string             CLOB := p_inputstr;
        v_substring          VARCHAR2(4000);
       
    --Invalid part
        v_msg_invalid_part   VARCHAR2(4000);
        v_msg_prt_cmpny      VARCHAR2(4000);
    --part number and cost  split
        v_qty                VARCHAR2(4000);
        v_out_status         VARCHAR2(1) := 'N';
        v_partnumber         t302_vendor_standard_cost.c205_part_number_id%TYPE;
        v_void_fl            VARCHAR2(1);
    BEGIN
	 
	--To call Invalid part Number validation 
        gm_pkg_pd_partnumber.gm_validate_mfg_parts(p_partinputstr, v_msg_invalid_part, v_msg_prt_cmpny);
        
        IF v_msg_invalid_part IS NOT NULL THEN
            p_out_msg := v_out_status|| '##'|| v_msg_invalid_part;
	     --
        END IF;
	
      --If error message is  null split partnumber and cost.
      --Format:partnumber^qty^flag,|partnumber^cost^flag.  

        IF v_msg_invalid_part IS NULL THEN
        --String Split
            WHILE instr(v_string, '|') <> 0 LOOP
                v_substring := substr(v_string, 1, instr(v_string, '|') - 1);

                v_string := substr(v_string, instr(v_string, '|') + 1);
				--
                v_partnumber := NULL;
                v_qty := NULL;
                v_void_fl := NULL;
				--
                v_partnumber := substr(v_substring, 1, instr(v_substring, '^') - 1);

                v_substring := substr(v_substring, instr(v_substring, '^') + 1);
                
                v_qty := substr(v_substring, 1, instr(v_substring, '^') - 1);

                v_void_fl := substr(v_substring, instr(v_substring, '^') + 1);
                --
                gm_pkg_op_ttp_by_vendor_moq_bulk.gm_save_vendor_moq_dtls(p_vendor_id, v_partnumber, v_qty, v_void_fl, p_user_id);
            END LOOP;--String loop
			
		--set flag  y for success message
            v_out_status := 'Y';
            p_out_msg := v_out_status;
        END IF;--if loop end

    END gm_process_ttp_by_vendor_moq_bulk;  
 
 /*************************************************************************
    * Description : This procedure used to insert or update the pnum,qty,void flag 
    				for each vendor in t3010_vendor_moq and also trg_3010_vendor_moq
    				trigger  is called when insert or update happen 
    				in t3010 table
    * Author      : Prabhu vigneshwaran M D 
    *************************************************************************/

    PROCEDURE gm_save_vendor_moq_dtls (
        p_vendor_id     IN   t3010_vendor_moq.c301_vendor_id%TYPE,
        p_part_number   IN   t3010_vendor_moq.c205_part_number_id%TYPE,
        p_moq_qty       IN   t3010_vendor_moq.c3010_moq%TYPE,
        p_void_fl       IN   t3010_vendor_moq.c3010_void_fl%TYPE,
        p_user_id       IN   t3010_vendor_moq.c3010_updated_by%TYPE
    ) AS
    --
    BEGIN
	    
        UPDATE t3010_vendor_moq
        SET
            c3010_moq = p_moq_qty,
            c3010_void_fl = p_void_fl,
            c3010_updated_by = p_user_id,
            c3010_updated_date = current_date
        WHERE
            c301_vendor_id = p_vendor_id
            AND c205_part_number_id = p_part_number;
    --

        IF ( SQL%rowcount = 0 ) THEN
            INSERT INTO t3010_vendor_moq (
                c301_vendor_id,
                c205_part_number_id,
                c3010_moq,
                c3010_updated_by,
                c3010_updated_date,
                c3010_void_fl
            ) VALUES (
                p_vendor_id,
                p_part_number,
                p_moq_qty,
                p_user_id,
                current_date,
                p_void_fl
            );

        END IF;

   END gm_save_vendor_moq_dtls;

END gm_pkg_op_ttp_by_vendor_moq_bulk;
/