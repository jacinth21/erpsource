CREATE OR REPLACE PACKAGE BODY gm_pkg_vendor_mail_fch IS
       
   /*********************************************************************
	Description : This Procedure is used to fetch purchase order the PO attributes.
	Author: PARTHIBAN
	*********************************************************************/

    PROCEDURE gm_fch_po_email_dtl (
        p_mail_st   IN    NUMBER,
        p_ref_act   IN    t401_purchase_order.c401_purchase_ord_id%TYPE,
        p_out       OUT   types.cursor_type
    ) AS
        v_date_fmt VARCHAR2(50);
    BEGIN
        SELECT
            get_compdtfmt_frm_cntx()
        INTO v_date_fmt
        FROM
            dual;

        IF p_ref_act = 108405  --Updated Purchase Order

         THEN
            OPEN p_out FOR SELECT
                               t401.c301_vendor_id         vid,
                               c301_vendor_name            vnm,
                               t205.c205_part_number_id    pnum,
                               t205.c205_part_num_desc     pdesc,
                               t401.c401_purchase_ord_id   refid,
                               t402.c402_work_order_id     woid,
                               c402_rev_num WOREV,
                               c402_qty_ordered            ordqty,
                               get_code_name(C301_CURRENCY) cur,
                               ( c402_qty_ordered * c402_cost_price ) extprice,
                               TO_CHAR(c401_date_required, v_date_fmt) reqdt,
                               TO_CHAR (t402.c402_wo_due_date, v_date_fmt) woduedt,
                               get_user_name(C402_LAST_UPDATED_BY) updatedby,
                               t401.c401_publish_fl        fl,
                               get_vendor_mail_id(t401.c301_vendor_id, t4206.c901_type) emailid,
                               t4206.c901_type             refact
                           FROM
                               t401_purchase_order   t401,
                               t402_work_order       t402,
                               t301_vendor           t301,
                               t205_part_number      t205,
                               t4206_vendor_email    t4206
                           WHERE
                               t401.c401_purchase_ord_id = t402.c401_purchase_ord_id
                               AND t301.c301_vendor_id = t401.c301_vendor_id
                               AND t401.c301_vendor_id = t301.c301_vendor_id
                               AND t205.c205_part_number_id = t402.c205_part_number_id
                               AND t4206.c4206_ref_id = t401.c401_purchase_ord_id
                               AND t401.c401_void_fl IS NULL
                               AND t402.c402_void_fl IS NULL
                               AND t4206.c4206_void_fl IS NULL
                               AND t4206.c4206_email_status = p_mail_st
                               AND t4206.c901_type = p_ref_act
                               AND t402.C402_QTY_ORDERED > 0
                           ORDER BY
                               vid,
                               refact;

        END IF;

        IF p_ref_act = 108400   --Published Purchase Order

         THEN
            
            OPEN p_out FOR SELECT
                               t401.c301_vendor_id          vid,
                               c401_purchase_ord_id         refid,
                               c401_po_total_amount         poamt,
                               c301_vendor_name             vnm,
                               get_code_name(C301_CURRENCY) cur,
                               TO_CHAR(c401_date_required, v_date_fmt) reqdt,
                               get_user_name(c401_created_by) raisedby,
                               t401.c401_publish_fl         fl,
                               get_vendor_mail_id(t401.c301_vendor_id, t4206.c901_type) emailid,
                               t4206.c901_type              refact
                           FROM
                               t401_purchase_order   t401,
                               t301_vendor           t301,
                               t4206_vendor_email    t4206
                           WHERE
                               t301.c301_vendor_id = t401.c301_vendor_id
                               AND t4206.c4206_ref_id = t401.c401_purchase_ord_id
                               AND t401.c401_void_fl IS NULL
                               AND t4206.c4206_void_fl IS NULL
                               AND t4206.c4206_email_status = p_mail_st
                               AND t4206.c901_type = p_ref_act
                           ORDER BY
                               vid,
                               refact;

        END IF;

        IF p_ref_act = 26240621   --Voided Purchase Order

         THEN
            OPEN p_out FOR SELECT
                               t401.c301_vendor_id          vid,
                               c401_purchase_ord_id         refid,
                               c301_vendor_name             vnm,
                               c401_po_total_amount         poamt,
                               get_code_name(C301_CURRENCY) cur,
                               TO_CHAR(c401_date_required, v_date_fmt) reqdt,
                               get_user_name(c401_last_updated_by) voidedby,
                               get_user_name(c401_created_by) raisedby,
                               t401.c401_publish_fl         fl,
                               get_vendor_mail_id(t401.c301_vendor_id, t4206.c901_type) emailid,
                               t4206.c901_type              refact
                           FROM
                               t401_purchase_order   t401,
                               t301_vendor           t301,
                               t4206_vendor_email    t4206
                           WHERE
                               t301.c301_vendor_id = t401.c301_vendor_id
                               AND t4206.c4206_ref_id = t401.c401_purchase_ord_id
                               AND t4206.c4206_void_fl IS NULL
                               AND t4206.c4206_email_status = p_mail_st
                               AND t4206.c901_type = p_ref_act
                           ORDER BY
                               vid,
                               refact;

        END IF;
	IF p_ref_act = 108401 OR p_ref_act = 108402 OR p_ref_act = 108403 THEN   --108401:Acknowledged Purchase Order  , 108402: Accepted Purchase Order, 108403:Rejected Purchase Order
				
		OPEN p_out FOR
		
		  select t301.c301_vendor_id VID,
		 		 t401.c401_purchase_ord_id REFID,
                 t4206.c901_type REFACT,
			   	 t301.c301_vendor_name VNM, 
			     t401.c401_po_total_amount POAMT, 
			     TO_CHAR (t401.c401_date_required, v_date_fmt) REQDT,
			     GET_USER_NAME(t401.c401_created_by) RAISEDBY,
			     gm_pkg_vendor_mail_fch.get_po_reason(t401.c401_purchase_ord_id) REASON,
			     gm_pkg_vendor_mail_fch.get_po_cmnt(t401.c401_purchase_ord_id) COM,
			     get_code_name(C301_CURRENCY) cur,
			     get_vendor_mail_id(t401.c301_vendor_id,p_ref_act) EMAILID,
			     t401.c401_purchase_ord_id REFID
		    from T401_PURCHASE_ORDER t401, 
		    	 T301_VENDOR t301, 
		    	 T4206_VENDOR_EMAIL t4206
		   where t401.c301_vendor_id = t301.c301_vendor_id 
     		 and t4206.c4206_ref_id = t401.c401_purchase_ord_id
     		 and t4206.c301_vendor_id = t301.c301_vendor_id
     		 and t4206.c4206_email_status = p_mail_st
     		 and t4206.c901_type = p_ref_act
     		 and t4206.c4206_void_fl IS NULL 
     		 and t401.c401_void_fl IS NULL order by vid;
    END IF;
	
        END gm_fch_po_email_dtl;

  /*********************************************************************
	Description : This function is used to save work order the WO attributes.
	Author: PARTHIBAN
	*********************************************************************/

    PROCEDURE gm_fch_wo_email_dtl (
        p_mail_st   IN    NUMBER,
        p_ref_act   IN    t401_purchase_order.c401_type%TYPE,
        p_out       OUT   types.cursor_type
    ) AS
        v_date_fmt VARCHAR2(50);
    BEGIN
	     SELECT
            get_compdtfmt_frm_cntx()
        INTO v_date_fmt
        FROM
            dual;
        IF p_ref_act = 26240620 THEN
            OPEN p_out FOR 
            			 
            			 SELECT
                               t401.c301_vendor_id         vid,
                               t205.c205_part_number_id    pnum,
                               t205.c205_part_num_desc     pdesc,
                               t401.c401_purchase_ord_id   poid,
                               t402.c402_work_order_id     refid,
                               c402c_wo_curr_rev           preworev,
                               c402c_wo_new_rev            worev,
                               c402_qty_ordered            orqty,
                               get_wo_pend_qty(t402.c402_work_order_id, t402.c402_qty_ordered) pend,
                               c402_cost_price             priceea,
                               get_code_name(C301_CURRENCY) cur,
                               ( c402_qty_ordered * c402_cost_price ) extprice,
                               TO_CHAR(c401_date_required, v_date_fmt) reqdt,
                               TO_CHAR (t402.c402_wo_due_date, v_date_fmt) woduedt,
                               get_user_name(c402_created_by) rsdby,
                               c401_po_notes               reason,
                               get_vendor_mail_id(t401.c301_vendor_id, t4206.c901_type) emailid,
                               t4206.c901_type             refact,
                               get_wo_rec_qty(t402.c402_work_order_id) recqty
                           FROM
                               t401_purchase_order         t401,
                               t402_work_order             t402,
                               t301_vendor                 t301,
                               t205_part_number            t205,
                               t402c_work_order_revision   t402c,
                               t4206_vendor_email          t4206
                               
                           WHERE
                               t401.c401_purchase_ord_id = t402.c401_purchase_ord_id
                               AND t301.c301_vendor_id = t401.c301_vendor_id
                               AND t401.c301_vendor_id = t301.c301_vendor_id
                               AND t205.c205_part_number_id = t402.c205_part_number_id
                               AND t402.c402_work_order_id = t402c.c402_work_order_id
                               AND t4206.c4206_ref_id = t402.c402_work_order_id
                               AND t4206.c4206_email_status = p_mail_st
                               AND t4206.c901_type = p_ref_act
                               AND t401.c401_void_fl IS NULL
                               AND t402.c402_void_fl IS NULL
                               AND t402c.c402c_void_fl IS NULL
                           ORDER BY
                               vid,
                               refact;

        END IF;
        IF p_ref_act = 108404  THEN    --108404: Rejected Work Order				
		OPEN p_out FOR
		
		select t301.c301_vendor_id VID,
			   t4206.c901_type REFACT,
			   t205.c205_part_number_id PNUM,
			   t205.c205_part_num_desc PDESC,
			   t402.c402_work_order_id REFID, 
			   t401.c401_purchase_ord_id PO, 
			   c301_vendor_name VNM,
     		   (t402.c402_qty_ordered * t402.c402_cost_price) EXTPR,
   			   TO_CHAR (c401_date_required, v_date_fmt) REQDT,
   			   TO_CHAR (t402.c402_wo_due_date, v_date_fmt) WODUEDT,
   			   GET_USER_NAME(C402_CREATED_BY) RAISEDBY,
   			   gm_pkg_vendor_mail_fch.get_po_reason(t402.c402_work_order_id) REASON, 
   		       gm_pkg_vendor_mail_fch.get_po_cmnt(t402.c402_work_order_id)  COM,
   		       get_code_name(C301_CURRENCY) cur,
   			   GET_VENDOR_MAIL_ID(t401.c301_vendor_id,p_ref_act) EMAILID
   		  from T401_PURCHASE_ORDER t401,
   		  	   T402_WORK_ORDER t402, 
   		  	   T301_VENDOR t301,
   		  	   T205_PART_NUMBER t205,
   		  	   T402C_WORK_ORDER_REVISION t402c,
   		  	   T4206_VENDOR_EMAIL t4206
   		 where t401.c401_purchase_ord_id = t402.c401_purchase_ord_id 
   		   and t301.c301_vendor_id = t401.c301_vendor_id 
   		   and t205.c205_part_number_id = t402.c205_part_number_id
   		   and t402.c402_work_order_id = t402c.c402_work_order_id(+)
   		   and t4206.c4206_ref_id = t402.c402_work_order_id
     	   and t4206.c301_vendor_id = t301.c301_vendor_id
     	   and t4206.c4206_email_status = p_mail_st 
     	   and t4206.c901_type = p_ref_act
     	   and t4206.c4206_void_fl IS NULL 
     	   and t401.c401_void_fl IS NULL 
     	   and t402.c402_void_fl IS NULL
     	   and t402c.c402c_void_fl IS NULL order by vid;
   END IF;
   
   IF p_ref_act = 26240623 THEN     --26240623: Reopened Work Order
				
		OPEN p_out FOR
		select t301.c301_vendor_id VID,
			   t4206.c901_type REFACT,
			   t205.c205_part_number_id PNUM,
			   t205.c205_part_num_desc PDESC,
			   t401.c401_purchase_ord_id PO, 
			   t402.c402_work_order_id REFID,
			   --c402c_wo_curr_rev WOREV, 
			   t402.c402_rev_num WOREV,
			   c402_qty_ordered ORQTY, 
			   GET_WO_PEND_QTY (t402.c402_work_order_id,t402.c402_qty_ordered) PENDQTY,
			   c402_cost_price PRICEEA, 
			   (c402_qty_ordered * c402_cost_price ) EXTPRICE,
			   TO_CHAR (c401_date_required, v_date_fmt) REQDT,
			   TO_CHAR (t402.c402_wo_due_date, v_date_fmt) WODUEDT,
			   GET_USER_NAME(c402_created_by) RSDBY , 
			   gm_pkg_vendor_mail_fch.get_po_reason(t402.c402_work_order_id) REASON,
			   gm_pkg_vendor_mail_fch.get_po_cmnt(t402.c402_work_order_id) CMNTS,
			   get_code_name(C301_CURRENCY) cur,
			   GET_VENDOR_MAIL_ID(t401.c301_vendor_id,p_ref_act) EMAILID
   		  from T401_PURCHASE_ORDER t401,
   		  	   T402_WORK_ORDER t402, 
   		  	   T301_VENDOR t301,
   		  	   T205_PART_NUMBER t205,
   		  	   T4206_VENDOR_EMAIL t4206
   		  	   --T402C_WORK_ORDER_REVISION t402c
   		 where t401.c401_purchase_ord_id = t402.c401_purchase_ord_id and t301.C301_VENDOR_ID =t401.C301_VENDOR_ID 
   		   and t205.c205_part_number_id = t402.c205_part_number_id
   		  -- and t402.c402_work_order_id = t402c.c402_work_order_id (+) 
   		   and t4206.c4206_ref_id= t402.C402_WORK_ORDER_ID
     	   and t4206.c301_vendor_id=t301.c301_vendor_id
     	   and t4206.c4206_email_status = p_mail_st
     	   and t4206.c901_type = p_ref_act
     	   and t4206.c4206_void_fl IS NULL 
     	   and t401.c401_void_fl IS NULL 
     	   and t402.c402_void_fl IS NULL
     	   --and t402c.c402c_void_fl IS NULL 
     	   order by vid;
     END IF; 
     
     IF p_ref_act = 108406 THEN     --108406: Shipped Purchase Order				
		OPEN p_out FOR
		
		select t301.c301_vendor_id VID,
			   t4206.c901_type REFACT,
		 	   t205.c205_part_number_id PNUM,
		 	   t205.c205_part_num_desc PDESC,
		 	   t401.c401_purchase_ord_id PO, 
		 	   t402.c402_work_order_id REFID,
		 	   t402.C402_REV_NUM WOREV, 
		 	   t402.c402_qty_ordered ORQTY, 
		 	   GET_SHIPPED_QTY(t402.c402_work_order_id) sqty,
		 	   t402.c402_cost_price PRICEEA,
		 	   TO_CHAR (t401.c401_date_required, v_date_fmt) REQDT,
		 	   TO_CHAR (t402.c402_wo_due_date, v_date_fmt) WODUEDT,
		 	   GET_USER_NAME(t402.c402_created_by) RSDBY, 
		 	   (t402.c402_qty_ordered * t402.c402_cost_price ) EXTPRICE,
		 	   t4200.c4200_tracking_no TRKNO,
		 	   c901_carrier CRR,
		 	   get_code_name(C301_CURRENCY) cur,
		 	   GET_VENDOR_MAIL_ID(t401.c301_vendor_id,p_ref_act) EMAILID
		from T401_PURCHASE_ORDER t401,
   		  	   T402_WORK_ORDER t402, 
   		  	   T301_VENDOR t301,
   		  	   T205_PART_NUMBER t205,
   		  	   T402C_WORK_ORDER_REVISION t402c, 
   		  	   T4206_VENDOR_EMAIL t4206, 
   		  	   T4200_DELIVERY_ROUTER t4200  			   
   		 where t401.c401_purchase_ord_id = t402.c401_purchase_ord_id 
   		   and t4206.c4206_ref_id = t402.c402_work_order_id
     	   and t4206.c301_vendor_id = t301.c301_vendor_id
   		   and t301.c301_vendor_id = t401.c301_vendor_id 
  		   and t205.c205_part_number_id = t402.c205_part_number_id
   		   and t402.c402_work_order_id = t402c.c402_work_order_id (+)
  		   and t4200.c401_purchase_ord_id = t401.c401_purchase_ord_id
  		   and t4206.c4206_email_status = p_mail_st
  		   and t4206.c901_type = p_ref_act
  		   and trunc(t4200.C4200_CREATED_DT) = trunc(current_date)
		   and t4206.c4206_void_fl IS NULL 
     	   and t401.c401_void_fl IS NULL 
     	   and t402.c402_void_fl IS NULL
     	   and t402c.c402c_void_fl IS NULL
     	   and t4200.c4200_void_fl IS NULL order by vid;

   END IF;
   
   --PC#3577-Vedor_portal_wo_due_update_notification
   IF p_ref_act = 26241178 THEN     --26241178: WO Due
				
		OPEN p_out FOR
		select t301.c301_vendor_id VID,
			   t4206.c901_type REFACT,
			   t205.c205_part_number_id PNUM,
			   t205.c205_part_num_desc PDESC,
			   t401.c401_purchase_ord_id PO, 
			   t402.c402_work_order_id REFID, 
			   t402.c402_rev_num WOREV,
			   c402_qty_ordered ORQTY, 
			   GET_WO_PEND_QTY (t402.c402_work_order_id,t402.c402_qty_ordered) PENDQTY,
			   c402_cost_price PRICEEA, 
			   (c402_qty_ordered * c402_cost_price ) EXTPRICE,
			   TO_CHAR (t402.c402_wo_due_date, v_date_fmt) WODUEDT, 
			   GET_USER_NAME(c402_created_by) RSDBY , 
			   gm_pkg_vendor_mail_fch.get_po_reason(t402.c402_work_order_id) REASON,
			   gm_pkg_vendor_mail_fch.get_po_cmnt(t402.c402_work_order_id) CMNTS,
			   get_code_name(C301_CURRENCY) cur,
			   GET_VENDOR_MAIL_ID(t401.c301_vendor_id,p_ref_act) EMAILID
   		  from T401_PURCHASE_ORDER t401,
   		  	   T402_WORK_ORDER t402, 
   		  	   T301_VENDOR t301,
   		  	   T205_PART_NUMBER t205,
   		  	   T4206_VENDOR_EMAIL t4206
   		 where t401.c401_purchase_ord_id = t402.c401_purchase_ord_id and t301.C301_VENDOR_ID =t401.C301_VENDOR_ID 
   		   and t205.c205_part_number_id = t402.c205_part_number_id
   		   and t4206.c4206_ref_id= t402.C402_WORK_ORDER_ID
     	   and t4206.c301_vendor_id=t301.c301_vendor_id
     	   and t4206.c4206_email_status = p_mail_st
     	   and t4206.c901_type = p_ref_act
     	   and t4206.c4206_void_fl IS NULL 
     	   and t401.c401_void_fl IS NULL 
     	   and t402.c402_void_fl IS NULL
     	   order by vid;
     END IF; 

    END gm_fch_wo_email_dtl;

/*****************************************************************************************
 *   Description     : THIS FUNCTION RETURNS VENDOR EMAIL INSTANCE FLAG GIVEN THE VENDOR_ID AND VENDOR_ACTION
 Parameters 		: p_ven_id , p_type
*****************************************************************************************/

    FUNCTION get_vendor_instance_fl (
        p_vend_id   t301_vendor.c301_vendor_id%TYPE,
        p_action    t4206_vendor_email.c901_type%TYPE
    ) RETURN VARCHAR2 IS
        v_instance_fl t107_contact.c107_instance_fl%TYPE;
    BEGIN
        BEGIN
            SELECT
                DECODE(c107_instance_fl, 'Y', 0, 1)
            INTO v_instance_fl
            FROM
                t107_contact   t107,
                t301_vendor    t301
            WHERE
                t107.c101_party_id = t301.c101_party_id
                AND t107.c107_void_fl IS NULL
                AND t301.c301_vendor_id = p_vend_id
                AND t107.c901_mode = p_action;

        EXCEPTION
            WHEN no_data_found THEN
                RETURN '1';
        END;

        RETURN v_instance_fl;
    END get_vendor_instance_fl;
     
     
     /*****************************************************************************************
      *   Description     : THIS FUNCTION RETURNS SUM OF SHIPPED QTY GIVEN THE PURCHASE ID
 	Parameters 		: p_po_id
	*****************************************************************************************/

    FUNCTION get_shipped_qty (
        p_wo_id t402_work_order.c402_work_order_id%TYPE
    ) RETURN VARCHAR2 IS
        v_ship_qty t4201_delivery_item.c4201_ship_qty%TYPE;
    BEGIN
        BEGIN
            SELECT
                SUM(c4201_ship_qty)
            INTO v_ship_qty
            FROM
                t4201_delivery_item
            WHERE
                c402_work_order_id IN (p_wo_id);

        EXCEPTION
            WHEN no_data_found THEN
                RETURN '0';
        END;

        RETURN v_ship_qty;
    END get_shipped_qty;
     
     
      /*  ************************************************************************************
       * Description     : THIS FUNCTION RETURNS VENDOR ACCESS FLAG GIVEN THE VENDOR ID
        Parameters         : p_po_id
    ****************************************************************************************/

    FUNCTION get_vendor_fl (
        p_vend_id t301_vendor.c301_vendor_id%TYPE
    ) RETURN VARCHAR2 IS
        v_vendor_fl t301_vendor.c301_vendor_portal_fl%TYPE;
    BEGIN
        BEGIN
	            SELECT
	                c301_vendor_portal_fl
	            INTO v_vendor_fl
	            FROM
	                t301_vendor
	            WHERE
	                c301_vendor_id = p_vend_id;

        EXCEPTION 
            WHEN no_data_found THEN
                RETURN 'N';
        END;

        RETURN v_vendor_fl;
    END get_vendor_fl;
  /***************************************************************
  * Description:This Procedure used to fetch dhr email details
  * Author:
  ****************************************************************/
PROCEDURE gm_fch_dhr_email_dtl(
    p_mail_st IN t4206_vendor_email.c4206_email_status%type,
    p_ref_act IN t4206_vendor_email.c901_type%type,
    p_out OUT TYPES.cursor_type )
AS
  v_date_fmt VARCHAR2 (20) ;
BEGIN
	--get date format
  SELECT get_compdtfmt_frm_cntx () INTO v_date_fmt FROM dual;
  
  OPEN p_out FOR 
  SELECT --t301.c301_vendor_portal_fl VENFL,
  t301.c301_vendor_id VID,
  t301.c301_vendor_name VENNM,
  t205.c205_part_number_id PNUM,
  get_code_name(C301_CURRENCY) CUR,
  t205.c205_part_num_desc PDESC,
  t401.c401_purchase_ord_id POID,
  t402.c402_work_order_id WOID,
  t402.c402_rev_num WOREV,
  t402.c402_qty_ordered ORDQTY,
  t408.c408_qty_received RECQTY,
  t402.c402_cost_price PRICEEA,
  (t402.c402_qty_ordered * t402.c402_cost_price ) EXTPRICE,
  t401.c401_publish_fl FL ,
  t408.c408_dhr_id REFID,
  t408.c408_qty_rejected QTYREJ,
  t409.c409_ncmr_id NCMRID,
  t401.c401_po_total_amount POTOT,
  get_user_name(t408.c408_created_by) REQBY,
  get_user_name(t402.c402_created_by) RAISEDBY,
  get_user_name(t408.c408_inspected_by) INSPECTEDBY,
  TO_CHAR(t408.c408_inspected_ts,v_date_fmt) INSPECTEDDT,
  TO_CHAR(t401.c401_date_required,v_date_fmt)REQDT,
  GET_VENDOR_MAIL_ID(t401.c301_vendor_id,t4206.c901_type) EMAILID,
  t4206.c901_type REFACT FROM t401_purchase_order t401,
  t402_work_order t402,
  t301_vendor t301,
  T205_Part_number t205,
  t408_dhr t408,
  t4206_vendor_email t4206,
  t409_ncmr t409
  WHERE t401.c401_purchase_ord_id = t402.c401_purchase_ord_id 
  AND t402.c402_work_order_id = t408.c402_work_order_id
  AND t301.c301_vendor_id = t408.c301_vendor_id 
  AND t401.c301_vendor_id = t301.c301_vendor_id
  AND t205.c205_part_number_id = t408.c205_part_number_id 
  AND t408.c408_dhr_id = t4206.c4206_ref_id
  AND t408.c408_dhr_id = t409.c408_dhr_id(+)
  AND t4206.c4206_email_status = p_mail_st 
  AND t4206.c901_type = p_ref_act
  AND t401.c401_void_fl IS NULL 
  AND t402.c402_void_fl IS NULL 
  AND t408.c408_void_fl IS NULL
  AND t409.c409_void_fl IS NULL
  AND t4206.c4206_void_fl IS NULL 
  ORDER BY t301.c301_vendor_id,t4206.c901_type;
END gm_fch_dhr_email_dtl;
  /***************************************************************
  * Description:This Procedure used to fetch PO closed count
  * Author:
  ****************************************************************/
PROCEDURE gm_fch_po_closed_count(
    p_ref_id IN VARCHAR2,
    p_out OUT NUMBER )
AS
  v_po_id t402_work_order.c401_purchase_ord_id%type;
  v_cnt NUMBER;
BEGIN
	-- Get Po id 
  BEGIN
    SELECT c401_purchase_ord_id
    INTO v_po_id
    FROM t402_work_order
    WHERE C402_WORK_ORDER_ID = p_ref_id
    AND c402_void_fl        IS NULL
    GROUP BY c401_purchase_ord_id;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    v_po_id := NULL;
  END;
  --Get count for PO closed
  SELECT COUNT(*)
  INTO v_cnt
  FROM t402_work_order
  WHERE c401_purchase_ord_id = v_po_id
  AND c402_status_fl        <> 3
  AND c402_void_fl          IS NULL;
  
  p_out := v_cnt;
  
END gm_fch_po_closed_count;

--
/********************************************************************************************
 * Description  :This function returns the Work order recived qty
 * Author: Agilan
 *********************************************************************************************/    
FUNCTION GET_WO_REC_QTY
(
 p_wo_id t408_dhr.c402_work_order_id%TYPE
)
RETURN NUMBER
IS

v_rec_cnt NUMBER;
--
BEGIN
 SELECT SUM(c408_qty_received)
   INTO v_rec_cnt
   FROM t408_dhr t408
  WHERE t408.c402_work_order_id = p_wo_id
    AND t408.c408_void_fl IS NULL;
RETURN v_rec_cnt;
--
   EXCEPTION
      WHEN NO_DATA_FOUND
   THEN
      RETURN '';
--
END GET_WO_REC_QTY;

/********************************************************************************************
 Description     : THIS FUNCTION RETURNS VENDOR EMAIL ID GIVEN THE VENDOR_ID AND REF TYPE
 Parameters 	 : p_ven_id , p_type
 *********************************************************************************************/    
  FUNCTION GET_VENDOR_MAIL_ID (
		  p_ven_id       IN   T4206_VENDOR_EMAIL.C4206_REF_ID%TYPE,
		  p_type        IN   T4206_VENDOR_EMAIL.C901_REF_TYPE%TYPE)
		  RETURN VARCHAR2
		  IS
		v_email_id T107_CONTACT.C107_CONTACT_VALUE%TYPE;
	BEGIN
		BEGIN
		
		SELECT c107_contact_value INTO v_email_id from 
				T107_CONTACT t107,
				T301_VENDOR t301
		  WHERE	t107.c101_party_id = t301.c101_party_id
			AND t107.c107_primary_fl = 'Y'
			AND t107.c901_mode = p_type
			and t301.c301_vendor_id = p_ven_id 
			and t107.c107_void_fl IS NULL
			AND t301.c301_active_fl IS NULL;
			EXCEPTION
			 WHEN NO_DATA_FOUND
			 THEN
				RETURN '';
		  END;
				RETURN v_email_id;
				
	   END GET_VENDOR_MAIL_ID;
 /*********************************************************************
	Description : This Procedure is used to fetch invoice details.
	Author: Agilan
	*********************************************************************/

    PROCEDURE gm_fch_invoice_email_dtl (
        p_mail_st   IN    NUMBER,
        p_ref_act   IN    t401_purchase_order.c401_purchase_ord_id%TYPE,
        p_out       OUT   types.cursor_type
    ) 
    AS
    BEGIN
       
        IF p_ref_act = 26240624  THEN             --Invoice Marked as Paid
 
         OPEN p_out FOR
         
           SELECT t401.c301_vendor_id vid,
           		  t4206.c901_type refact,
           		  get_vendor_mail_id(t401.c301_vendor_id, t4206.c901_type) emailid,
           		  --t4204.c4204_vendor_invoice_id invid, 
           		  t4204.c401_purchase_ord_id poid,
                  t4204.c4204_amount invamt, 
                  --t4204.c401_purchase_ord_id refid,
                  t4204.c4204_vendor_invoice_id refid,
                  get_code_name(C301_CURRENCY) cur, 
                  t401.c401_po_total_amount poamt,
                  get_user_name(NVL(t4204.c4204_updated_by,t4204.C4204_CREATED_BY)) paidby 
             FROM T401_PURCHASE_ORDER t401,
                  T4204_VENDOR_INVOICE t4204,
                  T4206_VENDOR_EMAIL t4206,
                  t301_vendor t301
			WHERE t401.c401_purchase_ord_id = t4204.c401_purchase_ord_id
			  AND t401.c301_vendor_id = t301.c301_vendor_id
			  AND t4206.c4206_ref_id = t4204.c4204_vendor_invoice_id
			  AND t4204.c901_status = '108620'        --Mark as Paid
			  AND t4206.c4206_email_status = p_mail_st
			  AND t4206.c901_type = p_ref_act
			  AND t401.c401_void_fl IS NULL
              AND t4206.c4206_void_fl IS NULL
              AND t4204.c4204_void_fl IS NULL
         ORDER BY vid,refact;
       
       END IF;
       
       		IF p_ref_act = 26240672  THEN   --  Upload invoice
 
            OPEN p_out FOR 
            
            	SELECT t401.c301_vendor_id vid,
           		  	   t4206.c901_type refact,
           		  	   GET_VENDOR_MAIL_ID(t401.c301_vendor_id,t4206.c901_type) EMAILID,
           		  	   --t4204.c4204_vendor_invoice_id invid
           		  	   t4204.c401_purchase_ord_id poid, 
            		   t4204.c4204_amount invamt, 
            		   --t4204.c401_purchase_ord_id refid,
            		   t4204.c4204_vendor_invoice_id refid,
            		   get_code_name(C301_CURRENCY) cur, 
            		   c401_po_total_amount poamt,
            		   get_user_name(t4204.c4204_invoiced_by) uploadedby 
            	  FROM T401_PURCHASE_ORDER t401,
            	  	   T4204_VENDOR_INVOICE t4204,
                       T4206_VENDOR_EMAIL t4206,
--                       T903_UPLOAD_FILE_LIST t903,
                       t301_vendor t301
				 WHERE t401.c401_purchase_ord_id = t4204.c401_purchase_ord_id
				   AND t401.c301_vendor_id = t301.c301_vendor_id
				   AND t4206.c4206_ref_id = t4204.c4204_vendor_invoice_id
--				   AND t903.c903_ref_id = t401.c401_purchase_ord_id
				   AND t4206.c4206_email_status = p_mail_st
				   AND t4206.c901_type = p_ref_act
				   AND t401.c401_void_fl IS NULL
              	   AND t4206.c4206_void_fl IS NULL
              	   AND t4204.c4204_void_fl IS NULL
              ORDER BY vid,refact;
				   
        END IF;

    END gm_fch_invoice_email_dtl;
    
   /*  ************************************************************************************
       * Description     : THIS FUNCTION RETURNS PO is Published or not
       * Parameters      : p_po_id
    ****************************************************************************************/

    FUNCTION get_published_fl (
        p_po_id 	t401_purchase_order.c401_purchase_ord_id%TYPE
    ) RETURN VARCHAR2 IS
        v_pub_fl    t401_purchase_order.c401_publish_fl%TYPE;
    BEGIN
        BEGIN
            SELECT
                c401_publish_fl
            INTO v_pub_fl
            FROM
                T401_PURCHASE_ORDER
            WHERE
                c401_purchase_ord_id = p_po_id;
           -- AND c401_void_fl is null;   // Void PO emails not received 

        EXCEPTION 
            WHEN no_data_found THEN
                RETURN 'N';
        END;

        RETURN v_pub_fl;
    END get_published_fl;
    
        /*********************************************************************
	Description : This Function is used to fetch Purchase/Work order comments
	Author: PARTHIBAN
	*********************************************************************/
    
     FUNCTION get_po_cmnt (
        p_ref_id   IN    t907_cancel_log.C907_REF_ID%TYPE
    ) RETURN VARCHAR2 IS
        v_cmnt    varchar2(4000);
    BEGIN
        BEGIN
	        
	         SELECT 
	         	COMMENTS 
	         INTO v_cmnt
             FROM( 
	          SELECT C907_COMMENTS COMMENTS
	 			 FROM t907_cancel_log
	 			 WHERE C907_REF_ID= p_ref_id
	 			 AND C907_VOID_FL IS NULL
                 ORDER BY C907_LAST_UPDATED_DATE_UTC DESC)
	         WHERE ROWNUM = 1;
	           EXCEPTION 
            WHEN no_data_found THEN
                RETURN '';
	     END; 
	     
	      RETURN v_cmnt;
    END get_po_cmnt;
    
    /*********************************************************************
	Description : This Function is used to fetch Purchase/Work order voided reason
	Author: PARTHIBAN
	*********************************************************************/
        
    
         FUNCTION get_po_reason (
        p_ref_id   IN    t907_cancel_log.C907_REF_ID%TYPE
    ) RETURN VARCHAR2 IS
        v_reason    varchar2(4000);
    BEGIN
        BEGIN
	        
	         SELECT 
	         	REASON  
	         INTO v_reason
             FROM( 
	  		  SELECT GET_CODE_NAME(C901_CANCEL_CD) REASON  
	 		   FROM t907_cancel_log
	 		   WHERE C907_REF_ID= p_ref_id
	 		   AND C907_VOID_FL IS NULL
    		   ORDER BY C907_LAST_UPDATED_DATE_UTC DESC)
	         WHERE ROWNUM = 1;
	         EXCEPTION 
            WHEN no_data_found THEN
                RETURN '';
	     END; 
	     
	      RETURN v_reason;
    END get_po_reason;
    
         
END gm_pkg_vendor_mail_fch;
/