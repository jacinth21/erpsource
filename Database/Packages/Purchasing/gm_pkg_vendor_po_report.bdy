CREATE OR REPLACE PACKAGE BODY GM_PKG_VENDOR_PO_REPORT  
IS
       
/*********************************************************************
	Description : This function is used to Publish bulk PO's.
*********************************************************************/
PROCEDURE gm_sav_blk_publish_po
(
	   p_POId				IN VARCHAR2,
	   p_UserId				IN T401_PURCHASE_ORDER.C401_LAST_UPDATED_BY%TYPE,	   
	   p_publishfl          IN T401_PURCHASE_ORDER.c401_publish_fl%TYPE,
	   p_message            OUT  VARCHAR2
)
AS
       v_message	VARCHAR2(200);
 CURSOR v_cur
       IS
          SELECT C401_PURCHASE_ORD_ID POID
           FROM T401_PURCHASE_ORDER
          WHERE C401_PURCHASE_ORD_ID IN (SELECT token FROM v_in_list)
            AND C401_VOID_FL          IS NULL;
  BEGIN
    my_context.set_my_inlist_ctx (p_POId);   
    FOR v_req IN v_cur
       LOOP
       
       		UPDATE T401_PURCHASE_ORDER SET
          			C401_PUBLISH_FL = p_publishfl,
			   		C401_PUBLISHED_DT = CURRENT_DATE,
			        C401_PUBLISHED_BY = p_UserId,
         			C901_STATUS = '108360',    --Pending Acknowledgement
					C401_LAST_UPDATED_BY = p_UserId,
					C401_LAST_UPDATED_DATE = CURRENT_DATE
			  WHERE C401_PURCHASE_ORD_ID = v_req.POID
			    AND C401_VOID_FL IS NULL;
                  
               GM_UPDATE_LOG(v_req.POID,'Published','1203',p_UserId,v_message);
        END LOOP; 
        EXCEPTION WHEN OTHERS
                 THEN
                       p_message := SQLERRM;
              RETURN;
END gm_sav_blk_publish_po;


/*********************************************************************
	Description : This function is used to Hold bulk PO's.
*********************************************************************/

PROCEDURE gm_sav_vendor_po_hold
(
       p_POId                IN VARCHAR2,
       p_UserId              IN T401_PURCHASE_ORDER.C401_LAST_UPDATED_BY%TYPE
)
AS   
	   v_message	VARCHAR2(200);
  CURSOR v_cur
       IS
          SELECT C401_PURCHASE_ORD_ID POID
           FROM T401_PURCHASE_ORDER
          WHERE C401_PURCHASE_ORD_ID IN (SELECT token FROM v_in_list)
            AND C401_VOID_FL          IS NULL;
  BEGIN
    my_context.set_my_inlist_ctx (p_POId);   
    FOR v_req IN v_cur
       LOOP
                   UPDATE T401_PURCHASE_ORDER SET     
                      C901_STATUS = '108364',
                    C401_LAST_UPDATED_BY = p_UserId,
                    C401_LAST_UPDATED_DATE = CURRENT_DATE
              WHERE C401_PURCHASE_ORD_ID = v_req.POID
               AND C401_VOID_FL          IS NULL;
                  
               GM_UPDATE_LOG(v_req.POID,'Moved to Hold Status','1203',p_UserId,v_message);
        END LOOP;     
END gm_sav_vendor_po_hold;

/*********************************************************************
	Description : This function is used to UnHold bulk PO's.
*********************************************************************/

PROCEDURE gm_sav_vendor_po_unhold
(
       p_POId                IN VARCHAR2,
       p_UserId              IN T401_PURCHASE_ORDER.C401_LAST_UPDATED_BY%TYPE
)
AS   
	   v_message	VARCHAR2(200);
  CURSOR v_cur
       IS
          SELECT C401_PURCHASE_ORD_ID POID
           FROM T401_PURCHASE_ORDER
          WHERE C401_PURCHASE_ORD_ID IN (SELECT token FROM v_in_list)
            AND C401_VOID_FL          IS NULL;
  BEGIN
    my_context.set_my_inlist_ctx (p_POId);   
    FOR v_req IN v_cur
       LOOP
                   UPDATE T401_PURCHASE_ORDER SET     
                      C901_STATUS = DECODE(C401_PUBLISH_FL, 'Y', '108360', '108366'),  --108360:Pending Acknowledgement, 108366-Pending Publish
                    C401_LAST_UPDATED_BY = p_UserId,
                    C401_LAST_UPDATED_DATE = CURRENT_DATE
              WHERE C401_PURCHASE_ORD_ID = v_req.POID
               AND C401_VOID_FL          IS NULL;
                  
               GM_UPDATE_LOG(v_req.POID,'Removed from Hold Status','1203',p_UserId,v_message);
        END LOOP;     
END gm_sav_vendor_po_unhold;


/***************************************************************
 *   Description     : THIS FUNCTION RETURNS VENDOR PORTAL ACCESS FLAG FOR GIVEN VENDOR ID
 *   Parameters 	 : p_vend_id
***************************************************************/
FUNCTION GET_VENDOR_PORTAL_ACCESS_FL
(
p_vend_id T301_VENDOR.C301_VENDOR_ID%TYPE
)
RETURN VARCHAR2
IS
v_vend_fl  T301_VENDOR.C301_VENDOR_PORTAL_FL%TYPE;
BEGIN
     BEGIN
       SELECT c301_vendor_portal_fl
	     INTO v_vend_fl
	     FROM T301_VENDOR
        WHERE c301_vendor_id = p_vend_id
          AND nvl(C301_ACTIVE_FL,'Y') <> 'N';
   EXCEPTION
      WHEN NO_DATA_FOUND
   THEN
      RETURN ' ';
      END;
     RETURN v_vend_fl;
END GET_VENDOR_PORTAL_ACCESS_FL;

END GM_PKG_VENDOR_PO_REPORT;
/