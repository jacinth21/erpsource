--@"Database\Packages\Purchasing\gm_pkg_op_ttp_by_vendor_moq.bdy";
CREATE OR REPLACE
PACKAGE BODY gm_pkg_op_ttp_by_vendor_moq
IS
  /****************************************************************************************************
  * Description  : Procedure used to Fetch the vendor MOQ Details 
  *                Based on vendor,division,part number,project id,purchase agent
  * Author       :  Tamizhthangam Ramasamy
  *****************************************************************************************************/
PROCEDURE gm_fch_ttp_by_vendor_moq_rpt(
    p_vendor_id      IN t3010_vendor_moq.c301_vendor_id%TYPE,
    p_part_number_id IN VARCHAR2,
    p_project_id     IN t202_project.c202_project_id%TYPE,
    p_division_id    IN t1910_division.c1910_division_id%TYPE,
    p_purchse_agent  IN t301_vendor.c301_purchasing_agent_id%TYPE,
    p_out_moq_rpt_dtls OUT CLOB )
AS
  v_date_fmt VARCHAR2 (12);
  
BEGIN
  SELECT get_compdtfmt_frm_cntx ()INTO v_date_fmt FROM DUAL;
  
  SELECT JSON_ARRAYAGG( JSON_OBJECT ( 
         'vendornm'    VALUE t301.c301_vendor_name, 
         'partnum'     VALUE t205.c205_part_number_id, 
         'partdesc'    VALUE REGEXP_REPLACE(t205.c205_part_num_desc,'[^0-9A-Za-z]', ' '), 
         'projectname' VALUE REGEXP_REPLACE(t202.c202_project_nm,'[^0-9A-Za-z]', ' '), 
         'divisionnm'  VALUE t1910.c1910_division_name, 
         'moq'         VALUE t3010.c3010_moq, 
         'updatedby'   VALUE NVL(t101.c101_user_f_name|| ' '|| t101.c101_user_l_name,' '), 
         'updateddate' VALUE NVL(TO_CHAR(t3010.c3010_updated_date, v_date_fmt||' HH:MI:SS AM'),' '),
         'purchaseagentname' VALUE get_user_name(t301.c301_purchasing_agent_id),
         'moqid'       VALUE t3010.c3010_vendor_moq_id,
         'historyfl'   VALUE  t3010.c3010_vendor_moq_hist_fl)
ORDER BY UPPER(t301.c301_vendor_name),t202.c202_project_nm, t1910.c1910_division_name 
         returning CLOB)
    INTO p_out_moq_rpt_dtls
    FROM t3010_vendor_moq t3010 ,
         t205_part_number t205 ,
         t202_project t202,
         t1910_division t1910,
         t101_user t101,
         t301_vendor t301
   WHERE t3010.c205_part_number_id = t205.c205_part_number_id
     AND t202.c202_project_id        = t205.c202_project_id
     AND t1910.c1910_division_id     = t202.c1910_division_id
     AND t301.c301_vendor_id         = t3010.c301_vendor_id
     AND t3010.c3010_updated_by      = t101.c101_user_id         
     AND t3010.c301_vendor_id        = DECODE(p_vendor_id,NULL,t3010.c301_vendor_id, p_vendor_id)
     AND REGEXP_LIKE(NVL(t3010.c205_part_number_id,'-999') , NVL(REGEXP_REPLACE(p_part_number_id,'[+]','\+'),NVL(t3010.c205_part_number_id,'-999')))
     AND t205.c202_project_id        = DECODE (p_project_id,NULL,t205.c202_project_id, p_project_id)
     AND t1910.c1910_division_id     = DECODE(p_division_id,0,t1910.c1910_division_id,p_division_id)
    AND NVL (t301.c301_purchasing_agent_id,0) = DECODE (p_purchse_agent,0, NVL(t301.c301_purchasing_agent_id, 0), p_purchse_agent)
     AND t202.c202_void_fl      IS NULL
     AND t1910.c1910_void_fl    IS NULL
     AND t3010.c3010_void_fl    IS NULL;
END gm_fch_ttp_by_vendor_moq_rpt;

/****************************************************************************************************
*Description  :  Procedure used to Fetch the vendor MOQ LOG vendor MOQ id 
* Author       :  Tamizhthangam Ramasamy
*****************************************************************************************************/

PROCEDURE gm_fch_ttp_by_vendor_moq_log(
   p_vendor_moq_id     IN  t3010a_vendor_moq_log.c3010_vendor_moq_id%type,
   p_out_moq_log_dtls OUT CLOB 
)
AS
 v_date_fmt VARCHAR2 (12);
BEGIN
  SELECT get_compdtfmt_frm_cntx ()INTO v_date_fmt FROM DUAL;
  
	SELECT JSON_ARRAYAGG( JSON_OBJECT ( 
         'partnum'     VALUE t3010a.c205_part_number_id,
         'moq'         VALUE t3010a.c3010_moq, 
         'updatedby'   VALUE NVL(t101.c101_user_f_name|| ' '|| t101.c101_user_l_name,' '), 
         'updateddate' VALUE NVL(TO_CHAR(t3010a.c3010_updated_date, v_date_fmt||' HH:MI:SS AM'),' '))
ORDER BY t3010a.c3010_updated_date DESC returning CLOB)
    INTO p_out_moq_log_dtls
    FROM t3010_vendor_moq t3010 ,t3010a_vendor_moq_log t3010a, t101_user t101
   WHERE t3010.c3010_vendor_moq_id   = t3010a.c3010_vendor_moq_id
     AND t3010a.c3010_updated_by    = t101.c101_user_id
     AND t3010a.c205_part_number_id = t3010.c205_part_number_id
     AND t3010a.c3010_vendor_moq_id = p_vendor_moq_id
     AND t3010.c3010_void_fl    IS NULL
     AND t3010a.c3010_void_fl    IS NULL;
	
END gm_fch_ttp_by_vendor_moq_log;

END gm_pkg_op_ttp_by_vendor_moq;
/