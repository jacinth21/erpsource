/* Formatted on 09/25/2019 10:18 (Formatter Plus v4.8.0) */
/* @ "\Database\Packages\Purchasing\gm_pkg_purchase_activity.bdy" */

CREATE OR REPLACE
PACKAGE BODY gm_pkg_purchase_activity
IS

    /***********************************************************
	Description : This is used to save PO activity log
	Auther: ppandiyan
	************************************************************/
	
PROCEDURE gm_sav_po_activity (
        p_ref_id     IN T4203_PURCHASE_ACTIVITY_LOG.C4203_REF_ID%TYPE,
        p_ref_type   IN T4203_PURCHASE_ACTIVITY_LOG.C901_REF_TYPE%TYPE,
        p_old_type   IN T4203_PURCHASE_ACTIVITY_LOG.C901_OLD_ACTION%TYPE,
        p_new_type   IN T4203_PURCHASE_ACTIVITY_LOG.C901_NEW_ACTION%TYPE,
        p_vendor_id  IN T4203_PURCHASE_ACTIVITY_LOG.C301_VENDOR_ID%TYPE,
        p_old_str    IN T4203_PURCHASE_ACTIVITY_LOG.C4203_OLD_STRING%TYPE,
        p_new_str    IN T4203_PURCHASE_ACTIVITY_LOG.C4203_NEW_STRING%TYPE,
        p_user_id    IN T4203_PURCHASE_ACTIVITY_LOG.C4203_CREATED_BY%TYPE,
        p_comp_id    IN T4203_PURCHASE_ACTIVITY_LOG.C1900_COMPANY_ID%TYPE
      
    )
AS
BEGIN


         INSERT
           INTO T4203_PURCHASE_ACTIVITY_LOG
            (
                C4203_ID, C4203_REF_ID, C901_REF_TYPE
              , C901_OLD_ACTION, C4203_OLD_STRING , C901_NEW_ACTION
              , C4203_NEW_STRING, C301_VENDOR_ID, C1900_COMPANY_ID
              , C4203_CREATED_DT, C4203_CREATED_BY
            )
            VALUES
            (
                S4203_PURCHASE_ACTIVITY_LOG.nextval, p_ref_id, p_ref_type
              , p_old_type, p_old_str, p_new_type
              , p_new_str, p_vendor_id, p_comp_id
              , CURRENT_DATE, p_user_id

            ) ;

END gm_sav_po_activity;
END gm_pkg_purchase_activity;
/