CREATE OR REPLACE
PACKAGE BODY GM_PKG_PC_VENDORPRICE_TXN
IS
PROCEDURE gm_sav_vendorprice (
        p_priceid   IN t405_vendor_pricing_details.C405_PRICING_ID%TYPE,
        p_vendor_id IN t405_vendor_pricing_details.C301_VENDOR_ID%TYPE,
        p_pnum      IN t405_vendor_pricing_details.C205_PART_NUMBER_ID%TYPE,
        p_price     IN t405_vendor_pricing_details.C405_COST_PRICE%TYPE,
        p_actfl     IN t405_vendor_pricing_details.C405_ACTIVE_FL%TYPE,
        p_vdate     IN t405_vendor_pricing_details.C405_PRICE_VALID_DATE%TYPE,
        p_cdate     IN t405_vendor_pricing_details.C405_CREATED_DATE%TYPE,
        p_createdby IN t405_vendor_pricing_details.C405_CREATED_BY%TYPE,
        p_updateddt IN t405_vendor_pricing_details.C405_LAST_UPDATED_DATE%TYPE,
        p_updatedby IN t405_vendor_pricing_details.C405_LAST_UPDATED_BY%TYPE,
        p_qtyquoted IN t405_vendor_pricing_details.C405_QTY_QUOTED%TYPE,
        p_dframe    IN t405_vendor_pricing_details.C405_DELIVERY_FRAME%TYPE,
        p_quoteid   IN t405_vendor_pricing_details.C405_VENDOR_QUOTE_ID%TYPE,
        p_uomid     IN t405_vendor_pricing_details.C901_UOM_ID%TYPE,
        p_uomqty    IN t405_vendor_pricing_details.C405_UOM_QTY%TYPE,
        p_lockfl    IN t405_vendor_pricing_details.C405_LOCK_FL%TYPE
      
    )
AS
BEGIN
     UPDATE t405_vendor_pricing_details
    SET C405_PRICING_ID         = p_priceid, C405_COST_PRICE = p_price, C405_ACTIVE_FL = p_actfl
      , C405_PRICE_VALID_DATE   = p_vdate, C405_CREATED_DATE = p_cdate, C405_CREATED_BY = p_createdby
      , C405_LAST_UPDATED_DATE  = sysdate, C405_LAST_UPDATED_BY = p_updatedby, C405_QTY_QUOTED = p_qtyquoted
      , C405_DELIVERY_FRAME     = p_dframe, C405_VENDOR_QUOTE_ID = p_quoteid, C901_UOM_ID = p_uomid
      , C405_UOM_QTY            = p_uomqty, C405_LOCK_FL = p_lockfl
      WHERE C405_ACTIVE_FL     IS NOT NULL
        AND c205_part_number_id = p_pnum
        AND c301_vendor_id      = p_vendor_id
        AND c405_void_fl IS NULL;
    IF (SQL%ROWCOUNT            = 0) THEN
         INSERT
           INTO t405_vendor_pricing_details
            (
                C405_PRICING_ID, C301_VENDOR_ID, C205_PART_NUMBER_ID
              , C405_COST_PRICE, C405_ACTIVE_FL, C405_PRICE_VALID_DATE
              , C405_CREATED_DATE, C405_CREATED_BY, C405_QTY_QUOTED
              , C405_DELIVERY_FRAME, C405_VENDOR_QUOTE_ID
              , C901_UOM_ID, C405_UOM_QTY
              , C405_LOCK_FL
            )
            VALUES
            (
                s405_vend_price_id.nextval, p_vendor_id, p_pnum
              , p_price, p_actfl, p_vdate
              , sysdate, p_createdby, p_qtyquoted
              , p_dframe, p_quoteid, p_uomid
              , p_uomqty, p_lockfl
            ) ;
    END IF;
END gm_sav_vendorprice;
	
	/***********************************************************
	 Description : This Fucntion is to get vendor price.
	 Auther: Velu
	************************************************************/
	FUNCTION get_vendor_price (
		 p_pnum   IN t205_part_number.c205_part_number_id %TYPE,
		 p_vendor_id   IN NUMBER
	)
	RETURN NUMBER
	IS
		v_price    	 NUMBER(15,2);
		v_rule_value VARCHAR(20);
	BEGIN
		
		SELECT get_rule_value(p_vendor_id,'NEED_COSTING') INTO v_rule_value FROM DUAL; -- For Chennai, Delhi
		
		IF v_rule_value = 'YES' THEN
			 SELECT GET_AC_COGS_VALUE(p_pnum, '4900') INTO v_price  FROM DUAL; -- 4900 denotes FG Inventory COGS Type.
		END IF;
		BEGIN
		IF v_price  IS NULL THEN
			SELECT c405_cost_price INTO v_price FROM  t405_vendor_pricing_details
			WHERE c205_part_number_id = p_pnum  AND c301_vendor_id = p_vendor_id
			AND c405_active_fl = 'Y' AND c405_void_fl IS NULL;
		END IF;
		EXCEPTION
		WHEN NO_DATA_FOUND THEN
			RETURN '';
		END;
		RETURN v_price;
	END get_vendor_price;
	
END GM_PKG_PC_VENDORPRICE_TXN;
/
