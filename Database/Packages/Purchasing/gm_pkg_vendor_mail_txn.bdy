CREATE OR REPLACE PACKAGE BODY GM_PKG_VENDOR_MAIL_TXN  
IS
       
    /*********************************************************************
	Description : This procedure is used to save Purchase order mail
	Author: Parthiban 
	*********************************************************************/
	PROCEDURE gm_sav_po_email_dtl (
	p_ref_id        IN clob,
	p_ref_type  	IN VARCHAR2,
    p_ref_act     	IN VARCHAR2,
    p_userid    	IN T401_PURCHASE_ORDER.C401_CREATED_BY%TYPE
   
    )
	AS
	v_ven_fl     VARCHAR2(5);
	v_ins_fl 	 VARCHAR2(5); 
	v_pub_fl     VARCHAR2(5);
	
	 CURSOR multi_po_curr
      IS
          SELECT C301_VENDOR_ID VENID,
          		 C401_PURCHASE_ORD_ID PID
            FROM t401_purchase_order
           WHERE C401_PURCHASE_ORD_ID IN (SELECT token FROM v_clob_list WHERE token IS NOT NULL);	
				
			BEGIN
				my_context.set_my_cloblist (p_ref_id||',');
					
			FOR multi_po IN multi_po_curr
		       LOOP
		   SELECT gm_pkg_vendor_mail_fch.get_vendor_fl( multi_po.venid),gm_pkg_vendor_mail_fch.get_vendor_instance_fl(multi_po.venid,p_ref_act), gm_pkg_vendor_mail_fch.get_published_fl(multi_po.PID)
		     INTO v_ven_fl,v_ins_fl,v_pub_fl
		     FROM dual;
		     
		     IF(v_ven_fl = 'Y' AND v_pub_fl = 'Y') THEN 
		     
			INSERT INTO T4206_VENDOR_EMAIL
								  (
								    c4206_vendor_email_id,
								  	c4206_ref_id,
								    c901_ref_type,
								    c901_type,
								    c4206_email_status,
								    c301_vendor_id,
								    c4206_void_fl,
								    c4206_created_dt,
								    c4206_created_by )
									  VALUES
									  (
									    S4206_VENDOR_EMAIL.nextval,
									  	multi_po.pid,
									    p_ref_type,
									    p_ref_act,
									    v_ins_fl,
									    multi_po.venid,
									    NULL,
									    CURRENT_DATE,
										p_userid );
			END IF;
		  	END LOOP;
		END gm_sav_po_email_dtl;
		
	/*********************************************************************
	Description : This procedure is used to save Work order mail
	Author: Parthiban 
	*********************************************************************/
		
	PROCEDURE gm_sav_wo_email_dtl (
	p_ref_id        IN CLOB,
	p_ref_type  	IN VARCHAR2,
	p_ref_act     	IN VARCHAR2,
	p_userid   		IN T402_WORK_ORDER.C402_CREATED_BY%TYPE
	)
			AS
			v_ven_fl  	 VARCHAR2(5);
			v_ins_fl 	 VARCHAR2(5); 
		    v_pub_fl     VARCHAR2(5);
			CURSOR multi_wo_cur
		      IS
		          SELECT c301_vendor_id VENID,c402_work_order_id WID,c401_purchase_ord_id pid
		           FROM t402_work_order
		          WHERE c402_work_order_id IN (SELECT token FROM v_clob_list WHERE token IS NOT NULL);	
		          
			BEGIN
				my_context.set_my_cloblist (p_ref_id||',');
			FOR multi_wo IN multi_wo_cur
		       LOOP		 	
		       	 SELECT gm_pkg_vendor_mail_fch.get_vendor_fl( multi_wo.venid),gm_pkg_vendor_mail_fch.get_vendor_instance_fl(multi_wo.venid,p_ref_act),gm_pkg_vendor_mail_fch.get_published_fl(multi_wo.pid)
		       	   INTO v_ven_fl,v_ins_fl,v_pub_fl
		       	   FROM dual;
		       	   
		       	   IF(v_ven_fl = 'Y' AND v_pub_fl = 'Y') THEN 
							INSERT INTO T4206_VENDOR_EMAIL
								  ( c4206_vendor_email_id,
								  	c4206_ref_id,
								    c901_ref_type,
								    c901_type,
								    c4206_email_status,
								    c301_vendor_id,
								    c4206_void_fl,
								    c4206_created_dt,
								    c4206_created_by)
							 VALUES (
								    S4206_VENDOR_EMAIL.nextval,
									multi_wo.WID,
								    p_ref_type,
								    p_ref_act,
								    v_ins_fl,
								    multi_wo.VENID,
								    NULL,
								    CURRENT_DATE,
								    p_userid );
								    
		  END IF;					    
		  END LOOP;
		  END gm_sav_wo_email_dtl;
		  
		  
	/*********************************************************************
	Description : This procedure is used to save mail status
	Author: Parthiban 
	*********************************************************************/ 
		
		 PROCEDURE gm_sav_email_status (
			p_ref_id         IN varchar2,
		    p_userid         IN T401_PURCHASE_ORDER.C401_CREATED_BY%TYPE
		    )
			AS
			
			BEGIN
				 my_context.set_my_inlist_ctx(p_ref_id);
		 		UPDATE T4206_VENDOR_EMAIL 
		 		   SET c4206_email_status = '2',
		 		   	   c4206_updated_by =  p_userid,
		 		   	   c4206_updated_dt = current_date
		 		 WHERE c4206_email_status = '0' 
		 		   AND c4206_ref_id in(SELECT token FROM v_in_list);
		 		
		END gm_sav_email_status;
/***************************************************************
 * Description:This Procedure used to save dhr email details
 * Author:
 ****************************************************************/
PROCEDURE gm_sav_dhr_email_dtls(
    p_ref_id   IN CLOB,
    p_ref_type IN VARCHAR2,
    p_ref_act  IN VARCHAR2,
    p_userid   IN T402_WORK_ORDER.C402_CREATED_BY%TYPE )
AS
	v_ven_fl  	 VARCHAR2(5);
	v_ins_fl 	 VARCHAR2(5);
	v_pub_fl     VARCHAR2(5);
--Get vendor Id
  CURSOR multi_dhr_curr
  IS
    SELECT C301_VENDOR_ID v_vendor_id,
      c408_dhr_id v_dhr_id, c401_purchase_ord_id pid
    FROM t408_dhr
    WHERE c408_dhr_id IN
      (SELECT token FROM v_clob_list WHERE token IS NOT NULL
      );
BEGIN
  my_context.set_my_cloblist (p_ref_id||',');
  --loop and insert int0 t4206
  FOR multi_dhr IN multi_dhr_curr
  LOOP
  SELECT gm_pkg_vendor_mail_fch.GET_VENDOR_FL(multi_dhr.v_vendor_id),gm_pkg_vendor_mail_fch.GET_VENDOR_INSTANCE_FL(multi_dhr.v_vendor_id,p_ref_act), gm_pkg_vendor_mail_fch.get_published_fl(multi_dhr.pid)  
    into v_ven_fl,v_ins_fl,v_pub_fl from dual;
    
  IF(v_ven_fl='Y' AND v_pub_fl = 'Y') THEN 
    INSERT
    INTO T4206_VENDOR_EMAIL
      (
        C4206_VENDOR_EMAIL_ID,
        C4206_REF_ID,
        C901_REF_TYPE,
        C901_TYPE,
        C4206_EMAIL_STATUS,
        C301_VENDOR_ID,
        C4206_VOID_FL,
        C4206_CREATED_DT,
        C4206_CREATED_BY
      )
      VALUES
      (
        S4206_VENDOR_EMAIL.nextval,
        multi_dhr.v_dhr_id,
        p_ref_type,
        p_ref_act,
        v_ins_fl,
        multi_dhr.v_vendor_id,
        NULL,
        CURRENT_DATE,
        p_userid
      );
      END IF;
  END LOOP;
END gm_sav_dhr_email_dtls;

    
    /*********************************************************************
	Description : This procedure is used to save invoice mail details
	Author: Agilan 
	*********************************************************************/
	PROCEDURE gm_sav_invoice_email_dtl ( 
	p_ref_id        IN clob,
	p_ref_type  	IN VARCHAR2,
    p_ref_act     	IN VARCHAR2,
    p_userid    	IN T401_PURCHASE_ORDER.C401_CREATED_BY%TYPE
   
    )
	AS
	v_ven_fl     VARCHAR2(5);
	v_ins_fl 	 VARCHAR2(5); 
	v_pub_fl     VARCHAR2(5);
	 CURSOR multi_inv_cur
      IS
          SELECT C301_VENDOR_ID VENID,
          		 C401_PURCHASE_ORD_ID PID,
          		 C4204_VENDOR_INVOICE_ID INVID
            FROM T4204_VENDOR_INVOICE
           WHERE C4204_VENDOR_INVOICE_ID IN (SELECT token FROM v_clob_list WHERE token IS NOT NULL);	
				
			BEGIN
				my_context.set_my_cloblist (p_ref_id||',');
					
			FOR multi_inv IN multi_inv_cur
		       LOOP
		   SELECT gm_pkg_vendor_mail_fch.get_vendor_fl( multi_inv.venid),gm_pkg_vendor_mail_fch.get_vendor_instance_fl(multi_inv.venid,p_ref_act), gm_pkg_vendor_mail_fch.get_published_fl(multi_inv.pid)
		     INTO v_ven_fl,v_ins_fl,v_pub_fl
		     FROM dual;
		     
		     IF(v_ven_fl = 'Y' AND v_pub_fl = 'Y') THEN 
		     
			INSERT INTO T4206_VENDOR_EMAIL
								  (
								    c4206_vendor_email_id,
								  	c4206_ref_id,
								    c901_ref_type,
								    c901_type,
								    c4206_email_status,
								    c301_vendor_id,
								    c4206_void_fl,
								    c4206_created_dt,
								    c4206_created_by )
									  VALUES
									  (
									    S4206_VENDOR_EMAIL.nextval,
									  	multi_inv.INVID,
									    p_ref_type,
									    p_ref_act,
									    v_ins_fl,
									    multi_inv.VENID,
									    NULL,
									    CURRENT_DATE,
										p_userid );
			END IF;
		  	END LOOP;
		END gm_sav_invoice_email_dtl;
END gm_pkg_vendor_mail_txn;
/