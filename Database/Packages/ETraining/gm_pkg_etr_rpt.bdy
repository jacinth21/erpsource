/* Formatted on 2009/05/08 10:07 (Formatter Plus v4.8.0) */
--@"C:\database\Packages\etraining\gm_pkg_etr_rpt.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_etr_rpt
IS
	PROCEDURE gm_fch_modlookup_detail (
		p_moduleid		IN		 t6302_module.c6302_module_id%TYPE
	  , p_modulenm		IN		 t6302_module.c6302_module_nm%TYPE
	  , p_moduleowner	IN		 t6302_module.c101_module_owner%TYPE
	  , p_courseid		IN		 t6301_course.c6301_course_id%TYPE
	  , p_outmodtypes	OUT 	 TYPES.cursor_type
	)
	AS
	/*******************************************************
	* Description : Procedure to fetch module lookup
	* Author		: Ritesh Shah
	*******************************************************/
	BEGIN
		OPEN p_outmodtypes
		 FOR
			 SELECT t6302.c6302_module_id ID, t6302.c6302_module_nm modulenm, t6302.c6301_course_id courseid
				  , t6301.c6301_course_nm coursenm, get_user_name (t6302.c101_module_owner) moduleowner
			   FROM t6302_module t6302, t6301_course t6301
			  WHERE t6302.c6302_module_nm = NVL (p_modulenm, t6302.c6302_module_nm)
				AND t6302.c6302_module_id = NVL (p_moduleid, t6302.c6302_module_id)
				AND t6302.c101_module_owner = DECODE (p_moduleowner, '0', t6302.c101_module_owner, p_moduleowner)
				AND t6302.c6301_course_id = DECODE (p_courseid, '0', t6302.c6301_course_id, p_courseid)
				AND t6302.c6301_course_id = t6301.c6301_course_id
				AND t6302.c6302_void_fl IS NULL;
	END gm_fch_modlookup_detail;

	PROCEDURE gm_fch_dashboard_detail (
		p_user				IN		 t6305_session_user_map.c101_user_id%TYPE
	  , p_outtrainigtypes	OUT 	 TYPES.cursor_type
	)
	AS
	/*******************************************************
	* Description : Procedure to fetch Open training detail
	* Author		: Ritesh Shah
	*******************************************************/
	BEGIN
		OPEN p_outtrainigtypes
		 FOR
			 SELECT t6303.c6303_session_desc sessiondesc, t6303.c6303_session_id sessionid
				  , get_user_name (t6303.c101_session_owner) trainer
				  , TO_CHAR (t6303.c6303_start_date, 'MM/DD/YYYY') startdate
				  , TO_CHAR (t6303.c6303_end_date, 'MM/DD/YYYY') enddate
			   FROM t6303_session t6303, t6305_session_user_map t6305
			  WHERE t6305.c101_user_id = p_user
				AND t6305.c6303_session_id = t6303.c6303_session_id
				AND t6305.c901_status = 60330	--added later for active user...
				AND c6305_void_fl IS NULL
				AND c6305_completed_date IS NULL
				AND t6303.c6303_void_fl IS NULL;
	END gm_fch_dashboard_detail;

	/*******************************************************
	* Description : Procedure to fetch courses detail report
	* Author		: Ritesh Shah/Angela
	*******************************************************/
	PROCEDURE gm_fch_courserpt_detail (
		p_coursedeptid	 IN 	  t6301_course.c901_primary_dept_id%TYPE
	  , p_owner 		 IN 	  t6301_course.c101_course_owner%TYPE
	  , p_coursestatus	 IN 	  t6301_course.c901_status%TYPE
	  , p_outcourserpt	 OUT	  TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_outcourserpt
		 FOR
			 SELECT DISTINCT t6301.c6301_course_id courseid, t6301.c6301_course_id programid
						   , t6301.c6301_course_nm coursename, t6301.c901_primary_dept_id coursedeptid
						   , get_code_name_alt (t6301.c901_primary_dept_id) coursedeptname
						   , get_user_name (t6301.c101_course_owner) ownername, t6301.c101_course_owner ownerid
						   , get_code_name (t6301.c901_status) statusname, t6301.c901_status statusid
						FROM t6301_course t6301, t101_user t101
					   WHERE t101.c101_dept_id = DECODE (p_coursedeptid, '0', t101.c101_dept_id, p_coursedeptid)
						 AND t6301.c901_status = DECODE (p_coursestatus, '0', t6301.c901_status, p_coursestatus)
						 AND t6301.c101_course_owner = DECODE (p_owner, '0', t6301.c101_course_owner, p_owner)
						 AND t6301.c101_course_owner = t101.c101_user_id
						 AND t6301.c6301_void_fl IS NULL;
	END gm_fch_courserpt_detail;

	/*******************************************************
	* Description : Procedure to fetch module detail report
	* Author		: Ritesh Shah/Angela
	*******************************************************/
	PROCEDURE gm_fch_modulerpt_detail (
		p_courseid		 IN 	  t6301_course.c6301_course_id%TYPE
	  , p_moduledeptid	 IN 	  t6306_course_dept_map.c901_dept_id%TYPE
	  , p_moduleowner	 IN 	  t6302_module.c101_module_owner%TYPE
	  , p_modulestatus	 IN 	  t6302_module.c901_status%TYPE
	  , p_outmodulerpt	 OUT	  TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_outmodulerpt
		 FOR
			 SELECT t6302.c6302_module_id moduleid, t6302.c6302_module_nm modulename
				  , get_user_name (t6302.c101_module_owner) ownername, t6302.c101_module_owner moduleownerid
				  , t101.c101_dept_id moduledeptid, get_code_name_alt (t6301.c901_primary_dept_id) moduledeptname
				  , t6301.c6301_course_nm coursename, get_code_name (t6302.c901_status) modulestatusname
				  , t6302.c901_status modulestatusid
			   FROM t6302_module t6302, t101_user t101, t6301_course t6301
			  WHERE t101.c101_dept_id = DECODE (p_moduledeptid, '0', t101.c101_dept_id, p_moduledeptid)
				AND t6301.c6301_course_id = t6302.c6301_course_id
				AND t6301.c6301_course_id = DECODE (p_courseid, '0', t6301.c6301_course_id, p_courseid)
				AND t6302.c101_module_owner = DECODE (p_moduleowner, '0', t6302.c101_module_owner, p_moduleowner)
				AND t6302.c901_status = DECODE (p_modulestatus, '0', t6302.c901_status, p_modulestatus)
				AND t6302.c101_module_owner = t101.c101_user_id
				AND t6302.c6302_void_fl IS NULL;
	END gm_fch_modulerpt_detail;

	/*******************************************************
	* Description : Procedure to fetch User detail report
	* Author		: Ritesh Shah
	*******************************************************/
	PROCEDURE gm_fch_userrpt_detail (
		p_traineeid    IN		t6305_session_user_map.c101_user_id%TYPE
	  , p_moduleid	   IN		t6302_module.c6302_module_id%TYPE
	  , p_courseid	   IN		t6301_course.c6301_course_id%TYPE
	  , p_startdt	   IN		VARCHAR2
	  , p_enddt 	   IN		VARCHAR2
	  , p_status	   IN		t6305_session_user_map.c901_status%TYPE
	  , p_userdeptid   IN		t6301_course.c901_primary_dept_id%TYPE
	  , p_reason	   IN		t6304_session_module_map.c901_reason%TYPE
	  , p_sesnid	   IN		t6303_session.c6303_session_id%TYPE
	  , p_outuserrpt   OUT		TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_outuserrpt
		 FOR
			 SELECT DISTINCT t6305.c101_user_id rptid, get_user_name (t6305.c101_user_id) usernm
						   , t6302.c6302_module_nm modulenm, get_user_name (t6304.c101_trainer_id) trainer
						   , t6301.c6301_course_nm coursenm, TO_CHAR (t6303.c6303_start_date, 'MM/DD/YYYY') startdt
						   , TO_CHAR (t6303.c6303_end_date, 'MM/DD/YYYY') enddt
						   , get_code_name (t6305.c901_status) status, get_code_name (t6304.c901_reason) reason
						   , t6303.c101_session_owner sessionowner, t6303.c6303_max_user maxuser
						   , t6303.c6303_session_id sesnid, get_code_name_alt (t101.c901_dept_id) trneedeptname
						   , TO_CHAR (t101.c101_hire_dt, 'mm/dd/yyyy') trneestartdate
						FROM t6305_session_user_map t6305
						   , t6304_session_module_map t6304
						   , t6302_module t6302
						   , t6301_course t6301
						   , t6303_session t6303
						   , t6307_session_user_detail t6307
						   , t101_user t101
					   WHERE t6305.c6305_void_fl IS NULL
						 AND t6305.c901_status = DECODE (p_status, '0', t6305.c901_status, p_status)
						 AND t6304.c6303_session_id = t6305.c6303_session_id
						 AND t6304.c6302_module_id = t6302.c6302_module_id
						 AND t6302.c6301_course_id = t6301.c6301_course_id
						 AND t6305.c6303_session_id = t6303.c6303_session_id
						 AND t6304.c6304_sess_mod_seq = t6307.c6304_sess_mod_seq
						 AND t6303.c6303_start_date = NVL (TO_DATE (p_startdt, 'MM/DD/YYYY'), t6303.c6303_start_date)
						 AND t6303.c6303_end_date = NVL (TO_DATE (p_enddt, 'MM/DD/YYYY'), t6303.c6303_end_date)
						 AND t6304.c901_reason = DECODE (p_reason, '0', t6304.c901_reason, p_reason)
						 AND t6301.c901_primary_dept_id =
													DECODE (p_userdeptid
														  , '0', t6301.c901_primary_dept_id
														  , p_userdeptid
														   )
						 --AND t6304.c101_trainer_id = DECODE (p_trainerid, '0', t6304.c101_trainer_id, p_trainerid)
						 AND t6305.c101_user_id = DECODE (p_traineeid, '0', t6305.c101_user_id, p_traineeid)
						 AND t6302.c6302_module_id = DECODE (p_moduleid, '0', t6302.c6302_module_id, p_moduleid)
						 AND t6301.c6301_course_id = DECODE (p_courseid, '0', t6301.c6301_course_id, p_courseid)
						 AND t6303.c6303_session_id = NVL (p_sesnid, t6303.c6303_session_id)
						 AND t6305.c101_user_id = t101.c101_user_id;
	END gm_fch_userrpt_detail;

			/*******************************************************
	* Description : Procedure to fetch Training Record
	* Author		: Angela
	*******************************************************/
	PROCEDURE gm_fch_training_records (
		p_sessionid 		  IN	   t6303_session.c6303_session_id%TYPE
	  , p_outtrainigrecords   OUT	   TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_outtrainigrecords
		 FOR
			 SELECT get_code_name_alt (t101.c901_dept_id) trdeptname, get_user_name (t6305.c101_user_id) trusername
			   FROM t6303_session t6303, t6305_session_user_map t6305, t101_user t101
			  WHERE t6303.c6303_session_id = DECODE (p_sessionid, '0', t6303.c6303_session_id, p_sessionid)
				AND t6303.c6303_session_id = t6305.c6303_session_id
				AND t6305.c101_user_id = t101.c101_user_id;
	END gm_fch_training_records;

		/*******************************************************
	* Description : Procedure to fetch Training Summary
	* Author		: Angela
	*******************************************************/
	PROCEDURE gm_fch_training_summary (
		p_traineeid 		  IN	   t6304_session_module_map.c101_trainer_id%TYPE
	  , p_outtrainigsummary   OUT	   TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_outtrainigsummary
		 FOR
			 SELECT DISTINCT t6302.c6302_module_nm coursetitle
						   , DECODE (c901_location_type
								   , 60310, get_code_name (c6303_location_value)
								   , c6303_location_value
									) LOCATION
						   , TO_CHAR (t6303.c6303_end_date, 'mm/dd/yyyy') projcomdate
						   , TO_CHAR (t6305.c6305_completed_date, 'mm/dd/yyyy') actcomdate
						FROM t6303_session t6303
						   , t6302_module t6302
						   , t6304_session_module_map t6304
						   , t6305_session_user_map t6305
					   WHERE t6303.c6303_session_id = t6305.c6303_session_id
						 AND t6303.c6303_session_id = t6304.c6303_session_id
						 AND t6304.c6302_module_id = t6302.c6302_module_id
						 AND t6305.c101_user_id = DECODE (p_traineeid, '0', t6305.c101_user_id, p_traineeid)
						 AND t6305.c6305_completed_date IS NOT NULL;
	END gm_fch_training_summary;
END gm_pkg_etr_rpt;
/
