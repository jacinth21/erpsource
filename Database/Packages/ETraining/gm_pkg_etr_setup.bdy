/* Formatted on 2009/05/11 17:41 (Formatter Plus v4.8.0) */

--@"C:\database\Packages\etraining\gm_pkg_etr_setup.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_etr_setup
IS
	PROCEDURE gm_sav_sesn_detail (
		p_sessionid 	   IN		t6303_session.c6303_session_id%TYPE
	  , p_sessiondesc	   IN		t6303_session.c6303_session_desc%TYPE
	  , p_sessionstartdt   IN		VARCHAR2
	  , p_sessionenddt	   IN		VARCHAR2
	  , p_owner 		   IN		t6303_session.c101_session_owner%TYPE
	  , p_sessionlcntype   IN		t6303_session.c901_location_type%TYPE
	  , p_sessionlcn	   IN		t6303_session.c6303_location_value%TYPE
	  , p_duration		   IN		t6303_session.c6303_duration_hrs%TYPE
	  , p_minuser		   IN		t6303_session.c6303_min_user%TYPE
	  , p_maxuser		   IN		t6303_session.c6303_max_user%TYPE
	  , p_status		   IN		t6303_session.c901_status%TYPE
	  , p_user_id		   IN		t6303_session.c6303_last_updated_by%TYPE
	  , p_outsessionid	   OUT		VARCHAR2
	)
	AS
		/*******************************************************
		* Description : Procedure to save session records
		* Author		: Ritesh Shah
		*******************************************************/
		v_void_fl	   VARCHAR2 (1);
		v_seq_no	   VARCHAR2 (20);
	BEGIN
		p_outsessionid := p_sessionid;

		UPDATE t6303_session
		   SET c6303_session_desc = p_sessiondesc
			 , c6303_start_date = TO_DATE (p_sessionstartdt, 'MM/DD/YYYY')
			 , c6303_end_date = TO_DATE (p_sessionenddt, 'MM/DD/YYYY')
			 , c101_session_owner = p_owner
			 , c901_location_type = p_sessionlcntype
			 , c6303_location_value = p_sessionlcn
			 , c6303_duration_hrs = p_duration
			 , c6303_min_user = p_minuser
			 , c6303_max_user = p_maxuser
			 , c901_status = p_status
			 , c6303_last_updated_date = SYSDATE
			 , c6303_last_updated_by = p_user_id
		 WHERE c6303_session_id = p_sessionid;

		IF (SQL%ROWCOUNT = 0)
		THEN
			SELECT 'GM-TS-' || s6303_session_id.NEXTVAL
			  INTO v_seq_no
			  FROM DUAL;

			INSERT INTO t6303_session
						(c6303_session_id, c6303_session_desc, c6303_start_date
					   , c6303_end_date, c101_session_owner, c901_location_type, c6303_location_value
					   , c6303_duration_hrs, c6303_min_user, c6303_max_user, c901_status, c6303_created_date
					   , c6303_created_by
						)
				 VALUES (v_seq_no, p_sessiondesc, TO_DATE (p_sessionstartdt, 'MM/DD/YYYY')
					   , TO_DATE (p_sessionenddt, 'MM/DD/YYYY'), p_owner, p_sessionlcntype, p_sessionlcn
					   , p_duration, p_minuser, p_maxuser, 60302, SYSDATE
					   , p_user_id
						);

			p_outsessionid := v_seq_no;
		END IF;
	END gm_sav_sesn_detail;

	PROCEDURE gm_fch_sesn_detail (
		p_sessionid 		IN		 t6303_session.c6303_session_id%TYPE
	  , p_outsessdtltypes	OUT 	 TYPES.cursor_type
	)
	AS
	/*******************************************************
	* Description : Procedure to fetch session details
	* Author		: Ritesh Shah
	*******************************************************/
	BEGIN
		OPEN p_outsessdtltypes
		 FOR
			 SELECT c6303_session_id sessionid, c6303_session_desc sessiondesc
				  , TO_CHAR (c6303_start_date, 'MM/DD/YYYY') sessionstartdt
				  , TO_CHAR (c6303_end_date, 'MM/DD/YYYY') sessionenddt, c101_session_owner sessionowner
				  , get_user_name (c101_session_owner) sessionownernm, c901_location_type sessionlcntype
				  , get_code_name (c901_location_type) sessionlcntypenm
				  , DECODE (c901_location_type
						  , 60310, get_code_name (c6303_location_value)   --60310 - for location field (Classroom)
						  , c6303_location_value
						   ) sessionlcn
				  , c6303_location_value sesnlcnval, c6303_duration_hrs DURATION, c6303_min_user minuser
				  , c6303_max_user maxuser, c901_status status
			   FROM t6303_session
			  WHERE c6303_session_id = p_sessionid AND c6303_void_fl IS NULL;
	END gm_fch_sesn_detail;

	PROCEDURE gm_fch_modlist (
		p_outmodlist   OUT	 TYPES.cursor_type
	)
	AS
	/*******************************************************
	* Description : Procedure to fetch module list
	* Author		: Ritesh Shah
	*******************************************************/
	BEGIN
		OPEN p_outmodlist
		 FOR
			 SELECT c6302_module_id codeid, c6302_module_nm codenm
			   FROM t6302_module
			  WHERE c6302_void_fl IS NULL;
	END gm_fch_modlist;

	PROCEDURE gm_fch_courselist (
		p_outcourselist   OUT	TYPES.cursor_type
	)
	AS
	/*******************************************************
	* Description : Procedure to fetch course list
	* Author		: Ritesh Shah
	*******************************************************/
	BEGIN
		OPEN p_outcourselist
		 FOR
			 SELECT c6301_course_id codeid, c6301_course_nm codenm
			   FROM t6301_course
			  WHERE c6301_void_fl IS NULL;
	END gm_fch_courselist;

	/*******************************************************
	* Description : Procedure to fetch course detail
	* Author		: Ritesh Shah/Angela
	*******************************************************/
	PROCEDURE gm_fch_course_detail (
		p_courseid	 IN 	  t6301_course.c6301_course_id%TYPE
	  , p_outdata	 OUT	  TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_outdata
		 FOR
			 SELECT c6301_course_id courseid, c6301_course_nm coursename, c6301_course_desc description
				  , c901_primary_dept_id deptid, get_user_name (c101_course_owner) ownername
				  , c101_course_owner courseowner, get_code_name (c901_status) statusname, c901_status coursestatus
			   FROM t6301_course
			  WHERE c6301_course_id = p_courseid AND c6301_void_fl IS NULL;
	-- void check has to be there.
	END gm_fch_course_detail;

	/*******************************************************
	* Description : Procedure to fetch module detail
	* Author		: Ritesh Shah/Angela
	*******************************************************/
	PROCEDURE gm_fch_module_detail (
		p_moduleid			IN		 t6302_module.c6302_module_id%TYPE
	  , p_outmoduledetail	OUT 	 TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_outmoduledetail
		 FOR
			 SELECT t6302.c6302_module_id moduleid, t6302.c6302_module_nm modulename
				  , t6302.c6302_module_desc description
				  , (SELECT t6301.c6301_course_nm
					   FROM t6301_course t6301
					  WHERE t6302.c6301_course_id = t6301.c6301_course_id) coursename, t6302.c6301_course_id courseid
				  , get_code_name (t6302.c901_status) statusname, t6302.c901_status modulestatus
				  , get_user_name (t6302.c101_module_owner) ownername, t6302.c101_module_owner moduleowner
			   FROM t6302_module t6302
			  WHERE t6302.c6302_module_id = p_moduleid AND t6302.c6302_void_fl IS NULL;
	END gm_fch_module_detail;

	/*******************************************************
	* Description : Procedure to save module detail
	* Author		: Ritesh Shah/Angela
	*******************************************************/
	PROCEDURE gm_sav_module_detail (
		p_moduleid		IN OUT	 t6302_module.c6302_module_id%TYPE
	  , p_modulenm		IN		 t6302_module.c6302_module_nm%TYPE
	  , p_description	IN		 t6302_module.c6302_module_desc%TYPE
	  , p_courseid		IN		 t6302_module.c6301_course_id%TYPE
	  , p_status		IN		 t6302_module.c901_status%TYPE
	  , p_user			IN		 t6302_module.c6302_created_by%TYPE
	  , p_owner 		IN		 t6302_module.c101_module_owner%TYPE
	)
	AS
		v_new_moduleid t6302_module.c6302_module_id%TYPE;
		v_statuscnt    NUMBER;
	BEGIN
		--Check for status updation...
		SELECT COUNT (*)
		  INTO v_statuscnt
		  FROM t6303_session t6303, t6304_session_module_map t6304, t6302_module t6302
		 WHERE t6303.c6303_session_id = t6304.c6303_session_id
		   AND t6304.c6302_module_id = t6302.c6302_module_id
		   AND t6302.c6302_module_id = p_moduleid
		   AND t6303.c901_status IN (60302, 60300);   --(session status "initiated and inprogress"

		IF (v_statuscnt > 0 AND p_status = 60291)
		THEN
			raise_application_error (-20138, p_moduleid);
		END IF;

		-------
		UPDATE t6302_module
		   SET c6302_module_nm = p_modulenm
			 , c6302_module_desc = p_description
			 , c101_module_owner = p_owner
			 , c901_status = p_status
			 , c6301_course_id = p_courseid
			 , c6302_last_updated_by = p_user
			 , c6302_last_updated_date = SYSDATE
		 WHERE c6302_module_id = p_moduleid AND c6302_void_fl IS NULL;

		IF (SQL%ROWCOUNT = 0)
		THEN
			SELECT s6302_module_id.NEXTVAL
			  INTO v_new_moduleid
			  FROM DUAL;

			p_moduleid	:= 'GM-MD-' || v_new_moduleid;

			INSERT INTO t6302_module
						(c6302_module_id, c6302_module_nm, c6302_module_desc, c6301_course_id, c901_status
					   , c6302_created_by, c101_module_owner, c6302_created_date
						)
				 VALUES (p_moduleid, p_modulenm, p_description, p_courseid, 60290	-- For Active Status
					   , p_user, p_owner, SYSDATE
						);
		--	Throw an exception if any unwanted condition happened.
		-- Exception when ��. Then raise_application_error(-20047, ' ');
		END IF;
	END gm_sav_module_detail;

	/*******************************************************
	* Description : Procedure to save course detail
	* Author		: Ritesh Shah/Angela
	*******************************************************/
	PROCEDURE gm_sav_course_detail (
		p_courseid		 IN OUT   t6301_course.c6301_course_id%TYPE
	  , p_coursenm		 IN 	  t6301_course.c6301_course_nm%TYPE
	  , p_description	 IN 	  t6301_course.c6301_course_desc%TYPE
	  , p_user			 IN 	  t6301_course.c6301_created_by%TYPE
	  , p_deptid		 IN 	  t6301_course.c901_primary_dept_id%TYPE
	  , p_owner 		 IN 	  t6301_course.c101_course_owner%TYPE
	  , p_coursestatus	 IN 	  t6301_course.c901_status%TYPE
	)
	AS
		v_new_courseid t6301_course.c6301_course_id%TYPE;
		v_statuscnt    NUMBER;
	BEGIN
		--Check for status updation...
		SELECT COUNT (*)
		  INTO v_statuscnt
		  FROM t6303_session t6303, t6304_session_module_map t6304, t6302_module t6302, t6301_course t6301
		 WHERE t6303.c6303_session_id = t6304.c6303_session_id
		   AND t6304.c6302_module_id = t6302.c6302_module_id
		   AND t6302.c6301_course_id = t6301.c6301_course_id
		   AND t6301.c6301_course_id = p_courseid
		   AND t6303.c901_status IN (60302, 60300);   --(session status "initiated and inprogress"

		IF (v_statuscnt > 0 AND p_coursestatus = 60281)
		THEN
			raise_application_error (-20138, p_courseid);
		END IF;

		-------
		UPDATE t6301_course
		   SET c6301_course_nm = p_coursenm
			 , c6301_course_desc = p_description
			 , c101_course_owner = p_owner
			 , c901_status = p_coursestatus
			 , c6301_last_updated_date = SYSDATE
			 , c6301_last_updated_by = p_user
		 WHERE c6301_course_id = p_courseid;   -- remove the void fl condition

		IF (SQL%ROWCOUNT = 0)
		THEN
			SELECT s6301_course_id.NEXTVAL
			  INTO v_new_courseid
			  FROM DUAL;

			p_courseid	:= 'GM-PM-' || v_new_courseid;

			INSERT INTO t6301_course
						(c6301_course_id, c6301_course_nm, c6301_course_desc, c901_primary_dept_id, c101_course_owner
					   , c901_status, c6301_created_by, c6301_created_date
						)
				 VALUES (p_courseid, p_coursenm, p_description, p_deptid, p_owner
					   , 60280, p_user, SYSDATE
						);
							 -- harcode the value for the course status...(and even for all status..)
		--	Throw an exception if any unwanted condition happened.
		-- Exception when ��. Then raise_application_error(-20047, ' ');
		END IF;
	END gm_sav_course_detail;
END gm_pkg_etr_setup;
/
