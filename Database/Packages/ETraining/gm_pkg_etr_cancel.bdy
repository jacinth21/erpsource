/* Formatted on 2009/01/27 10:39 (Formatter Plus v4.8.0) */
CREATE OR REPLACE PACKAGE BODY gm_pkg_etr_cancel
IS
	PROCEDURE gm_etr_sav_void_session (
		p_txn_id   IN	t907_cancel_log.c907_ref_id%TYPE
	  , p_userid   IN	t102_user_login.c102_user_login_id%TYPE
	)
	AS
		/*******************************************************
			* Description : Procedure to void Session
			* Parameters	 : 1. Transaction Id
			*				  2. User Id
			*
			*******************************************************/
		v_sesn_cnt	   NUMBER;
	BEGIN
		SELECT COUNT (*)
		  INTO v_sesn_cnt
		  FROM t6305_session_user_map
		 WHERE c901_status = '60330' AND c6303_session_id = p_txn_id;	--60330-For Active Status

		IF v_sesn_cnt > 0
		THEN
			raise_application_error (-20128, p_txn_id);
		ELSE
			UPDATE t6303_session
			   SET c6303_void_fl = 'Y'
				 , c6303_last_updated_by = p_userid
				 , c6303_last_updated_date = SYSDATE
			 WHERE c6303_session_id = p_txn_id;
		END IF;
	END gm_etr_sav_void_session;

	PROCEDURE gm_etr_sav_void_sesnmodmap (
		p_txn_id   IN	t907_cancel_log.c907_ref_id%TYPE
	  , p_userid   IN	t102_user_login.c102_user_login_id%TYPE
	)
	AS
		/*******************************************************
			* Description : Procedure to void Session module mapping
			* Parameters	 : 1. Transaction Id
			*				  2. User Id
			*
			*******************************************************/
		v_sesn_cnt	   NUMBER;
	BEGIN
		SELECT COUNT (*)
		  INTO v_sesn_cnt
		  FROM t6307_session_user_detail
		 WHERE c901_status = '60340' AND c6304_sess_mod_seq = TO_NUMBER (p_txn_id);   --60340 - For status is In Progress

		IF v_sesn_cnt > 0
		THEN
			raise_application_error (-20128, p_txn_id);
		ELSE
			UPDATE t6304_session_module_map
			   SET c6304_void_fl = 'Y'
			 WHERE c6304_sess_mod_seq = TO_NUMBER (p_txn_id);

			UPDATE t6307_session_user_detail
			   SET c6307_void_fl = 'Y'
			 WHERE c6304_sess_mod_seq = TO_NUMBER (p_txn_id);
		END IF;
	END gm_etr_sav_void_sesnmodmap;

	PROCEDURE gm_etr_sav_void_usermodmap (
		p_txn_id   IN	t907_cancel_log.c907_ref_id%TYPE
	  , p_userid   IN	t102_user_login.c102_user_login_id%TYPE
	)
	AS
		/*******************************************************
		* Description : Procedure to fetch user-module mapping
		* Author		: Ritesh Shah
		*******************************************************/
		v_cnt		   NUMBER;
		v_sesn_id	   VARCHAR2 (20);
		v_user_id	   NUMBER;

		CURSOR c_void_user
		IS
			SELECT t6307.c6304_sess_mod_seq modseq
			  FROM t6307_session_user_detail t6307, t6305_session_user_map t6305, t6304_session_module_map t6304
			 WHERE t6305.c6305_map_seq = p_txn_id
			   AND t6305.c6303_session_id = t6304.c6303_session_id
			   AND t6304.c6304_sess_mod_seq = t6307.c6304_sess_mod_seq
			   AND t6307.c101_user_id = p_userid;
	BEGIN
		SELECT c101_user_id
		  INTO v_user_id
		  FROM t6305_session_user_map
		 WHERE c6305_map_seq = TO_NUMBER (p_txn_id);

		UPDATE t6305_session_user_map
		   SET c6305_void_fl = 'Y'
		 WHERE c6305_map_seq = TO_NUMBER (p_txn_id) AND c901_status IN (60331, 60332);	 -- For status is Added and InActive

		IF (SQL%ROWCOUNT = 0)
		THEN
			raise_application_error (-20140, p_txn_id);
		ELSE
			FOR rec IN c_void_user
			LOOP
				UPDATE t6307_session_user_detail
				   SET c6307_void_fl = 'Y'
				 WHERE c6304_sess_mod_seq = rec.modseq AND c101_user_id = v_user_id;
			END LOOP;
		END IF;
		 /*.Check for t6307 void fl.. whether it is required to update or not?*/
	--END IF;
	END gm_etr_sav_void_usermodmap;

	/*******************************************************
	   * Description : Procedure to Void Course
	  *******************************************************/
	PROCEDURE gm_etr_sav_void_course_setup (
		p_courseid	 IN   t6301_course.c6301_course_id%TYPE
	  , p_userid	 IN   t6301_course.c6301_last_updated_by%TYPE
	)
	AS
		v_cnt		   NUMBER;
	BEGIN
		SELECT COUNT (*)
		  INTO v_cnt
		  FROM t6301_course t6301, t6302_module t6302, t6304_session_module_map t6304
		 WHERE t6301.c6301_course_id = p_courseid
		   AND t6301.c6301_course_id = t6302.c6301_course_id
		   AND t6302.c6302_module_id = t6304.c6302_module_id
		   AND t6304.c901_status = 60320;	-- For Status is In Progress

		--for update of T6301.c6301_course_id
		IF (v_cnt = 0)
		THEN
			UPDATE t6301_course
			   SET c6301_void_fl = 'Y'
				 , c6301_last_updated_date = SYSDATE
				 , c6301_last_updated_by = p_userid
			 WHERE c6301_course_id = p_courseid;
		ELSE
			raise_application_error (-20137, p_courseid);
		END IF;
	END gm_etr_sav_void_course_setup;

	/*******************************************************
	   * Description : Procedure to Void Module
	  *******************************************************/
	PROCEDURE gm_etr_sav_void_module_setup (
		p_moduleid	 IN   t6302_module.c6302_module_id%TYPE
	  , p_userid	 IN   t6302_module.c6302_last_updated_by%TYPE
	)
	AS
		v_cnt		   NUMBER;
	BEGIN
		SELECT COUNT (*)
		  INTO v_cnt
		  FROM t6304_session_module_map t6304, t6302_module t6302
		 WHERE t6302.c6302_module_id = t6304.c6302_module_id
		   AND t6302.c6302_module_id = p_moduleid
		   AND t6304.c901_status = 60320;

		IF (v_cnt = 0)
		THEN
			UPDATE t6302_module
			   SET c6302_void_fl = 'Y'
				 , c6302_last_updated_date = SYSDATE
				 , c6302_last_updated_by = p_userid
			 WHERE c6302_module_id = p_moduleid;
		ELSE
			raise_application_error (-20136, p_moduleid);
		END IF;
	END gm_etr_sav_void_module_setup;

	/*******************************************************
	   * Description : Procedure to Void course-department map
	  *******************************************************/
	PROCEDURE gm_etr_sav_void_coursedept (
		p_seqno   IN   t6306_course_dept_map.c6306_seq_no%TYPE
	)
	AS
	BEGIN
		UPDATE t6306_course_dept_map
		   SET c6306_void_fl = 'Y'
		 WHERE c6306_seq_no = p_seqno;
	END gm_etr_sav_void_coursedept;

	/*******************************************************
		* Description : Procedure to Void document for module
	   *******************************************************/
	PROCEDURE gm_etr_sav_void_moddoc (
		p_no   IN	t903_upload_file_list.c903_upload_file_list%TYPE
	)
	AS
	BEGIN
		UPDATE t903_upload_file_list
		   SET c903_delete_fl = 'Y'
		 WHERE c903_upload_file_list = p_no;
	END gm_etr_sav_void_moddoc;
END gm_pkg_etr_cancel;
/
