/* Formatted on 2009/05/11 18:14 (Formatter Plus v4.8.0) */
--@"C:\database\Packages\etraining\gm_pkg_etr_trans.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_etr_trans
IS
	PROCEDURE gm_fch_sesnmodmap_detail (
		p_sessionid 		IN		 t6303_session.c6303_session_id%TYPE
	  , p_outsesnmodtypes	OUT 	 TYPES.cursor_type
	)
	AS
	/*******************************************************
	* Description : Procedure to fetch session-module mapping
	* Author		: Ritesh Shah
	*******************************************************/
	BEGIN
		OPEN p_outsesnmodtypes
		 FOR
			 SELECT c6304_sess_mod_seq sessmodseq, c6303_session_id sessionid, t6302.c6302_module_id mapmoduleid
				  , c6302_module_nm comname, c6304_duration mapduration, c6304_details details
				  , c6302_module_desc moduledesc, get_user_name (c101_trainer_id) maptrainer
				  , get_code_name (c901_reason) mapreason, get_code_name (t6304.c901_status) mapstatus
			   FROM t6304_session_module_map t6304, t6302_module t6302
			  WHERE c6303_session_id = p_sessionid
				AND c6304_void_fl IS NULL
				AND t6304.c6302_module_id = t6302.c6302_module_id;
	END gm_fch_sesnmodmap_detail;

	PROCEDURE gm_fch_sesnmod_detail (
		p_sesnseqno 		IN		 t6304_session_module_map.c6304_sess_mod_seq%TYPE
	  , p_outsesnmodtypes	OUT 	 TYPES.cursor_type
	)
	AS
	/*******************************************************
	* Description : Procedure to fetch session-module details
	* Author		: Ritesh Shah
	*******************************************************/
	BEGIN
		OPEN p_outsesnmodtypes
		 FOR
			 SELECT c6304_sess_mod_seq sessmodseq, c6303_session_id sessionid, c6302_module_id moduleid
				  , c6304_duration durationhrs, c6304_details details, c101_trainer_id trainerid, c901_reason reason
				  , c901_status statusmap
			   FROM t6304_session_module_map
			  WHERE c6304_sess_mod_seq = p_sesnseqno AND c6304_void_fl IS NULL;
	END gm_fch_sesnmod_detail;

	PROCEDURE gm_fch_inviteduser_detail (
		p_sessionid 		 IN 	  t6305_session_user_map.c6303_session_id%TYPE
	  , p_outsesnusertypes	 OUT	  TYPES.cursor_type
	)
	AS
	 /*******************************************************
	* Description : Procedure to fetch invited user list
	* Author		: Ritesh Shah
	*******************************************************/
	BEGIN
		OPEN p_outsesnusertypes
		 FOR
			 SELECT t6305.c6305_map_seq usrmapseq, t6305.c101_user_id usrid, get_user_name (t6305.c101_user_id) usernm
				  , get_code_name (t6305.c901_status) status, t6305.c901_status statusid
				  , TO_CHAR (t6305.c6305_completed_date, 'MM/DD/YYYY') compltndt
			   FROM t6305_session_user_map t6305
			  WHERE c6303_session_id = p_sessionid AND c6305_void_fl IS NULL;
	END gm_fch_inviteduser_detail;

	PROCEDURE gm_fch_user_detail (
		p_fname 		   IN		t101_user.c101_user_f_name%TYPE
	  , p_lname 		   IN		t101_user.c101_user_l_name%TYPE
	  , p_deptid		   IN		t901_code_lookup.c901_code_id%TYPE
	  , p_outuserdeptdtl   OUT		TYPES.cursor_type
	)
	AS
	 /*******************************************************
	* Description : Procedure to fetch user list for invite
	* Author		: Ritesh Shah
	*******************************************************/
	BEGIN
		OPEN p_outuserdeptdtl
		 FOR
			 SELECT c101_user_id userid, c101_user_f_name || ' ' || c101_user_l_name usernm, c101_dept_id
				  , c902_code_nm_alt deptnm
			   FROM t101_user, t901_code_lookup
			  WHERE LOWER (c101_user_f_name) LIKE '%' || LOWER (NVL (p_fname, c101_user_f_name)) || '%'
				AND LOWER (c101_user_l_name) LIKE '%' || LOWER (NVL (p_lname, c101_user_l_name)) || '%'
				AND c101_dept_id = DECODE (p_deptid, '0', c101_dept_id, get_code_name (p_deptid))
				AND c101_dept_id = c901_code_nm
				AND c901_user_status = 311;   --311 for active users
	END gm_fch_user_detail;

	PROCEDURE gm_sav_sesnmodmap_detail (
		p_sessmodseq	  IN	   t6304_session_module_map.c6304_sess_mod_seq%TYPE
	  , p_sessionid 	  IN	   t6304_session_module_map.c6303_session_id%TYPE
	  , p_moduleid		  IN	   t6304_session_module_map.c6302_module_id%TYPE
	  , p_duration		  IN	   t6304_session_module_map.c6304_duration%TYPE
	  , p_reason		  IN	   t6304_session_module_map.c901_reason%TYPE
	  , p_details		  IN	   t6304_session_module_map.c6304_details%TYPE
	  , p_trainerid 	  IN	   t6304_session_module_map.c101_trainer_id%TYPE
	  , p_status		  IN	   t6304_session_module_map.c901_status%TYPE
	  , p_outsessmodseq   OUT	   VARCHAR2
	)
	AS
		 /*******************************************************
		* Description : Procedure to save session-module mapping
		* Author		: Ritesh Shah
		*******************************************************/
		v_void_fl	   VARCHAR2 (1);
		v_seq_no	   NUMBER;
		v_usrcnt	   NUMBER;
		v_seq_no2	   NUMBER;
		v_seq_mat	   NUMBER;
		v_seq_no1	   NUMBER;

		CURSOR c_fetch_user
		IS
			SELECT c101_user_id userid
			  FROM t6305_session_user_map
			 WHERE c6303_session_id = p_sessionid AND c901_status <> 60333 AND c6305_void_fl IS NULL;

		CURSOR c_fetch_mat
		IS
			SELECT c903_upload_file_list matid, c901_ref_type filetype
			  FROM t903_upload_file_list
			 WHERE c903_ref_id = p_moduleid AND c903_delete_fl IS NULL;
	BEGIN
		/*	SELECT	   c6304_void_fl
				  INTO v_void_fl
				  FROM t6304_session_module_map
				 WHERE c6304_sess_mod_seq = p_sessmodseq
			FOR UPDATE;

			IF v_void_fl IS NOT NULL
			THEN
				raise_application_error (-20129, p_sessmodseq);
			END IF;
			*/
		UPDATE t6304_session_module_map
		   SET c6302_module_id = p_moduleid
			 , c6304_duration = p_duration
			 , c901_reason = p_reason
			 , c6304_details = p_details
			 , c101_trainer_id = p_trainerid
			 , c901_status = p_status
		 WHERE c6304_sess_mod_seq = p_sessmodseq AND c6304_void_fl IS NULL;

		--c6303_session_id = p_sessionid AND c6302_module_id = p_moduleid AND c6304_void_fl IS NULL;
		p_outsessmodseq := p_sessmodseq;

		IF (SQL%ROWCOUNT = 0)
		THEN
			SELECT s6304_sess_mod_id.NEXTVAL
			  INTO v_seq_no
			  FROM DUAL;

			INSERT INTO t6304_session_module_map
						(c6304_sess_mod_seq, c6303_session_id, c6302_module_id, c6304_duration, c901_reason
					   , c6304_details, c101_trainer_id, c901_status
						)
				 VALUES (v_seq_no, p_sessionid, p_moduleid, p_duration, p_reason
					   , p_details, p_trainerid, 60320
						);

			/*
			For 2nd Phase
			*/
			FOR rec IN c_fetch_mat
			LOOP
				SELECT s6308_session_material_map.NEXTVAL
				  INTO v_seq_mat
				  FROM DUAL;

				INSERT INTO t6308_session_material_map
							(c6308_sess_mat_map_id, c6304_sess_mod_seq, c6308_mat_id, c901_type, c6308_mandatory_fl
						   , c6308_void_fl
							)
					 VALUES (v_seq_mat, v_seq_no, rec.matid, rec.filetype, ''
						   , ''
							);

				FOR rec IN c_fetch_user
				LOOP
					SELECT s6309_sess_user_mat_map.NEXTVAL
					  INTO v_seq_no1
					  FROM DUAL;

					INSERT INTO t6309_sess_user_mat_map
								(c6309_sess_user_mat_id, c6308_sess_mat_map_id, c6308_mandatory_fl
							   , c6309_completed_date, c101_user_id
								)
						 VALUES (v_seq_no1, v_seq_mat, NULL
							   , NULL, rec.userid
								);
				END LOOP;
			END LOOP;

			/*End*/
			SELECT COUNT (*)
			  INTO v_usrcnt
			  FROM t6305_session_user_map
			 WHERE c6303_session_id = p_sessionid AND c6305_void_fl IS NULL;

			IF v_usrcnt > 0
			THEN
				FOR rec IN c_fetch_user
				LOOP
					SELECT s6307_sess_user_detail.NEXTVAL
					  INTO v_seq_no2
					  FROM DUAL;

					INSERT INTO t6307_session_user_detail
								(c6307_sess_user_seq_no, c6304_sess_mod_seq, c6307_completed_date, c101_user_id
							   , c6307_mandatory_fl, c901_status, c901_reason
								)
						 VALUES (v_seq_no2, v_seq_no, '', rec.userid
							   , 'Y', 60340, ''
								);
				END LOOP;
			END IF;

			p_outsessmodseq := v_seq_no;
		END IF;
	END gm_sav_sesnmodmap_detail;

	PROCEDURE gm_sav_usersesnmap_detail (
		p_mapseq	  IN	   t6305_session_user_map.c6305_map_seq%TYPE
	  , p_sessionid   IN	   t6305_session_user_map.c6303_session_id%TYPE
	  , p_inputstr	  IN	   VARCHAR2
	  , p_outmapseq   OUT	   VARCHAR2
	)
	AS
		 /*******************************************************
		* Description : Procedure to save user-session mapping
		* Author		: Ritesh Shah
		*******************************************************/
		v_string	   VARCHAR2 (4000) := p_inputstr;
		v_substring    VARCHAR2 (2000);
		v_cnt		   NUMBER;
		v_sesncnt	   NUMBER;

		CURSOR c_fch_sesnmod_detail
		IS
			SELECT c6304_sess_mod_seq seq
			  FROM t6304_session_module_map t6304, t6303_session t6303
			 WHERE t6303.c6303_session_id = p_sessionid
			   AND t6303.c6303_session_id = t6304.c6303_session_id
			   AND t6304.c6304_void_fl IS NULL
			   AND t6303.c6303_void_fl IS NULL;

		CURSOR c_fch_matid
		IS
			SELECT c6308_sess_mat_map_id ID
			  FROM t6308_session_material_map t6308, t6304_session_module_map t6304
			 WHERE t6308.c6304_sess_mod_seq = t6304.c6304_sess_mod_seq
			   AND t6304.c6303_session_id = p_sessionid
			   AND t6308.c6308_void_fl IS NULL
			   AND t6304.c6304_void_fl IS NULL;
	BEGIN
		WHILE INSTR (v_string, '|') <> 0
		LOOP
			v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
			v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);

			SELECT COUNT (*)
			  INTO v_cnt
			  FROM t6305_session_user_map
			 WHERE c101_user_id = v_substring AND c6303_session_id = p_sessionid AND c6305_void_fl IS NULL;

			IF v_cnt <= 0
			THEN
				SELECT COUNT (*)
				  INTO v_sesncnt
				  FROM t6303_session
				 WHERE c6303_session_id = p_sessionid AND c901_status = 60302;

				IF v_sesncnt > 0
				THEN
					raise_application_error (-20135, p_sessionid);
				END IF;

				INSERT INTO t6305_session_user_map
							(c6305_map_seq, c6303_session_id, c101_user_id, c901_status, c6305_completed_date
							)
					 VALUES (s6305_sess_user_map.NEXTVAL, p_sessionid, v_substring, 60332, NULL
							);	 --(60332- "Added" Status)

				FOR rec IN c_fch_sesnmod_detail
				LOOP
					INSERT INTO t6307_session_user_detail
								(c6307_sess_user_seq_no, c6304_sess_mod_seq, c6307_completed_date, c101_user_id
							   , c6307_mandatory_fl, c901_status, c901_reason
								)
						 VALUES (s6307_sess_user_detail.NEXTVAL, rec.seq, NULL, v_substring
							   , 'Y', 60340, NULL
								);
				END LOOP;

				/*2nd phase*/
				FOR rec IN c_fch_matid
				LOOP
					INSERT INTO t6309_sess_user_mat_map
								(c6309_sess_user_mat_id, c6308_sess_mat_map_id, c6308_mandatory_fl
							   , c6309_completed_date, c101_user_id
								)
						 VALUES (s6309_sess_user_mat_map.NEXTVAL, rec.ID, NULL
							   , NULL, v_substring
								);
				END LOOP;
			/*End*/
			END IF;
		END LOOP;
	END gm_sav_usersesnmap_detail;

	PROCEDURE gm_fch_usermodmap_detail (
		p_sessionid 		IN		 t6304_session_module_map.c6303_session_id%TYPE
	  , p_userid			IN		 t6307_session_user_detail.c101_user_id%TYPE
	  , p_owner 			IN		 t6303_session.c101_session_owner%TYPE
	  , p_outuserdtltypes	OUT 	 TYPES.cursor_type
	)
	AS
	/*******************************************************
	* Description : Procedure to fetch user-module mapping
	* Author		: Ritesh Shah
	*******************************************************/
	BEGIN
		OPEN p_outuserdtltypes
		 FOR
			 SELECT ROWNUM NO, t6307.c6307_sess_user_seq_no userseq, get_user_name (t6307.c101_user_id) usrmodnm
				  ,    DECODE (get_cmpltn_prcnt (t6307.c101_user_id, t6302.c6302_module_id, p_sessionid)
							 , '0%', 'Completed'
							 , '', ''
							 , 'Y'
							  )
					|| '  '
					|| DECODE (get_cmpltn_prcnt (t6307.c101_user_id, t6302.c6302_module_id, p_sessionid)
							 , '0%', ''
							 , get_cmpltn_prcnt (t6307.c101_user_id, t6302.c6302_module_id, p_sessionid)
							  ) pending
				  , get_user_name (t6302.c101_module_owner) modowner, get_user_name (t6304.c101_trainer_id) trainerid
				  , TO_CHAR (t6303.c6303_end_date, 'MM/DD/YYYY') enddate, t6302.c6302_module_nm modulenm
				  , t6302.c6302_module_id modid, get_code_name (t6307.c901_reason) reason
				  , get_code_name (t6307.c901_status) status, TO_CHAR (c6307_completed_date, 'MM/DD/YYYY') compltndt
				  , c6307_mandatory_fl isrequired, t6307.c101_user_id modusrid
			   FROM t6307_session_user_detail t6307
				  , t6302_module t6302
				  , t6304_session_module_map t6304
				  , t6303_session t6303
				  , t6305_session_user_map t6305
			  WHERE t6304.c6303_session_id = p_sessionid
				AND t6304.c6304_sess_mod_seq = t6307.c6304_sess_mod_seq
				AND t6304.c6302_module_id = t6302.c6302_module_id
				AND t6307.c101_user_id = DECODE (p_userid, '0', t6307.c101_user_id, p_userid)
				AND t6303.c101_session_owner = DECODE (p_owner, '', t6303.c101_session_owner, p_owner)
				AND t6304.c6303_session_id = t6303.c6303_session_id
				AND t6305.c6303_session_id = t6304.c6303_session_id
				AND t6307.c101_user_id = t6305.c101_user_id
				AND t6305.c901_status NOT IN (60331, 60332)   -- For Active Status only
				AND t6305.c6305_void_fl IS NULL
				AND c6307_void_fl IS NULL;
	END gm_fch_usermodmap_detail;

	PROCEDURE gm_fch_usermod_detail (
		p_mapseqno			IN		 t6304_session_module_map.c6304_sess_mod_seq%TYPE
	  , p_outusermodtypes	OUT 	 TYPES.cursor_type
	)
	AS
	/*******************************************************
		* Description : Procedure to fetch user-module detail
		* Author		 : Ritesh Shah
		*******************************************************/
	BEGIN
		OPEN p_outusermodtypes
		 FOR
			 SELECT t6302.c6302_module_nm modulenm, DECODE (t6307.c6307_mandatory_fl, 'Y', 'on', 'off') isrequired
				  , get_code_name (t6307.c901_status) modstatus, t6307.c901_reason reason
			   FROM t6302_module t6302, t6307_session_user_detail t6307, t6304_session_module_map t6304
			  WHERE t6307.c6307_sess_user_seq_no = p_mapseqno
				AND t6307.c6304_sess_mod_seq = t6304.c6304_sess_mod_seq
				AND t6304.c6302_module_id = t6302.c6302_module_id;
	END gm_fch_usermod_detail;

	PROCEDURE gm_sav_usermodmap_detail (
		p_userseq	   IN		t6307_session_user_detail.c6307_sess_user_seq_no%TYPE
	  , p_reason	   IN		t6307_session_user_detail.c901_reason%TYPE
	  , p_isrequired   IN		t6307_session_user_detail.c6307_mandatory_fl%TYPE
	  , p_outuserseq   OUT		VARCHAR2
	)
	AS
		/*******************************************************
		* Description : Procedure to save user-module mapping
		* Author		: Ritesh Shah
		*******************************************************/
		v_cnt		   NUMBER;
	BEGIN
		SELECT COUNT (*)
		  INTO v_cnt
		  FROM t6307_session_user_detail
		 WHERE c6307_sess_user_seq_no = p_userseq AND c6307_void_fl IS NOT NULL;

		IF (v_cnt > 0)
		THEN
			raise_application_error (-20129, p_userseq);
		ELSE
			UPDATE t6307_session_user_detail
			   SET c6307_mandatory_fl = p_isrequired
				 , c901_reason = DECODE (p_reason, 0, NULL, p_reason)
			 WHERE c6307_sess_user_seq_no = p_userseq;

			p_outuserseq := p_userseq;
		END IF;
	END gm_sav_usermodmap_detail;

	PROCEDURE gm_fch_usermailid_detail (
		p_invitestr   IN	   VARCHAR2
	  , p_outputstr   OUT	   VARCHAR2
	)
	AS
		 /*******************************************************
		* Description : Procedure to fetch usermail list
		* Author		: Ritesh Shah
		*******************************************************/
		v_tomail_str   VARCHAR2 (4000);
		v_string	   VARCHAR2 (4000) := p_invitestr;
		v_substring    VARCHAR2 (2000);
		v_str		   VARCHAR2 (100);
	BEGIN
		WHILE INSTR (v_string, '|') <> 0
		LOOP
			v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
			v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);

			SELECT c101_email_id
			  INTO v_str
			  FROM t101_user
			 WHERE c101_user_id = v_substring AND c901_user_status = 311;	--311 for active users;

			v_tomail_str := v_tomail_str || ';' || v_str;
		END LOOP;

		p_outputstr := SUBSTR (v_tomail_str, 2);
	END gm_fch_usermailid_detail;

	PROCEDURE gm_sav_sesn_cmpltn_stage (
		p_inputstr			  VARCHAR2
	  , p_userid		 IN   t6307_session_user_detail.c101_user_id%TYPE
	  , p_sessionid 	 IN   t6304_session_module_map.c6302_module_id%TYPE
	  , p_feedback		 IN   t6305_session_user_map.c6305_feedback%TYPE
	  , p_sessionowner	 IN   t6303_session.c101_session_owner%TYPE
	)
	AS
		v_cnt		   NUMBER;
		v_cmpltndate   VARCHAR2 (20);
		v_userseq	   VARCHAR2 (15);
		v_string	   VARCHAR2 (4000) := p_inputstr;
		v_substring    VARCHAR2 (4000);
	/*******************************************************
	* Description : Procedure to save session completion
	* Author		: Ritesh Shah
	*******************************************************/
	BEGIN
		--In loop logic for inputstr
		WHILE INSTR (v_string, '|') <> 0
		LOOP
			v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
			v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
			v_userseq	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
			v_cmpltndate := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);

			UPDATE t6307_session_user_detail
			   SET c6307_completed_date = TO_DATE (v_cmpltndate, 'MM/DD/YYYY')
				 , c901_status = 60341	 -- For Status is Completed
			 WHERE c6307_sess_user_seq_no = TO_NUMBER (v_userseq) AND c6307_void_fl IS NULL;

--For updating the session user material mapping for one or more than one record.
			UPDATE t6309_sess_user_mat_map
			   SET c6309_completed_date = TO_DATE (v_cmpltndate, 'MM/DD/YYYY')
			 WHERE c6308_sess_mat_map_id IN (
					   SELECT t6308.c6308_sess_mat_map_id
						 FROM t6308_session_material_map t6308
							, t6304_session_module_map t6304
							, t6307_session_user_detail t6307
						WHERE t6308.c6304_sess_mod_seq = t6304.c6304_sess_mod_seq
						  AND t6304.c6304_sess_mod_seq = t6307.c6304_sess_mod_seq
						  AND t6307.c6307_sess_user_seq_no = TO_NUMBER (v_userseq))
			   AND c101_user_id IN (
					   SELECT t6307.c101_user_id
						 FROM t6307_session_user_detail t6307, t6304_session_module_map t6304
						WHERE t6304.c6304_sess_mod_seq = t6307.c6304_sess_mod_seq
						  AND t6307.c6307_sess_user_seq_no = TO_NUMBER (v_userseq));

--For updating the session completion date and status for not to display in User dashboard.
			IF p_sessionowner = p_userid
			THEN
				UPDATE t6305_session_user_map
				   SET c901_status = 60333	 --Completed Status
					 , c6305_completed_date = TO_DATE (v_cmpltndate, 'MM/DD/YYYY')
					 , c6305_feedback = p_feedback
				 WHERE c6303_session_id = p_sessionid
				   AND c101_user_id IN (
						   SELECT t6307.c101_user_id
							 FROM t6307_session_user_detail t6307, t6304_session_module_map t6304
							WHERE t6304.c6304_sess_mod_seq = t6307.c6304_sess_mod_seq
							  AND t6307.c6307_sess_user_seq_no = TO_NUMBER (v_userseq));
			END IF;
		END LOOP;

		SELECT COUNT (*)
		  INTO v_cnt
		  FROM t6307_session_user_detail t6307, t6304_session_module_map t6304
		 WHERE c6307_completed_date IS NULL
		   AND c101_user_id = p_userid
		   AND t6304.c6303_session_id = p_sessionid
		   AND t6304.c6304_sess_mod_seq = t6307.c6304_sess_mod_seq;

		IF v_cnt > 0
		THEN
			UPDATE t6305_session_user_map
			   SET c901_status = 60330	 --In Progress Status
				 , c6305_feedback = p_feedback
			 WHERE c6303_session_id = p_sessionid AND c101_user_id = p_userid;
		ELSE
			UPDATE t6305_session_user_map
			   SET c901_status = 60333	 --Completed Status
				 , c6305_completed_date =
					   (SELECT MAX (c6307_completed_date)
						  FROM t6307_session_user_detail t6307, t6304_session_module_map t6304
						 WHERE c101_user_id = p_userid
						   AND t6304.c6303_session_id = p_sessionid
						   AND t6304.c6304_sess_mod_seq = t6307.c6304_sess_mod_seq)
				 , c6305_feedback = p_feedback
			 WHERE c6303_session_id = p_sessionid AND c101_user_id = p_userid;

			UPDATE t6303_session
			   SET c901_status = 60301
				 ,	 -- completed status
				   c6303_last_updated_date = SYSDATE
				 , c6303_last_updated_by = p_userid
			 WHERE c6303_session_id = p_sessionid;
		END IF;
	END gm_sav_sesn_cmpltn_stage;

		/*******************************************************
	* Description : Procedure to fetch session completion
	* Author		: Ritesh Shah
	*******************************************************/
	PROCEDURE gm_fch_sesncmpltn_detail (
		p_sessionid    IN		VARCHAR2
	  , p_userid	   IN		VARCHAR2
	  , p_outsesndtl   OUT		TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_outsesndtl
		 FOR
			 SELECT get_code_name (c901_status) sesnusrmapstatus, TO_CHAR (c6305_completed_date, 'MM/DD/YYYY')
																											  cmpltndt
				  , c6305_feedback feedback
			   FROM t6305_session_user_map
			  WHERE c6303_session_id = p_sessionid AND c101_user_id = p_userid AND c6305_void_fl IS NULL;
	END gm_fch_sesncmpltn_detail;

		/*******************************************************
	* Description : Procedure to fetch course department detail
	* Author		: Ritesh Shah /Angela

	*******************************************************/
	PROCEDURE gm_fch_coursedept_detail (
		p_coursedeptseq 	 IN 	  VARCHAR2
	  , p_outcoursedeptdtl	 OUT	  TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_outcoursedeptdtl
		 FOR
			 SELECT t6306.c6306_seq_no coursemapseq, t6306.c901_dept_id deptmapid
				  , DECODE (t6306.c6306_mandatory_fl, 'Y', 'on', 'off') mandfltype
			   FROM t6306_course_dept_map t6306
			  WHERE t6306.c6306_seq_no = p_coursedeptseq;
	--join not required..and also name is not needed
	END gm_fch_coursedept_detail;

	/*******************************************************
	* Description : Procedure to fetch course department map detail
	* Author		: Ritesh Shah /Angela
	*******************************************************/
	PROCEDURE gm_fch_coursedeptmap_detail (
		p_courseid				IN		 t6306_course_dept_map.c6301_course_id%TYPE
	  , p_outcoursedeptmapdtl	OUT 	 TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_outcoursedeptmapdtl
		 FOR
			 SELECT c6306_seq_no csdeptseq, get_code_name_alt (c901_dept_id) comname, c6306_mandatory_fl mandfltype
			   FROM t6306_course_dept_map
			  WHERE c6301_course_id = p_courseid AND c6306_void_fl IS NULL;
	END gm_fch_coursedeptmap_detail;

		 /*******************************************************
	* Description : Procedure to save course-department mapping
	* Author		: Ritesh Shah/angela
	*******************************************************/
	PROCEDURE gm_sav_coursedeptmap_detail (
		p_seqno 		IN		 t6306_course_dept_map.c6306_seq_no%TYPE
	  , p_courseid		IN		 t6306_course_dept_map.c6301_course_id%TYPE
	  , p_cdmapdeptid	IN		 t6306_course_dept_map.c901_dept_id%TYPE
	  , p_mandfl		IN		 t6306_course_dept_map.c6306_mandatory_fl%TYPE
	  , p_ourseq		OUT 	 VARCHAR2
	)
	AS
		v_cnt		   NUMBER;
		v_seq_no	   NUMBER;
	BEGIN
				/*
				SELECT COUNT (*)
				  INTO v_cnt
				  FROM t6306_course_dept_map
				 WHERE c6306_void_fl IS NOT NULL AND c6301_course_id = p_courseid AND c901_dept_id = p_cdmapdeptid;

				IF (v_cnt > 0)
				THEN
					raise_application_error (-20047, p_seqno);
				END IF;
		*/
		UPDATE t6306_course_dept_map
		   SET c6306_mandatory_fl = p_mandfl
		 WHERE c6306_seq_no = p_seqno OR (c6301_course_id = p_courseid AND c901_dept_id = p_cdmapdeptid);

		p_ourseq	:= p_seqno;

		IF (SQL%ROWCOUNT = 0)
		THEN
			SELECT s6306_course_dept_map.NEXTVAL
			  INTO v_seq_no
			  FROM DUAL;

			p_ourseq	:= v_seq_no;

			INSERT INTO t6306_course_dept_map
						(c6306_seq_no, c6301_course_id, c901_dept_id, c6306_mandatory_fl
						)
				 VALUES (v_seq_no, p_courseid, p_cdmapdeptid, p_mandfl
						);
		END IF;
	END gm_sav_coursedeptmap_detail;

		/*******************************************************
	* Description : Procedure to fetch session material detail
	* Author		: Ritesh Shah
	*******************************************************/
	PROCEDURE gm_fch_sesnmatmap_detail (
		p_moduleid			 IN 	  t6302_module.c6302_module_id%TYPE
	  , p_outsesnmatmapdtl	 OUT	  TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_outsesnmatmapdtl
		 FOR
			 SELECT t6308.c6308_sess_mat_map_id seq, t6308.c6308_mat_id, t903.c903_file_name filenm
				  , get_user_name (t903.c903_created_by) matowner
				  , TO_CHAR (t903.c903_created_date, 'MM/DD/YYYY') crdate, get_code_name (t6308.c901_type) filetype
				  , t6308.c6308_mandatory_fl mandatory
			   FROM t6308_session_material_map t6308, t903_upload_file_list t903, t6304_session_module_map t6304
			  WHERE t6304.c6302_module_id = p_moduleid
				AND t6304.c6304_sess_mod_seq = t6308.c6304_sess_mod_seq
				AND t6308.c6308_mat_id = t903.c903_upload_file_list
				AND t6308.c6308_void_fl IS NULL
				AND t903.c903_delete_fl IS NULL;
	END gm_fch_sesnmatmap_detail;

	/*******************************************************
	* Description : Procedure to fetch material detail based on sequence
	* Author		: Ritesh Shah
	*******************************************************/
	PROCEDURE gm_fch_sesnmat_detail (
		p_seq			  IN	   t6308_session_material_map.c6308_sess_mat_map_id%TYPE
	  , p_outsesnmatdtl   OUT	   TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_outsesnmatdtl
		 FOR
			 SELECT c6308_sess_mat_map_id, c6308_mat_id, get_code_name (c901_type) filetype
				  , DECODE (c6308_mandatory_fl, 'Y', 'on', 'off') mandatory
				  , get_user_name (t6304.c101_trainer_id) trainer, t903.c903_file_name filenm
			   FROM t6308_session_material_map t6308, t6304_session_module_map t6304, t903_upload_file_list t903
			  WHERE c6308_sess_mat_map_id = p_seq
				AND t6308.c6304_sess_mod_seq = t6304.c6304_sess_mod_seq
				AND t903.c903_upload_file_list = t6308.c6308_mat_id;
	END gm_fch_sesnmat_detail;

	/*******************************************************
	* Description : Procedure to save mandatory material detail based on sequence
	* Author		: Ritesh Shah
	*******************************************************/
	PROCEDURE gm_sav_sesnmatmap_detail (
		p_seq	   IN	t6308_session_material_map.c6308_sess_mat_map_id%TYPE
	  , p_mandat   IN	t6308_session_material_map.c6308_mandatory_fl%TYPE
	)
	AS
	BEGIN
		UPDATE t6308_session_material_map
		   SET c6308_mandatory_fl = p_mandat
		 WHERE c6308_sess_mat_map_id = p_seq;
	END gm_sav_sesnmatmap_detail;

	/*******************************************************
	* Description : Function to fetch percentage for how many materials has been read
	* Author		: Ritesh Shah
	*******************************************************/
	FUNCTION get_cmpltn_prcnt (
		p_userid	  IN   t6309_sess_user_mat_map.c101_user_id%TYPE
	  , p_moduleid	  IN   t6304_session_module_map.c6302_module_id%TYPE
	  , p_sessionid   IN   t6304_session_module_map.c6303_session_id%TYPE
	)
		RETURN VARCHAR2
	IS
		v_total_cnt    NUMBER;
		v_cmplt_cnt    NUMBER;
		v_prcnt 	   VARCHAR2 (10);
	BEGIN
		SELECT COUNT (*)
		  INTO v_total_cnt
		  FROM t6309_sess_user_mat_map t6309, t6308_session_material_map t6308, t6304_session_module_map t6304
		 WHERE t6309.c101_user_id = p_userid
		   AND t6309.c6308_sess_mat_map_id = t6308.c6308_sess_mat_map_id
		   AND t6308.c6304_sess_mod_seq = t6304.c6304_sess_mod_seq
		   AND t6304.c6302_module_id = p_moduleid
		   AND t6304.c6303_session_id = p_sessionid;

		SELECT COUNT (*)
		  INTO v_cmplt_cnt
		  FROM t6309_sess_user_mat_map t6309, t6308_session_material_map t6308, t6304_session_module_map t6304
		 WHERE t6309.c101_user_id = p_userid
		   AND t6309.c6308_sess_mat_map_id = t6308.c6308_sess_mat_map_id
		   AND t6308.c6304_sess_mod_seq = t6304.c6304_sess_mod_seq
		   AND t6304.c6302_module_id = p_moduleid
		   AND t6304.c6303_session_id = p_sessionid
		   AND t6309.c6309_completed_date IS NOT NULL;

		IF ((v_total_cnt = v_cmplt_cnt) AND v_total_cnt > 0)
		THEN
			v_prcnt 	:= '0%';
		ELSIF (v_total_cnt > 0 AND v_cmplt_cnt > 0)
		THEN
			v_prcnt 	:= ROUND (100 - ((v_cmplt_cnt * 100) / v_total_cnt), 2) || '%';
		ELSIF (v_total_cnt > 0 AND v_cmplt_cnt = 0)
		THEN
			v_prcnt 	:= '100%';
		ELSIF (v_total_cnt = 0)
		THEN
			v_prcnt 	:= '';
		END IF;

		RETURN v_prcnt;
	END get_cmpltn_prcnt;

	/*******************************************************
	* Description : Procedure to fetch user material detail
	* Author		: Ritesh Shah
	*******************************************************/
	PROCEDURE gm_fch_usermatmap_detail (
		p_moduleid			 IN 	  t6302_module.c6302_module_id%TYPE
	  , p_userid			 IN 	  t6309_sess_user_mat_map.c101_user_id%TYPE
	  , p_sessionid 		 IN 	  t6303_session.c6303_session_id%TYPE
	  , p_outusermatmapdtl	 OUT	  TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_outusermatmapdtl
		 FOR
			 SELECT   t6309.c6309_sess_user_mat_id usermatid, t6308.c6308_mat_id matid, t903.c903_file_name filenm
					, get_code_name (t6308.c901_type) filetype, get_user_name (t6304.c101_trainer_id) modtrainer
					, TO_CHAR (t903.c903_created_date, 'MM/DD/YYYY') crdate, t6308.c6308_mandatory_fl mandatory
					, COUNT (t6310.c6309_sess_user_mat_id) cnt, SUM (t6310.c6310_duration) DURATION
					, TO_CHAR (t6309.c6309_completed_date, 'MM/DD/YYYY') matcmpltndt
				 FROM t6308_session_material_map t6308
					, t903_upload_file_list t903
					, t6304_session_module_map t6304
					, t6309_sess_user_mat_map t6309
					, t6310_user_mat_track t6310
				WHERE t6304.c6302_module_id = p_moduleid
				  AND t6309.c101_user_id = p_userid
				  AND t6309.c6308_sess_mat_map_id = t6308.c6308_sess_mat_map_id
				  AND t6304.c6304_sess_mod_seq = t6308.c6304_sess_mod_seq
				  AND t6308.c6308_mat_id = t903.c903_upload_file_list
				  AND t6308.c6308_void_fl IS NULL
				  AND t903.c903_delete_fl IS NULL
				  AND t6310.c6309_sess_user_mat_id(+) = t6309.c6309_sess_user_mat_id
				  AND t6304.c6303_session_id = p_sessionid
			 GROUP BY t6309.c6309_sess_user_mat_id
					, t6308.c6308_mat_id
					, t903.c903_file_name
					, t6308.c901_type
					, t6304.c101_trainer_id
					, t903.c903_created_date
					, t6308.c6308_mandatory_fl
					, t6309.c6309_completed_date;
	END gm_fch_usermatmap_detail;

	/*******************************************************
	* Description : Procedure to save material tracking details
	* Author		: Ritesh Shah
	*******************************************************/
	PROCEDURE gm_sav_usermattrack_detail (
		p_seq			  IN	   t6310_user_mat_track.c6310_user_mat_track_id%TYPE
	  , p_sesnusermatid   IN	   t6309_sess_user_mat_map.c6309_sess_user_mat_id%TYPE
	  , p_chkcmpltn 	  IN	   VARCHAR2
	  , p_moduleid		  IN	   t6304_session_module_map.c6302_module_id%TYPE
	  , p_userid		  IN	   t6309_sess_user_mat_map.c101_user_id%TYPE
	  , p_sessionid 	  IN	   t6304_session_module_map.c6303_session_id%TYPE
	  , p_outseq		  OUT	   t6310_user_mat_track.c6310_user_mat_track_id%TYPE
	)
	AS
		v_seq		   NUMBER;
		v_dr		   NUMBER;
		v_loggeddate   DATE;
		tmp_dr		   NUMBER;
		v_cnt		   NUMBER;
		v_trackid	   NUMBER;

		CURSOR c_mat_track
		IS
			SELECT c6310_user_mat_track_id trackid, c6310_logged_date loggeddate, c6310_duration dr
			  FROM t6310_user_mat_track t6310
			 WHERE c6310_user_mat_track_id = p_seq;
	/*
	--new code as per model window logic
	CURSOR c_mat_track
	IS
		SELECT c6310_user_mat_track_id trackid, c6310_logged_date loggeddate, c6310_duration dr
			FROM t6310_user_mat_track t6310, t6309_sess_user_mat_map t6309
		 WHERE t6310.c6309_sess_user_mat_id = t6309.c6309_sess_user_mat_id
			 AND t6309.c101_user_id = p_userid
			 AND t6309.c6309_sess_user_mat_id = p_sesnusermatid
			 AND t6309.c6309_completed_date IS NULL;
			 */
	BEGIN
		OPEN c_mat_track;

		FETCH c_mat_track
		 INTO v_trackid, v_loggeddate, v_dr;

		IF c_mat_track%FOUND
		THEN
			tmp_dr		:= ROUND ((SYSDATE - v_loggeddate) * 24, 4);
			v_dr		:= v_dr + tmp_dr;

			UPDATE t6310_user_mat_track
			   SET c6310_duration = v_dr
			 WHERE c6310_user_mat_track_id = p_seq;

			p_outseq	:= 0;	--Assign zero for action class validation.
		ELSE
			SELECT s6310_user_mat_track.NEXTVAL
			  INTO v_seq
			  FROM DUAL;

			INSERT INTO t6310_user_mat_track
						(c6310_user_mat_track_id, c6309_sess_user_mat_id, c6310_logged_date, c6310_duration
						)
				 VALUES (v_seq, p_sesnusermatid, SYSDATE, 0
						);

			p_outseq	:= v_seq;
		END IF;

		IF p_chkcmpltn = 'Y'
		THEN
			UPDATE t6309_sess_user_mat_map
			   SET c6309_completed_date = SYSDATE
			 WHERE c6309_sess_user_mat_id = p_sesnusermatid;

/*Code has to be checked for different scenario*/
			SELECT COUNT (*)
			  INTO v_cnt
			  FROM t6309_sess_user_mat_map t6309, t6304_session_module_map t6304, t6308_session_material_map t6308
			 WHERE t6309.c101_user_id = p_userid
			   AND t6304.c6304_sess_mod_seq = t6308.c6304_sess_mod_seq
			   AND t6304.c6302_module_id = p_moduleid
			   AND t6304.c6303_session_id = p_sessionid   --added later
			   AND t6308.c6308_sess_mat_map_id = t6309.c6308_sess_mat_map_id
			   AND t6309.c6309_completed_date IS NULL;

			IF v_cnt = 0
			THEN
				UPDATE t6307_session_user_detail
				   SET c6307_completed_date = (SELECT t6309.c6309_completed_date
												 FROM t6309_sess_user_mat_map t6309
												WHERE c6309_sess_user_mat_id = p_sesnusermatid)
					 /*
							 (SELECT MAX (t6309.c6309_completed_date)
								FROM t6309_sess_user_mat_map t6309
								   , t6308_session_material_map t6308
								   , t6304_session_module_map t6304
							   WHERE t6309.c6308_sess_mat_map_id = t6308.c6308_sess_mat_map_id
								 AND t6308.c6304_sess_mod_seq = t6304.c6304_sess_mod_seq
								 AND t6304.c6302_module_id = p_moduleid
								 AND t6309.c101_user_id = p_userid)*/
				,	   c901_status = 60341	 --For Status is Completed
				 WHERE c6304_sess_mod_seq =
									(SELECT t6304.c6304_sess_mod_seq
									   FROM t6304_session_module_map t6304
									  WHERE t6304.c6302_module_id = p_moduleid AND t6304.c6303_session_id = p_sessionid)
				   /*
							 (SELECT t6304.c6304_sess_mod_seq
								FROM t6309_sess_user_mat_map t6309
								   , t6308_session_material_map t6308
								   , t6304_session_module_map t6304
							   WHERE t6309.c6308_sess_mat_map_id = t6308.c6308_sess_mat_map_id
								 AND t6308.c6304_sess_mod_seq = t6304.c6304_sess_mod_seq
								 AND t6304.c6302_module_id = p_moduleid
								 AND t6309.c101_user_id = p_userid
								 AND ROWNUM < 2)
								 */
				   AND c101_user_id = p_userid
				   AND c6307_void_fl IS NULL;
			END IF;
		/*End of code which has to be checked*/
		END IF;

		CLOSE c_mat_track;
	END;

	/*******************************************************
	* Description : Procedure to update the user session status
	* Author		: Ritesh Shah
	*******************************************************/
	PROCEDURE gm_sav_sesnstatus (
		p_userstr	 IN   VARCHAR
	  , p_statusid	 IN   t6305_session_user_map.c901_status%TYPE
	  , p_userseq	 IN   t6305_session_user_map.c6305_map_seq%TYPE
	)
	AS
		v_string	   VARCHAR2 (4000) := p_userstr;
		v_substring    VARCHAR2 (2000);
	BEGIN
		IF p_userseq IS NULL
		THEN
			WHILE INSTR (v_string, '|') <> 0
			LOOP
				v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
				v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);

				UPDATE t6305_session_user_map
				   SET c901_status = 60330
				 WHERE c101_user_id = v_substring AND c901_status = 60332;
			END LOOP;
		ELSE
			UPDATE t6305_session_user_map
			   SET c901_status = DECODE (p_statusid, 60330, 60331, 60331, 60330)
			 WHERE c6305_map_seq = p_userseq;
		END IF;
	END gm_sav_sesnstatus;

	/*******************************************************
	* Description : Procedure to fetch material list for module
	* Author		: Ritesh Shah
	*******************************************************/
	PROCEDURE gm_fch_material_detail (
		p_moduleid	  IN	   t6304_session_module_map.c6302_module_id%TYPE
	  , p_sessionid   IN	   t6304_session_module_map.c6303_session_id%TYPE
	  , p_outmatdtl   OUT	   TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_outmatdtl
		 FOR
			 SELECT   c903_upload_file_list matid, c903_file_name matnm, get_code_name (c901_ref_type) mattype
					, c901_ref_type reftypeid, 'New' matstat
				 FROM t903_upload_file_list
				WHERE c903_upload_file_list NOT IN (
						  SELECT c6308_mat_id
							FROM t6308_session_material_map t6308, t6304_session_module_map t6304
						   WHERE t6308.c6304_sess_mod_seq = t6304.c6304_sess_mod_seq
							 AND t6304.c6302_module_id = p_moduleid
							 AND t6304.c6303_session_id = p_sessionid)
				  AND c903_ref_id = p_moduleid
				  AND c903_delete_fl IS NULL
			 UNION
			 SELECT   c903_upload_file_list matid, c903_file_name matnm, get_code_name (c901_ref_type) mattype
					, c901_ref_type reftypeid, 'Included' matstat
				 FROM t903_upload_file_list
				WHERE c903_upload_file_list IN (
						  SELECT c6308_mat_id
							FROM t6308_session_material_map t6308, t6304_session_module_map t6304
						   WHERE t6308.c6304_sess_mod_seq = t6304.c6304_sess_mod_seq
							 AND t6304.c6302_module_id = p_moduleid
							 AND t6304.c6303_session_id = p_sessionid
							 AND c6308_void_fl IS NULL)
				  AND c903_ref_id = p_moduleid
				  AND c903_delete_fl IS NULL
			 ORDER BY matstat;
	END gm_fch_material_detail;

	/*******************************************************
	* Description : Procedure to save material for module
	* Author		: Ritesh Shah
	*******************************************************/
	PROCEDURE gm_sav_material_detail (
		p_moduleid	  IN   t6304_session_module_map.c6302_module_id%TYPE
	  , p_sessionid   IN   t6304_session_module_map.c6303_session_id%TYPE
	  , p_inputstr	  IN   VARCHAR2
	  , p_inputstr1   IN   VARCHAR2
	)
	AS
		v_cnt		   NUMBER;
		v_seq_no	   NUMBER;
		v_seq_mat	   NUMBER;
		v_matid 	   VARCHAR2 (15);
		v_substring    VARCHAR2 (4000);
		v_substring1   VARCHAR2 (4000);
		v_string	   VARCHAR2 (4000) := p_inputstr;
		v_string1	   VARCHAR2 (4000) := p_inputstr1;
		v_reftypeid    VARCHAR2 (4000);
		v_seq_no1	   NUMBER;

		CURSOR c_userlist
		IS
			SELECT c101_user_id sesnuserid
			  FROM t6305_session_user_map
			 WHERE c6303_session_id = p_sessionid AND c901_status <> 60333 AND c6305_void_fl IS NULL;	--60333 is for completed status.
	BEGIN
		SELECT c6304_sess_mod_seq
		  INTO v_seq_no
		  FROM t6304_session_module_map
		 WHERE c6303_session_id = p_sessionid AND c6302_module_id = p_moduleid;

		WHILE INSTR (v_string, '|') <> 0
		LOOP
			v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
			v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
			v_matid 	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
			v_reftypeid := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);

			SELECT s6308_session_material_map.NEXTVAL
			  INTO v_seq_mat
			  FROM DUAL;

			INSERT INTO t6308_session_material_map
						(c6308_sess_mat_map_id, c6304_sess_mod_seq, c6308_mat_id, c901_type, c6308_mandatory_fl
					   , c6308_void_fl
						)
				 VALUES (v_seq_mat, v_seq_no, v_matid, v_reftypeid, ''
					   , ''
						);

			FOR rec IN c_userlist
			LOOP
				SELECT s6309_sess_user_mat_map.NEXTVAL
				  INTO v_seq_no1
				  FROM DUAL;

				INSERT INTO t6309_sess_user_mat_map
							(c6309_sess_user_mat_id, c6308_sess_mat_map_id, c6308_mandatory_fl, c6309_completed_date
						   , c101_user_id
							)
					 VALUES (v_seq_no1, v_seq_mat, NULL, NULL
						   , rec.sesnuserid
							);
			END LOOP;
		END LOOP;

		WHILE INSTR (v_string1, '|') <> 0
		LOOP
			v_substring1 := SUBSTR (v_string1, 1, INSTR (v_string1, '|') - 1);
			v_string1	:= SUBSTR (v_string1, INSTR (v_string1, '|') + 1);

			SELECT COUNT (*)
			  INTO v_cnt
			  FROM t6310_user_mat_track t6310, t6309_sess_user_mat_map t6309, t6308_session_material_map t6308
			 WHERE t6309.c6309_sess_user_mat_id = t6310.c6309_sess_user_mat_id
			   AND t6309.c6308_sess_mat_map_id = t6308.c6308_sess_mat_map_id
			   AND t6308.c6308_mat_id = v_substring1
			   AND t6308.c6304_sess_mod_seq = v_seq_no;

			IF v_cnt > 0
			THEN
				raise_application_error (-20139, v_substring1);
			ELSE
				DELETE FROM t6309_sess_user_mat_map
					  WHERE c6308_sess_mat_map_id IN (
													SELECT c6308_sess_mat_map_id
													  FROM t6308_session_material_map
													 WHERE c6308_mat_id = v_substring1
														   AND c6304_sess_mod_seq = v_seq_no);

				DELETE FROM t6308_session_material_map
					  WHERE c6308_mat_id = v_substring1 AND c6304_sess_mod_seq = v_seq_no;
			END IF;
		END LOOP;
	END gm_sav_material_detail;
END gm_pkg_etr_trans;
/
