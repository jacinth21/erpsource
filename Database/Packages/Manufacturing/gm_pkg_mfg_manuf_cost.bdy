/* Formatted on 2010/03/10 09:56 (Formatter Plus v4.8.0) */

--@C:\PMT\db\Packages\Manufacturing\gm_pkg_mfg_manuf_cost.bdy;
CREATE OR REPLACE PACKAGE BODY gm_pkg_mfg_manuf_cost
IS
/****************************************************************
    * Description : Procedure to save the Part number cost  
    * Author      : Prabhu Vigneshwaran
    *****************************************************************/
PROCEDURE gm_sav_manuf_bulk_update(
        p_partinputstr IN CLOB,
        p_inputstr     IN CLOB,
        p_user_id	   IN t205_part_number.c205_last_updated_by%TYPE,
        p_message OUT CLOB
        )
AS
    v_invalid_partids CLOB;
    v_cost NUMBER;
    v_partid T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE;
    v_strlen    NUMBER           := NVL (LENGTH (p_inputstr), 0) ;
    v_string    CLOB := p_inputstr;
    v_substring VARCHAR2 (4000) ;
    v_msg        VARCHAR2 (4000);
    v_out_invalid_part   CLOB;
    v_out_invalid_part_cmp  CLOB;
    v_msg_part CLOB;
BEGIN
      gm_pkg_pd_partnumber.gm_validate_mfg_parts(p_partinputstr,v_out_invalid_part,v_out_invalid_part_cmp);
     IF v_out_invalid_part IS NOT NULL THEN
      v_msg_part := 'The following are invalid parts :' || v_out_invalid_part ||'. Please remove the mentioned part number(s) in order to save.<BR>';
     END IF;
    IF v_out_invalid_part_cmp IS NOT NULL THEN
      v_msg_part :=  v_msg_part || ' Part not mapped for this company: '|| v_out_invalid_part_cmp ;
    END IF;
    IF v_msg_part IS NOT null THEN
        raise_application_error(-20999 ,v_msg_part );
    END IF;
        WHILE INSTR (v_string, '|') <> 0
        LOOP
            v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
            v_string    := SUBSTR (v_string, INSTR (v_string, '|')    + 1) ;
            v_partid     := NULL;
            v_cost      := NULL;
            v_partid     := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            v_substring := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
            v_cost      := v_substring ;
            gm_mf_sav_mfg_cost(v_partid,v_cost,p_user_id,v_msg) ;
        END LOOP;

END gm_sav_manuf_bulk_update;
  
  
 END gm_pkg_mfg_manuf_cost;
  /