--PC-5562 inserting new code id to voiding DHR request from screen
INSERT INTO t901_code_lookup (
    c901_code_id,
    c901_code_nm,
    c901_code_grp,
    c901_active_fl,
    c901_code_seq_no,
    c902_code_nm_alt,
    c901_control_type,
    c901_created_by,
    c901_created_date
) VALUES (
    108262,
    'Void DHR Request',
    'CNCLT',
    '0',
    '168',
    'VODHR',
    '',
    '2765071',
    current_date
);


--PC-5562 inserting three new code ids for the reason of voiding DHR request 
INSERT INTO t901_code_lookup (
    c901_code_id,
    c901_code_nm,
    c901_code_grp,
    c901_active_fl,
    c901_code_seq_no,
    c902_code_nm_alt,
    c901_control_type,
    c901_created_by,
    c901_created_date
) VALUES (
    112040,
    'Reason 1',
    'VODHR',
    '0',
    '1',
    '',
    '',
    '2765071',
    current_date
);

INSERT INTO t901_code_lookup (
    c901_code_id,
    c901_code_nm,
    c901_code_grp,
    c901_active_fl,
    c901_code_seq_no,
    c902_code_nm_alt,
    c901_control_type,
    c901_created_by,
    c901_created_date
) VALUES (
    112041,
    'Reason 2',
    'VODHR',
    '0',
    '2',
    '',
    '',
    '2765071',
    current_date
);

INSERT INTO t901_code_lookup (
    c901_code_id,
    c901_code_nm,
    c901_code_grp,
    c901_active_fl,
    c901_code_seq_no,
    c902_code_nm_alt,
    c901_control_type,
    c901_created_by,
    c901_created_date
) VALUES (
    112042,
    'Reason 3',
    'VODHR',
    '0',
    '3',
    '',
    '',
    '2765071',
    current_date
);

--pc 5562 inserting code group in group master for void dhr
INSERT INTO t901b_code_group_master (
    c901b_code_grp_id,
    c901b_code_grp,
    c901b_code_description,
    c901b_lookup_needed,
    c901b_created_by,
    c901b_created_date
) VALUES (
    s901b_code_group_master.NEXTVAL,
    'VODHR',
    'Void DHR Request',
    '',
    '2765071',
    current_date
);

--pc 5562 inserting into rules to void dhr
INSERT INTO t906_rules (
c906_rule_seq_id,
c906_rule_id,
c906_rule_desc,
c906_rule_value,
c906_created_date,
c906_rule_grp_id,
c906_created_by
) VALUES (
s906_rule.NEXTVAL,
'108262',
'Void DHR Request',
'gm_pkg_op_dhr.gm_op_void_mfg_dhrs',
sysdate,
'CMNCNCL',
'303043'
);