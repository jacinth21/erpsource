DELETE FROM T9300_Job Where C9300_Job_Id='1060' AND C9300_Job_Nm ='LOC_MNF_DHR_MAIN';

INSERT INTO T9300_Job(C9300_Job_Id,C9300_Job_Nm,C9300_What,C9300_Start_Date ,C9300_Created_By ,C9300_Created_Date,C9300_Inactive_Fl)
VALUES (1060,'LOC_MNF_DHR_MAIN', 'gm_pkg_mnf_app_main.gm_process_app_main()', NULL,'3178209', CURRENT_DATE, NULL);

DELETE FROM T906_Rules Where C906_Rule_Id ='LOCATION_MANF_DHR' AND C906_Rule_Grp_Id='LOCATION_DHR';

INSERT INTO t906_rules (c906_rule_seq_id,c906_rule_id,c906_rule_desc,c906_rule_value,c906_created_date,c906_rule_grp_id,
c906_created_by,c906_last_updated_date,c906_last_updated_by,c901_rule_type,c906_active_fl,c906_void_fl,c1900_company_id,c906_last_updated_date_utc) 
VALUES (S906_Rule.Nextval,'LOCATION_MANF_DHR','Clear Location manf DHR after processing','108197',current_date,'LOCATION_DHR','3178209',NULL,NULL,NULL,NULL,NULL,NULL,NULL);

UPDATE T408_Dhr SET C5052_Location_Id = NULL, c408_last_updated_by = 3178092, c408_last_updated_date = CURRENT_DATE WHERE C408_Status_Fl = 4 AND C408_Void_Fl IS NULL AND C5052_Location_Id = '108197';
