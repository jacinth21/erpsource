/*
FileName : gm_pkg_mnf_app_main.bdy
Author : Jeeva Balaraman
Copyright : Globus Medical Inc 
*/

CREATE OR REPLACE
PACKAGE BODY gm_pkg_mnf_app_main
IS
 /*********************************************************************************************************************
  * Description : Master Procedure to Fetch product moved to Finished goods after clearing location
  * Author : Jeeva Balaraman
  *********************************************************************************************************************/   
 PROCEDURE gm_process_app_main
AS
v_last_sync_date t9300_job.C9300_START_DATE%TYPE:= NULL;
v_job_id   		 t9300_job.C9300_JOB_ID%TYPE:= 1060;
v_user_id  		 t9300_job.C9300_LAST_UPDATED_BY%TYPE:= 30301;

BEGIN
	  --fetch the last sync date and update startdate as currentdate of job.
	  gm_pkg_device_job_run_date.gm_update_job_details(v_job_id,v_user_id,v_last_sync_date);
	  
	  --Process the Clear Locations for the Manf DHR after processing .
	  gm_pkg_mnf_app_txn.gm_update_dhr_location_id(v_last_sync_date,v_user_id);
	  	  
	  --update job end date.
	  gm_pkg_device_tag_sync_master.gm_update_end_date_job(v_job_id,v_user_id);
  
 END gm_process_app_main;

END gm_pkg_mnf_app_main;
/ 