-- Synonyms

CREATE OR REPLACE  public synonym gm_pkg_mnf_app_main FOR GLOBUS_APP.gm_pkg_mnf_app_main;
CREATE OR REPLACE  public synonym gm_pkg_mnf_app_txn FOR GLOBUS_APP.gm_pkg_mnf_app_txn;