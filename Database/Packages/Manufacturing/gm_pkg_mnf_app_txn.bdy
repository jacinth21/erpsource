-- @"C:\database\Data Correction\Script\gm_pkg_mnf_app_txn.bdy"
CREATE OR REPLACE PACKAGE BODY gm_pkg_mnf_app_txn
IS
    --
/*************************************************************************
 * Description : This procedure is used to product moved to Finished goods after clearing location
 * Author : Jeeva Balaraman
 *  **************************************************************************/  
 PROCEDURE gm_update_dhr_location_id(
				p_last_sync_date 		IN 	 t408_dhr.c408_last_updated_date%TYPE,
	    	 	p_user_id       		IN 	 VARCHAR2
 )
AS
v_count          	 number;
v_location_id 		 t408_dhr.C5052_Location_Id%TYPE;

CURSOR location_info 
  IS	
	SELECT t408.c408_dhr_id dhr_id 
	FROM t408_dhr t408
	WHERE t408.C5052_Location_Id IN (v_location_id) --108197 - location id
	AND t408.C408_Void_Fl IS NULL 
	AND t408.C408_Status_Fl = 4 
	AND t408.c408_last_updated_date >= NVL(p_last_sync_date,c408_last_updated_date);

BEGIN
	v_location_id 	:= get_rule_value ('LOCATION_MANF_DHR', 'LOCATION_DHR');
	
 FOR casedtl IN location_info
  LOOP
	BEGIN
	    SELECT COUNT(1) 
		  INTO v_count 
		FROM T412_Inhouse_Transactions T412
		WHERE T412.C412_Ref_Id = casedtl.dhr_id 
		AND T412.C412_Status_Fl = 4
		AND T412.c412_void_fl IS NULL;
		
	  IF v_count > 0 THEN
		UPDATE T408_Dhr 
		SET c5052_Location_Id		 =  NULL,
			c408_last_updated_by 	 =  p_user_id,
			c408_last_updated_date   =  CURRENT_DATE
		WHERE C408_Dhr_Id = casedtl.dhr_id
		AND C408_Status_Fl = 4
		AND C408_Void_Fl IS NULL;
	  END IF;			   
  END;
END LOOP;
			
 END gm_update_dhr_location_id; 	

END gm_pkg_mnf_app_txn;
/
