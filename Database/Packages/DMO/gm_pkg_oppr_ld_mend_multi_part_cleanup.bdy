
CREATE OR REPLACE PACKAGE BODY gm_pkg_oppr_ld_mend_multi_part_cleanup
IS
/*****************************************************************************
 * PC-3207 : Automate Multi Part flag Enable and Disable
 * Description : Procedure to used to fetch the multi part number id to update 
 * 				crossover fl as 'Y' 
 * Author      : Agilan Singaravel
 ******************************************************************************/
PROCEDURE gm_add_multi_part_flag(
	p_user_id		IN		t101_user.c101_user_id%type
)
AS
v_count		NUMBER;
cursor add_multi_part_cur
IS
	SELECT c205_part_number_id part_num
	  FROM (
	SELECT t4011.c205_part_number_id, 
	  	   count(t4010.c4010_group_id) grp_count 
	  FROM T4011_GROUP_DETAIL T4011, T205_PART_NUMBER T205, T4010_GROUP T4010
	 WHERE t4010.c4010_group_id = t4011.c4010_group_id
	   AND t4011.c205_part_number_id = t205.c205_part_number_id
	   AND t205.c205_crossover_fl IS NULL
	   AND t4010.c4010_void_fl IS NULL
	   AND t4010.c901_type = 40045
  GROUP BY t4011.c205_part_number_id
	HAVING count(t4010.c4010_group_id)>=2);
	
BEGIN
	FOR add_multi_part_det IN add_multi_part_cur
	LOOP
		SELECT COUNT(1) 
		  INTO v_count
		  FROM (
		SELECT t4020.C4020_DEMAND_MASTER_ID
		  FROM GLOBUS_DM_OPERATION.T4021_DEMAND_MAPPING T4021, T4010_GROUP T4010,T4011_GROUP_DETAIL T4011,
			   GLOBUS_DM_OPERATION.T4020_DEMAND_MASTER T4020, T205_PART_NUMBER T205
		 WHERE t4021.c4021_ref_id = t4010.c4010_group_id
		   AND t4010.c4010_group_id=t4011.c4010_group_id
		   AND t4021.c901_ref_type = 40030
		   AND t4010.C4010_VOID_FL is null
	       AND t4020.C4020_DEMAND_MASTER_ID= t4021.C4020_DEMAND_MASTER_ID
		   AND t4020.C4020_PARENT_DEMAND_MASTER_ID is null
		   AND t4020.C4020_VOID_FL IS NULL
		   AND t4011.C205_PART_NUMBER_ID=T205.C205_PART_NUMBER_ID
		   AND t4010.c4010_void_fl IS NULL
		   AND t4010.c901_type = '40045'
		   AND T205.C205_PART_NUMBER_ID = add_multi_part_det.part_num
		   AND C205_CROSSOVER_FL IS NULL
	  GROUP BY t4020.C4020_DEMAND_MASTER_ID);
	  
	  IF v_count >= 2 THEN
	  	 gm_upd_part_multi_flag(add_multi_part_det.part_num,'Y',p_user_id);
	  END IF;
	  
	END LOOP;
	
END gm_add_multi_part_flag;

/*****************************************************************************
 * PC-3207 : Automate Multi Part flag Enable and Disable
 * Description : Procedure to used to update crossover fl to t205_part_number table
 * Author      : Agilan Singaravel
 ******************************************************************************/
PROCEDURE gm_upd_part_multi_flag(
	p_part_num		IN		t205_part_number.c205_part_number_id%type,
	p_cross_fl		IN		t205_part_number.c205_crossover_fl%type,
	p_user_id		IN		t101_user.c101_user_id%type
)
AS
BEGIN
	UPDATE T205_PART_NUMBER
	   SET c205_crossover_fl = p_cross_fl
	     , c205_last_updated_by = p_user_id
	     , c205_last_updated_date = current_date
	 WHERE c205_part_number_id = p_part_num;
END gm_upd_part_multi_flag;

/******************************************************************************************
 * PC-3207 : Automate Multi Part flag Enable and Disable
 * Description : Procedure to used to remove crossover fl where part num is not a multi part
 * Author      : Agilan Singaravel
 *******************************************************************************************/
PROCEDURE gm_remove_multi_part_flag(
	p_user_id		IN		t101_user.c101_user_id%type
)
AS
v_count		NUMBER;
cursor remove_multi_part_cur
IS
	    SELECT part_num
		  FROM (SELECT t4011.C205_PART_NUMBER_ID part_num, count(t4010.C4010_GROUP_ID) grp_count 
				  FROM t4011_group_detail t4011,t205_part_number t205,t4010_group t4010
				 WHERE t4010.C4010_GROUP_ID = t4011.C4010_GROUP_ID
				   AND t4011.C205_PART_NUMBER_ID=t205.C205_PART_NUMBER_ID
				   AND t205.C205_CROSSOVER_FL ='Y'
				   AND t4010.c4010_void_fl IS NULL
				   AND t4010.c901_type = 40045
			  GROUP BY t4011.C205_PART_NUMBER_ID
		        HAVING count(t4010.C4010_GROUP_ID)>=1);
BEGIN
	FOR remove_multi_part_cur_det IN remove_multi_part_cur
	LOOP
		 SELECT COUNT(1) 
		  INTO v_count
		  FROM (
		SELECT t4020.C4020_DEMAND_MASTER_ID
		  FROM GLOBUS_DM_OPERATION.T4021_DEMAND_MAPPING T4021, T4010_GROUP T4010,T4011_GROUP_DETAIL T4011,
			   GLOBUS_DM_OPERATION.T4020_DEMAND_MASTER T4020, T205_PART_NUMBER T205
		 WHERE t4021.c4021_ref_id = t4010.c4010_group_id
		   AND t4010.c4010_group_id=t4011.c4010_group_id
		   AND t4021.c901_ref_type = 40030
		   AND t4010.C4010_VOID_FL is null
	       AND t4020.C4020_DEMAND_MASTER_ID= t4021.C4020_DEMAND_MASTER_ID
		   AND t4020.C4020_PARENT_DEMAND_MASTER_ID is null
		   AND t4020.C4020_VOID_FL IS NULL
		   AND t4011.C205_PART_NUMBER_ID=T205.C205_PART_NUMBER_ID
		   AND t4010.c4010_void_fl IS NULL
		   AND t4010.c901_type = '40045'
		   AND T205.C205_PART_NUMBER_ID = remove_multi_part_cur_det.part_num
		  AND C205_CROSSOVER_FL = 'Y'
	  GROUP BY t4020.C4020_DEMAND_MASTER_ID);
      
      IF v_count = 1 THEN
		gm_upd_part_multi_flag(remove_multi_part_cur_det.part_num,null,p_user_id);
      END IF;
      
	END LOOP;
	
END gm_remove_multi_part_flag;

END gm_pkg_oppr_ld_mend_multi_part_cleanup;
/

