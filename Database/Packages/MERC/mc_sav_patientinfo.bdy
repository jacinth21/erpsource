CREATE OR REPLACE PACKAGE BODY mc_pkg_post
IS
--

      PROCEDURE mc_sav_patientinfo (
  
    

      p_threadID      IN       MVNFORUMTHREAD.THREADID%TYPE,
      p_anatloc       IN       VARCHAR2,
      p_age           IN       VARCHAR2,
      p_height        IN       VARCHAR2,
      p_weight	      IN       VARCHAR2,
      p_painloc       IN       VARCHAR2,
      p_painsev       IN       VARCHAR2,
      p_symptoms      IN       VARCHAR2,
      p_injurydate    IN       VARCHAR2,
      p_injuryreason  IN       VARCHAR2,
      p_surgerydate   IN       VARCHAR2,
      p_comments      IN       VARCHAR2,
      p_sex	      IN       VARCHAR2,
      p_smoking       IN       VARCHAR2,

      p_outpatientid       OUT      VARCHAR2
    



)
--
AS
/***************************************************************************************************
   * This procedure is used to Insert / update the paient info in forum
   * Author : rgandhi
   ***************************************************************************************************/
--
BEGIN
    
    
  --  
    UPDATE T501_PATIENT_INFO m501

       SET m501.threadID = p_threadID
         , m501.c501_weight = p_weight
         , m501.c501_height = p_height
         , m501.c501_age = p_age
         , m501.c501_surgery_dt = TO_DATE ( p_surgerydate, 'MM/DD/YYYY')
	 , m501.c501_surgery_comments = p_comments
	 , m501.c501_injury_dt = TO_DATE ( p_injurydate , 'MM/DD/YYYY')
	 , m501.c901_injury_caused_by = decode (  p_injuryreason , '0', null, p_injuryreason) 
	 , m501.c501_pain_location = p_painloc
	 , m501.c901_pain_severity = decode ( p_painsev, '0', null, p_painsev)
	 , m501.c901_anatomy_location = decode( p_anatloc , '0', null, p_anatloc )
	 , m501.c501_other_symptoms = p_symptoms
	 , m501.C901_SEX = p_sex
	 , m501.C501_SMOKER_FL = p_smoking

     WHERE   	 m501.threadID = p_threadID;

  --



    IF (SQL%ROWCOUNT = 0)
    THEN
       
	--
	INSERT INTO T501_PATIENT_INFO m501
                    ( m501.C501_PATIENT_INFO_ID , m501.postid, m501.threadID, m501.c501_weight, m501.c501_height, m501.c501_age
                   , m501.c501_surgery_dt, m501.c501_surgery_comments, m501.c501_injury_dt , m501.c901_injury_caused_by , m501.c501_pain_location, m501.c901_pain_severity , m501.c901_anatomy_location , m501.c501_other_symptoms 
		   , m501.C901_SEX , m501.C501_SMOKER_FL
                    )
             VALUES ( S501_PATIENT_INFO.nextval, null,  p_threadID , p_weight, p_height, p_age
                   , TO_DATE ( p_surgerydate, 'MM/DD/YYYY') , p_comments,TO_DATE ( p_injurydate, 'MM/DD/YYYY'), 
		   decode ( p_injuryreason , '0', null, p_injuryreason ), p_painloc, 
		   decode ( p_painsev , '0', null, p_painsev ), 
		   decode ( p_anatloc, '0', null, p_anatloc  ), p_symptoms , p_sex , p_smoking
                    );
	--
    
    END IF;
END mc_sav_patientinfo;

END  mc_pkg_post;
/
