--@"C:\Database\Packages\Operations\gm_pkg_op_cpc_txn.bdy";

create or replace
PACKAGE BODY gm_pkg_op_cpc_txn
IS
--

	/*********************************************************
	* Description : This procedure inserts/updates CPC info
	* 
	*********************************************************/
	PROCEDURE gm_upd_cpc_data (
	  	p_user_id             IN t101_user.c101_user_id%TYPE 
    , p_cpc_id 	            IN OUT t704b_account_cpc_mapping.c704b_account_cpc_map_id%TYPE
	  , p_cpc_name						IN t704b_account_cpc_mapping.c704b_cpc_name%TYPE 
	  , p_cpc_address					IN t704b_account_cpc_mapping.c704b_cpc_address%TYPE
		, p_cpc_barcode_lbl_ver	IN t704b_account_cpc_mapping.c704b_cpc_barcode_lbl_ver%TYPE
		, p_cpc_final_cpc_ver		IN t704b_account_cpc_mapping.c704b_cpc_final_cpc_ver%TYPE
		, p_active_fl					  IN VARCHAR2
	)
	AS
  	v_activefl VARCHAR2(1);
  	v_count		NUMBER := 0;
  	v_cpc_id	VARCHAR2(4000) := p_cpc_id;
  	v_company_id     NUMBER;
	v_plant_id       NUMBER;
  BEGIN
	  
  SELECT get_compid_frm_cntx(), get_plantid_frm_cntx()
  INTO v_company_id, v_plant_id 
  FROM DUAL;	  
  
  SELECT DECODE(p_active_fl,'80130','Y','80131','N','') INTO v_activefl
    FROM DUAL;

    IF v_activefl = 'N'
    THEN
    	BEGIN
		SELECT count(1)
			INTO v_count 
		FROM T205D_PART_ATTRIBUTE 
		WHERE C205D_ATTRIBUTE_VALUE = v_cpc_id 
			AND C901_ATTRIBUTE_TYPE = '103731' --Contract Processing Client
			AND C205D_VOID_FL IS NULL ;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				v_count := 0;
		END;
		
		IF v_count > 0 -- should not inactivate the CPC which is mapped to a part number
		THEN
			raise_application_error('-20661','');
		END IF;
		
    END IF;
    
		UPDATE t704b_account_cpc_mapping 
       SET c704b_cpc_address = p_cpc_address,
           c704b_cpc_name = p_cpc_name,
           c704b_cpc_barcode_lbl_ver = p_cpc_barcode_lbl_ver,
           c704b_cpc_final_cpc_ver = p_cpc_final_cpc_ver,
           c704b_active_fl = v_activefl,
           c704b_last_updated_by = p_user_id,
           c704a_last_updated_date = SYSDATE,
           c1900_company_id = v_company_id,
           c5040_plant_id   = v_plant_id
     WHERE c704b_account_cpc_map_id  = p_cpc_id
       AND  c704b_void_fl IS NULL; 

    IF SQL%rowcount = 0 THEN
    
    SELECT s704b_account_cpc_mapping.nextval INTO p_cpc_id FROM DUAL;
    
			INSERT INTO t704b_account_cpc_mapping
				(c704b_account_cpc_map_id  , c704b_cpc_name, c704b_cpc_address, c704b_cpc_barcode_lbl_ver, c704b_cpc_final_cpc_ver,c704b_active_fl                  
			   , c704b_last_updated_by, c704a_last_updated_date , c1900_company_id , c5040_plant_id
				)
			VALUES (p_cpc_id, p_cpc_name, p_cpc_address, p_cpc_barcode_lbl_ver, p_cpc_final_cpc_ver, v_activefl
                   , p_user_id, SYSDATE , v_company_id , v_plant_id);
    END IF;
  END gm_upd_cpc_data;
  
 /*********************************************************
	* Description : This procedure to void Cpc Items  
	* 
	*********************************************************/
  PROCEDURE gm_void_cpc_item(
   p_cpc_id 	    IN  t704b_account_cpc_mapping.c704b_account_cpc_map_id%TYPE,
   p_user_id      IN t101_user.c101_user_id%TYPE 
	)
  AS
  	v_count		NUMBER := 0;
  	v_cpc_id	VARCHAR2(4000) := p_cpc_id;
  	v_company_id     NUMBER;
	v_plant_id       NUMBER;
  BEGIN
	SELECT get_compid_frm_cntx(), get_plantid_frm_cntx()
      INTO v_company_id, v_plant_id 
      FROM DUAL;
	  BEGIN
		SELECT count(1)
			INTO v_count 
		FROM T205D_PART_ATTRIBUTE 
		WHERE C205D_ATTRIBUTE_VALUE = v_cpc_id 
			AND C901_ATTRIBUTE_TYPE = '103731' --Contract Processing Client
			AND C205D_VOID_FL IS NULL ;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				v_count := 0;
		END;
		
		IF v_count > 0 -- should not void the CPC which is mapped to a part number
		THEN
			raise_application_error('-20662','');
		END IF;
			
	 	UPDATE t704b_account_cpc_mapping 
	    SET c704b_void_fl = 'Y', 
	    c704b_last_updated_by = p_user_id,
	    c704a_last_updated_date = SYSDATE
	    WHERE c704b_account_cpc_map_id  = p_cpc_id
	      AND c704b_void_fl IS NULL
	      AND c1900_company_id  = v_company_id
	      AND c5040_plant_id = v_plant_id; 
  END gm_void_cpc_item; 
  
  
END gm_pkg_op_cpc_txn;
/