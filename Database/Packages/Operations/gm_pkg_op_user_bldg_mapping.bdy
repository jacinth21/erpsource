--@"C:\PMT\BitBucket_WorkSpaces\erpglobus\Database\Packages\Operations\gm_pkg_op_user_bldg_mapping.bdy";

/* Description : This is used to give access for users to building
 * PMT-34026   : User building mapping
 */
CREATE OR REPLACE PACKAGE BODY gm_pkg_op_user_bldg_mapping
IS
   /*******************************************************
   * Description : Procedure to fetch the building list 
   * PMT-34026   : User building mapping
   * Author      : N RAJA
   *******************************************************/
   PROCEDURE gm_fch_building_list (
   p_cursor OUT TYPES.cursor_type)
   AS
    v_company_id T5057_BUILDING.C1900_COMPANY_ID%TYPE;
    v_plant_id T5057_BUILDING.C5040_PLANT_ID%TYPE;
   BEGIN
  
	SELECT  get_compid_frm_cntx(),get_plantid_frm_cntx
			  INTO  v_company_id,v_plant_id
			  FROM  DUAL;
   
      OPEN p_cursor
       FOR
            SELECT T5057.C5057_BUILDING_ID BID,
                   T5057.C5057_BUILDING_NAME BNM
              FROM T5057_BUILDING T5057
             WHERE T5057.C1900_COMPANY_ID = v_company_id
               AND T5057.C5040_PLANT_ID = v_plant_id
               AND T5057.C5057_ACTIVE_FL = 'Y'
               AND T5057.C5057_VOID_FL IS NULL;
   END gm_fch_building_list;
   
   /*******************************************************
   * Description : Procedure to save the users
   * PMT-34026   : User building mapping
   * Author      : N RAJA
   *******************************************************/
  PROCEDURE gm_upd_user_group_map(   
   	p_inpstr	IN	   VARCHAR2,           
   	p_id		IN 	   VARCHAR2,          
   	p_compid	IN	   t1900_company.C1900_COMPANY_ID%TYPE,
    p_building  IN OUT  t1501_group_mapping.c5057_building_id%TYPE
   )
   AS
   	v_inpstr	VARCHAR2(2000) := p_inpstr;
   BEGIN
	v_inpstr := substr(v_inpstr,0,length(v_inpstr)-1);
	my_context.set_my_inlist_ctx (v_inpstr);

		
		UPDATE T1501_GROUP_MAPPING T1501
           SET T1501.C5057_BUILDING_ID = NULL
         WHERE T1501.C1500_GROUP_ID IN ( SELECT C902_CODE_NM_ALT CODEID    
                                  FROM T901_CODE_LOOKUP T901 
                                 WHERE T901.C901_CODE_GRP = 'BLDGRP'
                                  AND T901.C901_ACTIVE_FL =1
                                  AND T901.C901_VOID_FL IS NULL
                                  )
		  AND T1501.C1900_COMPANY_ID = p_compid
		  AND T1501.C5057_BUILDING_ID <> p_building
		  AND T1501.C101_MAPPED_PARTY_ID IN (SELECT TOKEN FROM v_in_list)
		  AND T1501.C1501_VOID_FL IS NULL;
  
		UPDATE T1501_GROUP_MAPPING T1501
		   SET T1501.C5057_BUILDING_ID = NULL
		 WHERE T1501.C1500_GROUP_ID = p_id
		   AND T1501.C1900_COMPANY_ID = p_compid
		   AND T1501.C5057_BUILDING_ID = p_building
		   AND T1501.C1501_VOID_FL IS NULL;
  
	  UPDATE T1501_GROUP_MAPPING T1501
	     SET T1501.C5057_BUILDING_ID = p_building
	   WHERE T1501.C1500_GROUP_ID = p_id
	     AND T1501.C1900_COMPANY_ID = p_compid
	     AND T1501.C101_MAPPED_PARTY_ID IN (SELECT TOKEN FROM v_in_list)
	     AND T1501.C1501_VOID_FL IS NULL;
	     
  END gm_upd_user_group_map;
   
   /*******************************************************
   * Description : Procedure to fetch the User List based on group, company and building
   * PMT-34026   : User building mapping
   * Author      : N RAJA 
   *******************************************************/
   PROCEDURE gm_fch_selected_user_group (
      p_grp_id            IN       t1501_group_mapping.c1500_group_id%TYPE,
      p_mapping_user_id   IN       t1501_group_mapping.c101_mapped_party_id%TYPE,
      p_user_id           IN       t1501_group_mapping.c1501_created_by%TYPE,
      p_grp_mapping_id    IN       VARCHAR2,
      p_dept              IN       VARCHAR2,
      p_compid			  IN	   t1900_company.C1900_COMPANY_ID%TYPE,
      p_build_id          IN       t1501_group_mapping.c5057_building_id%TYPE,
      p_cursor            OUT      TYPES.cursor_type
   )
   AS
   v_comp_lang_id T1900_COMPANY.C901_LANGUAGE_ID%TYPE;
   BEGIN  
   	   SELECT get_complangid_frm_cntx() 
	    INTO v_comp_lang_id 
	    FROM DUAL;
	    

	    
       OPEN p_cursor
       FOR
     SELECT t1501.c101_mapped_party_id PARTYID,
                    pname USERNAME         
              FROM (SELECT t101p.c101_party_id, t101p.c1500_group_id,
                           TO_NUMBER (SUBSTR (c1500_group_id, 0, 4)) dept,
                   TRIM (DECODE (DECODE(v_comp_lang_id,'103097',NVL(C101_party_nm_en,C101_PARTY_NM),C101_PARTY_NM),
                               NULL,DECODE (c101_first_nm,NULL, '', ' ' || c101_first_nm)||' '||c101_last_nm || DECODE (c101_middle_initial,
                               NULL, '',' ' || c101_middle_initial),DECODE(v_comp_lang_id,'103097',NVL(C101_party_nm_en,C101_PARTY_NM),C101_PARTY_NM)
                                                 )
                                     ) pname,
                           t102.c102_login_username loginusernm
                      FROM t101_party t101p, t101_user t101, t102_user_login t102
                     WHERE t101p.c101_void_fl IS NULL
                       AND t101p.c101_party_id = t101.c101_party_id
                       AND t101.c101_user_id = t102.c101_user_id
                       ) t101p,
                   t1501_group_mapping t1501,
                   t1500_group t1500
             WHERE t101p.c101_party_id = t1501.c101_mapped_party_id(+)
               AND t1500.c1500_group_id = t1501.c1500_group_id
               AND t1501.c1501_group_mapping_id =
                          NVL (p_grp_mapping_id, t1501.c1501_group_mapping_id)
               AND t1501.c101_mapped_party_id =
                           NVL (p_user_id, t1501.c101_mapped_party_id)
               AND t1501.c1500_group_id = NVL (p_grp_id, t1501.c1500_group_id)
               AND NVL (dept, -999) = NVL (p_dept, NVL (dept, -999))
               AND NVL(t1501.c1900_company_id,-999) = NVL (p_compid, NVL(t1501.c1900_company_id,-999))
                 AND NVL(t1501. c5057_building_id,-999) = NVL (p_build_id, NVL(t1501. c5057_building_id,-999))              
               AND t1501.c1501_void_fl IS NULL
               AND t1500.c1500_void_fl IS NULL
               AND t1500.c901_group_type = 92264
             ORDER BY USERNAME;
    END gm_fch_selected_user_group;
END gm_pkg_op_user_bldg_mapping;
/