/* Formatted on 2010/10/13 17:27 (Formatter Plus v4.8.0) */
--	@"c:\database\packages\operations\gm_pkg_op_reprocess.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_reprocess
IS
--
 /*******************************************************
 * Purpose: This Procedure is used to fetch the
 * parts to reprocess for the given RA's and set ID
 *******************************************************/
--
   PROCEDURE gm_op_rpt_reprocess (
      p_ra_ids     IN       VARCHAR,
      p_setid      IN       t207_set_master.c207_set_id%TYPE,
      p_part_cur   OUT      TYPES.cursor_type
   )
--
   AS
      v_cnt   NUMBER;
   BEGIN
      my_context.set_my_inlist_ctx (p_ra_ids);

      -- gm_procedure_log (' p_ra_ids upp  ', p_ra_ids);
      SELECT COUNT (c506_rma_id)
        INTO v_cnt
        FROM t506_returns
       WHERE c506_rma_id IN (SELECT token
                               FROM v_in_list)
         AND (c506_status_fl <> '2' OR c501_reprocess_date IS NOT NULL);

      IF v_cnt > 0
      THEN
         raise_application_error (-20942, '');
      END IF;

      OPEN p_part_cur
       FOR
          SELECT   t507.c205_part_number_id pnum,
                   get_partnum_desc (t507.c205_part_number_id) pdesc,
                   get_set_partnum_qty_return (c506_rma_id,
                                               t507.c205_part_number_id
                                              ) qty,
                   c507_item_qty iqty, c507_control_number cnum,
                   c507_item_price price, c901_type itype,
                   DECODE (t208.c207_set_id, NULL, 'N', 'Y') issetfl,
                   get_set_qty (t208.c207_set_id,
                                t507.c205_part_number_id
                               ) set_qty,
                   get_part_attribute_value
                                           (t507.c205_part_number_id,
                                            92340
                                           ) tagable
                  ,DECODE(GET_PART_PROD_CLASS(   t507.c205_part_number_id),'4030','Y','N') PARTCLASS --Get Part Class to ReturnProcess.jsp
              FROM t507_returns_item t507, t208_set_details t208
             WHERE c506_rma_id IN (SELECT token
                                     FROM v_in_list)
               AND t507.c205_part_number_id = t208.c205_part_number_id(+)
               AND t208.c207_set_id(+) = p_setid
               AND t208.c208_void_fl(+) IS NULL
               AND t507.c507_status_fl = 'C'
          ORDER BY issetfl DESC, pnum;
   END gm_op_rpt_reprocess;

--
/*******************************************************
 * Purpose: This Procedure is used to save the
 * reprocess returns
 *******************************************************/
--
   PROCEDURE gm_save_reprocess_returns (
      p_raid         IN       t506_returns.c506_rma_id%TYPE,
      p_atch_raids   IN       VARCHAR2,
      p_cnstr        IN       VARCHAR2,
      p_qnstr        IN       VARCHAR2,
      p_pnstr        IN       VARCHAR2,
      p_set_id       IN       t506_returns.c207_set_id%TYPE,
      p_userid       IN       t506_returns.c506_last_updated_by%TYPE,
      p_request_id   IN       t520_request.c520_request_id%TYPE,
      p_comments	 IN	      t504_consignment.c504_comments%TYPE,
      p_ihstr		 IN		  VARCHAR2,
      p_iastr		 IN		  VARCHAR2,
      p_returnids    OUT      VARCHAR2,
      p_errmsg       OUT      VARCHAR2,
      p_sterile		IN        varchar2 default null, --Add p_sterile for split with default null value
      p_fgstr		IN        varchar2 default null -- Add p_fgstr for RHFG Transaction with default null value
   )
   AS
--
      v_id                VARCHAR2 (20);
      v_set_id            VARCHAR2 (20);
      v_reprocess_count   NUMBER;
      v_request_id        t520_request.c520_request_id%TYPE   := p_request_id;
      e_others            EXCEPTION;
      v520_status_fl      VARCHAR2 (20);
      v_shipto            t907_shipping_info.c901_ship_to%TYPE;
      v_shiptoid          t907_shipping_info.c907_ship_to_id%TYPE;
      v_shipid            VARCHAR2 (20);
      v_type              t520_request.c520_request_for%TYPE;
      v_ret_type		  t506_returns.C506_type%TYPE;
	  v_parent_raid  	  t506_returns.c506_rma_id%TYPE;
   BEGIN
--
      my_context.set_my_inlist_ctx (p_atch_raids);

      SELECT     c207_set_id, c506_parent_rma_id
            INTO v_set_id, v_parent_raid
            FROM t506_returns
           WHERE c506_rma_id = p_raid
      FOR UPDATE;

      SELECT COUNT (1)
        INTO v_reprocess_count
        FROM t506_returns
       WHERE c506_rma_id IN (SELECT token
                               FROM v_in_list)
         AND c506_void_fl IS NULL
         AND c501_reprocess_date IS NOT NULL;

      IF v_reprocess_count > 0
      THEN
         raise_application_error (-20943, '');
      -- ORA20063 = RA already reprocessed cant process again.
      END IF;

      IF p_cnstr IS NOT NULL
      THEN
         --
       

         IF p_request_id IS NULL
         THEN
            gm_pkg_op_request_master.gm_sav_request
                                                   (NULL,
                                                    TRUNC (LAST_DAY (CURRENT_DATE)),
                                                    '50617',
                                                    NULL,
                                                    p_set_id,
                                                    40021,		--new request from reprocess return , default to consignment 40021
                                                    NULL,
                                                    NULL,
                                                    NULL,
                                                    NULL,
                                                    NULL,
                                                    NULL,
                                                    15,
                                                    p_userid,
                                                    NULL,
                                                    v_request_id
                                                   );
         ELSE
            SELECT     c520_status_fl, c520_request_for
                  INTO v520_status_fl, v_type
                  FROM t520_request t520
                 WHERE c520_request_id = p_request_id
            FOR UPDATE;

            IF v520_status_fl = 10
            THEN
               UPDATE t520_request
                  SET c520_status_fl = 15
                    , c520_last_updated_by = p_userid
                    , c520_last_updated_date = CURRENT_DATE
                WHERE c520_request_id = p_request_id;
            ELSE
               raise_application_error (-20944, '');
            -- ORA20063 = RA already reprocessed cant process again.
            END IF;
         END IF;

         IF v_type = 40021
         THEN
            v_type := 4110; -- consigment
         ELSIF v_type = 40022
         THEN
            v_type := 4112; --inhouse consignment
         ELSE
            v_type := NULL;
         END IF;
         --         
           gm_pkg_op_request_master.gm_sav_consignment (	NULL, NULL, NULL, NULL, NULL, '1', 
           													NULL, NULL, NULL, p_set_id, NULL, NULL,
															NULL, v_type, NULL, NULL, NULL, NULL, '0', 
															NULL, NULL, NULL, p_raid, NULL, v_request_id, 
															p_userid, v_id
														);

         --
         gm_op_sav_rtn_reprocess_item (v_id, p_cnstr, p_userid, p_errmsg);
         --
         gm_op_sav_rtn_reprocess_part (v_id, 'SET-REPROCESS', p_userid);
         --
         gm_op_sav_rtn_request_details (v_request_id,
                                        p_cnstr,
                                        v_set_id,
                                        p_userid
                                       );

         --
         --Fetch the shipping details from the CN id created and enter a shipping record
         SELECT c504_ship_to shipto,
                DECODE (c504_ship_to_id,
                        NULL, c701_distributor_id,
                        c504_ship_to_id
                       ) shiptoid
           INTO v_shipto,
                v_shiptoid
           FROM t504_consignment
          WHERE c504_consignment_id = v_id;

         gm_pkg_cm_shipping_trans.gm_sav_ship_info (v_id,
                                                    50181,
                                                    v_shipto,
                                                    v_shiptoid,
                                                    5001,
                                                    5008,
                                                    NULL,
                                                    NULL,
                                                    NULL,
                                                    p_userid,
                                                    v_shipid
                                                   );
         p_returnids := p_returnids || ',' || v_id;
      END IF;

      --
      IF p_qnstr IS NOT NULL
      THEN
         --
   /*      gm_sav_consignment(
					p_distributor_id,p_ship_date,p_return_date,p_comments,p_total_cost,
					p_status_fl,p_void_fl,p_account_id,p_ship_to,p_set_id,p_ship_to_id
					p_final_comments,p_inhouse_purpose,p_type,p_delivery_carrier,p_delivery_mode
					p_tracking_number,p_ship_req_fl,p_verify_fl,p_verified_date,p_verified_by,p_update_inv_fl
					p_reprocess_id,p_master_consignment_id,p_request_id,p_created_by,p_out_consignment_id
			)*/
 
              gm_pkg_op_request_master.gm_sav_consignment (	NULL, NULL, NULL, p_comments, NULL, '3', 
   															NULL, '01', 4123, NULL, p_userid, NULL,
															'4151', '400064', NULL, NULL, NULL, '1', NULL, 
															NULL, NULL, NULL, p_raid, NULL, NULL, 
															p_userid, v_id
														);
         --
         gm_op_sav_rtn_reprocess_item (v_id, p_qnstr, p_userid, p_errmsg);
         --
         gm_op_sav_rtn_reprocess_part (v_id, 'QUAR-REPROCESS', p_userid);
         --
         p_returnids := p_returnids || ',' || v_id;
      END IF;

      /*For p_sterile str for split quarantine*/
        IF p_sterile IS NOT NULL
      THEN
         --
   /*      gm_sav_consignment(
					p_distributor_id,p_ship_date,p_return_date,p_comments,p_total_cost,
					p_status_fl,p_void_fl,p_account_id,p_ship_to,p_set_id,p_ship_to_id
					p_final_comments,p_inhouse_purpose,p_type,p_delivery_carrier,p_delivery_mode
					p_tracking_number,p_ship_req_fl,p_verify_fl,p_verified_date,p_verified_by,p_update_inv_fl
					p_reprocess_id,p_master_consignment_id,p_request_id,p_created_by,p_out_consignment_id
			)*/
 
              gm_pkg_op_request_master.gm_sav_consignment (	NULL, NULL, NULL, p_comments, NULL, '3', 
   															NULL, '01', 4123, NULL, p_userid, NULL,
															'4151', '400064', NULL, NULL, NULL, '1', NULL, 
															NULL, NULL, NULL, p_raid, NULL, NULL, 
															p_userid, v_id
														);
         --
         gm_op_sav_rtn_reprocess_item (v_id, p_sterile, p_userid, p_errmsg);
         --
         gm_op_sav_rtn_reprocess_part (v_id, 'QUAR-REPROCESS', p_userid);
         --
         p_returnids := p_returnids || ',' || v_id;
      END IF;
      
      
      
      
      
      --
      IF p_pnstr IS NOT NULL
      THEN
         --
            gm_pkg_op_request_master.gm_sav_consignment (	NULL, NULL, NULL, p_comments, NULL, '3', 
   															NULL, '01', 4123, NULL, p_userid, NULL,
															'4140', '400058', NULL, NULL, NULL, '1', NULL, 
															NULL, NULL, NULL, p_raid, NULL, NULL, 
															p_userid, v_id
														);
               --
         gm_op_sav_rtn_reprocess_item (v_id, p_pnstr, p_userid, p_errmsg);
         --
         -- Commented on 02/01/11 : Rajesh . As we will add/remove when we verify the RPN transaction.
         --gm_op_sav_rtn_reprocess_part (v_id, 'PACK-REPROCESS', p_userid);
         --
         p_returnids := p_returnids || ',' || v_id;
      END IF;
       
      --PC-4788 - to create RHFG Transaction
      IF  p_fgstr IS NOT NULL
      THEN
         -- 
         --111641- Returns to FG , code group- CONFG ,     111640 - Returns Hold to FG ,Code grp- CONTY
            gm_pkg_op_request_master.gm_sav_consignment (	NULL, NULL, NULL, p_comments, NULL, '3', 
   															NULL, '01', 4123, NULL, p_userid, NULL,
															'111641', '111640', NULL, NULL, NULL, '1', NULL, 
															NULL, NULL, NULL, p_raid, NULL, NULL, 
															p_userid, v_id
														);
               --
         gm_op_sav_rtn_reprocess_item (v_id, p_fgstr, p_userid, p_errmsg);
       
         p_returnids := p_returnids || ',' || v_id;
      END IF;
      BEGIN
      		SELECT C506_TYPE INTO v_ret_type 
      			FROM T506_RETURNS
      				WHERE C506_RMA_ID = p_raid
      					AND C506_VOID_FL IS NULL;
	  EXCEPTION WHEN NO_DATA_FOUND
	  THEN
	  	v_ret_type := NULL;
	  END;
      IF p_ihstr IS NOT NULL
      THEN
      		
      		gm_pkg_op_request_master.gm_sav_inhouse_txn(
      					p_comments,
      					3,
      					100331, -- Transaction Purpose
      					9109,	-- Returns Hold to IH Inv
      					p_raid,
      					v_ret_type,
      					NULL,
      					NULL,
      					p_userid,
      					v_id
      				);
      				
			gm_pkg_op_request_master.gm_sav_inhouse_txn_string(
				v_id,
				p_ihstr,
				p_userid
				);
      			
			p_returnids := p_returnids || ',' || v_id;
      END IF;
      
       IF p_iastr IS NOT NULL
      THEN
      		
      		gm_pkg_op_request_master.gm_sav_inhouse_txn(
      					p_comments,
      					3,
      					100331,	-- Transaction Purpose
      					9114,	-- Returns Hold to Inv Adjustment
      					p_raid,
      					v_ret_type,
      					NULL,
      					NULL,
      					p_userid,
      					v_id
      				);
      				
			gm_pkg_op_request_master.gm_sav_inhouse_txn_string(
				v_id,
				p_iastr,
				p_userid
				);
      			
			p_returnids := p_returnids || ',' || v_id;
      END IF;

      --
      UPDATE t506_returns
         SET c501_reprocess_date = CURRENT_DATE
           , c506_last_updated_by = p_userid
		   , c506_last_updated_date = CURRENT_DATE
       WHERE c506_rma_id IN (SELECT token
                               FROM v_in_list) AND c506_void_fl IS NULL;
                               
	    --When ever the child RA is updated, the parent RA in Parent table has to be updated ,so ETL can Sync it to GOP.
		IF v_parent_raid IS NOT NULL
		THEN
      		UPDATE t506_returns
		   	   SET C506_LAST_UPDATED_BY = p_userid, 
		           C506_LAST_UPDATED_DATE = CURRENT_DATE
			 WHERE C506_rma_Id = v_parent_raid;
		END IF;
      -- Save "Return" status: 91153 Reprocessed
      gm_save_status_details (p_raid,
                              ' ',
                              p_userid,
                              NULL,
                              CURRENT_DATE,
                              '91153',  -- Reprocessed
                              '91100'	-- Returns
                             );
   END gm_save_reprocess_returns;

--
/*******************************************************
 * Purpose: This Procedure is used to save the
 * reprocess item returns
 *******************************************************/
--
   PROCEDURE gm_op_sav_rtn_reprocess_item (
      p_id       IN       t504_consignment.c504_consignment_id%TYPE,
      p_str      IN       VARCHAR2,
      p_userid   IN       t506_returns.c506_last_updated_by%TYPE,
      p_errmsg   OUT      VARCHAR2
   )
   AS
--
      v_strlen      NUMBER                         := NVL (LENGTH (p_str), 0);
      v_string      VARCHAR2 (30000)                          := p_str;
      v_pnum        VARCHAR2 (20);
      v_qty         NUMBER (4, 2);
      v_control     t505_item_consignment.c505_control_number%TYPE;
      v_substring   VARCHAR2 (1000);
      v_tag_part    t5011_tag_log.c5010_tag_id%TYPE;
      v_tag_flag    VARCHAR2 (3);
      v_count       NUMBER;
      v_msg         VARCHAR2 (1000);
      e_others      EXCEPTION;
      v_ra_id       t504_consignment.c504_reprocess_id%TYPE;
      v_status_fl   t504_consignment.c504_status_fl%TYPE;
      v_tag_count   NUMBER;
      v_type        t504_consignment.c504_type%TYPE;
      v_tag_id      t5010_tag.c5010_tag_id%TYPE;
      v_set_id      t504_consignment.c207_set_id%TYPE;
   --
--
   BEGIN
--
      v_count := 0;

      IF v_strlen > 0
      THEN
         SELECT t504.c504_reprocess_id, t504.c504_type, c207_set_id,
                c504_status_fl
           INTO v_ra_id, v_type, v_set_id,
                v_status_fl
           FROM t504_consignment t504
          WHERE c504_consignment_id = p_id AND t504.c504_void_fl IS NULL;

         --
         WHILE INSTR (v_string, '|') <> 0
         LOOP
            --
            v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
            v_string := SUBSTR (v_string, INSTR (v_string, '|') + 1);
            v_pnum := NULL;
            v_control := NULL;
            v_qty := NULL;
            v_pnum := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
            v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
            v_control := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
            v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
            v_qty := TO_NUMBER (v_substring);
	
            --
            INSERT INTO t505_item_consignment
                        (c505_item_consignment_id, c504_consignment_id,
                         c205_part_number_id, c505_control_number,
                         c505_item_qty, c505_item_price
                        )
                 VALUES (s504_consign_item.NEXTVAL, p_id,
                         v_pnum, v_control,
                         v_qty, TO_NUMBER (get_part_price (v_pnum, 'E'))
                        );

            SELECT get_part_attribute_value (v_pnum, 92340)
              INTO v_tag_flag
              FROM DUAL;

            IF (v_tag_flag = 'Y' AND v_status_fl = '1')
            THEN
               v_tag_part := v_pnum;

               SELECT COUNT (t5010.c5010_tag_id)
                 INTO v_tag_count
                 FROM t5010_tag t5010
                WHERE t5010.c5010_last_updated_trans_id = v_ra_id
                  AND t5010.c205_part_number_id = v_pnum
                  AND t5010.c5010_control_number = v_control
                  AND t5010.c5010_void_fl IS NULL;

               IF (v_tag_count = 1)
               THEN
                  SELECT t5010.c5010_tag_id
                    INTO v_tag_id
                    FROM t5010_tag t5010
                   WHERE t5010.c5010_last_updated_trans_id = v_ra_id
                     AND t5010.c205_part_number_id = v_pnum
                     AND t5010.c5010_control_number = v_control
                     AND t5010.c5010_void_fl IS NULL;

                  /* IF (v_type = '4113')
                     THEN
                        v_location_id := 'Quarantine';
                     ELSIF (v_type = '4114')
                     THEN
                        v_location_id := 'Packing';
                     ELSE
                        v_location_id := '40033';
                     END IF;*/
                  UPDATE t5010_tag
                     SET c5010_control_number = v_control,
                         c205_part_number_id = v_pnum,
                         c207_set_id = v_set_id,
                         c901_trans_type = '51000',
                         c5010_last_updated_trans_id = p_id,
                         c901_location_type = '40033',
                         c5010_location_id = '52098',
                         c901_status = 51012,
                         c5010_last_updated_by = p_userid,
                         c5010_last_updated_date = CURRENT_DATE
                   WHERE c5010_tag_id = v_tag_id;
               END IF;
            END IF;
         END LOOP;
         
         --Whenever the detail is inserted, the consignment table has to be updated ,so ETL can Sync it to GOP.
		 UPDATE t504_consignment
		    SET c504_last_updated_by = p_userid
		      , c504_last_updated_date = CURRENT_DATE
		  WHERE c504_consignment_id = p_id;
      --
      ELSE
         p_errmsg := 'Empty String';
      END IF;
--
   END gm_op_sav_rtn_reprocess_item;

--
/*******************************************************
 * Purpose: This Procedure is used to save the
 * reprocess part returns
 *******************************************************/
--
   PROCEDURE gm_op_sav_rtn_reprocess_part (
      p_id       IN   VARCHAR2,
      p_type     IN   VARCHAR2,
      p_userid   IN   t101_user.c101_user_id%TYPE
   )
--
   AS
--
      v_stock      NUMBER;
      v_newstock   NUMBER;
      v_party_id   VARCHAR2 (20);
      v_set_id     VARCHAR2 (20);
      v_purpose    VARCHAR2 (20);
      v_type       VARCHAR2 (20);
      v_post_id    VARCHAR2 (20);
      v_dist_id    VARCHAR2 (20);
      v_acc_id     VARCHAR2 (20);
      v_reason     VARCHAR2 (20);

--
      CURSOR cur_consign
      IS
         SELECT   c205_part_number_id ID, SUM (c505_item_qty) qty,
                  c505_item_price price
             FROM t505_item_consignment
            WHERE c504_consignment_id = p_id
              AND TRIM (c505_control_number) IS NOT NULL
         GROUP BY c205_part_number_id, c505_item_price;
--
   BEGIN
      --
      IF p_type = 'SET-REPROCESS'
      THEN
         --
         FOR rad_val IN cur_consign
         LOOP
            --
            gm_cm_sav_partqty (rad_val.ID,
                               (rad_val.qty * -1),
                               p_id,
                               p_userid,
                               90812, -- returns_hold  Qty
                               4302, -- Minus
                               4311 -- Consignment
                               );

           gm_cm_sav_partqty (rad_val.ID,
                               rad_val.qty,
                               p_id,
                               p_userid,
                               90814, -- bulk  Qty
                               4301, -- Plus
                               4311 -- Consignment
                               );
         --
         END LOOP;

      END IF;
--
   END gm_op_sav_rtn_reprocess_part;

--
/*******************************************************
 * Purpose: This Procedure is used to fetch the requests
 *       that are in back order which are associated
 *       to the given set
 * Author: VPrasath
 *******************************************************/
--
   PROCEDURE gm_op_fch_request_for_set (
      p_set_id        IN       t506_returns.c207_set_id%TYPE,
      p_request_cur   OUT      TYPES.cursor_type
   )
--
   AS
   v_comp_dtfmt 		VARCHAR2(100);
   v_compid_frm_cntx	t1900_company.c1900_company_id%TYPE;
   BEGIN
   
   		
		SELECT get_compdtfmt_frm_cntx(), get_compid_frm_cntx()
          INTO v_comp_dtfmt, v_compid_frm_cntx
          FROM DUAL;

   
      OPEN p_request_cur
       FOR
          SELECT   '' ID, c520_request_id request_id,
                   get_code_name (c520_request_for) TYPE,
                   TO_CHAR(c520_required_date,v_comp_dtfmt) required_date,
                   get_user_name (c520_request_by) request_by,
                   get_code_name (c901_request_source) req_source
              FROM t520_request t520
             WHERE t520.c207_set_id = p_set_id
               AND t520.c520_status_fl = 10
               AND t520.c520_void_fl IS NULL
               AND t520.c520_delete_fl IS NULL
			   AND t520.c1900_company_id = v_compid_frm_cntx
               AND t520.c520_master_request_id IS NULL
          UNION
          SELECT   '' ID, 'New Request' request_id, '' TYPE, NULL required_date,
                   '' request_by, '' req_source
              FROM DUAL
          ORDER BY request_id;
   END gm_op_fch_request_for_set;

--
/*******************************************************
 * Purpose: This Procedure is used to save the
 * Requests details for the Reprocessed Set
 *******************************************************/
--
   PROCEDURE gm_op_sav_rtn_request_details (
      p_request_id   IN   t520_request.c520_request_id%TYPE,
      p_str          IN   VARCHAR2,
      p_set_id       IN   t506_returns.c207_set_id%TYPE,
      p_userid       IN   t506_returns.c506_last_updated_by%TYPE
   )
   AS
--
      v_strlen              NUMBER                 := NVL (LENGTH (p_str), 0);
      v_string              VARCHAR2 (30000)                        := p_str;
      v_pnum                VARCHAR2 (20);
      v_qty                 NUMBER (4);
      v_control             VARCHAR2 (20);
      v_substring           VARCHAR2 (1000);
      v_req_qty             NUMBER;
      v_pnum_str            VARCHAR2 (30000)                        := '';
      v_qty_str             VARCHAR2 (30000)                        := '';
      v_cnt                 NUMBER;
      v_etc_part_cnt        NUMBER;
      v_request_id          t520_request.c520_request_id%TYPE;
      v_existing_part_cnt   NUMBER;
      --
      v_request_to          t520_request.c520_request_to%TYPE;
      v_ship_to             t520_request.c901_ship_to%TYPE;
      v_ship_to_id          t520_request.c520_ship_to_id%TYPE;
      v_request_for         t520_request.c520_request_for%TYPE;
      v_purpose             t520_request.c901_purpose%TYPE;
      v_source              t520_request.c901_request_source%TYPE;
      v_txn_id              t520_request.c520_request_txn_id%TYPE;
   BEGIN
--
      SELECT c901_request_source, c520_request_txn_id
        INTO v_source, v_txn_id
        FROM t520_request
       WHERE c520_request_id = p_request_id;

      IF v_strlen > 0
      THEN
         --
         WHILE INSTR (v_string, '|') <> 0
         LOOP
            --
            v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
            v_string := SUBSTR (v_string, INSTR (v_string, '|') + 1);
            v_pnum := NULL;
            v_control := NULL;
            v_qty := NULL;
            v_pnum := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
            v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
            v_control := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
            v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
            v_qty := TO_NUMBER (v_substring);

            IF v_qty IS NOT NULL
            THEN
               v_pnum_str := v_pnum_str || v_pnum || ',';
               v_qty_str := v_qty_str || v_qty || ',';
            END IF;
         END LOOP;

         -- To assign part and qty information
         my_context.set_my_double_inlist_ctx (v_pnum_str, v_qty_str);

         --
         SELECT COUNT (t521.c205_part_number_id)
           INTO v_cnt
           FROM t521_request_detail t521
          WHERE t521.c520_request_id = p_request_id;

         IF v_cnt = 0                                           -- New Request
         THEN
            INSERT INTO t521_request_detail (c521_request_detail_id,c520_request_id,c521_qty,c205_part_number_id)
               SELECT s521_request_detail.NEXTVAL, p_request_id, qty, pnum
                 FROM (SELECT   SUM (TO_NUMBER (tokenii)) qty, token pnum
                           FROM v_double_in_list
                          WHERE token IS NOT NULL AND tokenii IS NOT NULL
                       GROUP BY token);

            --Check the Entered CN Qty is greater than the Set Qty
            --, If yes throw Error
            SELECT COUNT (*)
              INTO v_etc_part_cnt
              FROM t208_set_details t208,
                   (SELECT t521.c205_part_number_id, t521.c521_qty qty
                      FROM t520_request t520, t521_request_detail t521
                     WHERE t520.c520_request_id = p_request_id
                       AND t520.c520_request_id = t521.c520_request_id
                       AND t520.c207_set_id = p_set_id
                       AND t520.c520_void_fl IS NULL
                       AND t520.c520_delete_fl IS NULL) part
             WHERE c207_set_id = p_set_id
               AND c208_void_fl IS NULL
               AND t208.c205_part_number_id NOT IN (
                      SELECT t521.c205_part_number_id
                        FROM t520_request t520, t521_request_detail t521
                       WHERE t520.c520_request_id = p_request_id
                         AND t520.c520_request_id = t521.c520_request_id
                         AND t520.c207_set_id = p_set_id
                         AND t520.c520_void_fl IS NULL
                         AND t208.c208_set_qty = t521.c521_qty
                         AND t520.c520_delete_fl IS NULL)
               AND t208.c205_part_number_id = part.c205_part_number_id(+)
               AND t208.c208_set_qty - NVL (qty, 0) < 0;

            IF v_etc_part_cnt > 0
            THEN
               raise_application_error ('-20945', '');
            END IF;

            SELECT COUNT (*)
              INTO v_etc_part_cnt
              FROM t208_set_details t208
             WHERE c207_set_id = p_set_id
               AND c208_set_qty > 0
               AND c208_void_fl IS NULL
               AND c205_part_number_id NOT IN (
                      SELECT t521.c205_part_number_id
                        FROM t520_request t520, t521_request_detail t521
                       WHERE t520.c520_request_id = p_request_id
                         AND t520.c520_request_id = t521.c520_request_id
                         AND t520.c207_set_id = p_set_id
                         AND t520.c520_void_fl IS NULL
                         AND t208.c208_set_qty = t521.c521_qty
                         AND t520.c520_delete_fl IS NULL);

            IF v_etc_part_cnt > 0
            THEN
               gm_pkg_op_request_master.gm_sav_request
                                                   (NULL,
                                                    TRUNC (LAST_DAY (CURRENT_DATE)),
                                                    v_source,
                                                    v_txn_id,
                                                    p_set_id,
                                                    NULL,
                                                    NULL,
                                                    NULL,
                                                    NULL,
                                                    NULL,
                                                    NULL,
                                                    p_request_id,
                                                    10,
                                                    p_userid,
                                                    NULL,
                                                    v_request_id
                                                   );

               INSERT INTO t521_request_detail (c521_request_detail_id,c520_request_id,c521_qty,c205_part_number_id)
                  SELECT s521_request_detail.NEXTVAL, v_request_id,
                         t208.c208_set_qty - NVL (qty, 0) qty,
                         t208.c205_part_number_id
                    FROM t208_set_details t208,
                         (SELECT t521.c205_part_number_id, t521.c521_qty qty
                            FROM t520_request t520, t521_request_detail t521
                           WHERE t520.c520_request_id = p_request_id
                             AND t520.c520_request_id = t521.c520_request_id
                             AND t520.c207_set_id = p_set_id
                             AND t520.c520_void_fl IS NULL
                             AND t520.c520_delete_fl IS NULL) part
                   WHERE c207_set_id = p_set_id
                     AND c208_set_qty > 0
                     AND c208_void_fl IS NULL
                     AND t208.c205_part_number_id NOT IN (
                            SELECT t521.c205_part_number_id
                              FROM t520_request t520,
                                   t521_request_detail t521
                             WHERE t520.c520_request_id = p_request_id
                               AND t520.c520_request_id = t521.c520_request_id
                               AND t520.c207_set_id = p_set_id
                               AND t520.c520_void_fl IS NULL
                               AND t208.c208_set_qty = t521.c521_qty
                               AND t520.c520_delete_fl IS NULL)
                     AND t208.c205_part_number_id = part.c205_part_number_id(+);
            END IF;

            DELETE FROM t521_request_detail
                  WHERE c520_request_id = p_request_id;
         ELSE                                            ---- Existing Request
            DELETE FROM t521_request_detail t521
                  WHERE t521.c520_request_id = p_request_id
                    AND c205_part_number_id IN (
                           SELECT   token
                               FROM v_double_in_list temp
                              WHERE t521.c205_part_number_id = temp.token
                                AND temp.token IS NOT NULL
                                AND temp.tokenii IS NOT NULL
                             HAVING t521.c521_qty =
                                       NVL (SUM (TO_NUMBER (temp.tokenii)), 0)
                           GROUP BY temp.token);

            SELECT t520.c520_request_to, t520.c901_ship_to,
                   t520.c520_ship_to_id, t520.c520_request_for,
                   t520.c901_purpose
              INTO v_request_to, v_ship_to,
                   v_ship_to_id, v_request_for,
                   v_purpose
              FROM t520_request t520
             WHERE c520_request_id = p_request_id;

            UPDATE t504_consignment t504
               SET t504.c701_distributor_id =
                             DECODE (v_request_for,
                                     40021, v_request_to,
                                     NULL
                                    ),
                   t504.c704_account_id =
                                     DECODE (v_request_for,
                                             40021, NULL,
                                             '01'
                                            ),
                   t504.c504_ship_to = v_ship_to,
                   c504_ship_to_id = v_ship_to_id,
                   c504_inhouse_purpose = v_purpose,
                   c504_last_updated_by = p_userid,
				   c504_last_updated_date = CURRENT_DATE
             WHERE t504.c520_request_id = p_request_id;

            SELECT COUNT (*)
              INTO v_etc_part_cnt
              FROM t521_request_detail t521
             WHERE t521.c520_request_id = p_request_id;

            IF v_etc_part_cnt > 0
            THEN
               gm_pkg_op_request_master.gm_sav_request
                                                   (NULL,
                                                    TRUNC (LAST_DAY (CURRENT_DATE)),
                                                    v_source,
                                                    v_txn_id,
                                                    p_set_id,
                                                    NULL,
                                                    NULL,
                                                    NULL,
                                                    NULL,
                                                    NULL,
                                                    NULL,
                                                    p_request_id,
                                                    10,
                                                    p_userid,
                                                    NULL,
                                                    v_request_id
                                                   );

               INSERT INTO t521_request_detail (c521_request_detail_id,c520_request_id,c521_qty,c205_part_number_id)
                  SELECT s521_request_detail.NEXTVAL, v_request_id, qty, pnum
                    FROM (SELECT   (  SUM (t521.c521_qty)
                                    - NVL (SUM (TO_NUMBER (temp.tokenii)), 0)
                                   ) qty,
                                   t521.c205_part_number_id pnum
                              FROM t521_request_detail t521,
                                   v_double_in_list temp
                             WHERE t521.c520_request_id = p_request_id
                               AND t521.c205_part_number_id = temp.token
                               AND temp.token IS NOT NULL
                               AND temp.tokenii IS NOT NULL
                          GROUP BY temp.token,
                                   t521.c520_request_id,
                                   t521.c205_part_number_id);

               DELETE FROM t521_request_detail
                     WHERE c520_request_id = p_request_id;
            END IF;
         END IF;
      END IF;
      		  
      --Whenever the detail is inserted/updated, the request table has to be updated ,so ETL can Sync it to GOP.
	  UPDATE t520_request
		 SET c520_last_updated_by = p_userid
		   , c520_last_updated_date = CURRENT_DATE
	   WHERE C520_request_Id = p_request_id; 
--
   END gm_op_sav_rtn_request_details;
END gm_pkg_op_reprocess;
/
