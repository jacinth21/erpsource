/* Formatted on 2009/02/05 16:55 (Formatter Plus v4.8.0) */

--@"C:\Database\Packages\Operations\gm_pkg_op_subpart.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_subpart
IS
/*******************************************************
	* Description : Procedure to fetch all sub parts which
	corresponding to project name and part number be selected
	* Author		 : Xun Qu
	*******************************************************/
	PROCEDURE gm_fch_subpart_order (
		p_partnumber	IN		 t205_part_number.c205_part_number_id%TYPE
	  , p_project_id	IN		 t205_part_number.c202_project_id%TYPE
	  , p_out_subpart	OUT 	 TYPES.cursor_type
	)
	AS
		v_pnum CLOB;
		v_company_id t1900_company.c1900_company_id%TYPE;
	BEGIN
		v_pnum :=replace(p_partnumber,',','|');
		
		SELECT get_compid_frm_cntx()
    		INTO v_company_id
    		FROM DUAL;
		OPEN p_out_subpart
		 FOR
			 SELECT t205.c205_part_number_id pnum, t205.c205_part_num_desc description
				  , NVL (t205d.c205_part_number_id, 'N') status
			   FROM t205_part_number t205, t205d_sub_part_to_order t205d, t2023_part_company_mapping t2023
			  WHERE t205.c205_part_number_id = t205d.c205_part_number_id(+)
				AND t205.c202_project_id = DECODE (p_project_id, '0', t205.c202_project_id, p_project_id)
				--AND t205.c205_part_number_id = NVL (TRIM (p_partnumber), t205.c205_part_number_id)
			AND REGEXP_LIKE(t205.c205_part_number_id, CASE WHEN TO_CHAR(v_pnum) IS NOT NULL THEN TO_CHAR(REGEXP_REPLACE(v_pnum,'[+]','\+')) ELSE REGEXP_REPLACE(t205.c205_part_number_id,'[+]','\+') END)
				AND t205.c205_sub_component_fl = 'Y'
				AND t205.c205_part_number_id = t2023.c205_part_number_id 
				AND t2023.c2023_void_fl IS NULL
				AND t2023.c1900_company_id = v_company_id
				ORDER BY t205.c205_part_number_id;
	END gm_fch_subpart_order;

/*******************************************************
	  * Description : Procedure to save Sub Part Order Info
	  * Author		: Xun
	  *******************************************************/
	PROCEDURE gm_sav_subpart_order (
		p_inputstr	   IN	CLOB
	  , p_project_id   IN	t205_part_number.c202_project_id%TYPE
	  , p_part_num	   IN	t205_part_number.c205_part_number_id%TYPE
	  , p_userid	   IN	t205_part_number.c205_created_by%TYPE
	)
	AS
		v_strlen	   NUMBER := NVL (LENGTH (p_inputstr), 0);
		v_string	   CLOB := p_inputstr;
		v_partnum	   t205_part_number.c205_part_number_id%TYPE;
		v_sub_part_string	  CLOB;
		v_checkfl	   VARCHAR2 (10);
		v_parent_multi_part  NUMBER;
	--
	BEGIN

		IF v_strlen > 0
		THEN
			WHILE INSTR (v_string, '^') <> 0
			LOOP
				v_sub_part_string :=  SUBSTR (v_string, 1, INSTR (v_string, '^') - 1);
				v_string 		  :=  SUBSTR (v_string, INSTR (v_string, '^') + 1);
				
				v_partnum		  := NULL;
				v_checkfl		  := NULL;
				v_partnum	      := SUBSTR (v_sub_part_string, 1, INSTR (v_sub_part_string, ',') - 1);
				v_sub_part_string := SUBSTR (v_sub_part_string, INSTR (v_sub_part_string, ',') + 1);
				v_checkfl 		  := v_sub_part_string;
				
				--checking any of the parent parts of this sub-part as marked as Multi-project.		
			    SELECT count(1)      
				  INTO v_parent_multi_part 
			      FROM t205_part_number 
			 	 WHERE c205_part_number_id IN (
							 SELECT c205_from_part_number_id  
                               FROM t205a_part_mapping t205a,
                                    t205d_sub_part_to_order t205d
                              WHERE t205a.c205_to_part_number_id = t205d.c205_part_number_id
                                AND t205a.c205_to_part_number_id = v_partnum 
                                 AND t205a.c205a_void_fl is null)  --- added new column c205a_void_fl in t205a used to PMT-50777 - Create BOM and sub-component Mapping
				  AND NVL(c205_crossover_fl,-9999)='Y';
					  
				--Removing the mapping of multi-project for this sub-part before deleting the entry in t205d_sub_part_to_order. 
				 IF v_parent_multi_part = 0   
			 	 THEN
					  UPDATE t205_part_number
					   SET c205_crossover_fl      = ''
						 , c205_last_updated_by   = p_userid
					 	 , c205_last_updated_date = CURRENT_DATE
					 WHERE c202_project_id   = DECODE (p_project_id, '0', c202_project_id, p_project_id)
					 AND c205_part_number_id = v_partnum;
				 END IF;
				 
				DELETE FROM t205d_sub_part_to_order t205d
				  WHERE t205d.c205_part_number_id IN (
							SELECT t205.c205_part_number_id
							  FROM t205_part_number t205
							 WHERE t205.c202_project_id =
														 DECODE (p_project_id
															   , '0', t205.c202_project_id
															   , p_project_id
																)
							   AND t205.c205_part_number_id = NVL (v_partnum, t205.c205_part_number_id));
				
				IF(v_checkfl = 'Y')
				THEN
				INSERT INTO t205d_sub_part_to_order
							(c205d_sub_part_detail_id, c205_part_number_id, c205d_updated_date, c205d_updated_by
							)
					 VALUES (s205d_sub_part_to_order.NEXTVAL, v_partnum, CURRENT_DATE, p_userid
							);

				 --checking any of the parent parts of this sub-part as marked as Multi-project.		
				    SELECT count(1)      
					  INTO v_parent_multi_part 
				      FROM t205_part_number 
				 	 WHERE c205_part_number_id IN (
								 SELECT c205_from_part_number_id  
	                               FROM t205a_part_mapping t205a,
	                                    t205d_sub_part_to_order t205d
	                              WHERE t205a.c205_to_part_number_id = t205d.c205_part_number_id
	                                AND t205a.c205_to_part_number_id = v_partnum 
	                                AND t205a.c205a_void_fl is null) ---  added new column c205a_void_fl in t205a used to PMT-50777 - Create BOM and sub-component Mapping
					  AND NVL(c205_crossover_fl,-9999)='Y';		
					  
				 --if the count > 0 then there will be some parent parts of the sub-part as marked as Multi-project.so updating child as Multi-project.	 
			 		 IF v_parent_multi_part > 0   
			 		 THEN
				 		UPDATE t205_part_number
					       SET c205_crossover_fl      = 'Y'
						     , c205_last_updated_by   = p_userid
						     , c205_last_updated_date = CURRENT_DATE
					     WHERE c202_project_id   = DECODE (p_project_id, '0', c202_project_id, p_project_id)
			   			 AND c205_part_number_id = v_partnum;
			 		 END IF;

				END IF;
				
			END LOOP;
		END IF;
	END gm_sav_subpart_order;
END gm_pkg_op_subpart;
/
