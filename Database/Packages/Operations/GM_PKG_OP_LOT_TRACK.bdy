--@"C:\Database\Packages\Operations\GM_PKG_OP_LOT_TRACK.BDY"

CREATE OR REPLACE
PACKAGE BODY gm_pkg_op_lot_track
IS

/********************************************************
	* Author      : APrasath
	* Description : Call from Lot Tracking to track Lot information 
* ******************************************************/
PROCEDURE gm_lot_track_main(
	p_part_number_id     IN   t205c_part_qty.c205_part_number_id%TYPE
  , p_update_qty         IN   t205c_part_qty.c205_available_qty%TYPE
  , p_trans_id           IN   t205c_part_qty.c205_last_update_trans_id%TYPE
  , p_updated_by         IN   t205c_part_qty.c205_last_updated_by%TYPE
  , p_warehouse_type     IN   t205c_part_qty.c901_transaction_type%TYPE
  , p_action             IN   t205c_part_qty.c901_action%TYPE
  , p_type               IN   t205c_part_qty.c901_type%TYPE
  , p_control_number     IN t5055_control_number.c5055_control_number%TYPE DEFAULT NULL)
AS    
    cur_txn_log TYPES.cursor_type;
    v_warehouse_type t906_rules.c906_rule_value%TYPE;
    v_flag VARCHAR2(1);
    v_ref_type 	t5060_control_number_inv.c901_ref_type%TYPE;
    v_error_msg VARCHAR2(100); 
    v_company_id   t1900_company.c1900_company_id%TYPE;
BEGIN	
	    --validating lot tracking is available for company 
	   SELECT  get_compid_frm_cntx() INTO  v_company_id FROM DUAL;
	    
	   SELECT get_rule_value(v_company_id,'LOT_TRACK') INTO v_flag FROM DUAL; 
	   
	   IF v_flag IS NULL OR v_flag <> 'Y' 
	   THEN
		   RETURN;
	   END IF;
	    
	   --validating the part is required to track lot		  	
	   SELECT DECODE (p_type,'50982','Y',get_part_attr_value_by_comp(p_part_number_id,'104480',v_company_id)) INTO v_flag FROM DUAL; --50982 dhr;	  
	   
	   IF v_flag IS NULL OR v_flag <> 'Y' 
	   THEN
		   RETURN;
	   END IF;
	   
	   --Get data from t501,502a,t214, t5053 to update Inventory, Field sales or Account and return
	   gm_fch_txn_data(p_type,p_part_number_id,p_trans_id,p_warehouse_type,p_control_number,p_update_qty,p_action,cur_txn_log);     
			
	   --Save data in t5060
	   gm_upd_lot_data(p_type,p_part_number_id,p_trans_id,p_warehouse_type,p_action,p_updated_by,cur_txn_log);	   
      
END gm_lot_track_main;



/********************************************************
	* Author      : APrasath
	* Description : Fetching transaction data
* ******************************************************/
PROCEDURE gm_fch_txn_data
(	p_type               IN   t205c_part_qty.c901_type%TYPE
  ,	p_part_number_id     IN   t205c_part_qty.c205_part_number_id%TYPE  
  , p_trans_id           IN   t205c_part_qty.c205_last_update_trans_id%TYPE
  , p_warehouse_type   IN   t205c_part_qty.c901_transaction_type%TYPE
  , p_control_number IN t5055_control_number.c5055_control_number%TYPE
  , p_update_qty         IN   t205c_part_qty.c205_available_qty%TYPE
  , p_action             IN   t205c_part_qty.c901_action%TYPE
  , p_out_lot out TYPES.cursor_type
)
AS
		v_procedure  VARCHAR2 (2000);
BEGIN
	
	BEGIN
		SELECT c906_rule_desc
		  INTO v_procedure
		  FROM t906_rules
		 WHERE c906_rule_id = p_type AND c906_rule_grp_id = 'LOTFETCHPROC'
	     AND c906_rule_value=p_action AND c901_rule_type=p_warehouse_type
		 AND c1900_company_id IS NULL 
		 AND c906_void_fl IS NULL;
	 EXCEPTION WHEN NO_DATA_FOUND THEN
	  	v_procedure := NULL;
	  END;	  
	 IF v_procedure = 'ORDER' THEN
	 --calling while raising order and it has control numbers which used in surgery 
	 --get order data from t502a,t501 to update in t5060
	    gm_fch_txn_order_data(p_type,p_part_number_id,p_trans_id,p_warehouse_type,p_control_number,p_update_qty,p_action,p_out_lot);	  
	 ELSIF v_procedure = 'FSORDER' THEN
	 	--bill and ship, released backorder
	 	gm_fch_txn_order_fs_data(p_type,p_part_number_id,p_trans_id,p_warehouse_type,p_control_number,p_update_qty,p_action,p_out_lot);
	  ELSIF v_procedure = 'CONSIGNMENT' THEN
	 	--Consignment
	 	gm_fch_txn_consignment_data(p_type,p_part_number_id,p_trans_id,p_warehouse_type,p_control_number,p_update_qty,p_action,p_out_lot);	
	 ELSIF v_procedure = 'RETURNFS' THEN
	 	-- reducing qty from FS or Account while return 
	 	 gm_fch_txn_returns_fs_data(p_type,p_part_number_id,p_trans_id,p_warehouse_type,p_control_number,p_update_qty,p_action,p_out_lot);
	 ELSIF v_procedure = 'RETURNSHOLD' THEN
	 	-- increasing qty returns hold 
	 	 gm_fch_txn_returns_data(p_type,p_part_number_id,p_trans_id,p_warehouse_type,p_control_number,p_update_qty,p_action,p_out_lot);
	  ELSIF v_procedure = 'DHR' THEN
	 	-- increasing or decreasing qty DHR 
	 	 gm_fch_txn_dhr_data(p_type,p_part_number_id,p_trans_id,p_warehouse_type,p_control_number,p_update_qty,p_action,p_out_lot);
	 ELSIF v_procedure = 'INHOUSE' THEN
	 	-- increasing or decreasing qty INHOUSE 
	 	 gm_fch_txn_inhouse_data(p_type,p_part_number_id,p_trans_id,p_warehouse_type,p_control_number,p_update_qty,p_action,p_out_lot);
	  ELSIF v_procedure = 'IHCONSIGNMENT' THEN
	 	-- increasing or decreasing qty INHOUSE transaction in t504
	 	 gm_fch_txn_consignment_ih_data(p_type,p_part_number_id,p_trans_id,p_warehouse_type,p_control_number,p_update_qty,p_action,p_out_lot);
	 ELSE
	     --transactions from t5055
	  	 gm_fch_txn_other_data(p_type,p_part_number_id,p_trans_id,p_warehouse_type,p_control_number,p_update_qty,p_action,p_out_lot);
	 END IF;	
END gm_fch_txn_data;


/********************************************************
	* Author      : APrasath
	* Description : Get order, consignment, inhouse and return txn data 
* ******************************************************/
PROCEDURE gm_fch_txn_other_data
(	p_type               IN   t205c_part_qty.c901_type%TYPE
  ,	p_part_number_id     IN   t205c_part_qty.c205_part_number_id%TYPE  
  , p_trans_id           IN   t205c_part_qty.c205_last_update_trans_id%TYPE
  , p_warehouse_type   IN   t205c_part_qty.c901_transaction_type%TYPE
  , p_control_number IN t5055_control_number.c5055_control_number%TYPE
  , p_update_qty         IN   t205c_part_qty.c205_available_qty%TYPE
  , p_action             IN   t205c_part_qty.c901_action%TYPE
  , p_out_lot out TYPES.cursor_type
)
AS
BEGIN
  OPEN p_out_lot FOR 
    SELECT 't5055' tablename, t5055.c5055_ref_id txn_id, DECODE(p_type,'4000645',
    DECODE(SIGN (p_update_qty), - 1,t5055.c205_client_part_number_id, t5055.c205_part_number_id),t5055.c205_part_number_id) part_num
  , t5055.c5055_qty*SIGN (p_update_qty) qty, UPPER (t5055.c5055_control_number) ctrl_no, p_warehouse_type wh_type
  , p_type txn_type, '' ref_id, t5055.c901_type opr, t5055.c1900_company_id companyid, t5055.c5040_plant_id plantid
  ,  NVL(t907.c901_ship_to,'') shipto, NVL(t907.c907_ship_to_id,'') shiptoid, NVL(t907.c907_shipped_dt,'') shippeddate, '' customerpo
   FROM t5055_control_number t5055, t2550_part_control_number t2550, t907_shipping_info t907
  WHERE t5055.c5055_ref_id        = p_trans_id
    --AND NVL(t5055.c205_client_part_number_id,t5055.c205_part_number_id) = p_part_number_id
    AND DECODE(p_type,'4000645',DECODE(SIGN (p_update_qty), - 1,t5055.c205_client_part_number_id, t5055.c205_part_number_id),t5055.c205_part_number_id) = p_part_number_id
    --AND t5055.c5055_qty           = ABS(p_update_qty)
    AND t5055.c901_type           = DECODE(p_type,'4000645',93345,DECODE (SIGN (p_update_qty), - 1, 93343, 93345)) --93343 (Pick Transacted) 93345 (PUT-- Transacted)
    AND t2550.c205_part_number_id      = t5055.c205_part_number_id
    AND t2550.c2550_control_number = UPPER (t5055.c5055_control_number)
    AND t2550.c1900_company_id = t5055.c1900_company_id
    AND t2550.c5040_plant_id = t5055.c5040_plant_id
    AND NVL(t2550.c2550_lot_status,'-999') NOT IN ('105006','105000') --105006 SOLD  105000 IMPLANTED
    AND t5055.c5055_void_fl IS NULL 
    AND t5055.c5055_ref_id = t907.c907_ref_id(+)
    AND t907.c907_void_fl(+) IS NULL;       
END gm_fch_txn_other_data;

/********************************************************
	* Author      : APrasath
	* Description : Call from Lot Tracking to get Sales data
* ******************************************************/
PROCEDURE gm_fch_txn_order_data(
	p_type               IN   t205c_part_qty.c901_type%TYPE
  ,	p_part_number_id     IN   t205c_part_qty.c205_part_number_id%TYPE  
  , p_trans_id           IN   t205c_part_qty.c205_last_update_trans_id%TYPE
  , p_warehouse_type   IN   t205c_part_qty.c901_transaction_type%TYPE
  , p_control_number IN t5055_control_number.c5055_control_number%TYPE
  , p_update_qty         IN   t205c_part_qty.c205_available_qty%TYPE
  , p_action             IN   t205c_part_qty.c901_action%TYPE
  , p_out_lot out TYPES.cursor_type
)
AS
BEGIN
  OPEN p_out_lot FOR   
    SELECT 't502a' tablename, t502b.c501_order_id txn_id 
      , t502b.c205_part_number_id part_num
      , (sign(-1) * t502b.c502b_item_qty)  qty
      , upper(t502b.c502b_usage_lot_num) ctrl_no-- Control number
      , p_warehouse_type wh_type
      , NVL(t501.c901_order_type,2521) txn_type  -- Added 2521 for bill and ship lots
      , t501.c704_account_id ref_id     
      , 0 opr
      , t2550.c1900_company_id companyid       -- PMT-39855 - Order usage from mobile device not reduced lot qty in field sales inventory(Trifecta) 
      , t2550.c5040_plant_id plantid
      , t501.c501_ship_to shipto
      , t501.c501_ship_to_id shiptoid
      , t501.c501_shipping_date shippeddate
      ,t501.C501_CUSTOMER_PO customerpo
    FROM t502b_item_order_usage t502b , t501_order t501, t2550_part_control_number t2550
    WHERE t501.c501_order_id = t502b.c501_order_id 
      AND t501.c501_order_id = p_trans_id
      AND t502b.c205_part_number_id = p_part_number_id
      --AND t502a.c502a_control_number_update_fl IS NULL
      --AND t502a.c901_attribute_type= 101723 -- Control Number Attribute
      AND t502b.c502b_usage_lot_num = upper(p_control_number)
      --AND t502a.c502a_item_qty = p_update_qty
      AND t2550.c205_part_number_id      = t502b.c205_part_number_id
      AND t2550.c2550_control_number = t502b.c502b_usage_lot_num
      AND t2550.c1900_company_id = t501.c1900_company_id
      AND t2550.c5040_plant_id = t501.c5040_plant_id
      AND NVL(t2550.c2550_lot_status,'-999') NOT IN (105000,105006) -- Removed lot status is null and adding implanted and sold for PMT-29638 
      AND NVL(t2550.c901_lot_controlled_status,'-999') NOT IN (105008)--controlled
      AND t502b.c502b_void_fl IS NULL
      AND t501.c501_void_fl IS NULL;
END gm_fch_txn_order_data;

/********************************************************
	* Author      : APrasath
	* Description : Call from Lot Tracking to get Sales data
* ******************************************************/
PROCEDURE gm_fch_txn_order_fs_data(
	p_type               IN   t205c_part_qty.c901_type%TYPE
  ,	p_part_number_id     IN   t205c_part_qty.c205_part_number_id%TYPE  
  , p_trans_id           IN   t205c_part_qty.c205_last_update_trans_id%TYPE
  , p_warehouse_type   IN   t205c_part_qty.c901_transaction_type%TYPE
  , p_control_number IN t5055_control_number.c5055_control_number%TYPE
  , p_update_qty         IN   t205c_part_qty.c205_available_qty%TYPE
  , p_action             IN   t205c_part_qty.c901_action%TYPE
  , p_out_lot out TYPES.cursor_type
)
AS
BEGIN
  OPEN p_out_lot FOR
  SELECT 't501' tablename, t5055.c5055_ref_id txn_id, t5055.c205_part_number_id part_num
  , t5055.c5055_qty qty, UPPER (t5055.c5055_control_number) ctrl_no, p_warehouse_type wh_type
  , p_type txn_type, get_distributor_id(t501.c703_sales_rep_id) ref_id, t5055.c901_type opr,t501.c1900_company_id companyid, t501.c5040_plant_id plantid
  , t501.c501_ship_to shipto, t501.c501_ship_to_id shiptoid, t501.c501_shipping_date shippeddate, t501.C501_CUSTOMER_PO customerpo
   FROM t5055_control_number t5055, t501_order t501, t2550_part_control_number t2550
  WHERE t5055.c5055_ref_id        = p_trans_id
    AND t5055.c5055_ref_id        = t501.c501_order_id
    AND t5055.c205_part_number_id = p_part_number_id
    --AND t5055.c5055_qty           = p_update_qty
    AND t2550.c205_part_number_id      = t5055.c205_part_number_id
    AND t2550.c2550_control_number = UPPER (t5055.c5055_control_number)
    AND t2550.c1900_company_id = t501.c1900_company_id
    AND t2550.c5040_plant_id = t501.c5040_plant_id
    AND NVL(t2550.c2550_lot_status,'-999') NOT IN (105000,105006)  -- Removed lot status is null and adding implanted and sold for PMT-29638
    AND NVL(t2550.c901_lot_controlled_status,'-999') NOT IN (105008)--controlled
    AND t5055.c5055_void_fl      IS NULL
    AND t501.c501_void_fl        IS NULL;    
END gm_fch_txn_order_fs_data;


/********************************************************
	* Author      : APrasath
	* Description : Call from Lot Tracking to get consignment data
* ******************************************************/
PROCEDURE gm_fch_txn_consignment_data(
	p_type               IN   t205c_part_qty.c901_type%TYPE
  ,	p_part_number_id     IN   t205c_part_qty.c205_part_number_id%TYPE  
  , p_trans_id           IN   t205c_part_qty.c205_last_update_trans_id%TYPE
  , p_warehouse_type   IN   t205c_part_qty.c901_transaction_type%TYPE
  , p_control_number IN t5055_control_number.c5055_control_number%TYPE
  , p_update_qty         IN   t205c_part_qty.c205_available_qty%TYPE
  , p_action             IN   t205c_part_qty.c901_action%TYPE
  , p_out_lot out TYPES.cursor_type
)
AS
BEGIN
  OPEN p_out_lot FOR
   SELECT 't5053' tablename, t5053.c5053_last_update_trans_id txn_id, t5053.c205_part_number_id part_num
  , t505.c505_item_qty qty, upper (t505.c505_control_number) ctrl_no, t5051.c901_warehouse_type wh_type
  , t5053.c901_type txn_type, t5052.c5052_ref_id ref_id, t5053.c901_type opr, t504.c1900_company_id companyid , t504.c5040_plant_id plantid
  , t504.C504_SHIP_TO shipto, t504.C504_SHIP_TO_ID shiptoid, t504.C504_SHIP_DATE shippeddate, t520a.c520a_attribute_value customerpo
   FROM t505_item_consignment t505, t504_consignment t504, t5053_location_part_mapping t5053
  , t5052_location_master t5052, t5051_inv_warehouse t5051, t2550_part_control_number t2550, t520a_request_attribute t520a
  WHERE t5053.c5053_last_update_trans_id            = t505.c504_consignment_id
    AND t5053.c5053_last_update_trans_id            = p_trans_id
    AND t5053.c5052_location_id       = t5052.c5052_location_id
    AND t5051.c5051_inv_warehouse_id  = t5052.c5051_inv_warehouse_id
    AND t5052.c5051_inv_warehouse_id IN (3, 5) -- FS and Account Warehouse
    AND t5053.c901_type               IN (102907,102909) -- Consignment, Transfer
    AND t505.c205_part_number_id      = t5053.c205_part_number_id
    AND t5053.c205_part_number_id     = p_part_number_id
    --AND t505.c505_item_qty            = p_update_qty
    AND t2550.c205_part_number_id     = t5053.c205_part_number_id
    AND t2550.c2550_control_number    = t505.c505_control_number
    AND t2550.c1900_company_id = t504.c1900_company_id
    AND t2550.c5040_plant_id = t504.c5040_plant_id
    AND (T504.C207_Set_Id IS NOT NULL OR NVL(t2550.c901_lot_controlled_status,'-999') NOT IN (105008)) --controlled
    AND t505.c504_consignment_id      = t504.c504_consignment_id
    AND t505.c505_void_fl            IS NULL
    AND t504.c504_void_fl            IS NULL
    AND t5052.c5052_void_fl IS NULL
    AND t504.c520_request_id = t520a.c520_request_id (+)
    AND t520a.c901_attribute_type (+) = 1006420  -- This is for to get the Customer PO vlaue
    AND t520a.c520a_void_fl (+) is null;    
END gm_fch_txn_consignment_data;


/********************************************************
	* Author      : APrasath
	* Description : Call from Lot Tracking to get returns data
* ******************************************************/
PROCEDURE gm_fch_txn_returns_data(
	p_type               IN   t205c_part_qty.c901_type%TYPE
  ,	p_part_number_id     IN   t205c_part_qty.c205_part_number_id%TYPE  
  , p_trans_id           IN   t205c_part_qty.c205_last_update_trans_id%TYPE
  , p_warehouse_type   IN   t205c_part_qty.c901_transaction_type%TYPE
  , p_control_number IN t5055_control_number.c5055_control_number%TYPE
  , p_update_qty         IN   t205c_part_qty.c205_available_qty%TYPE
  , p_action             IN   t205c_part_qty.c901_action%TYPE
  , p_out_lot out TYPES.cursor_type
)
AS
BEGIN
  OPEN p_out_lot FOR   
     SELECT 't506' tablename, t506.c506_rma_id txn_id, t507.c205_part_number_id part_num
  , t507.c507_item_qty qty, upper (t507.c507_control_number) ctrl_no, p_warehouse_type wh_type
  , p_type txn_type, NULL ref_id, NULL opr, t506.c1900_company_id companyid , t506.c5040_plant_id plantid
  , '' shipto, '' shiptoid, '' shippeddate, '' customerpo
   FROM t507_returns_item t507, t506_returns t506, t2550_part_control_number t2550
  WHERE t507.c506_rma_id         = p_trans_id
    --AND ABS (t507.c507_item_qty) = ABS (p_update_qty)
    AND t507.c205_part_number_id = p_part_number_id
    AND t507.c506_rma_id         = t506.c506_rma_id
    AND t507.c507_status_fl      = 'C' -- Take only credits which only has lot number
    AND t2550.c205_part_number_id     = t507.c205_part_number_id
    AND t2550.c2550_control_number    = t507.c507_control_number
    AND t2550.c1900_company_id = t506.c1900_company_id
    AND t2550.c5040_plant_id = t506.c5040_plant_id
    AND NVL(t2550.c2550_lot_status,'-999') NOT IN (105000,105006)  -- Removed lot status is null and adding implanted and sold for PMT-29638
    AND NVL(t2550.c901_lot_controlled_status,'-999') NOT IN (105008) --controlled
    AND t506.c506_void_fl       IS NULL;
END gm_fch_txn_returns_data;

/********************************************************
	* Author      : APrasath
	* Description : Call from Lot Tracking to get returns field sales data
* ******************************************************/
PROCEDURE gm_fch_txn_returns_fs_data(
	p_type               IN   t205c_part_qty.c901_type%TYPE
  ,	p_part_number_id     IN   t205c_part_qty.c205_part_number_id%TYPE  
  , p_trans_id           IN   t205c_part_qty.c205_last_update_trans_id%TYPE
  , p_warehouse_type   IN   t205c_part_qty.c901_transaction_type%TYPE
  , p_control_number IN t5055_control_number.c5055_control_number%TYPE
  , p_update_qty         IN   t205c_part_qty.c205_available_qty%TYPE
  , p_action             IN   t205c_part_qty.c901_action%TYPE
  , p_out_lot out TYPES.cursor_type
)
AS
BEGIN
  OPEN p_out_lot FOR
   SELECT 't5053' tablename, t5053.c5053_last_update_trans_id txn_id, t5053.c205_part_number_id part_num
  , (t507.c507_item_qty * SIGN (p_update_qty)) qty, UPPER (t507.c507_control_number) ctrl_no,
    t5051.c901_warehouse_type wh_type, t5053.c901_type txn_type, t5052.c5052_ref_id ref_id
  , t5053.c901_type opr, t506.c1900_company_id companyid , t506.c5040_plant_id plantid
  , '' shipto, '' shiptoid, '' shippeddate, '' customerpo
   FROM t507_returns_item t507, t506_returns t506, t5053_location_part_mapping t5053
  , t5052_location_master t5052, t5051_inv_warehouse t5051, t2550_part_control_number t2550
  WHERE t5053.c5053_last_update_trans_id            = t507.c506_rma_id
    AND t507.c506_rma_id              = p_trans_id
    --AND ABS (t5053.c5054_txn_qty)     = ABS (p_update_qty)
    AND t5053.c205_part_number_id     = p_part_number_id
    AND t507.c205_part_number_id      = t5053.c205_part_number_id
    AND t507.c506_rma_id              = t506.c506_rma_id
    AND t507.c507_status_fl           = 'C' -- Take only credits which only has lot number
    AND t5053.c5052_location_id       = t5052.c5052_location_id
    AND t5051.c5051_inv_warehouse_id  = t5052.c5051_inv_warehouse_id
    AND t5052.c5051_inv_warehouse_id IN (3, 5) -- FS and Account Warehouse
    AND t5053.c901_type               IN (102908,102909) -- Return, Transfer
    AND t2550.c205_part_number_id     = t507.c205_part_number_id
    AND t2550.c2550_control_number    = t507.c507_control_number
    AND t2550.c1900_company_id = t506.c1900_company_id
    AND t2550.c5040_plant_id = t506.c5040_plant_id
    AND NVL(t2550.c901_lot_controlled_status,'-999')  = '105008' --controlled
    AND t506.c506_void_fl            IS NULL
    AND t5052.c5052_void_fl IS NULL
    UNION
    SELECT 't506' tablename, t506.c506_rma_id txn_id, t507.c205_part_number_id part_num
  , (t507.c507_item_qty * SIGN (p_update_qty)) qty, upper (t507.c507_control_number) ctrl_no, p_warehouse_type wh_type
  , p_type txn_type, get_distributor_id(t501.c703_sales_rep_id) ref_id, 102908 opr
  , t506.c1900_company_id companyid , t506.c5040_plant_id plantid
  , '' shipto, '' shiptoid, '' shippeddate, '' customerpo
   FROM t507_returns_item t507, t506_returns t506, t2550_part_control_number t2550, t501_order t501
  WHERE t507.c506_rma_id         = p_trans_id
    AND t506.c501_order_id        = t501.c501_order_id
    AND t507.c205_part_number_id = p_part_number_id
    AND t507.c506_rma_id         = t506.c506_rma_id
    AND t507.c507_status_fl      = 'C' -- Take only credits which only has lot number
    AND t506.c506_type = '3300' --sales item
    AND t2550.c205_part_number_id     = t507.c205_part_number_id
    AND t2550.c2550_control_number    = t507.c507_control_number
    AND t2550.c1900_company_id = t506.c1900_company_id
    AND t2550.c5040_plant_id = t506.c5040_plant_id
    AND NVL(t2550.c2550_lot_status,'-999') NOT IN (105000,105006)  -- Removed lot status is null and adding implanted and sold for PMT-29638
    AND NVL(t2550.c901_lot_controlled_status,'-999') IN (105008) --controlled
    AND t501.c501_void_fl        IS NULL
    AND t506.c506_void_fl       IS NULL;    
END gm_fch_txn_returns_fs_data;


/********************************************************
	* Author      : APrasath
	* Description : Call from Lot Tracking to get inhouse data
* ******************************************************/
PROCEDURE gm_fch_txn_inhouse_data(
	p_type               IN   t205c_part_qty.c901_type%TYPE
  ,	p_part_number_id     IN   t205c_part_qty.c205_part_number_id%TYPE  
  , p_trans_id           IN   t205c_part_qty.c205_last_update_trans_id%TYPE
  , p_warehouse_type   IN   t205c_part_qty.c901_transaction_type%TYPE
  , p_control_number IN t5055_control_number.c5055_control_number%TYPE
  , p_update_qty         IN   t205c_part_qty.c205_available_qty%TYPE
  , p_action             IN   t205c_part_qty.c901_action%TYPE
  , p_out_lot out TYPES.cursor_type
)
AS
v_status  VARCHAR2(20);
BEGIN
	v_status:= NULL;
  	 IF p_type = '120601' AND  p_warehouse_type='90815' AND p_action='4301'-- increasing qty DHR to repackage 
	 THEN
	 	v_status:= 105008; --should be update as controlled. 
	 ELSIF p_type = '120603' AND  p_warehouse_type='90813' AND p_action='4301'-- increasing qty DHR to Quaratine 
	 THEN
	 	v_status:= 105008; --should be update as controlled. 
	 ELSE
	  	SELECT decode(p_action,4302,'105008','-999') INTO v_status FROM DUAL;
	 END IF;
  OPEN p_out_lot FOR   
     SELECT '412' tablename, t412.c412_inhouse_trans_id txn_id, t413.c205_part_number_id part_num
  , t413.c413_item_qty * decode(p_action,4302,-1,1) qty, upper (t413.c413_control_number) ctrl_no, p_warehouse_type wh_type
  , p_type txn_type, '' ref_id, NULL opr, t412.c1900_company_id companyid, t412.c5040_plant_id plantid
  , '' shipto, '' shiptoid, '' shippeddate, '' customerpo
   FROM t413_inhouse_trans_items t413, t412_inhouse_transactions t412, t2550_part_control_number t2550
  WHERE t412.c412_inhouse_trans_id = p_trans_id
    AND t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
    --AND t412.c412_status_fl        = '4' -- Take only controlled inhouse transaction from DHR which only has lot number
    AND t2550.c205_part_number_id  = t413.c205_part_number_id
    AND t413.c205_part_number_id     = p_part_number_id
    AND t2550.c2550_control_number = t413.c413_control_number
    AND t2550.c1900_company_id = t412.c1900_company_id
    AND t2550.c5040_plant_id = t412.c5040_plant_id
    AND NVL(t2550.c901_lot_controlled_status,'-999') = v_status  
    AND t412.c412_void_fl         IS NULL
    AND t413.c413_void_fl         IS NULL;
END gm_fch_txn_inhouse_data;


/********************************************************
	* Author      : APrasath
	* Description : Call from Lot Tracking to get inhouse trans in t504 data
* ******************************************************/
PROCEDURE gm_fch_txn_consignment_ih_data(
	p_type               IN   t205c_part_qty.c901_type%TYPE
  ,	p_part_number_id     IN   t205c_part_qty.c205_part_number_id%TYPE  
  , p_trans_id           IN   t205c_part_qty.c205_last_update_trans_id%TYPE
  , p_warehouse_type   IN   t205c_part_qty.c901_transaction_type%TYPE
  , p_control_number IN t5055_control_number.c5055_control_number%TYPE
  , p_update_qty         IN   t205c_part_qty.c205_available_qty%TYPE
  , p_action             IN   t205c_part_qty.c901_action%TYPE
  , p_out_lot out TYPES.cursor_type
)
AS
BEGIN
  OPEN p_out_lot FOR   
     SELECT 't505' tablename, t504.c504_consignment_id txn_id, t505.c205_part_number_id part_num
  , t505.c505_item_qty * DECODE (p_action, 4302, - 1, 1) qty, upper (t505.c505_control_number) ctrl_no,
    p_warehouse_type wh_type, p_type txn_type, '' ref_id
  , NULL opr, t504.c1900_company_id companyid, t504.c5040_plant_id plantid
  ,t504.C504_SHIP_TO shipto,t504.C504_SHIP_TO_ID shiptoid,t504.C504_SHIP_DATE shippeddate, t520a.c520a_attribute_value customerpo
   FROM t505_item_consignment t505, t504_consignment t504, t2550_part_control_number t2550, t520a_request_attribute t520a
  WHERE t504.c504_consignment_id             = p_trans_id
    AND t505.c205_part_number_id             = p_part_number_id
    --AND t505.c505_item_qty                   = ABS(p_update_qty)
    AND t2550.c205_part_number_id            = t505.c205_part_number_id
    AND t2550.c2550_control_number           = t505.c505_control_number
    AND t2550.c1900_company_id = t504.c1900_company_id
    AND t2550.c5040_plant_id = t504.c5040_plant_id
    AND NVL (t2550.C901_LOT_CONTROLLED_STATUS, '-999') = DECODE (p_action, 4302, '105008', '-999')
    AND t505.c504_consignment_id             = t504.c504_consignment_id
    AND t505.c505_void_fl                   IS NULL
    AND t504.c504_void_fl                   IS NULL
    AND t504.c520_request_id = t520a.c520_request_id (+)
    AND t520a.c901_attribute_type (+) = 1006420  -- This is for to get the Customer PO vlaue
    AND t520a.c520a_void_fl (+) is null;
END gm_fch_txn_consignment_ih_data;

/********************************************************
	* Author      : APrasath
	* Description : Call from Lot Tracking to get dhr data
* ******************************************************/
PROCEDURE gm_fch_txn_dhr_data(
	p_type               IN   t205c_part_qty.c901_type%TYPE
  ,	p_part_number_id     IN   t205c_part_qty.c205_part_number_id%TYPE  
  , p_trans_id           IN   t205c_part_qty.c205_last_update_trans_id%TYPE
  , p_warehouse_type   IN   t205c_part_qty.c901_transaction_type%TYPE
  , p_control_number IN t5055_control_number.c5055_control_number%TYPE
  , p_update_qty         IN   t205c_part_qty.c205_available_qty%TYPE
  , p_action             IN   t205c_part_qty.c901_action%TYPE
  , p_out_lot out TYPES.cursor_type
)
AS
BEGIN
  OPEN p_out_lot FOR   
     SELECT '412' tablename, t412.c412_inhouse_trans_id txn_id, t413.c205_part_number_id part_num
  , t413.c413_item_qty * decode(p_action,4302,-1,1) qty, upper (t413.c413_control_number) ctrl_no, p_warehouse_type wh_type
  , p_type txn_type, '' ref_id, NULL opr, t412.c1900_company_id companyid, t412.c5040_plant_id plantid
  , '' shipto, '' shiptoid, '' shippeddate, '' customerpo
   FROM t413_inhouse_trans_items t413, t412_inhouse_transactions t412, t2550_part_control_number t2550
  WHERE decode(p_action,4302,t412.c412_inhouse_trans_id,t412.c412_ref_id) = p_trans_id
    AND t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
    --AND t412.c412_status_fl        = '4' -- Take only controlled inhouse transaction from DHR which only has lot number
    AND t2550.c205_part_number_id  = t413.c205_part_number_id
    AND t2550.c2550_control_number = t413.c413_control_number
    AND t2550.c1900_company_id = t412.c1900_company_id
    AND t2550.c5040_plant_id = t412.c5040_plant_id
    AND NVL(t2550.c901_lot_controlled_status,'-999') =  decode(p_action,4302,'105008','-999')
    AND t412.c412_void_fl         IS NULL
    AND t413.c413_void_fl         IS NULL;
END gm_fch_txn_dhr_data;

/********************************************************
	* Author      : APrasath
	* Description : Update Lot status in t2550
* ******************************************************/
PROCEDURE gm_upd_lot_status(
	p_type               IN   t205c_part_qty.c901_type%TYPE
  , p_txn_id			    IN   t5060_control_number_inv.c5060_last_update_trans_id%TYPE
  , p_part_num        IN   t5060_control_number_inv.c205_part_number_id%TYPE
  , p_control_num   	IN   t5060_control_number_inv.c5060_control_number%TYPE  
  , p_warehouse_type   IN   t205c_part_qty.c901_transaction_type%TYPE
  , p_action             IN   t205c_part_qty.c901_action%TYPE
  , p_user            IN   t101_user.c101_user_id%TYPE
  , p_company_id			IN  t5060_control_number_inv.c1900_company_id%TYPE
  , p_plant_id				IN  t5060_control_number_inv.c5040_plant_id%TYPE
  )
AS
  v_status  NUMBER;
BEGIN
	v_status:= NULL;
  	 IF p_type = '4312' AND  p_warehouse_type='90812' AND p_action='4301'-- increasing qty returns hold
	 THEN
	 	v_status:= 105008; --should be update as controlled. 
	 ELSIF p_type = '50982' AND  p_warehouse_type='90801' AND (p_action='4301' OR p_action='4302')-- increasing qty DHR 
	 THEN
	 	v_status:= 105008; --should be update as controlled. 
	 ELSE
	  	v_status:= NULL;
	 END IF;
	 
	 UPDATE t2550_part_control_number
	    SET c901_lot_controlled_status = v_status,
	    c2550_last_updated_trans_id = p_txn_id,
	    c2550_last_updated_date = SYSDATE,
	    c2550_last_updated_by = p_user
	  WHERE c205_part_number_id = p_part_num
	    AND c2550_control_number = p_control_num
	    AND c1900_company_id = p_company_id
	    AND c5040_plant_id = p_plant_id;  
END gm_upd_lot_status;


/********************************************************
	* Author      : APrasath
	* Description : Call from Lot Tracking to update to lot data (t5060) and log (t5061)
* ******************************************************/
PROCEDURE gm_upd_lot_data(
  p_type               IN   t205c_part_qty.c901_type%TYPE
  ,	p_part_number_id     IN   t205c_part_qty.c205_part_number_id%TYPE  
  , p_trans_id           IN   t205c_part_qty.c205_last_update_trans_id%TYPE
  , p_warehouse_type   IN   t205c_part_qty.c901_transaction_type%TYPE
  , p_action             IN   t205c_part_qty.c901_action%TYPE
  ,p_updated_by         IN   t205c_part_qty.c205_last_updated_by%TYPE
  ,p_cur TYPES.cursor_type
  )
AS    
    cur_lot TYPES.cursor_type;
    v_ref_type NUMBER;
    v_part_number t205_part_number.c205_part_number_id%TYPE;
    v_ctrl_number t5055_control_number.c5055_control_number%TYPE;
    v_qty t5060_control_number_inv.c5060_qty%TYPE;
    v_curqty t5060_control_number_inv.c5060_qty%TYPE;
    v_tablename VARCHAR2(10);
    v_txn_id t214_transactions.c214_id%TYPE;
    v_wh_type VARCHAR2(50);
    v_txn_type VARCHAR2(50);
    v_ref_id VARCHAR2(50);
    v_lot_ref_id VARCHAR2(50);
    v_flag VARCHAR2(1);
    v_lot_update CHAR(1):='Y';
    v_company_id NUMBER;
    v_plant_id   NUMBER;
    v_status BOOLEAN;
    v_error_msg VARCHAR2(1000); 
    v_skip_valid	t906_rules.c906_rule_value%TYPE;    
    v_shipto t501_order.c501_ship_to%TYPE; 
    v_shiptoid t501_order.c501_ship_to_id%TYPE; 
    v_shippeddate t501_order.c501_shipping_date%TYPE;
    v_customerpo t501_order.C501_CUSTOMER_PO%TYPE;
    v_Order_Mode	t906_rules.c906_rule_value%TYPE;
    v_ipad_skipfl	t906_rules.c906_rule_value%TYPE;
BEGIN   
	
   v_status:= FALSE;
   LOOP	
    fetch p_cur INTO v_tablename, v_txn_id, v_part_number,v_qty,v_ctrl_number,v_wh_type,v_txn_type, v_ref_id, v_ref_type,v_company_id,v_plant_id,
                     v_shipto, v_shiptoid, v_shippeddate, v_customerpo;
    exit
    WHEN p_cur%notfound;
    --validating the part is required to track lot from DHR		  	
	    SELECT DECODE(p_type,'50982',get_part_attr_value_by_comp(v_part_number,'104480',v_company_id),'Y') INTO v_flag FROM DUAL; --50982 dhr;	  
	   
	    IF v_company_id IS NULL OR v_plant_id IS NULL 
	    THEN
	    	SELECT C1900_COMPANY_ID , C5040_PLANT_ID 
	    		INTO v_company_id , v_plant_id
	    	FROM T2550_PART_CONTROL_NUMBER
	    	WHERE c205_part_number_id = v_part_number
	    	AND c2550_control_number = v_ctrl_number;
	    END IF;
	    
	   IF v_flag = 'Y' 
	   THEN    
		      v_ref_type  := NULL;
		      
		      IF p_type = '4310' AND  p_warehouse_type='4000339' AND p_action='4302' THEN --control number with order      
			        --Get the Warehouse, inv/FS/ACC and qty for give part and Lot
			      BEGIN
			        SELECT c901_warehouse_type, c5060_ref_id, c5060_qty
			        INTO v_wh_type, v_lot_ref_id, v_curqty
			        FROM t5060_control_number_inv t5060
			        WHERE t5060.c205_part_number_id = v_part_number
			        AND t5060.c5060_control_number = v_ctrl_number
			        AND t5060.c5060_qty > 0
			        AND t5060.c1900_company_id = v_company_id
			        AND t5060.c5040_plant_id = v_plant_id;
			        
			        v_ref_id := v_lot_ref_id; -- Get ref id where the Lot is currently
			        -- Get Ref type based on Warehouse type
				    SELECT get_lot_ref_type(v_wh_type) 
				    INTO v_ref_type 
				    FROM DUAL;  
			        
			      EXCEPTION
			      WHEN no_data_found THEN -- Update flag as 'N' if Lot not fond
			        v_lot_update:='N';
			        v_lot_ref_id := NULL;
			        v_wh_type := NULL;
			        v_curqty:=0;
			      END;      
		      ELSE      
		      	  -- Get Ref type based on Warehouse type
			      SELECT get_lot_ref_type(v_wh_type) 
			      INTO v_ref_type 
			      FROM DUAL;  
			       -- Get the current Qty
			      BEGIN
			        SELECT c5060_qty
			          INTO v_curqty
			          FROM t5060_control_number_inv t5060
			         WHERE t5060.c205_part_number_id = v_part_number
			           AND t5060.c5060_control_number = v_ctrl_number
			           AND t5060.c901_warehouse_type = v_wh_type
			           AND nvl(t5060.c5060_ref_id,'9999') = nvl(v_ref_id,'9999')
			           AND nvl(t5060.c901_ref_type,9999) =  nvl(v_ref_type,9999)
			           AND t5060.c5060_qty > 0
			           AND t5060.c1900_company_id = v_company_id
			       	   AND t5060.c5040_plant_id = v_plant_id;           
			         --FOR UPDATE;
			      EXCEPTION
			      WHEN no_data_found THEN
			        v_curqty:=0;
			      END;      
		      END IF;     
		        
		          
		      v_curqty := v_curqty + v_qty;
		      --check for Return 
		      IF v_txn_type = 102908 THEN
			      BEGIN
			      	SELECT c901_WAREHOUSE_TYPE,
                           c5060_REF_ID,
                           c901_REF_TYPE
                     	 INTO v_wh_type, v_ref_id, v_ref_type
                       FROM T5060_CONTROL_NUMBER_INV
                    	 WHERE C205_PART_NUMBER_ID = v_part_number
                    	 AND C5060_CONTROL_NUMBER = v_ctrl_number
                    	 AND C5060_QTY > 0;
                    EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                    	 NULL;
				   END;
			  END IF;
			  
		      --Insert or Update T5060
		     gm_pkg_op_lot_track.gm_sav_control_number_inv(v_part_number,v_ctrl_number,v_wh_type,v_curqty,v_txn_id,v_txn_type,v_ref_type,v_ref_id,p_updated_by,v_company_id,v_plant_id,
		                                                    v_shipto, v_shiptoid, v_shippeddate, v_customerpo);		       
		      --updating lot status in t2550 for order	  
			   gm_upd_lot_status(p_type,v_txn_id,v_part_number,v_ctrl_number,p_warehouse_type,p_action,p_updated_by,v_company_id,v_plant_id);			   
			   
       	END IF;
    v_status:= TRUE;
  	END LOOP;
  
  IF p_cur%isopen THEN
  	 CLOSE p_cur;
  END IF;
  
   
  /* Skip the below validation for the Orders generated from the IPAD's*/
  	BEGIN 
	          SELECT c501_receive_mode 
	          		INTO v_Order_Mode 
	          	FROM t501_order
	          	WHERE c501_order_id= p_trans_id 
	          	AND c501_void_fl IS NULL ;
	          
			EXCEPTION
			      WHEN no_data_found THEN
			        v_Order_Mode:=NULL;
			        
			SELECT get_rule_value (v_Order_Mode,'IPADSKIPFL')    
		  	 	INTO v_ipad_skipfl
		  	 FROM DUAL;
	  END;
	  
IF v_ipad_skipfl != 'Y' THEN  --IPAD
  IF v_status <> TRUE AND p_part_number_id IS NOT NULL THEN
  -- Need to skip this validation for IAFG,IAQN and IAPN related transactions
  v_skip_valid := get_rule_value(p_type,'SKIPVALID');
	  IF v_skip_valid IS NULL OR v_skip_valid <> 'Y' THEN
		  v_error_msg:= 'Unable to find data to track the lot for Transaction '||p_trans_id ||' and part number '||p_part_number_id;
				            		raise_application_error
				                           ('-20999',
				                            v_error_msg
				                           );
	  END IF;
  	END IF;
  END IF;
END gm_upd_lot_data;


/********************************************************
	* Author      : APrasath
	* Description : Update or Insert t5060_control_number_inv based on part number, control number, warehouse, Ref id and Ref type 
*******************************************************/
PROCEDURE gm_sav_control_number_inv (
  p_part_num						IN   t5060_control_number_inv.c205_part_number_id%TYPE
  , p_control_number		IN   t5060_control_number_inv.c5060_control_number%TYPE
  , p_warehouse_type		IN   t5060_control_number_inv.c901_warehouse_type%TYPE
  , p_qty								IN   t5060_control_number_inv.c5060_qty%TYPE
  , p_last_trans_id			IN   t5060_control_number_inv.c5060_last_update_trans_id%TYPE
  , p_last_trans_type   IN   t5060_control_number_inv.c901_last_transaction_type%TYPE
  , p_ref_type 					IN   t5060_control_number_inv.c901_ref_type%TYPE
  , p_ref_id 						IN   t5060_control_number_inv.c5060_ref_id%TYPE
  , p_user							IN   t101_user.c101_user_id%TYPE
  , p_company_id			IN  t5060_control_number_inv.c1900_company_id%TYPE
  , p_plant_id				IN  t5060_control_number_inv.c5040_plant_id%TYPE
  , p_shipto                IN t501_order.c501_ship_to%TYPE
  , p_shiptoid              IN t501_order.c501_ship_to_id%TYPE 
  , p_shippeddate           IN t501_order.c501_shipping_date%TYPE
  , p_customerpo            IN t501_order.C501_CUSTOMER_PO%TYPE
)
AS
BEGIN
  -- If same Part number, Control number, warehouse, ref_id and ref_type record available, update it else insert new record
  UPDATE t5060_control_number_inv t5060
    SET 
      c5060_qty = p_qty,
      c5060_last_update_trans_id = p_last_trans_id,
      c901_last_transaction_type = p_last_trans_type,
      c5060_last_updated_by = p_user, 
      c5060_last_updated_date = SYSDATE,
      c1900_company_id = p_company_id ,
      c5040_plant_id = p_plant_id,
      C901_SHIP_TO_TYPE = p_shipto,
      C5060_SHIP_TO_ID = p_shiptoid,
      C5060_SHIPPED_DT = p_shippeddate,
      C5060_CUSTOMER_PO = p_customerpo
   WHERE t5060.c205_part_number_id = p_part_num
     AND t5060.c5060_control_number = p_control_number
     AND t5060.c901_warehouse_type = p_warehouse_type
     AND nvl(t5060.c5060_ref_id,'9999') = nvl(p_ref_id,'9999')
     AND nvl(t5060.c901_ref_type,9999) = nvl(p_ref_type,9999);
  IF SQL%rowcount = 0
  THEN
  
    INSERT INTO t5060_control_number_inv 
      (c5060_control_number_inv_id, c5060_control_number, c205_part_number_id,c5060_qty, c5060_created_by,c5060_created_date,
      c5060_last_update_trans_id, c901_warehouse_type,c901_last_transaction_type,c5060_ref_id,c901_ref_type, c1900_company_id , c5040_plant_id,
      C901_SHIP_TO_TYPE, C5060_SHIP_TO_ID, C5060_SHIPPED_DT, C5060_CUSTOMER_PO)
    VALUES (s5060_control_number_inv.nextval, p_control_number, p_part_num, p_qty, p_user,SYSDATE,
      p_last_trans_id,p_warehouse_type,p_last_trans_type, p_ref_id, p_ref_type, p_company_id , p_plant_id,
      p_shipto, p_shiptoid, p_shippeddate, p_customerpo);
   
  END IF;
END gm_sav_control_number_inv;


/********************************************************
	* Author      : APrasath
	* Description : Update or Insert t5060_control_number_inv based on part number, control number, warehouse, Ref id and Ref type 
*******************************************************/
PROCEDURE gm_sav_control_num_inv_log (
		p_control_number_inv_id		IN   t5061_control_number_inv_log.c5060_control_number_inv_id%TYPE
	  , p_part_num							IN   t5061_control_number_inv_log.c205_part_number_id%TYPE
	  , p_control_number				IN   t5061_control_number_inv_log.c5060_control_number%TYPE
	  , p_warehouse_type				IN   t5061_control_number_inv_log.c901_warehouse_type%TYPE
	  , p_txn_qty								IN   t5061_control_number_inv_log.c5061_txn_qty%TYPE
	  , p_orig_qty							IN   t5061_control_number_inv_log.c5061_orig_qty%TYPE
	  , p_new_qty								IN   t5061_control_number_inv_log.c5061_new_qty%TYPE
	  , p_last_trans_id					IN   t5061_control_number_inv_log.c5061_last_update_trans_id%TYPE
	  , p_created_by						IN   t5061_control_number_inv_log.c5061_created_by%TYPE
    , p_ref_type 							IN   t5061_control_number_inv_log.c901_ref_type%TYPE
	  , p_last_transaction_type IN   t5061_control_number_inv_log.c901_last_transaction_type%TYPE
	  , p_ref_id 								IN   t5061_control_number_inv_log.c5060_ref_id%TYPE
	  , p_company_id			IN  t5060_control_number_inv.c1900_company_id%TYPE
  	  , p_plant_id				IN  t5060_control_number_inv.c5040_plant_id%TYPE
	  
	)
AS
  v_new_qty t5061_control_number_inv_log.c5061_new_qty%TYPE;
BEGIN
  
  v_new_qty:=p_new_qty;
  
  INSERT INTO t5061_control_number_inv_log(
      c5061_inv_log_id,
      c5060_control_number_inv_id,
      c205_part_number_id,
      c5060_control_number,
      c901_warehouse_type,
      c5061_txn_qty,
      c5061_orig_qty,
      c5061_new_qty,
      c5061_last_update_trans_id,
      c5061_created_by,
      c5061_created_date,
      c901_ref_type,
      c901_last_transaction_type,
      c5060_ref_id,
      c1900_company_id,
      c5040_plant_id      
      ) VALUES (
        s5061_control_number_inv_log.nextval,
        p_control_number_inv_id,
        p_part_num,
        p_control_number,
        p_warehouse_type,
        p_txn_qty,
        p_orig_qty,
        v_new_qty,
        p_last_trans_id,
        p_created_by,
        SYSDATE,
        p_ref_type,
        p_last_transaction_type,
        p_ref_id,
      	p_company_id,
     	p_plant_id
      );
          
END gm_sav_control_num_inv_log;




/*******************************************************
* Description : function to get ref type (distributor id or account id or location id) for a given lot
* Author    : APrasath
*******************************************************/
FUNCTION get_lot_ref_type (
 v_wh_type NUMBER)
    RETURN NUMBER
IS
    v_ref_type NUMBER;
BEGIN
       -- Update ref type based on warehouse. 
      --For FG and RW warehouse, set 103291 (Location Id).
      --For Field sales warehouse (4000339), set  103922 (FieldSales Id)
      --For Account warehouse (56002), set  103923 (Account Id)
      --Returns Hold 103924
      --DHR 103925
      SELECT decode(v_wh_type, 90800, 103921, 56001, 103921, 4000339, 103922, 56002, 103923,90812,103924,90801,103925,NULL) 
      INTO v_ref_type FROM dual;
      
    RETURN v_ref_type;
EXCEPTION
WHEN NO_DATA_FOUND THEN
    RETURN NULL;
END get_lot_ref_type;

/*******************************************************
* Description : procedure to revert the sold control nunmber status from edit control number screen
* Author    : APrasath
*******************************************************/
PROCEDURE gm_sav_edit_order_sold_cnum (
      p_order_id   IN   t501_order.c501_order_id%TYPE
    , p_pnum       IN   t502a_item_order_attribute.c205_part_number_id%TYPE
    , p_pkey 	   IN   t502a_item_order_attribute.c502a_item_order_attribute_id%TYPE
    , p_cnum  	   IN   t502a_item_order_attribute.c502a_attribute_value%TYPE
    , p_user_id    IN   t501_order.c501_created_by%TYPE
    , p_cnum_fl    IN OUT  BOOLEAN
   )
   AS
      v_plant_id      	t5040_plant_master.c5040_plant_id%TYPE;      
      v_company_id			t1900_company.c1900_company_id%TYPE;
      v_inv_id		t5060_control_number_inv.c5060_control_number_inv_id%TYPE;
      v_cnum  	   VARCHAR2(100);
      v_txn_type t501_order.c901_order_type%TYPE;
      v_flag VARCHAR2(1);
      BEGIN
	   p_cnum_fl := FALSE;
	       --validating lot tracking is available for company 
	   SELECT  NVL(get_plantid_frm_cntx(),get_rule_value('DEFAULT_PLANT','ONEPORTAL'))
		, NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL'))
			INTO  v_plant_id, v_company_id FROM DUAL;
	   
	   SELECT get_rule_value(v_company_id,'LOT_TRACK') INTO v_flag FROM DUAL; 
	   
	   IF v_flag IS NULL OR v_flag <> 'Y' 
	   THEN
		   RETURN;
	   END IF;
	    
	   --validating the part is required to track lot		  	
	   SELECT get_part_attr_value_by_comp(p_pnum,'104480',v_company_id) INTO v_flag FROM DUAL;   
	   
	   IF v_flag IS NULL OR v_flag <> 'Y' 
	   THEN
		   RETURN;
	   END IF;
	   BEGIN   
	      SELECT t502b.c502b_usage_lot_num -- Control number
			  , t501.c901_order_type  INTO v_cnum,v_txn_type
			   FROM t502b_item_order_usage t502b, t501_order t501, t2550_part_control_number t2550
			  WHERE t501.c501_order_id                    = t502b.c501_order_id
			  and t502b.c502b_item_order_usage_id = p_pkey
			    AND t501.c501_order_id                    = p_order_id
			    AND t502b.c205_part_number_id             = p_pnum
			    --AND t502a.c502a_control_number_update_fl IS NULL
			    --AND t502a.c901_attribute_type             = 101723 -- Control Number Attribute
			    --AND t502a.c502a_attribute_value = upper(p_control_number)
			    --AND t502a.c502a_item_qty = p_update_qty
			    AND t2550.c205_part_number_id  = t502b.c205_part_number_id
			    AND t2550.c2550_control_number = t502b.c502b_usage_lot_num
			    AND t2550.c1900_company_id     = t501.c1900_company_id
			    AND t2550.c5040_plant_id       = t501.c5040_plant_id
			    --AND t2550.c2550_lot_status NOT IN (105000,105006) --Implanted and sold (commented for PMT-29681)
			    AND t502b.c502b_void_fl       IS NULL
			    AND t501.c501_void_fl         IS NULL;
		EXCEPTION
			      WHEN NO_DATA_FOUND THEN 			        
			        v_cnum := NULL;
			        v_txn_type := NULL;
		END;    
      	IF v_cnum IS NOT NULL AND p_cnum IS NOT NULL AND p_cnum <> v_cnum THEN
		--updating status from sold to null
		      			    
			    UPDATE t2550_part_control_number
                SET c2550_lot_status = 105007,--consigned
                c901_last_updated_warehouse_type = 4000339, --field sales
                c2550_last_updated_trans_id = p_order_id,
                c2550_last_updated_date = CURRENT_DATE,
                c2550_last_updated_by = p_user_id
             WHERE c205_part_number_id = p_pnum
                AND c2550_control_number = v_cnum
                AND c2550_lot_status IN (105000,105006)--Implanted and sold
                and c1900_company_id = v_company_id
                and c5040_plant_id = v_plant_id;
			    	        
	        
	        SELECT MAX (t5060.c5060_control_number_inv_id) INTO v_inv_id
			   FROM t5060_control_number_inv t5060
			  WHERE t5060.c901_warehouse_type IN (56002, 4000339)--Account and FS warehouse
			    AND t5060.c205_part_number_id  = p_pnum
			    AND t5060.c5060_control_number = v_cnum
			    AND c5060_qty                  = 0
			    AND c1900_company_id           = v_company_id
			    AND c5040_plant_id             = v_plant_id;
	       
		  IF v_inv_id IS NOT NULL THEN 
	       -- increasing lot qty in FS or account warehouse
			   UPDATE t5060_control_number_inv t5060
			SET c5060_qty                             = 1, c5060_last_update_trans_id = p_order_id, c901_last_transaction_type = v_txn_type
			  , c5060_last_updated_by                 = p_user_id, c5060_last_updated_date = CURRENT_DATE
			  WHERE t5060.c205_part_number_id         = p_pnum
			    AND t5060.c5060_control_number        = v_cnum
			    AND t5060.c5060_control_number_inv_id = v_inv_id;
		  END IF;   
			p_cnum_fl := TRUE;
			    
		END IF;

END gm_sav_edit_order_sold_cnum;
/***********************************************************
 * Description : Procedure to validating control number 
 *                which is available and transaction type.
 *  Parameter: type,transaction id,action, warhouse type
 * ,part number id and control number
 *  Return: reference type.                
 ***********************************************************/   
 PROCEDURE gm_validate_input_data (
    p_reftype			IN		t5055_control_number.c901_ref_type%TYPE	
  , p_trans_id			IN		t5055_control_number.c5055_ref_id%TYPE	
  , p_action  			IN		VARCHAR2
  , p_warehouse_type	IN		VARCHAR2
  , p_pnum_id     		IN		t205c_part_qty.c205_part_number_id%TYPE
  , p_control_num     	IN		t5055_control_number.c5055_control_number%TYPE
  , p_qty				IN		NUMBER
  , p_out_err_msg		OUT		CLOB
)
AS
	v_err_msg			VARCHAR2(4000) := '';
	v_out_err_msg		CLOB := '';
	v_country			NUMBER;
	v_err_code			VARCHAR2(1000);
	v_out_fl			VARCHAR2(1);
	v_rule_warehouse	t906_rules.c906_rule_value%TYPE;
	v_rule_warehouse2	t906_rules.c906_rule_value%TYPE;
	v_curr_warehouse	t5060_control_number_inv.c901_warehouse_type%TYPE;
	v_rule_inv_nm		VARCHAR2(100);
	v_curr_inv_nm		t901_code_lookup.c902_code_nm_alt%TYPE;
	v_txn_type_id		t906_rules.c906_rule_value%TYPE;
	v_action  			VARCHAR2(20);
	v_warehouse_type 	t906_rules.c906_rule_value%TYPE;
	v_lot_status		t2550_part_control_number.c2550_lot_status%TYPE;
	v_controlled_lot_status		t2550_part_control_number.C901_LOT_CONTROLLED_STATUS%TYPE;
	v_pickput_fl		VARCHAR2(20);
	v_lot_cnt			NUMBER;
	v_wh_validate_fl	VARCHAR2(1) := 'N';
	v_plant_id      	t5040_plant_master.c5040_plant_id%TYPE;
	v_phy_wh_fl			t906_rules.c906_rule_value%TYPE;
	v_wh_skip_fl		t906_rules.c906_rule_value%TYPE;
	v_substring     	VARCHAR2(50);
	v_string     		VARCHAR2(50);
	v_validlotchk_fl	t906_rules.c906_rule_value%TYPE;
	v_comp_id			t1900_company.c1900_company_id%TYPE;
	v_shipped_cnt		NUMBER := 0;
	v_rule_warehouse3   t906_rules.c906_rule_value%TYPE;  
	v_count             NUMBER;           
	v_acct_id_count		NUMBER;	 
	v_ra_txn_type_count NUMBER;                
BEGIN
	
	SELECT  NVL(get_plantid_frm_cntx(),get_rule_value('DEFAULT_PLANT','ONEPORTAL'))
		, NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL'))
	INTO  v_plant_id, v_comp_id FROM DUAL;
	v_txn_type_id := get_rule_value(p_reftype,'TXN_RULE');
	v_rule_warehouse := get_rule_value(v_txn_type_id,'RLTXNWAREHOUSE');
	v_wh_skip_fl := get_rule_value(v_txn_type_id,'SKPWHVALID');
	
	/*
	 * 93343: Pick; 93345: Put
	 * 4302: minus; 4301: plus
	 * */
	SELECT DECODE(p_action, 93343, 4302, 93345, 4301) INTO v_action from DUAL;
	SELECT DECODE(p_action, 93343, 'pick', 93345, 'put') INTO v_pickput_fl from DUAL;
	
	/* To validate the warehouse type
	 * Whether the From warehouse and To warehouse is valid or not based on the rule value
	 */
	BEGIN
	  	
		 -- validating the input data and throwing error	 
		v_warehouse_type := gm_pkg_op_lot_track.get_transaction_valid_data(p_reftype,'LOT_TRACK_TYPE',v_action,p_pnum_id,p_control_num);	         
		
		IF v_warehouse_type IS NULL AND v_wh_skip_fl != 'Y' THEN
			v_err_msg:= 'Ware House not defined for '||p_trans_id  || '; ';
		--validating the control number which shipped out from consignment or order replenishment	
		ELSIF p_reftype='101723' AND v_warehouse_type IS NOT NULL AND v_warehouse_type <> '103921' THEN --if part not available from field
			v_err_msg:= 'Control number found in Invalid location for the Transaction-'||p_trans_id  || '; ';
		--otherthan order
		ELSIF p_reftype<>'101723' AND v_warehouse_type IS NOT NULL AND p_warehouse_type <> v_warehouse_type AND v_wh_skip_fl != 'Y' THEN
			v_err_msg:= 'Invalid Warehouse for the Transaction-'||p_trans_id || '; ';	
		END IF;
		
	END; 

	v_out_err_msg := v_err_msg;
	
	
	
	 /* Adding the fetch Column  C901_LOT_CONTROLLED_STATUS PC-1046
	    This is required since the column c901_lot_controlled_status is updated when the transaction is controlled with a Lot numbers
	 	And this should not be used in any other transaction before shipping.
	 */
		
	BEGIN
	
		SELECT C901_LOT_CONTROLLED_STATUS INTO v_controlled_lot_status
		  FROM t2550_part_control_number
		WHERE c205_part_number_id = p_pnum_id
		  AND c2550_control_number  = p_control_num
		  AND c5040_plant_id = v_plant_id;
		  
	EXCEPTION
	
		WHEN OTHERS THEN
		v_controlled_lot_status:= '-999';
	END;
	
	/*
	 * Validate whether the control number entered is used for other transaction or not
	 * 105008: Controlled
	 */
	BEGIN
		SELECT c2550_lot_status INTO v_lot_status
		  FROM t2550_part_control_number
		WHERE c205_part_number_id = p_pnum_id
		  AND c2550_control_number  = p_control_num
		  AND c5040_plant_id = v_plant_id;
	EXCEPTION
	WHEN NO_DATA_FOUND THEN
		/* If the rule engine is not available for a transaction, then we need to throw the validaiton if the lot number is not available in t2550 table
		 * Otherwise lot engine will take care of this
		*/
		v_validlotchk_fl := get_rule_value(p_reftype,'VALIDLOTCHK');
		IF v_validlotchk_fl IS NULL OR v_validlotchk_fl <> 'Y' THEN
			RETURN;-- If the control number is not available in t2550_part_control_number table rule validaiton is firing
		ELSE
			v_lot_status := NULL;
			v_err_msg:= '<B>' || p_control_num || '</B> for part # <B>' || p_pnum_id  || '</B> is not available, cannot use it.';
			v_out_err_msg := v_out_err_msg || v_err_msg || '; ';
		END IF;
	END;
	
	/* PC-1046 As part of this PC, we are adding the condition check for Controlled Lot status variable. Per T2550_part_control_number table, c2550_lot_status column does
	not contain the value  105008 rather C901_LOT_CONTROLLED_STATUS column has the value 105008. So the variable name is changed here.
	
	105008:Controlled; 105006: Sold; 105000: Implanted */
	IF v_controlled_lot_status = '105008' OR v_lot_status = '105006' OR v_lot_status = '105000' 
	THEN
		v_err_msg:= 'The control number is already used for another transaction';
	  
		SELECT COUNT(1)                  /* This error should not show if transaction is RA and lot status are sold and implanted PMT-34056 : Unable to complete direct sales RA in BBA  */
		  INTO v_count
		  FROM t506_returns 
		 WHERE C506_RMA_ID = p_trans_id 
		   AND C506_VOID_FL IS NULL ;     
          
		   IF v_count = 1 AND (v_lot_status = '105006' OR v_lot_status = '105000') THEN
              v_err_msg:=NULL;
           END IF;	                  
		v_out_err_msg := v_out_err_msg || v_err_msg || '; ';
	END IF;
	
	

	
	/* To validate the qty of lot number
	 * The control number should be unique for tissue parts, if the same control number is given for two quanities, it should show error
	 */
	
	IF p_qty > 1 AND (p_control_num IS NOT NULL OR p_control_num != '')
	THEN
		v_err_msg := 'Lot numbers are valid for 1 unit only. Please enter Unique lot numbers for each unit slot';
		v_out_err_msg := v_out_err_msg || v_err_msg || '; ';
	ELSIF p_qty = 1 AND (p_control_num IS NOT NULL OR p_control_num != '')
	THEN
		SELECT MAX(v_lot_cnt) INTO v_lot_cnt
		FROM
		  (SELECT COUNT(1) v_lot_cnt
		  	FROM t5055_control_number
		  WHERE c5055_control_number = p_control_num
			  AND c5055_ref_id           = p_trans_id
			  AND c5040_plant_id         = v_plant_id
			  AND c5055_void_fl         IS NULL
		  UNION
		  SELECT COUNT(1) v_lot_cnt
		  	FROM t413_inhouse_trans_items
		  WHERE c413_control_number = p_control_num
			  AND c412_inhouse_trans_id = p_trans_id
			  AND c413_void_fl         IS NULL
		  UNION
		  SELECT COUNT(1) v_lot_cnt
		    FROM t507_returns_item
		  WHERE c507_control_number = p_control_num
		  	   AND c506_rma_id = p_trans_id
		  	   AND c507_status_fl <> 'Q'
		  );	
			
		IF v_lot_cnt >0 THEN
			v_err_msg := 'Cannot use same Control Number for Tissue Part for the current transaction';
			v_out_err_msg := v_out_err_msg || v_err_msg || '; ';
		END IF;
	END IF;
	
	/* To validate the warehouse
	 * This is to check where the part number is available currently and showing error message if it is not available in the proper inventory
	 */
	--IF v_rule_warehouse IS NOT NULL
	--THEN
		gm_pkg_op_inv_warehouse.gm_fetch_curr_warehouse(p_pnum_id,p_control_num,v_curr_warehouse); 
		v_curr_inv_nm := GET_CODE_NAME_ALT(v_curr_warehouse);
		
			
	
	/*
	PMT-49746 Ability to process Viacell Returns in BBA
	Below condition will get executed only for BBA company. For the Rule group BBA_WAREHOUSE_SKIP, account id is mapped to the rule ids.
	If the account id [rule id column] in rule table match with [account id] in t506_returns table then we need to skip the warehouse check.
	By this way we can add multiple account in rule table on needed basis in future.
	
	*/
	
	-- Below select is get the count if the txn id is GM-RA or GM-CRA
			SELECT  COUNT (1)
			 INTO 
			v_ra_txn_type_count 
    		FROM DUAL
    	     WHERE REGEXP_LIKE(p_trans_id, 'GM-RA') OR	REGEXP_LIKE(p_trans_id, 'GM-CRA');
	
	IF(v_comp_id='1001' AND v_ra_txn_type_count>0) THEN
	
	  SELECT  count(1) 
	    INTO v_acct_id_count
      FROM t906_rules t906 , t506_returns T506
 	   WHERE t906.c906_rule_grp_id='BBA_WAREHOUSE_SKIP'
 	   AND t906.c906_rule_id=t506.c704_account_id
	   and t906.c1900_company_id=v_comp_id
	   AND t906.c906_active_fl='Y'
   	   AND t906.c906_void_fl IS NULL
   	   AND t506.C506_RMA_ID=p_trans_id
 	   AND t506.c506_void_fl IS NULL;
 	  
 	  -- If the account id count is greater than 0 then reset the warehouse details so that the validation will not get triggred. 
 	   IF (v_acct_id_count>0) THEN
 	   
 	   v_curr_warehouse:='';
 	   v_curr_inv_nm:='';
 	   
 	   END IF;
 	    	    
   	END IF;
	
		
	--END IF;
	IF INSTR (v_rule_warehouse, '|') = 0 THEN
		v_rule_inv_nm := GET_CODE_NAME_ALT(v_rule_warehouse);
	ELSE
		v_string := v_rule_warehouse;
		-- Get the count of control number which is shipped out
		SELECT COUNT(1) INTO v_shipped_cnt
			FROM t501_order T501, t502_item_order T502
		WHERE t501.c501_order_id = t502.c501_order_id
			AND t502.c502_control_number = p_control_num
			AND t501.c501_status_fl = 3 --shipped
			AND t501.c501_void_fl IS NULL
			AND t502.c502_void_fl IS NULL
			AND t501.c1900_company_id = v_comp_id;
		
	END IF;

	WHILE INSTR (v_string, '|') <> 0
  	LOOP
	    v_substring  		:= SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
	    v_string     		:= SUBSTR (v_string, INSTR (v_string, '|')    + 1);
	    v_rule_warehouse   	:= NULL;
	    v_rule_warehouse2   := NULL;
	    v_rule_warehouse3   := NULL;
	    v_rule_warehouse   	:= SUBSTR (v_substring, 1, INSTR (v_substring, ',')      - 1);
	    v_substring  		:= SUBSTR (v_substring, INSTR (v_substring, ',')         + 1);
	    v_rule_warehouse2   := SUBSTR (v_substring, 1, INSTR (v_substring, ',')      - 1);  /*Added the v_rule_warehouse3 substring to Sold warehouse PMT-34056 : Unable to complete direct sales RA in BBA */  
	    v_substring         := SUBSTR (v_substring, INSTR (v_substring, ',')         + 1);    
        v_rule_warehouse3   := v_substring;                                                 
	    v_rule_inv_nm := GET_CODE_NAME_ALT(v_rule_warehouse) || '/' ||GET_CODE_NAME_ALT(v_rule_warehouse2) || '/' ||GET_CODE_NAME_ALT(v_rule_warehouse3);  
	    
	   
	    
	END LOOP;

	/*
	 * For IAFG, IAPN and IAQN the lot number should not be availbale in any of the physical inventory  
	 */
	 

	 
	 
	IF v_rule_warehouse IS NULL
	THEN
		v_phy_wh_fl := get_rule_value(v_curr_warehouse,'EXCLUDEWH');
		IF v_phy_wh_fl = 'Y'
		THEN
			v_err_msg := 'The Control Number ' || p_control_num ||' entered for the part ' || p_pnum_id || ' is currently available in '|| v_curr_inv_nm ||', cannot use it';
			v_wh_validate_fl := 'Y';-- if the ware house is validated, then no need to show the availability of control number
			v_out_err_msg := v_out_err_msg || v_err_msg || '; ';
		END IF;
	END IF;
	
	/*
	 * if the transaction is other than Order Attribute, then v_rule_warehouse2 will be null
	 */
	IF v_rule_warehouse2 IS NOT NULL THEN
		IF v_curr_warehouse IS NOT NULL AND v_curr_warehouse <> v_rule_warehouse AND v_curr_warehouse <> v_rule_warehouse2 AND v_curr_warehouse <> v_rule_warehouse3    
		THEN
			v_err_msg := 'The Control Number ' || p_control_num ||' entered for the part ' || p_pnum_id || ' is currently not available in '|| v_rule_inv_nm || ' and it is available in ' || v_curr_inv_nm;
			v_wh_validate_fl := 'Y';-- if the ware house is validated, then no need to show the availability of control number
			v_out_err_msg := v_out_err_msg || v_err_msg || '; ';
		ELSIF v_curr_warehouse IS NULL AND v_shipped_cnt <0
		THEN
			v_err_msg := 'The Control Number ' || p_control_num ||' entered for the part ' || p_pnum_id || ' is not shipped out';
			v_wh_validate_fl := 'Y';-- if the ware house is validated, then no need to show the availability of control number
			v_out_err_msg := v_out_err_msg || v_err_msg || '; ';
		END IF;
	ELSE
		IF v_curr_warehouse IS NOT NULL AND v_curr_warehouse <> v_rule_warehouse THEN
			v_err_msg := 'The Control Number ' || p_control_num ||' entered for the part ' || p_pnum_id || ' is currently not available in '|| v_rule_inv_nm || ' and it is available in ' || v_curr_inv_nm;
			v_wh_validate_fl := 'Y';-- if the ware house is validated, then no need to show the availability of control number
			v_out_err_msg := v_out_err_msg || v_err_msg || '; ';
		/*
		 * If the current warehouse is null, then it means the control number is shipped out.
		 * v_wh_validate_fl : if none of the above warehouse validation is not done
		 * v_wh_skip_fl: If the transaction does not comes under IA transactions
		 * Then validate the control number and display the validation if the control number is not available in invnetory
		 */
		ELSIF v_curr_warehouse IS NULL AND v_wh_validate_fl = 'N' AND (v_wh_skip_fl != 'Y' OR v_wh_skip_fl IS NULL OR v_wh_skip_fl = ' ')
		THEN
			v_err_msg := 'The Control Number ' || p_control_num ||' entered for the part ' || p_pnum_id || ' is not available in the inventory';
			v_wh_validate_fl := 'Y';-- if the ware house is validated, then no need to show the availability of control number
			v_out_err_msg := v_out_err_msg || v_err_msg || '; ';
		END IF;
	END IF;
	
	/*
	 * Validate whether the transaction is available in the inventory; 90800: FG inventory
	 */
	IF v_wh_validate_fl = 'N' AND (v_wh_skip_fl != 'Y' OR v_wh_skip_fl IS NULL OR v_wh_skip_fl != ' ') AND v_rule_warehouse = '90800' THEN
		BEGIN
			gm_pkg_op_item_control_txn.gm_validate_fg_inv_qty(p_trans_id, p_pnum_id, p_control_num);
			EXCEPTION WHEN OTHERS THEN
			          v_err_code := SQLCODE;   
				-- If the exception code is same as -20999(error code of validation message), set the error message to out parameter
				If v_err_code = -20999 THEN
					v_err_msg := SQLERRM;
					v_out_err_msg := v_out_err_msg || SUBSTR(v_err_msg,INSTR(v_err_msg,':')+1,LENGTH(v_err_msg)) || '; ';
				ELSE
					raise;--For other exceptions
				END IF;
		END;
	END IF;
	/*
	 * Remove the last semi-colon in the error detail messages
	 */
	IF v_out_err_msg IS NOT NULL THEN 
		SELECT SUBSTR(trim(v_out_err_msg) , 1, INSTR(trim(v_out_err_msg) , ';', -1)-1)
    		INTO v_out_err_msg 
    		FROM dual;
    END IF;
	p_out_err_msg := v_out_err_msg;
	
END gm_validate_input_data;


/***********************************************************
 * Description : Procedure to get validation data                
 *  Parameter: id, transaction type,desc, pnum and cnum 
 *  Return: warehouse type.                
 ***********************************************************/   
 FUNCTION get_transaction_valid_data (
  	p_id	 t906_rules.c906_rule_id%TYPE
  , p_grp_id	 t906_rules.c906_rule_grp_id%TYPE
  , p_desc		   t906_rules.c906_rule_desc%TYPE
  , p_part_number_id     t205c_part_qty.c205_part_number_id%TYPE
  , p_control_number     t5055_control_number.c5055_control_number%TYPE
)
	RETURN VARCHAR2
IS
	v_warehouse_type t906_rules.c906_rule_value%TYPE;

	BEGIN
	   
 	 BEGIN  
	 IF p_grp_id = '101723' --calling while raising order and it has control numbers which used in surgery 
	 THEN
		-- SELECT c901_ref_type
       -- INTO v_warehouse_type 
       -- FROM t5060_control_number_inv t5060
      -- WHERE t5060.c205_part_number_id = p_part_number_id
        -- AND t5060.c5060_control_number = p_control_number
        -- AND t5060.c5060_qty > 0;
        
          SELECT c901_ref_type  --Get the data from t2550 table for PMT-29681
        INTO v_warehouse_type
	  FROM t2550_part_control_number t2550
	WHERE t2550.c205_part_number_id = p_part_number_id
	 AND t2550.c2550_control_number = p_control_number;
	
	
	ELSE --validating the transaction warehouse
		select c906_rule_value
		  INTO v_warehouse_type
		  from t906_rules
		 WHERE c906_rule_id = p_id 
		   AND c906_rule_grp_id = p_grp_id 
		   and c906_rule_desc=p_desc;
    END IF;
	EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		RETURN '';
	END;	
	RETURN v_warehouse_type;
END get_transaction_valid_data;


/***********************************************************
 * Description : Procedure to validating control number 
 *                which is available and transaction type.
 *  Parameter: type,transaction id,action, warhouse type
 * ,part number id and control number
 *  Return: reference type.                
 ***********************************************************/   
 PROCEDURE gm_validate_input_data (
    p_reftype			IN		t5055_control_number.c901_ref_type%TYPE	
  , p_trans_id			IN		t5055_control_number.c5055_ref_id%TYPE	
  , p_action  			IN		VARCHAR2
  , p_warehouse_type	IN		VARCHAR2
  , p_pnum_id     		IN		t205c_part_qty.c205_part_number_id%TYPE
  , p_control_num     	IN		t5055_control_number.c5055_control_number%TYPE
  , p_qty				IN		NUMBER
  , p_pnum_str			IN		CLOB
  , p_out_err_msg		OUT		CLOB
)
AS
	v_string        CLOB := p_pnum_str;
	v_substring     CLOB;
	v_pnum          t5055_control_number.c205_part_number_id%TYPE;
  	v_qty           t5055_control_number.c5055_qty%TYPE;
  	v_control       t5055_control_number.c5055_control_number%TYPE;
  	v_err_string	CLOB;
  	v_err_msg		CLOB;
  	v_flag			VARCHAR2(1) := 'N';
  	v_comp_id		t1900_company.c1900_company_id%TYPE;
BEGIN
	
	SELECT  NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL'))
	INTO  v_comp_id FROM DUAL;
			
	WHILE INSTR (v_string, '|') <> 0
  	LOOP
	    v_substring  := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
	    v_string     := SUBSTR (v_string, INSTR (v_string, '|')    + 1);
	    v_pnum       := NULL;
	    v_qty        := NULL;
	    v_control    := NULL;
	    v_pnum       := SUBSTR (v_substring, 1, INSTR (v_substring, ',')      - 1);
	    v_substring  := SUBSTR (v_substring, INSTR (v_substring, ',')         + 1);
	    v_control    := TRIM(SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1));
	    v_substring  := SUBSTR (v_substring, INSTR (v_substring, ',')         + 1);
	    v_qty        := to_number(v_substring);
	    
	    /*
	     * Get the part attribute type to validate the lot number from the part number setup screen
	     * If the attribute value is 'Y' validate the lot number for the part, otherwise skip the validation
	     * 104480: Lot tracking required ?
	     */
	     v_flag := get_part_attr_value_by_comp(v_pnum,'104480',v_comp_id);

	     -- If the attribute is not set for the part, then should not validat the part
	     IF v_flag IS NULL OR v_flag <> 'Y' THEN
	     	CONTINUE;
	     END IF;
		IF v_control IS NOT NULL OR v_control != ' '
		THEN
	    	gm_pkg_op_lot_track.gm_validate_input_data(p_reftype,p_trans_id,p_action,p_warehouse_type,v_pnum,v_control,v_qty, v_err_msg);
	    END IF;
	    
	    IF v_err_msg != ' ' AND v_err_msg IS NOT NULL THEN
	    	v_err_string := v_err_string || '<B>' ||v_pnum || '|' || v_control ||'</B>' || ' : '||  v_err_msg || '<BR>';
	    END IF;
	    
	END LOOP;
	
	p_out_err_msg := v_err_string;
END gm_validate_input_data;

/***********************************************************
 *  Description : Procedure to update the control number status
 *  Parameter   : order id
 *  Author      : karthik
 ***********************************************************/   
 PROCEDURE gm_update_lot_status (
 p_order_id   IN   t501_order.c501_order_id%TYPE,
 p_user_id    IN   t501_order.c501_created_by%TYPE
 )
 AS
 	v_lot_status t2550_part_control_number.c2550_lot_status%TYPE;
	v_lot_num    t2550_part_control_number.c2550_control_number%TYPE;
	v_pnum		 T205_PART_NUMBER.c205_part_number_id%TYPE;
	v_error_msg  VARCHAR2(1000);
	v_flag		 VARCHAR2(1) := 'N';
  	v_company_id t1900_company.c1900_company_id%TYPE;
	
	  CURSOR cur_lot
	  IS
		SELECT t502b.C502b_usage_lot_num lotnum,
			t502b.c205_part_number_id pnum,
	        t2550.c2550_lot_status lotstatus
	      FROM t502b_item_order_usage T502B ,
	        t2550_part_control_number t2550 ,
	         t501_order t501 ,
	         V205_PARTS_WITH_EXT_VENDOR V205
	      WHERE T502B.C502B_USAGE_LOT_NUM = T2550.C2550_CONTROL_NUMBER (+)
	      AND (t501.C501_ORDER_ID = p_order_id OR t501.C501_PARENT_ORDER_ID  = p_order_id)
	      AND T502B.C501_ORDER_ID = t501.C501_ORDER_ID
	      AND V205.c205_part_number_id = t502b.c205_part_number_id
	      AND t501.C1900_COMPANY_ID    = V205.C1900_COMPANY_ID
	      AND C502B_USAGE_LOT_NUM  IS NOT NULL
	      AND NVL(t2550.c2550_LOT_STATUS,'-999') NOT IN (105000,105006) -- IMPLANT OR SOLD
          AND NVL(t2550.C901_LOT_CONTROLLED_STATUS,'-999') NOT IN (105008)--NOT CONTROLLED
	      AND t501.c501_void_fl IS NULL
	      AND t502b.c502b_void_fl IS NULL;
 	BEGIN

	 	FOR lot_to_update IN cur_lot
	  LOOP
	   		v_lot_status := lot_to_update.lotstatus;
            v_lot_num    := lot_to_update.lotnum;
            v_pnum       := lot_to_update.pnum;
            
       SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL'))
			INTO   v_company_id 
			FROM DUAL;
	   
	   SELECT get_rule_value(v_company_id,'LOT_TRACK') INTO v_flag FROM DUAL; 
	   
	   IF v_flag IS NULL OR v_flag <> 'Y' 
	   THEN
		   RETURN;
	   END IF;
	   
	    IF (v_lot_status <> '105006' OR v_lot_status IS NULL) THEN 
	    
		      		      
		      UPDATE T5060_CONTROL_NUMBER_INV
		      SET C5060_QTY                = '0',
		      	c5060_last_update_trans_id = p_order_id,
		        c901_last_transaction_type = '4310',  --SALES
	      		c5060_last_updated_by      = p_user_id, 
	      		c5060_last_updated_date    = SYSDATE 
		      WHERE C5060_CONTROL_NUMBER   = v_lot_num
		       AND C5060_QTY               = '1'
		       AND c901_warehouse_type     = 4000339 --FIELDSALES
		       AND c205_part_number_id     = v_pnum; 
	      
	    END IF;
	  END LOOP;
  END gm_update_lot_status;

END gm_pkg_op_lot_track;
/