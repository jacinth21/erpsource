/* Formatted on 2010/10/22 15:36 (Formatter Plus v4.8.0) */
--@"C:\database\Packages\Operations\gm_pkg_op_loaner_ext_rpt.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_loaner_ext_rpt
IS
--
 /**********************************************************************
   * Description : Procedure to Approve/Reject loaner extension comments
   *********************************************************************/
   PROCEDURE gm_fch_app_rej_log (
      p_redtlid   	IN       varchar2,
      p_fl          IN       varchar2,
      p_datefmt   	IN       varchar2,
      p_outdata     OUT      TYPES.cursor_type
   )
   AS
    v_time_format VARCHAR2(30);
    v_date_time_fmt VARCHAR2(50);
   BEGIN
	  SELECT get_rule_value('TIME_FMT', 'TIMEFORMAT') INTO v_time_format FROM DUAL;
	    v_date_time_fmt := p_datefmt || v_time_format;
	    
	   IF p_fl = 'N' THEN
	   OPEN p_outdata
	   FOR
	     
	    SELECT c504a_appr_rej_comments comments,  
	           TO_CHAR(c504a_ext_appr_rej_dt,v_date_time_fmt)  dt, 
	           t101.c101_user_f_name ||' '||t101.c101_user_l_name name
          FROM t504a_loaner_transaction t504a,t101_user t101
         WHERE t504a.c526_product_request_detail_id = p_redtlid
           AND t101.c101_user_id = t504a.c504a_status_changed_by
           AND c504a_void_fl IS NULL
      ORDER BY TO_CHAR(c504a_ext_appr_rej_dt,v_date_time_fmt) desc;
           
       ELSE
       OPEN p_outdata
	   FOR
	   
	    SELECT t.name,t.dt,t.comments 
	      FROM (select c101_user_f_name ||' '||c101_user_l_name name,
                       to_char(c902_created_date,v_date_time_fmt) dt,
                       c902_comments comments,
                       row_number() over (order by C902_LOG_ID desc) rn
          FROM t902_log, t101_user
         WHERE c101_user_id = c902_created_by
           AND c902_ref_id = p_redtlid
           AND c902_type = 26240803   --GPLOG for loaner extension 
           AND c902_void_fl is null order by C902_LOG_ID desc)t where rn >1;
	               
      END IF;
           
 END gm_fch_app_rej_log;
 
 /***********************************************************************
  * *Description : Procedure to get history  details /PMT-50520
  * Author: Angeline
  * ***********************************************************************/
 PROCEDURE gm_fch_loaner_log (
  p_redtlid   	IN        varchar2,
  p_outdata     OUT       TYPES.cursor_type
 )
 AS
  v_comp_dt_fmt VARCHAR2(50);
  v_datefmt VARCHAR2(50);
   v_time_format VARCHAR2(30);
  v_date_time_fmt VARCHAR2(50);
 BEGIN
	 SELECT get_compdtfmt_frm_cntx() INTO v_comp_dt_fmt FROM dual;
	 
	  SELECT get_rule_value('TIME_FMT', 'TIMEFORMAT') INTO v_time_format FROM DUAL;
	    v_date_time_fmt := v_comp_dt_fmt || v_time_format;
	 OPEN p_outdata
	   FOR
	SELECT
		      t101.c101_user_f_name || ' '|| c101_user_l_name requestornm,
		     TO_CHAR(t504f.c504f_loaner_dt, v_comp_dt_fmt)Loanerdt,
		     TO_CHAR(t504f.c504f_ext_required_date, v_comp_dt_fmt)Extnreqretdt,
		     --TO_CHAR(t504f.c504f_ext_requested_date, v_comp_dt_fmt || ' ' ||'HH:MI:SS') Extnreqdt, c504a_ext_appr_rej_dt
		     TO_CHAR(t504f.c504f_ext_requested_date, v_comp_dt_fmt|| ' ' ||'HH:MI:SS') Extnreqdt,
		     TO_CHAR(t504f.c504f_surgery_date, v_comp_dt_fmt)Surgdt,
		     TO_CHAR(t504f.c504f_ext_required_date, v_comp_dt_fmt)Apprretdt,
		     TO_CHAR(t504f.c504f_expected_return_dt, v_comp_dt_fmt)Expretdt,
		     t504f.c504f_extension_comments comments

    FROM T101_User t101,t504F_loaner_transaction_log t504f,T504a_Loaner_Transaction t504a, t704_account t704
   
	WHERE
		     t504f.c704_account_id = t704.c704_account_id(+)
		    AND t504a.c526_product_request_detail_id = p_redtlid
		    AND t504a.c504a_loaner_transaction_id = t504f.c504a_loaner_transaction_id
		     AND t101.c101_user_id = t504a.c504a_ext_requested_by
		    AND t704.c704_void_fl IS NULL
         ORDER BY
             t504f.c504f_ext_requested_date DESC;
		
   END gm_fch_loaner_log;
 /**********************************************************************
   * Description : Procedure to get zone details based on company id
   *********************************************************************/
   PROCEDURE gm_fch_zone_cmp (
      p_compid   	 IN       t504a_loaner_transaction.c1900_company_id%TYPE,
      p_zonedata     OUT      TYPES.cursor_type
   )
   AS
   BEGIN
	   OPEN p_zonedata
	   FOR

	   SELECT  DISTINCT  GP_ID CODEID  , 
	                     GP_NAME || ' (' || VP_NAME  || ')' CODENM  , 
	                     DIVID COMPDIVID, 
	                     COMPID  
	     FROM  V700_TERRITORY_MAPPING_DETAIL V700  
        WHERE  compid=100800  --Globus Medical Inc
          AND d_compid = p_compid
          AND divid = decode(p_compid,'1000',100823,100824) ORDER BY CODENM ;  --100823:US   100824:OUS


 END gm_fch_zone_cmp;

  /**********************************************************************
   * Description : Procedure to get region details based on company id
   *********************************************************************/
   PROCEDURE gm_fch_region_cmp (
      p_compid   	 IN       t504a_loaner_transaction.c1900_company_id%TYPE,
      p_zonedata     OUT      TYPES.cursor_type
   )
   AS
   BEGIN
	   OPEN p_zonedata
	   FOR

	    SELECT  DISTINCT  REGION_ID CODEID  , 
	                      REGION_NAME || ' (' || AD_NAME  || ')' CODENM, 
	                      GP_ID ZONEID, 
	                      DIVID COMPDIVID, 
	                      COMPID COMPID 
	      FROM  V700_TERRITORY_MAPPING_DETAIL V700  
         WHERE  compid=100800   --Globus Medical Inc
           AND  divid = decode(p_compid,'1000',100823,100824) ORDER BY CODENM ;

 END gm_fch_region_cmp;

/**************************************************************************
    * Description : This function returns Previous Loaner Extension Date
**************************************************************************/
FUNCTION get_prev_extn_count_for_sales_rep(
    p_req_det_id   IN t504a_loaner_transaction.c526_product_request_detail_id%TYPE,
    p_sales_rep_id IN t504a_loaner_transaction.c703_sales_rep_id%TYPE
    )
  RETURN VARCHAR2
IS
  v_prev_extn_count VARCHAR2(100);
BEGIN
	  BEGIN
  	    SELECT COUNT (1) INTO v_prev_extn_count
		  FROM t504a_loaner_transaction
		 WHERE c703_sales_rep_id    = p_sales_rep_id
		   AND c526_product_request_detail_id     = p_req_det_id
		   AND c504a_is_loaner_extended = 'Y'
		   AND c504a_void_fl is null; 
    EXCEPTION
     WHEN NO_DATA_FOUND THEN
	 RETURN 0;
  END;
  RETURN v_prev_extn_count;

END get_prev_extn_count_for_sales_rep; 
/**********************************************************************
   * Description : Procedure to get Filed sales details based on company id
   *********************************************************************/
   PROCEDURE gm_fch_field_sales_cmp (
      p_compid   	         IN       t504a_loaner_transaction.c1900_company_id%TYPE,
      p_lang_id              IN       VARCHAR2,
      p_files_sales_data     OUT      TYPES.cursor_type
   )
   AS
   BEGIN
	   OPEN p_files_sales_data
	   FOR
	   
	   SELECT DISTINCT D_ID ID,
                       DECODE(p_lang_id, '103097',NVL(D_NAME_EN, D_NAME), D_NAME) NM,
					   REGION_ID PID,
					   AD_ID ADID,
					   VP_ID VPID,
					   COMPID COMPID
				  FROM V700_TERRITORY_MAPPING_DETAIL V700
				 WHERE D_ID         IS NOT NULL
				   AND compid =100800
				   AND divid = decode(p_compid,'1000',100823,100824)
				   AND DISTTYPEID NOT IN (70103,70106,70104)
					ORDER BY UPPER(NM); 
 END gm_fch_field_sales_cmp;
 
 /***************************************************************************************
  * Description : This is Procedure is to used fetch previous extn open request details
****************************************************************************************/  
procedure gm_fetch_previous_his( 
    p_consign_id IN    t504a_loaner_transaction.c504_consignment_id%type,
    p_out        OUT    varchar2
 )
 AS 
 	v_out   VARCHAR2(20);
 BEGIN
	 BEGIN
	  	SELECT c901_status into v_out
	  	  FROM t504a_loaner_transaction 
	  	 WHERE c504_consignment_id=p_consign_id 
	  	   AND c504a_return_dt is null 
	  	   AND c504a_void_fl is null 
	  	   AND c901_status=1901;
	  	   
	 EXCEPTION WHEN NO_DATA_FOUND THEN
	  	v_out := '0';
	 END;
	  
	 p_out := v_out;		
END gm_fetch_previous_his;

/******************************************************************************************
  * Description : This function is used to show history icon on approval and report screen
********************************************************************************************/	 
FUNCTION get_history_fl(
	p_consign_id IN    t504a_loaner_transaction.c504_consignment_id%type
)RETURN VARCHAR2
IS
	v_count  number;
BEGIN
	BEGIN
		SELECT count(1) 
		  INTO v_count
		  FROM t902_log
		 WHERE c902_ref_id = p_consign_id
		   AND c902_type='26240803'   --GPLOG for loaner extension 
		   AND c902_comments like '%~History' 
		   AND c902_void_fl is null;
	    EXCEPTION
     		WHEN NO_DATA_FOUND THEN
	 	RETURN 0;
  	END;
  RETURN v_count;
END get_history_fl;

END gm_pkg_op_loaner_ext_rpt;
/