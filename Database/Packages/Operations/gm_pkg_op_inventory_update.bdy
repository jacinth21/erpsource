/* Formatted on 2009/09/30 15:50 (Formatter Plus v4.8.0) */

--@"c:\database\packages\operations\gm_pkg_op_inventory_update.bdy"

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_inventory_update
IS
	/*******************************************************
	  * Description : Main Procedure for void open Request
		author: Xun
	  *******************************************************/
	PROCEDURE gm_op_sav_tpr_clear_request (
		p_request_id	 IN   t520_request.c520_request_id%TYPE
	  , p_action_type	 IN   VARCHAR2
	  , p_user_id		 IN   t520_request.c520_created_by%TYPE
	  , p_old_req_date	 IN   t520_request.c520_required_date%TYPE
	)
	AS
		v_request_id   t520_request.c520_request_id%TYPE;
		v_lock_status_fl VARCHAR2 (100);
		v_demand_period_dt DATE;
		v_newreq_dt    NUMBER;
		v_invlockdt    NUMBER;
		v_sheet_id	   t4040_demand_sheet.c4040_demand_sheet_id%TYPE;
		v_set_id	   t520_request.c207_set_id%TYPE;
		v_inv_lock_id  t4040_demand_sheet.c250_inventory_lock_id%TYPE;
		v_log_action   t254_inventory_txn_log.c901_action%TYPE;
		v_new_req_date t520_request.c520_required_date%TYPE;
    v_source  t520_request.c901_request_source%TYPE;
    v_master_sheet_id t520_request.c520_request_txn_id%TYPE;
	BEGIN
    select t520.c520_request_txn_id, t520.c901_request_source
    into v_master_sheet_id, v_source
    from t520_request t520
    where t520.c520_request_id = (
                                  select t520a.c520_master_request_id
                                  from t520_request t520a
                                  where t520a.c520_request_id = p_request_id)
    OR (t520.c520_request_id =  p_request_id and t520.c520_master_request_id is null);
    if v_source = 50616 then -- order planning request only            
      select TO_NUMBER (TO_CHAR (t520.c520_required_date, 'YYYYMM')) newreqdt,
              t520.c207_set_id, t520.c520_required_date
      into v_newreq_dt, v_set_id, v_new_req_date 
      from t520_request t520
      where t520.c520_request_id = p_request_id;
    
      SELECT t4040.c4040_demand_sheet_id
      into v_sheet_id
      FROM t4040_demand_sheet t4040
      WHERE t4040.c4020_demand_master_id = v_master_sheet_id
      AND t4040.c4040_demand_period_dt = TO_DATE ('01/'|| TO_CHAR (SYSDATE, 'MM/YYYY'), 'DD/MM/YYYY')
      AND t4040.c901_status = 50550; -- open sheet
      
      select t4040.c250_inventory_lock_id, t4040.c4040_demand_period_dt,
              TO_NUMBER (TO_CHAR (t250.c250_lock_date, 'YYYYMM')) invlockdt
      into v_inv_lock_id, v_demand_period_dt, v_invlockdt
      from t4040_demand_sheet t4040, t250_inventory_lock t250
      where t4040.c4040_demand_sheet_id = v_sheet_id
      and t250.c250_inventory_lock_id = t4040.c250_inventory_lock_id;
    else
      return;
    end if;		
		IF (p_action_type = 'void')
		THEN
			gm_pkg_op_inventory_update.gm_op_sav_tpr_void_request (p_request_id, p_user_id, v_inv_lock_id);
			v_log_action := 90850;

			IF (v_newreq_dt < v_invlockdt)
			THEN
				gm_pkg_op_inventory_update.gm_op_sav_tpr_clear_pbo (p_request_id
																  , p_user_id
																  , v_inv_lock_id
																  , v_sheet_id
																  , v_set_id
																  , 1
																   );
				gm_pkg_op_inventory_update.gm_op_sav_tpr_clear_pbl (p_request_id
																  , p_user_id
																  , v_inv_lock_id
																  , v_sheet_id
																  , v_set_id
																  , 1
																   );
				gm_pkg_op_inventory_update.gm_op_sav_tpr_clear_pcs (p_request_id
																  , p_user_id
																  , v_inv_lock_id
																  , v_sheet_id
																  , v_set_id
																  , 1
																   );
			END IF;
		END IF;

		IF (p_action_type = 'dateupdate')
		THEN
			gm_pkg_op_inventory_update.gm_op_sav_tpr_upt_request (p_request_id
																, p_user_id
																, v_inv_lock_id
																, v_new_req_date
																 );
			v_log_action := 90851;

			IF (TO_NUMBER (TO_CHAR (p_old_req_date, 'YYYYMM')) < v_invlockdt AND v_newreq_dt >= v_invlockdt)
			THEN
				gm_pkg_op_inventory_update.gm_op_sav_tpr_clear_pbo (p_request_id
																  , p_user_id
																  , v_inv_lock_id
																  , v_sheet_id
																  , v_set_id
																  , 1
																   );
				gm_pkg_op_inventory_update.gm_op_sav_tpr_clear_pbl (p_request_id
																  , p_user_id
																  , v_inv_lock_id
																  , v_sheet_id
																  , v_set_id
																  , 1
																   );
				gm_pkg_op_inventory_update.gm_op_sav_tpr_clear_pcs (p_request_id
																  , p_user_id
																  , v_inv_lock_id
																  , v_sheet_id
																  , v_set_id
																  , 1
																   );
			END IF;

			IF (v_newreq_dt < v_invlockdt)
			THEN
				gm_pkg_op_inventory_update.gm_op_sav_tpr_clear_pbo (p_request_id
																  , p_user_id
																  , v_inv_lock_id
																  , v_sheet_id
																  , v_set_id
																  , -1
																   );
				gm_pkg_op_inventory_update.gm_op_sav_tpr_clear_pbl (p_request_id
																  , p_user_id
																  , v_inv_lock_id
																  , v_sheet_id
																  , v_set_id
																  , -1
																   );
				gm_pkg_op_inventory_update.gm_op_sav_tpr_clear_pcs (p_request_id
																  , p_user_id
																  , v_inv_lock_id
																  , v_sheet_id
																  , v_set_id
																  , -1
																   );
			END IF;
		END IF;

		gm_pkg_op_inventory_update.gm_op_inv_log_update (p_request_id, v_inv_lock_id, 90870, v_log_action, p_user_id);
	  EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN;
	END gm_op_sav_tpr_clear_request;

	/*******************************************************
	  * Description : Procedure to update Request
		author: Xun
	  *******************************************************/
	PROCEDURE gm_op_sav_tpr_upt_request (
		p_req_id		IN	 t907_cancel_log.c907_ref_id%TYPE
	  , p_userid		IN	 t102_user_login.c102_user_login_id%TYPE
	  , p_inv_lock_id	IN	 t4040_demand_sheet.c250_inventory_lock_id%TYPE
	  , p_req_date		IN	 t520_request.c520_required_date%TYPE
	)
	AS
	BEGIN
		UPDATE t253c_request_lock
		   SET c520_required_date = p_req_date
			 , c520_last_updated_by = p_userid
			 , c520_last_updated_date = SYSDATE
		 WHERE c520_request_id = p_req_id AND c250_inventory_lock_id = p_inv_lock_id;

--Below SQL to update child record
		UPDATE t253c_request_lock
		   SET c520_required_date = p_req_date
			 , c520_last_updated_by = p_userid
			 , c520_last_updated_date = SYSDATE
		 WHERE c520_master_request_id = p_req_id AND c250_inventory_lock_id = p_inv_lock_id;
	--
	END gm_op_sav_tpr_upt_request;

	/*******************************************************
	* Description : Procedure to void Request
	  author: Xun
	*******************************************************/
	PROCEDURE gm_op_sav_tpr_void_request (
		p_req_id		IN	 t907_cancel_log.c907_ref_id%TYPE
	  , p_userid		IN	 t102_user_login.c102_user_login_id%TYPE
	  , p_inv_lock_id	IN	 t4040_demand_sheet.c250_inventory_lock_id%TYPE
	)
	AS
	BEGIN
		UPDATE t253c_request_lock
		   SET c520_void_fl = 'Y'
			 , c520_last_updated_by = p_userid
			 , c520_last_updated_date = SYSDATE
		 WHERE c520_request_id = p_req_id AND c250_inventory_lock_id = p_inv_lock_id;

		UPDATE t253a_consignment_lock
		   SET c504_void_fl = 'Y'
			 , c504_last_updated_by = p_userid
			 , c504_last_updated_date = SYSDATE
		 WHERE c520_request_id = p_req_id AND c250_inventory_lock_id = p_inv_lock_id;

--Below SQL to update child record
		UPDATE t253c_request_lock
		   SET c520_void_fl = 'Y'
			 , c520_last_updated_by = p_userid
			 , c520_last_updated_date = SYSDATE
		 WHERE c520_master_request_id = p_req_id AND c250_inventory_lock_id = p_inv_lock_id;
	--
	END gm_op_sav_tpr_void_request;

	/*******************************************************
	* Description : Procedure for pbo
	  author: Xun
	*******************************************************/
	PROCEDURE gm_op_sav_tpr_clear_pbo (
		p_req_id		IN	 t907_cancel_log.c907_ref_id%TYPE
	  , p_userid		IN	 t102_user_login.c102_user_login_id%TYPE
	  , p_inv_lock_id	IN	 t4040_demand_sheet.c250_inventory_lock_id%TYPE
	  , p_sheet_id		IN	 t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	  , p_set_id		IN	 t520_request.c207_set_id%TYPE
	  , p_value 		IN	 NUMBER
	)
	AS
		CURSOR pbo_cur
		IS
			SELECT c205_part_number_id pum_id, (c521_qty * p_value) qty
			  FROM t253d_request_detail_lock t253a
			 WHERE (   c520_request_id = p_req_id
					OR c520_request_id IN (
									  SELECT c520_request_id
										FROM t253c_request_lock
									   WHERE c520_master_request_id = p_req_id
											 AND c250_inventory_lock_id = p_inv_lock_id)
				   )
			   AND c250_inventory_lock_id = p_inv_lock_id;
	BEGIN
		FOR set_val IN pbo_cur
		LOOP
			gm_pkg_op_inventory_update.gm_op_sav_inv_detail_update (p_inv_lock_id, 50567, set_val.pum_id, set_val.qty);
			gm_pkg_op_inventory_update.gm_op_sav_demand_sheet_update (p_sheet_id
																	, p_set_id
																	, p_inv_lock_id
																	, 50567
																	, set_val.pum_id
																	, set_val.qty
																	 );
		END LOOP;
	--
	END gm_op_sav_tpr_clear_pbo;

	/*******************************************************
	* Description : Procedure for pbl
	  author: Xun
	*******************************************************/
	PROCEDURE gm_op_sav_tpr_clear_pbl (
		p_req_id		IN	 t907_cancel_log.c907_ref_id%TYPE
	  , p_userid		IN	 t102_user_login.c102_user_login_id%TYPE
	  , p_inv_lock_id	IN	 t4040_demand_sheet.c250_inventory_lock_id%TYPE
	  , p_sheet_id		IN	 t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	  , p_set_id		IN	 t520_request.c207_set_id%TYPE
	  , p_value 		IN	 NUMBER
	)
	AS
		CURSOR pbo_cur
		IS
			SELECT t253b.c205_part_number_id pum_id, (t253b.c505_item_qty * p_value) qty
			  FROM t253c_request_lock t253c, t253a_consignment_lock t253a, t253b_item_consignment_lock t253b
			 WHERE t253c.c520_request_id = p_req_id
			   AND t253c.c520_request_id = t253a.c520_request_id
			   AND t253a.c504_consignment_id = t253b.c504_consignment_id
			   AND t253c.c520_status_fl = 15
			   AND t253c.c250_inventory_lock_id = p_inv_lock_id
			   AND t253a.c250_inventory_lock_id = p_inv_lock_id
			   AND t253b.c250_inventory_lock_id = p_inv_lock_id;
	BEGIN
		FOR set_val IN pbo_cur
		LOOP
			gm_pkg_op_inventory_update.gm_op_sav_inv_detail_update (p_inv_lock_id, 50568, set_val.pum_id, set_val.qty);
			gm_pkg_op_inventory_update.gm_op_sav_demand_sheet_update (p_sheet_id
																	, p_set_id
																	, p_inv_lock_id
																	, 50568
																	, set_val.pum_id
																	, set_val.qty
																	 );
		END LOOP;
--
	END gm_op_sav_tpr_clear_pbl;

	/*******************************************************
	* Description : Procedure for pcs
		author: Xun
	*******************************************************/
	PROCEDURE gm_op_sav_tpr_clear_pcs (
		p_req_id		IN	 t907_cancel_log.c907_ref_id%TYPE
	  , p_userid		IN	 t102_user_login.c102_user_login_id%TYPE
	  , p_inv_lock_id	IN	 t4040_demand_sheet.c250_inventory_lock_id%TYPE
	  , p_sheet_id		IN	 t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	  , p_set_id		IN	 t520_request.c207_set_id%TYPE
	  , p_value 		IN	 NUMBER
	)
	AS
		CURSOR pbo_cur
		IS
			SELECT t253b.c205_part_number_id pum_id, (t253b.c505_item_qty * p_value) qty
			  FROM t253c_request_lock t253c, t253a_consignment_lock t253a, t253b_item_consignment_lock t253b
			 WHERE t253c.c520_request_id = p_req_id
			   AND t253c.c520_request_id = t253a.c520_request_id
			   AND t253a.c504_consignment_id = t253b.c504_consignment_id
			   AND t253c.c520_status_fl IN (20, 30)
			   AND t253c.c250_inventory_lock_id = p_inv_lock_id
			   AND t253a.c250_inventory_lock_id = p_inv_lock_id
			   AND t253b.c250_inventory_lock_id = p_inv_lock_id;
	BEGIN
		FOR set_val IN pbo_cur
		LOOP
			gm_pkg_op_inventory_update.gm_op_sav_inv_detail_update (p_inv_lock_id, 50569, set_val.pum_id, set_val.qty);
			gm_pkg_op_inventory_update.gm_op_sav_demand_sheet_update (p_sheet_id
																	, p_set_id
																	, p_inv_lock_id
																	, 50569
																	, set_val.pum_id
																	, set_val.qty
																	 );
		END LOOP;
--
	END gm_op_sav_tpr_clear_pcs;

	/*******************************************************
	* Description : Procedure for inventory detail update
	  author: Xun
	*******************************************************/
	PROCEDURE gm_op_sav_inv_detail_update (
		p_inv_lock_id	IN	 t4040_demand_sheet.c250_inventory_lock_id%TYPE
	  , p_type			IN	 t251_inventory_lock_detail.c901_type%TYPE
	  , p_part_num		IN	 t253d_request_detail_lock.c205_part_number_id%TYPE
	  , p_qty			IN	 t253d_request_detail_lock.c521_qty%TYPE
	)
	AS
		CURSOR sub_parts_cur
		IS
				SELECT t205a.c205_to_part_number_id sub_part, (t205a.c205a_qty * NVL ( PRIOR t205a.c205a_qty, 1)) qty
				FROM t205a_part_mapping t205a, t205d_sub_part_to_order t205d
				WHERE t205a.c205_to_part_number_id = t205d.c205_part_number_id																																								
				START WITH t205a.c205_from_part_number_id = p_part_num
				CONNECT BY PRIOR t205a.c205_to_part_number_id = t205a.c205_from_part_number_id
				AND t205a.c205a_void_fl is null;			---  added new column c205a_void_fl in t205a used to PMT-50777 - Create BOM and sub-component Mapping			
					
	BEGIN
		UPDATE t251_inventory_lock_detail t251
		   SET c251_qty = c251_qty - p_qty
		 WHERE t251.c205_part_number_id = p_part_num AND c250_inventory_lock_id = p_inv_lock_id AND c901_type = p_type;

		FOR sub_parts_details_cur IN sub_parts_cur
		LOOP
			UPDATE t251_inventory_lock_detail t251
			   SET c251_qty = c251_qty - (p_qty * sub_parts_details_cur.qty)
			 WHERE t251.c205_part_number_id = sub_parts_details_cur.sub_part
			   AND c250_inventory_lock_id = p_inv_lock_id
			   AND c901_type = p_type;
		END LOOP;
	END gm_op_sav_inv_detail_update;

	/*******************************************************
	* Description : Procedure for demand sheet update
	  author: Xun
	*******************************************************/
	PROCEDURE gm_op_sav_demand_sheet_update (
		p_sheet_id			 t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	  , p_set_id		IN	 t520_request.c207_set_id%TYPE
	  , p_inv_lock_id	IN	 t4040_demand_sheet.c250_inventory_lock_id%TYPE
	  , p_type			IN	 t251_inventory_lock_detail.c901_type%TYPE
	  , p_part_num		IN	 t253d_request_detail_lock.c205_part_number_id%TYPE
	  , p_qty			IN	 t253d_request_detail_lock.c521_qty%TYPE
	)
	AS
	BEGIN
		UPDATE t4042_demand_sheet_detail t4042
		   SET t4042.c4042_qty = t4042.c4042_qty - p_qty
		 WHERE t4042.c4040_demand_sheet_id = p_sheet_id
		   AND t4042.c205_part_number_id = p_part_num
		   AND t4042.c901_type = p_type
		   AND t4042.c4042_ref_id = DECODE (p_set_id, NULL, '-9999', p_set_id);
	END gm_op_sav_demand_sheet_update;

	/*******************************************************
	* Description : Procedure for Inventory TXN LOG update
	  author: Xun
	*******************************************************/
	PROCEDURE gm_op_inv_log_update (
		p_req_id		IN	 t254_inventory_txn_log.c254_txn_id%TYPE
	  , p_inv_lock_id	IN	 t4040_demand_sheet.c250_inventory_lock_id%TYPE
	  , p_type			IN	 t251_inventory_lock_detail.c901_type%TYPE
	  , p_action		IN	 t254_inventory_txn_log.c901_action%TYPE
	  , p_userid		IN	 t102_user_login.c102_user_login_id%TYPE
	)
	AS
	BEGIN
		INSERT INTO t254_inventory_txn_log
					(c254_inventory_txn_log_id, c250_inventory_lock_id, c254_txn_id, c901_txn_type, c901_action
				   , c254_last_updated_by, c254_last_updated_date
					)
			 VALUES (s254_inventory_txn_log.NEXTVAL, p_inv_lock_id, p_req_id, p_type, p_action
				   , p_userid, SYSDATE
					);
	END gm_op_inv_log_update;
--
END gm_pkg_op_inventory_update;
/
