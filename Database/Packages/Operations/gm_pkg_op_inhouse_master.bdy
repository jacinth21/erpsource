/* Formatted on 2011/01/25 10:20 (Formatter Plus v4.8.0) */


CREATE OR REPLACE PACKAGE BODY gm_pkg_op_inhouse_master
IS
--
--
   /*******************************************************
     * Description : Procedure to save Inhouse Transaction details
     * Author      : VPrasath
     *******************************************************/
--
   PROCEDURE gm_sav_inhouse_trans_items (
      p_inhtxn_id     IN   t413_inhouse_trans_items.c412_inhouse_trans_id%TYPE,
      p_part_num      IN   t413_inhouse_trans_items.c205_part_number_id%TYPE,
      p_qty           IN   t413_inhouse_trans_items.c413_item_qty%TYPE,
      p_control_num   IN   t413_inhouse_trans_items.c413_control_number%TYPE,
      p_price         IN   t413_inhouse_trans_items.c413_item_price%TYPE,
      p_client_pnum   IN   t413_inhouse_trans_items.c205_client_part_number_id%TYPE DEFAULT NULL,
      p_ref_id		  IN   t413_inhouse_trans_items.c413_ref_id%TYPE DEFAULT NULL,
      p_ref_type      IN   t413_inhouse_trans_items.c901_ref_type%TYPE DEFAULT NULL
   )
--
   AS
      v_control_num   t413_inhouse_trans_items.c413_control_number%TYPE
                                                             := p_control_num;
   BEGIN
      IF (v_control_num = 'TBE' OR v_control_num = 'tbe')
      THEN
         v_control_num := '';
      END IF;

      INSERT INTO t413_inhouse_trans_items
                  (c413_trans_id, c412_inhouse_trans_id, c205_part_number_id,
                   c413_item_qty, c413_control_number, c413_item_price, c413_created_date , c205_client_part_number_id,
                   c413_ref_id, c901_ref_type
                  )
           VALUES (s413_inhouse_item.NEXTVAL, p_inhtxn_id, p_part_num,
                   p_qty, v_control_num, p_price, SYSDATE ,p_client_pnum,
                   p_ref_id, p_ref_type
                  );
   END gm_sav_inhouse_trans_items;

	/*******************************************************
	Created by : Brinal Gupta
	* Purpose: function is used to return the status of a inhouse loaner trans
	*******************************************************/
	--
   FUNCTION get_inhouse_trans_status (
      p_statusflag   IN   t412_inhouse_transactions.c412_status_fl%TYPE
   )
      RETURN VARCHAR2
   IS
      v_status_name   VARCHAR2 (100);
   BEGIN
      SELECT DECODE (p_statusflag,
                     0, 'Back Order',
                     2, 'Pend control',
                     3, 'Pend Verification',
                     4, 'Completed'
                    )
        INTO v_status_name
        FROM DUAL;

      RETURN v_status_name;
   END get_inhouse_trans_status;

   /******************************************************************
    * Created by : Brinal Gupta
    * Description : Procedure to fetch all the open inhouse trans
    ****************************************************************/
   PROCEDURE gm_fch_open_inhouse_trans (
      p_ref_id        IN       t412_inhouse_transactions.c412_ref_id%TYPE,
      p_scrn_from     IN 	   VARCHAR2,
      p_trans         OUT      TYPES.cursor_type
   )
   AS
   BEGIN
      OPEN p_trans
       FOR
       	SELECT backorderinfo.*, DECODE(backorderinfo.statusflag,'0',DECODE(ROWNUM,1,'Y',NULL),NULL) BORPTFL FROM (
           SELECT t412.c412_inhouse_trans_id ID, c412_ref_id master_id,
                 t413.c205_part_number_id pnum,
                 t205.c205_part_num_desc pdesc,
                 t413.c413_item_qty qty, to_char(t412.c412_created_date,GET_RULE_VALUE('DATEFMT','DATEFORMAT'))  crdate,
                 gm_pkg_op_inhouse_master.get_inhouse_trans_status(t412.c412_status_fl) status,
                 t412.c412_status_fl statusflag,
                 --DECODE(t412.c412_status_fl,'0',DECODE(ROWNUM,1,'Y',NULL),NULL) BORPTFL,                                                  
                 get_code_name (t412.c412_type) TYPE,
                 t412.c412_type CTYPE
            FROM t412_inhouse_transactions t412,
                 t413_inhouse_trans_items t413, t205_part_number t205
           WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
             AND t205.c205_part_number_id = t413.c205_part_number_id
             --If the user loading the CN from Process Trans screen, should show only open back order transactions
        	 AND c412_status_fl < DECODE(p_scrn_from,'PROCESS_TXN_SCN',1,4)
             AND t412.c412_type NOT IN (50159, 1006571)--loaner sales trans
             AND t413.c413_void_fl IS NULL
             AND c412_void_fl IS NULL
             AND t412.c412_ref_id = p_ref_id
            UNION 
			   SELECT t412.c412_inhouse_trans_id ID,
			  c412_ref_id master_id,
			  t413.c205_part_number_id pnum,
			  t205.c205_part_num_desc pdesc,
			  t413.c413_item_qty qty,
			  TO_CHAR(t412.c412_created_date,GET_RULE_VALUE('DATEFMT','DATEFORMAT')) crdate,
			  gm_pkg_op_inhouse_master.get_inhouse_trans_status(t412.c412_status_fl) status,
			  t412.c412_status_fl statusflag,
			  --DECODE(t412.c412_status_fl,'0',DECODE(ROWNUM,1,'Y',NULL),NULL) BORPTFL,
			  get_code_name (t412.c412_type) TYPE,
			  t412.c412_type CTYPE
			FROM t412_inhouse_transactions t412,
			  t413_inhouse_trans_items t413,
			  t205_part_number t205
			WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
			AND t205.c205_part_number_id     = t413.c205_part_number_id
			AND c412_status_fl      <> 4
			AND t412.c412_type IN (1006571, 1006572)--InHouse Set Missing Part && IH Loaner Set Back Order
			AND t413.c413_void_fl  IS NULL
			AND c412_void_fl       IS NULL
			AND t412.c412_ref_id    = p_ref_id)backorderinfo; 
             
   END gm_fch_open_inhouse_trans;
   
   /*************************************************************************
	 * Description : Procedure to Save the lot number for validation purpose
	 * author	   : Arajan
	 ************************************************************************/
	PROCEDURE gm_sav_inhouse_lot_num(
		p_ref_id    		IN	t412_inhouse_transactions.c412_inhouse_trans_id%TYPE,
	    p_reftype  			IN 	t5055_control_number.c901_ref_type%TYPE,
	    p_inputstr 			IN 	CLOB,
	    p_userid   			IN 	t5055_control_number.c5055_last_updated_by%TYPE,
	    p_err_fl			OUT	VARCHAR2
	)
	AS
	  	v_string        CLOB := p_inputstr;
	  	v_substring     VARCHAR2 (1000);
	  	v_pnum          t5055_control_number.c205_part_number_id%TYPE;
	  	v_qty           t5055_control_number.c5055_qty%TYPE;
	  	v_control       t5055_control_number.c5055_control_number%TYPE;
	  	v_price 	   	t413_inhouse_trans_items.c413_item_price%TYPE;
		v_client_pnum  	t413_inhouse_trans_items.c205_client_part_number_id%TYPE;
		v_refid 	   	t412_inhouse_transactions.c412_ref_id%TYPE;
		v_ref_type     	t413_inhouse_trans_items.c901_ref_type%TYPE;
	  	v_whtype        t5055_control_number.c901_warehouse_type%TYPE :='';
	  	v_comma_count	NUMBER;
	  	v_plant_id      t5040_plant_master.c5040_plant_id%TYPE;
	  	v_comp_id		t1900_company.c1900_company_id%TYPE;
	  	v_err_msg		VARCHAR2(2000) := '';
	  	v_err_fl		VARCHAR2(1) := 'N';
	  	v_flag			VARCHAR2(1) := 'N';
	  	v_comp_flag		VARCHAR2(1);
	  	-- Modified in addition to PMT-14634
	  	v_temp_pnum		CLOB := '';
	  	v_pnum_str		VARCHAR2(100);
	BEGIN
		
		SELECT  NVL(get_plantid_frm_cntx(),get_rule_value('DEFAULT_PLANT','ONEPORTAL'))
				, NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL'))
			INTO  v_plant_id, v_comp_id FROM DUAL;
		
		SELECT get_rule_value(v_comp_id,'LOT_TRACK') INTO v_comp_flag FROM DUAL; 
	   -- if the flag is getting as 'Y', we need to do the validaiton check for that company otherwise no
	   IF v_comp_flag IS NULL OR v_comp_flag <> 'Y' 
	   THEN
		   RETURN;
	   END IF;

	  	WHILE INSTR (v_string, '|') <> 0
		LOOP
			v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
			v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
			--count how many commas in each substring
		    SELECT trim( LENGTH( v_substring ) ) - trim( LENGTH( TRANSLATE( v_substring, 'A,', 'A' ) ) )
		    INTO v_comma_count
			FROM dual;
			v_pnum		:= NULL;
			v_qty		:= NULL;
			v_control	:= NULL;
			v_price 	:= NULL;
			v_client_pnum := NULL;		
			v_refid  := NULL;
			v_ref_type  := NULL;--9135.0020BM,1,,NaN|
			v_pnum		:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_qty		:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_control	:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			IF v_comma_count != 3
	    	THEN
			v_client_pnum 	:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			END IF;
			-- if ref id and ref type is available, get it from the string
			IF SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1) IS NOT NULL AND v_comma_count != 3
			THEN
				v_price 	:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
				v_refid 	:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
				v_ref_type	:= v_substring;
			ELSE
				v_price		:= v_substring;
			END IF;
			--
			
			v_pnum_str := '|'||v_pnum||'|';
			
		    /*
		     * Get the part attribute type to validate the lot number from the part number setup screen
		     * If the attribute value is 'Y' validate the lot number for the part, otherwise skip the validation
		     * 104480: Lot tracking required ?
		     */
		     v_flag := get_part_attr_value_by_comp(v_pnum,'104480',v_comp_id);

		     -- If the attribute is not set for the part, then should not validate the part
		     IF v_flag IS NULL OR v_flag <> 'Y' THEN
		     	CONTINUE;
		     END IF;
		     
		    IF NVL(INSTR(v_temp_pnum,v_pnum_str),0) = 0 THEN
			     -- Need to delete the record first before inserting
				DELETE t413_inhouse_trans_items
				  WHERE c412_inhouse_trans_id = p_ref_id
				  AND c205_part_number_id = v_pnum
				  AND c413_void_fl IS NULL;
				
				v_temp_pnum := v_temp_pnum||'|'||v_pnum||'|';
			END IF;
		    
		    -- Validate the lot numbers 
			gm_pkg_op_lot_track.gm_validate_input_data(p_reftype,p_ref_id,'93343',v_whtype,v_pnum,v_control,v_qty, v_err_msg);

			-- save the lot numbers in the table t413_inhouse_trans_items
			gm_pkg_op_inhouse_master.gm_sav_inhouse_trans_items(p_ref_id, v_pnum, v_qty, v_control, v_price ,v_client_pnum,v_refid,v_ref_type);
			
			-- Set the flag to 'Y', if any error message is there to return the value 
			IF v_err_msg IS NOT NULL OR v_err_msg != '' THEN
				v_err_fl := 'Y';
			END IF;
			
			IF (v_control = 'TBE' OR v_control = 'tbe')
	      	THEN
	         	v_control := NULL;
	      	END IF;
			-- update the error message in the table
			UPDATE t413_inhouse_trans_items
				SET c413_error_dtls = v_err_msg
					, c413_last_updated_by = p_userid
					, c413_last_updated_date = CURRENT_DATE
			WHERE c205_part_number_id = v_pnum
				AND NVL(c413_control_number,'-9999') = NVL(v_control,'-9999')
				AND c412_inhouse_trans_id = p_ref_id
				AND c413_void_fl IS NULL;
			
		END LOOP;
		
		p_err_fl := v_err_fl;
		
	END gm_sav_inhouse_lot_num;
	
	/*************************************************************************
	 * Description : Procedure to Save the lot number for validation purpose
	 * author	   : Hreddi
	 ************************************************************************/
	PROCEDURE gm_sav_othr_inhouse_lot_num(
		p_consignid    		IN	t504_consignment.c504_consignment_id%TYPE,
	    p_reftype  			IN 	t5055_control_number.c901_ref_type%TYPE,
	    p_inputstr 			IN 	CLOB,
	    p_shipfl			IN  VARCHAR2, 
	    p_userid   			IN 	t5055_control_number.c5055_last_updated_by%TYPE,
	    p_err_fl			OUT	VARCHAR2
	)
	AS
	   v_strlen         NUMBER                         := NVL (LENGTH (p_inputstr), 0);
	   v_string         VARCHAR2 (30000)                        := p_inputstr;
	   v_pnum           VARCHAR2 (20);
	   v_qty            NUMBER;
	   v_control        t5010_tag.c5010_control_number%TYPE;
	   v_price          NUMBER (15, 2);
	   v_substring      VARCHAR2 (1000);
	   v_msg            VARCHAR2 (1000);
	   v_id             NUMBER;
	   v_flag           VARCHAR2 (2);
	   v_flag1          VARCHAR2 (2);
	   v_shipfl         NUMBER;
	   v_void_fl        VARCHAR2 (10);
	   v_consign_type   NUMBER;
	   v_quar_qty       NUMBER;
	   v_rqid           t504_consignment.c520_request_id%TYPE;
	   v_cnt            NUMBER                                  := 0;
	   v_isset          VARCHAR2 (10);
	   v_stock          NUMBER;
	   v_qstock         NUMBER;
	   v_code_grp       VARCHAR2(10);
	   v_inv_type       NUMBER;
	   v_comp_id		t1900_company.c1900_company_id%TYPE;
	   v_comp_flag		VARCHAR2(1);
	   v_part_valid_fl	VARCHAR2(1) := 'N';
	   v_whtype         t5055_control_number.c901_warehouse_type%TYPE :='';
	   v_plant_id       t5040_plant_master.c5040_plant_id%TYPE;
	   v_err_msg		VARCHAR2(2000) := '';
	   v_err_fl			VARCHAR2(1) := 'N';
	   
	BEGIN
		
		SELECT NVL(get_plantid_frm_cntx(),get_rule_value('DEFAULT_PLANT','ONEPORTAL'))
			 , NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL'))
		  INTO v_plant_id, v_comp_id 
		 FROM DUAL;
		
		 -- To check whether the lot tracking validation is needed for the company or not
	    SELECT get_rule_value(v_comp_id,'LOT_TRACK') INTO v_comp_flag FROM DUAL; 
	    -- if the flag is getting as 'Y', we need to do the validaiton check for that company otherwise no
	    IF v_comp_flag IS NULL OR v_comp_flag <> 'Y' THEN
	   	  RETURN;
	    END IF;
	   
	   -- Validate if the Consignment is already voided
	   SELECT c504_void_fl, c504_type, c520_request_id
	     INTO v_void_fl, v_consign_type, v_rqid
	     FROM t504_consignment
	    WHERE c504_consignment_id = p_consignid;
		  	
	   IF v_void_fl IS NOT NULL
	   THEN
	      -- Error message is Consignment already Voided.
	      raise_application_error (-20770, '');
	   END IF;
	   
	   IF v_strlen > 0  THEN
	   
		-- Need to delete the record first before inserting
		 DELETE      t505_item_consignment
            WHERE c504_consignment_id = p_consignid;

	   WHILE INSTR (v_string, '|') <> 0
	      LOOP
	         v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
	         v_string := SUBSTR (v_string, INSTR (v_string, '|') + 1);
	         v_pnum := NULL;
	         v_qty := NULL;
	         v_control := NULL;
	         v_price := NULL;
	         v_pnum := SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
	         v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
	         v_qty := SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
	         v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
	         v_control := SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
	         v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
	         v_price := v_substring;		
			
	         IF    v_consign_type = 4114
	            OR v_consign_type = 4113
	            OR v_consign_type = 4116
	         THEN           -- Repackaging/Shelf to quarantine/Quarantine to Shelf
	            SELECT      NVL(GET_QTY_IN_INVENTORY(p_consignid,'90800',c205_part_number_id),0) stock,
	                       (  get_partnumber_qty (c205_part_number_id, 90813)
	                        - get_qty_allocated (c205_part_number_id, 50540)
	                       ) qstock
	                  INTO v_stock,
	                       v_qstock
	                  FROM t205_part_number
	                 WHERE c205_part_number_id = v_pnum
	            FOR UPDATE;
	            IF v_consign_type IN (4114, 4113) 
	            THEN
	              IF v_stock < v_qty
	              THEN
	                 GM_RAISE_APPLICATION_ERROR('-20999','207',v_pnum);
	              END IF;
	            END IF;
	            IF v_consign_type = 4116
	            THEN
	               IF v_qstock < v_qty
	               THEN
	                  GM_RAISE_APPLICATION_ERROR('-20999','207',v_pnum);
	               END IF;
	            END IF;
	         END IF;
	
	         --
	         IF v_consign_type = 4115                             -- 4115 is scrap
	         THEN
	            SELECT     (  get_partnumber_qty(c205_part_number_id, 90813)
	                        - get_qty_allocated (c205_part_number_id, 50540)
	                       )
	                  INTO v_quar_qty
	                  FROM t205_part_number
	                 WHERE c205_part_number_id = v_pnum
	            FOR UPDATE;
	
	            IF v_quar_qty < v_qty
	            THEN
	               GM_RAISE_APPLICATION_ERROR('-20999','207',v_pnum);
	            END IF;
	         END IF;
	
	         SELECT get_part_attribute_value (v_pnum, 92340)      -- Taggable Part
	           INTO v_isset
	           FROM DUAL;
	
	         IF     v_isset = 'Y'
	            AND p_shipfl = '3'
	            --if the consigned item then
	            AND (   v_consign_type = 4110                       -- Consignment
	                 OR v_consign_type = 4111                             --Loaner
	                --    OR v_consign_type = 4112  --In-House Consignment
	                )
	         THEN
	            SELECT COUNT (1)
	              INTO v_cnt
	              FROM t5010_tag t5010
	             WHERE t5010.c205_part_number_id = v_pnum
	               AND t5010.c5010_last_updated_trans_id = p_consignid
	               AND t5010.c5010_control_number = v_control
	               AND t5010.c5010_void_fl IS NULL;
	
	            IF v_cnt = 0
	            THEN
	               -- Error message is TagID cannot be blank, please enter TagID.
	               raise_application_error (-20912, '');
	            END IF;
         	END IF;
         	
         	SELECT s504_consign_item.NEXTVAL
           	  INTO v_id
           	  FROM DUAL;
           	  
	         INSERT INTO t505_item_consignment(c505_item_consignment_id, c504_consignment_id, c205_part_number_id, c505_item_qty,c505_control_number, c505_item_price)
	              VALUES (v_id, p_consignid,v_pnum, v_qty,v_control, v_price);                  
		    /*
		     * Get the part attribute type to validate the lot number from the part number setup screen
		     * If the attribute value is 'Y' validate the lot number for the part, otherwise skip the validation
		     * 104480: Lot tracking required ?
		     */
		     v_flag := get_part_attr_value_by_comp(v_pnum,'104480',v_comp_id);

		     -- If the attribute is not set for the part, then should not validat the part
		     IF v_flag IS NULL OR v_flag <> 'Y' THEN
		     	CONTINUE;
		     END IF;
		   		     
			gm_pkg_op_lot_track.gm_validate_input_data(p_reftype,p_consignid,'93343',v_whtype,v_pnum,v_control,v_qty, v_err_msg);

			-- Set the flag to 'Y', if any error message is there to return the value 
			IF v_err_msg IS NOT NULL OR v_err_msg != '' THEN
				v_err_fl := 'Y';
			END IF;	  		
			
			IF (v_control = 'TBE' OR v_control = 'tbe')
	      	THEN
	         	v_control := NULL;
	      	END IF;
			-- update the error message in the table
			UPDATE t505_item_consignment
				SET C505_ERROR_DTLS = v_err_msg
			WHERE c205_part_number_id = v_pnum
				AND NVL(C505_CONTROL_NUMBER ,'-9999') = NVL(v_control,'-9999')
				AND C504_CONSIGNMENT_ID = p_consignid
				AND C505_VOID_FL  IS NULL;
			
		END LOOP;
		
		p_err_fl := v_err_fl;
		END IF;
		
	END gm_sav_othr_inhouse_lot_num;
END gm_pkg_op_inhouse_master;
/