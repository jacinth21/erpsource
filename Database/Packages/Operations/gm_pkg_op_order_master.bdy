/* Formatted on 2009/09/18 16:02 (Formatter Plus v4.8.0) */

--@"c:\database\packages\operations\gm_pkg_op_order_master.bdy"

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_order_master
IS
/*******************************************************
 * Purpose: This Procedure is used to save order details
 *******************************************************/
--
	PROCEDURE gm_op_sav_order_detail (
		p_order_id	   IN	t502_item_order.c501_order_id%TYPE
	  , p_pnumid	   IN	t502_item_order.c205_part_number_id%TYPE
	  , p_item_qty	   IN	t502_item_order.c502_item_qty%TYPE
	  , p_controlnum   IN	t502_item_order.c502_control_number%TYPE
	  , p_price 	   IN	t502_item_order.c502_item_price%TYPE
	  , p_type		   IN	t502_item_order.c901_type%TYPE	 -- consign/loaner currently hardcoded to consign
	  , p_void_flg	   IN	t502_item_order.c502_void_fl%TYPE
	)
	AS
	BEGIN
		INSERT INTO t502_item_order
					(c502_item_order_id, c501_order_id, c205_part_number_id, c502_item_qty, c502_control_number
				   , c502_item_price, c901_type, c502_void_fl
					)
			 VALUES (s502_item_order.NEXTVAL, p_order_id, p_pnumid, p_item_qty, p_controlnum
				   , p_price, NVL(p_type,50300), p_void_flg
					);
	END gm_op_sav_order_detail;

	/*******************************************************
	* Purpose: This Procedure is used to fetch order details for part
	*******************************************************/
	PROCEDURE gm_fch_part_order_det (
		p_partnum			   IN		t502_item_order.c205_part_number_id%TYPE
	  , p_order_to_date 	   IN		VARCHAR2
	  , p_order_from_date	   IN		VARCHAR2
	  , p_order_type		   IN		t502_item_order.c901_type%TYPE
	  , p_rep_id			   IN		NUMBER
	  , p_dist_id			   IN		NUMBER
	  , p_part_order_det_cur   OUT		CLOB
	)
	AS
	v_datefmt VARCHAR2(20);
	v_comp_id T1900_COMPANY.C1900_COMPANY_ID%TYPE;
	v_plant_id T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
	v_filter T906_RULES.C906_RULE_VALUE%TYPE;
	BEGIN
		
	SELECT get_compid_frm_cntx(), get_plantid_frm_cntx(),get_compdtfmt_frm_cntx()
	  INTO v_comp_id, v_plant_id, v_datefmt
	  FROM dual;
   
	SELECT DECODE(GET_RULE_VALUE(v_comp_id,'ORDFILTER'), null, v_comp_id, v_plant_id) --ORDFILTER- To fetch the records for EDC entity countries based on plant
	  INTO v_filter 
	  FROM dual;

			 SELECT JSON_ARRAYAGG (JSON_OBJECT ('acctnum' VALUE t501.c704_account_id , 'acctname' VALUE get_account_name (t501.c704_account_id) 
				  , 'repname' VALUE get_rep_name (t501.c703_sales_rep_id) , 'DO' VALUE t501.c501_order_id 
				  , 'ordertyp' VALUE get_code_name_alt (t502.c901_type) , 'orddate' VALUE TO_CHAR(t501.c501_order_date ,v_datefmt), 'qty' VALUE t502.c502_item_qty 
				  , 'pnum' VALUE t502.c205_part_number_id , 'pdesc' VALUE get_partnum_desc (t502.c205_part_number_id) ) RETURNING CLOB)
	           INTO p_part_order_det_cur
			   FROM t501_order t501, t502_item_order t502, t703_sales_rep t703
			  WHERE t501.c501_order_id = t502.c501_order_id
				AND t501.c501_void_fl IS NULL
				AND t502.c502_void_fl IS NULL
				--As of now the below commented conditions are not needed
				--AND (t501.c901_ext_country_id IS NULL OR t501.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH
				--AND (t703.c901_ext_country_id IS NULL OR t703.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH
				AND t501.c501_delete_fl IS NULL
				AND NVL (t501.c901_order_type, -9999) <> '2533'
				AND NVL (c901_order_type, -9999) NOT IN (
	                SELECT t906.c906_rule_value
	                  FROM t906_rules t906
	                 WHERE t906.c906_rule_grp_id = 'ORDTYPE'
	                   AND c906_rule_id = 'EXCLUDE')
				AND TRUNC (t501.c501_order_date) >=
						DECODE (TO_DATE (p_order_from_date, v_datefmt)
							  , '', TRUNC (t501.c501_order_date)
							  , TO_DATE (p_order_from_date, v_datefmt)
							   )
				AND TRUNC (t501.c501_order_date) <=
						DECODE (TO_DATE (p_order_to_date, v_datefmt)
							  , '', TRUNC (t501.c501_order_date)
							  , TO_DATE (p_order_to_date, v_datefmt)
							   )
				AND t502.c901_type = DECODE (p_order_type, 0, t502.c901_type, p_order_type)
				AND t703.c701_distributor_id = DECODE (p_dist_id, 0, t703.c701_distributor_id, p_dist_id)
				AND regexp_like (t502.c205_part_number_id, NVL (REGEXP_REPLACE(p_partnum,'[+]','\+'), REGEXP_REPLACE(t502.c205_part_number_id,'[+]','\+')))
				AND t501.c703_sales_rep_id = DECODE (p_rep_id, 0, t501.c703_sales_rep_id, p_rep_id)
				AND t703.c703_sales_rep_id = t501.c703_sales_rep_id
				AND (t501.c1900_company_id = v_filter
			         OR t501.c5040_plant_id   = v_filter);
	END gm_fch_part_order_det;
	
	/*******************************************************
	* Purpose: This Procedure is used to move do to backorder
	*******************************************************/
	PROCEDURE gm_move_do_to_backorder(
		 p_OrderId IN T501_ORDER.C501_ORDER_ID%TYPE,
 		p_user_id IN T501_ORDER.C501_LAST_UPDATED_BY%TYPE
 	)
 	AS
 	v_status_fl NUMBER;
	BEGIN
		SELECT TO_NUMBER(C501_STATUS_FL) 
    INTO v_status_fl
    FROM T501_ORDER WHERE  C501_ORDER_ID = p_OrderId; 
		
  		IF(v_status_fl > 1)
      THEN
     		raise_application_error ('-20935', '');
		  END IF;
		
 		UPDATE  T501_ORDER
  		SET  C501_STATUS_FL  = '0',
        C901_ORDER_TYPE = '2525',
    		C501_LAST_UPDATED_BY = p_user_id,
    		C501_LAST_UPDATED_DATE = CURRENT_DATE
  		WHERE  C501_ORDER_ID = p_OrderId;
  		
  		/* if we move the order to backorder then void the entries in below table to avoid submitting order in device
  		 */
  		 UPDATE t5050_invpick_assign_detail SET
		     c5050_void_fl = 'Y',
		     c5050_last_updated_by = p_user_id,
		     c5050_last_updated_date = CURRENT_DATE
	     WHERE c5050_ref_id = p_OrderId
	     AND c5050_void_fl IS NULL;
	     
	     UPDATE t5055_control_number SET
		     c5055_void_fl = 'Y',
		     c5055_last_updated_by = p_user_id,
		     c5055_last_updated_date = CURRENT_DATE
	     WHERE c5055_ref_id = p_OrderId
	     AND c5055_void_fl IS NULL;

	END gm_move_do_to_backorder;

/* Formatted on 2011/05/25 05:00 (Formatter Plus v4.8.0) */
PROCEDURE gm_op_sav_lon_con_swap (
	p_bo_string 		IN	 VARCHAR2
  , p_raid				IN	 t506_returns.c506_rma_id%TYPE
  , p_parent_order_id	IN	 t506_returns.c501_order_id%TYPE
  , p_usercomments		IN	 t501_order.c501_comments%TYPE
  , p_item_type 		IN	 NUMBER
  , p_status_fl 		IN	 t501_order.c501_status_fl%TYPE
  , p_userid			IN	 t501_order.c501_created_by%TYPE
)
AS
	v_stock 	   NUMBER := 0;
	v_qty		   NUMBER := 0;
	v_price 	   NUMBER := 0;
	v_total_cost   NUMBER := 0;
	v_pnum		   t205_part_number.c205_part_number_id%TYPE;
	v_strlen	   NUMBER := 0;
	v_substring    VARCHAR2 (1000);
	v_string	   VARCHAR2 (1000);
	v_border_id    VARCHAR2 (100);
	v_control_dr_num VARCHAR2 (20);
	v_shipid	   VARCHAR2 (50);
	v_shiptoid	   t907_shipping_info.c901_ship_to%TYPE;
	v_addressid    t907_shipping_info.c106_address_id%TYPE;
	v_list_price		t502_item_order.c502_list_price%TYPE;
    v_unit_price 		t502_item_order.c502_unit_price%TYPE;
    v_unit_prc_adj_code t502_item_order.c901_unit_price_adj_code%TYPE;
    v_unit_price_adj_val t502_item_order.c502_unit_price_adj_value%TYPE;
    v_disc_offered 		t502_item_order.c502_discount_offered%TYPE;
    v_disc_type			t502_item_order.c901_discount_type%TYPE;
    v_item_ordid t502_item_order.c502_item_order_id%TYPE;
    v_adj_code_value t1706_party_price_adj.c1706_adj_value%TYPE;
    v_do_unit_price t502_item_order.C502_DO_UNIT_PRICE%TYPE;
    v_account_id    t501_order.c704_account_id%TYPE;
    v_acc_cmp_id    t704_account.c704_account_id%TYPE;
    v_user_plant_id   t501_order.c5040_plant_id%TYPE;
BEGIN
	v_strlen	:= NVL (LENGTH (p_bo_string), 0);

	IF (v_strlen > 0)
	THEN
		v_string	:= p_bo_string;

		WHILE INSTR (v_string, '|') <> 0
		LOOP
			v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
			v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
			v_pnum		:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_qty		:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_price 	:= v_substring;
			/*	IF v_create_border = 'YES'
				THEN
					v_create_border := 'NO'; */
					
		 --Need To Fetch Account ID
		   SELECT  c704_account_id INTO v_account_id
		   FROM t501_order 
		   WHERE c501_order_id = p_parent_order_id;
		BEGIN
        --getting company id and plant id mapped from account
        SELECT c1900_company_id,gm_pkg_op_plant_rpt.get_default_plant_for_comp(gm_pkg_it_user.get_default_party_company(p_userid))
		  INTO v_acc_cmp_id, v_user_plant_id  
		  FROM T704_account 
		 WHERE c704_account_id = v_account_id 
		   AND c704_void_fl IS NULL;
	    EXCEPTION
           WHEN NO_DATA_FOUND
           THEN
           v_acc_cmp_id := NULL;
           v_user_plant_id := NULL;
        END;
			SELECT 'GM' || s501_order.NEXTVAL INTO v_border_id  FROM DUAL;
	-- Added c501_customer_po_date as field for PC-3880 New field in record PO Date in GM Italy
			INSERT INTO t501_order
						(c501_order_id, c704_account_id, c703_sales_rep_id, c501_customer_po, c501_order_date
					   , c501_total_cost, c501_created_date, c501_created_by, c501_status_fl, c501_order_date_time
					   , c901_order_type, c501_parent_order_id, c501_comments, c506_rma_id, c503_invoice_id, c1900_company_id, c5040_plant_id
					   , c1910_division_id, c101_dealer_id, c501_surgery_date,c501_customer_po_date)
				SELECT v_border_id, c704_account_id, c703_sales_rep_id, c501_customer_po, TRUNC (CURRENT_DATE), 0, CURRENT_DATE
					 , p_userid, p_status_fl, CURRENT_DATE, 2525, p_parent_order_id, p_usercomments, p_raid
					 , c503_invoice_id, nvl(c1900_company_id,v_acc_cmp_id), nvl(c5040_plant_id,v_user_plant_id)
					 , c1910_division_id, c101_dealer_id, c501_surgery_date,c501_customer_po_date
				  FROM t501_order
				 WHERE c501_order_id = p_parent_order_id;

--			END IF;
			gm_pkg_op_order_master.gm_op_sav_order_detail (v_border_id
														 , v_pnum
														 , v_qty
														 , v_control_dr_num
														 , v_price
														 , p_item_type
														 , ''
														  );
														  
			SELECT	C507_LIST_PRICE, C507_UNIT_PRICE, C901_UNIT_PRICE_ADJ_CODE, C507_UNIT_PRICE_ADJ_VALUE
					, C507_DISCOUNT_OFFERED, C901_DISCOUNT_TYPE , C507_DO_UNIT_PRICE, C507_ADJ_CODE_VALUE
	  			INTO  v_list_price, v_unit_price, v_unit_prc_adj_code, v_unit_price_adj_val
	  				, v_disc_offered,v_disc_type ,v_do_unit_price , v_adj_code_value
			FROM t507_returns_item
		   	WHERE c506_rma_id =  p_raid 
		   	AND c205_part_number_id = v_pnum 
		   	AND c507_item_price = v_price
	       	AND ROWNUM = 1;
														  
			UPDATE t502_item_order 
               SET C502_LIST_PRICE = v_list_price,
				   C502_UNIT_PRICE = v_unit_price,
				   C901_UNIT_PRICE_ADJ_CODE = v_unit_prc_adj_code,
				   C502_UNIT_PRICE_ADJ_VALUE = v_unit_price_adj_val,
				   C502_DISCOUNT_OFFERED = v_disc_offered,
				   C901_DISCOUNT_TYPE = v_disc_type,
				   C502_DO_UNIT_PRICE = v_do_unit_price,
				   C502_ADJ_CODE_VALUE = v_adj_code_value
             WHERE c501_order_id = v_border_id;
             
	SELECT SUM (c502_item_qty * c502_item_price)
	  INTO v_total_cost
	  FROM t502_item_order
	 WHERE c501_order_id = v_border_id;

	UPDATE t501_order
	   SET c501_total_cost = v_total_cost
		 , c501_last_updated_by = p_userid
		 , c501_last_updated_date = CURRENT_DATE	   
	 WHERE c501_order_id = v_border_id;

	BEGIN
		SELECT c703_sales_rep_id
		  INTO v_shiptoid
		  FROM t501_order
		 WHERE c501_order_id = p_parent_order_id;

		SELECT address_id
		  INTO v_addressid
		  FROM (SELECT	 c106_address_id address_id
					FROM t106_address
				   WHERE c101_party_id IN (SELECT c101_party_id
											FROM t703_sales_rep
										   WHERE c703_sales_rep_id = v_shiptoid) AND c106_void_fl IS NULL
				ORDER BY c106_primary_fl) address
		 WHERE ROWNUM = 1;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			IF v_shiptoid IS NULL
			THEN
				v_shiptoid	:= '';
			END IF;

			IF v_addressid IS NULL
			THEN
				v_addressid := '';
			END IF;
	END;
	
	v_shipid := '';
	gm_pkg_cm_shipping_trans.gm_sav_shipping (v_border_id
											, 50180   --orders
											, 4121
											, v_shiptoid
											, 5001
											, 5004
											, NULL
											, v_addressid
											, 0
											, p_userid
											, v_shipid
											 );									
END LOOP;
	END IF;											 
END gm_op_sav_lon_con_swap;



 /*******************************************************
	  * Description : Procedure to void a sales order
	  * Parameters	 : 1. Transaction Id - DO ID
	  * 		2. User Id
	  * Validation --
	  *  1. If no linked orders are present, then the DO can be voided and the DO ID is made available
	  *  2. If there are linked orders and all orders are in the same status (pending control #), then if the parent DO is voided, the linked orders will be voided as well
	  *  3. Linked Orders can be voided without affecting the parent order
	  *
	  *******************************************************/
	PROCEDURE gm_cs_sav_void_order (
		p_txn_id		 IN   t907_cancel_log.c907_ref_id%TYPE
	  , p_cancelreason	 IN   t907_cancel_log.c901_cancel_cd%TYPE
	  , v_cancelid		 IN   t907_cancel_log.c907_cancel_log_id%TYPE
	  , p_userid		 IN   t102_user_login.c102_user_login_id%TYPE
	)
	AS
		v_id		   	  VARCHAR2 (30);
		p_newid 	   	  VARCHAR2 (30);
		v_status_fl    	  t501_order.c501_status_fl%TYPE;
		v_void_fl	   	  CHAR (1);
		v_invfl 	   	  CHAR (1);
		v_childcnt	  	  NUMBER;
		v_psfgcnt 	   	  NUMBER;
		v_dept_id      	  NUMBER;
		v_status	   	  t501_order.c501_status_fl%TYPE;
		v_type         	  t501_order.c901_order_type%TYPE;
		v_parent_order_id t501_order.c501_parent_order_id%TYPE;
		v_spart_cnt		  NUMBER;
		v_order_date   t501_order.c501_order_date%TYPE;
		v_child_str	  	  VARCHAR2(4000);
		v_orderid_str	  VARCHAR2(4000) := p_txn_id;		       

		v_attrib_val    VARCHAR2(20);
		v_rep_id		t703_sales_rep.C703_SALES_REP_ID%TYPE;
		v_email_id		CLOB;
		v_data_cur	   	TYPES.CURSOR_TYPE;
		v_rank 			NUMBER;
		v_total			NUMBER;
		v_acc_id		t501_order.C704_ACCOUNT_ID%TYPE;
		v_acc_nm		VARCHAR2 (1000);
		v_doid			t501_order.c501_parent_order_id%TYPE;
		v_rej_dt		t501_order.c501_order_date%TYPE;
		v_rej_by		VARCHAR2 (100);
		v_reason		VARCHAR2 (1000);
		v_comments		VARCHAR2 (1000);
		v_subject		VARCHAR2 (1000);
		v_cclist		VARCHAR2 (1000);
		v_rejmsg		VARCHAR2 (4000);
		v_message		VARCHAR2 (4000);
		v_ra_status      VARCHAR2(5);
		v_out_cur  		TYPES.cursor_type;
		--
		v_ds_order_cnt NUMBER;
		v_master_parent_ord_id t501_order.c501_order_id%TYPE;	
		v_cnt_status NUMBER;
		v_orderpick_status t5050_invpick_assign_detail.c901_status%TYPE;
	    v_account_id    t501_order.c704_account_id%TYPE;
        v_acc_cmp_id    t704_account.c704_account_id%TYPE;
        v_user_plant_id   t501_order.c5040_plant_id%TYPE;
		CURSOR cur_child_ord
    	IS
         SELECT c501_order_id child_ord_id
           FROM t501_order
          WHERE c501_parent_order_id=p_txn_id
          AND c501_status_fl = 2
          AND c501_void_fl IS NULL;
		
	BEGIN
		--
		SELECT c501_status_fl, c501_void_fl, c501_update_inv_fl, NVL(c901_order_type, 9999), c501_parent_order_id, c501_order_date
			, get_parent_order_id (c501_order_id)
		  INTO v_status_fl, v_void_fl, v_invfl, v_type, v_parent_order_id, v_order_date 
		  , v_master_parent_ord_id
		  FROM t501_order
		 WHERE c501_order_id = p_txn_id
		 FOR UPDATE;

 -- To select status for Inventory Pick Order
			SELECT
			  COUNT(1)
			INTO
			  v_cnt_status
			FROM
			  t5050_invpick_assign_detail t5050
			WHERE
			  t5050.c901_status IN
			  (
			    SELECT
			      c906_rule_id
			    FROM
			      t906_rules
			    WHERE
		      c906_rule_grp_id = 'INVPICKSTATUS'
			      --AND c1900_company_id = v_company_id
			    AND c906_void_fl IS NULL
			  )
			AND t5050.c5050_void_fl IS NULL AND t5050.c5050_ref_id = p_txn_id AND
			v_status_fl              = 1;
			-- check  void order status and inventory pick status for cannot void
			IF v_cnt_status = 1 THEN
			GM_RAISE_APPLICATION_ERROR('-20999','778','');
			END IF;
					
		SELECT NVL (COUNT (1), 0)
		  INTO v_childcnt
		  FROM t501_order
		 WHERE c501_parent_order_id = p_txn_id AND c501_update_inv_fl IS NOT NULL;
		 --
		 
		--To Validate any RA created for loaner to consign swap
	    IF v_type = 2531
        THEN
          v_ra_status :=gm_pkg_op_order_rpt.get_partswap_ra_status(v_parent_order_id,v_type);
        END IF;    
        IF v_ra_status = 'Y' THEN
          GM_RAISE_APPLICATION_ERROR('-20999','253',p_txn_id);
        END IF;
		 
        
		--Getting all the child records as ',' seperated
		SELECT RTRIM(XMLAGG(XMLELEMENT(E,c501_order_id || ',')).EXTRACT('//text()').GETCLOBVAL(),',') 
			INTO v_child_str
            FROM T501_ORDER 
            WHERE c501_parent_order_id = p_txn_id 
            AND c501_update_inv_fl IS NULL;
	
		--If there is any back order, add it with the parent order to make a string
		IF v_child_str IS NOT NULL
		THEN
			v_orderid_str:= p_txn_id || ',' || v_child_str;
		END IF;
		
		--
		SELECT NVL (COUNT (1), 0)
		  INTO v_psfgcnt
		  FROM t412_inhouse_transactions 
		 WHERE c412_ref_id = p_txn_id
		   AND c412_status_fl < 3 --not controlled PSFG
		   AND c412_type = 100406
		   AND c412_void_fl IS NULL;
		--
		SELECT get_dept_id(p_userid) INTO v_dept_id FROM DUAL;
		
		IF v_type = 2535 -- Sales Discount
		THEN
		    GM_RAISE_APPLICATION_ERROR('-20999','254','');
		END IF;

		IF v_dept_id = 2005 AND v_status_fl <> 7
      	THEN
       	GM_RAISE_APPLICATION_ERROR('-20999','255','');

      	END IF;
  	
  	    IF TO_CHAR(v_order_date,'MM') != TO_CHAR(CURRENT_DATE,'MM') -- If not in the same month 
        THEN
            GM_RAISE_APPLICATION_ERROR('-20999','256','');
        END IF;
        
        IF v_status_fl > 2 AND v_status_fl < 3 
        THEN       
        GM_RAISE_APPLICATION_ERROR('-20999','777','');        
        END IF;

		--
		IF (v_void_fl IS NOT NULL)
		THEN
			-- Error Messaage. This order has already been made void.
			raise_application_error (-20801, '');
		ELSIF (v_invfl IS NOT NULL)
		THEN
			-- Error Messaage. This transaction has already completed Inventory postings.
			raise_application_error (-20936, '');
		ELSIF (v_childcnt > 0)
		THEN
			-- Error Messaage. DO cannot be voided as there are linked orders that have been transacted.
			raise_application_error (-20937, '');
		ELSIF (v_psfgcnt > 0)
		THEN
			--Error message if there is  any uncontrolled PSFG transaction for the Order.
			raise_application_error (-20938, '');
		ELSE
			--
			v_id		:= get_next_order_id (NULL);
			p_newid 	:= v_id;
         --Need To Fetch Account ID
           SELECT  c704_account_id INTO v_account_id
           FROM t501_order 
           WHERE c501_order_id = v_parent_order_id;
          BEGIN
        --getting company id and plant id mapped from account
           SELECT c1900_company_id,gm_pkg_op_plant_rpt.get_default_plant_for_comp(gm_pkg_it_user.get_default_party_company(p_userid))
           INTO v_acc_cmp_id, v_user_plant_id  
           FROM T704_account 
           WHERE c704_account_id = v_account_id 
           AND c704_void_fl IS NULL;   
          EXCEPTION
           WHEN NO_DATA_FOUND
           THEN
           v_acc_cmp_id := NULL;
           v_user_plant_id := NULL;
          END;
			-- Added c501_customer_po_date as field for PC-3880 New field in record PO Date in GM Italy
			INSERT INTO t501_order
						(c501_order_id, c704_account_id, c501_receive_mode, c501_received_from, c703_sales_rep_id
					   , c501_ship_to, c501_ship_to_id, c501_customer_po, c501_total_cost, c501_order_date
					   , c501_created_by, c501_created_date, c501_status_fl, c501_order_date_time
					   , c501_parent_order_id, c901_reason_type, c501_comments, c501_last_updated_by
					   , c501_last_updated_date, c501_void_fl, c1900_company_id, c5040_plant_id, c1910_division_id
					   , c101_dealer_id, c501_surgery_date,c501_customer_po_date)
				SELECT v_id, c704_account_id, c501_receive_mode, c501_received_from, c703_sales_rep_id, c501_ship_to
					 , c501_ship_to_id, c501_customer_po, c501_total_cost, c501_order_date, c501_created_by
					 , TRUNC (CURRENT_DATE), c501_status_fl, c501_order_date_time, p_txn_id, p_cancelreason
					 , c501_comments || CURRENT_DATE || ' - Orig Order ID was: ' || p_txn_id
					 , p_userid, CURRENT_DATE, 'Y', nvl(c1900_company_id,v_acc_cmp_id), nvl(c5040_plant_id,v_user_plant_id), c1910_division_id          
					 , c101_dealer_id, c501_surgery_date,c501_customer_po_date
				  FROM t501_order
				 WHERE c501_order_id = p_txn_id;

			---
		   SELECT c501_status_fl
		  	 INTO v_status
		  	 FROM t501_order
		 	WHERE c501_order_id = p_txn_id AND c501_void_fl IS NULL;
		 	
		 	-- Fetch the trasaction details of order
			gm_pkg_ship_rollback_txn.gm_fch_order_txn_details(p_txn_id, v_out_cur);
			-- Update the lot status to null from controlled
			gm_pkg_ship_rollback_txn.gm_sav_rollback_lot_status(v_out_cur,p_userid);
			
			-- Fetch the trasaction details of order
			gm_pkg_ship_rollback_txn.gm_fch_order_attr_txn_details(p_txn_id, v_out_cur);
			-- Update the lot status to null from controlled
			gm_pkg_ship_rollback_txn.gm_sav_rb_ord_lot_status(v_out_cur,p_userid);
	
			IF  v_status = 2 THEN
                  GM_CS_SAV_PSFG_VOID_ORD (p_txn_id, p_newid, p_userid);
			END IF;
			
			-- to get the DS order count (PMT-20705)
			SELECT	 COUNT(1)
				INTO v_ds_order_cnt
				FROM t501_order t501
			   WHERE t501.c501_parent_order_id = v_master_parent_ord_id 
			     AND t501.c901_order_type = 2535 -- DS order
			     AND t501.c501_void_fl IS NULL
			     AND t501.c501_delete_fl IS NULL
			     AND (t501.c901_ext_country_id IS NULL 
			     	  OR t501.c901_ext_country_id  in (select country_id from v901_country_codes)
			     	 );
		
					     	 
			-- Marking the items of the cloned DO as void
			UPDATE t502_item_order
			   SET c502_void_fl = 'Y'
			 WHERE c501_order_id = p_txn_id; 
			 
			 /* If child order has s parts that has (O.R.) control numbers, 
			  * then before voiding the child order, the control numbers
			  * need to be removed by the users, so validate the qty and throw an 
			  * error while voiding.
			  */
			 
			 IF v_parent_order_id IS NOT NULL THEN
			 
			  SELECT NVL(SUM(DECODE(gm_pkg_op_do_process_trans.get_control_number_needed_fl(c205_part_number_id),'Y',1,0)),0)
			   INTO v_spart_cnt
				FROM
				(SELECT c205_part_number_id
				from t502_item_order
				WHERE c501_order_id = p_txn_id
				INTERSECT
				SELECT c205_part_number_id
				FROM t502_item_order
				WHERE c501_order_id = v_parent_order_id);
				
				 IF v_spart_cnt  > 0 THEN
					-- To validate for the attribute qty and item qty.
			   		 gm_pkg_op_do_process_trans.gm_validate_do_attribute_qty(p_txn_id,101723);		   		 
		   		 END IF;		    
		   		 
		    END IF;
		    
		     -- Marking the order attributes of the cloned DO as void
			 UPDATE t501a_order_attribute
			   SET c501a_void_fl = 'Y'
				 , c501_order_id = v_id
				 , c501a_last_updated_by = p_userid
				 , c501a_last_updated_date = CURRENT_DATE
			 WHERE c501_order_id = p_txn_id;
		    
		    -- Marking the items of the cloned DO as void
		    UPDATE t502_item_order
			   SET c501_order_id = v_id
			 WHERE c501_order_id = p_txn_id; 
		    
			-- Marking the item attributes of the cloned DO as void
			 UPDATE t502a_item_order_attribute
			   SET c502a_void_fl = 'Y'
				 , c501_order_id = v_id
				 , c502a_last_updated_by = p_userid
				 , c502a_last_updated_date = CURRENT_DATE
			 WHERE c501_order_id = p_txn_id;
			 
			 -- Marking the Shipping details of the cloned DO as void
			 
			 update t907_shipping_info t907 
				set t907.c907_void_fl = 'Y'
				   ,t907.c907_ref_id = v_id
				   ,t907.c907_last_updated_by =  p_userid
				   ,t907.c907_last_updated_date = CURRENT_DATE
			  where t907.c907_ref_id = p_txn_id; 
			  
  			-- Voiding the Reserved Parts from t5062_control_number_reserve table 
	         UPDATE t5062_control_number_reserve 
                SET c5062_void_fl = 'Y'
                  , c5062_last_updated_by = p_userid
                  , c5062_last_updated_date = CURRENT_DATE
              WHERE c901_transaction_type = '103980' -- Order Transaction
                AND c5062_transaction_id = p_txn_id
                AND c5062_void_fl IS NULL; -- Added void flag check PMT-46880

			 -- Marking the order system of the cloned DO as void 
			 UPDATE t501b_order_by_system
			    SET c501b_void_fl     = 'Y'
			      , c501_order_id = v_id
			      , c501b_last_updated_by = p_userid
			      , c501b_last_updated_date = CURRENT_DATE
			  WHERE c501_order_id = p_txn_id;
			 
			 -- Added for PC-1726 auto revenue tracking 
			 -- Marking the order auto revenue as void
			 UPDATE  t5005_ar_revenue_scan_dtls 
			 	SET  c5005_void_fl = 'Y'
			 		,c501_order_id = v_id
			 		,c5005_last_updated_by = p_userid
			 		,c5005_last_updated_date = CURRENT_DATE
			 	WHERE c501_order_id = p_txn_id;
			 	
			-- Added for PC-310 Unable to void orders by CS team
			-- Making the CS team unable to void order
			UPDATE	t5003_order_revenue_sample_dtls
				SET c5003_void_fl = 'Y',
					c501_order_id = v_id,
					c5003_updated_by = p_userid,
					c5003_updated_date = current_date
				WHERE c501_order_id = p_txn_id;
			   
			-- Added for PC-310 Unable to void orders by CS team
			-- Making the CS team unable to void order
			UPDATE  t5001_order_po_dtls
				SET C5001_VOID_FL = 'Y',
					C501_ORDER_ID = v_id,
					C5001_LAST_UPDATED_BY = p_userid,
					C5001_LAST_UPDATED_DATE = current_date
				WHERE c501_order_id = p_txn_id;
			 				 
			--call recursive of void order prc to void all controlled child orders and generate PSFG for it.
			FOR var_child_ord IN cur_child_ord
    		LOOP
       			gm_cs_sav_void_order (var_child_ord.child_ord_id,p_cancelreason, v_cancelid, p_userid);
    		END LOOP;
			 -- if order is part of batch, then remove the orders to batch (invoice batch).
    		gm_pkg_ac_ar_batch_trans.gm_void_batch_order_dtls(p_txn_id,p_userid);
			-- Marking the items of Child Orders as void
			UPDATE t502_item_order
			   SET c502_void_fl = 'Y'
			 WHERE c501_order_id IN (SELECT c501_order_id
									   FROM t501_order
									  WHERE c501_parent_order_id = p_txn_id);

			-- Marking the order usage lot number - DO as void
			 UPDATE t502b_item_order_usage
			SET c502b_void_fl           = 'Y', c501_order_id = v_id, c502b_last_updated_by = p_userid
			  , c502b_last_updated_date = CURRENT_DATE
			  WHERE c501_order_id      IN
			    (
			         SELECT c501_order_id
			           FROM t501_order
			          WHERE c501_order_id       = p_txn_id
			            OR c501_parent_order_id = p_txn_id
			    ) ;
			-- Marking the order info - as void
			 UPDATE t500_order_info
			SET c501_order_id          = v_id, c500_void_fl = 'Y', c500_last_updated_by = p_userid
			  , c500_last_updated_date = CURRENT_DATE
			  WHERE c501_order_id     IN
			    (
			         SELECT c501_order_id
			           FROM t501_order
			          WHERE c501_order_id       = p_txn_id
			            OR c501_parent_order_id = p_txn_id
			    ) ;	
			    
			---- Marking the child order system of the cloned DO as void 
			 UPDATE t501b_order_by_system
			    SET c501b_void_fl     = 'Y'
			    , c501b_last_updated_by = p_userid
			    , c501b_last_updated_date = CURRENT_DATE
			  WHERE c501_order_id IN (SELECT c501_order_id
								        FROM t501_order
								       WHERE c501_parent_order_id = p_txn_id);
			--
			-- Marking the Child Orders as Void
			UPDATE t501_order
			   SET c501_void_fl = 'Y'
				 , c501_parent_order_id = v_id
				 , c501_last_updated_by = p_userid
				 , c501_last_updated_date = CURRENT_DATE
			 WHERE c501_order_id IN (SELECT c501_order_id
									   FROM t501_order
									  WHERE c501_parent_order_id = p_txn_id
									  	AND c501_order_id != v_id);
									  	
			-- Marking the Child Orders as Void in T501d_Order_Tag_Usage_Details
			UPDATE T501d_Order_Tag_Usage_Details
			   SET c501d_void_fl = 'Y',
			   	   c501_order_id = v_id,
				  C501d_Last_Updated_By = p_userid,
				  C501d_Last_Updated_Date = CURRENT_DATE
			 WHERE C501_Order_Id = p_txn_id;

            -- Save the status as Do Voided (New Code Lookup Type)
            gm_pkg_cm_status_log.gm_sav_status_details(p_txn_id, NULL, p_userid, NULL, CURRENT_DATE, 101862, 91102);
            -- Do Voided
            
           	-- PMT-28394 (To avoid the DS order update, System automatically void the DS Order)

			--
			DELETE		t512_order_construct
				  WHERE c501_order_id = p_txn_id;

			--
			-- Deleting the DO Id so it can be reused again
			DELETE		t501_order
				  WHERE c501_order_id = p_txn_id;

			--
			UPDATE t907_cancel_log
			   SET c907_ref_id = v_id
				 , c907_comments = c907_comments || ':Orig Order ID was: ' || p_txn_id
			 WHERE c907_cancel_log_id = v_cancelid;
			 
			-- update the order's log comments
			UPDATE t902_log 
			   SET c902_ref_id = v_id 
			 WHERE c902_ref_id = p_txn_id 
			   AND c902_void_fl IS NULL;
			 
		END IF;

		gm_pkg_common_cancel.gm_void_shipping (p_txn_id, p_userid, 50180);	--order

		-- make the shipp info of child orders as void also  issue 4341
		UPDATE t907_shipping_info
		   SET c907_void_fl = 'Y'
			 , c907_last_updated_date = CURRENT_DATE
			 , c907_last_updated_by = p_userid
			 , c901_source = 50180
		 WHERE c907_ref_id IN (SELECT c501_order_id
								 FROM t501_order
								WHERE c501_parent_order_id = p_txn_id);

-- As the order is deleted from order tbl and new order is entered, updating the existing order id in shipping tbl to the new ref id
		UPDATE t907_shipping_info
		   SET c907_ref_id = v_id
			 , c907_last_updated_date = CURRENT_DATE
			 , c907_last_updated_by = p_userid
		 WHERE c901_source = 50180 AND c907_ref_id = p_txn_id AND c907_void_fl = 'Y';
--
		-- Void Records In t5050_invpick_assign_detail for removing allocation in device.
		gm_pkg_allocation.gm_inv_void_bytxn (p_txn_id, '', p_userid);
		-- As the order is deleted, need to void the record in location table to adjust the location qty.
		gm_pkg_op_item_control_txn.gm_void_control_number_txn (p_txn_id, p_userid);
	
--
--void the order transaction related to Inserts
	gm_pkg_op_sav_insert_txn.gm_void_insert_details(p_txn_id,'106760',p_userid); --106760 --Insert required for ORDER
	
	--To void the FGBin id corresponding to the transaction
	-- 2525: Back Order, For parent order, If v_type is null, we are getting the value '9999'
	IF v_type = '2525' OR  v_type = '9999'
	THEN
		gm_pkg_op_item_control_txn.gm_void_FGBinId(v_orderid_str,p_userid);
	END IF;
	
	--Fetching Order Source.    
	SELECT get_order_attribute_value(p_txn_id,103560) into v_attrib_val FROM dual;
		
	--If CS voids the DO which is booked by DO APP, Rep should be notified via Email.
	--Attrib Val : 103521  - DO Booked From DO APP.	
	
	IF v_attrib_val = '103521' THEN 
		
		--Once the order is voided, Taking the c703_sales_rep_id to sent email.
		SELECT c703_sales_rep_id into v_rep_id from t501_order where C501_PARENT_ORDER_ID = p_txn_id;
		
		--fetching Sales rep email ID
		SELECT get_cs_ship_email (4121, v_rep_id) into v_email_id from dual;
		
		--Fetching the rules based on Subject, CC List, message.
		SELECT GET_RULE_VALUE('103521', 'DO_REJECT_SUBJECT') INTO v_subject from dual;
		SELECT GET_RULE_VALUE('103521', 'DO_REJECT_CC_LIST') INTO v_cclist from dual;
		SELECT GET_RULE_VALUE('103521', 'DO_REJECT_MESSAGE') INTO v_rejmsg from dual;
		
		--Setting the order id in In list.
		my_context.set_my_inlist_ctx ('') ;
		my_context.set_my_inlist_ctx (p_txn_id);
		
		--This prc call is used to get the Rejected order details.
		gm_fch_rej_order_info(p_txn_id,v_data_cur);
		
		--Loop through the cursor and replace the order details with v_rejmsg  and v_subject.
		LOOP
		  FETCH v_data_cur 
		  INTO v_rank,v_doid,v_rej_dt,v_total,v_acc_id,v_acc_nm, v_rej_by,v_reason,v_comments;
		
		  
      		EXIT WHEN v_data_cur%NOTFOUND;				
		      --Replacing the p_message with due date, to month, to date.
				     	  
				SELECT REPLACE (v_rejmsg,  '#REP_NM', (get_user_name ( v_rep_id))) INTO v_message FROM dual;
				
				SELECT REPLACE (v_message, '#ORD_DATE', (v_order_date) ) INTO v_message FROM dual;
				
				SELECT REPLACE (v_message, '#VOID_BY', (v_rej_by)) INTO v_message FROM dual;
				
				SELECT REPLACE (v_message, '#VOID_REASON', (v_reason) ) INTO v_message FROM dual;
				
				SELECT REPLACE (v_message, '#VOID_COMMENTS', (v_comments)) INTO v_message FROM dual;
				
				SELECT REPLACE (v_message, '#ORDER_ACC', (v_acc_nm)) INTO v_message FROM dual;
				
				SELECT REPLACE (v_message, '#REP_NM', (get_user_name (v_rep_id))) INTO v_message FROM dual;
				
				SELECT REPLACE (v_message, '#ORD_TOT', (v_total)) INTO v_message FROM dual;

		    	SELECT REPLACE(v_subject,'#ORD_ID',v_doid) INTO v_subject FROM DUAL;

	  END LOOP;
	  
	  	-- Updating status in t5050_invpick_assign_detail table if it was completed in Portal
            gm_pkg_op_inv_scan.gm_sav_inv_status(p_txn_id,NULL,'Y',p_userid); 
	     -- sending mail to Rep. 
	   	 GM_COM_SEND_EMAIL_PRC(v_email_id,v_subject,v_message);
	 	 
	  END IF;
		  
	END gm_cs_sav_void_order;

	/***************************************************************
	* Description : Procedure to get the voided sales order details
	****************************************************************/
	PROCEDURE gm_fch_rej_order_info (
		p_order_id	   IN	t501_order.c501_order_id%TYPE,
		p_data_cur	   OUT	TYPES.CURSOR_TYPE
		)
	AS
	v_voided_order_id t501_order.c501_order_id%TYPE;
	BEGIN
	
		SELECT c501_order_id 
		  INTO v_voided_order_id 
		  FROM  t501_order 
		 WHERE C501_PARENT_ORDER_ID = p_order_id 
		   AND C501_VOID_FL ='Y';
		   
		OPEN p_data_cur
		FOR
		SELECT *  FROM
	    (
	         SELECT ROW_NUMBER () over (partition BY T501.C501_PARENT_ORDER_ID order by T907.C907_CREATED_DATE DESC) RANK,
	                T501.C501_PARENT_ORDER_ID DO_ID, 
	                T907.C907_CREATED_DATE REJECTED_DT, 
	                order_total.total total, 
	                t501.c704_account_id acctid, 
	                t704.c704_account_nm accnm, 
	                t101.C101_USER_F_NAME || ' ' || t101.C101_USER_L_NAME REJECTED_BY,
	                t901.C901_CODE_NM REASON,
	                SUBSTR (T907.C907_COMMENTS, 0, INSTR (T907.C907_COMMENTS, ':Orig Order ID') - 1) comments
	           FROM T501_ORDER T501, T907_CANCEL_LOG T907, t704_account t704, t901_code_lookup t901, 
	                t101_user t101, (
	               SELECT SUM (c501_total_cost) total	 
	                 FROM t501_order
	                WHERE c501_order_id =  v_voided_order_id 
	                   OR c501_parent_order_id =  v_voided_order_id
	                  AND c501_void_fl IS NOT NULL) order_total
	          WHERE T501.C501_PARENT_ORDER_ID IN (  p_order_id )
	            AND T501.C501_ORDER_ID         = T907.C907_REF_ID
	            AND t907.c901_type             = '90217'
	            AND t704.c704_account_id = t501.c704_account_id
	            AND t901.c901_code_id = T907.C901_CANCEL_CD
	            AND t101.c101_user_id = T907.C907_CREATED_BY
	            AND T501.C501_VOID_FL = 'Y'
	            AND t704.C704_VOID_FL IS  NULL
	       ORDER BY T907.C907_CREATED_DATE DESC
	    )
	  WHERE rank = 1;
	
	END gm_fch_rej_order_info;
	
	/*******************************************************
	* Purpose: This procedure will get the account attribute for the passed input
	* account id and based on the attributes defined in the RULES table.
	*******************************************************/
	PROCEDURE gm_sav_sync_acc_attribute (
	        p_trans_id   IN VARCHAR2,
	        p_acc_id     IN T704_account.c704_account_id%TYPE,
	        p_trans_type IN VARCHAR2,
	        p_user_id    IN NUMBER)
	AS
	    v_value    VARCHAR2 (200) ;
	    v_inputstr VARCHAR2 (1000) := NULL;
	BEGIN
	    --The below procedure is called to get the input string with attribute details.
	    gm_sync_attribute_details (p_trans_id, p_acc_id, v_inputstr) ;
	    --
	    IF v_inputstr IS NOT NULL
	    THEN
		    IF p_trans_type = 'ORDER' THEN
		         DELETE
		           FROM t501a_order_attribute
		          WHERE c501_order_id        = p_trans_id
		            AND c901_attribute_type IN
		            (
		                 SELECT token FROM v_in_list
		            ) ;
		        --    
		        gm_pkg_op_order_master.gm_sav_sync_order_attribute (p_trans_id, v_inputstr, p_user_id) ;
		    ELSIF p_trans_type = 'CONS' THEN
		         DELETE
		           FROM t504a_cons_attribute
		          WHERE c504_consignment_id  = p_trans_id
		            AND c901_attribute_type IN
		            (
		                 SELECT token FROM v_in_list
		            ) ;
		        --    
		        gm_pkg_op_consignment.gm_sav_consign_attribute_batch (p_trans_id, p_user_id, v_inputstr) ;
		    END IF;
		    --
	    --
	    END IF; -- v_inputstr
	    --
	END gm_sav_sync_acc_attribute;
	/*******************************************************
	* Purpose: This procedure will Sync the order attribute for all the
	* child order or back order  orders.
	*******************************************************/
	PROCEDURE gm_sav_sync_order_attribute (
	        p_trans_id IN VARCHAR2,
	        p_str      IN VARCHAR2,
	        p_user_id  IN NUMBER)
	AS
	    v_value    VARCHAR2 (200) ;
	    v_inputstr VARCHAR2 (2000) := NULL;
	    CURSOR cur_order_attrib
	    IS
	         SELECT C901_ATTRIBUTE_TYPE atrib_type, C501A_ATTRIBUTE_VALUE atrib_val
	           FROM t501a_order_attribute
	          WHERE C501_ORDER_ID = p_trans_id;
	    CURSOR order_cur
	    IS
	         SELECT c501_order_id order_id
	           FROM T501_ORDER
	          WHERE C501_STATUS_FL              = 0
	            AND NVL (c901_order_type, - 1) IN (2525, 102364)
	            AND --For stock transfer order back order type is 102364.
	            c501_parent_order_id = p_trans_id
	            AND C501_VOID_FL    IS NULL;
	BEGIN
	    FOR atrib IN cur_order_attrib
	    LOOP
	        v_inputstr := v_inputstr || '^' || atrib.atrib_type|| '^'|| atrib.atrib_val ||'|';
	    END LOOP;
	    --
	    gm_pkg_ac_order.gm_sav_rev_sampling (p_trans_id, p_user_id, p_str) ;
	END gm_sav_sync_order_attribute;
	/***********************************************************************
	* Purpose: This procedure will get the attribute id's defined in rules,
	*          and for that attribute id's it will return the input string.
	***********************************************************************/
	PROCEDURE gm_sync_attribute_details (
	        p_trans_id IN VARCHAR2,
	        p_acc_id   IN T704_account.c704_account_id%TYPE,
	        p_inputstr OUT VARCHAR2)
	AS
	    v_value    VARCHAR2 (200)  := NULL;
	    v_inputstr VARCHAR2 (4000) := NULL;
	    v_company_id    t1900_company.c1900_company_id%TYPE;
	    CURSOR cur_acc_atrib_val
	    IS
	         SELECT C901_ATTRIBUTE_TYPE atrib_type, C704A_ATTRIBUTE_VALUE atrib_val
	           FROM T704A_ACCOUNT_ATTRIBUTE
	          WHERE c704_account_id      = p_acc_id
	            AND C901_ATTRIBUTE_TYPE IN
	            (
	                 SELECT token FROM v_in_list
	            ) ;
	    CURSOR cur_vat_atrib_val
	    IS
	         SELECT C901_CODE_ID atrib_type, C901_CODE_NM atrib_val
	           FROM T901_CODE_LOOKUP
	          WHERE C901_CODE_id IN
	            (
	                 SELECT token FROM v_in_list
	            )
	        AND C901_ACTIVE_FL = '1';
	BEGIN
			  --Getting company id from context.   
    SELECT get_compid_frm_cntx()
	  INTO v_company_id 
	  FROM dual;
	    --Below is to get the input string Account attribute deatils to sync with order.
	     SELECT get_rule_value_by_company ('ACC_ATTRIB', 'SYNC_ACC_ATTRIB', v_company_id)
	       INTO v_value
	       FROM dual;
	    --   
	    my_context.set_my_inlist_ctx (v_value) ;
	    --
	    FOR atrib IN cur_acc_atrib_val
	    LOOP
	        v_inputstr := v_inputstr || '^' || atrib.atrib_type|| '^'|| atrib.atrib_val ||'|';
	    END LOOP;
	    --
	    --Below is to get the input string for VAT attribute deatils to sync with order.
	     SELECT get_rule_value_by_company ('VAT_ATTRIB', 'SYNC_ACC_ATTRIB', v_company_id)
	       INTO v_value
	       FROM dual;
	    --   
	    my_context.set_my_inlist_ctx (v_value) ;
	    --
	    FOR atrib IN cur_vat_atrib_val
	    LOOP
	        v_inputstr := v_inputstr || atrib.atrib_type|| '^'|| atrib.atrib_val ||'|';
	    END LOOP;
	    --
	    p_inputstr := v_inputstr;
	END gm_sync_attribute_details;
	
END gm_pkg_op_order_master;
/
