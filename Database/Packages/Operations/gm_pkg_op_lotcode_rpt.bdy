/* Formatted on 2011/04/28 10:36 (Formatter Plus v4.8.0) */
--@"c:\database\packages\operations\gm_pkg_op_lotcode_rpt.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_lotcode_rpt
IS
--
  /*******************************************************
    * Description : Procedure to get the donor information
    *******************************************************/
   PROCEDURE gm_fch_donor_info (
		p_lotnum 		 IN 	  t408a_dhr_control_number.c408a_conrol_number%TYPE
	  , p_out			 OUT	  TYPES.cursor_type
	)
	AS
	v_donorid		t2550_part_control_number.c2540_donor_id%TYPE;
	v_company_id   	t1900_company.c1900_company_id%TYPE;
	BEGIN
		SELECT  get_compid_frm_cntx() 
			INTO  v_company_id 
			FROM DUAL;  
		v_donorid := get_donor_id_by_lot(p_lotnum); -- To get the donor id
		OPEN p_out
			FOR
				SELECT   
                     t2540.c2540_donor_number donorno 
				      , t2540.c2540_age age 
				      , get_code_name(t2540.c901_sex) sex  
				      , get_code_name(t2540.c901_international_use) internationaluse  
				      , get_code_name(t2540.c901_research) forresearch  
				      , '' dhrstatus
				      , '' vid
				      , '' dhrid
		          FROM t2540_donor_master t2540   
		         WHERE t2540.c2540_donor_id = v_donorid
		           AND t2540.c2540_void_fl IS NULL;
		          -- AND t2540.c1900_company_id = v_company_id;


	END gm_fch_donor_info;
	
	/*******************************************************
   	* Description : Procedure to fetch the product information
   	*******************************************************/
   PROCEDURE gm_fch_product_info (
		p_lotnum 		 IN 	  t408a_dhr_control_number.c408a_conrol_number%TYPE
	  , p_out			 OUT	  TYPES.cursor_type
	)
	AS
	v_date_fmt		t906_rules.c906_rule_value%TYPE;
	v_partnum		t2550_part_control_number.c205_part_number_id%TYPE;
	v_exp_date		VARCHAR2(30);
	v_company_id   	t1900_company.c1900_company_id%TYPE;
   	v_plant_id 	  	t5040_plant_master.c5040_plant_id%TYPE;
	BEGIN
		SELECT  get_compid_frm_cntx() ,get_plantid_frm_cntx()
			INTO  v_company_id,v_plant_id 
			FROM DUAL; 
			
		v_partnum := get_partnum_by_lot(p_lotnum); 
		OPEN p_out
			FOR
				SELECT   v_partnum partnum, t205.c205_part_num_desc pdesc
			     , NVL(t2550.c2550_custom_size, c205d_label_size) psize  
			     , NVL(trim(gm_pkg_op_bulk_dhr_rpt.get_cpc_account_name(c205d_contract_process_client)),'BBA') cpclient			  		     
			  FROM t205_part_number t205
			      , t2550_part_control_number t2550
			      , v205g_part_label_parameter v205g 
			 WHERE t205.c205_part_number_id = NVL(v_partnum, '-999')
			   AND t2550.c2550_control_number(+) = p_lotnum
               AND t205.c205_part_number_id = t2550.c205_part_number_id(+)   
			   AND t205.c205_part_number_id = v205g.c205_part_number_id;
			   --AND t2550.c1900_company_id = v_company_id
   			   --AND t2550.c5040_plant_id  = v_plant_id;		    

	END gm_fch_product_info;

	/*******************************************************
   	* Description : Procedure to fetch the receiving information
   	*******************************************************/
   PROCEDURE gm_fch_receiving_info (
		p_lotnum 		 IN 	  t408a_dhr_control_number.c408a_conrol_number%TYPE
	  , p_out			 OUT	  TYPES.cursor_type
	)
	AS
	v_date_fmt		t906_rules.c906_rule_value%TYPE;
	v_company_id   	t1900_company.c1900_company_id%TYPE;
   	v_plant_id 	  	t5040_plant_master.c5040_plant_id%TYPE;
	BEGIN
		SELECT  get_compid_frm_cntx() ,get_plantid_frm_cntx(),NVL(get_compdtfmt_frm_cntx(),get_rule_value ('DATEFMT', 'DATEFORMAT'))
			INTO  v_company_id,v_plant_id,v_date_fmt
			FROM DUAL;

			OPEN p_out
			FOR
				SELECT t4082.c4081_dhr_bulk_id rsid, t408.c408_dhr_id dhrid, t408.c408_packing_slip packslip, t301.c301_vendor_name vendornm, t301.c301_vendor_id vendorid
        			,TO_CHAR(t408.c408_created_date,v_date_fmt) createddt,  TO_CHAR(t408.c408_verified_ts,v_date_fmt) verifiedts
        			, gm_pkg_op_lotcode_rpt.get_dhr_statusnm(t408.c408_status_fl) STATUS
			  FROM t408_dhr t408, t301_vendor t301
			      , t408a_dhr_control_number t408a 
			      , t4082_bulk_dhr_mapping t4082
			 WHERE t408a.c408a_conrol_number = p_lotnum
			   AND t408.c408_dhr_id = t408a.c408_dhr_id   
               AND t408a.c408_dhr_id = t4082.c408_dhr_id 
               AND t408.c301_vendor_id = t301.c301_vendor_id
			   AND t408.c408_void_fl IS NULL
			   AND t408a.c408a_void_fl IS NULL
			   AND t4082.c4082_void_fl IS NULL ; --Removed company and plant filter for PMT-29684
			 


	END gm_fch_receiving_info;
	
	/*******************************************************
   	* Description : Procedure to fetch the lot information
   	*******************************************************/
   PROCEDURE gm_fch_lot_info (
		p_lotnum 		 IN 	  t408a_dhr_control_number.c408a_conrol_number%TYPE
	  , p_exp_date		 OUT	  VARCHAR2
	  , p_curr_status	 OUT  	  VARCHAR2
	  , p_err_fl		 OUT	  VARCHAR2
	  , p_curr_plant     OUT      VARCHAR2
	)
	AS
    v_status VARCHAR2(50);
    v_err_flag VARCHAR2(10);
	YES		    VARCHAR2(10);
	
	BEGIN
    P_EXP_DATE := GET_EXP_DATE_BY_LOT(P_LOTNUM);
    
    v_status := gm_pkg_op_lotcode_rpt.get_lot_Status(p_lotnum);
    -- Get Lot status from the get_lot_current_status function, if returns null get_lot_Status function where it get fron t2550 
    IF v_status IS NULL OR v_status = 'Controlled' THEN --105008 CONTROLLED
	    SELECT gm_pkg_op_lotcode_rpt.get_lot_current_status(p_lotnum)
	    	INTO p_curr_status FROM DUAL;
    ELSE
    	p_curr_status:=v_status;
    END IF;
    
    BEGIN
    	--  Added Current Plant information for PMT-47547.
    	
    	SELECT DECODE(c2550_error_fl,'Y','YES','') , get_plant_name(t2550.c5040_plant_id)
	        INTO v_err_flag, p_curr_plant
	        FROM t2550_part_control_number t2550
	      	WHERE t2550.c2550_control_number = p_lotnum;	      	
	     p_err_fl := v_err_flag;	       
	    EXCEPTION WHEN NO_DATA_FOUND
	     THEN
	      p_err_fl:=' ';
	      p_curr_plant:='Not Available';
	      END;   

	      
	END gm_fch_lot_info;
	
	/*******************************************************
   	* Description : Procedure to fetch the open transaction
   	*******************************************************/
   PROCEDURE gm_fch_open_txn (
		p_lotnum 		 IN 	  t408a_dhr_control_number.c408a_conrol_number%TYPE
	  , p_out			 OUT	  TYPES.cursor_type
	)
	AS
	v_date_fmt		t906_rules.c906_rule_value%TYPE;
	v_company_id   	t1900_company.c1900_company_id%TYPE;
   	v_plant_id 	  	t5040_plant_master.c5040_plant_id%TYPE;
	BEGIN
		SELECT  get_compid_frm_cntx() ,get_plantid_frm_cntx(),NVL(get_compdtfmt_frm_cntx(),get_rule_value ('DATEFMT', 'DATEFORMAT'))
			INTO  v_company_id,v_plant_id,v_date_fmt
			FROM DUAL;
			
		OPEN p_out
			FOR			 
				 SELECT T506.C506_RMA_ID TXNID, TO_CHAR(NVL (T506.C506_LAST_UPDATED_DATE, T506.C506_CREATED_DATE),v_date_fmt) TXNDATE, 'Returns' TXNTYPE,
				        t507.C507_ITEM_QTY QTY, GET_USER_NAME(NVL(T506.C506_LAST_UPDATED_BY, C506_CREATED_BY)) UPTBY
					, gm_pkg_op_lotcode_rpt.get_returns_status(t506.c506_status_fl) STATUS 
					, '' txntypeid
				   FROM t506_returns t506, t507_returns_item t507
				  WHERE t507.c507_control_number = p_lotnum
				    AND t506.c506_rma_id         = t507.c506_rma_id
				    AND t506.c501_reprocess_date IS NULL
				    AND t506.C506_VOID_FL       IS NULL 
				    AND t506.c506_status_fl IN (1,2)     
				    AND t507.c507_status_fl IN ('C','R') --Removed company and plant filter for PMT-29684
				 UNION ALL    
				 SELECT T412.C412_INHOUSE_TRANS_ID TXNID, TO_CHAR(NVL (T412.C412_LAST_UPDATED_DATE, T412.C412_CREATED_DATE),v_date_fmt) TXNDATE,
				    GET_CODE_NAME (T412.C412_TYPE) TXNTYPE, t413.C413_ITEM_QTY  QTY, GET_USER_NAME(NVL(t413.C413_LAST_UPDATED_BY, t412.C412_LAST_UPDATED_BY)) UPTBY
				, gm_pkg_op_lotcode_rpt.get_inhouse_status(t412.C412_STATUS_FL) STATUS 
				, to_char(T412.C412_TYPE) txntypeid
				   FROM t412_inhouse_transactions t412, t413_inhouse_trans_items t413
				  WHERE t413.c413_control_number   = p_lotnum
				    AND t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
				    AND t412.c412_status_fl != '4'
				    AND t412.c412_void_fl IS NULL
				    AND t413.c413_void_fl IS NULL --Removed company and plant filter for PMT-29684
				 UNION ALL    
				SELECT T501.C501_ORDER_ID TXNID, TO_CHAR(NVL (T501.C501_LAST_UPDATED_DATE, T501.C501_CREATED_DATE),v_date_fmt) TXNDATE, 'Order' TXNTYPE,
				       t502.C502_ITEM_QTY  QTY, GET_USER_NAME(NVL(t501.C501_LAST_UPDATED_BY, t501.C501_LAST_UPDATED_BY)) UPTBY
				,   gm_pkg_op_do_process_report.get_order_status_desc(T501.C501_ORDER_ID) STATUS  
				, '' txntypeid
				   FROM t501_order t501, t502_item_order t502
				  WHERE t501.c501_order_id       = t502.c501_order_id
				    AND T502.C502_CONTROL_NUMBER = p_lotnum
				    AND t501.c501_status_fl      < 3
				    AND t501.c501_void_fl       IS NULL
				    AND t502.c502_void_fl       IS NULL --Removed company and plant filter for PMT-29684
				  UNION ALL   
				   SELECT T504.C504_CONSIGNMENT_ID TXNID, TO_CHAR(NVL (T504.C504_LAST_UPDATED_DATE, T504.C504_CREATED_DATE),v_date_fmt) TXNDATE,
				       DECODE(t504.c504_type, 4110,'Consignment', 'Inhouse') TXNTYPE, t505.C505_ITEM_QTY  QTY, GET_USER_NAME(NVL(t504.C504_LAST_UPDATED_BY, t504.C504_LAST_UPDATED_BY)) UPTBY
				       , get_consignment_status(t504.C504_STATUS_FL, t504.c504_type) STATUS   
				       , to_char(t504.c504_type) txntypeid
				   FROM t504_consignment t504, t505_item_consignment t505
				  WHERE t505.c505_control_number = p_lotnum 
				    AND t504.c504_status_fl     != 4
				    AND t504.c504_consignment_id = t505.c504_consignment_id
				    AND t504.C504_VOID_FL       IS NULL
				    AND t505.C505_VOID_FL       IS NULL; --Removed company and plant filter for PMT-29684
				   
	END gm_fch_open_txn;
	
	/*******************************************************
   	* Description : Procedure to fetch the inventory information
   	*******************************************************/
   PROCEDURE gm_fch_inventory_info (
		p_lotnum 		 IN 	  t408a_dhr_control_number.c408a_conrol_number%TYPE
	  , p_out			 OUT	  TYPES.cursor_type
	)
	AS
	v_company_id   	t1900_company.c1900_company_id%TYPE;
   	v_plant_id 	  	t5040_plant_master.c5040_plant_id%TYPE;
	BEGIN
		SELECT  get_compid_frm_cntx() ,get_plantid_frm_cntx()
			INTO  v_company_id,v_plant_id 
			FROM DUAL; 
			
		OPEN p_out
			FOR
				SELECT GET_CODE_NAME (T5060.C901_WAREHOUSE_TYPE) WAREHOUSE, T5052.C5052_LOCATION_CD LOCATIONCODE
				   FROM T2550_PART_CONTROL_NUMBER T2550, T5060_CONTROL_NUMBER_INV T5060, T5052_LOCATION_MASTER T5052
				  WHERE T2550.C205_PART_NUMBER_ID          = T5060.C205_PART_NUMBER_ID
				    AND T5060.C5060_CONTROL_NUMBER         = T2550.C2550_CONTROL_NUMBER
				    AND T5060.C5060_REF_ID                 = T5052.C5052_LOCATION_CD(+)
				    AND T5052.C5052_VOID_FL(+)            IS NULL
				    AND NVL (T5060.C5060_QTY, '0')         > 0
				    AND T2550.C1900_COMPANY_ID = v_company_id
   			   		AND T2550.C5040_PLANT_ID  = v_plant_id
				    AND UPPER (T2550.C2550_CONTROL_NUMBER) = UPPER (p_lotnum);



	END gm_fch_inventory_info;
	
	/*******************************************************
   	* Description : Procedure to fetch the transaction history
   	*******************************************************/
   PROCEDURE gm_fch_txn_history (
		p_lotnum 		 IN 	  t408a_dhr_control_number.c408a_conrol_number%TYPE
	  , p_out			 OUT	  TYPES.cursor_type
	)
	AS
	v_date_fmt		t906_rules.c906_rule_value%TYPE;
	v_company_id   	t1900_company.c1900_company_id%TYPE;
   	v_plant_id 	  	t5040_plant_master.c5040_plant_id%TYPE;
	BEGIN
		SELECT  get_compid_frm_cntx() ,get_plantid_frm_cntx(),NVL(get_compdtfmt_frm_cntx(),get_rule_value ('DATEFMT', 'DATEFORMAT'))
			INTO  v_company_id,v_plant_id,v_date_fmt
			FROM DUAL;
			
		OPEN p_out
			FOR
			
			SELECT t2551.C2550_LAST_UPDATE_TRANS_ID TRANS_ID,  t2551.C205_PART_NUMBER_ID PNUM,
			    TO_CHAR (t2551.C2551_CREATED_DATE, v_date_fmt || ' HH24:MI:SS') TRANS_DT,
			    GET_CODE_NAME (t2551.C901_LAST_UPDATED_WAREHOUSE_TYPE) INVTYPE_NM, GET_CODE_NAME (t2551.C901_LAST_UPDATED_TRANS_TYPE) TTTYPE_NM,
			   t2551.C901_LAST_UPDATED_TRANS_TYPE TTYPE_ID  
			    , GM_PKG_OP_LOTCODE_RPT.GET_INTXN_TYPE(t2551.C901_LAST_UPDATED_TRANS_TYPE) txntypeid 
			    , gm_pkg_op_lotcode_rpt.get_txn_log_cnt(t2551.C2550_LAST_UPDATE_TRANS_ID) LOGCNT
			    , get_company_name(t2551.c1900_company_id) COMPNAME
                , get_plant_name(t2551.C5040_PLANT_ID) PLANTNAME
			   FROM T205_PART_NUMBER T205, t2551_part_control_number_log t2551
			  WHERE t2551.C205_PART_NUMBER_ID  = T205.C205_PART_NUMBER_ID
			    AND t2551.C2550_CONTROL_NUMBER = p_lotnum
          ORDER BY t2551.C2551_CREATED_DATE  DESC;

	END gm_fch_txn_history;
	
	/****************************************************************
   	* Description : Function to get the part number of lot code
   	*****************************************************************/
	FUNCTION get_partnum_by_lot (
		p_lotnum 		 IN 	  t408a_dhr_control_number.c408a_conrol_number%TYPE
	)
		RETURN t2550_part_control_number.c205_part_number_id%TYPE
	IS
	v_part_num		t2550_part_control_number.c205_part_number_id%TYPE;
	v_company_id   	t1900_company.c1900_company_id%TYPE;
   	v_plant_id 	  	t5040_plant_master.c5040_plant_id%TYPE;
	BEGIN
		SELECT  get_compid_frm_cntx() ,get_plantid_frm_cntx()
			INTO  v_company_id,v_plant_id 
			FROM DUAL; 
			
		SELECT C205_PART_NUMBER_ID INTO v_part_num 
			FROM T2550_PART_CONTROL_NUMBER
		WHERE C2550_CONTROL_NUMBER =  p_lotnum;
		--AND c1900_company_id = v_company_id
   		--AND c5040_plant_id  = v_plant_id;
		RETURN v_part_num;
        EXCEPTION
        	WHEN NO_DATA_FOUND THEN
        	BEGIN
            	SELECT  t408.C205_PART_NUMBER_ID INTO v_part_num 
		          FROM  t408_dhr t408, t408a_dhr_control_number t408a  
		         WHERE t408a.c408a_conrol_number = p_lotnum
                   AND t408.c408_dhr_id = t408a.c408_dhr_id 
		           AND t408.c408_void_fl IS NULL 
                   AND t408a.c408a_void_fl IS NULL 
                   AND t408.c1900_company_id = v_company_id
   				   AND t408.c5040_plant_id  = v_plant_id;
               RETURN v_part_num;
            EXCEPTION
        	WHEN NO_DATA_FOUND THEN
        		v_part_num := NULL;
        	END;
        RETURN v_part_num;
	END get_partnum_by_lot;
	
	/****************************************************************
   	* Description : Function to get the donor number of lot code
   	*****************************************************************/
	FUNCTION get_Donor_id_by_lot (
		p_lotnum 		 IN 	  t408a_dhr_control_number.c408a_conrol_number%TYPE
	)
		RETURN t2550_part_control_number.c2540_donor_id%TYPE
	IS
	v_donor_id		t2550_part_control_number.c2540_donor_id%TYPE;
	v_company_id   	t1900_company.c1900_company_id%TYPE;
   	v_plant_id 	  	t5040_plant_master.c5040_plant_id%TYPE;
	BEGIN
		SELECT  get_compid_frm_cntx() ,get_plantid_frm_cntx()
			INTO  v_company_id,v_plant_id 
			FROM DUAL;  
			
		SELECT C2540_DONOR_ID INTO v_donor_id
			FROM T2550_PART_CONTROL_NUMBER
		WHERE C2550_CONTROL_NUMBER =  p_lotnum;
		--AND c1900_company_id = v_company_id
   		--AND c5040_plant_id  = v_plant_id;
		RETURN v_donor_id;
        EXCEPTION
        	WHEN NO_DATA_FOUND THEN
        	BEGIN
               SELECT t2540.C2540_DONOR_ID INTO v_donor_id 
		          FROM  t408_dhr t408, t408a_dhr_control_number t408a, t2540_donor_master t2540   
		         WHERE t408a.c408a_conrol_number = p_lotnum
                   AND t408.c408_dhr_id = t408a.c408_dhr_id
			   	   AND t408.c2540_donor_id = t2540.c2540_donor_id
			   	   AND t408.c408_status_fl < 4     
		           AND t408.c408_void_fl IS NULL 
                   AND t408a.c408a_void_fl IS NULL
		           AND t2540.c2540_void_fl IS NULL
		           AND t408.c1900_company_id = v_company_id
   				   AND t408.c5040_plant_id  = v_plant_id;
		       RETURN v_donor_id;
		     EXCEPTION
        	WHEN NO_DATA_FOUND THEN
        		v_donor_id:=NULL;
        	END;
              
        RETURN v_donor_id;
	END get_Donor_id_by_lot;
	
	/****************************************************************
   	* Description : Function to get the expiry date of lot code
   	*****************************************************************/
	FUNCTION get_exp_date_by_lot (
		p_lotnum 		 IN 	  t408a_dhr_control_number.c408a_conrol_number%TYPE
	)
		RETURN VARCHAR2
	IS
	v_exp_date		VARCHAR2(30);
	v_date_fmt		t906_rules.c906_rule_value%TYPE;
	v_company_id   	t1900_company.c1900_company_id%TYPE;
   	v_plant_id 	  	t5040_plant_master.c5040_plant_id%TYPE;
	BEGIN
	SELECT NVL(get_compdtfmt_frm_cntx(),get_rule_value('DATEFMT','DATEFORMAT')), get_compid_frm_cntx(), get_plantid_frm_cntx() 
		INTO v_date_fmt ,v_company_id , v_plant_id
		FROM DUAL;
		
		SELECT TO_CHAR(C2550_EXPIRY_DATE,v_date_fmt) INTO v_exp_date
			FROM T2550_PART_CONTROL_NUMBER
		WHERE C2550_CONTROL_NUMBER =  p_lotnum;
		--AND c1900_company_id = v_company_id
   		--AND c5040_plant_id  = v_plant_id;
		RETURN v_exp_date;
        EXCEPTION
        	WHEN NO_DATA_FOUND THEN
        	BEGIN
               SELECT TO_CHAR(t408.C2550_EXPIRY_DATE,v_date_fmt) INTO v_exp_date
		          FROM  t408_dhr t408, t408a_dhr_control_number t408a, t2540_donor_master t2540   
		         WHERE t408a.c408a_conrol_number = p_lotnum
                   AND t408.c408_dhr_id = t408a.c408_dhr_id
			   	   AND t408.c2540_donor_id = t2540.c2540_donor_id
		           AND t408.c408_void_fl IS NULL 
                   AND t408a.c408a_void_fl IS NULL
		           AND t2540.c2540_void_fl IS NULL
		           AND t408.c1900_company_id = v_company_id
   				   AND t408.c5040_plant_id  = v_plant_id;
		    RETURN v_exp_date;
		 	EXCEPTION
        	WHEN NO_DATA_FOUND THEN
        		v_exp_date:= NULL;
        	END;
              
        RETURN v_exp_date;
	END get_exp_date_by_lot;
	
  
/****************************************************************
* Description : Function to get the current status of lot code
*****************************************************************/
	FUNCTION get_lot_status (
    p_lotnum 		 IN 	  t408a_dhr_control_number.c408a_conrol_number%TYPE
	)
  	RETURN VARCHAR2
	IS
	v_status		VARCHAR2(30);
	v_company_id   	t1900_company.c1900_company_id%TYPE;
   	v_plant_id 	  	t5040_plant_master.c5040_plant_id%TYPE;
	BEGIN
	SELECT  get_compid_frm_cntx() ,get_plantid_frm_cntx()
	 INTO  v_company_id,v_plant_id 
	 FROM DUAL;  
	    BEGIN	  
	      SELECT c901_code_nm 
	        INTO v_status
	        FROM t2550_part_control_number t2550, t901_code_lookup t901 
	      WHERE t2550.c2550_lot_status     = t901.c901_code_id 
	        AND t2550.c2550_control_number = p_lotnum; 
	       -- AND c1900_company_id = v_company_id --Removed company and plant filter for PMT-29864 PBUG-3337
	   		--AND c5040_plant_id  = v_plant_id;
	    EXCEPTION WHEN NO_DATA_FOUND THEN
	      V_STATUS:=NULL;
	    END;                    
    RETURN v_status;  
  END;
  
	/****************************************************************
   	* Description : Function to get the current status of lot code
   	*****************************************************************/
	FUNCTION get_lot_current_status (
		p_lotnum 		 IN 	  t408a_dhr_control_number.c408a_conrol_number%TYPE
	)
		RETURN VARCHAR2
	IS
	v_status		VARCHAR2(30);
	v_company_id   	t1900_company.c1900_company_id%TYPE;
   	v_plant_id 	  	t5040_plant_master.c5040_plant_id%TYPE;
	BEGIN
		SELECT  get_compid_frm_cntx() ,get_plantid_frm_cntx()
			INTO  v_company_id,v_plant_id 
			FROM DUAL;
		BEGIN
			SELECT status INTO v_status FROM (
			SELECT status, transdate
			   FROM
			    (
			         SELECT 'Inventory' status, NVL (t5060.c5060_last_updated_date,
			            CURRENT_DATE) transdate
			           FROM t5060_control_number_inv t5060
			          WHERE t5060.c5060_control_number = p_lotnum
			            AND t5060.c5060_qty            > 0
			            AND t5060.c901_warehouse_type  = 90800
			            AND t5060.c1900_company_id     = v_company_id 
			            AND t5060.c5040_plant_id       = v_plant_id
			      UNION ALL
			         SELECT 'Receiving' status, CURRENT_DATE transdate
			           FROM t408_dhr t408, t408a_dhr_control_number t408a
			          WHERE t408a.c408a_conrol_number = p_lotnum
			            AND t408.c408_dhr_id          = t408a.c408_dhr_id
			            AND t408.c408_status_fl      != 4
			            AND T408.C408_VOID_FL        IS NULL
			            AND T408A.C408A_VOID_FL      IS NULL
			            AND t408.c1900_company_id     = v_company_id 
			            AND t408.c5040_plant_id       = v_plant_id
			      UNION ALL
			         SELECT 'Return' status, t506.c506_created_date transdate
			           FROM t506_returns t506, t507_returns_item t507
			          WHERE t507.c507_control_number  = p_lotnum
			            AND t506.c506_rma_id          = t507.c506_rma_id
			            AND t506.C506_VOID_FL        IS NULL
			            AND t506.c506_status_fl      IN (1, 2)
			            AND t506.c501_reprocess_date IS NULL
			            AND t506.c1900_company_id     = v_company_id 
			            AND t506.c5040_plant_id       = v_plant_id
			      UNION ALL
			         SELECT DECODE(t501.c901_order_type, 26240232,'Sold','Consigned') status, DECODE(t501.c501_status_fl,'3',NVL(t907.c907_last_updated_date,t501.c501_created_date),t501.c501_created_date) transdate
			           FROM t501_order t501, t502_item_order t502, t907_shipping_info t907
			          WHERE t501.c501_order_id                      = t502.c501_order_id
			          	AND t501.c501_order_id                      = t907.c907_ref_id(+)
			            AND T502.C502_CONTROL_NUMBER                = p_lotnum
			            and t501.c501_status_fl                     = 3
			            AND NVL (t501.c901_order_type, - 9999) NOT IN ('2524', '2520', '2522', '101260','2528')
			            AND T501.C501_VOID_FL                      IS NULL
			            AND t502.c502_void_fl                      IS NULL
			            AND t907.c907_void_fl(+)                   IS NULL
			            AND t501.c1900_company_id    				= t907.c1900_company_id(+)
                        AND t501.c5040_plant_id      				= t907.c5040_plant_id(+)
			            AND t501.c1900_company_id                   = v_company_id 
			            AND t501.c5040_plant_id                     = v_plant_id
			      UNION ALL
			         SELECT DECODE (t504.c504_type, '4110', DECODE (t504.c504_status_fl, 4, 'Consigned', 'InHouse'), '4115', DECODE
			            (t504.c504_status_fl, 4, 'Scrap', 'InHouse'), 'InHouse') status,
			            DECODE (t504.c504_status_fl, 4, NVL(t907.c907_last_updated_date,t504.c504_created_date) , t504.c504_created_date) transdate
			           FROM t504_consignment t504, t505_item_consignment t505, t907_shipping_info t907
			          WHERE t505.c505_control_number = p_lotnum
			            AND t504.c504_consignment_id = t505.c504_consignment_id
			            AND t504.c504_consignment_id = t907.c907_ref_id(+)
			            AND T504.C504_VOID_FL       IS NULL
			            AND t505.C505_VOID_FL       IS NULL
			            AND t907.c907_void_fl(+)    IS NULL
                        AND t504.c1900_company_id    = t907.c1900_company_id(+)
                        AND t504.c5040_plant_id      = t907.c5040_plant_id(+)
			            AND t504.c1900_company_id    = v_company_id 
			            AND t504.c5040_plant_id      = v_plant_id
			      UNION ALL
			         SELECT DECODE (t412.c412_type, 400065, 'Adjustment', 400084, 'Adjustment', 400077, 'Adjustment', 'InHouse')
			            status, t412.c412_created_date transdate
			           FROM t412_inhouse_transactions t412, t413_inhouse_trans_items t413
			          WHERE t413.c413_control_number   = p_lotnum
			            AND t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
			            AND T412.C412_VOID_FL         IS NULL
			            AND T413.C413_VOID_FL         IS NULL
			            AND t412.c1900_company_id      = v_company_id 
			            AND t412.c5040_plant_id        = v_plant_id
				 UNION ALL
					 SELECT 'Not Available' status,  SYSDATE+10 transdate
					 FROM dual
					 WHERE 0 =  (SELECT COUNT(1) 
					               FROM t2550_part_control_number t2550 
					              WHERE t2550.c2550_control_number = p_lotnum
					                --AND t2550.c1900_company_id = v_company_id
					                --AND t2550.c5040_plant_id = v_plant_id
								)					
			    )
			ORDER BY transdate DESC)
			WHERE ROWNUM = 1;
			
		 EXCEPTION WHEN NO_DATA_FOUND THEN
	      v_status:= NULL;	 
	    END;
	    
    	RETURN v_status;
	END get_lot_current_status;
	
	/****************************************************************
   	* Description : Function to get the status name of DHR id
   	*****************************************************************/
	FUNCTION get_dhr_statusnm (
		p_status		 IN 	  t408_dhr.c408_status_fl%TYPE
	)
		RETURN VARCHAR2
	IS
	v_status		VARCHAR2(30);
	BEGIN
		SELECT DECODE(p_status, '1', 'Pending Inspection', '2', 'Pending Process','3', 'Pending Verify','4', 'Verified') 
			INTO v_status
		FROM dual;
		
		RETURN  v_status;
		
	END get_dhr_statusnm;
	
	/****************************************************************
   	* Description : Function to get the status name of returns item
   	*****************************************************************/
	FUNCTION get_returns_status (
		p_status		 IN 	  t507_returns_item.c507_status_fl%TYPE
	)
		RETURN VARCHAR2
	IS
	v_status		VARCHAR2(30);
	BEGIN
		SELECT DECODE(p_status,  '1', 'Pending Credit','2', 'Pending Reprocess')
			INTO v_status
		FROM dual;
		
		RETURN  v_status;
		
	END get_returns_status;
	
	/****************************************************************
   	* Description : Function to get the status name of inhouse transaction
   	*****************************************************************/
	FUNCTION get_inhouse_status (
		p_status		 IN 	  t412_inhouse_transactions.c412_status_fl%TYPE
	)
		RETURN VARCHAR2
	IS
	v_status		VARCHAR2(30);
	BEGIN
		SELECT DECODE(p_status, '3','Pending Verification', '4', 'Verified')
			INTO v_status
		FROM dual;
		
		RETURN  v_status;
		
	END get_inhouse_status;
	
	/****************************************************************
   	* Description : Function to get the status name of consignment id
   	*****************************************************************/
	FUNCTION get_consignment_status (
		p_status		 IN 	  t504_consignment.c504_status_fl%TYPE
		, p_type		 IN		  t504_consignment.c504_type%TYPE
	)
		RETURN VARCHAR2
	IS
	v_status		VARCHAR2(30);
	BEGIN
		SELECT DECODE(p_status, '3', DECODE(p_type, 4110, 'Pending Shipping', 'Pending Verification'), '4', DECODE(p_type, 4110,'Shipped', 'Verified'))
			INTO v_status
		FROM dual;
		
		RETURN  v_status;
		
	END get_consignment_status;
	
	/****************************************************************
   	* Description : Function to get the inhouse transaction type
   	*****************************************************************/
	FUNCTION get_intxn_type (
		p_txntype		 IN 	  VARCHAR2
	)
		RETURN VARCHAR2
	IS
	v_status		VARCHAR2(30);
	BEGIN
		/* 120601: DHR to Repack; 104621: DHR to Repack; 120602: DHR to Finished Goods; 104620: DHR to Finished Goods; 120603: DHR to Quarantine;
		 104622: Qty to Quarantine; 120433: Returns To Quarantine; 400064: Returns Hold to Quarantine; 120432: Returns To Repackage; 400058: Returns Hold to Repack
		 4313: Quarantine; 4115: Quarantine To Scrap; 120616: Repack To Quarantine; 400079: Repack To Quarantine; 120620:Quarantine to Repack; 400083:Quarantine to Repack 
		 120488: Repackage to Shelf, 400063: Repack to Shelf*/
		SELECT DECODE(p_txntype, '120601','104621', '120602', '104620', '120603', '104622', '120433', '400064', '120432', '400058', '120616', '400079', '120620', '400083', '120488', '400063', '4313', '4115', '4311', '4110',DECODE(p_txntype,'50982','',p_txntype))
			INTO v_status
		FROM dual;
		
		RETURN  v_status;
		
	END get_intxn_type;
	
	/*****************************************************************************************
   	* Description : Procedure to fetch the log details of the transactions to Lot code report
   	******************************************************************************************/
	PROCEDURE gm_fch_txn_log_info (
		p_txnid 		 IN 	  t902_log.c902_ref_id%TYPE
	  , p_out			 OUT	  TYPES.cursor_type
	)
	AS
		v_req_id	t504_consignment.c520_request_id%TYPE;
		v_date_fmt	VARCHAR2 (20);
	BEGIN
		SELECT get_compdtfmt_frm_cntx() 
		INTO v_date_fmt
		FROM DUAL;
		-- Get the request id of consignment id to get the comment of request id
		BEGIN
			SELECT c520_request_id
				INTO v_req_id
			FROM t504_consignment
			WHERE c504_consignment_id = p_txnid
			AND c504_void_fl         IS NULL;
			EXCEPTION WHEN NO_DATA_FOUND
			THEN
				v_req_id := '';
		END;
		-- Get the comments by joining different tables
		OPEN p_out FOR
			SELECT * FROM(
				SELECT get_user_name(c412_created_by) UNAME,
				  c412_comments COMMENTS,
				  c412_inhouse_trans_id ID,
				  TO_CHAR(c412_created_date, v_date_fmt
				  || ':HH:MI:SS') DT
				FROM t412_inhouse_transactions
				WHERE c412_inhouse_trans_id = p_txnid
				AND c412_comments          IS NOT NULL
				UNION
				SELECT get_user_name(c504_created_by) UNAME,
				  c504_comments COMMENTS,
				  c504_consignment_id ID,
				  TO_CHAR(c504_created_date,v_date_fmt
				  || ':HH:MI:SS') DT
				FROM t504_consignment
				WHERE c504_consignment_id = p_txnid
				AND c504_comments        IS NOT NULL
				UNION
				SELECT get_user_name(c902_created_by) UNAME,
				  c902_comments COMMENTS,
				  c902_ref_id ID,
				  TO_CHAR(c902_created_date,v_date_fmt
				  || ':HH:MI:SS') DT
				FROM t902_log
				WHERE (c902_ref_id = p_txnid
				OR c902_ref_id     = v_req_id)
				AND c902_comments IS NOT NULL
				UNION
				SELECT get_user_name(c408_created_by) UNAME,
				  c408_comments COMMENTS,
				  c408_dhr_id ID,
				  TO_CHAR(c408_created_date,v_date_fmt
				  || ':HH:MI:SS') DT
				FROM t408_dhr
				WHERE c408_dhr_id  = p_txnid
				AND c408_comments IS NOT NULL
			)
			ORDER BY DT DESC;
		
	END gm_fch_txn_log_info;
	
	/********************************************************
   	* Description : Function to Get the count of log details 
   	*********************************************************/
	FUNCTION get_txn_log_cnt (
		p_txnid 		 IN 	  t902_log.c902_ref_id%TYPE
	)
	RETURN NUMBER
	IS
		v_cnt	NUMBER;
		v_req_id	t504_consignment.c520_request_id%TYPE;
		v_date_fmt	VARCHAR2 (20);
	BEGIN
		
		SELECT get_compdtfmt_frm_cntx() 
		INTO v_date_fmt
		FROM DUAL;
		-- Get the request id of consignment id to get the comment of request id
		BEGIN
			SELECT c520_request_id
				INTO v_req_id
			FROM t504_consignment
			WHERE c504_consignment_id = p_txnid
			AND c504_void_fl         IS NULL;
			EXCEPTION WHEN NO_DATA_FOUND
			THEN
				v_req_id := '';
		END;
		-- Get the count of comments by joining different tables
		
		SELECT max(cnt) INTO v_cnt FROM(
			SELECT count(1) cnt
			FROM t412_inhouse_transactions
			WHERE c412_inhouse_trans_id = p_txnid
			AND c412_comments          IS NOT NULL
			UNION
			SELECT count(1) cnt
			FROM t504_consignment
			WHERE c504_consignment_id = p_txnid
			AND c504_comments        IS NOT NULL
			UNION
			SELECT count(1) cnt
			FROM t902_log
			WHERE (c902_ref_id = p_txnid
			OR c902_ref_id     = v_req_id)
			AND c902_comments IS NOT NULL
			UNION
			SELECT count(1) cnt
			FROM t408_dhr
			WHERE c408_dhr_id  = p_txnid
			AND c408_comments IS NOT NULL
		);
			
		RETURN v_cnt;
		
	END get_txn_log_cnt;
END gm_pkg_op_lotcode_rpt;
/