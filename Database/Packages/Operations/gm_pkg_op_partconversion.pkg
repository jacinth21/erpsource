/* Formatted on 2008/02/29 14:10 (Formatter Plus v4.8.8) */
-- @"c:\database\packages\operations\gm_pkg_op_partconversion.pkg"

CREATE OR REPLACE PACKAGE gm_pkg_op_partconversion
IS
--
/*******************************************************
     * Description : Procedure to fetch details about the part which has to be transformed
     * Author   : Joe P Kumar
 *******************************************************/
    PROCEDURE gm_fch_frompartdetails (
        p_partnum      IN       t205_part_number.c205_part_number_id%TYPE
      , p_invtype      IN       t901_code_lookup.c901_code_id%TYPE
      , p_outdetails   OUT      TYPES.cursor_type
    );

/*******************************************************
     * Description : Procedure to fetch details about the part to which p_partnum has to be converted to
     * Author   : Joe P Kumar
 *******************************************************/
    PROCEDURE gm_fch_topartdetails (
        p_partnum      IN       t205_part_number.c205_part_number_id%TYPE
      , p_invtype      IN       t901_code_lookup.c901_code_id%TYPE
      , p_outdetails   OUT      TYPES.cursor_type
    );

/*******************************************************
    * Description : Procedure to save part conversion details
    * Author   : Joe P Kumar
    * Algorithm:
           1. validations - p_convqty < get_qty_in_stock(p_partnum)(type is FG) or the new function (if type is RM)
           2. Insert 'from pnum' data into T217
           3. Decrease part qty in 205c or 205 depending on the type
               If 205, then regular update stmnt. Else gm_cm_sav_partqty
           4. call posting - gn_Save_ledger_posting
           5. Insert input string (will contain data as to_pnum^to_qty^to_cost^to_type|)data into t218
           6. Increase part qty in  205c or 205 depending on the type
               If 205, then regular update stmnt. Else gm_cm_sav_partqty
           7. call posting - gn_Save_ledger_posting (in loop)
           8. calculate the difference and post it to WIP change

*******************************************************/
    PROCEDURE gm_sav_topartdetails (
        p_partnum        IN       t205_part_number.c205_part_number_id%TYPE
      , p_partconvdesc   IN       t217_part_conversion.c217_comments%TYPE
      , p_inputstr       IN       VARCHAR2
      , p_convqty        IN       t218_part_conversion_detail.c218_conversion_qty%TYPE
      , p_totalprice     IN       t218_part_conversion_detail.c218_conversion_cost%TYPE
      , p_invtype        IN       t901_code_lookup.c901_code_id%TYPE
      , p_userid         IN       t217_part_conversion.c217_created_by%TYPE
      , p_outpcid        OUT      t217_part_conversion.c217_part_conversion_id%TYPE
    );

/*******************************************************
     * Description : Procedure to fetch part conversion
     * Author   : Joe P Kumar
 *******************************************************/
    PROCEDURE gm_ac_fch_pcdetails (
        p_pcid          IN       t217_part_conversion.c217_part_conversion_id%TYPE
      , p_outpcheader   OUT      TYPES.cursor_type
      , p_outpcdetail   OUT      TYPES.cursor_type
    );
END gm_pkg_op_partconversion;
/