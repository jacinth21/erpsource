/* Formatted on 2010/03/01 11:31 (Formatter Plus v4.8.0) */
--@"c:\database\packages\operations\gm_pkg_op_ld_demand.bdy"
-- show err  Package Body gm_pkg_op_ld_demand
-- exec gm_pkg_op_ld_demand.gm_op_ld_demand_main(15);

/*
 DELETE FROM T4042_DEMAND_SHEET_DETAIL WHERE c4040_demand_sheet_id IN (SELECT c4040_demand_sheet_id FROM t4040_demand_sheet
					WHERE	c4020_demand_master_id = 18)  ;
 DELETE FROM T4041_DEMAND_SHEET_MAPPING WHERE c4040_demand_sheet_id IN (SELECT c4040_demand_sheet_id FROM t4040_demand_sheet
					WHERE	c4020_demand_master_id = 18)  ;
 DELETE FROM T4040_DEMAND_SHEET WHERE  c4020_demand_master_id = 18;
 COMMIT ;
 exec gm_pkg_op_ld_demand.gm_op_ld_demand_main(18);
*/

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_ld_demand
IS
	/*******************************************************
	* Purpose: This procedure will be main procedure to
	*			load sheet information
	* Accepts demand id and loads demand  and forecast information
	*******************************************************/
	PROCEDURE gm_op_ld_demand_main (
		p_demand_id 		t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_request_by   IN	t4020_demand_master.c4020_created_by%TYPE DEFAULT '30301'
	  , p_load_type    IN   t250_inventory_lock.c901_lock_type%TYPE DEFAULT NULL
	)
	AS
		--
		v_demand_type  t4020_demand_master.c901_demand_type%TYPE;
		v_demand_period t4020_demand_master.c4020_demand_period%TYPE;
		v_forecast_period t4020_demand_master.c4020_forecast_period%TYPE;
		v_inc_current_fl t4020_demand_master.c4020_include_current_fl%TYPE;
		v_demand_sheet_id t4040_demand_sheet.c4040_demand_sheet_id%TYPE;
		v_demand_nm    t4020_demand_master.c4020_demand_nm%TYPE;
		v_primary_user t4020_demand_master.c4020_primary_user%TYPE;
		v_request_period t4020_demand_master.c4020_request_period%TYPE;
		v_inventory_id t250_inventory_lock.c250_inventory_lock_id%TYPE;
		--
		v_start_dt	   DATE;
		v_from_period  DATE;
		v_to_period    DATE;
		--
	--
	BEGIN
		
		
		SELECT	   c901_demand_type, c4020_demand_nm, c4020_demand_period, c4020_forecast_period
				 , c4020_include_current_fl, c4020_primary_user, c4020_request_period
			  INTO v_demand_type, v_demand_nm, v_demand_period, v_forecast_period
				 , v_inc_current_fl, v_primary_user, v_request_period
			  FROM t4020_demand_master
			 WHERE c4020_demand_master_id = p_demand_id
		FOR UPDATE;

		--
		SELECT s4040_demand_sheet.NEXTVAL
		  INTO v_demand_sheet_id
		  FROM DUAL;

			IF  p_load_type IS NULL
			THEN
		-- Below code to fetch forecast (from and to period)
		gm_pkg_op_ld_demand.gm_op_calc_period (v_forecast_period, v_inc_current_fl, 'F', v_from_period, v_to_period);
			ELSE
				SELECT TRUNC(current_date) INTO v_from_period FROM DUAL;
			END IF;
		--
		-- Below code to get current inventory value
		SELECT MAX (c250_inventory_lock_id)
		  INTO v_inventory_id
		  FROM t250_inventory_lock
			 WHERE c901_lock_type = NVL(p_load_type,20430);
	
		--
		-- Below procedure will load demand sheet information
		gm_pkg_op_ld_demand.gm_op_sav_sheet_main (p_demand_id
												, v_demand_sheet_id
												, v_demand_type
												, v_demand_period
												, v_forecast_period
												, v_inc_current_fl
												, v_from_period
												, v_primary_user
												, v_request_period
												, v_inventory_id
												 );
		--Calulate the Sales Demand and load it to the Demand Sheet Detail Table.
		IF p_load_type IS NOT NULL AND p_load_type = '4000564'
		THEN
			BEGIN
				gm_pkg_op_ld_sales_demand.gm_cp_ld_main(p_demand_id
														, v_demand_sheet_id
														, v_from_period);
														
				gm_pkg_op_ld_demand.gm_op_send_email (p_demand_id
													, v_demand_nm
													, '01/' || TO_CHAR (v_start_dt, 'MM/YYYY')
													, v_start_dt
													, 'S'
													, 'SUCCESS'
													, p_request_by
													 );														
				COMMIT;
				EXCEPTION
					WHEN OTHERS
					THEN
						ROLLBACK;
						-- Final log insert statement (Error Message)
						gm_pkg_op_ld_demand.gm_op_send_email (p_demand_id
															, v_demand_nm
															, TO_CHAR (v_start_dt, 'MM') || '/01/' || TO_CHAR (v_start_dt, 'YYYY')
															, v_start_dt
															, 'E'
															, SQLERRM
															, p_request_by
															 );
						COMMIT;		
			END;
		ELSE
			BEGIN
				v_start_dt	:= current_date;
		
				--
				
		-- Below code to load demand mapping information
		gm_pkg_op_ld_demand.gm_op_sav_sheet_mapping (p_demand_id, v_demand_sheet_id);
		-- Below code to save growth information
		gm_pkg_op_ld_demand.gm_op_sav_sheet_growth (p_demand_id, v_demand_sheet_id, v_from_period, v_to_period);

		--
		-- based on the type will redirect to sales or consignment
		-- 40020 maps to sales consignment
		-- 40021 maps to inhouse consignment
		IF (v_demand_type = 40020)
		THEN
			--
			gm_pkg_op_ld_sales_demand.gm_op_ld_main (p_demand_id
												   , v_demand_sheet_id
												   , v_demand_period
												   , v_forecast_period
												   , v_inc_current_fl
													);
		--DBMS_OUTPUT.put_line ('Sales Demand Completed ****');
		ELSE
			-- Sales and Inhouse consignment
			gm_pkg_op_ld_consign_demand.gm_op_ld_main (p_demand_id
													 , v_demand_sheet_id
													 , v_demand_type
													 , v_demand_period
													 , v_forecast_period
													 , v_inc_current_fl
													 , v_inventory_id
													  );
		END IF;

		gm_pkg_op_ld_demand.gm_op_ld_sub_components (v_demand_sheet_id);
		gm_pkg_op_ld_demand.gm_op_send_email (p_demand_id
											, v_demand_nm
											, '01/' || TO_CHAR (v_start_dt, 'MM/YYYY')
											, v_start_dt
											, 'S'
											, 'SUCCESS'
											, p_request_by
											 );
		COMMIT;
	EXCEPTION
		WHEN OTHERS
		THEN
			ROLLBACK;
			-- Final log insert statement (Error Message)
			gm_pkg_op_ld_demand.gm_op_send_email (p_demand_id
												, v_demand_nm
												, TO_CHAR (v_start_dt, 'MM') || '/01/' || TO_CHAR (v_start_dt, 'YYYY')
												, v_start_dt
												, 'E'
												, SQLERRM
												, p_request_by
												 );
			COMMIT;
	--
			END;
		END IF;
	END gm_op_ld_demand_main;

--
	/***********************************************************
	* Purpose: Procedure used to save demand sheet information
	************************************************************/
	PROCEDURE gm_op_sav_sheet_main (
		p_demand_id 		IN	 t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_demand_sheet_id	IN	 t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	  , p_demand_type		IN	 t4020_demand_master.c901_demand_type%TYPE
	  , p_demand_period 	IN	 t4020_demand_master.c4020_demand_period%TYPE
	  , p_forecast_period	IN	 t4020_demand_master.c4020_forecast_period%TYPE
	  , p_inc_current_fl	IN	 t4020_demand_master.c4020_include_current_fl%TYPE
	  , p_from_period		IN	 DATE
	  , p_primary_user		IN	 t4020_demand_master.c4020_primary_user%TYPE
	  , p_request_period	IN	 t4020_demand_master.c4020_request_period%TYPE
	  , p_inventory_id		IN	 t250_inventory_lock.c250_inventory_lock_id%TYPE
	)
	AS
		--
		v_count 	   NUMBER;
		v_status	   t4040_demand_sheet.c901_status%TYPE;
	BEGIN
		--
		BEGIN
			--
			-- Check if record already found for selected period if yes delete the same
			SELECT c901_status
			  INTO v_status
			  FROM t4040_demand_sheet
			 WHERE c4020_demand_master_id = p_demand_id AND c4040_demand_period_dt = p_from_period AND ROWNUM = 1;

			IF (v_status <> 50550)
			THEN
				-- Error message is  Set Consignment is not in Shipped Status and cannot be rolled back.
				GM_RAISE_APPLICATION_ERROR('-20999','233','');
			ELSE
				DELETE FROM t4042_demand_sheet_detail
					  WHERE c4040_demand_sheet_id IN (
								  SELECT c4040_demand_sheet_id
									FROM t4040_demand_sheet
								   WHERE c4020_demand_master_id = p_demand_id
										 AND c4040_demand_period_dt = p_from_period);

				DELETE FROM t4041_demand_sheet_mapping
					  WHERE c4040_demand_sheet_id IN (
								  SELECT c4040_demand_sheet_id
									FROM t4040_demand_sheet
								   WHERE c4020_demand_master_id = p_demand_id
										 AND c4040_demand_period_dt = p_from_period);

				DELETE FROM t4043_demand_sheet_growth
					  WHERE c4040_demand_sheet_id IN (
								  SELECT c4040_demand_sheet_id
									FROM t4040_demand_sheet
								   WHERE c4020_demand_master_id = p_demand_id
										 AND c4040_demand_period_dt = p_from_period);

				DELETE FROM t4044_demand_sheet_request
					  WHERE c4040_demand_sheet_id IN (
								  SELECT c4040_demand_sheet_id
									FROM t4040_demand_sheet
								   WHERE c4020_demand_master_id = p_demand_id
										 AND c4040_demand_period_dt = p_from_period);

				DELETE FROM t4040_demand_sheet
					  WHERE c4020_demand_master_id = p_demand_id AND c4040_demand_period_dt = p_from_period;
			END IF;
		--
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				NULL;
		END;

		INSERT INTO t4040_demand_sheet
					(c4040_demand_sheet_id, c4020_demand_master_id, c4040_load_dt, c4040_demand_period
				   , c4040_forecast_period, c901_status, c4040_demand_period_dt, c4040_primary_user, c901_demand_type
				   , c4040_request_period, c250_inventory_lock_id
					)
			 VALUES (p_demand_sheet_id, p_demand_id, TRUNC (SYSDATE), p_demand_period
				   , p_forecast_period, 50550, p_from_period, p_primary_user, p_demand_type
				   , p_request_period, p_inventory_id
					);
	END gm_op_sav_sheet_main;

--
	/***********************************************************
	* Purpose: Procedure used to save demand sheet information
	************************************************************/
	PROCEDURE gm_op_sav_sheet_mapping (
		p_demand_id 		IN	 t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_demand_sheet_id	IN	 t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	)
	AS
	--
	BEGIN
		--
		INSERT INTO t4041_demand_sheet_mapping
					(c4041_demand_sheet_map_id, c4040_demand_sheet_id, c901_ref_type, c4041_ref_id, c901_action_type)
			SELECT s4041_demand_sheet_mapping.NEXTVAL, p_demand_sheet_id, c901_ref_type, c4021_ref_id
				 , c901_action_type
			  FROM t4021_demand_mapping t4021, t4010_group t4010
			 WHERE t4021.c4020_demand_master_id = p_demand_id
			   AND t4021.c4021_ref_id = (TO_CHAR (t4010.c4010_group_id(+))) 
			   AND t4010.c4010_void_fl IS NULL;
	--
	END gm_op_sav_sheet_mapping;

	/***********************************************************
	* Purpose: Procedure used to save demand sheet growth information
	************************************************************/
	PROCEDURE gm_op_sav_sheet_growth (
		p_demand_id 		IN	 t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_demand_sheet_id	IN	 t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	  , p_from_period		IN	 DATE
	  , p_to_period 		IN	 DATE
	)
	AS
	--
	BEGIN
		--
		INSERT INTO t4043_demand_sheet_growth
					(c4043_demand_sheet_growth_id, c4040_demand_sheet_id, c4030_demand_growth_id, c901_ref_type
				   , c4043_value, c4043_start_date, c4043_end_date)
			SELECT s4043_demand_sheet_growth.NEXTVAL, p_demand_sheet_id, t4030.c4030_demand_growth_id
				 , t4031.c901_ref_type, t4031.c4031_value, t4031.c4031_start_date, t4031.c4031_end_date
			  FROM t4030_demand_growth_mapping t4030, t4031_growth_details t4031, t4010_group t4010
			 WHERE t4030.c4020_demand_master_id = p_demand_id
			   AND t4030.c4030_demand_growth_id = t4031.c4030_demand_growth_id
			   AND t4030.c4030_void_fl IS NULL
			   AND t4031.c4031_start_date >= p_from_period
			   AND t4031.c4031_start_date <= p_to_period
			   AND t4030.c4030_ref_id = (TO_CHAR (t4010.c4010_group_id(+))) 
			   AND t4010.c4010_void_fl IS NULL;
	--
	END gm_op_sav_sheet_growth;

--
	/***********************************************************
	* Purpose: Procedure used to save demand detail information
	************************************************************/
	PROCEDURE gm_op_sav_sheet_detail (
		p_demand_sheet_id	  IN   t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	  , p_ref_id			  IN   t4042_demand_sheet_detail.c4042_ref_id%TYPE
	  , p_type				  IN   t4042_demand_sheet_detail.c901_type%TYPE
	  , p_period			  IN   t4042_demand_sheet_detail.c4042_period%TYPE
	  , p_part_number_id	  IN   t4042_demand_sheet_detail.c205_part_number_id%TYPE
	  , p_qty				  IN   t4042_demand_sheet_detail.c4042_qty%TYPE
	  , p_parent_part_id	  IN   t4042_demand_sheet_detail.c205_part_number_id%TYPE
	  , p_parent_ref_id 	  IN   t4042_demand_sheet_detail.c4042_ref_id%TYPE
	  , p_parent_qty_needed   IN   t4042_demand_sheet_detail.c4042_parent_qty_needed%TYPE
	)
	AS
	--
	BEGIN
		--
		INSERT INTO t4042_demand_sheet_detail
					(c4042_demand_sheet_detail_id, c4040_demand_sheet_id, c4042_ref_id, c901_type, c4042_period
				   , c205_part_number_id, c4042_qty, c4042_updated_by, c4042_updated_date, c205_parent_part_num_id
				   , c4042_parent_ref_id, c4042_parent_qty_needed
					)
			 VALUES (s4042_demand_sheet_detail.NEXTVAL, p_demand_sheet_id, p_ref_id, p_type, p_period
				   , p_part_number_id, ROUND(p_qty,0), 30301, SYSDATE, p_parent_part_id
				   , p_parent_ref_id, ROUND(p_parent_qty_needed,0)
					);
	END gm_op_sav_sheet_detail;

	/*************************************************************************
		* Purpose: Procedure used to load sales demand/forecast for selected period
		* p_type : D - Demand F - Forecast
		*************************************************************************/
	PROCEDURE gm_op_calc_period (
		p_demand_period    IN		t4020_demand_master.c4020_demand_period%TYPE
	  , p_inc_current_fl   IN		t4020_demand_master.c4020_include_current_fl%TYPE
	  , p_type			   IN		CHAR
	  , p_from_period	   OUT		DATE
	  , p_to_period 	   OUT		DATE
	)
	AS
--
	BEGIN
		--
		IF (p_type = 'D')
		THEN
			SELECT TRUNC (LAST_DAY (DECODE (p_inc_current_fl, NULL, ADD_MONTHS (SYSDATE, -1), SYSDATE)))
			  INTO p_to_period
			  FROM DUAL;

			--
			SELECT TRUNC (ADD_MONTHS (p_to_period, (p_demand_period * -1))) + 1
			  INTO p_from_period
			  FROM DUAL;
		ELSE
			SELECT TO_DATE (   '01/'
							|| TO_CHAR ((DECODE (p_inc_current_fl, NULL, SYSDATE, ADD_MONTHS (SYSDATE, +1))), 'MM/YYYY')
						  , 'DD/MM/YYYY'
						   )
			  INTO p_from_period
			  FROM DUAL;

			--
			SELECT LAST_DAY ((ADD_MONTHS (p_from_period, (p_demand_period - 1))))
			  INTO p_to_period
			  FROM DUAL;
		END IF;

		--
		DBMS_OUTPUT.put_line ('From and To Date  ****' || p_from_period || ' - ' || p_to_period || ' - '
							  || p_demand_period
							 );
	END gm_op_calc_period;

	/*************************************************************************
		* Purpose: Procedure used to email
		*************************************************************************/
	PROCEDURE gm_op_send_email (
		p_demand_id    IN	t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_demand_nm    IN	t4020_demand_master.c4020_demand_nm%TYPE
	  , p_period	   IN	VARCHAR2
	  , p_start_dt	   IN	DATE
	  , p_type		   IN	CHAR
	  , p_message	   IN	VARCHAR2
	  , p_request_by   IN	t4020_demand_master.c4020_created_by%TYPE
	)
	AS
		--
		subject 	   VARCHAR2 (1000) := p_demand_nm || ' Order Planning Sheet Loaded Successfully	';
		mail_body	   VARCHAR2 (3000);
		crlf		   VARCHAR2 (2) := CHR (13) || CHR (10);
		p_end_dt	   DATE;
		to_mail 	   t906_rules.c906_rule_value%TYPE;
--
	BEGIN
		--
		DBMS_OUTPUT.put_line ('Inside Email ****');
		p_end_dt	:= SYSDATE;

		--
		-- Final log insert statement
		INSERT INTO t910_load_log_table
					(c910_load_log_id, c910_log_name, c910_state_dt, c910_end_dt, c910_status_details, c910_status_fl
					)
			 VALUES (s910_load_log.NEXTVAL, 'OP' || p_demand_nm, p_start_dt, p_end_dt, p_message, p_type
					);

		--
		DBMS_OUTPUT.put_line ('From and To Date  ****');
		mail_body	:=
			   ' Order Planning Sheet Load Status'
			|| crlf
			|| ' Sheet Name	 :- '
			|| p_demand_nm
			|| crlf
			|| ' Period		 :- '
			|| p_period
			|| crlf
			|| ' START Time	:- '
			|| TO_CHAR (p_start_dt, 'dd/mm/yyyy hh24:mi:ss')
			|| crlf
			|| ' END Time :- '
			|| TO_CHAR (p_end_dt, 'dd/mm/yyyy hh24:mi:ss')
			|| crlf
			|| ' MESSAGE '
			|| p_message;
		--
		DBMS_OUTPUT.put_line ('Before calling  gm_cm_mail_grp');

		--
		IF (p_type = 'E')
		THEN
			--
			subject 	:= p_demand_nm || ' Order Planning Sheet Loaded Error Contact IT';
			to_mail 	:= get_rule_value ('ORDERPLAN', 'EMAIL');
			gm_com_send_email_prc (to_mail, subject, mail_body);
		ELSE
			IF (p_request_by <> '30301')
			THEN
				--
				gm_pkg_cm_hierarchy.gm_cm_mail_grp (90735, p_demand_id, subject, mail_body, p_request_by);
			END IF;
		END IF;
--
	END gm_op_send_email;

	/********************************************************************
	 * Purpose: Procedure used to load sub component and calulate qty
	 ********************************************************************/
	PROCEDURE gm_op_ld_sub_components (
		p_demandsheetid   IN   t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	)
	AS
		v_demand_sheet_id t4040_demand_sheet.c4040_demand_sheet_id%TYPE;
		v_ref_id	   t4042_demand_sheet_detail.c4042_ref_id%TYPE;
		v_type		   t4042_demand_sheet_detail.c901_type%TYPE;
		v_period	   t4042_demand_sheet_detail.c4042_period%TYPE;
		v_partnum	   t4042_demand_sheet_detail.c205_part_number_id%TYPE;
		v_qty		   t4042_demand_sheet_detail.c4042_qty%TYPE;
		v_parent_part_id t4042_demand_sheet_detail.c205_part_number_id%TYPE;
		v_sub_part_id  t4042_demand_sheet_detail.c205_part_number_id%TYPE;
		v_sub_qty	   t4042_demand_sheet_detail.c4042_qty%TYPE;
		v_new_qty	   NUMBER;

		CURSOR cur_parent_details
		IS
			SELECT t4042.c4040_demand_sheet_id dmid, c205_part_number_id partnum, c4042_ref_id refid
				 , c4042_period period, c4042_qty qty, c4042_history_fl historyfl, c901_type parent_type
			  FROM t4042_demand_sheet_detail t4042
			 WHERE t4042.c4040_demand_sheet_id = p_demandsheetid
			   AND t4042.c205_part_number_id IN (
										 SELECT 	t205a.c205_from_part_number_id
											   FROM t205a_part_mapping t205a
										 START WITH t205a.c205_to_part_number_id IN (SELECT t205d.c205_part_number_id
																					   FROM t205d_sub_part_to_order t205d)
										 CONNECT BY PRIOR t205a.c205_from_part_number_id = t205a.c205_to_part_number_id
										 AND t205a.c205a_void_fl is null              	---  added new column c205a_void_fl in t205a used to PMT-50777 - Create BOM and sub-component Mapping	
										   GROUP BY t205a.c205_from_part_number_id)
			   AND t4042.c901_type IN (50563, 50567, 50568, 50569);
		CURSOR cur_sub_details
		IS
			SELECT		 t205a.c205_from_part_number_id parent_part, t205a.c205_to_part_number_id sub_part
					 , (t205a.c205a_qty * NVL (PRIOR t205a.c205a_qty, 1)) qty
				  FROM t205a_part_mapping t205a
				 WHERE t205a.c205_to_part_number_id IN (SELECT t205d.c205_part_number_id
															FROM t205d_sub_part_to_order t205d)
			START WITH t205a.c205_from_part_number_id = v_partnum	--'124.000'
			CONNECT BY PRIOR t205a.c205_to_part_number_id = t205a.c205_from_part_number_id
			AND t205a.c205a_void_fl is null;   ---  added new column c205a_void_fl in t205a used to PMT-50777 - Create BOM and sub-component Mapping	
	BEGIN
		FOR var_parent_detail IN cur_parent_details
		LOOP
			v_new_qty	:= 0;
			v_demand_sheet_id := var_parent_detail.dmid;
			v_ref_id	:= var_parent_detail.refid;
			v_type		:= var_parent_detail.parent_type;
			v_period	:= var_parent_detail.period;
			v_partnum	:= var_parent_detail.partnum;
			v_qty		:= var_parent_detail.qty;

			FOR var_sub_detail IN cur_sub_details
			LOOP
				v_parent_part_id := var_sub_detail.parent_part;
				v_sub_part_id := var_sub_detail.sub_part;
				v_sub_qty	:= var_sub_detail.qty;
				gm_op_sav_sheet_detail (v_demand_sheet_id
									  , 9999999999
									  , v_type
									  , v_period
									  , v_sub_part_id
									  , v_qty * v_sub_qty
									  , v_partnum
									  , v_ref_id
									  , v_sub_qty
									   );
			END LOOP;
		END LOOP;
	END gm_op_ld_sub_components;

	/***********************************************************************
	 *Purpose: Procedure used to execute load
	 ***********************************************************************/
	PROCEDURE gm_op_sav_load_detail (
		p_ref_id	   IN	t9302_execute_load.c9302_ref_id%TYPE
	  , p_ref_type	   IN	t9302_execute_load.c901_ref_type%TYPE
	  , p_request_by   IN	t9302_execute_load.c9302_request_by%TYPE
	)
	AS
		v_count 	   NUMBER;
		v_execute_load_id NUMBER;
	BEGIN
		SELECT COUNT (1)
		  INTO v_count
		  FROM t9302_execute_load t9302
		 WHERE t9302.c9302_ref_id = p_ref_id AND t9302.c901_ref_type = p_ref_type AND t9302.c9302_status IN (10, 20);

		IF (v_count <> 0)
		THEN
			raise_application_error (-20923, '');
		END IF;

		SELECT s9302_execute_load.NEXTVAL
		  INTO v_execute_load_id
		  FROM DUAL;

		INSERT INTO t9302_execute_load
					(c9302_execute_load_id, c9302_ref_id, c901_ref_type, c9302_request_by, c9302_request_date
				   , c9302_status
					)
			 VALUES (v_execute_load_id, p_ref_id, p_ref_type, p_request_by, SYSDATE
				   , 10
					);
	END gm_op_sav_load_detail;

	/**********************************************************************
	*Purpose: Procedure used to execute load details and call "gm_op_ld_demand_main"
	* to load demand
	**********************************************************************/
	PROCEDURE gm_op_exec_job_detail
	AS
		v_ref_id	   t9302_execute_load.c9302_ref_id%TYPE;
		v_execute_load_id t9302_execute_load.c9302_execute_load_id%TYPE;
		v_request_by   t9302_execute_load.c9302_request_by%TYPE;

		CURSOR cur_exec_job_detail
		IS
			SELECT	 c9302_execute_load_id, c9302_ref_id, c901_ref_type, c9302_request_by
				FROM t9302_execute_load t9302
			   WHERE t9302.c9302_status = 10
			ORDER BY c9302_execute_load_id;
	BEGIN
		FOR var_exec_job_detail IN cur_exec_job_detail
		LOOP
			BEGIN
				v_execute_load_id := var_exec_job_detail.c9302_execute_load_id;
				v_ref_id	:= var_exec_job_detail.c9302_ref_id;
				v_request_by := var_exec_job_detail.c9302_request_by;

				UPDATE t9302_execute_load t9302
				   SET t9302.c9302_status = 20
					 , t9302.c9302_job_start_time = SYSDATE
				 WHERE t9302.c9302_execute_load_id = v_execute_load_id;

				COMMIT;
				gm_pkg_op_ld_demand.gm_op_ld_demand_main (v_ref_id, v_request_by);

				UPDATE t9302_execute_load t9302
				   SET t9302.c9302_status = 40
					 , t9302.c9302_job_end_time = SYSDATE
				 WHERE t9302.c9302_execute_load_id = v_execute_load_id;

				COMMIT;
			EXCEPTION
				WHEN OTHERS
				THEN
					UPDATE t9302_execute_load t9302
					   SET t9302.c9302_status = 30
					 WHERE t9302.c9302_execute_load_id = v_execute_load_id;

					COMMIT;
			END;
		END LOOP;
	END gm_op_exec_job_detail;
END gm_pkg_op_ld_demand;
/
