/* Formatted on 2010/05/26 17:50 (Formatter Plus v4.8.0) */

--@"C:\Database\Packages\Operations\gm_pkg_op_sheet.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_sheet
IS
--
	PROCEDURE gm_fc_sav_demsheet (
		p_demandsheetid 	 IN 	  t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_demandsheetname	 IN 	  t4020_demand_master.c4020_demand_nm%TYPE
	  , p_demandperiod		 IN 	  t4020_demand_master.c4020_demand_period%TYPE
	  , p_forecastperiod	 IN 	  t4020_demand_master.c4020_forecast_period%TYPE
	  , p_hierarchyid		 IN 	  t9350_hierarchy.c9350_hierarchy_id%TYPE
	  , p_includecurrfl 	 IN 	  t4020_demand_master.c4020_include_current_fl%TYPE
	  , p_inactiveflag		 IN 	  t4020_demand_master.c4020_inactive_fl%TYPE
	  , p_userid			 IN 	  t4020_demand_master.c4020_created_by%TYPE
	  , p_demandsheetowner	 IN 	  t4020_demand_master.c4020_primary_user%TYPE
	  , p_outdemsheetid 	 OUT	  t4020_demand_master.c4020_demand_master_id%TYPE
	)
	AS
				/* To be removed later.. keeping this as a reference for now - Joe
			demExp Exception;
			code number;
			msg varchar2(512);
			pragma exception_init (demExp, -00001);
			*/
			--
		/*******************************************************
			* Description : Procedure to save demand sheet
			* Author			: Joe P Kumar
			*******************************************************/
		v_cur_owner    VARCHAR2 (50) := NULL;
	BEGIN
		p_outdemsheetid := p_demandsheetid;

		--update the pricning update group for new primary user change/add
		IF (p_demandsheetid <> '')
		THEN
			SELECT c4020_primary_user
			  INTO v_cur_owner
			  FROM t4020_demand_master
			 WHERE c4020_demand_master_id = p_outdemsheetid;
		END IF;

		gm_pkg_pd_group_pricing.gm_update_demand_sheet_owner (v_cur_owner, p_demandsheetowner);

		UPDATE t4020_demand_master
		   SET c4020_demand_nm = TRIM (p_demandsheetname)
			 , c4020_demand_period = p_demandperiod
			 , c4020_forecast_period = p_forecastperiod
			 , c4020_include_current_fl = p_includecurrfl
			 , c4020_inactive_fl = p_inactiveflag
			 , c4020_last_updated_by = p_userid
			 , c4020_last_updated_date = CURRENT_DATE
			 , c4020_primary_user = p_demandsheetowner
		 WHERE c4020_demand_master_id = p_outdemsheetid;

		IF (SQL%ROWCOUNT = 0)
		THEN
			SELECT s4020_demand_master.NEXTVAL
			  INTO p_outdemsheetid
			  FROM DUAL;

			INSERT INTO t4020_demand_master
						(c4020_demand_master_id, c4020_demand_nm, c4020_demand_period, c4020_forecast_period
					   , c4020_include_current_fl, c4020_inactive_fl, c4020_created_by, c4020_created_date
					   , c4020_primary_user
						)
				 VALUES (p_outdemsheetid, TRIM (p_demandsheetname), p_demandperiod, p_forecastperiod
					   , p_includecurrfl, p_inactiveflag, p_userid, CURRENT_DATE
					   , p_demandsheetowner
						);
		END IF;

		gm_pkg_cm_hierarchy.gm_cm_sav_hierarchyflow (p_hierarchyid, 60260, p_outdemsheetid, p_userid);
	EXCEPTION
		/*To be removed later.. keeping this as a reference for now - Joe
		WHEN demExp THEN
		code := sqlcode;
		msg := sqlerrm;
		gm_procedure_log('sqlcode with save dem',code);
		gm_procedure_log('msg with save dem',msg); */
		WHEN DUP_VAL_ON_INDEX
		THEN
			-- msg := SQLERRM(-00001);
			raise_application_error (-20047, '');	-- 20047 - demand sheet name already present.
	END gm_fc_sav_demsheet;

--
	PROCEDURE gm_fc_sav_grouppartmap (
		p_groupid				  IN	   t4010_group.c4010_group_id%TYPE
	  , p_groupname 			  IN	   t4010_group.c4010_group_nm%TYPE
	  , p_grouptype 			  IN	   t4010_group.c901_type%TYPE
	  , p_inactiveflag			  IN	   t4010_group.c4010_inactive_fl%TYPE
	  , p_inputstr				  IN	   VARCHAR2
	  , p_systemid				  IN	   t4010_group.c207_set_id%TYPE
	  , p_sys_grouptype 		  IN	   t4010_group.c901_group_type%TYPE
	  , p_primary_part_inputstr   IN	   VARCHAR2
	  , p_userid				  IN	   t4010_group.c4010_created_by%TYPE
	  , p_pricedtype			  IN	   t4010_group.c901_priced_type%TYPE
	  , p_group_desc			  IN	   t4010_group.c4010_group_desc%TYPE
	  , p_group_detailed_desc 	  IN	   t4010_group.c4010_group_dtl_desc%TYPE
	  , p_specl_grp_flag		  IN	   t4010_group.c4010_specl_group_fl%TYPE
	  , p_outgroupid			  OUT	   t4010_group.c4010_group_id%TYPE
	  , p_out_exception_part	  OUT	   VARCHAR2
	)
	AS
		v_count 	   NUMBER;
		v_count_pnum1  NUMBER;
		v_count_pnum2  NUMBER;
		subject 	   VARCHAR2 (200);
		to_mail 	   VARCHAR2 (1000);
		mail_body	   VARCHAR2 (4000);
		v_del_parts    VARCHAR2 (4000);
		v_insert_part  VARCHAR2 (4000);
		v_publish	   VARCHAR2 (10);
		v_group_price  t7010_group_price_detail.c7010_price%TYPE;
		v_validate	   VARCHAR2 (1);
		v_part_status  t205_part_number.c901_status_id%TYPE;
		v_publish_fl   VARCHAR2 (4000);
		v_priced_type  VARCHAR2 (4000);
		v_list_price   NUMBER;
 		v_loaner_price NUMBER;
		v_consignment_price  NUMBER;
		v_equity_price NUMBER;
		v_chk_group_name NUMBER;
		v_company_id NUMBER;
		 /*******************************************************
		* Description : Procedure to save group part info
		* Author		: Joe P Kumar
		*******************************************************/
		CURSOR cur_detail	---part which will be deleted from group
		IS
			SELECT t205.c205_part_number_id pnum
			  FROM t205_part_number t205
			 WHERE c205_part_number_id IN (SELECT t4011.c205_part_number_id
											 FROM t4011_group_detail t4011
											WHERE c4010_group_id = p_outgroupid
										   MINUS
										   SELECT token pnum
											 FROM v_in_list
											WHERE token IS NOT NULL);

		CURSOR cur_insert_detail   ---part which will be inserted to group(could be total new part or shared part)
		IS
			SELECT token pnum
			  FROM v_in_list
			 WHERE token IS NOT NULL
			MINUS
			SELECT t4011.c205_part_number_id pnum
			  FROM t4011_group_detail t4011
			 WHERE c4010_group_id = p_outgroupid AND c205_part_number_id IN (SELECT *
																			   FROM v_in_list);
	BEGIN
		p_outgroupid := p_groupid;
		
		    SELECT get_compid_frm_cntx()
	          INTO v_company_id 
	          FROM dual;
		
		-- Populating the temp table,so we can compare if the data is modified after the insert / update.
		DELETE FROM MY_TEMP_T4011_GROUP_DETAIL;
		
		INSERT INTO MY_TEMP_T4011_GROUP_DETAIL
			SELECT
			C205_PART_NUMBER_ID,
			C4010_GROUP_ID,
			C4011_CREATED_BY,
			C4011_CREATED_DATE,
			C4011_GROUP_DETAIL_ID,
			C4011_LAST_UPDATED_BY,
			C4011_LAST_UPDATED_DATE,
			C4011_PART_NUMBER_HISTORYFL,
			C4011_PRIMARY_PART_LOCK_FL,
			C901_ACTION_TYPE,
			C901_PART_PRICING_TYPE
			FROM T4011_GROUP_DETAIL WHERE c4010_group_id = p_outgroupid;
		
		-- The input String in the form of 101.210,101.211 will be set into this temp view.
		my_context.set_my_inlist_ctx (p_inputstr);
		
		UPDATE t4010_group
		   SET c4010_group_nm = p_groupname
			 , c4010_inactive_fl = p_inactiveflag
			 , c207_set_id = p_systemid
			 , c901_group_type = DECODE (p_sys_grouptype, '', c901_group_type, TO_NUMBER (p_sys_grouptype))
			 , c901_priced_type = TO_NUMBER (p_pricedtype)
			 , c4010_group_desc = p_group_desc
			 , c4010_group_dtl_desc = p_group_detailed_desc
			 , c4010_last_updated_by = p_userid
			 , c4010_last_updated_date = CURRENT_DATE
			 , c4010_specl_group_fl = p_specl_grp_flag
		 WHERE c4010_group_id = p_outgroupid;

		IF (SQL%ROWCOUNT = 0)
		THEN
			SELECT s4010_group.NEXTVAL
			  INTO p_outgroupid
			  FROM DUAL;
	          
			INSERT INTO t4010_group
						(c4010_group_id, c4010_group_nm, c901_type, c207_set_id, c901_group_type, c901_priced_type
					   , c4010_inactive_fl, c4010_created_by, c4010_created_date, c101_group_owner
					   , c4010_group_desc, c4010_group_dtl_desc,C1900_COMPANY_ID , c4010_specl_group_fl
						)
				 VALUES (p_outgroupid, p_groupname, p_grouptype, p_systemid, p_sys_grouptype, p_pricedtype
					   , p_inactiveflag, p_userid, CURRENT_DATE, p_userid
					   , p_group_desc, p_group_detailed_desc,v_company_id , p_specl_grp_flag
						);
			gm_pkg_pd_group_txn.gm_sav_system_group_tosync(p_outgroupid,p_systemid,v_company_id,'105360',p_userid);
		END IF;
		/*The below codition is added for PMT-4918 to validate the group Name.
		 * We need to validate when we are creating a new group or when we try to update the same group name, 
		 */
		v_chk_group_name := gm_pkg_pd_group_info.gm_chk_group_count(p_groupname);
		IF (v_chk_group_name > 1) THEN
			raise_application_error('-20624', '');--The Group Name  already exist.
		END IF;
		/* To save group description and detailed description values 
		* 103094: Group
		*/
		gm_pkg_pd_language.gm_sav_lang_details(p_outgroupid, '103094', p_groupname, p_group_desc, p_group_detailed_desc,p_userid);
		
		FOR var_detail IN cur_detail
		LOOP
			SELECT COUNT (1)
			  INTO v_count_pnum1
			  FROM t4010_group t4010, t4011_group_detail t4011
			 WHERE t4011.c205_part_number_id = var_detail.pnum
			   AND t4010.c4010_group_id = t4011.c4010_group_id
			   AND t4010.c901_type = 40045
			   AND T4010.c4010_void_fl IS NULL;

			--		AND t4011.c901_part_pricing_type = 52080
			SELECT COUNT (1)
			  INTO v_count_pnum2
			  FROM t4010_group t4010, t4011_group_detail t4011
			 WHERE t4011.c205_part_number_id = var_detail.pnum
			   AND t4010.c4010_group_id = t4011.c4010_group_id
			   AND t4011.c901_part_pricing_type = 52080
			   AND t4010.c4010_group_id = p_outgroupid
			   AND t4010.c901_type = 40045
			   AND T4010.c4010_void_fl IS NULL;

			SELECT c901_status_id
			  INTO v_part_status
			  FROM t205_part_number t205
			 WHERE t205.c205_part_number_id = var_detail.pnum;

			IF (v_count_pnum1 > 1 AND v_count_pnum2 > 0 AND v_part_status != 20369)
			THEN
				--check to see if any parts which will be deleted is shared part and it's primary with not having obsolete status,than apperr
				raise_application_error (-20196, '');
			END IF;
		END LOOP;

		SELECT c4010_publish_fl
		  INTO v_publish
		  FROM t4010_group
		 WHERE c4010_group_id = p_outgroupid AND c4010_void_fl IS NULL;

		-- if group is published, validate
		IF v_publish = 'Y'
		THEN
				-- append pricing team emails
			/*	FOR var_emails_detail IN cur_emails_detail
				LOOP
					to_mail 	:= to_mail || var_emails_detail.email_id || ', ';
				END LOOP;
				*/
				--append deleted parts
			FOR var_detail IN cur_detail
			LOOP
				v_del_parts := v_del_parts || var_detail.pnum || ', ';
			END LOOP;

			v_group_price := get_group_list_price (p_outgroupid);

			FOR var_insert_detail IN cur_insert_detail
			LOOP
				v_validate	:= get_validation_grp_part (var_insert_detail.pnum, p_pricedtype, v_group_price);

				IF (v_validate = 'N')
				THEN
					p_out_exception_part := p_out_exception_part || var_insert_detail.pnum || ', ';
				ELSE
					v_insert_part := v_insert_part || var_insert_detail.pnum || ', ';
					p_out_exception_part := '';
				END IF;
			END LOOP;

			IF (p_out_exception_part != '')
			THEN
				RETURN;
			END IF;
		END IF;

		--check to see if it's new group
		SELECT COUNT (1)
		  INTO v_count
		  FROM t4011_group_detail
		 WHERE c4010_group_id = p_outgroupid;

		-- Delete those parts which have already been mapped but unchecked from the input
		IF (v_count > 0)
		THEN
			DELETE FROM t4011_group_detail
				  WHERE c205_part_number_id IN (SELECT t4011.c205_part_number_id
												  FROM t4011_group_detail t4011
												 WHERE c4010_group_id = p_outgroupid
												MINUS
												SELECT token pnum
												  FROM v_in_list
												 WHERE token IS NOT NULL)
					AND c4010_group_id = p_outgroupid;
				
			--Whenever the detail is updated, the Group table has to be updated ,so ETL can Sync it to GOP.
			UPDATE t4010_group 
			   SET c4010_last_updated_by = p_userid
			     , c4010_last_updated_date = CURRENT_DATE
			WHERE c4010_group_id = p_outgroupid;					
		END IF;

				--New parts which is primary to update to group pricing
	  BEGIN
		SELECT c4010_publish_fl, c901_priced_type, gm_pkg_pd_group_pricing.get_base_group_price(c4010_group_id, 52060), gm_pkg_pd_group_pricing.get_base_group_price(c4010_group_id, 52061), gm_pkg_pd_group_pricing.get_base_group_price(c4010_group_id, 52062), gm_pkg_pd_group_pricing.get_base_group_price(c4010_group_id, 52063)
    		INTO v_publish_fl, v_priced_type, v_list_price, v_loaner_price, v_consignment_price , v_equity_price
   			FROM t4010_group
 		  WHERE c4010_group_id = p_groupid
  			AND c4010_void_fl IS NULL;
  	  EXCEPTION
    	WHEN NO_DATA_FOUND THEN
        v_list_price :=0;
        v_loaner_price:=0;
        v_priced_type:=NULL;
      END;
		 IF  v_priced_type=52110  AND v_list_price  <> 0 AND v_loaner_price <> 0 -- group
 		 THEN
		--once we changed all files (distributor related files) the below update command needs to delete. 
 		 UPDATE t205_part_number
			SET c205_list_price   = v_list_price   
			   ,c205_loaner_price = v_loaner_price
               ,c205_consignment_price  = v_consignment_price   
               ,c205_equity_price =v_equity_price
 			   ,c205_last_updated_date = CURRENT_DATE
 			   ,c205_last_updated_by = p_userid
  		WHERE c205_part_number_id IN
    		(SELECT token pnum FROM v_in_list WHERE token IS NOT NULL
     		 MINUS (
    		 SELECT t4011.c205_part_number_id
      			 FROM t4011_group_detail t4011
      			WHERE c4010_group_id       = p_outgroupid
        		AND c205_part_number_id IN
        	(SELECT * FROM v_in_list)
        	UNION
        	 SELECT v.token
           FROM v_in_list v, t4011_group_detail t4011 ,T4010_GROUP t4010
          WHERE t4011.c205_part_number_id    = v.token
            AND t4011.c901_part_pricing_type = 52080 -- Primary
            and T4011.C4010_GROUP_ID              <> p_outgroupid
            and T4011.C4010_GROUP_ID = T4010.C4010_GROUP_ID
            and T4010.C4010_VOID_FL IS NULL
           )
   		 )
   		 AND c205_active_fl = 'Y';
   		 
 		--We are updating the price in the  T2052_PART_PRICE_MAPPING instead of T2052_PART_PRICE_MAPPING.
 		--Once if we change all fetch quiries to T2052_PART_PRICE_MAPPING table. we are going to delete the above update query.
 		 UPDATE T2052_PART_PRICE_MAPPING
			SET c2052_list_price   = v_list_price   
			   ,c2052_loaner_price = v_loaner_price
               ,c2052_consignment_price  = v_consignment_price   
               ,c2052_equity_price =v_equity_price
 			   ,c2052_updated_date = CURRENT_DATE
 			   ,c2052_updated_by = p_userid
 			   , c2052_last_updated_by   =  p_userid
 			   , C2052_Last_updated_date = CURRENT_DATE
  		WHERE c205_part_number_id IN
    		(SELECT token pnum FROM v_in_list WHERE token IS NOT NULL
     		 MINUS (
    		 SELECT t4011.c205_part_number_id
      			 FROM t4011_group_detail t4011
      			WHERE c4010_group_id       = p_outgroupid
        		AND c205_part_number_id IN
        	(SELECT * FROM v_in_list)
        	UNION
        	 SELECT v.token
           FROM v_in_list v, t4011_group_detail t4011,T4010_GROUP t4010
          WHERE t4011.c205_part_number_id    = v.token
            AND t4011.c901_part_pricing_type = 52080 -- Primary
            and T4011.C4010_GROUP_ID              <> p_outgroupid
            and T4011.C4010_GROUP_ID = T4010.C4010_GROUP_ID
            and T4010.C4010_VOID_FL IS NULL
        	)
   		 )
   		AND c1900_company_id =  v_company_id;
   		 
 		END IF;
		
		-- Insert the new parts which the user wants to map to the existing group
		INSERT INTO t4011_group_detail
					(c4011_group_detail_id, c205_part_number_id, c901_part_pricing_type, c4010_group_id
				   , c4011_part_number_historyfl, c4011_created_by, c4011_created_date, c4011_last_updated_by
				   , c4011_last_updated_date,c4011_primary_part_lock_fl)
			SELECT s4011_group_detail.NEXTVAL, ins.pnum
				 , gm_pkg_op_sheet.get_pricing_type (ins.pnum, p_primary_part_inputstr), p_outgroupid, 'Y', p_userid
				 , CURRENT_DATE, p_userid, CURRENT_DATE,gm_pkg_op_sheet.get_primary_lock_from_input(ins.pnum, p_primary_part_inputstr)	--52091 ADD, 52090 Delete
			  FROM (SELECT token pnum
					  FROM v_in_list
					 WHERE token IS NOT NULL
					MINUS
					SELECT t4011.c205_part_number_id
					  FROM t4011_group_detail t4011
					 WHERE c4010_group_id = p_outgroupid AND c205_part_number_id IN (SELECT *
																					   FROM v_in_list)) ins;

		gm_pkg_pd_group_pricing.gm_pd_sav_group_price (p_outgroupid, p_primary_part_inputstr, p_userid);
		
		--This procedure will check if the Group details is modified and will mark the group as updated for Product Catalog.
		gm_pkg_pdpc_prodcattxn.gm_chk_group_detail_update(p_outgroupid,p_userid);
	END gm_fc_sav_grouppartmap;

--
	PROCEDURE gm_fc_fch_demandmap (
		p_demandsheetid    IN		t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_demandsheetype   IN		t4020_demand_master.c901_demand_type%TYPE
	  , p_reftype		   IN		t4021_demand_mapping.c901_ref_type%TYPE
	  , p_outunselected    OUT		TYPES.cursor_type
	  , p_outselected	   OUT		TYPES.cursor_type
	  , p_request_period   OUT		t4020_demand_master.c4020_request_period%TYPE
	)
	AS
		/*******************************************************
			* Description : Procedure to fetch mapping information for a demand sheet
			* Author		: Joe P Kumar
			*******************************************************/
		v_demandsheetype t4020_demand_master.c901_demand_type%TYPE;
	BEGIN
		SELECT c4020_request_period, c901_demand_type
		  INTO p_request_period, v_demandsheetype
		  FROM t4020_demand_master
		 WHERE c4020_demand_master_id = p_demandsheetid;

		IF (p_reftype = 40030)	 -- for Group
		THEN
			gm_fc_fch_demandgroupmap (p_demandsheetid, v_demandsheetype, p_reftype, p_outunselected, p_outselected);
		ELSIF (p_reftype = 40031)	-- for Set
		THEN
			gm_fc_fch_demandsetmap (p_demandsheetid, v_demandsheetype, p_reftype, p_outunselected, p_outselected);
		ELSIF (p_reftype = 40032)	-- for Region
		THEN
			gm_fc_fch_demandcodemap (p_demandsheetid
								   , v_demandsheetype
								   , p_reftype
								   , 'REGN'
								   , p_outunselected
								   , p_outselected
									);
		ELSIF (p_reftype = 40033)	-- for InHouse
		THEN
			gm_fc_fch_demandcodemap (p_demandsheetid
								   , v_demandsheetype
								   , p_reftype
								   , 'INPRP'
								   , p_outunselected
								   , p_outselected
									);
		END IF;
	END gm_fc_fch_demandmap;

--
	PROCEDURE gm_fc_fch_demandgroupmap (
		p_demandsheetid    IN		t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_demandsheetype   IN		t4020_demand_master.c901_demand_type%TYPE
	  , p_reftype		   IN		t4021_demand_mapping.c901_ref_type%TYPE
	  , p_outunselected    OUT		TYPES.cursor_type
	  , p_outselected	   OUT		TYPES.cursor_type
	)
	AS
	/*******************************************************
	 * Description : Procedure to fetch grouping information for a demand sheet
	 * Author		 : Joe P Kumar
	 *******************************************************/
	BEGIN
		OPEN p_outunselected
		 FOR
			 SELECT   t4010.c4010_group_id ID, t4010.c4010_group_nm NAME
				 FROM t4010_group t4010
				WHERE t4010.c4010_group_id NOT IN (
						  SELECT NVL (t4021.c4021_ref_id, 1)
							FROM t4021_demand_mapping t4021
						   WHERE t4021.c4020_demand_master_id = p_demandsheetid
							 AND get_demand_type (t4021.c4020_demand_master_id) = p_demandsheetype
							 AND t4021.c901_ref_type = p_reftype)
				  AND t4010.c4010_void_fl IS NULL
				  AND t4010.c901_type = '40045'   -- 40045 is forecast
			 ORDER BY t4010.c4010_group_nm;

		OPEN p_outselected
		 FOR
			 SELECT   t4010.c4010_group_id ID, t4010.c4010_group_nm NAME
				 FROM t4021_demand_mapping t4021, t4010_group t4010
				WHERE t4021.c4021_ref_id(+) = t4010.c4010_group_id
				  AND t4021.c901_ref_type = p_reftype
				  AND t4021.c4020_demand_master_id = p_demandsheetid
				  AND get_demand_type (t4021.c4020_demand_master_id) = p_demandsheetype
				  AND t4010.c4010_void_fl IS NULL
				  AND t4010.c901_type = '40045'   -- 40045 is forecast
			 ORDER BY t4010.c4010_group_nm;
	END gm_fc_fch_demandgroupmap;

	--
	PROCEDURE gm_fc_fch_demandsetmap (
		p_demandsheetid    IN		t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_demandsheetype   IN		t4020_demand_master.c901_demand_type%TYPE
	  , p_reftype		   IN		t4021_demand_mapping.c901_ref_type%TYPE
	  , p_outunselected    OUT		TYPES.cursor_type
	  , p_outselected	   OUT		TYPES.cursor_type
	)
	AS
	/*******************************************************
	 * Description : Procedure to fetch set information for a demand sheet
	 * Author		 : Joe P Kumar
	 *******************************************************/
	BEGIN
		OPEN p_outunselected
		 FOR
			 SELECT   t207.c207_set_id ID, t207.c207_set_nm NAME
				 FROM t207_set_master t207
				WHERE t207.c207_set_id NOT IN (
						  SELECT NVL (t4021.c4021_ref_id, 1)
							FROM t4021_demand_mapping t4021
						   WHERE t4021.c4020_demand_master_id = p_demandsheetid
							 AND get_demand_type (t4021.c4020_demand_master_id) = p_demandsheetype
							 AND t4021.c901_ref_type = p_reftype)
				  AND t207.c207_void_fl IS NULL
				  AND t207.c207_obsolete_fl IS NULL
				  AND t207.c901_status_id=20367 
				  AND t207.c901_set_grp_type = 1601
			 ORDER BY t207.c207_set_nm;

		OPEN p_outselected
		 FOR
			 SELECT   t207.c207_set_id ID, t207.c207_set_nm|| DECODE(t207.c901_status_id,20369,' (** '||GET_CODE_NAME(t207.c901_status_id)||') ') NAME
				 FROM t4021_demand_mapping t4021, t207_set_master t207
				WHERE t4021.c4021_ref_id(+) = t207.c207_set_id
				  AND t4021.c901_ref_type = p_reftype
				  AND t4021.c4020_demand_master_id = p_demandsheetid
				  AND get_demand_type (t4021.c4020_demand_master_id) = p_demandsheetype
				  AND t207.c207_void_fl IS NULL
				  AND t207.c207_obsolete_fl IS NULL
				  AND t207.c901_set_grp_type = 1601
			 ORDER BY t207.c207_set_nm;
	END gm_fc_fch_demandsetmap;

	--
	PROCEDURE gm_fc_fch_demandcodemap (
		p_demandsheetid    IN		t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_demandsheetype   IN		t4020_demand_master.c901_demand_type%TYPE
	  , p_reftype		   IN		t4021_demand_mapping.c901_ref_type%TYPE
	  , p_codegroup 	   IN		t901_code_lookup.c901_code_grp%TYPE
	  , p_outunselected    OUT		TYPES.cursor_type
	  , p_outselected	   OUT		TYPES.cursor_type
	)
	AS
			/*******************************************************
		* Description : Procedure to fetch region information for a demand sheet
		* Author		: Joe P Kumar
		*******************************************************/
		v_demandsheettype t4020_demand_master.c901_demand_type%TYPE;
	BEGIN
		SELECT c901_demand_type
		  INTO v_demandsheettype
		  FROM t4020_demand_master
		 WHERE c4020_demand_master_id = p_demandsheetid;

		IF (v_demandsheettype IS NOT NULL)
		THEN
			OPEN p_outunselected
			 FOR
				 SELECT   t901.c901_code_id ID, t901.c901_code_nm NAME
					 FROM t901_code_lookup t901
					WHERE t901.c901_code_id NOT IN (
							  SELECT NVL (t4021.c4021_ref_id, 1)
								FROM t4021_demand_mapping t4021
							   WHERE t4021.c4020_demand_master_id = p_demandsheetid
								 AND get_demand_type (t4021.c4020_demand_master_id) = p_demandsheetype
								 AND t4021.c901_ref_type = p_reftype)
					  AND t901.c901_code_grp = p_codegroup
				 ORDER BY t901.c901_code_nm;

			OPEN p_outselected
			 FOR
				 SELECT   t901.c901_code_id ID, t901.c901_code_nm NAME
					 FROM t4021_demand_mapping t4021, t901_code_lookup t901
					WHERE t4021.c4021_ref_id(+) = t901.c901_code_id
					  AND t4021.c901_ref_type = p_reftype
					  AND t4021.c4020_demand_master_id = p_demandsheetid
					  AND get_demand_type (t4021.c4020_demand_master_id) = p_demandsheetype
					  AND t901.c901_code_grp = p_codegroup
				 ORDER BY t901.c901_code_nm;
		END IF;

		-- initially for the first time, the default is other internationals is unselected, all others selected
		IF (v_demandsheettype IS NULL)
		THEN
			OPEN p_outunselected
			 FOR
				 SELECT t901.c901_code_id ID, t901.c901_code_nm NAME
				   FROM t901_code_lookup t901
				  WHERE t901.c901_code_id = 4007 AND t901.c901_code_grp = p_codegroup;

			OPEN p_outselected
			 FOR
				 SELECT t901.c901_code_id ID, t901.c901_code_nm NAME
				   FROM t901_code_lookup t901
				  WHERE t901.c901_code_id <> 4007 AND t901.c901_code_grp = p_codegroup;
		END IF;
	END gm_fc_fch_demandcodemap;

--
	PROCEDURE gm_fc_fch_demsheet_detail (
		p_demandsheetid   IN	   t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_detail		  OUT	   TYPES.cursor_type
	)
	AS
	 /*******************************************************
	* Description : Procedure to fetch detail for a demand sheet
	* Author		: Xun
	*******************************************************/
	BEGIN
		OPEN p_detail
		 FOR
			 SELECT c4020_demand_master_id demandsheetid, c4020_demand_nm demandsheetname
				  , c901_demand_type demandsheettype, get_code_name (c901_demand_type) demandsheettypename
				  , c4020_demand_period demandperiod, c4020_forecast_period forecastperiod
				  , DECODE (c4020_include_current_fl, 'Y', 'on', 'off') includecurrmonth
				  , DECODE (c4020_inactive_fl, 'Y', 'on', 'off') activeflag
				  , gm_pkg_op_sheet.get_hierarchy_id ('60260', p_demandsheetid) hierarchyid
				  , c4020_primary_user demandsheetowner, c4020_request_period requestperiod
				  , TO_CHAR (c4020_created_date, get_rule_value('DATEFMT','DATEFORMAT')) createddt
			   FROM t4020_demand_master
			  WHERE c4020_void_fl IS NULL AND c4020_demand_master_id = p_demandsheetid;
	END gm_fc_fch_demsheet_detail;

--
	PROCEDURE gm_fch_demand_par_info (
		p_demandmasterid   IN		t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_partnumber	   IN		t205_part_number.c205_part_number_id%TYPE
	  , p_detail		   OUT		TYPES.cursor_type
	)
	AS
			/*******************************************************
		* Description : Procedure to fetch demand par info
		* Author		: Xun Qu
		******************************************************/
		v_demandsheettype t4020_demand_master.c901_demand_type%TYPE;
	BEGIN
		SELECT c901_demand_type
		  INTO v_demandsheettype
		  FROM t4020_demand_master
		 WHERE c4020_demand_master_id = p_demandmasterid;

		IF (v_demandsheettype = 40020)
		THEN
			OPEN p_detail
			 FOR
				 SELECT   t205.c205_part_number_id pnum, t205.c205_part_num_desc pdesc
						, NVL (pvalue.parvalue, 0) parvalue, NVL (pvalue.c4022_history_fl, 'N') history_fl
						, '' comments
						, get_log_flag (t205.c205_part_number_id || '-' || t4021.c4020_demand_master_id, 1231) rlog
					 FROM t4010_group t4010
						, t4021_demand_mapping t4021
						, t4011_group_detail t4011
						, t205_part_number t205
						, (SELECT c205_part_number_id, c4022_par_value parvalue, t4022.c4022_history_fl
							 FROM t4022_demand_par_detail t4022
							WHERE c4020_demand_master_id = p_demandmasterid) pvalue
					WHERE t4021.c4020_demand_master_id = p_demandmasterid
					  AND t4021.c901_ref_type = 40030
					  AND t4021.c4021_ref_id = t4010.c4010_group_id
					  AND t4010.c4010_group_id = t4011.c4010_group_id
					  AND t4011.c205_part_number_id = t205.c205_part_number_id
					  AND t4011.c205_part_number_id = pvalue.c205_part_number_id(+)
					  AND t4011.c205_part_number_id LIKE p_partnumber || '%'
				 ORDER BY t205.c205_part_number_id;
		ELSIF (v_demandsheettype = 40021 OR v_demandsheettype = 40022)
		THEN
			OPEN p_detail
			 FOR
				 SELECT DISTINCT t205.c205_part_number_id pnum, t205.c205_part_num_desc pdesc
							   , NVL (pvalue.parvalue, 0) parvalue, NVL (pvalue.c4022_history_fl, 'N') history_fl
							   , '' comments
							   , get_log_flag (t205.c205_part_number_id || '-' || t4021.c4020_demand_master_id
											 , 1231
											  ) rlog
							FROM t4021_demand_mapping t4021
							   , t208_set_details t208
							   , t205_part_number t205
							   , (SELECT c205_part_number_id, c4022_par_value parvalue, t4022.c4022_history_fl
									FROM t4022_demand_par_detail t4022
								   WHERE c4020_demand_master_id = p_demandmasterid) pvalue
						   WHERE t4021.c4020_demand_master_id = p_demandmasterid
							 AND t4021.c901_ref_type = 40031
							 AND t4021.c4021_ref_id = t208.c207_set_id
							 AND NVL (t208.c208_set_qty, 0) = 0
							 AND t208.c208_void_fl IS NULL
							 AND t205.c205_part_number_id = t208.c205_part_number_id
							 AND t208.c205_part_number_id = pvalue.c205_part_number_id(+)
							 AND t205.c205_part_number_id LIKE p_partnumber || '%'
						ORDER BY t205.c205_part_number_id;
		END IF;
	END gm_fch_demand_par_info;

--
	PROCEDURE gm_sav_demand_par_info (
		p_userid	 IN   t4022_demand_par_detail.c4022_updated_by%TYPE
	  , p_inputstr	 IN   VARCHAR2
	)
	AS
		v_strlen	   NUMBER := NVL (LENGTH (p_inputstr), 0);
		v_string	   VARCHAR2 (30000) := p_inputstr;
		v_substring    VARCHAR2 (1000);
		v_demandmasterid t4020_demand_master.c4020_demand_master_id%TYPE;
		v_partnumber   t4022_demand_par_detail.c205_part_number_id%TYPE;
		v_parvalue	   t4022_demand_par_detail.c4022_par_value%TYPE;
		v_demand_par_detail_id t4022_demand_par_detail.c4022_demand_par_detail_id%TYPE;
		--
	/*******************************************************
	  * Description : Procedure to save demand sheet par
	  * Author		  : Xun
	  *******************************************************/
	BEGIN
		IF v_strlen > 0
		THEN
			WHILE INSTR (v_string, '|') <> 0
			LOOP
				--
				v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
				v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
				--
				v_partnumber := NULL;
				v_demandmasterid := NULL;
				v_parvalue	:= NULL;
				--
				v_partnumber := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_demandmasterid := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_parvalue	:= v_substring;

				UPDATE t4022_demand_par_detail
				   SET c4022_par_value = v_parvalue
					 , c4022_updated_by = p_userid
					 , c4022_updated_date = CURRENT_DATE
				 WHERE c205_part_number_id = v_partnumber AND c4020_demand_master_id = v_demandmasterid;

				IF (SQL%ROWCOUNT = 0)
				THEN
					INSERT INTO t4022_demand_par_detail
								(c4022_demand_par_detail_id, c4020_demand_master_id, c205_part_number_id
							   , c4022_par_value, c4022_updated_by, c4022_updated_date
								)
						 VALUES (s4022_demand_par_detail.NEXTVAL, v_demandmasterid, v_partnumber
							   , v_parvalue, p_userid, CURRENT_DATE
								);
				END IF;
			END LOOP;
		END IF;
	END gm_sav_demand_par_info;

--

	/*******************************************************
			 * Description : Procedure to Update Demand Sheet Detail
			 * Author			: Xun
			 *******************************************************/
	PROCEDURE gm_fc_upt_demandsheetpar (
		p_demandsheetid   IN   t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	  , p_partnumber	  IN   t205_part_number.c205_part_number_id%TYPE
	  , p_parvalue		  IN   t4022_demand_par_detail.c4022_par_value%TYPE
	)
	AS
		v_demandsheettype t4020_demand_master.c901_demand_type%TYPE;
	BEGIN
		SELECT t4020.c901_demand_type
		  INTO v_demandsheettype
		  FROM t4040_demand_sheet t4040, t4020_demand_master t4020
		 WHERE t4040.c4020_demand_master_id = t4020.c4020_demand_master_id
		   AND t4040.c4040_demand_sheet_id = p_demandsheetid;

		UPDATE t4042_demand_sheet_detail t4042
		   SET t4042.c4042_qty = p_parvalue
		 WHERE t4042.c4040_demand_sheet_id = p_demandsheetid
		   AND t4042.c205_part_number_id = p_partnumber
		   AND t4042.c901_type = 50566
		   AND t4042.c4042_ref_id = DECODE (v_demandsheettype, 40020, t4042.c4042_ref_id, -9999);
	END gm_fc_upt_demandsheetpar;

--
	PROCEDURE gm_fch_demand_par_history (
		p_demandmastertid	IN		 t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_partnumber		IN		 t205_part_number.c205_part_number_id%TYPE
	  , p_detail			OUT 	 TYPES.cursor_type
	)
	AS
	 /*******************************************************
	* Description : Procedure to fetch Par detail for a demand sheet
	* Author		: Xun
	*******************************************************/
	BEGIN
		OPEN p_detail
		 FOR
			 SELECT t4020.c4020_demand_nm demandsheetname, t4022.c205_part_number_id pnum
				  , get_partnum_desc (p_partnumber) pdesc, t4022.c4022_demand_par_detail_id detailid
			   FROM t4020_demand_master t4020, t4022_demand_par_detail t4022
			  WHERE t4020.c4020_demand_master_id = t4022.c4020_demand_master_id
				AND t4022.c4020_demand_master_id = p_demandmastertid
				AND t4022.c205_part_number_id = p_partnumber;
	END gm_fch_demand_par_history;

--
	PROCEDURE gm_fc_sav_demandsheetmap (
		p_demandsheetid    IN	t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_demandsheetype   IN	t4020_demand_master.c901_demand_type%TYPE
	  , p_reftype		   IN	t4021_demand_mapping.c901_ref_type%TYPE
	  , p_inputstr		   IN	VARCHAR2
	  , p_request_period   IN	t4020_demand_master.c4020_request_period%TYPE
	)
	AS
		/*******************************************************
			* Description : Procedure to save grouping information for a demand sheet
			* Author		: Joe P Kumar
			*******************************************************/
		v_demandsheettype t4020_demand_master.c901_demand_type%TYPE;
		v_demandsheetmonthcnt NUMBER (10);

		CURSOR cur_sheet_details
		IS
			SELECT token refid
			  FROM v_in_list;
	BEGIN
		-- The input String in the form of 101.210,101.211 will be set into this temp view.
		my_context.set_my_inlist_ctx (p_inputstr);

		SELECT COUNT (c4040_demand_sheet_id)
		  INTO v_demandsheetmonthcnt
		  FROM t4040_demand_sheet
		 WHERE c4020_demand_master_id = p_demandsheetid;

		SELECT c901_demand_type
		  INTO v_demandsheettype
		  FROM t4020_demand_master
		 WHERE c4020_demand_master_id = p_demandsheetid;

		IF (v_demandsheetmonthcnt > 0 AND v_demandsheettype <> p_demandsheetype)
		THEN
			-- Error message is Cannot change type as monthly demand sheets have already been generated
			raise_application_error (-20059, '');
		END IF;

		UPDATE t4020_demand_master
		   SET c901_demand_type = p_demandsheetype
			 , c4020_request_period = p_request_period
		 WHERE c4020_demand_master_id = p_demandsheetid;

		-- If its new mapping then remove old information and reload again
		-- For E.g. if sheet moved from sales to consignment then
		-- remove all old transaction
		IF (v_demandsheettype <> p_demandsheetype)
		THEN
			DELETE FROM t4021_demand_mapping
				  WHERE c4020_demand_master_id = p_demandsheetid;
		END IF;

		FOR var_sheet_detail IN cur_sheet_details
		LOOP
			UPDATE t4021_demand_mapping
			   SET c4021_ref_id = var_sheet_detail.refid
			 WHERE c4020_demand_master_id = p_demandsheetid
			   AND c901_ref_type = p_reftype
			   AND c4021_ref_id = var_sheet_detail.refid;

			IF (SQL%ROWCOUNT = 0)
			THEN
				INSERT INTO t4021_demand_mapping
							(c4021_demand_mapping_id, c4020_demand_master_id, c901_ref_type, c4021_ref_id
						   , c901_action_type
							)
					 VALUES (s4021_demand_mapping.NEXTVAL, p_demandsheetid, p_reftype, var_sheet_detail.refid
						   , DECODE (p_reftype, 40031, 50291, NULL)
							);
			END IF;
		END LOOP;

		DELETE FROM t4021_demand_mapping
			  WHERE c4020_demand_master_id = p_demandsheetid
				AND c901_ref_type = p_reftype
				AND c4021_ref_id NOT IN (SELECT token refid
										   FROM v_in_list);
	END gm_fc_sav_demandsheetmap;

 --
/*******************************************************
 *		 Purpose: function is used to fetch the demandtype given the demand id
 *******************************************************/
--
	FUNCTION get_demand_type (
		p_demandsheetid   IN   t4020_demand_master.c4020_demand_master_id%TYPE
	)
		RETURN NUMBER
	IS
--
		v_demand_sheet_type NUMBER;
	BEGIN
		SELECT c901_demand_type
		  INTO v_demand_sheet_type
		  FROM t4020_demand_master
		 WHERE c4020_demand_master_id = p_demandsheetid;

		RETURN v_demand_sheet_type;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN 0;
	END get_demand_type;

	/*******************************************************
				* Purpose: function is used to fetch the demand sheet id given the demand master id
				*******************************************************
			   --
				   FUNCTION get_demandsheetid (
						p_demandmasterid	IN		t4020_demand_master.c4020_demand_master_id%TYPE
				   )
						RETURN VARCHAR2
				   IS
						v_demand_sheet_id VARCHAR2 (100);
				   BEGIN
						SELECT c4040_demand_sheet_id
						 INTO v_demand_sheet_id
						 FROM t4040_demand_sheet
						WHERE c4020_demand_master_id = p_demandmasterid;

						RETURN v_demand_sheet_id;
				   EXCEPTION
						WHEN NO_DATA_FOUND
						THEN
							 RETURN 0;
				   END get_demandsheetid;/


		--


		/********************************************************
			   * Description : Procedure to approve the demand sheet for that month
			   * Author 	: Joe P Kumar
			*******************************************************/
	PROCEDURE gm_fc_sav_approveds (
		p_demandsheetmonthid   IN	t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	  , p_userid			   IN	t4040_demand_sheet.c4040_last_updated_by%TYPE
	)
	AS
	BEGIN
		-- Check whether this demand sheet can be approved
		gm_pkg_op_sheet.gm_fc_chk_request (p_demandsheetmonthid);

		-- Update
		UPDATE t4040_demand_sheet
		   SET c901_status = 50551
			 , c4040_approved_date = CURRENT_DATE
			 , c4040_approved_by = p_userid
			 , c4040_last_updated_by = p_userid
			 , c4040_last_updated_date = CURRENT_DATE
		 WHERE c4040_demand_sheet_id = p_demandsheetmonthid;
	END gm_fc_sav_approveds;

 /*******************************************************
 * Purpose: function is used to fetch the demand sheet name given the demand sheet id
 *******************************************************/
--
	FUNCTION get_demandsheet_name (
		p_demandsheetid   IN   t4020_demand_master.c4020_demand_master_id%TYPE
	)
		RETURN VARCHAR2
	IS
		v_demand_sheet_name VARCHAR2 (100);
	BEGIN
		SELECT c4020_demand_nm
		  INTO v_demand_sheet_name
		  FROM t4020_demand_master
		 WHERE c4020_demand_master_id = p_demandsheetid;

		RETURN v_demand_sheet_name;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN 0;
	END get_demandsheet_name;

 /*******************************************************
 * Purpose: function is used to fetch the hierarchy id given the demand sheet name
 *******************************************************/
--
	FUNCTION get_hierarchy_id (
		p_type	   IN	t9352_hierarchy_flow.c901_ref_type%TYPE
	  , p_ref_id   IN	t9352_hierarchy_flow.c9352_ref_id%TYPE
	)
		RETURN VARCHAR2
	IS
		v_hierarchyid  t9350_hierarchy.c9350_hierarchy_id%TYPE;
	BEGIN
		SELECT c9350_hierarchy_id
		  INTO v_hierarchyid
		  FROM t9352_hierarchy_flow
		 WHERE c901_ref_type = p_type AND c9352_ref_id = p_ref_id;

		RETURN v_hierarchyid;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN 0;
	END get_hierarchy_id;

--
/*******************************************************
 * Purpose: Procedure is used to check if the qty in
			the request is greater than the required,
			if greater then an exception is raised
 *******************************************************/
--
	PROCEDURE gm_fc_chk_request (
		p_dsmonthid   IN   t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	)
	AS
		v_count 	   NUMBER;
	BEGIN
		SELECT COUNT (1)
		  INTO v_count
		  FROM (SELECT ID
					 ,	 gm_pkg_op_sheet_summary.get_ds_growth_request_count (dmid, ID, ref_type, req_date)
					   - growth_qty req_count
				  FROM (SELECT	 c4030_ref_id ID
							   , DECODE (NVL (TRIM (get_set_name (c4030_ref_id)), '0')
									   , '0', 20298
									   , NVL (TRIM (get_partnum_desc (c4030_ref_id)), '0'), '0'
									   , 20296
										) ref_type
							   , NVL (SUM (t4043.c4043_value), 0) growth_qty, c4043_start_date req_date
							   , t4040.c4020_demand_master_id dmid
							FROM t4040_demand_sheet t4040
							   , t4043_demand_sheet_growth t4043
							   , t4030_demand_growth_mapping t4030
						   WHERE t4040.c4040_demand_sheet_id = p_dsmonthid
							 AND t4040.c4040_demand_sheet_id = t4043.c4040_demand_sheet_id
							 AND t4030.c4030_demand_growth_id = t4043.c4030_demand_growth_id
							 AND c4043_start_date BETWEEN t4040.c4040_demand_period_dt
													  AND ADD_MONTHS (t4040.c4040_demand_period_dt
																	, (t4040.c4040_request_period - 1)
																	 )
							 AND t4040.c901_demand_type IN ('40021', '40022')
							 AND (	 t4043.c901_ref_type = '20381'
								  OR (t4043.c901_ref_type = '20382' AND t4043.c4043_value <> 0)
								 )
						GROUP BY t4040.c4020_demand_master_id, c4043_start_date, c4030_ref_id)) temp
		 WHERE temp.req_count > 0;

		IF v_count > 0
		THEN
			raise_application_error ('-20075', '');
		END IF;
	END gm_fc_chk_request;

--

	/*******************************************************
	* Purpose: function is used to fetch the demand sheet status
	*******************************************************/
	FUNCTION get_demand_sheet_status (
		p_demand_master_id	 t4040_demand_sheet.c4020_demand_master_id%TYPE
	  , p_date				 t4031_growth_details.c4031_start_date%TYPE
	)
		RETURN NUMBER
	IS
/*	  Description	  : THIS FUNCTION RETURNS STATUS FOR DEMAND SHEET
 Parameters 		: p_demand_master_id,P_date
*/
		v_status	   NUMBER;
	BEGIN
		BEGIN
			SELECT c901_status
			  INTO v_status
			  FROM t4040_demand_sheet t4040
			 WHERE t4040.c4020_demand_master_id = p_demand_master_id AND t4040.c4040_demand_period_dt = p_date;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				RETURN NULL;
		END;

		RETURN v_status;
	END get_demand_sheet_status;

--

	/*******************************************************
   * Description : Procedure to fetch Set Mapping Info
   * Author 	 : Xun
   *******************************************************/
--
	PROCEDURE gm_fc_fch_setmap (
		p_demand_master_id	 IN 	  t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_setmapdetails 	 OUT	  TYPES.cursor_type
	)
	AS
		v_count 	   NUMBER;
	BEGIN
		OPEN p_setmapdetails
		 FOR
			 SELECT   DECODE (c4021_ref_id, '-9999', '', c4021_ref_id) setid
					, DECODE (c4021_ref_id, '-9999', 'Item Consignment', get_set_name (c4021_ref_id)) setname
					, c901_action_type setactiontype, c4021_stack_month monthid
				 FROM t4021_demand_mapping
				WHERE c4020_demand_master_id = p_demand_master_id AND c901_ref_type IN (40031, 40034)
			 UNION
			 SELECT   '' setid, 'Item Consignment' setname, NULL setactiontype, NULL monthid
				 FROM DUAL
				WHERE 0 IN (SELECT COUNT (1)
							  FROM t4021_demand_mapping
							 WHERE c4020_demand_master_id = p_demand_master_id AND c901_ref_type = 40034)	--first time, there's no item consignment
			 ORDER BY setid;
	END gm_fc_fch_setmap;

/*******************************************************
	  * Description : Procedure to save Set Mapping Info
	  * Author		  : Xun
	  *******************************************************/
	PROCEDURE gm_fc_sav_setmap (
		p_inputstr	 IN   VARCHAR2
	)
	AS
		v_strlen	   NUMBER := NVL (LENGTH (p_inputstr), 0);
		v_string	   VARCHAR2 (30000) := p_inputstr;
		v_substring    VARCHAR2 (1000);
		v_demandmasterid t4020_demand_master.c4020_demand_master_id%TYPE;
		v_setid 	   t4022_demand_par_detail.c205_part_number_id%TYPE;
		v_setactiontype t4022_demand_par_detail.c4022_par_value%TYPE;
		v_dmapping_id  t4021_demand_mapping.c4021_demand_mapping_id%TYPE;
		v_month 	   t4021_demand_mapping.c4021_stack_month%TYPE;
	--
	BEGIN
		IF v_strlen > 0
		THEN
			WHILE INSTR (v_string, '|') <> 0
			LOOP
				--
				v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
				v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
				--
				v_setid 	:= NULL;
				v_demandmasterid := NULL;
				v_setactiontype := NULL;
				--
				v_setid 	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_demandmasterid := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_setactiontype := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_month 	:= v_substring;

				IF (v_setid = 'null')	--update and insert only for item
				THEN
					UPDATE t4021_demand_mapping
					   SET c901_action_type = v_setactiontype
						 , c4021_stack_month = v_month
					 WHERE c4020_demand_master_id = v_demandmasterid AND c901_ref_type = 40034;   --item

					IF (SQL%ROWCOUNT = 0)
					THEN
						SELECT s4021_demand_mapping.NEXTVAL
						  INTO v_dmapping_id
						  FROM DUAL;

						INSERT INTO t4021_demand_mapping
									(c4021_demand_mapping_id, c4020_demand_master_id, c901_ref_type, c4021_ref_id
								   , c901_action_type, c4021_stack_month
									)
							 VALUES (v_dmapping_id, v_demandmasterid, 40034, '-9999'
								   , v_setactiontype, v_month
									);
					END IF;
				END IF;

				UPDATE t4021_demand_mapping
				   SET c901_action_type = v_setactiontype
					 , c4021_stack_month = v_month
				 WHERE c4021_ref_id = v_setid AND c4020_demand_master_id = v_demandmasterid AND c901_ref_type = 40031;	 --set
			END LOOP;
		END IF;
	END gm_fc_sav_setmap;

--
--
/*******************************************************
 * Purpose: Procedure is used to save the crossover parts
			of the given sheets in to Multipart Sheet
 *******************************************************/
--
	PROCEDURE gm_op_sav_multipartsheet (
		p_demand_sheetids	IN	 VARCHAR2
	  , p_userid			IN	 t4045_demand_sheet_assoc.c4045_last_updated_by%TYPE
	)
	AS
		v_multi_sheet_id t4040_demand_sheet.c4040_demand_sheet_id%TYPE;
		v_inventory_id t4040_demand_sheet.c250_inventory_lock_id%TYPE;
		v_multi_sheet_status t4040_demand_sheet.c901_status%TYPE;

		CURSOR cur_crossover_part
		IS
			SELECT t4042.c205_part_number_id pnum, t4042.c4042_qty qty, t4042.c4042_period period
			  FROM t4040_demand_sheet t4040, t4042_demand_sheet_detail t4042, t205_part_number t205
			 WHERE t4040.c4020_demand_master_id NOT IN (SELECT t4045.c4020_demand_master_assoc_id
														  FROM t4045_demand_sheet_assoc t4045
														 WHERE c4040_demand_sheet_id = v_multi_sheet_id)
			   AND t4040.c4040_demand_sheet_id = t4042.c4040_demand_sheet_id
			   AND t4040.c250_inventory_lock_id = v_inventory_id
			   AND t205.c205_part_number_id = t4042.c205_part_number_id
			   AND t205.c205_crossover_fl IS NOT NULL
			   AND t4042.c901_type = 50563
			   AND t4042.c4042_qty >0 -- This condition is to filter the number of parts that are fetched.MNTTASK-8037
			   AND t4040.c4040_demand_sheet_id IN (SELECT *
													 FROM v_in_list);
	BEGIN
		my_context.set_my_inlist_ctx (p_demand_sheetids);

		BEGIN
			SELECT	   t4040.c4040_demand_sheet_id, t4040.c250_inventory_lock_id, t4040.c901_status
				  INTO v_multi_sheet_id, v_inventory_id, v_multi_sheet_status
				  FROM t4040_demand_sheet t4040
				 WHERE t4040.c901_demand_type = '40023'
				   AND t4040.c250_inventory_lock_id = (SELECT MAX (t250.c250_inventory_lock_id)
														 FROM t250_inventory_lock t250
														WHERE c901_lock_type = 20430)
			FOR UPDATE;

			IF v_multi_sheet_status <> 50550
			THEN
				RETURN;
			END IF;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				raise_application_error ('-20099', '');
		END;

		FOR cur_val IN cur_crossover_part
		LOOP
			gm_op_sav_multipart_detail (v_multi_sheet_id, cur_val.pnum, cur_val.qty, cur_val.period, p_userid);
		END LOOP;

		INSERT INTO t4045_demand_sheet_assoc
					(c4045_demand_sheet_assoc_id, c4040_demand_sheet_id, c4020_demand_master_assoc_id, c901_status
				   , c4045_demand_period_dt, c4045_last_updated_by, c4045_last_updated_date)
			SELECT s4045_demand_sheet_assoc.NEXTVAL, v_multi_sheet_id, c4020_demand_master_id, c901_status
				 , t4040.c4040_demand_period_dt, p_userid, TRUNC (CURRENT_DATE)
			  FROM t4040_demand_sheet t4040
			 WHERE t4040.c4040_demand_sheet_id IN (SELECT *
													 FROM v_in_list);
	END gm_op_sav_multipartsheet;

--
--
/*******************************************************
 * Purpose: Procedure is used to save the details of
			of the given part in to Multipart Sheet
 *******************************************************/
--
	PROCEDURE gm_op_sav_multipart_detail (
		p_multi_sheet_id   IN	t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	  , p_pnum			   IN	t4042_demand_sheet_detail.c205_part_number_id%TYPE
	  , p_qty			   IN	t4042_demand_sheet_detail.c4042_qty%TYPE
	  , p_period		   IN	t4042_demand_sheet_detail.c4042_period%TYPE
	  , p_user_id		   IN	t4042_demand_sheet_detail.c4042_updated_by%TYPE
	)
	AS
		v_detail_id    t4042_demand_sheet_detail.c4042_demand_sheet_detail_id%TYPE;
		v_qty NUMBER;
	BEGIN
	       v_qty := ROUND(p_qty,0);
		
			UPDATE t4042_demand_sheet_detail
			   SET c4042_qty = c4042_qty + v_qty -- When enter decimal point in Part Number assembly screen, need to be display in TTP finilyzing screen.
				 , c4042_updated_by = p_user_id
				 , c4042_updated_date = TRUNC (CURRENT_DATE)
			 WHERE  c4040_demand_sheet_id = p_multi_sheet_id
			   AND c205_part_number_id = p_pnum
			   AND c4042_period = p_period
			   AND c901_type = 50563
			   AND c4042_ref_id = '-9999';
			   
		IF (SQL%ROWCOUNT = 0)
		THEN
			
				INSERT INTO t4042_demand_sheet_detail
							(c4042_demand_sheet_detail_id, c4040_demand_sheet_id, c4042_ref_id, c4042_period
						   , c4042_qty, c4042_updated_by, c901_type, c205_part_number_id, c4042_updated_date
							)
					 VALUES (s4042_demand_sheet_detail.NEXTVAL, p_multi_sheet_id, '-9999', p_period
						   , v_qty, p_user_id, 50563, p_pnum, TRUNC (CURRENT_DATE)
							);
		END IF;
	END gm_op_sav_multipart_detail;

--
--
/***********************************************************
 * Purpose: Function to get the demand sheet id's as comma
 *			  seperated string for the given ttp_detail_id
 ***********************************************************/
--
	FUNCTION get_demand_sheet_ids (
		p_ttp_detail_id   IN   t4040_demand_sheet.c4052_ttp_detail_id%TYPE
	)
		RETURN VARCHAR2
	IS
		v_ids		   VARCHAR2 (1000);

		CURSOR cur_part
		IS
			SELECT c4040_demand_sheet_id ID
			  FROM t4040_demand_sheet
			 WHERE c4052_ttp_detail_id = p_ttp_detail_id;
	BEGIN
		FOR cur_val IN cur_part
		LOOP
			v_ids		:= v_ids || cur_val.ID || ',';
		END LOOP;

		RETURN v_ids;
	END get_demand_sheet_ids;

--
/***********************************************************
 * Purpose: Procedure to fetch the Set Mapping Info for launch
 ***********************************************************/
--
	PROCEDURE gm_fch_setmap_for_launch (
		p_demand_master_id	 IN 	  t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_setmapdetails 	 OUT	  TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_setmapdetails
		 FOR
			 SELECT   c4021_demand_mapping_id mappingid, DECODE (c4021_ref_id, '-9999', '', c4021_ref_id) ID
					, c901_ref_type ref_type
					, DECODE (c901_ref_type
							, 40030, get_group_name (c4021_ref_id)
							, 40031, get_set_name (c4021_ref_id)
							, 40034, 'Item Consignment'
							 ) NAME
					, TO_CHAR (c4021_launch_date, get_rule_value('DATEFMT','DATEFORMAT')) launchdt, NULL newdt, c4021_history_fl histfl
					, DECODE (DECODE (c901_ref_type, 40031, gm_pkg_op_set_build.get_shippedset_count (c4021_ref_id), 0)
							, 0, 'N'
							, 'Y'
							 ) setshipped
				 FROM t4021_demand_mapping
				WHERE c4020_demand_master_id = p_demand_master_id
				  AND c901_ref_type IN (40030, 40031, 40034)
				  AND c4021_ref_id IS NOT NULL
			 UNION
			 SELECT   0 mappingid, '' ID, 40034 ref_type, 'Item Consignment' NAME, NULL launchdt, NULL newdt
					, NULL histfl, 'N' setshipped
				 FROM DUAL
				WHERE 0 IN (SELECT COUNT (1)
							  FROM t4021_demand_mapping
							 WHERE c4020_demand_master_id = p_demand_master_id AND c901_ref_type = 40034)	--first time, there's no item consignment
				  AND 0 IN (SELECT DECODE (t4020.c901_demand_type, 40020, 1, 0)
							  FROM t4020_demand_master t4020
							 WHERE t4020.c4020_demand_master_id = p_demand_master_id)
			 ORDER BY ID;
	END gm_fch_setmap_for_launch;

--
/***********************************************************
 * Purpose: Procedure to save the launch date
 ***********************************************************/
--
	PROCEDURE gm_sav_launchdates (
		p_sheetid	 IN   t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_inputstr	 IN   VARCHAR2
	  , p_userid	 IN   t4020_demand_master.c4020_created_by%TYPE
	)
	AS
		v_substring    VARCHAR2 (2000);
		v_strlen	   NUMBER := NVL (LENGTH (p_inputstr), 0);
		v_string	   VARCHAR2 (30000) := p_inputstr;
		v_refid 	   t4021_demand_mapping.c4021_ref_id%TYPE;
		v_reftype	   t4021_demand_mapping.c901_ref_type%TYPE;
		v_new_launchdt VARCHAR2 (10);
		v_new_launchdate DATE;
		v_old_launchdt DATE;
		v_mthdiff	   NUMBER;
		v_demand_sheet_id t4040_demand_sheet.c4040_demand_sheet_id%TYPE;

		CURSOR curr_group_parts
		IS
			SELECT c205_part_number_id pnum
			  FROM t4011_group_detail t4011, t4030_demand_growth_mapping t4030
			 WHERE t4011.c205_part_number_id = t4030.c4030_ref_id
			   AND c901_ref_type = 20295
			   AND c4010_group_id = v_refid
			   AND c4020_demand_master_id = p_sheetid
			   AND c4030_void_fl IS NULL;

		CURSOR curr_consign_parts
		IS
			SELECT c4030_ref_id pnum
			  FROM t4030_demand_growth_mapping t4030
			 WHERE c901_ref_type = 20298 AND c4020_demand_master_id = p_sheetid AND c4030_void_fl IS NULL;
	BEGIN
		IF v_strlen > 0
		THEN
			WHILE INSTR (v_string, '|') <> 0
			LOOP
				v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
				v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
				--
				v_refid 	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_reftype	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_new_launchdt := v_substring;

				--Fetch  the old launch date
				BEGIN
					SELECT c4021_launch_date, TO_DATE (v_new_launchdt, get_rule_value('DATEFMT','DATEFORMAT'))
					  INTO v_old_launchdt, v_new_launchdate
					  FROM t4021_demand_mapping
					 WHERE c4020_demand_master_id = p_sheetid AND c901_ref_type = v_reftype AND c4021_ref_id = v_refid;
				EXCEPTION
					WHEN NO_DATA_FOUND
					THEN
						v_old_launchdt := NULL;

						SELECT TO_DATE (v_new_launchdt, get_rule_value('DATEFMT','DATEFORMAT'))
						  INTO v_new_launchdate
						  FROM DUAL;
				END;

				--update the launch date to new launch date
				UPDATE t4021_demand_mapping
				   SET c4021_launch_date = v_new_launchdate
					 , c4021_last_updated_by = p_userid
					 , c4021_last_updated_date = CURRENT_DATE
				 WHERE c4020_demand_master_id = p_sheetid AND c901_ref_type = v_reftype AND c4021_ref_id = v_refid;

				--If record not available for -9999 then insert for that sheet ID
				IF (SQL%ROWCOUNT = 0 AND v_refid = '-9999')
				THEN
					INSERT INTO t4021_demand_mapping
								(c4021_demand_mapping_id, c4020_demand_master_id, c901_ref_type, c4021_ref_id
							   , c4021_stack_month, c4021_launch_date, c4021_created_by, c4021_created_date
								)
						 VALUES (s4021_demand_mapping.NEXTVAL, p_sheetid, 40034, '-9999'
							   , 0, v_new_launchdate, p_userid, CURRENT_DATE
								);
				END IF;

				-- chk if growth needs to be moved
				IF (v_old_launchdt IS NOT NULL)
				THEN
					SELECT MONTHS_BETWEEN (v_new_launchdate, v_old_launchdt)
					  INTO v_mthdiff
					  FROM DUAL;

					IF (v_mthdiff != 0 AND v_reftype != 40034)
					THEN
						--call prc to move the growth for set and group
						gm_pkg_op_sheet.gm_sav_move_growth (p_sheetid
														  , v_refid
														  , v_old_launchdt
														  , v_new_launchdate
														  , p_userid
														   );

						--move growth for group part details too
						IF v_reftype = 40030
						THEN
							FOR currindex IN curr_group_parts
							LOOP
								gm_pkg_op_sheet.gm_sav_move_growth (p_sheetid
																  , currindex.pnum
																  , v_old_launchdt
																  , v_new_launchdate
																  , p_userid
																   );
							END LOOP;
						END IF;
					END IF;

					IF (v_mthdiff != 0 AND v_reftype = 40034)	-- Item (-9999)
					THEN
						FOR currindex IN curr_consign_parts
						LOOP
							gm_pkg_op_sheet.gm_sav_move_growth (p_sheetid
															  , currindex.pnum
															  , v_old_launchdt
															  , v_new_launchdate
															  , p_userid
															   );
						END LOOP;
					END IF;
				END IF;
			END LOOP;

			-- reload the sheet
			BEGIN
				SELECT c4040_demand_sheet_id
				  INTO v_demand_sheet_id
				  FROM t4040_demand_sheet
				 WHERE c901_status = 50550	 -- open
				   AND c4040_demand_period_dt =
										 TO_DATE (TO_CHAR (CURRENT_DATE, 'mm') || '/' || TO_CHAR (CURRENT_DATE, 'YYYY')
												, 'mm/yyyy')
				   AND c4020_demand_master_id = p_sheetid
				   AND c4040_void_fl IS NULL;
			EXCEPTION
				WHEN NO_DATA_FOUND
				THEN
					v_demand_sheet_id := 0;
			END;

			IF (v_demand_sheet_id != 0)
			THEN
				BEGIN
					gm_pkg_common_cancel.gm_cm_sav_cancelrow
														 (v_demand_sheet_id
														, 90710
														, 'EXEDS'
														, 'Reloading sheet automatically after moving the launch date'
														, p_userid
														 );
				EXCEPTION
					WHEN OTHERS
					THEN
						NULL;
				END;
			END IF;
		END IF;
	END gm_sav_launchdates;

/***********************************************************
 * Purpose: Procedure to move the growth
 ***********************************************************/
--
	PROCEDURE gm_sav_move_growth (
		p_sheetid		 IN   t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_refid 		 IN   t4021_demand_mapping.c4021_ref_id%TYPE
	  , v_old_launchdt	 IN   t4031_growth_details.c4031_start_date%TYPE
	  , v_new_launchdt	 IN   t4031_growth_details.c4031_end_date%TYPE
	  , p_userid		 IN   t4020_demand_master.c4020_created_by%TYPE
	)
	AS
		v_moveby	   NUMBER;
		v_abs_moveby   NUMBER;
		v_tot_recs	   NUMBER := 0;
		v_current_rowindex NUMBER := 0;
		v_demand_growth_id t4030_demand_growth_mapping.c4030_demand_growth_id%TYPE;
		v_growth_details_id t4031_growth_details.c4031_growth_details_id%TYPE;
		v_current_startdate t4031_growth_details.c4031_start_date%TYPE;
		v_start_date   t4031_growth_details.c4031_start_date%TYPE;
		v_end_date	   t4031_growth_details.c4031_end_date%TYPE;
		v_type		   t4030_demand_growth_mapping.c901_ref_type%TYPE;
		v_message	   VARCHAR2 (100);

		CURSOR cur_tmptbl_details
		IS
			SELECT	 TO_DATE (my_temp_txn_id, get_rule_value('DATEFMT','DATEFORMAT')) sdate, to_char(my_temp_txn_key) refvalue, to_char(my_temp_txn_value) reftype
				FROM my_temp_key_value
			ORDER BY TO_DATE (my_temp_txn_id, get_rule_value('DATEFMT','DATEFORMAT'));

		CURSOR cur_set_request_details
		IS
			SELECT c520_request_to reqto, c520_request_id reqid, c520_required_date reqdate, c901_ship_to shipto
				 , c520_ship_to_id shiptoid
			  FROM t520_request
			 WHERE c520_request_txn_id = p_sheetid
			   AND c901_request_source = 50616
			   AND c207_set_id = p_refid
			   AND c520_void_fl IS NULL
			   AND c520_status_fl < 40
			   AND c520_master_request_id IS NULL;

		CURSOR cur_item_request_details
		IS
			SELECT c520_request_to reqto, t520.c520_request_id reqid, c520_required_date reqdate, c901_ship_to shipto
				 , c520_ship_to_id shiptoid
			  FROM t520_request t520, t521_request_detail t521
			 WHERE t520.c520_request_id = t521.c520_request_id
			   AND t521.c205_part_number_id = p_refid
			   AND c207_set_id IS NULL
			   AND c520_void_fl IS NULL
			   AND c520_master_request_id IS NULL
			   AND c901_request_source = '50616'   --order planning
			   AND c520_request_to IS NULL
			   AND c520_status_fl < 40;
	BEGIN
		SELECT MONTHS_BETWEEN (v_new_launchdt, v_old_launchdt)
		  INTO v_moveby
		  FROM DUAL;

		DELETE FROM my_temp_key_value;

		INSERT INTO my_temp_key_value
			SELECT	 TO_CHAR (t4031.c4031_start_date, get_rule_value('DATEFMT','DATEFORMAT')) sdate, t4031.c4031_value refvalue
				   , t4031.c901_ref_type reftype
				FROM t4030_demand_growth_mapping t4030, t4031_growth_details t4031
			   WHERE t4030.c4030_demand_growth_id = t4031.c4030_demand_growth_id
				 AND t4030.c4020_demand_master_id = p_sheetid
				 AND t4030.c4030_ref_id = p_refid
				 AND t4031.c4031_start_date >= v_old_launchdt
				 AND t4030.c4030_void_fl IS NULL
				 AND t4031.c4031_void_fl IS NULL
				 AND t4031.c4031_value IS NOT NULL
			ORDER BY t4031.c4031_start_date;

		SELECT COUNT (1), ABS (v_moveby)
		  INTO v_tot_recs, v_abs_moveby
		  FROM my_temp_key_value;

		IF (v_tot_recs != 0)
		THEN
			SELECT c4030_demand_growth_id, c901_ref_type
			  INTO v_demand_growth_id, v_type
			  FROM t4030_demand_growth_mapping t4030
			 WHERE t4030.c4020_demand_master_id = p_sheetid AND t4030.c4030_ref_id = p_refid AND c4030_void_fl IS NULL;

			FOR currindex IN cur_tmptbl_details
			LOOP
				v_current_rowindex := v_current_rowindex + 1;

				SELECT ADD_MONTHS (currindex.sdate, v_moveby), LAST_DAY (ADD_MONTHS (currindex.sdate, v_moveby))
				  INTO v_start_date, v_end_date
				  FROM DUAL;

				BEGIN
					SELECT c4031_growth_details_id
					  INTO v_growth_details_id
					  FROM t4031_growth_details
					 WHERE c4030_demand_growth_id = v_demand_growth_id
					   AND c4031_start_date = v_start_date
					   AND c4031_void_fl IS NULL;
				EXCEPTION
					WHEN NO_DATA_FOUND
					THEN
						v_growth_details_id := 0;
				END;

				--		Update the growth value
				UPDATE t4031_growth_details
				   SET c4031_value = currindex.refvalue
					 , c901_ref_type = currindex.reftype
					 , c4031_last_updated_by = p_userid
					 , c4031_last_updated_date = CURRENT_DATE
				 WHERE c4031_start_date = v_start_date
				   AND c4030_demand_growth_id = v_demand_growth_id
				   AND c4031_void_fl IS NULL;

				IF (SQL%ROWCOUNT = 0)
				THEN
					SELECT s4031_growth.NEXTVAL
					  INTO v_growth_details_id
					  FROM DUAL;

					INSERT INTO t4031_growth_details
								(c4031_growth_details_id, c4031_value, c901_ref_type, c4031_start_date, c4031_end_date
							   , c4031_created_by, c4031_created_date, c4030_demand_growth_id
								)
						 VALUES (v_growth_details_id, currindex.refvalue, currindex.reftype, v_start_date, v_end_date
							   , p_userid, CURRENT_DATE, v_demand_growth_id
								);
				END IF;

				-- enter comments manually
				gm_update_log (v_growth_details_id
							 , 'Moving growth as launch date moved from ' || v_old_launchdt || ' to ' || v_new_launchdt
							 , 1224
							 , p_userid
							 , v_message
							  );

				--If moving fwd then for first v_moveby months make value as null
				IF v_moveby > 0
				THEN
					IF v_current_rowindex <= v_moveby
					THEN
						SELECT c4031_growth_details_id
						  INTO v_growth_details_id
						  FROM t4031_growth_details
						 WHERE c4030_demand_growth_id = v_demand_growth_id
						   AND c4031_start_date = currindex.sdate
						   AND c4031_void_fl IS NULL;

						UPDATE t4031_growth_details
						   SET c4031_value = 0
							 , c901_ref_type = currindex.reftype
							 , c4031_last_updated_by = p_userid
							 , c4031_last_updated_date = CURRENT_DATE
						 WHERE c4031_start_date = currindex.sdate
						   AND c4030_demand_growth_id = v_demand_growth_id
						   AND c4031_void_fl IS NULL;

						-- enter comments manually
						gm_update_log (v_growth_details_id
									 ,	  'Moving growth as launch date moved from '
									   || v_old_launchdt
									   || ' to '
									   || v_new_launchdt
									 , 1224
									 , p_userid
									 , v_message
									  );
					END IF;
				ELSIF v_moveby < 0
				THEN   -- If moving backword then for last v_moveby months make value as null
					IF (v_tot_recs - v_current_rowindex) < v_abs_moveby
					THEN
						SELECT c4031_growth_details_id
						  INTO v_growth_details_id
						  FROM t4031_growth_details
						 WHERE c4030_demand_growth_id = v_demand_growth_id
						   AND c4031_start_date = currindex.sdate
						   AND c4031_void_fl IS NULL;

						UPDATE t4031_growth_details
						   SET c4031_value = 0
							 , c901_ref_type = currindex.reftype
							 , c4031_last_updated_by = p_userid
							 , c4031_last_updated_date = CURRENT_DATE
						 WHERE c4031_start_date = currindex.sdate
						   AND c4030_demand_growth_id = v_demand_growth_id
						   AND c4031_void_fl IS NULL;

						-- enter comments manually
						gm_update_log (v_growth_details_id
									 ,	  'Moving growth as launch date moved from '
									   || v_old_launchdt
									   || ' to '
									   || v_new_launchdt
									 , 1224
									 , p_userid
									 , v_message
									  );
					END IF;
				END IF;
			END LOOP;

			--	if type set/item Update C520_REQUIRED_DATE
			IF v_type = '20296'   -- set
			THEN
				FOR currindex IN cur_set_request_details
				LOOP
					gm_pkg_op_request_master.gm_sav_request_edit_info (TO_CHAR (ADD_MONTHS (currindex.reqdate, v_moveby)
																			  , get_rule_value('DATEFMT','DATEFORMAT')
																			   )
																	 , currindex.reqto
																	 , p_userid
																	 , currindex.reqid
																	 , currindex.shipto
																	 , currindex.shiptoid
																	  );
				END LOOP;
			END IF;

			IF v_type = '20298'   -- item
			THEN
				FOR currindex IN cur_item_request_details
				LOOP
					gm_pkg_op_request_master.gm_sav_request_edit_info (TO_CHAR (ADD_MONTHS (currindex.reqdate, v_moveby)
																			  , get_rule_value('DATEFMT','DATEFORMAT')
																			   )
																	 , currindex.reqto
																	 , p_userid
																	 , currindex.reqid
																	 , currindex.shipto
																	 , currindex.shiptoid
																	  );
				END LOOP;
			END IF;
		END IF;
	END gm_sav_move_growth;

--
/***********************************************************
 * Purpose: Function to get the launch date when demandsheetid , refid and reftype is passed
 ***********************************************************/
--
	FUNCTION get_launch_date (
		p_sheetid	IN	 t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_refid 	IN	 t4021_demand_mapping.c4021_ref_id%TYPE
	  , p_reftype	IN	 t4021_demand_mapping.c901_ref_type%TYPE
	)
		RETURN DATE
	IS
		v_date		   DATE;
		v_reftype	   t4021_demand_mapping.c901_ref_type%TYPE;
		v_refid 	   t4021_demand_mapping.c4021_ref_id%TYPE;
	BEGIN
		v_refid 	:= p_refid;

		IF p_reftype = '20296'	 --setid
		THEN
			v_reftype	:= 40031;
		ELSIF p_reftype = '20297'	--group id
		THEN
			v_reftype	:= 40030;
		ELSIF p_reftype = '20298'	--Part # - Consignment
		THEN
			v_reftype	:= 40034;
			v_refid 	:= '-9999';
		ELSIF p_reftype = '20295'
		-- Part # - Group
		THEN
			-- fetch groupid
			v_reftype	:= 40030;

			BEGIN
				SELECT c4010_group_id
				  INTO v_refid
				  FROM t4011_group_detail t4011, t4021_demand_mapping t4021
				 WHERE c205_part_number_id = p_refid
				   AND c4020_demand_master_id = p_sheetid
				   AND t4011.c4010_group_id = t4021.c4021_ref_id;
			EXCEPTION
				WHEN NO_DATA_FOUND
				THEN
					v_refid 	:= p_refid;
			END;
		END IF;

		BEGIN
			SELECT c4021_launch_date
			  INTO v_date
			  FROM t4021_demand_mapping
			 WHERE c4020_demand_master_id = p_sheetid AND c901_ref_type = v_reftype AND c4021_ref_id = v_refid;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				RETURN NULL;
		END;

		RETURN v_date;
	END get_launch_date;

	/*******************************************************
	* Description : Procedure to fetch Multiple parts Group
	* Author		: Xun
	*******************************************************/
	PROCEDURE gm_fch_multi_part_grp (
		p_pnum	   IN		t4011_group_detail.c205_part_number_id%TYPE
	  , p_detail   OUT		TYPES.cursor_type
	)
	AS
	
	v_company_id   t1900_company.c1900_company_id%TYPE;
	
	BEGIN
	
		--Getting company id from context.   
    SELECT get_compid_frm_cntx()
	  INTO v_company_id 
	  FROM dual;
	
		OPEN p_detail
		 FOR
		 -- When click Shared part link, Before one portal ,it will display  List,AD,VP,TW price in popup. If priced type is Individual means should get price from part number table.
		 -- Now for both the Individual and Group priced type will get the price from the T2052_part_price_mapping table based on the company.
				SELECT   pnum, get_partnum_desc (pnum), groupname, c4010_group_id, parttype
				, listprice
            	, adlimit
            	, vplimit
            	, tripwire
				 FROM (SELECT a.pnum, groupname, a.c4010_group_id, parttype,c.listprice,c.adlimit,c.vplimit,c.tripwire,pricedtype
						 FROM (SELECT t4011.c205_part_number_id pnum, get_group_name (t4011.c4010_group_id) groupname,c901_priced_type pricedtype
									, t4011.c4010_group_id, get_code_name (t4011.c901_part_pricing_type) parttype
								 FROM t4011_group_detail t4011, t4010_group t4010
								WHERE t4011.c205_part_number_id = p_pnum
								  AND t4010.c4010_group_id = t4011.c4010_group_id
								  AND t4010.c4010_void_fl IS NULL
								  AND t4010.c901_type = 40045) a
							, (SELECT T2052.C205_PART_NUMBER_ID pnum,T2052.C2052_LIST_PRICE listprice,T2052.C2052_CONSIGNMENT_PRICE vplimit,
						             T2052.C2052_LOANER_PRICE adlimit,T2052.C2052_EQUITY_PRICE tripwire
						       FROM t205_part_number  T205,T2052_PART_PRICE_MAPPING T2052 WHERE T205.c205_active_fl = 'Y'
						        AND T2052.C205_PART_NUMBER_ID = p_pnum
								AND T205.C205_PART_NUMBER_ID  = T2052.C205_PART_NUMBER_ID
								AND T2052.C1900_COMPANY_ID    = v_company_id
								AND T2052.C2052_VOID_FL      IS NULL) c
						      WHERE  c.pnum = a.pnum)
			 GROUP BY pnum, groupname, c4010_group_id, parttype,pricedtype,listprice ,adlimit ,vplimit ,tripwire;
	END gm_fch_multi_part_grp;

	/******************************************************************
	 Description : This procedure is used to fetch group part history
	****************************************************************/
	PROCEDURE gm_fch_group_part_history (
		p_pnum		  IN	   t7011_part_price_log.c205_part_number_id%TYPE
	  , p_outdetail   OUT	   TYPES.cursor_type
	)
	AS
	
	v_company_id   t1900_company.c1900_company_id%TYPE;
	
	BEGIN
	
	--Getting company id from context.   
    SELECT get_compid_frm_cntx()
	  INTO v_company_id 
	  FROM dual;
	
	-- When clicking on the Part History icon , this below query will fetch the details of part belongs to group with the pricing type(Primary/Secondary)
	-- and fetch the price from the T2052_part_price_mapping table based on the company.
		OPEN p_outdetail
		 FOR
			SELECT   groupnm, partnum, primary_secondary, listprice
					, adlimit
					, vplimit
					, tripwire, logtype, created_by, created_date
					, action_dates, action_date, action_by
				 FROM (SELECT get_group_name (t7011.c4010_group_id) groupnm, t7011.c205_part_number_id partnum
							, get_code_name (t7011.c901_part_pricing_type) primary_secondary
							, get_code_name (t7011.c901_log_type) logtype
							, get_user_name (t7011.c7011_created_by) created_by
							, TO_CHAR (t7011.c7011_created_date, get_rule_value('DATEFMT','DATEFORMAT')||' HH12:MI:SS AM') created_date
							, t7012.c901_price_type pricetype, t7012.c7012_price price
							, t2052.c2052_list_price listprice
					        , t2052.c2052_equity_price tripwire
					        , T2052.C2052_LOANER_PRICE ADLIMIT
					        , t2052.c2052_consignment_price vplimit 
							, TO_CHAR (t7011.c7011_action_date, get_rule_value('DATEFMT','DATEFORMAT')||' HH12:MI:SS AM') action_date
							, t7011.c7011_action_date action_dates, get_user_name (t7011.c7011_action_by) action_by
						 FROM t7011_part_price_log t7011, t7012_part_price_log_detail t7012, t2052_part_price_mapping t2052
						WHERE t7011.c7011_part_price_log_id = t7012.c7011_part_price_log_id(+)
						  AND t7011.c205_part_number_id       = t2052.c205_part_number_id
					      AND T2052.C2052_VOID_FL            IS NULL
					      AND T2052.C1900_COMPANY_ID          = v_company_id
						  AND t7011.c205_part_number_id = p_pnum
						  AND t7011.c7011_void_fl IS NULL)
			 GROUP BY groupnm
					, partnum
					, primary_secondary
					, listprice
					, adlimit
					, vplimit
					, tripwire 
					, logtype
					, created_by
					, created_date
					, action_date
					, action_by
					, action_dates
			 ORDER BY action_dates DESC;
	END gm_fch_group_part_history;

		/******************************************************************
	 Description : This procedure is used to fetch all group part history
	****************************************************************/
	PROCEDURE gm_fch_all_group_part_history (
		p_groupid	  IN	   t7011_part_price_log.c4010_group_id%TYPE
	  , p_outdetail   OUT	   TYPES.cursor_type
	)
	AS
	
	v_company_id   t1900_company.c1900_company_id%TYPE;
	
	BEGIN
	
	--Getting company id from context.   
    SELECT get_compid_frm_cntx()
	  INTO v_company_id 
	  FROM dual;
	
	-- When click on the View All Part History icon, The below query will fetch the details of the parts in that group 
	-- and fetch the price from the T2052_part_price_mapping table based on the company.
	
		OPEN p_outdetail
		 FOR
			  SELECT   groupnm, partnum, primary_secondary, listprice
					, adlimit
					, vplimit
					, tripwire, logtype, created_by, created_date
					, action_dates, action_date, action_by
				 FROM (SELECT get_group_name (t7011.c4010_group_id) groupnm, t7011.c205_part_number_id partnum
							, get_code_name (t7011.c901_part_pricing_type) primary_secondary
							, get_code_name (t7011.c901_log_type) logtype
							, get_user_name (t7011.c7011_created_by) created_by
							, TO_CHAR (t7011.c7011_created_date, get_rule_value('DATEFMT','DATEFORMAT')||' HH12:MI:SS AM') created_date
							, t7012.c901_price_type pricetype, t7012.c7012_price price
							, t2052.c2052_list_price listprice
							, t2052.c2052_equity_price tripwire
							, t2052.c2052_loaner_price adlimit
							, t2052.c2052_consignment_price vplimit 
							, TO_CHAR (t7011.c7011_action_date, get_rule_value('DATEFMT','DATEFORMAT')||' HH12:MI:SS AM') action_date
							, t7011.c7011_action_date action_dates, get_user_name (t7011.c7011_action_by) action_by
						 FROM t7011_part_price_log t7011, t7012_part_price_log_detail t7012, t2052_part_price_mapping t2052
						WHERE t7011.c7011_part_price_log_id = t7012.c7011_part_price_log_id(+)
						  AND t7011.c205_part_number_id = t2052.c205_part_number_id
				          AND T2052.C2052_VOID_FL IS NULL
				          AND T2052.C1900_COMPANY_ID = v_company_id
						  AND t7011.c4010_group_id = p_groupid
						  AND t7011.c7011_void_fl IS NULL)
			 GROUP BY groupnm
					, partnum
					, primary_secondary
					, listprice
				    , adlimit
				    , vplimit
				    , tripwire 
					, logtype
					, created_by
					, created_date
					, action_date
					, action_by
					, action_dates
			 ORDER BY action_dates DESC;
	END gm_fch_all_group_part_history;

	 /******************************************************************
	* Description : Function to get part_pricing_type
	****************************************************************/
	FUNCTION get_pricing_type (
		p_pnum					  IN   t4011_group_detail.c205_part_number_id%TYPE
	  , p_primary_part_inputstr   IN   VARCHAR2
	)
		RETURN NUMBER
	IS
/*	Description 	: THIS FUNCTION RETURNS part pricing type in group detail table
 Parameters 		: p_pnum, p_inputstr --- ex 101.109^52081|101.111^52080
*/
		v_strlen_primary_part NUMBER := NVL (LENGTH (p_primary_part_inputstr), 0);
		v_primary_part_string VARCHAR2 (4000) := p_primary_part_inputstr;
		v_primary_part_substring VARCHAR2 (1000);
		v_groupid	   t4010_group.c4010_group_id%TYPE;
		v_pnum		   t4011_group_detail.c205_part_number_id%TYPE;
		v_part_type    t4011_group_detail.c901_part_pricing_type%TYPE;
		v_part_primary_lock t4011_group_detail.c4011_primary_part_lock_fl%TYPE;
		v_pricing_type NUMBER;
	BEGIN
		IF v_strlen_primary_part > 0
		THEN
			WHILE INSTR (v_primary_part_string, '|') <> 0
			LOOP
				--
				v_primary_part_substring := SUBSTR (v_primary_part_string, 1, INSTR (v_primary_part_string, '|') - 1);
				v_primary_part_string := SUBSTR (v_primary_part_string, INSTR (v_primary_part_string, '|') + 1);
				--
				v_groupid	:= NULL;
				v_pnum		:= NULL;
				v_part_type := NULL;
				v_part_primary_lock := NULL;
				--
				v_groupid	:= SUBSTR (v_primary_part_substring, 1, INSTR (v_primary_part_substring, '^') - 1);
				v_primary_part_substring := SUBSTR (v_primary_part_substring, INSTR (v_primary_part_substring, '^') + 1);
				v_pnum		:= SUBSTR (v_primary_part_substring, 1, INSTR (v_primary_part_substring, '^') - 1);
				v_primary_part_substring := SUBSTR (v_primary_part_substring, INSTR (v_primary_part_substring, '^') + 1);
				v_part_type := SUBSTR (v_primary_part_substring, 1, INSTR (v_primary_part_substring, '^') - 1);
				v_primary_part_substring := SUBSTR (v_primary_part_substring, INSTR (v_primary_part_substring, '^') + 1);
				v_part_primary_lock := v_primary_part_substring;

				--check to see if part will be changed from primary to secondary
				IF (p_pnum = v_pnum)
				THEN
					RETURN v_part_type;
				END IF;
			END LOOP;

			RETURN 52080;
		ELSE
			RETURN 52080;
		END IF;
	END get_pricing_type;

	 /**************************************************************************************
	* Description : Function to get validation value (if part and group price is the same
	***************************************************************************************/
	FUNCTION get_validation_grp_part (
		p_pnum			IN	 t4011_group_detail.c205_part_number_id%TYPE
	  , p_priced_type	IN	 t4010_group.c901_priced_type%TYPE
	  , p_group_price	IN	 t7010_group_price_detail.c7010_price%TYPE
	)
		RETURN VARCHAR2
	IS
		v_part_price   VARCHAR2 (20) := '';
		v_group_id	   t4010_group.c4010_group_id%TYPE;
		v_pnum		   t4011_group_detail.c205_part_number_id%TYPE;
	BEGIN
		v_group_id	:= get_primary_part_groupid (p_pnum);
		
		-- if its priced individually dont perform the validation 
		if ( p_priced_type = 52111) 
		THEN
			RETURN 'Y';
		END IF;

		IF (v_group_id != 0 AND p_priced_type = 52110)	 ---group
		THEN
			v_part_price := TO_CHAR (get_group_list_price (v_group_id));
		ELSE
			v_part_price := get_part_price_list (p_pnum, '');
		END IF;

		IF (TO_CHAR (p_group_price) != v_part_price OR v_part_price != '')
		THEN
			RETURN 'N';
		ELSE
			RETURN 'Y';
		END IF;
	END get_validation_grp_part;

	/********************************************************************
		* Description : Function to get part list price in the group
		******************************************************************/
	FUNCTION get_group_list_price (
		p_group_id	 t4010_group.c4010_group_id%TYPE
	)
		RETURN NUMBER
	IS
		v_price 	   NUMBER;
	BEGIN
		BEGIN
			SELECT t7010.c7010_price
			  INTO v_price
			  FROM t7010_group_price_detail t7010
			 WHERE t7010.c4010_group_id = p_group_id AND t7010.c901_price_type = 52060 AND t7010.c7010_void_fl IS NULL;   --list price
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				RETURN 0;
		END;

		RETURN v_price;
	END get_group_list_price;

	/********************************************************************
		* Description : Function to get part list price in the group
		******************************************************************/
	FUNCTION get_primary_part_groupid (
		p_pnum	 IN   t4011_group_detail.c205_part_number_id%TYPE
	)
		RETURN NUMBER
	IS
		v_groupid	   NUMBER;
	BEGIN
		BEGIN
			SELECT t4010.c4010_group_id
			  INTO v_groupid
			  FROM t4011_group_detail t4011, t4010_group t4010
			 WHERE  T4010.C4010_GROUP_ID = t4011.C4010_GROUP_ID AND c205_part_number_id = p_pnum AND c901_part_pricing_type = 52080 AND T4010.C4010_VOID_FL IS NULL;
		--list price
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				RETURN 0;
		END;

		RETURN v_groupid;
	END get_primary_part_groupid;

	/************************************************************************************************************
	 Description	: THIS FUNCTION RETURNS part primary lock flag from the input string for a given part number
	 Parameters 		: p_pnum, p_inputstr --- ex 101.109^52081^Y|101.111^52080^N
	**************************************************************************************************************/
	FUNCTION get_primary_lock_from_input (
		p_pnum					  IN   t4011_group_detail.c205_part_number_id%TYPE
	  , p_primary_part_inputstr   IN   VARCHAR2
	)
		RETURN VARCHAR2
	IS
		v_strlen_primary_part NUMBER := NVL (LENGTH (p_primary_part_inputstr), 0);
		v_primary_part_string VARCHAR2 (4000) := p_primary_part_inputstr;
		v_primary_part_substring VARCHAR2 (1000);
		v_groupid	   t4010_group.c4010_group_id%TYPE;
		v_pnum		   t4011_group_detail.c205_part_number_id%TYPE;
		v_part_type    t4011_group_detail.c901_part_pricing_type%TYPE;
		v_part_primary_lock t4011_group_detail.c4011_primary_part_lock_fl%TYPE;
		v_pricing_type NUMBER;
	BEGIN
		IF v_strlen_primary_part > 0
		THEN
			WHILE INSTR (v_primary_part_string, '|') <> 0
			LOOP
				--
				v_primary_part_substring := SUBSTR (v_primary_part_string, 1, INSTR (v_primary_part_string, '|') - 1);
				v_primary_part_string := SUBSTR (v_primary_part_string, INSTR (v_primary_part_string, '|') + 1);
				--
				v_groupid	:= NULL;
				v_pnum		:= NULL;
				v_part_type := NULL;
				v_part_primary_lock := NULL;
				--
				v_groupid	:= SUBSTR (v_primary_part_substring, 1, INSTR (v_primary_part_substring, '^') - 1);
				v_primary_part_substring := SUBSTR (v_primary_part_substring, INSTR (v_primary_part_substring, '^') + 1);
				v_pnum		:= SUBSTR (v_primary_part_substring, 1, INSTR (v_primary_part_substring, '^') - 1);
				v_primary_part_substring := SUBSTR (v_primary_part_substring, INSTR (v_primary_part_substring, '^') + 1);
				v_part_type := SUBSTR (v_primary_part_substring, 1, INSTR (v_primary_part_substring, '^') - 1);
				v_primary_part_substring := SUBSTR (v_primary_part_substring, INSTR (v_primary_part_substring, '^') + 1);
				v_part_primary_lock := v_primary_part_substring;

				--check to see if part will be changed from primary to secondary
				IF (p_pnum = v_pnum)
				THEN
					RETURN v_part_primary_lock;
				END IF;
			END LOOP;

			RETURN 'Y';
		ELSE
			RETURN 'Y';
		END IF;
	END get_primary_lock_from_input;
END;
/
