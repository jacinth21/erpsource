/* Formatted on 2008/04/09 16:48 (Formatter Plus v4.8.5) */
-- @"c:\database\packages\operations\gm_pkg_op_msng_chrg.bdy"

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_msng_chrg
IS
--
  /**********************************************************************
   * Description : Procedure to fetch Missing Charge Details
   * Author      : VPrasath
  **********************************************************************/
 PROCEDURE gm_fch_msng_chrg_dtls (
      p_req_id		IN	t525_product_request.c525_product_request_id%TYPE
	, p_dist_id		IN	t701_distributor.c701_distributor_id%TYPE
	, p_rep_id		IN	t525_product_request.c525_request_for_id%TYPE
	, p_status      IN  t9201_charge_details.c9201_status%TYPE
	, p_screen      IN  VARCHAR2
	, p_dist_type   IN  VARCHAR2
	, p_from_dt		IN	t504a_loaner_transaction.c504a_return_dt%TYPE
	, p_to_dt		IN	t504a_loaner_transaction.c504a_return_dt%TYPE
	, p_recon_cmnt	IN	t9201_charge_details.c901_recon_comments%TYPE
	, p_cursor_out	OUT	TYPES.CURSOR_TYPE 
) AS
	  v_from_dt DATE;
	  v_to_dt   DATE;
	  v_screen VARCHAR2(50);
	  v_dist_type VARCHAR2(50);
	  v_company_id  t1900_company.c1900_company_id%TYPE;
	  v_datefmt VARCHAR2(10);
	  v_plant_id T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
  	  v_filter T906_RULES.C906_RULE_VALUE%TYPE;
BEGIN
	
	select get_compid_frm_cntx(),NVL(get_compdtfmt_frm_cntx(),get_rule_value('DATEFMT','DATEFORMAT')),get_plantid_frm_cntx() into v_company_id,v_datefmt,v_plant_id from dual;
	
	 SELECT DECODE(GET_RULE_VALUE(v_company_id,'MISSING_PART_CHARGES'), null, v_company_id, v_plant_id) --MISSING_PART_CHARGES- To fetch the records for EDC entity countries based on plant
       INTO v_filter 
       FROM DUAL;
	
	IF TRIM(p_screen) IS NULL 
	THEN
		v_screen := 'MissingCharges';
		v_dist_type := '1,70100|2,70101|';
	ELSE 
		v_screen := p_screen;
		v_dist_type := p_dist_type;
	END IF;	

	
	my_context.set_double_inlist_ctx(v_dist_type);
	
	OPEN p_cursor_out 
	   FOR
	SELECT ih_txn.*
		 , charges.* 
		 , NVL(ih_txn.ih_charge_qty,charges.qty_missing) charge_qty
	 	 , TO_CHAR(DECODE (NVL(charges.status_fl,-1), 25,(-1 * NVL(charges.edit_chrg,0))
	 	  		,  DECODE(v_screen,'MissingCharges',NVL(ih_txn.ih_charge,0),NVL(charges.edit_chrg,0)))) charge
	 FROM 
		(SELECT t9800.c9800_ticket_id tkt_id
		      , t701.c701_distributor_id dist_id 
		      , t701.c701_distributor_name dist_name
		      , t504a.c703_sales_rep_id rep_id
		      , get_rep_name(t504a.c703_sales_rep_id ) rep_name
		      , t504a.c703_ass_rep_id assocrepid
		      , get_rep_name(t504a.c703_ass_rep_id ) assocrepnm
		      , t525.c525_product_request_id req_id
		      , t504a.c504a_return_dt incident_dt
		      , t504a.c504_consignment_id cn_id
		      , get_set_name(t526.c207_set_id) set_nm
		      , t504.c504a_etch_id etch_id
		      , ih_txn.c205_part_number_id p_s_num
		      , get_partnum_desc(ih_txn.c205_part_number_id) pdesc
		      , ih_txn.missing_qty 
		      , ih_txn.reconciled_qty		      
		      , DECODE(v_screen,'MissingCharges', ih_txn.charge_qty, NULL) ih_charge_qty 
		      , TO_NUMBER(get_part_price(ih_txn.c205_part_number_id, 'L')) list_price
		      , ih_txn.charge ih_charge
		      , t701.c901_distributor_type dist_type
		      , t504a.c526_product_request_detail_id req_dtl_id
		      , get_log_flag(t504a.c526_product_request_detail_id,4000316) reconcmtcnt 
		      , c412_inhouse_trans_id trans_id
		      , t526.c207_set_id set_id     
		   FROM t504a_loaner_transaction t504a 
		      , t526_product_request_detail t526
		      , t9800_jira_ticket t9800       
		      , t525_product_request t525
		      , t504a_consignment_loaner t504
		      , t701_distributor t701
              , (SELECT t412.c412_inhouse_trans_id
			          , t413.c205_part_number_id 
			          , t412.c504a_loaner_transaction_id    
			          , SUM(DECODE(t413.c413_status_fl,'Q',t413.c413_item_qty,0)) missing_qty
			          , SUM(DECODE(t413.c413_status_fl,'Q',0,t413.c413_item_qty)) reconciled_qty
			          , SUM(DECODE(t413.c413_status_fl,'Q',t413.c413_item_qty,0)) - 
			            SUM(DECODE(t413.c413_status_fl,'Q',0,t413.c413_item_qty)) charge_qty       
			          , gm_pkg_cm_loaner_jobs.get_missing_part_price(t413.c205_part_number_id) charge			         
		           FROM t412_inhouse_transactions t412
		              , t413_inhouse_trans_items t413 
		          WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id          
		            AND t412.c412_type = 50159
		            AND (     c412_status_fl  = DECODE(v_screen,'MissingCharges',1,c412_status_fl)
		                   OR c412_status_fl  = DECODE(v_screen,'MissingCharges',2,c412_status_fl)
		                   OR c412_status_fl  = DECODE(v_screen,'MissingCharges',3,c412_status_fl)
		                 ) 
		            AND c412_void_fl IS NULL
		            AND t413.c413_void_fl IS NULL
		            AND t413.c205_part_number_id NOT IN (
		                     SELECT c205_part_number_id 
		                       FROM t208_set_details 
		                      WHERE c207_set_id IN (   SELECT c906_rule_value
		                                                FROM t906_rules
		                                               WHERE c906_rule_grp_id = 'CHARGES'
		                                                 AND c906_rule_id     = 'SETS'
		                                                 AND c906_void_fl IS NULL
		                                            )    
		                       AND c208_void_fl IS NULL
		                    )
		       GROUP BY t412.c412_inhouse_trans_id
		              , t413.c205_part_number_id 
		              , t412.c504a_loaner_transaction_id                   
		         ) ih_txn
		   WHERE  ih_txn.c504a_loaner_transaction_id = t504a.c504a_loaner_transaction_id
		     AND t504a.c526_product_request_detail_id = t526.c526_product_request_detail_id 
		     AND ((t526.c525_product_request_id = t9800.c9800_ref_id  and t9800.c9800_ref_type=110521)  OR (t526.c526_product_request_detail_id=t9800.c9800_ref_id and  t9800.c9800_ref_type=10056)) --110521 Parent Request, 10056 Child request PC-918 -Loaner Reconciliation - Auto Generating unique CSTKT
		     AND t525.c525_product_request_id = t526.c525_product_request_id
		     AND t504.c504_consignment_id = t504a.c504_consignment_id
		     AND t504a.c504a_consigned_to_id = t701.c701_distributor_id
		     AND t504a.c901_consigned_to = 50170 
		     AND ih_txn.charge > 0
		     AND t504a.c504a_void_fl IS NULL
		     AND t526.c526_void_fl IS NULL
		     AND t9800.c9800_void_fl IS NULL
		     AND t525.c525_void_fl IS NULL
		     AND t504.c504a_void_fl IS NULL   
		     AND t701.c701_void_fl IS NULL
		     AND (t525.c1900_company_id = v_filter
		     OR t525.c5040_plant_id   = v_filter)
  			 AND t525.c1900_company_id = t526.c1900_company_id 
	  UNION ALL
		 SELECT t9800.c9800_ticket_id tkt_id
		      , t701.c701_distributor_id dist_id 
		      , t701.c701_distributor_name dist_name
		      , t504a.c703_sales_rep_id rep_id
		      , get_rep_name( t504a.c703_sales_rep_id ) rep_name
		      , t504a.c703_ass_rep_id assocrepid
		      , get_rep_name(t504a.c703_ass_rep_id ) assocrepnm
		      , t525.c525_product_request_id req_id
		      , t504a.c504a_return_dt incident_dt
		      , t504a.c504_consignment_id cn_id
		      , get_set_name(t526.c207_set_id) set_nm
		      , t504.c504a_etch_id etch_id
		      , ih_txn.c207_set_id p_s_num
		      , '' pdesc
		      , ih_txn.missing_qty 
		      , ih_txn.reconciled_qty
		      , DECODE(v_screen,'MissingCharges', ih_txn.charge_qty, NULL) ih_charge_qty 
		      , TO_NUMBER('') list_price
		      , ih_txn.charge ih_charge
		      , t701.c901_distributor_type dist_type
		      , t504a.c526_product_request_detail_id req_dtl_id
		      , get_log_flag(t504a.c526_product_request_detail_id,4000316) reconcmtcnt 
		      , c412_inhouse_trans_id trans_id
		      , t526.c207_set_id set_id     		    
		   FROM t504a_loaner_transaction t504a 
		      , t526_product_request_detail t526
		      , t9800_jira_ticket t9800       
		      , t525_product_request t525
		      , t504a_consignment_loaner t504
		      , t701_distributor t701
		      , (SELECT DISTINCT t412.c412_inhouse_trans_id
		              , t526.c207_set_id
		              , t412.c504a_loaner_transaction_id   
		              , 1 missing_qty
		              , 0 reconciled_qty
		              , 1 charge_qty
		              , get_rule_value ('SET_CHARGE', 'CHARGES') charge		              
		           FROM t413_inhouse_trans_items t413
		        	  , t412_inhouse_transactions t412
		        	  , t504a_loaner_transaction t504a
		        	  , t526_product_request_detail t526
                , v_in_list
		          WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
		            AND t413.c205_part_number_id  IN
		                    (
		                         SELECT c205_part_number_id
		                           FROM t208_set_details
		                          WHERE c207_set_id IN
		                            	(
		                                 SELECT c906_rule_value
		                                   FROM t906_rules
		                                  WHERE c906_rule_grp_id = 'CHARGES'
		                                    AND c906_rule_id = 'SETS'
		                                    AND c906_void_fl IS NULL
		                            	)
		                            AND c208_void_fl IS NULL
		                    )       
		             AND ( c412_status_fl  = DECODE(v_screen,'MissingCharges',1,c412_status_fl)
		                   OR c412_status_fl  = DECODE(v_screen,'MissingCharges',2,c412_status_fl)
		                   OR c412_status_fl  = DECODE(v_screen,'MissingCharges',3,c412_status_fl)
		                  ) 
		            AND c412_type = 50159
		            AND t412.c504a_loaner_transaction_id = t504a.c504a_loaner_transaction_id
		            AND t504a.c526_product_request_detail_id = t526.c526_product_request_detail_id
		            AND t413.c413_void_fl IS NULL
		            AND t412.c412_void_fl IS NULL
		            AND t504a.c504a_void_fl IS NULL
		            AND t526.c526_void_fl IS NULL ) ih_txn
		   WHERE  ih_txn.c504a_loaner_transaction_id = t504a.c504a_loaner_transaction_id
		     AND t504a.c526_product_request_detail_id = t526.c526_product_request_detail_id 
		     AND ((t526.c525_product_request_id = t9800.c9800_ref_id  and t9800.c9800_ref_type=110521)  OR (t526.c526_product_request_detail_id=t9800.c9800_ref_id and  t9800.c9800_ref_type=10056)) --110521 Parent Request, 10056 Child request PC-918 -Loaner Reconciliation - Auto Generating unique CSTKT
		     AND t525.c525_product_request_id = t526.c525_product_request_id
		     AND t504.c504_consignment_id = t504a.c504_consignment_id
		     AND t504a.c504a_consigned_to_id = t701.c701_distributor_id
		     AND t504a.c901_consigned_to = 50170 
		     AND ih_txn.charge > 0
		     AND t504a.c504a_void_fl IS NULL
		     AND t526.c526_void_fl IS NULL
		     AND t9800.c9800_void_fl IS NULL
		     AND t525.c525_void_fl IS NULL
		     AND t504.c504a_void_fl IS NULL   
		     AND t701.c701_void_fl IS NULL
		     AND (t525.c1900_company_id = v_filter
		     OR t525.c5040_plant_id   = v_filter)
  			 AND t525.c1900_company_id = t526.c1900_company_id ) ih_txn
		   , (SELECT t9200.c9200_ref_id ch_req_id
		           , TO_CHAR(t9201.c9201_deduction_date,v_datefmt) ded_date
		           , TO_CHAR(t9201.c9201_deduction_date,v_datefmt) ded_date_excel
		           , TO_CHAR(t9201.c9201_credited_date,v_datefmt) crd_date
		           , TO_CHAR(t9201.c9201_credited_date,v_datefmt) crd_date_excel
		           , get_user_name(NVL(t9200.c9200_updated_by, t9200.c9200_created_by)) last_updated_by
		           , NVL(TRUNC(t9200.c9200_updated_date)
		           , TRUNC(t9200.c9200_created_date)) last_updated_date
		           , SUM(t9201.c9201_amount) edit_chrg
		           , t9201.c901_recon_comments recon_comments
		           , get_code_name(t9201.c901_recon_comments) recon_comment
		           , MAX(c9201_charge_details_id) charge_id
		           , t9201.c9201_status status_fl
		           , decode(t9201.c9201_status,10,'Open',20,'Approved',25,'Credited',30,'Closed',40,'Cancelled','') status_fl_excel
		           , t9200.c9200_incident_value ch_set_id
		           , t9201.c205_part_number pnum
		           , SUM(t9201.c9201_qty_missing) qty_missing
		        FROM t9200_incident t9200
		           , t9201_charge_details t9201
		       WHERE t9200.c9200_incident_id = t9201.c9200_incident_id
		         AND t9200.c901_incident_type = 92069 
				 AND ( t9200.C1900_company_id  = v_company_id
				 OR t9200.c1900_company_id  IN (SELECT c1900_company_id FROM t5041_plant_company_mapping WHERE C5041_PRIMARY_FL = 'Y' AND c5041_void_fl IS NULL AND c5040_plant_id =v_filter ) 
				     )
		         AND (     c9201_status =  DECODE(v_screen,'MissingCharges', 10, NULL)   
		                OR c9201_status =  DECODE(v_screen,'LoanerCharges', 20 ,10)   
		                OR c9201_status =  DECODE(v_screen,'LoanerCharges', 25 ,10)   
		                OR c9201_status =  DECODE(v_screen,'LoanerCharges', 30 ,10)   
		                OR c9201_status =  DECODE(v_screen,'LoanerCharges', 40 ,10)    
		             ) 
		         AND c9200_ref_id IS NOT NULL
		         AND t9200.c9200_void_fl IS NULL
		         AND t9201.c9201_void_fl IS NULL
		    GROUP BY t9200.c9200_ref_id
		           , t9201.c9201_deduction_date
		           , t9201.c9201_credited_date
		           , t9200.c9200_created_by
		           , TRUNC(t9200.c9200_created_date)
		           , t9200.c9200_updated_by
		           , TRUNC(t9200.c9200_updated_date)
		           , t9201.c901_recon_comments
		           , t9201.c9201_status
		           , t9200.c9200_incident_value
		           , t9201.c205_part_number
		         ) charges
		WHERE NVL(ih_txn.ih_charge_qty,charges.qty_missing) > 0
		  AND charges.edit_chrg (+) > 0
		  AND ih_txn.req_id = charges.ch_req_id(+)
		  AND ih_txn.p_s_num = NVL(charges.pnum(+),charges.ch_set_id(+))
          AND ih_txn.req_id = NVL(DECODE(p_req_id,'0',NULL,TRIM(p_req_id)), ih_txn.req_id) 
	  	  AND ih_txn.dist_id = NVL(DECODE(p_dist_id,'0',NULL,p_dist_id), ih_txn.dist_id) 
	   	  AND ih_txn.rep_id = NVL(DECODE(p_rep_id,'0',NULL,p_rep_id), ih_txn.rep_id) 
	  	  AND ih_txn.incident_dt >= NVL(p_from_dt, ih_txn.incident_dt) 
	   	  AND ih_txn.incident_dt <= NVL(p_to_dt, ih_txn.incident_dt) 
	   	  AND ih_txn.dist_type IN (SELECT tokenii 
	   							 FROM v_double_in_list 
	   							WHERE token IS NOT null)
	   	  AND NVL(charges.recon_comments,-9999) = NVL(DECODE(p_recon_cmnt,'0',NULL,p_recon_cmnt), NVL(charges.recon_comments,-9999))	   	 
	      AND NVL(charges.status_fl, -9999) = NVL(DECODE(p_status,'0',NULL,TRIM(p_status)), NVL(charges.status_fl,-9999)) 		
     ORDER BY ih_txn.dist_type, ih_txn.dist_name, ih_txn.rep_name, ih_txn.req_id, ih_txn.tkt_id, ih_txn.incident_dt;	  
          

	/*  OPEN p_cursor_out 
	   FOR
	SELECT v9201.* 
	     , NVL(v9201.ih_charge_qty,v9201.qty_missing) charge_qty
	 	 , TO_CHAR(DECODE (NVL(v9201.status_fl,-1), 25,(-1 * NVL(v9201.edit_chrg,0))
	 	        ,  DECODE(v_screen,'MissingCharges',NVL(v9201.ih_charge,0),NVL(v9201.edit_chrg,0)))) charge
	  FROM v9201_charge_details v9201
	 WHERE v9201.req_id = NVL(DECODE(p_req_id,'0',NULL,TRIM(p_req_id)), v9201.req_id) 
	   AND v9201.dist_id = NVL(DECODE(p_dist_id,'0',NULL,p_dist_id), v9201.dist_id) 
	   AND v9201.rep_id = NVL(DECODE(p_rep_id,'0',NULL,p_rep_id), v9201.rep_id) 
	   AND v9201.incident_dt >= NVL(p_from_dt, v9201.incident_dt) 
	   AND v9201.incident_dt <= NVL(p_to_dt, v9201.incident_dt) 
	   AND NVL(v9201.recon_comments,-9999) = NVL(DECODE(p_recon_cmnt,'0',NULL,p_recon_cmnt), NVL(v9201.recon_comments,-9999))
	   AND NVL(v9201.status_fl, -9999) = NVL(DECODE(p_status,'0',NULL,TRIM(p_status)), NVL(v9201.status_fl,-9999)) 		
	   AND v9201.dist_type IN (SELECT tokenii 
	   							 FROM v_double_in_list 
	   							WHERE token IS NOT null)
  ORDER BY v9201.dist_type, v9201.dist_name, v9201.rep_name, v9201.req_id, v9201.tkt_id, v9201.incident_dt;	   */

 END gm_fch_msng_chrg_dtls;
 
 /**********************************************************************
   * Description : Validate if the credit exceeds the charge
   * Author      : VPrasath
  **********************************************************************/
 
 PROCEDURE gm_validate_charge(
    p_master_chrg_det_id IN  t9201_charge_details.c9201_master_chrg_det_id%TYPE
  , p_charge_amount 	 IN  t9201_charge_details.c9201_amount%TYPE
 )
 AS
 v_amount t9201_charge_details.c9201_amount%type;
 v_ref_id t9200_incident.c9200_ref_id%TYPE;
 
 BEGIN
	 v_amount := 0;
	 
	BEGIN 
		 SELECT t9200.c9200_ref_id
		      , (SUM(DECODE(t9201.c9201_master_chrg_det_id,'', c9201_amount,0)) 
	            -  abs(SUM(DECODE(t9201.c9201_master_chrg_det_id,'',0, c9201_amount)) + NVL(p_charge_amount,0)))
	       INTO v_ref_id, v_amount     
	       FROM t9201_charge_details t9201, t9200_incident t9200	       
	      WHERE t9200.c9200_incident_id = t9201.c9200_incident_id
	        AND (t9201.c9201_charge_details_id = p_master_chrg_det_id 
	             OR t9201.c9201_master_chrg_det_id = p_master_chrg_det_id)
	   GROUP BY t9200.c9200_ref_id;
      
      EXCEPTION WHEN NO_DATA_FOUND THEN
     	 v_amount := 0;
      END;
     
        IF v_amount < 0 THEN
        	GM_RAISE_APPLICATION_ERROR('-20999','250',v_ref_id);
        END IF;
        
 END gm_validate_charge;	 
 
 
  /**********************************************************************
   * Description : PROCEDURE to save the missing charge details
   * Author      : VPrasath
  **********************************************************************/
 PROCEDURE gm_sav_msng_chrg_dtls (
      p_input_string	IN	CLOB
	, p_user_id			IN	t525_product_request.c525_last_updated_by%TYPE
	, p_str_opt			IN	VARCHAR2
   )AS
   	 v_strlen    	 NUMBER := NVL (LENGTH (p_input_string), 0) ;
     v_string    	 CLOB := p_input_string;
     v_substring 	 CLOB ;
     v_charge_id	 VARCHAR2 (50) ;
     v_trans_id 	 VARCHAR2 (50) ;
  	 v_req_id		 VARCHAR2 (50) ;
  	 v_dist_id		 VARCHAR2 (50) ;
  	 v_rep_id		 VARCHAR2 (50) ;
  	 v_set_id		 VARCHAR2 (50) ;
  	 v_part_num		 VARCHAR2 (100);
  	 v_qty_msng		 VARCHAR2 (50) ;
  	 v_unit_cost	 VARCHAR2 (50) ;
  	 v_ded_date		 VARCHAR2 (50) ;
  	 v_edit_chrg	 VARCHAR2 (50) ;
  	 v_recon_cmnt	 VARCHAR2 (50) ;
  	 v_incident_id 	 VARCHAR2 (50) ;
  	 v_charge_qty	 NUMBER;
  	 v_ex_status	 NUMBER;
  	 v_ex_credit_amt NUMBER;  	 
  	 v_credit_amt    VARCHAR2(50); 
  	 v_status		 VARCHAR2(50); 
  	 v_qty_chrg		 VARCHAR2 (50);
  	 v_close_txn	 VARCHAR2(1);
  	 v_assoc_rep_id  VARCHAR2 (50) ;
  	 v_company_id  t1900_company.c1900_company_id%TYPE;
  	 v_datefmt VARCHAR2(10);
  	 v_trans_company_id  t1900_company.c1900_company_id%TYPE;
  	 v_plant_id t5040_plant_master.c5040_plant_id%TYPE;
  	 --
  	 CURSOR v_cursor IS
  	 SELECT t412.c412_inhouse_trans_id trans_id
	 	  , t413.c205_part_number_id pnum
	 	  , SUM(DECODE(t413.c413_status_fl,'Q',t413.c413_item_qty,0))
		    - SUM(DECODE(t413.c413_status_fl,'Q',0,t413.c413_item_qty)) msng_qty
		  , t413.c413_item_price  unit_cost
       FROM t412_inhouse_transactions t412
          , t413_inhouse_trans_items t413             
      WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id          
        AND t412.c412_type = 50159 -- missing
        AND t412.c412_void_fl IS NULL
        AND t413.c413_void_fl IS NULL
        AND t412.c412_inhouse_trans_id = v_trans_id
   GROUP BY t412.c412_inhouse_trans_id, t413.c205_part_number_id, t413.c413_item_price;
  	 
   BEGIN
	  
	   SELECT get_compid_frm_cntx(),NVL(get_compdtfmt_frm_cntx(),get_rule_value('DATEFMT','DATEFORMAT')) 
	     INTO v_company_id,v_datefmt	     
	     FROM dual;
	   
	    WHILE INSTR (v_string, '|') <> 0
        LOOP
            v_substring   := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
            v_string      := SUBSTR (v_string, INSTR (v_string, '|')    + 1) ;
            v_charge_id   := NULL;
            v_trans_id 	  := NULL;
  			v_req_id	  := NULL;
  			v_dist_id	  := NULL;
  			v_rep_id	  := NULL;
  			v_set_id	  := NULL;
  			v_part_num	  := NULL;
  			v_qty_msng	  := NULL;
  			v_unit_cost	  := NULL;
  			v_ded_date	  := NULL;
  			v_edit_chrg	  := NULL;
  			v_recon_cmnt  := NULL;
  			v_assoc_rep_id:= NULL;
  			v_charge_id   := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
        	v_substring   := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
  			v_trans_id 	  := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
        	v_substring   := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        	v_set_id	  := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
        	v_substring   := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        	v_req_id 	  := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
        	v_substring   := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        	v_ded_date 	  := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
        	v_substring   := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        	v_recon_cmnt  := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
        	v_substring   := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        	v_edit_chrg   := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
        	v_substring   := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        	v_dist_id 	  := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
        	v_substring   := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        	v_rep_id 	  := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
        	v_substring   := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        	v_qty_msng 	  := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
        	v_substring   := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        	v_qty_chrg 	  := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
        	v_substring   := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        	v_unit_cost   := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
        	v_substring   := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;      
            v_part_num    := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;          
            v_substring   := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;      
            v_status      := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;  
            v_substring   := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;      
            v_assoc_rep_id:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;  
            v_substring   := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
            v_credit_amt  := v_substring ; 
            
            IF v_recon_cmnt = '0' THEN
            	v_recon_cmnt := '';
            END IF;
            -- to get the transactions company id
			BEGIN
			     SELECT c1900_company_id, c5040_plant_id
			       INTO v_trans_company_id, v_plant_id
			       FROM t412_inhouse_transactions
			      WHERE c412_inhouse_trans_id = v_trans_id;
			EXCEPTION
			WHEN NO_DATA_FOUND THEN
			    v_trans_company_id := NULL;
			    v_plant_id         := NULL;
			END;
			-- to set the transaction company id to context
			IF v_trans_company_id IS NOT NULL THEN
			    gm_pkg_cor_client_context.gm_sav_client_context (v_trans_company_id, v_plant_id) ;
			END IF;
			--
   IF p_str_opt != 'Credit' THEN
            
			          SELECT SUM(DECODE(t413.c413_status_fl,'Q',t413.c413_item_qty,0))
							 - SUM(DECODE(t413.c413_status_fl,'Q',0,t413.c413_item_qty)) 
				        INTO v_charge_qty      
				        FROM t412_inhouse_transactions t412
				           , t413_inhouse_trans_items t413             
				       WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id          
				         AND t412.c412_type = 50159 -- missing
				         AND t412.c412_void_fl IS NULL
				         AND t413.c413_void_fl IS NULL
				         AND t413.c205_part_number_id = NVL(v_part_num, t413.c205_part_number_id)
				         AND t412.c412_inhouse_trans_id = v_trans_id;
				         
        IF (v_charge_qty - TO_NUMBER(v_qty_chrg)) < 0 THEN
        	GM_RAISE_APPLICATION_ERROR('-20999','251',v_part_num||'#$#'||v_req_id);
        END IF;
   
   END IF;     
        
	IF p_str_opt = 'Credit' THEN
			
	BEGIN	
			SELECT t9201.c9201_status
			  INTO v_ex_status
			  FROM t9201_charge_details t9201
			 WHERE t9201.c9201_charge_details_id = TO_NUMBER(v_charge_id);   
		  EXCEPTION WHEN NO_DATA_FOUND THEN
		  		v_ex_status := NULL;
		  END;
		  
		  IF (v_ex_status <> 20) THEN
		  		GM_RAISE_APPLICATION_ERROR('-20999','252',v_req_id);
		  END IF;
		  
			 -- Validate Credit vs Charged	
		  	 gm_validate_charge (TO_NUMBER(v_charge_id),v_credit_amt);
	           
	           IF NVL(v_credit_amt,0) <> 0 THEN
	           
	           SELECT 'GM-IC-' || s9200_incident.NEXTVAL 
			      INTO v_incident_id 
			      FROM DUAL;
	           
	           	 INSERT INTO t9200_incident( c9200_incident_id, c9200_ref_type, c9200_ref_id
        		      	   , c9200_incident_value, c9200_status_fl, c9200_created_by
        				   , c9200_created_date, c901_incident_type, c9200_incident_date,c1900_company_id) 
		     	      VALUES (v_incident_id, 50891, v_req_id
		     	           , v_set_id, NULL, p_user_id
		     	           , CURRENT_DATE, 92069, CURRENT_DATE,v_company_id);
		     	           
        		INSERT INTO t9201_charge_details(c9201_charge_details_id , c9201_amount , c9201_status
                          , c9201_created_by , c9201_created_date, c9200_incident_id
                          , c701_distributor_id , c703_sales_rep_id , c205_part_number                   
                          , c9201_qty_missing , c9201_unit_cost , c9201_credited_date
                          , c901_recon_comments, c9201_master_chrg_det_id,c703_ass_rep_id )
                     VALUES ( s9201_charge_details.NEXTVAL , v_credit_amt , 25
                          , p_user_id , CURRENT_DATE , v_incident_id
                          , v_dist_id , v_rep_id , v_part_num
                          , v_qty_msng, v_unit_cost, TO_DATE(v_ded_date,v_datefmt)
                          , NULL, TO_NUMBER(v_charge_id), v_assoc_rep_id);
                 
                 IF v_status !=  50864 THEN  
		                UPDATE t9201_charge_details t9201
		        		   SET t9201.c9201_status = (SELECT TO_NUMBER(c902_code_nm_alt) 
		 											   FROM t901_code_lookup t901 
		 											  WHERE t901.c901_code_id = v_status),
		           			   c9201_updated_by = p_user_id,
		             		   c9201_updated_date = CURRENT_DATE
		       			WHERE t9201.c9201_charge_details_id = TO_NUMBER(v_charge_id);
		       	END IF;			           
	 END IF;
			
            ELSIF p_str_opt = 'Charge' THEN
            
	            IF v_edit_chrg = '' THEN
					v_edit_chrg := 0;
				END IF;
				
				-- For set level charges
				IF v_part_num IS NULL THEN
				
					FOR v_index IN v_cursor 
				    LOOP
					   gm_cs_sav_loaner_recon_insert (v_index.trans_id, v_index.pnum, v_index.msng_qty, 'NOC#', v_index.unit_cost, 'W', p_user_id );
		               gm_save_ledger_posting (4845 , CURRENT_DATE , v_index.pnum, '01' , v_index.trans_id, v_index.msng_qty, NULL, p_user_id);
			    	END LOOP;
				ELSE
				-- Part Level Charges
					   gm_cs_sav_loaner_recon_insert (v_trans_id, v_part_num, v_qty_msng, 'NOC#', v_unit_cost, 'W', p_user_id);
		               gm_save_ledger_posting (4845 , CURRENT_DATE , v_part_num, '01' , v_trans_id, v_qty_msng, NULL, p_user_id);
				
				END IF;
	
				-- Check all the parts are charged, THEN close the missing transaction
				-- As records are created/updated in T413 table,we have to update the parent table as well,
				-- so the udpate query has to be called even if the transaction cannot be closed.
		            IF v_charge_qty = 0 OR v_part_num IS NULL THEN
						v_close_txn := 'Y';
					END IF;
					
		            UPDATE t412_inhouse_transactions
					   SET c412_status_fl = DECODE(v_close_txn,'Y','4',c412_status_fl)
						 , c412_verified_date = DECODE(v_close_txn,'Y',CURRENT_DATE,c412_verified_date)
						 , c412_verified_by = DECODE(v_close_txn,'Y',p_user_id,c412_verified_by)
						 , c412_last_updated_date = CURRENT_DATE
						 , c412_last_updated_by = p_user_id
					 WHERE c412_inhouse_trans_id = v_trans_id 
		               AND c412_void_fl IS NULL;
		             --Reset the Variable.
		             v_close_txn := '';  
          END IF;		
     
          IF p_str_opt != 'Credit' THEN
       			
		    UPDATE t9201_charge_details
		       SET c9201_amount = v_edit_chrg
		         , c9201_status = DECODE(p_str_opt,'Charge',20,10)
		         , c9201_updated_by = p_user_id
		         , c9201_updated_date = CURRENT_DATE
		         , c9201_qty_missing = TO_NUMBER(v_qty_chrg)
		         , c9201_deduction_date = TO_DATE(v_ded_date,v_datefmt)
		         , c901_recon_comments = v_recon_cmnt
		     WHERE c9201_charge_details_id = TO_NUMBER(v_charge_id);	     
		       

			IF SQL%ROWCOUNT = 0 THEN     
        		
			    SELECT 'GM-IC-' || s9200_incident.NEXTVAL 
			      INTO v_incident_id 
			      FROM DUAL;
        		
        		 INSERT INTO t9200_incident( c9200_incident_id, c9200_ref_type, c9200_ref_id
        		           , c9200_incident_value, c9200_status_fl, c9200_created_by
        			       , c9200_created_date, c901_incident_type, c9200_incident_date,c1900_company_id) 
        	          VALUES (v_incident_id, 50891, v_req_id
        	     	       , v_set_id, NULL, p_user_id
        	     	       , CURRENT_DATE, 92069, CURRENT_DATE,v_company_id);
        	     	              
        		INSERT INTO t9201_charge_details(c9201_charge_details_id , c9201_amount , c9201_status
                       	  , c9201_created_by , c9201_created_date, c9200_incident_id
                       	  , c701_distributor_id , c703_sales_rep_id , c205_part_number                   
                       	  , c9201_qty_missing , c9201_unit_cost , c9201_deduction_date , c901_recon_comments, c703_ass_rep_id )
                     VALUES ( s9201_charge_details.NEXTVAL , v_edit_chrg , DECODE(p_str_opt,'Charge',20,10)
                          , p_user_id , CURRENT_DATE , v_incident_id
                          , v_dist_id , v_rep_id , v_part_num
                          , TO_NUMBER(v_qty_chrg), v_unit_cost, TO_DATE(v_ded_date,v_datefmt), v_recon_cmnt,v_assoc_rep_id );
     		END IF;
     	  END IF;	
        END LOOP;
   END gm_sav_msng_chrg_dtls;
   
  /**********************************************************************
   * Description : Procedure to fetch the late fee Charge Details
   * Author      : Sindhu
  **********************************************************************/
   PROCEDURE gm_fch_late_fee_details ( 
     p_input_str        IN    CLOB,
     p_cursor_out          OUT   TYPES.CURSOR_TYPE
     )
AS


    v_input_str  CLOB:=p_input_str;
    v_input_string      NUMBER;
	p_req_id  VARCHAR2(50);
	p_dist_id VARCHAR2(50);
	p_status  VARCHAR2(50);
	p_rep_id  VARCHAR2(50);
	p_screen  VARCHAR2(50);
	p_recon_cmnt VARCHAR2(50);
	v_company_id  t1900_company.c1900_company_id%TYPE;
  	v_datefmt VARCHAR2(10);
  	v_filter T906_RULES.C906_RULE_VALUE%TYPE;
  	v_plant_id t5040_plant_master.c5040_plant_id%TYPE;
  	v_screen VARCHAR2(50);
    v_dist_type VARCHAR2(50);

BEGIN
    
    my_context.set_my_inlist_ctx (v_input_str);
   -- v_input_string  :=TO_NUMBER(v_input_str);
     
     p_req_id  := '0';		
	 p_dist_id := '0';		
     p_status  := '0';
	 p_screen  := NULL;    
	 p_recon_cmnt := '0';
	 p_rep_id  := '0';	

	 select get_compid_frm_cntx(),NVL(get_compdtfmt_frm_cntx(),get_rule_value('DATEFMT','DATEFORMAT')),get_plantid_frm_cntx() into v_company_id, v_datefmt, v_plant_id from dual;
	
	 SELECT DECODE(GET_RULE_VALUE(v_company_id,'MISSING_PART_CHARGES'), null, v_company_id, v_plant_id) --MISSING_PART_CHARGES- To fetch the records for EDC entity countries based on plant
       INTO v_filter 
       FROM DUAL;
	
	IF TRIM(p_screen) IS NULL 
	THEN
		v_screen := 'MissingCharges';
		v_dist_type := '1,70100|2,70101|';
    END IF;	
	
	my_context.set_double_inlist_ctx(v_dist_type);
	
	
	OPEN p_cursor_out 
	   FOR
	SELECT ih_txn.*
		 , charges.* 
		 , NVL(ih_txn.ih_charge_qty,charges.qty_missing) charge_qty
	 	 , TO_CHAR(DECODE (NVL(charges.status_fl,-1), 25,(-1 * NVL(charges.edit_chrg,0))
	 	 , DECODE(v_screen,'MissingCharges',NVL(ih_txn.ih_charge,0),NVL(charges.edit_chrg,0)))) charge
	 FROM 
		(SELECT t9800.c9800_ticket_id tkt_id
		      , t701.c701_distributor_id dist_id 
		      , t701.c701_distributor_name dist_name
		      , t504a.c703_sales_rep_id rep_id
		      , get_rep_name(t504a.c703_sales_rep_id ) rep_name
		      , t504a.c703_ass_rep_id assocrepid
		      , get_rep_name(t504a.c703_ass_rep_id ) assocrepnm
		      , t525.c525_product_request_id req_id
		  --    , t504a.c504a_return_dt incident_dt
		      , t504a.c504_consignment_id cn_id
		      , get_set_name(t526.c207_set_id) set_nm
		      , t504.c504a_etch_id etch_id
		      , ih_txn.c205_part_number_id p_s_num
		      , get_partnum_desc(ih_txn.c205_part_number_id) pdesc
		      , ih_txn.missing_qty 
		      , ih_txn.reconciled_qty		      
		      , DECODE(v_screen,'MissingCharges', ih_txn.charge_qty, NULL) ih_charge_qty 
		      , TO_NUMBER(get_part_price(ih_txn.c205_part_number_id, 'L')) list_price
		      , ih_txn.charge ih_charge
		      , t701.c901_distributor_type dist_type
		      , t504a.c526_product_request_detail_id req_dtl_id
		      , get_log_flag(t504a.c526_product_request_detail_id,4000316) reconcmtcnt 
		      , c412_inhouse_trans_id trans_id
		      , t526.c207_set_id set_id   
		      , to_char(t504a.c504a_return_dt,v_datefmt) incident_date
		   FROM t504a_loaner_transaction t504a 
		      , t526_product_request_detail t526
		      , t9800_jira_ticket t9800       
		      , t525_product_request t525
		      , t504a_consignment_loaner t504
		      , t701_distributor t701
              , (SELECT t412.c412_inhouse_trans_id
			          , t413.c205_part_number_id 
			          , t412.c504a_loaner_transaction_id    
			          , SUM(DECODE(t413.c413_status_fl,'Q',t413.c413_item_qty,0)) missing_qty
			          , SUM(DECODE(t413.c413_status_fl,'Q',0,t413.c413_item_qty)) reconciled_qty
			          , SUM(DECODE(t413.c413_status_fl,'Q',t413.c413_item_qty,0)) - 
			            SUM(DECODE(t413.c413_status_fl,'Q',0,t413.c413_item_qty)) charge_qty       
			          , gm_pkg_cm_loaner_jobs.get_missing_part_price(t413.c205_part_number_id) charge			         
		           FROM t412_inhouse_transactions t412
		              , t413_inhouse_trans_items t413 
		          WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id          
		            AND t412.c412_type = 50159
		            AND (     c412_status_fl  = DECODE(v_screen,'MissingCharges',1,c412_status_fl)
		                   OR c412_status_fl  = DECODE(v_screen,'MissingCharges',2,c412_status_fl)
		                   OR c412_status_fl  = DECODE(v_screen,'MissingCharges',3,c412_status_fl)
		                 ) 
		            AND c412_void_fl IS NULL
		            AND t413.c413_void_fl IS NULL
		            AND t413.c205_part_number_id NOT IN (
		                     SELECT c205_part_number_id 
		                       FROM t208_set_details 
		                      WHERE c207_set_id IN (   SELECT c906_rule_value
		                                                FROM t906_rules
		                                               WHERE c906_rule_grp_id = 'CHARGES'
		                                                 AND c906_rule_id     = 'SETS'
		                                                 AND c906_void_fl IS NULL
		                                            )    
		                       AND c208_void_fl IS NULL
		                    )
		       GROUP BY t412.c412_inhouse_trans_id
		              , t413.c205_part_number_id 
		              , t412.c504a_loaner_transaction_id                   
		         ) ih_txn
		   WHERE  ih_txn.c504a_loaner_transaction_id = t504a.c504a_loaner_transaction_id
		     AND t504a.c526_product_request_detail_id = t526.c526_product_request_detail_id 
   	         AND ((t526.c525_product_request_id = t9800.c9800_ref_id  and t9800.c9800_ref_type=110521)  OR (t526.c526_product_request_detail_id=t9800.c9800_ref_id and  t9800.c9800_ref_type=10056)) --110521 Parent Request, 10056 Child request PC-918 -Loaner Reconciliation - Auto Generating unique CSTKT
		     AND t525.c525_product_request_id = t526.c525_product_request_id
		     AND t504.c504_consignment_id = t504a.c504_consignment_id
		     AND t504a.c504a_consigned_to_id = t701.c701_distributor_id
		     AND t504a.c901_consigned_to = 50170 
		     AND ih_txn.charge > 0
		     AND t504a.c504a_void_fl IS NULL
		     AND t526.c526_void_fl IS NULL
		     AND t9800.c9800_void_fl IS NULL
		     AND t525.c525_void_fl IS NULL
		     AND t504.c504a_void_fl IS NULL   
		     AND t701.c701_void_fl IS NULL
		     AND (t525.c1900_company_id = v_filter
		     OR t525.c5040_plant_id   = v_filter)
  			 AND t525.c1900_company_id = t526.c1900_company_id 
	  UNION ALL
		 SELECT t9800.c9800_ticket_id tkt_id
		      , t701.c701_distributor_id dist_id 
		      , t701.c701_distributor_name dist_name
		      , t504a.c703_sales_rep_id rep_id
		      , get_rep_name( t504a.c703_sales_rep_id ) rep_name
		      , t504a.c703_ass_rep_id assocrepid
		      , get_rep_name(t504a.c703_ass_rep_id ) assocrepnm
		      , t525.c525_product_request_id req_id
		--      , t504a.c504a_return_dt incident_dt
		      , t504a.c504_consignment_id cn_id
		      , get_set_name(t526.c207_set_id) set_nm
		      , t504.c504a_etch_id etch_id
		      , ih_txn.c207_set_id p_s_num
		      , '' pdesc
		      , ih_txn.missing_qty 
		      , ih_txn.reconciled_qty
		      , DECODE(v_screen,'MissingCharges', ih_txn.charge_qty, NULL) ih_charge_qty 
		      , TO_NUMBER('') list_price
		      , ih_txn.charge ih_charge
		      , t701.c901_distributor_type dist_type
		      , t504a.c526_product_request_detail_id req_dtl_id
		      , get_log_flag(t504a.c526_product_request_detail_id,4000316) reconcmtcnt 
		      , c412_inhouse_trans_id trans_id
		      , t526.c207_set_id set_id    
		      , to_char(t504a.c504a_return_dt,v_datefmt) incident_date
		   FROM t504a_loaner_transaction t504a 
		      , t526_product_request_detail t526
		      , t9800_jira_ticket t9800       
		      , t525_product_request t525
		      , t504a_consignment_loaner t504
		      , t701_distributor t701
		      , (SELECT DISTINCT t412.c412_inhouse_trans_id
		              , t526.c207_set_id
		              , t412.c504a_loaner_transaction_id   
		              , 1 missing_qty
		              , 0 reconciled_qty
		              , 1 charge_qty
		              , get_rule_value ('SET_CHARGE', 'CHARGES') charge		              
		           FROM t413_inhouse_trans_items t413
		        	  , t412_inhouse_transactions t412
		        	  , t504a_loaner_transaction t504a
		        	  , t526_product_request_detail t526
                      , v_in_list
		          WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
		            AND t413.c205_part_number_id  IN
		                    (
		                         SELECT c205_part_number_id
		                           FROM t208_set_details
		                          WHERE c207_set_id IN
		                            	(
		                                 SELECT c906_rule_value
		                                   FROM t906_rules
		                                  WHERE c906_rule_grp_id = 'CHARGES'
		                                    AND c906_rule_id = 'SETS'
		                                    AND c906_void_fl IS NULL
		                            	)
		                            AND c208_void_fl IS NULL
		                    )       
		             AND ( c412_status_fl  = DECODE(v_screen,'MissingCharges',1,c412_status_fl)
		                   OR c412_status_fl  = DECODE(v_screen,'MissingCharges',2,c412_status_fl)
		                   OR c412_status_fl  = DECODE(v_screen,'MissingCharges',3,c412_status_fl)
		                  ) 
		            AND c412_type = 50159
		            AND t412.c504a_loaner_transaction_id = t504a.c504a_loaner_transaction_id
		            AND t504a.c526_product_request_detail_id = t526.c526_product_request_detail_id
		            AND t413.c413_void_fl IS NULL
		            AND t412.c412_void_fl IS NULL
		            AND t504a.c504a_void_fl IS NULL
		            AND t526.c526_void_fl IS NULL ) ih_txn
		   WHERE  ih_txn.c504a_loaner_transaction_id = t504a.c504a_loaner_transaction_id
		     AND t504a.c526_product_request_detail_id = t526.c526_product_request_detail_id 
		     AND t526.c525_product_request_id = t9800.c9800_ref_id
		     AND ((t526.c525_product_request_id = t9800.c9800_ref_id  and t9800.c9800_ref_type=110521)  OR (t526.c526_product_request_detail_id=t9800.c9800_ref_id and  t9800.c9800_ref_type=10056)) --110521 Parent Request, 10056 Child request PC-918 -Loaner Reconciliation - Auto Generating unique CSTKT
		     AND t525.c525_product_request_id = t526.c525_product_request_id
		     AND t504.c504_consignment_id = t504a.c504_consignment_id
		     AND t504a.c504a_consigned_to_id = t701.c701_distributor_id
		     AND t504a.c901_consigned_to = 50170 
		     AND ih_txn.charge > 0
		     AND t504a.c504a_void_fl IS NULL
		     AND t526.c526_void_fl IS NULL
		     AND t9800.c9800_void_fl IS NULL
		     AND t525.c525_void_fl IS NULL
		     AND t504.c504a_void_fl IS NULL   
		     AND t701.c701_void_fl IS NULL
		     AND (t525.c1900_company_id = v_filter
		     OR t525.c5040_plant_id   = v_filter)
  			 AND t525.c1900_company_id = t526.c1900_company_id ) ih_txn
		   , (SELECT t9200.c9200_ref_id ch_req_id
		           , TO_CHAR(t9201.c9201_deduction_date,v_datefmt) ded_date
		           , TO_CHAR(t9201.c9201_deduction_date,v_datefmt) ded_date_excel
		           , TO_CHAR(t9201.c9201_credited_date,v_datefmt) crd_date
		           , TO_CHAR(t9201.c9201_credited_date,v_datefmt) crd_date_excel
		           , get_user_name(NVL(t9200.c9200_updated_by, t9200.c9200_created_by)) last_updated_by
		           , TO_CHAR(NVL(TRUNC(t9200.c9200_updated_date)
		           , TRUNC(t9200.c9200_created_date)),v_datefmt) last_updated_date
		           , SUM(t9201.c9201_amount) edit_chrg
		           , t9201.c901_recon_comments recon_comments
		           , get_code_name(t9201.c901_recon_comments) recon_comment
		           , MAX(c9201_charge_details_id) charge_id
		           , t9201.c9201_status status_fl
		           , decode(t9201.c9201_status,10,'Open',20,'Approved',25,'Credited',30,'Closed',40,'Cancelled','') status_fl_excel
		           , t9200.c9200_incident_value ch_set_id
		           , t9201.c205_part_number pnum
		           , SUM(t9201.c9201_qty_missing) qty_missing
		        FROM t9200_incident t9200
		           , t9201_charge_details t9201
		       WHERE t9200.c9200_incident_id = t9201.c9200_incident_id
		         AND t9200.c901_incident_type = 92069 
				 AND ( t9200.C1900_company_id  = v_company_id
				 OR t9200.c1900_company_id  IN (SELECT c1900_company_id FROM t5041_plant_company_mapping WHERE C5041_PRIMARY_FL = 'Y' AND c5041_void_fl IS NULL AND c5040_plant_id =v_filter ) 
				     )
		         AND (     c9201_status =  DECODE(v_screen,'MissingCharges', 10, NULL)   
		                OR c9201_status =  DECODE(v_screen,'LoanerCharges', 20 ,10)   
		                OR c9201_status =  DECODE(v_screen,'LoanerCharges', 25 ,10)   
		                OR c9201_status =  DECODE(v_screen,'LoanerCharges', 30 ,10)   
		                OR c9201_status =  DECODE(v_screen,'LoanerCharges', 40 ,10)    
		             ) 
		         AND c9200_ref_id IS NOT NULL
		         AND t9200.c9200_void_fl IS NULL
		         AND t9201.c9201_void_fl IS NULL
		    GROUP BY t9200.c9200_ref_id
		           , t9201.c9201_deduction_date
		           , t9201.c9201_credited_date
		           , t9200.c9200_created_by
		           , TRUNC(t9200.c9200_created_date)
		           , t9200.c9200_updated_by
		           , TRUNC(t9200.c9200_updated_date)
		           , t9201.c901_recon_comments
		           , t9201.c9201_status
		           , t9200.c9200_incident_value
		           , t9201.c205_part_number
		         ) charges
		WHERE NVL(ih_txn.ih_charge_qty,charges.qty_missing) > 0
		  AND charges.edit_chrg (+) > 0
		  AND ih_txn.req_id = charges.ch_req_id(+)
		  AND ih_txn.p_s_num = NVL(charges.pnum(+),charges.ch_set_id(+))
          AND ih_txn.req_id = NVL(DECODE(p_req_id,'0',NULL,TRIM(p_req_id)), ih_txn.req_id) 
	  	  AND ih_txn.dist_id = NVL(DECODE(p_dist_id,'0',NULL,p_dist_id), ih_txn.dist_id) 
	   	  AND ih_txn.rep_id = NVL(DECODE(p_rep_id,'0',NULL,p_rep_id), ih_txn.rep_id) 
	  	  
	   	  AND ih_txn.dist_type IN (SELECT tokenii 
	   							 FROM v_double_in_list 
	   							WHERE token IS NOT null)
	   	  AND NVL(charges.recon_comments,-9999) = NVL(DECODE(p_recon_cmnt,'0',NULL,p_recon_cmnt), NVL(charges.recon_comments,-9999))	   	 
	      AND NVL(charges.status_fl, -9999) = NVL(DECODE(p_status,'0',NULL,TRIM(p_status)), NVL(charges.status_fl,-9999)) 	
 	      AND charges.charge_id IN (SELECT token FROM  v_in_list)	
     ORDER BY ih_txn.dist_type, ih_txn.dist_name, ih_txn.rep_name, ih_txn.req_id, ih_txn.tkt_id, ih_txn.incident_date;	  
    
	
  END gm_fch_late_fee_details; 
  
  /**********************************************************************
   * Description : Procedure to fetch to mail id for late fee charges
   * Author      : Sindhu
  **********************************************************************/
  PROCEDURE gm_fch_late_fee_email( 
    p_rep_id  		IN      VARCHAR2,
    p_tomail_id     OUT   	t107_contact.c107_contact_VALUE%TYPE)

  AS
  	v_party_id		t107_contact.c101_party_id%TYPE;
  	v_to_email_id	CLOB;
  BEGIN
  
 
	    
	v_party_id    := TRIM (get_rep_party_id(p_rep_id));
	v_to_email_id := TRIM (gm_pkg_cm_contact.get_contact_value(v_party_id, 26241109)); --26241109-Loaner Late Fee Email
	
	p_tomail_id :=  v_to_email_id;
	
	END gm_fch_late_fee_email;

END gm_pkg_op_msng_chrg;
/
