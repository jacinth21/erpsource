/* Formatted on 2011/04/16 08:29 (Formatter Plus v4.8.0) */
--@"C:\database\packages\operations\gm_pkg_op_ld_replenishment.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_ld_replenishment
IS
/**********************************************************************
   * Description : This wrapper used to execute the replenishment job in 
   * different plants. Execution time also available in t906_rules.
   * Author 	 : APrasath
  **********************************************************************/
	PROCEDURE gm_sav_initiate_wrapper
	AS
---
		v_company_id   t1900_company.c1900_company_id%TYPE;
		v_plant_id     t5040_plant_master.c5040_plant_id%TYPE;
		v_job_flag     CHAR(1);
---

--Cursor to fetch the plant details and time of replenishment job execution in each plant
		CURSOR c_job_execution
		IS		
		  SELECT t5040.c5040_plant_id plantid, t5040.c5040_plant_name plantname, t5040.c1900_parent_company_id companyid
			  , get_code_name (t5040.c901_timezone) timezone, t906.c906_rule_value executetime
			   FROM t5040_plant_master t5040, t906_rules t906
			  WHERE t5040.c5040_active_fl = 'Y'
			    AND t5040.c5040_void_fl  IS NULL
			    AND t906.c906_rule_id     = t5040.c5040_plant_id
			    AND t906.c906_rule_grp_id = 'INVREPLENISHMENTJOB'
			    AND t906.c906_void_fl    IS NULL;
    
	BEGIN
		--fetch the plant and job execution details from cursor
		FOR c_job_dtl IN c_job_execution
		LOOP		
		--set timezone based on plant	
		EXECUTE IMMEDIATE 'ALTER SESSION SET TIME_ZONE='''||c_job_dtl.timezone||'''';
		v_company_id := c_job_dtl.companyid;
		v_plant_id := c_job_dtl.plantid;
		--validate the exection time with local time
		IF TO_NUMBER(TO_CHAR (CURRENT_DATE, 'HH24')) = c_job_dtl.executetime THEN 
			v_job_flag := 'Y';
		ELSE
			v_job_flag := 'N';
		END IF;	
		--job flag returns Y to execute the job in respective plant   
        	IF v_job_flag = 'Y' THEN 
        --set the plant id and primary company of plant into context
			gm_pkg_cor_client_context.gm_sav_client_context(v_company_id,NULL,NULL,v_plant_id);
		--Execute the replenishment job
			gm_pkg_op_ld_replenishment.gm_sav_initiate_main();
			END IF;
		END LOOP;
	END gm_sav_initiate_wrapper;
	--
	-- 
	/**********************************************************************
   * Description : Procedure to load the locations having qty less or equal to min.
   * and generate the FG Location Transfer transaction.
   * Author 	 : Ritesh
  **********************************************************************/
	PROCEDURE gm_sav_initiate_main
	AS
---
		v_bulk_qty	   t5053_location_part_mapping.c5053_max_qty%TYPE;
		v_sugg_qty	   t5053_location_part_mapping.c5053_max_qty%TYPE;
		v_lcnqtymsgstr VARCHAR2 (2000);
		v_out_trans_id VARCHAR2 (50);
		v_lcn_cnt	   NUMBER;
		v_start_dt	   DATE;
		v_replntype	   NUMBER;
		v_company_id   t1900_company.c1900_company_id%TYPE;
		v_plant_id     t5040_plant_master.c5040_plant_id%TYPE;
		v_plant_name   t5040_plant_master.c5040_plant_name%TYPE;
		v_plant_fl     VARCHAR2(1);
---

--Cursor to fetch the active shelf locations having curr. qty less or equal to min qty for replenishment
		CURSOR c_replenishment
		IS
			SELECT     t5053.c5053_location_part_map_id, t5053.c205_part_number_id pnum, t5053.c5052_location_id lcnid
				      , t5053.c5053_curr_qty currqty      
                --, gm_pkg_op_item_control_rpt.get_part_location_cur_qty (t5053.c205_part_number_id, t5052.c5052_location_id) currqty
              , t5053.c5053_min_qty           , t5053.c5053_max_qty maxqty
				      , t5051.c901_warehouse_type whtype, DECODE(t5051.c901_warehouse_type,56001, 56010,93341) rplntype
			    FROM  t5053_location_part_mapping t5053, t5052_location_master t5052, t5051_inv_warehouse t5051
			    WHERE t5053.c5052_location_id = t5052.c5052_location_id
		          AND t5051.c5051_inv_warehouse_id = t5052.c5051_inv_warehouse_id
		          AND t5051.c901_status_id = 1
		          AND t5053.c5053_curr_qty <= t5053.c5053_min_qty
		          AND t5053.c5053_max_qty IS NOT NULL
		          AND t5053.c5053_min_qty IS NOT NULL
		          AND t5053.c5053_curr_qty IS NOT NULL
		          AND t5052.c5052_void_fl IS NULL
		          AND t5052.c901_location_type = 93320 -- Shelf 
		          AND t5052.c5040_plant_id = v_plant_id
		          AND t5051.c5051_inv_warehouse_id NOT IN
					(SELECT c906_rule_value
					   FROM t906_rules
					    WHERE c906_rule_id     = 'WHID'
					      AND c906_rule_grp_id = 'RULEWH'
					      AND c906_void_fl    IS NULL)--excluding FS and account warehouse
		          AND t5052.c901_status = '93310'      -- ACTIVE LOCATION
			   ORDER BY whtype DESC,pnum;
    
	BEGIN
		--fetch the plant and company details from context		
		v_start_dt	:= CURRENT_DATE;
		SELECT  get_compid_frm_cntx()
			  INTO  v_company_id
			  FROM DUAL;
		SELECT  get_plantid_frm_cntx()
			  INTO  v_plant_id
			  FROM DUAL;
		v_plant_name := get_plant_name(v_plant_id);
		
		SELECT get_rule_value(v_plant_id,'BUILDINGPLANTMAP')       /* add building in replenishment job PMT-33508*/
		  INTO v_plant_fl
		  FROM DUAL;
		
				FOR c_dtl IN c_replenishment
				LOOP
					--Calculate the Replenishment Suggested Qty = Max - current
					v_replntype := c_dtl.rplntype;
					v_sugg_qty	:= c_dtl.maxqty - c_dtl.currqty;
					v_bulk_qty	:= gm_pkg_op_inv_location_module.get_total_bulkloc_qty (c_dtl.pnum,c_dtl.whtype);   -- To get the actual bulk qty
					v_lcn_cnt	:= gm_pkg_op_inv_location_module.get_open_txn_count (c_dtl.pnum, c_dtl.lcnid,c_dtl.whtype);	 -- To get the count for open transaction for same part and location.
					
					-- v_sugg_qty  goes -ive when min is 100 and max is 20 
					IF v_bulk_qty > 0 AND v_lcn_cnt <= 0 AND v_sugg_qty > 0
					THEN
						-- if bulk is < suggested then replace suggested qty with bulk.
						IF v_bulk_qty <= v_sugg_qty
						THEN
							v_sugg_qty	:= v_bulk_qty;
						END IF;
		
						--v_lcnqtymsgstr := "locationid||pnum||v_sugg_qty||control#||price||";
						v_lcnqtymsgstr :=
							   c_dtl.lcnid
							|| ','
							|| c_dtl.pnum
							|| ','
							|| v_sugg_qty
							|| ','
							|| ''
							|| ','
							|| get_part_price (c_dtl.pnum, 'E')
							|| '|';
						-- For T412, T413  entry and t413 Entry  v_lcnqtymsgstr- with partnum,
						gm_pkg_op_item_control_txn.gm_sav_inhouse_txn (v_lcnqtymsgstr, 30301, v_replntype, v_out_trans_id);
						
						--String for t5050	partnumber, v,suggqty,ctrl number as null and location ID is null for 93342 and 93343
						v_lcnqtymsgstr := c_dtl.pnum || ',' || v_sugg_qty || ',,,'|| c_dtl.whtype || '|';
						-- Entry for t5055_control_number table for  93342 :=Pick Initiated & 93343 :=Pick Transacted 
						gm_pkg_op_item_control_txn.gm_sav_item_control (v_out_trans_id, v_replntype, v_lcnqtymsgstr, 93342, 30301 );
  						gm_pkg_op_item_control_txn.gm_sav_item_control (v_out_trans_id, v_replntype, v_lcnqtymsgstr, 93343, 30301 );
  						
  						--String for t5050	partnumber, v,suggqty, ctrl number, location ID
						v_lcnqtymsgstr := c_dtl.pnum || ',' || v_sugg_qty || ',' || '' || ',' || c_dtl.lcnid || ',' || c_dtl.whtype || '|';	
						
						-- Entry for t5055_control_number table 93327 put location
						gm_pkg_op_item_control_txn.gm_sav_item_control (v_out_trans_id, v_replntype, v_lcnqtymsgstr, 93344, 30301);
						-- Entry for T5050_INVPICK_ASSIGN_DETAIL for replenishment 93341
						gm_pkg_allocation.gm_ins_invpick_assign_detail (v_replntype, v_out_trans_id, 30301);
						
						-- Building id mapped to transaction in PMT-33508 - add building in replenishment job
						IF v_plant_fl = 'Y' THEN
					      gm_pkg_op_storage_building_ih.gm_update_inhouset412_building_id(v_out_trans_id,30301,null);
						END IF;
						
					END IF;
					
					END LOOP;
		
					-- LOG THE STATUS
					gm_pkg_cm_job.gm_cm_notify_load_info (v_start_dt
													, CURRENT_DATE
													, 'S'
													, 'SUCCESS'
													, 'gm_pkg_op_ld_replenishment - FG/RW Transfer executed in '||v_plant_name
													 );
					COMMIT;
				
				EXCEPTION
				WHEN OTHERS
				THEN
					ROLLBACK;
					--
					gm_pkg_cm_job.gm_cm_notify_load_info (v_start_dt
														, CURRENT_DATE
														, 'E'
														, SQLERRM
														, 'gm_pkg_op_ld_no_stack - FG/RW Transfer Error in '||v_plant_name
														 );
					COMMIT;			
	END gm_sav_initiate_main;
	--
	-- 
END gm_pkg_op_ld_replenishment;
/
