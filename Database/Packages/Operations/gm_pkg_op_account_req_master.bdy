--@"C:\Database\Packages\Operations\gm_pkg_op_account_req_master.bdy";
CREATE OR REPLACE
PACKAGE BODY gm_pkg_op_account_req_master
IS
--
   PROCEDURE gm_sav_request (
      p_request_id          IN       t520_request.c520_request_id%TYPE,
      p_required_date       IN       t520_request.c520_required_date%TYPE,
      p_request_source      IN       t520_request.c901_request_source%TYPE,
      p_request_txn_id      IN       t520_request.c520_request_txn_id%TYPE,
      p_set_id              IN       t520_request.c207_set_id%TYPE,
      p_request_for         IN       t520_request.c520_request_for%TYPE,
      p_request_to          IN       t520_request.c520_request_to%TYPE,
      p_request_by_type     IN       t520_request.c901_request_by_type%TYPE,
      p_request_by          IN       t520_request.c520_request_by%TYPE,
      p_ship_to             IN       t520_request.c901_ship_to%TYPE,
      p_ship_to_id_by       IN       t520_request.c520_ship_to_id%TYPE,
      p_master_request_id   IN       t520_request.c520_master_request_id%TYPE,
      p_status_fl           IN       t520_request.c520_status_fl%TYPE,
      p_created_by          IN       t520_request.c520_created_by%TYPE,
      p_purpose             IN       t520_request.c901_purpose%TYPE,
      p_out_request_id      OUT      t520_request.c520_request_id%TYPE,
      p_plan_ship_date      IN		 t520_request.c520_planned_ship_date%TYPE DEFAULT NULL,
      p_loaner_req_id       IN       T520_REQUEST.C525_PRODUCT_REQUEST_ID%TYPE DEFAULT NULL -- PMT-32450 - to Save the Loaner Request Id to t520 Table
   )
   AS
    /**********************************************************************
   * Description : Procedure to insert  / update the request information
   * Author    : Joe P Kumar
     **********************************************************************/
   	v_div_type		 	VARCHAR2(20) := '100823'; -- US Distributor
	v_purpose			VARCHAR2(20);
	v_company_id  t1900_company.c1900_company_id%TYPE;
    v_plant_id 	  t5040_plant_master.c5040_plant_id%TYPE;	
    v_acc_cmp_id  t1900_company.c1900_company_id%TYPE;
    v_user_plant_id 	  t5040_plant_master.c5040_plant_id%TYPE;
   BEGIN
	   
      SELECT get_compid_frm_cntx(), get_plantid_frm_cntx()
        INTO v_company_id, v_plant_id
        FROM DUAL;
        
      BEGIN
         --getting company id and plant id mapped from account
      SELECT
         get_distributor_company_id(p_request_to),
         gm_pkg_op_plant_rpt.get_default_plant_for_comp(get_distributor_company_id(p_request_to))
      INTO v_acc_cmp_id, v_user_plant_id  
	  from dual;
      EXCEPTION
           WHEN NO_DATA_FOUND
           THEN
           v_acc_cmp_id := NULL;
           v_user_plant_id := NULL;
        END;
 
      SELECT 'GM-RQ-' || s520_request.NEXTVAL
        INTO p_out_request_id
        FROM DUAL;
        
        SELECT get_rule_value(p_purpose,'INHOUSEPUR') INTO v_purpose FROM DUAL;
	   	IF p_request_for <> '40025' THEN			
		v_div_type := NVL(get_distributor_division(p_request_to),'100823'); -- Fetch the US/OUS Distributor
        IF v_div_type = '100824'  AND NVL(v_purpose,'N') = 'Y' -- OUS Distributor, 4000097:OUS Sales Replenishment
			THEN
			-- To fetch request id based on OUS distribute like 'UK-CN-XXXX'
			gm_pkg_op_account_req_master.gm_fetch_ous_trans_id(p_out_request_id,p_request_to,p_created_by);
		END IF;
		END IF;		
       INSERT INTO t520_request
                  (c520_created_date, c901_request_source,
                   c520_required_date, c520_request_to, c520_request_txn_id,
                   c520_status_fl, c520_master_request_id, c901_ship_to,
                   c520_request_id, c520_created_by, c520_request_by,
                   c520_request_for, c207_set_id, c901_request_by_type,
                   c520_ship_to_id, c520_request_date, c901_purpose,
                   c520_planned_ship_date, c1900_company_id, c5040_plant_id ,c525_product_request_id
                  )
           VALUES (CURRENT_DATE, p_request_source,
                   p_required_date, p_request_to, p_request_txn_id,
                   p_status_fl, p_master_request_id, p_ship_to,
                   p_out_request_id, p_created_by, p_request_by,
                   p_request_for, p_set_id, p_request_by_type,
                   p_ship_to_id_by, TRUNC (CURRENT_DATE), p_purpose,
                   p_plan_ship_date, nvl(v_company_id,v_acc_cmp_id), nvl(v_plant_id,v_user_plant_id),p_loaner_req_id
                  );
   END gm_sav_request;

   /******************************************************************************
     * Description : Procedure to insert  / update the request details information
     * Author       : Joe P Kumar
    ******************************************************************************/
   PROCEDURE gm_sav_request_detail (
      p_request_id           IN       t521_request_detail.c520_request_id%TYPE,
      p_partnum              IN       t521_request_detail.c205_part_number_id%TYPE,
      p_qty                  IN       t521_request_detail.c521_qty%TYPE,
      p_out_request_detail   OUT      t521_request_detail.c521_request_detail_id%TYPE
   )
   AS
   BEGIN
      SELECT s521_request_detail.NEXTVAL
        INTO p_out_request_detail
        FROM DUAL;

      INSERT INTO t521_request_detail
                  (c521_qty, c520_request_id, c521_request_detail_id,
                   c205_part_number_id
                  )
           VALUES (p_qty, p_request_id, p_out_request_detail,
                   p_partnum
                  );
   END gm_sav_request_detail;

   /******************************************************************************
   * Description : Procedure to insert consignment details
   * Author     : Joe P Kumar
  ******************************************************************************/
   PROCEDURE gm_sav_consignment (
       p_distributor_id		  IN	   t504_consignment.c701_distributor_id%TYPE
	  , p_ship_date 			  IN	   t504_consignment.c504_ship_date%TYPE
	  , p_return_date			  IN	   t504_consignment.c504_return_date%TYPE
	  , p_comments				  IN	   t504_consignment.c504_comments%TYPE
	  , p_total_cost			  IN	   t504_consignment.c504_total_cost%TYPE
	  , p_status_fl 			  IN	   t504_consignment.c504_status_fl%TYPE
	  , p_void_fl				  IN	   t504_consignment.c504_void_fl%TYPE
	  , p_account_id			  IN	   t504_consignment.c704_account_id%TYPE
	  , p_ship_to				  IN	   t504_consignment.c504_ship_to%TYPE
	  , p_set_id				  IN	   t504_consignment.c207_set_id%TYPE
	  , p_ship_to_id			  IN	   t504_consignment.c504_ship_to_id%TYPE
	  , p_final_comments		  IN	   t504_consignment.c504_final_comments%TYPE
	  , p_inhouse_purpose		  IN	   t504_consignment.c504_inhouse_purpose%TYPE
	  , p_type					  IN	   t504_consignment.c504_type%TYPE
	  , p_delivery_carrier		  IN	   t504_consignment.c504_delivery_carrier%TYPE
	  , p_delivery_mode 		  IN	   t504_consignment.c504_delivery_mode%TYPE
	  , p_tracking_number		  IN	   t504_consignment.c504_tracking_number%TYPE
	  , p_ship_req_fl			  IN	   t504_consignment.c504_ship_req_fl%TYPE
	  , p_verify_fl 			  IN	   t504_consignment.c504_verify_fl%TYPE
	  , p_verified_date 		  IN	   t504_consignment.c504_verified_date%TYPE
	  , p_verified_by			  IN	   t504_consignment.c504_verified_by%TYPE
	  , p_update_inv_fl 		  IN	   t504_consignment.c504_update_inv_fl%TYPE
	  , p_reprocess_id			  IN	   t504_consignment.c504_reprocess_id%TYPE
	  , p_master_consignment_id   IN	   t504_consignment.c504_master_consignment_id%TYPE
	  , p_request_id			  IN	   t504_consignment.c520_request_id%TYPE
	  , p_created_by			  IN	   t504_consignment.c504_created_by%TYPE
	  , p_out_consignment_id	  OUT	   t504_consignment.c504_consignment_id%TYPE
   )
   AS
  v_prefix	VARCHAR2(20);
   BEGIN
	  	gm_pkg_op_account_req_master.gm_sav_consignment
                                    (p_distributor_id,
                                     p_ship_date,
                                     p_return_date,
                                     p_comments,
                                     p_total_cost,
                                     p_status_fl,
                                     p_void_fl,
                                     p_account_id,
                                     p_ship_to,
                                     p_set_id,
                                     p_ship_to_id,
                                     p_final_comments,
                                     p_inhouse_purpose,
                                     p_type,
                                     p_delivery_carrier,
                                     p_delivery_mode,
                                     p_tracking_number,
                                     p_ship_req_fl,                      --by default is 1
                                     p_verify_fl,
                                     p_verified_date,
                                     p_verified_by,
                                     p_update_inv_fl,
                                     p_reprocess_id,
                                     p_master_consignment_id,
                                     p_request_id,
                                     p_created_by,
                                     NULL,
                                     NULL,
				     p_out_consignment_id
                                    );          
      
   END gm_sav_consignment;

/******************************************************************************
   * Description : Overloaded Procedure to insert consignment details for ref id and type.
   * Author 	 : Rshah
  ******************************************************************************/
  PROCEDURE gm_sav_consignment (
	   p_distributor_id          IN       t504_consignment.c701_distributor_id%TYPE,
      p_ship_date               IN       t504_consignment.c504_ship_date%TYPE,
      p_return_date             IN       t504_consignment.c504_return_date%TYPE,
      p_comments                IN       t504_consignment.c504_comments%TYPE,
      p_total_cost              IN       t504_consignment.c504_total_cost%TYPE,
      p_status_fl               IN       t504_consignment.c504_status_fl%TYPE,
      p_void_fl                 IN       t504_consignment.c504_void_fl%TYPE,
      p_account_id              IN       t504_consignment.c704_account_id%TYPE,
      p_ship_to                 IN       t504_consignment.c504_ship_to%TYPE,
      p_set_id                  IN       t504_consignment.c207_set_id%TYPE,
      p_ship_to_id              IN       t504_consignment.c504_ship_to_id%TYPE,
      p_final_comments          IN       t504_consignment.c504_final_comments%TYPE,
      p_inhouse_purpose         IN       t504_consignment.c504_inhouse_purpose%TYPE,
      p_type                    IN       t504_consignment.c504_type%TYPE,
      p_delivery_carrier        IN       t504_consignment.c504_delivery_carrier%TYPE,
      p_delivery_mode           IN       t504_consignment.c504_delivery_mode%TYPE,
      p_tracking_number         IN       t504_consignment.c504_tracking_number%TYPE,
      p_ship_req_fl             IN       t504_consignment.c504_ship_req_fl%TYPE,
      p_verify_fl               IN       t504_consignment.c504_verify_fl%TYPE,
      p_verified_date           IN       t504_consignment.c504_verified_date%TYPE,
      p_verified_by             IN       t504_consignment.c504_verified_by%TYPE,
      p_update_inv_fl           IN       t504_consignment.c504_update_inv_fl%TYPE,
      p_reprocess_id            IN       t504_consignment.c504_reprocess_id%TYPE,
      p_master_consignment_id   IN       t504_consignment.c504_master_consignment_id%TYPE,
      p_request_id              IN       t504_consignment.c520_request_id%TYPE,
      p_created_by              IN       t504_consignment.c504_created_by%TYPE,
      p_ref_id                  IN       t504_consignment.c504_ref_id%TYPE,
      p_reftype                 IN       t504_consignment.c901_ref_type%TYPE,
      p_out_consignment_id      OUT      t504_consignment.c504_consignment_id%TYPE
  )
  AS
  v_prefix	VARCHAR2(20);
  v_div_type		 	VARCHAR2(20) := '100823'; -- US Distributor
  v_purpose				VARCHAR2(20);
  v_request_for         t520_request.c520_request_for%TYPE;
  v_distributor_id      t504_consignment.c701_distributor_id%TYPE;
  v_company_id  t1900_company.c1900_company_id%TYPE;
  v_plant_id 	  t5040_plant_master.c5040_plant_id%TYPE;
  v_acc_cmp_id  t1900_company.c1900_company_id%TYPE;
  v_user_plant_id 	  t5040_plant_master.c5040_plant_id%TYPE;
  BEGIN
  
      BEGIN
        --getting company id and plant id mapped from account
        SELECT c1900_company_id,gm_pkg_op_plant_rpt.get_default_plant_for_comp(gm_pkg_it_user.get_default_party_company(p_created_by))
		  INTO v_acc_cmp_id, v_user_plant_id  
		  FROM T704_account 
		 WHERE c704_account_id = p_account_id 
		   AND c704_void_fl IS NULL;
		EXCEPTION
           WHEN NO_DATA_FOUND
           THEN
           v_acc_cmp_id := NULL;
           v_user_plant_id := NULL;
        END;
        
      SELECT get_compid_frm_cntx(), get_plantid_frm_cntx()
        INTO v_company_id, v_plant_id
        FROM DUAL;
      
      v_distributor_id:= p_distributor_id;     
      SELECT get_rule_value ('TRANS_PREFIX', p_type)
        INTO v_prefix
        FROM DUAL;
        
        IF v_prefix IS NOT NULL THEN        
        SELECT get_next_consign_id (p_type)
           INTO p_out_consignment_id
           FROM DUAL;           
        END IF;

      IF p_out_consignment_id IS NULL
      THEN
         SELECT get_next_consign_id ('consignment')
           INTO p_out_consignment_id
           FROM DUAL;
      END IF;
      
     select c520_request_for into v_request_for from t520_request where c520_request_id = p_request_id and c520_void_fl is null;
   
 
    SELECT get_rule_value(p_inhouse_purpose,'INHOUSEPUR') INTO v_purpose FROM DUAL;
       IF v_request_for <> '40025' THEN  
    v_div_type := NVL(get_distributor_division(p_distributor_id),'100823'); -- Fetch the US/OUS Distributor
	IF v_div_type = '100824' AND NVL(v_purpose,'N') = 'Y' -- OUS Distributor, 4000097:OUS Sales Replenishment
		THEN
		-- To fetch consignment id based on OUS distribute like 'UK-CN-XXXX'
		gm_pkg_op_account_req_master.gm_fetch_ous_trans_id(p_out_consignment_id,p_distributor_id,p_created_by);
	END IF;	
	    ELSE
	    v_distributor_id:=NULL;
	    
		END IF;		
			
      INSERT INTO t504_consignment
                  (c504_tracking_number, c504_verified_date,
                   c504_delivery_carrier, c504_final_comments,
                   c504_total_cost, c520_request_id, c504_consignment_id,
                   c504_delivery_mode, c504_status_fl, c504_reprocess_id,
                   c504_return_date, c504_void_fl, c504_type, c504_comments,
                   c504_ship_to, c704_account_id, c504_created_by,
                   c701_distributor_id, c504_ship_to_id, c504_update_inv_fl,
                   c504_verified_by, c504_ship_req_fl,
                   c504_master_consignment_id, c207_set_id, c504_ship_date,
                   c504_verify_fl, c504_inhouse_purpose, c504_created_date, c504_ref_id, c901_ref_type,
                   c1900_company_id, c5040_plant_id
                  )
           VALUES (p_tracking_number, p_verified_date,
                   p_delivery_carrier, p_final_comments,
                   p_total_cost, p_request_id, p_out_consignment_id,
                   p_delivery_mode, p_status_fl, p_reprocess_id,
                   p_return_date, p_void_fl, p_type, p_comments,
                   p_ship_to, p_account_id, p_created_by,
                   v_distributor_id, p_ship_to_id, p_update_inv_fl,
                   p_verified_by, p_ship_req_fl,
                   p_master_consignment_id, p_set_id, p_ship_date,
                   p_verify_fl, p_inhouse_purpose, CURRENT_DATE, p_ref_id, p_reftype,
                   nvl(v_company_id,v_acc_cmp_id), nvl(v_plant_id,v_user_plant_id)
                  );
   END gm_sav_consignment;

/******************************************************************************
   * Description : Procedure to insert / update consignment item details
   * Author     : Joe P Kumar
  ******************************************************************************/
   PROCEDURE gm_sav_consignment_detail (
      p_item_consignment_id      IN       t505_item_consignment.c505_item_consignment_id%TYPE,
      p_consignment_id           IN       t505_item_consignment.c504_consignment_id%TYPE,
      p_part_number_id           IN       t505_item_consignment.c205_part_number_id%TYPE,
      p_control_number           IN       t505_item_consignment.c505_control_number%TYPE,
      p_item_qty                 IN       t505_item_consignment.c505_item_qty%TYPE,
      p_item_price               IN       t505_item_consignment.c505_item_price%TYPE,
      p_void_fl                  IN       t505_item_consignment.c505_void_fl%TYPE,
      p_type                     IN       t505_item_consignment.c901_type%TYPE,
      p_ref_id                   IN       t505_item_consignment.c505_ref_id%TYPE,
      p_out_item_consigment_id   OUT      t505_item_consignment.c505_item_consignment_id%TYPE
   )
   AS
   BEGIN
      IF TRIM (p_item_price) IS NULL
      THEN
         raise_application_error (-20901, p_part_number_id);
      END IF;

      UPDATE t505_item_consignment
         SET c505_item_qty = NVL (c505_item_qty, 0) + p_item_qty,
             c505_item_price = p_item_price
       WHERE c504_consignment_id = p_consignment_id
         AND c205_part_number_id = p_part_number_id
         AND NVL(c505_control_number,'TBE') = NVL(p_control_number,'TBE');

      IF (SQL%ROWCOUNT = 0)
      THEN
         SELECT s504_consign_item.NEXTVAL
           INTO p_out_item_consigment_id
           FROM DUAL;

         INSERT INTO t505_item_consignment
                     (c901_type, c505_void_fl, c505_item_price,
                      c504_consignment_id, c505_item_consignment_id,
                      c505_ref_id, c505_item_qty, c205_part_number_id,
                      c505_control_number
                     )
              VALUES (p_type, p_void_fl, p_item_price,
                      p_consignment_id, p_out_item_consigment_id,
                      p_ref_id, p_item_qty, p_part_number_id,
                      UPPER (NVL (p_control_number, 'TBE'))
                     );
      END IF;
   END gm_sav_consignment_detail;

   
   /******************************************************************************
   * Description : Overloaded Procedure to insert / update consignment item details with the warehouse Type
   * Author 	 : Joe P Kumar
  ******************************************************************************/
	PROCEDURE gm_sav_consignment_detail (
		p_item_consignment_id	   IN		t505_item_consignment.c505_item_consignment_id%TYPE
	  , p_consignment_id		   IN		t505_item_consignment.c504_consignment_id%TYPE
	  , p_part_number_id		   IN		t505_item_consignment.c205_part_number_id%TYPE
	  , p_control_number		   IN		t505_item_consignment.c505_control_number%TYPE
	  , p_item_qty				   IN		t505_item_consignment.c505_item_qty%TYPE
	  , p_item_price			   IN		t505_item_consignment.c505_item_price%TYPE
	  , p_void_fl				   IN		t505_item_consignment.c505_void_fl%TYPE
	  , p_type					   IN		t505_item_consignment.c901_type%TYPE
	  , p_ref_id				   IN		t505_item_consignment.c505_ref_id%TYPE
	  , p_whtype				   IN 		t505_item_consignment.C901_WAREHOUSE_TYPE%TYPE
	  , p_out_item_consigment_id   OUT		t505_item_consignment.c505_item_consignment_id%TYPE
	)
	AS
   BEGIN
      IF TRIM (p_item_price) IS NULL
      THEN
         raise_application_error (-20901, p_part_number_id);
      END IF;

      UPDATE t505_item_consignment
         SET c505_item_qty = NVL (c505_item_qty, 0) + p_item_qty,
             c505_item_price = p_item_price
       WHERE c504_consignment_id = p_consignment_id
         AND c205_part_number_id = p_part_number_id
         AND NVL(c505_control_number,'TBE') = NVL(p_control_number,'TBE')
         AND C901_WAREHOUSE_TYPE = p_whtype;

      IF (SQL%ROWCOUNT = 0)
      THEN
         SELECT s504_consign_item.NEXTVAL
           INTO p_out_item_consigment_id
           FROM DUAL;

         INSERT INTO t505_item_consignment
                     (c901_type, c505_void_fl, c505_item_price,
                      c504_consignment_id, c505_item_consignment_id,
                      c505_ref_id, c505_item_qty, c205_part_number_id,
                      c505_control_number, C901_WAREHOUSE_TYPE
                     )
              VALUES (p_type, p_void_fl, p_item_price,
                      p_consignment_id, p_out_item_consigment_id,
                      p_ref_id, p_item_qty, p_part_number_id,
                      UPPER (NVL (p_control_number, 'TBE')), p_whtype
                     );
      END IF;
   END gm_sav_consignment_detail;
--

   /******************************************************************************
    * Description : Procedure to update request details
    * Author       : Xun Qu
   ******************************************************************************/
   PROCEDURE gm_sav_request_edit_info (
      p_required_date   IN   VARCHAR2, 
      p_request_to      IN   t520_request.c520_request_to%TYPE,
      p_userid          IN   t520_request.c520_last_updated_by%TYPE,
      p_requestid       IN   t520_request.c520_request_id%TYPE,
      p_shipto          IN   t520_request.c901_ship_to%TYPE,
      p_shiptoid        IN   t520_request.c520_ship_to_id%TYPE,
      p_inpstr			IN	 VARCHAR2 DEFAULT NULL,
      p_plan_ship_dt    IN	 t520_request.c520_planned_ship_date%TYPE DEFAULT NULL
   )
   AS
      v_req_date   DATE;
      v_shipto     t520_request.c901_ship_to%TYPE;
      v_dist_type NUMBER;
 	  v_req_for	 t520_request.c520_request_for%TYPE;
 	  v_date_fmt  VARCHAR2(30);
      CURSOR v_cur
      IS 
      	SELECT t520.c520_request_id reqid
		   FROM t520_request t520
		  WHERE t520.c520_master_request_id = p_requestid
		    AND t520.c520_void_fl          IS NULL;
   BEGIN
	   
	   IF p_shipto != '0' AND p_shipto is NOT NULL AND p_request_to IS NULL	THEN   
	   		GM_RAISE_APPLICATION_ERROR('-20999','186','');
	   END IF;
      
	   gm_pkg_op_request_validate.gm_op_consigment_validate (p_requestid,
                                                            p_request_to
                                                           );

      SELECT c520_required_date
        INTO v_req_date
        FROM t520_request
       WHERE c520_request_id = p_requestid;

      SELECT DECODE (p_shipto, 0, NULL, p_shipto)
        INTO v_shipto
        FROM DUAL;
        
      v_dist_type := GET_DISTRIBUTOR_TYPE (p_request_to);

 	  -- 102930 Inter Company Transfer
	  IF v_req_for = 102930 AND NVL(v_dist_type,'-999') <> 70103
	  THEN
			GM_RAISE_APPLICATION_ERROR('-20999','185','');--Please select valid distributor for type ICT
	  END IF; -- end if v_req_for
	  v_date_fmt := NVL(get_rule_value('DATEFMT','DATEFORMAT'),'MM/dd/yyyy');
	  
      UPDATE t520_request
         SET c520_required_date = TO_DATE (p_required_date, v_date_fmt),
             c520_request_to = p_request_to,
             c520_last_updated_by = p_userid,
             c520_last_updated_date = CURRENT_DATE,
             c901_ship_to = v_shipto,
             c520_ship_to_id = p_shiptoid,
             c520_planned_ship_date = p_plan_ship_dt
       WHERE c520_request_id = p_requestid;
       IF p_inpstr IS NOT NULL
       THEN
       		gm_pkg_op_account_req_master.gm_sav_request_attribute(p_requestid,p_inpstr,p_userid);
	       	FOR v_req IN v_cur
	       	LOOP
	       		gm_pkg_op_account_req_master.gm_sav_request_attribute(v_req.REQID,p_inpstr,p_userid);
	       	END LOOP;
       END IF;

      IF (v_req_date <> TO_DATE (p_required_date, v_date_fmt))
      THEN
         gm_pkg_op_inventory_update.gm_op_sav_tpr_clear_request
                                                               (p_requestid,
                                                                'dateupdate',
                                                                p_userid,
                                                                v_req_date
                                                               );
      END IF;

	
      -- Below SQL to update consignment information
      UPDATE t504_consignment
         SET c504_ship_to = v_shipto,
             c504_ship_to_id = p_shiptoid,
             c701_distributor_id =
                               DECODE (p_request_to,
                                       '01', NULL,
                                       p_request_to
                                      ),
             c704_account_id = DECODE (p_request_to, '01', '01', NULL),
             c504_last_updated_by = p_userid,
             c504_last_updated_date = CURRENT_DATE
       WHERE c520_request_id = p_requestid AND c504_void_fl IS NULL;

      --Below SQL to update child record
      UPDATE t520_request
         SET c520_required_date = TO_DATE (p_required_date, v_date_fmt),
             c520_request_to = p_request_to,
             c901_ship_to = v_shipto,
             c520_ship_to_id = p_shiptoid,
             c520_last_updated_by = p_userid,
             c520_last_updated_date = CURRENT_DATE
       WHERE c520_master_request_id = p_requestid;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         raise_application_error (-20902, '');
   END gm_sav_request_edit_info;
   /*
    * Description: -- Rollback the Request to WIP Status
    * Author: Rajkumar J
    */
PROCEDURE gm_sav_request_status(
		p_req_id IN T520_REQUEST.C520_REQUEST_ID%TYPE,
		p_status IN T520_REQUEST.C520_STATUS_FL%TYPE,
		p_user_id IN T520_REQUEST.C520_LAST_UPDATED_BY%TYPE 
	)
	AS
	BEGIN
	UPDATE T520_REQUEST  
	  SET C520_STATUS_FL=p_status,
		C520_LAST_UPDATED_BY=p_user_id,
		C520_LAST_UPDATED_DATE=CURRENT_DATE  
	  WHERE 
		C520_REQUEST_ID=p_req_id 
		AND C520_VOID_FL IS NULL;
END gm_sav_request_status;
/*
 * Author		: Rajkumar
 * 
 * Description	: Function to fetch the Tag Ids that mapped with the CN 
 */
FUNCTION get_tag_id(
	p_consg_id 	t504_consignment.c504_consignment_id%TYPE
)RETURN VARCHAR2
IS
	v_tags		VARCHAR2(2000);
	v_consg_id	VARCHAR2(20);
BEGIN
	BEGIN
		 SELECT T504A.C504_CONSIGNMENT_ID INTO v_consg_id
		   FROM T504_CONSIGNMENT T504, T504_CONSIGNMENT T504A, T520_REQUEST T520
		  WHERE T504A.C520_REQUEST_ID    = T520.C520_MASTER_REQUEST_ID
		    AND T520.C520_REQUEST_ID     = T504.C520_REQUEST_ID
		    AND T504.C504_CONSIGNMENT_ID = p_consg_id
		    AND T520.C520_VOID_FL IS NULL
		    AND T504A.C504_VOID_FL IS NULL
		    AND T504A.C504_VOID_FL IS NULL;
	EXCEPTION 
	WHEN NO_DATA_FOUND
	THEN
		v_consg_id := p_consg_id;
	END;
	
   WITH v_txns AS (
         SELECT c5010_tag_id trans_id from t5010_tag WHERE c5010_last_updated_trans_id = v_consg_id and c5010_void_fl IS NULL
   )
   SELECT MAX (SYS_CONNECT_BY_PATH (trans_id, ', ')) INTO v_tags
     FROM
       (SELECT trans_id, ROW_NUMBER () OVER (ORDER BY trans_id) AS curr
          FROM v_txns)
     CONNECT BY curr - 1 = PRIOR curr
    START WITH curr = 1;
	v_tags := SUBSTR (v_tags, 2);
	RETURN v_tags;
END get_tag_id;

 /********************************************************************************
 * Description	: Function to fetch request attribute value by request id and type
 * Author	:	Xun
 *********************************************************************************/
FUNCTION get_request_attribute(
	p_req_id IN T520_REQUEST.C520_REQUEST_ID%TYPE,
	p_type	 IN	t520a_request_attribute.c901_attribute_type%TYPE 
) RETURN VARCHAR2
IS
	v_attval	VARCHAR2(2000) :='';
	v_req_id    T520_REQUEST.C520_REQUEST_ID%TYPE;
	v_rule_value t906_rules.c906_rule_value%TYPE;
	v_purpose	t504_consignment.c504_inhouse_purpose%TYPE;
BEGIN
	-- set the request id values to local variable.
		v_req_id := p_req_id;	
	-- get the inhouse purpose value based on the request id
	-- add exception block in case request is back order, there's no data found 
	BEGIN
	 SELECT NVL (c504_inhouse_purpose, -999)
	   INTO v_purpose
	   FROM t504_consignment t504
	  WHERE t504.c520_request_id = p_req_id
	    AND t504.c504_void_fl   IS NULL;
	    EXCEPTION
		WHEN NO_DATA_FOUND
		THEN 
			v_purpose :='';
	END;
	-- v_purpose values is - 4000097 (OUS Sales Replenishments) then rule value gives - Y otherwise NULL)
	 SELECT get_rule_value (v_purpose, 'INHOUSEPUR')
	   INTO v_rule_value
	   FROM dual;
	-- 
	IF v_rule_value = 'Y' THEN
		-- OUS Sales Replenishments - than Backorder PO # always empty (t520a).So, get the PO # from master request.
	     SELECT NVL (t520.c520_master_request_id, t520.c520_request_id)
	       INTO v_req_id
	       FROM t520_request t520
	      WHERE t520.c520_request_id = p_req_id
	        AND t520.c520_void_fl   IS NULL;
	END IF; -- end if v_rule_value
--
	BEGIN		
		     SELECT C520A_ATTRIBUTE_VALUE INTO v_attval
			   FROM T520A_REQUEST_ATTRIBUTE
			  WHERE C520_REQUEST_ID = v_req_id
			    AND C901_ATTRIBUTE_TYPE = p_type
			    AND C520A_VOID_FL      IS NULL;
		EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			v_attval :='';
		END;
		RETURN 'TEST CUSTPO';
END get_request_attribute;

/*
 *Description	: this procedure will update the request attribute,
 * if the record does not exists, it will create a new request attribute 
 */
PROCEDURE gm_sav_request_attribute(
	p_req_id IN T520_REQUEST.C520_REQUEST_ID%TYPE,
	p_inpstr IN	VARCHAR2,
	p_user_id IN T520_REQUEST.C520_LAST_UPDATED_BY%TYPE
)
AS
	v_substring	VARCHAR2(2000);
	v_string	VARCHAR2(2000) := p_inpstr;
	v_atttype	VARCHAR2(20);
	v_attval	VARCHAR2(1000);
BEGIN
	
	WHILE INSTR (v_string, '|') <> 0
    LOOP
         v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
         v_string := SUBSTR (v_string, INSTR (v_string, '|') + 1);
         v_atttype := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
         v_attval := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
         
        IF v_attval IS NULL OR v_attval = ''
        THEN
        	UPDATE T520A_REQUEST_ATTRIBUTE
			SET C520A_VOID_FL   = 'Y',
				C520A_LAST_UPDATED_BY = p_user_id,
				C520A_LAST_UPDATED_DATE = CURRENT_DATE 
			  WHERE C520_REQUEST_ID = p_req_id
			    AND C901_ATTRIBUTE_TYPE = v_atttype
			    AND C520A_VOID_FL IS NULL;
        END IF;
         
        UPDATE T520A_REQUEST_ATTRIBUTE
			SET C520A_ATTRIBUTE_VALUE   = v_attval,
				C520A_LAST_UPDATED_BY = p_user_id,
				C520A_LAST_UPDATED_DATE = CURRENT_DATE 
			  WHERE C520_REQUEST_ID     = p_req_id
			    AND C901_ATTRIBUTE_TYPE = v_atttype
			    AND C520A_VOID_FL IS NULL;
         
		IF SQL%ROWCOUNT = 0
		THEN
			  INSERT
			   INTO T520A_REQUEST_ATTRIBUTE
			    (
			        C520A_REQUEST_ATTRIBUTE_ID, C520_REQUEST_ID, C901_ATTRIBUTE_TYPE
			      , C520A_ATTRIBUTE_VALUE, C520A_CREATED_BY, C520A_CREATED_DATE
			    )
			    VALUES
			    (
			        S520A_REQUEST_ATTRIB.nextval, p_req_id, v_atttype
			      , v_attval, p_user_id, CURRENT_DATE
			    ) ;
		END IF;
    END LOOP;    
END gm_sav_request_attribute;

  /**********************************************************************
   * Description : Procedure to insert  / update the request information (to pass the extra parameter as Planned Ship Date)
   * Author    : 
     **********************************************************************/
   PROCEDURE gm_sav_request (
      p_request_id          IN       t520_request.c520_request_id%TYPE,
      p_required_date       IN       t520_request.c520_required_date%TYPE,
      p_plan_ship_date      IN		 t520_request.c520_planned_ship_date%TYPE,    
      p_request_source      IN       t520_request.c901_request_source%TYPE,
      p_request_txn_id      IN       t520_request.c520_request_txn_id%TYPE,
      p_set_id              IN       t520_request.c207_set_id%TYPE,
      p_request_for         IN       t520_request.c520_request_for%TYPE,
      p_request_to          IN       t520_request.c520_request_to%TYPE,
      p_request_by_type     IN       t520_request.c901_request_by_type%TYPE,
      p_request_by          IN       t520_request.c520_request_by%TYPE,
      p_ship_to             IN       t520_request.c901_ship_to%TYPE,
      p_ship_to_id_by       IN       t520_request.c520_ship_to_id%TYPE,
      p_master_request_id   IN       t520_request.c520_master_request_id%TYPE,
      p_status_fl           IN       t520_request.c520_status_fl%TYPE,
      p_created_by          IN       t520_request.c520_created_by%TYPE,
      p_purpose             IN       t520_request.c901_purpose%TYPE,
      p_out_request_id      OUT      t520_request.c520_request_id%TYPE,
      p_comments   	 	    IN 	     T520_REQUEST.C520_COMMENTS%TYPE DEFAULT NULL,
      p_loaner_req_id       IN       T520_REQUEST.C525_PRODUCT_REQUEST_ID%TYPE DEFAULT NULL 
   )
   --p_comments is used for additional info of OUS (loaner/item initiate)
   AS
   	v_div_type		 	VARCHAR2(20) := '100823'; -- US Distributor
   	v_purpose			VARCHAR2(20);
	v_company_id  t1900_company.c1900_company_id%TYPE;
    v_plant_id 	  t5040_plant_master.c5040_plant_id%TYPE;
    v_acc_cmp_id  t1900_company.c1900_company_id%TYPE;
    v_user_plant_id 	  t5040_plant_master.c5040_plant_id%TYPE;
   BEGIN
	   
      SELECT get_compid_frm_cntx(), get_plantid_frm_cntx()
        INTO v_company_id, v_plant_id
        FROM DUAL;
        
      BEGIN
        --getting company id and plant id mapped from account
      SELECT
         get_distributor_company_id(p_request_to),
         gm_pkg_op_plant_rpt.get_default_plant_for_comp(get_distributor_company_id(p_request_to))
      INTO v_acc_cmp_id, v_user_plant_id  
	  from dual;
      EXCEPTION
           WHEN NO_DATA_FOUND
           THEN
           v_acc_cmp_id := NULL;
           v_user_plant_id := NULL;
        END;
 
      SELECT 'GM-RQ-' || s520_request.NEXTVAL
        INTO p_out_request_id
        FROM DUAL;
 		
        SELECT get_rule_value(p_purpose,'INHOUSEPUR') INTO v_purpose FROM DUAL;
       IF p_request_for <> '40025' THEN
		v_div_type := NVL(get_distributor_division(p_request_to),'100823'); -- Fetch the US/OUS Distributor
        IF v_div_type = '100824'  AND NVL(v_purpose,'N') = 'Y' -- OUS Distributor, 4000097:OUS Sales Replenishment
			THEN
			-- To fetch request id based on OUS distribute like 'UK-CN-XXXX'
			gm_pkg_op_account_req_master.gm_fetch_ous_trans_id(p_out_request_id,p_request_to,p_created_by);
		END IF;
	   END IF;
      INSERT INTO t520_request
                  (c520_created_date, c901_request_source,
                   c520_required_date, c520_request_to, c520_request_txn_id,
                   c520_status_fl, c520_master_request_id, c901_ship_to,
                   c520_request_id, c520_created_by, c520_request_by,
                   c520_request_for, c207_set_id, c901_request_by_type,
                   c520_ship_to_id, c520_request_date, c901_purpose,
                   c520_planned_ship_date,C520_COMMENTS,
                   c1900_company_id, c5040_plant_id,c525_product_request_id
                  )
           VALUES (CURRENT_DATE, p_request_source,
                   p_required_date, p_request_to, p_request_txn_id,
                   p_status_fl, p_master_request_id, p_ship_to,
                   p_out_request_id, p_created_by, p_request_by,
                   p_request_for, p_set_id, p_request_by_type,
                   p_ship_to_id_by, TRUNC (CURRENT_DATE), p_purpose,
                   p_plan_ship_date,p_comments,
                   nvl(v_company_id,v_acc_cmp_id), nvl(v_plant_id,v_user_plant_id),p_loaner_req_id
                  );
   END gm_sav_request;
/*
    * Description: This Procedure is common procedure used to create the Inhouse Transaction
    * Author:	Rajkumar
    */
   PROCEDURE gm_sav_inhouse_txn(
   	p_comments		IN		T412_INHOUSE_TRANSACTIONS.C412_COMMENTS%TYPE,
   	p_status_fl		IN		T412_INHOUSE_TRANSACTIONS.C412_STATUS_FL%TYPE,
   	p_purpose		IN		T412_INHOUSE_TRANSACTIONS.C412_INHOUSE_PURPOSE%TYPE,
   	p_type			IN		T412_INHOUSE_TRANSACTIONS.C412_TYPE%TYPE,
   	p_refid			IN		T412_INHOUSE_TRANSACTIONS.C412_REF_ID%TYPE,
   	p_reftype               IN              T412_INHOUSE_TRANSACTIONS.C901_REF_TYPE%TYPE,
  	p_shipto		IN		T412_INHOUSE_TRANSACTIONS.C412_RELEASE_TO%TYPE,
   	p_shiptoid		IN		T412_INHOUSE_TRANSACTIONS.C412_RELEASE_TO_ID%TYPE,
   	p_userid		IN		T412_INHOUSE_TRANSACTIONS.C412_CREATED_BY%TYPE,
   	p_txnid			OUT		T412_INHOUSE_TRANSACTIONS.C412_INHOUSE_TRANS_ID%TYPE
   )
   AS
   v_status		NUMBER:=2;
   v_company_id   t1900_company.c1900_company_id%TYPE;
   v_plant_id     t5040_plant_master.c5040_plant_id%TYPE;
   v_trans_company_id   t1900_company.c1900_company_id%TYPE;
   v_default_company   t1900_company.c1900_company_id%TYPE;
   v_trans_plant_id     t5040_plant_master.c5040_plant_id%TYPE;
   BEGIN
	   
	   		p_txnid := get_next_consign_id(p_type);
	   		
	   		IF p_status_fl IS NULL
	   		THEN
	   			SELECT DECODE(get_rule_value('CONTROL_REQUIRED',p_type),'Y',2,3) INTO v_status
	   				FROM DUAL;
	   		ELSE
	   			v_status := p_status_fl;
	   		END IF;
	   		
	   		SELECT  get_compid_frm_cntx()
			  INTO  v_company_id
			  FROM DUAL;
	
	        SELECT  get_plantid_frm_cntx()
			  INTO  v_plant_id
			  FROM DUAL;
			  
			--validate the company based on ref_id and saving ref company into inhouse trasnaction incase of company mismatch 
			IF p_refid IS NOT NULL THEN
			 gm_pkg_common.gm_fch_trans_comp_plant_id(p_refid,v_trans_company_id,v_trans_plant_id);
				IF (v_trans_company_id IS NOT NULL) AND (v_company_id != v_trans_company_id) THEN
          			v_company_id := v_trans_company_id;
       			END IF;
       		ELSE
       			SELECT get_plant_parent_comp_id(v_plant_id)
		          INTO v_default_company 
		          FROM DUAL;		          
			        IF v_default_company IS NOT NULL AND v_default_company !=  v_company_id THEN
					  v_company_id := v_default_company;
					END IF;    
	  		END IF;
	   		
			INSERT INTO T412_INHOUSE_TRANSACTIONS
				(
					C412_INHOUSE_TRANS_ID,
					C412_COMMENTS,
					C412_STATUS_FL,
					C412_INHOUSE_PURPOSE,
					C412_TYPE,
					C412_REF_ID,
					C901_REF_TYPE,
					C412_RELEASE_TO,
					C412_RELEASE_TO_ID,
					C412_CREATED_BY,
					C412_CREATED_DATE, 
					c1900_company_id, 
					c5040_plant_id
				)
				VALUES
				(
					p_txnid,
					p_comments,
					v_status,
					p_purpose,
					p_type,
					p_refid,
					p_reftype,
					p_shipto,
					p_shiptoid,
					p_userid,
					CURRENT_DATE, 
					v_company_id, 
					v_plant_id
				);
	   
	END gm_sav_inhouse_txn;

	
	/*
    * Description: This Procedure is common procedure used to create the Inhouse Transaction for comma separated string
    * Author:	Rajkumar
    */
   PROCEDURE gm_sav_inhouse_txn_commastring(
   		p_txnid IN 	T412_INHOUSE_TRANSACTIONS.C412_INHOUSE_TRANS_ID%TYPE,
   		p_str	IN	VARCHAR2,
   		p_userid IN		T412_INHOUSE_TRANSACTIONS.C412_CREATED_BY%TYPE
   )
   AS
   	v_string		VARCHAR2(2000) := p_str;
   	v_substring		VARCHAR2(2000);
   	v_pnum      	T413_INHOUSE_TRANS_ITEMS.C205_PART_NUMBER_ID%TYPE;
    v_qty           T413_INHOUSE_TRANS_ITEMS.C413_ITEM_QTY%TYPE;
    v_cnum   		T413_INHOUSE_TRANS_ITEMS.C413_CONTROL_NUMBER%TYPE;			
    v_price			T413_INHOUSE_TRANS_ITEMS.C413_ITEM_PRICE%TYPE;
   
   BEGIN

	   WHILE INSTR(v_string,'|') <> 0
	   LOOP
	   		v_substring := substr(v_string,0,INSTR(v_string,'|') - 1);
	   		v_string := substr(v_string,INSTR(v_string,'|') + 1);
	   		
	   		v_pnum	:= null;
	   		v_qty	:= null;
	   		v_cnum	:= null;
	   		
	  		v_pnum := substr(v_substring,0,INSTR(v_substring,',') - 1); 
	   		v_substring := substr(v_substring,INSTR(v_substring,',') + 1);
	   		v_qty	:= substr(v_substring,0,INSTR(v_substring,',') - 1);
	   		v_substring := substr(v_substring,INSTR(v_substring,',') + 1);
	   		v_cnum	:= substr(v_substring,0,INSTR(v_substring,',') - 1);
	   		v_substring := substr(v_substring,INSTR(v_substring,',') + 1);
	   		

   			v_price 	:= get_part_price (v_pnum, 'E');
	   		
	   		gm_sav_inhouse_txn_dtl (
	   			p_txnid,
	   			v_pnum,
	   			v_qty,
	   			v_cnum,
	   			v_price,
	   			NULL,
	   			p_userid
	   		);
	   END LOOP;
	   
	   --Whenever the detail is updated, the inhouse transactions table has to be updated ,so ETL can Sync it to GOP.
		UPDATE t412_inhouse_transactions 
		   SET c412_last_updated_by   = p_userid
		     , c412_last_updated_date = CURRENT_DATE
		 WHERE c412_inhouse_trans_id  = p_txnid;	
   END gm_sav_inhouse_txn_commastring;
	 /*
    * Description: This Procedure is common procedure used to create the Inhouse Transaction for Cap separated string
    * Author:	Rajkumar
    */
   PROCEDURE gm_sav_inhouse_txn_string(
   		p_txnid IN 	T412_INHOUSE_TRANSACTIONS.C412_INHOUSE_TRANS_ID%TYPE,
   		p_str	IN	VARCHAR2,
   		p_userid IN		T412_INHOUSE_TRANSACTIONS.C412_CREATED_BY%TYPE
   )
   AS
   	v_string		VARCHAR2(2000) := p_str;
   	v_substring		VARCHAR2(2000);
   	v_pnum      	T413_INHOUSE_TRANS_ITEMS.C205_PART_NUMBER_ID%TYPE;
    v_qty           T413_INHOUSE_TRANS_ITEMS.C413_ITEM_QTY%TYPE;
    v_cnum   		T413_INHOUSE_TRANS_ITEMS.C413_CONTROL_NUMBER%TYPE;			
    v_price			T413_INHOUSE_TRANS_ITEMS.C413_ITEM_PRICE%TYPE;
   BEGIN
	   
	   WHILE INSTR(v_string,'|') <> 0
	   LOOP
	   		v_substring := substr(v_string,0,INSTR(v_string,'|') - 1);
	   		v_string := substr(v_string,INSTR(v_string,'|') + 1);
	   		
	   		v_pnum	:= null;
	   		v_qty	:= null;
	   		v_cnum	:= null;
	   		
	   		v_pnum := substr(v_substring,0,INSTR(v_substring,'^') - 1); 
	   		v_substring := substr(v_substring,INSTR(v_substring,'^') + 1);
	   		v_cnum	:= substr(v_substring,0,INSTR(v_substring,'^') - 1);
	   		v_substring := substr(v_substring,INSTR(v_substring,'^') + 1);
	   		v_qty	:=	TO_NUMBER(v_substring);
	   		
	   		v_price 	:= get_part_price (v_pnum, 'E');
	   		
	   		gm_sav_inhouse_txn_dtl (
	   			p_txnid,
	   			v_pnum,
	   			v_qty,
	   			v_cnum,
	   			v_price,
	   			NULL,
	   			p_userid
	   		);
	   END LOOP;
	  	--Whenever the detail is updated, the inhouse transactions table has to be updated ,so ETL can Sync it to GOP.
		UPDATE t412_inhouse_transactions 
		   SET c412_last_updated_by   = p_userid
		     , c412_last_updated_date = CURRENT_DATE
		 WHERE c412_inhouse_trans_id  = p_txnid;
		 
   END gm_sav_inhouse_txn_string;
	
    /*
    * Description: This Procedure is common procedure used to create the Inhouse Transaction
    * Author:	Rajkumar
    */
   PROCEDURE gm_sav_inhouse_txn_dtl(
   	p_txnid			IN		T412_INHOUSE_TRANSACTIONS.C412_INHOUSE_TRANS_ID%TYPE,
	p_part_num      IN   	T413_INHOUSE_TRANS_ITEMS.C205_PART_NUMBER_ID%TYPE,
    p_qty           IN   	T413_INHOUSE_TRANS_ITEMS.C413_ITEM_QTY%TYPE,
    p_control_num   IN   	T413_INHOUSE_TRANS_ITEMS.C413_CONTROL_NUMBER%TYPE,
    p_price         IN   	T413_INHOUSE_TRANS_ITEMS.C413_ITEM_PRICE%TYPE,
    p_status		IN		T413_INHOUSE_TRANS_ITEMS.C413_STATUS_FL%TYPE,
   	p_userid		IN		T412_INHOUSE_TRANSACTIONS.C412_CREATED_BY%TYPE
   )
   AS
   BEGIN
	   
     UPDATE T413_INHOUSE_TRANS_ITEMS
       SET C413_ITEM_QTY = NVL (C413_ITEM_QTY, 0) + p_qty
       	 , c413_last_updated_by = p_userid
	   	 , c413_last_updated_date = CURRENT_DATE       
      WHERE C412_INHOUSE_TRANS_ID = p_txnid
         AND C205_PART_NUMBER_ID = p_part_num
         AND NVL(C413_CONTROL_NUMBER,'TBE') = NVL(p_control_num,'TBE')
         AND NVL(C413_ITEM_PRICE,0) = NVL(p_price,0);
         
      IF (SQL%ROWCOUNT = 0)
      THEN   
	   INSERT INTO T413_INHOUSE_TRANS_ITEMS 
	   		(
	   			C413_TRANS_ID,
	   			C413_CONTROL_NUMBER,
	   			C413_ITEM_QTY,
	   			C413_ITEM_PRICE,
	   			C205_PART_NUMBER_ID,
	   			C412_INHOUSE_TRANS_ID,
	   			C413_STATUS_FL,
	   			C413_LAST_UPDATED_BY,
	   			C413_LAST_UPDATED_DATE
	   		)
	   		VALUES
	   		(
	   			s413_inhouse_item.NEXTVAL,
	   			p_control_num,
	   			p_qty,
	   			p_price,
	   			p_part_num,
	   			p_txnid,
	   			p_status,
	   			p_userid,
	   			CURRENT_DATE
	   		);
	   END IF;
	END gm_sav_inhouse_txn_dtl;

/************************************************************************************************************
    * Description: This Procedure is fetch consignment/request id based on OUS country from code lookup table.
    * Author     : Velu
**************************************************************************************************************/
	
   PROCEDURE gm_fetch_ous_trans_id(
   	 p_ref_id	IN	OUT   VARCHAR2,
	 p_dist_id  IN  t701_distributor.c701_distributor_id%TYPE,
	 p_userid	IN	t520_request.c520_created_by%TYPE
   )
   AS
		v_temp_ref_id   VARCHAR(50);
		v_ref_str		VARCHAR(50);
		
   BEGIN
	     v_temp_ref_id := p_ref_id;
 		 
	     SELECT SUBSTR (v_temp_ref_id, 1, INSTR (v_temp_ref_id, '-')) INTO v_ref_str  FROM DUAL;
		 
	     SELECT REPLACE(v_temp_ref_id,v_ref_str,NVL(get_code_nm_alt_from_distid(p_dist_id),'GM')) INTO v_temp_ref_id FROM DUAL;
 					  
		 p_ref_id := v_temp_ref_id;
		
   END gm_fetch_ous_trans_id;

/************************************************************************************************************
    * Description: This Procedure is validate request to move to common pool.
    * Author     : Gopinathan
**************************************************************************************************************/   
	PROCEDURE gm_move_req_to_compool (
	p_request_id IN t520_request.c520_request_id%TYPE,
	p_user_id    IN t520_request.c520_created_by%TYPE)
	AS
	    v_status 			t520_request.c520_status_fl%TYPE;
	    v_demandmasterid 	t520_request.c520_request_txn_id%TYPE;
	    v_type				t906_rules.c906_rule_value%TYPE;
	    v_setid 			t520_request.c207_set_id%TYPE;
	    v_out_msg				VARCHAR2(4000);
	    v_required_date	    DATE;
	    v_req_src			t520_request.C901_REQUEST_SOURCE%TYPE;
	   
	BEGIN
	    BEGIN
	         SELECT c520_status_fl, c520_request_txn_id, c207_set_id,C520_REQUIRED_DATE,C901_REQUEST_SOURCE
	           INTO v_status, v_demandmasterid, v_setid, v_required_date,v_req_src
	           FROM t520_request
	          WHERE c520_request_id = p_request_id
	            AND c520_void_fl   IS NULL
	            FOR UPDATE;
		    EXCEPTION
		    WHEN NO_DATA_FOUND THEN
		        raise_application_error ( -20903, '');
	    END;
	    
	    -- If the Request is More than Ready to Consign, throw an error.
	    IF v_status > 20 THEN
	        raise_application_error ( -20598, '');
	    ELSIF v_status = 10 THEN
	        -- if status is back order then void the request.
	        gm_pkg_common_cancel.gm_cs_sav_void_request (p_request_id, p_user_id) ;
	    ELSE
	    -- If the Request is already in common Pool, throw exception.
	    IF v_required_date IS NULL AND v_demandmasterid IS NULL
	    THEN
	    	 raise_application_error ( -20599, '');
	   	END IF;
	   	
	   	-- Src is Order Planning or Set PAR then proceed, else throw an error  
	    IF NVL(v_req_src,'-9999') != '50616' AND NVL(v_req_src,'-9999') != '4000122' 
	    THEN
	    	raise_application_error ( -20600, '');
	    END IF;
	    
	      -- Update the Request txn id and the required date (this procedure moves request to common pool).
	      gm_pkg_oppr_ld_no_stack.gm_op_request_action(p_request_id,p_user_id,v_out_msg);
	      gm_pkg_oppr_ld_no_stack.gm_op_nostack_email (v_demandmasterid, p_user_id, v_setid, v_out_msg);
			
	    END IF;	    
	   
	END gm_move_req_to_compool;
END gm_pkg_op_account_req_master;
/