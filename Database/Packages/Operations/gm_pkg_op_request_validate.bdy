/* Formatted on 2008/11/18 15:21 (Formatter Plus v4.8.0) */
-- @"c:\database\packages\operations\gm_pkg_op_request_validate.bdy"

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_request_validate
IS
/******************************************************************
	 Description : This procedure is used to validate the status of
	 the request which need to be swapped
	****************************************************************/
	PROCEDURE gm_swap_req_validate (
		p_reqfrom	IN	 t520_request.c520_request_id%TYPE
	  , p_reqto 	IN	 t520_request.c520_request_id%TYPE
	)
	AS
		v_count 	   NUMBER;
		v_setid_from   VARCHAR2 (20);
		v_setid_to	   VARCHAR2 (20);
		v_txn_id_from  VARCHAR2 (40);
		v_txn_id_to    VARCHAR2 (40);
		v_company_id   t1900_company.c1900_company_id%TYPE;
		v_plant_id	   t5040_plant_master.c5040_plant_id%TYPE;
		v_filter 	   T906_RULES.C906_RULE_VALUE%TYPE;
	BEGIN
		SELECT get_compid_frm_cntx(), get_plantid_frm_cntx() INTO v_company_id, v_plant_id FROM DUAL;
		SELECT NVL2(GET_RULE_VALUE(v_company_id,'REQUEST_SWAP'),v_plant_id,v_company_id) --REQUEST_SWAP:=rule value-plant
 			INTO v_filter 
			FROM DUAL;
		SELECT COUNT (1)
		  INTO v_count
		  FROM t520_request t520
		 WHERE c520_request_id = p_reqfrom
		   AND t520.c520_status_fl > 0
		   AND t520.c520_status_fl < 40
		   AND t520.c520_void_fl IS NULL
		   AND (t520.c1900_company_id = v_filter OR t520.c5040_plant_id = v_filter);

		IF (v_count = 0)
		THEN
			raise_application_error (-20114, p_reqfrom);
		END IF;

		SELECT COUNT (1)
		  INTO v_count
		  FROM t520_request t520
		 WHERE c520_request_id = p_reqto
		   AND t520.c520_status_fl > 0
		   AND t520.c520_status_fl < 40
		   AND t520.c520_void_fl IS NULL
		   AND (t520.c1900_company_id = v_filter OR t520.c5040_plant_id = v_filter);

		IF (v_count = 0)
		THEN
			raise_application_error (-20115, p_reqto);	-- request can't be completed for swap
		END IF;

		SELECT COUNT (1)
		  INTO v_count
		  FROM t520_request t520
		 WHERE c520_request_id = p_reqfrom
		   AND t520.c520_status_fl > 0
		   AND t520.c520_status_fl < 40
		   AND t520.c520_void_fl IS NULL
		   AND c520_master_request_id IS NOT NULL
		   AND (t520.c1900_company_id = v_filter OR t520.c5040_plant_id = v_filter);

		IF (v_count > 0)
		THEN
			raise_application_error (-20116, p_reqfrom);
		END IF;

		SELECT COUNT (1)
		  INTO v_count
		  FROM t520_request t520
		 WHERE c520_request_id = p_reqto
		   AND t520.c520_status_fl > 0
		   AND t520.c520_status_fl < 40
		   AND t520.c520_void_fl IS NULL
		   AND c520_master_request_id IS NOT NULL
		   AND (t520.c1900_company_id = v_filter OR t520.c5040_plant_id = v_filter);

		IF (v_count > 0)
		THEN
			raise_application_error (-20117, p_reqto);	-- request can't be child request for swap
		END IF;

		SELECT t520.c207_set_id, t520.c520_request_txn_id
		  INTO v_setid_from, v_txn_id_from
		  FROM t520_request t520
		 WHERE c520_request_id = p_reqfrom
		   AND t520.c520_status_fl > 0
		   AND t520.c520_status_fl < 40
		   AND t520.c520_void_fl IS NULL;

		SELECT t520.c207_set_id, t520.c520_request_txn_id
		  INTO v_setid_to, v_txn_id_to
		  FROM t520_request t520
		 WHERE c520_request_id = p_reqto
		   AND t520.c520_status_fl > 0
		   AND t520.c520_status_fl < 40
		   AND t520.c520_void_fl IS NULL;

		IF (v_setid_from != v_setid_to)
		THEN
			raise_application_error (-20122, p_reqto);	--set ID can't be different for swap
		END IF;

		/*  Remove the below Validation that occurs when trying to swap with the same sheet name in Request Swap screen for PMT-5248 */

		/*IF (v_txn_id_from = v_txn_id_to)
		THEN
			raise_application_error (-20123, '');	--Txn ID can't be the same for swap
		END IF;
		*/
		
	END gm_swap_req_validate;

	/******************************************************************
	 Description : This procedure is used to validate the consignment
	 process
	****************************************************************/
	PROCEDURE gm_op_consigment_validate (
		p_req_id	IN	 t520_request.c520_request_id%TYPE
	  , p_dist_id	IN	 t701_distributor.c701_distributor_id%TYPE
	)
	AS
		v_count 	   NUMBER;
		v_txn_id	   t520_request.c520_request_txn_id%TYPE;
		v_source	   t520_request.c901_request_source%TYPE;
		v_demand_type  t4020_demand_master.c901_demand_type%TYPE;
		v_region	   t701_distributor.c701_region%TYPE;
		v_company_id   t4020_demand_master.c901_company_id%TYPE;
		v_globalshtmap_id t4020_demand_master.c4015_global_sheet_map_id%TYPE;
		v_level_id 		  t4015_global_sheet_mapping.c901_level_id%TYPE;
		v_level_value 	  t4015_global_sheet_mapping.c901_level_value%TYPE;
		v_dist_country_id	 t701_distributor.c701_bill_country%TYPE;
		v_sheet_country_id	   t4016_global_inventory_mapping.c901_level_value%TYPE;
		v_edc_entity		t906_rules.c906_rule_value%TYPE;
		v_edc_dist			t906_rules.c906_rule_ID%TYPE;
		v_zone_flag			t4015_global_sheet_mapping.c901_level_value%TYPE;
	BEGIN
		SELECT c520_request_txn_id, c901_request_source
		  INTO v_txn_id, v_source
		  FROM t520_request
		 WHERE c520_request_id = p_req_id;

		-- v_txn_id  will be null when no stack procedure move the set to common pool 
		IF (v_source != 50616 OR v_txn_id IS NULL )
		THEN
			RETURN;
		END IF;
	
		SELECT c901_demand_type, c901_company_id, c4015_global_sheet_map_id
		  INTO v_demand_type, v_company_id, v_globalshtmap_id
		  FROM t4020_demand_master
		 WHERE c4020_demand_master_id = v_txn_id;
		
		IF v_globalshtmap_id IS NOT NULL
		THEN
			BEGIN
			-- Get the Country ID for the Demand Sheet. It(Country ID) is available only in T4016 Table.
				SELECT t4016.C901_LEVEL_VALUE INTO v_sheet_country_id
				FROM t4015_global_sheet_mapping t4015, t4016_global_inventory_mapping t4016
				WHERE t4015.c4016_global_inventory_map_id = t4016.c4016_global_inventory_map_id
				AND   t4015.C4015_GLOBAL_SHEET_MAP_ID = v_globalshtmap_id;
			EXCEPTION WHEN OTHERS 
			THEN
				NULL;
			END;
				
		END IF;
		
		IF (v_demand_type = 40021)	 --consignment
		THEN
			BEGIN
				IF (p_dist_id IS NULL)
				THEN
					RETURN;
				END IF;
				
	            BEGIN
					SELECT c701_region ,C701_SHIP_COUNTRY 
					  INTO v_region , v_dist_country_id  
					  FROM t701_distributor
				     WHERE c701_distributor_id = p_dist_id;
					EXCEPTION
						WHEN NO_DATA_FOUND
						THEN
							raise_application_error (-20124, '');
				END;				
				
				/* Validate the request while update dist. based on linked sheet global mapping.
				 * For Example if the forecast is for US and if user trying to ship the set to Germany,it throw the exception.  */
				IF v_globalshtmap_id IS NOT NULL THEN --Sheet with global mapping id
					BEGIN
						SELECT c901_level_id, c901_level_value INTO v_level_id, v_level_value
						  FROM t4015_global_sheet_mapping
			      		 WHERE c4015_global_sheet_map_id = v_globalshtmap_id
			               AND c4015_void_fl  IS NULL;
			           EXCEPTION 
			           		WHEN NO_DATA_FOUND THEN
			           			raise_application_error (-20124, '');
		            END;
		         
		            IF (v_level_id = 102583) THEN
		           	/* To check Zone level */
		            	BEGIN
			            	
							SELECT C906_RULE_VALUE into v_zone_flag FROM T906_RULES WHERE C906_RULE_GRP_ID='GOP_ROLLUP_EDIT'
							AND C906_VOID_FL IS NULL
							AND C906_RULE_ID=v_level_value;
							-- To make Zone level editable PC-3907
						
							EXCEPTION WHEN OTHERS
						   THEN
						   		v_zone_flag:=NULL;
						   END;
						END IF;
						
		            IF (v_level_id = 102584 OR v_zone_flag IS NOT NULL) THEN --102584 region and zone level check
						
		            	BEGIN
			            	-- Check if the Demand Sheet Country is part of EDC Entity.
			            	SELECT C906_RULE_ID INTO v_edc_entity FROM T906_RULES
			            	WHERE C906_RULE_GRP_ID='SALES_INV_CNTY_MAP'
			            	AND  C906_RULE_VALUE=v_sheet_country_id;
			            	
			            	SELECT C906_RULE_ID INTO v_edc_dist FROM T906_RULES
			            	WHERE C906_RULE_GRP_ID='SALES_INV_CNTY_MAP'
			            	AND  C906_RULE_VALUE=v_dist_country_id;
			            	
		            	EXCEPTION WHEN OTHERS THEN
		            		v_edc_entity := -1;
		            		v_edc_dist := 0;

			            END;
			            
		            	-- If the Distributor is part of EDC and the RQ is for an EDC entity, then allow it to ship.
		            	IF v_edc_entity  = v_edc_dist
		            	THEN
		            		RETURN;	
		            	ELSIF v_level_value = v_region THEN
							RETURN;
						ELSE
							raise_application_error (-20124, '');
						END IF;
						
				-- Commenting the company id check to allow US Order planning Request should be shipped to any Rep in US Region
				
					ELSIF v_level_id = 102581 THEN -- 102581 Division
						SELECT COUNT(1) INTO v_count
						  FROM v700_territory_mapping_detail 
						  WHERE region_id = v_region 
					--	    AND compid = v_company_id
						    AND divid = 100823; -- US
						    
						IF (v_count = 0)
						THEN
							raise_application_error (-20124, '');
						END IF;					    
					END IF;
				ELSE --Sheet without global mapping id
					SELECT COUNT (1)
					  INTO v_count
					  FROM t4021_demand_mapping t4021
					 WHERE t4021.c4020_demand_master_id = v_txn_id
					   AND t4021.c901_ref_type = 40032	 --region
					   AND t4021.c4021_ref_id = TO_CHAR(v_region);
				
					IF (v_count = 0) THEN
						raise_application_error (-20124, '');
					END IF;
				END IF;
			END;

		ELSIF (v_demand_type = 40022)	--in house consignment
		THEN
			IF (p_dist_id IS NOT NULL AND p_dist_id != '01')   ---'01' Globus - Med consignment
			THEN
				raise_application_error (-20126, '');
			END IF;
		END IF;
	END gm_op_consigment_validate;
END gm_pkg_op_request_validate;
/
