/* Formatted on 2009/11/25 13:32 (Formatter Plus v4.8.0) */
-- @"C:\Database\Packages\Operations\gm_pkg_op_tag_trans.bdy"
/*******************************************************
* Description  : Procedure to update the control number
* Author   : Velu
*******************************************************/
CREATE OR REPLACE
PACKAGE BODY gm_pkg_op_tag_trans
IS
    --
PROCEDURE gm_sav_tag_control_num (
        p_partnum     IN t205_part_number.c205_part_number_id%TYPE,
        p_ref_id      IN t505_item_consignment.c504_consignment_id%TYPE,
        p_control_num IN t5010_tag.c5010_control_number%TYPE,
        p_userid      IN t504_consignment.c504_last_updated_by%TYPE)
AS
    v_taggable_part t205d_part_attribute.c205d_attribute_value%TYPE;
    v_oldcontrolnum t5010_tag.c5010_control_number%TYPE;

BEGIN

	SELECT get_part_attribute_value (p_partnum, 92340)
    INTO v_taggable_part
    FROM DUAL;
    
    IF v_taggable_part = 'Y' THEN
        BEGIN
            SELECT c5010_control_number
            INTO v_oldcontrolnum
            FROM t5010_tag
            WHERE c205_part_number_id           = p_partnum
                AND c5010_last_updated_trans_id = p_ref_id
                AND C5010_VOID_FL IS NULL
		AND ROWNUM = 1;	
                --add rownum = 1, in one scenario, could be two same part in one set.
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            RETURN;
        END;
    END IF;
    
    IF v_oldcontrolnum <> p_control_num THEN
        UPDATE t5010_tag t5010
        SET t5010.c5010_control_number          = p_control_num,
        	t5010.c5010_last_updated_by 		= p_userid,
            t5010.c5010_last_updated_date       = SYSDATE
        WHERE t5010.c5010_last_updated_trans_id = p_ref_id
            AND t5010.c205_part_number_id       = p_partnum
            AND t5010.c5010_control_number      = v_oldcontrolnum;
    END IF;
    
END gm_sav_tag_control_num;
END gm_pkg_op_tag_trans;
/
