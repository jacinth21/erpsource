--@"c:\database\packages\operations\gm_pkg_op_inv_warehouse_loc_txn.bdy";
CREATE OR REPLACE PACKAGE BODY gm_pkg_op_inv_warehouse_loc_txn
/********************************************************************************
* Description : Procedure used to save the Activate/InActivate Location
*********************************************************************************/ IS

    PROCEDURE gm_upd_warhouse_location (
        p_location_id        IN    VARCHAR2,
        p_status             IN    t5052_location_master.c901_status%TYPE,
        p_user_id            IN    t5052_location_master.c5052_last_updated_by%TYPE,
        p_warehouse_id       IN    t5052_location_master.c5051_inv_warehouse_id%TYPE,
        p_out_loc_lst_succ   OUT   VARCHAR2,
        p_out_loc_lst_err    OUT   VARCHAR2
    ) AS

        v_qty               NUMBER(20);
        v_error             VARCHAR2(4000);
        v_out_error         VARCHAR2(4000);
        v_success           VARCHAR2(4000);
        v_out_success       VARCHAR2(4000);
        v_string            VARCHAR2(4000) := p_location_id;
        v_substring         VARCHAR2(4000);
        v_location_id       t5052_location_master.c5052_location_id%TYPE;
        v_plant_id          t5040_plant_master.c5040_plant_id%TYPE;
        v_logged_plant_id   t5040_plant_master.c5040_plant_id%TYPE;
        v_warehouse_id      t5052_location_master.c5051_inv_warehouse_id%TYPE;
        v_status            t5052_location_master.c901_status%TYPE;
        v_err_cnt           NUMBER(20) := 0;
        v_last_string        VARCHAR2(4000) := v_string ;
        v_active_ids        VARCHAR2(4000);
        v_inactive_ids      VARCHAR2(4000);
        v_invalid_ids        VARCHAR2(4000);
        v_initiated_ids     VARCHAR2(4000);
        v_qty_ids               VARCHAR2(4000);
    BEGIN
    v_last_string := substr(v_last_string, length(v_last_string), length(v_last_string));
    
    IF  v_last_string != ',' THEN
    v_string := v_string || ',';
    END IF;
  
   --getting company plant_id 
        SELECT
            get_plantid_frm_cntx()
        INTO v_logged_plant_id
        FROM
            dual;

        WHILE instr(v_string, ',') <> 0 LOOP
         v_err_cnt :=0;
         v_location_id :=NULL;
         v_status := NULL;
         v_qty :=0;
            v_substring := substr(v_string, 1, instr(v_string, ',') - 1);
            v_string := substr(v_string, instr(v_string, ',') + 1);
            v_location_id := v_substring;
            
             -- Getting ware house id,status,plant id for checking valid warehouse,status,company
            BEGIN
			    SELECT
			        c5051_inv_warehouse_id,
			        c901_status,
			        c5040_plant_id
			    INTO
			        v_warehouse_id,
			        v_status,
			        v_plant_id
			    FROM
			        t5052_location_master
			    WHERE
			        c5052_location_cd = v_location_id
			        AND c5051_inv_warehouse_id = p_warehouse_id
			        AND c5040_plant_id = v_logged_plant_id
			        AND c5052_void_fl IS NULL;
			
			EXCEPTION
			    WHEN no_data_found THEN
			        v_invalid_ids := v_invalid_ids|| v_location_id ||',';
			        v_err_cnt := v_err_cnt + 1;
			END;
         
            -- Getting count for checking Invalid allocated qty
            IF v_warehouse_id IS NOT NULL THEN 
            
            SELECT
    COUNT(*) INTO v_qty
FROM
    (
        SELECT
            ( t5053.c5053_curr_qty - gm_pkg_op_item_control_rpt.get_part_location_cur_qty(t5053.c205_part_number_id, t5052.c5052_location_id
            , NULL, 3000) ) allocqty,
            gm_pkg_op_item_control_rpt.get_part_location_cur_qty(t5053.c205_part_number_id, t5052.c5052_location_id, NULL, 3000) currqty
        FROM
            t5052_location_master         t5052,
            t5053_location_part_mapping   t5053,
            t5057_building                t5057
        WHERE
            t5052.c5052_location_id = t5053.c5052_location_id (+)
            AND t5052.c5051_inv_warehouse_id NOT IN (
                SELECT
                    c906_rule_value
                FROM
                    t906_rules
                WHERE
                    c906_rule_id = 'WHID'
                    AND c906_rule_grp_id = 'RULEWH'
                    AND c906_void_fl IS NULL
            )
            AND t5052.c5052_void_fl IS NULL
            AND t5052.c5057_building_id = t5057.c5057_building_id (+)
            AND t5057.c5057_void_fl IS NULL
            AND REGEXP_LIKE ( nvl(t5052.c5052_location_cd, t5052.c5052_location_id),
                              CASE
                                  WHEN upper(v_location_id) IS NOT NULL THEN
                                      upper(v_location_id)
                                  ELSE
                                      nvl(t5052.c5052_location_cd, t5052.c5052_location_id)
                              END
            )
            AND t5052.c901_location_type IN (
                93320,
                93336
            )
            AND t5052.c5040_plant_id = v_logged_plant_id
        ORDER BY
            t5052.c5052_location_id
    ) t5053
WHERE
    t5053.currqty > 0
    OR t5053.allocqty > 0;
            
            IF p_status = v_status THEN
                IF p_status = 93311 THEN --Inactive
                v_inactive_ids := v_inactive_ids || v_location_id || ',';
                ELSE 
                v_active_ids := v_active_ids || v_location_id || ',';
                END IF;
               v_err_cnt := v_err_cnt + 1;
            END IF;
          
            IF v_status = 93202 THEN
                v_initiated_ids := v_initiated_ids || v_location_id || ',';
                v_err_cnt := v_err_cnt + 1;
            END IF;
            

            IF v_qty > 0 THEN
                v_qty_ids := v_qty_ids || v_location_id || ',';
                v_err_cnt := v_err_cnt + 1;
            END IF;
            
          

            IF v_err_cnt = 0 THEN
    --Update status in t5052 table if the location qty and allocated qty > 0 and location is Active
                UPDATE t5052_location_master
                SET
                    c901_status = p_status,
                    c5052_last_updated_by = p_user_id,
                    c5052_last_updated_date = current_date
                WHERE
                    c5052_location_cd = v_location_id
                    AND c5040_plant_id = v_plant_id
                    AND c5051_inv_warehouse_id = p_warehouse_id
                    AND c5052_void_fl IS NULL;
                v_success := v_success
                             || v_location_id
                             || ',';
            END IF;
            
       
         END IF;
       
        END LOOP;
        
    
      IF v_invalid_ids IS NOT NULL THEN 
         v_invalid_ids := substr(v_invalid_ids, 1, length(v_invalid_ids) - 1);
         v_invalid_ids := 'The Entered location id '|| v_invalid_ids || ' is/are not valid or not from selected Warehouse or not from selected plant.';
      END IF;
      
      IF v_inactive_ids IS NOT NULL THEN
         v_inactive_ids := substr(v_inactive_ids, 1, length(v_inactive_ids) - 1);
         v_inactive_ids := 'The Entered location id '|| v_inactive_ids || ' is/are already inactive.';
      END IF;
      
      IF v_active_ids IS NOT NULL THEN
         v_active_ids := substr(v_active_ids, 1, length(v_active_ids) - 1);
         v_active_ids := 'The Entered location id '|| v_active_ids || ' is/are already active.';
      END IF;
      
      IF v_initiated_ids IS NOT NULL THEN
         v_initiated_ids := substr(v_initiated_ids, 1, length(v_initiated_ids) - 1);
         v_initiated_ids := 'The Entered location id '|| v_initiated_ids || ' is/are in Initiated.';
      END IF;
      
      IF v_qty_ids IS NOT NULL THEN
         v_qty_ids := substr(v_qty_ids, 1, length(v_qty_ids) - 1);
         v_qty_ids := 'The Entered location id '|| v_qty_ids || ' has Current Qty parts Or Allocated parts.';
      END IF;
      
      v_error := v_invalid_ids || v_inactive_ids || v_active_ids || v_initiated_ids || v_qty_ids;
      
        IF v_error IS NOT NULL THEN 
            p_out_loc_lst_err := v_error;
        END IF;
        IF v_success IS NOT NULL THEN
            v_success := substr(v_success, 1, length(v_success) - 1);
            IF p_status = 93310 THEN
             p_out_loc_lst_succ := 'The Entered Location id ' || v_success || ' is/are Activated Successfully';
            END IF;
            IF p_status = 93311 THEN
             p_out_loc_lst_succ := 'The Entered Location id ' || v_success || ' is/are Inactivated Successfully';
            END IF;
        END IF;
    
    END gm_upd_warhouse_location;
    
/********************************************************************************
* Description : Procedure used to save the Bulk Location Transfer
*********************************************************************************/

    PROCEDURE gm_sav_bulk_location (
        p_input_str       IN    CLOB,
        p_ware_house_id   IN    t5051_inv_warehouse.c5051_inv_warehouse_id%TYPE,
        p_user_id         IN    t101_user.c101_user_id%TYPE,
        p_out             OUT   VARCHAR2
    ) AS

        v_strlen           NUMBER := nvl(length(p_input_str), 0);
        v_string           VARCHAR2(4000) := p_input_str;
        v_substring        VARCHAR2(4000);
        v_partnum          t205_part_number.c205_part_number_id%TYPE;
        v_old_loc_id       t5052_location_master.c5052_location_id%TYPE;
        v_new_loc_id       t5052_location_master.c5052_location_id%TYPE;
        v_old_loc_cd       t5052_location_master.c5052_location_cd%TYPE;
        v_new_loc_cd       t5052_location_master.c5052_location_cd%TYPE;
        v_message          VARCHAR2(4000);
        v_plant_id         t5040_plant_master.c5040_plant_id%TYPE;
        v_inactive_ids     VARCHAR2(4000);
        v_invalid_ids      VARCHAR2(4000);
        v_err_cnt          NUMBER(20) := 0;
        v_allocqty         NUMBER(20);
        v_currqty          NUMBER(20);
        v_new_currqty      NUMBER(20);
        v_old_currqty      NUMBER(20);
        v_qty_ids          VARCHAR2(4000);
        v_out_flag         VARCHAR2(10);
        v_trans_id         t5053_location_part_mapping.c5053_last_update_trans_id%TYPE;
        v_trans_type       t5053_location_part_mapping.c901_last_transaction_type%TYPE;
        v_old_trans_id     t5053_location_part_mapping.c5053_last_update_trans_id%TYPE;
        v_old_trans_type   t5053_location_part_mapping.c901_last_transaction_type%TYPE;
    BEGIN
--getting company plant_id 
        SELECT
            get_plantid_frm_cntx()
        INTO v_plant_id
        FROM
            dual;
      -- Loop the input string(v_str)
        WHILE instr(v_string, '|') <> 0 LOOP
            v_substring := substr(v_string, 1, instr(v_string, '|') - 1);

            v_string := substr(v_string, instr(v_string, '|') + 1);
            v_partnum := NULL;
            v_old_loc_cd := NULL;
            v_new_loc_cd := NULL;
            v_old_loc_id := NULL;
            v_new_loc_id := NULL;
            v_err_cnt := 0;
            v_allocqty := 0;
            v_currqty := 0;
            v_new_currqty := 0;
            v_old_currqty := 0;
            v_partnum := substr(v_substring, 1, instr(v_substring, '^') - 1);

            v_substring := substr(v_substring, instr(v_substring, '^') + 1);
            v_old_loc_cd := substr(v_substring, 1, instr(v_substring, '^') - 1);

            v_substring := substr(v_substring, instr(v_substring, '^') + 1);
            v_new_loc_cd := v_substring;
            BEGIN
            -- Getting new location id,qty,trans_id,trans_type
                SELECT
                    t5052.c5052_location_id,
                    t5053.c5053_curr_qty,
                    t5053.c5053_last_update_trans_id,
                    t5053.c901_last_transaction_type
                INTO
                    v_new_loc_id,
                    v_new_currqty,
                    v_trans_id,
                    v_trans_type
                FROM
                    t5052_location_master         t5052,
                    t5053_location_part_mapping   t5053,
                    t5057_building                t5057
                WHERE
                    t5052.c5052_location_id = t5053.c5052_location_id (+)
                    AND t5052.c5051_inv_warehouse_id NOT IN (
                        SELECT
                            c906_rule_value
                        FROM
                            t906_rules
                        WHERE
                            c906_rule_id = 'WHID'
                            AND c906_rule_grp_id = 'RULEWH'
                            AND c906_void_fl IS NULL
                    )
                    AND t5052.c5052_void_fl IS NULL
                    AND t5052.c5057_building_id = t5057.c5057_building_id (+)
                    AND t5057.c5057_void_fl IS NULL
                    AND REGEXP_LIKE ( nvl(t5052.c5052_location_cd, t5052.c5052_location_id),
                                      CASE
                                          WHEN upper(v_new_loc_cd) IS NOT NULL THEN
                                              upper(v_new_loc_cd)
                                          ELSE
                                              nvl(t5052.c5052_location_cd, t5052.c5052_location_id)
                                      END
                    )
                    AND t5052.c901_location_type IN (
                        93320,
                        93336
                    )
                    AND t5052.c5040_plant_id = v_plant_id
                    AND t5053.c205_part_number_id = v_partnum
                ORDER BY
                    t5052.c5052_location_id;

            EXCEPTION
                WHEN no_data_found THEN
                    v_invalid_ids := 'The Entered  Location Id '
                                     || v_new_loc_cd
                                     || ' is from not selected warehouse or selected plant or inactive.';
                    v_err_cnt := v_err_cnt + 1;
            END;

            BEGIN
            -- Getting old location id,qty,trans_id,trans_type
                SELECT
                    t5052.c5052_location_id,
                    t5053.c5053_curr_qty,
                    t5053.c5053_last_update_trans_id,
                    t5053.c901_last_transaction_type,
                    ( t5053.c5053_curr_qty - gm_pkg_op_item_control_rpt.get_part_location_cur_qty(t5053.c205_part_number_id, t5052
                    .c5052_location_id, NULL, v_plant_id) ) allocqty,
                    gm_pkg_op_item_control_rpt.get_part_location_cur_qty(t5053.c205_part_number_id, t5052.c5052_location_id, NULL
                    , v_plant_id) currqty
                INTO
                    v_old_loc_id,
                    v_old_currqty,
                    v_old_trans_id,
                    v_old_trans_type,
                    v_allocqty,
                    v_currqty
                FROM
                    t5052_location_master         t5052,
                    t5053_location_part_mapping   t5053,
                    t5057_building                t5057
                WHERE
                    t5052.c5052_location_id = t5053.c5052_location_id (+)
                    AND t5052.c5051_inv_warehouse_id NOT IN (
                        SELECT
                            c906_rule_value
                        FROM
                            t906_rules
                        WHERE
                            c906_rule_id = 'WHID'
                            AND c906_rule_grp_id = 'RULEWH'
                            AND c906_void_fl IS NULL
                    )
                    AND t5052.c5052_void_fl IS NULL
                    AND t5052.c5057_building_id = t5057.c5057_building_id (+)
                    AND t5057.c5057_void_fl IS NULL
                    AND REGEXP_LIKE ( nvl(t5052.c5052_location_cd, t5052.c5052_location_id),
                                      CASE
                                          WHEN upper(v_old_loc_cd) IS NOT NULL THEN
                                              upper(v_old_loc_cd)
                                          ELSE
                                              nvl(t5052.c5052_location_cd, t5052.c5052_location_id)
                                      END
                    )
                    AND t5052.c901_location_type IN (
                        93320,
                        93336
                    )
                    AND t5052.c5040_plant_id = v_plant_id
                    AND t5053.c205_part_number_id = v_partnum
                ORDER BY
                    t5052.c5052_location_id;

            EXCEPTION
                WHEN no_data_found THEN
                    v_invalid_ids := 'The Entered Old Location Id '
                                     || v_old_loc_cd
                                     || ' is from not selected warehouse or selected plant or inactive.';
                    v_err_cnt := v_err_cnt + 1;
            END;
    
            -- checking valid location
            IF v_new_loc_id IS NOT NULL AND v_old_loc_id IS NOT NULL THEN
            -- checking current quantity 
                IF v_currqty = 0 THEN
                    v_qty_ids := 'The Entered Location id '
                                 || v_old_loc_cd
                                 || ' has 0 Current Qty.';
                    v_err_cnt := v_err_cnt + 1;
                END IF;
            -- checking allocated quantity 
                IF v_allocqty > 0 THEN
                    v_qty_ids := 'The Entered Location id '
                                 || v_old_loc_cd
                                 || ' has Allocated Qty.';
                    v_err_cnt := v_err_cnt + 1;
                END IF;

            END IF;
            
            -- checking error count
            IF v_err_cnt = 0 THEN
    --Update current qty for new location in t5053_location_part_mapping table 
              --  v_new_currqty := v_old_currqty + v_new_currqty;
                UPDATE t5053_location_part_mapping
                SET
                    c5053_curr_qty = (NVL (v_old_currqty, 0) + NVL (v_new_currqty, 0)),
                    c5053_last_updated_by = p_user_id,
                    c5053_last_update_trans_id = v_trans_id,
                    c901_last_transaction_type = v_trans_type
                WHERE
                    c5052_location_id = v_new_loc_id
                    AND c205_part_number_id = v_partnum;
                    
               --Update current qty for old location in t5055_control_number table 
               
                UPDATE t5053_location_part_mapping
                SET
                    c5053_curr_qty = 0,
                    c5053_last_updated_by = p_user_id,
                    c5053_last_update_trans_id = v_old_trans_id,
                    c901_last_transaction_type = v_old_trans_type
                WHERE
                    c5052_location_id = v_old_loc_id
                    AND c205_part_number_id = v_partnum;
            
            --Update current qty in t5055_control_number table 

                UPDATE t5055_control_number
                SET
                    c5052_location_id = v_new_loc_id
                WHERE
                    c5052_location_id = v_old_loc_id
                    AND c205_part_number_id = v_partnum;

                v_out_flag := v_out_flag
                              || 'Y'
                              || ',';
                v_message := v_message
                             || ' Success, ';
                             
            ELSE
                v_message := v_message
                             || v_invalid_ids
                             || v_qty_ids
                             || ',';
                v_out_flag := v_out_flag
                              || 'N'
                              || ',';
            END IF;

        END LOOP;

        p_out := substr(v_message, 1, length(v_message) - 1)
                 || '|'
                 || substr(v_out_flag, 1, length(v_out_flag) - 1);

    END gm_sav_bulk_location;
    

END gm_pkg_op_inv_warehouse_loc_txn;
/