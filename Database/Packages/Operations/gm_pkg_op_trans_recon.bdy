-- @"C:\Database\Packages\Operations\gm_pkg_op_trans_recon.bdy";
CREATE OR REPLACE PACKAGE BODY gm_pkg_op_trans_recon 
IS
/*
 * Description:	This procedure used to save the Missing LN Txn as SOLD item
 * Author:		Rajkumar
 */
PROCEDURE gm_sav_sold_ln(
	p_txnid		IN	T412_INHOUSE_TRANSACTIONS.C412_INHOUSE_TRANS_ID%TYPE,
	p_pnum		IN	T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE,
	p_qty		IN	T413_INHOUSE_TRANS_ITEMS.C413_ITEM_QTY%TYPE,
	p_price		IN	T413_INHOUSE_TRANS_ITEMS.C413_ITEM_PRICE%TYPE,
	p_ordid		IN	T501_ORDER.C501_ORDER_ID%TYPE,
	p_cnum		IN	T413_INHOUSE_TRANS_ITEMS.C413_CONTROL_NUMBER%TYPE,
	p_recpart	IN	T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE,
	p_reason	IN	T901_CODE_LOOKUP.C901_CODE_ID%TYPE,
	p_userid	IN	T413_INHOUSE_TRANS_ITEMS.C413_LAST_UPDATED_BY%TYPE
)
AS
BEGIN
	INSERT
   INTO T413_INHOUSE_TRANS_ITEMS
    (
        C413_TRANS_ID, C412_INHOUSE_TRANS_ID, C205_PART_NUMBER_ID
      , C413_ITEM_QTY, C413_CONTROL_NUMBER, C413_ITEM_PRICE
      , C413_STATUS_FL, C413_REF_ID, C901_REF_TYPE, C205_REC_PART_NUMBER_ID 
      , C901_REC_REASON, C413_LAST_UPDATED_BY,C413_LAST_UPDATED_DATE
    )
    VALUES
    (
        S413_INHOUSE_ITEM.nextval, p_txnid, p_pnum
      , p_qty, p_cnum, p_price
      , 'S',p_ordid , '50180',p_recpart
      , p_reason, p_userid,SYSDATE
    );
END gm_sav_sold_ln;
/*
 * Description:	This procedure used to save the Missing LN Txn as Scanned, here it will create a new record in T412A_INHOUSE_TRANS_ATTRIBUTE with type 50822 and value 'N'
 * Author:		Rajkumar
 */
PROCEDURE gm_sav_scanned_ln(
	p_txnid		IN	T412_INHOUSE_TRANSACTIONS.C412_INHOUSE_TRANS_ID%TYPE,
	p_userid	IN	T412A_INHOUSE_TRANS_ATTRIBUTE.C412A_CREATED_BY%TYPE
)
AS
BEGIN
	INSERT
   INTO T412A_INHOUSE_TRANS_ATTRIBUTE
    (
        C412A_TRANS_ATTRIBUTE_ID, C412_INHOUSE_TRANS_ID, C901_ATTRIBUTE_TYPE
      , C412A_ATTRIBUTE_VALUE, C412A_CREATED_BY, C412A_CREATED_DATE
    )
    VALUES
    (
        S412A_INHOUSE_TRANS.nextval, p_txnid, '50822'
      , 'N', p_userid, SYSDATE
    ) ;
END gm_sav_scanned_ln;
/*
 * Description:	This procedure used to Update the Missing LN Txn as Reconciled in T412A_INHOUSE_TRANS_ATTRIBUTE with type 50822 and value 'Y' 
 * Author:		Rajkumar
 */
PROCEDURE gm_sav_verify_ln(
	p_txnid		IN	T412_INHOUSE_TRANSACTIONS.C412_INHOUSE_TRANS_ID%TYPE,
	p_status	IN	VARCHAR2,
	p_userid	IN	T412A_INHOUSE_TRANS_ATTRIBUTE.C412A_LAST_UPDATED_BY%TYPE
)
AS
BEGIN
	     UPDATE T412A_INHOUSE_TRANS_ATTRIBUTE
        SET C412A_ATTRIBUTE_VALUE     = p_status,
            C412A_LAST_UPDATED_BY = p_userid, 
            C412A_LAST_UPDATED_DATE = SYSDATE
        WHERE C412_INHOUSE_TRANS_ID = p_txnid
        AND C901_ATTRIBUTE_TYPE   = '50822';
        IF (SQL%ROWCOUNT=0)
        THEN
	        INSERT
			   INTO T412A_INHOUSE_TRANS_ATTRIBUTE
			    (
			        C412A_TRANS_ATTRIBUTE_ID, C412_INHOUSE_TRANS_ID, C901_ATTRIBUTE_TYPE
			      , C412A_ATTRIBUTE_VALUE, C412A_CREATED_BY, C412A_CREATED_DATE
			    )
			    VALUES
			    (
			        S412A_INHOUSE_TRANS.nextval, p_txnid, '50822'
			      , p_status, p_userid, SYSDATE
			    ) ;
        END IF;
END gm_sav_verify_ln;
/*
 * Description:	This procedure used to save the Missing LN Txn sa SOLD item
 * Author:		Rajkumar
 */
PROCEDURE gm_sav_scanned_do(
	p_ordid		IN	T501_ORDER.C501_ORDER_ID%TYPE,
	p_pnum		IN	T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE,
	p_userid	IN	T502A_ITEM_ORDER_ATTRIBUTE.C502A_LAST_UPDATED_BY%TYPE
)
AS
BEGIN

	INSERT
   		INTO T502A_ITEM_ORDER_ATTRIBUTE
    	(
        	C502A_ITEM_ORDER_ATTRIBUTE_ID, C501_ORDER_ID, C205_PART_NUMBER_ID, C901_ATTRIBUTE_TYPE
      	, C502A_ATTRIBUTE_VALUE, C502A_LAST_UPDATED_BY, C502A_LAST_UPDATED_DATE
    	)
    	VALUES
    	(
        	S502A_ITEM_ORDER_ATTRIBUTE.nextval, p_ordid, p_pnum, '50822'
      	, 'N', p_userid, SYSDATE
    	);
END gm_sav_scanned_do;
/*
 * Description:	This procedure used to save the Missing LN Txn sa SOLD item
 * Author:		Rajkumar
 */
PROCEDURE gm_sav_reconciled_do(
	p_ordid		IN	T502A_ITEM_ORDER_ATTRIBUTE.C501_ORDER_ID%TYPE,
	p_pnum		IN	T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE,
	p_status	IN	VARCHAR2,
	p_userid	IN	T502A_ITEM_ORDER_ATTRIBUTE.C502A_LAST_UPDATED_BY%TYPE
)
AS
BEGIN
	UPDATE T502A_ITEM_ORDER_ATTRIBUTE
        SET C502A_ATTRIBUTE_VALUE   = p_status, 
        	C502A_LAST_UPDATED_BY = p_userid, 
        	C502A_LAST_UPDATED_DATE = SYSDATE
    WHERE C901_ATTRIBUTE_TYPE = '50822'
    AND C501_ORDER_ID       = p_ordid
    AND C205_PART_NUMBER_ID = p_pnum;
    
    IF (SQL%ROWCOUNT = 0)
    THEN
    	INSERT
   		INTO T502A_ITEM_ORDER_ATTRIBUTE
    	(
        	C502A_ITEM_ORDER_ATTRIBUTE_ID, C501_ORDER_ID, C205_PART_NUMBER_ID, C901_ATTRIBUTE_TYPE
      	, C502A_ATTRIBUTE_VALUE, C502A_LAST_UPDATED_BY, C502A_LAST_UPDATED_DATE
    	)
    	VALUES
    	(
        	S502A_ITEM_ORDER_ATTRIBUTE.nextval, p_ordid, p_pnum, '50822'
      	, p_status, p_userid, SYSDATE
    	);
    END IF;
END gm_sav_reconciled_do;


/*
 * Description: Procedure to Update the Qty for the Sold item
 * Author:		Rajkumar
 */
PROCEDURE gm_update_sold_ln(
	p_transid	IN T413_INHOUSE_TRANS_ITEMS.C413_TRANS_ID%TYPE,	
	p_qty		IN	T413_INHOUSE_TRANS_ITEMS.C413_ITEM_QTY%TYPE,	
	p_userid	IN	T413_INHOUSE_TRANS_ITEMS.C413_LAST_UPDATED_BY%TYPE
)
AS
BEGIN
	UPDATE T413_INHOUSE_TRANS_ITEMS SET 
			C413_ITEM_QTY = p_qty, 
			C413_LAST_UPDATED_BY = p_userid, 
			C413_LAST_UPDATED_DATE = SYSDATE
		WHERE 
			C413_TRANS_ID = p_transid
			AND C413_STATUS_FL = 'S'
			AND C413_VOID_FL IS NULL;
			
	--Whenever the detail is updated, the inhouse transactions table has to be updated ,so ETL can Sync it to GOP.
		UPDATE t412_inhouse_transactions 
		   SET c412_last_updated_by = p_userid
		     , c412_last_updated_date = SYSDATE
		 WHERE c412_inhouse_trans_id = to_char(p_transid);	
END gm_update_sold_ln;
/*
 * Description:	This procedure used to save the Missing LN Txn sa SOLD item
 * Author:		Rajkumar
 */
PROCEDURE gm_sav_ln_status(
	p_txnid		IN	T412_INHOUSE_TRANSACTIONS.C412_INHOUSE_TRANS_ID%TYPE,
	p_status	IN	T412_INHOUSE_TRANSACTIONS.C412_STATUS_FL%TYPE,
	p_userid	IN	T412_INHOUSE_TRANSACTIONS.C412_LAST_UPDATED_BY%TYPE
)
AS
BEGIN
	UPDATE T412_INHOUSE_TRANSACTIONS
	    SET C412_STATUS_FL            = p_status, 
	        C412_LAST_UPDATED_BY = p_userid, 
	        C412_LAST_UPDATED_DATE = SYSDATE
	WHERE C412_INHOUSE_TRANS_ID = to_char(p_txnid);
END gm_sav_ln_status;

/*
 * Description: Procedure to Update Reconcile Part and Reason in the Attribute Table
 * Author:		Rajkumar
 */
PROCEDURE gm_sav_attribute(
	p_txnid		IN	T412_INHOUSE_TRANSACTIONS.C412_INHOUSE_TRANS_ID%TYPE,
	p_pnum		IN	T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE,
	p_value		IN	VARCHAR2,
	p_type		IN	VARCHAR2,
	p_userid	IN	T413_INHOUSE_TRANS_ITEMS.C413_LAST_UPDATED_BY%TYPE
)
AS
v_attrid	T412A_INHOUSE_TRANS_ATTRIBUTE.C412A_TRANS_ATTRIBUTE_ID%TYPE;
BEGIN
	SELECT 
		S412A_INHOUSE_TRANS.nextval INTO v_attrid 
	  FROM DUAL;
	  
	INSERT INTO 
		T412A_INHOUSE_TRANS_ATTRIBUTE
			(C412A_TRANS_ATTRIBUTE_ID,C412_INHOUSE_TRANS_ID,C901_ATTRIBUTE_TYPE,C412A_ATTRIBUTE_VALUE,C205_PART_NUMBER_ID,C412A_CREATED_BY ,C412A_CREATED_DATE) 
		VALUES 
			(v_attrid,p_txnid,p_type,p_value,p_pnum,p_userid,SYSDATE);
END gm_sav_attribute;
/*
 * Description : This Procedure will store the Swapped and Returned Order as Scanned
 * Author	   : Rajkumar
 */
PROCEDURE gm_sav_record_attrib(
	p_ordid		IN	T502A_ITEM_ORDER_ATTRIBUTE.C501_ORDER_ID%TYPE,
	p_pnum		IN	T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE,
	p_qty		IN	T502_ITEM_ORDER.C502_ITEM_QTY%TYPE,
	p_type		IN	T502A_ITEM_ORDER_ATTRIBUTE.C901_ATTRIBUTE_TYPE%TYPE,
	p_status	IN	VARCHAR2,
	p_userid	IN	T502A_ITEM_ORDER_ATTRIBUTE.C502A_LAST_UPDATED_BY%TYPE
)
AS
BEGIN
	UPDATE T502A_ITEM_ORDER_ATTRIBUTE
        SET 
        	C502A_ITEM_QTY = p_qty,
        	C502A_ATTRIBUTE_VALUE   = p_status, 
        	C502A_LAST_UPDATED_BY = p_userid, 
        	C502A_LAST_UPDATED_DATE = SYSDATE
    WHERE C901_ATTRIBUTE_TYPE = p_type
    AND C501_ORDER_ID       = p_ordid
    AND C205_PART_NUMBER_ID = p_pnum;
    
    IF (SQL%ROWCOUNT = 0)
    THEN
    	INSERT
   		INTO T502A_ITEM_ORDER_ATTRIBUTE
    	(
        	C502A_ITEM_ORDER_ATTRIBUTE_ID, C501_ORDER_ID, C205_PART_NUMBER_ID, C901_ATTRIBUTE_TYPE
      	, C502A_ATTRIBUTE_VALUE,C502A_ITEM_QTY, C502A_LAST_UPDATED_BY, C502A_LAST_UPDATED_DATE
    	)
    	VALUES
    	(
        	S502A_ITEM_ORDER_ATTRIBUTE.nextval, p_ordid, p_pnum, p_type
      	, p_status,p_qty, p_userid, SYSDATE
    	);
    END IF;
END gm_sav_record_attrib;
END gm_pkg_op_trans_recon;
/