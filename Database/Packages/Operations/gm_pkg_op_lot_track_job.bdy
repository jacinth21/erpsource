--@"C:\DATABASE\packages\operations\gm_pkg_op_lot_track_job.bdy";
CREATE OR REPLACE PACKAGE BODY gm_pkg_op_lot_track_job
IS
  /**********************************************************************************************
   * Description : Procedure to used to initiate job to store data on t5080 and t5081 table
   * Author      : Agilan Singaravel
  *********************************************************************************************/
	PROCEDURE gm_initiate_job
	AS
	BEGIN
		    
		gm_process_transaction_job;
		
	END gm_initiate_job;
	
  /***************************************************************************************************
   * Description : Procedure to used to fetch job id and to store data on t5080 and t5081 table
   * Author      : Agilan Singaravel
  **************************************************************************************************/	
	PROCEDURE gm_process_transaction_job
	AS		
	v_job_id	t9300_job.c9300_job_id%type;
	BEGIN
		
		BEGIN
			SELECT c9300_job_id
		  	  INTO v_job_id
		  	  FROM t9300_job
		 	 WHERE c9300_job_nm = 'LOTTRACKSTERILE'
		   	   AND c9300_void_fl is null;
		EXCEPTION WHEN OTHERS THEN  
			v_job_id := NULL;			
	    END;

	    IF v_job_id IS NOT NULL THEN
	    	gm_pkg_op_lot_track_data.gm_update_job_process(v_job_id,'START');
	    	
	    	gm_pkg_op_lot_track_data.gm_process_transaction_data();
	    	
	    	gm_pkg_op_lot_track_data.gm_update_job_process(v_job_id,'END');
	    END IF;
	    
	END gm_process_transaction_job;
	/**********************************************************************************************
   * Description : Procedure to used to initiate job to store data on t5080 and t5080_log table
   				   for order usage job
   * Author      : Prabhu Vigneshwaran M D
  *********************************************************************************************/
	
	PROCEDURE gm_order_usage_job AS
    BEGIN
        gm_pkg_op_lot_track_job.gm_process_order_usage_job;
    END gm_order_usage_job;
	
	/***************************************************************************************************
   * Description : Procedure to used to fetch job id and to store data on t5080 and t5080_log table
   				   for Order Usage Job
   * Author      : Prabhu Vigneshwaran M D 
  **************************************************************************************************/
	    PROCEDURE gm_process_order_usage_job AS
        v_job_id     t9300_job.c9300_job_id%TYPE;
    BEGIN
	    
	    ---Get job id and ref type from t9300 table.
        BEGIN
            SELECT
                c9300_job_id
            INTO
                v_job_id
            FROM
                t9300_job
            WHERE
                c9300_job_nm = 'LOTTRACKORDERUSAGE'
                AND c9300_void_fl IS NULL;

        EXCEPTION
            WHEN OTHERS THEN
                v_job_id := NULL;
        END;
        --dbms_output.put_line ('**********************  gm_process_order_usage_job start time >> '|| to_char(sysdate,'MM/dd/yyyy HH:MI:SS AM'));
        --dbms_output.put_line('**********************JOB ID**********************' || v_job_id);
        IF v_job_id IS NOT NULL THEN
            gm_pkg_op_lot_track_data.gm_update_job_process(v_job_id,'START');
            gm_pkg_op_lot_track_data.gm_process_order_usage();
            gm_pkg_op_lot_track_data.gm_update_job_process(v_job_id,'END');
        END IF;
	 --dbms_output.put_line ('**********************  gm_process_order_usage_job end time >> '|| to_char(sysdate,'MM/dd/yyyy HH:MI:SS AM'));
    END gm_process_order_usage_job;

/**********************************************************************************************
   * Description : Procedure to used to initiate job to store data on t5080 and t5080_log table
   * Author      : Bala
  *********************************************************************************************/
	PROCEDURE gm_set_consign_job
	AS
	BEGIN
		gm_process_set_consign_job;
	END gm_set_consign_job;

/***************************************************************************************************
   * Description : Procedure to used to fetch job id and to store data on t5080 and t5080_log table
   * Author      : Bala
  **************************************************************************************************/		
	PROCEDURE gm_process_set_consign_job
	AS		
	v_job_id	t9300_job.c9300_job_id%type;
	BEGIN

		BEGIN
		SELECT c9300_job_id
		  INTO v_job_id
		  FROM t9300_job
		 WHERE c9300_job_nm = 'LOTTRACKSETCONSIGN'
		   AND c9300_void_fl is null;
		EXCEPTION WHEN OTHERS THEN
			v_job_id := NULL;		
	    END;	    
	   
	    IF v_job_id IS NOT NULL THEN
	    	gm_pkg_op_lot_track_data.gm_update_job_process(v_job_id,'START');

	    	gm_pkg_op_lot_track_data.gm_process_ship_trans();

	    	gm_pkg_op_lot_track_data.gm_update_job_process(v_job_id,'END');
	    END IF;
	END gm_process_set_consign_job;	
	
	/*******************************************************************
 	* Description : This procedure is used to execute all lot track job                 
 	********************************************************************/
	PROCEDURE gm_execute_all_procedure 
	AS
	BEGIN
		BEGIN
		
		gm_pkg_cor_client_context.gm_sav_client_context(null,null,'MM/DD/YYYY',null);
		
		gm_pkg_op_lot_track_job.gm_initiate_job;

 		gm_pkg_op_lot_track_job.gm_set_consign_job;

 		gm_pkg_op_lot_track_job.gm_order_usage_job;
 	
 		COMMIT;
 		EXCEPTION
			WHEN OTHERS
			THEN
			DBMS_OUTPUT.put_line ('Error Occurred while executing SP: ' || SQLERRM);
			ROLLBACK;
 		END;
 	END gm_execute_all_procedure;
	
END gm_pkg_op_lot_track_job;

/