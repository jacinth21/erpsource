 --@"C:\DATABASE\packages\operations\gm_pkg_op_inventory_qty.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_inventory_qty
IS
/*
   Procedure Execution Flow:
      (1) GM_UPDATE_REPROCESS_MATERIAL ->(2) gm_sav_inventory_main ->(3) gm_fch_inventory_detail
      ->(4) gm_sav_inventory_update -> (Based On ADD/MINUS Column defined in Rules, gm_sav_bulk_qty procedure will be called).

      To Look at existing Rule Values Created Use:
         400058 : ReturnsToRepack Transaction.
         select * from t906_rules where c906_rule_grp_id in ('400058') or c906_rule_id='400058'
 */

   /*******************************************************
   * Description : This is the
   * Author    : Rajeshwaran
   *******************************************************/
   PROCEDURE gm_sav_inventory_main (
      p_trans_id     IN   VARCHAR2,
      p_trans_type   IN   VARCHAR2,
      p_updated_by   IN   t205_part_number.c205_last_updated_by%TYPE
   )
   AS
      v_party_id    VARCHAR2 (20);
      v_table       t906_rules.c906_rule_value%TYPE;
      v_post_id     t906_rules.c906_rule_value%TYPE;
      v_data        TYPES.cursor_type;
      v_partnum     t205_part_number.c205_part_number_id%TYPE;
      v_qty         NUMBER;
      v_price       NUMBER;
      v_cogs_type   t906_rules.c906_rule_value%TYPE;
      v_to_partnum  t205_part_number.c205_part_number_id%TYPE;
      v_post_cpc_part_id     t906_rules.c906_rule_value%TYPE;
      -- International posting changes.
	  v_costing_id t820_costing.c820_costing_id%TYPE;
	  v_owner_company_id t1900_company.c1900_company_id%TYPE;
	  v_local_cost t820_costing.c820_purchase_amt%TYPE;
	  v_cost t820_costing.c820_purchase_amt%TYPE;
	  v_cost_err_str	VARCHAR2(4000);
	  v_company_id t1900_company.c1900_company_id%TYPE;
	  v_posting_cnt NUMBER;
	  v_set_type t207_set_master.c207_type%TYPE;
	  v_trans_id t412_inhouse_transactions.c412_inhouse_trans_id%TYPE;
	  v_ref_flag t906_rules.c906_rule_value%TYPE;
	  -- Based on Part number - family to avoid the costing validation (PMT-45345)
	  v_skip_posting_fl t906_rules.c906_rule_value%TYPE;
   BEGIN
	   --
	    SELECT get_compid_frm_cntx() 
      	INTO v_company_id 
      	FROM dual;
      	-- based on company to checking the posting
      	SELECT COUNT (1)
		  INTO v_posting_cnt
		   FROM t1900_company
		  WHERE c1900_company_id  = v_company_id
		    AND c901_posting_reqd = 105304;
	  --
	  BEGIN
		    v_trans_id := p_trans_id;
		    --100064 Loaner Reconciliation to Repack has ref id as GM-LN. GM-LN has ref id as GM-CN.
		  	IF get_rule_value ('POSTING_REF_FL', p_trans_type) IS NOT NULL THEN
		  		      SELECT c412_ref_id
		  		        INTO v_trans_id
		  		        FROM t412_inhouse_transactions 
		  		       WHERE c412_inhouse_trans_id = p_trans_id
		  		         AND c412_void_fl IS NULL;
		  	END IF;
		  	  
			SELECT t207.c207_type
			  INTO v_set_type
			  FROM t504_consignment t504, t207_set_master t207, t412_inhouse_transactions t412, t906_rules t906
			 WHERE t412.c412_inhouse_trans_id = v_trans_id
	           AND c504_consignment_id = t412.c412_ref_id
	           AND t504.c207_set_id = t207.c207_set_id
	           AND t504.c504_type = t906.c906_rule_id
	           AND t906.c906_rule_grp_id = 'DEMO_CN_TYPE'
	           AND t906.c906_rule_value ='Y'
	           AND t906.c906_void_fl IS NULL
	           AND c412_void_fl IS NULL;
			  -- AND c504_void_fl IS NULL --When all parts move FG/QN The CN is voided.
			   --AND c207_void_fl IS NULL;
			EXCEPTION WHEN NO_DATA_FOUND THEN
				v_set_type:= NULL;
		END;	    
      -- Get the Table Where the Request information is stored.
      -- This will decide from which table the transaction details to be fetched from.
      SELECT get_rule_value ('TRANS_TABLE', p_trans_type)
        INTO v_table
        FROM DUAL;

      -- Get the Inventory Post ID used for the transaction.
      -- When the Inventory Post ID is NULL, No posting will be done.
      SELECT NVL(get_rule_value (v_set_type, p_trans_type),get_rule_value ('INV_POST_ID', p_trans_type))
        INTO v_post_id
        FROM DUAL;

      -- Get rule value for cogs type from rule table, now purpose only for Inv Adj to Inv TXN.
      SELECT get_rule_value ('FG-COGS-TYPE', p_trans_type)
        INTO v_cogs_type
        FROM DUAL;

      -- Get the Transaction detail for the passed in transaction id AND table.
      -- For eg:  v_table: CONSIGNMENT , p_trans_id : GM-RPN-29033
      gm_fch_inventory_detail (v_table, p_trans_id, v_data);

      SELECT get_acct_party_id (p_trans_id, p_trans_type)
        INTO v_party_id
        FROM DUAL;
        
      -- Posting at the part level, currently applicable for PTRD transactions	
	  SELECT get_rule_value ('INV_POST_CPC_PART_ID', p_trans_type)
        INTO v_post_cpc_part_id
        FROM DUAL;

      -- GET THE INFORMATION FROM THE CURSOR.
      BEGIN
         LOOP
            FETCH v_data
             INTO v_partnum, v_qty, v_price ,v_to_partnum;

            EXIT WHEN v_data%NOTFOUND;
            --DO THE INVENTORY UPDATE FOR THE PASSED IN PARTNUM, QUANTITY, TRANSACTION TYPE.
            gm_sav_inventory_update (p_trans_id,
                                     p_trans_type,
                                     v_partnum,
                                     v_qty,
                                     v_to_partnum,
                                     p_updated_by
                                    );

           -- Based on Part number - family to avoid the costing validation (PMT-45345)
           SELECT NVL (get_rule_value (t205.c205_product_family, 'SKIP_INV_POSTING'), 'N')
	       		INTO v_skip_posting_fl
	       FROM t205_part_number t205
	      WHERE t205.c205_part_number_id = v_partnum;
	      --
            --Only for Inv Adj to Inv transaction(400068), we need get cogs value for v_price
            IF v_cogs_type = 'Y' AND v_posting_cnt = 1 AND v_skip_posting_fl = 'N'
            THEN
               -- to get the costing id (4900 FG)
				v_costing_id := get_ac_costing_id (v_partnum, 4900);
				-- to get the owner information.
				BEGIN
					SELECT t820.c1900_owner_company_id
					  , t820.c820_purchase_amt
					  , t820.c820_local_company_cost
					   INTO v_owner_company_id
					  , v_price
					  , v_local_cost
					   FROM t820_costing t820
					  WHERE t820.c820_costing_id =v_costing_id;
			   EXCEPTION WHEN NO_DATA_FOUND
			   THEN
			   		v_owner_company_id := NULL;
			   		v_price:= NULL;
			   		v_local_cost:= NULL;
			   		v_cost_err_str := v_cost_err_str || v_partnum || ', ';
			   END;
            END IF;
			
           IF v_post_cpc_part_id IS NOT NULL
			THEN
				gm_pkg_part_redesignation.gm_sav_ptrd_posting
									   (v_post_id, 
									    v_post_cpc_part_id, 
										v_partnum,
										v_to_partnum,
										v_party_id,
										p_trans_id,	
										p_trans_type,
										v_qty,
										p_updated_by
										);

            ELSIF v_post_id IS NOT NULL AND v_price IS NOT NULL
            THEN
            	-- Check the Intransit trans and do the local cost posting. (PMT-18235)
            	IF get_rule_value (v_post_id, 'LOCAL_COST_POSTING') = 'Y'
				THEN
	   				v_local_cost := v_price;
	   				BEGIN
		   				SELECT c1900_source_company_id
			  		      INTO v_owner_company_id
			  		      FROM t412_inhouse_transactions t412 , t504e_consignment_in_trans t504e 
			  		     WHERE c412_inhouse_trans_id = p_trans_id
			  		       AND t504e.c504_consignment_id = t412.c412_ref_id
			  		       AND c412_void_fl IS NULL
			  		       AND c504e_void_fl IS NULL;
			  		EXCEPTION WHEN NO_DATA_FOUND
			   		THEN
			   			v_owner_company_id := NULL;			   		
			   		END;       
				END IF;
				
               gm_save_ledger_posting (v_post_id,
                                       CURRENT_DATE,
                                       v_partnum,
                                       v_party_id,
                                       p_trans_id,
                                       v_qty,
                                       v_price,
                                       p_updated_by,
                                       NULL,
                                       v_owner_company_id,
                                       v_local_cost
                                      );
            END IF;
         END LOOP;

         CLOSE v_data;
      EXCEPTION
         WHEN OTHERS
         THEN
            CLOSE v_data;

            RAISE;
      --raise_application_error ('-20999', SQLERRM);
      END;
      -- IF no costing layer then, should not do the inv adj transaction
	  IF v_cost_err_str IS NOT NULL
	  THEN
	  		-- remove the last comma
	  		v_cost_err_str := substr(v_cost_err_str, 0, LENGTH(v_cost_err_str)-2);
	  		--Error message - There is no price for this parts
	  		raise_application_error('-20954', p_trans_id || '#$#' || v_cost_err_str);
	  		--gm_raise_application_error ('-20999','15',v_cost_err_str);
	  END IF;
   END gm_sav_inventory_main;

   /*******************************************************
   * Description : This Procedure will fetch the transaction details for the passed in tranaction ID.
   * Author    : Rajeshwaran
   *******************************************************/
   PROCEDURE gm_fch_inventory_detail (
      p_trans_table   IN       VARCHAR2,
      p_trans_id      IN       VARCHAR2,
      p_data          OUT      TYPES.cursor_type
   )
   AS
   BEGIN
      IF p_trans_table = 'CONSIGNMENT'
      THEN
         OPEN p_data
          FOR
             SELECT   c205_part_number_id ID, SUM (c505_item_qty) qty,
                      c505_item_price price , '' topnum
                 FROM t505_item_consignment
                WHERE c504_consignment_id = p_trans_id
                  AND TRIM (c505_control_number) IS NOT NULL
                  AND c505_void_fl IS NULL
             GROUP BY c205_part_number_id, c505_item_price;
      ELSIF p_trans_table = 'IN_HOUSE'
      THEN
         OPEN p_data
          FOR
             SELECT   c205_part_number_id ID, SUM (c413_item_qty) qty,
                      c413_item_price price, c205_client_part_number_id topnum
                 FROM t413_inhouse_trans_items
                WHERE c412_inhouse_trans_id = p_trans_id
                  AND TRIM (c413_control_number) IS NOT NULL
                  AND c413_void_fl IS NULL
             GROUP BY c205_part_number_id, c413_item_price,c205_client_part_number_id;
      ELSE
         GM_RAISE_APPLICATION_ERROR('-20999','208','');
      END IF;
   END gm_fch_inventory_detail;

   /*******************************************************
   * Description : This Procedure will do the Inventory Update,by adding/negating qunatity from
   * appropriate DB Column (stored in rules table)based on the Transaction type.
   * Author    : Rajeshwaran
   *******************************************************/
   PROCEDURE gm_sav_inventory_update (
      p_trans_id     IN   VARCHAR2,
      p_type         IN   VARCHAR2,
      p_partnum      IN   t205_part_number.c205_part_number_id%TYPE,
      p_qty          IN   NUMBER,
      p_to_partnum   IN	t205_part_number.c205_part_number_id%TYPE,
      p_updated_by   IN   t205_part_number.c205_last_updated_by%TYPE
   )
   IS
      TYPE v_rulevalue_array IS TABLE OF t906_rules.c906_rule_value%TYPE;

      TYPE v_ruleid_array IS TABLE OF t906_rules.c906_rule_id%TYPE;

      rulevalue_array   v_rulevalue_array;
      ruleid_array      v_ruleid_array;
      v_column_nm       t906_rules.c906_rule_value%TYPE;
      v_type            VARCHAR2 (100);
      v_action          VARCHAR2 (100);
      v_qty				NUMBER :=0;
      v_fl              VARCHAR2 (10);
      v_part_number     VARCHAR2 (20);
   BEGIN
      -- Get the Rule Value, Rule ID IN an ARRAY for the passed in Rule Group and Rule ID.
      /*Data Stored in Rules table will be :
       ID         VALUE             GROUP
       -----------------------------------------
      4301  C205_QTY_IN_PACKAGING      400058
      4302  C205_QTY_IN_RETURNS_HOLD   400058
      */
      SELECT c906_rule_value, c906_rule_id
      BULK COLLECT INTO rulevalue_array, ruleid_array
        FROM t906_rules
       WHERE c906_rule_grp_id = p_type
         AND c906_rule_id IN ('PLUS', 'MINUS')                    --4301, 4302
         AND c906_rule_value IS NOT NULL;

      SELECT get_rule_value ('INTP_GRP', p_type)
        INTO v_type
        FROM DUAL;

      IF rulevalue_array.COUNT > 0 AND p_type <> 100406 AND p_type <> 56028  ---For PSFG and PSRW   No need to updated the stock or inv qty.
      THEN
         FOR i IN rulevalue_array.FIRST .. rulevalue_array.LAST
         LOOP
            v_column_nm := rulevalue_array (i);

            SELECT DECODE (ruleid_array (i), 'PLUS', '4301', 'MINUS', '4302')
              INTO v_action
              FROM DUAL;              

            -- Check the Column Name and call the appropriate procedure. The procedure will add/subtract quantity based on the rule id(4301/4302) value.
            IF v_column_nm = 'QTY_IN_BULK'
            THEN
               gm_sav_bulk_qty (v_action,
                                v_type,
                                p_trans_id,
                                p_qty,
                                p_partnum,
                                p_updated_by
                               );
            ELSIF v_column_nm = 'QTY_IN_QUARANTINE'
            THEN
               gm_sav_quarantine_qty (v_action,
                                      v_type,
                                      p_trans_id,
                                      p_qty,
                                      p_partnum,
                                      p_updated_by
                                     );
            ELSIF v_column_nm = 'QTY_IN_PACKAGING'
            THEN
               gm_sav_packaging_qty (v_action,
                                     v_type,
                                     p_trans_id,
                                     p_qty,
                                     p_partnum,
                                     p_updated_by
                                    );
            ELSIF v_column_nm = 'QTY_IN_RETURNS_HOLD'
            THEN
               gm_sav_returns_qty (v_action,
                                   v_type,
                                   p_trans_id,
                                   p_qty,
                                   p_partnum,
                                   p_updated_by
                                  );
            ELSIF v_column_nm = 'QTY_IN_STOCK'
            THEN
               IF(NVL(GET_RULE_VALUE('RE-DESIG PART',p_type),'N') = 'Y') then
                   v_part_number := p_to_partnum;
             ELSE
                   v_part_number := p_partnum;
             END IF;
             
               gm_sav_shelf_qty (v_action,
                                 v_type,
                                 p_trans_id,
                                 p_qty,
                                 v_part_number,
                                 p_updated_by
                                );
            ELSIF v_column_nm = 'QTY_IN_RAWMATERIAL'
            THEN
               SELECT DECODE (v_action,'4302',p_qty * -1,p_qty)
	              INTO v_qty
	              FROM DUAL;	
               gm_cm_sav_partqty (p_partnum,
                               v_qty,
                               p_trans_id,
                               p_updated_by,
                               90802,
                               v_action,
                               4322
                              );                   
             ELSIF v_column_nm = 'QTY_IN_IHINV'
            THEN
	            SELECT DECODE (v_action,'4302',p_qty * -1,p_qty)
	              INTO v_qty
	              FROM DUAL;
               gm_cm_sav_partqty (p_partnum,
                               v_qty,
                               p_trans_id,
                               p_updated_by,
                               90816, -- Inhouse Inventory Qty
                               v_action,
                               4324 -- Inhouse Inventory
                              );                   
            ELSIF v_column_nm = 'QTY_IN_INV'
            THEN
	            SELECT DECODE (v_action,'4302',p_qty * -1,p_qty)
	              INTO v_qty
	              FROM DUAL;
               gm_cm_sav_partqty (p_partnum,
                               v_qty,
                               p_trans_id,
                               p_updated_by,
                               56001, -- Inhouse Inventory Qty
                               v_action,
                               NVL(v_type,56000) -- Inhouse Inventory
                              );                   
            END IF;
         END LOOP;
      END IF;
   END gm_sav_inventory_update;

   /*******************************************************
   * Description : This Procedure will do the quantity update in the c205_qty_in_bulk Column.
   * Author    : Rajeshwaran
   *******************************************************/
   PROCEDURE gm_sav_bulk_qty (
      p_action       IN   VARCHAR2,
      p_type         IN   VARCHAR2,
      p_trans_id     IN   VARCHAR2,
      p_qty          IN   NUMBER,
      p_partnum      IN   t205_part_number.c205_part_number_id%TYPE,
      p_updated_by   IN   t205_part_number.c205_last_updated_by%TYPE
   )
   AS
   BEGIN
      IF p_action = '4301'                                             --PLUS
      THEN
         gm_cm_sav_partqty (p_partnum,
                               p_qty,
                               p_trans_id,
                               p_updated_by,
                               90814, -- BULK  Qty
                               p_action,
                               p_type 
                               );
         gm_pkg_op_lot_track.gm_lot_track_main(p_partnum,
                               p_qty,
                               p_trans_id,
                               p_updated_by,
                               90814, -- BULK  Qty
                               p_action,
                               p_type   
                               );
      ELSIF p_action = '4302'                                          --MINUS
      THEN
         gm_cm_sav_partqty (p_partnum,
                               (p_qty * -1),
                               p_trans_id,
                               p_updated_by,
                               90814, -- BULK  Qty
                               p_action,
                               p_type 
                               );
         gm_pkg_op_lot_track.gm_lot_track_main(p_partnum,
                               (p_qty * -1),
                               p_trans_id,
                               p_updated_by,
                               90814, -- BULK  Qty
                               p_action,
                               p_type   
                               );
      END IF;
   END gm_sav_bulk_qty;

   /*******************************************************
   * Description : This Procedure will do the quantity update in the c205_qty_in_quarantine Column.
   * Author    : Rajeshwaran
   *******************************************************/
   PROCEDURE gm_sav_quarantine_qty (
      p_action       IN   VARCHAR2,
      p_type         IN   VARCHAR2,
      p_trans_id     IN   VARCHAR2,
      p_qty          IN   NUMBER,
      p_partnum      IN   t205_part_number.c205_part_number_id%TYPE,
      p_updated_by   IN   t205_part_number.c205_last_updated_by%TYPE
   )
   AS
   BEGIN
      IF p_action = '4301'                                             --PLUS
      THEN
         gm_cm_sav_partqty (p_partnum,
                               p_qty,
                               p_trans_id,
                               p_updated_by,
                               90813, -- quarantine  Qty
                               p_action,
                               p_type 
                               );
         gm_pkg_op_lot_track.gm_lot_track_main(p_partnum,
                               p_qty,
                               p_trans_id,
                               p_updated_by,
                               90813, -- quarantine  Qty
                               p_action,
                               p_type   
                               );
      ELSIF p_action = '4302'                                          --MINUS
      THEN
         gm_cm_sav_partqty (p_partnum,
                               (p_qty * -1),
                               p_trans_id,
                               p_updated_by,
                               90813, -- quarantine  Qty
                               p_action,
                               p_type 
                               );
         gm_pkg_op_lot_track.gm_lot_track_main(p_partnum,
                               (p_qty * -1),
                               p_trans_id,
                               p_updated_by,
                               90813, -- quarantine  Qty
                               p_action,
                               p_type   
                               );
      END IF;
   END gm_sav_quarantine_qty;

   /*******************************************************
   * Description : This Procedure will do the quantity update in the c205_qty_in_packaging Column.
   * Author    : Rajeshwaran
   *******************************************************/
   PROCEDURE gm_sav_packaging_qty (
      p_action       IN   VARCHAR2,
      p_type         IN   VARCHAR2,
      p_trans_id     IN   VARCHAR2,
      p_qty          IN   NUMBER,
      p_partnum      IN   t205_part_number.c205_part_number_id%TYPE,
      p_updated_by   IN   t205_part_number.c205_last_updated_by%TYPE
   )
   AS
   BEGIN
      IF p_action = '4301'                                             --PLUS
      THEN
         gm_cm_sav_partqty (p_partnum,
                               p_qty,
                               p_trans_id,
                               p_updated_by,
                               90815, -- packaging  Qty
                               p_action,
                               p_type 
                               );
         gm_pkg_op_lot_track.gm_lot_track_main(p_partnum,
                               p_qty,
                               p_trans_id,
                               p_updated_by,
                               90815, -- packaging  Qty
                               p_action,
                               p_type   
                               );
      ELSIF p_action = '4302'                                          --MINUS
      THEN
         gm_cm_sav_partqty (p_partnum,
                               (p_qty * -1),
                               p_trans_id,
                               p_updated_by,
                               90815, -- packaging  Qty
                               p_action,
                               p_type 
                               );
         gm_pkg_op_lot_track.gm_lot_track_main(p_partnum,
                               (p_qty * -1),
                               p_trans_id,
                               p_updated_by,
                               90815, -- packaging  Qty
                               p_action,
                               p_type   
                               );
      END IF;
   END gm_sav_packaging_qty;

   /*******************************************************
   * Description : This Procedure will do the quantity update in the c205_qty_in_returns_hold Column.
   * Author    : Rajeshwaran
   *******************************************************/
   PROCEDURE gm_sav_returns_qty (
      p_action       IN   VARCHAR2,
      p_type         IN   VARCHAR2,
      p_trans_id     IN   VARCHAR2,
      p_qty          IN   NUMBER,
      p_partnum      IN   t205_part_number.c205_part_number_id%TYPE,
      p_updated_by   IN   t205_part_number.c205_last_updated_by%TYPE
   )
   AS
   BEGIN
      IF p_action = '4301'                                             --PLUS
      THEN
         gm_cm_sav_partqty (p_partnum,
                               p_qty,
                               p_trans_id,
                               p_updated_by,
                               90812, -- returns  Qty
                               p_action,
                               p_type 
                               );
      ELSIF p_action = '4302'                                          --MINUS
      THEN
         gm_cm_sav_partqty (p_partnum,
                               (p_qty * -1),
                               p_trans_id,
                               p_updated_by,
                               90812, -- returns  Qty
                               p_action,
                               p_type 
                               );
         gm_pkg_op_lot_track.gm_lot_track_main(p_partnum,
                               (p_qty * -1),
                               p_trans_id,
                               p_updated_by,
                               90812, -- returns  Qty
                               p_action,
                               p_type   
                               );
      END IF;
   END gm_sav_returns_qty;

   /*******************************************************
   * Description : This Procedure will do the quantity update in the C205_QTY_IN_STOCK Column.
   * Author    : Rajeshwaran
   *******************************************************/
   PROCEDURE gm_sav_shelf_qty (
      p_action       IN   VARCHAR2,
      p_type         IN   VARCHAR2,
      p_trans_id     IN   VARCHAR2,
      p_qty          IN   NUMBER,
      p_partnum      IN   t205_part_number.c205_part_number_id%TYPE,
      p_updated_by   IN   t205_part_number.c205_last_updated_by%TYPE
   )
   AS
   BEGIN
      IF p_action = '4301'                                             --PLUS
      THEN
         gm_cm_sav_partqty (p_partnum,
                               p_qty,
                               p_trans_id,
                               p_updated_by,
                               90800, -- FG  Qty
                               p_action,
                               p_type 
                               );
         gm_pkg_op_lot_track.gm_lot_track_main(p_partnum,
                               p_qty,
                               p_trans_id,
                               p_updated_by,
                               90800, -- FG  Qty
                               p_action,
                               p_type   
                               );
      ELSIF p_action = '4302'                                          --MINUS
      THEN
         gm_cm_sav_partqty (p_partnum,
                               (p_qty * -1),
                               p_trans_id,
                               p_updated_by,
                               90800, -- FG  Qty
                               p_action,
                               p_type 
                               );
         gm_pkg_op_lot_track.gm_lot_track_main(p_partnum,
                               (p_qty * -1),
                               p_trans_id,
                               p_updated_by,
                               90800, -- FG  Qty
                               p_action,
                               p_type   
                               );
      END IF;
   END gm_sav_shelf_qty;
END gm_pkg_op_inventory_qty;
/
