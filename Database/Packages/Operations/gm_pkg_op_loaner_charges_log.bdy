
--@"C:\pmt\bit\SpineIT-ERP\Database\Packages\Operations\gm_pkg_op_loaner_charges_log.bdy";
CREATE OR REPLACE PACKAGE BODY gm_pkg_op_loaner_charges_log
IS
/*********************************************************
* Description : Procedure used to fetch the rebate audit trail details 
* 
*Author : Angeline
*********************************************************/
PROCEDURE gm_fch_charge_history_log (
    p_charge_dtl_id IN t9204_charge_history_log.C9201_CHARGE_DETAILS_ID%TYPE,
    p_type      IN t9204_charge_history_log.C901_TYPE%TYPE,
    p_date_fmt  IN VARCHAR2,
    p_out_log_cur OUT TYPES.cursor_type)
        
AS
    v_comp_dt_fmt VARCHAR2 (20);
    
BEGIN
	
    OPEN p_out_log_cur FOR
    
    SELECT
		    t9204.c9201_charge_details_id   reqid,
		    t9204.c9204_value               rvalue,
		    get_user_name(t9204.c9204_updated_by) updatedby,
		    to_char(t9204.c9204_updated_dt, p_date_fmt || ' HH24:MI:SS AM') update_date
	FROM
	    t9204_charge_history_log t9204
	WHERE
	    t9204.c9201_charge_details_id = p_charge_dtl_id
	    AND t9204.c901_type = p_type
	    AND t9204.c9204_void_fl IS NULL
	ORDER BY
	    t9204.c9204_updated_dt DESC;
    
END gm_fch_charge_history_log;
	
	
/*********************************************************
* Description : Procedure used to fetch the entire charge details
*
*Author : Angeline
*********************************************************/
PROCEDURE gm_fch_charges_log (
 p_charge_detail_id   IN t9203_charge_details_log.c9201_charge_details_id%TYPE,
 p_out_log_cur        OUT TYPES.cursor_type)
AS
BEGIN
OPEN p_out_log_cur FOR

		SELECT 
				C9201_CHARGE_DETAILS_ID, 
				C9200_INCIDENT_ID, 
				C9203_TXN_AMOUNT ,
				C9203_OPEN_AMOUNT, 
				C9203_CLOSED_AMOUNT,
				C9203_UPDATED_BY,
				C9203_CHARGED_BY, 
				C9203_CHARGED_DT, 
				C9203_UPDATED_DT
		FROM  T9203_charge_details_Log 
		WHERE C9201_CHARGE_DETAILS_ID = p_charge_detail_id
		AND c9203_void_fl is NULL 
		order by c9203_updated_dt desc;

END gm_fch_charges_log;
	
/********************************************************************************************
* Description :This function returns count of charge history.
*********************************************************************************************/
FUNCTION get_charge_history_count (
p_charge_dtl_id IN t9204_charge_history_log.c9201_charge_details_id%TYPE
) 
RETURN NUMBER
	 IS
	    v_count NUMBER;
	BEGIN
	  
	SELECT
	    COUNT(1)
	INTO v_count
	FROM
	    t9204_charge_history_log
	WHERE
	    c9201_charge_details_id = p_charge_dtl_id
	    AND c9204_void_fl IS NULL;
	    
	RETURN
	v_count;
	
	EXCEPTION
	WHEN NO_DATA_FOUND THEN
    RETURN 0;
    
END get_charge_history_count;
			
/*************************************************************************
*Description: This procedure is to save the comments history(notes) 
**********************************************************************************/

PROCEDURE gm_sav_charge_history_log(
	p_charge_detail_id   IN       t9204_charge_history_log.c9201_charge_details_id%TYPE,
	p_type               IN       t9204_charge_history_log.c901_type%TYPE,
	p_notes              IN       t9204_charge_history_log.c9204_value%TYPE,
	p_user_id            IN       t9204_charge_history_log.c9204_updated_by%TYPE,
	p_updated_dt         IN       t9204_charge_history_log.c9204_updated_dt%TYPE
)
IS
BEGIN

INSERT INTO t9204_charge_history_log (
    c9204_charge_history_log_id,
    c9201_charge_details_id,
    c901_type,
    c9204_value,
    c9204_updated_by,
    c9204_updated_dt
) VALUES (
    s9204_charge_history_log.NEXTVAL,
    p_charge_detail_id,
    p_type,
    p_notes,
    p_user_id,
    p_updated_dt
);			
	
END gm_sav_charge_history_log;	

/********************************************************************************
*DESCRIPTION : THIS PROCEDURE IS CALLED WHEN ANY UPDATE IS DONE 
*              IN t9203_charge_details_log TO SAVE THE AMOUNT
*
*********************************************************************************/

PROCEDURE gm_sav_charges_log(
	p_charge_details_id    IN     t9203_charge_details_log.C9201_CHARGE_DETAILS_ID%TYPE,
	p_incident_id          IN     t9203_charge_details_log.C9200_INCIDENT_ID %TYPE,
	p_txn_amount           IN     t9203_charge_details_log.C9203_TXN_AMOUNT %TYPE,
	p_open_amount          IN     t9203_charge_details_log.C9203_OPEN_AMOUNT %TYPE,
	p_closed_amount        IN     t9203_charge_details_log.C9203_CLOSED_AMOUNT%TYPE,
	p_user_id              IN     t9203_charge_details_log.C9203_UPDATED_BY %TYPE,
	p_charged_by           IN     t9203_charge_details_log.C9203_CHARGED_BY %TYPE,
	p_charged_dt           IN     t9203_charge_details_log.C9203_CHARGED_DT %TYPE,
	p_updated_dt           IN     t9203_charge_details_log.C9203_UPDATED_DT%TYPE
)
IS
BEGIN

	INSERT INTO t9203_charge_details_log(
		C9203_CHARGE_DETAILS_LOG_ID,
		C9201_CHARGE_DETAILS_ID,    
		C9200_INCIDENT_ID,    
		C9203_TXN_AMOUNT,
		C9203_OPEN_AMOUNT,    
		C9203_CLOSED_AMOUNT,    
		C9203_UPDATED_BY,    
		C9203_CHARGED_BY,    
		C9203_CHARGED_DT,
		C9203_UPDATED_DT 
	)
	VALUES(
		s9203_charge_details_log. NEXTVAL,
		p_charge_details_id,
		p_incident_id,
		p_txn_amount,
		p_open_amount,
		p_closed_amount,
		p_user_id,
		p_charged_by,
		p_charged_dt,
		p_updated_dt 
		);   

END gm_sav_charges_log;	

/********************************************************************************
*DESCRIPTION : THIS PROCEDURE IS CALLED WHEN ANY UPDATE IS DONE 
*              IN t9203_charge_details_log TO SAVE THE AMOUNT
*
*********************************************************************************/

PROCEDURE gm_fch_charge_amount_history_log(
	p_charge_dtl_id IN T9203_charge_details_Log.C9201_CHARGE_DETAILS_ID%TYPE,
	p_type          IN T901_code_lookup.C901_CODE_ID%TYPE,
	p_date_fmt      IN VARCHAR2,
	p_out_log_cur   OUT TYPES.cursor_type
)
AS
    
BEGIN
	OPEN p_out_log_cur FOR

	SELECT 
		t9203.C9201_CHARGE_DETAILS_ID reqid ,
		t9203.c9203_closed_amount rvalue,
		get_user_name (t9203.C9203_CHARGED_BY) updatedby,
		to_char(t9203.C9203_CHARGED_DT, p_date_fmt || ' HH24:MI:SS AM') update_date
	FROM T9203_charge_details_Log t9203
	WHERE t9203.C9201_CHARGE_DETAILS_ID = p_charge_dtl_id
	AND t9203.C9203_VOID_FL IS NULL
	ORDER BY t9203.c9203_charge_details_log_id DESC;
	
	
END gm_fch_charge_amount_history_log;

/********************************************************************************************
* Description :This function returns count of charge history.
*********************************************************************************************/
FUNCTION get_charge_amount_count (
p_charge_dtl_id IN t9204_charge_history_log.c9201_charge_details_id%TYPE
) 
RETURN NUMBER
	 IS
	    v_count NUMBER;
	BEGIN
	  
	SELECT
	    COUNT(1)
	INTO v_count
	FROM
	    T9203_charge_details_Log
	WHERE
	    c9201_charge_details_id = p_charge_dtl_id
	    AND C9203_VOID_FL IS NULL;
	    
	RETURN
	v_count;
	
	EXCEPTION
	WHEN NO_DATA_FOUND THEN
    RETURN 0;
    
END get_charge_amount_count;
	
END gm_pkg_op_loaner_charges_log;
/