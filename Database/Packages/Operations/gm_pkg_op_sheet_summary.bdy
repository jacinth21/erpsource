/* Formatted on 2009/12/15 10:21 (Formatter Plus v4.8.0) */
-- @"c:\database\packages\operations\gm_pkg_op_sheet_summary.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_sheet_summary
IS
--
   /*******************************************************
   * Description : Procedure to fetch demand sheet details
   *			 for selected month
   * Author  : Joe P Kumar
   *******************************************************/
	PROCEDURE gm_fc_fch_dsmonthdetails (
		p_demandsheetid   IN	   t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_monyear		  IN	   VARCHAR2
	  , p_output		  OUT	   TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_output
		 FOR
			 SELECT t4020.c4020_demand_master_id demandsheetid, TO_CHAR (c4040_load_dt, 'MM/DD/YYYY') loaddate
				  , c4040_demand_sheet_id demandsheetmonthid, c4040_demand_period demandperiod
				  , c4040_forecast_period forecastperiod, get_code_name (c901_status) status, c901_status statusid
				  , get_code_name (t4020.c901_demand_type) demandtypenm, t4020.c901_demand_type demandtypeid
				  , c4020_request_period requestperiod
			   FROM t4040_demand_sheet t4040, t4020_demand_master t4020
			  WHERE c4040_void_fl IS NULL
				AND t4020.c4020_demand_master_id = p_demandsheetid
				AND TO_CHAR (c4040_load_dt, 'MM/YYYY') = p_monyear
				AND t4040.c4020_demand_master_id = t4020.c4020_demand_master_id;
	END gm_fc_fch_dsmonthdetails;

/*******************************************************
   * Description : Procedure to fetch the details for demand sheet summary crosstab
   * Author 	: Joe P Kumar
 *******************************************************/
	PROCEDURE gm_fc_fch_dsdetails (
		p_demandsheetmonthid   IN		t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	  , p_groupids			   IN		VARCHAR2
	  , p_partnums			   IN		VARCHAR2
	  , p_overrideflag		   IN		VARCHAR2
	  , p_forecast_months	   IN		NUMBER
	  , p_unit_type 		   IN		VARCHAR2
	  , p_header			   OUT		TYPES.cursor_type
	  , p_detail			   OUT		TYPES.cursor_type
	  , p_forecast			   OUT		TYPES.cursor_type
	  , p_demand_last		   OUT		VARCHAR2
	  , p_sheet_status		   OUT		VARCHAR2
	)
	AS
		--
		v_group_fl	   CHAR (1);
		v_demand_start_dt DATE;
		v_type		   t4020_demand_master.c901_demand_type%TYPE;
	BEGIN
		-- If no group value then fetch all values
		SELECT DECODE (p_groupids, '', 'N', 'Y')
		  INTO v_group_fl
		  FROM DUAL;

		SELECT t4040.c901_demand_type
		  INTO v_type
		  FROM t4040_demand_sheet t4040
		 WHERE t4040.c4040_demand_sheet_id = p_demandsheetmonthid;

		-- Demand final value used for screen alignment
		BEGIN
			IF v_type = 40023
			THEN
				SELECT	 TO_CHAR (MIN (ADD_MONTHS (t4042.c4042_period, -1)), 'Mon YY') period, MIN (t4042.c4042_period)
					   , t4040.c901_status
					INTO p_demand_last, v_demand_start_dt
					   , p_sheet_status
					FROM t4042_demand_sheet_detail t4042, t4040_demand_sheet t4040
				   WHERE t4040.c4040_demand_sheet_id = p_demandsheetmonthid
					 AND t4040.c4040_demand_sheet_id = t4042.c4040_demand_sheet_id
					 AND t4042.c901_type = 50563
				GROUP BY t4040.c4020_demand_master_id, t4040.c901_status;
			ELSE
				SELECT	 TO_CHAR (MAX (t4042.c4042_period), 'Mon YY') period, MIN (t4042.c4042_period)
					   , t4040.c901_status
					INTO p_demand_last, v_demand_start_dt
					   , p_sheet_status
					FROM t4042_demand_sheet_detail t4042, t4040_demand_sheet t4040
				   WHERE t4040.c4040_demand_sheet_id = p_demandsheetmonthid
					 AND t4040.c4040_demand_sheet_id = t4042.c4040_demand_sheet_id
					 AND t4042.c901_type = 50560
				GROUP BY t4040.c4020_demand_master_id, t4040.c901_status;
			END IF;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				NULL;
		END;

		-- The input String in the form of 101.210,101.211 will be set into this temp view.
		my_context.set_my_inlist_ctx (p_groupids);

		-- Header information
		OPEN p_header
		 FOR
			 SELECT DISTINCT DECODE (t4042.c901_type
								   , 50560, TO_CHAR (t4042.c4042_period, 'Mon YY')
								   , 50563, TO_CHAR (t4042.c4042_period, 'Mon YY')
								   , get_code_name (t4042.c901_type)
									) period
						   , t4042.c4042_period, t4042.c901_type, t901.c901_code_seq_no seq_no
						FROM t4042_demand_sheet_detail t4042, t4040_demand_sheet t4040, t901_code_lookup t901
					   WHERE t4040.c4040_demand_sheet_id = p_demandsheetmonthid
						 AND t4040.c4040_demand_sheet_id = t4042.c4040_demand_sheet_id
						 AND t4042.c901_type = t901.c901_code_id
						 AND t4042.c4042_period <= ADD_MONTHS (TO_DATE (p_demand_last, 'Mon YY'), p_forecast_months)
					ORDER BY seq_no, t4042.c4042_period;

		OPEN p_detail
		 FOR
			 SELECT   DECODE (t205.c205_part_number_id
							, NULL, DECODE (t4042.c4042_ref_id
										  , NULL, 'Total'
										  , -9999, get_group_name (t4042.c4042_ref_id)
										  , -99991, get_group_name (t4042.c4042_ref_id)
										  , 9999999999, get_group_name (t4042.c4042_ref_id)
										  , '' || '^' || t4042.c4042_ref_id
										   )
							, t205.c205_part_number_id || '^' || t4042.c4042_ref_id
							 ) num	 --T4042.C205_PART_NUMBER_ID
					, DECODE (t205.c205_part_number_id
							, NULL, DECODE (t4042.c4042_ref_id
										  , NULL, 'Total'
										  , -9999, get_group_name (t4042.c4042_ref_id)
										  ,-99991, get_group_name (t4042.c4042_ref_id)
										  , 9999999999, get_group_name (t4042.c4042_ref_id)
										  , DECODE (v_type
												  , 40020, get_group_name (t4042.c4042_ref_id)
												  , get_set_name (t4042.c4042_ref_id)
												   )
										   )
							, t205.c205_part_number_id
							 ) des
					, DECODE (t4042.c901_type
							, 50560, TO_CHAR (t4042.c4042_period, 'Mon YY')
							, 50563, TO_CHAR (t4042.c4042_period, 'Mon YY')
							, t901.c901_code_nm
							 ) period
					, NVL (DECODE (p_unit_type
								 , 'Unit', SUM (DECODE (t4042.c4042_parent_ref_id, NULL, t4042.c4042_qty, 0))
								 , SUM (DECODE (t4042.c4042_parent_ref_id, NULL, t4042.c4042_qty, 0)
										* ut_price.unitprice)
								  )
						 , 0
						  ) sumqty
					, GROUPING_ID (t4042.c4042_ref_id, t205.c205_part_number_id) grpid
					, DECODE (GROUPING_ID (t4042.c4042_ref_id, t205.c205_part_number_id)
							, 0, MIN (NVL (t4042.c4042_history_fl, 'N'))
							, 'N'
							 ) hfl
					, t4042.c4042_ref_id grpcs
					, DECODE (t4042.c4042_period
							, v_demand_start_dt, get_log_flag (t4040.c4020_demand_master_id || t205.c205_part_number_id
															 -- 	|| '-'
															 -- 	|| t4042.c4042_ref_id
							  , 							   1228
															  )
							, 'N'
							 ) partflag   -- part level comments
					, NVL (c205_crossover_fl, 'N') crossoverf
					, (gm_pkg_op_sheet_summary.get_parent_sub_flag (t205.c205_part_number_id)) ps_flag
				 FROM t4042_demand_sheet_detail t4042
					, t4040_demand_sheet t4040
					--, t4041_demand_sheet_mapping t4041
					--, t4010_group t4010
			 ,		  t205_part_number t205
					, t901_code_lookup t901
					, (SELECT	t405.c205_part_number_id
							  , MAX (NVL (t405.c405_cost_price, 0) / NVL (c405_uom_qty, 1)) unitprice
						   FROM t405_vendor_pricing_details t405
						  WHERE t405.c405_active_fl(+) = 'Y'
						  AND t405.c405_void_fl IS NULL
					   GROUP BY t405.c205_part_number_id) ut_price
				WHERE t4040.c4040_demand_sheet_id = p_demandsheetmonthid
				  AND t4040.c4040_demand_sheet_id = t4042.c4040_demand_sheet_id
				  AND t4042.c205_part_number_id = t205.c205_part_number_id
				  /* AND t4042.c205_part_number_id IN (
							 SELECT c205_part_number_id
							 FROM t4042_demand_sheet_detail
							WHERE NVL (c4042_history_fl, 'N') =
															DECODE (p_overrideflag
																  , 'Y', 'Y'
																  , NVL (c4042_history_fl, 'N')
																   )
							  AND c4040_demand_sheet_id = p_demandsheetmonthid)*/--AND t4040.c4040_demand_sheet_id = t4041.c4040_demand_sheet_id
				   --AND (t4042.c4042_ref_id = t4041.c4041_ref_id OR t4042.c4042_ref_id = -9999) -- 9999 maps to item consignment
				  AND t4042.c901_type = t901.c901_code_id
				  AND t205.c205_part_number_id = ut_price.c205_part_number_id(+)
				  AND (   t4042.c4042_ref_id IN (SELECT *
												   FROM v_in_list)
					   OR t4042.c4042_ref_id = DECODE (v_group_fl, 'N', t4042.c4042_ref_id, -999)
					  )
				 AND regexp_like (t4042.c205_part_number_id, NVL (REGEXP_REPLACE(p_partnums,'[+]','\+'), REGEXP_REPLACE(t4042.c205_part_number_id,'[+]','\+')))
				  AND t4042.c4042_period <= ADD_MONTHS (TO_DATE (p_demand_last, 'Mon YY'), p_forecast_months)
			 --AND t4010.c4010_group_id = t4041.c4041_ref_id
			 GROUP BY t4042.c901_type
					, t4042.c4042_period
					, t901.c901_code_nm
					, ROLLUP (t4042.c4042_ref_id
							, (t205.c205_part_num_desc, t205.c205_part_number_id, c205_crossover_fl))
					, t4040.c4020_demand_master_id
			   HAVING (GROUPING_ID (t4042.c4042_ref_id, t205.c205_part_number_id) <> 1 OR t4042.c4042_ref_id IS NOT NULL
					  )
			 ORDER BY t4042.c4042_ref_id, grpid, num, t4042.c4042_period;

		-- sql used for screen alignment
		OPEN p_forecast
		 FOR
			 SELECT DISTINCT TO_CHAR (t4042.c4042_period, 'Mon YY') period, t4042.c4042_period
						FROM t4042_demand_sheet_detail t4042, t4040_demand_sheet t4040
					   WHERE t4040.c4040_demand_sheet_id = p_demandsheetmonthid
						 AND t4040.c4040_demand_sheet_id = t4042.c4040_demand_sheet_id
						 AND t4042.c901_type = 50563
					ORDER BY t4042.c4042_period;
	END gm_fc_fch_dsdetails;

	/*******************************************************
	 * Description : Procedure to fetch the Groups associated with a DS
	 * Author		: Joe P Kumar
	 *******************************************************/
	PROCEDURE gm_fc_fch_grpfords (
		p_demandsheetmonthid   IN		t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	  , p_demandsheetid 	   IN		t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_output			   OUT		TYPES.cursor_type
	)
	AS
		v_type		   t4020_demand_master.c901_demand_type%TYPE;
	BEGIN
		SELECT gm_pkg_op_sheet.get_demand_type (p_demandsheetid)
		  INTO v_type
		  FROM DUAL;

		OPEN p_output
		 FOR
			 SELECT   t4041.c4041_ref_id ID
					, DECODE (v_type
							, 40020, get_group_name (t4041.c4041_ref_id)
							, get_set_name (t4041.c4041_ref_id)
							 ) NAME   -- if sales then group else set information
					, 1 seq
				 FROM t4041_demand_sheet_mapping t4041
				WHERE t4041.c4040_demand_sheet_id = p_demandsheetmonthid AND t4041.c901_ref_type IN (40031, 40030)
			 UNION
			 SELECT   '-9999' ID, 'Item Consignment' NAME, 2 seq
				 FROM DUAL
				WHERE 'X' = DECODE (v_type, 40020, 'Y', 'X')
			 ORDER BY seq, NAME;
	END gm_fc_fch_grpfords;

	/**************************************************************************
	 * Description : Procedure to fetch Growth information for selected parts
	 *************************************************************************/
	PROCEDURE gm_fc_fch_dsgrowthdetail (
		p_demandsheetid   IN	   t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_header		  OUT	   TYPES.cursor_type
	  , p_detail		  OUT	   TYPES.cursor_type
	  , p_status		  OUT	   VARCHAR
	)
	AS
		v_type		   t4020_demand_master.c901_demand_type%TYPE;
	BEGIN
		SELECT c901_status
		  INTO p_status
		  FROM t4040_demand_sheet
		 WHERE c4040_demand_sheet_id = p_demandsheetid;

		SELECT gm_pkg_op_sheet.get_demand_type (t4040.c4020_demand_master_id)
		  INTO v_type
		  FROM t4040_demand_sheet t4040
		 WHERE t4040.c4040_demand_sheet_id = p_demandsheetid;

		-- Header information
		OPEN p_header
		 FOR
			 SELECT DISTINCT TO_CHAR (t4043.c4043_start_date, 'Mon YY') period, t4043.c4043_start_date
						FROM t4043_demand_sheet_growth t4043
					   WHERE t4043.c4040_demand_sheet_id = p_demandsheetid
					ORDER BY t4043.c4043_start_date;

		OPEN p_detail
		 FOR
			 SELECT   t4030.c4030_ref_id ID
					, DECODE (v_type
							, 40020, get_group_name (t4030.c4030_ref_id)
							,	 t4030.c4030_ref_id
							  || ' - '
							  || get_set_name (t4030.c4030_ref_id)
							  || DECODE (t4041.c901_action_type
									   , NULL, ''
									   , ' ( ' || get_code_name_alt (t4041.c901_action_type) || ' )'
										)
							 ) NAME
					, TO_CHAR (t4043.c4043_start_date, 'Mon YY') mon
					, t4043.c4043_value || get_code_name (t4043.c901_ref_type) VALUE, 1 seq
					, DECODE (v_type, 40020, 'G', 'S') gtype
				 FROM t4043_demand_sheet_growth t4043
					, t4030_demand_growth_mapping t4030
					, t4041_demand_sheet_mapping t4041
				WHERE t4043.c4040_demand_sheet_id = p_demandsheetid
				  AND t4043.c4030_demand_growth_id = t4030.c4030_demand_growth_id
				  AND t4030.c901_ref_type IN (20296, 20297)
				  AND t4043.c4043_value IS NOT NULL
				  AND t4041.c4041_ref_id = t4030.c4030_ref_id
				  AND t4041.c4040_demand_sheet_id = t4043.c4040_demand_sheet_id
			 UNION
			 SELECT   t4030.c4030_ref_id ID, t4030.c4030_ref_id || ' - ' || get_partnum_desc (t4030.c4030_ref_id) NAME
					, TO_CHAR (t4043.c4043_start_date, 'Mon YY') mon
					, t4043.c4043_value || get_code_name (t4043.c901_ref_type) VALUE, 2 seq, 'P' gtype
				 FROM t4030_demand_growth_mapping t4030, t4043_demand_sheet_growth t4043
				WHERE t4043.c4040_demand_sheet_id = p_demandsheetid
				  --AND t4040.c4020_demand_master_id = t4030.c4020_demand_master_id
				  AND t4030.c4030_demand_growth_id = t4043.c4030_demand_growth_id
				  AND t4030.c901_ref_type IN (20295, 20298)
				  AND t4030.c4030_void_fl IS NULL
				  AND t4043.c4043_value IS NOT NULL
			 ORDER BY seq, NAME;
	END gm_fc_fch_dsgrowthdetail;

	/**************************************************************************
		* Description : Procedure to fetch part changed details
		*************************************************************************/
	PROCEDURE gm_fc_fch_dsparthistory (
		p_demandsheetid   IN	   t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_detail		  OUT	   TYPES.cursor_type
	)
	AS
	BEGIN
		-- Change details
		OPEN p_detail
		 FOR
			 SELECT   t4042.c205_part_number_id pid, get_partnum_desc (t4042.c205_part_number_id) pname
					, TO_CHAR (t4042.c4042_period, 'Mon YY') period, t941.c941_value qvalue
					, get_user_name (t941.c941_updated_by) uby
					, TO_CHAR (t941.c941_updated_date, 'mm/dd/yyyy hh24:mi:ss') udate
				 FROM t4042_demand_sheet_detail t4042, t941_audit_trail_log t941
				WHERE t4042.c4042_history_fl IS NOT NULL
				  AND t4042.c4040_demand_sheet_id = p_demandsheetid
				  AND t4042.c4042_demand_sheet_detail_id = t941.c941_ref_id
				  AND t941.c940_audit_trail_id = 1001
			 ORDER BY pid, t4042.c4042_period, t941.c941_audit_trail_log_id DESC;
	END gm_fc_fch_dsparthistory;

	/**************************************************************************
	 * Description : Procedure to fetch desc approval details
	 *************************************************************************/
	PROCEDURE gm_fc_fch_dsapprovaldetail (
		p_demandsheetid   IN	   t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_detail		  OUT	   TYPES.cursor_type
	)
	AS
	BEGIN
		-- Change details
		OPEN p_detail
		 FOR
			 SELECT get_user_name (c4040_approved_by) approvedby
				  , TO_CHAR (c4040_approved_date, 'MM/DD/YYYY') approveddate, c901_status statusid
				  , get_log_comments (p_demandsheetid, 1226) appcomments
			   FROM t4040_demand_sheet t4040
			  WHERE t4040.c4040_demand_sheet_id = p_demandsheetid;
	END gm_fc_fch_dsapprovaldetail;

   /*******************************************************
   * Description : Procedure to fetch drill down details for a part
   * Author 	 : Joe P Kumar
   *******************************************************/
--
	PROCEDURE gm_fc_fch_partdrilldown (
		p_partnum		  IN	   t205_part_number.c205_part_number_id%TYPE
	  , p_demandsheetid   IN	   t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	  , p_grpinfo		  IN	   VARCHAR2
	  , p_output		  OUT	   TYPES.cursor_type
	)
	AS
		v_sheettype    t4020_demand_master.c901_demand_type%TYPE;
		v_from_month   t501_order.c501_order_date%TYPE;
		v_to_month	   t501_order.c501_order_date%TYPE;
	BEGIN
		SELECT t4020.c901_demand_type
		  INTO v_sheettype
		  FROM t4020_demand_master t4020, t4040_demand_sheet t4040
		 WHERE t4020.c4020_demand_master_id = t4040.c4020_demand_master_id
		   AND t4040.c4040_demand_sheet_id = p_demandsheetid;

		SELECT MIN (t4042.c4042_period), MAX (LAST_DAY (t4042.c4042_period))
		  INTO v_from_month, v_to_month
		  FROM t4042_demand_sheet_detail t4042
		 WHERE t4042.c4040_demand_sheet_id = p_demandsheetid AND t4042.c901_type = 50560;

		IF v_sheettype = 40020
		THEN
			OPEN p_output
			 FOR
				 SELECT   t502.c205_part_number_id pnum, get_partnum_desc (t502.c205_part_number_id) pdesc
						, v700.ac_name distname, get_rep_name (t501.c703_sales_rep_id) repname
						, t502.c501_order_id csgid, t502.c502_item_qty qty
						, TO_CHAR (t501.c501_order_date, 'MM/DD/YYYY') csgdate
						, TO_CHAR (t501.c501_order_date, 'YY-Mon') ordmon, '40020' stype
					 FROM v700_territory_mapping_detail v700
						, t4041_demand_sheet_mapping t4041
						, t501_order t501
						, t502_item_order t502
					WHERE t4041.c4040_demand_sheet_id = p_demandsheetid
					  AND t4041.c901_ref_type = 40032	-- 40032 is region
					  AND t4041.c4041_ref_id = v700.region_id
					  AND t501.c704_account_id = v700.ac_id
					  AND t501.c501_order_date >= v_from_month
					  AND t501.c501_order_date <= v_to_month
					  AND t501.c501_order_id = t502.c501_order_id
					  AND t501.c501_void_fl IS NULL
					  AND t502.C502_VOID_FL IS NULL 
					  AND (t501.c901_ext_country_id IS NULL OR t501.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH
					  AND NVL (t501.c901_order_type, -999) <> 2524
					  AND NVL (c901_order_type, -9999) NOT IN (
			                SELECT t906.c906_rule_value
			                  FROM t906_rules t906
			                 WHERE t906.c906_rule_grp_id = 'ORDTYPE'
			                   AND c906_rule_id = 'EXCLUDE')
					  AND t502.c205_part_number_id = DECODE (p_partnum, NULL, t502.c205_part_number_id, p_partnum)
					  AND t502.c205_part_number_id IN (
							  SELECT t4042.c205_part_number_id
								FROM t4042_demand_sheet_detail t4042
							   WHERE (	 t4042.c205_part_number_id IN DECODE (NVL (p_partnum, 1)
																			, 1, p_partnum
																			, p_partnum
																			 )
									  OR t4042.c4042_ref_id IN DECODE (NVL (p_grpinfo, 1), 1, p_grpinfo, p_grpinfo)
									 ))
				 ORDER BY t501.c501_order_date DESC, pnum;
		ELSIF (v_sheettype = 40021)
		THEN
			OPEN p_output
			 FOR
				 SELECT   t505.c205_part_number_id pnum, get_partnum_desc (t505.c205_part_number_id) pdesc
						, get_distributor_name (t504.c701_distributor_id) distname, t504.c504_consignment_id csgid
						, t505.c505_item_qty qty, TO_CHAR (t504.c504_ship_date, 'MM/DD/YYYY') csgdate
						, TO_CHAR (t504.c504_ship_date, 'YY-Mon') ordmon, '40021' stype
					 FROM t504_consignment t504
						, t505_item_consignment t505
						, (SELECT DISTINCT v700.d_id d_id
									  FROM v700_territory_mapping_detail v700, t4041_demand_sheet_mapping t4041
									 WHERE t4041.c4040_demand_sheet_id = p_demandsheetid
									   -- 910.906
									   AND t4041.c901_ref_type = 40032
									   -- 40032 is region
									   AND t4041.c4041_ref_id = v700.region_id) distlist
					WHERE t504.c701_distributor_id IS NOT NULL
					  AND t504.c504_consignment_id = t505.c504_consignment_id
					  AND t504.c701_distributor_id = distlist.d_id
					  AND DECODE (p_grpinfo, '-9999', NVL (t504.c207_set_id, '1'), NVL (t504.c207_set_id, '0')) =
									   DECODE (p_grpinfo
											 , '-9999', DECODE (t504.c207_set_id, NULL, '1', '0')
											 , p_grpinfo
											  )
					   -- Remove the consignment transfers
						AND t504.c504_consignment_id NOT IN (
			                    SELECT c921_ref_id
			                      FROM t920_transfer t920, t921_transfer_detail t921
			                     WHERE t920.c920_transfer_id = t921.c920_transfer_id
			                       AND t920.c920_void_fl IS NULL
			                       AND t920.c920_transfer_date >= v_from_month
			                       AND t920.c920_transfer_date <= v_to_month
			                       AND t921.c901_link_type = 90360)						  
					  AND t504.c504_ship_date >= v_from_month
					  AND t504.c504_ship_date <= v_to_month
					  AND t504.c504_status_fl = 4
					  AND t504.c504_type IN (4110)
					  AND TRIM (t505.c505_control_number) IS NOT NULL
					  AND c504_void_fl IS NULL
					  AND t504.c504_consignment_id NOT IN (SELECT a.c504_consignment_id a
															 FROM t504a_consignment_excess a
															WHERE a.c504_consignment_id = t504.c504_consignment_id)
					  AND t505.c205_part_number_id = DECODE (p_partnum, NULL, t505.c205_part_number_id, p_partnum)
					  AND ( t505.c901_type IS NULL OR t505.c901_type <> 100880 ) --Exclude  Consignments Adjusted for Tag
				 ORDER BY t504.c504_ship_date DESC, pnum;
		ELSIF v_sheettype = 40022
		THEN
			OPEN p_output
			 FOR
				SELECT * FROM (SELECT   t504.c207_set_id pnum, get_set_name (t504.c207_set_id) pdesc
						, get_user_name (t504.c504_ship_to_id) distname, t504.c504_consignment_id csgid
						, NVL (t505.c505_item_qty, 0) qty, TO_CHAR (t504.c504_ship_date, 'MM/DD/YYYY') csgdate
						, TO_CHAR (t504.c504_ship_date, 'YY-Mon') ordmon, '40022' stype
					 FROM t504_consignment t504, t505_item_consignment t505
					WHERE t504.c701_distributor_id IS NULL
					  AND t504.c504_consignment_id = t505.c504_consignment_id
					  AND DECODE (p_grpinfo, '-9999', NVL (t504.c207_set_id, '1'), NVL (t504.c207_set_id, '0')) =
									   DECODE (p_grpinfo
											 , '-9999', DECODE (t504.c207_set_id, NULL, '1', '0')
											 , p_grpinfo
											  )
					  AND t504.c704_account_id = '01'
					  -- Remove the consignment transfers
						AND t504.c504_consignment_id NOT IN (
			                    SELECT c921_ref_id
			                      FROM t920_transfer t920, t921_transfer_detail t921
			                     WHERE t920.c920_transfer_id = t921.c920_transfer_id
			                       AND t920.c920_void_fl IS NULL
			                       AND t920.c920_transfer_date >= v_from_month
			                       AND t920.c920_transfer_date <= v_to_month
			                       AND t921.c901_link_type = 90360)
					  AND t504.c504_ship_date >= v_from_month
					  AND t504.c504_ship_date <= v_to_month
					  AND t504.c504_status_fl = 4
					  AND TRIM (t505.c505_control_number) IS NOT NULL
					  AND c504_void_fl IS NULL
					  AND t505.c205_part_number_id = DECODE (p_partnum, NULL, t505.c205_part_number_id, p_partnum)
				UNION ALL
				 SELECT t413.c205_part_number_id pnum, get_partnum_desc (t413.c205_part_number_id) pdesc,
				 DECODE(t504a.c504a_loaner_transaction_id,NULL,'','R:')||get_rep_name(t504a.c703_sales_rep_id) distname,
				    t412.c412_inhouse_trans_id csgid, NVL (t413.c413_item_qty, 0) qty
				  , TO_CHAR (t413.c413_created_date, 'MM/DD/YYYY') csgdate, TO_CHAR (t413.c413_created_date, 'Mon-YYYY') ordmon,
				    '40023' stype
				   FROM t412_inhouse_transactions t412, t413_inhouse_trans_items t413 ,t504a_loaner_transaction t504a
				  WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
				    AND t412.c412_status_fl        = 4
				    AND t412.c412_void_fl         IS NULL
				    AND t413.c413_status_fl        = 'W'
				    AND t413.c413_void_fl         IS NULL
				    AND DECODE(p_grpinfo,'-99991','1',0) = DECODE(p_grpinfo,'-99991','1','-1') -- This condition is added to fetch data only for written off parts when drill down is clicked.
				    AND t412.c504a_loaner_transaction_id= t504a.c504a_loaner_transaction_id(+)
				    AND t413.c205_part_number_id   = DECODE (p_partnum, NULL, t413.c205_part_number_id, p_partnum)
				    AND t413.c413_created_date    >= v_from_month
				    AND t413.c413_created_date    <= v_to_month) ORDER BY csgdate DESC, pnum ;
		END IF;
	END gm_fc_fch_partdrilldown;

--

	/**************************************************************************
	 * Description : Procedure to fetch ttptype
	 *************************************************************************/
	PROCEDURE gm_fc_fch_ttpname (
		p_output   OUT	 TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_output
		 FOR
			 SELECT   c4050_ttp_id ID, c4050_ttp_nm ttpnm
				 FROM t4050_ttp
				WHERE c4050_void_fl IS NULL
			 ORDER BY UPPER (c4050_ttp_nm);
	END gm_fc_fch_ttpname;

--

	/**************************************************************************
		* Description : Procedure to fetch Region Info
		*************************************************************************/
	PROCEDURE gm_fc_fch_dsmapping (
		p_demandsheetid   IN	   t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_detail		  OUT	   TYPES.cursor_type
	)
	AS
	BEGIN
		-- Change details
		OPEN p_detail
		 FOR
			 SELECT   t4041.c4041_ref_id ref_id, get_code_name (t4041.c4041_ref_id) ref_name
				 FROM t4041_demand_sheet_mapping t4041
				WHERE t4041.c4040_demand_sheet_id = p_demandsheetid AND t4041.c901_ref_type IN (40032, 40033)
			 ORDER BY ref_name;
	END gm_fc_fch_dsmapping;

--
	PROCEDURE gm_fc_fch_requestdetail (
		p_dsmonthid   IN	   t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	  , p_type		  IN	   t4044_demand_sheet_request.c901_source_type%TYPE
	  , p_detail	  OUT	   TYPES.cursor_type
	)
	AS
	 /*******************************************************
	* Description : Procedure to fetch request information
	* Author		: Xun
	*******************************************************/
	BEGIN
		OPEN p_detail
		 FOR
			 SELECT t520.c520_request_id request_id, get_log_flag (t520.c520_request_id, 1239) wlog
				  , t504.c504_consignment_id consignment_id, t520.c520_request_for request_for
				  , TO_CHAR (t520.c520_request_date, 'MM/DD/YYYY') request_date
				  , TO_CHAR (t520.c520_required_date, 'MM/DD/YYYY') required_date
				  , DECODE (t520.c207_set_id
						  , NULL, get_op_request_part_number (t520.c520_request_id)
						  , t520.c207_set_id
						   ) set_id
				  , DECODE (t520.c520_request_for
						  , 40021, get_distributor_name (t520.c520_request_to)
						  , 40022, get_user_name (t520.c520_request_to)
						   ) request_to
				  , gm_pkg_op_request_summary.get_request_status (t4044.c4044_lock_status_fl) lock_status
				  , DECODE (t520.c520_void_fl
						  , 'Y', 'Voided'
						  , gm_pkg_op_request_summary.get_request_status (t520.c520_status_fl)
						   ) current_status
			   FROM t4044_demand_sheet_request t4044, t520_request t520, t504_consignment t504
			  WHERE t4044.c901_source_type = p_type
				AND t4044.c4040_demand_sheet_id = p_dsmonthid
				AND t4044.c520_request_id = t520.c520_request_id
				AND t4044.c520_request_id = t504.c520_request_id(+);
	END gm_fc_fch_requestdetail;

--
	FUNCTION get_op_request_part_number (
		p_requestid   t520_request.c520_request_id%TYPE
	)
		RETURN VARCHAR2
	IS
/**************************************************************
Description 	: THIS FUNCTION RETURNS part number  GIVEN THE request_ID
 Parameters 		: p_requestid
****************************************************************/
		v_partnumber   t521_request_detail.c205_part_number_id%TYPE;
		e_other_exp    EXCEPTION;
	BEGIN
		SELECT t521.c205_part_number_id pnum
		  INTO v_partnumber
		  FROM t521_request_detail t521
		 WHERE t521.c520_request_id = p_requestid;

		RETURN v_partnumber;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN ' ';
		WHEN e_other_exp
		THEN
			RETURN '*';
	-- RETURN v_partnumber;
	END get_op_request_part_number;

	  /*******************************************************
	* Description : Procedure to fetch request vs growth information
	* Author		: Xun
	*******************************************************/
	PROCEDURE gm_fch_growth_request_detail (
		p_dsmonthid   IN	   t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	  , p_detail	  OUT	   TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_detail
		 FOR
			 SELECT   c4030_ref_id ref_id
					, DECODE (t4030.c901_ref_type
							, 20296, get_set_name (t4030.c4030_ref_id)
							, get_partnum_desc (t4030.c4030_ref_id)
							 ) NAME
					, TO_CHAR (t4043.c4043_start_date, 'Mon-YY') required_month, NVL (t4043.c4043_value, 0) growth_qty
					, gm_pkg_op_sheet_summary.get_ds_growth_request_count (t4020.c4020_demand_master_id
																		 , t4030.c4030_ref_id
																		 , t4030.c901_ref_type
																		 , t4043.c4043_start_date
																		  ) request_qty
					, t4030.c901_ref_type ref_type
				 FROM t4040_demand_sheet t4040
					, t4043_demand_sheet_growth t4043
					, t4020_demand_master t4020
					, t4030_demand_growth_mapping t4030
				WHERE t4040.c4040_demand_sheet_id = p_dsmonthid
				  AND t4040.c4040_demand_sheet_id = t4043.c4040_demand_sheet_id
				  AND t4040.c4020_demand_master_id = t4020.c4020_demand_master_id
				  AND t4030.c4030_demand_growth_id = t4043.c4030_demand_growth_id
				  AND t4043.c4043_start_date <=
						  ADD_MONTHS (t4040.c4040_demand_period_dt
									, DECODE (t4040.c4040_request_period, 0, -1, (t4040.c4040_request_period - 1))
									 )
				  AND SIGN (NVL (t4043.c4043_value, 0)) <=
						  DECODE (t4043.c901_ref_type
								, 20381, SIGN (NVL (t4043.c4043_value, 0))
								, SIGN (NVL (t4043.c4043_value, 0) - 1)
								 )
			 ORDER BY t4030.c901_ref_type, c4030_ref_id;
	END gm_fch_growth_request_detail;

/*******************************************************
 * Purpose: function is used to fetch request qty
 *******************************************************/
--
	FUNCTION get_ds_growth_request_count (
		p_dsmid 	   IN	t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_ref_id	   IN	t4030_demand_growth_mapping.c4030_ref_id%TYPE
	  , p_ref_type	   IN	t4030_demand_growth_mapping.c901_ref_type%TYPE
	  , p_req_period   IN	t4043_demand_sheet_growth.c4043_start_date%TYPE
	)
		RETURN NUMBER
	IS
		v_req_qty	   NUMBER := 0;
		v_req_qty1	   NUMBER := 0;
		v_req_qty2	   NUMBER := 0;
	BEGIN
		IF (p_ref_type = 20296)
		THEN
			SELECT COUNT (1)
			  INTO v_req_qty
			  FROM t520_request t520
			 WHERE t520.c520_request_txn_id = p_dsmid
			   AND t520.c901_request_source = 50616
			   AND t520.c207_set_id = p_ref_id
			   AND t520.c520_required_date >= p_req_period
			   AND t520.c520_required_date <= LAST_DAY (p_req_period)
			   AND t520.c520_status_fl > 0
			   AND t520.c520_void_fl IS NULL
			   AND t520.c520_master_request_id IS NULL;
		ELSIF (p_ref_type = 20298)
		THEN
-- If mapped to part then execute below sql
-- fetch request qty
			SELECT NVL (SUM (t521.c521_qty), 0)
			  INTO v_req_qty1
			  FROM t520_request t520, t521_request_detail t521
			 WHERE t520.c520_request_id = t521.c520_request_id
			   AND t520.c520_request_txn_id = p_dsmid
			   AND t520.c901_request_source = 50616
			   AND t520.c207_set_id IS NULL
			   AND t520.c520_required_date >= p_req_period
			   AND t520.c520_required_date <= LAST_DAY (p_req_period)
			   AND t520.c520_status_fl > 0
			   AND t520.c520_void_fl IS NULL
			   AND t521.c205_part_number_id = p_ref_id
			   AND t520.c520_master_request_id IS NULL;

-- fetch consignment qty
			SELECT NVL (SUM (t505.c505_item_qty), 0)
			  INTO v_req_qty2
			  FROM t520_request t520, t504_consignment t504, t505_item_consignment t505
			 WHERE t520.c520_request_id = t504.c520_request_id
			   AND t504.c504_consignment_id = t505.c504_consignment_id
			   AND t520.c520_request_txn_id = p_dsmid
			   AND t520.c901_request_source = 50616
			   AND t520.c207_set_id IS NULL
			   AND t520.c520_required_date >= p_req_period
			   AND t520.c520_required_date <= LAST_DAY (p_req_period)
			   AND t520.c520_status_fl > 0
			   AND t520.c520_void_fl IS NULL
			   AND t505.c205_part_number_id = p_ref_id
			   AND t520.c520_master_request_id IS NULL;

			v_req_qty	:= v_req_qty1 + v_req_qty2;
		END IF;

		RETURN v_req_qty;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN 0;
	END get_ds_growth_request_count;

--
  /*******************************************************
   * Description : Procedure to fetch the open request for
				   given demandsheet and the set / part
   * Author 	 : VPrasath
   *******************************************************/
--
	PROCEDURE gm_fc_fch_growth_requestdetail (
		p_dsmid 		  IN	   t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	  , p_ref_id		  IN	   VARCHAR
	  , p_ref_type		  IN	   t4030_demand_growth_mapping.c901_ref_type%TYPE
	  , p_mon_year		  IN	   VARCHAR
	  , p_outrequests	  OUT	   TYPES.cursor_type
	  , p_request_count   OUT	   NUMBER
	)
	AS
		v_type		   NUMBER;
	BEGIN
		SELECT DECODE (NVL (TRIM (get_set_name (p_ref_id)), '0')
					 , '0', 20298
					 , NVL (TRIM (get_partnum_desc (p_ref_id)), '0'), '0'
					 , 20296
					  )
		  INTO v_type
		  FROM DUAL;

		OPEN p_outrequests
		 FOR
			 SELECT DISTINCT t520.c520_request_id request_id, t504.c504_consignment_id consignment_id
						   , TO_CHAR (t520.c520_request_date, 'MM/DD/YYYY') request_date
						   , TO_CHAR (t520.c520_required_date, 'MM/DD/YYYY') required_date
						   , DECODE (t520.c520_request_for
								   , 40021, get_distributor_name (t520.c520_request_to)
								   , 40022, get_user_name (t520.c520_request_to)
									) request_to
						   , t520.c520_request_for request_for
						   , gm_pkg_op_request_summary.get_request_status (t520.c520_status_fl) current_status
						   , t520.c207_set_id set_id
						FROM t520_request t520, t504_consignment t504, t521_request_detail t521
					   WHERE t520.c520_request_txn_id = p_dsmid
						 AND t520.c520_request_id = t521.c520_request_id(+)
						 AND t520.c901_request_source = 50616
						 AND NVL (t520.c207_set_id, 9999) =
														  NVL (DECODE (v_type, 20296, p_ref_id, t520.c207_set_id), 9999)
						 AND t520.c520_required_date BETWEEN TO_DATE (p_mon_year, 'Mon/YY')
														 AND LAST_DAY (TO_DATE (p_mon_year, 'Mon/YY'))
						 AND t520.c520_master_request_id IS NULL
						 AND t520.c520_void_fl IS NULL
						 AND t520.c520_delete_fl IS NULL
						 AND t520.c520_status_fl > 0
						 AND t520.c520_status_fl < 40
						 AND t504.c504_void_fl IS NULL
						 AND NVL (t521.c205_part_number_id, 9999) =
												  NVL (DECODE (v_type, 20298, p_ref_id, t521.c205_part_number_id), 9999)
						 AND t520.c520_request_id = t504.c520_request_id(+);

		SELECT get_ds_growth_request_count (p_dsmid, p_ref_id, v_type, TO_DATE (p_mon_year, 'Mon/YY'))
		  INTO p_request_count
		  FROM DUAL;
	END gm_fc_fch_growth_requestdetail;

	/*******************************************************
	  * Description : Procedure which takes the refid and month
					  and returns part status.
	  * Author	  : Lakshmi
	  *******************************************************/
	PROCEDURE gm_fch_setsheet (
		p_ref_id	   IN		VARCHAR
	  , p_mon_year	   IN		VARCHAR
	  , p_report_out   OUT		TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_report_out
		 FOR
			 
			  select C207_SET_ID set_id,  get_set_name (C207_SET_ID) set_nm, get_code_name(C901_FORECAST_TYPE) demshtname,
              sum(c4061_qty) COUNT
             from t4061_set_forecast_qty where 
             C207_SET_ID=p_ref_id and C4061_PERIOD BETWEEN TO_DATE (p_mon_year, 'Mon-YY')
												 AND LAST_DAY (ADD_MONTHS (TO_DATE (p_mon_year, 'Mon-YY'), 0))
												 AND C901_FORECAST_TYPE <> '4000113' -- SET PAR
                                                 group by C207_SET_ID,C901_FORECAST_TYPE
                                                 ORDER BY C207_SET_ID;
	END gm_fch_setsheet;

--
--
/***********************************************************
 * Purpose: Procedure to fetch the Multi part open sheets
 ***********************************************************/
--
	PROCEDURE gm_op_fch_multipart_opensheets (
		p_dsmid 	 IN 	  t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	  , p_open_cur	 OUT	  TYPES.cursor_type
	)
	AS
		v_inventory_id t4040_demand_sheet.c250_inventory_lock_id%TYPE;
	BEGIN
		SELECT t4040.c250_inventory_lock_id
		  INTO v_inventory_id
		  FROM t4040_demand_sheet t4040
		 WHERE t4040.c4040_demand_sheet_id = p_dsmid;

		OPEN p_open_cur
		 FOR
			 SELECT   t4040.c4040_demand_sheet_id ID
					, gm_pkg_op_sheet.get_demandsheet_name (t4040.c4020_demand_master_id) NAME
					, get_code_name (c901_status) status
				 FROM t4040_demand_sheet t4040
				WHERE t4040.c250_inventory_lock_id = v_inventory_id
				  AND t4040.c4020_demand_master_id NOT IN (SELECT t4045.c4020_demand_master_assoc_id
															 FROM t4045_demand_sheet_assoc t4045
															WHERE t4045.c4040_demand_sheet_id = p_dsmid)
				  AND t4040.c901_demand_type <> 40023
			 ORDER BY NAME, status;
	END gm_op_fch_multipart_opensheets;

--
--
/***********************************************************
 * Purpose: Procedure to fetch the details of the multi part
			Sheets
 ***********************************************************/
--
	PROCEDURE gm_op_fch_multisheet_details (
		p_multi_sheet_id	IN		 t4045_demand_sheet_assoc.c4040_demand_sheet_id%TYPE
	  , p_pnum				IN		 t4042_demand_sheet_detail.c205_part_number_id%TYPE
	  , p_forecast_months	IN		 VARCHAR2
	  , p_out_cur_header	OUT 	 TYPES.cursor_type
	  , p_out_cur_details	OUT 	 TYPES.cursor_type
	)
	AS
		v_date		   t4042_demand_sheet_detail.c4042_period%TYPE;
		v_multi_sheet_id t4045_demand_sheet_assoc.c4040_demand_sheet_id%TYPE;
	BEGIN
		IF p_multi_sheet_id IS NULL
		THEN
			SELECT c4040_demand_sheet_id
			  INTO v_multi_sheet_id
			  FROM t4040_demand_sheet t4040
			 WHERE t4040.c901_demand_type = 40023
			   AND t4040.c250_inventory_lock_id = (SELECT MAX (c250_inventory_lock_id)
													 FROM t250_inventory_lock
													WHERE c250_void_fl IS NULL AND c901_lock_type = 20430);
		ELSE
			v_multi_sheet_id := p_multi_sheet_id;
		END IF;

		SELECT MIN (t4042.c4042_period)
		  INTO v_date
		  FROM t4042_demand_sheet_detail t4042
		 WHERE t4042.c4040_demand_sheet_id = v_multi_sheet_id;

		OPEN p_out_cur_header
		 FOR
			 SELECT   TO_CHAR (t4042.c4042_period, 'Mon YY') period, t4042.c4042_period dt
				 FROM t4042_demand_sheet_detail t4042
				WHERE t4042.c4040_demand_sheet_id = v_multi_sheet_id
				  AND t4042.c901_type = 50563
				  AND t4042.c205_part_number_id = p_pnum
				  AND t4042.c4042_period <= ADD_MONTHS (v_date, p_forecast_months - 1)
			 UNION
			 SELECT   'STATUS', ADD_MONTHS (v_date, -1) dt
				 FROM DUAL
			 ORDER BY dt;

		OPEN p_out_cur_details
		 FOR
			 SELECT   master_id ID, gm_pkg_op_sheet.get_demandsheet_name (master_id) NAME
					, TO_CHAR (t4042.c4042_period, 'Mon YY') period, SUM (t4042.c4042_qty) qty
					, get_code_name (status) status
				 FROM (SELECT t4045.c4040_demand_sheet_id multi_sheet_id, t4045.c4020_demand_master_assoc_id master_id
							, t4045.c4045_demand_period_dt, t4045.c901_status status, t4040a.c4040_demand_sheet_id
						 FROM t4045_demand_sheet_assoc t4045, t4040_demand_sheet t4040a
						WHERE t4045.c4040_demand_sheet_id = v_multi_sheet_id
						  AND t4045.c4020_demand_master_assoc_id = t4040a.c4020_demand_master_id
						  AND t4045.c4045_demand_period_dt = t4040a.c4040_demand_period_dt) t4045
					, t4042_demand_sheet_detail t4042
				WHERE t4045.c4040_demand_sheet_id = t4042.c4040_demand_sheet_id
				  AND t4042.c901_type = 50563
				  AND t4042.c205_part_number_id = p_pnum
				  AND t4042.c4042_period <= ADD_MONTHS (v_date, p_forecast_months - 1)
			 GROUP BY master_id, t4042.c4042_period, status
			 ORDER BY NAME, t4042.c4042_period;
	END gm_op_fch_multisheet_details;

/***********************************************************
 * Purpose: Procedure to fetch the details of the all part
			Qty Details
 ***********************************************************/
	PROCEDURE gm_fc_fch_allpart_details (
		p_demandsheetid 	IN		 t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	  , p_partnum			IN		 VARCHAR2
	  , p_forecast_months	IN		 NUMBER
	  , p_current_date		IN		 VARCHAR2
	  , p_ref_id			IN		 t4042_demand_sheet_detail.c4042_ref_id%TYPE
	  , p_subheader 		OUT 	 TYPES.cursor_type
	  , p_parentheader		OUT 	 TYPES.cursor_type
	  , p_sub_detail		OUT 	 TYPES.cursor_type
	  , p_parent_detail 	OUT 	 TYPES.cursor_type
	  , p_forecast			OUT 	 TYPES.cursor_type
	  , p_partdesc			OUT 	 VARCHAR2
	  , p_header			OUT 	 TYPES.cursor_type
	  , p_detail			OUT 	 TYPES.cursor_type
	)
	AS
		v_type		   t4020_demand_master.c901_demand_type%TYPE;
	BEGIN
		SELECT get_partnum_desc (p_partnum)
		  INTO p_partdesc
		  FROM DUAL;

		SELECT t4040.c901_demand_type
		  INTO v_type
		  FROM t4040_demand_sheet t4040
		 WHERE t4040.c4040_demand_sheet_id = p_demandsheetid;

		-- Header information
		OPEN p_header
		 FOR
			 SELECT DISTINCT DECODE (t4042.c901_type
								   , 50560, TO_CHAR (t4042.c4042_period, 'Mon YY')
								   , 50563, TO_CHAR (t4042.c4042_period, 'Mon YY')
								   , get_code_name (t4042.c901_type)
									) period
						   , t4042.c4042_period, t4042.c901_type, t901.c901_code_seq_no seq_no
						FROM t4042_demand_sheet_detail t4042, t4040_demand_sheet t4040, t901_code_lookup t901
					   WHERE t4040.c4040_demand_sheet_id = p_demandsheetid
						 AND t4042.c901_type IN (50563, 50567, 50568, 50569)
						 AND t4040.c4040_demand_sheet_id = t4042.c4040_demand_sheet_id
						 AND t4042.c901_type = t901.c901_code_id
						 AND t4042.c4042_period <=
												 ADD_MONTHS (TO_DATE (p_current_date, 'MM/YYYY'), p_forecast_months - 1)
					ORDER BY seq_no, t4042.c4042_period;

		OPEN p_subheader
		 FOR
			 SELECT DISTINCT DECODE (t4042.c901_type
								   , 50560, TO_CHAR (t4042.c4042_period, 'Mon YY')
								   , 50563, TO_CHAR (t4042.c4042_period, 'Mon YY')
								   , get_code_name (t4042.c901_type)
									) period
						   , t4042.c4042_period, t4042.c901_type, t901.c901_code_seq_no seq_no
						FROM t4042_demand_sheet_detail t4042, t4040_demand_sheet t4040, t901_code_lookup t901
					   WHERE t4040.c4040_demand_sheet_id = p_demandsheetid
						 AND t4042.c901_type IN (50563, 50567, 50568, 50569)
						 AND t4040.c4040_demand_sheet_id = t4042.c4040_demand_sheet_id
						 AND t4042.c901_type = t901.c901_code_id
						 AND t4042.c4042_period <=
												 ADD_MONTHS (TO_DATE (p_current_date, 'MM/YYYY'), p_forecast_months - 1)
					ORDER BY seq_no, t4042.c4042_period;

		OPEN p_parentheader
		 FOR
			 SELECT DISTINCT DECODE (t4042.c901_type
								   , 50560, TO_CHAR (t4042.c4042_period, 'Mon YY')
								   , 50563, TO_CHAR (t4042.c4042_period, 'Mon YY')
								   , get_code_name (t4042.c901_type)
									) period
						   , t4042.c4042_period, t4042.c901_type, t901.c901_code_seq_no seq_no
						FROM t4042_demand_sheet_detail t4042, t4040_demand_sheet t4040, t901_code_lookup t901
					   WHERE t4040.c4040_demand_sheet_id = p_demandsheetid
						 AND t4042.c901_type IN (50563, 50567, 50568, 50569)
						 AND t4040.c4040_demand_sheet_id = t4042.c4040_demand_sheet_id
						 AND t4042.c901_type = t901.c901_code_id
						 AND t4042.c4042_period <=
												 ADD_MONTHS (TO_DATE (p_current_date, 'MM/YYYY'), p_forecast_months - 1)
					ORDER BY seq_no, t4042.c4042_period;

		-- sql used for sub parts
		OPEN p_detail
		 FOR
			 SELECT   DECODE (t205.c205_part_number_id
							, NULL, DECODE (t4042.c4042_ref_id
										  , NULL, 'Total'
										  , -9999, get_group_name (t4042.c4042_ref_id)
										  ,-99991, get_group_name (t4042.c4042_ref_id)
										  , 9999999999, get_group_name (t4042.c4042_ref_id)
										  , '' || '^' || t4042.c4042_ref_id
										   )
							, t205.c205_part_number_id || '^' || t4042.c4042_ref_id
							 ) num	 --T4042.C205_PART_NUMBER_ID
					, DECODE (t205.c205_part_number_id
							, NULL, DECODE (t4042.c4042_ref_id
										  , NULL, 'Total'
										  , -9999, get_group_name (t4042.c4042_ref_id)
										  ,-99991, get_group_name (t4042.c4042_ref_id)
										  , 9999999999, get_group_name (t4042.c4042_ref_id)
										  , DECODE (v_type
												  , 40020, get_group_name (t4042.c4042_ref_id)
												  , get_set_name (t4042.c4042_ref_id)
												   )
										   )
							, SUBSTR (t205.c205_part_number_id || '-' || t205.c205_part_num_desc, 1, 40)
							 ) des
					, DECODE (t4042.c901_type
							, 50560, TO_CHAR (t4042.c4042_period, 'Mon YY')
							, 50563, TO_CHAR (t4042.c4042_period, 'Mon YY')
							, t901.c901_code_nm
							 ) period
					, NVL (SUM (t4042.c4042_qty), 0) qty, '' empty
					, GROUPING_ID (t4042.c4042_ref_id, t205.c205_part_number_id) grpid
					, DECODE (GROUPING_ID (t4042.c4042_ref_id, t205.c205_part_number_id)
							, 0, MIN (NVL (t4042.c4042_history_fl, 'N'))
							, 'N'
							 ) hfl
					, t4042.c4042_ref_id grpcs
				 FROM t4042_demand_sheet_detail t4042, t205_part_number t205, t901_code_lookup t901
				WHERE t4042.c4040_demand_sheet_id = p_demandsheetid   --1672
				  AND t4042.c205_part_number_id = t205.c205_part_number_id
				  AND t4042.c901_type = t901.c901_code_id
				  AND t4042.c205_part_number_id = p_partnum
				  AND t4042.c901_type IN (50563, 50567, 50568, 50569)
				  AND t4042.c4042_period BETWEEN TO_DATE (p_current_date, 'MM/YYYY')   --('May 08', 'Mon YY')
											 AND ADD_MONTHS (TO_DATE (p_current_date, 'MM/YYYY')
														   , (p_forecast_months - 1)
															)
			 GROUP BY t4042.c901_type
					, t4042.c4042_period
					, t901.c901_code_nm
					, ROLLUP (t4042.c4042_ref_id, (t205.c205_part_num_desc, t205.c205_part_number_id))
			   HAVING (GROUPING_ID (t4042.c4042_ref_id, t205.c205_part_number_id) <> 1 OR t4042.c4042_ref_id IS NOT NULL
					  )
			 ORDER BY t4042.c4042_ref_id, grpid, num, t4042.c4042_period;

		OPEN p_sub_detail
		 FOR
			 SELECT   DECODE (t205.c205_part_number_id
							, NULL, DECODE (t4042.c4042_ref_id
										  , NULL, 'Total'
										  , -9999, get_group_name (t4042.c4042_ref_id)
										  ,-99991, get_group_name (t4042.c4042_ref_id)
										  , 9999999999, get_group_name (t4042.c4042_ref_id)
										  , '' || '^' || t4042.c4042_ref_id
										   )
							, t205.c205_part_number_id || '^' || t4042.c4042_ref_id
							 ) num	 --T4042.C205_PART_NUMBER_ID
					, DECODE (t205.c205_part_number_id
							, NULL, DECODE (t4042.c4042_ref_id
										  , NULL, 'Total'
										  , -9999, get_group_name (t4042.c4042_ref_id)
										  ,-99991, get_group_name (t4042.c4042_ref_id)
										  , 9999999999, get_group_name (t4042.c4042_ref_id)
										  , DECODE (v_type
												  , 40020, get_group_name (t4042.c4042_ref_id)
												  , get_set_name (t4042.c4042_ref_id)
												   )
										   )
							, SUBSTR (t205.c205_part_number_id || '-' || t205.c205_part_num_desc, 1, 40)
							 ) des
					, DECODE (t4042.c901_type
							, 50560, TO_CHAR (t4042.c4042_period, 'Mon YY')
							, 50563, TO_CHAR (t4042.c4042_period, 'Mon YY')
							, t901.c901_code_nm
							 ) period
					, NVL (SUM (t4042.c4042_qty), 0) qty, '' empty
					, GROUPING_ID (t4042.c4042_ref_id, t205.c205_part_number_id) grpid
					, DECODE (GROUPING_ID (t4042.c4042_ref_id, t205.c205_part_number_id)
							, 0, MIN (NVL (t4042.c4042_history_fl, 'N'))
							, 'N'
							 ) hfl
					, t4042.c4042_ref_id grpcs
				 FROM t4042_demand_sheet_detail t4042, t205_part_number t205, t901_code_lookup t901
				WHERE t4042.c4040_demand_sheet_id = p_demandsheetid   --1672
				  AND t4042.c205_part_number_id = t205.c205_part_number_id
				  AND t4042.c901_type = t901.c901_code_id
				  AND t4042.c205_part_number_id IN (
						  SELECT pnum
							FROM t205d_sub_part_to_order t205d
							   , (SELECT	 t205a.c205_to_part_number_id pnum
										FROM t205a_part_mapping t205a
								  START WITH t205a.c205_from_part_number_id = p_partnum
								  CONNECT BY PRIOR t205a.c205_to_part_number_id = t205a.c205_from_part_number_id
								  AND t205a.c205a_void_fl is null) sub    ---  added new column c205a_void_fl in t205a used to PMT-50777 - Create BOM and sub-component Mapping	
						   WHERE t205d.c205_part_number_id = sub.pnum)
				  AND t4042.c901_type IN (50563, 50567, 50568, 50569)
				  AND t4042.c4042_period BETWEEN TO_DATE (p_current_date, 'MM/YYYY')   --('May 08', 'Mon YY')
											 AND ADD_MONTHS (TO_DATE (p_current_date, 'MM/YYYY')
														   , (p_forecast_months - 1)
															)
				  AND t4042.c205_parent_part_num_id = p_partnum
			 GROUP BY t4042.c901_type
					, t4042.c4042_period
					, t901.c901_code_nm
					, ROLLUP (t4042.c4042_ref_id, (t205.c205_part_num_desc, t205.c205_part_number_id))
			   HAVING (GROUPING_ID (t4042.c4042_ref_id, t205.c205_part_number_id) <> 1 OR t4042.c4042_ref_id IS NOT NULL
					  )
			 ORDER BY t4042.c4042_ref_id, grpid, num, t4042.c4042_period;

		-- sql used for parent parts
		OPEN p_parent_detail
		 FOR
			 SELECT   DECODE (t205.c205_part_number_id
							, NULL, DECODE (t4042.c4042_ref_id
										  , NULL, 'Total'
										  , -9999, get_group_name (t4042.c4042_ref_id)
										  ,-99991, get_group_name (t4042.c4042_ref_id)
										  , 9999999999, get_group_name (t4042.c4042_ref_id)
										  , '' || '^' || t4042.c4042_ref_id
										   )
							, t205.c205_part_number_id || '^' || t4042.c4042_ref_id
							 ) num	 --T4042.C205_PART_NUMBER_ID
					, DECODE (t205.c205_part_number_id
							, NULL, DECODE (t4042.c4042_ref_id
										  , NULL, 'Total'
										  , -9999, get_group_name (t4042.c4042_ref_id)
										  ,-99991, get_group_name (t4042.c4042_ref_id)
										  , 9999999999, get_group_name (t4042.c4042_ref_id)
										  , DECODE (v_type
												  , 40020, get_group_name (t4042.c4042_ref_id)
												  , get_set_name (t4042.c4042_ref_id)
												   )
										   )
							, SUBSTR (t205.c205_part_number_id || '-' || t205.c205_part_num_desc, 1, 40)
							 ) des
					, DECODE (t4042.c901_type
							, 50560, TO_CHAR (t4042.c4042_period, 'Mon YY')
							, 50563, TO_CHAR (t4042.c4042_period, 'Mon YY')
							, t901.c901_code_nm
							 ) period
					, NVL (SUM (t4042.c4042_qty), 0) qty
					, GROUPING_ID (t4042.c4042_ref_id, t205.c205_part_number_id) grpid
					, DECODE (GROUPING_ID (t4042.c4042_ref_id, t205.c205_part_number_id)
							, 0, MIN (NVL (t4042.c4042_history_fl, 'N'))
							, 'N'
							 ) hfl
					, t4042.c4042_ref_id grpcs
				 FROM t4042_demand_sheet_detail t4042, t205_part_number t205, t901_code_lookup t901
				WHERE t4042.c4040_demand_sheet_id = p_demandsheetid
				  AND t4042.c205_part_number_id = t205.c205_part_number_id
				  AND t4042.c901_type = t901.c901_code_id
				  AND t4042.c205_part_number_id IN (
								SELECT	   t205a.c205_from_part_number_id parent_part
									  FROM t205a_part_mapping t205a
								START WITH t205a.c205_to_part_number_id IN (SELECT t205d.c205_part_number_id
																			  FROM t205d_sub_part_to_order t205d
																			 WHERE t205d.c205_part_number_id = p_partnum)
								CONNECT BY PRIOR t205a.c205_from_part_number_id = t205a.c205_to_part_number_id
								AND t205a.c205a_void_fl is null)   ---  added new column c205a_void_fl in t205a used to PMT-50777 - Create BOM and sub-component Mapping	
				  AND t4042.c901_type IN (50563, 50567, 50568, 50569)
				  AND t4042.c4042_period BETWEEN TO_DATE (p_current_date, 'MM/YYYY')
											 AND ADD_MONTHS (TO_DATE (p_current_date, 'MM/YYYY')
														   , (p_forecast_months - 1)
															)
			 GROUP BY t4042.c901_type
					, t4042.c4042_period
					, t901.c901_code_nm
					, ROLLUP (t4042.c4042_ref_id, (t205.c205_part_num_desc, t205.c205_part_number_id))
			   HAVING (GROUPING_ID (t4042.c4042_ref_id, t205.c205_part_number_id) <> 1 OR t4042.c4042_ref_id IS NOT NULL
					  )
			 ORDER BY t4042.c4042_ref_id, grpid, num, t4042.c4042_period;

		OPEN p_forecast
		 FOR
			 SELECT DISTINCT TO_CHAR (t4042.c4042_period, 'Mon YY') period, t4042.c4042_period
						FROM t4042_demand_sheet_detail t4042, t4040_demand_sheet t4040
					   WHERE t4040.c4040_demand_sheet_id = p_demandsheetid
						 AND t4040.c4040_demand_sheet_id = t4042.c4040_demand_sheet_id
						 AND t4042.c901_type = 50563
					ORDER BY t4042.c4042_period;
	END gm_fc_fch_allpart_details;

--
/***********************************************************
 * Purpose: Function Used to determine whether the given part
			has sub parts, parent parts or both.
 ***********************************************************/
--
	FUNCTION get_parent_sub_flag (
		p_part_num	 IN   t205_part_number.c205_part_number_id%TYPE
	)
		RETURN VARCHAR2
	IS
		v_sub_part_count NUMBER := 0;
		v_parent_part_count NUMBER := 0;
	BEGIN
		SELECT COUNT (pnum)
		  INTO v_sub_part_count
		  FROM t205d_sub_part_to_order t205d
			 , (SELECT	   t205a.c205_to_part_number_id pnum
					  FROM t205a_part_mapping t205a
				START WITH t205a.c205_from_part_number_id = p_part_num
				CONNECT BY PRIOR t205a.c205_to_part_number_id = t205a.c205_from_part_number_id
				AND t205a.c205a_void_fl is null) sub     ---  added new column c205a_void_fl in t205a used to PMT-50777 - Create BOM and sub-component Mapping
		 WHERE t205d.c205_part_number_id = sub.pnum;

		SELECT COUNT (pnum)
		  INTO v_parent_part_count
		  FROM t205d_sub_part_to_order t205d
			 , (SELECT	   t205a.c205_to_part_number_id pnum
					  FROM t205a_part_mapping t205a
				START WITH t205a.c205_to_part_number_id = p_part_num
				CONNECT BY PRIOR t205a.c205_from_part_number_id = t205a.c205_to_part_number_id
				AND t205a.c205a_void_fl is null) sub   ---  added new column c205a_void_fl in t205a used to PMT-50777 - Create BOM and sub-component Mapping	
		 WHERE t205d.c205_part_number_id = sub.pnum;

		IF v_sub_part_count > 0 AND v_parent_part_count > 0
		THEN
			RETURN 'A';
		ELSIF v_sub_part_count > 0
		THEN
			RETURN 'S';
		ELSIF v_parent_part_count > 0
		THEN
			RETURN 'P';
		ELSE
			RETURN '';
		END IF;
	END get_parent_sub_flag;
END gm_pkg_op_sheet_summary;
/
