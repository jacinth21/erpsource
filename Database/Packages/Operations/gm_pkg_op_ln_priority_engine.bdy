/* Formatted on 2011/05/26 19:20 (Formatter Plus v4.8.0) */
--@"c:\database\packages\operations\gm_pkg_op_ln_priority_engine.bdy";
CREATE OR REPLACE
PACKAGE BODY gm_pkg_op_ln_priority_engine
IS

	/*************************************************************************************
	* Description : Main Procedure to prioritize the set based on GM-CN id or request id
	* Author      : Arajan
	* ************************************************************************************/
	PROCEDURE gm_sav_main_ln_set_priority(
		p_txn_id	IN	t504_consignment.c504_consignment_id%TYPE
		,p_user_id	IN	t504a_consignment_loaner.c504a_last_updated_by%TYPE
	)
	AS
		v_plant_id		t5040_plant_master.c5040_plant_id%TYPE;
		v_comp_id		t1900_company.c1900_company_id%TYPE;
		cur_request		TYPES.cursor_type;
		cur_consignment	TYPES.cursor_type;
		v_consign_fl	VARCHAR2(1) := 'N';
		v_cn_id			t504_consignment.c504_consignment_id%TYPE;
		v_set_id		t504_consignment.c207_set_id%TYPE;
		v_type			t504_consignment.c504_type%TYPE;
		v_user_priority	t504a_consignment_loaner.c504a_user_priority%TYPE;
		v_prc_type		VARCHAR2(20);
		v_priority_chk_fl	VARCHAR2(1) := 'Y';
		v_type_fl		t906_rules.c906_rule_value%TYPE;
	BEGIN
	
		SELECT  get_plantid_frm_cntx() ,get_compid_frm_cntx() 
	  	INTO  v_plant_id ,v_comp_id
	  	FROM DUAL;
	  	
	  	-- Call function to check whether need to pioritize the set or not
	  	v_priority_chk_fl := get_priority_need_fl(p_txn_id,v_plant_id,p_user_id);
	  	
	  	IF v_priority_chk_fl = 'N'
	  	THEN
	  		RETURN;
	  	END IF;

	  	-- Get the details of request
	  	Gm_pkg_op_loaner_priority.gm_fch_request_dtls(p_txn_id, v_comp_id, v_plant_id,cur_request);
	  	
	  	-- Get the details of GM-CN-XXXX
	  	Gm_pkg_op_loaner_priority.gm_fch_consign_dtls(p_txn_id, v_comp_id, v_plant_id,cur_consignment);
	  	
	  	/* If the GM-CN-XXXX id is getting as input, value will be available in the cursor
	  	 * Call the procedure to prioritize the set based on GM-CN id
	  	 */
	  	LOOP
		  	FETCH cur_consignment INTO v_cn_id, v_set_id, v_type, v_user_priority, v_prc_type;
		    EXIT
		    WHEN cur_consignment%notfound;
		    
	    	v_consign_fl := 'Y';
	    	v_type_fl := get_rule_value(v_type,'SHOWPRIORITY');
	    	/* If user prioritized the set, no need to prioritize it again
	    	 * The set should be prioritized only if it is availble in rules table
	    	 */
	    	IF v_user_priority = 'Y' OR NVL(v_type_fl,'-9999') != 'Y'
	    	THEN
	    		RETURN;
	    	END IF;
	    	-- Call the below procedure to prioritize the set based on GM-CN id
	    	gm_sav_consign_set_priority(v_cn_id, v_set_id, v_comp_id, v_plant_id, p_user_id);
	    END LOOP;
	  	
	    -- if the txn id is not GM-CN id, call the procedure to prioritize sets based on request 
	    IF v_consign_fl <> 'Y'
	    THEN
	    	gm_sav_consign_req_priority(cur_request, v_comp_id, v_plant_id, p_user_id);
	    END IF;
	  	
	END gm_sav_main_ln_set_priority;
	
	/*****************************************************************************************************
	* Description : Procedure to prioritize the set based on the availability of set in shelf and request
	* Author      : Arajan
	* ****************************************************************************************************/
	PROCEDURE gm_sav_consign_set_priority(
		p_consign_id	IN		t504_consignment.c504_consignment_id%TYPE,
		p_set_id		IN		t504_consignment.c207_set_id%TYPE,
		p_company_id	IN		t1900_company.c1900_company_id%TYPE,
		p_plant_id		IN		t5040_plant_master.c5040_plant_id%TYPE,
		p_user_id		IN		t504a_consignment_loaner.c504a_last_updated_by%TYPE
	)
	AS
		v_avail_count		NUMBER;
		v_shipping_cnt		NUMBER;
		v_approved_cnt		NUMBER;
		v_pend_appr_cnt		NUMBER;
		v_priority			t901_code_lookup.c901_code_id%TYPE;
	BEGIN
		
		-- Get the count of set which is in available/ Pending putaway status
		v_avail_count := Gm_pkg_op_loaner_priority.get_avail_set_cnt(p_set_id,p_plant_id,p_company_id);
		-- Get the count of set which is having approved request and shipping date is the current date 
		v_shipping_cnt := Gm_pkg_op_loaner_priority.get_pend_ship_req_cnt(p_set_id,p_plant_id,p_company_id,p_consign_id); 
		
		-- if set is not available in shelf but approved request is availble with shipping date is current date, mark priority as critical
		IF v_avail_count = 0 AND v_shipping_cnt > 0
		THEN
			--103802 critical
			Gm_pkg_op_loaner_priority.gm_upd_loaner_priority_dtls(103802,p_set_id,p_user_id,p_consign_id,p_company_id,p_plant_id);
			RETURN;
		END IF;
		
		v_approved_cnt := Gm_pkg_op_loaner_priority.get_req_approved_cnt(p_set_id,p_plant_id,p_company_id,p_consign_id); 
		
		-- if set is not available in shelf but approved request is availble with shipping date is not current date, mark priority as high
		IF v_avail_count = 0 AND v_approved_cnt > 0
		THEN
			--103803 high
			Gm_pkg_op_loaner_priority.gm_upd_loaner_priority_dtls(103803,p_set_id,p_user_id,p_consign_id,p_company_id,p_plant_id);
			RETURN;
		END IF;
		
		v_pend_appr_cnt := Gm_pkg_op_loaner_priority.get_req_pend_appr_cnt(p_set_id,p_plant_id,p_company_id,p_consign_id);
		
		-- if set is not available in shelf but request is in pending approval, mark priority as medium
		IF v_avail_count = 0 AND v_pend_appr_cnt > 0
		THEN
			--103804 medium
			Gm_pkg_op_loaner_priority.gm_upd_loaner_priority_dtls(103804,p_set_id,p_user_id,p_consign_id,p_company_id,p_plant_id);
			RETURN;
		END IF;
		
		-- All the scenarios other than above, mark the priority as (103805) Low
		Gm_pkg_op_loaner_priority.gm_upd_loaner_priority(103805,p_user_id,p_consign_id);
		Gm_pkg_op_loaner_priority.gm_upd_ln_priority_log(p_consign_id,103805,'',p_set_id,p_user_id,p_company_id,p_plant_id);
		
	END gm_sav_consign_set_priority;
	
	/*****************************************************************************************************
	* Description : Procedure to prioritize the sets which are available in the request id
	* Author      : Arajan
	* ****************************************************************************************************/
	PROCEDURE gm_sav_consign_req_priority(
		p_req_cur		IN		TYPES.cursor_type,
		p_company_id	IN		t1900_company.c1900_company_id%TYPE,
		p_plant_id		IN		t5040_plant_master.c5040_plant_id%TYPE,
		p_user_id		IN		t504a_consignment_loaner.c504a_last_updated_by%TYPE
	)
	AS
		v_req_id			t525_product_request.c525_product_request_id%TYPE;
		v_req_det_id		t526_product_request_detail.c526_product_request_detail_id%TYPE;
		v_void_fl			t526_product_request_detail.c526_void_fl%TYPE;
		v_set_id			t526_product_request_detail.c207_set_id%TYPE;
		v_status			t526_product_request_detail.c526_status_fl%TYPE;
		v_req_dt			t526_product_request_detail.c526_required_date%TYPE;
		v_req_type			t526_product_request_detail.c901_request_type%TYPE;
		v_type_fl			t906_rules.c906_rule_value%TYPE;
		v_consign_id		t504_consignment.c504_consignment_id%TYPE;
	BEGIN
		
		LOOP
			FETCH p_req_cur INTO v_req_id, v_req_det_id, v_void_fl, v_set_id, v_status, v_req_dt, v_req_type;
		    EXIT
		    WHEN p_req_cur%notfound;
		
		    v_type_fl := get_rule_value(v_req_type,'SHOWPRIORITY');
	    	/* If the request status is allocated, no need to prioritize it
	    	 * The set should be prioritized only if it is availble in rules table
	    	 * If the request is rejected or voided, re prioritize the set
	    	 * 20 - Allocated
	    	 * 10 - Approved
	    	 * 5 - Pending Approval
	    	 * 40 - Rejected
	    	 */
		    
		    IF v_status = 20 OR NVL(v_type_fl,'-9999') != 'Y'
		    THEN
		    	BEGIN
    			/* When the request changed to allocated, check any CN was prioritized for the request or not
    			 * If Yes, reprioritise the set
    			 */
			    	SELECT C504_Consignment_Id INTO v_consign_id
					FROM T504a_Loaner_Priority_Log
					WHERE C526_Product_Request_Detail_Id = v_req_det_id
					AND C504a_Void_Fl                   IS NULL;
				EXCEPTION
				WHEN OTHERS THEN
					CONTINUE;
				END;

				gm_sav_main_ln_set_priority(v_consign_id,p_user_id);
		    
		    	CONTINUE;
		    END IF;
		    
		    IF v_void_fl = 'Y' OR v_status = 40
		    THEN
		    	gm_void_request_priority(v_set_id,v_req_det_id,p_user_id,p_company_id,p_plant_id);
		    	CONTINUE;
		    END IF;
		    
		    IF v_status = 10
		    THEN
		    	gm_sav_apprd_request_priority(v_set_id,v_req_dt,v_req_det_id,p_company_id,p_plant_id,p_user_id);
		    	CONTINUE;
		    END IF;
		    
		    IF v_status = 5
		    THEN
		    	gm_sav_pend_appr_req_priority(v_set_id,v_req_dt,v_req_det_id,p_company_id,p_plant_id,p_user_id);
		    	CONTINUE;
		    END IF;
		    
		END LOOP;
		
	END gm_sav_consign_req_priority;
	
	/*****************************************************************************************************
	* Description : Procedure to prioritize when the request is in approved status
	* Author      : Arajan
	* ****************************************************************************************************/
	PROCEDURE gm_sav_apprd_request_priority(
		p_set_id		IN		t504_consignment.c207_set_id%TYPE,
		p_req_date		IN		t526_product_request_detail.c526_required_date%TYPE,
		p_req_det_id	IN		t526_product_request_detail.c526_product_request_detail_id%TYPE,
		p_company_id	IN		t1900_company.c1900_company_id%TYPE,
		p_plant_id		IN		t5040_plant_master.c5040_plant_id%TYPE,
		p_user_id		IN		t504a_consignment_loaner.c504a_last_updated_by%TYPE
	)
	AS
		v_avail_count	NUMBER;
		v_cnid_cur		TYPES.cursor_type;
		v_consign_id	t504_consignment.c504_consignment_id%TYPE;
		v_priority		t504a_consignment_loaner.c504a_loaner_priority%TYPE;
	BEGIN
		
		-- Get the count of set which is in available/ Pending putaway status
		v_avail_count := Gm_pkg_op_loaner_priority.get_avail_set_cnt(p_set_id,p_plant_id,p_company_id);
		
		IF v_avail_count > 0
		THEN
			RETURN;
		END IF;
		
		gm_fch_consignid_in_process(p_set_id,p_req_det_id,p_company_id,p_plant_id,v_cnid_cur);
		
		LOOP
		FETCH v_cnid_cur INTO v_consign_id, v_priority;
		EXIT
		WHEN v_cnid_cur%notfound;
			-- 103802: Critical
			IF trunc(p_req_date) = trunc(CURRENT_DATE) AND NVL(v_priority,'-9999') != 103802
			THEN
				Gm_pkg_op_loaner_priority.gm_upd_loaner_priority_dtls(103802,p_set_id,p_user_id,v_consign_id,p_company_id,p_plant_id,p_req_det_id);
				EXIT;
			END IF;

			-- 103803: High
			IF trunc(p_req_date) <> trunc(CURRENT_DATE) AND (NVL(v_priority,'-9999') != 103802 OR NVL(v_priority,'-9999') != 103803)
			THEN
				Gm_pkg_op_loaner_priority.gm_upd_loaner_priority_dtls(103803,p_set_id,p_user_id,v_consign_id,p_company_id,p_plant_id,p_req_det_id);
				EXIT;
			END IF;
		
		END LOOP;
		
	END gm_sav_apprd_request_priority;
	
	/*****************************************************************************************************
	* Description : Procedure to prioritize the sets when request is in Pending approval status
	* Author      : Arajan
	* ****************************************************************************************************/
	PROCEDURE gm_sav_pend_appr_req_priority(
		p_set_id		IN		t504_consignment.c207_set_id%TYPE,
		p_req_date		IN		t526_product_request_detail.c526_required_date%TYPE,
		p_req_det_id	IN		t526_product_request_detail.c526_product_request_detail_id%TYPE,
		p_company_id	IN		t1900_company.c1900_company_id%TYPE,
		p_plant_id		IN		t5040_plant_master.c5040_plant_id%TYPE,
		p_user_id		IN		t504a_consignment_loaner.c504a_last_updated_by%TYPE
	)
	AS
		v_avail_count	NUMBER;
		v_cnid_cur		TYPES.cursor_type;
		v_consign_id	t504_consignment.c504_consignment_id%TYPE;
		v_priority		t504a_consignment_loaner.c504a_loaner_priority%TYPE;
	BEGIN
		
		-- Get the count of set which is in available/ Pending putaway status
		v_avail_count := Gm_pkg_op_loaner_priority.get_avail_set_cnt(p_set_id,p_plant_id,p_company_id);
		
		IF v_avail_count > 0
		THEN
			RETURN;
		END IF;
		
		gm_fch_consignid_in_process(p_set_id,p_req_det_id,p_company_id,p_plant_id,v_cnid_cur);
		
		LOOP
		FETCH v_cnid_cur INTO v_consign_id, v_priority;
		EXIT
		WHEN v_cnid_cur%notfound;
			-- 103805: Low; 103804: Medium
			IF (v_priority = 103805 OR v_priority IS NULL)
			THEN
				Gm_pkg_op_loaner_priority.gm_upd_loaner_priority_dtls(103804,p_set_id,p_user_id,v_consign_id,p_company_id,p_plant_id,p_req_det_id);
				EXIT;
			END IF;
		
		END LOOP;
		
	END gm_sav_pend_appr_req_priority;
	
	/******************************************************************************************************************
	* Description : Procedure to remove the existing priority and reprioritize the consignment when request is voided
	* Author      : Arajan
	* *****************************************************************************************************************/
	PROCEDURE gm_void_request_priority(
		p_set_id		IN		t504_consignment.c207_set_id%TYPE,
		p_req_id		IN		t525_product_request.c525_product_request_id%TYPE,
		p_user_id		IN		t504a_consignment_loaner.c504a_last_updated_by%TYPE,
		p_company_id	IN		t1900_company.c1900_company_id%TYPE,
		p_plant_id		IN		t5040_plant_master.c5040_plant_id%TYPE
	)
	AS
		v_consign_id t504_consignment.c504_consignment_id%TYPE;
	BEGIN
		BEGIN
			SELECT C504_Consignment_Id INTO v_consign_id
			FROM T504a_Loaner_Priority_Log
			WHERE C207_Set_Id                  = p_set_id
			AND C526_Product_Request_Detail_Id = p_req_id
			AND C504a_Void_Fl                 IS NULL;
			--AND C504a_Txn_Complete_Fl		  IS NULL;
		EXCEPTION
		WHEN OTHERS
		THEN 
			UPDATE T504a_Loaner_Priority_Log
			SET C504a_Void_Fl = 'Y',
				c504a_last_updated_by = p_user_id,
				c504a_last_updated_date = CURRENT_DATE
			WHERE C207_Set_Id = p_set_id
				AND C526_Product_Request_Detail_Id = p_req_id
				AND C504a_Void_Fl IS NULL;
		
			RETURN;
		END;
		
		UPDATE T504a_Loaner_Priority_Log
		SET C504a_Void_Fl = 'Y',
			c504a_last_updated_by = p_user_id,
			c504a_last_updated_date = CURRENT_DATE
		WHERE C207_Set_Id = p_set_id
			AND C526_Product_Request_Detail_Id = p_req_id
			AND C504a_Void_Fl IS NULL;
		
		UPDATE T504A_CONSIGNMENT_LOANER
		SET C504a_Loaner_Priority = NULL,
			c504a_last_updated_by = p_user_id,
			c504a_last_updated_date = CURRENT_DATE
		WHERE C504_Consignment_Id = v_consign_id
		AND C504a_Void_Fl IS NULL
		AND C504a_user_priority IS NULL;
		
		gm_sav_main_ln_set_priority(v_consign_id,p_user_id);
		
	END gm_void_request_priority;
	
	/*****************************************************************************************************
	* Description : Procedure to fetch the details of set which are in processing
	* Author      : Arajan
	* ****************************************************************************************************/
	PROCEDURE gm_fch_consignid_in_process(
		p_set_id		IN		t504_consignment.c207_set_id%TYPE,
		p_req_det_id	IN		t504a_loaner_priority_log.c526_product_request_detail_id%TYPE,
		p_company_id	IN		t1900_company.c1900_company_id%TYPE,
		p_plant_id		IN		t5040_plant_master.c5040_plant_id%TYPE,
		p_out_cur		OUT		TYPES.cursor_type
	)
	AS
	v_exist_fl		NUMBER;
	BEGIN
		
		SELECT COUNT(1) INTO v_exist_fl
		FROM T504a_Consignment_Loaner T504a,
		  T504a_Loaner_Priority_Log T504log
		WHERE T504a.C504_Consignment_Id            = T504log.C504_Consignment_Id
		AND T504log.C207_Set_Id                    = p_set_id
		AND T504log.C526_Product_Request_Detail_Id = p_req_det_id
		--AND T504log.C504a_Txn_Complete_Fl		  IS NULL
		AND T504log.C504a_Void_Fl                 IS NULL
		AND T504a.C504a_Void_Fl                   IS NULL;
		
		-- IF the same set is already prioritized based on a request, then reprioritize the same set instead of prioritizing another set
		IF v_exist_fl >0
		THEN
			OPEN p_out_cur
			FOR
				SELECT T504a.C504_Consignment_Id Consignid,
				  T504a.C504a_Loaner_Priority
				FROM T504a_Consignment_Loaner T504a,
				  T504a_Loaner_Priority_Log T504log
				WHERE T504a.C504_Consignment_Id            = T504log.C504_Consignment_Id
				AND T504log.C207_Set_Id                    = p_set_id
				AND T504log.C526_Product_Request_Detail_Id = p_req_det_id
				--AND T504log.C504a_Txn_Complete_Fl		  IS NULL
				AND T504log.C504a_Void_Fl                 IS NULL
				AND T504a.C504a_Void_Fl                   IS NULL;
		ELSE
			OPEN p_out_cur
			FOR
				SELECT T504.C504_Consignment_Id consignid,
				  T504a.C504a_Loaner_Priority priority
				FROM T504_Consignment T504,
				  T504a_Consignment_Loaner T504a
				WHERE T504.C504_Consignment_Id = T504a.C504_Consignment_Id
				AND T504.C207_Set_Id           = p_set_id
				AND T504a.C504a_Status_Fl     IN (SELECT C906_Rule_Value
					  FROM T906_Rules
					  WHERE C906_Rule_Id   = 'CHNGLNPRIORITY'
					  AND C906_Rule_Grp_Id = 'LNPRIORITY'
					  And C906_Void_Fl    IS NULL
					  )
				AND T504a.C504a_User_Priority IS NULL
				AND T504.C504_Void_Fl         IS NULL
				AND T504a.C504a_Void_Fl       IS NULL
				AND T504a.c5040_plant_id = p_plant_id
				ORDER BY T504a.C504a_Loaner_Priority;
		END IF;
		
	END gm_fch_consignid_in_process;
	
	/*************************************************************************
	 * Description: To reset the priority of all the set when the Job runs
	 * Author: Arajan
	 ***********************************************************************/
	PROCEDURE gm_upd_loaner_set_priority
	AS
	v_plant_id		T504a_Consignment_Loaner.C5040_Plant_Id%TYPE;
	BEGIN
		
		SELECT  get_plantid_frm_cntx()
	  	INTO  v_plant_id
	  	FROM DUAL;
		
		UPDATE T504a_Consignment_Loaner
		SET C504a_Loaner_Priority = NULL,
		  C504a_User_Priority     = NULL
		WHERE C5040_Plant_Id      = v_plant_id
		and C504a_Loaner_Priority is not null
		AND C504a_Void_Fl        IS NULL;
		
		UPDATE T504a_Loaner_Priority_Log
		SET C504a_Void_Fl  = 'Y',
		  C504a_Last_Updated_By    = 30301,
		  C504a_Last_Updated_Date  = CURRENT_DATE
		WHERE C5040_Plant_Id       = v_plant_id
		AND C504a_Void_Fl         IS NULL;
		
	END gm_upd_loaner_set_priority;
	
	/***************************************************************************************************************
	 * Description: To Remove the existing priority and reprioritize the set while doing roll back to Pending return
	 * 				or if the set is allocated
	 * Author: Arajan
	 ****************************************************************************************************************/
	PROCEDURE gm_rollback_cn_priority(
		p_priority		IN		T504a_Consignment_Loaner.C504a_Loaner_Priority%TYPE,
		p_userid		IN		T504a_Consignment_Loaner.c504a_last_updated_by%TYPE,
		p_conid			IN		T504a_Consignment_Loaner.C504_Consignment_Id%TYPE
	)
	AS
		v_alloc_fl		T504a_Consignment_Loaner.C504a_Allocated_Fl%TYPE;
		v_status_fl		T504a_Consignment_Loaner.c504a_status_fl%TYPE;
	BEGIN
		BEGIN
			SELECT C504a_Allocated_Fl, c504a_status_fl
			INTO v_alloc_fl, v_status_fl
			FROM T504a_Consignment_Loaner
			WHERE C504_Consignment_Id = p_conid
			AND C504a_Void_Fl        IS NULL;
		EXCEPTION
		WHEN NO_DATA_FOUND THEN
			v_alloc_fl := NULL;
		END;
		-- If the allocated flag is 'Y' need to remove the priority even if user prioritized it, otherwise do not remove the user priority
		--20: Pending Return; while doing rollback to Pending return, remove user priority also
		IF v_alloc_fl = 'Y' OR v_status_fl = 20
		THEN
			Gm_pkg_op_loaner_priority.gm_upd_user_priority(null,p_userid,p_conid);
		ELSE
			Gm_pkg_op_loaner_priority.gm_upd_loaner_priority(null,p_userid,p_conid);
		END IF;
		
		UPDATE T504a_Loaner_Priority_Log
		SET c504a_void_fl = 'Y',
			c504a_last_updated_date = CURRENT_DATE,
			c504a_last_updated_by = p_userid
		WHERE c504_consignment_id = p_conid
			AND c504a_void_fl IS NULL;
		
	END gm_rollback_cn_priority;

	/***************************************************************************************************************
	 * Description: To check whether need to prioritize the set or not
	 * Author: Arajan
	 ****************************************************************************************************************/
	FUNCTION get_priority_need_fl(
		p_txn_id		IN		t504_consignment.c504_consignment_id%TYPE,
		p_plant_id		IN		T504a_Consignment_Loaner.C5040_Plant_Id%TYPE,
		p_user_id		IN		t504a_consignment_loaner.c504a_last_updated_by%TYPE
	)
	RETURN VARCHAR2
	IS
	v_priority_chk_fl		VARCHAR2(1) := 'Y';
	v_status_fl				T504a_Consignment_Loaner.C504a_Status_Fl%TYPE;
	v_rule_cnt				NUMBER;
	v_plant_fl				VARCHAR2(1);
	BEGIN
		
		-- Prioritize the set only if it is enabled for the plant
	  	SELECT get_rule_value(p_plant_id,'SHOWPRIORITY') 
	  		INTO v_plant_fl 
	  	FROM DUAL;
	  	
	  	IF v_plant_fl <> 'Y'
	  	THEN
	  		v_priority_chk_fl:= 'N';
	  		RETURN v_priority_chk_fl;
	  	END IF;
	  	
	  	-- Prioritize the set only if the status of the set is available in rules table
	  	BEGIN
		  	SELECT C504a_Status_Fl
		  	INTO v_status_fl
			FROM T504a_Consignment_Loaner
			WHERE C504_Consignment_Id = p_txn_id
			AND C504a_Void_Fl        IS NULL
			AND C5040_Plant_Id        = p_plant_id;
		EXCEPTION
		WHEN NO_DATA_FOUND THEN
			v_priority_chk_fl	:= 'Y';
			RETURN v_priority_chk_fl;
	  	END;
		
	  	SELECT COUNT(1)
	  	INTO v_rule_cnt
		FROM T906_Rules
		WHERE C906_Rule_Id   = 'CHNGLNPRIORITY'
		AND C906_Rule_Grp_Id = 'LNPRIORITY'
		AND C906_Rule_Value  = v_status_fl
		AND C906_Void_Fl    IS NULL;

		/* If the stautus is not given in the above rule, then check whether the status is available/pending putaway/Pending shipping
		 * If yes, call the below procedure to reset the priority of the set
		 */
	  	IF v_rule_cnt = 0
		THEN
			gm_pkg_op_loaner_priority.gm_upd_reset_priority(NULL,p_txn_id,p_user_id, v_status_fl);
			v_priority_chk_fl:= 'N';
		END IF;
		
		v_priority_chk_fl:= 'Y';
		RETURN v_priority_chk_fl;
		
	END get_priority_need_fl;
	
END gm_pkg_op_ln_priority_engine;
/