CREATE OR REPLACE PACKAGE body gm_pkg_op_bba_label_print
IS
  --
  /************************************************************************
  * Description : procedure to get the transaction details for the given transaction id
  *               which are controlled and not voided. 
  * Transaction Used:RS, PTRD.
  * Company: BoneBank Allografts
  * Author      : Gomathi Palani
  ************************************************************************/
  
PROCEDURE gm_fetch_txn_details(
    p_txn_id IN VARCHAR2,
    p_out_dtl_cur OUT types.cursor_type )
    
  AS
  BEGIN	
  
  OPEN p_out_dtl_cur FOR
  
    SELECT
      t408a.c205_part_number_id Part_Number, t408a.c408a_conrol_number Control_Number, t408.c408_packing_slip packslip,
      t2540.c2540_donor_number donorno, t408.c2550_expiry_date exp_date, t408.c408_created_date createddate,
      t408.c408_received_date receiveddate, t408.c408_dhr_id dhrid, t408a.c408a_custom_size lblsize,
      t205m.c205m_generic_spec GENERICSPEC , t205.c205_part_num_desc Part_Desc, t205m.c205m_patient_label, t205m.c205m_box_label BOXLABEL,
      t205m.c205m_package_label, t205m.c205m_size,t205m.c205m_sample, t704b.c704b_cpc_barcode_lbl_ver PATIENTLABEL,
      t704b.c704b_cpc_final_cpc_ver FINALCPCVER,t704b.c704b_cpc_name CPCNAME,t704b.c704b_cpc_address CPCADDRESS, NVL(t7040.c7040_PRINT_QTY,1) Quantity
      , t7040.c7040_LABEL_PATH LBL_PATH,t901.C901_CODE_NM STORAGETEMP,T704B.C704B_BOX_LOGO CPCBARCODELOGO,T704B.C704B_FINAL_PACKAGING_LOGO CPCFINALLOGO,t704b.C704B_ACCOUNT_CPC_MAP_ID CPCID 
    FROM
      t408_dhr t408 ,t408a_dhr_control_number t408a ,t2540_donor_master t2540, t4081_dhr_bulk_receive t4081,
      t4082_bulk_dhr_mapping t4082, t205_part_number t205, t704b_account_cpc_mapping t704b, t205m_part_label_parameter t205m
      ,T7040_ACCOUNT_CPC_LABEL_MAPPING t7040,
       t205d_part_attribute t205d,
       t901_code_lookup t901
    WHERE
      t408a.c408_dhr_id        = t408.c408_dhr_id
    AND t2540.c2540_donor_id   = t408.c2540_donor_id
    AND t408.c408_void_fl     IS NULL
    AND t7040.C704B_ACCOUNT_CPC_MAP_ID(+)=t704b.C704B_ACCOUNT_CPC_MAP_ID
    AND t2540.c2540_void_fl   IS NULL
    AND t408.c408_status_fl   != 4
    AND t205d.C901_ATTRIBUTE_TYPE='103728'  --Storage Temperature
	AND t205d.c205_part_number_id=t205.c205_part_number_id
	AND t205d.c205d_void_fl IS NULL
	AND t205m.c205_part_number_id=t205d.c205_part_number_id
	AND t901.c901_code_id=t205d.c205d_attribute_value
    AND t4081.c4081_dhr_bulk_id=t4082.c4081_dhr_bulk_id
    AND t4082.c408_dhr_id      =t408a.c408_dhr_id
    AND t408a.c408_dhr_id      =t408.c408_dhr_id
    AND t4082.c408_dhr_id      =t408.c408_dhr_id
    AND t4082.c4082_void_fl          IS NULL
    AND t4081.c4081_void_fl          IS NULL
    AND (t4081.c4081_dhr_bulk_id       =p_txn_id OR
    t2540.c2540_donor_number=p_txn_id
    )
    AND t704b.c704b_account_cpc_map_id=t205m.c901_contract_proc_client
    AND t205m.c2550_void_fl          IS NULL
    AND t205m.c205_part_number_id     =t205.c205_part_number_id
    AND t205m.c205_part_number_id     =t408.c205_part_number_id
    AND t704b.c704b_void_fl          IS NULL
   
    UNION
    
    SELECT
      NVL(t413.c205_client_part_number_id ,t413.c205_part_number_id) Part_Number ,
      t413.c413_control_number Control_Number , t2550.c2550_packing_slip packslip , t2540.c2540_donor_number donorno ,
      t2550.c2550_expiry_date  exp_date, t2550.c2550_last_updated_date createddate ,  t2550.c2550_last_updated_date receiveddate,
      t413.c412_inhouse_trans_id , t205m.C205M_SIZE lblsize,
      t205m.c205m_generic_spec GENERICSPEC , t205.c205_part_num_desc Part_Desc, t205m.c205m_patient_label, t205m.c205m_box_label BOXLABEL, t205m.c205m_package_label,
      t205m.c205m_size,t205m.c205m_sample, t704b.c704b_cpc_barcode_lbl_ver PATIENTLABEL, t704b.c704b_cpc_final_cpc_ver FINALCPCVER,
      t704b.c704b_cpc_name CPCNAME, t704b.c704b_cpc_address CPCADDRESS ,NVL(t7040.c7040_PRINT_QTY,1) Quantity
      ,t7040.c7040_LABEL_PATH LBL_PATH,t901.C901_CODE_NM STORAGETEMP,T704B.C704B_BOX_LOGO CPCBARCODELOGO,T704B.C704B_FINAL_PACKAGING_LOGO CPCFINALLOGO
      ,t704b.C704B_ACCOUNT_CPC_MAP_ID CPCID
    FROM
      t412_inhouse_transactions t412 , t413_inhouse_trans_items t413, t2540_donor_master t2540 ,  t2550_part_control_number t2550,
      t205_part_number t205, t704b_account_cpc_mapping t704b, t205m_part_label_parameter t205m
       ,T7040_ACCOUNT_CPC_LABEL_MAPPING t7040,
       t205d_part_attribute t205d,
 		t901_code_lookup t901
    WHERE
      t412.c412_inhouse_trans_id         = t413.c412_inhouse_trans_id
    AND t412.c412_status_fl             != '4'      --not verified
    AND t205d.C901_ATTRIBUTE_TYPE='103728'  --Storage Temperature
	AND t205d.c205_part_number_id=t205.c205_part_number_id
	AND t205d.c205d_void_fl IS NULL
	AND t205d.c205_part_number_id=t2550.c205_part_number_id
	AND t205m.c205_part_number_id=t205d.c205_part_number_id
	AND t901.c901_code_id=t205d.c205d_attribute_value
    AND t412.c412_type                   = '103932' -- ptrd
    AND t412.c412_void_fl               IS NULL
    AND t7040.C704B_ACCOUNT_CPC_MAP_ID(+)=t704b.C704B_ACCOUNT_CPC_MAP_ID
    AND t413.c413_void_fl               IS NULL
    AND (t412.c412_inhouse_trans_id       = p_txn_id
    OR
    t2540.c2540_donor_number=p_txn_id)
    AND t413.c205_client_part_number_id IS NOT NULL
    AND t2540.c2540_donor_id             = t2550.c2540_donor_id
    AND t2550.c2550_control_number       = t413.C413_CONTROL_NUMBER
    AND t205.c205_part_number_id         =t413.c205_part_number_id
    AND t704b.c704b_account_cpc_map_id   =t205m.c901_contract_proc_client
    AND t205m.c205_part_number_id        =t205.c205_part_number_id
    AND t2540.c2540_void_fl             IS NULL
   ORDER BY 
    donorno,Part_Number ,Control_Number, Quantity  ASC;
    
 END gm_fetch_txn_details;
 
      
/************************************************************************
    * Description : Procedure to get Part and Allograft Details
    * Author      : Gomathi Palani
************************************************************************/
PROCEDURE gm_fetch_part_details (
p_part_id   	    IN  VARCHAR2,
p_control_num   	IN  VARCHAR2,
p_out_dtl_cur	OUT  TYPES.cursor_type
)

AS
BEGIN
/*1ST Fetch Query to fetch the Part and Control Number when a RS Transaction in-Progress - At this time, Graft information will not be available 
 in t2550 table, so in the 1st union we have excluded t2550.
*/
OPEN p_out_dtl_cur FOR

SELECT
  t408a.c205_part_number_id Part_Number,  t408a.c408a_conrol_number Control_Number,  t408.c408_packing_slip packslip,
  t2540.c2540_donor_number donorno,  t408.c2550_expiry_date exp_date,  t408.c408_created_date createddate,
  t408.c408_received_date receiveddate,  t408.c408_dhr_id dhrid,  t408a.c408a_custom_size lblsize,
  t205m.c205m_generic_spec GENERICSPEC ,  t205.c205_part_num_desc Part_Desc,  t205m.c205m_patient_label,
  t205m.c205m_box_label BOXLABEL, t205m.c205m_package_label,  t205m.c205m_size,
  t205m.c205m_sample,  t704b.c704b_cpc_barcode_lbl_ver PATIENTLABEL,  t704b.c704b_cpc_final_cpc_ver FINALCPCVER,
  t704b.c704b_cpc_name CPCNAME,  t704b.c704b_cpc_address CPCADDRESS ,NVL(t7040.c7040_PRINT_QTY,1) Quantity
  ,t901.C901_CODE_NM STORAGETEMP,T704B.C704B_BOX_LOGO CPCBARCODELOGO,T704B.C704B_FINAL_PACKAGING_LOGO CPCFINALLOGO
  ,t704b.C704B_ACCOUNT_CPC_MAP_ID CPCID,t7040.c7040_LABEL_PATH LBL_PATH
  FROM
  t408_dhr t408 ,  t408a_dhr_control_number t408a ,  t2540_donor_master t2540,
  t205_part_number t205,  t704b_account_cpc_mapping t704b,  t205m_part_label_parameter t205m,
  t205d_part_attribute t205d,
  t901_code_lookup t901,
  t7040_account_cpc_label_mapping t7040
  
  WHERE
  t2540.c2540_donor_id            = t408.c2540_donor_id
AND t408a.c408a_conrol_number     =p_control_num
AND t408a.c205_part_number_id     =p_part_id
AND t408.c408_void_fl            IS NULL
AND t7040.c704b_account_cpc_map_id=t704b.c704b_account_cpc_map_id
AND t7040.c7040_void_fl IS NULL
AND t205d.C901_ATTRIBUTE_TYPE='103728'  --Storage Temperature
AND t205d.c205_part_number_id=t205.c205_part_number_id
AND t205d.c205d_void_fl IS NULL
AND t205m.c205_part_number_id=t205d.c205_part_number_id
AND t901.c901_code_id=t205d.c205d_attribute_value
AND t2540.c2540_void_fl          IS NULL
AND t408.c408_status_fl          != 4
AND t408a.c408_dhr_id(+)          =t408.c408_dhr_id
AND t704b.c704b_account_cpc_map_id=t205m.c901_contract_proc_client
AND t205m.c2550_void_fl          IS NULL
AND t205m.c205_part_number_id     =t205.c205_part_number_id
AND t205m.c205_part_number_id     =t408.c205_part_number_id
AND t704b.c704b_void_fl          IS NULL
UNION
SELECT
  NVL(t413.c205_client_part_number_id, t413.c205_part_number_id) Part_Number,  t413.c413_control_number Control_Number ,
  t2550.c2550_packing_slip loadrunno ,  t2540.c2540_donor_number donorno ,  t2550.c2550_expiry_date ,
  t2550.c2550_last_updated_date entereddate ,  t2550.c2550_last_updated_date receiveddate,  t413.c412_inhouse_trans_id ,
  t205m.C205M_SIZE lblsize,  t205m.c205m_generic_spec GENERICSPEC ,  t205.c205_part_num_desc Part_Desc,
  t205m.c205m_patient_label,  t205m.c205m_box_label BOXLABEL,  t205m.c205m_package_label,
  t205m.c205m_size,  t205m.c205m_sample,  t704b.c704b_cpc_barcode_lbl_ver PATIENTLABEL,
  t704b.c704b_cpc_final_cpc_ver FINALCPCVER,  t704b.c704b_cpc_name CPCNAME,  t704b.c704b_cpc_address CPCADDRESS ,NVL(t7040.c7040_PRINT_QTY,1) Quantity,
  t901.C901_CODE_NM STORAGETEMP,T704B.C704B_BOX_LOGO CPCBARCODELOGO,T704B.C704B_FINAL_PACKAGING_LOGO CPCFINALLOGO
  ,t704b.C704B_ACCOUNT_CPC_MAP_ID CPCID,t7040.c7040_LABEL_PATH LBL_PATH
FROM
  t412_inhouse_transactions t412 ,
  t413_inhouse_trans_items t413,
  t2540_donor_master t2540 ,
  t2550_part_control_number t2550,
  t205_part_number t205,
  t704b_account_cpc_mapping t704b,
  t205m_part_label_parameter t205m,
  t205d_part_attribute t205d,
  t901_code_lookup t901,
  t7040_account_cpc_label_mapping t7040
WHERE
  t412.c412_inhouse_trans_id         = t413.c412_inhouse_trans_id
AND t412.c412_status_fl             != '4'      --not verified
AND t412.c412_type                   = '103932' -- ptrd
AND t205d.C901_ATTRIBUTE_TYPE='103728'  --Storage Temperature
AND t205d.c205_part_number_id=t205.c205_part_number_id
AND t205d.c205d_void_fl IS NULL
AND t7040.c704b_account_cpc_map_id=t704b.c704b_account_cpc_map_id
AND t7040.c7040_void_fl IS NULL
AND t205d.c205_part_number_id=t2550.c205_part_number_id
AND t205m.c205_part_number_id=t205d.c205_part_number_id
AND t901.c901_code_id=t205d.c205d_attribute_value
AND t412.c412_void_fl               IS NULL
AND t413.c413_void_fl               IS NULL
AND t413.c205_client_part_number_id IS NOT NULL
AND t2540.c2540_donor_id             = t2550.c2540_donor_id
AND t2550.c205_part_number_id        = t413.c205_part_number_id
AND (t413.c205_part_number_id        = p_part_id OR
t413.c205_client_part_number_id=p_part_id
)
AND t2550.c2550_control_number       =p_control_num
AND t2550.c2550_control_number       = t413.c413_control_number
AND t205.c205_part_number_id         =t413.c205_part_number_id
AND t704b.c704b_account_cpc_map_id   =t205m.c901_contract_proc_client
AND t205m.c205_part_number_id        =t205.c205_part_number_id
AND t2540.c2540_void_fl             IS NULL

UNION

SELECT
  t2550.c205_part_number_id Part_Number,  t2550.c2550_control_number Control_Number,  t2550.c2550_packing_slip,
  t2540.c2540_donor_number donorno,  t2550.c2550_expiry_date exp_date,  t2550.c2550_last_updated_date createddate,
  t2550.c2550_last_updated_date receiveddate,  t2550.c2550_ref_id dhrid,  t205m.C205M_SIZE lblsize,
  t205m.c205m_generic_spec GENERICSPEC ,  t205.c205_part_num_desc Part_Desc,  t205m.c205m_patient_label,
  t205m.c205m_box_label BOXLABEL,  t205m.c205m_package_label,  t205m.c205m_size,
  t205m.c205m_sample,  t704b.c704b_cpc_barcode_lbl_ver PATIENTLABEL,  t704b.c704b_cpc_final_cpc_ver FINALCPCVER,
  t704b.c704b_cpc_name CPCNAME,  t704b.c704b_cpc_address CPCADDRESS ,NVL(t7040.c7040_PRINT_QTY,1) Quantity, t901.C901_CODE_NM STORAGETEMP,
  T704B.C704B_BOX_LOGO CPCBARCODELOGO,T704B.C704B_FINAL_PACKAGING_LOGO CPCFINALLOGO,t704b.C704B_ACCOUNT_CPC_MAP_ID CPCID,t7040.c7040_LABEL_PATH LBL_PATH
FROM
  t2540_donor_master t2540,
  t205_part_number t205,
  t205d_part_attribute t205d,
  t704b_account_cpc_mapping t704b,
  t205m_part_label_parameter t205m,
  t2550_part_control_number t2550,
  t901_code_lookup t901,
  t7040_account_cpc_label_mapping t7040
WHERE
  t2540.c2540_donor_id          = t2550.c2540_donor_id
AND t205d.C901_ATTRIBUTE_TYPE='103728'  --Storage Temperature
AND t205d.c205_part_number_id=t205.c205_part_number_id
AND t205d.c205d_void_fl IS NULL
AND t205d.c205_part_number_id=t2550.c205_part_number_id
AND t205m.c205_part_number_id=t205d.c205_part_number_id
AND t7040.c704b_account_cpc_map_id=t704b.c704b_account_cpc_map_id
AND t7040.c7040_void_fl IS NULL
AND t901.c901_code_id=t205d.c205d_attribute_value
AND t2550.c2550_control_number  =p_control_num
AND t2550.c205_part_number_id   =p_part_id
AND t205m.c205_part_number_id   =t2550.c205_part_number_id
AND t2550.c205_part_number_id   =t205.c205_part_number_id
AND t2540.c2540_void_fl        IS NULL
AND t2550.c2550_lot_status NOT IN (105006,105000,105007)
AND t2550.c2550_control_number NOT IN (
SELECT t413.c413_control_number
               FROM t412_inhouse_transactions t412 ,
                    t413_inhouse_trans_items t413
              WHERE t412.c412_inhouse_trans_id         = t413.c412_inhouse_trans_id
                AND t412.c412_status_fl              != '4'      --Not VERIFIED
                AND t412.c412_type                   = '103932' -- PTRD
                AND t412.c412_void_fl               IS NULL
                AND t413.c413_void_fl               IS NULL
                AND t413.c205_client_part_number_id IS NOT NULL
)
AND t704b.c704b_account_cpc_map_id=t205m.c901_contract_proc_client
AND t205m.c2550_void_fl          IS NULL
AND t205m.c205_part_number_id     =t205.c205_part_number_id
AND t704b.c704b_void_fl          IS NULL
 ORDER BY 
    donorno,Part_Number ,Control_Number  ASC;
 
 END gm_fetch_part_details;
 
 
 /************************************************************************
    * Description : 
    *
    * Author      : Gomathi Palani
************************************************************************/

PROCEDURE gm_fetch_part_label_path (
p_part_id   	IN  VARCHAR2,
p_out_dtl_cur	OUT  TYPES.cursor_type
)
IS
BEGIN
	  -- get comma delimated values to store in v_in_list
	  my_context.set_my_inlist_ctx (p_part_id) ;
 OPEN p_out_dtl_cur
        FOR
           SELECT
			  t205d.c205_part_number_id part_number,
			  t7040.c901_stock_type stock_type,
			  t7040.c7040_label_path label_path,
			  t205l.c205l_stock_name stock_name,
			  t205l.c205l_stock_desc stock_desc,
			  t205l.c205l_stock_length stock_length,
			  t205l.c205l_stock_width stock_width
			 
			FROM
			  t205d_part_attribute t205d ,
			  t704b_account_cpc_mapping t704b,
			  t205l_printer_stock t205l,
			  T7040_ACCOUNT_CPC_LABEL_MAPPING t7040
			WHERE
			  t205d.c205_part_number_id IN
			  (
			    SELECT
			      token
			    FROM
			      v_in_list
			  )
				AND t7040.c704b_account_cpc_map_id =t704b.c704b_account_cpc_map_id
				AND t205d.c205d_attribute_value    =t704b.c704b_account_cpc_map_id
				AND t205d.c205d_void_fl           IS NULL
				AND t7040.c901_stock_type          = t205l.c901_stock_type (+) -- get all the
				  -- records from attribute table for the table
				AND t205d.c901_attribute_type IN ('103731')
				AND t7040.c901_stock_type     IN
  			  (SELECT   c901_code_id  FROM   t901_code_lookup  WHERE c901_code_grp = 'STOCK' -- this filters the assigned stocks with label
    			-- path for the part
    			UNION ALL
    			SELECT    0  FROM    dual ); 
      
END gm_fetch_part_label_path;

 

END gm_pkg_op_bba_label_print;
/