/* Formatted on 2011/12/19 12:45 (Formatter Plus v4.8.0) */
--@"C:\database\packages\operations\gm_pkg_op_insert_txn.bdy";
CREATE OR REPLACE
PACKAGE BODY gm_pkg_op_sav_insert_txn
IS

/***********************************************************
  * Description : Procedure to save insert.
  * Author   	: karthik
  ************************************************************/
PROCEDURE gm_sav_insert_txn_dtl
  (
    p_str	   IN CLOB,
    p_reftype  IN T5056_TRANSACTION_INSERT.C901_REF_TYPE%TYPE,
    p_status   IN T5056_TRANSACTION_INSERT.C901_STATUS%TYPE,
    p_userid   IN t5055_control_number.c5055_last_updated_by%TYPE,
    P_screen   IN VARCHAR2
   )
 AS
   	 v_string 		VARCHAR2(4000):= p_str||',';
   	 v_txn_id   	t520_request.c520_request_id%TYPE;
   BEGIN
    
	   /* If the request is coming from Pending Item Request Screen,then
	    * need to split the txn id and call the below procedure
	    */
	   IF v_string IS NOT NULL THEN
		  WHILE INSTR (v_string, ',') <> 0
			  LOOP
			   v_txn_id := SUBSTR (v_string, 1, INSTR (v_string, ',') - 1);
		       v_string	:= SUBSTR (v_string, INSTR (v_string, ',') + 1);
			   gm_pkg_op_sav_insert_txn.gm_sav_insert_rev_detail(v_txn_id,p_reftype,p_status,p_userid,P_screen);
			  END LOOP;
	   END IF;
	 
 END gm_sav_insert_txn_dtl;
  /***********************************************************
  * Description : Procedure to save insert details for the txn.
  * Author   	: karthik
  ************************************************************/
PROCEDURE gm_sav_insert_rev_detail
  (
    p_refid    IN T5056_TRANSACTION_INSERT.C5056_REF_ID%TYPE,
    p_reftype  IN T5056_TRANSACTION_INSERT.C901_REF_TYPE%TYPE,
    p_status   IN T5056_TRANSACTION_INSERT.C901_STATUS%TYPE,
    p_userid   IN t5055_control_number.c5055_last_updated_by%TYPE,
    P_screen   IN VARCHAR2
   )
AS
  v_plant_id      t5040_plant_master.c5040_plant_id%TYPE;
  v_company_id	  t1900_company.c1900_company_id%TYPE;
  v_flag 		  VARCHAR2(1);
  v_pnums		  CLOB;
  v_insert_id	  T205J_PART_INSERT.C205_INSERT_ID%TYPE;
  v_rev_num		  T205J_PART_INSERT.C205J_REV_NUM%TYPE;
  v_location_id	  T5056_TRANSACTION_INSERT.C5052_LOCATION_ID%TYPE;
  v_txn_count	  NUMBER;
  v_is_needed_fl  VARCHAR2(10);
  v_txn_id		  T5056_TRANSACTION_INSERT.C5056_REF_ID%TYPE;
  v_txn_plant_id   t5040_plant_master.c5040_plant_id%TYPE;
  
CURSOR v_insert_details
	IS 
	  SELECT T205J.C205_INSERT_ID insertid , T205J.C205J_REV_NUM revnum , txn.refid txnid ,txn.plantid txnplantid
		FROM T205J_PART_INSERT T205J
		   , ( SELECT T502.C205_PART_NUMBER_ID ,t501.C501_ORDER_ID refid ,t501.c5040_plant_id plantid
		    FROM T501_ORDER t501 ,    
		      T502_ITEM_ORDER t502   
		    WHERE T501.C501_ORDER_ID = T502.C501_ORDER_ID
		    AND (T501.C501_ORDER_ID   = p_refid OR t501.c501_parent_order_id = p_refid)
		    AND T501.C501_VOID_FL   IS NULL
		    AND t502.C502_VOID_FL   IS NULL
		    AND t502.c901_type <> 50301 --Loaner parts
		    AND t502.c205_part_number_id NOT IN (SELECT T205K.c205_part_number_id 
		                                           FROM T205K_PART_INSERT_EXCLN T205K
		                                          WHERE t205k.c1900_company_id = t501.c1900_company_id
		                                            AND t205k.c205_part_number_id = t502.c205_part_number_id) 
		    UNION ALL
		    SELECT t505.C205_PART_NUMBER_ID ,t504.C504_CONSIGNMENT_ID refid ,t504.c5040_plant_id plantid
		    FROM T504_CONSIGNMENT t504 ,
		      T505_ITEM_CONSIGNMENT T505
		    WHERE T504.C504_CONSIGNMENT_ID = T505.C504_CONSIGNMENT_ID
		    AND (T504.C504_CONSIGNMENT_ID   = p_refid OR t504.c504_master_consignment_id = p_refid)
		    AND T504.C504_VOID_FL         IS NULL
		    AND T505.C505_VOID_FL         IS NULL
		    AND T505.c205_part_number_id NOT IN (SELECT T205K.c205_part_number_id 
		                                           FROM T205K_PART_INSERT_EXCLN T205K
		                                          WHERE t205k.c1900_company_id = t504.c1900_company_id
		                                            AND t205k.c205_part_number_id = T505.c205_part_number_id) 
		    UNION ALL
		    SELECT T413.C205_PART_NUMBER_ID ,t412.c412_inhouse_trans_id refid ,t412.c5040_plant_id plantid
		    FROM T412_INHOUSE_TRANSACTIONS T412 ,
		      T413_INHOUSE_TRANS_ITEMS T413
		    WHERE T412.C412_INHOUSE_TRANS_ID = T413.C412_INHOUSE_TRANS_ID
		    AND T412.C412_INHOUSE_TRANS_ID   = p_refid
		    AND T412.C412_VOID_FL           IS NULL
		    AND T413.C413_VOID_FL           IS NULL
		    AND T413.c205_part_number_id NOT IN (SELECT T205K.c205_part_number_id 
		                                           FROM T205K_PART_INSERT_EXCLN T205K
		                                          WHERE t205k.c1900_company_id = T412.c1900_company_id
		                                            AND t205k.c205_part_number_id = T413.c205_part_number_id) 
		    ) txn
		WHERE T205J.C205_PART_NUMBER_ID = txn.C205_PART_NUMBER_ID
		AND T205J.C205J_VOID_FL IS NULL
		GROUP BY T205J.C205_INSERT_ID , T205J.C205J_REV_NUM ,txn.refid ,txn.plantid;
		
BEGIN
  --
	   SELECT  get_compid_frm_cntx()  INTO v_company_id FROM DUAL;
	   SELECT  get_plantid_frm_cntx() INTO v_plant_id FROM DUAL;
  --
  	   --Validating the Plant is Enabled for INSERT
   	   SELECT get_rule_value(v_plant_id,'INSERT_PART') INTO v_flag FROM DUAL; 
	   
	   IF v_flag IS NULL OR v_flag <> 'Y' 
	   THEN
		   RETURN;
	   END IF;
	    
	   --validating the Transaction Type is Enabled for INSERT	  	
	   SELECT get_rule_value(p_reftype,'INSERT_TRANS') INTO v_flag FROM DUAL; 	  
	   
	   IF v_flag IS NULL OR v_flag <> 'Y' 
	   THEN
		   RETURN;
	   END IF;
	   /*Update the void flag is null for the TXN*/
     
      UPDATE T5056_TRANSACTION_INSERT 
          SET C5056_VOID_FL = 'Y'
            , C5056_LAST_UPDATED_BY = p_userid
            , c5056_last_updated_date = CURRENT_DATE
        WHERE C5056_REF_ID = p_refid
        AND C5056_INSERT_MASTER_FL IS NULL;

       /* Procedure to save the details in T5056_TRANSACTION_INSERT table*/
	     FOR v_index IN v_insert_details
			LOOP
				  v_insert_id 		   := v_index.insertid;
				  v_rev_num 		   := v_index.revnum;
				  v_txn_id	 		   := v_index.txnid;
				  v_txn_plant_id	   := v_index.txnplantid;	
				  v_location_id		   := NULL;
				  
			   SELECT get_rule_value(v_txn_plant_id,'INSERT_PART') INTO v_flag FROM DUAL; 
	   
			   IF v_flag = 'Y' 
			   THEN
				  --Validate the insert before inserting in T5056
			  	  gm_pkg_op_sav_insert_txn.gm_validate_insert(v_txn_id, p_reftype, v_insert_id,v_is_needed_fl); 
			  
				  IF v_is_needed_fl = 'Y' THEN
			      	gm_pkg_op_sav_insert_txn.gm_save_insert_trans(v_txn_id,p_reftype,v_insert_id,v_rev_num,p_status,v_location_id,p_userid,NULL);
			      END IF;
			   END IF;
			   
			 
		END LOOP;
      	
END gm_sav_insert_rev_detail;

/********************************************************************************
  * Description : Procedure to Validate the insert before inserting in T5056.
  * Author   	: karthik
  *******************************************************************************/
PROCEDURE gm_validate_insert
  (
    p_refid          IN  T5056_TRANSACTION_INSERT.C5056_REF_ID%TYPE,
    p_reftype        IN  T5056_TRANSACTION_INSERT.C901_REF_TYPE%TYPE,
    p_insert         IN  T5056_TRANSACTION_INSERT.C205_INSERT_ID%TYPE,
    p_is_needed_fl   OUT VARCHAR2
   )
AS
   v_is_needed_fl 	VARCHAR2(10):= 'Y';
   v_count			NUMBER;
   v_ship_to_id		T907_SHIPPING_INFO.C907_SHIP_TO_ID%TYPE;
   v_exclude_dist 	VARCHAR2(10);
   v_company_id		t1900_company.c1900_company_id%TYPE;
   v_insert_index	VARCHAR2(10);
BEGIN
 
     -- Select the ship to of the transaction and if it is  distributor Globus Medical Japan ( 2345 )then mark p_is_needed_fl := 'N'
         BEGIN
	        SELECT C907_SHIP_TO_ID
	        	INTO v_ship_to_id
			 FROM T907_SHIPPING_INFO
			WHERE C907_REF_ID = p_refid
			AND C907_VOID_FL  IS NULL;
	     EXCEPTION
			WHEN NO_DATA_FOUND THEN
		    v_ship_to_id := NULL;
		END;
		
		SELECT get_rule_value(v_ship_to_id,'INST_DIST_EXCLUSION') 
			INTO v_exclude_dist 
			FROM DUAL;
			
		IF v_exclude_dist = 'Y' THEN
			v_is_needed_fl := 'N';
		END IF;
	   /*    
      *if the part has two inserts and If we are shipping to OUS and then only the part ends with B need to be shipped 
        *(For E.g., DI170A, DI170B then for DI170A mark p_is_needed_fl := 'N' )
      *If we are shipping to US and if the part has two inserts then only the part ends with A need to be shipped
        *(For E.g., DI170A, DI170B then for DI170B mark p_is_needed_fl := 'N' )
      */
     SELECT COUNT(1)
	   INTO v_count
		FROM
		  ( SELECT DISTINCT t205j.c205_insert_id
		  FROM t205j_part_insert t205J
		  WHERE t205j.c205_part_number_id IN
		    ( SELECT DISTINCT t205j.c205_part_number_id
		    FROM t205j_part_insert t205J ,
		      T502_ITEM_ORDER T502,
		      T501_ORDER t501
		    WHERE t205j.c205_insert_id   = p_insert
		    AND t502.c205_part_number_id = t205j.c205_part_number_id
		    AND T502.C501_ORDER_ID       = P_REFID
		    AND T501.C501_ORDER_ID       = T502.C501_ORDER_ID
		    AND T501.C501_VOID_FL       IS NULL
		    AND t502.C502_VOID_FL       IS NULL
		    UNION ALL
		    SELECT DISTINCT T205J.C205_PART_NUMBER_ID
		    FROM t205j_part_insert t205J ,
		      T504_CONSIGNMENT t504 ,
		      T505_ITEM_CONSIGNMENT T505
		    WHERE T205J.C205_INSERT_ID   = P_INSERT
		    AND T505.C205_PART_NUMBER_ID = T205J.C205_PART_NUMBER_ID
		    AND T504.C504_CONSIGNMENT_ID = p_refid
		    AND T504.C504_CONSIGNMENT_ID = T505.C504_CONSIGNMENT_ID
		    AND T504.C504_VOID_FL       IS NULL
		    AND T505.C505_VOID_FL       IS NULL
		    UNION ALL
		    SELECT DISTINCT T205J.C205_PART_NUMBER_ID
		    FROM t205j_part_insert t205J ,
		      T412_INHOUSE_TRANSACTIONS T412 ,
		      T413_INHOUSE_TRANS_ITEMS T413
		    WHERE T205J.C205_INSERT_ID     = P_INSERT
		    AND T413.C205_PART_NUMBER_ID   = T205J.C205_PART_NUMBER_ID
		    AND T412.C412_INHOUSE_TRANS_ID = p_refid
		    AND T412.C412_INHOUSE_TRANS_ID = T413.C412_INHOUSE_TRANS_ID
		    AND T412.C412_VOID_FL         IS NULL
		    AND T413.C413_VOID_FL         IS NULL
		    )
		  AND C205J_VOID_FL IS NULL
		  );
		
		IF v_count > 1 THEN 
	        	v_company_id := gm_pkg_op_fch_insert_details.get_ship_to_division_company(p_refid);
	        	
	        	SELECT SUBSTR(p_insert, -1) 
	        	 INTO v_insert_index 
	        	FROM DUAL;
				--100823:= US & 100824 := OUS
	        	IF v_insert_index = 'A' AND v_company_id ='100824' THEN
	        		v_is_needed_fl := 'N';
	        	END IF;
	        	
	        	IF v_insert_index = 'B' AND v_company_id ='100823' THEN
	        		v_is_needed_fl := 'N';
	        	END IF;
	        	/* If the Consignments from OUS restock [GM_INTL_Sales_Replenishment_Job ] , then the part ends with B need to be shipped*/
	        	IF v_insert_index = 'A' AND v_company_id IS NULL THEN
	        		v_is_needed_fl := 'N';
	        	END IF;
        END IF;
        p_is_needed_fl := v_is_needed_fl; 
END gm_validate_insert;

/********************************************************************************
  * Description : Procedure to Validate the Trasnaction while picking the Insert.
  * Author   	: karthik
  *******************************************************************************/
PROCEDURE gm_validate_pick_insert
  (
    p_refid  IN  T5056_TRANSACTION_INSERT.C5056_REF_ID%TYPE
   )
AS
v_status_fl VARCHAR2(10);
v_void_fl	VARCHAR2(10);
BEGIN
 	 SELECT DECODE(C907_STATUS_FL , '15' , 'Y' , 'N') --Pending Control Number 
  		, C907_VOID_FL
	  	INTO v_status_fl ,
	  	     v_void_fl
	  FROM T907_SHIPPING_INFO T907
	  WHERE T907.C907_REF_ID = TRIM(p_refid)
	  AND T907.C907_VOID_FL IS NULL;
	  
	  IF v_status_fl = 'N' OR v_void_fl = 'Y' THEN
	  	RETURN;
	  END IF;
	
END gm_validate_pick_insert;

/*****************************************************************************************************************************
  * Description : Procedure to save insert details for the txn while selecting the check box in Process Transaction screen.
  * Author   	: karthik
  ****************************************************************************************************************************/
PROCEDURE gm_upd_insert_trans_details
  (
    p_refid     IN T5056_TRANSACTION_INSERT.C5056_REF_ID%TYPE,
    p_reftype   IN T5056_TRANSACTION_INSERT.C901_REF_TYPE%TYPE,
    p_insert    IN T5056_TRANSACTION_INSERT.C205_INSERT_ID%TYPE,
    p_revnum    IN T5056_TRANSACTION_INSERT.C5056_REV_NUM%TYPE,
    p_location  IN T5056_TRANSACTION_INSERT.C5052_LOCATION_ID%TYPE,
    p_userid    IN t5055_control_number.c5055_last_updated_by%TYPE,
    p_chk_flag 	IN VARCHAR2
   )
AS
  v_plant_id      t5040_plant_master.c5040_plant_id%TYPE;
  v_company_id	  t1900_company.c1900_company_id%TYPE;
  v_flag 		  VARCHAR2(1);
  v_status		  T5056_TRANSACTION_INSERT.C901_STATUS%TYPE;
  v_loc_flag 	  VARCHAR2(1);
  v_txn_type	  T5056_TRANSACTION_INSERT.C901_REF_TYPE%TYPE;
  v_rule_group 	  T906_RULES.C906_RULE_GRP_ID%TYPE;

BEGIN
  --
	   SELECT  get_compid_frm_cntx()  INTO v_company_id FROM DUAL;
	   SELECT  get_plantid_frm_cntx() INTO v_plant_id FROM DUAL;
  --
  	    --Validating the Plant is Enabled for INSERT
   	   SELECT get_rule_value(v_plant_id,'INSERT_PART') INTO v_flag FROM DUAL; 
	   
	   IF v_flag IS NULL OR v_flag <> 'Y' 
	   THEN
		   RETURN;
	   END IF;
	   --validating the Transaction Type is Enabled for INSERT	  	
	   SELECT get_rule_value(p_reftype,'INSERT_TRANS') INTO v_flag FROM DUAL; 	  
	   
	   IF v_flag IS NULL OR v_flag <> 'Y' 
	   THEN
		   RETURN;
	   END IF;
	
	/*Validate the Transaction Status and Void flag
	 *If the Transaction is not in Pending Control status OR the Order is voided then RETURN from this procedure
	 */
	   gm_validate_pick_insert(p_refid);	  
	   
	   SELECT DECODE(p_chk_flag ,'true','93343','93342') --93343:= Pick Transacted & 93342:= Pick Initiated
	  	 INTO v_status
	   FROM DUAL;
	   
	   /* Procedure to save the details in T5056_TRANSACTION_INSERT table*/
	   gm_pkg_op_sav_insert_txn.gm_upd_insert_current_status(p_refid,p_reftype,p_insert,p_revnum,v_status,p_location,p_userid);
      	
END gm_upd_insert_trans_details;
  /***********************************************************
  * Description : common Procedure to save insert in T5056 table
  * Author   	: karthik
  ************************************************************/
PROCEDURE gm_save_insert_trans
  (
    p_refid    		IN T5056_TRANSACTION_INSERT.C5056_REF_ID%TYPE,
    p_reftype  		IN T5056_TRANSACTION_INSERT.C901_REF_TYPE%TYPE,
    p_insert_id 	IN T5056_TRANSACTION_INSERT.C205_INSERT_ID%TYPE,
    p_rev_num  		IN T5056_TRANSACTION_INSERT.C5056_REV_NUM%TYPE,
    p_status   		IN T5056_TRANSACTION_INSERT.C901_STATUS%TYPE,
    p_location_id   IN T5056_TRANSACTION_INSERT.C5052_LOCATION_ID%TYPE,
    p_userid 		IN T5056_TRANSACTION_INSERT.C5056_LAST_UPDATED_BY%TYPE,
    p_master_screen IN T5056_TRANSACTION_INSERT.C5056_INSERT_MASTER_FL%TYPE DEFAULT NULL
   )
 AS
 	v_location_id		T5056_TRANSACTION_INSERT.C5052_LOCATION_ID%TYPE;
 BEGIN
	 
	 
	 -- Get location id from location cd
		v_location_id:= gm_pkg_op_fch_insert_details.get_location_id_from_cd(p_location_id);
				INSERT INTO T5056_TRANSACTION_INSERT
				           (
				            C5056_TRANSACTION_INSERT_ID
				          , C5056_REF_ID 
				          , C901_REF_TYPE 
				          , C205_INSERT_ID
				          , C5056_REV_NUM
				          , C901_STATUS 
				          , C5052_LOCATION_ID 
				          , C5056_VOID_FL
				          , C5056_LAST_UPDATED_BY
				          , C5056_LAST_UPDATED_DATE
				          , C5056_INSERT_MASTER_FL
				          )
                     VALUES(
                     	   S5056_TRANSACTION_INSERT.nextval
                          , p_refid 
                          , p_reftype 
                          , p_insert_id 
                          , p_rev_num
                          , p_status 
                          , v_location_id
                          , NULL
                          , p_userid
                          , CURRENT_DATE
                          , p_master_screen
                          );					

	
 END gm_save_insert_trans;
 
 /***********************************************************
  * Description : Procedure to update insert current status
  * Author   	: karthik
  ************************************************************/
PROCEDURE gm_upd_insert_current_status
  (
    p_refid    		IN T5056_TRANSACTION_INSERT.C5056_REF_ID%TYPE,
    p_reftype  		IN T5056_TRANSACTION_INSERT.C901_REF_TYPE%TYPE,
    p_insert_id 	IN T5056_TRANSACTION_INSERT.C205_INSERT_ID%TYPE,
    p_rev_num  		IN T5056_TRANSACTION_INSERT.C5056_REV_NUM%TYPE,
    p_status   		IN T5056_TRANSACTION_INSERT.C901_STATUS%TYPE,
    p_location_id   IN T5056_TRANSACTION_INSERT.C5052_LOCATION_ID%TYPE,
    p_userid 		IN T5056_TRANSACTION_INSERT.C5056_LAST_UPDATED_BY%TYPE
   )
 AS
 	v_location_id    t5056_transaction_insert.c5052_location_id%TYPE;
 BEGIN
	 -- Get location id from location cd
	 v_location_id := gm_pkg_op_fch_insert_details.get_location_id_from_cd(p_location_id);
	 
		 UPDATE T5056_TRANSACTION_INSERT SET 
		 			C901_STATUS 		   = p_status
			     , C5052_LOCATION_ID 	   = v_location_id
			     , C5056_LAST_UPDATED_BY   = p_userid
			     , C5056_LAST_UPDATED_DATE = CURRENT_DATE
			 WHERE C5056_REF_ID = p_refid
			 AND C205_INSERT_ID = p_insert_id
			 AND NVL(C5056_REV_NUM,'-9999') = NVL(p_rev_num,'-9999')
			 AND C5056_VOID_FL IS NULL;

 END gm_upd_insert_current_status;
 
 /***********************************************************
  * Description : Procedure to void the insert details for the txn.
  * Author   	: karthik
  ************************************************************/
PROCEDURE gm_void_insert_details
  (
    p_refid     IN T5056_TRANSACTION_INSERT.C5056_REF_ID%TYPE,
    p_reftype  	IN T5056_TRANSACTION_INSERT.C901_REF_TYPE%TYPE,
    p_userid    IN t5055_control_number.c5055_last_updated_by%TYPE
   )
AS
  v_plant_id      t5040_plant_master.c5040_plant_id%TYPE;
  v_company_id	  t1900_company.c1900_company_id%TYPE;
  v_flag 		  VARCHAR2(1);
BEGIN
  --
  	   SELECT  get_compid_frm_cntx()  INTO v_company_id FROM DUAL;
  	   SELECT  get_plantid_frm_cntx() INTO v_plant_id FROM DUAL;
  --
  	  --Validating the Plant is Enabled for INSERT
   	   SELECT get_rule_value(v_plant_id,'INSERT_PART') INTO v_flag FROM DUAL; 
	   
	   IF v_flag IS NULL OR v_flag <> 'Y' 
	   THEN
		   RETURN;
	   END IF;
	    
	   --validating the Transaction Type is Enabled for INSERT	  	
	   SELECT get_rule_value(p_reftype,'INSERT_TRANS') INTO v_flag FROM DUAL; 	  

	   IF v_flag IS NULL OR v_flag <> 'Y' 
	   THEN
		   RETURN;
	   END IF;
     
        UPDATE T5056_TRANSACTION_INSERT SET
		C5056_VOID_FL 			= 'Y',
		C5056_LAST_UPDATED_BY   = P_USERID,
		C5056_LAST_UPDATED_DATE = CURRENT_DATE
		WHERE C5056_REF_ID 	= p_refid
		AND C5056_VOID_FL IS NULL;
		      	
END gm_void_insert_details;

/***************************************************************************************************
  * Description : Procedure to update status of the Insert for the txn while shipout the Transaction
  * Author   	: karthik
  **************************************************************************************************/
PROCEDURE gm_upd_insert_ship_status
  (
    p_refid    		IN T5056_TRANSACTION_INSERT.C5056_REF_ID%TYPE,
    p_status   		IN T5056_TRANSACTION_INSERT.C901_STATUS%TYPE,
    p_userid 		IN T5056_TRANSACTION_INSERT.C5056_LAST_UPDATED_BY%TYPE
   )
 AS
  v_plant_id      t5040_plant_master.c5040_plant_id%TYPE;
  v_company_id	  t1900_company.c1900_company_id%TYPE;
  v_flag 		  VARCHAR2(1);
  v_cnt			  NUMBER;			
BEGIN
  --
  	   SELECT  get_compid_frm_cntx()  INTO v_company_id FROM DUAL;
  	   SELECT  get_plantid_frm_cntx() INTO v_plant_id FROM DUAL;
  --
  	  --Validating the Plant is Enabled for INSERT
   	   SELECT get_rule_value(v_plant_id,'INSERT_PART') INTO v_flag FROM DUAL; 
	   
	   IF v_flag IS NULL OR v_flag <> 'Y' 
	   THEN
		   RETURN;
	   END IF;

	   UPDATE T5056_TRANSACTION_INSERT SET 
	 		C901_STATUS = p_status
		    ,C5056_LAST_UPDATED_BY = p_userid
		    ,C5056_LAST_UPDATED_DATE = CURRENT_DATE
		 WHERE C5056_REF_ID = p_refid
		 AND C5056_VOID_FL IS NULL;
		 
 END gm_upd_insert_ship_status;
 
 /***************************************************************************************************
  * Description : Procedure is used to add/edit the insert details in Insert Master Setup Screen
  * Author   	: karthik
  **************************************************************************************************/
 PROCEDURE gm_sav_edit_insert_details (
 		p_ref_id	 IN   T5056_TRANSACTION_INSERT.C5056_REF_ID%TYPE
	  , p_inputstr	 IN   VARCHAR2
	  , p_row_ids	 IN	 VARCHAR2
	  , p_userid	 IN   t5052_location_master.c5052_created_by%TYPE
	)
	AS
		v_string	   CLOB := p_inputstr;
		v_substring    VARCHAR2 (30000);
		v_inputstr	   VARCHAR2 (30000):= p_inputstr;
		v_row_data	   VARCHAR2 (30000);
		v_txn_id  	   VARCHAR2 (30000);
		v_insertid	   VARCHAR2 (30000);
		v_revnum	   VARCHAR2 (30000);
		v_location	   VARCHAR2 (30000);
		v_location_id	t5052_location_master.c5052_location_id%TYPE;
		v_ref_type	   T5056_TRANSACTION_INSERT.C901_REF_TYPE%TYPE;
		v_id		   T5056_TRANSACTION_INSERT.C5056_TRANSACTION_INSERT_ID%TYPE;
	BEGIN
		my_context.set_my_inlist_ctx (p_row_ids);

		--if we remove any records in master screen then updating the void flag for the Transaction
		IF p_row_ids IS NOT NULL THEN
			  UPDATE T5056_TRANSACTION_INSERT
			  SET C5056_VOID_FL                  = 'Y',
			    C5056_LAST_UPDATED_BY            = P_USERID,
			    C5056_LAST_UPDATED_DATE          = CURRENT_DATE
			  WHERE C5056_TRANSACTION_INSERT_ID IN
			    (SELECT token FROM v_in_list
			    )
			  AND C5056_VOID_FL IS NULL;
		END IF; 
		
		 BEGIN
		   SELECT C901_REF_TYPE
		     INTO v_ref_type
		    FROM T5056_TRANSACTION_INSERT
		    WHERE C5056_REF_ID = p_ref_id
			AND C5056_VOID_FL IS NULL
      		AND ROWNUM = 1;
		   EXCEPTION 
		   		WHEN NO_DATA_FOUND THEN
		   		v_ref_type := NULL;
		END;
		

		IF INSTR (p_inputstr, '$') = 0
		THEN
			v_inputstr	:= p_inputstr || '$';
		END IF;

		IF INSTR (v_inputstr, '$') <> 0
		THEN
			v_row_data	:= SUBSTR (v_inputstr, 1, INSTR (v_inputstr, '$') - 1);
			v_inputstr	:= SUBSTR (v_inputstr, INSTR (v_inputstr, '$') + 1);
			v_string	:= v_row_data;

			/* Loop the Delminted String and reterive the values */
			WHILE INSTR (v_string, '|') <> 0
			LOOP
				v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
				v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
				v_id		:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_insertid	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_revnum	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_location	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);

				-- Get location id from location cd
				v_location_id:= gm_pkg_op_fch_insert_details.get_location_id_from_cd(v_location,'Edit');
					
				IF v_location IS NOT NULL AND v_location_id IS NULL
				THEN
					gm_raise_application_error('-20999','413',v_insertid);
				END IF;
				
				IF p_ref_id IS NOT NULL
				THEN
					IF v_id IS NULL
					THEN	 
						--Pass C5056_INSERT_MASTER_FL as 'Y' if the input is from Add/Edit insert master setup screen
						gm_pkg_op_sav_insert_txn.gm_save_insert_trans( p_ref_id, v_ref_type, v_insertid , v_revnum ,'93342'
											, v_location , p_userid, 'Y');
					ELSE
					
						UPDATE T5056_TRANSACTION_INSERT
						   SET C205_INSERT_ID		   = v_insertid        
						     , C5056_REV_NUM 		   = v_revnum
						     , C5052_LOCATION_ID 	   = v_location_id
						     , C5056_LAST_UPDATED_BY   = p_userid
						     , C5056_LAST_UPDATED_DATE = CURRENT_DATE
						 WHERE C5056_TRANSACTION_INSERT_ID = v_id
						 AND C5056_VOID_FL IS NULL;
			 
					END IF;					
				END IF;
			END LOOP;
		END IF;
	END gm_sav_edit_insert_details;
	
/******************************************************************************************************
* call the procedure from ETL GmSplitPartInsertJob
* Description : Procedure to split the insert part numbers with C205_INSERT_ISSUE_FL IS NULL for JOB.
* Author      : karthik
*****************************************************************************************************/
PROCEDURE gm_fch_and_split_insert_parts(
    p_user_id IN t5055_control_number.c5055_last_updated_by%TYPE 
    )
AS
  v_string         VARCHAR2(4000);
  v_substr         VARCHAR2(4000);
  v_insert_pnum    VARCHAR2(50);
  v_insert_rev     VARCHAR2(50);
  v_error          VARCHAR2(4000) := '';
  v_part_num_count NUMBER;
  CURSOR pnum_cursor
  IS
    SELECT DISTINCT c205_part_number_id,
      c205_insert_id
    FROM
      (SELECT t205.c205_part_number_id,
        t205.c205_insert_id,
        NVL(t2023.c1900_company_id, 1000) company_id
      FROM t205_part_number t205 ,
        T2023_PART_COMPANY_MAPPING t2023
      WHERE t205.c205_active_fl    = 'Y'
      AND t205.c205_product_class NOT IN 
      (select c906_rule_value from T906_RULES 
      where c906_rule_id='INSERT_ST_PART' AND c906_rule_grp_id='INSERT_STER' AND C906_VOID_FL IS NULL)
      AND t205.c205_part_number_id = t2023.c205_part_number_id(+)
      AND T205.C205_INSERT_ISSUE_FL IS NULL
      )
  WHERE COMPANY_ID NOT IN (SELECT c906_rule_value
                              FROM T906_RULES
                              WHERE C906_RULE_ID   ='INSERT_CMP'
                              AND C906_RULE_GRP_ID = 'INSERT_CMP_FL'
                              AND C906_VOID_FL IS NULL
                           );
  
  CURSOR insert_cursor
  IS
    SELECT TOKEN FROM V_IN_LIST;
    
BEGIN
  FOR vpnum_index IN pnum_cursor
  LOOP
    v_error     := '';
    v_string    := TRIM(vpnum_index.c205_insert_id);
    V_STRING    := REPLACE(V_STRING,' ','');
    
    UPDATE T205J_PART_INSERT SET
	   C205J_VOID_FL = 'Y',
	   C205J_LAST_UPDATED_DATE = CURRENT_DATE,
	   C205J_LAST_UPDATED_BY = P_USER_ID
	   WHERE C205_PART_NUMBER_ID = vpnum_index.C205_PART_NUMBER_ID;
           
    IF v_string IS NULL OR UPPER(v_string) = 'N/A' OR v_string = '-' THEN
     UPDATE t205_part_number
          SET c205_insert_issue_fl  = 'Y',
            c205_last_updated_by    = p_user_id ,
            C205_LAST_UPDATED_DATE  = CURRENT_DATE
          WHERE c205_part_number_id = VPNUM_INDEX.C205_PART_NUMBER_ID;
          
      CONTINUE;
    END IF;
    
    MY_CONTEXT.SET_MY_INLIST_CTX (V_STRING);
    
    FOR v_insert_index IN insert_cursor
    LOOP
    
      IF INSTR(v_insert_index.token, '(') = 0 THEN
        v_insert_pnum := v_insert_index.token;
      ELSE
        v_insert_pnum := NVL(TRIM(SUBSTR(v_insert_index.token, 0, INSTR(v_insert_index.token, '(') - 1)),'-999');
      END IF;
      
      IF ( INSTR(v_insert_index.token, '(') = -1 ) THEN
        v_error := 'Y'; --Starting Bracket for revision not found
      END IF;
      
      IF ( INSTR(v_insert_index.token, ')') = -1 ) THEN
       v_error := 'Y';  -- Ending Bracket for revision not found
      END IF;
      
      V_INSERT_REV := TRIM(REPLACE(SUBSTR(V_INSERT_INDEX.TOKEN, INSTR(V_INSERT_INDEX.TOKEN, '(') + 1) ,')',''));
      
      SELECT COUNT(1)
      	INTO v_part_num_count
      FROM t205_part_number t205
      WHERE T205.C205_PART_NUMBER_ID = V_INSERT_PNUM;
      
      IF v_part_num_count = 0 THEN
       v_error := 'Y'; -- Part Number does not exist
      END IF;
      
      IF v_insert_rev IS NULL THEN
         v_error := 'Y'; -- Insert Revision does not exist
      END IF;
      
      IF v_error IS NOT NULL THEN
          UPDATE t205_part_number
          SET c205_insert_issue_fl  = 'Y',
            c205_last_updated_by    = p_user_id ,
            C205_LAST_UPDATED_DATE  = CURRENT_DATE
          WHERE c205_part_number_id = VPNUM_INDEX.C205_PART_NUMBER_ID;
          
        CONTINUE;
      END IF;
      
       INSERT INTO T205J_PART_INSERT
          (
            C205J_PART_INSERT_ID,
            C205_PART_NUMBER_ID,
            C205_INSERT_ID,
            C205J_REV_NUM,
            C205J_VOID_FL,
            C205J_LAST_UPDATED_DATE,
            C205J_LAST_UPDATED_BY
          )
          VALUES
          (
            S205J_PART_INSERT.NEXTVAL,
            VPNUM_INDEX.C205_PART_NUMBER_ID,
            v_insert_pnum,
            V_INSERT_REV,
            NULL,
            CURRENT_DATE,
            p_user_id
          );
              
       UPDATE T205_PART_NUMBER
          SET c205_insert_issue_fl  = 'N',
            c205_last_updated_by    = p_user_id ,
            C205_LAST_UPDATED_DATE  = CURRENT_DATE
          WHERE C205_PART_NUMBER_ID = VPNUM_INDEX.C205_PART_NUMBER_ID;
          
    END LOOP;
  END LOOP;
END gm_fch_and_split_insert_parts;

/******************************************************************************************************
* Description : This procedure will iterate thro string and update inserts status in T5056_TRANSACTION_INSERT table
* Author      : Matt
*****************************************************************************************************/
PROCEDURE gm_upd_inserts_list
(
    p_refid      IN T5056_TRANSACTION_INSERT.C5056_REF_ID%TYPE 
    ,p_reftype   IN T5056_TRANSACTION_INSERT.C901_REF_TYPE%TYPE 
    ,p_inputstr  IN  CLOB 
    ,p_userid    IN  T5056_TRANSACTION_INSERT.C5056_LAST_UPDATED_BY%TYPE 
    ,p_void_flag IN  VARCHAR2
)
AS
   v_string       CLOB := p_inputstr;
   v_substring    CLOB;
   v_txn_id       t5056_transaction_insert.c5056_ref_id%TYPE;
   v_insertid     t5056_transaction_insert.c205_insert_id%TYPE;
   v_revnum       t5056_transaction_insert.c5056_rev_num%TYPE;
   v_location     t5056_transaction_insert.c5052_location_id%TYPE;
   v_ref_type     T5056_TRANSACTION_INSERT.C901_REF_TYPE%TYPE;
   v_id           T5056_TRANSACTION_INSERT.C5056_TRANSACTION_INSERT_ID%TYPE;
BEGIN
    WHILE INSTR (v_string, '|') <> 0
    LOOP
      v_substring := SUBSTR(v_string, 1, INSTR(v_string, '|') - 1);
      v_string    := SUBSTR(v_string, INSTR(v_string, '|') + 1);                    
      v_insertid  := SUBSTR(v_substring, 1, INSTR(v_substring, '^') - 1);
      v_substring := SUBSTR(v_substring, INSTR(v_substring, '^') + 1);
      v_revnum    := SUBSTR(v_substring, 1, INSTR(v_substring, '^') - 1);
      v_location  := SUBSTR(v_substring, INSTR(v_substring, '^') + 1);
     
     BEGIN 
      SELECT C5056_REV_NUM 
        INTO v_revnum 
        from t5056_transaction_insert 
       WHERE C5056_REF_ID = p_refid 
         AND C205_INSERT_ID = v_insertid 
         AND c901_status = 93342 -- Not Picked
         AND c5056_void_fl IS NULL 
         AND ROWNUM = 1;
      EXCEPTION WHEN NO_DATA_FOUND THEN
        v_revnum := -9999;
      END;
      
      gm_pkg_op_sav_insert_txn.gm_upd_insert_trans_details(p_refid, p_reftype, v_insertid, v_revnum, v_location, p_userid,  p_void_flag  );
   END LOOP;  

END gm_upd_inserts_list;


/*******************************************************
* Description : Procedure to update the pick and put location qty.
* author  : Karthik
*******************************************************/
PROCEDURE gm_sav_insert_location_qty
  (
    p_trans_id   IN T5056_TRANSACTION_INSERT.C5056_REF_ID%TYPE,
    p_trans_type IN T5056_TRANSACTION_INSERT.C901_REF_TYPE%TYPE,
    p_loc_oper   IN T5056_TRANSACTION_INSERT.C901_STATUS%TYPE,
    p_userid     IN t101_user.c101_user_id%TYPE )
AS
  v_stringtemp VARCHAR2 (4000) := '';
  v_tempclob CLOB;
  v_location       VARCHAR2 (10);
  v_inv_type       VARCHAR2(20):= NULL;
  v_warehouse_type VARCHAR2(20):= NULL;
  v_whcnt          NUMBER;
  -- pick qty and locations
  -- Below Decode is a temp fix For Device -- At device 4110 and 4112 are considered as consignment
  CURSOR cur_pick_locs
  IS
	SELECT C205_INSERT_ID PNUM,
	'1' qty,
	 C5052_LOCATION_ID loc
	FROM T5056_TRANSACTION_INSERT
	WHERE C5056_REF_ID = p_trans_id
	AND C901_STATUS    = p_loc_oper
	AND C5056_VOID_FL IS NULL;	
BEGIN

  FOR v_loc IN cur_pick_locs
  LOOP
    v_stringtemp := v_loc.pnum || ',' || v_loc.qty || ',' || v_loc.loc || '|';
    v_tempclob   := v_tempclob || v_stringtemp;
    v_stringtemp := '';
  END LOOP;
  -- pick - 93326, put - 93327 (because of device coding we need to change
  -- the number using below decode
  SELECT DECODE (p_loc_oper, 93343, 93326, 93327)
  INTO v_location
  FROM DUAL;
  -- location qty update for pick
  gm_pkg_op_sav_insert_txn.gm_upd_insert_inv_loc_qty (p_trans_id, v_tempclob, v_location,p_trans_type, p_userid );
END gm_sav_insert_location_qty;

/*******************************************************
* Description : Procedure to Adjust the Quantity in Inventory Location
* Author     : Karthik
*******************************************************/
--
PROCEDURE gm_upd_insert_inv_loc_qty
  (
    p_txnid 		IN T5056_TRANSACTION_INSERT.C5056_REF_ID%TYPE,
    p_inputdataclob IN CLOB ,
    p_operation     IN T5056_TRANSACTION_INSERT.C901_STATUS%TYPE,
    p_trans_type	IN T5056_TRANSACTION_INSERT.C901_REF_TYPE%TYPE,
    p_userid        IN t101_user.c101_user_id%TYPE 
  )
AS
  v_curqty t5053_location_part_mapping.c5053_curr_qty%TYPE;
  v_string CLOB := p_inputdataclob;
  v_pnum      VARCHAR2 (20);
  v_substring VARCHAR2 (1000);
  v_qty       NUMBER;
  v_location_id t5053_location_part_mapping.c5052_location_id%TYPE;
  v_loc_type t5052_location_master.c901_location_type%TYPE;
  v_loc_part_id t5053_location_part_mapping.c5053_location_part_map_id%TYPE;
  v_txn_type   NUMBER;
  v_verify_cnt NUMBER;
  v_whtype     VARCHAR2(20) :='';
  v_plant_id t5040_plant_master.c5040_plant_id%TYPE;
  
BEGIN
  
   SELECT get_plantid_frm_cntx()
	 INTO v_plant_id
	 FROM DUAL;
			  
   IF v_plant_id IS NULL THEN   
	    SELECT NVL(get_rule_value('DEFAULT_PLANT','ONEPORTAL'),'3000')
	  	  INTO v_plant_id 
	  	  FROM dual;
   END IF;
   
  -- p_inputstring will have partnumber','quantity','location|
  WHILE INSTR (v_string, '|') <> 0
  LOOP
    v_substring   := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
    v_string      := SUBSTR (v_string, INSTR (v_string, '|')    + 1);
    v_pnum        := NULL;
    v_qty         := 0;
    v_location_id := NULL;
    v_pnum        := SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
    v_substring   := SUBSTR (v_substring, INSTR (v_substring, ',')    + 1);
    -- WHEN PUTAWAY PROCESSED FROM DEVICE, IT WILL SEND ALL LOCATIONS AND NULL QTY FOR LOCATIONS WHICH ARE NOT SCANNED DURING PUTAWAY.
    --TO AVOID ERROR FROM TRIGGER, REPLACING NULL WITH 0.
    v_qty         := NVL (SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1), 0);
    v_substring   := SUBSTR (v_substring, INSTR (v_substring, ',')         + 1);
    v_location_id := v_substring;
    -- Get the warehouse type
  
    BEGIN
	    SELECT t5051.c901_warehouse_type
	      INTO v_whtype
	      FROM t5052_location_master t5052,
	    	   t5051_inv_warehouse t5051
	     WHERE t5052.c5052_location_id = v_location_id
	       AND t5051.C5051_inv_warehouse_id = t5052.C5051_inv_warehouse_id
	       AND t5051.C901_STATUS_ID = 1
	       AND t5052.c5040_plant_id = v_plant_id;
	END;
    
    BEGIN
      SELECT NVL (c5053_curr_qty, 0),
        	 c5053_location_part_map_id
        INTO v_curqty,
             v_loc_part_id
        FROM t5053_location_part_mapping t5053,
	         t5052_location_master t5052,
	         t5051_inv_warehouse t5051
	   WHERE t5052.c5052_location_id = t5053.c5052_location_id
		 AND t5053.c205_part_number_id = v_pnum
		 AND t5053.c5052_location_id = v_location_id
		 AND t5052.c901_status = '93310'
		 AND t5051.C5051_inv_warehouse_id = t5052.C5051_inv_warehouse_id
		 AND t5053.c5052_location_id = t5052.c5052_location_id
		 AND t5051.c901_warehouse_type = nvl(v_whtype,t5051.c901_warehouse_type)
		 AND t5051.c901_status_id =1
		 AND t5052.c5040_plant_id = v_plant_id
		 AND t5052.c5052_void_fl is null
        FOR UPDATE;
        
      IF UPPER (p_operation) = '93326' -- PICK
        THEN
        v_curqty := v_curqty - v_qty;
      ELSIF UPPER (p_operation) = '93327' -- PUTAWAY
        THEN
        v_curqty := v_curqty + v_qty;
      END IF;
      
      IF v_loc_part_id IS NOT NULL THEN
      
      UPDATE t5053_location_part_mapping
         SET c5053_curr_qty = v_curqty ,
             c5053_last_updated_date = CURRENT_DATE ,
             c5053_last_updated_by = p_userid ,
             c5053_last_update_trans_id = p_txnid ,
             c901_last_transaction_type = p_trans_type
       WHERE c5053_location_part_map_id = v_loc_part_id;    
      END IF;
    END;
  END LOOP;
END gm_upd_insert_inv_loc_qty;
--

/**************************************************************
* Description : UnAssigns the transaction in the pick table
* Author      : Shiny --PMT-39215
***************************************************************/
PROCEDURE gm_unassign_transaction(
  p_txn_id   IN t5050_invpick_assign_detail.c5050_ref_id%type,
  p_userid   IN t5050_invpick_assign_detail.c5050_last_updated_by%type
)
AS
BEGIN
	
	UPDATE t5050_invpick_assign_detail
       SET c901_status  = NULL ,
           c101_allocated_user_id  = NULL ,
           c5050_allocated_dt      = NULL ,
           c5050_last_updated_by   = p_userid ,
           c5050_last_updated_date = CURRENT_DATE
    WHERE c5050_ref_id = p_txn_id;

END gm_unassign_transaction;

END gm_pkg_op_sav_insert_txn;
/