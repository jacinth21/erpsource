--@"c:\database\packages\operations\gm_pkg_op_set_pick_txn.bdy";
CREATE OR REPLACE
PACKAGE BODY gm_pkg_op_set_pick_txn
IS
/****************************************************************
* Description : this procedure is used split and call the procedure
* Author      :
*****************************************************************/
PROCEDURE gm_sav_set_pick_batch (
        p_inputstr  IN CLOB,
        p_user_id   IN t5052_location_master.c5052_created_by%TYPE)
AS
    v_string CLOB   := p_inputstr;
    v_strlen      NUMBER := NVL (LENGTH (p_inputstr), 0) ;
    v_substring   VARCHAR2 (30000) ;
    v_location_id VARCHAR2 (40) ;
    v_tagid       VARCHAR2 (40) ;
    v_cnid        VARCHAR2 (40) ;
    v_count       NUMBER;
    v_warehouse_id	NUMBER;
BEGIN
    IF v_strlen > 0 THEN
        /* Loop the Delminted String and reterive the values */
        WHILE INSTR (v_string, '|') <> 0
        LOOP
            v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
            v_string    := SUBSTR (v_string, INSTR (v_string, '|')    + 1) ;
            --
            v_cnid        := NULL;
            v_location_id := NULL;
            v_tagid       := NULL;
            --
            v_cnid        := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            v_substring   := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
            v_location_id := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            v_substring   := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
            v_tagid       := v_substring;
            -- validate the Location..
            --gm_pkg_op_set_validate.gm_validate_location(v_location_id,v_types);
            gm_pkg_op_set_pick_txn.gm_sav_set_pick (v_tagid, v_location_id, p_user_id, v_cnid) ;
        END LOOP;
    END IF;
END gm_sav_set_pick_batch;

/****************************************************************
* Description : this procedure is used to save the pick details
* Author      :
*****************************************************************/
PROCEDURE gm_sav_set_pick (
        p_tag_id IN t5010_tag.c5010_tag_id%TYPE,
        p_loc_id IN t5052_location_master.c5052_location_id%TYPE,
        p_user_id  IN t101_user.c101_user_id%TYPE,
        p_cn	 IN t504_consignment.c504_consignment_id%TYPE)
AS
  v_error_code NUMBER;
  v_txn_id VARCHAR2 (20);
  v_txn_type NUMBER;
  v_loc_id VARCHAR2 (20);
  v_out_param1 varchar2(20);
  v_out_param2 varchar2(20);
BEGIN
  
  IF p_tag_id IS NOT NULL THEN
  	  gm_pkg_ac_tag_info.gm_fch_tag_info(p_tag_id,v_loc_id, v_txn_id, v_txn_type) ; -- Get Txn id, Txn type and Location Id
  ELSE
  	  v_txn_type := get_tag_inv_type(p_cn); 
  	  v_loc_id := p_loc_id;
  	  v_txn_id := p_cn;
  END IF; 
 
  gm_pkg_op_set_txn.gm_sav_set_txn_status (v_txn_id, 'PICK', v_txn_type, p_user_id); -- Save transaction status
  
  IF v_loc_id IS NOT NULL THEN --From device it will be allowed when loc id is empty
  gm_pkg_op_set_txn.gm_sav_tag_location(p_tag_id, v_loc_id, v_txn_id, v_txn_type, p_user_id,'PICK');
  END IF;
      
 -- gm_pkg_allocation.gm_sav_set_allocation(v_txn_id, v_txn_type, p_user_id,93006, 'PICK'); --Completed. => Update user id in t5050 for the transaction
 
 gm_pkg_allocation.gm_sav_set_complete(v_txn_id, v_txn_type, p_user_id,93006); --Completed. => Update user id in t5050 for the transaction
    
END gm_sav_set_pick ;

END gm_pkg_op_set_pick_txn;
/