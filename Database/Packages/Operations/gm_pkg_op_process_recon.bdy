-- @"C:\Database\Packages\Operations\gm_pkg_op_process_recon.bdy";
CREATE OR REPLACE PACKAGE BODY gm_pkg_op_process_recon 
IS
/*
 * Description: This Procedure will be the starting execution of the Auto Reconciliation Job, which loads with the Missing Loaner Transactions
 * Author:		Rajkumar 
 */
PROCEDURE gm_ln_main_flow
AS
v_ln_cur		TYPES.cursor_type;
v_ln_item_cur	TYPES.cursor_type;

v_criteria		VARCHAR2(20);
v_swap			VARCHAR2(20) := 'N';
v_transid		T412_INHOUSE_TRANSACTIONS.C412_INHOUSE_TRANS_ID%TYPE;
v_swapcnt		NUMBER := 0;
v_exit_val		NUMBER := 0;
v_ret_qty		NUMBER := 0;

BEGIN
	-- Loading the Not Scanned Missing Loaner Txn and stored in v_ln_cur
	gm_pkg_op_fetch_recon.gm_fch_not_scanned_ln(v_ln_cur);
	LOOP
		FETCH v_ln_cur INTO v_transid;
		EXIT
		WHEN v_ln_cur%NOTFOUND;
			-- Loading the Missing Loaner Txn Items 
			gm_pkg_op_fetch_recon.gm_fch_ln_item(v_transid,v_ln_item_cur);
			--for Reconciliation by Loan date
			gm_pkg_op_process_recon.gm_do_process_loaner(v_transid,v_ln_item_cur,'LOAN_DT');
			
	END LOOP;
	-- Loading the Not Scanned Missing Loaner Txn after reconciled by loan date to avoid reprocessing of reconciled transaction 
	--and stored in v_ln_cur
	gm_pkg_op_fetch_recon.gm_fch_not_scanned_ln_by_date(v_ln_cur);--by cut off date for new GM-LN
	
	LOOP
		FETCH v_ln_cur INTO v_transid;
		EXIT
		WHEN v_ln_cur%NOTFOUND;
			-- Loading the Missing Loaner Txn Items by Loaner Extension date
			gm_pkg_op_fetch_recon.gm_fch_ln_item_by_loan_ext(v_transid,v_ln_item_cur);
			--for Reconciliation by Loaner Extension date
			gm_pkg_op_process_recon.gm_do_process_loaner(v_transid,v_ln_item_cur,'EXT_LOAN_DT');

	END LOOP;
END gm_ln_main_flow;
/*
 * Description: This Procedure will be the execution after the above Procedure called and ended of the Auto Reconciliation Job, 
 * 				which loads with the Not Reconciled Orders
 * Author:		Rajkumar 
 */
PROCEDURE gm_do_main_flow
AS
	v_ln_item_cur	TYPES.cursor_type;
	v_ord_item_cur	TYPES.cursor_type;
	
	v_criteria		VARCHAR2(20);
	v_swap			VARCHAR2(20) := 'N';
	v_transid		T412_INHOUSE_TRANSACTIONS.C412_INHOUSE_TRANS_ID%TYPE;
	v_pnum 			T413_INHOUSE_TRANS_ITEMS.C205_PART_NUMBER_ID%TYPE;
	v_repid			T504A_LOANER_TRANSACTION.C703_SALES_REP_ID%TYPE;
	v_accid 		T504A_LOANER_TRANSACTION.C704_ACCOUNT_ID%TYPE;
	v_itemqty 		T413_INHOUSE_TRANS_ITEMS.C413_ITEM_QTY%TYPE;
	v_itemprice 	T413_INHOUSE_TRANS_ITEMS.C413_ITEM_PRICE%TYPE;
	v_cnum 			T413_INHOUSE_TRANS_ITEMS.C413_CONTROL_NUMBER%TYPE;
	v_ordid 		T501_ORDER.C501_ORDER_ID%TYPE;
	v_ord_pnum 		T502_ITEM_ORDER.C205_PART_NUMBER_ID%TYPE;
	v_ord_qty 		T502_ITEM_ORDER.C502_ITEM_QTY%TYPE;
	v_swapcnt		NUMBER := 0;
	v_exit_val		NUMBER := 0;
	v_ret_qty		T502_ITEM_ORDER.C502_ITEM_QTY%TYPE := 0;
	v_rem_qty		T502_ITEM_ORDER.C502_ITEM_QTY%TYPE;
	v_order_dt		T501_ORDER.C501_ORDER_DATE%TYPE;
BEGIN
	gm_pkg_op_fetch_recon.gm_fch_not_rec_doitems(v_ord_item_cur);
	LOOP
		FETCH v_ord_item_cur INTO v_ordid,v_ord_pnum,v_ord_qty,v_repid,v_accid,v_order_dt;
		EXIT
		WHEN v_ord_item_cur%NOTFOUND;
		-- Checking whether this order is returned or not
		v_ret_qty := gm_pkg_op_fetch_recon.gm_chk_returns(v_ordid,v_ord_pnum);
		v_ord_qty := v_ord_qty - v_ret_qty;
		v_rem_qty := v_ord_qty; 
		IF v_rem_qty > 0
		THEN
			-- Fetch the Not Recociled Missing LN transaction with the combination of Part#, Repid and Accid from the Order
			gm_pkg_op_fetch_recon.gm_fch_notrec_ln_items(v_ord_pnum,v_accid,v_repid,v_ln_item_cur,v_order_dt);
			LOOP
				FETCH v_ln_item_cur INTO v_transid,v_pnum,v_itemqty,v_itemprice,v_cnum;
				EXIT
				WHEN v_ln_item_cur%NOTFOUND;
					-- Update the LN and DO process
					--gm_process_ln(v_ordid,v_transid,v_ord_pnum,v_ord_qty,v_itemqty,v_itemprice,v_cnum);
					IF v_itemqty >0 AND v_rem_qty > 0
					THEN
						gm_pkg_op_process_recon.gm_process_ln(v_ordid,v_transid,v_ord_pnum,v_rem_qty,v_itemqty,v_itemprice,v_cnum,null,null,'30301');
						gm_do_process(v_ordid,v_ord_pnum,'30301');
					
						--v_exit_val := gm_pkg_op_fetch_recon.get_missing_qty(v_ord_pnum,v_transid) - gm_pkg_op_fetch_recon.get_reconciled_qty(v_ord_pnum,v_transid);
						v_exit_val :=  gm_pkg_op_fetch_recon.get_order_qty(v_ordid,v_ord_pnum) - gm_pkg_op_fetch_recon.get_reconciled_ord_qty(v_ordid,v_ord_pnum); 			
						EXIT WHEN v_exit_val = 0;
						v_rem_qty := v_ord_qty - gm_pkg_op_fetch_recon.get_reconciled_ord_qty(v_ordid,v_ord_pnum);
					END IF;
			END LOOP;
		END IF;
	END LOOP;
END gm_do_main_flow;
/*
 * Description: This Procedure will unreconcilie the Orders that are reconciled and Swapped or Returned
 * Author:		Rajkumar 
 */
PROCEDURE gm_unrec_main_flow
AS
v_swap_ord_cur	TYPES.cursor_type;
v_ret_ord_cur	TYPES.cursor_type;
v_ordid		T501_ORDER.C501_ORDER_ID%TYPE;
v_ord_pnum	T502_ITEM_ORDER.C205_PART_NUMBER_ID%TYPE;
v_itemqty	T502_ITEM_ORDER.C502_ITEM_QTY%TYPE;
v_qty		T502_ITEM_ORDER.C502_ITEM_QTY%TYPE;
v_cnt		NUMBER:=0;
CURSOR v_ln_cur(v_ord_id IN T501_ORDER.C501_ORDER_ID%TYPE,v_pnum IN T502_ITEM_ORDER.C205_PART_NUMBER_ID%TYPE) IS
		SELECT C413_TRANS_ID ID,C412_INHOUSE_TRANS_ID TRANID, C413_ITEM_QTY QTY
		   FROM T413_INHOUSE_TRANS_ITEMS
		  WHERE C413_REF_ID = v_ord_id
		  AND C205_PART_NUMBER_ID = v_pnum
		  AND c413_status_fl = 'S'
		  AND C413_VOID_FL IS NULL;
BEGIN
	-- Loop For Swapped Orders
	gm_pkg_op_fetch_recon.gm_fch_swapped_ord(v_swap_ord_cur);
	LOOP
		FETCH v_swap_ord_cur INTO v_ordid,v_ord_pnum,v_itemqty;
		EXIT
		WHEN v_swap_ord_cur%NOTFOUND;
		-- Checking this Order with the pnum is already scanned
		SELECT COUNT(1) INTO v_cnt
		   FROM T502A_ITEM_ORDER_ATTRIBUTE
		  WHERE C501_ORDER_ID          = v_ordid
		    AND C205_PART_NUMBER_ID    = v_ord_pnum
		    AND C901_ATTRIBUTE_TYPE    = '1006458'
		    AND C502A_ATTRIBUTE_VALUE = 'Y'
		    AND C502A_VOID_FL IS NULL;
		 IF v_cnt = 0
		 THEN
		 -- If this order id and pnum is scanned and has remaining qty or not scanned, we getting the Remaining QTy or Actual qty respectively
			BEGIN
			 SELECT C502A_ITEM_QTY INTO v_qty
			   FROM T502A_ITEM_ORDER_ATTRIBUTE
			  WHERE C501_ORDER_ID          = v_ordid
			    AND C205_PART_NUMBER_ID    = v_ord_pnum
			    AND C901_ATTRIBUTE_TYPE    = '1006458'
			    AND C502A_ATTRIBUTE_VALUE <> 'Y'
			    AND C502A_VOID_FL IS NULL;
			 EXCEPTION WHEN NO_DATA_FOUND
			 THEN
			 	v_qty := v_itemqty ;
			 END;
			 
			IF v_qty > 0
			THEN
				v_itemqty := v_qty;
				FOR v_loop IN v_ln_cur(v_ordid,v_ord_pnum)
				LOOP		
					IF v_itemqty >= v_loop.QTY
					THEN
						gm_pkg_op_trans_unrecon.gm_remove_ln_item(v_loop.ID,'30301'); -- Void the Transaction, cause Reconciled QTy is less than Swapped QTy
						v_itemqty := v_itemqty - v_loop.QTY;
					ELSE
						v_itemqty := v_loop.QTY - v_itemqty;
						gm_pkg_op_trans_recon.gm_update_sold_ln(v_loop.ID,v_itemqty,'30301'); -- Updating the Swapped Qty as Reconciled Qty
						v_itemqty := 0; 
					END IF;
					-- Setting the Swapped is Scanned or not
					IF v_itemqty = 0 THEN
						gm_pkg_op_trans_recon.gm_sav_record_attrib(v_ordid,v_ord_pnum,v_itemqty,'1006458','Y','30301');
					ELSE
						gm_pkg_op_trans_recon.gm_sav_record_attrib(v_ordid,v_ord_pnum,v_itemqty,'1006458','N','30301');
					END IF;
					gm_pkg_op_trans_recon.gm_sav_ln_status(v_loop.TRANID,'3','30301');
					gm_pkg_op_trans_recon.gm_sav_verify_ln(v_loop.TRANID,'N', '30301');
					gm_pkg_op_trans_unrecon.gm_revert_do(v_ordid,v_ord_pnum,'30301');
					EXIT WHEN v_itemqty = 0;
				END LOOP;
			END IF;
		END IF;
	END LOOP;
	
	-- Loop For Returned Orders
	gm_pkg_op_fetch_recon.gm_fch_ret_ord(v_ret_ord_cur);
	LOOP		
	FETCH v_ret_ord_cur INTO v_ordid,v_ord_pnum,v_itemqty;
	EXIT
	WHEN v_ret_ord_cur%NOTFOUND;
	-- Checking this Order with the pnum is already scanned
	SELECT COUNT(1) INTO v_cnt
	   FROM T502A_ITEM_ORDER_ATTRIBUTE
	  WHERE C501_ORDER_ID          = v_ordid
	    AND C205_PART_NUMBER_ID    = v_ord_pnum
	    AND C901_ATTRIBUTE_TYPE    = '1006459'
	    AND C502A_ATTRIBUTE_VALUE = 'Y'
	    AND C502A_VOID_FL IS NULL;
	 IF v_cnt = 0
	 THEN
	 	-- If this order id and pnum is scanned and has remaining qty or not scanned, we getting the Remaining QTy or Actual qty respectively 
		BEGIN
			 SELECT C502A_ITEM_QTY INTO v_qty
			   FROM T502A_ITEM_ORDER_ATTRIBUTE
			  WHERE C501_ORDER_ID          = v_ordid
			    AND C205_PART_NUMBER_ID    = v_ord_pnum
			    AND C901_ATTRIBUTE_TYPE    = '1006459'
			    AND C502A_ATTRIBUTE_VALUE <> 'Y'
			    AND C502A_VOID_FL IS NULL;
			 EXCEPTION WHEN NO_DATA_FOUND
			 THEN
			 	v_qty := v_itemqty ;
			 END;
		IF v_qty > 0
		THEN
			v_itemqty := v_qty;
			FOR v_loop IN v_ln_cur(v_ordid,v_ord_pnum)
			LOOP
				IF v_itemqty >= v_loop.QTY
				THEN
					gm_pkg_op_trans_unrecon.gm_remove_ln_item(v_loop.ID,'30301'); -- Void the Transaction, cause Reconciled QTy is less than Returned QTy
					v_itemqty := v_itemqty - v_loop.QTY;
				ELSE
					v_itemqty := v_loop.QTY - v_itemqty;
					gm_pkg_op_trans_recon.gm_update_sold_ln(v_loop.ID,v_itemqty,'30301'); -- Updating the Returned Qty as Reconciled Qty
					v_itemqty := 0; 
				END IF;
					-- Setting the Return is Scanned or not
					IF v_itemqty = 0 THEN
						gm_pkg_op_trans_recon.gm_sav_record_attrib(v_ordid,v_ord_pnum,v_itemqty,'1006459','Y','30301');
					ELSE
						gm_pkg_op_trans_recon.gm_sav_record_attrib(v_ordid,v_ord_pnum,v_itemqty,'1006459','N','30301');
					END IF;
				gm_pkg_op_trans_recon.gm_sav_ln_status(v_loop.TRANID,'3','30301');
				gm_pkg_op_trans_recon.gm_sav_verify_ln(v_loop.TRANID,'N', '30301');
				gm_pkg_op_trans_unrecon.gm_revert_do(v_ordid,v_ord_pnum,'30301');
				EXIT WHEN v_itemqty = 0;
			END LOOP;
		END IF;
	END IF;
	END LOOP;
END gm_unrec_main_flow;
/*
 * Description: This Procedure will be the execute all the main Flows
 * Author:		Rajkumar 
 */
PROCEDURE gm_main_flow
AS
	v_start_dt 	DATE;
	v_plant_id     t5040_plant_master.c5040_plant_id%TYPE;
	v_plant_name   t5040_plant_master.c5040_plant_name%TYPE;
BEGIN
	v_start_dt := CURRENT_DATE;
	SELECT  get_plantid_frm_cntx()
		INTO  v_plant_id
	FROM DUAL;
	v_plant_name := get_plant_name(v_plant_id);
	
	gm_pkg_op_process_recon.gm_ln_main_flow();

	gm_pkg_op_process_recon.gm_do_main_flow();

	gm_pkg_op_process_recon.gm_unrec_main_flow();
	
	-- Log the status to know whether the Job is success or not 
	gm_pkg_cm_job.gm_cm_notify_load_info (v_start_dt , CURRENT_DATE , 'S' , 'SUCCESS' , 'gm_pkg_op_process_recon - Autoreconciliation executed in '||v_plant_name );
	COMMIT;
	EXCEPTION
	WHEN OTHERS THEN
  		ROLLBACK; 
	  	gm_pkg_cm_job.gm_cm_notify_load_info (v_start_dt , CURRENT_DATE , 'E' , SQLERRM , 'gm_pkg_op_process_recon - Autoreconciliation Error in '||v_plant_name );
  		COMMIT;
	
END gm_main_flow;

/*
 * Description:	This procedure will process the missing Loaner Txn will hande and update the sold with Quantity macthing
 * Author:		Rajkumar
 */
PROCEDURE gm_process_ln(
	p_ordid			IN	T501_ORDER.C501_ORDER_ID%TYPE,
	p_txnid			IN	T412_INHOUSE_TRANSACTIONS.C412_INHOUSE_TRANS_ID%TYPE,
	p_pnum			IN	T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE,
	p_ord_item_qty	IN	T502_ITEM_ORDER.C502_ITEM_QTY%TYPE,
	p_item_qty		IN	T413_INHOUSE_TRANS_ITEMS.C413_ITEM_QTY%TYPE,
	p_price			IN	T413_INHOUSE_TRANS_ITEMS.C413_ITEM_PRICE%TYPE,
	p_cnum			IN	T413_INHOUSE_TRANS_ITEMS.C413_CONTROL_NUMBER%TYPE,
	p_rec_pnum		IN	T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE,
	p_reason		IN	VARCHAR2,
	p_userid		IN	T413_INHOUSE_TRANS_ITEMS.C413_LAST_UPDATED_BY%TYPE
)
AS
v_reason	VARCHAR2(20);
BEGIN
	
	IF p_reason = '0'
	THEN
		v_reason := null;
	ELSE
		v_reason := p_reason;
	END IF;
	-- Checking the LN qty with order qty, if LN qty is less than order qty than save the LN as sold with LN qty otherwise sold with Order Qty
	IF p_item_qty <= p_ord_item_qty
	THEN
		IF p_item_qty > 0 
		THEN
			gm_pkg_op_trans_recon.gm_sav_sold_ln(p_txnid,p_pnum,p_item_qty,p_price,p_ordid,p_cnum,p_rec_pnum,v_reason,p_userid);
		END IF;
	ELSE
		IF p_ord_item_qty > 0
		THEN
			gm_pkg_op_trans_recon.gm_sav_sold_ln(p_txnid,p_pnum,p_ord_item_qty,p_price,p_ordid,p_cnum,p_rec_pnum,v_reason,p_userid);
		END IF;
	END IF;
	
	--Whenever the detail is updated, the inhouse transactions table has to be updated ,so ETL can Sync it to GOP.
	UPDATE t412_inhouse_transactions 
	   SET c412_last_updated_by = p_userid
	     , c412_last_updated_date = SYSDATE
	 WHERE c412_inhouse_trans_id = p_txnid;	

END gm_process_ln;
/*
 * Description:	This procedure will process the DO, checks whether this DO contains more or nothing
 * Author:		Rajkumar
 */
PROCEDURE gm_do_process(
	p_ordid			IN	T501_ORDER.C501_ORDER_ID%TYPE,
	p_pnum			IN	T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE,
	p_userid		IN	T413_INHOUSE_TRANS_ITEMS.C413_LAST_UPDATED_BY%TYPE
)
AS
BEGIN
	
	IF gm_pkg_op_fetch_recon.get_order_qty(p_ordid,p_pnum) <= gm_pkg_op_fetch_recon.get_reconciled_ord_qty(p_ordid,p_pnum)
	THEN
		gm_pkg_op_trans_recon.gm_sav_reconciled_do(p_ordid,p_pnum,'Y',p_userid);
	ELSE
		gm_pkg_op_trans_recon.gm_sav_reconciled_do(p_ordid,p_pnum,'N',p_userid);
	END IF;
END gm_do_process;
/*
 * Description:	This procedure will process the Missing Loaner LN Id, checks whether this LN contains more or nothing
 * Author:		Rajkumar
 */
PROCEDURE gm_ln_process(
	p_txnid			IN	T412_INHOUSE_TRANSACTIONS.C412_INHOUSE_TRANS_ID%TYPE,
	p_userid		IN	T413_INHOUSE_TRANS_ITEMS.C413_LAST_UPDATED_BY%TYPE
)
AS
v_rec_qty	NUMBER;
v_miss_qty	NUMBER;
BEGIN
	     SELECT NVL(SUM (T413.C413_ITEM_QTY),0) INTO v_miss_qty 
	       FROM T413_INHOUSE_TRANS_ITEMS T413
	      WHERE T413.C412_INHOUSE_TRANS_ID = p_txnid
	        AND T413.C413_VOID_FL         IS NULL
	        AND T413.C413_STATUS_FL        = 'Q';
	        
	     SELECT NVL(SUM (T413.C413_ITEM_QTY),0) INTO v_rec_qty 
	       FROM T413_INHOUSE_TRANS_ITEMS T413
	      WHERE T413.C412_INHOUSE_TRANS_ID = p_txnid
	        AND T413.C413_VOID_FL         IS NULL
	        AND T413.C413_STATUS_FL        IN ('S','W','R','C');
	        
	        IF v_miss_qty = v_rec_qty
	        THEN
	        	gm_pkg_op_trans_recon.gm_sav_verify_ln(p_txnid,'Y',p_userid);
	        	gm_pkg_op_trans_recon.gm_sav_ln_status(p_txnid,'4',p_userid);
	        ELSE
	        	gm_pkg_op_trans_recon.gm_sav_verify_ln(p_txnid,'N',p_userid);
	        END IF;
END gm_ln_process;
/*
 * 
 */
PROCEDURE gm_save_rec_do(
	p_txnid			IN	T412_INHOUSE_TRANSACTIONS.C412_INHOUSE_TRANS_ID%TYPE,
	p_inpstr		IN	VARCHAR2,
	p_userid	IN	T412_INHOUSE_TRANSACTIONS.C412_LAST_UPDATED_BY%TYPE
)
AS
v_string	VARCHAR2(2000) := p_inpstr;
v_substring	VARCHAR2(2000);
v_ordid		VARCHAR2(20);
v_pnum		VARCHAR2(20);
v_tmp_pnum	VARCHAR2(20);
v_qty		NUMBER;
v_reason	VARCHAR2(20);
v_recpart	VARCHAR2(20);
v_itemqty	NUMBER;
v_itemprice	NUMBER;
v_cnum		VARCHAR2(20);
v_miss_qty	NUMBER :=0;
v_rec_qty	NUMBER :=0;
v_ord_qty	NUMBER :=0;
v_ord_rec_qty	NUMBER :=0;
v_cnt	NUMBER:=0;
v_type	T412_INHOUSE_TRANSACTIONS.C412_TYPE%TYPE;
v_status T412_INHOUSE_TRANSACTIONS.C412_STATUS_FL%TYPE;

BEGIN
	BEGIN
		 SELECT T412.C412_STATUS_FL, T412.C412_TYPE
		 	INTO v_status, v_type
   			FROM T412_INHOUSE_TRANSACTIONS T412
  		   WHERE T412.C412_INHOUSE_TRANS_ID = p_txnid
    		AND T412.C412_VOID_FL         IS NULL
    		FOR UPDATE;
	EXCEPTION WHEN NO_DATA_FOUND
	THEN
		v_status := NULL;
		v_type	 := NULL; 
	END;
	
	SELECT COUNT(1) INTO v_cnt
   		 FROM T412_INHOUSE_TRANSACTIONS T412
  		WHERE T412.C412_INHOUSE_TRANS_ID = p_txnid
    		AND T412.C412_VOID_FL         IS NOT NULL;
    		
	IF v_cnt >0 
	THEN
		GM_RAISE_APPLICATION_ERROR('-20999','262',p_txnid);
	END IF;
	
	WHILE INSTR (v_string, '|') <> 0
	LOOP

	        v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
	        v_string    := SUBSTR (v_string, INSTR (v_string, '|') + 1) ;
	        
	        v_ordid		:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
	        v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1) ;
	        
	        v_pnum		:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
	        v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1) ;
	        
	        v_qty		:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
	        v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1) ;
	        
	        v_recpart	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
	        v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1) ;
	        
	        v_reason	:= v_substring;
	        
	        
	        IF v_recpart IS NOT NULL
	        THEN
	        	v_tmp_pnum := v_recpart;
	        ELSE
	        	v_tmp_pnum := v_pnum;
	        END IF;
	        
	         -- For Japan, the GM-LN txn can have multiple rows for the same part as they do the missing lot while processing loaner. 
             --Hence handling exact fetch error and taking one row for the part number.
	     	BEGIN
		     	BEGIN
	          SELECT T413.C413_ITEM_PRICE, T413.C413_CONTROL_NUMBER
	          	INTO v_itemprice, v_cnum
			   FROM T413_INHOUSE_TRANS_ITEMS T413
			  WHERE T413.C412_INHOUSE_TRANS_ID = p_txnid
			    AND T413.C205_PART_NUMBER_ID   = v_tmp_pnum
			    AND T413.C413_STATUS_FL = 'Q'
			    AND T413.C413_VOID_FL         IS NULL;
			EXCEPTION WHEN TOO_MANY_ROWS 
			THEN   
	          SELECT T413.C413_ITEM_PRICE, T413.C413_CONTROL_NUMBER
	          	INTO v_itemprice, v_cnum
			   FROM T413_INHOUSE_TRANS_ITEMS T413
			  WHERE T413.C412_INHOUSE_TRANS_ID = p_txnid
			    AND T413.C205_PART_NUMBER_ID   = v_tmp_pnum
			    AND T413.C413_STATUS_FL = 'Q'
			    AND T413.C413_VOID_FL         IS NULL
			    AND ROWNUM=1;
			 END;
	        EXCEPTION WHEN NO_DATA_FOUND
	        THEN
		        v_itemqty	:= 0;
				v_itemprice	:= 0;
				v_cnum		:= '';
	        END;
	        
					SELECT COUNT (1) INTO v_cnt
	   				 FROM t413_inhouse_trans_items
	  				 WHERE c205_part_number_id   = v_tmp_pnum
	    			   AND c412_inhouse_trans_id = p_txnid
	    			   AND c413_status_fl        = 'Q'
	    			   AND c413_void_fl         IS NULL;

				    IF v_cnt = 0
				    THEN
				    	GM_RAISE_APPLICATION_ERROR('-20999','263',v_recpart||'#$#'||p_txnid);
				    END IF;
 
	        v_miss_qty := gm_pkg_op_fetch_recon.get_missing_qty(v_tmp_pnum,p_txnid);
			v_rec_qty := gm_pkg_op_fetch_recon.get_reconciled_qty(v_tmp_pnum,p_txnid); 
			v_itemqty := v_miss_qty - v_rec_qty;  
			IF v_miss_qty = v_rec_qty
			THEN
				         GM_RAISE_APPLICATION_ERROR('-20999','264',p_txnid||'#$#'||v_tmp_pnum); 
			END IF;
			
			v_ord_qty := gm_pkg_op_fetch_recon.get_order_qty(v_ordid,v_pnum); 			
			v_ord_rec_qty := gm_pkg_op_fetch_recon.get_reconciled_ord_qty(v_ordid,v_pnum); 
			
			SELECT COUNT(1) INTO v_cnt FROM T501_ORDER
				WHERE C501_ORDER_ID = v_ordid
				AND C501_VOID_FL IS NOT NULL;
			
				IF v_cnt>0 THEN
					GM_RAISE_APPLICATION_ERROR('-20999','265','');
				END IF;
			
			IF v_ord_qty = v_ord_rec_qty
			THEN
				         GM_RAISE_APPLICATION_ERROR('-20999','264',v_ordid||'#$#'||v_pnum); 
			END IF;
			
			SELECT 
				T501.C501_ORDER_ID INTO v_ordid
				FROM T501_ORDER T501 
				WHERE 
					T501.C501_ORDER_ID = v_ordid AND T501.C501_VOID_FL IS NULL 
				FOR UPDATE;
			gm_pkg_op_process_recon.gm_process_ln(v_ordid,p_txnid,v_tmp_pnum,v_qty,v_itemqty,v_itemprice,v_cnum,v_pnum,v_reason,p_userid);
			gm_pkg_op_process_recon.gm_do_process(v_ordid,v_pnum,p_userid);
			gm_pkg_op_process_recon.gm_ln_process(p_txnid,p_userid);
			
	END LOOP;
END gm_save_rec_do;
/*
 * Description: Procedure Used to save the Missing Reconciliation fro IH Loaner and Items 
 * Author: Rajkumar
 */
PROCEDURE gm_sav_inhouse_recon(
   p_transid          IN       t412_inhouse_transactions.c412_inhouse_trans_id%TYPE,
   p_type			  IN	   t412_inhouse_transactions.c412_type%TYPE,
   p_retstr           IN       VARCHAR2,
   p_constr           IN       VARCHAR2,
   p_writestr         IN       VARCHAR2,
   p_ihstr            IN       VARCHAR2,
   p_userid           IN       t412_inhouse_transactions.c412_last_updated_by%TYPE,
   p_out              OUT      VARCHAR2
)
AS
   v_transid    VARCHAR2 (20);
   v_type       NUMBER;
   v_reason     NUMBER;
   v_comments   VARCHAR2 (100);
   v_qty        NUMBER;
   v_id         VARCHAR2 (20);
   v_distid     VARCHAR2 (20);
   v_string		VARCHAR2(2000);
   v_errmsg		VARCHAR2(2000);
BEGIN
	 IF p_constr IS NOT NULL
	   THEN
      -- Getting the Distributor ID
      
	   BEGIN
      	SELECT t504a.c504a_consigned_to_id
        	INTO v_distid
        	FROM t504a_loaner_transaction t504a, t412_inhouse_transactions t412
       	WHERE t412.c412_inhouse_trans_id = p_transid
         	AND t412.c504a_loaner_transaction_id =
                                             t504a.c504a_loaner_transaction_id;
		EXCEPTION WHEN NO_DATA_FOUND
		THEN
			v_distid := NULL;
		END;
      --
      v_id := get_next_consign_id ('Consign');

      gm_save_item_consign
                        (v_id,
                         4110,
                         0,
                         v_distid,
                         NULL,
                         0,
                            'Consigned after reconciling Loaner Transaction:'
                         || p_transid,
                         NULL,
                         p_userid,
                         p_constr
                        );
      --
      UPDATE t504_consignment
         SET c504_update_inv_fl = '1',
             c504_verify_fl = '1',
             c504_last_updated_date = SYSDATE,
             c504_last_updated_by = p_userid,
             c504_ship_date = TRUNC (SYSDATE),
             c504_status_fl = 4,
             c504_reprocess_id = p_transid
       WHERE c504_consignment_id = v_id;
      --

      gm_cs_sav_loaner_recon_items (p_transid, p_constr, 'C', v_id, p_userid);
   END IF;

   --
   IF p_writestr IS NOT NULL
   THEN
 	 v_type := 9102;                  -- Inhouse  to Inv Adjustment (IHIA)
     v_reason := 50107;               -- Inhouse Missing Item
     v_comments := 'Reprocess Transaction from Inhouse ID: ' || p_transid;
      
       /*   
		gm_pkg_op_request_master.gm_sav_inhouse_txn(v_comments,2,v_reason,v_type,p_transid,p_type,NULL,NULL,p_userid,v_transid);
	      
    	gm_pkg_op_request_master.gm_sav_inhouse_txn_commastring(v_transid,p_writestr,p_userid);                                   
		
    	gm_pkg_op_trans_recon.gm_sav_ln_status(v_transid,'4',p_userid);
    	*/
        gm_cs_sav_loaner_recon_items (p_transid,
                                    p_writestr,
                                    'W',
                                    v_transid,
                                    p_userid
                                   );
   END IF;

   --
   IF p_retstr IS NOT NULL
   THEN
      -- if loaner type = "IH Item"
      v_type := 9132;                  -- Inhouse Reconcilation to repackaging (IRPN)
      v_reason := NVL(get_rule_value( 'TRANS_PURPOSE',v_type) ,'4140');               -- Inhouse Reconcilation to repackaging
   
      v_comments := 'Reprocess Transaction from Inhouse ID: ' || p_transid;
      
     /*      gm_sav_consignment(
					p_distributor_id,p_ship_date,p_return_date,p_comments,p_total_cost,
					p_status_fl,p_void_fl,p_account_id,p_ship_to,p_set_id,p_ship_to_id
					p_final_comments,p_inhouse_purpose,p_type,p_delivery_carrier,p_delivery_mode
					p_tracking_number,p_ship_req_fl,p_verify_fl,p_verified_date,p_verified_by,p_update_inv_fl
					p_reprocess_id,p_master_consignment_id,p_request_id,p_created_by,p_out_consignment_id
			)*/
     
      v_string := SUBSTR(p_retstr,0,INSTR(p_retstr,',',-1)-1);
      
     gm_pkg_op_request_master.gm_sav_consignment (	NULL, NULL, NULL, v_comments, NULL, '3', 
   															NULL, '01', 4123, NULL, p_userid, NULL,
															v_reason, v_type, NULL, NULL, NULL, '1', NULL, 
															NULL, NULL, NULL, p_transid, NULL, NULL, 
															p_userid, v_id
														);
               --
         gm_pkg_op_reprocess.gm_op_sav_rtn_reprocess_item (v_id, v_string, p_userid, v_errmsg);
     
     gm_cs_sav_loaner_recon_items (p_transid, p_retstr, 'R', v_id, p_userid);
     
      p_out := v_transid;
      
   END IF;
   
    --
   IF p_ihstr IS NOT NULL
   THEN
      -- if loaner type = "IH Item"
      v_type := 9131;                  -- IH Reconciliation to IH Shelf (IRIH)
      v_reason := NVL(get_rule_value( 'TRANS_PURPOSE',v_type) ,'50107');-- Ihnouse Missing Items Reconciliation
      v_comments := 'Reprocess Transaction from Inhouse ID: ' || p_transid;
      

      
      /*										p_comments,p_status_fl,p_purpose,p_type,p_refid,p_shipto,p_shiptoid,p_userid,p_txnid,       */
	gm_pkg_op_request_master.gm_sav_inhouse_txn(v_comments,3,v_reason,v_type,p_transid,p_type,NULL,NULL,p_userid,v_transid);

														/*p_txnid,p_str,p_userid*/
	gm_pkg_op_request_master.gm_sav_inhouse_txn_commastring(v_transid,p_ihstr,p_userid);
	
	gm_cs_sav_loaner_recon_items (p_transid, p_ihstr, 'R', v_transid, p_userid);
      p_out := v_transid;
   END IF;


   --
   SELECT   NVL (SUM (c413_item_qty), 0)
          - get_cs_loaner_recon_qty (p_transid, NULL)
     INTO v_qty
     FROM t413_inhouse_trans_items
    WHERE c412_inhouse_trans_id = p_transid 
      AND c413_status_fl = 'Q'
      AND c413_void_fl IS NULL;

   --
   IF v_qty = 0
   THEN
      UPDATE t412_inhouse_transactions
         SET c412_status_fl = 4,
             c412_verified_date = SYSDATE,
             c412_verified_by = p_userid,
             c412_last_updated_by = p_userid,
			 c412_last_updated_date = SYSDATE
       WHERE c412_inhouse_trans_id = p_transid;
   END IF;
 END gm_sav_inhouse_recon;
/*
 * Description: This Procedure is used to execute the Auto Reconciliation Job, for the Missing Loaner Transactions
 * Author:		ppandiyan 
 */ 
 PROCEDURE gm_do_process_loaner(
  p_transid          IN       t412_inhouse_transactions.c412_inhouse_trans_id%TYPE,
  p_ln_item_cur		 IN       TYPES.cursor_type,
  p_type             IN       VARCHAR2

)
AS

v_ord_item_cur	TYPES.cursor_type;


v_transid		T412_INHOUSE_TRANSACTIONS.C412_INHOUSE_TRANS_ID%TYPE;
v_pnum 			T413_INHOUSE_TRANS_ITEMS.C205_PART_NUMBER_ID%TYPE;
v_repid			T504A_LOANER_TRANSACTION.C703_SALES_REP_ID%TYPE;
v_accid 		T504A_LOANER_TRANSACTION.C704_ACCOUNT_ID%TYPE;
v_itemqty 		T413_INHOUSE_TRANS_ITEMS.C413_ITEM_QTY%TYPE;
v_itemprice 	T413_INHOUSE_TRANS_ITEMS.C413_ITEM_PRICE%TYPE;
v_cnum 			T413_INHOUSE_TRANS_ITEMS.C413_CONTROL_NUMBER%TYPE;
v_ordid 		T501_ORDER.C501_ORDER_ID%TYPE;
v_ord_pnum 		T502_ITEM_ORDER.C205_PART_NUMBER_ID%TYPE;
v_ord_qty 		T502_ITEM_ORDER.C502_ITEM_QTY%TYPE;
v_exit_val		NUMBER := 0;
v_ret_qty		NUMBER := 0;
v_loan_date     DATE;
v_ext_date      DATE;

v_rem_qty		T413_INHOUSE_TRANS_ITEMS.C413_ITEM_QTY%TYPE;
BEGIN

	LOOP
				FETCH p_ln_item_cur INTO v_pnum,v_repid,v_accid,v_itemqty,v_itemprice,v_cnum,v_loan_date,v_ext_date;
				EXIT
				WHEN p_ln_item_cur%NOTFOUND;
				v_rem_qty := v_itemqty;
				-- Load the Order Items that matching with the Combinations from part#, repid, accid in Missing Loaner txn
				gm_pkg_op_fetch_recon.gm_fch_notrec_do_items(v_pnum,v_accid,v_repid,v_ord_item_cur,v_loan_date,v_ext_date,p_type);			
				LOOP
					FETCH v_ord_item_cur INTO v_ordid,v_ord_pnum,v_ord_qty;
					EXIT
					WHEN v_ord_item_cur%NOTFOUND;
					-- Checking whether this order is returned or not
					v_ret_qty := gm_pkg_op_fetch_recon.gm_chk_returns(v_ordid,v_ord_pnum);
					v_ord_qty := v_ord_qty - v_ret_qty;
					IF v_ord_qty > 0 AND v_rem_qty > 0
					THEN
						-- Update the LN and DO process
						gm_pkg_op_process_recon.gm_process_ln(v_ordid,p_transid,v_ord_pnum,v_ord_qty,v_rem_qty,v_itemprice,v_cnum,null,null,'30301');
						gm_do_process(v_ordid,v_ord_pnum,'30301');
						-- Checks whether the LN is still remains with some Qty
						v_exit_val := gm_pkg_op_fetch_recon.get_missing_qty(v_ord_pnum,p_transid) - gm_pkg_op_fetch_recon.get_reconciled_qty(v_ord_pnum,p_transid);
						EXIT WHEN v_exit_val = 0;
						v_rem_qty := v_itemqty - gm_pkg_op_fetch_recon.get_reconciled_qty(v_ord_pnum,p_transid);
					END IF;
				END LOOP;				
			END LOOP;
			gm_ln_process(p_transid,'30301');
	
END gm_do_process_loaner;

END gm_pkg_op_process_recon;
/