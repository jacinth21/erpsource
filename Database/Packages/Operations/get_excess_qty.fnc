/* Formatted on 2007/07/27 19:22 (Formatter Plus v4.8.0) */
CREATE OR REPLACE FUNCTION get_op_fch_ra_excess_qty (
	p_ra_id 	t506_returns.c506_rma_id%TYPE
  , p_part_id	t505_item_consignment.c205_part_number_id%TYPE
)
	RETURN NUMBER
IS
	v_excess_qty   NUMBER;
BEGIN
	SELECT NVL (SUM (c505_item_qty), 0)
	  INTO v_excess_qty
	  FROM t504a_consignment_excess t504a, t505_item_consignment t505
	 WHERE t504a.c504_consignment_id = t505.c504_consignment_id
	   AND t504a.c506_rma_id IN (SELECT c506_rma_id
								   FROM t506_returns t506
								  WHERE t506.c506_rma_id = p_ra_id OR t506.c506_parent_rma_id = p_ra_id)
	   AND t505.c205_part_number_id = p_part_id;

	RETURN v_excess_qty;
EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		RETURN 0;
END get_op_fch_ra_excess_qty;
