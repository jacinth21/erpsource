CREATE OR REPLACE
PACKAGE BODY gm_pkg_op_inv_field_sales_tran
IS
    /***********************************************************************************
    *Description :  Main Procedure to adjust warehouse
    *
    **************************************************************************************/
PROCEDURE gm_update_fs_inv (
        p_trans_id     IN t5053_location_part_mapping.c5053_last_update_trans_id%TYPE,
        p_source_table IN VARCHAR2,
        p_ref_id       IN t5052_location_master.c5052_ref_id%TYPE,
        p_txn_type     IN t5053_location_part_mapping.c901_last_transaction_type%TYPE,
        p_action       IN t5053_location_part_mapping.c901_action%TYPE,
        p_type         IN t5053_location_part_mapping.c901_type%TYPE,
        p_user_id      IN t5052_location_master.c5052_created_by%TYPE,
        p_req_type     IN VARCHAR2 DEFAULT NULL)
AS
    v_location_id t5052_location_master.C5052_LOCATION_ID%TYPE;
    v_ware_house      VARCHAR2 (20) ;
    v_ware_house_type VARCHAR2 (20) ;
    v_company_id t1900_company.c1900_company_id%TYPE;
    v_trans_type VARCHAR2 (50) ;
BEGIN
     SELECT get_compid_frm_cntx () INTO v_company_id FROM DUAL;
    -- get the warehouse id and location type from rule for passing request type, if its null it will pick default
    -- warehouse 3 and location type 4000338 (Field Sales)
    v_ware_house      := get_rule_value (NVL (p_req_type, '4121'), 'RULEWH') ;
    v_ware_house_type := get_rule_value (NVL (p_req_type, '4121'), 'RULEWHTYPE') ;
    IF v_ware_house   IS NULL OR v_ware_house_type IS NULL THEN
        -- raise App error
        GM_RAISE_APPLICATION_ERROR ('-20999', '210', '') ;
        --
    END IF; -- end v_location_id
    IF p_source_table    = 'T505' THEN
        v_trans_type    := 'CONSIGNMENT';
    ELSIF p_source_table = 'T506' THEN
        v_trans_type    := 'RETURNS';
    ELSIF p_source_table = 'T501' THEN
        v_trans_type    := 'ORDER';
        -- Net Number change: ATEC 301
    ELSIF p_source_table = 'T412' THEN
        v_trans_type    := 'INHOUSE';
    END IF;
    v_company_id := get_transaction_company_id (v_company_id, p_trans_id, 'FIELDSALES_TRANS', v_trans_type) ; --To get
    -- the transaction based company id
    --fetch location id, if not creating new location id
    v_location_id := gm_pkg_op_inv_field_sales_tran.get_fs_location_id (v_ware_house, p_ref_id, v_ware_house_type,
    v_company_id) ;
    IF v_location_id IS NULL THEN
        -- create a new location for field sales or account warehouse
        -- 93310 (Active)
        gm_pkg_op_inv_field_sales_tran.gm_sav_fs_inv_location (v_ware_house, p_ref_id, NULL, v_ware_house_type, 93310,
        p_user_id, v_location_id) ;
    END IF; -- end v_location_id
    --Updating location id in master table  - Distributor and Account
    gm_pkg_op_inv_field_sales_tran.gm_update_fs_wh_inv_location (v_ware_house, v_location_id, p_ref_id,
    v_ware_house_type, p_user_id) ;
    IF p_source_table = 'T505' THEN
        gm_pkg_op_inv_field_sales_tran.gm_update_fs_inv_consign (p_trans_id, p_ref_id, v_location_id, p_txn_type,
        p_action, p_type, p_user_id) ;
    ELSIF p_source_table = 'T506' THEN
        gm_pkg_op_inv_field_sales_tran.gm_update_fs_inv_returns (p_trans_id, p_ref_id, v_location_id, p_txn_type,
        p_action, p_type, p_user_id) ;
    ELSIF p_source_table = 'T501' THEN
        gm_pkg_op_inv_field_sales_tran.gm_update_fs_inv_sales (p_trans_id, p_ref_id, v_location_id, p_txn_type,
        p_action, p_type, p_user_id) ;
    ELSIF p_source_table = 'FS_INV_ADJ' THEN
        gm_pkg_op_inv_field_sales_tran.gm_update_fs_inv_adjust (p_trans_id, p_ref_id, v_location_id, p_txn_type,
        p_action, p_type, p_user_id) ;
        -- Net Number change: ATEC 301
    ELSIF p_source_table = 'T412' THEN
        gm_pkg_op_inv_field_sales_tran.gm_update_fs_inv_inhouse (p_trans_id, p_ref_id, v_location_id, p_txn_type,
        p_action, p_type, p_user_id) ;
    ELSE
        GM_RAISE_APPLICATION_ERROR ('-20999', '209', '') ;
    END IF;
END gm_update_fs_inv;
/***********************************************************************************
*Description :  All consignment to Field sales will call this proc to adjust warehouse
*
**************************************************************************************/
PROCEDURE gm_update_fs_inv_consign (
        p_consignid   IN t505_item_consignment.c504_consignment_id%TYPE,
        p_ref_id      IN t5052_location_master.C5052_REF_ID%TYPE,
        p_location_id IN t5052_location_master.C5052_LOCATION_ID%TYPE,
        p_txn_type    IN t5053_location_part_mapping.c901_last_transaction_type%TYPE,
        p_action      IN t5053_location_part_mapping.c901_action%TYPE,
        p_type        IN t5053_location_part_mapping.c901_type%TYPE,
        p_user_id     IN t5052_location_master.c5052_created_by%TYPE)
AS
    V_SIGN NUMBER;
    v_location_id t5052_location_master.C5052_LOCATION_ID%TYPE;
    CURSOR cur_consign
    IS
         SELECT c205_part_number_id pnum, SUM (c505_item_qty) qty, c505_item_price price
          , c505_control_number cntrl_num
           FROM t505_item_consignment
          WHERE c504_consignment_id         = p_consignid
            AND TRIM (c505_control_number) IS NOT NULL
            AND c505_void_fl               IS NULL
       GROUP BY c205_part_number_id, c505_item_price, c505_control_number;
BEGIN
     SELECT DECODE (p_action, 4302, - 1, 4301, 1, 1) INTO v_sign FROM DUAL;
    FOR con_dtls IN cur_consign
    LOOP
        gm_sav_fs_inv_loc_part_details (p_location_id, p_consignid, con_dtls.pnum, (v_sign * con_dtls.qty), p_txn_type,
        p_action, p_type, CURRENT_DATE, p_user_id, con_dtls.cntrl_num) ;
        --
    END LOOP;
END gm_update_fs_inv_consign;
/***********************************************************************************
*Description :  All Returns from Field sales will call this proc to adjust warehouse
*
**************************************************************************************/
PROCEDURE gm_update_fs_inv_returns (
        p_rma_id      IN t507_returns_item.c506_rma_id%TYPE,
        p_ref_id      IN t5052_location_master.c5052_ref_id%TYPE,
        p_location_id IN t5052_location_master.c5052_location_id%TYPE,
        p_txn_type    IN t5053_location_part_mapping.c901_last_transaction_type%TYPE,
        p_action      IN t5053_location_part_mapping.c901_action%TYPE,
        p_type        IN t5053_location_part_mapping.c901_type%TYPE,
        p_user_id     IN t5052_location_master.c5052_created_by%TYPE)
AS
    v_sign NUMBER;
    CURSOR cur_returns
    IS
      -- Fetch Control number and pass it to the gm_sav_fs_inv_loc_part_details procedure
      -- PMT-14376
         SELECT c205_part_number_id pnum, SUM (c507_item_qty) qty, c507_item_price price
          , c901_type itype, c507_control_number cntrl_num
           FROM t507_returns_item
          WHERE c506_rma_id     = p_rma_id
            AND c507_status_fl IN ('C', 'W')
       GROUP BY c205_part_number_id, c507_item_price, c901_type,c507_control_number ;
BEGIN
     SELECT DECODE (p_action, 4302, - 1, 4301, 1, 1) INTO v_sign FROM DUAL;
    FOR rma_dtls IN cur_returns
    LOOP
        --
        GM_SAV_FS_INV_LOC_PART_DETAILS (P_LOCATION_ID, P_RMA_ID, RMA_DTLS.PNUM, (V_SIGN * RMA_DTLS.QTY), P_TXN_TYPE,
        p_action, p_type, CURRENT_DATE, p_user_id,rma_dtls.cntrl_num) ;
        --
    END LOOP;
END gm_update_fs_inv_returns;
/***********************************************************************************
*Description :  All Bill only orders from Consignment to Field sales will call this proc to adjust warehouse
*
**************************************************************************************/
PROCEDURE gm_update_fs_inv_sales (
        p_orderid     IN t502_item_order.c501_order_id%TYPE,
        p_ref_id      IN t5052_location_master.c5052_ref_id%TYPE,
        p_location_id IN t5052_location_master.c5052_location_id%TYPE,
        p_txn_type    IN t5053_location_part_mapping.c901_last_transaction_type%TYPE,
        p_action      IN t5053_location_part_mapping.c901_action%TYPE,
        p_type        IN t5053_location_part_mapping.c901_type%TYPE,
        p_user_id     IN t5052_location_master.c5052_created_by%TYPE)
AS
    v_sign NUMBER;
    CURSOR cur_sales
    IS
    -- Fetch Control number and pass it to the gm_sav_fs_inv_loc_part_details procedure
    -- PMT-14376
         SELECT t502.c205_part_number_id pnum, SUM (t502.c502_item_qty) qty, t502.c502_item_price price
          , t502.c901_type itype, NVL (t501.c901_order_type, '-9999') ordertype, t502.c502_control_number cntrl_num
           FROM t502_item_order t502, t501_order t501
          WHERE t502.c501_order_id   = p_orderid
            AND t502.c501_order_id   = t501.c501_order_id
            AND t502.C502_VOID_FL IS NULL 
            AND t502.c901_type = 50300 --Sales Consignment
       GROUP BY t502.c205_part_number_id, t502.c502_item_price, t502.c901_type
          , t501.c901_order_type,t502.c502_control_number ;
BEGIN
     SELECT DECODE (p_action, 4302, - 1, 4301, 1, 1) INTO v_sign FROM DUAL;
    FOR ord_dtls IN cur_sales
    LOOP
        --
        gm_sav_fs_inv_loc_part_details (p_location_id, p_orderid, ord_dtls.pnum, (v_sign * ord_dtls.qty), p_txn_type,
        p_action, p_type,
        CURRENT_DATE, p_user_id, ord_dtls.cntrl_num);
        --
    END LOOP;
END gm_update_fs_inv_sales;
/***********************************************************************************
*Description : All Rollforward Adjust and Country to country transfers will call this proc to adjust field sales
warehouse
*
**************************************************************************************/
PROCEDURE gm_update_fs_inv_adjust (
        p_trans_id    IN t8900_ous_acct_trans.c8900_ous_acct_trans_id%TYPE,
        p_ref_id      IN t5052_location_master.c5052_ref_id%TYPE,
        p_location_id IN t5052_location_master.c5052_location_id%TYPE,
        p_txn_type    IN t5053_location_part_mapping.c901_last_transaction_type%TYPE,
        p_action      IN t5053_location_part_mapping.c901_action%TYPE,
        p_type        IN t5053_location_part_mapping.c901_type%TYPE,
        p_user_id     IN t5052_location_master.c5052_created_by%TYPE)
AS
    v_sign NUMBER;
    CURSOR cur_adjust
    IS
         SELECT t8901.c205_part_number_id pnum, SUM (t8901.c8901_item_qty) qty, t8901.c8901_item_price price
           FROM t8900_ous_acct_trans t8900, t8901_ous_acct_trans_item t8901
          WHERE t8900.c8900_ous_acct_trans_id = t8901.c8900_ous_acct_trans_id
            AND t8900.c8900_ous_acct_trans_id = p_trans_id
            AND t8900.c8900_void_fl          IS NULL
       GROUP BY t8901.c205_part_number_id, t8901.c8901_item_price;
BEGIN
     SELECT DECODE (p_action, 4302, - 1, 4301, 1, 1) INTO v_sign FROM DUAL;
    FOR adj_dtls IN cur_adjust
    LOOP
        --
        GM_SAV_FS_INV_LOC_PART_DETAILS (P_LOCATION_ID, P_TRANS_ID, ADJ_DTLS.PNUM, (V_SIGN * ADJ_DTLS.QTY), P_TXN_TYPE,
        p_action, p_type, CURRENT_DATE, p_user_id) ;
        --
    END LOOP;
END gm_update_fs_inv_adjust;
/***********************************************************************************
*Description :  All Returned Loaners and consignment from Field sales will call this proc to adjust warehouse
warehouse
* ATEC 301 - Lot Net Number change new procedure July 2017
**************************************************************************************/
PROCEDURE gm_update_fs_inv_inhouse (
        p_trans_id    IN t505_item_consignment.c504_consignment_id%TYPE,
        p_ref_id      IN t5052_location_master.C5052_REF_ID%TYPE,
        p_location_id IN t5052_location_master.C5052_LOCATION_ID%TYPE,
        p_txn_type    IN t5053_location_part_mapping.c901_last_transaction_type%TYPE,
        p_action      IN t5053_location_part_mapping.c901_action%TYPE,
        p_type        IN t5053_location_part_mapping.c901_type%TYPE,
        p_user_id     IN t5052_location_master.c5052_created_by%TYPE)
AS
    v_sign NUMBER;
    v_adjustment_flag T906_RULES.C906_RULE_VALUE%TYPE;
    CURSOR cur_consign
    IS
         SELECT c205_part_number_id pnum, SUM (c413_item_qty) qty, c413_control_number cntrl_num
           FROM t413_inhouse_trans_items t413, t412_inhouse_transactions t412
          WHERE t413.c412_inhouse_trans_id       = p_trans_id
            AND TRIM (t413.c413_control_number) IS NOT NULL
            AND t413.c413_void_fl               IS NULL
            AND t412.c412_ref_id                 = p_ref_id
            AND t412.c412_inhouse_trans_id       = t413.c412_inhouse_trans_id
       GROUP BY t413.c205_part_number_id, t413.c413_item_qty, t413.c413_control_number;
BEGIN
    FOR con_dtls IN cur_consign
    LOOP
        v_adjustment_flag := get_rule_value (p_txn_type, 'LOT_ADJUSTMENT') ;
         SELECT DECODE (v_adjustment_flag, 'PLUS', 1, 'MINUS', - 1, 1)
           INTO v_sign
           FROM DUAL;
        gm_sav_fs_inv_loc_part_details (p_location_id, p_trans_id, con_dtls.pnum, (v_sign * con_dtls.qty), p_txn_type,
        p_action, p_type, CURRENT_DATE, p_user_id, con_dtls.cntrl_num) ;
    END LOOP;
END gm_update_fs_inv_inhouse;
/***********************************************************************************
*Description : Procedure to insert or update to t5052 location master table.
* Location ID cant be based on Company Field Sales ID is Unique across all the modules
* Code is modified as part of UK Data migration
**************************************************************************************/
PROCEDURE gm_sav_fs_inv_location (
        p_warehouseid IN T5051_INV_WAREHOUSE.C5051_INV_WAREHOUSE_ID%TYPE,
        p_ref_id      IN t5052_location_master.C5052_REF_ID%TYPE,
        p_location_cd IN t5052_location_master.c5052_location_cd%TYPE,
        p_type        IN t5052_location_master.c901_location_type%TYPE,
        p_status      IN t5052_location_master.c901_status%TYPE,
        p_user_id     IN t5052_location_master.c5052_created_by%TYPE,
        p_location_id OUT t5052_location_master.c5052_location_id%TYPE)
AS
    v_location_cnt NUMBER;
    v_location_id t5052_location_master.c5052_location_id%TYPE;
    v_company_id t1900_company.c1900_company_id%TYPE;
BEGIN
     SELECT get_compid_frm_cntx () INTO v_company_id FROM DUAL;
    IF v_company_id  IS NULL THEN
        v_company_id := get_rule_value ('DEFAULT_COMPANY', 'ONEPORTAL') ;
    END IF;
     SELECT COUNT (1)
       INTO v_location_cnt
       FROM t5052_location_master
      WHERE c5051_inv_warehouse_id = p_warehouseid
        AND c901_location_type     = p_type
        AND c5052_ref_id           = p_ref_id
        AND c5052_void_fl         IS NULL
        AND c1900_company_id       = NVL (get_distributor_company_id (p_ref_id), v_company_id) ;
    IF v_location_cnt             <> 0 THEN
        -- get the location id
        -- If no company code then get the context company
        -- Mainly for  Globus Medical Australia Pty, UK, SA (To be decided later)
         SELECT c5052_location_id
           INTO v_location_id
           FROM t5052_location_master
          WHERE c5051_inv_warehouse_id = p_warehouseid
            AND c901_location_type     = p_type
            AND c5052_ref_id           = p_ref_id
            AND c5052_void_fl         IS NULL
            AND c1900_company_id       = NVL (get_distributor_company_id (p_ref_id), v_company_id) ;
    END IF;
     UPDATE t5052_location_master
    SET c901_location_type    = p_type, c901_status = p_status, c5052_ref_id = p_ref_id
      , c5052_location_cd     = p_location_cd, c5052_last_updated_by = p_user_id, c5052_last_updated_date = CURRENT_DATE
      WHERE c5052_location_id = v_location_id
        AND c5052_void_fl    IS NULL;
    IF SQL%ROWCOUNT           = 0 THEN
         SELECT TO_CHAR (s5052_location_MASTER.nextval) INTO v_location_id FROM DUAL;
        -- If Consignment is for a distributor should pick distributor company id , added as part of EDC Plant shipment
         INSERT
           INTO t5052_location_master
            (
                c5052_location_id, C5052_REF_ID, c901_status
              , c5052_created_by, c5052_created_date, c901_location_type
              , c5051_inv_warehouse_id, c5052_location_cd, c1900_company_id
            )
            VALUES
            (
                v_location_id, p_ref_id, p_status
              , p_user_id, CURRENT_DATE, p_type
              , p_warehouseid, p_location_cd, DECODE (p_type, 4000338, NVL (get_distributor_company_id (p_ref_id),
                v_company_id), v_company_id)
            ) ;
    END IF;
    p_location_id := v_location_id;
END gm_sav_fs_inv_location;
/***********************************************************************************
*Description : Procedure to call from all transactions to update part details for field sales warehouse.
*
**************************************************************************************/
PROCEDURE gm_sav_fs_inv_loc_part_details
    (
        p_location_id IN t5052_location_master.c5052_location_id%TYPE,
        p_trans_id    IN t5053_location_part_mapping.c5053_last_update_trans_id%TYPE,
        p_part_num    IN t5053_location_part_mapping.c205_part_number_id%TYPE,
        p_qty         IN t5053_location_part_mapping.c5053_curr_qty%TYPE,
        p_txn_type    IN t5053_location_part_mapping.c901_last_transaction_type%TYPE,
        p_action      IN t5053_location_part_mapping.c901_action%TYPE,
        P_TYPE        IN T5053_LOCATION_PART_MAPPING.C901_TYPE%TYPE,
        p_created_dt  IN t5053_location_part_mapping.c5053_created_date%TYPE,
        P_USER_ID     IN T5052_LOCATION_MASTER.C5052_CREATED_BY%TYPE,
        p_control_no  IN t5053_location_part_mapping.c5053_last_txn_control_number%TYPE DEFAULT NULL
    )
AS
    v_part_map_id t5053_location_part_mapping.c5053_location_part_map_id%TYPE;
    v_cur_qty   NUMBER;
    v_total_qty NUMBER;
BEGIN
     UPDATE t5053_location_part_mapping
    SET C5053_CURR_QTY                = NVL (C5053_CURR_QTY, 0) + p_qty, C5053_LAST_UPDATE_TRANS_ID = p_trans_id,
        C901_LAST_TRANSACTION_TYPE    = p_txn_type, c901_action = p_action, c901_type = p_type
      , c5053_last_updated_by         = p_user_id, c5053_last_updated_date = NVL (p_created_dt, CURRENT_DATE),
        c5053_last_txn_control_number = p_control_no
      WHERE c5052_location_id         = p_location_id
        AND c205_part_number_id       = p_part_num;
    IF SQL%ROWCOUNT                   = 0 THEN
         SELECT s5053_location_part_mapping.NEXTVAL INTO v_part_map_id FROM DUAL;
         INSERT
           INTO t5053_location_part_mapping
            (
                c5053_location_part_map_id, c5052_location_id, c205_part_number_id
              , C5053_CURR_QTY, c5053_last_update_trans_id, c901_last_transaction_type
              , c901_action, c901_type, c5053_created_by
              , c5053_created_date, C5053_LAST_TXN_CONTROL_NUMBER
            )
            VALUES
            (
                V_PART_MAP_ID, P_LOCATION_ID, P_PART_NUM
              , NVL (p_qty, 0), p_trans_id, p_txn_type
              , P_ACTION, P_TYPE, P_USER_ID
              , NVL (p_created_dt, CURRENT_DATE), p_control_no
            ) ;
    END IF;
END gm_sav_fs_inv_loc_part_details;
/***********************************************************************************
*Description :  Updating location id in master table  - Distributor and Account
*
**************************************************************************************/
PROCEDURE gm_sav_loaner_lot
    (
        p_trans_id IN t5053_location_part_mapping.c5053_last_update_trans_id%TYPE,
        p_user_id  IN t5052_location_master.c5052_created_by%TYPE
    )
AS
    v_ref_id t412_inhouse_transactions.c412_ref_id%TYPE;
    v_trans_type T412_inhouse_transactions.c412_type%TYPE;
    v_set_type t504_consignment.c504_type%TYPE;
    v_user_id t101_user.c101_user_id%TYPE;
BEGIN
    BEGIN
         SELECT c412_type, c412_ref_id
           INTO v_trans_type, v_ref_id
           FROM T412_inhouse_transactions
          WHERE c412_inhouse_trans_id = p_trans_id;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_trans_type := NULL;
        v_ref_id     := NULL;
    END;
    BEGIN
         SELECT c504_type
           INTO v_set_type
           FROM t504_consignment
          WHERE c504_consignment_id = v_ref_id;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_set_type := NULL;
    END;
    
    IF (v_set_type='4127' or v_set_type='4119') THEN
    
    gm_update_fs_inv (p_trans_id, 'T412', v_ref_id, v_trans_type, NULL, v_set_type, p_user_id, v_set_type) ;
    
    END IF;
END gm_sav_loaner_lot;
/***********************************************************************************
*Description :  Updating location id in master table  - Distributor and Account
*
**************************************************************************************/
PROCEDURE gm_update_fs_wh_inv_location (
        p_warehouseid   IN T5051_INV_WAREHOUSE.C5051_INV_WAREHOUSE_ID%TYPE,
        p_location_id   IN t5052_location_master.C5052_LOCATION_ID%TYPE,
        p_ref_id        IN t5052_location_master.C5052_REF_ID%TYPE,
        p_location_type IN t5052_location_master.c901_location_type%TYPE,
        p_user_id       IN t5052_location_master.c5052_created_by%TYPE)
AS
BEGIN
    IF p_warehouseid = '3' THEN --Distributor warehouse
         UPDATE t701_distributor
        SET c5052_location_id       = p_location_id, c701_last_updated_by = p_user_id, c701_last_updated_date = CURRENT_DATE
          WHERE c701_distributor_id = p_ref_id
            AND c701_void_fl       IS NULL;
    ELSIF p_warehouseid             = '5' THEN --Account warehouse
         UPDATE t704_account
        SET c5052_location_id   = p_location_id, c704_last_updated_by = p_user_id, c704_last_updated_date = CURRENT_DATE
          WHERE c704_account_id = p_ref_id
            AND c704_void_fl   IS NULL;
    END IF;
END gm_update_fs_wh_inv_location;
/***********************************************************************************
*Description : Function used to get the field sales location id.
*
**************************************************************************************/
FUNCTION get_fs_location_id (
        p_warehouseid IN T5051_INV_WAREHOUSE.C5051_INV_WAREHOUSE_ID%TYPE,
        p_ref_id      IN t5052_location_master.C5052_REF_ID%TYPE,
        p_type        IN t5052_location_master.c901_location_type%TYPE,
        p_company_id  IN t1900_company.c1900_company_id%TYPE)
    RETURN VARCHAR2
IS
    v_location_id t5052_location_master.C5052_LOCATION_ID%TYPE;
    v_company_id t1900_company.c1900_company_id%TYPE;
BEGIN
    BEGIN
        --Getting company id
        IF p_company_id IS NULL THEN
             SELECT NVL (get_compid_frm_cntx (), get_rule_value ('DEFAULT_COMPANY', 'ONEPORTAL'))
               INTO v_company_id
               FROM DUAL;
        END IF;
        -- get the location id where
        -- Should have a unique record in Ref ID and Type combination
        -- Ref ID Holds distributor ID and should be unique
         SELECT c5052_location_id
           INTO v_location_id
           FROM t5052_location_master
          WHERE c5051_inv_warehouse_id = p_warehouseid
            AND c5052_ref_id           = p_ref_id
            AND c901_location_type     = p_type
            AND c5052_void_fl         IS NULL
            AND c1900_company_id       = DECODE (p_type, 4000338, NVL (get_distributor_company_id (p_ref_id),
            v_company_id), v_company_id) ;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_location_id := NULL;
    END;
    RETURN v_location_id;
END get_fs_location_id;
END gm_pkg_op_inv_field_sales_tran;
/
