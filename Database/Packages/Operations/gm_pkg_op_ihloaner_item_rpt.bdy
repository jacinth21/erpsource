-- @"c:\database\packages\Operations\gm_pkg_op_ihloaner_item_rpt.bdy";
CREATE OR REPLACE
PACKAGE BODY gm_pkg_op_ihloaner_item_rpt
IS
    
	/**************************************************************************
    * Description	: Procedure to Fetch the HeaderInfo for Reconfigure Request
    * Author		: Jignesh Shah
    ***************************************************************************/
	
	PROCEDURE gm_fch_ihitem_dtl (
		p_req_id	 IN		t412_inhouse_transactions.c412_inhouse_trans_id%TYPE
	  , p_req_dtl	 OUT	TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_req_dtl FOR
			SELECT T413.C205_Part_Number_Id pnum
					, Get_Partnum_Desc(T413.C205_Part_Number_Id) pdesc
					, t413.c413_item_qty iqty
					, t413.c413_item_price iprice
			FROM t412_inhouse_transactions t412, t413_inhouse_trans_items t413
			WHERE T412.C412_Inhouse_Trans_Id = T413.C412_Inhouse_Trans_Id
			AND t412.c412_inhouse_trans_id = p_req_id
			AND t412.c412_void_fl IS null
			AND t413.c413_void_fl is null;
	END gm_fch_ihitem_dtl;
	
	/*****************************************************************
    * Description	: Procedure to fetch replenishment transactions
    * Author		: Gopinathan
    ******************************************************************/
	PROCEDURE gm_fch_replenishment_txn (
		p_ref_id	 IN		t412_inhouse_transactions.c412_inhouse_trans_id%TYPE
	  , p_out_dtl	 OUT	TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_out_dtl FOR
			SELECT     t413.c205_part_number_id pnum
		                 , t413.c413_item_qty qty
		                 , t412.c412_inhouse_trans_id refid
		                 , DECODE (t412.c412_status_fl, '2', 'Open', '3', 'Pending Verification', 'Verified') status
		                 , get_user_name (t412.c412_created_by) createdby
		                 , t412.c412_created_date createddate
		    FROM
		        (SELECT     t412.c412_inhouse_trans_id, t412.c412_status_fl, t412.c412_created_by ,t412.c412_created_date
		            FROM t412_inhouse_transactions t412
		            WHERE t412.c412_ref_id IN
		                (SELECT     t412.c412_inhouse_trans_id
		                    FROM t412_inhouse_transactions t412
		                    WHERE t412.c412_ref_id = p_ref_id
		                        AND t412.c412_type = 1006483
		                )
		                AND t412.c412_type NOT IN (9117) --- Void txns
		                AND t412.c412_void_fl IS NULL
		        UNION ALL
		        SELECT     t412.c412_inhouse_trans_id, t412.c412_status_fl, t412.c412_created_by ,t412.c412_created_date
		            FROM t412_inhouse_transactions t412
		            WHERE t412.c412_ref_id = p_ref_id        --Ref ID
		                AND t412.c412_type NOT IN (1006483) -- IHRQ not included
		                AND t412.c412_void_fl IS NULL
		        ) t412, t413_inhouse_trans_items t413
		    WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
		        AND t413.c413_void_fl IS NULL
		    Order By Status, T412.C412_Inhouse_Trans_Id;
	END gm_fch_replenishment_txn;
	/*****************************************************************
    * Description	: Procedure to fetch child request detail also it will pull only for IHRQ and IHLN not shipped.
    * Author		: Gopinathan
    ******************************************************************/
	PROCEDURE gm_fch_child_request (
		p_req_id	 IN		t525_product_request.C525_Product_Request_Id%TYPE
	  , p_txn_id	 IN		t504_consignment.c504_consignment_id%TYPE
	  , p_txn_type   IN 	t504_consignment.c901_ref_type%TYPE
	  , p_out_dtl	 OUT	TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_out_dtl FOR
		      SELECT     t413.c205_part_number_id PNUM, get_partnum_desc (t413.c205_part_number_id) pdesc, t413.c413_item_qty CRQTY
				      , t412.c412_inhouse_trans_id REQID, 'Allocated' STATUS_FL                            , '20' STATUS
				    FROM t412_inhouse_transactions t412 , t413_inhouse_trans_items t413
				    WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
				        AND t412.c412_status_fl IN ('0','3')
				        AND t412.c412_type = 1006483
				        AND t412.c412_void_fl IS NULL
				        AND t413.c413_item_qty > 0
				        AND t412.c412_ref_id IN
				        (SELECT     t504.c504_ref_id
				            FROM t504_consignment t504
				            WHERE t504.c504_consignment_id = p_txn_id
				                AND t504.c504_void_fl IS NULL
				        )
				    UNION ALL
				    SELECT     t505.c205_part_number_id PNUM, get_partnum_desc (t505.c205_part_number_id) pdesc, t505.c505_item_qty
				            CRQTY                           , t504.c504_consignment_id REQID                   , 'Allocated' STATUS_FL
				          , '20' STATUS
				        FROM t907_shipping_info t907, t504_consignment t504, t505_item_consignment t505
				        WHERE t907.c907_ref_id = t504.c504_consignment_id
				            AND t504.c504_consignment_id = t505.c504_consignment_id
				            AND t504.c901_ref_type = 4119
				            AND t504.c504_ref_id =
				            (SELECT     t504.c504_ref_id
				                FROM t504_consignment t504
				                WHERE t504.c504_consignment_id = p_txn_id
				                    AND t504.c504_void_fl IS NULL
				            )
				            AND t505.c505_item_qty > 0
				            AND t907.c907_status_fl != '40'
				            AND t504.c504_type = 9110
				            AND t504.c504_void_fl IS NULL
				            AND t907.c907_void_fl IS NULL;  
	END gm_fch_child_request;
    /*****************************************************************
    * Description	: function is used to get IHLN to update email notifcation status 
    * Author		: Gopinathan
    ******************************************************************/	
	FUNCTION get_ref_consignid (
         p_ref_id    IN   t526_product_request_detail.C525_PRODUCT_REQUEST_ID%TYPE
       , p_pnum      IN   t526_product_request_detail.C205_PART_NUMBER_ID%TYPE
       , p_type      IN   t504_consignment.c504_type%TYPE 
       , p_verify_fl IN   CHAR
     )
     RETURN VARCHAR2
     IS
     	v_consign_id         VARCHAR2 (20);
     BEGIN
            SELECT t504.c504_consignment_id  INTO v_consign_id
            FROM t504_consignment t504, t505_item_consignment t505 
            WHERE t504.c504_consignment_id= t505.c504_consignment_id
              AND t504.c504_ref_id = p_ref_id 
              AND t505.c205_part_number_id = p_pnum
              AND NVL(t504.c504_verify_fl,'0') = DECODE(p_verify_fl,'Y','1','0') 
              AND t504.c504_type = p_type
	      	  AND t505.c505_void_fl IS NULL
	          AND t504.c504_void_fl IS NULL; 
	      --
	      RETURN v_consign_id;
	      --
	EXCEPTION
	WHEN OTHERS
	THEN
		RETURN '';
     END get_ref_consignid;

    /*****************************************************************
    * Description	: function is used to get IHLN or its associated transaction 
    *				depending on the need status	
    * Author		: Richard.K
    ******************************************************************/	
	FUNCTION get_ref_inln_info (
         p_ref_id    IN   t526_product_request_detail.c525_product_request_id%TYPE
       , p_pnum      IN   t526_product_request_detail.c205_part_number_id%TYPE
       , p_type      IN   t504_consignment.c504_type%TYPE 
     )
     RETURN VARCHAR2
     IS
     	v_txn_id        VARCHAR2 (4000) := '';
		v_prod_req_id	t526_product_request_detail.c525_product_request_id%TYPE; 
     BEGIN
		SELECT t504.c504_consignment_id  INTO v_txn_id
          FROM t504_consignment t504, t505_item_consignment t505 
        WHERE t504.c504_consignment_id= t505.c504_consignment_id
          AND t504.c504_ref_id = p_ref_id 
          AND t505.c205_part_number_id = p_pnum
          AND t504.c504_type = p_type
          AND t505.c505_void_fl IS NULL
          AND t504.c504_void_fl IS NULL;
		--
		RETURN v_txn_id ;
		--
	  EXCEPTION
	  WHEN OTHERS
	  THEN
			BEGIN 
				SELECT  t526.c525_product_request_id 
				INTO	v_prod_req_id
				FROM    t526_product_request_detail t526
				WHERE   t526.c526_product_request_detail_id = p_ref_id;

				SELECT gm_pkg_sm_eventbook_rpt.get_open_txn (v_prod_req_id, p_pnum)   
				  INTO v_txn_id 
				  FROM DUAL;
				--
				RETURN v_txn_id;
				--
			EXCEPTION
			WHEN OTHERS
			THEN
			--
				RETURN '';
			--	
			END;
     END get_ref_inln_info;


    /*****************************************************************
    * Description	: function is used to get RA# for IHLN 
    * Author		: Gopinathan
    ******************************************************************/	
	FUNCTION get_item_return_id(
         p_ref_id    IN   T506_Returns.C506_Ref_Id%TYPE,
         p_ref_type  IN   T506_Returns.C901_Ref_Type%TYPE
     )
     RETURN VARCHAR2
     IS
     	v_return_id         VARCHAR2 (20);
     BEGIN
            BEGIN
				 SELECT C506_Rma_Id INTO v_return_id
				   FROM T506_Returns
				  WHERE C506_Ref_Id   = p_ref_id
				    AND C901_Ref_Type = p_ref_type
				    AND C506_Void_Fl IS NULL;
	            EXCEPTION
	            WHEN OTHERS
	            THEN
	                  RETURN '';
            END;
     RETURN v_return_id;
     END get_item_return_id;  
     /*****************************************************************
    * Description	: function is used to get cn ID 
    * Author		: Aprasath
    ******************************************************************/	
	FUNCTION get_back_order_consign_id(
         p_ref_id    IN   T412_Inhouse_Transactions.C412_Inhouse_Trans_Id%TYPE,
         p_ref_type  IN   t504_consignment.c504_type%TYPE
         
     )
     RETURN VARCHAR2
     IS
     	v_consign_id         VARCHAR2 (20);
     	v_ref_type           T412_Inhouse_Transactions.C901_Ref_Type%TYPE;
     BEGIN
	     
        BEGIN
	        SELECT C901_Ref_Type INTO v_ref_type  
                                 FROM T412_Inhouse_Transactions
                                WHERE C412_Inhouse_Trans_Id = p_ref_id
                                  AND C412_Void_Fl         IS NULL ;        
                  
            IF v_ref_type = '1006572' THEN --ISBO    
	            IF p_ref_type = '1006573' OR p_ref_type = '1006570' OR p_ref_type = '50150' THEN --IHIS OR BLIS OR FGISneed to get the CN id	    		
		    		 SELECT C412_Ref_Id into v_consign_id 
						   FROM T412_Inhouse_Transactions
						  WHERE C412_Type              = 1006572
						    AND C412_Void_Fl          IS NULL
						    AND C412_Inhouse_Trans_Id IN
						    (
						         SELECT C412_Ref_Id
						           FROM T412_Inhouse_Transactions
						          WHERE C412_Inhouse_Trans_Id = p_ref_id
						            AND C412_Void_Fl         IS NULL
						            AND C412_Type             = p_ref_type
						    ) ; 	    		
	    		ELSE
			    	v_consign_id := p_ref_id;			     
	    		END IF;    
      		ELSE--ISBO 
            	RETURN p_ref_id;
        	END IF;

      	EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               v_consign_id := p_ref_id;
        END;
	  
     RETURN v_consign_id;
   END get_back_order_consign_id; 
    /*****************************************************************
    * Description	: Function to fetch the IHLNs created when verified the FGIH and BLIH
    * Author		: Rajkumar
    ******************************************************************/	
     FUNCTION get_associated_ihln(
     	p_ref_id    T412_INHOUSE_TRANSACTIONS.C412_INHOUSE_TRANS_ID%TYPE
     ) RETURN VARCHAR2
     IS
     	v_req_id	t525_product_request.c525_product_request_id%TYPE;
     	v_txns		VARCHAR2(4000):='';
    	v_txn_id	VARCHAR2(20) :='';
    	v_contains 	VARCHAR2(20) :='';
     	CURSOR v_part_cur
     	IS 
     		SELECT C205_PART_NUMBER_ID PNUM
			   FROM T413_INHOUSE_TRANS_ITEMS
			  WHERE C412_INHOUSE_TRANS_ID = p_ref_id
			    AND C413_VOID_FL         IS NULL;
			 
     BEGIN
	     
	     -- Fetching the Product Request ID for the created FGIH or BLIH
	      BEGIN
                 SELECT T525.C525_PRODUCT_REQUEST_ID INTO v_req_id
                   FROM T412_INHOUSE_TRANSACTIONS T412, 
                        T412_INHOUSE_TRANSACTIONS T412B, 
                        T525_PRODUCT_REQUEST T525
                  WHERE T525.C525_PRODUCT_REQUEST_ID = T412B.C412_REF_ID
                    AND T412B.C412_INHOUSE_TRANS_ID  = T412.C412_REF_ID
                    AND T412.C412_INHOUSE_TRANS_ID   = p_ref_id
                    AND T412.C412_VOID_FL           IS NULL
                    AND T412B.C412_VOID_FL          IS NULL
                    AND T525.C525_VOID_FL           IS NULL;
            EXCEPTION WHEN NO_DATA_FOUND
            THEN
                BEGIN
                    SELECT T525.C525_PRODUCT_REQUEST_ID INTO v_req_id
                       FROM T412_INHOUSE_TRANSACTIONS T412, 
                                    T525_PRODUCT_REQUEST T525
                      WHERE T525.C525_PRODUCT_REQUEST_ID = T412.C412_REF_ID
                        AND T412.C412_INHOUSE_TRANS_ID   = p_ref_id
                        AND T412.C412_VOID_FL           IS NULL
                        AND T525.C525_VOID_FL           IS NULL;
                EXCEPTION WHEN NO_DATA_FOUND
                THEN
                    v_req_id := NULL;
                END ;
            END ;
            
            IF v_req_id IS NOT NULL
            THEN
            	FOR v_cur IN v_part_cur
            	LOOP
            		v_txn_id := NVL(gm_pkg_sm_loaner_item_alloc.get_consignid_bsd_shipdtl(v_req_id,'4119',v_cur.pnum),'');
            		IF v_txn_id IS NOT NULL
            		THEN
            			v_contains := NVL(INSTR(v_txns,v_txn_id),0);
            			IF v_contains = 0
            			THEN
            				v_txns := v_txns || v_txn_id || ',' ;
            			END IF;
            		END IF;
            	END LOOP;
            END IF;
            
            RETURN v_txns;
            
	 END get_associated_ihln;
	 
	  /*
      * Description	: Function to fetch the IHLN's Event Name
      * Author		: Rajkumar
      */
     FUNCTION GET_IHLN_EVENT_NAME(
     	p_ref_id    IN   T506_Returns.C506_Ref_Id%TYPE
     ) RETURN VARCHAR2
     IS
     	v_event_name	VARCHAR2(2000) := '';
     	v_reqid			T525_PRODUCT_REQUEST.C525_PRODUCT_REQUEST_ID%TYPE;
     BEGIN
	 	BEGIN
		 	SELECT C504_REF_ID INTO v_reqid 
		 		FROM T504_CONSIGNMENT 
		 		WHERE C504_CONSIGNMENT_ID = p_ref_id
		 			AND C504_VOID_FL IS NULL;
		EXCEPTION WHEN NO_DATA_FOUND
		THEN
			v_reqid := NULL;
		END;
		IF v_reqid IS NOT NULL
		THEN
			v_event_name := gm_pkg_sm_eventbook_txn.get_event_name(v_reqid);
		END IF;
		
		RETURN v_event_name;
	 END GET_IHLN_EVENT_NAME;
	 
   /***************************************************************************************************
    * Description: Function to fetch the RA ID created when Ship out the IHLN Transaction [For NSP-495]
    * Author: HReddi 
    ***************************************************************************************************/	
     FUNCTION get_associated_ra(
     	p_ref_id    T412_INHOUSE_TRANSACTIONS.C412_INHOUSE_TRANS_ID%TYPE
     ) RETURN VARCHAR2
     IS     
    	v_ra_id	VARCHAR2(20) :='';     	
     BEGIN
	     BEGIN
		     SELECT c506_rma_id INTO v_ra_id 
		     FROM t506_returns 
		     WHERE c506_ref_id = p_ref_id 
		     	AND c506_void_fl IS NULL  ;
		     EXCEPTION WHEN NO_DATA_FOUND
			 THEN
				v_ra_id := NULL;
			END;   
     		RETURN v_ra_id;            
	 END get_associated_ra;
END gm_pkg_op_ihloaner_item_rpt;
/	