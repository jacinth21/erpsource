
CREATE OR REPLACE
PACKAGE BODY gm_pkg_op_return_txn
IS
/******************************************************************
* Description : Procedure for Save Return Item/Set Information PMT-13474
****************************************************************/
PROCEDURE gm_op_sav_ra_ict_return (
        p_txn_id  IN t504_consignment.c504_consignment_id%TYPE,
        p_user_id IN t504_consignment.c504_created_by%TYPE,
        p_rma_id OUT t506_returns.c506_rma_id%TYPE)
AS
    v_qty  VARCHAR2 (4000) ;
    v_pnum VARCHAR2 (4000) ;
    v_dist_id t701_distributor.c701_distributor_id%TYPE;
    v_ra_dist_id t701_distributor.c701_distributor_id%TYPE;
    v_expdate VARCHAR2 (20) ;
    v_rma_id t506_returns.c506_rma_id%TYPE;
    v_company_id t1900_company.c1900_company_id%TYPE;
    v_parent_comp_id t1900_company.c1900_company_id%TYPE;
    v_plant_id t5040_plant_master.c5040_plant_id%TYPE;
    v_set_id t504_consignment.C207_SET_ID%TYPE;
    v_inputstr CLOB;
    v_err_fl VARCHAR2 (2000) ;
    v_ra_type t506_returns.c506_type%TYPE;
    v_count NUMBER;
    v_price NUMBER;
    CURSOR v_Inhouse_div
    IS
         SELECT c505_item_qty itemqty, c205_part_number_id partnum, c505_control_number ctrl
          , c505_item_price price
           FROM t505_item_consignment
          WHERE c504_consignment_id = p_txn_id
            AND c505_void_fl       IS NULL;
BEGIN
    BEGIN
         SELECT c701_distributor_id, TO_CHAR (TRUNC (SYSDATE + 2), NVL (get_compdtfmt_frm_cntx (), 'mm/dd/yyyy')),
            C207_SET_ID, NVL2 (C207_SET_ID, 3307, 26240177)
           INTO v_dist_id, v_expdate, v_set_id
          , v_ra_type
           FROM t504_consignment
          WHERE c504_consignment_id = p_txn_id
            AND c504_void_fl       IS NULL;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN;
    END;
    
    v_company_id := get_compid_frm_cntx () ;
    BEGIN
         SELECT t701.C1900_PARENT_COMPANY_ID, t5041.c5040_plant_id
              , get_rule_value_by_company(C1900_PARENT_COMPANY_ID,'PRIMARY-DIST',v_company_id)
           INTO v_parent_comp_id, v_plant_id, v_ra_dist_id
           FROM t701_distributor t701, t5041_plant_company_mapping t5041
          WHERE t701.C1900_PARENT_COMPANY_ID = t5041.c1900_company_id
            AND t5041.c5041_primary_fl       = 'Y'
            AND t5041.c5041_void_fl         IS NULL
            AND c701_distributor_id          = v_dist_id
            AND C701_VOID_FL                IS NULL;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        raise_application_error ('-20537', '') ; -- invalid distributor id.
    END;
    
    v_qty     := '';
    v_pnum    := '';
    
    FOR v_cur IN v_Inhouse_div
    LOOP
        v_qty      := v_qty||v_cur.itemqty||',';
        v_pnum     := v_pnum||v_cur.partnum||',';
        
        IF v_ra_type = 26240177
        THEN
        	v_price := NVL(get_account_part_pricing (NULL, v_cur.partnum, get_distributor_inter_party_id (v_ra_dist_id)),0);

			UPDATE t505_item_consignment
			   SET c505_item_price = v_price
			 WHERE c205_part_number_id = v_cur.partnum 
			   AND c504_consignment_id = p_txn_id;
        ELSE
        	v_price    := v_cur.price;
        END IF;
        
        v_inputstr := v_inputstr||v_cur.partnum||','||v_cur.itemqty||','||v_cur.ctrl||', ,,'||v_price||',0,0,|';
        --INPUTSTRING=124.000,2,BGH255CD, ,,235,0,0,|124.456,2,BGG215NE, ,,2294,0,0,|
    END LOOP;
    
    gm_pkg_op_return.gm_op_sav_return (v_ra_dist_id, v_expdate, 3313, -- Consignment Return --reason
	    p_user_id, --USERID
	    v_ra_type, --consignment set
	    NULL, --ACCOUNT ID
	    v_qty, v_pnum, v_set_id, --set
	    v_rma_id) ;
    
    gm_pkg_op_return.GM_OP_SAV_RETURN_ACCP ( v_rma_id, v_inputstr, v_expdate, '0', p_user_id, v_err_fl) ;
    
    --update t506_return
     UPDATE t506_returns
        SET c506_ref_id          = p_txn_id, c901_ref_type = '26240144', --Return to Owner Company
            c506_return_date     = NULL, c1900_company_id = v_parent_comp_id, c5040_plant_id = v_plant_id
          , c506_last_updated_by = p_user_id, c506_last_updated_date = sysdate
      WHERE c506_rma_id      = v_rma_id;
      
    --Return Stock in Tag Report Screen
     SELECT COUNT (1)
       INTO v_count
       FROM t5010_tag t5010
      WHERE t5010.c5010_last_updated_trans_id = p_txn_id;
      
    IF (v_count                               > 0) THEN
         UPDATE t5010_tag
            SET C901_STATUS                 = '26240201', --Returned Stock:26240201
                c901_location_type          = 40033, --Inhouse
                c5010_location_id           = 52099, --Return
                c901_sub_location_type 		= NULL,
                c5010_sub_location_id 		= NULL,
                c5010_last_updated_trans_id = v_rma_id, c5010_last_updated_by = p_user_id, c5010_last_updated_date =
            SYSDATE
          WHERE c5010_last_updated_trans_id = p_txn_id;
    END IF;
    
    p_rma_id := v_rma_id;
END gm_op_sav_ra_ict_return;

/**********************************************************************
   * Description : This Procedure will update the RA Flag for the Request.
   * Author 	 : Rajeshwaran.v
  **********************************************************************/
  
	PROCEDURE gm_sav_return_fl_for_req (
			p_req_id     IN      t520_request.C520_REQUEST_ID%TYPE
		  , p_ra_fl      IN      t520_request.C520_RA_FL%TYPE
		  , p_user_id    IN      t520_request.C520_CREATED_BY%TYPE
		)
		
	AS
	v_count 	   NUMBER;
	BEGIN
		
		-- As RA is required for this Request updating the RQ with the RA Flag.
		IF (p_ra_fl ='Y')
		THEN
			UPDATE 	T520_REQUEST 
			SET 	C520_RA_FL='Y',
				    C520_LAST_UPDATED_BY = p_user_id,
				    C520_LAST_UPDATED_DATE = CURRENT_DATE
			WHERE 	C520_REQUEST_ID= p_req_id;
			
		  SELECT COUNT (1)
		  INTO v_count
		  FROM T520_REQUEST WHERE 	c520_master_request_id = p_req_id;
			
		 	 IF (v_count > 0)
				THEN
				UPDATE 	T520_REQUEST 
					SET 	C520_RA_FL='Y',
				    C520_LAST_UPDATED_BY = p_user_id,
				    C520_LAST_UPDATED_DATE = CURRENT_DATE
					WHERE 	c520_master_request_id = p_req_id;
			END IF;
		END IF;
		
	END gm_sav_return_fl_for_req;

/**********************************************************************
   * Description : This Procedure will create GM-RA for the Item Request that 
                   needs Returns. The Return ID will be updated back in the Request table.
   * Author 	 : Rajeshwaran.v
  **********************************************************************/
  
	PROCEDURE gm_sav_return_from_req (
			p_req_id     IN      t520_request.C520_REQUEST_ID%TYPE
		  , p_user_id    IN      t520_request.C520_CREATED_BY%TYPE
		)
		
	AS
	v_ra_fl 		t520_request.C520_RA_FL%TYPE;
	v_ra_id  		t506_Returns.c506_rma_id%TYPE;
	v_return_days 	NUMBER;
	v_tmp_ret_days	NUMBER := 7; -- One Week.
	v_req_for  		t520_request.C520_REQUEST_FOR%TYPE;
	v_req_purpose 	t520_request.C901_PURPOSE%TYPE;
	v_distributor	t520_request.C520_REQUEST_TO%TYPE;
	v_req_status	T520_REQUEST.C520_STATUS_FL%TYPE;
	v_cn_id			T504_CONSIGNMENT.C504_CONSIGNMENT_ID%TYPE;
	v_return_date	VARCHAR2(100);
	v_ra_type		T506_RETURNS.c506_type%TYPE;
	v_ra_comments	T506_RETURNS.C506_COMMENTS%TYPE;
	v_rep_id        VARCHAR2(20);
	v_rep_count		NUMBER;
	v_account_id    T704_ACCOUNT.C704_ACCOUNT_ID%TYPE;
	v_request_to    T520_REQUEST.C520_REQUEST_TO%TYPE;
	v_ra_reason     VARCHAR2(100);

	
	BEGIN
		
		-- Get the RA Fl and RA ID from the Request.
		BEGIN
			SELECT  C520_RA_FL,C520_REQUEST_FOR,C901_PURPOSE, C520_REQUEST_TO, C520_STATUS_FL, C506_Rma_Id
			INTO     v_ra_fl, v_req_for,v_req_purpose, v_request_to, v_req_status,v_ra_id
			FROM t520_request
			WHERE C520_REQUEST_ID = p_req_id;
		EXCEPTION WHEN NO_DATA_FOUND
		THEN
			v_ra_fl := '';
		END;
		
		BEGIN
			SELECT C504_CONSIGNMENT_ID
			INTO v_cn_id
			FROM T504_CONSIGNMENT
			WHERE C520_REQUEST_ID =p_req_id;
			
		EXCEPTION WHEN NO_DATA_FOUND
		THEN
			v_cn_id :='';
		END;
		
		-- Based on the RQ Purpose the type of RA will be different.
		SELECT 	DECODE(v_req_purpose,50060,3302,4130,3302,4000097, 3307, 50061, 3307)
		INTO   	v_ra_type
		FROM 	DUAL;
		
		-- As RA is required for this Request and system has not created a RA, 
		-- need to create RA for this scenario.
		--v_ra_id IS NULL condition Added for PMT-29882(to avoid duplicate RA creation)
		IF (v_ra_fl ='Y' AND v_cn_id IS NOT NULL AND v_ra_id IS NULL)
		THEN
		
			-- Expected Return days retrieved from Rules.
			SELECT NVL(GET_RULE_VALUE_BY_COMPANY('EXPECTED_DAYS','RA_FRM_REQ',get_compid_frm_cntx()),
					   GET_RULE_VALUE('EXPECTED_DAYS','RA_FRM_REQ'))
			INTO   v_return_days
			FROM DUAL;
			
			--RA Comments from Rules.
			SELECT  REPLACE(NVL(GET_RULE_VALUE_BY_COMPANY('RA_COMMENTS','RA_FRM_REQ',get_compid_frm_cntx()),
					   GET_RULE_VALUE('RA_COMMENTS','RA_FRM_REQ')),'#GMCNID#',v_cn_id)
			INTO   v_ra_comments
			FROM DUAL;
			
			IF v_return_days IS NULL
			THEN
				v_return_days := v_tmp_ret_days;
			END IF;
			
			SELECT COUNT(1) INTO v_rep_count
			FROM T703_SALES_REP
			WHERE C703_VOID_FL IS NULL
			AND C701_DISTRIBUTOR_ID = v_request_to;
			
			-- If the distribtor is having only one sales rep,then use the rep to create the RA.
			IF v_rep_count=1
			THEN
				SELECT C703_SALES_REP_ID
				INTO v_rep_id
				FROM T703_SALES_REP
				WHERE C703_VOID_FL IS NULL
				AND C701_DISTRIBUTOR_ID = v_request_to;
				
			END IF;
			
			--For IH Consignment type the rep ID should be 01, as per the code written in the below procedure. 
			IF (v_req_for=40022)			
			THEN
				v_rep_id := '01';				
			END IF; 
			
			IF (v_req_for=40025) 
			THEN
				v_ra_type := '3304';
				v_ra_reason := '3320'; 
				v_account_id := v_request_to;
			ELSE
				v_distributor := v_request_to;
				v_ra_reason := '3313';
				v_account_id := v_rep_id;
			END IF;

			
			GM_PKG_OP_RETURN.GM_OP_SAV_RETURN(v_distributor,to_char(CURRENT_DATE+v_return_days,get_compdtfmt_frm_cntx()),v_ra_reason,p_user_id,
											  v_ra_type, v_account_id --Sales rep id or Account id
											  ,'','', '', v_ra_id);
			
			
			INSERT INTO t507_returns_item
						(c507_returns_item_id, c506_rma_id, c205_part_number_id, c507_item_qty, c507_item_price
					   , c507_status_fl,C507_CONTROL_NUMBER
						)
				 select s507_return_item.NEXTVAL, v_ra_id, c205_part_number_id,C505_ITEM_QTY, C505_ITEM_PRICE
					   , 'Q', C505_CONTROL_NUMBER
				FROM 	T505_ITEM_CONSIGNMENT
				WHERE 	C504_CONSIGNMENT_ID = v_cn_id
				AND 	C505_VOID_FL IS NULL;

			INSERT INTO t507_returns_item
						(c507_returns_item_id, c506_rma_id, c205_part_number_id, c507_item_qty, c507_item_price
					   , c507_status_fl,C507_CONTROL_NUMBER
						)
				 select s507_return_item.NEXTVAL, v_ra_id, c205_part_number_id,C505_ITEM_QTY, C505_ITEM_PRICE
					   , 'R', C505_CONTROL_NUMBER
				FROM 	T505_ITEM_CONSIGNMENT
				WHERE 	C504_CONSIGNMENT_ID = v_cn_id
				AND 	C505_VOID_FL IS NULL;
						
			UPDATE T506_RETURNS 
			SET  C506_COMMENTS = DECODE(v_cn_id,'','',v_ra_comments)
			WHERE C506_RMA_ID = v_ra_id;
			
			UPDATE 	t520_request 
			SET 	C520_RA_FL='Y',C506_RMA_ID= NVL(v_ra_id,C506_RMA_ID)
			WHERE 	C520_REQUEST_ID= p_req_id;	
			
			
		END IF;		
		
	END gm_sav_return_from_req;
	
/**********************************************************************
   * Description : This Procedure will get RA packslip comment
   * Author 	 : Mahavishnu.k
  **********************************************************************/
  	
	PROCEDURE gm_fch_CN_packslip_RAcomment (
			p_consign_id     IN      T504_CONSIGNMENT.C504_CONSIGNMENT_ID%TYPE
		  , p_ra_comments  OUT VARCHAR2
		)
		AS 
		v_rq_id      t520_request.C520_REQUEST_ID%TYPE;
		v_ra_fl      t520_request.C520_RA_FL%TYPE;
		v_rma_id     t520_request.c506_rma_id%TYPE;
		v_ra_comments VARCHAR2(500);
		v_exp_date   VARCHAR2(30);
		v_datefmt	VARCHAR2(30);
		v_rule_id  VARCHAR2(30);
		
		BEGIN
				
			BEGIN
				SELECT 	T504.C520_REQUEST_ID,T520.C520_RA_FL,T520.C506_RMA_ID
				INTO v_rq_id,v_ra_fl,v_rma_id
			  	 FROM t504_consignment T504,t520_request T520 
			  	 WHERE T504.C504_CONSIGNMENT_ID=p_consign_id AND
			   	T504.C520_REQUEST_ID = T520.C520_REQUEST_ID AND
			   	T520.C520_VOID_FL IS NULL AND
			   	T504.C504_VOID_FL IS NULL;
			EXCEPTION WHEN NO_DATA_FOUND
			THEN
				v_rq_id :='';
				v_ra_fl :='';
				v_rma_id :='';
			END;
						
			IF(v_ra_fl = 'Y' AND v_rma_id IS NOT NULL) THEN
			
				BEGIN
					SELECT GET_RULE_VALUE(get_compid_frm_cntx(),'MULTILINGUAL') INTO v_rule_id FROM DUAL;
					EXCEPTION WHEN NO_DATA_FOUND
					THEN	
					v_rule_id :='';
				END;
			
				BEGIN
					
					SELECT DECODE(get_compid_frm_cntx(),v_rule_id,'YYYY/MM/DD',NVL(get_compdtfmt_frm_cntx(),'MM/DD/YYYY'))  INTO v_datefmt FROM DUAL;	
							 
					SELECT TO_CHAR (C506_EXPECTED_DATE, v_datefmt) INTO v_exp_date FROM t506_returns WHERE  C506_RMA_ID=v_rma_id AND C506_VOID_FL IS NULL;
					EXCEPTION WHEN NO_DATA_FOUND
					THEN
					v_exp_date :='';
				END;
				SELECT  REPLACE(NVL(GET_RULE_VALUE_BY_COMPANY('RA_COMMENTS_PACKSLIP','RA_FRM_REQ',get_compid_frm_cntx()),
					   GET_RULE_VALUE('RA_COMMENTS_PACKSLIP','RA_FRM_REQ')),'#GMRADATE#',v_exp_date)
					INTO   v_ra_comments
					FROM DUAL;
			
					SELECT  REPLACE(v_ra_comments,'#GMRAID#',v_rma_id) INTO   v_ra_comments
					FROM DUAL;
				p_ra_comments := v_ra_comments;
				
			END IF;
		END gm_fch_CN_packslip_RAcomment;
		
/***********************************************
Procedure to update the BBA donor id and BBA company id when a part is returned from Globus to BBA
Author: gpalani
PMT:19790 
***********************************************/
PROCEDURE gm_update_bba_donor_mapping (
p_part_num      IN      t205_part_number.C205_PART_NUMBER_ID%TYPE,
p_control_num   IN      t2550_part_control_number.C2550_CONTROL_NUMBER%TYPE,
p_company_id    IN      t1900_company.C1900_COMPANY_ID%TYPE,
p_plant_id      IN      t5040_plant_master.C5040_PLANT_ID%TYPE,
p_user_id       IN      t2550_part_control_number.C2550_LAST_UPDATED_BY%TYPE)

AS 
v_donor_id     t2540_donor_master.c2540_donor_id%TYPE;
v_donor_no     t2540_donor_master.c2540_donor_number%TYPE;
v_bba_donor_id t2540_donor_master.c2540_donor_id%TYPE;
v_part_comp_id t1900_company.c1900_company_id%TYPE;

BEGIN

-- Below statement doesnt have company check since there will be only one record for a lot number.
-- Fetching the Current donor id and company id 
	BEGIN
		
        SELECT t2550.c2540_donor_id, t2550.c1900_company_id
           INTO v_donor_id, v_part_comp_id
         FROM t2550_part_control_number t2550
         WHERE t2550.c2550_control_number = p_control_num
         AND t2550.c205_part_number_id  = p_part_num;
    
   EXCEPTION WHEN OTHERS THEN
    v_donor_id:=NULL;
    v_part_comp_id:=NULL;
      
    END;
 
 -- Below condition will not satify if the return is from Globus, for returns from normal customers this will get returned from the procedure.
    
    IF(v_part_comp_id=p_company_id) THEN
    
    RETURN;
    
    END IF;  
    
       -- Fetching the donor number using the system assigned donor id.
     BEGIN
        
         SELECT t2540.c2540_donor_number
          INTO v_donor_no
         FROM t2540_donor_master t2540
         WHERE t2540.c2540_donor_id      = v_donor_id
         AND t2540.c1900_company_id    = v_part_comp_id
         AND t2540.c2540_void_fl      IS NULL;
         
         EXCEPTION WHEN OTHERS THEN
    		v_donor_no:=NULL;
    		
    	END;  
    
    -- Fetching the BBA donor ID for the donor number
    BEGIN
    
		 SELECT t2540.c2540_donor_id
   			INTO v_bba_donor_id
   		FROM t2540_donor_master t2540
  		WHERE t2540.c2540_donor_number = v_donor_no
    	AND t2540.c1900_company_id   = p_company_id
    	AND t2540.c2540_void_fl     IS NULL;
    	
     EXCEPTION WHEN OTHERS THEN
    		v_bba_donor_id:=NULL;
    		
    	END;   
    	
		 UPDATE t2550_part_control_number
			  SET c1900_company_id       = NVL(p_company_id ,c1900_company_id),
			    c5040_plant_id           = NVL(p_plant_id ,c5040_plant_id),
			    c2550_last_updated_by    = p_user_id ,
			    c2550_last_updated_date  = CURRENT_DATE,
			    c2540_donor_id=NVL(v_bba_donor_id,c2540_donor_id)
			  WHERE c2550_control_number = p_control_num
			  AND c205_part_number_id    =p_part_num
			  AND c1900_company_id       =v_part_comp_id; -- Changed the hardcode value from 1000 to v_part_comp_id	 
			  
END gm_update_bba_donor_mapping;


END gm_pkg_op_return_txn;
/
