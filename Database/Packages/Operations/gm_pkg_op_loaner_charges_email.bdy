CREATE OR REPLACE PACKAGE BODY gm_pkg_op_loaner_charges_email
IS

	/***************************************************************
	* Description : Procedure to fetch summary of charge detail ids
	* Author    : smanimaran 
	****************************************************************/
	PROCEDURE gm_fch_charge_hr_email_summary(
		p_charge_dtl_ids   IN CLOB,
		p_rep_category      IN  t701_distributor.c901_distributor_type%TYPE,
		p_charge_status 	IN      t9201_charge_details.c9201_status%TYPE,
		p_out_cursor OUT    TYPES.cursor_type)
	AS
	BEGIN

	my_context.set_my_inlist_ctx (p_charge_dtl_ids);

	IF  p_rep_category   =  70100 
	THEN         --(distributorrep )
	
	BEGIN
	OPEN 
	p_out_cursor 
	  FOR

		SELECT t701.c701_distributor_name distributorname,  t703rep.c703_sales_rep_name repname, t703assrep.c703_sales_rep_name assrepname,
		SUM(t9201.c9201_amount) amount
		FROM t9200_incident t9200,
		t9201_charge_details t9201,
		t703_sales_rep t703rep,
		t703_sales_rep t703assrep,
		t701_distributor t701
		WHERE T9200.C9200_Incident_Id = T9201.C9200_Incident_Id
		AND t9201.c703_sales_rep_id = t703rep.c703_sales_rep_id
		AND t9201.c703_ass_rep_id        = t703assrep.c703_sales_rep_id(+)
		AND t703rep.c701_distributor_id = t701.c701_distributor_id 
		AND T9200.C9200_Void_Fl      IS NULL
		AND T9200.C901_Incident_Type   = '92068'
		AND t9201.c9201_status  IN (p_charge_status)         --  20-submit
		AND t9201.c701_distributor_id IN ( SELECT c701_distributor_id FROM t701_distributor WHERE c901_distributor_type = p_rep_category)
		AND t9201.c9201_void_fl    IS NULL
		AND t703rep.c703_void_fl IS NULL
        AND t701.c701_void_fl IS NULL
		AND t9201.c9201_charge_details_id 
		IN (SELECT token FROM  v_in_list)
		GROUP BY t703rep.c703_sales_rep_name,t703assrep.c703_sales_rep_name,t701.c701_distributor_name
		ORDER BY t701.c701_distributor_name,t703rep.c703_sales_rep_name,t703assrep.c703_sales_rep_name;
	END;
	ELSIF p_rep_category  = 70101  
	THEN --direct rep
	BEGIN
	OPEN
	p_out_cursor 
		FOR

		SELECT t703.c703_sales_rep_name repname,
		SUM(t9201.c9201_amount) amount
		FROM t9200_incident t9200,
		t9201_charge_details t9201,
		t703_sales_rep t703
		WHERE T9200.C9200_Incident_Id = T9201.C9200_Incident_Id
		AND t9201.c703_sales_rep_id = t703.c703_sales_rep_id
		AND T9200.C9200_Void_Fl      IS NULL
		AND T9200.C901_Incident_Type   = '92068'
		AND t9201.c9201_status    IN (p_charge_status) 
		AND t9201.c701_distributor_id IN ( SELECT c701_distributor_id FROM t701_distributor WHERE c901_distributor_type = p_rep_category)
		AND t9201.c9201_void_fl    IS NULL
		AND t703.c703_void_fl IS NULL
		AND t9201.c9201_charge_details_id 
		IN (SELECT token FROM  v_in_list)
		GROUP BY t703.c703_sales_rep_name
		ORDER BY t703.c703_sales_rep_name;
		END;
		END IF;
	END gm_fch_charge_hr_email_summary;
	
	/***************************************************************
	* Description : Procedure to fetch details of charge detail ids
	* Author    : smanimaran
	****************************************************************/
	PROCEDURE gm_fch_charge_hr_email_detail (
		p_charge_dtl_ids   IN CLOB,
		p_rep_category     IN   t701_distributor.c901_distributor_type%TYPE,
		p_charge_status IN      t9201_charge_details.c9201_status%TYPE,
		p_out_cursor OUT    TYPES.cursor_type)
		AS
		v_datefmt varchar(10);
	BEGIN
	
	select NVL(get_compdtfmt_frm_cntx(),get_rule_value('DATEFMT','DATEFORMAT')) into  v_datefmt from dual;

			my_context.set_my_inlist_ctx (p_charge_dtl_ids);
			
			OPEN 
			    p_out_cursor 
			  FOR
			
			SELECT t701.c701_distributor_name distributorname,
			t703rep.c703_sales_rep_name repname,
			t703assrep.c703_sales_rep_name assrepname,
			t525.c525_product_request_id requestid,
			Get_Set_Name (T526.C207_Set_Id) setname,
			to_char(t9200.c9200_incident_date,v_datefmt) incidentdate,
			get_code_name (t9200.c901_incident_type) incidenttype,
			to_char(T504a.C504a_Loaner_Dt,v_datefmt) loaneddate ,
			to_char(T504a.C504a_Expected_Return_Dt,v_datefmt) expectedreturndate ,
			T504b.C504a_Etch_Id||'/'||T504a.C504_Consignment_Id consignid,
			to_char(T504a.C504A_RETURN_DT,v_datefmt) actualreturndate,
			T504b.C504a_Etch_Id etchid,
			gm_pkg_op_loaner_charges.Get_Work_Days_Count (T504a.C504a_Expected_Return_Dt,  NVL (T504a.C504a_Return_Dt, CURRENT_DATE)) daysoverdue,
			t9201.c9201_amount amount
			FROM t9200_incident t9200,
			  t9201_charge_details t9201,
			  t703_sales_rep t703rep,
			  t703_sales_rep t703assrep,
			  t701_distributor t701,
			  t525_product_request t525,
			  T526_Product_Request_Detail T526, 
			      (SELECT    T504b.C504a_Loaner_Transaction_Id ,
			    T504b.C526_Product_Request_Detail_Id ,
			    T504b.C504a_Loaner_Dt ,
			    T504b.C504a_Expected_Return_Dt ,
			    T504b.C504_Consignment_Id ,
			    T504b.C504A_RETURN_DT
			  FROM T504a_Loaner_Transaction T504b
			  WHERE T504b.C504a_Loaner_Transaction_Id = 
			    (SELECT MAX(T504a.C504a_Loaner_Transaction_Id)
			    FROM T504a_Loaner_Transaction T504a
			    WHERE T504a.C526_Product_Request_Detail_Id =
			      T504b.C526_Product_Request_Detail_Id
			    AND T504a.C504a_Void_Fl IS NULL
			    GROUP BY C526_Product_Request_Detail_Id
			    )
			  AND T504b.C504a_Void_Fl IS NULL
			  ) T504a
			 ,T504a_Consignment_Loaner T504b
			WHERE T9200.C9200_Incident_Id      = T9201.C9200_Incident_Id
			AND t9201.c703_sales_rep_id        = t703rep.c703_sales_rep_id
			AND t9201.c703_ass_rep_id        = t703assrep.c703_sales_rep_id(+)
			AND t703rep.c701_distributor_id = t701.c701_distributor_id 
			AND T9200.C9200_Ref_Id =T504a.C526_Product_Request_Detail_Id
			AND T9200.C9200_Ref_Id = T526.C526_Product_Request_Detail_Id
			AND t526.c525_product_request_id = t525.c525_product_request_id
			AND T504a.C504_Consignment_Id = T504b.C504_Consignment_Id
			AND T504b.C504a_Void_Fl IS NULL
			AND T525.C525_Void_Fl       IS NULL
			AND T526.C526_Void_Fl       IS NULL
			AND T9200.C9200_Void_Fl           IS NULL
			AND T9200.C901_Incident_Type       = '92068'
			AND t9201.c9201_status            IN (p_charge_status) 
			AND t9201.c701_distributor_id IN ( SELECT c701_distributor_id FROM t701_distributor WHERE c901_distributor_type = p_rep_category)
			AND t9201.c9201_void_fl           IS NULL
			AND t703rep.c703_void_fl IS NULL
        	AND t701.c701_void_fl IS NULL
			AND t9201.c9201_charge_details_id IN (SELECT token FROM  v_in_list)
			order BY t701.c701_distributor_name,t703rep.c703_sales_rep_name,t703assrep.c703_sales_rep_name;
	END gm_fch_charge_hr_email_detail;
	
END gm_pkg_op_loaner_charges_email;
/


