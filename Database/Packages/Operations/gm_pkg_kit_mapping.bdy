CREATE OR REPLACE PACKAGE BODY gm_pkg_kit_mapping
IS
/* *******************************************************************************
 * Author: Agilan singaravelan
 * Procedure:gm_fch_kit_name
 * Description:Fetch kit name
 ***********************************************************************************/ 
 PROCEDURE gm_fch_kit_name (
 	  p_kit_nm IN T2078_KIT_MAPPING.C2078_KIT_NM%TYPE
 	, p_kit_data OUT varchar
  )
  AS
  v_count NUMBER;
  BEGIN
	  SELECT count(*) 
	    INTO v_count 
	    FROM T2078_KIT_MAPPING 
	   WHERE C2078_KIT_NM = p_kit_nm
	     --AND C901_TYPE = 107920   -- Sales for Portal
	     AND C2078_VOID_FL IS NULL;

  		IF (v_count = 0) THEN
  			p_kit_data := 'N';
  		ELSE
  			p_kit_data := 'Y';
  		END IF;
 
  END gm_fch_kit_name;
  
 /* *******************************************************************************
 * Author: Agilan singaravelan
 * Procedure:gm_fetch_kit_details
 * Description:Fetch kit mapped Details based on Kit name list
 ***********************************************************************************/  
 PROCEDURE gm_fetch_kit_details(
 	  p_kit_id IN T2078_KIT_MAPPING.C2078_KIT_MAP_ID%TYPE
 	, p_kit_data OUT TYPES.cursor_type
  )
   AS
     BEGIN
	      OPEN p_kit_data
           FOR
           
			SELECT t2078.C2078_KIT_MAP_ID kitid,
  				   t2078.C2078_KIT_NM kitnm,
  				   t2078.C2078_ACTIVE_FL activefl,
  				   T2079.setid,
  				   T2079.setnm,
  				   T2079.tagid
			  FROM T2078_KIT_MAPPING t2078,
  		   (SELECT t2079.C2078_KIT_MAP_ID,t2079.C207_SET_ID setid,
    			   get_set_name(t2079.C207_SET_ID) setnm ,
    			   t2079.C5010_TAG_ID tagid
              FROM T2079_KIT_MAP_DETAILS t2079
  			 WHERE t2079.C2078_KIT_MAP_ID = p_kit_id
  			   AND t2079.C2079_VOID_FL IS NULL
  				   ) T2079
			 WHERE t2078.C2078_KIT_MAP_ID = p_kit_id
			  -- AND C901_TYPE = 107920   -- Sales for Portal
			   AND t2078.C2078_KIT_MAP_ID   = t2079.C2078_KIT_MAP_ID(+)
			   AND t2078.C2078_VOID_FL IS NULL;

	  END  gm_fetch_kit_details;
	     
/* *******************************************************************************
 * Author: Agilan singaravelan
 * Procedure:gm_fetch_set_details
 * Description:Fetch set details based on set selection
 ***********************************************************************************/   
 PROCEDURE  gm_fetch_set_details (
    p_set_id 		IN t207_set_master.c207_set_id%TYPE
   ,p_out_cursor 	OUT TYPES.cursor_type)
 AS
  v_company_id t1900_company.c1900_company_id%TYPE;
 BEGIN
	 
	  SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL'))
  	    INTO v_company_id 
	    FROM dual;
		   
	 OPEN p_out_cursor
	 FOR
		 SELECT T207.C207_SET_ID SETID,
		 	    T207.C207_SET_NM SETDESC,
		 	    get_code_name(T207.C207_TYPE) SETYPE,
		 	    get_code_name(T207.C901_SET_GRP_TYPE) SETTYPE,
		 	    get_code_name(T207.C901_STATUS_ID) SETSTATUS
		   FROM T207_SET_MASTER T207 , T2080_SET_COMPANY_MAPPING T2080 
		  WHERE T207.C207_SET_ID = p_set_id
		    AND T207.C207_SET_ID = T2080.C207_SET_ID
		    AND T2080.C1900_COMPANY_ID = v_company_id
		    AND T207.C207_VOID_FL IS NULL
		    AND T2080.C2080_VOID_FL IS NULL;
		    
END gm_fetch_set_details;
 /* *******************************************************************************
 * Author: Agilan singaravelan
 * Procedure:gm_fch_tag_num
 * Description:To check tag number is valid or not
 ***********************************************************************************/ 
 PROCEDURE gm_fch_tag_num (
      p_tag_num IN t5010_tag.C5010_TAG_ID%TYPE
   	, p_set_id  IN t207_set_master.c207_set_id%TYPE
 	, p_tag_val OUT varchar
 	, p_kit_id  IN T2078_KIT_MAPPING.C2078_KIT_MAP_ID%TYPE
  )
  AS
  v_count NUMBER;
  v_tag_number varchar2(100);
  v_tag_count NUMBER;
  v_company_id    t907_shipping_info.c1900_company_id%TYPE;
  v_plant_id      t907_shipping_info.c5040_plant_id%TYPE;
  BEGIN
	  
	  	  	-- getting the company and plant details
	 	SELECT NVL (GET_COMPID_FRM_CNTX (), GET_RULE_VALUE ('DEFAULT_COMPANY', 'ONEPORTAL')), NVL (GET_PLANTID_FRM_CNTX (),
		    GET_RULE_VALUE ('DEFAULT_PLANT', 'ONEPORTAL'))
		   INTO v_company_id, v_plant_id
		   FROM DUAL;	
		   
	  SELECT LPAD(p_tag_num,'7','0')  --LPAD will append zero before the tag number
	    INTO v_tag_number 
	    FROM dual;
	  
	  SELECT count(*) 
	    INTO v_count 
	    FROM t5010_tag 
	   WHERE C5010_TAG_ID = v_tag_number 
	     AND c5010_void_fl is null;
--	     AND c1900_company_id = v_company_id -- comment the code since other company tag also using. 
--	     AND c5040_plant_id = v_plant_id;
  
  		IF (v_count = 0) THEN
 			 p_tag_val := 'N';
  		ELSE
  			 p_tag_val := 'Y';
 END IF;

 IF (p_tag_val = 'Y') THEN
      SELECT count(*)
	    INTO v_tag_count
	    FROM T2079_KIT_MAP_DETAILS
	   WHERE C5010_TAG_ID = v_tag_number
	     AND c2078_kit_map_id != nvl(p_kit_id,c2078_kit_map_id)
	     AND c2079_void_fl is null;
	        
	IF (v_tag_count = 0) THEN
 		p_tag_val := 'Y';
  	ELSE
  		p_tag_val := 'NN';
    END IF;
 END IF;
 
  END gm_fch_tag_num;
  
  /* *******************************************************************************
 * Author: Agilan singaravelan
 * Procedure:gm_fch_tag_number
 * Description:To check tag number is valid or not
 ***********************************************************************************/ 
 PROCEDURE gm_fch_tag_number (
      p_tag_num IN t5010_tag.C5010_TAG_ID%TYPE
   	, p_set_id  IN t207_set_master.c207_set_id%TYPE
 	, p_tag_number OUT varchar
 	, p_set_id_value OUT varchar
 	, p_tag_id_asso OUT varchar
 	, p_kit_id IN T2078_KIT_MAPPING.C2078_KIT_MAP_ID%type
  )
  AS
  v_count NUMBER;
  v_tag_number varchar2(100);
  v_std_setid_str VARCHAR2 (4000) := p_set_id;
  v_substring     VARCHAR2 (4000) ;
  v_string     VARCHAR2 (4000) ;
  v_set_id t207_set_master.c207_set_id%TYPE;
  v_asso_set_id   t207_set_master.c207_set_id%TYPE;
  v_asso_set_id_val VARCHAR2 (4000) ;
  v_std_tagid_str VARCHAR2 (4000) := p_tag_num;
  v_substring_tag     VARCHAR2 (4000) ;
  v_string_tag     VARCHAR2 (4000) ;
  v_tag_id t5010_tag.C5010_TAG_ID%TYPE;
  v_tag_type t5010_tag.c901_inventory_type%type;
  v_set_val VARCHAR2 (4000) := NULL;
  v_tag_val VARCHAR2 (4000);
  v_tag_num VARCHAR2 (4000);
  v_tag_count NUMBER;
  v_company_id    t907_shipping_info.c1900_company_id%TYPE;
  v_plant_id      t907_shipping_info.c5040_plant_id%TYPE;
  v_kit_id     VARCHAR2 (4000);
  v_kit_id_val   VARCHAR2 (4000);
  BEGIN

	  	-- getting the company and plant details
	 	SELECT NVL (GET_COMPID_FRM_CNTX (), GET_RULE_VALUE ('DEFAULT_COMPANY', 'ONEPORTAL')), NVL (GET_PLANTID_FRM_CNTX (),
		        GET_RULE_VALUE ('DEFAULT_PLANT', 'ONEPORTAL'))
		   INTO v_company_id, v_plant_id
		   FROM DUAL;		   
	

	  WHILE INSTR (v_std_setid_str, '|') <> 0
        	LOOP         
            	v_substring := SUBSTR (v_std_setid_str, 1, INSTR (v_std_setid_str, '|') - 1) ;
            	v_std_setid_str    := SUBSTR (v_std_setid_str, INSTR (v_std_setid_str, '|')    + 1) ;
            	v_set_id    := NULL;
            	v_set_id    := TRIM (v_substring) ;
            	
            	v_substring_tag := SUBSTR (v_std_tagid_str, 1, INSTR (v_std_tagid_str, '|') - 1) ;
            	v_std_tagid_str    := SUBSTR (v_std_tagid_str, INSTR (v_std_tagid_str, '|')    + 1) ;
            	v_tag_id    := NULL;
            	v_tag_id    := TRIM (v_substring_tag) ;
            	
	    SELECT LPAD(v_tag_id,'7','0') INTO v_tag_number FROM dual;
 		
	 BEGIN

	    SELECT c901_inventory_type
	      INTO v_tag_type
	      FROM t5010_tag
	     WHERE c5010_tag_id=v_tag_number
	       AND c207_set_id = v_set_id
	       AND c5010_void_fl is null;
--	       AND c1900_company_id = v_company_id -- comment the code since other company tag also using.
--	       AND c5040_plant_id = v_plant_id;
	      EXCEPTION WHEN NO_DATA_FOUND THEN
			v_tag_type := NULL;  
			END;
	    IF (v_tag_type <>'10003') THEN
	    	v_set_val := v_set_val ||','||v_set_id; 
	    END IF;
	 
	  SELECT count(*) 
	    INTO v_count 
	    FROM t5010_tag t5010 
	   WHERE t5010.C5010_TAG_ID = v_tag_number 
	     AND t5010.c207_set_id = v_set_id 
	     AND c5010_void_fl is null;
	          
	     IF (v_count = 0) THEN			
	     SELECT c207_set_id 
	       INTO v_asso_set_id
	       FROM t5010_tag t5010 
	      WHERE t5010.C5010_TAG_ID = v_tag_number 
	        AND c5010_void_fl is null;
	     
 				v_tag_val :=  v_tag_val ||','|| v_tag_number;
 				v_asso_set_id_val := v_asso_set_id_val || ',' || v_asso_set_id;	    
  		END IF;  
  		
  	IF (p_kit_id is null) THEN
  	 BEGIN
  	    SELECT count(0), T2078.C2078_KIT_NM
	      INTO v_tag_count, v_kit_id
	      FROM T2079_KIT_MAP_DETAILS T2079, T2078_KIT_MAPPING T2078
	     WHERE C5010_TAG_ID = v_tag_number
	       AND T2079.C2078_KIT_MAP_ID = T2078.C2078_KIT_MAP_ID
	       --AND C901_TYPE = 107920
           AND C2078_VOID_FL IS NULL
	       AND c2079_void_fl is null group by T2078.C2078_KIT_NM;
	     
	     EXCEPTION WHEN NO_DATA_FOUND THEN
			v_tag_count := 0;  
	 END;
  	ELSE 
  	  BEGIN
  	    SELECT count(0),T2078.C2078_KIT_NM
	      INTO v_tag_count, v_kit_id
	      FROM T2079_KIT_MAP_DETAILS T2079, T2078_KIT_MAPPING T2078
	     WHERE C5010_TAG_ID = v_tag_number
	       AND T2079.c2078_kit_map_id <> nvl(p_kit_id,T2079.c2078_kit_map_id)
	       AND T2079.C2078_KIT_MAP_ID = T2078.C2078_KIT_MAP_ID
	        --AND C901_TYPE = 107920
	       AND C2078_VOID_FL IS NULL
	       AND c2079_void_fl is null  group by T2078.C2078_KIT_NM;
	     
	       EXCEPTION WHEN NO_DATA_FOUND THEN
			v_tag_count := 0;  
	   END;
	 END IF;

	    IF (v_tag_count <> 0) THEN	  
 			 v_tag_num :=  v_tag_num ||','|| v_tag_number;
 			 v_kit_id_val := v_kit_id_val || ',' || v_kit_id;
  		END IF;  
 END LOOP; 

	p_tag_number := v_tag_val || '/' || v_asso_set_id_val;
	p_set_id_value := v_set_val;
	p_tag_id_asso := v_tag_num || '/' || v_kit_id_val;
END gm_fch_tag_number;
   
 /* *******************************************************************************
 * Author: Agilan singaravelan
 * Procedure:gm_sav_kit_details
 * Description:Save the kit mapping details
 ***********************************************************************************/ 
     PROCEDURE gm_sav_kit_details (
      p_kit_nm IN OUT T2078_KIT_MAPPING.C2078_KIT_NM%type
    , p_active_fl IN T2078_KIT_MAPPING.C2078_ACTIVE_FL%type 	
 	, p_set_id  IN t207_set_master.c207_set_id%TYPE
 	, p_tag_num IN t5010_tag.C5010_TAG_ID%TYPE
 	, p_kit_id IN OUT T2078_KIT_MAPPING.C2078_KIT_MAP_ID%type
 	, p_user_id IN T2078_KIT_MAPPING.C2078_LAST_UPDATED_BY%TYPE
  )
  AS
  v_company_id t1900_company.c1900_company_id%TYPE;
  v_kit_map_id   T2078_KIT_MAPPING.C2078_KIT_MAP_ID%type;  
  v_tag_number varchar2(100);
  v_std_setid_str VARCHAR2 (4000) := p_set_id;
  v_substring     VARCHAR2 (4000) ;
  v_string     VARCHAR2 (4000) ;
  v_set_id t207_set_master.c207_set_id%TYPE;
  
  v_std_tagid_str VARCHAR2 (4000) := p_tag_num;
  v_substring_tag     VARCHAR2 (4000) ;
  v_string_tag     VARCHAR2 (4000) ;
  v_tag_id t5010_tag.C5010_TAG_ID%TYPE;
  v_count NUMBER;
  v_kit_id NUMBER;
  v_count_tag NUMBER;

  BEGIN
 
	  SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL'))
  	    INTO v_company_id 
	    FROM dual;
	    	
	SELECT count(*)
	  INTO v_kit_id
	  FROM T2078_KIT_MAPPING
	 WHERE C2078_KIT_MAP_ID = p_kit_id
	  --AND C901_TYPE = 107920
	   AND C2078_VOID_FL is null;
	
	UPDATE T2078_KIT_MAPPING
	   SET C2078_ACTIVE_FL = p_active_fl,
	   	   C2078_KIT_NM = p_kit_nm
	 WHERE C2078_KIT_MAP_ID = p_kit_id
	  --AND C901_TYPE = 107920
	   AND C2078_VOID_FL IS NULL;

	IF (v_kit_id = 0) THEN
		    SELECT S2078_KIT_MAPPING.NEXTVAL
			  INTO v_kit_map_id
			  FROM DUAL;
		p_kit_id := 'KM-' || v_kit_map_id;
				
	INSERT INTO T2078_KIT_MAPPING(C2078_KIT_MAP_ID,C2078_KIT_NM,C2078_ACTIVE_FL,C2078_VOID_FL,C1900_COMPANY_ID,C2078_LAST_UPDATED_BY,C2078_LAST_UPDATED_DATE,c901_kit_in_out_status) VALUES
	  (p_kit_id,p_kit_nm,p_active_fl,null,v_company_id,p_user_id,current_date,107280);
	 
	END IF;
 
	    WHILE INSTR (v_std_setid_str, '|') <> 0
        	LOOP         
            	v_substring := SUBSTR (v_std_setid_str, 1, INSTR (v_std_setid_str, '|') - 1) ;
            	v_std_setid_str    := SUBSTR (v_std_setid_str, INSTR (v_std_setid_str, '|')    + 1) ;
            	v_set_id    := NULL;
            	v_set_id    := TRIM (v_substring) ;
            	
            	v_substring_tag := SUBSTR (v_std_tagid_str, 1, INSTR (v_std_tagid_str, '|') - 1) ;
            	v_std_tagid_str    := SUBSTR (v_std_tagid_str, INSTR (v_std_tagid_str, '|')    + 1) ;
            	v_tag_id    := NULL;
            	v_tag_id    := TRIM (v_substring_tag) ;
     	
	    SELECT LPAD(v_tag_id,'7','0') INTO v_tag_number FROM dual;
 
	           SELECT COUNT(*)
	          INTO v_count_tag
	          FROM T2079_KIT_MAP_DETAILS
	         WHERE C5010_TAG_ID = v_tag_number 
	           AND C207_SET_ID = v_set_id
	           AND C2078_KIT_MAP_ID = p_kit_id         	 
	           AND C2079_VOID_FL IS NULL;

	  IF(v_count_tag = 0 ) THEN
	  
	  INSERT INTO T2079_KIT_MAP_DETAILS(C2079_KIT_MAP_DTL_ID,C2078_KIT_MAP_ID,C207_SET_ID,C5010_TAG_ID,C2079_VOID_FL,C2079_LAST_UPDATED_BY,C2079_LAST_UPDATED_DATE,C2079_LAST_UPDATED_DATE_UTC) VALUES
	  (S2079_KIT_MAP_DETAILS.nextval,p_kit_id,v_set_id,v_tag_number,null,p_user_id,current_date,current_date);
	 END IF;
	 
	END LOOP;  

	
	  END gm_sav_kit_details;
	  
/* *******************************************************************************
 * Author: Agilan singaravelan
 * Procedure:gm_rem_set_id
 * Description:Remove the set from existing kit list
 ***********************************************************************************/ 
  PROCEDURE gm_rem_set_id(
 	  p_set_id  IN t207_set_master.c207_set_id%TYPE
 	, p_kit_id  IN T2078_KIT_MAPPING.C2078_KIT_MAP_ID%type
 	, p_tag_id  IN t5010_tag.C5010_TAG_ID%TYPE
 	, p_out     OUT varchar2
  )
  AS
  v_count number;
  BEGIN
	  
	  SELECT COUNT(*)
	  INTO v_count 
	  FROM T2079_KIT_MAP_DETAILS
	  WHERE C2078_KIT_MAP_ID = p_kit_id
	  AND C2079_VOID_FL IS NULL;
	  
	  IF(v_count <> 1) THEN
	  UPDATE T2079_KIT_MAP_DETAILS
	     SET C2079_VOID_FL = 'Y'
	   WHERE C207_SET_ID = p_set_id
	     AND C2078_KIT_MAP_ID = p_kit_id
	     AND C5010_TAG_ID = p_tag_id
	     AND C2079_VOID_FL IS NULL;
	 ELSE
	 	p_out := v_count;
	 END IF; 
 END gm_rem_set_id;
/******************************************************************************************
	* Description :  This Function returns the Case Info Id based on earlier return date.
*******************************************************************************************/
FUNCTION GETCASE_INFO_ID
	(p_kit_id 		T2078_KIT_MAPPING.C2078_KIT_MAP_ID%TYPE)
RETURN VARCHAR2
IS
v_case_id		t7100_case_information.c7100_case_id%TYPE;

BEGIN
	BEGIN
	select t.c2078a_last_updated_trans_id into v_case_id from
		(SELECT distinct c2078a_last_updated_trans_id , 
						 c7100_ship_date
		   FROM t2078a_kit_mapping_log t2078a , t7100_case_information t7100, t7105_case_schedular_dtl t7105
		  WHERE t7105.c2078_kit_map_id = p_kit_id
		    and t2078a.c2078_kit_map_id = p_kit_id
			and t2078a.c2078a_last_updated_trans_id = t7100.c7100_case_id
			and t7100.c7100_case_info_id = t7105.c7100_case_info_id
			and c2078a_last_updated_trans_id is not null
			and c7100_void_fl is null
			and c7105_void_fl is null
			AND c2078a_void_fl    IS NULL 
		    AND t7100.c901_case_status != '107163'
		    AND t7100.c901_case_status != '107162'
			order by  c7100_ship_date asc) t where rownum = 1;
   EXCEPTION
      WHEN NO_DATA_FOUND
   THEN
      RETURN ' ';
      END;
 RETURN v_case_id;
END GETCASE_INFO_ID;

/* *******************************************************************************
 * Author: Agilan singaravel
 * Procedure:gm_sav_kit_dtl
 * Description:This Procedure is used to save kit details from IPAD.
 ***********************************************************************************/
	PROCEDURE gm_sav_kit_dtl(
	    p_kit_id       IN   OUT T2078_KIT_MAPPING.C2078_KIT_MAP_ID%type,
	    p_party_id     IN   T2078_KIT_MAPPING.C2078_OWNER_PARTY_ID%TYPE,
	    p_kit_nm       IN   T2078_KIT_MAPPING.C2078_KIT_NM%type,
	    p_active_fl    IN   T2078_KIT_MAPPING.C2078_ACTIVE_FL%type,
	    p_void_fl      IN   T2078_KIT_MAPPING.c2078_void_fl%type,
	    p_user_id      IN   T2078_KIT_MAPPING.C2078_LAST_UPDATED_BY%TYPE,
	    p_company_id   IN   T1900_COMPANY.C1900_COMPANY_ID%TYPE
	  )
	AS
		v_seq		   NUMBER;
		
	BEGIN
	
	UPDATE T2078_KIT_MAPPING
	   SET C2078_ACTIVE_FL = p_active_fl,
	   	   C2078_KIT_NM = p_kit_nm,
	   	   c2078_void_fl = p_void_fl,
	   	   C2078_LAST_UPDATED_BY = p_user_id,
	   	   C2078_LAST_UPDATED_DATE = current_date
	 WHERE C2078_KIT_MAP_ID = p_kit_id
	   AND C2078_VOID_FL IS NULL;
	   
	 IF (SQL%ROWCOUNT = 0) THEN
	 	   
		p_kit_id := GENERATEKITID(p_user_id);  
				
	INSERT INTO T2078_KIT_MAPPING(C2078_KIT_MAP_ID,C2078_KIT_NM,C2078_ACTIVE_FL,C2078_VOID_FL,C1900_COMPANY_ID,C2078_LAST_UPDATED_BY,C2078_LAST_UPDATED_DATE,c901_kit_in_out_status,C901_TYPE,C2078_OWNER_PARTY_ID) 
	     VALUES (p_kit_id,p_kit_nm,p_active_fl,null,p_company_id,p_user_id,current_date,107280,107921,p_party_id);
	 
	END IF;
  END gm_sav_kit_dtl;
  
/* *******************************************************************************
 * Author: Agilan singaravel
 * Procedure:gm_sav_set_details
 * Description:This Procedure is used to save set details based on kit id.
 ***********************************************************************************/
	PROCEDURE gm_sav_set_dtl(
		p_kit_id       IN   OUT T2078_KIT_MAPPING.C2078_KIT_MAP_ID%type,
		p_set_id       IN   t207_set_master.c207_set_id%TYPE,
		p_tag_id       IN   t5010_tag.C5010_TAG_ID%TYPE,
		p_void_fl      IN   T2078_KIT_MAPPING.c2078_void_fl%type,
		p_user_id      IN   T2078_KIT_MAPPING.C2078_LAST_UPDATED_BY%TYPE
	  )	
	  AS
	  BEGIN
		  
		  UPDATE T2079_KIT_MAP_DETAILS
	  		   SET C207_SET_ID = p_set_id,
	  		   	   C5010_TAG_ID = p_tag_id,
	  		   	   C2079_VOID_FL = p_void_fl,
	  		   	   C2079_LAST_UPDATED_BY = p_user_id,
	  		   	   C2079_LAST_UPDATED_DATE = sysdate,
	  		   	   C2079_LAST_UPDATED_DATE_UTC = sysdate
	  		 WHERE C2078_KIT_MAP_ID = p_kit_id
	  		   AND (C5010_TAG_ID = p_tag_id OR C5010_TAG_ID is NULL) 
	  		   AND C207_SET_ID = p_set_id
	  		   AND C2079_VOID_FL IS NULL;

		 IF (SQL%ROWCOUNT = 0) THEN
		 
 			INSERT INTO T2079_KIT_MAP_DETAILS(C2079_KIT_MAP_DTL_ID,C2078_KIT_MAP_ID,C207_SET_ID,C5010_TAG_ID,C2079_VOID_FL,C2079_LAST_UPDATED_BY,C2079_LAST_UPDATED_DATE,C2079_LAST_UPDATED_DATE_UTC) 
 			     VALUES (S2079_KIT_MAP_DETAILS.nextval,p_kit_id,p_set_id,p_tag_id,p_void_fl,p_user_id,current_date,current_date);	  					
 			     
	     END IF;
	END gm_sav_set_dtl;
	  
/* *******************************************************************************
 * Author: Agilan singaravel
 * Procedure:gm_sav_part_details
 * Description:This Procedure is used to save part details based on kit id.
 ***********************************************************************************/
	PROCEDURE gm_sav_part_dtl(
		p_kit_id       IN   OUT T2078_KIT_MAPPING.C2078_KIT_MAP_ID%type,
		p_part_num     IN   t205_part_number.C205_PART_NUMBER_ID%TYPE,
		p_qty          IN   NUMBER,
		p_void_fl      IN   T2078_KIT_MAPPING.c2078_void_fl%type,
		p_user_id      IN   T2078_KIT_MAPPING.C2078_LAST_UPDATED_BY%TYPE
	  )	
	  AS
	  BEGIN

		    UPDATE T2079A_KIT_PART_MAP_DTL 
	  		   SET C205_PART_NUMBER_ID = p_part_num,
	  		   	   C2079A_QTY = p_qty,
	  		   	   C2079A_LAST_UPDATED_BY = p_user_id,
	  		   	   C2079A_LAST_UPDATED_DATE = sysdate,
	  		   	   C2079A_LAST_UPDATED_DATE_UTC = sysdate,
	  		   	   C2079A_VOID_FL = p_void_fl
	  		 WHERE C2078_KIT_MAP_ID = p_kit_id
	  		   AND C205_PART_NUMBER_ID = p_part_num
	  		   AND C2079A_VOID_FL is null;	
	           
		  IF (SQL%ROWCOUNT = 0) THEN
		  
 			INSERT INTO T2079A_KIT_PART_MAP_DTL(C2079A_KIT_PART_MAP_ID ,C2078_KIT_MAP_ID,C205_PART_NUMBER_ID,C2079A_QTY,C2079A_VOID_FL,C2079A_LAST_UPDATED_BY,C2079A_LAST_UPDATED_DATE,C2079A_LAST_UPDATED_DATE_UTC) 
 				 VALUES (S2079A_KIT_PART_MAP_DTL.nextval,p_kit_id,p_part_num,p_qty,p_void_fl,p_user_id,current_date,current_date);
	     END IF;	     
	END gm_sav_part_dtl;  
	
/* *******************************************************************************
 * Author: Agilan singaravel
 * Procedure:gm_sav_rep_map
 * Description:This Procedure is used to save share kit details based on kit id.
 ***********************************************************************************/
	PROCEDURE gm_sav_rep_map(
		p_kit_id       IN   OUT T2078_KIT_MAPPING.C2078_KIT_MAP_ID%type,
		p_party_id     IN   T2078_KIT_MAPPING.C2078_OWNER_PARTY_ID%TYPE,
		p_void_fl      IN   T2078_KIT_MAPPING.c2078_void_fl%type,
		p_user_id      IN   T2078_KIT_MAPPING.C2078_LAST_UPDATED_BY%TYPE
	  )	
	  AS
	  v_count  number;
	  BEGIN

		  SELECT count(c2078_kit_map_id) 
		    INTO v_count 
		    FROM t2078_kit_mapping 
		   WHERE c2078_kit_map_id = p_kit_id 
		     AND c2078_void_fl is null;
		  		  
	  IF (v_count != 0) THEN  
		  
		UPDATE T2079B_KIT_REP_MAP
		   SET C2079B_VOID_FL = p_void_fl,
		   	   C2079B_LAST_UPDATED_BY = p_user_id,
		   	   C2079B_LAST_UPDATED_DATE = sysdate
		 WHERE C2078_KIT_MAP_ID = p_kit_id 
		   AND C2079B_REP_PARTY_ID = p_party_id;
		  
	  IF (SQL%ROWCOUNT = 0) THEN
	  
		  INSERT INTO T2079B_KIT_REP_MAP (C2079B_KIT_REP_MAP_ID,C2078_KIT_MAP_ID,C2079B_REP_PARTY_ID,C2079B_VOID_FL,C2079B_LAST_UPDATED_BY,C2079B_LAST_UPDATED_DATE)
		       VALUES (S2079B_KIT_REP_MAP.NEXTVAL,p_kit_id,p_party_id,p_void_fl,p_user_id,sysdate);
      END IF;
      
      ELSE
      
      	UPDATE T7107_CASE_SHARE
		   SET C7107_VOID_FL = p_void_fl,
		       C7107_LAST_UPDATED_BY = p_user_id,
		       C7107_LAST_UPDATED_DATE = current_date
		 WHERE C7100_CASE_INFO_ID = p_kit_id
		   AND C7107_REP_PARTY_ID = p_party_id
		   AND C7107_VOID_FL IS NULL;
		   
	IF (SQL%ROWCOUNT = 0) THEN		 
		
		INSERT INTO T7107_CASE_SHARE (C7107_CASE_SHARE_ID,C7100_CASE_INFO_ID,C7107_REP_PARTY_ID,C7107_VOID_FL)
		     VALUES (S7107_CASE_SHARE.nextval,p_kit_id,p_party_id,p_void_fl);
		     
	END IF;
   END IF;
      
   END gm_sav_rep_map;
   
/* *******************************************************************************
 * Author: Agilan singaravel
 * FUNCTION:GENERATEKITID
 * Description:This function is used to generate kit id.
 ***********************************************************************************/	
 FUNCTION GENERATEKITID(
	p_user_id      T2078_KIT_MAPPING.C2078_LAST_UPDATED_BY%TYPE
 )
	RETURN VARCHAR2
 IS
	v_kit_id		T2078_KIT_MAPPING.C2078_KIT_MAP_ID%TYPE;
	v_seq           NUMBER;
 BEGIN
	BEGIN
		SELECT  NVL (MAX(TO_NUMBER (SUBSTR (C2078_KIT_MAP_ID,INSTR(C2078_KIT_MAP_ID, '-', -1)+1))), 0) +1  			
	  	  INTO  v_seq
	 	  FROM  T2078_KIT_MAPPING
	 	 WHERE  C2078_KIT_MAP_ID LIKE 'SK-' || p_user_id || '-' || '%' 
	 	   AND  C2078_VOID_FL IS NULL; 
	 	  
	 	  v_kit_id := 'SK-' || p_user_id || '-' || v_seq;
	 	  
   EXCEPTION
      WHEN NO_DATA_FOUND
   THEN
      RETURN '1';
   END;
 RETURN v_kit_id; 
END GENERATEKITID;

  END gm_pkg_kit_mapping;
  /