/* Formatted on 2011/04/28 10:36 (Formatter Plus v4.8.0) */
--@"c:\database\packages\operations\gm_pkg_ls_bo_item_req_txn.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_ls_bo_item_req_txn
IS
--
/***********************************************************************************
* Description : Procedure to release the back orders for items
* Author  : pvigneshwaran
************************************************************************************/

  PROCEDURE gm_sav_release_bo_item_req (
   p_req_id   		  IN       t520_request.c520_request_id%TYPE,
   p_pnum   		  IN       T502_ITEM_ORDER.c205_part_number_id%TYPE ,
   p_bo_Release_qty   IN       T502_ITEM_ORDER.C502_ITEM_QTY%TYPE,
   p_user_id   		  IN       t501_order.c501_last_updated_by%TYPE,
   p_out_err_msg      OUT      CLOB,
   p_out_newrequest_id OUT    t520_request.c520_request_id%TYPE
)
AS
 v_status_fl	   t520_request.C520_STATUS_FL%TYPE;
 v_err_msg	   VARCHAR2(4000) := '';
 v_out_err_msg   CLOB := '';
 v_pnum          t502_item_order.c205_part_number_id%TYPE;
 v_item_qty      NUMBER;
 v_stock         NUMBER;
 v_out_newconsign_id  t504_consignment.c504_consignment_id%TYPE;
 v_out_inhtxn_id  t412_inhouse_transactions.c412_inhouse_trans_id%TYPE;
 v_input_string VARCHAR2 (30000) := '';
 v_out_newrequest_id   t520_request.c520_request_id%TYPE;
BEGIN


 ---------- VALIDATION STARTS -----------------
BEGIN
   SELECT C520_STATUS_FL
   INTO  v_status_fl
   FROM  t520_request 
   WHERE C520_REQUEST_ID = p_req_id
   AND C520_VOID_FL is null
   FOR UPDATE;
EXCEPTION WHEN NO_DATA_FOUND THEN
	v_err_msg := 'Request is not available to release';
	v_out_err_msg := v_out_err_msg || v_err_msg || '; ';
END;

   IF v_status_fl != 10 THEN
		v_err_msg := 'Request status is not in Back Order';
		v_out_err_msg := v_out_err_msg || v_err_msg || '; ';
	END IF;	
	-- 
 BEGIN
   SELECT C205_PART_NUMBER_ID,sum(C521_QTY) 
	INTO v_pnum,v_item_qty
	FROM t521_request_detail 
	WHERE c520_request_id = p_req_id  
	AND c205_part_number_id = p_pnum
	GROUP BY C205_PART_NUMBER_ID;
EXCEPTION WHEN NO_DATA_FOUND THEN
	v_err_msg := 'Part number is not available to release for this request';
	v_out_err_msg := v_out_err_msg || v_err_msg || '; ';
END;

   
    /* Release qty cannot be empty
	 */
	 
   IF p_bo_Release_qty IS NULL OR p_bo_Release_qty = ''
   THEN
      v_err_msg := 'Qty to Release cannot be empty';
      v_out_err_msg := v_out_err_msg || v_err_msg || '; ';
   END IF;
   
     /* if the release qty cannot be decimal OR if the release qty is greater then the item quantity OR
	 * if the item quantity is less than Zero 
	 */
	 IF ABS(p_bo_Release_qty) - trunc(ABS(p_bo_Release_qty)) <> 0  
	THEN  
	  v_err_msg := 'Qty to Release cannot be decimal';
      v_out_err_msg := v_out_err_msg || v_err_msg || '; ';
    ELSE
    	 IF p_bo_Release_qty > v_item_qty OR p_bo_Release_qty <= 0
   		THEN
      		v_err_msg := 'Qty to Release should be less than or equal to Backorder Quantity';
      		v_out_err_msg := v_out_err_msg || v_err_msg || '; ';
   		END IF;
	END IF;
	

   
   BEGIN
	     SELECT get_qty_in_stock (v_pnum) 
	       INTO v_stock
	       FROM t205_part_number
	      WHERE c205_part_number_id = v_pnum;
        END;
        
        	/* if the release qty is greater than FG qty OR
	*/
   IF p_bo_Release_qty > v_stock
   THEN
      v_err_msg := 'Qty in FG is less than the Qty to be released';
      v_out_err_msg := v_out_err_msg || v_err_msg || '; ';
   END IF;
    /*
	 * Remove the last semi-colon in the error detail messages
	 */
	IF v_out_err_msg IS NOT NULL THEN 
		SELECT SUBSTR(trim(v_out_err_msg) , 1, INSTR(trim(v_out_err_msg) , ';', -1)-1)
    		INTO v_out_err_msg 
    		FROM dual;
    		p_out_err_msg := v_out_err_msg;
    		RETURN;
    END IF;
    
    ----- VALIDATION ENDS ---------------------
    	-- 90809 release
    	v_input_string := p_pnum || ','|| p_bo_Release_qty|| ',90809'|| '|';
 BEGIN
 
  gm_pkg_op_request_summary.gm_sav_reconfig_part(p_req_id,v_input_string,p_user_id,v_out_newrequest_id,v_out_newconsign_id,v_out_inhtxn_id);    

	p_out_newrequest_id := p_req_id;
   
EXCEPTION WHEN others THEN
	p_out_err_msg := SQLERRM;
END;
  
END gm_sav_release_bo_item_req;

 
END gm_pkg_ls_bo_item_req_txn;
/