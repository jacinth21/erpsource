--@"c:\database\packages\operations\gm_pkg_op_set_put_rpt.bdy";
CREATE OR REPLACE
PACKAGE BODY gm_pkg_op_set_put_rpt
IS
    /****************************************************************
    * Description : this procedure is used to fetch the Pending FG data
    * Author      :
    *****************************************************************/
PROCEDURE gm_fch_pending_put_requests (
        p_scree_type IN VARCHAR2,
        p_out_details OUT TYPES.cursor_type)
AS
v_comp_id T1900_COMPANY.C1900_COMPANY_ID%TYPE;
v_plant_id T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
v_filter T906_RULES.C906_RULE_VALUE%TYPE;

BEGIN
    SELECT get_compid_frm_cntx(), get_plantid_frm_cntx()
	  INTO v_comp_id, v_plant_id
	  FROM DUAL;

	  SELECT DECODE(GET_RULE_VALUE(v_comp_id,'PENDING_PUT_AWAY'), null, v_comp_id, v_plant_id) --PENDING_PUT_AWAY- To fetch the records for EDC entity countries based on plant
      INTO v_filter 
      FROM DUAL;
      
    OPEN p_out_details FOR
  SELECT t504.c207_set_id setid, get_set_name (t504.c207_set_id) setdesc
	    , t5010.c5010_tag_id tagid, '' etchid, get_code_name (t504.c504_type) settype  
	    , decode(C520_Request_to, NULL,'Built Set','Pending Shipping') nextstatus,
	    NVL ( t504.c504_type, 4110) type, t504.c504_consignment_id REFID, '' loc
	FROM t520_request t520, t504_consignment t504, t5010_tag t5010
  WHERE t504.c504_consignment_id = t5010.c5010_last_updated_trans_id (+)
  	AND t504.c520_request_id = t520.c520_request_id
  	AND t504.c504_type = DECODE (p_scree_type, 'CN', 4110, 'CNLN', 4110, - 9999)
	AND t504.c504_status_fl = '1.20' --pending FG
	AND (t504.c1900_company_id = v_filter
	 OR t504.c5040_plant_id   = v_filter)
	AND t520.c520_void_fl IS NULL
	AND t504.c504_void_fl IS NULL
	AND t5010.c5010_void_fl(+) IS NULL
UNION ALL
--Loaner    Pending FG  , get_loaner_future_status pass two para -- tag id and cn id, if no tag id ,get the next status by cn id
 SELECT t504.c207_set_id setid, get_set_name (t504.c207_set_id) setdesc, t5010.c5010_tag_id tagid, t504a.c504a_etch_id etchid
  , get_code_name (t504.c504_type) settype, gm_pkg_op_set_put_rpt.get_loaner_future_status (t5010.c5010_tag_id, t504a.c504_consignment_id) nextstatus
  , t504.c504_type type, t504.c504_consignment_id REFID, '' loc
   FROM t504a_consignment_loaner t504a, t504_consignment t504, t5010_tag t5010
  WHERE t504a.c504_consignment_id = t5010.c5010_last_updated_trans_id (+) 
    AND t504a.c504_consignment_id         = t504.c504_consignment_id
    AND t504.c504_type                    = DECODE (p_scree_type, 'LN', 4127, 'CNLN', 4127, - 9999)
    AND t504a.c504a_status_fl             = 58 --pending FG
    AND (t504.c1900_company_id = v_filter
	 OR t504.c5040_plant_id   = v_filter)
    AND t504a.c504a_void_fl              IS NULL
    AND t504.c504_void_fl                IS NULL
    AND t5010.c5010_void_fl(+)              IS NULL
UNION ALL
--In house    Pending FG
 SELECT t504.c207_set_id setid, get_set_name (t504.c207_set_id) setdesc, t5010.c5010_tag_id tagid, t504a.c504a_etch_id etchid
  , get_code_name (t504.c504_type) settype, gm_pkg_op_set_put_rpt.get_loaner_future_status (t5010.c5010_tag_id, t504a.c504_consignment_id) nextstatus
  , t504.c504_type type, t504.c504_consignment_id REFID, '' loc
   FROM t504a_consignment_loaner t504a, t504_consignment t504, t5010_tag t5010
  WHERE t504a.c504_consignment_id = t5010.c5010_last_updated_trans_id (+) 
    AND t504a.c504_consignment_id         = t504.c504_consignment_id
    AND t504.c504_type                    = DECODE (p_scree_type, 'IN', 4119, - 9999)
    AND t504a.c504a_status_fl             = 58 --pending FG
    AND (t504.c1900_company_id = v_filter
	 OR t504.c5040_plant_id   = v_filter)
    AND t504a.c504a_void_fl              IS NULL
    AND t504.c504_void_fl                IS NULL
    AND t5010.c5010_void_fl(+)             IS NULL
ORDER BY settype;
END gm_fch_pending_put_requests;
/****************************************************************
* Description : this function is used to get future status
* Author      :
*****************************************************************/
FUNCTION get_loaner_future_status (
        p_tagid t5010_tag.c5010_tag_id%TYPE,
        p_cn  t504_consignment.c504_consignment_id%TYPE)    
        
        RETURN VARCHAR2
IS
    v_status VARCHAR2 (400) ;
BEGIN
    BEGIN
         SELECT (CASE  WHEN TRUNC (t526.c526_required_date) <= TRUNC (CURRENT_DATE)  
											   THEN 'Pending Shipping'
										   ELSE 'Available'
									   END
									) next_status
           INTO v_status
           FROM t504a_consignment_loaner t504a, t504a_loaner_transaction t504al, t526_product_request_detail t526
          , t5010_tag t5010
          WHERE t5010.c5010_tag_id                = p_tagid
            AND t5010.c5010_last_updated_trans_id = t504a.c504_consignment_id
            AND t504a.c504_consignment_id         = t504al.c504_consignment_id
            AND t504al.c504a_return_dt IS NULL
            AND t504al.c526_product_request_detail_id = t526.c526_product_request_detail_id
            AND t504a.c504a_void_fl                  IS NULL
            AND t504al.c504a_void_fl                 IS NULL
            AND t526.c526_void_fl                    IS NULL
            AND t5010.c5010_void_fl                  IS NULL
            AND ROWNUM                                = 1;
    EXCEPTION
    --if no tag id, get the next status by cn id
    WHEN NO_DATA_FOUND THEN
    	BEGIN
    	  SELECT (CASE  WHEN TRUNC (t504a.c504a_planned_ship_date) <= TRUNC (CURRENT_DATE)  
											   THEN 'Pending Shipping'
										   ELSE 'Available'
									   END
									) next_status
           INTO v_status
           FROM t504a_consignment_loaner t504a
          WHERE t504a.c504_consignment_id  = p_cn
    		AND t504a.c504a_void_fl  IS NULL;
	    EXCEPTION
	    WHEN NO_DATA_FOUND THEN		
	        v_status := 'Available' ;
	    END;
	  END;
	  RETURN v_status ;
	    
END get_loaner_future_status;

END gm_pkg_op_set_put_rpt;
/