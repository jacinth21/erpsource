/* Formatted on 2011/12/19 12:45 (Formatter Plus v4.8.0) */
--@"C:\database\packages\operations\gm_pkg_op_order_txn.bdy";
CREATE OR REPLACE
PACKAGE BODY gm_pkg_op_order_txn
IS

	/******************************************************************************************************
	* Description : Procedure to release the order from shipping if the order is having only loaner parts.
	*******************************************************************************************************/
	PROCEDURE gm_sav_release_lnorder(
	    p_trans_id   	IN 		t412_inhouse_transactions.c412_inhouse_trans_id%TYPE,
	    p_trans_type	IN		t412_inhouse_transactions.c412_type%TYPE,
	    p_userid     	IN 		t101_user.c101_user_id%TYPE
	)
	AS
		v_ref_id		T412_INHOUSE_TRANSACTIONS.C412_REF_ID%TYPE;
		v_tot_item 	   	NUMBER;
		v_ln_item 	   	NUMBER;
		v_msg	varchar2(100);
	BEGIN
	  	-- 100406: Pending Shipping to FG
	  	-- if the order is in Pending shipping status, need to get the order id from the PSFG-XXXX transaction id
	  	IF p_trans_type = 100406
	  	THEN
			BEGIN
				SELECT C412_REF_ID INTO v_ref_id 
					FROM T412_INHOUSE_TRANSACTIONS 
				WHERE C412_INHOUSE_TRANS_ID = p_trans_id
					AND C412_TYPE = p_trans_type
					AND C412_VOID_FL IS NULL; 
				EXCEPTION
					WHEN NO_DATA_FOUND THEN
						v_ref_id := NULL;
			END;
		ELSE
			v_ref_id := p_trans_id;-- IF the order is in pending control number status, order id will be getting as the parameter
		END IF;
			
		IF v_ref_id IS NOT NULL
		THEN
			BEGIN
				-- To get the count of total parts and total loaner parts in the order, 50301: Loaner
				SELECT COUNT(1)  ,SUM(DECODE(C901_TYPE,50301,1,0)) 
					INTO v_tot_item,v_ln_item
					FROM T502_ITEM_ORDER 
				WHERE C501_ORDER_ID = v_ref_id
					AND C502_VOID_FL IS NULL;
				EXCEPTION
					WHEN NO_DATA_FOUND THEN
						v_tot_item := NULL;
						v_ln_item := NULL;
			END;
		END IF;
		
		-- if all the parts in a bill and ship order is loaner, then convert the order to Bill only loaner, void the shipping record and do posting 
		IF v_tot_item = v_ln_item AND v_tot_item IS NOT NULL AND v_ln_item IS NOT NULL
		THEN
			
			Gm_pkg_cm_shipping_trans.gm_sav_order_ship(v_ref_id,0,p_userid,v_msg);
			
			UPDATE t501_order
			   	SET c901_order_type = 2530 -- Bill Only Loaner
					, c501_comments = 'Converted to Bill only loaner'
			   	 	, c501_last_updated_by = p_userid
				 	, c501_last_updated_date = SYSDATE
			 WHERE c501_order_id = v_ref_id
			 AND c901_order_type IS NULL
			 AND c501_void_fl IS NULL;
			
			 UPDATE t907_shipping_info
				SET c907_void_fl = 'Y'
					, c907_last_updated_by = p_userid
					, c907_last_updated_date = SYSDATE
			WHERE c907_ref_id = v_ref_id
			AND c907_void_fl IS NULL;
			
			UPDATE t502_item_order 
				SET c502_control_number = 'NOC#'
			WHERE C501_ORDER_ID = v_ref_id
			AND C901_TYPE = '50301';--50301: Loaner
			 
		END IF;
	END gm_sav_release_lnorder;
	
	/************************************************************
	* Description : Procedure to void the Acknowledgement order.
	*************************************************************/
	PROCEDURE gm_void_ack_order(
		  p_ack_order_id   	IN 		t501_order.c501_order_id%TYPE
		, p_userid     		IN 		t101_user.c101_user_id%TYPE
		, p_custom_msg 		OUT		VARCHAR2
	)
	AS
	
		v_bba_order_cnt		NUMBER;
		v_void_fl			t501_order.c501_void_fl%TYPE;
		v_ord_detail_cur	TYPES.cursor_type;
		v_total_qty			NUMBER;
	BEGIN
		BEGIN
			SELECT c501_void_fl
				INTO v_void_fl
			FROM t501_order
			WHERE c501_order_id = p_ack_order_id
			FOR UPDATE;
		END;
		-- If the transaction is voided, then throw validation message
		IF v_void_fl IS NOT NULL
		THEN
			raise_application_error (-20801, '');	
		END IF;
		
		gm_pkg_op_order_txn.gm_fch_order_dtls(p_ack_order_id,v_ord_detail_cur);
		
		-- Get the BBA order count, to know whether BBA order created or not
		SELECT count(1) INTO v_bba_order_cnt
		FROM t501_order
		WHERE c501_ref_id = p_ack_order_id
			AND c501_void_fl IS NULL;
		/* If the BBA order is not generated, then void the ACK order and related records
		 * If BBA order is generated, then reduce the qty of ACK order and do not void the ACK order
		 * Need to return custom message based on the condition whether the order is voided or updated
		*/
		IF v_bba_order_cnt = 0
		THEN
			gm_pkg_op_order_txn.gm_void_ack_order_items(p_ack_order_id,p_userid);
			p_custom_msg := 'Void Acknowledgment order for '||  p_ack_order_id  ||' performed.';
		ELSE
			v_total_qty := gm_pkg_op_order_rpt.get_released_qty(p_ack_order_id);
			
			IF v_total_qty = 0 THEN
				--p_custom_msg := 'All the parts in the Acknowledgment order '||  p_ack_order_id || ' is released to BBA order, cannot void it' ;
				p_custom_msg := 'N' ;
				RETURN;
			END IF;
			
			gm_pkg_op_order_txn.gm_upd_ack_order_items(p_ack_order_id,p_userid,v_ord_detail_cur);
			p_custom_msg := 'The Acknowledgment order '||  p_ack_order_id || ' is updated';
		END IF;
	END gm_void_ack_order;
	
	/*********************************************************************
	* Description : Procedure to fetch the Acknowledgement order details
	**********************************************************************/
	PROCEDURE gm_fch_order_dtls(
		  p_ack_order_id   	IN 		t501_order.c501_order_id%TYPE
		, p_out_detail		OUT 	TYPES.cursor_type
	)
	AS
	BEGIN
	-- Get the acknowledgement order, BBA order qty details
		OPEN p_out_detail FOR
			  SELECT min(itemOrdId) item_ord_id,SUM( NVL(ack_qty,0)) ack_qty, SUM(NVL(bba_qty,0)) ord_qty, ack_order.c205_part_number_id pnum
			  FROM
			    (SELECT t501.c501_order_id, t502.c205_part_number_id, SUM(t502.c502_item_qty) ack_qty, min(t502.c502_item_order_id) itemOrdId
			      FROM t501_order t501 , t502_item_order t502
			      WHERE t501.c501_order_id = t502.c501_order_id
			        AND T501.C901_ORDER_TYPE = 101260 -- Acknowledgment Order
			        AND T501.C501_ORDER_ID   = p_ack_order_id
			        AND T501.C501_VOID_FL   IS NULL
			        AND T502.C502_VOID_FL   IS NULL
			      GROUP BY t501.c501_order_id,
			        t502.c205_part_number_id
			    ) ack_order ,
			  (SELECT t501.c501_ref_id, t502.c205_part_number_id, SUM(t502.c502_item_qty) bba_qty
			    FROM t501_order t501 , t502_item_order t502
			    WHERE T501.C501_ORDER_ID = T502.C501_ORDER_ID
			      AND T501.C501_REF_ID     = p_ack_order_id
			      AND T501.C501_VOID_FL   IS NULL
			      AND T502.C502_VOID_FL   IS NULL
			    GROUP BY t501.c501_ref_id,
			      t502.c205_part_number_id
			    ) bba_order
			WHERE ACK_ORDER.C501_ORDER_ID     = BBA_ORDER.C501_REF_ID (+)
			  AND ACK_ORDER.C205_PART_NUMBER_ID = BBA_ORDER.C205_PART_NUMBER_ID (+)
			  GROUP BY ack_order.c205_part_number_id
			  HAVING (SUM( NVL(ack_qty,0)) - SUM(NVL(bba_qty,0)) <> 0) ;
	END gm_fch_order_dtls;
	
	/************************************************************
	* Description : Procedure to void the Acknowledgement order.
	*************************************************************/
	PROCEDURE gm_void_ack_order_items(
		  p_ack_order_id   	IN 		t501_order.c501_order_id%TYPE
		, p_userid     		IN 		t101_user.c101_user_id%TYPE
	)
	AS
	BEGIN
			UPDATE t501_order
				SET c501_void_fl = 'Y'
			WHERE c501_order_id = p_ack_order_id;
			
			-- Marking the items of the cloned DO as void
			UPDATE t502_item_order
			   SET c502_void_fl = 'Y'
			 WHERE c501_order_id = p_ack_order_id;
			 
			 -- Voiding the Reserved Parts from t5062_control_number_reserve table 
	         UPDATE t5062_control_number_reserve 
                SET c5062_void_fl = 'Y'
                  , c5062_last_updated_by = p_userid
                  , c5062_last_updated_date = CURRENT_DATE
              WHERE c901_transaction_type = '103980' -- Order Transaction
                AND c5062_transaction_id = p_ack_order_id
                AND c5062_void_fl IS NULL; -- Added void flag check PMT-46880
                
			-- Save the status as Ack Voided 
            gm_pkg_cm_status_log.gm_sav_status_details(p_ack_order_id, NULL, p_userid, NULL, CURRENT_DATE, 101862, 91102);
	END gm_void_ack_order_items;
	
	/************************************************************************************************
	* Description : Procedure to edit the quantity of Acknowledgement order based on the release qty
	**************************************************************************************************/
	PROCEDURE gm_upd_ack_order_items(
		  p_ack_order_id   	IN 		t501_order.c501_order_id%TYPE
		, p_userid     		IN 		t101_user.c101_user_id%TYPE
		, p_ord_detail_cur	IN		TYPES.cursor_type
	)
	AS
		v_item_ord_id		t502_item_order.c502_item_order_id%TYPE;
		v_ack_qty			NUMBER;
		v_ord_qty			NUMBER;
		v_pnum				t502_item_order.c205_part_number_id%TYPE;
		v_total 	        NUMBER (15, 2);
	BEGIN
		LOOP	
	    FETCH p_ord_detail_cur INTO v_item_ord_id, v_ack_qty, v_ord_qty, v_pnum;
	    EXIT
	    WHEN p_ord_detail_cur%notfound;
			BEGIN
				IF v_ack_qty > v_ord_qty THEN
					-- while voiding the ACK order, if the part number is not released to BBA order, then void it
					-- else reduce the qty of the part numbers which were released to BBA order and void other records
					IF v_ord_qty = 0 THEN
						UPDATE t502_item_order
	                    	SET c502_void_fl       = 'Y'
	                    WHERE c501_order_id     = p_ack_order_id
		                    AND c205_part_number_id = v_pnum
		                    AND c502_item_order_id = v_item_ord_id
		                    AND C502_VOID_FL IS NULL;	
					ELSE
						UPDATE t502_item_order
	                    	SET c502_item_qty       = v_ord_qty
	                    WHERE c501_order_id     = p_ack_order_id
		                    AND c205_part_number_id = v_pnum
		                    AND c502_item_order_id = v_item_ord_id
		                    AND c502_void_fl IS NULL;
		                    
		                UPDATE t502_item_order
	                    	SET c502_void_fl       = 'Y'
	                    WHERE c501_order_id     = p_ack_order_id
		                    AND c205_part_number_id = v_pnum
		                    AND c502_item_order_id <> v_item_ord_id
		                    AND c502_void_fl IS NULL;
	            	END IF;
	            END IF;
	    	END;
		END LOOP;
		--PMT-40658 ACK Order total cost mismatched while void partially released ACK
		SELECT SUM(NVL(TO_NUMBER(c502_item_qty)*TO_NUMBER(c502_item_price),0))
		INTO v_total
		FROM t502_item_order
		WHERE c501_order_id = p_ack_order_id
		AND c502_void_fl IS NULL;
		
		UPDATE t501_order
		SET c501_total_cost = NVL(v_total,c501_total_cost)
		,c501_last_updated_by = p_userid
		,c501_last_updated_date = CURRENT_DATE
		WHERE c501_order_id = p_ack_order_id
		AND c501_void_fl IS NULL;--PMT-40658
			
		-- Close ACK order after reducing the qty
		UPDATE t501_ORDER t501 
       	SET T501.c501_status_fl = 3, 
            t501.c501_last_updated_by = p_userid, 
            t501.c501_last_updated_date = CURRENT_DATE 
        WHERE t501.c501_order_id = p_ack_order_id;
	END gm_upd_ack_order_items;
	
	/******************************************************************************************************
	* Description : Procedure to save the details of Acknowledegent order from Edit screen
	*******************************************************************************************************/
	PROCEDURE gm_sav_ack_order (
	  p_ordid 		  IN	   t501_order.c501_order_id%TYPE
  	, p_accid 		  IN	   t501_order.c704_account_id%TYPE
	, p_po			  IN	   t501_order.c501_customer_po%TYPE
   	, p_userid		  IN	   t501_order.c501_created_by%TYPE
  	, p_omode 		  IN	   t501_order.c501_receive_mode%TYPE
  	, p_pname 		  IN	   t501_order.c501_received_from%TYPE
	, p_comments	  IN	   t501_order.c501_comments%TYPE
  	, p_parentorderid IN	   t501_order.c501_parent_order_id%TYPE
  	, p_salesrep	  IN	   t501_order.c703_sales_rep_id%TYPE
  	, p_rmqtystr	  IN	   CLOB
  	, p_hard_po	  	  IN	   t501_order.c501_hard_po_fl%TYPE
  	, p_quotestr		 IN  CLOB
  	, p_rmqtymsgstr	  OUT	   CLOB
	)
	AS
	--
		v_parent_cnt   NUMBER;
		v_inv_cnt	   NUMBER;
		v_ship_cnt	   NUMBER;
		v_order_type   t501_order.c901_order_type%TYPE;
		v_current_order_status t501_order.c501_status_fl%TYPE;
		v_total 	   NUMBER (15, 2);
		v_ds_orderid   t501_order.c501_order_id%TYPE;
		v_gpoid 	   VARCHAR2 (20);
		v_old_acctid   t501_order.c704_account_id%TYPE;
		v_comments	   VARCHAR2 (200);
		v_parentorderid t501_order.c501_parent_order_id%TYPE;
		v_rmqtystr		VARCHAR2(4000);
		v_boqtystr		VARCHAR2(4000);
		v_rmqtymsgstr	VARCHAR2(4000);
		v_boqtymsgstr	VARCHAR2(4000);
		v_strlen 		NUMBER 	:= NVL (LENGTH (p_quotestr), 0);
	 	v_string 		VARCHAR2 (30000) := p_quotestr;
	 	v_substring 	VARCHAR2 (1000);
	 	v_attrtype 		t501a_order_attribute.c901_attribute_type%TYPE;
	 	v_attrval 		t501a_order_attribute.c501a_attribute_value%TYPE;
	 	v_ordatbid 		t501a_order_attribute.c501a_order_attribute_id%TYPE;
	 	v_void_fl		t501_order.c501_void_fl%TYPE;
	 	v_division_id   t501_order.c1910_division_id%TYPE;
	 	v_sales_rep		t501_order.c703_sales_rep_id%TYPE;
	--
	BEGIN
		
		-- When enter the surgery details in edit order screen ,will call the save order attribute for the UPDATE/SAVE.
		WHILE INSTR (v_string, '|') <> 0
	 	LOOP
	 		v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
	 		v_string := SUBSTR (v_string, INSTR (v_string, '|') + 1);
	 		--
	 		v_ordatbid := NULL;
	 		v_attrtype := NULL;
	 		v_attrval := NULL;
	 		--
			v_ordatbid := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
			v_attrtype := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
			v_attrval := v_substring;
			gm_pkg_cm_order_attribute.gm_sav_order_attribute (p_ordid,v_attrtype,v_attrval,v_ordatbid,p_userid);

	 	END LOOP;
	 	
		-- Fetch current order status
		SELECT	   t501.c501_status_fl, NVL (t501.c901_order_type, '-999999'), t501.c704_account_id, c501_void_fl
			  INTO v_current_order_status, v_order_type, v_old_acctid, v_void_fl
			  FROM t501_order t501
			 WHERE t501.c501_order_id = p_ordid
		FOR UPDATE;
		
		-- If the transaction is voided, then throw validation message
		IF v_void_fl IS NOT NULL
		THEN
			raise_application_error (-20801, '');	
		END IF;
		
		-- If the transaction is closed, then throw validation message
		IF v_current_order_status = '3'
		THEN
			GM_RAISE_APPLICATION_ERROR('-20999','257','');
		END IF;
		
		--
		IF p_parentorderid IS NOT NULL
		THEN
			SELECT COUNT (*)
			  INTO v_parent_cnt
			  FROM t501_order
			 WHERE c501_order_id = p_parentorderid
			   AND c501_delete_fl IS NULL
			   AND c501_void_fl IS NULL
			   AND c704_account_id = p_accid;
	
			IF v_parent_cnt = 0
			THEN
				raise_application_error (-20799, '');
	                --validation added here, to make sure if order's parent order exist, need get it as new order's parent order
			ELSE
				SELECT NVL (c501_parent_order_id, c501_order_id) orderid
				  INTO v_parentorderid
				  FROM t501_order
				 WHERE c501_order_id = p_parentorderid
				   AND c501_delete_fl IS NULL
				   AND c501_void_fl IS NULL
				   AND c704_account_id = p_accid;
				
			END IF;
		END IF;

--if account name changed for an order or the sales rep is selected as 'Choose one', then getting the sales rep from the account,
--else selected sales rep will update.
IF p_accid <> v_old_acctid OR p_salesrep = '0'
	THEN
   BEGIN
	  SELECT  C703_SALES_REP_ID
	  INTO  v_sales_rep
	  FROM T704_ACCOUNT 
	  WHERE C704_ACCOUNT_ID=p_accid AND C704_VOID_FL IS NULL;
	  EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
           
            v_sales_rep := NULL;
      END;
      ELSE
      v_sales_rep := p_salesrep;
	 END IF;	  
	  
	--getting division id for the sales rep
	BEGIN
	SELECT C1910_DIVISION_ID 
	  INTO v_division_id
	  FROM T703_SALES_REP 
	  WHERE C703_SALES_REP_ID=v_sales_rep 
	  AND C703_VOID_FL IS NULL;
	EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            v_division_id := NULL;
      END;  
	      
		--
		UPDATE t501_order
		   SET c704_account_id = p_accid
			 , c703_sales_rep_id = v_sales_rep
			 , c501_receive_mode = p_omode
			 , c501_received_from = p_pname
		     , c501_last_updated_by = p_userid
			 , c501_last_updated_date = SYSDATE
		     , c501_comments = p_comments
			 , c501_parent_order_id = v_parentorderid
			 , c501_hard_po_fl = p_hard_po
			 , c1910_division_id = v_division_id
		 WHERE c501_order_id = p_ordid;
	
		 -- If the details of ACK order id is modified, then update the details of all the bba orders
		UPDATE t501_order
		SET c704_account_id = p_accid
			 , c703_sales_rep_id = v_sales_rep
			 , c501_receive_mode = p_omode
			 , c501_received_from = p_pname
		     , c501_last_updated_by = p_userid
			 , c501_last_updated_date = SYSDATE
		     , c501_comments = p_comments
		     , c501_parent_order_id = v_parentorderid
			 , c501_hard_po_fl = p_hard_po
			 , c1910_division_id = v_division_id
		WHERE c501_ref_id       IN
						  (SELECT c501_order_id
						  FROM t501_order
						  WHERE c501_order_id = p_ordid
						  AND c501_void_fl   IS NULL
						  );
		 
	-- For remove qty logic
		IF p_rmqtystr IS NOT NULL
		THEN
			gm_pkg_op_order_txn.gm_sav_order_qty(p_ordid, p_userid, p_rmqtystr, v_rmqtymsgstr);
		END IF;
	
		--Update the records of all the child orders 
		UPDATE t501_order
		   SET c704_account_id = p_accid
			 , c703_sales_rep_id =v_sales_rep
			 , c501_receive_mode = p_omode
			 , c501_received_from = p_pname
			 , c501_last_updated_by = p_userid
			 , c501_last_updated_date = SYSDATE
			 , c501_comments = p_comments
			 , c1910_division_id = v_division_id
		 WHERE c501_parent_order_id = p_ordid AND c501_void_fl IS NULL;
		
	   IF v_rmqtymsgstr IS NOT NULL THEN
		 p_rmqtymsgstr := p_rmqtymsgstr || '<br>' ||v_rmqtymsgstr;
	   END IF;
	     
	END gm_sav_ack_order;
	
	/******************************************************************************************************
	* Description : Procedure to save the edited quanitiy of Acknowledegent order from Edit screen
	*******************************************************************************************************/
	PROCEDURE gm_sav_order_qty (
		p_ordid 		IN		 t501_order.c501_order_id%TYPE
	  , p_userid		IN		 t501_order.c501_created_by%TYPE
	  , p_rmqtystr		IN		 CLOB
	  , p_rmqtymsgstr	OUT 	 CLOB
	)
	AS
		v_rmqtymsgstr	 CLOB;
		v_string       CLOB := p_rmqtystr;
   		v_substring    VARCHAR2 (4000);
   		v_org_qty      NUMBER;
		v_orgqty		VARCHAR2 (100);
	   	v_itemordid    VARCHAR2 (100);
	   	v_pnum         VARCHAR2 (100);
	   	v_ptype        VARCHAR2 (100);
	   	v_price        VARCHAR2 (100);
	   	v_qty          VARCHAR2 (100);
	   	v_pnum_str     VARCHAR2 (4000);
	   	v_pnum_temp_str  VARCHAR2 (4000) := ',';
	   	v_index			NUMBER;
	   	v_total 	    NUMBER (15, 2);
	   	v_release_qty  VARCHAR2 (100);
	BEGIN
		
		WHILE INSTR (v_string, '|') <> 0
	   	LOOP
	      v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
	      v_string := SUBSTR (v_string, INSTR (v_string, '|') + 1);
	      v_orgqty := NULL;
	      v_itemordid := NULL;
	      v_pnum := NULL;
	      v_ptype := NULL;
	      v_price := NULL;
	      v_qty := NULL;
	      --OrgQty^ItemOrdId^PNUM^PType^RmQty
	      v_orgqty := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
	      v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
	      v_itemordid := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
	      v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
	      v_pnum := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
	      v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
	      v_ptype := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
	      v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
	      v_price := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
	      v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
	      v_qty := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
	      v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
	      v_release_qty := v_substring;
      
			BEGIN
		      SELECT t502.c502_item_qty orgqty
		        INTO v_org_qty
		        FROM t502_item_order t502
		       WHERE t502.c502_item_order_id = v_itemordid
		         AND t502.c502_void_fl IS NULL
		         AND t502.c502_delete_fl IS NULL;
		      EXCEPTION WHEN NO_DATA_FOUND THEN
		      	v_org_qty := NULL;
		      END;
		      
		      IF v_qty IS NULL OR v_qty <= 0
		      THEN
		      	CONTINUE;
		      END IF;
		      
		      -- If the qty to be removed is greater than the difference of ordered qty and released qty, then it should not allow to update the qty
		      IF (TO_NUMBER(v_org_qty) - TO_NUMBER(v_release_qty)) < TO_NUMBER(v_qty)
		      THEN
		      	GM_RAISE_APPLICATION_ERROR('-20999','258','');
		      END IF;
		      
		      -- If the part number is not added to the string, then add it to display it in the message to avoid duplication of part number id in message
	          SELECT INSTR(v_pnum_temp_str, ','||v_pnum||',', 1) INTO v_index FROM DUAL;
	          IF v_index = 0 THEN
	      		  v_pnum_str := v_pnum_str || v_pnum || ',';
	      	  END IF;
			  v_pnum_temp_str := v_pnum_str || v_pnum || ',';
		        
		      -- If the original qty and qty to be removed is same, then delete the row, else update the qty
		      IF v_org_qty = v_qty
		      THEN
		         DELETE FROM t502_item_order
		               WHERE c502_item_order_id = v_itemordid;
		      ELSE
				UPDATE t502_item_order
	            	SET c502_item_qty = c502_item_qty - TO_NUMBER (v_qty)
	          	WHERE c502_item_order_id = v_itemordid;
	          END IF;
	           
		END LOOP;
		-- update the total cost in t501_order table after removing the part numbers
		SELECT SUM (NVL (TO_NUMBER (c502_item_qty) * TO_NUMBER (c502_item_price), 0))
		  INTO v_total
		  FROM t502_item_order
		 WHERE c501_order_id = p_ordid
		 AND c502_void_fl IS NULL;

		UPDATE t501_order
		   SET c501_total_cost = NVL (v_total, 0)
			 , c501_last_updated_by = p_userid
			 , c501_last_updated_date = CURRENT_DATE
		 WHERE c501_order_id = p_ordid;
		
		IF v_pnum_str IS NOT NULL AND v_pnum_str != ' '
		THEN
			v_pnum_str := SUBSTR (v_pnum_str, 1, LENGTH (v_pnum_str) - 1);
			
			gm_pkg_common_cancel_op.gm_cm_sav_cancelrow
		                           (p_ordid,
		                            100405,
		                            'VPTDO',
		                               'The Part numbers that are removed from the current DO are'
		                            || v_pnum_str,
		                            p_userid
		                           );
		                           
			p_rmqtymsgstr :=
		          'The parts that are removed for the current Acknowledgment Order are: ' || v_pnum_str;
		END IF;
		
	END gm_sav_order_qty;
	
	/******************************************************************************************************
	* Description : Procedure to update hold process for orders not completed
	*******************************************************************************************************/
	PROCEDURE gm_upd_process_hold (
        p_ordid          IN   t501_order.c501_order_id%TYPE
       ,p_userid         IN   t501_order.c501_last_updated_by%TYPE
	)
	AS
		v_parent_ord_id   t501_order.c501_parent_order_id%TYPE;
   	BEGIN
	   	--get the parent id by passing the p_ordid 
	   	--if it is not a child order then the below function will return passed order id   
		SELECT get_parent_order_id(p_ordid)
		 INTO v_parent_ord_id
		FROM dual;
		--if the above function return NULL, then assign the parameter p_ordid 
		IF v_parent_ord_id IS NULL
		THEN
			v_parent_ord_id :=p_ordid;
		END IF;
		
	 --update the process hold flag to indicate the process of hold order not yet completed
	   UPDATE T501_ORDER 
		   SET C501_HOLD_ORDER_PROCESS_FL = 'N',
		   		C501_HOLD_ORDER_PROCESS_DATE = CURRENT_DATE, 
		   		C501_LAST_UPDATED_BY = p_userid,
		   		C501_LAST_UPDATED_DATE = CURRENT_DATE 
		   WHERE (C501_ORDER_ID = p_ordid OR C501_PARENT_ORDER_ID = p_ordid)
		  		AND C501_VOID_FL IS NULL 
		   		AND C503_INVOICE_ID IS NULL  
		  		AND C501_HIST_HOLD_FL IS NULL
		  		AND NVL(C501_HOLD_ORDER_PROCESS_FL, -9999) <> 'Y'
		  		AND NVL (c901_order_type, -9999) NOT IN 
						 		(SELECT C906_RULE_VALUE FROM v901_ORDER_TYPE_GRP
								 );
						 
 	END gm_upd_process_hold;
 
 	/******************************************************************************************************
	* Description : Procedure to save the recorded tags for the DO raised from Globus App
	* Author: Karthik
	*******************************************************************************************************/
 	
 	 PROCEDURE gm_save_DO_record_tag_list (
  		p_order_id       IN    t501_order.C501_Order_Id%TYPE,
	    p_record_tag_str IN	   CLOB,
	    p_userid         IN    t501_order.C501_Created_By%TYPE
)
AS	
		v_tag_id    		t5010_tag.c5010_tag_id%TYPE;
		v_set_id			T207_Set_Master.c207_set_id%TYPE;
		v_set_nm			T207_Set_Master.c207_set_nm%TYPE;
		v_usage_fl   		t501d_order_tag_usage_details.c501d_tag_usage_fl%TYPE;
		v_tag_latitude      t501d_order_tag_usage_details.c501d_tag_latitude%TYPE;
		v_tag_longtitude   	t501d_order_tag_usage_details.c501d_tag_longtitude%TYPE;
		v_string	   		CLOB := p_record_tag_str;
		v_substring    		VARCHAR2 (4000);
		v_tag_cnt			NUMBER;
		v_company_id		T1900_COMPANY.C1900_COMPANY_ID%TYPE;
		v_tag_set_id		T207_Set_Master.c207_set_id%TYPE;
		
	BEGIN
		  SELECT get_compid_frm_cntx()
		  	INTO v_company_id
		  FROM dual;
		  
		IF (p_record_tag_str IS NOT NULL)	 THEN
			WHILE INSTR (v_string, '|') <> 0 LOOP
				--
				v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
				v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
				--
				v_tag_id   := NULL;
				v_set_id   := NULL;
				v_set_nm   := NULL;
				v_usage_fl := NULL;
				v_tag_latitude 	 := NULL;
				v_tag_longtitude := NULL;
				--
				v_tag_id 	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_set_id 	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_set_nm 	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_usage_fl 	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_tag_latitude 	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_tag_longtitude  := v_substring;
            	
            	
            	gm_pkg_op_order_txn.gm_save_record_tag(v_tag_id,v_set_id,p_order_id,v_usage_fl,v_tag_latitude,v_tag_longtitude,p_userid); 
            	
			END LOOP;
		END IF;
   
END gm_save_DO_record_tag_list;

 	/******************************************************************************************************
	* Description : Procedure to save the recorded tags for Bulk DO
	* Author	  : Yoga
    * PC  : 3534
	*******************************************************************************************************/
 	
 	  PROCEDURE gm_save_DO_record_tags (
 	    p_tag_id    	 IN	   t5010_tag.c5010_tag_id%TYPE,
 	    p_set_id	     IN    T207_Set_Master.c207_set_id%TYPE,
  		p_order_id       IN    t501_order.C501_Order_Id%TYPE,
	    p_usage_fl   	 IN    t501d_order_tag_usage_details.C501D_TAG_USAGE_FL%TYPE,
	    p_tag_latitude   IN    t501d_order_tag_usage_details.c501d_tag_latitude%TYPE,
	    p_tag_longtitude IN    t501d_order_tag_usage_details.c501d_tag_longtitude%TYPE,
	    p_userid         IN    t501_order.C501_Created_By%TYPE,
	    p_system_id        IN   t207_set_master.C207_set_id%TYPE,
	    p_system_usage    IN t501d_order_tag_usage_details.c501d_system_usage%TYPE,
	    p_order_category    IN t501d_order_tag_usage_details.c901_order_Category%TYPE
	)
	AS
		v_company_id    	T1900_COMPANY.C1900_COMPANY_ID%TYPE;
		v_plant_id			T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
		v_tag_id			t5010_tag.c5010_tag_id%TYPE;
		v_id				T501D_ORDER_TAG_USAGE_DETAILS.C501D_ORDER_TAG_USAGE_DETAILS_ID%TYPE;
		v_tag_count number := 0;
		v_set_count number := 0;
		v_tag_current_sts  t501d_order_tag_usage_details.c901_tag_status%TYPE;
   	BEGIN
	   	
	  SELECT get_compid_frm_cntx(), get_plantid_frm_cntx()
		  INTO v_company_id, v_plant_id
		  FROM dual;
			  
			SELECT DECODE(p_tag_id,'MISSING',NULL,p_tag_id) 
			  INTO v_tag_id 
			 FROM DUAl;
   
			SELECT S501D_ORDER_TAG_USAGE_DETAILS.NEXTVAL
           	 INTO v_id
           	FROM DUAL;
           	
           	 SELECT COUNT(1) -- Tag validation
         	   INTO v_tag_count
               FROM T5010_TAG 
              WHERE C5010_TAG_ID = p_tag_id
                AND C5010_VOID_FL IS NULL;
             
             SELECT COUNT(1)
               INTO v_set_count
       		   FROM T207_SET_MASTER 
       		  WHERE C207_SET_ID = p_set_id
       		    AND C207_VOID_FL IS NULL;
       	--PC-2369 -Record Tags - Save Current Tag Status for the DO
       	-- to get current status of Tag id and insert or update the tag status into t501d_order_tag_usage_details 	    
       	   BEGIN
	         SELECT c901_status
			   INTO v_tag_current_sts
			   FROM t5010_tag
			  WHERE c5010_tag_id = v_tag_id
			    AND c5010_void_fl IS NULL;
		    EXCEPTION
			   WHEN NO_DATA_FOUND THEN
			   v_tag_current_sts := NULL;
		   END;

				       		    
      --Update either by Tag or by system    		    
			UPDATE T501D_ORDER_TAG_USAGE_DETAILS SET
				C207_SET_ID = p_set_id,
				C501D_SYSTEM_USAGE = p_system_usage,
				C901_ORDER_CATEGORY = p_order_Category,
				C501D_TAG_LATITUDE = p_tag_latitude,
				C501D_TAG_LONGTITUDE = p_tag_longtitude,
				C501D_TAG_GEO_LOCATION = DECODE(p_tag_latitude , NULL , NULL , SDO_GEOMETRY(2001,8307,SDO_POINT_TYPE(p_tag_longtitude,p_tag_latitude,NULL),NULL,NULL)),
				C901_TAG_STATUS     = v_tag_current_sts,
				C501D_LAST_UPDATED_BY = p_userid,
				C501D_LAST_UPDATED_DATE = CURRENT_DATE
			WHERE C501_ORDER_ID = p_order_id
			AND nvl(C5010_TAG_ID,'9999') = nvl(v_tag_id,'9999')
			AND nvl(c207_system_id,'9999') = nvl(p_system_id,'9999')
			AND C501D_VOID_FL IS NULL;    
			
			IF (SQL%ROWCOUNT = 0) THEN 
	         	INSERT INTO T501D_ORDER_TAG_USAGE_DETAILS
	                     (C501D_ORDER_TAG_USAGE_DETAILS_ID, C501_ORDER_ID,
	                      C5010_TAG_ID, C207_SET_ID,C501D_TAG_LATITUDE,
	                      C501D_TAG_LONGTITUDE,C501D_TAG_GEO_LOCATION,
	                      C501D_VOID_FL,C501D_CREATED_BY,
	                      C501D_CREATED_DATE, C501D_LAST_UPDATED_BY,
	                      C501D_LAST_UPDATED_DATE, C501D_VALID_FL, C501D_TRANSACTION_FL,C901_TAG_STATUS,
	                      c207_system_id,
	                      c501d_system_usage,
	                      c901_order_Category
	                     )
	              VALUES (v_id, p_order_id,
	                      v_tag_id, p_set_id,p_tag_latitude ,
	                      p_tag_longtitude, DECODE(p_tag_latitude , NULL , NULL , SDO_GEOMETRY(2001,8307,SDO_POINT_TYPE(p_tag_longtitude,p_tag_latitude,NULL),NULL,NULL)) ,
	                      NULL,p_userid,
	                      CURRENT_DATE, p_userid
	                      ,CURRENT_DATE, NULL, NULL,v_tag_current_sts,
	                      p_system_id,
	                      p_system_usage,
	                      p_order_Category
	                     );
	         END IF;
	         
	         IF ((p_tag_id is not null OR p_set_id is not null) and (v_tag_count = 0 OR v_set_count = 0)) THEN
       	  		 UPDATE T501D_ORDER_TAG_USAGE_DETAILS 
				    SET C501D_VALID_FL = 'N',
				        C501D_LAST_UPDATED_BY = p_userid,
 						C501D_LAST_UPDATED_DATE = CURRENT_DATE
				  WHERE C501_ORDER_ID = p_order_id
					 AND (C5010_TAG_ID = v_tag_id
          			 OR C207_SET_ID = p_set_id)	
					AND C501D_VOID_FL IS NULL;
      		 END IF;
      		 
      		 IF ((p_tag_id is not null OR p_set_id is not null) and (v_tag_count = 1 OR v_set_count = 1)) THEN
       	  		 UPDATE T501D_ORDER_TAG_USAGE_DETAILS 
				    SET C501D_VALID_FL = 'Y',
				        C501D_LAST_UPDATED_BY = p_userid,
 						C501D_LAST_UPDATED_DATE = CURRENT_DATE
				  WHERE C501_ORDER_ID = p_order_id
				    AND (C5010_TAG_ID = v_tag_id
          			 OR C207_SET_ID = p_set_id)
					AND C501D_VOID_FL IS NULL;
      		 END IF;
    
    
				
  END gm_save_DO_record_tags;

 	/******************************************************************************************************
	* Description : Procedure to save the recorded tags for the DO with the Location
	* Author	  : Karthik
	*******************************************************************************************************/
 	
 	  PROCEDURE gm_save_DO_record_tags (
 	    p_tag_id    	 IN	   t5010_tag.c5010_tag_id%TYPE,
 	    p_set_id	     IN    T207_Set_Master.c207_set_id%TYPE,
  		p_order_id       IN    t501_order.C501_Order_Id%TYPE,
	    p_usage_fl   	 IN    t501d_order_tag_usage_details.C501D_TAG_USAGE_FL%TYPE,
	    p_tag_latitude   IN    t501d_order_tag_usage_details.c501d_tag_latitude%TYPE,
	    p_tag_longtitude IN    t501d_order_tag_usage_details.c501d_tag_longtitude%TYPE,
	    p_userid         IN    t501_order.C501_Created_By%TYPE
	)
AS
 v_system_id     t207_set_master.C207_set_id%TYPE;
 BEGIN	
  
    BEGIN
      SELECT GM_PKG_SM_TAG_ORDER_HISTORY.get_system_id(p_set_id) INTO v_system_id
      FROM DUAL;
    EXCEPTION WHEN OTHERS THEN
      v_system_id := NULL;
    END;
    
    -- 111160  Order Category = 'DO'
    gm_save_DO_record_tags(p_tag_id,p_set_id,p_order_id,p_usage_fl,p_tag_latitude,p_tag_longtitude,p_userid,v_system_id,1, 111160 );
    
END gm_save_DO_record_tags;

/**********************************************************************************************************
	* Description :  The below procedure is used to save the tag with Location details from the DO
	* Author	  :  Karthik
	*******************************************************************************************************/
 	
 	  PROCEDURE gm_save_tag_location_details (
 	    p_tag_id    	 IN	   t5010_tag.c5010_tag_id%TYPE,
 	    p_set_id	     IN    T207_Set_Master.c207_set_id%TYPE,
 	    p_order_id       IN    t501_order.C501_Order_Id%TYPE,
	    p_tag_latitude   IN    t501d_order_tag_usage_details.c501d_tag_latitude%TYPE,
	    p_tag_longtitude IN    t501d_order_tag_usage_details.c501d_tag_longtitude%TYPE,
	    p_userid         IN    t501_order.C501_Created_By%TYPE
	)
AS
		v_company_id    	T1900_COMPANY.C1900_COMPANY_ID%TYPE;
		v_plant_id			T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
		v_account_id		T704_Account.C704_Account_Id%TYPE;
		v_rep_id			T703_Sales_Rep.C703_Sales_Rep_Id%TYPE;
		v_tag_id			 t5010_tag.c5010_tag_id%TYPE;
		
   	BEGIN
	   	
	  SELECT get_compid_frm_cntx(), get_plantid_frm_cntx()
		  INTO v_company_id, v_plant_id
		  FROM dual;
           	
		  BEGIN
			     SELECT C704_Account_Id accounid ,
		  				C703_Sales_Rep_Id repid
			  		INTO v_account_id , v_rep_id
				FROM T501_Order
				WHERE C501_Order_Id = p_order_id
				AND C501_Void_Fl   IS NULL;
		      EXCEPTION WHEN NO_DATA_FOUND THEN
		      	v_account_id := NULL;
		      	v_rep_id := NULL;
		      END;
			
		     SELECT DECODE(p_tag_id,'MISSING',NULL,p_tag_id) 
			  INTO v_tag_id 
			 FROM DUAl;
			 
		IF v_tag_id IS NOT NULL THEN 
			UPDATE T5010B_TAG_LOCATION_DETAILS SET
				C207_SET_ID = p_set_id,
				C704_Account_Id = v_account_id,
				C703_Sales_Rep_Id = v_rep_id,
				C5010B_TAG_LATITUDE = p_tag_latitude,
				C5010B_TAG_LONGTITUDE = p_tag_longtitude,
				C5010B_TAG_GEO_LOCATION = DECODE(p_tag_latitude , NULL ,NULL, SDO_GEOMETRY(2001,8307,SDO_POINT_TYPE(p_tag_longtitude,p_tag_latitude,NULL),NULL,NULL)),
				C5010B_LAST_UPDATED_BY = p_userid,
				C5010B_LAST_UPDATED_DATE = CURRENT_DATE
			WHERE C5010_TAG_ID = v_tag_id
			AND C5010B_VOID_FL IS NULL
			AND C1900_COMPANY_ID = v_company_id;
			
			IF (SQL%ROWCOUNT = 0) THEN 
	         	INSERT INTO T5010B_TAG_LOCATION_DETAILS
	                     (C5010_Tag_Id, C207_Set_Id,C704_Account_Id,C703_Sales_Rep_Id,
	                      C5010b_Tag_Latitude, C5010b_Tag_Longtitude,C5010b_Tag_Geo_Location,
	                      C5010b_Void_Fl,C1900_Company_Id,
	                      C5040_PLANT_ID,C5010b_Created_By,
	                      C5010b_Created_Date, C5010b_Last_Updated_By,
	                      C5010B_LAST_UPDATED_DATE
	                     )
	              VALUES (v_tag_id, p_set_id,v_account_id,v_rep_id,
	                      p_tag_latitude, p_tag_longtitude ,
	                      DECODE(p_tag_latitude , NULL ,NULL, SDO_GEOMETRY(2001,8307,SDO_POINT_TYPE(p_tag_longtitude,p_tag_latitude,NULL),NULL,NULL)),
	                      NULL,v_company_id,v_plant_id,p_userid,
	                      CURRENT_DATE, p_userid
	                      ,CURRENT_DATE
	                     );
	         END IF;
	     END IF;
END gm_save_tag_location_details;

/**************************************************************************************************************
	* Description :  The below procedure is used to save the log for Location details for the corresponding Tag and it's calling 
	* 				 from the TRG_T5010B_TAG_LOCATION_DETAILS.trg
	* Author	  :  Karthik
***************************************************************************************************************/
 	
 	  PROCEDURE gm_save_tag_location_log (
 	    p_tag_id    	 IN	   t5010_tag.c5010_tag_id%TYPE,
 	    p_set_id	     IN    T207_Set_Master.c207_set_id%TYPE,
 	    p_account_id	 IN    T704_ACCOUNT.C704_Account_Id%TYPE,
 	    p_rep_id	     IN    T703_Sales_Rep.C703_SALES_REP_ID%TYPE,
	     p_tag_latitude    IN    T5010B_TAG_LOCATION_DETAILS.C5010B_TAG_LATITUDE%TYPE,
	    p_tag_longtitude   IN    T5010B_TAG_LOCATION_DETAILS.C5010B_TAG_LONGTITUDE%TYPE,
	    p_tag_geo_location IN    T5010B_TAG_LOCATION_DETAILS.C5010B_TAG_GEO_LOCATION%TYPE,
	    p_company_id 	   IN    T1900_COMPANY.C1900_COMPANY_ID%TYPE,
	    p_plant_id 		   IN    T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE,
	    p_userid           IN    t501_order.C501_Created_By%TYPE
	)
AS	
   	BEGIN
	   	
     	INSERT INTO T5010C_TAG_LOCATION_LOG
                 (C5010c_Tag_Location_Log_Id,C5010_Tag_Id,
                  C207_Set_Id,C704_Account_Id,C703_Sales_Rep_Id,
                  C5010b_Tag_Latitude, C5010b_Tag_Longtitude,C5010b_Tag_Geo_Location,
                  C5010C_VOID_FL,C1900_Company_Id,
                  C5040_PLANT_ID,C5010C_CREATED_BY,
                  C5010C_CREATED_DATE, C5010C_LAST_UPDATED_BY,
                  C5010C_Last_Updated_Date
                 )
          VALUES (S5010C_TAG_LOCATION_LOG.NEXTVAL, p_tag_id,p_set_id,p_account_id,p_rep_id,
                  p_tag_latitude, p_tag_longtitude ,
                  DECODE(p_tag_latitude , NULL , NULL , SDO_GEOMETRY(2001,8307,SDO_POINT_TYPE(p_tag_longtitude,p_tag_latitude,NULL),NULL,NULL)),
                  NULL,p_company_id,p_plant_id,p_userid,
                  CURRENT_DATE, p_userid
                  ,CURRENT_DATE
                 );
                 
END gm_save_tag_location_log;

/******************************************************************************************************
	* Description : Procedure to save the recorded tags for the order raised from Portal
	* Author: Vinoth
	*******************************************************************************************************/
 	
 	 PROCEDURE gm_save_record_tag(
  		p_tag_id    	 IN	   t5010_tag.c5010_tag_id%TYPE,
 	    p_set_id	     IN    T207_Set_Master.c207_set_id%TYPE,
  		p_order_id       IN    t501_order.C501_Order_Id%TYPE,
  		p_usage_fl       IN    t501d_order_tag_usage_details.c501d_tag_usage_fl%TYPE DEFAULT NULL,
  		p_tag_latitude   IN    t501d_order_tag_usage_details.c501d_tag_latitude%TYPE DEFAULT NULL,
		p_tag_longtitude IN    t501d_order_tag_usage_details.c501d_tag_longtitude%TYPE DEFAULT NULL,
  		p_userid         IN    t501_order.C501_Created_By%TYPE
)
AS	
		v_usage_fl   		t501d_order_tag_usage_details.c501d_tag_usage_fl%TYPE;
		v_company_id		T1900_COMPANY.C1900_COMPANY_ID%TYPE;
		v_tag_set_id		T207_Set_Master.c207_set_id%TYPE;
		v_part_num      T5010_Tag.C205_PART_NUMBER_ID%TYPE;
		
	BEGIN
		  SELECT get_compid_frm_cntx()
		  	INTO v_company_id
		  FROM dual;
				
				/*Get the set id for the valid tag,if the set id is coming as null then save the Set id getting from the below query*/
				BEGIN
					SELECT  T5010.C207_Set_Id setid ,T5010.c205_part_number_id pnum
						INTO v_tag_set_id , v_part_num
					 FROM T5010_Tag T5010
					WHERE T5010.C5010_Tag_Id   = p_tag_id
					AND T5010.C5010_Void_Fl   IS NULL
					AND T5010.C1900_Company_Id = v_company_id
					GROUP BY  T5010.C5010_Tag_Id , T5010.C207_Set_Id,T5010.c205_part_number_id;
					EXCEPTION WHEN NO_DATA_FOUND THEN
		      			v_tag_set_id := NULL;
		      			v_part_num := NULL;
				END;
				
				--To get the set id for the tag if it is associated to the partnumber.
				IF v_tag_set_id IS NULL AND v_part_num IS NOT NULL THEN
		          v_tag_set_id := gm_pkg_common.get_part_primary_set_id(v_part_num);
		        END IF;
        
				/*The below procedure is used to save the tag details for the Order ID*/
				gm_save_DO_record_tags(p_tag_id,NVL(p_set_id,v_tag_set_id),p_order_id,v_usage_fl,p_tag_latitude,p_tag_longtitude,p_userid);
				
				/*The below procedure is used to save only the tag details*/
				IF p_tag_id IS NOT NULL THEN
					gm_save_tag_location_details(p_tag_id,NVL(p_set_id,v_tag_set_id),p_order_id,p_tag_latitude,p_tag_longtitude,p_userid);
				END IF;

END gm_save_record_tag;
/******************************************************************************************************
	* Description : Procedure to save the recorded tags for the order raised from Portal if the order is valid
	* Author: Paveethra
	*******************************************************************************************************/
 	 PROCEDURE gm_save_do_record_tag_dtls(
  		p_tag_id    	 IN	   t5010_tag.c5010_tag_id%TYPE,
 	    p_set_id	     IN    T207_Set_Master.c207_set_id%TYPE,
  		p_order_id       IN    t501_order.C501_Order_Id%TYPE,
  		p_usage_fl       IN    t501d_order_tag_usage_details.c501d_tag_usage_fl%TYPE DEFAULT NULL,
  		p_tag_latitude   IN    t501d_order_tag_usage_details.c501d_tag_latitude%TYPE DEFAULT NULL,
		p_tag_longtitude IN    t501d_order_tag_usage_details.c501d_tag_longtitude%TYPE DEFAULT NULL,
  		p_userid         IN    t501_order.C501_Created_By%TYPE
)
AS	

		v_company_id		T1900_COMPANY.C1900_COMPANY_ID%TYPE;
		v_count             NUMBER;
		
	BEGIN
		  SELECT get_compid_frm_cntx()
		  	INTO v_company_id
		  FROM dual;
				
				BEGIN
						SELECT COUNT(1) 
						INTO v_count
						FROM t501_order 
						WHERE c501_order_id = p_order_id
						AND c501_void_fl IS NULL; 
					EXCEPTION WHEN NO_DATA_FOUND THEN
		      			v_count := 0;
				END;
				
				IF v_count > 0 THEN
				 gm_pkg_op_order_txn.gm_save_record_tag(p_tag_id,p_set_id,p_order_id,p_usage_fl,p_tag_latitude,p_tag_longtitude,p_userid); 
				END IF;


END gm_save_do_record_tag_dtls;

END gm_pkg_op_order_txn;
/