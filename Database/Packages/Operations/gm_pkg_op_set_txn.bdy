--@"c:\database\packages\operations\gm_pkg_op_set_txn.bdy";
CREATE OR REPLACE
PACKAGE BODY gm_pkg_op_set_txn
IS
    /****************************************************************
    * Description : this procedure is used to call the Con or Loaner prc
    * Author      :
    *****************************************************************/
	PROCEDURE gm_sav_set_txn_status (
	        p_txnid     IN VARCHAR2,
	        p_operation IN VARCHAR2,
	        p_txn_type  IN t504_consignment.c504_type%TYPE,
	        p_user_id   IN t504_consignment.c504_last_updated_by%TYPE)
	AS 
	BEGIN
		---- 4110- Consignment, 4112 - inhouse Consignment,26240144 - Return Set
		--111802 - Finished Goods - Inhouse, 111805- Restricted Warehouse - Inhouse
	    IF p_txn_type = '4110' OR p_txn_type = '4112' OR p_txn_type = '26240144' 
	        OR p_txn_type = '111802' OR p_txn_type = '111805' 
	        THEN
	        gm_pkg_op_consignment.gm_sav_consignment_status (p_txnid, p_operation, p_user_id) ;
	    ELSIF p_txn_type = '4127' -- Loaner
	        THEN
	        gm_pkg_op_loaner_set_txn.gm_sav_loaner_status (p_txnid, p_operation, p_user_id) ;
	    ELSIF p_txn_type = '4119' -- In House Loaner
	        THEN
	        gm_pkg_op_loaner_set_txn.gm_sav_loaner_status (p_txnid, p_operation, p_user_id) ;
	    END IF;
	END gm_sav_set_txn_status ;
	
	
	/****************************************************************
	    * Description : this procedure is used to save the location and Tag
	    * Author      : Yogabalakrishnan
	*****************************************************************/
	PROCEDURE gm_sav_tag_location (
	    p_tag_id IN t5010_tag.c5010_tag_id%TYPE,
	    p_loc_id IN t5052_location_master.c5052_location_id%TYPE,
	    p_txn_id IN t504_consignment.c504_consignment_id%TYPE,
	    p_txn_type IN t504_consignment.c504_type%TYPE,
	    p_user_id IN t101_user.c101_user_id%TYPE,
	    p_operation IN VARCHAR2
	) 
	AS
	    v_count NUMBER;
	    v_loc_id  t5052_location_master.c5052_location_id%TYPE;
	    v_plant_id    t5040_plant_master.c5040_plant_id%TYPE;
	    
	BEGIN
         SELECT  NVL(get_plantid_frm_cntx(),get_rule_value('DEFAULT_PLANT','ONEPORTAL')) INTO  v_plant_id FROM DUAL;

	     IF p_operation = 'PUT' AND p_loc_id IS NOT NULL
	 		 THEN
	   		gm_pkg_op_set_validate.gm_validate_location(p_loc_id,p_txn_type);
		 END IF;
	  
	    BEGIN
	      SELECT COUNT (1)
	           INTO v_count
	           FROM t504_consignment t504
	          WHERE t504.c504_consignment_id    = p_txn_id
	            AND t504.c504_void_fl IS NULL
	            AND c5040_plant_id= v_plant_id;
	    
	      IF  v_count = 0
	      THEN
	      		GM_RAISE_APPLICATION_ERROR('-20999','285','');
	      END IF;
	    END; 
	   	    
	  BEGIN
	 	 --after pick, need remove the set from location  
	   IF p_operation = 'PICK'  
  	   THEN	 
  	   	---this update used here to have txn_id and txn type in log table t5054_inv_location_log	
  	  	 gm_sav_location_qty(p_tag_id, p_loc_id, 0, p_txn_id, p_txn_type, p_user_id, p_operation);  
  	   
   			DELETE FROM t5053_location_part_mapping t5053
			  WHERE t5053.c5010_tag_id = p_tag_id
       		  AND t5053.c5052_location_id = p_loc_id;
       ---when do put away,	only save the set which has tag id into location.	  
 	   ELSIF  p_tag_id IS NOT NULL
 	   	THEN
	    -- Update tag location for the given tag_id  for PUT       
	    gm_sav_location_qty(p_tag_id, p_loc_id, 1, p_txn_id, p_txn_type, p_user_id, p_operation);    
	      
	  END IF; 
	    
	  END; 
	  
	END gm_sav_tag_location;
	
/**********************************************************************
* Description : this procedure is used to update qty in the location  
* Author      : 
	*****************************************************************/
	PROCEDURE gm_sav_location_qty (
	    p_tag_id IN t5010_tag.c5010_tag_id%TYPE,
	    p_loc_id IN t5052_location_master.c5052_location_id%TYPE,
	    p_qty IN t5053_location_part_mapping.c5053_curr_qty%TYPE,
	    p_txn_id IN t504_consignment.c504_consignment_id%TYPE,
	    p_txn_type IN t504_consignment.c504_type%TYPE,
	    p_user_id IN t101_user.c101_user_id%TYPE,
	    p_operation IN VARCHAR2
	) 
	AS 
		v_loc_part_id  t5053_location_part_mapping.c5053_location_part_map_id%TYPE;
	BEGIN
		 
		--update should be based on tag ID instead of tag id and location
	    UPDATE t5053_location_part_mapping t5053 
	      SET c5053_curr_qty = p_qty
	        , c5053_last_updated_date = SYSDATE
	        , c5053_last_updated_by = p_user_id
	        , c5053_last_update_trans_id = p_txn_id
	        , c901_last_transaction_type = p_txn_type
	        , c5052_location_id = p_loc_id
	      WHERE t5053.c5010_tag_id = p_tag_id; 
	      
	    IF SQL%ROWCOUNT = 0 AND p_operation='PUT'  THEN -- If given tag_id not found insert a record with Qty=1
	      SELECT s5053_location_part_mapping.NEXTVAL
							  INTO v_loc_part_id
							  FROM DUAL;
	           
	      INSERT INTO t5053_location_part_mapping
			(c5053_location_part_map_id, c5052_location_id, c5010_tag_id
			, c5053_curr_qty,c5053_last_update_trans_id, c5053_created_by, c901_last_transaction_type, c5053_created_date)
			VALUES (v_loc_part_id, p_loc_id, p_tag_id
	          ,1,p_txn_id, p_user_id ,p_txn_type, SYSDATE);
	    END IF;
	END gm_sav_location_qty ;
	
/**********************************************************************
* Description : this procedure is used to remove set from the location
* 				this is common procedure when necessary remove set form 
*				location, ex, when reconfig available which is in location.  
* Author      : Xun
	*****************************************************************/
	PROCEDURE gm_location_set_clear ( 
	    p_txn_id IN t504_consignment.c504_consignment_id%TYPE 
	)
	AS 
		v_tagid  t5010_tag.c5010_tag_id%TYPE;
	BEGIN
		BEGIN
		 SELECT T5010.C5010_Tag_Id  
			INTO v_tagid 
		   FROM T5053_Location_Part_Mapping T5053, T504a_Consignment_Loaner T504a, T5010_Tag T5010
		  WHERE T504a.C504_Consignment_Id = p_txn_id
		    AND T504a.C504_Consignment_Id = T5010.C5010_Last_Updated_Trans_Id 
		    AND T5010.C5010_Tag_Id        = T5053.C5010_Tag_Id
		    AND T504a.C504a_Void_Fl      IS NULL
		    AND T5010.C5010_Void_Fl      IS NULL;
		EXCEPTION
		    WHEN NO_DATA_FOUND THEN
			v_tagid :=  NULL ;
		END;
		
		IF v_tagid IS NOT NULL
		     THEN
				DELETE FROM t5053_location_part_mapping t5053
				  Where T5053.C5010_Tag_Id = v_tagid ;
		 END IF;
		  
	END gm_location_set_clear;	
	 
END gm_pkg_op_set_txn;
/