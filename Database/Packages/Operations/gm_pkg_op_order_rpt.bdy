/* Formatted on 2009/09/17 10:48 (Formatter Plus v4.8.0) */
-- @"C:\database\Packages\Operations\gm_pkg_op_order_rpt.bdy"

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_order_rpt
IS

	/*************************************************************************
	* Purpose   : Function to validate any RA created for loaner to consign swap
	* Parameters: p_parent_ord_id ,p_type
	* Author    : Karthik
	**************************************************************************/	
    FUNCTION GET_PARTSWAP_RA_STATUS( 
         p_parent_ord_id         t501_order.c501_parent_order_id%TYPE
        ,p_type                  t501_order.c901_order_type%TYPE
    )
       RETURN CHAR
        IS
		v_ra_count CHAR(1);
		BEGIN
		IF (p_type='2531') --2531:Loan-Consign Swap
		THEN
		BEGIN
		           select DECODE(count(1),0,'N','Y')
		           INTO v_ra_count
		           from t506_returns where c501_order_id =p_parent_ord_id
		           AND c506_type='3300'   --Sales - Items
		           AND c506_reason='3317' --Parts Swap (Loaner to Consign)
		           AND c506_void_fl is NULL;
		EXCEPTION
		 WHEN NO_DATA_FOUND
		THEN
		 RETURN 'N';
		 END;
		END IF;
		RETURN v_ra_count;
		END GET_PARTSWAP_RA_STATUS;
		
		
	/*************************************************************************
	* Purpose   : Function to get the difference of total qty in Ack order and BBA order
	* Parameters: p_ack_order_id 
	* Author    : arajan
	**************************************************************************/	
    FUNCTION get_released_qty( 
         p_ack_order_id         t501_order.c501_parent_order_id%TYPE
    )
       RETURN NUMBER
        IS
		v_total_qty		NUMBER;
		BEGIN
			SELECT NVL(SUM( NVL(ack_qty,0)) - SUM(NVL(ord_qty,0)),0)
				INTO v_total_qty FROM 
				(SELECT min(itemOrdId) item_ord_id,SUM( NVL(ack_qty,0)) ack_qty, SUM(NVL(bba_qty,0)) ord_qty, ack_order.c205_part_number_id pnum
							  FROM
							    (SELECT t501.c501_order_id, t502.c205_part_number_id, SUM(t502.c502_item_qty) ack_qty, min(t502.c502_item_order_id) itemOrdId
							      FROM t501_order t501 , t502_item_order t502
							      WHERE t501.c501_order_id = t502.c501_order_id
							        AND T501.C901_ORDER_TYPE = 101260 -- Acknowledgment Order
							        AND T501.C501_ORDER_ID   = p_ack_order_id
							        AND T501.C501_VOID_FL   IS NULL
							        AND T502.C502_VOID_FL   IS NULL
							      GROUP BY t501.c501_order_id,
							        t502.c205_part_number_id
							    ) ack_order ,
							  (SELECT t501.c501_ref_id, t502.c205_part_number_id, SUM(t502.c502_item_qty) bba_qty
							    FROM t501_order t501 , t502_item_order t502
							    WHERE T501.C501_ORDER_ID = T502.C501_ORDER_ID
							      AND T501.C501_REF_ID     = p_ack_order_id
							      AND T501.C501_VOID_FL   IS NULL
							      AND T502.C502_VOID_FL   IS NULL
							    GROUP BY t501.c501_ref_id,
							      t502.c205_part_number_id
							    ) bba_order
							WHERE ACK_ORDER.C501_ORDER_ID     = BBA_ORDER.C501_REF_ID (+)
							  and ACK_ORDER.C205_PART_NUMBER_ID = BBA_ORDER.C205_PART_NUMBER_ID (+)
							  group by ACK_ORDER.C205_PART_NUMBER_ID);
			RETURN v_total_qty;
		END get_released_qty;

	/*************************************************************************
	* Purpose   : Procedure to validate the plant selected for the part number
	* Parameters: p_pnum
	* Author    : arajan
	**************************************************************************/	
	PROCEDURE gm_validate_pnum_plant(
		 p_pnum		IN	t205_part_number.c205_part_number_id%TYPE
	)
	AS
		v_company_id    	T1900_COMPANY.C1900_COMPANY_ID%TYPE;
		v_plant_id			T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
		v_viacell_plant_fl  T906_RULES.C906_RULE_VALUE%TYPE;
		v_viacell_cmpny_fl  T906_RULES.C906_RULE_VALUE%TYPE;
		v_part_num			T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE;
		v_proj_id			T202_PROJECT.C202_PROJECT_ID%TYPE;
		v_viacell_fl 		T906_RULES.C906_RULE_VALUE%TYPE;
		v_tissue_count      NUMBER;
	BEGIN
		
		SELECT get_compid_frm_cntx(), get_plantid_frm_cntx()
		  INTO v_company_id, v_plant_id
		  FROM dual;
		  
		-- Get the flag from rules table, if it is 'Y', then the company has a seperate plant for viacell parts 
		SELECT NVL(get_rule_value(v_company_id,'VIACELLCMPNY'),'N')
			INTO v_viacell_cmpny_fl 
			FROM DUAL;
		-- If the value is not 'Y', we do not need to validate the plant
	   	IF v_viacell_cmpny_fl IS NULL OR v_viacell_cmpny_fl <> 'Y' 
	   	THEN
		   RETURN;
	    END IF;
	   
		--Get the viacell plant flag from rules table by passing the rule id as plant id and rule group as �VIACELLPLANT�
		SELECT NVL(get_rule_value(v_plant_id,'VIACELLPLANT'),'N')
			INTO v_viacell_plant_fl 
			FROM DUAL;
		
		IF p_pnum IS NOT NULL OR p_pnum != ''
		THEN
			BEGIN      
				-- Get the project id of the part number to check whether the part number is viacell part or not 
			     SELECT c202_project_id 
					INTO v_proj_id	
				  FROM t205_part_number 
				  WHERE c205_part_number_id = p_pnum
				  	AND c205_active_fl = 'Y';
				 	EXCEPTION
					WHEN NO_DATA_FOUND THEN
					v_proj_id := NULL;
				END;
				
	       	 	 v_viacell_fl 	:= NVL(get_rule_value(v_proj_id,'VIACELL'),'N');  --Rule value := 'Y'
				/*
				 * If the v_viacell_fl is coming as 'Y' and the v_viacell_plant_fl  is not equal to 'Y'
				 * or if the v_viacell_fl is not equal to 'Y' and if the v_viacell_plant_fl is equal to 'Y'
				 * Then validate Part number based on the plant
				 */
	       	 	 --Validating part is a tissue part for PMT-39890
				 SELECT COUNT(1) INTO v_tissue_count
					FROM v205_parts_with_ext_vendor
					WHERE c205_part_number_id=p_pnum
					AND c1900_company_id     =v_company_id;				 
				 
				IF (v_viacell_fl = 'Y' AND v_viacell_plant_fl != 'Y') THEN
					GM_RAISE_APPLICATION_ERROR('-20999','342',p_pnum||'#$#'||'San Antonio - Globus');
				ELSIF(v_viacell_fl != 'Y' AND v_viacell_plant_fl = 'Y') THEN
					GM_RAISE_APPLICATION_ERROR('-20999','342',p_pnum||'#$#'||'Audubon');
				-- PMT-49848 To validate tissue part in new order
				ELSIF(v_tissue_count > 0) THEN  
					RAISE_APPLICATION_ERROR('-20999', 'TISSUEPART');
				END IF;
		END IF;
			
	END gm_validate_pnum_plant;
		
	/*************************************************************************
	* Purpose   : Procedure to validate the plant selected for the part number 
	* 				[overloaded procedure of gm_validate_pnum_plant()]. 
	* Parameters: p_pnum
	* Author    : arajan
	**************************************************************************/	
	PROCEDURE gm_validate_pnum_plant (
	    p_pnum		IN	t205_part_number.c205_part_number_id%TYPE
	  , p_out_str	OUT VARCHAR2
	)
	AS
	BEGIN
		p_out_str := NULL;
		BEGIN
		    gm_validate_pnum_plant(p_pnum);
			EXCEPTION 
			WHEN OTHERS THEN
			p_out_str := SQLERRM ;
		END;
		p_out_str :=  SUBSTR(trim(p_out_str) , INSTR(trim(p_out_str) , ':', -1)+1, length(p_out_str) );
	END gm_validate_pnum_plant;
	
	/******************************************************************************************************
	* Description : Procedure to fetch all the child orders including parent order for the passed order id
	*******************************************************************************************************/	
	PROCEDURE gm_fch_all_orders (
        p_ordid          IN   t501_order.c501_order_id%TYPE
       ,p_orders         OUT   TYPES.cursor_type
	)
	AS
		v_parent_ord_id   t501_order.c501_parent_order_id%TYPE;
		v_specific_comments t906_rules.c906_rule_value%TYPE;
		
   	BEGIN
	   	--get the parent id by passing the p_ordid 
	   	--if it is not a child order then the below function will return passed order id   
		SELECT get_parent_order_id(p_ordid)
		 INTO v_parent_ord_id
		FROM dual;
		--if the above function return NULL, then assign the parameter p_ordid 
		IF v_parent_ord_id IS NULL
		THEN
			v_parent_ord_id :=p_ordid;
		END IF;
		--getting the child order comments from rules
		SELECT GET_RULE_VALUE('CHILD_ORDER_HOLD','ORDER_COMMENTS') 
		 INTO v_specific_comments
		FROM dual;

	   	--fetch all the child orders including parent order
	   OPEN p_orders FOR 
	    		
				 SELECT c501_order_id ORDERID , decode(C501_PARENT_ORDER_ID,null,'',decode(c501_order_id,p_ordid,'',v_specific_comments))  SPECIFIC_COMMENTS
					FROM t501_order
				   WHERE (c501_parent_order_id = v_parent_ord_id OR c501_order_id = v_parent_ord_id)--to handle Parent / Child Reference orders
				   		AND c503_invoice_id is null
				   		AND c501_hist_hold_fl is null
						 AND NVL (c901_order_type, -9999) NOT IN 
						 		(SELECT C906_RULE_VALUE FROM v901_ORDER_TYPE_GRP
								 )
						 AND c501_VOID_FL IS NULL;
						 
 	END gm_fch_all_orders;
 	/******************************************************************************************************
	* Description : Procedure to fetch all the unprocessed hold orders
	*******************************************************************************************************/	
   PROCEDURE gm_fch_unprocessed_hold_orders (
        p_orders         OUT   TYPES.cursor_type
	)
	AS		
		v_company_id    	T1900_COMPANY.C1900_COMPANY_ID%TYPE;
   	BEGIN
		
		SELECT get_compid_frm_cntx()
		  INTO v_company_id
		  FROM dual;
	   	--fetch all the unprocessed hold orders
	   OPEN p_orders FOR 
	    		
				 SELECT c501_order_id ORDERID , C501_RECEIVE_MODE  RECEIVE_MODE
					FROM t501_order
				   WHERE C501_HOLD_ORDER_PROCESS_FL = 'N' 
				   		AND c503_invoice_id is null
				   		AND c501_hist_hold_fl is null
						 AND NVL (c901_order_type, -9999) NOT IN 
						 		(SELECT C906_RULE_VALUE FROM v901_ORDER_TYPE_GRP
								 )
						 AND c501_VOID_FL IS NULL
						 AND C1900_COMPANY_ID = v_company_id;
						 
 	END gm_fch_unprocessed_hold_orders;
 	
 	/******************************************************************************************************
	* Description : Procedure to fetch the set details associated to the tag id
	* Author	  : Karthik
	*******************************************************************************************************/	
	PROCEDURE gm_fch_DO_tag_list (
        p_tag_id         	   IN   T5010_Tag.C5010_Tag_Id%TYPE
       ,p_out_tag_dtls         OUT   TYPES.cursor_type
	)
	AS
		v_company_id    	T1900_COMPANY.C1900_COMPANY_ID%TYPE;
		v_plant_id			T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
		v_tag_cnt			NUMBER;
   	BEGIN
	   	
	  SELECT get_compid_frm_cntx(), get_plantid_frm_cntx()
		  INTO v_company_id, v_plant_id
		  FROM dual;
		  
	   	--fetch the set details associated to the tag id
   		OPEN p_out_tag_dtls FOR 
			SELECT T5010.C5010_Tag_Id tagid,
			  T5010.C207_Set_Id setid,
			  Get_Set_Name(T5010.C207_Set_Id) setnm
			FROM T5010_Tag T5010
			WHERE T5010.C5010_Tag_Id   = p_tag_id
			AND T5010.C5010_Void_Fl   IS NULL
			AND T5010.C1900_Company_Id = v_company_id
			GROUP BY  T5010.C5010_Tag_Id , T5010.C207_Set_Id;
						 
 	END gm_fch_DO_tag_list;
 	
/******************************************************************************************************
  * Description : Procedure to fetch part number and product family details 
  * Author	  : Suganthi
*******************************************************************************************************/		
PROCEDURE gm_fch_partprod_fmly(
    p_ref_id IN t501_order.c501_ref_id%TYPE ,
    p_source IN T901_CODE_LOOKUP.c901_code_id%type ,
    p_out_str OUT TYPES.cursor_type )
AS
BEGIN
  --50180 Order
  IF p_source = '50180' THEN
    OPEN p_out_str FOR
    SELECT DISTINCT T502.C205_PART_NUMBER_ID pnum, get_partnum_product(T502.C205_PART_NUMBER_ID) pdtfmly
    FROM T501_ORDER T501, T502_ITEM_ORDER T502
    WHERE t501.c501_order_id = t502.c501_order_id
    AND T501.C501_ORDER_ID   = p_ref_id
    AND t501.c501_void_fl   IS NULL
    AND T502.C502_VOID_FL   IS NULL;
  ELSE
    OPEN P_OUT_STR FOR
    SELECT T505.C205_PART_NUMBER_ID PNUM, GET_PARTNUM_PRODUCT(T505.C205_PART_NUMBER_ID) PDTFMLY
    FROM T504_CONSIGNMENT T504, T505_ITEM_CONSIGNMENT T505 
    WHERE T504.C504_CONSIGNMENT_ID = T505.C504_CONSIGNMENT_ID 
    AND T504.C504_CONSIGNMENT_ID = p_ref_id 
    AND T504.C504_VOID_FL IS NULL 
    AND T505.C505_VOID_FL IS NULL;
  END IF;
END gm_fch_partprod_fmly;

 /******************************************************************************************************
* Description : Function to fetch parent id used order id
* Author      :  N RAJA
*******************************************************************************************************/   
FUNCTION gm_parent_order_id(
     p_order_id IN t501_order.c501_order_id%TYPE)
     RETURN t501_order.c501_parent_order_id%TYPE
IS
    v_do_id t501_order.c501_parent_order_id%TYPE;
BEGIN
    BEGIN
	 SELECT NVL(c501_parent_order_id , c501_order_id)
	   INTO v_do_id
	   FROM t501_order
	  WHERE c501_order_id = p_order_id 
		AND c501_void_fl IS NULL;
	
	EXCEPTION
	WHEN NO_DATA_FOUND THEN
	    RETURN NULL;
	END;    
	     RETURN v_do_id;
 END gm_parent_order_id;

END gm_pkg_op_order_rpt;
/ 			