CREATE OR REPLACE PACKAGE BODY "GM_PKG_OP_MANUAL_ICT"
IS
/******************************************************************************
 NAME:	gm_pkg_op_ict
 PURPOSE: This package holds the procedures for ICT project

 REVISIONS:
 Ver		 Date		 Author
 --------- ---------- --------------- ------------------------------------
 1.0		 6/26/2008 	Brinal		1. Created this package.
******************************************************************************/

	/*******************************************************
 * Purpose: This Procedure is main
 *******************************************************/
--
	PROCEDURE gm_op_sav_initiateict (
		p_ref_id	 IN		t504_consignment.c504_consignment_id%TYPE
	 , p_type		 IN		t504_consignment.c504_type%TYPE --50250
	 , p_poid		 IN OUT	t501_order.c501_customer_po%TYPE
	 , p_freightamt IN		t501_order.c501_ship_cost%TYPE
	 , p_userid	 IN		t504_consignment.c504_created_by%TYPE
	 , p_dist_id	 IN		t504_consignment.c701_distributor_id%TYPE
	 , p_purpose	 IN		t504_consignment.c504_inhouse_purpose%TYPE
	)
	AS
		v_order_id	 t501_order.c501_order_id%TYPE;
		v_poid		 t501_order.c501_customer_po%TYPE;
		v_frt_order_id t501_order.c501_order_id%TYPE;
		v_frt_poid	 t501_order.c501_customer_po%TYPE;
		v_inv_id	 t503_invoice.c503_invoice_id%TYPE;
		v_dist_type t701_distributor.c901_distributor_type%TYPE;
		v_inter_party_id t101_party.c101_party_id%TYPE;
		v_account_id t501_order.c704_account_id%TYPE;
		v_count 	 NUMBER;
		v_orderid	 t501_order.c501_order_id%TYPE;
	BEGIN
		SELECT get_distributor_type (p_dist_id), get_distributor_inter_party_id (p_dist_id)
			 , get_rule_value (get_distributor_inter_party_id (p_dist_id), 'ICTACCOUNTMAP')
		 INTO v_dist_type, v_inter_party_id
			 , v_account_id
		 FROM DUAL;

		IF v_account_id IS NULL
		THEN
			raise_application_error (-20910, '');
		END IF;

		gm_pkg_op_manual_ict.gm_op_sav_ict_order (p_ref_id
												, p_type
												, p_userid
												, p_dist_id
												, p_freightamt
												, v_order_id
												, v_poid
												 );
		gm_pkg_op_manual_ict.gm_op_sav_ict_invoice (v_order_id, 50200, v_inv_id);
		DBMS_OUTPUT.put_line ('Init ICT for ' || p_ref_id || 'Order - ' || v_order_id || 'Invoice - ' || v_inv_id);
--		gm_procedure_log ('ICT ', 'ICT: ' || p_ref_id || 'Order - ' || v_order_id || ' Invoice - ' || v_inv_id);

		---50060 regular, 50061 ICT Sample
		IF p_purpose = 50061
		THEN
			UPDATE t505_item_consignment
			 SET c505_item_price = 0
			 WHERE c504_consignment_id = p_ref_id AND c505_void_fl IS NULL;

			UPDATE t502_item_order
			 SET c502_item_price = 0
			 WHERE c501_order_id = v_order_id AND c502_void_fl IS NULL;
		ELSE
			gm_update_consign_price (p_ref_id, v_inter_party_id);
		END IF;

		IF v_dist_type = 70104	 -- foreign Distributor
		THEN
			gm_pkg_op_manual_ict.gm_op_sav_dist_csg_invoice (p_poid, p_ref_id, p_userid, p_freightamt);
		END IF;
		
		--When ever the detail is updated, the master table has to be updated ,so ETL can Sync it to GOP.
		UPDATE t504_consignment
		   SET  c504_last_updated_by	= p_userid
		       ,c504_last_updated_date	= SYSDATE
		 WHERE c504_consignment_id	= p_ref_id;
		
		  UPDATE t501_order
			SET c501_last_updated_by =  p_userid
				,c501_last_updated_date = SYSDATE
		  WHERE c501_order_id = v_order_id;
	END gm_op_sav_initiateict;

 /*******************************************************
 * Purpose: This Procedure is used to generate	order
 **********************************************************/
--
	PROCEDURE gm_op_sav_ict_order (
		p_ref_id	 IN		t504_consignment.c504_consignment_id%TYPE
	 , p_type		 IN		t504_consignment.c504_type%TYPE
	 , p_userid	 IN		t504_consignment.c504_created_by%TYPE
	 , p_dist_id	 IN		t504_consignment.c701_distributor_id%TYPE
	 , p_freightamt IN		t501_order.c501_ship_cost%TYPE
	 , p_order_id	 OUT		t501_order.c501_order_id%TYPE
	 , p_poid		 OUT		t501_order.c501_customer_po%TYPE
	)
	AS
		v_created_by t504_consignment.c504_created_by%TYPE;
		v_account_id t501_order.c704_account_id%TYPE;
		cur_part_details TYPES.cursor_type;
		--rec_part_details		gm_op_fch_part_detail.p_part_details%rowtype;
		c_pnumid	 t502_item_order.c205_part_number_id%TYPE;
		c_itemq 	 t502_item_order.c502_item_qty%TYPE;
		c_controlnum t502_item_order.c502_control_number%TYPE;
		c_voidflg	 t502_item_order.c502_void_fl%TYPE;
		c_price 	 NUMBER;
		v_post_id	 t906_rules.c906_rule_value%TYPE;
		v_total 	 NUMBER;
		v_dist_type t701_distributor.c901_distributor_type%TYPE;
		v_party_id	 t101_party.c101_party_id%TYPE;
		v_dist_party_id t101_party.c101_party_id%TYPE;
		v_sign		 NUMBER := 1;
		v_shipid	 NUMBER;
		v_count 	 NUMBER;
	BEGIN
		-- get the order id ,account id, poid, dist type, inter_party_id
		SELECT get_rule_value (get_distributor_inter_party_id (p_dist_id), 'ICTACCOUNTMAP')
			 , get_distributor_inter_party_id (p_dist_id)
			 , TO_CHAR (SYSDATE, 'YYYYMMDD') || '-' || s501_order_ict_po.NEXTVAL, get_distributor_type (p_dist_id)
		 INTO v_account_id
			 , v_dist_party_id
			 , p_poid, v_dist_type
		 FROM DUAL;

		SELECT get_distributor_inter_party_id (t703.c701_distributor_id)
		 INTO v_party_id
		 FROM t704_account t704, t703_sales_rep t703
		 WHERE t704.c704_account_id = v_account_id AND t704.c703_sales_rep_id = t703.c703_sales_rep_id;

		-- get the created by
		IF p_type = 50250
		THEN --consignment
			SELECT c504_created_by, get_next_order_id (NULL), 1
			 INTO v_created_by, p_order_id, v_sign
			 FROM t504_consignment
			 WHERE c504_consignment_id = p_ref_id;
		ELSIF p_type = 50251
		THEN --return
			SELECT c506_created_by, 'GM' || s501_order.NEXTVAL || 'R', -1
			 INTO v_created_by, p_order_id, v_sign
			 FROM t506_returns
			 WHERE c506_rma_id = p_ref_id;
		END IF;

		-- receive_mode = 5015 (fax)
		--dlvry carr = 5040 (N/A)
		-- dlvry mode = 5031(N/A)
		gm_save_item_order (p_order_id
						 , v_account_id
						 , 5015
						 , NULL
						 , NULL
						 , p_poid
						 , 0
						 , v_created_by
						 , NULL
						 , 2533
						 , NULL
						 , NULL
						 , NULL
						 , NULL
						 , 5040
						 , 5031
						 , NULL
						 , v_shipid
						 );
		gm_pkg_op_manual_ict.gm_op_fch_part_detail (p_ref_id, p_type, v_account_id, 70103, v_party_id, cur_part_details);	-- 70103 as only ICT Order

		--	v_post_id	:= get_rule_value (p_type, 'ORDERICT');
		SELECT COUNT (1)
		 INTO v_count
		 FROM t504_consignment
		 WHERE c504_consignment_id = p_ref_id AND c504_inhouse_purpose = 50061 AND c504_void_fl IS NULL;

		---50060 regular, 50061 ICT Sample, 40057 ICT
	 --iterate
--	 FOR set_val IN v_part_details
		LOOP
			FETCH cur_part_details
			 INTO c_pnumid, c_itemq, c_controlnum, c_voidflg, c_price;

			EXIT WHEN cur_part_details%NOTFOUND;

			IF (c_price IS NULL AND v_count = 0)
			THEN
				raise_application_error (-20911, '');
			END IF;

			IF (c_price IS NULL AND v_count > 0)
			THEN
				c_price 	:= 0;
			END IF;

			gm_pkg_op_order_master.gm_op_sav_order_detail (p_order_id
														 , c_pnumid
														 , c_itemq * v_sign
														 , c_controlnum
														 , c_price
														 , ''
														 , c_voidflg
														 );
		/*
			gm_save_ledger_posting (v_post_id, SYSDATE, c_pnumid, v_account_id, p_ref_id, c_itemq, c_price, v_created_by);

		*/
		END LOOP;

		CLOSE cur_part_details;

		--end iterate
		SELECT SUM (NVL (TO_NUMBER (c502_item_qty) * TO_NUMBER (c502_item_price), 0))
		 INTO v_total
		 FROM t502_item_order
		 WHERE c501_order_id = p_order_id;

		UPDATE t501_order
		 SET c501_status_fl = 2
			 , c501_total_cost = NVL (v_total, 0)
			 , c501_shipping_date = TRUNC (SYSDATE)
			 , c501_last_updated_by = p_userid
			 , c501_last_updated_date = SYSDATE
			 , c501_ship_cost = p_freightamt
		 WHERE c501_order_id = p_order_id;

		IF p_type = 50250
		THEN
			INSERT INTO t504c_consignment_ict_detail
						(c504_consignment_id, c501_order_id, c504c_created_by, c504c_created_date
						)
				 VALUES (p_ref_id, p_order_id, p_userid, SYSDATE
						);
		END IF;
	END gm_op_sav_ict_order;

 /*******************************************************
 * Purpose: This Procedure is used to generate invoice
 *******************************************************/
--
	PROCEDURE gm_op_sav_ict_invoice (
		p_order_id	 IN 	 t501_order.c501_order_id%TYPE
	 , p_invtype	 IN 	 t503_invoice.c901_invoice_type%TYPE
	 , p_inv_id	 OUT	 t503_invoice.c503_invoice_id%TYPE
	)
	AS
		v_custpo	 t501_order.c501_customer_po%TYPE;
		v_accid 	 t501_order.c704_account_id%TYPE;
		v_created_by t501_order.c501_created_by%TYPE;
		v_payterms	 t704_account.c704_payment_terms%TYPE;
	BEGIN
		SELECT c704_account_id, c501_customer_po, c501_created_by
		 INTO v_accid, v_custpo, v_created_by
		 FROM t501_order
		 WHERE c501_order_id = p_order_id;

		SELECT c704_payment_terms
		 INTO v_payterms
		 FROM t704_account
		 WHERE c704_account_id = v_accid;

		gm_generate_invoice (v_custpo
						 , v_accid
						 , 0
						 , TO_CHAR (SYSDATE, 'mm/dd/yyyy')
						 , v_created_by
						 , 'ADD'
						 , p_invtype
						 , 50257
						 , p_inv_id
							);

		UPDATE t501_order
		 SET c503_invoice_id = p_inv_id
			 , c501_status_fl = 3 
			 , c501_last_updated_by = v_created_by
			 , c501_last_updated_date = SYSDATE
		 WHERE c501_order_id = p_order_id;
		 
		 gm_pkg_ac_invoice_txn.gm_sav_invoice_amount(p_inv_id,v_created_by);
		 
	END gm_op_sav_ict_invoice;

		/*******************************************************
 * Purpose: This Procedure is used to generate	invoice and
 *******************************************************/
--
	PROCEDURE gm_op_sav_dist_invoice (
		p_poid		 IN		t501_order.c501_customer_po%TYPE
	 , p_distid	 IN		t701_distributor.c701_distributor_id%TYPE
	 , p_ref_id	 IN		t503a_invoice_txn.c503a_ref_id%TYPE
	 , p_created_by IN		t504_consignment.c504_created_by%TYPE
	 , p_userid	 IN		t101_user.c101_user_id%TYPE
	 , p_totalcost IN		t503a_invoice_txn.c503a_total_amt%TYPE
	 , p_freightamt IN		t501_order.c501_ship_cost%TYPE
	 , p_invtype	 IN		t503_invoice.c901_invoice_type%TYPE
	 , p_reftype	 IN		t503a_invoice_txn.c901_ref_type%TYPE
	 , p_outinvid	 OUT		t503_invoice.c503_invoice_id%TYPE
	)
	AS
	BEGIN
		-- p_invtype - 50200 regular
		gm_generate_invoice (p_poid
						 , NULL
						 , NULL
						 , TO_CHAR (SYSDATE, 'mm-dd-yyyy')
						 , p_created_by
						 , 'ADD'
						 , p_invtype
						 , 50256
						 , p_outinvid
							);

		INSERT INTO t503a_invoice_txn
					(c503a_invoice_txn_id, c503_invoice_id, c901_ref_type, c503a_ref_id, c503a_customer_po
				 , c503a_total_amt, c503a_last_updated_by, c503a_last_updated_date
					)
			 VALUES (s503a_invoice_txn_id.NEXTVAL, p_outinvid, p_reftype, p_ref_id, p_poid
				 , p_totalcost, p_userid, SYSDATE
					);

		UPDATE t503_invoice
		 SET c701_distributor_id = p_distid
		 WHERE c503_invoice_id = p_outinvid;
		 
		 gm_pkg_ac_invoice_txn.gm_sav_invoice_amount(p_outinvid,p_userid);
		 
	END gm_op_sav_dist_invoice;

 /*******************************************************
 * Purpose: This Procedure is used to generate	consignment/return invoice (distributor invoice)
 *******************************************************/
--
	PROCEDURE gm_op_sav_dist_csg_invoice (
		p_poid		 IN	t501_order.c501_customer_po%TYPE
	 , p_csg_id	 IN	t504_consignment.c504_consignment_id%TYPE
	 , p_userid	 IN	t504_consignment.c504_created_by%TYPE
	 , p_freightamt IN	t501_order.c501_ship_cost%TYPE
	)
	AS
		v_custpo	 t501_order.c501_customer_po%TYPE;
		v_created_by t501_order.c501_created_by%TYPE;
		v_dist_id	 t504_consignment.c701_distributor_id%TYPE;
		p_inv_id	 t503_invoice.c503_invoice_id%TYPE;
		v_total 	 t503a_invoice_txn.c503a_total_amt%TYPE;
		v_conv_freight t501_order.c501_ship_cost%TYPE;
		v_outinvid	 t503_invoice.c503_invoice_id%TYPE;
	BEGIN
		IF p_poid IS NULL
		THEN
			SELECT TO_CHAR (SYSDATE, 'YYYYMMDD') || '-' || s501_order_ict_po.NEXTVAL
			 INTO v_custpo
			 FROM DUAL;
		ELSE
			v_custpo	:= p_poid;
		END IF;

		SELECT c504_created_by, c701_distributor_id
		 INTO v_created_by, v_dist_id
		 FROM t504_consignment
		 WHERE c504_consignment_id = p_csg_id;

		SELECT SUM (NVL (TO_NUMBER (c505_item_qty) * TO_NUMBER (c505_item_price), 0))
		 INTO v_total
		 FROM t505_item_consignment
		 WHERE c504_consignment_id = p_csg_id;

		SELECT get_currency_conversion (get_rule_value (50219, 'USD')
									 , get_rule_value (50219
													 , get_rule_value (50218
																	 , get_distributor_inter_party_id (v_dist_id)
																	 )
													 )
									 , SYSDATE
									 , p_freightamt
									 )
		 INTO v_conv_freight
		 FROM DUAL;

		UPDATE t504_consignment
		 SET c504_ship_cost = v_conv_freight
		     ,c504_last_updated_by	= p_userid
		     ,c504_last_updated_date= SYSDATE
		 WHERE c504_consignment_id = p_csg_id;

		gm_op_sav_dist_invoice (v_custpo
							 , v_dist_id
							 , p_csg_id
							 , v_created_by
							 , p_userid
							 , v_total
							 , v_conv_freight
							 , 50200
							 , 50258
							 , v_outinvid
							 );
	END gm_op_sav_dist_csg_invoice;

/*******************************************************
 * Purpose: This Procedure is used to generate return invoice (distributor invoice)
 *******************************************************/
--
	PROCEDURE gm_op_sav_dist_ret_invoice (
		p_return_id IN	 t506_returns.c506_rma_id%TYPE
	 , p_userid	 IN	 t506_returns.c506_created_by%TYPE
	 , p_outinvid	 OUT	 t503_invoice.c503_invoice_id%TYPE
	)
	AS
		v_custpo	 t501_order.c501_customer_po%TYPE;
		v_created_by t501_order.c501_created_by%TYPE;
		v_dist_id	 t506_returns.c701_distributor_id%TYPE;
		v_total 	 t503a_invoice_txn.c503a_total_amt%TYPE;
	BEGIN
		SELECT TO_CHAR (SYSDATE, 'YYYYMMDD') || '-' || s501_order_ict_po.NEXTVAL
		 INTO v_custpo
		 FROM DUAL;

		SELECT c506_created_by, c701_distributor_id
		 INTO v_created_by, v_dist_id
		 FROM t506_returns
		 WHERE c506_rma_id = p_return_id;

		SELECT SUM (NVL (TO_NUMBER (c507_item_qty) * -1 * TO_NUMBER (c507_item_price), 0))
		 INTO v_total
		 FROM t507_returns_item
		 WHERE c506_rma_id = p_return_id;

		gm_op_sav_dist_invoice (v_custpo
							 , v_dist_id
							 , p_return_id
							 , v_created_by
							 , p_userid
							 , v_total
							 , 0
							 , 50202
							 , 50259
							 , p_outinvid
							 );
	END gm_op_sav_dist_ret_invoice;

 /*******************************************************
 * Purpose: This Procedure is used to fetch part detail
 *******************************************************/
--
	PROCEDURE gm_op_fch_part_detail (
		p_ref_id		 IN 	 t504_consignment.c504_consignment_id%TYPE
	 , p_type			 IN 	 t504_consignment.c504_type%TYPE
	 , p_account_id	 IN 	 t501_order.c704_account_id%TYPE
	 , p_dist_type 	 IN 	 t701_distributor.c901_distributor_type%TYPE
	 , p_party_id		 IN 	 t101_party.c101_party_id%TYPE
	 , p_part_details	 OUT	 TYPES.cursor_type
	)
	AS
		v_order_id	 t501_order.c501_order_id%TYPE;
--	 v_price	 t705_account_pricing.c705_unit_price%TYPE;
		c_pnumid	 t502_item_order.c205_part_number_id%TYPE;
		c_itemq 	 t502_item_order.c502_item_qty%TYPE;
		c_controlnum t502_item_order.c502_control_number%TYPE;
		c_voidflg	 t502_item_order.c502_void_fl%TYPE;
		c_price 	 NUMBER;
	BEGIN
		IF p_type = 50250
		THEN
			--consignment
			OPEN p_part_details
			 FOR
				 SELECT c205_part_number_id, c505_item_qty, c505_control_number, c505_void_fl
					 , DECODE (p_dist_type
							 , '70103', get_account_part_pricing (NULL, c205_part_number_id, p_party_id)
							 , get_account_part_pricing (p_account_id, c205_part_number_id, NULL)
							 ) price
				 FROM t505_item_consignment
				 WHERE c504_consignment_id = p_ref_id;
		ELSE
			OPEN p_part_details
			 FOR
				 SELECT c205_part_number_id, c507_item_qty, c507_control_number, NULL
					 , get_account_part_pricing (NULL, c205_part_number_id, p_party_id) price
				 FROM t507_returns_item
				 WHERE c506_rma_id = p_ref_id AND c507_status_fl = 'C';
		END IF;
	END gm_op_fch_part_detail;

 /*******************************************************
 * Purpose: This Procedure is used to fetch ICT Summary
 *******************************************************/
--
	PROCEDURE gm_op_fch_ict_summary (
		p_ref_id	IN		 t504_consignment.c504_consignment_id%TYPE
	 , p_summary	OUT 	 TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_summary
		 FOR
			 SELECT t504.c504_consignment_id consignid, TO_CHAR (t504.c504_ship_date, 'MM/DD/YYYY') shipdate
					-- SQL for ICT
					 ,get_user_name (t504.c504_last_updated_by) transby, t501.c503_invoice_id invoiceid
					, t501.c501_customer_po poid, get_code_name (t501.c901_order_type) invtype
					, t501.c901_order_type invtypeid
					, (DECODE (t504.c504_inhouse_purpose, 50061, 0, t501.c501_total_cost) + t501.c501_ship_cost
					 ) invamt, 'USD' currency, --ict will alwys be USD
											 t501.c501_order_id orderid
					, DECODE (t701.c901_distributor_type
							, 70103, get_distributor_name (t504.c701_distributor_id)
							, get_account_name (t501.c704_account_id)
							 ) acctype
				 FROM t504_consignment t504, t504c_consignment_ict_detail t504c, t501_order t501
					, t701_distributor t701
				WHERE t504.c504_consignment_id = p_ref_id
				 AND t504.c504_consignment_id = t504c.c504_consignment_id
				 AND t501.c501_order_id = t504c.c501_order_id
				 AND t701.c701_distributor_id = t504.c701_distributor_id
			 UNION ALL
			 SELECT t504.c504_consignment_id consignid, TO_CHAR (t504.c504_ship_date, 'MM/DD/YYYY') shipdate
					-- SQL for Foreign Distributor
					 ,get_user_name (t504.c504_last_updated_by) transby, t503a.c503_invoice_id invoiceid
					, t503a.c503a_customer_po poid, get_code_name (t503a.c901_ref_type) invtype
					, t503a.c901_ref_type invtypeid
					, (DECODE (t504.c504_inhouse_purpose, 50061, 0, t503a.c503a_total_amt) + t504.c504_ship_cost
					 ) invamt
					, get_rule_value (50218, get_distributor_inter_party_id (t504.c701_distributor_id)) currency
					, t504.c504_consignment_id orderid, get_distributor_name (t504.c701_distributor_id) acctype
				 FROM t503a_invoice_txn t503a, t504_consignment t504
				WHERE t504.c504_consignment_id = p_ref_id AND t504.c504_consignment_id = t503a.c503a_ref_id
			 ORDER BY invoiceid DESC;
	END gm_op_fch_ict_summary;

		/*******************************************************
	* Purpose: This Procedure is used to save ICT returns
	*******************************************************/
	PROCEDURE gm_po_sav_ict_returns (
		p_raid		 IN t506_returns.c506_rma_id%TYPE
	 , p_distid	 IN t701_distributor.c701_distributor_id%TYPE
	 , p_userid	 IN t506_returns.c506_last_updated_by%TYPE
	 , p_dist_type IN t701_distributor.c901_distributor_type%TYPE
	)
	AS
		v_order_id	 t501_order.c501_order_id%TYPE;
		v_po_id 	 t501_order.c501_customer_po%TYPE;
		v_account_id t501_order.c704_account_id%TYPE;
		v_newictinvid t503_invoice.c503_invoice_id%TYPE;
		v_newdistinvid t503_invoice.c503_invoice_id%TYPE;
		v_total_cost t501_order.c501_total_cost%TYPE;
		v_dist_party_id t101_party.c101_party_id%TYPE;
	BEGIN
		gm_op_sav_ict_order (p_raid, 50251, p_userid, p_distid, 0, v_order_id, v_po_id);

		SELECT c704_account_id, c501_total_cost, get_distributor_inter_party_id (p_distid)
		 INTO v_account_id, v_total_cost, v_dist_party_id
		 FROM t501_order
		 WHERE c501_order_id = v_order_id;

		gm_op_sav_ict_invoice (v_order_id, 50202, v_newictinvid);

		UPDATE t501_order
		 SET c503_invoice_id = v_newictinvid
			 , c506_rma_id = p_raid
			 , c501_last_updated_by = p_userid
			 , c501_last_updated_date = SYSDATE
		 WHERE c501_order_id = v_order_id;

		UPDATE t507_returns_item
		 SET c507_item_price = get_account_part_pricing (NULL, c205_part_number_id, v_dist_party_id)
		 WHERE c506_rma_id = p_raid;

		IF p_dist_type = 70104	 -- foreign Distributor
		THEN
			gm_op_sav_dist_ret_invoice (p_raid, p_userid, v_newdistinvid);
		END IF;

		gm_pkg_op_manual_ict.gm_op_mail_ict_return (p_raid, v_newictinvid, v_newdistinvid, v_order_id, v_total_cost);
		
		--When ever the detail is updated, the group table has to be updated ,so ETL can Sync it to GOP.
		UPDATE t506_returns
		   SET c506_last_updated_by = p_userid
		       ,c506_last_updated_date	= SYSDATE
		 WHERE c506_rma_id	= p_raid;
		 
	END gm_po_sav_ict_returns;

	/*******************************************************
	* Purpose: This Procedure emails the details of the ICT returns
	*******************************************************/
	PROCEDURE gm_op_mail_ict_return (
		p_raid			 IN	t506_returns.c506_rma_id%TYPE
	 , p_ict_invoice_id IN	t503_invoice.c503_invoice_id%TYPE
	 , p_fd_invoice_id IN	t503_invoice.c503_invoice_id%TYPE
	 , p_order_id		 IN	t501_order.c501_order_id%TYPE
	 , p_total_cost	 IN	t501_order.c501_total_cost%TYPE
	)
	AS
		subject 	 VARCHAR2 (100);
		to_mail 	 VARCHAR2 (200);
		mail_body	 VARCHAR2 (4000);
		crlf		 VARCHAR2 (2) := CHR (13) || CHR (10);
	BEGIN
		-- Email Area
		to_mail 	:= get_rule_value ('ICTRETURN', 'EMAIL');
		subject 	:= ' ICT Returns credited ';
		mail_body	:=
			 'ICT Returns has been credited. Find the details below '
			|| crlf
			|| crlf
			|| 'RA : '
			|| p_raid
			|| crlf
			|| crlf
			|| 'ICT Invoice Id : '
			|| p_ict_invoice_id
			|| crlf
			|| crlf
			|| 'FD Invoice Id : '
			|| p_fd_invoice_id
			|| crlf
			|| crlf
			|| 'Return Order Id: '
			|| p_order_id
			|| crlf
			|| crlf
			|| 'Total Cost : $'
			|| p_total_cost;
		gm_com_send_email_prc (to_mail, subject, mail_body);
	END gm_op_mail_ict_return;

	/*******************************************************
	* Purpose: Function to get the total cost of ICT
	*******************************************************/
	FUNCTION get_csg_ship_cost (
		p_ref_id IN	t503a_invoice_txn.c503a_ref_id%TYPE
	)
		RETURN NUMBER
	IS
--
		v_shipcost	 t504_consignment.c504_ship_cost%TYPE;
	BEGIN
		SELECT NVL (t504.c504_ship_cost, 0)
		 INTO v_shipcost
		 FROM t504_consignment t504
		 WHERE t504.c504_consignment_id = p_ref_id;

		RETURN v_shipcost;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN 0;
	END get_csg_ship_cost;

	/*******************************************************
	* Purpose: Function to get the release of shipment. For ICT Change
	*******************************************************/
	FUNCTION get_ship_date (
		p_ref_id IN	t907_shipping_info.c907_ref_id%TYPE
	)
		RETURN VARCHAR2
	IS
--
		v_shipdt	 VARCHAR2 (120);
	BEGIN
		SELECT TO_CHAR (c907_shipped_dt, 'MM/DD/YYYY')
		 INTO v_shipdt
		 FROM t907_shipping_info
		 WHERE c907_ref_id = p_ref_id;

		RETURN v_shipdt;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN '';
	END get_ship_date;
END gm_pkg_op_manual_ict;
/

