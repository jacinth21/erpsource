CREATE OR REPLACE PACKAGE BODY gm_pkg_wo_lead_time_txn
IS
/****************************************************************************
 * Description: This procedure is used to fetch lead time for the part number
 * Author	  : Agilan Singaravel
 ****************************************************************************/
PROCEDURE gm_fch_part_lead_time(
	p_part_num      IN    t205_part_number.c205_part_number_id%type,
	p_vendor_id		IN	  t301_vendor.c301_vendor_id%type,
	p_lead_date	    OUT	  date
)
AS
v_lead_time		DATE;
BEGIN
	
	BEGIN
		SELECT trunc(current_date + (c3011_lead_time * 7))
		  INTO v_lead_time 
		  FROM T3011_VENDOR_LEAD_TIME 
		 WHERE c205_part_number_id = p_part_num
		   AND c301_vendor_id = p_vendor_id
		   AND c3011_void_fl is null;
	EXCEPTION WHEN NO_DATA_FOUND THEN
	BEGIN
		SELECT trunc(current_date + (c205d_attribute_value * 7))
		  INTO v_lead_time 
		  FROM T205D_PART_ATTRIBUTE
		 WHERE c901_attribute_type = 4000101 --part lead time
		   AND c205_part_number_id = p_part_num
		   AND c205d_void_fl is null;
	EXCEPTION WHEN NO_DATA_FOUND THEN
		v_lead_time := trunc(current_date + (7*7));
	END;
	END;
	
	p_lead_date := v_lead_time;
	
END gm_fch_part_lead_time;

END gm_pkg_wo_lead_time_txn;
/
