/* Formatted on 2011/08/23 11:49 (Formatter Plus v4.8.0) */
/*
 * @"C:\Database\Packages\Operations\receiving\gm_pkg_op_bulk_dhr_txn.bdy";
 */

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_bulk_dhr_txn
IS
--   	 	
	/******************************************************************************
	* Description : Procedure to save the details at the time of DHR Batch Process
	*******************************************************************************/
	PROCEDURE gm_pkg_op_rec_bulk_shipment(
		  p_dhr_bulk_id  	IN OUT		t4081_dhr_bulk_receive.c4081_dhr_bulk_id%TYPE
		, p_po_number 		IN 		temp_bulk_dhr_table.c401_po_number%TYPE
		, p_date_received 	IN		t401_purchase_order.C401_PURCHASE_ORD_DATE%TYPE
		, p_pack_slip_id 	IN		T4081_DHR_BULK_RECEIVE.c4081_packing_slip%TYPE
		, p_donor_no 		IN		t2540_donor_master.c2540_donor_number%TYPE
		, p_input_str 		IN 		CLOB
		, p_user_id			IN		T4081_DHR_BULK_RECEIVE.c4081_last_updated_by%TYPE
		, p_action			IN		VARCHAR2
		, p_rej_rs_fl		IN		VARCHAR2 DEFAULT NULL
		, p_stage_date		IN      T4081_DHR_BULK_RECEIVE.C4081_STAGE_DATE%TYPE
	)
	AS
	-- Param Declaration in packages
	v_vendor_id		t401_purchase_order.c301_vendor_id%type;
	v_donor_id 		t2540_donor_master.c2540_donor_id%type;
	v_company_id    T1900_Company.c1900_company_id%TYPE;
	-- 
	BEGIN
		SELECT get_compid_frm_cntx() INTO v_company_id FROM DUAL;
		
		BEGIN
		    SELECT c301_vendor_id
		      INTO v_vendor_id
		      FROM t401_purchase_order t401
		     WHERE t401.c401_purchase_ord_id = p_po_number
		       AND t401.c1900_company_id = v_company_id
		FOR UPDATE;
		EXCEPTION WHEN NO_DATA_FOUND THEN
		    GM_RAISE_APPLICATION_ERROR('-20999','302','');
		END;
		
		--input string format PartNumber^ControlNumber^Size^ExpiryDate^ManufacturedDate^DHRID^ApprovalStatus^LocationType^Qty|
		
		-- Save the input string in temp table TEMP_BULK_DHR_TABLE
		gm_sav_dhr_input_str_tmp(p_po_number, p_input_str);
		
		/* Check and create work orders necessary, so RC DHRS
		are not created for parts that are received in addition */
		gm_op_sav_wo_for_extra_parts(p_po_number, p_action, p_user_id);
		
		-- Check  and save Donor Number, get the saved primary key in v_donor_id
		gm_pkg_op_dhr.gm_op_sav_donor_number(p_donor_no, v_vendor_id, p_user_id, v_donor_id);
		
	
		-- Check and save the shipment master information, get the saved primary key in p_dhr_bulk_id
		gm_op_sav_bulk_dhr_info(p_dhr_bulk_id, p_po_number, p_date_received, p_pack_slip_id, v_donor_id, p_user_id, p_rej_rs_fl,p_stage_date);
		
		-- Create DHR's for the received shipment
		gm_op_sav_bulk_dhr_detail(p_dhr_bulk_id, p_po_number, v_vendor_id, v_donor_id, p_pack_slip_id, p_action, p_user_id);
		 
	END gm_pkg_op_rec_bulk_shipment;
	
	
	/*************************************************************************
	* Description : Save the input string in temp table TEMP_BULK_DHR_TABLE
	**************************************************************************/
	PROCEDURE gm_sav_dhr_input_str_tmp(
		  p_po_number 	IN 		temp_bulk_dhr_table.c401_po_number%TYPE
		, p_input_str 	IN		CLOB
	)
	AS
	 v_strlen     	NUMBER  := NVL (LENGTH (p_input_str), 0);
	 v_string     	CLOB 	:= p_input_str;
	 v_substring  	VARCHAR2 (30000);
	 v_pnum       	temp_bulk_dhr_table.c205_part_number_id%TYPE;
	 v_ctrlno     	temp_bulk_dhr_table.c205_control_number%TYPE;
	 v_size       	temp_bulk_dhr_table.c205_custom_size%TYPE;
	 v_expdate    	VARCHAR2(20);
	 v_mfgdate    	VARCHAR2(20);
	 v_qty        	temp_bulk_dhr_table.c205_qty%TYPE;
	 v_dateformat 	t906_rules.c906_rule_value%TYPE;
	 v_dhrId		temp_bulk_dhr_table.c408_dhr_id%TYPE;
	 v_appr_sts		temp_bulk_dhr_table.c901_ins_status%TYPE;
	 v_loc_type		temp_bulk_dhr_table.c901_loc_type%TYPE;
	BEGIN
	
	v_dateformat := get_rule_value ('DATEFMT', 'DATEFORMAT');
	
	DELETE FROM TEMP_BULK_DHR_TABLE;
	
	 WHILE INSTR (v_string, '|') <> 0
	  LOOP
	    v_substring  := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
	    v_string     := SUBSTR (v_string, INSTR (v_string, '|')    + 1);
	    v_pnum       := null;
	    v_ctrlno     := null;
	    v_size       := null;
	    v_expdate    := null;
	    v_mfgdate    := null;
	    v_dhrId		 := null;
	    v_appr_sts	 := null;
	    v_loc_type	 := null;
	    v_qty		 := null;
	    v_pnum       := trim(substr (v_substring, 1, instr (v_substring, '^') - 1));
	    v_substring  := substr (v_substring, instr (v_substring, '^') + 1);
	    v_ctrlno     := trim(substr (v_substring, 1, instr (v_substring, '^') - 1));
	    v_substring  := substr (v_substring, instr (v_substring, '^') + 1);
	    v_size       := trim(substr (v_substring, 1, instr (v_substring, '^') - 1));
	    v_substring  := substr (v_substring, instr (v_substring, '^') + 1);
	    v_expdate    := trim(substr (v_substring, 1, instr (v_substring, '^') - 1));
	    v_substring  := substr (v_substring, instr (v_substring, '^') + 1);
	    v_mfgdate    := trim(substr (v_substring, 1, instr (v_substring, '^') - 1));
		v_substring  := substr (v_substring, instr (v_substring, '^') + 1);
	    v_dhrId    	 := trim(substr (v_substring, 1, instr (v_substring, '^') - 1));
		v_substring  := substr (v_substring, instr (v_substring, '^') + 1);
	    v_appr_sts   := trim(substr (v_substring, 1, instr (v_substring, '^') - 1));
		v_substring  := substr (v_substring, instr (v_substring, '^') + 1);
	    v_loc_type   := trim(substr (v_substring, 1, instr (v_substring, '^') - 1));
	    v_substring  := substr (v_substring, instr (v_substring, '^') + 1);
	    v_qty        := v_substring;
	    
	    
	   INSERT INTO  temp_bulk_dhr_table(
	                c401_po_number
	              , c205_part_number_id
	              , c205_control_number
	              , c205_custom_size
	              , c205_exp_dt
	              , c205_mfg_dt
				  , c408_dhr_id
				  , c901_ins_status
				  , c901_loc_type
	              , c205_qty)
	       VALUES ( p_po_number
	              , v_pnum
	              , v_ctrlno
	              , v_size
	              , to_date(v_expdate,v_dateformat)
	              , TO_DATE(v_mfgdate,v_dateformat)
				  , v_dhrId
				  , v_appr_sts
				  , v_loc_type
	              , v_qty);
	  END LOOP;              
	    
	END gm_sav_dhr_input_str_tmp;
	
	/***************************************************************************************************************************
	* Description : Check and create work orders necessary, so RC DHRS are not created for parts that are received in addition
	****************************************************************************************************************************/
	PROCEDURE gm_op_sav_wo_for_extra_parts(
	  p_po_number 		IN 		t401_purchase_order.c401_purchase_ord_id%TYPE
	, p_action 			IN		VARCHAR
	, p_user_id 		IN 		T4081_DHR_BULK_RECEIVE.c4081_last_updated_by%TYPE
	)
	AS
		CURSOR v_cursor 
		IS
			SELECT * FROM
				(SELECT tmp.c205_part_number_id pnum
				      , tmp.qty + nvl(qty_received.qty,0) - nvl(qty_ordered.qty,0) wo_qty
				 FROM
				 (SELECT c205_part_number_id
				       , SUM(c205_qty) qty
				    FROM temp_bulk_dhr_table tmp   
				GROUP BY c205_part_number_id) tmp
				,(SELECT C205_part_number_id, SUM(t402.c402_qty_ordered) qty
				    FROM t401_purchase_order t401
				       , t402_work_order t402
				   WHERE t401.c401_purchase_ord_id = t402.c401_purchase_ord_id
				     AND t401.c401_purchase_ord_id = p_po_number
				     AND t401.c401_void_fl is null
				     AND t402.c402_void_fl is null
				   GROUP BY c205_part_number_id) qty_ordered
				,(SELECT t402.c205_part_number_id
				       , SUM(nvl(t408.c408_qty_received,0)) qty
				    FROM t401_purchase_order t401
				       , t402_work_order t402
				       , t408_dhr t408
				   WHERE t401.c401_purchase_ord_id = t402.c401_purchase_ord_id
				     AND t402.c402_work_order_id = t408.c402_work_order_id (+)
				     AND t401.c401_purchase_ord_id = p_po_number
				     AND t401.c401_void_fl is null
				     AND t402.c402_void_fl is null
				     AND t408.c408_void_fl(+) is null
				   GROUP BY t402.c205_part_number_id) qty_received
				WHERE tmp.c205_part_number_id = qty_ordered.c205_part_number_id(+)
				  AND tmp.c205_part_number_id = qty_received.c205_part_number_id(+))
			 WHERE wo_qty > 0;
		
		v_po_type 		t401_purchase_order.c401_type%type := 3100; -- Product 
		v_critical_fl 	t402_work_order.c402_critical_fl%TYPE := 'N';
		v_dhr_id 		t402_work_order.c408_dhr_id%TYPE := null;
		v_far_fl 		t402_work_order.c402_far_fl%TYPE := 'N';
		v_lot_count 	NUMBER := 0;
		v_message 		VARCHAR2(100);
		v_vendor_id		t402_work_order.c301_vendor_id%TYPE;
		v_cost			t402_work_order.c402_cost_price%TYPE;
		v_rev_no		t402_work_order.c402_rev_num%TYPE;
		v_price 		t402_work_order.c405_pricing_id%TYPE;
		v_validate_sts 	t402_work_order.c402_validation_fl%TYPE;
		v_verified_sts 	VARCHAR2(10);
	BEGIN
	  
		BEGIN
		
			SELECT t401.c301_vendor_id 
				INTO v_vendor_id
			FROM t401_purchase_order t401 
			WHERE t401.c401_purchase_ord_id = p_po_number;
			 
			EXCEPTION WHEN NO_DATA_FOUND 
			THEN
			  GM_RAISE_APPLICATION_ERROR('-20999','303','');
		END;
		
		FOR v_index IN v_cursor
		LOOP
		
			BEGIN
			 /* Update the Qty for any work order found, 
			    other wise create a new work order
				*/
				
				/* $$$$$$$$$$$$$$$ Need to Check if there is any posting need to be done for updating the QTY $$$$$$$$$$$$$$$ */
				
				 UPDATE t402_work_order
				      SET c402_qty_ordered = c402_qty_ordered + v_index.wo_qty   
				        , c402_status_fl = 3
				        , c402_last_updated_date = SYSDATE
				        , c402_last_updated_by   = p_user_id
				    WHERE c402_work_order_id IN (SELECT max(c402_work_order_id)
				                                 FROM t402_work_order t402 
				                                WHERE t402.c401_purchase_ord_id = p_po_number  
				                                  AND t402.c301_vendor_id = v_vendor_id
				                                  AND t402.c205_part_number_id = v_index.pnum) ;
				 IF SQL%ROWCOUNT = 0 THEN
				 	BEGIN
					SELECT B.C405_COST_PRICE
						 , A.C205_REV_NUM
						 , c405_pricing_id 
						 , GET_VALIDATED_STATUS (A.C205_PART_NUMBER_ID, B.C301_VENDOR_ID)
						 , get_verified_status (a.c205_part_number_id) verifl 
					  INTO
					     v_cost
					   , v_rev_no  
					   , v_price
					   , v_validate_sts
					   , v_verified_sts
					  FROM T205_PART_NUMBER A
					     , T405_VENDOR_PRICING_DETAILS B
					 WHERE a.c205_part_number_id = b.c205_part_number_id(+)  
					   AND a.c205_active_fl = 'Y'  
					   AND a.c205_part_number_id=v_index.pnum
					   AND A.C901_STATUS_ID = 20367 -- Approved
					   AND b.c301_vendor_id = v_vendor_id
					   AND b.c405_cost_price IS NOT NULL 
					   AND b.c405_active_fl = 'Y' 
					   AND b.c405_void_fl IS NULL;
					
					 EXCEPTION WHEN NO_DATA_FOUND THEN
						 v_rev_no := '';
						 v_cost := '';
						 v_price := '';
						 v_validate_sts := '';
						 v_verified_sts := '';
					 
					 END;
					 
					GM_PLACE_WO (
					     p_po_number         
					   , v_index.pnum   
					   , v_rev_no        
					   , v_vendor_id   
					   , v_index.wo_qty        
					   , v_cost       
					   , v_critical_fl
					   , v_dhr_id   
					   , v_far_fl 
					   , v_validate_sts     
					   , v_lot_count    
					   , v_price    
					   , v_po_type      
					   , p_user_id     
					   , v_message    
					);
				END IF;
			END;						  
		END LOOP;
	
	END gm_op_sav_wo_for_extra_parts;
	
	/*************************************************************************
	* Description : Check and save the shipment master information
	**************************************************************************/
	PROCEDURE gm_op_sav_bulk_dhr_info (
	  p_dhr_bulk_id 	IN OUT 	T4081_DHR_BULK_RECEIVE.c4081_dhr_bulk_id%TYPE
	, p_po_number		IN		t401_purchase_order.c401_purchase_ord_id%TYPE
	, p_date_received	IN		t401_purchase_order.c401_purchase_ord_date%TYPE
	, p_pack_slip_id	IN		T4081_DHR_BULK_RECEIVE.c4081_packing_slip%TYPE
	, p_donor_id		IN		T4081_DHR_BULK_RECEIVE.c2540_donor_id%TYPE
	, p_user_id			IN		T4081_DHR_BULK_RECEIVE.c4081_last_updated_by%TYPE
	, p_rej_rs_fl		IN		VARCHAR2 DEFAULT NULL
	, p_stage_date		IN      T4081_DHR_BULK_RECEIVE.C4081_STAGE_DATE%TYPE
	)
	AS
	v_date_format 	t906_rules.c906_rule_value%TYPE;
	v_company_id    T1900_Company.c1900_company_id%TYPE;
	v_plant_id      t5040_plant_master.C5040_PLANT_ID%type;
	v_cmp_dt_fmt    VARCHAR2(20);
	v_rs_prefix		VARCHAR2(20) := 'RS';
	BEGIN
	   SELECT get_compid_frm_cntx(), get_plantid_frm_cntx() , get_compdtfmt_frm_cntx()
	     INTO v_company_id, v_plant_id , v_cmp_dt_fmt FROM DUAL;
	   
	   v_date_format := NVL(v_cmp_dt_fmt,get_rule_value ('DATEFMT', 'DATEFORMAT'));

	   UPDATE T4081_DHR_BULK_RECEIVE
	      SET c401_purchase_ord_id = p_po_number
		    , c4081_received_date = p_date_received
			, c4081_packing_slip = p_pack_slip_id
			, c2540_donor_id = p_donor_id
			, c4081_last_updated_by = p_user_id
			, c4081_last_updated_date = sysdate
			, C1900_COMPANY_ID = v_company_id
			, C5040_PLANT_ID  = v_plant_id
	    WHERE c4081_dhr_bulk_id = p_dhr_bulk_id;
	
	IF SQL%ROWCOUNT = 0 THEN
	-- If reject flag is 'Y', then get prefix from rules table
		IF p_rej_rs_fl = 'Y'
		THEN
			SELECT GET_RULE_VALUE('PREFIX','RSREJFL')
				INTO v_rs_prefix
			FROM DUAL;
		END IF;
		
	  	SELECT v_rs_prefix || '-' || S4081_DHR_BULK_RECEIVE.NEXTVAL 
	    INTO p_dhr_bulk_id 
		FROM DUAL;
		
		INSERT INTO T4081_DHR_BULK_RECEIVE(
		            c4081_dhr_bulk_id
				  , c401_purchase_ord_id
				  , c4081_received_date
				  , c4081_packing_slip
				  , c2540_donor_id
				  , c4081_last_updated_by
				  , c4081_last_updated_date
				  , c4081_stage_date
				  , C1900_COMPANY_ID
				  , C5040_PLANT_ID)
			 VALUES	( p_dhr_bulk_id
	              , p_po_number
				  , p_date_received
				  , p_pack_slip_id
				  , p_donor_id
				  , p_user_id
				  , CURRENT_DATE
				  , p_stage_date
				  , v_company_id
				  , v_plant_id);		
	  
	END IF;	

	END gm_op_sav_bulk_dhr_info;
	
	/*************************************************************************
	* Description : Create DHR's for the received shipment
	**************************************************************************/
	PROCEDURE gm_op_sav_bulk_dhr_detail(
	 p_dhr_bulk_id		IN  	T4081_DHR_BULK_RECEIVE.c4081_dhr_bulk_id%TYPE
	, p_po_number		IN		t401_purchase_order.c401_purchase_ord_id%TYPE
	, p_vendor_id		IN      t408_dhr.c301_vendor_id%TYPE
	, p_donor_id		IN		T4081_DHR_BULK_RECEIVE.c2540_donor_id%TYPE
	, p_packslip		IN		T4081_DHR_BULK_RECEIVE.c4081_packing_slip%TYPE
	, p_action			IN		VARCHAR2
	, p_user_id			IN		T4081_DHR_BULK_RECEIVE.c4081_last_updated_by%TYPE
	)
	AS
	
	v_date_format 	t906_rules.c906_rule_value%TYPE;
	v_dhr_id    	t408_dhr.c408_dhr_id%TYPE;
	v_message		VARCHAR2(20);
	v_input_str 	CLOB;
	v_cmp_dt_fmt    VARCHAR2(20);
    
		CURSOR v_cursor IS
            SELECT tmp.c205_part_number_id pnum 
                             , c205_exp_dt exp_date
                            , c205_mfg_dt mfg_date
                            , tmp.qty qty
                            , nvl(pend_qty,0) pending_qty
                            , c402_work_order_id wo_id
            FROM
            (SELECT c205_part_number_id 
                              , c205_exp_dt
                              , c205_mfg_dt
                              , SUM(c205_qty) qty
            FROM temp_bulk_dhr_table
            GROUP BY c205_part_number_id 
                               , c205_exp_dt
                               , c205_mfg_dt) tmp
            ,
            (SELECT qty_ordered.c402_work_order_id
                              , qty_ordered.c205_part_number_id
                              , qty_ordered.qty - NVL(qty_received.qty,0) pend_qty
            FROM
            (SELECT t402.c402_work_order_id
                              , c205_part_number_id
                              , SUM(t402.c402_qty_ordered) qty
                            FROM t401_purchase_order t401
                               , t402_work_order t402
               WHERE t401.c401_purchase_ord_id = t402.c401_purchase_ord_id
                            AND t401.c401_purchase_ord_id = p_po_number
                            AND t401.c401_void_fl is null
                            AND t402.c402_void_fl is null
               GROUP BY t402.c402_work_order_id, c205_part_number_id) qty_ordered
            ,(SELECT t402.c402_work_order_id
                               , t402.c205_part_number_id
                               , SUM(nvl(t408.c408_qty_received,0)) qty
                            FROM t401_purchase_order t401
                               , t402_work_order t402
                               , t408_dhr t408
               WHERE t401.c401_purchase_ord_id = t402.c401_purchase_ord_id
                            AND t402.c402_work_order_id = t408.c402_work_order_id (+)
                            AND t401.c401_purchase_ord_id = p_po_number
                            AND t401.c401_void_fl is null
                            AND t402.c402_void_fl is null
                            AND t408.c408_void_fl(+) is null
            GROUP BY t402.c402_work_order_id
                               , t402.c205_part_number_id) qty_received    
            WHERE qty_ordered.c402_work_order_id =qty_received.c402_work_order_id (+)
              AND qty_ordered.c205_part_number_id = qty_received.c205_part_number_id (+)) wo
            WHERE tmp.C205_part_number_id = wo.C205_part_number_id(+);

	    BEGIN
	    
		 SELECT get_compdtfmt_frm_cntx()
	       INTO v_cmp_dt_fmt FROM DUAL;
	   
	    v_date_format := NVL(v_cmp_dt_fmt,get_rule_value ('DATEFMT', 'DATEFORMAT'));    
	    
	    FOR v_index IN v_cursor
	    LOOP 
	                    
                GM_CREATE_DHR (
                                v_index.wo_id       
                   , p_vendor_id     
                   , v_index.pnum    
                   , NULL -- control number 
                   , to_number(v_index.qty)           
                   , TO_CHAR(v_index.mfg_date, v_date_format)
                   , p_packslip   
                   , TO_CHAR(SYSDATE, v_date_format) -- received date
                   , 'DHR Created for shipment ' ||  p_dhr_bulk_id -- comments
                   , NULL -- type
                   , p_user_id   
                   , v_dhr_id    
                   , v_message 
                   , p_donor_id   
                   , TO_CHAR(v_index.exp_date, v_date_format)
                ); 
               
                gm_op_sav_bulk_dhr_ctrl_dtl(v_dhr_id, v_index.pnum,v_index.mfg_date,v_index.exp_date, p_user_id);
                
                gm_op_sav_bulk_dhr_mapping(p_dhr_bulk_id, v_dhr_id, p_user_id);
	                    
	    END LOOP; 
	 
    END gm_op_sav_bulk_dhr_detail;
    
    
    /*************************************************************************
    * Description : Create control numbers for the updated DHRs
    **************************************************************************/
    PROCEDURE gm_op_sav_bulk_dhr_ctrl_dtl(
      p_dhr_id			IN 		t408_dhr.c408_dhr_id%TYPE,
      p_part_num		IN  	t408_dhr.c205_part_number_id%TYPE,
      p_mfg_date		IN		t408_dhr.c408_manf_date%TYPE,
      p_exp_date		IN		t408_dhr.c2550_expiry_date%TYPE,
      p_user_id			IN		T4081_DHR_BULK_RECEIVE.c4081_last_updated_by%TYPE
    )
    AS 
       
    CURSOR v_cursor IS
		  SELECT c205_part_number_id pnum
                , c205_control_number cnum
                , c205_custom_size c_size                           
        FROM temp_bulk_dhr_table 
        WHERE c205_part_number_id = p_part_num
        AND  c205_exp_dt  = p_exp_date 
        AND  c205_mfg_dt  =  p_mfg_date; 
                                
        BEGIN 

        FOR v_index IN v_cursor
        LOOP     
             gm_pkg_op_dhr.gm_op_sav_dhr_control_num(p_dhr_id, v_index.pnum, v_index.cnum, v_index.c_size, p_user_id);
                        
        END LOOP;
        
    END gm_op_sav_bulk_dhr_ctrl_dtl;
	
	/*************************************************************************
	* Description : Map the DHRs to the shipment information
	**************************************************************************/
	PROCEDURE gm_op_sav_bulk_dhr_mapping (
	  p_dhr_bulk_id 	IN		t4081_dhr_bulk_receive.c4081_dhr_bulk_id%TYPE
	, p_dhr_id			IN 		t408_dhr.c408_dhr_id%TYPE
	, p_user_id			IN		t4082_bulk_dhr_mapping.c4082_last_updated_by%TYPE	
	)
	AS
	BEGIN 
    
	 INSERT INTO t4082_bulk_dhr_mapping
	           ( c4082_bulk_dhr_map_id
	           , c4081_dhr_bulk_id
	           , c408_dhr_id
	           , c4082_last_updated_by
	           , c4082_last_updated_date)
	 VALUES( s4082_bulk_dhr_mapping.nextval
	      , p_dhr_bulk_id
	      , p_dhr_id
	      , p_user_id
	      , CURRENT_DATE);
	   
	END gm_op_sav_bulk_dhr_mapping;
	
 /********************************************************************
  * Description : This Procedure void old dhr control number 
  ********************************************************************/
	PROCEDURE gm_op_void_old_dhr_ctrlno(
		p_dhr_id			IN	    t408_dhr.c408_dhr_id%TYPE,
		p_user_id			IN 		t2540_donor_master.c2540_last_updated_by%TYPE 
	)
	AS 
	BEGIN	   
		
		 UPDATE t408a_dhr_control_number
	       SET c408a_void_fl = 'Y',
	       c408a_last_updated_by = p_user_id,
	       c408a_last_updated_date = SYSDATE
		 WHERE c408_dhr_id = p_dhr_id AND c408a_void_fl IS NULL;
		 
	END gm_op_void_old_dhr_ctrlno;
	
	
	/*********************************************************************************
	* Description : Procedure to update the DHR information at the time of Processing
	**********************************************************************************/
	PROCEDURE gm_op_edit_bulk_dhr_detail(
		  p_dhr_bulk_id  	IN OUT		t4081_dhr_bulk_receive.c4081_dhr_bulk_id%TYPE 
		, p_input_str 		IN 			CLOB
		, p_user_id			IN			T4081_DHR_BULK_RECEIVE.c4081_last_updated_by%TYPE 
	)
	AS
	
	v_date_format 	t906_rules.c906_rule_value%TYPE; 
	v_dhrid       t408_dhr.c408_dhr_id%TYPE;
    v_recqty      t408_dhr.c408_qty_received%TYPE;
    v_control     t408_dhr.c408_control_number%TYPE;
    v_packslip    t408_dhr.c408_packing_slip%TYPE;
    v_comments    t408_dhr.c408_comments%TYPE; 
    v_workordid   t408_dhr.c402_work_order_id%TYPE;
    v_updstatusfl t408_dhr.c408_status_fl%TYPE; 
	v_cmp_dt_fmt  VARCHAR2(20);
	CURSOR v_cursor IS
		SELECT c205_part_number_id	pnum
		      , c205_control_number	cnum
		      , c205_custom_size	c_size
		      , c205_exp_dt		expdt
		      , c205_mfg_dt		mfgdt
		      , c408_dhr_id 	dhrid
		      , c901_ins_status statusfl
		FROM temp_bulk_dhr_table ;
	
	CURSOR v_cursor_dhr IS
		SELECT c408_dhr_id 	dhrid
		FROM temp_bulk_dhr_table 
		GROUP BY c408_dhr_id;
	
	BEGIN
	SELECT get_compdtfmt_frm_cntx()     INTO v_cmp_dt_fmt FROM DUAL;
	v_date_format := NVL(v_cmp_dt_fmt,get_rule_value ('DATEFMT', 'DATEFORMAT'));
	
	-- Save the input string in temp table TEMP_BULK_DHR_TABLE
	gm_sav_dhr_input_str_tmp(NULL, p_input_str);
 	
 	FOR v_index_dhr IN v_cursor_dhr
	LOOP
	
		gm_op_void_old_dhr_ctrlno(v_index_dhr.dhrid, p_user_id);
	
	END LOOP;
 	
	FOR v_index IN v_cursor
	LOOP  
		
		SELECT c408_qty_received, c408_control_number, c408_packing_slip
		  , c408_comments, c402_work_order_id, c408_status_fl
		   INTO v_recqty, v_control, v_packslip
		  , v_comments, v_workordid, v_updstatusfl
		   FROM t408_dhr t408
		  WHERE T408.c408_dhr_id = v_index.dhrid
		    AND t408.c408_void_fl IS NULL;
		  		 
		    gm_modify_dhr_receive(v_index.dhrid, v_recqty, v_control,v_packslip,v_comments,to_char(v_index.mfgdt,v_date_format), p_user_id, v_workordid,v_updstatusfl, to_char(v_index.expdt,v_date_format));
		 
		 gm_pkg_op_dhr.gm_op_sav_dhr_control_num(v_index.dhrid, v_index.pnum, v_index.cnum, v_index.c_size, p_user_id, v_index.statusfl);
		
	END LOOP; 
	
	END gm_op_edit_bulk_dhr_detail;
/*************************************************************************
	* Description : This Procedure will be called to update the void flag for 
	existing Attribute and create new DHR Attribute.
**************************************************************************/
	PROCEDURE gm_sav_bulk_dhr_attr(
	 p_dhr_bulk_id		IN		t4081_dhr_bulk_receive.c4081_dhr_bulk_id%TYPE
	,P_attr_type        IN      VARCHAR2
	,P_attr_value        IN     CLOB
	,p_user_id   		IN		T4081_DHR_BULK_RECEIVE.c4081_last_updated_by%TYPE	
	)
	AS
	 v_strlen NUMBER   := NVL (LENGTH (P_attr_value), 0);
	 --v_attr_value 	CLOB ;
	 v_string     	CLOB 	:= P_attr_value;
	 v_substring  	VARCHAR2 (30000);
	 v_attr_value	t4081a_dhr_bulk_attribute.c4081a_attribute_value%TYPE;
	 v_chkd_fl	    VARCHAR2(2);
	 
	 --CURSOR attr_val_cur  IS
   		--SELECT * FROM v_in_list where token is not null; 
	BEGIN
		--my_context.set_my_inlist_ctx (P_attr_value);
		--gm_void_bulk_dhr_attr(p_dhr_bulk_id,P_attr_type,p_user_id); 
		--FOR v_attr_value IN attr_val_cur
	   	--LOOP
	   	WHILE INSTR (v_string, '|') <> 0
	  	LOOP
		    v_substring  := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
		    v_string     := SUBSTR (v_string, INSTR (v_string, '|')    + 1);
		    v_attr_value := null;
		    v_chkd_fl    := null;
		    v_attr_value := trim(substr (v_substring, 1, instr (v_substring, ',') - 1));
		    v_substring  := substr (v_substring, instr (v_substring, ',') + 1);
		    v_chkd_fl    := v_substring;
		    
		    IF v_chkd_fl = '0' THEN --if check box is unselect, void the record
		    	gm_void_bulk_dhr_attr(p_dhr_bulk_id,P_attr_type,v_attr_value,p_user_id);
		    ELSE --if checkbox is selected, insert the value
		   		gm_sav_bulk_dhr_attribute(p_dhr_bulk_id,P_attr_type,v_attr_value,p_user_id);
		   	END IF;
	  	END LOOP;
		
	END gm_sav_bulk_dhr_attr;
/*************************************************************************
	* Description : This procedure is used to void the DHR Attribute.
**************************************************************************/ 
	PROCEDURE gm_void_bulk_dhr_attr(
	 p_dhr_bulk_id		IN		t4081_dhr_bulk_receive.c4081_dhr_bulk_id%TYPE
	,P_attr_type        IN      VARCHAR2
	,p_attr_value		IN		t4081a_dhr_bulk_attribute.c4081a_attribute_value%TYPE
	,p_user_id   		IN		T4081_DHR_BULK_RECEIVE.c4081_last_updated_by%TYPE	
	)
	AS
	
	BEGIN
		UPDATE T4081A_DHR_BULK_ATTRIBUTE
                SET C4081A_VOID_FL = 'Y'
                	,C4081A_LAST_UPDATED_BY = p_user_id
                	,C4081A_LAST_UPDATED_DATE = SYSDATE
                WHERE C4081_DHR_BULK_ID =p_dhr_bulk_id
                AND C901_ATTRIBUTE_TYPE = P_attr_type
                AND C4081A_ATTRIBUTE_VALUE = p_attr_value
                AND C4081A_VOID_FL IS NULL;
                
    END gm_void_bulk_dhr_attr;
/*************************************************************************
	* Description :  This procedure is used to create new DHR Attribute.
**************************************************************************/ 
    PROCEDURE gm_sav_bulk_dhr_attribute(
	 p_dhr_bulk_id		IN		t4081_dhr_bulk_receive.c4081_dhr_bulk_id%TYPE
	,P_attr_type        IN      VARCHAR2
	,P_attr_value        IN     T4081A_DHR_BULK_ATTRIBUTE.C4081A_ATTRIBUTE_VALUE%TYPE
	,p_user_id   		IN		T4081_DHR_BULK_RECEIVE.c4081_last_updated_by%TYPE	
	)
	AS
	
	BEGIN
		UPDATE T4081A_DHR_BULK_ATTRIBUTE
             set C4081A_VOID_FL = ''
                ,C4081A_LAST_UPDATED_BY = p_user_id
                ,C4081A_LAST_UPDATED_DATE = SYSDATE
                WHERE C4081_DHR_BULK_ID =p_dhr_bulk_id
                AND C901_ATTRIBUTE_TYPE = P_attr_type
                AND C4081A_ATTRIBUTE_VALUE = P_attr_value
                AND C4081A_VOID_FL IS NULL;
	
         IF (SQL%ROWCOUNT = 0) THEN
				INSERT
			        INTO T4081A_DHR_BULK_ATTRIBUTE
			         (C4081A_DHR_BULK_ATTRIBUTE_ID, C4081_DHR_BULK_ID,C901_ATTRIBUTE_TYPE
			          , C4081A_ATTRIBUTE_VALUE,C4081A_LAST_UPDATED_BY, C4081A_LAST_UPDATED_DATE,c4081a_void_fl)
			         VALUES
			          (s4081A_DHR_BULK_ATTRIBUTE.nextval, p_dhr_bulk_id, P_attr_type
			            , P_attr_value, p_user_id, sysdate,NULL);
		END IF;

	
	END gm_sav_bulk_dhr_attribute;
END gm_pkg_op_bulk_dhr_txn;
/