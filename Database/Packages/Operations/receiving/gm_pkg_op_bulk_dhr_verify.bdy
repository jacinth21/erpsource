 
/*
 * @"C:\Database\Packages\Operations\receiving\gm_pkg_op_bulk_dhr_verify.bdy";
 */

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_bulk_dhr_verify
IS
--   	 	
	/******************************************************************************************
	* Description : This procedure is used to save the details at the time of DHR Batch pROCESS
	*******************************************************************************************/
	PROCEDURE gm_op_sav_bulk_dhr_verify(
	  p_dhr_bulk_id		IN OUT	T4081_DHR_BULK_RECEIVE.c4081_dhr_bulk_id%TYPE
	, p_input_str 		IN 	CLOB
	, p_action			IN	CHAR
	, p_user_id			IN	T4081_DHR_BULK_RECEIVE.c4081_last_updated_by%TYPE
	, p_spmnt_skp_fl	IN	VARCHAR2  DEFAULT NULL
	)
	AS
	 
	v_woid 		t408_dhr.c402_work_order_id%TYPE;
	v_inputstr	VARCHAR2(200);
	 
	
	CURSOR v_cursor_dhr IS
	  SELECT temp.c408_dhr_id dhrid,t205.c205_part_number_id pnum, sum (c205_qty) qty, decode (t205.c205_product_class, 4030, '1', '') sterfl 
		   FROM temp_bulk_dhr_table temp,  t205_part_number t205 
		 WHERE temp.c205_part_number_id = t205.c205_part_number_id
		GROUP BY temp.c408_dhr_id, t205.c205_part_number_id, t205.c205_product_class; 

	BEGIN

	-- Save the input string in temp table TEMP_BULK_DHR_TABLE
	gm_pkg_op_bulk_dhr_txn.gm_sav_dhr_input_str_tmp(NULL, p_input_str); 
	 
	FOR v_index IN v_cursor_dhr
	LOOP 

		BEGIN
			  SELECT c402_work_order_id
			   INTO v_woid
			   FROM t408_dhr
			  WHERE c408_dhr_id   = v_index.dhrid
			    AND c408_void_fl IS NULL; 
			EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				v_woid :=NULL;
		END; 
	 
		gm_update_dhr_pkver (
				 v_index.DHRID, --  p_dhrid          
				 v_index.PNUM,	--  p_partnum        
				 v_woid,	--  p_woid           
				 v_index.qty  ,	-- qty 
				 NULL,		--  p_labelskipfl   
				 v_index.STERFL, --  p_sterilefl      
				 p_user_id,	--  p_transby        
				 'V',		--  p_action         
				 '3',		-- p_updstatusfl   
				 NULL		--  p_udi_chk_fl    
			);
		 
	END LOOP;	

	IF p_spmnt_skp_fl = 'Y'
	THEN
		--Release the inhouse transactions, which is in rules table, for verification if the verification needs to be skipped
		UPDATE t412_inhouse_transactions
		 SET c412_status_fl = '3', 
		     c412_last_updated_date = SYSDATE,
		     c412_last_updated_by = p_user_id
	        WHERE c412_ref_id = p_dhr_bulk_id
	        AND c412_type           IN
			    (SELECT c906_rule_value
			    FROM t906_rules
			    WHERE c906_rule_id   = 'SPLIT'
			    AND c906_rule_grp_id = 'SKIP_VERIFICATION'
			    AND c906_void_fl    IS NULL
			    )
			AND c412_void_fl IS NULL;
	ELSE
		--update inhouse txn (RSFG, RSPN) status to pending verify after Verified RS.
		UPDATE t412_inhouse_transactions
		 SET c412_status_fl = '3', 
		     c412_last_updated_date = SYSDATE,
		     c412_last_updated_by = p_user_id
	        WHERE c412_ref_id = p_dhr_bulk_id
	        AND c412_status_fl <> 4 ;
	END IF;
        
    gm_pkg_op_lot_track.gm_lot_track_main(NULL,
                               NULL,
                               p_dhr_bulk_id,
                               p_user_id,
                               90801, -- DHR  
                               4301,-- PLUS
                               50982 -- DHR
                               );
	
	END gm_op_sav_bulk_dhr_verify;	
	
	
END gm_pkg_op_bulk_dhr_verify;
/