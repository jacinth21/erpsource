/* Formatted on 2011/08/23 11:49 (Formatter Plus v4.8.0) */
/*
 * @"C:\Database\Packages\Operations\gm_pkg_op_bulk_dhr_rpt.bdy";
 */

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_bulk_dhr_rpt
IS
--  
	 /***************************************************************
	 * Description : This Procedure is used to fetch part info by part num
	 * 
	 ****************************************************************/
	PROCEDURE gm_fch_part_info (	 
	  p_num				IN		t205_part_number.c205_part_number_id%TYPE
	, p_out   			OUT		TYPES.cursor_type	
	)
	   AS
	    v_company_id t1900_Company.c1900_company_id%TYPE;
	   BEGIN
	   
	    SELECT get_compid_frm_cntx()
           INTO v_company_id FROM DUAL; 
	   
	      OPEN p_out
	       FOR
	        SELECT t205.c205_part_number_id partnum, c205_part_num_desc pdesc, v205g.c205d_shelf_life shelflife
  				, v205g.c205d_label_size lsize, DECODE(v205g.c205d_contract_process_client,'null','',v205g.c205d_contract_process_client) contproclnt
  				, NVL(trim(get_cpc_account_name(V205G.C205D_CONTRACT_PROCESS_CLIENT)),'BBA') cpclient
  				, get_code_name(c205d_preservation_method) prodtype , NVL(T405.c405_cost_price,0) costprice, NVL(t205.c205_active_fl,'N') partstatus
 			  FROM t205_part_number t205, v205g_part_label_parameter v205g , t405_vendor_pricing_details t405
 			WHERE T205.c205_part_number_id = TO_CHAR(p_num)
            AND t405.c405_active_fl (+)  = 'Y'
            AND t405.c405_void_fl (+)  IS  NULL
            AND t405.c1900_company_id (+)=v_company_id
            AND t205.c205_part_number_id=t405.c205_part_number_id (+)
    		AND t205.c205_part_number_id = v205g.c205_part_number_id (+);
	           
	           
	   END gm_fch_part_info;
	   
	/*************************************************************************
	* Description : This function returns the expiry date based on shelf life
	**************************************************************************/
	FUNCTION get_pnum_expdt (
		p_mfg_date		IN	 DATE
	  , p_shelf_life	IN	 t205d_part_attribute.c205d_attribute_value%TYPE
	)
		RETURN VARCHAR2
	IS
		v_date_fmt		t906_rules.c906_rule_value%TYPE;
		v_exp_date		VARCHAR(20);
		v_mfg_date		VARCHAR(20);
		v_comp_dt_fmt   VARCHAR2(20);
	BEGIN
		SELECT get_compdtfmt_frm_cntx() INTO v_comp_dt_fmt FROM DUAL;
		
		 v_date_fmt := NVL(v_comp_dt_fmt,GET_RULE_VALUE('DATEFMT','DATEFORMAT'));
		 v_mfg_date := TO_CHAR(p_mfg_date,v_date_fmt);
		 
		BEGIN
			SELECT TO_CHAR(ADD_MONTHS(p_mfg_date,p_shelf_life),v_date_fmt)
			INTO v_exp_date
     		FROM DUAL;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				RETURN v_mfg_date;
		END;
		
		IF v_exp_date IS NULL
		THEN
			RETURN '';
		ELSE
			RETURN v_exp_date;
		END IF;		
	END get_pnum_expdt;
	
	 /***************************************************************
	 * Description : This Procedure is used to fetch bulk DHR info
	 * 
	 ****************************************************************/
	PROCEDURE gm_fch_bulk_dhr (	 
	  p_rsnum				IN		t4081_dhr_bulk_receive.c4081_dhr_bulk_id%TYPE
	, p_rsheader   			OUT		TYPES.cursor_type	
	, p_rsdtls   			OUT		TYPES.cursor_type
	, p_action              IN      VARCHAR2 DEFAULT NULL
	)
	   AS
	   v_date_fmt	t906_rules.c906_rule_value%TYPE;
	   v_chk_status VARCHAR(3);
       v_status_id VARCHAR(3);
       v_rs_cnt		NUMBER;
       v_company_id t1900_Company.c1900_company_id%type;
       v_cmp_dt_fmt VARCHAR2(20);
	   BEGIN
		  
		  SELECT get_compid_frm_cntx(), get_compdtfmt_frm_cntx()
	        INTO v_company_id, v_cmp_dt_fmt FROM DUAL; 
		  
	      v_date_fmt := NVL(v_cmp_dt_fmt,get_rule_value ('DATEFMT', 'DATEFORMAT'));   
		  v_date_fmt := NVL(v_date_fmt,'MM/DD/YYYY');
		--Removed company and plant to load the transaction details in different companies PMT-29683
		  BEGIN
			  SELECT COUNT(1) INTO v_rs_cnt
			  	FROM t4081_dhr_bulk_receive
			  WHERE c4081_dhr_bulk_id = p_rsnum
			  --  AND c1900_company_id = v_company_id
			  	AND c4081_void_fl IS NULL;
			  EXCEPTION
			  WHEN NO_DATA_FOUND THEN
			  	v_rs_cnt := '0';
		  END;
		  
		  IF v_rs_cnt = '0' THEN
		  	GM_RAISE_APPLICATION_ERROR('-20999','301','');
		  END IF;
		  
		   BEGIN
                SELECT gm_pkg_op_bulk_dhr_rpt.get_bulk_dhr_status(p_rsnum) 
                    INTO v_status_id
                    FROM DUAL; 
                    
                    	IF v_status_id = '1.5'
                    	THEN
                    		v_chk_status :='2';                    	
                    	ELSE
                         v_chk_status := v_status_id;
                        END IF;           
                                        
         			EXCEPTION WHEN NO_DATA_FOUND THEN
                        v_status_id := NULL;
          END;    
		   
	      OPEN p_rsheader
	       FOR	        
			SELECT t4081.c4081_dhr_bulk_id dhr_bulk_id
			     , t4081.c401_purchase_ord_id po_id
			     , to_char(t4081.c4081_received_date,v_date_fmt) receiveddt
			     , t4081.c4081_packing_slip packslipid
			     , get_donor_number(t4081.c2540_donor_id) donornum
			     , gm_pkg_op_bulk_dhr_rpt.get_bulk_dhr_status_name(t4081.c4081_dhr_bulk_id) status
			     , gm_pkg_op_bulk_dhr_rpt.get_bulk_dhr_status_name(t4081.c4081_dhr_bulk_id) statusname
			     , v_status_id statusId
			     , get_user_name(t4081.c4081_last_updated_by) allograftname
			     , GET_CODE_NAME(t409.c901_reason_type) rejtype
           		 , t409.c409_rej_reason rejreason
           		 , t4081.c4081_comments rejrsid
           		 , get_rule_value('SHOW_CHK','SPLIT') showchkfl
			FROM  
			t4081_dhr_bulk_receive t4081, t4082_bulk_dhr_mapping t4082, t409_ncmr t409
			WHERE t4081.c4081_dhr_bulk_id = p_rsnum
			  AND t4081.c4081_dhr_bulk_id = t4082.c4081_dhr_bulk_id
		      AND t4082.c408_dhr_id = t409.c408_dhr_id(+)
             -- AND t4081.c1900_company_id = v_company_id
		      AND t4082.c4082_void_fl IS NULL
		      AND t409.c409_void_fl(+) IS NULL
		      AND ROWNUM = 1;
    			
    	 OPEN p_rsdtls
	       FOR
	        SELECT NVL(t408.c408_control_number, t408a.c408a_conrol_number) lotcode
			     , t408.c205_part_number_id pnum
			     , t408a.c408a_custom_size custsize
			     , c205d_label_size partsize
			     , NVL(t408a.c408a_custom_size, c205d_label_size) psize
			     , to_char(t408.c408_manf_date,v_date_fmt)  manfdate
			     , to_char(t408.c2550_expiry_date,v_date_fmt) expdate 
			     , NVL(trim(get_cpc_account_name(c205d_contract_process_client)),'BBA') cpclient
			     , get_code_name(c205d_preservation_method) prodtype
			     , t408.c408_dhr_id dhrid
			     , t408a.c901_status codeid
			     , get_code_name(T408A.C901_STATUS) apprej
			     , get_donor_number(c2540_donor_id) donornum
			     , get_inhouse_txn_id(p_rsnum, t408.c408_dhr_id,t408a.c408a_conrol_number) txnid  
				 -- Added the below additional fetch to get the inventory type selected for each lot number from Donor Allocation screen
			     , t408a.c901_inventory_type_id move_to_id 
			     
			  FROM t408_dhr t408
			      , t408a_dhr_control_number t408a
			      , v205g_part_label_parameter v205g
			      , t4082_bulk_dhr_mapping t4082
			 WHERE t4082.c4081_dhr_bulk_id = p_rsnum
			   AND t408.c408_dhr_id = t408a.c408_dhr_id   
			   AND t408.c205_part_number_id = v205g.c205_part_number_id
			   AND t408.c408_dhr_id = t4082.c408_dhr_id
              -- AND t408.c1900_company_id = v_company_id
			   AND t408.c408_void_fl IS NULL
			   AND t408a.c408a_void_fl IS NULL
			   AND t4082.c4082_void_fl IS NULL
			   AND t408.c408_status_fl = NVL(v_chk_status, t408.c408_status_fl )
			   ORDER BY prodtype,lotcode,pnum;
	           
	   END gm_fch_bulk_dhr;

	/*************************************************************************
	* Description : This function returns the expiry date based on shelf life
	**************************************************************************/
	FUNCTION get_bulk_dhr_status_name (
		p_rsnum			IN	 t4081_dhr_bulk_receive.c4081_dhr_bulk_id%TYPE 
	)
		RETURN VARCHAR2
	IS
		v_status_nm		VARCHAR(100);
		v_status		VARCHAR(3);
		
	BEGIN	
      	v_status := gm_pkg_op_bulk_dhr_rpt.get_bulk_dhr_status (p_rsnum);

		SELECT DECODE(v_status,
		                    '1', 'Pending Inspection',
  		                    '1.5','Pending Processing',
				            '2', 'Pending Processing',
		     		        '3', 'Pending Verification',
				            '4', 'Verified')
		INTO v_status_nm
		FROM dual; 
		
		RETURN v_status_nm;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN '';   
	END get_bulk_dhr_status_name;
	
	/*************************************************************************
	* Description : This function returns the status of dhr id
	**************************************************************************/
	FUNCTION get_dhr_status_name (
		p_dhrid			IN	 t408_dhr.c408_dhr_id%TYPE 
	)
		RETURN VARCHAR2
	IS
		v_status_nm		VARCHAR(100);
		v_status		VARCHAR(3);
		
	BEGIN	
      	SELECT c408_status_fl INTO  v_status
      	FROM t408_dhr
      	WHERE c408_dhr_id = p_dhrid
      	AND c408_void_fl IS NULL;

		SELECT DECODE(v_status,
		                    '1', 'Pending Inspection',
				            '2', 'Pending Processing',
		     		        '3', 'Pending Verification',
				            '4', 'Verified')
		INTO v_status_nm
		FROM dual; 
		
		RETURN v_status_nm;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN '';   
	END get_dhr_status_name;
	
	/*************************************************************************
	* Description : This function returns least dhr status of shipment
	**************************************************************************/
	FUNCTION get_bulk_dhr_status (
		p_rsnum			IN	 t4081_dhr_bulk_receive.c4081_dhr_bulk_id%TYPE 
	)
		RETURN		 t408_dhr.c408_status_fl%TYPE
	IS
		v_status		VARCHAR(3);
		v_ncmr_status	t409_ncmr.c409_status_fl%TYPE;
		v_company_id    T1900_company.c1900_company_id%TYPE;
	BEGIN	
		SELECT get_compid_frm_cntx() INTO v_company_id FROM DUAL;
		
      	SELECT MIN(t408.c408_status_fl)
      	 INTO v_status
		  FROM t408_dhr t408
		      , t4082_bulk_dhr_mapping t4082
		 WHERE t4082.c4081_dhr_bulk_id = p_rsnum
		   AND t408.c408_dhr_id = t4082.c408_dhr_id
		   AND t408.c1900_company_id  = v_company_id
		   AND t408.c408_void_fl IS NULL
		   AND t4082.c4082_void_fl IS NULL;
		   
		SELECT gm_pkg_op_bulk_dhr_rpt.get_bulk_ncmr_status(p_rsnum) 
           INTO v_ncmr_status
           FROM DUAL;                  

         IF v_ncmr_status = 'Y' THEN
             v_status := '1.5';                 	
         END IF;   
				
		RETURN v_status;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN ''; 
	END get_bulk_dhr_status;
	
	/*************************************************************************
	* Description : This function returns the least status of NCMR
	**************************************************************************/	
   FUNCTION get_bulk_ncmr_status (
		p_rsnum			IN	 t4081_dhr_bulk_receive.c4081_dhr_bulk_id%TYPE 
	)
		RETURN		 t409_ncmr.c409_status_fl%TYPE
	is
		v_status_fl		t409_ncmr.c409_status_fl%TYPE;
		v_company_id    T1900_company.c1900_company_id%TYPE;
	BEGIN	
		SELECT get_compid_frm_cntx() INTO v_company_id FROM DUAL;
		
      	SELECT MIN(t409.c409_status_fl)
      	 INTO v_status_fl
		  FROM t4082_bulk_dhr_mapping t4082, t409_ncmr t409
		 WHERE t4082.c4081_dhr_bulk_id = p_rsnum
		   and t4082.c408_dhr_id = t409.c408_dhr_id
		   AND t409.c1900_company_id  = v_company_id
           AND t4082.c4082_void_fl IS NULL
		   AND t409.c409_void_fl IS NULL  ;
				
           IF v_status_fl < '3'
            THEN
                v_status_fl :='Y';
            ELSE
                v_status_fl :='N';
           END IF;       
       
		RETURN v_status_fl;
	EXCEPTION
		WHEN NO_DATA_FOUND
		then
			return 'N'; 
	END get_bulk_ncmr_status; 
	
/*************************************************************************
 * * Description : This Procedure returns the DHR Attribute Values.
**************************************************************************/
	PROCEDURE gm_fch_bulk_dhr_attr(
	 p_dhr_bulk_id		IN		t4081_dhr_bulk_receive.c4081_dhr_bulk_id%TYPE
	, p_user_id   		IN		T4081_DHR_BULK_RECEIVE.c4081_last_updated_by%TYPE	
	, p_attrdtls  		OUT		TYPES.cursor_type
	)
	AS
	v_dateformat VARCHAR2(1000);
	v_comp_dt_fmt VARCHAR2(1000); 
	BEGIN
		SELECT get_compdtfmt_frm_cntx() INTO v_comp_dt_fmt FROM DUAL;
		
		v_dateformat := NVL(v_comp_dt_fmt,get_rule_value ('DATEFMT', 'DATEFORMAT')) || '  HH12:MI:SS AM';
		open p_attrdtls
		
		FOR 
		SELECT C901_CODE_ID CODEID,C901_CODE_NM CODENM, C902_CODE_NM_ALT CODENMALT,C901_CONTROL_TYPE CONTROLTYP,
              C901_CODE_SEQ_NO CODESEQNO,T4081A.ATTRTP,T4081A.ATTRVAL,T4081A.UPDATEDBY,T4081A.UPDATEDDATE
        FROM T901_Code_Lookup T901,
            (SELECT T4081A.C901_ATTRIBUTE_TYPE ATTRTP,T4081A.C4081A_ATTRIBUTE_VALUE ATTRVAL,
             get_user_name(T4081A.C4081A_LAST_UPDATED_BY) updatedby,TO_CHAR(T4081A.C4081A_LAST_UPDATED_DATE,v_dateformat) updateddate
             FROM T4081A_DHR_BULK_ATTRIBUTE T4081A
            WHERE T4081A.C4081_DHR_BULK_ID = p_dhr_bulk_id
            AND T4081A.c4081a_void_fl     IS NULL
            ) T4081A
      WHERE C901_CODE_GRP  IN('DBATTR')
      AND T901.C901_CODE_ID = T4081A.attrval(+)
      AND C901_ACTIVE_FL    = '1'
      ORDER BY  
      C901_CODE_SEQ_NO;
	END gm_fch_bulk_dhr_attr;
	
	
	/*************************************************************************
	* Description : This function returns the donor number count
	**************************************************************************/	
   	FUNCTION get_donor_num_cnt (
		  p_donornum		IN	 	t2540_donor_master.c2540_donor_number%TYPE
		, p_vendor_id		IN		t2540_donor_master.c301_vendor_id%TYPE
	)
		RETURN	VARCHAR2
	IS
		v_count		INTEGER;
		
	BEGIN	

		BEGIN
			SELECT COUNT(1) INTO v_count
				FROM t2540_donor_master 
			WHERE c2540_donor_number = p_donornum
				AND NVL( c301_vendor_id,'-9999') =NVL(p_vendor_id,'-9999')
				AND c2540_void_fl IS NULL;
		
		EXCEPTION WHEN NO_DATA_FOUND
		THEN
			v_count := 0;
		END;
           
		RETURN v_count;

	END get_donor_num_cnt; 
	
    /*******************************************************
	 Description: this function returns DHR id 
	*******************************************************/
	FUNCTION get_inhouse_txn_id (
	    p_dhr_bulk_id	IN    t4081_dhr_bulk_receive.c4081_dhr_bulk_id%TYPE
	   ,p_dhrid         IN	  t4082_bulk_dhr_mapping.c408_dhr_id%TYPE
	   ,p_ctrlno        IN    t413_inhouse_trans_items.c413_control_number%TYPE
	)
		RETURN VARCHAR2
	IS
		v_txnid   t413_inhouse_trans_items.c412_inhouse_trans_id%TYPE;
		v_company_id  T1900_company.c1900_company_id%TYPE;
	BEGIN
		SELECT get_compid_frm_cntx() INTO v_company_id FROM DUAL;
	     SELECT  t412.c412_inhouse_trans_id
	         INTO v_txnid
	       from   T4082_BULK_DHR_MAPPING T4082, t412_inhouse_transactions t412, t413_inhouse_trans_items t413
	      WHERE  t4082.c4081_dhr_bulk_id = p_dhr_bulk_id
	      and t4082.c408_dhr_id = p_dhrid
	        AND t4082.c4081_dhr_bulk_id  = t412.C412_REF_ID 
	        AND t412.C412_INHOUSE_TRANS_ID= t413.c412_inhouse_trans_id
	        and t413.c413_control_number = p_ctrlno
	        AND t412.c1900_company_id =  v_company_id
	        AND t4082.c4082_void_fl  IS NULL
	        AND t412.c412_void_fl IS NULL
	        ANd t413.c413_void_fl IS NULL;
	    
		RETURN v_txnid;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN '';
	END get_inhouse_txn_id;	

	/*************************************************************************
	 * * Description : This Procedure is to fetch the dhr bulk attribute values.
	**************************************************************************/
	FUNCTION get_bulk_dhr_attr_checked(
	 	  p_dhr_bulk_id		IN		v4081a_dhr_bulk_attribute.c4081_dhr_bulk_id%TYPE
	)
		RETURN VARCHAR2
	IS
		v_form_42				v4081a_dhr_bulk_attribute.form_42%TYPE;
		v_label_printed			v4081a_dhr_bulk_attribute.labels_printed%TYPE;
		v_qc_verified			v4081a_dhr_bulk_attribute.qc_verified%TYPE;
		v_label_pre_stage		v4081a_dhr_bulk_attribute.label_pre_stage%TYPE;
		v_label_stage			v4081a_dhr_bulk_attribute.label_stage%TYPE;
		v_all_checked			VARCHAR2(20) := 'false';
		
	BEGIN
		
		BEGIN
			SELECT FORM_42, LABELS_PRINTED, QC_VERIFIED,LABEL_PRE_STAGE,LABEL_STAGE
				INTO v_form_42, v_label_printed, v_qc_verified, v_label_pre_stage, v_label_stage
			FROM V4081A_DHR_BULK_ATTRIBUTE
			WHERE C4081_DHR_BULK_ID = p_dhr_bulk_id;
			EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				v_all_checked := 'false';
		END;
		
		-- to check all the check box are checked or not
		IF v_form_42 = 'Y' AND v_label_printed = 'Y' AND v_qc_verified = 'Y' AND v_label_pre_stage = 'Y' AND v_label_stage = 'Y'
		THEN
			v_all_checked := 'true';
		END IF;
		
		RETURN v_all_checked;
	END get_bulk_dhr_attr_checked;
	
	/*************************************************************************
	 * * Description : This Procedure is to get the cpc name.
	**************************************************************************/
	FUNCTION get_cpc_account_name(
	 	  p_acc_id		IN		T704B_ACCOUNT_CPC_MAPPING.c704b_account_cpc_map_id%TYPE
	)
		RETURN VARCHAR2
	IS
		v_acc_nm  T704B_ACCOUNT_CPC_MAPPING.c704b_cpc_name%TYPE;
		
	BEGIN
			
			BEGIN
		       SELECT  c704b_cpc_name NM
			   		INTO v_acc_nm
			   FROM  T704B_ACCOUNT_CPC_MAPPING
		       WHERE c704b_account_cpc_map_id = p_acc_id
		       AND c704b_void_fl IS NULL;
		   	EXCEPTION
		      WHEN NO_DATA_FOUND
		   	THEN
		      RETURN NULL;
		    END;
	     
	  	RETURN v_acc_nm;
	     
	END get_cpc_account_name;
	
	/*************************************************************************
	* Description : This function returns the donor number count
	**************************************************************************/	
   	FUNCTION get_lot_num_cnt (
		  p_lot_number		IN	 	t408a_dhr_control_number.c408a_conrol_number%TYPE
	)
		RETURN	VARCHAR2
	IS      
	v_count		VARCHAR2(4000);
	v_rs_ids    VARCHAR2(4000);
	BEGIN	

		BEGIN
			SELECT count(1) INTO v_count
			 FROM t408a_dhr_control_number 
            WHERE c408a_conrol_number = p_lot_number 
              AND c408a_void_fl IS  NULL;
		
		EXCEPTION WHEN NO_DATA_FOUND
		THEN
			v_count := 0;
		END;
		
		IF v_count > 0 THEN
		
			SELECT (RTRIM(XMLAGG(XMLELEMENT(E,c4081_dhr_bulk_id || ',')).EXTRACT('//text()').GETCLOBVAL(),',') )
		      INTO v_rs_ids 
	          FROM ( SELECT c4081_dhr_bulk_id 
	                   FROM t4082_bulk_dhr_mapping 
	                  WHERE c408_dhr_id IN (
						          			SELECT c408_dhr_id
								  			  FROM t408a_dhr_control_number 
            							     WHERE c408a_conrol_number = p_lot_number
              								   AND c408a_void_fl IS  NULL));
           v_count := v_count || '@' || v_rs_ids;
		
		END IF;
           
		RETURN v_count;

	END get_lot_num_cnt; 
	
	/****************************************************************************
    * Description : Procedure to filter the Duplicate Lots among the scanned lots
    * Author      : hreddi
    *****************************************************************************/
	PROCEDURE gm_fch_duplicate_lots (
        p_lotinputstr IN CLOB,
        p_rs_id       IN t4082_bulk_dhr_mapping.c4081_dhr_bulk_id%TYPE,
        p_message 	  OUT CLOB)
	AS
    	v_invalid_lots CLOB;
	BEGIN
    	my_context.set_my_cloblist (p_lotinputstr);
    	
    	IF p_rs_id IS NULL THEN 
    	
    	SELECT RTRIM (XMLAGG (XMLELEMENT (e, token || ',')) .EXTRACT ('//text()'), ',')
          INTO  v_invalid_lots
          FROM v_clob_list
          WHERE TO_CHAR (token)  IN (
             				 SELECT c408a_conrol_number 
             				  FROM t408a_dhr_control_number t408a, t4082_bulk_dhr_mapping t4082 
             				 WHERE t408a.c408_dhr_id = t4082.c408_dhr_id(+)
                             AND t408a.c408a_conrol_number  IN token
             				   AND t408a.c408a_void_fl IS NULL
                               AND t4082.c4082_void_fl(+) IS NULL
                               AND t4082.c4081_dhr_bulk_id = NVL(p_rs_id,t4082.c4081_dhr_bulk_id)   
        	);
       ELSE
       		SELECT RTRIM (XMLAGG (XMLELEMENT (e, token || ',')) .EXTRACT ('//text()'), ',')
	          INTO  v_invalid_lots
	          FROM v_clob_list
	          WHERE TO_CHAR (token)  IN (
	             				 SELECT c408a_conrol_number 
	             				  FROM t408a_dhr_control_number t408a, t4082_bulk_dhr_mapping t4082 
	             				 WHERE t408a.c408_dhr_id = t4082.c408_dhr_id(+)
	                             AND t408a.c408a_conrol_number  IN token
	             				   AND t408a.c408a_void_fl IS NULL
	                               AND t4082.c4082_void_fl(+) IS NULL
	                               AND t4082.c4081_dhr_bulk_id <> NVL(p_rs_id,t4082.c4081_dhr_bulk_id)   
	        	);
       	
       END IF;
     
    	IF v_invalid_lots IS NOT NULL THEN
       		p_message       := v_invalid_lots;
        	RETURN;   
    	END IF;
	END gm_fch_duplicate_lots;
	
	/****************************************************************************
    * Description : Function to get the QC verification flag
    * Author      : Arajan
    *****************************************************************************/
	FUNCTION get_qc_verification_fl(
		p_rs_id		IN		t4082_bulk_dhr_mapping.c4081_dhr_bulk_id%TYPE
	)
	RETURN VARCHAR2
	IS
		v_avail_fl	VARCHAR2(2);
	BEGIN
		SELECT DECODE(COUNT(1),0,'N','Y')
			INTO v_avail_fl
		FROM T4081A_DHR_BULK_ATTRIBUTE
		WHERE C4081_DHR_BULK_ID    = p_rs_id
		AND C4081A_ATTRIBUTE_VALUE = 104520 -- QC verification of Entries
		AND C901_ATTRIBUTE_TYPE    = 104560 -- BBA Parameters Attribute Type
		AND C4081A_VOID_FL        IS NULL;
		
		RETURN v_avail_fl;
	END get_qc_verification_fl;
	
END gm_pkg_op_bulk_dhr_rpt;
/