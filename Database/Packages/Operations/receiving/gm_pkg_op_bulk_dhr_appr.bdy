/* Formatted on 2011/08/23 11:49 (Formatter Plus v4.8.0) */
/*
 * @"C:\Database\Packages\Operations\receiving\gm_pkg_op_bulk_dhr_appr.bdy";
 */

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_bulk_dhr_appr
IS
--   	 	
	/*************************************************************************
	* Description : This procedure is used to save the details at the time of DHR Batch Inspection
	**************************************************************************/
	PROCEDURE gm_op_sav_bulk_dhr_appr(
	  p_dhr_bulk_id		IN OUT	T4081_DHR_BULK_RECEIVE.c4081_dhr_bulk_id%TYPE
	, p_input_str 		IN 		CLOB
	, p_rejreason_type  IN 		t409_ncmr.c901_reason_type%TYPE
    , p_rejreason       IN 		t409_ncmr.c409_rej_reason%TYPE
	, p_user_id			IN		T4081_DHR_BULK_RECEIVE.c4081_last_updated_by%TYPE
	)
	AS
	
	v_date_format 	t906_rules.c906_rule_value%TYPE;
	v_ncmr_id	    t409_ncmr.c409_ncmr_id%TYPE;
	v_rejqty		NUMBER;
	v_message		VARCHAR2(200);
	
	CURSOR v_cursor IS
		 
		 SELECT TEMP.C408_DHR_ID DHRID, DECODE (T205.C205_PRODUCT_CLASS, 4030, '1', '') STERFL, SUM (TEMP.C205_QTY) INPQTY 
		   FROM TEMP_BULK_DHR_TABLE TEMP, T205_PART_NUMBER T205
		  WHERE TEMP.C205_PART_NUMBER_ID = T205.C205_PART_NUMBER_ID
		GROUP BY TEMP.C408_DHR_ID, T205.C205_PRODUCT_CLASS ;  
		 
	BEGIN 
	-- Save the input string in temp table TEMP_BULK_DHR_TABLE
	--input string format PartNumber^ControlNumber^Size^ExpiryDate^ManufacturedDate^DHRID^ApprovalStatus^LocationType^Qty|
	gm_pkg_op_bulk_dhr_txn.gm_sav_dhr_input_str_tmp(NULL, p_input_str);
	
	--save bulk dhr detail info before approve/reject
	gm_pkg_op_bulk_dhr_txn.gm_op_edit_bulk_dhr_detail(p_dhr_bulk_id,p_input_str,p_user_id); 
	
	FOR v_index IN v_cursor
	LOOP
	
		BEGIN
			 SELECT SUM (C205_QTY) REJQTY 
			 	INTO v_rejqty
			   FROM TEMP_BULK_DHR_TABLE
			  WHERE C408_DHR_ID     = v_index.DHRID
			    AND C901_INS_STATUS = 1401				--rejected
			 GROUP BY C408_DHR_ID; 
			EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				v_rejqty :=0;
		END;
			
		gm_update_dhr_inspect (
			        v_index.DHRID   ,
			        v_index.INPQTY  ,
			        NVL(v_rejqty,0) ,
			        p_user_id  ,
			        p_rejreason_type  ,
			        p_rejreason  ,
			        '2'   ,			
			        v_index.STERFL  ,
			        p_user_id     ,
			        '2'	  ,	--status
			        NULL  , --udi check fl
			        v_ncmr_id ,
			        v_message  );
			     
	END LOOP;
	
	END gm_op_sav_bulk_dhr_appr;
	
	/**************************************************************************************
	* Description : This function returns the count of bulk shipment id corresponding to the DHR id
	****************************************************************************************/	
   	FUNCTION get_bulk_shipment_id (
		  p_dhrId		IN	 	t4082_bulk_dhr_mapping.c408_dhr_id%TYPE
	)
		RETURN	VARCHAR2
	IS
		v_count		NUMBER;
		v_company_id t1900_company.c1900_company_id%TYPE;
		
	BEGIN	
      	SELECT get_compid_frm_cntx()    INTO v_company_id    FROM DUAL;
		BEGIN
			SELECT count(1) INTO v_count
				FROM t4082_bulk_dhr_mapping t4082 , t4081_dhr_bulk_receive t4081
			WHERE t4082.c4081_dhr_bulk_id = t4081.c4081_dhr_bulk_id
			    AND t4082.c408_dhr_id = p_dhrId
			    AND t4081.c1900_company_id = v_company_id
				AND t4082.c4082_void_fl IS NULL
				AND t4081.c4081_void_fl IS NULL;
		
		EXCEPTION WHEN NO_DATA_FOUND
		THEN
			v_count := 0;
		END;
           
		RETURN v_count;

	END get_bulk_shipment_id; 
	
	/***************************************************************
	 * Description : This Procedure is used to fetch control number information
	 ****************************************************************/
	PROCEDURE gm_fch_lotcode_status (	 
	  p_dhrId			IN		t408a_dhr_control_number.c408_dhr_id%TYPE
	, p_out   			OUT		TYPES.cursor_type	
	)
	AS
		v_bulk_shpmnt_id_cnt		NUMBER;
	BEGIN
		   
		   -- To get the shipment id, if the dhr is associated with a bulk shipment
			v_bulk_shpmnt_id_cnt := gm_pkg_op_bulk_dhr_appr.get_bulk_shipment_id(p_dhrId);
		   
		   IF v_bulk_shpmnt_id_cnt != '0'
		   THEN
				OPEN p_out
		       	FOR
			        SELECT c408a_conrol_number ctrlno, c901_status status
			        	, GET_CODE_NAME(c901_status) statusnm	
						FROM t408a_dhr_control_number 
					WHERE c408_dhr_id = p_dhrId
						AND c901_status NOT IN (1400)	--1400 approved
						AND c408a_void_fl IS NULL;
			ELSE
				OPEN p_out
		       	FOR
			        SELECT c408a_conrol_number ctrlno, NVL(c901_status,1400) status
			        	, DECODE(C901_STATUS, NULL, 'Approved', GET_CODE_NAME(c901_status)) statusnm	
						FROM t408a_dhr_control_number 
					WHERE c408_dhr_id = p_dhrId
						AND c408a_void_fl IS NULL;
			END IF;
	           
	   END gm_fch_lotcode_status;
	
	/*************************************************************************
	* Description : This procedure is used to save the status of lot code
	**************************************************************************/
	PROCEDURE gm_sav_lotcode_status(
	  p_input_str 		IN 		CLOB
	, p_dhr_id			IN 		t408a_dhr_control_number.c408_dhr_id%TYPE
	, p_user_id			IN		t408a_dhr_control_number.c408a_last_updated_by%TYPE
	)
	AS
	
	 v_strlen     	NUMBER  := NVL (LENGTH (p_input_str), 0);
	 v_string     	CLOB 	:= p_input_str;
	 v_substring  	VARCHAR2 (30000);
	 v_ctrlno     	t408a_dhr_control_number.c408a_conrol_number%TYPE;
	 v_status      	t408a_dhr_control_number.c901_status%TYPE; 
	
	BEGIN 
	
		WHILE INSTR (v_string, '|') <> 0
	  	LOOP
		    v_substring  := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
		    v_string     := SUBSTR (v_string, INSTR (v_string, '|')    + 1);
		    v_ctrlno     := null;
		    v_status     := null;
		    v_ctrlno     := trim(substr (v_substring, 1, instr (v_substring, '^') - 1));
		    v_substring  := substr (v_substring, instr (v_substring, '^') + 1);
		    v_status     := v_substring; 
		    
		    UPDATE t408a_dhr_control_number
		    SET c901_status = v_status
		    	, c408a_last_updated_by = p_user_id
		    	, c408a_last_updated_date = SYSDATE
		    WHERE c408_dhr_id = p_dhr_id
		    AND c408a_conrol_number = v_ctrlno
		    AND c408a_void_fl IS NULL;
		    
		END LOOP; 
		
	END gm_sav_lotcode_status;
	
	/**************************************************************************************
	* Description : This function returns the control number if it is duplicated
	****************************************************************************************/	
   	FUNCTION get_duplicate_control_num (
		p_scan_code			IN	 	CLOB,
   		p_dhr_id			IN	 	t4082_bulk_dhr_mapping.c408_dhr_id%TYPE
	)
		RETURN	CLOB
	IS
		v_ctrl_num		CLOB;
		v_company_id    t1900_company.c1900_company_id%TYPE;
		
	BEGIN
		SELECT get_compid_frm_cntx()    INTO v_company_id    FROM DUAL;
      	my_context.set_my_inlist_ctx (p_scan_code);
		
		BEGIN
			SELECT RTRIM (XMLAGG (XMLELEMENT (E, c408a_conrol_number || ',')) .EXTRACT ('//text()'), ',')
				INTO v_ctrl_num
			FROM
			(
				SELECT DISTINCT t408a.c408a_conrol_number
				FROM t408a_dhr_control_number t408a , t408_dhr t408
				WHERE t408a.c408_dhr_id = t408.c408_dhr_id
                AND t408a.c408_dhr_id != p_dhr_id
				AND t408a.c408a_conrol_number IN (SELECT token FROM v_in_list)
				AND t408a.c408a_void_fl IS NULL
                AND t408.c408_void_fl IS NULL
                AND t408.c1900_company_id = v_company_id
				ORDER BY t408a.c408a_conrol_number
			);
		
		EXCEPTION WHEN NO_DATA_FOUND
		THEN
			v_ctrl_num := NULL;
		END;
           --raise_application_error('-20999','v_count '||v_count);
			RETURN v_ctrl_num;

	END get_duplicate_control_num;
	
END gm_pkg_op_bulk_dhr_appr;
/