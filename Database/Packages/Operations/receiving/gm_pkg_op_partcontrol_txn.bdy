CREATE OR REPLACE PACKAGE BODY gm_pkg_op_partcontrol_txn
IS

/******************************************************************************************
	* Description : This procedure is used to fetch save details of the Control Number
	* Author      : Karthik
	*******************************************************************************************/

   PROCEDURE gm_save_part_control_info (
    p_pcn_id      	IN OUT  T2550_PART_CONTROL_NUMBER.c2550_part_control_number_id%TYPE,
    p_partnum  		IN 		T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE ,
    p_lot      		IN 		T2550_PART_CONTROL_NUMBER.C2550_CONTROL_NUMBER%TYPE ,
    p_cust_size		IN		T2550_PART_CONTROL_NUMBER.C2550_CUSTOM_SIZE%TYPE ,
    p_vendor_id		IN		T2550_PART_CONTROL_NUMBER.C301_VENDOR_ID%TYPE ,
    p_mfg_date		IN		VARCHAR ,
    p_donor_num		IN		t2540_donor_master.c2540_donor_number%TYPE ,
    p_expdt    		IN 		VARCHAR ,
    p_userid   		IN 		T2550_PART_CONTROL_NUMBER.C2550_LAST_UPDATED_BY%TYPE ,
    p_pack_slip		IN 		T2550_PART_CONTROL_NUMBER.C2550_PACKING_SLIP%TYPE ,
    p_rev_level   	IN    	T2550_PART_CONTROL_NUMBER.c2550_rev_level%TYPE,
    p_udi           IN      T2550_PART_CONTROL_NUMBER.c2550_udi_hrf%TYPE DEFAULT NULL
  )
AS
	v_count NUMBER;
	v_donor_id T2550_PART_CONTROL_NUMBER.C2540_DONOR_ID%TYPE;
	v_ctrl_asd_id NUMBER;
	v_di_no       T2060_DI_PART_MAPPING.C2060_DI_NUMBER%TYPE;
	v_agency      T2060_DI_PART_MAPPING.C1600_ISSUING_AGENCY_ID%TYPE;
	v_mrf        T2550_PART_CONTROL_NUMBER.C2550_UDI_MRF%TYPE;
    v_dateformat  VARCHAR2(100);
    v_company_id t1900_Company.c1900_company_id%type;
    v_plant_id      T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
    v_out_log	t902_log.c902_comments%TYPE;
BEGIN
	BEGIN
		
			SELECT get_compdtfmt_frm_cntx() ,get_compid_frm_cntx(), get_plantid_frm_cntx()
			  INTO v_dateformat ,v_company_id, v_plant_id
			  FROM dual;
		
    SELECT COUNT(1) INTO v_count
      FROM T2550_PART_CONTROL_NUMBER
     WHERE C2550_CONTROL_NUMBER = UPPER(p_lot)
       AND C205_PART_NUMBER_ID IN (p_partnum)
       AND C1900_COMPANY_ID = v_company_id
       AND C5040_PLANT_ID = v_plant_id;
     EXCEPTION WHEN NO_DATA_FOUND THEN
     v_count := 0;
   END;
  IF v_count > 0 AND p_pcn_id IS NULL THEN
   GM_RAISE_APPLICATION_ERROR('-20999','304',Upper(p_lot)||'#$#'||p_partnum);   
  END IF;
  
  IF p_donor_num IS NOT NULL THEN
  	SELECT gm_pkg_op_partcontrol_txn.get_donor_id(p_donor_num,p_vendor_id) INTO v_donor_id FROM DUAL;
  ELSE
  	v_donor_id := NULL;
  END IF;    
  
   -- Get the di number for the part        
      BEGIN
        SELECT c2060_di_number,c1600_issuing_agency_id 
        INTO  v_di_no,v_agency
        FROM t2060_di_part_mapping 
        WHERE c205_part_number_id = p_partnum ;
      EXCEPTION WHEN no_data_found THEN
        v_di_no := NULL;
        v_agency := NULL;    
      END;
      
      -- If di number available, construct HRF and MRF values 
      IF (v_di_no IS NOT NULL) AND (v_agency = 101) THEN -- GS1
        v_mrf :=  REPLACE(REPLACE(p_udi, '(',NULL),')',NULL);
        v_mrf := '<FNC1>'|| v_mrf;
      ELSE -- Vendor Design assign HRF (entered UDI number) value as MRF 
        v_mrf := p_udi;
      END IF;
            
      
  IF p_pcn_id IS NOT NULL THEN
      		
          
      
   UPDATE T2550_PART_CONTROL_NUMBER
      SET C205_PART_NUMBER_ID   			= p_partnum,
          C2550_CONTROL_NUMBER  			= UPPER(p_lot),
          C2550_EXPIRY_DATE     			= to_date(p_expdt, v_dateformat),
          C2550_CUSTOM_SIZE					= p_cust_size,
          C301_VENDOR_ID		            = p_vendor_id,
          C2550_MFG_DATE					= to_date(p_mfg_date, v_dateformat),
          C2540_DONOR_ID					= v_donor_id,
          C2550_LAST_UPDATED_BY     		= p_userid,
          C2550_LAST_UPDATED_DATE   		= SYSDATE,
          c2550_rev_level       			= p_rev_level,
          C2550_UDI_HRF						= p_udi,
          C2550_UDI_MRF						= v_mrf ,
          c1900_company_id                  = v_company_id,
          C5040_PLANT_ID                    = v_plant_id
    WHERE c2550_part_control_number_id     	= p_pcn_id;
	--In lot number override screen, if the expiry date is past date, we need to allow and update the below log in log table for PMT-48498  
    IF p_expdt IS NOT NULL THEN 
	  gm_update_log(UPPER(p_lot),'Expiry date updated to ' || to_date(p_expdt, v_dateformat)|| ' for the LOT '  || UPPER(p_lot)|| ' by ' || GET_USER_NAME(p_userid) ,'109020',p_userid,v_out_log);
	END IF;
	
     ELSE
    	  SELECT S2550_part_control_number.NEXTVAL INTO p_pcn_id FROM dual;
     
        
         INSERT INTO t2550_part_control_number
                  (C2550_PART_CONTROL_NUMBER_ID ,C205_PART_NUMBER_ID, C2550_CONTROL_NUMBER, C2550_REV_LEVEL,C2550_UDI_HRF,C2550_UDI_MRF,C2540_DONOR_ID,C2550_EXPIRY_DATE,C2550_MFG_DATE,
                  C2550_CUSTOM_SIZE ,c2550_last_updated_by, c2550_last_updated_date,C1900_COMPANY_ID,C5040_PLANT_ID
                  )
              VALUES (p_pcn_id ,p_partnum ,UPPER(p_lot) ,p_rev_level ,p_udi , v_mrf,v_donor_id , to_date(p_expdt, v_dateformat) , to_date(p_mfg_date, v_dateformat),
                   p_cust_size ,p_userid ,SYSDATE,v_company_id,v_plant_id
                  );
    END IF; 
    
END gm_save_part_control_info;

/******************************************************************************************
	* Description : This function is used to fetch get donor details
	*******************************************************************************************/

FUNCTION get_donor_id(
   p_donor_number   t2540_donor_master.c2540_donor_number%TYPE,
   p_vendor_id   	t2540_donor_master.c301_vendor_id%TYPE
)
   RETURN VARCHAR2
IS
   v_donor_id   t2540_donor_master.c2540_donor_id%TYPE;
   v_count	    NUMBER;
   v_company_id t1900_Company.c1900_company_id%type;
BEGIN
	
	SELECT get_compid_frm_cntx()
			      INTO v_company_id
			      FROM dual;
	
   	  		SELECT  count(1) 
			INTO v_count
			FROM t2540_donor_master 
			WHERE NVL(c301_vendor_id,'-9999') = NVL(p_vendor_id,'-9999')
			AND c2540_donor_number = p_donor_number
		    AND c1900_company_id = v_company_id
		    AND c2540_void_fl IS NULL;
			--	
			BEGIN 			
			      
				SELECT   c2540_donor_id				
					INTO  v_donor_id
				FROM t2540_donor_master 
				WHERE NVL(c301_vendor_id,'-9999') = NVL(p_vendor_id,'-9999')
				AND c2540_donor_number = p_donor_number
				AND c1900_company_id = v_company_id
				AND c2540_void_fl IS NULL;
				EXCEPTION
			        WHEN NO_DATA_FOUND THEN
			            v_donor_id := '';
			END;
		--	
			IF v_count > 0 THEN
	    		v_donor_id := v_donor_id;	    		
	    	ELSE
		    	v_donor_id := s2540_donor.NEXTVAL;
			END IF;
	 	RETURN v_donor_id;	 	
END get_donor_id;

/******************************************************************************************************
	* Description : This procedure is used to validate and save part and control details to t2550 table
	* Author	  : Agilan Singaravel
	***************************************************************************************************/
PROCEDURE gm_sav_bulk_lot_override_by_txn(
		p_txn_ids				IN		CLOB,
		p_company_id			IN		t1900_company.c1900_company_id%type,
		p_user_id				IN		t101_user.c101_user_id%type,
		p_out_error_txn_ids		OUT		CLOB
)
AS
	v_out_txn_id		CLOB;
BEGIN
		
	my_context.set_my_cloblist (p_txn_ids || ',');
	
	DELETE FROM my_temp_key_value;
	--Used to validate the transaction ids
	gm_validate_lot_override_txn(p_company_id,v_out_txn_id);
	
	--If transaction id is null then save or update to t2550 table OR return error transaction id(s).
	IF v_out_txn_id IS NULL THEN
		gm_sav_lot_override_by_txn(p_user_id);
	END IF;
	
	p_out_error_txn_ids := v_out_txn_id;
	
END gm_sav_bulk_lot_override_by_txn;

	/******************************************************************************************************
	* Description : This procedure is used to validate and save part and control details to t2550 table
	* Author	  : Agilan Singaravel
	***************************************************************************************************/
	PROCEDURE gm_validate_lot_override_txn(
		p_company_id			IN		t1900_company.c1900_company_id%type,
		p_out_error_txn_ids		OUT		CLOB		
	)
	AS
	v_out_txn_id		CLOB;
	BEGIN
		--Insert MY_TEMP_KEY_VALUE table values based on Consignment, RA and Inhouse transactions
			INSERT INTO MY_TEMP_KEY_VALUE
    				(MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY
    		)
 			SELECT c504_consignment_id, 'CN'
   		  	  FROM T504_CONSIGNMENT
  	     	 WHERE c504_consignment_id IN (
         			SELECT TOKEN FROM v_clob_list WHERE TOKEN IS NOT NULL)
    	   	   AND c1900_company_id = p_company_id
   		   	   AND c504_void_fl    IS NULL    		 
   		     UNION
   		    SELECT c506_rma_id, 'RA'
   		      FROM T506_RETURNS 
   		     WHERE c506_rma_id IN (
   			  	   	SELECT TOKEN FROM v_clob_list WHERE TOKEN IS NOT NULL)
   		   	   AND c1900_company_id = p_company_id
   		   	   AND c506_type <> '3300'   --Sales Items
   		   	   AND c506_void_fl IS NULL   		   
   		 	 UNION
   			SELECT c412_inhouse_trans_id, 'IN_HOUSE'
   		  	  FROM T412_INHOUSE_TRANSACTIONS 
   		 	 WHERE c412_inhouse_trans_id IN (
   		  			SELECT TOKEN FROM v_clob_list WHERE TOKEN IS NOT NULL)
   		   	   AND c1900_company_id = p_company_id
   		   	   AND c412_void_fl IS NULL;
   		  
   			--Below select is used return error details transaction id(s).
   			BEGIN
		 		SELECT rtrim(xmlagg(xmlelement(e,v_txn_id,',').extract('//text()') 
		 			ORDER BY v_txn_id).GetClobVal(),',') INTO v_out_txn_id
		 		 FROM (
		 	   		SELECT TOKEN v_txn_id FROM v_clob_list WHERE TOKEN IS NOT NULL 
		 	   		minus 
		 	   		SELECT MY_TEMP_TXN_ID FROM MY_TEMP_KEY_VALUE);
		 	   
	    	EXCEPTION WHEN NO_DATA_FOUND THEN
	    		v_out_txn_id := '';
	    	END;

   		p_out_error_txn_ids := v_out_txn_id;
   		
END gm_validate_lot_override_txn;

	/*******************************************************************
	* Description : used save part and control details to t2550 table
	* Author	  : Agilan Singaravel
	****************************************************************/
	PROCEDURE gm_sav_lot_override_by_txn(
		p_user_id		IN		t101_user.c101_user_id%type
	)
	AS
	v_id  varchar2(100);
	CURSOR all_trans_data
	IS

	SELECT pnum, cnum
   	  FROM
    	(
        	-- CN
         	SELECT t505.c205_part_number_id pnum, TRIM (upper (t505.c505_control_number)) cnum
           	  FROM t504_consignment t504, t505_item_consignment t505
          	 WHERE t504.c504_consignment_id  = t505.c504_consignment_id
               AND t504.c504_consignment_id IN
            	 (
                 	SELECT MY_TEMP_TXN_ID FROM my_temp_key_value WHERE MY_TEMP_TXN_KEY = 'CN'
            	 )
               AND t505.c505_control_number     IS NOT NULL
               AND not regexp_like (t505.c505_control_number,'^(NOC#|TBE).*')                        
               AND t504.c504_void_fl IS NULL
               AND t505.c505_void_fl IS NULL
            UNION
        	-- Inhouse
           SELECT t413.C205_PART_NUMBER_ID PNUM, TRIM (upper (t413.C413_CONTROL_NUMBER)) cnum
             FROM T412_INHOUSE_TRANSACTIONS t412, T413_INHOUSE_TRANS_ITEMS t413
            WHERE t412.C412_INHOUSE_TRANS_ID  = t413.C412_INHOUSE_TRANS_ID
              AND t412.C412_INHOUSE_TRANS_ID IN
            	(
                 SELECT MY_TEMP_TXN_ID
                   FROM my_temp_key_value
                  WHERE MY_TEMP_TXN_KEY = 'IN_HOUSE'
            	)
             AND not regexp_like (t413.C413_CONTROL_NUMBER,'^(NOC#|TBE).*')  
             AND t413.C413_CONTROL_NUMBER     IS NOT NULL
             AND t412.C412_VOID_FL            IS NULL
             AND t413.C413_VOID_FL            IS NULL
           UNION
        	-- RA
          SELECT t507.c205_part_number_id pnum, TRIM (t507.c507_control_number) cnum
            FROM t506_returns t506, t507_returns_item t507
           WHERE t506.c506_rma_id  = t507.c506_rma_id
             AND t506.c506_rma_id IN
               (
                 SELECT MY_TEMP_TXN_ID
                   FROM my_temp_key_value
                  WHERE MY_TEMP_TXN_KEY = 'RA'
               )
            AND not regexp_like (t507.c507_control_number,'^(NOC#|TBE).*')  
            AND t506.c506_type <> '3300' 
            AND TRIM (t507.c507_control_number) IS NOT NULL
            AND t506.c506_void_fl               IS NULL
    	)
  		MINUS
 		SELECT c205_part_number_id, c2550_control_number
   		  FROM t2550_part_control_number
		 WHERE c205_part_number_id IN (SELECT c205_part_number_id FROM my_temp_part_list);

	BEGIN
	
		DELETE FROM my_temp_part_list;
	
		-- Insert part number deatils on based on RA, Consignment and In-house transactions Id(s) to my_temp_part_list
		INSERT INTO my_temp_part_list(c205_part_number_id)
	     SELECT t505.c205_part_number_id 
           FROM T504_CONSIGNMENT t504, T505_ITEM_CONSIGNMENT t505
          WHERE t504.c504_consignment_id  = t505.c504_consignment_id
            AND t504.c504_consignment_id IN
            (
                 SELECT MY_TEMP_TXN_ID FROM my_temp_key_value WHERE MY_TEMP_TXN_KEY = 'CN'
            )
           AND t504.c504_void_fl IS NULL
           AND t505.C505_VOID_FL IS NULL
           GROUP BY t505.c205_part_number_id
           UNION ALL
           
         SELECT t413.C205_PART_NUMBER_ID
           FROM T412_INHOUSE_TRANSACTIONS t412, T413_INHOUSE_TRANS_ITEMS t413
          WHERE t412.C412_INHOUSE_TRANS_ID  = t413.C412_INHOUSE_TRANS_ID
            AND t412.C412_INHOUSE_TRANS_ID IN
            (
                 SELECT MY_TEMP_TXN_ID FROM my_temp_key_value WHERE MY_TEMP_TXN_KEY = 'IN_HOUSE'
            )
            AND t412.C412_VOID_FL IS NULL
            AND t413.C413_VOID_FL IS NULL
	        GROUP BY t413.c205_part_number_id
			UNION ALL
			
		 SELECT t507.c205_part_number_id
           FROM t506_returns t506, t507_returns_item t507
          WHERE t506.c506_rma_id  = t507.c506_rma_id
            AND t506.c506_rma_id IN
            (
                 SELECT MY_TEMP_TXN_ID FROM my_temp_key_value WHERE MY_TEMP_TXN_KEY = 'RA'
            )
            AND t506.c506_void_fl IS NULL
            GROUP BY t507.c205_part_number_id;
            
		-- Loop is used to update or insert the partnum and control number details based on Consignment,
		-- RA and In-house transaction
    	FOR v_all_trans IN all_trans_data
		LOOP 

			v_id := '';

			gm_pkg_op_partcontrol_txn.gm_save_part_control_info (v_id, v_all_trans.pnum, v_all_trans.cnum, NULL,
                NULL, NULL, NULL, NULL, p_user_id, NULL, NULL, NULL);
		END LOOP;
           
END gm_sav_lot_override_by_txn;

END gm_pkg_op_partcontrol_txn;
/