/* Formatted on 2010/11/16 11:53 (Formatter Plus v4.8.0) */
-- @"C:\Database\Packages\Operations\receiving\gm_pkg_op_bulk_dhr_split_txn.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_bulk_dhr_split_txn
IS
--

	/*************************************************************************
	* Description : Procedure to fetch all the DHR id from RS
	**************************************************************************/
	PROCEDURE gm_fch_rs_dhr_list(
		p_rs_id		IN		T4081_DHR_BULK_RECEIVE.c4081_dhr_bulk_id%TYPE,
		p_out_cur	OUT		TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_out_cur
		FOR
			SELECT T4082.c408_dhr_id dhrid
			FROM T4082_BULK_DHR_MAPPING T4082,
			  T408_DHR T408
			WHERE T4082.C408_DHR_ID     = T408.C408_DHR_ID
			AND t4082.C4081_DHR_BULK_ID = p_rs_id
			AND T4082.C4082_VOID_FL    IS NULL
			AND T408.C408_VOID_FL      IS NULL
			AND T408.C408_STATUS_FL     < 4; -- verified
		
	END gm_fch_rs_dhr_list;
	
	/*************************************************************************
	* Description : Procedure to fetch all details from RS
	**************************************************************************/
	PROCEDURE gm_fch_rs_txn_dtls(
		p_rs_id		IN		t4081_dhr_bulk_receive.c4081_dhr_bulk_id%TYPE,
		p_out_cur	OUT		TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_out_cur
		FOR
			SELECT T4081.C401_PURCHASE_ORD_ID poid,
			  T4081.C2540_DONOR_ID donorid,
			  T4081.C4081_PACKING_SLIP packslip,
			  T401.C301_VENDOR_ID vendorid
			FROM T4081_DHR_BULK_RECEIVE T4081,
			  T401_PURCHASE_ORDER T401
			WHERE T4081.C4081_DHR_BULK_ID  = p_rs_id
			AND T4081.C401_PURCHASE_ORD_ID = T401.C401_PURCHASE_ORD_ID
			AND T4081.C4081_VOID_FL       IS NULL
			AND T401.C401_VOID_FL         IS NULL;
		
	END gm_fch_rs_txn_dtls;

	/*************************************************************************
	* Description : Procedure to update the input string with new DHR details
	**************************************************************************/
	FUNCTION gm_upd_dhr_input_str_tmp(
		  p_po_number 	IN 		temp_bulk_dhr_table.c401_po_number%TYPE
		, p_input_str 	IN		CLOB
		, p_rs_id		IN		t4081_dhr_bulk_receive.c4081_dhr_bulk_id%TYPE
	)
	RETURN CLOB
	IS
		v_dhrid			T408A_DHR_CONTROL_NUMBER.c408_dhr_id%TYPE;
		v_input_str		CLOB;
		v_instr_cnt		NUMBER;
	
		CURSOR cur_temp_dtls
		IS
			SELECT C205_PART_NUMBER_ID pnum, c205_control_number ctrlnum
			FROM TEMP_BULK_DHR_TABLE;
	
	BEGIN
		--gm_pkg_op_bulk_dhr_txn.gm_sav_dhr_input_str_tmp(p_po_number,p_input_str);
		FOR v_index in cur_temp_dtls
		LOOP
			-- Get the DHR id for the part number and control number
			BEGIN
				SELECT c408_dhr_id INTO v_dhrid
				FROM T408A_DHR_CONTROL_NUMBER
				WHERE C205_PART_NUMBER_ID = v_index.pnum
				AND c408a_conrol_number = v_index.ctrlnum
				AND c408_dhr_id IN (SELECT c408_dhr_id FROM t4082_bulk_dhr_mapping WHERE c4081_dhr_bulk_id = p_rs_id AND c4082_void_fl IS NULL )
				AND c408a_void_fl IS NULL;
			EXCEPTION WHEN NO_DATA_FOUND
			THEN
				RETURN p_input_str;
			END;
			-- update the temp table with new dhr id
			UPDATE TEMP_BULK_DHR_TABLE
			SET c408_dhr_id = v_dhrid
			WHERE C205_PART_NUMBER_ID = v_index.pnum
			AND c205_control_number = v_index.ctrlnum;
		END LOOP;
		/* Prepare the string based on new DHR id 
		 * approve/reject string format: pnum^cnum^custsize^expdt^mgfdt^dhrid^status^^1|
		 * Inventory string format: partNum^ctrlNum^custsize^expdate^manfdate^dhrId^^moveTo^1|';
		 */
		BEGIN
			SELECT (RTRIM(XMLAGG(XMLELEMENT(E,INPUTSTR || '|')).EXTRACT('//text()').GETCLOBVAL(),'|') ) || '|'
			INTO v_input_str
			FROM
			(SELECT C205_PART_NUMBER_ID
			  ||'^'
			  ||C205_CONTROL_NUMBER
			  ||'^'
			  || C205_CUSTOM_SIZE
			  ||'^'
			  ||TO_CHAR(C205_EXP_DT,'mm/dd/yyyy')
			  ||'^'
			  ||TO_CHAR(C205_MFG_DT,'mm/dd/yyyy')
			  ||'^'
			  ||C408_DHR_ID
			  ||'^'
			  ||C901_INS_STATUS
			  ||'^'
			  ||C901_LOC_TYPE
			  ||'^'
			  ||C205_QTY inputstr
			FROM TEMP_BULK_DHR_TABLE); 

		EXCEPTION WHEN NO_DATA_FOUND
		THEN
			RETURN p_input_str;
		END;
		
		SELECT INSTR(v_input_str,'|') INTO v_instr_cnt 
		FROM DUAL;
		  
		  IF v_instr_cnt <= 1
		  THEN
		  	v_input_str := p_input_str;
		  END IF;
		
		RETURN v_input_str;
	END gm_upd_dhr_input_str_tmp;

	/*************************************************************************
	* Description : Procedure to save the rejected RS id to the existing RS id
	**************************************************************************/
	PROCEDURE gm_op_sav_linked_rs_detail(
		p_bulk_id		IN	t4081_dhr_bulk_receive.c4081_dhr_bulk_id%TYPE,
		p_rej_bulk_id	IN  t4081_dhr_bulk_receive.c4081_dhr_bulk_id%TYPE,
		p_user_id		IN	t4081_dhr_bulk_receive.c4081_last_updated_by%TYPE
	)
	AS
		v_comments		t4081_dhr_bulk_receive.c4081_comments%TYPE;
	BEGIN
		
		SELECT DECODE(c4081_comments,NULL,'Shipment is split into following transaction(s): ',c4081_comments||',')
			INTO v_comments
		FROM t4081_dhr_bulk_receive
		WHERE c4081_dhr_bulk_id = p_bulk_id
		AND c4081_void_fl IS NULL;
		
		UPDATE t4081_dhr_bulk_receive
		SET c4081_comments = v_comments||' '||p_rej_bulk_id
			, c4081_last_updated_by = p_user_id
			, c4081_last_updated_date = CURRENT_DATE
		WHERE c4081_dhr_bulk_id = p_bulk_id
		AND c4081_void_fl IS NULL;
	END gm_op_sav_linked_rs_detail;
	
	/*************************************************************************
	* Description : Procedure to void RS-DHR mapping
	**************************************************************************/
	PROCEDURE gm_op_sav_void_rsdhr(
		p_dhr_id	IN		t4082_bulk_dhr_mapping.c408_dhr_id%TYPE
		, p_bulk_id	IN		t4082_bulk_dhr_mapping.c4081_dhr_bulk_id%TYPE
		, p_user_id	IN		t4082_bulk_dhr_mapping.c4082_last_updated_by%TYPE
	)
	AS
	BEGIN
		UPDATE t4082_bulk_dhr_mapping
		SET c4082_void_fl = 'Y'
		 , c4082_last_updated_by = p_user_id
		 , c4082_last_updated_date = CURRENT_DATE
		WHERE c408_dhr_id = p_dhr_id
			AND c4081_dhr_bulk_id = p_bulk_id
			AND c4082_void_fl IS NULL;
	END gm_op_sav_void_rsdhr;
	
	/*****************************************************************************************************
	* Description : Procedure to prepare the string for inhouse transactions to skip verification process 
	******************************************************************************************************/
	PROCEDURE gm_fch_dhr_split_txn_str(
		p_bulk_id	IN		t4082_bulk_dhr_mapping.c4081_dhr_bulk_id%TYPE
		, p_string	OUT		CLOB
	)
	AS
		v_instr_cnt		NUMBER;
	BEGIN
		
		-- If the transaction is available in rules table, need to skip the verification process for that txn
		
		SELECT (RTRIM(XMLAGG(XMLELEMENT(E,INPUTSTR || '|')).EXTRACT('//text()').GETCLOBVAL(),'|') ) || '|'
		INTO p_string
		FROM
		  (SELECT T408A.C205_PART_NUMBER_ID
		    || '^'
		    || T408A.C408A_CONROL_NUMBER
		    || '^^^^'
		    || T4082.C408_DHR_ID
		    || '^^^1' inputstr
		  FROM T4082_BULK_DHR_MAPPING T4082,
		    T408A_DHR_CONTROL_NUMBER T408A ,
		    t412_inhouse_transactions t412,
		    t413_inhouse_trans_items t413
		  WHERE C4081_DHR_BULK_ID       = p_bulk_id
		  AND T4082.C408_DHR_ID         = T408A.C408_DHR_ID
		  AND t4082.c4081_dhr_bulk_id   = t412.C412_REF_ID
		  AND t412.C412_INHOUSE_TRANS_ID= t413.c412_inhouse_trans_id
		  AND T413.C413_CONTROL_NUMBER  = T408A.C408A_CONROL_NUMBER
		  AND T412.C412_TYPE           IN
		    (SELECT c906_rule_value
		    FROM t906_rules
		    WHERE c906_rule_id   = 'SPLIT'
		    AND C906_RULE_GRP_ID = 'SKIP_VERIFICATION'
		    AND c906_void_fl    IS NULL
		    )
		  AND t4082.c4082_void_fl  IS NULL
		  AND T412.C412_VOID_FL    IS NULL
		  AND t413.c413_void_fl    IS NULL
		  AND T4082.C4082_VOID_FL  IS NULL
		  AND t408A.C408A_VOID_FL  IS NULL
		  ) ;
		  
		  SELECT INSTR(p_string,'|') INTO v_instr_cnt 
		  FROM DUAL;
		  
		  IF v_instr_cnt <= 1
		  THEN
		  	p_string := NULL;
		  END IF;
		  
	END gm_fch_dhr_split_txn_str;
	
	/***************************************************************************
	* Description : Procedure to update the status of transactoin in temp table 
	****************************************************************************/
	PROCEDURE gm_upd_dhr_status_temp(
		p_bulk_id	IN		t4082_bulk_dhr_mapping.c4081_dhr_bulk_id%TYPE
	)
	AS
		v_status	t408a_dhr_control_number.c901_status%TYPE;
		CURSOR dhr_cur
		IS
			SELECT c205_part_number_id pnum,
			  c205_control_number ctrlnum,
			  c408_dhr_id dhrid
			FROM temp_bulk_dhr_table;
	BEGIN
		FOR v_index in dhr_cur
		LOOP
			BEGIN
				SELECT c901_status
					INTO v_status
				FROM t408a_dhr_control_number
				WHERE c408a_conrol_number = v_index.ctrlnum
				AND c205_part_number_id = v_index.pnum
				AND c408_dhr_id  = v_index.dhrid;
			EXCEPTION WHEN NO_DATA_FOUND
			THEN
				v_status := NULL;
			END;
			
			UPDATE temp_bulk_dhr_table
				SET c901_ins_status = v_status
			WHERE c205_part_number_id = v_index.pnum
				AND c205_control_number = v_index.ctrlnum
				AND c408_dhr_id  = v_index.dhrid;
		END LOOP;
		
	END gm_upd_dhr_status_temp;
	
	/*************************************************************************************************
	* Description : Procedure to update the status of transactoin in t408a_dhr_control_number table 
	* since it is getting removed while creating new dhr at the time of PP
	****************************************************************************/
	PROCEDURE gm_upd_dhr_lot_sts(
		p_bulk_id	IN		t4082_bulk_dhr_mapping.c4081_dhr_bulk_id%TYPE
	)
	AS
		v_status	t408a_dhr_control_number.c901_status%TYPE;
		
		CURSOR dhr_cur
		IS
			SELECT c205_part_number_id pnum,
			  c408a_conrol_number ctrlnum,
			  c408_dhr_id dhrid
			FROM t408a_dhr_control_number
			WHERE c408a_void_fl      IS NULL
			AND C408_DHR_ID IN
			  (SELECT C408_DHR_ID
			  FROM T4082_BULK_DHR_MAPPING
			  WHERE C4081_DHR_BULK_ID = p_bulk_id
			  AND C4082_VOID_FL      IS NULL
			  );
	BEGIN
		
		FOR v_index in dhr_cur
		LOOP
			BEGIN
				SELECT c901_ins_status
					INTO v_status
				FROM temp_bulk_dhr_table
				WHERE c205_control_number = v_index.ctrlnum
				AND c205_part_number_id = v_index.pnum
				AND c408_dhr_id	= v_index.dhrid;
			EXCEPTION WHEN NO_DATA_FOUND
			THEN
				v_status := NULL;
			END;
			
			IF v_status IS NOT NULL
			THEN
				UPDATE t408a_dhr_control_number
					SET c901_status = v_status
				WHERE c205_part_number_id = v_index.pnum
					AND c408a_conrol_number = v_index.ctrlnum
					AND c408_dhr_id	= v_index.dhrid;
			END IF;
			
		END LOOP;
		
	END gm_upd_dhr_lot_sts;
	
END gm_pkg_op_bulk_dhr_split_txn;
/
