/*
 * @"C:\Database\Packages\Operations\receiving\gm_pkg_op_partlabeling_rpt.bdy";
 */
 
CREATE OR REPLACE PACKAGE BODY gm_pkg_op_partlabeling_rpt
IS
--   	 	
	
	/*************************************************************************
	* Description : This procedure is used to fetch data for part labeling dash board.
	* Authot : Matt B
	**************************************************************************/
	PROCEDURE gm_fch_resdesignate_ptrds(
	p_out_ptrds_cur  OUT  TYPES.cursor_type
	)
	AS
	  v_company_id  T1900_company.c1900_company_id%TYPE;
	  v_dateformat  VARCHAR2(100);
	BEGIN
	    SELECT get_compid_frm_cntx()    INTO v_company_id    FROM DUAL;
	    SELECT get_compdtfmt_frm_cntx() INTO v_dateformat    FROM dual;
	    OPEN p_out_ptrds_cur FOR  
	    
	    SELECT T412.C412_INHOUSE_TRANS_ID CID,
                     NVL(GET_RULE_VALUE('CON_TYPE',C412_TYPE),'I') CON_TYPE,
                     TO_CHAR(DECODE(T412.C412_STATUS_FL,2,'Y','-')) PP,
               TO_CHAR(DECODE(T412.C412_STATUS_FL,1,'Y','-')) PI,
               TO_CHAR(DECODE(T412.C412_STATUS_FL,3,'Y','-')) PV,
                     T412.C412_status_fl STATUS_FL,
               t412.C412_TYPE CTYPE,
               TO_CHAR(T412.c412_created_date,v_dateformat) CREATE_DATE,
               --T412.c412_created_date CREATE_DATE,
               COUNT( distinct T413.c205_part_number_id) PART_NUM_COUNT,
               SUM(t413.c413_item_qty) QTY,
               NVL(V412a.Labels_Printed,'N') LABELS,
               NVL(V412a.QC_Verified,'N') QC,
               T412.C2540_DONOR_NUMBER DONORID
              FROM t412_inhouse_transactions T412 ,
               T413_inhouse_trans_items T413 ,
               V412a_inhouse_trans_attribute V412a
         WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
               AND t412.c412_status_fl          < 4
               AND t412.C412_type               = 103932 --Part Re-designation
               AND t412.c412_inhouse_trans_id   = v412a.C412_INHOUSE_TRANS_ID(+)
               AND t412.c1900_company_id        = v_company_id
               AND C412_VOID_FL                IS NULL
      GROUP BY T412.C412_INHOUSE_TRANS_ID,
             T412.C412_TYPE,
             T412.C412_status_fl,
             T412.c412_created_date,
             T412.C2540_DONOR_NUMBER,
             V412a.Labels_Printed,
             V412a.QC_Verified 
      ORDER BY t412.c412_created_date;
	   
	    
	
    END gm_fch_resdesignate_ptrds;
 
  
	/*************************************************************************
	* Description : This procedure will fetch label parameters for transaction.
	* Author : Matt B
	**************************************************************************/
	procedure gm_fch_partlabel_parameters(
	    p_ref_id 		   	IN 	T412a_INHOUSE_TRANS_ATTRIBUTE.C412_INHOUSE_TRANS_ID%TYPE
	   ,p_out               OUT TYPES.cursor_type
	)
	AS
	BEGIN
		 OPEN p_out FOR
	      SELECT Labels_Printed LABELS, QC_Verified QC, status_fl  STATUS_FL,LABEL_PRE_STAGE LBL_PRE_STAGE, LABEL_STAGE LBL_STAGE
	     FROM V412a_inhouse_trans_attribute 
	      WHERE c412_inhouse_trans_id = p_ref_id; 

	END gm_fch_partlabel_parameters;
    	
END gm_pkg_op_partlabeling_rpt;
/