CREATE OR REPLACE PACKAGE BODY gm_pkg_op_partcontrol_rpt
IS

/******************************************************************************************
	* Description : This procedure is used to fetch the details of the Control Number
	*******************************************************************************************/

   PROCEDURE gm_fch_part_control_info (
      p_pc_id   IN       t2550_part_control_number.c205_part_number_id%TYPE,
      p_data             OUT      TYPES.cursor_type
   )
   AS
       v_dateformat  VARCHAR2(100);
       v_company_id t1900_Company.c1900_company_id%type;
       v_plant_id      T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
   BEGIN   
	   		SELECT get_compdtfmt_frm_cntx() ,get_compid_frm_cntx(), get_plantid_frm_cntx()
			  INTO v_dateformat ,v_company_id, v_plant_id 
			  FROM dual;
	   
      OPEN p_data
       FOR
       	SELECT c2550_part_control_number_id PARTCNID
               ,t2550.c205_part_number_id PARTNM
               ,t2550.c2550_control_number controlnm
		       ,t2550.c301_vendor_id vendorid
		       ,t2540.c2540_donor_number donornm
		       , t2540.c2540_donor_id donorid
		       ,t2550.c2550_custom_size customsize
		       ,TO_CHAR(t2550.c2550_expiry_date,v_dateformat) expdate
		       ,TO_CHAR(t2550.c2550_mfg_date,v_dateformat) manfdate
		       ,t2550.c2550_rev_level revlevel
		       ,t2550.c2550_packing_slip packslip
		       ,t2550.c2550_udi_hrf udi
		       ,t205.c205_product_class prodclass
		       ,t205.c205_product_material prodtype
		       ,gm_pkg_op_dhr.get_di_no_from_vendor(t205.C205_PART_NUMBER_ID) SHOW_TXTBOX	  
		 FROM  t2550_part_control_number t2550,t205_part_number t205 , t2540_donor_master t2540
		 WHERE t205.c205_part_number_id = t2550.c205_part_number_id
	     AND   t2550.c2540_donor_id = t2540.c2540_donor_id(+)
		 AND   t2550.c2550_part_control_number_id = p_pc_id
		 AND   t2540.c1900_company_id(+) = v_company_id
		 AND   t2540.c2540_void_fl(+) IS NULL ;
	
	END gm_fch_part_control_info;
	--
	/******************************************************************************************
	* Description : This procedure is used to fetch the details of the Part number while tabout
	*******************************************************************************************/
	 PROCEDURE gm_fch_part_no (
      p_pc_id   IN       t2550_part_control_number.c205_part_number_id%TYPE,
      p_data    OUT      VARCHAR
   )
   AS
   v_data VARCHAR2(4000);
   v_company_id t1900_Company.c1900_company_id%type;
   
   BEGIN   
	   		
	        SELECT get_compid_frm_cntx()  
			  INTO v_company_id
			  FROM dual;
			  
            SELECT  b.C205_PART_NUMBER_ID 
              ||'~'|| DECODE (GM_PKG_PD_RPT_UDI.GET_PART_DI_NUMBER(b.C205_PART_NUMBER_ID), NULL, '', GM_PKG_PD_RPT_UDI.get_udi_format_prefix (b.C205_PART_NUMBER_ID) || GM_PKG_PD_RPT_UDI.GET_PART_DI_NUMBER(b.C205_PART_NUMBER_ID)) 
              ||'~'|| gm_pkg_op_dhr.get_di_no_from_vendor(b.C205_PART_NUMBER_ID)            
              ||'~'|| b.C205_PRODUCT_MATERIAL                              
              ||'~'|| c205_product_class                             
              ||'~'|| gm_pkg_pd_rpt_udi.get_part_udi_format (b.C205_PART_NUMBER_ID) 
              ||'~'|| gm_pkg_pd_rpt_udi.get_part_di_number(b.C205_PART_NUMBER_ID) 
			   INTO v_data                         
          	  from  t205_part_number b,t2023_part_company_mapping a
          WHERE  b.C205_PART_NUMBER_ID = a.C205_PART_NUMBER_ID
            AND b.C205_PART_NUMBER_ID = p_pc_id
            AND a.c1900_company_id = v_company_id;
          p_data := v_data;
          	
	END gm_fch_part_no;
	
	/**************************************************************************************************************
	* Description : This procedure is used to fetch the partcontrol number id for the Part# and Lot# combination
	* Author      : karthik
	***************************************************************************************************************/
	
	PROCEDURE get_part_ctrlnum_id (
	p_pnum  		IN  T2550_PART_CONTROL_NUMBER.C205_PART_NUMBER_ID%TYPE,
	p_cnum  		IN  T2550_PART_CONTROL_NUMBER.C2550_CONTROL_NUMBER%TYPE,
	p_partCnId      OUT T2550_PART_CONTROL_NUMBER.C2550_PART_CONTROL_NUMBER_ID%TYPE
	)
	AS
	v_part_control_id T2550_PART_CONTROL_NUMBER.C2550_PART_CONTROL_NUMBER_ID%TYPE;
	BEGIN
		
		BEGIN
			SELECT C2550_PART_CONTROL_NUMBER_ID
				INTO v_part_control_id
			FROM T2550_PART_CONTROL_NUMBER
			WHERE C205_PART_NUMBER_ID = p_pnum
			AND C2550_CONTROL_NUMBER  = p_cnum;
			EXCEPTION
         WHEN NO_DATA_FOUND THEN
            v_part_control_id := NULL;
           END;
           
		p_partCnId := v_part_control_id;		
	END get_part_ctrlnum_id;

END gm_pkg_op_partcontrol_rpt;
/