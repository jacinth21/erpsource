/* Main Package to calculate and determine the priority when receiving a part in DHR
* @ Author: gpalani PMT-16649 Jun 2018
*
*/
--  @"C:\Projects\branches\PMT\db\Packages\Operations\receiving\gm_pkg_dhr_priority.pkg";

CREATE OR REPLACE PACKAGE BODY gm_pkg_dhr_priority
AS
    
 /******************************************************************
* Description : Main procedure for the DHR priority project
*******************************************************************/
PROCEDURE gm_dhr_priority_update 
       (
        p_dhr_id   IN t408_dhr.c408_dhr_id%TYPE,
        p_user_id  IN t101_user.c101_user_id%TYPE
        )
AS
    v_priority t408_dhr.c901_dhr_priority%TYPE;
    v_part_num t205_part_number.c205_part_number_id%TYPE;
    v_available_qty t413_inhouse_trans_items.c413_item_qty%TYPE;

    
         
BEGIN
	
	    SELECT C205_PART_NUMBER_ID INTO v_part_num
         FROM T408_DHR WHERE
         C408_DHR_ID = p_dhr_id
         AND C408_VOID_FL IS NULL
         ;
	          
             
            gm_pkg_dhr_priority.gm_insert_part_temp(NULL,v_part_num) ;
            
            -- This function retrieves the priority for the given part number
            
            v_priority := gm_dhr_priority_calc_main (v_part_num);
            
            -- Call DHR method to update the DHR with priority returned by the function gm_dhr_priority_calc_main
           
            gm_pkg_dhr_priority.gm_dhr_priority_upd(p_dhr_id, v_priority,p_user_id);
            
      
    END gm_dhr_priority_update;
   
 /******************************************************************
 * Description : Main procedure for the DHR priority project
 *******************************************************************/
PROCEDURE gm_dhr_priority_refresh
(        p_user_id  IN t101_user.c101_user_id%TYPE
)
 
 AS
    v_priority t408_dhr.c901_dhr_priority%TYPE;
    
	
CURSOR cur_dhr
IS
     SELECT t408.c408_dhr_id dhr_id
       FROM t408_dhr t408
      WHERE t408.c408_status_fl<>4
        AND t408.c901_dhr_priority IS NOT NULL
        AND t408.c408_void_fl      IS NULL
        AND t408.c5040_plant_id=gm_pkg_dhr_priority.g_plant_id
        AND t408.c1900_company_id=gm_pkg_dhr_priority.g_company_id
        ORDER BY t408.c408_created_date ASC;
        
BEGIN
    FOR v_dhr IN cur_dhr
    
    LOOP
        
        gm_pkg_dhr_priority.gm_dhr_priority_update (v_dhr.dhr_id,p_user_id);
        COMMIT;
    END LOOP;


 END gm_dhr_priority_refresh;       
  
 /******************************************************************
    * Description : Function to calculate the DHR priority
 ******************************************************************/
    FUNCTION gm_dhr_priority_calc_main
            (
            p_part_num IN t205_part_number.c205_part_number_id%TYPE
            )
    RETURN NUMBER
    
  AS  
    v_priority t408_dhr.c901_dhr_priority%TYPE;
    v_available_qty t413_inhouse_trans_items.c413_item_qty%TYPE;

   
  /* This is the main function which will determine the DHR priority for a given part number. System will start checking the Priority starting with US Sales back order and
   * assign the priority in that order. For eg: If there is a backorder from "US Sales Back Order" Priority 0 will be assigned to the part/DHR transaction and the system
   * will skip the rest of the priority calculation. At any given time, a dhr transaction will have only one priority.
   * 
   * Priority Type:              Priority Number
   *    
   * US Sales Back Order:           Priority 0	
   * OUS Replenishment Back Order:	Priority 1	
   * Item Shipped Set (US and OUS):	Priority 2	
   * US Loaner Item Back order:     Priority 3	
   * One Week Need:	                Priority 4
   * Two Week Need:	                Priority 5	
   * Others:                        Priority 6
   * DHR Moved to NCMR              Priority 7 
   *  
   */
  
   BEGIN
	   
	   --Exclude Unwanted Inventory bucket, as we need only Shelf Quantity for this query.
	     my_context.set_my_inlist_ctx ('70018,70002,70003,70019,70005,70015,70007,70023,70006,70008,70004,70014,70022,70016,70009,70013,70020,70001,70021');
	     
	   -- Setting the context to GMNA
         gm_pkg_cor_client_context.gm_sav_client_context('1000','3000');
         
      	-- Call this procedure to calculate the additional inventory available in the other buckets   
         gm_pkg_dhr_priority.gm_dhr_calc_available_inv(p_part_num,v_available_qty);
   
      -- US sales back order
	     gm_pkg_dhr_priority.gm_dhr_calc_us_bck_order(p_part_num,v_available_qty,v_priority);
	   
	       IF (v_priority = 106840)THEN
	
	       RETURN v_priority;
	        
	       END IF;
       
	     -- OUS sales back order  
	
	      gm_pkg_dhr_priority.gm_dhr_calc_ous_bck_order(p_part_num,v_available_qty,v_priority);
	
	       IF (v_priority = 106841)THEN
	       
	        RETURN v_priority;
	        
	       END IF;
	       
	     -- Item from Shipped Set 
	         gm_pkg_dhr_priority.gm_dhr_calc_item_ship_set(p_part_num,v_available_qty,v_priority);
	
	       IF (v_priority = 106842)THEN
		         
	
	        RETURN v_priority;
	        
	       END IF;
	   
	       
	     ---- Loaner Back Order
	     
	         gm_pkg_dhr_priority.gm_dhr_calc_loaner_bck_order(p_part_num,v_available_qty,v_priority);
	
	       IF (v_priority = 106843)THEN
	       
	 			 
	        RETURN v_priority;
	        
	       END IF;
	       
	      -- One Week Supply
	         gm_pkg_dhr_priority.gm_dhr_calc_week_supply(p_part_num,'ONE',v_available_qty,v_priority);
	
	       IF (v_priority = 106844)THEN
	       
	
	        RETURN v_priority;
	        
	       END IF;
	       
	     -- Two Week Supply
	       gm_pkg_dhr_priority.gm_dhr_calc_week_supply(p_part_num,'TWO',v_available_qty,v_priority);
	
	       IF (v_priority = 106845)THEN
	
	        RETURN v_priority;
	       
	       END IF;
	       
	-- For others   
	        
	      v_priority:=26240446;
	     
	       RETURN v_priority;

   
END gm_dhr_priority_calc_main;

/******************************************************************
* Description : Procedure to calculate the DHR priority for US sales Back Order
*******************************************************************/
PROCEDURE gm_dhr_calc_us_bck_order (
        p_part_num      IN t205_part_number.c205_part_number_id%TYPE,
        p_available_qty IN t413_inhouse_trans_items.c413_item_qty%TYPE,
        p_priority      OUT NUMBER
)
AS
    v_us_sales_bck_qty NUMBER;
    v_us_bck_order_priority t408_dhr.c901_dhr_priority%TYPE;
    v_received_dhr_qty t413_inhouse_trans_items.c413_item_qty%TYPE;
     
BEGIN

	v_us_bck_order_priority:='106840';  -- 106840 US sales back order code id
       
     SELECT SUM (t502.c502_item_qty)
       INTO v_us_sales_bck_qty 
       FROM T501_ORDER T501, T502_ITEM_ORDER T502
      WHERE T501.C501_ORDER_ID       = T502.C501_ORDER_ID
        AND T502.C205_PART_NUMBER_ID = p_part_num
        AND T501.C901_ORDER_TYPE     = '2525' -- 2525 Back Order type
        AND T501.C501_STATUS_FL      = 0
        AND T501.C501_DELETE_FL       IS NULL
        AND T501.C501_VOID_FL         IS NULL
        AND T502.C502_VOID_FL       IS NULL
        AND (T501.c901_ext_country_id IS NULL
        OR T501.c901_ext_country_id   IN
        ( SELECT country_id FROM v901_country_codes
        ))
        AND EXISTS
        (
         SELECT V700S.ac_id
           FROM v700_territory_mapping_detail V700S
          WHERE (V700S.COMPID                = DECODE (100800, 100803, NULL, 100800)
            OR DECODE (100800, 100803, 1, 2) = 1)
            AND V700S.ac_id                  = T501.C704_ACCOUNT_ID
         )
        AND EXISTS
        (
         SELECT V700S.REP_ID
           FROM V700_TERRITORY_MAPPING_DETAIL V700S
          WHERE V700S.DIVID IN (100823)
            AND V700S.REP_ID = T501.C703_SALES_REP_ID
         )
        AND T502.C502_ITEM_QTY       > 0
        AND T501.C501_STATUS_FL      = 0
        AND (T501.C1900_COMPANY_ID    = gm_pkg_dhr_priority.g_company_id
        OR  T501.C5040_PLANT_ID      = gm_pkg_dhr_priority.g_company_id);
        
   BEGIN
	 
   SELECT NVL(SUM (c408_qty_received),0)
   INTO v_received_dhr_qty ----- 40
   FROM t408_dhr
  WHERE c205_part_number_id = p_part_num
    AND c901_dhr_priority   = v_us_bck_order_priority 
    AND c408_status_fl <> 4
    AND c1900_company_id=gm_pkg_dhr_priority.g_company_id
    AND c408_void_fl IS  NULL
    AND c5040_plant_id=gm_pkg_dhr_priority.g_plant_id;
    
    EXCEPTION
     WHEN OTHERS THEN v_received_dhr_qty := 0;
     END;

  
  IF (v_us_sales_bck_qty > (p_available_qty+v_received_dhr_qty)) THEN

     p_priority:= v_us_bck_order_priority; -- Code ID for priority 0
          
    END IF;
    
  END gm_dhr_calc_us_bck_order;
  
    
  /******************************************************************
* Description : Procedure to calculate the DHR priority for OUS sales replenshiment
****************************************************************/
PROCEDURE gm_dhr_calc_ous_bck_order (
        p_part_num IN t205_part_number.c205_part_number_id%TYPE,
        p_available_qty IN t413_inhouse_trans_items.c413_item_qty%TYPE,
        p_priority OUT NUMBER)
AS
    v_ous_sales_bck_qty NUMBER;
    v_ous_bck_order_priority t408_dhr.c901_dhr_priority%TYPE;
    v_available_qty t408_dhr.C408_QTY_RECEIVED%TYPE;
    v_received_dhr_qty t408_dhr.C408_QTY_RECEIVED%TYPE;

BEGIN
	
	v_ous_bck_order_priority:='106841'; -- 106840 OUS Sales Order Replenshipment back order code id

 
 SELECT SUM(T521.C521_QTY) INTO v_ous_sales_bck_qty
       FROM T520_REQUEST T520, T521_REQUEST_DETAIL T521, T504_CONSIGNMENT T504, T4020_DEMAND_MASTER T4020
      WHERE 
            T520.C901_REQUEST_SOURCE = '4000121' -- 4000121 OUS SALES RESTOCK
        AND T520.C520_REQUEST_ID     = T521.C520_REQUEST_ID
        AND T520.C520_STATUS_FL      = '10' ---10; Back order
        AND T521.C205_PART_NUMBER_ID = p_part_num 
        AND T504.C504_VOID_FL       IS NULL
        AND T520.C520_VOID_FL       IS NULL
        AND T520.C520_REQUEST_ID     = T504.C520_REQUEST_ID (+)
        AND T520.C5040_PLANT_ID      = T504.C5040_PLANT_ID (+)
        AND T520.C520_REQUEST_TO    IS NOT NULL
        AND T520.C520_REQUEST_TXN_ID = T4020.C4020_DEMAND_MASTER_ID (+)
        AND (T520.C1900_COMPANY_ID=gm_pkg_dhr_priority.g_company_id
        OR   T520.C5040_PLANT_ID=gm_pkg_dhr_priority.g_company_id);
     

      BEGIN
		SELECT NVL(SUM (c408_qty_received),0)
	   INTO v_received_dhr_qty ----- 40
	   FROM t408_dhr
	  WHERE c205_part_number_id = p_part_num
	    AND c901_dhr_priority   = v_ous_bck_order_priority 
	    AND c408_status_fl <> 4
	    AND c1900_company_id=gm_pkg_dhr_priority.g_company_id
	    AND c408_void_fl IS  NULL
	    AND c5040_plant_id=gm_pkg_dhr_priority.g_plant_id;
	    
    EXCEPTION
     WHEN OTHERS THEN v_received_dhr_qty := 0;
     END;
     
     IF (v_ous_sales_bck_qty > (p_available_qty+v_received_dhr_qty)) THEN

     p_priority:= v_ous_bck_order_priority; -- Code ID for priority 1
     
    END IF;
  
END  gm_dhr_calc_ous_bck_order;
  
  
  
   /******************************************************************
   * Description : Procedure to calculate the DHR priority for Item Shipped Set
   ****************************************************************/
   PROCEDURE gm_dhr_calc_item_ship_set (
      p_part_num      IN       t205_part_number.c205_part_number_id%TYPE,
      p_available_qty IN t413_inhouse_trans_items.c413_item_qty%TYPE,
      p_priority      OUT      NUMBER
   )
  
   AS
   v_item_ship_set_qty NUMBER;
   v_item_ship_set_priority NUMBER; -- DHR PRIORITY FOR Item Ship Set
   v_available_qty t413_inhouse_trans_items.c413_item_qty%TYPE;
   v_received_dhr_qty t408_dhr.C408_QTY_RECEIVED%TYPE;


	       
 BEGIN
	 
   v_item_ship_set_priority:=106842; -- 106842 Item Shipped Set
 
	 
       SELECT NVL(SUM(T521.C521_QTY),0)
        INTO v_item_ship_set_qty
       FROM T520_REQUEST T520, T521_REQUEST_DETAIL T521
       WHERE 
            T520.C520_REQUEST_ID     = T521.C520_REQUEST_ID
        AND T520.C520_STATUS_FL      = '10' -- Backorder flag
        and T520.C520_VOID_FL           IS NULL
        AND T521.C205_PART_NUMBER_ID = p_part_num --Sample Part Number
        AND T520.C520_REQUEST_TO        IS NOT NULL
        AND (T520.C1900_COMPANY_ID= gm_pkg_dhr_priority.g_company_id
        OR  T520.C5040_PLANT_ID  =  gm_pkg_dhr_priority.g_company_id)
        AND T520.C520_MASTER_REQUEST_ID IN
          (
         SELECT C520_REQUEST_ID
           FROM T520_REQUEST
          WHERE C520_STATUS_FL = 40
            AND C207_SET_ID   IS NOT NULL
          );
    
      BEGIN
		SELECT NVL(SUM (c408_qty_received),0)
	   INTO v_received_dhr_qty ----- 40
	   FROM t408_dhr
	  WHERE c205_part_number_id = p_part_num
	    AND c901_dhr_priority   = v_item_ship_set_priority 
	    AND c408_status_fl <> 4
	    AND c1900_company_id=gm_pkg_dhr_priority.g_company_id
	    AND c408_void_fl IS  NULL
	    AND c5040_plant_id=gm_pkg_dhr_priority.g_plant_id;
	    
    EXCEPTION
     WHEN OTHERS THEN v_received_dhr_qty := 0;
     END;
     
  
	  IF (v_item_ship_set_qty > (p_available_qty+v_received_dhr_qty)) THEN
	
	     p_priority:= v_item_ship_set_priority; -- Code ID for priority 2
	     
	    END IF;
    
  END gm_dhr_calc_item_ship_set;
  
  
  /******************************************************************
   * Description : Procedure to calculate the DHR priority for Loaner Sets
   ****************************************************************/
   PROCEDURE gm_dhr_calc_loaner_bck_order (
      p_part_num      IN       t205_part_number.c205_part_number_id%TYPE,
      p_available_qty IN       t413_inhouse_trans_items.c413_item_qty%TYPE,
      p_priority      OUT      NUMBER
   )
  
   AS
   v_loaner_set_qty NUMBER;
   v_loaner_set_qty_priority NUMBER; -- DHR PRIORITY FOR Item Ship Set
   v_received_dhr_qty t408_dhr.C408_QTY_RECEIVED%TYPE;


       
 BEGIN

  v_loaner_set_qty_priority:=106843; -- 106842 Loaner Back Order
 
 
  	 SELECT NVL(SUM(T413.C413_ITEM_QTY),0) INTO v_loaner_set_qty
     FROM T412_INHOUSE_TRANSACTIONS T412, T413_INHOUSE_TRANS_ITEMS T413, T504A_LOANER_TRANSACTION T504A
    
        WHERE T412.C412_TYPE             = '100062' --100062: Loaner Back Order
        AND   T412.C412_STATUS_FL        = '0' -- 0: Loaner Back order not yet processed
        AND   T412.C412_VOID_FL         IS NULL
        AND   T413.C413_VOID_FL         IS NULL
        AND   T413.C205_PART_NUMBER_ID   = p_part_num 
        AND   T412.C412_INHOUSE_TRANS_ID = T413.C412_INHOUSE_TRANS_ID 
        AND   t412.c504a_loaner_transaction_id=t504a.c504a_loaner_transaction_id(+)
        AND   T412.C1900_COMPANY_ID=gm_pkg_dhr_priority.g_company_id
        AND   T412.C5040_PLANT_ID=gm_pkg_dhr_priority.g_plant_id
        AND   T412.C5040_PLANT_ID= T504A.C5040_PLANT_ID (+);


  BEGIN
     SELECT NVL(SUM (c408_qty_received),0)
	   INTO v_received_dhr_qty ----- 40
	   FROM t408_dhr
	  WHERE c205_part_number_id = p_part_num
	    AND c901_dhr_priority   = v_loaner_set_qty_priority 
	    AND c408_status_fl <> 4
	    AND c1900_company_id=gm_pkg_dhr_priority.g_company_id
	    AND c408_void_fl IS  NULL
	    AND c5040_plant_id=gm_pkg_dhr_priority.g_plant_id;
	    
    EXCEPTION
     WHEN OTHERS THEN v_received_dhr_qty := 0;
     END;

	  IF (v_loaner_set_qty > (p_available_qty+v_received_dhr_qty)) THEN
	
	     p_priority:= v_loaner_set_qty_priority; -- Code ID for priority 3
	     
	    END IF;
  
END gm_dhr_calc_loaner_bck_order;
  
  
  
   /******************************************************************
   * Description : Procedure to calculate the DHR priority for One Week and two week Supply
   ****************************************************************/
   PROCEDURE gm_dhr_calc_week_supply (
      p_part_num        IN       t205_part_number.c205_part_number_id%TYPE,
      p_week_type       IN       VARCHAR,
      p_available_qty   IN t413_inhouse_trans_items.c413_item_qty%TYPE,
      p_priority        OUT      NUMBER      
   )
  
   AS
   v_one_week_supply_avg NUMBER;
   v_two_week_supply_avg NUMBER;
   v_one_week_supply_priority NUMBER; -- DHR PRIORITY FOR ONE WEEK SUPPLY
   v_two_week_supply_priority NUMBER; -- DHR PRIORITY FOR TWO WEEK SUPPLY
   v_available_qty t413_inhouse_trans_items.c413_item_qty%TYPE;
   v_shelf_qty t205c_part_qty.C205_AVAILABLE_QTY%TYPE;
   v_received_dhr_qty t408_dhr.C408_QTY_RECEIVED%TYPE;


     
 BEGIN
 	
   -- Fetch last 3 months qty sold and determine 1 week supply needed for a part.    
     SELECT NVL((SUM (T502.C502_ITEM_QTY) / 12),0) 
       INTO v_one_week_supply_avg
       FROM T501_ORDER T501, T502_ITEM_ORDER T502
      WHERE T501.C501_ORDER_ID            = T502.C501_ORDER_ID
        AND TRUNC (T501.C501_ORDER_DATE) >= CURRENT_DATE - 90
        AND TRUNC (T501.C501_ORDER_DATE) <= CURRENT_DATE 
        AND T501.C901_ORDER_TYPE      NOT IN (SELECT c906_rule_value FROM v901_order_type_grp)
        AND T502.C205_PART_NUMBER_ID = p_part_num
        AND T501.C501_VOID_FL IS NULL
        AND T502.C502_VOID_FL IS NULL
        AND T501.C1900_COMPANY_ID=gm_pkg_dhr_priority.g_company_id
        AND T501.C5040_PLANT_ID=gm_pkg_dhr_priority.g_plant_id;
        
   IF(p_week_type='ONE') THEN
   
      v_one_week_supply_priority :=106844; -- Code ID for One week Need


    BEGIN
     SELECT NVL(SUM (c408_qty_received),0)
	   INTO v_received_dhr_qty ----- 40
	   FROM t408_dhr
	  WHERE c205_part_number_id = p_part_num
	    AND c901_dhr_priority   = v_one_week_supply_priority 
	    AND c408_status_fl <> 4
	    AND c1900_company_id=gm_pkg_dhr_priority.g_company_id
	    AND c408_void_fl IS  NULL
	    AND c5040_plant_id=gm_pkg_dhr_priority.g_plant_id;
	    
    EXCEPTION
     WHEN OTHERS THEN v_received_dhr_qty := 0;
     END;
    
    IF ((p_available_qty+v_received_dhr_qty)  < v_one_week_supply_avg) THEN
    
        p_priority      := v_one_week_supply_priority;
        
    END IF;
 END IF;  
	    
   IF(p_week_type='TWO') THEN
   
       v_two_week_supply_priority :=106845; -- Code ID for Two week Need
       
       BEGIN
     SELECT NVL(SUM (c408_qty_received),0)
	   INTO v_received_dhr_qty ----- 40
	   FROM t408_dhr
	  WHERE c205_part_number_id = p_part_num
	    AND c901_dhr_priority   = v_one_week_supply_priority 
	    AND c408_status_fl <> 4
	    AND c1900_company_id=gm_pkg_dhr_priority.g_company_id
	    AND c408_void_fl IS  NULL
	    AND c5040_plant_id=gm_pkg_dhr_priority.g_plant_id;
	    
    EXCEPTION
    
     WHEN OTHERS THEN v_received_dhr_qty := 0;
     
     END;

  --  Find the supply needed for two week.
       v_two_week_supply_avg := v_one_week_supply_avg * 2;
   
  
       IF ((p_available_qty+v_received_dhr_qty)  < v_two_week_supply_avg) THEN
       
          p_priority := v_two_week_supply_priority;
        
       END IF;
   
       
  END IF;
  
 
END gm_dhr_calc_week_supply;
  
  /******************************************************************
   * Description : Procedure to Update the Priority for the DHR ID. This procedure will be called from the following places
   * 1. Generate DHR ID
   * 2. DHR Priority Refresh Job
   * 3. When Moving a DHR transaction to NCMR transaction
   * 4. When Moving a NCMR transaction to DHR Transaction
   ****************************************************************/
  
     PROCEDURE gm_dhr_priority_upd (
      p_dhr_id     IN       t408_dhr.c408_dhr_id%TYPE,
      p_priority   IN       NUMBER,
      p_user_id    IN       t101_user.c101_user_id%TYPE
   )
   AS
   
   v_priority NUMBER;
   
   BEGIN
	   
   -- Updating the priority for the received DHR part

      UPDATE t408_dhr
        SET c901_dhr_priority    = p_priority
        ,c408_last_updated_by    = p_user_id 
        ,c408_last_updated_date  = current_date
       WHERE c408_dhr_id         = p_dhr_id;

       
 END gm_dhr_priority_upd;
  
 
 
   /******************************************************************
   * Description : Procedure to calculate the Available inventory
   ****************************************************************/
   PROCEDURE gm_dhr_calc_available_inv (
      p_part_num           IN       t205_part_number.c205_part_number_id%TYPE,
   -- p_bck_order_priority IN       NUMBER,
      p_additional_inv     OUT      NUMBER
      
   )
   
   AS
    
    v_dhfg_qty t413_inhouse_trans_items.c413_item_qty%TYPE;
    v_shelf_qty t413_inhouse_trans_items.c413_item_qty%TYPE;
    v_received_dhr_qty t408_dhr.c408_qty_received%TYPE;
    v_part_num t205_part_number.c205_part_number_id%TYPE;

    
 BEGIN
 
  -- Fetch the Shelf Qty for the plant GMNA. 
    BEGIN
	    
     SELECT NVL(C205_INSHELF,0) 
       INTO v_shelf_qty
     FROM v205_qty_in_stock
     WHERE C205_PART_NUMBER_ID=p_part_num ;

     EXCEPTION WHEN OTHERS THEN
     v_shelf_qty:='0';
     
    END;
   -- Fetch the DHFG Qty. This query will give qty which are processed in DHFG but not yet moved to Shelf    
   BEGIN     
    
	 SELECT NVL(SUM(t413.c413_item_qty),0)
	   INTO v_dhfg_qty
	   FROM t412_inhouse_transactions T412, t413_inhouse_trans_items T413
	  WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
	    AND t412.c412_void_fl         IS NULL
	    AND t413.c413_void_fl         IS NULL
	    AND t412.c412_type            = '100067'
	    AND t413.c205_part_number_id  =p_part_num
	    AND t412.c412_status_fl  NOT IN (0,4)
        AND t412.c412_verify_fl IS NULL
        AND t412.c1900_company_id=gm_pkg_dhr_priority.g_company_id
        AND t412.c5040_plant_id=gm_pkg_dhr_priority.g_plant_id;
	   
      EXCEPTION  WHEN OTHERS THEN 
     v_dhfg_qty := 0;
  END;
 -- Add all the available inventory qty and assign to a variable.  
  -- p_additional_inv:=v_received_dhr_qty+v_shelf_qty+v_dhfg_qty;
  p_additional_inv:=v_shelf_qty+v_dhfg_qty;
  
 END gm_dhr_calc_available_inv;
 
    /******************************************************************
   * Description : Procedure to Insert part in temp table
   ****************************************************************/
   PROCEDURE gm_insert_part_temp (
       p_po_id       IN   T402_WORK_ORDER.C401_PURCHASE_ORD_ID%TYPE
      ,p_part_num    IN   T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE)
   
   AS 
   
   v_part_num_tmp T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE;
   
   CURSOR cur_part 
   IS
     SELECT C205_PART_NUMBER_ID v_part_num_tmp
   --   INTO v_part_num_tmp
     FROM T402_WORK_ORDER
     WHERE C401_PURCHASE_ORD_ID =p_po_id;
     
   BEGIN

	DELETE FROM my_temp_part_list;
	
	IF(p_part_num IS NULL) THEN
	
	    FOR v_part IN cur_part
	    
	    LOOP
	    
	     INSERT INTO my_temp_part_list(C205_PART_NUMBER_ID) VALUES (v_part.v_part_num_tmp);
	     
	     END LOOP;
     
     ELSE
     
        INSERT INTO my_temp_part_list(C205_PART_NUMBER_ID) VALUES (p_part_num);
      
  END IF;

  END gm_insert_part_temp;
  
END gm_pkg_dhr_priority;
/
