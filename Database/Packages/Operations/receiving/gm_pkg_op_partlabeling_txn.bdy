/*
 * @"C:\PMT\db\Packages\Operations\receiving\gm_pkg_op_partlabeling_txn.bdy";
 */
 
CREATE OR REPLACE PACKAGE BODY gm_pkg_op_partlabeling_txn
IS
--   	 	
	
	/****************************************************************************************
	* Description : This procedure is used to update the status of PTRD to Pending Processing
	* Author	  : Arajan
	******************************************************************************************/
	PROCEDURE gm_sav_part_label_status(
	    p_txn_id	IN 		t412_inhouse_transactions.c412_inhouse_trans_id%TYPE
	    ,p_user_id	IN		t412_inhouse_transactions.c412_last_updated_by%TYPE
	)
	AS
	  v_status NUMBER;
	  v_void_fl VARCHAR2(10);
	BEGIN
      
        SELECT C412_STATUS_FL, C412_Void_FL  
	         INTO v_status, v_void_fl 
	         FROM t412_inhouse_transactions 
	        WHERE C412_INHOUSE_TRANS_ID = p_txn_id;
	  
	    IF v_status <> 1 THEN
	         GM_RAISE_APPLICATION_ERROR('-20999','415',p_txn_id);
	    END IF;
	    IF v_void_fl IS NOT NULL THEN
	         GM_RAISE_APPLICATION_ERROR('-20999','414',p_txn_id);
	    END IF; 
	
		UPDATE t412_inhouse_transactions
		SET c412_status_fl          = 2,
		  c412_last_updated_by      = p_user_id,
		  c412_last_updated_date    = CURRENT_DATE
		WHERE c412_inhouse_trans_id = p_txn_id
		AND c412_void_fl           IS NULL;
	END gm_sav_part_label_status;
	
	/****************************************************************************************
	* Description : This procedure is used to void the part numbers that are rejected from PTRD
	* Author	  : Arajan
	******************************************************************************************/
	PROCEDURE gm_op_upd_part_label(
	 	p_txn_id	IN 		t412_inhouse_transactions.c412_inhouse_trans_id%TYPE
	    ,p_user_id	IN		t412_inhouse_transactions.c412_last_updated_by%TYPE
	    ,p_rej_str	IN		VARCHAR2
	)
	AS
		v_string	CLOB := p_rej_str;
		v_substring	CLOB;
		v_pnum		t413_inhouse_trans_items.c205_part_number_id%type;
		v_qty		t413_inhouse_trans_items.c413_control_number%type;
		v_control	t413_inhouse_trans_items.c413_control_number%type;
		v_price 	t413_inhouse_trans_items.c413_item_price%type;
		
	BEGIN
		WHILE INSTR (v_string, '|') <> 0
		LOOP
		
			v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
			v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
			v_pnum		:= NULL;
			v_qty		:= NULL;
			v_control	:= NULL;
			v_price 	:= NULL;
			
			v_pnum		:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_qty		:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_control	:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
		
			UPDATE t413_inhouse_trans_items
			SET c413_void_fl = 'Y'
				, c413_last_updated_by = p_user_id
				, c413_last_updated_date = CURRENT_DATE
			WHERE c412_inhouse_trans_id = p_txn_id
			AND c205_part_number_id = v_pnum
			AND c413_control_number = v_control
			AND c413_void_fl           IS NULL;
		END LOOP;
	END gm_op_upd_part_label;
	
	/****************************************************************
	* Description : this proc is used to save printlabel parameters for PP txn
	* Author      :Matt B
	*****************************************************************/
	PROCEDURE gm_sav_printlabel_parameters(
	   p_ref_Id 			  IN 		T412a_INHOUSE_TRANS_ATTRIBUTE.C412_INHOUSE_TRANS_ID%TYPE,
	   p_attrib_type 		  IN 		T412a_INHOUSE_TRANS_ATTRIBUTE.C901_ATTRIBUTE_TYPE%TYPE,
	   p_attrib_value		  IN 		T412a_INHOUSE_TRANS_ATTRIBUTE.C412A_ATTRIBUTE_VALUE%TYPE,
	   p_user_id 			  IN 		T412a_INHOUSE_TRANS_ATTRIBUTE.C412A_CREATED_BY%TYPE
	  )
	AS
	  v_void_fl VARCHAR2(10);
	BEGIN   
	  
	       SELECT C412_Void_FL  
	         INTO v_void_fl 
	         FROM t412_inhouse_transactions 
	        WHERE C412_INHOUSE_TRANS_ID = p_ref_Id;

	    IF v_void_fl IS NOT NULL THEN
	         GM_RAISE_APPLICATION_ERROR('-20999','414',p_ref_Id);
	    END IF; 
	       UPDATE T412a_INHOUSE_TRANS_ATTRIBUTE 
	          SET C412A_ATTRIBUTE_VALUE = p_attrib_value,C412A_LAST_UPDATED_BY = P_USER_ID, C412A_LAST_UPDATED_DATE = SYSDATE 
	        WHERE C412_INHOUSE_TRANS_ID = p_ref_Id AND C901_ATTRIBUTE_TYPE = p_attrib_type  
	          AND C412A_VOID_FL IS NULL;
	        
	    IF SQL%ROWCOUNT = 0 THEN
	       INSERT INTO T412a_INHOUSE_TRANS_ATTRIBUTE (C412A_TRANS_ATTRIBUTE_ID, C412_INHOUSE_TRANS_ID, C901_ATTRIBUTE_TYPE, C412A_ATTRIBUTE_VALUE, C412A_CREATED_BY, C412A_CREATED_DATE)
	       VALUES (S412A_INHOUSE_TRANS.nextval,p_ref_Id, p_attrib_type, p_attrib_value, p_user_id,SYSDATE);
	    END IF;
	END gm_sav_printlabel_parameters;
	
	
	/****************************************************************
	* Description :this proc is used to save printlabel change of status 
	*              to Pending Verification  
	* Author      :Matt B
	*****************************************************************/
	PROCEDURE gm_sav_printlabel_process(
	     P_REF_ID IN T412_INHOUSE_TRANSACTIONS.C412_INHOUSE_TRANS_ID%TYPE
	    ,P_USER_ID IN T412_INHOUSE_TRANSACTIONS.C412_LAST_UPDATED_BY%TYPE
	)
	AS
	   v_status NUMBER;
	   v_void_fl VARCHAR2(10); 
	BEGIN
		    SELECT C412_STATUS_FL, C412_VOID_FL 
		      INTO v_status, v_void_fl 
		      FROM  t412_inhouse_transactions 
		     WHERE C412_INHOUSE_TRANS_ID = P_REF_ID;
		  IF v_status <> 2 THEN
		     GM_RAISE_APPLICATION_ERROR('-20999','415',P_REF_ID);
		  END IF;
		  IF v_void_fl IS NOT NULL THEN 
		     GM_RAISE_APPLICATION_ERROR('-20999','414',P_REF_ID);
		  END IF;
		  
		  UPDATE T412_INHOUSE_TRANSACTIONS 
		      SET C412_STATUS_FL = 3, C412_LAST_UPDATED_BY = P_USER_ID,   C412_LAST_UPDATED_DATE = Sysdate 
		    WHERE C412_INHOUSE_TRANS_ID = P_REF_ID; 
	  END gm_sav_printlabel_process;
	  
	  /**************************************************************************
		* Description :Procedure to update the status to Pending Verification (No status check)
		* Author      :Arajan
	  ***************************************************************************/
	  PROCEDURE gm_op_upd_inhousetxn(
	     p_txn_id IN T412_INHOUSE_TRANSACTIONS.C412_INHOUSE_TRANS_ID%TYPE
	    ,p_user_id IN T412_INHOUSE_TRANSACTIONS.C412_LAST_UPDATED_BY%TYPE
	  )
	  AS
	    v_void_fl VARCHAR2(10);
	  BEGIN
	     SELECT C412_VOID_FL 
		      INTO v_void_fl 
		  FROM  t412_inhouse_transactions 
		  WHERE C412_INHOUSE_TRANS_ID = p_txn_id;
		  
		  IF v_void_fl IS NOT NULL THEN 
	           GM_RAISE_APPLICATION_ERROR('-20999','414',p_txn_id);
		  END IF;
	  
		  UPDATE t412_inhouse_transactions
		  SET c412_status_fl = 3
		  	, c412_last_updated_by = p_user_id
		  	, c412_last_updated_date = CURRENT_DATE
		  WHERE c412_inhouse_trans_id = p_txn_id
		  	AND c412_void_fl IS NULL;
	  END gm_op_upd_inhousetxn;
    	
END gm_pkg_op_partlabeling_txn;
/