create or replace
PACKAGE BODY gm_pkg_op_thb_load_trans
is

/**********************************************************************************************
*Description : Procedure to insert or update the status of the scanned grafts
* author : ssharmila
* Ref # : PMT-30752 
*************************************************************************************************/

PROCEDURE gm_sav_scan_grafts(
    p_load_dtl_id IN T2561_THB_LOAD_ITEM_DETAIL.C2561_THB_LOAD_ITEM_DETAIL_ID%TYPE ,
    p_load_id     IN T2560_THB_LOAD.C2560_THB_LOAD_ID%TYPE,
    p_ctrl_no     IN T2561_THB_LOAD_ITEM_DETAIL.C2561_CONTROL_NUMBER%TYPE,
    p_user_id     IN T2561_THB_LOAD_ITEM_DETAIL.C2561_RECEIVED_BY%TYPE,
    p_tot_out 	  OUT types.CURSOR_TYPE,
    p_dtl_out	  OUT types.CURSOR_TYPE)
AS

  v_load_id 	T2560_THB_LOAD.C2560_THB_LOAD_ID%TYPE;
  v_out_sts		T2560_THB_LOAD.C901_LOAD_STATUS%TYPE;
BEGIN
v_load_id := p_load_id;
  -- Update the Receiving status to 'Y' during scanning.
  UPDATE T2561_THB_LOAD_ITEM_DETAIL
  SET C2561_RECEIVED_STATUS_FL = 'Y',
  	C2561_RECEIVED_BY          = p_user_id,
  	C2561_RECEIVED_DATE		   = CURRENT_DATE,
    C2561_LAST_UPDATED_BY      = p_user_id,
    C2561_LAST_UPDATED_DATE    = CURRENT_DATE
  WHERE C2561_THB_LOAD_ITEM_DETAIL_ID = p_load_dtl_id
  AND C2561_VOID_FL           IS NULL;
  -- To insert new record if the scanned allograft is not listed
  
  IF SQL%ROWCOUNT = 0 THEN
   INSERT
    INTO T2561_THB_LOAD_ITEM_DETAIL
      (
        C2560_THB_LOAD_ID,
        C205_PART_NUMBER_ID,
        C2561_CONTROL_NUMBER,
        C2561_DONOR_NUMBER,
        C2561_RECEIVED_STATUS_FL,
        C2561_RECEIVED_BY ,
        C2561_RECEIVED_DATE ,
        C2561_THB_QTY
      )
      VALUES
      (
        P_LOAD_ID,
        NULL,
        P_CTRL_NO,
        NULL,
        'Y',
        p_user_id,
        CURRENT_DATE,
        1
      );
  END IF;
  -- To fetch the Load_id if new lot number is scanned 
IF v_load_id IS NULL THEN
    BEGIN
      SELECT C2560_THB_LOAD_ID
      INTO v_load_id
      FROM T2561_THB_LOAD_ITEM_DETAIL
      WHERE C2561_CONTROL_NUMBER = P_CTRL_NO;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
    v_load_id := null;
    END;
END IF;

  
  gm_pkg_op_thb_load_report.gm_fch_thb_load_box_details(v_load_id,p_dtl_out,p_tot_out,v_out_sts); 
  
  
END gm_sav_scan_grafts;
/**********************************************************************************************
*Description : Procedure to update the status of the Load details in Master table
* author : ssharmila
* Ref # : PMT-30752 
*************************************************************************************************/
PROCEDURE gm_upd_scan_grafts(
		p_load_id	IN  T2560_THB_LOAD.C2560_THB_LOAD_ID%TYPE
		,p_user_id 	IN  T2561_THB_LOAD_ITEM_DETAIL.C2561_RECEIVED_BY%TYPE
)
AS
BEGIN
  UPDATE T2560_THB_LOAD
  SET C901_LOAD_STATUS      = 107881, --Status - THB Load Processed
    C2560_LAST_UPDATED_BY   = p_user_id,
    C2560_LAST_UPDATED_DATE = CURRENT_DATE,
    C2560_LOAD_RECEIVED_BY  = p_user_id,
    C2560_LOAD_RECEIVED_DATE = CURRENT_DATE
  WHERE C2560_THB_LOAD_ID   = p_load_id
  AND C2560_VOID_FL        IS NULL;
  -- Call Procedure to check and send email notification if all the boxes of a THB load number is completed.
  gm_pkg_op_thb_load_trans.gm_thb_load_closure_box_validation(p_load_id,p_user_id);
  
END gm_upd_scan_grafts;


/**********************************************************************************************
* Description : Procedure to email once all the THB Load is scanned
* Author : gpalani
* Date: 04/26/2020
* PMT - 33764  
*************************************************************************************************/
PROCEDURE gm_thb_load_closure_box_validation(
	p_load_id	IN  t2560_thb_load.c2560_thb_load_id%TYPE,
	p_user_id 	IN  t2561_thb_load_item_detail.c2561_received_by%TYPE
)

AS
v_load_num            t2560_thb_load.C2560_THB_LOAD_NUM%TYPE;
v_completion_count	  NUMBER;

-- Fetch THB Load number from the THB Load ID
 BEGIN
   SELECT T2560.C2560_THB_LOAD_NUM
     INTO v_load_num
   FROM T2560_THB_LOAD T2560
  WHERE C2560_THB_LOAD_ID    = p_load_id
    AND T2560.C2560_VOID_FL IS NULL;
    
-- Check whether all the thb boxes scan are completed.
  SELECT COUNT(1) 
   INTO v_completion_count
  FROM T2560_THB_LOAD T2560
  WHERE T2560.C2560_THB_LOAD_NUM=v_load_num
  AND T2560.C901_LOAD_STATUS='107880' -- 107880 THB Load Initiated. Checking this flag to verify if there are any boxes left for the THB Load.
  AND T2560.C2560_VOID_FL IS NULL;
    
  IF(v_completion_count=0) THEN
  
  gm_pkg_op_thb_load_trans.gm_thb_load_closure_email(v_load_num,p_user_id);
  
  END IF;
   
 
END gm_thb_load_closure_box_validation;


/**********************************************************************************************
* Description : Procedure to email once all the THB Load is scanned
* Author : gpalani
* Date: 04/26/2020
* PMT - 33764  
*************************************************************************************************/
PROCEDURE gm_thb_load_closure_email(
	p_load_num	IN  t2560_thb_load.C2560_THB_LOAD_NUM%TYPE,
	p_user_id 	IN  t2561_thb_load_item_detail.c2561_received_by%TYPE
)
AS
v_thb_load_num  	  t2560_thb_load.C2560_THB_LOAD_NUM%TYPE;
v_thb_load_box_num    t2560_thb_load.C2560_THB_BOX_NUM%TYPE;
v_thb_tot_count		  NUMBER;
v_bba_received_date   VARCHAR2 (1000); -- t2560_thb_load.C2560_LOAD_RECEIVED_DATE%TYPE:=NULL;
v_bba_received_by     t101_user.c101_user_f_name%TYPE;
v_str 				  CLOB;
v_to_mail	 	      VARCHAR2(2000);
v_mail_subject        VARCHAR2 (1000);
v_mail_body           CLOB;
v_col_str			VARCHAR2 (1000);
v_end_row_str		VARCHAR2 (1000);
v_table_str	          CLOB;
CURSOR thb_load_sum
IS 
SELECT 
	t2560.c2560_thb_load_num thb_load_num,
    t2560.c2560_thb_box_num thb_box_num,
    NVL (COUNT (t2561.c2561_thb_qty), 0) total_count,
    TO_CHAR(t2560.c2560_load_received_date,'MM/DD/YYYY HH:MI:SS') bba_received_date,
	t101.c101_user_f_name||' '||t101.c101_user_l_name bba_received_by
  	FROM t2560_thb_load t2560,
	t2561_thb_load_item_detail t2561, T101_USER t101
	 WHERE 
	  t2560.c901_load_status = '107881' -- THB Load Received 
	  AND t2560.c2560_void_fl    IS NULL
      AND t2560.c2560_thb_load_num=p_load_num
	  AND t2561.c2561_void_fl    IS NULL
	  AND t2560.c2560_thb_load_id = T2561.C2560_THB_LOAD_ID
      AND t2560.c2560_load_received_by=T101.C101_USER_ID
	  GROUP BY t2560.c2560_thb_load_num, t2560.c2560_thb_box_num,
	  t2560.c2560_load_received_date,t101.c101_user_f_name,t101.c101_user_l_name
ORDER BY t2560.c2560_thb_load_num, t2560.c2560_thb_box_num asc;  


BEGIN



-- Fetch the Email to and Email Subject messages from Rule
SELECT get_rule_value ('THB_LOAD_EMAIL_TO', 'THB_LOAD_EMAIL')
 INTO v_to_mail
FROM dual;


           
SELECT get_rule_value ('THB_LOAD_EMAIL_SUB', 'THB_LOAD_EMAIL')
 INTO v_mail_subject
FROM dual;
   

v_table_str   := '<TABLE style="border: 1px solid  #676767">'
                        || '<TR><TD width=175><b>THB Load Number</b></TD><TD width=175><b>THB Box Number</b></TD>'
                        || '<TD width=175><b>Box Count</b></TD><TD width=175><b>Received Date/Time</b></TD>'
                        || '<TD width=175><b>Received By</b></TD></TR>';
                        
v_col_str := '<TR><TD>';
v_end_row_str := '</TD><TD>';


    
    FOR v_thb_load_sum IN thb_load_sum
 LOOP
     
    v_thb_load_num:=v_thb_load_sum.thb_load_num;
    v_thb_load_box_num:= v_thb_load_sum.thb_box_num;
    v_thb_tot_count:=v_thb_load_sum.total_count;
    v_bba_received_date:=v_thb_load_sum.bba_received_date;
    v_bba_received_by:=v_thb_load_sum.bba_received_by;
 
 -- below string is the transaction details in a table    
 v_str:= v_str||v_col_str|| v_thb_load_num||v_end_row_str||v_thb_load_box_num||v_end_row_str||v_thb_tot_count||v_end_row_str||
 v_bba_received_date||v_end_row_str||v_bba_received_by||'</TD></TR>';
 
 END LOOP;
 
 v_mail_body  :=
        '<style>TD{ FONT-SIZE: 11px; COLOR: #000000; FONT-FAMILY: verdana, arial, sans-serif;}</style><font face=arial size="2">';        
		v_mail_body := v_mail_body||'THB Rad Run scan completed at BBA for the following Load. Please visit Donor Load detail report for details.<br><br>'
						||v_table_str||v_str;						
		--Call the common procedure to send mail		
		gm_com_send_html_email(v_to_mail, v_mail_subject,'test', v_mail_body);-- Sending mail	
 END;
 /**********************************************************************************************
* Description : Procedure to update the status of the Load details in Master table
* author : tramasamy
*************************************************************************************************/
    PROCEDURE gm_rollback_thb_load_box (
        p_load_id   IN   t2560_thb_load.c2560_thb_load_id%TYPE,
        p_user_id   IN   t2560_thb_load.c2560_thb_processed_by%TYPE
    ) AS
    BEGIN
        UPDATE t2560_thb_load
        SET
            c901_load_status = '107880', --THB Load Initiated 
            c2560_thb_processed_by = '',
            c2560_thb_processed_date = '',
            c2560_last_updated_by = p_user_id,
            c2560_last_updated_date = current_date
        WHERE
            c2560_thb_load_id = p_load_id
            AND c2560_void_fl IS NULL;

        UPDATE t2561_thb_load_item_detail
           SET c2561_received_status_fl = NULL,           
               c2561_received_by        ='' ,                   
               c2561_received_date      ='',                              
               c2561_last_updated_by    = p_user_id,                
               c2561_last_updated_date  = current_date
         WHERE c2560_thb_load_id        = p_load_id
          AND C2561_VOID_FL IS NULL;                       
        
        gm_pkg_op_thb_load_trans.gm_thb_load_box_re_open_email(p_load_id, p_user_id);
    END gm_rollback_thb_load_box;
/**********************************************************************************************
* Description :  Procedure used to email the load box is reopened for re scan 
* author : tramasamy
*************************************************************************************************/
    PROCEDURE gm_thb_load_box_re_open_email (
        p_load_id   IN   t2560_thb_load.c2560_thb_load_id%TYPE,
        p_user_id   IN   t2561_thb_load_item_detail.c2561_received_by%TYPE
    ) AS

        v_load_num            t2560_thb_load.c2560_thb_load_num%TYPE;
        v_box_num             t2560_thb_load.c2560_thb_box_num%TYPE;
        v_thb_load_num        t2560_thb_load.c2560_thb_load_num%TYPE;
        v_thb_load_box_num    t2560_thb_load.c2560_thb_box_num%TYPE;
        v_thb_tot_count       NUMBER;
        v_bba_received_date   VARCHAR2(1000); 
        v_bba_received_by     t101_user.c101_user_f_name%TYPE;
        v_str                 CLOB;
        v_to_mail             VARCHAR2(2000);
        v_mail_subject        VARCHAR2(1000);
        v_mail_body           CLOB;
        v_col_str             VARCHAR2(1000);
        v_end_row_str         VARCHAR2(1000);
        v_table_str           CLOB;
        
    CURSOR thb_load_sum IS
        SELECT
            t2560.c2560_thb_load_num   thb_load_num,
            t2560.c2560_thb_box_num    thb_box_num,
            nvl(COUNT(t2561.c2561_thb_qty), 0) total_count,
            to_char(t2560.c2560_load_received_date, 'MM/DD/YYYY HH:MI:SS') bba_received_date,
            t101.c101_user_f_name
            || ' '
            || t101.c101_user_l_name bba_received_by
        FROM
            t2560_thb_load               t2560,
            t2561_thb_load_item_detail   t2561,
            t101_user                    t101
        WHERE
            t2560.c901_load_status = '107880'-- THB Load Initiated
            AND t2560.c2560_void_fl IS NULL
            AND t2560.c2560_thb_load_id = p_load_id
      -- And t2560.c2560_thb_box_num= v_box_num
            AND t2561.c2561_void_fl IS NULL
            AND t2560.c2560_thb_load_id = t2561.c2560_thb_load_id
            AND t2560.c2560_load_received_by = t101.c101_user_id
        GROUP BY
            t2560.c2560_thb_load_num,
            t2560.c2560_thb_box_num,
            t2560.c2560_load_received_date,
            t101.c101_user_f_name,
            t101.c101_user_l_name
        ORDER BY
            t2560.c2560_thb_load_num,
            t2560.c2560_thb_box_num ASC;

    BEGIN
   
    -- Fetch the Email to and Email Subject messages from Rule
        SELECT get_rule_value('THB_LOAD_EMAIL_TO', 'THB_LOAD_EMAIL')
          INTO v_to_mail FROM dual;
   
   --fetch Re open mail subject    

        SELECT get_rule_value('THB_LOAD_RE_OPEN_SUB', 'THB_LOAD_EMAIL')
          INTO v_mail_subject  FROM dual;

        v_table_str := '<TABLE style="border: 1px solid  #676767">'
                       || '<TR><TD width=175><b>THB Load Number</b></TD><TD width=175><b>THB Box Number</b></TD>'
                       || '<TD width=175><b>Box Count</b></TD><TD width=175><b>Received Date/Time</b></TD>'
                       || '<TD width=175><b>Received By</b></TD></TR>';
        v_col_str := '<TR><TD>';
        v_end_row_str := '</TD><TD>';
        
        FOR v_thb_load_sum IN thb_load_sum LOOP
            v_thb_load_num := v_thb_load_sum.thb_load_num;
            v_thb_load_box_num := v_thb_load_sum.thb_box_num;
            v_thb_tot_count := v_thb_load_sum.total_count;
            v_bba_received_date := v_thb_load_sum.bba_received_date;
            v_bba_received_by := v_thb_load_sum.bba_received_by;
 
 -- below string is the transaction details in a table    
            v_str := v_str
                     || v_col_str
                     || v_thb_load_num
                     || v_end_row_str
                     || v_thb_load_box_num
                     || v_end_row_str
                     || v_thb_tot_count
                     || v_end_row_str
                     || v_bba_received_date
                     || v_end_row_str
                     || v_bba_received_by
                     || '</TD></TR>';

        END LOOP;

        v_mail_body := '<style>TD{ FONT-SIZE: 11px; COLOR: #000000; FONT-FAMILY: verdana, arial, sans-serif;}</style><font face=arial size="2">';
        v_mail_body := v_mail_body
                       || 'THB Rad Run Scan - Re-opened at BBA for the following Load. Please visit Donor Load detail report for details.<br><br>'
                       || v_table_str
                       || v_str;						
		--Call the common procedure to send mail		
        gm_com_send_html_email(v_to_mail, v_mail_subject, 'test', v_mail_body);-- Sending mail	
    END gm_thb_load_box_re_open_email;
		
END GM_PKG_OP_THB_LOAD_TRANS;
/