/* PMT-32710 
*/
--C:\pmt\Database\Packages\Operations\receiving\thbreceiving\gm_pkg_op_thb_lock_bck_ord_txn.pkg";     
CREATE OR REPLACE PACKAGE gm_pkg_op_thb_lock_bck_ord_txn
IS

    
    /******************************************************************
    * Description : Create Analysis lock for the THB Load in t2570_thb_load_lock_master
    * Author   : GPALANI
    ****************************************************************/
    PROCEDURE gm_thb_load_lock_master_main (
		p_load_num_str 			IN CLOB,
    	p_processing_start_date IN t2560_thb_load.c2560_thb_processed_date%TYPE,
    	p_processing_end_date   IN t2560_thb_load.c2560_thb_processed_date%TYPE,
    	p_user_id 				IN t101_user.c101_user_id%TYPE,
    	p_company_id			IN t5041_plant_company_mapping.c1900_company_id%TYPE,
    	p_plant_id				IN t5041_plant_company_mapping.c5040_plant_id%TYPE
	) ;
       
   v_company_id t5041_plant_company_mapping.c1900_company_id%TYPE ;
   v_plant_id 	t5041_plant_company_mapping.c5040_plant_id%TYPE  ;     
	 /******************************************************************
	* Description : Check if the Load Number already has a Lock generated 
	* Author   : GPALANI
	****************************************************************/
	PROCEDURE gm_chk_thb_load_status(
	        p_thb_load_num      IN  t2560_thb_load.c2560_thb_load_num%TYPE,
	        p_thb_load_status   OUT VARCHAR
	);
            
    /******************************************************************
    * Description : Create Analysis lock for the THB Load in t2570_thb_load_lock_master
    * Author   : GPALANI
    ****************************************************************/
	PROCEDURE gm_create_thb_load_lock_master (
        p_processing_start_date 	IN t2570_thb_load_lock_master.c2570_thb_process_start_date%TYPE,
        p_processing_end_date   	IN t2570_thb_load_lock_master.c2570_thb_process_end_date%TYPE,
        p_user_id 					IN t101_user.c101_user_id%TYPE,
        p_load_lock_master_id 		OUT t2570_thb_load_lock_master.c2570_thb_load_lock_master_id%TYPE
     );
            
    /******************************************************************
    * Description : Map the Analysis Lock Master ID to THB Load Number
    * Author   : GPALANI
    ****************************************************************/
    PROCEDURE gm_update_thb_load_num (
        p_load_lock_master_id IN t2570_thb_load_lock_master.c2570_thb_load_lock_master_id%TYPE,
        p_thb_load_num        IN t2560_thb_load.c2560_thb_load_num%TYPE,
        p_user_id             IN t101_user.c101_user_id%TYPE
    ) ;
            
    /**********************************************************************************************
    * Description : Transaction Lock Main Procedure
    * author : GPALANI
    *************************************************************************************************/
    PROCEDURE gm_lock_bck_ord_ptrd_txn (
        p_load_lock_master_id IN t2570_thb_load_lock_master.c2570_thb_load_lock_master_id%TYPE,
        p_user_id             IN t101_user.c101_user_id%TYPE
 	) ;
            
    /**********************************************************************************************
    * Description : Procedure to fetch the back order
    * author : GPALANI
    *************************************************************************************************/
    PROCEDURE gm_fch_bck_ord_txn (
	    p_load_lock_master_id IN t2570_thb_load_lock_master.c2570_thb_load_lock_master_id%TYPE,
	    p_user_id             IN t101_user.c101_user_id%TYPE,
	    p_txn_detail_cursor   IN TYPES.CURSOR_TYPE
    ) ;
            
    /**********************************************************************************************
    * Description : Procedure to fetch the PTRD transaction
    * author : GPALANI
    *************************************************************************************************/
    PROCEDURE gm_fch_ptrd_ord_txn (
	    p_load_lock_master_id IN t2570_thb_load_lock_master.c2570_thb_load_lock_master_id%TYPE,
	    p_user_id             IN t101_user.c101_user_id%TYPE
    ) ;            
            
    /**********************************************************************************************
    * Description : This Procedure to save the Back order and PTRD Transactions in the IT_SAV_BCK_PTRD_LOCK table.
    * The part numbers from this transactions will be used to determine the THB Analsis report
    * author : GPALANI
    *************************************************************************************************/
	PROCEDURE gm_sav_bck_ptrd_lock (
        p_load_lock_master_id IN t2570_thb_load_lock_master.c2570_thb_load_lock_master_id%TYPE,
        p_part_num            IN t205_part_number.c205_part_number_id%TYPE,
        p_acct_id             IN t704_account.c704_account_id%TYPE,
        p_ref_type            IN t412_inhouse_transactions.c412_type%TYPE,
        p_ref_id              IN t412_inhouse_transactions.c412_inhouse_trans_id%TYPE,
        p_req_date            IN VARCHAR2, -- datatype is assigned as varchar since the cursor data is returning varchar
        p_pend_qty            IN t413_inhouse_trans_items.c413_item_qty%TYPE,
        p_created_by          IN t412_inhouse_transactions.c412_created_by%TYPE,
        p_created_date        IN t412_inhouse_transactions.c412_created_date%TYPE,
        p_user_id             IN t101_user.c101_user_id%TYPE
	);
	            
    /**********************************************************************************************
    * Description : This Procedure to load the BO and Rad run part number and the inventory quantities
    * The part numbers from this transactions will be used to determine the THB Analsis report
    * author : GPALANI
    *************************************************************************************************/
    PROCEDURE gm_fetch_thb_load_lock_details (
	    p_load_lock_master_id IN t2570_thb_load_lock_master.c2570_thb_load_lock_master_id%TYPE,
	    p_user_id             IN t101_user.c101_user_id%TYPE
    ) ;
            
            
  /*************************************************************************************************
     * Description : This Procedure to load the Rad Run parts into Lock Details
    * author : GPALANI
    *************************************************************************************************/
	PROCEDURE gm_create_thb_rad_run_load_lock_details (
	        p_load_lock_master_id IN t2570_thb_load_lock_master.c2570_thb_load_lock_master_id%TYPE,
	        p_base_part 		  IN t205_part_number.c205_part_number_id%TYPE,
	        p_private_part 		  IN t205_part_number.c205_part_number_id%TYPE,
	        p_user_id 			  IN t101_user.c101_user_id%TYPE
	 );
    /**********************************************************************************************
    * Description : This Procedure to load the back_order parts, qty and available inventory qty
    * author : GPALANI
    *************************************************************************************************/
    PROCEDURE gm_fetch_back_order_parts (
            p_load_lock_master_id IN t2570_thb_load_lock_master.c2570_thb_load_lock_master_id%TYPE,
            p_user_id             IN t101_user.c101_user_id%TYPE
    ) ;
            
    /**********************************************************************************************
    * Description : This Procedure to load the Rad run parts
    * author : GPALANI
    *************************************************************************************************/
    PROCEDURE gm_fetch_rad_run_parts (
            p_load_lock_master_id IN t2570_thb_load_lock_master.c2570_thb_load_lock_master_id%TYPE,
            p_user_id             IN t101_user.c101_user_id%TYPE
     ) ;
            
    /**********************************************************************************************
    * Description : This Procedure to load the bACK o
    * author : GPALANI
    *************************************************************************************************/
    PROCEDURE gm_fetch_back_order_qty (
            p_load_lock_master_id IN t2570_thb_load_lock_master.c2570_thb_load_lock_master_id%TYPE,
            p_user_id             IN t101_user.c101_user_id%TYPE
     ) ;
            
    /**********************************************************************************************
    * Description : This Procedure to load the Rad Run Qty
    * author : GPALANI
    *************************************************************************************************/
    PROCEDURE gm_fetch_rad_run_qty (
            p_load_lock_master_id IN t2570_thb_load_lock_master.c2570_thb_load_lock_master_id%TYPE,
            p_user_id             IN t101_user.c101_user_id%TYPE
    ) ;
            
    /**********************************************************************************************
    * Description : This Procedure to load the three months sales
    * author : GPALANI
    *************************************************************************************************/
    PROCEDURE gm_fetch_three_months_sales_parts (
            p_load_lock_master_id IN t2570_thb_load_lock_master.c2570_thb_load_lock_master_id%TYPE,
            p_user_id             IN t101_user.c101_user_id%TYPE
     ) ;
            
            
	/**********************************************************************************************
	* Description : This Procedure to load base part entry separately
	* author : GPALANI
	*************************************************************************************************/
	PROCEDURE gm_fetch_base_part (
	        p_load_lock_master_id IN t2570_thb_load_lock_master.c2570_thb_load_lock_master_id%TYPE,
	        p_user_id             IN t101_user.c101_user_id%TYPE
	);
            
	/**********************************************************************************************
	* Description : This Procedure to load the three months sales qty
	* author : GPALANI
	*************************************************************************************************/
	PROCEDURE gm_fetch_three_months_sales_qty (
	        p_load_lock_master_id IN t2570_thb_load_lock_master.c2570_thb_load_lock_master_id%TYPE,
	        p_user_id             IN t101_user.c101_user_id%TYPE
	 );
        
	/**********************************************************************************************
    * Description : This Procedure to load the DHR QTY
    * author : GPALANI
    *************************************************************************************************/
    PROCEDURE gm_fetch_inv_qty (
	        p_load_lock_master_id IN t2570_thb_load_lock_master.c2570_thb_load_lock_master_id%TYPE,
	        p_user_id             IN t101_user.c101_user_id%TYPE
	);
            
    /**********************************************************************************************
    * Description : This Procedure to load the DHR QTY
    * author : GPALANI
    *************************************************************************************************/
    PROCEDURE gm_fetch_ptrd_qty (
            p_load_lock_master_id IN t2570_thb_load_lock_master.c2570_thb_load_lock_master_id%TYPE,
            p_user_id             IN t101_user.c101_user_id%TYPE
    ) ;

    /**********************************************************************************************
    * Description : This Procedure to load the back_order parts, qty and available inventory qty
    * author : GPALANI
    *************************************************************************************************/
    PROCEDURE gm_create_thb_load_lock_details (
            p_load_lock_master_id IN t2570_thb_load_lock_master.c2570_thb_load_lock_master_id%TYPE,
            p_base_part 		  IN t205_part_number.c205_part_number_id%TYPE,
            p_private_part 		  IN t205_part_number.c205_part_number_id%TYPE,
            p_user_id 			  IN t101_user.c101_user_id%TYPE
     ) ;

    /**********************************************************************************************
    * Description : This Procedure to Fetch Needed qty
    * author : GPALANI
    *************************************************************************************************/
    PROCEDURE gm_fetch_needed_qty (
            p_load_lock_master_id IN t2570_thb_load_lock_master.c2570_thb_load_lock_master_id%TYPE,
            p_user_id             IN t101_user.c101_user_id%TYPE
    ) ;
            
    /**********************************************************************************************
    * Description : This Procedure to update the Need qty
    * author : GPALANI
    *************************************************************************************************/
    PROCEDURE gm_update_need_qty (
            p_load_lock_detail_id IN t2570_thb_load_lock_master.c2570_thb_load_lock_master_id%TYPE,
            p_need_qty            IN t2571_thb_load_lock_details.c2571_qty_needed%TYPE,
            p_user_id             IN t101_user.c101_user_id%TYPE
    ) ;
            
	/**********************************************************************************************
	* Description : This Procedure to load the back_order parts in temp part list to fetch inventory qty
	* author : GPALANI
	*************************************************************************************************/
	PROCEDURE gm_save_my_temp_part(
	        p_load_lock_master_id IN t2570_thb_load_lock_master.c2570_thb_load_lock_master_id%TYPE
	 );
END gm_pkg_op_thb_lock_bck_ord_txn;
/