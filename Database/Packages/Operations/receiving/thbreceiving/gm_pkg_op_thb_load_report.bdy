--C:\Pmt\bit\erpGlobus\Database\Packages\Operations\receiving\gm_pkg_op_thb_load_report.bdy";

CREATE OR REPLACE
PACKAGE BODY gm_pkg_op_thb_load_report
AS

  /************************************************************
  * Description :  This procedure is used to fetch the report
  PMT-30752
  author : shiny
  ************************************************************/
  
PROCEDURE gm_fch_thb_load_report(
    p_data OUT TYPES.cursor_type )
AS
  v_date_fmt VARCHAR2(100);
  
BEGIN
   -- to get the date format from context 
 SELECT  get_compdtfmt_frm_cntx INTO v_date_fmt FROM DUAL;
 
  --Load Status -107880 Load Initiated
  OPEN p_data FOR 
		  SELECT T2560.C2560_THB_LOAD_NUM LOADNUM,
		  T2560.C2560_THB_LOAD_ID LOADID,
		  NVL(SUM(T2561. C2561_THB_QTY),0) BOXCNT,
		  T2560.C2560_THB_BOX_NUM BOXNUM,
		  TO_CHAR (T2560.C2560_THB_PROCESSED_DATE,v_date_fmt) PROCESSEDDDT,
		  T2560.C2560_THB_PROCESSED_BY PROCESSEDBY FROM T2560_THB_LOAD T2560,
		  T2561_THB_LOAD_ITEM_DETAIL T2561 WHERE T2560.C901_LOAD_STATUS='107880' AND T2560.C2560_VOID_FL IS NULL AND T2560.C2560_THB_LOAD_ID =T2561.C2560_THB_LOAD_ID group by T2560.C2560_THB_LOAD_NUM,
		  T2560.C2560_THB_LOAD_ID,
		  T2560.C2560_THB_BOX_NUM,
		  T2560.C2560_THB_PROCESSED_BY ,
		  T2560.C2560_THB_PROCESSED_DATE
		  ORDER BY T2560.C2560_THB_LOAD_NUM, T2560.C2560_THB_BOX_NUM;
  
END gm_fch_thb_load_report;
/**********************************************************************************************
*Description : Procedure to fetch the header details to THB Receiving Transaction screen
* author : ssharmila
* Ref # : PMT-30752 
*************************************************************************************************/
PROCEDURE gm_fch_thb_head_details 
(
		p_load_no	IN	T2560_THB_LOAD.C2560_THB_LOAD_NUM%TYPE,
		p_head_out	OUT	types.cursor_type,
		p_dtl_out 	OUT	types.cursor_type
)
AS
  v_date_fmt   VARCHAR2(100);
BEGIN
  -- to get the date format from context 
 SELECT  get_compdtfmt_frm_cntx INTO v_date_fmt FROM DUAL;
 
  --to fetch the header details of the transaction screen
  open P_HEAD_OUT for 
	  SELECT 
	  C2560_THB_LOAD_NUM strLoadNo,
	  C2560_THB_PROCESSED_BY STRSYNCBY,
	  TO_CHAR(C2560_THB_PROCESSED_DATE,v_date_fmt) STRSYNCDT 
	  FROM T2560_THB_LOAD 
	  WHERE C2560_THB_LOAD_NUM = p_load_no AND 
	  C2560_VOID_FL IS NULL 
	  GROUP BY C2560_THB_LOAD_NUM,C2560_THB_PROCESSED_BY,C2560_THB_PROCESSED_DATE ;
	  
  --to fetch the details for box number dropdown 
  OPEN p_dtl_out FOR 
		  SELECT C2560_THB_LOAD_ID CODEID,
		  C2560_THB_BOX_NUM CODENM FROM T2560_THB_LOAD 
		  WHERE C2560_THB_LOAD_NUM = p_load_no 
		  AND C2560_VOID_FL IS NULL ;
end GM_FCH_THB_HEAD_DETAILS;
/**********************************************************************************************
*Description : Procedure to fetch the details to THB Receiving Transaction screen
* author : ssharmila
* Ref # : PMT-30752 
*************************************************************************************************/
PROCEDURE gm_fch_thb_load_box_details (
		p_load_id	IN	T2561_THB_LOAD_ITEM_DETAIL.C2560_THB_LOAD_ID%TYPE
		,p_dtl_out 	OUT	TYPES.cursor_type
		,p_tot_out  OUT TYPES.cursor_type
		,p_load_status OUT T2560_THB_LOAD.C901_LOAD_STATUS%TYPE
)
AS
BEGIN
--to fetch the grid data section
  OPEN p_dtl_out FOR 
	  SELECT C205_PART_NUMBER_ID PARTNUM,
	  C2561_CONTROL_NUMBER CTRLNO,
	  C2561_DONOR_NUMBER DONOR ,
	  C2560_THB_LOAD_ID LOADID ,
	  C2561_THB_LOAD_ITEM_DETAIL_ID DTLID,
	  C2561_RECEIVED_STATUS_FL STATFL FROM T2561_THB_LOAD_ITEM_DETAIL 
	  WHERE C2561_VOID_FL IS NULL AND C2560_THB_LOAD_ID = p_load_id;
  
  --to fetch the Status of the BOX to check whether scan completed or not
  	BEGIN
	  	SELECT C901_LOAD_STATUS INTO p_load_status 
	  	FROM T2560_THB_LOAD 
	  	WHERE C2560_THB_LOAD_ID = p_load_id;
  	EXCEPTION WHEN NO_DATA_FOUND THEN
  		p_load_status := NULL;
  	END;
  
   --to fetch the Total allografts
  OPEN p_tot_out FOR 
	  SELECT TOT_ALLO.strTotAllo,
	  scan_allo.strScanAllo FROM
	  (SELECT COUNT(1) strTotAllo
	  FROM T2561_THB_LOAD_ITEM_DETAIL
	  WHERE C2560_THB_LOAD_ID = P_LOAD_ID
	  AND C2561_VOID_FL      IS NULL
	  ) tot_allo,
	  (SELECT COUNT(C2561_RECEIVED_STATUS_FL) strScanAllo
	  FROM T2561_THB_LOAD_ITEM_DETAIL
	  WHERE C2560_THB_LOAD_ID      = P_LOAD_ID
	  AND C2561_VOID_FL           IS NULL
	  AND C2561_RECEIVED_STATUS_FL = 'Y'
	  ) scan_allo;
  
  
END gm_fch_thb_load_box_details;
/************************************************************************************************************************
*Description : Procedure to fetch the details to THB Receiving Transaction screen from THB Receiving Dashboard screen
* author : ssharmila
* Ref # : PMT-30752 
**************************************************************************************************************************/
PROCEDURE gm_fch_thb_details_frm_dash (
		 p_load_id	IN  T2560_THB_LOAD.C2560_THB_LOAD_ID%TYPE
		,p_head_out	OUT	TYPES.cursor_type
		,p_dtl_out 	OUT	TYPES.cursor_type
		,p_tot_out   OUT TYPES.cursor_type
		,p_box_out	OUT TYPES.cursor_type
		,p_load_stat OUT T2560_THB_LOAD.C901_LOAD_STATUS%TYPE
)
AS
  v_load_num T2560_THB_LOAD.C2560_THB_LOAD_NUM%TYPE;
  v_load_stat T2560_THB_LOAD.C901_LOAD_STATUS%TYPE;
BEGIN
	  SELECT C2560_THB_LOAD_NUM
	  INTO v_load_num
	  FROM T2560_THB_LOAD
	  WHERE C2560_THB_LOAD_ID = p_load_id;
  
  --to fetch the head session data and drop down data
  gm_fch_thb_head_details(v_load_num,p_head_out,p_box_out);
  
  ---to fetch the grid data ,Total values and Status
  gm_fch_thb_load_box_details(p_load_id,p_dtl_out,p_tot_out,p_load_stat);
END gm_fch_thb_details_frm_dash;

/**********************************************************************************************
* Description : Procedure to fetch the details to THB ReceivingList Report details
* author : pvigneshwaran
*************************************************************************************************/
PROCEDURE gm_fch_thb_load_detail(
		 p_load_num	IN  T2560_THB_LOAD.C2560_THB_LOAD_NUM%TYPE,
		 p_load_id	IN  T2560_THB_LOAD.C2560_THB_LOAD_ID%TYPE,
		 p_out	OUT TYPES.cursor_type
)
AS
v_date_fmt VARCHAR2(100);
BEGIN
	-- to get the date format from context 
 	SELECT  get_compdtfmt_frm_cntx INTO V_DATE_FMT FROM DUAL;
	IF  p_load_num IS NOT NULL THEN
	OPEN p_out FOR 
		SELECT 
				T2560.C2560_THB_LOAD_NUM LOADNUM, 
				T2560.C2560_THB_BOX_NUM  BOXNUM, 
				T2561.C205_PART_NUMBER_ID PARTNUM,
				T2561.C2561_CONTROL_NUMBER CNTRL_NUM, 
				T2561.C2561_DONOR_NUMBER DONOR_NUM, 
				get_user_name(T2561.C2561_RECEIVED_BY) RECEIVED_BY, 
				TO_CHAR(T2561.C2561_RECEIVED_DATE,V_DATE_FMT) RECEIVED_DATE,
				T2561.C2561_THB_QTY QTY,
				T2561.C2561_RECEIVED_STATUS_FL RSTATUS,
				T2560.C2560_THB_PROCESSED_BY PROCESSEDBY
		FROM T2560_THB_LOAD T2560, T2561_THB_LOAD_ITEM_DETAIL T2561
		WHERE T2560.C2560_THB_LOAD_NUM = p_load_num
		AND T2560.C2560_VOID_FL    IS NULL
		AND T2561.C2561_VOID_FL IS NULL
		AND T2560.C2560_THB_LOAD_ID = T2561.C2560_THB_LOAD_ID
		ORDER BY T2560.C2560_THB_LOAD_NUM, T2560.C2560_THB_BOX_NUM, T2561.C205_PART_NUMBER_ID;
	ELSE
	OPEN p_out FOR 
			SELECT  T2560.C2560_THB_LOAD_NUM LOADNUM,
  					T2560.C2560_THB_BOX_NUM BOXNUM,
  					T2561.C205_PART_NUMBER_ID PARTNUM,
  					T2561.C2561_CONTROL_NUMBER CNTRL_NUM,
  					T2561.C2561_DONOR_NUMBER DONOR_NUM,
  					get_user_name(T2561.C2561_RECEIVED_BY) RECEIVED_BY,
  					TO_CHAR(T2561.C2561_RECEIVED_DATE,V_DATE_FMT) RECEIVED_DATE,
  					T2561.C2561_THB_QTY QTY,
  					T2561.C2561_RECEIVED_STATUS_FL RSTATUS,
  					T2560.C2560_THB_PROCESSED_BY PROCESSEDBY
			FROM T2560_THB_LOAD T2560,
  				 T2561_THB_LOAD_ITEM_DETAIL T2561
			WHERE T2560.C2560_THB_LOAD_ID = p_load_id
			AND T2560.C2560_VOID_FL     IS NULL
			AND T2561.C2561_VOID_FL     IS NULL
			AND T2560.C2560_THB_LOAD_ID  = T2561.C2560_THB_LOAD_ID
			ORDER BY T2560.C2560_THB_LOAD_NUM,T2560.C2560_THB_BOX_NUM,T2561.C205_PART_NUMBER_ID;
	END IF;
END gm_fch_thb_load_detail;

END gm_pkg_op_thb_load_report;
/
