CREATE OR REPLACE
PACKAGE BODY gm_pkg_op_thb_analysis_rpt
AS
	/************************************************************
	 * Description :  This procedure is used to fetch the details for the Pending Analysis Dashboard
	 * Author      :  ssharmila
	************************************************************/
	PROCEDURE gm_fch_pend_analysis_dash(
	    p_out_analysis_dtls OUT CLOB 
	)
	AS
	  v_date_fmt t906_rules.c906_rule_value%TYPE;
	  
	BEGIN
	
      BEGIN
	  -- to get the date format
	  SELECT get_compdtfmt_frm_cntx
	  INTO v_date_fmt
	  FROM DUAL;
	  
	  SELECT JSON_ARRAYAGG (JSON_OBJECT 
	  			('LOADID' VALUE '', 'NUMOFDONORS' VALUE NVL (COUNT (DISTINCT ( t2561.c2561_donor_number)), 0)
	  			, 'LOADNUM' VALUE t2560.c2560_thb_load_num
	  			, 'RDATE' VALUE TO_CHAR ( t2560.c2560_thb_processed_date, v_date_fmt)
	  			, 'type' value get_code_name (c901_load_type)
	  			, 'TOTALPARTQTY' VALUE NVL (COUNT (t2561.c2561_thb_qty), 0))
	  ORDER BY t2560.c2560_thb_load_num ASC RETURNING CLOB)
	  INTO p_out_analysis_dtls
	  FROM t2560_thb_load t2560,
	    t2561_thb_load_item_detail t2561
	  WHERE t2560.c2570_thb_load_lock_master_id IS NULL
	    -- AND t2560.c901_load_status             = '107880'
	  AND t2560.c2560_void_fl    IS NULL
	  AND t2561.c2561_void_fl    IS NULL
	  AND t2560.c2560_thb_load_id = t2561.c2560_thb_load_id
	  GROUP BY t2560.c2560_thb_load_num,
	    t2560.c2560_thb_processed_date,
	    c901_load_type
	  ORDER BY t2560.c2560_thb_load_num ASC;
	 
      EXCEPTION WHEN OTHERS
		 THEN
		 p_out_analysis_dtls:=NULL;
	  END;	 
	END gm_fch_pend_analysis_dash;
	/**************************************************************************************
	* Description :  This procedure is used to Load the THB Analysis List Report details
	* Author      :  gomathi
	***************************************************************************************/
	PROCEDURE gm_fch_analysis_list_rpt (
        p_analysis_id  			IN t2570_thb_load_lock_master.c2570_thb_load_lock_master_id%TYPE,
        p_load_id      			IN t2560_thb_load.c2560_thb_load_num%TYPE,
        p_status       			IN t2560_thb_load.c901_load_status%TYPE,
        p_procstart_dt 			IN t2570_thb_load_lock_master.c2570_thb_process_start_date%TYPE,
        p_procend_dt   			IN t2570_thb_load_lock_master.c2570_thb_process_end_date%TYPE,
        p_out_analysis_list_dtls OUT CLOB)
        
	AS
	    v_date_fmt 		T906_RULES.C906_RULE_VALUE%TYPE;
	    
	BEGIN
	    -- to get the date format
	     SELECT get_compdtfmt_frm_cntx
	       INTO v_date_fmt
	       FROM DUAL;
	   
	  	 BEGIN
	   
	    --Load Status -107881 THB Load Processed
				
	SELECT JSON_ARRAYAGG (JSON_OBJECT ('ANALYSISID' VALUE ANALYSISID, 'PFMDATE' VALUE PFMDATE, 'PTODATE' VALUE PTODATE,
    'LOADSTATUS' VALUE LOADSTATUS, 'SQTY' VALUE SQTY, 'NUMOFDONORS' VALUE NUMOFDONORS, 'TOTALPARTQTY'
    VALUE TOTALPARTQTY, 'LOADID' VALUE LOADID)
	ORDER BY analysisid ASC returning CLOB)
   INTO p_out_analysis_list_dtls
   FROM
    (
         SELECT analysisid, loadid, pfmdate
          , ptodate, loadstatus, SUM (numofdonors) numofdonors
          , SUM (totalpartqty) totalpartqty, SUM (sqty) SQTY
           FROM
            (
                 SELECT tot_qty.lock_master_id analysisid, listagg (tot_qty.load_num, ',') within GROUP (
               ORDER BY tot_qty.load_num) over (partition BY tot_qty.lock_master_id) AS loadid, tot_qty.no_of_don
                    numofdonors, tot_qty.thb_qty totalpartqty, scheudled_qty.scheduled_qty sqty
                  , tot_qty.pfmdate pfmdate, tot_qty.ptodate ptodate, get_code_name(tot_qty.status) loadstatus
                   FROM
                    (
                         SELECT COUNT (DISTINCT (t2561.c2561_donor_number)) no_of_don, NVL (SUM (t2561.c2561_thb_qty),
                            0) thb_qty, t2560.c2560_thb_load_num load_num, t2560.c2570_thb_load_lock_master_id
                            lock_master_id, TO_CHAR (t2570.c2570_thb_process_start_date, v_date_fmt) pfmdate, TO_CHAR (
                            t2570.c2570_thb_process_end_date, v_date_fmt) ptodate, t2570.c901_status status
                           FROM t2560_thb_load t2560, t2561_thb_load_item_detail t2561, t2570_thb_load_lock_master
                            t2570
                          WHERE t2570.c2570_thb_load_lock_master_id = t2560.c2570_thb_load_lock_master_id
                            AND t2560.c2570_thb_load_lock_master_id = NVL (p_analysis_id,
                            t2560.c2570_thb_load_lock_master_id)
                            AND TRUNC (t2570.c2570_thb_process_start_date) >= NVL (p_procstart_dt,
                            t2570.c2570_thb_process_start_date)
                            AND TRUNC (t2570.c2570_thb_process_end_date) <= NVL (p_procend_dt,
                            t2570.c2570_thb_process_end_date)
                            AND t2570.c901_status        = NVL (p_status, t2570.c901_status)
                            AND t2560.c2560_thb_load_num = NVL (p_load_id, t2560.c2560_thb_load_num)
                            AND t2560.c2560_void_fl     IS NULL
                            AND t2561.c2561_void_fl     IS NULL
                            AND t2570.c2570_void_fl     IS NULL
                            AND t2560.c2560_thb_load_id  = t2561.c2560_thb_load_id
                       GROUP BY t2560.c2560_thb_load_num, t2560.c2570_thb_load_lock_master_id,
                            t2570.c2570_thb_process_start_date, t2570.c2570_thb_process_end_date,
                            t2570.c901_status
                    )
                    tot_qty, (
                         SELECT NVL (SUM (t2571.c2571_qty_allocated), 0) scheduled_qty,
                            t2571.c2570_thb_load_lock_master_id master_id
                           FROM t2571_thb_load_lock_details t2571
                          WHERE c2570_thb_load_lock_master_id = NVL (p_analysis_id, c2570_thb_load_lock_master_id)
                            AND t2571.c2571_void_fl          IS NULL
                       GROUP BY t2571.c2570_thb_load_lock_master_id
                    )
                    scheudled_qty
                  WHERE scheudled_qty.master_id = tot_qty.lock_master_id
            )
       GROUP BY ANALYSISID, LOADID, PFMDATE
          , PTODATE, LOADSTATUS
    ) ; 
  
  
	   
	 EXCEPTION WHEN OTHERS THEN
	  p_out_analysis_list_dtls:=NULL;
	  
	 END;
	 
	END gm_fch_analysis_list_rpt;
	/**************************************************************************************
	* Description :  This procedure is used to Load the data for the THB Analysis details Screen
	* Author      :  gomathi
	***************************************************************************************/
	PROCEDURE gm_fch_analysis_details(
	      p_analysis_id 		IN t2570_thb_load_lock_master.c2570_thb_load_lock_master_id%TYPE,
	      p_out_analysis_dtls OUT CLOB )
	AS
	BEGIN
	
	  SELECT RTRIM (regexp_replace (xmlagg (xmlelement (e, p_out_analysis_dtls
	    || ',')) .extract ('//text()') .getClobVal (), '\&'
	    ||'quot;', '"'), ',')
	  INTO P_OUT_ANALYSIS_DTLS
	  FROM
	    (SELECT JSON_OBJECT ('ANALYSISID' VALUE t2571.c2570_thb_load_lock_master_id
	    	, 'BASE_PART' VALUE t2571.c205_base_part_number, 'PRIVATE_PART' VALUE c205_pvt_lbl_part_number_id
	    	, 'PART_DESCRIPTION' VALUE GET_PARTNUM_DESC (c205_pvt_lbl_part_number_id)
	    	, 'BO_QTY' VALUE t2571.c2571_back_ord_qty, 'PEND_2_WEEKS' VALUE c2571_pend_in_two_weeks
	    	, 'PEND_AFTER_2_WEEKS' VALUE c2571_pend_after_two_weeks
	    	, 'PAST_3_MONTH_SALE' VALUE t2571.c2571_avg_three_months_sale
	    	, 'SHELF_QTY' VALUE t2571.c2571_shelf_qty, 'DHR_QTY' VALUE t2571.c2571_dhr_qty
	    	, 'REPACK_QTY' VALUE c2571_repack_qty, 'PTRD_QTY' VALUE c2571_redesignated_qty
	    	, 'NEED_QTY' VALUE c2571_qty_needed, 'RAD_RUN_QTY' VALUE c2571_rad_run_qty
	    	, 'ALLOCATED_QTY' VALUE c2571_qty_allocated) P_OUT_ANALYSIS_DTLS
	    FROM t2571_thb_load_lock_details t2571
	    WHERE t2571.c2570_thb_load_lock_master_id = p_analysis_id
	    AND t2571.c2571_void_fl                  IS NULL
	    ORDER BY c205_base_part_number,c205_pvt_lbl_part_number_id ASC) ;
		
		EXCEPTION WHEN OTHERS THEN
		  P_OUT_ANALYSIS_DTLS:=NULL;
	END gm_fch_analysis_details;
	/**************************************************************************************************
	* Description :  This procedure is used to update the Status to indicate the Lock and Generate process is in progress
	* Author      :  ssharmila
	**************************************************************************************************/
	PROCEDURE gm_upd_lock_status(
	    p_load_num 			IN CLOB,
	    p_user_id  			IN t101_user.c101_user_id%TYPE ,
	    p_stat 				IN t2560_thb_load.c901_load_status%TYPE
    )
	AS
	    v_load_string CLOB := P_LOAD_NUM;
	    v_load_number t2560_thb_load.c2560_thb_load_num%TYPE ;
	    
	BEGIN
	    WHILE INSTR (v_load_string, '|') <> 0
	    LOOP
	        v_load_number := TO_NUMBER(SUBSTR (v_load_string, 1, INSTR (v_load_string, '|') - 1)) ;
	        v_load_string := SUBSTR (v_load_string, INSTR (v_load_string, '|')    + 1) ;
			
	
	        
	        UPDATE t2560_thb_load
	        SET c901_load_status = p_stat
	        	, c2560_last_updated_by = p_user_id
	        	, c2560_last_updated_date = CURRENT_DATE
	        WHERE c2560_thb_load_num IN (v_load_number)
	           AND c2560_void_fl      IS NULL ;
		  
	    END LOOP;
	

	    
	END gm_upd_lock_status;
	
		
	/************************************************************
	* Description :  This procedure is used to fetch the Donor details Report
	* Author      :  gpalani
	************************************************************/
	PROCEDURE gm_fch_donor_load_details(
	    p_load_num  		IN t2560_thb_load.c2560_thb_load_num%TYPE,
	    p_donor_num 		IN t2561_thb_load_item_detail.c2561_donor_number%TYPE,
	    p_part_num  		IN t2561_thb_load_item_detail.c205_part_number_id%TYPE,
	    p_proc_dt   		IN t2560_thb_load.c2560_thb_processed_date%TYPE,
	    p_out_donor_dtls 	OUT CLOB 
	)
	AS
	    v_date_fmt t906_rules.c906_rule_value%TYPE;
	    
	BEGIN
	     SELECT get_compdtfmt_frm_cntx INTO v_date_fmt FROM DUAL;
	          
	     SELECT JSON_ARRAYAGG (JSON_OBJECT ('Load_Num' VALUE t2560.c2560_thb_load_num, 'Allograft_Num' VALUE
	        t2561.c2561_control_number, 'Processed_Date' VALUE TO_CHAR (t2560.c2560_thb_processed_date, v_date_fmt),
	        'PO_Num' VALUE t2561.c401_purchase_ord_id, 'Donor_Num' VALUE t2561.c2561_donor_number, 'Donor_Purpose' VALUE
	        GET_CODE_NAME (t2561.c901_donor_type), 'Donor_International_Flag' VALUE GET_CODE_NAME (
	        t2561.c901_donor_international_flag), 'Part_Num' VALUE t2561.c205_part_number_id, 'Measurement' VALUE
	        t2561.c2561_material_spec, 'Re_Designated_As' VALUE t2561.C205_REDSG_PART_NUMBER_ID,'Move_To_Inventory' VALUE
	         get_code_name(t2561.c901_ware_house_type))
	   	ORDER BY t2560.c2560_thb_load_num, t2561.c2561_control_number, t2561.c2561_donor_number ASC RETURNING CLOB)
	       INTO p_out_donor_dtls
	       FROM t2560_thb_load t2560, t2561_thb_load_item_detail t2561
	      WHERE t2560.c2560_thb_load_id                = t2561.c2560_thb_load_id
	        AND t2560.c2560_thb_load_num               = NVL (p_load_num, t2560.c2560_thb_load_num)
	        AND t2561.c2561_donor_number               = NVL (p_donor_num, t2561.c2561_donor_number )
	        AND t2561.c205_part_number_id              = NVL (p_part_num, t2561.c205_part_number_id )
	        AND TRUNC (t2560.c2560_thb_processed_date) = NVL (p_proc_dt, t2560.c2560_thb_processed_date)
	        AND t2560.c2560_void_fl IS NULL
	        AND t2561.c2561_void_fl IS NULL
	   ORDER BY t2560.c2560_thb_load_num, t2561.c2561_control_number, t2561.c2561_donor_number ASC;
	  	
		EXCEPTION WHEN OTHERS THEN
		  p_out_donor_dtls:=NULL;
			
	END gm_fch_donor_load_details;
	
	/************************************************************
	* Description :  This procedure is used to fetch the Load details data
	* Author      :  gpalani
	************************************************************/
	PROCEDURE gm_fch_load_stage_details (
	  	p_load_num 			IN t2560_thb_load.c2560_thb_load_num%TYPE,
	  	p_donor_num 		IN t2561_thb_load_item_detail.c2561_donor_number%TYPE,
		p_stgfrm_dat		IN t2573_thb_donor_lock.c2540_stage_date%TYPE,
	    p_stgto_dat  		IN t2573_thb_donor_lock.c2540_stage_date%TYPE,
	  	p_out_load_dtls OUT CLOB)
	AS
	  	v_date_fmt 	t906_rules.c906_rule_value%TYPE;

	BEGIN
	  SELECT get_compdtfmt_frm_cntx INTO v_date_fmt FROM DUAL;
	  
		SELECT JSON_ARRAYAGG (JSON_OBJECT ('Load_Num' value loadnum ,'Donor_Num' value donornum ,'Age' value age ,'Sex' value sex 
			,'No_of_Units' value partcount ,'Qty_Allocated' value qtyalloc ,'Qty_Not_Allocated' value NVL(partcount,0)-NVL(qtyalloc,0) 
			, 'Stage_Date' value TO_CHAR(stgdat,v_date_fmt))
		  ORDER BY loadnum,to_number(donornum) ASC RETURNING CLOB)
		  INTO p_out_load_dtls
		FROM
		  (SELECT t2560.c2560_thb_load_num loadnum ,
		    t2561.c2561_donor_number donornum ,
		    t2561.c2561_donor_age age,
		    t2561.c2561_donor_sex sex ,
		    NVL (COUNT (DISTINCT (t2561.c2561_control_number)), 0) partcount ,
		    t2571.c2571_qty_allocated qtyalloc ,
		    t2573.c2540_stage_date stgdat
		  FROM t2560_thb_load t2560,
		    t2561_thb_load_item_detail t2561,
		    t2571_thb_load_lock_details t2571 ,
		    t2573_thb_donor_lock t2573
		  WHERE t2560.c2560_thb_load_id           = t2561.c2560_thb_load_id
		  AND t2560.c2570_thb_load_lock_master_id = t2571.c2570_thb_load_lock_master_id
		  AND t2571.c2570_thb_load_lock_master_id = t2573.c2570_thb_load_lock_master_id
		  AND t2560.c2560_thb_load_num            = NVL(p_load_num,t2560.c2560_thb_load_num)
		  AND t2561.c2561_donor_number            = NVL(p_donor_num,t2561.c2561_donor_number)
		  AND TRUNC(t2573.c2540_stage_date)      >= NVL(p_stgfrm_dat,t2573.c2540_stage_date)
		  AND TRUNC(t2573.c2540_stage_date)      <= NVL(p_stgto_dat,t2573.c2540_stage_date)
		  AND t2561.c205_part_number_id           = t2571.c205_pvt_lbl_part_number_id
		  AND t2560.c2560_void_fl                IS NULL
		  AND t2561.c2561_void_fl                IS NULL
		  AND t2571.c2571_void_fl                IS NULL
		  GROUP BY t2561.c2561_donor_number,
		    t2560.c2560_thb_load_num,
		    t2561.c2561_donor_age ,
		    t2561.c2561_donor_sex,
		    t2571.c2571_qty_allocated,
		    t2573.c2540_stage_date
		  ORDER BY t2560.c2560_thb_load_num,to_number(t2561.c2561_donor_number) ASC );
		  
		EXCEPTION WHEN OTHERS THEN
		  p_out_load_dtls:=NULL;
		  
	END gm_fch_load_stage_details;
    /*******************************************************************************
    * Description :  This procedure is used to fetch the Donor Load Details Report
    * Author      :  gpalani
    *********************************************************************************/
  	PROCEDURE gm_fch_load_details (
	  	p_load_num 			IN t2560_thb_load.c2560_thb_load_num%TYPE,
	  	p_donor_num 		IN t2561_thb_load_item_detail.c2561_donor_number%TYPE,
	  	p_out_load_dtls OUT CLOB)	
	AS
	BEGIN
		SELECT JSON_ARRAYAGG (JSON_OBJECT ('Load_Num' VALUE loadnum ,'Donor_Num' VALUE donornum ,'Age' VALUE age
		      ,'Sex' VALUE sex,'No_of_Units' VALUE partcount,'Qty_Allocated' VALUE qtyalloc 
		      ,'Qty_Not_Allocated' VALUE NVL(PARTCOUNT,0)-NVL(QTYALLOC,0) )
		ORDER BY loadnum,donornum asc returning clob)
		INTO p_out_load_dtls
		FROM
		  (SELECT t2560.c2560_thb_load_num loadnum ,
		    t2561.c2561_donor_number donornum ,
		    t2561.c2561_donor_age age,
		    t2561.c2561_donor_sex sex ,
		    NVL (COUNT (DISTINCT (t2561.c2561_control_number)), 0) partcount ,
		    t2571.c2571_qty_allocated qtyalloc ,
		    t2561.c205_part_number_id pnum
		  FROM t2560_thb_load t2560,
		    t2561_thb_load_item_detail t2561,
		    t2571_thb_load_lock_details t2571
		  WHERE t2560.c2560_thb_load_id           = t2561.c2560_thb_load_id
		  AND t2560.c2560_thb_load_num            = NVL(p_load_num,t2560.c2560_thb_load_num)
		  AND t2561.c2561_donor_number            = NVL(p_donor_num,t2561.c2561_donor_number)
		  AND t2560.c2570_thb_load_lock_master_id = t2571.c2570_thb_load_lock_master_id (+)
		  AND t2561.c205_part_number_id           = t2571.c205_pvt_lbl_part_number_id (+)
		  AND t2560.c2560_void_fl                IS NULL
		  AND t2561.c2561_void_fl                IS NULL
		  AND t2571.c2571_void_fl                IS NULL
		  GROUP BY t2561.c2561_donor_number,
		    t2560.c2560_thb_load_num,
		    t2561.c2561_donor_age ,
		    t2561.c2561_donor_sex,
		    t2571.c2571_qty_allocated,
		    t2561.c205_part_number_id
		  ORDER BY c2561_donor_number ASC);
		  
		 EXCEPTION WHEN OTHERS 
		 THEN
		  p_out_load_dtls:=NULL;
	END gm_fch_load_details;
	/************************************************************
	* Description :  This procedure is used to fetch the Back order details
	* Author      :  gpalani
	************************************************************/
	PROCEDURE gm_fch_load_back_order_dtls(
		p_private_part    IN t2572_thb_txn_lock.c205_part_number_id%TYPE,
		p_lock_id         IN t2572_thb_txn_lock.c2570_thb_load_lock_master_id%TYPE,
		p_back_order_type IN t901_code_lookup.c901_code_id%TYPE, -- PTRD, Back_Order
		p_out_load_dtls OUT CLOB )		
	AS
	  v_date_fmt t906_rules.c906_rule_value%TYPE;
	BEGIN
      --Date format
	  SELECT get_compdtfmt_frm_cntx INTO v_date_fmt FROM DUAL;
	  
	  SELECT RTRIM (regexp_replace (xmlagg (xmlelement (e, p_out_load_dtls
	    || ',')) .extract ('//text()') .getClobVal (), '\&'
	   ||'quot;', '"'), ',')
	   INTO p_out_load_dtls
	 FROM   
	  (SELECT JSON_OBJECT ('Base_Part' VALUE t2571.c205_base_part_number
	  	, 'Private_Part' VALUE t2571.c205_pvt_lbl_part_number_id
	  	, 'Part_Description' VALUE GET_PARTNUM_DESC (c205_pvt_lbl_part_number_id)
	  	, 'Account_Name' VALUE GET_ACCOUNT_NAME (t2572.c704_account_id)
	  	, 'Required_Date' VALUE TO_CHAR (t2572.c2572_required_date,v_date_fmt), 'QTY' VALUE t2572.c2572_qty
	  	, 'created_date' VALUE TO_CHAR (t2572.c2572_created_date,v_date_fmt), 'created_by' VALUE GET_USER_NAME (t2572.c2572_created_by)
	  	, 'Ref_Id' VALUE t2572.c2572_ref_id, 'Ref_type' VALUE GET_CODE_NAME (t2572.c901_ref_type))
	     p_out_load_dtls
	  FROM t2571_thb_load_lock_details t2571,
	   t2572_thb_txn_lock t2572
	  WHERE t2572.c205_part_number_id         = p_private_part
	  AND t2572.c205_part_number_id           = t2571.c205_pvt_lbl_part_number_id
	  AND t2571.c2570_thb_load_lock_master_id = t2572.c2570_thb_load_lock_master_id
	  AND t2572.c2570_thb_load_lock_master_id = p_lock_id
	  AND t2572.c901_ref_type                 = p_back_order_type
	  AND t2572.c2572_void_fL                IS NULL
	  AND t2571.c2571_void_fl                IS NULL
	  ORDER BY decode(p_back_order_type,103932,t2572.c2572_created_date,t2572.c2572_required_date) ASC); 
	  -- based on requirement Sorting should be PTRD - Created date,BO - Required date
    	
    EXCEPTION WHEN OTHERS
		 THEN
		 p_out_load_dtls:=NULL;

	END gm_fch_load_back_order_dtls;
	/************************************************************
	* Description :  This procedure is used to update the analysis lock date
	* Author      :  gpalani
	************************************************************/
	PROCEDURE gm_update_lock_date (
        p_analysis_id        IN t2570_thb_load_lock_master.c2570_thb_load_lock_master_id%TYPE,
        p_process_start_date IN t2570_thb_load_lock_master.c2570_thb_process_start_date%TYPE,
        p_process_end_date   IN t2570_thb_load_lock_master.c2570_thb_process_end_date%TYPE,
        p_user_id            IN t101_user.c101_user_id%TYPE
	)
	AS
	
		v_analysis_id	t2570_thb_load_lock_master.c2570_thb_load_lock_master_id%TYPE;
	
	  BEGIN
	  		
			 SELECT c2570_thb_load_lock_master_id
			  INTO v_analysis_id 
			  FROM t2570_thb_load_lock_master t2570 
			 WHERE c2570_thb_load_lock_master_id = p_analysis_id
			 AND c2570_void_fl IS NULL
			FOR UPDATE;		 
  			
			UPDATE t2570_thb_load_lock_master
			SET c2570_thb_process_start_date = p_process_start_date
				, c2570_thb_process_end_date = p_process_end_date
				, c2570_last_updated_date    = CURRENT_DATE
				, c2570_last_updated_by 	 = p_user_id
			  WHERE c2570_thb_load_lock_master_id = p_analysis_id
				AND c2570_void_fl                IS NULL;
			
	    EXCEPTION WHEN OTHERS
		 THEN
		 v_analysis_id:=NULL;
		 
	 END gm_update_lock_date;
	 
	/***************************************************************************
	* Description :  This procedure is used to fetch the RadRunQty details
	* Author      :  shiny
	****************************************************************************/
	PROCEDURE gm_fch_rad_run_details(
	      p_analysis_id IN t2571_thb_load_lock_details.c2570_thb_load_lock_master_id %TYPE,
	      p_basepart_num    IN t2571_thb_load_lock_details.c205_base_part_number%TYPE,
	      p_privpart_num    IN t2571_thb_load_lock_details.c205_pvt_lbl_part_number_id%TYPE,
	      p_out_donor_load_details OUT CLOB)
	      
	AS
	  v_date_fmt t906_rules.c906_rule_value%TYPE;
	  
	BEGIN
	  --Date format
	  SELECT get_compdtfmt_frm_cntx 
	  INTO  V_DATE_FMT FROM DUAL;
	  
	 
	  --Load Status -107880 THB Load Initiated
	  SELECT JSON_ARRAYAGG (JSON_OBJECT ( 'LOAD_NUM' VALUE t2560.c2560_thb_load_num
	  	, 'ALLOGARFT_NUM' VALUE t2561.c2561_control_number
	  	, 'PROCESSED_DATE' VALUE TO_CHAR(t2560.c2560_thb_processed_date,v_date_fmt)
	  	, 'DONOR_NUM' VALUE t2561.c2561_donor_number, 'DONOR_PURPOSE' VALUE GET_CODE_NAME (t2561.c901_donor_type)
	  	, 'DONOR_INTERNATIONAL_FLAG' VALUE GET_CODE_NAME (t2561.c901_donor_international_flag)
	  	, 'BASEPART' VALUE t2571.c205_base_part_number , 'MEASUREMENT' VALUE t2561.c2561_material_spec
	  	, 'REDESIGNATEDAS' VALUE t2561.c205_redsg_part_number_id, 'MOVETO' VALUE t2561.c901_ware_house_type
	  	, 'ITEM_DETAIL_ID' VALUE t2561.c2561_thb_load_item_detail_id )
	  ORDER BY t2560.c2560_thb_load_num ,t2561.c2561_control_number ASC RETURNING CLOB)
	  INTO p_out_donor_load_details
	  FROM t2560_thb_load t2560,
	    t2561_thb_load_item_detail t2561,
	    t2571_thb_load_lock_details t2571
	  WHERE t2571.c205_base_part_number       = p_basepart_num
	  AND t2571.c205_pvt_lbl_part_number_id   = p_privpart_num
	  AND t2560.c2570_thb_load_lock_master_id =	p_analysis_id
	  AND t2561.c205_part_number_id           =	t2571.c205_base_part_number
	  AND t2560.c2570_thb_load_lock_master_id =	t2571.c2570_thb_load_lock_master_id
	  AND t2571.c205_base_part_number = t2571.c205_pvt_lbl_part_number_id
	  AND t2560.c2560_thb_load_id             = t2561.c2560_thb_load_id
	  AND t2560.c2560_void_fl                IS NULL
	  AND t2571.c2571_void_fl                IS NULL
	  AND t2561.c2561_void_fl                IS NULL
	  ORDER BY t2560.c2560_thb_load_num ,t2561.c2561_control_number  ASC;
	
		EXCEPTION WHEN OTHERS
		 THEN
		 p_out_donor_load_details:=NULL;
	  	
	END gm_fch_rad_run_details;
	/**************************************************************************************
	* Description :  This procedure is used to update the status based on the analysis ID
	* Author      :  Shiny
	****************************************************************************************/
	PROCEDURE gm_sav_analysis_status(
	      p_analysis_id IN t2570_thb_load_lock_master.c2570_thb_load_lock_master_id%TYPE,
	      p_user_id     in t101_user.c101_user_id%TYPE)
	AS
	
	v_analysis_id t2570_thb_load_lock_master.c2570_thb_load_lock_master_id%TYPE;
	
	BEGIN
	
		SELECT t2570.c2570_thb_load_lock_master_id
		  INTO v_analysis_id
		FROM t2570_thb_load_lock_master t2570
		WHERE t2570.c2570_thb_load_lock_master_id= p_analysis_id
		AND t2570.c2570_void_fl IS NULL
		FOR UPDATE;
		
		  --108042 Status as Completed
		  UPDATE t2570_thb_load_lock_master
		  SET c901_status            = '108042',
			c2570_last_updated_by    = p_user_id,
			c2570_last_updated_date  = CURRENT_DATE
		  WHERE c2570_thb_load_lock_master_id= p_analysis_id
		  AND c2570_void_fl IS NULL;
	
    EXCEPTION WHEN OTHERS
     THEN
     v_analysis_id:=NULL;
	
	END gm_sav_analysis_status;
	/**************************************************************************************
	* Description :  This procedure is used to update the status as In Progress  based on the analysis ID
	* Author      :  TamizhThangam
	****************************************************************************************/
	PROCEDURE gm_upd_analysis_status(
	    p_analysis_id IN t2570_thb_load_lock_master.c2570_thb_load_lock_master_id%TYPE,
	    p_user_id     IN t101_user.c101_user_id%TYPE)
	AS
	
	v_analysis_id t2570_thb_load_lock_master.c2570_thb_load_lock_master_id%TYPE;
	
	BEGIN
		SELECT t2570.c2570_thb_load_lock_master_id
		  INTO v_analysis_id
		FROM t2570_thb_load_lock_master t2570
		WHERE t2570.c2570_thb_load_lock_master_id= p_analysis_id
		AND t2570.c2570_void_fl IS NULL
		FOR UPDATE;
		
		  --108041 Status as In Progress
		  UPDATE t2570_thb_load_lock_master
		  SET c901_status              = '108041',
			c2570_last_updated_by      = p_user_id,
			c2570_last_updated_date    = CURRENT_DATE
		  WHERE c2570_thb_load_lock_master_id = p_analysis_id
		  AND c2570_void_fl IS NULL;

	EXCEPTION WHEN OTHERS
      THEN
       v_analysis_id:=NULL;
   
	END gm_upd_analysis_status;
	
	
	/**************************************************************************************
	* Description :  This procedure is used to void the analysis ID
	* Author      :  gpalani
	* PMT-54200
	****************************************************************************************/
	PROCEDURE gm_rollback_lock_genereate(
	    p_analysis_id IN t2570_thb_load_lock_master.c2570_thb_load_lock_master_id%TYPE,
	    p_user_id     IN t101_user.c101_user_id%TYPE)
	AS
	
	v_analysis_id t2570_thb_load_lock_master.c2570_thb_load_lock_master_id%TYPE;
	v_count NUMBER;
	
BEGIN

    BEGIN
	
-- Check if any RS already initiated from the Stage scheduling. If the RS is already created using the Stage Scheduling and then raise a exception.
	
	SELECT count (1) INTO v_count
	FROM t2573_thb_donor_lock
	WHERE c2570_thb_load_lock_master_id=p_analysis_id
	AND c2540_rs_created_fl IS NOT NULL
	AND c2573_void_fl IS NULL;
	
	IF(v_count>0) THEN
	
    gm_raise_application_error ('-20999', '449',p_analysis_id) ;
    
	END IF; 
	
	-- Fetching the Master lock before updating the records to avoid conflict in the transaction.
		
		BEGIN 
			SELECT  t2570.c2570_thb_load_lock_master_id
			  INTO v_analysis_id
			FROM t2570_thb_load_lock_master t2570
			WHERE t2570.c2570_thb_load_lock_master_id= p_analysis_id
			AND t2570.c2570_void_fl IS NULL
			FOR UPDATE;
		
		EXCEPTION WHEN OTHERS THEN
		v_analysis_id:='';
		RETURN;
		
		END;
				
		/* Resetting the Re-designated part number and Inventory type from the Analysis id.
		    Even though this is related THB Rad run master transaction, the values in the below columns 
		    where used to determine the total number of control number scheduled for each THB Load ID */
		 
 			UPDATE t2561_thb_load_item_detail
				SET c205_redsg_part_number_id = '', c901_ware_house_type = '', c2561_last_updated_by=p_user_id, c2561_last_updated_date=CURRENT_DATE
  			WHERE c2560_thb_load_id    IN
    		(
         		SELECT t2560.c2560_thb_load_id
           			FROM t2560_thb_load t2560
          		WHERE t2560.c2570_thb_load_lock_master_id = p_analysis_id
          		AND t2560.c2560_void_fl IS NULL 
    ) ;  		 
		 
		
-- Remove the Lock and generate id from the Rad run Load number						  
		UPDATE t2560_thb_load
		  SET c2570_thb_load_lock_master_id='', C2560_LAST_UPDATED_BY=p_user_id
		  ,C2560_LAST_UPDATED_DATE=CURRENT_DATE
		WHERE c2570_thb_load_lock_master_id=p_analysis_id
		AND c2560_void_fl IS NULL;
	
	-- Below table has the TXN locked (Ack order, PTRD) during the Lock and generate
			
	    UPDATE T2572_THB_TXN_LOCK
		 SET C2570_THB_LOAD_LOCK_MASTER_ID='',
		C2572_VOID_FL='Y', 
		C2572_LAST_UPDATED_BY=p_user_id, 
		C2572_LAST_UPDATED_DATE=CURRENT_DATE
		WHERE C2570_THB_LOAD_LOCK_MASTER_ID=p_analysis_id
		AND C2572_VOID_FL IS NULL;
	
	-- Below table has the mapping with Lock master ID and Stage scheduling transaction
	
		  UPDATE T2573_THB_DONOR_LOCK
			SET C2573_VOID_FL='Y',
		  C2573_LAST_UPDATED_BY=p_user_id,
		  C2573_LAST_UPDATED_DATE=CURRENT_DATE
		  WHERE c2570_thb_load_lock_master_id = p_analysis_id
		  AND c2573_void_fl IS NULL;
	
	-- Below table has the mapping with Lock master ID and Lock Master details
		 UPDATE t2571_thb_load_lock_details
		    SET c2571_void_fl='Y',
		  c2571_last_updated_by=p_user_id,
		  c2571_last_updated_date=CURRENT_DATE
		  WHERE c2570_thb_load_lock_master_id=p_analysis_id
		  AND c2571_void_fl IS NULL;
		
			
		  --108122 update Status as void
		  UPDATE t2570_thb_load_lock_master
		  SET c901_status              = '108122', -- 108122 Void Analysis
		  	C2570_VOID_FL='Y',
			c2570_last_updated_by      = p_user_id,
			c2570_last_updated_date    = CURRENT_DATE
		  WHERE c2570_thb_load_lock_master_id = p_analysis_id
		  AND c2570_void_fl IS NULL;
 

	EXCEPTION WHEN OTHERS
      THEN
       v_analysis_id:=NULL;
   END;
   
END gm_rollback_lock_genereate;
	
	
END gm_pkg_op_thb_analysis_rpt;
/