
CREATE OR REPLACE PACKAGE body gm_pkg_op_donor_sav_bulk_rs
AS
/**************************************************************************************
* Description :   this procedure is to create rs creation
* 
1. input parameters: donor number string, load number, user_id, po_number, company_id, plant id
2. Loop the Donor number string and call gm_sav_rs_creation procedure for create RS.
PC-2139-ability to bulk RScreation-for-Rad Run
*/
PROCEDURE gm_sav_rs_creation_main (
    p_donor_no     IN   CLOB,
    p_load_id      IN   t2560_thb_load.c2560_thb_load_num%TYPE,
    p_user_id      IN   t101_user.c101_user_id%TYPE,
    p_cust_po      IN   t401_purchase_order.c401_purchase_ord_id%TYPE,
    p_company_id   IN   t1900_company.c1900_company_id%TYPE,
    p_plant_id     IN   t5041_plant_company_mapping.c5040_plant_id%TYPE
) 
AS
        v_string      CLOB := p_donor_no;
        v_substring   CLOB;
        v_donor_num   t2561_thb_load_item_detail.c2561_donor_number%TYPE;
        v_email_to    t906_rules.c906_rule_value%TYPE;
        v_email_msg   VARCHAR2(2000);
        v_email_sub   VARCHAR2(2000);
    BEGIN
        WHILE instr(v_string, ',') <> 0 
        LOOP
            v_donor_num := NULL;
            v_substring := substr(v_string, 1, instr(v_string, ',') - 1);

            v_string := substr(v_string, instr(v_string, ',') + 1);
            v_donor_num := v_substring;
            gm_pkg_op_donor_sav_bulk_rs.gm_sav_rs_creation(v_donor_num, p_load_id, p_user_id, p_cust_po, p_company_id,
                                                           p_plant_id);
        END LOOP;--While Loop End
          
    -- email notification for success scenario
     SELECT get_rule_value ('RS_CREATION_SUCCESS_SUB', 'RS_CREATION')
       INTO v_email_sub
       FROM dual;
       
     SELECT get_rule_value ('RS_EMAIL_TO', 'RS_CREATION')
       INTO v_email_to
       FROM dual;
       
     SELECT get_rule_value ('RS_EMAIL_SUC_MSG', 'RS_CREATION')
       INTO v_email_msg
       FROM dual;
       
    v_email_msg := v_email_msg || ':'||p_donor_no||'. <br>' || 'Load Number : ' || p_load_id || '. <br>';
    v_email_sub := v_email_sub || ' - ' || p_load_id ;
    -- call the email procedure to send email to the user
     gm_com_send_html_email(v_email_to,v_email_sub,null,v_email_msg);
    END gm_sav_rs_creation_main;
/**************************************************************************************
* Description :   this procedure is to create rs creation
* author      :   gomathi palani
*    
1. input parameters: donor number, load number, user_id, po_number, company_id, plant id
2. sequence flow:

a)fetch po details using the input parameter po number.[po date, vendor id] - this is required for the rs creation.

b)update expiry date for all the lot numbers for the received load number. this step is performed here since the data
from thb get loaded in to spineit every monday and the lock and generate will happen on thursday and friday of the same
week. if there is any update to the part shelf life this will have compliance issue in the expiry date, so we are
updating the expiry date value at the time of lock and generate.

c) updating donor attributes [ fetch donor attributes from the received thb load and call the portal procedure to
create/update donor.

d) find out if there are any duplicate lot number received from the thb load.
if there are duplicates, update the c2560_duplicate_lot_flag in t2561_thb_load_item_detail table for the respective lot
numbers.

send an email notification for the duplicated lot number
e)  RS creation procedure call for finished good [ non frozen]

f)  RS creation procedure call for re-pack [ non frozen]

g)  RS creation procedure call for finished good [frozen]

h)  RS creation procedure call for re-pack [frozen]

i)  once the rs is created system will send out a automatic email notification.

***************************************************************************************/
PROCEDURE gm_sav_rs_creation (
        p_donor_no   IN t2561_thb_load_item_detail.c2561_donor_number%type,
        p_load_id    IN t2560_thb_load.c2560_thb_load_num%type,
        p_user_id    IN t101_user.c101_user_id%type,
        p_cust_po    IN t401_purchase_order.c401_purchase_ord_id%type,
        p_company_id IN t1900_company.c1900_company_id%type,
        p_plant_id   IN t5041_plant_company_mapping.c5040_plant_id%type)
AS
    v_po_num t401_purchase_order.c401_purchase_ord_id %type;
    v_po_received_date t401_purchase_order.c401_purchase_ord_date%type;
    v_input_string CLOB;
    v_email_to t906_rules.c906_rule_value%type;
    v_email_msg VARCHAR2 (2000) ;
    v_email_sub VARCHAR2 (2000) ;
    v_vendor_id t301_vendor.c301_vendor_id%type;
    v_date_fmt t906_rules.c906_rule_value%type;
BEGIN
    -- fetch current week purchase order and purchase order date (currently passing the po in the param)
    BEGIN
    
        gm_pkg_cor_client_context.gm_sav_client_context (p_company_id, p_plant_id) ;
        
         SELECT get_compdtfmt_frm_cntx 
         INTO v_date_fmt FROM dual;
     
     -- Fetch the PO Received date and vendor ID
         SELECT t401.c401_purchase_ord_date, t401.c301_vendor_id
           INTO v_po_received_date, v_vendor_id
           FROM t401_purchase_order t401
          WHERE t401.c401_purchase_ord_id = p_cust_po
            AND t401.c401_void_fl        IS NULL
            AND t401.c1900_company_id     = p_company_id;
            
         SELECT get_rule_value ('RS_EMAIL_TO', 'RS_CREATION')
           INTO v_email_to
           FROM dual;
           
    EXCEPTION
    WHEN OTHERS THEN
        v_po_received_date := NULL; -- add clear message here and skip the subsequent process
    END;
    
    -- Below procedure will check and exclude part numbers which doesnt have a vendor price.
    
    gm_pkg_op_donor_sav_bulk_rs.gm_rs_creation_chk_vendor_price(p_donor_no,p_load_id, p_user_id);
    
    -- Below procedure will update the expiry date for all the part numbers.
    gm_pkg_op_donor_sav_bulk_rs.gm_sav_update_expiry_date (p_load_id, p_donor_no, p_user_id) ;
       
    
    gm_pkg_op_donor_sav_bulk_rs.gm_sav_rs_donor_attributes (p_donor_no, v_vendor_id, p_load_id, p_user_id) ;
    
    -- fetch the lot numbers and check for duplicates
    gm_pkg_op_donor_sav_bulk_rs.gm_rs_creation_fetch_duplicate_lots (p_donor_no, p_load_id, p_user_id) ;
    
    -- rs creation for fg inventory non frozen part
    gm_pkg_op_donor_sav_bulk_rs.gm_rs_creation_non_frozen_fg_re_pack (p_donor_no, p_load_id, p_cust_po, v_po_received_date, '104620'
    , p_user_id) ;
    
    -- rs creation for re-pack inventory inventory non frozen part
    gm_pkg_op_donor_sav_bulk_rs.gm_rs_creation_non_frozen_fg_re_pack (p_donor_no, p_load_id, p_cust_po, v_po_received_date, '104621'
    , p_user_id) ;
    
    -- rs creation for fg inventory inventory and  frozen part
    gm_pkg_op_donor_sav_bulk_rs.gm_rs_creation_frozen_fg_re_pack (p_donor_no, p_load_id, p_cust_po, v_po_received_date, '104620',
    p_user_id) ;
    
    -- rs creation for re pack inventory inventory and  frozen part
    gm_pkg_op_donor_sav_bulk_rs.gm_rs_creation_frozen_fg_re_pack (p_donor_no, p_load_id, p_cust_po, v_po_received_date, '104621',
    p_user_id) ;
    
    -- Call the below procedure to update the RS Creation Flag
    gm_pkg_op_donor_sav_bulk_rs.gm_update_RS_Flag_donor(p_load_id,p_donor_no,'Y',p_user_id);
    
    -- Commit is added below so that if the email scenario is not working the RS transaction will still get created and transaction will not get stopped.
     
EXCEPTION

WHEN OTHERS THEN
     SELECT get_rule_value ('RS_EMAIL_ERR_SUB', 'RS_CREATION')
       INTO v_email_sub
       FROM dual;
       
     SELECT get_rule_value ('RS_EMAIL_ERR_MSG', 'RS_CREATION')
       INTO v_email_msg
       FROM dual;
       
    v_email_msg := v_email_msg || ':'||p_donor_no||'. <br>'||sqlerrm;
    
    -- call the email procedure to send email to the user
    gm_com_send_html_email(v_email_to, v_email_sub, NULL, v_email_msg) ;
    
END gm_sav_rs_creation;
/**************************************************************************************
* description :   this procedure is update the donor attributes
* author      :   gomathi palani
***************************************************************************************/
PROCEDURE gm_sav_rs_donor_attributes (
        p_donor_no  IN t2561_thb_load_item_detail.c2561_donor_number%type,
        p_vendor_id IN t2560_thb_load.c2560_thb_load_num%type,
        p_load_id   IN t2560_thb_load.c2560_thb_load_num%type,
        p_user_id   IN t101_user.c101_user_id%type)
AS
    CURSOR donor_attributes
    IS
	-- Changing the Decode statement value from m to M
        SELECT DISTINCT t2561.c2561_donor_age age, DECODE (t2561.c2561_donor_sex, 'M', 1950, 1951) sex, DECODE (
            t2561.c901_donor_international_flag, 108160, 1961, 1960) internation_flag, DECODE (t2561.c901_donor_type,
            108144, 1970, 1971) research_flag
           FROM t2560_thb_load t2560, t2561_thb_load_item_detail t2561
          WHERE t2560.c2560_thb_load_id  = t2561.c2560_thb_load_id
            AND t2561.c2561_donor_number = p_donor_no
            AND t2560.c2560_thb_load_num = p_load_id
            AND t2560.c2560_void_fl     IS NULL
            AND t2561.c2561_void_fl     IS NULL;
BEGIN
    FOR v_donor_attr IN donor_attributes
    LOOP
        gm_pkg_op_dhr.gm_op_sav_donor_master (p_donor_no, p_vendor_id, v_donor_attr.age, v_donor_attr.sex,
        v_donor_attr.internation_flag, v_donor_attr.research_flag, p_user_id) ;
    END LOOP;
END gm_sav_rs_donor_attributes;
/**************************************************************************************
* description :   this procedure is to check for duplicate lots
* author      :   gomathi palani
***************************************************************************************/
PROCEDURE gm_rs_creation_fetch_duplicate_lots (
        p_donor_no IN t2561_thb_load_item_detail.c2561_donor_number%type,
        p_load_id  IN t2560_thb_load.c2560_thb_load_num%type,
        p_user_id  IN t101_user.c101_user_id%type)
AS
    v_message CLOB;
    v_lot_string CLOB := NULL;
    lotstring_str CLOB;
    v_email_to t906_rules.c906_rule_value%type;
    v_email_msg VARCHAR2 (2000) ;
    v_email_sub VARCHAR2 (2000) ;
    CURSOR lot_string
    IS
        SELECT DISTINCT (t2561.c2561_control_number) ctrlnum
           FROM t2560_thb_load t2560, t2561_thb_load_item_detail t2561
          WHERE t2560.c2560_thb_load_id  = t2561.c2560_thb_load_id
            AND t2560.c2560_thb_load_num = p_load_id
            AND t2561.c2561_donor_number = p_donor_no
            AND t2560.c2560_void_fl     IS NULL
            AND t2561.c2561_void_fl     IS NULL;
BEGIN

    FOR v_lot_string IN lot_string
    
    LOOP
        lotstring_str := lotstring_str || v_lot_string.ctrlnum|| ',';
    END LOOP;
    -- call the portal procedure to check for duplictes;
    gm_pkg_op_bulk_dhr_rpt.gm_fch_duplicate_lots (lotstring_str, '', v_message) ;
    
    IF (v_message IS NOT NULL) THEN
        -- insert the duplicate lot numbers in temp table
         DELETE FROM my_temp_list;
         INSERT INTO my_temp_list
            (my_temp_txn_id
            )
    WITH data AS
        (
             SELECT v_message AS str FROM dual
        )
     SELECT trim (regexp_substr (str, '[^,]+', 1, level)) str
       FROM data
        CONNECT BY instr (str, ',', 1, level - 1) > 0;
  -- Updating the Duplicate flag for the Duplicate Lot Number      
     UPDATE t2561_thb_load_item_detail
    SET c2561_duplicate_lot_fl    = 'y', c2561_last_updated_date = CURRENT_DATE, c2561_last_updated_by = p_user_id
      WHERE c2561_donor_number    = p_donor_no
        AND c2561_control_number IN
        (
             SELECT my_temp_txn_id FROM my_temp_list
        ) ;
        
     SELECT get_rule_value ('RS_EMAIL_TO', 'RS_CREATION')
       INTO v_email_to
       FROM dual;
     SELECT get_rule_value ('RS_EMAIL_ERR_SUB', 'RS_CREATION')
       INTO v_email_sub
       FROM dual;
     SELECT get_rule_value ('RS_EMAIL_DUP_LOT_MSG', 'RS_CREATION')
       INTO v_email_msg
       FROM dual;
       
    v_email_msg := v_email_msg || ':'||v_message||'. <br>';
    -- call the email procedure to send email to the user
    gm_com_send_html_email(v_email_to, v_email_sub, NULL, v_email_msg) ;
    -- Commeting the below line since we want to proceed creating RS for Unique Lot number so the procedure flow should continue.
    --  gm_raise_application_error('-20999','302','');
END IF;
END gm_rs_creation_fetch_duplicate_lots;

/**************************************************************************************
* description :   this procedure is to create rs creation
* author      :   gomathi palani
***************************************************************************************/
PROCEDURE gm_rs_creation_non_frozen_fg_re_pack (
        p_donor_no       IN t2561_thb_load_item_detail.c2561_donor_number%type,
        p_load_id        IN t2560_thb_load.c2560_thb_load_num%type,
        p_cust_po        IN t401_purchase_order.c401_purchase_ord_id%type,
        p_cust_po_dte    IN t401_purchase_order.c401_purchase_ord_date%type,
        p_warehouse_type IN t901_code_lookup.c901_code_id%type,
        p_user_id        IN t101_user.c101_user_id%type)
AS
    v_input_string CLOB        := '';
    v_dhr_action   VARCHAR2 (20) := 'save_bulk_dhr';
    v_str_rej_rsfl VARCHAR2 (1)  := 'n';
    v_rs_id t4081_dhr_bulk_receive.c4081_dhr_bulk_id%type;
    v_stage_dt t4081_dhr_bulk_receive.c4081_stage_date%type;
    v_date_fmt t906_rules.c906_rule_value%type;
    
    CURSOR fg_rp_non_frozen
    IS
         SELECT  NVL(t2561.c205_redsg_part_number_id,t2561.c205_part_number_id) partnum, t2561.c2561_control_number
            ctrlnum, t2561.c2561_material_spec custsize, TO_CHAR (t2561.c2550_expiry_date, v_date_fmt) expdate
          , TO_CHAR (t2561.c2550_mfg_date, v_date_fmt) mandfate, t2561.c2561_donor_number, t2573.c2540_stage_date
            stage_date
           FROM t2560_thb_load t2560, t2561_thb_load_item_detail t2561, t205d_part_attribute t205d
          , t2573_thb_donor_lock t2573
          WHERE t2560.c2560_thb_load_id          = t2561.c2560_thb_load_id
            AND t2560.c2560_thb_load_num         = p_load_id
            AND t2561.c2561_donor_number         = p_donor_no
            AND t2561.c901_ware_house_type       = p_warehouse_type
            AND t2560.c2560_void_fl             IS NULL
            AND t2561.c2561_void_fl             IS NULL
            AND t2561.c405_vendor_price_fl      IS NULL
            AND t2561.c2561_duplicate_lot_fl    IS NULL
            AND t2561.c2561_control_number		IS NOT NULL
            AND t2561.c205_part_number_id       IS NOT NULL
            AND t205d.c205d_void_fl             IS NULL
            AND t2573.c2573_void_fl             IS NULL
            AND t2561.c2561_donor_number         = t2573.c2540_donor_number (+)
            AND t2561.c205_part_number_id        = t205d.c205_part_number_id (+)
            AND t2573.c2570_thb_load_lock_master_id=t2560.c2570_thb_load_lock_master_id
            AND t2573.c2540_rs_created_fl		IS  NULL
            AND t205d.c901_attribute_type        = '103728' -- storage temperature
            AND t205d.c205d_attribute_value NOT IN ('103708', '4000862') -- storage temperature values. this condition
            -- is added to create input string for non-frozen parts.
       ORDER BY NVL (t2561.c205_redsg_part_number_id, t2561.c205_part_number_id), t2561.c2561_control_number;
       
BEGIN

     SELECT get_compdtfmt_frm_cntx INTO v_date_fmt FROM dual;
     
    FOR v_fg_rp_non_frozen IN fg_rp_non_frozen
    
    LOOP
        v_stage_dt     := v_fg_rp_non_frozen.stage_date;
        v_input_string := v_input_string||v_fg_rp_non_frozen.partnum||'^'||v_fg_rp_non_frozen.ctrlnum||'^'||
        v_fg_rp_non_frozen.custsize||'^'|| v_fg_rp_non_frozen.expdate||'^'||v_fg_rp_non_frozen.mandfate||'^'||'^'||'^'
        ||'^'||'1'||'|';
    END LOOP;
    
    IF (v_input_string IS NOT NULL) THEN
    
        gm_pkg_op_bulk_dhr_txn.gm_pkg_op_rec_bulk_shipment (v_rs_id, p_cust_po, p_cust_po_dte, p_load_id, p_donor_no,
        v_input_string, p_user_id, v_dhr_action, 'n', v_stage_dt) ;
        -- call the below procedure to update the inventory type for dhr control numbers
        gm_pkg_op_donor_sav_bulk_rs.gm_update_inventory_dhr_control (p_warehouse_type, v_rs_id, p_user_id) ;
    END IF;
    
END gm_rs_creation_non_frozen_fg_re_pack;
/**************************************************************************************
* description :   this procedure is to create rs creation
* author      :   gomathi palani
***************************************************************************************/
PROCEDURE gm_rs_creation_frozen_fg_re_pack (
        p_donor_no       IN t2561_thb_load_item_detail.c2561_donor_number%type,
        p_load_id        IN t2560_thb_load.c2560_thb_load_num%type,
        p_cust_po        IN t401_purchase_order.c401_purchase_ord_id%type,
        p_cust_po_dte    IN t401_purchase_order.c401_purchase_ord_date%type,
        p_warehouse_type IN t901_code_lookup.c901_code_id%type,
        p_user_id        IN t101_user.c101_user_id%type)
AS

    v_input_string CLOB        := NULL;
    v_dhr_action VARCHAR2 (20) := 'save_bulk_dhr';
    v_rs_id t4081_dhr_bulk_receive.c4081_dhr_bulk_id%type;
    v_stage_dt t4081_dhr_bulk_receive.c4081_stage_date%type;
    v_date_fmt t906_rules.c906_rule_value%type;
    
    CURSOR fg_rp_frozen
    IS
         SELECT NVL(t2561.c205_redsg_part_number_id,t2561.c205_part_number_id) partnum, t2561.c2561_control_number
            ctrlnum, t2561.c2561_material_spec custsize, TO_CHAR (t2561.c2550_expiry_date, v_date_fmt) expdate
          , TO_CHAR (t2561.c2550_mfg_date, v_date_fmt) mandfate, t2561.c2561_donor_number, t2573.c2540_stage_date
            stage_date
           FROM t2560_thb_load t2560, t2561_thb_load_item_detail t2561, t205d_part_attribute t205d
          , t2573_thb_donor_lock t2573
          WHERE t2560.c2560_thb_load_id       = t2561.c2560_thb_load_id
            AND t2560.c2560_thb_load_num      = p_load_id
            AND t2561.c2561_donor_number      = p_donor_no
            AND t2561.c901_ware_house_type    = p_warehouse_type
            AND t2560.c2560_void_fl          IS NULL
            AND t2561.c2561_void_fl          IS NULL
            AND t2561.c2561_control_number	IS NOT NULL
            AND t2561.c205_part_number_id   IS NOT NULL
            AND t205d.c205d_void_fl          IS NULL
            AND t2561.c405_vendor_price_fl   IS NULL
            AND t2561.c2561_duplicate_lot_fl IS NULL
            AND t2573.c2573_void_fl          IS NULL
            AND t2573.c2570_thb_load_lock_master_id=t2560.c2570_thb_load_lock_master_id
            AND t2573.c2540_rs_created_fl		IS  NULL
            AND t2561.c2561_donor_number      = t2573.c2540_donor_number (+)
            AND t205d.c901_attribute_type     = '103728'-- storage temperature
            AND t205d.c205d_attribute_value  IN ('103708', '4000862') -- storage temperature values
            AND t2561.c205_part_number_id     = t205d.c205_part_number_id (+)
       ORDER BY NVL (t2561.c205_redsg_part_number_id, t2561.c205_part_number_id), t2561.c2561_control_number;
       
BEGIN
     SELECT get_compdtfmt_frm_cntx INTO v_date_fmt FROM dual;
     
    FOR v_fg_rp_frozen IN fg_rp_frozen
    
    LOOP
        v_stage_dt := v_fg_rp_frozen.stage_date;
        -- v_input_string = v_input_string + partnum + '^'+ ctrlnum.touppercase() + '^' + custsize + '^' + expdate + '^
        -- ' + manfdate + '^'+ '^'+ '^'+ '^'+ '1' +'|';
        v_input_string := v_input_string||v_fg_rp_frozen.partnum||'^'||v_fg_rp_frozen.ctrlnum||'^'||
        v_fg_rp_frozen.custsize||'^'|| v_fg_rp_frozen.expdate||'^'||v_fg_rp_frozen.mandfate||'^'||'^'||'^'||'^'||'1'||
        '|';
    END LOOP;
    
    IF (v_input_string IS NOT NULL) THEN
    
        gm_pkg_op_bulk_dhr_txn.gm_pkg_op_rec_bulk_shipment (v_rs_id, p_cust_po, p_cust_po_dte, p_load_id, p_donor_no,
        v_input_string, p_user_id, v_dhr_action, 'n', v_stage_dt) ;
        -- call the below procedure to update the inventory type for dhr control numbers
        gm_pkg_op_donor_sav_bulk_rs.gm_update_inventory_dhr_control (p_warehouse_type, v_rs_id, p_user_id) ;
    END IF;
END gm_rs_creation_frozen_fg_re_pack;
/**************************************************************************************
* description :   this procedure is to update the inventory type for each allograft number in the dhr control number
table
* author      :   gomathi palani
***************************************************************************************/
PROCEDURE gm_update_inventory_dhr_control (
        p_warehouse_type IN t901_code_lookup.c901_code_id%type,
        p_rs_id          IN t4081_dhr_bulk_receive.c4081_dhr_bulk_id%type,
        p_user_id        IN t101_user.c101_user_id%type)
AS
    CURSOR fetch_dhr
    IS
         SELECT t4082.c408_dhr_id dhr_id
           FROM t4082_bulk_dhr_mapping t4082
          WHERE t4082.c4081_dhr_bulk_id = p_rs_id
            AND t4082.c4082_void_fl    IS NULL;
BEGIN
    FOR v_fetch_dhr IN fetch_dhr
    LOOP
         UPDATE t408a_dhr_control_number
        SET c901_inventory_type_id = p_warehouse_type, c408a_last_updated_by = p_user_id, c408a_last_updated_date =
            CURRENT_DATE
          WHERE c408_dhr_id    = v_fetch_dhr.dhr_id
            AND c408a_void_fl IS NULL;
    END LOOP;
END gm_update_inventory_dhr_control;
/**************************************************************************************
* description :   this procedure is to void the rs and dhr's created
* author      :   gomathi palani
***************************************************************************************/
PROCEDURE gm_void_rs (
        p_rs_id   IN t4081_dhr_bulk_receive.c4081_dhr_bulk_id%type,
        p_user_id IN t101_user.c101_user_id%type)
AS
    CURSOR fetch_dhr
    IS
         SELECT t4082.c408_dhr_id dhr_id
           FROM t4082_bulk_dhr_mapping t4082
          WHERE t4082.c4081_dhr_bulk_id = p_rs_id
            AND t4082.c4082_void_fl    IS NULL;
BEGIN
    FOR v_fetch_dhr IN fetch_dhr
    
    LOOP
    
         UPDATE t408_dhr t408
        SET c408_void_fl         = 'y', c408_last_updated_date = CURRENT_DATE, c408_last_updated_by = p_user_id
          WHERE t408.c408_dhr_id = v_fetch_dhr.dhr_id
            AND c408_void_fl    IS NULL;
            
         UPDATE t408a_dhr_control_number
        SET c408a_conrol_number     = NULL, c408a_last_updated_by = p_user_id, c408a_void_fl = 'y'
          , c408a_last_updated_date = CURRENT_DATE
          WHERE c408_dhr_id         = v_fetch_dhr.dhr_id
            AND c408a_void_fl      IS NULL;
    END LOOP;
    
    -- check if we need to remove the rs-dhr mapping
     UPDATE t4081_dhr_bulk_receive
    SET c4081_void_fl         = 'y', c4081_last_updated_by = p_user_id, c4081_last_updated_date = CURRENT_DATE
      WHERE c4081_dhr_bulk_id = p_rs_id
        AND c4081_void_fl    IS NULL;
END gm_void_rs;
/**************************************************************************************
* description :   this procedure is update the stage date
* author      :   gomathi palani
***************************************************************************************/
PROCEDURE gm_update_stage_date (
        p_txn_id   IN t4081_dhr_bulk_receive.c4081_dhr_bulk_id%type,
        p_stage_dt IN t4081_dhr_bulk_receive.c4081_stage_date%type,
        p_txn_type IN t412_inhouse_transactions.c412_type%type,
        p_user_id  IN t101_user.c101_user_id%type)
AS
BEGIN
    IF (p_txn_type = 103932) THEN
         UPDATE t412_inhouse_transactions
        SET c412_stage_date           = p_stage_dt, c412_last_updated_by = p_user_id, c412_last_updated_date = CURRENT_DATE
          WHERE c412_inhouse_trans_id = p_txn_id
            AND c412_void_fl         IS NULL;
    ELSE
         UPDATE t4081_dhr_bulk_receive
        SET c4081_stage_date      = p_stage_dt, c4081_last_updated_by = p_user_id, c4081_last_updated_date = CURRENT_DATE
          WHERE c4081_dhr_bulk_id = p_txn_id
            AND c4081_void_fl    IS NULL;
    END IF;
END gm_update_stage_date;
/**************************************************************************************
* description :   this procedure is update the expiry date
* author      :   gomathi palani
***************************************************************************************/
PROCEDURE gm_sav_update_expiry_date (
        p_load_id  IN t2560_thb_load.c2560_thb_load_num%type,
        p_donor_no IN t2561_thb_load_item_detail.c2561_donor_number%type,
        p_user_id  IN t101_user.c101_user_id%type)
AS
    v_part_num t2561_thb_load_item_detail.c205_part_number_id%type;
    v_expiry_date t2561_thb_load_item_detail.c2550_expiry_date%type;
    v_mfg_date t2561_thb_load_item_detail.c2550_mfg_date%TYPE;
    v_load_id t2560_thb_load.c2560_thb_load_id%TYPE;
    CURSOR fch_expiry
    IS
        SELECT DISTINCT t2561.c205_part_number_id part_num, v205g.c205d_shelf_life, t2561.c2550_mfg_date mfg_date
          , add_months (t2561.c2550_mfg_date, v205g.c205d_shelf_life) expiry_date, t2560.c2560_thb_load_id load_id
           FROM t2560_thb_load t2560, t2561_thb_load_item_detail t2561, v205g_part_label_parameter v205g
          WHERE t2560.c2560_thb_load_id   = t2561.c2560_thb_load_id
            AND t2560.c2560_thb_load_num  = p_load_id
            AND t2561.c205_part_number_id = v205g.c205_part_number_id
            AND t2561.c2561_donor_number  = p_donor_no
            AND t2561.c2561_void_fl      IS NULL
            AND t2560.c2560_void_fl      IS NULL
       ORDER BY t2561.c205_part_number_id;
BEGIN

    FOR v_fch_expiry IN fch_expiry
    
    LOOP
        v_part_num    := v_fch_expiry.part_num;
        v_expiry_date := v_fch_expiry.expiry_date;
        v_mfg_date    := v_fch_expiry.mfg_date;
        v_load_id	  :=v_fch_expiry.load_id;
        
         UPDATE t2561_thb_load_item_detail
        SET c2550_expiry_date = v_expiry_date, c2561_last_updated_by = p_user_id, c2561_last_updated_date =
            CURRENT_DATE
          WHERE c2561_donor_number  = p_donor_no
            AND c205_part_number_id = v_part_num
            AND c2550_mfg_date=v_mfg_date
            AND c2560_thb_load_id  = v_load_id
            AND c2561_void_fl      IS NULL;
            
    END LOOP;
END gm_sav_update_expiry_date;


  /**************************************************************************************
* Description :   This procedure is update the RS Creation flag for the donor number
* Author      :   Gomathi Palani
***************************************************************************************/  
  PROCEDURE gm_update_RS_Flag_donor(
     p_load_id        IN t2560_thb_load.c2560_thb_load_num%TYPE,
	 p_donor_no       IN t2561_thb_load_item_detail.c2561_donor_number%TYPE,
	 p_rs_flag		  IN t2573_thb_donor_lock.c2540_rs_created_fl%TYPE,
     p_user_id        IN t101_user.c101_user_id%TYPE
    )
  AS
  
  v_lock_master_id t2573_thb_donor_lock.c2570_thb_load_lock_master_id%TYPE;
  
  BEGIN
  
  -- Fetch the lock master id of the load number
  		BEGIN
         SELECT DISTINCT t2560.c2570_thb_load_lock_master_id
	         INTO v_lock_master_id
  	     FROM t2560_thb_load t2560
  		 WHERE t2560.c2560_thb_load_num = p_load_id
  		 AND t2560.c2560_void_fl IS NULL;
        
        EXCEPTION WHEN OTHERS THEN
        v_lock_master_id:=NULL;
        RETURN;
        END;
     
         UPDATE t2573_thb_donor_lock
			SET C2540_RS_CREATED_FL= p_rs_flag, C2540_RS_CREATED_BY = p_user_id, C2573_LAST_UPDATED_BY = p_user_id
  		, C2573_LAST_UPDATED_DATE           =CURRENT_DATE
  		WHERE C2570_THB_LOAD_LOCK_MASTER_ID =v_lock_master_id
    	AND C2540_DONOR_NUMBER              =p_donor_no
    	AND C2573_VOID_FL IS NULL;  
    	
    	IF SQL%ROWCOUNT = 0 THEN
	    INSERT
	    INTO t2573_thb_donor_lock
	      (
	        c2570_thb_load_lock_master_id,
	        c2540_donor_number,
	        c2573_created_by,
	        c2573_created_date,
	        C2540_RS_CREATED_FL,
	        C2540_RS_CREATED_BY
	      )
	      VALUES
	      (
	        v_lock_master_id,
	        p_donor_no,
	        p_user_id,
	        CURRENT_DATE,
	        p_rs_flag,
	        p_user_id
	      );
	  END IF;
   
  END gm_update_RS_Flag_donor;
  
  
  /**************************************************************************************
* Description :   This procedure is to check for vendor price for the rad run parts
* Author      :   Gomathi Palani
***************************************************************************************/  

  PROCEDURE gm_rs_creation_chk_vendor_price(
            p_donor_no   IN t2561_thb_load_item_detail.c2561_donor_number%type,
            p_load_id    IN t2560_thb_load.c2560_thb_load_num%type,
			p_user_id    IN t101_user.c101_user_id%type)

AS

v_vendor_price_string CLOB := NULL;
v_email_to t906_rules.c906_rule_value%type;
v_email_msg VARCHAR2 (2000) ;
v_email_sub VARCHAR2 (2000) ;
v_company_id t1900_company.c1900_company_id%TYPE;
v_plant_id t5040_plant_master.c5040_plant_id%TYPE;
v_load_id t2560_thb_load.c2560_thb_load_id%TYPE;
v_vendor_price_part t205_part_number.c205_part_number_id%TYPE;
v_vendor_cost t405_vendor_pricing_details.c405_cost_price%TYPE;
v_part_status t205_part_number.c205_active_fl%TYPE;

CURSOR vendor_price
    IS
                  
         SELECT DISTINCT  NVL(t2561.c205_redsg_part_number_id,t2561.c205_part_number_id)  pnum, NVL(t405.c405_cost_price,'0') cost_price, T2561.c2560_thb_load_id load_id, NVL(t205.c205_active_fl,'N') part_active_flag
           FROM t2560_thb_load t2560, t2561_thb_load_item_detail t2561, t405_vendor_pricing_details t405, t205_part_number t205
          WHERE t2560.c2560_thb_load_id  = t2561.c2560_thb_load_id
            AND t2560.c2560_thb_load_num = p_load_id
            AND t2561.c2561_donor_number = p_donor_no
            AND NVL(t2561.c205_redsg_part_number_id,t2561.c205_part_number_id) =t205.c205_part_number_id (+)
            AND NVL(t2561.c205_redsg_part_number_id,T2561.C205_PART_NUMBER_ID) =T405.C205_PART_NUMBER_ID (+)
            AND t405.c1900_company_id(+) =v_company_id
            AND t405.c405_active_fl (+) ='Y'
            AND t405.c405_void_fl (+) IS NULL
            AND t2560.c2560_void_fl     IS NULL
            AND t2561.c2561_void_fl     IS NULL;

BEGIN

		
		

-- To get the company information.

      SELECT  get_plantid_frm_cntx, get_compid_frm_cntx()
		INTO  v_plant_id, v_company_id FROM DUAL;
        
        delete from my_temp_list;
    FOR v_vendor_price IN vendor_price
    
     LOOP
       v_load_id:=v_vendor_price.load_id;
       v_vendor_price_part:=v_vendor_price.pnum;
       v_vendor_cost:=v_vendor_price.cost_price;
       v_part_status:=v_vendor_price.part_active_flag;
       
    
   -- If the Vendor price is null or if the Part status is not active set the flag as Yes and ignore those part numbers,
   
    IF ((v_vendor_cost =NULL OR v_vendor_cost='0') OR (v_part_status='N' OR v_part_status = NULL))  THEN
       
       	v_vendor_price_string:= v_vendor_price_string ||v_vendor_price_part|| ',';
       
       
       INSERT INTO my_temp_list
            (my_temp_txn_id
            ) values (v_vendor_price_part); 
       
       END IF;
     END LOOP;
     
     IF (v_vendor_price_string IS NOT NULL) THEN
                
                 	                
  -- Updating the vendor price flag for the Base part number which doesnt  have a vendor price in the system    
    
     UPDATE t2561_thb_load_item_detail
    SET c405_vendor_price_fl    = 'N', c2561_last_updated_date = CURRENT_DATE, c2561_last_updated_by = p_user_id
      WHERE c2561_donor_number    = p_donor_no
        AND c2560_thb_load_id IN (
        SELECT c2560_thb_load_id FROM t2560_thb_load WHERE c2560_thb_load_num=p_load_id AND c2560_void_fl IS NULL
        )
        AND c205_part_number_id IN
        (
             SELECT my_temp_txn_id FROM my_temp_list
        ) ;
  
    -- Updating the vendor price flag for the re-designated part number which doesnt  have a vendor price in the system    
        
         UPDATE t2561_thb_load_item_detail
    SET c405_vendor_price_fl    = 'N', c2561_last_updated_date = CURRENT_DATE, c2561_last_updated_by = p_user_id
      WHERE c2561_donor_number    = p_donor_no
  		AND c2560_thb_load_id IN
  		 (
  		  SELECT c2560_thb_load_id FROM t2560_thb_load WHERE c2560_thb_load_num=p_load_id AND c2560_void_fl IS NULL
        )
             
        AND C205_REDSG_PART_NUMBER_ID IN
        (
             SELECT my_temp_txn_id FROM my_temp_list
        ) ;
        
        SELECT get_rule_value ('RS_EMAIL_TO', 'RS_CREATION')
       INTO v_email_to
       FROM dual;
     SELECT get_rule_value ('RS_EMAIL_ERR_SUB', 'RS_CREATION')
       INTO v_email_sub
       FROM dual;
     SELECT get_rule_value ('RS_EMAIL_VENDOR_PRICE_ERR_MSG', 'RS_CREATION')
       INTO v_email_msg
       FROM dual;
       
        v_email_msg := v_email_msg || ':'||v_vendor_price_string||'. <br>';
    -- call the email procedure to send email to the user
    gm_com_send_html_email(v_email_to, v_email_sub, NULL, v_email_msg) ;

ELSE
  UPDATE t2561_thb_load_item_detail
    SET c405_vendor_price_fl    = '', c2561_last_updated_date = CURRENT_DATE, c2561_last_updated_by = p_user_id
      WHERE c2561_donor_number    = p_donor_no
       AND c2560_thb_load_id IN
  		 (
  		  SELECT c2560_thb_load_id FROM t2560_thb_load WHERE c2560_thb_load_num=p_load_id AND c2560_void_fl IS NULL
        );


END IF;


END gm_rs_creation_chk_vendor_price;

END gm_pkg_op_donor_sav_bulk_rs;
/