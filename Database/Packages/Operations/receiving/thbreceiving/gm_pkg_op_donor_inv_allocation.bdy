CREATE OR REPLACE
PACKAGE BODY gm_pkg_op_donor_inv_allocation
AS
    /**************************************************************************************
    * Description :  This procedure is used update the private part allocation for the Rad Run load allografts
    * Author      :  gomathi
    ***************************************************************************************/
PROCEDURE gm_sav_part_allocation (
        p_item_details_id IN CLOB,
        p_redsg_part_num  IN t2561_thb_load_item_detail.c205_redsg_part_number_id%TYPE,
		p_user_id         IN t101_user.c101_user_id%TYPE)        
AS

    v_string CLOB := p_item_details_id;
    v_substring CLOB;
    v_item_id T2561_THB_LOAD_ITEM_DETAIL.C2561_THB_LOAD_ITEM_DETAIL_ID%TYPE;
    
BEGIN

     
    WHILE INSTR (v_string, ',') <> 0
    LOOP
        --
        v_substring := SUBSTR (v_string, 1, INSTR (v_string, ',') - 1) ;
        v_string    := SUBSTR (v_string, INSTR (v_string, ',')    + 1) ;
        v_item_id   := TO_NUMBER (V_SUBSTRING) ;
        
        
         SELECT t2561.c2561_thb_load_item_detail_id
              INTO v_item_id
               FROM t2561_thb_load_item_detail t2561
          WHERE t2561.c2561_thb_load_item_detail_id = v_item_id
         AND t2561.c2561_void_fl   IS NULL
         FOR UPDATE;
        
         UPDATE t2561_thb_load_item_detail
        SET c205_redsg_part_number_id = p_redsg_part_num, c2561_last_updated_by = p_user_id, c2561_last_updated_date =
            CURRENT_DATE
          WHERE C2561_THB_LOAD_ITEM_DETAIL_ID = v_item_id
            AND c2561_void_fl      IS NULL;
    END LOOP;
	
	
    EXCEPTION WHEN OTHERS
      THEN
      v_item_id:=NULL;
     
END gm_sav_part_allocation;
/**************************************************************************************
* Description :  This procedure is used update the warehosue type allocation for the Rad Run load allografts
* Author      :  gomathi
***************************************************************************************/
PROCEDURE gm_sav_inventory_allocation (
        p_item_details_id IN CLOB,
        p_ware_house_type IN t2561_thb_load_item_detail.c901_ware_house_type%TYPE,
        p_user_id         IN t101_user.c101_user_id%TYPE)
AS
    v_string CLOB := p_item_details_id;
    v_substring CLOB ;
    v_item_id T2561_THB_LOAD_ITEM_DETAIL.C2561_THB_LOAD_ITEM_DETAIL_ID%TYPE;
    
BEGIN

   
    WHILE INSTR (v_string, ',') <> 0
    LOOP
        v_substring := SUBSTR (v_string, 1, INSTR (v_string, ',') - 1) ;
        v_string    := SUBSTR (v_string, INSTR (v_string, ',')    + 1) ;
        v_item_id   := TO_NUMBER (V_SUBSTRING) ;
        
              SELECT t2561.C2561_THB_LOAD_ITEM_DETAIL_ID 
                INTO v_item_id
               FROM t2561_thb_load_item_detail t2561
              WHERE t2561.c2561_thb_load_item_detail_id = v_item_id
              AND t2561.c2561_void_fl   IS NULL
              FOR UPDATE;
        
			 UPDATE t2561_thb_load_item_detail
			   SET c901_ware_house_type = p_ware_house_type, c2561_last_updated_by = p_user_id, c2561_last_updated_date =
			   CURRENT_DATE
			WHERE c2561_thb_load_item_detail_id = v_item_id
			AND c2561_void_fl   IS NULL;
            
    END LOOP;
    
  EXCEPTION WHEN OTHERS
    THEN
    v_item_id:=NULL;
  
  
END gm_sav_inventory_allocation;

/**************************************************************************************
* Description :   This procedure is used update the Qty Allocated for the donor number
* Author      :  gomathi
***************************************************************************************/
PROCEDURE gm_sav_rad_run_allocation (
        p_donor_no    IN t2561_thb_load_item_detail.c2561_donor_number%TYPE,
        p_base_part   IN t2571_thb_load_lock_details.c205_base_part_number%TYPE,
        p_analysis_id IN t2570_thb_load_lock_master.c2570_thb_load_lock_master_id%TYPE,
        p_priv_part   IN t2571_thb_load_lock_details.c205_pvt_lbl_part_number_id%TYPE,
        p_user_id     IN t101_user.c101_user_id%TYPE)
AS

    v_donor_allocated_qty t2571_thb_load_lock_details.c2571_qty_allocated%TYPE;
    v_load_lock_master_id t2571_thb_load_lock_details.c2570_thb_load_lock_master_id%TYPE;
    
BEGIN
    -- Fetch the total quantity for the selected donor id and the analaysis id.
    
     SELECT COUNT (1)
       INTO v_donor_allocated_qty
       FROM t2561_thb_load_item_detail t2561, t2560_thb_load t2560, t2571_thb_load_lock_details t2571
      WHERE t2560.c2570_thb_load_lock_master_id = t2571.c2570_thb_load_lock_master_id
        AND t2571.c205_pvt_lbl_part_number_id  = p_priv_part
        AND t2571.c205_base_part_number = p_base_part
        AND t2561.c205_part_number_id   = t2571.c205_base_part_number
        AND t2561.c2561_void_fl         IS NULL
        AND t2560.c2560_void_fl         IS NULL
        AND t2560.c2560_thb_load_id    = t2561.c2560_thb_load_id
        AND t2560.c2570_thb_load_lock_master_id = p_analysis_id
        AND t2571.c2571_void_fl     IS NULL;
        
    -- Locking the record to avoid multiple users updating the same record.
    
        SELECT t2571.c2570_thb_load_lock_master_id
          INTO v_load_lock_master_id
          FROM t2571_thb_load_lock_details t2571
          WHERE t2571.c2570_thb_load_lock_master_id = p_analysis_id
         AND t2571.c205_base_part_number         = p_base_part
         AND t2571.c205_pvt_lbl_part_number_id   = p_priv_part
         AND t2571.c2571_void_fl                IS NULL
      FOR UPDATE ;
        
        
     UPDATE t2571_thb_load_lock_details
    SET c2571_qty_allocated = v_donor_allocated_qty, c2571_last_updated_by = p_user_id, c2571_last_updated_date =
        CURRENT_DATE
      WHERE c2570_thb_load_lock_master_id = p_analysis_id
        AND c205_base_part_number         = p_base_part
        AND c205_pvt_lbl_part_number_id   = p_priv_part
        AND c2571_void_fl                  IS NULL;
     
     EXCEPTION WHEN OTHERS
      THEN
       v_load_lock_master_id:=NULL;
       v_donor_allocated_qty:='0';

  END gm_sav_rad_run_allocation;

/**************************************************************************************
* Description :   This procedure is used save the Stage date to backend
* Author      :  Suganthi
***************************************************************************************/
PROCEDURE gm_sav_donor_scheduling_stage 
(
	p_load_num  	IN	t2561_thb_load_item_detail.c2560_thb_load_id%TYPE,
	p_donor_num 	IN	t2561_thb_load_item_detail.c2561_donor_number%TYPE,
	p_stage_dt 		IN	t2573_thb_donor_lock.c2540_stage_date%TYPE,
	p_user_id  		IN t101_user.c101_user_id%TYPE
)
AS
v_analysis_id		t2560_thb_load.c2570_thb_load_lock_master_id%TYPE;
v_stage_start_date 	t2570_thb_load_lock_master.c2570_thb_process_start_date%TYPE;
v_stage_end_date 	t2570_thb_load_lock_master.c2570_thb_process_end_date%TYPE;

BEGIN
	BEGIN
		SELECT DISTINCT T2570.C2570_THB_LOAD_LOCK_MASTER_ID,
		  t2570.c2570_thb_process_start_date,
		  t2570.c2570_thb_process_end_date
		INTO v_analysis_id,
		  v_stage_start_date,
		  v_stage_end_date
		FROM t2561_thb_load_item_detail t2561,
		  t2560_thb_load t2560,
		  t2570_thb_load_lock_master t2570
		WHERE T2560.C2560_THB_LOAD_ID           = T2561.C2560_THB_LOAD_ID
		AND t2560.c2570_thb_load_lock_master_id = t2570.c2570_thb_load_lock_master_id
		AND T2560.C2560_THB_LOAD_NUM            = p_load_num
		AND T2561.C2561_DONOR_NUMBER            = p_donor_num
		AND T2560.C2560_VOID_FL                IS NULL
		AND t2561.c2561_void_fl                IS NULL;
	EXCEPTION WHEN OTHERS THEN
	 v_analysis_id := NULL;
	END;
	
	IF (p_stage_dt>=v_stage_start_date AND p_stage_dt<=v_stage_end_date) THEN
	  UPDATE t2573_thb_donor_lock
	  SET c2540_stage_date                = p_stage_dt ,
	    c2573_last_updated_by             = p_user_id ,
	    c2573_last_updated_date           = CURRENT_DATE
	  WHERE c2570_thb_load_lock_master_id = v_analysis_id
	  AND c2540_donor_number              = p_donor_num ;
	  IF SQL%ROWCOUNT = 0 THEN
	    INSERT
	    INTO t2573_thb_donor_lock
	      (
	        c2570_thb_load_lock_master_id,
	        c2540_donor_number,
	        c2540_stage_date,
	        c2573_created_by,
	        c2573_created_date
	      )
	      VALUES
	      (
	        v_analysis_id,
	        p_donor_num,
	        p_stage_dt,
	        p_user_id,
	        CURRENT_DATE
	      );
	  END IF;
	ELSE
		gm_raise_application_error ('-20999', '448',p_stage_dt) ;
	END IF;
	
END gm_sav_donor_scheduling_stage;


  /**************************************************************************************
* Description :   This procedure is used save the Stage date to PTRD and RS Transaction
* Author      :  gpalani PMT:44211
***************************************************************************************/
PROCEDURE gm_sav_stage_date (
        p_txn_id   IN t412_inhouse_transactions.c412_inhouse_trans_id%TYPE,
        p_txn_type IN t901_code_lookup.C901_CODE_ID%TYPE,
        p_stage_dt IN t2573_thb_donor_lock.c2540_stage_date%TYPE,
        p_user_id  IN t101_user.c101_user_id%TYPE)

AS 

v_txn_id  t412_inhouse_transactions.c412_inhouse_trans_id%TYPE;

 BEGIN
   
    IF (p_txn_type = 101260) THEN
    
         SELECT c412_inhouse_trans_id
           INTO v_txn_id
           FROM t412_inhouse_transactions
          WHERE c412_inhouse_trans_id = p_txn_id
            AND c412_void_fl  IS NULL 
            FOR UPDATE;
            
         UPDATE t412_inhouse_transactions
        SET c412_stage_date = p_stage_dt, c412_last_updated_by = p_user_id, c412_last_updated_date = CURRENT_DATE
          WHERE c412_inhouse_trans_id = p_txn_id
            AND c412_void_fl IS NULL ;
    ELSE
         SELECT C4081_DHR_BULK_ID
           INTO v_txn_id
           FROM T4081_DHR_BULK_RECEIVE
          WHERE C4081_DHR_BULK_ID = p_txn_id
            AND c4081_void_fl IS NULL
            FOR UPDATE;
            
         UPDATE T4081_DHR_BULK_RECEIVE
        SET C4081_STAGE_DATE= p_stage_dt, C4081_LAST_UPDATED_BY = p_user_id, C4081_LAST_UPDATED_DATE = CURRENT_DATE
          WHERE C4081_DHR_BULK_ID = p_txn_id
            AND c4081_void_fl IS NULL;
    END IF;
    
    EXCEPTION WHEN OTHERS
     THEN
    v_txn_id:=NULL;
	 	
END gm_sav_stage_date; 

END gm_pkg_op_donor_inv_allocation;
/