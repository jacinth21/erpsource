create or replace
PACKAGE BODY gm_pkg_op_thb_lock_bck_ord_txn
IS
    /**********************************************************************************************
    *Description : Main Wrap-Up procedure which will be invoked during Lock and Generate from the Analyis Dashboard
    * author : GPALANI
    * Ref # : PMT-32710
    * Date: May 2019
    *************************************************************************************************/
    
	PROCEDURE gm_thb_load_lock_master_main (
	        	p_load_num_str 			IN CLOB,
	        	p_processing_start_date IN t2560_thb_load.c2560_thb_processed_date%TYPE,
	        	p_processing_end_date   IN t2560_thb_load.c2560_thb_processed_date%TYPE,
	        	p_user_id 				IN t101_user.c101_user_id%TYPE,
	        	p_company_id			IN t5041_plant_company_mapping.c1900_company_id%TYPE,
	        	p_plant_id				IN t5041_plant_company_mapping.c5040_plant_id%TYPE
	        	)
	AS
	    v_date_fmt				t906_rules.c906_rule_value%TYPE;
	    v_load_lock_master      t2560_thb_load.c2570_thb_load_lock_master_id%TYPE;
	    v_load_string           CLOB := p_load_num_str;
	    v_load_num_status_flag  t906_rules.c906_rule_value%TYPE;
	    v_email_to 				t906_rules.c906_rule_value%TYPE;
	    v_email_msg 			VARCHAR2 (2000);
	    v_email_sub 			VARCHAR2 (2000);
	    v_load_number 			t2560_thb_load.c2560_thb_load_num%TYPE;
	BEGIN
	
	-- Assigning the company id and Plant id to the global variable so that the company id and plant id can be accessible in all the procedure.
	
		v_company_id  := p_company_id;
		v_plant_id	  := p_plant_id;
		
	     SELECT  get_compdtfmt_frm_cntx
	       INTO  v_date_fmt
	       FROM DUAL;
	
	    WHILE INSTR (v_load_string, '|') <> 0
   		  LOOP
		  
        v_load_number := TO_NUMBER(SUBSTR (v_load_string, 1, INSTR (v_load_string, '|') - 1)) ;
        v_load_string := SUBSTR (v_load_string, INSTR (v_load_string, '|')    + 1) ;
	
	        -- Call procedure to check if there are any lock generated for the same Load number
	        -- This is avoid multiple Lock Generated for same Load Number
	
	       gm_pkg_op_thb_lock_bck_ord_txn.gm_chk_thb_load_status(v_load_number,v_load_num_status_flag);
	       
	       -- Company and Plant id is hard coded since the Primary table (t2560_thb_load) is not having the company id stored.
	       gm_pkg_cor_client_context.gm_sav_client_context (v_company_id, v_plant_id) ;
	
	
	        IF(v_load_num_status_flag IS NULL) THEN
	
		        gm_create_thb_load_lock_master (p_processing_start_date, p_processing_end_date, p_user_id, v_load_lock_master);
	
		        -- Call Procedure to Update the Master Lock ID in THB Main Load.
		        gm_update_thb_load_num (v_load_lock_master, v_load_number, p_user_id) ;
	
		        -- Call Procedure to fetch and lock open AB Order txx and PTRD Transactions
		        gm_lock_bck_ord_ptrd_txn (v_load_lock_master, p_user_id) ;
	
		        gm_fetch_thb_load_lock_details (v_load_lock_master, p_user_id) ;
		
		   END IF;
	     END LOOP;   
		 
		gm_pkg_op_thb_analysis_rpt.gm_upd_lock_status(p_load_num_str,p_user_id,'107880'); -- THB Load Initiated
		commit;
		
	-- Commit is added so that if there is an issue with the below email failure the lock will be created for the selected Load Numbers
		
		SELECT get_rule_value('LOCK_EMAIL_TO','LOCK_EMAIL') 
		    INTO v_email_to FROM DUAL;
		
		SELECT get_rule_value('LOCK_EMAIL_SUB','LOCK_EMAIL') 
		    INTO v_email_sub FROM DUAL;
		            
		SELECT get_rule_value('LOCK_EMAIL_TEXT','LOCK_EMAIL') 
		    INTO v_email_msg FROM DUAL;
		
		v_email_msg:=v_email_msg || ':'||p_load_num_str||'. <br>';
		
		-- Call the email procedure to send email to the user
		gm_com_send_html_email(v_email_to,v_email_sub,NULL,v_email_msg);
		
	EXCEPTION WHEN OTHERS
      THEN 
	  
	  raise_application_error(-20999, SQLERRM);

    	  
	END gm_thb_load_lock_master_main;


	/******************************************************************
	* Description : Check if the Load Number already has a Lock generated
	* Author   : GPALANI
	****************************************************************/
	PROCEDURE gm_chk_thb_load_status(
		        p_thb_load_num      IN  t2560_thb_load.c2560_thb_load_num%TYPE,
		        p_thb_load_status   OUT VARCHAR
		)
	AS
	    v_load_lock_count 	NUMBER;  -- declared as number since its a counter variable check
	BEGIN
		
	   	BEGIN
	       SELECT count (1) INTO v_load_lock_count FROM t2560_thb_load t2560
	       WHERE t2560.c2560_thb_load_num  = p_thb_load_num
	         AND t2560.c2570_thb_load_lock_master_id IS NOT NULL
	         AND t2560.c2560_void_fl IS NULL;
	         
		EXCEPTION WHEN OTHERS THEN
			v_load_lock_count := 0;
		END;
		
	        IF (v_load_lock_count> 0) THEN
	
	         p_thb_load_status:='Lock Generated';
	
	        END IF;
	
	END gm_chk_thb_load_status;

	/******************************************************************
	* Description : Create Analysis lock for the THB Load in t2570_thb_load_lock_master
	* Author   : GPALANI
	****************************************************************/
	PROCEDURE gm_create_thb_load_lock_master (
	        p_processing_start_date 	IN t2570_thb_load_lock_master.c2570_thb_process_start_date%TYPE,
	        p_processing_end_date   	IN t2570_thb_load_lock_master.c2570_thb_process_end_date%TYPE,
	        p_user_id 					IN t101_user.c101_user_id%TYPE,
	        p_load_lock_master_id 		OUT t2570_thb_load_lock_master.c2570_thb_load_lock_master_id%TYPE
	)
	AS
	    v_load_lock_seq t2570_thb_load_lock_master.c2570_thb_load_lock_master_id%TYPE ;
	    v_seq t2570_thb_load_lock_master.c2570_thb_load_lock_master_id%TYPE ;
	
	BEGIN
	   		SELECT S2570_THB_LOAD_LOCK_MASTER_ID.nextval
			  INTO v_seq
			  FROM DUAL;
		  
		  v_load_lock_seq := 'A' || v_seq;
	     	
	     INSERT
	       INTO t2570_thb_load_lock_master
	        (
	            c2570_thb_load_lock_master_id, c2570_thb_locked_date, c2570_thb_process_start_date
	          , c2570_thb_process_end_date, c901_status, c2570_created_by
	          , c2570_created_date
	        )
	        VALUES
	        (
	            v_load_lock_seq, current_date, p_processing_start_date
	          , p_processing_end_date, '108040', p_user_id -- 108040 analysis status: initiated
	          , current_date
	        ) ;
	
	   -- Returning the Lock ID to the Wrap-up procedure so that the Lock id will be passed to other procedure to map it.
	
	    p_load_lock_master_id := v_load_lock_seq;
	END gm_create_thb_load_lock_master;

	/******************************************************************
	* Description : Update Analysis lock ID for the chosen THB Load Number from the Analysis Dashboard
	* Author   : GPALANI
	****************************************************************/
	PROCEDURE gm_update_thb_load_num (
	            p_load_lock_master_id IN t2570_thb_load_lock_master.c2570_thb_load_lock_master_id%TYPE,
	            p_thb_load_num        IN t2560_thb_load.c2560_thb_load_num%TYPE,
	            p_user_id             IN t101_user.c101_user_id%TYPE) 
	AS
	
	v_load_lock_master_id  t2560_thb_load.c2570_thb_load_lock_master_id%TYPE;
	
	BEGIN
	
-- The for update clause is not used since the record is already locked

	         UPDATE t2560_thb_load
	        	SET c2570_thb_load_lock_master_id = p_load_lock_master_id, c2560_last_updated_by = p_user_id,
	            c2560_last_updated_date       = current_date
	          WHERE c2560_thb_load_num        = p_thb_load_num 
	          AND   c2560_void_fl IS NULL;
			  
		
	END gm_update_thb_load_num;
	/**********************************************************************************************
	* Description : Procedure to fetch the back order and PTRD transaction and load into t2572_thb_back_ord_lock table
	* author : GPALANI
	*************************************************************************************************/
	PROCEDURE gm_lock_bck_ord_ptrd_txn (
	            p_load_lock_master_id IN t2570_thb_load_lock_master.c2570_thb_load_lock_master_id%TYPE,
	            p_user_id             IN t101_user.c101_user_id%TYPE)
	AS
	    v_bo_txn_cursor TYPES.CURSOR_TYPE;
	
	BEGIN
	    /* Fetch Back Order Details from existing portal procedure
	    * V_BO_TXN_CURSOR is the out cursor from the existing procedure and not passing account id since we need all part
	    * Pass the out cursor to GM_FCH_BCK_ORD_TXN procedure and save the back order transaction
	    */
	    -- Fetch Open AB Transactions. Portal Procedure
	    gm_pkg_op_backorder_rpt.gm_fch_ord_ack_bo ('', v_bo_txn_cursor) ;
	
	    -- Save Open AB order Details
	   gm_pkg_op_thb_lock_bck_ord_txn.gm_fch_bck_ord_txn (p_load_lock_master_id, p_user_id, v_bo_txn_cursor) ;
	
	
	    -- Fetch and save PTRD Details
	    gm_pkg_op_thb_lock_bck_ord_txn.gm_fch_ptrd_ord_txn (p_load_lock_master_id, p_user_id) ;
	
	END gm_lock_bck_ord_ptrd_txn;

	/**********************************************************************************************
	* Description : Procedure to fetch ans save the back order load into t2572_thb_back_ord_lock table
	* author : GPALANI
	*************************************************************************************************/
	    PROCEDURE gm_fch_bck_ord_txn (
	            p_load_lock_master_id IN t2570_thb_load_lock_master.c2570_thb_load_lock_master_id%TYPE,
	            p_user_id             IN t101_user.c101_user_id%TYPE,
	            p_txn_detail_cursor   IN TYPES.CURSOR_TYPE)
	AS
	    v_part_num 		t205_part_number.c205_part_number_id%TYPE;
	    v_part_num_desc t205_part_number.c205_part_num_dtl_desc%TYPE;
	    v_acct_id 		t704_account.c704_account_id%TYPE;
	    v_acct_nm 		t704_account.c704_account_nm%TYPE;
	    v_ref_type    	t412_inhouse_transactions.c412_type%TYPE ;
	    v_ref_id      	t412_inhouse_transactions.c412_inhouse_trans_id%TYPE ;
	    v_req_date    	VARCHAR2(20); -- datatype is assigned as varchar since the PORTAL cursor data is returning varchar
	    v_pend_qty    	t413_inhouse_trans_items.c413_item_qty%TYPE;
	    v_shelf_qty   	t2571_thb_load_lock_details.c2571_shelf_qty%TYPE;
	    v_credit_type 	t901_code_lookup.c901_code_nm%TYPE;
	    v_ord_qty     	t2571_thb_load_lock_details.c2571_shelf_qty%TYPE;
	    v_ship_qty    	t2571_thb_load_lock_details.c2571_shelf_qty%TYPE;
	    v_cust_po 		t401_purchase_order.c401_purchase_ord_id%TYPE;
		v_date_fmt		t906_rules.c906_rule_value%TYPE;
	BEGIN
		SELECT  get_compdtfmt_frm_cntx
	    INTO  v_date_fmt
	    FROM DUAL;
	    BEGIN
	        LOOP
	   -- Fetch the out Cursor
	
	            FETCH p_txn_detail_cursor
	               INTO v_part_num, v_part_num_desc, v_acct_id
	              , v_acct_nm, v_cust_po, v_ref_id
	              , v_shelf_qty, v_ord_qty, v_ship_qty
	              , v_pend_qty, v_req_date, v_credit_type;
	            EXIT
	        WHEN p_txn_detail_cursor%NOTFOUND;
	            -- Below method will insert the Back Order Transaction
	           gm_pkg_op_thb_lock_bck_ord_txn.gm_sav_bck_ptrd_lock (p_load_lock_master_id, v_part_num, v_acct_id, '101260' -- 101260: back order type
	            , v_ref_id, v_req_date, v_pend_qty,p_user_id,current_date, p_user_id) ;
	        END LOOP;
	
	        CLOSE p_txn_detail_cursor;
	
	    EXCEPTION
	    WHEN OTHERS THEN
	        CLOSE p_txn_detail_cursor;
	        RAISE;
	
	    END;
	END gm_fch_bck_ord_txn;

	/**********************************************************************************************
	* Description : Procedure to fetch the PTRD transaction
	* author : GPALANI
	*************************************************************************************************/
    PROCEDURE gm_fch_ptrd_ord_txn (
            p_load_lock_master_id IN t2570_thb_load_lock_master.c2570_thb_load_lock_master_id%TYPE,
            p_user_id             IN t101_user.c101_user_id%TYPE
    ) 
	AS
	    CURSOR ptrd_txn
	    IS
	         SELECT t413.c205_client_part_number_id pnum, '' account_id, get_code_name (t412.c412_type) ref_type
	          , t412.c412_inhouse_trans_id ref_id, '' required_date, sum (t413.c413_item_qty) redegn_qty
	          , t412.c412_created_by created_by, t412.c412_created_date created_dte
	           FROM t412_inhouse_transactions t412, t413_inhouse_trans_items t413
	          WHERE c412_type                  = '103932' -- 103932: PTRD TRANSACTION TYPE(Part Re-designation)
	            AND c412_void_fl              IS NULL
	            AND t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
	            AND c412_verify_fl            IS NULL
	            AND c412_status_fl            <> 4
	            AND c412_update_inv_fl        IS NULL
	            AND c1900_company_id          = v_company_id
	            AND c5040_plant_id             = v_plant_id
	       GROUP BY t413.c205_client_part_number_id, t412.c412_type, t412.c412_inhouse_trans_id,t412.c412_created_by, t412.c412_created_date ;
	
	BEGIN

	    FOR v_ptrd_txn IN ptrd_txn
	
	    LOOP
	
	        gm_pkg_op_thb_lock_bck_ord_txn.gm_sav_bck_ptrd_lock (p_load_lock_master_id, v_ptrd_txn.pnum,
	        v_ptrd_txn.account_id, 103932, v_ptrd_txn.ref_id, v_ptrd_txn.required_date, v_ptrd_txn.redegn_qty,v_ptrd_txn.created_by,v_ptrd_txn.created_dte,p_user_id) ;
	
	    END LOOP;
	END gm_fch_ptrd_ord_txn;

	/**********************************************************************************************
	* Description : This Procedure to save the Back order and PTRD Transactions in the GM_SAV_BCK_PTRD_LOCK table.
	* The part numbers from this transactions will be used to determine the THB Analsis report
	* author : GPALANI
	*************************************************************************************************/
	PROCEDURE gm_sav_bck_ptrd_lock (
		        p_load_lock_master_id IN t2570_thb_load_lock_master.c2570_thb_load_lock_master_id%TYPE,
		        p_part_num            IN t205_part_number.c205_part_number_id%TYPE,
		        p_acct_id             IN t704_account.c704_account_id%TYPE,
		        p_ref_type            IN t412_inhouse_transactions.c412_type%TYPE,
		        p_ref_id              IN t412_inhouse_transactions.c412_inhouse_trans_id%TYPE,
		        p_req_date            IN VARCHAR2, -- datatype is assigned as varchar since the cursor data is returning varchar
		        p_pend_qty            IN t413_inhouse_trans_items.c413_item_qty%TYPE,
		        p_created_by          IN t412_inhouse_transactions.c412_created_by%TYPE,
		        p_created_date        IN t412_inhouse_transactions.c412_created_date%TYPE,
		        p_user_id             IN t101_user.c101_user_id%TYPE
	)
	AS
	v_date_fmt		t906_rules.c906_rule_value%TYPE;
	BEGIN
		select  get_compdtfmt_frm_cntx
	    INTO  v_date_fmt
	    from dual;
	    
	     INSERT
	       INTO t2572_thb_txn_lock
	        (
	            c2572_thb_txn_lock_id, c2570_thb_load_lock_master_id, c205_part_number_id
	          , c704_account_id, c901_ref_type, c2572_ref_id
	          , c2572_required_date, c2572_qty, c2572_created_by
	          , c2572_created_date
	        )
	        VALUES
	        (
	            S2572_THB_TXN_LOCK_ID.NEXTVAL, p_load_lock_master_id, p_part_num
	          , p_acct_id, p_ref_type, p_ref_id
	          , to_date(p_req_date,v_date_fmt), p_pend_qty, p_created_by
	          , p_created_date
	        ) ;
	
	END gm_sav_bck_ptrd_lock;

	/**********************************************************************************************
	* Description : This Procedure to load the back_order parts, qty and available inventory qty
	* author : GPALANI
	*************************************************************************************************/
	
	PROCEDURE gm_fetch_thb_load_lock_details (
	            p_load_lock_master_id IN t2570_thb_load_lock_master.c2570_thb_load_lock_master_id%TYPE,
	            p_user_id             IN t101_user.c101_user_id%TYPE)
	AS
	
	BEGIN
	
	    -- Fetch and Save Back Order Parts
	    gm_pkg_op_thb_lock_bck_ord_txn.gm_fetch_back_order_parts (p_load_lock_master_id, p_user_id) ;
	
	    -- Fetch and Save Rad Run Parts
	    gm_pkg_op_thb_lock_bck_ord_txn.gm_fetch_rad_run_parts (p_load_lock_master_id, p_user_id) ;
	
	    -- Fetch and three months sales back order parts
	
	    gm_pkg_op_thb_lock_bck_ord_txn.gm_fetch_three_months_sales_parts (p_load_lock_master_id, p_user_id) ;
	
	    -- Fetch and insert the base parts in private column
	    gm_pkg_op_thb_lock_bck_ord_txn.gm_fetch_base_part(p_load_lock_master_id, p_user_id) ;
	
	    -- Load the parts in temp table for inventory check
	    gm_pkg_op_thb_lock_bck_ord_txn.gm_save_my_temp_part(p_load_lock_master_id);
	
	    -- Fetch and Save Back Order Qty
	    gm_pkg_op_thb_lock_bck_ord_txn.gm_fetch_back_order_qty (p_load_lock_master_id, p_user_id) ;
	
	    -- Fetch and Rad Run Qty
	    gm_pkg_op_thb_lock_bck_ord_txn.gm_fetch_rad_run_qty (p_load_lock_master_id, p_user_id) ;
	
	    -- Fetch Three Month Sales qty
	    gm_pkg_op_thb_lock_bck_ord_txn.gm_fetch_three_months_sales_qty (p_load_lock_master_id, p_user_id) ;
	
	       -- Fetch PTRD Qty
	    gm_pkg_op_thb_lock_bck_ord_txn.gm_fetch_ptrd_qty (p_load_lock_master_id, p_user_id) ;
	
	    -- Fetch Inventory Qty
	    gm_pkg_op_thb_lock_bck_ord_txn.gm_fetch_inv_qty (p_load_lock_master_id, p_user_id) ;
	
	    -- Fetch Need Qty
	    gm_pkg_op_thb_lock_bck_ord_txn.gm_fetch_needed_qty (p_load_lock_master_id, p_user_id) ;
	
	END gm_fetch_thb_load_lock_details;


	/**********************************************************************************************
	* Description : This Procedure to load the back_order parts in temp part list to fetch inventory qty
	* author : GPALANI
	*************************************************************************************************/
	PROCEDURE gm_save_my_temp_part
	    (
	        p_load_lock_master_id IN t2570_thb_load_lock_master.c2570_thb_load_lock_master_id%TYPE
	     
	    )
	AS
	
	BEGIN
	   DELETE FROM my_temp_part_list;
	
		 INSERT INTO my_temp_part_list
		    (c205_part_number_id
		    )
		   SELECT DISTINCT C205_PVT_LBL_PART_NUMBER_ID FROM t2571_thb_load_lock_details WHERE c2570_thb_load_lock_master_id =p_load_lock_master_id
		   AND c2571_void_fl IS NULL;
	
	
	END gm_save_my_temp_part;

	/**********************************************************************************************
	* Description : This Procedure to load the back_order parts in Lock Detail table
	* author : GPALANI
	*************************************************************************************************/
	PROCEDURE gm_fetch_back_order_parts (
	            p_load_lock_master_id IN t2570_thb_load_lock_master.c2570_thb_load_lock_master_id%TYPE,
	            p_user_id             IN t101_user.c101_user_id%TYPE)
	AS
	    CURSOR ack_order
	    IS
	        -- Back Order Part Loading
		         SELECT NVL (base_part_att.base_part, ack.pnum) base_part, NVL(base_part_att.private_part,ack.pnum) private_part
		           FROM
		            (
		                SELECT DISTINCT t2572.c205_part_number_id pnum
		                    --T205D. C205_PART_NUMBER_ID BASE_PART, T205D.C205D_ATTRIBUTE_VALUE PRIVATE_PART
		                   FROM t2572_thb_txn_lock t2572
		                  WHERE t2572.c901_ref_type                 = '101260' --Acknowledgement Order
		                    AND t2572.c2570_thb_load_lock_master_id = p_load_lock_master_id
		            )
		            -- Joining Part Attibute table to fetch Private Parts
		        ack, (
		             SELECT t205d.c205_part_number_id private_part, t205d.c205d_attribute_value base_part
		               FROM t205d_part_attribute t205d
		              WHERE t205d.c901_attribute_type    = 103730 --Master Product
		                AND t205d.c205d_attribute_value IS NOT NULL
		                AND T205D.C205D_ATTRIBUTE_VALUE <> 'N/A'
		                AND T205D.C205D_VOID_FL         IS NULL
		        )
		        base_part_att
		      WHERE ack.pnum = base_part_att.private_part (+);
	
	BEGIN
	    FOR v_ack_order_private IN ack_order
	
	    LOOP
	        gm_pkg_op_thb_lock_bck_ord_txn.gm_create_thb_load_lock_details (p_load_lock_master_id,
	        v_ack_order_private.base_part, v_ack_order_private.private_part, p_user_id) ;
	
	    END LOOP;
   
	END gm_fetch_back_order_parts;
	/**********************************************************************************************
	* Description : This Procedure to fetch the Rad run parts for the mapped Load lock master id
	* author : GPALANI
	*************************************************************************************************/
	PROCEDURE gm_fetch_rad_run_parts (
	            p_load_lock_master_id IN t2570_thb_load_lock_master.c2570_thb_load_lock_master_id%TYPE,
	            p_user_id             IN t101_user.c101_user_id%TYPE) 
	AS
	    CURSOR Rad_Run_Part
	    IS
	        -- Loading Rad run part into t2571_Lock_Detail table
	         SELECT DISTINCT t2561.c205_part_number_id pnum
	           FROM t2560_thb_load t2560, t2561_thb_load_item_detail t2561
	          WHERE t2560.c2560_thb_load_id   = t2561.c2560_thb_load_id
	            AND t2560.c2560_thb_load_num IN
	            (
	                 SELECT c2560_thb_load_num
	                   FROM t2560_thb_load
	                  WHERE c2570_thb_load_lock_master_id = p_load_lock_master_id
	                    AND t2560.c2560_void_fl          IS NULL
	            )
	        AND t2560.c2560_void_fl IS NULL
	   GROUP BY t2561.c205_part_number_id;
	
	BEGIN
	
	    FOR V_Rad_Run_Part IN Rad_Run_Part
	
	    LOOP
	        gm_pkg_op_thb_lock_bck_ord_txn.gm_create_thb_rad_run_load_lock_details (p_load_lock_master_id, v_rad_run_part.pnum,
	        NULL, p_user_id) ;
	    END LOOP;
	
	END gm_fetch_rad_run_parts;

	/**********************************************************************************************
	* Description : This Procedure to load the three months sales
	* author : GPALANI
	*************************************************************************************************/
	PROCEDURE gm_fetch_three_months_sales_parts (
	            p_load_lock_master_id IN t2570_thb_load_lock_master.c2570_thb_load_lock_master_id%TYPE,
	            p_user_id             IN t101_user.c101_user_id%TYPE)
	AS
	
	v_base_part_num 	t205d_part_attribute.c205d_attribute_value%TYPE;
	v_private_part_num 	t205d_part_attribute.c205_part_number_id%TYPE;

	
	    -- THREE_MONTHS_SALES update
	 CURSOR three_months_sales
	    IS
	  SELECT DISTINCT NVL(base_part_att.base_part,threemonsales.pnum) base_part, NVL(base_part_att.private_part,threemonsales.pnum) private_part
	   FROM
	    (
	         SELECT t502.c205_part_number_id pnum, SUM
	            (t502.c502_item_price * t502.c502_item_qty) sales
	           FROM t501_order t501, t502_item_order T502
	          WHERE t501.c501_order_id                      = t502.c501_order_id
	            AND NVL (t501.c901_order_type,     - 9999) NOT IN ('2533', '2518', '2519', '101260')
	            AND TRUNC (t501.c501_order_date)           >= CURRENT_DATE - 90
	            AND TRUNC (t501.c501_order_date)            < CURRENT_DATE
	            AND t501.c501_void_fl                      IS NULL
	            AND t501.c501_delete_fl                    IS NULL
	            AND t501.c1900_company_id                   = v_company_id
	            AND t502.c502_void_fl   IS NULL
	            AND t502.c502_delete_fl IS NULL
	       GROUP BY t502.c205_part_number_id
	    ) threemonsales
	
	
	   -- Joining Part Attibute table to fetch Private Parts
		        , (
		             SELECT t205d.c205_part_number_id private_part, t205d.c205d_attribute_value base_part
		               FROM t205d_part_attribute t205d
		              WHERE t205d.c901_attribute_type    = 103730
		                AND t205d.c205d_attribute_value IS NOT NULL
		                AND t205d.c205d_attribute_value <> 'N/A'
		                AND t205d.c205d_void_fl         IS NULL
		        ) base_part_att
	      WHERE threemonsales.pnum=base_part_att.private_part (+)
	      AND threemonsales.sales>0;
	
	BEGIN
	
	
	    FOR v_three_months_sales IN three_months_sales
	
	    LOOP
	
	        v_base_part_num:=v_three_months_sales.base_part;
	        v_private_part_num:=v_three_months_sales.private_part;
	
	
	       UPDATE t2571_thb_load_lock_details
			SET c2571_last_updated_by             	= p_user_id, 
				c2571_last_updated_date = current_date
	 		 WHERE c2570_thb_load_lock_master_id 	= p_load_lock_master_id
	   		 AND c205_base_part_number  			= v_base_part_num
	   		 AND c205_pvt_lbl_part_number_id    	= v_private_part_num
	   		 AND c2571_void_fl                IS NULL;
	
		      IF (SQL%ROWCOUNT = 0) THEN
	
	         INSERT
	           INTO t2571_thb_load_lock_details
	            (
	                c2571_thb_load_lock_details_id, c2570_thb_load_lock_master_id, c205_base_part_number
	              , c205_pvt_lbl_part_number_id,  c2571_created_by, c2571_created_date
	            )
	            VALUES
	            (
	                S2571_THB_LOAD_LOCK_DETAILS_ID.NEXTVAL, p_load_lock_master_id, v_base_part_num
	              , v_private_part_num, p_user_id, current_date
	            ) ;
		 END IF;
	    END LOOP;
	
	END gm_fetch_three_months_sales_parts;			


	/**********************************************************************************************
	* Description : This Procedure to load base part entry separately.
	* This is needed if there is no back order for base part but we need to show a separate entry so that we can find the main inventory values for base part
	* author : GPALANI
	*************************************************************************************************/
	PROCEDURE gm_fetch_base_part (
		        p_load_lock_master_id IN t2570_thb_load_lock_master.c2570_thb_load_lock_master_id%TYPE,
		        p_user_id             IN t101_user.c101_user_id%TYPE)
	AS
	
	CURSOR base_part_entry
	 IS
	
	        SELECT c205_base_part_number base_part
	   FROM
	    (
	         SELECT DISTINCT t2571_private.c205_base_part_number, SUM (DECODE (t2571_base.c205_base_part_number, NULL, 0, 1))
	            base_part_count
	           FROM t2571_thb_load_lock_details t2571_private, t2571_thb_load_lock_details t2571_base
	          WHERE t2571_private.c2570_thb_load_lock_master_id = p_load_lock_master_id
	            AND t2571_private.c2570_thb_load_lock_master_id = t2571_base.c2570_thb_load_lock_master_id (+)
	            AND t2571_private.c205_pvt_lbl_part_number_id   = t2571_base.c205_base_part_number (+)
	            AND t2571_private.c2571_void_fl is null
	       GROUP BY t2571_private.c205_base_part_number
	    )
	  WHERE base_part_count        = 0
	    AND c205_base_part_number IS NOT NULL;
	
	  BEGIN
	
	    FOR v_base_part_entry IN base_part_entry
	
	      LOOP
	
	         UPDATE t2571_thb_load_lock_details
			SET c2571_last_updated_by             	= p_user_id
				, c2571_last_updated_date 			= current_date
	 		 WHERE c2570_thb_load_lock_master_id 	= p_load_lock_master_id
	   		 AND c205_pvt_lbl_part_number_id    	= v_base_part_entry.base_part
	   		 AND c2571_void_fl                IS NULL;
	
	       IF (SQL%ROWCOUNT = 0) THEN
	
	         INSERT
	           INTO t2571_thb_load_lock_details
	            (
	                c2571_thb_load_lock_details_id, c2570_thb_load_lock_master_id, c205_base_part_number
	              , c205_pvt_lbl_part_number_id,  c2571_created_by, c2571_created_date
	            )
	            VALUES
	            (
	                S2571_THB_LOAD_LOCK_DETAILS_ID.NEXTVAL, p_load_lock_master_id, v_base_part_entry.base_part
	              , v_base_part_entry.base_part, p_user_id, current_date
	            ) ;
	       END IF;
	      END LOOP;
	 END gm_fetch_base_part;
	/**********************************************************************************************
	* Description : This Procedure to Fetch and save back order qty, 2 weeks due, 2 weeks after qty in T2571_THB_LOAD_LOCK_DETAILS table
	* author : GPALANI
	*************************************************************************************************/
	PROCEDURE gm_fetch_back_order_qty (
	            p_load_lock_master_id IN t2570_thb_load_lock_master.c2570_thb_load_lock_master_id%TYPE,
	            p_user_id             IN t101_user.c101_user_id%TYPE)
	AS
	
	v_back_order_qty 		t2571_thb_load_lock_details.c2571_back_ord_qty%TYPE;
	v_pend_2_weeks 			t2571_thb_load_lock_details.c2571_pend_in_two_weeks%TYPE;
	v_pend_after_2_weeks 	t2571_thb_load_lock_details.c2571_pend_after_two_weeks%TYPE;
	v_part_num 				t2571_thb_load_lock_details.c205_pvt_lbl_part_number_id%TYPE;
	
	    -- Back Order Part Loading
	    CURSOR back_order
	    IS
	         SELECT pnum, SUM (back_order_qty) Back_Order_Qty, SUM (pending_in_2_weeks) pend_2_weeks
	          , SUM (Pending_After_2_weeks) pend_greater_2_weeks
	           FROM
	            (
	                 SELECT c205_part_number_id pnum, (
	                    CASE
	                        when to_date (c2572_required_date) <= TRUNC (CURRENT_DATE)
	                        THEN c2572_qty
	                        else 0
	                    END) back_order_qty, (
	                    case
	                        when to_date (c2572_required_date) >TRUNC (CURRENT_DATE) and to_date (c2572_required_date)<=(TRUNC (CURRENT_DATE) + 14)
	                        THEN c2572_qty
	                        else 0
	                    END) pending_in_2_weeks, (
	                    case
	                        when to_date (c2572_required_date) > TRUNC ((CURRENT_DATE) + 14) and to_date (c2572_required_date) <=(TRUNC (CURRENT_DATE) + 30)
	                        THEN c2572_qty
	                        else 0
	                    END) pending_after_2_weeks
	                   FROM t2572_thb_txn_lock ack_back_order
	                  WHERE ack_back_order.c2570_thb_load_lock_master_id = p_load_lock_master_id
	            )
	   GROUP BY pnum;
	
	BEGIN
	    FOR v_back_order IN back_order
	
	    LOOP
	
		v_back_order_qty	:=v_back_order.back_order_qty;
		v_pend_2_weeks 		:=v_back_order.pend_2_weeks;
		v_pend_after_2_weeks :=v_back_order.pend_greater_2_weeks;
		v_part_num			:=v_back_order.pnum;
	
		UPDATE t2571_thb_load_lock_details
		SET c2571_back_ord_qty              = NVL (v_back_order_qty, 0),
		  c2571_pend_in_two_weeks           = NVL (v_pend_2_weeks, 0),
		  c2571_pend_after_two_weeks        = NVL (v_pend_after_2_weeks, 0),
		  c2571_last_updated_by             = p_user_id,
		  c2571_last_updated_date           = CURRENT_DATE
		WHERE c2570_thb_load_lock_master_id = p_load_lock_master_id
		AND c205_pvt_lbl_part_number_id     = v_part_num
		AND c2571_void_fl                  IS NULL;
	
	
	    END LOOP;
	
	END gm_fetch_back_order_qty;
	/**********************************************************************************************
	* Description : This Procedure to load the Rad Run Qty
	* author : GPALANI
	*************************************************************************************************/
	PROCEDURE gm_fetch_rad_run_qty (
	            p_load_lock_master_id IN t2570_thb_load_lock_master.c2570_thb_load_lock_master_id%TYPE,
	            p_user_id             IN t101_user.c101_user_id%TYPE)
	AS
	
	v_rad_run_qty 	t2571_thb_load_lock_details.c2571_rad_run_qty%TYPE;
	v_part_num 		t2571_thb_load_lock_details.c205_pvt_lbl_part_number_id%TYPE;
	
	    -- Rad Run Qty update
	    CURSOR rad_run_qty_update
	    IS
	         SELECT t2561.c205_part_number_id pnum, COUNT (t2561.c2561_control_number) rad_run_qty
	           FROM t2560_thb_load t2560, t2561_thb_load_item_detail t2561
	          WHERE t2560.c2560_thb_load_id   = t2561.c2560_thb_load_id
	            AND t2560.c2560_thb_load_num IN
	            (
	                 SELECT thb_load.c2560_thb_load_num
	                   FROM t2560_thb_load thb_load
	                  WHERE thb_load.c2570_thb_load_lock_master_id = p_load_lock_master_id
	                  AND  thb_load.c2560_void_fl IS NULL
	            )
	        AND t2560.c2560_void_fl IS NULL
	   GROUP BY t2561.c205_part_number_id;
	
	BEGIN
	
	    FOR v_rad_run_qty_update IN rad_run_qty_update
	
	    LOOP
	        v_rad_run_qty	:=v_rad_run_qty_update.rad_run_qty;
	        v_part_num		:=v_rad_run_qty_update.pnum;
	
			UPDATE t2571_thb_load_lock_details
			SET c2571_rad_run_qty               = NVL (v_rad_run_qty, 0),
			  c2571_last_updated_by             = p_user_id,
			  c2571_last_updated_date           = CURRENT_DATE
			WHERE c2570_thb_load_lock_master_id = p_load_lock_master_id
			AND c205_pvt_lbl_part_number_id     = v_part_num
			AND c2571_void_fl                  IS NULL;
	
	    END LOOP;
	END gm_fetch_rad_run_qty;


	/**********************************************************************************************
	* Description : This Procedure to load the three months sales qty
	* author : GPALANI
	*************************************************************************************************/
	PROCEDURE gm_fetch_three_months_sales_qty (
		        p_load_lock_master_id IN t2570_thb_load_lock_master.c2570_thb_load_lock_master_id%TYPE,
		        p_user_id             IN t101_user.c101_user_id%TYPE)
	AS
	
	v_three_month_sales_qty t2571_thb_load_lock_details.c2571_avg_three_months_sale%TYPE;
	v_part_num 				t2571_thb_load_lock_details.c205_pvt_lbl_part_number_id%TYPE;

	
	    -- THREE_MONTHS_SALES update
	    CURSOR three_months_sales_qty
	    IS
	
	   SELECT c205_part_number_id pnum, CEIL (item_qty / 3) three_month_sale_qty
	   FROM
	    (
	         SELECT t502.c205_part_number_id, SUM (DECODE (c901_order_type, 2524, NULL, t502.c502_item_qty)) item_qty, SUM
	            (t502.c502_item_price * t502.c502_item_qty) sales
	           FROM t501_order t501, t502_item_order T502, t2571_thb_load_lock_details T2571
	          WHERE t501.c501_order_id                      = t502.c501_order_id
	            AND NVL (t501.c901_order_type, - 9999) NOT IN ('2533', '2518', '2519','101260')
	            AND TRUNC (t501.c501_order_date)           >= CURRENT_DATE - 90
	            AND TRUNC (t501.c501_order_date)            < CURRENT_DATE
	            AND t501.c501_void_fl                      IS NULL
	            AND t501.c501_delete_fl                    IS NULL
	            AND t501.c1900_company_id                   = v_company_id
	            AND t502.c205_part_number_id=t2571.c205_pvt_lbl_part_number_id
	            AND t2571.c2570_thb_load_lock_master_id = p_load_lock_master_id
	            AND t502.c502_void_fl   IS NULL
	            AND t502.c502_delete_fl IS NULL
	       GROUP BY t502.c205_part_number_id
	    )
	  WHERE sales > 0;
	
	BEGIN
	
	    FOR v_three_months_sales IN three_months_sales_qty
	
	    LOOP
	        v_three_month_sales_qty	:=v_three_months_sales.three_month_sale_qty;
	        v_part_num				:=v_three_months_sales.pnum;
	
			UPDATE t2571_thb_load_lock_details
			SET c2571_avg_three_months_sale     = NVL (v_three_month_sales_qty, 0),
			  c2571_last_updated_by             = p_user_id,
			  c2571_last_updated_date           = CURRENT_DATE
			WHERE c2570_thb_load_lock_master_id = p_load_lock_master_id
			AND c205_pvt_lbl_part_number_id     = v_part_num
			AND c2571_void_fl                  IS NULL;
	
	    END LOOP;
	
	END gm_fetch_three_months_sales_qty;


	/**********************************************************************************************
	* Description : This Procedure to load the PTRD QTY
	* author : GPALANI
	*************************************************************************************************/
	PROCEDURE gm_fetch_ptrd_qty (
	            p_load_lock_master_id IN t2570_thb_load_lock_master.c2570_thb_load_lock_master_id%TYPE,
	            p_user_id             IN t101_user.c101_user_id%TYPE)
	AS
	
	v_ptrdqty 	t2572_thb_txn_lock.c2572_qty%TYPE;
	v_part_num 	t2572_thb_txn_lock.c205_part_number_id%TYPE;
	    -- PTRD QTY
	    CURSOR ptrd_qty
	    IS
	         SELECT t2572a.c205_part_number_id pnum, sum (t2572a.c2572_qty) ptrd_qty
	           FROM t2572_thb_txn_lock t2572a
	          WHERE t2572a.c901_ref_type                 = '103932' -- PTRD transaction type
	            AND t2572a.c2570_thb_load_lock_master_id = p_load_lock_master_id
	            AND t2572a.c2572_void_fl                IS NULL
	       GROUP BY t2572a.c205_part_number_id;
	BEGIN
	
	    FOR v_ptrd_qty IN ptrd_qty
	
	    LOOP
	        v_ptrdqty	:=v_ptrd_qty.ptrd_qty;
	        v_part_num	:=v_ptrd_qty.pnum;
	
			UPDATE t2571_thb_load_lock_details
			SET c2571_redesignated_qty          = NVL (v_ptrdqty, 0),
			  c2571_last_updated_by             = p_user_id,
			  c2571_last_updated_date           = CURRENT_DATE
			WHERE c2570_thb_load_lock_master_id = p_load_lock_master_id
			AND c205_pvt_lbl_part_number_id     = v_part_num
			AND c2571_void_fl                  IS NULL;
	
	     END LOOP;
	END gm_fetch_ptrd_qty;
	/**********************************************************************************************
	* Description : This Procedure to load the Shelf QTY
	* author : GPALANI
	*************************************************************************************************/
	PROCEDURE gm_fetch_inv_qty (
		        p_load_lock_master_id IN t2570_thb_load_lock_master.c2570_thb_load_lock_master_id%TYPE,
		        p_user_id             IN t101_user.c101_user_id%TYPE)
	AS
	    v_part_num       t2572_thb_txn_lock.c205_part_number_id%TYPE;
	    v_shelf_qty      t2571_thb_load_lock_details.c2571_shelf_qty%TYPE;
	    v_dhr_qty        t2571_thb_load_lock_details.c2571_dhr_qty%TYPE;
	    v_repack_inv_qty t2571_thb_load_lock_details.c2571_repack_qty%TYPE;
	
	    CURSOR inv_qty
	    IS
	         SELECT c205_part_number_id pnum, NVL (c205_repackavail, 0) RE_PACK_QTY, NVL (c205_inshelf, 0) shelf_qty
	          , c205_dhrqty dhr_qty
	           FROM v205_qty_in_stock;
	
	BEGIN
	
	
	    my_context.set_my_inlist_ctx ('70018,70002,70003,70019,70005,70007,70023,70006,70008,70004,70014,70022,70009,70013,70020,70001') ;
	
	    gm_pkg_cor_client_context.gm_sav_client_context (v_company_id, v_plant_id) ;
	
	
	    FOR v_inv_qty IN inv_qty
	
	    LOOP
	        v_shelf_qty      := v_inv_qty.shelf_qty;
	        v_dhr_qty        := v_inv_qty.dhr_qty;
	        v_repack_inv_qty := v_inv_qty.re_pack_qty;
	        v_part_num       := v_inv_qty.pnum;

			UPDATE t2571_thb_load_lock_details
			SET c2571_shelf_qty                 = NVL (v_shelf_qty, 0),
			  c2571_dhr_qty                     = NVL (v_dhr_qty, 0),
			  c2571_repack_qty                  = NVL ( v_repack_inv_qty, 0),
			  c2571_last_updated_by             = p_user_id,
			  c2571_last_updated_date           = CURRENT_DATE
			WHERE c2570_thb_load_lock_master_id = p_load_lock_master_id
			AND c205_pvt_lbl_part_number_id     = v_part_num
			AND c2571_void_fl                  IS NULL;
	
	    END LOOP;
	
	END gm_fetch_inv_qty;

	/**********************************************************************************************
	* Description : This Procedure to Fetch the needed qty
	* author : GPALANI
	*************************************************************************************************/
	 PROCEDURE gm_fetch_needed_qty (
	            p_load_lock_master_id IN t2570_thb_load_lock_master.c2570_thb_load_lock_master_id%TYPE,
	            p_user_id             IN t101_user.c101_user_id%TYPE
	)
	AS
	    -- REPACK_INV_QTY QTY
	    CURSOR Need_Qty
	    IS
	          SELECT lock_detail_id, need_qty
	           FROM
	            (
	                 SELECT t2571_thb_load_lock_details.c2571_thb_load_lock_details_id lock_detail_id, nvl(c2571_back_ord_qty,0) -
	                    ( ( nvl(c2571_shelf_qty,0)                   +
	                    NVL(c2571_dhr_qty,0)                         +
	                    NVL(c2571_redesignated_qty ,0)               +
	                    NVL(c2571_repack_qty,0))) need_qty
	
	                   FROM t2571_thb_load_lock_details
	                  WHERE c2570_thb_load_lock_master_id =p_load_lock_master_id
	                    AND c2571_void_fl                IS NULL
	         )
	      WHERE need_qty > 0;
	
	BEGIN
	
	
	    FOR V_Need_Qty IN Need_Qty
	
	    LOOP
	        gm_pkg_op_thb_lock_bck_ord_txn.gm_update_need_qty (v_need_qty.lock_detail_id, v_need_qty.need_qty, p_user_id) ;
	    END LOOP;
	
	END gm_fetch_needed_qty;
	/**********************************************************************************************
	* Description : This Procedure to load the back_order parts in T2571_THB_LOAD_LOCK_DETAILS
	* author : GPALANI
	*************************************************************************************************/
	PROCEDURE gm_create_thb_load_lock_details (
	            p_load_lock_master_id IN t2570_thb_load_lock_master.c2570_thb_load_lock_master_id%TYPE,
	            p_base_part 		  IN t205_part_number.c205_part_number_id%TYPE,
	            p_private_part 		  IN t205_part_number.c205_part_number_id%TYPE,
	            p_user_id 			  IN t101_user.c101_user_id%TYPE
	)
	AS
	
	BEGIN
	

	        INSERT
	           INTO t2571_thb_load_lock_details
	            (
	                c2571_thb_load_lock_details_id, c2570_thb_load_lock_master_id, c205_base_part_number
	              , c205_pvt_lbl_part_number_id, c2571_created_by, c2571_created_date
	            )
	            VALUES
	            (
	                s2571_thb_load_lock_details_id.nextval, p_load_lock_master_id, p_base_part
	              , NVL (p_private_part, p_base_part), p_user_id, current_date
	            ) ;
	
	END gm_create_thb_load_lock_details;

  /*************************************************************************************************
     * Description : This Procedure to load the Rad Run parts into  T2571_THB_LOAD_LOCK_DETAILS
    * author : GPALANI
    *************************************************************************************************/

	PROCEDURE gm_create_thb_rad_run_load_lock_details (
		        p_load_lock_master_id IN t2570_thb_load_lock_master.c2570_thb_load_lock_master_id%TYPE,
		        p_base_part 		  IN t205_part_number.c205_part_number_id%TYPE,
		        p_private_part 		  IN t205_part_number.c205_part_number_id%TYPE,
		        p_user_id 			  IN t101_user.c101_user_id%TYPE
	)
	AS
	
	BEGIN
	    -- This Update script is to avoid duplicate part numbers when loading the Rad Run Part numbers
		-- Updating the condition to always check for private part since if the THB mistakenly produced for Private part
		-- This will result in an error.
		UPDATE t2571_thb_load_lock_details
		SET c2571_last_updated_by           = p_user_id ,
		  c2571_last_updated_date           = CURRENT_DATE
		WHERE c2570_thb_load_lock_master_id = p_load_lock_master_id
		AND c205_pvt_lbl_part_number_id     = p_base_part
		AND c2571_void_fl                  IS NULL;   

	
	    IF (SQL%ROWCOUNT = 0) THEN
		
	         INSERT
	           INTO t2571_thb_load_lock_details
	            (
	                c2571_thb_load_lock_details_id, c2570_thb_load_lock_master_id, c205_base_part_number
	              , c205_pvt_lbl_part_number_id, c2571_created_by, c2571_created_date
	            )
	            VALUES
	            (
	                S2571_THB_LOAD_LOCK_DETAILS_ID.NEXTVAL, p_load_lock_master_id, p_base_part
	              , p_base_part, p_user_id, current_date
	            ) ;
	    END IF;
		
		EXCEPTION WHEN OTHERS
         THEN 	  
	  raise_application_error(-20999, SQLERRM||p_base_part);

	END gm_create_thb_rad_run_load_lock_details;

	/**********************************************************************************************
	* Description : This Procedure to update the Need qty
	* author : GPALANI
	*************************************************************************************************/
	PROCEDURE gm_update_need_qty (
	          p_load_lock_detail_id IN t2570_thb_load_lock_master.c2570_thb_load_lock_master_id%TYPE,
	          p_need_qty            IN t2571_thb_load_lock_details.c2571_qty_needed%TYPE,
	          p_user_id             IN t101_user.c101_user_id%TYPE)
	AS
	v_load_lock_detail_id	t2570_thb_load_lock_master.c2570_thb_load_lock_master_id%TYPE;
	BEGIN
		SELECT c2571_thb_load_lock_details_id
		INTO v_load_lock_detail_id
		FROM t2571_thb_load_lock_details
		WHERE c2571_thb_load_lock_details_id = p_load_lock_detail_id
		AND c2571_void_fl                   IS NULL FOR UPDATE;
		
		UPDATE t2571_thb_load_lock_details
		SET c2571_qty_needed                 = NVL(p_need_qty,0),
		  c2571_last_updated_by              = p_user_id,
		  c2571_last_updated_date            = CURRENT_DATE
		WHERE c2571_thb_load_lock_details_id = v_load_lock_detail_id
		AND c2571_void_fl                   IS NULL;
	END gm_update_need_qty;

END gm_pkg_op_thb_lock_bck_ord_txn;
/