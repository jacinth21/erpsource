CREATE OR REPLACE PACKAGE body gm_pkg_op_bba_staging_dash
AS
    /**************************************************************************************
    * description :  this procedure is used to fetch the staging dashboard transaction rs and ptrd.
    * author      :  gpalani
    * pmt#        :  pmt-36523
    * date        : 16-aug-2019
    ***************************************************************************************/
PROCEDURE gm_fch_staging_dash (
        p_out_stagin_dash_dtls OUT CLOB)
        
AS
    v_company_id t1900_company.c1900_company_id%type;
    v_date_fmt t906_rules.c906_rule_value%TYPE ;
    
BEGIN
    -- to get the date format
     SELECT get_compdtfmt_frm_cntx
       INTO v_date_fmt
       FROM dual;  
       
    -- to get the company info
     SELECT get_compid_frm_cntx ()
       INTO v_company_id
       FROM dual;
       
     SELECT JSON_ARRAYAGG (JSON_OBJECT ( 'log_msg' value staging_dash.log_msg, 'txn_id' value staging_dash.txn_id,
        'donor_number' value staging_dash.donor_number, 'load_number' value staging_dash.Load_Num ,'part_count' value staging_dash.part_count, 'qty_received'
        value staging_dash.qty_received, 'form_42' value staging_dash.form_42, 'labels_printed' value
        staging_dash.labels_printed, 'qc_verified' value staging_dash.qc_verified, 'PP' value staging_dash.pp, 'PI'
        value staging_dash.pi, 'PV' value staging_dash.pv, 'STAGE_DATE' value staging_dash.stagedate, 'PRE_STAGING'
        value staging_dash.pre_staging, 'Staging' value staging, 'CON_TYPE' value staging_dash.con_type, 'CTYPE' value
        staging_dash.ctype, 'STATUS_FL' value staging_dash.status, 'TXNTYPE' value staging_dash.txntype, 'DATE_ENTERED' value staging_dash.date_entered)
   ORDER BY staging_dash.stagedate ASC returning CLOB)
   
       INTO p_out_stagin_dash_dtls
       
       FROM
        (
        -- DHR Transaction Query
        -- RS Transaction does not have a type in t4081_dhr_bulk_receive table, so sending 9105 as Txn Type for the RS Transaction type
             SELECT get_log_flag (t4081.c4081_dhr_bulk_id, 4000697) log_msg, dhr.c4081_dhr_bulk_id txn_id, NULL
                con_type, 9105 ctype, gm_pkg_op_bulk_dhr_rpt.get_bulk_dhr_status (t4081.c4081_dhr_bulk_id) status
              , t2540.c2540_donor_number donor_number, t4081.c4081_packing_slip Load_Num,part_count, qty_received
              , NVL (v4081a.form_42, 'N') form_42, NVL (v4081a.labels_printed, 'N') labels_printed, NVL (
                v4081a.qc_verified, 'N') qc_verified, NVL (v4081a.label_pre_stage, 'N') pre_staging, NVL (
                v4081a.label_stage, 'N') staging, dhr.pi pi, dhr.pp pp
              , dhr.pv pv, TO_CHAR (t4081.c4081_stage_date, v_date_fmt) stagedate, 9105 txntype,
               dhr.date_entered date_entered
               FROM t4081_dhr_bulk_receive t4081, v4081a_dhr_bulk_attribute v4081a, t2540_donor_master t2540
              , (
                     SELECT c4081_dhr_bulk_id, COUNT (t408.c205_part_number_id) part_count, SUM (t408.c408_qty_received
                        ) qty_received, SUM (t408.c408_qty_rejected) qty_rejected, TO_CHAR (DECODE (t408.c408_status_fl
                        , 1, 'Y', '-')) pi, TO_CHAR (DECODE (t408.c408_status_fl, 2, 'Y', '-')) pp, TO_CHAR (DECODE (
                        t408.c408_status_fl, 3, 'Y', '-')) pv, to_char(TRUNC(t408.c408_created_date),v_date_fmt)date_entered
                       FROM t4082_bulk_dhr_mapping t4082, t408_dhr t408
                      WHERE t408.c408_dhr_id      = t4082.c408_dhr_id
                        AND t408.c408_status_fl  != 4
                        AND t4082.c4082_void_fl  IS NULL
                        AND t408.c408_void_fl    IS NULL
                        AND t408.c1900_company_id = v_company_id
                   GROUP BY c4081_dhr_bulk_id, t408.c408_status_fl ,TRUNC(t408.c408_created_date)
                )
                dhr
              WHERE t4081.c4081_dhr_bulk_id = v4081a.c4081_dhr_bulk_id (+)
                AND t4081.c2540_donor_id    = t2540.c2540_donor_id(+)
                AND t4081.c4081_dhr_bulk_id = dhr.c4081_dhr_bulk_id
                AND t4081.c1900_company_id  = v_company_id
                
          UNION ALL  
          ---- PTRD Transaction Query
          
             SELECT get_log_flag (t412.c412_inhouse_trans_id, 4000697)log_msg, t412.c412_inhouse_trans_id txn_id, NVL (get_rule_value ('con_type', c412_type), 'i')
                con_type, t412.c412_type ctype, t412.c412_status_fl status_fl
              , t412.c2540_donor_number donor_number, 'N/A' Load_Num,COUNT (DISTINCT t413.c205_part_number_id) part_count, SUM (
                t413.c413_item_qty) qty_received, 'NA'form_42, NVL (v412a.labels_printed, 'N') labels_printed
              , NVL (v412a.qc_verified, 'N') qc_verified, NVL (v412a.label_pre_stage, 'N') pre_staging, NVL (
                v412a.label_stage, 'N') staging, TO_CHAR (DECODE (t412.c412_status_fl, 1, 'Y', '-')) pi, TO_CHAR (
                DECODE (t412.c412_status_fl, 2, 'Y', '-')) pp, TO_CHAR (DECODE (t412.c412_status_fl, 3, 'Y', '-')) pv,
                TO_CHAR (t412.c412_stage_date, v_date_fmt) stage_date, t412.c412_type txntype,
                to_char(TRUNC (t412.c412_created_date),v_date_fmt) date_entered
               FROM t412_inhouse_transactions t412, t413_inhouse_trans_items t413, v412a_inhouse_trans_attribute v412a
              WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
                AND t412.c412_status_fl        < 4
                AND t412.c412_type             = 103932 --part re-designation
                AND t412.c412_inhouse_trans_id = v412a.c412_inhouse_trans_id(+)
                AND t412.c1900_company_id      = v_company_id
                AND c412_void_fl              IS NULL
           GROUP BY t412.c412_inhouse_trans_id, t412.c2540_donor_number, v412a.labels_printed
              , v412a.qc_verified, t412.c412_status_fl, t412.c412_stage_date
              , v412a.label_pre_stage, v412a.label_stage, t412.c412_type,TRUNC(t412.c412_created_date)
        )
        staging_dash;
    END gm_fch_staging_dash;
END gm_pkg_op_bba_staging_dash;
/