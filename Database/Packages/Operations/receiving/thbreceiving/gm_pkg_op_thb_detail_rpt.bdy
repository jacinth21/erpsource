CREATE OR REPLACE
PACKAGE BODY gm_pkg_op_thb_detail_rpt
IS
	/**************************************************************************************************************
	* Description :  This procedure is used to fetch the Private Part details for the selected Master Part from the
	  Donor
	*                Inventory Allocation screen
	* Author      :  gpalani
	* PMT#        :  PMT-32606
	* Date        : 16-July-2019
	***************************************************************************************************************/
	
	PROCEDURE gm_fch_private_part_dtls(
	    p_master_part 		IN T2571_THB_LOAD_LOCK_DETAILS.C205_BASE_PART_NUMBER%TYPE,
	    p_out_part_detais 	OUT TYPES.cursor_type)
	AS
	BEGIN
	  OPEN p_out_part_detais FOR 
	  	SELECT c205_part_number_id PARTNUMID 
	  	FROM t205d_part_attribute t205d 
	  	WHERE t205d.c901_attribute_type = 103730 --Master Product Code Type
	  	AND t205d.c205d_attribute_value = p_master_part 
	  	AND t205d.c205d_attribute_value IS NOT NULL 
	  	AND t205d.c205d_attribute_value <> 'N/A' 
	  	AND t205d.c205d_void_fl IS NULL;
	END gm_fch_private_part_dtls;
	
	/**************************************************************************************
	* Description :  This procedure is used to identify whether all the quantities are allocated for the load number
	* Author      :  gpalani
	* PMT#        :  PMT-32606
	* Date        : 16-July-2019
	***************************************************************************************/
	
	PROCEDURE gm_fch_analysis_completion_status(
	    p_analysis_id 		IN t2570_thb_load_lock_master.c2570_thb_load_lock_master_id%TYPE,
	    p_out_allocation_status OUT CLOB)
	    
	AS
	  -- If the JSON output value is greater than 0 the analysis completion button will not be enabled in the Analysis detail screen.
	  
	BEGIN
		  SELECT (JSON_OBJECT ('Analysis_ID' VALUE t2570.c2570_thb_load_lock_master_id
		  		, 'Pending_Allocated_Qty' VALUE (SUM (NVL(t2571.c2571_rad_run_qty,0)) - SUM (NVL(t2571.c2571_qty_allocated,0)))))
		  INTO p_out_allocation_status
		  FROM t2570_thb_load_lock_master t2570,
		    t2571_thb_load_lock_details t2571
		  WHERE t2570.c2570_thb_load_lock_master_id = t2571.c2570_thb_load_lock_master_id
		  AND t2570.c2570_thb_load_lock_master_id   = p_analysis_id
		  AND t2570.c2570_void_fl                  IS NULL
		  AND t2571.c2571_void_fl                  IS NULL
		  GROUP BY t2570.c2570_thb_load_lock_master_id;
	END gm_fch_analysis_completion_status;

	/**************************************************************************************
	* Description :  This procedure is used to fetch the saved donor staged date 
	* Author      :  Suganthi
	***************************************************************************************/
	PROCEDURE gm_fch_donor_stage_date(
	    p_load_num  IN t2561_thb_load_item_detail.c2560_thb_load_id%TYPE DEFAULT NULL,
	    p_donor_num IN t2573_thb_donor_lock.c2540_donor_number%type DEFAULT NULL,
	    p_out_donor_sgdt OUT TYPES.cursor_type)
	AS
	  v_date_fmt t906_rules.c906_rule_value%TYPE;
	  v_analysis_id t2573_thb_donor_lock.c2570_thb_load_lock_master_id%TYPE;
	  
	BEGIN
		
	  SELECT get_compdtfmt_frm_cntx INTO v_date_fmt FROM DUAL;
	  
	  BEGIN                                                    
	    SELECT DISTINCT t2560.c2570_thb_load_lock_master_id
	    INTO v_analysis_id
	    FROM t2560_thb_load t2560
	    WHERE t2560.c2560_thb_load_num = p_load_num
	    AND t2560.c2560_void_fl       IS NULL;
	  EXCEPTION
	  WHEN OTHERS THEN
	    v_analysis_id := NULL;
	  END;
	  
	  OPEN p_out_donor_sgdt FOR 
	     SELECT TO_CHAR(c2540_stage_date,v_date_fmt) stagedt ,
	  		COUNT(c2540_donor_number) noofunits 
	  	FROM t2573_thb_donor_lock 
	  	WHERE c2573_void_fl is null
          AND c2540_stage_date is not null
          AND c2570_thb_load_lock_master_id =v_analysis_id
     GROUP BY c2540_stage_date
	 ORDER BY c2540_stage_date ASC;
	END gm_fch_donor_stage_date;
	
/**************************************************************************************
* Description :  This procedure is used to fetch the schedulling stage details 
* Author      :  Suganthi
***************************************************************************************/
	PROCEDURE gm_fch_load_schedue_details(
	    p_load_num IN t2560_thb_load.c2560_thb_load_num%TYPE,
	    p_out_load_scheduedtls OUT CLOB)
	AS
	  v_date_fmt t906_rules.c906_rule_value%TYPE;
	  
	BEGIN
		
	  SELECT get_compdtfmt_frm_cntx INTO v_date_fmt FROM DUAL;
	  
	   SELECT JSON_ARRAYAGG (JSON_OBJECT 
	   (
	   'Load_Num' value loadnum,
	   'Donor_Num' value donornum, 
	   'Age' value age,
	   'Sex' value sex, 
	   'No_Of_Units' value ctrl_no_count, 
	   'Qty_Allocated' value qtyalloc, 
	   'Qty_Not_Allocated' value NVL (ctrl_no_count, 0) - NVL (qtyalloc, 0),
	   'stgdt' value stgdt,
	   'rsflag' value rs_flag
	   )
    ORDER BY rs_flag, loadnum, donornum ASC returning CLOB)
	   
   INTO p_out_load_scheduedtls
   FROM
    (
         SELECT stage_dtls.donornum donornum, stage_dtls.loadnum loadnum, stage_dtls.age age
          , stage_dtls.sex sex, lot_count.control_num_count ctrl_no_count, stage_dtls.stgdt
          , stage_dtls.master_id, lot_count.qty_allocated qtyalloc, stage_dtls.rs_flag rs_flag
           FROM
            (
                SELECT DISTINCT t2561.c2561_donor_number donornum, t2560.c2560_thb_load_num loadnum,
                    t2561.c2561_donor_age age, t2561.c2561_donor_sex sex, TO_CHAR (t2573.c2540_stage_date, v_date_fmt)
                    stgdt, t2560.c2570_thb_load_lock_master_id master_id, t2573.c2540_rs_created_fl rs_flag
                   FROM t2560_thb_load t2560, t2561_thb_load_item_detail t2561, t2573_thb_donor_lock t2573
                  WHERE t2560.c2560_thb_load_id             = t2561.c2560_thb_load_id
                    AND t2560.c2560_thb_load_num            = NVL (p_load_num, t2560.c2560_thb_load_num)
                    AND t2561.c2561_donor_number            = t2573.c2540_donor_number (+)
                    AND t2560.c2570_thb_load_lock_master_id = t2573.c2570_thb_load_lock_master_id(+)
                    AND t2560.c2560_void_fl                IS NULL
                    AND t2561.c2561_void_fl                IS NULL
                    AND t2573.c2573_void_fl                IS NULL
               GROUP BY t2561.c2561_donor_number, t2560.c2560_thb_load_num, t2561.c2561_donor_age
                  , t2561.c2561_donor_sex, t2561.c205_part_number_id, t2573.c2540_stage_date
                  , t2560.c2570_thb_load_lock_master_id,t2573.c2540_rs_created_fl 
               ORDER BY  t2573.c2540_rs_created_fl,t2560.c2560_thb_load_num, c2561_donor_number ASC
            )
            stage_dtls, (
                SELECT DISTINCT t2561.c2561_donor_number donor_num, NVL (COUNT (t2561.c2561_control_number), 0)
                    control_num_count, t2560.c2570_thb_load_lock_master_id master_id,  count(t2561.c901_ware_house_type) Qty_Allocated
                   FROM t2561_thb_load_item_detail t2561, t2560_thb_load t2560
                  WHERE t2560.c2560_thb_load_num = NVL (p_load_num, t2560.c2560_thb_load_num)
                    AND t2561.c2561_void_fl     IS NULL
                    AND t2560.c2560_void_fl     IS NULL
                    AND t2560.c2560_thb_load_id  = t2561.c2560_thb_load_id
               GROUP BY t2561.c2561_donor_number, t2560.c2570_thb_load_lock_master_id
            )
            lot_count
            
          WHERE stage_dtls.master_id = lot_count.master_id
            AND stage_dtls.donornum  = lot_count.donor_num
            
    ) ;
	END gm_fch_load_schedue_details;
	  	
END gm_pkg_op_thb_detail_rpt;
/