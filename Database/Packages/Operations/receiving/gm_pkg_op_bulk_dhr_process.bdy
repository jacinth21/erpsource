/* Formatted on 2011/08/23 11:49 (Formatter Plus v4.8.0) */
/*
 * @"C:\Database\Packages\Operations\receiving\gm_pkg_op_bulk_dhr_process.bdy";
 */

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_bulk_dhr_process
IS
--   	 	
	/******************************************************************************************
	* Description : This procedure is used to save the details at the time of DHR Batch Process
	*******************************************************************************************/
	PROCEDURE gm_op_sav_bulk_dhr_process(
	  p_dhr_bulk_id		IN OUT	T4081_DHR_BULK_RECEIVE.c4081_dhr_bulk_id%TYPE
	, p_input_str 		IN 	CLOB
	, p_action		IN	CHAR
	, p_user_id		IN	T4081_DHR_BULK_RECEIVE.c4081_last_updated_by%TYPE
	)
	AS
	 
	v_dhrid		t408_dhr.c408_dhr_id%TYPE;
	v_inputstr	CLOB;
	v_price         NUMBER (15, 2);
	v_message       VARCHAR2 (2000);
	v_txn_id	VARCHAR2 (20); 
	 
	BEGIN  
		
	-- Save the input string in temp table TEMP_BULK_DHR_TABLE
	gm_pkg_op_bulk_dhr_txn.gm_sav_dhr_input_str_tmp(NULL, p_input_str); 
	
	--before process, validate DHR status	 	 
	gm_dhr_validate ('P'); 
	
	--save the existing inhouse txn in the temp table, mainly used for sencond time process if any DHR rollbacked
	gm_sav_existing_data_in_temp(p_dhr_bulk_id, p_user_id); 
	
	--pick one DHR which used when generate inhouse txn in t412, it will be replaced by actual 'RS-xxx' after used 
	SELECT c408_dhr_id 
	  INTO v_dhrid 
	  FROM temp_bulk_dhr_table 
	 WHERE rownum = 1
	   and c408_dhr_id is not null; 
	
	--generate inhouse txn for each location type.
	gm_create_ihtxn(v_dhrid, p_user_id);
	
	--update inhouse txn status and reset ref id to 'RS-xx' id after created.
	UPDATE t412_inhouse_transactions
	 SET c412_status_fl = '0',
	     c412_ref_id      = p_dhr_bulk_id,
	     c412_last_updated_date = SYSDATE,
	     c412_last_updated_by = p_user_id
        WHERE c412_ref_id = v_dhrid;

	--assign all control numbers to each inhouse txn
	gm_dhr_split (p_user_id);
	-- call the procedure to update the rejected comment
	gm_upd_ihtxn_comments(p_dhr_bulk_id,p_user_id);
	--update all DHRs status
	gm_dhr_status_upt(p_user_id); 	


END gm_op_sav_bulk_dhr_process;	



/******************************************************************************************
* Description : This procedure is used to save inhouse txn for each location type
*******************************************************************************************/
PROCEDURE gm_create_ihtxn 
(  p_dhrid         IN   t408_dhr.c408_dhr_id%TYPE 
  ,p_user_id	   IN   T4081_DHR_BULK_RECEIVE.c4081_last_updated_by%TYPE
 )
 AS

v_txn_id	VARCHAR2 (20);
v_inputstr	CLOB;
v_price         NUMBER (15, 2);
v_message       VARCHAR2 (2000);

CURSOR v_cursor ( v_loctype IN  t412_inhouse_transactions.c412_type%TYPE )	
	IS		 
		SELECT temp.c408_dhr_id dhrid, temp.c205_part_number_id pnum, sum (c205_qty) qty
		   FROM temp_bulk_dhr_table temp 
		  WHERE temp.C901_LOC_TYPE = v_loctype 
		GROUP BY temp.c408_dhr_id, temp.c205_part_number_id; 
	
CURSOR v_cursor_txn 
	IS 
		 SELECT DISTINCT temp.c901_loc_type loctype
		   FROM temp_bulk_dhr_table temp;

BEGIN

	FOR v_index IN v_cursor_txn
	LOOP
		v_inputstr :='';

		 SELECT get_next_consign_id (v_index.loctype)
		   INTO v_txn_id
		   FROM DUAL; 
			
		FOR v_value IN v_cursor (v_index.loctype)
		LOOP		
			v_price := get_dhr_partnum_price(v_value.dhrid); 
			v_inputstr := v_inputstr || v_value.pnum ||',' || v_value.qty ||',,' || v_price||'|';
			 
		END LOOP; 
	 
		 gm_ls_upd_inloanercsg (v_txn_id,                    -- transaction id
                                v_index.loctype,                -- transaction type
                                41278,                       -- from DHR Split
                                NULL,			    -- release to is not needed
                                NULL,			    -- release to id is not needed
                                '',			    -- final comments is not needed
                                p_user_id,                           -- user id
                                v_inputstr,                   -- Input String
                                p_dhrid,                             -- ref id
                                'PlaceOrder',
                                v_message
                               );

		 -- Release the InHouse Transaction for Verification
		 gm_ls_upd_inloanercsg (v_txn_id,                    -- transaction id
					v_index.loctype,                -- transaction type
					41278,                       -- from DHR Split
					NULL,              -- release to is not needed
					NULL,           -- release to id is not needed
					NULL,          -- final comments is not needed
					p_user_id,                           -- user id
					v_inputstr,                   -- Input String
					p_dhrid,                             -- ref id
					'ReleaseControl',
					v_message
				       );

		--update inhouse txn id in the temp table after created
		UPDATE temp_bulk_dhr_table
		   SET c412_inhouse_trans_id = v_txn_id
		WHERE c901_loc_type =  v_index.loctype;
		
		--void all inhouse txns in t413 which used for later assign control number to inhouse txn.
		UPDATE t413_inhouse_trans_items
		       SET C413_VOID_FL = 'Y',
		       C413_LAST_UPDATED_BY = p_user_id,
		       C413_LAST_UPDATED_DATE = SYSDATE
		   WHERE c412_inhouse_trans_id = v_txn_id 
		  AND c413_control_number IS NULL;
		 
	END LOOP;	 

END gm_create_ihtxn;	

/******************************************************************************************
* Description : This procedure is used to update DHR status to pending verification
*******************************************************************************************/
PROCEDURE gm_dhr_status_upt
( 
   p_user_id			IN    T4081_DHR_BULK_RECEIVE.c4081_last_updated_by%TYPE
 )
 AS
	CURSOR v_cursor_dhr 
	IS 
		 SELECT temp.c408_dhr_id dhrid , sum (c205_qty) qty
		   FROM temp_bulk_dhr_table temp   
		GROUP BY  temp.c408_dhr_id;
BEGIN
	FOR var_dhr IN v_cursor_dhr  
	LOOP		 
		UPDATE t408_dhr
		 SET c408_status_fl = '3',
		     c408_packaged_by = p_user_id, 
		     c408_packaged_ts = SYSDATE,
		     c408_qty_packaged = var_dhr.qty,
		     c408_last_updated_by = p_user_id,
		     c408_last_updated_date = SYSDATE 
	       WHERE c408_dhr_id = var_dhr.dhrid;

	END LOOP; 

END gm_dhr_status_upt;


/******************************************************************************************
* Description : This procedure is used to assign control number to each inhouse txn
*******************************************************************************************/
PROCEDURE gm_dhr_split
( 
   p_user_id			IN    T4081_DHR_BULK_RECEIVE.c4081_last_updated_by%TYPE
 )

AS 
	v_txn_id	VARCHAR2 (20);

	CURSOR v_cursor_ctrlno
	IS
	         SELECT temp.c205_control_number ctrlnum, c412_inhouse_trans_id txnid,  c205_part_number_id pnum, c205_qty qty
		   FROM temp_bulk_dhr_table temp ;

BEGIN 
	FOR var_ctrlno IN v_cursor_ctrlno
	 LOOP	   
			INSERT INTO t413_inhouse_trans_items
				(c413_trans_id, c412_inhouse_trans_id, c205_part_number_id,
			c413_item_qty, c413_control_number, c413_item_price, c413_created_date
			)
				SELECT s413_inhouse_item.NEXTVAL, var_ctrlno.txnid, c205_part_number_id,
				var_ctrlno.qty, var_ctrlno.ctrlnum, c413_item_price, SYSDATE
			FROM t413_inhouse_trans_items 
			WHERE c412_inhouse_trans_id = var_ctrlno.txnid 
			AND c205_part_number_id =  var_ctrlno.pnum
			AND ROWNUM = 1; 
	  END LOOP;

END gm_dhr_split;


/******************************************************************************************
* Description : This procedure is used to save existing data in the inhouse txn and 
* associated DHR id in the temp table , this mainly used for second time to screen
* process, for ex, one dhr rollback and come back to screen to process rollbacked DHR.
*******************************************************************************************/
PROCEDURE gm_sav_existing_data_in_temp
(
    p_dhr_bulk_id		IN    T4081_DHR_BULK_RECEIVE.c4081_dhr_bulk_id%TYPE
  , p_user_id			IN    T4081_DHR_BULK_RECEIVE.c4081_last_updated_by%TYPE
)
AS

v_count 	NUMBER;

CURSOR v_cursor IS

	 SELECT t413.c205_part_number_id pnum, t413.c413_control_number cnum, 
	   get_dhr_id(p_dhr_bulk_id,t413.c205_part_number_id,t413.c413_control_number) dhrid, t412.c412_type loctype
	   FROM  t412_inhouse_transactions t412, t413_inhouse_trans_items t413 
	 WHERE t412.c412_ref_id =  p_dhr_bulk_id
	 AND t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id  
	 AND t412.c412_void_fl IS NULL
	 AND t413.c413_void_fl IS NULL ; 
 
BEGIN

	FOR v_index in v_cursor 
	LOOP

	SELECT COUNT(1) 
	  INTO v_count 
	  FROM temp_bulk_dhr_table 
	 WHERE c205_part_number_id = v_index.pnum 
	  AND c205_control_number = v_index.cnum;
	  
	 IF v_count = 0 
	 THEN
		 INSERT INTO temp_bulk_dhr_table
		 (c408_dhr_id, c205_control_number, c205_part_number_id, c205_qty, c901_loc_type)
		 VALUES
		 (v_index.dhrid, v_index.cnum, v_index.pnum, 1, v_index.loctype);
	 END IF;

	END LOOP;

	UPDATE t413_inhouse_trans_items
	SET c413_void_fl = 'Y',
	   c413_last_updated_by = p_user_id,
	   c413_last_updated_date = SYSDATE
	WHERE c412_inhouse_trans_id in  ( 
	SELECT c412_inhouse_trans_id FROM
		t412_inhouse_transactions
		WHERE c412_ref_id = p_dhr_bulk_id);
	
	UPDATE t412_inhouse_transactions
	SET c412_void_fl = 'Y',
	   c412_last_updated_by = p_user_id,
	   c412_last_updated_date = SYSDATE
	WHERE c412_ref_id = p_dhr_bulk_id;
 
 END gm_sav_existing_data_in_temp;
 


/*******************************************************
 Description: this function returns DHR id 
*******************************************************/
FUNCTION get_dhr_id (
    p_dhr_bulk_id	IN    t4081_dhr_bulk_receive.c4081_dhr_bulk_id%TYPE
   ,p_partnum           IN	t413_inhouse_trans_items.c205_part_number_id%TYPE
   ,p_ctrlno            IN  t413_inhouse_trans_items.c413_control_number%TYPE
)
	RETURN VARCHAR2
IS
	v_dhrid   t408_dhr.c408_dhr_id%TYPE;
	v_company_id  T1900_company.c1900_company_id%TYPE;
BEGIN
 SELECT get_compid_frm_cntx()    INTO v_company_id    FROM DUAL;
 
 SELECT t408a.c408_dhr_id 
    INTO v_dhrid
 FROM t408a_dhr_control_number t408a, t4082_bulk_dhr_mapping t4082 , t4081_dhr_bulk_receive t4081
  WHERE  t4082.c4081_dhr_bulk_id = p_dhr_bulk_id
    AND t408a.c408_dhr_id  = t4082.c408_dhr_id
    AND t4082.c4081_dhr_bulk_id = t4081.c4081_dhr_bulk_id
    AND t408a.c205_part_number_id = p_partnum
    AND t408a.c408a_conrol_number = p_ctrlno 
    AND t4081.c1900_company_id = v_company_id
    AND t408a.c408a_void_fl IS NULL
    AND t4082.c4082_void_fl IS NULL
    AND t4081.c4081_void_fl IS NULL;
    
	RETURN v_dhrid;
EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		RETURN '';
END get_dhr_id;

/*******************************************************
 Description: this function returns part price by DHR id 
*******************************************************/
FUNCTION get_dhr_partnum_price (
     p_dhrid         IN   t408_dhr.c408_dhr_id%TYPE 
)
	RETURN NUMBER
IS 
--
      v_partyid         t401_purchase_order.c301_vendor_id%TYPE;
      v_wo_uom_str      VARCHAR2 (100);
      v_amt             t402_work_order.c402_cost_price%TYPE;
      v_uomqty          NUMBER;
      v_price           NUMBER (15, 2);
      v_comments        VARCHAR2 (4000);
      v_received_date   DATE;
      v_po_type         VARCHAR2 (20);
      vprodclass        VARCHAR2 (20);
      v_company_id		t1900_company.c1900_company_id%TYPE;
--
BEGIN
	SELECT b.c301_vendor_id, get_wo_uom_type_qty (a.c402_work_order_id),
             NVL (DECODE (b.c401_type, 3104, c.c205_cost, a.c402_cost_price),
                  a.c402_cost_price
                 ),
             c408_received_date--Removed created date because received date was used in currency conversion, the difference in date will result in different rates, hence diff. amount.
             ,get_potype_from_woid(t408.C402_Work_Order_Id),c.c205_product_class -- Get the PO type and part number product class
             ,t408.c1900_company_id
        INTO v_partyid, v_wo_uom_str,
             v_amt,
             v_received_date,v_po_type,vprodclass,
             v_company_id
        FROM t402_work_order a,
             t401_purchase_order b,
             t205_part_number c,
             t408_dhr t408
       WHERE t408.c408_dhr_id = p_dhrid
         AND a.c402_work_order_id = t408.c402_work_order_id
         AND a.c401_purchase_ord_id = b.c401_purchase_ord_id
         AND a.c205_part_number_id = c.c205_part_number_id 
         AND a.C402_VOID_FL is null and b.C401_VOID_FL is null and c.C205_ACTIVE_FL = 'Y' and t408.C408_VOID_FL is null;

	 v_uomqty :=
         TO_NUMBER (NVL (SUBSTR (v_wo_uom_str, INSTR (v_wo_uom_str, '/') + 1),
                         1
                        )
                   );
       
   	--Added the currency conversion function because the removed code and currency function fetched different values for conversion due to the difference in the removed SQL and function
   	  v_price := (get_currency_conversion (get_vendor_currency (v_partyid), get_comp_curr (v_company_id), v_received_date, v_amt) / v_uomqty);
	 
	RETURN v_price;
--
EXCEPTION
	WHEN OTHERS
	THEN
--
		RETURN 0;
--
END get_dhr_partnum_price;
 

/*******************************************************************
* Description : This procedure is used to DHR status validation
********************************************************************/ 
PROCEDURE gm_dhr_validate ( 
  p_action      IN	CHAR 
)
AS
   v_inv_fl            CHAR (1);
   v_chkstatusfl       NUMBER (1);
   v_chkdhrvoidfl      CHAR (1);
   v_chkwordervoidfl   CHAR (1);
   v_wo		       t402_work_order.c402_work_order_id%TYPE;

	CURSOR v_cursor_dhr 
	IS 
		 SELECT temp.c408_dhr_id dhrid 
		   FROM temp_bulk_dhr_table temp   
		GROUP BY  temp.c408_dhr_id;
BEGIN

	FOR var_dhr IN v_cursor_dhr  
	LOOP		 
	 -- Check for DHR Void Flag. If the Flag is turned ON, then throw an Application Error
	   SELECT t408.c408_void_fl, t402.c402_void_fl, t402.c402_work_order_id
	     INTO v_chkdhrvoidfl, v_chkwordervoidfl,v_wo 
	     FROM t408_dhr t408, t402_work_order t402
	    WHERE t408.c408_dhr_id = var_dhr.dhrid
	      AND t408.c402_work_order_id = t402.c402_work_order_id;

	   IF (v_chkdhrvoidfl IS NOT NULL)
	   THEN
	      --
	      raise_application_error (-20004, '');
	   --
	   END IF;

	   IF (v_chkwordervoidfl IS NOT NULL)
	   THEN
	      --
	      GM_RAISE_APPLICATION_ERROR('-20999','300',v_wo);
	   --
	   END IF; 

	   -- End of Check for DHR Void Flag and  check if the value is updated by other user
	   SELECT     c408_update_inv_fl, TO_NUMBER (c408_status_fl)
		 INTO v_inv_fl, v_chkstatusfl
		 FROM t408_dhr
		WHERE c408_dhr_id = var_dhr.dhrid
	   FOR UPDATE;
 
	   IF v_chkstatusfl > 2
	    THEN	      
		 raise_application_error (-20002, '');  
	   END IF;
	 
	END LOOP;
	END gm_dhr_validate;

	/*******************************************************************
	* Description : This procedure is used to update the comment of RSQN
	********************************************************************/
	PROCEDURE gm_upd_ihtxn_comments(
		p_dhr_bulk_id	IN		t4081_dhr_bulk_receive.c4081_dhr_bulk_id%TYPE,
		p_user_id		IN		t412_inhouse_transactions.c412_last_updated_by%TYPE
	)
	AS
		v_rej_reason	t409_ncmr.c409_rej_reason%TYPE;
	BEGIN
		-- Get the rejected reason of RS transaction
		BEGIN
			-- Rejected reason will be same for all the NCMR generated from one RS id, so we can use rownum
			SELECT c409_rej_reason
			INTO v_rej_reason
			FROM t409_ncmr
			WHERE c408_dhr_id IN
			  (SELECT c408_dhr_id
			  FROM t4082_bulk_dhr_mapping
			  WHERE c4081_dhr_bulk_id = p_dhr_bulk_id
			  AND c4082_void_fl      IS NULL
			  )
			AND ROWNUM =1;
		EXCEPTION
		WHEN NO_DATA_FOUND THEN
			v_rej_reason := '';
		END;
		
		-- Update the comment for RSQN
		UPDATE t412_inhouse_transactions
		SET c412_comments        = v_rej_reason,
		  c412_last_updated_by   = p_user_id,
		  c412_last_updated_date = CURRENT_DATE
		WHERE c412_ref_id        = p_dhr_bulk_id
		AND c412_type            = 104622 --Qty to Quarantine
		AND c412_void_fl        IS NULL;

	END gm_upd_ihtxn_comments;


END gm_pkg_op_bulk_dhr_process;
/