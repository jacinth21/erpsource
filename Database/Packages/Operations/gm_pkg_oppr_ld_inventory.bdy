/*
-- @"c:\database\packages\operations\purchasing\gm_pkg_oppr_ld_inventory.bdy"
-- show err  Package Body gm_pkg_oppr_ld_inventory

delete from t251_inventory_lock_detail;
delete from t253a_consignment_lock;
delete from t253b_item_consignment_lock;
delete from t253c_request_lock;
delete from t253d_request_detail_lock;
delete from t253e_order_lock;
delete from t253f_item_order_lock;
delete from t252_inventory_lock_ref;
delete from t254_inventory_txn_log;
update t4040_demand_sheet set c250_inventory_lock_id='';
delete from t250_inventory_lock;
exec gm_pkg_oppr_ld_inventory.gm_op_ld_demand_main(20430);
commit;
*/
CREATE OR REPLACE PACKAGE BODY gm_pkg_oppr_ld_inventory
IS
    /*******************************************************
    * Purpose: This procedure will be main procedure to
    *   load inventory information
    *******************************************************/
    PROCEDURE gm_op_ld_demand_main (
        p_lock_type IN t250_inventory_lock.c901_lock_type%TYPE
    )
    AS
        --
        v_inventory_id t250_inventory_lock.c250_inventory_lock_id%TYPE;
        v_start_dt DATE;
        --
        CURSOR inventory_mapping_cur
        IS
            SELECT c4016_global_inventory_map_id
                 , c901_level_id
                 , c901_level_value
              FROM t4016_global_inventory_mapping t4016
             WHERE t4016.c4016_void_fl IS NULL ;

    BEGIN
        --
        v_start_dt := SYSDATE;
        --
        FOR set_val IN inventory_mapping_cur
        LOOP
        
            SELECT s250_inventory_lock.NEXTVAL INTO v_inventory_id FROM DUAL;
            --
            INSERT INTO t250_inventory_lock
                (c250_inventory_lock_id, c250_lock_date, c901_lock_type, c4016_global_inventory_map_id)
            VALUES
                (v_inventory_id, TRUNC (SYSDATE), p_lock_type, set_val.c4016_global_inventory_map_id) ;
            
            -- If level ID is company then load US Inventory 
            IF (set_val.c901_level_id = 102580) 
            THEN
                --
                gm_pkg_oppr_ld_inventory.gm_op_ld_us_inventory (v_inventory_id) ;
                gm_pkg_oppr_ld_inventory.gm_op_ld_pending_request (v_inventory_id, v_start_dt) ;
                gm_pkg_oppr_ld_inventory.gm_op_ld_sub_components (v_inventory_id) ;
                gm_pkg_oppr_ld_inventory.gm_op_ld_part_attribute (v_inventory_id); 
                -- currently par is handled at demand sheet level
                -- So the code is commented
                --gm_pkg_oppr_ld_inventory.gm_op_ld_par (v_inventory_id);
                --
            ELSE
                gm_pkg_oppr_ld_inventory.gm_op_ld_ous_inventory(v_inventory_id, 
                                        set_val.c901_level_id, set_val.c901_level_value);
                                        
            END IF;
            --
            UPDATE t251_inventory_lock_detail t251
	           SET t251.c251_qty = 0
	        WHERE t251.c251_qty < 0
	        AND t251.c250_inventory_lock_id = v_inventory_id;

            COMMIT;
            --
        END LOOP;    
            --
            -- LOG THE STATUS
            gm_pkg_cm_job.gm_cm_notify_load_info (v_start_dt, SYSDATE, 'S', 'SUCCESS', 'gm_pkg_oppr_ld_inventory') ;
            COMMIT;
        
    EXCEPTION
    WHEN OTHERS THEN
        ROLLBACK;
        --
        gm_pkg_cm_job.gm_cm_notify_load_info (v_start_dt, SYSDATE, 'E', SQLERRM, 'gm_pkg_oppr_ld_inventory') ;
        COMMIT;
        --
    END gm_op_ld_demand_main;
--
    /*************************************************************************
    * Purpose: Procedure used to load inventory on Shelf Info
    *************************************************************************/
     PROCEDURE gm_op_ld_us_inventory
    (
        p_inventory_lock_id IN t250_inventory_lock.c250_inventory_lock_id%TYPE
    )
    AS
        V_INSTOCK_CALC NUMBER;
        CURSOR inshelf_cur
        IS
            SELECT part_id pnum, sales_alloc sales_allocated, consignment_alloc cons_alloc
                 , quar_alloc_to_self_quar quar_alloc_self_quar, quar_alloc_to_scrap quar_alloc_to_scrap
                 , quar_alloc_to_inv quar_alloc_to_inv, bulk_alloc_to_set bulk_alloc_to_set, bulk_alloc_to_inv bulk_alloc_to_inv
                 , in_returns rtn_hold_area, inv_alloc_to_pack inv_alloc_to_pack, in_shelf inshelf, shelf_alloc shelfalloc
                 , raw_material_qty rmqty, in_quar quaravail, quar_alloc quaralloc, dhr_qty dhrqty, tbe_alloc tbe_alloc
                 , inhouse_bulk inbulk, in_stock instock, bulk_all bulkall, stock_all stockall, pack_all packall, quar_all quarall
                 , BULK_ALLOC BULKALLOC,  PART.PO_PENDING , PART.DHR_QTY, PART.BUILD_SET_QTY
                 , RW_AVAIL_QTY, RW_ALLOC_QTY, PART.DHR_WIP 
                 , part.DHR_ALLOC DHR_ALLOC, CONSIGNMENT_ALLOC
              FROM part_inventory_fact part;

        icount number :=0;
    BEGIN
         DELETE FROM my_temp_part_list ;

         INSERT INTO my_temp_part_list
            (c205_part_number_id
            ) 
         SELECT t205.c205_part_number_id pnum
           FROM t205_part_number t205
          WHERE t205.c205_qty_in_stock IS NOT NULL;
          
        -- load inventory info
        FOR set_val IN inshelf_cur
        LOOP
            
        	-- Reducing the DHR alloc qty from the Instock qty,to avoid double couting of quantity that is allocated to trans [DHFG] and 
        	-- also available in the DHR Alloc Bucket.
            BEGIN
                SELECT DECODE(SIGN(set_val.instock-set_val.DHR_ALLOC),-1,0,set_val.instock-set_val.DHR_ALLOC) 
                INTO V_INSTOCK_CALC
                FROM DUAL;
            EXCEPTION WHEN OTHERS
            THEN
                V_INSTOCK_CALC := '';
            END;
            
            --Add the Cons Alloc qty to inventory qty,as we are taking the RQ and showing in TPR.
            --This addition will is allocated to the RQ will offset the TPR and will not prompt to order more. 
            V_INSTOCK_CALC := V_INSTOCK_CALC + set_val.CONSIGNMENT_ALLOC;
            
            -- Save the value In shelf
            gm_pkg_oppr_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id, set_val.pnum, 20464,V_INSTOCK_CALC) ;

            -- Save the value Quarantine Available
            gm_pkg_oppr_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id, set_val.pnum, 20454,set_val.quaravail) ;

            -- Save the value 'Quarantine Allocated (Inventory)
            gm_pkg_oppr_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id, set_val.pnum, 20455,set_val.quaralloc) ;

            -- Save Open PO Information
            gm_pkg_oppr_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id, set_val.pnum, 20460,
                                                        set_val.po_pending - (set_val.dhr_wip)) ;

            -- Save DHR Information
            gm_pkg_oppr_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id, set_val.pnum, 20461, (set_val.dhr_qty  + set_val.dhr_wip)) ;

            -- Save Built Set Information
            gm_pkg_oppr_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id, set_val.pnum, 20462, set_val.build_set_qty) ;

            -- Save Restricted warehouse
            gm_pkg_oppr_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id, set_val.pnum, 4000114,
                    NVL(set_val.rw_avail_qty,0) +  NVL(set_val.rw_alloc_qty,0)) ;

            icount := icount + 1;

            -- Commit every 1000 rows for better performance
            IF (icount = 1000)
            THEN
                --
                COMMIT;
                icount := 0;
                --
            END IF;
            -- Save the value Bulk Allocated (Pack)
            --gm_pkg_oppr_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id, set_val.pnum, 20458,set_val.inv_alloc_to_pack) ;
            -- Save the value Sales Allocated
            --gm_pkg_oppr_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id, set_val.pnum, 20451,set_val.sales_allocated) ;
            -- Save the value Consignment Allocated
            --gm_pkg_oppr_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id, set_val.pnum, 20452,set_val.cons_alloc) ;
            -- Save the value Quarantine Allocated (Self)
            --gm_pkg_oppr_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id, set_val.pnum, 20453,set_val.quar_alloc_self_quar) ;
            -- Save the value Bulk Allocated (Set)
            --gm_pkg_oppr_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id, set_val.pnum, 20456,set_val.bulk_alloc_to_set) ;
            -- Save the value Bulk Allocated (Inventory)
            --gm_pkg_oppr_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id, set_val.pnum, 20457,set_val.bulk_alloc_to_inv) ;
            -- Save return hold ared
            --gm_pkg_oppr_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id, set_val.pnum, 20459,set_val.rtn_hold_area) ;
            --
        END LOOP;

        -- Load PO plus DHR numbers
        gm_pkg_oppr_ld_inventory.gm_op_ld_openpodhr (p_inventory_lock_id) ;

    END gm_op_ld_us_inventory ;


   /*************************************************************************
    * Purpose: Procedure used to load inventory on Shelf Info
    *************************************************************************/
    PROCEDURE gm_op_ld_ous_inventory 
    (
        p_inventory_lock_id IN t250_inventory_lock.c250_inventory_lock_id%TYPE
      , p_level_id          IN t4016_global_inventory_mapping.c901_level_id%TYPE
      , p_level_value       IN t4016_global_inventory_mapping.c901_level_value%TYPE
    )
    AS
        CURSOR inshelf_cur
        IS
            SELECT part_id pnum, SUM(in_stock) instock, SUM(in_quar) quaravail
                 , SUM(quar_alloc) quaralloc, SUM(build_set_qty) buildset    
              FROM ous_part_inventory_fact ous
             WHERE month_year = TO_CHAR (ADD_MONTHS (TRUNC (SYSDATE, 'MONTH'), -1), 'MM/YYYY') 
               AND ous.ext_country_id IN (SELECT my_temp_txn_id from my_temp_list)
             GROUP BY part_id;  
          
		icount number :=0;
	BEGIN
       DELETE FROM my_temp_list;

       --
       INSERT INTO my_temp_list
                   (my_temp_txn_id)
            SELECT t906.c906_rule_id 
    		  FROM t906_rules t906
    		 WHERE t906.c906_rule_grp_id = 'OUS_REGION_MAPPING'
    		   AND ( t906.c906_rule_value = DECODE(p_level_id, 102584, p_level_value, -9999)
                  OR t906.c906_rule_value IN 
                            ( SELECT v700.region_id
                                FROM v700_territory_mapping_detail v700
                               WHERE v700.gp_id = DECODE (p_level_id,102583, p_level_value,-9999) -- Zone Level Rollup 
                                  OR v700.divid = DECODE (p_level_id,102581, p_level_value ,-9999) -- Division level Rollup
                            )
                   );
                                            
        -- load inventory info
        FOR set_val IN inshelf_cur
        LOOP
            -- Physical Audit Lock
            -- Save the value In shelf
            gm_pkg_oppr_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id, set_val.pnum, 20464,set_val.instock) ;
            
            -- Save the value Quarantine Available 
            gm_pkg_oppr_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id, set_val.pnum, 20454,set_val.quaravail) ;
            
            -- Save the value 'Quarantine Allocated (Inventory)
            gm_pkg_oppr_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id, set_val.pnum, 20455,set_val.quaralloc) ;
            
            -- Save Built Set Information 
            gm_pkg_oppr_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id, set_val.pnum, 20462, set_val.buildset) ;
            
            --
            icount := icount + 1;
		    -- Commit every 1000 rows for better performance 
            IF (icount = 1000) 
            THEN
                -- 
                COMMIT;
                icount := 0;
                --
            END IF;
        END LOOP;
    
        -- Load PO plus DHR numbers
        --gm_pkg_oppr_ld_inventory.gm_op_ld_openpodhr (p_inventory_lock_id) ;
    
    END gm_op_ld_ous_inventory ;
    
    /*************************************************************************
    * Purpose: Procedure used to load Open DHR and PO (mainly used for TTP load)
    *************************************************************************/
    PROCEDURE gm_op_ld_openpodhr (
            p_inventory_lock_id IN t250_inventory_lock.c250_inventory_lock_id%TYPE)
    AS
        icount number :=0;

		CURSOR openpodhr_cur
        IS
             SELECT b.c205_part_number_id pnum, SUM (c251_qty) qty
               FROM t250_inventory_lock a, t251_inventory_lock_detail b
              WHERE a.c250_inventory_lock_id = p_inventory_lock_id
                AND a.c250_inventory_lock_id = b.c250_inventory_lock_id
                AND b.c901_type             IN (20460, 20461)
           GROUP BY b.c205_part_number_id;
    BEGIN

		--
		-- load inventory info (PO Plus DHR ) 
		FOR set_val IN openpodhr_cur
		LOOP

			gm_pkg_oppr_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id, set_val.pnum, 20463, set_val.qty) ;

            --
            icount := icount + 1;
		    -- Commit every 1000 rows for better performance 
            IF (icount = 1000) 
            THEN
                -- 
                COMMIT;
                icount := 0;
                --
            END IF;

		END LOOP;
        
        
    END gm_op_ld_openpodhr;



    
    /*************************************************************************
    * Purpose: Procedure used to load Pending Requests
    *************************************************************************/
    PROCEDURE gm_op_ld_pending_request (
            p_inventory_lock_id IN t250_inventory_lock.c250_inventory_lock_id%TYPE,
            p_from_period       IN DATE)
    AS
        icount number :=0;
		--
        CURSOR prequest_calc_cur
        IS
             SELECT t205.c205_part_number_id pnum, NVL (SUM (temp.qty), 0) qty
                --, NVL (DECODE (temp.sfl, 10, 50567, 15, 50568, 20, 50569), 50567) code_id
               FROM
                (
                    SELECT t521.c205_part_number_id p_num, SUM(t521.c521_qty) qty
                    --, DECODE (c520_status_fl, 30, 20,c520_status_fl) sfl
                      FROM t520_request t520, t521_request_detail t521
                     WHERE t520.c520_request_id      = t521.c520_request_id
                       AND t520.c520_void_fl        IS NULL
                       AND t520.c520_delete_fl      IS NULL
                       AND t520.c520_status_fl       > 0
                       AND t520.c520_status_fl       < 40
                       AND t520.c901_request_source <> 50617
                       AND t520.c901_ext_country_id  = 1101
		       -- Include ETL Restock RQ irrespective of the Required Date is passed by or not.
                       AND (t520.c520_required_date   < p_from_period
                       		 OR t520.c901_request_source='4000121'
                       		)
                     GROUP BY  t521.c205_part_number_id  
              UNION ALL
                    SELECT t505.c205_part_number_id p_num, SUM(t505.c505_item_qty) qty
                        --, DECODE (c520_status_fl, 30, 20,c520_status_fl) sfl
                      FROM t520_request t520, t504_consignment t504, t505_item_consignment t505
                     WHERE t520.c520_request_id      = t504.c520_request_id
                       AND t504.c504_consignment_id  = t505.c504_consignment_id
                       AND t520.c520_void_fl        IS NULL
                       AND t520.c520_delete_fl      IS NULL
                       AND t520.c520_status_fl       > 0
                       AND t520.c520_status_fl       < 40
                       AND t520.c901_request_source <> 50617
		       -- Include ETL Restock RQ irrespective of the Required Date is passed by or not.
                       AND (t520.c520_required_date   < p_from_period
                       		OR t520.c901_request_source='4000121'
                       		)
                       AND t520.c901_ext_country_id  = 1101
                       AND t504.c504_void_fl        IS NULL
                     GROUP BY  t505.c205_part_number_id 
                UNION ALL
                    SELECT t502.c205_part_number_id p_num, SUM(t502.c502_item_qty) qty
                    --, 10 sfl
                      FROM t501_order t501, t502_item_order t502
                     WHERE t501.c501_order_id                = t502.c501_order_id
                       AND t501.c501_status_fl               = 0
                       AND NVL (t501.c901_order_type, - 999) = 2525
                       AND t501.c501_void_fl                IS NULL
                       AND t501.c901_ext_country_id = 1101
                     GROUP BY t502.c205_part_number_id   
                UNION ALL
                    -- Get the Backorder Loaner transaction.
                    SELECT t413.c205_part_number_id pnum, SUM(t413.c413_item_qty) qty
                    --, 10 sfl
                      FROM t412_inhouse_transactions t412, t413_inhouse_trans_items t413
                     WHERE c412_type IN (100062, 1006572)   -- PL Back Order, IH Back Order
                       AND t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
                       AND c412_status_fl = '0'   -- Back Order
                       AND c412_void_fl IS NULL
                       AND t413.c413_void_fl IS NULL
                       AND t412.c901_country_id IS NULL   -- To get only US Back Order Loaner Transaction.
                       AND t412.c901_ext_country_id  = 1101
                     GROUP BY t413.c205_part_number_id  
    		    )temp, t205_part_number t205
              WHERE t205.c205_part_number_id = temp.p_num(+)
                AND t205.c205_qty_in_stock  IS NOT NULL         
           GROUP BY t205.c205_part_number_id;
        --   
        BEGIN
        
			-- load inventory info (PO Plus DHR ) 
			FOR set_val IN prequest_calc_cur
			LOOP

				gm_pkg_oppr_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id, set_val.pnum, 4000106, set_val.qty) ;

				icount := icount + 1;
                -- Commit every 1000 rows for better performance 
				IF (icount = 1000) 
				THEN
					-- 
					COMMIT;
					icount := 0;
					--
				END IF;

			END LOOP;
        
            -- load associated record for further verification
			gm_pkg_oppr_ld_inventory.gm_op_ld_tpr_split_ous_us (p_inventory_lock_id, p_from_period) ;
            gm_pkg_oppr_ld_inventory.gm_op_ld_pending_req_detail (p_inventory_lock_id) ;
        
            --
        END gm_op_ld_pending_request;
    
    /*************************************************************************
    * Purpose: Procedure used to load Total Pending Requests for OUS 
    *  and Split US vs OUS Information 
    *************************************************************************/
    PROCEDURE gm_op_ld_tpr_split_ous_us (
      p_inventory_lock_id IN t250_inventory_lock.c250_inventory_lock_id%TYPE
    , p_from_period       IN DATE )
    AS
    --
    CURSOR ousquest_calc_cur
    IS
         SELECT t205.c205_part_number_id pnum, NVL (SUM (temp.qty), 0) qty
            --, NVL (DECODE (temp.sfl, 10, 50567, 15, 50568, 20, 50569), 50567) code_id
           FROM
            (
                SELECT t521.c205_part_number_id p_num, t521.c521_qty qty
                    --, DECODE (c520_status_fl, 30, 20,c520_status_fl) sfl
                  FROM t520_request t520, t521_request_detail t521
                 WHERE t520.c520_request_id      = t521.c520_request_id
                   AND t520.c520_void_fl        IS NULL
                   AND t520.c520_delete_fl      IS NULL
                   AND t520.c520_status_fl       > 0
                   AND t520.c520_status_fl       < 40
                   AND t520.c901_request_source <> 50617
                   AND t520.c901_ext_country_id  = 1101
                   AND (t520.c520_required_date   < p_from_period
                   		OR t520.c901_request_source='4000121'
                       	)
                   AND ( ( t520.c520_request_for = 40021
                         AND t520.c520_request_to IN (SELECT my_temp_txn_id
                                                      FROM my_temp_key_value
                                                     WHERE my_temp_txn_key = 'DISTRIBUTOR') )
                    OR ( t520.c901_request_source    = '50616'                             
                        AND t520.c520_request_txn_id   IN (SELECT my_temp_txn_id
                                                      FROM my_temp_key_value
                                                     WHERE my_temp_txn_key = 'SHEET') )
                      )                              
          UNION ALL
                SELECT t505.c205_part_number_id p_num, t505.c505_item_qty qty
                    --, DECODE (c520_status_fl, 30, 20, c520_status_fl) sfl
                  FROM t520_request t520, t504_consignment t504, t505_item_consignment t505
                 WHERE t520.c520_request_id      = t504.c520_request_id
                   AND t504.c504_consignment_id  = t505.c504_consignment_id
                   AND t520.c520_void_fl        IS NULL
                   AND t520.c520_delete_fl      IS NULL
                   AND t520.c520_status_fl       > 0
                   AND t520.c520_status_fl       < 40
                   AND t520.c901_request_source <> 50617
                   AND (t520.c520_required_date   < p_from_period
                   		OR t520.c901_request_source='4000121'
                       	)
                   AND t520.c901_ext_country_id  = 1101
                   AND t504.c504_void_fl        IS NULL
                   AND ( ( t520.c520_request_for = 40021
                         AND t520.c520_request_to IN (SELECT my_temp_txn_id
                                                      FROM my_temp_key_value
                                                     WHERE my_temp_txn_key = 'DISTRIBUTOR') )
                    OR 
                        ( t520.c901_request_source    = '50616'                             
                        AND t520.c520_request_txn_id   IN (SELECT my_temp_txn_id
                                                      FROM my_temp_key_value
                                                     WHERE my_temp_txn_key = 'SHEET') )
                       )                              
    	    )temp, t205_part_number t205
          WHERE t205.c205_part_number_id = temp.p_num(+)
            AND t205.c205_qty_in_stock  IS NOT NULL         
       GROUP BY t205.c205_part_number_id;
       --
       CURSOR usforecat_calc_cur
        IS
          SELECT  t251.c205_part_number_id pnum
                , SUM (DECODE(t251.c901_type,4000106, t251.c251_qty , 0)) 
                 - SUM (DECODE(t251.c901_type,4000108, t251.c251_qty, 0)) qty
            FROM t251_inventory_lock_detail t251
           WHERE t251.c250_inventory_lock_id =  p_inventory_lock_id
             AND t251.c901_type IN (4000106,  4000108)
        GROUP BY t251.c205_part_number_id;

        icount number :=0;
    --   
    BEGIN
        --
              
        DELETE FROM my_temp_key_value;

        -- Below SQL to fetch only OUS Shipment
        -- 100823 Represemt US Sheet  
        INSERT INTO my_temp_key_value
                    (my_temp_txn_id, my_temp_txn_key)
            SELECT DISTINCT v700.d_id, 'DISTRIBUTOR'
                       FROM v700_territory_mapping_detail v700
                      WHERE v700.divid = 100824
            UNION ALL
            SELECT t4020.c4020_demand_master_id, 'SHEET'
              FROM t4015_global_sheet_mapping t4015, t4020_demand_master t4020
             WHERE t4020.c901_demand_type IN (40021, 40022)
               AND t4020.c4015_global_sheet_map_id = t4015.c4015_global_sheet_map_id
               AND (t4015.c901_level_id, t4015.c901_level_value) IN (
                              SELECT t4015.c901_level_id, t4015.c901_level_value
                                FROM t4015_global_sheet_mapping t4015
                               WHERE t4015.c901_level_value != 100823
                                 AND t4015.c901_access_type = 102623);           

        
		-- load pending OUS TPR Information
		FOR set_val IN ousquest_calc_cur
		LOOP

            gm_pkg_oppr_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id, set_val.pnum, 4000108, set_val.qty) ;
            
            icount := icount + 1;
            -- Commit every 1000 rows for better performance 
            IF (icount = 1000) 
            THEN
            	-- 
            	COMMIT;
            	icount := 0;
            	--
            END IF;            

		END LOOP;
        
        -- load us forecast
		FOR set_val IN usforecat_calc_cur
		LOOP

			gm_pkg_oppr_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id, set_val.pnum, 4000107, set_val.qty) ;

            icount := icount + 1;
            -- Commit every 1000 rows for better performance 
            IF (icount = 1000) 
            THEN
            	-- 
            	COMMIT;
            	icount := 0;
            	--
            END IF; 
            
		END LOOP;

        
    END gm_op_ld_tpr_split_ous_us;
    
    
    /*************************************************************************
    * Purpose: Procedure used to load Pending Requests detail
    * Will lock open request, used for further reference
    *************************************************************************/
    PROCEDURE gm_op_ld_pending_req_detail (
            p_inventory_lock_id IN t250_inventory_lock.c250_inventory_lock_id%TYPE)
    AS
        icount number :=0;
    BEGIN
         INSERT
           INTO t253c_request_lock
            (
                c253c_request_lock_id, c250_inventory_lock_id, c520_request_id
              , c520_request_date, c520_required_date, c901_request_source
              , c520_request_txn_id, c207_set_id, c520_request_for
              , c520_request_to, c901_request_by_type, c520_request_by
              , c901_purpose, c520_master_request_id, c520_status_fl
              , c901_ship_to, c520_ship_to_id
            )
         SELECT s253c_request_lock.NEXTVAL, p_inventory_lock_id, c520_request_id
          , c520_request_date, c520_required_date, c901_request_source
          , c520_request_txn_id, c207_set_id, c520_request_for
          , c520_request_to, c901_request_by_type, c520_request_by
          , c901_purpose, c520_master_request_id, c520_status_fl
          , c901_ship_to, c520_ship_to_id 
           FROM t520_request t520
          WHERE t520.c520_void_fl   IS NULL
            AND t520.c520_delete_fl IS NULL
            AND t520.c901_ext_country_id  = 1101
            AND t520.c520_status_fl  < 40;
        
         INSERT
           INTO t253d_request_detail_lock
            (
                c253d_request_detail_lock_id, c250_inventory_lock_id, c520_request_id
              , c205_part_number_id, c521_qty
            )
         SELECT s253d_request_detail_lock.NEXTVAL, p_inventory_lock_id, t521.c520_request_id
          , t521.c205_part_number_id, t521.c521_qty
           FROM t520_request t520, t521_request_detail t521
          WHERE t520.c520_void_fl   IS NULL
            AND t520.c520_delete_fl IS NULL
            AND t520.c520_status_fl  < 40
            AND t520.c901_ext_country_id  = 1101
            AND t520.c520_request_id = t521.c520_request_id;
        --
            COMMIT;
         INSERT
           INTO t253a_consignment_lock
            (
                c253a_consignment_lock_id, c250_inventory_lock_id, c504_consignment_id
              , c701_distributor_id, c504_status_fl, c504_void_fl
              , c704_account_id, c207_set_id, c504_type
              , c504_verify_fl, c504_verified_date, c504_verified_by
              , c520_request_id, c504_master_consignment_id
            )
         SELECT s253a_consignment_lock.NEXTVAL, p_inventory_lock_id, t504.c504_consignment_id
          , t504.c701_distributor_id, t504.c504_status_fl, t504.c504_void_fl
          , t504.c704_account_id, t504.c207_set_id, t504.c504_type
          , t504.c504_verify_fl, t504.c504_verified_date, t504.c504_verified_by
          , t504.c520_request_id, t504.c504_master_consignment_id
           FROM t520_request t520, t504_consignment t504
          WHERE t520.c520_void_fl   IS NULL
            AND t520.c520_delete_fl IS NULL
            AND t520.c520_status_fl  < 40
            AND t520.c901_ext_country_id  = 1101
            AND t520.c520_request_id = t504.c520_request_id;
        --
         INSERT
           INTO t253b_item_consignment_lock
            (
                c253b_item_consignment_lock_id, c250_inventory_lock_id, c504_consignment_id
              , c205_part_number_id, c505_item_qty
            )
         SELECT s253b_item_consignment_lock.NEXTVAL, p_inventory_lock_id, t505.c504_consignment_id
          , c205_part_number_id, c505_item_qty
           FROM t520_request t520, t504_consignment t504, t505_item_consignment t505
          WHERE t520.c520_void_fl       IS NULL
            AND t520.c520_delete_fl     IS NULL
            AND t520.c520_status_fl      < 40
            AND t520.c901_ext_country_id  = 1101
            AND t520.c520_request_id     = t504.c520_request_id
            AND t504.c504_consignment_id = t505.c504_consignment_id;
        --
         COMMIT;   
         INSERT
           INTO t253e_order_lock
            (
                c253e_order_lock_id, c250_inventory_lock_id, c501_order_id
              , c501_status_fl, c501_void_fl, c501_last_updated_by
              , c501_last_updated_date, c901_order_type, c501_parent_order_id
              , c501_total_cost, c501_ship_to, c501_ship_to_id
              , c501_ship_cost, c501_update_inv_fl, c503_invoice_id
              , c506_rma_id, c501_customer_po, c704_account_id
              , c501_order_date, c703_sales_rep_id, c501_shipping_date
              , c501_order_date_time
            )
         SELECT s253e_order_lock.NEXTVAL, p_inventory_lock_id, c501_order_id
          , c501_status_fl, c501_void_fl, c501_last_updated_by
          , c501_last_updated_date, c901_order_type, c501_parent_order_id
          , c501_total_cost, c501_ship_to, c501_ship_to_id
          , c501_ship_cost, c501_update_inv_fl, c503_invoice_id
          , c506_rma_id, c501_customer_po, c704_account_id
          , c501_order_date, c703_sales_rep_id, c501_shipping_date
          , c501_order_date_time 
           FROM t501_order t501
          WHERE t501.c501_status_fl               = 0
            AND t501.c501_void_fl                IS NULL
            AND t501.c901_ext_country_id =  1101
            AND NVL (t501.c901_order_type, - 999) = 2525;
        --
         INSERT
           INTO t253f_item_order_lock
            (
                c253f_item_order_lock_id, c250_inventory_lock_id, c501_order_id
              , c205_part_number_id, c502_item_qty, c502_control_number
              , c502_item_price, c502_void_fl, c502_item_seq_no
              , c901_type, c502_construct_fl
            )
         SELECT s253f_item_order_lock.NEXTVAL, p_inventory_lock_id, t502.c501_order_id
          , c205_part_number_id, c502_item_qty, c502_control_number
          , c502_item_price, c502_void_fl, c502_item_seq_no
          , c901_type, c502_construct_fl
           FROM t501_order t501, t502_item_order t502
          WHERE t501.c501_order_id     = t502.c501_order_id
            AND t501.c501_status_fl    = 0
            AND t501.c901_ext_country_id =  1101
            AND t501.c501_void_fl      IS NULL
            AND NVL (t501.c901_order_type, - 999) = 2525;
     
        COMMIT;
        
        -- LOAD BACK ORDER LOANER TRANSACTION DETAILS>
        INSERT INTO t253g_inhouse_transaction_lock
                    (c253g_inhouse_trans_lock_id, c250_inventory_lock_id, c412_inhouse_trans_id, c412_status_fl, c412_void_fl
                   , c412_type, c412_update_inv_fl, c412_verified_date, c412_verified_by, c412_verify_fl
                   , c412_last_updated_date, c412_last_updated_by, c901_country_id, c412_ref_id)
            SELECT s253g_inhouse_transaction_lock.NEXTVAL, p_inventory_lock_id, c412_inhouse_trans_id, c412_status_fl
                 , c412_void_fl, c412_type, c412_update_inv_fl, c412_verified_date, c412_verified_by, c412_verify_fl
                 , c412_last_updated_date, c412_last_updated_by, c901_country_id, c412_ref_id
              FROM t412_inhouse_transactions
             WHERE c412_type IN (100062, 1006572)   -- PL Back Order , IH Back Order
               AND c412_status_fl = '0'   -- Back Order
               AND c412_void_fl IS NULL
               AND c901_ext_country_id = 1101
               AND c901_country_id IS NULL;
	
        INSERT INTO t253h_inhouse_trans_items_lock
                    (c253h_inh_trans_item_lck_id, c412_inhouse_trans_id, c250_inventory_lock_id, c205_part_number_id
                   , c413_item_qty, c413_status_fl, c413_void_fl)
            SELECT s253h_inh_trans_items_lock.NEXTVAL, t412.c412_inhouse_trans_id, p_inventory_lock_id
                 , t413.c205_part_number_id, c413_item_qty, c413_status_fl, c413_void_fl
              FROM t412_inhouse_transactions t412, t413_inhouse_trans_items t413
             WHERE t412.c412_type IN (100062, 1006572)   -- PL Back Order , IH Back Order
               AND t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
               AND t412.c412_status_fl = '0'   -- Back Order
               AND t412.c412_void_fl IS NULL
               AND t413.c413_void_fl IS NULL
               AND t412.c901_ext_country_id = 1101        
               AND t412.c901_country_id IS NULL;
               
        COMMIT;   
    END gm_op_ld_pending_req_detail;




    /*************************************************************************
    * Purpose: Procedure used to load Sub Components
    *************************************************************************/
    PROCEDURE gm_op_ld_sub_components
        (
            p_inventory_lock_id IN t250_inventory_lock.c250_inventory_lock_id%TYPE
        )
    AS
        v_sub_part_num t205_part_number.c205_part_number_id%TYPE;
        v_parent_part_qty NUMBER;
        v_qty             NUMBER;
        v_pbo_qty         NUMBER;
        v_pbl_qty         NUMBER;
        v_pcs_qty         NUMBER;
        icount            NUMBER :=0;
        
        CURSOR sub_parts_cur
        IS
             SELECT t205d.c205_part_number_id pnum FROM t205d_sub_part_to_order t205d;
             
        CURSOR parent_parts_cur
        IS
             SELECT t205a.c205_from_part_number_id parent_part, t205a.c205_to_part_number_id to_part, (t205a.c205a_qty *
                NVL (PRIOR t205a.c205a_qty, 1)) qty
               FROM t205a_part_mapping t205a
                START WITH t205a.c205_to_part_number_id         = v_sub_part_num
                CONNECT BY PRIOR t205a.c205_from_part_number_id = t205a.c205_to_part_number_id
                AND t205a.c205a_void_fl is null;    ---  added new column c205a_void_fl in t205a used to PMT-50777 - Create BOM and sub-component Mapping
    BEGIN
        
        -- Inserting vale into temp table for better performance
        DELETE FROM my_t_part_list;

        INSERT INTO my_t_part_list (
           c205_part_number_id, c205_qty) 
                 SELECT t251.c205_part_number_id   , SUM (t251.c251_qty) 
                   FROM t251_inventory_lock_detail t251
                  WHERE t251.c250_inventory_lock_id = p_inventory_lock_id
        		    AND t251.c901_type    IN (20464, 20461) 
        		    AND t251.c251_qty <> 0
                GROUP BY t251.c205_part_number_id;

        FOR sub_parts_cur_details IN sub_parts_cur
        LOOP
            v_parent_part_qty            := 0;
            v_pbo_qty                    := 0;
            v_pbl_qty                    := 0;
            v_pcs_qty                    := 0;
            v_sub_part_num               := sub_parts_cur_details.pnum;
            
            FOR parent_parts_cur_details IN parent_parts_cur
            LOOP
                 -- Fetch requried value from temp table 
                 SELECT SUM (t251.c205_qty) * parent_parts_cur_details.qty
                   INTO v_qty
                   FROM my_t_part_list t251
                  WHERE t251.c205_part_number_id    = parent_parts_cur_details.parent_part ;

                v_parent_part_qty                  := v_parent_part_qty + v_qty;
                 
                /* SELECT SUM (t251.c251_qty)                             * parent_parts_cur_details.qty
                   INTO v_qty
                   FROM t251_inventory_lock_detail t251
                  WHERE t251.c250_inventory_lock_id = p_inventory_lock_id
                    AND t251.c205_part_number_id    = parent_parts_cur_details.parent_part
                    AND t251.c901_type                         IN (50567) ;
                v_pbo_qty                          := v_pbo_qty + v_qty;
                 SELECT SUM (t251.c251_qty)                     * parent_parts_cur_details.qty
                   INTO v_qty
                   FROM t251_inventory_lock_detail t251
                  WHERE t251.c250_inventory_lock_id = p_inventory_lock_id
                    AND t251.c205_part_number_id    = parent_parts_cur_details.parent_part
                    AND t251.c901_type                         IN (50568) ;
                v_pbl_qty                          := v_pbl_qty + v_qty;
                 SELECT SUM (t251.c251_qty)                     * parent_parts_cur_details.qty
                   INTO v_qty
                   FROM t251_inventory_lock_detail t251
                  WHERE t251.c250_inventory_lock_id = p_inventory_lock_id
                    AND t251.c205_part_number_id    = parent_parts_cur_details.parent_part
                    AND t251.c901_type                         IN (50569) ;
                v_pcs_qty                          := v_pcs_qty + v_qty;*/
            END LOOP;
        
            gm_pkg_oppr_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id, sub_parts_cur_details.pnum, 20477,
            v_parent_part_qty) ;
            icount := icount + 1;
            -- Commit every 1000 rows for better performance 
            IF (icount = 1000) 
            THEN
            	-- 
            	COMMIT;
            	icount := 0;
            	--
            END IF; 
            
            /*
            -- While calculating TPR for sub component it was adding the qty for PBO, PBL, PCS.
            -- Because of that the count was comming wrong. So need to hide that code.
            */
            /*
            gm_pkg_oppr_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id
            , sub_parts_cur_details.pnum
            , 50567
            , v_pbo_qty
            );
            gm_pkg_oppr_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id
            , sub_parts_cur_details.pnum
            , 50568
            , v_pbl_qty
            );
            gm_pkg_oppr_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id
            , sub_parts_cur_details.pnum
            , 50569
            , v_pcs_qty
            );
            */
        END LOOP;
    END gm_op_ld_sub_components;


    /*************************************************************************
    * Purpose: Procedure used to load part attribute information
    * currently loading information like 
    * lead time , Calculated lead time and Attribute information 
    *************************************************************************/
    PROCEDURE gm_op_ld_part_attribute 
    (
        p_inventory_lock_id IN t250_inventory_lock.c250_inventory_lock_id%TYPE
    )
    AS
        --
        v_default_leadtime NUMBER;
        v_parent_leadtime NUMBER;
        v_calc_leadtime NUMBER;
        icount number :=0;
        --
        CURSOR openleadtime_cur
        IS
            SELECT partlist.c205_part_number_id, NVL (leadtime.leadvalue, v_default_leadtime) leadtime
                 , NVL(unitcost.price,0) price ,NVL (subcomponent.subcomponentfl, 'N') subcomponentfl
              FROM my_temp_part_list partlist
                 , (SELECT t205d.c205_part_number_id, t205d.c205d_attribute_value leadvalue
                      FROM t205d_part_attribute t205d
                     WHERE t205d.c901_attribute_type = 4000101 AND t205d.c205d_void_fl IS NULL) leadtime
                 , (SELECT   t405.c205_part_number_id, MAX (NVL (t405.c405_cost_price, 0) / NVL (c405_uom_qty, 1)) price
                        FROM t405_vendor_pricing_details t405
                       WHERE t405.c405_active_fl(+) = 'Y'
                       AND t405.c405_void_fl IS NULL
                    GROUP BY t405.c205_part_number_id) unitcost
                 , (SELECT DISTINCT t205d.c205_part_number_id, 'Y' subcomponentfl
                      FROM t205d_sub_part_to_order t205d
                     WHERE t205d.c205d_void_fl IS NULL) subcomponent
             WHERE partlist.c205_part_number_id = leadtime.c205_part_number_id(+) 
               AND partlist.c205_part_number_id = unitcost.c205_part_number_id(+)
               AND partlist.c205_part_number_id = subcomponent.c205_part_number_id(+);

    BEGIN
        --
        SELECT GET_RULE_VALUE(4000101, 'LEADTIME') 
          INTO v_default_leadtime 
          FROM DUAL;
        --  
        -- load inventory info (PO DHR)
        FOR set_val IN openleadtime_cur
        LOOP
            --
            -- Save the value In shelf
            v_parent_leadtime := 0;
            IF (set_val.subcomponentfl = 'Y')
            THEN 
                SELECT NVL(SUM(leadvalue),0)
                INTO   v_parent_leadtime 
                FROM 
                    (SELECT partmappinglevel, MAX(leadvalue) leadvalue 
                       FROM  (SELECT parent_part, parentpart.partmappinglevel , NVL (t205d.c205d_attribute_value ,7) leadvalue
                             FROM t205d_part_attribute t205d, 
                            (SELECT level partmappinglevel, t205a.c205_from_part_number_id parent_part, t205a.c205_to_part_number_id to_part 
                               FROM t205a_part_mapping t205a
                             START WITH t205a.c205_to_part_number_id  = set_val.c205_part_number_id
                             CONNECT BY PRIOR t205a.c205_from_part_number_id = t205a.c205_to_part_number_id 
                             AND t205a.c205a_void_fl is null) parentpart     ---  added new column c205a_void_fl in t205a used to PMT-50777 - Create BOM and sub-component Mapping
                    WHERE parentpart.parent_part = t205d.c205_part_number_id (+)
                    AND  t205d.c901_attribute_type (+) = 4000101  
                    AND t205d.c205d_void_fl (+) IS NULL ) 
                    GROUP BY partmappinglevel );  
                
            END IF;
            
            v_calc_leadtime := set_val.leadtime + v_parent_leadtime;
            
            gm_pkg_oppr_ld_inventory.gm_op_sav_part_attribute (p_inventory_lock_id, set_val.c205_part_number_id, set_val.leadtime,
            v_calc_leadtime, set_val.price) ;
            --
            icount := icount + 1;
            -- Commit every 1000 rows for better performance 
            IF (icount = 1000) 
            THEN
            	-- 
            	COMMIT;
            	icount := 0;
            	--
            END IF;
            --             
        END LOOP;
    END gm_op_ld_part_attribute;

    /*************************************************************************
    * Purpose: Procedure used to save part attribute table in 
    * t255_part_attribute_lock table 
    *************************************************************************/
    PROCEDURE gm_op_sav_part_attribute
    (
        p_inventory_lock_id IN t255_part_attribute_lock.c250_inventory_lock_id%TYPE
      , p_part_number       IN t255_part_attribute_lock.c205_part_number_id%TYPE
      , p_leadtime          IN t255_part_attribute_lock.c255_lead_time%TYPE
      , p_calc_leadtime     IN t255_part_attribute_lock.c255_calc_lead_time%TYPE
      , p_unit_cost         IN t255_part_attribute_lock.c255_unit_cost%TYPE
    )
    AS
    BEGIN
    --

    INSERT INTO t255_part_attribute_lock
                (c255_part_attribute_lock_id, c250_inventory_lock_id, c205_part_number_id, c255_lead_time, c255_calc_lead_time, c255_unit_cost
                )
         VALUES (s255_part_attribute_lock.NEXTVAL, p_inventory_lock_id, p_part_number, p_leadtime, p_calc_leadtime, p_unit_cost
                );           
    --
    END gm_op_sav_part_attribute;

    /*************************************************************************
    * Purpose: Procedure used to save inventory details (For different type)
    *************************************************************************/
    PROCEDURE gm_op_sav_inventory_detail
        (
            p_inventory_lock_id IN t250_inventory_lock.c250_inventory_lock_id%TYPE,
            p_part_number       IN t251_inventory_lock_detail.c205_part_number_id%TYPE,
            p_type              IN t251_inventory_lock_detail.c901_type%TYPE,
            p_qty               IN t251_inventory_lock_detail.c251_qty%TYPE
        )
    AS
    BEGIN
         INSERT /*+append*/
           INTO t251_inventory_lock_detail
            (
                c251_inventory_lock_detail_id, c250_inventory_lock_id, c205_part_number_id
              , c901_type, c251_qty
            )
            VALUES
            (
                s251_inventory_lock_detail.NEXTVAL, p_inventory_lock_id, p_part_number
              , p_type, ROUND (p_qty)
            ) ;
    END gm_op_sav_inventory_detail;

    
END gm_pkg_oppr_ld_inventory;
/
