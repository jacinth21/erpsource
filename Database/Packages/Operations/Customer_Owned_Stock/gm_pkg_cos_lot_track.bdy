/* Created on 2020/08/24 12:58*/
--@"C:\PMT\Database\Packages\Operations\Customer_Owned_Stock\gm_pkg_cos_lot_track.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_cos_lot_track
IS
--	
	/*************************************************************************************************************************
		  * Description : This procedure used inset/update COS account, Part and Lot details on t5066_cos_part_lot_qty table
		  * Author		: Ramachandiran T.S 
   	***************************************************************************************************************************/
	PROCEDURE gm_pkg_cos_part_lot(
		p_ref_id  			IN  	t5066_cos_part_lot_qty.c704_account_id%TYPE,
		p_lot_id  			IN  	t5066_cos_part_lot_qty.c2550_control_number%TYPE,
		p_txn_id  			IN  	t5066_cos_part_lot_qty.c5066_last_updated_txn_id%TYPE,
		p_inv_status	  	IN  	t5066_cos_part_lot_qty.c901_current_inventory_type%TYPE,
		p_user_id           IN   	t101_user.c101_user_id%TYPE,
		p_out				OUT		VARCHAR2
	)
	AS
		v_part_num_id	t2550_part_control_number.c205_part_number_id%TYPE;
		v_lot_qty_id	t5066_cos_part_lot_qty.c5066_cos_part_lot_qty_id%TYPE;
		v_ref_id		t5066_cos_part_lot_qty.c704_account_id%TYPE;
		v_lot_number	t5066_cos_part_lot_qty.c2550_control_number%TYPE;
		v_count			NUMBER;
		v_lot_id        t2550_part_control_number.c2550_control_number%TYPE;
		v_order_id  	t501_order.c501_order_id%TYPE;
		v_item_price	t502_item_order.c502_item_price%TYPE:='0';
		v_order_date	t501_order.C501_ORDER_DATE%TYPE;
	BEGIN
	
	   v_lot_id := UPPER(p_lot_id);
	  
		BEGIN
		
		SELECT c704_account_id, C2550_CONTROL_NUMBER, COUNT(1)
		  INTO v_ref_id, v_lot_number, v_count
		  FROM t5066_cos_part_lot_qty
		 WHERE C2550_CONTROL_NUMBER = v_lot_id
		   AND c5066_void_fl IS NULL
	     GROUP BY c704_account_id, C2550_CONTROL_NUMBER;
		 
	  EXCEPTION
    		WHEN OTHERS THEN
        		v_count := 0;
    		END;
    		
    		IF v_count > 0 AND p_ref_id = v_ref_id THEN
			p_out := 'Allograft Number already scanned';
			RETURN;
			
		ELSIF v_count > 0 AND p_ref_id <> v_ref_id THEN
			p_out := 'Allograft Number already mapped with different Account';
			RETURN;
		ELSE
			p_out := '';
		END IF;

    	BEGIN	
    		
    	 SELECT
    ord_detail.orderid, ord_detail.partnum, ord_detail.price,ord_detail.ord_date
    INTO v_order_id, v_part_num_id, v_item_price,v_order_date
FROM
    (
        SELECT
            t501.c501_order_id         orderid,
            t502.c205_part_number_id   partnum,
            t502.c502_item_price       price,
			t501.C501_ORDER_DATE ord_date
        FROM
            t501_order                  t501,
            t502_item_order             t502,
            t2550_part_control_number   t2550
        WHERE
            t501.c501_order_id = t502.c501_order_id
            AND t2550.c2550_control_number = t502.c502_control_number
            AND t2550.c2550_control_number = v_lot_id
            AND ( t501.c901_order_type IN ('26240233','26240236','26240232')  -- 26240232-Direct order
             OR NVL(t501.c901_order_type, '-999') IN ('-999') )
            --AND t501.c501_ship_to_id = '16116'    --BBA Customer Owned Stock Account
            AND t2550.c205_part_number_id = t502.c205_part_number_id
            AND t501.c501_void_fl IS NULL
            AND t502.c502_void_fl IS NULL
        ORDER BY
            c501_created_date DESC
    ) ord_detail
WHERE
    ROWNUM = 1;    --rownum is added since for the data migration if lot number is shipped twice to COS account, this query will return recently shipped record
    
    EXCEPTION
    		WHEN OTHERS THEN
			BEGIN
				SELECT
                    NVL(t705.c705_unit_price,0), t2550.c205_part_number_id
                INTO v_item_price,v_part_num_id
                FROM
                    t705_account_pricing t705, t2550_part_control_number t2550
                WHERE
                    t705.c704_account_id (+) = p_ref_id
                    AND t705.c705_void_fl (+) IS NULL
					AND t2550.c205_part_number_id=t705.c205_part_number_id (+)
                    AND t2550.c2550_control_number=v_lot_id;

            EXCEPTION
                WHEN OTHERS THEN
                    v_item_price := 0;
					v_order_id :=NULL;
					v_order_date:=NULL;
            END;
	END;
    	
    		IF (v_part_num_id IS NOT NULL) THEN  --- no need
    		
    			UPDATE t5066_cos_part_lot_qty
    			   SET c901_current_inventory_type = p_inv_status
    			 WHERE c704_account_id = p_ref_id
    			   AND c2550_control_number = v_lot_id
    			   AND c205_part_number_id = v_part_num_id
    			   AND c5066_void_fl IS NULL;
    			   
    			   IF (SQL%ROWCOUNT = 0) THEN
    			   
	    			   INSERT 
	    			     INTO t5066_cos_part_lot_qty
	    			     	(
	    			     	   C704_ACCOUNT_ID, C205_PART_NUMBER_ID, C2550_CONTROL_NUMBER, C502_ITEM_PRICE
	    			     	, C5066_LAST_UPDATED_TXN_ID, C901_CURRENT_INVENTORY_TYPE, C5066_LAST_UPDATED_BY
	    			     	, C5066_LAST_UPDATED_DATE, C5066_VOID_FL ,C5066_BILLED_ORDER_ID,C5066_BILLED_ORDER_DATE
	    			     	)
	    			     	VALUES
	    			     	(
	    			     	  p_ref_id, v_part_num_id, v_lot_id, v_item_price
	    			     	,NVL(v_order_id,'COS-INVADJ'), p_inv_status, p_user_id
	    			     	,CURRENT_DATE, NULL,v_order_id,v_order_date
	    			     	);
    			   END IF;
    		END IF;
		
	END gm_pkg_cos_part_lot;	
	
	/*********************************************************************************************************************************
		  * Description : This procedure used to insert the part and lot details in T5066A_COS_PART_LOT_QTY_LOG table to maintain log 
		  * 			: and it's called by trigger.
		  * Author		: Ramachandiran T.S 
   	**********************************************************************************************************************************/
	PROCEDURE gm_pkg_cos_part_lot_qty_log(
	p_cos_part_lot_qty_id   	IN 			t5066_cos_part_lot_qty.c5066_cos_part_lot_qty_id%TYPE,
	p_ref_id					IN			t5066_cos_part_lot_qty.c704_account_id%TYPE,
	p_part_id					IN			t5066_cos_part_lot_qty.c205_part_number_id%TYPE,
	p_lot_id					IN			t5066_cos_part_lot_qty.c2550_control_number%TYPE,
	p_txn_id					IN			t5066_cos_part_lot_qty.c5066_last_updated_txn_id%TYPE,
	p_inv_status				IN			t5066_cos_part_lot_qty.c901_current_inventory_type%TYPE,
	p_created_by				IN			t5066a_cos_part_lot_qty_log.c5066a_last_updated_by%TYPE,
	p_created_dt				IN			t5066a_cos_part_lot_qty_log.c5066a_last_updated_date%TYPE
	)
	AS
	BEGIN
		
		INSERT
	       INTO t5066a_cos_part_lot_qty_log
	        (
	        	C5066_COS_PART_LOT_QTY_ID, C205_PART_NUMBER_ID
	          , C2550_CONTROL_NUMBER, C5066A_TXN_ID, C901_TXN_TYPE
	          , C5066A_LAST_UPDATED_BY, C5066A_LAST_UPDATED_DATE
	        )
	        VALUES
	        (
	           	p_cos_part_lot_qty_id, p_part_id
	          , p_lot_id, p_txn_id, p_inv_status
	          , p_created_by, p_created_dt
	        );
	
	END gm_pkg_cos_part_lot_qty_log;
	
	
	/**********************************************************************************************************
		  * Description : This procedure used to sync COS shipped records into t5066_cos_part_lot_qty table 
		  * 			: and it's called from COS portal
		  * Author		: Ramachandiran T.S 
   	 ***********************************************************************************************************/

	PROCEDURE gm_sync_cos_order(
	p_ref_id					IN			t5066_cos_part_lot_qty.c704_account_id%TYPE
	)
	AS
	v_out  VARCHAR2(100);
	v_order_id_str clob;
	CURSOR cus_cos_order
	IS
		SELECT t501.c501_order_id orderid
		 FROM t501_order t501, t907_shipping_info t907
		 WHERE t501.c704_account_id  =  p_ref_id
		   AND t501.c501_order_id    =  t907.c907_ref_id 
           AND t907.c907_ship_to_id  =  '16116' -- BoneBank Allograft Ship to id
           AND (t501.c901_order_type  IN ('26240233','26240236','26240232')  -- 26240232-Direct order
            OR NVL(t501.c901_order_type ,'-999') IN ('-999') )
           AND t907.C907_STATUS_FL = 40     --shipped 
           AND t907.c907_cos_sync_fl IS NULL
           AND t501.c501_void_fl IS NULL
           AND t907.c907_void_fl IS NULL;
          
	BEGIN
		
		FOR cos_order_dtl IN cus_cos_order
		LOOP
			
			 gm_pkg_cos_sync.gm_sav_cos_txn(cos_order_dtl.orderid);
			
			UPDATE t907_shipping_info SET c907_cos_sync_fl = 'Y'
 			   WHERE c907_ref_id = cos_order_dtl.orderid
 			  AND c907_void_fl IS NULL;
			
			 v_order_id_str := cos_order_dtl.orderid;
			 
 		     gm_pkg_cos_lot_track.gm_send_cos_sales_confirmation_email(v_order_id_str);

		END LOOP;
		
		
	END gm_sync_cos_order;	
	
	/****************************************************************************
		  * Description : This procedure used to unmap allograft from the account
		  * Author		: Vinoth
   	******************************************************************************/
	PROCEDURE gm_pkg_cos_remove_graft(
		p_ref_id  			IN  	t5066_cos_part_lot_qty.c704_account_id%TYPE,
		p_lot_id  			IN  	t5066_cos_part_lot_qty.c2550_control_number%TYPE,
		p_user_id           IN   	t101_user.c101_user_id%TYPE
	)
	AS
		v_part_num_id	t2550_part_control_number.c205_part_number_id%TYPE;
		v_ref_id		t5066_cos_part_lot_qty.c704_account_id%TYPE;
		v_lot_number	t5066_cos_part_lot_qty.c2550_control_number%TYPE;
		v_count			NUMBER;
	BEGIN
	
    	UPDATE t5066_cos_part_lot_qty
    	   SET c5066_void_fl = 'Y',
    	   	   c5066_last_updated_by = p_user_id,
    	   	   c5066_last_updated_date = SYSDATE
    	 WHERE c704_account_id = p_ref_id
    	   AND c2550_control_number = p_lot_id
    	   AND c5066_void_fl IS NULL;
		
	END gm_pkg_cos_remove_graft;	
	
	
	/*************************************************************************
 * Description : This procedure is used to send the email to BoneBank Allograft after COS sync done
 *  **************************************************************************/

    PROCEDURE gm_send_cos_sales_confirmation_email(
    p_order_id_str  IN CLOB
    )
    
    AS

        v_email_to      t906_rules.c906_rule_value%TYPE;
        v_email_msg     VARCHAR2(4000);
        v_email_sub     t906_rules.c906_rule_value%TYPE;
        v_str           CLOB;
        v_mail_body     CLOB;
        v_col_str       VARCHAR2(1000);
        v_end_row_str   VARCHAR2(1000);
        v_table_str     CLOB;
        v_add_notes     VARCHAR2(4000);
        

    BEGIN
	    
		  SELECT get_rule_value('COS_SALES_EMAIL_SUB', 'COS_RULE_GRP')
            INTO v_email_sub 
            FROM dual;

        SELECT get_rule_value('COS_INV_SUM_EMAIL_TO', 'COS_RULE_GRP')
        INTO v_email_to
        FROM dual;
        	v_email_sub := v_email_sub || ' - ' || p_order_id_str;
        	
        v_mail_body := '<TABLE <style>TD{ FONT-SIZE: 11px; COLOR: #000000; FONT-FAMILY: verdana, arial, sans-serif;}</style><font face=arial size="2"></font>';
        v_mail_body := v_mail_body || v_email_sub;
         v_mail_body := v_mail_body || '</TABLE> <br><br>';
        
          -- call the email procedure to send email to the user
        gm_com_send_html_email(v_email_to, v_email_sub, NULL, v_mail_body);
    END gm_send_cos_sales_confirmation_email;
	
END gm_pkg_cos_lot_track;
/