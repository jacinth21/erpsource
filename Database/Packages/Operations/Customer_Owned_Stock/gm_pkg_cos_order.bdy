CREATE OR REPLACE PACKAGE BODY gm_pkg_cos_order IS 

/*************************************************************************
  * Description : This procedure is used to validate the order id and insert/update the order id , 
  * billto, ship to details to t5067 table
 *  **************************************************************************/

    PROCEDURE gm_validate_cos_order (
       -- p_order_id   IN    t5067_cos_order.c5067_cos_order_id%TYPE,
        p_bill_to    IN    t5067_cos_order.c5067_cos_bill_to_id%TYPE,
        p_ship_to    IN    t5067_cos_order.c5067_cos_ship_to_id%TYPE,
        p_user_id    IN    t5067_cos_order.c5067_last_updated_by%TYPE,
        p_out_msg    OUT   VARCHAR2
    ) AS
        v_order_id t5067_cos_order.c5067_cos_order_id%TYPE;
        v_date_fmt VARCHAR2(50);
    BEGIN
        --BEGIN
           -- SELECT c5067_cos_order_id INTO v_order_id FROM t5067_cos_order
            --WHERE c5067_cos_order_id = p_order_id AND c5067_void_fl IS NULL;

       -- EXCEPTION WHEN OTHERS THEN v_order_id := NULL;
       -- END;
       
        v_date_fmt := TO_CHAR(CURRENT_DATE,'yymmdd');
     
        SELECT 'COS-'|| v_date_fmt || '-'||s5067_cos_order.NEXTVAL INTO v_order_id FROM DUAL;
     
            INSERT INTO t5067_cos_order (
                c5067_cos_order_id,
                c5067_cos_bill_to_id,
                c5067_cos_ship_to_id,
                c5067_cos_order_date,
                c5067_created_by,
                c5067_created_date,
		c901_cos_order_status
            ) VALUES (
                v_order_id,
                p_bill_to,
                p_ship_to,
                current_date,
                p_user_id,
                current_date,
		        110660 -- Order Status Draft

            );

            p_out_msg := v_order_id;
    END gm_validate_cos_order;
/*************************************************************************
  * Description : This procedure is used to save the order part details to t5067a
  * 
 *  **************************************************************************/

    PROCEDURE gm_save_cos_order_part (
        p_order_id   IN    t5067_cos_order.c5067_cos_order_id%TYPE,
        p_bill_to    IN    t5067_cos_order.c5067_cos_bill_to_id%TYPE,
        p_part_num   IN    t5067a_cos_order_item.c205_part_number%TYPE,
        p_qty        IN    t5067a_cos_order_item.c5067a_qty%TYPE,
        p_user_id    IN    t5067_cos_order.c5067_last_updated_by%TYPE,
        p_out_msg    OUT   VARCHAR2
    ) AS
        v_part_price     t5066_cos_part_lot_qty.C502_ITEM_PRICE%TYPE;
        v_sheft_qty      NUMBER;
        v_order_status   t5067_cos_order.c901_cos_order_status%TYPE;
    BEGIN

        SELECT
            COUNT(C2550_CONTROL_NUMBER)
        INTO v_sheft_qty
        FROM
            t5066_cos_part_lot_qty
        WHERE
            c704_account_id = p_bill_to
            AND c205_part_number_id = p_part_num
            AND c5066_void_fl IS NULL;
	
	
 BEGIN
        SELECT
            c901_cos_order_status
        INTO v_order_status
        FROM
            t5067_cos_order
        WHERE
            c5067_cos_order_id = p_order_id
            AND c5067_void_fl IS NULL;
        EXCEPTION
          WHEN OTHERS THEN
          v_order_status :=NULL;
          END;   

        IF p_qty <= v_sheft_qty THEN

		-- If the Order Status is controlled and if the order qty is updated changing the order status to order intiated.
      		  IF ( v_order_status = 110614 ) THEN  -- Controlled status
              		  UPDATE t5067_cos_order
              		  SET
                	    c901_cos_order_status = 110613,	---- COS Order Initiated
                    	    c5067_last_updated_by = p_user_id,
                            c5067_last_updated_date = current_date
                          WHERE
                            c5067_cos_order_id = p_order_id
                            AND c5067_void_fl IS NULL;

                  END IF;

            UPDATE t5067a_cos_order_item
            SET
                c205_part_number = p_part_num,
                c5067a_qty = p_qty,
                c5067a_part_price = v_part_price,
                c5067a_last_updated_by = p_user_id,
                c5067a_last_updated_date = current_date
            WHERE
                c5067_cos_order_id = p_order_id
                AND c205_part_number = p_part_num
                AND c5067a_void_fl IS NULL
				AND C2550_CONTROL_NUMBER IS NULL;

    

            IF ( SQL%rowcount = 0 ) THEN
                INSERT INTO t5067a_cos_order_item (                  
                    c5067_cos_order_id,
                    c205_part_number,
                    c5067a_qty,
                    c5067a_part_price,
                    c5067a_last_updated_by,
                    c5067a_last_updated_date
                ) VALUES (
                    p_order_id,
                    p_part_num,
                    p_qty,
                    0,
                    p_user_id,
                    current_date
                );

            END IF;

            
   
        ELSE
            p_out_msg := 'Qty should be less than or equal to shef qty';
        END IF;
 
    END gm_save_cos_order_part;
/*************************************************************************
* Description : This procedure is used update the order total cost  
***************************************************************************/

    PROCEDURE gm_update_order_total_cost (
        p_order_id   IN   t5067_cos_order.c5067_cos_order_id%TYPE,
        p_user_id    IN   t5067_cos_order.c5067_last_updated_by%TYPE
    ) AS

        v_total_price t5067_cos_order.C5067_TOTAL_PRICE%TYPE;

     BEGIN
	 
      SELECT sum(((COUNT (C5067A_QTY)) * C5067A_PART_PRICE))
      INTO v_total_price
      FROM t5067a_cos_order_item
	WHERE C5067_COS_ORDER_ID=p_order_id
        AND C2550_CONTROL_NUMBER IS NOT NULL
        AND  C5067A_VOID_FL IS NULL
	GROUP BY C5067A_PART_PRICE ;

    BEGIN

         UPDATE t5067_cos_order
            SET
                C5067_TOTAL_PRICE  = v_total_price,
                C5067_LAST_UPDATED_BY  = p_user_id,
                C5067_LAST_UPDATED_DATE = current_date
            WHERE
                c5067_cos_order_id = p_order_id
                AND C5067_VOID_FL  IS NULL;
  END;   
   
END gm_update_order_total_cost;
/*************************************************************************
* Description : This procedure is used update the order details to t5067, t5068 table  
***************************************************************************/

    PROCEDURE gm_update_cos_order (
        p_order_id           IN   t5067_cos_order.c5067_cos_order_id%TYPE,
        p_po_num             IN   t5067_cos_order.c5067_cos_po_number%TYPE,
        p_ship_date          IN   t5068_cos_shipping.c5067_cos_planned_shipped_date%TYPE,
        p_courier_mode       IN   t5068_cos_shipping.c5067_carrier_mode%TYPE,
        p_courier_id         IN   t5068_cos_shipping.c5067_carrier_id%TYPE,
        p_courier_acct_id    IN   t5068_cos_shipping.c5067_carrier_acct_no%TYPE,
        p_email_id           IN   t5068_cos_shipping.c5067_ship_to_email%TYPE,
        p_comments           IN   t5068_cos_shipping.c5067_comments%TYPE,
        p_user_id            IN   t5067_cos_order.c5067_last_updated_by%TYPE,
        p_delivery_slip_fl   IN   t5068_cos_shipping.c5067_delivery_slip_fl%TYPE,
        p_comm_invoice_fl    IN   t5068_cos_shipping.c5067_comm_invoice_fl%TYPE,
        p_cust_shipment_fl   IN   t5068_cos_shipping.c5067_customer_shipment_email_fl%TYPE,
        p_bill_to            IN   t5067_cos_order.c5067_cos_bill_to_id%TYPE,
        p_ship_to            IN   t5067_cos_order.c5067_cos_ship_to_id%TYPE,
        p_cos_attn           IN   t5067_cos_order.c5067_cos_attn%TYPE
    ) AS
       

    BEGIN
        UPDATE t5068_cos_shipping
        SET
            c5067_cos_planned_shipped_date = p_ship_date,
            c5067_carrier_mode = p_courier_mode,
            c5067_carrier_id = p_courier_id,
            c5067_ship_to_email = p_email_id,
            c5067_comments = p_comments,
            c5067_delivery_slip_fl = p_delivery_slip_fl,
            c5067_comm_invoice_fl = p_comm_invoice_fl,
            c5067_customer_shipment_email_fl = p_cust_shipment_fl,
            c5067_carrier_acct_no = p_courier_acct_id,
            c5068_cos_ship_last_updated_by = p_user_id,
            c5068_cos_ship_last_updated_date = current_date
        WHERE
            c5067_cos_order_id = p_order_id
            AND c5068_void_fl IS NULL;

        IF ( SQL%rowcount = 0 ) THEN
            INSERT INTO t5068_cos_shipping (c5067_cos_order_id,c5067_cos_planned_shipped_date,c5067_carrier_mode,c5067_carrier_id,
                            c5067_ship_to_email,c5067_comments,c5067_delivery_slip_fl,c5067_comm_invoice_fl, c5067_customer_shipment_email_fl,
                            c5067_carrier_acct_no,c5068_cos_ship_last_updated_by,c5068_cos_ship_last_updated_date
            ) VALUES (p_order_id,p_ship_date,p_courier_mode,p_courier_id,
                p_email_id,p_comments,p_delivery_slip_fl, p_comm_invoice_fl,p_cust_shipment_fl,
                p_courier_acct_id, p_user_id, current_date
            );

        END IF;

        UPDATE t5067_cos_order
        SET
            c5067_cos_po_number = p_po_num,
            c901_cos_order_status = 110613, -- COS Order Initiated
            c5067_cos_bill_to_id = p_bill_to,
            c5067_cos_ship_to_id = p_ship_to,
            c5067_last_updated_by = p_user_id,
            c5067_last_updated_date = current_date,
            c5067_cos_attn         = p_cos_attn
        WHERE
            c5067_cos_order_id = p_order_id
            AND c5067_void_fl IS NULL;
 
    END gm_update_cos_order;
/*************************************************************************
* Description : This procedure is remove the selected part from order  
***************************************************************************/

    PROCEDURE gm_remove_cos_order_part_dtl (
        p_order_id      IN   t5067_cos_order.c5067_cos_order_id%TYPE,
        p_cos_item_id   IN   t5067a_cos_order_item.c5067a_cos_item_id%TYPE,
        p_user_id       IN   t5067a_cos_order_item.c5067a_last_updated_by%TYPE
    ) AS
	
	V_PART_NUM t5067a_cos_order_item.C205_PART_NUMBER%TYPE;
    BEGIN
	
	-- updating the Lot Number Inventory type to COS-BILLED

		BEGIN	
		
		SELECT C205_PART_NUMBER
     		INTO V_PART_NUM
		FROM t5067a_cos_order_item
	    	WHERE c5067a_cos_item_id = p_cos_item_id
		AND c5067a_void_fl IS NULL;
		
		EXCEPTION WHEN OTHERS THEN
		V_PART_NUM:=NULL;
		RETURN;
		END;
		
		UPDATE T5066_COS_PART_LOT_QTY
			SET C901_CURRENT_INVENTORY_TYPE='110600', -- 110600 COS-BILLED
			C5066_LAST_UPDATED_BY        =p_user_id ,
			C5066_LAST_UPDATED_DATE      =CURRENT_DATE
		WHERE C2550_CONTROL_NUMBER    IN
			(SELECT T5067A.C2550_CONTROL_NUMBER
				FROM T5067A_COS_ORDER_ITEM T5067A
				WHERE T5067A.C5067_COS_ORDER_ID=p_order_id
				AND T5067A.C205_PART_NUMBER = V_PART_NUM
				AND T5067A.C5067A_VOID_FL     IS NULL
				AND T5067A.C2550_CONTROL_NUMBER IS NOT NULL
			);

		
		
        UPDATE t5067a_cos_order_item
        SET
            c5067a_void_fl = 'Y',
            c5067a_last_updated_by = p_user_id,
            c5067a_last_updated_date = current_date
        WHERE
            c5067_cos_order_id = p_order_id
			AND C205_PART_NUMBER = V_PART_NUM
			AND c5067a_void_fl IS NULL;
			
        UPDATE t5067_cos_order
           SET c901_cos_order_status = 110613, -- COS Order Initiated
               c5067_last_updated_by = p_user_id,
               c5067_last_updated_date = current_date
         WHERE c5067_cos_order_id = p_order_id
          AND c5067_void_fl IS NULL;
			
		-- Calling the below procedure to update the total price since the lot number is removed from the order controlling.
		gm_pkg_cos_order.gm_update_order_total_cost(p_order_id,p_user_id);
  
    END gm_remove_cos_order_part_dtl;

/*************************************************************************
* Description : This procedure is update the order qty of part number and order id  
***************************************************************************/

    PROCEDURE gm_upd_order_qty (
        p_part_num   IN    t5067a_cos_order_item.c205_part_number%TYPE,
        p_item_id    IN    t5067a_cos_order_item.c5067a_cos_item_id%TYPE,
        p_order_id   IN    t5067_cos_order.c5067_cos_order_id%TYPE,
        p_bill_to    IN    t5067_cos_order.c5067_cos_bill_to_id%TYPE,
        p_qty        IN    t5067a_cos_order_item.c5067a_qty%TYPE,
        p_user_id    IN    t5067_cos_order.c5067_last_updated_by%TYPE,
        p_out_msg    OUT   VARCHAR2
    ) AS
        v_part_price     NUMBER;
        v_sheft_qty      NUMBER;
        v_order_status   t5067_cos_order.c901_cos_order_status%TYPE;
    BEGIN
        SELECT
            COUNT(C2550_CONTROL_NUMBER)
        INTO v_sheft_qty
        FROM
            t5066_cos_part_lot_qty
        WHERE
            c704_account_id = p_bill_to
            AND c205_part_number_id = p_part_num
            AND c5066_void_fl IS NULL;

	BEGIN
          SELECT
            c901_cos_order_status
        INTO v_order_status
        FROM
            t5067_cos_order
        WHERE
            c5067_cos_order_id = p_order_id
            AND c5067_void_fl IS NULL;

	EXCEPTION WHEN OTHERS THEN
	 v_order_status:=NULL;
	END;

        IF ( p_qty > v_sheft_qty ) THEN
            p_out_msg := 'Entered Qty Greater than Shelf qty';
        ELSE
            IF ( v_order_status = 110613 OR v_order_status = 110660 OR v_order_status = 110614 ) THEN
                UPDATE t5067a_cos_order_item
                SET
                    c205_part_number = p_part_num,
                    c5067a_qty = p_qty,
                    c5067a_part_price = v_part_price,
                    c5067a_last_updated_by = p_user_id,
		    c5067a_last_updated_date = current_date
                WHERE
                    c5067_cos_order_id = p_order_id
                    AND c205_part_number = p_part_num
                    AND c5067a_cos_item_id = p_item_id
                    AND c5067a_void_fl IS NULL;

                
            END IF;

            IF ( v_order_status = 110614 ) THEN
      --If Order Staus as controlled, update order status as COS Order Initiated
                UPDATE t5067_cos_order
                SET
                    c901_cos_order_status = 110613, -- COS Order Initiated
                    c5067_last_updated_by = p_user_id,
                    c5067_last_updated_date = current_date
                WHERE
                    c5067_cos_order_id = p_order_id
                    AND c5067_void_fl IS NULL;

            END IF;--t5067 table update END

        END IF;  --qty compare IF END

    END gm_upd_order_qty;
/*************************************************************************
* Description : This procedure is void the order  
***************************************************************************/

    PROCEDURE gm_void_cos_order (
        p_order_id   IN   t5067_cos_order.c5067_cos_order_id%TYPE,
        p_user_id    IN   t5067_cos_order.c5067_last_updated_by%TYPE
    ) AS
    BEGIN
	
	-- Updating the Lot status to COS-BILLED
	
	UPDATE t5066_cos_part_lot_qty
        SET 
            c901_current_inventory_type = 110600, --COS_BILLED
            c5066_last_updated_by = p_user_id,
            c5066_last_updated_date = current_date
       WHERE 
			(c5066_last_updated_txn_id = p_order_id
			OR
			  C2550_CONTROL_NUMBER    IN
				   (SELECT T5067A.C2550_CONTROL_NUMBER
					FROM T5067A_COS_ORDER_ITEM T5067A
					WHERE T5067A.C5067_COS_ORDER_ID=p_order_id
					AND T5067A.C5067A_VOID_FL     IS NULL
					AND T5067A.C2550_CONTROL_NUMBER IS NOT NULL
					))
            AND c5066_void_fl IS NULL;
			
	-- COS Order Table
        UPDATE t5067_cos_order
        SET
            c5067_void_fl = 'Y',
            c901_cos_order_status = 26241145,
            c5067_last_updated_by = p_user_id,
            c5067_last_updated_date = current_date
        WHERE
            c5067_cos_order_id = p_order_id
            AND c5067_void_fl IS NULL;
      -- Item Order table

        UPDATE t5067a_cos_order_item
        SET
            c5067a_void_fl = 'Y',
            c5067a_last_updated_by = p_user_id,
            c5067a_last_updated_date = current_date
        WHERE
            c5067_cos_order_id = p_order_id
            AND c5067a_void_fl IS NULL;
		   --Shipping Table

        UPDATE t5068_cos_shipping
        SET
            c5068_void_fl = 'Y',
            c5068_cos_ship_last_updated_by = p_user_id,
            c5068_cos_ship_last_updated_date = current_date
        WHERE
            c5067_cos_order_id = p_order_id
            AND c5068_void_fl IS NULL;
            
	
    
    END gm_void_cos_order;

/*************************************************************************************************************************
		  * Description : This procedure used inset/update Scanned Allograft NUMBER
		  * Author		: Vinoth
   	***************************************************************************************************************************/

    PROCEDURE gm_update_order_control_number (
        p_order_id    IN    t5067a_cos_order_item.c5067_cos_order_id%TYPE,
        p_lotnumber   IN    t5067a_cos_order_item.c2550_control_number%TYPE,
        p_user_id     IN    t5067a_cos_order_item.c5067a_last_updated_by%TYPE,
        p_out_msg     OUT   VARCHAR2
    ) AS

        v_lot_part_count     NUMBER;
        v_lot_acc_count      NUMBER;
        v_lot_count          NUMBER;
        v_lot_exp_count      NUMBER;
		v_lot_expiry_count   NUMBER;
        v_scanned_part_qty   NUMBER;
        v_order_qty          NUMBER;
        v_part_number        t5067a_cos_order_item.c205_part_number%TYPE;
        v_part_price         t5067a_cos_order_item.c5067a_part_price%TYPE;
        v_qty                t5067a_cos_order_item.c5067a_qty%TYPE;
        v_total_price        t5067_cos_order.C5067_TOTAL_PRICE%TYPE;
        v_lot_num            t5067a_cos_order_item.c2550_control_number%type;
        v_lot_status_cnt     NUMBER;
		v_expiry_day         t906_rules.c906_rule_value%type;
		
    BEGIN
       v_lot_num := UPPER(p_lotnumber);
		--To validate the lot is available for the part with the associated order 
        SELECT COUNT(1) INTO v_lot_part_count
         FROM t5067a_cos_order_item
        WHERE c5067_cos_order_id = p_order_id
          AND c205_part_number IN (
                SELECT c205_part_number_id
                FROM t5066_cos_part_lot_qty
                WHERE C2550_CONTROL_NUMBER = v_lot_num
                  AND c5066_void_fl IS NULL
               )
            AND c5067a_void_fl IS NULL;

        IF v_lot_part_count = 0 THEN
            p_out_msg := 'Allograft Scanned is not mapped with the Ordered Part';
            return;
        END IF;
         
         --To validate the lot is available for the associated account 

        SELECT COUNT(1) INTO v_lot_acc_count
          FROM t5067_cos_order
         WHERE c5067_cos_order_id = p_order_id
           AND c5067_cos_bill_to_id IN (
                SELECT c704_account_id
                FROM t5066_cos_part_lot_qty
                WHERE C2550_CONTROL_NUMBER = v_lot_num
                  AND c5066_void_fl IS NULL
             )
            AND c5067_void_fl IS NULL;

        IF v_lot_acc_count = 0 THEN
            p_out_msg := 'Allograft not available in the associated account';
            return;
        END IF;
         
        --To check whether the allograft is already scanned

        SELECT COUNT(1)
          INTO v_lot_count FROM t5067a_cos_order_item
         WHERE c5067_cos_order_id = p_order_id
           AND c2550_control_number = v_lot_num
           AND c5067a_void_fl IS NULL;
            
        IF v_lot_count > 0 THEN
            p_out_msg := 'Allograft is already scanned';
            return;
        END IF;
        
        --To check whether the allograft is expired
	v_expiry_day := get_rule_value('COS_LOT_EXPIRY_DAY','COS_RULE_GRP');

        SELECT COUNT(1) INTO v_lot_exp_count
        FROM t2550_part_control_number
       WHERE c2550_expiry_date <= CURRENT_DATE
          AND c2550_control_number = v_lot_num;

        IF v_lot_exp_count > 0 THEN
            p_out_msg := 'Allograft is already expired';
            return;
        END IF;
		
		    SELECT COUNT(1) INTO v_lot_expiry_count
             FROM t2550_part_control_number
            WHERE c2550_expiry_date >= CURRENT_DATE +v_expiry_day
          AND c2550_control_number = v_lot_num;

        IF v_lot_expiry_count =0 THEN
            p_out_msg := 'Allograft scanned is going to Expiry in less than 30 days';
            return;
        END IF;	
          
      
              BEGIN
                SELECT c205_part_number_id, c502_item_price
		           INTO v_part_number, v_part_price
                  FROM t5066_cos_part_lot_qty
                 WHERE C2550_CONTROL_NUMBER = v_lot_num
                   AND c5066_void_fl IS NULL;

              EXCEPTION WHEN OTHERS THEN
		 p_out_msg := 'Allograft not available in COS inventory';
            	RETURN;
	      END;
             
        SELECT
            COUNT(c2550_control_number)
        INTO v_scanned_part_qty
        FROM
            t5067a_cos_order_item
        WHERE
            c205_part_number = v_part_number
            AND c2550_control_number IS NOT NULL
            AND c5067_cos_order_id = p_order_id
            AND c5067a_void_fl IS NULL;

        SELECT
            c5067a_qty
        INTO v_order_qty
        FROM
            t5067a_cos_order_item
        WHERE
            c205_part_number = v_part_number
            AND c2550_control_number IS NULL
            AND c5067_cos_order_id = p_order_id
            AND c5067a_void_fl IS NULL;

        IF v_order_qty <= v_scanned_part_qty THEN
            p_out_msg := 'Cannot Scan more than the Qty ordered for the part number';
            RETURN;
        END IF;
          
        SELECT COUNT(1) INTO v_lot_status_cnt
         FROM t5066_cos_part_lot_qty
        WHERE c2550_control_number = v_lot_num
          AND C901_CURRENT_INVENTORY_TYPE = 110620  -- COS Shipped
          AND c5066_void_fl IS NULL;
           
         IF v_lot_status_cnt > 0 THEN
            p_out_msg := 'Allograft is already Shipped';
            RETURN;
         END IF;
        
        IF ( p_out_msg IS NULL ) THEN
        
            INSERT INTO t5067a_cos_order_item (c5067_cos_order_id,c205_part_number, c2550_control_number, c5067a_part_price, c5067a_qty
                                               ) 
                     VALUES (p_order_id,v_part_number, v_lot_num, v_part_price,1);

        END IF;
 
    END gm_update_order_control_number;
	
	/****************************************************************************
		  * Description : This procedure used to remove allograft from the cos item order
		  * Author		: Vinoth
   	******************************************************************************/

    PROCEDURE gm_remove_order_control_number (
        p_ord_item_id   IN   t5067a_cos_order_item.c5067a_cos_item_id%TYPE,
        p_user_id       IN   t5067a_cos_order_item.c5067a_last_updated_by%TYPE
    ) AS
    BEGIN
	
		-- updating the Lot Number Inventory type to COS-BILLED

		UPDATE T5066_COS_PART_LOT_QTY
			SET C901_CURRENT_INVENTORY_TYPE='110600', -- 110600 COS-BILLED
			C5066_LAST_UPDATED_BY        =p_user_id ,
			C5066_LAST_UPDATED_DATE      =CURRENT_DATE
		WHERE C2550_CONTROL_NUMBER    IN
			(SELECT T5067A.C2550_CONTROL_NUMBER
				FROM T5067A_COS_ORDER_ITEM T5067A
				WHERE T5067A.C5067A_COS_ITEM_ID=p_ord_item_id
				AND T5067A.C5067A_VOID_FL     IS NULL
			);
			
        UPDATE t5067a_cos_order_item
        SET
            c5067a_void_fl = 'Y',
            c5067a_last_updated_by = p_user_id,
            c5067a_last_updated_date = sysdate
        WHERE
            c5067a_cos_item_id = p_ord_item_id
    	    AND c5067a_void_fl IS NULL;
 
    END gm_remove_order_control_number;	
	
	/*********************************************************************************************
		  * Description : This procedure used to save order Status as controlled while controlling
		  * Author		: Vinoth
   	*******************************************************************************************/

    PROCEDURE gm_save_cos_order_control (
        p_order_id   IN    t5067_cos_order.c5067_cos_order_id%TYPE,
        p_user_id    IN    t5067_cos_order.c5067_last_updated_by%TYPE,
        p_out_msg    OUT   VARCHAR2
    ) AS

        v_scanned_part_qty   NUMBER;
        v_order_qty          NUMBER;
        CURSOR curs_allograft_detail IS
        
        SELECT t5067.c5067_cos_bill_to_id accid, t5067a.c205_part_number partnum, t5067a.c2550_control_number lotnum
         FROM t5067_cos_order t5067, t5067a_cos_order_item t5067a
        WHERE t5067.c5067_cos_order_id = t5067a.c5067_cos_order_id
          AND t5067.c5067_cos_order_id = p_order_id
          AND t5067a.c2550_control_number IS NOT NULL
          AND t5067.c5067_void_fl IS NULL
          AND t5067a.c5067a_void_fl IS NULL;

    BEGIN
        FOR allograft_detail IN curs_allograft_detail
        LOOP
        
            SELECT COUNT(c2550_control_number) INTO v_scanned_part_qty
            FROM t5067a_cos_order_item
            WHERE
                c205_part_number = allograft_detail.partnum
                AND c2550_control_number IS NOT NULL
                AND c5067_cos_order_id = p_order_id
                AND c5067a_void_fl IS NULL;

            SELECT
                c5067a_qty
            INTO v_order_qty
            FROM
                t5067a_cos_order_item
            WHERE
                c205_part_number = allograft_detail.partnum
                AND c2550_control_number IS NULL
                AND c5067_cos_order_id = p_order_id
                AND c5067a_void_fl IS NULL;

            IF v_scanned_part_qty < v_order_qty THEN
                p_out_msg := 'Qty controlled is lesser than the qty requested';
                return;
            END IF;
		
	--Update the order status as Controlled in t5067_cos_order
            UPDATE t5067_cos_order
            SET
                c901_cos_order_status = 110614,   -- 110614 - Controlled
                c5067_last_updated_by = p_user_id,
                c5067_last_updated_date = current_date
            WHERE
                c5067_cos_order_id = p_order_id
                AND c5067_void_fl IS NULL;
	
	--update Allograft status as "controlled" in T5066_COS_PART_LOT_QTY table for the input order
	
            UPDATE t5066_cos_part_lot_qty
            SET
                c901_current_inventory_type = 110601,    --COS-PENDING SHIPPING
                c5066_last_updated_txn_id = p_order_id,
                c5066_last_updated_by = p_user_id,
                c5066_last_updated_date = sysdate
            WHERE
                c704_account_id = allograft_detail.accid
                AND c205_part_number_id = allograft_detail.partnum
                AND C2550_CONTROL_NUMBER = allograft_detail.lotnum
                AND c5066_void_fl IS NULL;

        END LOOP;
	-- Call below Procedure to update total cost for the order id.
	gm_pkg_cos_order.gm_update_order_total_cost(p_order_id, p_user_id);
	 
    END gm_save_cos_order_control;

END gm_pkg_cos_order;
/