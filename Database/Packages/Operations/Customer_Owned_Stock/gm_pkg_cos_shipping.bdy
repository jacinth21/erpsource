/* Created on 2020/08/24 12:58*/
--@"C:\PMT\Database\Packages\Operations\Customer_Owned_Stock\gm_pkg_cos_shipping.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_cos_shipping
IS
--	
	/*************************************************************************************************************************
		  * Description : This procedure used inset/update COS account, Part and Lot details on t5066_cos_part_lot_qty table
		  * Author		: Vinoth
   	***************************************************************************************************************************/
	PROCEDURE gm_save_cos_shipping(
		p_order_id 				IN  t5068_cos_shipping.c5067_cos_order_id%TYPE,
		p_ship_box_dim			IN  t5068_cos_shipping.c5068_ship_box_dim_id%TYPE,
		p_ice_weight  			IN  t5068_cos_shipping.c5068_ship_dry_ice_weight%TYPE,
		p_tracking_number		IN	t5068_cos_shipping.c5068_ship_tracking_number%TYPE,
		p_user_id				IN 	t5068_cos_shipping.c5068_cos_ship_last_updated_by%TYPE
	)
	AS
	
	CURSOR curs_allograft_detail
	    IS
			SELECT t5067.c5067_cos_bill_to_id accid, t5067a.c205_part_number partnum, t5067a.c2550_control_number lotnum 
			  FROM t5067_cos_order t5067, t5067a_cos_order_item t5067a
			 WHERE t5067.c5067_cos_order_id = t5067a.c5067_cos_order_id 
			   AND t5067.c5067_cos_order_id = p_order_id
			   AND t5067a.c2550_control_number IS NOT NULL
			   AND t5067.c5067_void_fl IS NULL 
			   AND t5067a.c5067a_void_fl IS NULL; 
	BEGIN


		--update the status as Shipped and other details  in cos shipping table 
		UPDATE t5068_cos_shipping
		   SET c5068_ship_box_dim_id = p_ship_box_dim,
		   	   c5068_ship_dry_ice_weight = p_ice_weight,
		   	   c5068_ship_tracking_number = p_tracking_number,
		   	   c5068_cos_ship_status = 110615,
		   	   c5067_cos_planned_shipped_date = CURRENT_DATE,
		   	   c5068_cos_ship_last_updated_by = p_user_id,
		   	   c5068_cos_ship_last_updated_date = CURRENT_DATE
		 WHERE c5067_cos_order_id = p_order_id
		   AND c5068_void_fl IS NULL;
		 
		 --update the status as Shipped in cos order table 
		UPDATE t5067_cos_order
		   SET c901_cos_order_status = 110615,     -- 110615 COS Order Shipped
		   	   c5067_last_updated_by = p_user_id,
		   	   c5067_last_updated_date = CURRENT_DATE
		 WHERE c5067_cos_order_id = p_order_id
		   AND c5067_void_fl IS NULL;

 --void record from cos item order table which not having control number
		 UPDATE t5067a_cos_order_item
		    SET c5067a_void_fl = 'Y',
		    	c5067a_last_updated_by = p_user_id,
		    	c5067a_last_updated_date = CURRENT_DATE
		  WHERE c5067_cos_order_id = p_order_id
		    AND c2550_control_number IS NULL
		    AND c5067a_void_fl IS NULL;

		FOR allograft_detail IN curs_allograft_detail
		LOOP
		
		
		--Update inventory type and last updated txn id in allograft master table for the lotnumber, account and part number in order   
		UPDATE t5066_cos_part_lot_qty  
		   SET C901_CURRENT_INVENTORY_TYPE = 110620,   -- 110620 COS-SHIPPED
		       C5066_LAST_UPDATED_TXN_ID = p_order_id,
		       C5066_LAST_UPDATED_BY = p_user_id,
		       C5066_LAST_UPDATED_DATE = CURRENT_DATE
		 WHERE c704_account_id = allograft_detail.accid
		   AND c205_part_number_id = allograft_detail.partnum
		   AND C2550_CONTROL_NUMBER = allograft_detail.lotnum
		   AND c5066_void_fl IS NULL; 
		    			  
		 END LOOP;
	 		
	END gm_save_cos_shipping;
	
	
END gm_pkg_cos_shipping;
/