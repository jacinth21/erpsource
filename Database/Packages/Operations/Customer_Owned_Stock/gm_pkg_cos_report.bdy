CREATE OR REPLACE PACKAGE BODY gm_pkg_cos_report IS 

/*************************************************************************
 * Description : This procedure is used to send the COS summary details mail
 *  **************************************************************************/

    PROCEDURE gm_send_email_summary_report (
        p_accountid   IN   t5066_cos_part_lot_qty.c704_account_id%TYPE,
        p_notes		  IN   VARCHAR2
    ) 
    
    AS

        v_email_to      t906_rules.c906_rule_value%TYPE;
        v_email_msg     VARCHAR2(4000);
        v_email_sub     t906_rules.c906_rule_value%TYPE;
        v_str           CLOB;
        v_mail_body     CLOB;
        v_col_str       VARCHAR2(1000);
        v_end_row_str   VARCHAR2(1000);
        v_table_str     CLOB;
        v_add_notes     VARCHAR2(4000);
         v_account_name t704_account.c704_account_nm%type;
        
        CURSOR fetch_summary_dtls IS
        SELECT
            t5066.c205_part_number_id partnum,
            regexp_replace(t205.c205_part_num_desc, '[^0-9A-Za-z]', ' ') partdesc,
            COUNT(t5066.C2550_CONTROL_NUMBER) qty,
            sum(t5066.c502_item_price) price
        FROM
            t5066_cos_part_lot_qty   t5066,
    		t205_part_number         t205,
    		t704_account             t704
        WHERE
            t5066.c704_account_id = t704.c704_account_id
		    AND t5066.c205_part_number_id = t205.c205_part_number_id
		    AND t5066.c704_account_id = p_accountid
		    AND t5066.c5066_void_fl IS NULL
		    AND t704.c704_void_fl IS NULL
		GROUP BY
		    t5066.c205_part_number_id,
		    t205.c205_part_num_desc
		    ORDER BY t5066.c205_part_number_id;
		   

    BEGIN
     SELECT GET_ACCOUNT_NAME(p_accountid)
            INTO v_account_name
            FROM dual;
        FOR v_fetch_summarydtls IN fetch_summary_dtls LOOP
			  
         v_str := v_str || '<TR ><TD style="border: 1px solid  #676767;background-color:#87CEEB;">'|| v_fetch_summarydtls.partnum
                        || '</TD><TD style="border: 1px solid  #676767;background-color:#87CEEB;">'|| v_fetch_summarydtls.partdesc
                        || '</TD><TD style="border: 1px solid  #676767;background-color:#F2D3BF;text-align:right;">'|| v_fetch_summarydtls.qty
                        || '</TD><TD style="border: 1px solid  #676767;background-color:#E2EFDA;">'|| v_fetch_summarydtls.price
                        || '</TD></TR>';
        END LOOP;
		
	
		
		  SELECT get_rule_value('COS_INV_SUM_EMAIL_SUB', 'COS_RULE_GRP')
            INTO v_email_sub 
            FROM dual;

        SELECT get_rule_value('COS_INV_SUM_EMAIL_TO', 'COS_RULE_GRP')
        INTO v_email_to
        FROM dual;
        
        v_email_sub :=  v_email_sub || ' - ' || v_account_name;
		
        IF ( p_notes = 'undefined' OR p_notes = 'null' ) THEN
            v_add_notes := '';
        ELSE
            v_add_notes := p_notes;
        END IF;

        v_email_msg := '<b>Additional Notes :</b><br>' || v_add_notes;
        v_table_str := '<TABLE style="border: 1px solid  #676767; border-collapse: collapse;">'
                       || '<TR><TD width=150 style="border: 1px solid  #676767;color:#0286c1; text-align: center;"><b>Part Number</b></TD>'
                       || '<TD width=250 style="border: 1px solid  #676767;color:#0286c1; text-align: center;"><b>Part Desc</b></TD>'
                       || '<TD width=150 style="border: 1px solid  #676767;color:#0286c1; text-align: center;"><b>Qty</b></TD>'
                       || '<TD width=150 style="border: 1px solid  #676767;color:#0286c1; text-align: center;"><b>Price</b></TD></TR>'
                       ;
        v_mail_body := '<style>TD{ FONT-SIZE: 11px; COLOR: #000000; FONT-FAMILY: verdana, arial, sans-serif;}</style><font face=arial size="2"></font>';
        v_mail_body := v_mail_body
                       || '<br>'
                       || v_table_str
                       || v_str
                       || '</TABLE> <br><br>'
                       || v_email_msg;
          -- call the email procedure to send email to the user
        gm_com_send_html_email(v_email_to, v_email_sub, NULL, v_mail_body);
    END gm_send_email_summary_report;
    
 /********************************************************************************
 * Description : This procedure is used to send the COS Inventory shelf details mail
 **********************************************************************************/

    PROCEDURE gm_send_email_shelf_report (
    	p_accountid			IN   t5066_cos_part_lot_qty.c704_account_id%TYPE,
        p_notes	  			IN   VARCHAR2
    ) 
    
    AS

        v_email_to      t906_rules.c906_rule_value%TYPE;
        v_email_msg     VARCHAR2(4000);
        v_email_sub     t906_rules.c906_rule_value%TYPE;
        v_str           CLOB;
        v_mail_body     CLOB;
        v_col_str       VARCHAR2(1000);
        v_end_row_str   VARCHAR2(1000);
        v_table_str     CLOB;
        v_add_notes     VARCHAR2(4000);
        v_account_name t704_account.c704_account_nm%type;
        
        CURSOR fetch_summary_dtls IS
        SELECT 
        	t5066.c205_part_number_id partnum, regexp_replace(t205.c205_part_num_desc, '[^0-9a-za-z]', ' ') partdesc,
	  		t5066.C5066_BILLED_ORDER_ID orderid, t5066.C5066_BILLED_ORDER_DATE orderdt, get_code_name(t5066.c901_current_inventory_type) billingstatus,
	  		t2550.c2550_custom_size cussize, t2540.c2540_age dage, get_code_name(t2540.c901_sex) dsex, t5066.C2550_CONTROL_NUMBER lotnumber,
			t2550.c2550_mfg_date mfgdate, t2550.c2550_expiry_date expdate , t5066.c502_item_price price
		  FROM 
		  	T5066_COS_PART_LOT_QTY t5066, T205_part_number t205, T2550_part_control_number t2550, T2540_DONOR_MASTER t2540,t704_account t704 
		 WHERE 
			t5066.c704_account_id = t704.c704_account_id
			AND t5066.c205_part_number_id = t205.c205_part_number_id
			AND t5066.C2550_CONTROL_NUMBER = t2550.c2550_control_number
			AND t205.c205_part_number_id = t2550.c205_part_number_id
			AND t2550.c2540_donor_id = t2540.c2540_donor_id(+)
			AND t5066.c704_account_id = p_accountid
			AND t5066.c5066_void_fl IS NULL
			AND t2540.c2540_void_fl(+) IS NULL
			AND t704.c704_void_fl IS NULL
	    ORDER BY t5066.c205_part_number_id ASC;

    BEGIN
      
		   SELECT GET_ACCOUNT_NAME(p_accountid)
            INTO v_account_name
            FROM dual;
   
        FOR v_fetch_summarydtls IN fetch_summary_dtls LOOP
			  
         v_str := v_str || '<TR ><TD style="border: 1px solid  #676767;background-color:#AEAAAA;">'|| v_fetch_summarydtls.partnum
                        || '</TD><TD style="border: 1px solid  #676767;background-color:#AEAAAA;">'|| v_fetch_summarydtls.partdesc
                        || '</TD><TD style="border: 1px solid  #676767;background-color:#FFF2CC;text-align:right;">'|| v_fetch_summarydtls.orderid
                        || '</TD><TD style="border: 1px solid  #676767;background-color:greenyellow;text-align:right;">'|| v_fetch_summarydtls.orderdt
                        || '</TD><TD style="border: 1px solid  #676767;background-color:#E2EFDA;">'|| v_fetch_summarydtls.billingstatus
						|| '</TD><TD style="border: 1px solid  #676767;background-color:#E2EFDA;">'|| v_fetch_summarydtls.cussize
                        || '</TD><TD style="border: 1px solid  #676767;background-color:#E2EFDA;">'|| v_fetch_summarydtls.dage
                        || '</TD><TD style="border: 1px solid  #676767;background-color:#E2EFDA;">'|| v_fetch_summarydtls.dsex
                        || '</TD><TD style="border: 1px solid  #676767;background-color:#F2D3BF;">'|| v_fetch_summarydtls.lotnumber
                        || '</TD><TD style="border: 1px solid  #676767;background-color:#F4B084;">'|| v_fetch_summarydtls.mfgdate
                        || '</TD><TD style="border: 1px solid  #676767;background-color:#F2D3BF;">'|| v_fetch_summarydtls.expdate
                        || '</TD><TD style="border: 1px solid  #676767;background-color:#F4B084;">'|| v_fetch_summarydtls.price
                        || '</TD></TR>';
        END LOOP;
		
		
		 
		   SELECT get_rule_value('COS_SHELF_EMAIL_SUB', 'COS_RULE_GRP')
            INTO v_email_sub
            FROM dual;

        SELECT get_rule_value('COS_INV_SUM_EMAIL_TO', 'COS_RULE_GRP')
        INTO v_email_to
        FROM dual;
		
		 v_email_sub :=  v_email_sub || ' - ' || v_account_name;
		 
		
		
        IF ( p_notes = 'undefined' OR p_notes = 'null' ) THEN
            v_add_notes := '';
        ELSE
            v_add_notes := p_notes;
        END IF;
          

        v_email_msg := '<b>Additional Notes :</b><br>' || v_add_notes;
        
         
        v_table_str := '<TABLE style="border: 1px solid  #676767; border-collapse: collapse;">'
                       || '<TR><TD width=150 style="border: 1px solid  #676767;color:#0286c1; text-align: center;"><b>Part Number</b></TD>'
                       || '<TD width=250 style="border: 1px solid  #676767;color:#0286c1; text-align: center;"><b>Part Desc</b></TD>'
                       || '<TD width=150 style="border: 1px solid  #676767;color:#0286c1; text-align: center;"><b>Order Billed</b></TD>'
                       || '<TD width=150 style="border: 1px solid  #676767;color:#0286c1; text-align: center;"><b>Order Date</b></TD>'
                       || '<TD width=150 style="border: 1px solid  #676767;color:#0286c1; text-align: center;"><b>Billing Status</b></TD>'
                       || '<TD width=150 style="border: 1px solid  #676767;color:#0286c1; text-align: center;"><b>Size</b></TD>'
                       || '<TD width=150 style="border: 1px solid  #676767;color:#0286c1; text-align: center;"><b>Age</b></TD>'
                       || '<TD width=150 style="border: 1px solid  #676767;color:#0286c1; text-align: center;"><b>Sex</b></TD>'
                       || '<TD width=150 style="border: 1px solid  #676767;color:#0286c1; text-align: center;"><b>Allograft Code</b></TD>'
                       || '<TD width=150 style="border: 1px solid  #676767;color:#0286c1; text-align: center;"><b>Manufacturing Date</b></TD>'
                       || '<TD width=150 style="border: 1px solid  #676767;color:#0286c1; text-align: center;"><b>Expiration Date</b></TD>'
                       || '<TD width=150 style="border: 1px solid  #676767;color:#0286c1; text-align: center;"><b>Price Total</b></TD></TR>'
                       ;
        v_mail_body := '<style>TD{ FONT-SIZE: 11px; COLOR: #000000; FONT-FAMILY: verdana, arial, sans-serif;}</style><font face=arial size="2"></font>';
        v_mail_body := v_mail_body
                       || '<br>'
                       || v_table_str
                       || v_str
                       || '</TABLE> <br><br>'
                       || v_email_msg;
                       
   
          -- call the email procedure to send email to the user
        gm_com_send_html_email(v_email_to, v_email_sub, NULL, v_mail_body);
    END gm_send_email_shelf_report;
    
END gm_pkg_cos_report;
/