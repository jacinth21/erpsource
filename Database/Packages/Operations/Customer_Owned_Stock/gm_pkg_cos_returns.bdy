/* Created on 2020/08/24 12:58*/
--@"C:\PMT\Database\Packages\Operations\Customer_Owned_Stock\gm_pkg_cos_returns.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_cos_returns
IS
--	
	/*************************************************************************************************************************
		  * Description : This procedure used inset/update COS account, Part and Lot details on t5066_cos_part_lot_qty table
		  * Author		: Vinoth
   	***************************************************************************************************************************/
	PROCEDURE gm_initiate_return (
		p_order_id 		IN  t5069_cos_return.c5067_cos_order_id%TYPE,
		p_account_id	IN 	t5069_cos_return.c5069_cos_bill_to_id%TYPE,
		p_user_id		IN 	t5069_cos_return.c5069_created_by%TYPE,
		p_ra_id			OUT VARCHAR
	)
	AS
	
	v_ra_id t5069_cos_return.c5069_cos_ra_id%TYPE;
	v_ra_out_id t5069_cos_return.c5069_cos_ra_id%TYPE;
	v_order_status_count NUMBER:='0';
	v_spineIT_order_status_count NUMBER:='0';
	v_spineIT_order_bill_to_id t501_order.c704_account_id%TYPE;
	
	BEGIN
		
		BEGIN
		
		SELECT C5069_COS_RA_ID INTO v_ra_id 
		  FROM t5069_cos_return
		 WHERE C5067_COS_ORDER_ID = p_order_id
		   AND c5069_void_fl IS NULL;
		EXCEPTION
			WHEN NO_DATA_FOUND THEN
			v_ra_id := NULL;
		END;
		
		-- Validating the order status, RA should be initiated only for shipped orders.
		
		SELECT COUNT(1) INTO 
		 v_order_status_count
		FROM T5067_COS_ORDER t5067
		WHERE t5067.C901_COS_ORDER_STATUS='110615' --110615 Order Shipped Status
		AND t5067.C5067_COS_ORDER_ID     =p_order_id
		AND t5067.C5067_VOID_FL         IS NULL;
		
		BEGIN
		
		SELECT COUNT(1) , c704_account_id INTO 
		 v_spineIT_order_status_count, v_spineIT_order_bill_to_id
		 FROM T501_ORDER T501 
		WHERE T501.C501_ORDER_ID=p_order_id
		AND T501.C501_STATUS_FL=3
		AND T501.C501_VOID_FL IS NULL
		GROUP BY c704_account_id;
		
		EXCEPTION WHEN OTHERS
		THEN
		v_spineIT_order_status_count:='0';
		v_spineIT_order_bill_to_id:='';
		END;
		
		
		IF(v_order_status_count=0 AND v_spineIT_order_status_count=0) THEN
		   p_ra_id :='';
		   RETURN;
		END IF;
		
		IF(v_spineIT_order_status_count>0) THEN
		
		gm_pkg_cos_sync.gm_sav_spineIT_order_to_cos(p_order_id,p_user_id);
		END IF;
		
		
		IF v_ra_id IS NULL THEN
		SELECT 'COS-RA-'||s5069_cos_return.NEXTVAL INTO v_ra_out_id FROM DUAL;
       				
		INSERT INTO t5069_cos_return (C5069_COS_RA_ID, C5067_COS_ORDER_ID, C5069_COS_BILL_TO_ID, C5069_COS_RA_STATUS, C5069_CREATED_BY,
						 C5069_CREATED_DATE, C5069_COS_RA_DATE)
					VALUES (v_ra_out_id, p_order_id, NVL(p_account_id,v_spineIT_order_bill_to_id), 110760, p_user_id, CURRENT_DATE, CURRENT_DATE);
					
		
		END IF;
			
		p_ra_id := v_ra_out_id;
		
	END gm_initiate_return;
	
	
	/*************************************************************************************************************************
		  * Description : This procedure used inset/update COS account, Part and Lot details on t5066_cos_part_lot_qty table
		  * Author		: Vinoth
   	***************************************************************************************************************************/
	PROCEDURE gm_save_cos_return (
		p_return_order_id 		IN  t5069_cos_return.c5069_cos_ra_id%TYPE,
		p_return_inv_type 		IN  t5069_cos_return.c5069_cos_ra_status%TYPE,
		p_date_checked_in		IN  t5069_cos_return.c5069_ra_chck_in_date_time%TYPE,
		p_contact_person		IN  t5069_cos_return.c5069_cos_ra_contact_name%TYPE,
		p_lot_number			IN  t5069a_cos_return_detail.C2550_CONTROL_NUMBER%TYPE,
		p_comments				IN  t5069_cos_return.c5069_cos_ra_comments%TYPE,
		p_account_id			IN  t5069_cos_return.c5069_cos_bill_to_id%TYPE,
		p_user_id				IN 	t5069_cos_return.c5069_created_by%TYPE,
		p_out					OUT CLOB
	)
	AS
	v_lot_count NUMBER;
	v_part_number t5069a_cos_return_detail.c205_part_number_id%TYPE;
	v_part_price  t5069a_cos_return_detail.c5069a_part_price%TYPE;
	V_DUPLICATE_COUNT NUMBER:='0';
	
	BEGIN

		BEGIN
                SELECT c205_part_number_id, c502_item_price
		           INTO v_part_number, v_part_price
                  FROM t5066_cos_part_lot_qty
                 WHERE C2550_CONTROL_NUMBER = p_lot_number
                   AND c5066_void_fl IS NULL;

              EXCEPTION WHEN OTHERS THEN
				v_part_number:=NULL;
				v_part_price:=NULL;
				RETURN;
            	
	      END;
		  
		--To validate if the lot is already scanned
  		BEGIN
                SELECT COUNT(1) 
					INTO V_DUPLICATE_COUNT
				FROM T5069A_COS_RETURN_DETAIL t5069a ,T5069_COS_RETURN t5069
				WHERE t5069.c5069_cos_ra_id=t5069a.c5069_cos_ra_id
				 AND t5069.C5069_VOID_FL   IS NULL
				 AND t5069a.C2550_CONTROL_NUMBER=p_lot_number
				 AND t5069.c5069_cos_ra_id = p_return_order_id
				 AND t5069a.C5069A_VOID_FL IS NULL
				 AND t5069a.C2550_CONTROL_NUMBER IS NOT NULL;

              EXCEPTION WHEN OTHERS THEN
				V_DUPLICATE_COUNT:='0';
            	
	    END;
		
		IF(V_DUPLICATE_COUNT>0) THEN
		
		 p_out := 'Allograft is already scanned for the return transaction'||p_return_order_id;
		 
		 RETURN;
		 
		END IF;
	
		
		BEGIN
		 --To validate the lot is available for the part with the associated order 
		SELECT COUNT(1), t5069a.c205_part_number_id
		  INTO v_lot_count, v_part_number
		  FROM t5069a_cos_return_detail t5069a				
         WHERE t5069a.c5069_cos_ra_id = p_return_order_id
           AND t5069a.c205_part_number_id IN 
		(SELECT c205_part_number_id 
         	  FROM t5066_cos_part_lot_qty 
          	 WHERE c2550_control_number = p_lot_number
	   	AND c704_account_id = p_account_id
	   	AND c5066_void_fl IS NULL
		)
	   	AND t5069a.C2550_CONTROL_NUMBER IS NULL
	   AND t5069a.c5069a_void_fl IS NULL
	   GROUP BY  t5069a.c205_part_number_id;
	EXCEPTION  	
	  WHEN OTHERS THEN
	  v_lot_count := 0;
	  v_part_number := NULL;
	  
	END;
		  IF (v_lot_count > 0) THEN
		  	
		 	INSERT INTO t5069a_cos_return_detail (c5069_cos_ra_id, c205_part_number_id, 
				C2550_CONTROL_NUMBER, c5069a_qty,C5069A_PART_PRICE)
		  	VALUES (p_return_order_id, v_part_number, p_lot_number, 1,v_part_price);		
	
		  ELSE 
		  	  p_out := 'Allograft scanned is not associated with this part or account';
	  
		  END IF;
	   
		
	END gm_save_cos_return;
	
	/*************************************************************************************************************************
		  * Description : This procedure used inset/update COS account, Part and Lot details on t5066_cos_part_lot_qty table
		  * Author		: Vinoth
   	***************************************************************************************************************************/
	PROCEDURE gm_complete_cos_return (
		p_return_order_id 		IN  t5069_cos_return.c5069_cos_ra_id%TYPE,
		p_return_inv_type 		IN  t5069_cos_return.c5069_cos_ra_status%TYPE,
		p_date_checked_in		IN  t5069_cos_return.c5069_ra_chck_in_date_time%TYPE,
		p_contact_person		IN  t5069_cos_return.c5069_cos_ra_contact_name%TYPE,
		--p_lot_number			IN  t5069a_cos_return_detail.C2550_CONTROL_NUMBER%TYPE,
		p_comments				IN  t5069_cos_return.c5069_cos_ra_comments%TYPE,
		p_user_id				IN 	t5069_cos_return.c5069_created_by%TYPE
		
	)
	AS
	
	CURSOR curs_allograft_detail
	    IS
		SELECT t5069.c5067_cos_order_id ordid, t5069a.C2550_CONTROL_NUMBER lotnumber
		  FROM t5069_cos_return t5069, t5069a_cos_return_detail t5069a
		 WHERE t5069.c5069_cos_ra_id = p_return_order_id
		  AND t5069.c5069_cos_ra_id = t5069a.c5069_cos_ra_id
		   AND t5069.c5069_void_fl IS NULL
		   AND t5069a.C2550_CONTROL_NUMBER IS NOT NULL
		   AND t5069a.c5069a_void_fl IS NULL;
	BEGIN
		
		   UPDATE t5069_cos_return
		   SET c5069_cos_ra_status = 110761,   --110761 - COS Return Processed
		       c5069_ra_chck_in_date_time = p_date_checked_in,
		   	   c5069_cos_ra_contact_name = p_contact_person,
		   	   c5069_cos_ra_comments = p_comments,
		   	   c5069_last_updated_by = p_user_id,
		   	   c5069_last_updated_date = CURRENT_DATE
		 WHERE c5069_cos_ra_id = p_return_order_id
		   AND c5069_void_fl IS NULL;
		  
	BEGIN

		FOR allograft_detail IN curs_allograft_detail
		LOOP
		
		   --110600 - COS Billed  , 110602 - COS-QUARANTINE  ,110741 - Return Quarantine
		UPDATE t5066_cos_part_lot_qty
		   SET c901_current_inventory_type = DECODE(p_return_inv_type,110741,110602,110600),-- COS Return Processed and changing the Lot status to COS-Billed
		   	   c5066_last_updated_txn_id = p_return_order_id,
		       c5066_last_updated_by = p_user_id,
		   	   c5066_last_updated_date = CURRENT_DATE
		 WHERE c2550_control_number = allograft_detail.lotnumber
		   AND c5066_void_fl IS NULL;
		   
		END LOOP;
	  END;	
	
	END gm_complete_cos_return;
	

/*************************************************************************************************************************
		  * Description : This procedure used inset/update COS account, Part and Lot details on t5066_cos_part_lot_qty table
		  * Author		: Vinoth
   	***************************************************************************************************************************/
	PROCEDURE gm_update_part (
		p_return_order_id 		IN  t5069a_cos_return_detail.c5069_cos_ra_id%TYPE,
		p_part_number			IN 	t205_part_number.c205_part_number_id%TYPE,
		p_part_price			IN 	t5069a_cos_return_detail.c5069a_part_price%TYPE,
		p_return_qty			IN 	t5069a_cos_return_detail.c5069a_qty%TYPE,
		p_lot_num               IN CLOB,
		p_account_id			IN  t5069_cos_return.c5069_cos_bill_to_id%TYPE,
		p_out_msg               OUT CLOB
	)
	AS
	 v_string CLOB   := p_lot_num;
    v_substring CLOB;
    v_lot_num  CLOB;
    v_out   CLOB;
	BEGIN
		
		 UPDATE t5069a_cos_return_detail
		  	   SET 
		  	   C5069A_PART_PRICE=p_part_price,
			   c5069a_qty=p_return_qty
		  	 WHERE c5069_cos_ra_id = p_return_order_id
		  	   AND c205_part_number_id = p_part_number
		  	   AND c5069a_void_fl IS NULL;
			   
		 IF ( SQL%rowcount = 0) THEN
		INSERT INTO t5069a_cos_return_detail (c5069_cos_ra_id, c205_part_number_id, 
							c5069a_part_price, c5069a_qty)
		 VALUES (p_return_order_id, p_part_number, p_part_price, p_return_qty);	
		  
		END IF;
		
	WHILE instr (v_string, ',') <> 0
    LOOP
		 v_substring   := SUBSTR (v_string, 1, INSTR (v_string, ',') - 1) ;
         v_string      := SUBSTR (v_string, INSTR (v_string, ',')    + 1) ;
         v_lot_num := v_substring;
     
		 gm_pkg_cos_returns.gm_save_cos_return(p_return_order_id,null,null,null,v_lot_num,null,p_account_id,NULL,v_out);
    END LOOP;
    p_out_msg := v_out;
	END gm_update_part;
/************************************************************************
 *  * Description : This procedure used remove the part details 
 * Author		: Vinoth
**********************************************************************/	
PROCEDURE gm_remove_return_part(
p_return_dtl_id  IN t5069a_cos_return_detail.C5069A_COS_RA_DETAIL_ID%TYPE,
p_user_id        IN t5069a_cos_return_detail.C5069A_LAST_UPDATED_BY%TYPE
)
AS
BEGIN
		UPDATE t5069a_cos_return_detail
		  	   SET 
		  	   	   C5069A_VOID_FL ='Y',
				   C5069A_LAST_UPDATED_BY=p_user_id,
				   C5069A_LAST_UPDATED_DATE = CURRENT_DATE
		  	 WHERE C5069A_COS_RA_DETAIL_ID = p_return_dtl_id
		  	   AND c5069a_void_fl IS NULL;
END gm_remove_return_part;
/************************************************************************
 *  * Description : This procedure used to update the comments  
 * Author		: Tamizhthangamgm_update_part
**********************************************************************/	
PROCEDURE gm_save_comments(
    p_return_order_id 		IN  t5069_cos_return.c5069_cos_ra_id%TYPE,
	p_comments				IN  t5069_cos_return.c5069_cos_ra_comments%TYPE,
	p_user_id				IN 	t5069_cos_return.c5069_created_by%TYPE
)
AS
BEGIN
	UPDATE t5069_cos_return
	   SET c5069_cos_ra_comments = p_comments,
	       C5069_LAST_UPDATED_BY=p_user_id,
	       C5069_LAST_UPDATED_DATE = current_date
	WHERE c5069_cos_ra_id = p_return_order_id
	  AND C5069_VOID_FL IS NULL;
END gm_save_comments;
END gm_pkg_cos_returns;
/