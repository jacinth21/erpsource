CREATE OR REPLACE PACKAGE gm_pkg_cos_order IS 

/*************************************************************************
  * Description : This procedure is used to validate the order id and insert/update the order id , 
  * billto, ship to details to t5067 table
 *  **************************************************************************/
    PROCEDURE gm_validate_cos_order (
      --  p_order_id   IN    t5067_cos_order.c5067_cos_order_id%TYPE,
        p_bill_to    IN    t5067_cos_order.c5067_cos_bill_to_id%TYPE,
        p_ship_to    IN    t5067_cos_order.c5067_cos_ship_to_id%TYPE,
        p_user_id    IN    t5067_cos_order.c5067_last_updated_by%TYPE,
        p_out_msg    OUT   VARCHAR2
    );

    PROCEDURE gm_save_cos_order_part (
        p_order_id   IN    t5067_cos_order.c5067_cos_order_id%TYPE,
        p_bill_to    IN    t5067_cos_order.c5067_cos_bill_to_id%TYPE,
        p_part_num   IN    t5067a_cos_order_item.c205_part_number%TYPE,
        p_qty        IN    t5067a_cos_order_item.c5067a_qty%TYPE,
        p_user_id    IN    t5067_cos_order.c5067_last_updated_by%TYPE,
        p_out_msg    OUT   VARCHAR2
    );

    PROCEDURE gm_update_order_total_cost (
        p_order_id   IN   t5067_cos_order.c5067_cos_order_id%TYPE,
        p_user_id    IN   t5067_cos_order.c5067_last_updated_by%TYPE
    );

    PROCEDURE gm_update_cos_order (
        p_order_id           IN   t5067_cos_order.c5067_cos_order_id%TYPE,
        p_po_num             IN   t5067_cos_order.c5067_cos_po_number%TYPE,
        p_ship_date          IN   t5068_cos_shipping.c5067_cos_planned_shipped_date%TYPE,
        p_courier_mode       IN   t5068_cos_shipping.c5067_carrier_mode%TYPE,
        p_courier_id         IN   t5068_cos_shipping.c5067_carrier_id%TYPE,
        p_courier_acct_id    IN   t5068_cos_shipping.c5067_carrier_acct_no%TYPE,
        p_email_id           IN   t5068_cos_shipping.c5067_ship_to_email%TYPE,
        p_comments           IN   t5068_cos_shipping.c5067_comments%TYPE,
        p_user_id            IN   t5067_cos_order.c5067_last_updated_by%TYPE,
        p_delivery_slip_fl   IN   t5068_cos_shipping.c5067_delivery_slip_fl%TYPE,
        p_comm_invoice_fl    IN   t5068_cos_shipping.c5067_comm_invoice_fl%TYPE,
        p_cust_shipment_fl   IN   t5068_cos_shipping.c5067_customer_shipment_email_fl%TYPE,
        p_bill_to            IN   t5067_cos_order.c5067_cos_bill_to_id%TYPE,
        p_ship_to            IN   t5067_cos_order.c5067_cos_ship_to_id%TYPE,
        p_cos_attn           IN   t5067_cos_order.c5067_cos_attn%TYPE
    );

    PROCEDURE gm_remove_cos_order_part_dtl (
        p_order_id      IN   t5067_cos_order.c5067_cos_order_id%TYPE,
        p_cos_item_id   IN   t5067a_cos_order_item.c5067a_cos_item_id%TYPE,
        p_user_id       IN   t5067a_cos_order_item.c5067a_last_updated_by%TYPE
    );

    PROCEDURE gm_upd_order_qty (
        p_part_num   IN    t5067a_cos_order_item.c205_part_number%TYPE,
        p_item_id    IN    t5067a_cos_order_item.c5067a_cos_item_id%TYPE,
        p_order_id   IN    t5067_cos_order.c5067_cos_order_id%TYPE,
        p_bill_to    IN    t5067_cos_order.c5067_cos_bill_to_id%TYPE,
        p_qty        IN    t5067a_cos_order_item.c5067a_qty%TYPE,
        p_user_id    IN    t5067_cos_order.c5067_last_updated_by%TYPE,
        p_out_msg    OUT   VARCHAR2
    );

    PROCEDURE gm_void_cos_order (
        p_order_id   IN   t5067_cos_order.c5067_cos_order_id%TYPE,
        p_user_id    IN   t5067_cos_order.c5067_last_updated_by%TYPE
    );
/*************************************************************************************************************************
		  * Description : This procedure used inset/update COS account, Part and Lot details on t5066_cos_part_lot_qty table
		  * Author		: Vinoth
   	***************************************************************************************************************************/

    PROCEDURE gm_update_order_control_number (
        p_order_id    IN    t5067a_cos_order_item.c5067_cos_order_id%TYPE,
        p_lotnumber   IN    t5067a_cos_order_item.c2550_control_number%TYPE,
        p_user_id     IN    t5067a_cos_order_item.c5067a_last_updated_by%TYPE,
        p_out_msg     OUT   VARCHAR2
    );
	
	/****************************************************************************
		  * Description : This procedure used to remove allograft from the cos item order
		  * Author		: Vinoth
   	******************************************************************************/

    PROCEDURE gm_remove_order_control_number (
        p_ord_item_id   IN   t5067a_cos_order_item.c5067a_cos_item_id%TYPE,
        p_user_id       IN   t5067a_cos_order_item.c5067a_last_updated_by%TYPE
    );
	
	/****************************************************************************
		  * Description : This procedure used to remove allograft from the cos item order
		  * Author		: Vinoth
   	******************************************************************************/

    PROCEDURE gm_save_cos_order_control (
        p_order_id   IN    t5067_cos_order.c5067_cos_order_id%TYPE,
        p_user_id    IN    t5067_cos_order.c5067_last_updated_by%TYPE,
        p_out_msg    OUT   VARCHAR2
    );

END gm_pkg_cos_order;
/