

CREATE OR REPLACE PACKAGE BODY gm_pkg_cos_sync
IS
--	
	/*************************************************************************************************************************
		  * Description : This procedure used inset/update COS account, Part and Lot details on t5066_cos_part_lot_qty table
		  * Author		: Vinoth
   	***************************************************************************************************************************/
	PROCEDURE gm_sav_cos_txn(
	  p_order_id  IN  t907_shipping_info.c907_ref_id%TYPE
	
	)
	AS
		v_part_num_id	t2550_part_control_number.C205_PART_NUMBER_ID%TYPE;
		v_lot_qty_id	t5066_cos_part_lot_qty.C5066_COS_PART_LOT_QTY_ID%TYPE;
		
	CURSOR cur_cos_ord_dtl IS
	
		SELECT t501.c501_order_id orderid, t502.C205_PART_NUMBER_ID partnum, t502.c502_control_number lotnum, 
		       t501.c704_account_id accid, t501.c501_created_by userid  ,t502.c502_item_price itemprice,t501.c501_order_date orddate
		  FROM t501_order t501, t502_item_order t502,t2550_part_control_number t2550
		 WHERE t501.c501_order_id    =  p_order_id 
           AND t501.c501_order_id    =  t502.c501_order_id
           AND t2550.c2550_control_number = t502.c502_control_number
           AND t2550.c205_part_number_id = t502.c205_part_number_id
           AND t501.c501_void_fl IS NULL
           AND t502.C502_VOID_FL IS NULL
           ORDER BY t502.C205_PART_NUMBER_ID; 
	BEGIN
		
      FOR v_cur_cos_ord_dtl IN cur_cos_ord_dtl
		LOOP	
    	
    		
    			UPDATE t5066_cos_part_lot_qty
    			   SET c901_current_inventory_type = 110600 -- 110600 - COS-BILLED
    			 WHERE c704_account_id = v_cur_cos_ord_dtl.accid
    			   AND C2550_CONTROL_NUMBER = v_cur_cos_ord_dtl.lotnum
    			   AND c205_part_number_id = v_cur_cos_ord_dtl.partnum
    			   AND c5066_void_fl IS NULL;
    			   
    			   IF (SQL%ROWCOUNT = 0) THEN
    			   
	    			   INSERT 
	    			     INTO t5066_cos_part_lot_qty
	    			     	(
	    			     	  C704_ACCOUNT_ID, C205_PART_NUMBER_ID, C2550_CONTROL_NUMBER
	    			     	, C5066_LAST_UPDATED_TXN_ID, C901_CURRENT_INVENTORY_TYPE, C5066_LAST_UPDATED_BY
	    			     	, C5066_LAST_UPDATED_DATE, C5066_VOID_FL ,c502_item_price,C5066_BILLED_ORDER_ID ,C5066_BILLED_ORDER_DATE 
	    			     	)
	    			     	VALUES
	    			     	(
	    			     	 v_cur_cos_ord_dtl.accid, v_cur_cos_ord_dtl.partnum, v_cur_cos_ord_dtl.lotnum
	    			     	,p_order_id, 110600, v_cur_cos_ord_dtl.userid
	    			     	,CURRENT_DATE, NULL,v_cur_cos_ord_dtl.itemprice,p_order_id,v_cur_cos_ord_dtl.orddate
	    			     	);
    			   END IF;
    		
		END LOOP;
	END gm_sav_cos_txn;	
	
	/**********************************************************************************************************
		  * Description : This procedure used to sync spineIt shipped records inot COS t5066_cos_part_lot_qty table 
		  * 			: This is called by Obsidian
		  * Author		: Vinoth
   	 ***********************************************************************************************************/

	PROCEDURE gm_sync_cos_accounts
	AS
	
	CURSOR cus_cos_order
	IS
		SELECT t907.c907_ref_id orderid
		  FROM  t907_shipping_info t907,t501_order t501
		 WHERE t907.c907_ship_to_id  =  '16116' -- Ship to id(Customer Owned Stock Account)
         AND t907.c907_ref_id = t501.c501_order_id
         AND (t501.c901_order_type  IN ('26240233','26240236','26240232')  -- 26240232-Direct order
         OR NVL(t501.c901_order_type ,'-999') IN ('-999') )
           AND t907.C907_STATUS_FL = 40     --shipped 
           AND t907.c907_cos_sync_fl IS NULL
           AND t907.c907_void_fl IS NULL
           AND t501.c501_void_fl IS NULL;
          
	BEGIN
		
		FOR cos_order_dtl IN cus_cos_order
		LOOP
			gm_pkg_cos_sync.gm_sav_cos_txn(cos_order_dtl.orderid); -- 110380 - COS-BILLED
			
			UPDATE t907_shipping_info SET c907_cos_sync_fl = 'Y' WHERE c907_ref_id = cos_order_dtl.orderid AND c907_void_fl IS NULL;
			 
		END LOOP;
		
	END gm_sync_cos_accounts;	
	
	
	/*************************************************************************************************************************
		  * Description : This procedure used inset/update spineIT order into COS system. This get invoked when a spineIT order is returned into COS.
		  * Author		: Gomathi
   	***************************************************************************************************************************/
	PROCEDURE gm_sav_spineIT_order_to_cos(
	  p_order_id    IN  t907_shipping_info.c907_ref_id%TYPE,
	  p_user_id  	IN 	t5068_cos_shipping.c5068_cos_ship_last_updated_by%TYPE
	
	)
	AS
		v_part_num_id	t2550_part_control_number.C205_PART_NUMBER_ID%TYPE;
		v_lot_qty_id	t5066_cos_part_lot_qty.C5066_COS_PART_LOT_QTY_ID%TYPE;
		
	CURSOR cur_cos_ord_dtl IS
	
		SELECT t501.c501_order_id orderid, t502.C205_PART_NUMBER_ID partnum, t502.c502_control_number lotnum, 
		       t501.c704_account_id accid, t501.c501_created_by userid  ,t502.c502_item_price itemprice,t501.c501_order_date orddate
           , t501.C501_SHIP_To_id ship_to_id, t501.C501_ORDER_DATE ORD_DATE
		  FROM t501_order t501, t502_item_order t502,t2550_part_control_number t2550
		 WHERE t501.c501_order_id    =  p_order_id
           AND t501.c501_order_id    =  t502.c501_order_id
           AND t2550.c2550_control_number = t502.c502_control_number
           AND t2550.c205_part_number_id = t502.c205_part_number_id
           AND t501.c501_void_fl IS NULL
           AND t502.C502_VOID_FL IS NULL
           ORDER BY t502.C205_PART_NUMBER_ID; 
	BEGIN
			
			
      FOR v_cur_cos_ord_dtl IN cur_cos_ord_dtl
	  
	   LOOP

-- Insert/update the spineIT order in COS Order Table.

        UPDATE t5067_cos_order
        SET
            c901_cos_order_status = 110615, -- cos Order Shipped
            c5067_cos_bill_to_id = v_cur_cos_ord_dtl.accid,
            c5067_cos_ship_to_id = v_cur_cos_ord_dtl.ship_to_id,
            c5067_last_updated_by = p_user_id,
            c5067_last_updated_date = current_date
        WHERE
            c5067_cos_order_id = p_order_id
            AND c5067_void_fl IS NULL;	
			
    	 IF (SQL%ROWCOUNT = 0) THEN
			INSERT INTO t5067_cos_order (c5067_cos_order_id,c5067_cos_bill_to_id,c5067_cos_ship_to_id,
					c5067_cos_order_date,c5067_created_by,c5067_created_date,c901_cos_order_status)
					VALUES (p_order_id,v_cur_cos_ord_dtl.accid,v_cur_cos_ord_dtl.ship_to_id,v_cur_cos_ord_dtl.ord_date,p_user_id,current_date,110615 -- Order Status Shipped
				);
		END IF;

-- Insert/update the spineIT order in COS Shipping Table.
		UPDATE t5068_cos_shipping
        SET
            c5068_cos_ship_status = 110615,
			c5068_cos_ship_last_updated_by = p_user_id,
		   	c5068_cos_ship_last_updated_date = CURRENT_DATE
        WHERE
            c5067_cos_order_id = p_order_id
            AND c5068_void_fl IS NULL;
			
		IF (SQL%ROWCOUNT = 0) THEN	
		
			 INSERT INTO t5068_cos_shipping (c5067_cos_order_id,c5068_cos_ship_status,
			 c5068_cos_ship_last_updated_by,c5068_cos_ship_last_updated_date
            ) VALUES (p_order_id,110615, p_user_id, current_date
            );
			
		END IF;
		
 -- Insert/update the spineIT order in COS Lot Tracking Table
  		
    			UPDATE t5066_cos_part_lot_qty
    			   SET c901_current_inventory_type = 110620 -- 110620 - SHIPPED ORDER
    			 WHERE c704_account_id = v_cur_cos_ord_dtl.accid
    			   AND C2550_CONTROL_NUMBER = v_cur_cos_ord_dtl.lotnum
    			   AND c205_part_number_id = v_cur_cos_ord_dtl.partnum
    			   AND c5066_void_fl IS NULL;
    			   
    			   IF (SQL%ROWCOUNT = 0) THEN
    			   
	    			   INSERT 
	    			     INTO t5066_cos_part_lot_qty
	    			     	(
	    			     	  C704_ACCOUNT_ID, C205_PART_NUMBER_ID, C2550_CONTROL_NUMBER
	    			     	, C5066_LAST_UPDATED_TXN_ID, C901_CURRENT_INVENTORY_TYPE, C5066_LAST_UPDATED_BY
	    			     	, C5066_LAST_UPDATED_DATE, C5066_VOID_FL ,c502_item_price,C5066_BILLED_ORDER_ID ,C5066_BILLED_ORDER_DATE 
	    			     	)
	    			     	VALUES
	    			     	(
	    			     	 v_cur_cos_ord_dtl.accid, v_cur_cos_ord_dtl.partnum, v_cur_cos_ord_dtl.lotnum
	    			     	,p_order_id, 110620, v_cur_cos_ord_dtl.userid
	    			     	,CURRENT_DATE, NULL,v_cur_cos_ord_dtl.itemprice,p_order_id,v_cur_cos_ord_dtl.orddate
	    			     	);
    			   END IF;
				   
				    UPDATE t5067a_cos_order_item
					 SET
					   c205_part_number = v_cur_cos_ord_dtl.partnum,
					   C2550_CONTROL_NUMBER=v_cur_cos_ord_dtl.lotnum,
					   c5067a_qty = 1,
					   c5067a_part_price = v_cur_cos_ord_dtl.itemprice,
					   c5067a_last_updated_by = v_cur_cos_ord_dtl.userid,
					    c5067a_last_updated_date = current_date
					WHERE
					c5067_cos_order_id = p_order_id
					AND c205_part_number = v_cur_cos_ord_dtl.partnum
					AND c5067a_void_fl IS NULL
					and C2550_CONTROL_NUMBER=v_cur_cos_ord_dtl.lotnum;
					
  

            IF ( SQL%rowcount = 0 ) THEN
			
                INSERT INTO t5067a_cos_order_item 
				(                  
                    c5067_cos_order_id,
                    c205_part_number,
					C2550_CONTROL_NUMBER,
                    c5067a_qty,
                    c5067a_part_price,
                    c5067a_last_updated_by,
                    c5067a_last_updated_date
                ) VALUES (
                    p_order_id,
                    v_cur_cos_ord_dtl.partnum,
					v_cur_cos_ord_dtl.lotnum,
                    1,
                   v_cur_cos_ord_dtl.itemprice,
                   v_cur_cos_ord_dtl.userid,
                    current_date
                );

            END IF;
    		
		END LOOP;
	END gm_sav_spineIT_order_to_cos;	
	
END gm_pkg_cos_sync;
/