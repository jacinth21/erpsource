create or replace
PACKAGE BODY gm_pkg_op_ict
IS
/******************************************************************************
   NAME: gm_pkg_op_ict
   PURPOSE:    This package holds the procedures for ICT project

   REVISIONS:
   Ver		  Date		  Author
   ---------  ----------  ---------------  ------------------------------------
   1.0		  6/26/2008    Brinal		 1. Created this package.
******************************************************************************/

	/*******************************************************
 * Purpose: This Procedure is main
 *******************************************************/
--
	PROCEDURE gm_op_sav_initiateict (
		p_ref_id	   IN		t504_consignment.c504_consignment_id%TYPE
	  , p_type		   IN		t504_consignment.c504_type%TYPE   --50250
	  , p_poid		   IN OUT	t501_order.c501_customer_po%TYPE
	  , p_freightamt   IN		t501_order.c501_ship_cost%TYPE
	  , p_userid	   IN		t504_consignment.c504_created_by%TYPE
	  , p_dist_id	   IN		t504_consignment.c701_distributor_id%TYPE
	  , p_purpose	   IN		t504_consignment.c504_inhouse_purpose%TYPE
	)
	AS
		v_order_id	   t501_order.c501_order_id%TYPE;
		v_poid		   t501_order.c501_customer_po%TYPE;
		v_frt_order_id t501_order.c501_order_id%TYPE;
		v_frt_poid	   t501_order.c501_customer_po%TYPE;
		v_inv_id	   t503_invoice.c503_invoice_id%TYPE;
		v_dist_type    t701_distributor.c901_distributor_type%TYPE;
		v_inter_party_id t101_party.c101_party_id%TYPE;
		v_account_id   t501_order.c704_account_id%TYPE;
		v_count 	   NUMBER;
		v_orderid	   t501_order.c501_order_id%TYPE;
		--
		v_dist_account_id   t501_order.c704_account_id%TYPE;
	BEGIN
		SELECT get_distributor_type (p_dist_id), get_distributor_inter_party_id (p_dist_id)
			 , get_rule_value (get_distributor_inter_party_id (p_dist_id), 'ICTACCOUNTMAP')
			 ,get_dist_account_id(p_dist_id)
		  INTO v_dist_type, v_inter_party_id
			 , v_account_id, v_dist_account_id
		  FROM DUAL;

		IF v_dist_type = 70105	 -- ICS
		THEN
			IF v_account_id IS NULL
			THEN
				raise_application_error (-20910, '');
			END IF;
			
			--If v_dist_account_id is null 
			IF v_dist_account_id IS NULL
			THEN
				raise_application_error (-20910, '');
			END IF;
			--If v_dist_account_id is more than one account 
			IF v_dist_account_id = 'TOO_MANY_ROWS'
			THEN
				raise_application_error (-20590, '');
			END IF;
			
			gm_pkg_op_ict.gm_op_sav_ict_order (p_ref_id,2533, p_type, p_userid, p_dist_id, p_freightamt, v_order_id, v_poid);
			gm_pkg_op_ict.gm_op_sav_ict_invoice (v_order_id, 50200, v_inv_id);
		END IF;

		---50060 regular, 50061 ICT Sample
		IF p_purpose = 50061
		THEN
			UPDATE t505_item_consignment
			   SET c505_item_price = 0
			 WHERE c504_consignment_id = p_ref_id AND c505_void_fl IS NULL;
			 
			--When ever the detail is updated, the consignment table has to be updated ,so ETL can Sync it to GOP.
		 	UPDATE t504_consignment
			   SET c504_last_updated_by    = p_userid
		  		 , c504_last_updated_date = sysdate
		  	 WHERE c504_consignment_id = p_ref_id;			 

			UPDATE t502_item_order
			   SET c502_item_price = 0
			 WHERE c501_order_id = v_order_id AND c502_void_fl IS NULL;
			 
	        --When ever the detail is updated, the order table has to be updated ,so ETL can Sync it to GOP.
	        UPDATE t501_order
	           SET c501_last_updated_by = p_userid
	             , c501_last_updated_date = SYSDATE
	         WHERE c501_order_id = v_order_id;  			 
		ELSE
			-- Removing the if condition to update the consignment price for Chemtech also
			--IF (p_dist_id != 554)
			-- for chemtech need to pick up us price. so no need to update the price here
			--THEN
				gm_update_consign_price (p_ref_id, v_inter_party_id);
				--in above call of gm_update_consign_price procedure 505 table gets updated.
				--When ever the detail is updated, the consignment table has to be updated ,so ETL can Sync it to GOP.
			 	UPDATE t504_consignment
				   SET c504_last_updated_by    = p_userid
			  		 , c504_last_updated_date = sysdate
			  	 WHERE c504_consignment_id = p_ref_id;	
			--END IF;
		END IF;

		IF v_dist_type = 70105	 -- ICS
		THEN
			gm_pkg_op_ict.gm_op_sav_ict_order (p_ref_id,102080, p_type, p_userid, p_dist_id, p_freightamt, v_order_id, p_poid);
			gm_pkg_op_ict.gm_op_sav_dist_csg_invoice (p_poid, p_ref_id, p_userid, p_freightamt,v_order_id,v_dist_account_id);
		END IF;
	END gm_op_sav_initiateict;

   /*******************************************************
 * Purpose: This Procedure is used to generate	order
 **********************************************************/
--
	PROCEDURE gm_op_sav_ict_order (
		p_ref_id	   IN		t504_consignment.c504_consignment_id%TYPE
	  , p_order_type   IN		t501_order.c901_order_type%TYPE --102080 --OUS Distributor 2533 --ICS
	  , p_type		   IN		t504_consignment.c504_type%TYPE
	  , p_userid	   IN		t504_consignment.c504_created_by%TYPE
	  , p_dist_id	   IN		t504_consignment.c701_distributor_id%TYPE
	  , p_freightamt   IN		t501_order.c501_ship_cost%TYPE
	  , p_order_id	   OUT		t501_order.c501_order_id%TYPE
	  , p_poid		   OUT		t501_order.c501_customer_po%TYPE
	)
	AS
		v_created_by   t504_consignment.c504_created_by%TYPE;
		v_account_id   t501_order.c704_account_id%TYPE;
		cur_part_details TYPES.cursor_type;
		--rec_part_details		gm_op_fch_part_detail.p_part_details%rowtype;
		c_pnumid	   t502_item_order.c205_part_number_id%TYPE;
		c_itemq 	   t502_item_order.c502_item_qty%TYPE;
		c_controlnum   t502_item_order.c502_control_number%TYPE;
		c_voidflg	   t502_item_order.c502_void_fl%TYPE;
		c_price 	   NUMBER;
		v_post_id	   t906_rules.c906_rule_value%TYPE;
		v_total 	   NUMBER;
		v_dist_type    t701_distributor.c901_distributor_type%TYPE;
		v_party_id	   t101_party.c101_party_id%TYPE;
		v_dist_party_id t101_party.c101_party_id%TYPE;
		v_sign		   NUMBER := 1;
		v_shipid	   NUMBER;
		v_count 	   NUMBER;
	BEGIN
		-- get the order id ,account id, poid, dist type, inter_party_id
		SELECT DECODE(p_order_type,2533,get_rule_value (get_distributor_inter_party_id (p_dist_id), 'ICTACCOUNTMAP'),get_dist_account_id(p_dist_id))
			 , get_distributor_inter_party_id (p_dist_id)
			 , DECODE(p_order_type,2533,TO_CHAR (SYSDATE, 'YYYYMMDD') || '-' || s501_order_ict_po.NEXTVAL,p_poid), get_distributor_type (p_dist_id)
		  INTO v_account_id
			 , v_dist_party_id
			 , p_poid, v_dist_type
		  FROM DUAL;

		BEGIN
			SELECT get_distributor_inter_party_id (t703.c701_distributor_id)
			  INTO v_party_id
			  FROM t704_account t704, t703_sales_rep t703
			 WHERE t704.c704_account_id = v_account_id AND t704.c703_sales_rep_id = t703.c703_sales_rep_id;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				raise_application_error (-20910, '');
		END;

		-- get the created by
		IF p_type = 50250
		THEN   --consignment
			SELECT c504_created_by, get_next_order_id (NULL), 1
			  INTO v_created_by, p_order_id, v_sign
			  FROM t504_consignment
			 WHERE c504_consignment_id = p_ref_id;
		ELSIF p_type = 50251
		THEN   --return
			SELECT c506_created_by, 'GM' || s501_order.NEXTVAL || 'R', -1
			  INTO v_created_by, p_order_id, v_sign
			  FROM t506_returns
			 WHERE c506_rma_id = p_ref_id;
		END IF;

		-- receive_mode = 5015 (fax)
		--dlvry carr = 5040 (N/A)
		-- dlvry mode = 5031(N/A)
		----Swiss Inter company Account Order /--OUS Distributor Order based on order type
		gm_save_item_order (p_order_id
						  , v_account_id
						  , 5015 --FAX
						  , NULL
						  , NULL
						  , p_poid
						  , 0
						  , v_created_by
						  , NULL
						  , p_order_type --ICS/ OUS Distributor
						  , NULL
						  , NULL
						  , NULL
						  , NULL
						  , 5040 --N/A
						  , 5031 --N/A
						  , NULL
						  , v_shipid
						   );
		gm_pkg_op_ict.gm_op_fch_part_detail (p_ref_id, p_type, v_account_id, 70105, v_party_id, cur_part_details);	 -- 70105 as only ICS Order
		v_post_id	:= get_rule_value (p_type, 'ORDERICS');

		SELECT COUNT (1)
		  INTO v_count
		  FROM t504_consignment
		 WHERE c504_consignment_id = p_ref_id AND c504_inhouse_purpose = 50061 AND c504_void_fl IS NULL;

	  ---50060 regular, 50061 ICT Sample, 40057 ICT
	 --iterate
--	 FOR set_val IN v_part_details
		LOOP
			FETCH cur_part_details
			 INTO c_pnumid, c_itemq, c_controlnum, c_voidflg, c_price;

			EXIT WHEN cur_part_details%NOTFOUND;

			IF (c_price IS NULL AND v_count = 0)
			THEN
				raise_application_error (-20911, '');
			END IF;

			IF (c_price IS NULL AND v_count > 0)
			THEN
				c_price 	:= 0;
			END IF;

			gm_pkg_op_order_master.gm_op_sav_order_detail (p_order_id
														 , c_pnumid
														 , c_itemq * v_sign
														 , c_controlnum
														 , c_price
														 , ''
														 , c_voidflg
														  );
			
			IF(p_order_type = 2533)
			THEN
				gm_save_ledger_posting (v_post_id, CURRENT_DATE, c_pnumid, v_account_id, p_ref_id, c_itemq, c_price , v_created_by);
			END IF;
		END LOOP;

		CLOSE cur_part_details;

		--end iterate
		SELECT SUM (NVL (TO_NUMBER (c502_item_qty) * TO_NUMBER (c502_item_price), 0))
		  INTO v_total
		  FROM t502_item_order
		 WHERE c501_order_id = p_order_id
		 AND c502_void_fl IS NULL;

		UPDATE t501_order
		   SET c501_status_fl = 2
			 , c501_total_cost = NVL (v_total, 0)
			 , c501_shipping_date = TRUNC (SYSDATE)
			 , c501_last_updated_by = p_userid
			 , c501_last_updated_date = SYSDATE
			 , c501_ship_cost = p_freightamt
		 WHERE c501_order_id = p_order_id;

		
		IF p_type = 50250 --AND p_order_type = 2533
		THEN
			INSERT INTO t504c_consignment_ict_detail
						(c504_consignment_id, c501_order_id, c504c_created_by, c504c_created_date
						)
				 VALUES (p_ref_id, p_order_id, p_userid, SYSDATE
						);
		END IF;
		
	END gm_op_sav_ict_order;

 /*******************************************************
 * Purpose: This Procedure is used to generate	order for OUS distributor
 **********************************************************/
--
	PROCEDURE gm_op_sav_ous_dist_order (
		p_ref_id	   IN		t501_order.c501_ref_id%TYPE --ACK order
	  , p_order_type   IN		t501_order.c901_order_type%TYPE --102080 --OUS Distributor 2533 --ICS	  
	  , p_userid	   IN		t101_user.c101_user_id%TYPE
	  , p_freightamt   IN		t501_order.c501_ship_cost%TYPE
	  , p_poid		   IN		t501_order.c501_customer_po%TYPE
	  , p_input_str    IN       CLOB	 
	  , p_order_id		OUT 	t501_order.C501_ORDER_ID%TYPE
	)
	AS
		v_created_by   t504_consignment.c504_created_by%TYPE;		
		cur_part_details TYPES.cursor_type;
		c_pnumid	   t502_item_order.c205_part_number_id%TYPE;
		c_itemq 	   t502_item_order.c502_item_qty%TYPE;
		c_controlnum   t502_item_order.c502_control_number%TYPE;
		c_voidflg	   t502_item_order.c502_void_fl%TYPE;
		c_price 	   NUMBER;		
		v_total 	   NUMBER;		
		v_sign		   NUMBER := 1;
		v_shipid	   NUMBER;
		v_count 	   NUMBER;
		v_order_id	   t501_order.c501_order_id%TYPE;
		v_dist_id	   t504_consignment.c701_distributor_id%TYPE;
		v_account_id   t501_order.c704_account_id%TYPE;
		v_mode         t907_shipping_info.c901_Delivery_mode%TYPE; 
		v_carrier      t907_shipping_info.c901_Delivery_carrier%TYPE;
		p_rowid		   t502_item_order.c502_item_order_id%type;
		v_poid		   t501_order.c501_customer_po%TYPE;
	BEGIN
      
	 SELECT c704_account_id,
		  get_distributor_id(c703_sales_rep_id)
		  INTO v_account_id,v_dist_id
		FROM t501_order
		WHERE c501_order_id = p_ref_id
		AND c501_void_fl  IS NULL;
		
		-- get mode and carrier from ack order and pass to OUS dist order
		BEGIN
		SELECT c901_Delivery_mode,
			  c901_Delivery_carrier
			INTO v_mode,
			  v_carrier
			FROM t907_shipping_info
			WHERE c907_ref_id = p_ref_id
			AND C907_VOID_FL IS NULL; 
		EXCEPTION
			WHEN TOO_MANY_ROWS
			THEN
		    v_mode := 5004;
		    v_carrier := 5001;
		END;        		
   	    -- adding below code to use unique PO to avoid issues in invoice paperwork like existing item process
		BEGIN
		SELECT p_poid
		  ||'-'||(COUNT(1)+1) INTO v_poid
			FROM t501_order t501, t501_order t502
			WHERE  t501.c704_account_id = v_account_id
			AND t501.c501_customer_po = p_poid
			AND t501.c501_void_fl   IS NULL
	    	AND t502.c501_void_fl   IS NULL
			AND t502.c901_order_type = 102080  -- ous dist order
	    	AND t502.c501_ref_id = t501.c501_order_id
			AND t501.c901_order_type = 101260 ;-- Acknowledgement Order
		EXCEPTION
			WHEN OTHERS
			THEN
		    v_poid := p_poid;
		END;
    
      SELECT p_userid, get_next_order_id (NULL), 1
		INTO v_created_by, v_order_id, v_sign
		  FROM dual;
		 -- to identify the OUS distributor orders in device
		v_order_id := replace(v_order_id,'GM','GMOUS');
		gm_save_item_order (v_order_id
						  , v_account_id
						  , 5015 --FAX
						  , NULL
						  , NULL
						  , v_poid
						  , 0
						  , v_created_by
						  , NULL
						  , 102080 --ICS/ OUS Distributor
						  , NULL
						  , NULL
						  , 4120 --distributor
						  , v_dist_id  -- dist id
						  , v_carrier --carrier fedex
                          , v_mode --mode priority overnight
                 		  , NULL
						  , v_shipid
						  );

				
			-- to inser part details
			gm_pkg_op_ict.gm_op_sav_ous_dist_part_detail(v_order_id, v_account_id, p_userid,p_input_str,p_rowid);					
			

			--end iterate
			SELECT SUM (NVL (TO_NUMBER (c502_item_qty) * TO_NUMBER (c502_item_price), 0))
			  INTO v_total
			  FROM t502_item_order
			 WHERE c501_order_id = v_order_id
			 AND c502_void_fl IS NULL;
	
			UPDATE t501_order
			   SET c501_status_fl = 1 
				 , c501_total_cost = NVL (v_total, 0)
				 , c501_last_updated_by = p_userid
				 , c501_last_updated_date = SYSDATE
				 , c501_ship_cost = p_freightamt
				 , c501_ref_id = p_ref_id  --update ack order
				 , c501_delete_fl = 'Y'
			 WHERE c501_order_id = v_order_id;
		p_order_id := v_order_id;
	END gm_op_sav_ous_dist_order;


   /*******************************************************
 * Purpose: This Procedure is used to generate invoice
 *******************************************************/
--
	PROCEDURE gm_op_sav_ict_invoice (
		p_order_id	 IN 	  t501_order.c501_order_id%TYPE
	  , p_invtype	 IN 	  t503_invoice.c901_invoice_type%TYPE
	  , p_inv_id	 OUT	  t503_invoice.c503_invoice_id%TYPE
	)
	AS
		v_custpo	   t501_order.c501_customer_po%TYPE;
		v_accid 	   t501_order.c704_account_id%TYPE;
		v_created_by   t501_order.c501_created_by%TYPE;
		v_payterms	   t704_account.c704_payment_terms%TYPE;
	BEGIN
		SELECT c704_account_id, c501_customer_po, c501_created_by
		  INTO v_accid, v_custpo, v_created_by
		  FROM t501_order
		 WHERE c501_order_id = p_order_id;

		SELECT c704_payment_terms
		  INTO v_payterms
		  FROM t704_account
		 WHERE c704_account_id = v_accid;
	--50200 p_invtype
		gm_generate_invoice (v_custpo, v_accid, 0, CURRENT_DATE, v_created_by, 'ADD', p_invtype, 50257, p_inv_id);

		UPDATE t501_order
		   SET c503_invoice_id = p_inv_id
			 , c501_status_fl = 3
			 , c501_last_updated_by = v_created_by
			 , c501_last_updated_date = SYSDATE
		 WHERE c501_order_id = p_order_id;
		 
		 gm_pkg_ac_invoice_txn.gm_sav_invoice_amount(p_inv_id,v_created_by);
	END gm_op_sav_ict_invoice;

	  /*******************************************************
 * Purpose: This Procedure is used to generate	invoice and
 *******************************************************/
--
	PROCEDURE gm_op_sav_dist_invoice (
		p_poid		   IN		t501_order.c501_customer_po%TYPE
	  , p_distid	   IN		t701_distributor.c701_distributor_id%TYPE
	  , p_ref_id	   IN		t503a_invoice_txn.c503a_ref_id%TYPE
	  , p_created_by   IN		t504_consignment.c504_created_by%TYPE
	  , p_userid	   IN		t101_user.c101_user_id%TYPE
	  , p_totalcost    IN		t503a_invoice_txn.c503a_total_amt%TYPE
	  , p_freightamt   IN		t501_order.c501_ship_cost%TYPE
	  , p_invtype	   IN		t503_invoice.c901_invoice_type%TYPE
	  , p_reftype	   IN		t503a_invoice_txn.c901_ref_type%TYPE
	  , p_dist_order_id	 IN  t501_order.c501_order_id%TYPE
	   ,p_dist_account_id IN  t501_order.c704_account_id%TYPE
	  , p_outinvid	   OUT		t503_invoice.c503_invoice_id%TYPE
	)
	AS
		v_inv_source   t503_invoice.c901_invoice_source%TYPE;
		v_payterms t704_account.c704_payment_terms%TYPE;
		
	BEGIN		
		-- p_invtype - 50200 regular
		IF p_reftype = 50258
		THEN   -- ICS
			v_inv_source := 50253;
		END IF;
		/*The below function is added for PMT-834 here we are getting payment term for distributor from distributoe setup screen*/
		v_payterms := get_ict_payment_terms(p_distid);
		
		gm_generate_invoice (p_poid, NULL, NULL, CURRENT_DATE, p_created_by, 'ADD', p_invtype, v_inv_source, p_outinvid);
		
		INSERT INTO t503a_invoice_txn
					(c503a_invoice_txn_id, c503_invoice_id, c901_ref_type, c503a_ref_id, c503a_customer_po
				   , c503a_total_amt, c503a_last_updated_by, c503a_last_updated_date,c501_order_id
					)
			 VALUES (s503a_invoice_txn_id.NEXTVAL, p_outinvid, p_reftype, p_ref_id, p_poid
				   , p_totalcost, p_userid, SYSDATE,p_dist_order_id
					);

		UPDATE t503_invoice
		   SET c701_distributor_id = p_distid,
		       c704_account_id = p_dist_account_id,
		       c503_terms = v_payterms,
               c503_last_updated_date = sysdate,
               c503_last_updated_by = p_userid
		 WHERE c503_invoice_id = p_outinvid;
		
		 --Update Invoice id in order table for OUS Distributor
		 UPDATE t501_order
		   SET c503_invoice_id = p_outinvid
			 , c501_status_fl = 3
			 , c501_last_updated_by = p_userid
			 , c501_last_updated_date = SYSDATE			 
		 WHERE c501_order_id = p_dist_order_id;
	 --The Below Code is added as part of PMT-5901.This code is used to update status as closed for ICS Invoice which is having price as 0$.
		 gm_pkg_ac_invoice.gm_upd_invoice_status(v_inv_source,p_outinvid,p_userid);
		 gm_pkg_ac_invoice_txn.gm_sav_invoice_amount(p_outinvid,p_userid); --Save Invoice and Payment Amount
	END gm_op_sav_dist_invoice;

   /*******************************************************
 * Purpose: This Procedure is used to generate	consignment/return invoice (distributor invoice)
 *******************************************************/
--
	PROCEDURE gm_op_sav_dist_csg_invoice (
		p_poid		   IN	t501_order.c501_customer_po%TYPE
	  , p_csg_id	   IN	t504_consignment.c504_consignment_id%TYPE
	  , p_userid	   IN	t504_consignment.c504_created_by%TYPE
	  , p_freightamt   IN	t501_order.c501_ship_cost%TYPE
	  , p_dist_order_id	 IN  t501_order.c501_order_id%TYPE
	  , p_dist_account_id  IN t501_order.c704_account_id%TYPE
	)
	AS
		v_custpo	   t501_order.c501_customer_po%TYPE;
		v_created_by   t501_order.c501_created_by%TYPE;
		v_dist_id	   t504_consignment.c701_distributor_id%TYPE;
		p_inv_id	   t503_invoice.c503_invoice_id%TYPE;
		v_total 	   t503a_invoice_txn.c503a_total_amt%TYPE;
		v_conv_freight t501_order.c501_ship_cost%TYPE;
		v_outinvid	   t503_invoice.c503_invoice_id%TYPE;
	BEGIN
		IF p_poid IS NULL
		THEN
			SELECT TO_CHAR (SYSDATE, 'YYYYMMDD') || '-' || s501_order_ict_po.NEXTVAL
			  INTO v_custpo
			  FROM DUAL;
		ELSE
			v_custpo	:= p_poid;
		END IF;

		SELECT c504_created_by, c701_distributor_id
		  INTO v_created_by, v_dist_id
		  FROM t504_consignment
		 WHERE c504_consignment_id = p_csg_id;

		SELECT SUM (NVL (TO_NUMBER (c505_item_qty) * TO_NUMBER (c505_item_price), 0))
		  INTO v_total
		  FROM t505_item_consignment
		 WHERE c504_consignment_id = p_csg_id
		 AND c505_void_fl IS NULL;

		SELECT get_currency_conversion (get_rule_value (50219, 'USD')
									  , get_rule_value (50219
													  , get_rule_value (50218
																	  , get_distributor_inter_party_id (v_dist_id)
																	   )
													   )
									  , SYSDATE
									  , p_freightamt
									   )
		  INTO v_conv_freight
		  FROM DUAL;

		UPDATE t504_consignment
		   SET c504_ship_cost = v_conv_freight
		     , c504_last_updated_by = p_userid
			 , c504_last_updated_date = SYSDATE
		 WHERE c504_consignment_id = p_csg_id;

		gm_op_sav_dist_invoice (v_custpo
							  , v_dist_id
							  , p_csg_id
							  , v_created_by
							  , p_userid
							  , v_total
							  , v_conv_freight
							  , 50200
							  , 50258
							  ,p_dist_order_id
							  ,p_dist_account_id
							  , v_outinvid
							   );
		--Update OUS Distributor Order Details					   
		UPDATE t501_order
		   SET c501_ship_cost = v_conv_freight
		   	 ,c501_customer_po = v_custpo
			 , c501_last_updated_by = p_userid
			 , c501_last_updated_date = SYSDATE
		WHERE c501_order_id = p_dist_order_id;

	END gm_op_sav_dist_csg_invoice;
	
 /*******************************************************
 * Purpose: This Procedure is used to generate	invoice from ous distributor order while ship out)
 * Created by : APrasath
 * Created Date : 09/13/2018
 *******************************************************/
--
	PROCEDURE gm_op_sav_ous_dist_invoice (
	    p_dist_order_id	 IN  t501_order.c501_order_id%TYPE
	  ,	p_poid		   IN	t501_order.c501_customer_po%TYPE
	  , p_userid	   IN	t504_consignment.c504_created_by%TYPE
	)
	AS
		v_custpo	   t501_order.c501_customer_po%TYPE;
		v_created_by   t501_order.c501_created_by%TYPE;
		v_dist_id	   t504_consignment.c701_distributor_id%TYPE;
		v_total 	   t503a_invoice_txn.c503a_total_amt%TYPE;
		v_conv_freight t501_order.c501_ship_cost%TYPE;
		v_outinvid	   t503_invoice.c503_invoice_id%TYPE;
		v_freightamt   	t501_order.c501_ship_cost%TYPE;
		v_dist_account_id   t501_order.c704_account_id%TYPE;
	BEGIN
		IF p_poid IS NULL
		THEN
			SELECT TO_CHAR (SYSDATE, 'YYYYMMDD') || '-' || s501_order_ict_po.NEXTVAL
			  INTO v_custpo
			  FROM DUAL;
		ELSE
			v_custpo	:= p_poid;
		END IF;

		SELECT c704_account_id,
		  get_distributor_id(c703_sales_rep_id),
		  c501_created_by,
		  c501_ship_cost
		  INTO v_dist_account_id,v_dist_id,
		  v_created_by,v_freightamt 
		FROM t501_order
		WHERE c501_order_id = p_dist_order_id
		AND c501_void_fl  IS NULL;	
     
     	SELECT SUM (NVL (TO_NUMBER (c502_item_qty) * TO_NUMBER (c502_item_price), 0))
		  INTO v_total
		  FROM t502_item_order
		 WHERE c501_order_id = p_dist_order_id
		 AND c502_void_fl IS NULL;

		SELECT get_currency_conversion (get_rule_value (50219, 'USD')
									  , get_rule_value (50219
													  , get_rule_value (50218
																	  , get_distributor_inter_party_id (v_dist_id)
																	   )
													   )
									  , SYSDATE
									  , v_freightamt
									   )
		  INTO v_conv_freight
		  FROM DUAL;

		gm_op_sav_dist_invoice (v_custpo
							  , v_dist_id
							  , NULL
							  , v_created_by
							  , p_userid
							  , v_total
							  , v_conv_freight
							  , 50200
							  , 50258
							  ,p_dist_order_id
							  ,v_dist_account_id
							  , v_outinvid
							   );
		--Update OUS Distributor Order Details					   
		UPDATE t501_order
		   SET c501_ship_cost = v_conv_freight
		   	 ,c501_customer_po = v_custpo
			 , c501_last_updated_by = p_userid
			 , c501_last_updated_date = SYSDATE
		WHERE c501_order_id = p_dist_order_id;

END gm_op_sav_ous_dist_invoice;


/*******************************************************
 * Purpose: This Procedure is used to generate return invoice (distributor invoice)
 *******************************************************/
--
	PROCEDURE gm_op_sav_dist_ret_invoice (
		p_return_id   IN	   t506_returns.c506_rma_id%TYPE
	  , p_userid	  IN	   t506_returns.c506_created_by%TYPE
	  , p_dist_order_id	IN   t501_order.c501_order_id%TYPE
	  , p_dist_account_id IN  t501_order.c704_account_id%TYPE
	  , p_outinvid	  OUT	   t503_invoice.c503_invoice_id%TYPE
	)
	AS
		v_custpo	   t501_order.c501_customer_po%TYPE;
		v_created_by   t501_order.c501_created_by%TYPE;
		v_dist_id	   t506_returns.c701_distributor_id%TYPE;
		v_total 	   t503a_invoice_txn.c503a_total_amt%TYPE;
	BEGIN
		SELECT TO_CHAR (SYSDATE, 'YYYYMMDD') || '-' || s501_order_ict_po.NEXTVAL
		  INTO v_custpo
		  FROM DUAL;

		SELECT c506_created_by, c701_distributor_id
		  INTO v_created_by, v_dist_id
		  FROM t506_returns
		 WHERE c506_rma_id = p_return_id;

		SELECT SUM (NVL (TO_NUMBER (c507_item_qty) * -1 * TO_NUMBER (c507_item_price), 0))
		  INTO v_total
		  FROM t507_returns_item
		 WHERE c506_rma_id = p_return_id
		 AND c507_status_fl = 'C';

		gm_op_sav_dist_invoice (v_custpo
							  , v_dist_id
							  , p_return_id
							  , v_created_by
							  , p_userid
							  , v_total
							  , 0
							  , 50202
							  , 50258
							  , p_dist_order_id
							  , p_dist_account_id
							  , p_outinvid
							   );
		--Update OUS Distributor Order Details						   
		UPDATE t501_order
		   SET c501_customer_po = v_custpo
		     , c506_rma_id = p_return_id
			 , c501_last_updated_by = p_userid
			 , c501_last_updated_date = SYSDATE
		WHERE c501_order_id = p_dist_order_id;
	END gm_op_sav_dist_ret_invoice;

  /*******************************************************
 * Purpose: This Procedure is used to fetch part detail
 *******************************************************/
--
	PROCEDURE gm_op_fch_part_detail (
		p_ref_id		 IN 	  t504_consignment.c504_consignment_id%TYPE
	  , p_type			 IN 	  t504_consignment.c504_type%TYPE
	  , p_account_id	 IN 	  t501_order.c704_account_id%TYPE
	  , p_dist_type 	 IN 	  t701_distributor.c901_distributor_type%TYPE
	  , p_party_id		 IN 	  t101_party.c101_party_id%TYPE
	  , p_part_details	 OUT	  TYPES.cursor_type
	)
	AS
		v_order_id	   t501_order.c501_order_id%TYPE;
--	 v_price	 t705_account_pricing.c705_unit_price%TYPE;
		c_pnumid	   t502_item_order.c205_part_number_id%TYPE;
		c_itemq 	   t502_item_order.c502_item_qty%TYPE;
		c_controlnum   t502_item_order.c502_control_number%TYPE;
		c_voidflg	   t502_item_order.c502_void_fl%TYPE;
		c_price 	   NUMBER;
	BEGIN
		IF p_type = 50250
		THEN
			--consignment
			OPEN p_part_details
			 FOR
				SELECT c205_part_number_id, c505_item_qty, c505_control_number, c505_void_fl,
					  DECODE (p_dist_type, '70105', get_account_part_pricing (NULL, c205_part_number_id, p_party_id)
							  , get_account_part_pricing (p_account_id, c205_part_number_id, NULL)) price
				   FROM t505_item_consignment
				  WHERE c504_consignment_id = p_ref_id
				   AND c505_void_fl is null;	   
		ELSE
			OPEN p_part_details
			 FOR
				 SELECT c205_part_number_id, c507_item_qty, c507_control_number, NULL
					  , DECODE (p_dist_type
							  , '70105', get_account_part_pricing (NULL, c205_part_number_id, p_party_id)
							  , get_account_part_pricing (p_account_id, c205_part_number_id, NULL)
							   ) price
				   FROM t507_returns_item
				  WHERE c506_rma_id = p_ref_id AND c507_status_fl = 'C';
		END IF;
	END gm_op_fch_part_detail;


 /*******************************************************
 * Purpose: This Procedure is used to save part detail
 *******************************************************/
	PROCEDURE gm_op_sav_ous_dist_part_detail(
		p_order_id	  IN t501_order.c501_order_id%TYPE,
		p_account_id  IN t501_order.c704_account_id%TYPE,		
		p_userid	  IN t101_user.c101_user_id%TYPE ,
		p_input_str   IN CLOB,
		p_rowid		  OUT T502_ITEM_ORDER.C502_ITEM_ORDER_ID%type
	)
	AS
	
		v_price 	   NUMBER;
		v_count			NUMBER;
		v_string CLOB:= p_input_str; --'partno,qty,price|partno,qty,price|';
		v_substring VARCHAR2(200);
		v_part_no   t205_part_number.c205_part_number_id%TYPE;
  		v_qty   NUMBER;
  		v_total	number;
		v_cnt number :=0;	
	BEGIN	 	
		SELECT COUNT (1)
		  INTO v_count
		  FROM T502_ITEM_ORDER
		 WHERE c501_order_id = p_order_id
		AND c502_void_fl        IS NULL;  
		
					
		WHILE INSTR (v_string, '|') <> 0
        		LOOP
              v_substring   := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
              v_string	    := SUBSTR (v_string, INSTR (v_string, '|') + 1);
              v_part_no		:= NULL;
              v_qty 	    := NULL;
              v_price 		:= NULL;
              v_part_no     := SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
              v_substring   := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
              v_qty         := SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
              v_substring   := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
              v_price       := v_substring;
		         
                   	
           
             IF (v_price IS NULL AND v_count = 0)
			THEN
				raise_application_error (-20911, '');
			END IF;

			IF (v_price IS NULL AND v_count > 0)
			THEN
				v_price 	:= 0;
			END IF;
			v_cnt := v_cnt+1;
             gm_pkg_op_order_master.gm_op_sav_order_detail (p_order_id
														 , v_part_no
														 , v_qty 
														 , NULL
														 , v_price
														 , ''
														 , NULL
														  );
			
			 UPDATE t502_item_order
				SET c502_unit_price=v_price, c502_unit_price_adj_value = 0 
				WHERE c501_order_id   = p_order_id 
				    AND c205_part_number_id = v_part_no
				    AND c502_item_qty = v_qty
					AND c502_void_fl IS NULL; 
		
		if(v_cnt = 1)
		then			
		select max(C502_ITEM_ORDER_ID)	into p_rowid
		FROM t502_item_order 
		WHERE c501_order_id = p_order_id 
		AND c205_part_number_id = v_part_no
		AND c502_item_qty = v_qty
		AND c502_void_fl IS NULL;
		end if; 
			 			
		END LOOP;
		
		SELECT SUM (NVL (TO_NUMBER (c502_item_qty) * TO_NUMBER (c502_item_price), 0)) 
		INTO v_total 
		FROM t502_item_order 
		WHERE c501_order_id = p_order_id 
		AND c502_void_fl IS NULL; 

		UPDATE t501_order 
		SET c501_total_cost = NVL (v_total, 0) 
		, c501_last_updated_by = p_userid 
		, c501_last_updated_date = SYSDATE 
		WHERE c501_order_id = p_order_id;
		
	
	END gm_op_sav_ous_dist_part_detail;
	
   /*******************************************************
 * Purpose: This Procedure is used to fetch ICT Summary
 *******************************************************/
--
	PROCEDURE gm_op_fch_ict_summary (
		p_ref_id	IN		 t504_consignment.c504_consignment_id%TYPE
	  , p_summary	OUT 	 TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_summary
		 FOR
			 SELECT   t504.c504_consignment_id consignid, TO_CHAR (t504.c504_ship_date, GET_RULE_VALUE('DATEFMT', 'DATEFORMAT')) shipdate
					-- SQL for ICT
					  ,get_user_name (t504.c504_last_updated_by) transby, t501.c503_invoice_id invoiceid
					, t501.c501_customer_po poid, get_code_name (t501.c901_order_type) invtype
					, t501.c901_order_type invtypeid
					, (DECODE (t504.c504_inhouse_purpose, 50061, 0, t501.c501_total_cost) + t501.c501_ship_cost
					  ) invamt, DECODE (t701.c901_distributor_type,70105,NVL(get_rule_value (50218,  get_distributor_inter_party_id (get_account_dist_id(t501.c704_account_id))),'USD'),'USD') currency,   --ict will alwys be USD
											   t501.c501_order_id orderid
					, DECODE (t701.c901_distributor_type
							, 70103, get_distributor_name (t504.c701_distributor_id)
							, get_account_name (t501.c704_account_id)
							 ) acctype
				 FROM t504_consignment t504, t504c_consignment_ict_detail t504c, t501_order t501
					, t701_distributor t701
				WHERE t504.c504_consignment_id = p_ref_id
				  AND t504.c504_consignment_id = t504c.c504_consignment_id
				  AND t501.c501_order_id = t504c.c501_order_id
				  AND t701.c701_distributor_id = t504.c701_distributor_id
				  AND (t501.c901_ext_country_id IS NULL OR t501.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH
				  AND (t701.c901_ext_country_id IS NULL OR t701.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH
				  AND NVL(t501.C901_ORDER_TYPE,-9999) = 2533
			 UNION ALL
			 SELECT   t504.c504_consignment_id consignid, TO_CHAR (t504.c504_ship_date, GET_RULE_VALUE('DATEFMT', 'DATEFORMAT')) shipdate
					-- SQL for Foreign Distributor
					  ,get_user_name (t504.c504_last_updated_by) transby, t503a.c503_invoice_id invoiceid
					, t503a.c503a_customer_po poid, get_code_name (t503a.c901_ref_type) invtype
					, t503a.c901_ref_type invtypeid
					, (DECODE (t504.c504_inhouse_purpose, 50061, 0, t503a.c503a_total_amt) + t504.c504_ship_cost
					  ) invamt
					, get_rule_value (50218, get_distributor_inter_party_id (t504.c701_distributor_id)) currency
					, t504.c504_consignment_id orderid, get_distributor_name (t504.c701_distributor_id) acctype
				 FROM t503a_invoice_txn t503a, t504_consignment t504
				WHERE t504.c504_consignment_id = p_ref_id AND t504.c504_consignment_id = t503a.c503a_ref_id
			 ORDER BY invoiceid DESC;
	END gm_op_fch_ict_summary;

		/*******************************************************
	* Purpose: This Procedure is used to save ICT returns
	*******************************************************/
	PROCEDURE gm_po_sav_ict_returns (
		p_raid		  IN   t506_returns.c506_rma_id%TYPE
	  , p_distid	  IN   t701_distributor.c701_distributor_id%TYPE
	  , p_userid	  IN   t506_returns.c506_last_updated_by%TYPE
	  , p_dist_type   IN   t701_distributor.c901_distributor_type%TYPE
	)
	AS
		v_order_id	   t501_order.c501_order_id%TYPE;
		v_po_id 	   t501_order.c501_customer_po%TYPE;
		v_account_id   t501_order.c704_account_id%TYPE;
		v_newictinvid  t503_invoice.c503_invoice_id%TYPE;
		v_newdistinvid t503_invoice.c503_invoice_id%TYPE;
		v_total_cost   t501_order.c501_total_cost%TYPE;
		v_dist_party_id t101_party.c101_party_id%TYPE;
		v_account_map_id t501_order.c704_account_id%TYPE;
		v_dist_party_map_id t101_party.c101_party_id%TYPE;
		v_dist_account_id   t501_order.c704_account_id%TYPE;
	BEGIN
		SELECT get_rule_value (get_distributor_inter_party_id (p_distid), 'ICTACCOUNTMAP')
			 , get_distributor_inter_party_id (p_distid)
			 ,get_dist_account_id(p_distid)
		  INTO v_account_map_id
			 , v_dist_party_map_id
			 ,v_dist_account_id
		  FROM DUAL;

		IF p_dist_type = 70105	 -- ICS
		THEN
			IF v_account_map_id IS NULL
			THEN
				raise_application_error (-20910, '');
			END IF;
			
			IF v_dist_account_id IS NULL
			THEN
				raise_application_error (-20910, '');
			END IF;

			--If v_dist_account_id is more than one account 
			IF v_dist_account_id = 'TOO_MANY_ROWS'
			THEN
				raise_application_error (-20910, '');
			END IF;
			
			gm_op_sav_ict_order (p_raid,2533 ,50251, p_userid, p_distid, 0, v_order_id, v_po_id);

			SELECT c704_account_id, c501_total_cost
			  INTO v_account_id, v_total_cost
			  FROM t501_order
			 WHERE c501_order_id = v_order_id;

			gm_op_sav_ict_invoice (v_order_id, 50202, v_newictinvid);

			UPDATE t501_order
			   SET c503_invoice_id = v_newictinvid
				 , c506_rma_id = p_raid
				 , c501_last_updated_by = p_userid
				 , c501_last_updated_date = SYSDATE
			 WHERE c501_order_id = v_order_id;
		END IF;

		gm_update_return_price (p_raid, v_dist_party_map_id);
		--in above call of gm_update_return_price procedure 507 table gets updated.
		--When ever the child is updated/Inserted, the parent table has to be updated ,so ETL can Sync it to GOP.
		UPDATE t506_returns
		   SET C506_LAST_UPDATED_BY = p_userid, 
		       C506_LAST_UPDATED_DATE = SYSDATE
		WHERE C506_rma_Id = p_raid;
		
		IF p_dist_type = 70105	 -- ICS
		THEN
			gm_op_sav_ict_order (p_raid,102080 ,50251, p_userid, p_distid, 0, v_order_id, v_po_id);
			gm_op_sav_dist_ret_invoice (p_raid, p_userid,v_order_id,v_dist_account_id, v_newdistinvid);
		END IF;

		gm_pkg_op_ict.gm_op_mail_ict_return (p_raid, v_newictinvid, v_newdistinvid, v_order_id, v_total_cost);
	END gm_po_sav_ict_returns;

	/*******************************************************
	* Purpose: This Procedure emails the details of the ICT returns
	*******************************************************/
	PROCEDURE gm_op_mail_ict_return (
		p_raid			   IN	t506_returns.c506_rma_id%TYPE
	  , p_ict_invoice_id   IN	t503_invoice.c503_invoice_id%TYPE
	  , p_fd_invoice_id    IN	t503_invoice.c503_invoice_id%TYPE
	  , p_order_id		   IN	t501_order.c501_order_id%TYPE
	  , p_total_cost	   IN	t501_order.c501_total_cost%TYPE
	)
	AS
		subject 	   VARCHAR2 (100);
		to_mail 	   VARCHAR2 (200);
		mail_body	   VARCHAR2 (4000);
		crlf		   VARCHAR2 (2) := CHR (13) || CHR (10);
	BEGIN
		-- Email Area
		to_mail 	:= get_rule_value ('ICTRETURN', 'EMAIL');
		subject 	:= ' ICT Returns credited ';
		mail_body	:=
			   'ICT Returns has been credited. Find the details below '
			|| crlf
			|| crlf
			|| 'RA : '
			|| p_raid
			|| crlf
			|| crlf
			|| 'ICT Invoice Id : '
			|| p_ict_invoice_id
			|| crlf
			|| crlf
			|| 'FD Invoice Id : '
			|| p_fd_invoice_id
			|| crlf
			|| crlf
			|| 'Return Order Id: '
			|| p_order_id
			|| crlf
			|| crlf
			|| 'Total Cost : $'
			|| p_total_cost;
		gm_com_send_email_prc (to_mail, subject, mail_body);
	END gm_op_mail_ict_return;

	/*******************************************************
	* Purpose: Function to get the total cost of ICT
	*******************************************************/
	FUNCTION get_csg_ship_cost (
		p_ref_id   IN	t503a_invoice_txn.c503a_ref_id%TYPE
	)
		RETURN NUMBER
	IS
--
		v_shipcost	   t504_consignment.c504_ship_cost%TYPE;
	BEGIN
		SELECT NVL (t504.c504_ship_cost, 0)
		  INTO v_shipcost
		  FROM t504_consignment t504
		 WHERE t504.c504_consignment_id = p_ref_id;

		RETURN v_shipcost;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN 0;
	END get_csg_ship_cost;

	/*******************************************************
	* Purpose: Function  to get the release of shipment. For ICT Change
	*******************************************************/
	FUNCTION get_ship_date (
		p_ref_id   IN	t907_shipping_info.c907_ref_id%TYPE
	)
		RETURN VARCHAR2
	IS
--
		v_shipdt	   VARCHAR2 (120);
		v_datefmt 	   VARCHAR2(10);
	BEGIN
		SELECT get_compdtfmt_frm_cntx() INTO v_datefmt FROM DUAL;
		SELECT TO_CHAR (c907_shipped_dt, v_datefmt)
		  INTO v_shipdt
		  FROM t907_shipping_info
		 WHERE c907_ref_id = p_ref_id AND c907_void_fl IS NULL;

		RETURN v_shipdt;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN '';
	END get_ship_date;
	
	/*******************************************************
	* Purpose: This Procedure used to fetch all the ICT country
	*******************************************************/
	PROCEDURE gm_fch_posting_by_country (
		p_rule_grp		IN      t906_rules.c906_rule_grp_id%TYPE,
		p_country_list	OUT 	 TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_country_list 
		FOR
		 SELECT DISTINCT DECODE (c906_rule_id, 102460, 1103, 102461, 1103, c906_rule_id) CODEID, get_code_name (DECODE (
    c906_rule_id, 102460, 1103, 102461, 1103, c906_rule_id)) CODENM
   FROM t906_rules
  WHERE c906_rule_grp_id = p_rule_grp --ICT_CNTRY_DIST_MAP
  ORDER BY CODENM;
  END gm_fch_posting_by_country;
  
  /*******************************************************
	* Purpose: This Procedure used to fetch ICS bank details
	*******************************************************/
	PROCEDURE gm_fch_ics_bank_dtls (
	        p_dist_id  IN t701_distributor.c701_distributor_id%TYPE,
	        p_party_id IN t101_party.c101_party_id%TYPE,
	        p_out_bank_dtls OUT TYPES.cursor_type)
	AS
	    v_ics_account t704_account.c704_account_id%TYPE;
	    v_rule_id t906_rules.c906_rule_id%TYPE;
	    v_gmna_ics_acc_fl VARCHAR2(10);
	BEGIN
	    -- to get the ICS account
	     SELECT get_rule_value (p_party_id, 'ICTACCOUNTMAP')
	       INTO v_ics_account
	       FROM dual;
	    -- to check the Globus ICS account
	    SELECT get_rule_value (v_ics_account, 'ICS_CN_GMNA_ADD')  
	      INTO v_gmna_ics_acc_fl
	      FROM dual;
	    -- to get the rule id based on ICS account flag
	    SELECT DECODE(v_gmna_ics_acc_fl, 'Y', v_ics_account, p_dist_id)
	      INTO v_rule_id
	    FROM dual;
	    --
	    OPEN p_out_bank_dtls FOR
	    SELECT get_rule_value (v_rule_id, 'IBAN') iban,
	    get_rule_value (v_rule_id, 'SWIFTCODE') SwiftCode,
	    get_rule_value (v_rule_id, 'BANK') bank_add,
	    get_rule_value (v_rule_id, 'IMPORT_SHIPPER_TAX') import_Shipper_tax
	    FROM dual;
	    
	END gm_fch_ics_bank_dtls;
	
PROCEDURE gm_release_ous_dist_order (
	p_orderid	  IN	   t501_order.c501_order_id%TYPE 
   , p_userid	  IN   t501_order.c501_last_updated_by%TYPE
	)
	AS
	v_cnt				NUMBER;
	v_dist_id	   t504_consignment.c701_distributor_id%TYPE;
	p_dist_id		t701_distributor.c701_distributor_id%TYPE;
	
	BEGIN
	
		SELECT COUNT(*) INTO V_CNT FROM t501_order WHERE c501_order_id = p_orderid;
		IF v_cnt > 0 THEN
		 UPDATE t501_order
			   SET  c501_delete_fl = NULL 
			   , c501_order_date = TRUNC (CURRENT_DATE)
			   , c501_order_date_time = CURRENT_DATE
			   , c501_last_updated_by = p_userid
			   , c501_last_updated_date = CURRENT_DATE 
			 WHERE c501_order_id = p_orderid AND c501_void_fl IS NULL;
		--To View the order in device
			 gm_pkg_allocation.gm_ins_invpick_assign_detail (93001, p_orderid, p_userid);
		-- To add insert for parts 
             gm_pkg_op_sav_insert_txn.gm_sav_insert_txn_dtl(p_orderid,106760,93342,p_userid,NULL);      
            
		END IF;	
		--To Validate Parts ICT pricing and send mail
		select get_distributor_id(t501.c703_sales_rep_id) INTO p_dist_id from t501_order t501 where c501_order_id= p_orderid;
		  gm_pkg_op_ict.gm_order_price_check_email(p_orderid, p_dist_id);  
		 
  END gm_release_ous_dist_order;
   /*******************************************************
 * Purpose: This Procedure is used to validate Parts ICT pricing
 **********************************************************/
  PROCEDURE gm_op_validate_dist_price (
  	  p_refid	      IN	   t907_shipping_info.c907_ref_id%TYPE
    , p_dist_id	      IN	   t701_distributor.c701_distributor_id%TYPE
  	, p_partnum	      OUT	   varchar2
  	, p_dist_name	  OUT      t701_distributor.c701_distributor_name%TYPE
    )
   
    AS
    v_dist_party_id 	t101_party.c101_party_id%TYPE;
    v_company_id    	t1900_company.c1900_company_id%TYPE;
    v_validate_fl		VARCHAR2(2);
    v_part_numbers_id   clob;
    v_dist_name         t701_distributor.c701_distributor_name%TYPE;
    
    BEGIN	   
	   
	   -- PC-4299 - OUS Distributor Code Change 
	    BEGIN
	    	SELECT get_distributor_id(t704.c703_sales_rep_id) INTO v_dist_party_id
			FROM T701_DISTRIBUTOR t701,  T704_ACCOUNT t704
			WHERE t701.C701_DISTRIBUTOR_ID = p_dist_id
			AND t704.c704_account_id       = get_rule_value (t701.C101_PARTY_INTRA_MAP_ID, 'ICTACCOUNTMAP')
			AND t704.c704_void_fl IS NULL
			AND t701.c701_void_fl IS NULL;		
		EXCEPTION WHEN OTHERS THEN
			v_dist_party_id := '';
		END;

		SELECT get_distributor_name(v_dist_party_id) INTO v_dist_name FROM dual; --PMT-42833		
	    p_dist_name := v_dist_name;
		
		SELECT get_compid_frm_cntx () INTO v_company_id FROM DUAL;

		SELECT get_rule_value_by_company(v_dist_party_id,'ICSPRICECHECK',v_company_id) INTO v_validate_fl FROM dual;
		
		IF v_validate_fl = 'Y' THEN
		select c101_party_intra_map_id INTO v_dist_party_id from t701_distributor where c701_distributor_id=v_dist_party_id;
	
		BEGIN
			SELECT RTRIM(XMLAGG(XMLELEMENT(e,c205_part_number_id || ',')).EXTRACT('//text()').getclobval(),',')--Even after updating ICT pricing unable to shipout displays validation BUG-10830
                 INTO v_part_numbers_id
                  FROM
                      (SELECT t502.c205_part_number_id,
                              t705.c705_unit_price
                             FROM t705_account_pricing t705,
                                  T205_PART_NUMBER t205 ,
                                 (SELECT c205_part_number_id
                                  FROM t502_item_order
                                  WHERE C501_ORDER_ID = p_refid
                                  AND c502_void_fl   IS NULL
                                  GROUP BY c205_part_number_id
                                 ) t502
               WHERE t705.c101_party_id(+)  = v_dist_party_id
                   AND t502.C205_PART_NUMBER_ID = t205.C205_PART_NUMBER_ID
                   AND t502.c205_part_number_id = t705.c205_part_number_id (+)
                   AND t705.c705_void_fl(+)    IS NULL
                      )
               WHERE c705_unit_price IS NULL; 
		EXCEPTION
		WHEN NO_DATA_FOUND THEN
		v_part_numbers_id := null;	
		END;
		END IF;
		p_partnum:=v_part_numbers_id;
		
  	END gm_op_validate_dist_price;
  /*******************************************************
 * Purpose: This Procedure is used to validate Parts ICT pricing and send mail 
 **********************************************************/
  	PROCEDURE gm_order_price_check_email (
  		p_orderid	   IN	  t501_order.c501_order_id%TYPE
  	  , p_dist_id	   IN	  t701_distributor.c701_distributor_id%TYPE
  	)
  	AS
  
    v_company_id    	t1900_company.c1900_company_id%TYPE;
    v_partnum		    varchar2(2000);
    subject 	   		VARCHAR2 (100);
	to_mail 	   		VARCHAR2 (200);
	mail_body	 	  	VARCHAR2 (4000);
	v_dist_name         t701_distributor.c701_distributor_name%TYPE;
	
  	BEGIN
  		SELECT get_compid_frm_cntx () INTO v_company_id FROM DUAL;
  		
  		gm_pkg_op_ict.gm_op_validate_dist_price(p_orderid, p_dist_id, v_partnum,v_dist_name);
  				
		IF v_partnum is not null THEN

		to_mail     := get_rule_value_by_company ('OUSDIST_PRICE', 'EMAIL',v_company_id);
     	subject     := get_rule_value_by_company ('OUSDIST_SUBJECT', 'EMAILSUBJECT',v_company_id) || ' ' || p_orderid;
		mail_body   := '<meta http-equiv="Content-Type" content="text/html"  charset="UTF-8" />';
	    mail_body   := mail_body || '<style>TD{ FONT-SIZE: 11px; COLOR: #000000; FONT-FAMILY: verdana, arial, sans-serif;}</style><font face=arial size="2">';
	    mail_body   := mail_body || ' The following Parts are missing ICT Pricing: <Part Numbers listed in separate rows>' || v_partnum || ' <br>'; 
	    mail_body   := mail_body || ' Distributor name:' || v_dist_name || ' <br>';
	    mail_body   := mail_body || ' Order Id:' || p_orderid || ' <br>';	    
		
	    gm_com_send_html_email(to_mail, subject, NULL, mail_body);
		END IF;
		 
		END gm_order_price_check_email;

END gm_pkg_op_ict;
/
