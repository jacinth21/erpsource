--@"C:\database\packages\operations\gm_pkg_op_order_dtl.bdy";
CREATE OR REPLACE PACKAGE BODY gm_pkg_op_order_dtl
IS

/**************************************************************************************************************
	* Description :  The below procedure is used to track the DO's device type
***************************************************************************************************************/

PROCEDURE gm_track_DO_device_typ(
    p_uuid     IN T9150_DEVICE_INFO.C9150_DEVICE_UUID%TYPE,
    p_order_id IN T9035_SCREEN_USAGE_TRACK.C9035_TRANSACTION_ID%TYPE,
    p_user_id  IN T9035_SCREEN_USAGE_TRACK.C9035_SCREEN_USAGE_TRACK_ID%TYPE )

AS
	v_device_type T9150_DEVICE_INFO.C901_DEVICE_TYPE%TYPE;
	BEGIN
		BEGIN  
			SELECT C901_DEVICE_TYPE
		  	INTO v_device_type
		  	FROM T9150_DEVICE_INFO
		  	WHERE C9150_VOID_FL  IS NULL
		  	AND C9150_DEVICE_UUID = p_uuid;
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
		  v_device_type:=NULL;
	END;	  
		  --to save device type
		  gm_pkg_cm_usage_track.Gm_sav_usage_track(p_user_id, v_device_type, p_order_id);
		  
END gm_track_DO_device_typ;
END gm_pkg_op_order_dtl;
/