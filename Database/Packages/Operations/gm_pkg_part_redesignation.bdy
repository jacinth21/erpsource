--@"c:\database\packages\operations\gm_pkg_part_redesignation.bdy";
CREATE OR REPLACE
PACKAGE BODY gm_pkg_part_redesignation
IS
  /********************************************************************
  * Description : This procedure is to fetch the part details
  * Author      : arajan
  *********************************************************************/
PROCEDURE GM_FCH_PART_REDESIGNATION(
    p_part_num 		IN 	VARCHAR2 ,
	p_donor_no		IN 	VARCHAR2,
    p_out_ctrl_dtls OUT TYPES.cursor_type,
    p_ctrl_str		IN	varchar2)
AS
  v_c205d_master_Productcnt NUMBER;
  v_partnumcnt  NUMBER;
  v_repack_qty NUMBER;
  v_company_id  T1900_COMPANY.C1900_company_id%TYPE;
  v_plant_id    T5040_PLANT_MASTER.c5040_plant_id%TYPE;
  v_cmp_dt_fmt  VARCHAR2(50);
  v_donor_str		CLOB := p_donor_no;
  v_pnum_str		CLOB := p_part_num;
  v_pnum_comma_cnt	NUMBER;
  v_donor_comma_cnt	NUMBER;
  v_comma_cnt		NUMBER;
BEGIN
	SELECT NVL(REGEXP_COUNT(p_part_num, ',', 1, 'i'),0)
	INTO v_pnum_comma_cnt
	FROM DUAL;
	
	SELECT NVL(REGEXP_COUNT(p_donor_no, ',', 1, 'i'),0)
	INTO v_donor_comma_cnt
	FROM DUAL;
	
	IF v_pnum_comma_cnt > v_donor_comma_cnt
	THEN
		v_comma_cnt:= v_pnum_comma_cnt - v_donor_comma_cnt;
		WHILE(v_comma_cnt > 0)
		LOOP
			v_donor_str := v_donor_str || ',';
			v_comma_cnt := v_comma_cnt - 1;
		END LOOP;
	END IF;
	
	IF v_donor_comma_cnt > v_pnum_comma_cnt
	THEN
		v_comma_cnt:= v_donor_comma_cnt - v_pnum_comma_cnt;
		WHILE(v_comma_cnt > 0)
		LOOP
			v_pnum_str := v_pnum_str || ',';
			v_comma_cnt := v_comma_cnt - 1;
		END LOOP;
	END IF;
	
	
	my_context.set_my_cloblist(p_ctrl_str|| ',');
	my_context.set_double_clob_inlist_ctx (v_pnum_str|| ',',v_donor_str|| ',');
	
SELECT get_compid_frm_cntx(), get_plantid_frm_cntx() , NVL(get_compdtfmt_frm_cntx(),get_rule_value('DATEFMT', 'DATEFORMAT'))
  INTO v_company_id, v_plant_id , v_cmp_dt_fmt 
  FROM DUAL;

SELECT  NVL(SUM(t5060.c5060_qty),0) 
  INTO v_repack_qty
  FROM t5060_control_number_inv t5060
 where t5060.c901_warehouse_type = '90815'
   and ( (t5060.c205_part_number_id in (select token from v_clob_double_list) AND t5060.c1900_company_id = v_company_id AND t5060.c5040_plant_id = v_plant_id)
        OR t5060.c205_part_number_id in ( select c205_part_number_id
                   from t2540_donor_master t2540 
                      , t2550_part_control_number t2550
                  where t2540.c2540_donor_id = t2550.c2540_donor_id
                   and c2540_donor_number in (select tokenii from v_clob_double_list) AND t2540.c1900_company_id = v_company_id) 
        OR (t5060.c5060_control_number in (SELECT TOKEN FROM v_clob_list) AND t5060.c1900_company_id = v_company_id AND t5060.c5040_plant_id = v_plant_id));

 IF v_repack_qty <= 0 THEN
   GM_RAISE_APPLICATION_ERROR('-20999','292','');
 END IF;
-- Modify the query for performance issue
OPEN p_out_ctrl_dtls FOR
	SELECT Master.c205_part_number_id frompart ,
	  Master.c5060_control_number CNUM ,
	  Master.c5060_qty repackqty ,
	  Master.c5060_control_number_inv_id cnumid ,
	  get_part_size(Master.c205_part_number_id,Master.c5060_control_number) CUSTOMSIZE ,
	  TO_CHAR(FILTER.c2550_expiry_date,v_cmp_dt_fmt) expdate ,
	  FILTER.c2540_donor_number donornum ,
	  FILTER.c2540_age AGE ,
	  get_code_name(FILTER.c901_sex) SEX ,
	  get_code_name(FILTER.c901_international_use) intluse ,
	  GET_CODE_Name(FILTER.c901_research) RESEARCH
	FROM
	  (SELECT *
	  FROM
	    (SELECT t5060.c5060_control_number_inv_id ,
	      t5060.c205_part_number_id ,
	      t5060.c5060_control_number ,
	      t5060.c5060_qty ,
	      NVL(t5062.c901_status,       -999) c901_status ,
	      NVL(t5062.c5062_reserved_qty,-999) c5062_reserved_qty
	    FROM t5060_control_number_inv t5060 ,
	      t5062_control_number_reserve t5062
	    WHERE t5060.c901_warehouse_type       = '90815' --Repackage Qty Type
	    AND t5060.c5060_qty                   > 0
	    AND T5060.C5060_CONTROL_NUMBER_INV_ID = T5062.C5060_CONTROL_NUMBER_INV_ID(+)
	    AND T5060.C1900_COMPANY_ID            = v_company_id
	    AND T5060.C5040_PLANT_ID              = v_plant_id
	    AND t5062.c5062_void_fl(+)              IS NULL
        AND NVL(t5062.c901_status,-999) IN ( -999, 103962 )
        GROUP BY T5060.c5060_control_number_inv_id,T5060.c205_part_number_id,T5060.c5060_control_number,T5060.c5060_qty,T5062.c901_status,T5062.c5062_reserved_qty)	    
	    
	  
	  ) Master ,
	  (SELECT *
	  FROM
	    (SELECT t2550.c205_part_number_id ,
	      t2550.c2550_control_number ,
	      t2550.c2550_custom_size ,
	      t2550.c2550_expiry_date ,
	      t2540.c2540_donor_number ,
	      t2540.c2540_age ,
	      t2540.c901_sex ,
	      t2540.c901_international_use ,
	      t2540.c901_research
	    FROM t2540_donor_master t2540 ,
	      t2550_part_control_number t2550 ,
	      globus_app.v_clob_double_list t_dbl
	    WHERE t2540.c2540_donor_id     = t2550.c2540_donor_id
	    AND T2550.C1900_COMPANY_ID     = v_company_id
	    AND t2540.c2540_void_fl       IS NULL
	    AND ( t2540.c2540_donor_number = t_dbl.tokenii
	    OR t2550.c205_part_number_id   = t_dbl.token )
	    UNION
	    SELECT t2550.c205_part_number_id ,
	      t2550.c2550_control_number ,
	      t2550.c2550_custom_size ,
	      t2550.c2550_expiry_date ,
	      t2540.c2540_donor_number ,
	      t2540.c2540_age ,
	      t2540.c901_sex ,
	      t2540.c901_international_use ,
	      t2540.c901_research
	    FROM t2540_donor_master t2540 ,
	      t2550_part_control_number t2550 ,
	      globus_app.v_clob_list t_l
	    WHERE t2540.c2540_donor_id     = t2550.c2540_donor_id
	    AND T2550.C1900_COMPANY_ID     = v_company_id
	    AND t2540.c2540_void_fl       IS NULL
	    AND t2550.c2550_control_number = t_l.TOKEN
	    )
	  ) FILTER
	WHERE FILTER.c205_part_number_id = Master.c205_part_number_id
	AND filter.C2550_CONTROL_NUMBER  = master.C5060_CONTROL_NUMBER;

END GM_FCH_PART_REDESIGNATION;
/********************************************************************
* Description : This procedure is to save the part details
* Author      : arajan
*********************************************************************/
	PROCEDURE gm_sav_part_redesignation(
	  p_part_num		   IN	 t205_part_number.c205_part_number_id%TYPE, 
	  p_input_string       IN	 clob,
      p_cntnum_inv_string  IN    clob,
      p_user_id		       IN	 t412_inhouse_transactions.c412_last_updated_by%TYPE,
      p_stage_date	       IN	 t412_inhouse_transactions.c412_stage_date%TYPE,
      p_out_txn_id         OUT   VARCHAR2
      )
      
AS
  v_con_id VARCHAR2(20);
  v_string VARCHAR2(20);
  v_stage_date t412_inhouse_transactions.c412_stage_date%TYPE;
  v_message CLOB;
BEGIN
  -- get the next transaction id
  SELECT get_next_consign_id(103932)
  INTO v_con_id
  FROM dual;
  -- call the existing procedure to save the details
  gm_save_inhouse_item_consign(v_con_id,'103932','104000',NULL,NULL,NULL,p_user_id,p_input_string,v_message); --103930: Part Re-designation
  p_out_txn_id := v_con_id;
  -- Save the records in t5062
  if v_con_id is not null then
  gm_sav_part_reserve(v_con_id,'103981',p_cntnum_inv_string,p_user_id); -- 103981 (Redesignation Transaction -- RSVTXN)
  end if;
  
-- Calls gm_sav_stage_date_PTRD to save stage date 
 gm_sav_stage_date_PTRD(v_con_id,p_stage_date,p_user_id);

END gm_sav_part_redesignation;

/*************************************************************
* Description : Updates the Stage date for PTRD Transaction
* Author      : sshiny
**************************************************************/
	PROCEDURE gm_sav_stage_date_ptrd(
      p_out_txn_id         IN    t412_inhouse_transactions.c412_inhouse_trans_id%TYPE,
      p_stage_date	       IN	 t412_inhouse_transactions.c412_stage_date%TYPE,
      p_user_id		       IN	 t412_inhouse_transactions.c412_last_updated_by%TYPE)
  
  AS
  BEGIN
    
  UPDATE t412_inhouse_transactions
  SET c412_stage_date = p_stage_date
      ,c412_last_updated_by = p_user_id
      ,c412_last_updated_date = CURRENT_DATE
  WHERE c412_inhouse_trans_id = p_out_txn_id
  AND c412_void_fl  IS NULL;
  
  END gm_sav_stage_date_ptrd;

/********************************************************************
 * Description : This procedure is used to save the part Qty details
 * Author      : Bala
*********************************************************************/
PROCEDURE gm_sav_part_reserve(
	  p_trans_id           IN    t5062_control_number_reserve.c5062_transaction_id%TYPE
	, p_trans_type         IN    t412_inhouse_transactions.c412_type%TYPE
    , p_cntnum_inv_string  IN    clob  
	, p_user_id		       IN	 t5062_control_number_reserve.c5062_last_updated_by%TYPE	
	)
	AS
	
	v_strlen	   NUMBER := NVL (LENGTH (p_cntnum_inv_string), 0);
	v_string	   clob := p_cntnum_inv_string;
	v_substring    VARCHAR2 (1000);
	v_cnum_inv_id  VARCHAR2 (100);
	v_qty		   NUMBER;
	v_reserve_id	t5062_control_number_reserve.C5062_CONTROL_NUMBER_RESV_ID%type;
	v_company_id   NUMBER;
	v_plant_id     NUMBER;
	BEGIN
		
	SELECT get_compid_frm_cntx(), get_plantid_frm_cntx()
      INTO v_company_id, v_plant_id  
      FROM DUAL;
	 
	WHILE INSTR (v_string, '|') <> 0
	LOOP
		v_substring    := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
		v_string	   := SUBSTR (v_string, INSTR (v_string, '|') + 1);
		v_cnum_inv_id  := NULL;
		v_qty		   := NULL;
		v_cnum_inv_id  := SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
		v_substring    := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);		
		v_qty   	   := v_substring;		
	  
		UPDATE t5062_control_number_reserve
	              SET c5062_reserved_qty = v_qty,
		      		C5062_LAST_UPDATED_BY = p_user_id,
		      		C5062_LAST_UPDATED_DATE = SYSDATE
	            WHERE c5060_control_number_inv_id = v_cnum_inv_id
	            AND c5062_transaction_id         = p_trans_id
	              AND c5062_void_fl IS NULL;

	    IF SQL%ROWCOUNT = 0 THEN
		  SELECT S5062_CONTROL_NUMBER_RESERVE.NEXTVAL
		  INTO v_reserve_id 
	          FROM DUAL;			
	      INSERT INTO t5062_control_number_reserve
	            (C5062_CONTROL_NUMBER_RESV_ID, c5062_reserved_qty, c5060_control_number_inv_id, c901_transaction_type, c5062_transaction_id
	             , c5062_reserved_by, c5062_reserved_date, c901_status , C1900_company_id , c5040_plant_id 
	            )
	         VALUES (v_reserve_id, v_qty, v_cnum_inv_id,p_trans_type,p_trans_id
	             , p_user_id,SYSDATE, '103961' , v_company_id ,  v_plant_id -- In Progress 
	            );
		END IF;
	END LOOP;	
	
END gm_sav_part_reserve;

/********************************************************************
 * Description : This procedure is used to redesignate the control 
                 number from one part to another part  
 * Author      : VPrasath
*********************************************************************/
PROCEDURE gm_sav_redesgn_control(
	  p_refid      IN     t412_inhouse_transactions.c412_inhouse_trans_id%TYPE
	, p_pnum       IN     t413_inhouse_trans_items.c205_part_number_id%TYPE	
	, p_control    IN     t413_inhouse_trans_items.c413_control_number%TYPE
	, p_saved_cntl IN     t413_inhouse_trans_items.c413_control_number%TYPE DEFAULT NULL
	, p_to_pnum    IN OUT t413_inhouse_trans_items.c205_client_part_number_id%TYPE
	)
AS
	v_status_fl t412_inhouse_transactions.c412_status_fl%TYPE;
	v_company_id  NUMBER;
	v_plant_id    NUMBER;
	v_upd_cnt     NUMBER;
BEGIN
	SELECT get_compid_frm_cntx(), get_plantid_frm_cntx()
      INTO v_company_id, v_plant_id  
      FROM DUAL;
	 
	  BEGIN	
		
		SELECT c205_client_part_number_id,c412_status_fl
		  INTO p_to_pnum,v_status_fl
          FROM t413_inhouse_trans_items T413
		      ,t412_inhouse_transactions T412
         WHERE c205_part_number_id = p_pnum
           AND t412.c412_inhouse_trans_id = p_refid
           AND t413.c412_inhouse_trans_id = t412.c412_inhouse_trans_id
           AND c413_control_number = p_saved_cntl
           AND t412.c1900_company_id = v_company_id
           AND t412.c5040_plant_id = v_plant_id
           AND c412_void_fl IS NULL
           AND c413_void_fl IS NULL;
		   
       EXCEPTION
       WHEN NO_DATA_FOUND THEN
	   
          p_to_pnum := NULL;
        v_status_fl := NULL;
		
	   WHEN TOO_MANY_ROWS THEN
	   
         GM_RAISE_APPLICATION_ERROR('-20999','293',p_control);   
		 
       END;              
         
		 -- To save/update the control number for the PTRD Trans
           IF v_status_fl = '3' THEN           

		   /* 
			   Changing the lot to the re-designated part
		       so the lot is not attached to more than one part		
			 */
           
           SELECT count(1) INTO  v_upd_cnt
             FROM t2550_part_control_number 
            WHERE c205_part_number_id = p_to_pnum 
              AND c2550_control_number = p_control;
		/* The above count is checking if we have PTRD Qty is more than 1 for same COntrol NUmber, no need to update for those many times for same purpose.*/	
           IF v_upd_cnt = '0' THEN 
	           UPDATE t2550_part_control_number
	              SET c205_part_number_id = p_to_pnum,
		      		c2550_last_updated_trans_id = p_refid,
		      		c1900_company_id = v_company_id ,
	                c5040_plant_id = v_plant_id
	            WHERE c205_part_number_id = p_pnum
	              AND c2550_control_number = p_control;
			 
				 IF SQL%ROWCOUNT = 0 THEN
					GM_RAISE_APPLICATION_ERROR('-20999','294',p_control||'#$#'||p_pnum);
				 END IF;
			END IF;
			 
           END IF;
END gm_sav_redesgn_control;

	/********************************************************************
	 * Description : This procedure is used to validate the parts for  
					 redesignate transaction   
	 * Author      : Bala
	*********************************************************************/
	PROCEDURE gm_part_redesgn_validation(
	 p_part_num      IN     t413_inhouse_trans_items.c205_part_number_id%TYPE
	)
	AS
	  v_partnumcnt NUMBER;
      v_repack_qty NUMBER;
      v_company_id NUMBER;
      v_plant_id   NUMBER;
      
	BEGIN
		
		SELECT get_compid_frm_cntx(), get_plantid_frm_cntx()
          INTO v_company_id, v_plant_id  
          FROM DUAL;
      
			SELECT NVL(SUM(t5060.c5060_qty),0) -- Get the part num count based on master product part num
			  INTO v_repack_qty 
			  FROM t5060_control_number_inv t5060
			 WHERE t5060.c901_warehouse_type = '90815'
			   AND t5060.c205_part_number_id = p_part_num
			   AND t5060.c1900_company_id = v_company_id
               AND t5060.c5040_plant_id = v_plant_id ;
			   
			 IF v_repack_qty <= 0 THEN
			   GM_RAISE_APPLICATION_ERROR('-20999','295',p_part_num);
			 END IF;
			
			 SELECT COUNT(1)
			   INTO v_partnumcnt
			  FROM
						  (SELECT c205_part_number_id, c205d_processing_spec_type
							 FROM v205g_part_label_parameter 
					   START WITH c205_part_number_id =  p_part_num
				 CONNECT BY PRIOR  c205d_master_product  = c205_part_number_id
							UNION
						   SELECT c205_part_number_id, c205d_processing_spec_type
							 FROM v205g_part_label_parameter 
					   START WITH c205_part_number_id =  p_part_num
				 CONNECT BY PRIOR  c205_part_number_id = c205d_master_product)
			 WHERE c205_part_number_id <> p_part_num;
			 
			IF v_partnumcnt = 0 THEN
			   GM_RAISE_APPLICATION_ERROR('-20999','296',p_part_num);
			END IF;
		
	END gm_part_redesgn_validation;	

	/***************************************************************************
	 * Description : This procedure is used to fetch the Control Number Details   
					 based on Part/Donor Number  
	 * Author      : HReddi
	****************************************************************************/
	PROCEDURE gm_fch_cntl_details(
	 p_part_num      IN     t413_inhouse_trans_items.c205_part_number_id%TYPE,
	 p_donor_num	 IN 	T2540_DONOR_MASTER.C2540_DONOR_NUMBER%TYPE,
	 p_out_ctrl_dtls OUT 	TYPES.cursor_type
	)
	AS
		v_query	    	VARCHAR2(4000);
  		v_condition 	VARCHAR2(1000);
  		v_part_nums   	VARCHAR2(4000);
  		v_date_fmt   	VARCHAR2(50);
  		v_plant_id      NUMBER;
  		v_company_id    NUMBER;
	BEGIN
		SELECT get_compid_frm_cntx(), get_plantid_frm_cntx() ,  NVL(get_compdtfmt_frm_cntx(),get_rule_value('DATEFMT', 'DATEFORMAT'))
          INTO v_company_id, v_plant_id  , v_date_fmt
          FROM DUAL;         
		
		v_query := 'SELECT t5060.c5060_control_number_inv_id CNUMID
				    , T2550.C205_PART_NUMBER_ID frompart
				    , T2550.C2550_CONTROL_NUMBER CNUM
				    , TO_CHAR (T2550.C2550_EXPIRY_DATE, '''||v_date_fmt||''') expdate
				    , T2540.C2540_DONOR_NUMBER donornum
				    , GET_CODE_NAME (T2540.C901_SEX) sex
				    , T2540.C2540_AGE age
				    , DECODE(sign(nvl(t5060.c5060_qty,0) - nvl(reserved_qty, 0)),''-1'',''0'',nvl(t5060.c5060_qty,0) - nvl(reserved_qty, 0)) REPACKQTY
				    , DECODE (T2540.C901_INTERNATIONAL_USE, ''1960'', ''Y'', ''1961'', ''N'', NULL) intluse
				    , NVL (T2550.C2550_CUSTOM_SIZE, V205G.C205D_LABEL_SIZE) customsize
				   FROM T2550_PART_CONTROL_NUMBER T2550, T2540_DONOR_MASTER T2540, T5060_CONTROL_NUMBER_INV T5060 , (SELECT t5060.c5060_control_number_inv_id
							 , sum(nvl(c5062_reserved_qty,0)) reserved_qty
						  FROM t5060_control_number_inv t5060
							 , t5062_control_number_reserve t5062
						 WHERE t5060.c5060_control_number_inv_id = t5062.c5060_control_number_inv_id(+)
						   AND c5062_void_fl(+) is null 
						   AND t5060.c1900_company_id = t5062.c1900_company_id(+)
                           AND t5060.c1900_company_id = '''|| v_company_id ||'''
                           AND t5060.c5040_plant_id = '''|| v_plant_id ||'''
						   AND t5060.c205_part_number_id =  NVL('''|| p_part_num ||''',t5060.c205_part_number_id)
						GROUP BY t5060.c5060_control_number_inv_id) t5062
				  , T5052_LOCATION_MASTER T5052, V205G_PART_LABEL_PARAMETER V205G
				  WHERE T2550.C205_PART_NUMBER_ID  = T5060.C205_PART_NUMBER_ID
				    AND T5060.C205_PART_NUMBER_ID  = V205G.C205_PART_NUMBER_ID
				    AND T2550.C2540_DONOR_ID       = T2540.C2540_DONOR_ID(+)
				    AND T5060.C5060_CONTROL_NUMBER = T2550.C2550_CONTROL_NUMBER
				    AND T5060.C5060_REF_ID         = T5052.C5052_LOCATION_CD(+)
				    AND t5060.c5060_control_number_inv_id = t5062.c5060_control_number_inv_id
                    AND t5060.c1900_company_id     = '''|| v_company_id ||'''
                    AND t5060.c5040_plant_id      = '''|| v_plant_id ||'''
				    AND T2540.C2540_VOID_FL(+)    IS NULL
				    AND T5052.C5052_VOID_FL(+)    IS NULL
				    AND T5060.c901_warehouse_type = ''90815''
				    AND NVL (T5060.C5060_QTY, ''0'') > 0
				    AND DECODE(sign(nvl(T5060.c5060_qty,0) - nvl(reserved_qty, 0)),''-1'',''0'',nvl(T5060.c5060_qty,0) - nvl(reserved_qty, 0)) != 0 ';
 
 	IF p_part_num IS NOT NULL THEN
 		v_condition := ' AND T2550.c205_part_number_id = '''||p_part_num||'''';
 	END IF; 	
 	IF p_donor_num IS NOT NULL THEN
 		v_condition := ' AND UPPER (T2540.C2540_DONOR_NUMBER) = UPPER ('''||p_donor_num||''')';
 	END IF;	
 	
	OPEN p_out_ctrl_dtls FOR v_query || v_condition ;
	
	END gm_fch_cntl_details;
	
	/***************************************************************************
	 * Description : This procedure is used to fetch the TOPart Details   
					 based on Part/Donor Number  
	 * Author      : HReddi
	****************************************************************************/
	PROCEDURE gm_fch_toPart_details(
	 p_part_num      	IN  t413_inhouse_trans_items.c205_part_number_id%TYPE,
	 p_donor_num	 	IN 	T2540_DONOR_MASTER.C2540_DONOR_NUMBER%TYPE,
	 p_out_topart_dtls 	OUT TYPES.cursor_type
	)
	AS		
  		v_part_nums   	VARCHAR2(4000);
  		v_company_id    NUMBER;
  		v_plant_id      NUMBER;
  		
  		CURSOR part_detail_cursor
		IS
		 SELECT DISTINCT T2550.C205_PART_NUMBER_ID partnum
   		   FROM T2550_PART_CONTROL_NUMBER T2550, T2540_DONOR_MASTER T2540
   		      , T5060_CONTROL_NUMBER_INV T5060
  		  WHERE T2550.C205_PART_NUMBER_ID        = T5060.C205_PART_NUMBER_ID
    		AND T2550.C2540_DONOR_ID             = T2540.C2540_DONOR_ID(+)
    		AND T5060.C5060_CONTROL_NUMBER       = T2550.C2550_CONTROL_NUMBER
    		AND T2540.C2540_VOID_FL(+)           IS NULL
    		AND t5060.c1900_company_id           = t2550.c1900_company_id(+)
            AND t5060.c1900_company_id           = v_company_id
            AND t5060.c5040_plant_id             = v_plant_id
   			AND NVL (T5060.C5060_QTY, '0')       > 0
   			AND T5060.c901_warehouse_type 		 = '90815'
    		AND UPPER (T2540.C2540_DONOR_NUMBER) = UPPER (p_donor_num);
	BEGIN
		
		SELECT get_compid_frm_cntx(), get_plantid_frm_cntx()
          INTO v_company_id, v_plant_id 
          FROM DUAL;      
		-- Validating the Resulted part weather the part having enough Qty in Pepack Qty or not
		FOR curindex IN part_detail_cursor
		LOOP
			gm_pkg_part_redesignation.gm_part_redesgn_validation(curindex.partnum);
		END LOOP;
		
	/* IF p_part_num is NULL, we need to fetch the part numbers for the input Donor Number and then we need to fetch Mapped parts based on the Resulted all the Parts.*/
	IF p_part_num IS NULL THEN
	DELETE FROM my_temp_key_value;
		FOR curindex IN part_detail_cursor
		LOOP			
			INSERT INTO my_temp_key_value
                (my_temp_txn_id, my_temp_txn_key)
				SELECT c205_part_number_id , frompart  
			  	  FROM (SELECT c205_part_number_id, c205d_processing_spec_type , curindex.partnum frompart
						  FROM v205g_part_label_parameter 
					START WITH c205_part_number_id   = curindex.partnum
			  CONNECT BY PRIOR c205d_master_product  = c205_part_number_id
						 UNION
						SELECT c205_part_number_id, c205d_processing_spec_type , curindex.partnum frompart
						  FROM v205g_part_label_parameter 
					START WITH c205_part_number_id = curindex.partnum
			  CONNECT BY PRIOR c205_part_number_id = c205d_master_product)
			      WHERE c205_part_number_id <> curindex.partnum;
		END LOOP;
		OPEN p_out_topart_dtls FOR SELECT my_temp_txn_id topart, my_temp_txn_key frompart FROM my_temp_key_value;
	 ELSE  	
	 	OPEN p_out_topart_dtls FOR
			SELECT c205_part_number_id topart , frompart  
		  	  FROM (SELECT c205_part_number_id, c205d_processing_spec_type , p_part_num frompart
					  FROM v205g_part_label_parameter 
				START WITH c205_part_number_id = p_part_num
		  CONNECT BY PRIOR c205d_master_product  = c205_part_number_id
					 UNION
					SELECT c205_part_number_id, c205d_processing_spec_type , p_part_num frompart
					  FROM v205g_part_label_parameter 
				START WITH c205_part_number_id = p_part_num
		  CONNECT BY PRIOR c205_part_number_id = c205d_master_product)
		      WHERE c205_part_number_id <> p_part_num;	           
	 END IF; 
	END gm_fch_toPart_details;

/********************************************************************
 * Description : This procedure is used to post the re-designation                 
 * Author      : VPrasath
*********************************************************************/
PROCEDURE gm_sav_ptrd_posting
   (p_post_id       IN	 VARCHAR, 
    p_post_cpc      IN 	 VARCHAR, 
	p_partnum		IN   t205_part_number.c205_part_number_id%TYPE,
	p_to_partnum	IN   t205_part_number.c205_part_number_id%TYPE,
    p_party_id      IN   VARCHAR2,
    p_trans_id 		IN   VARCHAR2,	
	p_trans_type    IN   VARCHAR2,
	p_qty			IN	 NUMBER,
	p_updated_by	IN   t205_part_number.c205_last_updated_by%TYPE
	)
   AS

   v_check_qty  NUMBER;
   v_price      NUMBER;
   v_running_total	NUMBER := 0;
   -- International posting changes.
   v_company_id  t1900_company.c1900_company_id%TYPE;
   v_plant_id_ctx t5040_plant_master.c5040_plant_id%TYPE;
   v_costing_plant_id t5040_plant_master.c5040_plant_id%TYPE;
   v_costing_id t820_costing.c820_costing_id%TYPE;
   v_owner_company_id t1900_company.c1900_company_id%TYPE;
   v_local_cost t820_costing.c820_purchase_amt%TYPE;
   
   -- Taking active layers for the from part
   CURSOR v_cursor IS
   SELECT * FROM 
		(
		 SELECT qoh
			  , po_amt
			  , owner_company_id
              , local_company_cost
			  , running_total
			  , ( CASE WHEN running_total <= p_qty THEN 'Y' ELSE 'N' END ) INCLUDE_ROW
	FROM
			(
				 SELECT c820_qty_on_hand qoh
					  , c820_purchase_amt po_amt
					  , t820.c1900_owner_company_id owner_company_id
                      , t820.c820_local_company_cost local_company_cost
					  , SUM(C820_QTY_ON_HAND) OVER (PARTITION BY c205_part_number_id ORDER BY  c820_costing_id RANGE BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) as running_total
				   FROM t820_costing t820 
				  WHERE t820.c205_part_number_id = p_partnum
					AND t820.c901_costing_type = 4900 -- inventory
					AND t820.c1900_company_id = v_company_id
					AND NVL(c5040_plant_id, -999) = NVL(v_costing_plant_id, - 999)
					AND c901_status != 4802 
			   ORDER BY c820_costing_id
			)
		)
   WHERE include_row = 'Y';
   
   BEGIN
   --
    SELECT get_plantid_frm_cntx()
      	INTO v_plant_id_ctx
      FROM dual;
    -- to get the paranet company id
    v_company_id := get_plant_parent_comp_id(v_plant_id_ctx);
    BEGIN
		SELECT DECODE(c823_plant_level, 'Y', v_plant_id_ctx, NULL)
	    INTO v_costing_plant_id
	   FROM t823_costing_type_ref
	  WHERE c901_costing_type = 4900
	    AND c823_void_fl     IS NULL;
    --
     EXCEPTION
        WHEN NO_DATA_FOUND   -- when it is scrap
        THEN
        	v_costing_plant_id:= NULL;
        END;   
   FOR v_index in v_cursor LOOP
   
	gm_sav_ptrd_posting_detail(p_post_id, 
							   p_post_cpc,
							   p_partnum,
							   p_to_partnum,
							   p_party_id,
							   p_trans_id,	
							   p_trans_type,
							   v_index.qoh,
							   v_index.po_amt,
							   p_updated_by,
							   v_index.owner_company_id,
							   v_index.local_company_cost);
   									
   v_running_total := v_index.running_total;
   
   END LOOP;
   
   v_check_qty := p_qty - v_running_total;
   -- to get the costing id (4900 FG)
	v_costing_id := get_ac_costing_id (p_partnum, 4900);
	-- to get the owner information.
	BEGIN
			SELECT t820.c1900_owner_company_id
			  , t820.c820_purchase_amt
			  , t820.c820_local_company_cost
			   INTO v_owner_company_id
			  , v_price
			  , v_local_cost
			   FROM t820_costing t820
			  WHERE t820.c820_costing_id =v_costing_id;
	   EXCEPTION WHEN NO_DATA_FOUND
	   THEN
	   		v_owner_company_id := NULL;
	   		v_price:= NULL;
	   		v_local_cost:= NULL;
	   END;
   
   if v_check_qty > 0 THEN
	
		gm_sav_ptrd_posting_detail(p_post_id, 
							   p_post_cpc,
							   p_partnum,
							   p_to_partnum,
							   p_party_id,
							   p_trans_id,	
							   p_trans_type,
							   v_check_qty,
							   v_price,
							   p_updated_by,
							   v_owner_company_id,
							   v_local_cost
							   );
     
   END IF;
	
   END gm_sav_ptrd_posting;
   
   
/********************************************************************
 * Description : This procedure is used to posting details for
                 the re-designation                 
 * Author      : VPrasath
*********************************************************************/

   PROCEDURE gm_sav_ptrd_posting_detail
    (p_post_id      IN	 VARCHAR, 
	p_post_cpc      IN 	 VARCHAR, 
	p_partnum		IN   t205_part_number.c205_part_number_id%TYPE,
	p_to_partnum	IN   t205_part_number.c205_part_number_id%TYPE,
    p_party_id      IN   VARCHAR2,
    p_trans_id 		IN   VARCHAR2,	
	p_trans_type    IN   VARCHAR2,
	p_qty			IN	 NUMBER,
	p_price		    IN   NUMBER,
	p_updated_by	IN   t205_part_number.c205_last_updated_by%TYPE,
	p_owner_comp_id IN   t820_costing.c1900_company_id%TYPE,
	p_local_cost	IN	 t820_costing.c820_local_company_cost%TYPE
  )
  AS
  
  BEGIN

	   gm_save_ledger_posting (p_post_id,
							   CURRENT_DATE,
							   p_partnum,
							   p_party_id,
							   p_trans_id,
							   p_qty,
							   p_price,
							   p_updated_by,
							   NULL,
							   p_owner_comp_id,
							   p_local_cost
							  );

	   gm_save_ledger_posting (p_post_cpc,
							   CURRENT_DATE,
							   p_to_partnum,
							   p_party_id,
							   p_trans_id,
							   p_qty,
							   p_price,
							   p_updated_by,
							   NULL,
							   p_owner_comp_id,
							   p_local_cost
							  );
	
   
	END gm_sav_ptrd_posting_detail;
	
	/********************************************************************
 	* Description : This procedure is used to validate the 'to part' number      
	 * Author      : Arajan
	*********************************************************************/
	FUNCTION get_topart_num_valid_cnt(
		p_from_part		IN		t205d_part_attribute.c205d_attribute_value%type
		, p_to_part		IN		t205d_part_attribute.c205_part_number_id%type
	)
	RETURN NUMBER
	AS
	  v_count	NUMBER;
	BEGIN
       SELECT SUM(cnt) into v_count
       FROM
      (SELECT COUNT(1) cnt
          FROM t205d_part_attribute
          WHERE C901_Attribute_Type = 103730 --Master Product
          AND C205d_Attribute_Value = p_from_part
          AND c205_part_number_id   = p_to_part
          AND c205d_void_fl        IS NULL
      UNION
           SELECT COUNT(1) cnt
           FROM t205d_part_attribute
           WHERE C901_Attribute_Type = 103730 --Master Product
           AND C205d_Attribute_Value = p_to_part
           AND C205_Part_Number_Id   = p_from_part
           AND c205d_void_fl        IS NULL
      );
	RETURN v_count;
	END get_topart_num_valid_cnt;
	
/********************************************************************
* Description : This procedure is to update the Donor Number
* Author      : Suresh
*********************************************************************/
PROCEDURE gm_sav_donor_part_redesignation(
    p_trans_id      IN    t5062_control_number_reserve.c5062_transaction_id%TYPE ,
    p_user_id       IN t412_inhouse_transactions.c412_last_updated_by%TYPE 
    )
AS
  v_cntrl_num VARCHAR2(20);
  v_donor_num VARCHAR2(20);
  v_donor_id  NUMBER;
BEGIN
  -- get the Donor number based on transaction id
  BEGIN
	   
	SELECT c413_control_number
	  INTO v_cntrl_num
	FROM t413_inhouse_trans_items
	WHERE c412_inhouse_trans_id = p_trans_id
	AND ROWNUM=1
	;
	
	EXCEPTION
       WHEN OTHERS THEN
	     v_cntrl_num := NULL;
       		
       END;    
	
BEGIN
	SELECT c2540_donor_id
	  INTO v_donor_id
	FROM t2550_part_control_number
	WHERE c2550_control_number=v_cntrl_num
    AND ROWNUM=1;
    
	EXCEPTION
       WHEN OTHERS THEN
	     v_donor_id := NULL;
 END;
 
 BEGIN
	 
	SELECT C2540_DONOR_NUMBER
	  INTO v_donor_num
	FROM t2540_donor_master
	WHERE c2540_donor_id = v_donor_id;
	
EXCEPTION
       WHEN OTHERS THEN
	     v_donor_num := NULL;
 END;
 
  UPDATE t412_inhouse_transactions
          SET C2540_DONOR_NUMBER     = v_donor_num
          , C412_LAST_UPDATED_BY	 = p_user_id
          , C412_LAST_UPDATED_DATE   = CURRENT_DATE          
        WHERE c412_inhouse_trans_id     = p_trans_id;

END gm_sav_donor_part_redesignation;

END gm_pkg_part_redesignation;
/
