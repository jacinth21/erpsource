/* Formatted on 2009/07/06 15:57 (Formatter Plus v4.8.0) */
--@"c:\database\packages\operations\gm_pkg_op_loaner_charges.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_loaner_charges
IS

/***********************************************************
 * Description : This Procedure is used to create daily loaner late charges 
 * Author	   : Sindhu
 ***********************************************************/
PROCEDURE gm_sav_loaner_late_charge (
    p_date IN DATE DEFAULT CURRENT_DATE,
    p_request_id   IN   t525_product_request.c525_product_request_id%TYPE DEFAULT NULL,
    p_prod_req_dtl_id   IN   T526_Product_Request_Detail.C526_Product_Request_Detail_Id%TYPE DEFAULT NULL
)
AS
	v_company_id	      t1900_company.c1900_company_id%TYPE;
	v_charge_start_date   t504a_loaner_transaction.c504a_expected_return_dt%TYPE;
    v_charge_end_date     DATE;
    v_incident_id         t9200_incident.c9200_incident_id%TYPE;
    v_incident_date       t9200_incident.C9200_INCIDENT_DATE%TYPE;
    v_incident_status_fl  t9200_incident.c9200_status_fl%TYPE;
    v_user_id             t9200_incident.C9200_CREATED_BY%TYPE;
    v_last_charged_date   DATE;
    v_temp_charge_start_date  DATE;
    v_late_fee    	NUMBER;
    v_job_incident_date DATE;
    v_charge_details_id  t9201_charge_details.c9201_charge_details_id%TYPE;
CURSOR request_cursor
IS
	SELECT t525.c525_product_request_id reqid,
	MAX (t504a.c504a_expected_return_dt) ex_date,
	MAX (t504a.c504a_loaner_dt) loaner_dt ,
	t526.c526_product_request_detail_id reqDetailId,
	t504.c504a_etch_id etchid
	FROM t504a_consignment_loaner t504,
	t504a_loaner_transaction t504a,
	t526_product_request_detail t526,
	t525_product_request t525,
	t207_set_master t207
	WHERE
	(
	TRUNC (t504.c504a_expected_return_dt) < TRUNC (p_date)
	OR
	t525.c525_product_request_id = p_request_id )
	AND t504.c504a_status_fl = 20 -- pending return
	AND t504.c504_consignment_id =	t504a.c504_consignment_id
	AND t526.c207_set_id = t207.c207_set_id
	AND t207.c207_void_fl IS NULL
	AND t207.c1910_division_id IN
	(
		SELECT
	    c906_rule_value
	FROM
	    t906_rules
	WHERE
	    c906_rule_id = 'DIVISIONS'
	    AND c906_rule_grp_id = 'CALCLOANERLATEFEE'
	    AND c906_void_fl IS NULL 
	)
	AND t504a.c504a_return_dt                IS NULL
	AND t504a.c504a_void_fl IS NULL
	AND t504.c504a_void_fl IS NULL
	AND t526.C526_VOID_FL IS NULL
	AND t504.c504a_missed_fl IS NULL
	AND t525.c901_request_for = 4127 -- loaner late charges only
	-- applicable for product loaner
	AND t526.c526_product_request_detail_id = t504a.c526_product_request_detail_id
	AND t525.c525_product_request_id = t526.c525_product_request_id
	AND t525.c1900_company_id = v_company_id
	AND t526.c526_product_request_detail_id = NVL (p_prod_req_dtl_id, t526.c526_product_request_detail_id)
	GROUP BY t525.c525_product_request_id,
	t526.c526_product_request_detail_id,t504.c504a_etch_id
	UNION   --this union query for Loaner transfer
	SELECT t525.c525_product_request_id reqid,
	MAX (t504a.c504a_expected_return_dt) ex_date,
	MAX (t504a.c504a_loaner_dt) loaner_dt ,
	t526.c526_product_request_detail_id reqDetailId,
	t504.c504a_etch_id etchid
	FROM t504a_consignment_loaner t504,
	t504a_loaner_transaction t504a,
	t526_product_request_detail t526,
	t525_product_request t525,
	t207_set_master t207
	WHERE
	(
	TRUNC (t504.c504a_expected_return_dt) < TRUNC (p_date)
	OR
	t525.c525_product_request_id = p_request_id )
	AND t504a.c504a_return_dt >= TRUNC(p_date-5) -- transfer in last 5 days
	AND t504.c504a_status_fl = 20 -- pending return
	AND t504.c504_consignment_id =	t504a.c504_consignment_id
	AND t526.c207_set_id = t207.c207_set_id
	AND t207.c207_void_fl IS NULL
	AND t207.c1910_division_id IN
	(
		SELECT
	    c906_rule_value
	FROM
	    t906_rules
	WHERE
	    c906_rule_id = 'DIVISIONS'
	    AND c906_rule_grp_id = 'CALCLOANERLATEFEE'
	    AND c906_void_fl IS NULL
	)
	AND t504a.c504a_void_fl IS NULL
	AND t504.c504a_void_fl IS NULL
	AND t526.C526_VOID_FL IS NULL
	AND t504.c504a_missed_fl IS NULL
	AND t525.c901_request_for = 4127 -- loaner late charges only
	-- applicable for product loaner
	AND t526.c526_product_request_detail_id = t504a.c526_product_request_detail_id
	AND t525.c525_product_request_id = t526.c525_product_request_id
	AND t525.c1900_company_id = v_company_id
	AND t526.c526_product_request_detail_id = NVL (p_prod_req_dtl_id, t526.c526_product_request_detail_id)
	GROUP BY t525.c525_product_request_id,
	t526.c526_product_request_detail_id,t504.c504a_etch_id;


CURSOR date_cursor (v_charge_start_date IN t504a_loaner_transaction.c504a_expected_return_dt%TYPE)
IS
	SELECT cal_date
	FROM date_dim
	WHERE cal_date >= v_charge_start_date 
	AND cal_date < v_charge_end_date 
	AND DAY NOT IN ('Sat','Sun')
	AND date_key_id NOT IN (
	SELECT date_key_id
	FROM COMPANY_HOLIDAY_DIM
	WHERE HOLIDAY_DATE >= v_charge_start_date 
	AND HOLIDAY_DATE <= v_charge_end_date 
	AND HOLIDAY_FL = 'Y'
	AND C1900_COMPANY_ID = v_company_id
	)
	ORDER BY cal_date;

BEGIN
	
	v_charge_end_date := TRUNC (p_date);
	/*
	Job is running everyday 1am to calculate charge upto yesterday. But, Month last date charge is adding to next month.
	So, Job incident date will be used to get previous day incident data. 
	Ex. If job executing on 01/01/2021. It will calculate charge for 12/31/2020 and adding to DEC charge instead of creating JAN charge for 12/31/2020
	*/
	v_job_incident_date:= TRUNC(p_date-1);
	
	--Fetch the company ID
	SELECT NVL(get_compid_frm_cntx(),1000)
		INTO v_company_id
	FROM DUAL;

	FOR var_request IN request_cursor
	LOOP
	
	/*Get the Incident Details for the Product request detail id from T9200_incident table for the current month*/
	  BEGIN
		SELECT c9200_incident_id, C9200_INCIDENT_DATE, c9200_status_fl, C9200_CREATED_BY
			INTO v_incident_id, v_incident_date, v_incident_status_fl, v_user_id
		FROM t9200_incident
		WHERE c9200_ref_id = TO_CHAR(var_request.reqDetailId)
		AND c9200_ref_type = 92068 -- Loaner LateFee(BySet)
		AND c9200_void_fl IS NULL
		AND TO_CHAR(c9200_incident_date,'MMYYYY') = TO_CHAR(v_job_incident_date,'MMYYYY');
		EXCEPTION
		WHEN NO_DATA_FOUND THEN
			v_incident_id:= NULL;
			v_incident_date:= v_job_incident_date; --Job run date
			v_incident_status_fl := NULL;
			v_user_id := '30301';
		END;
	
	 BEGIN
		SELECT t9201.lastChargedDate
			INTO v_last_charged_date
		FROM 
			(SELECT t9201a.c9201_charge_end_date lastChargedDate , row_number() OVER (ORDER BY t9201a.c9201_charge_end_date desc) as rn
			 FROM t9201_charge_details t9201a,t9200_incident t9200
			 WHERE t9200.C9200_REF_ID = TO_CHAR(var_request.reqDetailId)
			 AND t9201a.C9200_INCIDENT_ID = t9200.C9200_INCIDENT_ID
			 AND t9201a.C9201_STATUS IN (10,15,20,40)   --10-Accrued 15-Hold 20-Submitted 40-Waived
			 AND t9200.c9200_ref_type = 92068 -- Loaner LateFee(BySet)
			 AND t9201a.C9201_VOID_FL IS NULL 
			 AND t9200.C9200_VOID_FL IS NULL) t9201
		WHERE rn = 1; 
		EXCEPTION
		WHEN NO_DATA_FOUND THEN
			v_last_charged_date:= NULL;
		END;
	
	-- charge start date is last charged date from t9201 table or expected return date
		IF v_last_charged_date IS NOT NULL AND v_last_charged_date >= var_request.ex_date THEN
			v_temp_charge_start_date:= get_business_day(v_last_charged_date,1);
		ELSE
			v_temp_charge_start_date := var_request.ex_date;
		END IF;
	
	
	FOR var_date IN date_cursor (v_temp_charge_start_date)
	LOOP
	
		-- calculate late fee
		v_late_fee:= get_loaner_late_fee(var_request.ex_date , var_date.cal_date);
		--save late fee for current month
		--Charge start date will be the last charged date and end date will be the job run date (Current_date)
		gm_pkg_op_loaner_charges.gm_sav_incident(v_incident_id,92068, var_request.reqDetailId, 92068, var_request.etchid, v_incident_date, v_late_fee, v_incident_status_fl, v_user_id, v_company_id, v_temp_charge_start_date, var_date.cal_date,v_charge_details_id);
	END LOOP;
	--Update deduction date 
	gm_sav_deduction_date(v_charge_details_id,'20',v_user_id);
	
END LOOP;

END gm_sav_loaner_late_charge;

/***********************************************************
 * Description : This Procedure is used to calculate the late fee 
 * Author	   : Sindhu
 ***********************************************************/
FUNCTION get_loaner_late_fee(
	p_expected_return_date IN Date,
	p_charge_date IN Date
) 
	RETURN NUMBER
AS
	v_days_late NUMBER;
	v_least_amt_days  NUMBER;
	v_late_fee    	VARCHAR2(50);
	v_company_id	      t1900_company.c1900_company_id%TYPE;
BEGIN
	SELECT NVL(get_compid_frm_cntx(),1000)
		INTO v_company_id
	FROM DUAL;

	/*Get the delayed days based in the acturn return date */
	--v_days_late := gm_pkg_op_loaner_charges.get_work_days_count(p_expected_return_date, p_charge_date);
	BEGIN
		SELECT COUNT (1)
			INTO v_days_late
		FROM date_dim
		WHERE cal_date >= TRUNC (p_expected_return_date)
		AND cal_date <= TRUNC (p_charge_date)
		AND DAY NOT IN ('Sat','Sun')
		AND date_key_id NOT IN (
								SELECT date_key_id
								FROM COMPANY_HOLIDAY_DIM
								WHERE HOLIDAY_DATE >= TRUNC (p_expected_return_date)
								AND HOLIDAY_DATE <= TRUNC (p_charge_date)
								AND HOLIDAY_FL = 'Y'
								AND C1900_COMPANY_ID = v_company_id
								);
	EXCEPTION WHEN NO_DATA_FOUND THEN
		v_days_late := 0;
	END;
	
	/*Get the Lease amount of days for the loaner set to be return based on the rule value */
	v_least_amt_days := get_rule_value('LATEDAYS', 'LATEDAYS') ; --2
	
	--Added this because not to charge when the expected return days equal to charge rate
	
	IF (v_days_late > 0) THEN
		IF v_days_late <= v_least_amt_days THEN
			v_late_fee:= get_rule_value ('1006425', 'CHARGES') ; --50$
		ELSE
			v_late_fee:= get_rule_value ('92072', 'CHARGES'); --100$
		END IF;
	END IF;
	
	RETURN v_late_fee;
END get_loaner_late_fee;


/***********************************************************
 * Description : This Procedure is used to get the working days
 * Author	   : Sindhu
 ***********************************************************/
FUNCTION get_work_days_count(
	p_start_date IN DATE,
	p_end_date   IN DATE,
	p_comp_id   IN t1900_company.c1900_company_id%TYPE DEFAULT NULL
) 
	RETURN NUMBER
AS
	v_company_id	      t1900_company.c1900_company_id%TYPE;
	v_working_days      NUMBER;
BEGIN

	IF p_comp_id IS NULL THEN
		SELECT NVL(get_compid_frm_cntx(),1000)
			INTO v_company_id
		FROM DUAL;
	ELSE
		v_company_id := p_comp_id;
	END IF;
	
	BEGIN
		SELECT COUNT (1)
			INTO v_working_days
		FROM date_dim
		WHERE cal_date >= TRUNC (p_start_date)
		AND cal_date < TRUNC (p_end_date)
		AND DAY NOT IN ('Sat','Sun')
		AND date_key_id NOT IN (
								SELECT date_key_id
								FROM COMPANY_HOLIDAY_DIM
								WHERE HOLIDAY_DATE >= TRUNC (p_start_date)
								AND HOLIDAY_DATE <= TRUNC (p_end_date)
								AND HOLIDAY_FL = 'Y'
								AND C1900_COMPANY_ID = v_company_id
								);
	EXCEPTION WHEN OTHERS THEN
		RETURN 0;
	END;
	
	RETURN v_working_days;
END get_work_days_count;


/***********************************************************
 * Description : This Procedure is used to save the loaner fee details in incident table
 * Author	   : Sindhu
 ***********************************************************/
PROCEDURE gm_sav_incident(
	p_incident_id     IN OUT t9200_incident.c9200_incident_id%TYPE,
	p_reftype         IN t9200_incident.c9200_ref_type%TYPE,
	p_req_dtl_id      IN t9200_incident.c9200_ref_id%TYPE,
	p_incident_type   IN t9200_incident.c901_incident_type%TYPE,
	p_incident_value  IN t9200_incident.c9200_incident_value%TYPE,
	p_incident_date   IN t9200_incident.c9200_incident_date%TYPE,
	p_late_fee        IN t9201_charge_details.c9201_amount%TYPE,
	p_status          IN t9201_charge_details.c9201_status%TYPE,
	p_user_id         IN t9200_incident.c9200_created_by%TYPE,
	p_company_id      IN t9200_incident.c1900_company_id%TYPE,
	p_charge_start_date IN t9201_charge_details.c9201_charge_start_date%TYPE,
	p_charge_end_date IN t9201_charge_details.c9201_charge_end_date%TYPE,
	p_charge_details_id OUT t9201_charge_details.c9201_charge_details_id%TYPE
)
AS
	v_incident_id t9200_incident.c9200_incident_id%TYPE;
	v_dist_id   t504a_loaner_transaction.c504a_consigned_to_id%TYPE;
	v_rep_id    t504a_loaner_transaction.c703_sales_rep_id%TYPE;
	v_assoc_rep_id   t504a_loaner_transaction.c703_ass_rep_id%TYPE;
BEGIN
	
	BEGIN
		SELECT c9200_incident_id
			INTO v_incident_id
		FROM t9200_incident
		WHERE c9200_ref_id = TO_CHAR(p_req_dtl_id)
		AND c9200_ref_type = 92068 -- Loaner LateFee(BySet)
		AND c9200_void_fl IS NULL
		AND TO_CHAR(c9200_incident_date,'MMYYYY') = TO_CHAR(p_incident_date,'MMYYYY');
		EXCEPTION 
		WHEN NO_DATA_FOUND THEN
			v_incident_id := NULL;
		END;

	IF v_incident_id IS NULL AND p_late_fee > 0 THEN
	
		SELECT 'GM-IC-' || s9200_incident.NEXTVAL
			INTO v_incident_id
		FROM DUAL;

		INSERT INTO t9200_incident(c9200_incident_id, c9200_ref_type, c9200_ref_id,
		c901_incident_type, c9200_incident_value, c9200_created_by, c9200_created_date, c9200_incident_date,c1900_company_id)VALUES
		(v_incident_id, p_reftype, TO_CHAR(p_req_dtl_id ), p_incident_type, p_incident_value , p_user_id, CURRENT_DATE, p_incident_date, p_company_id) ;
	ELSE
		UPDATE T9200_Incident
			SET C9200_Updated_By = p_user_id,
		C9200_Updated_Date = CURRENT_DATE
		WHERE C9200_Incident_Id = v_incident_id
		AND C9200_Void_Fl IS NULL;
	END IF;

-- get distributor and rep from t504a for request detail
BEGIN
	SELECT c504a_consigned_to_id, c703_sales_rep_id, c703_ass_rep_id
		INTO v_dist_id, v_rep_id, v_assoc_rep_id
	FROM t504a_loaner_transaction
	WHERE c526_product_request_detail_id = p_req_dtl_id
	AND c504a_void_fl IS NULL
	AND ROWNUM = 1;
	EXCEPTION WHEN NO_DATA_FOUND THEN
		v_dist_id      := NULL;
		v_rep_id       := NULL;
		v_assoc_rep_id := NULL;
	END;

	gm_sav_charge_detail(v_incident_id, v_rep_id , v_dist_id, v_assoc_rep_id , p_user_id , p_late_fee, p_charge_start_date, p_charge_end_date,p_charge_details_id );
END gm_sav_incident;

/***********************************************************
 * Description : This Procedure is used to save the loaner late fee details in charge details table
 * Author	   : Sindhu
 ***********************************************************/
PROCEDURE gm_sav_charge_detail(
	p_incident_id          IN t9201_charge_details.c9200_incident_id%TYPE,
	p_rep_id               IN t9201_charge_details.c703_sales_rep_id%TYPE,
	p_dist_id              IN t9201_charge_details.c701_distributor_id%TYPE,
	p_assoc_rep_id         IN t9201_charge_details.c703_ass_rep_id%TYPE,
	p_user_id              IN t9201_charge_details.c9201_credited_by%TYPE,
	p_late_fee             IN t9201_charge_details.c9201_amount%TYPE,
	p_charge_start_date    IN t9201_charge_details.c9201_charge_start_date%TYPE,
	p_charge_end_date      IN t9201_charge_details.c9201_charge_end_date%TYPE,
	p_charge_details_id    OUT t9201_charge_details.c9201_charge_details_id%TYPE
)
AS
	v_charge_details_id    t9201_charge_details.c9201_charge_details_id%TYPE;
BEGIN
	--Take the last charge_details_id with accued/hold status

	BEGIN
		SELECT t9201.lastchargedetailId
			INTO v_charge_details_id
		FROM
			(SELECT c9201_charge_details_id lastchargedetailId, row_number() OVER (ORDER BY c9201_charge_end_date desc) as rn
			 FROM t9201_charge_details
			 WHERE c9200_incident_id = p_incident_id
			 AND (C9201_STATUS = 10 OR C9201_STATUS = 15)   --10 - accrued ,15 hold
			 AND c9201_void_fl IS NULL) t9201 
		WHERE rn = 1; 
		EXCEPTION
		WHEN NO_DATA_FOUND THEN
			v_charge_details_id := NULL;
		END;

	 --If charge detail id is null then need to insert in charge details table otherwise its updating
	IF v_charge_details_id IS NULL THEN
	
	    SELECT s9201_charge_details.NEXTVAL INTO v_charge_details_id FROM DUAL;
		INSERT INTO t9201_charge_details(c9201_charge_details_id, c9200_incident_id, c703_sales_rep_id, c701_distributor_id, c9201_amount, c9201_status, c9201_created_by, c9201_created_date, c703_ass_rep_id, c9201_charge_start_date , c9201_charge_end_date )
	    VALUES (v_charge_details_id, p_incident_id, p_rep_id, p_dist_id, p_late_fee, 10, p_user_id, CURRENT_DATE, p_assoc_rep_id, p_charge_start_date , p_charge_end_date) ;
	ELSE
		-- update late fee with (existing + new)amount
		--Charge end date is updated because we are summing up the amount with the existing amount till charge end date
		UPDATE t9201_charge_details
		SET c9201_amount= p_late_fee + NVL(c9201_amount,0)
			, c9201_updated_by = p_user_id
			, c9201_updated_date = CURRENT_DATE
			, c9201_charge_end_date = p_charge_end_date 
		WHERE c9201_charge_details_id = v_charge_details_id
		AND c9201_void_fl IS NULL;
	END IF;
	p_charge_details_id := v_charge_details_id;
END gm_sav_charge_detail;
/***********************************************************
 * Description : This Procedure is used to save the loaner late fee deduction date
 * Author	   : Gopi
 ************************************************************/		
	PROCEDURE gm_sav_deduction_date(
		p_charge_details_id IN t9201_charge_details.c9201_charge_details_id%TYPE,
		p_statusid          IN T901_CODE_LOOKUP.C901_CODE_NM%TYPE,
		p_user_id           IN t9201_charge_details.c9201_credited_by%TYPE
	)
	AS
	    v_sales_rep_id         t703_sales_rep.c703_sales_rep_id%TYPE;
		v_deduction_date       DATE;
	BEGIN
		
	     BEGIN
	      			SELECT t9201.c703_sales_rep_id  
					INTO v_sales_rep_id
					FROM t9201_charge_details t9201
					WHERE t9201.c9201_charge_details_id = p_charge_details_id 
					AND t9201.c9201_void_fl IS NULL;				
	      			
	      EXCEPTION
			WHEN OTHERS THEN
				v_sales_rep_id := NULL;
		 END;
		 
		 IF v_sales_rep_id IS NOT NULL THEN
		 
		 		v_deduction_date := gm_pkg_op_loaner_charges_rpt.get_deduction_date(v_sales_rep_id,NULL,p_statusid);
		 		
				UPDATE t9201_charge_details
				SET c9201_deduction_date = v_deduction_date,
				c9201_updated_by = p_user_id,
				c9201_updated_date = CURRENT_DATE
				WHERE c9201_charge_details_id = p_charge_details_id;	
				
		 END IF;
		 
	END gm_sav_deduction_date;
END gm_pkg_op_loaner_charges; 
/
