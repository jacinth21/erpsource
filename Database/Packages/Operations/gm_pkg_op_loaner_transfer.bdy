/* Formatted on 2010/11/03 11:45 (Formatter Plus v4.8.0) */
--@"c:\database\packages\operations\gm_pkg_op_loaner_transfer.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_loaner_transfer
IS
   /******************************************************************
    * Description : This is main Procedure called initially to do loaner to consignment swap
    ****************************************************************/
   PROCEDURE gm_op_sav_loaner_transfer (
      p_inputstr       IN       VARCHAR2,
      p_userid         IN       t101_user.c101_user_id%TYPE,
      p_type           IN       t525_product_request.c901_request_for%TYPE,
      p_transf_lnids   OUT      VARCHAR2,
      p_from_reps      OUT      CLOB,
      p_from_accs      OUT      CLOB,
      p_to_reps 	   OUT      CLOB,
      p_to_accs        OUT      CLOB
   )
   AS
      v_string              VARCHAR2 (4000)                     := p_inputstr;
      v_transid             VARCHAR2 (20);
      v_newloanid           NUMBER;
      v_consignid           t504_consignment.c504_consignment_id%TYPE;
      v_req_det_id          t526_product_request_detail.c526_product_request_detail_id%TYPE;
      v_substring           VARCHAR2 (4000);
      v_substr1             VARCHAR2 (4000);
      v_substr2             VARCHAR2 (4000);
      v_ln_status_fl        t504a_consignment_loaner.c504a_status_fl%TYPE;
      v_transfer_date       VARCHAR2 (10);
      v_transfer_ref_date   DATE;
      v_date                VARCHAR2 (10);
      v_req_det_stat        t526_product_request_detail.c526_product_request_detail_id%TYPE;
      v_dist_name           t701_distributor.c701_distributor_name%TYPE;
      v_exp_ret_date        DATE;
      v_loaner_type         t504_consignment.c504_type%TYPE;
      v_incident_id         t9200_incident.c9200_incident_id%TYPE;
      v_sales_rep_id        t504a_loaner_transaction.c703_sales_rep_id%TYPE;
      v_sales_reps_id		t504a_loaner_transaction.c703_sales_rep_id%TYPE;
      v_consigned_to_id     t504a_loaner_transaction.c504a_consigned_to_id%TYPE;
      v_transf_lnids        VARCHAR2 (1000);
      v_request_id          t525_product_request.c525_product_request_id%TYPE;
      v_set_id VARCHAR2 (100);
      v_dist_id VARCHAR2 (100);
      v_trns_type VARCHAR2 (100);
      v_req_id VARCHAR2 (100);
      v_prodreq_id t526_product_request_detail.c526_product_request_detail_id%TYPE;
      v_prodreq_ids t526_product_request_detail.c526_product_request_detail_id%TYPE;
      v_expt_fl VARCHAR2(10);
      v_expected_return_dt DATE;
      v_rep_id  t703_sales_rep.c703_sales_rep_id%TYPE;
      v_acc_id  t704_account.c704_account_id%TYPE;
      v_old_req_dtl_id   VARCHAR2(100);
      v_assoc_rep_id        t504a_loaner_transaction.c703_ass_rep_id%TYPE;
      v_from_rep_nm      t703_sales_rep.C703_SALES_REP_NAME%TYPE;
      v_from_account_nm  t704_account.C704_ACCOUNT_NM%TYPE;
      v_to_rep_nm   t703_sales_rep.C703_SALES_REP_NAME%TYPE;
      v_to_account_nm t704_account.C704_ACCOUNT_NM%TYPE;
      v_Prod_req_dtl_id T526_Product_Request_Detail.C526_Product_Request_Detail_Id%TYPE;
      
      TYPE acc_rep_array IS TABLE OF VARCHAR2 (30) INDEX BY VARCHAR2 (30) ;
      rep_acc_array acc_rep_array;
      v_comp_dt_fmt VARCHAR2 (20);
   BEGIN
      IF p_inputstr IS NULL
      THEN
         RETURN;
      END IF;
      
      SELECT get_compdtfmt_frm_cntx() INTO v_comp_dt_fmt FROM DUAL;

      -- doing the below select to ONLY lock the table
      SELECT     TO_NUMBER (c504a_loaner_transaction_id)
            INTO v_transid
            FROM t504a_loaner_transaction
           WHERE ROWNUM = 1
      FOR UPDATE;

      WHILE INSTR (v_string, '|') <> 0
      LOOP
         --1.  Fetch the value of consignmentId and requestId into v_consignid and v_req_det_id
         v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
         v_string := SUBSTR (v_string, INSTR (v_string, '|') + 1);
         v_consignid := NULL;
         v_req_det_id := NULL;
         v_transfer_date := NULL;
         v_set_id := NULL;
         v_dist_id :=NULL;
         v_trns_type :=NULL;
         v_rep_id    :=NULL;
         v_acc_id   := NULL;
         v_assoc_rep_id := NULL;
         v_consignid := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
         v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
         v_set_id := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
         v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
         v_req_det_id := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
         v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
         v_transfer_date := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
		 v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
         v_trns_type := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
         v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
         v_dist_id := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
         v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
         v_rep_id := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
         v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
         v_assoc_rep_id := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
         v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
         v_acc_id := v_substring;
        --perform validation that the tranfer date being updated is not greater than the loaned date of the transferre
         v_transid := gm_pkg_op_loaner.get_loaner_trans_id (v_consignid);

         BEGIN
            --get the loaned date and distname  for the reference id
            SELECT t504al.c504a_loaner_dt,
                   c504a_expected_return_dt, c504a_consigned_to_id,
                   NVL(t504al.c703_sales_rep_id,-999),t504al.c526_product_request_detail_id,
                   GET_REP_NAME(t504al.c703_sales_rep_id),
                   GET_ACCOUNT_NAME(t504al.c704_account_id) 
              INTO v_transfer_ref_date,
                   v_exp_ret_date, v_consigned_to_id,
                   v_sales_rep_id , v_old_req_dtl_id,
                   v_from_rep_nm,v_from_account_nm
              FROM t504a_loaner_transaction t504al
             WHERE t504al.c504a_loaner_transaction_id = v_transid
               AND t504al.c504a_void_fl IS NULL;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               v_dist_name := '';
         END;
      --getting status flag and type   
       BEGIN  
         SELECT t504a.c504a_status_fl, t504.c504_type
           INTO v_ln_status_fl, v_loaner_type
           FROM t504_consignment t504, t504a_consignment_loaner t504a
          WHERE t504.c504_status_fl = 4
            AND t504.c504_consignment_id = v_consignid
            AND t504a.c504_consignment_id = t504.c504_consignment_id
            AND t504a.c504a_void_fl IS NULL
            AND t504.c504_void_fl IS NULL;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               v_ln_status_fl := '';
               v_loaner_type  := '';
         END;

      --If it is a Disputed Product Loaner then updating the status as Pending Return
      IF p_type = '4127' AND v_ln_status_fl = '21'
      THEN
	      UPDATE  t504a_consignment_loaner 
			SET c504a_status_fl = '20',
			C504A_MISSED_FL = NULL,
			C504A_LAST_UPDATED_BY = p_userid,
			C504A_LAST_UPDATED_DATE = CURRENT_DATE
			WHERE c504_consignment_id = v_consignid
			AND c504a_void_fl IS NULL;
      
      END IF;
         --product loaner-4127
         IF(p_type = '4127') THEN
         --sales rep from name already presented in p_from_reps, to avoid duplication
         	IF(INSTR(p_from_reps,v_from_rep_nm) IS NULL OR INSTR(p_from_reps,v_from_rep_nm) = 0 ) THEN
        	 	p_from_reps:= p_from_reps || v_from_rep_nm || '/';
        	 END IF;
        	 --account from name already presented in p_from_accs, to avoid duplication
         	IF(INSTR(p_from_accs,v_from_account_nm) IS NULL OR INSTR(p_from_accs,v_from_account_nm) = 0) THEN
         		p_from_accs:= p_from_accs || v_from_account_nm || '/';
         	END IF;
         	
         	--If loaner transfer type is set transfer
         	IF v_trns_type = '1006702' THEN 
        		 SELECT   GET_REP_NAME(v_rep_id),GET_ACCOUNT_NAME(v_acc_id)
         		 INTO v_to_rep_nm,v_to_account_nm
         		 FROM DUAL;
         	
         	--v_req_det_id not in (1006704-Create For Wrong Shipment and 1006703-Create For Surgery)
	         	IF (v_req_det_id != '1006703' AND v_req_det_id != '1006704')  THEN
	         		BEGIN
		         		SELECT GET_REP_NAME(C703_SALES_REP_ID),GET_ACCOUNT_NAME(C704_ACCOUNT_ID)
		         		INTO v_to_rep_nm,v_to_account_nm
		   	--request detail dropdown is having product request id,so used c525_product_request_id
		         		FROM 	t525_product_request 
		         		WHERE c525_product_request_id = v_req_det_id;
		         		EXCEPTION
	            	WHEN NO_DATA_FOUND
	           		THEN
	               		v_to_rep_nm := '';
	               		v_to_account_nm := '';
	            	END; 
	            END IF;
            END IF;
            
            
         	--If loaner transfer type is request transfer
          	IF v_trns_type = '1006701' THEN
          	BEGIN
          --Get account name and sales rep name from request id
	          	SELECT   GET_REP_NAME(t525.C703_SALES_REP_ID),GET_ACCOUNT_NAME(t525.C704_ACCOUNT_ID)
	          	INTO v_to_rep_nm,v_to_account_nm
	  			FROM t526_product_request_detail t526,t525_product_request t525 
	  			WHERE t525.c525_product_request_id = t526.c525_product_request_id  
	  			AND t526.c526_product_request_detail_id=v_req_det_id
	  			AND t525.c525_void_fl IS NULL 
	  			AND t526.c526_void_fl IS NULL;
	  		EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               v_to_rep_nm := '';
               v_to_account_nm := '';
            END;
          	END IF;
          	
          	--sales rep to name already presented in p_to_reps, to avoid duplication
	          IF(INSTR(p_to_reps,v_to_rep_nm) IS NULL OR INSTR(p_to_reps,v_to_rep_nm) = 0 ) THEN
	        	 p_to_reps:= p_to_reps || v_to_rep_nm || '/';
	          END IF;
         --account to name already presented in p_to_accs, to avoid duplication
	         IF(INSTR(p_to_accs,v_to_account_nm) IS NULL OR INSTR(p_to_accs,v_to_account_nm) = 0) THEN
	         	p_to_accs:= p_to_accs || v_to_account_nm || '/';
	         END IF;
         END IF;
         
        SELECT DECODE(v_assoc_rep_id, '0', NULL, v_assoc_rep_id)  into v_assoc_rep_id from dual;
         
        IF v_trns_type = '1006702' THEN -- set Transfer

            /* commented for TSK-1024.
              IF v_consigned_to_id = v_dist_id THEN
               raise_application_error
                   ('-20999',
                       'Cannot Transfer the Set for Same Distributor/Employee');
              END IF;*/
              
              IF v_req_det_id = '1006704' THEN --Create For Wrong Shipment
               BEGIN 
	               -- get the product request id from array based on sales rep and account id
	             v_prodreq_id:=rep_acc_array(v_req_det_id||v_rep_id||v_acc_id);
	          EXCEPTION
              WHEN NO_DATA_FOUND
              THEN
                 v_prodreq_id :='';             
              END;
              	gm_op_sav_set_transfer(v_req_det_id,p_type,v_transfer_date,v_set_id,v_dist_id,p_userid,v_rep_id,v_acc_id,v_prodreq_id,v_assoc_rep_id,v_req_id);
              	v_expt_fl :='Y';
              	 -- set the product request id to array based on sales rep and account id
              	rep_acc_array(v_req_det_id||v_rep_id||v_acc_id):= v_prodreq_id;

              ELSE
                 -- Create for Surgery / attaching set to existing request.
               BEGIN 
	                -- get the product request id from array based on sales rep and account id
	             v_prodreq_ids:=rep_acc_array(v_req_det_id||v_rep_id||v_acc_id);
	          EXCEPTION
              WHEN NO_DATA_FOUND
              THEN
                 v_prodreq_ids :='';
              END;
              	gm_op_sav_set_transfer(v_req_det_id,p_type,v_transfer_date,v_set_id,v_dist_id,p_userid,v_rep_id,v_acc_id,v_prodreq_ids,v_assoc_rep_id,v_req_id);
              	
              	 -- set the product request id to array based on sales rep and account id
              	rep_acc_array(v_req_det_id||v_rep_id||v_acc_id):= v_prodreq_ids;
              END IF;
              v_req_det_id:= v_req_id;
           
         END IF; -- end of set transfer
        

         --2.  Confirm if the status is pending return or Disputed
         IF v_ln_status_fl <> '20' AND v_ln_status_fl <> '21'
         THEN
            raise_application_error ('-20932', '');
         --  Cannot perform loaner transfer as status is not pending return
         END IF;

         -- also confirm if the request id is in open status
         SELECT c526_status_fl
           INTO v_req_det_stat
           FROM t526_product_request_detail
          WHERE c526_product_request_detail_id = v_req_det_id
            AND c526_void_fl IS NULL;

         IF v_req_det_stat <> 10
         THEN
            raise_application_error ('-20933', '');
         --  Cannot perform loaner transfer as request status is not open
         END IF;

         IF (v_transfer_ref_date > TO_DATE (v_transfer_date, v_comp_dt_fmt))
         THEN
            GM_RAISE_APPLICATION_ERROR('-20999','245','');
         END IF;
         
        --In PMT-16743-loaner is able to transfer same Field Sales name 

   /*     SELECT NVL(t525.C703_SALES_REP_ID,-9999) INTO v_sales_reps_id 
		FROM T525_PRODUCT_REQUEST t525,T526_PRODUCT_REQUEST_DETAIL t526 
		    WHERE t525.C525_PRODUCT_REQUEST_ID = t526.C525_PRODUCT_REQUEST_ID 
  			AND t526.C526_PRODUCT_REQUEST_DETAIL_ID = v_req_det_id;
 
 		IF v_sales_rep_id = v_sales_reps_id 
 		THEN
 		GM_RAISE_APPLICATION_ERROR('-20999','246','');
 		
  		END IF;*/
		
--3.  Perform "Accept return" for loaner.
         SELECT TO_CHAR (CURRENT_DATE, v_comp_dt_fmt)
           INTO v_date
           FROM DUAL;

-- need to do accept return as of the transferred date. not for CURRENT_DATE anymore
         gm_pkg_op_loaner.gm_cs_sav_accept_return (v_consignid,
                                                   p_userid,
                                                   TO_DATE (v_transfer_date, v_comp_dt_fmt)
                                                  );

         --4.  Update the "processed date" for loaner
         UPDATE t504a_loaner_transaction
            SET c504a_processed_date = TO_DATE (v_transfer_date, v_comp_dt_fmt),
                c504a_last_updated_date = CURRENT_DATE,
                c504a_last_updated_by = p_userid
          WHERE c504a_loaner_transaction_id = TO_NUMBER (v_transid);

         --5. Allocate the request id to the released CN
         gm_op_save_transfer_allocate (v_consignid,
                                       v_req_det_id,
                                       p_userid,
                                       v_newloanid
                                      );

   -- need to override the expected return date to 2 working days for 'Create For Wrong Shipment' 
      IF v_expt_fl = 'Y' THEN
            v_expected_return_dt :=get_expected_return_dt(TO_DATE (v_transfer_date, v_comp_dt_fmt),'TRNSDATE');
            
             UPDATE t504a_loaner_transaction
			    SET c504a_expected_return_dt = v_expected_return_dt, c504a_last_updated_date = CURRENT_DATE,
			        c504a_last_updated_by = p_userid
			  WHERE c504a_loaner_transaction_id = v_newloanid
			    AND c504a_void_fl IS NULL;
			    
			 v_expt_fl:=NULL;
      ELSE
            v_expected_return_dt :=gm_pkg_op_loaner.get_loaner_expected_return_dt(TO_DATE (v_transfer_date, v_comp_dt_fmt));
      END IF;
      
     --need to overwrite the transfer date with the date passed from the screen 
         UPDATE t504a_consignment_loaner
            SET c504a_loaner_dt = TO_DATE (v_transfer_date, v_comp_dt_fmt),
                c504a_expected_return_dt =v_expected_return_dt,
                  --  TO_DATE (v_transfer_date, v_comp_dt_fmt)
                  -- + get_rule_value ('DATE', 'EXPRTDT'),
                c504a_last_updated_date = CURRENT_DATE,
                c504a_last_updated_by = p_userid
          WHERE c504_consignment_id = v_consignid AND c504a_void_fl IS NULL;
		
       --When ever the child is updated, the consignment table has to be updated ,so ETL can Sync it to GOP.
	 	UPDATE t504_consignment
		   SET c504_last_updated_by    = p_userid
	  		 , c504_last_updated_date = CURRENT_DATE
	  	 WHERE c504_consignment_id = v_consignid;	
	  	 
--calculate the late charges if any
         IF (p_type !='4119' AND TO_DATE (v_transfer_date, v_comp_dt_fmt) > v_exp_ret_date )
         THEN
            v_request_id := gm_pkg_op_loaner.get_loaner_request_id(v_transid);
            /* Get the Prodcuct request detail id and pass that also to Loaner late charge package*/
      		v_Prod_req_dtl_id  := gm_pkg_op_loaner.get_product_loaner_request_id(v_transid);
      
            gm_pkg_op_charges.gm_sav_loaner_late_charge(CURRENT_DATE,v_request_id,v_Prod_req_dtl_id);
            
         END IF;

-- transfer open ln trans to new distributor
         gm_op_sav_transf_loaner_xtn (v_transid,
                                      v_newloanid,
                                      v_consignid,
                                      p_userid,
                                      v_transf_lnids
                                     );
         p_transf_lnids := p_transf_lnids || v_transf_lnids;

         --6. To maintain history in t504a_loaner_transaction update the loaner transfer reference id for the new record
         UPDATE t504a_loaner_transaction
            SET c504a_transf_ref_id = v_transid,
                c504a_transf_date = TO_DATE (v_transfer_date, v_comp_dt_fmt),
                c504a_transf_by = p_userid,
                c504a_last_updated_date = CURRENT_DATE,
                c504a_last_updated_by = p_userid
          WHERE c504a_loaner_transaction_id = v_newloanid;
        
          IF p_type ='4119' THEN -- inhouse loaner
          
           UPDATE t526_product_request_detail
			SET c526_exp_return_date               = v_expected_return_dt, c526_last_updated_by = p_userid, c526_last_updated_date = CURRENT_DATE
			 WHERE c526_product_request_detail_id = v_req_det_id
			   AND c526_void_fl                  IS NULL;
          END IF;
          
          /* UPDATING THE T902_LOG TABLE FOR CARRYING THE RECONCILE COMMENT ALONG WITH THE LOANER TRANSFER.
           * THE FOLLOWING UPDATE QUERY IS WRITTEN UNDER THE PMT-2145 TASK BY HREDDI */
          
          UPDATE t902_log 
             SET c902_ref_id = v_req_det_id
               , c902_last_updated_by = p_userid
               , c902_last_updated_date = CURRENT_DATE
          WHERE c902_ref_id = v_old_req_dtl_id
            AND c902_void_fl IS NULL ;   
            
      
       -- Call the rollback procedure to remove the priority for the Loaner Transferred sets which is in Pending Return Status.      
	      gm_pkg_op_ln_priority_engine.gm_rollback_cn_priority(null,p_userid,v_consignid);
            
          
      END LOOP;
      -- Override usage date for Create For Wrong Shipment
      IF v_prodreq_id IS NOT NULL THEN
       UPDATE t525_product_request
		  SET c525_usage_date             = NULL, c525_last_updated_by = p_userid, c525_last_updated_date = CURRENT_DATE
		WHERE c525_product_request_id = v_prodreq_id
		  AND c525_void_fl           IS NULL;
      END IF;
      
      
      
      IF(p_type='4127') THEN
 				p_from_reps:=SUBSTR(p_from_reps , 1, INSTR(p_from_reps , '/', -1)-1);
				p_from_accs:=SUBSTR(p_from_accs , 1, INSTR(p_from_accs , '/', -1)-1);
  				p_to_reps:=SUBSTR(p_to_reps , 1, INSTR(p_to_reps , '/', -1)-1);
 				p_to_accs:=SUBSTR(p_to_accs , 1, INSTR(p_to_accs, '/', -1)-1);
 	  END IF; 
      
   END gm_op_sav_loaner_transfer;

   /******************************************************************
    * Description : This Procedure allocates the CN passed to the RQ passed
   ****************************************************************/
   PROCEDURE gm_op_save_transfer_allocate (
      p_consignid    IN       t504_consignment.c504_consignment_id%TYPE,
      p_req_det_id   IN       t526_product_request_detail.c526_product_request_detail_id%TYPE,
      p_userid       IN       t101_user.c101_user_id%TYPE,
      p_transid      OUT      NUMBER
   )
   AS
      v_req_for_id          t525_product_request.c525_request_for_id%TYPE;
      v_acc_id              t525_product_request.c704_account_id%TYPE;
      v_rep_id              t525_product_request.c703_sales_rep_id%TYPE;
      v_planned_ship_date   DATE;
      v_req_for             t525_product_request.c901_request_for%TYPE;
      v_assoc_rep_id		t525_product_request.c703_ass_rep_id%TYPE;
      v_tissue_fl			t207_set_master.c207_tissue_fl%TYPE;
   BEGIN
      SELECT c525_request_for_id, c704_account_id, c703_sales_rep_id,
             c526_required_date ,c901_request_for, c703_ass_rep_id,
             gm_pkg_op_tissues.get_tissue_set_fl(c207_set_id)   --PC-417 - To get tissue flag for the set
        INTO v_req_for_id, v_acc_id, v_rep_id,
             v_planned_ship_date ,v_req_for, v_assoc_rep_id, v_tissue_fl
        FROM t525_product_request t525, t526_product_request_detail t526
       WHERE t525.c525_product_request_id = t526.c525_product_request_id
         AND c526_product_request_detail_id = p_req_det_id
         AND c526_void_fl IS NULL
         AND c525_void_fl IS NULL;
      
         -- for inhouse loaner transfer --19518	Field Sales,50170	Distributor,50172	Employee
          IF v_req_for = '4119' THEN
	   			BEGIN
	   	  			SELECT DECODE(t7103.c901_loan_to,19518,50170,50172) ,
	             		   DECODE(t7103.c901_loan_to,19518,get_distributor_id(t7103.c101_loan_to_id),t7103.c101_loan_to_id),
	             		    DECODE(t7103.c901_loan_to,19518,t7103.c101_loan_to_id,'')
	            	  INTO v_req_for,v_req_for_id,v_rep_id
	                  FROM t7104_case_set_information t7104
	                     , t7103_schedule_info t7103
	                     , t7100_case_information t7100
	                 WHERE T7104.c526_product_request_detail_id = p_req_det_id
	                   AND t7104.c7100_case_info_id = t7103.c7100_case_info_id 
	                   AND t7103.c7100_case_info_id = t7100.c7100_case_info_id
			   		   AND t7100.c901_case_status = 19524	-- Active
			           AND t7100.c7100_void_fl IS NULL 
			           AND t7100.c901_type = 1006504
	                   AND t7104.c7104_void_fl IS NULL;
	            EXCEPTION WHEN NO_DATA_FOUND THEN
	            	RETURN;
	            END;
        ELSE
        		v_req_for := '50170';
        END IF;            
       
      gm_pkg_op_loaner.gm_sav_loaner_transaction (p_consignid,
                                                  v_planned_ship_date,
                                                  gm_pkg_op_loaner.get_loaner_expected_return_dt(v_planned_ship_date),
                                                  v_req_for,     
                                                  v_req_for_id,
                                                  p_userid,
                                                  v_rep_id,
                                                  v_acc_id,
                                                  p_req_det_id,
                                                  v_assoc_rep_id
                                                 );
     IF v_req_for !='50172' THEN
      UPDATE t504_consignment
         SET c701_distributor_id = v_req_for_id,
             c704_account_id = v_acc_id,
             c504_last_updated_by = p_userid,
			 c504_last_updated_date = CURRENT_DATE
       WHERE c504_consignment_id = p_consignid AND c504_void_fl IS NULL;
     END IF;
     
      UPDATE t504a_consignment_loaner
         SET c504a_loaner_dt = TRUNC (CURRENT_DATE),
             c504a_expected_return_dt =gm_pkg_op_loaner.get_loaner_expected_return_dt(CURRENT_DATE),
                          -- TRUNC (CURRENT_DATE)
                          -- + get_rule_value ('DATE', 'EXPRTDT'),
             c504a_last_updated_date = CURRENT_DATE,
             c504a_last_updated_by = p_userid
       WHERE c504_consignment_id = p_consignid AND c504a_void_fl IS NULL;
       
     --When ever the child is updated, the consignment table has to be updated ,so ETL can Sync it to GOP.
	 	UPDATE t504_consignment
		   SET c504_last_updated_by    = p_userid
	  		 , c504_last_updated_date = CURRENT_DATE
	  	 WHERE c504_consignment_id = p_consignid;	
	  	 
      --   Update shipping record and loaner table to mark the transaction as shipped
      UPDATE t907_shipping_info
         SET c907_status_fl = 40,
             c907_shipped_dt = TRUNC (CURRENT_DATE),
             c907_email_update_fl = 'Y',
             c907_email_update_dt = TRUNC (CURRENT_DATE),
             c907_ref_id = p_consignid,
             c901_delivery_mode = 5031,
             c901_delivery_carrier = 5040,
             c907_tracking_number = 'LOANERTRANSFER',
             c901_ship_to = DECODE(v_tissue_fl, 'Y', 4122, c901_ship_to),       --PC-417 - To Update as Hospital-4122 if the consignment has Tissue sets
             c907_ship_to_id = DECODE(v_tissue_fl, 'Y', v_acc_id, c907_ship_to_id),
             c907_last_updated_by = p_userid,
             c907_last_updated_date = CURRENT_DATE
       WHERE c907_ref_id = TO_CHAR (p_req_det_id)
         AND c901_source = 50182
         AND c907_void_fl IS NULL;

      gm_pkg_cm_shipping_trans.gm_sav_loaner_ship (p_consignid, p_userid);
      gm_pkg_cm_shipping_trans.gm_sav_shipping_status_log (p_consignid,
                                                           50182,
                                                           p_userid,
                                                           'Shipped'
                                                          );

      -- fetch the loaner trans id to return back
      SELECT MAX (TO_NUMBER (c504a_loaner_transaction_id))
        INTO p_transid
        FROM t504a_loaner_transaction
       WHERE c504_consignment_id = p_consignid AND c504a_void_fl IS NULL;
   END gm_op_save_transfer_allocate;

   /******************************************************************
    * Description : This is procedure returns a cursor of all the loaner extensions that were shipped today
    ****************************************************************/
   PROCEDURE gm_fch_loanxtns_ship_today (
      p_consignids   IN       VARCHAR2,
      p_refids       OUT      TYPES.cursor_type
   )
   AS
   BEGIN
      my_context.set_my_inlist_ctx (p_consignids);

      OPEN p_refids
       FOR
          SELECT c907_ref_id refid
            FROM t907_shipping_info
           WHERE c901_source = 50183
             AND c907_status_fl = 40
             AND c907_tracking_number IS NOT NULL
             AND c907_void_fl IS NULL
             AND c907_shipped_dt = TRUNC (CURRENT_DATE)
             AND c907_ref_id IN (SELECT c412_inhouse_trans_id
                                   FROM t412_inhouse_transactions
                                  WHERE c412_ref_id IN (SELECT token
                                                          FROM v_in_list));
   END gm_fch_loanxtns_ship_today;

   /******************************************************************
    * Description : Procedure to fetch the loaner transfer log
    * Author        : Gopinathan
    ****************************************************************/
   PROCEDURE gm_fch_loaner_transfer_log (
      p_from_distid   IN       t701_distributor.c701_distributor_id%TYPE,
      p_to_distid     IN       t701_distributor.c701_distributor_id%TYPE,
      p_setid         IN       VARCHAR2,
      p_fromdate      IN       VARCHAR2,
      p_todate        IN       VARCHAR2,
      p_status        IN       VARCHAR2,
      p_outlist       OUT      TYPES.cursor_type
   )
   AS
    v_company_id      t1900_company.c1900_company_id%TYPE;
    v_date_format     varchar2(20); 
     
   BEGIN
	    
	   SELECT get_compid_frm_cntx() ,get_compdtfmt_frm_cntx()
        INTO v_company_id, v_date_format
        FROM dual;
    
      OPEN p_outlist
       FOR     
		SELECT   loanid, to_loan.c504_consignment_id cnid, to_dist loanto,
		         from_dist loanfrom, GET_USER_NAME(to_loan.c504a_transf_by) transferred_by,
		         TO_CHAR (c504a_transf_date,v_date_format) transferred_date,
		         c207_set_id || ' / ' || get_set_name (c207_set_id) setname,
		         TO_CHAR (c504a_loaner_dt, v_date_format) loaned_on,
		         c504a_return_dt return_date, c504a_etch_id etchid,
		         DECODE(c504a_return_dt,null,'Active','Closed') RET_DT_VALUE
		    FROM (SELECT c504a_loaner_transaction_id loanid,
		                 t504a.c504_consignment_id, c504a_return_dt,
		                 t504a.c504a_loaner_dt, t504a.c504a_transf_by,
		                 (   'D:'
		                  || get_distributor_name (t504a.c504a_consigned_to_id)
		                  || '<BR>A:'
		                  || get_account_name (t504a.c704_account_id)
		                  || '<BR>R:'
		                  || get_rep_name (t504a.c703_sales_rep_id)
		                 ) to_dist,
		                 c504a_transf_ref_id, c504a_transf_date, t504.c207_set_id,
		                 t504al.c504a_etch_id
		            FROM t504a_loaner_transaction t504a,
		                 t504_consignment t504,
		                 t504a_consignment_loaner t504al
		           WHERE t504a.c504_consignment_id = t504.c504_consignment_id
		             AND t504al.c504_consignment_id = t504.c504_consignment_id
		             AND c504a_transf_ref_id IS NOT NULL
		             AND c504a_consigned_to_id =
		                    DECODE (p_to_distid,
		                            0, c504a_consigned_to_id,
		                            p_to_distid
		                           )
		             AND t504a.c504a_void_fl IS NULL
		             AND t504.c504_void_fl IS NULL
		             AND t504al.c504a_void_fl IS NULL
		             AND t504a.c1900_company_id = v_company_id
		             AND ((p_status='21' AND c504a_return_dt IS NOT NULL)
  					 	OR
  						  (p_status='20' AND c504a_return_dt IS NULL)
  						OR
  						 (p_status='22' AND (c504a_return_dt IS null OR c504a_return_dt IS NOT NULL)))) to_loan,
		         (SELECT (   'D:'
		                  || get_distributor_name (t504a.c504a_consigned_to_id)
		                  || '<BR>A:'
		                  || get_account_name (t504a.c704_account_id)
		                  || '<BR>R:'
		                  || get_rep_name (t504a.c703_sales_rep_id)
		                 ) from_dist,
		                 c504a_loaner_transaction_id
		            FROM t504a_loaner_transaction t504a
		           WHERE c504a_loaner_transaction_id IN (
		                    SELECT c504a_transf_ref_id
		                      FROM t504a_loaner_transaction t504a
		                     WHERE c504a_transf_ref_id IS NOT NULL
		                       AND c504a_consigned_to_id =
		                              DECODE (p_from_distid,
		                                      0, c504a_consigned_to_id,
		                                      p_from_distid
		                                     )
		                       AND c504a_void_fl IS NULL)) from_loan
		   WHERE to_loan.c504a_transf_ref_id = from_loan.c504a_loaner_transaction_id
		     AND TRUNC (to_loan.c504a_transf_date) >=
		            DECODE (p_fromdate,
		                    NULL, TO_DATE ('01/01/1900', v_date_format),
		                    TO_DATE (p_fromdate, v_date_format)
		                   )
		     AND TRUNC (to_loan.c504a_transf_date) <=
		            DECODE (p_todate,
		                    NULL, TRUNC (CURRENT_DATE),
		                    TO_DATE (p_todate, v_date_format)
		                   )
		     AND to_loan.c207_set_id =
		                           DECODE (p_setid,
		                                   '0', to_loan.c207_set_id,
		                                   p_setid
		                                  )
		ORDER BY c504_consignment_id, c504a_return_dt DESC;

      
   END gm_fch_loaner_transfer_log;

   /*********************************************************************************************
    * DESCRIPTION : Fecth Loaner Transfer Rep Details
    * Author      : Rajkumar Jayakumar
    *******************************************************************************************/
   PROCEDURE gm_fch_loantransf_rep_details (
      p_conids   IN       VARCHAR2,
      p_data     OUT      TYPES.cursor_type
   )
   AS
   v_company_id    t1900_company.c1900_company_id%TYPE;
   BEGIN
	   --Getting company id from context.   
    SELECT get_compid_frm_cntx()
	  INTO v_company_id 
	  FROM dual;
	  --
      my_context.set_my_inlist_ctx (p_conids);

      OPEN p_data
       FOR
          SELECT t504atxn.c703_sales_rep_id rep_id,
                 get_rep_name (t504atxn.c703_sales_rep_id) to_rep_name,
                  NVL (CASE WHEN t504atxn.c703_sales_rep_id IS NOT NULL 
                        THEN get_cs_ship_email (4121,t504atxn.c703_sales_rep_id)
			            WHEN t504atxn.c901_consigned_to = 50172 
			            THEN get_cs_ship_email (4123,t504atxn.c504a_consigned_to_id)
			            WHEN t504atxn.c901_consigned_to = 50170 
			            THEN get_cs_ship_email (4120, t504atxn.c504a_consigned_to_id)
			             END,get_rule_value_by_company ('GMCSMAILID', 'SHIPDTL', t504atxn.c1900_company_id)) to_rep_email,
                 get_rep_name
                    (gm_pkg_op_loaner_transfer.get_loantxn_repid
                                                 (t504atxn.c504a_transf_ref_id)
                    ) from_rep_name,
                 NVL(get_cs_ship_email(4121,gm_pkg_op_loaner_transfer.get_loantxn_repid(t504atxn.c504a_transf_ref_id)), 
                 	 get_rule_value_by_company ('GMCSMAILID', 'SHIPDTL', t504atxn.c1900_company_id) ) ||',' || DECODE(t504atxn.c703_ass_rep_id,NULL,NULL,get_cs_ship_email (4121,t504atxn.c703_sales_rep_id)) cc_rep_email
                 , t504atxn.c703_ass_rep_id assocrepid
			     , get_rep_name(t504atxn.c703_ass_rep_id) assocrepnm
				 , DECODE(t504atxn.c703_ass_rep_id,NULL, NULL, NVL(get_cs_ship_email (4121, t504atxn.c703_ass_rep_id), get_rule_value_by_company ('GMCSTOMAILID', 'SHIPDTL', t504atxn.c1900_company_id))) assocrepemail   
            FROM t504a_consignment_loaner t504acl,
                 t504a_loaner_transaction t504atxn
           WHERE t504atxn.c504_consignment_id = t504acl.c504_consignment_id
             AND t504atxn.c504a_void_fl IS NULL
             AND t504atxn.c504a_transf_ref_id IS NOT NULL
             AND t504acl.c504_consignment_id IN (SELECT token
                                                   FROM v_in_list)
             AND c504a_transf_email_flg IS NULL
             order by rep_id, assocrepid;
   END gm_fch_loantransf_rep_details;

   /*********************************************************************************************
    * DESCRIPTION : Fecth details to send an email for the loaner transaction
    * Author      : Rajkumar Jayakumar
    *******************************************************************************************/
   PROCEDURE gm_fch_loantrans_email_details (
      p_conids   IN       VARCHAR2,
      p_rep_id   IN       t504a_loaner_transaction.c703_sales_rep_id%TYPE,
      p_assrepid IN		  t504a_loaner_transaction.c703_ass_rep_id%TYPE,
      p_data     OUT      TYPES.cursor_type
   )
   AS
   	v_comp_dt_fmt VARCHAR2(20);
   BEGIN
      my_context.set_my_inlist_ctx (p_conids);

      SELECT get_compdtfmt_frm_cntx() INTO v_comp_dt_fmt FROM DUAL;
      
      OPEN p_data
       FOR
          SELECT t526.c207_set_id setid,
                 get_set_name (t526.c207_set_id) setnm,
                 t504acl.c504a_etch_id etchid,
                 TO_CHAR (t504acl.c504a_expected_return_dt,
                          v_comp_dt_fmt
                         ) due_date,
                     get_rep_name(gm_pkg_op_loaner_transfer.get_loantxn_repid
                        (t504atxn.c504a_transf_ref_id)) fromRepName 
            FROM t526_product_request_detail t526,
                 t504a_consignment_loaner t504acl,
                 t504a_loaner_transaction t504atxn
           WHERE t526.c526_void_fl IS NULL
             AND t526.c526_product_request_detail_id = t504atxn.c526_product_request_detail_id(+)
             AND t504atxn.c504_consignment_id = t504acl.c504_consignment_id(+)
             AND t504atxn.c504a_void_fl IS NULL
             --   AND TRUNC (t504atxn.c504a_transf_date) = TRUNC (CURRENT_DATE)
             AND t504atxn.c504a_transf_ref_id IS NOT NULL
             AND t504acl.c504_consignment_id IN (SELECT token
                                                   FROM v_in_list)
             AND t504atxn.c703_sales_rep_id = NVL(p_rep_id,t504atxn.c703_sales_rep_id)
             AND NVL(t504atxn.c703_ass_rep_id,-999) = NVL(p_assrepid,NVL(t504atxn.c703_ass_rep_id,-999))
             AND c504a_transf_email_flg IS NULL;
   END gm_fch_loantrans_email_details;

   /*********************************************************************************************
    * DESCRIPTION : Fecth REPID based on loaner transaction id
    * Author      : Rajkumar Jayakumar
    *******************************************************************************************/
   FUNCTION get_loantxn_repid (
      p_loantx_id   IN   t504a_loaner_transaction.c504a_loaner_transaction_id%TYPE
   )
      RETURN NUMBER
   AS
      v_rep_id   NUMBER;
   BEGIN
      SELECT t504atxn.c703_sales_rep_id
        INTO v_rep_id
        FROM t504a_loaner_transaction t504atxn
       WHERE c504a_loaner_transaction_id = p_loantx_id
         AND t504atxn.c504a_void_fl IS NULL;

      RETURN v_rep_id;
   END get_loantxn_repid;

   /******************************************************************
   * Description : This is Procedure is called to overwrite the transfer date
   ****************************************************************/
   PROCEDURE gm_op_sav_loaner_transfer_date (
      p_inputstr   IN   VARCHAR2,
      p_userid     IN   t101_user.c101_user_id%TYPE
   )
   AS
      v_string            VARCHAR2 (4000)                       := p_inputstr;
      v_transid           VARCHAR2 (20);
      v_transf_ref_id     VARCHAR2 (20);
      v_transf_ref_date   DATE;
      v_date              VARCHAR2 (10);
      v_consignid         t504_consignment.c504_consignment_id%TYPE;
      v_substring         VARCHAR2 (4000);
      v_substr1           VARCHAR2 (4000);
      v_substr2           VARCHAR2 (4000);
      v_dist_name         t701_distributor.c701_distributor_name%TYPE;
      v_comp_dt_fmt		  VARCHAR2 (20);
   BEGIN
      IF p_inputstr IS NULL
      THEN
         RETURN;
      END IF;

      SELECT get_compdtfmt_frm_cntx() INTO v_comp_dt_fmt FROM DUAL;
      
      WHILE INSTR (v_string, '|') <> 0
      LOOP
         --1.  Fetch the value of loanid and date
         v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
         v_string := SUBSTR (v_string, INSTR (v_string, '|') + 1);
         v_transid := NULL;
         v_date := NULL;
         v_transid := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
         v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
         v_date := v_substring;

         SELECT c504_consignment_id, c504a_transf_ref_id
           INTO v_consignid, v_transf_ref_id
           FROM t504a_loaner_transaction
          WHERE c504a_loaner_transaction_id = v_transid
            AND c504a_void_fl IS NULL;

--get the loaned date and distname  for the reference id
         BEGIN
            SELECT t504al.c504a_loaner_dt,
                   get_distributor_name (t703.c701_distributor_id)
              INTO v_transf_ref_date,
                   v_dist_name
              FROM t504a_loaner_transaction t504al, t703_sales_rep t703
             WHERE t504al.c504a_loaner_transaction_id = v_transf_ref_id
               AND t504al.c504a_void_fl IS NULL
               AND t703.c703_void_fl IS NULL
	       	   AND (t703.c901_ext_country_id is NULL OR t703.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH
               AND t703.c703_sales_rep_id = t504al.c703_sales_rep_id;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               v_dist_name := '';
         END;

         IF (v_transf_ref_date > TO_DATE (v_date, v_comp_dt_fmt))
         THEN
            GM_RAISE_APPLICATION_ERROR('-20999','247',v_dist_name);
         END IF;

         UPDATE t504a_consignment_loaner
            SET c504a_loaner_dt = TO_DATE (v_date, v_comp_dt_fmt),
                c504a_expected_return_dt =gm_pkg_op_loaner.get_loaner_expected_return_dt(TO_DATE (v_date, v_comp_dt_fmt)),
                   --  TO_DATE (v_date, get_rule_value('DATEFMT','DATEFORMAT'))
                 --  + get_rule_value ('DATE', 'EXPRTDT'),
                c504a_last_updated_date = CURRENT_DATE,
                c504a_last_updated_by = p_userid
          WHERE c504_consignment_id = v_consignid AND c504a_void_fl IS NULL;

         UPDATE t504a_loaner_transaction
            SET c504a_transf_date = TO_DATE (v_date, v_comp_dt_fmt),
                c504a_last_updated_date = CURRENT_DATE,
                c504a_last_updated_by = p_userid
          WHERE c504a_loaner_transaction_id = v_transid;
          
        --When ever the child is updated, the consignment table has to be updated ,so ETL can Sync it to GOP.
	 	UPDATE t504_consignment
		   SET c504_last_updated_by    = p_userid
	  		 , c504_last_updated_date = CURRENT_DATE
	  	 WHERE c504_consignment_id = v_consignid;	
      END LOOP;
   END gm_op_sav_loaner_transfer_date;

   /******************************************************************
    * Description : This is Procedure is called to save the email flag after the email is sent
    ****************************************************************/
   PROCEDURE gm_op_sav_send_email_flg (
      p_inputstr   IN   VARCHAR2,
      p_userid     IN   t101_user.c101_user_id%TYPE
   )
   AS
      v_transid     VARCHAR2 (20);
      v_consignid   t504_consignment.c504_consignment_id%TYPE;

      CURSOR cur_token
      IS
         SELECT token
           FROM v_in_list;
   BEGIN
      my_context.set_my_inlist_ctx (p_inputstr);

      FOR currindex IN cur_token
      LOOP
         v_transid := gm_pkg_op_loaner.get_loaner_trans_id (currindex.token);

         UPDATE t504a_loaner_transaction
            SET c504a_transf_email_flg = 'Y',
                c504a_last_updated_date = CURRENT_DATE,
                c504a_last_updated_by = p_userid
          WHERE c504a_loaner_transaction_id = v_transid
            AND c504a_void_fl IS NULL
            AND c504a_transf_ref_id IS NOT NULL
            AND c504a_transf_date IS NOT NULL;
      END LOOP;
   END gm_op_sav_send_email_flg;

     /******************************************************************
   * Description : This is Procedure is called to transfer the open LN trans to the transferre dist
   ****************************************************************/
   PROCEDURE gm_op_sav_transf_loaner_xtn (
      p_loantxnid_from   IN       t504a_loaner_transaction.c504a_loaner_transaction_id%TYPE,
      p_loantxnid_to     IN       t504a_loaner_transaction.c504a_loaner_transaction_id%TYPE,
      p_consignid        IN       t504_consignment.c504_consignment_id%TYPE,
      p_userid           IN       t101_user.c101_user_id%TYPE,
      p_transf_lnids     OUT      VARCHAR2
   )
   AS
      CURSOR curr_ln
      IS
         SELECT c412_inhouse_trans_id transid
           FROM t412_inhouse_transactions
          WHERE c412_ref_id = p_consignid
            AND c412_status_fl < 4
            AND c412_inhouse_purpose = 50128 --Loaner Extension with Replenish
            --    AND c504a_loaner_transaction_id = p_loantxnid_from
            AND c412_void_fl IS NULL;

      v_shipto      t907_shipping_info.c901_ship_to%TYPE;
      v_shiptoid    t907_shipping_info.c907_ship_to_id%TYPE;
      v_addressid   t907_shipping_info.c106_address_id%TYPE;
   BEGIN
      BEGIN
         -- fetch data to update the shipping record
         SELECT c901_ship_to, c907_ship_to_id, c106_address_id
           INTO v_shipto, v_shiptoid, v_addressid
           FROM t907_shipping_info
          WHERE c907_tracking_number = 'LOANERTRANSFER'
            AND c907_status_fl = 40
            AND c907_shipped_dt = TRUNC (CURRENT_DATE)
            AND c907_ref_id = p_consignid
            AND c901_source = 50182
            AND c907_void_fl IS NULL
            AND ROWNUM =1;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            v_shipto := '';
            v_shiptoid := '';
            v_addressid := '';
      END;

      --create the ln string that needs to be returned back
      FOR currindex IN curr_ln
      LOOP
         p_transf_lnids := currindex.transid || ', ' || p_transf_lnids;

         -- update the open Ln trans with new ln trans id
         UPDATE t412_inhouse_transactions
            SET c504a_loaner_transaction_id = p_loantxnid_to,
                c412_last_updated_date = CURRENT_DATE,
                c412_last_updated_by = p_userid
          WHERE c412_inhouse_trans_id = currindex.transid;

         UPDATE t907_shipping_info
            SET c901_ship_to = v_shipto,
                c907_ship_to_id = v_shiptoid,
                c106_address_id = v_addressid,
                c907_last_updated_date = CURRENT_DATE,
                c907_last_updated_by = p_userid
          WHERE c907_ref_id = currindex.transid;
      END LOOP;

      p_transf_lnids := TRIM (p_transf_lnids);
   END gm_op_sav_transf_loaner_xtn;
  /***************************************************************************************
 * Description      : This procedure will rollback the transfer for the given consignment
 * Author           : VPrasath
 *
 ****************************************************************************************/
   PROCEDURE gm_op_rollback_loaner_transfer (
      p_txn_id    IN   VARCHAR2,
       p_userid           IN       t101_user.c101_user_id%TYPE
   )
   AS
		v_count 		NUMBER;
		v_ln_status_fl 	t504a_consignment_loaner.c504a_status_fl%TYPE;
		v_loaner_type	t504_consignment.c504_type%TYPE;
		v_req_det_id	t504a_loaner_transaction.c526_product_request_detail_id%TYPE;
		v_loaner_dt		t504a_loaner_transaction.C504A_LOANER_DT%TYPE;
		v_exp_ret_dt	t504a_loaner_transaction.C504A_EXPECTED_RETURN_DT%TYPE;
		v_sales_rep_id	t504a_loaner_transaction.c703_sales_rep_id%TYPE;
		v_acc_id		t504a_loaner_transaction.c704_account_id%TYPE;
		v_dist_id		t504a_loaner_transaction.c504a_consigned_to_id%TYPE;
		v_req_id		t526_product_request_detail.c525_product_request_id%TYPE;
		v_trans_ref_id  t504a_loaner_transaction.C504A_TRANSF_REF_ID%TYPE;
		v_string	   	VARCHAR2 (2000);
		v_txn_id			VARCHAR2 (2000);
   BEGIN
	   
		v_string:=p_txn_id || ',';

		
		WHILE INSTR (v_string, ',') <> 0
        LOOP
        	v_txn_id := SUBSTR (v_string, 1, INSTR (v_string, ',') - 1);
         	v_string := SUBSTR (v_string, INSTR (v_string, ',') + 1);
	   select COUNT(C504A_TRANSF_REF_ID) 
	   INTO v_count
	   from t504a_loaner_transaction t504a
	   where t504a.c504_consignment_id = v_txn_id
		and c504a_return_dt is NULL
		AND c504a_void_fl IS NULL;

	IF v_count != 1 THEN
		GM_RAISE_APPLICATION_ERROR('-20999','248',v_txn_id);
	END IF;

 	SELECT t504a.c504a_status_fl, c504_type
           INTO v_ln_status_fl, v_loaner_type
           FROM t504_consignment t504, t504a_consignment_loaner t504a
          WHERE t504.c504_status_fl = 4
            AND t504.c504_consignment_id = v_txn_id
            AND t504a.c504_consignment_id = t504.c504_consignment_id
            AND t504a.c504a_void_fl IS NULL
            AND t504.c504_void_fl IS NULL;

         --2.  Confirm if the status is pending return
         IF v_ln_status_fl <> '20'
         THEN
            GM_RAISE_APPLICATION_ERROR('-20999','249','');
         --  Cannot perform loaner transfer as status is not pending return
         END IF;

     -- Change the Request of To Distributor to Open status
         SELECT c526_product_request_detail_id, C504A_TRANSF_REF_ID
			INTO  v_req_det_id, v_trans_ref_id
			FROM t504a_loaner_transaction t504a
			where t504a.c504_consignment_id = v_txn_id
			and c504a_return_dt is NULL
			AND c504a_void_fl IS NULL;
	
		-- reset allocated fl and planned ship date in t504a
		gm_pkg_op_loaner_allocation.gm_loaner_allocation_update(v_req_det_id, 'CLEAR', p_userid); 
		--10: approved, call common procedure to update the status
		gm_pkg_op_loaner_allocation.gm_upd_request_status(v_req_det_id, 10, p_userid);	

-- Void the loaner transaction that is created as part of the transfer

		UPDATE t504a_loaner_transaction
			SET c504a_void_fl = 'Y'
			, c504a_last_updated_by = p_userid
			, c504a_last_updated_date = CURRENT_DATE
			where c504_consignment_id = v_txn_id
			and c504a_return_dt is NULL
			AND c504a_void_fl IS NULL;


-- Revert back the loaner transaction that is updated as part of the transfer

		UPDATE t504a_loaner_transaction
			SET C504A_RETURN_DT = ''
			, C504A_PROCESSED_DATE = ''
			, c504a_last_updated_by = p_userid
			, c504a_last_updated_date = CURRENT_DATE
			where C504A_LOANER_TRANSACTION_ID = v_trans_ref_id;


-- Update the parent table with corresponding dates

		SELECT C504A_LOANER_DT,	C504A_EXPECTED_RETURN_DT, c703_sales_rep_id, c704_account_id, c504a_consigned_to_id
			INTO v_loaner_dt, v_exp_ret_dt, v_sales_rep_id, v_acc_id, v_dist_id
			FROM t504a_loaner_transaction t504a
			where t504a.c504_consignment_id = v_txn_id
			and c504a_return_dt is NULL
			AND c504a_void_fl IS NULL;

		UPDATE t504a_consignment_loaner 
			SET c504a_loaner_dt = v_loaner_dt
			, C504A_EXPECTED_RETURN_DT = v_exp_ret_dt
			, c504a_last_updated_by = p_userid
			, c504a_last_updated_date = CURRENT_DATE
			WHERE C504_CONSIGNMENT_ID = v_txn_id AND c504a_void_fl IS NULL;
	
		UPDATE t504_consignment t504
			SET c701_distributor_id = v_dist_id
			, c704_account_id = v_acc_id
			, c504_last_updated_by = p_userid
			, c504_last_updated_date = CURRENT_DATE
			where t504.c504_consignment_id = v_txn_id AND c504_void_fl IS NULL;
	
	 -- update tag  
	      SELECT COUNT (1)
	        INTO v_count
	        FROM t5010_tag
	       WHERE c5010_last_updated_trans_id = v_txn_id AND c5010_void_fl IS NULL;

      IF v_count > 0
      THEN
         GM_PKG_CM_SHIPPING_TRANS.gm_update_location (v_dist_id,
                             4120,
                            v_txn_id,
                             51002,
                             p_userid,
                             v_acc_id,
                             v_sales_rep_id
                            );
      END IF;

-- Update the Shipping Info

		SELECT c525_product_request_id INTO v_req_id
			from t526_product_request_detail t526 
			where t526.c526_product_request_detail_id = v_req_det_id;

		UPDATE t907_shipping_info t907 
			SET C907_REF_ID = v_req_det_id
			, C901_DELIVERY_MODE = 5004
			, C901_DELIVERY_CARRIER = 5001
			, C907_TRACKING_NUMBER = NULL
			, c907_last_updated_by = p_userid
			, c907_last_updated_date = CURRENT_DATE
			, c907_status_fl = 15
			, C907_SHIPPED_DT = NULL
			, C907_EMAIL_UPDATE_FL = NULL
			, C907_EMAIL_UPDATE_DT = NULL
			where t907.C907_REF_ID= v_txn_id AND C525_PRODUCT_REQUEST_ID = v_req_id;
END LOOP;
   END gm_op_rollback_loaner_transfer;
/************************************************************
 * Description : to get the loaner request for last 10 days
 *       Author: Dhana Reddy
 ***********************************************************/
   PROCEDURE gm_fch_loaner_requests (
        p_req_forid   IN t525_product_request.c525_request_for_id%TYPE,
        p_loaner_type IN t525_product_request.c901_request_for%TYPE,
        p_out_requests OUT TYPES.cursor_type)
    AS
    v_company_id  t1900_company.c1900_company_id%TYPE;
    BEGIN
	    
	    select get_compid_frm_cntx() into v_company_id from dual;
	    
        OPEN p_out_requests FOR 
        SELECT c525_product_request_id codeid, DECODE (c525_surgery_date,NULL,
            c525_product_request_id, c525_product_request_id ||'-' || c525_surgery_date) codenm 
         FROM t525_product_request
        WHERE c901_request_for = p_loaner_type 
          AND c525_request_for_id = p_req_forid 
          AND c525_void_fl IS NULL 
          AND c525_created_date  > TRUNC (CURRENT_DATE) - 10
          AND c1900_company_id = v_company_id
          ORDER BY c525_surgery_date DESC;
    END gm_fch_loaner_requests;
    
 /*************************************
  * Description: procedure to create request for set transfer 
  * Author     : Dhana Reddy
  ******************************************/
	PROCEDURE gm_op_sav_set_transfer (
	        p_req_det_id    IN t526_product_request_detail.c525_product_request_id%tYPE,
	        p_loaner_Type   IN t525_product_request.c901_request_for%TYPE,
	        p_transfer_date IN VARCHAR2,
	        p_set_id        IN t526_product_request_detail.c207_set_id%TYPE,
	        p_dist_id       IN t525_product_request.c525_request_for_id%TYPE,
	        p_userid        IN t101_user.c101_user_id%TYPE,
	        p_rep_id        IN t703_sales_rep.c703_sales_rep_id%TYPE,
            p_acc_id        IN t704_account.c704_account_id%TYPE,
	        p_req_id        IN OUT t525_product_request.c525_product_request_id%TYPE,
	        p_assoc_rep_id  IN t525_product_request.c703_ass_rep_id%TYPE,
	        p_req_dtl_id    OUT t526_product_request_detail.c525_product_request_id%tYPE)
	AS
	    v_pdt_id_num t525_product_request.c525_product_request_id%TYPE;
	    v_shipid NUMBER;
	    v_req_det_id t526_product_request_detail.c525_product_request_id%TYPE;
	    v_request_by_type t525_product_request.c901_request_by_type%TYPE;
	    v_ship_to_id    t525_product_request.c901_ship_to%TYPE;
	    v_event_set_id  t7104_case_set_information.c7100_case_info_id%TYPE;
	    v_event_info_id t7104_case_set_information.c7104_case_set_id%TYPE;
	    v_req_detail_id VARCHAR2(30);
	    v_comp_dt_fmt  VARCHAR2(20);
	    v_company_id  t1900_company.c1900_company_id%TYPE;
		v_plant_id T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
		v_dist_company_id t1900_company.c1900_company_id%TYPE;
		v_loaner_filter   VARCHAR2(20);
	BEGIN
		
		SELECT get_compdtfmt_frm_cntx() INTO v_comp_dt_fmt FROM DUAL;
		
		select get_compid_frm_cntx(),get_plantid_frm_cntx() 
		into v_company_id,v_plant_id 
		from dual;
				
	    IF p_loaner_Type       = '4119' THEN -- In-House Loaner
	        v_request_by_type := '50625';    -- 50625 Employee
	        v_ship_to_id      := '4123';     --4123 Employee
	    ELSE
	        v_request_by_type := '50626';--50626 Sales Rep
	        v_ship_to_id      := '4120'; --4120 Distributor
	    END IF;

	    IF (p_req_det_id = '1006703' OR p_req_det_id = '1006704') AND p_req_id IS NULL THEN --1006704 Create For Wrong
	        -- Shipment  --1006703 Create
	        -- For Surgery
	         SELECT s525_product_request.NEXTVAL
	           INTO v_pdt_id_num
	           FROM DUAL;
	           
	         SELECT get_transaction_company_id (v_company_id,p_dist_id,'LOANERTRANSFERFILTER','DIST') INTO v_dist_company_id FROM DUAL;
	       
	         INSERT
	           INTO t525_product_request
	            (
	                c525_product_request_id, c525_requested_date, c901_request_for,
	                c525_request_for_id, c901_request_by_type, c525_request_by_id,
	                c901_ship_to, c525_ship_to_id, c525_created_by,
	                c525_created_date, c525_surgery_date, c704_account_id,
	                c703_sales_rep_id, c525_status_fl, c703_ass_rep_id, c1900_company_id , c5040_plant_id
	            )
	            VALUES
	            (
	                v_pdt_id_num, TO_DATE (p_transfer_date, v_comp_dt_fmt), p_loaner_Type,
	                p_dist_id, v_request_by_type, p_userid,
	                v_ship_to_id, p_dist_id, p_userid,
	                CURRENT_DATE, TO_DATE (p_transfer_date, v_comp_dt_fmt), p_acc_id,
	                p_rep_id, 10, p_assoc_rep_id,v_dist_company_id,v_plant_id
	            ) ;
	            
	        p_req_id := v_pdt_id_num;
	        
	    END IF;
	    
	    IF v_pdt_id_num  IS NULL AND p_req_det_id <> '1006703' AND p_req_det_id <> '1006704' THEN
	        v_pdt_id_num := p_req_det_id;
	    END IF;
	    IF (p_req_det_id = '1006703' OR p_req_det_id = '1006704') AND p_req_id IS NOT NULL THEN
	        v_pdt_id_num := p_req_id;
	    END IF;
	    
	    -- to create product req detail id
	    gm_pkg_op_product_requests.gm_sav_product_requests_detail (v_pdt_id_num, p_loaner_Type, p_transfer_date, p_set_id,
	    NULL, 1, 10) ;
	    
	     SELECT MAX (c526_product_request_detail_id)
	       INTO v_req_det_id
	       FROM t526_product_request_detail
	      WHERE c525_product_request_id = v_pdt_id_num
	        AND c526_void_fl           IS NULL;
	  
	  /* The following code is added for PMT-6770, During Loaner Transfer for selecting the Request ID from Request Details section, 
	   * system will creating the Duplicate records in t907_shipping_infor table as because of by passing Request ID instead of Request Detail id for this type of Loaner Transfer.
	   * so for this type, we need to pass Request Details id, due to this there is no duplicate records i=on Loaners(Process Request) screen for any set/Request.
	   * */
	  IF p_req_det_id <> '1006703' AND  p_req_det_id <> '1006704' THEN 
	  		v_req_detail_id := v_req_det_id;
	  ELSE
	  		v_req_detail_id := v_pdt_id_num;
	  END IF;
	  -- to create shipping record for  product req detail id
	  --Below procedure is changed to add the shipping details while doing loaner transfer, 50185:Product Loaner
	    gm_pkg_cm_shipping_trans.gm_sav_shipping(v_req_detail_id,50185,v_ship_to_id,p_dist_id,null,null,null,0,null,p_userid,v_shipid,null,null);
	      
	     IF v_pdt_id_num IS NOT NULL AND p_loaner_Type = '4119' -- inhouse loaner
	      THEN 
	       -- find event info id for inhouse loaner req
	         SELECT t7104.c7100_case_info_id
			   INTO v_event_info_id
			   FROM T7104_Case_Set_Information t7104, t526_product_request_detail t526
			  WHERE t7104.c526_product_request_detail_id = t526.c526_product_request_detail_id
			    AND t526.c525_product_request_id         = v_pdt_id_num
			    AND t7104.c7104_void_fl                 IS NULL
			    AND t526.c526_void_fl                   IS NULL
			    AND rownum                               = 1;
			    
	     -- to create event set details
	     	gm_pkg_sm_casebook_txn.gm_save_case_sets (v_event_info_id, p_set_id, NULL, NULL, '19531', TO_DATE (p_transfer_date,
										v_comp_dt_fmt), p_userid) ; -- 19531 In-house Loaner Set(s)
	    	
	       SELECT MAX (c7104_case_set_id)
			   INTO v_event_set_id
			   FROM t7104_case_set_information
			  WHERE c7100_case_info_id = v_event_info_id
			    AND c7104_void_fl     IS NULL;
			    
			 -- updating product req dtl id to event set
			  UPDATE t7104_case_set_information
				 SET c526_product_request_detail_id = v_req_det_id, c7104_last_updated_date = CURRENT_DATE, c7104_last_updated_by = p_userid
			   WHERE c7104_case_set_id          = v_event_set_id
				 AND c7100_case_info_id         = v_event_info_id
				 AND c7104_void_fl             IS NULL;
	      END IF;
	      
	    p_req_dtl_id := v_req_det_id; 
	      
	END gm_op_sav_set_transfer;
	
/****************************************************************
 * Description : this function is used to get loaner expected return date 
 * Author      : Dhana Reddy
 *****************************************************************/
	FUNCTION get_expected_return_dt (
	        p_date DATE,
	        p_rule_id t906_rules.c906_rule_id%TYPE)
	    RETURN DATE
	IS
	    v_return_dt DATE;
	    v_rule_days NUMBER;
	    v_day_number NUMBER;
	    v_company_id NUMBER;
	BEGIN
	    
	    SELECT   get_compid_frm_cntx()
		  INTO   v_company_id
		  FROM   DUAL;
			
		 SELECT TO_NUMBER (get_rule_value (p_rule_id, 'EXPRTDT'))
		   INTO v_rule_days
		   FROM dual;
		BEGIN
		   SELECT WEEK_DAY_NUMBER
		     INTO v_day_number
		     FROM date_dim
		    WHERE DAY NOT IN ('Sat','Sun')
		      AND TRUNC (cal_date) = TO_DATE (p_date)
		      AND date_key_id NOT IN (
				              	SELECT date_key_id 
				              	  FROM COMPANY_HOLIDAY_DIM 
				              	 WHERE HOLIDAY_DATE =  TO_DATE (p_date)
				              	   AND HOLIDAY_FL = 'Y'
				              	    AND C1900_COMPANY_ID = v_company_id
								)	       ;
		EXCEPTION
		WHEN NO_DATA_FOUND THEN
	       v_day_number := 0;
	    END;
	    
	    IF v_day_number NOT IN (0,1,7) 
		THEN
	    v_rule_days:= v_rule_days - 1;
		END IF;
	
	    BEGIN
		 SELECT caldate
		   INTO v_return_dt
		   FROM
		    (SELECT caldate, ROWNUM rnum
		       FROM
		        (SELECT cal_date caldate
		           FROM date_dim
		          WHERE DAY NOT IN ('Sat','Sun')
		            AND TRUNC (cal_date) >= TRUNC (p_date)
		            AND TRUNC (cal_date) <= TRUNC (CURRENT_DATE) + 30
		            AND date_key_id NOT IN (
				              	SELECT date_key_id 
				              	  FROM COMPANY_HOLIDAY_DIM 
				              	 WHERE HOLIDAY_DATE   >= TRUNC (p_date)
				              	   AND HOLIDAY_DATE   <= TRUNC (sysdate) + 30
				              	   AND HOLIDAY_FL = 'Y'
				              	    AND C1900_COMPANY_ID = v_company_id
								)
		       ORDER BY TRUNC (cal_date) ASC
		        )
		    )
		  WHERE rnum = v_rule_days +1;
	    EXCEPTION
	    WHEN NO_DATA_FOUND THEN
	        v_return_dt := NULL;
	    END;
	    RETURN v_return_dt;
	END get_expected_return_dt;
	
/**********************************************************************
 * Author      : Dhana Reddy
 * Description : to get the pending return inhouse loaner sets
 ************************************************************************/
	PROCEDURE gm_fch_inhouse_loaner_status (
	        p_consign_to IN t504a_loaner_transaction.c504a_consigned_to_id%TYPE,
	        p_type       IN t504_consignment.c504_type%TYPE,
	        p_status     IN t504a_consignment_loaner.c504a_status_fl%TYPE,
	        p_inhouse_sets OUT TYPES.cursor_type)
	AS
	v_plant_id T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
	v_date_fmt VARCHAR2(100);
	v_company_id  t1900_company.c1900_company_id%TYPE;
	BEGIN
		
		select get_plantid_frm_cntx(),get_compdtfmt_frm_cntx(),get_compid_frm_cntx into v_plant_id,v_date_fmt,v_company_id from dual;
		
	    OPEN p_inhouse_sets FOR 
	     SELECT t504.c504_consignment_id CONID, t504.c207_set_id SETID, get_set_name (
	        t504.c207_set_id) SNAME, t504.C504_HOLD_FL holdfl, t504.c504_status_fl SFL, get_code_name (
	        t504.c504_inhouse_purpose) PNAME, t504.c504_inhouse_purpose PID, t504a.c504a_etch_id ETCHID, t504a.c504a_status_fl
	        LSFL, gm_pkg_op_loaner.get_loaner_status (T504A.C504A_STATUS_FL) LOANSFL, t504a.c504a_expected_return_dt EDATE
	        , t504a.c504a_loaner_dt LDATE, get_cs_rep_loaner_bill_nm_add (
	        t504.c504_consignment_id) LOANNM, DECODE (t504a.c504a_status_fl, 20, TRUNC (CURRENT_DATE) - t504a.c504a_loaner_dt)
	        ELPDAYS, get_work_days_count (TO_CHAR (TRUNC (t504a.c504a_expected_return_dt), v_date_fmt), TO_CHAR (TRUNC (
	        CURRENT_DATE), v_date_fmt)) OVERDUE, GET_LOG_FLAG (t504a.c504_consignment_id, 1222) CMTCNT FROM t504_consignment t504,
	        t504a_consignment_loaner t504a, t504a_loaner_transaction t504txn 
	      WHERE t504.c504_status_fl = '4' 
	        AND t504.c504_verify_fl  = '1' 
	        AND t504.C504_VOID_FL IS NULL 
	        AND t504.c207_set_id IS NOT NULL 
	        AND t504.c504_type  = p_type 
	        AND t504a.c504a_status_fl = p_status 
	        AND t504txn.c504a_consigned_to_id  =  decode(t504txn.c901_consigned_to,50170,GET_DISTRIBUTOR_ID(p_consign_to),p_consign_to) 
	        AND NVL(t504txn.c703_sales_rep_id,-9999) = DECODE(t504txn.c901_consigned_to,50170,p_consign_to,NVL(t504txn.c703_sales_rep_id,-9999))
	        AND t504.c504_consignment_id  = t504a.c504_consignment_id 
	        AND t504txn.c504_consignment_id =t504a.c504_consignment_id 
	        AND t504txn.c504a_return_dt IS NULL 
	        AND t504txn.c504a_void_fl IS NULL
	        AND t504txn.c1900_company_id = v_company_id
	        AND t504txn.c1900_company_id = t504.c1900_company_id
	   ORDER BY setid,t504.c504_created_date, t504.c504_inhouse_purpose DESC ;
	END gm_fch_inhouse_loaner_status;

END gm_pkg_op_loaner_transfer;
/
