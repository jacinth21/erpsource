-- @"C:\Database\Packages\Operations\gm_pkg_op_fetch_recon.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_fetch_recon 
IS
/*
 * Description: This procedure will fetch the Missing Loaner Transaction that was not scanned
 * Author: 		Rajkumar
 */
PROCEDURE gm_fch_not_scanned_ln(
p_out	OUT		TYPES.cursor_type
)
AS
v_plant_id   t5040_plant_master.c5040_plant_id%TYPE;
BEGIN
	SELECT  get_plantid_frm_cntx()
		INTO  v_plant_id
	FROM DUAL;
	
	OPEN p_out FOR 
		  SELECT T412.C412_INHOUSE_TRANS_ID TRANSID
           FROM T412_INHOUSE_TRANSACTIONS T412
          WHERE T412.C412_STATUS_FL            <= 3
            AND T412.C412_TYPE                  = '50159'	-- Missing
            AND T412.C412_INHOUSE_TRANS_ID NOT IN
            (
                 SELECT C412_INHOUSE_TRANS_ID
                   FROM T412A_INHOUSE_TRANS_ATTRIBUTE
                  WHERE C901_ATTRIBUTE_TYPE   = '50822'		-- Reconciled
                    AND C412A_ATTRIBUTE_VALUE = 'Y'
                    AND C412A_VOID_FL        IS NULL
            )
            AND T412.C412_VOID_FL IS NULL
            AND T412.C5040_PLANT_ID = v_plant_id
        ORDER BY T412.C412_CREATED_DATE DESC;
END gm_fch_not_scanned_ln;

/*
 * Description:	This procedure will fetch the Missing Loaner Transaction Items for the particular Missing Loaner Transactions
 * Author	  :	Rajkumar
 */
PROCEDURE gm_fch_ln_item(
	p_txnid	IN	T412_INHOUSE_TRANSACTIONS.C412_INHOUSE_TRANS_ID%TYPE,
	p_out	OUT	TYPES.cursor_type
)
AS
BEGIN
	
	OPEN p_out FOR
		SELECT C205_PART_NUMBER_ID PNUM, T504A.C703_SALES_REP_ID REPID, T504A.C704_ACCOUNT_ID ACCID
	      , (GM_PKG_OP_FETCH_RECON.GET_MISSING_QTY (C205_PART_NUMBER_ID, T412.C412_INHOUSE_TRANS_ID) -
	        GM_PKG_OP_FETCH_RECON.GET_RECONCILED_QTY (C205_PART_NUMBER_ID, T412.C412_INHOUSE_TRANS_ID)) ITEMQTY,
	        T413.C413_ITEM_PRICE ITEMPRICE, T413.C413_CONTROL_NUMBER CNUM, T504A.C504A_LOANER_DT LOANDT,'' LOAN_EXT_DT
	       FROM T413_INHOUSE_TRANS_ITEMS T413, T412_INHOUSE_TRANSACTIONS T412, T504A_LOANER_TRANSACTION T504A
	      WHERE T504A.C504A_LOANER_TRANSACTION_ID = T412.C504A_LOANER_TRANSACTION_ID
	        AND T412.C412_INHOUSE_TRANS_ID        = T413.C412_INHOUSE_TRANS_ID
	        AND T413.C412_INHOUSE_TRANS_ID        = p_txnid
	        AND T413.C413_STATUS_FL				  = 'Q'
	        --AND T412.C1900_COMPANY_ID			  = v_company_id
	        --AND T504A.C1900_COMPANY_ID			  = v_company_id
	        AND T504A.C504A_VOID_FL              IS NULL
	        AND T412.C412_VOID_FL                IS NULL
	        AND T413.C413_VOID_FL                IS NULL;
END gm_fch_ln_item;
/*
 * Description: This Procedure is used to load to matched LN item from the Order with the combination of Part#, Rep and Acc
 * Author:		Rajkumar 
 */
PROCEDURE gm_fch_notrec_ln_items(
	p_pnum			IN	T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE,
	p_acc			IN	T704_ACCOUNT.C704_ACCOUNT_ID%TYPE,
	p_repid			IN	t703_sales_rep.C703_SALES_REP_ID%TYPE,
	p_out			OUT	TYPES.cursor_type,
	p_order_date	IN	T501_ORDER.C501_ORDER_DATE%TYPE
)
AS
v_plant_id   t5040_plant_master.c5040_plant_id%TYPE;
v_days         t906_rules.c906_rule_value%TYPE;
BEGIN
	
	SELECT  get_plantid_frm_cntx()
		INTO  v_plant_id
	FROM DUAL;

	SELECT get_rule_value ('LOANERRECONDAY', 'LOANER_RECON')
		INTO v_days
    FROM DUAL;
	
	OPEN p_out FOR
		SELECT T412.C412_INHOUSE_TRANS_ID TRANSID, C205_PART_NUMBER_ID PNUM		 
			, (GM_PKG_OP_FETCH_RECON.GET_MISSING_QTY (C205_PART_NUMBER_ID, T412.C412_INHOUSE_TRANS_ID) -
		    GM_PKG_OP_FETCH_RECON.GET_RECONCILED_QTY (C205_PART_NUMBER_ID, T412.C412_INHOUSE_TRANS_ID)) ITEMQTY
		    ,T413.C413_ITEM_PRICE ITEMPRICE, T413.C413_CONTROL_NUMBER CNUM
		   FROM T413_INHOUSE_TRANS_ITEMS T413, T412_INHOUSE_TRANSACTIONS T412, T504A_LOANER_TRANSACTION T504A
		  WHERE T504A.C703_SALES_REP_ID =  p_repid
		    AND T504A.C704_ACCOUNT_ID = p_acc
		    AND T504A.C504A_LOANER_TRANSACTION_ID = T412.C504A_LOANER_TRANSACTION_ID
		    AND T412.C412_INHOUSE_TRANS_ID        = T413.C412_INHOUSE_TRANS_ID
		    AND T412.C412_TYPE = '50159'		-- Missing    
		    AND T413.C412_INHOUSE_TRANS_ID NOT IN
		    (
		         SELECT T412A.C412_INHOUSE_TRANS_ID
		           FROM T412A_INHOUSE_TRANS_ATTRIBUTE T412A
		          WHERE T412A.C901_ATTRIBUTE_TYPE   = '50822' -- Reconciled
		            AND T412A.C412A_ATTRIBUTE_VALUE = 'Y'
		            AND T412A.C412A_VOID_FL        IS NULL
		    )
		    AND T413.C205_PART_NUMBER_ID = p_pnum
		    AND T413.C413_STATUS_FL = 'Q'
		    AND T412.c5040_plant_id = v_plant_id
		    --AND TRUNC(T504A.C504A_LOANER_DT) - p_order_date <=  v_days
		    AND p_order_date - TRUNC(T504A.C504A_LOANER_DT) >= 0
		    AND p_order_date - TRUNC(T504A.C504A_LOANER_DT) <=  v_days
   		    --AND T504A.C1900_COMPANY_ID = v_company_id
		    AND T504A.C504A_VOID_FL IS NULL
		    AND T412.C412_VOID_FL   IS NULL
		    AND T413.C413_VOID_FL   IS NULL; 
END gm_fch_notrec_ln_items;
/*
 * Description: This Procedure will Fetch the not Reconciled DO Items that compare the Part#, Sales Rep, Accid from LN 
 * Author:		Rajkumar
 */
PROCEDURE gm_fch_notrec_do_items(
	p_pnum		IN	T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE,
	p_acc		IN	T704_ACCOUNT.C704_ACCOUNT_ID%TYPE,
	p_repid		IN	t703_sales_rep.C703_SALES_REP_ID%TYPE,
	p_out		OUT	TYPES.cursor_type,
	p_loan_dt   IN T504A_LOANER_TRANSACTION.C504A_LOANER_DT%TYPE DEFAULT NULL,
	p_ext_date 	IN T504A_LOANER_TRANSACTION.C504A_EXPECTED_RETURN_DT%TYPE  DEFAULT NULL,
	p_type 		IN VARCHAR2 DEFAULT 'LOAN_DT'
)
AS
v_plant_id   t5040_plant_master.c5040_plant_id%TYPE;
v_days         t906_rules.c906_rule_value%TYPE;
v_frm_days     t906_rules.c906_rule_value%TYPE;

BEGIN

	SELECT  get_plantid_frm_cntx()
	  INTO  v_plant_id
	FROM DUAL;
	--Getting the number of days to be added with loaner date
	       
	          SELECT get_rule_value ('LOANERRECONDAY', 'LOANER_RECON')
                INTO v_days
                FROM DUAL;
     --Getting the number of days to be added with ext date for order date range         
              SELECT get_rule_value ('LOANER_RECON_FRM_DAY', 'LOANER_RECON')
                INTO v_frm_days
                FROM DUAL;

	       
	OPEN p_out FOR
	  SELECT ORDID,PNUM,QTY FROM 
		(SELECT ORDDET.ORDID ORDID, ORDDET.PNUM PNUM, SUM(ORDDET.QTY) QTY,ORDDET.ORDDATE ORDERDT
		   FROM
		    (
		         SELECT T502A.C501_ORDER_ID ORDID, T502A.C502A_ATTRIBUTE_VALUE ATTVAL
		           FROM T502A_ITEM_ORDER_ATTRIBUTE T502A
		          WHERE T502A.C205_PART_NUMBER_ID = p_pnum
		            AND T502A.C901_ATTRIBUTE_TYPE = '50822' -- Reconciled
		            AND T502A.C502A_VOID_FL      IS NULL
		    )
		    ORDATT, (
		         SELECT T501.C501_ORDER_ID ORDID, T502.C205_PART_NUMBER_ID PNUM, T502.C502_ITEM_QTY QTY
		          , T501.C703_SALES_REP_ID REPID, T501.C704_ACCOUNT_ID ACCID, T501.C501_ORDER_DATE ORDDATE
		           FROM T501_ORDER T501, T502_ITEM_ORDER T502
		          WHERE T502.C501_ORDER_ID       = T501.C501_ORDER_ID
		            AND T501.C501_VOID_FL       IS NULL
		            AND T502.C502_VOID_FL       IS NULL
		            --AND T501.C501_STATUS_FL      >= 2  --code change for PMT-43181
		            AND T501.C703_SALES_REP_ID   = p_repid
		            AND T501.C704_ACCOUNT_ID     = p_acc
		            AND T502.C205_PART_NUMBER_ID = p_pnum
		            AND T502.C901_TYPE           = '50301' -- Loaner Item
					AND T501.C501_DELETE_FL      IS NULL
					AND T501.c5040_plant_id	 = v_plant_id
					AND (T501.C901_EXT_COUNTRY_ID IS NULL OR t501.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH
					--AND T501.C501_SHIPPING_DATE  IS NOT NULL
		    )
		    ORDDET
		  WHERE ORDDET.ORDID             = ORDATT.ORDID (+)
		    AND NVL (ORDATT.ATTVAL, 'N') = 'N'
		    --AND TRUNC(p_loan_dt) - ORDDET.ORDDATE  <= v_days
		    AND ((ORDDET.ORDDATE - TRUNC(p_loan_dt) >= 0
		    AND ORDDET.ORDDATE - TRUNC(p_loan_dt) <= v_days)
		    OR (ORDDET.ORDDATE BETWEEN TRUNC(p_ext_date)-v_frm_days AND TRUNC(p_ext_date) + v_days))--order date should in the this date range for extension loan date 		  
		    GROUP BY ORDDET.ORDID, ORDDET.PNUM,ORDDET.ORDDATE )
		    WHERE QTY > 0 
			ORDER BY ORDID;		  
END gm_fch_notrec_do_items;

/*
 * Description:	THis Procedure will fetch all the DO items that is not Reconciled
 * Author:		Rajkumar
 */
PROCEDURE gm_fch_not_rec_doitems(
	p_out	OUT	TYPES.cursor_type
)
AS
v_plant_id    t5040_plant_master.c5040_plant_id%TYPE;
BEGIN
	
	--Getting company id from context.   
    SELECT get_plantid_frm_cntx()
	  INTO v_plant_id 
	  FROM dual;
	  
	OPEN p_out FOR
	SELECT ORDID,PNUM,QTY,REPID,ACCID,ORDERDATE FROM
		 (SELECT ORDDET.ORDID ORDID, ORDDET.PNUM PNUM, SUM(ORDDET.QTY) QTY, ORDDET.REPID REPID, ORDDET.ACCID ACCID, ORDDET.ORDERDT ORDERDATE
		   FROM
		    (
		         SELECT T502A.C501_ORDER_ID ORDID, T502A.C502A_ATTRIBUTE_VALUE ATTVAL, t502a.c205_part_number_id pnum
		           FROM T502A_ITEM_ORDER_ATTRIBUTE T502A
		          WHERE T502A.C901_ATTRIBUTE_TYPE = '50822' -- Reconciled
		            AND T502A.C502A_VOID_FL      IS NULL
		    )
		    ORDATT, (
		         SELECT T501.C501_ORDER_ID ORDID, T502.C205_PART_NUMBER_ID PNUM, T502.C502_ITEM_QTY QTY
		          , T501.C703_SALES_REP_ID REPID, T501.C704_ACCOUNT_ID ACCID, t501.c501_order_date orderdt
		           FROM T501_ORDER T501, T502_ITEM_ORDER T502
		          WHERE T502.C501_ORDER_ID  = T501.C501_ORDER_ID
		            AND T501.C501_VOID_FL  IS NULL
		            AND T502.C502_VOID_FL  IS NULL
 					--AND T501.C501_STATUS_FL >= 2  --code change for PMT-43181
		            AND T502.C901_TYPE      = '50301'  -- Loaner Item
                    AND t501.C501_DELETE_FL      IS NULL
                    AND T501.c5040_plant_id = v_plant_id
                    AND (T501.C901_EXT_COUNTRY_ID IS NULL OR t501.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH
                   --AND T501.C501_SHIPPING_DATE  IS NOT NULL
		    )
		    ORDDET
		  WHERE ORDDET.ORDID             = ORDATT.ORDID (+)
		    AND ORDDET.PNUM              = ORDATT.PNUM (+)
		    AND NVL (ORDATT.ATTVAL, 'N') = 'N'
		    GROUP BY ORDDET.ORDID, ORDDET.PNUM,ORDDET.REPID,ORDDET.ACCID, ORDDET.ORDERDT )
		    WHERE QTY > 0 
			ORDER BY ORDID;
END gm_fch_not_rec_doitems;

/*
 * Description: This function will get the Remaining missing Loaner Qty
 * Author:		Rajkumar
 */
FUNCTION get_missing_qty(
	p_pnum	T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE,
	p_txnid	T412_INHOUSE_TRANSACTIONS.C412_INHOUSE_TRANS_ID%TYPE
) RETURN NUMBER
IS
	v_miss_qty	NUMBER;
BEGIN
	BEGIN
		SELECT NVL(SUM(T413.C413_ITEM_QTY),0) INTO v_miss_qty 
			FROM T413_INHOUSE_TRANS_ITEMS T413
        	WHERE
            	T413.C413_STATUS_FL = 'Q'
            AND T413.C205_PART_NUMBER_ID = p_pnum
            AND T413.C412_INHOUSE_TRANS_ID = p_txnid
            AND T413.C413_VOID_FL IS NULL;
          EXCEPTION WHEN NO_DATA_FOUND 
          THEN
          	v_miss_qty := 0;
          END;
          RETURN v_miss_qty;
END get_missing_qty;

FUNCTION get_item_qty(
	 p_transid t413_inhouse_trans_items.c412_inhouse_trans_id%TYPE,
     p_pnum  t413_inhouse_trans_items.c205_part_number_id%TYPE
) RETURN NUMBER
IS
v_qty NUMBER;
BEGIN
--
  IF p_pnum IS NOT NULL THEN
	  SELECT SUM(NVL(T413.C413_ITEM_QTY,0)) INTO v_qty
	  FROM T413_INHOUSE_TRANS_ITEMS T413, T412_INHOUSE_TRANSACTIONS T412
	  WHERE T413.C412_INHOUSE_TRANS_ID = p_transid
	  AND  t413.C412_INHOUSE_TRANS_ID = T412.C412_INHOUSE_TRANS_ID
	  AND T413.C413_STATUS_FL = 'Q'
	  AND T413.C205_PART_NUMBER_ID = p_pnum
	  AND T412.C412_VOID_FL IS NULL
	  AND T413.C413_VOID_FL IS NULL;
  END IF;
--
 RETURN v_qty;
 END get_item_qty;
/*
 * Description: This function will get the Reconciled Loaner Qty
 * Author:		Rajkumar
 */
FUNCTION get_reconciled_qty(
	p_pnum	T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE,
	p_txnid	T412_INHOUSE_TRANSACTIONS.C412_INHOUSE_TRANS_ID%TYPE
) RETURN NUMBER
IS
	v_recon_qty	NUMBER;
BEGIN
	BEGIN
		SELECT NVL(SUM(T413.C413_ITEM_QTY),0) INTO v_recon_qty FROM  T413_INHOUSE_TRANS_ITEMS T413
        	WHERE T413.C413_STATUS_FL IN ('W','S','R','C')
            AND T413.C205_PART_NUMBER_ID = p_pnum
            AND T413.C412_INHOUSE_TRANS_ID = p_txnid
          --  AND T412.C412_TYPE = '50159'
          --  AND T412.C412_STATUS_FL = 4
            AND T413.C413_VOID_FL IS NULL;
	EXCEPTION WHEN NO_DATA_FOUND
	THEN
		v_recon_qty := 0;
	END;
	RETURN v_recon_qty;
END get_reconciled_qty;

/*
 * Description: This Function to fetch the Order Qty
 * Author:		Rajkumar
 */
FUNCTION get_order_qty(
	p_ordid		T501_ORDER.C501_ORDER_ID%TYPE,
	p_pnum	T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE	
)RETURN NUMBER
IS
v_qty	NUMBER := 0;
BEGIN
	BEGIN
	SELECT SUM(T502.C502_ITEM_QTY) INTO v_qty
       FROM T502_ITEM_ORDER T502
      WHERE T502.C501_ORDER_ID       = p_ordid
        AND T502.C205_PART_NUMBER_ID = p_pnum
        AND T502.C502_VOID_FL       IS NULL;
     EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
      	v_qty := 0;
      END;
      v_qty := v_qty - gm_pkg_op_fetch_recon.get_swapped_qty(p_ordid,p_pnum) - gm_pkg_op_fetch_recon.gm_chk_returns(p_ordid,p_pnum);
      RETURN v_qty;
END get_order_qty;
/*
 * Description: This Function to fetch the Reconciled Order Qty
 * Author:		Rajkumar
 */
FUNCTION get_reconciled_ord_qty(
	p_ordid		T501_ORDER.C501_ORDER_ID%TYPE,
	p_pnum		T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE	
)RETURN NUMBER
IS
	v_qty	NUMBER := 0;
BEGIN

      SELECT SUM(c413_item_qty) INTO v_qty
      FROM t413_inhouse_trans_items 
     WHERE c413_ref_id = p_ordid
       AND NVL(c205_rec_part_number_id, c205_part_number_id) = p_pnum
       AND C413_VOID_FL IS NULL;

	RETURN v_qty;
END get_reconciled_ord_qty;

/*
* Description: This Function to fetch the Missing Transaction Sold qty
* Author:        Rajkumar
*/
FUNCTION get_sold_qty(
      p_txn_id    t412_inhouse_transactions.c412_inhouse_trans_id%TYPE,
      p_pnum      T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE 
)RETURN NUMBER
IS
      v_qty NUMBER := 0;
BEGIN
      BEGIN
       SELECT NVL (SUM (t413.c413_item_qty), 0)
   INTO v_qty
   FROM t413_inhouse_trans_items t413
  WHERE t413.c413_status_fl       IN ('S')
      AND t413.c412_inhouse_trans_id = p_txn_id
    AND t413.c205_part_number_id   = p_pnum
    AND t413.c413_void_fl         IS NULL;
    
      EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
            v_qty := 0;
      END;
      RETURN v_qty;
END get_sold_qty;

/*
* Description: This procedure to fetch the Reconciled transaction details
* Author:        Rajkumar
*/
PROCEDURE gm_fch_rec_lnitems(
      p_txnid     IN    t412_inhouse_transactions.c412_inhouse_trans_id%TYPE,
      p_out OUT   TYPES.cursor_type
)
AS
BEGIN
      OPEN p_out FOR
        SELECT 
        	T413.C413_TRANS_ID	ID,
		    T413.C205_PART_NUMBER_ID PNUM, 
		    T413.C413_ITEM_QTY QTY, 
		    DECODE (T413.C413_STATUS_FL, 'S', 'Sold', 'R','Returned', 'W', 'Writeoff', 'C', 'Consigned') STATUS, 
		    T413.C413_REF_ID REFID, 
		    GET_USER_NAME (T413.C413_LAST_UPDATED_BY) RECONBY, 
		    T413.C413_LAST_UPDATED_DATE RECONDATE,
		    NVL(T413.c205_rec_part_number_id,T413.C205_PART_NUMBER_ID) recpart, 
		    get_code_name(t413.c901_rec_reason) reason,
		    T413.C413_STATUS_FL STATUS_FL
		   FROM t413_inhouse_trans_items t413
		  WHERE t413.c413_status_fl       <> 'Q'
		    AND t413.c412_inhouse_trans_id = p_txnid
		    AND t413.c413_void_fl         IS NULL;
END gm_fch_rec_lnitems;
/*
 * Description: Fetch the Reconciled Qty of the Order
 * Author	  : Rajkumar
  */
FUNCTION GET_ORDER_RECONCILED_QTY (
	p_ordid		T501_ORDER.C501_ORDER_ID%TYPE,
	p_pnum	T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE	
) RETURN NUMBER
IS
	v_qty	NUMBER := 0;
BEGIN
	  SELECT SUM(QTY) INTO v_qty FROM
      (
      SELECT NVL (t413.c205_rec_part_number_id, t413.c205_part_number_id) PNUM,
        t413.c413_item_qty QTY
      FROM  t413_inhouse_trans_items t413
      WHERE t413.c413_ref_id  IS NOT NULL
      AND t413.c413_status_fl  = 'S'
      AND t413.c413_void_fl IS NULL     
      AND t413.c413_ref_id = p_ordid
      AND t413.c205_part_number_id = p_pnum
      )
       GROUP BY PNUM;
	RETURN v_qty;
END GET_ORDER_RECONCILED_QTY;
/*
 * Description: This function will check the order item is returned or not
 * Author: Rajkumar
 */
FUNCTION gm_chk_returns (
	p_ordid		T501_ORDER.C501_ORDER_ID%TYPE,
	p_pnum	T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE	
) RETURN NUMBER
IS
	v_qty	NUMBER := 0;
BEGIN
	BEGIN
	SELECT NVL(SUM(T507.C507_ITEM_QTY),0) INTO v_qty 
		FROM T506_RETURNS T506, T507_RETURNS_ITEM T507
		WHERE T507.C506_RMA_ID = T506.C506_RMA_ID
		AND T507.C205_PART_NUMBER_ID = p_pnum
		AND T506.C501_ORDER_ID = p_ordid
		AND (T507.C507_STATUS_FL = 'Q' OR T507.C507_STATUS_FL = 'C') 
		AND T506.C506_REASON NOT IN ('3316','3317')
		AND T506.C506_VOID_FL IS NULL
		GROUP BY T507.C205_PART_NUMBER_ID,T506.C501_ORDER_ID;
	EXCEPTION WHEN NO_DATA_FOUND
	THEN
		v_qty := 0;
	END;
		
		RETURN v_qty;
		
END gm_chk_returns;

/*
 * Description:	THis Procedure will fetch all the DO items that are Reconciled
 * Author:		Rajkumar
 */
PROCEDURE gm_fch_rec_doitems(
	p_out	OUT	TYPES.cursor_type
)
AS
v_plant_id    t5040_plant_master.c5040_plant_id%TYPE;
BEGIN
	
	SELECT  GET_PLANTID_FRM_CNTX()
		INTO  v_plant_id
	FROM DUAL;
	
	OPEN p_out FOR
	 SELECT ID, ORDID, PNUM
	  , SUM (QTY) QTY,MISSING_TXN,ITEMPRICE,CNUM
	   FROM
	    (
	         SELECT T413.C413_TRANS_ID ID, T413.C413_REF_ID ORDID, NVL (T413.C205_REC_PART_NUMBER_ID,
	            T413.C205_PART_NUMBER_ID) PNUM, T413.C413_ITEM_QTY QTY, T412.C412_INHOUSE_TRANS_ID MISSING_TXN,
	            T413.C413_ITEM_PRICE ITEMPRICE,T413.C413_CONTROL_NUMBER CNUM
	           FROM T412_INHOUSE_TRANSACTIONS T412, T413_INHOUSE_TRANS_ITEMS T413, T504A_LOANER_TRANSACTION T504A
	          , T504A_CONSIGNMENT_LOANER T504
	          WHERE T412.C412_INHOUSE_TRANS_ID        = T413.C412_INHOUSE_TRANS_ID
	            AND T504A.C504A_LOANER_TRANSACTION_ID = T412.C504A_LOANER_TRANSACTION_ID
	            AND T504A.C504A_VOID_FL              IS NULL
	            AND T504.C504_CONSIGNMENT_ID          = T504A.C504_CONSIGNMENT_ID
	            AND T504.C504A_VOID_FL               IS NULL
	            AND T413.C413_REF_ID                 IS NOT NULL
	            AND T413.C413_STATUS_FL               = 'S' -- SOLD
	            AND T412.c5040_plant_id			  = v_plant_id
	            AND T412.C412_VOID_FL                IS NULL
	            AND T413.C413_VOID_FL                IS NULL
	    )
	GROUP BY PNUM, ORDID, ID,MISSING_TXN,ITEMPRICE,CNUM;
END gm_fch_rec_doitems;

/*
 * Description: This Procedure will Fetch the Orders that are Swapped from Loaner to Consignment
 * Author: Rajkumar
 */
PROCEDURE gm_fch_swapped_ord(
	p_out	OUT	TYPES.cursor_type
)
AS
v_plant_id    t5040_plant_master.c5040_plant_id%TYPE;
BEGIN
	
	--Getting company id from context.   
    SELECT get_plantid_frm_cntx()
	  INTO v_plant_id 
	  FROM dual;
	  
	OPEN p_out FOR
		 SELECT REDORD.ORDID ORDID, REDORD.PNUM PNUM, NVL(ABS(SUM(T502.C502_ITEM_QTY)),0) QTY
		   FROM
		    (
		         SELECT DISTINCT T413.C413_REF_ID ORDID, NVL (T413.C205_REC_PART_NUMBER_ID, T413.C205_PART_NUMBER_ID) PNUM
		           FROM T412_INHOUSE_TRANSACTIONS T412, T413_INHOUSE_TRANS_ITEMS T413, T504A_LOANER_TRANSACTION T504A
		          , T504A_CONSIGNMENT_LOANER T504
		          WHERE T412.C412_INHOUSE_TRANS_ID        = T413.C412_INHOUSE_TRANS_ID
		            AND T504A.C504A_LOANER_TRANSACTION_ID = T412.C504A_LOANER_TRANSACTION_ID
		            AND T504A.C504A_VOID_FL              IS NULL
		            AND T504.C504_CONSIGNMENT_ID          = T504A.C504_CONSIGNMENT_ID
		            AND T504.C504A_VOID_FL               IS NULL
		            AND T413.C413_REF_ID                 IS NOT NULL
		            AND T413.C413_STATUS_FL               = 'S' -- SOLD
		            AND T412.c5040_plant_id			  = v_plant_id
		            --AND T504A.C1900_COMPANY_ID			  = v_company_id
		            AND T412.C412_VOID_FL                IS NULL
		            AND T413.C413_VOID_FL                IS NULL
		    )
		    REDORD, T501_ORDER T501, T502_ITEM_ORDER T502
		  WHERE T502.C205_PART_NUMBER_ID  = REDORD.PNUM
		    AND T502.C901_TYPE            = '50301'
		    AND T502.C501_ORDER_ID        = T501.C501_ORDER_ID
		    AND T501.C901_ORDER_TYPE      = '2531'
		    AND T501.C501_PARENT_ORDER_ID = REDORD.ORDID
		    AND T501.c5040_plant_id	  = v_plant_id
		    AND T501.C501_VOID_FL        IS NULL
		    AND T502.C502_VOID_FL        IS NULL
		    GROUP BY REDORD.ORDID,REDORD.PNUM; 
END gm_fch_swapped_ord;

/*
 *	Description: Fetch the swapped qty for the order
 *	Author	: Rajkumar 
 */
FUNCTION get_swapped_qty(
	p_ordid		T501_ORDER.C501_ORDER_ID%TYPE,
	p_pnum	T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE
) RETURN NUMBER
IS
v_qty	NUMBER :=0;
BEGIN
	BEGIN
		SELECT NVL(ABS(SUM(T502.C502_ITEM_QTY)),0) INTO v_qty
		FROM T501_ORDER T501, T502_ITEM_ORDER T502
	  	 WHERE T502.C205_PART_NUMBER_ID  = p_pnum
		    AND T502.C901_TYPE            = '50301'
		    AND T502.C501_ORDER_ID        = T501.C501_ORDER_ID
		    AND T501.C901_ORDER_TYPE      = '2531'
		    AND T501.C501_PARENT_ORDER_ID = p_ordid
		    --AND T501.C1900_COMPANY_ID	  = v_company_id
		    AND T501.C501_VOID_FL        IS NULL
		    AND T502.C502_VOID_FL        IS NULL;
	EXCEPTION WHEN NO_DATA_FOUND
	THEN
		v_qty := 0;
	END;
	RETURN v_qty;
END get_swapped_qty;
/*
 * Description: This Procedure will Fetch the Orders that are Returned Orders 
 * Author: Rajkumar
 */
PROCEDURE gm_fch_ret_ord(
	p_out	OUT	TYPES.cursor_type
)
AS
v_plant_id    t5040_plant_master.c5040_plant_id%TYPE;
BEGIN
	--Getting company id from context.   
    SELECT get_plantid_frm_cntx()
	  INTO v_plant_id 
	  FROM dual;
	  
	OPEN p_out FOR 
		 SELECT REDORD.ORDID ORDID, REDORD.PNUM PNUM, NVL(ABS(SUM(T507.C507_ITEM_QTY)),0) QTY
		   FROM
		    (
		         SELECT DISTINCT T413.C413_REF_ID ORDID, NVL (T413.C205_REC_PART_NUMBER_ID, T413.C205_PART_NUMBER_ID) PNUM
		           FROM T412_INHOUSE_TRANSACTIONS T412, T413_INHOUSE_TRANS_ITEMS T413, T504A_LOANER_TRANSACTION T504A
		          , T504A_CONSIGNMENT_LOANER T504
		          WHERE T412.C412_INHOUSE_TRANS_ID        = T413.C412_INHOUSE_TRANS_ID
		            AND T504A.C504A_LOANER_TRANSACTION_ID = T412.C504A_LOANER_TRANSACTION_ID
		            AND T504A.C504A_VOID_FL              IS NULL
		            AND T504.C504_CONSIGNMENT_ID          = T504A.C504_CONSIGNMENT_ID
		            AND T412.c5040_plant_id			  = v_plant_id
		            --AND T504A.C1900_COMPANY_ID			  = v_company_id
		            AND T504.C504A_VOID_FL               IS NULL
		            AND T413.C413_REF_ID                 IS NOT NULL
		            AND T413.C413_STATUS_FL               = 'S' -- SOLD
		            AND T412.C412_VOID_FL                IS NULL
		            AND T413.C413_VOID_FL                IS NULL
		    )
		    REDORD, T506_RETURNS T506, T507_RETURNS_ITEM T507
		    WHERE T507.C506_RMA_ID = T506.C506_RMA_ID
				AND T507.C205_PART_NUMBER_ID = REDORD.PNUM
				AND T506.C501_ORDER_ID = REDORD.ORDID
				AND (T507.C507_STATUS_FL = 'Q' OR T507.C507_STATUS_FL = 'C') 
		        AND T507.C507_ITEM_QTY > 0 
				AND T506.C506_REASON NOT IN ('3316','3317')
				AND T506.c5040_plant_id			  = v_plant_id
				AND T506.C506_VOID_FL IS NULL
				GROUP BY REDORD.ORDID,REDORD.PNUM; 
END gm_fch_ret_ord;

/*
 * Description:	This procedure will fetch the Missing Loaner Transaction Items  
 * 				for the particular Missing Loaner Transactions based on the loaner extension  date
 * Author	  :	ppandiyan
 */
PROCEDURE gm_fch_ln_item_by_loan_ext(
	p_txnid	IN	T412_INHOUSE_TRANSACTIONS.C412_INHOUSE_TRANS_ID%TYPE,
	p_out	OUT	TYPES.cursor_type
)
AS
BEGIN
	
	OPEN p_out FOR
		SELECT C205_PART_NUMBER_ID PNUM, T504A.C703_SALES_REP_ID REPID, T504A.C704_ACCOUNT_ID ACCID
	      , (GM_PKG_OP_FETCH_RECON.GET_MISSING_QTY (C205_PART_NUMBER_ID, T412.C412_INHOUSE_TRANS_ID) -
	        GM_PKG_OP_FETCH_RECON.GET_RECONCILED_QTY (C205_PART_NUMBER_ID, T412.C412_INHOUSE_TRANS_ID)) ITEMQTY,
	        T413.C413_ITEM_PRICE ITEMPRICE, T413.C413_CONTROL_NUMBER CNUM, T504A.C504A_LOANER_DT LOANDT,T504A.C504A_EXPECTED_RETURN_DT LAON_EXT_DT
	       FROM T413_INHOUSE_TRANS_ITEMS T413, T412_INHOUSE_TRANSACTIONS T412, T504A_LOANER_TRANSACTION T504A
	      WHERE T504A.C504A_LOANER_TRANSACTION_ID = T412.C504A_LOANER_TRANSACTION_ID
	        AND T412.C412_INHOUSE_TRANS_ID        = T413.C412_INHOUSE_TRANS_ID
	        AND (T504A.C504A_IS_LOANER_EXTENDED = 'Y' or T504A.C504A_IS_REPLENISHED = 'Y')
	        AND T413.C412_INHOUSE_TRANS_ID        = p_txnid
	        AND T413.C413_STATUS_FL				  = 'Q'
	        --AND T412.C1900_COMPANY_ID			  = v_company_id
	        --AND T504A.C1900_COMPANY_ID		  = v_company_id
	        AND T504A.C504A_VOID_FL              IS NULL
	        AND T412.C412_VOID_FL                IS NULL
	        AND T413.C413_VOID_FL                IS NULL;
	        
END gm_fch_ln_item_by_loan_ext;

/*
 * Description: This procedure will fetch the Missing Loaner Transaction that was not scanned for new transaction by date
 * Author: 		ppandiyan
 */
PROCEDURE gm_fch_not_scanned_ln_by_date(
p_out	OUT		TYPES.cursor_type
)
AS
v_plant_id   t5040_plant_master.c5040_plant_id%TYPE;
v_date       t906_rules.c906_rule_value%TYPE;
v_date_fmt   t906_rules.c906_rule_value%TYPE;
BEGIN
	--the below code is referred from PROCEDURE 'gm_fch_not_scanned_ln' but slightly modified for new Transaction
	
	SELECT  get_plantid_frm_cntx()
		INTO  v_plant_id
	FROM DUAL;
	
	SELECT get_company_dtfmt(get_plant_parent_comp_id(v_plant_id))
	    INTO v_date_fmt 
	FROM DUAL;
   
	SELECT get_rule_value (v_plant_id,'LOANER_RECON')
         INTO v_date
    FROM DUAL;

	OPEN p_out FOR 
		  SELECT T412.C412_INHOUSE_TRANS_ID TRANSID
           FROM T412_INHOUSE_TRANSACTIONS T412
          WHERE T412.C412_STATUS_FL            <= 3
            AND T412.C412_TYPE                  = '50159'	-- Missing
            AND NOT EXISTS
            (
                 SELECT t412A.C412_INHOUSE_TRANS_ID 
                   FROM T412A_INHOUSE_TRANS_ATTRIBUTE T412A
                  WHERE T412A.C901_ATTRIBUTE_TYPE   = '50822'		-- Reconciled
                   AND T412A.C412_INHOUSE_TRANS_ID = T412.C412_INHOUSE_TRANS_ID
                   AND T412.C5040_PLANT_ID = v_plant_id
                   AND T412A.C412A_ATTRIBUTE_VALUE = 'Y'
                   AND T412.C412_VOID_FL  IS NULL
                   AND t412A.C412A_VOID_FL  IS NULL
            )
            AND T412.C412_VOID_FL IS NULL
            AND T412.C5040_PLANT_ID = v_plant_id
            AND T412.C412_CREATED_DATE > to_date(v_date,v_date_fmt)
        ORDER BY T412.C412_CREATED_DATE DESC;
END gm_fch_not_scanned_ln_by_date;

END gm_pkg_op_fetch_recon;
/