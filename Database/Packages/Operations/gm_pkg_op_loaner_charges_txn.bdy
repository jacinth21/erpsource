    CREATE OR REPLACE PACKAGE BODY gm_pkg_op_loaner_charges_txn
IS
 /***********************************************************
 * Description : This Procedure is used to get the late fee status name
 * Author	   : Sindhu
 ***********************************************************/   
PROCEDURE gm_sav_loaner_charge_amount(
	p_dtl_id IN  t9201_charge_details.c9201_charge_details_id%TYPE,
	p_user_id IN  t9201_charge_details.c9201_updated_by%TYPE,
	p_amount IN t9201_charge_details.C9201_amount%TYPE
	)
AS
BEGIN
	UPDATE t9201_charge_details t9201
	SET t9201.C9201_amount = p_amount,
	t9201.c9201_updated_by = p_user_id,
  	t9201.c9201_updated_date = CURRENT_DATE
	WHERE t9201.c9201_charge_details_id = p_dtl_id 
    AND t9201.C9201_VOID_FL IS NULL;
    
END gm_sav_loaner_charge_amount;
        
        
PROCEDURE gm_sav_loaner_charge_comment(
	p_dtl_id IN  t9201_charge_details.c9201_charge_details_id%TYPE,
	p_user_id IN  t9201_charge_details.c9201_updated_by%TYPE,
	p_comment IN t9201_charge_details.C9201_Comment %TYPE
)
AS
BEGIN
	UPDATE t9201_charge_details t9201
	SET t9201.C9201_Comment = p_comment,
		t9201.c9201_updated_by = p_user_id,
		t9201.c9201_updated_date = CURRENT_DATE
	WHERE t9201.c9201_charge_details_id = p_dtl_id 
		AND t9201.C9201_VOID_FL IS NULL;
	
END gm_sav_loaner_charge_comment;

/***********************************************************
 * Description : This Procedure is used to get the late fee status name
 * Author	   : Sindhu 
 ***********************************************************/	
PROCEDURE gm_sav_loaner_charge_credit_amount(
	p_charge_dtl_id  IN t9201_charge_details.c9201_charge_details_id%TYPE,
	p_credit_amt     IN t9201_charge_details.c9201_amount%TYPE,
	p_comments       IN t9201_charge_details.C9201_Comment%TYPE,
	p_credit_date    IN t9201_charge_details.C9201_CREDITED_DATE%TYPE,
	p_charge_start_date IN t9201_charge_details.c9201_charge_start_date%TYPE,
	p_charge_end_date  IN t9201_charge_details.c9201_charge_end_date%TYPE,
	p_user_id IN  t9201_charge_details.c9201_updated_by%TYPE
)
AS
	v_date_fmt  		VARCHAR2(20);
	v_incident_id 	    VARCHAR2(20);
	v_distributor_id    t9201_charge_details.c701_distributor_id%TYPE;
	v_sales_rep_id      t703_sales_rep.c703_sales_rep_id%TYPE;
	v_ass_rep_id        t703_sales_rep.c703_rep_category%TYPE;
	v_rep_category      t703_sales_rep.c703_rep_category%TYPE;
	v_status            t9201_charge_details.c9201_status%TYPE;
	v_deduction_date    DATE;
BEGIN

	select get_compdtfmt_frm_cntx()
      into v_date_fmt 
      from dual;
	
	gm_pkg_op_msng_chrg.gm_validate_charge(TO_NUMBER(p_charge_dtl_id), p_credit_amt);
	
	SELECT t9201.c9200_incident_id ,t703.c701_distributor_id , t703.c703_sales_rep_id, t9201.c703_ass_rep_id, t703.c703_rep_category,t9201.c9201_status
		INTO v_incident_id, v_distributor_id ,v_sales_rep_id, v_ass_rep_id,v_rep_category,v_status
    FROM t9201_charge_details t9201,t703_sales_rep t703
    WHERE t9201.c703_sales_rep_id     = t703.c703_sales_rep_id
    AND c9201_charge_details_id = p_charge_dtl_id;

	v_deduction_date := gm_pkg_op_loaner_charges_rpt.get_deduction_date(v_sales_rep_id,NULL,v_status);
	
	INSERT
		INTO t9201_charge_details
		(
		c9201_charge_details_id ,
		c9201_amount ,
		c9201_status ,
		c9201_created_by ,
		c9201_created_date,
		c9200_incident_id ,
		c701_distributor_id,
		c703_sales_rep_id,
		c9201_credited_date,
		c9201_master_chrg_det_id,
		c703_ass_rep_id,
		c9201_charge_start_date,
		c9201_charge_end_date,
		C9201_Comment,
		c9201_deduction_date
		)
		VALUES(
		s9201_charge_details.NEXTVAL,
		p_credit_amt,
		25,
		p_user_id,
		current_date,
		v_incident_id ,
		v_distributor_id ,
		v_sales_rep_id,
		p_credit_date,
		p_charge_dtl_id,
		v_ass_rep_id,
		CURRENT_DATE,
		CURRENT_DATE,
		p_comments,
		v_deduction_date 
		);
END gm_sav_loaner_charge_credit_amount;	


/***********************************************************
 * Description : This Procedure is used to get the late fee status name
 * Author	   : Sindhu
 ***********************************************************/	
PROCEDURE gm_sav_loaner_charge_status(
	p_json    IN CLOB,
	p_user_id IN  t9201_charge_details.c9201_updated_by%TYPE
)
AS
   v_sales_rep_id         t703_sales_rep.c703_sales_rep_id%TYPE;
   v_sav_late_fee_obj     json_object_t;
   v_late_fee_array       json_array_t;
   v_charge_id            t9201_charge_details.c9201_charge_details_id%TYPE;
   v_status               T901_CODE_LOOKUP.C901_CODE_ID%TYPE;
   v_status_id            NUMBER(2);
   v_deduction_date       DATE;
   v_waive_reason         t9201_charge_details.c901_recon_comments%TYPE;
BEGIN
	 v_late_fee_array := json_array_t(p_json);
	 
	FOR i IN 0..v_late_fee_array.get_size - 1 
	LOOP
		v_sav_late_fee_obj 	:=      TREAT(v_late_fee_array.get(i) AS json_object_t);
		v_charge_id 		:= 		v_sav_late_fee_obj.get_string('chargeid');
	    v_status     	    := 		v_sav_late_fee_obj.get_string('status');
	    v_waive_reason     	:= 		v_sav_late_fee_obj.get_string('waiveReason');
	       
	       v_status_id := get_code_name_alt(v_status);
      		
      		IF 	v_status_id = 20 THEN  --Submitted
      			SELECT t703.c703_sales_rep_id
				INTO v_sales_rep_id
				FROM t9201_charge_details t9201,
				  t703_sales_rep t703
				WHERE t9201.c703_sales_rep_id     = t703.c703_sales_rep_id
				AND t9201.c9201_charge_details_id = v_charge_id 
				AND t9201.c9201_void_fl IS NULL
				and t703.c703_void_fl is null;
				
				v_deduction_date := gm_pkg_op_loaner_charges_rpt.get_deduction_date(v_sales_rep_id,NULL,v_status_id);
      			
      		END IF;

			UPDATE t9201_charge_details
			SET c9201_status = v_status_id,
			c9201_deduction_date = v_deduction_date,
			c901_recon_comments = v_waive_reason,
			c9201_updated_by = p_user_id,
			c9201_updated_date = CURRENT_DATE
			WHERE c9201_charge_details_id = v_charge_id;
		
	END LOOP;
		 
        
	
END gm_sav_loaner_charge_status;
    
END gm_pkg_op_loaner_charges_txn;
/