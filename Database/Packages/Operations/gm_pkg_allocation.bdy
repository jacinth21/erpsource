/* Formatted on 2011/10/26 10:53 (Formatter Plus v4.8.0) */
--@"C:\DATABASE\packages\operations\gm_pkg_allocation.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_allocation
IS
/*	gm_invpick_assign_detail - This procedure will upate user for specific order in T5050_INVPICK_ASSIGN_DETAIL
	input: Deliminted String
*/
	PROCEDURE gm_invpick_assign_detail (
		p_inputstr	 IN   VARCHAR2
	)
	AS
		v_string	   VARCHAR2 (30000) := p_inputstr;
		v_substring    VARCHAR2 (30000);
		v_inputstr	   VARCHAR2 (30000) := p_inputstr;
		v_row_data	   VARCHAR2 (30000);
		v_pick_type    VARCHAR2 (30000);
		v_type		   VARCHAR2 (30000);
		v_user		   VARCHAR2 (30000);
		v_id		   VARCHAR2 (30000);
		v_ref_id	   VARCHAR2 (30000);
		v_status	   VARCHAR2 (30000);
	BEGIN
		IF INSTR (p_inputstr, '$') = 0
		THEN
			v_inputstr	:= p_inputstr || '$';
		END IF;

		IF INSTR (v_inputstr, '$') <> 0
		THEN
			v_row_data	:= SUBSTR (v_inputstr, 1, INSTR (v_inputstr, '$') - 1);
			v_inputstr	:= SUBSTR (v_inputstr, INSTR (v_inputstr, '$') + 1);
			v_string	:= v_row_data;

			/* Loop the Delminted String and reterive the values */
			WHILE INSTR (v_string, '|') <> 0
			LOOP
				v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
				v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
				v_id		:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_pick_type := SUBSTR (v_substring, 1, INSTR (v_substring, '^') + 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_user		:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_ref_id	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_status	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);

				IF v_status IS NULL
				THEN
					v_status	:= 93003;
				END IF;

				IF v_status = 93003 OR v_status = 93007
				THEN
					IF v_user IS NOT NULL
					THEN
						/* Update User in T5050 table for specific Order */
						UPDATE t5050_invpick_assign_detail t5050
						   SET t5050.c101_allocated_user_id = v_user
							 , t5050.c901_status = v_status
							 , t5050.c5050_allocated_dt = CURRENT_DATE
						 WHERE t5050.c5050_invpick_assign_id = v_id;
					ELSE
						/* Update User in T5050 table for specific Order */
						UPDATE t5050_invpick_assign_detail t5050
						   SET t5050.c101_allocated_user_id = ''
							 , t5050.c901_status = ''
						 WHERE t5050.c5050_invpick_assign_id = v_id;
					END IF;
				ELSE
					raise_application_error (-20897, '');
				END IF;
			END LOOP;
		END IF;
	END gm_invpick_assign_detail;

/*	gm_allocate_user - This procedure will upate Status for specific User in T5051_Allocate_User
	input: Deliminted String
*/
	PROCEDURE gm_allocate_user (
		p_inputstr	 IN   VARCHAR2
	)
	AS
		v_string	   VARCHAR2 (30000) := p_inputstr;
		v_substring    VARCHAR2 (30000);
		v_inputstr	   VARCHAR2 (30000) := p_inputstr;
		v_row_data	   VARCHAR2 (30000);
		v_pick_type    VARCHAR2 (30000);
		v_type		   VARCHAR2 (30000);
		v_user		   VARCHAR2 (30000);
		v_id		   VARCHAR2 (30000);
		v_status	   VARCHAR2 (30000);
		v_user_count   NUMBER;
	BEGIN
		v_user_count := 0;

		IF INSTR (p_inputstr, '$') = 0
		THEN
			v_inputstr	:= p_inputstr || '$';
		END IF;

		IF INSTR (v_inputstr, '$') <> 0
		THEN
			v_row_data	:= SUBSTR (v_inputstr, 1, INSTR (v_inputstr, '$') - 1);
			v_inputstr	:= SUBSTR (v_inputstr, INSTR (v_inputstr, '$') + 1);
			v_string	:= v_row_data;

			/* Loop the Delminted String and reterive the values */
			WHILE INSTR (v_string, '|') <> 0
			LOOP
				v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
				v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
				v_id		:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_user		:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_status	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);

				/* Update Status in T5151table for specific User */
				IF (LENGTH (v_status) > 0)
				THEN
					UPDATE t5051_allocate_user t5051
					   SET t5051.c901_status = v_status
					 WHERE t5051.c5051_allocate_user_id = v_id;
				END IF;

				IF (SQL%ROWCOUNT = 0)
				THEN
					/* Allow Insert only when both User and Status are not null's */
					IF (LENGTH (v_user) > 0 AND LENGTH (v_status) > 0)
					THEN
						SELECT s5051_allocate_user.NEXTVAL
						  INTO v_id
						  FROM DUAL;

						/* checking whether same user exists or not, if no, Insert ot raise exception */
						SELECT COUNT (*)
						  INTO v_user_count
						  FROM t5051_allocate_user t5051
						 WHERE t5051.c101_user_id = v_user;

						IF (v_user_count = 0)
						THEN
							/* Insert User in to T5051 */
							INSERT INTO t5051_allocate_user
										(c5051_allocate_user_id, c101_user_id, c901_status
									   , c5051_created_by, c5051_created_date, c5051_allocated_sequence
										)
								 VALUES (TO_NUMBER (v_id), TO_NUMBER (v_user), TO_NUMBER (v_status)
									   , TO_NUMBER (v_user), CURRENT_DATE, 0
										);
						ELSE
							raise_application_error (-20898, '');
						END IF;
					ELSE
						raise_application_error (-20899, '');
					END IF;
				END IF;   /* End of SQL Count */
			END LOOP;	/* End of Loop */
		END IF;
	END gm_allocate_user;

/*	gm_ins_invpick_assign_detail- This procedure will insert record in T5050_INVPICK_ASSIGN_DETAIL with ref_type (type of request), ref_id (id of request), created_by
	input: ref_type, ref_id, created_by
*/
	PROCEDURE gm_ins_invpick_assign_detail (
		p_ref_type	   IN	t5050_invpick_assign_detail.c901_ref_type%TYPE
	  , p_ref_id	   IN	t5050_invpick_assign_detail.c5050_ref_id%TYPE
	  , p_created_by   IN	t5050_invpick_assign_detail.c5050_created_by%TYPE
	)
	AS
		v_invpick_assign_id t5050_invpick_assign_detail.c5050_invpick_assign_id%TYPE;
		v_ref_type	 VARCHAR2(10);
		v_comp_id  T1900_COMPANY.C1900_COMPANY_ID%TYPE;
		v_plant_id T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
		v_req_dtl_id   t526_product_request_detail.c526_product_request_detail_id%TYPE;
        v_req_company_id  t1900_company.c1900_company_id%TYPE;
        v_count NUMBER :=0;
	BEGIN	  
		SELECT get_compid_frm_cntx(),get_plantid_frm_cntx() INTO v_comp_id,v_plant_id FROM dual;
					
		BEGIN
    		SELECT c526_product_request_detail_id INTO v_req_dtl_id 
    		  FROM t504a_loaner_transaction 
    		 WHERE c504_consignment_id = p_ref_id 
    		   AND c504a_return_dt IS NULL 
    		   AND c504a_void_fl IS NULL;
	    EXCEPTION WHEN NO_DATA_FOUND THEN
      			v_req_dtl_id := null;
		END; 
		
	    SELECT get_transaction_company_id (v_comp_id,v_req_dtl_id,'LOANERPICKFILTER' ,'LOANERREQUEST') INTO v_req_company_id FROM DUAL;	  
	   		
		IF p_ref_type in (93601, 93602,93603, 93604)	--93601	Consigned Set, 93602 Loaner Set,93603 Inhouse Loaner Set --(Pending Pick), 93604	Pending Putaway 
		THEN
			v_ref_type := 'SET';
		END IF;
		-- When the Order/Consignment (c5050_ref_id) already exists, do nothing.
		SELECT c5050_invpick_assign_id
		  INTO v_invpick_assign_id
		  FROM t5050_invpick_assign_detail
		 WHERE c5050_ref_id = p_ref_id 
       	   AND c901_ref_type = DECODE(v_ref_type, 'SET', p_ref_type, c901_ref_type)  -- For Loaner set and consign set inserting issue-SPP
		   AND c5050_void_fl IS NULL
       	   AND NVL(c901_status, -9999) != 93006;       	   

	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
		--PMT#48517-Avoid duplicate entries with same order Id on t5050_invpick_assign_detail table that will prevent exact fetch error while controlling parts. 
		IF(p_ref_type = 93001)THEN
			BEGIN
				SELECT count(c5050_invpick_assign_id)
					INTO v_count
					FROM t5050_invpick_assign_detail
					WHERE c5050_ref_id = p_ref_id
					AND c5050_void_fl IS NULL
					AND c901_status = 93006; --completed
			EXCEPTION WHEN NO_DATA_FOUND
			THEN
			v_count := 0;
			END;
		END IF;

		IF v_count < 1 THEN
			SELECT s5050_invpick_assign_detail.NEXTVAL
			  	INTO v_invpick_assign_id
			  	FROM DUAL;

			INSERT INTO t5050_invpick_assign_detail
						(c5050_invpick_assign_id, c901_ref_type, c5050_ref_id, c5050_created_by, c5050_created_date,c1900_company_id,c5040_plant_id
						)
				 VALUES (v_invpick_assign_id, p_ref_type, p_ref_id, p_created_by, CURRENT_DATE,v_req_company_id,v_plant_id
						);
		ELSE
			UPDATE t5050_invpick_assign_detail 
			SET c901_status = NULL,
				c101_allocated_user_id = NULL,
				c5050_allocated_dt = NULL,
				c5050_processed_dt = NULL,
				c5050_last_updated_by = p_created_by,
				c5050_last_updated_date = CURRENT_DATE
			 WHERE c5050_ref_id = p_ref_id
			  AND c5050_void_fl IS NULL
			  AND c901_ref_type = 93001
			  AND c901_status = 93006;
		END IF;
	END gm_ins_invpick_assign_detail;

	PROCEDURE gm_inv_void_txn (
		p_invpick_assign_id   IN   t5050_invpick_assign_detail.c5050_invpick_assign_id%TYPE
	  , p_userid			  IN   t5050_invpick_assign_detail.c5050_created_by%TYPE
	)
	AS
	BEGIN
		UPDATE t5050_invpick_assign_detail
		   SET c5050_void_fl = 'Y'
			 , c5050_last_updated_by = p_userid
			 , c5050_last_updated_date = CURRENT_DATE
		 WHERE c5050_invpick_assign_id = p_invpick_assign_id;
	END;

	/*************************
	* This Procedure will be executed as an oracle JOB.
	* It will check if the assignments are voided in the parent table.
	* If it is voided, it will void the assignment.
	*************************/
	PROCEDURE gm_upd_voided_assignment
	AS
		v_ref_id	   t5050_invpick_assign_detail.c5050_ref_id%TYPE;
		v_ref_typ	   t5050_invpick_assign_detail.c901_ref_type%TYPE;
		v_status_fl    VARCHAR2 (10);--Status Flag size needs to be increased to avoid character string buffer too small error 
		v_void_fl	   VARCHAR2 (1);
		v_inv_id	   t5050_invpick_assign_detail.c5050_invpick_assign_id%TYPE;
		v_created_by   t5050_invpick_assign_detail.c5050_created_by%TYPE;
		v_start_dt	   DATE;
		v_invpick_status t5050_invpick_assign_detail.c901_status%TYPE;

/*
	ALGORITHM:
	1. Get All the assignments that are NOT ASSIGNED, ASSIGNED.
	2. ITerate the cursor and get the REF ID, Ref Type.
	3. IF Ref TYpe ='Order'/Consignment/Pick/Put Away, get the record from order/consignment/In House transaction table and check for void flag.
	4. If the void flag is NOT NULL in	table, Update the assignment table and void the record.

*/		-- Cursor to fetch all the record thats not completed
		CURSOR curr_assignment
		IS
			SELECT c5050_ref_id refid, c901_ref_type reftyp, c5050_invpick_assign_id invid
			  FROM t5050_invpick_assign_detail
			 WHERE NVL (c901_status, -999) NOT IN (93006) AND c5050_void_fl IS NULL AND c5050_ref_id IS NOT NULL;
	BEGIN
		FOR var_assignment IN curr_assignment
		LOOP
			v_ref_id	:= var_assignment.refid;
			v_ref_typ	:= var_assignment.reftyp;
			v_inv_id	:= var_assignment.invid;
			v_status_fl := '';
			v_void_fl	:= '';
			BEGIN
				-- If record already updated then skip the void process
				SELECT	   c5050_ref_id, c5050_created_by
					  INTO v_ref_id, v_created_by
					  FROM t5050_invpick_assign_detail
					 WHERE c5050_invpick_assign_id = v_inv_id
					   AND NVL (c901_status, -999) NOT IN (93006)
					   AND c5050_void_fl IS NULL
				FOR UPDATE;

				-- ORDER
				IF (v_ref_typ = 93001)
				THEN
					BEGIN
						SELECT	   DECODE (c501_status_fl, '2', '4', c501_status_fl)
								 , DECODE (c501_status_fl
										 , '0', 'Y'
										 , c501_void_fl
										  )   --Decode has been added for not to display the BO on device.
							  INTO v_status_fl
								 , v_void_fl
							  FROM t501_order
							 WHERE c501_order_id = v_ref_id
						FOR UPDATE;
					EXCEPTION
						-- Common no data found for all the SQL exception
						WHEN NO_DATA_FOUND
						THEN
							gm_inv_void_txn (v_inv_id, v_created_by);
					END;
				-- CONSIGNMENT,Quarantine,, Qua > Shelf, Reprocess, shelf to repack, Repack to FG
				ELSIF (   v_ref_typ = 93002
					   OR v_ref_typ = 93321
					   OR v_ref_typ = 93333
					   OR v_ref_typ = 93334
					   OR v_ref_typ = 93349
					   OR v_ref_typ = 93338
					  )
				THEN
					SELECT	   c504_void_fl, c504_status_fl
						  INTO v_void_fl, v_status_fl
						  FROM t504_consignment
						 WHERE c504_consignment_id = v_ref_id
					FOR UPDATE;
				--Loaner Ext,Loaner Set, In House Set, Build Set, Bulk Out,, Build > Shelf, In House > Shelf, Loaner Set > Shelf, Raw Mat > Finished, Return to Shelf,FGBL txn, FGRM txn
				ELSIF (   v_ref_typ = 93322
					   OR v_ref_typ = 93323
					   OR v_ref_typ = 93008
					   OR v_ref_typ = 93324
					   OR v_ref_typ = 93325
					   OR v_ref_typ = 93328
					   OR v_ref_typ = 93329
					   OR v_ref_typ = 93330
					   OR v_ref_typ = 93331
					   OR v_ref_typ = 93332
					   OR v_ref_typ = 93337
					   OR v_ref_typ = 93352
					  )
				THEN
					SELECT	   c412_void_fl, c412_status_fl
						  INTO v_void_fl, v_status_fl
						  FROM t412_inhouse_transactions
						 WHERE c412_inhouse_trans_id = v_ref_id
					FOR UPDATE;
				-- DHR
				ELSIF (v_ref_typ = 93335)
				THEN
					SELECT	   c408_void_fl, c408_status_fl
						  INTO v_void_fl, v_status_fl
						  FROM t408_dhr
						 WHERE c408_dhr_id = v_ref_id
					FOR UPDATE;
				END IF;

				IF v_void_fl IS NOT NULL
				THEN
					gm_inv_void_txn (v_inv_id, v_created_by);
				END IF;

				IF v_status_fl = 4
				THEN
					gm_pkg_op_inv_scan.gm_check_inv_order_status (v_ref_id, 'Completed', v_created_by);
				END IF;
			EXCEPTION
				-- Common no data found for all the SQL exception
				WHEN NO_DATA_FOUND
				THEN
					NULL;
			END;
		END LOOP;

		COMMIT;
	EXCEPTION
		WHEN OTHERS
		THEN
			ROLLBACK;
			--
			gm_pkg_cm_job.gm_cm_notify_load_info (v_start_dt
												, CURRENT_DATE
												, 'E'
												, SQLERRM
												, 'gm_pkg_allocation.gm_upd_voided_assignment'
												 );
			COMMIT;
	END gm_upd_voided_assignment;

	/*************************
	  * This Procedure will void txn which type is DHR
	*******************************/
	PROCEDURE gm_inv_void_bytxn (
		p_ref_id	 IN   t5050_invpick_assign_detail.c5050_ref_id%TYPE
	  , p_ref_type	 IN   t5050_invpick_assign_detail.c901_ref_type%TYPE
	  , p_userid	 IN   t5050_invpick_assign_detail.c5050_created_by%TYPE
	)
	AS
	BEGIN
		-- 93601 Pending Pick (consigned set), 93602 Pending Pick (Loaner Set), 93604 Pending Putaway, when void not including complete one
		IF p_ref_type in (93601, 93602, 93604)
		THEN
			UPDATE t5050_invpick_assign_detail
			   SET c5050_void_fl = 'Y'
				 , c5050_last_updated_by = p_userid
				 , c5050_last_updated_date = CURRENT_DATE
			 WHERE c5050_ref_id = p_ref_id 
			 AND c901_ref_type = p_ref_type	
			 AND c5050_void_fl IS NULL
			 AND NVL(c901_status, -9999) != 93006;
		ELSE 
		UPDATE t5050_invpick_assign_detail
		   SET c5050_void_fl = 'Y'
			 , c5050_last_updated_by = p_userid
			 , c5050_last_updated_date = CURRENT_DATE
		 WHERE c5050_ref_id = p_ref_id AND c5050_void_fl IS NULL;
		 END IF;
	END gm_inv_void_bytxn;

/*************************
	  * This Procedure will allocate user
	*******************************/
	PROCEDURE gm_sav_allocate_request (
		p_orderid	 IN 	  t502_item_order.c501_order_id%TYPE
	  , p_reftype	 IN 	  t5050_invpick_assign_detail.c901_ref_type%TYPE
	  , p_user_id	 IN 	  t5051_allocate_user.c101_user_id%TYPE
	  , p_code_grp	 OUT	  VARCHAR
	  , p_code_id	 OUT	  VARCHAR
	  , p_status	 OUT	  VARCHAR
	)
	AS
		v_invpick_assign_id t5050_invpick_assign_detail.c5050_invpick_assign_id%TYPE;
		v_invpick_status t5050_invpick_assign_detail.c901_status%TYPE;
		v_allocated_user_id t5050_invpick_assign_detail.c101_allocated_user_id%TYPE;
		v_code_id	   t901_code_lookup.c901_code_id%TYPE;
		v_code_grp	   t901_code_lookup.c901_code_grp%TYPE;
		v_status	   VARCHAR2(10);
	BEGIN
		BEGIN
			SELECT t901.c901_code_id codeid, t901.c901_code_grp codegrp
			  INTO v_code_id, v_code_grp
			  FROM t901_code_lookup t901
			 WHERE c902_code_nm_alt = TO_CHAR (p_reftype) AND c901_code_grp IN ('INVPT', 'INVPW', 'CONTY');

			SELECT t5050.c5050_invpick_assign_id, t5050.c901_status, t5050.c101_allocated_user_id
			  INTO v_invpick_assign_id, v_invpick_status, v_allocated_user_id
			  FROM t5050_invpick_assign_detail t5050
			 WHERE t5050.c5050_ref_id = p_orderid AND t5050.c901_ref_type = v_code_id AND t5050.c5050_void_fl IS NULL;

			-- Get the current status of transaction
			BEGIN
			  select c901_type 
			    INTO v_status
			    from(
			       select c901_type 
			         from t5055_control_number 
			        where  c5055_ref_id=p_orderid
			          order by C5055_CONTROL_NUMBER_ID desc) 
				where rownum=1;
			EXCEPTION WHEN NO_DATA_FOUND THEN
			  v_status:='';
			END;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				GM_RAISE_APPLICATION_ERROR('-20999','183',''); 
		END;
    

		IF v_invpick_status = 93006   -- COMPLETED
		THEN
			GM_RAISE_APPLICATION_ERROR('-20999','343',''); 
		END IF;
    
    gm_pkg_op_inv_scan.gm_validate_device_access(p_reftype,p_user_id); --- Validate Device access Permission

		IF NVL (v_allocated_user_id, p_user_id) <> p_user_id 
		THEN
			GM_RAISE_APPLICATION_ERROR('-20999','184','');                 
		ELSIF v_invpick_status IS NULL AND v_code_grp = 'INVPT' OR v_code_grp = 'CONTY'
		THEN
			UPDATE t5050_invpick_assign_detail
			   SET c101_allocated_user_id = p_user_id
				 , c5050_allocated_dt = CURRENT_DATE
				 , c901_status = 93003
				 , c5050_last_updated_by = p_user_id
				 , c5050_last_updated_date = CURRENT_DATE
			 WHERE c5050_invpick_assign_id = v_invpick_assign_id
			   AND c5050_ref_id = p_orderid
			   AND c901_ref_type = v_code_id
			   AND c5050_void_fl IS NULL;
		END IF;

		p_code_id	:= v_code_id;
		p_code_grp	:= v_code_grp;
		p_status	:= v_status;
	END gm_sav_allocate_request;

/*	gm_allocation_procedure - This procedure will assign user's to the order's bwith respect to Sequence number's in T5051_ALLOCATE_USER and upates user for specific order in T5050_INVPICK_ASSIGN_DETAIL
	input: nothing
*/
	PROCEDURE gm_allocation_procedure
	AS
		v_user_id	   t5051_allocate_user.c5051_allocate_user_id%TYPE;
		v_id		   t5051_allocate_user.c5051_allocate_user_id%TYPE;
		v_total_active_users t5051_allocate_user.c5051_allocate_user_id%TYPE;
		v_start_dt	   DATE;

		/* Get all UnAssigned Order's */
		CURSOR orders_cursor
		IS
			SELECT	 t5050.c5050_invpick_assign_id ID, t5050.c901_ref_type TYPE, t5050.c101_allocated_user_id user_id
				   , t5050.c901_status status
				FROM t5050_invpick_assign_detail t5050, t901_code_lookup t901
			   WHERE t5050.c101_allocated_user_id IS NULL
				 AND t5050.c5050_void_fl IS NULL
				 -- While Assigning request, fetch the records that are not complete and having processed date as null.
				 -- This situation arise when the assignment is processed from the portal before it is allocated to user in device.
				 --Such Assignment should not be allocated .
				 -- For JIRA ID:IBC-40
				 AND NVL (t5050.c901_status, '93003') != '93006'
				 AND t5050.c5050_processed_dt IS NULL
				 AND t5050.c901_ref_type = t901.c901_code_id
				 AND t901.c901_code_grp IN ('CONTY', 'INVPT')
			ORDER BY t5050.c5050_invpick_assign_id;
	BEGIN
		/* Update the T5050, if any User's are Inactive */
		UPDATE t5050_invpick_assign_detail t5050
		   SET t5050.c101_allocated_user_id = ''
			 , t5050.c901_status = ''
		 WHERE t5050.c101_allocated_user_id IN (SELECT t5051.c101_user_id user_id
												  FROM t5051_allocate_user t5051
												 WHERE t5051.c901_status = 93026)
																				 -- Fetch the Orders/consignment which are only in "assigned" status.
			   AND t5050.c901_status = '93003';

		/* Intialize Variable */
		v_user_id	:= 0;
		v_total_active_users := 0;

		/* Get the Total Number of Active User's */
		SELECT COUNT (1)
		  INTO v_total_active_users
		  FROM t5051_allocate_user t5051
		 WHERE t5051.c901_status = 93025;

		/* Loop the Cursor - Order's */
		FOR order_value IN orders_cursor
		LOOP
			/* Check whether Any User's Active */
			IF (v_total_active_users <> 0)
			THEN
				/* Get User ID with Min Sequence Number */
				SELECT t5051.c5051_allocate_user_id ID, t5051.c101_user_id user_id
				  INTO v_id, v_user_id
				  FROM t5051_allocate_user t5051
				 WHERE t5051.c5051_allocated_sequence = (SELECT MIN (t5051.c5051_allocated_sequence)
														   FROM t5051_allocate_user t5051
														  WHERE t5051.c901_status = 93025) AND ROWNUM = 1;

				/* Update T5050 */
				UPDATE t5050_invpick_assign_detail t5050
				   SET t5050.c101_allocated_user_id = v_user_id
					 , t5050.c5050_allocated_dt = CURRENT_DATE
					 , t5050.c901_status = 93003
				 WHERE t5050.c5050_invpick_assign_id = order_value.ID;

				/* Update TimeStamp */
				UPDATE t5051_allocate_user t5051
				   SET c5051_last_job_allocated_time = CURRENT_TIMESTAMP
					 , c5051_allocated_sequence = s5051_alloc_seq.NEXTVAL
				 WHERE c5051_allocate_user_id = v_id;
			END IF;   /* End of If */
		END LOOP;	/* End of Cursor Looop */

		COMMIT;
	EXCEPTION
		WHEN OTHERS
		THEN
			ROLLBACK;
			--
			gm_pkg_cm_job.gm_cm_notify_load_info (v_start_dt
												, CURRENT_DATE
												, 'E'
												, SQLERRM
												, 'gm_pkg_allocation.gm_allocation_procedure'
												 );
			COMMIT;
	END gm_allocation_procedure;   /* End of Allocation Procedure */
  
    /********************************************************
	* This Procedure will allocate set request
  **********************************************************/
  PROCEDURE gm_sav_set_allocation(
     p_ref_id	    IN	VARCHAR2
    ,p_reftype    IN t5050_invpick_assign_detail.c901_ref_type%TYPE
    ,p_user_id    IN t5051_allocate_user.c101_user_id%TYPE 
    ,p_status     IN    t5050_invpick_assign_detail.c901_status%TYPE
    ,p_oper_type IN VARCHAR2
  )
  AS
  	v_cur_req	          TYPES.cursor_type;
		v_status	           VARCHAR2(10);
    v_tag_id             VARCHAR2(100);
    v_setid              VARCHAR2(100);
    v_cnid               VARCHAR2(100);
    v_locid              VARCHAR2(100);
    v_loccd              VARCHAR2(100);
    v_setdesc            VARCHAR2(100);
    v_etchid             VARCHAR2(100);
    v_ref_type           t5050_invpick_assign_detail.c901_ref_type%TYPE;   
    BEGIN
    SELECT  DECODE(p_reftype,4110,93601,4127,93602,p_reftype) 
    INTO v_ref_type 
    FROM DUAL;
    BEGIN
    gm_pkg_op_set_pick_rpt.gm_fch_pick_request_dtls(p_ref_id,p_reftype,v_cur_req);
    LOOP
    FETCH v_cur_req
    INTO v_tag_id, v_setid,v_cnid,v_locid,v_loccd,v_setdesc,v_etchid;
    EXIT WHEN v_cur_req%NOTFOUND;
   -- gm_sav_allocate_request(v_cnid,v_ref_type,p_user_id,v_code_grp,v_code_id,v_status);
    	UPDATE t5050_invpick_assign_detail
			   SET c101_allocated_user_id = p_user_id
				 , c5050_allocated_dt = CURRENT_DATE
				 , c901_status = p_status
				 , c5050_last_updated_by = p_user_id
				 , c5050_last_updated_date = CURRENT_DATE
			 WHERE c5050_ref_id = v_cnid
			   AND c901_ref_type = v_ref_type
			   AND c5050_void_fl IS NULL; 
    END LOOP;
    IF v_cur_req%ISOPEN THEN
  	 CLOSE v_cur_req;
    END IF;
    END;
    END gm_sav_set_allocation;
   /********************************************************
	* This Procedure will complete set request
  **********************************************************/    
   PROCEDURE gm_sav_set_complete(
     p_ref_id	    IN	VARCHAR2
    ,p_reftype    IN t5050_invpick_assign_detail.c901_ref_type%TYPE
    ,p_user_id    IN t5051_allocate_user.c101_user_id%TYPE 
    ,p_status     IN    t5050_invpick_assign_detail.c901_status%TYPE
  )  
  AS
   v_ref_type           t5050_invpick_assign_detail.c901_ref_type%TYPE;
  BEGIN
   SELECT  DECODE(p_reftype,4110,93601,26240144,93601,4127,93602,4119,93603,p_reftype) 
    INTO v_ref_type 
    FROM DUAL;
    UPDATE t5050_invpick_assign_detail
			   SET c101_allocated_user_id = p_user_id
				 , c5050_allocated_dt = CURRENT_DATE
				 , c901_status = p_status
				 , c5050_last_updated_by = p_user_id
				 , c5050_last_updated_date = CURRENT_DATE
			 WHERE c5050_ref_id = p_ref_id
			   AND c901_ref_type = v_ref_type
			   AND c5050_void_fl IS NULL;
  END gm_sav_set_complete;
END gm_pkg_allocation;
/
