/* Formatted on 2011/05/17 18:33 (Formatter Plus v4.8.0) */
-- @ "C:\database\Packages\Operations\gm_pkg_op_return.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_return
IS
--
 /*******************************************************
 * Purpose: This Procedure is used to fetch the
 * open transactions for the given distributor
 *******************************************************/
--
	PROCEDURE gm_op_fch_distopentrans (
		p_distid	   IN		t701_distributor.c701_distributor_id%TYPE
	  , p_cons_cur	   OUT		TYPES.cursor_type
	  , p_return_cur   OUT		TYPES.cursor_type
	  , p_trans_cur    OUT		TYPES.cursor_type
	)
--
	AS
	v_company_id 		t1900_company.c1900_company_id%TYPE;
	BEGIN
		
		SELECT get_compid_frm_cntx()
          INTO v_company_id 
          FROM DUAL;
        	
		OPEN p_cons_cur
		 FOR
			 SELECT c504_consignment_id ID, get_code_name (c504_type) TYPE
				  , NVL (c504_last_updated_date, c504_created_date) cdate
				  , DECODE (c504_status_fl
						  , 6, 'Pending Transfer'
						  , 0, 'Back Order'
						  , 1, 'Initated'
						  , 2, 'Pending Control Number'
						  , 3, 'Pending Shipment'
						   ) status
				  , c207_set_id setid
			   FROM t504_consignment
			  WHERE c701_distributor_id = p_distid 
			  AND c504_status_fl < 4 
			  AND c504_void_fl IS NULL 
			  AND c504_type = 4110
			  AND C1900_COMPANY_ID = v_company_id ;

		OPEN p_return_cur
		 FOR
			 SELECT c506_rma_id ID, get_code_name (c506_type) TYPE
				  , NVL (c506_last_updated_date, c506_created_date) cdate
				  , DECODE (c506_status_fl, 0, 'Initiated', 1, 'Pending Credit') status, c207_set_id setid
			   FROM t506_returns
			  WHERE c701_distributor_id = p_distid
				AND c506_type IN (3301, 3302)
				AND c506_status_fl = 1
				AND c506_void_fl IS NULL
				AND c506_reason <> 3311
				AND C1900_COMPANY_ID = v_company_id ;

		OPEN p_trans_cur
		 FOR
			 SELECT c920_transfer_id ID, get_code_name (c901_transfer_mode) TYPE
				  , c920_transfer_date cdate, DECODE (c920_status_fl, 1, 'Initiated') status
			   FROM t920_transfer
			  WHERE c920_status_fl < 2
				AND ((c920_from_ref_id = p_distid AND c901_transfer_type = 90300) OR c920_to_ref_id = p_distid)
				AND c920_void_fl IS NULL;
	--
	END gm_op_fch_distopentrans;

	---
/*******************************************************
 * Purpose: Procedure is used to load the open consignment
 * for the given distributor
 *******************************************************/
--
	PROCEDURE gm_op_fch_distopencons (
		p_distid	  IN	   t701_distributor.c701_distributor_id%TYPE
	  , p_consg_cur   OUT	   TYPES.cursor_type
	  , p_rule_cur	  OUT	   TYPES.cursor_type
	  , p_ra_cur	  OUT	   TYPES.cursor_type
	)
--
	AS
		v_company_id   VARCHAR2 (20);
		v_default_company T1900_COMPANY.C1900_COMPANY_ID%TYPE;
	BEGIN
		-- 4000338 (FS location type)
		-- 3 (FS Warehouse)
		BEGIN		
			SELECT  c1900_company_id
			INTO    v_company_id
			FROM    t701_distributor where C701_DISTRIBUTOR_ID =p_distid and C701_VOID_FL is null;
			EXCEPTION
				WHEN NO_DATA_FOUND
			THEN
				v_company_id := NULL;
				v_default_company := get_rule_value('DEFAULT_COMPANY','ONEPORTAL');
				
				IF v_company_id IS NULL THEN
					v_company_id := v_default_company;
				END IF;
		END;

		OPEN p_consg_cur
		 FOR
			 SELECT pnum, partdesc, price, cons_qty, ret_qty, pending_qty, total_value
			   FROM (SELECT t205.c205_part_number_id pnum, c205_part_num_desc partdesc, t2052.c2052_equity_price price
						  , NVL (consign, 0) cons_qty, NVL (cons_return, 0) ret_qty
						  , get_field_sales_net_con_qty(3, p_distid, t205.c205_part_number_id, 4000338) pending_qty
						  , (get_field_sales_net_con_qty(3, p_distid, t205.c205_part_number_id, 4000338) * t2052.c2052_equity_price) total_value
					   FROM t205_part_number t205 , t2052_part_price_mapping t2052
						  , (SELECT   t505.c205_part_number_id partnum, SUM (t505.c505_item_qty) consign
								 FROM t504_consignment t504, t505_item_consignment t505
								WHERE t504.c504_consignment_id = t505.c504_consignment_id
								  AND t504.c701_distributor_id = p_distid
								  AND c504_type IN (4110, 40057)
								  AND t504.c504_status_fl = 4
								  AND c504_void_fl IS NULL
								  AND TRIM (t505.c505_control_number) IS NOT NULL
							 GROUP BY t505.c205_part_number_id) cons
						  , (SELECT   t507.c205_part_number_id partnum, SUM (c507_item_qty) cons_return
								 FROM t506_returns t506, t507_returns_item t507
								WHERE t506.c506_rma_id = t507.c506_rma_id
								  AND t506.c701_distributor_id = p_distid
								  --AND t506.c506_type IN (3301, 3302, 3305, 3306)
								  AND t506.c506_void_fl IS NULL
								  AND t506.c506_status_fl = 2
								  AND TRIM (t507.c507_control_number) IS NOT NULL
								  AND t507.c507_status_fl IN ('C', 'W')
							 GROUP BY t507.c205_part_number_id) retn
					  WHERE t205.c205_part_number_id = cons.partnum AND t205.c205_part_number_id = retn.partnum(+)
					  AND t205.c205_part_number_id = t2052.c205_part_number_id(+)
					  AND t2052.c1900_company_id(+) = v_company_id
					  AND t2052.c2052_void_fl(+) is NULL)
			  WHERE pending_qty > 0;

		OPEN p_rule_cur
		 FOR
			  SELECT c906_rule_value emailto
			   FROM t906_rules
			  WHERE c906_rule_id = 'INIRA'
			    AND c906_rule_grp_id = 'EMAIL'
			    AND c1900_company_id = v_company_id
			    AND c906_void_fl IS NULL;

		OPEN p_ra_cur
		 FOR
			 SELECT t506.c506_rma_id raid, t506.c506_reason reason, c506_expected_date edate
				  , t506.c506_status_fl sfl
			   FROM t506_returns t506
			  WHERE t506.c701_distributor_id = p_distid
				AND t506.c506_type = 3306
				AND c506_void_fl IS NULL
				AND c506_status_fl IN (0, 1);
	END gm_op_fch_distopencons;

--
/*******************************************************
 * Purpose: Procedure is used to initiate a
 * Return for the distributor closure
 *******************************************************/
--
	PROCEDURE gm_op_sav_distclosure (
		p_distid	IN		 t701_distributor.c701_distributor_id%TYPE
	  , p_expdate	IN		 VARCHAR2
	  , p_reason	IN		 t506_returns.c506_reason%TYPE
	  , p_userid	IN		 t101_user.c101_user_id%TYPE
	  , p_rma_id	OUT 	 t506_returns.c506_rma_id%TYPE
	)
--
	AS
		v_ra_id 	   NUMBER;
		v_cnt		   NUMBER;
		v_id		   NUMBER;
		v_string	   VARCHAR2 (20);
		v_id_string    VARCHAR2 (20);
		v_empname	   NUMBER;
		subject 	   VARCHAR2 (100);
		mail_body	   VARCHAR2 (3000);
		crlf		   VARCHAR2 (2) := CHR (13) || CHR (10);
		to_mail 	   t906_rules.c906_rule_value%TYPE;
		v_user_name    VARCHAR2 (100);
		v_dist_name    VARCHAR2 (100);
		v_company_id  t1900_company.c1900_company_id%TYPE;
		v_plant_id 	  t5040_plant_master.c5040_plant_id%TYPE;
        v_default_company T1900_COMPANY.C1900_COMPANY_ID%TYPE;
		-- 4000338 (FS location type)
		-- 3 (FS Warehouse)
		
		CURSOR pop_val
		IS
			SELECT t5053.c205_part_number_id pnum,t5053.c5053_curr_qty pending_qty,t2052.c2052_equity_price price
			  FROM t5051_inv_warehouse t5051, t5052_location_master t5052, t5053_location_part_mapping t5053,t2052_part_price_mapping t2052
		      WHERE t5051.c5051_inv_warehouse_id = t5052.c5051_inv_warehouse_id
			AND t5052.c5052_location_id      = t5053.c5052_location_id
			AND t5051.c901_warehouse_type = 4000339 --Field Sales
			AND t5053.c205_part_number_id = t2052.c205_part_number_id
			AND t5052.c5052_ref_id           = p_distid
		       -- AND t5053.c205_part_number_id    = p_pnum
			AND t5052.c901_location_type     = 4000338 --'4000338' Field Sales
			AND t5052.c5052_void_fl         IS NULL
			AND NVL(t5053.c5053_curr_qty,0) > 0
			AND t2052.c1900_company_id(+) = v_company_id
			AND t2052.c2052_void_fl(+) is NULL;
	BEGIN
		
		BEGIN
		SELECT  c1900_company_id
    	INTO    v_company_id
    	FROM    t701_distributor where C701_DISTRIBUTOR_ID =p_distid and C701_VOID_FL is null;
    	EXCEPTION
 		WHEN NO_DATA_FOUND
    	THEN
      	v_company_id := NULL;
    	
    	v_default_company := get_rule_value('DEFAULT_COMPANY','ONEPORTAL');
    	
    	IF v_company_id IS NULL THEN
   		v_company_id := v_default_company;
   		END IF;
		END;
		
    	SELECT gm_pkg_op_plant_rpt.get_default_plant_for_comp(v_company_id)
    	INTO v_plant_id
    	FROM DUAL;
		
		-- moving this up to lock the distributor selection which will lock the count calculation as well. updated by Joe.
		SELECT	   c701_distributor_name
			  INTO v_dist_name
			  FROM t701_distributor
			 WHERE c701_distributor_id = p_distid
		FOR UPDATE;

		SELECT COUNT (t506.c506_rma_id)
		  INTO v_cnt
		  FROM t506_returns t506
		 WHERE t506.c701_distributor_id = p_distid
		   AND t506.c506_type = 3306
		   AND t506.c506_void_fl IS NULL
		   AND t506.c506_status_fl IN (1);

		IF v_cnt <> 0
		THEN
			--
			SELECT t506.c506_rma_id
			  INTO p_rma_id
			  FROM t506_returns t506
			 WHERE t506.c701_distributor_id = p_distid AND t506.c506_type = 3306;

			raise_application_error (-20031, p_rma_id);
		--
		END IF;

		UPDATE t506_returns
		   SET c506_void_fl = 'Y'
		     , c506_last_updated_by = p_userid
			 , c506_last_updated_date = CURRENT_DATE
		 WHERE c701_distributor_id = p_distid
		   AND c506_type IN (3301, 3302)
		   AND c506_status_fl = 0
		   AND c506_void_fl IS NULL
		   AND c506_reason <> 3311;

		SELECT 'GM-RA-' || s506_return.NEXTVAL
		  INTO v_string
		  FROM DUAL;

		INSERT INTO t506_returns
					(c506_rma_id, c701_distributor_id, c506_type, c506_reason, c506_expected_date, c506_status_fl
				   , c506_created_by, c506_created_date, c1900_company_id, c5040_plant_id
					)
			 VALUES (v_string, p_distid, 3306, p_reason, TO_DATE (p_expdate, NVL(get_compdtfmt_frm_cntx(),'mm/dd/yyyy')), '0'
				   , p_userid, CURRENT_DATE, v_company_id, v_plant_id
					);

		FOR rad_val IN pop_val
		LOOP
			INSERT INTO t507_returns_item
						(c507_returns_item_id, c506_rma_id, c205_part_number_id, c507_item_qty, c507_item_price
					   , c507_status_fl
						)
				 VALUES (s507_return_item.NEXTVAL, v_string, rad_val.pnum, rad_val.pending_qty, rad_val.price
					   , 'Q'
						);

			INSERT INTO t507_returns_item
						(c507_returns_item_id, c506_rma_id, c205_part_number_id, c507_item_qty, c507_item_price
					   , c507_status_fl
						)
				 VALUES (s507_return_item.NEXTVAL, v_string, rad_val.pnum, rad_val.pending_qty, rad_val.price
					   , 'R'
						);
		END LOOP;

		UPDATE t701_distributor
		   SET c701_active_fl = 'N'
		     , c701_last_updated_by = P_Userid
			 , c701_last_updated_date = CURRENT_DATE 
		 WHERE c701_distributor_id = p_distid;

		to_mail 	:= get_rule_value_by_company ('INIRA', 'EMAIL',v_company_id);
		v_user_name := get_user_name (p_userid);
		subject 	:= 'Distributor closure - ' || v_dist_name;
		mail_body	:=
			   'RETURN INITIATED FOR DISTRIBUTOR CLOSURE'
			|| crlf
			|| 'DISTTRIBUTOR NAME :- '
			|| v_dist_name
			|| crlf
			|| 'RA NO :- '
			|| v_string
			|| crlf
			|| 'INITIATED BY :- '
			|| v_user_name
			|| crlf
			|| 'INITIATED DATE	:- '
			|| TO_CHAR (CURRENT_DATE, 'dd/mm/yyyy hh24:mi:ss');
		gm_com_send_email_prc (to_mail, subject, mail_body);
		p_rma_id	:= v_string;
		-- Save Return status : Distclosure  using 91150 initiate
		gm_save_status_details (v_string, '', p_userid, NULL, CURRENT_DATE, '91150', '91100');
	END gm_op_sav_distclosure;

--
/*******************************************************
 * Purpose: Procedure is used to fetch
 * parts for the given sets, set quantities and part numbers
 *******************************************************/
--
	PROCEDURE gm_op_fch_return_info (
		p_distid	 IN 	  t701_distributor.c701_distributor_id%TYPE
	  , p_set_id	 IN 	  VARCHAR2
	  , p_qty		 IN 	  VARCHAR2
	  , p_part_id	 IN 	  VARCHAR2
	  , p_emp_id	 IN 	  t504_consignment.c704_account_id%TYPE
	  , p_type		 IN		  t506_returns.c506_type%TYPE
	  , p_part_cur	 OUT	  TYPES.cursor_type
	)
--
	AS
		v_ware_house 		t906_rules.c906_rule_value%TYPE;
    	v_ware_house_type   t906_rules.c906_rule_value%TYPE;
    	v_req_type			t506_returns.c506_type%TYPE;
		v_company_id 		t1900_company.c1900_company_id%TYPE;
    	v_comp_map_type     VARCHAR2(4000);
	BEGIN
		SELECT get_compid_frm_cntx()
        	INTO v_company_id FROM DUAL;
-- To assign set and part information
		my_context.set_my_double_inlist_ctx (p_set_id, p_qty);
-- To assign part information
		my_context.set_my_inlist_ctx (p_part_id);

		-- 3304: Account consignment item
		SELECT DECODE(p_type, '3304', p_type, NULL) INTO v_req_type FROM dual;
		--421: Sales Rep
		v_ware_house      := get_rule_value(NVL(v_req_type,'4121'),'RULEWH'); -- to get the warehouse based on the item return type
    	v_ware_house_type := get_rule_value(NVL(v_req_type,'4121'),'RULEWHTYPE'); -- to get the warehouse type based on the item return type
		
    	my_context.set_my_cloblist (get_rule_value('ALL','PART_MAP_TYPE')|| ',');
		IF p_distid = '01'
		THEN
		OPEN p_part_cur
		 FOR
			 SELECT pnum, partdesc, set_qty, pending_qty, '1' chkbox, DECODE (set_qty, NULL, 0, set_qty) retqty
			   FROM (SELECT c205_part_number_id pnum, c205_part_num_desc partdesc, NVL (consign, 0) cons_qty
						  , (NVL (consign, 0) - NVL (cons_return, 0)) pending_qty
						  , DECODE (gm_pkg_op_return.get_op_fch_set_qty (c205_part_number_id)
								  , 0, (NVL (consign, 0) - NVL (cons_return, 0))
								  , NVL (consign, 0)
								   ) retqty
						  , gm_pkg_op_return.get_op_fch_set_qty (c205_part_number_id) set_qty
					   FROM t205_part_number t205
						  , (SELECT   t505.c205_part_number_id partnum, SUM (t505.c505_item_qty) consign
								 FROM t504_consignment t504, t505_item_consignment t505
								WHERE t504.c504_consignment_id = t505.c504_consignment_id
								  -- AND t504.c701_distributor_id = p_distid
								  AND DECODE (p_distid, '01', t504.c704_account_id, c701_distributor_id) = p_distid
								  AND NVL (t504.c504_ship_to_id, -999) =
													  DECODE (p_distid
															, '01', p_emp_id
															, NVL (t504.c504_ship_to_id, -999)
															 )
								  AND c504_type IN (4110, 4112)
								  AND t504.c504_status_fl = 4
								  AND c504_void_fl IS NULL
								  AND TRIM (t505.c505_control_number) IS NOT NULL
							 GROUP BY t505.c205_part_number_id) cons
						  , (SELECT   t507.c205_part_number_id partnum, SUM (c507_item_qty) cons_return
								 FROM t506_returns t506, t507_returns_item t507
								WHERE t506.c506_rma_id = t507.c506_rma_id
								  -- AND t506.c701_distributor_id = p_distid
								  AND DECODE (p_distid, '01', t506.c704_account_id, t506.c701_distributor_id) = p_distid
								  AND DECODE (p_distid, '01', t506.c101_user_id, '01') =
																				 DECODE (p_distid
																					   , '01', p_emp_id
																					   , '01'
																						)
								  AND t506.c506_type IN (3301, 3302)
								  AND t506.c506_void_fl IS NULL
								  AND t506.c506_status_fl = 2
								  AND TRIM (t507.c507_control_number) IS NOT NULL
								  AND t507.c507_status_fl IN ('C', 'W')
							 GROUP BY t507.c205_part_number_id) retn
					  WHERE t205.c205_part_number_id = cons.partnum(+) AND t205.c205_part_number_id = retn.partnum(+))
			  WHERE (pnum IN (SELECT c205_part_number_id
								FROM t208_set_details, v_double_in_list
							   WHERE c207_set_id = token AND c208_void_fl IS NULL) OR (pnum IN (SELECT token
																		FROM v_in_list)
								AND pnum IN (SELECT t2023.c205_part_number_id 
					 				FROM t2023_part_company_mapping t2023 	 
					  				   WHERE pnum IN  (SELECT token FROM v_in_list)
											 AND t2023.c2023_void_fl IS NULL 
											 AND t2023.c1900_company_id = v_company_id 
											 AND t2023.c901_part_mapping_type IN (select * from  v_clob_list))));
		ELSE -- Distributor id (to get the FS warehouse qty)
		-- 4000338 (FS location type)
		-- 3 (FS Warehouse)
		OPEN p_part_cur
		 FOR
			 SELECT pnum, partdesc, NVL(set_qty,0) set_qty, pending_qty, '1' chkbox, DECODE (set_qty, NULL, 0, set_qty) retqty
			   FROM (SELECT c205_part_number_id pnum, c205_part_num_desc partdesc
						  , get_field_sales_net_con_qty(v_ware_house, p_distid, c205_part_number_id, v_ware_house_type) pending_qty
						  , DECODE (gm_pkg_op_return.get_op_fch_set_qty (c205_part_number_id)
								  , 0, get_field_sales_net_con_qty(v_ware_house, p_distid, c205_part_number_id, v_ware_house_type)
								  , get_field_sales_net_con_qty(v_ware_house, p_distid, c205_part_number_id, v_ware_house_type)
								   ) retqty
						  , gm_pkg_op_return.get_op_fch_set_qty (c205_part_number_id) set_qty
					   FROM t205_part_number t205)
			  WHERE (pnum IN (SELECT c205_part_number_id
								FROM t208_set_details, v_double_in_list
							   WHERE c207_set_id = token AND c208_void_fl IS NULL) OR (pnum IN (SELECT token
																		FROM v_in_list)
								AND pnum IN (SELECT t2023.c205_part_number_id 
					 				FROM t2023_part_company_mapping t2023 	 
					  				   WHERE pnum IN  (SELECT token FROM v_in_list)
											 AND t2023.c2023_void_fl IS NULL 
											 AND t2023.c1900_company_id = v_company_id 
											 AND t2023.c901_part_mapping_type IN (select * from  v_clob_list))));
		END IF;																
	END gm_op_fch_return_info;

--
/*******************************************************
 * Purpose: function is used to fetch
 * the quantity of part in a set for the given part number
 *******************************************************/
--
	FUNCTION get_op_fch_set_qty (
		p_part_id	t205_part_number.c205_part_number_id%TYPE
	)
		RETURN NUMBER
	IS
--
		v_set_qty	   NUMBER;
	BEGIN
		SELECT SUM (t208.c208_set_qty * vdi.tokenii)
		  INTO v_set_qty
		  FROM t208_set_details t208, v_double_in_list vdi
		 WHERE t208.c207_set_id = vdi.token AND t208.c205_part_number_id = p_part_id AND t208.c208_void_fl is null;  -- PC-5134 Returns Qty doubled for sets created using the Copy Set process

		RETURN v_set_qty;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN 0;
	END get_op_fch_set_qty;

--
/*******************************************************
 * Purpose: Procedure is used to initiate a
 * RA for the Consignment Set / Item Return
 *******************************************************/
--
	PROCEDURE gm_op_sav_return (
		p_distid	 IN 	  t701_distributor.c701_distributor_id%TYPE
	  , p_expdate	 IN 	  VARCHAR2
	  , p_reason	 IN 	  t506_returns.c506_reason%TYPE
	  , p_userid	 IN 	  t101_user.c101_user_id%TYPE
	  , p_type		 IN 	  t506_returns.c506_type%TYPE
	  , p_id		 IN 	  t506_returns.c101_user_id%TYPE
	  , p_part_qty	 IN 	  VARCHAR2
	  , p_part_id	 IN 	  VARCHAR2
	  , p_set_id	 IN 	  t506_returns.c207_set_id%TYPE
	  , p_rma_id	 OUT	  t506_returns.c506_rma_id%TYPE
	)
--
	AS
		v_string	   VARCHAR2 (20);
		v_dist_id	   VARCHAR2 (20);
		v_acc_id	   VARCHAR2 (20) := '';
		v_clsr_count   NUMBER;
		v_dist_name    t701_distributor.c701_distributor_name%TYPE;
		v_id			t506_returns.c101_user_id%TYPE;
		v_default_company T1900_COMPANY.C1900_COMPANY_ID%TYPE;
        v_company_id  t1900_company.c1900_company_id%TYPE;
		v_plant_id 	  t5040_plant_master.c5040_plant_id%TYPE;
		CURSOR v_part_cur
		IS
			SELECT token pnum, tokenii qty, t2052.c2052_equity_price price
			FROM v_double_in_list vlist, t205_part_number t205 ,t2052_part_price_mapping t2052
			WHERE t205.c205_part_number_id = vlist.token
			AND t205.c205_part_number_id = t2052.c205_part_number_id(+) 
			AND t2052.c1900_company_id(+) = v_company_id
			AND t2052.c2052_void_fl(+) is NULL;
	BEGIN
		
		-- Get the company id from context
		SELECT  NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL'))
			INTO  v_default_company 
		FROM DUAL;
					
		BEGIN
			SELECT  c1900_company_id
	    	INTO    v_company_id
	    	FROM    t701_distributor where c701_distributor_id =p_distid and C701_VOID_FL IS NULL;
    	EXCEPTION
 		WHEN NO_DATA_FOUND
    	THEN
    		BEGIN
				SELECT  c1900_company_id
		    	INTO    v_company_id
		    	FROM    t704_account where c704_account_id =p_id and c704_void_fl IS NULL;
		    	EXCEPTION
		 		WHEN NO_DATA_FOUND
		    	THEN
		      	v_company_id := NULL;    	
		    	
		   	END;
		END;
		-- IF company id is null, then save the default company id
		IF v_company_id IS NULL THEN
			v_company_id := v_default_company;
		END IF;		
		
    	/*SELECT gm_pkg_op_plant_rpt.get_default_plant_for_comp(v_company_id)
    	INTO v_plant_id
    	FROM DUAL;*/
		
		SELECT  get_plantid_frm_cntx()
	  	  INTO v_plant_id
	  	  FROM dual;
		
		-- this code to lock the distributor selection which will lock the count calculation as well. updated by Joe.
		IF p_distid <> '01'
		THEN
			SELECT	   c701_distributor_name
				  INTO v_dist_name
				  FROM t701_distributor
				 WHERE c701_distributor_id = p_distid
			FOR UPDATE;

			SELECT COUNT (1)
			  INTO v_clsr_count
			  FROM t506_returns
			 WHERE c701_distributor_id = p_distid AND c506_type = '3306' AND c506_void_fl IS NULL
				   AND c506_status_fl <> 2;

			IF v_clsr_count > 0
			THEN
				raise_application_error (-20041, '');
			END IF;
		END IF;

-- To assign set and part information
		my_context.set_my_double_inlist_ctx (p_part_id, p_part_qty);

		SELECT 'GM-RA-' || s506_return.NEXTVAL
		  INTO v_string
		  FROM DUAL;

		IF p_distid = '01'
		THEN
			v_dist_id	:= '';
			v_acc_id	:= '01';
		ELSE
			v_dist_id	:= p_distid;
		END IF;

		-- get the distributor id or account id based on the returns type
		 SELECT DECODE(p_type,'3304',to_char(p_id), to_char(v_acc_id)) INTO v_acc_id FROM DUAL; 
		-- get the sales rep id based on the returns type
		SELECT DECODE(p_type,'3304',null,p_id) INTO v_id FROM DUAL;
		
		INSERT INTO t506_returns
					(c506_rma_id, c701_distributor_id, c506_type, c506_reason, c506_expected_date, c506_status_fl
				   , c704_account_id, c101_user_id, c207_set_id, c506_created_by, c506_created_date
				   , c703_sales_rep_id, c1900_company_id, c5040_plant_id
					)
			 VALUES (v_string, v_dist_id, p_type, p_reason, TO_DATE (p_expdate, NVL(get_compdtfmt_frm_cntx(),'mm/dd/yyyy')), '0'
				   , v_acc_id, DECODE (p_distid, '01', v_id, NULL), p_set_id, p_userid, CURRENT_DATE
				   , DECODE (p_distid, '01', NULL, v_id), v_company_id, v_plant_id
					);

/*
		INSERT INTO t506_returns
					(c506_rma_id, c701_distributor_id, c506_type, c506_reason, c506_expected_date, c506_status_fl
				   , c704_account_id, c101_user_id, c207_set_id, c506_created_by, c506_created_date, c703_sales_rep_id
					)
			 VALUES (v_string, v_dist_id, p_type, p_reason, TO_DATE (p_expdate, NVL(get_rule_value('DATEFMT', 'DATEFORMAT'),'mm/dd/yyyy')), '0'
				   , v_acc_id, p_emp_id, p_set_id, p_userid, CURRENT_DATE, v_rep_id
					);
*/
		FOR rad_val IN v_part_cur
		LOOP
			INSERT INTO t507_returns_item
						(c507_returns_item_id, c506_rma_id, c205_part_number_id, c507_item_qty, c507_item_price
					   , c507_status_fl
						)
				 VALUES (s507_return_item.NEXTVAL, v_string, rad_val.pnum, rad_val.qty, rad_val.price
					   , 'Q'
						);

			INSERT INTO t507_returns_item
						(c507_returns_item_id, c506_rma_id, c205_part_number_id, c507_item_qty, c507_item_price
					   , c507_status_fl
						)
				 VALUES (s507_return_item.NEXTVAL, v_string, rad_val.pnum, rad_val.qty, rad_val.price
					   , 'R'
						);
						
			IF (p_reason = '3253' OR p_reason = '26240385') THEN
				gm_pkg_qa_com_rsr_txn.gm_sav_ra_incident_link(v_string,rad_val.pnum, rad_val.qty,p_userid);
			END IF;
		END LOOP;

		--Save Return status: Initiate
		gm_save_status_details (v_string, '', p_userid, NULL, CURRENT_DATE, '91150', '91100');	--91100 'Return'; 91150 Initiate
		p_rma_id	:= v_string;
	END gm_op_sav_return;

/********************************************************************************************
 * Description	:This procedure is used for processing Returns
 *********************************************************************************************/
--
	PROCEDURE gm_op_sav_return_accp (
		p_raid		IN		 t506_returns.c506_rma_id%TYPE
	  , p_str		IN		 VARCHAR2
	  , p_retdate	IN		 VARCHAR2
	  , p_flag		IN		 VARCHAR2
	  , p_userid	IN		 t506_returns.c506_last_updated_by%TYPE
	  , p_errmsg	OUT 	 VARCHAR2
	)
	AS
--
		CURSOR v_returns_item_cur
		IS
			SELECT	 t507.c506_rma_id raid, t507.c205_part_number_id pnum, t507.c507_control_number cnum
				FROM t507_returns_item t507
			   WHERE t507.c506_rma_id = p_raid
				 AND t507.c507_status_fl <> 'Q'
				 AND TRIM (t507.c507_control_number) IS NOT NULL
				 AND get_part_attribute_value (t507.c205_part_number_id, 92340) = 'Y'
			GROUP BY t507.c506_rma_id, t507.c205_part_number_id, t507.c507_control_number;

		v_strlen	   NUMBER := NVL (LENGTH (p_str), 0);
		v_string	   VARCHAR2 (30000) := p_str;
		v_pnum		   VARCHAR2 (20);
		v_qty		   NUMBER;
		v_control	   t507_returns_item.c507_control_number%TYPE;
		v_orgcontrol   VARCHAR2 (20);
		v_substring    VARCHAR2 (1000);
		v_msg		   VARCHAR2 (1000);
		v_price 	   VARCHAR2 (20);
		v_verify_date  DATE;
		v_fl		   t506_returns.c207_set_id%TYPE;
		v_partqtyorig  NUMBER;
		v_partqty	   NUMBER;
		v_partnum	   VARCHAR2 (20);
		v_diffinnum    NUMBER;
		v_itemtype	   NUMBER;
--
		v_order_id	   t506_returns.c501_order_id%TYPE;
		v_reason	   t506_returns.c506_reason%TYPE;
		v_items 	   t507_returns_item%ROWTYPE;
--
		e_void_exp	   EXCEPTION;	-- Exception for checking if the Void flag is NULL
		v_void_fl	   VARCHAR2 (10);
		v_status_fl    VARCHAR2 (10);
		v_cnt_qty	   NUMBER;
		v_type		   t507_returns_item.c901_type%TYPE;
		v_dist_id	   t506_returns.c701_distributor_id%TYPE;
		v_errmsg	   VARCHAR2 (4000);
		v_msgout	   VARCHAR2 (4000);
		v_rec_count    NUMBER;
		v_cnt		   NUMBER := 0;
		v_mis_count    NUMBER;
		v_item_count   NUMBER;
		v_rtn_type	   t506_returns.c506_type%TYPE;
		--v_tag_count    NUMBER; 
		v_parent_raid  t506_returns.c506_rma_id%TYPE;
		v_transid		VARCHAR2(20) := '';
		v_unit_price   VARCHAR2(50);
		v_unit_price_adj   VARCHAR2(50);
		v_adj_code   VARCHAR2(50);
		v_list_price  VARCHAR2(20);
		v_do_unit_price		t507_returns_item.C507_DO_UNIT_PRICE%TYPE;
		v_adj_code_val		t507_returns_item.C507_ADJ_CODE_VALUE%TYPE;
		v_item_ord_id		t502_item_order.c502_item_order_id%TYPE;
		v_adj_code_per		VARCHAR2(10);
		v_acc_id			t501_order.c704_account_id%TYPE;
		v_party_id			t740_gpo_account_mapping.c101_party_id%TYPE;
		v_orderid			t501_order.c501_order_id%TYPE;
		v_flag				VARCHAR2(1) := 'N';
		v_err_cnt			NUMBER;
		v_comp_id			t1900_company.c1900_company_id%TYPE;
		v_acct_id			t704_account.c704_account_id%TYPE;
	BEGIN
		p_errmsg	:= 'RA NOT SAVED';

		SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL'))
			INTO  v_comp_id FROM DUAL;
		--
		SELECT c506_void_fl, c506_status_fl, c207_set_id, c501_order_id, c506_reason, c701_distributor_id
				 , c506_type, c506_parent_rma_id, c704_account_id
			  INTO v_void_fl, v_status_fl, v_fl, v_order_id, v_reason, v_dist_id
				 , v_rtn_type, v_parent_raid, v_acct_id
			  FROM t506_returns
			 WHERE c506_rma_id = p_raid
		FOR UPDATE;

		IF v_void_fl IS NOT NULL
		THEN
			-- Error message is Returns already Voided.
			raise_application_error (-20021, '');
		END IF;

		IF v_status_fl = 1
		THEN
			raise_application_error (-20036, '');
		END IF;

		IF (v_strlen = 0)
		THEN
			RETURN;
		END IF;

		SELECT COUNT (1)
		  INTO v_mis_count
		  FROM t507_returns_item
		 WHERE c506_rma_id = p_raid AND c507_missing_fl = 'Y';

		--
		IF (v_mis_count = 0)
		THEN
			DELETE FROM t507_returns_item
				  WHERE c506_rma_id = p_raid AND c507_status_fl <> 'Q';
		ELSE
			DELETE FROM t507_returns_item
				  WHERE c506_rma_id = p_raid
					AND c507_status_fl <> 'Q'
					AND c205_part_number_id NOT IN (SELECT c205_part_number_id
													  FROM t205d_part_attribute
													 WHERE c901_attribute_type = 92340);
		END IF;

		v_errmsg	:= '';

		--v_mis_count := 0;

		--insert new value into returns table
		WHILE INSTR (v_string, '|') <> 0
		LOOP
			--
			v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
			v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
			v_pnum		:= NULL;
			v_qty		:= NULL;
			v_control	:= NULL;
			v_orgcontrol := NULL;
			v_itemtype	:= NULL;
			v_price     := NULL;
			v_unit_price := NULL;
			v_unit_price_adj := NULL;
			v_adj_code := NULL;
			v_pnum		:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_qty		:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_control	:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_orgcontrol := SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_itemtype	:= TO_NUMBER (SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1));
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_price	:= TO_NUMBER (SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1));
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_unit_price	:= TO_NUMBER (SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1));
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_unit_price_adj	:= TO_NUMBER (SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1));
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_adj_code	:=  TO_NUMBER(v_substring);

			--
				-- If Consignment get the Equity Value
				-- or get the order value
			IF(v_price IS NULL)THEN
			IF (v_order_id IS NULL)
			THEN
				v_price 	:= get_part_price (v_pnum, 'E');
				v_list_price:= get_part_price (v_pnum, 'L');
			ELSE
				SELECT c502_item_price , c502_list_price
				  INTO v_price , v_list_price
				  FROM t502_item_order
				 WHERE c501_order_id = v_order_id AND c205_part_number_id = v_pnum AND C502_VOID_FL IS NULL AND ROWNUM = 1;
			END IF;
			END IF;
			
			BEGIN
			     SELECT c704_account_id, c501_order_id  into v_acc_id, v_orderid
			     FROM t501_order
			     WHERE c506_rma_id = p_raid
			     AND c501_void_fl IS NULL;
			     EXCEPTION WHEN NO_DATA_FOUND THEN
			      	v_acc_id := NULL;
			END;
			BEGIN
			     SELECT t740.c101_party_id 
			     	INTO v_party_id
			     FROM t740_gpo_account_mapping t740 
			     WHERE t740.c704_account_id = v_acc_id
			     AND c740_void_fl IS NULL;
			     EXCEPTION WHEN NO_DATA_FOUND THEN
			      	v_party_id := NULL;
			END;
			v_adj_code_per := gm_pkg_sm_adj_txn.get_adj_code_value(v_orderid,v_pnum,v_item_ord_id, v_adj_code);
			v_adj_code_val	:= ((NVL(v_unit_price,0) - NVL(v_unit_price_adj,0)) * NVL(v_adj_code_per,0)/100);
			-- Get the unit price using below formula, net unit price + unit adjustment value + adjustment code value
	        --v_acc_price := NVL(v_price,0) + NVL(v_unit_price_adj,0) + NVL(v_adj_code_val,0);
			v_do_unit_price := NVL(gm_pkg_sm_adj_rpt.get_account_part_unitprice(v_acc_id,v_pnum, v_party_id),0);
			
			
			--
			IF (v_mis_count = 0 OR get_part_attribute_value (v_pnum, 92340) <> 'Y')
			THEN
				IF (v_qty > 0)
				THEN
					INSERT INTO t507_returns_item
								(c507_returns_item_id, c506_rma_id, c205_part_number_id, c507_control_number
							   , c507_item_qty, c507_item_price,c507_org_control_number, c507_status_fl, c901_type
							   ,C507_UNIT_PRICE,C507_UNIT_PRICE_ADJ_VALUE ,C901_UNIT_PRICE_ADJ_CODE , C507_LIST_PRICE
							   , C507_DO_UNIT_PRICE, C507_ADJ_CODE_VALUE
								)
						 VALUES (s507_return_item.NEXTVAL, p_raid, v_pnum, v_control
							   , v_qty, v_price, NVL (v_orgcontrol, '-'),'R', v_itemtype 
							   , v_unit_price , v_unit_price_adj , v_adj_code , v_list_price
							   , v_do_unit_price, v_adj_code_val
								);

					UPDATE t507_returns_item
					   SET c507_control_number = v_control
					 WHERE c506_rma_id = p_raid AND c205_part_number_id = v_pnum AND c507_status_fl = 'Q';
					 
					 -- Check whether the validation is applicable for the part number or not, 104480: Lot tracking required ?
					v_flag := get_part_attr_value_by_comp(v_pnum,'104480',v_comp_id);
				    -- Get the error count from the t507_returns_item table to update the lot status based on that
				    SELECT count(1) INTO v_err_cnt
						FROM t507_returns_item
					WHERE c506_rma_id    = p_raid
						AND c507_status_fl <> 'Q'
						AND c507_error_dtls IS NOT NULL
						AND c507_control_number IS NOT NULL;
				  
					-- If the Release check box is checked and no validation available for the lot, the change the status to controlled 
					IF p_flag = 1 AND v_err_cnt = 0 AND v_flag = 'Y' THEN
						UPDATE t2550_part_control_number
						SET C901_LOT_CONTROLLED_STATUS = '105008' --Controlled
						  , c2550_last_updated_by = p_userid
						  , c2550_last_updated_date = CURRENT_DATE
						WHERE c205_part_number_id = v_pnum
							AND c2550_control_number = v_control;
					END IF;
					 
				END IF;
			END IF;
		END LOOP;
		
 		--When ever the child is updated/Inserted, the parent table has to be updated ,so ETL can Sync it to GOP.
		IF v_parent_raid IS NOT NULL
		THEN
		UPDATE t506_returns
		   SET C506_LAST_UPDATED_BY = p_userid, 
		       C506_LAST_UPDATED_DATE = CURRENT_DATE
		WHERE C506_rma_Id = v_parent_raid;
		END IF;
		
		--Unable to receive the returned sets that have more than one taggable part [Commenting the below section]
	 	/*IF (v_rtn_type = '3301')
		THEN
			SELECT COUNT (1)
			  INTO v_tag_count
			  FROM t507_returns_item t507
			 WHERE c506_rma_id = p_raid
			   AND t507.c507_status_fl <> 'Q'
			   AND get_part_attribute_value (t507.c205_part_number_id, 92340) = 'Y';

			IF (v_tag_count > 1)
			THEN
				raise_application_error (-20218, '');
			END IF;
		END IF;*/

		FOR item_cur IN v_returns_item_cur
		LOOP
			IF (get_part_attribute_value (item_cur.pnum, 92340) = 'Y' AND p_flag = 1 AND item_cur.cnum IS NOT NULL AND v_order_id IS NULL)
			THEN
				gm_tag_return_ra (p_raid, item_cur.pnum, item_cur.cnum, p_userid, p_flag, v_msgout);
				v_errmsg	:= v_errmsg || ' ' || v_msgout;
			END IF;
		END LOOP;

		-- to check if the order qty or inhouse qty matchs the init qty
		-- Added Acct ID check to avoid this condition, else it will throw error if Initiated and return qty are different
		IF (v_acct_id is NOT null AND v_rtn_type='3304' AND v_order_id IS null)
		   THEN 
		   v_cnt_qty := 1;
		
		
		ELSIF (v_order_id IS NOT NULL OR v_dist_id IS NULL) AND (v_rtn_type NOT IN ('3308','3309')) 
		
		THEN
			IF p_flag = 1
			THEN
				SELECT COUNT (*)
				  INTO v_cnt_qty
				  FROM (SELECT SUM (DECODE (c507_status_fl, 'Q', c507_item_qty, 0)) ini_qty
							 , SUM (DECODE (c507_status_fl, 'R', c507_item_qty, 0)) ret_qty
							 , SUM (DECODE (c507_status_fl, 'R', (DECODE (TRIM (c507_control_number), NULL, 1, 0)), 0)
								   ) cntrl_number
						  FROM t507_returns_item
						 WHERE c506_rma_id = p_raid)
				 WHERE ini_qty = ret_qty AND cntrl_number = 0;

				IF v_cnt_qty = 0
				THEN
					raise_application_error (-20039, '');
				END IF;
			END IF;
		END IF;

		UPDATE t506_returns
		   SET c506_return_date = TO_DATE (p_retdate, NVL(get_compdtfmt_frm_cntx(),'mm/dd/yyyy'))
			 , c506_last_updated_by = p_userid
			 , c506_last_updated_date = CURRENT_DATE
		 WHERE c506_rma_id = p_raid;

		/*SELECT c901_type
			INTO v_type
			FROM t507_returns_item
		 WHERE c506_rma_id = p_raid AND c507_status_fl = 'Q' AND ROWNUM = 1;


		UPDATE t507_returns_item
			 SET c901_type = v_type
		 WHERE c506_rma_id = p_raid AND c507_status_fl <> 'Q';
		 */
		--
		-- If the uses checks the 'completed' checkbox, then the following code is executed
		--
		IF p_flag = '1'
		THEN
			--
			UPDATE t506_returns
			   SET c506_status_fl = 1
				 , c506_last_updated_by = p_userid
				 , c506_last_updated_date = CURRENT_DATE
			 WHERE c506_rma_id = p_raid;

			-- Save "Return" status: Completed
			gm_save_status_details (p_raid, ' ', p_userid, NULL, CURRENT_DATE, '91151', '91100');   --91151 completed; 91100 return

			--
			DELETE		t507_returns_item
				  WHERE c506_rma_id = p_raid AND TRIM (c507_control_number) IS NULL AND c507_status_fl <> 'Q';
			
		--
		ELSE
			-- if unchecks the 'completed' checkbox, that's mean 'Control'
			--Save "Return" status: control
			gm_save_status_details (p_raid, ' ', p_userid, NULL, CURRENT_DATE, '91155', '91100');   --91155 Control; 91100 return
		END IF;

		-- Changed the status to Payment Received
		-- This is applicable only if its a duplicate order
		-- 3312 maps to Duplicate order
		IF ((v_order_id IS NOT NULL) AND (v_reason = 3312))
		THEN
			UPDATE t501_order
			   SET c501_status_fl = '3'
			     , c501_last_updated_by = p_userid
				 , c501_last_updated_date = CURRENT_DATE
			 WHERE c501_order_id = v_order_id;
		END IF;

		--IF p_flag <> '1'
		--THEN
		-- 3308 & 3309 - IH & Product Loaner Item - No Need to create the missing QTy, This missing Qty will becomes IHMP
		IF v_rtn_type NOT IN ('3308','3309')
		THEN

				gm_pkg_op_return.gm_op_sav_check_qr (p_raid, v_order_id);
		ELSE
			IF p_flag = '1'
			THEN
				gm_pkg_op_ihloaner_item_txn.gm_sav_inih_missing_item(p_raid,v_rtn_type,p_userid,v_transid);
				--IHMP Transaction Post without Job Run
				gm_pkg_op_loaner.gm_post_inih_missing_item(v_transid);   --9112 ihmp transaction type
			END IF;
		END IF;

		/*	SELECT COUNT (1)
			  INTO v_item_count
			  FROM t507_returns_item
			 WHERE c506_rma_id = p_raid AND c507_status_fl <> 'Q' AND c507_control_number IS NOT NULL;

			IF (v_item_count < 1)
			THEN
				UPDATE t506_returns t506
				   SET t506.c506_void_fl = 'Y'
				 WHERE t506.c506_rma_id = p_raid AND t506.c506_void_fl IS NULL;
			END IF;*/

		--END IF;
		SELECT COUNT (1)
		  INTO v_rec_count
		  FROM t506_returns
		 WHERE c506_rma_id = p_raid AND c506_void_fl IS NULL;
		 
		 IF v_rtn_type IN ('3308','3309')
		THEN
		 	SELECT COUNT(1) INTO v_cnt
		 		FROM T507_RETURNS_ITEM
		 		WHERE C506_RMA_ID = p_raid
		 		AND C507_STATUS_FL = 'R';
			IF v_cnt = 0
		 	THEN
		 		UPDATE T506_RETURNS 
		 			SET C506_STATUS_FL = '3',
		 				C506_LAST_UPDATED_DATE = CURRENT_DATE,
						C506_LAST_UPDATED_BY = p_userid
		 			WHERE C506_RMA_ID = p_raid;
		 			
		 			IF v_transid IS NOT NULL
					THEN
						UPDATE T412_INHOUSE_TRANSACTIONS SET C412_STATUS_FL = '3'
						, c412_last_updated_by = p_userid
						, c412_last_updated_date = CURRENT_DATE
						WHERE c412_inhouse_trans_id = v_transid
						AND C412_VOID_FL IS NULL;
					END IF;
		 	END IF;
		END IF; 	
		IF (v_rec_count = 0)
		THEN
			p_errmsg	:= 'RA IS VOIDED AND NEW RA IS CREATED. NEW RA NOs' || v_errmsg;
		ELSIF (TRIM (v_errmsg) IS NOT NULL)
		THEN
			p_errmsg	:= 'RA SAVED SUCCESSFULLY AND NEW RA IS CREATED.NEW RA NOs' || v_errmsg;
		ELSE
			p_errmsg	:= 'RA SAVED SUCCESSFULLY.' ;

			IF v_transid IS NOT NULL
			THEN
				p_errmsg := p_errmsg || '|The following transaction(s) <a href="#" onclick="javascript:fnPicSlip('  || '''' || v_transid  || '''' || ');">' || v_transid || '</a>  has been created and can be viewed in the Reconciliation screen';
			END IF;
		END IF;
	--
	END gm_op_sav_return_accp;

/********************************************************************************************
 * Description	: This procedure is used to check the diff between Q and R and
 *				 and create missing R record
 *********************************************************************************************/
--
	PROCEDURE gm_op_sav_check_qr (
		p_raid		 IN   t506_returns.c506_rma_id%TYPE
	  , p_order_id	 IN   t506_returns.c501_order_id%TYPE
	)
	AS
		CURSOR v_returns_cur
		IS
			SELECT pnum, ra.iniqty - ra.rqty qty, 'R' sfl
			  FROM (SELECT	 c205_part_number_id pnum, SUM (DECODE (c507_status_fl, 'Q', c507_item_qty, 0)) iniqty
						   , SUM (DECODE (c507_status_fl, 'R', c507_item_qty, 0)) rqty
						FROM t507_returns_item
					   WHERE c506_rma_id = p_raid
					GROUP BY c205_part_number_id) ra
			 WHERE ra.iniqty - ra.rqty > 0;

		--
		v_price 	   VARCHAR2 (20);
	BEGIN
		--
		FOR rad_val IN v_returns_cur
		LOOP
			IF (p_order_id IS NULL)
			THEN
				v_price 	:= get_part_price (rad_val.pnum, 'E');
			ELSE
				SELECT c502_item_price
				  INTO v_price
				  FROM t502_item_order
				 WHERE c501_order_id = p_order_id AND c205_part_number_id = rad_val.pnum AND C502_VOID_FL IS NULL AND ROWNUM = 1;
			END IF;

			INSERT INTO t507_returns_item
						(c507_returns_item_id, c506_rma_id, c205_part_number_id, c507_item_qty, c507_item_price
					   , c507_status_fl
						)
				 VALUES (s507_return_item.NEXTVAL, p_raid, rad_val.pnum, rad_val.qty, v_price
					   , rad_val.sfl
						);
		END LOOP;
	--
	END gm_op_sav_check_qr;

/********************************************************************************************
 * Description	: This procedure is used for creating Child RA's
 *********************************************************************************************/
--
	PROCEDURE gm_op_sav_reconf_accp (
		p_parent_raid	IN		 t506_returns.c506_rma_id%TYPE
	  , p_str			IN		 VARCHAR2
	  , p_retdate		IN		 VARCHAR2
	  , p_flag			IN		 VARCHAR2
	  , p_userid		IN		 t506_returns.c506_last_updated_by%TYPE
	  , p_cra_id		IN		 t506_returns.c506_rma_id%TYPE
	  , p_set_id		IN		 t506_returns.c207_set_id%TYPE
	  , p_raid			OUT 	 VARCHAR2
	)
	AS
--
		v_strlen	   NUMBER := NVL (LENGTH (p_str), 0);
		v_string	   VARCHAR2 (30000) := p_str;
		v_pnum		   VARCHAR2 (20);
		v_qty		   NUMBER;
		v_control	   t507_returns_item.c507_control_number%TYPE;
		v_orgcontrl    VARCHAR2 (20);
		v_substring    VARCHAR2 (1000);
		v_price 	   VARCHAR2 (20);
		v_fl		   VARCHAR2 (10);
		v_partnum	   VARCHAR2 (20);
		v_itemtype	   NUMBER;
--
		v_order_id	   t506_returns.c501_order_id%TYPE;
		v_reason	   t506_returns.c506_reason%TYPE;
		v_items 	   t507_returns_item%ROWTYPE;
--
		e_void_exp	   EXCEPTION;	-- Exception for checking if the Void flag is NULL
		e_status_exp   EXCEPTION;
		v_void_fl	   VARCHAR2 (10);
		v_ra_id 	   t506_returns.c506_rma_id%TYPE;
		v_dist_id	   t506_returns.c701_distributor_id%TYPE;
		v_type		   t506_returns.c506_type%TYPE;
		v_status_fl    t506_returns.c506_status_fl%TYPE;
		v_accid 	   t506_returns.c704_account_id%TYPE;
		v_sal_rep	   t506_returns.c703_sales_rep_id%TYPE;
		v_org_qty	   NUMBER;
		v_qty_sum	   NUMBER;
		v_errmsg	   VARCHAR2 (4000);
		v_count 	   NUMBER;
		v_unit_price   VARCHAR2(50);
		v_unit_price_adj   VARCHAR2(50);
		v_adj_code   VARCHAR2(50);
		v_list_price  VARCHAR2 (20);
		v_do_unit_price		t507_returns_item.C507_DO_UNIT_PRICE%TYPE;
		v_adj_code_val		t507_returns_item.C507_ADJ_CODE_VALUE%TYPE;
		v_item_ord_id		t502_item_order.c502_item_order_id%TYPE;
		v_adj_code_per		VARCHAR2(10);
		v_acc_id			t501_order.c704_account_id%TYPE;
		v_party_id			t740_gpo_account_mapping.c101_party_id%TYPE;
		v_company_id        t1900_company.c1900_company_id%TYPE;
		v_plant_id          T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
		
	BEGIN
		SELECT get_compid_frm_cntx(), get_plantid_frm_cntx()
	  INTO v_company_id, v_plant_id
	  FROM dual;
		--
		SELECT	   c506_void_fl, c506_status_fl
			  INTO v_void_fl, v_status_fl
			  FROM t506_returns
			 WHERE c506_rma_id = p_parent_raid
		FOR UPDATE;

		IF v_void_fl IS NOT NULL
		THEN
			-- Error message is Returns already Voided.
			RAISE e_void_exp;
		END IF;

		IF v_status_fl <> 0
		THEN
			RAISE e_status_exp;
		END IF;

		IF v_status_fl = 1
		THEN
			raise_application_error (-20036, '');
		END IF;

		IF (v_strlen = 0)
		THEN
			RETURN;
		END IF;

		SELECT c501_order_id, c506_rma_id, c701_distributor_id, c506_type, c506_reason, c506_status_fl
			 , c704_account_id, c703_sales_rep_id
		  INTO v_order_id, v_ra_id, v_dist_id, v_type, v_reason, v_status_fl
			 , v_accid, v_sal_rep
		  FROM t506_returns
		 WHERE c506_rma_id = p_parent_raid;

		SELECT 'GM-CRA-' || s506_return.NEXTVAL
		  INTO v_ra_id
		  FROM DUAL;

		INSERT INTO t506_returns
					(c506_rma_id, c701_distributor_id, c506_type, c506_reason, c506_expected_date
				   , c506_status_fl, c704_account_id, c506_created_by, c506_created_date, c506_parent_rma_id
				   , c207_set_id, c703_sales_rep_id, c1900_company_id, c5040_plant_id
					)
			 VALUES (v_ra_id, v_dist_id, v_type, v_reason, TO_DATE (p_retdate, NVL(get_compdtfmt_frm_cntx(),'mm/dd/yyyy'))
				   , DECODE (p_flag, '1', 1, 0), v_accid, p_userid, CURRENT_DATE, p_parent_raid
				   , DECODE (TRIM (p_set_id), NULL, NULL, p_set_id), v_sal_rep, v_company_id, v_plant_id
					);

		--When ever the child RA's is created, the parent table has to be updated ,so ETL can Sync it to GOP.
		UPDATE t506_returns
		   SET C506_LAST_UPDATED_BY = p_userid, 
		       C506_LAST_UPDATED_DATE = CURRENT_DATE
		WHERE C506_rma_Id = p_parent_raid;

		--insert new value into returns table
		WHILE INSTR (v_string, '|') <> 0
		LOOP
			--
			v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
			v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
			v_pnum		:= NULL;
			v_qty		:= NULL;
			v_control	:= NULL;
			v_itemtype	:= NULL;
			v_price		:= NULL;
			v_unit_price := NULL;
			v_unit_price_adj := NULL;
			v_adj_code  := NULL;
			v_pnum		:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_qty		:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_control	:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_orgcontrl := SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_itemtype	:= TO_NUMBER (SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1));
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_price	:= TO_NUMBER (SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1));
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_unit_price	:= TO_NUMBER (SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1));
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_unit_price_adj	:= TO_NUMBER (SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1));
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_adj_code	:=  TO_NUMBER (v_substring);
				--
			-- If Consignment get the Equity Value
			-- or get the order value
			IF(v_price IS NULL)THEN
			IF (v_order_id IS NULL)
			THEN
				v_price 	:= get_part_price (v_pnum, 'E');
				v_list_price:= get_part_price (v_pnum, 'L');
			ELSE
				SELECT c502_item_price
				  INTO v_price
				  FROM t502_item_order
				 WHERE c501_order_id = v_order_id AND c205_part_number_id = v_pnum AND C502_VOID_FL IS NULL AND ROWNUM = 1;
			END IF;
			END IF;

			BEGIN
			     SELECT c704_account_id into v_acc_id
			     FROM t501_order
			     WHERE c501_order_id = v_order_id
			     AND c501_void_fl IS NULL;
			     EXCEPTION WHEN NO_DATA_FOUND THEN
			      	v_acc_id := NULL;
			END;
			BEGIN
			     SELECT t740.c101_party_id 
			     	INTO v_party_id
			     FROM t740_gpo_account_mapping t740 
			     WHERE t740.c704_account_id = v_acc_id
			     AND c740_void_fl IS NULL;
			     EXCEPTION WHEN NO_DATA_FOUND THEN
			      	v_party_id := NULL;
			END;
			v_adj_code_per := gm_pkg_sm_adj_txn.get_adj_code_value(v_order_id,v_pnum,v_item_ord_id, v_adj_code);
			v_adj_code_val	:= ((NVL(v_unit_price,0) - NVL(v_unit_price_adj,0)) * NVL(v_adj_code_per,0)/100);
			-- Get the unit price using below formula, net unit price + unit adjustment value + adjustment code value
	        --v_acc_price := NVL(v_price,0) + NVL(v_unit_price_adj,0) + NVL(v_adj_code_val,0);
			v_do_unit_price := NVL(gm_pkg_sm_adj_rpt.get_account_part_unitprice(v_acc_id,v_pnum, v_party_id),0);
			--
			INSERT INTO t507_returns_item
						(c507_returns_item_id, c506_rma_id, c205_part_number_id, c507_control_number, c507_item_qty
					   , c507_item_price, c507_status_fl, c901_type ,C507_UNIT_PRICE,C507_UNIT_PRICE_ADJ_VALUE ,C901_UNIT_PRICE_ADJ_CODE,C507_LIST_PRICE
					   , c507_do_unit_price, C507_ADJ_CODE_VALUE
						)
				 VALUES (s507_return_item.NEXTVAL, v_ra_id, v_pnum, v_control, v_qty
					   , v_price, 'R', v_itemtype , v_unit_price , v_unit_price_adj , v_adj_code , v_list_price
					   , v_do_unit_price, v_adj_code_val
						);
						
		IF v_type <> '3300' --Sales - Items
		THEN 
			UPDATE t507_returns_item
			   SET c507_item_qty =
							  DECODE (SIGN (c507_item_qty - TO_NUMBER (v_qty))
									, 1, c507_item_qty - TO_NUMBER (v_qty)
									, 0
									 )
			 WHERE c506_rma_id = p_parent_raid AND c507_status_fl IN ('Q', 'R') AND c205_part_number_id = v_pnum;
		END IF; 
		 
		END LOOP;

		INSERT INTO t507_returns_item
					(c507_returns_item_id, c506_rma_id, c205_part_number_id, c507_item_qty, c507_item_price
				   , c507_status_fl,C507_UNIT_PRICE,C507_UNIT_PRICE_ADJ_VALUE ,C901_UNIT_PRICE_ADJ_CODE,C507_LIST_PRICE, c507_do_unit_price, C507_ADJ_CODE_VALUE)
			SELECT s507_return_item.NEXTVAL, v_ra_id, c205_part_number_id, item_qty, c507_item_price, 'Q' , C507_UNIT_PRICE,C507_UNIT_PRICE_ADJ_VALUE ,C901_UNIT_PRICE_ADJ_CODE,C507_LIST_PRICE
				, c507_do_unit_price, C507_ADJ_CODE_VALUE
			  FROM (SELECT	 c205_part_number_id, c507_item_price, SUM (c507_item_qty) item_qty , C507_UNIT_PRICE,C507_UNIT_PRICE_ADJ_VALUE ,C901_UNIT_PRICE_ADJ_CODE,C507_LIST_PRICE
			  		, c507_do_unit_price, C507_ADJ_CODE_VALUE
						FROM t507_returns_item
					   WHERE c506_rma_id = v_ra_id AND c507_status_fl = 'R'
			GROUP BY c205_part_number_id, c507_item_price , C507_UNIT_PRICE,C507_UNIT_PRICE_ADJ_VALUE ,C901_UNIT_PRICE_ADJ_CODE,C507_LIST_PRICE, c507_do_unit_price, C507_ADJ_CODE_VALUE);		

		SELECT SUM (c507_item_qty)
		  INTO v_qty_sum
		  FROM t507_returns_item
		 WHERE c506_rma_id = p_parent_raid;

		IF p_flag = '1'
		THEN
			SELECT COUNT (1)
			  INTO v_count
			  FROM t5010_tag t5010
			 WHERE t5010.c5010_void_fl IS NULL AND t5010.c5010_last_updated_trans_id = v_ra_id;

			IF (v_count = 0)
			THEN
				raise_application_error ('-20209', '');
			END IF;
		END IF;

		IF v_qty_sum = 0
		THEN
			UPDATE t506_returns
			   SET c506_status_fl = '2'
				 , c506_last_updated_by = p_userid
				 , c506_last_updated_date = CURRENT_DATE
			 WHERE c506_rma_id = p_parent_raid;

			--
			DELETE FROM t507_returns_item t507
				  WHERE c506_rma_id = p_parent_raid AND c507_status_fl = 'Q';
		END IF;

		p_raid		:= v_ra_id;

		IF (v_errmsg IS NOT NULL)
		THEN
			p_raid		:= v_errmsg;
		END IF;
	--
	EXCEPTION
		WHEN e_void_exp
		THEN
			-- Raising Application error with msg "Consignment already Voided."
			raise_application_error (-20021, '');
		WHEN e_status_exp
		THEN
			raise_application_error (-20022, '');
	END gm_op_sav_reconf_accp;

--
/********************************************************************
 * Description : This function is used to get the Initiated part Qty
 * for the	given Returns id and part number.
 ********************************************************************/
--
	FUNCTION get_ra_part_init_qty (
		p_raid		IN	 t506_returns.c506_rma_id%TYPE
	  , p_part_id	IN	 t205_part_number.c205_part_number_id%TYPE
	)
		RETURN NUMBER
	IS
		v_item_qty	   NUMBER;
--
	BEGIN
		--The sum is added because of IHLN transaction can have more than one record for status having Q for the same part number. 
		SELECT SUM(c507_item_qty)
		  INTO v_item_qty
		  FROM t507_returns_item
		 WHERE c506_rma_id = p_raid AND c507_status_fl IN ('Q') AND c205_part_number_id = p_part_id;	 

		RETURN v_item_qty;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN 0;
	END get_ra_part_init_qty;

/********************************************************************************************
 * Description	: This Procedure is used to fetch the part details to process Returns
 *********************************************************************************************/
--
	PROCEDURE gm_op_fch_return_accp (
		p_raid		   IN		t506_returns.c506_rma_id%TYPE
	  , p_consign_id   IN		t504_consignment.c504_consignment_id%TYPE
	  , p_part_nos	   IN		VARCHAR2
	  , p_part_cur	   OUT		TYPES.cursor_type
	)
	--
	AS
		v_id		   t701_distributor.c701_distributor_id%TYPE;
		v_user_id	   t506_returns.c101_user_id%TYPE;
		v_ret_type	   t506_returns.c506_type%TYPE;
		v_ware_house	t906_rules.c906_rule_value%TYPE;
    	v_ware_house_type  	t906_rules.c906_rule_value%TYPE;
    	v_req_type		t506_returns.c506_type%TYPE;
	BEGIN
		BEGIN
			SELECT NVL (c701_distributor_id, c704_account_id), c101_user_id, c506_type
		  	INTO v_id, v_user_id,v_ret_type
		  	FROM t506_returns
		 	WHERE c506_rma_id = p_raid;
		EXCEPTION WHEN NO_DATA_FOUND
		THEN
			v_id := NULL; 
			v_user_id := NULL;
		END;
		-- To assign part information
		my_context.set_my_inlist_ctx (p_part_nos);
		

		-- 3304: Account consignment item; 4121: Sales Rep
		SELECT DECODE(v_ret_type, '3304', v_ret_type, NULL) INTO v_req_type FROM dual;
		
		
		v_ware_house      := get_rule_value(NVL(v_req_type,'4121'),'RULEWH'); -- to get the warehouse based on the item return type
    	v_ware_house_type := get_rule_value(NVL(v_req_type,'4121'),'RULEWHTYPE'); -- to get the warehouse type based on the item return type
    	
		-- 4000338 (FS location type)
		-- 3 (FS Warehouse)
 	IF (p_consign_id is not null) THEN
		OPEN p_part_cur
		 FOR
			 SELECT   c205_part_number_id pnum, get_partnum_desc (c205_part_number_id) pdesc			 		
			 		  , SUM (decode(p_consign_id,'',c507_item_qty,t505.orgqty)) qty   
					, DECODE(v_ret_type,'3308',gm_pkg_op_ihloaner_item_txn.get_csg_part_qty_for_dist(v_user_id,c205_part_number_id),
							 DECODE(v_id, '01', get_csg_part_qty_for_dist (v_id, v_user_id, c205_part_number_id),get_field_sales_net_con_qty(v_ware_house, v_id, c205_part_number_id, v_ware_house_type))) cons_qty
					, gm_pkg_op_return.get_ra_part_init_qty (p_raid, c205_part_number_id) iqty--
					, t505.cnum cnum
					, TO_CHAR (c901_type) itype, get_part_attribute_value (c205_part_number_id, 92340) tagfl
					, get_partnum_material_type( c205_part_number_id) PARTMTRELTYPE
					, TO_CHAR (C507_ITEM_PRICE) price
					, TO_CHAR(c507_unit_price) UPRICE
                    , TO_CHAR(c507_unit_price_adj_value) UADJVAL
                    , c901_unit_price_adj_code UADJCODE
                    , TO_CHAR(C507_DO_UNIT_PRICE) DOUPRICE
                    , TO_CHAR(C507_ADJ_CODE_VALUE) ADJCODEVAL
                    , c507_error_dtls ERRORDTLS 
				 FROM t507_returns_item t507,
					 (SELECT t.pnum,t.cnum,sum(t.orgqty),t.retqty,
					         (CASE WHEN (sum(t.orgqty) > t.retqty) THEN t.retqty ELSE sum(t.orgqty) END) orgqty
					    FROM      
						 (SELECT t505_item.c505_item_consignment_id,
						 		 t507.c205_part_number_id pnum,
								 get_returned_cnums (p_consign_id,p_raid,t505_item.c505_item_consignment_id,t507.c205_part_number_id ) cnum,
								 sum(t505_item.c505_item_qty) consqty,sum(c507_item_qty)retqty,
								(CASE WHEN (t505_item.c505_item_qty > c507_item_qty) THEN c507_item_qty ELSE t505_item.c505_item_qty END) orgqty
							FROM t505_item_consignment t505_item, t504_consignment t504 ,t507_returns_item t507
						   WHERE t505_item.c504_consignment_id = p_consign_id						 
							 AND t504.c504_consignment_id   = t505_item.c504_consignment_id
							 AND t507.c205_part_number_id = t505_item.c205_part_number_id
							 AND c507_status_fl IN ('Q') 
							 AND c506_rma_id            = p_raid
							 AND c504_void_fl              IS NULL
							 AND t505_item.c505_void_fl              IS NULL
						   GROUP BY t505_item.c505_item_consignment_id,t507.c205_part_number_id,t505_item.c505_item_qty,c507_item_qty)t group by cnum,pnum,retqty)t505 
				WHERE c506_rma_id = p_raid AND c507_status_fl IN ('C', 'R', 'W')
				AND t507.c205_part_number_id = t505.pnum(+)
			 GROUP BY t507.c205_part_number_id ,t505.cnum,c507_control_number, c507_item_qty,c901_type,C507_ITEM_PRICE, c507_unit_price , c507_unit_price_adj_value , c901_unit_price_adj_code, C507_DO_UNIT_PRICE, C507_ADJ_CODE_VALUE, c507_error_dtls
			 UNION
			 SELECT   t205.c205_part_number_id pnum, get_partnum_desc (t205.c205_part_number_id) pdesc, 0 qty 
					, DECODE(v_id, '01', get_csg_part_qty_for_dist (v_id, v_user_id, t205.c205_part_number_id), get_field_sales_net_con_qty(v_ware_house, v_id, t205.c205_part_number_id, v_ware_house_type)) cons_qty, 0 iqty, '' cnum
					, '' itype, get_part_attribute_value (t205.c205_part_number_id, 92340) tagfl
					, get_partnum_material_type( c205_part_number_id) PARTMTRELTYPE
					, '' price
					, '' UPRICE
                    , '' UADJVAL
                    , null UADJCODE
                    , '' DOUPRICE
                    , '' ADJCODEVAL
                    , '' ERRORDTLS
				 FROM t205_part_number t205
				WHERE t205.c205_part_number_id IN (SELECT token
													 FROM v_in_list)
				  AND t205.c205_part_number_id NOT IN (SELECT x.c205_part_number_id
														 FROM t507_returns_item x
														WHERE x.c506_rma_id = p_raid AND x.c507_status_fl <> 'Q')
			 ORDER BY pnum;
			 END IF;
	IF p_consign_id is null then
		OPEN p_part_cur
		 FOR
			SELECT   c205_part_number_id pnum, get_partnum_desc (c205_part_number_id) pdesc, SUM (c507_item_qty) qty
					, DECODE(v_ret_type,'3308',gm_pkg_op_ihloaner_item_txn.get_csg_part_qty_for_dist(v_user_id,c205_part_number_id),
							 DECODE(v_id, '01', get_csg_part_qty_for_dist (v_id, v_user_id, c205_part_number_id),get_field_sales_net_con_qty(v_ware_house, v_id, c205_part_number_id, v_ware_house_type))) cons_qty
					, gm_pkg_op_return.get_ra_part_init_qty (p_raid, c205_part_number_id) iqty
					, DECODE (p_consign_id
							, '', c507_control_number
							, get_control_number (p_consign_id, c205_part_number_id, c507_item_qty)
							 ) cnum
					, TO_CHAR (c901_type) itype, get_part_attribute_value (c205_part_number_id, 92340) tagfl
					, get_partnum_material_type( c205_part_number_id) PARTMTRELTYPE
					, TO_CHAR (C507_ITEM_PRICE) price
					, TO_CHAR(c507_unit_price) UPRICE
                    , TO_CHAR(c507_unit_price_adj_value) UADJVAL
                    , c901_unit_price_adj_code UADJCODE
                    , TO_CHAR(C507_DO_UNIT_PRICE) DOUPRICE
                    , TO_CHAR(C507_ADJ_CODE_VALUE) ADJCODEVAL
                    , c507_error_dtls ERRORDTLS 
				 FROM t507_returns_item
				WHERE c506_rma_id = p_raid AND c507_status_fl IN ('C', 'R', 'W')
			 GROUP BY c205_part_number_id, c507_control_number, c507_item_qty, c901_type,C507_ITEM_PRICE, c507_unit_price , c507_unit_price_adj_value , c901_unit_price_adj_code, C507_DO_UNIT_PRICE, C507_ADJ_CODE_VALUE, c507_error_dtls
			 UNION
			 SELECT   t205.c205_part_number_id pnum, get_partnum_desc (t205.c205_part_number_id) pdesc, 0 qty
					, DECODE(v_id, '01', get_csg_part_qty_for_dist (v_id, v_user_id, t205.c205_part_number_id), get_field_sales_net_con_qty(v_ware_house, v_id, t205.c205_part_number_id, v_ware_house_type)) cons_qty, 0 iqty, '' cnum
					, '' itype, get_part_attribute_value (t205.c205_part_number_id, 92340) tagfl
					, get_partnum_material_type( c205_part_number_id) PARTMTRELTYPE
					, '' price
					, '' UPRICE
                    , '' UADJVAL
                    , null UADJCODE
                    , '' DOUPRICE
                    , '' ADJCODEVAL
                    , '' ERRORDTLS
				 FROM t205_part_number t205
				WHERE t205.c205_part_number_id IN (SELECT token
													 FROM v_in_list)
				  AND t205.c205_part_number_id NOT IN (SELECT x.c205_part_number_id
														 FROM t507_returns_item x
														WHERE x.c506_rma_id = p_raid AND x.c507_status_fl <> 'Q')
			 ORDER BY pnum;		 
	END IF;
	END gm_op_fch_return_accp;

/********************************************************************************************
 * Description	: This Procedure is used to fetch the part details to reconfigure set
 *********************************************************************************************/
--
	PROCEDURE gm_op_fch_reconf_accp (
		p_distid	   IN		t701_distributor.c701_distributor_id%TYPE
	  , p_raid		   IN		t506_returns.c506_rma_id%TYPE
	  , p_consign_id   IN		t504_consignment.c504_consignment_id%TYPE
	  , p_part_nos	   IN		VARCHAR2
	  , p_set_id	   IN		t207_set_master.c207_set_id%TYPE
	  , p_part_cur	   OUT		TYPES.cursor_type
	)
	--
	AS
		e_void_exp	   EXCEPTION;
		-- Exception for checking if the Void flag is NULL
		e_status_exp   EXCEPTION;
		v_void_fl	   VARCHAR2 (10);
		v_status_fl    t506_returns.c506_status_fl%TYPE;
		v_qty_sum	   NUMBER;
		v_id		   t701_distributor.c701_distributor_id%TYPE;
		v_user_id	   t506_returns.c101_user_id%TYPE;
		v_ret_type	   t506_returns.c506_type%TYPE;
		v_ware_house	t906_rules.c906_rule_value%TYPE;
    	v_ware_house_type  	t906_rules.c906_rule_value%TYPE;
        v_company_id 		  t1900_company.c1900_company_id%TYPE;
      BEGIN

	
	 	 SELECT get_compid_frm_cntx()
	       INTO v_company_id
	       FROM DUAL;
       
		SELECT NVL (c701_distributor_id, c704_account_id), c101_user_id, c506_void_fl, c506_status_fl, c506_type
		  INTO v_id, v_user_id, v_void_fl, v_status_fl, v_ret_type
		  FROM t506_returns
		 WHERE c506_rma_id = p_raid;

		SELECT SUM (c507_item_qty)
		  INTO v_qty_sum
		  FROM t507_returns_item
		 WHERE c506_rma_id = p_raid;

		IF v_void_fl IS NOT NULL
		THEN
			-- Error message is Returns already Voided.
			RAISE e_void_exp;
		END IF;

		IF v_status_fl <> 0 AND v_qty_sum = 0
		THEN
			RAISE e_status_exp;
		END IF;

		-- 3304: Account consignment item; 4121: Sales Rep
		SELECT DECODE(v_ret_type, '3304', v_ret_type, NULL) INTO v_ret_type FROM dual;
		
		v_ware_house      := get_rule_value(NVL(v_ret_type,'4121'),'RULEWH'); -- to get the warehouse based on the item return type
    	v_ware_house_type := get_rule_value(NVL(v_ret_type,'4121'),'RULEWHTYPE'); -- to get the warehouse type based on the item return type
    	
    	
		-- To assign part information
		my_context.set_my_inlist_ctx (p_part_nos);

		OPEN p_part_cur
		 FOR
			 SELECT   t205.c205_part_number_id pnum, t205.c205_part_num_desc pdesc
					, NVL (c507_item_qty, get_set_qty (p_set_id, t205.c205_part_number_id)) qty
					, get_set_qty (p_set_id, t205.c205_part_number_id) setqty
					, DECODE(v_id, '01', get_csg_part_qty_for_dist (v_id, v_user_id, t205.c205_part_number_id), get_field_sales_net_con_qty(v_ware_house, v_id, t205.c205_part_number_id, v_ware_house_type)) cons_qty
					, gm_pkg_op_return.get_ra_part_init_qty (t507.c506_rma_id, t507.c205_part_number_id) iqty
					, c507_item_qty pending_qty
					, DECODE (p_consign_id
							, '', t507.c507_control_number
							, get_control_number (p_consign_id
												, t205.c205_part_number_id
												, get_set_qty (p_set_id, t205.c205_part_number_id)
												 )
							 ) cnum
					, TO_CHAR(t507.c507_item_price) PRICE
				 FROM t507_returns_item t507, t205_part_number t205
				WHERE t507.c506_rma_id = p_raid
				  AND t507.c507_status_fl = 'R'
				  AND t507.c205_part_number_id = t205.c205_part_number_id
				  AND (   t205.c205_part_number_id IN (SELECT c205_part_number_id
														 FROM t208_set_details
														WHERE c207_set_id = p_set_id)
					   OR t205.c205_part_number_id IN (SELECT token
														 FROM v_in_list)
					  )
			 UNION
			 SELECT   t205.c205_part_number_id pnum, t205.c205_part_num_desc pdesc, 0 qty, 0 setqty
					, DECODE(v_id, '01', get_csg_part_qty_for_dist (v_id, v_user_id, t205.c205_part_number_id), get_field_sales_net_con_qty(v_ware_house, v_id, t205.c205_part_number_id, v_ware_house_type)) cons_qty, 0 iqty
					, 0 pending_qty, '' cnum , '' PRICE
				 FROM t205_part_number t205, t2023_part_company_mapping t2023
				WHERE t205.c205_part_number_id = t2023.c205_part_number_id 
	              AND t2023.c2023_void_fl IS NULL
	              AND t2023.c1900_company_id = v_company_id
				  AND (   t205.c205_part_number_id IN (SELECT *
														 FROM v_in_list)
					   OR t205.c205_part_number_id IN (SELECT c205_part_number_id
														 FROM t208_set_details
														WHERE c207_set_id = p_set_id)
					  )
				  AND t205.c205_part_number_id NOT IN (SELECT x.c205_part_number_id
														 FROM t507_returns_item x
														WHERE x.c506_rma_id = p_raid AND x.c507_status_fl <> 'Q')
			 ORDER BY pnum;
	EXCEPTION
		WHEN e_void_exp
		THEN
			-- Raising Application error with msg "Consignment already Voided."
			raise_application_error (-20021, '');
		WHEN e_status_exp
		THEN
			raise_application_error (-20022, '');
	END gm_op_fch_reconf_accp;

--
/********************************************************************************************
 * Description	: This Procedure is used to fetch the details of a RA
 * (To display in the Credit RA Screen)
 *********************************************************************************************/
--
	PROCEDURE gm_op_fch_return_forcredit (
		p_raid			   IN		t506_returns.c506_rma_id%TYPE
	  , p_frm_screen	   IN		VARCHAR2	
	  , p_part_cur		   OUT		TYPES.cursor_type
	  , p_associated_cur   OUT		TYPES.cursor_type
	)
--
	AS
		v_id		   t701_distributor.c701_distributor_id%TYPE;
		v_user_id	   t506_returns.c101_user_id%TYPE;
		v_ret_type		t506_returns.c506_type%TYPE;
		v_ware_house	t906_rules.c906_rule_value%TYPE;
    	v_ware_house_type  	t906_rules.c906_rule_value%TYPE;
    	v_req_type		t506_returns.c506_type%TYPE;
    	v_company_id    t506_returns.C1900_COMPANY_ID%type;
    	v_plant_id      t506_returns.C5040_PLANT_ID%type;
	BEGIN
		SELECT NVL (c701_distributor_id, c704_account_id), c101_user_id, c506_type,c1900_company_id ,c5040_plant_id
		  INTO v_id, v_user_id, v_ret_type,v_company_id,v_plant_id
		  FROM t506_returns
		 WHERE c506_rma_id = p_raid;

		  --PC-3707 -set company and plant id in context
          gm_pkg_cor_client_context.gm_sav_client_context (v_company_id, v_plant_id);
          
		 -- 3304: Account consignment item; 4121: Sales Rep
		SELECT DECODE(v_ret_type, '3304', v_ret_type, NULL) INTO v_req_type FROM dual;
		
		v_ware_house      := get_rule_value(NVL(v_req_type,'4121'),'RULEWH'); -- to get the warehouse based on the item return type
    	v_ware_house_type := get_rule_value(NVL(v_req_type,'4121'),'RULEWHTYPE'); -- to get the warehouse type based on the item return type
    	
    	
		 /*
		  * Below if block will execute for Credit Returns screen when click on Pending credit Y from A/R home.
		  */
		IF p_frm_screen = 'AccountsReturnsDashboard' THEN
			OPEN p_part_cur
			 FOR
				 SELECT RTN.PNUM, T205.C205_PART_NUM_DESC PDESC, RTN.CNUM, RTN.ITEMTYPE
				  , RTN.PRICE, INIT.INIT_QTY, RTN.RETURN_QTY
				  , GET_PART_ATTRIBUTE_VALUE (rtn.PNUM, 92340) TAGABLE
				  , RTN.UNITPRICE, RTN.ADJVAL, RTN.ADJCODE, RTN.ADJCODEID
				   FROM
				    (
				         SELECT T507.C205_PART_NUMBER_ID PNUM, TRIM(t507.c507_control_number) CNUM, C507_ITEM_PRICE PRICE, C901_TYPE ITEMTYPE
				          , SUM (DECODE (TRIM (t507.c507_control_number), NULL, 0, c507_item_qty)) return_qty
				          , C507_UNIT_PRICE unitprice, C507_UNIT_PRICE_ADJ_VALUE*-1 adjval, DECODE(c901_unit_price_adj_code,NULL,'N/A',GET_CODE_NAME(c901_unit_price_adj_code)) adjcode
				          , c901_unit_price_adj_code adjcodeid
				           FROM T506_RETURNS T506, T507_RETURNS_ITEM T507
				          WHERE T506.C506_RMA_ID    = p_raid
	                        AND T506.C506_RMA_ID = T507.C506_RMA_ID
	                        AND T506.C506_VOID_FL IS NULL
				            AND c507_status_fl <> 'Q'
				       GROUP BY T507.C205_PART_NUMBER_ID, t507.c507_control_number, C507_ITEM_PRICE, c901_type, c507_unit_price, c507_unit_price_adj_value, c901_unit_price_adj_code
				       ORDER BY PNUM
				    )
				    RTN, (
				         SELECT T507.C205_PART_NUMBER_ID PNUM
				          , SUM (c507_item_qty) init_qty
				           FROM T506_RETURNS T506, T507_RETURNS_ITEM T507
				          WHERE T506.C506_RMA_ID    = p_raid
	                        AND T506.C506_RMA_ID = T507.C506_RMA_ID
				            AND C507_STATUS_FL = 'Q'
				       GROUP BY T507.C205_PART_NUMBER_ID
				       ORDER BY PNUM
				    )
				    INIT, T205_PART_NUMBER T205
				  WHERE RTN.PNUM = INIT.PNUM(+)
				    AND rtn.pnum = T205.C205_PART_NUMBER_ID
				ORDER BY rtn.pnum;
		ELSE	
			OPEN p_part_cur
			 FOR
				 SELECT pnum, pdesc,  DECODE(v_ret_type,'3308',gm_pkg_op_ihloaner_item_txn.get_csg_part_qty_for_dist (v_user_id, pnum),cons_qty) cons_qty
				 		, init_qty, return_qty
					  , DECODE (SIGN (init_qty - return_qty), -1, 0, (init_qty - return_qty)) miss_qty
					  , get_part_attribute_value (pnum, 92340) tagable
				   FROM (SELECT   t205.c205_part_number_id pnum, t205.c205_part_num_desc pdesc
								,DECODE(v_id, '01', get_csg_part_qty_for_dist (v_id, v_user_id, t205.c205_part_number_id), get_field_sales_net_con_qty(v_ware_house, v_id, t205.c205_part_number_id, v_ware_house_type)) cons_qty
								, SUM (DECODE (c507_status_fl, 'Q', c507_item_qty, 0)) init_qty
								, SUM (DECODE (c507_status_fl
											 , 'Q', 0
											 , DECODE (TRIM (t507.c507_control_number), NULL, 0, c507_item_qty)
											  )
									  ) return_qty
							 FROM t507_returns_item t507, t205_part_number t205
							WHERE c506_rma_id = p_raid AND t205.c205_part_number_id = t507.c205_part_number_id
						 GROUP BY t205.c205_part_number_id, t205.c205_part_num_desc
						 ORDER BY pnum);
		END IF;
		
		OPEN p_associated_cur
		 FOR
			 SELECT pnum, pdesc, DECODE(v_ret_type,'3308',gm_pkg_op_ihloaner_item_txn.get_csg_part_qty_for_dist (v_user_id, pnum),cons_qty) cons_qty
			 				, qty, repname
			   FROM (SELECT   t205.c205_part_number_id pnum, get_rep_name (t506.c703_sales_rep_id) repname
							, t205.c205_part_num_desc pdesc
							, get_csg_part_qty_for_dist (172, 242, t205.c205_part_number_id) cons_qty
							, SUM (DECODE (c507_status_fl
										 , 'Q', 0
										 , DECODE (TRIM (t507.c507_control_number), NULL, 0, c507_item_qty)
										  )
								  ) qty
						 FROM t507_returns_item t507, t506_returns t506, t205_part_number t205
						WHERE t507.c506_rma_id = t506.c506_rma_id
						  AND t205.c205_part_number_id = t507.c205_part_number_id
						  AND t506.c506_associated_rma_id = p_raid
					 GROUP BY t205.c205_part_number_id, t205.c205_part_num_desc, t506.c703_sales_rep_id
					 ORDER BY pnum);
	END gm_op_fch_return_forcredit;

--
	--
/********************************************************************************************
 * Description	: This Procedure is used to credit a RA
 *********************************************************************************************/
--
	PROCEDURE gm_op_sav_return_forcredit (
		p_raid		   IN	t506_returns.c506_rma_id%TYPE
	  , p_creditdate   IN	DATE
	  , p_userid	   IN	t506_returns.c506_last_updated_by%TYPE
	)
--
	AS
		v_dist_id	   t506_returns.c701_distributor_id%TYPE;
		v_status_fl    t506_returns.c506_status_fl%TYPE;
		v_inventory_fl t506_returns.c506_status_fl%TYPE;
		v_msg		   VARCHAR2 (1000);
		v_reason	   t506_returns.c506_reason%TYPE;
		v_order_id	   t506_returns.c501_order_id%TYPE;
		v_dist_type    t701_distributor.c901_distributor_type%TYPE;
		v_upd_inv_type VARCHAR2 (4) := 'R';
		v_rtn_count    NUMBER;
		v_tag_count    NUMBER;
		v_rettype		VARCHAR2(20);
		v_txnid			T412_INHOUSE_TRANSACTIONS.C412_INHOUSE_TRANS_ID%TYPE;
		v_account	t704_account.c704_account_id%TYPE;
		v_fs_id	   t506_returns.c701_distributor_id%TYPE;
		v_parent_raid  t506_returns.c506_rma_id%TYPE;
		v_rettype_tmp		t506_returns.c506_type%TYPE;
		v_skip_fs_ws_flag T906_RULES.C906_RULE_VALUE%TYPE; 
		v_dist_parent_comp_id t701_distributor.c1900_parent_company_id%TYPE;
		v_set_id			t506_returns.C207_SET_ID%TYPE; 
		v_distributor_id	t506_returns.C701_DISTRIBUTOR_ID%TYPE;
   		v_plant_id			t506_returns.C5040_PLANT_ID%TYPE;
   		v_company_id        t1900_company.c1900_company_id%TYPE;
   		v_max_id			t5011_tag_log.c5011_tag_txn_log_id%TYPE;
   		v_last_id			t5011_tag_log.c5011_last_updated_trans_id%TYPE;
		
	BEGIN
		SELECT	   c506_status_fl, c506_update_inv_fl, c701_distributor_id, c506_reason, c501_order_id
				 , get_distributor_type (c701_distributor_id), C506_TYPE, c704_account_id, c506_parent_rma_id,  C207_SET_ID
			  INTO v_status_fl, v_inventory_fl, v_dist_id, v_reason, v_order_id
				 , v_dist_type, v_rettype, v_account, v_parent_raid, v_set_id
			  FROM t506_returns
			 WHERE c506_rma_id = p_raid
		FOR UPDATE;
		
		
				
		IF v_status_fl <> 1
		THEN
			raise_application_error (-20024, '');
		END IF;

		IF (TRIM (v_dist_id) IS NOT NULL)
		THEN
			-- Lock the distributor for update
			SELECT	   c701_distributor_id, c1900_parent_company_id
				  INTO v_dist_id, v_dist_parent_comp_id
				  FROM t701_distributor
				 WHERE c701_distributor_id = v_dist_id
			FOR UPDATE;
		END IF;

		v_fs_id := v_dist_id;
		-- gm_procedure_log('Order ID',v_order_id);
		-- To remove all the missing R record
		DELETE FROM t507_returns_item t507
			  WHERE c506_rma_id = p_raid AND c507_status_fl = 'R' AND TRIM (c507_control_number) IS NULL;

		-- Update the C507_STATUS_FL to C (Credited)
		UPDATE t507_returns_item t507
		   SET c507_status_fl = 'C'
		 WHERE c506_rma_id = p_raid AND c507_status_fl = 'R' AND TRIM (c507_control_number) IS NOT NULL;

		-- save credit order
		IF (v_reason = 3316 OR v_reason = 3317)
		THEN
			gm_save_credit_order_swap_type (p_raid, v_reason, v_order_id, CURRENT_DATE, '', p_userid);
		END IF;

		IF v_rettype NOT IN ('3308','3309') 
		THEN
			-- To mark the M Record
			INSERT INTO t507_returns_item
						(c507_returns_item_id, c507_item_qty, c507_item_price, c205_part_number_id	 --, c507_control_number
					   , c506_rma_id, c507_status_fl)
				SELECT s507_return_item.NEXTVAL, iqty - rqty, iprice, pnum, rma_id, status
				  FROM (SELECT	 c506_rma_id rma_id, c205_part_number_id pnum, 0 iprice
							   , SUM (DECODE (c507_status_fl, 'Q', c507_item_qty, 0)) iqty
							   , SUM (DECODE (c507_status_fl, 'C', c507_item_qty, 0)) rqty	 --, c507_control_number cnum
							   , 'M' status
							FROM t507_returns_item t507
						   WHERE c506_rma_id = p_raid
						GROUP BY c506_rma_id, c205_part_number_id)
				 WHERE (iqty - rqty) > 0;
		END IF;
		-- delete the Q record
		DELETE FROM t507_returns_item t507
			  WHERE c506_rma_id = p_raid AND c507_status_fl = 'Q';

		-- update the returns for the RA
		UPDATE t506_returns
		   SET c506_credit_date = p_creditdate
			 -- Setting reprocess date since the Loaner to Consign doesnt need to be reprocessed: James 3/4/09
		,	   c501_reprocess_date = DECODE (v_reason, 3317, p_creditdate, NULL)
			 , c506_status_fl = 2
			 , c506_last_updated_by = p_userid
			 , c506_last_updated_date = CURRENT_DATE
		 WHERE c506_rma_id = p_raid;
		 
		--When ever the child RA is updated, the parent RA in parent table has to be updated ,so ETL can Sync it to GOP.
		IF v_parent_raid IS NOT NULL
		THEN
		 UPDATE t506_returns
		   SET C506_LAST_UPDATED_BY = p_userid, 
		       C506_LAST_UPDATED_DATE = CURRENT_DATE
		WHERE C506_rma_Id = v_parent_raid;
		END IF;
		
		/*	SELECT COUNT (1)
			  INTO v_rtn_count
			  FROM t507_returns_item t507
			 WHERE get_part_attribute_value (t507.c205_part_number_id, 92340) = 'Y'
			   AND t507.c507_status_fl = 'C'
			   AND t507.c506_rma_id = p_raid;

			SELECT COUNT (1)
			  INTO v_tag_count
			  FROM t5010_tag t5010
			 WHERE t5010.c5010_last_updated_trans_id = p_raid AND t5010.c5010_void_fl IS NULL;*/

		

		-- For ICT returns Inter-Company Sale(ICS)
		IF v_dist_type = 70105
		THEN
			UPDATE t506_returns
			   SET c506_type = 3307
			     , c506_last_updated_by = p_userid
				 , c506_last_updated_date = CURRENT_DATE
			 WHERE c506_rma_id = p_raid;

			v_upd_inv_type := 'ICR';
		END IF;
		
		-- Skipping FS warehouse for ICS distributor

		SELECT NVL(get_rule_value(v_dist_type,'SKIP_FS_WAREHOUSE'), 'N') INTO v_skip_fs_ws_flag FROM DUAL;

		-- Save Excess Quantity (PBUG-2218: before update the FS warehouse to get the correct Excess qty and create the GM-EX-xxx transaction) 
		-- to avoid the internation distributor to hit the excess transactions (PMT-20929)
		
		IF (v_dist_id IS NOT NULL AND v_dist_parent_comp_id IS NULL AND v_skip_fs_ws_flag <> 'Y')
		THEN
			gm_pkg_op_excess_return.gm_op_save_excess_qty (p_raid, p_userid, v_dist_id);
		END IF;

		
		-- code to perform posting
		--
		IF v_inventory_fl IS NULL
		THEN
			-- to avoid the FS hit for international distributor
			IF (v_dist_id IS NOT NULL AND v_skip_fs_ws_flag <> 'Y' AND v_dist_parent_comp_id IS NULL) OR v_rettype = '3304' --3304 account consignment - items 
			THEN
			-- update the FS warehouse Qty
			-- 102901 - Return
			-- 102908 - Return
			-- 3304 - Account Item consignement
			-- 3302 - Consignment - Items
			
				SELECT DECODE(v_rettype,'3304',v_rettype,NULL) INTO v_rettype_tmp FROM DUAL;
				SELECT DECODE(v_rettype,'3304',v_account,v_dist_id) INTO v_fs_id FROM DUAL; -- Field sales or Account cannot be empty 

			
				 gm_pkg_op_inv_field_sales_tran.gm_update_fs_inv(p_raid, 'T506', v_fs_id, 102901, 4302, 102908, p_userid, v_rettype_tmp);				 
			END IF;
		
			gm_update_inventory (p_raid, v_upd_inv_type, '', p_userid, v_msg);
			
			--This Procedure is used to insert the entry in T5060 table for the control number returning from Set Consignment
			IF v_rettype = '3301' THEN --Consignment - Sets
				gm_pkg_op_lot_track_txn.gm_pkg_op_lot_track_for_returns(p_raid , p_userid);
			END IF;
			
			--
			UPDATE t506_returns
			   SET c506_update_inv_fl = '1'
			     , c506_last_updated_by = p_userid
				 , c506_last_updated_date = CURRENT_DATE
			 WHERE c506_rma_id = p_raid AND c506_void_fl IS NULL;
		--
			
		END IF;

		-- For ICT returns Inter-Company Sale(ICS),ICT,ICA
		IF v_dist_type = 70105 OR v_dist_type = 70103 OR v_dist_type = 70106
		THEN
			gm_pkg_op_ict.gm_po_sav_ict_returns (p_raid, v_dist_id, p_userid, v_dist_type);
		END IF;
		
		-- UPDATE the missing transaction for IH and Product Loaner Items missing to "Open" status
		IF v_rettype IN ('3308','3309') 
		THEN
					BEGIN
						SELECT C412_INHOUSE_TRANS_ID INTO v_txnid 
							FROM 
							 T412_INHOUSE_TRANSACTIONS 
							 WHERE 
							 C412_REF_ID = p_raid AND C412_VOID_FL IS NULL;
							 
					EXCEPTION WHEN NO_DATA_FOUND
					THEN
						v_txnid := NULL;
					END;
			 
			 		IF v_txnid IS NOT NULL
			 		THEN
			 			gm_pkg_op_trans_recon.gm_sav_ln_status(v_txnid,'3',p_userid);
			 		END IF;
		END IF;
		
		-- save "Return" status: Credit
		gm_save_status_details (p_raid, ' ', p_userid, NULL, CURRENT_DATE, '91152', '91100');
		
		IF v_set_id IS NOT NULL THEN
		
		-- Fetching last updated id from tag log table
				 SELECT v_last_id INTO v_last_id  FROM(
		SELECT MAX(c5011_tag_txn_log_id) as v_max_id,
		  c5011_last_updated_trans_id as v_last_id
		FROM t5011_tag_log ,t504_consignment 
		WHERE c5010_tag_id in (SELECT C5010_TAG_ID
		  FROM t5010_tag
		  WHERE C5010_LAST_UPDATED_TRANS_ID = p_raid)
		AND c5011_void_fl IS NULL
        and c5011_last_updated_trans_id = c504_consignment_id
        and c504_void_fl is null
		GROUP BY c5011_last_updated_trans_id,c5011_tag_txn_log_id
        ORDER BY c5011_tag_txn_log_id DESC
		) WHERE ROWNUM =1;   
		
		
		-- void the back orders
		gm_pkg_ret_pend_credit.gm_void_open_bo_request(v_last_id);
		
		
	END IF;
	END gm_op_sav_return_forcredit;

/********************************************************************************************
 * Description	: This Procedure will be called to initiate order return
 *********************************************************************************************/
/*This procedure is calling by ETL Job, If there is any changes then please coordinate with DWH team during design phase */
	PROCEDURE gm_op_sav_order_return (
		p_type			   IN		t506_returns.c506_type%TYPE
	  , p_reason		   IN		t506_returns.c506_reason%TYPE
	  , p_distid		   IN		VARCHAR2
	  , p_comments		   IN		t506_returns.c506_comments%TYPE
	  , p_expdate		   IN		DATE
	  , p_orderid		   IN		t506_returns.c501_order_id%TYPE
	  , p_userid		   IN		t506_returns.c506_created_by%TYPE
	  , p_parent_orderid   IN		t506_returns.c501_order_id%TYPE
	  , p_empname		   IN		t101_user.c101_user_id%TYPE
	  , p_str			   IN		VARCHAR2
	  , p_raid			   OUT		VARCHAR2
	)
	AS
--
		v_id		   NUMBER;
		v_string	   VARCHAR2 (30000);
		v_id_string    VARCHAR2 (20);
		v_price 	   VARCHAR2 (20);
		v_date		   DATE;
		v_empname	   NUMBER;
		v_status	   CHAR (1) := 0;
		v_ret_date	   DATE;
		v_rep_date	   DATE;
		-- Loop variable declaration
		v_strlen	   NUMBER := 0;
		v_substring    VARCHAR2 (1000);
		v_pnum		   VARCHAR2 (20);
		v_qty		   NUMBER;
		v_control	   VARCHAR2 (20);
		v_orgcontrol   VARCHAR2 (20);
		v_itemtype	   NUMBER;
		v_order_id	   t506_returns.c501_order_id%TYPE;
		v_message      VARCHAR2 (2000);
		v_orig_order_type   VARCHAR2 (20);
		v_order_attr_id   NUMBER;
		v_unit_price   VARCHAR2(50);
		v_unit_price_adj   VARCHAR2(50);
		v_adj_code   VARCHAR2(50);
		v_list_price  VARCHAR2 (20);
		v_do_unit_price		t507_returns_item.c507_do_unit_price%TYPE;
		v_adj_code_val		t507_returns_item.c507_adj_code_value%TYPE;
		v_item_ord_id		t502_item_order.c502_item_order_id%TYPE;
		v_adj_code_per		VARCHAR2(10);
		v_acc_id			t501_order.c704_account_id%TYPE;
		v_party_id			t740_gpo_account_mapping.c101_party_id%TYPE;
		--v_acc_price			t502_item_order.c502_unit_price%TYPE;
		v_company_id  t1900_company.c1900_company_id%TYPE;
		v_plant_id 	  t5040_plant_master.c5040_plant_id%TYPE;
		
--
	BEGIN
		SELECT get_compid_frm_cntx(), get_plantid_frm_cntx()
          INTO v_company_id, v_plant_id
          FROM DUAL;
		--
		SELECT 'GM-RA-' || s506_return.NEXTVAL
		  INTO p_raid
		  FROM DUAL;

		--
		IF LENGTH (p_expdate) < 1
		THEN
			v_date		:= TO_DATE (CURRENT_DATE + 4, NVL(get_compdtfmt_frm_cntx(),'mm/dd/yyyy'));
		ELSE
			v_date		:= p_expdate;
		END IF;

		--
		--if return reason is 'Duplicate Order', save the parent order id in the comment log if it exist.
	 	IF (p_reason              = '3312') THEN
		    IF (p_parent_orderid IS NOT NULL) THEN
		        GM_UPDATE_LOG (p_parent_orderid, 'Parent order is '||p_parent_orderid, 1200, p_userid, v_message) ;
		    END IF;
		    -- get original order type
		     SELECT TO_CHAR (c901_order_type)
		       INTO v_orig_order_type
		       FROM t501_order
		      WHERE c501_order_id = p_orderid;
		    --
			BEGIN
			 SELECT c501a_order_attribute_id
			   INTO v_order_attr_id
			   FROM t501a_order_attribute
			  WHERE c501_order_id       = p_orderid
			    AND c901_attribute_type = 101720;
			EXCEPTION
			 WHEN NO_DATA_FOUND THEN
				v_order_attr_id := NULL;
			END;
		    --save original order type in the order attribute table, 101720 (original order type)
		    gm_pkg_cm_order_attribute.gm_sav_order_attribute (p_orderid, 101720, v_orig_order_type, v_order_attr_id, p_userid)
		    ;
		END IF;
	 	
		-- Mark the order as duplicate order or as Item Return consignment
		gm_order_return (p_orderid, p_type, p_reason, p_parent_orderid); 
		
		--
		INSERT INTO t506_returns
					(c506_rma_id, c704_account_id, c501_order_id, c506_type, c506_reason, c506_comments
				   , c506_expected_date, c506_status_fl, c506_created_by, c506_created_date, c506_return_date
				   , c501_reprocess_date, c1900_company_id, c5040_plant_id
					)
			 VALUES (p_raid, p_distid, p_orderid, p_type, p_reason, p_comments
				   , v_date, v_status, p_userid, CURRENT_DATE, v_ret_date
				   , v_rep_date, v_company_id, v_plant_id
					);
        gm_pkg_cm_status_log.gm_sav_status_details (p_orderid, '', p_userid, NULL, CURRENT_DATE, 91178, 91102);
		--
		
		
		-- Load item information
		v_string	:= p_str;
		v_strlen	:= NVL (LENGTH (p_str), 0);

		--
		WHILE INSTR (v_string, '|') <> 0
		LOOP
			--
			v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
			v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
			v_pnum		:= NULL;
			v_qty		:= NULL;
			v_control	:= NULL;
			v_orgcontrol := NULL;
			v_itemtype	:= NULL;
			v_price     := NULL;
			v_unit_price := NULL;
			v_unit_price_adj := NULL;
			v_adj_code := NULL;
			v_pnum		:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_qty		:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_control	:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_orgcontrol := SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_itemtype	:= TO_NUMBER (SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1));
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_price	:= TO_NUMBER (SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1));
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_unit_price	:= TO_NUMBER (SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1));
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_unit_price_adj	:= TO_NUMBER (SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1));
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_adj_code	:=  TO_NUMBER (v_substring);
			--
			IF v_price is NULL THEN
     BEGIN 
	     
			SELECT c502_item_price,c502_list_price
			  INTO v_price , v_list_price
			  FROM t502_item_order
			 WHERE c501_order_id = p_orderid
			   AND c205_part_number_id = v_pnum
			   AND c502_control_number = v_orgcontrol 
			   AND C502_VOID_FL IS NULL 
			   AND ROWNUM = 1;
     EXCEPTION WHEN NO_DATA_FOUND THEN
      v_price := 0;
     END;
     END IF;

	BEGIN
	     SELECT c704_account_id into v_acc_id
	     FROM t501_order
	     WHERE c501_order_id = p_orderid
	     AND c501_void_fl IS NULL;
	     EXCEPTION WHEN NO_DATA_FOUND THEN
	      	v_acc_id := NULL;
	END;
	BEGIN
	     SELECT t740.c101_party_id 
	     	INTO v_party_id
	     FROM t740_gpo_account_mapping t740 
	     WHERE t740.c704_account_id = v_acc_id
	     AND c740_void_fl IS NULL;
	     EXCEPTION WHEN NO_DATA_FOUND THEN
	      	v_party_id := NULL;
	END;
			v_adj_code_per := gm_pkg_sm_adj_txn.get_adj_code_value(p_orderid,v_pnum,v_item_ord_id, v_adj_code);
			v_adj_code_val	:= ((NVL(v_unit_price,0) - NVL(v_unit_price_adj,0)) * NVL(v_adj_code_per,0)/100);
			-- Get the unit price using below formula, net unit price + unit adjustment value + adjustment code value
	        --v_acc_price := NVL(v_price,0) + NVL(v_unit_price_adj,0) + NVL(v_adj_code_val,0);
			v_do_unit_price := NVL(gm_pkg_sm_adj_rpt.get_account_part_unitprice(v_acc_id,v_pnum, v_party_id),0);
	        
			INSERT INTO t507_returns_item
						(c507_returns_item_id, c506_rma_id, c205_part_number_id, c507_control_number, c507_item_qty
					   , c507_item_price, c507_status_fl, c507_org_control_number, c901_type ,C507_UNIT_PRICE,C507_UNIT_PRICE_ADJ_VALUE ,C901_UNIT_PRICE_ADJ_CODE , C507_LIST_PRICE
					   , C507_DO_UNIT_PRICE, C507_ADJ_CODE_VALUE
						)
				 VALUES (s507_return_item.NEXTVAL, p_raid, v_pnum, v_control, v_qty
					   , v_price, 'R', NVL (v_orgcontrol, '-'), v_itemtype , v_unit_price , v_unit_price_adj , v_adj_code , v_list_price
					   , v_do_unit_price, v_adj_code_val
						);
						
			--- Added for PMT-20882
			UPDATE t5062_control_number_reserve t5062
			SET t5062.c901_status                     = 103962 ,
			  t5062.c5062_reserved_qty                = 0
			WHERE t5062.c5062_control_number_resv_id IN
			  (SELECT t5062.c5062_control_number_resv_id
			  FROM t5062_control_number_reserve t5062 ,
			    t5060_control_number_inv t5060
			  WHERE t5060.c5060_control_number_inv_id = t5062.c5060_control_number_inv_id
			  AND T5060.C5060_CONTROL_NUMBER          = v_control
			  )
			AND T5062.C5062_RESERVED_QTY = 1 ;
			
			-----To update company plant and donor id when parts return back from globus medical inc to BBA (TSK-10595) PMT-21348
		IF v_company_id = 1001 THEN
		
	     gm_pkg_op_return_txn.gm_update_bba_donor_mapping(v_pnum,v_control,v_company_id, v_plant_id,p_userid);
		
			END IF;
		IF (p_reason = '3253' OR p_reason = '26240385') THEN
			gm_pkg_qa_com_rsr_txn.gm_sav_ra_incident_link(p_raid,v_pnum, v_qty, p_userid);
 			END IF;	
		
		END LOOP;

		INSERT INTO t507_returns_item
					(c507_returns_item_id, c506_rma_id, c205_part_number_id, c507_item_qty, c507_item_price
				   , c507_status_fl,C507_UNIT_PRICE,C507_UNIT_PRICE_ADJ_VALUE,C901_UNIT_PRICE_ADJ_CODE,C507_LIST_PRICE, C507_DO_UNIT_PRICE, C507_ADJ_CODE_VALUE)
			SELECT s507_return_item.NEXTVAL, p_raid, c205_part_number_id, item_qty, c507_item_price, 'Q' , C507_UNIT_PRICE,C507_UNIT_PRICE_ADJ_VALUE ,C901_UNIT_PRICE_ADJ_CODE,C507_LIST_PRICE
				, C507_DO_UNIT_PRICE, C507_ADJ_CODE_VALUE
			  FROM (SELECT	 c205_part_number_id, 0 c507_item_price, SUM (c507_item_qty) item_qty ,  C507_UNIT_PRICE,C507_UNIT_PRICE_ADJ_VALUE ,C901_UNIT_PRICE_ADJ_CODE , C507_LIST_PRICE
			  		, C507_DO_UNIT_PRICE, C507_ADJ_CODE_VALUE
						FROM t507_returns_item
					   WHERE c506_rma_id = p_raid AND c507_status_fl = 'R'
					GROUP BY c205_part_number_id ,  C507_UNIT_PRICE,C507_UNIT_PRICE_ADJ_VALUE ,C901_UNIT_PRICE_ADJ_CODE,C507_LIST_PRICE, C507_DO_UNIT_PRICE, C507_ADJ_CODE_VALUE
					, C507_DO_UNIT_PRICE, C507_ADJ_CODE_VALUE
					);
					

		
		SELECT COUNT (1)
		  INTO v_qty
		  FROM (SELECT *
				  FROM t507_returns_item
				 WHERE c506_rma_id = p_raid AND c507_status_fl = 'R');

--
		-- 3317 means Parts Swap (Loaner to Consign)
		-- used by loaner program
		IF p_reason = '3317'
		THEN
			UPDATE t506_returns
			   SET c506_status_fl = 1
				 , c506_return_date = CURRENT_DATE
				 , c506_last_updated_by = p_userid
				 , c506_last_updated_date = CURRENT_DATE
			 WHERE c506_rma_id = p_raid;

			--
			UPDATE t507_returns_item
			   SET c507_status_fl = 'C'
				 , c507_control_number = 'NOC#'
			 WHERE c506_rma_id = p_raid AND c507_status_fl = 'R';
		END IF;
		--Save Return status: Initiate
		 gm_pkg_cm_status_log.gm_sav_status_details (p_raid, '', p_userid, NULL, CURRENT_DATE, 91150, 91100);	--91100 'Return'; 91150 Initiate
	END gm_op_sav_order_return;

/********************************************************************************************
 * Description	: This Procedure will be link the rep based on the tag
 *********************************************************************************************/
	PROCEDURE gm_tag_return_ra (
		p_raid	   IN		t506_returns.c506_rma_id%TYPE
	  , p_pnum	   IN		t507_returns_item.c205_part_number_id%TYPE
	  , p_cnum	   IN		t507_returns_item.c507_control_number%TYPE
	  , p_userid   IN		t101_user.c101_user_id%TYPE
	  , p_flag				VARCHAR2
	  , p_errmsg   OUT		VARCHAR2
	)
	AS
		v_id		   t5011_tag_log.c5011_tag_txn_log_id%TYPE;
		v_con_dist_id  t504_consignment.c701_distributor_id%TYPE;
		v_int_dist	   t506_returns.c701_distributor_id%TYPE;
		v_type		   t506_returns.c506_type%TYPE;
		v_new_ra	   VARCHAR2 (20);
		v_count 	   NUMBER;
		v_mis_count    NUMBER;
		v_ship_type    t504_consignment.c504_ship_to%TYPE;
		v_ship_id	   t504_consignment.c504_ship_to_id%TYPE;
		v_sales_rep    t703_sales_rep.c703_sales_rep_id%TYPE;
		item_count	   NUMBER;
		v_set_id	   t506_returns.c207_set_id%TYPE;
		v_transid      t506_returns.c506_ref_id%TYPE;
		v_company_id   t506_returns.c1900_company_id%TYPE;
		v_plant_id	   t506_returns.c5040_plant_id%TYPE;
    
		CURSOR cur_tag_detail
		IS
			SELECT t5010.c5010_tag_id tag_id, t5010.c5010_control_number c_num, t5010.c207_set_id set_id
				 , t5010.c205_part_number_id pnum
			  FROM t5010_tag t5010
			 WHERE t5010.c5010_last_updated_trans_id = p_raid
			   AND t5010.c205_part_number_id = p_pnum
			   AND t5010.c5010_control_number = p_cnum
			   AND t5010.c5010_void_fl IS NULL;
	BEGIN
		SELECT COUNT (1)
		  INTO v_count
		  FROM t5010_tag t5010
		 WHERE t5010.c5010_last_updated_trans_id = p_raid
		   AND t5010.c205_part_number_id = p_pnum
		   AND t5010.c5010_control_number = p_cnum;

		SELECT COUNT (1)
		  INTO v_mis_count
		  FROM t507_returns_item t507
		 WHERE t507.c506_rma_id = p_raid 
		   AND t507.c507_missing_fl = 'Y';

		IF (v_count = 0 AND v_mis_count = 0)
		THEN
			raise_application_error ('-20209', '');
		END IF;

		SELECT	   c701_distributor_id, c506_type, c207_set_id, c1900_company_id, c5040_plant_id
			  INTO v_int_dist, v_type, v_set_id, v_company_id, v_plant_id
			  FROM t506_returns
			 WHERE c506_rma_id = p_raid
		FOR UPDATE;

		--Tag is Consigned
		IF v_type IN ('26240177','3307')  --26240177:ICT Returns - item,3307:ICT Returns - Sets
		THEN
		FOR tag_cur IN cur_tag_detail
		LOOP
			UPDATE t5010_tag
			   SET c901_location_type = 40033 --Inhouse
				 , c5010_location_id = 52099 --Return
				 , c901_status = 51012 --active
				 , c901_sub_location_type = NULL
                 , c5010_sub_location_id = NULL
				 , c5010_last_updated_by = p_userid
				 , c5010_last_updated_date = CURRENT_DATE
				 , c1900_company_id = v_company_id
      			 , c5040_plant_id    = v_plant_id
			 WHERE c5010_tag_id = tag_cur.tag_id;
		END LOOP;
		
		RETURN; 
		
        END IF;
		
		IF (v_type = '3301' AND v_mis_count > 0) OR (v_type = 3306 AND v_set_id IS NOT NULL AND v_mis_count > 0)
		THEN
			UPDATE t507_returns_item
			   SET c507_hold_fl = 'Y'
			 WHERE c506_rma_id = p_raid;
			 
			 --When ever the child is updated, the parent table has to be updated ,so ETL can Sync it to GOP.
		 	UPDATE t506_returns
		  	   SET C506_LAST_UPDATED_BY = p_userid, 
				   C506_LAST_UPDATED_DATE = CURRENT_DATE
		     WHERE C506_rma_Id = p_raid;
		END IF;

		IF (v_type = '3302' AND v_mis_count > 0) OR (v_type = 3306 AND v_set_id IS NULL AND v_mis_count > 0)
		THEN
			UPDATE t507_returns_item
			   SET c507_hold_fl = 'Y'
			 WHERE c506_rma_id = p_raid AND c507_missing_fl = 'Y';
			 
			--When ever the child is updated, the parent table has to be updated ,so ETL can Sync it to GOP.
		 	UPDATE t506_returns
		  	   SET C506_LAST_UPDATED_BY = p_userid, 
				   C506_LAST_UPDATED_DATE = CURRENT_DATE
		     WHERE C506_rma_Id = p_raid;
		END IF;

		p_errmsg	:= '';

		FOR tag_cur IN cur_tag_detail
		LOOP
			BEGIN
				SELECT t504.c701_distributor_id, t504.c504_ship_to, t504.c504_ship_to_id
				  INTO v_con_dist_id, v_ship_type, v_ship_id
				  FROM t5011_tag_log t5011, t504_consignment t504
				 WHERE c5011_tag_txn_log_id IN (
										   SELECT MAX (t5011.c5011_tag_txn_log_id)
											 FROM t5011_tag_log t5011
											WHERE t5011.c5010_tag_id = tag_cur.tag_id
												  AND t5011.c901_trans_type = '51000')
				   AND t504.c504_consignment_id = c5011_last_updated_trans_id
				   AND t504.c504_void_fl IS NULL
				   AND t5011.c5011_void_fl IS NULL;
			EXCEPTION
				WHEN NO_DATA_FOUND
				THEN
					BEGIN
						SELECT t5011.c5011_location_id, 4120
						  INTO v_con_dist_id, v_ship_type
						  FROM t5011_tag_log t5011
						 WHERE c5011_tag_txn_log_id IN (
								   SELECT MAX (t5011.c5011_tag_txn_log_id)
									 FROM t5011_tag_log t5011
									WHERE t5011.c5010_tag_id = tag_cur.tag_id
									  AND t5011.c901_trans_type = '51000'
									  AND t5011.c901_location_type = 4120
									  AND t5011.c5011_void_fl IS NULL);
					EXCEPTION
						WHEN NO_DATA_FOUND
						THEN
							raise_application_error ('-20215', '');
					END;
			END;

			IF (v_con_dist_id != v_int_dist)
			THEN
				SELECT 'GM-RA-' || s506_return.NEXTVAL
				  INTO v_new_ra
				  FROM DUAL;

				v_sales_rep := '';

				IF (v_ship_type = '4121')
				THEN
					v_sales_rep := v_ship_id;
				ELSIF (v_ship_type = '4122')
				THEN
					SELECT c703_sales_rep_id
					  INTO v_sales_rep
					  FROM t704_account
					 WHERE c704_account_id = v_ship_id;
				END IF;

				p_errmsg	:= p_errmsg || ' ' || v_new_ra;

				IF (v_type = '3301' OR (v_type = 3306 AND v_set_id IS NOT NULL))   ---Consignment - Sets
				THEN
					INSERT INTO t506_returns
								(c506_rma_id, c701_distributor_id, c506_type, c506_reason, c506_expected_date
							   , c506_status_fl, c704_account_id, c101_user_id, c207_set_id, c506_created_by
							   , c506_return_date, c506_created_date, c506_associated_rma_id, c703_sales_rep_id
							   , c1900_company_id, c5040_plant_id)
						SELECT v_new_ra, v_con_dist_id, c506_type, c506_reason, c506_expected_date, 1, c704_account_id
							 , c101_user_id, tag_cur.set_id, p_userid, CURRENT_DATE, c506_return_date, p_raid, v_sales_rep
							 , c1900_company_id, c5040_plant_id
						  FROM t506_returns
						 WHERE c506_rma_id = p_raid AND c506_void_fl IS NULL;

					INSERT INTO t507_returns_item
								(c507_returns_item_id, c506_rma_id, c205_part_number_id, c507_control_number
							   , c507_item_qty, c507_item_price, c507_status_fl, c901_type)
						SELECT s507_return_item.NEXTVAL, v_new_ra, c205_part_number_id, c507_control_number
							 , c507_item_qty, c507_item_price, c507_status_fl, c901_type
						  FROM t507_returns_item
						 WHERE c506_rma_id = p_raid AND c507_status_fl <> 'Q' AND c507_control_number IS NOT NULL;

					DELETE FROM t507_returns_item
						  WHERE c506_rma_id = p_raid AND c507_status_fl <> 'Q';

					UPDATE t506_returns t506
					   SET t506.c506_void_fl = 'Y'
					     , c506_last_updated_by = p_userid
						 , c506_last_updated_date = CURRENT_DATE
					 WHERE t506.c506_rma_id = p_raid AND t506.c506_void_fl IS NULL;
				END IF;

				IF (v_type = '3302' OR (v_type = 3306 AND v_set_id IS NULL))   ---Consignment - Item
				THEN
					INSERT INTO t506_returns
								(c506_rma_id, c701_distributor_id, c506_type, c506_reason, c506_expected_date
							   , c506_status_fl, c704_account_id, c101_user_id, c207_set_id, c506_created_by
							   , c506_created_date, c506_return_date, c506_associated_rma_id, c703_sales_rep_id
							   , c1900_company_id, c5040_plant_id)
						SELECT v_new_ra, v_con_dist_id, c506_type, c506_reason, c506_expected_date, 1, c704_account_id
							 , c101_user_id, tag_cur.set_id, p_userid, CURRENT_DATE, c506_return_date, p_raid, v_sales_rep
							 , c1900_company_id, c5040_plant_id
						  FROM t506_returns
						 WHERE c506_rma_id = p_raid AND c506_void_fl IS NULL;

					INSERT INTO t507_returns_item
								(c507_returns_item_id, c506_rma_id, c205_part_number_id, c507_control_number
							   , c507_item_qty, c507_item_price, c507_status_fl, c901_type)
						SELECT s507_return_item.NEXTVAL, v_new_ra, c205_part_number_id, c507_control_number, 1
							 , c507_item_price, 'R', c901_type
						  FROM t507_returns_item
						 WHERE c506_rma_id = p_raid
						   AND c205_part_number_id = tag_cur.pnum
						   AND c507_control_number = tag_cur.c_num
						   AND c507_hold_fl IS NULL
						   -- AND c507_status_fl <> 'Q'
						   AND ROWNUM < 2;

					UPDATE t507_returns_item
					   SET c507_item_qty = (c507_item_qty - 1)
					 WHERE c506_rma_id = p_raid
					   AND c205_part_number_id = tag_cur.pnum
					   AND c507_control_number = tag_cur.c_num
					   AND c507_status_fl <> 'Q'
					   AND c507_hold_fl IS NULL
					   AND ROWNUM < 2;

					DELETE		t507_returns_item
						  WHERE c506_rma_id = p_raid
							AND c205_part_number_id = tag_cur.pnum
							AND c507_control_number = tag_cur.c_num
							AND c507_item_qty = 0;

						--Whenever the detail is updated, the return table has to be updated ,so ETL can Sync it to GOP.
						UPDATE t506_returns t506
						   SET  c506_last_updated_by = p_userid
							 , c506_last_updated_date = CURRENT_DATE
						 WHERE t506.c506_rma_id = p_raid;	
						 
					SELECT COUNT (1)
					  INTO item_count
					  FROM t507_returns_item
					 WHERE c506_rma_id = p_raid AND c507_status_fl <> 'Q' AND c507_control_number IS NOT NULL;

					IF (item_count < 1)
					THEN
						UPDATE t506_returns t506
						   SET t506.c506_void_fl = 'Y'
						   	 , c506_last_updated_by = p_userid
							 , c506_last_updated_date = CURRENT_DATE
						 WHERE t506.c506_rma_id = p_raid AND t506.c506_void_fl IS NULL;
					END IF;
				--	SELECT	 COUNT (1)
					--	INTO v_count
					--	FROM t507_returns_item
					--	 WHERE c506_rma_id = p_cra_id
				--	GROUP BY c205_part_number_id;
				END IF;

				UPDATE t5010_tag
				   SET c5010_last_updated_trans_id = v_new_ra
					 , c5010_last_updated_by = p_userid
					 , c5010_last_updated_date = CURRENT_DATE
				 WHERE c5010_tag_id = tag_cur.tag_id;
			END IF;
		END LOOP;
	END;

--
/********************************************************************
 * Description : This function is used to get the return reason for RA
 ********************************************************************/
--
	FUNCTION get_return_reason (
		p_refid   IN   t506_returns.c506_rma_id%TYPE
	  , p_type	  IN   VARCHAR2
	)
		RETURN NUMBER
	IS
		v_reason	   NUMBER;
	BEGIN
		IF p_type = 'orders'
		THEN
			SELECT t506.c506_reason
			  INTO v_reason
			  FROM t501_order t501, t506_returns t506
			 WHERE t501.c501_order_id = p_refid AND t501.c506_rma_id = t506.c506_rma_id AND t506.c506_void_fl IS NULL;
		ELSE
			SELECT t506.c506_reason
			  INTO v_reason
			  FROM t506_returns t506
			 WHERE t506.c506_rma_id = p_refid AND t506.c506_void_fl IS NULL;
		END IF;

		--
		RETURN v_reason;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN '';
	END;

	--
/********************************************************************
 * Description : This function is used to get the associated RA's sales rep
 ********************************************************************/
--
	FUNCTION get_associated_rep_name (
		p_refid   IN   t506_returns.c506_associated_rma_id%TYPE
	)
		RETURN VARCHAR2
	IS
		v_rep_nm	   t703_sales_rep.c703_sales_rep_name%TYPE;
	BEGIN
		SELECT get_rep_name (c703_sales_rep_id) repname
		  INTO v_rep_nm
		  FROM t506_returns
		 WHERE c506_rma_id = p_refid;

		--
		RETURN v_rep_nm;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN NULL;
	END;

--
/********************************************************************
 * Description : This function is used to get the associated RA's sales rep Email Id
 ********************************************************************/
--
	FUNCTION get_associated_rep_email (
		p_refid   IN   t506_returns.c506_associated_rma_id%TYPE
	)
		RETURN VARCHAR2
	IS
		v_ship_to	   t107_contact.c107_contact_value%TYPE;
		v_party_id     t703_sales_rep.c101_party_id%TYPE;
		v_rep_nm	   t703_sales_rep.c703_sales_rep_name%TYPE;
		v_ship_to_id   t506_returns.c703_sales_rep_id%TYPE;
		v_dist_id	   t504_consignment.c701_distributor_id%TYPE;
	BEGIN	
		/* Removed the default email code, because CC is 
		 *  not needed if ASS is not available
		 */
		
		SELECT c703_sales_rep_id, c701_distributor_id
		  INTO v_ship_to_id, v_dist_id
		  FROM t506_returns
		 WHERE c506_rma_id = p_refid;

		v_party_id	:= TRIM (get_rep_party_id (v_ship_to_id));
		v_ship_to	:= TRIM (gm_pkg_cm_contact.get_contact_value (v_party_id, 90452));

		IF (TRIM (v_ship_to) IS NULL)
		THEN
			v_ship_to	:= get_distributor_email (v_dist_id);
		END IF;

		--
		RETURN v_ship_to;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN NULL;
	END;

--

	/********************************************************************************************
	* Description	: This Procedure used to Reconcile missing the tags
	*********************************************************************************************/
	PROCEDURE gm_tag_reconciliation (
		p_inputs	IN		 VARCHAR2
	  , p_userid	IN		 t301_vendor.c301_last_updated_by%TYPE
	  , p_out_msg	OUT 	 VARCHAR2
	)
	AS
		v_strlen	   NUMBER := NVL (LENGTH (p_inputs), 0);
		v_string	   VARCHAR2 (4000) := p_inputs;
		v_substring    VARCHAR2 (4000);
		v_tagid 	   t5010_tag.c5010_tag_id%TYPE;
		v_control_num  t5010_tag.c5010_control_number%TYPE;
		v_partnum	   t5010_tag.c205_part_number_id%TYPE;
		v_raid		   t506_returns.c506_rma_id%TYPE;
		v_type		   t506_returns.c506_type%TYPE;
		v_userid	   t101_user.c101_user_id%TYPE;
		v_missing_fl   t507_returns_item.c507_missing_fl%TYPE;
		v_count 	   NUMBER;
		v_ship_type    t504_consignment.c504_ship_to%TYPE;
		v_ship_id	   t504_consignment.c504_ship_to_id%TYPE;
		v_sales_rep    t703_sales_rep.c703_sales_rep_id%TYPE;
		v_excess	   t504_consignment.c504_consignment_id%TYPE;
		v_con_dist_id  t504_consignment.c701_distributor_id%TYPE;
		v_return_dist  t506_returns.c701_distributor_id%TYPE;
		v_setid 	   t5010_tag.c207_set_id%TYPE;
		v_txn_id	   t902_log.c902_ref_id%TYPE;
		v_part_inputstr VARCHAR2 (4000);
		v_inputstr	   VARCHAR2 (4000);
		v_message	   VARCHAR2 (3000);
		v_tagged_pnum  t5010_tag.c205_part_number_id%TYPE;
		v_excess_count NUMBER;
		v_newexcess    t504_consignment.c504_consignment_id%TYPE;
		v_distid	   t506_returns.c701_distributor_id%TYPE;
		v_distid_passed t506_returns.c701_distributor_id%TYPE;
		v_tag_status   NUMBER;
		v_set_id	   t506_returns.c207_set_id%TYPE;
		v_setid_passed t506_returns.c207_set_id%TYPE;
		v_reason	   t507_returns_item.c901_missing_tag_reason%TYPE;
		v_qty		   NUMBER;
		v_out_errmsg   VARCHAR2 (4000);
		v_void_fl	   VARCHAR2 (1);
		v_reason_exist t507_returns_item.c901_missing_tag_reason%TYPE;
		v_msg_post_ex  VARCHAR2 (2000);
		v_msg_untag_set VARCHAR2 (1000);
		v_msg_consign  VARCHAR2 (1000);
		v_status_fl    NUMBER;
		v_msg		   VARCHAR2 (200);
		v_setid_ck	   t5010_tag.c207_set_id%TYPE;
		v_company_id  t1900_company.c1900_company_id%TYPE;
		v_plant_id 	  t5040_plant_master.c5040_plant_id%TYPE;
		v_dateformat varchar2(100);
		-- International posting changes.
		v_costing_id t820_costing.c820_costing_id%TYPE;
		v_owner_company_id t1900_company.c1900_company_id%TYPE;
		v_local_cost t820_costing.c820_purchase_amt%TYPE;
		v_cost t820_costing.c820_purchase_amt%TYPE;

		CURSOR v_rtn_cur
		IS
			SELECT t507.c205_part_number_id pnum, t507.c507_item_qty qty, t507.c507_control_number cnum
			  FROM t507_returns_item t507
			 WHERE t507.c506_rma_id = v_raid AND t507.c507_status_fl = 'C';

		CURSOR v_item_rtn_cur
		IS
			SELECT t507.c205_part_number_id pnum, t507.c507_item_qty qty, t507.c507_control_number cnum
			  FROM t507_returns_item t507
			 WHERE t507.c506_rma_id = v_raid
			   AND t507.c507_status_fl = 'C'
			   AND t507.c205_part_number_id = v_partnum
			   AND t507.c507_control_number = v_control_num
			   AND t507.c507_missing_fl = 'Y'
			   AND ROWNUM < 2;

		CURSOR excess_part_nums
		IS
			SELECT c205_part_number_id part_num, c505_item_qty qty
			  FROM t505_item_consignment
			 WHERE c504_consignment_id = v_excess;
	BEGIN
		
		SELECT get_plantid_frm_cntx()  INTO  v_plant_id   FROM DUAL;                                 
		SELECT get_compid_frm_cntx() INTO  v_company_id FROM DUAL;
		SELECT get_compdtfmt_frm_cntx() INTO v_dateformat FROM dual;
		
		IF v_strlen > 0
		THEN
			WHILE INSTR (v_string, '|') <> 0
			LOOP
				--
				v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
				v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
				--
				v_raid		:= NULL;
				v_type		:= NULL;
				v_tagid 	:= NULL;
				v_excess	:= NULL;
				v_partnum	:= NULL;
				v_control_num := NULL;
				v_setid 	:= NULL;
				--	v_ref_type	:= NULL;

				--
				v_raid		:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_type		:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_tagid 	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_excess	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_partnum	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_control_num := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_reason	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_setid_passed := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_distid_passed := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_qty		:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);

				IF v_tagid IS NULL
				THEN
					--by default, execute the default update condition
					SELECT t507.c901_missing_tag_reason
					  INTO v_reason_exist
					  FROM t507_returns_item t507
					 WHERE t507.c506_rma_id = v_raid
					   AND t507.c205_part_number_id = v_partnum
					   AND t507.c507_control_number = v_control_num
					   AND t507.c507_missing_fl = 'Y'
					   AND t507.c507_status_fl = 'C';

					IF v_reason_exist IS NOT NULL
					THEN
						raise_application_error ('-20318', '');   --Missing tag reason already exist.
					END IF;

					UPDATE t507_returns_item t507
					   SET t507.c901_missing_tag_reason = v_reason
						   ,t507.c507_reconciled_by = p_userid
    					   ,t507.c507_reconciled_date = CURRENT_DATE
					 WHERE t507.c506_rma_id = v_raid
					   AND t507.c205_part_number_id = v_partnum
					   AND t507.c507_control_number = v_control_num
					   AND t507.c507_missing_fl = 'Y'
					   AND t507.c507_status_fl = 'C';
					   
				   --When ever the detail is updated, the return table has to be updated ,so ETL can Sync it to GOP.
				   UPDATE t506_returns
				      SET c506_last_updated_by   = p_userid
				        , c506_last_updated_date = CURRENT_DATE
				    WHERE c506_rma_id = v_raid;  					   

					IF v_reason = 54001 OR v_reason = 54002
					THEN
						IF v_reason = 54001
						THEN
							IF v_setid_passed IS NOT NULL
							THEN
								gm_sav_virtual_cons_set (v_raid, v_setid_passed, v_distid_passed, p_userid, p_out_msg);
							ELSE
								BEGIN
									SELECT t207.c207_set_id
									  INTO v_setid_ck
									  FROM t208_set_details t208, t207_set_master t207
									 WHERE t208.c205_part_number_id = v_partnum
									   AND t208.c208_set_qty <> 0
									   AND t208.c208_inset_fl = 'Y'
									   AND t207.c207_set_id = t208.c207_set_id
									   AND t207.c901_set_grp_type = '1601'
									   AND t207.c207_type = 4070;

									gm_sav_virtual_cons_set (v_raid, v_setid_ck, v_distid_passed, p_userid, p_out_msg);
								EXCEPTION
									WHEN TOO_MANY_ROWS
									THEN
										raise_application_error (-20325, '');
								END;
							/*gm_sav_virtual_cons_item (v_raid
													, v_partnum
													, v_distid_passed
													, v_qty
													, p_userid
													, p_out_msg
													 ); */
							END IF;
							gm_update_log (v_raid, 'The Following consignment was created for this return: '|| p_out_msg, 1270, p_userid, v_message);
						END IF;

						IF v_reason = 54002 AND v_excess IS NOT NULL
						THEN
							SELECT c504a_status_fl
							  INTO v_status_fl
							  FROM t504a_consignment_excess
							 WHERE c504_consignment_id = v_excess;

							IF v_status_fl = 1
							THEN
								raise_application_error (-20909, '');
							ELSE
								FOR rec IN excess_part_nums
								LOOP
									-- to get the costing id (4900 FG)
									v_costing_id := get_ac_costing_id (rec.part_num, 4900);
									-- to get the owner information.
									BEGIN
										SELECT t820.c1900_owner_company_id
										  , t820.c820_purchase_amt
										  , t820.c820_local_company_cost
										   INTO v_owner_company_id
										  , v_cost
										  , v_local_cost
										   FROM t820_costing t820
										  WHERE t820.c820_costing_id =v_costing_id;
								   EXCEPTION WHEN NO_DATA_FOUND
								   THEN
								   		v_owner_company_id := NULL;
								   		v_cost:= NULL;
								   		v_local_cost:= NULL;
								   END;
								   --
									gm_save_ledger_posting (4839
														  , CURRENT_DATE
														  , rec.part_num
														  , NULL
														  , v_excess
														  , TO_NUMBER (rec.qty)
														  , v_cost
														  , p_userid
														  , NULL
														  , v_owner_company_id
														  , v_local_cost
														   );
								END LOOP;

								UPDATE t504a_consignment_excess
								   SET c504a_status_fl = 1
									 , c504a_closed_dt = TO_DATE (CURRENT_DATE, NVL(get_compdtfmt_frm_cntx(),'MM/dd/YYYY'))
									 , c901_reason = 50531
									 , c504a_last_updated_by = p_userid
									 , c504a_last_updated_date = CURRENT_DATE 
								 WHERE c504_consignment_id = v_excess;
								 
								--When ever the detail is updated, the consignment table has to be updated ,so ETL can Sync it to GOP.
								   UPDATE T504_Consignment
								      SET c504_last_updated_by = p_userid
								        , c504_last_updated_date = CURRENT_DATE
								    WHERE C504_Consignment_Id = v_excess;
										    
								gm_update_log (v_excess, 'Excess has been reconcilied', 1221, p_userid, v_msg);
							END IF;
							
							gm_update_log (v_raid, 'The following excess have been posted for this return:'||v_excess , 1270, p_userid, v_message);
							
							IF v_msg_post_ex IS NULL
							THEN
								v_msg_post_ex := v_excess;	 --|| '->' || p_out_msg;
							ELSE
								v_msg_post_ex := v_msg_post_ex || ',' || v_excess;	 --|| '->' || p_out_msg;
							END IF;
						ELSE
							IF v_msg_consign IS NULL
							THEN
								v_msg_consign := '(' || v_raid || ')' || p_out_msg;
							ELSE
								v_msg_consign := v_msg_consign || ',' || '(' || v_raid || ')' || p_out_msg;
							END IF;
						END IF;
					ELSE
						IF v_msg_untag_set IS NULL
						THEN
							v_msg_untag_set := v_raid;
						ELSE
							v_msg_untag_set := v_msg_untag_set || ',' || v_raid;
						END IF;
						
						gm_update_log (v_raid, 'Untagged Sets has been reconcilied ', 1270, p_userid, v_message);
						
					END IF;
				ELSE
					SELECT COUNT (1)
					  INTO v_count
					  FROM t5010_tag
					 WHERE c5010_tag_id = v_tagid AND c5010_void_fl IS NULL;

					IF (v_count < 1)
					THEN
						raise_application_error ('-20193', '');
					END IF;

					SELECT c901_status
					  INTO v_tag_status
					  FROM t5010_tag
					 WHERE c5010_tag_id = v_tagid AND c5010_void_fl IS NULL;

					IF (v_tag_status = 51010 OR v_tag_status = 51013)
					THEN
						raise_application_error ('-20171', '');
					END IF;

					IF (v_type <> '3305')
					THEN
						IF (v_type = '3302')   ---Consignment - Item
						THEN
							UPDATE t507_returns_item t507
							   SET t507.c507_hold_fl = ''
							   	,t507.c507_reconciled_by = p_userid
    					   		,t507.c507_reconciled_date = CURRENT_DATE
							 WHERE t507.c506_rma_id = v_raid
							   AND t507.c205_part_number_id = v_partnum
							   AND t507.c507_control_number = v_control_num
							   AND t507.c507_hold_fl IS NOT NULL
							   AND ROWNUM < 2;
						ELSE
							UPDATE t507_returns_item t507
							   SET t507.c507_hold_fl = ''
							   	 ,t507.c507_reconciled_by = p_userid
    					   	  	 ,t507.c507_reconciled_date = CURRENT_DATE
							 WHERE t507.c506_rma_id = v_raid AND t507.c507_hold_fl IS NOT NULL;
						END IF;

						BEGIN
							SELECT t504.c701_distributor_id, t504.c504_ship_to, t504.c504_ship_to_id
								 , t5011.c205_part_number_id
							  INTO v_con_dist_id, v_ship_type, v_ship_id
								 , v_tagged_pnum
							  FROM t5011_tag_log t5011, t504_consignment t504
							 WHERE c5011_tag_txn_log_id IN (
												  SELECT MAX (t5011.c5011_tag_txn_log_id)
													FROM t5011_tag_log t5011
												   WHERE t5011.c5010_tag_id = v_tagid
														 AND t5011.c901_trans_type = '51000')
							   AND t504.c504_consignment_id = c5011_last_updated_trans_id
							   AND t504.c504_void_fl IS NULL
							   AND t5011.c5011_void_fl IS NULL;
						EXCEPTION
							WHEN NO_DATA_FOUND
							THEN
								BEGIN
									SELECT t5011.c5011_location_id, 4120, t5011.c205_part_number_id
									  INTO v_con_dist_id, v_ship_type, v_tagged_pnum
									  FROM t5011_tag_log t5011
									 WHERE c5011_tag_txn_log_id IN (
											   SELECT MAX (t5011.c5011_tag_txn_log_id)
												 FROM t5011_tag_log t5011
												WHERE t5011.c5010_tag_id = v_tagid
												  AND t5011.c901_trans_type = '51000'
												  AND t5011.c901_location_type = 4120
												  AND t5011.c5011_void_fl IS NULL);
								EXCEPTION
									WHEN NO_DATA_FOUND
									THEN
										raise_application_error ('-20215', '');
								END;
						END;

						UPDATE t5010_tag
						   SET c5010_void_fl = 'Y'
							 , c5010_last_updated_by = p_userid
							 , c5010_last_updated_date = CURRENT_DATE
							 , c1900_company_id  = v_company_id 
                             , c5040_plant_id = v_plant_id
						 WHERE c5010_tag_id = v_tagid AND c5010_void_fl IS NULL;

						IF (v_tagged_pnum != v_partnum)
						THEN
							raise_application_error ('-20219', '');
						END IF;

						SELECT t506.c701_distributor_id, t506.c207_set_id
						  INTO v_return_dist, v_setid
						  FROM t506_returns t506
						 WHERE t506.c506_rma_id = v_raid AND t506.c506_void_fl IS NULL;

						IF (v_type = '3302')   ---Consignment - Item
						THEN
							FOR val_cur IN v_item_rtn_cur
							LOOP
								v_inputstr	:= v_inputstr || val_cur.pnum || '^' || val_cur.qty || '|';
								v_part_inputstr := val_cur.pnum || ',';
							END LOOP;
						ELSE
							FOR val_cur IN v_rtn_cur
							LOOP
								v_inputstr	:= v_inputstr || val_cur.pnum || '^' || val_cur.qty || '|';
								v_part_inputstr := val_cur.pnum || ',';
							END LOOP;
						END IF;

						UPDATE t507_returns_item t507
						   SET t507.c507_missing_fl = 'N'
						   	,t507.c507_reconciled_by = p_userid
    					    ,t507.c507_reconciled_date = CURRENT_DATE
						 WHERE t507.c506_rma_id = v_raid
						   AND t507.c205_part_number_id = v_partnum
						   AND t507.c507_control_number = v_control_num
						   AND t507.c507_missing_fl = 'Y'
						   AND ROWNUM < 2;

						--When ever the child is updated, the parent table has to be updated ,so ETL can Sync it to GOP.
					 	UPDATE t506_returns
					  	   SET C506_LAST_UPDATED_BY = p_userid, 
							   C506_LAST_UPDATED_DATE = CURRENT_DATE
					     WHERE C506_rma_Id = v_raid;
					     
						IF (v_excess IS NOT NULL)
						THEN
							SELECT COUNT (1)
							  INTO v_excess_count	-- count of closed excess
							  FROM t504a_consignment_excess t504a
							 WHERE t504a.c504a_status_fl = 0 AND t504a.c504_consignment_id = v_excess;
						END IF;

						IF (v_con_dist_id != v_return_dist)
						THEN
							IF (v_excess IS NOT NULL AND v_excess_count > 0)
							THEN
								IF (v_type = '3302' OR (v_type = 3306 AND v_set_id IS NULL))
								THEN
									SELECT SUM (c505_item_qty)
									  INTO v_count
									  FROM t505_item_consignment t505
									 WHERE t505.c504_consignment_id = v_excess
									   AND t505.c205_part_number_id = v_partnum
									   --AND t505.c505_control_number = v_control_num
									   AND c505_void_fl IS NULL;

									IF (v_count > 1)
									THEN
										SELECT 'GM-EX-' || s504_consign.NEXTVAL
										  INTO v_newexcess
										  FROM DUAL;

										SELECT c701_distributor_id,c1900_company_id, c5040_plant_id
										  INTO v_distid, v_company_id, v_plant_id
										  FROM t504_consignment
										 WHERE c504_consignment_id = v_excess;

										INSERT INTO t504_consignment
													(c504_consignment_id, c701_distributor_id, c504_ship_date
												   , c504_created_date, c504_created_by, c504_status_fl, c504_ship_to
												   , c504_ship_to_id, c504_type, c504_ship_req_fl, c504_update_inv_fl
												   , c1900_company_id, c5040_plant_id												   
													)
											 VALUES (v_newexcess, v_distid, CURRENT_DATE
												   , CURRENT_DATE, p_userid, 4, 4120
												   , v_distid, 4110, 'N', 1
												   , v_company_id, v_plant_id												   
													);

										INSERT INTO t504a_consignment_excess
													(c506_rma_id, c504_consignment_id, c504a_status_fl
												   , c504a_created_by, c504a_created_date, c901_reason
													)
											 VALUES (v_raid, v_newexcess, 0
												   , p_userid, CURRENT_DATE, 50530
													);

										INSERT INTO t505_item_consignment
													(c505_item_consignment_id, c504_consignment_id, c205_part_number_id
												   , c505_control_number, c505_item_qty, c901_type, c505_ref_id
												   , c505_item_price)
											SELECT s504_consign_item.NEXTVAL, v_newexcess, c205_part_number_id, 'NOC##'
												 , 1, c901_type, c505_ref_id, c505_item_price
											  FROM t505_item_consignment
											 WHERE c504_consignment_id = v_excess
											   AND c205_part_number_id = v_partnum
											   --AND c505_control_number = v_control_num
											   AND c505_void_fl IS NULL;

										UPDATE t505_item_consignment
										   SET c505_item_qty = (c505_item_qty - 1)
										 WHERE c504_consignment_id = v_excess
										   AND c205_part_number_id = v_partnum
										   -- AND c505_control_number = v_control_num
										   AND c505_void_fl IS NULL;

									   --When ever the detail is updated, the consignment table has to be updated ,so ETL can Sync it to GOP.
									   UPDATE T504_Consignment
									      SET c504_last_updated_by = p_userid
									        , c504_last_updated_date = CURRENT_DATE
									    WHERE C504_Consignment_Id = v_excess;

										v_excess	:= v_newexcess;
									END IF;

									gm_pkg_op_excess_return.gm_op_sav_excess_return_detail (v_excess
																						  , v_raid
																						  , 50530
																						  , TO_CHAR(CURRENT_DATE,v_dateformat)
																						  , 90300
																						  , 90353
																						  , v_con_dist_id
																						  , v_return_dist
																						  , 90321
																						  , TO_CHAR(CURRENT_DATE,v_dateformat)
																						  , p_userid
																						  , v_setid
																						  , v_inputstr
																						  , v_part_inputstr
																						   );
								ELSE
									gm_pkg_op_excess_return.gm_op_sav_excess_return_detail (v_excess
																						  , v_raid
																						  , 50530
																						  , TO_CHAR(CURRENT_DATE,v_dateformat)
																						  , 90300
																						  , 90353
																						  , v_con_dist_id
																						  , v_return_dist
																						  , 90321
																						  , TO_CHAR(CURRENT_DATE,v_dateformat)
																						  , p_userid
																						  , v_setid
																						  , v_inputstr
																						  , v_part_inputstr
																						   );
								END IF;
							ELSE
								-- Initiate the Transfer
								IF (v_type = '3302' OR (v_type = 3306 AND v_set_id IS NULL))
								THEN
									gm_pkg_cs_transfer.gm_cs_sav_transfer (90300   -- Distributor to Distributor
																		 , 90352
																		 --  90351 Set Transfer  90352 part Transfer
									,									   v_con_dist_id
																		 , v_return_dist
																		 , 90321
																		 -- inventory transfer
									,									   TO_CHAR(CURRENT_DATE,v_dateformat)
																		 , p_userid
																		 , v_setid
																		 , v_inputstr
																		 , v_txn_id
																		  );
								ELSE
									gm_pkg_cs_transfer.gm_cs_sav_transfer
																	 (90300   -- Distributor to Distributor
																	, 90351   --  90351 Set Transfer  90352 part Transfer
																	, v_con_dist_id
																	, v_return_dist
																	, 90321
																	-- inventory transfer
									,								  TO_CHAR(CURRENT_DATE,v_dateformat)
																	, p_userid
																	, v_setid
																	, v_inputstr
																	, v_txn_id
																	 );
								END IF;

								gm_update_log (v_txn_id
											 , 'Transfer made as a part of missing tag reconciliation process'
											 , 1209
											 , p_userid
											 , v_message
											  );
								-- Accept the transfer
								gm_pkg_cs_transfer.gm_cs_sav_accept_transfer (v_txn_id
																			, p_userid
																			, 'Y'
																			, ''
																			, 0
																			, v_out_errmsg
																			 );
								gm_update_log (v_txn_id
											 , 'Transfer Verified as a part of missing tag reconciliation process'
											 , 1209
											 , p_userid
											 , v_message
											  );
							END IF;
						END IF;
					END IF;   -- end of type<>3305

					IF (v_type = '3305')
					THEN
						UPDATE t507_returns_item t507
						   SET t507.c507_missing_fl = ''
							 , t507.c5010_tag_id = v_tagid
							 , t507.c507_reconciled_by = p_userid
    					     , t507.c507_reconciled_date = CURRENT_DATE
						 WHERE t507.c506_rma_id = v_raid
						   AND t507.c205_part_number_id = v_partnum
						   AND t507.c507_control_number = v_control_num
						   AND t507.c507_missing_fl = 'Y'
						   AND ROWNUM < 2;
					END IF;
					
					gm_update_log (v_raid, 'This RA was returned with tag: '|| v_tagid, 1270, p_userid, v_message);
					
				END IF;   --end of IF v_tagid IS NULL
			END LOOP;

			IF v_msg_untag_set IS NOT NULL OR v_msg_consign IS NOT NULL OR v_msg_post_ex IS NOT NULL
			THEN
				p_out_msg	:= '';

				IF v_msg_untag_set IS NOT NULL
				THEN
					p_out_msg	:= 'The following transaction(s) have been untagged:' || '<BR>';
					p_out_msg	:= p_out_msg || v_msg_untag_set || '<BR>';
				END IF;

				IF v_msg_consign IS NOT NULL
				THEN
					p_out_msg	:=
							p_out_msg || '<BR>' || 'Following consignment have been created for the returns:' || '<BR>';
					p_out_msg	:= p_out_msg || v_msg_consign || '<BR>';
				END IF;

				IF v_msg_post_ex IS NOT NULL
				THEN
					p_out_msg	:= p_out_msg || '<BR>' || 'The following excess have been posted:' || '<BR>';
					p_out_msg	:= p_out_msg || v_msg_post_ex;
				END IF;
			END IF;
		END IF;
	END;

	/********************************************************************************************
	  * Description   : This Procedure used to fetch the associated return details
	  *********************************************************************************************/
	PROCEDURE gm_fch_associatedra (
		p_initra	   IN		t506_returns.c506_rma_id%TYPE
	  , p_initdist	   IN		t506_returns.c701_distributor_id%TYPE
	  , p_creditra	   IN		t506_returns.c506_rma_id%TYPE
	  , p_creditdist   IN		t506_returns.c701_distributor_id%TYPE
	  , p_outasstra    OUT		TYPES.cursor_type
	)
	AS
	v_company_id  t1900_company.c1900_company_id%TYPE;
	BEGIN
		SELECT NVL(GET_COMPID_FRM_CNTX(),GET_RULE_VALUE('DEFAULT_COMPANY','ONEPORTAL')) INTO v_company_id FROM DUAL;

		OPEN p_outasstra
		 FOR
			 SELECT NVL (t506b.c506_parent_rma_id, t506b.c506_rma_id) initra
				  , get_distributor_name (t506b.c701_distributor_id) initdist, t506a.c506_rma_id creditra
				  , get_distributor_name (t506a.c701_distributor_id) creditdist
				  , DECODE (t506b.c506_parent_rma_id, NULL, '', t506b.c506_rma_id) otherra
			   FROM t506_returns t506a, t506_returns t506b
			  WHERE t506a.c506_associated_rma_id = t506b.c506_rma_id
				AND NVL (t506b.c506_parent_rma_id, t506b.c506_rma_id) =
									  DECODE (p_initra
											, '', NVL (t506b.c506_parent_rma_id, t506b.c506_rma_id)
											, p_initra
											 )
				AND t506b.c701_distributor_id = DECODE (p_initdist, 0, t506b.c701_distributor_id, p_initdist)
				AND t506a.c506_rma_id = DECODE (p_creditra, '', t506a.c506_rma_id, p_creditra)
				AND t506a.c701_distributor_id = DECODE (p_creditdist, 0, t506a.c701_distributor_id, p_creditdist)
				AND t506a.c1900_company_id = v_company_id;
	END gm_fch_associatedra;

	/********************************************************************************************
	  * Description   : This Procedure used for offsetting virtual consignment for set
	  *********************************************************************************************/
	PROCEDURE gm_sav_virtual_cons_set (
		p_raid		IN		 t506_returns.c506_rma_id%TYPE
	  , p_setid 	IN		 t207_set_master.c207_set_id%TYPE
	  , p_distid	IN		 t701_distributor.c701_distributor_id%TYPE
	  , p_userid	IN		 t301_vendor.c301_last_updated_by%TYPE
	  , p_out_msg	OUT 	 t504_consignment.c504_consignment_id%TYPE
	)
	AS
		v_request_id   t520_request.c520_request_id%TYPE;
		v_consignment_id t504_consignment.c504_consignment_id%TYPE;
		v_item_consigment_id t505_item_consignment.c505_item_consignment_id%TYPE;
		v_shipid	   t907_shipping_info.c907_shipping_id%TYPE;
		v_comments	   VARCHAR2 (100);
		-- International posting changes.
	  	v_costing_id t820_costing.c820_costing_id%TYPE;
	  	v_owner_company_id t1900_company.c1900_company_id%TYPE;
	  	v_local_cost t820_costing.c820_purchase_amt%TYPE;
	  	v_cost t820_costing.c820_purchase_amt%TYPE;
	  	
		CURSOR c_get_setdtl
		IS
			SELECT	 t208.c207_set_id ID, t208.c205_part_number_id pnum, NVL (t208.c208_set_qty, 0) qty
				   , t208.c208_set_details_id mid, get_partnum_desc (t208.c205_part_number_id) pdesc
				   , t208.c208_inset_fl insetfl, NVL (get_part_price (t208.c205_part_number_id, 'L'), 0) lprice
				FROM t208_set_details t208
			   WHERE t208.c208_void_fl IS NULL
				 AND t208.c207_set_id = p_setid
				 AND t208.c208_set_qty <> 0
				 AND t208.c208_inset_fl = 'Y'
			ORDER BY t208.c205_part_number_id, t208.c208_critical_fl DESC, t208.c208_inset_fl DESC;
	v_company_id  t1900_company.c1900_company_id%TYPE;
	v_plant_id 	  t5040_plant_master.c5040_plant_id%TYPE;
	v_acc_cmp_id  t1900_company.c1900_company_id%TYPE;
	v_user_plant_id   t5040_plant_master.c5040_plant_id%TYPE;
   BEGIN
	   
	    SELECT GET_COMPID_FRM_CNTX(), GET_PLANTID_FRM_CNTX()
	      INTO v_company_id, v_plant_id
	      FROM DUAL;	 
	      
	    BEGIN
          --getting company id and plant id mapped from account
        SELECT
           get_distributor_company_id(p_distid),
           gm_pkg_op_plant_rpt.get_default_plant_for_comp(get_distributor_company_id(p_distid))
        INTO v_acc_cmp_id, v_user_plant_id  
		from dual;
        EXCEPTION
           WHEN NO_DATA_FOUND
           THEN
           v_acc_cmp_id := NULL;
           v_user_plant_id := NULL;
        END;
	      
		SELECT 'GM-RQ-' || s520_request.NEXTVAL
		  INTO v_request_id
		  FROM DUAL;

		INSERT INTO t520_request
					(c520_created_date, c901_request_source, c520_required_date, c520_request_to, c520_status_fl
				   , c901_ship_to, c520_request_id, c520_created_by, c520_request_for, c207_set_id, c520_ship_to_id
				   , c520_request_date, c901_request_by_type, c1900_company_id, c5040_plant_id
					)
			 VALUES (CURRENT_DATE, 50618, TRUNC (CURRENT_DATE), p_distid, 40
				   , 4120, v_request_id, p_userid, 40021, p_setid, p_distid
				   , TRUNC (CURRENT_DATE), 50626, nvl(v_company_id,v_acc_cmp_id), nvl(v_plant_id,v_user_plant_id)

					);

		SELECT get_next_consign_id ('consignment')
		  INTO v_consignment_id
		  FROM DUAL;

		p_out_msg	:= v_consignment_id;
		v_comments	:= 'Consignment Write-up(set) for Missing Reconciliation ' || p_raid;

		INSERT INTO t504_consignment
					(c504_verified_date, c504_delivery_carrier, c520_request_id, c504_consignment_id
				   , c504_delivery_mode, c504_status_fl, c504_type, c504_ship_to, c504_created_by, c701_distributor_id
				   , c504_ship_to_id, c504_update_inv_fl, c504_ship_req_fl, c207_set_id, c504_ship_date
				   , c504_verify_fl, c504_created_date, c504_comments, c1900_company_id, c5040_plant_id
					)
			 VALUES (TRUNC (CURRENT_DATE), 5040, v_request_id, v_consignment_id
				   , 5031, 4, 4110, 4120, p_userid, p_distid
				   , p_distid, '1', '1', p_setid, TRUNC (CURRENT_DATE)
				   , '1', CURRENT_DATE, v_comments, v_company_id, v_plant_id
					);

		SELECT s907_ship_id.NEXTVAL
		  INTO v_shipid
		  FROM DUAL;

		INSERT INTO t907_shipping_info
					(c907_shipping_id, c907_ref_id, c901_ship_to, c907_ship_to_id, c907_ship_to_dt, c901_source
				   , c901_delivery_mode, c901_delivery_carrier, c907_status_fl, c907_created_by, c907_created_date
				   , c907_active_fl, c907_release_dt, c907_shipped_dt, c1900_company_id
				   , c5040_plant_id
					)
			 VALUES (v_shipid, v_consignment_id, 4120, p_distid, TRUNC (CURRENT_DATE), 50181
				   , 5031, 5040, 40, p_userid, CURRENT_DATE
				   , 'N', TRUNC (CURRENT_DATE), TRUNC (CURRENT_DATE), v_company_id
				   , v_plant_id
					);

		FOR rec IN c_get_setdtl
		LOOP
			SELECT s504_consign_item.NEXTVAL
			  INTO v_item_consigment_id
			  FROM DUAL;

			INSERT INTO t505_item_consignment
						(c505_item_price, c504_consignment_id, c505_item_consignment_id, c505_item_qty
					   , c205_part_number_id, c505_control_number
						)
				 VALUES (rec.lprice, v_consignment_id, v_item_consigment_id, rec.qty
					   , rec.pnum, 'NOC#'
						);

				-- to get the costing id (4900 FG)
				v_costing_id := NVL (get_ac_costing_id (rec.pnum, 4904),  get_ac_costing_id (rec.pnum, 4900));
				-- to get the owner information.
				BEGIN
					SELECT t820.c1900_owner_company_id
					  , t820.c820_purchase_amt
					  , t820.c820_local_company_cost
					   INTO v_owner_company_id
					  , v_cost
					  , v_local_cost
					   FROM t820_costing t820
					  WHERE t820.c820_costing_id =v_costing_id;
			   EXCEPTION WHEN NO_DATA_FOUND
			   THEN
			   		v_owner_company_id := NULL;
			   		v_cost:= NULL;
			   		v_local_cost:= NULL;
			   END;
			--Posting
			gm_save_ledger_posting (4839
								  , CURRENT_DATE
								  , rec.pnum
								  , p_distid
								  , v_consignment_id
								  , rec.qty
								  , v_cost
								  , p_userid
								  , NULL
								  , v_owner_company_id
								  , v_local_cost
								   );
		END LOOP;
	END gm_sav_virtual_cons_set;

	/******************************************************************************
	* Description : This procedure is written as the solution to create consignment
	*				if parts are not shipped and the same in identified in field audit
	*******************************************************************************/
	PROCEDURE gm_sav_virtual_cons_item (
		p_raid		IN		 t506_returns.c506_rma_id%TYPE
	  , p_pnum		IN		 t208_set_details.c205_part_number_id%TYPE
	  , p_distid	IN		 t701_distributor.c701_distributor_id%TYPE
	  , p_qty		IN		 NUMBER
	  , p_userid	IN		 t301_vendor.c301_last_updated_by%TYPE
	  , p_out_msg	OUT 	 t504_consignment.c504_consignment_id%TYPE
	)
	AS
		v_request_id   t520_request.c520_request_id%TYPE;
		v_consignment_id t504_consignment.c504_consignment_id%TYPE;
		v_item_consigment_id t505_item_consignment.c505_item_consignment_id%TYPE;
		v_shipid	   t907_shipping_info.c907_shipping_id%TYPE;
		v_price 	   t505_item_consignment.c505_item_price%TYPE;
		v_comments	   VARCHAR2 (100);
		v_company_id  t1900_company.c1900_company_id%TYPE;
    	v_plant_id 	  t5040_plant_master.c5040_plant_id%TYPE;
    	-- International posting changes.
		v_costing_id t820_costing.c820_costing_id%TYPE;
		v_owner_company_id t1900_company.c1900_company_id%TYPE;
		v_local_cost t820_costing.c820_purchase_amt%TYPE;
		v_cost t820_costing.c820_purchase_amt%TYPE;
		v_acc_cmp_id  t1900_company.c1900_company_id%TYPE;
    	v_user_plant_id 	  t5040_plant_master.c5040_plant_id%TYPE;
   BEGIN
	   
      	SELECT get_compid_frm_cntx(), get_plantid_frm_cntx()
          INTO v_company_id, v_plant_id
          FROM DUAL;        
          
        BEGIN
           --getting company id and plant id mapped from account
        SELECT
           get_distributor_company_id(p_distid),
           gm_pkg_op_plant_rpt.get_default_plant_for_comp(get_distributor_company_id(p_distid))
        INTO v_acc_cmp_id, v_user_plant_id  
		from dual;
        EXCEPTION
           WHEN NO_DATA_FOUND
           THEN
           v_acc_cmp_id := NULL;
           v_user_plant_id := NULL;
        END;
       
		SELECT 'GM-RQ-' || s520_request.NEXTVAL
		  INTO v_request_id
		  FROM DUAL;

		INSERT INTO t520_request
					(c520_created_date, c901_request_source, c520_required_date, c520_request_to, c520_status_fl
				   , c901_ship_to, c520_request_id, c520_created_by, c520_request_for, c207_set_id, c520_ship_to_id
				   , c520_request_date, c901_request_by_type, c1900_company_id, c5040_plant_id
					)
			 VALUES (CURRENT_DATE, 50618, TRUNC (CURRENT_DATE), p_distid, 40
				   , 4120, v_request_id, p_userid, 40021, '', p_distid
				   , TRUNC (CURRENT_DATE), 50626, nvl(v_company_id,v_acc_cmp_id), nvl(v_plant_id,v_user_plant_id)
					);

		SELECT get_next_consign_id ('consignment')
		  INTO v_consignment_id
		  FROM DUAL;

		p_out_msg	:= v_consignment_id;
		v_comments	:= 'Consignment Write-up(item) for Missing Reconciliation ' || p_raid;

		INSERT INTO t504_consignment
					(c504_verified_date, c504_delivery_carrier, c520_request_id, c504_consignment_id
				   , c504_delivery_mode, c504_status_fl, c504_type, c504_ship_to, c504_created_by, c701_distributor_id
				   , c504_ship_to_id, c504_update_inv_fl, c504_ship_req_fl, c207_set_id, c504_ship_date
				   , c504_verify_fl, c504_created_date, c504_comments, c1900_company_id, c5040_plant_id
					)
			 VALUES (TRUNC (CURRENT_DATE), 5040, v_request_id, v_consignment_id
				   , 5031, 4, 4110, 4120, p_userid, p_distid
				   , p_distid, '1', '1', '', TRUNC (CURRENT_DATE)
				   , '1', CURRENT_DATE, 'Consignment Write-up for part', v_company_id, v_plant_id
					);

		SELECT s907_ship_id.NEXTVAL
		  INTO v_shipid
		  FROM DUAL;

		INSERT INTO t907_shipping_info
					(c907_shipping_id, c907_ref_id, c901_ship_to, c907_ship_to_id, c907_ship_to_dt, c901_source
				   , c901_delivery_mode, c901_delivery_carrier, c907_status_fl, c907_created_by, c907_created_date
				   , c907_active_fl, c907_release_dt, c907_shipped_dt, c1900_company_id
				   , c5040_plant_id
					)
			 VALUES (v_shipid, v_consignment_id, 4120, p_distid, TRUNC (CURRENT_DATE), 50181
				   , 5031, 5040, 40, p_userid, CURRENT_DATE
				   , 'N', TRUNC (CURRENT_DATE), TRUNC (CURRENT_DATE), v_company_id
				   , v_plant_id
					);

		SELECT s504_consign_item.NEXTVAL
		  INTO v_item_consigment_id
		  FROM DUAL;

		SELECT get_part_price (p_pnum, 'C')
		  INTO v_price
		  FROM DUAL;

		INSERT INTO t505_item_consignment
					(c505_item_price, c504_consignment_id, c505_item_consignment_id, c505_item_qty
				   , c205_part_number_id, c505_control_number
					)
			 VALUES (v_price, v_consignment_id, v_item_consigment_id, p_qty
				   , p_pnum, 'NOC#'
					);

		--Posting
		-- to get the costing id (4900 FG)
			v_costing_id := NVL(get_ac_costing_id (p_pnum, 4904), get_ac_costing_id (p_pnum, 4900));
			-- to get the owner information.
			BEGIN
				SELECT t820.c1900_owner_company_id
					 , t820.c820_purchase_amt
					 , t820.c820_local_company_cost
					  INTO v_owner_company_id
					 , v_cost
					 , v_local_cost
					  FROM t820_costing t820
					 WHERE t820.c820_costing_id =v_costing_id;
			  EXCEPTION WHEN NO_DATA_FOUND
			  THEN
			   		v_owner_company_id := NULL;
			   		v_cost:= NULL;
			   		v_local_cost:= NULL;
			   END;
			   
		gm_save_ledger_posting (4839
							  , CURRENT_DATE
							  , p_pnum
							  , p_distid
							  , v_consignment_id
							  , p_qty
							  , v_cost
							  , p_userid
							  , NULL
							  , v_owner_company_id
							  , v_local_cost
							   );
	END gm_sav_virtual_cons_item;
/*
  * Description: This function will return the repid that fetch from the T501_ORDER for the RAID, if its a Sales Item Returns
  * Author:		Rajkumar 
  */
 FUNCTION get_ra_rep_id(
 	p_raid		IN   t506_returns.c506_rma_id%TYPE
 ) RETURN NUMBER
 IS
 	v_type		NUMBER;
	v_ordid		t501_order.c501_order_id%TYPE;
	v_retval	NUMBER := 0;
BEGIN
	     SELECT C506_TYPE, C501_ORDER_ID
		     INTO v_type,v_ordid
		   FROM T506_RETURNS
		  WHERE C506_RMA_ID = p_raid
		  AND C506_VOID_FL IS NULL; 

	  IF v_type = 3300
	  THEN
	     SELECT NVL(C703_SALES_REP_ID,0) INTO v_retval
		   FROM T501_ORDER
		  WHERE C501_ORDER_ID = v_ordid
	    AND C501_VOID_FL IS NULL;
	  END IF;
	  RETURN v_retval;
 END get_ra_rep_id;
 
	 PROCEDURE gm_cs_sav_void_return (
	        p_txn_id IN t907_cancel_log.c907_ref_id%TYPE,
	        p_userid IN t102_user_login.c102_user_login_id%TYPE)
	AS
	    /*******************************************************
	    * Description : Procedure to void a return
	    * Parameters   : 1. Transaction Id - Return RMA ID
	    *    2. User Id
	    * Validation --
	    *  1. For Consignment return (Both Item and Set), If the returns status is 0 then user can void the record
	    *  2. For Sales return is the return status is 0 and if C506_AC_CREDIT_DT is null then user should be able to void
	    the record
	    *  3. Check if Void flag is already set.
	    *
	    *******************************************************/
	    v_return_status t506_returns.c506_status_fl%TYPE;
	    v_ac_credit_dt t506_returns.c506_ac_credit_dt%TYPE;
	    v_void_fl t506_returns.c506_void_fl%TYPE;
	    v_ret_type t506_returns.c506_type%TYPE;
	    v_order_id t506_returns.c501_order_id%TYPE;
	    v_order_type t501_order.c901_order_type%TYPE;
	    v_orig_order_type t501_order.c901_order_type%TYPE;
	    v_message VARCHAR2 (1000) ;
	    v_txnid			T412_INHOUSE_TRANSACTIONS.C412_INHOUSE_TRANS_ID%TYPE;
		v_status		T412_INHOUSE_TRANSACTIONS.C412_STATUS_FL%TYPE;
	BEGIN
	    --
	     SELECT c506_status_fl, c506_ac_credit_dt, c506_void_fl
	      , c506_type, c501_order_id
	       INTO v_return_status
	        -- To validate if the Return status is 0 meaning that the Return is only in initated status
	      , v_ac_credit_dt
	        -- To validate if credit has been issued by Accounting team for this return
	      , v_void_fl
	        -- To validate if someone is trying to click back and repost a void txn
	      , v_ret_type, v_order_id
	        -- To fetch the return type so we know if the return type is Consignment or Sales Return
	        -- 3300 -> Sales Item Return
	        -- 3301 -> Consignment - Sets
	        -- 3302 -> Consignment - Items
	       FROM t506_returns
	      WHERE c506_rma_id = p_txn_id FOR UPDATE;
	    -- If the Return type is Sales item / Consignment Sets / Items, then validate if the return status is 0. If not 0,
	    -- then throw Error
	    IF (v_return_status <> 0) THEN
	        -- Error Messaage. This return transaction has moved out of Initate state and so cannot be made void
	        raise_application_error ( - 20019, '') ;
	        -- If the Return type is Sales Items, then validate if the return has already been credited. If it has already
	        -- been credited then the C506_AC_CREDIT_DT will have a value
	    ELSIF (v_ret_type = 3300) AND (v_ac_credit_dt IS NOT NULL) THEN
	        -- Error Messaage. This return transaction has already been credited and so cannot be made void
	        raise_application_error ( - 20020, '') ;
	        -- If the void flag is already set, then throw an Error
	    ELSIF (v_void_fl IS NOT NULL) THEN
	        -- Error Messaage. This return transaction has already been made void.
	        raise_application_error ( - 20021, '') ;
	    ELSE
	    
	    	IF v_ret_type IN ('3308','3309')
	    	THEN
	    		BEGIN
						
						SELECT C412_INHOUSE_TRANS_ID, C412_STATUS_FL INTO v_txnid, v_status 
							FROM 
							 T412_INHOUSE_TRANSACTIONS 
							 WHERE 
							 C412_REF_ID = p_txn_id AND C412_VOID_FL IS NULL;

					EXCEPTION WHEN NO_DATA_FOUND
					THEN
						v_txnid := NULL;
					END;

			 		IF v_txnid IS NOT NULL
			 		THEN
			 			IF v_status = '4'
			 			THEN
			 								GM_RAISE_APPLICATION_ERROR('-20999','338',p_txn_id||'#$#'||v_txnid); 
			 			END IF;
			 			
			 			UPDATE T412_INHOUSE_TRANSACTIONS 
			 					SET C412_VOID_FL = 'Y'
			 					, c412_last_updated_by = p_userid
								, c412_last_updated_date = CURRENT_DATE
			 				WHERE C412_INHOUSE_TRANS_ID = v_txnid;
			 		END IF;
	    	END IF;
	        -- Perform the void operation by setting the C506_VOID_FL = 'Y'
	         UPDATE t506_returns
	        SET c506_void_fl    = 'Y', c506_last_updated_by = p_userid, c506_last_updated_date = CURRENT_DATE
	          WHERE c506_rma_id = p_txn_id;
	        IF v_order_id      IS NOT NULL THEN
	             SELECT NVL (c901_order_type, 9999)
	               INTO v_order_type
	               FROM t501_order
	              WHERE c501_order_id = v_order_id;
	            -- if order type is duplicate order 2522, then update order type to original order type
	            IF (v_order_type = 2522) THEN
			BEGIN
	                 SELECT to_number (c501a_attribute_value)
	                   INTO v_orig_order_type
	                   FROM t501a_order_attribute
	                  WHERE c501_order_id       = v_order_id
	                    AND c901_attribute_type = 101720;
			EXCEPTION
			WHEN NO_DATA_FOUND THEN
				v_orig_order_type := NULL;
			END;
	                --
	                 UPDATE t501_order
	                SET c901_order_type = v_orig_order_type, C501_LAST_UPDATED_BY = p_userid, C501_LAST_UPDATED_DATE =
	                    CURRENT_DATE
	                  WHERE c501_order_id = v_order_id ;
	                GM_UPDATE_LOG (v_order_id, 'Associated return ' ||p_txn_id ||
	                ' is voided,  change order type from Duplicate order to Original order type '|| get_code_name (
	                v_orig_order_type), 1200, p_userid, v_message) ;
	            END IF;
	        END IF;
	    END IF;
	    -- To void the usage table data 
	  UPDATE t502b_item_order_usage
		SET c502b_void_fl      = 'Y', c502b_last_updated_by = p_userid, c502b_last_updated_date = CURRENT_DATE
		  WHERE c502b_trans_id = p_txn_id
		    AND c502b_void_fl IS NULL;
	END gm_cs_sav_void_return;
	--
/***************************************************************************************************
 * Description	:This procedure is used to save order loaner return - to process the RA automatically
 ***************************************************************************************************/
--
	PROCEDURE gm_sav_order_loaner_return (
		p_raid		IN		 t506_returns.c506_rma_id%TYPE
	  , p_str		IN		 VARCHAR2
	  , p_retdate	IN		 VARCHAR2
	  , p_flag		IN		 VARCHAR2
	  , p_comments  IN       t506_returns.c506_comments%TYPE
	  , p_order_id  IN       t506_returns.c501_order_id%TYPE
	  , p_userid	IN		 t506_returns.c506_last_updated_by%TYPE
	)
	AS
	v_errmsg VARCHAR2(2000);
	v_returnids VARCHAR2(2000);
	BEGIN
	--to load the loaner child RA details in Delivery Order ? Return Parts screen.
	--updating comments and order id for loaner RA
	--BUG FIX 11704 UI Issue
		UPDATE t506_returns 
			 SET c506_comments=p_comments||' - <b>Note:</b> This RA is processed automatically'
			 ,c501_order_id=p_order_id 
			 ,c506_last_updated_by=p_userid
			 , c506_last_updated_date=CURRENT_DATE
		  WHERE c506_rma_id=p_raid 
		     AND c506_void_fl IS NULL;
	--move the loaner RA to Pending Credit Status	   
	gm_pkg_op_return.gm_op_sav_return_accp (p_raid,p_str,p_retdate,p_flag,p_userid,v_errmsg);
	--credit the loaner RA and Its moved to pending reprocess
	gm_pkg_op_return.gm_op_sav_return_forcredit(p_raid,TO_DATE(p_retdate,get_compdtfmt_frm_cntx()),p_userid) ;	
	--to complete the RA from pending re process
	gm_pkg_op_reprocess.gm_save_reprocess_returns(p_raid,p_raid,NULL,NULL,NULL,'0',p_userid,NULL,NULL,NULL,NULL,v_returnids,v_errmsg);
	END gm_sav_order_loaner_return;
	
  /******************************************************************************
 * Description : This Procedure used to fetch RA paperwork item detail.
 *               If RA type is sales item and RA has issue credited then it will fetch
 *               Item detail from item order table otherwise return item table.
 *******************************************************************************/		
	PROCEDURE gm_fch_credit_report (
	        p_raid IN t506_returns.c506_rma_id%TYPE,
	        p_raitemdtl OUT TYPES.cursor_type
	)
	AS
	    v_type t506_returns.C506_TYPE%type;
	    v_credit_dt t506_returns.c506_ac_credit_dt%type;
	BEGIN
	    
	    SELECT c506_type, c506_ac_credit_dt 
	      INTO v_type,v_credit_dt 
	      FROM t506_returns 
	     WHERE c506_rma_id = p_raid
	       AND c506_void_fl IS NULL;
	
	       
	    --3300 Sales Item
	    --If RA type is sales item and RA has issue credited then it will fetch
        --Item detail from item order table otherwise return item table.   
	    IF v_type = '3300' AND v_credit_dt IS NOT NULL THEN
	        OPEN p_raitemdtl FOR 
	         SELECT SUM (T502.C502_ITEM_QTY) QTY, SUM (T502.C502_ITEM_QTY) IQTY, T502.C502_ITEM_PRICE PRICE
	          , T502.C502_CONTROL_NUMBER CNUM, ' ' SFL, T502.C205_PART_NUMBER_ID PNUM, get_partdesc_by_company(t502.C205_PART_NUMBER_ID) PDESC
	           FROM T501_ORDER T501, T502_ITEM_ORDER T502, T205_PART_NUMBER T205
	          WHERE T501.C506_RMA_ID   = p_raid
	            AND T501.C501_ORDER_ID = T502.C501_ORDER_ID
	            AND t502.c205_part_number_id  = t205.c205_part_number_id
	            AND T501.C501_VOID_FL IS NULL
	            AND T502.C502_VOID_FL IS NULL 
	       GROUP BY T502.C205_PART_NUMBER_ID, T502.C502_CONTROL_NUMBER, T502.C502_ITEM_PRICE, T205.C205_PART_NUM_DESC
	       ORDER BY t502.C205_PART_NUMBER_ID;
	    ELSE
	        OPEN p_raitemdtl FOR 
	         SELECT - 1 * SUM (C507_ITEM_QTY) QTY, - 1 * SUM (C507_ITEM_QTY) IQTY, NVL (C507_ITEM_PRICE, 0) PRICE
	          , '-' CNUM, DECODE (C507_STATUS_FL, 'R', 'Pending Return', 'C', 'Return', 'W', 'Write-off', 'M', 'Missing')
	            SFL, T507.C205_PART_NUMBER_ID PNUM, get_partdesc_by_company(T507.C205_PART_NUMBER_ID) PDESC
	           FROM T506_RETURNS T506, T507_RETURNS_ITEM T507, T205_PART_NUMBER T205
	          WHERE T506.C506_RMA_ID = p_raid
	          	AND T506.C506_RMA_ID = T507.C506_RMA_ID
                AND T507.c205_part_number_id  = t205.c205_part_number_id
                AND T506.C506_VOID_FL IS NULL
	            AND C507_STATUS_FL IN ('C', 'R', 'W', 'M')
	       GROUP BY C507_ITEM_PRICE, T507.C205_PART_NUMBER_ID, C507_STATUS_FL
	          , C507_ITEM_PRICE, T205.C205_PART_NUM_DESC
	          ORDER BY T507.C205_PART_NUMBER_ID;
	    END IF;
	
	END gm_fch_credit_report; 
	
  /******************************************************************************
 * Description : This Procedure used to fetch RA paperwork item detail.
 *               If RA type is sales item and RA has issue credited then it will fetch
 *               Item detail from item order table otherwise return item table.
 *******************************************************************************/	
	PROCEDURE gm_fch_bba_credit_report (
	        p_raid IN t506_returns.c506_rma_id%TYPE,
	        p_raitemdtl OUT TYPES.cursor_type
	)
	AS
		v_datafmt VARCHAR2(50);
	    v_type t506_returns.C506_TYPE%type;
	    v_credit_dt t506_returns.c506_ac_credit_dt%type;
	BEGIN
    
    SELECT c506_type, c506_ac_credit_dt,  GET_RULE_VALUE ('DATEFMT', 'DATEFORMAT') 
      INTO v_type,v_credit_dt,v_datafmt  
      FROM t506_returns 
     WHERE c506_rma_id = p_raid
       AND c506_void_fl IS NULL;
       
	    --3300 Sales Item
	    --If RA type is sales item and RA has issue credited then it will fetch
        --Item detail from item order table otherwise return item table. 
    IF v_type = '3300' AND v_credit_dt IS NOT NULL THEN
        OPEN p_raitemdtl FOR 
			SELECT t502.c205_part_number_id PNUM, get_partdesc_by_company(T502.C205_PART_NUMBER_ID) PDESC, TO_CHAR (t2550.edate,
					    v_datafmt) PRODEXPIRY, T502.C502_CONTROL_NUMBER
					    ALLOGRAFTNO, GET_PART_SIZE (T502.C205_PART_NUMBER_ID, T502.C502_CONTROL_NUMBER)
					    prodsize, t2550.c2540_donor_number proddonor, T502.C502_ITEM_QTY QTY, T502.C502_ITEM_QTY IQTY, T502.C502_ITEM_PRICE PRICE
					   FROM
					    (
					        SELECT DISTINCT t2550.c205_part_number_id, t2550.c2550_control_number, t2550.c2550_expiry_date edate
					          , t2550.c2550_custom_size, t2540.c2540_donor_number, t2550.c2540_donor_id
					           FROM t2550_part_control_number t2550, t2540_donor_master t2540
					          WHERE NVL (t2550.c2540_donor_id, - 999)       = t2540.c2540_donor_id
					            AND t2540.c2540_void_fl                    IS NULL
					    )
					    t2550, T501_ORDER T501, T502_ITEM_ORDER T502, T205_PART_NUMBER T205
					  WHERE  T501.C506_RMA_ID   = p_raid
			            AND T501.C501_ORDER_ID = T502.C501_ORDER_ID
			            AND T501.C501_VOID_FL IS NULL
			             AND t502.c205_part_number_id  = t205.c205_part_number_id
			             AND t502.C502_VOID_FL IS NULL 
					    AND t502.c205_part_number_id  = t2550.c205_part_number_id (+)
					    AND T502.C502_CONTROL_NUMBER = t2550.c2550_control_number (+)
					ORDER BY PNUM, allograftno;         
    ELSE
        OPEN p_raitemdtl FOR 
		 SELECT t507.c205_part_number_id PNUM, get_partdesc_by_company(T507.C205_PART_NUMBER_ID) PDESC, TO_CHAR (t2550.edate,
		    v_datafmt) prodexpiry, NVL (t507.c507_org_control_number, t507.c507_control_number)
		    allograftno, get_part_size (t507.c205_part_number_id, NVL (t507.c507_org_control_number, t507.c507_control_number))
		    prodsize, t2550.c2540_donor_number proddonor, DECODE (t507.C507_STATUS_FL, 'R', 'Pending Return', 'C', 'Return',
		    'W', 'Write-off', 'M', 'Missing') SFL, - 1 * t507.C507_ITEM_QTY QTY, - 1 * t507.C507_ITEM_QTY IQTY
		  , NVL (t507.c507_item_price, 0) PRICE
		   FROM
		    (
		        SELECT DISTINCT t2550.c205_part_number_id, t2550.c2550_control_number, t2550.c2550_expiry_date edate
		          , t2550.c2550_custom_size, t2540.c2540_donor_number, t2550.c2540_donor_id
		           FROM t2550_part_control_number t2550, t2540_donor_master t2540, t507_returns_item t507
		          WHERE NVL (t2550.c2540_donor_id, - 999)       = t2540.c2540_donor_id
		            AND t2550.c205_part_number_id               = t507.c205_part_number_id
		            AND NVL (t2550.c2550_control_number, - 999) = NVL (t507.c507_org_control_number, t507.c507_control_number)
		            AND t507.c506_rma_id                        = p_raid
		            AND t507.C507_STATUS_FL                    IN ('C', 'W', 'R', 'M')
		            AND t2540.c2540_void_fl                    IS NULL
		    )
		    t2550, T506_RETURNS T506, T507_RETURNS_ITEM t507
		  WHERE t506.c506_rma_id                                             = p_raid
		    AND T506.C506_RMA_ID = T507.C506_RMA_ID
            AND T506.C506_VOID_FL IS NULL
		    AND t507.c205_part_number_id                                     = t2550.c205_part_number_id (+)
		    AND NVL (t507.c507_org_control_number, t507.c507_control_number) = t2550.c2550_control_number (+)
		    AND T507.C507_STATUS_FL                                         IN ('C', 'W', 'R', 'M')
		ORDER BY PNUM, allograftno;
    END IF;
	    
END gm_fch_bba_credit_report; 

/******************************************************************************
 * Purpose: function is used to fetch the part numbers consigned to an account
 *******************************************************************************/
--
	FUNCTION get_returned_parts (
		p_rma_id	t507_returns_item.c506_rma_id%TYPE
	)
		RETURN CLOB
	IS
--
		v_id				t506_returns.c701_distributor_id%TYPE;
		v_part_num	   		CLOB;
		v_ware_house		t906_rules.c906_rule_value%TYPE;
    	v_ware_house_type  	t906_rules.c906_rule_value%TYPE;
    	v_ret_type			t506_returns.c506_type%TYPE;
	BEGIN
		
		BEGIN
			SELECT NVL (c701_distributor_id, c704_account_id), c506_type
			  	INTO v_id, v_ret_type
			  	FROM t506_returns
			 	WHERE c506_rma_id = p_rma_id;
			EXCEPTION WHEN NO_DATA_FOUND
			THEN
				v_id := NULL; 
				v_ret_type := NULL;
		END;
		
		-- 3304: Account consignment item; 4121: Sales Rep
		SELECT DECODE(v_ret_type, '3304', v_ret_type, NULL) INTO v_ret_type FROM dual;
		
		v_ware_house      := get_rule_value(NVL(v_ret_type,'4121'),'RULEWH'); -- to get the warehouse based on the item return type
    	v_ware_house_type := get_rule_value(NVL(v_ret_type,'4121'),'RULEWHTYPE'); -- to get the warehouse type based on the item return type
    	
    	
		SELECT RTRIM (XMLAGG (XMLELEMENT (E, c205_part_number_id || ',')) .EXTRACT ('//text()').getclobval(), ',') INTO v_part_num
               FROM t5051_inv_warehouse t5051, t5052_location_master t5052, t5053_location_part_mapping t5053
              WHERE t5051.c5051_inv_warehouse_id = t5052.c5051_inv_warehouse_id
                AND t5052.c5052_location_id      = t5053.c5052_location_id
                AND T5051.C5051_INV_WAREHOUSE_ID = v_ware_house
                AND t5052.c5052_ref_id           = v_id
                AND t5052.c901_location_type     = v_ware_house_type
                AND t5052.c5052_void_fl         IS NULL
				AND 1 = 2;

		RETURN v_part_num;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN '';
	END get_returned_parts;
	
	/***************************************************************************************
	 * Description : Procedure to Save the lot number in t507 table for validation purpose
	 * author	   : Arajan
	 ****************************************************************************************/
	PROCEDURE gm_sav_return_lot_num(
		p_refid    			IN 	t5055_control_number.c5055_ref_id%TYPE,
	    p_reftype  			IN 	t5055_control_number.c901_ref_type%TYPE,
	    p_inputstr 			IN 	CLOB,
	    p_loc_oper 			IN 	t5055_control_number.c901_type%TYPE,
	    p_userid   			IN 	t5055_control_number.c5055_last_updated_by%TYPE,
	    p_rel_verify_fl		IN	VARCHAR2, 
	    p_err_fl			OUT	VARCHAR2
	)
	AS
		v_string	   VARCHAR2 (30000) := p_inputstr;
		v_pnum		   VARCHAR2 (20);
		v_qty		   NUMBER;
		v_control	   t507_returns_item.c507_control_number%TYPE;
		v_orgcontrol   VARCHAR2 (20);
		v_substring    VARCHAR2 (1000);
		v_price 	   VARCHAR2 (20);
		v_itemtype	   NUMBER;
		v_order_id	   t506_returns.c501_order_id%TYPE;
	  	v_plant_id      t5040_plant_master.c5040_plant_id%TYPE;
	  	v_comp_id		t1900_company.c1900_company_id%TYPE;
	  	v_err_msg		VARCHAR2(2000) := '';
	  	v_err_fl		VARCHAR2(1) := 'N';
	  	v_flag			VARCHAR2(1) := 'N';
	  	v_unit_price   VARCHAR2(50);
		v_unit_price_adj   VARCHAR2(50);
		v_adj_code   VARCHAR2(50);
		v_list_price  VARCHAR2(20);
		v_do_unit_price		t507_returns_item.C507_DO_UNIT_PRICE%TYPE;
		v_adj_code_val		t507_returns_item.C507_ADJ_CODE_VALUE%TYPE;
		v_orderid			t501_order.c501_order_id%TYPE;
		v_mis_count 	NUMBER;
		v_comp_flag		t906_rules.c906_rule_value%TYPE;
		v_temp_pnum 	CLOB := '';
		v_pnum_str 		VARCHAR2(100);
		v_actual_fs t501_order.c501_distributor_id%TYPE; --Added for PMT-29681
        v_lot_error_type t5063_lot_error_info.c901_error_type%TYPE; --Added for PMT-29681
	    v_lot_error_msg    VARCHAR2(200); --Added for PMT-29681
	    v_return_date t506_returns.C506_CREATED_DATE%TYPE; --Added for PMT-29681
	    v_returnitem_id t507_returns_item.C507_RETURNS_ITEM_ID%TYPE;  --Added for PMT-29861
	BEGIN
		
		SELECT  NVL(get_plantid_frm_cntx(),get_rule_value('DEFAULT_PLANT','ONEPORTAL'))
				, NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL'))
			INTO  v_plant_id, v_comp_id FROM DUAL;
		
		 SELECT get_rule_value(v_comp_id,'LOT_TRACK') INTO v_comp_flag FROM DUAL;
		 
	   -- if the flag is getting as 'Y', we need to do the validaiton check for that company otherwise no
	   IF v_comp_flag IS NULL OR v_comp_flag <> 'Y' 
	   THEN
	   		RETURN;
	   END IF;
			
			SELECT c501_order_id
			  INTO v_order_id
			  FROM t506_returns
			 WHERE c506_rma_id = p_refid
		FOR UPDATE;
			
			SELECT COUNT(1)
		 	 INTO v_mis_count
		 	 FROM t507_returns_item
			 WHERE c506_rma_id = p_refid AND c507_missing_fl = 'Y';
		--
		
	  	--insert new value into returns table
		WHILE INSTR (v_string, '|') <> 0
		LOOP
			--
			v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
			v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
			v_pnum		:= NULL;
			v_qty		:= NULL;
			v_control	:= NULL;
			v_orgcontrol := NULL;
			v_itemtype	:= NULL;
			v_price     := NULL;
			v_unit_price := NULL;
			v_unit_price_adj := NULL;
			v_adj_code := NULL;
			v_pnum		:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_qty		:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_control	:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_orgcontrol := SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_itemtype	:= TO_NUMBER (SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1));
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_price	:= TO_NUMBER (SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1));
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_unit_price	:= TO_NUMBER (SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1));
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_unit_price_adj	:= TO_NUMBER (SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1));
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_adj_code	:=  TO_NUMBER(v_substring);
			
			IF(v_price IS NULL)THEN
				IF (v_order_id IS NULL)
				THEN
					v_price 	:= get_part_price (v_pnum, 'E');
					v_list_price:= get_part_price (v_pnum, 'L');
				ELSE
					SELECT c502_item_price , c502_list_price
					  INTO v_price , v_list_price
					  FROM t502_item_order
					 WHERE c501_order_id = v_order_id AND c205_part_number_id = v_pnum AND C502_VOID_FL IS NULL AND ROWNUM = 1;
				END IF;
			END IF;
			v_pnum_str := '|'||v_pnum||'|';
			-- to avoid the duplicate rows in T507 table
			/*
		     * Get the part attribute type to validate the lot number from the part number setup screen
		     * If the attribute value is 'Y' validate the lot number for the part, otherwise skip the validation
		     * 104480: Lot tracking required ?
		     */
		     v_flag := get_part_attr_value_by_comp(v_pnum,'104480',v_comp_id);
		     
		     -- If the attribute is not set for the part, then should not validat the part
		     IF v_flag IS NULL OR v_flag <> 'Y' THEN
		     	CONTINUE;
		     END IF;
		    --If the control number is wrong in receive return screen,the control number row should not deleted  
		    IF NVL(INSTR(v_temp_pnum,v_pnum_str),0) = 0 THEN
            IF (v_mis_count = 0)
	        	THEN
					DELETE FROM t507_returns_item
		   			WHERE c506_rma_id = p_refid AND c507_status_fl <> 'Q';
				ELSE
					DELETE FROM t507_returns_item
		 			WHERE c506_rma_id = p_refid
					AND c507_status_fl <> 'Q'
					AND c205_part_number_id NOT IN (SELECT c205_part_number_id
					FROM t205d_part_attribute
					WHERE c901_attribute_type = 92340);
			END IF;
              v_temp_pnum := v_temp_pnum||'|'||v_pnum||'|';
			END IF;

			-- save the lot numbers in the table t5055_control_number
			IF (v_mis_count = 0)
			THEN
			INSERT INTO t507_returns_item
								(c507_returns_item_id, c506_rma_id, c205_part_number_id, c507_control_number
							   , c507_item_qty, c507_item_price,c507_org_control_number, c901_type
							   ,C507_UNIT_PRICE,C507_UNIT_PRICE_ADJ_VALUE ,C901_UNIT_PRICE_ADJ_CODE , C507_LIST_PRICE
							   , C507_DO_UNIT_PRICE, C507_ADJ_CODE_VALUE 
								)
						 VALUES (s507_return_item.NEXTVAL, p_refid, v_pnum, v_control
							   , v_qty, v_price, NVL (v_orgcontrol, '-'), v_itemtype 
							   , v_unit_price , v_unit_price_adj , v_adj_code , v_list_price
							   , v_do_unit_price, v_adj_code_val
								);
			END IF;
			
		    

		    
		    -- Validate the lot numbers 
			gm_pkg_op_lot_track.gm_validate_input_data(p_reftype,p_refid,p_loc_oper,'',v_pnum,v_control,v_qty, v_err_msg);
			
			-- Set the flag to 'Y', if any error message is there to return the value 
			IF v_err_msg IS NOT NULL OR v_err_msg != '' THEN
				v_err_fl := 'Y';
			END IF;
			
		   --validating the part is required to track lot  --Added for PMT-29861
           SELECT get_part_attr_value_by_comp(v_pnum,'104480',v_comp_id) INTO v_flag FROM DUAL;
           
           IF v_flag = 'Y' AND (v_err_msg IS NULL OR v_err_msg = '') --for fieldsales mismatch PMT-29681
           THEN
		                 
             /*Getting Return Item ID, transaction field sales id, return date for PMT-29681*/
            BEGIN
				SELECT t507.c507_returns_item_id, t506.c701_distributor_id, t506.c506_created_date
					INTO  v_returnitem_id, v_actual_fs, v_return_date
				FROM t507_returns_item t507, t506_returns t506
				 WHERE t507.C205_PART_NUMBER_ID = v_pnum
				 AND t507.C507_CONTROL_NUMBER = v_control
				 AND t507.C506_RMA_ID = p_refid
				 AND t507.C506_RMA_ID = t506.C506_RMA_ID
				 AND  t507.c507_status_fl='R'                     /*get unique record for PMT-34056 : Unable to complete direct sales RA in BBA */
				 AND t506.C506_VOID_FL IS NULL;
			  EXCEPTION
            WHEN NO_DATA_FOUND THEN
                v_actual_fs := NULL;
            END;        
          
          
              gm_pkg_op_lot_validation.gm_validate_lot_number(v_control, v_pnum, v_actual_fs, v_lot_error_msg, v_lot_error_type); --calling validation for PMT-29681
                  
            IF v_lot_error_msg IS NOT NULL AND v_lot_error_type = '107975'  --Field Sales Mismatch
              THEN
               
                --This procedure will save the Error details in t5063_lot_error_info table 
                 gm_pkg_op_lot_error_txn.gm_sav_lot_error(v_lot_error_type, v_lot_error_msg, v_pnum, v_control, p_refid, v_return_date, NULL, p_userid, 107981, v_actual_fs, v_returnitem_id); --107981 Return
                 
            END IF;
          END IF;			
			/*
			 * The insert script is moved before validating the part number. Since if the "Serial Number Needed" [PD tab in Part number setup screen]
			 * flag is not checked for the part then it will return and will not execute the procedure and hence the insert will not be happening
			 * The status flag is moved with Update script for BUG-7642.
			 * Here since the records are inserting before validation, It is wrongly validating the lot number while receiving the return  
			 * So we should update the status flag only after the validation 
			 */
			IF (v_mis_count = 0)
			THEN
				UPDATE t507_returns_item 
					SET C507_ERROR_DTLS = v_err_msg 
						, c507_status_fl = 'R'
				 WHERE C506_RMA_ID 	     = p_refid
				 AND C205_PART_NUMBER_ID = v_pnum
				 AND c507_status_fl IS NULL
				 AND C507_CONTROL_NUMBER = v_control;
			END IF;
			      
		END LOOP;
		
		p_err_fl := v_err_fl;
		
	END gm_sav_return_lot_num;

	/***************************************************************************************
	 * Description : Procedure to Save the lot number in t507 table for validation purpose
	 * author	   : Arajan
	 ****************************************************************************************/
	PROCEDURE gm_sav_return_reconf_lot_num(
		p_refid    			IN 	t506_returns.c506_rma_id%TYPE,
	    p_reftype  			IN 	t5055_control_number.c901_ref_type%TYPE,
	    p_inputstr 			IN 	CLOB,
	    p_loc_oper 			IN 	t5055_control_number.c901_type%TYPE,
	    p_userid   			IN 	t5055_control_number.c5055_last_updated_by%TYPE,
	    p_rel_verify_fl		IN	VARCHAR2
	)
	AS
		v_string	   VARCHAR2 (30000) := p_inputstr;
		v_pnum		   VARCHAR2 (20);
		v_qty		   NUMBER;
		v_control	   t507_returns_item.c507_control_number%TYPE;
		v_orgcontrol   VARCHAR2 (20);
		v_substring    VARCHAR2 (1000);
		v_price 	   VARCHAR2 (20);
		v_itemtype	   NUMBER;
		v_order_id	   t506_returns.c501_order_id%TYPE;
	  	v_plant_id      t5040_plant_master.c5040_plant_id%TYPE;
	  	v_comp_id		t1900_company.c1900_company_id%TYPE;
	  	v_err_msg		VARCHAR2(2000) := '';
	  	v_err_fl		VARCHAR2(1) := 'N';
	  	v_flag			VARCHAR2(1) := 'N';
	  	v_unit_price   VARCHAR2(50);
		v_unit_price_adj   VARCHAR2(50);
		v_adj_code   VARCHAR2(50);
		v_list_price  VARCHAR2(20);
		v_do_unit_price		t507_returns_item.C507_DO_UNIT_PRICE%TYPE;
		v_adj_code_val		t507_returns_item.C507_ADJ_CODE_VALUE%TYPE;
		v_orderid			t501_order.c501_order_id%TYPE;
		v_parent_id			t506_returns.c506_rma_id%TYPE;
		v_parent_status_id	t506_returns.c506_status_fl%TYPE;
		v_orgcontrl    VARCHAR2 (20);
		v_comp_flag VARCHAR2(1);
		v_actual_fs t501_order.c501_distributor_id%TYPE; --Added for PMT-29681
        v_lot_error_type t5063_lot_error_info.c901_error_type%TYPE; --Added for PMT-29681
	    v_lot_error_msg    VARCHAR2(200); --Added for PMT-29681
	    v_return_date t506_returns.C506_CREATED_DATE%TYPE; --Added for PMT-29681 
	    v_returnitem_id t507_returns_item.C507_RETURNS_ITEM_ID%TYPE;  --Added for PMT-29861
	BEGIN
		
		SELECT  NVL(get_plantid_frm_cntx(),get_rule_value('DEFAULT_PLANT','ONEPORTAL'))
				, NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL'))
			INTO  v_plant_id, v_comp_id FROM DUAL;
		 SELECT get_rule_value(v_comp_id,'LOT_TRACK') INTO v_comp_flag FROM DUAL; 
	   -- if the flag is getting as 'Y', we need to do the validaiton check for that company otherwise no
	   IF v_comp_flag IS NULL OR v_comp_flag <> 'Y' 
	   THEN
		   RETURN;
	   END IF;
		BEGIN
			SELECT c506_parent_rma_id INTO v_parent_id
			FROM t506_returns
			WHERE c506_rma_id = p_refid
				AND c506_void_fl IS NULL;
		EXCEPTION WHEN NO_DATA_FOUND
		THEN
			v_parent_id := NULL;
		END;
		
		BEGIN
			SELECT c506_status_fl INTO v_parent_status_id
			FROM t506_returns
			WHERE c506_rma_id = v_parent_id
				AND c506_void_fl IS NULL;
		EXCEPTION WHEN NO_DATA_FOUND
		THEN
			v_parent_id := NULL;
		END;
		
	  	--insert new value into returns table
		WHILE INSTR (v_string, '|') <> 0
		LOOP
			--
			v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
			v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
			v_pnum		:= NULL;
			v_qty		:= NULL;
			v_control	:= NULL;
			v_itemtype	:= NULL;
			v_price		:= NULL;
			v_unit_price := NULL;
			v_unit_price_adj := NULL;
			v_adj_code  := NULL;
			v_pnum		:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_qty		:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_control	:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_orgcontrl := SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_itemtype	:= TO_NUMBER (SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1));
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_price	:= TO_NUMBER (SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1));
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_unit_price	:= TO_NUMBER (SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1));
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_unit_price_adj	:= TO_NUMBER (SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1));
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_adj_code	:=  TO_NUMBER (v_substring);
			
			
		    /*
		     * Get the part attribute type to validate the lot number from the part number setup screen
		     * If the attribute value is 'Y' validate the lot number for the part, otherwise skip the validation
		     * 104480: Lot tracking required ?
		     */
		     v_flag := get_part_attr_value_by_comp(v_pnum,'104480',v_comp_id);

		     -- If the attribute is not set for the part, then should not validat the part
		     IF v_flag IS NULL OR v_flag <> 'Y' THEN
		     	CONTINUE;
		     END IF;
		    
		    -- Validate the lot numbers 
			gm_pkg_op_lot_track.gm_validate_input_data(p_reftype,p_refid,p_loc_oper,'',v_pnum,v_control,v_qty, v_err_msg);
			
			-- Set the flag to 'Y', if any error message is there to return the value 
			IF v_err_msg IS NOT NULL OR v_err_msg != '' THEN
				
				UPDATE t507_returns_item
					SET c507_error_dtls = v_err_msg
				WHERE c506_rma_id = p_refid
					AND c205_part_number_id = v_pnum
					AND c507_control_number = v_control
					AND c507_status_fl <> 'Q';
				
			END IF;
			
			
			 --validating the part is required to track lot  --Added for PMT-29861
           SELECT get_part_attr_value_by_comp(v_pnum,'104480',v_comp_id) INTO v_flag FROM DUAL;
           
           IF v_flag = 'Y' AND (v_err_msg IS NULL OR v_err_msg = '') --for fieldsales mismatch PMT-29681
           THEN
		                 
             /*Getting Return Item ID, transaction field sales id, return date for PMT-29681 */
            BEGIN
				SELECT t507.c507_returns_item_id, NVL(t501.c501_distributor_id,t506.c701_distributor_id), t506.c506_created_date
					INTO  v_returnitem_id, v_actual_fs, v_return_date
				FROM t507_returns_item t507, t506_returns t506, t501_order t501
				 WHERE t507.c205_part_number_id = v_pnum
				 AND t507.c507_control_number = v_control
				 AND t507.c506_rma_id = p_refid
				 AND t507.c506_rma_id = t506.c506_rma_id
				 AND t507.c507_status_fl='R'                     /*get unique record for PMT-34056 : Unable to complete direct sales RA in BBA */              
         		 AND t506.c501_order_id = t501.c501_order_id(+)
         		 AND t501.c501_void_fl(+) IS NULL
				 AND t506.c506_void_fl IS NULL;
			  EXCEPTION
            WHEN NO_DATA_FOUND THEN
                v_actual_fs := NULL;
            END;  
            
                 --calling validation for PMT-29681
                  gm_pkg_op_lot_validation.gm_validate_lot_number(v_control, v_pnum, v_actual_fs, v_lot_error_msg, v_lot_error_type); 
                  
              IF v_lot_error_msg IS NOT NULL AND v_lot_error_type = '107975'  --Field Sales Mismatch
              THEN
               
                --This procedure will save the Error details in t5063_lot_error_info table 
                 gm_pkg_op_lot_error_txn.gm_sav_lot_error(v_lot_error_type, v_lot_error_msg, v_pnum, v_control, p_refid, v_return_date, NULL, p_userid, 107981, v_actual_fs, v_returnitem_id); --107981 Return
                 
             END IF;
			END IF;
		END LOOP;
		
			
		
		SELECT count(1) INTO v_err_fl
			FROM t507_returns_item
		WHERE  c506_rma_id= p_refid
			AND c507_status_fl <> 'Q'
			AND c507_error_dtls IS NOT NULL
			AND c507_control_number IS NOT NULL;
		
		IF v_err_fl = 'Y' THEN
			UPDATE t506_returns
				SET c506_status_fl = 1
			WHERE c506_rma_id = p_refid;	
			
			IF v_parent_status_id = '2' THEN
				UPDATE t506_returns
			    	SET c506_status_fl = '1'
			 	WHERE c506_rma_id = v_parent_id;
			 END IF;
		END IF;
		
	END gm_sav_return_reconf_lot_num;
	
	
/******************************************************************************
 * Purpose: function is used to fetch the control number for the returns
 *******************************************************************************/
  FUNCTION get_returned_cnums (
		p_consignment_id   IN	t505_item_consignment.c504_consignment_id%TYPE
	  ,	p_ra_id            IN	t507_returns_item.c506_rma_id%TYPE
	  ,	p_cons_item_id     IN	t505_item_consignment.c505_item_consignment_id%TYPE
	  , p_part_id		   IN	t205_part_number.c205_part_number_id%TYPE
	)
		RETURN VARCHAR
	IS
	
	v_cntrl_number t505_item_consignment.c505_control_number%TYPE;
	v_plant_id     t5040_plant_master.c5040_plant_id%type;
	v_count number := 0;
	v_cons_pnum    t505_item_consignment.c205_part_number_id%TYPE;
	v_cons_qty  number   := 0;
	v_ret_pnum     t507_returns_item.c205_part_number_id%TYPE;
	v_ret_qty  number    := 0;
	
--
	BEGIN

		SELECT  get_plantid_frm_cntx()
		  INTO  v_plant_id
		  FROM dual;
	
		IF (p_consignment_id = '')
		THEN
			RETURN '';
		END IF;	
		
		SELECT t505.c205_part_number_id ,sum(t505.c505_item_qty) 
		  INTO v_cons_pnum,v_cons_qty      
		  FROM t505_item_consignment t505
		 WHERE t505.c504_consignment_id = p_consignment_id
	       and  t505.c205_part_number_id  = p_part_id
		   AND t505.c505_void_fl              IS NULL
	     GROUP BY t505.c205_part_number_id;
	     
	     SELECT t507.c205_part_number_id ,sum(c507_item_qty)
	       INTO v_ret_pnum,v_ret_qty 
		   FROM t507_returns_item t507
		  WHERE t507.c205_part_number_id  = p_part_id
		    AND c507_status_fl IN ('Q') 
		    AND c506_rma_id  = p_ra_id
	      GROUP BY t507.c205_part_number_id;
	
		SELECT count(1)
		  INTO v_count
		  FROM t505_item_consignment t505,t504_consignment t504
		 WHERE t505.c504_consignment_id = p_consignment_id 
		   AND c205_part_number_id = p_part_id
		   AND t504.c5040_plant_id = v_plant_id
	       AND t504.c504_consignment_id = t505.c504_consignment_id;
	       
	    IF (v_count = 1 OR (v_cons_qty = v_ret_qty) OR (v_cons_qty < v_ret_qty)) THEN
	    
			SELECT c505_control_number
			  INTO v_cntrl_number
			  FROM t505_item_consignment t505,t504_consignment t504
			 WHERE t505.c504_consignment_id = p_consignment_id 
			   AND t505.c505_item_consignment_id = p_cons_item_id
			   AND c205_part_number_id = p_part_id 
			   AND t504.c5040_plant_id = v_plant_id
			   AND t504.c504_consignment_id = t505.c504_consignment_id;
	    ELSE
	        
			SELECT c505_control_number
			  INTO v_cntrl_number
			  FROM t505_item_consignment t505,t504_consignment t504
			 WHERE t505.c504_consignment_id = p_consignment_id 
			   AND c205_part_number_id = p_part_id 
			   AND t504.c5040_plant_id = v_plant_id
			   AND t504.c504_consignment_id = t505.c504_consignment_id
			   and ROWNUM =1;
	    END IF;    
	
		RETURN v_cntrl_number;
	EXCEPTION
		WHEN OTHERS
		THEN
			RETURN '';
	END get_returned_cnums;

END gm_pkg_op_return;

/
