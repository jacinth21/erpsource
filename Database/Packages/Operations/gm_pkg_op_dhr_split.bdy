/*
 * @"C:\Database\Packages\Operations\gm_pkg_op_dhr_split.bdy";
 */

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_dhr_split IS
  --

  /*******************************************************************
  * Description : Procedure will do the DHR Split for the passed in Information.
  * Logic Implemented in this entire Package:
  *  For the passed in DHR ID , set the DHR part number in the Temp Part List.
  *  For the DHR Part, get all the parent part along with the BOM and populate the T410_DHR_PARENT_PARTS
  *  From the T410_DHR_PARENT_PARTS, get the DHR part and loop through the list.
  *    For each DHR Part, get all Parent Part and  set it in the Temp Part List (as it will be required for V205_quantity_stock view).
  *  If the DHR Part is Not in Set List and if its part of BOM, then the part will go directly to RM.
  *  For the part(s) get the Inventory quantity (Shelf, Bulk, RW, RM) @gm_calc_inv_qty
  *  For the part(s) get the quantity that is allocated in at different bucket (Shelf, Bulk, RM, RW) @gm_calc_tans_allocin_qty
  *  For the Part(s) get the US, OUS Sales Quantity @gm_calc_sales_qty
  *  For the Part(s) get the Request Quantity(TPR,Open RQ,Set PAR) @gm_calc_pend_req_qty
  *  For the Part(s) get the Sales BO (Sales, Item from Shipped Set, Loaner, Set) @gm_calc_sales_bo
  *  All the values obtained in the above procedures are updated back to T410_DHR_PARENT_PARTS or T410A_DHR_CALCULATION if we are processing
  *    the Parent Part or DHR PArt.
  *  
  *  After the Parent Part details are populated (T410_DHR_PARENT_PARTS) we should calculate Parent Need (US Sales, OUS Sales, US Bulk, OUS Bulk) @gm_calc_parent_need
  *  After all required values are populated, the DHR Split calculation is done @gm_populate_dhr_split_calc
  *  After all the DHR Split is calcualted, populate the 1.5 Month Sales, 2 Month Set Need @gm_upd_set_sales_need
  *  Write the cursor to get #to_Shelf #to_bulk #to_RM #1.5 Month Sales #2 Month Set Need.
              
  * Author     : Rajeshwaran
  *******************************************************************/
  PROCEDURE gm_fch_dhr_split_dtls
  (
    p_tokeni     IN VARCHAR,
    p_dhr_id     IN t408_DHR.C408_DHR_ID%TYPE DEFAULT NULL,
    p_report_out OUT TYPES.cursor_type
  ) AS
    v_cnt    NUMBER;
    v_dhr_id VARCHAR2(255) := p_dhr_id;
    v_temp   NUMBER;
    v_company_id  t1900_company.c1900_company_id%TYPE;
   v_plant_id T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
  BEGIN
  SELECT	get_compid_frm_cntx(), get_plantid_frm_cntx()
	INTO    v_company_id, v_plant_id
	FROM	dual;
	
    DELETE FROM my_temp_part_list;
    DELETE FROM t410_dhr_parent_parts;
    DELETE FROM T410A_DHR_CALCULATION;
  
    my_context.set_double_inlist_ctx(p_tokeni);
  
    SELECT INSTR(v_dhr_id, 'GM-DHR-') INTO v_temp FROM DUAL;
  
    IF (v_temp = 0)
    THEN
      v_dhr_id := 'GM-DHR-' || v_dhr_id;
    END IF;
  
    INSERT INTO my_temp_part_list
      (c205_part_number_id, c205_qty)
      SELECT DISTINCT t205.c205_part_number_id,
                      DECODE(SIGN(NVL(SUM(t408.c408_shipped_qty), 0) -
                                   NVL(SUM(t408.c408_qty_rejected), 0)),
                              -1,
                              0,
                              NVL(SUM(t408.c408_shipped_qty), 0) -
                              NVL(SUM(t408.c408_qty_rejected), 0))
        FROM t408_dhr t408, t205_part_number t205
       WHERE t408.c408_status_fl IN (0, 1, 2, 3)
             AND t408.c408_void_fl IS NULL
             AND T408.c1900_company_id = v_company_id
             AND T408.C5040_PLANT_ID = v_plant_id   
             AND t408.c408_dhr_id = NVL(v_dhr_id, t408.c408_dhr_id)
             AND t408.c205_part_number_id = t205.c205_part_number_id
             AND
             (t205.c205_part_number_id IN
             (SELECT tokenii FROM v_double_in_list WHERE token = 'PART') OR
             t205.c202_project_id IN
             (SELECT tokenii FROM v_double_in_list WHERE token = 'PROJECT'))
       GROUP BY t205.c205_part_number_id;
  
    gm_populate_parent_parts();
    gm_populate_dhr_split_calc();
  
    
    OPEN p_report_out FOR
      SELECT C205_DHR_PART_NUMBER pnum,
             get_partnum_desc(C205_DHR_PART_NUMBER) description,
             NVL(C410A_DHR_QTY, 0) DHRQTY,
             NVL(C410A_SALES_NEED, 0) FORECAST_QTY,
             NVL(C410A_SET_NEED, 0) SET_NEED,
             NVL(C410A_TO_SHELF, 0) TO_SHELF, 
             NVL(C410A_TO_BULK, 0) TO_BULK,
             NVL(C410A_TO_RM, 0) TO_RM
        FROM T410A_DHR_CALCULATION;
  
  END gm_fch_dhr_split_dtls;

  /*******************************************************
  * Description : This procedure will populate the T410_DHR_PARENT_PARTS
  *         with the DHR part (from Temp Table) and the Parent Parent for the DHR Part.
  * Author     : Rajeshwaran
  *******************************************************/

  PROCEDURE gm_populate_parent_parts AS
  
    v_dhr_partnum     t205_part_number.c205_part_number_id%TYPE;
    v_dhr_qty         T410_DHR_PARENT_PARTS.C410_DHR_QTY%TYPE;
    v_bom_qty         t205a_part_mapping.c205a_qty%TYPE;
    v_dhr_parent_part t205_part_number.c205_part_number_id%TYPE;
    v_mwo_qty         NUMBER;
    v_sub_prt_to_ordr VARCHAR2(1);
    
    CURSOR dhr_part IS
      SELECT c205_part_number_id dhr_pnum, C205_QTY dhr_qty
        FROM my_temp_part_list;

    --- added new column c205a_void_fl in t205a used to PMT-50777 - Create BOM and sub-component Mapping
	-- PC-2080: to fix the user request cancel error 
	--PC-3708- Add nocycle in belwo cursor - fix for connect by loop error       
    CURSOR dhr_parent_part IS
    SELECT * FROM (
	      SELECT C205_FROM_PART_NUMBER_ID parent_part, C205A_QTY bom_qty
	        FROM T205A_PART_MAPPING T205A
	        WHERE T205A.C205A_VOID_FL IS NULL
	         START WITH T205A.C205_TO_PART_NUMBER_ID = v_dhr_partnum
	      CONNECT BY NOCYCLE PRIOR T205A.C205_FROM_PART_NUMBER_ID =
	                  T205A.C205_TO_PART_NUMBER_ID
	      )WHERE 1 = v_sub_prt_to_ordr;
	      -- This query to get output only for part that is having sub-part-to order mapping.
  
    v_cnt NUMBER;
  BEGIN
  
    FOR var_dhr_part IN dhr_part
    LOOP
      v_dhr_partnum := var_dhr_part.dhr_pnum;
      v_dhr_qty     := var_dhr_part.dhr_qty;
      
      BEGIN
	      
	      --Check if the DHR part is having "sub-part to order" mapping. If it is not in the SPO mapping,
	      --we should not consider the parent part. Eg will be 1067.1540.
	      -- This part will be sold as itself and all the parent will be coming from vendor as finished product,so
	      --this need not go to RM even though 1067.1540 does have parent parts.
	      
	      SELECT DECODE(C205_PART_NUMBER_ID,'',0,1) INTO v_sub_prt_to_ordr 
	      FROM T205D_SUB_PART_TO_ORDER
	      WHERE C205_PART_NUMBER_ID = v_dhr_partnum
	      AND C205D_VOID_FL IS NULL;
	      
      EXCEPTION WHEN NO_DATA_FOUND
      THEN
      		v_sub_prt_to_ordr := 0;
      END;
      	
      DELETE FROM T410_DHR_PARENT_PARTS
       WHERE C205_DHR_PART_NUMBER = v_dhr_partnum;
    
      FOR var_dhr_parent_part IN dhr_parent_part
      LOOP
        v_dhr_parent_part := var_dhr_parent_part.parent_part;
        v_bom_qty         := var_dhr_parent_part.bom_qty;
      
        -- If the Parent Part is having any Manf.WorkOrder that quantity has to be included as "Alloc In" .
        BEGIN
          SELECT (NVL(t408.c408_shipped_qty, 0) -
                  NVL(t408.c408_qty_rejected, 0)) +
                  DECODE(t408.c408_status_fl, 0, t408.c408_qty_received, 0)
            INTO v_mwo_qty
            FROM t401_purchase_order t401, t402_work_order t402,
                 t408_dhr t408
           WHERE t401.c401_type IN ('3105', '3104')
                 AND t401.c401_purchase_ord_id = t402.c401_purchase_ord_id
                 AND t402.c402_work_order_id = t408.c402_work_order_id
                 AND t408.c205_part_number_id = v_dhr_parent_part
                 AND t401.c401_void_fl IS NULL
                 AND t402.c402_void_fl IS NULL
                 AND t408.c408_void_fl IS NULL
                 AND t408.c408_status_fl < 4;
        EXCEPTION
          WHEN OTHERS THEN
            v_mwo_qty := 0;
        END;
      
        INSERT INTO T410_DHR_PARENT_PARTS
          (C205_DHR_PART_NUMBER, C410_DHR_QTY, C205_DHR_PARENT_PART,
           C205_DHR_PARENT_PART_QTY, C410_PARENT_SHELF_ALLOC_IN)
        VALUES
          (v_dhr_partnum, v_dhr_qty, v_dhr_parent_part, v_bom_qty,
           v_mwo_qty);
      
        v_mwo_qty := 0;
      END LOOP;
    
      SELECT COUNT(1)
        INTO v_cnt
        FROM T410_DHR_PARENT_PARTS
       WHERE C205_DHR_PART_NUMBER = v_dhr_partnum;
    
      -- If the DHR part does not have any parent, this condition will fire.
      IF v_cnt = 0
      THEN
        INSERT INTO T410_DHR_PARENT_PARTS
          (C205_DHR_PART_NUMBER, C410_DHR_QTY, C205_DHR_PARENT_PART,
           C205_DHR_PARENT_PART_QTY)
        VALUES
          (v_dhr_partnum, v_dhr_qty, '', '');
      END IF;
    END LOOP;
  
  END gm_populate_parent_parts;

  /*******************************************************
  * Description : This procedure will populate the T410A_DHR_CALCULATION
  *         With all the data  like Sales, TPR, For DHR Part, Parent Part for US, OUS.
  * Author     : Rajeshwaran
  *******************************************************/
  PROCEDURE gm_populate_dhr_split_calc AS
  
    v_to_shelf  NUMBER;
    v_to_bulk   NUMBER;
    v_to_rmt    NUMBER;
    v_dhr_part  T410_DHR_PARENT_PARTS.C205_DHR_PART_NUMBER%TYPE;
    v_set_count NUMBER;
    v_dhr_qty   T410_DHR_PARENT_PARTS.C410_DHR_QTY%TYPE;
    v_bom_count T410_DHR_PARENT_PARTS.C410_DHR_QTY%TYPE;
    b_do_calculation BOOLEAN := true;
	v_product_family NUMBER;
	v_split_to VARCHAR2(20);
    
    CURSOR dhr_part IS
      SELECT DISTINCT C205_DHR_PART_NUMBER dhr_part, C410_DHR_QTY dhr_qty ,C205_PRODUCT_FAMILY product_family
        FROM T410_DHR_PARENT_PARTS T410,T205_PART_NUMBER T205
        WHERE T205.C205_PART_NUMBER_ID = T410.C205_DHR_PART_NUMBER;
  
    CURSOR parent_part IS
      SELECT C205_DHR_PARENT_PART parent_part
        FROM T410_DHR_PARENT_PARTS
       WHERE C205_DHR_PARENT_PART IS NOT NULL
             AND C205_DHR_PART_NUMBER = v_dhr_part;
  
  BEGIN
    FOR var_dhr_part IN dhr_part
    LOOP
      v_dhr_part 		:= var_dhr_part.dhr_part;
      v_dhr_qty  		:= var_dhr_part.dhr_qty;
      v_product_family	:= var_dhr_part.product_family;
    
    
      DELETE FROM T410A_DHR_CALCULATION
       WHERE C205_DHR_PART_NUMBER = var_dhr_part.dhr_part;
      INSERT INTO T410A_DHR_CALCULATION
        (C205_DHR_PART_NUMBER, C410A_DHR_QTY)
      VALUES
        (var_dhr_part.dhr_part, v_dhr_qty);
    
      -- Check if the DHR Part is there in any Set List. If it is not, then it might be a RM Part and has to be sent to RM.
      SELECT COUNT(1)
        INTO v_set_count
        FROM t208_set_details t208, t207_set_master t207
       WHERE t207.c207_set_id = t208.c207_set_id
         AND C901_SET_GRP_TYPE = 1601
         AND C207_TYPE NOT IN (4078) -- Exclude Demo Set.
         AND t207.C901_STATUS_ID NOT IN (20369, 20368) -- Obsolete, Suspended
         AND t207.c207_void_fl IS NULL
         AND c208_void_fl IS NULL
         AND c205_part_number_id = v_dhr_part;
    
      -- Check if the DHR part is there BOM [ BOM Report under Manfucaturing].
      SELECT COUNT(1)
        INTO v_bom_count
        FROM t207_set_master
       WHERE c901_status_id NOT IN (20369)
	     AND C207_SET_DESC IN(SELECT c205_dhr_parent_part FROM t410_dhr_parent_parts);
    
      --If the DHR part is not in Set but only in BOM, then the entire qty to be sent to RM and skip calculation.
      IF (v_set_count = 0 AND v_bom_count > 0)
      THEN
      	-- The DHR part is not in Set and its available in BOM report, then the entire DHR quantity should go to RM.
        -- As the part is not going to be sold by itself and not in the Bulk.
        
      		SELECT v_dhr_qty
      		INTO v_to_rmt 
      		FROM DUAL;
      		
      		b_do_calculation := false;
      ELSIF(v_product_family='4052')
      THEN
      	-- If the Part has to be sent to Bulk this condition will be satisfied and the entire DHR quantity will go to Bulk.
      		SELECT v_dhr_qty
      		INTO v_to_bulk 
      		FROM DUAL;
      		b_do_calculation := false;
      END IF;
      
      IF(b_do_calculation)
      THEN
        gm_populate_parentpart(var_dhr_part.dhr_part, 'dhrpart');
        gm_populate_parentpart(var_dhr_part.dhr_part, 'parentpart');
        gm_calc_parent_need(var_dhr_part.dhr_part);
        gm_calc_dhr_split(var_dhr_part.dhr_part,
                          v_to_shelf,
                          v_to_bulk,
                          v_to_rmt);
      END IF;
    
      UPDATE T410A_DHR_CALCULATION
         SET C410A_TO_SHELF = NVL(v_to_shelf, C410A_TO_SHELF),
             C410A_TO_BULK  = NVL(v_to_bulk, C410A_TO_BULK),
             C410A_TO_RM    = NVL(v_to_rmt, C410A_TO_RM)
       WHERE C205_DHR_PART_NUMBER = var_dhr_part.dhr_part;
    
      --Reset variable to avoid caching.
      v_to_shelf := '';
      v_to_bulk  := '';
      v_to_rmt   := '';
      b_do_calculation := true;
      v_split_to := '';
      v_product_family := '';
      
    END LOOP;
  
    -- For all the DHR Parts, populate the 1.5 Month worldwide Sales, 2 Month Set Need. 
    my_context.set_my_inlist_ctx('100800,100801'); -- Globus Medical, Algea Therapies.
    gm_upd_set_sales_need();
  
  END gm_populate_dhr_split_calc;

  /* This is the core procedure that populate the required data [Inventory Qty, BO , Alloc IN,  TPR]
  *   etc for the DHR Part and the Parent Part.
  *   
  *   Based on the p_type, we will either update the data in T410_DHR_PARENT_PARTS (parentpart) or T410A_DHR_CALCULATION (dhrpart).
  *   This procedure will call multiple procedure to get the relevant data and the cursor will be looped and data will be updated.
  */
  PROCEDURE gm_populate_parentpart
  (
    p_part_num IN t205_part_number.c205_part_number_id%TYPE,
    p_type     IN VARCHAR2
  ) AS
    v_dhr_qty             NUMBER;
    v_shelf_qty           NUMBER;
    v_rw_qty              NUMBER;
    v_rmt_qty             NUMBER;
    v_bulk_qty            NUMBER;
    v_parent_shelf_qty    NUMBER;
    v_parent_rw_qty       NUMBER;
    v_parent_rmt_qty      NUMBER;
    v_parent_bulk_qty     NUMBER;
    v_us_sales            NUMBER;
    v_ous_sales           NUMBER;
    v_parent_us_sales     NUMBER;
    v_parent_ous_sales    NUMBER;
    v_date                DATE;
    v_tpr_us_qty          NUMBER;
    v_us_sales_bo         NUMBER;
    v_ous_sales_bo        NUMBER;
    v_ous_set_bo          NUMBER;
    v_ous_etl_bo          NUMBER;
    v_us_set_bo           NUMBER;
    v_us_loaner_backorder NUMBER;
    v_us_sales_backorder  NUMBER;
    v_tpr_cn_us_qty       NUMBER;
    v_tpr_ous_qty         NUMBER;
    v_tpr_cn_ous_qty      NUMBER;
    v_shelf_alloc_in      NUMBER;
    v_rmt_alloc_in        NUMBER;
    v_rw_alloc_in         NUMBER;
    v_bulk_alloc_in       NUMBER;
    cur_inv_det           TYPES.CURSOR_TYPE;
    cur_alloc_in          TYPES.CURSOR_TYPE;
    cur_sales             TYPES.CURSOR_TYPE;
    cur_tpr_qty           TYPES.CURSOR_TYPE;
    cur_sales_bo          TYPES.CURSOR_TYPE;
  
    v_parent_part T410_DHR_PARENT_PARTS.C205_DHR_PARENT_PART%TYPE;
    v_temp        NUMBER;
    v_lvlid       VARCHAR2(10);
    V_QTY         NUMBER;
    V_TYPE        VARCHAR2(20);
  
  BEGIN
    DELETE FROM my_temp_part_list;
  
    -- Set the Part in the Temp Table based on the type that is passed in.
    INSERT INTO my_temp_part_list
      (C205_PART_NUMBER_ID)
      SELECT DISTINCT C205_DHR_PART_NUMBER
        FROM T410_DHR_PARENT_PARTS
       WHERE DECODE(p_type, 'dhrpart', 1, -1) = 1
         AND C205_DHR_PART_NUMBER = p_part_num
      UNION
      SELECT DISTINCT C205_DHR_PARENT_PART
        FROM T410_DHR_PARENT_PARTS
       WHERE DECODE(p_type, 'parentpart', 1, -1) = 1
         AND C205_DHR_PARENT_PART IS NOT NULL
         AND C205_DHR_PART_NUMBER = p_part_num;
  
    -- Get the DHR Quantity.
    SELECT DISTINCT C410_DHR_QTY
      INTO v_dhr_qty
      FROM T410_DHR_PARENT_PARTS
     WHERE C205_DHR_PART_NUMBER = p_part_num;
  
    --Get the First Day of the current month, as it will be used to get the 1.5 month sales or 2 month forecast.
    --As in Order Planning the current month is always considered future, the same is applied here.
    SELECT TO_DATE('01/' || TO_CHAR(CURRENT_DATE, 'MM/YYYY'), 'DD/MM/YYYY')
      INTO v_date
      FROM dual;
  
    --Set the exclusion so the "v205_qty_in_stock" returns data faster.
    my_context.set_my_inlist_ctx('70006,70007,70010,70011,70012,70013,70014,70016');
  
    -- Get the Inventory quantity from the v205_qty_in_stock.
    gm_calc_inv_qty(cur_inv_det);
  
    LOOP
      FETCH cur_inv_det
        INTO v_parent_part, v_parent_shelf_qty, v_parent_rw_qty,
             v_parent_rmt_qty, v_parent_bulk_qty;
      IF (p_type = 'parentpart')
      THEN
        UPDATE T410_DHR_PARENT_PARTS
           SET C410_PARENT_SHELF_QTY = v_parent_shelf_qty,
               C410_PARENT_RW_QTY    = v_parent_rw_qty,
               C410_PARENT_BULK_QTY  = v_parent_bulk_qty
         WHERE C205_DHR_PARENT_PART = v_parent_part;
      ELSE
        UPDATE T410A_DHR_CALCULATION
           SET C410A_DHR_QTY   = v_dhr_qty,
               C410A_SHELF_QTY = v_parent_shelf_qty,
               C410A_RM_QTY    = v_parent_rmt_qty,
               C410A_RW_QTY    = v_parent_rw_qty,
               C410A_BULK_QTY  = v_parent_bulk_qty
         WHERE C205_DHR_PART_NUMBER = v_parent_part;
      
      END IF;
    
      EXIT WHEN cur_inv_det%NOTFOUND;
      v_parent_part      := '';
      v_parent_shelf_qty := '';
      v_parent_rw_qty    := '';
      v_parent_rmt_qty   := '';
      v_parent_bulk_qty  := '';
    END LOOP;
  
    -- Get the Alloc In quantity for parts in Temp Table.
    gm_calc_tans_allocin_qty(cur_alloc_in);
  
    --Based on the type for which the quantity is coming out, we will update those columns using DECODE.
    LOOP
      FETCH cur_alloc_in
        INTO v_parent_part, v_qty, v_type;
    
      IF (p_type = 'parentpart')
      THEN
        --As we populate the parent shelf alloc in much earlier, we will append the quantity that is already available in the column
        UPDATE T410_DHR_PARENT_PARTS
           SET C410_PARENT_SHELF_ALLOC_IN = DECODE(v_type,
                                                   'SHELF_PLUS',
                                                   (v_qty +
                                                   C410_PARENT_SHELF_ALLOC_IN),
                                                   C410_PARENT_SHELF_ALLOC_IN),
               C410_PARENT_BULK_ALLOC_IN  = DECODE(v_type,
                                                   'BULK_PLUS',
                                                   v_qty,
                                                   C410_PARENT_BULK_ALLOC_IN),
               C410_PARENT_RW_ALLOC_IN    = DECODE(v_type,
                                                   'RW_PLUS',
                                                   v_qty,
                                                   C410_PARENT_RW_ALLOC_IN)
         WHERE C205_DHR_PARENT_PART = v_parent_part;
      ELSE
        UPDATE T410A_DHR_CALCULATION
           SET C410A_SHELF_ALLOC_IN = DECODE(v_type,
                                             'SHELF_PLUS',
                                             v_qty,
                                             C410A_SHELF_ALLOC_IN),
               C410A_BLK_ALLOC_IN   = DECODE(v_type,
                                             'BULK_PLUS',
                                             v_qty,
                                             C410A_BLK_ALLOC_IN),
               C410A_RM_ALLOC_IN    = DECODE(v_type,
                                             'RM_PLUS',
                                             v_qty,
                                             C410A_RM_ALLOC_IN),
               C410A_RW_ALLOC_IN    = DECODE(v_type,
                                             'RW_PLUS',
                                             v_qty,
                                             C410A_RW_ALLOC_IN)
         WHERE C205_DHR_PART_NUMBER = v_parent_part;
      
      END IF;
      EXIT WHEN cur_alloc_in%NOTFOUND;
    
      v_parent_part := '';
      v_qty         := '';
      v_type        := '';
    END LOOP;
  
    -- Get the Sales quantity for parts in Temp Table for US, OUS.   
    my_context.set_my_inlist_ctx('100823,100824'); -- US, OUS
    gm_calc_sales_qty(v_date, cur_sales);
  
    LOOP
      FETCH cur_sales
        INTO v_parent_part, v_qty, v_lvlid;
    
      IF (p_type = 'parentpart')
      THEN
        UPDATE T410_DHR_PARENT_PARTS
           SET C410_PARENT_US_SALES  = DECODE(v_lvlid,
                                              '100823',
                                              v_qty,
                                              C410_PARENT_US_SALES),
               C410_PARENT_OUS_SALES = DECODE(v_lvlid,
                                              '100824',
                                              v_qty,
                                              C410_PARENT_OUS_SALES)
         WHERE C205_DHR_PARENT_PART = v_parent_part;
      ELSE
      
        UPDATE T410A_DHR_CALCULATION
           SET C410A_TOT_US_SALES  = DECODE(v_lvlid,
                                            '100823',
                                            v_qty,
                                            C410A_TOT_US_SALES),
               C410A_TOT_OUS_SALES = DECODE(v_lvlid,
                                            '100824',
                                            v_qty,
                                            C410A_TOT_OUS_SALES)
         WHERE C205_DHR_PART_NUMBER = v_parent_part;
      
      END IF;
      EXIT WHEN cur_sales%NOTFOUND;
    
      v_parent_part := '';
      v_qty         := '';
      v_lvlid       := '';
    END LOOP;
    DBMS_OUTPUT.PUT_LINE('3.. Sales Qty Comp' ||
                         TO_CHAR(CURRENT_DATE, 'MM/DD/YYYY HH:MI:SS'));
  
    -- Get the TPR quantity for parts in Temp Table for US, OUS.   
    gm_calc_pend_req_qty(cur_tpr_qty);
  
    LOOP
      FETCH cur_tpr_qty
        INTO v_parent_part, v_qty, v_type;
      IF (p_type = 'parentpart')
      THEN
        UPDATE T410_DHR_PARENT_PARTS
           SET C410_PARENT_TPR     = DECODE(v_type,
                                            'US',
                                            v_qty,
                                            C410_PARENT_TPR),
               C410_PARENT_OUS_TPR = DECODE(v_type,
                                            'OUS',
                                            v_qty,
                                            C410_PARENT_OUS_TPR)
         WHERE C205_DHR_PARENT_PART = v_parent_part;
      ELSE
        UPDATE T410A_DHR_CALCULATION
           SET C410A_TPR     = DECODE(v_type, 'US', v_qty, C410A_TPR),
               C410A_OUS_TPR = DECODE(v_type, 'OUS', v_qty, C410A_OUS_TPR)
         WHERE C205_DHR_PART_NUMBER = v_parent_part;
      END IF;
    
      EXIT WHEN cur_tpr_qty%NOTFOUND;
    
      v_parent_part := '';
      v_qty         := '';
      v_type        := '';
    END LOOP;
    DBMS_OUTPUT.PUT_LINE('4.. Pend Req Comp' ||
                         TO_CHAR(CURRENT_DATE, 'MM/DD/YYYY HH:MI:SS'));
  
    -- Get the Sales BO quantity for parts in Temp Table for US, OUS. 
    gm_calc_sales_bo(cur_sales_bo);
  
    LOOP
      FETCH cur_sales_bo
        INTO v_parent_part, v_qty, v_type;
    
      IF (p_type = 'parentpart')
      THEN
        UPDATE T410_DHR_PARENT_PARTS
           SET C410_PARENT_US_SALES_BO  = DECODE(v_type,
                                                 'US',
                                                 v_qty,
                                                 C410_PARENT_US_SALES_BO),
               C410_PARENT_OUS_SALES_BO = DECODE(v_type,
                                                 'OUS',
                                                 v_qty,
                                                 C410_PARENT_OUS_SALES_BO)
         WHERE C205_DHR_PARENT_PART = v_parent_part;
      ELSE
        UPDATE T410A_DHR_CALCULATION
           SET C410A_US_SALES_BO  = DECODE(v_type,
                                           'US',
                                           v_qty,
                                           C410A_US_SALES_BO),
               C410A_OUS_SALES_BO = DECODE(v_type,
                                           'OUS',
                                           v_qty,
                                           C410A_OUS_SALES_BO)
         WHERE C205_DHR_PART_NUMBER = v_parent_part;
      END IF;
      EXIT WHEN cur_sales_bo%NOTFOUND;
    
      v_parent_part := '';
      v_qty         := '';
      v_type        := '';
    END LOOP;
  END gm_populate_parentpart;

  /* After the Parent part relevant quantity is populated in T410_DHR_PARENT_PARTS,
  *   we have to run through the list and apply the formula to determine the parent need for each part.
  *   All the parent Need for all parts are calculated and the total will be populated back in T410A_DHR_CALCULATION.
  * 
  */
  PROCEDURE gm_calc_parent_need(p_part_num IN t205_part_number.c205_part_number_id%TYPE) AS
    CURSOR parent_part IS
      SELECT C205_DHR_PARENT_PART PNT_PART, C205_DHR_PARENT_PART_QTY PNT_BOM,
             NVL(C410_PARENT_US_SALES, 0) PNT_US_SALES,
             NVL(C410_PARENT_US_SALES_BO, 0) PNT_US_SALES_BO,
             NVL(C410_PARENT_SHELF_ALLOC_IN, 0) PNT_SHELF_ALLOC,
             NVL(C410_PARENT_SHELF_QTY, 0) PNT_SHELF_QTY,
             NVL(C410_PARENT_OUS_SALES, 0) PNT_OUS_SALES,
             NVL(C410_PARENT_OUS_SALES_BO, 0) PNT_OUS_SALES_BO,
             NVL(C410_PARENT_BULK_QTY, 0) PNT_BLK_QTY,
             NVL(C410_PARENT_BULK_ALLOC_IN, 0) PNT_BLK_ALLOC,
             NVL(C410_PARENT_RW_ALLOC_IN, 0) PNT_RW_ALLOC,
             NVL(C410_PARENT_RW_QTY, 0) PNT_RW_QTY,
             NVL(C410_PARENT_TPR, 0) PNT_US_TPR,
             NVL(C410_PARENT_OUS_TPR, 0) PNT_OUS_TPR
        FROM T410_DHR_PARENT_PARTS
       WHERE C205_DHR_PART_NUMBER = p_part_num
             AND C205_DHR_PARENT_PART IS NOT NULL;
  
    v_par_shelf_qty      NUMBER := 0;
    v_par_shelf_allocin  NUMBER := 0;
    v_par_us_sales       NUMBER := 0;
    v_par_us_sales_bo    NUMBER := 0;
    v_excess_shelf       NUMBER := 0;
    v_excess_blk         NUMBER := 0;
    v_calculation        NUMBER := 0;
    v_us_sales_pnt_need  NUMBER := 0;
    v_ous_sales_pnt_need NUMBER := 0;
    v_us_blk_pnt_need    NUMBER := 0;
    v_ous_blk_pnt_need   NUMBER := 0;
    v_parent_part        T410_DHR_PARENT_PARTS.C205_DHR_PARENT_PART%TYPE;
  
    /*  Logic :
    *  -------
    *  1. Add Shelf Qty + Shelf Alloc in and reduce it with the Sales and Sales BO.
    *  2. If it result in -ve, then we need those quantity * BOM as the parent Need. If the result is +ve we have enough inventory to cover the sales need.
    *  3. Do the math (Step 1,2) for the OUS Numbers to determine what is the Parent Need for OUS.
    *  4. Add Bult Qty + Bulk Alloc In and reduce it with the TPR.
    *  5. If it result in -ve, then we need those quantity * BOM as the parent Need. If the result is +ve we have enough Bulk inventory to cover the TPR need.
    *  6.  Do the math (Step 4,5) for the OUS Numbers to determine what is the Parent Need for OUS.
    *  7. This formula will return : us_sales_pNeed, OUS_Sales_pneed, US_Blk_pneed, OUS_Blk_pneed.
    *  8. All this will be added up to determine the final quantity for us_sales_pNeed, OUS_Sales_pneed, US_Blk_pneed, OUS_Blk_pneed for all Parent Parts.
    */
  BEGIN
    FOR var_parent_part IN parent_part
    LOOP
      v_parent_part := var_parent_part.PNT_PART;
      v_calculation := (var_parent_part.PNT_SHELF_QTY +
                       var_parent_part.PNT_SHELF_ALLOC) -
                       (var_parent_part.PNT_US_SALES +
                       var_parent_part.PNT_US_SALES_BO);
    
      IF (v_calculation <= 0)
      THEN
        v_us_sales_pnt_need := v_calculation * -1 * var_parent_part.PNT_BOM;
      ELSE
        v_excess_shelf := v_calculation;
      END IF;
    
      -- OUS Sales Need
      v_calculation := (v_excess_shelf + var_parent_part.PNT_RW_QTY +
                       var_parent_part.PNT_RW_ALLOC) -
                       (var_parent_part.PNT_OUS_SALES +
                       var_parent_part.PNT_OUS_SALES_BO);
    
      IF (v_calculation <= 0)
      THEN
        v_ous_sales_pnt_need := v_calculation * -1 *
                                var_parent_part.PNT_BOM;
        v_excess_shelf       := 0;
      ELSE
        v_excess_shelf := v_calculation;
      END IF;
    
      -- US Bulk Need
      v_calculation := (var_parent_part.PNT_BLK_QTY +
                       var_parent_part.PNT_BLK_ALLOC) -
                       (var_parent_part.PNT_US_TPR);
    
      IF (v_calculation <= 0)
      THEN
        v_us_blk_pnt_need := v_calculation * -1 * var_parent_part.PNT_BOM;
      ELSE
        v_excess_blk := v_calculation;
      END IF;
    
      -- OUS Bulk Need
      v_calculation := (v_excess_shelf) - (var_parent_part.PNT_OUS_TPR);
    
      IF (v_calculation <= 0)
      THEN
        v_ous_blk_pnt_need := v_calculation * -1 * var_parent_part.PNT_BOM;
      ELSE
        v_excess_shelf := v_calculation;
      END IF;
    
      --dbms_outputPUT.PUT_LINE(v_parent_part||' US-SPN:  '||v_us_sales_pnt_need||'  OUS-SPN  '||v_ous_sales_pnt_need||'  US-B-PN  '||v_us_blk_pnt_need||'  OUS-B-PN  '||v_ous_blk_pnt_need);
      UPDATE T410A_DHR_CALCULATION
         SET C410A_US_SALES_PN_QTY  = v_us_sales_pnt_need +
                                      NVL(C410A_US_SALES_PN_QTY, 0),
             C410A_OUS_SALES_PN_QTY = v_ous_sales_pnt_need +
                                      NVL(C410A_OUS_SALES_PN_QTY, 0),
             C410A_US_BLK_PN_QTY    = v_us_blk_pnt_need +
                                      NVL(C410A_US_BLK_PN_QTY, 0),
             C410A_OUS_BLK_PN_QTY   = v_ous_blk_pnt_need +
                                      NVL(C410A_OUS_BLK_PN_QTY, 0)
       WHERE C205_DHR_PART_NUMBER = p_part_num;
    
      v_us_sales_pnt_need  := 0;
      v_ous_sales_pnt_need := 0;
      v_us_blk_pnt_need    := 0;
      v_ous_blk_pnt_need   := 0;
      v_excess_shelf       := 0;
    END LOOP;
  
  END gm_calc_parent_need;

  /* This procedure will get the Inventory numbers for the parts in Temp Table.
  * 
  */

  PROCEDURE gm_calc_inv_qty(p_inv_det OUT TYPES.CURSOR_TYPE) AS
  BEGIN
    OPEN p_inv_det FOR
    
      SELECT C205_PART_NUMBER_ID PNUM, NVL(c205_inshelf, 0) shelf_qty,
             NVL(C205_RW_AVAIL_QTY, 0) rw_qty, NVL(RMQTY, 0) rmt_qty,
             (NVL(c205_inbulk, 0) + NVL(c205_in_wip_set, 0) +
              NVL(c205_tbe_alloc, 0) + NVL(build_set_qty, 0)) blk_qty
        FROM V205_QTY_IN_STOCK;
  
  END gm_calc_inv_qty;

  /* This procedure will get the Sales numbers.
  *  The data will come from GOP (when the Sheets are locked).
  * 
  */
  PROCEDURE gm_calc_sales_qty
  (
    p_date      IN DATE,
    p_sales_det OUT TYPES.CURSOR_TYPE
  ) AS
  
  BEGIN
    OPEN p_sales_det FOR
      SELECT t4060.c205_part_number_id PNUM,
             ROUND(SUM(DECODE(TO_CHAR(c4060_period, 'MON'),
                               TO_CHAR(ADD_MONTHS(p_date, 0), 'MON'),
                               C4060_QTY,
                               0)) +
                    SUM(DECODE(TO_CHAR(c4060_period, 'MON'),
                               TO_CHAR(ADD_MONTHS(p_date, 1), 'MON'),
                               C4060_QTY * 0.5,
                               0)),
                    0) QTY, t4060.c901_level_value LVL_VAL
        FROM t4060_part_forecast_qty t4060
       WHERE t4060.c4060_period BETWEEN p_date AND
             LAST_DAY(ADD_MONTHS(p_date, 1))
             AND t4060.C901_FORECAST_TYPE IN (40020) --SALES
             AND t4060.c901_level_value IN (SELECT * FROM v_in_list)
             AND t4060.c205_part_number_id IN
             (SELECT C205_PART_NUMBER_ID FROM my_temp_part_list)
       GROUP BY t4060.c205_part_number_id, t4060.c901_level_value;
  
  END gm_calc_sales_qty;

  /*
  *  This procedure get the Total Request quantity {Back Order, BLog,RConsign,RShip}
  *  If the RQ is in Common Pool or created by Set Build team, it will be excluded from the calculation.
  */
  PROCEDURE gm_calc_pend_req_qty(p_pen_req_qty_det OUT TYPES.CURSOR_TYPE) AS
  
    v_tpr_rq_qty NUMBER;
    v_tpr_cn_qty NUMBER;
  
  BEGIN
    DELETE FROM my_temp_key_value;
  
    -- Get all Distributors and the Demand Sheet 
    INSERT INTO my_temp_key_value
      (my_temp_txn_id, my_temp_txn_key)
      SELECT DISTINCT v700.d_id,
                      DECODE(v700.divid,
                              100824,
                              'OUS_DISTRIBUTOR',
                              'US_DISTRIBUTOR')
        FROM v700_territory_mapping_detail v700
      UNION ALL
      SELECT t4020.c4020_demand_master_id,
             DECODE(t4015.c901_level_value,
                     100823,
                     'US_SHEET',
                     100800,
                     'US_SHEET',
                     100801,
                     'US_SHEET',
                     'OUS_SHEET')
        FROM t4015_global_sheet_mapping t4015, t4020_demand_master t4020
       WHERE t4020.c901_demand_type IN (40021, 40022)
             AND t4020.c4020_void_fl IS NULL
             AND t4020.c4015_global_sheet_map_id =
             t4015.c4015_global_sheet_map_id
             AND (t4015.c901_level_id, t4015.c901_level_value) IN
             (SELECT t4015.c901_level_id, t4015.c901_level_value
                    FROM t4015_global_sheet_mapping t4015
                   WHERE t4015.c901_access_type = 102623
                         OR t4015.c901_level_id IN (102580));
  
    OPEN p_pen_req_qty_det FOR
      SELECT PNUM, SUM(qty) QTY, TYPE
        FROM (
               
               -- Get the Quantity from RQ that is in Back Order Status.
               SELECT t521.c205_part_number_id PNUM,
                       SUM(NVL(t521.c521_qty, 0)) QTY,
                       DECODE(my_temp_txn_key,
                               'US_DISTRIBUTOR',
                               'US',
                               'US_SHEET',
                               'US',
                               'OUS') TYPE
                 FROM t520_request t520, t521_request_detail t521,
				 -- PC-2080: Code not merged to bitbucket (orginal code avaialble SVN PMT-11753)
                 -- Added my_temp_txt_key
               ( SELECT DISTINCT v700.d_id my_temp_txn_id,
                      DECODE(v700.divid,
                              100824,
                              'OUS_DISTRIBUTOR',
                              'US_DISTRIBUTOR') my_temp_txn_key
                    FROM v700_territory_mapping_detail v700
                  UNION ALL
                  SELECT t4020.c4020_demand_master_id my_temp_txn_id,
                         DECODE(t4015.c901_level_value  ,
                                 100823,
                                 'US_SHEET',
                                 100800,
                                 'US_SHEET',
                                 100801,
                                 'US_SHEET',
                                 'OUS_SHEET') my_temp_txn_key
                    FROM t4015_global_sheet_mapping t4015, t4020_demand_master t4020
                   WHERE t4020.c901_demand_type IN (40021, 40022)
                         AND t4020.c4020_void_fl IS NULL
                         AND t4020.c4015_global_sheet_map_id =
                         t4015.c4015_global_sheet_map_id
                         AND (t4015.c901_level_id, t4015.c901_level_value) IN
                         (SELECT t4015.c901_level_id, t4015.c901_level_value
                                FROM t4015_global_sheet_mapping t4015
                               WHERE t4015.c901_access_type = 102623
                                     OR t4015.c901_level_id IN (102580))
               ) mytemp
                WHERE t521.c205_part_number_id IN
                      (SELECT C205_PART_NUMBER_ID FROM my_temp_part_list)
                      AND t520.c520_request_id = t521.c520_request_id
                      AND t520.c901_request_source <> 50617
                      AND t520.c207_set_id IS NOT NULL
                      AND (t520.c520_required_date IS NOT NULL OR
                      t520.c901_request_source = '4000122' -- Safety Stock
                      )
                      AND t520.c520_status_fl < 40
                      AND t520.c520_void_fl IS NULL
                      AND (NVL(t520.c520_request_to, '-9999') =
                      DECODE(mytemp.my_temp_txn_key,
                                  'US_DISTRIBUTOR',
                                  my_temp_txn_id,
                                  'OUS_DISTRIBUTOR',
                                  my_temp_txn_id) OR
                      NVL(t520.c520_request_txn_id, '-9999') =
                      DECODE(mytemp.my_temp_txn_key,
                                  'US_SHEET',
                                  my_temp_txn_id,
                                  'OUS_SHEET',
                                  my_temp_txn_id))
                GROUP BY t521.c205_part_number_id, my_temp_txn_key
               
               UNION ALL
               
               -- Get the Quantity from RQ that is in Back Log, Ready to Consign Status.
               SELECT t505.c205_part_number_id PNUM,
                       SUM(NVL(t505.c505_item_qty, 0)) QTY,
                       DECODE(my_temp_txn_key,
                               'US_DISTRIBUTOR',
                               'US',
                               'US_SHEET',
                               'US',
                               'OUS') TYPE
                 FROM t520_request t520, t504_consignment t504,
                       t505_item_consignment t505, 
					   -- PC-2080: Code not merged to bitbucket (orginal code avaialble SVN PMT-11753)
                       -- Temp Fix for My temp List
                       ( SELECT DISTINCT v700.d_id my_temp_txn_id,
                      DECODE(v700.divid,
                              100824,
                              'OUS_DISTRIBUTOR',
                              'US_DISTRIBUTOR') my_temp_txn_key
                            FROM v700_territory_mapping_detail v700
                          UNION ALL
                          SELECT t4020.c4020_demand_master_id my_temp_txn_id,
                                 DECODE(t4015.c901_level_value  ,
                                         100823,
                                         'US_SHEET',
                                         100800,
                                         'US_SHEET',
                                         100801,
                                         'US_SHEET',
                                         'OUS_SHEET') my_temp_txn_key
                            FROM t4015_global_sheet_mapping t4015, t4020_demand_master t4020
                           WHERE t4020.c901_demand_type IN (40021, 40022)
                                 AND t4020.c4020_void_fl IS NULL
                                 AND t4020.c4015_global_sheet_map_id =
                                 t4015.c4015_global_sheet_map_id
                                 AND (t4015.c901_level_id, t4015.c901_level_value) IN
                                 (SELECT t4015.c901_level_id, t4015.c901_level_value
                                        FROM t4015_global_sheet_mapping t4015
                                       WHERE t4015.c901_access_type = 102623
                                             OR t4015.c901_level_id IN (102580))
                         ) mytemp
                WHERE t505.c205_part_number_id IN
                      (SELECT C205_PART_NUMBER_ID FROM my_temp_part_list)
                      AND t520.c520_request_id = t504.c520_request_id
                      AND t520.c901_request_source <> 50617
                      AND t504.c504_consignment_id = t505.c504_consignment_id
                      AND t504.c207_set_id IS NOT NULL
                      AND (t520.c520_required_date IS NOT NULL OR
                      t520.c901_request_source = '4000122' -- Safety Stock
                      )
                      AND t520.c520_status_fl < 40
                      AND t520.c520_void_fl IS NULL
                      AND (NVL(t520.c520_request_to, '-9999') =
                      DECODE(mytemp.my_temp_txn_key,
                                  'US_DISTRIBUTOR',
                                  my_temp_txn_id,
                                  'OUS_DISTRIBUTOR',
                                  my_temp_txn_id) OR
                      NVL(t520.c520_request_txn_id, '-9999') =
                      DECODE(mytemp.my_temp_txn_key,
                                  'US_SHEET',
                                  my_temp_txn_id,
                                  'OUS_SHEET',
                                  my_temp_txn_id))
                GROUP BY t505.c205_part_number_id, my_temp_txn_key
               
               UNION ALL
               --BO RQ From Set being Built which is not allocated to any Distributor or Rep. 
               SELECT t521.c205_part_number_id PNUM,
                       SUM(NVL(t521.c521_qty, 0)) QTY, 'US' TYPE
                 FROM t520_request t520, t521_request_detail t521
                WHERE t521.c205_part_number_id IN
                      (SELECT C205_PART_NUMBER_ID FROM my_temp_part_list)
                      AND t520.c520_request_id = t521.c520_request_id
                      AND t520.c901_request_source <> 50617
                      AND t520.c207_set_id IS NOT NULL
                      AND (t520.c520_required_date IS NOT NULL OR
                      t520.c901_request_source = '4000122' -- Safety Stock
                      )
                      AND t520.c520_status_fl < 40
                      AND t520.c520_void_fl IS NULL
                      AND t520.c520_request_txn_id IS NOT NULL
                      AND (NVL(t520.c520_request_txn_id, '-9999') NOT IN
                      		(SELECT my_temp_txn_id FROM my_temp_key_value)
                      	 AND NVL(t520.c520_request_to, '-9999') NOT IN
                      		(SELECT my_temp_txn_id FROM my_temp_key_value)	
                      )
                GROUP BY t521.c205_part_number_id, 'US')
       GROUP BY PNUM, TYPE;
  
  END gm_calc_pend_req_qty;

  /*  This procedure will get the Sales Back order ( US Sales BO, Loaner BO, Items from Shipped Set, ETL BO)
  * 
  */
  PROCEDURE gm_calc_sales_bo(p_sales_bo_qty OUT TYPES.CURSOR_TYPE) AS
    v_sales_backorder  NUMBER;
    v_loaner_backorder NUMBER;
    v_set_bo           NUMBER;
    v_ous_etl_bo       NUMBER;
  
  BEGIN
    OPEN p_sales_bo_qty FOR
      SELECT PNUM, SUM(QTY) QTY, TYPE
        FROM (
               -- Sales BO 
               SELECT t502.c205_part_number_id PNUM,
                       SUM(NVL(t502.c502_item_qty, 0)) QTY, 'US' TYPE
                 FROM t501_order t501, t502_item_order t502
                WHERE t501.c501_order_id = t502.c501_order_id
                      AND t501.c901_order_type = 2525
                      AND t501.c501_delete_fl IS NULL
                      AND t501.c501_void_fl IS NULL
                      AND t502.C502_VOID_FL IS NULL 
                      AND (t501.c901_ext_country_id IS NULL OR t501.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH
                      AND t501.c501_status_fl = 0
                      AND t502.c205_part_number_id IN
                      (SELECT C205_PART_NUMBER_ID FROM my_temp_part_list)
                GROUP BY t502.c205_part_number_id, 'US'
               
               UNION ALL
               -- Loaner BO Items.
               SELECT t413.c205_part_number_id PNUM,
                       NVL(SUM(NVL(t413.c413_item_qty, 0)), 0) QTY, 'US' TYPE
                 FROM T412_INHOUSE_TRANSACTIONS t412,
                       t413_inhouse_trans_items t413,
                       t504a_loaner_transaction t504a
                WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
                      AND t412.c504a_loaner_transaction_id =
                      t504a.c504a_loaner_transaction_id(+)
                      AND C412_STATUS_FL = 0
                      AND t412.c412_type = 100062
                      AND t413.C413_VOID_FL IS NULL
                      AND C412_VOID_FL IS NULL
                      AND t413.c205_part_number_id IN
                      (SELECT C205_PART_NUMBER_ID FROM my_temp_part_list)
                GROUP BY t413.c205_part_number_id, 'US'
               
               UNION ALL
               -- Item from Shipped Set ( Consigned to US, OUS Distributor) .
               SELECT T521.c205_part_number_id PNUM,
                       NVL(SUM(NVL(T521.C521_QTY, 0)), 0) QTY,
                       DECODE(v700.divid, 100824, 'OUS', 'US') TYPE
                 FROM T520_REQUEST T520, T521_REQUEST_DETAIL T521,
                       (SELECT DISTINCT v700.d_id d_id, v700.divid divid,
                                         DISTTYPEID
                           FROM v700_territory_mapping_detail v700
                          WHERE v700.divid IN (100823, 100824) --US
                                AND DECODE(v700.divid, 100824, disttypeid, 1) =
                                DECODE(v700.divid, 100824, 70103, 1)) v700
                WHERE T520.C520_REQUEST_ID = T521.C520_REQUEST_ID
                      AND T520.C520_STATUS_FL = 10
                      AND T520.C520_REQUEST_TO IS NOT NULL
                      AND T520.C520_REQUEST_TO = v700.d_id
                      AND v700.divid IN (100823, 100824)
                      AND DECODE(v700.divid, 100824, disttypeid, 1) =
                      DECODE(v700.divid, 100824, 70103, 1)
                      AND T520.C520_VOID_FL IS NULL
                      AND T520.C207_SET_ID IS NULL
                      AND T520.C520_MASTER_REQUEST_ID IN
                      (SELECT C520_REQUEST_ID
                             FROM T520_REQUEST
                            WHERE C520_STATUS_FL = 40
                                  AND C207_SET_ID IS NOT NULL
                                  AND c520_void_fl IS NULL)
                      AND T521.c205_part_number_id IN
                      (SELECT C205_PART_NUMBER_ID FROM my_temp_part_list)
                GROUP BY T521.c205_part_number_id, v700.divid
               
               UNION ALL
               
               -- OUS Sales Restock.
               SELECT T521.c205_part_number_id PNUM,
                       NVL(SUM(NVL(T521.C521_QTY, 0)), 0) QTY, 'OUS' TYPE
                 FROM T520_REQUEST T520, T521_REQUEST_DETAIL T521
                WHERE T520.C520_REQUEST_ID = T521.C520_REQUEST_ID
                      AND T520.C520_STATUS_FL = 10
                      AND C901_REQUEST_SOURCE = 4000121 -- OUS Sales Restock (ETL) 
                      AND T520.C520_REQUEST_TO IS NOT NULL
                      AND T520.C520_VOID_FL IS NULL
                      AND T520.C207_SET_ID IS NULL
                      AND T521.c205_part_number_id IN
                      (SELECT C205_PART_NUMBER_ID FROM my_temp_part_list)
                GROUP BY T521.c205_part_number_id, 'OUS')
       GROUP BY PNUM, TYPE;
  
  END gm_calc_sales_bo;

  /*
  *   This procedure get the alloc in quantity for all transaction that is configured in the Rules table.
  *   Pls refer to the Rule ID, Rule Value to determin the txn that is currently mapped to get the alloc in txn.
  *   The transaction will be added to the appropriate rule id (SHELF_PLUS, BULK_PLUS etc)
  */
  PROCEDURE gm_calc_tans_allocin_qty(p_allocin_det OUT TYPES.CURSOR_TYPE) AS
  
  BEGIN
    OPEN p_allocin_det FOR
      SELECT PNUM, SUM(qty) QTY, TYPE
        FROM (SELECT t505.c205_part_number_id PNUM,
                      NVL(t505.c505_item_qty, 0) qty, C906_RULE_ID TYPE
                 FROM t504_consignment t504, t505_item_consignment t505,
                      t906_rules t906, my_temp_part_list temp
                WHERE t504.c504_consignment_id = t505.c504_consignment_id
                      AND c504_status_fl > 1
                      AND c504_status_fl < 4
                      AND c207_set_id IS NULL
                      AND c504_void_fl IS NULL
                      AND c504_type = t906.C906_RULE_GRP_ID
                      AND t906.C906_RULE_ID IN
                      ('SHELF_PLUS', 'BULK_PLUS', 'RM_PLUS', 'RW_PLUS')
                      AND t906.C906_RULE_VALUE = 'QTY_IN_INV'
                      AND c704_account_id = '01'
                      AND t906.C906_VOID_FL IS NULL
                      AND t505.c205_part_number_id = temp.C205_PART_NUMBER_ID
               UNION ALL
               SELECT t413.c205_part_number_id PNUM,
                      NVL(t413.c413_item_qty, 0) qty, C906_RULE_ID TYPE
                 FROM t412_inhouse_transactions t412,
                      t413_inhouse_trans_items t413, t906_rules t906,
                      my_temp_part_list temp
                WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
                      AND c412_status_fl > 1
                      AND c412_status_fl < 4
                      AND c412_type = t906.C906_RULE_GRP_ID
                      AND t906.C906_RULE_ID IN
                      ('SHELF_PLUS', 'BULK_PLUS', 'RM_PLUS', 'RW_PLUS')
                      AND t906.C906_RULE_VALUE = 'QTY_IN_INV'
                      AND t413.c205_part_number_id = temp.C205_PART_NUMBER_ID
                      AND c412_void_fl IS NULL
                      AND t906.C906_VOID_FL IS NULL)
       GROUP BY PNUM, TYPE;
  END gm_calc_tans_allocin_qty;

  /*
  *   This procedure will get the Worldwide Sales and Set Need and it will populate the T410A_DHR_CALCULATION table.
  */
  PROCEDURE gm_upd_set_sales_need AS
    v_date DATE := TO_DATE('01/' || TO_CHAR(CURRENT_DATE, 'MM/YYYY'),
                           'DD/MM/YYYY');
  
    CURSOR sales_det IS
      SELECT t4060.c205_part_number_id PNUM,
             ROUND(SUM(DECODE(TO_CHAR(c4060_period, 'MON'),
                               TO_CHAR(ADD_MONTHS(v_date, 0), 'MON'),
                               C4060_QTY,
                               0)) +
                    SUM(DECODE(TO_CHAR(c4060_period, 'MON'),
                               TO_CHAR(ADD_MONTHS(v_date, 1), 'MON'),
                               C4060_QTY * 0.5,
                               0)),
                    0) QTY
        FROM t4060_part_forecast_qty t4060, T410A_DHR_CALCULATION T410A
       WHERE t4060.c4060_period BETWEEN v_date AND
             LAST_DAY(ADD_MONTHS(v_date, 1))
             AND t4060.C901_FORECAST_TYPE IN (40020) --SALES
             AND t4060.c901_level_value IN (SELECT * FROM v_in_list)
             AND T410A.c205_dhr_part_number = t4060.c205_part_number_id
       GROUP BY t4060.c205_part_number_id;
  
    CURSOR set_need IS
      SELECT t410a.c205_dhr_part_number PNUM,
             SUM(c208_set_qty * t4061.c4061_qty) QTY
        FROM t208_set_details t208, t4061_set_forecast_qty t4061,
             t410a_dhr_calculation t410a
       WHERE c208_set_qty > 0
             AND t4061.c207_set_id = t208.c207_set_id
             AND t208.c205_part_number_id = t410a.c205_dhr_part_number
             AND t4061.c4061_period BETWEEN v_date AND
             ADD_MONTHS(v_date, 1)
       GROUP BY t410a.c205_dhr_part_number
       ORDER BY t410a.c205_dhr_part_number;
  BEGIN
    FOR var_sales_det IN sales_det
    LOOP
    
      --dbms_outputput.put_line('################# var_sales_det.PNUM'||var_sales_det.PNUM||' ... var_sales_det.QTY'||var_sales_det.QTY);
      UPDATE T410A_DHR_CALCULATION
         SET C410A_SALES_NEED = var_sales_det.QTY
       WHERE c205_dhr_part_number = var_sales_det.PNUM;
    
    END LOOP;
  
    FOR var_set_need IN set_need
    LOOP
      UPDATE T410A_DHR_CALCULATION
         SET C410A_SET_NEED = var_set_need.QTY
       WHERE c205_dhr_part_number = var_set_need.PNUM;
    
    END LOOP;
  
  END gm_upd_set_sales_need;

  /*
  *   This is the final procedure that will do the actual DHR SPlit based on the data that is populated so far.
  *  Pls refer to the Logic below to understand how it works.
  *   This procedure will give the quantity to be set to : Shelf, Bulk, Raw Materials.
  */
  PROCEDURE gm_calc_dhr_split
  (
    p_part_num IN t205_part_number.c205_part_number_id%TYPE,
    p_to_shelf OUT NUMBER,
    p_to_bulk  OUT NUMBER,
    p_to_rmt   OUT NUMBER
  ) AS
    v_dhr_qty                 NUMBER := 0;
    v_shelf_qty               NUMBER := 0;
    v_shelf_alloc_in          NUMBER := 0;
    v_us_sales                NUMBER := 0;
    v_us_sales_bo             NUMBER := 0;
    v_rmt_qty                 NUMBER := 0;
    v_rmt_alloc_in            NUMBER := 0;
    v_rw_qty                  NUMBER := 0;
    v_rw_alloc_in             NUMBER := 0;
    v_ous_sales               NUMBER := 0;
    v_ous_sales_bo            NUMBER := 0;
    v_bulk_qty                NUMBER := 0;
    v_tpr_us_qty              NUMBER := 0;
    v_tpr_ous_qty             NUMBER := 0;
    v_parent_shelf_qty        NUMBER := 0;
    v_parent_shelf_alloc_in   NUMBER := 0;
    v_parent_us_sales         NUMBER := 0;
    v_parent_us_sales_bo      NUMBER := 0;
    v_parent_rw_qty           NUMBER := 0;
    v_parent_rw_alloc_in      NUMBER := 0;
    v_parent_ous_sales        NUMBER := 0;
    v_parent_ous_sales_bo     NUMBER := 0;
    v_parent_bulk_qty         NUMBER := 0;
    v_parent_tpr_us_qty       NUMBER := 0;
    v_parent_tpr_ous_qty      NUMBER := 0;
    v_calculation             NUMBER := 0;
    v_sales_need              NUMBER := 0;
    v_remaining_dhr_qty       NUMBER := 0;
    v_remaining_shelf_qty     NUMBER := 0;
    v_remaining_par_shelf_qty NUMBER := 0;
    v_remaining_rmt_shelf_qty NUMBER := 0;
    v_to_shelf                NUMBER := 0;
    v_to_bulk                 NUMBER := 0;
    v_to_rmt                  NUMBER := 0;
    v_bom_qty                 NUMBER := 0;
    v_sales_parent_need       NUMBER := 0;
    v_ous_sales_need          NUMBER := 0;
    v_temp_rem_dhr_qty        NUMBER := 0;
    v_temp_to_shelf           NUMBER := 0;
    v_temp_rmt_qty            NUMBER := 0;
    v_remaining_blk           NUMBER := 0;
    v_bulk_parent_need        NUMBER := 0;
    v_remaining_par_blk       NUMBER := 0;
    v_temp_ous_blk            NUMBER := 0;
    v_us_sales_pnt_need       NUMBER := 0;
    v_ous_sales_pnt_need      NUMBER := 0;
    v_us_blk_pnt_need         NUMBER := 0;
    v_ous_blk_pnt_need        NUMBER := 0;
    v_bulk_alloc_in           NUMBER := 0;
    v_rm_parts_cnt            NUMBER := 0;
  
  BEGIN
  
    /*  Logic :
    *  -------
    *   1. Add Shelf Qty + Shelf Alloc in and reduce it with the Sales and Sales BO.
    *  2. If it result in -ve, then we need those quantity to be sent to Shelf. If the result is +ve we have enough inventory to cover the sales need.
    *  3. Take the US Sales Parent Need and compare it with RM, if there is deficient, then that quantity should go to Raw Materials.
    *   4. Do the math (Step 1,2) for the OUS Numbers to determine how much to be sent to Shelf to cover OUS Sales Need.
    *   5. Take the OUS Sales Parent Need and compare it with RM, if there is deficient, then that quantity should go to Raw Materials.  
    *  6. Add Bult Qty + Bulk Alloc In and reduce it with the TPR.
    *  7. If it result in -ve, then we need those quantity  to be sent to Bulk. If the result is +ve we have enough Bulk inventory to cover the TPR need.
    *  8. Do the math (Step 6) for the OUS Numbers to determine what is Bulk Need for OUS.
    *  9. Any Excess DHR Quantity will be sent back to Shelf.
    *  10. This formula will return : to Shelf, To Bulk, To Rawmaterials.
    *  11. While doing any of the step, if there is no DHR quantity left, the contorl goes back .
    */
  
    SELECT NVL(C410A_DHR_QTY, 0), NVL(C410A_SHELF_QTY, 0),
           NVL(C410A_SHELF_ALLOC_IN, 0), NVL(C410A_TOT_US_SALES, 0),
           NVL(C410A_US_SALES_BO, 0), NVL(C410A_RM_QTY, 0),
           NVL(C410A_RM_ALLOC_IN, 0), NVL(C410A_RW_QTY, 0),
           NVL(C410A_RW_ALLOC_IN, 0), NVL(C410A_TOT_OUS_SALES, 0),
           NVL(C410A_OUS_SALES_BO, 0), NVL(C410A_BULK_QTY, 0),
           NVL(C410A_BLK_ALLOC_IN, 0), NVL(C410A_TPR, 0),
           NVL(C410A_OUS_TPR, 0), NVL(C410A_US_SALES_PN_QTY, 0),
           NVL(C410A_OUS_SALES_PN_QTY, 0), NVL(C410A_US_BLK_PN_QTY, 0),
           NVL(C410A_OUS_BLK_PN_QTY, 0)
    
      INTO v_dhr_qty, v_shelf_qty, v_shelf_alloc_in, v_us_sales,
           v_us_sales_bo, v_rmt_qty, v_rmt_alloc_in, v_rw_qty, v_rw_alloc_in,
           v_ous_sales, v_ous_sales_bo, v_bulk_qty, v_bulk_alloc_in,
           v_tpr_us_qty, v_tpr_ous_qty, v_us_sales_pnt_need,
           v_ous_sales_pnt_need, v_us_blk_pnt_need, v_ous_blk_pnt_need
    
      FROM T410A_DHR_CALCULATION
     WHERE C205_DHR_PART_NUMBER = p_part_num;
  
    SELECT SUM(C205_DHR_PARENT_PART_QTY)
      INTO v_bom_qty
      FROM T410_DHR_PARENT_PARTS
     WHERE C205_DHR_PART_NUMBER = p_part_num;
  
    -- *****STEP -1 US FG For DHR Part*****
    v_calculation := (v_shelf_qty + v_shelf_alloc_in) -
                     (v_us_sales + v_us_sales_bo);
    --If Sales Need is more than Shelf Qty
    IF (v_calculation < 0)
    THEN
      v_sales_need := v_calculation * -1;
    ELSE
      --If There is any excess shelf qty,assign it to variable.
      v_remaining_shelf_qty := v_calculation;
    END IF;
  
    p_to_shelf          := v_sales_need;
    v_remaining_dhr_qty := v_dhr_qty - v_sales_need;
  
    IF (v_sales_need > v_dhr_qty)
    THEN
      p_to_shelf          := v_dhr_qty;
      v_remaining_dhr_qty := 0;
    END IF;
  
    --As No DHR qty is left, none of the other calculation has to be done.
    IF (v_remaining_dhr_qty = 0)
    THEN
      RETURN;
    END IF;
  
    --dbms_outputPUT.PUT_LINE(' ** STEP-1** p_to_shelf'||p_to_shelf||' v_remaining_dhr_qty '||v_remaining_dhr_qty||' v_remaining_shelf_qty'||v_remaining_shelf_qty);
  
    --*****STEP -3 RM Calcualtion.*****
    v_calculation := (v_rmt_qty + v_rmt_alloc_in) - v_us_sales_pnt_need;
    p_to_rmt      := 0;
  
    --dbms_outputPUT.PUT_LINE(' ** STEP-3** v_calculation'||v_calculation);
  
    IF (v_calculation < 0)
    THEN
      p_to_rmt := v_calculation * -1;
    ELSE
      --If There is any excess raw mat qty,assign it to variable.
      v_remaining_rmt_shelf_qty := v_calculation;
    END IF;
  
    IF (v_remaining_dhr_qty >= p_to_rmt)
    THEN
      v_temp_rem_dhr_qty := v_remaining_dhr_qty - p_to_rmt;
    END IF;
    --dbms_outputPUT.PUT_LINE(' ** STEP-3** v_remaining_dhr_qty'||v_remaining_dhr_qty);
    IF (v_remaining_dhr_qty < p_to_rmt)
    THEN
      p_to_rmt            := v_remaining_dhr_qty;
      v_remaining_dhr_qty := 0;
      v_temp_rem_dhr_qty  := 0;
    END IF;
  
    v_remaining_dhr_qty := v_temp_rem_dhr_qty;
  
    --dbms_outputPUT.PUT_LINE(' ** STEP-3** p_to_rmt'||p_to_rmt||' v_remaining_dhr_qty '||v_remaining_dhr_qty||' v_remaining_rmt_shelf_qty '||v_remaining_rmt_shelf_qty);
  
    --As No DHR qty is left, none of the other calculation has to be done.
    IF (v_remaining_dhr_qty = 0)
    THEN
      RETURN;
    END IF;
    v_calculation := 0;
  
    -- *****STEP -4 OUS FG For DHR Part*****
    v_calculation    := (v_remaining_shelf_qty + v_rw_qty + v_rw_alloc_in) -
                        (v_ous_sales + v_ous_sales_bo);
    v_ous_sales_need := 0;
  
    IF (v_calculation < 0)
    THEN
      v_ous_sales_need := v_calculation * -1;
    ELSE
      v_remaining_shelf_qty := v_calculation;
    END IF;
  
    IF (v_remaining_dhr_qty < v_ous_sales_need)
    THEN
      p_to_shelf          := p_to_shelf + v_remaining_dhr_qty;
      v_remaining_dhr_qty := 0;
    ELSE
      p_to_shelf          := p_to_shelf + v_ous_sales_need;
      v_remaining_dhr_qty := v_remaining_dhr_qty - v_ous_sales_need;
    END IF;
  
    --As No DHR qty is left, none of the other calculation has to be done.
    IF (v_remaining_dhr_qty = 0)
    THEN
      RETURN;
    END IF;
  
    v_calculation := 0;
  
    --dbms_outputPUT.PUT_LINE(' ** STEP-4** p_to_shelf'||p_to_shelf||' v_remaining_dhr_qty '||v_remaining_dhr_qty);
  
    -- *****STEP -6 OUS RM For Parent Part*****
    v_calculation  := (v_remaining_rmt_shelf_qty) - (v_ous_sales_pnt_need);
    v_temp_rmt_qty := 0;
  
    --dbms_outputPUT.PUT_LINE(' ** STEP-6** '||v_remaining_rmt_shelf_qty|| 'v_ous_sales_pnt_need'||v_ous_sales_pnt_need);
  
    IF (v_calculation < 0)
    THEN
      v_temp_rmt_qty            := v_calculation * -1;
      v_remaining_rmt_shelf_qty := 0;
    ELSE
      v_remaining_rmt_shelf_qty := v_calculation;
    END IF;
  
    --dbms_outputPUT.PUT_LINE(' v_remaining_dhr_qty '||v_remaining_dhr_qty||' v_temp_rmt_qty '||v_temp_rmt_qty);
    IF (v_remaining_dhr_qty < v_temp_rmt_qty)
    THEN
      p_to_rmt            := p_to_rmt + v_remaining_dhr_qty;
      v_remaining_dhr_qty := 0;
    ELSE
      p_to_rmt            := p_to_rmt + v_temp_rmt_qty;
      v_remaining_dhr_qty := v_remaining_dhr_qty - v_temp_rmt_qty;
    END IF;
  
    v_calculation := 0;
    --dbms_outputPUT.PUT_LINE(' ** STEP-6** p_to_rmt'||p_to_rmt||'v_remaining_dhr_qty '||v_remaining_dhr_qty|| '..'|| v_remaining_rmt_shelf_qty || '..'||v_remaining_rmt_shelf_qty);
  
    --As No DHR qty is left, none of the other calculation has to be done.
    IF (v_remaining_dhr_qty = 0)
    THEN
      RETURN;
    END IF;
  
    -- *****STEP -7 US Bulk DHR Part*****
    v_calculation   := (v_bulk_qty + v_bulk_alloc_in) - (v_tpr_us_qty);
    p_to_bulk       := 0;
    v_remaining_blk := 0;
  
    IF (v_calculation < 0)
    THEN
      p_to_bulk := v_calculation * -1;
    ELSE
      v_remaining_blk := v_calculation;
    END IF;
  
    IF (p_to_bulk > v_remaining_dhr_qty)
    THEN
      p_to_bulk           := v_remaining_dhr_qty;
      v_remaining_dhr_qty := 0;
    ELSE
      v_remaining_dhr_qty := v_remaining_dhr_qty - p_to_bulk;
    
    END IF;
    v_calculation := 0;
  
    --dbms_outputPUT.PUT_LINE(' ** STEP-7** p_to_bulk'||p_to_bulk||'v_remaining_dhr_qty '||v_remaining_dhr_qty|| 'v_remaining_blk'|| v_remaining_blk );
  
    --As No DHR qty is left, none of the other calculation has to be done.
    IF (v_remaining_dhr_qty = 0)
    THEN
      RETURN;
    END IF;
  
    -- *****STEP -9 US RM DHR Part*****
    v_calculation := v_remaining_rmt_shelf_qty - v_us_blk_pnt_need;
  
    v_temp_rmt_qty := 0;
    IF (v_calculation < 0)
    THEN
      v_temp_rmt_qty := v_calculation * -1;
    END IF;
  
    IF (v_remaining_dhr_qty < v_temp_rmt_qty)
    THEN
      p_to_rmt            := p_to_rmt + v_remaining_dhr_qty;
      v_remaining_dhr_qty := 0;
    ELSE
      v_remaining_dhr_qty := v_remaining_dhr_qty - v_temp_rmt_qty;
      p_to_rmt            := p_to_rmt + v_temp_rmt_qty;
    END IF;
    v_calculation := 0;
  
    --dbms_outputPUT.PUT_LINE(' ** STEP-9 ** p_to_rmt'||p_to_rmt||'v_remaining_dhr_qty '||v_remaining_dhr_qty);
    --As No DHR qty is left, none of the other calculation has to be done.
    IF (v_remaining_dhr_qty = 0)
    THEN
      RETURN;
    END IF;
  
    -- *****STEP -10 OUS Bulk DHR Part*****
    v_calculation  := v_remaining_blk - v_tpr_ous_qty;
    v_temp_ous_blk := 0;
  
    IF (v_calculation < 0)
    THEN
      v_temp_ous_blk := v_calculation * -1;
    END IF;
  
    IF (v_remaining_dhr_qty < v_temp_ous_blk)
    THEN
      p_to_bulk           := p_to_bulk + v_remaining_dhr_qty;
      v_remaining_dhr_qty := 0;
    ELSE
      p_to_bulk           := p_to_bulk + v_temp_ous_blk;
      v_remaining_dhr_qty := v_remaining_dhr_qty - v_temp_ous_blk;
    END IF;
  
    --As No DHR qty is left, none of the other calculation has to be done.
    IF (v_remaining_dhr_qty = 0)
    THEN
      RETURN;
    END IF;
  
    v_calculation := 0;
    --dbms_outputPUT.PUT_LINE(' ** STEP-10 ** p_to_bulk'||p_to_bulk||'v_remaining_dhr_qty '||v_remaining_dhr_qty);
  
    -- *****STEP -12 OUS RM DHR Part*****
    v_calculation  := v_remaining_rmt_shelf_qty - v_ous_blk_pnt_need;
    v_temp_rmt_qty := 0;
  
    IF (v_calculation < 0)
    THEN
      v_temp_rmt_qty := v_calculation * -1;
    END IF;
  
    IF (v_remaining_dhr_qty < v_temp_rmt_qty)
    THEN
      p_to_rmt            := p_to_rmt + v_remaining_dhr_qty;
      v_remaining_dhr_qty := 0;
    ELSE
      v_remaining_dhr_qty := v_remaining_dhr_qty - v_temp_rmt_qty;
      p_to_rmt            := p_to_rmt + v_temp_rmt_qty;
      
      --For the parts in the rules table, the remaining qty has to be sent to Raw Materials rather than FG.
      --Hence checking the rules and sending the excess to the RM bucket and not to shelf.
      SELECT COUNT(1) INTO v_rm_parts_cnt 
      FROM T906_RULES
      WHERE C906_RULE_ID='EXCESS_RM_PARTS'
      AND C906_RULE_GRP_ID='DHR_SPLIT'
      AND C906_RULE_VALUE = p_part_num
      AND C906_VOID_FL IS NULL;
      
      IF (v_rm_parts_cnt>0)
      THEN
      	p_to_rmt            := p_to_rmt +v_remaining_dhr_qty;
      	v_remaining_dhr_qty := 0;
      END IF;
      
      p_to_shelf          := p_to_shelf + v_remaining_dhr_qty;
    END IF;
  
    --ALL CALCULATIONS OVER
    --dbms_outputPUT.PUT_LINE('p_to_rmt'||p_to_rmt||'p_to_bulk'||p_to_bulk||'p_to_shelf'||p_to_shelf);
  
  END gm_calc_dhr_split;
END gm_pkg_op_dhr_split;
/
