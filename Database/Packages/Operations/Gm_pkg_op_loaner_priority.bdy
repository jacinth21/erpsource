--@"C:\PMT\db\Packages\Operations\Gm_pkg_op_loaner_priority.bdy";
CREATE OR REPLACE
PACKAGE body Gm_pkg_op_loaner_priority
IS
--
/*******************************************************************
	* Description : Procedure to fetch the setname and current priority details
	* Author      : shanmugapriya
************************************************************************/
PROCEDURE gm_fch_priority_details(
    p_consignment_id	  IN      t504a_consignment_loaner.c504_consignment_id%TYPE,
    p_out_cursor1         OUT     Types.cursor_type
    )
    AS
       BEGIN
	       OPEN  p_out_cursor1 
	        FOR 
	          SELECT 
	             t504.c504_consignment_id consignmentId ,
                 t504.c207_set_id SETID ,
                 get_set_name(t504.c207_set_id) setName,
                 get_code_name(t504a.c504a_loaner_priority) currentPriority,
                 t504a.c504a_loaner_priority currentPriorityid
              FROM 
                 t504_consignment t504, t504a_consignment_loaner t504a 
              where 
                 t504.c504_consignment_id = t504a.c504_consignment_id
                 and t504a.c504_consignment_id =  p_consignment_id
                 and t504.c504_type IN (select c906_rule_value 
               					from t906_rules 
               					where c906_rule_id = 'PRIORITYTYP' 
               					and C906_RULE_GRP_ID = 'SHOWPRIORITY' 
               					and c906_void_fl is null)
                 and t504.c504_void_fl is null 
                 and t504a.c504a_void_fl is null;
END gm_fch_priority_details; 
/*******************************************************************
	* Description : Procedure to update the  priority details
	* Author      : shanmugapriya
************************************************************************/
PROCEDURE  gm_sav_priority(
    p_consignment_id	  IN      t504a_consignment_loaner.c504_consignment_id%TYPE,
    p_selected_priority   IN	  t504a_consignment_loaner.c504a_loaner_priority%TYPE,
    p_user_id             IN      t504a_consignment_loaner.c504a_last_updated_by%TYPE,
    p_company_id          IN	  t1900_company.c1900_company_id%TYPE,
    p_plantid             IN      t5040_plant_master.c5040_plant_id%TYPE
    )
    AS
    	  BEGIN 
	    	  UPDATE 
                      t504a_consignment_loaner t504a
                 set
                      c504a_loaner_priority = DECODE(p_selected_priority,103806,null,p_selected_priority),--NO Priority(103806)
                      C504a_user_priority = DECODE(p_selected_priority,103806,null,'Y'),--NO Priority(103806)
                      C504A_LAST_UPDATED_BY = p_user_id,
                      C504A_LAST_UPDATED_DATE = current_date
                  WHERE 
                      c504_consignment_id = p_consignment_id
                      AND   c504a_void_fl is null;
    Gm_pkg_op_loaner_priority.gm_sav_ln_priority_log(p_consignment_id,p_selected_priority,p_user_id,p_company_id,p_plantid,null,null);               
END gm_sav_priority; 
/*******************************************************************
	* Description : Procedure to save and update the  priority log
	* Author      : shanmugapriya
************************************************************************/
	PROCEDURE gm_sav_ln_priority_log(
	    p_consignment_id	         IN      t504a_consignment_loaner.c504_consignment_id%TYPE,
	    p_selected_priority          IN	     t504a_consignment_loaner.c504a_loaner_priority%TYPE,
	    p_user_id                    IN      t504a_consignment_loaner.c504a_last_updated_by%TYPE,
	    p_company_id                 IN	     t1900_company.c1900_company_id%TYPE,
	    p_plantid                    IN      t5040_plant_master.c5040_plant_id%TYPE,
	    p_product_request_detail_id  IN      t504a_loaner_priority_log.c526_product_request_detail_id%TYPE,
	    p_setid                      IN      t504a_loaner_priority_log.c207_set_id%TYPE
    )
	AS
	BEGIN
		
		UPDATE T504a_Loaner_Priority_Log
		SET C504_CONSIGNMENT_ID = p_consignment_id,
			C504A_PRIORITY = p_selected_priority,
			C504A_LAST_UPDATED_BY = p_user_id,
			C504A_LAST_UPDATED_DATE = current_date
		WHERE C207_SET_ID = p_setid
		AND C526_PRODUCT_REQUEST_DETAIL_ID = p_product_request_detail_id
		AND C504_CONSIGNMENT_ID = p_consignment_id
		AND c504a_void_fl IS NULL;
		--AND C504a_Txn_Complete_Fl IS NULL;
		
		IF (SQL%ROWCOUNT = 0)
		THEN
		    INSERT
		    INTO T504a_Loaner_Priority_Log
		        (C504A_LOANER_PRIORITY_ID , C504_CONSIGNMENT_ID, C504A_PRIORITY, C526_PRODUCT_REQUEST_DETAIL_ID, C207_SET_ID,
		        C504A_LAST_UPDATED_BY, C504A_LAST_UPDATED_DATE,C1900_COMPANY_ID ,C5040_PLANT_ID)
		    VALUES
		      ( s504a_loaner_priority_id.NEXTVAL, p_consignment_id , p_selected_priority ,p_product_request_detail_id,p_setid,
		        p_user_id, current_date, p_company_id, p_plantid);
		END IF;
    END gm_sav_ln_priority_log;
     
     
    /*************************************************************************************
	* Description : Procedure to fetch the loaner set details based on the GM-CN id
	* Author      : Arajan
	* ************************************************************************************/
	PROCEDURE gm_fch_consign_dtls(
		p_txn_id		IN		t504_consignment.c504_consignment_id%TYPE,
		p_company_id	IN		t1900_company.c1900_company_id%TYPE,
		p_plant_id		IN		t5040_plant_master.c5040_plant_id%TYPE,
		p_out_cur		OUT 	TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_out_cur
		FOR
			SELECT T504.C504_Consignment_Id Txnid,
			  T504.C207_Set_Id Setid,
			  T504.C504_Type cnType,
			  T504a.C504a_User_Priority User_Priority_Fl,
			  'LOANER' prc_type
			FROM T504_Consignment T504,
			  T504a_Consignment_Loaner T504a
			WHERE T504.C504_Consignment_Id = T504a.C504_Consignment_Id
			AND T504.C504_Consignment_Id   = p_txn_id
			AND T504a.c5040_plant_id = p_plant_id
			AND C504a_Status_Fl      IN
			  (SELECT C906_Rule_Value
			  FROM T906_Rules
			  WHERE C906_Rule_Id   = 'CHNGLNPRIORITY'
			  AND C906_Rule_Grp_Id = 'LNPRIORITY'
			  And C906_Void_Fl    Is Null
			  )
			AND T504.C504_Void_Fl         IS NULL
			AND T504a.C504a_Void_Fl       IS NULL;

	END gm_fch_consign_dtls;
	
	/*************************************************************************************
	* Description : Procedure to fetch the request details based on the request id
	* Author      : Arajan
	* ************************************************************************************/
	PROCEDURE gm_fch_request_dtls(
		p_txn_id		IN		t504_consignment.c504_consignment_id%TYPE,
		p_company_id	IN		t1900_company.c1900_company_id%TYPE,
		p_plant_id		IN		t5040_plant_master.c5040_plant_id%TYPE,
		p_out_cur		OUT 	TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_out_cur
		FOR
			SELECT T525.C525_Product_Request_Id Reqid,
			  T526.C526_Product_Request_Detail_Id Reqdetid,
			  T526.C526_Void_Fl Voidfl,
			  T526.C207_Set_Id Setid,
			  T526.C526_Status_Fl Status,
			  T526.C526_Required_Date reqdate,
			  t526.C901_Request_Type reqType
			FROM T525_Product_Request T525,
			  T526_Product_Request_Detail T526
			WHERE T525.C525_Product_Request_Id = T526.C525_Product_Request_Id
			AND T526.c5040_plant_id = p_plant_id
			AND T525.C525_Product_Request_Id   = p_txn_id;

	END gm_fch_request_dtls;
	
	/********************************************************************************************
	* Description : Function to get the set count which is in available/ Pending Putaway status
	* Author      : Arajan
	* ********************************************************************************************/
	FUNCTION get_avail_set_cnt(
		p_set_id 	IN 	t504_consignment.c207_set_id%TYPE,
		p_plant_id	IN	t5040_plant_master.c5040_plant_id%TYPE,
		p_comp_id	IN	t1900_company.c1900_company_id%TYPE
	)
	RETURN NUMBER
	IS
	v_avail_count	NUMBER;
	BEGIN
		
		SELECT COUNT(1)
			INTO v_avail_count
		FROM T504_CONSIGNMENT T504,
			T504A_CONSIGNMENT_LOANER T504A
		WHERE T504.C504_CONSIGNMENT_ID = T504A.C504_CONSIGNMENT_ID
		AND T504.C207_SET_ID           = p_set_id
		AND C504A_STATUS_FL           IN (0,58) -- Available, Pending Put away
		AND t504a.c504a_allocated_fl IS NULL  -- ( Pending putaway - not allocated )
		AND t504a.c5040_plant_id = p_plant_id
		AND c504_void_fl is null
		AND C504A_VOID_FL             IS NULL;
		  
		RETURN v_avail_count;
	END get_avail_set_cnt;
	
	/********************************************************************************************
	* Description : Function to get the request count which is in approved stauts
	* Author      : Arajan
	* ********************************************************************************************/
	FUNCTION get_pend_ship_req_cnt(
		p_set_id 		IN 	t504_consignment.c207_set_id%TYPE,
		p_plant_id		IN	t5040_plant_master.c5040_plant_id%TYPE,
		p_comp_id		IN	t1900_company.c1900_company_id%TYPE,
		p_consign_id	IN	t504_consignment.c504_consignment_id%TYPE
	)
	RETURN NUMBER
	IS
	v_ship_count	NUMBER;
	BEGIN
		SELECT count(1)
		INTO v_ship_count
		FROM T525_PRODUCT_REQUEST T525,
		  T526_PRODUCT_REQUEST_DETAIL T526
		WHERE T525.C525_Product_Request_Id = T526.C525_Product_Request_Id
		AND T526.C207_Set_Id               = p_set_id
		AND T526.C526_Status_Fl           = 10 -- Approved
		AND TRUNC(T526.C526_REQUIRED_DATE) = TRUNC(CURRENT_DATE)
		AND T526.C526_Product_Request_Detail_Id NOT IN
			    (SELECT C526_Product_Request_Detail_Id
			    FROM T504a_Loaner_Priority_Log
			    WHERE C207_Set_Id  = p_set_id
			    AND C504a_Void_Fl Is Null
          		AND C526_Product_Request_Detail_Id is not null
          		AND c504_consignment_id <> p_consign_id
          		AND C5040_Plant_Id = p_plant_id
			    )
		AND T526.C526_VOID_FL             IS NULL
		AND T525.C525_VOID_FL             IS NULL
		AND T526.C5040_PLANT_ID = p_plant_id;
		
		RETURN v_ship_count;
	END get_pend_ship_req_cnt;

	/************************************************************************************************************************
	* Description : Function to get the request count which is in approved stauts and no need to shipout on the current date
	* Author      : Arajan
	* ************************************************************************************************************************/
	FUNCTION get_req_approved_cnt(
		p_set_id 	IN 	t504_consignment.c207_set_id%TYPE,
		p_plant_id	IN	t5040_plant_master.c5040_plant_id%TYPE,
		p_comp_id	IN	t1900_company.c1900_company_id%TYPE,
		p_consign_id	IN	t504_consignment.c504_consignment_id%TYPE
	)
	RETURN NUMBER
	IS
		v_appr_cnt	NUMBER;
	BEGIN
		SELECT count(1)
		INTO v_appr_cnt
		FROM T525_PRODUCT_REQUEST T525,
		  T526_PRODUCT_REQUEST_DETAIL T526
		WHERE T525.C525_Product_Request_Id = T526.C525_Product_Request_Id
		AND T526.C207_Set_Id               = p_set_id
		AND T526.C526_Status_Fl           = 10 -- Approved
		AND TRUNC(T526.C526_REQUIRED_DATE) <> TRUNC(CURRENT_DATE)
		AND T526.C526_Product_Request_Detail_Id NOT IN
			    (SELECT C526_Product_Request_Detail_Id
			    FROM T504a_Loaner_Priority_Log
			    WHERE C207_Set_Id  = p_set_id
			    AND C504a_Void_Fl Is Null
          		AND C526_Product_Request_Detail_Id is not null
          		AND c504_consignment_id <> p_consign_id
          		AND C5040_Plant_Id = p_plant_id
			    )
		AND T526.C526_VOID_FL             IS NULL
		AND T525.C525_VOID_FL             IS NULL
		AND T526.C5040_PLANT_ID = p_plant_id;
		
		RETURN v_appr_cnt;
	END get_req_approved_cnt;
	
	/*****************************************************************************************
	* Description : Function to get the request count which is in pending approved status
	* Author      : Arajan
	* ****************************************************************************************/
	FUNCTION get_req_pend_appr_cnt(
		p_set_id 	IN 	t504_consignment.c207_set_id%TYPE,
		p_plant_id	IN	t5040_plant_master.c5040_plant_id%TYPE,
		p_comp_id	IN	t1900_company.c1900_company_id%TYPE,
		p_consign_id	IN	t504_consignment.c504_consignment_id%TYPE
	)
	RETURN NUMBER
	IS
		v_pend_appr_cnt	NUMBER;
	BEGIN
		SELECT count(1)
		INTO v_pend_appr_cnt
		FROM T525_PRODUCT_REQUEST T525,
		  T526_PRODUCT_REQUEST_DETAIL T526
		WHERE T525.C525_Product_Request_Id = T526.C525_Product_Request_Id
		AND T526.C207_Set_Id               = p_set_id
		AND T526.C526_Status_Fl           = 5 -- Approved
		AND T526.C526_Product_Request_Detail_Id NOT IN
			    (SELECT C526_Product_Request_Detail_Id
			    FROM T504a_Loaner_Priority_Log
			    WHERE C207_Set_Id  = p_set_id
			    AND C504a_Void_Fl Is Null
          		AND C526_Product_Request_Detail_Id is not null
          		AND c504_consignment_id <> p_consign_id
          		AND C5040_Plant_Id = p_plant_id
			    )
		AND T526.C526_VOID_FL             IS NULL
		AND T525.C525_VOID_FL             IS NULL
		AND T526.C5040_PLANT_ID = p_plant_id;
		
		RETURN v_pend_appr_cnt;
	END get_req_pend_appr_cnt;
	
	/*******************************************************************************
	* Description : Procedure to call the procedures to update the priority details
	* Author      : Arajan
	* *****************************************************************************/
	PROCEDURE gm_upd_loaner_priority_dtls(
		p_priority		IN		t901_code_lookup.c901_code_id%TYPE,
		p_set_id		IN		t504_consignment.c207_set_id%TYPE,
		p_user_id		IN		t504a_consignment_loaner.c504a_last_updated_by%TYPE,
		p_consign_id	IN		t504_consignment.c504_consignment_id%TYPE,
		p_company_id	IN		t1900_company.c1900_company_id%TYPE,
		p_plant_id		IN		t5040_plant_master.c5040_plant_id%TYPE,
		p_req_det_id	IN		t526_product_request_detail.c526_product_request_detail_id%TYPE DEFAULT NULL
	)
	AS
	BEGIN
		gm_upd_loaner_priority(p_priority,p_user_id,p_consign_id);
		
		IF p_req_det_id IS NULL
		THEN
			gm_upd_ln_priority_log(p_consign_id,p_priority,p_set_id,p_user_id,p_company_id,p_plant_id);
		ELSE
			gm_upd_ln_priority_log(p_consign_id,p_priority,p_req_det_id,p_set_id,p_user_id,p_company_id,p_plant_id);
		END IF;
	END gm_upd_loaner_priority_dtls;
	
	/*******************************************************************************
	* Description : Procedure to update the priority details of set
	* Author      : Arajan
	* *****************************************************************************/
	PROCEDURE gm_upd_loaner_priority(
		p_priority		IN		t901_code_lookup.c901_code_id%TYPE,
		p_user_id		IN		t504a_consignment_loaner.c504a_last_updated_by%TYPE,
		p_consign_id	IN		t504_consignment.c504_consignment_id%TYPE
	)
	AS
	BEGIN
		UPDATE t504a_consignment_loaner 
		SET c504a_loaner_priority = p_priority,
			c504a_last_updated_by = p_user_id,
			c504a_last_updated_date = current_date
		WHERE c504_consignment_id = p_consign_id
		AND c504a_user_priority IS NULL
		AND c504a_void_fl IS NULL;
	END gm_upd_loaner_priority;
	
	/******************************************************************************************
	* Description : Procedure to update the priority details of set even if user prioritize it
	* Author      : Arajan
	* *****************************************************************************************/
	PROCEDURE gm_upd_user_priority(
		p_priority		IN		t901_code_lookup.c901_code_id%TYPE,
		p_user_id		IN		t504a_consignment_loaner.c504a_last_updated_by%TYPE,
		p_consign_id	IN		t504_consignment.c504_consignment_id%TYPE
	)
	AS
	BEGIN
		UPDATE t504a_consignment_loaner 
		SET c504a_loaner_priority = p_priority,
			c504a_user_priority = NULL,
			c504a_last_updated_by = p_user_id,
			c504a_last_updated_date = CURRENT_DATE
		WHERE c504_consignment_id = p_consign_id
		AND c504a_void_fl IS NULL;
	END gm_upd_user_priority;
	
	/*************************************************************************************
	* Description : Procedure to get the request for the consignment and add in log table
	* Author      : Arajan
	* **************************************************************************************/
	PROCEDURE gm_upd_ln_priority_log(
		p_consign_id	IN		t504_consignment.c504_consignment_id%TYPE,
		p_priority		IN		t901_code_lookup.c901_code_id%TYPE,
		p_set_id		IN		t504_consignment.c207_set_id%TYPE,
		p_user_id		IN		t504a_consignment_loaner.c504a_last_updated_by%TYPE,
		p_company_id	IN		t1900_company.c1900_company_id%TYPE,
		p_plant_id		IN		t5040_plant_master.c5040_plant_id%TYPE
	)
	AS
		v_prc_name		VARCHAR2(50);
		v_exec_string	VARCHAR2(200);
		v_req_id		T526_Product_Request_Detail.C526_Product_Request_Detail_Id%TYPE;
	BEGIN
		v_prc_name := 'Gm_pkg_op_loaner_priority.gm_fch_request_id_'||p_priority;
		v_exec_string := 'BEGIN ' || v_prc_name || '(:p_set_id,:p_company_id,:p_plant_id,:v_req_id); END;';
		EXECUTE IMMEDIATE v_exec_string USING p_set_id,p_company_id,p_plant_id, out v_req_id;
		
		Gm_pkg_op_loaner_priority.gm_upd_ln_priority_log(p_consign_id,p_priority,v_req_id,p_set_id,p_user_id,p_company_id,p_plant_id);
		
	END gm_upd_ln_priority_log;
	
	/*************************************************************************************
	* Description : Procedure to save the details in log table
	* Author      : Arajan
	* **************************************************************************************/
	PROCEDURE gm_upd_ln_priority_log(
		p_consign_id	IN		T504_CONSIGNMENT.C504_CONSIGNMENT_ID%TYPE,
		p_priority		IN		t901_code_lookup.c901_code_id%TYPE,
		p_req_det_id	IN		T526_Product_Request_Detail.C526_Product_Request_Detail_Id%TYPE,
		p_set_id		IN		T504_CONSIGNMENT.C207_SET_ID%TYPE,
		p_user_id		IN		T504A_CONSIGNMENT_LOANER.c504a_last_updated_by%TYPE,
		p_company_id	IN		t1900_company.c1900_company_id%TYPE,
		p_plant_id		IN		t5040_plant_master.c5040_plant_id%TYPE
	)
	AS
	BEGIN
		Gm_pkg_op_loaner_priority.gm_sav_ln_priority_log(p_consign_id,p_priority,p_user_id,p_company_id,p_plant_id,p_req_det_id,p_set_id);
		
		-- Void all the records related to the GM-CN/request id other than the new one
		UPDATE T504a_Loaner_Priority_Log
		SET C504a_Void_Fl                  = 'Y',
		  C504a_Last_Updated_By            = p_user_id,
		  C504a_Last_Updated_Date          = CURRENT_DATE
		WHERE C207_Set_Id                  = p_set_id
		AND C5040_Plant_Id                 = p_plant_id
		AND C504a_Void_Fl IS NULL
		AND ((C504_Consignment_Id            = p_consign_id
		AND NVL(C526_Product_Request_Detail_Id,'-9999') <> NVL(p_req_det_id,'-9999'))
		OR (C504_Consignment_Id            <> p_consign_id
		AND NVL(C526_Product_Request_Detail_Id,'-9999') = NVL(p_req_det_id,'-9999')));
		
	END gm_upd_ln_priority_log;
	
	/******************************************************************************************************
	* Description : Function to get the request for Approved request (shipping date should be current date)
	* Author      : Arajan
	* *****************************************************************************************************/
	PROCEDURE gm_fch_request_id_103802(
		p_set_id		IN		t504_consignment.c207_set_id%TYPE,
		p_company_id	IN		t1900_company.c1900_company_id%TYPE,
		p_plant_id		IN		t5040_plant_master.c5040_plant_id%TYPE,
		p_req_det_id	OUT		t526_product_request_detail.c526_product_request_detail_id%TYPE
	)
	AS
	BEGIN
		BEGIN
			SELECT REQID
				INTO p_req_det_id
			FROM
			  (SELECT T526.C526_Product_Request_Detail_Id REQID
			  FROM T525_Product_Request T525,
			    T526_Product_Request_Detail T526
			  WHERE T525.C525_Product_Request_Id           = T526.C525_Product_Request_Id
			  AND T526.C526_Status_Fl                      = 10 --Approved
			  AND T526.C207_Set_Id                         = p_set_id
			  AND TRUNC(T526.C526_Required_Date) = TRUNC(CURRENT_DATE)
			  AND T525.C525_Void_Fl             IS NULL
			  AND T526.C526_Void_Fl             IS NULL
			  and t526.c5040_plant_id = p_plant_id
			  ORDER BY T526.C526_Last_Updated_Date
			  )
			WHERE ROWNUM = 1;
		EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			p_req_det_id:= NULL;
		END;
	END gm_fch_request_id_103802;
	
	/******************************************************************************************************
	* Description : Function to get the request for Approved request (shipping date should not be current date)
	* Author      : Arajan
	* *****************************************************************************************************/
	PROCEDURE gm_fch_request_id_103803(
		p_set_id		IN		t504_consignment.c207_set_id%TYPE,
		p_company_id	IN		t1900_company.c1900_company_id%TYPE,
		p_plant_id		IN		t5040_plant_master.c5040_plant_id%TYPE,
		p_req_det_id	OUT		t526_product_request_detail.c526_product_request_detail_id%TYPE
	)
	AS
	BEGIN
		BEGIN
			SELECT REQID
				INTO p_req_det_id
			FROM
			  (SELECT T526.C526_Product_Request_Detail_Id REQID
			  FROM T525_Product_Request T525,
			    T526_Product_Request_Detail T526
			  WHERE T525.C525_Product_Request_Id           = T526.C525_Product_Request_Id
			  AND T526.C526_Status_Fl                      = 10 --Approved
			  AND T526.C207_Set_Id                         = p_set_id
			  AND TRUNC(T526.C526_Required_Date) <> TRUNC(CURRENT_DATE)
			  AND T525.C525_Void_Fl             IS NULL
			  AND T526.C526_Void_Fl             IS NULL
			  and t526.c5040_plant_id = p_plant_id
			  ORDER BY T526.C526_Last_Updated_Date
			  )
			WHERE ROWNUM = 1;
		EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			p_req_det_id:= NULL;
		END;
	END gm_fch_request_id_103803;
	
	/******************************************************************************************************
	* Description : Function to get the request for Pending approval status
	* Author      : Arajan
	* *****************************************************************************************************/
	PROCEDURE gm_fch_request_id_103804(
		p_set_id		IN		t504_consignment.c207_set_id%TYPE,
		p_company_id	IN		t1900_company.c1900_company_id%TYPE,
		p_plant_id		IN		t5040_plant_master.c5040_plant_id%TYPE,
		p_req_det_id	OUT		t526_product_request_detail.c526_product_request_detail_id%TYPE
	)
	AS
	BEGIN
		BEGIN
			SELECT REQID
				INTO p_req_det_id
			FROM
			  (SELECT T526.C526_Product_Request_Detail_Id REQID
			  FROM T525_Product_Request T525,
			    T526_Product_Request_Detail T526
			  WHERE T525.C525_Product_Request_Id           = T526.C525_Product_Request_Id
			  AND T526.C526_Status_Fl                      = 5 --Pending Approval
			  AND T526.C207_Set_Id                         = p_set_id
			  AND T525.C525_Void_Fl             IS NULL
			  AND T526.C526_Void_Fl             IS NULL
			  and t526.c5040_plant_id = p_plant_id
			  ORDER BY T526.C526_Last_Updated_Date
			  )
			WHERE ROWNUM = 1;
		EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			p_req_det_id:= NULL;
		END;
	END gm_fch_request_id_103804;
	
	/******************************************************************************************************
	* Description : Procedure to get the priority of the set
	* Author      : Arajan
	* *****************************************************************************************************/
	PROCEDURE gm_fch_set_priority(
		p_consign_id		IN		t504a_consignment_loaner.c504_consignment_id%TYPE,
		p_out_priority		OUT		VARCHAR2
	)
	AS
	BEGIN
		BEGIN
			SELECT priority_str 
			INTO p_out_priority
			FROM
			  (SELECT T504a.C504a_Loaner_Priority
			    ||'|'
			    ||Get_Code_Name(T504a.C504a_Loaner_Priority)
			    ||'|'
			    ||T504alog.C526_Product_Request_Detail_Id priority_str
			  FROM T504a_Consignment_Loaner T504a,
			    T504a_Loaner_Priority_Log T504alog
			  WHERE T504a.C504_Consignment_Id = T504alog.C504_Consignment_Id(+)
			  AND T504a.C504_Consignment_Id   = p_consign_id
			  AND T504a.C504a_Status_Fl      IN
			    (SELECT C906_Rule_Value
			    FROM T906_Rules
			    WHERE C906_Rule_Id   = 'CHNGLNPRIORITY'
			    AND C906_Rule_Grp_Id = 'LNPRIORITY'
			    AND C906_Void_Fl    IS NULL
			    )
			  AND T504a.C504a_Void_Fl    IS NULL
			  AND T504alog.C504a_Void_Fl(+) IS NULL
			  ORDER BY T504alog.C504a_Last_Updated_Date DESC
			  )
			WHERE ROWNUM =1;
		EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			p_out_priority := NULL;
		END;
	END gm_fch_set_priority;
    
	/******************************************************************************************************
	* Description : Procedure to remove the priority of the set
	* Author      : Arajan
	* *****************************************************************************************************/
	PROCEDURE gm_upd_reset_priority(
		p_priority		IN		t901_code_lookup.c901_code_id%TYPE,
		p_user_id		IN		t504a_consignment_loaner.c504a_last_updated_by%TYPE,
		p_consign_id	IN		t504_consignment.c504_consignment_id%TYPE,
		p_status_fl		IN		T504a_Consignment_Loaner.C504a_Status_Fl%TYPE
	)
	AS
		v_rule_cnt				NUMBER;
	BEGIN
		SELECT COUNT(1)
	  	INTO v_rule_cnt
		FROM T906_Rules
		WHERE C906_Rule_Id   = 'REMPRIORITY'
		AND C906_Rule_Grp_Id = 'LNPRIORITY'
		AND C906_Rule_Value  = p_status_fl
		AND C906_Void_Fl    IS NULL;
		/* If the status is available/pending putaway/Pending shipping
		 * then, call the below procedure to reset the priority of the set
		 */
		IF v_rule_cnt > 0
		THEN
			Gm_pkg_op_loaner_priority.gm_upd_user_priority(null,p_user_id,p_consign_id);
			
			UPDATE T504a_Loaner_Priority_Log
			SET C504a_Void_Fl = 'Y',
				c504a_last_updated_by = p_user_id,
				c504a_last_updated_date = CURRENT_DATE
			WHERE c504_consignment_id = p_consign_id
				AND C504a_Void_Fl IS NULL;
		END IF;
	END gm_upd_reset_priority;
	
END Gm_pkg_op_loaner_priority;
/