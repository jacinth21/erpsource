create or replace
PACKAGE BODY gm_pkg_op_LabelPrint
IS
    --
/************************************************************************
    * Description : Procedure to get the Transaction details for the given Transaction ID
    *               which are controlled and not voided. It returns transactions from folowwing tables
    *               t408_dhr, t504_consignment and t412_inhouse_transactions
    *
    * Author      : Yogabalakrishnan K
************************************************************************/
    --
PROCEDURE gm_fetch_txn_details (
p_txn_id   	IN  VARCHAR2,
p_out_dtl_cur	OUT  TYPES.cursor_type
)
IS
  v_part_num    t205_part_number.c205_part_number_id%TYPE;
	v_ctrl_num    t408_dhr.c408_control_number%TYPE;
	v_dhr_id      t408_dhr.c408_dhr_id%TYPE;
	v_etch_id		VARCHAR2(10000);
BEGIN
	
	IF (instr(p_txn_id, 'DHR')>0 OR instr(p_txn_id, 'MWO')>0) 
  THEN
	  --For Regular Parts
  	BEGIN 
			SELECT c408_dhr_id, t408.c205_part_number_id, t408.c408_control_number
			INTO v_dhr_id, v_part_num, v_ctrl_num 
			FROM t408_dhr t408
			WHERE c408_dhr_id = p_txn_id
			AND c408_void_fl is null;
		EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_part_num := NULL;
				v_ctrl_num := NULL;
    END;

    v_etch_id := get_etch_data(v_part_num,v_ctrl_num);
    -- Use get_etch_data() function to get the Etch portion of UDI string 
    -- For Tissue parts
		IF v_ctrl_num IS NULL THEN
			OPEN p_out_dtl_cur FOR
				Select v_dhr_id  Transaction_Id, t205.c205_part_number_id  Part_Number, t205.c205_part_num_desc  Part_Desc,
					GET_CODE_NAME(t205.c205_product_family)  Part_Family, GET_CODE_NAME(t205.c205_product_class)  Part_Class,
					t2550.c2550_control_number  Control_Number, 
					1 Quantity, '' Transaction_Type,
					get_part_di_number(t205.c205_part_number_id) di,t2550.c2550_expiry_date exp_date, t2550.c2550_udi_hrf udi_no, t2550.C2550_udi_mrf udi_mrf, get_etch_data(t205.c205_part_number_id,t2550.c2550_control_number) etch
				FROM t205_part_number t205, t2550_part_control_number t2550
				WHERE t205.c205_part_number_id = t2550.c205_part_number_id
					AND t2550.c2550_last_updated_trans_id =  p_txn_id 
				ORDER BY Part_Number;
		ELSE
			OPEN p_out_dtl_cur FOR
				Select v_dhr_id  Transaction_Id, t205.c205_part_number_id  Part_Number, t205.c205_part_num_desc  Part_Desc,
					GET_CODE_NAME(t205.c205_product_family)  Part_Family, GET_CODE_NAME(t205.c205_product_class)  Part_Class,
					t2550.c2550_control_number  Control_Number, get_part_ctrl_dhr_qty(t2550.c205_part_number_id,t2550.c2550_control_number,v_dhr_id)  Quantity, '' Transaction_Type,
					get_part_di_number(t205.c205_part_number_id) di,t2550.c2550_expiry_date exp_date, t2550.c2550_udi_hrf udi_no, t2550.C2550_udi_mrf udi_mrf, v_etch_id etch
				FROM t205_part_number t205, t2550_part_control_number t2550
				WHERE t205.c205_part_number_id = t2550.c205_part_number_id
					AND t2550.c205_part_number_id = v_part_num 
					AND t2550.c2550_control_number = v_ctrl_num
				ORDER BY Part_Number;
		END IF;
  ELSE
		OPEN p_out_dtl_cur FOR
			SELECT t504.c504_consignment_id  Transaction_Id, t205.c205_part_number_id  Part_Number, t205.c205_part_num_desc  Part_Desc,
				GET_CODE_NAME(t205.c205_product_family)  Part_Family, GET_CODE_NAME(t205.c205_product_class)  Part_Class,
			  t505.c505_control_number  Control_Number, t505.c505_item_qty  Quantity,NVL(GET_CODE_NAME(t504.c504_type),'')  Transaction_Type, 
        get_part_di_number(t205.c205_part_number_id) di,t2550.c2550_expiry_date exp_date, t2550.c2550_udi_hrf udi_no, t2550.C2550_udi_mrf udi_mrf, get_etch_data(t205.c205_part_number_id,t505.c505_control_number) etch
			FROM t504_consignment t504, t505_item_consignment t505, t205_part_number t205, t2550_part_control_number t2550
			WHERE t504.c504_consignment_id = t505.c504_consignment_id
				AND t505.c205_part_number_id = t205.c205_part_number_id
				AND t2550.c205_part_number_id = t505.c205_part_number_id 
				AND t2550.c2550_control_number = t505.c505_control_number
				AND t504.c504_consignment_id = p_txn_id
        AND t505.c505_control_number IS NOT NULL
        AND t504.c504_void_fl IS NULL
      
			UNION ALL

			SELECT t412.c412_inhouse_trans_id  Transaction_Id, t205.c205_part_number_id  Part_Number, t205.c205_part_num_desc  Part_Desc,
				GET_CODE_NAME(t205.c205_product_family)  Part_Family, GET_CODE_NAME(t205.c205_product_class)  Part_Class,
				t413.c413_control_number  Control_Number, t413.c413_item_qty  Quantity,NVL(GET_CODE_NAME(t412.c412_type),'')  Transaction_Type,
				get_part_di_number(t205.c205_part_number_id) di,t2550.c2550_expiry_date exp_date, t2550.c2550_udi_hrf udi_no, t2550.C2550_udi_mrf udi_mrf, get_etch_data(t205.c205_part_number_id,t413.c413_control_number) etch
				FROM t412_inhouse_transactions t412,t413_inhouse_trans_items t413, t205_part_number t205, t2550_part_control_number t2550
			WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
				AND t413.c205_part_number_id = t205.c205_part_number_id
				AND t2550.c205_part_number_id = t413.c205_part_number_id 
				AND t2550.c2550_control_number = t413.c413_control_number
				AND t412.c412_inhouse_trans_id = p_txn_id
				AND t413.c413_control_number IS NOT NULL
				AND t412.c412_void_fl IS NULL
			ORDER BY Part_Number;
  End IF;

END gm_fetch_txn_details;

    --
/************************************************************************
    * Description : Procedure to get Part Details
    *
    * Author      : Thamarai Selvan S
************************************************************************/

PROCEDURE gm_fetch_part_details (
p_part_id   	IN  VARCHAR2,
p_control_num   	IN  VARCHAR2,
p_out_dtl_cur	OUT  TYPES.cursor_type
)
AS 
  v_count       NUMBER ;
	v_part_num    t205_part_number.c205_part_number_id%TYPE;
	v_ctrl_num    t408_dhr.c408_control_number%TYPE;
	v_dhr_id      t408_dhr.c408_dhr_id%TYPE;
BEGIN
	Select COUNT(1) INTO v_count
		FROM t2550_part_control_number t2550, t205_part_number t205
	WHERE  t2550.c205_part_number_id = t205.c205_part_number_id
		AND t205.c205_part_number_id = p_part_id
		AND t2550.c2550_control_number = p_control_num;

	IF v_count = 0  THEN
		GM_RAISE_APPLICATION_ERROR('-20999','232',p_part_id);
	END IF;
	-- Use get_etch_data() function to get the Etch portion of UDI string
	OPEN p_out_dtl_cur FOR
		Select '' Transaction_Id, t205.c205_part_number_id  Part_Number, t205.c205_part_num_desc  Part_Desc,
			GET_CODE_NAME(t205.c205_product_family)  Part_Family, GET_CODE_NAME(t205.c205_product_class)  Part_Class,
			t2550.c2550_control_number  Control_Number, get_part_ctrl_dhr_qty(t2550.c205_part_number_id,t2550.c2550_control_number,v_dhr_id)  Quantity, ''  Transaction_Type,
			get_part_di_number(t205.c205_part_number_id) di,t2550.c2550_expiry_date exp_date, t2550.c2550_udi_hrf udi_no, t2550.C2550_udi_mrf udi_mrf, get_etch_data(t205.c205_part_number_id,t2550.c2550_control_number) etch
		FROM t205_part_number t205, t2550_part_control_number t2550
		WHERE t205.c205_part_number_id = t2550.c205_part_number_id
			AND t2550.c205_part_number_id = p_part_id 
			AND t2550.c2550_control_number = p_control_num
		ORDER BY Part_Number;
  
END gm_fetch_part_details;

   --
/************************************************************************
    * Description : Procedure to fetch Stock master data
    *
    * Author      : Thamarai Selvan S
 ************************************************************************/

PROCEDURE gm_fetch_printer_stock (
p_out_dtl_cur	OUT  TYPES.cursor_type
)
AS 
  v_count       NUMBER ;
BEGIN
        
        OPEN p_out_dtl_cur
        FOR
        select t205l.C205L_STOCK_NAME stock_nm, 
        t205l.c205l_stock_desc stock_desc ,
        t205l.c901_stock_type stock_type,
        t205l.c205l_stock_length stock_length,
        t205l.c205l_stock_width stock_width  
        from t205l_printer_stock t205l
        where  t205l.c205l_void_fl IS NULL;
  
END gm_fetch_printer_stock;


/************************************************************************
    * Description : Void the label path When Release from LPA
    * Author      : Thamarai Selvan S
************************************************************************/

PROCEDURE gm_void_part_label_path (
  p_label_pathInputstr   IN CLOB,
  p_userid               IN t205d_part_attribute.c205d_created_by%TYPE
)
AS 
    v_count       NUMBER ;
    v_strlen   	  NUMBER          := NVL (LENGTH (p_label_pathInputstr), 0) ;
    v_string      VARCHAR2 (4000) := p_label_pathInputstr;
    v_substring   VARCHAR2 (4000);
    v_part_num    t205_part_number.c205_part_number_id%TYPE;
    v_attr_type   t205d_part_attribute.c901_attribute_type%TYPE;
    v_label_path  t205d_part_attribute.c205d_attribute_value%TYPE;
BEGIN
  WHILE INSTR (v_string, '|') <> 0
    LOOP
        v_attr_type  := NULL;
        v_label_path := NULL;
        v_substring  := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
        v_string     := SUBSTR (v_string, INSTR (v_string, '|')  + 1) ;
        v_part_num  := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
        v_substring  := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        v_attr_type := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
        v_substring  := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        v_label_path := v_substring;
        
        UPDATE t205d_part_attribute 
        SET c205d_void_fl = 'Y'
        ,c205d_last_updated_by = p_userid
        ,c205d_last_updated_date = SYSDATE
        WHERE c205_part_number_id = v_part_num 
        AND c901_attribute_type = v_attr_type
        AND C205d_attribute_value = v_label_path; -- When a particular label is selected, only that should be voided
    
        
END LOOP;         
END gm_void_part_label_path;        

/************************************************************************
    * Description : 
    * Author      : Thamarai Selvan S
    ************************************************************************/
PROCEDURE gm_sav_part_label_path (
  p_label_pathInputstr   IN CLOB,
  p_userid               IN t205d_part_attribute.c205d_created_by%TYPE
)
AS 
    v_count       NUMBER ;
    v_strlen   	  NUMBER          := NVL (LENGTH (p_label_pathInputstr), 0) ;
    v_string      VARCHAR2 (4000) := p_label_pathInputstr;
    v_substring   VARCHAR2 (4000);
    v_part_num    t205_part_number.c205_part_number_id%TYPE;
    v_attr_type   t205d_part_attribute.c901_attribute_type%TYPE;
    v_label_path  t205d_part_attribute.c205d_attribute_value%TYPE;
    v_voidfl   t205d_part_attribute.c205d_void_fl%TYPE;
BEGIN

  WHILE INSTR (v_string, '|') <> 0
    LOOP
        v_attr_type  := NULL;
        v_label_path := NULL;
        
        v_substring  := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
        v_string     := SUBSTR (v_string, INSTR (v_string, '|')  + 1) ;
        v_part_num  := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
        v_substring  := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        v_attr_type := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
        v_substring  := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        v_label_path := v_substring;
          
      UPDATE t205d_part_attribute 
         SET c205d_attribute_value = v_label_path
            , c901_attribute_type = v_attr_type
            ,c205d_last_updated_by = p_userid
            ,c205d_last_updated_date = SYSDATE    
            ,c205d_void_fl = ''
       WHERE  c205_part_number_id = v_part_num
         AND c205d_attribute_value = v_label_path;
        -- AND c901_attribute_type IN (select c901_code_id from t901_code_lookup where c901_code_grp = 'STOCK');
            
   IF SQL%rowcount = 0 THEN 
	   	 INSERT INTO t205d_part_attribute
       (
         c2051_part_attribute_id,
         c205_part_number_id, 
         c205d_attribute_value, 
         c901_attribute_type,
         c205d_created_by, 
         c205d_created_date
       ) 
       VALUES 
       (S205D_PART_ATTRIBUTE.NEXTVAL, 
       v_part_num, 
       v_label_path,
       v_attr_type,
       p_userid,
       SYSDATE
       );
    END IF;
END LOOP;         
END gm_sav_part_label_path;
/************************************************************************
    * Description : 
    *
    * Author      : Thamarai Selvan S
************************************************************************/

PROCEDURE gm_fetch_part_label_path (
p_part_id   	IN  VARCHAR2,
p_out_dtl_cur	OUT  TYPES.cursor_type
)
IS
BEGIN
	  -- get comma delimated values to store in v_in_list
	  my_context.set_my_inlist_ctx (p_part_id) ;
 OPEN p_out_dtl_cur
        FOR
            Select t205d.c205_part_number_id part_number,
            t205d.c901_attribute_type stock_type,
            t205d.c205d_attribute_value label_path,
            t205l.C205L_STOCK_NAME stock_name,
            t205l.C205L_STOCK_DESC stock_desc,
            t205l.c205l_stock_length stock_length,
            t205l.c205l_stock_width stock_width      
            FROM t205d_part_attribute t205d , t205l_printer_stock t205l
			      WHERE  t205d.c205_part_number_id IN 
	         (SELECT token FROM v_in_list)
            AND t205d.c901_attribute_type = t205l.c901_stock_type (+) -- Get all the records from attribute table for the table
            AND t205d.c205d_void_fl IS NULL
						AND t205d.c901_attribute_type IN (
							SELECT c901_code_id from t901_code_lookup where c901_code_grp = 'STOCK' -- This filters the assigned stocks with label path for the part
							UNION ALL  
							SELECT 0 from dual); --   This filters the no stocks label path for the part 

      
END gm_fetch_part_label_path;


/************************************************************************
    * Description : Function to get Exp date for the given Part and Control number
    * Author      : Thamarai Selvan S
************************************************************************/

FUNCTION get_exp_date(
		p_pnum		 IN   t5053_location_part_mapping.c205_part_number_id%TYPE,
	  p_control_num   	IN  VARCHAR2
	)
  	RETURN DATE
IS
		v_exp_dt	DATE;
  BEGIN
		select c2550_expiry_date into v_exp_dt 
    from t2550_part_control_number 
    where c205_part_number_id = p_pnum
    AND c2550_control_number=  p_control_num;
		
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
			v_exp_dt := NULL;

		RETURN 	v_exp_dt;
		
END get_exp_date;   

/*******************************************************
    * Description : Get the part DI Number
    * Author   : Thamarai Selvan
*******************************************************/
FUNCTION get_part_di_number (
        p_part_number IN t2060_di_part_map_log.C205_PART_NUMBER_ID%TYPE)
    RETURN VARCHAR2
IS
    v_di_number t2060_di_part_mapping.c2060_di_number%TYPE;
BEGIN
    --
    BEGIN
         SELECT c2060_di_number
           INTO v_di_number
           FROM t2060_di_part_mapping
          WHERE c205_part_number_id = p_part_number;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_di_number := NULL; 
    END;
    RETURN v_di_number;
END get_part_di_number;

/*******************************************************
    * Description : Get the DHR Qty for a part and control number
    * Author   : Yoga
*******************************************************/
FUNCTION get_part_ctrl_dhr_qty (
  p_part_number			IN   t205_part_number.c205_part_number_id%TYPE,
  p_control_num   		IN  VARCHAR2,
  p_dhr_id  			IN  t408_dhr.c408_dhr_id%TYPE)
  
	RETURN NUMBER
IS
  qty NUMBER;
BEGIN
  --
  BEGIN
    SELECT c408_qty_received 
      INTO qty
    FROM t408_dhr
    WHERE c205_part_number_id = p_part_number
      AND c408_control_number = p_control_num
      AND c408_dhr_id = p_dhr_id
      AND c408_void_fl is null;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    qty := NULL; 
  END;
  RETURN qty;
END get_part_ctrl_dhr_qty;

/***************************************************************
 * Description : Function to get the Etch portion of UDI string
 * Author      : Yoga
 ****************************************************************/
FUNCTION get_etch_data (
  p_part_number			IN   t205_part_number.c205_part_number_id%TYPE,
  p_control_num   		IN   t2550_part_control_number.c2550_control_number%TYPE)
  
	RETURN VARCHAR2
IS
  v_laser_mark			VARCHAR2(10000) := NULL;
  v_wo_id				t408_dhr.c402_work_order_id%TYPE;
  v_dhr_id				t408_dhr.c408_dhr_id%TYPE;
  v_vendor_dsgn			NUMBER;
  v_wo_dco				NUMBER;
BEGIN

	/*
	103420	Yes	VENDGN
	103421	No	VENDGN
	4000517	Vendor Design	DHRPAR
	103380	WO Created		DHRPAR
	103360	Yes	WOCFL
	103361	No	WOCFL
	103362	TBE	WOCFL
	*/
	--Vendor Design Check
	BEGIN
		SELECT NVL(TRIM(GET_PART_ATTRIBUTE_VALUE (p_part_number, '4000517')),'103421')
			INTO v_vendor_dsgn
		FROM DUAL;
	
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			v_vendor_dsgn := 103421; --No
	END;
	
	IF (v_vendor_dsgn = 103420)  THEN -- Vendor Design Part?
		RETURN 'LPA0001'; -- Return as *Vendor Designed*
	END IF;

	--WO DCO'ed check
	BEGIN
		SELECT NVL(TRIM(GET_PART_ATTRIBUTE_VALUE (p_part_number, '103380')),'103361')
			INTO v_wo_dco
		FROM DUAL;
	
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			v_wo_dco := 103361; -- No
	END;
 
	IF (v_wo_dco = 103361)  THEN -- WO not DCO'ed?
		RETURN 'LPA0002'; -- Return as *Missing Etch Requirement. Please contact Product Development team*
	END IF;

	-- Get the Etch data from Part number setup	
	RETURN get_part_etch_data(p_part_number, p_control_num);

END get_etch_data;

/***************************************************************
 * Description : Function to construct Etch Data based on Part setup
 * Author      : Yoga
 ****************************************************************/
FUNCTION get_part_etch_data (
	p_part_number			IN   t205_part_number.c205_part_number_id%TYPE,
	p_control_num   		IN   t2550_part_control_number.c2550_control_number%TYPE)
  
	RETURN VARCHAR2
IS
	v_udi_format		VARCHAR2(100):=NULL;
	v_laser_mark		VARCHAR2(1000):=NULL;
	v_etch_udi		VARCHAR2(100):=NULL;
	v_etch_di			VARCHAR2(100):=NULL;
	v_etch_pi			VARCHAR2(100):=NULL;
	v_exp_dt			VARCHAR2(100):=NULL;
	v_udi_di      VARCHAR2(100):=NULL;
	v_udi_pi      VARCHAR2(100):=NULL;
  
	/*
	103381	UDI Etch Required, 		103382	DI Only Etch Required, 		103383	PI Only Etch Required
	
	103363	Yes	UDIER, 		103364	No	UDIER
	103366	Yes	DIOER, 		103367	No	DIOER
	103384	Yes	PIOER, 		103385	No	PIOER
	*/

BEGIN
	-- Get the UDI format for the part. If no for mat available return null
	BEGIN
		SELECT gm_pkg_pd_partnumber.get_dhr_tab_sam_pattern(p_part_number) 
			INTO v_udi_format
		FROM DUAL;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      v_udi_format := NULL;
      
      RETURN NULL;
	END;
	
	-- Get exp date for the lot
	BEGIN
		SELECT  TO_CHAR(TO_DATE(GET_LOT_EXPIRY_DATE(p_part_number, p_control_num),get_rule_value ('DATEFMT', 'DATEFORMAT')), 'YYMMDD')
			INTO v_exp_dt
		FROM DUAL;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      v_exp_dt := NULL;
	END;

	-- Get the Etch required info
	BEGIN
    SELECT 
       NVL(max(Etch_UDI),0) Etch_UDI 
      , NVL(max(Etch_DI),0) Etch_DI
      , NVL(max(Etch_PI),0) Etch_PI
    INTO
      v_etch_udi,
      v_etch_di,
      v_etch_pi
    FROM
        (select t205d.c205_part_number_id , gm_pkg_pd_rpt_udi.get_part_di_number (t205.C205_PART_NUMBER_ID) diNumber
                     , decode(t205d.c901_attribute_type , '103381', t205d.c205d_attribute_value, null) Etch_UDI 
                     , decode(t205d.c901_attribute_type , '103382', t205d.c205d_attribute_value, null) Etch_DI 
                     , decode(t205d.c901_attribute_type , '103383',t205d.c205d_attribute_value, null) Etch_PI  
                  from t205d_part_attribute t205d, t205_part_number t205
                 WHERE t205d.c205_part_number_id = t205.c205_part_number_id
            AND t205.c205_part_number_id = p_part_number
              AND t205d.c901_attribute_type in ('103381','103382','103383') 
            AND t205d.c205d_void_fl is NULL
        )
      GROUP BY diNumber;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
      v_etch_udi := NULL;
      v_etch_di := NULL;
      v_etch_pi := NULL;
      
      RETURN NULL;
    END;
    
    
    /*
	Ex:
		part=6111.1810 controlNumber=YOGATEST
		
    	v_udi_format = (01)00889095343106(17)EXP date[YYMMDD](10)LOT    -- S Part
    	v_udi_format = (01)00849044090710(10)LOT                        -- Non S Part
		
		v_udi_di = (01)00889095343106
		v_udi_pi = (10)YOGATEST						-- Non S Part		
		v_etch_udi = (01)00889095343106(10)YOGATEST	
  
    */
    -- From UDI_Format string remove other than (01)<di>
    v_udi_di := REPLACE(REPLACE(v_udi_format,'(10)LOT',''), '(17)EXP date[YYMMDD]','');
    
     -- From UDI_Format string remove (01)<di> and form the PI
    IF (INSTR(v_udi_format,'(17)')>0) THEN
      v_udi_pi := REPLACE(v_udi_format, v_udi_di,'') ;
      
      SELECT REPLACE(v_udi_pi,'(17)EXP date[YYMMDD]',DECODE(v_exp_dt,NULL,NULL,'(17)' || v_exp_dt)) 
        INTO v_udi_pi
      FROM DUAL;
      
      --v_udi_pi := REPLACE(v_udi_pi,'(17)EXP date[YYMMDD]',DECODE(v_exp_dt,NULL,NULL,'(17)' || v_exp_dt)) ;
      v_udi_pi := REPLACE( v_udi_pi, 'LOT', p_control_num);
    ELSE
      v_udi_pi := REPLACE(REPLACE(v_udi_format, v_udi_di,''),'LOT',p_control_num) ; 
    END IF;
    
    
    -- Replace the Etch format with actual values
    IF (v_etch_udi = '103363') THEN
      v_laser_mark:= v_udi_di || v_udi_pi;
    ELSIF (v_etch_di ='103366') THEN
      v_laser_mark:= v_udi_di;
    ELSIF (v_etch_pi = '103384') THEN
      v_laser_mark:= v_udi_pi;
    END IF;
    
  	RETURN v_laser_mark;
END get_part_etch_data;

END gm_pkg_op_LabelPrint;
/