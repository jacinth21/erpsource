/* Formatted on 2010/10/22 15:36 (Formatter Plus v4.8.0) */
--@"C:\database\Packages\Operations\gm_pkg_op_loaner_extension.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_loaner_extension
IS
--
 /******************************************************************
   * Description : Procedure to fetch loaner extension summary
   ****************************************************************/
   PROCEDURE gm_op_fch_le_summary (
      p_consignmentid   IN       t504a_loaner_transaction.c504_consignment_id%TYPE,
      p_outdata         OUT      TYPES.cursor_type
   )
   AS
      v_consignmentid   t504a_loaner_transaction.c504_consignment_id%TYPE;
   BEGIN
      OPEN p_outdata
       FOR
          SELECT   t504b.c504b_loaner_extension_id loaextid,
                   t504b.c504a_loaner_transaction_id loatraid,
                   t504b.c504b_expected_dt expectdt,
                   get_code_name (t504b.c901_reason_type) reasontype,
                   t504b.c504b_comments comments,
                   get_user_name (t504b.c504b_created_by) creatby,
                   t504b.c504b_created_date creatdt
              FROM t504b_loaner_extension_detail t504b
             WHERE t504b.c504a_loaner_transaction_id IN (
                      SELECT t504a.c504a_loaner_transaction_id
                        FROM t504a_loaner_transaction t504a
                       WHERE t504a.c504_consignment_id = p_consignmentid
                         AND t504a.c504a_return_dt IS NULL)
          ORDER BY c504b_created_date;            --c504b_loaner_extension_id;
   END gm_op_fch_le_summary;

/******************************************************************
   * Description : Procedure to save loaner extension when more than once to daw/ Replenish
   ****************************************************************/
   PROCEDURE gm_op_sav_multi_le (
      p_parentltxnid        IN   t504b_loaner_extension_detail.c504a_loaner_transaction_id%TYPE,
      p_new_loaner_txn_id   IN   t504b_loaner_extension_detail.c504a_loaner_transaction_id%TYPE
   )
   AS
      CURSOR setdetail_cursor
      IS
         SELECT t504b.c504a_loaner_transaction_id, t504b.c504b_expected_dt,
                t504b.c901_reason_type, t504b.c504b_comments,
                t504b.c504b_created_by
           FROM t504b_loaner_extension_detail t504b
          WHERE t504b.c504a_loaner_transaction_id = p_parentltxnid;
   BEGIN
      FOR setdetail_val IN setdetail_cursor
      LOOP
         gm_op_sav_le_detail (p_new_loaner_txn_id,
                              setdetail_val.c504b_expected_dt,
                              setdetail_val.c901_reason_type,
                              setdetail_val.c504b_comments,
                              setdetail_val.c504b_created_by,
                              NULL,
                              NULL
                             );
      END LOOP;
   END gm_op_sav_multi_le;

/***************************************************************************************
 * Description      : This procedure is used to Save loaner extension detail
 *
 ****************************************************************************************/
   PROCEDURE gm_op_sav_le_detail (
      p_loanertxnid   IN   t504b_loaner_extension_detail.c504a_loaner_transaction_id%TYPE,
      p_expectdate    IN   t504b_loaner_extension_detail.c504b_expected_dt%TYPE,
      p_reasontype    IN   t504b_loaner_extension_detail.c901_reason_type%TYPE,
      p_comments      IN   t504b_loaner_extension_detail.c504b_comments%TYPE,     
      p_createdby     IN   t504b_loaner_extension_detail.c504b_created_by%TYPE,
      p_status		  IN   t504b_loaner_extension_detail.c901_status_fl%TYPE,
      p_req_dtl_id    IN   t504b_loaner_extension_detail.C526_PRODUCT_REQUEST_DETAIL_ID%TYPE
   )
   AS
   v_comments  t504b_loaner_extension_detail.c504b_comments%TYPE;
   BEGIN
   v_comments := REPLACE ( REPLACE (p_comments, Chr (13), ''),Chr (10), ' ');  ---This code change is for PMT-33919 Special character issue
	   UPDATE  t504b_loaner_extension_detail
          SET C901_STATUS_FL = 1902                            ---approved 
            , C504B_STATUS_CHANGED_BY = p_createdby
            , C504B_STATUS_CHANGED_DATE =CURRENT_DATE
        WHERE C526_PRODUCT_REQUEST_DETAIL_ID = p_req_dtl_id
          AND C901_STATUS_FL =1901;               ---pending approve
	
		IF (SQL%ROWCOUNT = 0)
    	THEN
	      	INSERT INTO t504b_loaner_extension_detail
	                  (c504b_loaner_extension_id, c504a_loaner_transaction_id,
	                   c504b_expected_dt, c901_reason_type, c504b_comments,
	                   c504b_created_by, c504b_created_date , c901_status_fl ,C526_PRODUCT_REQUEST_DETAIL_ID 
	                  )
	           VALUES (s504b_loaner_extension_id.NEXTVAL, p_loanertxnid,
	                   p_expectdate, p_reasontype, v_comments,           ---p_comments replaced by v_comments
	                   p_createdby, CURRENT_DATE , p_status , p_req_dtl_id
	                  );
	   END IF;
   END gm_op_sav_le_detail;

/*********************************************
* this procedure for saving loaner extension include update loaner transaction table
**********************************************/
   PROCEDURE gm_op_sav_loaner_extension (
      p_consignmentid   IN   t504a_loaner_transaction.c504_consignment_id%TYPE,
      p_extensiondate   IN   Date,
      p_reasontype      IN   t504b_loaner_extension_detail.c901_reason_type%TYPE,
      p_comments        IN   t504b_loaner_extension_detail.c504b_comments%TYPE,
      p_createdby       IN   t504b_loaner_extension_detail.c504b_created_by%TYPE,
      p_reqextfl        IN   VARCHAR2,
      p_surg_date       IN   t525_product_request.C525_SURGERY_DATE%TYPE DEFAULT NULL,
      p_approve_fl      IN   VARCHAR2 DEFAULT NULL
   )
   AS
      CURSOR requestextension_cur (
         p_consignmentid   IN   t504a_loaner_transaction.c504_consignment_id%TYPE
      )
      IS
         SELECT t504a.c504a_loaner_transaction_id ltxnid,
                t504a.c504a_expected_return_dt initrtndt,
                NVL (t526.c525_product_request_id, 0) prid,
                t504a.c504_consignment_id conid,
                t526.c526_product_request_detail_id  req_det_id 
           FROM t526_product_request_detail t526,
                t504a_loaner_transaction t504a
          WHERE t504a.c526_product_request_detail_id = t526.c526_product_request_detail_id(+)
            AND (   t526.c525_product_request_id =
                       DECODE
                          (p_reqextfl,
                           'Y', gm_pkg_op_loaner_extension.get_product_request_id
                                                              (p_consignmentid),
                           -9999
                          )
                 OR t504a.c504_consignment_id = p_consignmentid
                )
            AND t504a.c504a_return_dt IS NULL
            AND t504a.c504a_void_fl IS NULL
            AND t526.c526_void_fl IS NULL;

      v_loaner_txn_id           VARCHAR2 (20);
      v_cnt                     NUMBER;
      v_cnt_2                   NUMBER;
      v_cnt_status              NUMBER;
      v_ext_cnt                 NUMBER;
      v_count                   NUMBER;
      v_new_loaner_txn_id       VARCHAR2 (20);
      v_parent_loaner_txn_id    VARCHAR2 (20);
      v_initial_exp_return_dt   t504a_loaner_transaction.c504a_expected_return_dt%TYPE;
      v_loaner_dt               t504a_loaner_transaction.c504a_loaner_dt%TYPE;
      v_expected_return_dt      t504a_loaner_transaction.c504a_expected_return_dt%TYPE;
      v_return_dt               t504a_loaner_transaction.c504a_return_dt%TYPE;
      v_consign_to              NUMBER;
      v_consigned_to_id         t504a_loaner_transaction.c504a_consigned_to_id%TYPE;
      v_sales_rep_id            t504a_loaner_transaction.c703_sales_rep_id%TYPE;
      v_account_id              t504a_loaner_transaction.c704_account_id%TYPE;
      v_product_req_det_id      t504a_loaner_transaction.c526_product_request_detail_id%TYPE;
      v_loaner_type             t504_consignment.c504_type%TYPE;
      v_incident_id             t9200_incident.c9200_incident_id%TYPE;
      v_request_id              t525_product_request.c525_product_request_id%TYPE;
      v_req_dtl_id              VARCHAR2(20);
      v_bus_day					date_dim.bus_day_ind%TYPE;
      v_company_id  t1900_company.c1900_company_id%TYPE;
	  v_plant_id 	  t5040_plant_master.c5040_plant_id%TYPE;
	  v_Prod_req_dtl_id 		T526_Product_Request_Detail.C526_Product_Request_Detail_Id%TYPE;
	  v_status                  NUMBER;
	  v_pend_appr_fl            VARCHAR2(1);
	   v_comments  t504b_loaner_extension_detail.c504b_comments%TYPE;
	   v_status_cnt             NUMBER;
   BEGIN
      -- code to calculate the late fees
      -- doing it here before the extension as, after extension the exp return date will be changed
      
		
		SELECT get_compid_frm_cntx(), get_plantid_frm_cntx()
     	INTO v_company_id, v_plant_id
     	FROM DUAL;
     	
     	SELECT nvl(get_rule_value_by_company(p_reasontype,'LEEXTAPPR',v_company_id),'N')
     	INTO v_pend_appr_fl
     	FROM DUAL;
      BEGIN
         SELECT t504a.c504a_loaner_transaction_id, c504_type,
                c504a_expected_return_dt, c504a_consigned_to_id,
                c703_sales_rep_id,t504a.c526_product_request_detail_id
           INTO v_loaner_txn_id, v_loaner_type,
                v_expected_return_dt, v_consigned_to_id,
                v_sales_rep_id,v_req_dtl_id
           FROM t504a_loaner_transaction t504a, t504_consignment t504
          WHERE t504a.c504_consignment_id = p_consignmentid
            AND t504a.c504_consignment_id = t504.c504_consignment_id
            AND t504a.c504a_return_dt IS NULL
            AND t504a.c504a_void_fl IS NULL
            AND t504.c504_void_fl IS NULL;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            v_consigned_to_id := 0;
            v_sales_rep_id := 0;
      END;
	
     
      --Below is to block the extn, when some of th sets are not in pending return status.      
      SELECT count(t526.c526_product_request_detail_id) INTO v_cnt_status
	   FROM t526_product_request_detail t526,
	        t504a_loaner_transaction t504a
	  WHERE t504a.c526_product_request_detail_id = t526.c526_product_request_detail_id(+)
	    AND (      t526.c525_product_request_id =   DECODE (p_reqextfl,'Y', gm_pkg_op_loaner_extension.get_product_request_id(p_consignmentid), -9999 )
	            OR t504a.c504_consignment_id = p_consignmentid
	        )
	    AND t504a.c504a_return_dt IS NULL
	    AND t504a.c504a_void_fl IS NULL
	    AND t526.c526_status_fl NOT IN(30,40) --closed status
	    AND t526.c526_void_fl IS NULL;
	    
		IF v_cnt_status > 0
		THEN 
			raise_application_error (-20644, '');
		END IF;
		-- for getting the business day indicator to know whether the day is holiday or not
		IF v_loaner_type = 4127 AND p_reasontype <> 26240637  -- 4127: Product Loaner   --26240637: Replenishment
		THEN
			BEGIN
			SELECT bus_day_ind 
					INTO v_bus_day 
				FROM date_dim 
				WHERE cal_date = p_extensiondate;		
			EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				raise_application_error('-20664','');
			END;
			
			SELECT COUNT(1) INTO v_count FROM COMPANY_HOLIDAY_DIM 
			WHERE HOLIDAY_DATE = p_extensiondate 
			AND HOLIDAY_FL       = 'Y'
			AND C1900_COMPANY_ID = v_company_id;
			
			IF (v_count > 0)
			THEN
				raise_application_error('-20663','');
			END IF;
		END IF;
		
      v_request_id := gm_pkg_op_loaner.get_loaner_request_id(v_loaner_txn_id);
      /* Get the Prodcuct request detail id and pass that also to Loaner late charge package*/
      v_Prod_req_dtl_id  := gm_pkg_op_loaner.get_product_loaner_request_id(v_loaner_txn_id);
   
      IF TRUNC (CURRENT_DATE) > v_expected_return_dt AND v_loaner_type = 4127
      THEN          
          gm_pkg_op_charges.gm_sav_loaner_late_charge(CURRENT_DATE,v_request_id,v_Prod_req_dtl_id);         
      END IF;
      
      IF TRUNC (p_extensiondate) = v_expected_return_dt AND v_loaner_type = 4127
      THEN         
          RETURN;        
      END IF;
    
   IF (v_pend_appr_fl = 'Y' AND v_loaner_type = '4127') THEN
         v_status := '1901';--pending approval
    ELSE
         v_status := '1902';--approved
         v_pend_appr_fl := 'N'; -- It will not go for Pending Approval screen. 
   END IF;
   
--( p_reasontype='50321') extension only
      IF (p_reasontype = 50321)
      THEN
         FOR requestext IN requestextension_cur (p_consignmentid)
         LOOP
    
		gm_pkg_op_loaner_extn_txn.gm_op_loaner_extension_approve(requestext.ltxnid,requestext.conid,p_extensiondate,p_comments,p_surg_date,p_createdby,v_pend_appr_fl,p_approve_fl,requestext.initrtndt,p_reasontype);
         IF(v_pend_appr_fl != 'Y' OR p_approve_fl = 'Y') THEN 
             UPDATE t526_product_request_detail
                SET C526_EXTEND_COUNT = NVL(C526_EXTEND_COUNT,0) +1
                  , C526_LAST_UPDATED_BY = p_createdby
                  , C526_LAST_UPDATED_DATE = CURRENT_DATE
              WHERE C526_PRODUCT_REQUEST_DETAIL_ID =  requestext.req_det_id;
         END IF;
         END LOOP;
         -- Added this following code to extend the item consignments (IHLN) and item RA's 
	         IF v_request_id IS NOT NULL AND p_reqextfl = 'Y' 
	         THEN
	         	UPDATE t504_consignment 
				   SET c504_return_date = p_extensiondate
	 				 , c504_last_updated_by = p_createdby
	 				 , c504_last_updated_date = CURRENT_DATE
				 WHERE c901_ref_type = 4119 -- In House Loaners
				   AND c504_ref_id = v_request_id
				   AND c504_void_fl is NULL;
				   
			    UPDATE t506_returns T506
			       SET t506.c506_expected_date = p_extensiondate
			       	 , t506.c506_last_updated_by = p_createdby
	       			 , t506.c506_last_updated_date = CURRENT_DATE
			     WHERE T506.c506_rma_id IN
			                (SELECT c506_rma_id 
			               FROM t506_returns t506, t504_consignment t504
			               WHERE t504.c504_consignment_id = t506.c506_ref_id
			                AND t506.c901_ref_type = 9110 -- In House Items 
			                AND t504.c901_ref_type = 4119 -- In House Loaners
			                AND t504.c504_ref_id = v_request_id
			                AND t504.c504_void_fl IS NULL
			                AND t506.c506_void_fl IS NULL
			                AND t506.c506_status_fl = 0);
	         END IF;
         
      END IF;                 /* End: ( p_reasontype='50321') extension only*/

--(p_reasontype = 50322) Extension w/Replenish   26240637--Replenishment
      IF (p_reasontype = 50322 )
      THEN
    gm_pkg_op_loaner_extn_txn.gm_op_loaner_exten_replenish(p_consignmentid,p_extensiondate,p_extensiondate,p_comments,p_surg_date,p_createdby,v_company_id, v_plant_id,v_status,v_pend_appr_fl,p_approve_fl,p_reasontype);
        --This update occurs when approve the Loaner Extension from approval screen(p_approve_fl) OR  LEEXTAPPR - Rule Value not Equal to Y(v_pend_appr_fl)
      END IF;
   END gm_op_sav_loaner_extension;

   /******************************************************************
   * Description : Procedure to fetch loaner Request Detail
   ****************************************************************/
   PROCEDURE gm_op_fch_req_det (
      p_consignmentid   IN       t504a_loaner_transaction.c504_consignment_id%TYPE,
      p_reqextfl        IN       VARCHAR2,
      p_outdata         OUT      TYPES.cursor_type
   )
   AS
   BEGIN
      OPEN p_outdata
       FOR
          SELECT t504a.c504a_loaner_transaction_id ltxnid,
                 t504a.c504a_expected_return_dt initrtndt,
                 NVL (t526.c525_product_request_id, 0) prid,
                 t504a.c504_consignment_id conid
            FROM t526_product_request_detail t526,
                 t504a_loaner_transaction t504a
           WHERE t504a.c526_product_request_detail_id = t526.c526_product_request_detail_id(+)
             AND (   t526.c525_product_request_id =
                        DECODE
                           (p_reqextfl,
                            'Y', gm_pkg_op_loaner_extension.get_product_request_id
                                                              (p_consignmentid),
                            -9999
                           )
                  OR t504a.c504_consignment_id = p_consignmentid
                 )
             AND t504a.c504a_return_dt IS NULL
             AND t504a.c504a_void_fl IS NULL
             AND t526.c526_void_fl IS NULL;
   END gm_op_fch_req_det;

   /******************************************************************
    * Description : This function returns Product Request ID for the given CN
   ****************************************************************/
   FUNCTION get_product_request_id (
      p_conid   IN   t504a_loaner_transaction.c504_consignment_id%TYPE
   )
      RETURN VARCHAR2
   IS
      v_prod_req_id   VARCHAR2 (20);
   BEGIN
      BEGIN
         SELECT NVL (t526.c525_product_request_id, '')
           INTO v_prod_req_id
           FROM t526_product_request_detail t526,
                t504a_loaner_transaction t504a
          WHERE t504a.c504_consignment_id = p_conid
            AND t504a.c526_product_request_detail_id = t526.c526_product_request_detail_id(+)
            AND t504a.c504a_return_dt IS NULL
            AND t504a.c504a_void_fl IS NULL
            AND t526.c526_void_fl IS NULL;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            RETURN '';
      END;

      RETURN v_prod_req_id;
   END get_product_request_id;
   
  /******************************************************************************
    * Description : Procedure to fetch Loaner Extension Pending Approval Requests
    * Author : HReddi
    *****************************************************************************/
	PROCEDURE gm_op_fch_lnext_req_det(
	  p_outdata		  OUT	   TYPES.cursor_type
	)AS
	BEGIN
		 OPEN p_outdata FOR
	          SELECT t526.c526_product_request_detail_id reqdtlid
	               , t525.c525_product_request_id reqid
	               , t504a.c504_consignment_id cnid
	               , t504b.c504b_comments comments
	               , t526.c207_set_id setid
	               , get_set_name(t526.c207_set_id) setname
	               , t525.c703_sales_rep_id  repid
	               , get_rep_name(t525.c703_sales_rep_id) repname
	               , get_rep_name(t525.c703_ass_rep_id) assocrepnm
	               , GET_REP_PHONE(t525.c703_sales_rep_id) repphone
	               , get_account_name(t525.c704_account_id) acctname
	               , t504a.c504a_loaner_dt loanerdt
	               , TO_CHAR(t504a.c504a_expected_return_dt, GET_RULE_VALUE('DATEFMT','DATEFORMAT') )  oldexpdt 
	               , t504b.c504b_expected_dt  expdt  
	               , t526.c526_extend_count extcount
	               , get_system_from_setid (t526.c207_set_id) sysid
                   , get_ad_rep_id (t525.c703_sales_rep_id) adid
	   			FROM t525_product_request t525,t526_product_request_detail t526,t504a_loaner_transaction t504a,t504b_loaner_extension_detail t504b
			   WHERE t525.c525_product_request_id        = t526.c525_product_request_id
			     AND t526.c526_product_request_detail_id = t504a.c526_product_request_detail_id
			     AND t504a.c526_product_request_detail_id = t504b.c526_product_request_detail_id
			     AND t504b.c901_status_fl                = 1901 ----pending approval
			     AND t504a.c504a_return_dt              IS NULL
			     AND t526.c526_void_fl                  IS NULL
			     AND t525.c525_void_fl                  IS NULL
			     AND t504a.c504a_void_fl                IS NULL ;		
	END gm_op_fch_lnext_req_det;
	
	/************************************************************************
	* Description : Procedure to save the Approval Loaner Extension Request
	* Author      : hreddi
	************************************************************************/
	PROCEDURE gm_op_sav_lnext_req_det(
	       p_req_id      IN    t525_product_request.c525_product_request_id%TYPE,
           p_req_det_id  IN    t526_product_request_detail.c526_product_request_detail_id%TYPE,
           p_exp_ret_dt	 IN	   t504a_loaner_transaction.c504a_expected_return_dt%TYPE,
           p_approv_cmts IN    t504a_loaner_transaction.c504a_appr_rej_comments%TYPE,
           p_user_id     IN    t2656_audit_entry.c2656_created_by%TYPE,
           p_surg_date   IN    t504a_loaner_transaction.c504a_surgery_date%TYPE,
           p_approve_fl  IN    VARCHAR2,
           p_out_err_msg OUT   CLOB)
	AS
    v_request_dtl_id 	t2656_audit_entry.c2656_audit_entry_id%TYPE;
    v_count  			NUMBER;
    v_date_fmt 		    VARCHAR2(40);
    v_exp_ret_dt        t504a_loaner_transaction.c504a_expected_return_dt%TYPE;
    v_out_err_msg       CLOB;
   
       CURSOR request_detais
		IS
		 	SELECT t504a.c504_consignment_id CNID , t504a.c504a_expected_return_dt expdt
              FROM t504a_loaner_transaction t504a
             WHERE t504a.c526_product_request_detail_id  IN (p_req_det_id) 
               AND t504a.c901_status = 1901  --Pending Approval
               AND t504a.c504a_return_dt IS NULL
               AND t504a.c504a_void_fl IS NULL;   
	BEGIN
		SELECT get_compdtfmt_frm_cntx() 
		  INTO v_date_fmt
		  FROM DUAL;

		FOR ApproveReqCur IN request_detais
			LOOP	
				BEGIN
				gm_pkg_op_loaner_extension.gm_op_sav_loaner_extension(ApproveReqCur.CNID,p_exp_ret_dt,'50321',p_approv_cmts,p_user_id,'N',p_surg_date,p_approve_fl);
				  --update FGLE Transactions 
				gm_pkg_op_loaner_extension.gm_op_update_fgle (p_req_det_id,NULL,p_user_id);
				EXCEPTION WHEN OTHERS THEN
					v_out_err_msg := 'Error Occured When Approve Loaner Extension Request. '||p_req_det_id;	
				END;
		END LOOP;	
		
		IF v_out_err_msg IS NOT NULL THEN 
    		p_out_err_msg := v_out_err_msg;
        ELSE
    	    p_out_err_msg := 'Y';
   		 END IF;		 
	END gm_op_sav_lnext_req_det;
	
	/************************************************************
	* Description : Procedure to Reject Loaner Extension Request
	* Author      : HReddi
	**************************************************************/
	PROCEDURE gm_op_sav_loaner_ext_rejt(
	    p_req_id        IN    t525_product_request.c525_product_request_id%TYPE,
        p_req_det_id    IN    t526_product_request_detail.c526_product_request_detail_id%TYPE,
        p_rej_cmts      IN    t504a_loaner_transaction.c504a_appr_rej_comments%TYPE,
        p_user_id       IN     t2656_audit_entry.c2656_created_by%TYPE,
        p_out_err_msg   OUT   CLOB)
	AS	
		v_out_err_msg CLOB;
	BEGIN
						BEGIN
						--Update status as rejected
				        UPDATE t504a_loaner_transaction 
				           SET c901_status = 1903 ---rejected
				             , c504a_appr_rej_comments = p_rej_cmts
				             , c504a_status_changed_by = p_user_id
				             , c504a_ext_appr_rej_dt = CURRENT_DATE
				         WHERE c526_product_request_detail_id = p_req_det_id
				           AND c901_status = 1901;
				          --Void FGLE Transactions 
				         gm_pkg_op_loaner_extension.gm_op_update_fgle (p_req_det_id,1903,p_user_id);
				         
						EXCEPTION WHEN OTHERS THEN
							v_out_err_msg := 'Error Occured When Reject Loaner Extension Request. '||p_req_det_id;	
						END;
					IF v_out_err_msg IS NOT NULL THEN 
    					p_out_err_msg := v_out_err_msg;
    				ELSE
    					p_out_err_msg := 'Y';
   		 			END IF;
	END gm_op_sav_loaner_ext_rejt;
	
	/*************************************************************************************
	* This procedure is to send mail while Rejecting the Loaner Extension Request Details
	* Author : HReddi
	***************************************************************************************/
	PROCEDURE gm_op_loaner_ext_rejt_email (
		p_str			IN		 CLOB
	  , p_addr			IN		 VARCHAR2
	  , p_user_id 		IN		 t106_address.c106_created_by%TYPE
	)
	AS	
		v_usrmail		VARCHAR2(200);
		v_to_mail		VARCHAR2(2000);
		v_mail_subject  VARCHAR2 (1000);
		v_table_str		VARCHAR2 (3000);
		v_mail_body     CLOB;
		v_inactivate_dt	VARCHAR2(20);
		v_rep_email 	VARCHAR2(200);
	BEGIN		
		v_usrmail := get_user_emailid (p_user_id);
		v_rep_email := get_rep_emailid(p_addr);
		v_to_mail 	:= v_rep_email || ',' || v_usrmail;
		v_inactivate_dt := to_char(sysdate,NVL(GET_RULE_VALUE('DATEFMT','DATEFORMAT'),'MM/DD/YYYY'));
		v_mail_subject  := 'Loaner extension rejection detail';		
		--below string is to crete the table header
		 v_table_str   := '<TABLE style="border: 1px solid  #676767" cellspacing = "0" cellpadding="3">'
                        || '<TR><TD width=100 style="background-color: #BDBDBD"><b>Req ID</b></TD><TD width=100 style="background-color: #BDBDBD"><b>Set ID</b></TD>'
                        || '<TD width=250 style="background-color: #BDBDBD"><b>Description</b></TD><TD width=100 style="background-color: #BDBDBD"><b>Expected Return Date</b></TD>'
                        || '<TD width=130 style="background-color: #BDBDBD"><b>Rejected By</b></TD></TR>';                        
		v_mail_body  :=
        '<style>TD{ FONT-SIZE: 11px; COLOR: #000000; FONT-FAMILY: verdana, arial, sans-serif;}</style><font face=arial size="2">';        
		v_mail_body := v_mail_body||'Following Loaner Extension Request(s) are rejected,<br><br>'
						||v_table_str||p_str;						
		--Call the common procedure to send mail		
		gm_com_send_html_email(v_to_mail, v_mail_subject,'test', v_mail_body);-- Sending mail		
	END gm_op_loaner_ext_rejt_email;
	
	/******************************************************************************************
	* This procedure is to fetch the Loaner Extn Detail based on the Product Request Detail ID
	* Author : HReddi
	*******************************************************************************************/
	PROCEDURE gm_op_fch_lnext_req_requests (
		p_str				IN	   CLOB,
		p_extnDetails		OUT	   TYPES.cursor_type,
		p_newExtnDetails	OUT	   TYPES.cursor_type
	)AS
	BEGIN
		my_context.set_my_inlist_ctx (p_str);
		OPEN p_extnDetails FOR
	         SELECT t526.c526_product_request_detail_id REQDTLID 
	              , t526.C525_PRODUCT_REQUEST_ID reqid
	              , t526.c207_set_id setid
	              , t504a.c504a_etch_id etchid
	              , t504b.c504a_expected_return_dt exprtdate
	              , t504a.c504_consignment_id consignid
	              , NVL(gm_pkg_op_loaner_extension.get_parent_lnrextn_cnt(t526.c525_product_request_id ),0) parentextncnt
	              , t504a.c504a_loaner_dt loandt
	          FROM t526_product_request_detail t526,  t504a_loaner_transaction t504b , t504a_consignment_loaner t504a   
	         WHERE t526.c526_product_request_detail_id  IN (SELECT token reqdtlid
										                      FROM v_in_list WHERE token IS NOT NULL)
	    	   AND t526.c526_product_request_detail_id = t504b.c526_product_request_detail_id  
	           AND t504b.c504_consignment_id = t504a.c504_consignment_id    
	           AND t526.C526_VOID_FL is null  
	           AND t504b.c504a_return_dt IS NULL
	           AND t504b.c504a_void_fl IS NULL 
	           AND t504a.c504a_void_fl IS NULL
	      ORDER BY t526.c526_product_request_detail_id;	      
	      gm_op_fch_req_new_exprtdate (p_str, p_newExtnDetails);
	END gm_op_fch_lnext_req_requests;
	
	/*************************************************************************************
	* This procedure is to fetch the New Extn return Date based on the day Extn Count
	* Author : HReddi
	***************************************************************************************/
	PROCEDURE gm_op_fch_req_new_exprtdate (
		p_input_str			IN	   CLOB,
		p_newExtnDetails	OUT	   TYPES.cursor_type
	)AS
		v_req_dtl_id 	t526_product_request_detail.c526_product_request_detail_id%TYPE;
		v_exp_date		t504a_loaner_transaction.c504a_expected_return_dt%TYPE;
		v_new_exprtdate t504a_loaner_transaction.c504a_expected_return_dt%TYPE;
		v_date_fmt VARCHAR2(20);
		
		CURSOR extend_day_details
		IS			   
		  SELECT C901_CODE_ID CODEID
		       , C901_CODE_NM CODENM
		       , C902_CODE_NM_ALT CODENMALT
  			   , C901_CONTROL_TYPE CONTROLTYP
  			   , C901_CODE_SEQ_NO CODESEQNO
   			FROM T901_CODE_LOOKUP
  		   WHERE C901_CODE_GRP IN ('EXTDAY')
   			 AND C901_ACTIVE_FL = '1'
		ORDER BY C901_CODE_SEQ_NO ;
		
		CURSOR extend_req_details
		IS			   
		  SELECT t526.c526_product_request_detail_id REQDTLID
		       , t504b.c504a_expected_return_dt exprtdate
	        FROM t526_product_request_detail t526,  t504a_loaner_transaction t504b  
	       WHERE t526.c526_product_request_detail_id  IN (SELECT token reqdtlid
										                      FROM v_in_list)
	         AND t526.c526_product_request_detail_id = t504b.c526_product_request_detail_id  
	         AND t526.C526_VOID_FL is null  
	         AND t504b.c504a_return_dt IS NULL
	         AND t504b.c504a_void_fl IS NULL  
        ORDER BY t526.c526_product_request_detail_id ;
		
	BEGIN
		my_context.set_my_inlist_ctx (p_input_str);
		DELETE FROM MY_TEMP_KEY_VALUE;
		
		--SELECT NVL(get_rule_value('DATEFMT','DATEFORMAT'),'MM/dd/yyyy') INTO v_rule_date_fmt FROM DUAL;
		SELECT get_compdtfmt_frm_cntx() INTO v_date_fmt FROM DUAL;
		
		FOR loanExtn_Details IN extend_req_details
			LOOP
				v_req_dtl_id := loanExtn_Details.REQDTLID;
				v_exp_date   := loanExtn_Details.exprtdate;
					FOR extnDay_Details IN extend_day_details
						LOOP
							v_new_exprtdate := get_new_expected_return_dt(v_exp_date, extnDay_Details.CODENMALT);
							INSERT INTO MY_TEMP_KEY_VALUE VALUES(v_req_dtl_id,extnDay_Details.CODENMALT,to_char(v_new_exprtdate,v_date_fmt));
					END LOOP;
			END LOOP;
		OPEN p_newExtnDetails FOR
			SELECT my_temp_txn_id reqdtlID
			     , my_temp_txn_key dayextend
			     , my_temp_txn_value newextnDate 
			  FROM my_temp_key_value;
			  
	END gm_op_fch_req_new_exprtdate;
	
	/***************************************************************
	* This procedure is used to Extend the Loaner Extension Request
	* Author : HReddi
	****************************************************************/
	PROCEDURE gm_op_sav_loaner_ext_only(
		p_input_str		IN		CLOB,
		p_user_id		IN 		t106_address.c106_created_by%TYPE,
		p_commnets      IN 		VARCHAR2,
        p_out_consignids     OUT     VARCHAR2
	)AS
		v_strlen	    NUMBER := NVL (LENGTH (p_input_str), 0);
		v_string	    VARCHAR2 (30000) := p_input_str;
		v_substring     VARCHAR2 (1000);
		v_requestid	    t526_product_request_detail.c525_product_request_id%TYPE;
		v_reqDtlID		t526_product_request_detail.c526_product_request_detail_id%TYPE;
		v_consignID	    t504a_consignment_loaner.c504_consignment_id%TYPE;
		v_newExtnDate	VARCHAR2(40);
		v_oldExpRtnDate	VARCHAR2(40);
		v_wholeExtnFl 	VARCHAR2(5);
		v_extn_count    NUMBER;
		v_temp_reqid 	t526_product_request_detail.c525_product_request_id%TYPE;
		v_temp_fl 		VARCHAR2(5);
		v_consign_Ids    CLOB;
		v_consign_date   CLOB;
		v_consign_WExtnFl   VARCHAR2(1000);
		v_repids_str    CLOB;
		v_repid         t504a_loaner_transaction.c703_sales_rep_id%TYPE; 
		v_cnt           NUMBER;
		v_surgery_dt    VARCHAR2(40);
		v_date_fmt 		VARCHAR2(40);
		v_company_id    VARCHAR2(40);
	BEGIN		
	--getting date format from rule table
		--SELECT NVL(GET_RULE_VALUE('DATEFMT', 'DATEFORMAT'),'mm/dd/yyyy') 
		SELECT get_compdtfmt_frm_cntx(), get_compid_frm_cntx()
		  INTO v_date_fmt,v_company_id
		  FROM DUAL;
  	
		WHILE INSTR(v_string,'|') <> 0 
			LOOP
		    	v_substring 	:= SUBSTR(v_string,1,INSTR(v_string,'|')-1);
		    	v_string    	:= SUBSTR(v_string,INSTR(v_string,'|')+1);
				v_requestid		:= NULL;
				v_reqDtlID		:= NULL;
				v_consignID		:= NULL;	
				v_newExtnDate	:= NULL;	
				v_wholeExtnFl	:= NULL;
				v_requestid		:= SUBSTR(v_substring,1,INSTR(v_substring,'^')-1);
				v_substring 	:= SUBSTR(v_substring,INSTR(v_substring,'^')+1);			  
				v_reqDtlID		:= SUBSTR(v_substring,1,INSTR(v_substring,'^')-1);
				v_substring 	:= SUBSTR(v_substring,INSTR(v_substring,'^')+1);	  			  
				v_consignID		:= SUBSTR(v_substring,1,INSTR(v_substring,'^')-1);
				v_substring 	:= SUBSTR(v_substring,INSTR(v_substring,'^')+1);	  			  
				v_newExtnDate	:= SUBSTR(v_substring,1,INSTR(v_substring,'^')-1);
				v_substring 	:= SUBSTR(v_substring,INSTR(v_substring,'^')+1);
				v_wholeExtnFl	:= SUBSTR(v_substring,1,INSTR(v_substring,'^')-1);	
				v_substring 	:= SUBSTR(v_substring,INSTR(v_substring,'^')+1);		  
				v_surgery_dt 	:= v_substring;
			
	
		   SELECT NVL(c526_extend_count, 0)
			 INTO v_extn_count 
			 FROM t526_product_request_detail
			WHERE c526_product_request_detail_id = v_reqDtlID
			  AND c526_void_fl IS NULL;
			  
			SELECT to_char(c504a_expected_return_dt, v_date_fmt), t504a.c703_sales_rep_id 
			  INTO v_oldExpRtnDate, v_repid
			  FROM t504a_loaner_transaction t504a              
			 WHERE t504a.c526_product_request_detail_id = v_reqDtlID
			   AND t504a.c504a_return_dt  IS NULL
			   AND t504a.c504a_void_fl    IS NULL ;
			     
			--IF NVL(v_temp_reqid,'0') <> v_requestid AND NVL(v_temp_fl,'N') <> 'Y' THEN
			/*Commenting the below Count condition ,first time Loaner Extension request should go to pending approval status in case managem*/	
				/*IF 	v_extn_count = 0 THEN
					gm_pkg_op_loaner_extension.gm_op_sav_loaner_extension(v_consignID,to_date(v_newExtnDate, NVL(GET_RULE_VALUE('DATEFMT', 'DATEFORMAT'),'mm/dd/yyyy')),50321,p_commnets,p_user_id,v_wholeExtnFl);  -- 50321 Extension Only
				    v_consign_Ids  := v_consign_Ids  || v_consignID   ||',' ;
				    v_consign_date := v_consign_date || v_oldExpRtnDate ||',';
				    v_repids_str   := v_repids_str || v_repid ||',';
				    v_consign_WExtnFl :=v_consign_WExtnFl || v_wholeExtnFl ||',';
				ELSIF v_extn_count > 0 THEN*/
				    --below is DB validation needed, when a request is in pending approval status and trying to extend from device.
				        SELECT COUNT(1) v_cnt
						  INTO v_cnt
						  FROM t504a_loaner_transaction t504a 
						 WHERE t504a.c526_product_request_detail_id = v_reqDtlID 
						  AND t504a.c901_status = 1901;
						  
					   IF v_cnt > 0 THEN
					     raise_application_error (-20649, '');
					   END IF;
					  --Commenting this line to avoid the entry of t504b table for the PMT-40150
					-- gm_pkg_op_loaner_extension.gm_op_sav_le_only_detail(v_reqDtlID,to_date(v_newExtnDate, NVL(GET_RULE_VALUE('DATEFMT', 'DATEFORMAT'),'mm/dd/yyyy')),50321,1901,p_commnets,p_user_id);  -- 1901 Pending Approval
				/*END IF;*/
			--END IF;
		
		  /*-- Setting the whole Request Flag value to the Temp Variable
			IF v_wholeExtnFl = 'Y' THEN 
				v_temp_fl := v_wholeExtnFl;
				-- Setting the Request ID to the Temp request Variable
				v_temp_reqid := v_requestid;				
			END IF;*/
			--to update the requested details for the PMT-40150
			
			gm_pkg_op_loaner_extension.gm_op_sav_loaner_extension(v_consignID,to_date(v_newExtnDate,v_date_fmt),50321,p_commnets,p_user_id,v_wholeExtnFl,to_date(v_surgery_dt, v_date_fmt));  -- 50321 Extension Only
			
			insert into t902_log(c902_log_id,c902_ref_id,c902_comments,c902_type,c902_created_by,c902_created_date,c1900_company_id)
			values(s507_log.NEXTVAL,v_consignID,p_commnets || '~' ||v_newExtnDate,26240803,p_user_id,current_date,v_company_id);  --26240803 GPLOG
			
			END LOOP;
			
				
			IF v_consign_Ids IS NULL AND v_consign_date IS NULL AND v_consign_WExtnFl IS NULL 
			THEN
				p_out_consignids := NULL;
			ELSE 
				p_out_consignids := v_consign_Ids ||'@'|| v_consign_date ||'@' || v_consign_WExtnFl ||'@' || v_repids_str;
			END IF;

	END gm_op_sav_loaner_ext_only;
	
	/***************************************************************
	* This procedure is used to Extend the Loaner Extension Request
	* Author : HReddi
	****************************************************************/
	PROCEDURE gm_op_sav_le_only_detail (
		p_req_dtl_id	IN		t526_product_request_detail.c526_product_request_detail_id%TYPE,
		p_extn_date		IN 		t504a_consignment_loaner.c504a_expected_return_dt%TYPE,
		p_reason_type   IN 		VARCHAR2,
		p_status        IN      t504b_loaner_extension_detail.c901_status_fl%TYPE,
		p_commnets      IN      VARCHAR2,
		p_user_id       IN 		t106_address.c106_created_by%TYPE
	)AS
		v_commnets VARCHAR2(4000);
	BEGIN
	v_commnets := REPLACE ( REPLACE (p_commnets, Chr (13), ''),Chr (10), ' ');  ---This code change is for PMT-33919 Special character issue
		INSERT INTO t504b_loaner_extension_detail(c504b_loaner_extension_id,  c504b_expected_dt, c901_reason_type, c504b_comments,
                   c504b_created_by, c504b_created_date, C901_STATUS_FL,C526_PRODUCT_REQUEST_DETAIL_ID)
  				 VALUES (s504b_loaner_extension_id.NEXTVAL,p_extn_date, p_reason_type, v_commnets, p_user_id, CURRENT_DATE, p_status, p_req_dtl_id);  ---p_commnets replaced by v_commnets
  				 
	END gm_op_sav_le_only_detail;
	
   /**************************************************************************
    * Description : This function returns Product Request ID Loaner extn count
   **************************************************************************/
   FUNCTION get_parent_lnrextn_cnt (
      p_requestids   IN   t526_product_request_detail.c525_product_request_id%TYPE
   )
      RETURN NUMBER
   IS
            v_parent_lnrextn_cnt   NUMBER;
            v_prod_req_id          VARCHAR2 (20);
   BEGIN
      BEGIN         
            SELECT SUM(c526_extend_count) , c525_product_request_id
              INTO v_parent_lnrextn_cnt, v_prod_req_id
              FROM t526_product_request_detail    
             WHERE c525_product_request_id  IN ( p_requestids ) 
             GROUP BY c525_product_request_id ;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            RETURN 0;
      END;

      RETURN v_parent_lnrextn_cnt;
   END get_parent_lnrextn_cnt;      
/*************************************************************************************
 * Description : Procedure to void FGLE Transactions
 *  Author      : Prabhu vigneshwaran M D  
 **************************************************************************************/	
 PROCEDURE gm_op_update_fgle(
	    p_req_det_id   IN    t526_product_request_detail.c526_product_request_detail_id%TYPE,
        p_status       IN    t504a_loaner_transaction.c901_status%TYPE,
        p_user_id      IN    t2656_audit_entry.c2656_created_by%TYPE)
  AS
  --Get FGLE Transactions based on requestid
     CURSOR void_fgle_cur
		IS			   
		  SELECT T412.c412_inhouse_trans_id  fgle_id 
	      FROM t412_inhouse_transactions T412 ,
	           t504a_loaner_transaction T504A
	      WHERE t412.c504a_loaner_transaction_id     = t504a.c504a_loaner_transaction_id
			AND t504a.c526_product_request_detail_id = p_req_det_id
			AND T412.C412_STATUS_FL = 1-- Pending Approval
			AND t504a.C504A_VOID_FL is null
			AND t412.C412_VOID_FL is null;
BEGIN
  FOR fgle_id_cur IN void_fgle_cur
	LOOP
	 IF p_status = 1903 THEN
		--void FGLE Transactions
		UPDATE t412_inhouse_transactions
		   SET c412_void_fl ='Y',
				c412_last_updated_by   = p_user_id,
				c412_last_updated_date = CURRENT_DATE
		 WHERE c412_inhouse_trans_id   = fgle_id_cur.fgle_id;
		 
		 --void FGLE Transactions on shipping info table
		UPDATE t907_shipping_info
		   SET c907_void_fl = 'Y'
		     , c907_last_updated_by = p_user_id
		     , c907_last_updated_date = CURRENT_DATE
		 WHERE c907_ref_id = fgle_id_cur.fgle_id;
	 ELSE
			--Update FGLE Transactions 
		UPDATE t412_inhouse_transactions
		   SET c412_status_fl = 2,
				c412_last_updated_by   = p_user_id,
				c412_last_updated_date = CURRENT_DATE
		 WHERE c412_inhouse_trans_id   = fgle_id_cur.fgle_id;
			
	 END IF;
   END LOOP;
END gm_op_update_fgle;
 /*************************************************************************************
 * Description : Procedure to get the Email Notification Details
 *  Author      : Tamizhthangam 
 **************************************************************************************/      
PROCEDURE gm_fch_loaner_details(
    p_input_str IN CLOB,
    p_user_id   IN t106_address.c106_created_by%TYPE,
    p_status    IN t504a_loaner_transaction.c901_status%TYPE,
    p_out_loaner_dtls OUT TYPES.cursor_type,
    p_out_header_dtls OUT TYPES.cursor_type )
AS
  v_date_fmt VARCHAR2(20);
  v_time_format VARCHAR2(20);
  v_date_time_fmt VARCHAR2(50);
BEGIN
    SELECT get_compdtfmt_frm_cntx(),get_rule_value('TIME_FMT', 'TIMEFORMAT') 
      INTO v_date_fmt, v_time_format 
      FROM dual;
  v_date_time_fmt := v_date_fmt||v_time_format;
  my_context.set_my_inlist_ctx(p_input_str);
 
  --to get parent Request ID Details for Email Header section
  OPEN p_out_header_dtls FOR
      SELECT t525.c525_product_request_id PARENTREQID,
             t504a.c704_account_id ACCID,
			 get_account_name(t504a.c704_account_id) ACCTNAME,
			 TO_CHAR(CURRENT_DATE ,v_date_time_fmt || ' PM') APPRREJDT,
			 t101.c101_user_f_name ||' '||t101.c101_user_l_name  STATUSBY,
			 get_cs_ship_email (4121, t504a.c703_sales_rep_id) emailadd,
			 t703.c703_Sales_rep_name SALESREP,
			 t504a.c504a_appr_rej_comments CMNTS
        FROM t526_product_request_detail t526,
             t504a_loaner_transaction t504a ,
             t525_product_request t525,
             t101_user t101,
             t703_sales_rep t703
       WHERE t526.c526_product_request_detail_id= t504a.c526_product_request_detail_id
         AND t525.c525_product_request_id         = t526.c525_product_request_id
         AND t101.c101_user_id = t504a.c504a_status_changed_by
         AND t526.c526_product_request_detail_id IN
             (SELECT token FROM v_in_list
              )
          AND t504a.c901_status = p_status
          AND t525.c703_sales_rep_id = t703.c703_sales_rep_id
          AND t504a.c504a_return_dt is null 
          AND t526.c526_void_fl   is null
          AND t504a.c504a_void_fl is null
          AND t525.c525_void_fl is null
          AND t703.c703_void_fl   IS NULL
          AND t504a.c504a_appr_rej_email_fl is null
     GROUP BY t525.c525_product_request_id,t504a.c704_account_id,t101.c101_user_f_name ,t101.c101_user_l_name,t504a.c703_sales_rep_id,t504a.c504a_appr_rej_comments,t504a.c504a_ext_requested_date,t703.c703_Sales_rep_name;
  
  -- To get the All child Request Detail
  OPEN p_out_loaner_dtls FOR 
  SELECT t526.c207_set_id SETID , 
  get_set_name(t526.c207_set_id ) SETNM ,
  t504b.c504a_etch_id ETCHID ,
  TO_CHAR(c504a_old_expected_retdt ,v_date_fmt) PREVDT ,
  TO_CHAR(t504a.c504a_ext_required_date ,v_date_fmt) REQDT,
  TO_CHAR(t504a.c504a_expected_return_dt  ,v_date_fmt) APPRDT,
  t525.c525_product_request_id PARENTREQID,
  t526.c526_product_request_detail_id REQDID,
  t504a.c901_status EXTNSTATUS 
  FROM t504a_loaner_transaction t504a,
  t504a_consignment_loaner t504b ,
  t526_product_request_detail t526,
  t525_product_request t525 
  WHERE t504a.c526_product_request_detail_id IN
    (SELECT token FROM v_in_list WHERE token IS NOT NULL
     )
   AND t526.c526_product_request_detail_id= t504a.c526_product_request_detail_id
   AND t525.c525_product_request_id= t526.c525_product_request_id 
   AND t504a.c504_consignment_id= t504b.c504_consignment_id 
   AND t504a.c901_status = p_status
   AND t526.c526_void_fl is null 
   AND t504a.c504a_return_dt is null 
   AND t504a.c504a_void_fl is null 
   AND t504b.c504a_void_fl is null 
   AND t525.c525_void_fl is null
   AND t504a.c504a_appr_rej_email_fl is null
  ORDER BY t525.c525_product_request_id;
END gm_fch_loaner_details;
  
END gm_pkg_op_loaner_extension;
/
