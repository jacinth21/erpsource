
CREATE OR REPLACE
PACKAGE BODY gm_pkg_op_return_rpt
IS

  	/******************************************************************
	 * Description : Procedure for fetch RA ID by consignment id when RA flag is Y -PMT-14689
	 * Author 	 : Mahavishnu.k
	 ****************************************************************/
  	
	PROCEDURE gm_fch_RA_id_by_CN (
			p_consign_id     IN      T504_CONSIGNMENT.C504_CONSIGNMENT_ID%TYPE
		  , p_ra_id  OUT t520_request.c506_rma_id%TYPE
		)
			AS 
			v_ra_id       t520_request.c506_rma_id%TYPE;
			
			BEGIN
				BEGIN
					SELECT 	T520.C506_RMA_ID
					INTO v_ra_id 
			  	 	FROM t504_consignment T504,t520_request T520 
			  	 	WHERE T504.C504_CONSIGNMENT_ID=p_consign_id 
			  	 	AND	T504.C520_REQUEST_ID = T520.C520_REQUEST_ID 
			  	 	AND	T520.C520_VOID_FL IS NULL 
			  	 	AND	T504.C504_VOID_FL IS NULL;
				EXCEPTION WHEN NO_DATA_FOUND
				THEN
				v_ra_id  :='';
				END;
				
			p_ra_id := v_ra_id ;
		
		END gm_fch_RA_id_by_CN;

END gm_pkg_op_return_rpt;
/
