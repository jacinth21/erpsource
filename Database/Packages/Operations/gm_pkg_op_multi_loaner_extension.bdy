/* Formatted on 2020/10/06 13:19 (Formatter Plus v4.8.0) */
--@"c:\database\packages\operations\gm_pkg_op_multi_loaner_extension.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_multi_loaner_extension
IS
--
   /*******************************************************************************
	* Description : Procedure to save the multi loaner extension
	* Author      : rajan
	********************************************************************************/
	  PROCEDURE gm_op_sav_multi_loaner_ext(
      p_consignmentid   IN   t504a_loaner_transaction.c504_consignment_id%TYPE,
      p_extensiondate   IN   Date,
      p_reasontype      IN   t504b_loaner_extension_detail.c901_reason_type%TYPE,
      p_comments        IN   t504b_loaner_extension_detail.c504b_comments%TYPE,
      p_createdby       IN   t504b_loaner_extension_detail.c504b_created_by%TYPE,
      p_reqextfl        IN   VARCHAR2,
      p_surg_date       IN   t525_product_request.c525_surgery_date%TYPE DEFAULT NULL,
      p_approve_fl      IN   VARCHAR2 DEFAULT NULL)
     
	AS
	
	BEGIN
	   ---	
		 gm_pkg_op_loaner_extension.gm_op_sav_loaner_extension(p_consignmentid,p_extensiondate,p_reasontype,p_comments,p_createdby,p_reqextfl,p_surg_date,p_approve_fl);
		
	END gm_op_sav_multi_loaner_ext;

END gm_pkg_op_multi_loaner_extension;
/