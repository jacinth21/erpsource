/* Formatted on 2010/09/13 18:50 (Formatter Plus v4.8.0) */
-- @"c:\database\packages\operations\gm_pkg_op_process_prod_request.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_process_prod_request
IS
/**********************************************************************
   * Description : Procedure to initiate a request
   * Author 	 : Joe P Kumar
  **********************************************************************/
-- This procedure is calling by ETL Job, please coordinate with DWH team during design phase
	PROCEDURE gm_sav_initiate_request (
		p_request_id		  IN	   t520_request.c520_request_id%TYPE
	  , p_required_date 	  IN	   Date
	  , p_request_source	  IN	   t520_request.c901_request_source%TYPE
	  , p_request_txn_id	  IN	   t520_request.c520_request_txn_id%TYPE
	  , p_set_id			  IN	   t520_request.c207_set_id%TYPE
	  , p_request_for		  IN	   t520_request.c520_request_for%TYPE
	  , p_request_to		  IN	   t520_request.c520_request_to%TYPE
	  , p_request_by_type	  IN	   t520_request.c901_request_by_type%TYPE
	  , p_request_by		  IN	   t520_request.c520_request_by%TYPE
	  , p_ship_to			  IN	   t520_request.c901_ship_to%TYPE
	  , p_ship_to_id		  IN	   t520_request.c520_ship_to_id%TYPE
	  , p_master_request_id   IN	   t520_request.c520_master_request_id%TYPE
	  , p_status_fl 		  IN	   t520_request.c520_status_fl%TYPE
	  , p_userid			  IN	   t520_request.c520_created_by%TYPE
	  , p_inputstring		  IN	   CLOB
	  , p_purpose			  IN	   t520_request.c901_purpose%TYPE
	  , p_rep_id			  IN	   t525_product_request.c703_sales_rep_id%TYPE
	  , p_acc_id			  IN	   t525_product_request.c704_account_id%TYPE
	  , p_inpstr			  IN 	   VARCHAR2
	  , p_planshipdate		  IN 	   t520_request.C520_PLANNED_SHIP_DATE%TYPE
	  , p_assoc_rep_id		  IN	   t525_product_request.c703_ass_rep_id%TYPE
	  , p_out_request_id	  OUT	   t520_request.c520_request_id%TYPE
	  , p_out_consign_id	  OUT	   t504_consignment.c504_consignment_id%TYPE
	  , p_comments 		 	  IN 	   T520_REQUEST.C520_COMMENTS%TYPE DEFAULT NULL
	)
	-- --p_comments is used for additional info of OUS (loaner/item initiate)
	AS
	v_req_for_fl   T906_RULES.C906_RULE_VALUE%TYPE;
	
	BEGIN
		
		SELECT NVL(GET_RULE_VALUE(p_request_for,'REQ_TYPE_GRP'),'N') INTO v_req_for_fl FROM DUAL;
		
		--IF p_request_for = 40021 OR p_request_for = 40022 OR p_request_for = 102930 OR p_request_for = 26240420
		IF v_req_for_fl = 'Y'
		THEN
					gm_sav_initiate_item (p_request_id
							, p_required_date
							, p_request_source
							, p_request_txn_id
							, p_request_for
							, p_request_to
							, p_request_by_type
							, p_request_by
							, p_ship_to
							, p_ship_to_id
							, p_master_request_id
							, p_status_fl
							, p_userid
							, p_inputstring
							, p_purpose
							, p_inpstr
							, p_planshipdate
							, p_out_request_id
							, p_out_consign_id
							, p_comments
							);
				/* Need to insert the INSERT Part number details for the Consignment Created From ETL JOB
				 * 106761:= Consignment;
				 * 93342 := Pick Inititated
				 */
				IF p_purpose = '4000097' THEN -- OUS �Sales Replenishments
					gm_pkg_op_sav_insert_txn.gm_sav_insert_rev_detail(p_out_consign_id,106761,93342,p_userid,NULL);
				END IF;
		ELSE
		  DELETE FROM my_temp_list;
					gm_pkg_op_product_requests.gm_sav_product_requests (p_required_date
											  , p_request_for
											  , p_request_to
											  , p_request_by_type
											  , p_request_by
											  , p_ship_to
											  , p_ship_to_id
											  , p_userid
											  , p_inputstring
											  , p_rep_id
											  , p_acc_id
											  , p_out_request_id
											  , p_inpstr
											  , p_assoc_rep_id
											   );
		 --find any obsolete sets are added,It will throw app error
		 gm_pkg_op_product_requests.gm_validate_set_obsolete_sts();
		END IF;
		
		-- Call the priority engine to prioritize the set
		gm_pkg_op_ln_priority_engine.gm_sav_main_ln_set_priority(p_out_request_id,p_userid);
	END gm_sav_initiate_request;
	
	/****************************************************************
	 * Description : Procedure is to save the loaner item consignment
	 * Author 	 : Angeline
	 *****************************************************************/
	procedure gm_sav_master_loaner_item_consign(
	 p_master_request_id   IN	   t520_request.c520_master_request_id%TYPE,
	 p_request_id          IN	   t520_request.c520_request_id%TYPE,
	 p_userid			   IN	   t520_request.c520_created_by%TYPE
	)
	AS 
	 
	 v_master_request_id         t520_request.c520_master_request_id%TYPE;
	 v_request_id                t520_request.c520_request_id%TYPE;
	 v_userid                    t520_request.c520_created_by%TYPE;
	 
	BEGIN
		 
		--update request table with master product  request id/PBUG-4036
		  UPDATE T520_REQUEST
	   	  SET C525_PRODUCT_REQUEST_ID = p_request_id ,
		  C520_LAST_UPDATED_BY        = p_userid ,
		  C520_LAST_UPDATED_DATE      = CURRENT_DATE
		  WHERE C520_REQUEST_ID       IN 
		  						(SELECT C520_REQUEST_ID
								FROM T520_REQUEST
								WHERE C520_REQUEST_ID     = p_master_request_id
								OR C520_MASTER_REQUEST_ID = p_master_request_id
								AND C520_VOID_FL         IS NULL
								)
		  AND C520_VOID_FL            IS NULL ;
		  
		  --update shipping info table with master product  request id
		 UPDATE T907_SHIPPING_INFO
         SET C525_PRODUCT_REQUEST_ID = p_request_id,
         C907_LAST_UPDATED_DATE    = CURRENT_DATE,
         C907_LAST_UPDATED_BY      = p_userid
         WHERE C907_REF_ID          IN
		        (SELECT C504_CONSIGNMENT_ID
		         FROM T504_CONSIGNMENT
		         WHERE C520_REQUEST_ID = p_master_request_id
		         AND C504_VOID_FL     IS NULL
		         )
         AND C907_VOID_FL IS NULL;
         
END gm_sav_master_loaner_item_consign; 
/**********************************************************************
   * Description : Procedure to initiate request for item
   * Author 	 : Joe P Kumar
  **********************************************************************/
	PROCEDURE gm_sav_initiate_item (
		p_request_id		  IN	   t520_request.c520_request_id%TYPE
	  , p_required_date 	  IN	   Date
	  , p_request_source	  IN	   t520_request.c901_request_source%TYPE
	  , p_request_txn_id	  IN	   t520_request.c520_request_txn_id%TYPE
	  , p_request_for		  IN	   t520_request.c520_request_for%TYPE
	  , p_request_to		  IN	   t520_request.c520_request_to%TYPE
	  , p_request_by_type	  IN	   t520_request.c901_request_by_type%TYPE
	  , p_request_by		  IN	   t520_request.c520_request_by%TYPE
	  , p_ship_to			  IN	   t520_request.c901_ship_to%TYPE
	  , p_ship_to_id		  IN	   t520_request.c520_ship_to_id%TYPE
	  , p_master_request_id   IN	   t520_request.c520_master_request_id%TYPE
	  , p_status_fl 		  IN	   t520_request.c520_status_fl%TYPE
	  , p_userid			  IN	   t520_request.c520_created_by%TYPE
	  , p_inputstring		  IN	   CLOB
	  , p_purpose			  IN	   t520_request.c901_purpose%TYPE
	  , p_inpstr			  IN 	   VARCHAR2
	  , p_shipdate			  IN 	   t520_request.C520_PLANNED_SHIP_DATE%TYPE
	  , p_out_request_id	  OUT	   t520_request.c520_request_id%TYPE
	  , p_out_consign_id	  OUT	   t504_consignment.c504_consignment_id%TYPE
	  , p_comments 		 	  IN 	   T520_REQUEST.C520_COMMENTS%TYPE DEFAULT NULL
	)
	AS
		v_partnum	   t205_part_number.c205_part_number_id%TYPE;
		v_shelfqty	   t208_set_details.c208_set_qty%TYPE;
		v_rwqty		   t208_set_details.c208_set_qty%TYPE;	
		v_reqqty	   t208_set_details.c208_set_qty%TYPE;
		v_child_material_request_bool BOOLEAN := FALSE;
		v_consign_first_time_flag BOOLEAN := FALSE;
		v504_type	   t504_consignment.c504_type%TYPE;
		v_account_id   t520_request.c520_request_to%TYPE;
		v_out_item_consign_id t505_item_consignment.c505_item_consignment_id%TYPE;
		v_out_request_detail_id t521_request_detail.c521_request_detail_id%TYPE;
		v_backorderqty t208_set_details.c208_set_qty%TYPE;
		v_backorder_request_id t520_request.c520_request_id%TYPE;
		v_required_date t520_request.c520_required_date%TYPE;
		v_strlen	   NUMBER := NVL (LENGTH (p_inputstring), 0);
		v_string	   CLOB := p_inputstring;
		v_substring    VARCHAR2 (1000);
		v_seq		   NUMBER;
		v_id		   VARCHAR2 (20);
		v_reqdate	   VARCHAR2 (40);
		v_part_price   NUMBER;
		v_allocate_fl  VARCHAR2 (1);
		v_ref_type varchar2(20);
		v_back_order_master_req_id  t520_request.c520_request_id%TYPE;
		v_div_type 	VARCHAR2(20) := '100823'; -- US Distributor
		
		v_whtype		T505_ITEM_CONSIGNMENT.C901_WAREHOUSE_TYPE%TYPE;
		v_strwhtype		T505_ITEM_CONSIGNMENT.C901_WAREHOUSE_TYPE%TYPE;
		
		 v_allocrwqty 	NUMBER := 0;
		 v_left 		NUMBER := 0;
		 v_ship_carrier VARCHAR2(20);
		 v_ship_mode    VARCHAR2(20);
		 v_dist_type    NUMBER;
		 v_cntry_inv    VARCHAR2(20);
		 v_cntry	    VARCHAR2(10);
		 v_part_string  CLOB;
		 v_ship_string  CLOB;
		 v_ship_substring VARCHAR2(100);
		 v_consign_id    t504_consignment.c504_consignment_id%TYPE;
		 v_company_id    t1900_company.c1900_company_id%TYPE;
    	 v_plant_id 	 t5040_plant_master.c5040_plant_id%TYPE;
    	 v_status        NUMBER := 10;--10-- back order status
	BEGIN
		SELECT DECODE
				   (p_request_for
				  , 40021, 4110
				  , 102930, 4110
				  , 40022, 4112
				  , p_request_for
				   )   -- we are not using the value 40021/4110 (Consignment) of DMDTP. 20021 - Sales. 40022 - InHouse
			 , DECODE (p_request_for, 40022, '01', NULL)
		  INTO v504_type
			 , v_account_id
		  FROM DUAL;

		SELECT DECODE (p_required_date, NULL, SYSDATE, p_required_date)
		  INTO v_required_date
		  FROM DUAL;
		  
		SELECT GET_COMPID_FRM_CNTX() , GET_PLANTID_FRM_CNTX()
      	  INTO v_company_id, v_plant_id
           FROM DUAL;

		  v_div_type := NVL(get_distributor_division(p_request_to),'100823'); -- Fetch the US/OUS Distributor
		  -- get the distributor type
		 v_dist_type := GET_DISTRIBUTOR_TYPE (p_request_to);
		 IF p_request_for = 102930 AND NVL(v_dist_type,'-999') <> 70103
		 THEN
		 	GM_RAISE_APPLICATION_ERROR('-20999','261','');--Please select valid distributor for type ICT
		 END IF;
		 
		SELECT NVL(GET_RULE_VALUE('US','COUNTRY'),'N') INTO v_cntry FROM DUAL;      
		--			3. Creating MR since we have to create it in any case
		IF v_strlen > 0
		THEN
		   -- following code added for resolving the bug for PMT-4359
			-- when ever we are initiating Item for one part having enough qty and one part is having nothing.
			-- this time Ship mode and Ship carrier is not saving in t504 due to incorrect v_string[124.000^1^09/04/2014^|124.000^1^09/04/2014^5001^5004|] comming from GmRequestTransBean.java
			-- now from bean we are getting the v_string as 124.000^1^09/04/2014^|124.000^1^09/04/2014^|~5001^5004| and we are doing susstring as follows...
			IF INSTR (v_string, '~') <> 0 THEN
				v_part_string := SUBSTR (v_string, 1, INSTR (v_string, '~') - 1);
				v_ship_string := SUBSTR (v_string, INSTR (v_string, '~') + 1);
				v_string := v_part_string;
			ELSE
				v_string := v_string;
			END IF;		
			
			IF INSTR (v_ship_string, '|') <> 0 THEN
				v_ship_substring := SUBSTR (v_ship_string, 1, INSTR (v_ship_string, '|') - 1);
				v_ship_carrier := SUBSTR (v_ship_substring, 1, INSTR (v_ship_substring, '^') - 1);
				v_ship_substring := SUBSTR (v_ship_substring, INSTR (v_ship_substring, '^') + 1);
				v_ship_mode := v_ship_substring;
			END IF;
			
			WHILE INSTR (v_string, '|') <> 0
			LOOP
				--
				v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
				v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
				--
				v_partnum	:= NULL;
				v_reqqty	:= NULL;
				--
				v_partnum	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_reqqty	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_reqdate	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_strwhtype	:= v_substring;
							
					     BEGIN
	
							SELECT NVL(GET_QTY_IN_INVENTORY_BY_PLANT(v_consign_id,'90800',v_partnum,v205.C906_RULE_VALUE),0), 0
							  INTO v_shelfqty, v_rwqty
							  FROM t205_part_number t205, V205_PARTS_WITH_EXT_VENDOR v205
							 WHERE t205.c205_part_number_id = v_partnum
							  AND t205.c205_part_number_id = v205.c205_part_number_id
							  AND v205.c1900_company_id = v_company_id
						FOR UPDATE;
	                   EXCEPTION WHEN NO_DATA_FOUND THEN
		          			SELECT NVL(GET_QTY_IN_INVENTORY(v_consign_id,'90800',v_partnum),0), get_qty_in_inv(v_partnum,'56001')
							  INTO v_shelfqty, v_rwqty
							  FROM t205_part_number
							 WHERE c205_part_number_id = v_partnum
						FOR UPDATE;
					
			             END;

		---Setting the Stock Initiate Transaction Qty As zero to execute the flow as same as back order transaction--PMT-18710
			 SELECT DECODE(p_request_for,26240420,0,v_shelfqty), DECODE(p_request_for,26240420,0,v_rwqty) INTO v_shelfqty,v_rwqty FROM DUAL;

				--Below code is for inhouse item consignment US only
				IF v504_type = 4112 AND v_strwhtype = '90800' THEN
					v_rwqty:= 0;
				ELSIF v504_type = 4112 AND v_strwhtype = '56001' THEN
					v_shelfqty := v_rwqty;
				END IF;
				--
				
				--Below code is for item consignment and US/OUS only
				IF v_div_type = '100824' -- OUS Distributor
				THEN
					IF v_rwqty < 0
					THEN
						v_rwqty :=0;
					END IF;
				ELSE -- US Distributor
					v_rwqty := 0;
				END IF;
				--
				
				IF v_shelfqty < 0
				THEN
					v_shelfqty	:= 0;
				END IF;

				-- 4. Since we do orderby on bulkqty and the first bulkqty is > 0, we initiate a consignment
				IF (NOT v_consign_first_time_flag AND (v_shelfqty > 0 OR v_rwqty > 0))
				THEN
					gm_pkg_op_request_master.gm_sav_request (p_request_id
														   , v_required_date
														   , p_shipdate
														   , p_request_source
														   , p_request_txn_id
														   , NULL
														   , p_request_for
														   , p_request_to
														   , p_request_by_type
														   , p_request_by
														   , p_ship_to
														   , p_ship_to_id
														   , p_master_request_id
														   , 15
														   , p_userid
														   , p_purpose
														   , p_out_request_id
														   , p_comments
															);
				
					
					IF p_inpstr is not null
					THEN
						gm_pkg_op_request_master.gm_sav_request_attribute(
																	p_out_request_id,
																	p_inpstr,
																	p_userid
																	);
					END IF;
					gm_pkg_op_request_master.gm_sav_consignment (p_request_to
															   , NULL
															   , NULL
															   , NULL
															   , NULL
															   , '2'   -- Status Flag (Pending Control
															   , NULL
															   , v_account_id
															   , p_ship_to
															   , NULL
															   , p_ship_to_id
															   , p_comments
															   , p_purpose
															   , v504_type
															   , v_ship_carrier -- Shipp Carrier
															   , v_ship_mode  -- Ship Mode
															   , NULL
															   , '1'   --Ship Req Flag
															   , NULL
															   , NULL
															   , NULL
															   , NULL
															   , NULL
															   , NULL
															   , p_out_request_id
															   , p_userid
															   , p_out_consign_id
																);
					v_consign_first_time_flag := TRUE;
					
					
					  IF     v_backorder_request_id IS NOT NULL
                  AND p_out_request_id IS NOT NULL
                  AND v_back_order_master_req_id IS NULL
               THEN
                  UPDATE t520_request
                     SET c520_master_request_id = p_out_request_id
                       , c520_last_updated_by = p_userid
                       , c520_last_updated_date = SYSDATE
                   WHERE c520_request_id = v_backorder_request_id;

                  v_back_order_master_req_id := p_out_request_id;
               END IF;
					
				END IF;
				
				 SELECT DECODE(p_request_for,26240420,0,v_status) INTO v_status FROM DUAL;--Stock Transfer -- need to set the status as Requested(PMT-18710)
			
				-- 5. If we have qty > 0 and shelf has qty lesser than req qty, then we are sure to create a backorder RQ
				IF (NOT v_child_material_request_bool AND (v_shelfqty + v_rwqty) < v_reqqty)
				THEN
					gm_pkg_op_request_master.gm_sav_request (NULL
														   , v_required_date
														   , p_shipdate
														   , p_request_source
														   , p_request_txn_id
														   , NULL
														   , p_request_for
														   , p_request_to
														   , p_request_by_type
														   , p_request_by
														   , p_ship_to
														   , p_ship_to_id
														   , p_out_request_id
														   , v_status
														   , p_userid
														   , p_purpose
														   , v_backorder_request_id
														   , p_comments
															);
					IF p_inpstr is not null
					THEN												
						gm_pkg_op_request_master.gm_sav_request_attribute(
																	v_backorder_request_id,
																	p_inpstr,
																	p_userid
																	);
					END IF;
					
					
					-- for issue 4846 add this code for save sipping information
					IF ((p_out_request_id IS NULL) OR (p_out_request_id = ''))
					THEN
						p_out_request_id := v_backorder_request_id;
					END IF;

					v_child_material_request_bool := TRUE;
				END IF;

				v_part_price := get_part_price (v_partnum, 'C', v_company_id);

				IF TRIM (v_part_price) IS NULL
				THEN
					raise_application_error (-20901, v_partnum);
				END IF;

				-- 6. Consign the parts which has sufficient bulkqty
				IF (v_shelfqty + v_rwqty)  >= v_reqqty
				THEN
				
					 IF v_rwqty >= v_reqqty
					 THEN
				      	v_allocrwqty := v_reqqty; 
				      	v_left := 0;      
				   	 ELSIF v_rwqty > 0   AND v_rwqty < v_reqqty 
				     THEN 
				     	v_allocrwqty := v_rwqty; 
				     	v_left := v_reqqty - v_rwqty; 
				     ELSE
				     	v_left := v_reqqty;
				   	 END IF;
				   	 
				   	 IF v_shelfqty >0 AND v_shelfqty < v_left
				   	 THEN
				   	 	v_left := v_shelfqty;
				   	 END IF;
				   	 
						  
				   	IF v_allocrwqty >0 
				   	THEN
				   		SELECT DECODE(GET_RULE_VALUE('US','COUNTRY'),'Y','56001',NULL) INTO v_cntry_inv FROM DUAL;	
				   		
					   	gm_pkg_op_request_master.gm_sav_consignment_detail (NULL
																	  , p_out_consign_id
																	  , v_partnum
																	  , 'TBE'
																	  , v_allocrwqty
																	  , v_part_price
																	  , NULL
																	  , NULL
																	  , NULL
																	  , v_cntry_inv
																	  , v_out_item_consign_id
																	   );
					v_allocrwqty:=0;
					END IF;
					
					IF v_left >0 
				   	THEN
				   	
						IF v_cntry != 'Y' AND v_strwhtype IS NULL 
						THEN						
							v_strwhtype := 90800;	--OUS WAREHOUSE -- As a quick fix, update as 90800  
						
						ELSIF v_cntry = 'Y' AND v_strwhtype IS NULL 
						THEN						
							v_strwhtype := 90800;	--US WAREHOUSE							
						END IF;
				   	
					   	SELECT DECODE(v_cntry ,'Y', DECODE(v504_type, 4112, v_strwhtype,'90800'), v_strwhtype) INTO v_whtype FROM DUAL;
				   	
						gm_pkg_op_request_master.gm_sav_consignment_detail (NULL
																	  , p_out_consign_id
																	  , v_partnum
																	  , 'TBE'
																	  , v_left
																	  , v_part_price
																	  , NULL
																	  , NULL
																	  , NULL
																	  , v_whtype-- FG Warehouse
																	  , v_out_item_consign_id
																	   );
																	   
						v_left := 0;																	   
					END IF;
					
					
				END IF;

				IF  (v_shelfqty + v_rwqty)  < v_reqqty
				THEN
					-- 7. Calculate backorderQty and update request detail and item consignment accordingly
					v_backorderqty := v_reqqty - (v_shelfqty +v_rwqty);
					
					-- update the table with the quantity. if there are same part in the request sum up the qty ordered
					UPDATE t521_request_detail
						SET c521_qty = NVL (c521_qty, 0) + v_backorderqty
					WHERE c520_request_id = v_backorder_request_id 
						AND c205_part_number_id = v_partnum;
					IF (SQL%ROWCOUNT = 0)
					THEN
						gm_pkg_op_request_master.gm_sav_request_detail (v_backorder_request_id
																	  , v_partnum
																	  , v_backorderqty
																	  , v_out_request_detail_id
																	   );
					END IF;
					IF (v_shelfqty + v_rwqty) > 0
					THEN
						 IF v_rwqty > 0 
						 THEN 
                            v_allocrwqty := v_rwqty; 
                            v_left := v_reqqty - v_allocrwqty; 
                 		 ELSE 
                          	v_left := v_reqqty;             
                 		 END IF; 

                 		 IF v_shelfqty >0 AND v_shelfqty < v_left
				   	 	 THEN
				   	 		v_left := v_shelfqty;
				   	 	 END IF;
				   	 
						IF v_allocrwqty >0 
					   	THEN
					   		gm_pkg_op_request_master.gm_sav_consignment_detail (NULL
																		  , p_out_consign_id
																		  , v_partnum
																		  , 'TBE'
																		  , v_allocrwqty
																		  , v_part_price
																		  , NULL
																		  , NULL
																		  , NULL
																		  , '56001' -- RW Warehouse
																		  , v_out_item_consign_id
																		   );
							v_allocrwqty := 0;																		   
						END IF;
						
						IF v_left >0 AND v_shelfqty >0 AND v_left >= v_shelfqty -- Checked shelf qty count
						THEN
						v_left := v_shelfqty; 
						
							IF v_cntry != 'Y' AND v_strwhtype IS NULL 
							THEN						
								v_strwhtype := 90800;	--OUS WAREHOUSE -- As a quick fix, update as 90800 
							
							ELSIF v_cntry = 'Y' AND v_strwhtype IS NULL 
							THEN						
								v_strwhtype := 90800;	--US WAREHOUSE							
							END IF;
					   	
						   	SELECT DECODE(v_cntry ,'Y', DECODE(v504_type, 4112, v_strwhtype,'90800'), v_strwhtype) INTO v_whtype FROM DUAL; 
						   	
							gm_pkg_op_request_master.gm_sav_consignment_detail (NULL
																		  , p_out_consign_id
																		  , v_partnum
																		  , 'TBE'
																		  , v_left
																		  , v_part_price
																		  , NULL
																		  , NULL
																		  , NULL
																		  , v_whtype -- FG Warehouse
																		  , v_out_item_consign_id
																		   );
							v_left := 0;																
						END IF;
					END IF;
				END IF;
				v_consign_id := p_out_consign_id;
			END LOOP;
		END IF;

		-- Code added for inventory pickup
		IF (p_out_consign_id IS NOT NULL)
		THEN
			-- Code added for allocation logic
			-- If the Transaction has Literature part, it should not be allocated to device.			
			v_allocate_fl := gm_pkg_op_inventory.chk_txn_details (p_out_consign_id, 93002);

			IF NVL (v_allocate_fl, 'N') = 'Y'
			THEN
			  select DECODE(v504_type,4110,93002,4112,93009,93002) INTO v_ref_type from dual;
			  gm_pkg_allocation.gm_ins_invpick_assign_detail (v_ref_type, p_out_consign_id, p_userid);
			END IF;
			
			--When ever the child is updated, the consignment table has to be updated ,so ETL can Sync it to GOP.
			UPDATE t504_consignment
			   SET c504_last_updated_by   = p_userid
			     , c504_last_updated_date = SYSDATE
			 WHERE c504_consignment_id = p_out_consign_id;	
		END IF;
	END gm_sav_initiate_item;
	
  /****************************************************************************************************************
   * Description : Procedure to update Tissue Parts shipping details in Shipping table and request table (PMT-40240)
   * Author 	 : Agilan Singaravel
  *****************************************************************************************************************/
	PROCEDURE gm_update_tissue_shipping (
		p_master_request_id   IN	   t520_request.c520_master_request_id%TYPE
	  , p_master_consign_id   IN       t504_consignment.c504_consignment_id%TYPE
	  , p_tissue_ship_to_id   IN       t520_request.c520_ship_to_id%TYPE
	  , p_tissue_ship_to      IN       t520_request.c901_ship_to%TYPE
	  , p_tissue_address_id   IN       t907_shipping_info.c106_address_id%TYPE
	  , p_tissue_rep_id       IN       t525_product_request.c703_sales_rep_id%TYPE
	  , p_userid              IN       t520_request.c520_created_by%TYPE
	)
	AS
	
		v_ref_id    t907_shipping_info.c907_ref_id%type;
		
		CURSOR fch_request_id
		IS
			SELECT t520.c520_request_id request_id
   			  FROM T520_REQUEST t520
  			 WHERE t520.c520_master_request_id = p_master_request_id
  			   AND t520.c520_void_fl is null;
  			   
  	    CURSOR fch_consignment_id
		IS
			SELECT t504.c504_consignment_id consignment_id
   			  FROM T504_CONSIGNMENT t504
  			 WHERE t504.c504_master_consignment_id = p_master_consign_id
  			   AND t504.c504_void_fl is null;

	BEGIN
		
		--1. Check if the  master request is Tissue Related
	  BEGIN
		SELECT t907.c907_ref_id INTO v_ref_id
		  FROM T907_SHIPPING_INFO t907
		 WHERE t907.c907_ref_id  = p_master_request_id
		   AND t907.c901_tissue_ship_fl is not null
		   AND t907.c907_void_fl is null;
	  EXCEPTION WHEN OTHERS THEN
	      v_ref_id := '';
	  END;   
	  
	 IF v_ref_id is NOT NULL THEN
		   gm_update_tissue_shipping_request(p_master_request_id,p_tissue_ship_to_id,p_tissue_ship_to,p_tissue_address_id,p_userid);
	 END IF;
	 
	 	--2. Find all the Spilt Request created for the  master Request id and update Shipto and Ship to id in t520 and t907
	 	FOR v_requestid IN fch_request_id
		LOOP
		  BEGIN
			SELECT t907.c907_ref_id INTO v_ref_id
   	   		  FROM T907_SHIPPING_INFO t907
  			 WHERE t907.c907_ref_id = v_requestid.request_id
  			   AND t907.c901_tissue_ship_fl IS NOT NULL
    		   AND t907.c907_void_fl is null;
    		   
        EXCEPTION WHEN OTHERS THEN
	      v_ref_id := '';
	  	END; 
	  	
	  	   IF v_ref_id is NOT NULL THEN
		   		gm_update_tissue_shipping_request(v_requestid.request_id,p_tissue_ship_to_id,p_tissue_ship_to,p_tissue_address_id,p_userid);
	 	   END IF;
    	END LOOP;
    	
    	--3. Check if the Master Consignment id is Tissue related
    	IF p_master_consign_id is NOT NULL THEN 
    		BEGIN
    	 		SELECT t907.c907_ref_id INTO v_ref_id
     	   		  FROM T907_SHIPPING_INFO t907
  		         WHERE t907.c907_ref_id = p_master_consign_id
  		  		   AND t907.c901_tissue_ship_fl IS NOT NULL
  		  		   AND t907.c907_void_fl is null;
		    EXCEPTION WHEN OTHERS THEN
	       		v_ref_id := '';
	  		END;
	  	
	  		IF v_ref_id is NOT NULL THEN
		   		gm_update_tissue_shipping_consignment(p_master_consign_id,p_tissue_ship_to_id,p_tissue_ship_to,p_tissue_address_id,p_userid);
	 		END IF;
	 	END IF;
	 	
	 	--4. Find Spilt Consignment for the Child  request and update Ship to and Ship to ID or we can do for Master Consignment id
	 	FOR v_consignmentid IN fch_consignment_id
		LOOP
		  BEGIN
			SELECT t907.c907_ref_id INTO v_ref_id
   	   		  FROM T907_SHIPPING_INFO t907
  			 WHERE t907.c907_ref_id = v_consignmentid.consignment_id
  			   AND t907.c901_tissue_ship_fl IS NOT NULL
    		   AND t907.c907_void_fl is null;
    		   
        EXCEPTION WHEN OTHERS THEN
	      v_ref_id := '';
	  	END; 
	  	
	  	   IF v_ref_id is NOT NULL THEN
		   		gm_update_tissue_shipping_consignment(v_consignmentid.consignment_id,p_tissue_ship_to_id,p_tissue_ship_to,p_tissue_address_id,p_userid);
	 	   END IF;
    	END LOOP;
	  	
	END gm_update_tissue_shipping;
	
  /******************************************************************************************************
   * Description : Procedure to update Tissue Parts ship to id and ship to on Shipping table and request table
   * 			   details based on Request id (PMT-40240)
   * Author 	 : Agilan Singaravel
  *******************************************************************************************************/
	PROCEDURE gm_update_tissue_shipping_request ( 
		p_request_id          IN	   t907_shipping_info.c907_ref_id%TYPE
	  , p_tissue_ship_to_id   IN       t520_request.c520_ship_to_id%TYPE
	  , p_tissue_ship_to      IN       t520_request.c901_ship_to%TYPE
	  , p_tissue_address_id   IN       t907_shipping_info.c106_address_id%TYPE
	  , p_userid              IN       t520_request.c520_created_by%TYPE
	)
	AS
	BEGIN
		
		UPDATE T520_REQUEST
		   SET c901_ship_to = p_tissue_ship_to
		     , c520_ship_to_id = p_tissue_ship_to_id
		     , c520_last_updated_by =  p_userid
		     , c520_last_updated_date =  CURRENT_DATE
		 WHERE c520_request_id = p_request_id
		   AND c520_void_fl is null;
		  
		UPDATE T907_SHIPPING_INFO
		   SET c901_ship_to = p_tissue_ship_to
		     , c907_ship_to_id = p_tissue_ship_to_id
		     , c106_address_id=p_tissue_address_id
		     , c907_last_updated_by =  p_userid
		     , c907_last_updated_date =  CURRENT_DATE
		 WHERE c907_ref_id = p_request_id
		   AND c901_tissue_ship_fl is NOT NULL
		   AND c907_void_fl is null;
		   
	END gm_update_tissue_shipping_request;
		
  /******************************************************************************************************
   * Description : Procedure to update Tissue Parts ship to id and ship to on Shipping table and request table
   * 			   details based on Consignment id (PMT-40240)
   * Author 	 : Agilan Singaravel
  *******************************************************************************************************/
	PROCEDURE gm_update_tissue_shipping_consignment ( 
		p_consignment_id      IN	   t504_consignment.c504_consignment_id%TYPE
	  , p_tissue_ship_to_id   IN       t504_consignment.c504_ship_to_id%TYPE
	  , p_tissue_ship_to      IN       t504_consignment.c504_ship_to%TYPE
	  , p_tissue_address_id   IN       t907_shipping_info.c106_address_id%TYPE
	  , p_userid              IN       t504_consignment.c504_created_by%TYPE
	)
	AS
	BEGIN
		
		UPDATE T504_CONSIGNMENT
		   SET c504_ship_to = p_tissue_ship_to
		     , c504_ship_to_id = p_tissue_ship_to_id
		     , c504_last_updated_by =  p_userid
		     , c504_last_updated_date =  CURRENT_DATE
		 WHERE c504_consignment_id = p_consignment_id
		   AND c504_void_fl is null;
		   
		UPDATE T907_SHIPPING_INFO
		   SET c901_ship_to = p_tissue_ship_to
		     , c907_ship_to_id = p_tissue_ship_to_id
		     , c106_address_id=p_tissue_address_id
		     , c907_last_updated_by =  p_userid
		     , c907_last_updated_date =  CURRENT_DATE
		 WHERE c907_ref_id = p_consignment_id
		   AND c901_tissue_ship_fl is not null   
		   AND c907_void_fl is null;
		
	END gm_update_tissue_shipping_consignment;
	
  /******************************************************************************************************
   * Description : Procedure to update Tissue Parts ship to id and ship to on Shipping table and request table
   * 			   details based product detail id (PMT-40240)
   * Author 	 : Agilan Singaravel
  *******************************************************************************************************/
	PROCEDURE gm_sav_request_tissue_shipping_details(
		p_product_request_id	IN		t907_shipping_info.c525_product_request_id%type
	  , p_tissue_ship_to_id		IN		t525_product_request.c525_ship_to_id%type
	  , p_tissue_ship_to		IN		t525_product_request.c901_ship_to%type
	  , p_tissue_rep_id       	IN      t525_product_request.c703_sales_rep_id%type 
	  , p_tissue_address_id     IN      t907_shipping_info.c106_address_id%TYPE
	  , p_userid              	IN      t525_product_request.c525_created_by%TYPE
	)	
	AS
	v_ref_id    	  t907_shipping_info.c907_ref_id%type;
	v_req_dtl_id      t526_product_request_detail.c526_product_request_detail_id%type;
	
	CURSOR fch_request_dtl_id
	IS
		SELECT t526.c526_product_request_detail_id reqdtlid
		  FROM T526_PRODUCT_REQUEST_DETAIL t526
		 WHERE t526.c525_product_request_id = p_product_request_id
		   AND t526.c526_void_fl is null;
	
	BEGIN

		FOR v_request_dtl_id IN fch_request_dtl_id
		LOOP
		v_req_dtl_id := v_request_dtl_id.reqdtlid;
	
		  BEGIN
			SELECT t907.c907_ref_id INTO v_ref_id
			  FROM T907_SHIPPING_INFO t907
			 WHERE t907.c907_ref_id = to_char(v_req_dtl_id)
  			   AND t907.c901_tissue_ship_fl IS NOT NULL
    		   AND t907.c907_void_fl is null;

		  EXCEPTION WHEN OTHERS THEN
	      	v_ref_id := '';
	  	  END; 
		
		IF v_ref_id IS NOT NULL THEN
		   		gm_sav_ship_tissue_shipping_details(v_request_dtl_id.reqdtlid,p_tissue_ship_to_id,p_tissue_ship_to,p_tissue_address_id,p_userid);
	 	END IF;
 	  END LOOP; 
	END gm_sav_request_tissue_shipping_details;
		
  /******************************************************************************************************
   * Description : Procedure to update Tissue Parts ship to id and ship to on Shipping table
   * 			   details based on product request detail id (PMT-40240)
   * Author 	 : Agilan Singaravel
  *******************************************************************************************************/
	PROCEDURE gm_sav_ship_tissue_shipping_details(
		p_product_request_dtl_id	IN		t907_shipping_info.c525_product_request_id%type
	  , p_tissue_ship_to_id			IN		t525_product_request.c525_ship_to_id%type
	  , p_tissue_ship_to			IN		t525_product_request.c901_ship_to%type
	  , p_tissue_address_id         IN       t907_shipping_info.c106_address_id%TYPE
	  , p_userid              		IN      t525_product_request.c525_created_by%TYPE
	)
	AS
	BEGIN

		UPDATE t504_consignment
		   SET c504_ship_to = p_tissue_ship_to
		     , c504_ship_to_id = p_tissue_ship_to_id
		     , c504_last_updated_by = p_userid
		     , c504_last_updated_date = CURRENT_DATE
		 WHERE c526_product_request_detail_id  = p_product_request_dtl_id
		   AND c504_void_fl is null;
		
		UPDATE T907_SHIPPING_INFO
		   SET c901_ship_to = p_tissue_ship_to
		     , c907_ship_to_id = p_tissue_ship_to_id
		     , c106_address_id=  p_tissue_address_id
		     , c907_last_updated_by = p_userid
		     , c907_last_updated_date = CURRENT_DATE
		 WHERE c907_ref_id = p_product_request_dtl_id
		   AND c901_tissue_ship_fl is not null 
		   AND c907_void_fl is null;
		
	END gm_sav_ship_tissue_shipping_details;
	
  /******************************************************************************
   * Description : Procedure to update tissue fl on t907_shipping_info (PMT-40240)
   * Author 	 : Agilan Singaravel
  *******************************************************************************/		
	PROCEDURE gm_upd_tissue_fl(
		p_request_id		  IN	   t520_request.c520_request_id%TYPE
	  ,	p_type                IN       VARCHAR2
	  , p_userid			  IN	   t520_request.c520_created_by%TYPE
	)
	AS
		v_count       	VARCHAR2(2);
		v_count_cons	VARCHAR2(2);
		v_consign_id    VARCHAR2(20);
		v_count_loaner  NUMBER;
		
		CURSOR v_child_req_id
		IS
			SELECT t520.c520_request_id childreqid
			  FROM T520_REQUEST t520
			 WHERE t520.c520_master_request_id = p_request_id 
			   AND t520.c520_void_fl is null;
			   
	    CURSOR fch_req_dtl_id
		IS
			SELECT t526.c526_product_request_detail_id reqdtlid
		      FROM T526_PRODUCT_REQUEST_DETAIL t526
		     WHERE t526.c525_product_request_id = p_request_id
		       AND t526.c526_void_fl is null;
	BEGIN
		
		--Consignment
		IF p_type <> '4127' THEN
		--1. Check Master request had tissue parts or not and update tissue fl.		
			gm_fch_tissue_parts_count(p_request_id,v_count);
			gm_fch_tissue_parts_on_cons(p_request_id,v_count_cons,v_consign_id);

			IF (v_count = 'Y' OR v_count_cons = 'Y' ) THEN
				gm_upd_ship_tissue_fl(p_request_id,p_userid);
				
				IF v_consign_id is not null THEN
					gm_upd_ship_tissue_fl(v_consign_id,p_userid);
			    END IF;
			    
  			END IF;	
  			
  		 --2. Check split child request had tissue parts or not and update tissue fl.
  		 	FOR v_childRequestId IN v_child_req_id
			LOOP 

				gm_fch_tissue_parts_count(v_childRequestId.childreqid,v_count);
				gm_fch_tissue_parts_on_cons(v_childRequestId.childreqid,v_count_cons,v_consign_id);

				IF (v_count = 'Y' OR v_count_cons = 'Y' ) THEN
					gm_upd_ship_tissue_fl(v_childRequestId.childreqid,p_userid);
					
					IF v_consign_id is not null THEN
						gm_upd_ship_tissue_fl(v_consign_id,p_userid);
			    	END IF;
			    	
  				END IF;	
  			
			END LOOP;
		ELSE
			FOR v_loaner_req_dtl_id IN fch_req_dtl_id
			LOOP 
				
				SELECT count(1) INTO v_count_loaner
				  FROM T207_SET_MASTER t207,T526_PRODUCT_REQUEST_DETAIL t526
				 WHERE t207.c207_set_id = t526.c207_set_id
				   AND c526_product_request_detail_id = v_loaner_req_dtl_id.reqdtlid
				   AND t207.c207_tissue_fl IS NOT NULL
				   AND t207.c207_void_fl is null
				   AND t526.c526_void_fl is null;
			
				IF (v_count_loaner <> 0) THEN
					gm_upd_ship_tissue_fl(v_loaner_req_dtl_id.reqdtlid,p_userid);
  				END IF;	
  			
			END LOOP;
		END IF;
	END gm_upd_tissue_fl;
	
 /*******************************************************************************
   * Description : Procedure to fetch tissue part count on request id (PMT-40240)
   * Author 	 : Agilan Singaravel
  *******************************************************************************/	
	PROCEDURE gm_fch_tissue_parts_count(
		p_request_id		  	  IN	   t520_request.c520_request_id%TYPE
	  , p_tissue_fl				  OUT      VARCHAR2
	)
	AS
		v_count 	NUMBER;
	BEGIN
		
		SELECT count(1) INTO v_count
		  FROM T521_REQUEST_DETAIL t521,V205_PARTS_WITH_EXT_VENDOR v205,T520_REQUEST t520 
		 WHERE t521.c205_part_number_id = v205.c205_part_number_id
		   AND t520.c520_request_id = t521.c520_request_id
		   AND t520.c520_request_id = p_request_id
		   AND t520.c520_void_fl is null;
		   
		   IF v_count > 0 THEN
		  		p_tissue_fl := 'Y';
		   END IF;
		
	END gm_fch_tissue_parts_count;
	
  /***************************************************************************************
   * Description : Procedure to fetch consignment id have tissue part or not (PMT-40240)
   * Author 	 : Agilan Singaravel
  ***************************************************************************************/	
	PROCEDURE gm_fch_tissue_parts_on_cons(
			p_request_id		  IN	   t520_request.c520_request_id%TYPE
	  	  , p_tissue_fl			  OUT      VARCHAR2
	  	  , p_consign_id		  OUT      VARCHAR2
		)
	AS
	v_count   		NUMBER;
	v_consign_id 	t504_consignment.c504_consignment_id%type;
	BEGIN	
		BEGIN
			SELECT c504_consignment_id INTO v_consign_id 
		  	  FROM t504_consignment 
		 	 WHERE c520_request_id = p_request_id
		 	   AND c504_void_fl is null;
		EXCEPTION WHEN OTHERS THEN
				v_consign_id := '';
		END;
		v_count := 0;
		IF v_consign_id is not null THEN
		SELECT count(1) INTO v_count
		  FROM t505_item_consignment t505,t504_consignment t504,v205_parts_with_ext_vendor v205
		 WHERE t505.c205_part_number_id = v205.c205_part_number_id 
		   AND t504.c504_consignment_id = t505.c504_consignment_id
		   AND nvl(t505.c504_consignment_id,-999) = v_consign_id 			
		   AND t504.c504_void_fl is null
		   AND t505.c505_void_fl is null;
		END IF;
		
		   IF v_count > 0 THEN
		  		p_tissue_fl := 'Y';
		   END IF;
		p_consign_id := v_consign_id;
	END gm_fch_tissue_parts_on_cons;
	
  /*******************************************************************************
   * Description : Procedure to update tissue fl on t907_shipping_info (PMT-40240)
   * Author 	 : Agilan Singaravel
  ********************************************************************************/	
	PROCEDURE gm_upd_ship_tissue_fl(
		p_request_id		  IN	   t907_shipping_info.c907_ref_id%TYPE
	  , p_userid			  IN	   t520_request.c520_created_by%TYPE
	)
	AS
	BEGIN
			UPDATE T907_SHIPPING_INFO
  			   SET c901_tissue_ship_fl = 'Y'
  				 , c907_last_updated_by = p_userid
  				 , c907_last_updated_date = CURRENT_DATE
  		     WHERE c907_ref_id = p_request_id
  			   AND c907_void_fl is null;
	END gm_upd_ship_tissue_fl;
	
END gm_pkg_op_process_prod_request;
/