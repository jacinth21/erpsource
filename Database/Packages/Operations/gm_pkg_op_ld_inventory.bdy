/* Formatted on 2011/01/06 12:09 (Formatter Plus v4.8.0) */
/*Formatted on 2010/11/17 11:54 (Formatter Plus v4.8.0) */
-- @"c:\database\packages\operations\gm_pkg_op_ld_inventory.bdy"
-- show err  Package Body gm_pkg_op_ld_inventory
/*
delete from t251_inventory_lock_detail;
delete from t253a_consignment_lock;
delete from t253b_item_consignment_lock;
delete from t253c_request_lock;
delete from t253d_request_detail_lock;
delete from t253e_order_lock;
delete from t253f_item_order_lock;
delete from t252_inventory_lock_ref;
delete from t254_inventory_txn_log;
update t4040_demand_sheet set c250_inventory_lock_id='';
delete from t250_inventory_lock;
exec gm_pkg_op_ld_inventory.gm_op_ld_demand_main(20430);
commit;
*/
CREATE OR REPLACE
PACKAGE BODY gm_pkg_op_ld_inventory
IS
    /*******************************************************
    * Purpose: This procedure will be main procedure to
    *   load inventory information
    *******************************************************/
PROCEDURE gm_op_ld_demand_main (
        p_lock_type IN t250_inventory_lock.c901_lock_type%TYPE,
        p_company_id IN t1900_company.c1900_company_id%TYPE,
        p_plant_id IN t5040_plant_master.c5040_plant_id%TYPE)
AS
    --
    v_inventory_id t250_inventory_lock.c250_inventory_lock_id%TYPE;
    v_us_time_zone t901_code_lookup.c901_code_nm%TYPE;
    v_us_time_format t901_code_lookup.c901_code_nm%TYPE;
    
    v_start_dt DATE;
    --
BEGIN
    --
    v_start_dt := CURRENT_DATE;
    
    SELECT GET_CODE_NAME(C901_TIMEZONE), GET_CODE_NAME(C901_DATE_FORMAT)
        INTO v_us_time_zone,v_us_time_format
     FROM t1900_company
     WHERE c1900_company_id=p_company_id;
     
     --Set Context
     gm_pkg_cor_client_context.gm_sav_client_context(p_company_id,v_us_time_zone,v_us_time_format,p_plant_id);
    --
     SELECT s250_inventory_lock.NEXTVAL INTO v_inventory_id FROM DUAL;
    --
     INSERT
       INTO t250_inventory_lock
        (
            c250_inventory_lock_id, c250_lock_date, c901_lock_type,c1900_company_id,c5040_plant_id
        )
        VALUES
        (
            v_inventory_id, TRUNC (CURRENT_DATE), p_lock_type,NULL,p_plant_id
        ) ;
    -- If mapped to Main inventory load
    IF (p_lock_type = 20430) THEN
        --
        gm_pkg_op_ld_inventory.gm_op_ld_inshelf (v_inventory_id, p_lock_type) ;
        gm_pkg_op_ld_inventory.gm_op_ld_openpo (v_inventory_id) ;
        gm_pkg_op_ld_inventory.gm_op_ld_opendhr (v_inventory_id) ;
        gm_pkg_op_ld_inventory.gm_op_ld_buildset (v_inventory_id) ;
        gm_pkg_op_ld_inventory.gm_op_ld_openpodhr (v_inventory_id) ;
        gm_pkg_op_ld_inventory.gm_op_ld_pending_request (v_inventory_id, v_start_dt) ;
        gm_pkg_op_ld_inventory.gm_op_ld_sub_components (v_inventory_id) ;
        -- currently par is handled at demand sheet level
        -- So the code is commented
        --gm_pkg_op_ld_inventory.gm_op_ld_par (v_inventory_id);
    ELSIF (p_lock_type = 20431) THEN
        gm_pkg_op_ld_inventory.gm_op_ld_inshelf (v_inventory_id, p_lock_type) ;
        gm_pkg_op_ld_inventory.gm_op_ld_loaners (v_inventory_id) ;
        gm_pkg_op_ld_inventory.gm_op_ld_consignments (v_inventory_id) ;
        gm_pkg_op_ld_inventory.gm_op_ld_manufacturing (v_inventory_id) ;
        gm_pkg_op_ld_inventory.gm_op_ld_del_rm_parts (v_inventory_id) ;
        gm_pkg_op_ld_inventory.gm_op_ld_inv_locations (v_inventory_id) ;
        -- Lock the DHR Area by the DHR Txn ID.
        gm_pkg_op_ld_inventory.GM_OP_LD_DHR_TXN (v_inventory_id) ;
        
        --This is for BBA Cut Plan Inventory Load.
    ELSIF (p_lock_type = 4000564) THEN
    	gm_pkg_op_ld_inventory.gm_op_ld_inshelf (v_inventory_id, p_lock_type) ;
    	
    	-- We need last 12 month Sales Data ,hence the start date is set to 12 months back.
    	SELECT TRUNC(ADD_MONTHS(CURRENT_DATE,-12)) INTO v_start_dt FROM DUAL;
    	gm_pkg_op_ld_inventory.gm_op_ld_sales_orders (v_inventory_id, v_start_dt) ;
    
        --
    END IF;
    --
    -- LOG THE STATUS
    gm_pkg_cm_job.gm_cm_notify_load_info (v_start_dt, CURRENT_DATE, 'S', 'SUCCESS', 'gm_pkg_op_ld_inventory') ;
    COMMIT;
EXCEPTION
WHEN OTHERS THEN
    ROLLBACK;
    --
    gm_pkg_cm_job.gm_cm_notify_load_info (v_start_dt, CURRENT_DATE, 'E', SQLERRM, 'gm_pkg_op_ld_inventory') ;
    COMMIT;
    --
END gm_op_ld_demand_main;
--


/*************************************************************************
* Purpose: Procedure used to Load the Sales Data to the Order Lock and Item Order Lock Table.
*************************************************************************/
PROCEDURE gm_op_ld_sales_orders (
 		p_inventory_lock_id IN t250_inventory_lock.c250_inventory_lock_id%TYPE,
        p_from_period       IN DATE)
AS
v_company_id  t1900_company.c1900_company_id%TYPE;

BEGIN
	--
	SELECT  NVL(get_compid_frm_cntx(),GET_RULE_VALUE('DEFAULT_COMPANY','ONEPORTAL'))
	      INTO  v_company_id
	      FROM DUAL;
	--      
	/* Get all Orders for the last 12 months. 
		The p_from_period will be 12 months back from current date.
	*/
	    INSERT
       INTO t253e_order_lock
        (
            c253e_order_lock_id, c250_inventory_lock_id, c501_order_id
          , c501_status_fl, c501_void_fl, c501_last_updated_by
          , c501_last_updated_date, c901_order_type, c501_parent_order_id
          , c501_total_cost, c501_ship_to, c501_ship_to_id
          , c501_ship_cost, c501_update_inv_fl, c503_invoice_id
          , c506_rma_id, c501_customer_po, c704_account_id
          , c501_order_date, c703_sales_rep_id, c501_shipping_date
          , c501_order_date_time, c1900_company_id, c5040_plant_id
        )
     SELECT s253e_order_lock.NEXTVAL, p_inventory_lock_id, c501_order_id
      , c501_status_fl, c501_void_fl, c501_last_updated_by
      , c501_last_updated_date, c901_order_type, c501_parent_order_id
      , c501_total_cost, c501_ship_to, c501_ship_to_id
      , c501_ship_cost, c501_update_inv_fl, c503_invoice_id
      , c506_rma_id, c501_customer_po, c704_account_id
      , c501_order_date, c703_sales_rep_id, c501_shipping_date
      , c501_order_date_time, c1900_company_id, c5040_plant_id
       FROM t501_order t501
      WHERE t501.c501_void_fl IS NULL
      	AND t501.c1900_company_id = v_company_id
		AND (t501.c901_ext_country_id IS NULL OR 
			 t501.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH
        AND t501.c501_order_date BETWEEN p_from_period AND CURRENT_DATE
        ;
    --
     INSERT
       INTO t253f_item_order_lock
        (
            c253f_item_order_lock_id, c250_inventory_lock_id, c501_order_id
          , c205_part_number_id, c502_item_qty, c502_control_number
          , c502_item_price, c502_void_fl, c502_item_seq_no
          , c901_type, c502_construct_fl
        )
     SELECT s253f_item_order_lock.NEXTVAL, p_inventory_lock_id, t502.c501_order_id
      , c205_part_number_id, c502_item_qty, c502_control_number
      , c502_item_price, c502_void_fl, c502_item_seq_no
      , c901_type, c502_construct_fl
       FROM t501_order t501, t502_item_order t502
      WHERE t501.c501_order_id = t502.c501_order_id
      	AND t501.c1900_company_id = v_company_id
        AND t501.c501_void_fl IS NULL
		AND (t501.c901_ext_country_id IS NULL OR 
			t501.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH
        AND t501.c501_order_date BETWEEN p_from_period AND CURRENT_DATE
        ;
     
	
END gm_op_ld_sales_orders;

/*************************************************************************
* Purpose: Procedure used to save inventory details (For different type)
*************************************************************************/
PROCEDURE gm_op_sav_inventory_detail
    (
        p_inventory_lock_id IN t250_inventory_lock.c250_inventory_lock_id%TYPE,
        p_part_number       IN t251_inventory_lock_detail.c205_part_number_id%TYPE,
        p_type              IN t251_inventory_lock_detail.c901_type%TYPE,
        p_qty               IN t251_inventory_lock_detail.c251_qty%TYPE
    )
AS
BEGIN
     INSERT
       INTO t251_inventory_lock_detail
        (
            c251_inventory_lock_detail_id, c250_inventory_lock_id, c205_part_number_id
          , c901_type, c251_qty
        )
        VALUES
        (
            s251_inventory_lock_detail.NEXTVAL, p_inventory_lock_id, p_part_number
          , p_type, ROUND (p_qty)
        ) ;
END gm_op_sav_inventory_detail;
/*************************************************************************
* Purpose: Procedure used to load inventory on Shelf Info
*************************************************************************/
PROCEDURE gm_op_ld_inshelf
    (
        p_inventory_lock_id IN t250_inventory_lock.c250_inventory_lock_id%TYPE,
        p_lock_type         IN t250_inventory_lock.c901_lock_type%TYPE
    )
AS
    	v_plant_id 	  t5040_plant_master.c5040_plant_id%TYPE;
    	v_company_id  t1900_company.c1900_company_id%TYPE;
    CURSOR inshelf_cur
    IS
         SELECT c205_part_number_id pnum, sales_allocated sales_allocated, c205_cons_alloc cons_alloc
          , c205_quar_alloc_self_quar quar_alloc_self_quar, c205_quar_alloc_to_scrap quar_alloc_to_scrap,
            c205_quar_alloc_to_inv quar_alloc_to_inv, c205_bulk_alloc_to_set bulk_alloc_to_set, c205_bulk_alloc_to_inv
            bulk_alloc_to_inv, inreturns rtn_hold_area, c205_inv_alloc_to_pack inv_alloc_to_pack
          , c205_inshelf inshelf, c205_shelfalloc shelfalloc, rmqty rmqty
          , c205_inquaran quaravail, c205_quaralloc quaralloc, DECODE(p_lock_type,20431,get_pi_pnum_pdhr_wip_qty(c205_part_number_id),c205_dhrqty) dhrqty
          , c205_tbe_alloc tbe_alloc, c205_inbulk inbulk, c205_instock instock
          , c205_bulkall bulkall, c205_stockall stockall, c205_packall packall
          , c205_quarall quarall, c205_bulkalloc bulkalloc
          , c205_rw_avail_qty rwavail, c205_rw_alloc_qty rwalloc
          ,c205_ih_avail_qty  ihavail,c205_ih_alloc_qty ihalloc
           FROM v205_qty_in_stock;
BEGIN
	 SELECT  NVL(GET_PLANTID_FRM_CNTX(),GET_RULE_VALUE('DEFAULT_PLANT','ONEPORTAL')), GET_COMPID_FRM_CNTX() 
	      INTO  v_plant_id, v_company_id
	      FROM DUAL;
		  
     DELETE FROM my_temp_part_list ;
     INSERT INTO my_temp_part_list
        (c205_part_number_id
        )
    SELECT t205c.c205_part_number_id pnum
           FROM t205c_part_qty t205c
          WHERE DECODE(p_lock_type,'4000564',1,NVL(t205c.c205_available_qty,'-9999')) >= DECODE(p_lock_type,'4000564',1,NVL(t205c.c205_available_qty,'0'))
             AND t205c.c901_transaction_type = '90800' AND t205c.C5040_PLANT_ID 	= 	v_plant_id
      UNION
	 SELECT c205_part_number_id pnum
	   FROM t2023_part_company_mapping
	  WHERE c1900_company_id                         = v_company_id
	    AND DECODE (p_lock_type, '4000564', 1, -1) >= DECODE (p_lock_type, '4000564', 1, 1)
	    AND c2023_void_fl                           IS NULL;
      --Modified the query for Cut Plan,as we need to have all part number irrespective of if its having stock.
    --
    -- load inventory info
    FOR set_val IN inshelf_cur
    LOOP
        -- Physical Audit Lock
        IF p_lock_type = 20431 THEN
            -- Save the value for Qty in Stock all(FG Available / In shelf + FG Allocated OUT )
            gm_pkg_op_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id, set_val.pnum, 20450,
            set_val.stockall) ;
            /*  -- Save the value Bulk Allocated (Pack) /Inventory Allocated (Pack) PI 2011
            gm_pkg_op_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id, set_val.pnum, 20458,
            set_val.inv_alloc_to_pack) ;*/
            /* -- Save the value for FG Allocated OUT PI 2011
            gm_pkg_op_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id, set_val.pnum, 20465,
            set_val.shelfalloc) ;*/
            -- Mihir Save the value for Raw Material
            gm_pkg_op_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id, set_val.pnum, 20466, set_val.rmqty);
            -- Save the value for Quarantine Allocated PI 2011
            /* gm_pkg_op_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id, set_val.pnum, 20454,
            set_val.quaralloc) ;*/
            -- Mihir Save the value for Quarantine All PI 2011  (Quarantine Available  +  Quarantine Allocated)
            gm_pkg_op_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id, set_val.pnum, 20455,
            set_val.quarall) ;
            -- MIhir Save the value DHR (Open DHR)
            gm_pkg_op_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id, set_val.pnum, 20461, set_val.dhrqty
            ) ;
            /*  -- Save the value for TBE
            -- issue 4479: Add the TBE quantity to Loose quantity and have it as Loose Quantity
            gm_pkg_op_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id
            , set_val.pnum
            , 20468
            , set_val.tbe_alloc
            );
            */
            -- Mihir Save the value for Loose   TBE/LOOSE(Bulk) PI 2011 Add bulkalloc (Bulk allocated)
            gm_pkg_op_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id, set_val.pnum, 20469, set_val.inbulk
            + set_val.tbe_alloc + set_val.bulkalloc) ;
            -- Mihir Save the value for Repack PI 2011
            gm_pkg_op_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id, set_val.pnum, 20481,
            set_val.packall) ;

            -- PI 2013  Restricted warehouse available + Restricted allocated
             gm_pkg_op_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id, set_val.pnum,4000114,
            set_val.rwavail+set_val.rwalloc) ;
            
            
	      -- PI 2013 In house warehouse inhouse available + inhouse allocated
             gm_pkg_op_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id, set_val.pnum,20482,
            set_val.ihavail+set_val.ihalloc) ;
            
            --PMT-14859 : PI module include the return hold area
            gm_pkg_op_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id, set_val.pnum, 20459,
            set_val.rtn_hold_area) ;
            
        ELSIF p_lock_type = 20430 THEN
            -- Save the value for FG Available / In shelf
            gm_pkg_op_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id, set_val.pnum, 20450,
            set_val.inshelf) ;
            -- Save the value Bulk Allocated (Pack)
            gm_pkg_op_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id, set_val.pnum, 20458,
            set_val.inv_alloc_to_pack) ;
            -- Save the value In shelf
            gm_pkg_op_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id, set_val.pnum, 20464,
            set_val.instock) ;
            -- Save the value Sales Allocated
            gm_pkg_op_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id, set_val.pnum, 20451,
            set_val.sales_allocated) ;
            -- Save the value Consignment Allocated
            gm_pkg_op_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id, set_val.pnum, 20452,
            set_val.cons_alloc) ;
            -- Save the value Quarantine Allocated (Self)
            gm_pkg_op_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id, set_val.pnum, 20453,
            set_val.quar_alloc_self_quar) ;
            -- Save the value Quarantine Allocated (Scrap)
            gm_pkg_op_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id, set_val.pnum, 20454,
            set_val.quaravail) ;
            -- Save the value 'Quarantine Allocated (Inventory)
            gm_pkg_op_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id, set_val.pnum, 20455,
            set_val.quaralloc) ;
            -- Save the value Bulk Allocated (Set)
            gm_pkg_op_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id, set_val.pnum, 20456,
            set_val.bulk_alloc_to_set) ;
            -- Save the value Bulk Allocated (Inventory)
            gm_pkg_op_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id, set_val.pnum, 20457,
            set_val.bulk_alloc_to_inv) ;
            -- Save return hold ared
            gm_pkg_op_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id, set_val.pnum, 20459,
            set_val.rtn_hold_area) ;
            --
          -- Cut Plan Load
         ELSIF p_lock_type = 4000564 THEN
         	--For Cut Plan we should take the FG Quantity + Qty in WIP [DHR].
         	gm_pkg_op_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id, set_val.pnum, 20450,
            												   set_val.inshelf + set_val.dhrqty) ;
            
        END IF;
    END LOOP;
END gm_op_ld_inshelf;
/*************************************************************************
* Purpose: Procedure used to load Open PO Info
*************************************************************************/
PROCEDURE gm_op_ld_openpo (
        p_inventory_lock_id IN t250_inventory_lock.c250_inventory_lock_id%TYPE)
AS
		v_plant_id     T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
    CURSOR po_cur
    IS
         SELECT t205.c205_part_number_id pnum, (NVL (opo.qty_order, 0) - NVL (dhrs.qty_received, 0)) open_po
           FROM t205_part_number t205, (
                 SELECT t402.c205_part_number_id, SUM (c402_qty_ordered * t405.c405_uom_qty) qty_order
                   FROM t402_work_order t402, t405_vendor_pricing_details t405
                  WHERE c402_status_fl       < 3
                    AND c402_void_fl        IS NULL
                    AND t405.c405_void_fl IS NULL
                    AND t402.c405_pricing_id = t405.c405_pricing_id
               GROUP BY t402.c205_part_number_id
            )
        opo, (
             SELECT t408.c205_part_number_id, NVL (SUM (t408.c408_qty_received), 0) - NVL (SUM (t408.c408_qty_rejected)
                , 0)                                                                + NVL (SUM (t409.c409_closeout_qty)
                , 0) qty_received
               FROM t408_dhr t408, t409_ncmr t409
              WHERE t408.c402_work_order_id IN
                (
                     SELECT c402_work_order_id FROM t402_work_order WHERE c402_status_fl < 3
                )
                AND t408.c408_void_fl IS NULL
                AND t409.c409_void_fl IS NULL
                AND t408.c408_dhr_id NOT LIKE 'GM-RC%' -- should skip RC DHR which is already verified (4)
                --AND t408.c402_work_order_id = t409.c402_work_order_id(+)
                AND t408.c408_dhr_id = t409.c408_dhr_id(+)
           GROUP BY t408.c205_part_number_id
        )
        dhrs
      WHERE t205.c205_part_number_id = opo.c205_part_number_id(+)
        AND t205.c205_part_number_id = dhrs.c205_part_number_id(+)
        AND EXISTS
    	(
         SELECT t205c.c205_part_number_id
           FROM t205c_part_qty t205c
          WHERE t205c.c205_part_number_id   = t205.c205_part_number_id
            AND t205c.c205_available_qty   IS NOT NULL
            AND t205c.c901_transaction_type = '90800' --FG qty
            AND t205c.C5040_PLANT_ID 	= 	v_plant_id
    	);
BEGIN
    --
    -- load inventory info
    SELECT  get_rule_value('DEFAULT_PLANT','ONEPORTAL') INTO v_plant_id FROM DUAL;
    FOR set_val IN po_cur
    LOOP
        --
        -- Save the value In shelf
        gm_pkg_op_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id, set_val.pnum, 20460, set_val.open_po) ;
        --
    END LOOP;
END gm_op_ld_openpo;
/*************************************************************************
* Purpose: Procedure used to load Open DHR Info
*************************************************************************/
PROCEDURE gm_op_ld_opendhr (
        p_inventory_lock_id IN t250_inventory_lock.c250_inventory_lock_id%TYPE)
AS
		v_plant_id     T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
    CURSOR opendhr_cur
    IS
         SELECT t205.c205_part_number_id pnum, NVL (dhrqty, 0) dhrqty
           FROM t205_part_number t205, (
                 SELECT t408.c205_part_number_id, NVL (SUM (t408.c408_qty_received), 0) - NVL (SUM (c408_qty_rejected),
                    0)                                                                  + NVL (SUM (
                    t409.c409_closeout_qty), 0) dhrqty
                   FROM t408_dhr t408, t409_ncmr t409
                  WHERE t408.c408_status_fl < 4
                    AND t408.c408_void_fl  IS NULL
                    AND t408.c408_dhr_id    = t409.c408_dhr_id(+)
                    AND t409.c409_void_fl  IS NULL
               GROUP BY t408.c205_part_number_id
            )
        dhrqty
      WHERE t205.c205_part_number_id = dhrqty.c205_part_number_id(+)
        AND EXISTS
    	(
         SELECT t205c.c205_part_number_id
           FROM t205c_part_qty t205c
          WHERE t205c.c205_part_number_id   = t205.c205_part_number_id
            AND t205c.c205_available_qty   IS NOT NULL
            AND t205c.c901_transaction_type = '90800' --FG qty
            AND t205c.C5040_PLANT_ID 	= 	v_plant_id
    	);
BEGIN
    --
    -- load inventory info
    SELECT  get_rule_value('DEFAULT_PLANT','ONEPORTAL') INTO v_plant_id FROM DUAL;
    FOR set_val IN opendhr_cur
    LOOP
        --
        -- Save the value In shelf
        gm_pkg_op_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id, set_val.pnum, 20461, set_val.dhrqty) ;
        --
    END LOOP;
END gm_op_ld_opendhr;
/*************************************************************************
* Purpose: Procedure used to load Open DHR and PO (mainly used for TTP load)
*************************************************************************/
PROCEDURE gm_op_ld_openpodhr (
        p_inventory_lock_id IN t250_inventory_lock.c250_inventory_lock_id%TYPE)
AS
    CURSOR openpodhr_cur
    IS
         SELECT b.c205_part_number_id pnum, SUM (c251_qty) podhrqty
           FROM t250_inventory_lock a, t251_inventory_lock_detail b
          WHERE a.c250_inventory_lock_id = p_inventory_lock_id
            AND a.c250_inventory_lock_id = b.c250_inventory_lock_id
            AND b.c901_type             IN (20460, 20461)
       GROUP BY b.c205_part_number_id;
BEGIN
    --
    -- load inventory info (PO DHR)
    FOR set_val IN openpodhr_cur
    LOOP
        --
        -- Save the value In shelf
        gm_pkg_op_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id, set_val.pnum, 20463, set_val.podhrqty)
        ;
        --
    END LOOP;
END gm_op_ld_openpodhr;
/*************************************************************************
* Purpose: Procedure used to load PAR information
*************************************************************************/
PROCEDURE gm_op_ld_par (
        p_inventory_lock_id IN t250_inventory_lock.c250_inventory_lock_id%TYPE)
AS
		v_plant_id     T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
    CURSOR openpar_cur
    IS
         SELECT c205_part_number_id pnum, c205_available_qty parqty, c901_transaction_type ttype
           FROM t205c_part_qty
          WHERE c901_transaction_type IN (40020, 40021, 40022) 
          AND C5040_PLANT_ID 	= 	v_plant_id;
BEGIN
    --
    -- load inventory info (PO DHR)
    SELECT  get_rule_value('DEFAULT_PLANT','ONEPORTAL') INTO v_plant_id FROM DUAL;
    FOR set_val IN openpar_cur
    LOOP
        --
        -- Save the value In shelf
        gm_pkg_op_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id, set_val.pnum, set_val.ttype,
        set_val.parqty) ;
        --
    END LOOP;
END gm_op_ld_par;
/*************************************************************************
* Purpose: Procedure used to load Build Set Qty
*************************************************************************/
PROCEDURE gm_op_ld_buildset (
        p_inventory_lock_id IN t250_inventory_lock.c250_inventory_lock_id%TYPE)
AS
		v_plant_id     T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
    CURSOR buildset_cur
    IS
         SELECT t205.c205_part_number_id pnum, NVL (build_set_qty, 0) build_set_qty
           FROM t205_part_number t205, (
                 SELECT b.c205_part_number_id, SUM (b.c505_item_qty) build_set_qty
                   FROM t504_consignment a, t505_item_consignment b
                  WHERE a.c504_consignment_id         = b.c504_consignment_id
                    AND a.c504_status_fl             IN (2, 3)
                    AND a.c207_set_id                IS NOT NULL
                    AND a.c504_verify_fl              = '1'
                    AND a.c504_ship_date             IS NULL
                    AND a.c504_void_fl               IS NULL
                    AND TRIM (b.c505_control_number) IS NOT NULL
               GROUP BY b.c205_part_number_id
            )
        cons
      WHERE t205.c205_part_number_id = cons.c205_part_number_id(+)
        AND EXISTS
    	(
         SELECT t205c.c205_part_number_id
           FROM t205c_part_qty t205c
          WHERE t205c.c205_part_number_id   = t205.c205_part_number_id
            AND t205c.c205_available_qty   IS NOT NULL
            AND t205c.c901_transaction_type = '90800' --FG qty
            AND t205c.C5040_PLANT_ID 	= 	v_plant_id
    	);
BEGIN
    --
    -- load inventory info
    SELECT  get_rule_value('DEFAULT_PLANT','ONEPORTAL') INTO v_plant_id FROM DUAL;
    FOR set_val IN buildset_cur
    LOOP
        --
        -- Save the value In shelf
        gm_pkg_op_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id, set_val.pnum, 20462,
        set_val.build_set_qty) ;
        --
    END LOOP;
END gm_op_ld_buildset;
/*************************************************************************
* Purpose: Procedure used to load Pending Requests
*************************************************************************/
PROCEDURE gm_op_ld_pending_request (
        p_inventory_lock_id IN t250_inventory_lock.c250_inventory_lock_id%TYPE,
        p_from_period       IN DATE)
AS
    v_demand_master_id t4020_demand_master.c4020_demand_master_id%TYPE;
    v_plant_id     	   T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
    --
    CURSOR prequest_calc_cur
    IS
         SELECT t205.c205_part_number_id p_num, NVL (SUM (temp.qty), 0) qty, NVL (DECODE (temp.sfl, 10, 50567, 15,
            50568, 20, 50569), 50567) code_id
           FROM
            (
                 SELECT t521.c205_part_number_id p_num, t521.c521_qty qty, DECODE (c520_status_fl, 30, 20,
                    c520_status_fl) sfl
                   FROM t520_request t520, t521_request_detail t521
                  WHERE t520.c520_request_id      = t521.c520_request_id
                    AND t520.c520_void_fl        IS NULL
                    AND t520.c520_delete_fl      IS NULL
                    AND t520.c520_status_fl       > 0
                    AND t520.c520_status_fl       < 40
                    AND t520.c901_request_source <> 50617
                    AND t520.c520_required_date   < p_from_period
          UNION ALL
             SELECT t505.c205_part_number_id p_num, t505.c505_item_qty qty, DECODE (c520_status_fl, 30, 20,
                c520_status_fl) sfl
               FROM t520_request t520, t504_consignment t504, t505_item_consignment t505
              WHERE t520.c520_request_id      = t504.c520_request_id
                AND t504.c504_consignment_id  = t505.c504_consignment_id
                AND t520.c520_void_fl        IS NULL
                AND t520.c520_delete_fl      IS NULL
                AND t520.c520_status_fl       > 0
                AND t520.c520_status_fl       < 40
                AND t520.c901_request_source <> 50617
                AND t520.c520_required_date   < p_from_period
                AND t504.c504_void_fl        IS NULL
          UNION ALL
             SELECT t502.c205_part_number_id p_num, t502.c502_item_qty qty, 10 sfl
               FROM t501_order t501, t502_item_order t502
              WHERE t501.c501_order_id                = t502.c501_order_id
                AND t501.c501_status_fl               = 0
                AND NVL (t501.c901_order_type, - 999) = 2525
                AND t501.c501_void_fl                IS NULL
				AND (t501.c901_ext_country_id IS NULL OR t501.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH
          UNION ALL
         -- Get the Backorder Loaner transaction.
         	select c205_part_number_id pnum, t413.c413_item_qty qty,10 sfl
         	from t412_inhouse_transactions t412, t413_inhouse_trans_items t413
			where c412_type IN (100062, 1006572)  -- PL Back Order, IH Back Order 
			and t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
			and c412_status_fl='0'  -- Back Order
			--and c412_last_updated_date is not null
			and c412_void_fl is null			
			and t413.C413_VOID_FL is null
			and c901_country_id is null -- To get only US Back Order Loaner Transaction.  
			)
            temp, t205_part_number t205
          WHERE t205.c205_part_number_id = temp.p_num(+)
            AND EXISTS
    	(
         SELECT t205c.c205_part_number_id
           FROM t205c_part_qty t205c
          WHERE t205c.c205_part_number_id   = t205.c205_part_number_id
            AND t205c.c205_available_qty   IS NOT NULL
            AND t205c.c901_transaction_type = '90800' --FG qty
            AND t205c.C5040_PLANT_ID 	= 	v_plant_id
    	)          
       GROUP BY t205.c205_part_number_id, temp.sfl;
    BEGIN
        --
        -- load pending request information
        SELECT  get_rule_value('DEFAULT_PLANT','ONEPORTAL') INTO v_plant_id FROM DUAL;
        FOR set_val IN prequest_calc_cur
        LOOP
            --
            -- Save the value In shelf
            gm_pkg_op_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id, set_val.p_num, set_val.code_id,
            set_val.qty) ;
            --
        END LOOP;
        -- load associated record for further verification
        gm_pkg_op_ld_inventory.gm_op_ld_pending_req_detail (p_inventory_lock_id) ;
        --
    END gm_op_ld_pending_request;
    /*************************************************************************
    * Purpose: Procedure used to load Pending Requests detail
    * Will lock open request, used for further reference
    *************************************************************************/
PROCEDURE gm_op_ld_pending_req_detail (
        p_inventory_lock_id IN t250_inventory_lock.c250_inventory_lock_id%TYPE)
AS
BEGIN
     INSERT
       INTO t253c_request_lock
        (
            c253c_request_lock_id, c250_inventory_lock_id, c520_request_id
          , c520_request_date, c520_required_date, c901_request_source
          , c520_request_txn_id, c207_set_id, c520_request_for
          , c520_request_to, c901_request_by_type, c520_request_by
          , c901_purpose, c520_master_request_id, c520_status_fl
          , c901_ship_to, c520_ship_to_id
        )
     SELECT s253c_request_lock.NEXTVAL, p_inventory_lock_id, c520_request_id
      , c520_request_date, c520_required_date, c901_request_source
      , c520_request_txn_id, c207_set_id, c520_request_for
      , c520_request_to, c901_request_by_type, c520_request_by
      , c901_purpose, c520_master_request_id, c520_status_fl
      , c901_ship_to, c520_ship_to_id
       FROM t520_request t520
      WHERE t520.c520_void_fl   IS NULL
        AND t520.c520_delete_fl IS NULL
        AND t520.c520_status_fl  < 40;
     INSERT
       INTO t253d_request_detail_lock
        (
            c253d_request_detail_lock_id, c250_inventory_lock_id, c520_request_id
          , c205_part_number_id, c521_qty
        )
     SELECT s253d_request_detail_lock.NEXTVAL, p_inventory_lock_id, t521.c520_request_id
      , t521.c205_part_number_id, t521.c521_qty
       FROM t520_request t520, t521_request_detail t521
      WHERE t520.c520_void_fl   IS NULL
        AND t520.c520_delete_fl IS NULL
        AND t520.c520_status_fl  < 40
        AND t520.c520_request_id = t521.c520_request_id;
    --
     INSERT
       INTO t253a_consignment_lock
        (
            c253a_consignment_lock_id, c250_inventory_lock_id, c504_consignment_id
          , c701_distributor_id, c504_status_fl, c504_void_fl
          , c704_account_id, c207_set_id, c504_type
          , c504_verify_fl, c504_verified_date, c504_verified_by
          , c520_request_id, c504_master_consignment_id
        )
     SELECT s253a_consignment_lock.NEXTVAL, p_inventory_lock_id, t504.c504_consignment_id
      , t504.c701_distributor_id, t504.c504_status_fl, t504.c504_void_fl
      , t504.c704_account_id, t504.c207_set_id, t504.c504_type
      , t504.c504_verify_fl, t504.c504_verified_date, t504.c504_verified_by
      , t504.c520_request_id, t504.c504_master_consignment_id
       FROM t520_request t520, t504_consignment t504
      WHERE t520.c520_void_fl   IS NULL
        AND t520.c520_delete_fl IS NULL
        AND t520.c520_status_fl  < 40
        AND t520.c520_request_id = t504.c520_request_id;
    --
     INSERT
       INTO t253b_item_consignment_lock
        (
            c253b_item_consignment_lock_id, c250_inventory_lock_id, c504_consignment_id
          , c205_part_number_id, c505_item_qty
        )
     SELECT s253b_item_consignment_lock.NEXTVAL, p_inventory_lock_id, t505.c504_consignment_id
      , c205_part_number_id, c505_item_qty
       FROM t520_request t520, t504_consignment t504, t505_item_consignment t505
      WHERE t520.c520_void_fl       IS NULL
        AND t520.c520_delete_fl     IS NULL
        AND t520.c520_status_fl      < 40
        AND t520.c520_request_id     = t504.c520_request_id
        AND t504.c504_consignment_id = t505.c504_consignment_id;
    --
     INSERT
       INTO t253e_order_lock
        (
            c253e_order_lock_id, c250_inventory_lock_id, c501_order_id
          , c501_status_fl, c501_void_fl, c501_last_updated_by
          , c501_last_updated_date, c901_order_type, c501_parent_order_id
          , c501_total_cost, c501_ship_to, c501_ship_to_id
          , c501_ship_cost, c501_update_inv_fl, c503_invoice_id
          , c506_rma_id, c501_customer_po, c704_account_id
          , c501_order_date, c703_sales_rep_id, c501_shipping_date
          , c501_order_date_time, c1900_company_id, c5040_plant_id
        )
     SELECT s253e_order_lock.NEXTVAL, p_inventory_lock_id, c501_order_id
      , c501_status_fl, c501_void_fl, c501_last_updated_by
      , c501_last_updated_date, c901_order_type, c501_parent_order_id
      , c501_total_cost, c501_ship_to, c501_ship_to_id
      , c501_ship_cost, c501_update_inv_fl, c503_invoice_id
      , c506_rma_id, c501_customer_po, c704_account_id
      , c501_order_date, c703_sales_rep_id, c501_shipping_date
      , c501_order_date_time, c1900_company_id, c5040_plant_id
       FROM t501_order t501
      WHERE t501.c501_status_fl               = 0
        AND t501.c501_void_fl                IS NULL
		AND (t501.c901_ext_country_id IS NULL OR t501.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH
        AND NVL (t501.c901_order_type, - 999) = 2525;
    --
     INSERT
       INTO t253f_item_order_lock
        (
            c253f_item_order_lock_id, c250_inventory_lock_id, c501_order_id
          , c205_part_number_id, c502_item_qty, c502_control_number
          , c502_item_price, c502_void_fl, c502_item_seq_no
          , c901_type, c502_construct_fl
        )
     SELECT s253f_item_order_lock.NEXTVAL, p_inventory_lock_id, t502.c501_order_id
      , c205_part_number_id, c502_item_qty, c502_control_number
      , c502_item_price, c502_void_fl, c502_item_seq_no
      , c901_type, c502_construct_fl
       FROM t501_order t501, t502_item_order t502
      WHERE t501.c501_order_id                = t502.c501_order_id
        AND t501.c501_status_fl               = 0
        AND t501.c501_void_fl                IS NULL
		AND (t501.c901_ext_country_id IS NULL OR t501.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH
        AND NVL (t501.c901_order_type, - 999) = 2525;
     
     -- LOAD BACK ORDER LOANER TRANSACTION DETAILS>
	     INSERT INTO t253g_inhouse_transaction_lock (
	C253g_inhouse_trans_lock_id  ,C250_INVENTORY_LOCK_ID,
	       c412_inhouse_trans_id ,c412_status_fl, 
	       c412_void_fl ,c412_type,   
	       c412_update_inv_fl , c412_verified_date ,
	       c412_verified_by  , c412_verify_fl   ,
	       c412_last_updated_date,c412_last_updated_by ,
	       c901_country_id,c412_ref_id )
	SELECT s253g_inhouse_transaction_lock.nextval,p_inventory_lock_id,
	       c412_inhouse_trans_id ,c412_status_fl ,
	       c412_void_fl ,c412_type  , 
	       c412_update_inv_fl , c412_verified_date ,
	       c412_verified_by  , c412_verify_fl   ,
	       c412_last_updated_date,c412_last_updated_by ,
	       c901_country_id,c412_ref_id
	  FROM t412_inhouse_transactions
		 where c412_type IN (100062, 1006572)  -- PL Back Order , IH Back Order 
		and c412_status_fl='0'  -- Back Order
		and c412_void_fl is null			
		and c901_country_id is null;
	
	INSERT INTO t253h_inhouse_trans_items_lock (	
	c253h_inh_trans_item_lck_id ,C412_INHOUSE_TRANS_ID 
	,C250_INVENTORY_LOCK_ID,C205_PART_NUMBER_ID ,
	C413_ITEM_QTY , C413_STATUS_FL,C413_VOID_FL )
	SELECT s253h_inh_trans_items_lock.NEXTVAL,t412.C412_INHOUSE_TRANS_ID
	       ,p_inventory_lock_id,t413.C205_PART_NUMBER_ID
	       ,C413_ITEM_QTY , C413_STATUS_FL,C413_VOID_FL 
	     FROM t412_inhouse_transactions t412, t413_inhouse_trans_items t413
	     where t412.c412_type IN (100062, 1006572)  -- PL Back Order , IH Back Order  
	     and t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
	     and t412.c412_status_fl='0'  -- Back Order
	     and t412.c412_void_fl is null			
	     and t413.c413_void_fl is null
	     and t412.c901_country_id is null ;
	     
   
END gm_op_ld_pending_req_detail;
/***************************************************************
* Purpose: Procedure used to load inhouse loaners
***************************************************************/
PROCEDURE gm_op_ld_loaners (
        p_inventory_lock_id IN t250_inventory_lock.c250_inventory_lock_id%TYPE)
AS
	v_company_id  t1900_company.c1900_company_id%TYPE;
	v_plant_id    t5040_plant_master.c5040_plant_id%TYPE;

    CURSOR inhouse_loaner_details_inv
    IS
         SELECT t505.c205_part_number_id pnum, DECODE (t504.c504_inhouse_purpose, 40000, 20470 -- Inhouse Training
            , 40001, 20473 -- Inhouse Display (
            -- Training)
            , 40004, 20471 -- Inhouse Cad Lab
            , 40005, 20472 -- Inhouse MERC
            , 40002, 20475 -- Product Room (Inhouse Display)
            , 40003, 20476 -- External Cad Lab (Inhouse Display)
            ) location_type, SUM (t505.c505_item_qty) qty
           FROM t504_consignment t504, t504a_consignment_loaner t504a, t505_item_consignment t505
          WHERE t504.c504_consignment_id   = t505.c504_consignment_id
            AND t504.c504_status_fl        = '4'
            AND t504.c504_verify_fl        = '1'
            AND t504.c504_void_fl         IS NULL
            AND t504.c207_set_id          IS NOT NULL
            AND t504.c504_type             = 4119 -- inhouse loaner
            AND t504.c504_inhouse_purpose IN (40000, 40001, 40002, 40003, 40004, 40005) -- Training Set, Display
            -- Set, Internal Cad lab, MERC
            AND t504a.c504a_status_fl NOT       IN ('20', '60','22') -- Only Skip Pending return , inactive, missing
            AND t505.c505_void_fl               IS NULL
            AND TRIM (t505.c505_control_number) IS NOT NULL
            AND t504.c504_consignment_id         = t504a.c504_consignment_id(+)
            AND t504.c5040_plant_id=v_plant_id
       GROUP BY t505.c205_part_number_id, t504.c504_inhouse_purpose;
    CURSOR sales_loaner_details_inv
    IS
         SELECT cons.pnum pnum, 20474 location_type, NVL ( (NVL (cons.qty, 0) - NVL (returna.qty, 0)), 0) qty
           FROM
            (
                 SELECT t505.c205_part_number_id pnum, 20474 location_type, SUM (t505.c505_item_qty) qty
                   FROM t504_consignment t504, t504a_consignment_loaner t504a, t505_item_consignment t505
                  WHERE t504.c504_consignment_id         = t505.c504_consignment_id
                    AND t504.c504_status_fl              = '4'
                    AND t504.c504_verify_fl              = '1'
                    AND t504.c504_void_fl               IS NULL
                    AND t504.c207_set_id                IS NOT NULL
                    AND t504.c504_type                   = 4127 -- sales loaner
                    AND t504a.c504a_status_fl NOT       IN ('20', '60','22') -- Only Skip Pending return , inactive, missing
                    AND t505.c505_void_fl               IS NULL
                    AND TRIM (t505.c505_control_number) IS NOT NULL
                    AND t504.c504_consignment_id         = t504a.c504_consignment_id(+)
					AND t504.c5040_plant_id=v_plant_id
               GROUP BY t505.c205_part_number_id --, t504.c504_inhouse_purpose
            )
        cons, (
             SELECT t413.c205_part_number_id, SUM (DECODE (t413.c413_status_fl, 'Q', t413.c413_item_qty, 0)) qty, SUM (
                DECODE (NVL (t413.c413_status_fl, 'Q'), 'Q', 0, t413.c413_item_qty)) qtyrecon
               FROM t412_inhouse_transactions t412, t413_inhouse_trans_items t413
              WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
                AND t412.c412_ref_id          IN
                (
                     SELECT t504.c504_consignment_id
                       FROM t504_consignment t504, t504a_consignment_loaner t504a
                      WHERE t504.c504_status_fl        = '4'
                        AND t504.c504_verify_fl        = '1'
                        AND t504.c504_void_fl         IS NULL
                        AND t504.c207_set_id          IS NOT NULL
                        AND t504.c504_type             = 4127 -- sales loaner
                        AND t504a.c504a_status_fl NOT IN ('20', '60','22') -- Only Skip Pending return , inactive, missing
                        -- loaner
                        AND t504.c504_consignment_id = t504a.c504_consignment_id(+)
						AND t504.c5040_plant_id=v_plant_id
                )
                AND t412.c412_void_fl IS NULL
                AND t413.c413_void_fl IS NULL
				AND t412.c5040_plant_id=v_plant_id
           GROUP BY t413.c205_part_number_id
        )
        returna
      WHERE cons.pnum = returna.c205_part_number_id(+);
    CURSOR inhouse_consignment_lock_cur
    IS
			SELECT lockinv.c504_consignment_id, lockinv.c701_distributor_id, lockinv.c504_status_fl
			  , lockinv.c504_void_fl, lockinv.c704_account_id, lockinv.c207_set_id
			  , lockinv.c504_type, lockinv.c504_verify_fl, lockinv.c504_verified_date
			  , lockinv.c504_verified_by, lockinv.c520_request_id, lockinv.c504_master_consignment_id
			  , lockinv.c504_last_updated_by, lockinv.c504_last_updated_date, lockinv.location_type
			  , NVL(taggedcns.invtagid,get_tagid_from_txn_id(lockinv.c504_consignment_id)) invtagid, taggedcns.invlocid invlocid	 
		   FROM
			(
				 SELECT t504.c504_consignment_id, t504.c701_distributor_id, t504.c504_status_fl
				  , t504.c504_void_fl, t504.c704_account_id, t504.c207_set_id
				  , t504.c504_type, t504.c504_verify_fl, t504.c504_verified_date
				  , t504.c504_verified_by, t504.c520_request_id, t504.c504_master_consignment_id
				  , t504.c504_last_updated_by, t504.c504_last_updated_date, DECODE (t504.c504_inhouse_purpose, 40000, 20470,
					40001, 20473, 40004, 20471, 40005, 20472, 40002, 20475, 40003, 20476 -- External Cad Lab (
					-- Inhouse Display)
					) location_type
				   FROM t504_consignment t504, t504a_consignment_loaner t504a
				  WHERE t504.c504_consignment_id   = t504a.c504_consignment_id
					AND t504.c504_status_fl        = '4'
					AND t504.c504_verify_fl        = '1'
					AND t504.c504_void_fl         IS NULL
					AND t504.c207_set_id          IS NOT NULL
					AND t504.c504_type             = 4119 -- inhouse loaner
					AND t504.c504_inhouse_purpose IN (40000, 40001, 40002, 40003, 40004, 40005) -- Training Set, Display
					-- Set, Internal Cad lab, MERC
					AND t504a.c504a_status_fl NOT IN ('20', '60', '22') -- Only Skip Pending return , inactive, missing
					AND t504.c504_consignment_id   = t504a.c504_consignment_id(+)
					AND t504.c5040_plant_id=v_plant_id
			)
			lockinv, (
				 SELECT t5010.c5010_last_updated_trans_id lastupdtransid, T5053.c5010_tag_id invtagid, t5053.c5052_location_id
					invlocid
				   FROM t5010_tag t5010, t5051_inv_warehouse t5051, t5052_location_master t5052
				  , t5053_location_part_mapping T5053
				  WHERE t5010.c5010_tag_id           = t5053.c5010_tag_id
					AND t5051.c5051_inv_warehouse_id = t5052.c5051_inv_warehouse_id
					AND t5052.c5052_location_id      = t5053.c5052_location_id
					AND T5052.c901_location_type    IN (93503) --In House Loaner
					AND t5052.c5052_void_fl         IS NULL
					AND t5010.c5010_void_fl      IS NULL
					AND t5051.c901_status_id      = 1
					AND t5051.c901_warehouse_type = 56013 --Inhouse Warehouse
					AND t5052.c5040_plant_id=v_plant_id
			)
			taggedcns
		  WHERE lockinv.c504_consignment_id = taggedcns.lastupdtransid(+);
    CURSOR sales_consignment_lock_cur
    IS
			SELECT lockinv.c504_consignment_id, lockinv.c701_distributor_id, lockinv.c504_status_fl
		  , lockinv.c504_void_fl, lockinv.c704_account_id, lockinv.c207_set_id
		  , lockinv.c504_type, lockinv.c504_verify_fl, lockinv.c504_verified_date
		  , lockinv.c504_verified_by, lockinv.c520_request_id, lockinv.c504_master_consignment_id
		  , lockinv.c504_last_updated_by, lockinv.c504_last_updated_date, 20474 location_type
		  , NVL(taggedcns.invtagid,get_tagid_from_txn_id(lockinv.c504_consignment_id)) invtagid, taggedcns.invlocid invlocid
		   FROM
			(
				 SELECT t504.c504_consignment_id, t504.c701_distributor_id, t504.c504_status_fl
				  , t504.c504_void_fl, t504.c704_account_id, t504.c207_set_id
				  , t504.c504_type, t504.c504_verify_fl, t504.c504_verified_date
				  , t504.c504_verified_by, t504.c520_request_id, t504.c504_master_consignment_id
				  , T504.c504_last_updated_by, T504.c504_last_updated_date
				   FROM t504_consignment t504, t504a_consignment_loaner t504a
				  WHERE t504.c504_status_fl        = '4'
					AND t504.c504_verify_fl        = '1'
					AND t504.c504_void_fl         IS NULL
					AND t504.c207_set_id          IS NOT NULL
					AND t504.c504_type             = 4127 -- sales loaner
					AND t504a.c504a_status_fl NOT IN ('20', '60', '22') -- Only Skip Pending return , inactive, missing
					AND t504.c504_consignment_id   = t504a.c504_consignment_id(+)
					AND t504.c5040_plant_id=v_plant_id
			)
			lockinv, (
				 SELECT t5010.c5010_last_updated_trans_id lastupdtransid, T5053.c5010_tag_id invtagid, t5053.c5052_location_id
					invlocid
				   FROM t5010_tag t5010, t5051_inv_warehouse t5051, t5052_location_master t5052
				  , t5053_location_part_mapping T5053
				  WHERE t5010.c5010_tag_id           = t5053.c5010_tag_id
					AND t5051.c5051_inv_warehouse_id = t5052.c5051_inv_warehouse_id
					AND t5052.c5052_location_id      = t5053.c5052_location_id
					AND T5052.c901_location_type    IN (93502) --Product Loaner
					AND t5052.c5052_void_fl         IS NULL
					AND t5010.c5010_void_fl      IS NULL
					AND t5051.c901_status_id      = 1
					AND t5051.c901_warehouse_type = 90800 --FG Warehouse
					AND t5052.c5040_plant_id=v_plant_id
			)
			taggedcns
		  WHERE lockinv.c504_consignment_id = taggedcns.lastupdtransid(+);
    CURSOR inhouse_cons_lock_cur_details
    IS
         SELECT t505.c205_part_number_id pnum, t504.c504_consignment_id, SUM (t505.c505_item_qty) qty
           FROM t504_consignment t504, t504a_consignment_loaner t504a, t505_item_consignment t505
          WHERE t504.c504_consignment_id   = t505.c504_consignment_id
            AND t504.c504_status_fl        = '4'
            AND t504.c504_verify_fl        = '1'
            AND t504.c504_void_fl         IS NULL
            AND t504.c207_set_id          IS NOT NULL
            AND t504.c504_type             = 4119 -- inhouse loaner
            AND t504.c504_inhouse_purpose IN (40000, 40001, 40002, 40003, 40004, 40005) -- Training Set, Display
            -- Set, Internal Cad lab, MERC
            AND t504a.c504a_status_fl NOT       IN ('20', '60','22') -- Only Skip Pending return , inactive, missing
            AND t505.c505_void_fl               IS NULL
            AND TRIM (t505.c505_control_number) IS NOT NULL
            AND t504.c504_consignment_id         = t504a.c504_consignment_id(+)
			AND t504.c5040_plant_id=v_plant_id
       GROUP BY t505.c205_part_number_id, t504.c504_consignment_id;
    CURSOR sales_cons_lock_cur_details
    IS
         SELECT cons.pnum pnum, cons.c504_consignment_id, NVL ( (NVL (cons.qty, 0) - NVL (returna.qty, 0)), 0) qty
           FROM
            (
                 SELECT t505.c205_part_number_id pnum, t504.c504_consignment_id, SUM (t505.c505_item_qty) qty
                   FROM t504_consignment t504, t504a_consignment_loaner t504a, t505_item_consignment t505
                  WHERE t504.c504_consignment_id         = t505.c504_consignment_id
                    AND t504.c504_status_fl              = '4'
                    AND t504.c504_verify_fl              = '1'
                    AND t504.c504_void_fl               IS NULL
                    AND t504.c207_set_id                IS NOT NULL
                    AND t504.c504_type                   = 4127 -- sales loaner
                    AND t504a.c504a_status_fl NOT       IN ('20', '60','22') -- Only Skip Pending return , inactive, missing
                    AND t505.c505_void_fl               IS NULL
                    AND TRIM (t505.c505_control_number) IS NOT NULL
                    AND t504.c504_consignment_id         = t504a.c504_consignment_id(+)
					AND t504.c5040_plant_id=v_plant_id
               GROUP BY t505.c205_part_number_id, t504.c504_consignment_id
            )
        cons, (
             SELECT t413.c205_part_number_id, t412.c412_ref_id refid, SUM (DECODE (t413.c413_status_fl, 'Q',
                t413.c413_item_qty, 0)) qty, SUM (DECODE (NVL (t413.c413_status_fl, 'Q'), 'Q', 0, t413.c413_item_qty))
                qtyrecon
               FROM t412_inhouse_transactions t412, t413_inhouse_trans_items t413
              WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
                AND t412.c412_ref_id          IN
                (
                     SELECT t504.c504_consignment_id
                       FROM t504_consignment t504, t504a_consignment_loaner t504a
                      WHERE t504.c504_status_fl        = '4'
                        AND t504.c504_verify_fl        = '1'
                        AND t504.c504_void_fl         IS NULL
                        AND t504.c207_set_id          IS NOT NULL
                        AND t504.c504_type             = 4127 -- sales loaner
                        AND t504a.c504a_status_fl NOT IN ('20', '60','22') -- Only Skip Pending return , inactive, missing
                        AND t504.c504_consignment_id   = t504a.c504_consignment_id(+)
						AND t504.c5040_plant_id=v_plant_id
                )
                AND t412.c412_void_fl IS NULL
                AND t413.c413_void_fl IS NULL
				AND t412.c5040_plant_id=v_plant_id
           GROUP BY t413.c205_part_number_id, t412.c412_ref_id
        )
        returna
      WHERE cons.pnum                = returna.c205_part_number_id(+)
        AND cons.c504_consignment_id = returna.refid(+);
BEGIN
	SELECT GET_COMPID_FRM_CNTX(), GET_PLANTID_FRM_CNTX()
	      INTO v_company_id, v_plant_id
	      FROM DUAL;
	
    /* Following Cursor loads all the parts for inhouse loaners  in to Inventory lock details table */
    FOR current_row IN inhouse_loaner_details_inv
    LOOP
        gm_pkg_op_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id, current_row.pnum,
        current_row.location_type, current_row.qty) ;
    END LOOP;
    /* Following Cursor loads all the parts for sales loaners  in to Inventory lock details table */
    FOR current_row IN sales_loaner_details_inv
    LOOP
        gm_pkg_op_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id, current_row.pnum,
        current_row.location_type, current_row.qty) ;
    END LOOP;
    /* Following Cursor loads all the consignments for inhouse loaners in to Consignment Lock table */
    FOR current_row IN inhouse_consignment_lock_cur
    LOOP
        gm_pkg_op_ld_inventory.gm_op_sav_cons_lock (p_inventory_lock_id, current_row.c504_consignment_id,
        current_row.c701_distributor_id, current_row.c504_status_fl, current_row.c504_void_fl,
        current_row.c704_account_id, current_row.c207_set_id, current_row.c504_type, current_row.c504_verify_fl,
        current_row.c504_verified_date, current_row.c504_verified_by, current_row.c520_request_id,
        current_row.c504_master_consignment_id, current_row.c504_last_updated_by, current_row.c504_last_updated_date,
        current_row.location_type, '', '', NULL,current_row.invtagid,current_row.invlocid) ;
    END LOOP;
    /* Following Cursor loads all the consignments for sales loaners in to Consignment Lock table */
    FOR current_row IN sales_consignment_lock_cur
    LOOP
        gm_pkg_op_ld_inventory.gm_op_sav_cons_lock (p_inventory_lock_id, current_row.c504_consignment_id,
        current_row.c701_distributor_id, current_row.c504_status_fl, current_row.c504_void_fl,
        current_row.c704_account_id, current_row.c207_set_id, current_row.c504_type, current_row.c504_verify_fl,
        current_row.c504_verified_date, current_row.c504_verified_by, current_row.c520_request_id,
        current_row.c504_master_consignment_id, current_row.c504_last_updated_by, current_row.c504_last_updated_date,
        current_row.location_type, '', '', NULL,current_row.invtagid,current_row.invlocid) ;
    END LOOP;
    /* Following Cursor loads all the consignments for inhouse loaners in to Consignment Lock Details table */
    FOR current_row IN inhouse_cons_lock_cur_details
    LOOP
        gm_pkg_op_ld_inventory.gm_op_sav_cons_lock_detail (p_inventory_lock_id, current_row.pnum,
        current_row.c504_consignment_id, current_row.qty) ;
    END LOOP;
    /* Following Cursor loads all the consignments for sales loaners in to Consignment Lock Details table */
    FOR current_row IN sales_cons_lock_cur_details
    LOOP
        gm_pkg_op_ld_inventory.gm_op_sav_cons_lock_detail (p_inventory_lock_id, current_row.pnum,
        current_row.c504_consignment_id, current_row.qty) ;
    END LOOP;
END gm_op_ld_loaners;
/***************************************************************
* Purpose: Procedure used to load consignments
***************************************************************/
PROCEDURE gm_op_ld_consignments (
        p_inventory_lock_id IN t250_inventory_lock.c250_inventory_lock_id%TYPE)
AS
	v_company_id  t1900_company.c1900_company_id%TYPE;
	v_plant_id    t5040_plant_master.c5040_plant_id%TYPE;
    /* To load the t253a_consiignment_lock table with Built Set Consignments */
    CURSOR builtset_cons_lock_cur
    IS
			SELECT bsinv.c504_consignment_id, bsinv.c701_distributor_id, bsinv.c504_status_fl
		  , bsinv.c504_void_fl, bsinv.c704_account_id, bsinv.c207_set_id
		  , bsinv.c504_type, bsinv.c504_verify_fl, bsinv.c504_verified_date
		  , bsinv.c504_verified_by, bsinv.c520_request_id, bsinv.c504_master_consignment_id
		  , bsinv.c504_last_updated_by, bsinv.c504_last_updated_date, 20462 location_type
		  , NVL(taggedcns.invtagid,get_tagid_from_txn_id(bsinv.c504_consignment_id)) invtagid, taggedcns.invlocid invlocid
		   FROM
			(
				 SELECT t504.c504_consignment_id, t504.c701_distributor_id, t504.c504_status_fl
				  , t504.c504_void_fl, t504.c704_account_id, t504.c207_set_id
				  , t504.c504_type, t504.c504_verify_fl, t504.c504_verified_date
				  , t504.c504_verified_by, t504.c520_request_id, t504.c504_master_consignment_id
				  , t504.c504_last_updated_by, t504.c504_last_updated_date
				   FROM t504_consignment t504, t906_rules t906
				  WHERE t504.c504_status_fl = t906.c906_rule_value --            IN ('2','2.20','3','3.30','3.60' ) -- 2 Built
					-- Set  /2.20 Pending Pick  /3 Pending Shipping  /3.30 Packing In Progress /3.60 Ready for Pickup
					AND t504.c504_verify_fl   = '1'
					AND t504.c504_void_fl    IS NULL
					AND t504.c207_set_id     IS NOT NULL
					AND t906.c906_rule_id     = 'PI_BUILTSETSTATUS'
					AND t906.c906_rule_grp_id = 'PI_LOCK'
					AND t906.c906_void_fl    IS NULL
					AND t504.c5040_plant_id=v_plant_id
			)
			bsinv, (
				 SELECT T5010.c5010_last_updated_trans_id lastupdtransid, T5053.C5010_TAG_ID invtagid, t5053.c5052_location_id
					invlocid
				   FROM t5010_tag t5010, t5051_inv_warehouse t5051, t5052_location_master t5052
				  , t5053_location_part_mapping T5053
				  WHERE t5010.c5010_tag_id           = t5053.c5010_tag_id
					AND t5051.c5051_inv_warehouse_id = t5052.c5051_inv_warehouse_id
					AND t5052.c5052_location_id      = t5053.c5052_location_id
					AND T5052.c901_location_type    IN (93501) --Consignment
					AND t5052.c5052_void_fl         IS NULL
					AND t5010.c5010_void_fl         IS NULL
					AND t5051.c901_status_id         = 1
					AND t5051.c901_warehouse_type    = 90800 --FG Warehouse
					AND t5052.c5040_plant_id=v_plant_id
			)
			taggedcns
		  WHERE bsinv.c504_consignment_id = taggedcns.lastupdtransid(+) ;
    /* To load the t253a_consignment_lock table with WIP Set Consignments */
    CURSOR wipset_cons_lock_cur
    IS
		SELECT wipinv.c504_consignment_id, wipinv.c701_distributor_id, wipinv.c504_status_fl
		  , wipinv.c504_void_fl, wipinv.c704_account_id, wipinv.c207_set_id
		  , wipinv.c504_type, wipinv.c504_verify_fl, wipinv.c504_verified_date
		  , wipinv.c504_verified_by, wipinv.c520_request_id, wipinv.c504_master_consignment_id
		  , wipinv.c504_last_updated_by, wipinv.c504_last_updated_date, 20467 location_type
		  , NVL(taggedcns.invtagid,get_tagid_from_txn_id(wipinv.c504_consignment_id)) invtagid, taggedcns.invlocid invlocid
		   FROM
			(
				 SELECT t504.c504_consignment_id, t504.c701_distributor_id, t504.c504_status_fl
				  , t504.c504_void_fl, t504.c704_account_id, t504.c207_set_id
				  , t504.c504_type, t504.c504_verify_fl, t504.c504_verified_date
				  , t504.c504_verified_by, t504.c520_request_id, t504.c504_master_consignment_id
				  , t504.c504_last_updated_by, t504.c504_last_updated_date
				   FROM t504_consignment t504, t906_rules t906
				  WHERE t504.c504_status_fl = t906.c906_rule_value --IN ('1','1.10','1.20') -- 1 WIP status /1.10 Completed Set
					-- status/1.20 Pending Put status
					AND t504.c504_verify_fl   = '0'
					AND t504.c504_void_fl    IS NULL
					AND t504.c207_set_id     IS NOT NULL
					AND t906.c906_rule_id     = 'PI_WIPSETSTATUS'
					AND t906.c906_rule_grp_id = 'PI_LOCK'
					AND t906.c906_void_fl    IS NULL
					AND t504.c5040_plant_id=v_plant_id
			)
			wipinv, (
				 SELECT T5010.c5010_last_updated_trans_id lastupdtransid, T5053.C5010_TAG_ID invtagid, t5053.c5052_location_id
					invlocid
				   FROM t5010_tag t5010, t5051_inv_warehouse t5051, t5052_location_master t5052
				  , t5053_location_part_mapping T5053
				  WHERE t5010.c5010_tag_id           = t5053.c5010_tag_id
					AND t5051.c5051_inv_warehouse_id = t5052.c5051_inv_warehouse_id
					AND t5052.c5052_location_id      = t5053.c5052_location_id
					AND T5052.c901_location_type    IN (93501) --Consignment
					AND t5052.c5052_void_fl         IS NULL
					AND t5010.c5010_void_fl         IS NULL
					AND t5051.c901_status_id         = 1
					AND t5051.c901_warehouse_type    = 90800 --FG Warehouse
					AND t5052.c5040_plant_id=v_plant_id
			)
			taggedcns
		  WHERE wipinv.c504_consignment_id = taggedcns.lastupdtransid(+) ;
    /* To Load t253b_item_consignment_lock table with Built Set Consignments */
    CURSOR builtset_cons_lock_details_cur
    IS
         SELECT t505.c205_part_number_id pnum, t504.c504_consignment_id, 20462 location_type
          , SUM (t505.c505_item_qty) qty
           FROM t504_consignment t504, t505_item_consignment t505, t906_rules t906
          WHERE t504.c504_consignment_id         = t505.c504_consignment_id
            AND t504.c504_status_fl = t906.c906_rule_value            --IN ('2','2.20','3','3.30','3.60' ) -- 2 Built Set  /2.20 Pending Pick  /3 Pending Shipping  /3.30 Packing In Progress /3.60 Ready for Pickup 
            AND t504.c504_verify_fl              = '1'
            AND t504.c504_void_fl               IS NULL
            AND t504.c207_set_id                IS NOT NULL
            AND trim (t505.c505_control_number) is not null
            AND t906.c906_rule_id     = 'PI_BUILTSETSTATUS'
            AND t906.c906_rule_grp_id = 'PI_LOCK'
            AND t906.c906_void_fl    IS NULL
			AND t504.c5040_plant_id=v_plant_id
       GROUP BY t505.c205_part_number_id, t504.c504_consignment_id;
    /* To Load t253b_item_consignment_lock table with WIP Set Consignments */
    CURSOR wipset_cons_lock_details_cur
    IS
         SELECT t505.c205_part_number_id pnum, t504.c504_consignment_id, 20467 location_type
          , SUM (t505.c505_item_qty) qty
           FROM t504_consignment t504, t505_item_consignment t505, t906_rules t906
          WHERE t504.c504_consignment_id         = t505.c504_consignment_id
            AND t504.c504_status_fl = t906.c906_rule_value             --IN ('1','1.10','1.20') -- 1 WIP status /1.10 Completed Set status/1.20 Pending Put status
            AND t504.c504_verify_fl              = '0'
            AND t504.c504_void_fl               IS NULL
            AND t504.c207_set_id                IS NOT NULL
            AND TRIM (t505.c505_control_number) IS NOT NULL
            AND trim (t505.c505_control_number) <> 'TBE'
            AND t906.c906_rule_id     = 'PI_WIPSETSTATUS'
            AND t906.c906_rule_grp_id = 'PI_LOCK'
            AND t906.c906_void_fl    IS NULL
			AND t504.c5040_plant_id=v_plant_id
       GROUP BY t505.c205_part_number_id, t504.c504_consignment_id;
    /* To Load t251_inventory_lock_detail table with Built Set Consignments */
    CURSOR inv_builtset_part_cur
    IS
         SELECT t505.c205_part_number_id pnum, 20462 location_type, SUM (t505.c505_item_qty) qty
           FROM t504_consignment t504, t505_item_consignment t505, t906_rules t906
          WHERE t504.c504_consignment_id         = t505.c504_consignment_id
            AND t504.c504_status_fl = t906.c906_rule_value             --IN ('2','2.20','3','3.30','3.60' ) -- 2 Built Set  /2.20 Pending Pick  /3 Pending Shipping  /3.30 Packing In Progress /3.60 Ready for Pickup 
            AND t504.c504_verify_fl              = '1'
            AND t504.c504_void_fl               IS NULL
            AND t504.c207_set_id                IS NOT NULL
            AND trim (t505.c505_control_number) IS NOT NULL
            AND t906.c906_rule_id     = 'PI_BUILTSETSTATUS'
            AND t906.c906_rule_grp_id = 'PI_LOCK'
            AND t906.c906_void_fl    IS NULL
			AND t504.c5040_plant_id=v_plant_id
       GROUP BY t505.c205_part_number_id;
    /* To Load t251_inventory_lock_detail table with WIP Set Consignments */
    CURSOR inv_wipset_part_cur
    IS
         SELECT T505.c205_part_number_id PNUM, 20467 LOCATION_TYPE, SUM (T505.C505_ITEM_QTY) QTY
           FROM t504_consignment t504, t505_item_consignment t505, t906_rules t906
          WHERE T504.c504_consignment_id         = T505.c504_consignment_id
            AND t504.c504_status_fl = t906.c906_rule_value            -- IN ('1','1.10','1.20') -- 1 WIP status /1.10 Completed Set status/1.20 Pending Put status
            AND t504.c504_verify_fl              = '0'
            AND t504.c504_void_fl               IS NULL
            AND t504.c207_set_id                IS NOT NULL
            AND TRIM (t505.c505_control_number) IS NOT NULL
            AND TRIM (T505.c505_control_number) <> 'TBE'
            AND t906.c906_rule_id     = 'PI_WIPSETSTATUS'
            AND t906.c906_rule_grp_id = 'PI_LOCK'
            AND t906.c906_void_fl    IS NULL
			AND t504.c5040_plant_id=v_plant_id
       GROUP BY t505.c205_part_number_id;
BEGIN
	--Get Company, plant from context
 SELECT GET_COMPID_FRM_CNTX(), GET_PLANTID_FRM_CNTX()
	      INTO v_company_id, v_plant_id
	      FROM DUAL;
    FOR current_row IN builtset_cons_lock_cur
    LOOP
        gm_pkg_op_ld_inventory.gm_op_sav_cons_lock (p_inventory_lock_id, current_row.c504_consignment_id,
        current_row.c701_distributor_id, current_row.c504_status_fl, current_row.c504_void_fl,
        current_row.c704_account_id, current_row.c207_set_id, current_row.c504_type, current_row.c504_verify_fl,
        current_row.c504_verified_date, current_row.c504_verified_by, current_row.c520_request_id,
        current_row.c504_master_consignment_id, current_row.c504_last_updated_by, current_row.c504_last_updated_date,
        current_row.location_type, '', '', NULL,current_row.invtagid,current_row.invlocid) ;
    END LOOP;
    FOR current_row IN wipset_cons_lock_cur
    LOOP
        gm_pkg_op_ld_inventory.gm_op_sav_cons_lock (p_inventory_lock_id, current_row.c504_consignment_id,
        current_row.c701_distributor_id, current_row.c504_status_fl, current_row.c504_void_fl,
        current_row.c704_account_id, current_row.c207_set_id, current_row.c504_type, current_row.c504_verify_fl,
        current_row.c504_verified_date, current_row.c504_verified_by, current_row.c520_request_id,
        current_row.c504_master_consignment_id, current_row.c504_last_updated_by, current_row.c504_last_updated_date,
        current_row.location_type, '', '', NULL,current_row.invtagid,current_row.invlocid) ;
    END LOOP;
    FOR current_row IN builtset_cons_lock_details_cur
    LOOP
        gm_pkg_op_ld_inventory.gm_op_sav_cons_lock_detail (p_inventory_lock_id, current_row.pnum,
        current_row.c504_consignment_id, current_row.qty) ;
    END LOOP;
    FOR current_row IN wipset_cons_lock_details_cur
    LOOP
        gm_pkg_op_ld_inventory.gm_op_sav_cons_lock_detail (p_inventory_lock_id, current_row.pnum,
        current_row.c504_consignment_id, current_row.qty) ;
    END LOOP;
    FOR current_row IN inv_builtset_part_cur
    LOOP
        gm_pkg_op_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id, current_row.pnum,
        current_row.location_type, current_row.qty) ;
    END LOOP;
    FOR current_row IN inv_wipset_part_cur
    LOOP
        gm_pkg_op_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id, current_row.pnum,
        current_row.location_type, current_row.qty) ;
    END LOOP;
END gm_op_ld_consignments;
--
/***************************************************************
* Purpose: Procedure used to load consignment lock detail
***************************************************************/
--
PROCEDURE gm_op_sav_cons_lock_detail (
        p_inventory_lock_id IN t250_inventory_lock.c250_inventory_lock_id%TYPE,
        p_part_number       IN t205_part_number.c205_part_number_id%TYPE,
        p_consignment_id    IN t504_consignment.c504_consignment_id%TYPE,
        p_qty               IN t253b_item_consignment_lock.c505_item_qty%TYPE,
	p_location_type     IN t253a_consignment_lock.c901_location_type%TYPE DEFAULT NULL)
AS
BEGIN
     INSERT
       INTO t253b_item_consignment_lock
        (
            c253b_item_consignment_lock_id, c250_inventory_lock_id, c205_part_number_id
          , c505_item_qty, c504_consignment_id,c901_location_type
        )
        VALUES
        (
            s253b_item_consignment_lock.NEXTVAL, p_inventory_lock_id, p_part_number
          , p_qty, p_consignment_id,p_location_type
        ) ;
END gm_op_sav_cons_lock_detail;
--
/***************************************************************
* Purpose: Procedure used to load inhouse consignment lock
***************************************************************/
--
PROCEDURE gm_op_sav_cons_lock
    (
        p_inventory_lock_id     IN t250_inventory_lock.c250_inventory_lock_id%TYPE,
        p_consignment_id        IN t253a_consignment_lock.c504_consignment_id%TYPE,
        p_distributor_id        IN t253a_consignment_lock.c701_distributor_id%TYPE,
        p_status_fl             IN t253a_consignment_lock.c504_status_fl%TYPE,
        p_void_fl               IN t253a_consignment_lock.c504_void_fl%TYPE,
        p_account_id            IN t253a_consignment_lock.c704_account_id%TYPE,
        p_set_id                IN t253a_consignment_lock.c207_set_id%TYPE,
        p_type                  IN t253a_consignment_lock.c504_type%TYPE,
        p_verify_fl             IN t253a_consignment_lock.c504_verify_fl%TYPE,
        p_verified_date         IN t253a_consignment_lock.c504_verified_date%TYPE,
        p_verified_by           IN t253a_consignment_lock.c504_verified_by%TYPE,
        p_request_id            IN t253a_consignment_lock.c520_request_id%TYPE,
        p_master_consignment_id IN t253a_consignment_lock.c504_master_consignment_id%TYPE,
        p_last_updated_by       IN t253a_consignment_lock.c504_last_updated_by%TYPE,
        p_last_updated_date     IN t253a_consignment_lock.c504_last_updated_date%TYPE,
        p_location_type         IN t253a_consignment_lock.c901_location_type%TYPE,
        p_pnum                  IN t253a_consignment_lock.c205_part_number_id%TYPE,
        p_controlnum            IN t253a_consignment_lock.c408_control_number%TYPE,
        p_qty                   IN t253a_consignment_lock.c408_qty_received%TYPE,
		p_tag_id                IN t5010_tag.c5010_tag_id%TYPE DEFAULT NULL,
        p_inv_loc_id            IN t5052_location_master.c5052_location_id%TYPE DEFAULT NULL
    )
AS
v_company_id  t1900_company.c1900_company_id%TYPE;
v_plant_id    t5040_plant_master.c5040_plant_id%TYPE;
BEGIN
	--Get Company, plant from context
	SELECT GET_COMPID_FRM_CNTX(), GET_PLANTID_FRM_CNTX()
	      INTO v_company_id, v_plant_id
	      FROM DUAL;
     INSERT
       INTO t253a_consignment_lock
        (
            c253a_consignment_lock_id, c250_inventory_lock_id, c504_consignment_id
          , c701_distributor_id, c504_status_fl, c504_void_fl
          , c704_account_id, c207_set_id, c504_type
          , c504_verify_fl, c504_verified_date, c504_verified_by
          , c520_request_id, c504_master_consignment_id, c504_last_updated_by
          , c504_last_updated_date, c901_location_type, c205_part_number_id
          , c408_control_number, c408_qty_received,c5010_tag_id,c5052_location_id,c1900_company_id,c5040_plant_id
        )
        VALUES
        (
            s253a_consignment_lock.NEXTVAL, p_inventory_lock_id, p_consignment_id
          , p_distributor_id, p_status_fl, p_void_fl
          , p_account_id, p_set_id, p_type
          , p_verify_fl, p_verified_date, p_verified_by
          , p_request_id, p_master_consignment_id, p_last_updated_by
          , p_last_updated_date, p_location_type, p_pnum
          , p_controlnum, p_qty,p_tag_id,p_inv_loc_id,NULL,v_plant_id
        ) ;
END gm_op_sav_cons_lock;
--
/*************************************************************************
* Purpose: Procedure used to load Sub Components
*************************************************************************/
--
PROCEDURE gm_op_ld_sub_components
    (
        p_inventory_lock_id IN t250_inventory_lock.c250_inventory_lock_id%TYPE
    )
AS
    v_sub_part_num t205_part_number.c205_part_number_id%TYPE;
    v_parent_part_qty NUMBER;
    v_qty             NUMBER;
    v_pbo_qty         NUMBER;
    v_pbl_qty         NUMBER;
    v_pcs_qty         NUMBER;
    CURSOR sub_parts_cur
    IS
         SELECT t205d.c205_part_number_id pnum FROM t205d_sub_part_to_order t205d;
    CURSOR parent_parts_cur
    IS
         SELECT t205a.c205_from_part_number_id parent_part, t205a.c205_to_part_number_id to_part, (t205a.c205a_qty *
            NVL (PRIOR t205a.c205a_qty, 1)) qty
           FROM t205a_part_mapping t205a
            START WITH t205a.c205_to_part_number_id         = v_sub_part_num
            CONNECT BY PRIOR t205a.c205_from_part_number_id = t205a.c205_to_part_number_id
            AND t205a.c205a_void_fl is null;  ---  added new column c205a_void_fl in t205a used to PMT-50777 - Create BOM and sub-component Mapping	
BEGIN
    FOR sub_parts_cur_details IN sub_parts_cur
    LOOP
        v_parent_part_qty            := 0;
        v_pbo_qty                    := 0;
        v_pbl_qty                    := 0;
        v_pcs_qty                    := 0;
        v_sub_part_num               := sub_parts_cur_details.pnum;
        FOR parent_parts_cur_details IN parent_parts_cur
        LOOP
             SELECT SUM (t251.c251_qty) * parent_parts_cur_details.qty
               INTO v_qty
               FROM t251_inventory_lock_detail t251
              WHERE t251.c250_inventory_lock_id = p_inventory_lock_id
                AND t251.c205_part_number_id    = parent_parts_cur_details.parent_part
                AND t251.c901_type                                 IN (20464, 20461) ;
            v_parent_part_qty                  := v_parent_part_qty + v_qty;
             SELECT SUM (t251.c251_qty)                             * parent_parts_cur_details.qty
               INTO v_qty
               FROM t251_inventory_lock_detail t251
              WHERE t251.c250_inventory_lock_id = p_inventory_lock_id
                AND t251.c205_part_number_id    = parent_parts_cur_details.parent_part
                AND t251.c901_type                         IN (50567) ;
            v_pbo_qty                          := v_pbo_qty + v_qty;
             SELECT SUM (t251.c251_qty)                     * parent_parts_cur_details.qty
               INTO v_qty
               FROM t251_inventory_lock_detail t251
              WHERE t251.c250_inventory_lock_id = p_inventory_lock_id
                AND t251.c205_part_number_id    = parent_parts_cur_details.parent_part
                AND t251.c901_type                         IN (50568) ;
            v_pbl_qty                          := v_pbl_qty + v_qty;
             SELECT SUM (t251.c251_qty)                     * parent_parts_cur_details.qty
               INTO v_qty
               FROM t251_inventory_lock_detail t251
              WHERE t251.c250_inventory_lock_id = p_inventory_lock_id
                AND t251.c205_part_number_id    = parent_parts_cur_details.parent_part
                AND t251.c901_type                         IN (50569) ;
            v_pcs_qty                          := v_pcs_qty + v_qty;
        END LOOP;
        gm_pkg_op_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id, sub_parts_cur_details.pnum, 20477,
        v_parent_part_qty) ;
        /*
        -- While calculating TPR for sub component it was adding the qty for PBO, PBL, PCS.
        -- Because of that the count was comming wrong. So need to hide that code.
        */
        /*
        gm_pkg_op_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id
        , sub_parts_cur_details.pnum
        , 50567
        , v_pbo_qty
        );
        gm_pkg_op_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id
        , sub_parts_cur_details.pnum
        , 50568
        , v_pbl_qty
        );
        gm_pkg_op_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id
        , sub_parts_cur_details.pnum
        , 50569
        , v_pcs_qty
        );
        */
    END LOOP;
END gm_op_ld_sub_components;
/*************************************************************************
* Purpose: Procedure used to delete Inserts and labels from locked Raw material for PA
*************************************************************************/
PROCEDURE gm_op_ld_del_rm_parts (
        p_inventory_lock_id IN t250_inventory_lock.c250_inventory_lock_id%TYPE)
AS
BEGIN
     DELETE
       FROM t251_inventory_lock_detail t251
      WHERE t251.c901_type              = 20466
        AND t251.c250_inventory_lock_id = p_inventory_lock_id
        AND t251.c251_qty               > 0
        AND (c205_part_number_id LIKE 'GM%'
        OR c205_part_number_id LIKE 'DI%') ;
END gm_op_ld_del_rm_parts;
/********************************************************************************************
* Description :This function returns the WIP Qty for a Part for purchase DHR
and not Manf. Dhr
* Change Details:
Mihir Since new Phy Audit Locations Manf WIP
and Manf. Dhr was added as a part of 2010 Phy Audit
Added condition NOT LIKE 'GM-MWO%' to exclude MDHR
*********************************************************************************************/
FUNCTION get_pnum_pdhr_wip_qty (
        p_partnum IN t402_work_order.c205_part_number_id%TYPE)
    RETURN VARCHAR2
IS
    --
    --
    v_cnt       VARCHAR2 (10) ;
    v_rec_cnt   NUMBER;
    v_dhr_alloc NUMBER;
    v_plant_id  T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
    --
BEGIN
	 SELECT  get_plantid_frm_cntx() INTO v_plant_id FROM DUAL;
	
     SELECT NVL (SUM (t408.c408_shipped_qty), 0) - NVL (SUM (t408.c408_qty_rejected), 0) + NVL (SUM (
        t409.c409_closeout_qty), 0)
       INTO v_rec_cnt
       FROM t408_dhr t408, t409_ncmr t409
      WHERE t408.c408_dhr_id         = t409.c408_dhr_id(+)
        AND t408.c408_status_fl      < 4
        AND t408.c205_part_number_id = p_partnum
        AND t408.c408_void_fl       IS NULL
        AND t409.c409_void_fl(+)    IS NULL  --Add DHR_QC_Release in DHR bucket for GOP Inventory calculation
        AND t408.C5040_PLANT_ID = v_plant_id
	    AND t408.C5040_PLANT_ID = t409.C5040_PLANT_ID(+)
      -- Removing this condition,so we can also take the MWO Qty in C205_DHRQTY column for GOP.
      --  AND t408.c408_dhr_id NOT LIKE 'GM-MWO%'
      ;
     SELECT NVL (SUM (c413_item_qty), 0)
       INTO v_dhr_alloc
       FROM t412_inhouse_transactions a, t413_inhouse_trans_items b
      WHERE a.c412_inhouse_trans_id = b.c412_inhouse_trans_id
        AND a.c412_verified_date   IS NULL
        AND a.c412_void_fl         IS NULL
        AND a.c412_status_fl       != '0'
        AND a.c412_type            IN (100065, 100066, 100067, 100068, 100069, 100070,1006461)
        AND b.c205_part_number_id   = p_partnum
        AND a.C5040_PLANT_ID    = v_plant_id;
    RETURN v_rec_cnt + v_dhr_alloc;
END get_pnum_pdhr_wip_qty;
/***************************************************************
* Purpose: Procedure used to load Manufacturing WIP and DHRs
***************************************************************/
PROCEDURE gm_op_ld_manufacturing (
        p_inventory_lock_id IN t250_inventory_lock.c250_inventory_lock_id%TYPE)
AS
	v_company_id  t1900_company.c1900_company_id%TYPE;
	v_plant_id    t5040_plant_master.c5040_plant_id%TYPE;
    /* To load the t253a_consiignment_lock table with Mfg WIP transactions*/
    CURSOR manf_wip_lock_cur
    IS
         SELECT t408.c408_dhr_id dhrid, t408.c205_part_number_id pnum, t408.c408_control_number cnum
          , t408.c408_qty_received qty, 20478 location_type
           FROM t408_dhr t408
          WHERE t408.c408_status_fl IN (0)
            AND t408.c408_dhr_id LIKE 'GM-MWO%'
            AND t408.c408_void_fl IS NULL
			AND t408.c5040_plant_id=v_plant_id;
    /* To Load t253b_item_consignment_lock table with Mfg WIP transactions */
    CURSOR manf_wip_lock_details_cur
    IS
         SELECT t304.c205_part_number_id pnum, t303.c408_dhr_id dhrid, SUM (DECODE (t303.c901_type, 30000,
            t304.c304_item_qty, 0) --release
            - DECODE (t303.c901_type, 30001, t304.c304_item_qty, 0) --scrap
            - DECODE (t303.c901_type, 30002, t304.c304_item_qty, 0) -- return
            ) qty
           FROM t303_material_request t303, t304_material_request_item t304, t408_dhr t408
          WHERE t408.c408_status_fl          IN (0)
            AND t408.c408_dhr_id              = t303.c408_dhr_id
            AND t303.c303_material_request_id = t304.c303_material_request_id
            --AND TRIM (t304.c304_control_number) IS NOT NULL
            AND t408.c408_void_fl   IS NULL
            AND t303.c303_delete_fl IS NULL
            AND t304.c304_delete_fl IS NULL
            AND (t304.c205_part_number_id NOT LIKE 'DI%'
            AND t304.c205_part_number_id NOT LIKE 'GM%')
			AND t408.c5040_plant_id=v_plant_id
       GROUP BY t304.c205_part_number_id, t303.c408_dhr_id;
    /* To Load t251_inventory_lock_detail table with Mfg WIP transactions */
    CURSOR inv_manf_wip_part_cur
    IS
         SELECT t304.c205_part_number_id pnum, 20478 location_type, SUM (DECODE (t303.c901_type, 30000,
            t304.c304_item_qty, 0) --release
            - DECODE (t303.c901_type, 30001, t304.c304_item_qty, 0) --scrap
            - DECODE (t303.c901_type, 30002, t304.c304_item_qty, 0) -- return
            ) qty
           FROM t303_material_request t303, t304_material_request_item t304, t408_dhr t408
          WHERE t408.c408_status_fl          IN (0)
            AND t408.c408_dhr_id              = t303.c408_dhr_id
            AND t303.c303_material_request_id = t304.c303_material_request_id
            -- AND TRIM (t304.c304_control_number) IS NOT NULL
            AND t408.c408_void_fl   IS NULL
            AND t303.c303_delete_fl IS NULL
            AND t304.c304_delete_fl IS NULL
            AND (t304.c205_part_number_id NOT LIKE 'DI%'
            AND t304.c205_part_number_id NOT LIKE 'GM%')
			AND t408.c5040_plant_id=v_plant_id
       GROUP BY t304.c205_part_number_id;
    /* To load the t253a_consignment_lock/t253b_item_consignment_lock tables with Manf DHR Transactions */
    CURSOR manf_dhr_lock_cur
    IS
         SELECT t408.c408_dhr_id dhrid, t408.c205_part_number_id pnum, t408.c408_control_number cnum
          , t408.c408_qty_received qty, 20479 location_type
           FROM t408_dhr t408
          WHERE t408.c408_status_fl IN (1,2,3)
            AND t408.c408_dhr_id LIKE 'GM-MWO%'
            AND t408.c408_void_fl IS NULL
			AND t408.c5040_plant_id=v_plant_id
	 UNION ALL
	 --need to include all DHR which is verified and associated inhouse txn not verified
	 SELECT t408.c408_dhr_id dhrid, t408.c205_part_number_id pnum, t408.c408_control_number cnum
	  , t408.c408_qty_received qty, 20479 location_type
	   FROM t412_inhouse_transactions t412, t413_inhouse_trans_items t413, t408_dhr t408
	  WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
	    AND t408.c408_dhr_id           = t412.c412_ref_id
	    AND t412.c412_verified_date   IS NULL
	    AND t412.c412_void_fl         IS NULL
	    AND t408.c408_void_fl         IS NULL
	    AND t412.c412_status_fl       != '4'
	    AND t412.c412_type            IN (100065, 100066, 100067, 100068, 100069, 100070)
	    AND t408.c408_status_fl        = 4
	    AND t412.c412_ref_id LIKE '%MWO%'
		AND t408.c5040_plant_id=v_plant_id;
    /* To Load t251_inventory_lock_detail table with Manf DHR Transactions */
    CURSOR inv_manf_dhr_part_cur
    IS
         SELECT t408.c205_part_number_id pnum, 20479 location_type, SUM (t408.c408_qty_received) qty
           FROM t408_dhr t408
          WHERE t408.c408_status_fl IN (1,2,3)
            AND t408.c408_dhr_id LIKE 'GM-MWO%'
            AND t408.c408_void_fl IS NULL
			AND t408.c5040_plant_id=v_plant_id
       GROUP BY t408.c205_part_number_id
       UNION ALL
	 SELECT t408.c205_part_number_id pnum, 20479 location_type, t408.c408_qty_received qty
	   FROM t412_inhouse_transactions t412, t413_inhouse_trans_items t413, t408_dhr t408
	  WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
	    AND t408.c408_dhr_id           = t412.c412_ref_id
	    AND t412.c412_verified_date   IS NULL
	    AND t412.c412_void_fl         IS NULL
	    AND t408.c408_void_fl         IS NULL
	    AND t412.c412_status_fl       != '4'
	    AND t412.c412_type            IN (100065, 100066, 100067, 100068, 100069, 100070)
	    AND t408.c408_status_fl        = 4
	    AND t412.c412_ref_id LIKE '%MWO%'
		AND t408.c5040_plant_id=v_plant_id;
BEGIN
	--Get Company, plant from context
 SELECT GET_COMPID_FRM_CNTX(), GET_PLANTID_FRM_CNTX()
	      INTO v_company_id, v_plant_id
	      FROM DUAL;
    FOR current_row IN manf_wip_lock_cur
    LOOP
        gm_pkg_op_ld_inventory.gm_op_sav_cons_lock (p_inventory_lock_id, current_row.dhrid, '', NULL, '', '', '', '',
        '', '', '', '', '', '', '', current_row.location_type, current_row.pnum, current_row.cnum, current_row.qty) ;
    END LOOP;
    FOR current_row IN manf_wip_lock_details_cur
    LOOP
        gm_pkg_op_ld_inventory.gm_op_sav_cons_lock_detail (p_inventory_lock_id, current_row.pnum, current_row.dhrid,
        current_row.qty) ;
    END LOOP;
    FOR current_row IN inv_manf_wip_part_cur
    LOOP
        gm_pkg_op_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id, current_row.pnum,
        current_row.location_type, current_row.qty) ;
    END LOOP;
    FOR current_row IN manf_dhr_lock_cur
    LOOP
        gm_pkg_op_ld_inventory.gm_op_sav_cons_lock (p_inventory_lock_id, current_row.dhrid, '', NULL, '', '', '', '',
        '', '', '', '', '', '', '', current_row.location_type, current_row.pnum, current_row.cnum, current_row.qty) ;
        gm_pkg_op_ld_inventory.gm_op_sav_cons_lock_detail (p_inventory_lock_id, current_row.pnum, current_row.dhrid,
        current_row.qty) ;
    END LOOP;
    FOR current_row IN inv_manf_dhr_part_cur
    LOOP
        gm_pkg_op_ld_inventory.gm_op_sav_inventory_detail (p_inventory_lock_id, current_row.pnum,
        current_row.location_type, current_row.qty) ;
    END LOOP;
END gm_op_ld_manufacturing;
/***************************************************************
* Purpose: Procedure used for Inv. Locations for warehouse shelf and Restricted warehouse
* The locations loaded will behave as transactions for PI.
* Loading table  t251_inventory_lock_detail  with Parts in stock  has been handled by
gm_pkg_op_ld_inventory.gm_op_ld_inshelf for PI
***************************************************************/
PROCEDURE gm_op_ld_inv_locations (
        p_inventory_lock_id IN t250_inventory_lock.c250_inventory_lock_id%TYPE)
AS
    v_no_loc_shelf VARCHAR2 (200) ;
    v_no_loc_rw VARCHAR2 (200) ;
	v_company_id  t1900_company.c1900_company_id%TYPE;
	v_plant_id    t5040_plant_master.c5040_plant_id%TYPE;
    /* To load the t253a_consiignment_lock table with  warehouse shelf and Restricted warehouse location */
    CURSOR stock_inv_loc_lock_cur
    IS
         SELECT t5052.c5052_location_cd locid,  DECODE(t5051.C901_WAREHOUSE_TYPE,90800, 20450,56001,4000114) location_type
           FROM t5051_inv_warehouse t5051,t5052_location_master t5052
          WHERE t5051.C5051_INV_WAREHOUSE_ID = t5052.C5051_INV_WAREHOUSE_ID
            AND t5051.C901_STATUS_ID = 1
            AND t5051.C901_WAREHOUSE_TYPE <> (4000339)-- 56001 Restricted Warehouse  --90800 FG Warehouse
            AND t5051.c5051_inv_warehouse_id <> 3 
            AND t5052.c5052_void_fl IS NULL
            AND c901_status          = 93310 --93310/Active  93202/Initiated
            AND c901_location_type  IS NOT NULL
			AND t5052.c5040_plant_id=v_plant_id; -- 93336/bulk 93320/Pick Face   NULL
    /* To Load t253b_item_consignment_lock table with  warehouse shelf and Restricted warehouse location  */
    CURSOR stock_inv_loc_lock_det_cur
    IS
          SELECT t5053.c205_part_number_id pnum, t5052.c5052_location_cd locid,  DECODE(t5051.C901_WAREHOUSE_TYPE,90800, 20450,56001,4000114) location_type
          , SUM (NVL(t5053.c5053_curr_qty,0)) qty
           FROM t5051_inv_warehouse t5051,t5052_location_master t5052, t5053_location_part_mapping t5053
          WHERE t5051.C5051_INV_WAREHOUSE_ID=t5052.C5051_INV_WAREHOUSE_ID
          AND t5052.c5052_location_id = t5053.c5052_location_id
            AND t5051.C901_WAREHOUSE_TYPE <> (4000339)-- 56001 Restricted Warehouse  --90800 FG Warehouse
            AND C901_STATUS_ID =1
            AND c901_status             = 93310 --93310/Active  93202/Initiated
            AND t5051.c5051_inv_warehouse_id <> 3
            AND c901_location_type     IS NOT NULL -- 93336/bulk  93320/Pick Face   NULL
            --AND t5053.c5052_location_id = '036-B-002-A-1'
            AND t5053.c5053_curr_qty IS NOT NULL --'036-B-002-A-1' has 3 parts with with qty 0 and 4 row with null
            -- part no  and null qty
            AND t5052.c5052_void_fl IS NULL
	    -- if location is bulk and qty is 0, need exclude it
            AND (t5052.c901_location_type = 93336
    	    AND NVL(t5053.c5053_curr_qty,0)     > 0
            OR t5052.c901_location_type   = 93320)
			AND t5052.c5040_plant_id=v_plant_id
       GROUP BY t5053.c205_part_number_id, t5052.c5052_location_cd, t5051.C901_WAREHOUSE_TYPE;
    /* FOR Shelf : To load the t253a_consiignment_lock table with a fictitious Inv. location(No Location Mapped).All the parts that
    are not mapped to Inv. location will be tagged as  No Location Mapped */
    CURSOR stock_inv_noloc_lock_cur
    IS
         SELECT v_no_loc_shelf locid, 20450 location_type FROM dual;
    /*  FOR Shelf :To load the t253b_item_consignment_lock table with a fictitious Inv. location(No Location Mapped).All the parts
    that are not mapped to Inv. location will be tagged as  No Location Mapped */
    CURSOR stock_inv_noloc_lock_det_cur
    IS
            SELECT pnum, locid, location_type
              , qty
               FROM
                (
                     SELECT parttab.pnum pnum, v_no_loc_shelf locid, 20450 location_type
                      , NVL (parttab.c205_qty_in_stock, 0) - NVL (loctab.location_qty, 0) qty
                       FROM
                        (
                             SELECT t205c.c205_part_number_id pnum, t205c.c205_available_qty c205_qty_in_stock
						           FROM t205c_part_qty t205c
						          WHERE t205c.c901_transaction_type = '90800'
						          AND t205c.C5040_PLANT_ID 	= 	v_plant_id
                        )
                    parttab, (
                         SELECT c205_part_number_id pnum, SUM (c5053_curr_qty) location_qty
                           FROM t5051_inv_warehouse t5051,t5053_location_part_mapping t5053, t5052_location_master t5052
                          WHERE t5051.C5051_INV_WAREHOUSE_ID=t5052.C5051_INV_WAREHOUSE_ID
                          AND t5052.c5052_location_id = t5053.c5052_location_id
                          AND t5051.C901_STATUS_ID =1
                        AND t5051.C901_WAREHOUSE_TYPE = (90800)-- 56001 Restricted Warehouse  --90800 FG Warehouse
                        AND t5051.c5051_inv_warehouse_id <> 3
                        AND t5052.c5052_void_fl IS NULL
						AND t5052.c5040_plant_id=v_plant_id
                       -- AND c205_part_number_id IN ('101.825' )
                    GROUP BY c205_part_number_id
                    )
                    loctab
                  WHERE parttab.pnum                        = loctab.pnum(+)
                    AND NVL (parttab.c205_qty_in_stock, 0) != NVL (loctab.location_qty, 0)
                  -- AND parttab.pnum IN ('GMBS05','182.350','101.825' )-- 'GMBS05'
                )
              WHERE qty > 0
           ORDER BY locid, pnum;
      
         /* FOR RW: To load the t253a_consiignment_lock table with a fictitious Inv. location(No Location Mapped).All the parts that
    are not mapped to RW or diff of Loc vs. RW . location will be tagged as  No Location Mapped */
    CURSOR rw_noloc_lock_cur
    IS
         SELECT v_no_loc_rw locid, 4000114 location_type FROM dual;
      /*  FOR RW :To load the t253b_item_consignment_lock table with a fictitious Inv. location(No Location Mapped).All the parts
    that are not mapped to RW or diff of Loc vs. RW . location will be tagged as  No Location Mapped */
    CURSOR rw_noloc_lock_det_cur
    IS
             SELECT pnum, locid, location_type
          , qty
           FROM
            (
                 SELECT parttab.pnum pnum, v_no_loc_rw locid, 4000114 location_type
                  , NVL (parttab.rwqty, 0) - NVL (loctab.location_qty, 0) qty
                   FROM
                    (
                         SELECT     T205c.c205_part_number_id pnum, NVL (T205c.c205_available_qty, 0) rwqty
				    FROM T205c_Part_Qty T205c
				    WHERE T205c.C901_Transaction_Type IN (56001) 
					AND T205c.c5040_plant_id=v_plant_id
                    )
                parttab, (
                     SELECT c205_part_number_id pnum, SUM (c5053_curr_qty) location_qty
                       FROM t5051_inv_warehouse t5051,t5053_location_part_mapping t5053, t5052_location_master t5052
                      WHERE t5051.C5051_INV_WAREHOUSE_ID=t5052.C5051_INV_WAREHOUSE_ID
                      AND t5052.c5052_location_id = t5053.c5052_location_id
                      AND t5051.C901_STATUS_ID =1
                      AND t5051.C901_WAREHOUSE_TYPE = (56001)-- 56001 Restricted Warehouse  --90800 FG Warehouse
                      AND t5051.c5051_inv_warehouse_id <> 3
                    AND t5052.c5052_void_fl IS NULL
					AND t5052.c5040_plant_id=v_plant_id
	            GROUP BY c205_part_number_id
                )
                loctab
              WHERE parttab.pnum                        = loctab.pnum(+)
                AND NVL (parttab.rwqty, 0) != NVL (loctab.location_qty, 0)
               -- AND parttab.pnum IN ('124.456' )-- 'GMBS05'
            )
          WHERE qty > 0
       ORDER BY locid, pnum;    
    BEGIN
    
--Get Company, plant from context
		SELECT GET_COMPID_FRM_CNTX(), NVL(GET_PLANTID_FRM_CNTX(),GET_RULE_VALUE('DEFAULT_PLANT','ONEPORTAL'))
	      INTO v_company_id, v_plant_id
	      FROM DUAL;
	     
	    v_no_loc_shelf     := get_rule_value ('20450', 'PINOLOCPARTS') ;
        v_no_loc_rw     := get_rule_value ('4000114', 'PINOLOCPARTS') ;
        
        FOR current_row IN stock_inv_loc_lock_cur
        LOOP
            gm_pkg_op_ld_inventory.gm_op_sav_cons_lock (p_inventory_lock_id, current_row.locid, '', NULL, '', '', '',
            '', '', '', '', '', '', '', '', current_row.location_type, '', '', NULL) ;
        END LOOP;
        FOR current_row IN stock_inv_loc_lock_det_cur
        LOOP
            gm_pkg_op_ld_inventory.gm_op_sav_cons_lock_detail (p_inventory_lock_id, current_row.pnum, current_row.locid
            , current_row.qty,current_row.location_type) ;
        END LOOP;
        FOR current_row IN stock_inv_noloc_lock_cur
        LOOP
            gm_pkg_op_ld_inventory.gm_op_sav_cons_lock (p_inventory_lock_id, current_row.locid, '', NULL, '', '', '',
            '', '', '', '', '', '', '', '', current_row.location_type, '', '', NULL) ;
        END LOOP;
        FOR current_row IN stock_inv_noloc_lock_det_cur
        LOOP
            gm_pkg_op_ld_inventory.gm_op_sav_cons_lock_detail (p_inventory_lock_id, current_row.pnum, current_row.locid
            , current_row.qty,current_row.location_type) ;
        END LOOP;
         FOR current_row IN rw_noloc_lock_cur
        LOOP
            gm_pkg_op_ld_inventory.gm_op_sav_cons_lock (p_inventory_lock_id, current_row.locid, '', NULL, '', '', '',
            '', '', '', '', '', '', '', '', current_row.location_type, '', '', NULL) ;
        END LOOP;
        FOR current_row IN rw_noloc_lock_det_cur
        LOOP
            gm_pkg_op_ld_inventory.gm_op_sav_cons_lock_detail (p_inventory_lock_id, current_row.pnum, current_row.locid
            , current_row.qty,current_row.location_type) ;
        END LOOP;
    END gm_op_ld_inv_locations;
	/********************************************************************************************
* Description :This function returns the WIP Qty for a Part for purchase DHR
*and not Manf. Dhr. This function is added for PI only which is designed to exclude GM-MWO from DHR Lock.
* the detail of this function  is taken from original function "get_pnum_pdhr_wip_qty version 55221".
*********************************************************************************************/
FUNCTION get_pi_pnum_pdhr_wip_qty (
        p_partnum IN t402_work_order.c205_part_number_id%TYPE)
    RETURN VARCHAR2
IS
    --
    --
    v_cnt       VARCHAR2 (10) ;
    v_rec_cnt   NUMBER;
    v_dhr_alloc NUMBER;
    --
BEGIN
     RETURN get_pi_pnum_pdhr_wip_qty(p_partnum,'');
END get_pi_pnum_pdhr_wip_qty;

    /********************************************************************************************
    * Description  :This function returns the DHR Qty for a Part and based on the DHR ID as well
    and not Manf. Dhr
    *********************************************************************************************/

FUNCTION get_pi_pnum_pdhr_wip_qty (
        p_partnum IN t402_work_order.c205_part_number_id%TYPE,
        p_dhr_id IN t408_dhr.c408_dhr_id%TYPE
        )
    RETURN VARCHAR2
IS
    --
    --
    v_cnt       VARCHAR2 (10) ;
    v_rec_cnt   NUMBER;
    v_dhr_alloc NUMBER;
    --
BEGIN
     SELECT NVL (SUM (t408.c408_shipped_qty), 0) - NVL (SUM (t408.c408_qty_rejected), 0) + NVL (SUM (
        t409.c409_closeout_qty), 0)
       INTO v_rec_cnt
       FROM t408_dhr t408, t409_ncmr t409
      WHERE t408.c408_dhr_id         = t409.c408_dhr_id(+)
        AND t408.c408_status_fl      < 4
        AND t408.c205_part_number_id = p_partnum
        AND t408.c408_void_fl       IS NULL
        AND t409.c409_void_fl       IS NULL
        AND t408.c408_dhr_id = NVL(p_dhr_id,t408.c408_dhr_id)
        AND t408.c408_dhr_id NOT LIKE 'GM-MWO%';
		
     SELECT NVL (SUM (c413_item_qty), 0)
       INTO v_dhr_alloc
       FROM t412_inhouse_transactions a, t413_inhouse_trans_items b
      WHERE a.c412_inhouse_trans_id = b.c412_inhouse_trans_id
        AND a.c412_verified_date   IS NULL
        AND a.c412_void_fl         IS NULL
        AND a.c412_status_fl       != '0'
        AND NVL(a.C412_REF_ID,'-9999') = NVL(p_dhr_id,NVL(a.C412_REF_ID,'-9999'))
        AND a.c412_type            IN (100065, 100066, 100067, 100068, 100069, 100070,1006461)
        AND b.c205_part_number_id   = p_partnum;
    RETURN v_rec_cnt + v_dhr_alloc;
END get_pi_pnum_pdhr_wip_qty;

/*
 *  Procedure to Lock the DHR Qty by Part Number and DHR ID.
 *  As PI is done in DHR area based on the open DHR Txn, this will load
 *  the Part Number and Qty based on the DHR Txn.
 */
PROCEDURE GM_OP_LD_DHR_TXN (
        p_inventory_lock_id IN t250_inventory_lock.c250_inventory_lock_id%TYPE)
AS

v_dhr_id t408_dhr.C408_DHR_ID%TYPE;
v_company_id  t1900_company.c1900_company_id%TYPE;
v_plant_id    t5040_plant_master.c5040_plant_id%TYPE;

CURSOR DHR_TXN_CUR
    IS
select t408.C408_DHR_ID DHR_ID , 20461 LOC_TYPE
 FROM t408_dhr t408 , T401_PURCHASE_ORDER T401, T402_WORK_ORDER T402
  WHERE c408_status_fl            < 4
  AND t402.c402_work_order_id   = t408.c402_work_order_id
  AND t401.c401_purchase_ord_id = t402.c401_purchase_ord_id
  AND T402.C402_VOID_FL IS NULL
  AND T401.C401_VOID_FL IS NULL
  AND t408.c408_void_fl IS NULL
  AND T408.c408_dhr_id NOT LIKE 'GM-MWO%'
  AND t401.c401_type           IN (3100,3101)  
  AND t408.c5040_plant_id=v_plant_id;

CURSOR DHR_PART_QTY_CUR
    IS

select t408.C408_DHR_ID  DHR_ID, 
t408.C205_Part_number_id  PNUM,
-- Get the DHR qty by Part number and DHR ID.
gm_pkg_op_ld_inventory.get_pi_pnum_pdhr_wip_qty(t408.c205_part_number_id,t408.C408_DHR_ID) QTY
,20461 LOC_TYPE
 FROM t408_dhr t408 , T401_PURCHASE_ORDER T401, T402_WORK_ORDER T402
  WHERE c408_status_fl            < 4
  AND t402.c402_work_order_id   = t408.c402_work_order_id
  AND t401.c401_purchase_ord_id = t402.c401_purchase_ord_id
  AND T402.C402_VOID_FL IS NULL
  AND T401.C401_VOID_FL IS NULL
  AND t408.c408_void_fl IS NULL
  AND t401.c401_type           IN (3100,3101)
  AND T408.c408_dhr_id NOT LIKE 'GM-MWO%'
  AND t408.c5040_plant_id=v_plant_id;

BEGIN
	
	--Get Company, plant from context
		SELECT GET_COMPID_FRM_CNTX(), NVL(GET_PLANTID_FRM_CNTX(),GET_RULE_VALUE('DEFAULT_PLANT','ONEPORTAL'))
	      INTO v_company_id, v_plant_id
	      FROM DUAL;

        FOR current_row IN DHR_TXN_CUR
        LOOP
        	
        -- While Recording Tag, if the txn Start with GM, system throws a front end validation not to enter more than 1 qty.
        -- As DHR Txn will have more qunatity we are stripping  GM from GM-DHR-XXXXXX txn.
        	v_dhr_id := SUBSTR(current_row.DHR_ID,4);
            gm_pkg_op_ld_inventory.gm_op_sav_cons_lock (p_inventory_lock_id, v_dhr_id, '', NULL, '', '', '',
            '', '', '', '', '', '', '', '', current_row.LOC_TYPE, '', '', NULL) ;
            
            v_dhr_id :='';
        END LOOP;

        FOR current_row IN DHR_PART_QTY_CUR
        LOOP
        
        	v_dhr_id := SUBSTR(current_row.DHR_ID,4);
        	
            gm_pkg_op_ld_inventory.gm_op_sav_cons_lock_detail (p_inventory_lock_id, current_row.pnum, v_dhr_id
            , current_row.qty,current_row.LOC_TYPE) ;
            
            v_dhr_id := '';
        END LOOP;

END GM_OP_LD_DHR_TXN;
END gm_pkg_op_ld_inventory;
/
