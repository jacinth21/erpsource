/* Formatted on 2011/12/19 12:45 (Formatter Plus v4.8.0) */
--@"C:\database\packages\operations\gm_pkg_op_fch_insert_details.bdy";
CREATE OR REPLACE
PACKAGE BODY gm_pkg_op_fch_insert_details
IS
 
 /****************************************************************************
  * Description : Procedure to fetch the insert details for the part numbers.
  * Author   	: karthik
  ****************************************************************************/
PROCEDURE gm_fetch_insert_dtl
  (
    p_refid    	   IN 		T5056_TRANSACTION_INSERT.C5056_REF_ID%TYPE,
    p_reftype  	   IN 		T5056_TRANSACTION_INSERT.C901_REF_TYPE%TYPE,
    p_out_dtl	   OUT		TYPES.cursor_type
   )
AS
  v_plant_id      t5040_plant_master.c5040_plant_id%TYPE;
  v_company_id	  t1900_company.c1900_company_id%TYPE;
  v_flag 		  VARCHAR2(1);
  v_pnums		  CLOB;
BEGIN
  --
	   SELECT  get_compid_frm_cntx()  INTO v_company_id FROM DUAL;
	   SELECT  get_plantid_frm_cntx() INTO v_plant_id FROM DUAL;
  --
  	   --Validating the Plant is Enabled for INSERT	
   	   SELECT get_rule_value(v_plant_id,'INSERT_PART') INTO v_flag FROM DUAL; 
	   
	   IF v_flag IS NULL OR v_flag <> 'Y' 
	   THEN
		   RETURN;
	   END IF;
	   
	   --validating the Transaction Type is Enabled for INSERT	  	
	   SELECT get_rule_value(p_reftype,'INSERT_TRANS') INTO v_flag FROM DUAL; 	  
	   
	   IF v_flag IS NULL OR v_flag <> 'Y' 
	   THEN
		   RETURN;
	   END IF;
         
      OPEN p_out_dtl
		 FOR	
	  SELECT ID ,
		  revnum,
		  PICKQTY,
		  SUGLOCATION,
		  MAPLOCATION,
		  LOCATIONID,
		  statusfl
		FROM
		  (SELECT T5056.C205_INSERT_ID ID ,
		    T5056.C5056_REV_NUM revnum,
		    '1' PICKQTY,
		    gm_pkg_op_fch_insert_details.get_insert_location_dtl (T5056.C205_INSERT_ID, '90800') SUGLOCATION,
		    T5056.C5052_LOCATION_ID MAPLOCATIONID,
			gm_pkg_op_inv_scan.get_part_location (T5056.C205_INSERT_ID, '') LOCATIONID ,
			gm_pkg_op_inv_scan.get_location_code(t5056.c5052_location_id) MAPLOCATION,
		    T5056.C901_STATUS statusfl
		  FROM T5056_TRANSACTION_INSERT T5056
		  WHERE T5056.C5056_REF_ID = p_refid
		  AND T5056.C5056_VOID_FL IS NULL
		  ORDER BY T5056.C205_INSERT_ID ASC
		  )
		GROUP BY ID ,
		  revnum ,
		  PICKQTY,
		  SUGLOCATION,
		  MAPLOCATION,
		  LOCATIONID,
		  statusfl;
			
END gm_fetch_insert_dtl;

/*******************************************************
* Description : Function to return the location for insert
* Author    : Karthik
*******************************************************/
FUNCTION get_insert_location_dtl
  (
    p_inserid t205_part_number.c205_part_number_id%TYPE ,
    p_whtype  t5052_location_master.c5052_location_cd%TYPE )
  RETURN VARCHAR
IS

  v_location 		VARCHAR (20);
  v_plant_id     	t5040_plant_master.c5040_plant_id%TYPE;
  v_default_plant	t5040_plant_master.c5040_plant_id%TYPE;
  
BEGIN
	
  v_default_plant := NVL(get_rule_value('DEFAULT_PLANT','ONEPORTAL'),'3000');
  
   SELECT get_plantid_frm_cntx()
	 INTO v_plant_id
	 FROM DUAL;

   IF v_plant_id IS NULL THEN
   		v_plant_id := v_default_plant;
   END IF;
   
	 SELECT loc_cd
	   INTO	v_location
	   FROM
		  (
		    SELECT t5052.c5052_location_cd loc_cd
		      FROM t5052_location_master t5052,
		           t5053_location_part_mapping t5053,
		      	   t5051_inv_warehouse t5051
		     WHERE t5052.c5052_location_id = t5053.c5052_location_id
		       AND t5051.c5051_inv_warehouse_id  = t5052.c5051_inv_warehouse_id
		       AND t5051.c5051_inv_warehouse_id <> 3 --Field Sales
		       AND t5051.c901_warehouse_type = 90800 --90800 (FG)
		       AND t5052.c901_status = '93310' --Active Location
		       AND t5053.c205_part_number_id = p_inserid
		       AND t5052.c5052_void_fl IS NULL
		       AND t5051.C901_STATUS_ID = 1 -- Active
		       AND t5052.c5040_plant_id = v_plant_id
		         ORDER BY c205_part_number_id ASC
					, t5051.C901_WAREHOUSE_TYPE
					,  T5052.C901_LOCATION_TYPE ASC
		  )
	WHERE ROWNUM = 1;

RETURN v_location;

END get_insert_location_dtl;
  
  /******************************************************************************************************
  * Description : Procedure to fetch the Insert Active Locations
  * Author   	: karthik
  *****************************************************************************************************/
PROCEDURE gm_fch_insert_Location_details
  ( 
  	p_inserid 			IN t205_part_number.c205_part_number_id%TYPE ,
    p_whtype  			IN VARCHAR2,
  	p_out_loc_dtl	    OUT  TYPES.cursor_type
   )
  AS
  v_location 		VARCHAR (20);
  v_plant_id     	t5040_plant_master.c5040_plant_id%TYPE;
  v_default_plant	t5040_plant_master.c5040_plant_id%TYPE;
  BEGIN	
	
  v_default_plant := NVL(get_rule_value('DEFAULT_PLANT','ONEPORTAL'),'3000');
  
   SELECT get_plantid_frm_cntx()
	 INTO v_plant_id
	 FROM DUAL;

   IF v_plant_id IS NULL THEN
   		v_plant_id := v_default_plant;
   END IF;
   
	  my_context.set_my_inlist_ctx (p_inserid);
	  OPEN p_out_loc_dtl FOR
	 SELECT c205_part_number_id pnum
	 		,t5052.c5052_location_id LOCATIONID
	 		,T5052.c5052_location_cd LOCATIONCD
	 		,'FG' LOCATIONTYPE
	 		, gm_pkg_op_item_control_rpt.get_part_location_cur_qty (t5053.c205_part_number_id
																		  , t5053.c5052_location_id
																		  , 90800
																		   ) CURRENTLOCQTY
		      FROM t5052_location_master t5052,
		           t5053_location_part_mapping t5053,
		      	   t5051_inv_warehouse t5051
		     WHERE t5052.c5052_location_id = t5053.c5052_location_id
		       AND t5051.c5051_inv_warehouse_id  = t5052.c5051_inv_warehouse_id
		       AND t5051.c5051_inv_warehouse_id <> 3 --Field Sales
		       AND t5051.c901_warehouse_type = 90800 --90800 (FG)
		       AND t5052.c901_status = '93310' --Active Location
		       AND t5053.c205_part_number_id IN (SELECT TOKEN FROM v_in_list)
		       AND t5052.c5052_void_fl IS NULL
		       AND t5051.C901_STATUS_ID = 1 -- Active
		       AND t5052.c5040_plant_id = v_plant_id
		         ORDER BY c205_part_number_id ASC
					, t5051.C901_WAREHOUSE_TYPE
					,  T5052.C901_LOCATION_TYPE ASC;
  	   
  END gm_fch_insert_Location_details;
  
  /****************************************************************************
  * Description : Procedure to fetch the insert details for the Transaction.
  * Author   	: karthik
  ****************************************************************************/
PROCEDURE gm_fetch_txn_insert_info
  (
    p_refid    	   IN 		T5056_TRANSACTION_INSERT.C5056_REF_ID%TYPE,
    p_out_dtl	   OUT		TYPES.cursor_type,
    p_out_status   OUT		VARCHAR2
   )
AS
 v_status_fl  VARCHAR2(10);
 v_plant_id   t5040_plant_master.c5040_plant_id%TYPE;
 v_comp_id    t1900_company.c1900_company_id%TYPE;
 v_filter     VARCHAR2(40);
 v_cnt		  NUMBER;

BEGIN
		SELECT get_plantid_frm_cntx() ,get_compid_frm_cntx() 
				INTO v_plant_id ,v_comp_id
				 FROM DUAL;
		 
	    SELECT DECODE(GET_RULE_VALUE(v_comp_id,'INSERTTXNFILTER'), null, v_comp_id, v_plant_id) --INSERTTXNFILTER- 	To fetch the records based on Plant (or) Company
	          INTO v_filter
	          FROM DUAL;
	          
	    IF p_refid IS NOT NULL THEN
		      BEGIN
		      	SELECT count(*) 
		      		INTO v_cnt
				 FROM T907_SHIPPING_INFO
				WHERE C907_REF_ID    = p_refid
				AND C1900_COMPANY_ID = v_filter
				AND C907_VOID_FL  IS NULL;
			 END;
		END IF;
		
		IF v_cnt = 0 THEN
			raise_application_error ('-20999', 'No Data Available');
		END IF;

	    OPEN p_out_dtl
		  FOR	
		 SELECT T5056.C5056_TRANSACTION_INSERT_ID ID,
		 		T5056.C205_INSERT_ID INSERTID,
	  			T5056.C5056_REV_NUM REVNUM,
	  			T5056.C5052_LOCATION_ID MAPLOCATIONID,
	  			gm_pkg_op_inv_scan.get_location_code(t5056.c5052_location_id) MAPLOCATION
			FROM  T5056_TRANSACTION_INSERT T5056 , T907_SHIPPING_INFO T907
			WHERE T5056.C5056_REF_ID = TRIM(p_refid)
			AND T5056.C5056_REF_ID = T907.C907_REF_ID
			AND T907.C907_VOID_FL IS NULL
			AND T907.C907_STATUS_FL = '15' --Pending Control 
      		AND T5056.C5056_VOID_FL IS NULL
      		AND (T907.C1900_COMPANY_ID = v_filter OR C5040_PLANT_ID = v_filter )
			ORDER BY T5056.C205_INSERT_ID ASC;
				
		BEGIN
		  SELECT DECODE(C907_STATUS_FL , '15' , 'Y' , 'N')
		  	INTO v_status_fl
		  FROM T907_SHIPPING_INFO T907
		  WHERE T907.C907_REF_ID = TRIM(p_refid)
		  AND T907.C907_VOID_FL IS NULL;
		EXCEPTION
		WHEN NO_DATA_FOUND THEN
		  v_status_fl := NULL;
		END;
				p_out_status := v_status_fl;
		
END gm_fetch_txn_insert_info;

/****************************************************************************
  * Description : Procedure to fetch the Request Type for the Request id.
  * Author   	: karthik
  ****************************************************************************/
PROCEDURE gm_fch_request_type
  (
    p_req_id   	   	   IN 		T504_CONSIGNMENT.C504_CONSIGNMENT_ID%TYPE,
    p_out_ref_type	   OUT		T520_REQUEST.C520_REQUEST_FOR%TYPE
   )
AS
	v_req_type T520_REQUEST.C520_REQUEST_FOR%TYPE;
	v_req_id   T520_REQUEST.C520_REQUEST_ID%TYPE;
BEGIN
  --	
  BEGIN
		SELECT C520_REQUEST_ID 
			 INTO v_req_id
			FROM T504_CONSIGNMENT
			WHERE C504_CONSIGNMENT_ID = p_req_id
			AND C504_VOID_FL    IS NULL;
		EXCEPTION 
		   		WHEN NO_DATA_FOUND THEN
		   		v_req_id := NULL;
		END; 
		
  	BEGIN
		SELECT C520_REQUEST_FOR 
			 INTO v_req_type
			FROM T520_REQUEST
			WHERE C520_REQUEST_ID = v_req_id
			AND C520_VOID_FL    IS NULL
			GROUP BY C520_REQUEST_FOR;
		EXCEPTION 
		   		WHEN NO_DATA_FOUND THEN
		   		v_req_type := NULL;
		END; 
		p_out_ref_type := v_req_type;
        
END gm_fch_request_type;

/********************************************************************************
  * Description : Procedure to fetch the transaction id for the PSFG Transaction
  * Author   	: karthik
  *******************************************************************************/
PROCEDURE gm_fch_PSFG_ref_id
  (
    p_ref_id   	   	   IN 		T412_INHOUSE_TRANSACTIONS.C412_INHOUSE_TRANS_ID%TYPE,
    p_out_txn_id	   OUT		T412_INHOUSE_TRANSACTIONS.C412_REF_ID%TYPE
   )
AS
	v_txn_id T412_INHOUSE_TRANSACTIONS.C412_REF_ID%TYPE;
BEGIN
  --	
  BEGIN
		SELECT C412_REF_ID 
				INTO v_txn_id
			FROM T412_INHOUSE_TRANSACTIONS
			WHERE C412_INHOUSE_TRANS_ID = p_ref_id
			AND c412_void_fl IS NULL;
		EXCEPTION 
	   		WHEN NO_DATA_FOUND THEN
	   		v_txn_id := NULL;
		END; 
		p_out_txn_id := v_txn_id;
        
END gm_fch_PSFG_ref_id;

/******************************************************************
* Description : function to get shipto division Company
* Author      : Karthik
****************************************************************/
FUNCTION get_ship_to_division_company(
    p_refid IN t501_order.c501_order_id%TYPE
  )
  RETURN VARCHAR2
IS
  v_ship 		VARCHAR2(50);
  v_shipto_id 	t907_shipping_info.c907_ship_to_id%TYPE;
  v_company_id	t1900_company.c1900_company_id%TYPE;
BEGIN
  --
	  BEGIN
	    SELECT t907.c901_ship_to ,
	      t907.c907_ship_to_id
	    INTO v_ship ,
	      v_shipto_id
	    FROM t907_shipping_info t907
	    WHERE t907.c907_ref_id = p_refid
	    AND c907_void_fl      IS NULL;
	    
	    IF v_ship = '4120' THEN --Distributor
	      SELECT DISTINCT DIVID
	      INTO v_company_id
	      FROM V700_TERRITORY_MAPPING_DETAIL
	      WHERE D_ID = v_shipto_id;
	      
	    ELSIF (v_ship = '4121' OR v_ship = '4000642') THEN --Sales Rep & Shipping Account
	      SELECT DISTINCT DIVID
	      INTO v_company_id
	      FROM V700_TERRITORY_MAPPING_DETAIL
	      WHERE REP_ID = v_shipto_id;
	      
	    ELSIF v_ship = '4122' THEN --Hospital
	      SELECT DISTINCT DIVID
	      INTO v_company_id
	      FROM V700_TERRITORY_MAPPING_DETAIL
	      WHERE AC_ID = v_shipto_id;
	      
	    ELSIF v_ship = '50625' THEN --Employee
	      SELECT DISTINCT DECODE(t101p.C1900_COMPANY_ID,'1000','100823','1001','100823','1018','100823','100824')
	      INTO v_company_id
	      FROM t101_user t101,
	        t907_shipping_info t907,
	        t101_party t101p
	      WHERE t101.c101_user_id = v_shipto_id
	      AND t907.c907_ref_id    = p_refid
	      AND t101p.C101_PARTY_ID = t101.C101_PARTY_ID
	      AND t907.c907_void_fl  IS NULL;
	    END IF;
	    
	  EXCEPTION
	  WHEN NO_DATA_FOUND THEN
	    RETURN '';
	  END;
  RETURN v_company_id;
END get_ship_to_division_company;

/****************************************************************************
* Description : Procedure to fetch the insert details for the Transaction.
* Author    : Vprasath
****************************************************************************/
PROCEDURE gm_fch_insert_type(
    p_refid   IN T5056_TRANSACTION_INSERT.C5056_REF_ID%TYPE,
    p_reftype IN T5056_TRANSACTION_INSERT.C901_REF_TYPE%TYPE,
    p_out_dtl OUT TYPES.cursor_type 
    )
AS
  v_plant_id t5040_plant_master.c5040_plant_id%TYPE;
  v_company_id t1900_company.c1900_company_id%TYPE;
  v_out_value NUMBER := 0 ;
  v_cnt       NUMBER;
  v_rule_group 	  T906_RULES.C906_RULE_GRP_ID%TYPE;
BEGIN
	  SELECT get_plantid_frm_cntx() ,
	    get_compid_frm_cntx()
	  INTO v_plant_id ,
	    v_company_id
	  FROM DUAL;
	  
	  SELECT COUNT(1)
	  INTO v_out_value
	  FROM t906_rules t906
	  WHERE t906.c906_rule_id   = v_plant_id
	  AND t906.c906_rule_grp_id = 'INSERT_PART';
	  
	   v_rule_group := 'INSERT_TXN_MAP'||'_'||v_plant_id;
	   
	  IF v_out_value <> 0 THEN
	    BEGIN
	      SELECT TO_NUMBER(T906.C906_RULE_VALUE) * TO_NUMBER(NVL(GET_RULE_VALUE(v_plant_id, 'INSERT_TYPE' ),0))
                INTO v_out_value
                 FROM T906_RULES T906                                                                           
                WHERE T906.C906_RULE_GRP_ID = v_rule_group
                  AND T906.C906_RULE_ID = p_reftype
                  AND T906.C906_VOID_FL IS NULL;
	    EXCEPTION
	    WHEN NO_DATA_FOUND THEN
	      v_out_value := 0;
	    END;
	    
	    IF v_out_value <> 0 THEN
	      SELECT COUNT(1)
	      INTO v_cnt
	      FROM t5056_transaction_insert t5056
	      WHERE t5056.c5056_ref_id = p_refid
	      AND t5056.C901_STATUS    = 93342 -- Not Picked
	      AND t5056.c5056_void_fl IS NULL;
	      
	      IF v_cnt       = 0 THEN
	        v_out_value := 0;
	      END IF;
	      
	    END IF;
	    
	  END IF;
	  
	  OPEN p_out_dtl FOR 
	  	SELECT v_out_value TYPE 
	  		FROM DUAL;
  		
END gm_fch_insert_type;

	/****************************************************************************
	* Description : Function to get location id from location cd
	* Author    : Arajan
	****************************************************************************/
	FUNCTION get_location_id_from_cd(
		p_location_cd		IN		t5052_location_master.c5052_location_cd%TYPE,
		p_screen_typ		IN		VARCHAR2 DEFAULT NULL
	)
	RETURN	t5052_location_master.c5052_location_id%TYPE
	IS
		v_plant_id		t5040_plant_master.c5040_plant_id%TYPE;
		v_location_id	t5052_location_master.C5052_Location_Id%TYPE;
	BEGIN
		
		SELECT get_plantid_frm_cntx()
	  	INTO v_plant_id
	  	FROM DUAL;
		-- To get the location id from location cd
	  	-- 93311: Inactive; 1: FG warehouse
		BEGIN
		  	SELECT C5052_Location_Id
				INTO v_location_id
			FROM t5052_location_master
			WHERE c5040_plant_id       = v_plant_id
			AND c5052_void_fl         IS NULL
			AND c5052_location_cd      = p_location_cd
			AND c901_status           <> 93311
			AND c5051_inv_warehouse_id = 1;
		EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			IF p_screen_typ = 'Edit'
			THEN
				RETURN NULL;
			END IF;
			
			BEGIN
				SELECT C5052_Location_Id
					INTO v_location_id
				FROM t5052_location_master
				WHERE c5040_plant_id       = v_plant_id
				AND c5052_void_fl         IS NULL
				AND c5052_location_id      = p_location_cd
				AND c901_status           <> 93311
				AND c5051_inv_warehouse_id = 1;
			EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				v_location_id := NULL;
			END;
		END;
		
		RETURN v_location_id;

	END get_location_id_from_cd;

END gm_pkg_op_fch_insert_details;
/