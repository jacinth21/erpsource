--@"c:\database\packages\operations\GM_PKG_OP_INV_WAREHOUSE.BDY";

create or replace
PACKAGE BODY gm_pkg_op_inv_warehouse
IS
  /**********************************************************************
  * Description :  This function will get the Warehouse Name for the passed in Warehouse ID or the Location ID.
  * Author  : Elango
  **********************************************************************/
FUNCTION GET_INV_WAREHOUSE_NM (
    p_location_id t5052_location_master.c5052_location_id%TYPE ,
    p_warehouse_id T5051_INV_WAREHOUSE.C5051_INV_WAREHOUSE_ID%TYPE 
  )
  RETURN VARCHAR2
  IS
  v_warehouse_id T5051_INV_WAREHOUSE.C5051_INV_WAREHOUSE_ID%TYPE;
  v_warehouse_nm T5051_INV_WAREHOUSE.C5051_INV_WAREHOUSE_NM%TYPE;
BEGIN
  v_warehouse_id    := p_warehouse_id;
  IF p_warehouse_id IS NULL AND p_location_id IS NOT NULL THEN
    SELECT C5051_INV_WAREHOUSE_ID
      INTO v_warehouse_id
      FROM T5052_LOCATION_MASTER
     WHERE C5052_LOCATION_ID = p_location_id
       AND C5052_VOID_FL      IS NULL;
  END IF;
  BEGIN
    SELECT C5051_INV_WAREHOUSE_NM
      INTO v_warehouse_nm
      FROM T5051_INV_WAREHOUSE
     WHERE C901_STATUS_ID       = '1'
       AND C5051_INV_WAREHOUSE_ID = v_warehouse_id ;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    v_warehouse_nm:='';
  END;
  RETURN v_warehouse_nm;
END GET_INV_WAREHOUSE_NM;
--
  /*******************************************************
   * Description : Function to get warehouse Short name
   * Author 		  : Yoga
   *******************************************************/
--
  FUNCTION get_warehouse_sh_name (
    p_wh_type  IN  t5051_inv_warehouse.C901_WAREHOUSE_TYPE%TYPE,
    p_wh_id  IN  t5051_inv_warehouse.C5051_INV_WAREHOUSE_ID%TYPE
  )
  RETURN VARCHAR
  IS
  v_wh_sh_nm t5051_inv_warehouse.C5051_WAREHOUSE_SH_NM%TYPE;
  BEGIN
  
    BEGIN
      SELECT t5051.C5051_WAREHOUSE_SH_NM WAREHOUSE_SH_NM
        INTO v_wh_sh_nm
        FROM t5051_inv_warehouse t5051
       WHERE t5051.C901_WAREHOUSE_TYPE = NVL(p_wh_type,t5051.C901_WAREHOUSE_TYPE)
         AND t5051.C5051_INV_WAREHOUSE_ID = NVL(p_wh_id,t5051.C5051_INV_WAREHOUSE_ID)      
          AND t5051.C901_STATUS_ID = 1; -- Active
    
    EXCEPTION WHEN NO_DATA_FOUND THEN
      v_wh_sh_nm:='';
    END;
  
  RETURN v_wh_sh_nm;
  
  END get_warehouse_sh_name;
  
  /*******************************************************
   * Description : Procedure to fetch Warehouse details fo given transaction id
   * author		: Yoga
   *******************************************************/
   PROCEDURE gm_fch_warehouse_dtl (
     p_txn_type	   IN		NUMBER,
     p_out_dtl	   OUT		TYPES.cursor_type
   )
   AS
   v_txn_type NUMBER;
   v_whtype NUMBER;

   BEGIN
    
     -- v_txn_type := GET_CODE_NAME_ALT(p_txn_type);
       v_whtype := get_rule_value('TRANS_TYPE', p_txn_type);

       OPEN p_out_dtl
         FOR
           SELECT C5051_INV_WAREHOUSE_ID ID, C5051_WAREHOUSE_SH_NM WAREHOUSE_SH_NM,C901_WAREHOUSE_TYPE WAREHOUSE_TYPE, C5051_INV_WAREHOUSE_NM WHNM
             FROM t5051_inv_warehouse
            WHERE C901_STATUS_ID = 1-- Active
          AND c5051_inv_warehouse_id NOT IN
			(SELECT c906_rule_value
			   FROM t906_rules
			    WHERE c906_rule_id     = 'WHID'
			      AND c906_rule_grp_id = 'RULEWH'
			      AND c906_void_fl    IS NULL)--excluding FS and account warehouse
	      AND C901_WAREHOUSE_TYPE = DECODE(v_whtype,NULL,C901_WAREHOUSE_TYPE,v_whtype);               
   END gm_fch_warehouse_dtl;
  
  /*******************************************************
   * Description : Function to get warehouse name
   * Author 		  : Yoga
   *******************************************************/
--
  FUNCTION get_warehouse_name (
    p_wh_type  IN  t901_code_lookup.C901_code_id%TYPE,
    p_wh_id  IN  t5051_inv_warehouse.C5051_INV_WAREHOUSE_ID%TYPE
  )
  RETURN VARCHAR
  IS
  v_wh_nm t5051_inv_warehouse.C5051_INV_WAREHOUSE_NM%TYPE;
  BEGIN
      BEGIN
        SELECT t5051.C5051_INV_WAREHOUSE_NM WAREHOUSE_NM
          INTO v_wh_nm
          FROM t5051_inv_warehouse t5051 
         WHERE t5051.C901_WAREHOUSE_TYPE = NVL(p_wh_type,t5051.C901_WAREHOUSE_TYPE)
            AND t5051.C5051_INV_WAREHOUSE_ID = NVL(p_wh_id,t5051.C5051_INV_WAREHOUSE_ID)
            AND t5051.C901_STATUS_ID = 1; -- Active;
    
      EXCEPTION WHEN NO_DATA_FOUND THEN
        v_wh_nm:= NULL;
      END;

    RETURN v_wh_nm;
  
  END get_warehouse_name;

    /*******************************************************
   * Description : Function to get warehouse Id
   * Author 		  : Yoga
   *******************************************************/
--
  FUNCTION get_warehouse_id (
    p_wh_type  IN  t901_code_lookup.C901_code_id%TYPE
  )
  RETURN NUMBER
  IS
  v_wh_id t5051_inv_warehouse.C5051_INV_WAREHOUSE_ID%TYPE;
  BEGIN
      BEGIN
        SELECT t5051.C5051_INV_WAREHOUSE_ID WAREHOUSE_ID
          INTO v_wh_id
          FROM t5051_inv_warehouse t5051
         WHERE  NVL(t5051.C901_WAREHOUSE_TYPE,'-9999')= DECODE(p_wh_type,NULL, NVL(t5051.C901_WAREHOUSE_TYPE,'-9999'), NVL(p_wh_type,'-9999')) 
            AND t5051.C901_STATUS_ID = 1; -- Active;
      EXCEPTION WHEN NO_DATA_FOUND THEN
        v_wh_id:= -1;
      END;

    RETURN v_wh_id;
  
  END get_warehouse_id;
  /**************************************************************************************
   * Description : This Procedure is used to fetch the excluded inventory buckets mapping 
   * by sutracting the included and excluded inventory buckets
   * Author      : HReddi 
   **************************************************************************************/
   PROCEDURE gm_fch_InvExclude_Map(
      p_includeInvBuckets   IN   VARCHAR2,
      p_excludeInvBuckets   IN   VARCHAR2,
      p_excluded_mapping    OUT  TYPES.cursor_type
   )
   AS
   BEGIN	 
	  my_context.set_my_inlist_ctx (p_includeInvBuckets);
	  my_context.set_my_double_inlist_ctx (p_excludeInvBuckets, '');	  
	  OPEN p_excluded_mapping FOR  
 		SELECT c901_code_id excludemapping FROM t901a_lookup_access WHERE c901_access_code_id IN (SELECT token FROM v_double_in_list)
  		MINUS
  		SELECT c901_code_id excludemapping FROM t901a_lookup_access WHERE c901_access_code_id IN (SELECT token from v_in_list where token is not null);
   END gm_fch_InvExclude_Map;   
   
   /*******************************************************
   *	Description : Procedure to fetch Warehouse details
   *	Author		: Jignesh Shah
   *******************************************************/
   PROCEDURE gm_fetch_inv_warehouse_dtls (
     p_txn_type		IN		NUMBER,
     p_code_grp		IN		VARCHAR2,
     p_dept_id		IN		NUMBER,
     p_out_dtl		OUT		TYPES.cursor_type
   )
   AS
   
   v_whtype 		NUMBER;
   v_company_id     t1900_company.c1900_company_id%TYPE;
   BEGIN
   --
		SELECT get_compid_frm_cntx() 
      		INTO v_company_id  
      	FROM dual;
    --  	
		v_whtype := get_rule_value('TRANS_TYPE', p_txn_type);

		OPEN p_out_dtl
		FOR
			SELECT	t5051.c5051_inv_warehouse_id id
					,t5051.c5051_inv_warehouse_nm WHNM
					,t5051.c901_warehouse_type warehouse_type
					,t5051.c5051_warehouse_sh_nm warehouse_sh_nm
			FROM t5051_inv_warehouse t5051,t901_code_lookup t901 
			WHERE t901.c902_code_nm_alt = t5051.c901_warehouse_type
			AND t5051.c901_warehouse_type = decode(v_whtype,NULL,t5051.c901_warehouse_type,v_whtype)
			AND t901.c901_code_grp = p_code_grp
			AND t901.c901_code_id	IN (SELECT t901a.c901_code_id
										FROM t901a_lookup_access t901a
										WHERE t901a.c901_access_code_id IN (p_dept_id)
										AND t901a.c1900_company_id = v_company_id
										)
			AND t5051.c901_status_id = 1-- active
			AND t901.c901_void_fl IS NULL;

   END gm_fetch_inv_warehouse_dtls;
   
   /*********************************************************************************************************************
	* Description : This procedure will get the warehouse address details for FGRP-paerwork if warehouse difference only.
	* Author  : HReddi
	*********************************************************************************************************************/
PROCEDURE gm_fch_picslip_info(
    p_transid   IN t5055_control_number.c5055_ref_id%TYPE ,
    p_transtype IN t5055_control_number.c901_ref_type%TYPE ,
    p_wh_fromaddr OUT VARCHAR2 ,
    p_wh_toaddr OUT VARCHAR2 ,
    p_wh_consign_name OUT t701_distributor. C701_DISTRIBUTOR_NAME%TYPE ,
    p_packslip_header OUT VARCHAR2 )
AS
  v_whfrom_id        VARCHAR(20);
  v_whto_id          VARCHAR(20);
  v_whfromcnty_id    VARCHAR(20);
  v_whtocnty_id      VARCHAR(20);
  v_whtoconsign_name VARCHAR(100);
  v_whfrom_addr      VARCHAR(200);
  v_whto_addr        VARCHAR(200);
  v_packslip_header  VARCHAR(200);
BEGIN
  BEGIN
    SELECT c5051_inv_warehouse_id
    INTO v_whfrom_id
    FROM t5052_location_master t5052,
      t5055_control_number t5055
    WHERE t5052.c5052_location_id = t5055.c5052_location_id
    AND t5055.c901_type          IN ('93342') -- 93342 PICK LOC
    AND t5055.c5055_ref_id        = p_transid
    AND t5055.c901_ref_type       = p_transtype
    AND t5052.c5052_void_fl      IS NULL
    AND t5052.c5051_inv_warehouse_id NOT IN
	(SELECT c906_rule_value
	   FROM t906_rules
	    WHERE c906_rule_id     = 'WHID'
	      AND c906_rule_grp_id = 'RULEWH'
	      AND c906_void_fl    IS NULL)--excluding FS and account warehouse
    AND t5055.c5055_void_fl      IS NULL
    GROUP BY c5051_inv_warehouse_id;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    v_whfrom_id := NULL;
  WHEN TOO_MANY_ROWS THEN  
    v_whfrom_id := NULL;
  END;
  --THE ABOVE QUERY IS USED FOR GET THE FROM LOCATION WAREHOUSE ID VALUE.
  BEGIN
    SELECT c5051_inv_warehouse_id
    INTO v_whto_id
    FROM t5052_location_master t5052,
      t5055_control_number t5055
    WHERE t5052.c5052_location_id = t5055.c5052_location_id
    AND t5055.c901_type          IN ('93344') --93342 PUT LOC
    AND t5055.c5055_ref_id        = p_transid
    AND t5052.c5051_inv_warehouse_id NOT IN
	(SELECT c906_rule_value
	   FROM t906_rules
	    WHERE c906_rule_id     = 'WHID'
	      AND c906_rule_grp_id = 'RULEWH'
	      AND c906_void_fl    IS NULL)--excluding FS and account warehouse
    AND t5055.c901_ref_type       = p_transtype AND t5052.c5052_void_fl IS NULL
    AND t5055.c5055_void_fl      IS NULL
    GROUP BY c5051_inv_warehouse_id;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    v_whto_id := NULL;
  WHEN TOO_MANY_ROWS THEN  
    v_whto_id := NULL;  
  END;
  -- THE ABOVE QUERY IS USED FOR GET THE TO LOCATION WAREHOUSE ID VALUE.
  IF v_whfrom_id != v_whto_id AND v_whfrom_id IS NOT NULL AND v_whto_id IS NOT NULL THEN
    BEGIN
      SELECT C901_WH_CNTY_ID
      INTO v_whfromcnty_id
      FROM t5051_inv_warehouse
      WHERE C5051_INV_WAREHOUSE_ID = v_whfrom_id;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
      v_whfromcnty_id := NULL;
    END;
    -- THE ABOVE QUERY IS USED FOR GET THE FROM WAREHOUSE COUNTRY ID
    BEGIN
      SELECT C901_WH_CNTY_ID
      INTO v_whtocnty_id
      FROM t5051_inv_warehouse
      WHERE C5051_INV_WAREHOUSE_ID = v_whto_id;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
      v_whtocnty_id := NULL;
    END;
    -- THE ABOVE QUERY IS USED FOR GET THE TO WAREHOUSE COUNTRY ID
    BEGIN
      SELECT t701.C701_DISTRIBUTOR_NAME
      INTO v_whtoconsign_name
      FROM t701_distributor t701
      WHERE t701.C701_SHIP_COUNTRY = v_whtocnty_id
      AND t701.C701_ACTIVE_FL      = 'Y'
      AND t701.C701_VOID_FL       IS NULL
      AND ROWNUM                   =1;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
      v_whtoconsign_name := NULL;
    END;
    -- THE ABOVE QUERY IS USED FOR GET THE TO WAREHOUSE DISTRIBUTOR NAME
    BEGIN
      SELECT t701.c701_distributor_name
        || '<BR>&nbsp'
        || t701.c701_ship_add1
        || '<BR>&nbsp'
        || t701.c701_ship_add2
        || '<BR>&nbsp'
        || get_code_name(t701.C701_SHIP_COUNTRY)
        || ',&nbsp'
        || get_code_name(t701.c701_ship_state)
        || ',&nbsp'
        || t701.C701_SHIP_ZIP_CODE
      INTO v_whto_addr
      FROM t701_distributor t701
      WHERE C701_SHIP_COUNTRY =v_whtocnty_id
      AND t701.c701_active_fl = 'Y'
      AND t701.c701_void_fl  IS NULL
      AND rownum              =1;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
      v_whto_addr := NULL;
    END;
    -- THE ABOVE QUERY IS USED FOR GET THE TO WAREHOUSE SHIP ADDRESS
    BEGIN
      SELECT t701.c701_ship_add1
        || '<BR>'
        || t701.c701_ship_add2
        || '<BR>'
        || get_code_name(t701.C701_SHIP_COUNTRY)
        || ',&nbsp'
        || get_code_name(t701.c701_ship_state)
        || ',&nbsp'
        || t701.C701_SHIP_ZIP_CODE
      INTO v_whfrom_addr
      FROM t701_distributor t701
      WHERE C701_SHIP_COUNTRY =v_whfromcnty_id
      AND t701.c701_active_fl = 'Y'
      AND t701.c701_void_fl  IS NULL
      AND rownum              =1;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
      v_whfrom_addr := NULL;
    END;
    -- THE ABOVE QUERY IS USED FOR GET THE FROM WAREHOUSE SHIP ADDRESS
    SELECT get_rule_value('FGLT_Diff_Warehouse','PIC_SLIP_HEADER')
    INTO v_packslip_header
    FROM dual;
    -- THE ABOVE QUERY IS USED FOR GET THE PICSLIP HEADER VALUE
  END IF;
  p_wh_fromaddr     := v_whfrom_addr;
  p_wh_toaddr       := v_whto_addr;
  p_wh_consign_name := v_whtoconsign_name;
  p_packslip_header := v_packslip_header;
END gm_fch_picslip_info;

 /***************************************************************************
   *	Description : Procedure to fetch Part Reserved Details for Ack Order
   *	Author		: Harinadh Reddi
   **************************************************************************/
   PROCEDURE gm_fetch_part_resrve_dtls (
     p_part_number	IN		t5060_control_number_inv.c205_part_number_id%TYPE,
     p_trans_id     IN      VARCHAR2,
     p_out_dtl		OUT		TYPES.cursor_type
   )
   AS  
   	v_datefmt VARCHAR2(20);
   	v_company_id NUMBER;
   	v_plant_id   NUMBER;
   BEGIN	  
	   
	   SELECT get_compid_frm_cntx(), get_plantid_frm_cntx()
         INTO v_company_id, v_plant_id  
         FROM DUAL;
	   
	   v_datefmt := NVL(GET_RULE_VALUE('DATEFMT','DATEFORMAT'),'MM/DD/YYYY');
	   OPEN p_out_dtl
		FOR
				select * from     
				(select t5060.c5060_control_number_inv_id PARTINVID
			     , t5060.c205_part_number_id  PARTNUM
			     , get_partnum_desc (t5060.c205_part_number_id) PARTDESC
			     , t5060.c5060_control_number CTRLNUM
			     , nvl(t5060.c5060_qty,0)  exct_available_qty
			     , nvl(t5062.c5062_reserved_qty, 0) reserved_qty
			     , Nvl(Other_Reserve.Qty,0) Other_Reserve_Qty
			     , DECODE(sign((Nvl(T5060.C5060_Qty,0) - Nvl(T5062.C5062_Reserved_Qty, 0) - Nvl(Other_Reserve.Qty,0)))
     					,'-1','0',(NVL(t5060.c5060_qty,0) - NVL(t5062.c5062_reserved_qty, 0) - NVL(Other_Reserve.Qty,0))) available_qty     
			  from t5060_control_number_inv t5060
			     , t5062_control_number_reserve t5062
			     , (select t5060.c205_part_number_id
			             , t5060.c5060_control_number
			             , sum(t5062.c5062_reserved_qty) qty
			          from t5062_control_number_reserve t5062
			             , t5060_control_number_inv t5060
			         where t5060.c5060_control_number_inv_id = t5062.c5060_control_number_inv_id (+)
			           and t5062.c901_transaction_type = '103980'
			           and t5060.c205_part_number_id =  p_part_number
			           and t5062.c901_status in ('103960','103961')
			           and t5062.c5062_transaction_id <> p_trans_id
			           and T5062.C5062_Void_Fl is null
			      GROUP BY t5060.c205_part_number_id, t5060.c5060_control_number ) other_reserve
			where t5060.c205_part_number_id =  p_part_number
			   and t5060.c5060_control_number_inv_id = t5062.c5060_control_number_inv_id (+)
			   and t5062.c901_transaction_type (+) = '103980'
			   and t5062.c901_status (+) in ('103960','103961') -- open && in progress
			   and t5062.c5062_transaction_id(+) = p_trans_id
			   and t5060.c901_warehouse_type = '90800' -- Inventory(FG) TYPE
			   and t5060.c205_part_number_id = other_reserve.c205_part_number_id (+)
			   AND t5060.c5060_control_number = other_reserve.c5060_control_number (+)
			   and t5062.c5062_void_fl is null
			   and t5060.c1900_company_id = v_company_id
               and t5060.c5040_plant_id = v_plant_id
               and t5060.c1900_company_id = t5062.c1900_company_id(+)
               and t5060.c5040_plant_id = t5062.c5040_plant_id(+)
			   ) main_sql
			  , 
			   (  
			     select t2550.c205_part_number_id PNUM
			          , t2550.c2550_control_number CNUM
			          , t2540.c2540_donor_number DONORNUM
			          , get_code_name(t2540.c901_sex) SEX
			          , t2540.c2540_age AGE
			          , to_char(t2550.C2550_Expiry_Date,v_datefmt) EXPDATE 
			          , get_code_name(t2540.c901_international_use) INTLUSE
			          , get_part_size(t2550.c205_part_number_id,t2550.c2550_control_number) partsize
			       from t2550_part_control_number t2550
			          , t2540_donor_master t2540
			      where t2550.c2540_donor_id = t2540.c2540_donor_id
			        and t2550.c205_part_number_id = p_part_number
			        and t2540.c2540_void_fl is null
			        and t2550.c2550_lot_status not in (105000,105006)
                    and NVL(t2550.C901_LOT_CONTROLLED_STATUS,'-999') NOT IN (105008) 
			        and t2540.c1900_company_id = v_company_id 
			   ) other_details
			where main_sql.PARTNUM = other_details.PNUM (+)
			  and main_sql.CTRLNUM = other_details.CNUM (+)
			  and (reserved_qty != 0 OR available_qty != 0); 
			  
	   END gm_fetch_part_resrve_dtls;
	   
	/***********************************************************************
   *	Description : Procedure to save Part Reserved Details for Ack Order
   *	Author		: Harinadh Reddi
   **************************************************************************/
   PROCEDURE gm_save_part_resrve (
     p_input_String	IN		VARCHAR2,
     p_user_id      IN      VARCHAR2    
   )
   AS
   		v_strlen	    NUMBER := NVL (LENGTH (p_input_String), 0);
		v_string	    VARCHAR2 (30000) := p_input_String;
		v_substring     VARCHAR2 (1000);
		v_reservQty  VARCHAR2(10);
		v_transid    VARCHAR2(30);
		v_partnum	 VARCHAR2(20);
		v_partinvID  VARCHAR2(30);
		v_reserve_id	t5062_control_number_reserve.C5062_CONTROL_NUMBER_RESV_ID%type;
		v_company_id NUMBER;
		v_plant_id  NUMBER;
   BEGIN
	   
	   SELECT get_compid_frm_cntx(), get_plantid_frm_cntx()
         INTO v_company_id, v_plant_id  
         FROM DUAL;
	   		
		   WHILE INSTR(v_string,'|') <> 0 
			LOOP
		    	v_substring 	:= SUBSTR(v_string,1,INSTR(v_string,'|')-1);
		    	v_string    	:= SUBSTR(v_string,INSTR(v_string,'|')+1);
				v_partnum		:= NULL;
				v_transid		:= NULL;
				v_partinvID		:= NULL;					
				v_reservQty		:= NULL;
				v_partinvID		:= SUBSTR(v_substring,1,INSTR(v_substring,'^')-1);
				v_substring 	:= SUBSTR(v_substring,INSTR(v_substring,'^')+1);			  
				v_partnum		:= SUBSTR(v_substring,1,INSTR(v_substring,'^')-1);
				v_substring 	:= SUBSTR(v_substring,INSTR(v_substring,'^')+1);	  			  
				v_transid		:= SUBSTR(v_substring,1,INSTR(v_substring,'^')-1);
				v_substring 	:= SUBSTR(v_substring,INSTR(v_substring,'^')+1);						  
				v_reservQty 	:= v_substring;		
		 		
				IF v_reservQty = '0' THEN				
				    DELETE 
				      FROM t5062_control_number_reserve 
				     WHERE c5060_control_number_inv_id = v_partinvID
   				       AND c5062_transaction_id = v_transid
   				       AND c1900_company_id = v_company_id
   				       AND c5040_plant_id = v_plant_id
				       AND c5062_void_fl IS NULL;
			    ELSE				
					UPDATE t5062_control_number_reserve
	   				   SET c5062_reserved_qty = v_reservQty
	     				 , c5062_last_updated_by = p_user_id
	     			     , c5062_last_updated_date = SYSDATE
	     			     , c1900_company_id = v_company_id
   				         , c5040_plant_id = v_plant_id
	 				 WHERE c5060_control_number_inv_id = v_partinvID
	   				   AND c5062_transaction_id = v_transid
	   				   AND c5062_void_fl IS NULL;
	   				   
	   				 IF (SQL%ROWCOUNT = 0) THEN
   					   SELECT S5062_CONTROL_NUMBER_RESERVE.NEXTVAL
					     INTO v_reserve_id FROM DUAL;	
   				
					INSERT 
					  INTO t5062_control_number_reserve(C5062_CONTROL_NUMBER_RESV_ID, C5062_RESERVED_QTY
					      ,C5060_CONTROL_NUMBER_INV_ID,C901_TRANSACTION_TYPE,C5062_TRANSACTION_ID,C5062_RESERVED_BY
					      ,C5062_RESERVED_DATE,C901_STATUS, c1900_company_id , c5040_plant_id) 
					VALUES(v_reserve_id,v_reservQty,v_partinvID
					      ,'103980',v_transid,p_user_id
					      ,SYSDATE,'103960' , v_company_id , v_plant_id);   -- 103960[Open]
				     END IF;	  
				    
   				END IF;
   				
   				
			END LOOP;	   
   END gm_save_part_resrve;
   
   
   /**************************************************************************************
   * Description : Function to get the Reserved Quantity for the Corresponding Ack Order
   * Author		 : Harinadh Reddi
   **************************************************************************************/
   
   	FUNCTION GET_RESERVED_QTY (
    	p_trans_id T5062_Control_Number_Reserve.C5062_Transaction_Id%TYPE ,
    	p_part_num T5060_Control_Number_Inv.C205_Part_Number_Id%TYPE 
   	)
   	RETURN VARCHAR2
   	IS
  		v_reserved_qty T5062_Control_Number_Reserve.c5062_reserved_qty%TYPE;
  		v_trans_id VARCHAR2(2000);
  	BEGIN
	  	/* The following code is commented due to not showing the Reserved Qty column in Item Details Screen */
		/*BEGIN		
			 SELECT (RTRIM(XMLAGG(XMLELEMENT(E,c501_order_id || ',')).EXTRACT('//text()').GETCLOBVAL(),',') )
		       INTO v_trans_id 
	           FROM ( SELECT c501_order_id FROM t501_order WHERE c501_ref_id = p_trans_id AND c501_void_fl IS NULL);			
  			EXCEPTION
  			WHEN NO_DATA_FOUND THEN
    			v_trans_id:= p_trans_id;
    	END;
    	 IF v_trans_id IS NULL THEN
    	 	v_trans_id:= p_trans_id;
    	 END IF;
    	 
    	 v_trans_id := v_trans_id ||','||p_trans_id;
    	 my_context.set_my_inlist_ctx (v_trans_id);*/
    	 
    	BEGIN
	    	SELECT SUM(T5062.c5062_reserved_qty) 
	    	  INTO v_reserved_qty                            
	          FROM T5060_Control_Number_Inv T5060,T5062_Control_Number_Reserve T5062
	         WHERE t5060.C5060_Control_Number_Inv_Id  = t5062.C5060_Control_Number_Inv_Id 
	           AND T5062.C5062_Transaction_Id IN (p_trans_id)
	           AND T5060.C205_Part_Number_Id = p_part_num
	           AND T5062.c5062_void_fl IS NULL;
    	EXCEPTION
  			WHEN NO_DATA_FOUND THEN
    			v_reserved_qty:= 0; 
    	END;		
  		RETURN v_reserved_qty;   	
	END GET_RESERVED_QTY;
	
	/*************************************************************************
    * Description : Procedure to fetch Part Reserved Details for Ack Order
    * Author		: Harinadh Reddi
    **************************************************************************/
   	PROCEDURE gm_fch_reserved_qty (
     	p_trans_id     IN      VARCHAR2,
     	p_out_dtl		OUT		TYPES.cursor_type
   	)
   	AS
   	BEGIN
	   OPEN p_out_dtl FOR 
	     SELECT SUM(T5062.c5062_reserved_qty) RESERVEQTY 
	          , t5060.c205_part_number_id PARTNUM
	          , get_user_name(t5062.c5062_reserved_by ) reservedby 
	          , to_char(t5062.c5062_reserved_date,get_rule_value('DATEFMT','DATEFORMAT')) resereddate
	          , t5060.c5060_control_number CNUM
	          FROM T5060_Control_Number_Inv T5060,T5062_Control_Number_Reserve T5062
	         WHERE t5060.C5060_Control_Number_Inv_Id  = t5062.C5060_Control_Number_Inv_Id 
	           AND T5062.C5062_Transaction_Id = p_trans_id
               AND T5060.C205_Part_Number_Id IN (SELECT c205_part_number_id FROM t502_item_order WHERE c501_order_id = p_trans_id AND c502_void_fl IS NULL)
	           AND T5062.c5062_void_fl IS NULL
          GROUP BY t5060.c205_part_number_id,t5062.c5062_reserved_by , t5062.c5062_reserved_date , t5060.c5060_control_number;
	   
   END gm_fch_reserved_qty;

    /************************************************************************************************
	 * Description : This Procedure is used to fetch the Current Warehouse for input Part and Control
	 * Author	   : HReddi
	 *************************************************************************************************/
	PROCEDURE gm_fetch_curr_warehouse (
	  p_part_num		IN		VARCHAR2
	, p_cntl_num		IN		VARCHAR2
	, p_out_warehouse   OUT	   	VARCHAR2
	)
	AS
		v_plant_id      t5040_plant_master.c5040_plant_id%TYPE;
		v_count         NUMBER;
	    v_company_id	t1900_company.c1900_company_id%TYPE;
        v_flag 			VARCHAR2(1);
        v_out 			VARCHAR2(2000);
        v_lot_flag 		VARCHAR2(1);
        v_viacell_count NUMBER;
        v_plant         NUMBER;
	BEGIN
		SELECT  get_compid_frm_cntx() , NVL(get_plantid_frm_cntx(),get_rule_value('DEFAULT_PLANT','ONEPORTAL'))
			INTO  v_company_id , v_plant_id
			 FROM DUAL;
		
	   gm_chk_lot_track_enable(p_part_num , p_cntl_num ,v_company_id,v_lot_flag);
	   
	   IF v_lot_flag = 'N' THEN
	     RETURN;
	    END IF;
	    
	    SELECT COUNT(1) 
            INTO v_viacell_count
              FROM V205_PARTS_WITH_EXT_VENDOR
              WHERE C205_PART_NUMBER_ID = p_part_num
              AND C1900_COMPANY_ID = V_COMPANY_ID;
              
         SELECT c906_rule_value 
            INTO v_plant
              FROM T906_RULES 
              WHERE c906_rule_id='PART_PLANT_ID'
              AND c906_rule_grp_id='PARTS_PLANT' 
              AND C906_VOID_FL IS NULL;
	   /*  check the lot and part number combination alone in T2550 table
	    * if the v_out is not null then no need to fetch the warehouse
	    *  */
	    gm_pkg_common.gm_chk_cntrl_num_exists(p_part_num,p_cntl_num,v_out);
	    IF v_out IS NOT NULL THEN
	      RETURN;
	     END IF;
	     
	    IF v_viacell_count > 0 AND v_plant_id = v_plant  THEN
	         RETURN;
            END IF;
	   
		SELECT COUNT(1) 
		 INTO v_count
		 FROM T2550_PART_CONTROL_NUMBER T2550 
		WHERE T2550.C205_PART_NUMBER_ID =   p_part_num
		  AND T2550.C2550_CONTROL_NUMBER = p_cntl_num
		  AND T2550.C5040_PLANT_ID	= v_plant_id;
               	
		IF 	v_count = 0 THEN
			p_out_warehouse := 99999;
			RETURN;
		END IF;
			
		BEGIN
			SELECT t2550.c901_last_updated_warehouse_type INTO p_out_warehouse --Get the data from t2550 table for PMT-29681
			  FROM t2550_part_control_number t2550
			 WHERE t2550.c2550_control_number     = p_cntl_num
			   AND t2550.c205_part_number_id      = p_part_num
			   AND t2550.c5040_plant_id  		  = v_plant_id;
        EXCEPTION WHEN NO_DATA_FOUND THEN
    		p_out_warehouse:= NULL;
        END;
	END gm_fetch_curr_warehouse;
	
 /************************************************************************************************************
	 * Description : This Procedure is used to check the lot is enabled for the company and check in T2550 table
	 * Author	   : karthik
	 ********************************************************************************************************/
	PROCEDURE gm_chk_lot_track_enable(
	  p_part_num		IN		VARCHAR2
	, p_cntl_num		IN		VARCHAR2
	, p_company_id      IN	   	t1900_company.c1900_company_id%TYPE
	, p_out_flag        OUT     VARCHAR2
	)
	AS
        v_company_flag   VARCHAR2(1);
        v_part_comp_flag VARCHAR2(1);
     --   v_out            VARCHAR2(2000);
	BEGIN
		 --validating the company is required for lot track
	   SELECT get_rule_value(p_company_id,'LOT_TRACK') INTO v_company_flag FROM DUAL; 
		  --validating the part is required to track lot		  	
	   SELECT get_part_attr_value_by_comp(p_part_num,'104480',p_company_id) INTO v_part_comp_flag FROM DUAL;   
	   
	   /*  check the lot and part number combination alone in T2550 table
	    * if the v_out is not null then no need to fetch the warehouse
	    *  */
	   /* gm_pkg_common.gm_chk_cntrl_num_exists(p_part_num,p_cntl_num,v_out);*/
	  
	    /*v_company_flag checking the company is enabled for lot track
	     * v_part_comp_flag checking the part is enabled for lot track for the company
	     * v_out checking the part and lot available in T2550 table 
	     * */
	    IF (v_company_flag IS NULL OR v_company_flag <> 'Y') OR (v_part_comp_flag IS NULL OR v_part_comp_flag <> 'Y') THEN
	 	  p_out_flag := 'N';
	 	  ELSE
	 	  p_out_flag := 'Y';
	   END IF;
	   
		END gm_chk_lot_track_enable;

 END gm_pkg_op_inv_warehouse;
 /
