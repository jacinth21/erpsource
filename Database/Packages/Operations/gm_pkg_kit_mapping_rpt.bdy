CREATE OR REPLACE PACKAGE BODY gm_pkg_kit_mapping_rpt
IS
/* *******************************************************************************
 * Author: Agilan singaravel
 * Procedure:gm_fch_kit_nm_availability
 * Description:Check kit name availability based on party id
 ***********************************************************************************/ 
 PROCEDURE gm_fch_kit_nm_avl (
  	  p_party_id 		IN 		T2078_KIT_MAPPING.C2078_OWNER_PARTY_ID%TYPE
 	, p_kit_nm 			IN 		T2078_KIT_MAPPING.C2078_KIT_NM%TYPE
 	, p_kit_nm_avil 	OUT 	varchar
  )
  AS
  	  v_count NUMBER(10);
  BEGIN
	  SELECT count(*) 
	    INTO v_count 
	    FROM T2078_KIT_MAPPING 
	   WHERE C2078_KIT_NM = p_kit_nm
	     AND C2078_OWNER_PARTY_ID = p_party_id
	     AND C901_TYPE = 107921   -- Sales Internal for Ipad and mobile
	     AND C2078_VOID_FL IS NULL;

  		IF (v_count = 0) THEN
  			p_kit_nm_avil := 'N';
  		ELSE
  			p_kit_nm_avil := 'Y';
  		END IF;	 
  		
   END gm_fch_kit_nm_avl;
   
/* *******************************************************************************
 * Author: Agilan singaravel
 * Procedure:gm_fch_set_id
 * Description: Fetch set details based on ref id 
 ***********************************************************************************/ 	  	  
 PROCEDURE gm_fch_set_id (
  	  p_ref_id		    IN 		t504_consignment.c504_consignment_id%TYPE
  	, p_user_id         IN      T101_USER.C101_USER_ID%TYPE
 	, p_values 			OUT 	TYPES.cursor_type
  )
  AS
  	v_company_id    t504_consignment.C1900_COMPANY_ID%TYPE;
  	v_user_id       T101_USER.C101_USER_ID%TYPE;  
  BEGIN
	  
	 SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL'))
  	    INTO v_company_id 
	    FROM dual;
	    
		OPEN p_values 
		FOR
			
		SELECT tag.tagid,
  			   t504.c504_consignment_id,
  			   T207.C207_set_id setid,
 			   t701.c701_distributor_name dname,
  			   t504.c701_distributor_id did,
  			   tag.tagid ||'-'|| T207.C207_Set_Id ID,
  			   tag.tagid ALTNAME ,
  			   DECODE(tag.tagid,NULL,'*Missing Tag*',tag.tagid) || '-' || T207.C207_set_id ||'-'|| T207.C207_Set_Nm NAME
		  FROM T207_SET_MASTER T207,
  			   T2080_SET_COMPANY_MAPPING T2080,
  			   t504_consignment t504,
  			   t701_distributor t701,
  	   		   (SELECT T5010.C5010_Tag_Id tagid,
    		   t5010.c5010_last_updated_trans_id txnid,
    		   T5010.C207_Set_Id setid
  		  	   FROM T5010_Tag T5010
  		 	   WHERE T5010.C901_LOCATION_TYPE = 4120  --Distributor
  		   	   AND t5010.c5010_void_fl       IS NULL
  			   )tag
		 WHERE T207.C207_set_Id       = tag.setid(+)
		   AND t504.c504_consignment_id = tag.txnid(+)
		   AND t504.c701_distributor_id = t701.c701_distributor_id
		   AND T2080.C207_SET_ID        = T207.C207_SET_ID
		   AND T504.C207_SET_ID         = T207.C207_SET_ID
		   AND T504.C504_CONSIGNMENT_ID = p_ref_id 
		   AND t504.c504_status_fl      = '4'     --Completed
		   AND T207.C901_Set_Grp_Type   = 1601    --Set Report Group
		   AND T504.C504_TYPE           = 4110     -- Consignment 
		   AND t207.c207_Type           = '4070'   -- Consignment
		   AND C207_VOID_FL            IS NULL
	       AND T2080.C2080_VOID_FL     IS NULL
		   AND T504.C1900_COMPANY_ID    = v_company_id
		   AND T2080.C1900_COMPANY_ID   = v_company_id
		   ORDER BY UPPER(T207.C207_Set_Id);
  	  	  
   END gm_fch_set_id;
  
/* *******************************************************************************
 * Author: Agilan singaravel
 * Procedure:gm_fch_part_num
 * Description: Fetch Part details based on part num when shipout
 ***********************************************************************************/ 	  	  
   PROCEDURE gm_fch_part_num(
  	  p_part_num		 IN      T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE
    , p_cmp_id           IN      T1900_COMPANY.C1900_COMPANY_ID%TYPE
 	, p_values 			 OUT 	 TYPES.cursor_type
  )
  AS
  BEGIN
	  	    
	OPEN 	p_values 
	FOR
           SELECT T205.C205_Part_Number_Id ID, T205.C205_Part_Number_Id ||' -'||T205.C205_Part_Num_Desc NAME
			 FROM T205_Part_Number T205,
 				  T205d_Part_Attribute T205d,
 		  		  (SELECT C901_RFS_ID RFSID,
   				  C1900_Company_Id CMPID
 			 	  FROM T901c_Rfs_Company_Mapping
 				  WHERE C901c_Void_Fl IS NULL
 				  UNION
 		   		  SELECT 80120 Rfsid, p_cmp_id Cmpid FROM DUAL
 				  )RFS
			WHERE T205.C205_Part_Number_Id = T205d.C205_Part_Number_Id
			  AND T205d.C205d_Void_Fl       IS NULL
			  AND T205.C205_Part_Number_Id = p_part_num
			  AND T205.C205_Active_Fl        = 'Y'
			  AND T205d.C205d_Attribute_Value = TO_CHAR(Rfs.Rfsid)
			  AND RFS.CMPID = p_cmp_id
			  AND T205d.C901_Attribute_Type = 80180;    --Release for Sale - Region
	  
 END gm_fch_part_num;

 /* *******************************************************************************
 * Author: Agilan singaravelan
 * Procedure:gm_fch_kit_dtl
 * Description:Fetch Kit details for summary page
 ***********************************************************************************/  	  
 PROCEDURE  gm_fch_kit_dtl (
    p_kit_id 		IN     T2078_KIT_MAPPING.C2078_KIT_MAP_ID%type,
    p_company_id    IN     T1900_company.c1900_company_id%type,
    p_party_id      IN     T2078_KIT_MAPPING.C2078_OWNER_PARTY_ID%type,
    p_comp_fmt		  IN       varchar2,
    p_out_cursor 	OUT    CLOB)
  AS
   	v_json_out   JSON_OBJECT_T;
 	v_kitdtl     VARCHAR2(4000);
 	v_kitsetdtl  CLOB;
 	v_kitpartdtl VARCHAR2(4000);
 	v_kitrepdtl  VARCHAR2(4000);
 	v_dist_id    t701_distributor.c701_distributor_id%type;
  BEGIN
	   --Fetch Kit Details
	                  SELECT JSON_OBJECT(
         'caseid' VALUE T71041.caseid,
         'kitid' VALUE T71041.kitid,
         'kitnm' VALUE T71041.kitnm,
         'activefl' VALUE T71041.actfl,
         'deactivedate' VALUE T71041.deact,
         'sharedby' VALUE T71041.shardby,
         'kitowner' VALUE T71041.kitowner,                                                                         
         'surgdt' VALUE TO_CHAR(T7100.C7100_SURGERY_DATE,p_comp_fmt),
         'sttm' VALUE TO_CHAR (T7100.C7100_SURGERY_TIME,'HH12:MI AM'),
         'endtm' VALUE  TO_CHAR (T7100.C7100_SURGERY_END_TIME,'HH12:MI AM'),
         'accnm' VALUE  GET_ACCOUNT_NAME(T7100.C704_ACCOUNT_ID),
         'surgnm' VALUE gm_pkg_case_rpt.GET_SURG_NAME(T7100.C7100_CASE_INFO_ID),
         'dofl' VALUE t7100.c7100_do_flag 
        )INTO v_kitdtl 
         FROM T7100_CASE_INFORMATION T7100,(
       SELECT T7104.C7100_CASE_INFO_ID caseid,
	          T2078.C2078_KIT_MAP_ID kitid,
	          T2078.C2078_KIT_NM kitnm,
	          T2078.C2078_ACTIVE_FL actfl,
	          TO_CHAR(T2078.C2078_DEACTIVATE_DATE,p_comp_fmt) deact,
	          DECODE(p_party_id,T2078.C2078_OWNER_PARTY_ID,'',GET_PARTY_NAME(T2078.C2078_OWNER_PARTY_ID)) shardby,
	          T2078.C2078_OWNER_PARTY_ID kitowner 
         FROM T2078_KIT_MAPPING T2078, T7104_CASE_SET_INFORMATION T7104
        WHERE T2078.C2078_KIT_MAP_ID = p_kit_id
          AND T7104.C2078_KIT_MAP_ID(+) = T2078.C2078_KIT_MAP_ID
          AND T2078.C901_TYPE = 107921
       	  AND T2078.C2078_VOID_FL IS NULL)T71041   
        WHERE T7100.C7100_CASE_INFO_ID(+) = T71041.caseid
          AND C7100_VOID_FL IS NULL
          AND ROWNUM=1
     ORDER BY T7100.C7100_SURGERY_DATE ASC;

	    
	 BEGIN
	   SELECT C701_distributor_id 
	     INTO v_dist_id 
	     FROM t101_user 
	    WHERE c101_party_id = p_party_id;
	       EXCEPTION
      		WHEN NO_DATA_FOUND
   		 THEN
      		v_dist_id := '';
          END;
	      
	    --Fetch Set Details based on Kit
	    SELECT JSON_ARRAYAGG(JSON_OBJECT( 'setid' VALUE T2079.C207_SET_ID,
				'setdesc' VALUE get_set_name(t2079.C207_SET_ID), 
				'tagid' VALUE T2079.C5010_TAG_ID ,
				'consignto' VALUE decode(v_dist_id,tag.did,'',tag.dname),
				'kitid' VALUE t2078.C2078_KIT_MAP_ID, 
				'voidfl' VALUE t2079.C2079_VOID_FL)order by  T2079.C207_SET_ID,T2079.C5010_TAG_ID RETURNING CLOB) INTO v_kitsetdtl
		 FROM T2079_KIT_MAP_DETAILS T2079,
  			  T2078_KIT_MAPPING T2078,
  	  (SELECT T5010.C5010_Tag_Id tagid,
  			  t5010.c5010_last_updated_trans_id txnid,
  			  T5010.C207_Set_Id setid,
  			  t701.c701_distributor_name dname,
  			  t701.c701_distributor_id did
		 FROM T5010_Tag T5010,
  			  t701_distributor t701
		WHERE t5010.c5010_location_id = t701.c701_distributor_id
		  AND t701.c701_void_fl        IS NULL
		  AND t5010.c901_location_type  = 4120
		  AND T5010.C1900_COMPANY_ID     = p_company_id
		  AND t5010.c5010_void_fl      IS NULL)tag
		WHERE T2079.C2078_KIT_MAP_ID = p_kit_id
		  AND T2079.c5010_tag_id = tag.tagid(+)
		  AND T2079.C2078_KIT_MAP_ID   = t2078.C2078_KIT_MAP_ID
		  AND T2078.C2078_VOID_FL     IS NULL
		  AND T2079.C2079_VOID_FL     IS NULL;
	 
	   --Fetch Part Details based on Kit
	    SELECT JSON_ARRAYAGG(JSON_OBJECT(
	   				'partnum' VALUE T2079A.C205_PART_NUMBER_ID,
  				  	'partdesc' VALUE gm_pkg_kit_mapping_rpt.GETPARTDESC(T2079A.C205_PART_NUMBER_ID),
  				   	'qty' VALUE T2079A.C2079A_QTY,
  				   	'kitid' VALUE t2078.C2078_KIT_MAP_ID,
  				   	'voidfl' VALUE T2079A.C2079A_VOID_FL
  			   )order by T2079A.C205_PART_NUMBER_ID RETURNING CLOB) INTO v_kitpartdtl 
	     FROM T2079A_KIT_PART_MAP_DTL T2079A,T205_PART_NUMBER T205, T2078_KIT_MAPPING t2078
	    WHERE T2079A.C2078_KIT_MAP_ID = p_kit_id
	      AND T2079A.C2078_KIT_MAP_ID   = t2078.C2078_KIT_MAP_ID
	      AND T2079A.C205_PART_NUMBER_ID = T205.C205_PART_NUMBER_ID
	      AND T2078.C2078_VOID_FL IS NULL
	      AND T2079A.C2079A_VOID_FL IS NULL; 
	      	      
	    --Fetch Sales rep list based on shared by kit
	      SELECT JSON_ARRAYAGG(JSON_OBJECT(
	   				'partyid' VALUE T2079B.C2079B_REP_PARTY_ID,
	   				'partynm' VALUE GET_PARTY_NAME(T2079B.C2079B_REP_PARTY_ID),
  				   	'kitid' VALUE t2078.C2078_KIT_MAP_ID,
  				   	'voidfl' VALUE T2079B.C2079B_VOID_FL 
  			   )order by  GET_PARTY_NAME(T2079B.C2079B_REP_PARTY_ID) RETURNING CLOB) INTO v_kitrepdtl 
	     FROM T2079B_KIT_REP_MAP T2079B, T2078_KIT_MAPPING t2078
	    WHERE T2079B.C2078_KIT_MAP_ID = p_kit_id
	      AND T2079B.C2078_KIT_MAP_ID   = t2078.C2078_KIT_MAP_ID
	      AND t2078.C2078_VOID_FL IS NULL
	      AND T2079B.C2079B_VOID_FL IS NULL; 
	
	     v_json_out := JSON_OBJECT_T.parse(v_kitdtl);
	  
	IF v_kitsetdtl !=empty_clob() THEN 
		v_json_out.put('arrkitsetvo',JSON_ARRAY_T(v_kitsetdtl));
	END IF;
	
	IF v_kitpartdtl IS NOT NULL THEN
		v_json_out.put('arrkitpartvo',JSON_ARRAY_T(v_kitpartdtl));
	END IF;
	  
	IF v_kitrepdtl IS NOT NULL THEN
		v_json_out.put('arrrepdtl',JSON_ARRAY_T(v_kitrepdtl));
	END IF;
	
	p_out_cursor := v_json_out.to_string;
	      
 END gm_fch_kit_dtl;

 /* *******************************************************************************
 * Author: Agilan singaravelan
 * Procedure:gm_fch_sync_set
 * Description:Fetch set tag list to update when Job running
 ***********************************************************************************/  	  
 PROCEDURE  gm_fch_sync_set (
    p_company_id 		IN     T2078_KIT_MAPPING.C1900_COMPANY_ID%type,
    p_out_cursor 	    OUT    TYPES.cursor_type)
  AS 
  BEGIN
	  OPEN p_out_cursor
	  FOR

	  	SELECT tag.tagid,
  			   t504.c504_consignment_id,
  			   T207.C207_set_id setid,
 			   t701.c701_distributor_name dname,
  			   t504.c701_distributor_id did,
  			   tag.tagid ||'-'|| T207.C207_Set_Id ID,
  			   tag.tagid ALTNAME ,
  			   DECODE(tag.tagid,NULL,'*Missing Tag*',tag.tagid) || '-' || T207.C207_set_id ||'-'|| T207.C207_Set_Nm NAME
		  FROM T207_SET_MASTER T207,
  			   T2080_SET_COMPANY_MAPPING T2080,
  			   t504_consignment t504,
  			   t701_distributor t701,
  	   	       (SELECT T5010.C5010_Tag_Id tagid,
    		   t5010.c5010_last_updated_trans_id txnid,
    		   T5010.C207_Set_Id setid
  		  	   FROM T5010_Tag T5010
  		 	   WHERE T5010.C901_LOCATION_TYPE = 4120   --Distributor
  		   	   AND t5010.c5010_void_fl       IS NULL
  			   )tag
		 WHERE T207.C207_set_Id       = tag.setid(+)
		   AND t504.c504_consignment_id = tag.txnid(+)
		   AND t504.c701_distributor_id = t701.c701_distributor_id
		   AND T2080.C207_SET_ID        = T207.C207_SET_ID
		   AND T504.C207_SET_ID         = T207.C207_SET_ID
		   AND t504.c504_status_fl      = '4'      --Completed
		   AND T207.C901_Set_Grp_Type   = 1601     --Set Report Group
		   AND T504.C504_TYPE           = 4110     --Consignment
		   AND t207.c207_Type           = '4070'   --Consignment
		   AND C207_VOID_FL            IS NULL
	       AND T2080.C2080_VOID_FL     IS NULL
		   AND T504.C1900_COMPANY_ID    = p_company_id
		   AND T2080.C1900_COMPANY_ID   = p_company_id
		   AND T504.C504_SHIP_DATE =  trunc(current_date)
		   ORDER BY UPPER(T207.C207_Set_Id);
	   
  END gm_fch_sync_set;
  
/* *******************************************************************************
 * Author: Agilan singaravelan
 * Procedure:gm_fch_sync_part
 * Description:Fetch part number list to update when Job running
 ***********************************************************************************/  	  
 PROCEDURE  gm_fch_sync_part (
    p_company_id 		IN     T2078_KIT_MAPPING.C1900_COMPANY_ID%type,
    p_out_cursor 	    OUT   TYPES.cursor_type)
  AS 	  
 BEGIN
	 
	 OPEN p_out_cursor
	  FOR
	 SELECT T205.C205_PART_NUMBER_ID ID,
  			T205.C205_PART_NUMBER_ID
  			|| ' - '
  			|| T205.C205_PART_NUM_DESC NAME
	   FROM T205_PART_NUMBER T205,
  			T205D_PART_ATTRIBUTE T205D,
  	(SELECT C901_RFS_ID RFSID,
    		C1900_COMPANY_ID CMPID
  	   FROM T901C_RFS_COMPANY_MAPPING
  	  WHERE C901C_VOID_FL IS NULL
      UNION
     SELECT 80120 RFSID, 1000 CMPID FROM DUAL
  			)RFS
	  WHERE T205.C205_PART_NUMBER_ID  = T205D.C205_PART_NUMBER_ID
		AND t205d.c205d_void_fl        IS NULL
		AND t205.c205_active_fl         = 'Y'
		AND t205d.c205d_attribute_value = TO_CHAR(RFS.Rfsid)
		AND RFS.cmpid                   = 1000
		AND t205d.c901_attribute_type   =80180
		AND trunc(T205.C205_LAST_UPDATED_DATE) = trunc(CURRENT_DATE);

	END gm_fch_sync_part;  
/* *******************************************************************************
 * Author: Agilan singaravelan
 * Procedure:gm_fch_part_dtl
 * Description:Fetch part details based on set id when P icon clicked
 ***********************************************************************************/  	  
 PROCEDURE  gm_fch_part_dtl (
    p_set_id 		IN     T207_SET_MASTER.C207_SET_ID%type,
    p_out_cursor 	OUT    CLOB)
    
 AS
 	BEGIN
	 
        SELECT JSON_ARRAYAGG(JSON_OBJECT(
	   				'partnum' VALUE T208.C205_PART_NUMBER_ID,
  				  	'partdesc' VALUE gm_pkg_kit_mapping_rpt.GETPARTDESC(T208.C205_PART_NUMBER_ID),
  				  	'qty' VALUE T208.C208_SET_QTY
  			   ) order by T208.C205_PART_NUMBER_ID RETURNING CLOB) INTO p_out_cursor
  		  FROM T208_SET_DETAILS T208, 
       		   T205_PART_NUMBER T205    
		 WHERE T205.C205_PART_NUMBER_ID = T208.C205_PART_NUMBER_ID 
  		   AND T208.C207_SET_ID = p_set_id
  		   AND T208.C208_SET_QTY <> 0
  		   AND T208.C208_INSET_FL = 'Y'
  		   AND T205.C205_ACTIVE_FL='Y'
  		   AND T208.C208_VOID_FL IS NULL;
	 	
	END gm_fch_part_dtl;
	
/* *******************************************************************************
* Author: Parthiban Dhanasekaran 
* Procedure:gm_sav_kit_toggle
* Description:InActive the kit
***********************************************************************************/
 PROCEDURE  gm_sav_kit_toggle (
    p_kit_id            IN    T2078_KIT_MAPPING.C2078_KIT_MAP_ID%TYPE
   ,p_userid            IN    T2078_KIT_MAPPING.C2078_OWNER_PARTY_ID%TYPE
   ,p_actfl				IN	  T2078_KIT_MAPPING.C2078_ACTIVE_FL%TYPE
   )
   AS 
   BEGIN
                        
            UPDATE T2078_KIT_MAPPING 
               SET C2078_ACTIVE_FL = p_actfl,
               	   C2078_LAST_UPDATED_BY = p_userid,
               	   C2078_LAST_UPDATED_DATE = CURRENT_DATE,
                   C2078_DEACTIVATE_DATE = DECODE(p_actfl,'N',CURRENT_DATE,NULL)
             WHERE C2078_KIT_MAP_ID = p_kit_id 
               AND C2078_VOID_FL IS NULL;
               
END gm_sav_kit_toggle;


 /* *******************************************************************************
 * Author: Parthiban Dhanasekaran 
 * Procedure:gm_fch_tag_validate
 * Description: Validate the Loaner Tag
 ***********************************************************************************/  	 
PROCEDURE gm_fch_tag_validate (
  	  p_tag_id 			IN 		T5010_TAG.C5010_TAG_ID%TYPE
 	, p_tag_avl      	OUT 	varchar
 ) AS
 	
 	 v_count NUMBER(10);
  BEGIN
	  SELECT count(*) 
	    INTO v_count 
	    FROM t5010_tag 
	   WHERE C901_INVENTORY_TYPE IN('10001','10004')
	     AND C5010_TAG_ID = p_tag_id
	     AND C5010_VOID_FL IS NULL;
 	
	   	IF (v_count = 0) THEN
  			p_tag_avl := 'N';
  		ELSE
  			p_tag_avl := 'Y';
  		END IF;
 	
	END gm_fch_tag_validate;
  
/* *******************************************************************************
* Author: Agilan  
* Procedure:get_distributor_id
* Description: Get distributor id from user id
***********************************************************************************/           
FUNCTION  get_distributor_id (
    p_user_id		   IN	 t101_user.c101_user_id%type)
    RETURN VARCHAR2
   IS
   		p_dist_id    t701_distributor.c701_distributor_id%type;
   BEGIN
	   BEGIN
	   SELECT c701_distributor_id 
	     INTO p_dist_id 
	     FROM t101_user 
	    WHERE c101_user_id = p_user_id;
	    EXCEPTION
      		WHEN NO_DATA_FOUND
   		THEN
      		RETURN ' ';
      	END;
      RETURN p_dist_id;  	
  END get_distributor_id;
  
  /* *******************************************************************************
 * Author: Agilan singaravel
 * Procedure:gm_fch_teg_set_id
 * Description: Fetch set details based on ref id 
 ***********************************************************************************/ 	  	  
 PROCEDURE gm_fch_teg_set_id (
  	  p_ref_id		    IN 		t504_consignment.c504_consignment_id%TYPE
  	, p_user_id         IN      T101_USER.C101_USER_ID%TYPE
 	, p_values 			OUT 	TYPES.cursor_type
  )
  AS
  v_company_id    t504_consignment.C1900_COMPANY_ID%TYPE;
  v_user_id       T101_USER.C101_USER_ID%TYPE;
  
  BEGIN
	 SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL'))
  	    INTO v_company_id 
	    FROM dual;
	    
		OPEN p_values 
		FOR

			SELECT  T5010.C5010_Tag_Id tagid,
    				t5010.c5010_last_updated_trans_id txnid,
    				T5010.C207_SET_ID SETID,
    				GET_SET_NAME(T5010.C207_SET_ID) SETNM,
    				T701.C701_DISTRIBUTOR_NAME DNAME,
    				T701.C701_DISTRIBUTOR_ID DID,
    				T5010.C5010_TAG_ID ||'-'||T5010.C207_SET_ID ID,
    				T5010.C5010_TAG_ID || '-'|| T5010.C207_SET_ID ||'-'|| GET_SET_NAME(T5010.C207_SET_ID) ||'*Consigned to '||t701.c701_distributor_name||'*' NAME
  			  FROM  T5010_Tag T5010,
    				t701_distributor t701, t207_set_master t207
  			 WHERE  T5010.C5010_LOCATION_ID = T701.C701_DISTRIBUTOR_ID
  			   AND  T207.C207_SET_ID = T5010.C207_SET_ID
  			   AND  t5010.c5010_last_updated_trans_id = p_ref_id
  			   AND  t207.c207_type = 4070              --Consignment
  			   AND  T5010.C901_LOCATION_TYPE  = 4120   --Relase for Sale
  			   AND  T5010.C1900_COMPANY_ID    = v_company_id
  			   AND  T5010.C5010_VOID_FL  IS NULL
  			   AND  t207.C207_VOID_FL IS NULL
  			   AND  t701.c701_void_fl  IS NULL
  			   AND  T5010.C207_SET_ID is not null
  		  ORDER by  UPPER(T5010.C5010_Tag_Id);
		   
END gm_fch_teg_set_id;

  /* *******************************************************************************
 * Author: Agilan singaravel
 * Procedure:gm_fch_sync_tag_set
 * Description: Fetch set details based on ref id 
 ***********************************************************************************/ 	  	  
 PROCEDURE gm_fch_sync_tag_set (
  	  p_comapny_id		    IN 		t1900_company.c1900_company_id%TYPE
  	, p_dist_id             IN      T101_USER.c701_distributor_ID%TYPE
 	, p_values 			    OUT 	TYPES.cursor_type
  )
  AS
  v_company_id    t504_consignment.C1900_COMPANY_ID%TYPE;
  v_user_id       T101_USER.C101_USER_ID%TYPE;
  
  BEGIN
	 SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL'))
  	    INTO v_company_id 
	    FROM dual;
	    
		OPEN p_values 
		FOR
	
	SELECT tag.tagid tid,
  		   t504.c504_consignment_id,
  		   T207.C207_set_id setid,
  		   t701.c701_distributor_name dname,
  		   t504.c701_distributor_id did,
  		   tag.tagid ||'-' ||T207.C207_Set_Id ID,
  		   tag.tagid ALTNAME ,
  		   tag.tagid || '-' || T207.C207_set_id ||'-'|| T207.C207_Set_Nm ||'*Consigned to ' ||t701.c701_distributor_name ||'*' NAME
      FROM T207_SET_MASTER T207,
  		   T2080_SET_COMPANY_MAPPING T2080,
  		   t504_consignment t504,
  		   t701_distributor t701,
   		   (SELECT T5010.C5010_Tag_Id tagid,
   		   t5010.c5010_last_updated_trans_id txnid,
    	   T5010.C207_Set_Id setid
  	       FROM T5010_Tag T5010
 	       WHERE t5010.c901_location_type = 4120    --Relase for sale
 	       AND T5010.C5010_LOCATION_ID   != p_dist_id
  	       AND t5010.c5010_void_fl       IS NULL
  		   )tag
	 WHERE T207.C207_set_Id        = tag.setid
	   AND t504.c504_consignment_id  = tag.txnid
	   AND t504.c701_distributor_id  = t701.c701_distributor_id
	   AND T2080.C207_SET_ID         = T207.C207_SET_ID
	   AND T504.C207_SET_ID          = T207.C207_SET_ID
	   AND T504.C504_SHIP_DATE = trunc(current_date)
	   AND t504.c701_distributor_id != p_dist_id
	   AND t504.c504_status_fl       = '4'    --Completed
	   AND T207.C901_Set_Grp_Type    = 1601   ----Set Report Group
	   AND T504.C504_TYPE            = 4110   --Consignment
	   AND t207.c207_Type            = '4070' --Consignment
	   AND T207.C207_VOID_FL IS NULL
	   AND T2080.C2080_VOID_FL IS NULL
	   AND t504.C504_void_fl IS NULL
	   AND t701.c701_void_fl IS NULL
	   AND T504.C1900_COMPANY_ID     = v_company_id
	   AND T2080.C1900_COMPANY_ID    = v_company_id
	ORDER BY UPPER(tag.tagid);
		   
END gm_fch_sync_tag_set;

/* *******************************************************************************
 * Author: Agilan singaravel
 * FUNCTION:GETPARTDESC
 * Description:This function is used to get the part desc without special char
 ***********************************************************************************/	
FUNCTION GETPARTDESC (
	p_part_num	 	t205_part_number.c205_part_number_id%TYPE	
)
	RETURN VARCHAR2
IS	 
	v_part_desc	    t205_part_number.c205_part_num_desc%TYPE;
BEGIN	 
	
    BEGIN    	
	   	SELECT TRIM(REGEXP_REPLACE(t205.c205_part_num_desc,'[^ ,0-9A-Za-z]', ' '))
	   	  INTO v_part_desc
          FROM T205_PART_NUMBER  t205 
         WHERE t205.C205_PART_NUMBER_ID = p_part_num
           AND t205.c205_ACTIVE_FL = 'Y';
       	   
    EXCEPTION WHEN NO_DATA_FOUND THEN    
      v_part_desc := null;
    END;
	RETURN v_part_desc;
	
END GETPARTDESC;

END gm_pkg_kit_mapping_rpt;
/