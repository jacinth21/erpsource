/* Formatted on 2011/06/09 12:16 (Formatter Plus v4.8.0) */
--@"c:\database\packages\operations\gm_pkg_op_ld_sales_demand.bdy"
-- show err  Package Body gm_pkg_op_ld_sales_demand
-- exec gm_pkg_op_ld_demand.gm_op_ld_demand_main(17);
/*
 DELETE FROM T4042_DEMAND_SHEET_DETAIL;
 DELETE FROM T4041_DEMAND_SHEET_MAPPING;
 DELETE FROM T4040_DEMAND_SHEET;
 COMMIT ;
 exec gm_pkg_op_ld_demand.gm_op_ld_demand_main(5009);

*/

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_ld_sales_demand
IS
	/*************************************************************************
	 * Purpose: Procedure used to load sales demand, calculated value and
	 *	Forecast information
	 *************************************************************************/
	PROCEDURE gm_op_ld_main (
		p_demand_id 		IN	 t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_demand_sheet_id	IN	 t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	  , p_demand_period 	IN	 t4020_demand_master.c4020_demand_period%TYPE
	  , p_forecast_period	IN	 t4020_demand_master.c4020_forecast_period%TYPE
	  , p_inc_current_fl	IN	 t4020_demand_master.c4020_include_current_fl%TYPE
	)
	AS
		--
		v_from_period  DATE;
		v_to_period    DATE;
	--
	BEGIN
		--
		-- Below code to fetch demand (from and to period)
		gm_pkg_op_ld_demand.gm_op_calc_period (p_demand_period, p_inc_current_fl, 'D', v_from_period, v_to_period);
		--
		--DBMS_OUTPUT.put_line ('From and To Date  ****' || v_from_period || ' - ' || v_to_period);
		--
		-- Below procedure called to load sales demand information
		gm_pkg_op_ld_sales_demand.gm_op_ld_demand (p_demand_id, p_demand_sheet_id, v_from_period, v_to_period);
		--
		-- Below code to fetch forecast (from and to period)
		gm_pkg_op_ld_demand.gm_op_calc_period (p_forecast_period, p_inc_current_fl, 'F', v_from_period, v_to_period);
		-- Below procedure called to load sales Weighted and Avg sales information
		gm_pkg_op_ld_sales_demand.gm_op_ld_demand_calc (p_demand_sheet_id, v_from_period);
		--
		-- Below procedure called to load forecast information
		gm_pkg_op_ld_sales_demand.gm_op_ld_forecast (p_demand_id, p_demand_sheet_id, v_from_period, v_to_period);
		--
		-- Below procedure called to load sales Weighted and Avg sales information
		gm_pkg_op_ld_sales_demand.gm_op_ld_forecast_calc (p_demand_sheet_id, v_from_period);
		--
		-- Below procedure called to load sales variance
		gm_pkg_op_ld_sales_demand.gm_op_ld_forecast_variance (p_demand_sheet_id, v_from_period);
		-- Below procedure called to load Par Information
		gm_pkg_op_ld_sales_demand.gm_op_ld_par_value (p_demand_id, p_demand_sheet_id, v_from_period);
		-- Below procedure called to load Sales Backorder
		gm_pkg_op_ld_sales_demand.gm_op_ld_sales_bo (p_demand_id, p_demand_sheet_id, v_from_period);
	--
	END gm_op_ld_main;

	--
	/*************************************************************************
	 * Purpose: Procedure used to load sales demand for selected period
	 * 40030 maps to Group Mapping and 40032 maps to Region Mapping
	 *************************************************************************/
	PROCEDURE gm_op_ld_demand (
		p_demand_id 		IN	 t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_demand_sheet_id	IN	 t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	  , p_from_period		IN	 DATE
	  , p_to_period 		IN	 DATE
	)
	AS
		--

		--
		CURSOR demand_cur
		IS
			SELECT	 partgrpinfo.c4010_group_id, partgrpinfo.c205_part_number_id, partgrpinfo.month_value
				   , SUM (NVL (c502_item_qty, 0)) item_qty
				FROM (SELECT t4011.c4010_group_id, t4011.c205_part_number_id, v9001.month_value
						FROM t4010_group t4010
						   , t4021_demand_mapping t4021
						   , t4011_group_detail t4011
						   , v9001_month_list v9001
					   WHERE t4021.c4020_demand_master_id = p_demand_id
						 AND t4021.c901_ref_type = 40030
						 AND t4021.c4021_ref_id = t4010.c4010_group_id
						 AND t4010.c4010_group_id = t4011.c4010_group_id
						 AND v9001.month_value >= p_from_period
						 AND v9001.month_value <= p_to_period) partgrpinfo
				   , (SELECT TO_DATE ('01/' || TO_CHAR (t501.c501_order_date, 'MM/YYYY'), 'DD/MM/YYYY') order_date
						   , t502.c205_part_number_id, t502.c502_item_qty
						FROM v700_territory_mapping_detail v700
						   , t4021_demand_mapping t4021
						   , t4011_group_detail t4011
						   , t501_order t501
						   , t502_item_order t502
					   WHERE t4021.c4020_demand_master_id = p_demand_id
						 AND t4021.c901_ref_type = 40032
						 AND t4021.c4021_ref_id = v700.region_id
						 AND t501.c704_account_id = v700.ac_id
						 AND t501.c501_order_date >= p_from_period
						 AND t501.c501_order_date <= p_to_period
						 AND t501.c501_order_id = t502.c501_order_id
						 AND t501.c501_void_fl IS NULL
						 AND (t501.c901_ext_country_id IS NULL OR t501.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH
						 AND t4011.c4010_group_id IN (SELECT c4021_ref_id
														FROM t4021_demand_mapping t4021
													   WHERE t4021.c4020_demand_master_id = p_demand_id)
						 AND t502.c205_part_number_id = t4011.c205_part_number_id
						 AND NVL (t501.c901_order_type, -999) <> 2524
						 AND NVL (c901_order_type, -9999) NOT IN (
			                SELECT t906.c906_rule_value
			                  FROM t906_rules t906
			                 WHERE t906.c906_rule_grp_id = 'ORDTYPE'
			                   AND c906_rule_id = 'EXCLUDE')
						 ) partorderinfo
			   WHERE partgrpinfo.c205_part_number_id = partorderinfo.c205_part_number_id(+)
					 AND partgrpinfo.month_value = partorderinfo.order_date(+)
			GROUP BY partgrpinfo.c4010_group_id, partgrpinfo.c205_part_number_id, partgrpinfo.month_value;
	BEGIN
		--
		-- load demand information
		FOR set_val IN demand_cur
		LOOP
			--
			gm_pkg_op_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id
													  , set_val.c4010_group_id
													  , 50560
													  , set_val.month_value
													  , set_val.c205_part_number_id
													  , set_val.item_qty
													  , NULL
													  , NULL
													  , NULL
													   );
		--
		END LOOP;
	--
	END gm_op_ld_demand;

	/*************************************************************************
	 * Purpose: Procedure used to load sales demand Wieghted Avg and Avg
	 * for the selected period
	 *************************************************************************/
	PROCEDURE gm_op_ld_demand_calc (
		p_demand_sheet_id	IN	 t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	  , p_to_period 		IN	 DATE
	)
	AS
		--
		CURSOR demand_calc_cur
		IS
			SELECT	 c4042_ref_id, c205_part_number_id, AVG (c4042_qty) sales_avg
				   , SUM (c4042_qty * val_index) / SUM (val_index) sales_weight_avg
				FROM (SELECT t4042.c205_part_number_id, t4042.c4042_ref_id, t4042.c4042_period, t4042.c4042_qty
						   , ROW_NUMBER () OVER (PARTITION BY t4042.c205_part_number_id ORDER BY t4042.c4042_period)
																											  val_index
						FROM t4042_demand_sheet_detail t4042
					   WHERE t4042.c4040_demand_sheet_id = p_demand_sheet_id AND t4042.c901_type = 50560)
			GROUP BY c4042_ref_id, c205_part_number_id;
	BEGIN
		-- load demand information
		FOR set_val IN demand_calc_cur
		LOOP
			-- Load Average Info
			gm_pkg_op_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id
													  , set_val.c4042_ref_id
													  , 50561
													  , p_to_period
													  , set_val.c205_part_number_id
													  , ROUND (set_val.sales_avg, 1)
													  , NULL
													  , NULL
													  , NULL
													   );
			-- Load Weighted Avergae Info
			gm_pkg_op_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id
													  , set_val.c4042_ref_id
													  , 50562
													  , p_to_period
													  , set_val.c205_part_number_id
													  , ROUND (set_val.sales_weight_avg, 1)
													  , NULL
													  , NULL
													  , NULL
													   );
		--
		END LOOP;
	--
	END gm_op_ld_demand_calc;

	--
	/*************************************************************************
	 * Purpose: Procedure used to load sales forecast information
	 * Based on the demand weighted will load forecase information
	 *************************************************************************/
	PROCEDURE gm_op_ld_forecast (
		p_demand_id 		IN	 t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_demand_sheet_id	IN	 t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	  , p_from_period		IN	 DATE
	  , p_to_period 		IN	 DATE
	)
	AS
		--
		v_part_number  t205_part_number.c205_part_number_id%TYPE;
		v_part_number_curr t205_part_number.c205_part_number_id%TYPE;
		--
		v_previous_qty t4042_demand_sheet_detail.c4042_qty%TYPE;
		v_current_qty  t4042_demand_sheet_detail.c4042_qty%TYPE;
		--
		v_firstloop    CHAR (1);

		--
		CURSOR forecast_cur
		IS
			SELECT	 partgrpinfo.c4010_group_id, partgrpinfo.c205_part_number_id, partgrpinfo.month_value
				   , NVL (partgrowhtinfo.c901_ref_type, grpgrowthinfo.c901_ref_type) growth_type
				   , NVL (partgrowhtinfo.c4031_value, grpgrowthinfo.c4031_value) growth_value, dmdweightavg.weight_avg
				FROM (SELECT t4011.c4010_group_id, t4011.c205_part_number_id, v9001.month_value
						FROM t4010_group t4010
						   , t4021_demand_mapping t4021
						   , t4011_group_detail t4011
						   , v9001_month_list v9001
					   WHERE t4021.c4020_demand_master_id = p_demand_id
						 AND t4021.c901_ref_type = 40030
						 AND t4021.c4021_ref_id = t4010.c4010_group_id
						 AND t4010.c4010_group_id = t4011.c4010_group_id
						 AND v9001.month_value >= p_from_period
						 AND v9001.month_value <= p_to_period) partgrpinfo
				   , (SELECT t4030.c4030_ref_id GROUP_ID, t4031.c4031_start_date, t4031.c4031_end_date
						   , t4031.c901_ref_type, t4031.c4031_value
						FROM t4020_demand_master t4020, t4030_demand_growth_mapping t4030, t4031_growth_details t4031
					   WHERE t4020.c4020_demand_master_id = p_demand_id
						 AND t4020.c4020_demand_master_id = t4030.c4020_demand_master_id
						 AND t4031.c4030_demand_growth_id = t4030.c4030_demand_growth_id
						 AND t4030.c901_ref_type = 20297
						 AND t4031.c4031_start_date >= p_from_period
						 AND t4031.c4031_end_date <= p_to_period
						 AND t4030.c4030_void_fl IS NULL
						 AND t4031.c4031_value IS NOT NULL) grpgrowthinfo
				   , (SELECT t4030.c4030_ref_id part_number, t4031.c4031_start_date, t4031.c4031_end_date
						   , t4031.c901_ref_type, t4031.c4031_value
						FROM t4020_demand_master t4020, t4030_demand_growth_mapping t4030, t4031_growth_details t4031
					   WHERE t4020.c4020_demand_master_id = p_demand_id
						 AND t4020.c4020_demand_master_id = t4030.c4020_demand_master_id
						 AND t4031.c4030_demand_growth_id = t4030.c4030_demand_growth_id
						 AND t4030.c901_ref_type = 20295
						 AND t4031.c4031_start_date >= p_from_period
						 AND t4031.c4031_end_date <= p_to_period
						 AND t4030.c4030_void_fl IS NULL
						 AND t4031.c4031_value IS NOT NULL) partgrowhtinfo
				   , (SELECT t4042.c205_part_number_id, t4042.c4042_period, t4042.c4042_qty weight_avg
						FROM t4042_demand_sheet_detail t4042
					   WHERE t4042.c4040_demand_sheet_id = p_demand_sheet_id AND t4042.c901_type = 50562) dmdweightavg
			   WHERE partgrpinfo.c4010_group_id = grpgrowthinfo.GROUP_ID
				 AND partgrpinfo.month_value = grpgrowthinfo.c4031_start_date
				 AND partgrpinfo.c205_part_number_id = partgrowhtinfo.part_number(+)
				 AND partgrpinfo.month_value = partgrowhtinfo.c4031_start_date(+)
				 AND partgrpinfo.c205_part_number_id = dmdweightavg.c205_part_number_id(+)
				 AND partgrpinfo.month_value = dmdweightavg.c4042_period(+)
			ORDER BY partgrpinfo.c205_part_number_id, partgrpinfo.month_value;
	BEGIN
		--
		v_part_number := '!@';	 -- Default value First time check
		v_firstloop := '';

		-- load demand information
		FOR set_val IN forecast_cur
		LOOP
			--
			v_part_number_curr := set_val.c205_part_number_id;

			--
			-- For Each part get the weighted average for calculation
			IF (v_part_number != v_part_number_curr)
			THEN
				v_part_number := v_part_number_curr;
				v_previous_qty := set_val.weight_avg;
				v_firstloop := 'Y';
			END IF;

			-- If percent then calculate the value
			IF (set_val.growth_type = 20380)
			THEN
				-- If Acutal the perform the calculation
				IF (v_firstloop = 'Y')
				THEN
					v_current_qty := ROUND (v_previous_qty + (v_previous_qty * set_val.growth_value) / 100, 0);
				ELSE
					v_current_qty := ROUND (v_previous_qty + (v_previous_qty * set_val.growth_value) / 100, 0);
				END IF;
			ELSE
				-- If Acutal the assign actual value
				v_current_qty := ROUND (set_val.growth_value, 0);
			END IF;

			-- Save the value
			gm_pkg_op_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id
													  , set_val.c4010_group_id
													  , 50563
													  , set_val.month_value
													  , v_part_number_curr
													  , v_current_qty
													  , NULL
													  , NULL
													  , NULL
													   );
			-- Reassign the current qty to previous qty for calcuation
			v_previous_qty := v_current_qty;
			v_firstloop := 'N';
		--
		END LOOP;
	--
	END gm_op_ld_forecast;

	/*************************************************************************
	 * Purpose: Procedure used to load sales forecast and Avg
	 * for the selected period
	 *************************************************************************/
	PROCEDURE gm_op_ld_forecast_calc (
		p_demand_sheet_id	IN	 t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	  , p_to_period 		IN	 DATE
	)
	AS
		--
		CURSOR forecast_calc_cur
		IS
			SELECT	 c4042_ref_id, c205_part_number_id, AVG (c4042_qty) sales_avg
				FROM (SELECT t4042.c205_part_number_id, t4042.c4042_ref_id, t4042.c4042_period, t4042.c4042_qty
						FROM t4042_demand_sheet_detail t4042
					   WHERE t4042.c4040_demand_sheet_id = p_demand_sheet_id
						 AND t4042.c901_type = 50563
						 AND t4042.c4042_period < ADD_MONTHS (p_to_period, 4))
			GROUP BY c4042_ref_id, c205_part_number_id;
			--only need calaulate the forecast avg by 4 months.
	BEGIN
		-- load demand information
		FOR set_val IN forecast_calc_cur
		LOOP
			-- Load Average Info
			gm_pkg_op_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id
													  , set_val.c4042_ref_id
													  , 50564
													  , p_to_period
													  , set_val.c205_part_number_id
													  , ROUND (set_val.sales_avg, 0)
													  , NULL
													  , NULL
													  , NULL
													   );
		--
		END LOOP;
	--
	END gm_op_ld_forecast_calc;

	/*************************************************************************
	 * Purpose: Procedure used to load sales Variance %
	 * for the selected sheet
	 *************************************************************************/
	PROCEDURE gm_op_ld_forecast_variance (
		p_demand_sheet_id	IN	 t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	  , p_to_period 		IN	 DATE
	)
	AS
		--
		CURSOR forecast_calc_cur
		IS
			SELECT	 t4042.c205_part_number_id, t4042.c4042_ref_id
				   , (	SUM (DECODE (t4042.c901_type, 50561, t4042.c4042_qty, 0))
					  - SUM (DECODE (t4042.c901_type, 50564, t4042.c4042_qty, 0))
					 ) mainval
				   , SUM (DECODE (t4042.c901_type, 50564, t4042.c4042_qty, 0)) divvalue
				FROM t4042_demand_sheet_detail t4042
			   WHERE t4042.c4040_demand_sheet_id = p_demand_sheet_id AND t4042.c901_type IN (50561, 50564)
			GROUP BY t4042.c205_part_number_id, t4042.c4042_ref_id;

		v_divvalue	   NUMBER;
	BEGIN
		-- load demand information
		FOR set_val IN forecast_calc_cur
		LOOP
			SELECT DECODE (set_val.divvalue, 0, 1, set_val.divvalue)
			  INTO v_divvalue
			  FROM DUAL;

			-- Load Average Info
			gm_pkg_op_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id
													  , set_val.c4042_ref_id
													  , 50565
													  , p_to_period
													  , set_val.c205_part_number_id
													  , (set_val.mainval * 100) / v_divvalue
													  , NULL
													  , NULL
													  , NULL
													   );
		--
		END LOOP;
	--
	END gm_op_ld_forecast_variance;

	/*************************************************************************
	 * Purpose: Procedure used to load sheet par information
	 *************************************************************************/
	PROCEDURE gm_op_ld_par_value (
		p_demand_id 		IN	 t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_demand_sheet_id	IN	 t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	  , p_to_period 		IN	 DATE
	)
	AS
		--
		CURSOR forecast_calc_cur
		IS
			SELECT t4011.c4010_group_id, t4011.c205_part_number_id, NVL (pvalue.parvalue, 0) parvalue
			  FROM t4010_group t4010
				 , t4021_demand_mapping t4021
				 , t4011_group_detail t4011
				 , (SELECT c205_part_number_id, c4022_par_value parvalue
					  FROM t4022_demand_par_detail
					 WHERE c4020_demand_master_id = p_demand_id) pvalue
			 WHERE t4021.c4020_demand_master_id = p_demand_id
			   AND t4021.c901_ref_type = 40030
			   AND t4021.c4021_ref_id = t4010.c4010_group_id
			   AND t4010.c4010_group_id = t4011.c4010_group_id
			   AND t4011.c205_part_number_id = pvalue.c205_part_number_id(+);
	BEGIN
		-- load par information
		FOR set_val IN forecast_calc_cur
		LOOP
			-- load par Info
			gm_pkg_op_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id
													  , set_val.c4010_group_id
													  , 50566
													  , p_to_period
													  , set_val.c205_part_number_id
													  , set_val.parvalue
													  , NULL
													  , NULL
													  , NULL
													   );
		--
		END LOOP;
	--
	END gm_op_ld_par_value;

--
/*************************************************************************
 * Purpose: Procedure used to load sales back order
 *************************************************************************/
	PROCEDURE gm_op_ld_sales_bo (
		p_demand_master_id	 IN   t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_demand_sheet_id	 IN   t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	  , p_to_period 		 IN   DATE
	)
	AS
		CURSOR salesbo_cur
		IS
			SELECT	 t4011.c4010_group_id ref_id, t253f.c205_part_number_id p_num, SUM (t253f.c502_item_qty) qty
				FROM v700_territory_mapping_detail v700
				   , t4021_demand_mapping t4021
				   , t4011_group_detail t4011
				   , t253e_order_lock t253e
				   , t253f_item_order_lock t253f
				   , t4040_demand_sheet t4040
			   WHERE t4021.c4020_demand_master_id = p_demand_master_id
				 AND t4040.c4020_demand_master_id = p_demand_master_id
				 AND t4040.c4040_demand_sheet_id = p_demand_sheet_id
				 AND t4021.c901_ref_type = 40032
				 AND t4021.c4021_ref_id = v700.region_id
				 AND t253e.c704_account_id = v700.ac_id
				 AND t253e.c501_order_id = t253f.c501_order_id
				 AND NVL (t253e.c901_order_type, -999) = 2525
				 AND t253e.c501_status_fl = 0
				 AND t253e.c501_void_fl IS NULL
				 AND t4011.c4010_group_id IN (SELECT c4021_ref_id
												FROM t4021_demand_mapping t4021
											   WHERE t4021.c4020_demand_master_id = p_demand_master_id)
				 AND t253f.c205_part_number_id = t4011.c205_part_number_id
				 AND t253e.c250_inventory_lock_id = t4040.c250_inventory_lock_id
				 AND t253f.c250_inventory_lock_id = t4040.c250_inventory_lock_id
			GROUP BY t4011.c4010_group_id, t253f.c205_part_number_id;
	BEGIN
		FOR val IN salesbo_cur
		LOOP
			gm_pkg_op_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id
													  , val.ref_id
													  , 50567
													  , p_to_period
													  , val.p_num
													  , val.qty
													  , NULL
													  , NULL
													  , NULL
													   );
		END LOOP;
	END gm_op_ld_sales_bo;
--
/*************************************************************************
* Purpose:This is the Main procedure that loads the Cut Plan Load.
*
*************************************************************************/
PROCEDURE gm_cp_ld_main (
        p_demand_master_id IN t4020_demand_master.c4020_demand_master_id%TYPE,
        p_demand_sheet_id  IN t4040_demand_sheet.c4040_demand_sheet_id%TYPE,
        p_from_date        IN DATE)
AS
BEGIN
    gm_pkg_op_ld_sales_demand.gm_cp_ld_sales_demand (p_demand_master_id, p_demand_sheet_id, p_from_date) ;
END gm_cp_ld_main;
/*************************************************************************
* Purpose:This procedure calculates the Moving Average from Sales Demand .
*
*************************************************************************/
/*  Logic :
*  1. From the T253E_ORDER_LOCK table get all the Sales Order [Exclude Acknowledgement Order]
*     from Sysdate -12 months to Sysdate.
*  2. Exclude Demand for Certain Parts From the Rules table.
*  3. Calculate the 3 , 6, 12 Months Moving Average for the Parts.
*  4. Convert the Columns to Rows and Insert it into T4042_DEMAND_SHEET_DETAIL Table.
*/
PROCEDURE gm_cp_ld_sales_demand (
        p_demand_master_id IN t4020_demand_master.c4020_demand_master_id%TYPE,
        p_demand_sheet_id  IN t4040_demand_sheet.c4040_demand_sheet_id%TYPE,
        p_from_date        IN DATE)
AS
type pnum_array IS     varray (10000) OF VARCHAR2 (20) ;
type dmd_type_array IS varray (10000) OF NUMBER;
type mra_array IS      varray (10000) OF NUMBER (10, 2) ;
v_date_fmt             VARCHAR2 (10) ;
arr_pnum pnum_array;
arr_dmdtype dmd_type_array;
arr_mra mra_array;
--
v_company_id t1900_company.c1900_company_id%TYPE;
--
CURSOR cur_order
IS
WITH data AS
    (
         SELECT Level temp_tbl FROM dual CONNECT BY level <= 3
    ) -- Logic to convert Columns 3MRA,6MRA,12MRA into rows to be inserted into T4042 Table.
 SELECT C205_PART_NUMBER_ID pnum, DECODE (temp_tbl, 1, '4000637', 2, '4000638', 3, '4000639') mra_type, DECODE (
    temp_tbl, 1, THREE_MRA, 2, SIX_MRA, 3, TWELVE_MRA) mra
   FROM data, (
         SELECT T205.C205_PART_NUMBER_ID, T501.THREE_MRA, T501.SIX_MRA
          , T501.TWELVE_MRA
           FROM
            (
                 SELECT T501.PNUM, T501.order_date, ROUND (THREE_MRA, 2) THREE_MRA
                  , ROUND (SIX_MRA, 2) SIX_MRA, ROUND (TWELVE_MRA, 2) TWELVE_MRA
                   FROM
                    (
                        --Logic to get the Moving Average for 3 Months, 6 Months, 12 Months
                        -- We should always check if the record count is 3 or 6 or 12 [this is the reason for the
                        -- decode]
                        -- and calculate the average for those rows of data. This is how Moving Average to be
                        -- calcualted.
                        -- We need convert the columns into rows and insert into T4042 table.
                         SELECT T502.C205_PART_NUMBER_ID pnum, T502.C501_SHIPPING_DATE order_date, DECODE (COUNT (
                            T502.C502_ITEM_QTY) over (partition BY T502.C205_PART_NUMBER_ID order by
                            T502.C501_SHIPPING_DATE ASC rows BETWEEN 2 preceding AND CURRENT row), 3, AVG (
                            T502.C502_ITEM_QTY) over (partition BY T502.C205_PART_NUMBER_ID order by
                            T502.C501_SHIPPING_DATE ASC rows BETWEEN 2 preceding AND CURRENT row)) THREE_MRA, DECODE (
                            COUNT (T502.C502_ITEM_QTY) over (partition BY T502.C205_PART_NUMBER_ID order by
                            T502.C501_SHIPPING_DATE ASC rows BETWEEN 5 preceding AND CURRENT row), 6, AVG (
                            T502.C502_ITEM_QTY) over (partition BY T502.C205_PART_NUMBER_ID order by
                            T502.C501_SHIPPING_DATE ASC rows BETWEEN 5 preceding AND CURRENT row)) SIX_MRA, DECODE (
                            COUNT (T502.C502_ITEM_QTY) over (partition BY T502.C205_PART_NUMBER_ID order by
                            T502.C501_SHIPPING_DATE ASC rows BETWEEN 11 preceding AND CURRENT row), 12, AVG (
                            T502.C502_ITEM_QTY) over (partition BY T502.C205_PART_NUMBER_ID order by
                            T502.C501_SHIPPING_DATE ASC rows BETWEEN 11 preceding AND CURRENT row)) TWELVE_MRA
                           FROM
                            (
                                 SELECT NVL (C205_PART_NUMBER_ID, v9001_month_list.pnum) C205_PART_NUMBER_ID, NVL (
                                    TO_DATE (TO_CHAR (C501_SHIPPING_DATE, 'MM/YYYY'), 'MM/YYYY'),
                                    v9001_month_list.month_value) C501_SHIPPING_DATE, SUM (NVL (C502_ITEM_QTY, 0))
                                    C502_ITEM_QTY
                                   FROM
                                    (
                                         SELECT *
                                           FROM
                                            (
                                                 SELECT T502.C205_PART_NUMBER_ID C205_PART_NUMBER_ID,
                                                    T501.C501_SHIPPING_DATE C501_SHIPPING_DATE, SUM (NVL (
                                                    T502.C502_ITEM_QTY, 0)) C502_ITEM_QTY
                                                   FROM T253E_ORDER_LOCK T501, T253F_ITEM_ORDER_LOCK T502, (
                                                         SELECT MAX(c250_inventory_lock_id) c250_inventory_lock_id
                                                           FROM t250_inventory_lock
                                                          WHERE c901_lock_type                                = '4000564'
                                                            AND C250_VOID_FL                                 IS NULL                                                          
                                                    )
                                                    T250
                                                  WHERE TRUNC (T501.C501_SHIPPING_DATE) BETWEEN add_months (TRUNC (
                                                    current_date), - 12) AND TRUNC (current_date)
                                                    AND T501.C501_ORDER_ID                   = T502.C501_ORDER_ID
                                                    AND T501.C501_VOID_FL                   IS NULL
                                                    AND T502.C502_VOID_FL                   IS NULL
                                                    AND T502.C502_ITEM_PRICE                 > 0
                                                    AND T501.C1900_COMPANY_ID				 = v_company_id
                                                    AND T501.C250_INVENTORY_LOCK_ID          = T250.C250_INVENTORY_LOCK_ID
                                                    AND T502.C250_INVENTORY_LOCK_ID          = T250.C250_INVENTORY_LOCK_ID
                                                    AND NVL (T501.C901_ORDER_TYPE, '-9999') <> '101260' -- Exclude
                                                    -- Ack.Order
                                               GROUP BY T502.C205_PART_NUMBER_ID, T501.C501_SHIPPING_DATE
                                               ORDER BY T501.C501_SHIPPING_DATE ASC
                                            )
                                            T501
                                    )
                                    order_filter, (
                                         SELECT TO_DATE (TO_CHAR (month_value, 'MM/YYYY'), 'MM/YYYY') month_value,
                                            t205.C205_PART_NUMBER_ID pnum
                                           FROM v9001_month_list, T205_PART_NUMBER t205, t2023_part_company_mapping
                                            t2023
                                          WHERE t205.C205_PART_NUMBER_ID = t2023.C205_PART_NUMBER_ID
                                            AND t2023.c1900_company_id   = v_company_id
                                            AND t2023.c2023_void_fl	IS NULL
                                            AND month_value BETWEEN add_months (TRUNC (current_date), - 13) AND TRUNC (
                                            current_date)
                                    )
                                    v9001_month_list
                                  WHERE v9001_month_list.month_value = TO_DATE (TO_CHAR (C501_SHIPPING_DATE(+),
                                    'MM/YYYY'), 'MM/YYYY')
                                    AND v9001_month_list.pnum = C205_PART_NUMBER_ID(+)
                               GROUP BY C205_PART_NUMBER_ID, TO_DATE (TO_CHAR (C501_SHIPPING_DATE, 'MM/YYYY'),
                                    'MM/YYYY'), v9001_month_list.month_value, v9001_month_list.pnum
                               ORDER BY v9001_month_list.month_value
                            )
                            T502
                    )
                    T501
                  WHERE TRUNC (ORDER_DATE) = TO_DATE (TO_CHAR (current_date, 'MM/YYYY'), 'MM/YYYY')
            )
            T501, (
                 SELECT t205_tmp.C205_PART_NUMBER_ID
                   FROM T205_PART_NUMBER t205_tmp, t2023_part_company_mapping t2023
                  WHERE t205_tmp.C205_PART_NUMBER_ID = t2023.C205_PART_NUMBER_ID
                    AND t2023.c1900_company_id   = v_company_id
                    AND t2023.c2023_void_fl	IS NULL
            )
            t205
          WHERE T205.C205_PART_NUMBER_ID = T501.pnum(+)
    )
    final_table ;
BEGIN
    --
     SELECT NVL (get_compid_frm_cntx (), GET_RULE_VALUE ('DEFAULT_COMPANY', 'ONEPORTAL')), NVL (get_compdtfmt_frm_cntx(), GET_RULE_VALUE ('DATEFMT', 'DATEFORMAT'))
       INTO v_company_id, v_date_fmt
       FROM DUAL;
    --
    OPEN cur_order;
    LOOP
        FETCH cur_order BULK COLLECT INTO arr_pnum, arr_dmdtype, arr_mra LIMIT 500;
        dbms_output.put_line ('pnum:'||arr_pnum.COUNT||' dm-type:'||TO_CHAR (current_date, v_date_fmt ||' HH:MI:SS')) ;
        --
        FORALL i IN 1 .. arr_pnum.COUNT
         INSERT
           INTO t4042_demand_sheet_detail
            (
                c4042_demand_sheet_detail_id, c4040_demand_sheet_id, c4042_ref_id
              , c901_type, c4042_period, c205_part_number_id
              , c4042_qty, c4042_updated_by, c4042_updated_date
            )
            VALUES
            (
                s4042_demand_sheet_detail.NEXTVAL, p_demand_sheet_id, '-9999'
              , arr_dmdtype (i), TRUNC (current_date), arr_pnum (i)
              , NVL (arr_mra (i), 0), 30301, current_date
            ) ;
        EXIT
    WHEN arr_pnum.COUNT = 0;
        dbms_output.put_line ('pnum:'||arr_pnum.COUNT||' End of a loop'||TO_CHAR (current_date, v_date_fmt ||' HH:MI:SS')) ;
    END LOOP;
    CLOSE cur_order;
END gm_cp_ld_sales_demand;
END gm_pkg_op_ld_sales_demand;
/
