CREATE OR REPLACE PACKAGE BODY gm_pkg_get_inserts_exception
IS
/* *******************************************************************************
 * Author      :  N Raja
 * Procedure   :  GM_GET_INSERT_EXCEPTION
 * Description :  Fetch the insert's part exception list in ship job
 * PMT-48520   :  Remove Insert Part Number in Shipping Job
 ***********************************************************************************/ 
 PROCEDURE GM_GET_INSERT_EXCEPTION(
   p_exception_dtl   OUT      TYPES.cursor_type )
  AS

    v_company_id   t1900_company.c1900_company_id%TYPE;
    v_plant_id     t5040_plant_master.C5040_PLANT_ID%TYPE;
   
   BEGIN
	   SELECT get_compid_frm_cntx(),get_plantid_frm_cntx()
            INTO v_company_id,v_plant_id
            FROM DUAL;
   --
   OPEN p_exception_dtl FOR
   
		SELECT * FROM (
		SELECT  c907_ref_id TRANSID, ( '20500^ Requested quantity cannot exceed the available quantity for Part:'
		  || t5056.c205_insert_id
		  || ', Current Qty:'
		  || t205c.c205_available_qty
		  || ', New Qty:'
		  || '-1'
		  || ', Inventory Bucket:'
		  || NVL(get_code_name(t205c.c901_transaction_type),'')
		  || ', Plant:'
		  || NVL(get_plant_name(t907.c5040_plant_id),'Empty')) ERRMSG
		FROM t907_shipping_info t907 ,
			t5056_transaction_insert t5056,
			t205c_part_qty t205c
	   WHERE t907.c907_ref_id = t5056.c5056_ref_id
	   	 AND t5056.c205_insert_id = t205c.c205_part_number_id
	   	 AND t907.c907_status_fl IN (36,40)
		 AND t907.c5040_plant_id = v_plant_id
		 AND t907.c907_tracking_number IS NOT NULL
		 AND t907.c907_void_fl IS NULL
		 AND t5056.c5056_void_fl IS NULL
		 AND t5056.c205_insert_id IS NOT NULL
		 AND t205c.c901_transaction_type = 90800           
		 AND t205c.c5040_plant_id = v_plant_id
		 AND t205c.c205_available_qty = 0 )
    GROUP BY  TRANSID, ERRMSG;
    
   END GM_GET_INSERT_EXCEPTION;
   
END gm_pkg_get_inserts_exception;
/