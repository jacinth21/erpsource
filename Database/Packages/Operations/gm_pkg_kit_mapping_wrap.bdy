CREATE OR REPLACE
PACKAGE BODY gm_pkg_kit_mapping_wrap
IS

/* *******************************************************************************
 * Author: Agilan singaravel
 * Procedure:gm_sav_kit_dtl
 * Description:This Procedure is used to save kit details into appropriate table
 * 			   by calling repective procedures.
 ***********************************************************************************/
	PROCEDURE gm_sav_kit_dtl(
	    p_input_string 		IN 		CLOB,
	    p_user_id      		IN 		T2078_KIT_MAPPING.C2078_LAST_UPDATED_BY%TYPE,
	    p_out_id 	   		OUT 	VARCHAR2,
	    p_kit_type          OUT     VARCHAR2)
	AS
	  v_json 	 	json_object_t;
	  v_kit_nm   	T2078_KIT_MAPPING.C2078_KIT_NM%TYPE;
	  v_Active_fl 	T2078_KIT_MAPPING.C2078_ACTIVE_FL%TYPE;
	  v_void_fl  	T2078_KIT_MAPPING.C2078_VOID_FL%TYPE;
	  v_party_id  	T2078_KIT_MAPPING.C2078_LAST_UPDATED_BY%TYPE;
	  v_company_id  T1900_COMPANY.C1900_COMPANY_ID%TYPE;
	  v_kit_type    NUMBER;
	BEGIN
	
	  v_json     := json_object_t.parse(p_input_string);
	  p_out_id   := v_json.get_String('kitid');
	  v_kit_nm   := v_json.get_String('kitnm');
	  v_Active_fl := v_json.get_String('activefl');
	  v_void_fl   := v_json.get_String('voidfl');
	  v_party_id   := v_json.get_String('partyid');
	  v_company_id := v_json.get_String('cmpid');
	  
	  --save kit details on t2078
	  gm_pkg_kit_mapping.gm_sav_kit_dtl(p_out_id,v_party_id,v_kit_nm,v_Active_fl,v_void_fl,p_user_id,v_company_id);

	  -- save set details into t2079
	  gm_sav_set_dtl(p_out_id,treat(v_json.get('arrkitsetvo') AS json_array_t),p_user_id);
	  
	  -- save part details into t2079A_kit_part
	 gm_sav_part_dtl(p_out_id,treat(v_json.get('arrkitpartvo') AS json_array_t),p_user_id);
	 
	 -- Get kit Type
	 gm_upt_kit_type(p_out_id,p_kit_type);
	
	 
	END gm_sav_kit_dtl;
	
	/* *******************************************************************************
 * Author: Thangasamy Muthusamy
 * Procedure: gm_upt_kit_type
 * Description:This Procedure is used to return Kit type
 ***********************************************************************************/
 	PROCEDURE gm_upt_kit_type(
	    p_out_id 	   	IN   T2078_KIT_MAPPING.C2078_KIT_MAP_ID%TYPE,
	    p_kit_type          OUT  T2078_KIT_MAPPING.C901_KIT_TYPE%TYPE
	)
	AS
	  v_part_cnt    NUMBER;
	  v_set_cnt     NUMBER;
	  v_kit_type    NUMBER;
	BEGIN
	
	--Based on Set and Part Count Kit Type will be set on KIT_MAPPING Table
	
	 select count(*) into v_set_cnt FROM T2079_KIT_MAP_DETAILS WHERE C2078_KIT_MAP_ID=p_out_id AND C2079_VOID_FL IS NULL; 
	 
	 select count(*) into v_part_cnt FROM T2079a_Kit_Part_Map_Dtl WHERE C2078_KIT_MAP_ID=p_out_id AND C2079A_VOID_FL IS NULL;
	 
	 IF v_set_cnt > 0 AND v_part_cnt > 0 THEN
	 	v_kit_type:=107924;
	 ELSIF v_set_cnt > 0 THEN
	 	v_kit_type:=107922;
	 ELSIF v_part_cnt > 0 THEN
	 	v_kit_type:=107923;
	 END IF;
	  
 	 p_kit_type:= v_kit_type;
	  
	 UPDATE T2078_KIT_MAPPING SET C901_KIT_TYPE = v_kit_type  WHERE C2078_KIT_MAP_ID=p_out_id AND C2078_VOID_FL IS NULL;
	 
	END gm_upt_kit_type;
	
/* *******************************************************************************
 * Author: Agilan singaravel
 * Procedure:gm_sav_set_dtl
 * Description:This Procedure is used to save set details into appropriate table 
 * 			   by call respective procedure.
 ***********************************************************************************/
	PROCEDURE gm_sav_set_dtl(
		p_kit_id       IN   OUT T2078_KIT_MAPPING.C2078_KIT_MAP_ID%type,
		p_attr_array   IN   json_array_t,
		p_user_id      IN   T2078_KIT_MAPPING.C2078_LAST_UPDATED_BY%TYPE
	  )
	AS
	v_set_attr_obj   json_object_t;
	BEGIN

	FOR i IN 0 .. p_attr_array.get_size - 1
		 LOOP
		 
		    v_set_attr_obj := treat(p_attr_array.get(i) AS json_object_t);
		    
			gm_pkg_kit_mapping.gm_sav_set_dtl (p_kit_id,
									v_set_attr_obj.get_String('setid'),
									v_set_attr_obj.get_String('tagid'),
									v_set_attr_obj.get_String('voidfl'),
									p_user_id );
		 END LOOP;
		
	END gm_sav_set_dtl;

/* *******************************************************************************
 * Author: Agilan singaravel
 * Procedure:gm_sav_part_dtl
 * Description:This Procedure is used to save part details into appropriate table 
 * 			   by call respective procedure.
 ***********************************************************************************/
	PROCEDURE gm_sav_part_dtl(
		p_kit_id       IN   OUT T2078_KIT_MAPPING.C2078_KIT_MAP_ID%type,
		p_attr_array   IN   json_array_t,
		p_user_id      IN    T2078_KIT_MAPPING.C2078_LAST_UPDATED_BY%TYPE
	  )
	AS
	v_part_attr_obj   json_object_t;
	BEGIN
	FOR i IN 0 .. p_attr_array.get_size - 1
		 LOOP
		 
		    v_part_attr_obj := treat(p_attr_array.get(i) AS json_object_t);
		    
			gm_pkg_kit_mapping.gm_sav_part_dtl (p_kit_id,
									v_part_attr_obj.get_String('partnum'),
									v_part_attr_obj.get_String('qty'),
									v_part_attr_obj.get_String('voidfl'),
									p_user_id );
		 END LOOP;
		
	END gm_sav_part_dtl;

/* *******************************************************************************
 * Author: Agilan singaravel
 * Procedure:gm_sav_kit_rep_dtl
 * Description:This Procedure is used to save share kit details into appropriate table
 * 			   by calling repective procedures.
 ***********************************************************************************/
	PROCEDURE gm_sav_kit_rep_dtl(
	    p_input_string 		IN 		CLOB,
	    p_user_id      		IN 		T2078_KIT_MAPPING.C2078_LAST_UPDATED_BY%TYPE,
	    p_kit_id 	   		OUT 	VARCHAR2 )
	AS
	  v_json 	 	json_object_t;
	  v_kit_nm   	T2078_KIT_MAPPING.C2078_KIT_NM%type;
	  v_Active_fl 	T2078_KIT_MAPPING.C2078_ACTIVE_FL%type;
	  v_void_fl  	T2078_KIT_MAPPING.c2078_void_fl%type;
	  v_party_id  	T2078_KIT_MAPPING.C2078_LAST_UPDATED_BY%TYPE;
	BEGIN
	
    --SAMPLE DATE
	--{"token":"a290745a7da6a89272a4a4bead6ea9","userid":"1882","kitid":"SK-1882-2",
	--"arrrepdtl":[{"partyid":"266","partynm":"Chris Bell","kitid":"","voidfl":""}],
	--"cmpid":"1000","cmptzone":"US/Eastern","cmpdfmt":"MM/dd/yyyy"}
	
	  v_json     := json_object_t.parse(p_input_string);
	  p_kit_id   := v_json.get_String('kitid');

	  -- save rep map details into t2079b
	  gm_sav_rep_map(p_kit_id,treat(v_json.get('arrrepdtl') AS json_array_t),p_user_id);

	END gm_sav_kit_rep_dtl;	

/* *******************************************************************************
 * Author: Agilan singaravel
 * Procedure:gm_sav_rep_map
 * Description:This Procedure is used to save share kit details into appropriate table 
 * 			   by call respective procedure.
 ***********************************************************************************/
	PROCEDURE gm_sav_rep_map(
		p_kit_id       IN   OUT T2078_KIT_MAPPING.C2078_KIT_MAP_ID%type,
		p_attr_array   IN   json_array_t,
		p_user_id      IN   T2078_KIT_MAPPING.C2078_LAST_UPDATED_BY%TYPE
	  )
	AS
	v_part_attr_obj   json_object_t;
	BEGIN
	FOR i IN 0 .. p_attr_array.get_size - 1
		 LOOP
		 
		    v_part_attr_obj := treat(p_attr_array.get(i) AS json_object_t);
		    
			gm_pkg_kit_mapping.gm_sav_rep_map (p_kit_id,
									v_part_attr_obj.get_String('partyid'),
									v_part_attr_obj.get_String('voidfl'),
									p_user_id );
		 END LOOP;
		
	END gm_sav_rep_map;
	
END gm_pkg_kit_mapping_wrap;
/