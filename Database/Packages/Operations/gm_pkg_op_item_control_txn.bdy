/* Formatted on 2011/12/19 12:45 (Formatter Plus v4.8.0) */
--@"C:\database\packages\operations\gm_pkg_op_item_control_txn.bdy";
CREATE OR REPLACE
PACKAGE BODY gm_pkg_op_item_control_txn
IS
  /*******************************************************
  * Description : Procedure to save item details with control number.
  * Author   : Murali
  *******************************************************/
PROCEDURE gm_sav_item_control
  (
    p_refid    IN t5055_control_number.c5055_ref_id%TYPE,
    p_reftype  IN t5055_control_number.c901_ref_type%TYPE,
    p_inputstr IN CLOB,
    p_loc_oper IN t5055_control_number.c901_type%TYPE,
    p_userid   IN t5055_control_number.c5055_last_updated_by%TYPE,
    p_rel_verify_fl		IN	VARCHAR2 DEFAULT NULL)
AS
  v_strlen        NUMBER           := NVL (LENGTH (p_inputstr), 0);
  v_string        CLOB := p_inputstr;
  v_substring     VARCHAR2 (1000);
  v_pnum          VARCHAR2 (20);
  v_ctrllogid     VARCHAR2 (20);
  v_qty           NUMBER;
  v_control       t5060_control_number_inv.c5060_control_number%TYPE;
  v_locid         VARCHAR2 (20);
  v_count         NUMBER       := 0;
  v_whtype        VARCHAR2(20) :='';
  v_new_whtype    VARCHAR2(20) :='';
  v_divid         VARCHAR2(20) := '100823'; -- US Distributor
  v_to_pnum       VARCHAR2 (20);
  v_ack_ref_id    VARCHAR2(50);
  v5060_cnum      t5060_control_number_inv.c5060_control_number%TYPE;
  v_part_inv   	  VARCHAR2(20);
  v_temp_cntl     t5060_control_number_inv.c5060_control_number%TYPE;
  v_status_fl     t412_inhouse_transactions.c412_status_fl%TYPE;
  v_rule_param    VARCHAR2(20);
  v_avail_qty     NUMBER;
  v_frm_part      VARCHAR2 (20);
  v_cntl_inv_id   VARCHAR2 (50);
  v_comma_count	  NUMBER;
  v_plant_id      t5040_plant_master.c5040_plant_id%TYPE;
  v_err_cnt			NUMBER;
  v_flag			VARCHAR2(1) := 'N';
  v_stelkast_flag			VARCHAR2(1) := 'N';
  v_comp_id		t1900_company.c1900_company_id%TYPE;
  v_excl_txn_cnt	NUMBER;

BEGIN
  --
  gm_pkg_op_item_control_txn.gm_sav_item_control_log (p_refid, p_reftype, p_loc_oper, p_userid );
  SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL')) INTO v_comp_id FROM DUAL;
  SELECT NVL(GET_RULE_VALUE_BY_COMPANY('CONTROLNUMBER','VALIDATECONTROL',GET_COMPID_FRM_CNTX()),'N') INTO v_rule_param FROM DUAL; 
  SELECT  NVL(get_plantid_frm_cntx(),get_rule_value('DEFAULT_PLANT','ONEPORTAL')) INTO  v_plant_id FROM DUAL;
  SELECT NVL(GET_RULE_VALUE_BY_COMPANY('LOTCHANGE','STELKAST',v_comp_id),'N') INTO v_stelkast_flag FROM DUAL; 
  --
  WHILE INSTR (v_string, '|') <> 0
  LOOP
    v_substring  := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
    v_string     := SUBSTR (v_string, INSTR (v_string, '|')    + 1);
    --count how many commas in each substring
    SELECT trim( LENGTH( v_substring ) ) - trim( LENGTH( TRANSLATE( v_substring, 'A,', 'A' ) ) )
    INTO v_comma_count
	FROM dual;
    v_pnum       := NULL;
    v_ctrllogid  := NULL;
    v_qty        := NULL;
    v_control    := NULL;
    v_locid      := NULL;
    v_pnum       := SUBSTR (v_substring, 1, INSTR (v_substring, ',')      - 1);
    v_substring  := SUBSTR (v_substring, INSTR (v_substring, ',')         + 1);
    v_qty        := SUBSTR (v_substring, 1, INSTR (v_substring, ',')      - 1);
    v_substring  := SUBSTR (v_substring, INSTR (v_substring, ',')         + 1);
    v_control    := TRIM(SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1));
    v_substring  := SUBSTR (v_substring, INSTR (v_substring, ',')         + 1);
    v_locid      := TRIM(SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1));
    v_substring  := SUBSTR (v_substring, INSTR (v_substring, ',')         + 1);
    --if comma count is 5 , it means includes temp_cntl
    IF v_comma_count = 5
    THEN
    v_temp_cntl  := SUBSTR (v_substring, 1, INSTR (v_substring, ',') 	  - 1);
    v_substring  := SUBSTR (v_substring, INSTR (v_substring, ',')         + 1);
    END IF;
    v_whtype	 := to_number(v_substring);
	/* Stelkast lot related changes - PC-277*/
	IF v_stelkast_flag = 'Y' THEN
		v_control := NVL(gm_pkg_op_controlnumber.gm_fch_steklast_cnum(v_pnum,v_control),v_control);
	END IF;
    /* The following code is added for validating the Control Number which is available in t5060/t5062 table as Reserved or not.
     * As of now this change is done for BBA BKP only based on the v_rule_param [Rule id-CONTROLNUMBER,Rule Grp Id-VALIDATECONTROL]*/
    
    IF v_rule_param = 'Y' AND  p_reftype = '50261'  THEN	-- 50261[Order Transaction]
    	gm_validate_fg_inv_qty(p_refid,v_pnum,v_control);
    END IF;
        
    IF(NVL(GET_RULE_VALUE('RE-DESIG PART',p_reftype),'N') = 'Y') THEN
        -- To validate part num and control num availablity for PTRD Trans 
    	   gm_validate_repack_inventory(p_refid,v_pnum,v_control);
    	   
    	   SELECT c5060_control_number_inv_id 
    	     INTO v_cntl_inv_id
             FROM t5060_control_number_inv 
            WHERE c5060_control_number = v_control
              AND c205_part_number_id  = v_pnum
              AND c5060_qty > 0                  -- PMT-38201 - Exact Fetch error when controlling order transaction
              AND c901_warehouse_type = '90815'; -- Repackage Qty
                      
           SELECT c412_status_fl 
    	     INTO v_status_fl
             FROM t412_inhouse_transactions t412
            WHERE t412.c412_inhouse_trans_id = p_refid
              AND t412.c412_void_fl  is null
              AND t412.c5040_plant_id = v_plant_id;
           
         -- If the control num changed then updated the same in t5062    
         /*
         PMT-46898 author: gpalani
         When the PTRD transaction is verified, during the loop system should update only the Lot status for each lot.
         Including the control number inventory id in the where clause to 
          avoid updating the control number inventory id for all the other lot number from the same transaction id.          
         */
          
           UPDATE t5062_control_number_reserve 
              SET 
                 c901_status = decode(v_status_fl,2,103961,103962)
                , c5062_last_updated_by = p_userid
                , c5062_last_updated_date = CURRENT_DATE
            WHERE c5062_transaction_id = p_refid
              AND c5062_void_fl IS NULL
              AND c5060_control_number_inv_id = v_cntl_inv_id;
    
		   gm_pkg_part_redesignation.gm_sav_redesgn_control(p_refid,v_pnum,v_control,v_temp_cntl,v_to_pnum);
		   v_frm_part := v_pnum;
		   v_pnum := v_to_pnum; 
    END IF;
    -- PMT-22529 . Tag part validation
    IF(get_part_attribute_value(v_pnum, 92340) = 'Y' AND p_rel_verify_fl ='on' AND NVL(GET_RULE_VALUE(p_reftype,'TAG_PART_VALIDATION'),'N') = 'Y')
    THEN    
    
         SELECT COUNT (1)
           INTO v_count
           FROM t5010_tag t5010
          WHERE t5010.c205_part_number_id = v_pnum
            AND t5010.c5010_last_updated_trans_id = p_refid
            AND t5010.c5010_control_number = v_control
            AND t5010.c5010_void_fl IS NULL;

            IF v_count = 0
            THEN
               -- Error message is TagID cannot be blank, please enter TagID.
               raise_application_error (-20912, '');
            END IF;
    
    END IF;
    
    
    --50154: FGLE, -For FGLE  Verification, no need to update the location
    --93345: Put Transacted   
    IF p_reftype = '50154' AND p_loc_oper = '93345'
    THEN
    	v_locid := '';
    END IF;
    
    IF p_reftype IN ('4110','4112','111802','111805') THEN
      BEGIN
        SELECT DECODE(p_reftype,4112,'100824',4110,NVL(GET_DISTRIBUTOR_DIVISION(C701_DISTRIBUTOR_ID),'100823'),'111802','100824','111805','100824')
        INTO v_divid
        FROM T504_CONSIGNMENT
        WHERE C504_CONSIGNMENT_ID = p_refid
        AND C504_VOID_FL         IS NULL;
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
        v_divid := '100823';
      END;
      BEGIN
        SELECT T5051.C901_WAREHOUSE_TYPE
        INTO v_new_whtype
        FROM T5052_LOCATION_MASTER T5052,
          T5051_INV_WAREHOUSE T5051
        WHERE T5052.C5052_LOCATION_ID                   = v_locid
        AND NVL (T5051.C5051_INV_WAREHOUSE_ID, '-9999') = NVL (T5052.C5051_INV_WAREHOUSE_ID, '-9999')
        AND T5052.C5052_VOID_FL                        IS NULL
        AND t5051.C901_STATUS_ID                        = 1
        AND T5052.C5040_PLANT_ID 						= v_plant_id;
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
        v_new_whtype := v_whtype;
      END;
      v_whtype  := v_new_whtype;
      IF v_divid = '100824' THEN
        gm_validate_cn_qty(p_refid,p_reftype,v_pnum,v_whtype,v_qty);
      END IF;
    END IF;
    
    -- CODE ADDED FOR BBA-171 and these changes only for Acknowledgement Order Type
    BEGIN
    SELECT c501_ref_id INTO v_ack_ref_id 
	  FROM t501_order 
	 WHERE c501_order_id = p_refid 
	   AND c501_void_fl IS NULL ;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        v_ack_ref_id := null;
      END;
	IF v_ack_ref_id IS NOT NULL THEN	  	   	
	  	IF v_temp_cntl IS NOT NULL THEN
		   BEGIN
		  	SELECT t5060.c5060_control_number
			     , t5060.c5060_control_number_inv_id 
			  INTO v5060_cnum , v_part_inv
		  	  FROM t5060_control_number_inv t5060
		  	 WHERE 
		  	 t5060.c5060_qty>0 -- System should check only the Qty which is greater than 0 PMT-46800
		  	 AND t5060.c5060_control_number_inv_id IN(
		  	            SELECT t5062.c5060_control_number_inv_id
		  	              FROM  t5062_control_number_reserve t5062
		  	              WHERE t5060.c205_part_number_id = v_pnum	
		  	              AND t5062.c5062_void_fl IS NULL
		  	              AND t5060.c5060_control_number = v_temp_cntl);
			 EXCEPTION WHEN NO_DATA_FOUND THEN
			  v5060_cnum := '-999';
			  v_part_inv := '-999';
			 END;	
	  	
		  	 IF v5060_cnum <> v_control THEN
		  	 	UPDATE t5062_control_number_reserve
		  		   SET c5062_void_fl = 'Y'
		    		 , c5062_last_updated_by = p_userid
		    		 , c5062_last_updated_date = CURRENT_DATE
				 WHERE c5062_transaction_id = p_refid
				   AND c5060_control_number_inv_id= v_part_inv
		  		   AND c5062_void_fl IS NULL;
	  	 	END IF;
	  	END IF;
  	END IF;

    INSERT
    INTO t5055_control_number
      (
        c5055_control_number_id,
        c5055_ref_id,
        c901_ref_type,
        c205_part_number_id,
        c5055_qty,
        c5055_control_number,
        c5052_location_id,
        c901_type,
        c5055_last_updated_by,
        c5055_last_updated_date,
        C901_warehouse_type,
        C205_CLIENT_PART_NUMBER_ID,
        C5040_PLANT_ID,
        C1900_COMPANY_ID
      )
      VALUES
      (
        s5055_control_number_id.NEXTVAL,
        p_refid,
        p_reftype,
        v_pnum,
        v_qty,
        v_control,
        v_locid,
        p_loc_oper,
        p_userid,
        CURRENT_DATE,
        v_whtype,
        v_frm_part,
        v_plant_id,
        v_comp_id
      );
      -- For PTRD-XXXX transactions, we need track LOT for TO Part as it should be in INVENTORY Qty.
     /*  IF(NVL(GET_RULE_VALUE('RE-DESIG PART',p_reftype),'N') = 'Y') THEN      
       		gm_pkg_op_lot_track.gm_lot_track_main(v_pnum,
                               						   v_qty,
                               						   p_refid,
                               						   p_userid,
                               						   90800, -- SHELF  
                               						   4301,-- PLUS
                               						   120488 -- Repackage to Shelf
            );  
	  END IF;*/
	
	  SELECT COUNT(1) INTO v_excl_txn_cnt FROM t906_rules
	  WHERE c906_rule_id = p_reftype
	  	AND c906_rule_grp_id = 'TXN_RULE'
	  	AND c906_void_fl IS NULL;
	  
	  -- Check whether the validation is applicable for the part number or not, 104480: Lot tracking required ?
      v_flag := get_part_attr_value_by_comp(v_pnum,'104480',v_comp_id);
      -- Get the error count from the t5055 table to update the lot status based on that
      SELECT count(1) INTO v_err_cnt
		FROM t5055_control_number
	  WHERE c5055_ref_id    = p_refid
		AND c5055_error_dtls IS NOT NULL
		AND c5055_void_fl    IS NULL;
  
		-- If the Release check box is checked and no validation available for the lot, the change the status to controlled 
		IF p_rel_verify_fl = 'on' AND v_err_cnt = 0 AND v_flag = 'Y' AND v_excl_txn_cnt > 0 THEN
			UPDATE t2550_part_control_number
			SET C901_LOT_CONTROLLED_STATUS = '105008' --Controlled
			  , c2550_last_updated_by = p_userid
			  , c2550_last_updated_date = CURRENT_DATE
			WHERE c205_part_number_id = v_pnum
				AND c2550_control_number = v_control;
		END IF;
      	
  END LOOP;
END gm_sav_item_control;
/*******************************************************
* Description : Procedure to save in t5055a_item_control_log
* author  : Gopinathan
*******************************************************/
PROCEDURE gm_sav_item_control_log
  (
    p_trans_id   IN t5055_control_number.c5055_ref_id%TYPE,
    p_trans_type IN t5055_control_number.c901_ref_type%TYPE,
    p_loc_oper   IN t5055_control_number.c901_type%TYPE,
    p_userid     IN t101_user.c101_user_id%TYPE
  )
AS
  v_batch_id NUMBER;
  v_plant_id t5040_plant_master.c5040_plant_id%TYPE;
BEGIN
  SELECT s5055a_control_batch_id.NEXTVAL INTO v_batch_id FROM DUAL;
  SELECT  NVL(get_plantid_frm_cntx(),get_rule_value('DEFAULT_PLANT','ONEPORTAL')) INTO  v_plant_id FROM DUAL;
  INSERT
  INTO t5055a_control_number_log
    (
      c5055a_control_number_log_id,
      c5055a_control_batch_id,
      c5055_ref_id,
      c901_ref_type,
      c205_part_number_id,
      c5055_qty,
      c5055_control_number,
      c5052_location_id,
      c901_type,
      c5055_last_updated_by,
      c5055_last_updated_date,
      c5040_plant_id
    )
  SELECT c5055_control_number_id,
    v_batch_id,
    c5055_ref_id,
    c901_ref_type,
    c205_part_number_id,
    c5055_qty,
    c5055_control_number,
    c5052_location_id,
    c901_type,
    p_userid,
    CURRENT_DATE,
    v_plant_id
  FROM t5055_control_number
  WHERE c5055_ref_id = p_trans_id
  AND c901_ref_type  = p_trans_type
  AND c901_type      = p_loc_oper
  AND c5055_void_fl IS NULL;
  DELETE
  FROM t5055_control_number
  WHERE c5055_ref_id = p_trans_id
  AND c901_type      = p_loc_oper;
END gm_sav_item_control_log;
/**********************************************************************
* Description : Procedure to save the inhouse details for any transaction.
*
* Author    : Ritesh
**********************************************************************/
PROCEDURE gm_sav_inhouse_txn
  (
    p_lcnqtymsgstr IN VARCHAR2,
    p_userid       IN t520_request.c520_created_by%TYPE,
    p_transtype    IN t412_inhouse_transactions.c412_type%TYPE,
    p_out_id OUT t412_inhouse_transactions.c412_inhouse_trans_id%TYPE )
AS
  v_outtxnid     VARCHAR2 (100);
  v_message      VARCHAR2 (1000);
  v_lcnid        VARCHAR2 (100);
  v_lcnqtymsgstr VARCHAR2 (1000);
  v_act_loc_id   VARCHAR2 (1000);
BEGIN
  v_outtxnid     := get_next_consign_id (p_transtype);
  p_out_id       := v_outtxnid;
  v_lcnid        := SUBSTR (p_lcnqtymsgstr, 1, INSTR (p_lcnqtymsgstr, ',', 1, 1) - 1);
  v_lcnqtymsgstr := SUBSTR (p_lcnqtymsgstr, INSTR (p_lcnqtymsgstr, ',', 1, 1)    + 1);
  BEGIN
    SELECT T5052.C5052_LOCATION_CD
    INTO v_act_loc_id
    FROM T5052_LOCATION_MASTER t5052
    WHERE T5052.C5052_LOCATION_ID = v_lcnid
    AND T5052.C5052_VOID_FL      IS NULL;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    v_act_loc_id := v_lcnid;
  END;
  gm_save_inhouse_item_consign (v_outtxnid, p_transtype, '93351' --Bulk to shelf (purpose/reason)
  , NULL                                                         --shipto
  , NULL                                                         --shipto id
  , 'Location Transfer Initiated for Location :' || v_act_loc_id, p_userid, v_lcnqtymsgstr, v_message );
END gm_sav_inhouse_txn;
/*******************************************************
* Description : Procedure to save in t5055a_item_control_log
* author    : Gopinathan
*******************************************************/
PROCEDURE gm_sav_item_control_main
  (
    p_trans_id      IN t412_inhouse_transactions.c412_inhouse_trans_id%TYPE,
    p_trans_type    IN t412_inhouse_transactions.c412_type%TYPE,
    p_rel_verify_fl IN VARCHAR2,
    p_inputstr      IN CLOB,
    p_loc_oper      IN t5055_control_number.c901_type%TYPE,
    p_userid        IN t101_user.c101_user_id%TYPE
  )
AS
  v_pick_loc VARCHAR2 (20);
  v_table t906_rules.c906_rule_value%TYPE;
  v_req_ship_fl VARCHAR2 (10);
  v_err_cnt		NUMBER;
  v_source VARCHAR2 (20);
  v_Insert_fl	VARCHAR2 (10);
  v_In_txn_fl	VARCHAR2 (10);
  v_loc_fl		VARCHAR2 (10);
  v_plant_id      t5040_plant_master.c5040_plant_id%TYPE;
  v_comp_id		  t1900_company.c1900_company_id%TYPE;
  v_rule_group 	  T906_RULES.C906_RULE_GRP_ID%TYPE;
  v_txn_type	  T5056_TRANSACTION_INSERT.C901_REF_TYPE%TYPE;
  v_trans_fl	 VARCHAR2 (10);
BEGIN	

	 SELECT  get_plantid_frm_cntx(),get_compid_frm_cntx()
    	  INTO  v_plant_id,v_comp_id  FROM DUAL;  
	v_req_ship_fl := get_rule_value ('REQ_SHIP_REL', p_trans_type);

  gm_validate_txn_verify (p_trans_id, p_trans_type);
  gm_sav_item_control (p_trans_id, p_trans_type, p_inputstr, p_loc_oper, p_userid, p_rel_verify_fl );
  -- Pick quantity validation
  v_pick_loc   := get_rule_value ('PICK_LOC', p_trans_type);
  v_table      := get_rule_value ('TRANS_TABLE', p_trans_type);
  
 
    SELECT get_rule_value(v_plant_id,'INSERT_PART') 
  	 INTO v_Insert_fl
  	FROM DUAL; 
  	
  	v_rule_group := 'INSERT_TXN_MAP'||'_'||v_plant_id;
  	SELECT get_rule_value(p_trans_type,v_rule_group) 
	 INTO v_txn_type
	FROM DUAL;
	
  	SELECT get_rule_value(v_txn_type,'INSERT_TRANS') 
  	 INTO v_In_txn_fl 
  	FROM DUAL; 
  	
  	SELECT get_rule_value(v_plant_id,'INSERT_LOC') 
   	  INTO v_loc_fl
   	  FROM DUAL;
		
   	SELECT get_rule_value(p_trans_type,'FGTXN') 
  	  INTO v_trans_fl
  	  FROM DUAL;
  	  
   	 IF v_pick_loc = 'YES' THEN
    gm_validate_loc_qty (p_trans_id, p_trans_type, p_loc_oper);
  END IF;
  IF ( (v_table = 'ORDERS' AND p_rel_verify_fl = 'on') OR v_table = 'IN_HOUSE' OR v_table = 'CONSIGNMENT' ) THEN
    IF( v_table = 'IN_HOUSE' OR v_table = 'CONSIGNMENT') THEN
  	--qty_upd_chk validation
    gm_validate_txn_qty (p_trans_id, p_trans_type, 'pick');
    END IF;
    --Procedure to reduce the Location qty for inserts 
    --while controlling the transaction by satisfying the below conditions
    --Release flag should checked and insert should be enabled for company and transactions
  	IF p_rel_verify_fl = 'on' AND v_Insert_fl ='Y' AND v_In_txn_fl = 'Y' AND v_loc_fl = 'Y' THEN
    	gm_pkg_op_sav_insert_txn.gm_sav_insert_location_qty (p_trans_id, p_trans_type, 93343, p_userid);
   END IF;
   
    --93343 is for pick transacted validation.
    --used to save the transaction details in t413,t504,t502...
    gm_sav_txn_details (p_trans_id, p_trans_type, 93343,v_comp_id);
    --93343 is for pick transacted
    
  END IF;
  
  SELECT count(1) INTO v_err_cnt
	FROM t5055_control_number
  WHERE c5055_ref_id    = p_trans_id
	AND c5055_error_dtls IS NOT NULL
	AND c5055_void_fl    IS NULL;
  
  IF p_rel_verify_fl = 'on' AND v_err_cnt = 0 THEN
    gm_sav_trans_flag (p_trans_id, p_trans_type, p_userid);
    
    --v_req_ship_fl is used to update the request and release it for shipping.
	IF  v_req_ship_fl = 'YES' AND v_table = 'IN_HOUSE' AND p_trans_type = '50154' THEN
		-- v_req_ship_fl is YES for FGLE, to skip the verification process
		-- For FGLE(50154), while controlling itself need to do the verification process also, 93345:Put Transacted
		gm_sav_item_verify_main(p_trans_id,p_trans_type,p_inputstr,'93345',p_userid);

	END IF;
	
  END IF;
  
  IF get_rule_value ('RELEASE_SHIPPING', p_trans_type) = 'Y' -- IHLN, FGRT,QNRT
  THEN
  	v_source := get_rule_value ('SHIPPING_SOURCE', p_trans_type);
 	gm_pkg_cm_shipping_trans.gm_sav_release_shipping(p_trans_id, v_source, p_userid, NULL); --50186 - Inhouse Loaner
  END IF;
    --Add new condition 
  IF v_table = 'ORDERS' AND p_rel_verify_fl = 'on' THEN
	GM_PKG_OP_INV_SCAN.gm_sav_inv_status(p_trans_id,'93006',null,p_userid);  
  END IF; 
  
 --PC-407 -Record the Inventory change of Transactions
 --Pass the value of p_trans_type as the rule id and rule group 'FGTXN' and get value to v_trans_fl
-- Add below at the end to update the status in T5050_INVPICK_ASSIGN_DETAIL table since it  is not getting updated while controlling the inhouse transactions 
--Save the status as controlled - 93111
--Since for FGLE txn it will be verified when it is controlled, so controlled status is not set for FGLE txn (50154)
  
  IF p_rel_verify_fl = 'on' AND (v_table = 'IN_HOUSE' OR v_table = 'CONSIGNMENT') AND  p_trans_type != '50154'  AND  v_trans_fl = 'Y'
  THEN
  gm_pkg_op_inv_scan.gm_sav_inv_status(p_trans_id,'93111',null,p_userid);
  
  END IF;

      
END gm_sav_item_control_main;
/*******************************************************
* Description : Procedure to validate the entered qty with the pick location qty
* author  : Gopinathan
*******************************************************/
PROCEDURE gm_validate_loc_qty
  (
    p_trans_id   IN t412_inhouse_transactions.c412_inhouse_trans_id%TYPE,
    p_trans_type IN t412_inhouse_transactions.c412_type%TYPE,
    p_loc_oper   IN t5055_control_number.c901_type%TYPE )
AS
  err_msg VARCHAR2 (500) := '';
  CURSOR cur_pick_locs
  IS
    SELECT c205_part_number_id pnum,
      c5052_location_id loc_id,
      SUM (c5055_qty) sum_qty
    FROM t5055_control_number
    WHERE c5055_ref_id     = p_trans_id
    AND c901_type          = p_loc_oper --should be pick transacted
    AND c5052_location_id IS NOT NULL
    GROUP BY c205_part_number_id,
      c5052_location_id;
BEGIN
  --validation 1 - to validate the entered qty with the pick location qty.
  FOR v_loc IN cur_pick_locs
  LOOP
    IF ( gm_pkg_op_item_control_rpt.get_part_location_cur_qty (v_loc.pnum, v_loc.loc_id ) + v_loc.sum_qty ) < v_loc.sum_qty --need to add already picked ones
      THEN
      err_msg := err_msg || 'For a part ' || v_loc.pnum || ', picked quantity is more than available quantity for the following location ' || v_loc.loc_id || ',';
    END IF;
  END LOOP;
  IF LENGTH (err_msg) > 0 THEN
    raise_application_error ('-20401', err_msg);
  END IF;
END gm_validate_loc_qty;
/*******************************************************
* Description : Procedure to validate qty of transaction tables to control table.
* author  : Gopinathan
*******************************************************/
PROCEDURE gm_validate_txn_qty
  (
    p_trans_id   IN t412_inhouse_transactions.c412_inhouse_trans_id%TYPE,
    p_trans_type IN t412_inhouse_transactions.c412_type%TYPE,
    p_pickput_fl VARCHAR2 )
AS
  v_qty_upd_chk VARCHAR2 (10);
  v_count       NUMBER := 0;
  v_table t906_rules.c906_rule_value%TYPE;
BEGIN
  --validation 2 - to validate the qty of [t413,t502,t505] with t5055
  v_qty_upd_chk   := 'YES';
  IF p_pickput_fl  = 'pick' THEN
    v_qty_upd_chk := get_rule_value ('QTY_UPD_CHK', p_trans_type);
  END IF;
  IF v_qty_upd_chk = 'YES' THEN
    v_table       := get_rule_value ('TRANS_TABLE', p_trans_type);
    IF v_table     = 'IN_HOUSE' THEN
      -- Check the qty of t413 with t5055 for same transaction id and part number.
      --If not matched then throw exception.
      SELECT COUNT (1)
      INTO v_count
      FROM
        (SELECT nvl(c205_client_part_number_id,c205_part_number_id) c205_part_number_id,
          SUM (c413_item_qty) itemqty
        FROM t413_inhouse_trans_items
        WHERE c412_inhouse_trans_id = p_trans_id
        AND c413_void_fl           IS NULL
        GROUP BY nvl(c205_client_part_number_id,c205_part_number_id)
        ) t413
      FULL OUTER JOIN
        (SELECT c205_part_number_id,
          SUM (c5055_qty ) locqty
        FROM t5055_control_number
        WHERE c5055_ref_id = p_trans_id
        AND c901_type      = DECODE (p_pickput_fl, 'pick', '93343', '93345' )
          -- pick transacted
        AND c5055_void_fl IS NULL
        GROUP BY c205_part_number_id
        ) t5055
      ON t413.c205_part_number_id = t5055.c205_part_number_id
      WHERE NVL (itemqty, 0)     != NVL (locqty, 0);
    ELSIF v_table                 = 'CONSIGNMENT' THEN
      SELECT COUNT (1)
      INTO v_count
      FROM
        (SELECT c205_part_number_id,
          SUM (c505_item_qty) itemqty
        FROM t505_item_consignment
        WHERE c504_consignment_id = p_trans_id
        AND c505_void_fl         IS NULL
        GROUP BY c205_part_number_id
        ) t505
      FULL OUTER JOIN
        (SELECT c205_part_number_id,
          SUM (c5055_qty ) locqty
        FROM t5055_control_number
        WHERE c5055_ref_id = p_trans_id
        AND c901_type      = DECODE (p_pickput_fl, 'pick', '93343', '93345' )
          -- pick transacted
        AND c5055_void_fl IS NULL
        GROUP BY c205_part_number_id
        ) t5055
      ON t505.c205_part_number_id = t5055.c205_part_number_id
      WHERE NVL (itemqty, 0)     != NVL (locqty, 0);
    ELSIF v_table                 = 'ORDERS' THEN
      SELECT COUNT (1)
      INTO v_count
      FROM
        (SELECT c205_part_number_id,
          SUM (c502_item_qty) itemqty
        FROM t502_item_order
        WHERE c501_order_id         = p_trans_id
        AND c502_void_fl           IS NULL
        AND NVL (c901_type, 99999) != 50301
        GROUP BY c205_part_number_id
        ) t501
      FULL OUTER JOIN
        (SELECT c205_part_number_id,
          SUM (c5055_qty ) locqty
        FROM t5055_control_number
        WHERE c5055_ref_id = p_trans_id
        AND c901_type      = DECODE (p_pickput_fl, 'pick', '93343', '93345' )
          -- pick transacted
        AND c5055_void_fl IS NULL
        GROUP BY c205_part_number_id
        ) t5055
      ON t501.c205_part_number_id = t5055.c205_part_number_id
      WHERE NVL (itemqty, 0)     != NVL (locqty, 0);
    END IF;
  END IF;
  IF v_count > 0 THEN
    raise_application_error ('-20920', '');
  END IF;
END gm_validate_txn_qty;
/************************************************************************
* Description : Procedure to update parent order detail in t5055 table for the pending shipping to shelf
* author  : Gopinathan
***********************************************************************/
PROCEDURE gm_sav_psfg_parent_ord_detail
  (
    p_order_id IN t502_item_order.c501_order_id%TYPE,
    p_trans_id IN t412_inhouse_transactions.c412_inhouse_trans_id%TYPE,
    p_user_id  IN t5055_control_number.c5055_last_updated_by%TYPE )
AS
  CURSOR cur_t413
  IS
    SELECT t413.c205_part_number_id pnum,
      t413.c413_control_number cnum,
      t413.c413_item_qty qty
    FROM t413_inhouse_trans_items t413
    WHERE t413.c412_inhouse_trans_id = p_trans_id
    AND t413.c413_void_fl           IS NULL;
  CURSOR cur_t5055
  IS
    SELECT c5055_control_number_id ctrl_id,
      c205_part_number_id pnum,
      c5055_control_number cnum,
      c5055_qty qty,
      c5052_location_id loc_id
    FROM t5055_control_number
    WHERE c5055_ref_id       = p_order_id
    AND c5055_void_fl       IS NULL
    AND c5055_update_inv_fl IS NULL;
  
  v_qty t413_inhouse_trans_items.c413_item_qty%TYPE;
  v_ctrl_num_id t5055_control_number.c5055_control_number_id%TYPE;
  v_updated_records_str VARCHAR2 (500)  := '';
  v_stringtemp          VARCHAR2 (4000) := '';
BEGIN
  FOR v_cur_t413 IN cur_t413
  LOOP
    v_qty           := v_cur_t413.qty;
    FOR v_cur_t5055 IN cur_t5055
    LOOP
      IF v_cur_t413.pnum         = v_cur_t5055.pnum AND v_cur_t413.cnum = v_cur_t5055.cnum THEN
        IF v_qty                 = v_cur_t5055.qty THEN
          IF v_cur_t5055.loc_id IS NOT NULL THEN
            v_stringtemp        := v_stringtemp || v_cur_t5055.pnum || ',' || v_cur_t5055.qty || ',' || v_cur_t5055.loc_id || '|';
          END IF;
          DELETE
          FROM t5055_control_number
          WHERE c5055_control_number_id = v_cur_t5055.ctrl_id;
          EXIT;
        ELSIF v_qty              > v_cur_t5055.qty THEN
          IF v_cur_t5055.loc_id IS NOT NULL THEN
            v_stringtemp        := v_stringtemp || v_cur_t5055.pnum || ',' || v_cur_t5055.qty || ',' || v_cur_t5055.loc_id || '|';
          END IF;
          v_qty := (v_qty - v_cur_t5055.qty);
          DELETE
          FROM t5055_control_number
          WHERE c5055_control_number_id = v_cur_t5055.ctrl_id;
        ELSIF v_qty                     < v_cur_t5055.qty THEN
          IF v_cur_t5055.loc_id        IS NOT NULL THEN
            v_stringtemp               := v_stringtemp || v_cur_t5055.pnum || ',' || v_qty || ',' || v_cur_t5055.loc_id || '|';
          END IF;
          UPDATE t5055_control_number
          SET c5055_qty                 = c5055_qty - v_qty,
            c5055_last_updated_by       = p_user_id,
            c5055_last_updated_date     = CURRENT_DATE
          WHERE c5055_control_number_id = v_cur_t5055.ctrl_id
          AND c5055_void_fl            IS NULL
          AND c5055_update_inv_fl      IS NULL;
          EXIT;
        END IF;
      END IF;
    END LOOP;
  END LOOP;
  gm_pkg_op_inv_scan.gm_upd_inventory_loc_qty (p_order_id, v_stringtemp, 93326, p_user_id );
  --Need to update update_inv_fl as null. Otherwise,while shipout the Order we get error message.
  UPDATE t5055_control_number
  SET c5055_update_inv_fl   = NULL,
    c5055_last_updated_date = CURRENT_DATE,
    c5055_last_updated_by   = p_user_id
  WHERE c5055_ref_id        = p_order_id
  AND c901_type             = 93343
  AND c5055_void_fl        IS NULL
  AND c5055_update_inv_fl  IS NOT NULL;
END gm_sav_psfg_parent_ord_detail;
/*******************************************************
* Description : Procedure to save transaction status flag
* author  : Gopinathan
*******************************************************/
PROCEDURE gm_sav_trans_flag
  (
    p_trans_id   IN t412_inhouse_transactions.c412_inhouse_trans_id%TYPE,
    p_trans_type IN t412_inhouse_transactions.c412_type%TYPE,
    p_userid     IN t101_user.c101_user_id%TYPE )
AS
  v_table t906_rules.c906_rule_value%TYPE;
  v_status_flag t412_inhouse_transactions.c412_status_fl%TYPE;
  v_rqid t504_consignment.c520_request_id%TYPE;
  v_ref_id t412_inhouse_transactions.c412_ref_id%TYPE;
  v_purpose t412_inhouse_transactions.c412_inhouse_purpose%TYPE;
  v_msg         VARCHAR2 (200);
  v_taggable    VARCHAR2 (10);
  v_req_ship_fl VARCHAR2 (10);
  v_dev_txn_upd VARCHAR2 (10);
  v_rule_status VARCHAR2 (10);
  v_lnsetstatus VARCHAR2 (10);
  v_count       NUMBER := 0;
  v_order_typ   NUMBER := 0;
  v_status_fl   VARCHAR2 (10) := '2';
  v_ship_source t907_shipping_info.c901_source%TYPE;
BEGIN
  SELECT get_rule_value ('TRANS_TABLE', p_trans_type) INTO v_table FROM DUAL;
  
  v_req_ship_fl := get_rule_value ('REQ_SHIP_REL', p_trans_type); --Since this value is using in 3 places, Moved to the beginning
  
  IF v_table = 'IN_HOUSE' THEN
    SELECT c412_status_fl statusfl,
      c412_ref_id,
      gm_pkg_op_loaner.get_cs_fch_loaner_status (c412_ref_id),
      c412_inhouse_purpose
    INTO v_status_flag,
      v_ref_id,
      v_lnsetstatus,
      v_purpose
    FROM t412_inhouse_transactions
    WHERE c412_inhouse_trans_id = p_trans_id FOR UPDATE;
    IF v_status_flag           >= '3' THEN
      raise_application_error ('-20921', '');
    END IF;
    SELECT COUNT (1)
    INTO v_count
    FROM t413_inhouse_trans_items
    WHERE c412_inhouse_trans_id     = p_trans_id
    AND TRIM (c413_control_number) IS NULL;
    IF v_count                      > 0 THEN
      GM_RAISE_APPLICATION_ERROR('-20999','223','');
    END IF;
    UPDATE t412_inhouse_transactions
    SET c412_status_fl          = '3',
      c412_last_updated_by      = p_userid,
      c412_last_updated_date    = CURRENT_DATE
    WHERE c412_inhouse_trans_id = p_trans_id
    AND c412_void_fl           IS NULL;
    --Need to update Order records for Pending Shipping to shelf (100406)
    IF p_trans_type IN ('100406') THEN
      gm_update_pendship_ord (v_ref_id, v_purpose, p_userid, p_trans_id, v_msg );
    END IF;

	  
    --below code added for the transactions like shelf to loaner, shelf to inhouse loaner
    IF p_trans_type NOT IN ('50154') THEN
      --Avoid Loaner CN status update if Loaner CN is â€˜Pending returnâ€™
      v_rule_status    := get_rule_value ('REF_UPD_AVOID_STATUS', p_trans_type);
      IF v_lnsetstatus <> v_rule_status THEN
        gm_ls_upd_csg_loanerstatus (3, 40, v_ref_id, p_userid);
      END IF;
    END IF;
  ELSIF v_table = 'CONSIGNMENT' THEN
    SELECT c504_status_fl statusfl,
      c520_request_id
    INTO v_status_flag,
      v_rqid
    FROM t504_consignment
    WHERE c504_consignment_id = p_trans_id FOR UPDATE;
    IF v_status_flag         >= '3' THEN
      raise_application_error ('-20921', '');
    END IF;
    SELECT COUNT (1)
    INTO v_count
    FROM t505_item_consignment
    WHERE c504_consignment_id       = p_trans_id
    AND TRIM (c505_control_number) IS NULL;
    IF v_count                      > 0 THEN
      GM_RAISE_APPLICATION_ERROR('-20999','223',''); 
    END IF;
    --v_req_ship_fl := get_rule_value ('REQ_SHIP_REL', p_trans_type);
    --v_req_ship_fl is used to update the request and release it for shipping.
    IF v_req_ship_fl = 'YES' THEN
      -- consignment 50181, Stock Transfer 26240435
      -- if type for is Stock transfer  then ship to source is Stock trasfer(26240435) else consignment(50181)
      SELECT DECODE(p_trans_type,'106703','26240435','106704','26240435','50181')
       INTO v_ship_source
      FROM DUAL;
      
      gm_pkg_cm_shipping_trans.gm_sav_release_shipping (p_trans_id, v_ship_source, p_userid, '' );
      UPDATE t520_request
      SET c520_status_fl       = '30' ,
        c520_last_updated_by   = p_userid ,
        c520_last_updated_date = CURRENT_DATE
      WHERE c520_request_id    = v_rqid
      AND c520_void_fl        IS NULL;
    END IF;
    UPDATE t504_consignment
    SET c504_status_fl        = '3',
      c504_last_updated_by    = p_userid,
      c504_last_updated_date  = CURRENT_DATE
    WHERE c504_consignment_id = p_trans_id
    AND c504_void_fl         IS NULL;
  ELSIF v_table               = 'ORDERS' THEN
    SELECT c501_status_fl statusfl ,NVL(C901_ORDER_TYPE,'-9999') 
    INTO v_status_flag,v_order_typ
    FROM t501_order
    WHERE c501_order_id = p_trans_id FOR UPDATE;
    IF v_status_flag   >= '2' THEN
      raise_application_error ('-20921', '');
      --This transaction has been already released for shipping, cannot
      -- be processed.
    END IF;
    SELECT COUNT (1)
    INTO v_count
    FROM t502_item_order
    WHERE c501_order_id             = p_trans_id 
    AND C502_VOID_FL IS NULL 
    AND TRIM (c502_control_number) IS NULL;
    IF v_count                      > 0 THEN
      GM_RAISE_APPLICATION_ERROR('-20999','223','');
    END IF;
    --v_req_ship_fl := get_rule_value ('REQ_SHIP_REL', p_trans_type);
    --v_req_ship_fl is used to update the request and release it for shipping.
    IF v_req_ship_fl = 'YES' THEN
      -- 50180 Orders
      gm_pkg_cm_shipping_trans.gm_sav_release_shipping (p_trans_id, 50180, p_userid, '' );
    END IF;
    -- When create 2530,2532 order type the status flag value need to update 3 eventhough without cust po id.
    -- Other order type status flag 2 when update control number.
  
     IF NVL(get_rule_value(v_order_typ,'BO_AVOID_SHIPPING'),'-9999') = 'YES'  THEN
        	v_status_fl := '3';
        END IF;
        
        UPDATE t501_order
        SET c501_status_fl  = v_status_fl, c501_last_updated_by = p_userid, c501_last_updated_date = CURRENT_DATE
        WHERE c501_order_id = p_trans_id
        AND c501_void_fl IS NULL;

  END IF;
  --Taggable part validation
  v_taggable   := get_rule_value ('TAGGED_PART', p_trans_type);
  IF v_taggable = 'YES' THEN
    gm_pkg_op_item_control_rpt.gm_fch_validate_tagpart (p_trans_id, p_trans_type );
  END IF;
  --to update the completed status in device
  v_dev_txn_upd   := get_rule_value ('DEV_TXN_UPD', p_trans_type);
  IF v_dev_txn_upd = 'YES' THEN
    -- When Consigment is Completed, Wipe out from the Inventory Pick queue Table.
    gm_pkg_op_inv_scan.gm_check_inv_order_status (p_trans_id, 'Completed', p_userid );
  END IF;
END gm_sav_trans_flag;
/*******************************************************
* Description : Procedure to save transaction status flag
* author  : Gopinathan
*******************************************************/
PROCEDURE gm_sav_txn_details
  (
    p_trans_id   IN t412_inhouse_transactions.c412_inhouse_trans_id%TYPE,
    p_trans_type IN t412_inhouse_transactions.c412_type%TYPE,
    p_loc_oper   IN t5055_control_number.c901_type%TYPE,
    p_companyid  IN t1900_company.c1900_company_id%TYPE)
AS
  v_table t906_rules.c906_rule_value%TYPE;
  v_part_price VARCHAR2(20);
  v_part       VARCHAR2 (1000);
  CURSOR cur_item
  IS
    SELECT c205_part_number_id pnum
    FROM t5055_control_number
    WHERE c5055_ref_id = p_trans_id
    AND c901_ref_type  = p_trans_type
    AND c901_type      = p_loc_oper
    AND c5055_void_fl IS NULL;
BEGIN
  SELECT get_rule_value ('TRANS_TABLE', p_trans_type) INTO v_table FROM DUAL;
  IF v_table = 'IN_HOUSE' THEN
    --t413 records price will be retain for Pending Shipping to Shelf.
    IF p_trans_type IN ('100406') THEN
      gm_sav_psfg_detail (p_trans_id, p_trans_type, p_loc_oper);
    ELSE
      DELETE t413_inhouse_trans_items WHERE c412_inhouse_trans_id = p_trans_id;
      INSERT
      INTO t413_inhouse_trans_items
        (
          c413_trans_id,
          c412_inhouse_trans_id,
          c205_part_number_id,
          c413_item_qty,
          c413_control_number,
          c413_item_price,
          c413_status_fl,
          c413_created_date,
          c205_client_part_number_id
        )
      SELECT s413_inhouse_item.NEXTVAL,
        c5055_ref_id,
        DECODE(p_trans_type,'103932', c205_client_part_number_id,c205_part_number_id) frompartnum,
        c5055_qty,
        c5055_control_number,
        get_part_price (c205_part_number_id, 'E'),
        DECODE(p_trans_type, 9106, 'R', 9104, 'R',NULL),
        CURRENT_DATE,
        DECODE(p_trans_type,'103932', c205_part_number_id,c205_client_part_number_id) topartnum
      FROM
        (SELECT c5055_ref_id,
          c205_part_number_id,
          SUM (c5055_qty) c5055_qty,
          c5055_control_number,
          c205_client_part_number_id
        FROM t5055_control_number
        WHERE c5055_ref_id = p_trans_id
        AND c901_ref_type  = p_trans_type
        AND c901_type      = p_loc_oper
        AND c5055_void_fl IS NULL
        GROUP BY c5055_ref_id,
          c205_part_number_id,
          c5055_control_number,
          c205_client_part_number_id
        );
      --Whenever the child is updated, the parent inhouse transactions table has to be updated ,so ETL can Sync it to GOP.
      UPDATE t412_inhouse_transactions
      SET c412_last_updated_date  = CURRENT_DATE
      WHERE c412_inhouse_trans_id = p_trans_id;
    END IF;
  ELSIF v_table   = 'CONSIGNMENT' THEN
    FOR item_val IN cur_item
    LOOP
      v_part_price    := get_part_price (item_val.pnum, 'E',p_companyid) ;
      IF v_part_price IS NULL THEN
        v_part        := v_part || item_val.pnum || ',';
      END IF;
    END LOOP;
    IF v_part IS NOT NULL THEN
      GM_RAISE_APPLICATION_ERROR('-20999','224',v_part||'#$#'||p_trans_id);
    END IF;
    DELETE t505_item_consignment WHERE c504_consignment_id = p_trans_id;
    INSERT
    INTO t505_item_consignment
      (
        c505_item_consignment_id,
        c504_consignment_id,
        c205_part_number_id,
        c505_item_qty,
        c505_control_number,
        c505_item_price,
        C901_WAREHOUSE_TYPE
      )
    SELECT s504_consign_item.NEXTVAL,
      c5055_ref_id,
      c205_part_number_id,
      c5055_qty,
      c5055_control_number,
      get_part_price (c205_part_number_id, 'E',p_companyid),
      C901_warehouse_type
    FROM
      (SELECT c5055_ref_id,
        c205_part_number_id,
        SUM (c5055_qty) c5055_qty,
        c5055_control_number,
        C901_warehouse_type
      FROM t5055_control_number
      WHERE c5055_ref_id = p_trans_id
      AND c901_ref_type  = p_trans_type
      AND c901_type      = p_loc_oper
      AND c5055_void_fl IS NULL
      GROUP BY c5055_ref_id,
        c205_part_number_id,
        C901_warehouse_type,
        c5055_control_number
      );
    --When ever the child is updated, the parent table has to be updated ,so ETL can Sync it to GOP.
    UPDATE t504_consignment
    SET c504_last_updated_date = CURRENT_DATE
    WHERE c504_consignment_id  = p_trans_id;
  ELSIF v_table                = 'ORDERS' THEN
    gm_pkg_op_item_control_txn.gm_upt_item_order (p_trans_id, p_trans_type, p_loc_oper );
  END IF;
END gm_sav_txn_details;
/*******************************************************
* Description : Procedure to save in t5055a_item_control_log
* author  : Gopinathan
*******************************************************/
PROCEDURE gm_sav_item_verify_main
  (
    p_trans_id   IN t412_inhouse_transactions.c412_inhouse_trans_id%TYPE,
    p_trans_type IN t412_inhouse_transactions.c412_type%TYPE,
    p_inputstr   IN CLOB,
    p_loc_oper   IN t5055_control_number.c901_type%TYPE,
    p_userid     IN t101_user.c101_user_id%TYPE
  )
AS
  v_count   NUMBER := 0;
  v_message VARCHAR2 (50);
  v_assignid t5050_invpick_assign_detail.c5050_invpick_assign_id%TYPE;
  v_pick_loc    VARCHAR2 (20);
  v_put_loc     VARCHAR2 (20);
  v_lnsetstatus VARCHAR2 (10);
  v_plant_id    t5040_plant_master.c5040_plant_id%TYPE;
  v_out_cur  		TYPES.cursor_type;
BEGIN
  SELECT  NVL(get_plantid_frm_cntx(),get_rule_value('DEFAULT_PLANT','ONEPORTAL')) INTO  v_plant_id FROM DUAL;

  gm_validate_txn_verify (p_trans_id, p_trans_type);
  -- save t5055
  gm_sav_item_control (p_trans_id, p_trans_type, p_inputstr, p_loc_oper, p_userid );
  --qty_upd_chk validation
  gm_validate_txn_qty (p_trans_id, p_trans_type, 'put');
  --update for pick_loc  (minus)
  v_pick_loc      := get_rule_value ('PICK_LOC', p_trans_type);
  IF p_trans_type IN ('50155') THEN
    SELECT gm_pkg_op_loaner.get_cs_fch_loaner_status (c412_ref_id)
    INTO v_lnsetstatus
    FROM t412_inhouse_transactions
    WHERE c412_inhouse_trans_id = p_trans_id AND c5040_plant_id = v_plant_id;
  END IF;
  IF v_pick_loc = 'YES' THEN
    gm_sav_location_qty (p_trans_id, p_trans_type, 93343, p_userid);
  END IF;
  --update for put location (plus)
  v_put_loc   := get_rule_value ('PUT_LOC', p_trans_type);
  IF v_put_loc = 'YES' THEN
    gm_sav_location_qty (p_trans_id, p_trans_type, 93345, p_userid);
  END IF;
  -- update t412 status
  gm_update_reprocess_material (p_trans_id, 'FG Location Transfer for : ' || p_trans_id, p_trans_type, p_userid --
  -- p_verifiedby
  , p_userid, v_message -- OUT
  );
  -- update t5050 status
  BEGIN
    SELECT c5050_invpick_assign_id
    INTO v_assignid
    FROM t5050_invpick_assign_detail
    WHERE c5050_ref_id = p_trans_id
    AND c5050_void_fl IS NULL
    AND c5040_plant_id = v_plant_id;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    v_assignid := NULL;
  END;
  -- When the putaway save is called from Device, the p_trans_type passed will
  -- be the c901_code_nm_alt value for the transaction and that value will not be
  -- in the T5050 table. As C5050_ref_id and void fl combination is unique,we do not
  -- need to check the c901_ref_type value.
  --AND c901_ref_type = p_trans_type
  IF v_assignid IS NOT NULL THEN
    gm_pkg_op_inv_scan.gm_sav_inv_order_status (v_assignid, 'Completed', p_userid );
  END IF;
  IF p_trans_type IN ('50155') THEN
    --create shipping record for the LN
    gm_sav_loaner_ship (p_trans_id, v_lnsetstatus, p_userid);
  END IF;
  --Inhouser Loaner mapped txns to update parent txn
  IF p_trans_type IN ('9104','9106') -- BLIH(9104),FGIH(9106)
    THEN
    gm_pkg_op_ihloaner_item_txn.gm_sav_ihtxn_map(p_trans_id,p_userid,p_trans_type);
  END IF;
  
  IF p_trans_type = 100406 --Pending Shipping to FG
  THEN
  	-- while verifying a PSFG-XXXX for an order[Bill AND Ship], need to check whether all the parts in the order is loaner or not
	-- If all are loaner parts, then convert it into Bill Only Loaner and release it from shipping
  	gm_pkg_op_order_txn.gm_sav_release_lnorder(p_trans_id,p_trans_type,p_userid);
  	-- Fetch the trasaction details of In-House Transaction
  	gm_pkg_ship_rollback_txn. gm_fch_inhouse_txn_details (p_trans_id, v_out_cur);
  	-- Update the lot status to NULL from CONTROLLED for Voided transactions
	gm_pkg_ship_rollback_txn.gm_sav_rollback_lot_status(v_out_cur,p_userid); 

  END IF;
  
  gm_pkg_op_process_inhouse_txn.gm_update_shipping_status(p_trans_id,p_trans_type,p_userid);
END gm_sav_item_verify_main;
/*******************************************************
* Description : Procedure to update the pick and put location qty.
* author  : Gopinathan
*******************************************************/
PROCEDURE gm_sav_location_qty
  (
    p_trans_id   IN t5055_control_number.c5055_ref_id%TYPE,
    p_trans_type IN t5055_control_number.c901_ref_type%TYPE,
    p_loc_oper   IN t5055_control_number.c901_type%TYPE,
    p_userid     IN t101_user.c101_user_id%TYPE )
AS
  v_stringtemp VARCHAR2 (4000) := '';
  v_tempclob CLOB;
  v_location       VARCHAR2 (10);
  v_inv_type       VARCHAR2(20):= NULL;
  v_warehouse_type VARCHAR2(20):= NULL;
  v_whcnt          NUMBER;
  -- pick qty and locations
  -- Below Decode is a temp fix For Device -- At device 4110 and 4112 are considered as consignment
  CURSOR cur_pick_locs
  IS
    SELECT t5055.c205_part_number_id pnum,
      SUM (NVL (t5055.c5055_qty, 0)) qty,
      t5055.c5052_location_id loc
    FROM t5055_control_number t5055
    WHERE t5055.c901_ref_type    = DECODE (p_trans_type, 4112, t5055.c901_ref_type, 4110, t5055.c901_ref_type, p_trans_type )
    AND c5055_ref_id             = p_trans_id
    AND t5055.c901_type          = p_loc_oper
    AND t5055.c5055_void_fl     IS NULL
    AND t5055.c5052_location_id IS NOT NULL
    GROUP BY t5055.c205_part_number_id,
      t5055.c5052_location_id,
      t5055.c901_warehouse_type;
BEGIN
  v_inv_type       := get_rule_value('PLUS',p_trans_type);
  v_warehouse_type := get_rule_value(v_inv_type,'WHQTYTYP');
  -- Below code is added to validate the put away locations are belong to proper warehouse based on transaction type.
  -- Below code is not applicable for Orders, Consignments.
  IF v_warehouse_type IS NOT NULL AND p_loc_oper = 93345 THEN
    BEGIN
      SELECT COUNT(t5051.c901_warehouse_type)
      INTO v_whcnt
      FROM t5055_control_number T5055,
        t5052_location_master T5052 ,
        t5051_inv_warehouse T5051
      WHERE t5055.c5052_location_id    = t5052.c5052_location_id
      AND t5052.c5051_inv_warehouse_id = t5051.c5051_inv_warehouse_id
      AND t5052.c5052_void_fl         IS NULL
      AND t5055.c5055_ref_id           = p_trans_id
      AND t5055.c901_type              = p_loc_oper
      AND t5051.c901_warehouse_type   <> to_number(v_warehouse_type);
      IF v_whcnt                       > 0 THEN
        GM_RAISE_APPLICATION_ERROR('-20999','225','');
      END IF;
    END;
  END IF;
  FOR v_loc IN cur_pick_locs
  LOOP
    v_stringtemp := v_loc.pnum || ',' || v_loc.qty || ',' || v_loc.loc || '|';
    v_tempclob   := v_tempclob || v_stringtemp;
    v_stringtemp := '';
  END LOOP;
  -- pick - 93326, put - 93327 (because of device coding we need to change
  -- the number using below decode
  SELECT DECODE (p_loc_oper, 93343, 93326, 93327)
  INTO v_location
  FROM DUAL;
  -- location qty update for pick
  gm_pkg_op_inv_scan.gm_upd_inventory_loc_qty (p_trans_id, v_tempclob, v_location, p_userid );
END gm_sav_location_qty;
/*******************************************************
* Description : Procedure to void location transfer Transactions
* author  : Rajeshwaran.v
*******************************************************/
PROCEDURE gm_void_location_tranfer_txn
  (
    p_ref_id IN t907_cancel_log.c907_ref_id%TYPE,
    p_userid IN t102_user_login.c102_user_login_id%TYPE )
AS
BEGIN
  -- Void Records In T505 and T412 Tables
  gm_pkg_common_cancel_op.gm_op_sav_void_inhousetxn (p_ref_id, p_userid);
  -- below two procedures commented because same job doing by trigger
  -- Void Records In t5050_invpick_assign_detail
  --gm_pkg_allocation.gm_inv_void_bytxn (p_ref_id, '93341', p_userid);
  -- Void Records in t5055_control_number
  -- gm_pkg_op_item_control_txn.gm_void_control_number_txn (p_ref_id,p_userid);
END gm_void_location_tranfer_txn;
/*******************************************************
* Description : Procedure to void control Number records
* author    : Rajeshwaran.v
*******************************************************/
PROCEDURE gm_void_control_number_txn
  (
    p_ref_id IN t5055_control_number.c5055_ref_id%TYPE,
    p_userid IN t101_user.c101_user_id%TYPE )
AS
  v_voidcnt NUMBER := 0;
BEGIN
  SELECT COUNT (1)
  INTO v_voidcnt
  FROM t5055_control_number
  WHERE c5055_ref_id = p_ref_id
  AND c5055_void_fl IS NOT NULL;
  IF v_voidcnt       > 0 THEN
    raise_application_error (-20817, '');
  END IF;
  UPDATE t5055_control_number
  SET c5055_void_fl         = 'Y',
    c5055_last_updated_by   = p_userid,
    c5055_last_updated_date = CURRENT_DATE
  WHERE c5055_ref_id        = p_ref_id
  AND c5055_void_fl        IS NULL;
END gm_void_control_number_txn;
/**************************************************************************************************
* Description : Procedure to save the Initiate Replenishment.
* Author    : Murali
**************************************************************************************************/
PROCEDURE gm_sav_init_replenishment
  (
    p_inputstr IN VARCHAR2,
    p_userid   IN t5055_control_number.c5055_last_updated_by%TYPE,
    p_whtype   IN t5051_inv_warehouse.C901_WAREHOUSE_TYPE%TYPE,
    p_out_txn_id OUT VARCHAR2 )
AS
  v_strlen    NUMBER           := NVL (LENGTH (p_inputstr), 0);
  v_string    VARCHAR2 (30000) := p_inputstr;
  v_substring VARCHAR2 (1000);
  v_pnum t205_part_number.c205_part_number_id%TYPE;
  v_pick_locid    VARCHAR2 (20);
  v_put_locid     VARCHAR2 (20);
  v_loc_id        VARCHAR2 (20);
  v_type          NUMBER;
  v_qty           VARCHAR2 (20);
  v_count         NUMBER := 0;
  v_lcnqtymsgstr  CLOB;
  v_out_trans_id  VARCHAR2 (50);
  v_sugg_qty      NUMBER := 0;
  v_out_trans_ids VARCHAR2 (4000);
  v_pick_loc_qty t5053_location_part_mapping.c5053_curr_qty%TYPE;
  v_lcn_cnt  NUMBER;
  v_rplntype NUMBER;
  v_whtype   NUMBER;
BEGIN
  SELECT DECODE(p_whtype,56001, 56010,93341) INTO v_rplntype FROM DUAL;
  
  WHILE INSTR (v_string, '|') <> 0
  LOOP
    v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
    -- contains P#,loc, qty , type -      pic
    v_string     := SUBSTR (v_string, INSTR (v_string, '|') + 1);
    v_pnum       := NULL;
    v_pick_locid := NULL;
    v_put_locid  := NULL;
    v_qty        := NULL;
    -- Part Number , Pick Location , Put Location , v_qty
    v_pnum         := SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
    v_substring    := SUBSTR (v_substring, INSTR (v_substring, ',')    + 1);
    v_put_locid    := SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
    v_substring    := SUBSTR (v_substring, INSTR (v_substring, ',')    + 1);
    v_pick_locid   := SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
    v_substring    := SUBSTR (v_substring, INSTR (v_substring, ',')    + 1);
    v_qty          := v_substring;
    v_lcn_cnt      := gm_pkg_op_inv_location_module.get_open_txn_count (v_pnum, v_put_locid, p_whtype );
    v_pick_loc_qty := NVL (gm_pkg_op_item_control_rpt.get_part_location_cur_qty (v_pnum, v_pick_locid, p_whtype ), 0 ); -- To
    -- get the actual Current qty
    IF ( v_pick_loc_qty > 0 AND (v_pick_loc_qty - TO_NUMBER (v_qty) >= 0) AND (v_lcn_cnt <= 0) ) THEN
      --T412,t413 Entries
      v_lcnqtymsgstr := v_put_locid || ',' || v_pnum || ',' || v_qty || ',' || '' || ',' || get_part_price (v_pnum, 'E') || '|';
      gm_pkg_op_item_control_txn.gm_sav_inhouse_txn (v_lcnqtymsgstr, p_userid, v_rplntype, v_out_trans_id );
      v_out_trans_ids := v_out_trans_ids || ',' || v_out_trans_id;
      --T5050  Entries for Device
      gm_pkg_allocation.gm_ins_invpick_assign_detail (v_rplntype, v_out_trans_id, p_userid );
      --T5055  Entries
      IF (v_rplntype=93341) THEN
        v_whtype   := 90800;
      END IF;
      IF (v_rplntype=56010) THEN
        v_whtype   := 56001;
      END IF;
      v_lcnqtymsgstr := v_pnum || ',' || v_qty || ',,' || v_pick_locid || ','|| v_whtype || '|';
      gm_pkg_op_item_control_txn.gm_sav_item_control (v_out_trans_id, v_rplntype, v_lcnqtymsgstr, 93342, p_userid );
      gm_pkg_op_item_control_txn.gm_sav_item_control (v_out_trans_id, v_rplntype, v_lcnqtymsgstr, 93343, p_userid );
      v_lcnqtymsgstr := v_pnum || ',' || v_qty || ',,' || v_put_locid || ','|| v_whtype || '|';
      gm_pkg_op_item_control_txn.gm_sav_item_control (v_out_trans_id, v_rplntype, v_lcnqtymsgstr, 93344, p_userid );
      p_out_txn_id := v_out_trans_ids;
    END IF;
  END LOOP;
END gm_sav_init_replenishment;

/**************************************************************************************************
	 * Description : Procedure to save the Initiate Group Replenishments.
	 * Author	 : Elango
**************************************************************************************************/
	PROCEDURE gm_sav_init_grp_replenishment (
	   p_inputstr	IN		VARCHAR2
	 , p_userid	 	IN		t5055_control_number.c5055_last_updated_by%TYPE
	 , p_whtype   	IN 		t5051_inv_warehouse.C901_WAREHOUSE_TYPE%TYPE
	 , p_out_txn_id OUT		VARCHAR2
	)
	AS
		v_strlen	 NUMBER := NVL (LENGTH (p_inputstr), 0);
		v_string	 VARCHAR2(4000) := p_inputstr;
		v_substring  VARCHAR2 (1000);
		v_pnum		 t205_part_number.c205_part_number_id%TYPE;
		v_pick_locid VARCHAR2 (20);
		v_put_locid  VARCHAR2 (20);
		v_qty		 VARCHAR2 (20);
		v_count 	 VARCHAR2 (20);
		v_lcnqtymsgstr      VARCHAR2 (2000);
		v_lcnqtymsgstr_pick  CLOB;
		v_lcnqtymsgstr_put   CLOB;
		v_out_trans_id  VARCHAR2 (1000);
		v_out_trans_ids VARCHAR2 (4000);
    	v_pick_loc_qty  t5053_location_part_mapping.c5053_curr_qty%TYPE;
		v_message	 VARCHAR2 (1000);
		v_txn_key    VARCHAR2 (1000);
		v_temp_txn_key  VARCHAR2 (2000);
		v_txn_string  VARCHAR2(4000);		
		v_txn_substring VARCHAR2(4000);
		v_put_loccd    VARCHAR2 (20);		
		v_location   VARCHAR2 (4000);
		v_put_locs    VARCHAR2 (1000);
		v_put_loc   VARCHAR2 (20);	
		
		TYPE v_trans_id_array IS TABLE OF my_temp_key_value.my_temp_txn_id%TYPE;
		TYPE v_location_array IS TABLE OF my_temp_key_value.my_temp_txn_key%TYPE;
		TYPE v_string_array IS TABLE OF my_temp_key_value.my_temp_txn_value%TYPE;
		
		trans_id_array  v_trans_id_array;
		location_array  v_location_array;
		string_array    v_string_array;
		v_rplntype NUMBER;
		v_whtype NUMBER;
	BEGIN
		SELECT DECODE(p_whtype,56001, 56010,93341) INTO v_rplntype FROM DUAL;
		WHILE INSTR (v_string, '|') <> 0
		LOOP
			v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1); -- contains P#,loc, qty , type -				 pic
			v_txn_substring := v_substring;
			v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
			v_pnum		:= NULL;
			v_pick_locid := NULL;
			v_put_locid := NULL;
			v_qty		:= NULL;
			v_txn_key	:= NULL;
			v_temp_txn_key := NULL;
			v_count	    := NULL;
			v_out_trans_id	:= NULL;			
			-- Part Number , Put Location , Pick Location , v_qty
			v_pnum		:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_put_locid := SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_pick_locid := SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_qty		:= v_substring;
			v_txn_key := ',' ||v_pnum || ',';
		 		
			v_count := chk_part_in_temp(v_pnum);
			 IF v_count IS NOT NULL 
       		 THEN
		 		UPDATE  my_temp_key_value
		 		   SET  my_temp_txn_value = my_temp_txn_value || v_txn_substring || '|',
		 			    MY_TEMP_TXN_KEY = MY_TEMP_TXN_KEY || v_txn_key || ','
		 		 WHERE  my_temp_txn_id = v_count; 
       		 ELSE
       		 -- Hardcoded value is removed for BUG-3634,due to this 93341[FG Replenishment], its creating FGXX transactions instead of RWXX for RW Warehouse.
       		 	SELECT get_next_consign_id (v_rplntype) INTO v_out_trans_id FROM DUAL;
       		 	
       		 	v_txn_string  := v_txn_substring || '|';
       		  	INSERT INTO my_temp_key_value(
       		  			  my_temp_txn_id,
       		  			  my_temp_txn_key,
       		  			  my_temp_txn_value
       		  	)VALUES(
			        	  v_out_trans_id,
       		  			  v_txn_key,
			        	  v_txn_string
		        	);
		     	v_out_trans_ids := v_out_trans_ids || ',' ||v_out_trans_id ;
		        p_out_txn_id := v_out_trans_ids ;		        
			 END IF;				
		END LOOP;	
	 	
		SELECT 	my_temp_txn_id, my_temp_txn_key, my_temp_txn_value 
				BULK COLLECT 
		  INTO  trans_id_array,location_array,string_array 
		  FROM  my_temp_key_value;
	 
		FOR i IN trans_id_array.FIRST .. trans_id_array.LAST 
		LOOP
		
		v_out_trans_id  := trans_id_array(i);
  		v_location      := location_array(i);
		v_txn_string    := string_array(i);
		v_lcnqtymsgstr		:= NULL;
		v_lcnqtymsgstr_pick	:= NULL;
		v_lcnqtymsgstr_put	:= NULL;
		v_put_locs := NULL;
		
		v_put_loc  := SUBSTR (v_location, 1, INSTR (v_location, ',') - 1);
		WHILE INSTR (v_txn_string, '|') <> 0
		LOOP
			v_substring := SUBSTR (v_txn_string, 1, INSTR (v_txn_string, '|') - 1); -- contains P#,loc, qty , type -				 pic
			v_txn_string	:= SUBSTR (v_txn_string, INSTR (v_txn_string, '|') + 1);
			v_pnum		:= NULL;
			v_pick_locid := NULL;
			v_put_locid := NULL;
			v_qty		:= NULL;
			v_put_loccd := NULL;
			-- Part Number ,Put Location , Pick Location , v_qty
			v_pnum		:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_put_locid := SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_pick_locid := SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_qty		:= v_substring;
		    v_pick_loc_qty := NVL (gm_pkg_op_item_control_rpt.get_part_location_cur_qty (v_pnum, v_pick_locid), 0) ; -- To get the actual Current qty
			
		    IF (v_rplntype=93341) THEN
		        v_whtype   := 90800;
		    END IF;
		    IF (v_rplntype=56010) THEN
		        v_whtype   := 56001;
		    END IF;
		    
	   		IF (v_pick_loc_qty > 0 AND (v_pick_loc_qty - TO_NUMBER (v_qty) >= 0)) 
			THEN  
				 v_lcnqtymsgstr := v_lcnqtymsgstr || v_pnum || ',' || v_qty || ',' || '' || ',' || get_part_price (v_pnum, 'E') || '|';	
				 v_lcnqtymsgstr_pick := v_lcnqtymsgstr_pick || v_pnum || ',' || v_qty || ',,' || v_pick_locid || ','|| v_whtype || '|';		
				 v_lcnqtymsgstr_put := v_lcnqtymsgstr_put || v_pnum || ',' || v_qty || ',,' || v_put_locid || ','|| v_whtype || '|';
			END IF;
				
			--The following section is for fetching the loc code for comments section 	
	     	--The c412_comments will be  "Stock Transfer Initiated for Location : put_loccd,put_loccd,put_loccd   	 
		    BEGIN
			      SELECT c5052_location_cd INTO v_put_loccd
			      FROM t5052_location_master 
			      WHERE c5052_location_id = v_put_locid 
			      AND c5052_void_fl IS NULL;
			EXCEPTION WHEN NO_DATA_FOUND THEN
			      v_put_loccd := '';
			END;
			
		    v_put_locs :=	 v_put_locs || v_put_loccd ||',' ;		    
		  END LOOP;
		  --T412,t413 Entries	
		  gm_save_inhouse_item_consign(v_out_trans_id
										, v_rplntype -- Hardcoded value is removed for BUG-3634,due to this 93341[FG Replenishment], its creating FGXX transactions instead of RWXX for RW Warehouse.
										, '93351'	--Bulk to shelf (purpose/reason)
										, NULL	 --shipto
										, NULL	 --shipto id
										, get_rule_value('LOCMSG','STKTRNS') || v_put_locs
										, p_userid
										, v_lcnqtymsgstr
										, v_message
										 );	
			--T5050 Entries for Device
			gm_pkg_allocation.gm_ins_invpick_assign_detail (93341, v_out_trans_id, p_userid);
			--T5055 Entries	
			gm_pkg_op_item_control_txn.gm_sav_item_control (v_out_trans_id, 93341, v_lcnqtymsgstr_pick, 93342, p_userid);
			gm_pkg_op_item_control_txn.gm_sav_item_control (v_out_trans_id, 93341, v_lcnqtymsgstr_pick, 93343, p_userid) ;
			gm_pkg_op_item_control_txn.gm_sav_item_control (v_out_trans_id, 93341, v_lcnqtymsgstr_put, 93344, p_userid);			
		END LOOP;
		DELETE FROM my_temp_key_value;
	END gm_sav_init_grp_replenishment;
/****************************************************************
* Description : Procedure to update item order table with control number
* author   : Xun
****************************************************************/
PROCEDURE gm_upt_item_order
  (
    p_trans_id   IN t412_inhouse_transactions.c412_inhouse_trans_id%TYPE,
    p_trans_type IN t412_inhouse_transactions.c412_type%TYPE,
    p_loc_oper   IN t5055_control_number.c901_type%TYPE )
AS
  v_temp_qty         NUMBER;
  v_sum_5055         NUMBER;
  v_502_avaiable_qty NUMBER;
  v_502_used_qty     NUMBER;
  v_sum_before       NUMBER := 0;
  v_sum_after        NUMBER := 0;
  v_out_item_ord_id t502_item_order.c502_item_order_id%TYPE;
  CURSOR cur_temp
  IS
    SELECT c501_order_id orderid,
           c205_part_number_id pnum,
      	   c901_type ordertype,
      	   c502_item_qty qty,
      	   c502_item_price price,
      	   c502_construct_fl cons_fl,
      	   c502_ref_id ref_id,
      	   c502_attribute_id attr_id,
      	   c502_vat_rate vat_rate,
      	   c502_list_price,
      	   c502_unit_price,
      	   c901_unit_price_adj_code,
      	   c502_unit_price_adj_value,
      	   c502_discount_offered,
      	   c901_discount_type,
      	   c502_system_cal_unit_price,
      	   C502_DO_UNIT_PRICE,
      	   C502_ADJ_CODE_VALUE,
      	   c500_order_info_id order_info_id,
      	   c502_ext_item_price ext_item_price,
      	   c502_tax_amt tax_amount,
      	   c502_igst_rate igst_rate,
      	   c502_cgst_rate cgst_rate,
      	   c502_sgst_rate sgst_rate,
           c502_hsn_code hsncode,
           c7200_rebate_rate rebate_rate,
           c502_rebate_process_fl rebate_process_fl,
           c502_tcs_rate tcs_rate
      FROM my_temp_t520_item_order
  ORDER BY c205_part_number_id;
  
  CURSOR cur_t5055 ( v_partnumber IN t5055_control_number.c205_part_number_id%TYPE )                                IS
    SELECT c205_part_number_id pnum,
      c5055_control_number contrlnum,
      SUM (c5055_qty) qty
    FROM t5055_control_number
    WHERE c5055_ref_id      = p_trans_id
    AND c901_ref_type       = p_trans_type
    AND c901_type           = p_loc_oper
    AND c5055_void_fl      IS NULL
    AND c205_part_number_id = v_partnumber
    GROUP BY c205_part_number_id,
      c5055_control_number
    ORDER BY c205_part_number_id,
      c5055_control_number;
BEGIN
  --calculate the total price before new records with control number insert into t502
  SELECT SUM (c502_item_qty * c502_item_price)
  INTO v_sum_before
  FROM t502_item_order
  WHERE c501_order_id = p_trans_id
  AND c502_void_fl   IS NULL;
  
  DELETE FROM my_temp_t520_item_order;
  --put part number with price info in the order into temp table
 INSERT
 INTO my_temp_t520_item_order
 (c502_item_order_id,
		c501_order_id,
		c205_part_number_id,
		c502_item_qty,
		c502_control_number,
		c502_item_price,
		c502_void_fl,
		c502_delete_fl,
		c502_item_seq_no,
		c901_type,
		c502_construct_fl,
		c502_ref_id,
		c502_vat_rate,
		c502_attribute_id,
		c502_list_price,
		c502_unit_price,
		c901_unit_price_adj_code,
		c502_unit_price_adj_value,
		c901_discount_type,
		c502_system_cal_unit_price,
		c502_discount_offered,
		c502_do_unit_price,
		c502_adj_code_value,
		c500_order_info_id,
		c502_ext_item_price,
		c502_tax_amt,
		c502_igst_rate,
		c502_cgst_rate,
		c502_sgst_rate,
		c502_hsn_code,
		c7200_rebate_rate,
		c502_rebate_process_fl,
		c502_tcs_rate)
 SELECT c502_item_order_id,
	    c501_order_id,
	    c205_part_number_id,
	    c502_item_qty,
	    c502_control_number,
	    c502_item_price,
	    c502_void_fl,
	    c502_delete_fl,
	    c502_item_seq_no,
	    c901_type,
	    c502_construct_fl,
	    c502_ref_id,
	    c502_vat_rate,
	    c502_attribute_id,
	    c502_list_price,
	    c502_unit_price,
	    c901_unit_price_adj_code,
	    c502_unit_price_adj_value,
	    c901_discount_type,
        c502_system_cal_unit_price,
	    c502_discount_offered,
	    C502_DO_UNIT_PRICE,
      	C502_ADJ_CODE_VALUE,
      	c500_order_info_id,
      	c502_ext_item_price,
   -- to divide the tax amount/item qty
      	(c502_tax_amt / c502_item_qty),
      	c502_igst_rate,
      	c502_cgst_rate,
      	c502_sgst_rate,
      	c502_hsn_code, 
      	-- PMT-28394 (Added the column for Rebate Management)
		c7200_rebate_rate,
        c502_rebate_process_fl,
        c502_tcs_rate
  FROM t502_item_order
  WHERE c501_order_id         = p_trans_id
  AND c502_void_fl           IS NULL
  AND NVL (c901_type, 99999) != 50301;
  
  DELETE t502_item_order
  WHERE c501_order_id = p_trans_id
  AND NVL (c901_type, 9999) != 50301;
  
  -- here's two for loops associate with two cursors, outer loop is cusor for the parts with price in the temp table
  -- inner loop is the cursor for the part with control number in t5055
  FOR var_temp IN cur_temp
  LOOP
    v_temp_qty    := var_temp.qty;
    FOR var_t5055 IN cur_t5055 (var_temp.pnum)
    LOOP
      -- If less than zero then move to next for loop
      IF (v_temp_qty <= 0) THEN
        EXIT;
      END IF;
      -- fetch all the qty for the part from first loop ( temp table)
      v_sum_5055 := var_t5055.qty;
      --calculate the all qty (by order id, pnum, controlnum)which alredy been inserted into t502 for the part
      -- from first loop
      BEGIN
        SELECT SUM (c502_item_qty)
        INTO v_502_used_qty
        FROM t502_item_order
        WHERE c501_order_id     = var_temp.orderid
        AND c205_part_number_id = var_t5055.pnum
        AND c502_control_number = var_t5055.contrlnum
        AND C502_VOID_FL IS NULL 
        GROUP BY c205_part_number_id;
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
        v_502_used_qty := 0;
      END;
      -- calaulate how many parts avail to be inserted into t502
      v_502_avaiable_qty := v_sum_5055 - v_502_used_qty;
      -- if not available then move to next cursor
      IF (v_502_avaiable_qty > 0) THEN
        --compare the part qty from first loop with available qty of the part
        IF (v_temp_qty >= v_502_avaiable_qty) THEN
        ----adding extra columns to pass the gst percentage and hsn code for Chennai/Delhi  
        gm_pkg_op_do_master.gm_sav_order_item_master (NULL, var_temp.orderid, var_t5055.pnum, v_502_avaiable_qty, var_t5055.contrlnum, var_temp.price,
        											  NULL, var_temp.ordertype, var_temp.cons_fl, var_temp.ref_id, var_temp.vat_rate, 
        											  var_temp.attr_id, var_temp.order_info_id, (var_temp.tax_amount * v_502_avaiable_qty),v_out_item_ord_id,
        											  var_temp.ext_item_price,var_temp.igst_rate,var_temp.cgst_rate,var_temp.sgst_rate,var_temp.hsncode,
        											  var_temp.rebate_rate, var_temp.rebate_process_fl, var_temp.tcs_rate);
        
          v_temp_qty := v_temp_qty - v_502_avaiable_qty;
        ELSE
          gm_pkg_op_do_master.gm_sav_order_item_master (NULL, var_temp.orderid, var_t5055.pnum, v_temp_qty, var_t5055.contrlnum, var_temp.price, NULL, var_temp.ordertype,
          												var_temp.cons_fl, var_temp.ref_id, var_temp.vat_rate, var_temp.attr_id, var_temp.order_info_id, (var_temp.tax_amount * v_temp_qty),
          												v_out_item_ord_id,var_temp.ext_item_price,var_temp.igst_rate,var_temp.cgst_rate,var_temp.sgst_rate,var_temp.hsncode,
        											    var_temp.rebate_rate, var_temp.rebate_process_fl, var_temp.tcs_rate);
          v_temp_qty := 0;
        END IF;
        
        gm_pkg_sm_adj_txn.gm_sav_ord_adj_details(
	        											v_out_item_ord_id
	        										  , var_temp.c502_list_price
	        										  , var_temp.c502_unit_price
	        										  , var_temp.c901_unit_price_adj_code
	        										  , var_temp.c502_unit_price_adj_value
	        										  , var_temp.c502_discount_offered
	        										  , var_temp.c901_discount_type
	        										  , var_temp.c502_system_cal_unit_price
	        										  , var_temp.C502_DO_UNIT_PRICE
	        										  , var_temp.C502_ADJ_CODE_VALUE
        										  );
        -- End of avability check
      END IF;
    END LOOP;
    -- End of Mail For PartNumber For loop
  END LOOP;
  --need to update control number to 'NOC#' if order is 'Loaner'
  UPDATE t502_item_order
  SET c502_control_number    = 'NOC#'
  WHERE c501_order_id        = p_trans_id
  AND c502_void_fl          IS NULL
  AND NVL (c901_type, 99999) = 50301;
  --Whenever the detail is updated, the Order table has to be updated ,so ETL can Sync it to GOP.
  UPDATE t501_order
  SET c501_last_updated_date = CURRENT_DATE
  WHERE c501_order_id        = p_trans_id;
  --- calculate the total price after new records with control number insert into t502
  SELECT SUM (c502_item_qty * c502_item_price)
  INTO v_sum_after
  FROM t502_item_order
  WHERE c501_order_id = p_trans_id
  AND c502_void_fl   IS NULL;
  -- GROUP BY c205_part_number_id;
  --- total price before and after insertion should match, otherwise throw error.
  IF (v_sum_before != v_sum_after) THEN
    raise_application_error (-20922, '');
  END IF;
END gm_upt_item_order;
/****************************************************************
* Description : Procedure to update item order table with control number
* author   : Xun
****************************************************************/
PROCEDURE gm_sav_item_order
  (
    p_order_id       IN t502_item_order.c501_order_id%TYPE,
    p_pnum           IN t502_item_order.c205_part_number_id%TYPE,
    p_qty            IN t502_item_order.c502_item_qty%TYPE,
    p_control_number IN t502_item_order.c502_control_number%TYPE,
    p_item_price     IN t502_item_order.c502_item_price%TYPE,
    p_order_type     IN t502_item_order.c901_type%TYPE,
    p_cons_fl        IN t502_item_order.c901_type%TYPE,
    p_ref_id         IN t502_item_order.c901_type%TYPE )
AS
BEGIN
  INSERT
  INTO t502_item_order
    (
      c502_item_order_id,
      c501_order_id,
      c205_part_number_id,
      c502_item_qty,
      c502_control_number,
      c502_item_price,
      c901_type,
      c502_construct_fl,
      c502_ref_id
    )
    VALUES
    (
      s502_item_order.NEXTVAL,
      p_order_id,
      p_pnum,
      p_qty,
      p_control_number,
      p_item_price,
      p_order_type,
      p_cons_fl,
      p_ref_id
    );
END gm_sav_item_order;
/************************************************************************
* Description : Procedure to validate if the txn already been verified
* author  : Xun
***********************************************************************/
PROCEDURE gm_validate_txn_verify
  (
    p_trans_id   IN t412_inhouse_transactions.c412_inhouse_trans_id%TYPE,
    p_trans_type IN t412_inhouse_transactions.c412_type%TYPE
  )
AS
  v_table t906_rules.c906_rule_value%TYPE;
  v_status_flag t412_inhouse_transactions.c412_status_fl%TYPE;
  v_void_fl t412_inhouse_transactions.c412_void_fl%TYPE;
  v_comp_id		t1900_company.c1900_company_id%TYPE;
  v_trans_company_id		t1900_company.c1900_company_id%TYPE;
  v_plant_id    t5040_plant_master.c5040_plant_id%TYPE;
BEGIN
  BEGIN
    SELECT  get_plantid_frm_cntx(),	get_compid_frm_cntx()
    	  INTO  v_plant_id,v_comp_id  FROM DUAL;    

    SELECT get_rule_value ('TRANS_TABLE', p_trans_type) INTO v_table FROM DUAL;
    IF v_table = 'CONSIGNMENT' THEN
      SELECT c504_status_fl,
        c504_void_fl,
        c1900_company_id
      INTO v_status_flag,
        v_void_fl,
        v_trans_company_id
      FROM t504_consignment
      WHERE c504_consignment_id = p_trans_id AND c5040_plant_id= v_plant_id FOR UPDATE;
    ELSIF v_table               = 'IN_HOUSE' THEN
      SELECT c412_status_fl,
        c412_void_fl,
        c1900_company_id
      INTO v_status_flag,
        v_void_fl,
        v_trans_company_id
      FROM t412_inhouse_transactions t412
      WHERE t412.c412_inhouse_trans_id = p_trans_id AND t412.c5040_plant_id= v_plant_id FOR UPDATE;
    END IF;
  -- setting transaction company id into context incase of mismatch  
     IF (v_trans_company_id IS NOT NULL) AND (v_comp_id != v_trans_company_id) THEN
          gm_pkg_cor_client_context.gm_sav_client_context(v_trans_company_id,v_plant_id);
       END IF;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    v_status_flag := NULL;
    v_void_fl     := NULL;
  END;
  IF v_void_fl IS NOT NULL THEN
    GM_RAISE_APPLICATION_ERROR('-20999','228',p_trans_id);
  END IF;
  IF NVL(v_status_flag,0) >= '4' THEN
    raise_application_error ('-20921', '');
  END IF;
END gm_validate_txn_verify;
/************************************************************************
* Description : Procedure to save the shipping record if CN status is Pending return
* author  : Gopinathan
***********************************************************************/
PROCEDURE gm_sav_loaner_ship
  (
    p_trans_id IN t412_inhouse_transactions.c412_inhouse_trans_id%TYPE,
    p_status   IN t504a_consignment_loaner.c504a_status_fl%TYPE,
    p_userid   IN t101_user.c101_user_id%TYPE )
AS
  --
  v_type t412_inhouse_transactions.c412_type%TYPE;
  v_cn_id t412_inhouse_transactions.c412_ref_id%TYPE;
  v_rule_status VARCHAR2 (10);
  v_lnsetstatus VARCHAR2 (10);
  v_shipping_id VARCHAR2 (20);
  v_ship_to t907_shipping_info.c901_ship_to%TYPE;
  v_ship_to_id t907_shipping_info.c907_ship_to_id%TYPE;
  v_ship_carr t907_shipping_info.c901_delivery_carrier%TYPE;
  v_ship_mode t907_shipping_info.c901_delivery_mode%TYPE;
  --
BEGIN
  SELECT c412_type,
    c412_ref_id
  INTO v_type,
    v_cn_id
  FROM t412_inhouse_transactions
  WHERE c412_inhouse_trans_id = p_trans_id;
  
  v_rule_status := get_rule_value ('REF_UPD_AVOID_STATUS', v_type);
  IF p_status    = v_rule_status THEN
    BEGIN
      /*fetch the last shipping detail for the loaner CN*/
      SELECT ship_to,
        ship_to_id,
        ship_carr,
        ship_mode
      INTO v_ship_to,
        v_ship_to_id,
        v_ship_carr,
        v_ship_mode
      FROM
        (SELECT c901_ship_to ship_to,
          c907_ship_to_id ship_to_id,
          c901_delivery_carrier ship_carr,
          c901_delivery_mode ship_mode
        FROM t907_shipping_info
        WHERE c907_ref_id  = v_cn_id
        AND c907_status_fl = '40'
        ORDER BY c907_created_date DESC
        )
      WHERE ROWNUM = 1;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
      v_ship_to    := NULL;
      v_ship_to_id := NULL;
    END;
    /*Create a Shipping Record In this case , we are creating a shipping record when the LN Status is â€œPENDING
    RETURNâ€� .*/
    gm_pkg_cm_shipping_trans.gm_sav_shipping (p_trans_id, 50183, v_ship_to, v_ship_to_id, v_ship_carr, v_ship_mode, NULL, NULL, NULL, p_userid, v_shipping_id );
    /*Update the Status (Release) of the Shipping record.*/
    gm_pkg_cm_shipping_trans.gm_sav_release_shipping (p_trans_id, 50183, p_userid, '' );
    --Update CN status is pending return
    gm_pkg_op_loaner_set_txn.gm_upd_consign_loaner_status(v_cn_id,p_status,p_userid);
  END IF;
END gm_sav_loaner_ship;
/************************************************************************
* Description : Procedure to update the t413 table records from t5055 table for the pending shipping to shelf
* author  : Gopinathan
***********************************************************************/
PROCEDURE gm_sav_psfg_detail
  (
    p_trans_id   IN t412_inhouse_transactions.c412_inhouse_trans_id%TYPE,
    p_trans_type IN t412_inhouse_transactions.c412_type%TYPE,
    p_loc_oper   IN t5055_control_number.c901_type%TYPE )
AS
  CURSOR cur_temp
  IS
    SELECT c412_inhouse_trans_id txnid,
      c205_part_number_id pnum,
      c413_control_number cnum,
      c413_item_qty qty,
      c413_item_price price,
      c413_status_fl status,
      c413_ref_id refid,
      c901_ref_type reftype
    FROM temp_t413_inhouse_trans_items
    ORDER BY c205_part_number_id,
      c413_control_number,
      c413_ref_id;
      
  CURSOR cur_t5055 ( v_partnumber IN t5055_control_number.c205_part_number_id%TYPE )
                                  IS
    SELECT c205_part_number_id pnum,
      c5055_control_number cnum,
      SUM (c5055_qty) qty
    FROM t5055_control_number
    WHERE c5055_ref_id       = p_trans_id
    AND c901_ref_type        = p_trans_type
    AND c901_type            = p_loc_oper
    AND c5055_void_fl       IS NULL
    AND c205_part_number_id  = v_partnumber
    AND c5055_void_fl       IS NULL
    AND c5055_update_inv_fl IS NULL
    GROUP BY c205_part_number_id,
      c5055_control_number
    ORDER BY c205_part_number_id,
      c5055_control_number;
  
  v_inhouse_item_id t413_inhouse_trans_items.c413_trans_id%TYPE;
  v_temp_qty         NUMBER;
  v_sum_5055         NUMBER;
  v_413_avaiable_qty NUMBER;
  v_413_used_qty     NUMBER;
  v_sum_before       NUMBER := 0;
  v_sum_after        NUMBER := 0;
BEGIN
  -- Validate control number and qty with Order records
  gm_validate_cnum_qty (p_trans_id);
  --calculate the total price before new records with control number insert into t413
  SELECT SUM (c413_item_qty * c413_item_price)
  INTO v_sum_before
  FROM t413_inhouse_trans_items
  WHERE c412_inhouse_trans_id = p_trans_id
  AND c413_void_fl           IS NULL;
  DELETE FROM temp_t413_inhouse_trans_items;
  --put part number with price info in the order into temp table
  -- below code has been changed due to new column added to T413 table.
  INSERT
  INTO temp_t413_inhouse_trans_items
    (
      c413_trans_id ,
      c413_control_number ,
      c413_item_qty ,
      c413_item_price ,
      c413_void_fl ,
      c205_part_number_id ,
      c412_inhouse_trans_id ,
      c413_status_fl ,
      c413_created_date ,
      c413_ref_id ,
      c901_ref_type ,
      c413_last_updated_by ,
      c413_last_updated_date ,
      c205_rec_part_number_id ,
      c901_rec_reason
    )
  SELECT c413_trans_id ,
    c413_control_number ,
    c413_item_qty ,
    c413_item_price ,
    c413_void_fl ,
    c205_part_number_id ,
    c412_inhouse_trans_id ,
    c413_status_fl ,
    c413_created_date ,
    c413_ref_id ,
    c901_ref_type ,
    c413_last_updated_by ,
    c413_last_updated_date ,
    c205_rec_part_number_id ,
    c901_rec_reason
  FROM t413_inhouse_trans_items
  WHERE c412_inhouse_trans_id = p_trans_id
  AND c413_void_fl           IS NULL;
  DELETE
  FROM t413_inhouse_trans_items
  WHERE c412_inhouse_trans_id = p_trans_id
  AND c413_void_fl           IS NULL;
  
  FOR var_temp IN cur_temp
  LOOP
    v_temp_qty    := var_temp.qty;
    FOR var_t5055 IN cur_t5055 (var_temp.pnum)
    LOOP
      -- If less than zero then move to next for loop
      IF (v_temp_qty <= 0) THEN
        EXIT;
      END IF;
      v_sum_5055 := var_t5055.qty;
      BEGIN
        SELECT SUM (c413_item_qty)
        INTO v_413_used_qty
        FROM t413_inhouse_trans_items
        WHERE c412_inhouse_trans_id = var_temp.txnid
        AND c205_part_number_id     = var_t5055.pnum
        AND c413_control_number     = var_t5055.cnum
        GROUP BY c205_part_number_id;
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
        v_413_used_qty := 0;
      END;
      -- calaulate how many parts avail to be inserted into t413
      v_413_avaiable_qty := v_sum_5055 - v_413_used_qty;
      -- if not available then move to next cursor
      IF (v_413_avaiable_qty > 0) THEN
        --compare the part qty from first loop with available qty of the part
        IF (v_temp_qty >= v_413_avaiable_qty) THEN
          gm_pkg_op_item_control_txn.gm_sav_inhouse_trans_items (var_temp.txnid, var_t5055.pnum, v_413_avaiable_qty, var_t5055.cnum, var_temp.price, var_temp.status, var_temp.refid, var_temp.reftype );
          v_temp_qty := v_temp_qty - v_413_avaiable_qty;
        ELSE
          gm_pkg_op_item_control_txn.gm_sav_inhouse_trans_items (var_temp.txnid, var_t5055.pnum, v_temp_qty, var_t5055.cnum, var_temp.price, var_temp.status, var_temp.refid, var_temp.reftype );
          v_temp_qty := 0;
        END IF;
      END IF;
    END LOOP;
  END LOOP;
  --- calculate the total price after new records with control number insert into t413
  SELECT SUM (c413_item_qty * c413_item_price)
  INTO v_sum_after
  FROM t413_inhouse_trans_items
  WHERE c412_inhouse_trans_id = p_trans_id
  AND c413_void_fl           IS NULL;
  --- total price before and after insertion should match, otherwise throw error.
  IF (v_sum_before != v_sum_after) THEN
    raise_application_error (-20922, '');
  END IF;
END gm_sav_psfg_detail;
/************************************************************************
* Description : Procedure to update the t413 table records from t5055 table for the pending shipping to shelf
* author  : Gopinathan
***********************************************************************/
PROCEDURE gm_sav_inhouse_trans_items
  (
    p_trans_id   IN t412_inhouse_transactions.c412_inhouse_trans_id%TYPE,
    p_pnum       IN t413_inhouse_trans_items.c205_part_number_id%TYPE,
    p_qty        IN t413_inhouse_trans_items.c413_item_qty%TYPE,
    p_cnum       IN t413_inhouse_trans_items.c413_control_number%TYPE,
    p_item_price IN t413_inhouse_trans_items.c413_item_price%TYPE,
    p_status_fl  IN t413_inhouse_trans_items.c413_status_fl%TYPE,
    p_ref_id	 IN	t413_inhouse_trans_items.c413_ref_id%TYPE DEFAULT NULL,
    p_ref_type	 IN t413_inhouse_trans_items.c901_ref_type%TYPE DEFAULT NULL)
AS
BEGIN
  INSERT
  INTO t413_inhouse_trans_items
    (
      c413_trans_id,
      c413_control_number,
      c413_item_qty,
      c413_item_price,
      c205_part_number_id,
      c412_inhouse_trans_id,
      c413_status_fl,
      c413_created_date,
      c413_ref_id,
      c901_ref_type
    )
    VALUES
    (
      s413_inhouse_item.NEXTVAL,
      p_cnum,
      p_qty,
      p_item_price,
      p_pnum,
      p_trans_id,
      p_status_fl,
      CURRENT_DATE,
      p_ref_id,
      p_ref_type
    );
END gm_sav_inhouse_trans_items;
/************************************************************************
* Description : Procedure to update the t413 table records from t5055 table for the pending shipping to shelf
* author  : Gopinathan
***********************************************************************/
PROCEDURE gm_validate_cnum_qty
  (
    p_trans_id IN t412_inhouse_transactions.c412_inhouse_trans_id%TYPE
  )
AS
  CURSOR cur_t5055
  IS
    SELECT c205_part_number_id pnum,
      c5055_control_number cnum,
      SUM (c5055_qty) qty
    FROM t5055_control_number
    WHERE c5055_ref_id       = p_trans_id
    AND c5055_void_fl       IS NULL
    AND c5055_update_inv_fl IS NULL
    GROUP BY c205_part_number_id,
      c5055_control_number
    ORDER BY c205_part_number_id,
      c5055_control_number;
  
  v_ord_id t412_inhouse_transactions.c412_ref_id%TYPE;
  v_org_qty t502_item_order.c502_item_qty%TYPE;
BEGIN
  SELECT c412_ref_id
  INTO v_ord_id
  FROM t412_inhouse_transactions
  WHERE c412_inhouse_trans_id = p_trans_id
  AND c412_void_fl           IS NULL;
  
  FOR var_t5055 IN cur_t5055
  LOOP
    SELECT SUM (t502.c502_item_qty)
    INTO v_org_qty
    FROM t502_item_order t502
    WHERE t502.c501_order_id            = v_ord_id
    AND TRIM (t502.c502_control_number) = var_t5055.cnum
    AND t502.c205_part_number_id        = var_t5055.pnum
    AND t502.c502_void_fl              IS NULL;
    IF v_org_qty                       IS NULL OR v_org_qty < var_t5055.qty THEN
      raise_application_error ('-20796', '');
    END IF;
  END LOOP;
END gm_validate_cnum_qty;
/*
* Description: Validate the CN Location QTy
* Author: Rajkumar
*/
PROCEDURE gm_validate_cn_qty
  (
    p_trans_id IN t412_inhouse_transactions.c412_inhouse_trans_id%TYPE,
    p_reftype  IN t5055_control_number.c901_ref_type%TYPE,
    p_pnum     IN t505_item_consignment.c205_part_number_id%TYPE,
    p_whtype   IN t505_item_consignment.c901_warehouse_type%TYPE,
    p_pick_qty IN t505_item_consignment.C505_item_qty%TYPE )
AS
  v_avail_qty NUMBER :=0;
  v_alloc_qty NUMBER :=0;
  v_total_qty NUMBER :=0;
BEGIN
  SELECT DECODE(p_whtype,56001,GET_QTY_IN_INV(p_pnum,p_whtype),get_qty_in_stock(p_pnum))
  INTO v_avail_qty
  FROM DUAL;
  
  v_alloc_qty                                                           := gm_pkg_op_process_request.get_cn_txn_alloc(p_trans_id,p_reftype,p_pnum,p_whtype);
  v_total_qty                                                           := gm_pkg_op_process_request.get_total_warehouse_qty(p_pnum,p_whtype);
  IF (v_total_qty- v_avail_qty)+( p_pick_qty-(v_alloc_qty+ v_avail_qty)) > v_total_qty THEN
    GM_RAISE_APPLICATION_ERROR('-20999','226',p_whtype||'#$#'||p_pnum); 
  END IF;
END gm_validate_cn_qty;
/************************************************************************
* Description :
* author  :
***********************************************************************/
PROCEDURE gm_sav_fgbin_dtls
  (
    p_ref_id   IN t907c_fgbin_trans_detail.c907c_ref_id%TYPE,
    p_inputstr IN VARCHAR2,
    p_userid   IN t5055_control_number.c5055_last_updated_by%TYPE )
AS
  v_strlen    NUMBER           := NVL (LENGTH (p_inputstr), 0);
  v_string    VARCHAR2 (30000) := p_inputstr;
  v_substring VARCHAR2 (1000);
  v_bin_id    t907c_fgbin_trans_detail.c907c_bin_id%TYPE;
  v_refid     t907c_fgbin_trans_detail.c907c_ref_id%TYPE;
  v_ref_type  VARCHAR(20);
  v_fgbin_dtl_id	t907c_fgbin_trans_detail.c907c_fgbin_trans_detail_id%TYPE;
  v_inp_str	 VARCHAR2 (30000);
  v_plant_id t5040_plant_master.c5040_plant_id%TYPE;
BEGIN
  BEGIN
     SELECT  NVL(get_plantid_frm_cntx(),get_rule_value('DEFAULT_PLANT','ONEPORTAL')) INTO  v_plant_id FROM DUAL;
    
  -- Getting the source of the transaction to v_ref_type, say 50180: Order, 50181: Consignment, 50183:FGLE
    SELECT DECODE(c901_source,NULL,'',c901_source)
    INTO v_ref_type
    FROM t907_shipping_info
    WHERE c907_ref_id = p_ref_id
    AND c907_void_fl IS NULL AND c5040_plant_id= v_plant_id;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    v_ref_type:= '';
  END;
  
  --Inside the loop we will split each FG Bin id and insert/Update the record in the table
  WHILE INSTR (v_string, '|') <> 0
  LOOP
    v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
    v_string    := SUBSTR (v_string, INSTR (v_string, '|')    + 1);
    v_bin_id    := v_substring;
    -- v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
    IF v_ref_type IS NOT NULL THEN
    
    --Added the Update query to avoid duplicate entries
    	
    	UPDATE t907c_fgbin_trans_detail
    	SET c907c_ref_id = p_ref_id,
    		c901_ref_type = v_ref_type,
    		c907c_bin_id = v_bin_id,
    		c907c_last_updated_by = p_userid,
    		c907c_last_updated_date = CURRENT_DATE
    	WHERE c907c_ref_id = p_ref_id
	    AND c907c_bin_id = v_bin_id
	    AND c901_ref_type = v_ref_type
	    AND c907c_void_fl IS NULL;
    	
	    IF SQL%ROWCOUNT = 0
		THEN
	      INSERT
	      INTO t907c_fgbin_trans_detail
	        (
	          c907c_fgbin_trans_detail_id,
	          c907c_ref_id,
	          c901_ref_type,
	          c907c_bin_id,
	          c907c_created_by,
	          c907c_created_date
	        )
	        VALUES
	        (
	          S907C_FGBIN_TRANS_DETAIL_ID.NEXTVAL,
	          p_ref_id,
	          v_ref_type,
	          v_bin_id,
	          p_userid,
	          CURRENT_DATE
	        );
        END IF;
    END IF;
  END LOOP;
  
  -- To void the record when we remove FG Bin id of a transaction from control screen
  	v_inp_str := REPLACE(SUBSTR (p_inputstr, 1, v_strlen - 1),'|', ','); --To remove the last '|' and converting '|' to ','
   my_context.set_my_inlist_ctx(v_inp_str);
   
   	UPDATE t907c_fgbin_trans_detail 
	SET C907C_VOID_FL = 'Y', c907c_last_updated_by = p_userid, c907c_last_updated_date = CURRENT_DATE 
	WHERE c907c_bin_id NOT IN ( SELECT token FROM v_in_list)
	AND c901_ref_type = v_ref_type
	AND c907c_ref_id = p_ref_id
	AND c907c_void_fl IS NULL;
   
   
END gm_sav_fgbin_dtls;

/************************************************************************
* Description : Function to get the part number already exists for initiate transfer
* author  : Bala
***********************************************************************/
FUNCTION chk_part_in_temp (
		p_pnum		 IN VARCHAR2	 
	)
		RETURN VARCHAR2
IS
		v_id	 VARCHAR2 (20);
BEGIN
	BEGIN
		SELECT  my_temp_txn_id   INTO v_id
		  FROM 	my_temp_key_value
         WHERE 	my_temp_txn_id NOT IN
        	(SELECT	my_temp_txn_id
               FROM my_temp_key_value
              WHERE my_temp_txn_key LIKE '%,'||p_pnum ||',%' 
             )
           AND ROWNUM = 1;
      	RETURN v_id;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN NULL;	
		 END;
	END chk_part_in_temp; 	
	

	/********************************************************
	 * Description : Procedure to Void FG Bin ids corresponding to a transaction
	 * author	   : arajan
	 *******************************************************/
	PROCEDURE gm_void_FGBinId (
		p_txn_id	IN	 VARCHAR2
	  , p_user_id	IN	 t102_user_login.c102_user_login_id%TYPE
	)
	AS
	BEGIN
	
	--Inserting the values into a temporary table
	my_context.set_my_inlist_ctx(p_txn_id);
	
	--Update the void flag of all the transactions in temprary table
		UPDATE t907c_fgbin_trans_detail 
		SET C907C_VOID_FL = 'Y'
			, c907c_last_updated_by = p_user_id
			, c907c_last_updated_date = CURRENT_DATE 
		WHERE c907c_ref_id IN (SELECT token FROM v_in_list)
		AND c907c_void_fl IS NULL;

	END gm_void_FGBinId;

	/********************************************************
	 * Description : Procedure to Validate the Available FG Invntory Quantity
	 * author	   : Bala
	 *******************************************************/
	PROCEDURE gm_validate_fg_inv_qty(
		p_trans_id		IN	 t5062_Control_Number_Reserve.c5062_transaction_id%TYPE
	  , p_part_num		IN	 t5060_Control_Number_Inv.c205_part_number_id%TYPE 
	  , p_control_num 	IN   t5060_Control_Number_Inv.c5060_control_number%TYPE 
	  )
	  AS
		v_count    		NUMBER;
		v_inv_id   		t5060_Control_Number_Inv.c5060_control_number_inv_id%TYPE;
		v_plant_id      t5040_plant_master.c5040_plant_id%TYPE;
	  BEGIN
		 
		  SELECT  NVL(get_plantid_frm_cntx(),get_rule_value('DEFAULT_PLANT','ONEPORTAL'))
			INTO  v_plant_id FROM DUAL;
		  
		  BEGIN 
		  	SELECT t5060.c5060_control_number_inv_id
		   		INTO v_inv_id
	            FROM t5060_control_number_inv t5060
	           WHERE t5060.c5060_control_number = p_control_num
	             AND t5060.c901_warehouse_type = '90800' -- FG inventory
	             AND t5060.c205_part_number_id =  p_part_num
	             AND t5060.c5040_plant_id = v_plant_id
	             AND T5060.C5060_QTY > 0; 
		  EXCEPTION
	      WHEN NO_DATA_FOUND THEN
	        GM_RAISE_APPLICATION_ERROR('-20999','227',p_control_num||'#$#'||p_part_num);
	      END;  
		  
		  
		 SELECT count(1) 
           INTO v_count
           FROM t5062_control_number_reserve t5062 
               WHERE t5062.c5060_control_number_inv_id = v_inv_id
                 AND t5062.c5062_transaction_id   <>  p_trans_id
                 AND t5062.c5062_void_fl IS NULL
                 AND t5062.c5040_plant_id = v_plant_id
                 AND t5062.c901_status   IN ('103960','103961'); -- ( Open , In-Progress ); 
	   
	   IF v_count > 0 THEN
      		GM_RAISE_APPLICATION_ERROR('-20999','229',p_control_num||'#$#'||p_part_num);
       END IF;       
	END gm_validate_fg_inv_qty;
	
	/********************************************************
	 * Description : Procedure to Validate the Available Repack Invntory Quantity
	 * author	   : Bala
	 *******************************************************/
	PROCEDURE gm_validate_repack_inventory(
		p_trans_id		IN	 t5062_Control_Number_Reserve.c5062_transaction_id%TYPE
	  , p_part_num		IN	 t5060_Control_Number_Inv.c205_part_number_id%TYPE 
	  , p_control_num 	IN   t5060_Control_Number_Inv.c5060_control_number%TYPE 
	  )
	  AS
		v_avail_qty    NUMBER;
		v_contl_count  NUMBER;
	  BEGIN
		   SELECT COUNT(1) INTO v_contl_count 
		     FROM t5060_control_number_inv t5060                
            WHERE t5060.c5060_control_number = p_control_num
              AND t5060.c205_part_number_id = p_part_num                 
              AND t5060.c901_warehouse_type = '90815';  -- Repackage Qty
		  	   
	   IF v_contl_count = 0 THEN
      		GM_RAISE_APPLICATION_ERROR('-20999','230',p_control_num ||'#$#'||p_part_num);
       END IF;  
       
        SELECT sum(nvl(t5062.c5062_reserved_qty,0)) qty
           INTO v_avail_qty
           FROM t5062_control_number_reserve t5062
          , (SELECT t5060.c5060_control_number_inv_id
                FROM t5060_control_number_inv t5060
               WHERE t5060.c5060_control_number = p_control_num
                 AND t5060.c901_warehouse_type = '90815' -- Repack inventory
                 AND t5060.c205_part_number_id =  p_part_num) t5060
               WHERE t5060.c5060_control_number_inv_id = t5062.c5060_control_number_inv_id (+)
                 AND t5062.c5062_transaction_id (+) <>  p_trans_id
                 AND t5062.c5062_void_fl(+) IS NULL
                 AND t5062.c901_status(+) = '103961'; -- ( In Progress )
	   
	   IF v_avail_qty > 0 THEN
      		GM_RAISE_APPLICATION_ERROR('-20999','231',p_control_num||'#$#'||p_part_num);
       END IF;       
       
	END gm_validate_repack_inventory;
	
	/***************************************************************************************
	 * Description : Procedure to Save the lot number in t5055 table for validation purpose
	 * author	   : Arajan
	 ****************************************************************************************/
	PROCEDURE gm_sav_item_lot_num(
		p_refid    			IN 	t5055_control_number.c5055_ref_id%TYPE,
	    p_reftype  			IN 	t5055_control_number.c901_ref_type%TYPE,
	    p_inputstr 			IN 	CLOB,
	    p_loc_oper 			IN 	t5055_control_number.c901_type%TYPE,
	    p_userid   			IN 	t5055_control_number.c5055_last_updated_by%TYPE,
	    p_rel_verify_fl		IN	VARCHAR2, 
	    p_err_fl			OUT	VARCHAR2
	)
	AS
	  	v_string        CLOB := p_inputstr;
	  	v_substring     VARCHAR2 (1000);
	  	v_pnum          t5055_control_number.c205_part_number_id%TYPE;
	  	v_qty           t5055_control_number.c5055_qty%TYPE;
	  	v_control       t5055_control_number.c5055_control_number%TYPE;
	  	v_locid         t5055_control_number.c5052_location_id%TYPE;
	  	v_whtype        t5055_control_number.c901_warehouse_type%TYPE :='';
	  	v_temp_cntl     t5060_control_number_inv.c5060_control_number%TYPE;
	  	v_frm_part      t5055_control_number.c205_client_part_number_id%TYPE;
	  	v_comma_count	NUMBER;
	  	v_plant_id      t5040_plant_master.c5040_plant_id%TYPE;
	  	v_comp_id		t1900_company.c1900_company_id%TYPE;
	  	v_err_msg		VARCHAR2(2000) := '';
	  	v_err_fl		VARCHAR2(1) := 'N';
	  	v_flag			VARCHAR2(1) := 'N';
	  	v_status_flag	t501_order.c501_status_fl%TYPE := NULL;
	  	v_void_fl		t501_order.c501_void_fl%TYPE;
	  	v_ack_ref_id	t501_order.c501_order_id%TYPE;
	  	v5060_cnum		t5055_control_number.c5055_control_number%TYPE;
	  	v_part_inv		t5060_control_number_inv.c5060_control_number_inv_id%TYPE;
	  	v_comp_flag		VARCHAR2(1);
	BEGIN
		
		-- Check whether the transaction is already verified or not, if verified then it should not allow to submit again
		gm_pkg_op_item_control_txn.gm_validate_txn_verify (p_refid, p_reftype);
		
		SELECT  NVL(get_plantid_frm_cntx(),get_rule_value('DEFAULT_PLANT','ONEPORTAL'))
				, NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL'))
			INTO  v_plant_id, v_comp_id FROM DUAL;
		
		 SELECT get_rule_value(v_comp_id,'LOT_TRACK') INTO v_comp_flag FROM DUAL;
	   -- if the flag is getting as 'Y', we need to do the validaiton check for that company otherwise no
	   IF v_comp_flag IS NULL OR v_comp_flag <> 'Y' 
	   THEN
	   		RETURN;
	   END IF;
	   
		-- Get the status of transaction from Order table
		BEGIN
			SELECT c501_status_fl, c501_void_fl 
		    	INTO v_status_flag, v_void_fl
		    FROM t501_order
		    WHERE c501_order_id = p_refid;
		EXCEPTION
		WHEN NO_DATA_FOUND THEN
			v_status_flag:= NULL;
			v_void_fl := NULL;
		END;
		 	
	    IF v_status_flag IS NOT NULL AND v_status_flag   >= '2' THEN
	    -- If the transaction is already controlled show validaiton This transaction has been already released/verify, cannot be processed.
	      raise_application_error ('-20921', '');
	    END IF;
	    
	    IF v_void_fl IS NOT NULL THEN
	    	GM_RAISE_APPLICATION_ERROR('-20999','228',p_refid); 
	  	END IF;
	    
		-- Need to delete the record first before inserting
		DELETE
			  FROM t5055_control_number
			  WHERE c5055_ref_id = p_refid
			  AND c901_type      = p_loc_oper;
	  	
		-- Need to update the control number to null before validation
		UPDATE t413_inhouse_trans_items SET c413_control_number = '' WHERE c412_inhouse_trans_id = p_refid;
			  
	  	WHILE INSTR (v_string, '|') <> 0
	  	LOOP
		    v_substring  := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
		    v_string     := SUBSTR (v_string, INSTR (v_string, '|')    + 1);
		    --count how many commas in each substring
		    SELECT trim( LENGTH( v_substring ) ) - trim( LENGTH( TRANSLATE( v_substring, 'A,', 'A' ) ) )
		    INTO v_comma_count
			FROM dual;
		    v_pnum       := NULL;
		    v_qty        := NULL;
		    v_control    := NULL;
		    v_locid      := NULL;
		    v_pnum       := SUBSTR (v_substring, 1, INSTR (v_substring, ',')      - 1);
		    v_substring  := SUBSTR (v_substring, INSTR (v_substring, ',')         + 1);
		    v_qty        := SUBSTR (v_substring, 1, INSTR (v_substring, ',')      - 1);
		    v_substring  := SUBSTR (v_substring, INSTR (v_substring, ',')         + 1);
		    v_control    := TRIM(SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1));
		    v_substring  := SUBSTR (v_substring, INSTR (v_substring, ',')         + 1);
		    v_locid      := TRIM(SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1));
		    v_substring  := SUBSTR (v_substring, INSTR (v_substring, ',')         + 1);
		    --if comma count is 5 , it means includes temp_cntl
		    IF v_comma_count = 5
		    THEN
		    v_temp_cntl  := SUBSTR (v_substring, 1, INSTR (v_substring, ',') 	  - 1);
		    v_substring  := SUBSTR (v_substring, INSTR (v_substring, ',')         + 1);
		    END IF;
		    v_whtype	 := to_number(v_substring);
			
		    /*
		     * Get the part attribute type to validate the lot number from the part number setup screen
		     * If the attribute value is 'Y' validate the lot number for the part, otherwise skip the validation
		     * 104480: Lot tracking required ?
		     */
		     v_flag := get_part_attr_value_by_comp(v_pnum,'104480',v_comp_id);

		     -- If the attribute is not set for the part, then should not validat the part
		     IF v_flag IS NULL OR v_flag <> 'Y' THEN
		     	CONTINUE;
		     END IF;
		    
		    -- Validate the lot numbers 
		    
			gm_pkg_op_lot_track.gm_validate_input_data(p_reftype,p_refid,p_loc_oper,v_whtype,v_pnum,v_control,v_qty, v_err_msg);
			
			-- Set the flag to 'Y', if any error message is there to return the value 
			IF v_err_msg IS NOT NULL OR v_err_msg != '' THEN
				v_err_fl := 'Y';
			END IF;
			
			-- CODE ADDED FOR BBA-171 and these changes only for Acknowledgement Order Type
			-- If the lot number is already reserved, and the lot number in control number screen does not match the reserved one, then void the reserved lot num
		    BEGIN
		    SELECT c501_ref_id INTO v_ack_ref_id 
			  FROM t501_order 
			 WHERE c501_order_id = p_refid 
			   AND c501_void_fl IS NULL ;
		    EXCEPTION
		      WHEN NO_DATA_FOUND THEN
		        v_ack_ref_id := null;
		      END;
			IF v_ack_ref_id IS NOT NULL THEN	  	   	
			  	IF v_temp_cntl IS NOT NULL THEN
				   BEGIN
				  	SELECT t5060.c5060_control_number
					     , t5060.c5060_control_number_inv_id 
					  INTO v5060_cnum , v_part_inv
				  	  FROM t5060_control_number_inv t5060
				  	 WHERE
				  	 t5060.C5060_qty>0 --System should check only the Qty which is greater than 0 PMT-46800
				  	 AND t5060.c5060_control_number_inv_id IN(
				  	            SELECT t5062.c5060_control_number_inv_id
				  	              FROM  t5062_control_number_reserve t5062
				  	              WHERE t5060.c205_part_number_id = v_pnum
				  	              AND t5062.c5062_void_fl IS NULL -- Added void flag check PMT-46880
				  	              AND t5060.c5060_control_number = v_temp_cntl);
					 EXCEPTION WHEN NO_DATA_FOUND THEN
					  v5060_cnum := '-999';
					  v_part_inv := '-999';
					 END;	
			  		-- if the control number from pending control screen is different from reserved control number, then void reserved one and update new lot number in t502_item_order table
				  	 IF v5060_cnum <> v_control THEN
				  	 	UPDATE t5062_control_number_reserve
				  		   SET c5062_void_fl = 'Y'
				    		 , c5062_last_updated_by = p_userid
				    		 , c5062_last_updated_date = CURRENT_DATE
						 WHERE c5062_transaction_id = p_refid
						   AND c5060_control_number_inv_id= v_part_inv
				  		   AND c5062_void_fl IS NULL;
				  		   
				  		UPDATE t502_item_order
				  			SET c502_control_number = v_control
				  		WHERE c501_order_id = p_refid
				  			AND c205_part_number_id = v_pnum
				  			AND c502_control_number = v5060_cnum
				  			AND c502_void_fl IS NULL;
				  			
			  	 	END IF;
			  	 	-- if the old control number and new control number is different, then replace the control number with new value in t502_item_order table
			  	 	IF v_control <> v_temp_cntl
				  	THEN
				  		UPDATE t502_item_order
						  			SET c502_control_number = v_control
						  		WHERE c501_order_id = p_refid
						  			AND c205_part_number_id = v_pnum
						  			AND c502_control_number = v_temp_cntl
						  			AND c502_void_fl IS NULL;
				  	END IF;
			  	 	
			  	END IF;
		  	END IF;
			
	  		-- save the lot numbers in the table t5055_control_number
			INSERT
		    INTO t5055_control_number
		      (
		        c5055_control_number_id,
		        c5055_ref_id,
		        c901_ref_type,
		        c205_part_number_id,
		        c5055_qty,
		        c5055_control_number,
		        c5052_location_id,
		        c901_type,
		        c5055_last_updated_by,
		        c5055_last_updated_date,
		        c901_warehouse_type,
		        c205_client_part_number_id,
		        C5040_PLANT_ID,
		        C1900_COMPANY_ID,
		        C5055_ERROR_DTLS
		      )
		      VALUES
		      (
		        s5055_control_number_id.NEXTVAL,
		        p_refid,
		        p_reftype,
		        v_pnum,
		        v_qty,
		        v_control,
		        v_locid,
		        p_loc_oper,
		        p_userid,
		        CURRENT_DATE,
		        v_whtype,
		        v_frm_part,
		        v_plant_id,
		        v_comp_id,
		        v_err_msg
		      );
		END LOOP;
		
		p_err_fl := v_err_fl;
		
	END gm_sav_item_lot_num;
END gm_pkg_op_item_control_txn;
/