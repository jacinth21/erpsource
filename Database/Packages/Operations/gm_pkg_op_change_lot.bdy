CREATE OR REPLACE
PACKAGE BODY gm_pkg_op_change_lot
IS

/***********************************************************
*  Description : Procedure to save lot number for transaction PMT#39597
*  Parameter   : transaction id, partnum, qty, control number, userid
*  Author      : Vinoth
***********************************************************/
PROCEDURE gm_save_change_lot (
	   p_txn_id     IN    t413_inhouse_trans_items.C412_INHOUSE_TRANS_ID%TYPE,
	   p_part_num   IN    t413_inhouse_trans_items.C205_PART_NUMBER_ID%TYPE,
       p_qty        IN    t413_inhouse_trans_items.C413_ITEM_QTY%TYPE,
       p_old_ctrl   IN    t413_inhouse_trans_items.C413_CONTROL_NUMBER%TYPE,
       p_new_ctrl   IN    t413_inhouse_trans_items.C413_CONTROL_NUMBER%TYPE,
       p_userid     IN    t413_inhouse_trans_items.C413_LAST_UPDATED_BY%TYPE,
       p_out_msg    OUT   VARCHAR2
)
	AS
	v_valid_ctrl	VARCHAR2(200);
	v_rule_value	VARCHAR2(200);
	v_out_msg		VARCHAR2(200);
	v_prc_nm		VARCHAR2(200);
	v_exec_string	VARCHAR2(200);
	v_txntype		t5055_control_number.C901_REF_TYPE%TYPE;
	v_company_id	t1900_company.c1900_company_id%TYPE;
	v_txn_id        t413_inhouse_trans_items.C412_INHOUSE_TRANS_ID%TYPE;
	
	CURSOR txntype_cur
    IS
    -- For select transaction type based on transaction id
        SELECT c901_ref_type v_txntype 
		  FROM t5055_control_number 
		 WHERE c5055_ref_id = p_txn_id
		   AND c5055_void_fl IS NULL;
	BEGIN
	FOR txntype IN txntype_cur
    LOOP
		v_txntype := txntype.v_txntype;
	END LOOP; 
	
	SELECT SUBSTR(p_txn_id,0,5) INTO v_txn_id
     FROM DUAL;
     
     IF v_txn_id = 'GM-RA' THEN
      p_out_msg := 'Cannot Perform this Transaction';
		 Return;
	 END IF;
	 
	 BEGIN
		SELECT get_compid_frm_cntx INTO v_company_id
          FROM DUAL;
     EXCEPTION
 	 WHEN NO_DATA_FOUND THEN
     v_company_id := NULL;
    END;
	 
	BEGIN
		-- for selecting procedure name based on transaction id
		SELECT v_prc_nm INTO v_prc_nm 
		  FROM (
		SELECT 'INHT_LOT' v_prc_nm 
		  FROM t413_inhouse_trans_items t413, t412_inhouse_transactions t412 
		 WHERE t413.C412_INHOUSE_TRANS_ID = t412.C412_INHOUSE_TRANS_ID 
		   AND t413.C412_INHOUSE_TRANS_ID = p_txn_id
		   AND t412.C1900_COMPANY_ID = v_company_id
		   AND t413.C413_VOID_FL IS NULL
		   AND t412.C412_VOID_FL IS NULL
		 UNION
		SELECT 'CNSGN_LOT' v_prc_nm 
		  FROM t505_item_consignment t505, t504_consignment t504 
		 WHERE t505.C504_CONSIGNMENT_ID = t504.c504_consignment_id
           AND t505.C504_CONSIGNMENT_ID = p_txn_id
           AND t504.C1900_COMPANY_ID = v_company_id
           AND t505.C505_VOID_FL IS NULL
           AND t504.C504_VOID_FL IS NULL
		 UNION 
		SELECT 'ORD_LOT' v_prc_nm 
		  FROM t502_item_order t502, t501_order t501
		 WHERE t502.C501_ORDER_ID = t501.C501_ORDER_ID
		   AND t502.C501_ORDER_ID = p_txn_id
		   AND t501.C1900_COMPANY_ID = v_company_id
		   AND t502.C502_VOID_FL IS NULL
		   AND t501.C501_VOID_FL IS NULL
		 );
	 EXCEPTION
  	 WHEN NO_DATA_FOUND THEN
     p_out_msg := 'No Data Found';
		 Return;
     END;
     
	BEGIN
		SELECT get_rule_value (v_prc_nm, 'CHNGLOT') INTO v_rule_value
          FROM DUAL;
     EXCEPTION
 	 WHEN NO_DATA_FOUND THEN
     v_rule_value := NULL;
    END;
    	
    	 -- To check control number is valid or invalid
        	gm_pkg_common.gm_chk_cntrl_num(p_part_num,p_new_ctrl,v_txntype,v_company_id,v_valid_ctrl);
    
			IF ( v_valid_ctrl IS NULL AND v_rule_value IS NOT NULL )   
            THEN 
	        v_exec_string := 'BEGIN ' || v_rule_value || '(:p_txn_id,:p_qty,:p_part_num,:p_new_ctrl,:p_old_ctrl,:p_userid,:v_txntype,:v_out_msg); END;'; 
			EXECUTE IMMEDIATE v_exec_string using p_txn_id ,p_qty,p_part_num,p_old_ctrl,p_new_ctrl,p_userid,v_txntype , out v_out_msg;
			ELSE
			v_out_msg := v_valid_ctrl;
			END IF;
			p_out_msg := v_out_msg;
			
END gm_save_change_lot;

/***********************************************************
*  Description : Procedure to save lot number for Inhouse transaction PMT#39597
*  Parameter   : transaction id, partnum, qty, control number, userid
*  Author      : Vinoth
***********************************************************/
PROCEDURE gm_sav_inht_chng_lot (
       p_txn_id		IN    t413_inhouse_trans_items.C412_INHOUSE_TRANS_ID%TYPE,
       p_qty		IN    t413_inhouse_trans_items.C413_ITEM_QTY%TYPE,
       p_part_num	IN    t413_inhouse_trans_items.C205_PART_NUMBER_ID%TYPE,
       p_old_ctrl	IN    t413_inhouse_trans_items.C413_CONTROL_NUMBER%TYPE,
       p_new_ctrl	IN    t413_inhouse_trans_items.C413_CONTROL_NUMBER%TYPE,
       p_userid		IN    t413_inhouse_trans_items.C413_LAST_UPDATED_BY%TYPE,
       p_txntype    IN    t5055_control_number.C901_REF_TYPE%TYPE,
       p_out_msg    OUT   VARCHAR)
    
    AS
    v_message	VARCHAR2(200);
    v_rowcount	NUMBER :=0;
    v_rowcount1	NUMBER :=0;
   BEGIN
 
   UPDATE t413_inhouse_trans_items  
      SET c413_control_number = p_new_ctrl,
      	  C413_LAST_UPDATED_BY = p_userid,
      	  C413_LAST_UPDATED_DATE = CURRENT_DATE
    WHERE C412_INHOUSE_TRANS_ID = p_txn_id 
      AND C205_PART_NUMBER_ID = p_part_num 
      AND C413_ITEM_QTY = p_qty
      AND c413_control_number = p_old_ctrl
      AND c413_void_fl IS NULL;
      
       v_rowcount := SQL%ROWCOUNT;
       
   UPDATE t5055_control_number 
      SET C5055_CONTROL_NUMBER = p_new_ctrl,
      	  C5055_LAST_UPDATED_BY = p_userid,
      	  C5055_LAST_UPDATED_DATE = CURRENT_DATE
    WHERE C5055_REF_ID = p_txn_id 
      AND C205_PART_NUMBER_ID = p_part_num 
      AND C5055_QTY = p_qty
      AND C5055_CONTROL_NUMBER = p_old_ctrl
      AND C5055_VOID_FL IS NULL;
      
       v_rowcount1 := SQL%ROWCOUNT;	
      
       GM_UPDATE_LOG (p_txn_id , p_new_ctrl, 108820, p_userid, v_message);
      
       IF v_rowcount = 0 OR v_rowcount1 = 0 
       THEN
       p_out_msg := 'No Data Found';
       ELSE
       p_out_msg := 'Control Number Updated Successfully';
       END IF;
      
   END gm_sav_inht_chng_lot;

/***********************************************************
*  Description : Procedure to save lot number for Order transaction PMT#39597
*  Parameter   : transaction id, partnum, qty, control number, userid
*  Author      : Vinoth
***********************************************************/
      PROCEDURE gm_sav_ord_chng_lot (
       p_txn_id    IN    t502_item_order.C501_ORDER_ID%TYPE,
       p_qty       IN    t502_item_order.C502_ITEM_QTY%TYPE,
       p_part_num  IN    t502_item_order.C205_PART_NUMBER_ID%TYPE,
       p_old_ctrl  IN    t502_item_order.C502_CONTROL_NUMBER%TYPE,
       p_new_ctrl  IN    t502_item_order.C502_CONTROL_NUMBER%TYPE,
       p_userid    IN    t413_inhouse_trans_items.C413_LAST_UPDATED_BY%TYPE,
       p_txntype   IN    t5055_control_number.C901_REF_TYPE%TYPE,
       p_out_msg   OUT   VARCHAR)
    
    AS
    v_message VARCHAR2(200);
    v_rowcount NUMBER :=0;
    v_rowcount1 NUMBER :=0;
   BEGIN

   UPDATE t502_item_order 
      SET C502_CONTROL_NUMBER = p_new_ctrl 
    WHERE C501_ORDER_ID = p_txn_id 
      AND C205_PART_NUMBER_ID = p_part_num 
      AND C502_ITEM_QTY = p_qty
      AND C502_CONTROL_NUMBER = p_old_ctrl
      AND C502_VOID_FL IS NULL;
      v_rowcount := SQL%ROWCOUNT;
      
   UPDATE t5055_control_number 
      SET C5055_CONTROL_NUMBER = p_new_ctrl,
      	  C5055_LAST_UPDATED_BY = p_userid,
      	  C5055_LAST_UPDATED_DATE = CURRENT_DATE
    WHERE C5055_REF_ID = p_txn_id 
      AND C205_PART_NUMBER_ID = p_part_num 
      AND C5055_QTY = p_qty
      AND C5055_CONTROL_NUMBER = p_old_ctrl
      AND C5055_VOID_FL IS NULL;
      
      v_rowcount1 := SQL%ROWCOUNT;	
      
       GM_UPDATE_LOG (p_txn_id , p_new_ctrl, 108820, p_userid, v_message);
		
       IF v_rowcount = 0 OR  v_rowcount1 = 0 
       THEN
       p_out_msg := 'No Data Found';
       ELSE
       p_out_msg := 'Control Number Updated Successfully';
       END IF;
       
   END gm_sav_ord_chng_lot;

/***********************************************************
*  Description : Procedure to save lot number for Consignment transaction PMT#39597
*  Parameter   : transaction id, partnum, qty, control number, userid
*  Author      : Vinoth
***********************************************************/
   PROCEDURE gm_sav_cn_chng_lot (
       p_txn_id    IN    t505_item_consignment.C504_CONSIGNMENT_ID%TYPE,
       p_qty       IN    t505_item_consignment.C505_ITEM_QTY%TYPE,
       p_part_num  IN    t505_item_consignment.C205_PART_NUMBER_ID%TYPE,
       p_old_ctrl  IN    t505_item_consignment.C505_CONTROL_NUMBER%TYPE,
       p_new_ctrl  IN    t505_item_consignment.C505_CONTROL_NUMBER%TYPE,
       p_userid    IN    t413_inhouse_trans_items.C413_LAST_UPDATED_BY%TYPE,
       p_txntype   IN    t5055_control_number.C901_REF_TYPE%TYPE,
       p_out_msg   OUT   VARCHAR)
    
    AS
    v_message	VARCHAR2(200);
    v_rowcount	NUMBER :=0;
    v_rowcount1	NUMBER :=0;
   BEGIN
    
   UPDATE t505_item_consignment 
      SET C505_CONTROL_NUMBER = p_new_ctrl 
    WHERE C504_CONSIGNMENT_ID = p_txn_id 
      AND C205_PART_NUMBER_ID = p_part_num 
      AND C505_ITEM_QTY = p_qty
      AND C505_CONTROL_NUMBER = p_old_ctrl
      AND C505_VOID_FL IS NULL;
      
      v_rowcount := SQL%ROWCOUNT;
      
   UPDATE t5055_control_number 
      SET C5055_CONTROL_NUMBER = p_new_ctrl,
      	  C5055_LAST_UPDATED_BY = p_userid,
      	  C5055_LAST_UPDATED_DATE = CURRENT_DATE
    WHERE C5055_REF_ID = p_txn_id 
      AND C205_PART_NUMBER_ID = p_part_num 
      AND C5055_QTY = p_qty
      AND C5055_CONTROL_NUMBER = p_old_ctrl
      AND C5055_VOID_FL IS NULL;
      
      v_rowcount1 := SQL%ROWCOUNT;
      
      GM_UPDATE_LOG (p_txn_id , p_new_ctrl, 108820, p_userid, v_message);
   	  
      IF v_rowcount = 0 OR v_rowcount1 = 0 
      THEN
      p_out_msg := 'No Data Found';
      ELSE
      p_out_msg := 'Control Number Updated Successfully';
      END IF;
   END gm_sav_cn_chng_lot;
   
END gm_pkg_op_change_lot;
/