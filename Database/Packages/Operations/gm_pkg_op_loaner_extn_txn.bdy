/* Formatted on 2010/10/22 15:36 (Formatter Plus v4.8.0) */
--@"C:\PMT\bit\SpineIT-ERP\Database\Packages\Operations\gm_pkg_op_loaner_extn_txn.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_loaner_extn_txn
IS
--
--gm_pkg_op_loaner_extension_approve.gm_op_loaner_extension_approve(requestext.ltxnid,p_extensiondate,p_comments,p_surg_date,p_createdby,v_fl);
/******************************************************************
  * Description : Procedure used to save loaner extension Request and 
  * Approve Loaner Extension Requset
  ****************************************************************/
PROCEDURE gm_op_loaner_extension_approve(
    p_loaner_txn_id IN t504a_loaner_transaction.c504a_loaner_transaction_id%TYPE,
    p_consignmentid IN t504a_loaner_transaction.c504_consignment_id%TYPE,
    p_extensiondate IN t504a_loaner_transaction.c504a_expected_return_dt%TYPE,
    p_comments      IN t504a_loaner_transaction.c504a_extension_comments%TYPE,
    p_surg_date     IN t504a_loaner_transaction.c504a_surgery_date%TYPE,
    p_createdby     IN t504a_loaner_transaction.c504a_last_updated_by%TYPE,
    p_pend_apprl_fl IN VARCHAR2,
    p_approve_fl    IN VARCHAR2,
    p_old_extn_date IN t504a_loaner_transaction.c504a_old_expected_retdt%TYPE,
    p_reasontype    IN t504a_loaner_transaction.c901_reason_type%TYPE)
AS
 v_loaner_status        t504a_loaner_transaction.c901_status%TYPE;
 v_loaner_txn_id        t504a_loaner_transaction.c504a_loaner_transaction_id%TYPE;
 v_consign_type 		t504_consignment.c504_type%TYPE;
BEGIN
  BEGIN
	SELECT t504_a.c901_status , t504_a.c504a_loaner_transaction_id , t504.c504_type 
		INTO v_loaner_status , v_loaner_txn_id,  v_consign_type
	    	FROM t504a_loaner_transaction t504_a, t504_consignment t504
			WHERE t504_a.c504a_loaner_transaction_id = p_loaner_txn_id
            AND   t504.c504_consignment_id = p_consignmentid
            AND t504.c504_consignment_id = t504_a.C504_consignment_id  
	    	AND t504_a.c504a_void_fl IS NULL 
            AND t504.C504_void_fl  IS NULL
    FOR UPDATE;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
    v_consign_type:= NULL;
  END;
	   -- p_pend_apprlFl - it get from Rule(LEEXTAPPR) - if rule value is Y update the request to  status Pending Approval else Update the staus is Approved
  IF p_pend_apprl_fl = 'Y' AND v_consign_type = 4127 THEN -- For Product loaners only, the Extension should go to Pending Approval screen (PMT-49694)
     -- p_approve_fl comes When approve the loaner extension request from pending approval screen
    UPDATE t504a_loaner_transaction
       SET c504a_expected_return_dt      = DECODE(p_approve_fl,'Y',TRUNC(p_extensiondate),c504a_expected_return_dt),
           c504a_is_loaner_extended        = DECODE(p_approve_fl,'Y','Y',c504a_is_loaner_extended),
	       c504a_ext_requested_date        = CURRENT_DATE,
	       c504a_ext_requested_by          = DECODE(p_approve_fl,'Y',c504a_ext_requested_by,p_createdby),
	       c504a_ext_appr_rej_dt           = CURRENT_DATE,
	       c901_status                     = DECODE(p_approve_fl,'Y',1902,1901),
	       c504a_extension_comments        = DECODE(p_approve_fl,'Y',c504a_extension_comments,p_comments),
	       c504a_appr_rej_comments         = DECODE(p_approve_fl,'Y',p_comments,NULL),
	       c504a_status_changed_by         = DECODE(p_approve_fl,'Y',p_createdby,NULL),
	       c504a_surgery_date              = TRUNC(p_surg_date),
	       c504a_last_updated_date         = TRUNC(CURRENT_DATE),
	       c504a_last_updated_by           = p_createdby,
	       c504a_ext_required_date         = DECODE(p_approve_fl,'Y',c504a_ext_required_date,TRUNC(p_extensiondate)),
	       c504a_old_expected_retdt        = TRUNC(p_old_extn_date) ,
	       c901_reason_type                = DECODE(p_approve_fl,'Y',c901_reason_type,p_reasontype),
	       c504a_appr_rej_email_fl         = null
     WHERE c504a_loaner_transaction_id     = v_loaner_txn_id;
  ELSE
    UPDATE t504a_loaner_transaction
       SET c504a_expected_return_dt      = TRUNC(p_extensiondate),
	       c504a_is_loaner_extended      = 'Y',
	       c504a_ext_requested_date      = CURRENT_DATE,
	       c504a_ext_requested_by        = p_createdby,
	       c504a_ext_appr_rej_dt         = CURRENT_DATE,
	       c901_status                   = 1902,
	       c504a_extension_comments      = p_comments,
	       c504a_appr_rej_comments       = NULL,
	       c504a_status_changed_by       = p_createdby,
	       c504a_surgery_date            = TRUNC(p_surg_date),
	       c504a_last_updated_date       = TRUNC(CURRENT_DATE),
	       c504a_last_updated_by         = p_createdby,
	       c504a_ext_required_date       = TRUNC(p_extensiondate) ,
	       c901_reason_type              = p_reasontype
     WHERE c504a_loaner_transaction_id   = v_loaner_txn_id;
  END IF;
   --This update occurs when approve the Loaner Extension from approval screen(p_approve_fl) OR  LEEXTAPPR - Rule Value not Equal to Y(v_pend_appr_fl)
  IF(p_pend_apprl_fl != 'Y' OR p_approve_fl = 'Y') THEN --when update from pending approval screen and OUS
	    UPDATE t504a_consignment_loaner
	       SET c504a_expected_return_dt = TRUNC(p_extensiondate) ,
	           c504a_last_updated_by    = p_createdby ,
	           c504a_last_updated_date  = TRUNC(CURRENT_DATE)
         WHERE c504_consignment_id      = p_consignmentid;  
   END IF;
END gm_op_loaner_extension_approve;
/******************************************************************
  * Description : Procedure used to save loaner extension with Replenishment
  ****************************************************************/
PROCEDURE gm_op_loaner_exten_replenish(
    p_consignmentid  IN t504a_loaner_transaction.c504_consignment_id%TYPE,
    p_extensiondate  IN t504a_loaner_transaction.c504a_expected_return_dt%TYPE,
    p_ext_require_dt IN t504a_loaner_transaction.c504a_ext_required_date%TYPE,
    p_comments       IN t504a_loaner_transaction.c504a_extension_comments%TYPE,
    p_surg_date      IN t504a_loaner_transaction.c504a_surgery_date%TYPE,
    p_createdby      IN t504a_loaner_transaction.c504a_created_by%TYPE,
    p_company_id     IN t1900_company.c1900_company_id%TYPE,
    p_plant_id       IN t5040_plant_master.c5040_plant_id%TYPE,
    p_status         IN t504a_loaner_transaction.c901_status%TYPE,
    p_pend_apprl_fl  IN VARCHAR2,
    p_approve_fl     IN VARCHAR2,
    p_reasontype     IN t504a_loaner_transaction.c901_reason_type%TYPE)
AS
  v_loaner_txn_id VARCHAR2 (20);
  v_initial_exp_return_dt t504a_loaner_transaction.c504a_expected_return_dt%TYPE;
  v_consign_to NUMBER;
  v_loaner_dt t504a_loaner_transaction.c504a_loaner_dt%TYPE;
  v_consigned_to_id t504a_loaner_transaction.c504a_consigned_to_id%TYPE;
  v_sales_rep_id t504a_loaner_transaction.c703_sales_rep_id%TYPE;
  v_account_id t504a_loaner_transaction.c704_account_id%TYPE;
  v_product_req_det_id t504a_loaner_transaction.c526_product_request_detail_id%TYPE;
  v_new_loaner_txn_id VARCHAR2 (20);
  v_loaner_Extn_fl VARCHAR2(2) := NULL;
  v_request_id t525_product_request.c525_product_request_id%TYPE;
  v_date_diff NUMBER;
  v_date_format varchar2(20);
BEGIN
   BEGIN
    SELECT t504a.c504a_loaner_transaction_id,t504a.c504a_expected_return_dt,t504a.c901_consigned_to,t504a.c504a_loaner_dt,
	       c504a_consigned_to_id,c703_sales_rep_id,c704_account_id,s504a_loaner_trans_id.NEXTVAL,c526_product_request_detail_id
      INTO v_loaner_txn_id,v_initial_exp_return_dt,v_consign_to,v_loaner_dt,
           v_consigned_to_id,v_sales_rep_id, v_account_id,v_new_loaner_txn_id,v_product_req_det_id
      FROM t504a_loaner_transaction t504a
     WHERE t504a.c504_consignment_id = p_consignmentid
       AND t504a.c504a_return_dt      IS NULL
       AND t504a.c504a_void_fl        IS NULL;
  EXCEPTION
     WHEN NO_DATA_FOUND THEN
      raise_application_error (-20999, p_consignmentid || ' is not in Pending return status.');
  END;
  IF p_pend_apprl_fl != 'Y'  THEN
     v_loaner_Extn_fl := 'Y';
  END IF;
  
    --PMT-51852
    v_request_id := gm_pkg_op_loaner.get_loaner_request_id(v_loaner_txn_id);
    
    --PC-4351
     SELECT get_compdtfmt_frm_cntx() 
	  INTO v_date_format 
	  from dual;
	  
    -- check return date	
	SELECT trunc(v_initial_exp_return_dt) - trunc(current_date)
  	  INTO v_date_diff
      FROM dual;
      
     IF v_date_diff > 0 THEN
		raise_application_error (- 20999, ' Return Date ('||to_char(v_initial_exp_return_dt,v_date_format)||') cannot be greater than current date ('||to_char(current_date,v_date_format)||') ');
	 END IF;
   
  --B--
	  UPDATE t504a_loaner_transaction
	     SET c504a_return_dt             = TRUNC(v_initial_exp_return_dt),
	         c504a_processed_date        = v_initial_exp_return_dt,
	         c504a_last_updated_date     = TRUNC(CURRENT_DATE),
	         c504a_last_updated_by       = p_createdby
	   WHERE c504a_loaner_transaction_id = v_loaner_txn_id;
  ---C---
  INSERT
    INTO t504a_loaner_transaction
         (c504a_loaner_transaction_id,c504_consignment_id,c504a_loaner_dt,c504a_expected_return_dt,c901_consigned_to,c504a_parent_loaner_txn_id,
         c504a_is_loaner_extended,c504a_created_date,c504a_created_by,c504a_consigned_to_id,c703_sales_rep_id,c704_account_id,
         c526_product_request_detail_id,c1900_company_id,c5040_plant_id,c901_status ,c504a_ext_requested_date, c504a_ext_requested_by,
         c504a_ext_required_date, c504a_surgery_date,c504a_extension_comments,c504a_old_expected_retdt,c901_reason_type)
  VALUES (v_new_loaner_txn_id,p_consignmentid,TRUNC(v_loaner_dt), DECODE(v_loaner_Extn_fl,'Y',TRUNC(p_extensiondate),v_initial_exp_return_dt),v_consign_to,v_loaner_txn_id,
         v_loaner_Extn_fl,TRUNC(CURRENT_DATE),p_createdby,v_consigned_to_id, v_sales_rep_id,v_account_id,
         v_product_req_det_id,p_company_id,p_plant_id,p_status ,CURRENT_DATE,p_createdby,
         TRUNC(p_ext_require_dt),TRUNC(p_surg_date), p_comments,TRUNC(v_initial_exp_return_dt),p_reasontype);
         
   --This update occurs when approve the Loaner Extension from approval screen(p_approve_fl) OR  LEEXTAPPR - Rule Value not Equal to Y(p_approve_fl)
      IF(p_pend_apprl_fl != 'Y' OR p_approve_fl = 'Y') THEN 
         UPDATE t504a_consignment_loaner
            SET c504a_expected_return_dt = p_extensiondate
              , c504a_last_updated_by = p_createdby
	          , c504a_last_updated_date = CURRENT_DATE            
          WHERE c504_consignment_id = p_consignmentid;
          
          UPDATE t526_product_request_detail
                SET C526_EXTEND_COUNT = NVL(C526_EXTEND_COUNT,0) +1
                  , C526_LAST_UPDATED_BY = p_createdby
                  , C526_LAST_UPDATED_DATE = CURRENT_DATE
          WHERE C526_PRODUCT_REQUEST_DETAIL_ID =  v_product_req_det_id;
         END IF;
             
END gm_op_loaner_exten_replenish;
/******************************************************************
  * Description : This is Procedure is called to save the Approve reject 
  *  email flag after the email is sent
****************************************************************/
PROCEDURE gm_op_upd_appr_rej_email_flg( 
    p_input_str IN CLOB,
    p_user_id   IN t504a_loaner_transaction.c504a_last_updated_by%TYPE
 )
AS
 v_loaner_tran_id     VARCHAR2 (20);
 CURSOR cur_token IS
         SELECT token FROM v_in_list;
  BEGIN
      my_context.set_my_inlist_ctx (p_input_str);
      FOR currindex IN cur_token
      LOOP
	   UPDATE t504a_loaner_transaction
            SET c504a_appr_rej_email_fl  = 'Y'
              , c504a_last_updated_by = p_user_id
	          , c504a_last_updated_date = CURRENT_DATE            
          WHERE c526_product_request_detail_id = currindex.token
            AND c504a_return_dt IS NULL
            AND c504a_ext_appr_rej_dt IS NOT NULL
            AND c504a_void_fl  IS NULL;
      END LOOP;
  END gm_op_upd_appr_rej_email_flg;
END gm_pkg_op_loaner_extn_txn;
/
