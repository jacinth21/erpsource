/* Formatted on 2011/06/14 17:57 (Formatter Plus v4.8.0) */
--@"C:\Database\Packages\Operations\gm_pkg_op_inv_field_sales_rpt.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_inv_field_sales_rpt
IS
   /*******************************************************
     * Description : Procedure to fetch fieldsales part mapping details
     * Author      : 
     *******************************************************/
	PROCEDURE gm_fch_fs_part_mapping (
		  p_warehouseid IN 		 t5051_inv_warehouse.c5051_inv_warehouse_id%type
		, p_partnum		IN		 t205_part_number.c205_part_number_id%TYPE
		, p_location_id	IN		 t5052_location_master.c5052_location_id%TYPE  
		, p_ref_id		IN 		 t5052_location_master.c5052_ref_id%TYPE
		, p_location_type IN     t5052_location_master.c901_location_type%TYPE
		, p_out_cur		OUT 	 TYPES.cursor_type
	)
	AS
	   v_locationstr_id t5052_location_master.c5052_location_id%TYPE;
	   v_company_id   t1900_company.c1900_company_id%TYPE;
	   v_filter 		T906_RULES.C906_RULE_VALUE%TYPE;
  	   v_plant_id 		T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
	BEGIN
	SELECT  get_compid_frm_cntx() ,get_plantid_frm_cntx() INTO  v_company_id,v_plant_id FROM DUAL;  
			/**************************************************************************************************************************************************************
			* NVL2() Description : If the first parameter is NOTNULL it returns the value in second parameter,if the first parameter is NULL it returns the third parameter.
			* Author: karthik
			****************************************************************************************************************************************************************/
       	SELECT NVL2(GET_RULE_VALUE(v_company_id,'FIELD_SALES_RPT'),v_plant_id,v_company_id) --FIELD_SALES_RPT:=rule value-plant
 			INTO v_filter 
			FROM DUAL;

		OPEN p_out_cur FOR
    SELECT DECODE (t5051.c5051_inv_warehouse_id, 3, get_distributor_name (t5052.c5052_ref_id),5,get_account_name(t5052.c5052_ref_id), t5052.c5052_location_cd) location,
    	   get_code_name (t5052.c901_location_type) loctype, get_code_name (t5052.c901_status) status,
    	   t5053.c205_part_number_id pnum, t205.c205_part_num_desc pdesc, t5053.c5053_curr_qty qty,
    	   t5052.c5052_location_id locationid,  t5052.c5052_location_cd locationcd,
    	   t5052.c901_status status_id
  		  , t5051.c5051_inv_warehouse_id warehouse_id, t5051.c5051_inv_warehouse_nm warehouse_nm, t5051.C901_WAREHOUSE_TYPE
		    warehouse_type, t5051.C901_STATUS_ID warehouse_status_id, get_code_name (t5051.C901_STATUS_ID) warehouse_status_nm
		  , t5053.C5053_MAX_QTY maxqty, t5053.C5053_MIN_QTY minqty, t5053.C5053_LAST_UPDATE_TRANS_ID last_trans_id
		  , t5053.C901_LAST_TRANSACTION_TYPE last_trans_type, get_code_name (C901_LAST_TRANSACTION_TYPE) last_trans_type_nm,
		    t5053.C901_TYPE map_type, t5053.C901_ACTION map_action, get_code_name (t5053.C901_TYPE) map_type_nm
		  , get_code_name (t5053.C901_ACTION) map_action_nm
     FROM  t5053_location_part_mapping t5053, t5051_inv_warehouse t5051, t5052_location_master t5052, t205_part_number t205
    WHERE t5051.c5051_inv_warehouse_id = t5052.c5051_inv_warehouse_id
    AND t205.c205_part_number_id     = t5053.c205_part_number_id
    AND t5053.c5052_location_id      = t5052.c5052_location_id
   AND regexp_like (t5053.c205_part_number_id, NVL ((REGEXP_REPLACE(p_partnum,'[+]','\+')), REGEXP_REPLACE(t5053.c205_part_number_id,'[+]','\+')))
    AND t5052.c5052_location_id      = NVL (p_location_id, t5052.c5052_location_id)
    AND t5052.c5052_ref_id			 = DECODE(p_ref_id,'0',t5052.c5052_ref_id,p_ref_id)
    AND t5052.c901_location_type = DECODE(p_location_type,'0',t5052.c901_location_type,p_location_type)
    AND t5051.c5051_inv_warehouse_id = p_warehouseid
    AND (t5052.c1900_company_id = v_company_id OR t5052.c1900_company_id IN (SELECT c1900_company_id FROM t5041_plant_company_mapping WHERE C5041_PRIMARY_FL = 'Y' AND c5041_void_fl IS NULL AND c5040_plant_id =v_filter))
    AND t5052.c5052_void_fl IS NULL
    ORDER BY location,t5053.c205_part_number_id;	
END gm_fch_fs_part_mapping;
	
   
   PROCEDURE gm_fch_acct_part_mapping (
		  p_warehouseid 	IN 		 t5051_inv_warehouse.c5051_inv_warehouse_id%type
		, p_partnum			IN		 t205_part_number.c205_part_number_id%TYPE
		, p_ref_id			IN 		 t5052_location_master.c5052_ref_id%TYPE
		, p_location_type	IN       t5052_location_master.c901_location_type%TYPE
		, p_select_systems  IN       CLOB
		, p_type            IN       t901_code_lookup.c901_code_nm%type
		, p_out_cur		    OUT 	 TYPES.cursor_type
	)
	AS
	   v_locationstr_id t5052_location_master.c5052_location_id%TYPE;
	   v_company_id   t1900_company.c1900_company_id%TYPE;
	   v_filter 		T906_RULES.C906_RULE_VALUE%TYPE;
  	   v_plant_id 		T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
  	   v_count          NUMBER;
  	   
	BEGIN
		
		SELECT  get_compid_frm_cntx() ,get_plantid_frm_cntx() INTO  v_company_id,v_plant_id FROM DUAL;  
			/**************************************************************************************************************************************************************
			* NVL2() Description : If the first parameter is NOTNULL it returns the value in second parameter,if the first parameter is NULL it returns the third parameter.
			* Author: GPALANI
			****************************************************************************************************************************************************************/
           SELECT NVL2(GET_RULE_VALUE(v_company_id,'FIELD_SALES_RPT'),v_plant_id,v_company_id) --FIELD_SALES_RPT:=rule value-plant
 		   INTO v_filter 
		   FROM DUAL;
		   
		   -- Set system ids in context
		   my_context.set_my_inlist_ctx (p_select_systems);
		     
		   IF p_type = '106931' THEN --106931 - By system
		   
		   DELETE FROM my_temp_list;
		   
   		   -- Fetching partnumber from system list and insert into my_temp_list temp table
		   INSERT INTO my_temp_list (my_temp_txn_id) 
		   SELECT t208.c205_part_number_id FROM  t207_set_master t207,t208_set_details t208	
		   WHERE t207.c207_set_id IN (SELECT TOKEN FROM v_in_list) 
		   AND t207.c207_set_id = T208.c207_set_id
		   AND t207.c901_set_grp_type = '1600' -- 1600 System type
		   AND t208.c208_void_fl IS NULL
		   AND t207.c207_void_fl IS NULL
		   ;  
			
		   END IF;

		   -- Get partnumber count from temp table 
		   SELECT COUNT(*) INTO v_count FROM my_temp_list;
		
		   OPEN p_out_cur FOR
    
        select T5072.C205_PART_NUMBER_ID PART,DECODE(get_complangid_frm_cntx(),'103097',t205.c205_part_num_desc,get_partdesc_by_company (T5072.C205_PART_NUMBER_ID)) PART_DESC,
         T5072.C5072_CONTROL_NUMBER LOT,C5072_CURR_QTY QTY, T704.C101_DEALER_ID Dealer_ID, DECODE(get_complangid_frm_cntx(),'103097',NVL(t704.C704_ACCOUNT_NM_EN,t704.C704_ACCOUNT_NM),
         C704_ACCOUNT_NM) ACCT_NAME, DECODE(get_complangid_frm_cntx(),'103097',NVL(t101.c101_party_nm_en,t101.c101_party_nm),t101.c101_party_nm) Dealer_nm, T5070.C901_TXN_ID ACCT_ID,
          T5070.C5070_SET_LOT_MASTER_ID Set_Lot_master_id
		,t207.c207_set_nm SET_NAME,t207.c207_set_id SET_ID
		from T5072_SET_PART_LOT_QTY T5072, T5070_SET_LOT_MASTER T5070, T5052_LOCATION_MASTER T5052, T704_ACCOUNT T704,T101_party T101,
		t208_set_details T208,t207_set_master t207,t205_part_number t205
		where
		(
		(regexp_like (T5072.C205_PART_NUMBER_ID, NVL (p_partnum, T5072.C205_PART_NUMBER_ID)) and v_count=0)
 			OR T5072.C205_PART_NUMBER_ID IN (SELECT my_temp_txn_id FROM my_temp_list))
		AND t207.c207_set_id = T208.c207_set_id
		AND t207.c901_set_grp_type = '1600' -- 1600 System type
		and t5072.c205_part_number_id = T208.c205_part_number_id   
		and t5072.c205_part_number_id = t205.c205_part_number_id  
		and t207.c207_void_fl is null
		and t208.c208_void_fl is null
		and T5070.C901_TXN_ID=DECODE(p_ref_id,'0',T5070.C901_TXN_ID,p_ref_id)
		and T5052.C5051_INV_WAREHOUSE_ID=p_warehouseid
		and T5052.C5052_LOCATION_ID=T5070.C5052_LOCATION_ID
		and T5070.C5070_SET_LOT_MASTER_ID=T5072.C5070_SET_LOT_MASTER_ID
		and T704.C901_ACCOUNT_TYPE=p_location_type
		and T704.C704_ACCOUNT_ID=T5070.C901_TXN_ID 
		and T704.C704_ACCOUNT_ID=T5052.C5052_REF_ID
		AND t101.c101_party_id = t704.C101_DEALER_ID(+)
		and (t5052.c1900_company_id = v_company_id OR t5052.c1900_company_id IN (SELECT c1900_company_id FROM t5041_plant_company_mapping WHERE C5041_PRIMARY_FL = 'Y' AND c5041_void_fl IS NULL AND c5040_plant_id =v_filter)) 
		order by ACCT_NAME,PART,LOT,QTY;

end gm_fch_acct_part_mapping;



 /*******************************************************
     * Description : Procedure to fetch inventory location log details
     * Author      : 
     *******************************************************/
	PROCEDURE gm_fch_inv_loc_log (
		  p_partnum		IN		 t205_part_number.c205_part_number_id%TYPE
		, p_location_id	IN		 t5052_location_master.c5052_location_id%TYPE
		, p_from_dt     IN       t5054_inv_location_log.c5054_created_date%TYPE
		, p_to_dt       IN       t5054_inv_location_log.c5054_created_date%TYPE
		, p_out_cur		OUT 	 TYPES.cursor_type
	)
	AS
	v_company_id   t1900_company.c1900_company_id%TYPE;
	v_filter 		T906_RULES.C906_RULE_VALUE%TYPE;
  	v_plant_id 		T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
  	
	BEGIN
	SELECT  get_compid_frm_cntx() ,get_plantid_frm_cntx() INTO  v_company_id,v_plant_id FROM DUAL;
	
		/**************************************************************************************************************************************************************
			* NVL2() Description : If the first parameter is NOTNULL it returns the value in second parameter,if the first parameter is NULL it returns the third parameter.
		****************************************************************************************************************************************************************/
		SELECT NVL2(GET_RULE_VALUE(v_company_id,'FIELD_SALES_RPT'),v_plant_id,v_company_id) --FIELD_SALES_RPT:=rule value-plant
 			INTO v_filter 
			FROM DUAL;

		OPEN p_out_cur FOR
	SELECT DECODE(t5052.c5051_inv_warehouse_id,3,get_distributor_name(t5052.c5052_ref_id),5,get_account_name(t5052.c5052_ref_id),t5052.c5052_location_cd) FIELDSALES,t5054.c5054_txn_id txnid, t5054.c205_part_number_id pnum, get_partnum_desc (t5054.c205_part_number_id) pdesc,
		   t5054.c5054_orig_qty orgQty, t5054.c5054_txn_qty txnQty, t5054.c5054_new_qty newQty,get_user_name (t5054.c5054_created_by) cby,
		   TO_CHAR (t5054.c5054_created_date, get_compdtfmt_frm_cntx() ||' HH:MM:MI') trandate, 
		   t5054.c901_txn_type txntype, get_code_name (t5054.c901_txn_type) txnTypeNm, 
		   t5054.c901_action actionid, get_code_name (t5054.c901_action) actionNm,
		   t5054.c901_type type, get_code_name (t5054.c901_type) typeNm, t5054.c5054_created_date cdate,
  		   t5052.c5052_location_id location_id, t5052.c5052_location_cd location_cd, t5052.c5052_ref_id refid, T5052.C5051_INV_WAREHOUSE_ID WAREHOUSE_ID
   	FROM t5054_inv_location_log t5054 ,  t5052_location_master t5052
  	WHERE t5054.c205_part_number_id = p_partnum
  	AND t5054.c5052_location_id = t5052.c5052_location_id
    AND t5054.c5052_location_id   = p_location_id
    AND TRUNC(t5054.c5054_created_date) >= TRUNC (NVL(p_from_dt ,t5054.c5054_created_date)) 
    AND TRUNC (t5054.c5054_created_date) <= TRUNC (NVL(p_to_dt ,t5054.c5054_created_date))
    AND (t5052.c1900_company_id = v_company_id OR t5052.c1900_company_id IN (SELECT c1900_company_id FROM t5041_plant_company_mapping WHERE C5041_PRIMARY_FL = 'Y' AND c5041_void_fl IS NULL AND c5040_plant_id =v_filter))
    AND t5052.c5052_void_fl IS NULL
  	ORDER BY t5054.c5054_inv_location_log_id DESC;
END gm_fch_inv_loc_log;



 /*******************************************************
     * Description : Procedure to fetch inventory location log details
     * Author      : 
     *******************************************************/

	PROCEDURE gm_fch_acct_lot_log (
		  p_partnum				IN		 t205_part_number.c205_part_number_id%TYPE
		, p_set_lot_master_id	IN		 t5070_set_lot_master.c5070_set_lot_master_id%TYPE
		, p_from_dt		        IN       t5072a_set_part_lot_qty_log.c5072a_created_date%TYPE
		, p_to_dt       		IN       t5072a_set_part_lot_qty_log.c5072a_created_date%TYPE
		, p_lot					IN 	     t5072_set_part_lot_qty.c5072_control_number%TYPE
		, p_out_cur				OUT 	 TYPES.cursor_type
	)
	AS
	v_company_id   t1900_company.c1900_company_id%TYPE;
	v_filter 		T906_RULES.C906_RULE_VALUE%TYPE;
  	v_plant_id 		T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
  	
	BEGIN
	SELECT  get_compid_frm_cntx() ,get_plantid_frm_cntx() INTO  v_company_id,v_plant_id FROM DUAL;

		/**************************************************************************************************************************************************************
			* NVL2() Description : If the first parameter is NOTNULL it returns the value in second parameter,if the first parameter is NULL it returns the third parameter.
		****************************************************************************************************************************************************************/
		SELECT NVL2(GET_RULE_VALUE(v_company_id,'FIELD_SALES_RPT'),v_plant_id,v_company_id) --FIELD_SALES_RPT:=rule value-plant
 		INTO v_filter 
		FROM DUAL;

		OPEN p_out_cur FOR
		
		
	select GET_ACCOUNT_NAME(T5070.C901_TXN_ID) FIELDSALES,T5072A.C5072A_TXN_ID TXNID, T5072A.C205_PART_NUMBER_ID PNUM, GET_PARTNUM_DESC (T5072A.C205_PART_NUMBER_ID) PDESC,
		   T5072A.C5072A_ORIG_QTY ORGQTY, T5072A.C5072A_TXN_QTY TXNQTY, T5072A.C5072A_NEW_QTY NEWQTY,GET_USER_NAME (T5072A.C5072A_CREATED_BY) CBY,
		   TO_CHAR (T5072A.C5072A_CREATED_DATE, GET_COMPDTFMT_FRM_CNTX() ||' HH:MM:MI') TRANDATE, T5072A.c901_type type, T5072A.c901_txn_type txntype
	from T5070_SET_LOT_MASTER T5070 ,  T5072A_SET_PART_LOT_QTY_LOG T5072A
  	where T5072A.C205_PART_NUMBER_ID = p_partnum
  	and T5070.C5070_SET_LOT_MASTER_ID = T5072a.C5070_SET_LOT_MASTER_ID
    and T5070.C5070_SET_LOT_MASTER_ID   = p_set_lot_master_id
    and T5072a.c5072_control_number=p_lot
    and TRUNC(T5072A.C5072A_CREATED_DATE)>= TRUNC(NVL(p_from_dt,T5072A.C5072A_CREATED_DATE)) 
    and TRUNC (T5072A.C5072A_CREATED_DATE)<= TRUNC(NVL(p_to_dt,T5072A.C5072A_CREATED_DATE))
    and T5070.C5070_VOID_FL is null
  	order by T5072A.C5072A_CREATED_DATE desc;

  	
END gm_fch_acct_lot_log;
		
END gm_pkg_op_inv_field_sales_rpt;
/
