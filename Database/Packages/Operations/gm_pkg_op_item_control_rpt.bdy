/* Formatted on 2011/05/16 16:24 (Formatter Plus v4.8.0) */
--@"C:\DATABASE\packages\operations\gm_pkg_op_item_control_rpt.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_item_control_rpt
IS
	/*******************************************************
	  * Description : Procedure to fetch Transaction header
	  * author		: Gopinathan
	 *******************************************************/
	PROCEDURE gm_fch_txn_header (
		p_trans_id	   IN		t412_inhouse_transactions.c412_inhouse_trans_id%TYPE
	  , p_trans_type   IN		t412_inhouse_transactions.c412_type%TYPE
	  , p_out_dtl	   OUT		TYPES.cursor_type
	)
	AS
		v_table 	   t906_rules.c906_rule_value%TYPE;
		v_plant_id     	t5040_plant_master.c5040_plant_id%TYPE;
		v_trans_plant_id     	t5040_plant_master.c5040_plant_id%TYPE;
		v_comp_id       t1900_company.c1900_company_id%TYPE;
		v_company_name  T1900_COMPANY.C1900_COMPANY_NAME%TYPE;
		v_request_company_id  T1900_COMPANY.C1900_COMPANY_ID%TYPE;
		v_request_plant_id T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
		v_plant_name    T5040_PLANT_MASTER.c5040_plant_name%TYPE;
		v_filter VARCHAR2(40);
		v_filter_name   VARCHAR2(40);
		v_insert_flag			VARCHAR2(1);
		v_insert_trans_fl		VARCHAR2(1);
		v_insert_txn_typ		t901_code_lookup.C901_code_id%TYPE;
		v_rule_group 	  		T906_RULES.C906_RULE_GRP_ID%TYPE;
	BEGIN
		SELECT get_rule_value ('TRANS_TABLE', p_trans_type)
		  INTO v_table
		  FROM DUAL;
		  
		  SELECT  NVL(get_plantid_frm_cntx(),get_rule_value('DEFAULT_PLANT','ONEPORTAL')) ,get_compid_frm_cntx() 
			  INTO  v_plant_id ,v_comp_id
			  FROM DUAL;
		  SELECT DECODE(GET_RULE_VALUE(v_comp_id,'ORDFILTER'), null, v_comp_id, v_plant_id) into v_filter from dual;
		  
		--Validating the Plant is Enabled for INSERT  
	    SELECT get_rule_value(v_plant_id,'INSERT_PART') 
	    	INTO v_insert_flag 
	        FROM DUAL;
		
	         v_rule_group := 'INSERT_TXN_MAP'||'_'||v_plant_id;
	    SELECT get_rule_value(p_trans_type,v_rule_group) 
	    	INTO v_insert_txn_typ 
	    	FROM DUAL;
	    --Validating the Transaction Type is Enabled for INSERT
		SELECT get_rule_value(v_insert_txn_typ,'INSERT_TRANS')
			INTO v_insert_trans_fl
			FROM DUAL;
		

		-- Fetching the Transaction Header values for Inhouse Transactions like FGLE[50154]
		IF v_table = 'IN_HOUSE'
		THEN
			SELECT c5040_plant_id
			   INTO v_trans_plant_id
			   FROM t412_inhouse_transactions
			  WHERE c412_inhouse_trans_id = p_trans_id
				AND c412_type             = p_trans_type;
					  
			 IF v_trans_plant_id = 	v_plant_id THEN
				v_plant_id := v_trans_plant_id;
			 ELSE
				v_plant_id := NULL;
			 END IF;
			 
			OPEN p_out_dtl
			 FOR
				 SELECT c412_inhouse_trans_id txnid, get_code_name (c412_type) txntypenm
					  , get_code_name (c412_inhouse_purpose) purpose, c412_ref_id refid
					  , get_set_name (gm_pkg_op_consignment.get_set_id (c412_ref_id)) setname
					  , get_user_name (c412_created_by) cname, c412_created_date cdate, c412_comments comments
					  , c412_type txntype, c412_status_fl statusfl, c412_verify_fl verifyfl
					  , DECODE(c412_type,'1006483',
					  					DECODE(c412_status_fl,'0','Open','3','Reviewed','4','Closed'),
					  					'1006572',
					  					DECODE(c412_status_fl,'0','Open','3','Reviewed','4','Closed'),'') status
					  , DECODE(c412_type,'1006483',
					  					DECODE(c412_status_fl,'0','Open','3','Reviewed','4','Closed'),
					  					'1006572',
					  					DECODE(c412_status_fl,'0','Open','3','Reviewed','4','Closed'),
					  					'50154',
					  					get_transaction_status(c412_status_fl,p_trans_type)) statusnm
					  , DECODE (c412_status_fl, '2', 'pick', '3', 'put', 'pick') pickputfl
					  , c412_type txntype
					  , c412_created_by initiatedby, get_user_name(c412_created_by) initiatedbynm, t5055.errcnt errcnt
					  , v_insert_flag  insertFl
					  , v_insert_trans_fl transInsertFl
				   FROM t412_inhouse_transactions, 
				   		(SELECT count(1) errcnt 
				   			FROM t5055_control_number 
				   		WHERE c5055_ref_id = p_trans_id 
				   			AND c5055_error_dtls IS NOT NULL 
				   			AND c5055_control_number IS NOT NULL
				   			AND c5055_void_fl IS NULL) t5055
				  WHERE c412_inhouse_trans_id = p_trans_id AND c412_type = p_trans_type AND c5040_plant_id = v_plant_id;
				  
	 -- Fetching the Transaction Header values for Consignment Sets[4110] and IH Loaner Item[9110] 
		ELSIF v_table = 'CONSIGNMENT'
		THEN
			v_filter_name := 'ITEMEDITREQFILTER';
			
			SELECT c5040_plant_id,c1900_company_id
			   INTO v_request_plant_id,v_request_company_id
			   FROM t504_consignment
			  WHERE c504_consignment_id = p_trans_id;
			 
			  	  BEGIN 
			      	SELECT GET_RULE_VALUE(v_comp_id,v_filter_name)   INTO v_filter  FROM DUAL;
			      EXCEPTION WHEN NO_DATA_FOUND THEN
							v_filter := NULL;
				  END;
			     
					  IF v_filter IS NOT NULL THEN
				      	IF v_request_plant_id IS NOT NULL THEN				      		
				      		IF (v_plant_id != v_request_plant_id) THEN
				      			v_plant_name := get_plant_name(v_request_plant_id);
				      			GM_RAISE_APPLICATION_ERROR('-20999','222',p_trans_id||'#$#'||v_plant_name);
				      		END IF;
				      	END IF;
				      ELSE			
				      	IF v_request_company_id is NOT NULL THEN				      		
				      		IF (v_comp_id != v_request_company_id) THEN
				      			v_company_name := get_company_name(v_request_company_id);
				      			GM_RAISE_APPLICATION_ERROR('-20999','222',p_trans_id||'#$#'||v_company_name);
				      		END IF;
				      	END IF;
					  END IF;
					  
					   SELECT DECODE(GET_RULE_VALUE(v_comp_id,v_filter_name), null, v_comp_id, v_plant_id) into v_filter from dual;
			OPEN p_out_dtl
			 FOR
				 SELECT c504_consignment_id txnid, get_code_name (c504_type) txntypenm
					  , get_code_name (c504_inhouse_purpose) purpose, DECODE(c504_type,106703,c520_request_id,106704,c520_request_id, 9110, c504_ref_id, c207_set_id) refid, c207_set_id setid
					  , get_set_name (c207_set_id) setname,get_distributor_name(C701_DISTRIBUTOR_ID) distname, get_user_name (c504_created_by) cname
					  , c504_created_date cdate, c504_comments comments, c504_type txntype, c504_status_fl statusfl
					  , get_transaction_status(c504_status_fl,p_trans_type) statusnm
					  , c504_verify_fl verifyfl, DECODE (c504_status_fl, '2', 'pick', '3', 'put', 'pick') pickputfl
					  , c504_type txntype, c520_request_id txnparentid
					  , c504_created_by initiatedby, get_user_name(c504_created_by) initiatedbynm, t5055.errcnt errcnt
					  , v_insert_flag  insertFl
					  , v_insert_trans_fl transInsertFl
				   FROM t504_consignment, 
				   		(SELECT count(1) errcnt 
				   			FROM t5055_control_number 
				   		WHERE c5055_ref_id = p_trans_id 
				   			AND c5055_error_dtls IS NOT NULL 
				   			AND c5055_control_number IS NOT NULL
				   			AND c5055_void_fl IS NULL) t5055
				  WHERE c504_consignment_id = p_trans_id  AND (c1900_company_id = v_filter
			         OR c5040_plant_id   = v_filter);

     -- Fetching the Transaction Header values for Sales[50261] 
		ELSIF v_table = 'ORDERS'
		THEN
		SELECT c5040_plant_id
			   INTO v_trans_plant_id
			   FROM t501_order
			  WHERE c501_order_id = p_trans_id;
					  
			 IF v_trans_plant_id = 	v_plant_id THEN
				v_plant_id := v_trans_plant_id;
			 ELSE
				v_plant_id := NULL;
			 END IF;
			OPEN p_out_dtl
			 FOR
				 SELECT c501_order_id txnid, 'Orders' txntypenm, get_account_name (c704_account_id) purpose, '' refid
					  , '' setname, get_user_name (c501_created_by) cname, c501_created_date cdate , c501_surgery_date surgerydate
					  , c501_comments comments, p_trans_type txntype, c501_status_fl statusfl
					  , get_transaction_status(c501_status_fl,p_trans_type) statusnm
					  , c501_update_inv_fl verifyfl, DECODE (c501_status_fl, '1', 'pick', 'put') pickputfl
					  , p_trans_type txntype, c501_hard_po_fl hardcopy
					  , c501_created_by initiatedby, get_user_name(c501_created_by) initiatedbynm,C901_ORDER_TYPE orderType, t5055.errcnt errcnt
					  , v_insert_flag  insertFl
					  , v_insert_trans_fl transInsertFl
				   FROM t501_order, 
				   		(SELECT count(1) errcnt 
				   			FROM t5055_control_number 
				   		WHERE c5055_ref_id = p_trans_id 
				   			AND c5055_error_dtls IS NOT NULL  
				   			AND c5055_control_number IS NOT NULL
				   			AND c5055_void_fl IS NULL) t5055
				  WHERE c501_order_id = p_trans_id AND c501_void_fl IS NULL 
				  AND (c1900_company_id = v_filter
			         OR c5040_plant_id   = v_filter);
		ELSE
			raise_application_error ('-20919', '');
		END IF;
	END gm_fch_txn_header;

	/*******************************************************
	  * Description : Procedure to fetch Transaction detail
	  * author		: Gopinathan
	 *******************************************************/
	PROCEDURE gm_fch_txn_detail (
		p_trans_id	   IN		t412_inhouse_transactions.c412_inhouse_trans_id%TYPE
	  , p_trans_type   IN		t412_inhouse_transactions.c412_type%TYPE
	  , p_loc_oper	   IN		t5055_control_number.c901_type%TYPE
	  , p_out_dtl	   OUT		TYPES.cursor_type
	)
	AS
		v_table 	   		t906_rules.c906_rule_value%TYPE;
		v_warehouse_type 	VARCHAR2(20);
		v_inv_type  		VARCHAR2(20);
		v_wh_type 			VARCHAR2(20);
		v_country_code 		VARCHAR2(10);
		v_Ack_Fl			VARCHAR2(10);
		v_count             NUMBER;
		v_plant_id     	t5040_plant_master.c5040_plant_id%TYPE;
		v_trans_plant_id     	t5040_plant_master.c5040_plant_id%TYPE;
		v_comp_id       t1900_company.c1900_company_id%TYPE;
		v_filter        t906_rules.c906_rule_value%TYPE;
		v_flag VARCHAR2(1);
		v_inhouse_status t412_inhouse_transactions.c412_status_fl%TYPE;
		v_consign_status t504_consignment.c504_status_fl%TYPE;
		v_created_by     t412_inhouse_transactions.c412_created_by%TYPE;
		v_loc_opr        t5055_control_number.c901_type%TYPE;
		v_wh_loc_type    t5051_inv_warehouse.c901_warehouse_type%TYPE;
	BEGIN
		
		SELECT  NVL(get_plantid_frm_cntx(),get_rule_value('DEFAULT_PLANT','ONEPORTAL')) ,get_compid_frm_cntx() 
			  INTO  v_plant_id ,v_comp_id
			  FROM DUAL;
		 
	    SELECT DECODE(GET_RULE_VALUE(v_comp_id,'ORDFILTER'), null, v_comp_id, v_plant_id) --ORDFILTER- To fetch the records for EDC entity countries based on plant
	          INTO v_filter
	          FROM DUAL;  
	          
	    SELECT get_rule_value(v_comp_id,'LOT_TRACK') INTO v_flag FROM DUAL; 
		 
		 v_country_code := NVL(GET_RULE_VALUE('CURRENT_DB','COUNTRYCODE'),'en');
   
  		SELECT  DECODE(v_country_code,'en',90800, 90800)--90800 (US FG)  ,  GH-EDC Change
  			INTO v_wh_type
		  FROM DUAL;	
  			
		SELECT get_rule_value ('TRANS_TABLE', p_trans_type)
		  INTO v_table
		  FROM DUAL;		
		
		-- Get the warehouse type
	   SELECT  DECODE(p_loc_oper, 93343, get_rule_value('MINUS',p_trans_type),get_rule_value('PLUS',p_trans_type))
		 INTO  v_inv_type
		 FROM  DUAL;
	   
	   SELECT  DECODE(p_trans_type,93341,v_wh_type,56010,56001,NVL(get_rule_value(v_inv_type,'WHQTYTYP'),v_wh_type))
	     INTO  v_warehouse_type
	     FROM  DUAL;
	   
	   SELECT  DECODE(v_warehouse_type,90816,v_wh_type,v_warehouse_type)	
	     INTO  v_warehouse_type
	     FROM  DUAL;
-- Added the void flag check pmt-46880
	   
	   SELECT count(1) 
	     INTO v_count
	     FROM t5062_control_number_reserve t5062
	    WHERE t5062.c5062_transaction_id = p_trans_id
	    AND   t5062.c5062_void_fl IS NULL;
	    
	  
	   IF v_count > 0 THEN v_Ack_Fl := 'Y'; ELSE v_Ack_Fl := 'N'; END IF;
	   
		IF v_table = 'IN_HOUSE'
		THEN
			SELECT c5040_plant_id ,c412_status_fl , c412_created_by
			   INTO v_trans_plant_id , v_inhouse_status , v_created_by
			   FROM t412_inhouse_transactions
			  WHERE c412_inhouse_trans_id = p_trans_id
				AND c412_type             = p_trans_type;
					  
			 IF v_trans_plant_id = 	v_plant_id THEN
				v_plant_id := v_trans_plant_id;
			 ELSE
				v_plant_id := NULL;
			 END IF;
			 
			 /* To fix the records which created from autoreplenishment job
			  * if the Transaction type  is "93341 := FG Replenishment" and location type is "93344:=Put Initiated" and inhouse status should notequal to 4 [completed]
			  * and created by is "30301:Manual Load"
			  * Then the location oper is '93344' [Put Initiated: From device while selecting the FGRP transaction]
			  * or else the location oper is '93343' [Pick Transacted]
			  */
			 IF p_trans_type = '93341' AND p_loc_oper = '93344' AND v_inhouse_status != '4' AND v_created_by = '30301' THEN
			 v_loc_opr := '93344';
			 ELSE
			 v_loc_opr :='93343';
			 END IF;
			 
			 IF p_trans_type = '56010' THEN    --RWRP_type=56010 
			 v_wh_loc_type := '56001';   -- RW warehouse type
			 ELSE
			 v_wh_loc_type := '90800';
			 END IF;

			OPEN p_out_dtl
			 FOR
			 	 SELECT   t413.ID ID   -- PART NUMBER
						, t413.ID pnum	 -- part# need for RULE ENGINE
						, t413.topart TOPART -- CLIENT PART NUM ID 
					    , t413.reftype itemtype, t413.pdesc pdesc, NVL (t413.price, 0) price, get_code_name(t413.status) status
						, NVL(GET_OP_QTY_INHOUSE_PARTID(p_trans_id,t413.ID), 0) qty
						, t413.pickqty pickqty, t413.cnum cnum
						, t413.LOCATION LOCATION,GM_PKG_OP_INV_SCAN.GET_LOCATION_CODE(t413.LOCATION) LOCATIONCODE
						, gm_pkg_op_item_control_rpt.get_part_location_cur_qty (t413.ID, t413.LOCATION,p_trans_id) currqty
						, NVL (gm_pkg_op_item_control_rpt.get_initiated_location (decode(t413.reftype,'103932',t413.topart,t413.ID), p_trans_id, t413.pickputfl)
						, gm_pkg_op_inv_scan.get_part_location(decode(t413.reftype,'103932',t413.topart,t413.ID), t413.reftype,v_wh_loc_type)   --type must need to get rule values for pick- we need bulk
						
							  ) sugloc
						, GM_PKG_OP_INV_SCAN.GET_LOCATION_CODE(NVL(gm_pkg_op_item_control_rpt.get_initiated_location (t413.ID, p_trans_id, t413.pickputfl)
						, gm_pkg_op_inv_scan.get_part_location (t413.ID, t413.reftype,v_wh_loc_type))) SUGLOCATIONCODE
						, DECODE(v_warehouse_type,NULL,1,GM_PKG_OP_INV_WAREHOUSE.get_warehouse_id(v_wh_loc_type)) warehouse_id
						, DECODE(v_warehouse_type,NULL,'FG',GM_PKG_OP_INV_WAREHOUSE.get_warehouse_sh_name(v_wh_loc_type,NULL)) warehouse_sh_name
						, DECODE(v_warehouse_type,NULL,90800,v_wh_loc_type) warehouse_type
						, gm_pkg_pd_rpt_udi.get_part_di_number(t413.ID) DI, t413.errordtls errordtls
						, get_part_attr_value_by_comp(t413.ID,'104480',v_comp_id) lottrackfl -- 104480 := Lot tracking required?
						, get_part_price(t413.ID, 'E') equityprice
					 FROM (
					 --	Distinct used to get unique record from above records from t413 and t5055
						 	SELECT DISTINCT ref_id,  reftype,  status ,  statusfl,  ID,  qty ,  pdesc,  price ,  pickputfl ,  cnum ,  topart ,
							  pickqty ,  LOCATION ,  part_number,  errordtls
							FROM
							  (SELECT NVL(t5055.c5055_ref_id,t413.c412_inhouse_trans_id) ref_id,
							    NVL(t5055.c901_ref_type,t413.c412_type) reftype,
							    t413.c901_status status ,
							    t413.c412_status_fl statusfl,
							    NVL(t5055.c205_part_number_id, t413.c205_part_number_id) ID,
							    t413.c413_item_qty qty ,
							    get_partnum_desc (NVL(t5055.c205_part_number_id, t413.c205_part_number_id)) pdesc,
							    t413.c413_item_price price ,
							    DECODE (t413.c412_status_fl, '2', 'pick', '3', 'put', 'pick') pickputfl ,
							    DECODE (p_loc_oper, 93343, DECODE(t413.c412_type,'103932',NVL(NVL(t5055.c5055_control_number,' '),t413.c413_control_number),NVL(t5055.c5055_control_number,' ')), t413.c413_control_number) cnum ,
							    t413.c205_client_part_number_id topart ,
							    DECODE (p_loc_oper , 93343, NVL (t5055.c5055_qty, t413.c413_item_qty) , DECODE (t413.c412_status_fl, '4', t5055.c5055_qty, NVL (t5055.c5055_qty, t413.c413_item_qty)) ) pickqty ,
							    t5055.c5052_location_id LOCATION ,
							    t5055.c205_part_number_id part_number,
							    t5055.c5055_error_dtls errordtls ,NVL(t5055.c5055_qty,0) act_pick_qty,
							    ( SELECT  COUNT(1) 
				                   FROM t5055_control_number t5055A 
				                  where t5055a.c5055_ref_id = p_trans_id
				                ) rec_cnt,
							    c413_control_number ,
							    c5055_control_number
							  FROM
							    /*	Inhouse � Using full outer  join between T413 and T5055 to get the overall data then
							     *  adding conditions in part number, inhouse trans id and control number*/
							    (SELECT t413.c412_inhouse_trans_id,
							      t412.c412_type ,
							      t412.c412_status_fl,
							      t413.c205_part_number_id ,
							      SUM(t413.c413_item_qty) c413_item_qty ,
							      SUM(t413.c413_item_price) c413_item_price ,
							      t5050.c901_status ,
							      NVL(t413.c413_control_number,'TBE') c413_control_number ,
							      --DECODE(t412.c1900_company_id,1001, DECODE(t413.c413_control_number,'TBE',NULL,t413.c413_control_number), NVL(t413.c413_control_number, 'TBE')) c413_control_number,
							      t413.c205_client_part_number_id
							    FROM t413_inhouse_trans_items t413,
							      t412_inhouse_transactions t412,
							      t5050_invpick_assign_detail t5050
							    WHERE t413.c412_inhouse_trans_id = p_trans_id
							    AND t413.c412_inhouse_trans_id   = t412.c412_inhouse_trans_id
							    AND t413.c412_inhouse_trans_id   = t5050.c5050_ref_id(+)
							    AND t412.c412_void_fl           IS NULL
							    AND t413.c413_void_fl           IS NULL
							    AND t5050.c5050_void_fl(+)      IS NULL
							    AND t412.c5040_plant_id          = v_plant_id
							    GROUP BY t413.c412_inhouse_trans_id,t412.c1900_company_id,
							      t412.c412_type ,
							      t412.c412_status_fl,
							      t413.c205_part_number_id ,
							      t5050.c901_status ,
							      t413.c413_control_number ,
							      t413.c205_client_part_number_id
							    ) t413
							  FULL OUTER JOIN
							    (SELECT t5055.c5055_REF_ID ,
							      t5055.c205_part_number_id ,
							      SUM(t5055.c5055_qty) c5055_qty ,
							       NVL(t5055.c5055_control_number,'TBE') c5055_control_number ,
							      --DECODE(t5055.c1900_company_id,1001, DECODE(t5055.c5055_control_number,'TBE',NULL,t5055.c5055_control_number), NVL(t5055.c5055_control_number, 'TBE')) c5055_control_number,
							      t5055.c901_ref_type ,
							      t5055.c5055_error_dtls ,
							      t5055.C901_WAREHOUSE_TYPE ,
							      t5055.c5052_location_id
							    FROM t5055_control_number t5055
							    WHERE t5055.c5055_ref_id = p_trans_id
							    AND t5055.c901_ref_type  = p_trans_type
							   -- AND t5055.c901_type      = p_loc_oper
							    AND t5055.c901_type      = DECODE(v_inhouse_status,'4','93345',v_loc_opr)
							    AND t5055.c5055_void_fl IS NULL
							    AND t5055.c5040_plant_id = v_plant_id
							    GROUP BY t5055.c5055_REF_ID ,t5055.c1900_company_id,
							      t5055.c205_part_number_id ,
							      t5055.c5055_control_number ,
							      t5055.c901_ref_type ,
							      t5055.c5055_error_dtls ,
							      t5055.C901_WAREHOUSE_TYPE ,
							      t5055.c5052_location_id
							    ) t5055
							  ON t413.c412_inhouse_trans_id = t5055.c5055_ref_id
							  AND t413.c205_part_number_id  = t5055.c205_part_number_id
							  AND (
								  CASE
								  WHEN c5055_control_number IS NOT NULL AND c413_control_number IS NOT NULL THEN
								    NVL(t413.c413_control_number,'TBE')
								  ELSE
								    NVL(c5055_control_number,'TBE')
								  END) = NVL(c5055_control_number,'TBE')
								--act_pick_qty used to avoid duplicate rows			  
								--rec_cnt used to identify whether consignment loaded first time in the screen and exclude from actual pick qty validation
							  )WHERE  ( CASE WHEN rec_cnt != 0 THEN
									          act_pick_qty
									       ELSE
									          1
									       END ) != 0 )t413										  
							ORDER BY t413.ID;
							
		ELSIF v_table = 'CONSIGNMENT'
		THEN
			SELECT c504_status_fl
				 INTO v_consign_status
				FROM t504_consignment
				 WHERE c504_consignment_id = p_trans_id
			     AND c504_type             = p_trans_type; 
			     
			 SELECT DECODE(GET_RULE_VALUE(v_comp_id,'ITEMEDITREQFILTER'), null, v_comp_id, v_plant_id) into v_filter from dual;
			 
		
			OPEN p_out_dtl
			 FOR
				 SELECT   t5055.part_number ID   -- PART NUMBER
						, t5055.part_number pnum, t5055.reftype itemtype, t5055.pdesc pdesc, NVL (t5055.price, 0) price
                        , get_partnum_material_type(t5055.part_number) PARTMTRELTYPE									
						, NVL(GET_COUNT_PARTID_CONSIGN(p_trans_id, t5055.part_number), 0) qty, NVL (t5055.qty, 0) iqty
						, t5055.pickqty pickqty
						, t5055.cnum cnum
						, get_part_attribute_value (t5055.part_number, 92340) tagfl, t5055.location location
						, NVL(gm_pkg_op_item_control_rpt.get_initiated_location (t5055.part_number, p_trans_id, t5055.pickputfl) 
                        , gm_pkg_op_inv_scan.get_part_location (t5055.part_number, t5055.reftype,
                        DECODE(p_trans_type,'4110',t5055.c901_warehouse_type,'4112',t5055.c901_warehouse_type,
                        '111800',t5055.c901_warehouse_type,
                        '111801',t5055.c901_warehouse_type,
                        '111802',t5055.c901_warehouse_type,
                        '111803',t5055.c901_warehouse_type,
                        '111804',t5055.c901_warehouse_type,
                        '111805',t5055.c901_warehouse_type,
                        '90800'))) sugloc   --type must need to get rule values for pick- we need bulk
						,t5055.c901_warehouse_id warehouse_id, t5055.c901_warehouse_sh_name warehouse_sh_name, t5055.c901_warehouse_type warehouse_type	, t5055.errordtls errordtls			
					 FROM 
						 (--Distinct used to get unique record from above records from t505 and t5055
						 SELECT DISTINCT ref_id,  reftype,  part_number ,  pickputfl,  price,  qty ,  pdesc,  pickqty ,  cnum ,  location ,  errordtls ,
  							c901_warehouse_id ,  c901_warehouse_sh_name ,  c901_warehouse_type
							FROM
							(
			               SELECT NVL(t5055.c5055_ref_id, t505.c504_consignment_id ) ref_id
			               , NVL(t5055.c901_ref_type,t505.c504_type)  reftype
			               , NVL(t5055.c205_part_number_id, t505.c205_part_number_id) part_number
			               , DECODE (t505.c504_status_fl, '2', 'pick', '3', 'put', 'pick') pickputfl
			               , t505.c505_item_price price
			               , t505.C505_item_qty qty
			               , get_partnum_desc (NVL(t5055.c205_part_number_id, t505.c205_part_number_id)) pdesc
			               , DECODE (p_loc_oper
								, 93343, NVL (t5055.c5055_qty, NVL(t5055.c5055_qty,t505.c505_item_qty))
								, DECODE (t505.c504_status_fl, '4', t5055.c5055_qty, NVL(t5055.c5055_qty,t505.c505_item_qty))
								 ) pickqty
						   , DECODE (p_loc_oper
								, 93343, NVL (t5055.c5055_control_number, t505.c505_control_number)
								, t505.c505_control_number
								 ) cnum
			               , t5055.c5052_location_id location
			               , t5055.C901_WAREHOUSE_TYPE warehouse_type, t5055.c5055_error_dtls errordtls,NVL(t5055.c5055_qty,0) act_pick_qty,
			               ( SELECT  COUNT(1) 
				                   FROM t5055_control_number t5055A 
				                  where t5055a.c5055_ref_id = p_trans_id
				                ) rec_cnt
			               , DECODE(p_trans_type 
                        		,'4110',gm_pkg_op_inv_warehouse.get_warehouse_id(NVL(nvl(t5055.C901_WAREHOUSE_TYPE,t505.C901_WAREHOUSE_TYPE),'90800')) 
                 				,'4112',gm_pkg_op_inv_warehouse.get_warehouse_id(NVL(nvl(t5055.C901_WAREHOUSE_TYPE,t505.C901_WAREHOUSE_TYPE),'90800')) 
                 				,'111800',gm_pkg_op_inv_warehouse.get_warehouse_id(NVL(nvl(t5055.C901_WAREHOUSE_TYPE,t505.C901_WAREHOUSE_TYPE),'90800'))
                 				,'111801',gm_pkg_op_inv_warehouse.get_warehouse_id(NVL(nvl(t5055.C901_WAREHOUSE_TYPE,t505.C901_WAREHOUSE_TYPE),'90800'))
                 				,'111802',gm_pkg_op_inv_warehouse.get_warehouse_id(NVL(nvl(t5055.C901_WAREHOUSE_TYPE,t505.C901_WAREHOUSE_TYPE),'90800'))
                 				,'111803',gm_pkg_op_inv_warehouse.get_warehouse_id(NVL(nvl(t5055.C901_WAREHOUSE_TYPE,t505.C901_WAREHOUSE_TYPE),'90800'))
                 				,'111804',gm_pkg_op_inv_warehouse.get_warehouse_id(NVL(nvl(t5055.C901_WAREHOUSE_TYPE,t505.C901_WAREHOUSE_TYPE),'90800'))
                 				,'111805',gm_pkg_op_inv_warehouse.get_warehouse_id(NVL(nvl(t5055.C901_WAREHOUSE_TYPE,t505.C901_WAREHOUSE_TYPE),'90800'))
                 				,1) c901_warehouse_id 
            					, DECODE(p_trans_type 
                 				,'4110',gm_pkg_op_inv_warehouse.get_warehouse_sh_name(NVL(nvl(t5055.C901_WAREHOUSE_TYPE,t505.C901_WAREHOUSE_TYPE),'90800'),NULL) 
                 				,'4112',gm_pkg_op_inv_warehouse.get_warehouse_sh_name(NVL(nvl(t5055.C901_WAREHOUSE_TYPE,t505.C901_WAREHOUSE_TYPE),'90800'),NULL) 
                 				,'111800',gm_pkg_op_inv_warehouse.get_warehouse_sh_name(NVL(nvl(t5055.C901_WAREHOUSE_TYPE,t505.C901_WAREHOUSE_TYPE),'90800'),NULL) 
                 				,'111801',gm_pkg_op_inv_warehouse.get_warehouse_sh_name(NVL(nvl(t5055.C901_WAREHOUSE_TYPE,t505.C901_WAREHOUSE_TYPE),'90800'),NULL) 
                 				,'111802',gm_pkg_op_inv_warehouse.get_warehouse_sh_name(NVL(nvl(t5055.C901_WAREHOUSE_TYPE,t505.C901_WAREHOUSE_TYPE),'90800'),NULL) 
                 				,'111803',gm_pkg_op_inv_warehouse.get_warehouse_sh_name(NVL(nvl(t5055.C901_WAREHOUSE_TYPE,t505.C901_WAREHOUSE_TYPE),'90800'),NULL) 
                 				,'111804',gm_pkg_op_inv_warehouse.get_warehouse_sh_name(NVL(nvl(t5055.C901_WAREHOUSE_TYPE,t505.C901_WAREHOUSE_TYPE),'90800'),NULL) 
                 				,'111805',gm_pkg_op_inv_warehouse.get_warehouse_sh_name(NVL(nvl(t5055.C901_WAREHOUSE_TYPE,t505.C901_WAREHOUSE_TYPE),'90800'),NULL) 
                				 ,'FG') c901_warehouse_sh_name 
            					, DECODE(p_trans_type
                 				,'4110',nvl(t5055.C901_WAREHOUSE_TYPE,t505.C901_WAREHOUSE_TYPE) 
                 				,'4112',nvl(t5055.C901_WAREHOUSE_TYPE,t505.C901_WAREHOUSE_TYPE)
                 				,'111800',nvl(t5055.C901_WAREHOUSE_TYPE,t505.C901_WAREHOUSE_TYPE)
                 				,'111801',nvl(t5055.C901_WAREHOUSE_TYPE,t505.C901_WAREHOUSE_TYPE)
                 				,'111802',nvl(t5055.C901_WAREHOUSE_TYPE,t505.C901_WAREHOUSE_TYPE)
                 				,'111803',nvl(t5055.C901_WAREHOUSE_TYPE,t505.C901_WAREHOUSE_TYPE)
                 				,'111804',nvl(t5055.C901_WAREHOUSE_TYPE,t505.C901_WAREHOUSE_TYPE)
                 				,'111805',nvl(t5055.C901_WAREHOUSE_TYPE,t505.C901_WAREHOUSE_TYPE)
                 				,90800) c901_warehouse_type
                 				/*Using full outer join between T505 and T5055 to get the overall data then adding conditions in part number, 
                 				 * consignment id and control number
                 				 */
				           FROM (SELECT t505.c504_consignment_id,  SUM(t505.c505_item_price) c505_item_price,  t504.c504_status_fl ,
									  t505.c205_part_number_id ,
									  /*T505 stored control number as �TBE� until all the parts having valid control number for BBA,
									  so It is required below condition in t505 subquery*/
									 -- DECODE(t504.c1900_company_id,1001, DECODE( t505.C505_CONTROL_NUMBER,'TBE',NULL,t505.C505_CONTROL_NUMBER), NVL(t505.C505_CONTROL_NUMBER, 'TBE'))C505_CONTROL_NUMBER,
									 NVL(t505.C505_CONTROL_NUMBER, 'TBE') C505_CONTROL_NUMBER,
									  SUM(t505.c505_item_qty) c505_item_qty ,  t505.C901_WAREHOUSE_TYPE ,  t504.c504_type
									FROM t505_item_consignment t505 ,
									  t504_consignment t504
									WHERE t504.c504_consignment_id = p_trans_id
									AND t504.c504_consignment_id   = t505.c504_consignment_id
									AND t505.c505_void_fl         IS NULL
									AND t504.c504_void_fl         IS NULL
									AND (t504.c1900_company_id(+)  = v_filter
									OR t504.c5040_plant_id(+)      = v_filter)
									GROUP BY t504.c1900_company_id ,t505.c504_consignment_id ,  t505.c205_part_number_id ,  t505.c505_control_number,
									  t504.c504_status_fl,  t505.c901_warehouse_type,
									  t504.c504_type) t505 
									  FULL OUTER JOIN 
									(SELECT t5055.c5055_REF_ID ,  t5055.c205_part_number_id ,  SUM(t5055.c5055_qty) c5055_qty,
									  NVL(t5055.c5055_control_number,'TBE') c5055_control_number,  t5055.c901_ref_type ,
									  t5055.c5055_error_dtls ,  t5055.C901_WAREHOUSE_TYPE ,  t5055.c5052_location_id
									FROM t5055_control_number t5055
									WHERE t5055.c5055_ref_id = p_trans_id
									AND t5055.c901_ref_type  = p_trans_type
									--AND t5055.c901_type      = p_loc_oper
									AND t5055.c901_type      = DECODE(v_consign_status,'4','93345','93343')
									AND t5055.c5055_void_fl IS NULL
									AND (t5055.c1900_company_id(+)  = v_filter
									OR t5055.c5040_plant_id(+)      = v_filter)
									GROUP BY t5055.c5055_REF_ID ,
								      t5055.c205_part_number_id ,
								      t5055.c5055_control_number ,
								      t5055.c901_ref_type ,
								      t5055.c5055_error_dtls ,
								      t5055.C901_WAREHOUSE_TYPE ,
								      t5055.c5052_location_id
									) t5055 
				          					  ON
											  t505.c504_consignment_id = t5055.c5055_ref_id
				            				  AND t505.c205_part_number_id = t5055.c205_part_number_id
				            				  
											  AND (CASE
												  WHEN c5055_control_number IS NOT NULL AND c505_control_number IS NOT NULL THEN
												    NVL(c505_control_number,'TBE')
												  ELSE
												    NVL(c5055_control_number,'TBE')
												  END) = NVL(c5055_control_number,'TBE')
				            	--act_pick_qty used to avoid duplicate rows			  
								--rec_cnt used to identify whether consignment loaded first time in the screen and exclude from actual pick qty validation	          
				                ) t5055 WHERE ( CASE WHEN rec_cnt != 0 THEN
									          act_pick_qty
									       ELSE
									          1
									       END ) != 0
								 )t5055									
								 order by t5055.part_number;
		ELSIF v_table = 'ORDERS'
		THEN
		      SELECT c5040_plant_id
			   INTO v_trans_plant_id
			   FROM t501_order
			  WHERE c501_order_id = p_trans_id;
					  
			 IF v_trans_plant_id = 	v_plant_id THEN
				v_plant_id := v_trans_plant_id;
			 ELSE
				v_plant_id := NULL;
			 END IF;
			OPEN p_out_dtl
			 FOR
				 SELECT   t502.ID ID   -- PART NUMBER
									, t502.reftype itemtype, t502.pdesc pdesc, NVL (t502.price, 0) price
						, NVL(GET_COUNT_PARTID(p_trans_id,t502.ID), 0) qty
						,get_partnum_material_type(t502.ID) PARTMTRELTYPE
						, DECODE (p_loc_oper
								, 93343, NVL (t5055.pickqty, t502.qty)
								, DECODE (t502.statusfl, '4', t5055.pickqty, t502.qty)
								 ) pickqty
						, DECODE (p_loc_oper, 93343,DECODE(v_Ack_Fl,'Y',t502.c502_control_number,t5055.cnum), t502.c502_control_number) cnum
						, t5055.LOCATION LOCATION
						, NVL (gm_pkg_op_item_control_rpt.get_initiated_location (t502.ID, p_trans_id, t502.pickputfl)
							 , gm_pkg_op_inv_scan.get_part_location (t502.ID, t502.reftype)
							  --type must need to get rule values for pick- we need bulk
							  ) sugloc
						, 1 warehouse_id, 'FG' warehouse_sh_name, 90800 warehouse_type, t5055.errordtls errordtls
					 FROM (SELECT	t501.c501_order_id ref_id, p_trans_type reftype, t501.c501_status_fl statusfl
								  , t502.c205_part_number_id ID, SUM (t502.c502_item_qty) qty
								  , get_partnum_desc (t502.c205_part_number_id) pdesc
								  , 0 price   -- Price not required for the pick process
								  , DECODE (t501.c501_status_fl, '2', 'pick', '3', 'put', 'pick') pickputfl
								  , t502.c502_control_number
							   FROM t502_item_order t502, t501_order t501
							  WHERE t501.c501_order_id = p_trans_id
								AND t501.c501_order_id = t502.c501_order_id
								AND (t501.c1900_company_id = v_filter
			         				OR t501.c5040_plant_id   = v_filter) -- adding plant and company id filter for orders
								AND t502.c502_void_fl IS NULL
								AND NVL (c901_type, 99999) != 50301
								AND (t501.c901_ext_country_id IS NULL OR t501.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH
						   GROUP BY t501.c501_order_id
								  , t501.c501_status_fl
								  , t502.c205_part_number_id
								  , t502.c502_control_number) t502
						, (SELECT t5055.c5055_ref_id ref_id, t5055.c901_ref_type reftype, t5055.c5055_qty pickqty
								, t5055.c5055_control_number cnum, t5055.c5052_location_id LOCATION
								, t5055.c205_part_number_id part_number, t5055.c5055_error_dtls errordtls
							 FROM t5055_control_number t5055
							WHERE t5055.c901_ref_type = p_trans_type
							  AND t5055.c5055_ref_id = p_trans_id
							  AND t5055.c901_type = p_loc_oper
							  AND t5055.c5040_plant_id = v_plant_id
							  AND t5055.c5055_void_fl IS NULL) t5055
					WHERE t502.ref_id = t5055.ref_id(+)
					 AND t502.ID = t5055.part_number(+)
					 AND NVL (t502.c502_control_number, NVL (t5055.cnum, -9999)) = NVL (t5055.cnum, NVL (t502.c502_control_number, -9999)) 
                                                                ORDER BY t502.ID;

		END IF;
	END gm_fch_txn_detail;

		/*******************************************************
	  * Description : Procedure to fetch locations detail based on mapped part#
	  * author		: Yoga
	 *******************************************************/
	PROCEDURE gm_fch_item_loc_dtl (
		p_refid 	   IN		VARCHAR2
	  , p_numstr	   IN		VARCHAR2
	  , p_txntype	   IN		t5055_control_number.c901_ref_type%TYPE
	  , p_pickput_fl   IN		VARCHAR2
	  , p_release_fl   IN		VARCHAR2
	  , p_out_dtl	   OUT		TYPES.cursor_type
	)
	AS
		v_lcn_type	   VARCHAR2 (20);
		v_statusfl	   NUMBER := 0;
		v_warehouse_type   VARCHAR2(20):= NULL;
		v_inv_type	   VARCHAR2(20):= NULL;
		v_txn_type         t901_code_lookup.C901_code_id%TYPE;
		v_dist_id          T701_DISTRIBUTOR.C701_DISTRIBUTOR_ID%TYPE;
		v_div_id           V700_TERRITORY_MAPPING_DETAIL.DIVID%TYPE;
		TYPE v_wh_array    IS TABLE OF t5051_inv_warehouse.c901_warehouse_type%TYPE;
		wh_array           v_wh_array;
		v_rw_exists        char(1):='N';
		v_fg_exists        char(1):='N';
		v_country_code 	   VARCHAR2(10);
		v_redesig_trans	   T906_RULES.C906_RULE_VALUE%TYPE;
		v_plant_id     	t5040_plant_master.c5040_plant_id%TYPE;
		v_wh_skip_fl 	char(1):='N';
	BEGIN
	
		SELECT  NVL(get_plantid_frm_cntx(),get_rule_value('DEFAULT_PLANT','ONEPORTAL')) 
			  INTO  v_plant_id
			  FROM DUAL;
			  
		-- getting country code 
		SELECT GET_RULE_VALUE('CURRENT_DB','COUNTRYCODE')
		  INTO v_country_code
		  FROM DUAL;
		
		SELECT NVL(GET_RULE_VALUE('RE-DESIG PART',p_txntype),'N')
	      INTO v_redesig_trans
		  FROM DUAL;
		
		my_context.set_my_inlist_ctx (p_numstr);
		v_lcn_type	:= NVL (get_rule_value (p_txntype, p_pickput_fl), 93320);

	 v_dist_id := get_distributor_id_for_txn(p_refid); -- Get Distributor type (to find US/OUS)
 
   		SELECT  DECODE(p_txntype ,4112,'100824',111800,'100824',111801,'100824',111802,'100824',111803,'100824',111804,'100824',111805,'100824'
   				,get_distributor_division(v_dist_id))
          INTO v_div_id 
          FROM dual;
			 
        v_inv_type := get_rule_value('MINUS',p_txntype); -- Control / Pick process

		IF p_release_fl='ON' THEN -- OFF for control always
		    v_inv_type := get_rule_value('PLUS',p_txntype); -- Verify / PUT Process
	    END IF;

	--	v_warehouse_type := get_rule_value(v_inv_type,'WHQTYTYP');--Get the warehouse type of the transaction

        SELECT DECODE(p_txntype,'93341','90800','56010','56001', get_rule_value(v_inv_type,'WHQTYTYP')) --93341-FGRP,56010-RWRP
          INTO v_warehouse_type
          FROM dual; 
		
		IF v_warehouse_type IS NULL AND p_txntype not in ('4110','4112','111800','111801','111802','111803','111804','111805') 
		THEN
			v_warehouse_type := NVL(get_rule_value('TRANS_TYPE',p_txntype),'90800');
		END IF;

--	If the transaction order type is �50261� then get the order type based on the order id

  IF p_txntype = 50261 THEN
  	select DECODE(GET_ORDER_TYPE(p_refid),102080,'Y','N') into v_wh_skip_fl from dual; -- get order type for OUS distributor
  END IF;

		OPEN p_out_dtl
		 FOR
			 SELECT  DECODE(v_redesig_trans,'Y',PNUM_LIST.TO_PART,PNUM_LIST.FROM_PART) pnum, t5053.c5052_location_id locationid
			        ,DECODE(t5057.C5057_BUILDING_ID, NULL, '', t5057.C5057_BUILDING_SHORT_NAME || ':') ||(gm_pkg_op_inv_warehouse.get_warehouse_sh_name(NULL,T5052.C5051_INV_WAREHOUSE_ID) || ':' || NVL(t5052.c5052_location_cd,t5052.c5052_location_id)) locationcd   /* added building Id(building short name view ) PMT-33509 */
			        -- DECODE(v_warehouse_type,t5053.c5052_location_id,NULL,GM_PKG_OP_INV_WAREHOUSE.get_warehouse_sh_name(v_warehouse_type)||':'||t5053.c5052_location_id) locationid
					, gm_pkg_op_item_control_rpt.get_part_location_cur_qty (t5053.c205_part_number_id
																		  , t5053.c5052_location_id
																		  , p_refid
																		   ) currqty
					, t5053.c5053_location_part_map_id lcnmapid, NVL (t5053.c5053_max_qty, 0) maxqty
					, NVL (t5053.c5053_min_qty, 0) minqty, get_code_name (t5053.c901_last_transaction_type) lcntxntype, 
					GM_PKG_OP_INV_WAREHOUSE.get_warehouse_id(t5051.C901_WAREHOUSE_TYPE) warehouse_id,
					GM_PKG_OP_INV_WAREHOUSE.get_warehouse_sh_name(t5051.C901_WAREHOUSE_TYPE,null) warehouse_sh_name,
					t5051.C901_WAREHOUSE_TYPE warehouse_type,
					gm_pkg_pd_rpt_udi.get_part_di_number(t5053.c205_part_number_id)
				 FROM t5053_location_part_mapping t5053
				    , t5052_location_master t5052
					, t5051_inv_warehouse t5051
				    , t5057_building t5057
					, (SELECT vlist.token FROM_PART, NVL(in_house.TO_PNUM,vlist.token) TO_PART
						 FROM
							(select C205_PART_NUMBER_ID PNUM, NVL(t413.c205_client_part_number_id, C205_PART_NUMBER_ID) TO_PNUM
							   from  t413_inhouse_trans_items t413  
							  where t413.c412_inhouse_trans_id = p_refid
							   and c413_VOID_FL IS NULL  group by C205_PART_NUMBER_ID,c205_client_part_number_id) in_house
							, v_in_list vlist
								where vlist.token = in_house.PNUM(+) and vlist.token is not null) PNUM_LIST
				WHERE t5053.c205_part_number_id =PNUM_LIST.TO_PART
				  AND t5052.c5052_location_id = t5053.c5052_location_id
				  AND t5051.C5051_INV_WAREHOUSE_ID=t5052.C5051_INV_WAREHOUSE_ID
			      AND t5052.C5057_BUILDING_ID = t5057.C5057_BUILDING_ID(+)
			      AND t5052.C5040_PLANT_ID = t5057.C5040_PLANT_ID(+)
                  AND t5057.C5057_ACTIVE_FL(+) = 'Y'
                  AND t5057.C5057_VOID_FL IS NULL
				  AND t5052.c5051_inv_warehouse_id NOT IN
					(SELECT c906_rule_value
					   FROM t906_rules
					    WHERE c906_rule_id     = 'WHID'
					      AND c906_rule_grp_id = 'RULEWH'
					      AND c906_void_fl    IS NULL)--excluding FS and account warehouse
				  AND t5052.c5052_void_fl IS NULL
				  AND t5052.c901_status = 93310
				  AND t5051.C901_STATUS_ID =1 -- Active
				  AND t5052.c5040_plant_id = v_plant_id
				  AND t5051.C901_WAREHOUSE_TYPE = DECODE(v_wh_skip_fl,'Y',t5051.C901_WAREHOUSE_TYPE,DECODE(v_country_code,'en',
				  										DECODE(v_warehouse_type,NULL,DECODE(p_txntype,4110,DECODE(v_div_id,100824,t5051.C901_WAREHOUSE_TYPE,90800),
				  										4112,t5051.C901_WAREHOUSE_TYPE,
				  										111800,t5051.C901_WAREHOUSE_TYPE,
				  										111801,t5051.C901_WAREHOUSE_TYPE,
				  										111802,t5051.C901_WAREHOUSE_TYPE,
				  										111803,t5051.C901_WAREHOUSE_TYPE,
				  										111804,t5051.C901_WAREHOUSE_TYPE,
				  										111805,t5051.C901_WAREHOUSE_TYPE,
				  										90800),v_warehouse_type),
				  										t5051.C901_WAREHOUSE_TYPE))  -- For Consignment, no condition. If Order show FG Location else based on  v_warehouse_type, For OUS warehouse type is taking from t5051.C901_WAREHOUSE_TYPE
				  --AND t5051.C901_WAREHOUSE_TYPE = DECODE(v_warehouse_type,NULL,DECODE(p_txntype,4110,t5051.C901_WAREHOUSE_TYPE,90800),v_warehouse_type)  -- For Consignment,no condition. If Order show FG Location else based on  v_warehouse_type
			      ORDER BY c205_part_number_id ASC
					-- RW should come first then FG Locations for consignment
					-- for pick - bulk should come first.
					-- for put - shelf should come first.
					-- As Shelf type is 93320, during put,we want all 93320 on top,so doing order by asc.
					-- As Bulk type is 93336, during pick,we want all 93336 on top,so doing order by desc.
					, t5051.C901_WAREHOUSE_TYPE
					,  DECODE (v_lcn_type, 93320, t5052.c901_location_type) ASC
					, DECODE (v_lcn_type, 93336, t5052.c901_location_type) DESC
					, t5053.c5052_location_id;
	END gm_fch_item_loc_dtl;

	/*******************************************************
	  * Description : Procedure to fetch locations detail based on mapped part#
	  * author		: Gopinathan
	 *******************************************************/
	FUNCTION get_initiated_location (
		p_partnum	   IN	t5055_control_number.c205_part_number_id%TYPE
	  , p_refid 	   IN	t5055_control_number.c5055_ref_id%TYPE
	  , p_pickput_fl   IN	VARCHAR2
	)
		RETURN VARCHAR2
	IS
		v_loc_id	   VARCHAR2 (20);
	BEGIN
		SELECT t5055.c5052_location_id
		  INTO v_loc_id
		  FROM t5055_control_number t5055
		 WHERE t5055.c5055_ref_id = p_refid
		   AND t5055.c205_part_number_id = p_partnum
		   AND t5055.c901_type = DECODE (p_pickput_fl, 'pick', '93342', 'put', '93344')
		   --93342 PICK_INITIATED	93344 PUT_INITIATED
		   AND c5055_void_fl IS NULL;

		RETURN v_loc_id;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN NULL;
	END get_initiated_location;

	/*******************************************************
	 * Description : Procedure to validate the taggable parts
	 * author	   : Ritesh
	*******************************************************/
	PROCEDURE gm_fch_validate_tagpart (
		p_trans_id	   IN	t5055_control_number.c5055_ref_id%TYPE
	  , p_trans_type   IN	t5055_control_number.c901_ref_type%TYPE
	)
	AS
		v_isset 	   VARCHAR2 (1);
		v_cnt		   NUMBER;

		CURSOR c_cn_dtl
		IS
			SELECT t5055.c205_part_number_id pnum, t5055.c5055_control_number cnum
			  FROM t5055_control_number t5055
			 WHERE t5055.c5055_ref_id = p_trans_id
			   AND t5055.c901_ref_type = p_trans_type
			   AND t5055.c901_type = 93343	 -- pick transacted
			   AND t5055.c5055_void_fl IS NULL;
	BEGIN
		FOR c_dtl IN c_cn_dtl
		LOOP
			SELECT get_part_attribute_value (c_dtl.pnum, 92340)   -- Taggable Part
			  INTO v_isset
			  FROM DUAL;

			IF v_isset = 'Y'
			THEN
				SELECT COUNT (1)
				  INTO v_cnt
				  FROM t5010_tag t5010
				 WHERE t5010.c205_part_number_id = c_dtl.pnum
				   AND t5010.c5010_last_updated_trans_id = p_trans_id
				   AND t5010.c5010_control_number = c_dtl.cnum
				   AND t5010.c5010_void_fl IS NULL;

				IF v_cnt = 0
				THEN
					-- Error message is TagID cannot be blank, please enter TagID.
					raise_application_error (-20912, '');
				END IF;
			END IF;
		END LOOP;
	END gm_fch_validate_tagpart;

	/*******************************************************
		 * Description : function overload to return current quantiy of part in a given warehouse location and
		 * also subtracts the already allocated qty.
		 * author	   : Yoga
	*******************************************************/
	FUNCTION get_part_location_cur_qty (
		p_pnum		 IN   t5053_location_part_mapping.c205_part_number_id%TYPE
	  , p_location	 IN   t5053_location_part_mapping.c5052_location_id%TYPE
	  , p_whtype    IN         t5051_inv_warehouse.C901_WAREHOUSE_TYPE%TYPE
	  , p_refid 	 IN   VARCHAR2 DEFAULT NULL
	  , p_plantid	IN	  t5055_control_number.c5040_plant_id%TYPE DEFAULT NULL
	)
		RETURN NUMBER
	IS
		v_curqty	   NUMBER (20);
		v_plant_id     	t5040_plant_master.c5040_plant_id%TYPE;
	BEGIN
		IF p_plantid IS NULL
		THEN
		SELECT  NVL(get_plantid_frm_cntx(),get_rule_value('DEFAULT_PLANT','ONEPORTAL')) 
			  INTO  v_plant_id
			  FROM DUAL;
		ELSE
		v_plant_id:= p_plantid;
		END IF;
		
		SELECT NVL (loc.qty, 0) - NVL (picks.qty, 0)
		  INTO v_curqty
		  FROM (SELECT NVL (c5053_curr_qty, 0) qty
				  FROM t5053_location_part_mapping t5053, t5052_location_master t5052, t5051_inv_warehouse t5051
				 WHERE t5052.c5052_location_id = t5053.c5052_location_id
				   AND t5053.c205_part_number_id = p_pnum
				   AND t5053.c5052_location_id = p_location
				   AND t5052.c901_status = '93310'	 -- active
				   AND t5051.C5051_INV_WAREHOUSE_ID = t5052.C5051_INV_WAREHOUSE_ID
				   AND t5051.C901_STATUS_ID =1
				   AND C901_WAREHOUSE_TYPE = p_whtype
				   AND t5052.c5052_void_fl IS NULL
				   AND t5052.c5040_plant_id = v_plant_id) loc
			 , (SELECT NVL (SUM (t5055.c5055_qty), 0) qty
				  FROM t5055_control_number t5055
				 WHERE t5055.c205_part_number_id = p_pnum
				   AND t5055.c5052_location_id = p_location
				   AND t5055.c901_type = 93343	 -- pick transacted
				   AND t5055.c5055_void_fl IS NULL
				   AND t5055.c5055_ref_id NOT IN NVL (p_refid, '-99999')
				   AND t5055.c5055_update_inv_fl IS NULL
				   AND t5055.c5040_plant_id = v_plant_id) picks;

		RETURN v_curqty;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN 0;
	END get_part_location_cur_qty;

	/*******************************************************
		 * Description : function overload to return current quantiy of part in a location and
		 * also subtracts the already allocated qty.
		 * author	   : Gopinathan
	*******************************************************/
	FUNCTION get_part_location_cur_qty (
		p_pnum		 IN   t5053_location_part_mapping.c205_part_number_id%TYPE
	  , p_location	 IN   t5053_location_part_mapping.c5052_location_id%TYPE
	  , p_refid 	 IN   VARCHAR2 DEFAULT NULL
	  , p_plantid	IN	  t5055_control_number.c5040_plant_id%TYPE DEFAULT NULL
	)
		RETURN NUMBER
	IS
		v_curqty	   NUMBER (20);
		v_plant_id     	t5040_plant_master.c5040_plant_id%TYPE;
	BEGIN
		IF p_plantid IS NULL
		THEN
		SELECT  NVL(get_plantid_frm_cntx(),get_rule_value('DEFAULT_PLANT','ONEPORTAL')) 
			  INTO  v_plant_id
			  FROM DUAL;
		ELSE
		v_plant_id:= p_plantid;
		END IF;
		SELECT NVL (loc.qty, 0) - NVL (picks.qty, 0)
		  INTO v_curqty
		  FROM (SELECT NVL (c5053_curr_qty, 0) qty
				  FROM t5053_location_part_mapping t5053, t5052_location_master t5052
				 WHERE t5052.c5052_location_id = t5053.c5052_location_id
				   AND t5053.c205_part_number_id = p_pnum
				   AND t5053.c5052_location_id = p_location
				   AND t5052.c901_status = '93310'	 -- active
				   AND t5052.c5052_void_fl IS NULL
				   AND t5052.c5040_plant_id = v_plant_id) loc
			 , (SELECT NVL (SUM (t5055.c5055_qty), 0) qty
				  FROM t5055_control_number t5055
				 WHERE t5055.c205_part_number_id = p_pnum
				   AND t5055.c5052_location_id = p_location
				   AND t5055.c901_type = 93343	 -- pick transacted
				   AND t5055.c5055_void_fl IS NULL
				   AND t5055.c5055_ref_id NOT IN NVL (p_refid, '-99999')
				   AND t5055.c5055_update_inv_fl IS NULL
				   AND t5055.c5040_plant_id = v_plant_id) picks;

		RETURN v_curqty;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN 0;
	END get_part_location_cur_qty;
	
/***********************************************************************************************
	 * Description : To fetch the FG Bin Id's corresponding to the transaction id to the screen
	 * author	   : arajan
	************************************************************************************************/	
	PROCEDURE gm_fch_fgbin_dtls (
		p_trans_id	   IN	t907c_fgbin_trans_detail.c907c_ref_id%TYPE
	  , p_FGBin_str	   OUT	CLOB
	)
	AS
	BEGIN
		-- This will fetch the FG Bin ids with ',' seperated, Orderby is used to get the FG Bin ids in the order that we saved	
		SELECT (RTRIM(XMLAGG(XMLELEMENT(E,c907c_bin_id || '|')).EXTRACT('//text()').GETCLOBVAL(),'|') )
	    	INTO p_FGBin_str 
        FROM (SELECT c907c_bin_id FROM T907C_FGBIN_TRANS_DETAIL 
	        WHERE c907c_ref_id = p_trans_id 
		    AND c907c_void_fl IS NULL
		    ORDER BY c907c_fgbin_trans_detail_id
		    );
	
	END gm_fch_fgbin_dtls;

	/*******************************************************
	  * Description : Function to fetch the location cd
	  * author		: arajan
	 *******************************************************/
	FUNCTION get_location_cd (
		p_partnum	   	IN	t5055_control_number.c205_part_number_id%TYPE
	  , p_refid 	   	IN	t5055_control_number.c5055_ref_id%TYPE
	  , p_pickput_fl   	IN	VARCHAR2
	  , p_txntype		IN	t5055_control_number.c901_type%TYPE
	  , p_whtype		IN	t5055_control_number.c901_warehouse_type%TYPE
	  , p_control_no    IN  t5055_control_number.c5052_location_id%TYPE DEFAULT NULL
	)
		RETURN VARCHAR2
	IS
		v_tran_status	t906_rules.c906_rule_value%TYPE;
		v_location_cd	VARCHAR(2000);
		v_lcn_type		NUMBER;
		v_country_code 	VARCHAR(10);
		v_plant_id     	t5040_plant_master.c5040_plant_id%TYPE;
		
	BEGIN
		
		-- Getting the value from rules table based on the transaction type
		-- Based on the above value, need to decide the c901_type of  transaction, since it is different for different transactions 
		v_tran_status := NVL(get_rule_value(p_txntype,'LOCTRANSTS'),'N');
		--
		  SELECT NVL(get_plantid_frm_cntx(), get_rule_value('DEFAULT_PLANT','ONEPORTAL'))
	 		INTO v_plant_id
	 	  FROM DUAL;
	 	--
		BEGIN
			-- Need to get all the locations(comma seperated) if we are spitting the location while placing the parts to location
			-- Below query is to get the locations based on the ref id
			SELECT RTRIM(XMLAGG(XMLELEMENT(E,loc_cd || ', ')).EXTRACT('//text()').GETCLOBVAL(),', ') 
			INTO v_location_cd
			FROM
			(
				SELECT loc_cd
				FROM
			    (
					SELECT  t5052.C5052_LOCATION_CD loc_cd
		            FROM t5055_control_number t5055,t5052_location_master t5052 
		        	WHERE t5055.c5055_ref_id = p_refid --consignment id
		            AND t5055.c205_part_number_id = p_partnum
		            --93342 Pick Initiate, 93344 Put Initiate,93343 Pick Transacted,93345-Put Transacted
		            AND (t5055.c901_type = DECODE(v_tran_status,'Y',DECODE (p_pickput_fl, 'pick', '93342', 'put', '93344')) 
		            OR t5055.c901_type =DECODE(v_tran_status,'N', '93343') OR t5055.c901_type = DECODE(v_tran_status,'N', '93345'))
		            AND t5052.c5052_location_id = t5055.c5052_location_id
		            AND t5052.c5040_plant_id = v_plant_id
		            AND NVL(t5055.c5055_control_number,'-999') = DECODE (p_control_no, NULL, NVL(t5055.c5055_control_number,'-999'), p_control_no)
		            AND t5052.c5052_void_fl is null
		            AND c5055_void_fl IS NULL 
		            ORDER BY c901_type DESC
		        )
				GROUP by loc_cd
			);
		
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
			v_location_cd := NULL;
		END;
		
		-- If the location is null from above query, then need to fetch the suggested location for the part
		IF v_location_cd IS NULL
		THEN
			v_lcn_type := NVL (get_rule_value (p_txntype, 'LOCATION'), 93320);
			v_country_code := NVL(GET_RULE_VALUE('CURRENT_DB','COUNTRYCODE'),'en');
			  
				SELECT loc_cd
				INTO v_location_cd
				FROM
				(
					SELECT NVL(t5052.c5052_location_cd,t5052.c5052_location_id) loc_cd
				    FROM t5052_location_master t5052,
				      t5053_location_part_mapping t5053,
				      t5051_inv_warehouse t5051
				    WHERE t5052.c5052_location_id     = t5053.c5052_location_id
				    AND t5051.c5051_inv_warehouse_id  = t5052.c5051_inv_warehouse_id
				    AND t5051.c901_status_id          = 1
				    AND t5051.c5051_inv_warehouse_id NOT IN
					(SELECT c906_rule_value
					   FROM t906_rules
					    WHERE c906_rule_id     = 'WHID'
					      AND c906_rule_grp_id = 'RULEWH'
					      AND c906_void_fl    IS NULL)--excluding FS and account warehouse
				    AND t5051.c901_warehouse_type     = DECODE(v_country_code,'en',NVL(p_whtype,90800),NVL(p_whtype,90800))--90800 (FG) --56001 (RW),  GH-EDC Change
				    AND t5052.c901_status             = '93310' --93310: Active
				    AND t5053.c205_part_number_id     = p_partnum
				    AND t5052.c5040_plant_id = v_plant_id
				    AND NVL(t5053.c5053_curr_qty,0)  >= 0
				    AND t5052.c5052_void_fl          IS NULL
				    ORDER BY DECODE (v_lcn_type, 93320, t5052.c901_location_type) ASC ,
				      DECODE (v_lcn_type, 93336, t5052.c901_location_type) DESC ,
				      NVL(t5053.c5053_curr_qty,0) DESC   
				 )
				 WHERE ROWNUM = 1;
			
		END IF;
		
		RETURN 	v_location_cd;
		
	END get_location_cd;	
		
END gm_pkg_op_item_control_rpt;
/