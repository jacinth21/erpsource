/* Formatted on 2010/12/06 12:50 (Formatter Plus v4.8.0) */
-- @"c:\database\packages\operations\gm_pkg_op_dhr_loc.bdy"

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_dhr_loc 
IS

	/****************************************************************
	* Description : Log table to save the Location for DHR
	* Author      : Karthik Somanathan
	*****************************************************************/
	PROCEDURE gm_sav_manf_dhr_loc_log (
	        p_txn_id      IN T408_DHR.C408_DHR_ID%TYPE,
	        p_location_id IN T5052_LOCATION_MASTER.C5052_LOCATION_ID%TYPE,
			p_location_scan_dt IN T408_DHR.C408_LOCATION_SCAN_DT%TYPE,
			p_cnum		  IN T408_DHR.C408_CONTROL_NUMBER%TYPE,
			p_wo_id		  IN T408_DHR.C402_WORK_ORDER_ID%TYPE,
			p_pnum 		  IN T408_DHR.C205_PART_NUMBER_ID%TYPE,
	        p_user_id     IN T408B_DHR_LOC_LOG.C408B_LAST_UPDATED_BY%TYPE
	)
	AS
	BEGIN
	
		INSERT INTO T408B_DHR_LOC_LOG
		(C408_DHR_ID,
		 C5052_LOCATION_ID,
		 C408_LOCATION_SCAN_DT,
		 C408_CONTROL_NUMBER,
		 C402_WORK_ORDER_ID,
		 C205_PART_NUMBER_ID,
		 C408B_VOID_FL,
		 C408B_LAST_UPDATED_BY,
		 C408B_LAST_UPDATED_DATE
		 )
		VALUES(
		p_txn_id,
		p_location_id,
		p_location_scan_dt,
		p_cnum,
		p_wo_id,
		p_pnum,
		NULL,
		p_user_id,
		CURRENT_DATE
		);
	END gm_sav_manf_dhr_loc_log;

END gm_pkg_op_dhr_loc;
/
