--@"C:\Database\Packages\Operations\gm_pkg_op_ra_master.pkg"

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_ra_master
IS

 PROCEDURE gm_sav_ra_master (
      p_ra_id                  IN   t506_returns.c506_rma_id%TYPE,
      p_comments               IN   t506_returns.c506_comments%TYPE,
      p_invoice_id             IN   t506_returns.c503_invoice_id%TYPE,
      p_created_date           IN   t506_returns.c506_created_date%TYPE,
      p_type                   IN   t506_returns.c506_type%TYPE,
      p_distributor_id         IN   t506_returns.c701_distributor_id%TYPE,
      p_reason                 IN   t506_returns.c506_reason%TYPE,
      p_status_fl              IN   t506_returns.c506_status_fl%TYPE,
      p_created_by             IN   t506_returns.c506_created_by%TYPE,
      p_credit_memo_id         IN   t506_returns.c506_credit_memo_id%TYPE,
      p_expected_date          IN   t506_returns.c506_expected_date%TYPE,
      p_set_id                 IN   t506_returns.c207_set_id%TYPE,
      p_last_updated_date      IN   t506_returns.c506_last_updated_date%TYPE,
      p_last_updated_by        IN   t506_returns.c506_last_updated_by%TYPE,
      p_account_id             IN   t506_returns.c704_account_id%TYPE,
      p_credit_date            IN   t506_returns.c506_credit_date%TYPE,
      p_return_date            IN   t506_returns.c506_return_date%TYPE,
      p_update_inv_fl          IN   t506_returns.c506_update_inv_fl%TYPE,
      p_order_id               IN   t506_returns.c501_order_id%TYPE,
      p_qb_consignment_txnid   IN   t506_returns.qb_consignment_txnid%TYPE,
      p_reprocess_date         IN   t506_returns.c501_reprocess_date%TYPE,
      p_user_id                IN   t506_returns.c101_user_id%TYPE,
      p_ac_credit_dt           IN   t506_returns.c506_ac_credit_dt%TYPE,
      p_void_fl                IN   t506_returns.c506_void_fl%TYPE,
      p_parent_rma_id          IN   t506_returns.c506_parent_rma_id%TYPE,
      p_sales_rep_id           IN   t506_returns.c703_sales_rep_id%TYPE,
      p_associated_rma_id      IN   t506_returns.c506_associated_rma_id%TYPE
   )
   AS
   v_company_id   t1900_company.c1900_company_id%TYPE;
   v_plant_id 	  t5040_plant_master.c5040_plant_id%TYPE;
   BEGIN
	   
      SELECT get_compid_frm_cntx(), get_plantid_frm_cntx()
      INTO v_company_id, v_plant_id
      FROM DUAL; 
     
      UPDATE t506_returns
         SET c506_comments = p_comments,
             c503_invoice_id = p_invoice_id,
             c506_created_date = p_created_date,
             c506_type = p_type,
             c701_distributor_id = p_distributor_id,
             c506_reason = p_reason,
             c506_status_fl = p_status_fl,
             c506_created_by = p_created_by,
             c506_credit_memo_id = p_credit_memo_id,
             c506_expected_date = p_expected_date,
             c207_set_id = p_set_id,
             c506_last_updated_date = p_last_updated_date,
             c506_last_updated_by = p_last_updated_by,
             c704_account_id = p_account_id,
             c506_credit_date = p_credit_date,
             c506_return_date = p_return_date,
             c506_update_inv_fl = p_update_inv_fl,
             c501_order_id = p_order_id,
             qb_consignment_txnid = p_qb_consignment_txnid,
             c501_reprocess_date = p_reprocess_date,
             c101_user_id = p_user_id,
             c506_ac_credit_dt = p_ac_credit_dt,
             c506_void_fl = p_void_fl,
             c506_parent_rma_id = p_parent_rma_id,
             c703_sales_rep_id = p_sales_rep_id,
             c506_associated_rma_id = p_associated_rma_id
       WHERE c506_rma_id = p_ra_id AND c506_void_fl IS NULL;

      IF SQL%ROWCOUNT = 0
      THEN
         INSERT INTO t506_returns
                     (c506_rma_id, c506_comments, c503_invoice_id,
                      c506_created_date, c506_type, c701_distributor_id,
                      c506_reason, c506_status_fl, c506_created_by,
                      c506_credit_memo_id, c506_expected_date, c207_set_id,
                      c506_last_updated_date, c506_last_updated_by,
                      c704_account_id, c506_credit_date, c506_return_date,
                      c506_update_inv_fl, c501_order_id,
                      qb_consignment_txnid, c501_reprocess_date,
                      c101_user_id, c506_ac_credit_dt, c506_void_fl,
                      c506_parent_rma_id, c703_sales_rep_id,
                      c506_associated_rma_id, c1900_company_id, c5040_plant_id
                     )
              VALUES (p_ra_id, p_comments, p_invoice_id,
                      p_created_date, p_type, p_distributor_id,
                      p_reason, p_status_fl, p_created_by,
                      p_credit_memo_id, p_expected_date, p_set_id,
                      p_last_updated_date, p_last_updated_by,
                      p_account_id, p_credit_date, p_return_date,
                      p_update_inv_fl, p_order_id,
                      p_qb_consignment_txnid, p_reprocess_date,
                      p_user_id, p_ac_credit_dt, p_void_fl,
                      p_parent_rma_id, p_sales_rep_id,
                      p_associated_rma_id, v_company_id, v_plant_id
                     );
      END IF;
      
   END gm_sav_ra_master;
   
   /*procedure to save return attributes
    * 
    * 
    */
   PROCEDURE gm_sav_ra_attribute_maste (
      p_att_id      IN   t506a_returns_attribute.c506a_returns_attribute_id%TYPE,
      p_ra_id       IN   t506a_returns_attribute.c506_rma_id%TYPE,
      p_att_type    IN   t506a_returns_attribute.c901_attribute_type%TYPE,
      p_att_value   IN   t506a_returns_attribute.c506a_attribute_value%TYPE,
      p_user_id     IN   t506a_returns_attribute.c506a_last_updated_by%TYPE
   )
   AS
      v_attribute_id   t506a_returns_attribute.c506a_returns_attribute_id%TYPE;
   BEGIN
      UPDATE t506a_returns_attribute
         SET c506_rma_id = p_ra_id,
             c901_attribute_type = p_att_type,
             c506a_attribute_value = p_att_value,
             c506a_last_updated_by = p_user_id,
             c506a_last_updated_date = CURRENT_DATE
       WHERE c506a_returns_attribute_id = p_att_id AND c506a_void_fl IS NULL;

      IF SQL%ROWCOUNT = 0
      THEN
         SELECT s506a_returns_attribute.NEXTVAL
           INTO v_attribute_id
           FROM DUAL;

         INSERT INTO t506a_returns_attribute
                     (c506a_returns_attribute_id, c506_rma_id,
                      c901_attribute_type, c506a_attribute_value
                     )
              VALUES (v_attribute_id, p_ra_id,
                      p_att_type, p_att_value
                     );
      END IF;
   END gm_sav_ra_attribute_maste;
   
 END gm_pkg_op_ra_master;
 /