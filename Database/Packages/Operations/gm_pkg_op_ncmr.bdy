--@"c:\database\packages\operations\gm_pkg_op_ncmr.bdy"
CREATE OR REPLACE
PACKAGE BODY gm_pkg_op_ncmr
IS
    /*******************************
    * Description : Procedure to fetch all NCMR details for a particular part
    * Author : Mihir
    *************************************************/
PROCEDURE gm_fch_ncmr_email_det (
        p_partnum IN t409_ncmr.c205_part_number_id%TYPE,
        p_ncmr_email_cur OUT TYPES.cursor_type)
AS
v_company_id  t1900_company.c1900_company_id%TYPE;
v_plant_id T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
BEGIN
	SELECT	get_compid_frm_cntx(), get_plantid_frm_cntx()
	INTO	v_company_id, v_plant_id
	FROM	dual;
    OPEN p_ncmr_email_cur FOR SELECT t409.c409_ncmr_id ncmrid, get_vendor_name (t409.c301_vendor_id) vname, t409.c409_created_date cdate FROM t409_ncmr t409, t408_dhr t408 WHERE t409.c205_part_number_id =
    p_partnum AND t409.c408_dhr_id = t408.c408_dhr_id AND t408.c408_void_fl IS 
    NULL AND t409.c409_void_fl  IS NULL
    AND T409.c1900_company_id = v_company_id 
    AND T409.C5040_PLANT_ID = v_plant_id 
    ORDER BY t409.c409_created_date DESC;
END gm_fch_ncmr_email_det;
/***************************************************************************
*Description :To create GM-EVAL-XXXXX before creating NCMR
*Author : Himanshu
****************************************************************************/
PROCEDURE gm_create_eval_ncmr (
        p_dhrid          IN t409_ncmr.c408_dhr_id%TYPE,
        p_userid         IN t409_ncmr.c409_created_by%TYPE,
        p_rejreason_type IN t409_ncmr.c901_reason_type%TYPE,
        p_rejreason      IN t409_ncmr.c409_rej_reason%TYPE,
        p_ncmrid OUT VARCHAR2,
        p_message OUT VARCHAR2)
AS
    v_eval_id NUMBER;
    v_id_string t409_ncmr.c408_dhr_id%TYPE;
    v_string t409_ncmr.c408_dhr_id%TYPE;
    v_statu_fl NUMBER := 0;
    v_company_id  t1900_company.c1900_company_id%TYPE;
   v_plant_id T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
    CURSOR ncmr_eval_cur
    IS
         SELECT c205_part_number_id ID, c402_work_order_id woid, c301_vendor_id vid
           FROM t408_dhr
          WHERE c408_dhr_id = p_dhrid;
BEGIN
	  SELECT	get_compid_frm_cntx(), get_plantid_frm_cntx()
	INTO	v_company_id, v_plant_id
	FROM	dual;
     SELECT s409_ncmr_eval.NEXTVAL INTO v_eval_id FROM DUAL;
     SELECT DECODE ( (LENGTH (v_eval_id)), 1, LPAD (v_eval_id, 2, 0), v_eval_id)
       INTO v_id_string
       FROM DUAL;
     SELECT 'GM-EVAL-' || v_id_string INTO v_string FROM DUAL;
    p_ncmrid      := v_string;
    FOR ncmr_eval IN ncmr_eval_cur
    LOOP
         INSERT
           INTO t409_ncmr
            (
                c409_ncmr_id, c408_dhr_id, c402_work_order_id
              , c205_part_number_id, c301_vendor_id, c409_status_fl
              , c409_created_by, c409_created_date, c409_rej_reason
              , c901_reason_type, c409_eval_by, c409_eval_remarks
              , c409_eval_date , c901_type,c1900_company_id
			,C5040_PLANT_ID
            )
            VALUES
            (
                v_string, p_dhrid, ncmr_eval.woid
              , ncmr_eval.ID, ncmr_eval.vid, v_statu_fl
              , p_userid, CURRENT_DATE, p_rejreason
              , DECODE (p_rejreason_type, 0, NULL, p_rejreason_type), p_userid, p_rejreason
              , CURRENT_DATE , 51061  -- Eval Type
              , v_company_id
			  , v_plant_id
            ) ;
    END LOOP;
END gm_create_eval_ncmr;
/***************************************************************************
*Description :To create GM-EVAL-XXXXX before creating NCMR
*Author : Himanshu
****************************************************************************/
PROCEDURE gm_sav_ncmr
    (
        p_ncmrid  IN OUT t409_ncmr.c409_ncmr_id%TYPE,
        p_remarks IN t409_ncmr.c409_eval_remarks%TYPE,
        p_evaldt  IN VARCHAR2,
        p_user_id IN t409_ncmr.c409_eval_by%TYPE,
        p_type    IN VARCHAR2
    )
AS
    v_status_fl t409_ncmr.c409_status_fl%TYPE;
    v_id_string t409_ncmr.c408_dhr_id%TYPE;
    v_string t409_ncmr.c408_dhr_id%TYPE;
    v_dhr_id t409_ncmr.c408_dhr_id%TYPE;
    v_ncmr_id NUMBER;
     v_dateformat  varchar2(100);
BEGIN
	select get_compdtfmt_frm_cntx() into v_dateformat from dual;
     SELECT c409_status_fl
       INTO v_status_fl
       FROM t409_ncmr
      WHERE c409_ncmr_id  = p_ncmrid
        AND c409_void_fl IS NULL FOR UPDATE;
    IF v_status_fl       <> '0' THEN
        raise_application_error ( - 20934, '') ;
    END IF;
    IF p_type = 'E' THEN
         SELECT s409_ncmr.NEXTVAL INTO v_ncmr_id FROM DUAL;
         SELECT DECODE ( (LENGTH (v_ncmr_id)), 1, LPAD (v_ncmr_id, 2, 0), v_ncmr_id)
           INTO v_id_string
           FROM DUAL;
         SELECT 'GM-NCMR-' || v_id_string INTO v_string FROM DUAL;
        v_status_fl := 1;
    ELSIF p_type     = 'X' THEN
         SELECT c408_dhr_id
           INTO v_dhr_id
           FROM t409_ncmr
          WHERE c409_ncmr_id  = p_ncmrid
            AND c409_void_fl IS NULL FOR UPDATE;
         UPDATE t408_dhr
        SET c408_qty_rejected = 0, c408_last_updated_date = CURRENT_DATE, c408_last_updated_by = p_user_id
          WHERE c408_dhr_id   = v_dhr_id;
        v_status_fl          := 3;
    END IF;
   
    IF v_string IS NULL -- To Cancel the NCMR
    THEN
    	UPDATE t409_ncmr
    	SET c409_void_fl = 'Y', c409_last_updated_by = p_user_id, c409_last_updated_date = CURRENT_DATE
    	WHERE c409_ncmr_id = p_ncmrid
    	AND c409_void_fl IS NULL;
    ELSE
     UPDATE t409_ncmr
    SET c409_ncmr_id   = DECODE (v_string, NULL, p_ncmrid, v_string), c409_status_fl = v_status_fl, c409_last_updated_by
                       = p_user_id, c409_last_updated_date = CURRENT_DATE, c409_eval_remarks = p_remarks
      , c409_eval_date = TO_DATE (p_evaldt, v_dateformat), c409_eval_by = p_user_id, c901_type = DECODE (v_string, NULL
        , 51061, 51062)
      WHERE c409_ncmr_id = p_ncmrid;
    
    BEGIN
     SELECT C408_DHR_ID 
      INTO v_dhr_id 
     FROM T409_NCMR 
     WHERE c409_ncmr_id = v_string;
     
       EXCEPTION WHEN OTHERS THEN
        v_dhr_id:=NULL;
     END;
     
  /* 
   * Assigning the DHR Priority to (26240450: Priority 7) when a DHR Transaction is moved to NCMR transaction
   * If the current DHR Priority for the DHR Transaction is US Sales ( 106840: Priority 0) Keep the priority as "0"
   * PMT-20850 Author: gpalani
   * 
   */ 
         
      update t408_dhr SET 
       c901_dhr_priority=decode(c901_dhr_priority,106840,c901_dhr_priority,26240450) ,
       c408_last_updated_by=p_user_id,
       c408_last_updated_date=current_date
      where c408_dhr_id=v_dhr_id;
      
    END IF;
    p_ncmrid            := v_string;
END gm_sav_ncmr;
PROCEDURE gm_fch_eval_rem_email_det (
        p_recordset OUT TYPES.cursor_type)
AS
BEGIN
    OPEN p_recordset FOR SELECT t409.c409_ncmr_id evalid,
    t409.c205_part_number_id pnum,
    get_partnum_desc (t409.c205_part_number_id) pdesc,
    get_vendor_name (t409.c301_vendor_id) vname,
    t409.c408_dhr_id dhrid,
    t408.c408_qty_received dqty,
    t408.c408_qty_rejected nqty,
    t409.c409_rej_reason nres,
    get_proj_team_emails (t202.c202_project_id, t409.c301_vendor_id) to_email FROM t409_ncmr t409, --t409.c301_vendor_id added for PMT#51316
    t408_dhr t408,
    t205_part_number t205,
    t202_project t202,
    t101_user t101 WHERE TO_DATE (CURRENT_DATE - 7) >= TO_DATE (t409.c409_created_date) AND t409.c409_status_fl = 0 AND
    t409.c409_void_fl                          IS NULL AND t408.c408_dhr_id = t409.c408_dhr_id AND
    t409.c205_part_number_id                    = t205.c205_part_number_id AND t205.c202_project_id =
    t202.c202_project_id AND t101.c101_user_id  = t202.c202_created_by AND NVL (t409.c901_type, - 9999) != 51061
    ORDER BY t101.c101_email_id;
    --
END gm_fch_eval_rem_email_det;
PROCEDURE gm_fch_eval_email_det (
        p_ncmrid IN t409_ncmr.c409_ncmr_id%TYPE,
        p_recordset OUT TYPES.cursor_type)
AS
BEGIN
    OPEN p_recordset FOR SELECT t409.c409_ncmr_id evalid,
    t409.c205_part_number_id pnum,
    get_partnum_desc (t409.c205_part_number_id) pdesc,
    get_vendor_name (t409.c301_vendor_id) vname,
    t409.c408_dhr_id dhrid,
    t408.c408_qty_received dqty,
    t408.c408_qty_rejected nqty,
    t409.c409_rej_reason nres,
    get_proj_team_emails (t202.c202_project_id, t409.c301_vendor_id) to_email,  --t409.c301_vendor_id added for PMT#51316
    get_user_name(t409.C409_CREATED_BY) uname, 
    t409.c409_created_by userid,
    t202.c202_project_id projid,
    t202.c202_project_nm projnm
    FROM t409_ncmr t409,  
    t408_dhr t408,
    t205_part_number t205,
    t202_project t202,
    t101_user t101 WHERE t409.c409_ncmr_id = p_ncmrid AND t409.c409_void_fl IS NULL AND t409.c409_status_fl = 0 AND
    t408.c408_dhr_id                       = t409.c408_dhr_id AND t409.c205_part_number_id = t205.c205_part_number_id
    AND t205.c202_project_id               = t202.c202_project_id AND t101.c101_user_id = t202.c202_created_by ORDER BY
    t101.c101_email_id;
    --
END gm_fch_eval_email_det;
/******************************************************************
* created Himanshu
* Description : To change the NCMR rej qty
****************************************************************/
PROCEDURE gm_change_rej_qty (
        p_DHRId  IN T408_DHR.C408_DHR_ID%TYPE,
        P_QtyRej IN T408_DHR.C408_QTY_REJECTED%TYPE,
        p_UserId IN T408_DHR.C408_LAST_UPDATED_BY%TYPE)
AS
    v_status_fl NUMBER;
BEGIN
     SELECT t409.c409_status_fl
       INTO v_status_fl
       FROM T409_ncmr t409
      WHERE t409.C408_DHR_ID = p_DHRId
      AND t409.c409_void_fl IS NULL FOR UPDATE;
    IF v_status_fl           = '0' OR v_status_fl = '1' THEN
         UPDATE T408_DHR
        SET C408_QTY_REJECTED = P_QtyRej, C408_LAST_UPDATED_BY = p_UserId, C408_LAST_UPDATED_DATE = CURRENT_DATE
          WHERE C408_DHR_ID   = p_DHRId;
    ELSE
        raise_application_error ( - 20932, '') ;
    END IF;
END gm_change_rej_qty;
/***************************************************************************
*Description : To Rollback NCMR to status 1
*Author : VPrasath
****************************************************************************/
PROCEDURE gm_rollback_ncmr (
        p_ncmrid IN t409_ncmr.c409_ncmr_id%TYPE,
        p_UserId IN t408_DHR.C408_LAST_UPDATED_BY%TYPE)
AS
v_ncmr_sts_fl	NUMBER;
BEGIN
	
	    SELECT TO_NUMBER(t409.c409_status_fl)
       INTO v_ncmr_sts_fl     
       FROM t409_ncmr t409
      WHERE t409.c409_ncmr_id = p_ncmrid
      AND t409.c409_void_fl IS NULL; 
	
    IF v_ncmr_sts_fl >= 2 THEN
        gm_pkg_op_ncmr.gm_revert_ncmr_posting (p_ncmrid, p_UserId) ;
    END IF;
    
   --If NCMR status is 0 (EVAL), when rollback, should keep as 0
--   UPDATE t409_ncmr
--    SET  c409_status_fl = DECODE(v_ncmr_sts_fl, 0,0,1), c409_qty_rejected = NULL, c409_closeout_qty = NULL
--      , c409_closeout_date = NULL, c409_closeout_by = NULL, c409_disposition = NULL
--      , c409_rts_fl         = NULL, c409_disp_remarks = NULL, c409_disp_by = NULL
--      , c409_disp_date      = NULL, c409_responsibility = NULL, c409_rej_reason = NULL
--      , c901_reason_type    = NULL, c409_last_updated_by = p_userid, c409_last_updated_date = CURRENT_DATE
--      WHERE c409_ncmr_id    = p_ncmrid;
      
      UPDATE t409_ncmr
      SET c409_void_fl = 'Y', c409_last_updated_by = p_userid, c409_last_updated_date = CURRENT_DATE
      WHERE c409_ncmr_id    = p_ncmrid
      AND c409_void_fl IS NULL;
      
END gm_rollback_ncmr;
/***************************************************************************
*Description : To revert the NCMR Posting
*Author : VPrasath
****************************************************************************/
PROCEDURE gm_revert_ncmr_posting (
        p_ncmrid IN t409_ncmr.c409_ncmr_id%TYPE,
        p_userid IN t408_DHR.C408_LAST_UPDATED_BY%TYPE)
AS
    v_dhr_id t408_dhr.c408_dhr_id%TYPE;
    v_qty NUMBER := 0;
    v_control t408_dhr.c408_control_number%TYPE;
    v_qtyrec t408_dhr.c408_qty_received%TYPE;
    v_pnum t408_dhr.c205_part_number_id%TYPE;
    v_workordid t408_dhr.c402_work_order_id%TYPE;
    v_partyid t401_purchase_order.c301_vendor_id%TYPE;
    v_wo_uom_str VARCHAR2 (100) ;
    v_amt t402_work_order.c402_cost_price%TYPE;
    v_uomqty NUMBER;
    v_chkdhrvoidfl t408_dhr.c408_void_fl%TYPE;
    v_chkstatusfl   NUMBER;
    v_po_type       NUMBER;
    v_post_id       NUMBER;
    v_ncmr_sts_fl   NUMBER;
    v_close_out_qty NUMBER;
    v_qty_rejected  NUMBER;
    v_dhr_status	NUMBER;
    v_received_date	DATE;
    v_company_id t1900_company.c1900_company_id%TYPE;
    v_vendor_std_cost t302_vendor_standard_cost.c302_standard_cost%TYPE;
    v_vendor_cost_type t301_vendor.c901_vendor_cost_type%TYPE;
    v_purchase_amt t820_costing.c820_purchase_amt%TYPE;
BEGIN
     SELECT TO_NUMBER(t409.c409_status_fl), NVL (t408.c408_qty_rejected, 0), NVL (t409.c409_closeout_qty, 0)
      , t408.c408_dhr_id, TO_NUMBER(t408.c408_status_fl), c408_received_date
       INTO v_ncmr_sts_fl, v_qty_rejected, v_close_out_qty
      , v_dhr_id, v_dhr_status, v_received_date
       FROM t409_ncmr t409, t408_dhr t408
      WHERE t409.c409_ncmr_id = p_ncmrid
        AND t408.c408_dhr_id  = t409.c408_dhr_id;
        
    v_qty                    := v_qty_rejected - v_close_out_qty;
       
    if v_qty > 0 THEN
    
     SELECT c408_void_fl, TO_NUMBER (c408_status_fl), c408_control_number
      , c408_qty_received, c205_part_number_id, c402_work_order_id
      , c1900_company_id
       INTO v_chkdhrvoidfl, v_chkstatusfl, v_control
      , v_qtyrec, v_pnum, v_workordid
      , v_company_id
       FROM t408_dhr       
      WHERE c408_dhr_id = v_dhr_id;
      
     SELECT b.c301_vendor_id, get_wo_uom_type_qty (c402_work_order_id), NVL (DECODE (b.c401_type, 3104, c.c205_cost,
        a.c402_cost_price), a.c402_cost_price), b.c401_type,
         gm_pkg_pur_vendor_cost.get_vendor_cost_type (b.c301_vendor_id)
       INTO v_partyid, v_wo_uom_str, v_amt
      , v_po_type, v_vendor_cost_type
       FROM t402_work_order a, t401_purchase_order b, t205_part_number c
      WHERE c402_work_order_id     = v_workordid
        AND a.c401_purchase_ord_id = b.c401_purchase_ord_id
        AND a.c205_part_number_id  = c.c205_part_number_id;
        
    IF (v_po_type = 3104 OR v_po_type = 3105) THEN
        v_post_id                 := 48157;
    ELSE
        v_post_id := 48158;
    END IF;
    
    -- Only Regular PO - get the Standard Cost
    --106561 Standard Cost
    IF NVL(v_vendor_cost_type, '-999') = '106561' AND v_po_type = 3100
    THEN
       	SELECT gm_pkg_pur_vendor_cost.get_vendor_std_cost (v_partyid, v_pnum)
       		INTO v_vendor_std_cost
       	FROM DUAL;
       	--
   		v_amt := v_vendor_std_cost; 
   		-- getting the posting id (Standard Cost)  - code changed for this PMT-20749
   		-- 26240428 Std Cost Revert NCMR Rejection
   		v_post_id := 26240428;
   	END IF;	-- end vendor cost type
   		
    v_uomqty := TO_NUMBER (NVL (SUBSTR (v_wo_uom_str, INSTR (v_wo_uom_str, '/') + 1), 1)) ;
    -- to get the purchase amount
    v_purchase_amt := (get_currency_conversion (get_vendor_currency (v_partyid), get_comp_curr(v_company_id), v_received_date, v_amt) / v_uomqty);
    -- to pass the owner and local cost information
    gm_save_ledger_posting (v_post_id, CURRENT_DATE, v_pnum, v_partyid, p_ncmrid, v_qty, v_purchase_amt, p_userid, NULL, v_company_id, v_purchase_amt) ;
END IF;

END gm_revert_ncmr_posting;

/****************************************************************
 * Description : Procedure to get Ticket details for Evaluation id
 * Author : Vinoth
 *****************************************************************/
PROCEDURE gm_fch_eval_tkt_dtls (
	p_eval_id 	IN	t409_ncmr.c409_ncmr_id%TYPE,
	p_dhr_id	IN  t408_dhr.c408_dhr_id%TYPE,
	p_proj_id	IN  t202_project.c202_project_id%TYPE,
	p_role		IN	t204_project_team.c901_role_id%TYPE,
	p_create_tkt_fl	OUT	VARCHAR2,
    p_out OUT TYPES.cursor_type)
AS
v_company_id	t1900_company.c1900_company_id%TYPE;
v_plant_id	t5040_plant_master.c5040_plant_id%TYPE;
v_po_type	t401_purchase_order.c401_type%TYPE;
v_tkt_fl	VARCHAR2(1);
BEGIN
	
	BEGIN
	SELECT c1900_company_id, c5040_plant_id 
	  INTO v_company_id, v_plant_id
	  FROM t409_ncmr 
	 WHERE c409_ncmr_id = p_eval_id 
	   AND c409_void_fl IS NULL;
	 EXCEPTION WHEN OTHERS THEN
	 	RETURN;
	 END;
	
	--set company id and plant id in context   
	gm_pkg_cor_client_context.gm_sav_client_context(v_company_id,v_plant_id);
	
	v_po_type := get_po_type(p_dhr_id);
	
	SELECT NVL(get_rule_value_by_company(v_po_type, 'JIRAPOTYP',v_company_id),'N') INTO v_tkt_fl FROM DUAL;
	
	--Project assignee fetch is not required per business request and all the tickets will be un-assigned. Commented the below code for future use
	
	-- IF v_tkt_fl = 'Y' 
	-- THEN
	--   gm_pkg_pd_staffing_info.gm_fch_staffingreport(p_proj_id, p_role,'0','0','','',p_out);
	-- END IF;
	
	p_create_tkt_fl := v_tkt_fl;
	
END gm_fch_eval_tkt_dtls;

/*********************************************************************
 * Description : Procedure to get NCMR JIRA Ticket Id for Evaluation id
 * Author : Vinoth
 *********************************************************************/
PROCEDURE gm_fch_ticket_id (
   p_eval_id    IN     t409_ncmr.c409_ncmr_id%TYPE,
   p_out        OUT    t409_ncmr.c409_jira_ticket_id%TYPE)
AS
v_out 	t409_ncmr.c409_jira_ticket_id%TYPE;
BEGIN
	
	BEGIN
	SELECT c409_jira_ticket_id 
	  INTO v_out
	  FROM t409_ncmr 
	 WHERE c409_ncmr_id = p_eval_id;
	EXCEPTION WHEN OTHERS THEN
	v_out := NULL;
	END;
	
	p_out := v_out;
	   
END gm_fch_ticket_id;

/***********************************************************************
 * Description : Procedure to save NCMR JIRA Ticket Id for Evaluation id
 * Author : Vinoth
 ***********************************************************************/
PROCEDURE gm_sav_ncmr_jira_ticket (
   p_ticket_id  IN   t409_ncmr.c409_jira_ticket_id%TYPE,
   p_eval_id    IN   t409_ncmr.c409_ncmr_id%TYPE,
   p_user       IN   t409_ncmr.c409_created_by%TYPE)
AS
BEGIN
	
	UPDATE t409_ncmr 
	   SET c409_jira_ticket_id = p_ticket_id,
	       c409_last_updated_by = p_user,
	       c409_last_updated_date = SYSDATE
	 WHERE c409_ncmr_id = p_eval_id
	   AND c409_void_fl IS NULL;
	   
END gm_sav_ncmr_jira_ticket;

END gm_pkg_op_ncmr;
/
