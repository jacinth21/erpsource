--@"C:\Database\Packages\Operations\gm_pkg_op_split_process.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_split_process
IS

	/******************************************************************************
	 * Author: Agilan Singaravel
 	 * Procedure: gm_update_order_type
 	 * Description: This procedure is used to get sterlie part shelf qty from 
 	 * 				Heathrow-UK plant and based on acailable qty update order type in t501
	 ******************************************************************************/
   procedure gm_update_order_type(
   		p_order_id	IN	t501_order.c501_order_id%TYPE,
   		p_plant_id	IN	t5040_plant_master.c5040_plant_id%TYPE,
   		p_user_id   IN  t501_order.c501_created_by%TYPE
   )
   AS
   v_part_number   t205_part_number.c205_part_number_id%type;
   v_avail_qty     NUMBER;
   BEGIN
	   BEGIN
		   SELECT t502.c205_part_number_id
		     INTO v_part_number
		     FROM t502_item_order t502,t501_order t501
		    WHERE t501.c501_order_id = t502.c501_order_id
		      AND t502.c501_order_id = p_order_id
		      AND t501.c901_order_type = 2525 --Back Order
		      AND t501.c501_void_fl is null
		      AND t502.c502_void_fl is null;
		      
	   EXCEPTION WHEN OTHERS THEN
	   		v_part_number := null;
	   END;
	   
	   IF v_part_number IS NOT NULL THEN
	   		v_avail_qty := get_quantity_in_stock_byplant(v_part_number,p_plant_id);
	   
	   	IF v_avail_qty > 0 THEN
	   		UPDATE t501_order t501
			   SET t501.c901_order_type =  null    -- Bill & Ship
			     , t501.c501_status_fl  = '1'
				 , t501.c501_last_updated_by = p_user_id
				 , t501.c501_last_updated_date = SYSDATE
			 WHERE t501.c501_order_id = p_order_id 
	     	   AND c501_void_fl IS NULL;
	   	ELSE
	   		UPDATE t501_order t501
			   SET t501.c901_order_type =  2525 -- Back Order
			     , t501.c501_status_fl  = '0'
				 , t501.c501_last_updated_by = p_user_id
				 , t501.c501_last_updated_date = SYSDATE
			 WHERE t501.c501_order_id = p_order_id 
	     	   AND c501_void_fl IS NULL;
	    END IF;
	   END IF;
	   
   END gm_update_order_type;
   
END gm_pkg_op_split_process;
/