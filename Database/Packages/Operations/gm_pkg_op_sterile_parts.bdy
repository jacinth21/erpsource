--@"C:\Database\Packages\Operations\gm_pkg_op_sterile_parts.bdy";

/************************************************************************************
* Description  	: Procedure to fetch Sterile parts detail based on the order id input
* Author   		: T.S. Ramachandiran
*************************************************************************************/

CREATE OR REPLACE
PACKAGE BODY gm_pkg_op_sterile_parts
IS
PROCEDURE gm_fch_sterile_part_order(
		p_order_id IN t501_order.c501_order_id%TYPE,
        p_out_used_lot OUT TYPES.cursor_type)
AS
BEGIN
	OPEN p_out_used_lot 
	FOR 
		SELECT t502.C205_PART_NUMBER_ID PNUM
		FROM t501_order t501, T502_ITEM_ORDER t502, T205_PART_NUMBER t205
		WHERE t501.C501_ORDER_ID =  t502.C501_ORDER_ID
		AND t205.C205_PART_NUMBER_ID = t502.C205_PART_NUMBER_ID
		AND t205.C205_PRODUCT_CLASS='4030' -- Sterile Parts
		AND t501.C501_ORDER_ID = p_order_id
	    AND T501.C501_VOID_FL is NULL
		AND t502.C502_VOID_FL IS NULL;
END gm_fch_sterile_part_order;

/******************************************************************************************
* Description  	: Procedure to fetch Sterile parts detail based on the consigment id input
* Author   		: T.S. Ramachandiran
*******************************************************************************************/

PROCEDURE gm_fch_sterile_part_consignment(
		p_consig_id IN T504_CONSIGNMENT.C504_CONSIGNMENT_ID%TYPE,
        p_out_used_lot OUT TYPES.cursor_type)
AS
BEGIN
	OPEN p_out_used_lot 
	FOR 
		  SELECT t505.C205_PART_NUMBER_ID PNUM
		    FROM t504_consignment t504,t505_item_consignment t505, T205_PART_NUMBER t205
		   WHERE t504.C504_CONSIGNMENT_ID = t505.C504_CONSIGNMENT_ID
			 AND t205.C205_PART_NUMBER_ID = t505.C205_PART_NUMBER_ID
			 AND t205.C205_PRODUCT_CLASS='4030' -- Sterile Parts
			 AND t504.C504_CONSIGNMENT_ID = p_consig_id
			 AND t504.C504_VOID_FL IS NULL
			 AND t505.C505_VOID_FL IS NULL;

END gm_fch_sterile_part_consignment;
END gm_pkg_op_sterile_parts;
/