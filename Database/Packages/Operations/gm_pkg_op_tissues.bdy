--@"C:\Database\Packages\Operations\gm_pkg_op_tissues.bdy"

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_tissues
IS
/************************************************************************************
* Description  	: Procedure to check whether the parts in the order are tissue parts
* Author   		: Vinoth
*************************************************************************************/
FUNCTION get_order_tissue_cnt (
    p_order_id     IN   t501_order.c501_order_id%TYPE
  , p_shipto	   IN	t907_shipping_info.c901_ship_to%TYPE
  , p_addressid    IN	t907_shipping_info.c106_address_id%TYPE
)
    RETURN NUMBER
IS
    v_part_count   NUMBER := 0;
    v_address_type t106_address.c901_address_type%TYPE;
BEGIN
	BEGIN
		SELECT c901_address_type INTO v_address_type 
		  FROM t106_address
		 WHERE c106_address_id = p_addressid
		   AND c106_void_fl IS NULL;
	EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
            v_address_type := NULL;
    END; 
	
	--4122, 26240675 - Hospital
    IF p_shipto = 4122 OR v_address_type = 26240675 THEN
    	v_part_count := 0;
    ELSE
    BEGIN
       SELECT COUNT (pnum) cnt
        INTO v_part_count
        FROM (SELECT t502.c501_order_id, v205.c205_part_number_id pnum
                FROM t502_item_order t502,
                     V205_PARTS_WITH_EXT_VENDOR v205,
                      t501_order t501
               WHERE t502.c501_order_id = p_order_id        
                 AND v205.c205_part_number_id = t502.c205_part_number_id
                 AND t501.c501_order_id       = t502.c501_order_id
 				 AND t501.c1900_company_id    = v205.c1900_company_id
                 AND t502.c502_void_fl IS NULL
                 AND  T502.C901_TYPE = 50300  --Sales Consignment
                 AND t502.c502_delete_fl IS NULL);
    EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
            RETURN 0;
    END;
    
	END IF;
	RETURN v_part_count;
	
END get_order_tissue_cnt;

/**************************************************************
* Description  	: Procedure to check whether the sets are tissue
* Author   		: Vinoth
***************************************************************/
FUNCTION get_tissue_set_fl (
    p_set_id	IN	t526_product_request_detail.C207_SET_ID%TYPE
  , p_shipto	IN	t907_shipping_info.c901_ship_to%TYPE DEFAULT NULL
  , p_addressid IN	t907_shipping_info.c106_address_id%TYPE DEFAULT NULL
)
    RETURN t207_set_master.C207_TISSUE_FL%TYPE
IS
    v_tissue_fl		VARCHAR2(1) := 'N';
    v_address_type	t106_address.c901_address_type%TYPE;
BEGIN
	
	BEGIN
		SELECT c901_address_type INTO v_address_type 
		  FROM t106_address
		 WHERE c106_address_id = p_addressid
		   AND c106_void_fl IS NULL;
	EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
            v_address_type := NULL;
    END; 
    
    --4122, 26240675 - Hospital
	IF p_shipto = 4122 OR v_address_type = 26240675 THEN
    	v_tissue_fl := 'N';
    ELSE
    BEGIN
       SELECT c207_tissue_fl
        INTO v_tissue_fl
        FROM t207_set_master
       WHERE c207_set_id = p_set_id
         AND c207_void_fl IS NULL;
    EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
            v_tissue_fl := 'N';
    END;
    
    IF v_tissue_fl IS NULL THEN
    	v_tissue_fl := 'N';
    END IF;
    END IF;
    
    RETURN v_tissue_fl;
END get_tissue_set_fl;

END gm_pkg_op_tissues;
/
