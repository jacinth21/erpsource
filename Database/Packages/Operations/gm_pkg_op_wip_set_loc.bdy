/* Formatted on 2010/12/06 12:50 (Formatter Plus v4.8.0) */
-- @"c:\database\packages\operations\gm_pkg_op_wip_set_loc.bdy"

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_wip_set_loc 
IS

	/****************************************************************
	* Description : Log table to save the Location for the consignment
	* Author      : Karthik Somanathan
	*****************************************************************/
	PROCEDURE gm_sav_wip_set_loc_log (
	        p_txnid       IN t504_consignment.c504_consignment_id%TYPE,
	        p_location_id IN T5052_LOCATION_MASTER.C5052_LOCATION_ID%TYPE,
			p_status_fl	  IN t504_consignment.C504_STATUS_FL%TYPE,
			p_verify_fl	  IN t504_consignment.C504_VERIFY_FL%TYPE,
			p_type 		  IN t504_consignment.C504_TYPE%TYPE,
	        p_user_id     IN T504F_SET_CONSIGNMENT_LOC_LOG.c504f_LAST_UPDATED_BY%TYPE
	)
	AS
	BEGIN
	
		INSERT INTO T504F_SET_CONSIGNMENT_LOC_LOG
		(C504_CONSIGNMENT_ID,
		 C5052_LOCATION_ID,
		 C504_STATUS_FL,
		 C504_VERIFY_FL,
		 C504_TYPE,
		 c504f_VOID_FL,
		 C504F_LAST_UPDATED_BY,
		 c504f_LAST_UPDATED_DATE
		 )
		VALUES(
		p_txnid,
		p_location_id,
		p_status_fl,
		p_verify_fl,
		p_type,
		NULL,
		p_user_id,
		CURRENT_DATE
		);
	END gm_sav_wip_set_loc_log;

END gm_pkg_op_wip_set_loc;
/
