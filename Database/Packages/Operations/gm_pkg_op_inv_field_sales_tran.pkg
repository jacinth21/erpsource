/* Formatted on 2011/03/10 11:33 (Formatter Plus v4.8.0) */
--@"C:\Database\Packages\Operations\gm_pkg_op_inv_field_sales_tran.pkg"
CREATE OR REPLACE
PACKAGE gm_pkg_op_inv_field_sales_tran
IS
    /***********************************************************************************
    *Description : Procedure used to update the field sales warehouse details.
    *
    **************************************************************************************/
PROCEDURE gm_update_fs_inv (
        p_trans_id     IN t5053_location_part_mapping.c5053_last_update_trans_id%TYPE,
        p_source_table IN VARCHAR2,
        p_ref_id       IN t5052_location_master.c5052_ref_id%TYPE,
        p_txn_type     IN t5053_location_part_mapping.c901_last_transaction_type%TYPE,
        p_action       IN t5053_location_part_mapping.c901_action%TYPE,
        p_type         IN t5053_location_part_mapping.c901_type%TYPE,
        p_user_id      IN t5052_location_master.c5052_created_by%TYPE,
        p_req_type     IN VARCHAR2 DEFAULT NULL
        ) ;


    /***********************************************************************************
    *Description : Procedure used to update the consign Qty to FS warehouse.
    *
    **************************************************************************************/
PROCEDURE gm_update_fs_inv_consign (
        p_consignid   IN t505_item_consignment.c504_consignment_id%TYPE,
        p_ref_id      IN t5052_location_master.C5052_REF_ID%TYPE,
        p_location_id IN t5052_location_master.C5052_LOCATION_ID%TYPE,
        p_txn_type    IN t5053_location_part_mapping.c901_last_transaction_type%TYPE,
        p_action      IN t5053_location_part_mapping.c901_action%TYPE,
        p_type        IN t5053_location_part_mapping.c901_type%TYPE,
        p_user_id     IN t5052_location_master.c5052_created_by%TYPE) ;
    /***********************************************************************************
    *Description : Procedure used to update the returns Qty to FS warehouse.
    *
    **************************************************************************************/
PROCEDURE gm_update_fs_inv_returns (
        p_rma_id      IN t507_returns_item.c506_rma_id%TYPE,
        p_ref_id      IN t5052_location_master.c5052_ref_id%TYPE,
        p_location_id IN t5052_location_master.c5052_location_id%TYPE,
        p_txn_type    IN t5053_location_part_mapping.c901_last_transaction_type%TYPE,
        p_action      IN t5053_location_part_mapping.c901_action%TYPE,
        p_type        IN t5053_location_part_mapping.c901_type%TYPE,
        p_user_id     IN t5052_location_master.c5052_created_by%TYPE) ;
    /***********************************************************************************
    *Description : Procedure used to update the Sales Qty to FS warehouse.
    *
    **************************************************************************************/
PROCEDURE gm_update_fs_inv_sales (
        p_orderid     IN t502_item_order.c501_order_id%TYPE,
        p_ref_id      IN t5052_location_master.c5052_ref_id%TYPE,
        p_location_id IN t5052_location_master.c5052_location_id%TYPE,
        p_txn_type    IN t5053_location_part_mapping.c901_last_transaction_type%TYPE,
        p_action      IN t5053_location_part_mapping.c901_action%TYPE,
        p_type        IN t5053_location_part_mapping.c901_type%TYPE,
        p_user_id     IN t5052_location_master.c5052_created_by%TYPE) ;
    /***********************************************************************************
    *Description : Procedure used to update the inventory Adj. Qty to FS warehouse.
    *
    **************************************************************************************/
PROCEDURE gm_update_fs_inv_adjust (
        p_trans_id    IN t8900_ous_acct_trans.c8900_ous_acct_trans_id%TYPE,
        p_ref_id      IN t5052_location_master.c5052_ref_id%TYPE,
        p_location_id IN t5052_location_master.c5052_location_id%TYPE,
        p_txn_type    IN t5053_location_part_mapping.c901_last_transaction_type%TYPE,
        p_action      IN t5053_location_part_mapping.c901_action%TYPE,
        p_type        IN t5053_location_part_mapping.c901_type%TYPE,
        p_user_id     IN t5052_location_master.c5052_created_by%TYPE) ;
    /***********************************************************************************
    *Description : Procedure to insert or update to t5052 location master table.
    *
    **************************************************************************************/
PROCEDURE gm_sav_fs_inv_location (
        p_warehouseid IN T5051_INV_WAREHOUSE.C5051_INV_WAREHOUSE_ID%TYPE,
        p_ref_id      IN t5052_location_master.C5052_REF_ID%TYPE,
        p_location_cd IN t5052_location_master.c5052_location_cd%TYPE,
        p_type        IN t5052_location_master.c901_location_type%TYPE,
        p_status      IN t5052_location_master.c901_status%TYPE,
        p_user_id     IN t5052_location_master.c5052_created_by%TYPE,
        p_location_id OUT t5052_location_master.c5052_location_id%TYPE) ;
    /***********************************************************************************
    *Description : Procedure to update or insert to t5053 part mapping table.
    *
    **************************************************************************************/
PROCEDURE gm_sav_fs_inv_loc_part_details (
        p_location_id IN t5052_location_master.c5052_location_id%TYPE,
        p_trans_id    IN t5053_location_part_mapping.c5053_last_update_trans_id%TYPE,
        p_part_num    IN t5053_location_part_mapping.c205_part_number_id%TYPE,
        p_qty         IN t5053_location_part_mapping.c5053_curr_qty%TYPE,
        p_txn_type    IN t5053_location_part_mapping.c901_last_transaction_type%TYPE,
        p_action      IN t5053_location_part_mapping.c901_action%TYPE,
        p_type        IN t5053_location_part_mapping.c901_type%TYPE,
        p_created_dt  IN t5053_location_part_mapping.c5053_created_date%TYPE,
        p_user_id     IN t5052_location_master.c5052_created_by%TYPE,
        p_control_no  IN t5053_location_part_mapping.C5053_LAST_TXN_CONTROL_NUMBER%TYPE DEFAULT NULL);        
    /***********************************************************************************
	*Description :  Updating location id in master table  - Distributor and Account
	*
	**************************************************************************************/
PROCEDURE gm_update_fs_wh_inv_location (
        p_warehouseid       IN T5051_INV_WAREHOUSE.C5051_INV_WAREHOUSE_ID%TYPE,
        p_location_id       IN t5052_location_master.C5052_LOCATION_ID%TYPE,
        p_ref_id            IN t5052_location_master.C5052_REF_ID%TYPE,
        p_location_type     IN t5052_location_master.c901_location_type%TYPE,
        p_user_id     IN t5052_location_master.c5052_created_by%TYPE);
        
        
        
PROCEDURE gm_sav_loaner_lot (
          p_trans_id    IN t5053_location_part_mapping.c5053_last_update_trans_id%TYPE
         ,p_user_id     IN t5052_location_master.c5052_created_by%TYPE);
        
   /***********************************************************************************
*Description :  All Returned Loaners and consignment from Field sales will call this proc to adjust warehouse
warehouse
*
**************************************************************************************/
PROCEDURE gm_update_fs_inv_inhouse (
        p_trans_id    IN t505_item_consignment.c504_consignment_id%TYPE,
        p_ref_id      IN t5052_location_master.C5052_REF_ID%TYPE,
        p_location_id IN t5052_location_master.C5052_LOCATION_ID%TYPE,
        p_txn_type    IN t5053_location_part_mapping.c901_last_transaction_type%TYPE,
        p_action      IN t5053_location_part_mapping.c901_action%TYPE,
        p_type        IN t5053_location_part_mapping.c901_type%TYPE,
        p_user_id     IN t5052_location_master.c5052_created_by%TYPE);     
        
    /***********************************************************************************
    *Description : Function used to get the field sales location id.
    *
    **************************************************************************************/
    FUNCTION get_fs_location_id (
            p_warehouseid IN T5051_INV_WAREHOUSE.C5051_INV_WAREHOUSE_ID%TYPE,
            p_ref_id      IN t5052_location_master.C5052_REF_ID%TYPE,
            p_type        IN t5052_location_master.c901_location_type%TYPE,
            p_company_id  IN t1900_company.c1900_company_id%TYPE)
        RETURN VARCHAR2;
    END gm_pkg_op_inv_field_sales_tran;
    /
