--@Database\Packages\Operations\gm_pkg_op_process_inhouse_txn.bdy
CREATE OR REPLACE PACKAGE BODY gm_pkg_op_process_inhouse_txn
IS
/******************************************************************************************************
	* Description : Procedure to save inhouse transaction.
*******************************************************************************************************/
PROCEDURE gm_sav_inhouse_request (
	    p_request_for		  IN	   t520_request.c520_request_for%TYPE
	  , p_request_to		  IN	   t520_request.c520_request_to%TYPE
	  , p_purpose			  IN	   t520_request.c901_purpose%TYPE
	  , p_inputstring		  IN 	   CLOB
	  , p_status_fl 		  IN	   t520_request.c520_status_fl%TYPE
	  , p_userid			  IN	   t520_request.c520_created_by%TYPE
	  , p_ship_to			  IN	   t520_request.c901_ship_to%TYPE
	  , p_request_source	  IN	   t520_request.c901_request_source%TYPE
	  , p_ship_to_id		  IN	   t520_request.c520_ship_to_id%TYPE
	  , p_out_request_id	  OUT	   t520_request.c520_request_id%TYPE
	  , p_out_consign_id	  OUT	   t504_consignment.c504_consignment_id%TYPE
	)
AS
      v_partnum	   t205_part_number.c205_part_number_id%TYPE;
		v_shelfqty	   t208_set_details.c208_set_qty%TYPE;
		v_reqqty	   t208_set_details.c208_set_qty%TYPE;
		v_child_material_request_bool BOOLEAN := FALSE;
		v_consign_first_time_flag BOOLEAN := FALSE;
		v_account_id   t520_request.c520_request_to%TYPE :='01';
		v_out_item_consign_id t505_item_consignment.c505_item_consignment_id%TYPE;
		v_out_request_detail_id t521_request_detail.c521_request_detail_id%TYPE;
		v_backorderqty t208_set_details.c208_set_qty%TYPE;
		v_backorder_request_id t520_request.c520_request_id%TYPE;
		v_strlen	   NUMBER := NVL (LENGTH (p_inputstring), 0);
		v_string	   CLOB := p_inputstring;
		v_substring    VARCHAR2 (1000);
		v_part_price   NUMBER;
		v_allocate_fl  VARCHAR2 (1);
		v_ref_type varchar2(20);
		v_back_order_master_req_id  t520_request.c520_request_id%TYPE;
		
		v_strwhtype		T505_ITEM_CONSIGNMENT.C901_WAREHOUSE_TYPE%TYPE;
		 v_left 		NUMBER := 0;
		 
		 v_part_string  CLOB;
		 v_ship_string  CLOB;
		 v_ship_substring VARCHAR2(100);
		 v_ship_carrier VARCHAR2(20);
		 v_ship_mode    VARCHAR2(20);
		 v_consign_id    t504_consignment.c504_consignment_id%TYPE;
		 v_company_id    t1900_company.c1900_company_id%TYPE;
		 v_plant_id 	 t5040_plant_master.c5040_plant_id%TYPE;
		 v_status        NUMBER := 10;--10-- back order status
	
BEGIN
	

	SELECT GET_COMPID_FRM_CNTX() , GET_PLANTID_FRM_CNTX()
      	  INTO v_company_id, v_plant_id
           FROM DUAL;
          
      	--3. Creating MR since we have to create it in any case
		IF v_strlen > 0
		THEN     
       IF INSTR (v_string, '~') <> 0 THEN
				v_part_string := SUBSTR (v_string, 1, INSTR (v_string, '~') - 1);
				v_ship_string := SUBSTR (v_string, INSTR (v_string, '~') + 1);
				v_string := v_part_string;
			ELSE
				v_string := v_string;
			END IF;		

			IF INSTR (v_ship_string, '|') <> 0 THEN
				v_ship_substring := SUBSTR (v_ship_string, 1, INSTR (v_ship_string, '|') - 1);
				v_ship_carrier := SUBSTR (v_ship_substring, 1, INSTR (v_ship_substring, '^') - 1);
				v_ship_substring := SUBSTR (v_ship_substring, INSTR (v_ship_substring, '^') + 1);
				v_ship_mode := v_ship_substring;
			END IF;
           
         WHILE INSTR (v_string, '|') <> 0
			LOOP
				--
				v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
				v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
				--
				v_partnum	:= NULL;
				v_reqqty	:= NULL;
				--
				v_partnum	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_reqqty	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_strwhtype := v_substring;
				
				IF v_strwhtype = 56001 THEN
					  v_shelfqty    := get_qty_in_inv(v_partnum,'56001');
				ELSE 
					v_shelfqty := NVL(GET_QTY_IN_INVENTORY(v_consign_id,v_strwhtype,v_partnum),0);
					
				END IF;
				--Below code is for inhouse item consignment US only
					IF v_shelfqty < 0
				THEN
					v_shelfqty	:= 0;
				END IF;
	
				--	-- 4. Since we do orderby on bulkqty and the first bulkqty is > 0, we initiate a consignment
					IF (NOT v_consign_first_time_flag AND (v_shelfqty > 0 ))
				THEN
					gm_pkg_op_request_master.gm_sav_request (NULL
														   , NULL
														   , p_request_source --p_request_source  50618= Customerservce
														   , NULL
														   , NULL
														   , p_request_for
														   , p_request_to
														   , 50625   --50625 Employee Code Id  p_request_by_type to
														   , p_userid --p_request_by
														   , p_ship_to -- 4123 Employee Code Id  
														   , p_ship_to_id
														   , NULL
														   , 15
														   , p_userid
														   , p_purpose
														   , p_out_request_id
														   , NULL
														);
		
				gm_pkg_op_request_master.gm_sav_consignment (p_request_to
															   , NULL
															   , NULL
															   , NULL
															   , NULL
															   , '2'   -- Status Flag (Pending Control
															   , NULL
															   , v_account_id
															   , p_ship_to
															   , NULL
															   , p_ship_to_id
															   , NULL
															   , p_purpose
															   , p_request_for
															   , v_ship_carrier -- Shipp Carrier
															   , v_ship_mode  -- Ship Mode
															   , NULL
															   , '1'   --Ship Req Flag
															   , NULL
															   , NULL
															   , NULL
															   , NULL
															   , NULL
															   , NULL
															   , p_out_request_id
															   , p_userid
															   , p_out_consign_id
																);
					v_consign_first_time_flag := TRUE;
					
			IF   v_backorder_request_id IS NOT NULL AND p_out_request_id IS NOT NULL AND v_back_order_master_req_id IS NULL
               THEN
                  UPDATE t520_request
                     SET c520_master_request_id = p_out_request_id
                       , c520_last_updated_by = p_userid
                       , c520_last_updated_date = SYSDATE
                   WHERE c520_request_id = v_backorder_request_id;

                  v_back_order_master_req_id := p_out_request_id;
               END IF;
					
				END IF;
						
				-- 5. If we have qty > 0 and shelf has qty lesser than req qty, then we are sure to create a backorder RQ
				IF (NOT v_child_material_request_bool AND (v_shelfqty) < v_reqqty)
				THEN
					gm_pkg_op_request_master.gm_sav_request (NULL
														   , NULL
														   , p_request_source --p_request_source  50618= Customerservce
														   , NULL
														   , NULL
														   , p_request_for
														   , p_request_to
														   , 50625   --50625 Employee Code Id  p_request_by_type-RQBYT
														   , p_userid
														   , p_ship_to -- 4123 Employee Code Id -CONSP
														   , p_ship_to_id
														   , p_out_request_id
														   , v_status
														   , p_userid
														   , p_purpose
														   , v_backorder_request_id
														   , NULL
															);
														
					-- for issue 4846 add this code for save sipping information
					IF ((p_out_request_id IS NULL) OR (p_out_request_id = ''))
					THEN
						p_out_request_id := v_backorder_request_id;
					END IF;

					v_child_material_request_bool := TRUE;
				END IF;--v_child_material_request_bool END IF
				
				v_part_price := get_part_price (v_partnum, 'C', v_company_id);

				IF TRIM (v_part_price) IS NULL
				THEN
					raise_application_error (-20901, v_partnum);
				END IF;
				
				IF v_shelfqty < 0
				THEN
					v_shelfqty	:= 0;
				END IF;
				
				-- 6. Consign the parts which has sufficient bulkqty
				IF (v_shelfqty )  >= v_reqqty
				THEN
				   	
						gm_pkg_op_request_master.gm_sav_consignment_detail (NULL
																	  , p_out_consign_id
																	  , v_partnum
																	  , 'TBE'
																	  , v_reqqty
																	  , v_part_price
																	  , NULL
																	  , NULL
																	  , NULL
																	  , v_strwhtype-- FG Warehouse
																	  , v_out_item_consign_id
																	   );
																	   
	
					
				END IF;
				--BAck order
				IF  (v_shelfqty) < v_reqqty
				THEN
					-- 7. Calculate backorderQty and update request detail and item consignment accordingly
					v_backorderqty := v_reqqty - (v_shelfqty);
					
					-- update the table with the quantity. if there are same part in the request sum up the qty ordered
					UPDATE t521_request_detail
						SET c521_qty = NVL (c521_qty, 0) + v_backorderqty
					WHERE c520_request_id = v_backorder_request_id 
						AND c205_part_number_id = v_partnum;
					IF (SQL%ROWCOUNT = 0)
					THEN
						gm_pkg_op_request_master.gm_sav_request_detail (v_backorder_request_id
																	  , v_partnum
																	  , v_backorderqty
																	  , v_out_request_detail_id
																	   );
					END IF;
					
					IF (v_shelfqty) > 0
					THEN
                         v_left := v_reqqty;             
                 
                    IF v_shelfqty >0 AND v_shelfqty < v_left
				   	 THEN
				   	 v_left := v_shelfqty;
				   	END IF;
				   	 	 
				IF v_left >0 AND v_shelfqty >0 AND v_left >= v_shelfqty -- Checked shelf qty count
                         THEN
						v_left := v_shelfqty; 
					
							gm_pkg_op_request_master.gm_sav_consignment_detail (NULL
																		  , p_out_consign_id
																		  , v_partnum
																		  , 'TBE'
																		  , v_left
																		  , v_part_price
																		  , NULL
																		  , NULL
																		  , NULL
																		  , v_strwhtype -- FG Warehouse
																		  , v_out_item_consign_id
																		   );
							v_left := 0;																
						END IF;   	 	 
				   	 	 
		END IF;			
	END IF;
   END LOOP;	
	END IF;
    IF (p_out_consign_id IS NOT NULL)
		THEN
			-- Code added for allocation logic
			-- If the Transaction has Literature part, it should not be allocated to device.			
			v_allocate_fl := gm_pkg_op_inventory.chk_txn_details (p_out_consign_id, 93002);

			IF NVL (v_allocate_fl, 'N') = 'Y'
			THEN
			  select DECODE(p_request_for,111802,93009,111805,93009,93009) INTO v_ref_type from dual;
			  --THIS ONLY FOR FINISHED GOODS AND RESTICTRED WAREHOUSE TRANSACTION
				IF get_rule_value (p_request_for,'IH_INV_PICK_TYPE') = 'Y' THEN 
				  gm_pkg_allocation.gm_ins_invpick_assign_detail (v_ref_type, p_out_consign_id, p_userid);
				END IF;
			END IF;
			
			--When ever the child is updated, the consignment table has to be updated ,so ETL can Sync it to GOP.
			UPDATE t504_consignment
			   SET c504_last_updated_by   = p_userid
			     , c504_last_updated_date = SYSDATE
			 WHERE c504_consignment_id = p_out_consign_id;	
		END IF;     
END gm_sav_inhouse_request;
/******************************************************************************************************
	* Description : Procedure to update Shipping c907_active_fl 
*******************************************************************************************************/
PROCEDURE gm_update_shipping_active_fl(
    p_consign_id	IN	 t907_shipping_info.c907_ref_id%TYPE
  , p_userid	    IN	 t907_shipping_info.c907_last_updated_by%TYPE
)
AS
BEGIN
	UPDATE t907_shipping_info
	   SET c907_active_fl = 'N'
		 , c907_last_updated_by = p_userid
		 , c907_last_updated_date = CURRENT_DATE
   WHERE c907_ref_id = p_consign_id
    AND c907_void_fl IS NULL;
    
END gm_update_shipping_active_fl;
/******************************************************************************************************
	* Description : Procedure to update Shipping status as shipped(40).
*******************************************************************************************************/
PROCEDURE gm_update_shipping_status(
 p_consign_id	IN	 t907_shipping_info.c907_ref_id%TYPE
,p_trans_type   IN   t504_consignment.c504_type%TYPE
, p_userid	    IN	 t907_shipping_info.c907_last_updated_by%TYPE
)
AS

BEGIN		
	
	IF get_rule_value (p_trans_type,'IH_SHIP_FL') = 'Y' THEN
		UPDATE t907_shipping_info
               SET c907_status_fl = 40
                 , C907_TRACKING_NUMBER = 'Hand Delivered'
                 , c907_shipped_dt = TRUNC (CURRENT_DATE)
                 , c907_last_updated_by = p_userid
                 , c907_last_updated_date = CURRENT_DATE
             WHERE c907_ref_id = p_consign_id
             AND c907_void_fl IS NULL;
             gm_pkg_cm_shipping_trans.gm_sav_shipping_status_log (p_consign_id, '50181', p_userid, 'Shipped');
       END IF;
END gm_update_shipping_status;

        FUNCTION get_part_own_compid (
                P_part_number_id T2023_PART_COMPANY_MAPPING.c205_part_number_id%TYPE)
            RETURN VARCHAR2
        IS
            /************************************************************ 
            Description  : THIS FUNCTION RETURNS OWNER COMPANY ID
            Parameters   : P_part_number_id
            *************************************************************/
            v_comp_id T2023_PART_COMPANY_MAPPING.C1900_COMPANY_ID%TYPE;
        BEGIN
            BEGIN
                  SELECT NVL(C1900_COMPANY_ID,1000) INTO v_comp_id FROM T2023_PART_COMPANY_MAPPING 
                  WHERE C901_PART_MAPPING_TYPE = 105360
                    AND c205_part_number_id =    p_part_number_id
                     AND c2023_void_fl is null ;
            EXCEPTION
            WHEN NO_DATA_FOUND THEN
                v_comp_id := 1000;
            END;
            RETURN v_comp_id;
        END get_part_own_compid;
        

        FUNCTION GET_DIST_COMPID (
                P_DISTRIBUTOR_id T701_DISTRIBUTOR.C701_DISTRIBUTOR_ID%TYPE)
            RETURN VARCHAR2
        IS
            /***********************************************************
             Description  : THIS FUNCTION RETURNS OWNER COMPANY ID
            Parameters   : P_DISTRIBUTOR_id
            *************************************************************/
            v_comp_id T2023_PART_COMPANY_MAPPING.C1900_COMPANY_ID%TYPE;
        BEGIN
            BEGIN
            select c1900_parent_company_id INTO  v_comp_id  from t701_distributor where c701_distributor_id = P_DISTRIBUTOR_id and c701_void_fl is null;  
            EXCEPTION
            WHEN NO_DATA_FOUND THEN
                v_comp_id := 1000;
            END;
            RETURN v_comp_id;
        END GET_DIST_COMPID;

END gm_pkg_op_process_inhouse_txn;
/