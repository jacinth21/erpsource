/* Formatted on 2008/04/09 16:48 (Formatter Plus v4.8.5) */
-- @"c:\database\packages\operations\gm_pkg_op_lot_expiry_report.bdy"

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_lot_expiry_report
IS
--
   /*****************************************************************************************
     * Description : Procedure to fecth data for Lot Expiry dashboard based on sterile parts
     * Author      : Agilan Singaravel
     ****************************************************************************************/
--
  PROCEDURE gm_fch_lot_exp_by_range(
  		p_part_num		IN		t205_part_number.c205_part_number_id%type,
  		p_project_id	IN		t202_project.c202_project_id%type,
  		p_exclude_quar	IN		VARCHAR2,
  		p_exp_days		IN		VARCHAR2,
  		p_load_fl		IN		VARCHAR2,
  		p_like_search	IN		VARCHAR2,
  		p_expired_lots	OUT		CLOB
  )
  AS
  v_out_Details 		CLOB;
  v_out_heder_dtls 		CLOB;
  BEGIN

	  SELECT JSON_ARRAYAGG(JSON_OBJECT(
	  			 'PARTNUM' VALUE PARTNUM,
	   			 'PARTDESC' VALUE PARTDESC,
  				 'COUNT' VALUE sum(COUNT),
  				 'FLAG' VALUE p_load_fl,
  				 'PROJECTID' VALUE p_project_id)
  			 ORDER BY PARTNUM RETURNING CLOB) INTO v_out_Details 
  	    FROM
	  	 	(SELECT t5080.c205_part_number_id partnum,
	   				REGEXP_REPLACE(t205.c205_part_num_desc,'[^ ,0-9A-Za-z]', '') partdesc,
  				  	count(distinct t5080.c2550_control_number) count
	    	   FROM T2550_PART_CONTROL_NUMBER t2550, T205_PART_NUMBER t205, T5080_LOT_TRACK_INV t5080, T202_PROJECT t202,V205_STERILE_PARTS v205
	          WHERE t5080.c205_part_number_id = t205.c205_part_number_id  
         		AND t5080.c2550_control_number = t2550.c2550_control_number
         		AND t5080.c205_part_number_id = v205.c205_part_number_id 
         		AND t205.c205_part_number_id = t2550.c205_part_number_id
         		AND t205.c202_project_id   = t202.c202_project_id
         		AND ((p_load_fl = 'expired' and t2550.c2550_expiry_date < sysdate) 
         	   		OR (p_load_fl = '30Days' and t2550.C2550_expiry_date  between sysdate+0 and sysdate+30) 
         	   		OR (p_load_fl = '60Days' and t2550.c2550_expiry_date  between sysdate+31 and sysdate+60)
         	   		OR (p_load_fl = '90Days' and t2550.c2550_expiry_date  between sysdate+61 and sysdate+90)
         	   		OR (p_load_fl = 'others' and t2550.c2550_expiry_date > sysdate+91))
      			AND ((p_exclude_quar = 'Y' and t5080.c901_warehouse_type not in (90813)) OR (p_exclude_quar = 'N' and t5080.c901_warehouse_type = t5080.c901_warehouse_type)) 
         		AND t202.c202_project_id= decode(p_project_id,'0',t202.c202_project_id,p_project_id) 
    			AND ((p_part_num IS NOT NULL AND t5080.c205_part_number_id like decode(p_like_search,'LIT',upper(p_part_num),
               				'LIKEPRE', upper(p_part_num||'%'),
               				'LIKESUF', upper('%'||p_part_num),
               				upper('%'||p_part_num||'%')))  
               		OR (p_part_num IS NULL))        		
    			AND t5080.c5080_void_fl IS NULL
    	   GROUP BY t5080.c205_part_number_id,t205.c205_part_num_desc
       UNION
    	   	 SELECT t5072.c205_part_number_id PARTNUM,
            		REGEXP_REPLACE(t205.c205_part_num_desc,'[^ ,0-9A-Za-z]', '') PARTDESC,
            		count(distinct t5072.c5072_control_number) COUNT    
        	   FROM T5072_SET_PART_LOT_QTY t5072,T205_PART_NUMBER t205,T2550_PART_CONTROL_NUMBER t2550,
        	   		T5070_SET_LOT_MASTER t5070,T5071_SET_PART_QTY t5071, T202_PROJECT t202, V205_STERILE_PARTS v205
              WHERE t5072.c205_part_number_id = t205.c205_part_number_id
                AND t5072.c205_part_number_id = v205.c205_part_number_id
            	AND t5072.c5072_control_number = t2550.c2550_control_number
            	AND t2550.c205_part_number_id = t205.c205_part_number_id
            	AND t5072.c5072_last_updated_txn_id = t5071.c5071_last_updated_txn_id
           		AND t5070.c5070_set_lot_master_id = t5071.c5070_set_lot_master_id
            	AND t5071.c5053_last_txn_control_number = t5072.c5072_control_number  --need to check
            	AND t5072.c5070_set_lot_master_id = t5070.c5070_set_lot_master_id
            	AND t205.c202_project_id   = t202.c202_project_id
            	AND ((p_load_fl = 'expired' and t2550.c2550_expiry_date < sysdate) 
         	   		OR (p_load_fl = '30Days' and t2550.C2550_expiry_date  between sysdate+0 and sysdate+30) 
         	   		OR (p_load_fl = '60Days' and t2550.c2550_expiry_date  between sysdate+31 and sysdate+60)
         	   		OR (p_load_fl = '90Days' and t2550.c2550_expiry_date  between sysdate+61 and sysdate+90)
         	   		OR (p_load_fl = 'others' and t2550.c2550_expiry_date > sysdate+91))
			    AND ((p_part_num IS NOT NULL AND t5072.c205_part_number_id like decode(p_like_search,'LIT',upper(p_part_num),
               				'LIKEPRE', upper(p_part_num||'%'),
               				'LIKESUF', upper('%'||p_part_num),
               				upper('%'||p_part_num||'%')))  
               		 OR (p_part_num IS NULL))  
                AND t202.c202_project_id= decode(p_project_id,'0',t202.c202_project_id,p_project_id) 
            	AND c5072_void_fl IS NULL
            	AND c5070_void_fl IS NULL
            	AND c5071_void_fl IS NULL
            	AND t5070.c901_txn_id = t5072.c5072_last_updated_txn_id
           GROUP BY t5072.c205_part_number_id,t205.c205_part_num_desc)t group by t.partnum,t.partdesc;
		  
     SELECT p_load_fl  
       INTO v_out_heder_dtls 
       FROM dual;
    
	p_expired_lots := v_out_Details || '^'|| v_out_heder_dtls;
	
  END gm_fch_lot_exp_by_range;
  
  /***************************************************************************************
   * Description : Procedure to fetch data for Lot Expiry Dashboard based on tissue Parts
   * Author      : Agilan Singaravel
  ****************************************************************************************/
  PROCEDURE gm_fch_tissue_lot_exp_by_range(
  		p_part_num		IN		t205_part_number.c205_part_number_id%type,
  		p_project_id	IN		t202_project.c202_project_id%type,
  		p_exclude_quar	IN		VARCHAR2,
  		p_load_fl		IN		VARCHAR2,
  		p_like_search	IN		VARCHAR2,
  		p_expired_lots	OUT		CLOB
  )
  AS
  v_out_Details 	CLOB;
  v_out_heder_dtls 	CLOB;
  BEGIN
	  
	   SELECT JSON_ARRAYAGG(JSON_OBJECT(
	  			 'PARTNUM' VALUE PARTNUM,
	   			 'PARTDESC' VALUE PARTDESC,
  				 'COUNT' VALUE sum(COUNT),
  				 'FLAG' VALUE p_load_fl,
  				 'PROJECTID' VALUE p_project_id)
  			 ORDER BY PARTNUM RETURNING CLOB) INTO v_out_Details 
  	    FROM  	    
	  	 	(SELECT t2550.c205_part_number_id partnum,
	   				REGEXP_REPLACE(t205.c205_part_num_desc,'[^ ,0-9A-Za-z]', '') partdesc,
  				  	count(distinct t2550.c2550_control_number) count
	    	   FROM T2550_PART_CONTROL_NUMBER t2550, T205_PART_NUMBER t205,  T202_PROJECT t202, V205_TISSUE_PARTS v205
	          WHERE v205.C205_PART_NUMBER_ID = t2550.C205_PART_NUMBER_ID
         		AND ((p_load_fl = 'expired' and t2550.c2550_expiry_date < sysdate) 
         	   		OR (p_load_fl = '30Days' and t2550.C2550_expiry_date  between sysdate+0 and sysdate+30) 
         	   		OR (p_load_fl = '60Days' and t2550.c2550_expiry_date  between sysdate+31 and sysdate+60)
         	   		OR (p_load_fl = '90Days' and t2550.c2550_expiry_date  between sysdate+61 and sysdate+90)
         	   		OR (p_load_fl = 'others' and t2550.c2550_expiry_date > sysdate+91))         		
         		AND t205.c205_part_number_id = t2550.c205_part_number_id
         		AND t205.c202_project_id   = t202.c202_project_id
      			AND ((p_exclude_quar = 'Y' and t2550.c901_last_updated_warehouse_type not in (90813)) OR (p_exclude_quar = 'N' and t2550.c901_last_updated_warehouse_type = t2550.c901_last_updated_warehouse_type)) 
         		AND t202.c202_project_id= decode(p_project_id,'0',t202.c202_project_id,p_project_id) 
         		AND t2550.c901_last_updated_warehouse_type not in ('108020','108000') --Adjustment and Sold Qty
    			AND ((p_part_num IS NOT NULL AND t2550.c205_part_number_id like decode(p_like_search,'LIT',upper(p_part_num),
               				'LIKEPRE', upper(p_part_num||'%'),
               				'LIKESUF', upper('%'||p_part_num),
               				upper('%'||p_part_num||'%')))  
               		  OR (p_part_num IS NULL))                 			
    	   GROUP BY t2550.c205_part_number_id,t205.c205_part_num_desc
       UNION ALL
    	   	 SELECT t5072.c205_part_number_id PARTNUM,
            		REGEXP_REPLACE(t205.c205_part_num_desc,'[^ ,0-9A-Za-z]', '') PARTDESC,
            		count(distinct t5072.c5072_control_number) COUNT    
        	   FROM T5072_SET_PART_LOT_QTY t5072,T205_PART_NUMBER t205,T2550_PART_CONTROL_NUMBER t2550,
        	   		T5070_SET_LOT_MASTER t5070,T5071_SET_PART_QTY t5071, T202_PROJECT t202, V205_TISSUE_PARTS v205
              WHERE t5072.c205_part_number_id = t205.c205_part_number_id
                AND v205.C205_PART_NUMBER_ID = t2550.C205_PART_NUMBER_ID
            	AND t5072.c5072_control_number = t2550.c2550_control_number
            	AND t2550.c205_part_number_id = t205.c205_part_number_id
            	AND t5072.c5072_last_updated_txn_id = t5071.c5071_last_updated_txn_id
           		AND t5070.c5070_set_lot_master_id = t5071.c5070_set_lot_master_id
            	AND t5071.c5053_last_txn_control_number = t5072.c5072_control_number  --need to check
            	AND t5072.c5070_set_lot_master_id = t5070.c5070_set_lot_master_id
            	AND t205.c202_project_id   = t202.c202_project_id
            	AND ((p_load_fl = 'expired' and t2550.c2550_expiry_date < sysdate) 
         	   		OR (p_load_fl = '30Days' and t2550.C2550_expiry_date  between sysdate+0 and sysdate+30) 
            	AND t2550.c901_last_updated_warehouse_type not in ('108020','108000') --Adjustment and Sold Qty
         	   		OR (p_load_fl = '60Days' and t2550.c2550_expiry_date  between sysdate+31 and sysdate+60)
         	   		OR (p_load_fl = '90Days' and t2550.c2550_expiry_date  between sysdate+61 and sysdate+90)
         	   		OR (p_load_fl = 'others' and t2550.c2550_expiry_date > sysdate+91))
			    AND ((p_part_num IS NOT NULL AND t5072.c205_part_number_id like decode(p_like_search,'LIT',upper(p_part_num),
               				'LIKEPRE', upper(p_part_num||'%'),
               				'LIKESUF', upper('%'||p_part_num),
               				upper('%'||p_part_num||'%')))  
               		 OR (p_part_num IS NULL)) 
                AND t202.c202_project_id= decode(p_project_id,'0',t202.c202_project_id,p_project_id) 
            	AND c5072_void_fl IS NULL
            	AND c5070_void_fl IS NULL
            	AND c5071_void_fl IS NULL
              AND t5070.c901_txn_id = t5072.c5072_last_updated_txn_id
           GROUP BY t5072.c205_part_number_id,t205.c205_part_num_desc)t group by t.partnum,t.partdesc;
           
	SELECT p_load_fl  
      INTO v_out_heder_dtls 
      FROM dual;
    
	p_expired_lots := v_out_Details || '^'|| v_out_heder_dtls;
	
  END gm_fch_tissue_lot_exp_by_range;
  
  /*********************************************************************************************
   * Description : Procedure to fecth data for Lot Expiry dashboard chart based on sterile parts
   * Author      : Agilan Singaravel
  **********************************************************************************************/
  PROCEDURE gm_fch_lot_exp_graph(
  		p_part_num		IN		t205_part_number.c205_part_number_id%type,
  		p_project_id	IN		t202_project.c202_project_id%type,
  		p_exclude_quar	IN		VARCHAR2,
  		p_load_fl		IN		VARCHAR2,
  		p_like_search	IN		VARCHAR2,
  		p_expired_lots	OUT		CLOB
  )
  AS
  BEGIN
	  
	 SELECT JSON_ARRAYAGG (JSON_OBJECT(
	 			'THIRTY' Value thirtydays, 
	 			'SIXTY' Value sixtydays, 
	 			'NINTY' Value nintydays,
    		    'OTHERS' Value others, 
    		    'EXPIRED' Value expired
    		)RETURNING CLOB) INTO p_expired_lots
       FROM (SELECT sum(thirtydays) thirtydays, sum(sixtydays) sixtydays, 
       				sum(nintydays) nintydays, sum(others) others,sum(expireddays) expired
    		   FROM (SELECT (CASE WHEN t2550.C2550_EXPIRY_DATE BETWEEN sysdate + 0 AND sysdate + 30 THEN COUNT(1) END)thirtydays,
    						(CASE WHEN t2550.C2550_EXPIRY_DATE BETWEEN sysdate + 31 AND sysdate + 60 THEN COUNT(1) END)sixtydays,
    				        (CASE WHEN t2550.C2550_EXPIRY_DATE BETWEEN sysdate + 61 AND sysdate + 90 THEN COUNT(1) END)nintydays,
    						(CASE WHEN t2550.C2550_EXPIRY_DATE > sysdate + 91 THEN COUNT(1) END) others,
    						(CASE WHEN t2550.C2550_EXPIRY_DATE < sysdate THEN COUNT(1) END)expireddays 
    				   FROM t2550_part_control_number t2550, t205_part_number t205, T5080_LOT_TRACK_INV t5080, T202_PROJECT t202
    				   		,V205_STERILE_PARTS v205
    				  WHERE t5080.c205_part_number_id = t205.c205_part_number_id  
    				    AND t5080.c205_part_number_id = v205.c205_part_number_id
         				AND t5080.C2550_CONTROL_NUMBER = t2550.C2550_CONTROL_NUMBER
         				AND t205.C205_Part_Number_Id = t2550.C205_Part_Number_ID
         				AND t205.C202_PROJECT_ID   = t202.C202_PROJECT_ID
         				AND ((p_exclude_quar = 'Y' and t5080.C901_WAREHOUSE_TYPE not in (90813)) OR (p_exclude_quar = 'N' and t5080.C901_WAREHOUSE_TYPE = t5080.C901_WAREHOUSE_TYPE)) 
         				AND t202.c202_project_id = decode(p_project_id,'0',t202.c202_project_id,p_project_id) 
    					AND ((p_part_num IS NOT NULL AND t5080.c205_part_number_id like decode(p_like_search,'LIT',upper(p_part_num),
               					'LIKEPRE', upper(p_part_num||'%'),
               					'LIKESUF', upper('%'||p_part_num),
               					upper('%'||p_part_num||'%')))  
               		  		OR (p_part_num IS NULL)) 
         				AND t5080.c5080_void_fl IS NULL
         	       GROUP BY t2550.c2550_expiry_date
            	      UNION    
            		 SELECT (CASE WHEN t2550.C2550_EXPIRY_DATE BETWEEN sysdate + 0 AND sysdate + 30 THEN COUNT(1) END)thirtydays,
    						(CASE WHEN t2550.C2550_EXPIRY_DATE BETWEEN sysdate + 31 AND sysdate + 60 THEN COUNT(1) END)sixtydays,
    				        (CASE WHEN t2550.C2550_EXPIRY_DATE BETWEEN sysdate + 61 AND sysdate + 90 THEN COUNT(1) END)nintydays,
    						(CASE WHEN t2550.C2550_EXPIRY_DATE > sysdate + 91 THEN COUNT(1) END) others,
    						(CASE WHEN t2550.C2550_EXPIRY_DATE < sysdate THEN COUNT(1) END)expireddays 
    				   FROM T5072_SET_PART_LOT_QTY t5072,T205_PART_NUMBER t205,T2550_PART_CONTROL_NUMBER t2550,
        	   				T5070_SET_LOT_MASTER t5070,T5071_SET_PART_QTY t5071, T202_PROJECT t202,V205_STERILE_PARTS v205
        	   	      WHERE t5072.c205_part_number_id = t205.c205_part_number_id
            			AND t5072.c5072_control_number = t2550.c2550_control_number
            			AND t5072.c205_part_number_id = v205.c205_part_number_id
            			AND t2550.c205_part_number_id = t205.c205_part_number_id
            			AND t5072.c5072_last_updated_txn_id = t5071.c5071_last_updated_txn_id
           				AND t5070.c5070_set_lot_master_id = t5071.c5070_set_lot_master_id
            			AND t5071.c5053_last_txn_control_number = t5072.c5072_control_number  --need to check
            			AND t5072.c5070_set_lot_master_id = t5070.c5070_set_lot_master_id
            			AND t205.c202_project_id   = t202.c202_project_id
            			AND t202.c202_project_id = decode(p_project_id,'0',t202.c202_project_id,p_project_id)             			
			    		AND ((p_part_num IS NOT NULL AND t5072.c205_part_number_id like decode(p_like_search,'LIT',upper(p_part_num),
               					'LIKEPRE', upper(p_part_num||'%'),
               					'LIKESUF', upper('%'||p_part_num),
               					upper('%'||p_part_num||'%')))  
               		 		OR (p_part_num IS NULL)) 
         	       		AND c5072_void_fl IS NULL
            			AND c5070_void_fl IS NULL
            			AND c5071_void_fl IS NULL
            			AND t5070.c901_txn_id = t5072.c5072_last_updated_txn_id
           		   GROUP BY t2550.c2550_expiry_date
						));

  END gm_fch_lot_exp_graph;
  
  /********************************************************************************************
   * Description : Procedure to fecth data for Lot Expiry dashboard chart based on tissue parts
   * Author      : Agilan Singaravel	
  *********************************************************************************************/
  PROCEDURE gm_fch_tissue_lot_exp_graph(
  		p_part_num		IN		t205_part_number.c205_part_number_id%type,
  		p_project_id	IN		t202_project.c202_project_id%type,
  		p_exclude_quar	IN		VARCHAR2,
  		p_load_fl		IN		VARCHAR2,
  		p_like_search	IN		VARCHAR2,
  		p_expired_lots	OUT		CLOB
  )
  AS
  BEGIN
	  
	   SELECT JSON_ARRAYAGG (JSON_OBJECT(
	 			'THIRTY' Value thirtydays, 
	 			'SIXTY' Value sixtydays, 
	 			'NINTY' Value nintydays,
    		    'OTHERS' Value others, 
    		    'EXPIRED' Value expired
    		)RETURNING CLOB) INTO p_expired_lots
       FROM (SELECT sum(thirtydays) thirtydays, sum(sixtydays) sixtydays, 
       				sum(nintydays) nintydays, sum(others) others,sum(expireddays) expired
    		   FROM (SELECT (CASE WHEN t2550.C2550_EXPIRY_DATE BETWEEN sysdate +0 AND sysdate + 30 THEN COUNT(1) END)thirtydays,
    						(CASE WHEN t2550.C2550_EXPIRY_DATE BETWEEN sysdate +31 AND sysdate + 60 THEN COUNT(1) END)sixtydays,
    				        (CASE WHEN t2550.C2550_EXPIRY_DATE BETWEEN sysdate +61 AND sysdate + 90 THEN COUNT(1) END)nintydays,
    						(CASE WHEN t2550.C2550_EXPIRY_DATE > sysdate + 91 THEN COUNT(1) END) others,
    						(CASE WHEN t2550.C2550_EXPIRY_DATE < sysdate THEN COUNT(1) END)expireddays 
    				   FROM t2550_part_control_number t2550, t205_part_number t205, T202_PROJECT t202,V205_TISSUE_PARTS v205
    				  WHERE v205.C205_PART_NUMBER_ID = t2550.C205_PART_NUMBER_ID
         				AND t205.C205_Part_Number_Id = t2550.C205_Part_Number_ID
         				AND t205.C202_PROJECT_ID   = t202.C202_PROJECT_ID
         				AND ((p_exclude_quar = 'Y' and t2550.c901_last_updated_warehouse_type not in (90813)) OR (p_exclude_quar = 'N' and t2550.c901_last_updated_warehouse_type = t2550.c901_last_updated_warehouse_type)) 
         				AND t202.c202_project_id = decode(p_project_id,'0',t202.c202_project_id,p_project_id) 
         				AND t2550.c901_last_updated_warehouse_type not in ('108020','108000') --Adjustment and Sold Qty
    					AND ((p_part_num IS NOT NULL AND t2550.c205_part_number_id like decode(p_like_search,'LIT',upper(p_part_num),
               					'LIKEPRE', upper(p_part_num||'%'),
               					'LIKESUF', upper('%'||p_part_num),
               					upper('%'||p_part_num||'%')))  
               		  		OR (p_part_num IS NULL))
         		   GROUP BY t2550.c2550_expiry_date
         		 UNION ALL
         		 SELECT (CASE WHEN t2550.C2550_EXPIRY_DATE BETWEEN sysdate + 0 AND sysdate + 30 THEN COUNT(1) END)thirtydays,
    						(CASE WHEN t2550.C2550_EXPIRY_DATE BETWEEN sysdate + 31 AND sysdate + 60 THEN COUNT(1) END)sixtydays,
    				        (CASE WHEN t2550.C2550_EXPIRY_DATE BETWEEN sysdate + 61 AND sysdate + 90 THEN COUNT(1) END)nintydays,
    						(CASE WHEN t2550.C2550_EXPIRY_DATE > sysdate + 91 THEN COUNT(1) END) others,
    						(CASE WHEN t2550.C2550_EXPIRY_DATE < sysdate THEN COUNT(1) END)expireddays 
    				   FROM T5072_SET_PART_LOT_QTY t5072,T205_PART_NUMBER t205,T2550_PART_CONTROL_NUMBER t2550,
        	   				T5070_SET_LOT_MASTER t5070,T5071_SET_PART_QTY t5071, T202_PROJECT t202,V205_TISSUE_PARTS v205
        	   	      WHERE t5072.c205_part_number_id = t205.c205_part_number_id
        	   	      	AND v205.C205_PART_NUMBER_ID = t2550.C205_PART_NUMBER_ID
            			AND t5072.c5072_control_number = t2550.c2550_control_number
            			AND t2550.c205_part_number_id = t205.c205_part_number_id
            			AND t5072.c5072_last_updated_txn_id = t5071.c5071_last_updated_txn_id
           				AND t5070.c5070_set_lot_master_id = t5071.c5070_set_lot_master_id
            			AND t5071.c5053_last_txn_control_number = t5072.c5072_control_number  --need to check
            			AND t5072.c5070_set_lot_master_id = t5070.c5070_set_lot_master_id
            			AND t205.c202_project_id   = t202.c202_project_id
            			AND t202.c202_project_id = decode(p_project_id,'0',t202.c202_project_id,p_project_id) 
            			AND t2550.c901_last_updated_warehouse_type not in ('108020','108000') --Adjustment and Sold Qty
			    		AND ((p_part_num IS NOT NULL AND t5072.c205_part_number_id like decode(p_like_search,'LIT',upper(p_part_num),
               					'LIKEPRE', upper(p_part_num||'%'),
               					'LIKESUF', upper('%'||p_part_num),
               					upper('%'||p_part_num||'%')))  
               		 		OR (p_part_num IS NULL))          	  
         	       		AND c5072_void_fl IS NULL
            			AND c5070_void_fl IS NULL
            			AND c5071_void_fl IS NULL
                        AND t5070.c901_txn_id = t5072.c5072_last_updated_txn_id
           		   GROUP BY t2550.c2550_expiry_date
           		   ));
	  
  END gm_fch_tissue_lot_exp_graph;
  
  /*******************************************************************************************
   * Description : Procedure to fecth data for Lot Expiry dashboard popup based on inventory
   * Author      : Agilan Sinagravel
  ********************************************************************************************/
  PROCEDURE gm_fch_inventory_lot_details(
  		p_part_num		IN		t205_part_number.c205_part_number_id%type,
  		p_load_fl		IN		VARCHAR2,
  		p_type	        IN		VARCHAR2,
  		p_project_id    IN      t202_project.c202_project_id%type,
  		p_exclude_quar	IN		VARCHAR2,
  		p_like_search	IN		VARCHAR2,
  		p_expired_lots	OUT		CLOB
  )
  AS
  v_out_Details 	CLOB;
  v_out_heder_dtls 	CLOB;
  BEGIN
	  
	  	 SELECT JSON_ARRAYAGG(JSON_OBJECT(
	 		        'PARTNUM' VALUE t5080.c205_part_number_id,
	   				'PARTDESC' VALUE REGEXP_REPLACE(t205.c205_part_num_desc,'[^ ,0-9A-Za-z]', ''),
  				  	'PLANTNAME' VALUE t5040.c5040_plant_name,
  				  	'WAREHOUSEID' VALUE t5080.c901_warehouse_type,
  				  	'WAREHOUSETYPE' VALUE t901.c901_code_nm,
  				  	'QTY' VALUE sum(t5080.c5080_qty),
  				  	'PLANTID' VALUE t5080.c5040_plant_id,
  				  	'LOADFL' VALUE p_load_fl,
  				  	'COMPANYID' VALUE '0',
  				  	'TYPE' VALUE p_type,
  				  	'FIELDSALES' VALUE '0',
  				  	'SETID' VALUE '0'
  			 )RETURNING CLOB) INTO v_out_Details 
	    FROM T5080_LOT_TRACK_INV t5080, T202_PROJECT t202, T205_PART_NUMBER t205, T2550_PART_CONTROL_NUMBER t2550,T901_CODE_LOOKUP t901,
	    	 T5040_PLANT_MASTER t5040, V205_STERILE_PARTS v205
	   WHERE t5080.c205_part_number_id = t205.c205_part_number_id
         AND t5080.c2550_control_number = t2550.c2550_control_number
         AND t901.c901_code_id = t5080.c901_warehouse_type
         AND t5040.c5040_plant_id = t5080.c5040_plant_id
         AND t5080.c205_part_number_id = v205.c205_part_number_id
         AND ((p_load_fl = 'expired' and t2550.c2550_expiry_date < sysdate) 
         	   OR (p_load_fl = '30Days' and t2550.C2550_EXPIRY_DATE  between sysdate+0 and sysdate+30) 
         	   OR (p_load_fl = '60Days' and t2550.C2550_EXPIRY_DATE  between sysdate+31 and sysdate+60)
         	   OR (p_load_fl = '90Days' and t2550.C2550_EXPIRY_DATE  between sysdate+61 and sysdate+90)
         	   OR (p_load_fl = 'others' and t2550.C2550_EXPIRY_DATE > sysdate+91))
         AND t205.c205_part_number_id = t2550.c205_part_number_id
         AND t205.c202_project_id   = t202.c202_project_id
         AND t5080.c901_warehouse_type not in (4000339,56002,106221) --FS, Account, Loaner, consignment             
    	 AND t5080.c205_part_number_id = p_part_num
         AND t202.c202_project_id= decode(p_project_id,'0',t202.c202_project_id,p_project_id)
         AND ((p_exclude_quar = 'Y' and t5080.C901_WAREHOUSE_TYPE not in (90813)) OR (p_exclude_quar = 'N' and t5080.C901_WAREHOUSE_TYPE = t5080.C901_WAREHOUSE_TYPE)) 
         AND t5080.c5080_void_fl IS NULL
    GROUP BY t5080.c205_part_number_id,t205.c205_part_num_desc,t5080.c901_warehouse_type,t901.c901_code_nm,t5040.c5040_plant_name,t5080.c5040_plant_id;
	  
     SELECT p_type
       INTO v_out_heder_dtls 
       FROM dual;
       
       p_expired_lots := v_out_Details || '^'|| v_out_heder_dtls;
       
  END gm_fch_inventory_lot_details;
  
   /*********************************************************************************************************
   * Description : Procedure to fecth data for Lot Expiry dashboard popup based on Filed Sales warehouse type
   * Author      : Agilan Sinagravel
  ***********************************************************************************************************/ 
    PROCEDURE gm_fch_fieldsales_lot_details(
  		p_part_num		IN		t205_part_number.c205_part_number_id%type,
  		p_load_fl		IN		VARCHAR2,
  		p_type	        IN		VARCHAR2,
  		p_project_id    IN      t202_project.c202_project_id%type,
  		p_exclude_quar	IN		VARCHAR2,
  		p_like_search	IN		VARCHAR2,
  		p_expired_lots	OUT		CLOB
  )
  AS
  v_out_Details 		CLOB;
  v_out_heder_dtls 		CLOB;
  BEGIN
	  
	  	  	 SELECT JSON_ARRAYAGG(JSON_OBJECT(
	 		        'PARTNUM' VALUE t5080.c205_part_number_id,
	   				'PARTDESC' VALUE REGEXP_REPLACE(t205.c205_part_num_desc,'[^ ,0-9A-Za-z]', ''),
  				  	'PLANTNAME' VALUE t1900.c1900_company_name,
  				  	'WAREHOUSEID' VALUE t5080.c901_warehouse_type,
  				  	'WAREHOUSETYPE' VALUE t5080.c5080_location_name,
  				  	'QTY' VALUE sum(t5080.c5080_qty),
  				  	'TYPE' VALUE p_type,
  				  	'PLANTID' VALUE '0',
  				  	'LOADFL' VALUE p_load_fl,
  				  	'COMPANYID' VALUE t5080.c1900_company_id,
  				  	'FIELDSALES' VALUE t5080.c5080_location_name,
  				  	'SETID' VALUE '0'
  			 )RETURNING CLOB) INTO v_out_Details 
	    FROM T5080_LOT_TRACK_INV t5080, T202_PROJECT t202, T205_PART_NUMBER t205, T2550_PART_CONTROL_NUMBER t2550, 
	    	 T1900_COMPANY t1900, V205_STERILE_PARTS v205
	   WHERE t5080.c205_part_number_id = t205.c205_part_number_id
         AND t5080.c2550_control_number = t2550.c2550_control_number
         AND t1900.c1900_company_id = t5080.c1900_company_id
         AND t5080.c205_part_number_id = v205.c205_part_number_id
         AND ((p_load_fl = 'expired' and t2550.c2550_expiry_date < sysdate) 
         	   OR (p_load_fl = '30Days' and t2550.C2550_EXPIRY_DATE  between sysdate+0 and sysdate+30) 
         	   OR (p_load_fl = '60Days' and t2550.C2550_EXPIRY_DATE  between sysdate+31 and sysdate+60)
         	   OR (p_load_fl = '90Days' and t2550.C2550_EXPIRY_DATE  between sysdate+61 and sysdate+90)
         	   OR (p_load_fl = 'others' and t2550.C2550_EXPIRY_DATE > sysdate+91))
         AND t205.c205_part_number_id = t2550.c205_part_number_id
         AND t205.c202_project_id   = t202.c202_project_id
         AND t5080.c901_warehouse_type in (4000339,56002) --FS, Account      
    	 AND t5080.c205_part_number_id = p_part_num
         AND t202.c202_project_id= decode(p_project_id,'0',t202.c202_project_id,p_project_id) 
         AND ((p_exclude_quar = 'Y' and t5080.C901_WAREHOUSE_TYPE not in (90813)) OR (p_exclude_quar = 'N' and t5080.C901_WAREHOUSE_TYPE = t5080.C901_WAREHOUSE_TYPE)) 
         AND t5080.c5080_void_fl IS NULL
    GROUP BY t5080.c205_part_number_id,t205.c205_part_num_desc,t1900.c1900_company_name,t5080.c5080_location_name,t5080.c901_warehouse_type,t5080.c1900_company_id;
	  
     SELECT p_type
       INTO v_out_heder_dtls FROM dual;
       p_expired_lots := v_out_Details || '^'|| v_out_heder_dtls;
	  
  END gm_fch_fieldsales_lot_details;
  
  /*********************************************************************************************************
   * Description : Procedure to fecth data for Lot Expiry dashboard popup based on consignment warehouse type
   * Author      : Agilan Sinagravel
  ***********************************************************************************************************/ 
  PROCEDURE gm_fch_consignment_set_lot_details(
  		p_part_num		IN		t205_part_number.c205_part_number_id%type,
  		p_load_fl		IN		VARCHAR2,
  		p_type	        IN		VARCHAR2,
  		p_project_id    IN      t202_project.c202_project_id%type,
  		p_exclude_quar	IN		VARCHAR2,
  		p_like_search	IN		VARCHAR2,
  		p_expired_lots	OUT		CLOB
  )
  AS
  v_out_Details 	CLOB;
  v_out_heder_dtls 	CLOB;
  BEGIN
	  
	   SELECT JSON_ARRAYAGG(JSON_OBJECT(
	 		        'PARTNUM' VALUE t5080.c205_part_number_id,
	   				'PARTDESC' VALUE t5081.c207_set_id,
  				  	'PLANTNAME' VALUE t5081.c207_set_name,
  				  	'WAREHOUSEID' VALUE t5080.c901_warehouse_type,
  				  	'WAREHOUSETYPE' VALUE t5040.c5040_plant_name,
  				  	'QTY' VALUE sum(t5080.c5080_qty),
  				  	'PLANTID' VALUE t5080.c5040_plant_id,
  				  	'LOADFL' VALUE p_load_fl,
  				  	'COMPANYID' VALUE '0',
  				  	'TYPE' VALUE p_type,
  				  	'FIELDSALES' VALUE '0',
  				  	'SETID' VALUE t5081.c207_set_id
  			 )RETURNING CLOB) INTO v_out_Details 
	    FROM T5080_LOT_TRACK_INV t5080, T202_PROJECT t202, T205_PART_NUMBER t205, T2550_PART_CONTROL_NUMBER t2550,
	    	 T5040_PLANT_MASTER t5040,T5081_LOT_TRACK_INV_LOG t5081, V205_STERILE_PARTS v205
	   WHERE t5080.c205_part_number_id = t205.c205_part_number_id
	   	 AND t5080.c205_part_number_id = v205.c205_part_number_id
         AND t5080.c2550_control_number = t2550.c2550_control_number  
         AND t5081.C5080_CONTROL_NUMBER_INV_ID = t5080.C5080_CONTROL_NUMBER_INV_ID
         AND t5081.c205_part_number_id = t5080.c205_part_number_id
         AND t5040.c5040_plant_id = t5080.c5040_plant_id
         AND ((p_load_fl = 'expired' and t2550.c2550_expiry_date < sysdate) 
         	   OR (p_load_fl = '30Days' and t2550.C2550_EXPIRY_DATE  between sysdate+0 and sysdate+30) 
         	   OR (p_load_fl = '60Days' and t2550.C2550_EXPIRY_DATE  between sysdate+31 and sysdate+60)
         	   OR (p_load_fl = '90Days' and t2550.C2550_EXPIRY_DATE  between sysdate+61 and sysdate+90)
         	   OR (p_load_fl = 'others' and t2550.C2550_EXPIRY_DATE > sysdate+91))
         AND t205.c205_part_number_id = t2550.c205_part_number_id
         AND t205.c202_project_id   = t202.c202_project_id
         AND t5080.c901_warehouse_type in (26240819) --Consignment Qty         
    	 AND t5080.c205_part_number_id = p_part_num
         AND t202.c202_project_id= decode(p_project_id,'0',t202.c202_project_id,p_project_id)
         AND ((p_exclude_quar = 'Y' and t5080.C901_WAREHOUSE_TYPE not in (90813)) OR (p_exclude_quar = 'N' and t5080.C901_WAREHOUSE_TYPE = t5080.C901_WAREHOUSE_TYPE)) 
         AND t5080.c5080_void_fl IS NULL
    GROUP BY t5080.c205_part_number_id,t5081.c207_set_id,t5081.c207_set_name,t5040.c5040_plant_name,t5080.c901_warehouse_type,t5080.c5040_plant_id;
	  
     SELECT p_type
       INTO v_out_heder_dtls FROM dual;
       p_expired_lots := v_out_Details || '^'|| v_out_heder_dtls;
	  
  END gm_fch_consignment_set_lot_details;
  
  /*********************************************************************************************************
   * Description : Procedure to fecth data for Lot Expiry dashboard popup based on Loaner Set warehouse type
   * Author      : Agilan Sinagravel
  ***********************************************************************************************************/ 
  PROCEDURE gm_fch_loaner_set_lot_details(
  		p_part_num		IN		t205_part_number.c205_part_number_id%type,
  		p_load_fl		IN		VARCHAR2,
  		p_type	        IN		VARCHAR2,
  		p_project_id    IN      t202_project.c202_project_id%type,
  		p_like_search	IN		VARCHAR2,
  		p_expired_lots	OUT		CLOB
  )
  AS
  v_out_Details 	CLOB;
  v_out_heder_dtls 	CLOB;
  BEGIN
	  
	  SELECT JSON_ARRAYAGG(JSON_OBJECT(
	    			'PARTNUM' VALUE PARTNUM,
	   				'PARTDESC' VALUE PARTDESC,
  				  	'PLANTNAME' VALUE PLANTNAME,
  				  	'WAREHOUSEID' VALUE WAREHOUSEID, 			--loaner qty warehouse
  				  	'WAREHOUSETYPE' VALUE WAREHOUSETYPE,
  				  	'QTY' VALUE QTY,
  				  	'PLANTID' VALUE PLANTID,
  				  	'LOADFL' VALUE p_load_fl,
  				  	'COMPANYID' VALUE '0',
  				  	'TYPE' VALUE TYPE,
  				  	'FIELDSALES' VALUE '0',
  				  	'SETID' VALUE PARTDESC
  			 )RETURNING CLOB) INTO v_out_Details 
  	   FROM	( 
     SELECT	 t5072.c205_part_number_id PARTNUM,
     		 t504.c207_set_id PARTDESC,
     		 t207.c207_set_nm PLANTNAME,
     		 '106221' WAREHOUSEID,
     		 t5040.c5040_plant_name WAREHOUSETYPE,
     		 sum(t5072.C5072_CURR_QTY) QTY,
     		 p_type TYPE,
     		 t504.c5040_plant_id PLANTID
	   FROM  t5072_set_part_lot_qty t5072, T205_PART_NUMBER t205, T2550_PART_CONTROL_NUMBER t2550, t5040_plant_master t5040,t207_set_master t207, t202_project t202
	   		 ,t504_consignment t504,t5070_set_lot_master t5070, V205_STERILE_PARTS v205
	  WHERE  t5072.C205_PART_NUMBER_ID = t205.c205_part_number_id
	    AND  t5072.c5072_control_number = t2550.C2550_CONTROL_NUMBER 
	    AND  t2550.C205_PART_NUMBER_ID = t205.C205_PART_NUMBER_ID
	    AND  t5072.c5072_last_updated_txn_id = t504.c504_consignment_id
	    AND  t5070.c901_txn_id = t504.c504_consignment_id
        AND  t5072.c5070_set_lot_master_id = t5070.c5070_set_lot_master_id
        AND  t5072.C205_PART_NUMBER_ID = v205.c205_part_number_id
        AND  t504.c5040_plant_id = t5040.c5040_plant_id
        AND  t207.c207_set_id = t504.c207_set_id
        AND  t202.c202_project_id = t205.c202_project_id
        AND ((p_load_fl = 'expired' and t2550.c2550_expiry_date < sysdate) 
         	   OR (p_load_fl = '30Days' and t2550.C2550_EXPIRY_DATE  between sysdate+0 and sysdate+30) 
         	   OR (p_load_fl = '60Days' and t2550.C2550_EXPIRY_DATE  between sysdate+31 and sysdate+60)
         	   OR (p_load_fl = '90Days' and t2550.C2550_EXPIRY_DATE  between sysdate+61 and sysdate+90)
         	   OR (p_load_fl = 'others' and t2550.C2550_EXPIRY_DATE > sysdate+91))
		AND  t5072.c205_part_number_id = p_part_num
        AND  t202.c202_project_id= decode(p_project_id,'0',t202.c202_project_id,p_project_id)  
        AND  t5072.c5072_void_fl IS NULL
        AND  c504_void_fl IS NULL
        AND  c5070_void_fl IS NULL
        AND  t5040.c5040_void_fl is null
   GROUP BY  t5072.c205_part_number_id,t504.c207_set_id,t207.c207_set_nm,t5040.c5040_plant_name,t504.c5040_plant_id
   
      UNION 
      
      SELECT t5072.c205_part_number_id PARTNUM,
	   		 t504.c207_set_id PARTDESC,
  			 t207.c207_set_nm PLANTNAME,
  			 '106221' WAREHOUSEID, 			--loaner qty warehouse
  			 t5040.c5040_plant_name WAREHOUSETYPE,
  			 sum(t5072.C5072_CURR_QTY) QTY,
  			 p_type TYPE,
  			 t504.c5040_plant_id PLANTID
	   FROM  t5072_set_part_lot_qty t5072, T205_PART_NUMBER t205, T2550_PART_CONTROL_NUMBER t2550, t5040_plant_master t5040,t207_set_master t207, t202_project t202
	   		 ,t504_consignment t504,t5070_set_lot_master t5070,t412_inhouse_transactions t412, V205_STERILE_PARTS v205
	  WHERE  t5072.C205_PART_NUMBER_ID = t205.c205_part_number_id
	    AND  t5072.c5072_control_number = t2550.C2550_CONTROL_NUMBER 
	    AND  t2550.C205_PART_NUMBER_ID = t205.C205_PART_NUMBER_ID
	    AND  t5072.c5072_last_updated_txn_id = t412.c412_inhouse_trans_id
        AND  t504.c504_consignment_id = t412.c412_ref_id
	    AND  t5070.c901_txn_id = t504.c504_consignment_id
        AND  t5072.c5070_set_lot_master_id = t5070.c5070_set_lot_master_id
        AND  t5072.C205_PART_NUMBER_ID = v205.c205_part_number_id
        AND  t504.c5040_plant_id = t5040.c5040_plant_id
        AND  t207.c207_set_id = t504.c207_set_id
        AND  t202.c202_project_id = t205.c202_project_id
        AND ((p_load_fl = 'expired' and t2550.c2550_expiry_date < sysdate) 
         	   OR (p_load_fl = '30Days' and t2550.C2550_EXPIRY_DATE  between sysdate+0 and sysdate+30) 
         	   OR (p_load_fl = '60Days' and t2550.C2550_EXPIRY_DATE  between sysdate+31 and sysdate+60)
         	   OR (p_load_fl = '90Days' and t2550.C2550_EXPIRY_DATE  between sysdate+61 and sysdate+90)
         	   OR (p_load_fl = 'others' and t2550.C2550_EXPIRY_DATE > sysdate+91))
		AND  t5072.c205_part_number_id = p_part_num
        AND  t202.c202_project_id= decode(p_project_id,'0',t202.c202_project_id,p_project_id)  
        AND  t5072.c5072_void_fl IS NULL
        AND  c504_void_fl IS NULL
        AND  c5070_void_fl IS NULL
        AND  t5040.c5040_void_fl is null
        AND  c412_void_fl IS NULL
   GROUP BY  t5072.c205_part_number_id,t504.c207_set_id,t207.c207_set_nm,t5040.c5040_plant_name,t504.c5040_plant_id);
   
    SELECT p_type
      INTO v_out_heder_dtls 
      FROM dual;
       
       p_expired_lots := v_out_Details || '^'|| v_out_heder_dtls;
       
  END gm_fch_loaner_set_lot_details;
  
  /**********************************************************************************************
   * Description : Procedure to fecth data for Lot Expiry dashboard popup based on tissue parts
   * Author      : Agilan Sinagravel
  ************************************************************************************************/ 
  PROCEDURE gm_fch_tissue_lot_popup_details(
  		p_part_num		IN		t205_part_number.c205_part_number_id%type,
  		p_load_fl		IN		VARCHAR2,
  		p_type	        IN		VARCHAR2,
  		p_project_id    IN      t202_project.c202_project_id%type,
  		p_exclude_quar	IN		VARCHAR2,
  		p_like_search	IN		VARCHAR2,
  		p_expired_lots	OUT		CLOB
  )
  AS
  v_out_Details 		CLOB;
  v_out_heder_dtls 		CLOB;
  BEGIN
	
	  IF p_type = 'FieldSales' THEN	  
	  		gm_fch_tissue_fieldsales_details(p_part_num,p_load_fl,p_type,p_project_id,p_exclude_quar,p_like_search,v_out_Details);	  
	  ELSIF p_type = 'Inventory' THEN	  		
	  		gm_fch_tissue_inventory_details(p_part_num,p_load_fl,p_type,p_project_id,p_exclude_quar,p_like_search,v_out_Details);
	  ELSE 
	  		gm_fch_tissue_consignment_details(p_part_num,p_load_fl,p_type,p_project_id,p_exclude_quar,p_like_search,v_out_Details);
	  END IF;

     SELECT p_type
       INTO v_out_heder_dtls 
       FROM dual;
       
       p_expired_lots := v_out_Details || '^'|| v_out_heder_dtls; 
       
 END gm_fch_tissue_lot_popup_details;
 
 /**********************************************************************************************
   * Description : Procedure to fecth data for Lot Expiry dashboard fieldsales/Account 
   * 				details based on tissue parts
   * Author      : Agilan Sinagravel
  ************************************************************************************************/ 
  PROCEDURE gm_fch_tissue_fieldsales_details(
  		p_part_num		IN		t205_part_number.c205_part_number_id%type,
  		p_load_fl		IN		VARCHAR2,
  		p_type	        IN		VARCHAR2,
  		p_project_id    IN      t202_project.c202_project_id%type,
  		p_exclude_quar	IN		VARCHAR2,
  		p_like_search	IN		VARCHAR2,
  		p_expired_lots	OUT		CLOB
  )
  AS
  BEGIN
	  
	  	SELECT JSON_ARRAYAGG(JSON_OBJECT(
	 		        'PARTNUM' VALUE PARTNUM,
	   				'PARTDESC' VALUE PARTDESC,
  				  	'PLANTNAME' VALUE PLANTNAME,
  				  	'WAREHOUSEID' VALUE WAREHOUSEID,
  				  	'WAREHOUSETYPE' VALUE WAREHOUSETYPE,
  				  	--'QTY' VALUE 1,
  				  	'TYPE' VALUE p_type,
  				  	'QTY' VALUE QTY,
  				  	'COMPANYID' VALUE companyid,
  				  	'PLANTID' VALUE plantid,
  				  	'LOADFL' VALUE p_load_fl
  			 	)RETURNING CLOB) INTO p_expired_lots
  		  FROM (
  		 		SELECT t2550.c205_part_number_id PARTNUM,
  		 			   REGEXP_REPLACE(t205.c205_part_num_desc,'[^ ,0-9A-Za-z]', '') PARTDESC,
  		 			   t5040.c5040_plant_name PLANTNAME,
  		 			   t2550.c901_last_updated_warehouse_type WAREHOUSEID,
  		 			   t901.c901_code_nm WAREHOUSETYPE,
  		 			   t2550.c5040_plant_id plantid,
  		 			   t2550.c1900_company_id companyid,
  		 			   COUNT(t2550.c2550_control_number) QTY
  		 	      FROM T202_PROJECT t202, T205_PART_NUMBER t205, T2550_PART_CONTROL_NUMBER t2550,
	    	 		   T901_CODE_LOOKUP t901,T5040_PLANT_MASTER t5040,V205_TISSUE_PARTS v205
  		  		 WHERE v205.c205_part_number_id = t2550.c205_part_number_id
         		   AND t901.c901_code_id = t2550.c901_last_updated_warehouse_type
         		   AND t5040.c5040_plant_id = t2550.c5040_plant_id
         		   AND ((p_load_fl = 'expired' and t2550.c2550_expiry_date < sysdate) 
         	   			OR (p_load_fl = '30Days' and t2550.C2550_EXPIRY_DATE  between sysdate+0 and sysdate+30) 
         	   			OR (p_load_fl = '60Days' and t2550.C2550_EXPIRY_DATE  between sysdate+31 and sysdate+60)
         	   			OR (p_load_fl = '90Days' and t2550.C2550_EXPIRY_DATE  between sysdate+61 and sysdate+90)
         	   			OR (p_load_fl = 'others' and t2550.C2550_EXPIRY_DATE > sysdate+91))
         		   AND t205.c205_part_number_id = t2550.c205_part_number_id
         		   AND t205.c202_project_id   = t202.c202_project_id
				   AND t2550.c901_last_updated_warehouse_type = 4000339       
    	 	       AND t2550.c205_part_number_id = p_part_num
    	 		   AND t202.c202_project_id= decode(p_project_id,'0',t202.c202_project_id,p_project_id)
    	 		   AND ((p_exclude_quar = 'Y' and t2550.c901_last_updated_warehouse_type not in (90813)) OR (p_exclude_quar = 'N' and t2550.c901_last_updated_warehouse_type = t2550.c901_last_updated_warehouse_type)) 
    		  GROUP BY t2550.c205_part_number_id,t205.c205_part_num_desc,t901.c901_code_nm,
    		  		   t5040.c5040_plant_name,t2550.c901_last_updated_warehouse_type,t2550.c5040_plant_id,t2550.c1900_company_id
    		  		
    		  	  UNION 
    		  	  
    		  	  SELECT t2550.c205_part_number_id PARTNUM,
  		 			   REGEXP_REPLACE(t205.c205_part_num_desc,'[^ ,0-9A-Za-z]', '') PARTDESC,
  		 			   t5040.c5040_plant_name PLANTNAME,
  		 			   t2550.c901_last_updated_warehouse_type WAREHOUSEID,
  		 			   t901.c901_code_nm WAREHOUSETYPE,
  		 			   t2550.c5040_plant_id plantid,
  		 			   t2550.c1900_company_id companyid,
  		 			   COUNT(t2550.c2550_control_number) QTY
  		 	      FROM T202_PROJECT t202, T205_PART_NUMBER t205, T2550_PART_CONTROL_NUMBER t2550,
	    	 		   T901_CODE_LOOKUP t901,T5040_PLANT_MASTER t5040,V205_TISSUE_PARTS v205
  		  		 WHERE v205.c205_part_number_id = t2550.c205_part_number_id
         		   AND t901.c901_code_id = t2550.c901_last_updated_warehouse_type
         		   AND t5040.c5040_plant_id = t2550.c5040_plant_id
         		   AND ((p_load_fl = 'expired' and t2550.c2550_expiry_date < sysdate) 
         	   			OR (p_load_fl = '30Days' and t2550.C2550_EXPIRY_DATE  between sysdate+0 and sysdate+30) 
         	   			OR (p_load_fl = '60Days' and t2550.C2550_EXPIRY_DATE  between sysdate+31 and sysdate+60)
         	   			OR (p_load_fl = '90Days' and t2550.C2550_EXPIRY_DATE  between sysdate+61 and sysdate+90)
         	   			OR (p_load_fl = 'others' and t2550.C2550_EXPIRY_DATE > sysdate+91))
         		   AND t205.c205_part_number_id = t2550.c205_part_number_id
         		   AND t205.c202_project_id   = t202.c202_project_id
				   AND t2550.c901_last_updated_warehouse_type = 56002     
    	 	       AND t2550.c205_part_number_id = p_part_num
    	 		   AND t202.c202_project_id= decode(p_project_id,'0',t202.c202_project_id,p_project_id)
    	 		   AND ((p_exclude_quar = 'Y' and t2550.c901_last_updated_warehouse_type not in (90813)) OR (p_exclude_quar = 'N' and t2550.c901_last_updated_warehouse_type = t2550.c901_last_updated_warehouse_type)) 
    		  GROUP BY t2550.c205_part_number_id,t205.c205_part_num_desc,t901.c901_code_nm,
    		  		   t5040.c5040_plant_name,t2550.c901_last_updated_warehouse_type,t2550.c5040_plant_id,t2550.c1900_company_id
					   );
	  
  END gm_fch_tissue_fieldsales_details;
 /*******************************************************************************
   * Description : Procedure to fecth data for Lot Expiry dashboard Inventory
   * 				details based on tissue parts
   * Author      : Agilan Sinagravel
  *******************************************************************************/ 
  PROCEDURE gm_fch_tissue_inventory_details(
  		p_part_num		IN		t205_part_number.c205_part_number_id%type,
  		p_load_fl		IN		VARCHAR2,
  		p_type	        IN		VARCHAR2,
  		p_project_id    IN      t202_project.c202_project_id%type,
  		p_exclude_quar	IN		VARCHAR2,
  		p_like_search	IN		VARCHAR2,
  		p_expired_lots	OUT		CLOB
  )
  AS
  BEGIN
	  SELECT JSON_ARRAYAGG(JSON_OBJECT(
	 		        'PARTNUM' VALUE PARTNUM,
	   				'PARTDESC' VALUE PARTDESC,
  				  	'PLANTNAME' VALUE PLANTNAME,
  				  	'WAREHOUSEID' VALUE WAREHOUSEID,
  				  	'WAREHOUSETYPE' VALUE WAREHOUSETYPE,
  				  	--'QTY' VALUE 1,
  				  	'TYPE' VALUE p_type,
  				  	'QTY' VALUE QTY,
  				  	'COMPANYID' VALUE companyid,
  				  	'PLANTID' VALUE plantid,
  				  	'LOADFL' VALUE p_load_fl
  			 	)RETURNING CLOB) INTO p_expired_lots
  		  FROM (
  		 		SELECT t2550.c205_part_number_id PARTNUM,
  		 			   REGEXP_REPLACE(t205.c205_part_num_desc,'[^ ,0-9A-Za-z]', '') PARTDESC,
  		 			   t5040.c5040_plant_name PLANTNAME,
  		 			   t2550.c901_last_updated_warehouse_type WAREHOUSEID,
  		 			   t901.c901_code_nm WAREHOUSETYPE,
  		 			   t2550.c5040_plant_id plantid,
  		 			   t2550.c1900_company_id companyid,
  		 			   COUNT(t2550.c2550_control_number) QTY
  		 	      FROM T202_PROJECT t202, T205_PART_NUMBER t205, T2550_PART_CONTROL_NUMBER t2550,
	    	 		   T901_CODE_LOOKUP t901,T5040_PLANT_MASTER t5040,V205_TISSUE_PARTS v205
  		  		 WHERE v205.c205_part_number_id = t2550.c205_part_number_id
         		   AND t901.c901_code_id = t2550.c901_last_updated_warehouse_type
         		   AND t5040.c5040_plant_id = t2550.c5040_plant_id
         		   AND ((p_load_fl = 'expired' and t2550.c2550_expiry_date < sysdate) 
         	   			OR (p_load_fl = '30Days' and t2550.C2550_EXPIRY_DATE  between sysdate+0 and sysdate+30) 
         	   			OR (p_load_fl = '60Days' and t2550.C2550_EXPIRY_DATE  between sysdate+31 and sysdate+60)
         	   			OR (p_load_fl = '90Days' and t2550.C2550_EXPIRY_DATE  between sysdate+61 and sysdate+90)
         	   			OR (p_load_fl = 'others' and t2550.C2550_EXPIRY_DATE > sysdate+91))
         		   AND t205.c205_part_number_id = t2550.c205_part_number_id
         		   AND t205.c202_project_id   = t202.c202_project_id
				   AND t2550.c901_last_updated_warehouse_type not in ('4000339','56002','106221','108020','108000')
    	 	       AND t2550.c205_part_number_id = p_part_num
    	 		   AND t202.c202_project_id= decode(p_project_id,'0',t202.c202_project_id,p_project_id)
    	 		   AND ((p_exclude_quar = 'Y' and t2550.c901_last_updated_warehouse_type not in (90813)) OR (p_exclude_quar = 'N' and t2550.c901_last_updated_warehouse_type = t2550.c901_last_updated_warehouse_type)) 
    		  GROUP BY t2550.c205_part_number_id,t205.c205_part_num_desc,t901.c901_code_nm,
    		  		   t5040.c5040_plant_name,t2550.c901_last_updated_warehouse_type,t2550.c5040_plant_id,t2550.c1900_company_id			  
    		  		   );
	  
  END gm_fch_tissue_inventory_details;
  
 /*******************************************************************************
   * Description : Procedure to fecth data for Lot Expiry dashboard Consignment
   * 				details based on tissue parts
   * Author      : Agilan Sinagravel
  *******************************************************************************/ 
  PROCEDURE gm_fch_tissue_consignment_details(
  		p_part_num		IN		t205_part_number.c205_part_number_id%type,
  		p_load_fl		IN		VARCHAR2,
  		p_type	        IN		VARCHAR2,
  		p_project_id    IN      t202_project.c202_project_id%type,
  		p_exclude_quar	IN		VARCHAR2,
  		p_like_search	IN		VARCHAR2,
  		p_expired_lots	OUT		CLOB
  )
  AS
  BEGIN
	  SELECT JSON_ARRAYAGG(JSON_OBJECT(
	 		        'PARTNUM' VALUE PARTNUM,
	   				'PARTDESC' VALUE PARTDESC,
  				  	'PLANTNAME' VALUE PLANTNAME,
  				  	'WAREHOUSEID' VALUE WAREHOUSEID,
  				  	'WAREHOUSETYPE' VALUE WAREHOUSETYPE,
  				  	--'QTY' VALUE 1,
  				  	'TYPE' VALUE p_type,
  				  	'QTY' VALUE QTY,
  				  	'COMPANYID' VALUE companyid,
  				  	'PLANTID' VALUE plantid,
  				  	'LOADFL' VALUE p_load_fl
  			 	)RETURNING CLOB) INTO p_expired_lots
  		  FROM (
  		 		SELECT t2550.c205_part_number_id PARTNUM,
  		 			   REGEXP_REPLACE(t205.c205_part_num_desc,'[^ ,0-9A-Za-z]', '') PARTDESC,
  		 			   t5040.c5040_plant_name PLANTNAME,
  		 			   t2550.c901_last_updated_warehouse_type WAREHOUSEID,
  		 			   t901.c901_code_nm WAREHOUSETYPE,
  		 			   t2550.c5040_plant_id plantid,
  		 			   t2550.c1900_company_id companyid,
  		 			   COUNT(t2550.c2550_control_number) QTY
  		 	      FROM T202_PROJECT t202, T205_PART_NUMBER t205, T2550_PART_CONTROL_NUMBER t2550,
	    	 		   T901_CODE_LOOKUP t901,T5040_PLANT_MASTER t5040,V205_TISSUE_PARTS v205
  		  		 WHERE v205.c205_part_number_id = t2550.c205_part_number_id
         		   AND t901.c901_code_id = t2550.c901_last_updated_warehouse_type
         		   AND t5040.c5040_plant_id = t2550.c5040_plant_id
         		   AND ((p_load_fl = 'expired' and t2550.c2550_expiry_date < sysdate) 
         	   			OR (p_load_fl = '30Days' and t2550.C2550_EXPIRY_DATE  between sysdate+0 and sysdate+30) 
         	   			OR (p_load_fl = '60Days' and t2550.C2550_EXPIRY_DATE  between sysdate+31 and sysdate+60)
         	   			OR (p_load_fl = '90Days' and t2550.C2550_EXPIRY_DATE  between sysdate+61 and sysdate+90)
         	   			OR (p_load_fl = 'others' and t2550.C2550_EXPIRY_DATE > sysdate+91))
         		   AND t205.c205_part_number_id = t2550.c205_part_number_id
         		   AND t205.c202_project_id   = t202.c202_project_id
				   AND t2550.c901_last_updated_warehouse_type = '26240819'     
    	 	       AND t2550.c205_part_number_id = p_part_num
    	 		   AND t202.c202_project_id= decode(p_project_id,'0',t202.c202_project_id,p_project_id)
    	 		   AND ((p_exclude_quar = 'Y' and t2550.c901_last_updated_warehouse_type not in (90813)) OR (p_exclude_quar = 'N' and t2550.c901_last_updated_warehouse_type = t2550.c901_last_updated_warehouse_type)) 
    		  GROUP BY t2550.c205_part_number_id,t205.c205_part_num_desc,t901.c901_code_nm,
    		  		   t5040.c5040_plant_name,t2550.c901_last_updated_warehouse_type,t2550.c5040_plant_id,t2550.c1900_company_id			  
    		  		   );
	  
  END gm_fch_tissue_consignment_details;
  /*********************************************************************************************************
   * Description : Procedure to fecth data for Lot Expiry dashboard popup on Loaner Set based on tissue parts
   * Author      : Agilan Sinagravel
  ***********************************************************************************************************/ 
  PROCEDURE gm_fch_tissue_loaner_set_lot_details(
  		p_part_num		IN		t205_part_number.c205_part_number_id%type,
  		p_load_fl		IN		VARCHAR2,
  		p_type	        IN		VARCHAR2,
  		p_project_id    IN      t202_project.c202_project_id%type,
  		p_like_search	IN		VARCHAR2,
  		p_expired_lots	OUT		CLOB
  )
  AS
  v_out_Details 	CLOB;
  v_out_heder_dtls 	CLOB;
  BEGIN
	  
	  SELECT JSON_ARRAYAGG(JSON_OBJECT(
	    			'PARTNUM' VALUE PARTNUM,
	   				'PARTDESC' VALUE PARTDESC,
  				  	'PLANTNAME' VALUE PLANTNAME,
  				  	'WAREHOUSEID' VALUE WAREHOUSEID, 			--loaner qty warehouse
  				  	'WAREHOUSETYPE' VALUE WAREHOUSETYPE,
  				  	'QTY' VALUE QTY,
  				  	'TYPE' VALUE TYPE,
  				  	'COMPANYID' VALUE companyid,
  				  	'PLANTID' VALUE plantid,
  				  	'LOADFL' VALUE p_load_fl
  			 )RETURNING CLOB) INTO v_out_Details 
  	   FROM  (
  	 SELECT  t5072.c205_part_number_id PARTNUM,
	   		 t504.c207_set_id PARTDESC,
  			 t207.c207_set_nm PLANTNAME,
  			 '106221' WAREHOUSEID , 			--loaner qty warehouse
  			 t5040.c5040_plant_name WAREHOUSETYPE,
  			 sum(t5072.C5072_CURR_QTY) QTY,
  			 p_type TYPE,
  			 t504.c5040_plant_id PLANTID,
  			 t504.c1900_company_id COMPANYID
	   FROM  t5072_set_part_lot_qty t5072, T205_PART_NUMBER t205, T2550_PART_CONTROL_NUMBER t2550, t5040_plant_master t5040,t207_set_master t207, t202_project t202
	   		 ,t504_consignment t504,t5070_set_lot_master t5070,V205_TISSUE_PARTS v205
	  WHERE  t5072.C205_PART_NUMBER_ID = t205.c205_part_number_id
	   	AND  v205.c205_part_number_id = t2550.c205_part_number_id
	    AND  t5072.c5072_control_number = t2550.C2550_CONTROL_NUMBER 
	    AND  t2550.C205_PART_NUMBER_ID = t205.C205_PART_NUMBER_ID
        AND  t5072.c5072_last_updated_txn_id = t504.c504_consignment_id
	    AND  t5070.c901_txn_id = t504.c504_consignment_id
        AND  t5072.c5070_set_lot_master_id = t5070.c5070_set_lot_master_id
        AND  t504.c5040_plant_id = t5040.c5040_plant_id
        AND  t207.c207_set_id = t504.c207_set_id
        AND  t202.c202_project_id = t205.c202_project_id
        AND ((p_load_fl = 'expired' and t2550.c2550_expiry_date < sysdate) 
         	   OR (p_load_fl = '30Days' and t2550.C2550_EXPIRY_DATE  between sysdate+0 and sysdate+30) 
         	   OR (p_load_fl = '60Days' and t2550.C2550_EXPIRY_DATE  between sysdate+31 and sysdate+60)
         	   OR (p_load_fl = '90Days' and t2550.C2550_EXPIRY_DATE  between sysdate+61 and sysdate+90)
         	   OR (p_load_fl = 'others' and t2550.C2550_EXPIRY_DATE > sysdate+91))
		AND  t5072.c205_part_number_id = p_part_num 
        AND  t202.c202_project_id= decode(p_project_id,'0',t202.c202_project_id,p_project_id)  
        AND  t5072.c5072_void_fl IS NULL
        AND  c504_void_fl IS NULL
        AND  c5070_void_fl IS NULL
        AND  t5040.c5040_void_fl IS null
   GROUP BY  t5072.c205_part_number_id,t504.c207_set_id,t207.c207_set_nm,t5040.c5040_plant_name,
   			 t504.c5040_plant_id,t504.c1900_company_id
   
      UNION 
      
      SELECT t5072.c205_part_number_id PARTNUM,
	   		 t504.c207_set_id PARTDESC,
  			 t207.c207_set_nm PLANTNAME,
  			 '106221' WAREHOUSEID , 			--loaner qty warehouse
  			 t5040.c5040_plant_name WAREHOUSETYPE,
  			 sum(t5072.C5072_CURR_QTY) QTY,
  			 p_type TYPE,
  			 t504.c5040_plant_id PLANTID,
  			 t504.c1900_company_id COMPANYID
	   FROM  t5072_set_part_lot_qty t5072, T205_PART_NUMBER t205, T2550_PART_CONTROL_NUMBER t2550, t5040_plant_master t5040,t207_set_master t207, t202_project t202
	   		 ,t504_consignment t504,t5070_set_lot_master t5070,V205_TISSUE_PARTS v205,t412_inhouse_transactions t412
	  WHERE  t5072.C205_PART_NUMBER_ID = t205.c205_part_number_id
	   	AND  v205.c205_part_number_id = t2550.c205_part_number_id
	    AND  t5072.c5072_control_number = t2550.C2550_CONTROL_NUMBER 
	    AND  t2550.C205_PART_NUMBER_ID = t205.C205_PART_NUMBER_ID
	    AND  t5072.c5072_last_updated_txn_id = t412.c412_inhouse_trans_id
        AND  t504.c504_consignment_id = t412.c412_ref_id
	    AND  t5070.c901_txn_id = t504.c504_consignment_id
        AND  t5072.c5070_set_lot_master_id = t5070.c5070_set_lot_master_id
        AND  t504.c5040_plant_id = t5040.c5040_plant_id
        AND  t207.c207_set_id = t504.c207_set_id
        AND  t202.c202_project_id = t205.c202_project_id
        AND ((p_load_fl = 'expired' and t2550.c2550_expiry_date < sysdate) 
         	   OR (p_load_fl = '30Days' and t2550.C2550_EXPIRY_DATE  between sysdate+0 and sysdate+30) 
         	   OR (p_load_fl = '60Days' and t2550.C2550_EXPIRY_DATE  between sysdate+31 and sysdate+60)
         	   OR (p_load_fl = '90Days' and t2550.C2550_EXPIRY_DATE  between sysdate+61 and sysdate+90)
         	   OR (p_load_fl = 'others' and t2550.C2550_EXPIRY_DATE > sysdate+91))
		AND  t5072.c205_part_number_id = p_part_num
        AND  t202.c202_project_id= decode(p_project_id,'0',t202.c202_project_id,p_project_id)  
        AND  t5072.c5072_void_fl IS NULL
        AND  c504_void_fl IS NULL
        AND  c5070_void_fl IS NULL
        AND  t5040.c5040_void_fl IS null
        AND  c412_void_fl IS NULL
   GROUP BY  t5072.c205_part_number_id,t504.c207_set_id,t207.c207_set_nm,t5040.c5040_plant_name,
   			 t504.c5040_plant_id,t504.c1900_company_id);
   
    SELECT p_type
      INTO v_out_heder_dtls 
      FROM dual;
       
       p_expired_lots := v_out_Details || '^'|| v_out_heder_dtls;
       
  END gm_fch_tissue_loaner_set_lot_details;
  
  
   /*******************************************************************************
   * Description : Procedure to fecth data for Lot Expiry email notification
   * Author      : Sindhu
   *******************************************************************************/ 
  PROCEDURE gm_fch_expired_lot_mail_internal(
  		p_type		    IN		VARCHAR2,
  		p_out_cursor	OUT		types.cursor_type
  )
  AS
  		v_date_fmt VARCHAR2(50);
 BEGIN
	    SELECT get_compdtfmt_frm_cntx()
        INTO v_date_fmt
        FROM dual;

	IF p_type = '15DAYS' THEN
		OPEN p_out_cursor FOR
			 SELECT t205.c205_part_number_id PNUM,
			  	    t205.c205_part_num_desc PDESC,
			  		TO_CHAR(t2550.c2550_expiry_date, v_date_fmt) EXPIRYDT,
			  		t5080.c5080_location_name LOCATION,
			  		get_code_name(t5080.c901_warehouse_type) WAREHOUSE,
			  		t2550.c2550_control_number CNUM,
			  		SUM(t5080.c5080_qty) QTY
			   FROM T2550_PART_CONTROL_NUMBER t2550,
			  		T205_PART_NUMBER t205,
			  		T5080_LOT_TRACK_INV t5080
			  WHERE t2550.c205_part_number_id = t205.c205_part_number_id
				AND t5080.c205_part_number_id = t205.c205_part_number_id
				AND t5080.c2550_control_number = t2550.c2550_control_number
				AND t5080.c901_warehouse_type NOT IN ('4000339','56002','26240819')
				AND t5080.c5080_qty > 0
				AND t2550.C2550_expiry_date BETWEEN CURRENT_DATE-1 AND CURRENT_DATE+14
				AND t5080.c5080_void_fl IS NULL
		   GROUP BY t205.c205_part_number_id,t205.c205_part_num_desc,t2550.c2550_expiry_date,
			  		c901_warehouse_type,t5080.c5080_location_name,t2550.c2550_control_number
		   ORDER BY t2550.c2550_expiry_date;
	END IF;
	
	IF p_type='30DAYS' THEN
		OPEN p_out_cursor FOR 
			SELECT t205.c205_part_number_id PNUM,
			  	   t205.c205_part_num_desc PDESC,
			  	   TO_CHAR(t2550.c2550_expiry_date, v_date_fmt) EXPIRYDT,
			  	   t5080.c5080_location_name LOCATION,
			  	   get_code_name(t5080.c901_warehouse_type) WAREHOUSE,
			  	   t2550.c2550_control_number CNUM,
			  	   SUM(t5080.c5080_qty) QTY
			  FROM T2550_PART_CONTROL_NUMBER t2550,
			  	   T205_PART_NUMBER t205,
			  	   T5080_LOT_TRACK_INV t5080
			 WHERE t2550.c205_part_number_id = t205.c205_part_number_id
			   AND t5080.c205_part_number_id = t205.c205_part_number_id
			   AND t5080.c2550_control_number = t2550.c2550_control_number
			   AND t5080.c901_warehouse_type NOT IN ('4000339','56002','26240819')
			   AND t5080.c5080_qty > 0
			   AND t2550.C2550_expiry_date  between current_date+15 and current_date+29
			   AND t5080.c5080_void_fl IS NULL
		  GROUP BY t205.c205_part_number_id,t205.c205_part_num_desc,t2550.c2550_expiry_date,
			  	   c901_warehouse_type,t5080.c5080_location_name,t2550.c2550_control_number
		  ORDER BY t2550.c2550_expiry_date;  	   
	END IF;

	IF p_type = 'EXPIRED' THEN
		OPEN p_out_cursor FOR 
			SELECT t205.c205_part_number_id PNUM,
			  	   t205.c205_part_num_desc PDESC,
			  	   TO_CHAR(t2550.c2550_expiry_date, v_date_fmt) EXPIRYDT,
			  	   t5080.c5080_location_name LOCATION,
			  	   get_code_name(t5080.c901_warehouse_type) WAREHOUSE,
			  	   t2550.c2550_control_number CNUM,
			       SUM(t5080.c5080_qty) QTY
			  FROM T2550_PART_CONTROL_NUMBER t2550,
			  	   T205_PART_NUMBER t205,
			       T5080_LOT_TRACK_INV t5080
			 WHERE t2550.c205_part_number_id = t205.c205_part_number_id
			   AND t5080.c205_part_number_id = t205.c205_part_number_id
			   AND t5080.c2550_control_number = t2550.c2550_control_number
			   AND t5080.c901_warehouse_type NOT IN ('4000339','56002','26240819')
			   AND t5080.c5080_qty > 0
			   AND t2550.C2550_expiry_date < CURRENT_DATE-1
			   AND t5080.c5080_void_fl IS NULL
		  GROUP BY t205.c205_part_number_id,t205.c205_part_num_desc,t2550.c2550_expiry_date,
		  		   c901_warehouse_type,t5080.c5080_location_name,t2550.c2550_control_number
		  ORDER BY t2550.c2550_expiry_date;	   
	END IF;
	
	
  END gm_fch_expired_lot_mail_internal;
  
  
  /****************************************************************************************************************
   * Description : Procedure to fecth Lot Expiry reports of sterile parts to send notification email to external
   * Author      : T.S. Ramachandiran
  *****************************************************************************************************************/ 
  
  PROCEDURE gm_fch_expired_lot_mail_external(
  		p_type		    IN		VARCHAR2,
  		p_out_cursor	OUT		types.cursor_type
  )
 AS
 	v_date_fmt VARCHAR2(50);
 	
 BEGIN
	 	SELECT get_compdtfmt_frm_cntx()
        INTO v_date_fmt
        FROM dual;
     --To fetch Lot expiry details for within 15 days 
       IF p_type = '15DAYS' THEN
        OPEN p_out_cursor FOR 
        	SELECT  t205.c205_part_number_id PNUM,
         			t205.c205_part_num_desc PDESC,
					t2550.c2550_control_number CNUM, 
					to_char(t2550.c2550_expiry_date,v_date_fmt) EXPIRYDT,
					t5080.c5080_location_name LOCNAME,
					get_code_name(T5080.c901_warehouse_type) WAREHOUSE,
					t5080.c5080_location_id DISTID,
					NVL(get_distributor_email(t5080.c5080_location_id),get_rep_emailid(t5080.c5080_location_id)) TOMAIL, 
					SUM(t5080.C5080_QTY) QTY 
			  FROM 	T2550_PART_CONTROL_NUMBER t2550,
		  			T205_PART_NUMBER t205,
		  			T5080_LOT_TRACK_INV t5080
			 WHERE 	t2550.c205_part_number_id = t205.c205_part_number_id
			   AND 	t5080.c205_part_number_id = t205.c205_part_number_id
			   AND 	t5080.c2550_control_number = t2550.c2550_control_number
			   AND  t5080.c901_warehouse_type in ('4000339','56002','26240819')
			   AND  t5080.c5080_qty > 0
			   AND  t2550.C2550_expiry_date BETWEEN CURRENT_DATE-1 AND CURRENT_DATE+14
			   AND  t5080.c5080_void_fl IS NULL
		  GROUP BY  t205.c205_part_number_id,
		  		    t205.c205_part_num_desc,
		  		    t2550.c2550_control_number,
		  		    t2550.c2550_expiry_date,
		  		    t5080.c901_warehouse_type,
		  		    t5080.c5080_location_name,
		  		    t5080.c5080_location_id
		   ORDER BY T5080.c5080_location_id,t2550.c2550_expiry_date;
	 END IF;
	 
	  --To fetch Lot expiry details from current date to next 30 days 
	 IF p_type = '30DAYS' THEN
        OPEN p_out_cursor FOR 
        	SELECT  t205.c205_part_number_id PNUM,
         			t205.c205_part_num_desc PDESC,
					t2550.c2550_control_number CNUM, 
					to_char(t2550.c2550_expiry_date,v_date_fmt) EXPIRYDT,
					t5080.c5080_location_name LOCNAME,
					get_code_name(T5080.c901_warehouse_type) WAREHOUSE,
					t5080.c5080_location_id DISTID,
					NVL(get_distributor_email(t5080.c5080_location_id),get_rep_emailid(t5080.c5080_location_id)) TOMAIL, 
					SUM(t5080.c5080_qty) QTY 
			  FROM  T2550_PART_CONTROL_NUMBER t2550,
		  			T205_PART_NUMBER t205,
		  			T5080_LOT_TRACK_INV t5080
			 WHERE  t2550.c205_part_number_id = t205.c205_part_number_id
			   AND  t5080.c205_part_number_id = t205.c205_part_number_id
			   AND  t5080.c2550_control_number = t2550.c2550_control_number
			   AND  t5080.c901_warehouse_type in ('4000339','56002','26240819')
			   AND  t5080.c5080_qty > 0
			   AND  t2550.C2550_expiry_date BETWEEN CURRENT_DATE+15 AND CURRENT_DATE+29
			   AND  t5080.c5080_void_fl IS NULL
		  GROUP BY  t205.c205_part_number_id,
		  			t205.c205_part_num_desc,
		  			t2550.c2550_control_number,
		  			t2550.c2550_expiry_date,
		  			t5080.c901_warehouse_type,
		  			t5080.c5080_location_name,
		  			t5080.c5080_location_id
		  ORDER BY  T5080.c5080_location_id,t2550.c2550_expiry_date;
	 END IF;
	 
	 --To fetch Lot expiry details for expired date 
	 IF p_type = 'EXPIRED' THEN
        OPEN p_out_cursor FOR 
        	SELECT  t205.c205_part_number_id PNUM,
         			t205.c205_part_num_desc PDESC,
					t2550.c2550_control_number CNUM, 
					to_char(t2550.c2550_expiry_date,v_date_fmt) EXPIRYDT,
					t5080.c5080_location_name LOCNAME,
					get_code_name(T5080.c901_warehouse_type) WAREHOUSE,
					t5080.c5080_location_id DISTID,
					NVL(get_distributor_email(t5080.c5080_location_id),get_rep_emailid(t5080.c5080_location_id)) TOMAIL, 
					SUM(t5080.c5080_qty) QTY 
			  FROM  T2550_PART_CONTROL_NUMBER t2550,
		  			T205_PART_NUMBER t205,
		  			T5080_LOT_TRACK_INV t5080
			 WHERE  t2550.c205_part_number_id = t205.c205_part_number_id
			   AND  t5080.c205_part_number_id = t205.c205_part_number_id
			   AND  t5080.c2550_control_number = t2550.c2550_control_number
			   AND  t5080.c901_warehouse_type in ('4000339','56002','26240819')
			   AND  t5080.c5080_qty > 0
			   AND  t2550.C2550_expiry_date < CURRENT_DATE -1
			   AND  t5080.c5080_void_fl IS NULL
		  GROUP BY 	t205.c205_part_number_id,
		  			t205.c205_part_num_desc,
		  			t2550.c2550_control_number,
		  			t2550.c2550_expiry_date,
		  			t5080.c901_warehouse_type,
		  			t5080.c5080_location_name,
		  			t5080.c5080_location_id
		  ORDER BY  T5080.c5080_location_id,t2550.c2550_expiry_date;
	 END IF;
	END gm_fch_expired_lot_mail_external;
  
 
END gm_pkg_op_lot_expiry_report;
/