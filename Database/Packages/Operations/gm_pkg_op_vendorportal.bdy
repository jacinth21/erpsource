--@C:\pmt\Database\Packages\Operations\gm_pkg_op_vendorportal.bdy;

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_vendorportal
IS

/***********************************************************
 	 *Auther: Manigandan 
     * Description : Procedure to get the Purchase order ID and Vendor ID 
	 * to load Po Paperwork
     * by using GuID
     ***********************************************************/
     
procedure gm_fch_podetails_by_guid ( 
   	      p_guid      IN    t401_purchase_order.C401_GUID%TYPE,
          p_out_cur   OUT   TYPES.cursor_type 
            )          
   AS     
		--PC-2851 Ability to Print All WO's Paperwork in a PO
		v_date_fmt VARCHAR2 (20) ;
   
     BEGIN
     
		--PC-2851 Ability to Print All WO's Paperwork in a PO
		SELECT get_compdtfmt_frm_cntx ()
       			INTO v_date_fmt
       			FROM dual;
       			     
      OPEN p_out_cur 
       FOR
			--PC-2851 Ability to Print All WO's Paperwork in a PO
			SELECT POID, VENDORID, REVNUM DOCREV, GET_DOC_FOOTER (TO_CHAR(C402_CREATED_DATE,v_date_fmt), 'GM-G001') DOCFOOTER
			FROM (SELECT t401.c401_purchase_ord_id POID, t401.C301_VENDOR_ID VENDORID, C402_CREATED_DATE, c402_rev_num REVNUM
				FROM t401_purchase_order t401, t402_work_order t402
				WHERE t401.C401_GUID = p_guid
				AND t401.c401_purchase_ord_id = t402.c401_purchase_ord_id
				AND t402.c402_void_fl          IS NULL
				AND t401.C401_VOID_FL          IS NULL
				AND c402_rev_num               IS NOT NULL
				ORDER BY c402_rev_num DESC) WHERE rownum =1;
          
      END gm_fch_podetails_by_guid; 
      
     /***********************************************************
 	 *Auther: Manigandan 
     * Description : Procedure to get the Work Order ID, Rev Num and DocFOoter 
	 * to load the Wo paperwork
     * by using GuID
     ***********************************************************/
	 
      
 procedure gm_fch_wodetails_by_guid (  
   	    p_guid       IN     t402_work_order.C402_GUID%TYPE,
        p_out_cur    OUT    TYPES.cursor_type
    	)
   AS  
   v_date_fmt VARCHAR2 (20) ;
      
     BEGIN
     
     SELECT get_compdtfmt_frm_cntx ()
       			INTO v_date_fmt
       			FROM dual;
       			
       OPEN p_out_cur
        FOR
          SELECT c402_work_order_id WOID,  c402_rev_num DOCREV,
                 GET_DOC_FOOTER (TO_CHAR(C402_CREATED_DATE,v_date_fmt), 'GM-G001') DOCFOOTER
          FROM t402_work_order t402
               WHERE t402.C402_GUID = p_guid
                        and t402.c402_void_fl is null;
            
     END gm_fch_wodetails_by_guid;
 END gm_pkg_op_vendorportal;
 /                   
                