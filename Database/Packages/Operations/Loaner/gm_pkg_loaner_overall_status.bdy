-- @"C:\database\Packages\Operations\Loaner\gm_pkg_loaner_overall_status.bdy"

CREATE OR REPLACE
PACKAGE BODY gm_pkg_loaner_overall_status
AS

/**************************************************************************************
* Description :  This procedure is used to fetch the Loaner Overall details
* Author      :  gpalani
* PMT#        :  PMT-40258
* Date        :  12-Feb-2020   
***************************************************************************************/

PROCEDURE gm_fch_loaner_overall (  
            p_loaner_type IN t504_consignment.c504_type%TYPE,   
            p_out_loaner_dtls OUT CLOB)
            
AS
    v_company_id t1900_company.c1900_company_id%type;
    v_plant_id   t5040_plant_master.C5040_PLANT_ID%type;
    v_date_fmt VARCHAR2 (100) ;
    v_comp_Plant_filter t1900_company.c1900_company_id%type;
    
BEGIN
    -- to get the date format
     SELECT get_compdtfmt_frm_cntx
       INTO v_date_fmt
       FROM dual;
       
    -- to get the company info
     SELECT get_compid_frm_cntx (), get_plantid_frm_cntx
       INTO v_company_id,v_plant_id
       FROM dual; 
    
     -- LOANER_OVERALLBY_ST- To fetch the records for EDC entity countries based on plant
      
   BEGIN
      SELECT DECODE (get_rule_value (v_company_id, 'LOANER_OVERALLBY_ST'), 'Plant', v_plant_id,v_company_id)
         INTO v_comp_Plant_filter
   		FROM dual;
   		
   	EXCEPTION WHEN OTHERS THEN
   	   v_comp_Plant_filter:=v_company_id;
   	END;
   
    SELECT JSON_ARRAYAGG (JSON_OBJECT ( 
        'Set_ID'      value loaner_overall.set_id,
        'Set_Name'    value loaner_overall.set_name,
        'Pnd_Rtn'     value loaner_overall.pnd_rtn,
	     'Avail'       value loaner_overall.avail,
        'OpenReq'     value loaner_overall.openreq,
        'Pnd_Apr_Req' value loaner_overall.pnd_apr_req,
        'Alloc'       value loaner_overall.alloc, 
        'Pnd_Ship'    value loaner_overall.pnd_ship, 
        'PIP'         value loaner_overall.pip,
        'RFP'         value loaner_overall.rfp, 
        'Missed'      value loaner_overall.missed, 
        'Pnd_Chk'     value loaner_overall.pnd_chk, 
        'WIP'         value loaner_overall.wip,
        'Pnd_Vrf'     value loaner_overall.pnd_vrf, 
        'Pnd_Prc'     value loaner_overall.pnd_prc, 
        'Pnd_Pic'     value loaner_overall.pnd_pic,
        'Pnd_Fg'      value loaner_overall.Pnd_FG,
        'Pen_Accept'  value loaner_overall.Pnd_Accept,
        'Pen_Pick'  value loaner_overall.Pnd_Pick,
        'Deployed'    value loaner_overall.Deployed,
        'Disputed'    value loaner_overall.Disputed,
        'Total'     value '0'
        
        ) 
    ORDER BY loaner_overall.Set_Name ASC returning CLOB)   
    
     INTO p_out_loaner_dtls
FROM 
   (
 SELECT loaner_set_pivot.ID Set_ID, t207.C207_SET_NM Set_Name,
    NVL(loaner_set_pivot.Pending_Return_NAME_COUNT,0)Pnd_Rtn,
    NVL (loaner_set_pivot.Available_NAME_COUNT, 0) Avail, NVL (loaner_set_pivot.Open_REQ_NAME_COUNT, 0) OpenReq, NVL (
    loaner_set_pivot.Pending_REQ_NAME_COUNT, 0) Pnd_Apr_Req, NVL (loaner_set_pivot.Allocated_NAME_COUNT, 0) Alloc, NVL
    (loaner_set_pivot.Pending_Ship_NAME_COUNT, 0) Pnd_Ship, NVL (loaner_set_pivot.Pending_In_Progres_NAME_COUNT, 0) PIP
    , NVL (loaner_set_pivot.RFP_NAME_COUNT, 0) RFP, NVL (loaner_set_pivot.Deployed_NAME_COUNT, 0) Deployed, NVL (
    loaner_set_pivot.Missed_NAME_COUNT, 0) Missed, NVL (loaner_set_pivot.Pending_Chk_NAME_COUNT, 0) Pnd_Chk, NVL (
    loaner_set_pivot.WIP_NAME_COUNT, 0) WIP, NVL (loaner_set_pivot.Pending_Verify_NAME_COUNT, 0) Pnd_Vrf, NVL (
    loaner_set_pivot.Pending_Process_NAME_COUNT, 0) Pnd_Prc, NVL (loaner_set_pivot.Pending_Pic_NAME_COUNT, 0) Pnd_Pic,
    NVL (loaner_set_pivot.Pending_Pick_NAME_COUNT, 0) Pnd_Pick,
    NVL (loaner_set_pivot.Pnd_Accept_NAME_COUNT, 0) Pnd_Accept,
    NVL (loaner_set_pivot.Pending_FG_NAME_COUNT, 0) Pnd_FG,
    NVL (loaner_set_pivot.Disputed_NAME_COUNT, 0) Disputed
    
   FROM
    (
         SELECT T504.C207_SET_ID ID,  get_rule_value ( T504A.C504A_STATUS_FL, 'LOANS') NAME
          , COUNT (1) COUNT
           FROM t504_consignment t504, t504a_consignment_loaner t504a
          WHERE t504a.c504_consignment_id = t504.c504_consignment_id
            AND t504.c504_type            = p_loaner_type
            AND T504.c504_void_fl        IS NULL
            AND t504a.c504a_void_fl IS NULL
            AND t504a.c504a_status_fl    != 60
            AND t504.c207_set_id         IS NOT NULL
            AND (t504.c1900_company_id    = v_comp_Plant_filter
            OR t504.c5040_plant_id        = v_comp_Plant_filter)
            AND t504.c5040_plant_id       = T504A.C5040_PLANT_ID
       GROUP BY t504.c207_set_id, t504a.c504a_status_fl
      UNION ALL
      
          
      SELECT open_req_count.id set_id, CASE
        WHEN open_req_count.status_fl = '10'
        THEN 'Opn Req'
        WHEN open_req_count.status_fl = '5'
        THEN 'Pnd Apr Req'
    END AS NAME, CASE
        WHEN open_req_count.status_fl IS NOT NULL
        THEN open_req_count.count
    END AS COUNT
   FROM
    (
         SELECT t504.c207_set_id id, open_count.open_sum COUNT, open_count.status_fl status_fl
           FROM t504_consignment t504, t504a_consignment_loaner t504a, (
                 SELECT t526.c207_set_id set_id, SUM (c526_qty_requested) open_sum, t526.c526_status_fl status_fl
                   FROM t526_product_request_detail t526
                  WHERE t526.c901_request_type = p_loaner_type
                    AND t526.c526_status_fl  IN (10, 5) -- Loaner Request Open and Pending Request
                    AND t526.c207_set_id      IS NOT NULL
                    AND t526.c526_void_fl     IS NULL
                    AND (t526.c1900_company_id = v_comp_Plant_filter
                    OR t526.c5040_plant_id     = v_comp_Plant_filter)
               GROUP BY t526.c207_set_id, t526.c526_status_fl
            )
            open_count
          WHERE t504a.c504_consignment_id = t504.c504_consignment_id
            AND t504.c504_type            = p_loaner_type
            AND t504.c504_void_fl        IS NULL
            AND t504.c207_set_id          = open_count.set_id(+)
            AND t504.c207_set_id         IS NOT NULL
            AND t504a.c504a_status_fl    != 60 -- Loaner status 60- Not available
            AND (t504.c1900_company_id    = v_comp_Plant_filter
            OR t504.c5040_plant_id        = v_comp_Plant_filter)
            AND t504.c5040_plant_id       = t504a.c5040_plant_id
       GROUP BY t504.c207_set_id, open_count.status_fl, open_count.open_sum
    )
    open_req_count) 
      
  Loaner_Set PIVOT (SUM (COUNT)
AS
    name_count FOR Name IN ('Pnd Rtn' Pending_Return, 'Avail' Available, 'Opn Req' Open_REQ, 'Pnd Apr Req' Pending_REQ,
    'Pen Pick' Pending_Pick, 'Alloc' Allocated, 'Pnd Shp' Pending_Ship, 'PIP' Pending_In_Progres, 'RFP' RFP, 'Deployed'
    Deployed, 'Missed' Missed, 'Pnd Chk' Pending_Chk, 'WIP' WIP, 'Pnd Vrf'Pending_Verify, 'Pnd Prc' Pending_Process,
    'Pnd Accept' Pnd_Accept, 'Pnd Pic' Pending_Pic, 'Pen FG' Pending_FG, 'Disputed' Disputed )) loaner_set_pivot, t207_set_master t207 WHERE loaner_set_pivot.id =
t207.c207_set_id AND t207.c207_void_fl IS NULL) loaner_overall;

     
   END gm_fch_loaner_overall;
END gm_pkg_loaner_overall_status; 
/