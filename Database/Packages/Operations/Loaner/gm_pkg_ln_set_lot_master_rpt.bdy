
-- @"C:\database\Packages\Operations\Loaner\gm_pkg_ln_set_lot_master_rpt.bdy"

CREATE OR REPLACE
PACKAGE BODY gm_pkg_ln_set_lot_master_rpt
IS
/******************************************************************
    * Description : Procedure to fetch  part number, control number and quantity details for cosignment set
 ****************************************************************/
PROCEDURE Gm_fch_set_part_cnum_details (
         p_consign_id IN t5070_set_lot_master.C901_TXN_ID%TYPE,
         p_out_cur    OUT TYPES.cursor_type)
AS
BEGIN
   	
	OPEN p_out_cur FOR

			SELECT t5072.C205_PART_NUMBER_ID PNUM,
			  t5072.c5072_control_number CNUM,
			  SUM(t5072.c5072_curr_qty) QTY,
			  get_partdesc_by_company(t5072.c205_part_number_id) PDESC,
			  decode(GM_PKG_PD_RPT_UDI.GET_PART_DI_NUMBER(t5072.C205_PART_NUMBER_ID),'',decode(GM_PKG_OP_UDI_LABEL.GET_EXPIRY_DATE_AS_CHAR(t5072.C205_PART_NUMBER_ID, t5072.c5072_control_number),'','10' ||  t5072.c5072_control_number,'17' || GM_PKG_OP_UDI_LABEL.GET_EXPIRY_DATE_AS_CHAR(t5072.C205_PART_NUMBER_ID, t5072.c5072_control_number) || '10' ||  t5072.c5072_control_number),decode(GM_PKG_OP_UDI_LABEL.GET_EXPIRY_DATE_AS_CHAR(t5072.C205_PART_NUMBER_ID, t5072.c5072_control_number),'','01' || GM_PKG_PD_RPT_UDI.GET_PART_DI_NUMBER(t5072.C205_PART_NUMBER_ID)  || '10' ||  t5072.c5072_control_number,'01' || GM_PKG_PD_RPT_UDI.GET_PART_DI_NUMBER(t5072.C205_PART_NUMBER_ID) || '17' || GM_PKG_OP_UDI_LABEL.GET_EXPIRY_DATE_AS_CHAR(t5072.C205_PART_NUMBER_ID, t5072.c5072_control_number) || '10' ||  t5072.c5072_control_number)) BARCODEEXP,
   			  decode(GM_PKG_PD_RPT_UDI.GET_PART_DI_NUMBER(t5072.C205_PART_NUMBER_ID),'',decode(GM_PKG_OP_UDI_LABEL.GET_EXPIRY_DATE_AS_CHAR(t5072.C205_PART_NUMBER_ID, t5072.c5072_control_number),'','(10)' ||  t5072.c5072_control_number,'(17)' || GM_PKG_OP_UDI_LABEL.GET_EXPIRY_DATE_AS_CHAR(t5072.C205_PART_NUMBER_ID, t5072.c5072_control_number) || '(10)' ||  t5072.c5072_control_number),decode(GM_PKG_OP_UDI_LABEL.GET_EXPIRY_DATE_AS_CHAR(t5072.C205_PART_NUMBER_ID, t5072.c5072_control_number),'','(01)' || GM_PKG_PD_RPT_UDI.GET_PART_DI_NUMBER(t5072.C205_PART_NUMBER_ID)  || '(10)' ||  t5072.c5072_control_number,'(01)' || GM_PKG_PD_RPT_UDI.GET_PART_DI_NUMBER(t5072.C205_PART_NUMBER_ID) || '(17)' || GM_PKG_OP_UDI_LABEL.GET_EXPIRY_DATE_AS_CHAR(t5072.C205_PART_NUMBER_ID, t5072.c5072_control_number) || '(10)' ||  t5072.c5072_control_number)) BARCODEEXPVAL,
    	      GM_PKG_OP_UDI_LABEL.GET_EXPIRY_DATE(t5072.C205_PART_NUMBER_ID, t5072.c5072_control_number) EXPDATE
			FROM t5070_set_lot_master t5070,
			  t5071_set_part_qty t5071,
			  t5072_set_part_lot_qty t5072
			WHERE t5070.C5070_SET_LOT_MASTER_ID = t5071.C5070_SET_LOT_MASTER_ID
			AND t5071.C5071_SET_PART_QTY_ID     = t5072.C5071_SET_PART_QTY_ID
			AND t5070.C901_TXN_ID               = p_consign_id
			AND t5070.c5070_void_fl IS NULL
			AND t5071.c5071_void_fl IS NULL
			AND t5072.c5072_void_fl IS NULL
			AND t5072.c5072_curr_qty >0
			--AS this is a log table, we will have 0 qty,which is not required in the paperwork.
			GROUP BY t5072.C205_PART_NUMBER_ID,t5072.c5072_control_number
			ORDER BY PNUM,CNUM;

END Gm_fch_set_part_cnum_details;


/*********************************************************
* Description : This procedure is used to fetch Part number,Control number and quantity details for loaner details
*********************************************************/
PROCEDURE gm_fch_set_part_lot_qty(
    p_txn_id IN t5070_set_lot_master.C901_TXN_ID%TYPE,
    p_pnum   IN t5071_set_part_qty.C205_PART_NUMBER_ID%TYPE,
    p_out_cur OUT TYPES.cursor_type)
AS
BEGIN
  OPEN p_out_cur FOR SELECT t5072.* , t5072.CNUM||'('|| t5072.CQTY||')' CNUMQTY FROM
	  (SELECT t5072.C205_PART_NUMBER_ID PNUM,
	    t5072.c5072_control_number CNUM,
	    SUM(t5072.c5072_curr_qty) CQTY
	  FROM t5070_set_lot_master t5070,
	    t5071_set_part_qty t5071,
	    t5072_set_part_lot_qty t5072
	  WHERE t5070.C5070_SET_LOT_MASTER_ID = t5071.C5070_SET_LOT_MASTER_ID
	  AND t5071.C5071_SET_PART_QTY_ID     = t5072.C5071_SET_PART_QTY_ID
	  AND t5072.C205_PART_NUMBER_ID       = t5071.C205_PART_NUMBER_ID
	    --for PMT-42614 , BUG-12392 "Lot # to Remove" field drop down displays Lot number with 0 count
	  AND t5070.C901_TXN_ID         = p_txn_id
	  AND t5071.C205_PART_NUMBER_ID = p_pnum
	  AND t5070.c5070_void_fl      IS NULL
	  AND t5071.c5071_void_fl      IS NULL
	  AND t5072.c5072_void_fl      IS NULL
	  GROUP BY t5072.C205_PART_NUMBER_ID,
	    t5072.c5072_control_number
	  ) t5072
	WHERE t5072.cqty <>0;
	--for PMT-42614 , BUG-12392 "Lot # to Remove" field drop down displays Lot number with 0 count
END Gm_fch_set_part_lot_qty;

END gm_pkg_ln_set_lot_master_rpt;
/