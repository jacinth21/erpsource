-- @"C:\database\Packages\Operations\Loaner\gm_pkg_op_loaner_lot_process.bdy"

CREATE OR REPLACE
PACKAGE BODY gm_pkg_op_loaner_lot_process
IS

/*********************************************************
*  Description    : This function validate the lot track flag for the given set id
*  Parameters     : p_code_id
*********************************************************/
FUNCTION validate_set_lot_trk(
	p_set_id   T207_SET_MASTER.C207_SET_ID%TYPE
)
	RETURN VARCHAR2
IS
	v_lot_fl   T207_SET_MASTER.C207_LOT_TRACK_FL%TYPE;
BEGIN
    BEGIN
      SELECT NVL(C207_LOT_TRACK_FL,'N')
        INTO v_lot_fl
        	FROM T207_SET_MASTER
        WHERE  C207_SET_ID = p_set_id 
        AND    C207_VOID_FL IS NULL;  
      RETURN v_lot_fl;
   EXCEPTION
      WHEN NO_DATA_FOUND
   THEN
      RETURN 'N';
   END;
END;


PROCEDURE  gm_is_fch__lnset_detail(
	 p_consign_id IN t504_consignment.c504_consignment_id%TYPE,
     p_out_cur    OUT CLOB
)
AS
v_datefmt VARCHAR2(10);
v_rollback_fl t504a_loaner_transaction.c504a_rollback_fl%TYPE;
BEGIN
	
	SELECT NVL(get_compdtfmt_frm_cntx(),get_rule_value('DATEFMT','DATEFORMAT')) INTO v_datefmt FROM DUAL;

--
	IF p_consign_id IS NOT NULL
	THEN
	--	my_context.set_my_inlist_ctx (p_consign_id);
	
	 
		BEGIN			
			SELECT t504a.c504a_rollback_fl
			  INTO v_rollback_fl
		      FROM t504a_loaner_transaction t504a
		     WHERE t504a.c504a_void_fl IS NULL
		       AND c504a_loaner_transaction_id IN (SELECT MAX (TO_NUMBER (c504a_loaner_transaction_id))
		                                             FROM t504a_loaner_transaction t504a
		                                            WHERE t504a.c504_consignment_id = p_consign_id
		                                              AND t504a.c504a_void_fl IS NULL);
			EXCEPTION WHEN NO_DATA_FOUND THEN
				v_rollback_fl := NULL;
			END;

			SELECT JSON_ARRAYAGG(JSON_OBJECT( 'CONSIGNID' VALUE t504.c504_consignment_id ,'setname' VALUE get_set_name (t504.c207_set_id)
                  ,'setid' VALUE t504.c207_set_id ,'settype' VALUE get_code_name (t504.c504_inhouse_purpose) ,'ctype' VALUE c504_type
                  ,'etchid' VALUE t504a.c504a_etch_id ,'edate' VALUE TO_CHAR (t504a.c504a_expected_return_dt,v_datefmt) 
                 -- ,'eredate' VALUE TO_CHAR(t504a.c504a_expected_return_dt,v_datefmt)
                  ,'ldate' VALUE TO_CHAR (t504a.c504a_loaner_dt,v_datefmt) 
                  ,'loansfl' VALUE gm_pkg_op_loaner.get_loaner_status (t504a.c504a_status_fl)
                  ,'loansflcd' VALUE t504a.c504a_status_fl
                  ,'billnm' VALUE DECODE (get_cs_rep_loaner_bill_nm_add (t504.c504_consignment_id)
                          , ' ', 'Globus Medical'
                          , get_cs_rep_loaner_bill_nm_add (t504.c504_consignment_id)
                           )
                  /*,'distid' VALUE t504.c701_distributor_id ,'distnm' VALUE GET_DISTRIBUTOR_NAME(t504.c701_distributor_id)
                  ,'repid' VALUE t504al.c703_sales_rep_id ,'repnm' VALUE get_rep_name(t504al.c703_sales_rep_id)
                  ,'accid' VALUE t504al.c704_account_id ,'accnm' VALUE get_account_name(t504al.c704_account_id)
                  ,'delearnm' VALUE get_party_name(get_dealer_id_by_acct_id(t504al.c704_account_id))*/
                  , 'returndt' VALUE DECODE (t504a.c504a_status_fl, 25, gm_pkg_op_loaner.get_return_dt (p_consign_id), '')
                  ,'loantransid' VALUE gm_pkg_op_loaner.get_loaner_trans_id (p_consign_id)
               --   ,'emailadd' VALUE get_cs_ship_email (4121, t504al.c703_sales_rep_id)
             --     ,'holdfl' VALUE t504.C504_HOLD_FL
                  ,'COMPANYID' VALUE get_compid_from_distid(t504.C701_DISTRIBUTOR_ID)
                  ,'txntypenm' VALUE get_code_name(c504_type)
                  --,'txnparentid' VALUE gm_pkg_op_loaner.get_loaner_request_id(t504al.c504a_loaner_transaction_id)
                  --,'assocrepnm' VALUE t504al.c703_ass_rep_id
                  --,'assorepid' VALUE get_rep_name(t504al.c703_ass_rep_id)
                 -- ,'setpurpose' VALUE t504.c504_inhouse_purpose
               -- , gm_pkg_op_loaner.get_loaner_set_list_change (vlist.token) setlistfl
               --   ,'DIVISION_ID' VALUE t504.c1910_division_id
              --    ,'COMPANYCD' VALUE t504.c1900_company_id
              --    ,'CMPDFMT' VALUE get_company_dtfmt(T504.c1900_company_id)
              --    ,'TRANS_COMPANY_ID' VALUE t504al.c1900_company_id
                  ,'surgerydate' VALUE gm_pkg_op_loaner.get_request_surgery_date(t504al. C526_PRODUCT_REQUEST_DETAIL_ID)
                  --,'shipdate' VALUE t907.lnshipdate
                  ,'requestedby' VALUE gm_pkg_op_loaner.get_loaner_requested_by(t504al. C526_PRODUCT_REQUEST_DETAIL_ID)
                  --,'tagid' VALUE GET_TAG_ID(t504.c504_consignment_id) ,'setidname' VALUE t504.c207_set_id||' '||get_set_name (t504.c207_set_id)
                  ,'logcomments' VALUE GET_LATEST_LOG_COMMENTS(t504.c504_consignment_id ,'1222') 
                  ,'rollbackfl' VALUE v_rollback_fl) RETURNING CLOB) INTO p_out_cur
             FROM    t504_consignment t504
                  , t504a_consignment_loaner t504a
                  , t504a_loaner_transaction t504al
                  /*, v_in_list vlist
                  , ( SELECT t504a.c504_consignment_id, t907.c907_ship_to_dt lnshipdate
                         FROM t504a_consignment_loaner t504a
                         , t504a_loaner_transaction t504al
                         , t526_product_request_detail t526
                         , t907_shipping_info t907
                        WHERE t504a.c504a_status_fl                 = 20 -- PENDING RETURN
                          AND t504al.c504a_return_dt               IS NULL
                          AND t504al.c504a_void_fl                 IS NULL
                          AND t504a.c504_consignment_id             = t504al.c504_consignment_id
                          AND t504al.c526_product_request_detail_id = t526.c526_product_request_detail_id
                          AND t526.c525_product_request_id          = t907.c525_product_request_id
                          AND t504a.c504_consignment_id             = t907.c907_ref_id
                          AND t504a.c504a_void_fl                  IS NULL
                          AND t907.c907_void_fl                    IS NULL
                          AND t526.C526_VOID_FL                    IS NULL) t907*/
              WHERE t504.c504_consignment_id = p_consign_id
                AND t504.c504_consignment_id = t504a.c504_consignment_id
                AND t504al.c504_consignment_id(+) = t504.c504_consignment_id
                --AND t504.c504_consignment_id = t907.c504_consignment_id (+)
                AND t504al.c504a_loaner_transaction_id(+) = gm_pkg_op_loaner.get_loaner_trans_id (t504.c504_consignment_id);
END IF;
--
END gm_is_fch__lnset_detail;

PROCEDURE gm_ls_fch__setcsgdetails(
	 p_consign_id IN t504_consignment.c504_consignment_id%TYPE,
	 p_type IN VARCHAR2,
     p_out_cur    OUT CLOB
)
AS
BEGIN
	SELECT JSON_ARRAYAGG(JSON_OBJECT(
'PNUM' VALUE PNUM,
--'DINUM' VALUE DINUM,
--'STOCK' VALUE STOCK,
--'INHOUSE_STOCK' VALUE INHOUSE_STOCK,
'PDESC' VALUE PDESC,
'SETTYPE' VALUE SETTYPE,
'QTY' VALUE QTY,
'PRICE' VALUE PRICE,
'MISSQTY' VALUE MISSQTY,
'QTYRECON' VALUE QTYRECON,
'COGSQTY' VALUE COGSQTY,
'SETLISTQTY' VALUE SETLISTQTY,
'BOQTY' VALUE BOQTY,
'SHELF' VALUE SHELF,
'BULK' VALUE BULK,
'BACKORDER' VALUE BACKORDER) ORDER BY PNUM RETURNING CLOB)INTO p_out_cur
FROM
  (SELECT t205.c205_part_number_id pnum,
    --gm_pkg_pd_rpt_udi.get_part_di_number(t205.c205_part_number_id) dinum,
    --DECODE (p_type, p_type, get_qty_in_stock (t205.c205_part_number_id), 0 ) stock,
    --DECODE (p_type, p_type, GET_QTY_IN_INHOUSEINV (t205.c205_part_number_id), 0 ) inhouse_stock,
    get_partdesc_by_company(t205.c205_part_number_id) pdesc,
    c205_product_class settype,
    NVL(cons.qty, 0)qty,
    to_number(NVL(GET_PART_PRICE(t205.c205_part_number_id,''),0)) price,
    --(NVL (miss.qty, 0) + NVL (missing_qty, 0)) missqty,
    NVL (setdetails.qty, 0) missqty,
    NVL (miss.qtyrecon, 0) qtyrecon,
    NVL(cons.qty, 0) + NVL (missing_qty, 0) - NVL (miss.qtyrecon, 0) cogsqty,
    NVL (setdetails.qty, 0) setlistqty,
    NVL(loanerboqty.qty ,0) boqty,
    get_qty_in_stock(t205.c205_part_number_id) shelf,
    GET_QTY_IN_BULK(t205.c205_part_number_id) bulk,
    NVL(loanerboqty.qty ,0)  backorder
  FROM t205_part_number t205,
    (     
      SELECT t5071.C205_PART_NUMBER_ID ,   
	    SUM(t5071.c5071_curr_qty) QTY  
	  FROM t5070_set_lot_master t5070,
	    t5071_set_part_qty t5071    
	  WHERE t5070.C5070_SET_LOT_MASTER_ID = t5071.C5070_SET_LOT_MASTER_ID  
	  AND t5070.C901_TXN_ID               = p_consign_id
	  AND t5070.c901_txn_type=4127
	  AND t5070.c901_location_type = 93502
	  AND t5070.c5070_void_fl IS NULL
	  AND t5071.c5071_void_fl IS NULL  
	  GROUP BY t5071.C205_PART_NUMBER_ID    
    /*SELECT t505.c205_part_number_id,
      SUM (t505.c505_item_qty) qty
    FROM t505_item_consignment t505
    WHERE t505.c504_consignment_id = p_consign_id
    AND t505.c505_void_fl         IS NULL
    GROUP BY t505.c205_part_number_id*/
    ) cons,
    (SELECT c205_part_number_id,
      SUM (qty) qty,
      SUM (qtyrecon) qtyrecon,
      SUM (missing_qty) missing_qty
    FROM
      (SELECT t413.c205_part_number_id,
        DECODE (c412_type, 50151, 0, DECODE (t413.c413_status_fl, 'Q', NVL (t413.c413_item_qty, 0 ), 0 ) ) qty,
        DECODE (c412_type, 50151, DECODE (t413.c413_status_fl, 'Q', NVL (t413.c413_item_qty, 0 ), 0 ), 0 ) missing_qty,
        DECODE (c412_type, 50151, NVL (t413.c413_item_qty, 0 ), DECODE (NVL (t413.c413_status_fl, 'Q' ), 'Q', 0, NVL (t413.c413_item_qty, 0) ) ) qtyrecon
      FROM t412_inhouse_transactions t412,
        t413_inhouse_trans_items t413
      WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
      AND t412.c412_ref_id             = p_consign_id
      AND t412.c412_void_fl           IS NULL
      AND t413.c413_void_fl           IS NULL
      )
    GROUP BY c205_part_number_id
    ) miss,
    (SELECT t413.c205_part_number_id,
      SUM(t413.c413_item_qty) qty
    FROM t412_inhouse_transactions t412,
      t413_inhouse_trans_items t413
    WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
    AND c412_status_fl               < 4
    AND t412.c412_type               = 100062
    AND t413.c413_void_fl           IS NULL
    AND c412_void_fl                IS NULL
    AND t412.c412_ref_id             = p_consign_id
    GROUP BY t413.c205_part_number_id
    )loanerboqty,
    (SELECT t208.c205_part_number_id,
      t208.c208_set_qty qty
    FROM t208_set_details t208,
      t504_consignment t504
    WHERE t208.c208_void_fl     IS NULL
    AND t208.c208_inset_fl       = 'Y'
    AND t504.c207_set_id         = t208.c207_set_id
    AND t504.c504_void_fl       IS NULL
    AND t504.c504_consignment_id = p_consign_id
    ) setdetails
  WHERE t205.c205_part_number_id = cons.c205_part_number_id
  AND t205.c205_part_number_id   = miss.c205_part_number_id(+)
  AND t205.c205_part_number_id   = setdetails.c205_part_number_id(+)
  AND t205.c205_part_number_id   = loanerboqty.c205_part_number_id(+)
  UNION ALL
  SELECT t208.c205_part_number_id pnum,
    --gm_pkg_pd_rpt_udi.get_part_di_number(t208.c205_part_number_id ) dinum,
    --DECODE (p_type, p_type, get_qty_in_stock (t208.c205_part_number_id), 0 ) stock,
    --DECODE (p_type, p_type, GET_QTY_IN_INHOUSEINV (t208.c205_part_number_id), 0 ) inhouse_stock,
    get_partdesc_by_company(t208.c205_part_number_id) pdesc,
    t205.c205_product_class settype,
    0 qty,
    0 price,
    t208.c208_set_qty missqty,
    0 qtyrecon,
    0 cogsqty,
    t208.c208_set_qty qty ,
    0 boqty ,
    0 shelf,
    0 bulk,
    loanerboqty.qty backorder
  FROM t208_set_details t208,
    t504_consignment t504,
    t205_part_number t205,
    (SELECT t413.c205_part_number_id,
      SUM(t413.c413_item_qty) qty
    FROM t412_inhouse_transactions t412,
      t413_inhouse_trans_items t413
    WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
    AND c412_status_fl               < 4
    AND t412.c412_type               = 100062
    AND t413.c413_void_fl           IS NULL
    AND c412_void_fl                IS NULL
    AND t412.c412_ref_id             = p_consign_id
    GROUP BY t413.c205_part_number_id
    )loanerboqty
  WHERE t208.c208_void_fl          IS NULL
  AND t208.c208_inset_fl            = 'Y'
  AND t504.c207_set_id              = t208.c207_set_id
  AND t504.c504_void_fl            IS NULL
  AND t504.c504_consignment_id      = p_consign_id
  AND t205.c205_part_number_id = t208.c205_part_number_id
  AND t205.c205_part_number_id   = loanerboqty.c205_part_number_id(+)
  AND t208.c205_part_number_id NOT IN
    (SELECT t5071.C205_PART_NUMBER_ID 
                  FROM t5070_set_lot_master t5070,
                    t5071_set_part_qty t5071    
                  WHERE t5070.C5070_SET_LOT_MASTER_ID = t5071.C5070_SET_LOT_MASTER_ID  
                  AND t5070.C901_TXN_ID               = p_consign_id
                  AND t5070.c901_txn_type=4127
                  AND t5070.c901_location_type = 93502
                  AND t5070.c5070_void_fl IS NULL
                  AND t5071.c5071_void_fl IS NULL  
                  GROUP BY t5071.C205_PART_NUMBER_ID  
    )
  ORDER BY 1
  )
WHERE qty     != 0
OR setlistqty != 0;
END gm_ls_fch__setcsgdetails;

PROCEDURE gm_fch_open_loaner_inhouse_trans(
	 p_consign_id IN t412_inhouse_transactions.c412_ref_id%TYPE,
	 p_scrn_from IN VARCHAR2,
     p_out_cur    OUT CLOB
)
AS
BEGIN
	
	SELECT JSON_ARRAYAGG(JSON_OBJECT(
 'ID' value ID,
 'MASTER_ID' VALUE MASTER_ID,
 'PNUM' VALUE PNUM,
 'PDESC' VALUE PDESC,
 'QTY' VALUE QTY,
 'CRDATE' VALUE CRDATE,
 'STATUS' VALUE STATUS,
 'STATUSFLAG' VALUE STATUSFLAG,
 'TYPE' VALUE TYPE,
 'CTYPE' VALUE CTYPE,
 'BORPTFL' VALUE BORPTFL) RETURNING CLOB)INTO p_out_cur from (select
  backorderinfo.*, DECODE(backorderinfo.statusflag,'0',DECODE(ROWNUM,1,'Y',NULL),NULL) BORPTFL FROM (
           SELECT t412.c412_inhouse_trans_id ID, c412_ref_id master_id,
                 t413.c205_part_number_id pnum,
                 t205.c205_part_num_desc pdesc,
                 t413.c413_item_qty qty, to_char(t412.c412_created_date,GET_RULE_VALUE('DATEFMT','DATEFORMAT'))  crdate,
                 gm_pkg_op_inhouse_master.get_inhouse_trans_status(t412.c412_status_fl) status,
                 t412.c412_status_fl statusflag,
                 --DECODE(t412.c412_status_fl,'0',DECODE(ROWNUM,1,'Y',NULL),NULL) BORPTFL,                                                
                 get_code_name (t412.c412_type) TYPE,
                 t412.c412_type CTYPE
            FROM t412_inhouse_transactions t412,
                 t413_inhouse_trans_items t413, t205_part_number t205
           WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
             AND t205.c205_part_number_id = t413.c205_part_number_id
             --If the user loading the CN from Process Trans screen, should show only open back order transactions
             AND c412_status_fl < DECODE(p_scrn_from,'PROCESS_TXN_SCN',1,4)
             AND t412.c412_type NOT IN (50159, 1006571)--loaner sales trans
             AND t413.c413_void_fl IS NULL
             AND c412_void_fl IS NULL
             AND t412.c412_ref_id = p_consign_id
            UNION
               SELECT t412.c412_inhouse_trans_id ID,
              c412_ref_id master_id,
              t413.c205_part_number_id pnum,
              t205.c205_part_num_desc pdesc,
              t413.c413_item_qty qty,
              TO_CHAR(t412.c412_created_date,GET_RULE_VALUE('DATEFMT','DATEFORMAT')) crdate,
              gm_pkg_op_inhouse_master.get_inhouse_trans_status(t412.c412_status_fl) status,
              t412.c412_status_fl statusflag,
              --DECODE(t412.c412_status_fl,'0',DECODE(ROWNUM,1,'Y',NULL),NULL) BORPTFL,
              get_code_name (t412.c412_type) TYPE,
              t412.c412_type CTYPE
            FROM t412_inhouse_transactions t412,
              t413_inhouse_trans_items t413,
              t205_part_number t205
            WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
            AND t205.c205_part_number_id     = t413.c205_part_number_id
            AND c412_status_fl      <> 4
            AND t412.c412_type IN (1006571, 1006572)--InHouse Set Missing Part && IH Loaner Set Back Order
            AND t413.c413_void_fl  IS NULL
            AND c412_void_fl       IS NULL
            AND t412.c412_ref_id    = p_consign_id)backorderinfo);
END gm_fch_open_loaner_inhouse_trans;

/******************************************************************
* Description : Procedure to save the received lots for loaner returns
* Author    : APrasath
****************************************************************/
PROCEDURE gm_sav_received_lots
  (
    p_consign_id        IN t504_consignment.c504_consignment_id%TYPE,
    p_part_no           IN t5073_set_lot_received_qty.c205_part_number_id%TYPE,
    p_control_no        IN t5073_set_lot_received_qty.c5073_control_number%TYPE,
    p_qty               IN t5073_set_lot_received_qty.c5073_curr_qty%TYPE,
    p_userid            IN t504_consignment.c504_created_by%TYPE,
    p_status_fl         IN t504a_loaner_transaction.c504a_rollback_fl%TYPE)
AS 
  v_company_id  t1900_company.c1900_company_id%TYPE;
  v_plant_id t5040_plant_master.c5040_plant_id%TYPE;
  v_received_id t5073_set_lot_received_qty.c5073_set_lot_received_qty_id%TYPE;
  v_lot_master_id t5073_set_lot_received_qty.c5070_set_lot_master_id%TYPE;
  v_loaner_transaction_id t5073_set_lot_received_qty.c504a_loaner_transaction_id%TYPE;
BEGIN
			
	SELECT get_compid_frm_cntx()
      INTO v_company_id
      FROM DUAL;
      
    SELECT get_plantid_frm_cntx() 
	  INTO v_plant_id 
	  FROM dual;      
 
    BEGIN
     SELECT c5070_set_lot_master_id
       INTO v_lot_master_id
		FROM t5070_set_lot_master
		WHERE c901_txn_id     = p_consign_id
		AND c901_txn_type     = 4127
		AND c5070_void_fl    IS NULL
		AND c901_location_type='93502'; --Product Loaner
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
      v_lot_master_id := NULL;
    END;
    
    BEGIN
      SELECT MAX(TO_NUMBER(C504A_LOANER_TRANSACTION_ID))
       INTO v_loaner_transaction_id
		FROM t504a_loaner_transaction
		WHERE c504_consignment_id= p_consign_id
		AND c1900_company_id     = v_company_id
		AND c504a_void_fl       IS NULL
		AND c504a_return_dt     IS NOT NULL;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
      v_loaner_transaction_id := NULL;
    END;
    
           
        
	IF p_status_fl = 'Y' THEN
		UPDATE t5073_set_lot_received_qty SET c5073_status_fl = 'Y'
		WHERE c504_consignment_id = p_consign_id
		AND c5073_status_fl IS NULL 
		AND c5073_void_fl IS NULL;
	END IF;
    
      SELECT  s5073_set_lot_received_qty.NEXTVAL INTO v_received_id FROM DUAL;
      INSERT
      INTO t5073_set_lot_received_qty
        (
          c5073_set_lot_received_qty_id,
          c5070_set_lot_master_id,
          c504_consignment_id,
          c205_part_number_id,
          c5073_control_number,
          c5073_curr_qty,
          c504a_loaner_transaction_id,
          c5073_created_by,
          c5073_created_date,          
          c1900_company_id,
          c5040_plant_id
        )
        VALUES
        (
          v_received_id,
          v_lot_master_id,
          p_consign_id,
          p_part_no,
          p_control_no,
          p_qty,
          v_loaner_transaction_id,
          p_userid,
          CURRENT_DATE,
          v_company_id,
          v_plant_id
        );
 
    
END gm_sav_received_lots;


/********************************************************************************************************
* Description : Procedure to identify and  save the missing lots from received lots for loaner returns
* Author    : APrasath
*********************************************************************************************************/
PROCEDURE gm_sav_add_or_missing_lots
  (
    p_consign_id        IN t504_consignment.c504_consignment_id%TYPE,   
    p_user_id            IN t504_consignment.c504_created_by%TYPE )
AS 
  v_company_id  t1900_company.c1900_company_id%TYPE;
  v_plant_id t5040_plant_master.c5040_plant_id%TYPE;  
  v_lot_master_id t5073_set_lot_received_qty.c5070_set_lot_master_id%TYPE;
  v_loaner_transaction_id t5073_set_lot_received_qty.c504a_loaner_transaction_id%TYPE;
  v_missing_transid t412_inhouse_transactions.c412_inhouse_trans_id%TYPE;
  v_set_part_qty_id  t5071_set_part_qty.c5071_set_part_qty_id%TYPE;
  CURSOR add_missing_lots
    IS
    -- IF QTY IS NEGATIVE THEN LOT QTY ARE MISSING ELSE ADDITIONAL LOT QTY FOUND
	SELECT (case when QTY<0 then NVL(v_missing_transid,TXNID) else TXNID end)TXNID,PNUM,CNUM,QTY, 
	(case when QTY<0 then '50159' else '4127' end) txntype,type,setlotmasterid FROM (
	SELECT t5070.C901_TXN_ID TXNID,t5072.C205_PART_NUMBER_ID PNUM,
	  t5072.c5072_control_number CNUM,
	  SUM(t5073.c5073_curr_qty) -  SUM(t5072.c5072_curr_qty) QTY,
	  t5071.c901_type type,t5071.C5070_SET_LOT_MASTER_ID setlotmasterid
	FROM t5070_set_lot_master t5070,
	  t5071_set_part_qty t5071,
	  t5072_set_part_lot_qty t5072,
    (SELECT C205_PART_NUMBER_ID ,
  SUM(c5073_curr_qty) c5073_curr_qty,
  c5073_control_number
	FROM t5073_set_lot_received_qty
	WHERE c504_consignment_id      = p_consign_id
	AND c1900_company_id           = v_company_id
	AND c504a_loaner_transaction_id= v_loaner_transaction_id
	AND c5070_set_lot_master_id    = v_lot_master_id
	AND c5073_status_fl           IS NULL
	AND c5073_void_fl is null
	GROUP BY C205_PART_NUMBER_ID,
	  c5073_control_number) t5073
		WHERE t5070.C5070_SET_LOT_MASTER_ID = t5071.C5070_SET_LOT_MASTER_ID
		AND t5071.C5071_SET_PART_QTY_ID     = t5072.C5071_SET_PART_QTY_ID
		AND t5070.C901_TXN_ID               = p_consign_id
		AND t5070.c901_txn_type=4127 --PRODUCT LOANER
		AND t5070.c901_location_type = 93502 --PRODUCT LOANER
		AND t5070.c5070_void_fl IS NULL
		AND t5071.c5071_void_fl IS NULL
		AND t5072.c5072_void_fl IS NULL
		AND t5072.c5072_curr_qty >0
		AND t5072.C205_PART_NUMBER_ID = t5073.C205_PART_NUMBER_ID
		AND t5072.c5072_control_number = t5073.c5073_control_number
		GROUP BY t5070.C901_TXN_ID,t5072.C205_PART_NUMBER_ID,t5072.c5072_control_number,t5071.c901_type,t5071.C5070_SET_LOT_MASTER_ID
		) WHERE QTY <> 0;
	
  CURSOR add_lots
    IS
  --ADDITIONAL PARTS LOTS IN RECEIVED QTY, NEED TO ADD IN LOT INVENTORY
	SELECT c504_consignment_id TXNID,C205_PART_NUMBER_ID PNUM,
	  c5073_control_number CNUM,
	  SUM(c5073_curr_qty) QTY,
	  '4127' txntype,'4127' type,
	  c5070_set_lot_master_id setlotmasterid
	FROM t5073_set_lot_received_qty
	WHERE c504_consignment_id      =p_consign_id
	AND c1900_company_id           =v_company_id
	AND c504a_loaner_transaction_id=v_loaner_transaction_id
	AND c5070_set_lot_master_id    =v_lot_master_id
	AND c5073_status_fl IS NULL
	AND C205_PART_NUMBER_ID
	  ||c5073_control_number NOT IN
	  (SELECT t5072.C205_PART_NUMBER_ID
	    || t5072.c5072_control_number
	  FROM t5070_set_lot_master t5070,
	    t5071_set_part_qty t5071,
	    t5072_set_part_lot_qty t5072
	  WHERE t5070.C5070_SET_LOT_MASTER_ID = t5071.C5070_SET_LOT_MASTER_ID
	  AND t5071.C5071_SET_PART_QTY_ID     = t5072.C5071_SET_PART_QTY_ID
	  AND t5070.C901_TXN_ID               = p_consign_id
	  AND t5070.c901_txn_type             =4127
	  AND t5070.c901_location_type        = 93502
	  AND t5070.c5070_void_fl            IS NULL
	  AND t5071.c5071_void_fl            IS NULL
	  AND t5072.c5072_void_fl            IS NULL
	  AND t5072.c5072_curr_qty            >0
	  GROUP BY t5072.C205_PART_NUMBER_ID,
	    t5072.c5072_control_number
	  )
	GROUP BY c504_consignment_id,C205_PART_NUMBER_ID,
	  c5073_control_number,c5070_set_lot_master_id ;
	  
  CURSOR missing_lots
    IS
  --ADDITIONAL LOTS WHICH NOT AVAILABLE IN RECEVICED LOTS BUT IT WAS IN PREVIOUS, SO NEED TO MARK AS MISSING  
	SELECT (case when t5072.c5072_control_number='NOC#' then t5070.C901_TXN_ID else 	
	(case when t5072.C205_PART_NUMBER_ID=t413.c205_part_number_id THEN NVL(v_missing_transid,t5070.C901_TXN_ID) ELSE t5070.C901_TXN_ID END)
	end) TXNID,t5072.C205_PART_NUMBER_ID PNUM,
	  t5072.c5072_control_number CNUM,
	  -SUM(t5072.c5072_curr_qty) QTY,
	  '50159' txntype, --loaner set to loaner sales
	  t5071.c901_type type,t5071.C5070_SET_LOT_MASTER_ID setlotmasterid
	FROM t5070_set_lot_master t5070,
	  t5071_set_part_qty t5071,
	  t5072_set_part_lot_qty t5072,
     ( SELECT t413.c205_part_number_id
		FROM t412_inhouse_transactions t412, t413_inhouse_trans_items t413
		WHERE t412.c412_inhouse_trans_id              = v_missing_transid
		AND c412_void_fl              IS NULL
        AND t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
        AND t413.c413_void_fl is null
		AND c412_status_fl             < 4
		AND c1900_company_id           = v_company_id
		AND c412_type                  = 50159 --loaner set to loaner sales
    group by t413.c205_part_number_id) t413
	WHERE t5070.C5070_SET_LOT_MASTER_ID = t5071.C5070_SET_LOT_MASTER_ID
	AND t5071.C5071_SET_PART_QTY_ID     = t5072.C5071_SET_PART_QTY_ID
	AND t5072.C205_PART_NUMBER_ID = t413.C205_PART_NUMBER_ID(+)
	AND t5070.C901_TXN_ID               = p_consign_id
	AND t5070.c901_txn_type=4127
	AND t5070.c901_location_type = 93502
	AND t5070.c5070_void_fl IS NULL
	AND t5071.c5071_void_fl IS NULL
	AND t5072.c5072_void_fl IS NULL
	AND t5072.c5072_curr_qty >0
	AND t5072.C205_PART_NUMBER_ID||t5072.c5072_control_number NOT IN (
	SELECT C205_PART_NUMBER_ID||c5073_control_number
	FROM t5073_set_lot_received_qty
	WHERE c504_consignment_id      =p_consign_id
	AND c1900_company_id           =v_company_id
	AND c504a_loaner_transaction_id=v_loaner_transaction_id
	AND c5070_set_lot_master_id    =v_lot_master_id
	AND c5073_status_fl IS NULL
	GROUP BY C205_PART_NUMBER_ID,
	  c5073_control_number 
	)
	GROUP BY t5070.C901_TXN_ID,t5072.C205_PART_NUMBER_ID,t5072.c5072_control_number,t5071.c901_type,t5071.C5070_SET_LOT_MASTER_ID,t413.c205_part_number_id;
BEGIN
			
	SELECT get_compid_frm_cntx()
      INTO v_company_id
      FROM DUAL;
      
    SELECT get_plantid_frm_cntx() 
	  INTO v_plant_id 
	  FROM dual;      
 
    BEGIN
     SELECT c5070_set_lot_master_id
       INTO v_lot_master_id
		FROM t5070_set_lot_master
		WHERE c901_txn_id     = p_consign_id
		AND c901_txn_type     = 4127
		AND c5070_void_fl    IS NULL
		AND c901_location_type='93502'; --Product Loaner
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
      v_lot_master_id := NULL;
    END;
    
    BEGIN
      SELECT MAX(TO_NUMBER(C504A_LOANER_TRANSACTION_ID))
       INTO v_loaner_transaction_id
		FROM t504a_loaner_transaction
		WHERE c504_consignment_id= p_consign_id
		AND c1900_company_id     = v_company_id
		AND c504a_void_fl       IS NULL
		AND c504a_return_dt     IS NOT NULL;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
      v_loaner_transaction_id := NULL;
    END;    
    
    BEGIN
     SELECT c412_inhouse_trans_id INTO v_missing_transid
		FROM t412_inhouse_transactions
		WHERE c412_ref_id              = p_consign_id
		AND c412_void_fl              IS NULL
		AND c412_status_fl             < 4
		AND c1900_company_id           = v_company_id
		AND c412_type                  = 50159 --loaner set to loaner sales
		AND c504a_loaner_transaction_id=v_loaner_transaction_id;
	EXCEPTION
    WHEN NO_DATA_FOUND THEN
      v_missing_transid := NULL;
    END;
      
       
    -- IF QTY IS NEGATIVE THEN LOT QTY ARE MISSING ELSE ADDITIONAL LOT QTY FOUND
    FOR loaner_lot_parts IN add_missing_lots    
    LOOP  
     gm_pkg_op_loaner_lot_process.gm_sav_loaner_lot_update(loaner_lot_parts.txnid,loaner_lot_parts.pnum,loaner_lot_parts.cnum,loaner_lot_parts.qty,loaner_lot_parts.setlotmasterid,loaner_lot_parts.txntype,loaner_lot_parts.type,p_user_id);
    END LOOP;
    
    --ADDITIONAL PARTS LOTS IN RECEIVED QTY, NEED TO ADD IN LOT INVENTORY
    FOR add_lot_parts IN add_lots    
    LOOP 
     gm_pkg_op_loaner_lot_process.gm_sav_loaner_lot_update(add_lot_parts.txnid,add_lot_parts.pnum,add_lot_parts.cnum,add_lot_parts.qty,add_lot_parts.setlotmasterid,add_lot_parts.txntype,add_lot_parts.type,p_user_id);
    END LOOP;
    
    --ADDITIONAL LOTS WHICH NOT AVAILABLE IN RECEVICED LOTS BUT IT WAS IN PREVIOUS, SO NEED TO MARK AS MISSING
    FOR missing_lot_parts IN missing_lots    
    LOOP   
     gm_pkg_op_loaner_lot_process.gm_sav_loaner_lot_update(missing_lot_parts.txnid,missing_lot_parts.pnum,missing_lot_parts.cnum,missing_lot_parts.qty,missing_lot_parts.setlotmasterid,missing_lot_parts.txntype,missing_lot_parts.type,p_user_id);
    END LOOP;    
    
 -- PMT-46800 - Lot Track - Rollback loaner changes
	--gm_pkg_op_loaner_lot_process.gm_update_missing_lots(v_lot_master_id,v_loaner_transaction_id,v_missing_transid,p_user_id); 
	UPDATE t413_inhouse_trans_items 
        SET c413_control_number = 'NOC#',
            c413_last_updated_by = p_user_id,
            c413_last_updated_date = SYSDATE 
      WHERE c412_inhouse_trans_id = v_missing_transid                                        
        AND c413_void_fl IS NULL 
		AND c413_control_number IS NULL;
    
END gm_sav_add_or_missing_lots;


/********************************************************************************************************
* Description : Procedure to save or add lot in qty T5071_SET_PART_QTY
* Author    : APrasath
*********************************************************************************************************/
PROCEDURE gm_sav_loaner_lot_update (
 	p_txnid IN T5071_SET_PART_QTY.c5071_last_updated_txn_id%TYPE,
 	p_pnum    IN T5071_SET_PART_QTY.c205_part_number_id%TYPE,
 	p_cnum    IN T5071_SET_PART_QTY.c5053_last_txn_control_number%TYPE,
 	p_qty    IN T5071_SET_PART_QTY.c5071_curr_qty%TYPE,
 	p_setlotmasterid    IN T5071_SET_PART_QTY.c5070_set_lot_master_id%TYPE,
 	p_txntype    IN T5071_SET_PART_QTY.c901_txn_type%TYPE,
 	p_type    IN T5071_SET_PART_QTY.c901_type%TYPE,
    p_user_id        IN t101_user.c101_user_id%TYPE
        )
AS
 v_set_part_qty_id T5071_SET_PART_QTY.C5071_SET_PART_QTY_ID%TYPE;
BEGIN
      
         UPDATE T5071_SET_PART_QTY
        SET c5071_curr_qty              = c5071_curr_qty + p_qty
        , c5071_last_updated_txn_id     =  p_txnid
        , c5053_last_txn_control_number = p_cnum
        , c5053_last_updated_by         = p_user_id
        , c5053_last_updated_date       = CURRENT_DATE
        , c901_txn_type				    = p_txntype --4127 or 50159 --Loaner set to loaner sales (missing)
        , c901_type                     = p_type    --4127    
        WHERE c205_part_number_id     = p_pnum
        AND c5070_set_lot_master_id = p_setlotmasterid
        AND c5071_void_fl IS NULL;
        
        IF (SQL%ROWCOUNT                = 0) THEN
        
             SELECT TO_CHAR (S5071_SET_PART_QTY_ID.NEXTVAL)
               INTO V_SET_PART_QTY_ID
               FROM DUAL;
               
             INSERT
               INTO T5071_SET_PART_QTY
                (
                    c5071_set_part_qty_id, c5070_set_lot_master_id, c205_part_number_id
                  , c5071_curr_qty, c5053_last_txn_control_number, c5071_last_updated_txn_id
                  , c5053_last_updated_by, c5053_last_updated_date, c901_txn_type, c901_type
                )
                VALUES
                (
                    v_set_part_qty_id, p_setlotmasterid, p_pnum
                  , p_qty, p_cnum, p_txnid
                  , p_user_id, CURRENT_DATE, p_txntype,p_type
                ) ;
        END IF;         
    
END gm_sav_loaner_lot_update;

/********************************************************************************************************
* Description : Procedure to update in t413_inhouse_trans_items and update the missing lot qty in location part mapping
* Author      : N RAJA  
*********************************************************************************************************/
PROCEDURE gm_update_missing_lots (
  p_lot_master_id          IN t5073_set_lot_received_qty.c5070_set_lot_master_id%TYPE,
  p_loaner_transaction_id  IN t5073_set_lot_received_qty.c504a_loaner_transaction_id%TYPE, 
  p_missing_transid        IN t412_inhouse_transactions.c412_inhouse_trans_id%TYPE,
  p_user_id                IN t412_inhouse_transactions.c412_created_by%TYPE
)
AS
    v_company_id  t1900_company.c1900_company_id%TYPE;
    v_count NUMBER;
	v_part_no t413_inhouse_trans_items.c205_part_number_id%TYPE;
    
     CURSOR v_trans_details  
         IS 
	 SELECT t5072a.c205_part_number_id part_num,
	       	t5072a.c5072_control_number control_num,
	       	ABS(t5072a.c5072a_txn_qty) qty,
	       	t413.c413_item_price price,
	       	t412.c412_inhouse_trans_id txnid,
	       	t413.c413_status_fl status,
	       	t413.c413_last_updated_by userid
	   FROM t5072a_set_part_lot_qty_log t5072a,
	       	t413_inhouse_trans_items t413,
	        t412_inhouse_transactions t412
	  WHERE t5072a.c5072a_txn_id      =  p_missing_transid                                            
	    AND t5072a.c205_part_number_id = t413.c205_part_number_id
	    AND t5072a.c5072a_txn_id = t413.c412_inhouse_trans_id
	    AND t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
	    AND t412.c504a_loaner_transaction_id = p_loaner_transaction_id
		AND t413.c205_part_number_id = v_part_no
		AND t5072a.c205_part_number_id = t413.c205_part_number_id
	  	AND t5072a.c901_txn_type          = 50159               --Loaner Set to Loaner Sales
	  	AND t412.c412_type = t5072a.c901_txn_type
	  	AND t5072a.c5070_set_lot_master_id = p_lot_master_id
	  	AND t413.c413_status_fl = 'Q'
	  	AND t413.c413_void_fl IS NULL
	  	AND t412.c412_void_fl IS NULL;
		
		
  CURSOR v_part_details  
         IS 	
  SELECT t413.c205_part_number_id part_num
		FROM t412_inhouse_transactions t412, t413_inhouse_trans_items t413
		WHERE t412.c412_inhouse_trans_id              = p_missing_transid
		AND c412_void_fl              IS NULL
        AND t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
        AND t413.c413_void_fl IS NULL
		AND c412_status_fl             < 4		
		AND c412_type                  = 50159 --loaner set to loaner sales
        group by t413.c205_part_number_id;
BEGIN

	 SELECT get_compid_frm_cntx()
	   INTO v_company_id
	   FROM DUAL;
	FOR v_part_detail IN v_part_details
	LOOP
		
	v_part_no := v_part_detail.part_num;
		
	SELECT  SUM(ABS(t5072a.c5072a_txn_qty))- SUM(t413.c413_item_qty) INTO v_count
	  FROM t5072a_set_part_lot_qty_log t5072a,
	       t413_inhouse_trans_items t413,
	       t412_inhouse_transactions t412
	 WHERE t5072a.c5072a_txn_id       =   p_missing_transid                     
	   AND t5072a.c205_part_number_id = t413.c205_part_number_id
	   and t413.c205_part_number_id = v_part_no
	   AND t5072a.c5072a_txn_id = t413.c412_inhouse_trans_id
	   AND t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
	   AND t5072a.c901_txn_type       = 50159    --Loaner Set to Loaner Sales
	   AND t412.c412_type = t5072a.c901_txn_type
	   AND t412.c504a_loaner_transaction_id = p_loaner_transaction_id
	   AND t5072a.c5070_set_lot_master_id = p_lot_master_id                    
	   AND t413.c413_status_fl = 'Q'
	   AND t413.c413_void_fl IS NULL
	   AND t412.c412_void_fl IS NULL;

 IF v_count = 0 then

     UPDATE t413_inhouse_trans_items 
        SET c413_void_fl = 'Y',
            c413_last_updated_by = 30301,
            c413_last_updated_date = SYSDATE 
      WHERE c412_inhouse_trans_id = p_missing_transid   
        AND c205_part_number_id = v_part_no	  
        AND c413_void_fl IS NULL;
			  
		FOR v_trans_detail IN v_trans_details
		LOOP	  
		gm_pkg_op_request_master.gm_sav_inhouse_txn_dtl(v_trans_detail.txnid,v_trans_detail.part_num,v_trans_detail.qty,v_trans_detail.control_num,v_trans_detail.price,v_trans_detail.status,v_trans_detail.userid);
	    END LOOP;	    
	 
  ELSE
    UPDATE t413_inhouse_trans_items 
        SET c413_control_number = 'NOC#',
            c413_last_updated_by = 30301,
            c413_last_updated_date = SYSDATE 
      WHERE c412_inhouse_trans_id = p_missing_transid                                        
        AND c413_void_fl IS NULL 
		AND c205_part_number_id = v_part_no	
		AND c413_control_number IS NULL;
  END IF;  
  
  END LOOP;	
  -- update the missing lot qty in location part mapping - PMT-46800   
     gm_pkg_op_inv_field_sales_tran.gm_sav_loaner_lot(p_missing_transid,p_user_id);
		
		
END gm_update_missing_lots;

/********************************************************************************************************
* Description : Procedure to fetch received lots
* Author      : Sindhu 
*********************************************************************************************************/
PROCEDURE gm_fch_received_cn_lots(
  p_consign_id        IN t504_consignment.c504_consignment_id%TYPE,
  p_out_cur    OUT CLOB
)
AS
  v_company_id  t1900_company.c1900_company_id%TYPE;
  v_loaner_transaction_id t5073_set_lot_received_qty.c504a_loaner_transaction_id%TYPE;
  v_datefmt VARCHAR2(10);
  v_rollback_fl t504a_loaner_transaction.c504a_rollback_fl%TYPE;
BEGIN

		SELECT get_compid_frm_cntx()
	   	INTO v_company_id
	   	FROM DUAL;
		
		SELECT NVL(get_compdtfmt_frm_cntx(),get_rule_value('DATEFMT','DATEFORMAT'))
	   	INTO v_datefmt
	   	FROM DUAL;
		
	
	   	
	 BEGIN  
        SELECT MAX(TO_NUMBER(C504A_LOANER_TRANSACTION_ID))
        INTO v_loaner_transaction_id
		FROM t504a_loaner_transaction
		WHERE c504_consignment_id= p_consign_id
		AND c1900_company_id     = v_company_id
		AND c504a_void_fl       IS NULL
		AND c504a_return_dt     IS NOT NULL;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
      v_loaner_transaction_id := NULL;
	END;  

	BEGIN			
			SELECT t504a.c504a_rollback_fl
			  INTO v_rollback_fl
		      FROM t504a_loaner_transaction t504a
		     WHERE t504a.c504a_void_fl IS NULL
		       AND c504a_loaner_transaction_id IN (SELECT MAX (TO_NUMBER (c504a_loaner_transaction_id))
		                                             FROM t504a_loaner_transaction t504a
		                                            WHERE t504a.c504_consignment_id = p_consign_id
		                                              AND t504a.c504a_void_fl IS NULL);
	EXCEPTION WHEN NO_DATA_FOUND THEN
	 v_rollback_fl := NULL;
	END;
	
	IF v_rollback_fl = 'Y' THEN
	
	SELECT JSON_ARRAYAGG(JSON_OBJECT(
	'PNUM' VALUE PNUM,
	'CNUM' VALUE CNUM,
	'PDESC' VALUE PDESC,
	'QTY' VALUE QTY,
	'EXPIRYDT' VALUE EXPIRYDT,
	'PARTTYPE' VALUE PARTTYPE) ORDER BY PNUM,CNUM RETURNING CLOB) INTO p_out_cur
	FROM
	(SELECT t5073.C205_PART_NUMBER_ID PNUM,
	  t5073.c5073_control_number CNUM,
	  t205.c205_part_num_desc PDESC,
	  SUM(t5073.c5073_curr_qty) QTY,
	  TO_CHAR (t2550.c2550_expiry_date,v_datefmt) EXPIRYDT,
	  t205.c205_product_class PARTTYPE
	FROM t5070_set_lot_master t5070,  
	  t5073_set_lot_received_qty t5073, T205_PART_NUMBER T205, t2550_part_control_number t2550
	WHERE 
	--t5070.C5070_SET_LOT_MASTER_ID = t5073.C5070_SET_LOT_MASTER_ID
	 t5070.C901_TXN_ID               = p_consign_id
	AND t5073.c504a_loaner_transaction_id = v_loaner_transaction_id
	AND t5070.C901_TXN_ID               = t5073.c504_consignment_id
	AND t5073.c5073_control_number=t2550.c2550_control_number
	AND t5070.c901_txn_type=4127 --PRODUCT LOANER
	AND t5070.c901_location_type = 93502 --PRODUCT LOANER
	AND t205.c205_part_number_id= t5073.c205_part_number_id
	AND t205.c205_part_number_id= t2550.c205_part_number_id
	AND t5073.c5073_status_fl IS NULL
	AND t5070.c5070_void_fl IS NULL
	AND t5073.c5073_void_fl IS NULL
	AND t5073.c5073_curr_qty >0
	--AS this is a log table, we will have 0 qty,which is not required in the paperwork.
	GROUP BY t5073.C205_PART_NUMBER_ID,t5073.c5073_control_number,t205.c205_part_num_desc,t205.c205_product_class,t2550.c2550_expiry_date);
END IF;
END gm_fch_received_cn_lots;
/********************************************************************************************************
* Description : Procedure to update the Roll back flag to t504a_loaner_transaction Table
* Author      : Tamizhthangam Ramasamy
*********************************************************************************************************/ 
 PROCEDURE  gm_update_rollback_flag (
    p_consign_id  IN  t504a_loaner_transaction.c504_consignment_id%TYPE,
    p_user_id     IN  t504a_loaner_transaction.c504a_created_by%TYPE
)
AS
BEGIN
	
	UPDATE t504a_loaner_transaction 
       SET  c504a_rollback_fl = 'Y',
            c504a_last_updated_by = p_user_id,
            c504a_last_updated_date = sysdate
      WHERE c504a_loaner_transaction_id 
              IN (SELECT MAX (TO_NUMBER (c504a_loaner_transaction_id))
		            FROM t504a_loaner_transaction t504a
		           WHERE t504a.c504_consignment_id = p_consign_id
		             AND t504a.c504a_void_fl IS NULL);

END gm_update_rollback_flag;

/**********************************************************************************
* Description : This procedure is used update the lot track flag for the passing set
* Author      : Vinoth
************************************************************************************/ 
 PROCEDURE gm_sav_loaner_lot_track (
	   p_consign_id  IN t504a_loaner_transaction.c504_consignment_id%TYPE
	  ,p_userId		 IN t412_inhouse_transactions.c412_last_updated_by%TYPE
	)
	AS
/*
 This is used create lot details for all the sterile parts used loaner sets - PMT-46800
*/
  v_trans_id VARCHAR2 (50);
  v_loc_part_id t5053_location_part_mapping.c5053_location_part_map_id%TYPE;
  v_set_lot_master_id t5070_set_lot_master.c5070_set_lot_master_id%TYPE;
  v_set_part_qty_id t5071_set_part_qty.c5071_set_part_qty_id%TYPE;
  v_cnt     NUMBER;
  v_lcn_cnt NUMBER;
  v_lcn_id t5052_location_master.c5052_location_id%TYPE;
  v_setlot_id NUMBER;
  v_slot_cnt  NUMBER;
  v_company_id	t1900_company.c1900_company_id%TYPE;
  v_plant_id	t5040_plant_master.c5040_plant_id%TYPE;
  CURSOR consignment_details
  IS
    SELECT t504.c504_consignment_id
    FROM t504a_consignment_loaner t504a ,
      t504_consignment t504 ,
      t207_set_master t207
    WHERE t504a.c504_consignment_id = t504.c504_consignment_id
    AND c504_type                   = '4127'
    AND t504.c1900_company_id       = v_company_id
    AND c504a_status_fl            !=60
    AND t504.c504_void_fl          IS NULL
    AND t504a.c504a_void_fl        IS NULL
    AND t207.c207_set_id            = t504.c207_set_id 
    AND t504.c504_consignment_id = p_consign_id
    AND c207_lot_track_fl           ='Y';
  CURSOR part_details
  IS
    SELECT v_trans_id consignment_id,
      t205.c205_part_number_id pnum,
      'NOC#' cnum,
      NVL(cons.qty, 0) - NVL (miss.qty, 0) qty,
      (SELECT c5070_set_lot_master_id
      FROM t5070_set_lot_master
      WHERE c901_txn_id     = v_trans_id
      AND c901_txn_type     = 4127
      AND c5070_void_fl    IS NULL
      AND c901_location_type='93502'
      ) setlotmasterid,
    4127 txntype,
    4127 type,
    706328 userid
  FROM t205_part_number t205,
    (SELECT t505.c205_part_number_id,
      SUM (t505.c505_item_qty) qty
    FROM t505_item_consignment t505
    WHERE t505.c504_consignment_id = v_trans_id
    AND t505.c505_void_fl         IS NULL
    GROUP BY t505.c205_part_number_id
    ) cons,
    (SELECT c205_part_number_id,
      SUM (qty) qty,
      SUM (qtyrecon) qtyrecon,
      SUM (missing_qty) missing_qty
    FROM
      (SELECT t413.c205_part_number_id,
        DECODE (c412_type, 50151, 0, DECODE (t413.c413_status_fl, 'Q', NVL (t413.c413_item_qty, 0 ), 0 ) ) qty,
        DECODE (c412_type, 50151, DECODE (t413.c413_status_fl, 'Q', NVL (t413.c413_item_qty, 0 ), 0 ), 0 ) missing_qty,
        DECODE (c412_type, 50151, NVL (t413.c413_item_qty, 0 ), DECODE (NVL (t413.c413_status_fl, 'Q' ), 'Q', 0, NVL (t413.c413_item_qty, 0) ) ) qtyrecon
      FROM t412_inhouse_transactions t412,
        t413_inhouse_trans_items t413
      WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
      AND t412.c412_ref_id             = v_trans_id
      AND t412.c412_void_fl           IS NULL
      AND t413.c413_void_fl           IS NULL
      )
    GROUP BY c205_part_number_id
    ) miss,
    (SELECT t413.c205_part_number_id,
      SUM(t413.c413_item_qty) qty
    FROM t412_inhouse_transactions t412,
      t413_inhouse_trans_items t413
    WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
    AND c412_status_fl               < 4
    AND t412.c412_type               = 100062
    AND t413.c413_void_fl           IS NULL
    AND c412_void_fl                IS NULL
    AND t412.c412_ref_id             = v_trans_id
    GROUP BY t413.c205_part_number_id
    )loanerboqty,
    (SELECT t504.c504_consignment_id,
      t208.c205_part_number_id,
      t208.c208_set_qty qty
    FROM t208_set_details t208,
      t504_consignment t504
    WHERE t208.c208_void_fl     IS NULL
    AND t208.c208_inset_fl       = 'Y'
    AND t504.c207_set_id         = t208.c207_set_id
    AND t504.c504_void_fl       IS NULL
    AND t504.c504_consignment_id = v_trans_id
    ) setdetails
  WHERE t205.c205_part_number_id = cons.c205_part_number_id
  AND t205.c205_part_number_id   = miss.c205_part_number_id(+)
  AND t205.c205_part_number_id   = setdetails.c205_part_number_id(+)
  AND t205.c205_part_number_id   = loanerboqty.c205_part_number_id(+) ;
BEGIN
	SELECT get_compid_frm_cntx(), get_plantid_frm_cntx() 
      INTO v_company_id, v_plant_id
      FROM DUAL;
      
  FOR cn_details IN consignment_details
  LOOP
      v_trans_id := cn_details.c504_consignment_id;
      UPDATE t5070_SET_LOT_master
        SET c5070_void_fl = 'Y'
        WHERE c901_txn_id = v_trans_id;	
      BEGIN
        SELECT COUNT(1),
          c5052_location_id
        INTO v_cnt,
          v_lcn_id
        FROM t5052_location_master
        WHERE c5052_ref_id         = v_trans_id
        AND c5051_inv_warehouse_id = 6
        AND c5052_void_fl         IS NULL
        GROUP BY c5052_location_id;
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
        v_cnt := 0;
      END;
      IF v_cnt > 0 THEN
        -- t5052 void
        UPDATE t5052_location_master
        SET c5052_void_fl       = 'Y'
        WHERE c5052_location_id = v_lcn_id;        
      END IF;
      BEGIN
        SELECT COUNT(1),
          C5070_SET_LOT_MASTER_ID
        INTO v_slot_cnt,
          v_setlot_id
        FROM t5070_SET_LOT_master
        WHERE c901_txn_id  = v_trans_id
        AND C901_TXN_TYPE  = 4127
        AND C5070_VOID_FL IS NULL
        GROUP BY C5070_SET_LOT_MASTER_ID;
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
        v_slot_cnt := 0;
      END;
      IF v_slot_cnt > 0 THEN
        -- t5070 void      
        
        UPDATE t5071_set_part_qty
        SET c5071_void_fl = 'Y'
        WHERE C5070_SET_LOT_MASTER_ID = v_setlot_id;
        
        UPDATE t5072_set_part_lot_qty
        SET c5072_void_fl = 'Y'
        WHERE C5070_SET_LOT_MASTER_ID = v_setlot_id; 
      END IF;
	  
      -- t5052 insert
      SELECT globus_app.s5053_location_part_mapping.NEXTVAL
      INTO v_loc_part_id
      FROM DUAL;
      INSERT
      INTO t5052_location_master
        (
          c5052_location_id,
          c5052_ref_id,
          c901_status ,
          c5052_created_by,
          c5052_created_date,
          c901_zone,
          c5052_shelf,
          c901_location_type ,
          c5051_inv_warehouse_id,
          c5052_location_cd,
          c1900_company_id,
          c5040_plant_id,
          c5057_building_id -- added for PMT-33507
        )
        VALUES
        (
          TO_CHAR(v_loc_part_id),
          v_trans_id,
          93310 ,
          p_userId,
          CURRENT_DATE,
          NULL,
          NULL,
          93502 ,
          6,
          NULL,
          v_company_id,
          v_plant_id,
          NULL
        );
      -- -- t5070 insert
      SELECT TO_CHAR (globus_app.S5070_SET_LOT_MASTER.NEXTVAL)
      INTO v_set_lot_master_id
      FROM DUAL;
      INSERT
      INTO t5070_SET_LOT_master
        (
          C5070_SET_LOT_MASTER_ID,
          C901_TXN_ID,
          C901_TXN_TYPE ,
          C5052_LOCATION_ID,
          C901_LOCATION_TYPE,
          C5070_VOID_FL ,
          C5070_LAST_UPDATED_BY,
          C5070_LAST_UPDATED_DATE
        )
        VALUES
        (
          v_set_lot_master_id,
          v_trans_id,
          4127 ,
          TO_CHAR(v_loc_part_id),
          93502,
          NULL ,
          p_userId,
          CURRENT_DATE
        ) ;
      SELECT COUNT(t5071.c205_part_number_id)
      INTO v_lcn_cnt
      FROM t5070_set_lot_master t5070,
        t5071_set_part_qty t5071,
        t5072_set_part_lot_qty t5072
      WHERE t5070.C5070_SET_LOT_MASTER_ID = t5071.C5070_SET_LOT_MASTER_ID
      AND t5071.C5071_SET_PART_QTY_ID     = t5072.C5071_SET_PART_QTY_ID
      AND t5070.C901_TXN_ID               = v_trans_id
      AND t5070.c901_txn_type             =4127
      AND t5070.c901_location_type        = 93502
      AND t5070.c5070_void_fl            IS NULL
      AND t5071.c5071_void_fl            IS NULL
      AND t5072.c5072_void_fl            IS NULL
      AND t5072.c5072_curr_qty            >0 ;
      FOR tran_details                   IN part_details
      LOOP
        BEGIN
            --IF tran_details.qty > 0 THEN
              -- t5053 insert
              INSERT
              INTO t5053_location_part_mapping
                (
                  c5053_location_part_map_id,
                  c5052_location_id ,
                  c205_part_number_id ,
                  c5053_curr_qty,
                  c901_type ,
                  c5053_last_update_trans_id,
                  c901_last_transaction_type ,
                  c5053_last_txn_control_number,
                  c5053_created_by,
                  c5053_created_date
                )
                VALUES
                (
                  globus_app.s5053_location_part_mapping.NEXTVAL,
                  TO_CHAR(v_loc_part_id),
                  tran_details.pnum ,
                  tran_details.qty,
                  4127,
                  v_trans_id,
                  4127 ,
                  tran_details.cnum,
                  p_userId ,
                  SYSDATE
                );
              IF v_lcn_cnt = 0 THEN
                SELECT TO_CHAR (globus_app.S5071_SET_PART_QTY_ID.NEXTVAL)
                INTO V_SET_PART_QTY_ID
                FROM DUAL;
                INSERT
                INTO T5071_SET_PART_QTY
                  (
                    C5071_SET_PART_QTY_ID,
                    C5070_SET_LOT_MASTER_ID,
                    C205_PART_NUMBER_ID ,
                    C5071_CURR_QTY,
                    C5053_LAST_TXN_CONTROL_NUMBER,
                    C5071_LAST_UPDATED_TXN_ID ,
                    C5053_LAST_UPDATED_BY,
                    C5053_LAST_UPDATED_DATE,
                    C901_TXN_TYPE,
                    C901_TYPE
                  )
                  VALUES
                  (
                    v_set_part_qty_id,
                    v_set_lot_master_id,
                    tran_details.pnum ,
                    tran_details.qty,
                    tran_details.cnum,
                    v_trans_id ,
                    p_userId,
                    CURRENT_DATE,
                    4127,4127
                  ) ;
                -- gm_pkg_op_loaner_lot_process.gm_sav_loaner_lot_update(tran_details.consignment_id, tran_details.pnum, tran_details.cnum, tran_details.qty, v_set_lot_master_id,tran_details.txntype, tran_details.type,tran_details.userid );
              END IF;
        END;
      END LOOP;
  END LOOP;
END gm_sav_loaner_lot_track;

END gm_pkg_op_loaner_lot_process;
/