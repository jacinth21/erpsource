--@"C:\DATABASE\packages\operations\gm_pkg_op_lot_track_data.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_lot_track_data
IS
/********************************************************************************
   * Description : Procedure to used to update job start date on t9300_job table
   * Author      : Agilan Singaravel
  *******************************************************************************/
	PROCEDURE gm_update_job_process (
		p_job_id		IN		t9300_job.c9300_job_id%type,
		p_time          IN      VARCHAR2
	)
	AS
	BEGIN
		
		IF p_time = 'START' THEN  
		
		  UPDATE T9300_JOB
  			 SET c9300_start_date = CURRENT_DATE,
    			 c9300_last_updated_by  = '30301',
    			 c9300_last_updated_date= CURRENT_DATE
  		   WHERE c9300_job_id       = p_job_id
  			 AND c9300_void_fl       IS NULL
  			 AND c9300_inactive_fl   IS NULL;
  			 
  	ELSE 
  	  UPDATE T9300_JOB
  			 SET c9300_end_date = CURRENT_DATE,
    			 c9300_last_updated_by  = '30301',
    			 c9300_last_updated_date= CURRENT_DATE
  		   WHERE c9300_job_id       = p_job_id
  			 AND c9300_void_fl       IS NULL
  			 AND c9300_inactive_fl   IS NULL;
  			 
  	END IF;

	END gm_update_job_process;

/********************************************************************************
   * Description : Procedure to used to fetch transaction id from t214 table
   * Author      : Agilan Singaravel
  *******************************************************************************/
	PROCEDURE gm_process_transaction_data 
	AS
	v_error_msg    		CLOB;
	to_mail 	   		t906_rules.c906_rule_value%TYPE;
	subject 	   		VARCHAR2 (100);
	mail_body	   		CLOB;
	v_date_fmt 			VARCHAR2 (20) ;
	v_error_fl      	VARCHAR2 (1) ;
	
	CURSOR fch_trans_data
	IS
		SELECT t214.c214_id transid, c922_value transval,t214.c901_type ttype,
  					    c922_plus_warehouse plusWH, c922_minus_warehouse minusWH,
  				        c922_addl_plus_warehouse addplusWH,c922_addl_minus_warehouse addminusWH,trunc(t214.c214_created_dt) cdate
		  FROM t214_transactions t214, V205_STERILE_PARTS v205,t922_inventory_transaction_map t922
		 WHERE t214.c205_part_number_id = v205.c205_part_number_id
		   AND t214.c901_type = t922.c901_type
		   AND c922_category = 'T214'
		   AND C214_job_run_fl IS NULL
           AND c922_void_fl IS NULL
      GROUP BY t214.c214_id,c922_value,t214.c901_type,c922_plus_warehouse,c922_minus_warehouse,
      		   c922_addl_plus_warehouse,c922_addl_minus_warehouse,trunc(t214.c214_created_dt)
      ORDER BY trunc(t214.c214_created_dt);
	
	BEGIN	
		
	 SELECT get_compdtfmt_frm_cntx ()
       INTO v_date_fmt
       FROM dual;
       
		FOR v_trans_data IN fch_trans_data
		LOOP
			BEGIN
      			v_error_fl := 'N';
				gm_fch_transaction_dtl(v_trans_data.transid,v_trans_data.plusWH,v_trans_data.minusWH,v_trans_data.addplusWH,
								   v_trans_data.addminusWH,v_trans_data.transval,v_trans_data.ttype,v_trans_data.cdate);
			EXCEPTION WHEN OTHERS THEN
				 v_error_msg := v_error_msg || v_trans_data.transid || ' - Error Message:' || SQLERRM || '\n';
         		 v_error_fl := 'Y';
			END;
			IF v_error_fl = 'N' THEN
				gm_upd_transaction_job_flag(v_trans_data.transid,v_trans_data.ttype);
			END IF;
		END LOOP;
		
		IF v_error_msg IS NOT NULL THEN
			to_mail := get_rule_value('LOT_TRACK_JOB','ERR_MAIL');
			subject := 'Lot Track ETL Job Error';
			mail_body := v_error_msg;
			
			gm_com_send_email_prc (to_mail, subject, mail_body);
	    END IF;
				
	END gm_process_transaction_data;
	
 /********************************************************************************
   * Description : Procedure to used to fetch t922 value and insert into t5080
   * Author      : Agilan Singaravel
  *******************************************************************************/
	PROCEDURE gm_fch_transaction_dtl(
		p_trans_id		IN		t214_transactions.c214_id%type,
		p_plusWH		IN		t922_inventory_transaction_map.c922_plus_warehouse%type,
		p_minusWH		IN		t922_inventory_transaction_map.c922_minus_warehouse%type,
		p_addl_plusWH	IN		t922_inventory_transaction_map.c922_addl_plus_warehouse%type,
		p_addl_minusWH	IN		t922_inventory_transaction_map.c922_addl_minus_warehouse%type,
		p_trans_val		IN		t922_inventory_transaction_map.c922_value%type,
		p_trans_type	IN		t214_transactions.c901_type%type,
		p_trans_date	IN		t214_transactions.c214_created_dt%type
	)
	AS
	v_rule_value   VARCHAR2 (2000);
	v_exec_string  VARCHAR2 (2000);
	BEGIN

		SELECT get_rule_value(p_trans_val,'INV_TRANS_MAP')
		  INTO v_rule_value
		  FROM DUAL;
	
		  IF (v_rule_value IS NOT NULL AND p_trans_val IS NOT NULL) THEN 
				v_exec_string := 'BEGIN ' || v_rule_value || '(''' || p_trans_id || ''','''|| p_plusWH || ''','''|| p_minusWH|| ''','''|| p_addl_plusWH ||''','''||p_addl_minusWH||''','''||p_trans_type||''','''||p_trans_date||'''); END;'; 

		  END IF;
		  --dbms_output.put_line('v_exec_string==> ' || v_exec_string);
		  EXECUTE IMMEDIATE (v_exec_string);
		
	END gm_fch_transaction_dtl;
	
  /*************************************************************************************
   * Description : Procedure to used to save t412 table values to t5080 and t5080_log
   * Author      : Agilan Singaravel
  ************************************************************************************/
	PROCEDURE gm_fch_412_transaction_dtl(
		p_trans_id		IN		t214_transactions.c214_id%type,
		p_plusWH		IN		t922_inventory_transaction_map.c922_plus_warehouse%type,
		p_minusWH		IN		t922_inventory_transaction_map.c922_minus_warehouse%type,
		p_addl_plusWH	IN		t922_inventory_transaction_map.c922_addl_plus_warehouse%type,
		p_addl_minusWH	IN		t922_inventory_transaction_map.c922_addl_minus_warehouse%type,
		p_trans_type	IN		t214_transactions.c901_type%type,
		p_trans_date	IN		t214_transactions.c214_created_dt%type
	)	
	AS
	CURSOR fch_partnum_dtl
	IS
		SELECT t413.c205_part_number_id partnum,
  			   t413.c413_control_number cntnum,
  			   nvl(locationdtl.txnqty,t413.c413_item_qty) txnqty, 
  			   locationdtl.locid,
  			   locationdtl.loctype,
  			   locationdtl.loccd,
  			   t412.c1900_company_id cmpid,
               t412.c5040_plant_id plantid,
               c922_plus_warehouse plusWH, 
               c922_minus_warehouse minusWH,
               t412.c412_type
		  FROM T412_INHOUSE_TRANSACTIONS t412,
  			   T413_INHOUSE_TRANS_ITEMS t413 ,
  			   V205_STERILE_PARTS v205 , 
  			   (SELECT c5055_ref_id refid,t5055.c5055_control_number cnum,
  			   		   t5055.C205_part_number_id pnum,t5055.c5052_location_id locid,
  			   		   t5052.c901_location_type loctype,t5052.c5052_location_cd loccd,t5055.c5055_qty txnqty
                  FROM T5052_LOCATION_MASTER t5052,T5055_CONTROL_NUMBER t5055 
               	 WHERE t5055.c5052_location_id = t5052.c5052_location_id
                   AND t5055.c5052_location_id IS NOT NULL 
                   AND t5052.c901_status = 93310 --'Active'
                   AND c5055_ref_id = p_trans_id
                   AND t5052.c5052_void_fl IS NULL
		   		   AND t5055.c5055_void_fl IS NULL
              GROUP BY c5055_ref_id,c5055_control_number,C205_part_number_id,t5055.c5052_location_id,
              		   t5052.c901_location_type,c5052_location_cd,t5055.c5055_qty)locationdtl,t922_inventory_transaction_map t922
		 WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
		   AND t413.c205_part_number_id = v205.c205_part_number_id
           AND t412.c412_inhouse_trans_id = locationdtl.refid(+)
           AND t413.C205_part_number_id = locationdtl.pnum(+)
           AND t413.c413_control_number = DECODE(t412.c412_type,100071,t413.c413_control_number, locationdtl.cnum(+)) -- 100071 MWFG
		   AND t412.c412_inhouse_trans_id = p_trans_id
		   AND t412.c412_type = t922.c901_type(+)
		   AND t413.c413_control_number IS NOT NULL
		   AND t412.c412_void_fl IS NULL
		   AND t413.c413_void_fl IS NULL; 
	BEGIN
		
		FOR v_partnum_dtl IN fch_partnum_dtl
		LOOP
		gm_pkg_op_lot_track_data.gm_upd_lot_inventory_details(v_partnum_dtl.partnum,v_partnum_dtl.cntnum,v_partnum_dtl.txnqty,
								  nvl(v_partnum_dtl.plusWH,p_plusWH),nvl(v_partnum_dtl.minusWH,p_minusWH),p_addl_plusWH,p_addl_minusWH,p_trans_id,p_trans_type,
								  v_partnum_dtl.locid,v_partnum_dtl.loctype,v_partnum_dtl.loccd,
								  v_partnum_dtl.cmpid,v_partnum_dtl.plantid,p_trans_date);
		END LOOP;
		   
	END gm_fch_412_transaction_dtl;

 /*******************************************************************************************************
   * Description : Procedure to used to fetch part details from t504,t505 and to save in t5080 and t5081
   * Author      : Agilan Singaravel
  ******************************************************************************************************/
	PROCEDURE gm_fch_504_transaction_dtl(
		p_trans_id		IN		t214_transactions.c214_id%type,
		p_plusWH		IN		t922_inventory_transaction_map.c922_plus_warehouse%type,
		p_minusWH		IN		t922_inventory_transaction_map.c922_minus_warehouse%type,
		p_addl_plusWH	IN		t922_inventory_transaction_map.c922_addl_plus_warehouse%type,
		p_addl_minusWH	IN		t922_inventory_transaction_map.c922_addl_minus_warehouse%type,
		p_trans_type	IN		t214_transactions.c901_type%type,
		p_trans_date	IN		t214_transactions.c214_created_dt%type
	)	
	AS
	v_loc_from		VARCHAR2(400);
	v_loc_cd		VARCHAR2(100);
	v_loc_id		VARCHAR2(100);
	CURSOR fch_partnum_dtl
	IS				
		SELECT t505.c205_part_number_id partnum,
  			   t505.c505_control_number cntnum,
  			   nvl(locationdtl.txnqty,t505.c505_item_qty) txnqty, 			   
			   --locationdtl.locid,
			   locationdtl.loctype,
			   --locationdtl.loccd,
			   NVL (gm_pkg_op_item_control_rpt.get_initiated_location (t505.c205_part_number_id, p_trans_id, DECODE (t504.c504_status_fl, '2', 'pick', '3', 'put', 'pick')), gm_pkg_op_inv_scan.get_part_location(t505.c205_part_number_id, t504.c504_type)) locid  , 
               gm_pkg_op_item_control_rpt.get_location_cd (t505.c205_part_number_id,p_trans_id, DECODE (t504.c504_status_fl, '2', 'pick', '3', 'put', 'pick'), t504.c504_type,'',t505.c505_control_number) loccd,
  			   t504.c1900_company_id cmpid,
               t504.c5040_plant_id plantid,
               c922_plus_warehouse plusWH, c922_minus_warehouse minusWH,
  			   c922_addl_plus_warehouse addplusWH,c922_addl_minus_warehouse addminusWH
		  FROM T504_CONSIGNMENT t504,
  			   T505_ITEM_CONSIGNMENT t505,
  			   V205_STERILE_PARTS v205 ,  
               (SELECT c5055_ref_id refid,t5055.c5055_control_number cnum,
               		   t5055.C205_part_number_id pnum,t5055.c5052_location_id locid,
               		   t5052.c901_location_type loctype,t5052.c5052_location_cd loccd,t5055.c5055_qty txnqty            
               	  FROM T5052_LOCATION_MASTER t5052,T5055_CONTROL_NUMBER t5055
               	 WHERE t5055.c5052_location_id = t5052.c5052_location_id
               	   AND c5055_ref_id = p_trans_id 
               	   AND t5055.c5052_location_id is not null 
               	   AND t5052.c5052_void_fl IS NULL
		   		   AND t5055.c5055_void_fl IS NULL
              GROUP BY c5055_ref_id,C5055_CONTROL_NUMBER,C205_part_number_id,t5055.c5052_location_id,t5052.c901_location_type,c5052_location_cd,t5055.c5055_qty
              )locationdtl,t922_inventory_transaction_map t922
		 WHERE t504.c504_consignment_id = t505.c504_consignment_id
		   AND t505.c205_part_number_id = v205.c205_part_number_id
		   AND t505.c505_control_number = locationdtl.cnum(+)
       AND t505.c205_part_number_id = locationdtl.pnum(+)
           AND t504.c504_consignment_id = locationdtl.refid(+)
		   AND t504.c504_consignment_id = p_trans_id
       	   AND DECODE(t504.c504_type,4110,(DECODE(t504.c207_set_id,NULL,4110,3301)),t504.c504_type) = t922.c901_type(+)
		   AND t922.c922_category(+) = 'T214'
		   AND t505.c505_control_number IS NOT NULL
      	   AND t922.c922_void_fl IS NULL
		   AND t504.c504_void_fl IS NULL
		   AND t505.c505_void_fl IS NULL;
		   
	CURSOR fch_cons_partnum_dtl
	IS
	  		SELECT distinct t505.c205_part_number_id partnum,
  			   t505.c505_control_number cntnum,
  			   nvl(t5055.c5055_qty,t505.c505_item_qty) txnqty,  			   
  			   --t5055.c5052_location_id locid,
  			   NVL (gm_pkg_op_item_control_rpt.get_initiated_location (t505.c205_part_number_id, p_trans_id, DECODE (t504.c504_status_fl, '2', 'pick', '3', 'put', 'pick')), gm_pkg_op_inv_scan.get_part_location(t505.c205_part_number_id, t504.c504_type)) locid  , 
               t5052.c901_location_type loctype,
  			   nvl(t504.c701_distributor_id,t504.c504_ship_to_id) distid,
  			   Decode(p_trans_type,50181,null,t922.c922_location_from) locfrom,
  			   --locationdtl.loccd loccd,
  			   gm_pkg_op_item_control_rpt.get_location_cd (t505.c205_part_number_id,p_trans_id, DECODE (t504.c504_status_fl, '2', 'pick', '3', 'put', 'pick'), t504.c504_type,'',t505.c505_control_number) loccd,
  			   nvl(t701.c701_distributor_name, get_dist_rep_name(c504_ship_to_id)) fsloccd,
  			   t504.c1900_company_id cmpid,
               t504.c5040_plant_id plantid,
               Decode(p_trans_type,4311,c922_plus_warehouse,p_plusWH) plusWH, 
               Decode(p_trans_type,4311,c922_minus_warehouse,p_minusWH) minusWH,
               --c922_plus_warehouse plusWH, c922_minus_warehouse minusWH,
               c922_addl_plus_warehouse addplusWH,c922_addl_minus_warehouse addminusWH
		  FROM T504_CONSIGNMENT t504,
  			   T505_ITEM_CONSIGNMENT t505,
  			   V205_STERILE_PARTS v205 , 
  			   T5052_LOCATION_MASTER t5052,
  			   T5055_CONTROL_NUMBER t5055,
  			   T701_DISTRIBUTOR t701,
               T703_SALES_REP t703,
               t922_inventory_transaction_map t922,
               (SELECT c5055_ref_id refid,t5055.c5055_control_number cnum,
               		   t5055.C205_part_number_id pnum,t5055.c5052_location_id locid,
               		   t5052.c901_location_type loctype,t5052.c5052_location_cd loccd
               	  FROM T5052_LOCATION_MASTER t5052,T5055_CONTROL_NUMBER t5055
               	 WHERE t5055.c5052_location_id = t5052.c5052_location_id
               	   AND c5055_ref_id = p_trans_id
               	   AND t5055.c5052_location_id is not null
               	   AND t5052.c5052_void_fl IS NULL
		   		   AND t5055.c5055_void_fl IS NULL
              GROUP BY c5055_ref_id,C5055_CONTROL_NUMBER,C205_part_number_id,t5055.c5052_location_id,t5052.c901_location_type,c5052_location_cd
              )locationdtl
		 WHERE t504.c504_consignment_id = t505.c504_consignment_id
		   AND t505.c205_part_number_id = v205.c205_part_number_id
		   AND t505.c505_control_number = t5055.c5055_control_number(+)
		   AND t504.c504_consignment_id = t5052.c5052_ref_id(+)
           AND t504.c504_consignment_id = t5055.c5055_ref_id(+)
           AND t504.c701_distributor_id = t701.c701_distributor_id(+)
           AND t504.c504_ship_to_id = t703.c703_sales_rep_id(+)
           AND t505.c505_control_number = locationdtl.cnum(+)
           AND t504.c504_consignment_id = locationdtl.refid(+)
            AND t505.c205_part_number_id = locationdtl.pnum(+)
		   AND t504.c504_consignment_id = p_trans_id
       	   --AND DECODE(t504.c504_type,4110,(DECODE(t504.c207_set_id,NULL,4110,3301)),t504.c504_type) = t922.c901_type(+)
		   AND DECODE(t504.c504_type,4110,(DECODE(t504.c207_set_id,NULL,4110,3301)),4127,(DECODE(t504.c207_set_id,NULL,t504.c504_type,120468)),t504.c504_type) = t922.c901_type(+) 
		   AND DECODE(t504.c504_type,4110,NVL(t505.c901_type,'-999'),4127,NVL(t505.c901_type,'-999'),NVL(t505.c901_type,'-999')) = DECODE(t504.c504_type,4110,'-999',4127,'-999',NVL(t505.c901_type,'-999')) 		   
		   AND t922.c922_category(+) = 'T214'
		   AND t505.c505_control_number IS NOT NULL
      	   AND t922.c922_void_fl IS NULL
		   --AND c5055_update_inv_fl = 'Y'
		   AND t504.c504_void_fl IS NULL
		   AND t505.c505_void_fl IS NULL
		   AND t5052.c5052_void_fl IS NULL
		   AND t5055.c5055_void_fl IS NULL
		   AND c701_void_fl IS NULL
		   AND c703_void_fl IS NULL;
	
	BEGIN
		IF (p_trans_type = '4311' OR p_trans_type = '50181') THEN --Consignment
			
			FOR v_partnum_dtl IN fch_cons_partnum_dtl
			LOOP
			v_loc_from := v_partnum_dtl.locfrom;
			
			IF v_loc_from = 'LOCATION' THEN
				v_loc_cd := v_partnum_dtl.loccd;
				v_loc_id := v_partnum_dtl.locid;
			ELSIF v_loc_from = 'FS' THEN
				v_loc_cd := v_partnum_dtl.fsloccd;
				v_loc_id := v_partnum_dtl.distid;
			ELSE
				v_loc_cd := v_partnum_dtl.fsloccd;
				v_loc_id := v_partnum_dtl.distid;
			END IF;
			
				gm_pkg_op_lot_track_data.gm_upd_lot_inventory_details(v_partnum_dtl.partnum,v_partnum_dtl.cntnum,v_partnum_dtl.txnqty,
								  NVL(v_partnum_dtl.plusWH,p_plusWH),NVL(v_partnum_dtl.minusWH,p_minusWH),NVL(v_partnum_dtl.addplusWH,p_addl_plusWH),
								  NVL(v_partnum_dtl.addminusWH,p_addl_minusWH),p_trans_id,p_trans_type,
								  v_loc_id,v_partnum_dtl.loctype,v_loc_cd,
								  v_partnum_dtl.cmpid,v_partnum_dtl.plantid,p_trans_date);
			END LOOP;
		 ELSE
		 	FOR v_partnum_dtl IN fch_partnum_dtl
			LOOP
				gm_pkg_op_lot_track_data.gm_upd_lot_inventory_details(v_partnum_dtl.partnum,v_partnum_dtl.cntnum,v_partnum_dtl.txnqty,
                  				  NVL(v_partnum_dtl.plusWH,p_plusWH),NVL(v_partnum_dtl.minusWH,p_minusWH),NVL(v_partnum_dtl.addplusWH,p_addl_plusWH),
                  				  NVL(v_partnum_dtl.addminusWH,p_addl_minusWH),p_trans_id,p_trans_type,
								  v_partnum_dtl.locid,v_partnum_dtl.loctype,v_partnum_dtl.loccd,
								  v_partnum_dtl.cmpid,v_partnum_dtl.plantid,p_trans_date);
			END LOOP;
		 END IF;
	END gm_fch_504_transaction_dtl;
	
 /*******************************************************************************************************
   * Description : Procedure to used to fetch part details from t506,t507 and to save in t5080 and t5081
   * Author      : Agilan Singaravel
  ******************************************************************************************************/
	PROCEDURE gm_fch_506_transaction_dtl(
		p_trans_id		IN		t214_transactions.c214_id%type,
		p_plusWH		IN		t922_inventory_transaction_map.c922_plus_warehouse%type,
		p_minusWH		IN		t922_inventory_transaction_map.c922_minus_warehouse%type,
		p_addl_plusWH	IN		t922_inventory_transaction_map.c922_addl_plus_warehouse%type,
		p_addl_minusWH	IN		t922_inventory_transaction_map.c922_addl_minus_warehouse%type,
		p_trans_type	IN		t214_transactions.c901_type%type,
		p_trans_date	IN		t214_transactions.c214_created_dt%type
	)	
	AS
	CURSOR fch_partnum_dtl
	IS
		SELECT t507.c205_part_number_id partnum,
  			   t507.c507_control_number cntnum,
  			   nvl(t5055.c5055_qty,t507.c507_item_qty) txnqty, 			   
  			   --t5055.c5052_location_id locid,
  			   t5052.c901_location_type loctype,
  			   --t5052.c5052_location_cd loccd,
  			   NVL (gm_pkg_op_item_control_rpt.get_initiated_location (t507.c205_part_number_id, p_trans_id, DECODE (t506.C506_STATUS_FL, '2', 'pick', '3', 'put', 'pick')), gm_pkg_op_inv_scan.get_part_location(t507.c205_part_number_id, t506.c506_type)) locid  , 
               gm_pkg_op_item_control_rpt.get_location_cd (t507.c205_part_number_id,p_trans_id, DECODE (t506.C506_STATUS_FL, '2', 'pick', '3', 'put', 'pick'), t506.c506_type,'',t507.c507_control_number) loccd,
  			   t506.c1900_company_id cmpid,
               t506.c5040_plant_id plantid
		  FROM T506_RETURNS t506,
  			   T507_RETURNS_ITEM t507,
  			   V205_STERILE_PARTS v205 , 
  			   T5052_LOCATION_MASTER t5052,
  			   T5055_CONTROL_NUMBER t5055
		 WHERE t506.c506_rma_id = t507.c506_rma_id
		   AND t507.c205_part_number_id = v205.c205_part_number_id
		   AND t507.c507_control_number = t5055.c5055_control_number(+)
		   AND t506.c506_rma_id = t5052.c5052_ref_id(+)
           AND t506.c506_rma_id = t5055.c5055_ref_id(+)
		   AND t506.c506_rma_id = p_trans_id
		  -- AND c5055_update_inv_fl = 'Y'
		   AND c506_void_fl IS NULL
		   AND t507.c507_control_number IS NOT NULL
		  -- AND c507_hold_fl IS NULL
		   AND c5052_void_fl IS NULL
		   AND c5055_void_fl IS NULL;
		   
     CURSOR fch_partnum_FS_dtl
	 IS
		SELECT t507.c205_part_number_id partnum,
  			   t507.c507_control_number cntnum,
  			   nvl(t5055.c5055_qty,t507.c507_item_qty) txnqty, 			   
  			   nvl(t506.c701_distributor_id,t506.c703_sales_rep_id) locid,
               t5052.c901_location_type loctype,
               nvl(t701.c701_distributor_name, get_dist_rep_name(t506.c703_sales_rep_id)) loccd,
  			   t506.c1900_company_id cmpid,
               t506.c5040_plant_id plantid
		  FROM T506_RETURNS t506,
  			   T507_RETURNS_ITEM t507,
  			   V205_STERILE_PARTS v205 , 
  			   T5052_LOCATION_MASTER t5052,
  			   T5055_CONTROL_NUMBER t5055,
  			   T701_DISTRIBUTOR t701,
               T703_SALES_REP t703
		 WHERE t506.c506_rma_id = t507.c506_rma_id
		   AND t507.c205_part_number_id = v205.c205_part_number_id
		   AND t507.c507_control_number = t5055.c5055_control_number(+)
		   AND t506.c506_rma_id = t5052.c5052_ref_id(+)
           AND t506.c506_rma_id = t5055.c5055_ref_id(+)
           AND t506.c701_distributor_id = t701.c701_distributor_id(+)
           AND t506.c703_sales_rep_id = t703.c703_sales_rep_id(+)
		   AND t506.c506_rma_id = p_trans_id
		  -- AND c5055_update_inv_fl = 'Y'
		   AND c506_void_fl IS NULL
		   AND t507.c507_control_number IS NOT NULL
		  -- AND c507_hold_fl IS NULL
		   AND c5052_void_fl IS NULL
		   AND c5055_void_fl IS NULL
		   AND c701_void_fl IS NULL
		   AND c703_void_fl IS NULL;
	BEGIN
		
		FOR v_partnum_dtl IN fch_partnum_dtl
		LOOP
				gm_pkg_op_lot_track_data.gm_upd_lot_inventory_details(v_partnum_dtl.partnum,v_partnum_dtl.cntnum,v_partnum_dtl.txnqty,
								  p_plusWH,null,p_addl_plusWH,p_addl_minusWH,p_trans_id,p_trans_type,
								  v_partnum_dtl.locid,v_partnum_dtl.loctype,v_partnum_dtl.loccd,
								  v_partnum_dtl.cmpid,v_partnum_dtl.plantid,p_trans_date);
		END LOOP;
		FOR v_partnum_dtl IN fch_partnum_FS_dtl
		LOOP
				gm_pkg_op_lot_track_data.gm_upd_lot_inventory_details(v_partnum_dtl.partnum,v_partnum_dtl.cntnum,v_partnum_dtl.txnqty,
								  null,p_minusWH,p_addl_plusWH,p_addl_minusWH,p_trans_id,p_trans_type,
								  v_partnum_dtl.locid,v_partnum_dtl.loctype,v_partnum_dtl.loccd,
								  v_partnum_dtl.cmpid,v_partnum_dtl.plantid,p_trans_date);
		END LOOP;
		   
	END gm_fch_506_transaction_dtl;
	
 /*******************************************************************************************************
   * Description : Procedure to used to fetch part details from t501,t502 and to save in t5080 and t5081
   * Author      : Agilan Singaravel
  ******************************************************************************************************/
	PROCEDURE gm_fch_501_transaction_dtl(
		p_trans_id		IN		t214_transactions.c214_id%type,
		p_plusWH		IN		t922_inventory_transaction_map.c922_plus_warehouse%type,
		p_minusWH		IN		t922_inventory_transaction_map.c922_minus_warehouse%type,
		p_addl_plusWH	IN		t922_inventory_transaction_map.c922_addl_plus_warehouse%type,
		p_addl_minusWH	IN		t922_inventory_transaction_map.c922_addl_minus_warehouse%type,
		p_trans_type	IN		t214_transactions.c901_type%type,
		p_trans_date	IN		t214_transactions.c214_created_dt%type
	)	
	AS
	CURSOR fch_partnum_order_dtl
	IS
		SELECT t502.c205_part_number_id partnum,
               t502.c502_control_number cntnum,
               nvl(locationdtl.c5055_qty,t502.c502_item_qty) txnqty,                                                  
               locationdtl.locid locid,
               locationdtl.loctype loctype,
               locationdtl.loccd loccd,
               t501.c1900_company_id cmpid,
               t501.c5040_plant_id plantid
          FROM T501_ORDER t501,
               (SELECT c501_order_id, c205_part_number_id,c502_control_number,SUM(c502_item_qty) c502_item_qty FROM t502_item_order
                 WHERE c501_order_id = p_trans_id  AND c502_void_fl IS NULL   
              GROUP BY c501_order_id,c205_part_number_id,c502_control_number) t502,
                       V205_STERILE_PARTS v205 ,                                      
               (SELECT c5055_ref_id refid,t5055.c5055_control_number cnum,sum(t5055.c5055_qty) c5055_qty,
                       t5055.C205_part_number_id pnum,t5055.c5052_location_id locid,
                       t5052.c901_location_type loctype,t5052.c5052_location_cd loccd             
                  FROM T5052_LOCATION_MASTER t5052,T5055_CONTROL_NUMBER t5055
                 WHERE t5055.c5052_location_id = t5052.c5052_location_id
                   AND c5055_ref_id = p_trans_id 
                   AND t5055.c5052_location_id is not null 
                   AND t5052.c5052_void_fl IS NULL
                   AND t5055.c5055_void_fl IS NULL
              GROUP BY c5055_ref_id,C5055_CONTROL_NUMBER,C205_part_number_id,t5055.c5052_location_id,t5052.c901_location_type,
                       c5052_location_cd )locationdtl
                 WHERE t501.c501_order_id = t502.c501_order_id
                   AND t502.c205_part_number_id = v205.c205_part_number_id
               --  AND t502.c502_control_number = t5055.c5055_control_number(+)                    
                   AND t502.c502_control_number = locationdtl.cnum(+)
                   AND t502.C205_part_number_id = locationdtl.pnum(+)
                   AND t501.c501_order_id = locationdtl.refid(+)
                   AND t501.c501_order_id = p_trans_id
                   AND t502.c502_control_number IS NOT NULL
                   AND c501_void_fl IS NULL;
		   
	  CURSOR fch_partnum_dtl
	  IS
		SELECT t502.c205_part_number_id partnum,
               t502.c502_control_number cntnum,
               nvl(t5055.c5055_qty,t502.c502_item_qty) txnqty,                                            
               nvl(t501.c501_distributor_id,t501.c501_ship_to_id) locid,
               t5052.c901_location_type loctype,
               nvl(t701.c701_distributor_name, get_dist_rep_name(c501_ship_to_id)) loccd,
               t501.c1900_company_id cmpid,
               t501.c5040_plant_id plantid
          FROM T501_ORDER t501,
               (SELECT c501_order_id, c205_part_number_id, c502_control_number,SUM(c502_item_qty) c502_item_qty 
                  FROM t502_item_order
                 WHERE c501_order_id = p_trans_id 
                   AND c502_void_fl  IS NULL   
              GROUP BY c501_order_id,c205_part_number_id,c502_control_number) t502,
                       V205_STERILE_PARTS v205 , 
                       T5052_LOCATION_MASTER t5052,
                       T5055_CONTROL_NUMBER t5055,
                       T701_DISTRIBUTOR t701,
                       T703_SALES_REP t703
                 WHERE t501.c501_order_id = t502.c501_order_id
                   AND t502.c205_part_number_id = v205.c205_part_number_id
                   AND t502.c502_control_number = t5055.c5055_control_number(+)
                   AND t501.c501_order_id = t5052.c5052_ref_id(+)
                   AND t501.c501_order_id = t5055.c5055_ref_id(+)
                   AND t501.c501_distributor_id = t701.c701_distributor_id(+)
                   AND t501.c501_ship_to_id = t703.c703_sales_rep_id(+)
                   AND t501.c501_order_id = p_trans_id
                 --AND c5055_update_inv_fl = 'Y'
                   AND t502.c502_control_number IS NOT NULL
                   AND c501_void_fl IS NULL
                   AND c5052_void_fl IS NULL
                   AND c5055_void_fl IS NULL
                   AND c701_void_fl IS NULL
                   AND c703_void_fl IS NULL;

	BEGIN

		FOR v_partnum_dtl IN fch_partnum_dtl
		LOOP
			gm_pkg_op_lot_track_data.gm_upd_lot_inventory_details(v_partnum_dtl.partnum,v_partnum_dtl.cntnum,v_partnum_dtl.txnqty,
								  p_plusWH,null,p_addl_plusWH,p_addl_minusWH,p_trans_id,p_trans_type,
								  v_partnum_dtl.locid,v_partnum_dtl.loctype,v_partnum_dtl.loccd,
								  v_partnum_dtl.cmpid,v_partnum_dtl.plantid,p_trans_date);
		END LOOP;
		
		FOR v_partnum_dtl IN fch_partnum_order_dtl
		LOOP
			gm_pkg_op_lot_track_data.gm_upd_lot_inventory_details(v_partnum_dtl.partnum,v_partnum_dtl.cntnum,v_partnum_dtl.txnqty,
								  null,p_minusWH,p_addl_plusWH,p_addl_minusWH,p_trans_id,p_trans_type,
								  v_partnum_dtl.locid,v_partnum_dtl.loctype,v_partnum_dtl.loccd,
								  v_partnum_dtl.cmpid,v_partnum_dtl.plantid,p_trans_date);
	    END LOOP;
		   
	END gm_fch_501_transaction_dtl;
	
 /*****************************************************************************************
   * Description : Procedure to used to update or insert partnum details on t5080 and t5081
   * Author      : Agilan Singaravel
  *****************************************************************************************/
	PROCEDURE gm_upd_lot_inventory(
		p_part_num		IN		t5080_lot_track_inv.c205_part_number_id%type,
		p_control_num	IN		t5080_lot_track_inv.c2550_control_number%type,
		p_txn_qty		IN		t5080_lot_track_inv.c5080_qty%type,
		p_whType		IN		t5080_lot_track_inv.c901_warehouse_type%type,
		p_transid		IN		t5080_lot_track_inv.c5080_last_update_trans_id%type,
		p_trans_type	IN   	t214_transactions.c901_type%type,
		p_location_id	IN		t5080_lot_track_inv.c5080_location_id%type,
		p_location_type	IN		t5080_lot_track_inv.c901_location_type%type,
		p_location_name	IN		t5080_lot_track_inv.c5080_location_name%type,
		p_company_id	IN		t5080_lot_track_inv.c1900_company_id%type,
		p_plant_id		IN		t5080_lot_track_inv.c5040_plant_id%type,
		p_trans_date	IN		t214_transactions.c214_created_dt%type
	)
	AS
	v_error_msg		CLOB;
	BEGIN

		UPDATE T5080_LOT_TRACK_INV
	   	   SET c2550_control_number = p_control_num,
	   	   	   c205_part_number_id = p_part_num,
	   	   	   c5080_last_update_trans_id = p_transid,
	   	   	   c5080_qty =  c5080_qty + p_txn_qty,
	   	   	   c901_warehouse_type = p_whType,
	   	   	   c5080_location_id = p_location_id,
	   	   	   c901_location_type = p_location_type,
	   	   	   c5080_location_name = p_location_name,
	   	   	   c1900_company_id = p_company_id,
	   	   	   c5040_plant_id = p_plant_id,
	   	   	   c5080_last_updated_by = '30301',
	   	   	   c5080_last_updated_date = current_date,
               c901_last_transaction_type = p_trans_type,
               c5080_created_date = p_trans_date
	 	 WHERE c2550_control_number = p_control_num
	 	   AND c205_part_number_id = p_part_num
	 	   AND c901_warehouse_type = p_whType
	 	   AND NVL(c5080_location_id,'-999') = NVL(p_location_id,'-999')
           AND c5080_void_fl is null;

	   
	 IF (SQL%ROWCOUNT = 0) THEN
	 
			INSERT INTO T5080_LOT_TRACK_INV(c5080_control_number_inv_id,c2550_control_number,c205_part_number_id,c5080_qty,c901_warehouse_type,
											c5080_last_update_trans_id,c901_last_transaction_type,c5080_location_id,c901_location_type,c1900_company_id,
											c5040_plant_id,c5080_created_by,c5080_created_date,c5080_location_name) 
	     		 VALUES (S5080_LOT_TRACK_INV.nextval,p_control_num,p_part_num,p_txn_qty,p_whType,p_transid,p_trans_type,p_location_id,p_location_type,p_company_id
	     		 		 ,p_plant_id,'30301',p_trans_date,p_location_name);
	 END IF;
	 		
	END gm_upd_lot_inventory;
	
 /*****************************************************************************************
   * Description : Procedure to used to update or insert partnum details on t5080 and t5081
   * Author      : Agilan Singaravel
  *****************************************************************************************/
	PROCEDURE gm_sav_lot_track_inv_log(
		p_part_num		IN		t5081_lot_track_inv_log.c205_part_number_id%type,
		p_control_num	IN		t5081_lot_track_inv_log.c2550_control_number%type,
		p_whType		IN		t5081_lot_track_inv_log.c901_warehouse_type%type,
		p_org_qty		IN		t5081_lot_track_inv_log.c5081_orig_qty%type,
		p_txn_qty		IN		t5081_lot_track_inv_log.c5081_txn_qty%type,
		p_new_qty		IN		t5081_lot_track_inv_log.c5081_new_qty%type,
		p_transid		IN		t5081_lot_track_inv_log.c5081_last_update_trans_id%type,
		p_cntl_inv_id	IN		t5081_lot_track_inv_log.c5080_control_number_inv_id%type,
		p_trans_type	IN		t5081_lot_track_inv_log.c901_last_transaction_type%type,
		p_location_id	IN		t5081_lot_track_inv_log.c5080_location_id%type,
		p_location_type	IN		t5081_lot_track_inv_log.c901_location_type%type,
		p_location_name	IN		t5081_lot_track_inv_log.c5080_location_name%type,
		p_company_id	IN		t5081_lot_track_inv_log.c1900_company_id%type,
		p_plant_id		IN		t5081_lot_track_inv_log.c5040_plant_id%type,
		p_set_id		IN		t5081_lot_track_inv_log.c207_set_id%type,
		p_set_nm		IN		t5081_lot_track_inv_log.c207_set_name%type,
		p_tag_id		IN		t5081_lot_track_inv_log.c5010_tag_id%type,
		p_trans_date	IN		t214_transactions.c214_created_dt%type
	)
	AS
	BEGIN
		
		INSERT INTO T5081_LOT_TRACK_INV_LOG (c5081_inv_log_id,c5080_control_number_inv_id,c205_part_number_id,c2550_control_number,
								c901_warehouse_type,c5081_txn_qty,c5081_orig_qty,c5081_new_qty,c5081_last_update_trans_id,
								c901_last_transaction_type,c5080_location_id,c901_location_type,c1900_company_id,c5040_plant_id,
								c5081_created_date,c207_set_id,c207_set_name,c5010_tag_id,c5080_location_name)
		     VALUES (S5081_LOT_TRACK_INV_LOG.nextval,p_cntl_inv_id,p_part_num,p_control_num,p_whType,p_txn_qty,p_org_qty,p_new_qty,
		     		 p_transid,p_trans_type,p_location_id,p_location_type,p_company_id,p_plant_id,p_trans_date,p_set_id,p_set_nm,p_tag_id,p_location_name);
		
	END gm_sav_lot_track_inv_log;
	
 /*********************************************************************************************
   * Description : Procedure to used to update job fl in t214 table after completion of insert
   * Author      : Agilan Singaravel
  *********************************************************************************************/
	PROCEDURE gm_upd_transaction_job_flag(
		p_trans_id		IN		t214_transactions.c214_id%type,
		p_trans_type	IN		t214_transactions.c901_type%type
	)
	AS
	BEGIN

		UPDATE T214_TRANSACTIONS
		   SET c214_job_run_fl = 'Y'
		 WHERE c214_id = p_trans_id
		   AND c901_type = p_trans_type; 

	END gm_upd_transaction_job_flag;
/********************************************************************************
   * Description : Procedure to used to fetch transaction id from t502b table
   * Author      : Prabhu Vigneshwaran M D
  *******************************************************************************/
	
	PROCEDURE gm_process_order_usage 
	AS
	v_error_msg    		CLOB;
	to_mail 	   		t906_rules.c906_rule_value%TYPE;
	subject 	   		VARCHAR2 (100);
	mail_body	        CLOB;
	--This cursor which is used to fetch the order id from t502b and t501
    CURSOR fch_order_usage_data
	IS
		  SELECT
    			distinct t502b.c501_order_id transid,
    		    'T501' transval,
    			'4310' ttype,
   			    NULL pluswh,
    		    '4000339' minuswh,
   			    NULL addpluswh,
   			    NULL addminuswh,
   			    t502b.c502b_created_date cdate
		FROM
   			 t502b_item_order_usage   t502b,
    		 t501_order               t501,
    		 v205_sterile_parts       v205
	   WHERE t502b.c501_order_id = t501.c501_order_id
         AND t502b.c205_part_number_id = v205.c205_part_number_id
         AND t502b.c502b_void_fl IS NULL
		 AND t502b.c502b_usage_lot_num IS NOT NULL
         AND t501.c501_void_fl IS NULL
         AND t502b.c502b_job_run_fl IS NULL
         ORDER BY t502b.c502b_created_date;

	BEGIN	
		--dbms_output.put_line ('**********************  gm_pkg_op_lot_track_data.gm_process_order_usage start time >> '|| to_char(sysdate,'MM/dd/yyyy HH:MI:SS AM'));
     
		--loop and store the value in t5080 and t5081 based on fch_trans_data cursor
		FOR v_trans_data IN fch_order_usage_data
		LOOP
			BEGIN
				
			gm_fch_502b_transaction_dtl(v_trans_data.transid,v_trans_data.plusWH,v_trans_data.minusWH,v_trans_data.addplusWH,
								   v_trans_data.addminusWH,v_trans_data.ttype,v_trans_data.cdate);	
			EXCEPTION WHEN OTHERS THEN
				 v_error_msg := v_error_msg || v_trans_data.transid || ' - Error Message:' || SQLERRM || '\n';								   
			END;
			 gm_upd_orderusage_trans_job_flag(v_trans_data.transid,v_trans_data.ttype);
		 END LOOP;
		
		 --If any error occur send email by using gm_com_send_email_prc.
			IF v_error_msg IS NOT NULL THEN
			to_mail := get_rule_value('LOT_TRACK_JOB','ERR_MAIL');
			subject := 'Lot Track ETL Job Error';
			mail_body := v_error_msg;
			gm_com_send_email_prc (to_mail, subject, mail_body);
			
	       END IF;
	  --dbms_output.put_line ('**********************  gm_pkg_op_lot_track_data.gm_process_order_usage End time >> '|| to_char(sysdate,'MM/dd/yyyy HH:MI:SS AM'));
	END gm_process_order_usage;
	
	
  /********************************************************************************
   * Description : Procedure to used to fetch transaction id from t502b table
   * Author      : Prabhu Vigneshwaran M D 
  *******************************************************************************/
	PROCEDURE gm_fch_502b_transaction_dtl(
		p_trans_id		IN		t502b_item_order_usage.c501_order_id%type,
		p_plusWH		IN		t922_inventory_transaction_map.c922_plus_warehouse%type,
		p_minusWH		IN		t922_inventory_transaction_map.c922_minus_warehouse%type,
		p_addl_plusWH	IN		t922_inventory_transaction_map.c922_addl_plus_warehouse%type,
		p_addl_minusWH	IN		t922_inventory_transaction_map.c922_addl_minus_warehouse%type,
		p_trans_type	IN	    t214_transactions.c901_type%type,
		p_trans_date	IN		t214_transactions.c214_created_dt%type
	)	
	AS
	
	CURSOR fch_partnum_dtl
	IS

   SELECT
    t502b.c205_part_number_id   partnum,
    t502b.c502b_usage_lot_num   cntnum,
    nvl(t5055.c5055_qty,t502b.c502b_item_qty) txnqty,
    nvl(t501.c501_distributor_id, t501.c703_sales_rep_id) locid,
    t5052.c901_location_type    loctype,
    nvl(t701.c701_distributor_name, get_dist_rep_name(c501_ship_to_id))  loccd,
    t501.c1900_company_id       cmpid,
    t501.c5040_plant_id         plantid
  FROM
    t502b_item_order_usage   t502b,
    t501_order               t501,
    v205_sterile_parts       v205,
    t5052_location_master    t5052,
    t5055_control_number     t5055,
    t701_distributor 		 t701,
    t703_sales_rep 		     t703
  WHERE
    t502b.c501_order_id = t501.c501_order_id
	AND t501.c501_order_id = p_trans_id
    AND t502b.c205_part_number_id = v205.c205_part_number_id
    AND t502b.c502b_usage_lot_num = t5055.c5055_control_number (+)
    AND t501.c501_distributor_id = t5052.c5052_ref_id (+)
    AND t502b.c502b_usage_lot_num IS NOT NULL
    AND t501.c501_order_id = t5055.c5055_ref_id (+)
    AND t501.C501_DISTRIBUTOR_ID = t701.c701_distributor_id (+)
    AND t501.C501_SHIP_TO_ID = t703.c703_sales_rep_id (+)
   -- AND t5055.c5055_update_inv_fl = 'Y'
    AND c501_void_fl IS NULL
    AND t502b.c502b_void_fl IS NULL
    AND t5052.c5052_void_fl IS NULL
    AND t5055.c5055_void_fl IS NULL
    AND c701_void_fl IS NULL
	AND c703_void_fl IS NULL;
        
	BEGIN
				
		--dbms_output.put_line('**********************gm_fch_502b_transaction_dtl**********************');
		--Fetch partnum,controlnum,qty based on orderd id from t502b table to store t5080 and t5081.
		FOR v_partnum_dtl IN fch_partnum_dtl
		LOOP
		gm_pkg_op_lot_track_data.gm_upd_lot_inventory_details(v_partnum_dtl.partnum,v_partnum_dtl.cntnum,v_partnum_dtl.txnqty,
								  p_plusWH,p_minusWH,p_addl_plusWH,p_addl_minusWH,p_trans_id,p_trans_type,
								  v_partnum_dtl.locid,v_partnum_dtl.loctype,v_partnum_dtl.loccd,
								  v_partnum_dtl.cmpid,v_partnum_dtl.plantid,p_trans_date);
		END LOOP;

	END gm_fch_502b_transaction_dtl;
	
/*********************************************************************************************
   * Description :Procedure to used to update job fl in t214 table after completion of insert
   * Author      : Prabhu Vigneshwaran M D
 *********************************************************************************************/
	PROCEDURE gm_upd_orderusage_trans_job_flag(
		p_trans_id		IN		t214_transactions.c214_id%type,
		p_trans_type	IN		t214_transactions.c901_type%type
	)
	AS
	BEGIN
   -- update job fl equal to Y when job is completed.
		UPDATE t502b_item_order_usage
		   SET c502b_job_run_fl = 'Y'
		 WHERE c501_order_id = p_trans_id
		   AND c502b_usage_lot_num is not null
           AND c502b_void_fl IS NULL; 

	END gm_upd_orderusage_trans_job_flag;	
 /*****************************************************************************************
   * Description : This Procedure  used to call as commonly to
   * 			   update or insert partnum details on t5080 and t5081.			  
   * Author      : Prabhu Vigneshwaran M D
  *****************************************************************************************/
  PROCEDURE gm_upd_lot_inventory_details (
        p_part_num        IN   t5080_lot_track_inv.c205_part_number_id%TYPE,
        p_control_num     IN   t5080_lot_track_inv.c2550_control_number%TYPE,
        p_txn_qty         IN   t5080_lot_track_inv.c5080_qty%TYPE,
        p_pluswh          IN   t5080_lot_track_inv.c901_warehouse_type%TYPE,
        p_minuswh         IN   t5080_lot_track_inv.c901_warehouse_type%TYPE,
        p_addl_pluswh     IN   t5080_lot_track_inv.c901_warehouse_type%TYPE,
        p_addl_minuswh    IN   t5080_lot_track_inv.c901_warehouse_type%TYPE,
        p_trans_id        IN   t5080_lot_track_inv.c5080_last_update_trans_id%TYPE,
        p_trans_type	  IN   t214_transactions.c901_type%type,
        p_location_id     IN   t5080_lot_track_inv.c5080_location_id%TYPE,
        p_location_type   IN   t5080_lot_track_inv.c901_location_type%TYPE,
        p_location_name   IN   t5080_lot_track_inv.c5080_location_name%TYPE,
        p_company_id      IN   t5080_lot_track_inv.c1900_company_id%TYPE,
        p_plant_id        IN   t5080_lot_track_inv.c5040_plant_id%TYPE,
        p_trans_date	  IN   t214_transactions.c214_created_dt%type
    ) 
    
    AS
    
    BEGIN
	    --
        IF ( p_pluswh IS NOT NULL ) THEN
            gm_upd_lot_inventory(p_part_num, p_control_num, p_txn_qty, p_pluswh, p_trans_id,p_trans_type,
                                 p_location_id, p_location_type, p_location_name, p_company_id, p_plant_id,p_trans_date);
        END IF;
		--
        IF ( p_minuswh IS NOT NULL ) THEN
            gm_upd_lot_inventory(p_part_num, p_control_num, -1 * p_txn_qty, p_minuswh, p_trans_id,p_trans_type,
                                 p_location_id, p_location_type, p_location_name, p_company_id, p_plant_id,p_trans_date);

        END IF;
	    --
        IF ( p_addl_pluswh IS NOT NULL ) THEN
            gm_upd_lot_inventory(p_part_num, p_control_num, p_txn_qty, p_addl_pluswh, p_trans_id,p_trans_type,
                                 p_location_id, p_location_type, p_location_name, p_company_id, p_plant_id,p_trans_date);
        END IF;
		--
        IF ( p_addl_minuswh IS NOT NULL ) THEN
            gm_upd_lot_inventory(p_part_num, p_control_num, -1 * p_txn_qty, p_addl_minuswh, p_trans_id,p_trans_type,
                                 p_location_id, p_location_type, p_location_name, p_company_id, p_plant_id,p_trans_date);
        END IF;

  END gm_upd_lot_inventory_details;
  
  
  
 /*********************************************************************************************
   * Description : Procedure is used to fetch the shipped consign set details
   * Author      : Bala
  *********************************************************************************************/
	PROCEDURE gm_process_ship_trans		
	AS
	
	CURSOR fch_ship_trans_data IS  
	SELECT * FROM (
	SELECT c907_ref_id transid, 't504' transval, c901_source ttype,
		                c901_source transtype , '4000339' plusWH , '26240819' minusWH , c907_shipped_dt cdate         	                
		  FROM t907_shipping_info t907 , t504_consignment t504,t207_set_master t207 
		 WHERE t907.c907_ref_id = t504.c504_consignment_id
		   AND t207.c207_set_id = t504.c207_set_id
		   AND c901_source = 50181 -- Consignment
		   AND t504.c504_ship_to in (4120,4121) --> Distributor
		   AND c907_status_fl = 40 --Shipped
		   AND t504.c504_type = 4110 --Consignment
		   AND C207_LOT_TRACK_FL = 'Y'
		   AND t504.c207_set_id is not null
		   AND t907.c907_job_run_fl is null
		   AND t207.c207_void_fl IS NULL
		   AND t504.c504_void_fl IS NULL
		   AND t907.c907_void_fl IS NULL) 
		   GROUP BY transid,transval,ttype,transtype,plusWH,minusWH,cdate
		   ORDER BY cdate;
		  

		BEGIN
			
			FOR v_ship_trans_data IN fch_ship_trans_data
			LOOP
					--dbms_output.put_line('gm_process_ship_trans==> ' || v_ship_trans_data.transid );
				
				gm_fch_transaction_dtl(v_ship_trans_data.transid,v_ship_trans_data.plusWH,v_ship_trans_data.minusWH,null,
									   null,v_ship_trans_data.transval,v_ship_trans_data.ttype,v_ship_trans_data.cdate);	
									   
				gm_upd_ship_trans_job_flag(v_ship_trans_data.transid,v_ship_trans_data.ttype);
				
			END LOOP;
		   		
		
	END gm_process_ship_trans;
	
 /*********************************************************************************************
   * Description : Procedure to used to update job fl in t214 table after completion of insert
   * Author      : Agilan Singaravel
  *********************************************************************************************/
	PROCEDURE gm_upd_ship_trans_job_flag(
		p_trans_id		IN		t214_transactions.c214_id%type,
		p_trans_type	IN		t214_transactions.c901_type%type
	)
	AS
	BEGIN
		
		UPDATE t907_shipping_info
		   SET c907_job_run_fl = 'Y'
		 WHERE c907_ref_id = p_trans_id
		   AND c901_source = p_trans_type 
		   AND c907_void_fl IS NULL; 
		   
	END gm_upd_ship_trans_job_flag;	
 /*******************************************************************************************************************
   * Description : Procedure to used to fetch transaction id from t214 table for loaner trans like FGLN, LNFG , LNLQ
   * Author      : Agilan Singaravel
  *******************************************************************************************************************/
	PROCEDURE gm_fch_loaner_lot_track(
		p_trans_id		IN		t214_transactions.c214_id%type
	)
	AS
	v_error_msg    		CLOB;
	to_mail 	   		t906_rules.c906_rule_value%TYPE;
	subject 	   		VARCHAR2 (100);
	mail_body	   		CLOB;
	
	CURSOR fch_trans_data
	IS
		SELECT t214.c214_id transid, c922_value transval,t214.c901_type ttype, 
  					    c922_plus_warehouse plusWH, c922_minus_warehouse minusWH,
  				        c922_addl_plus_warehouse addplusWH,c922_addl_minus_warehouse addminusWH,trunc(c214_created_dt) cdate
		  FROM t214_transactions t214, V205_STERILE_PARTS v205,t922_inventory_transaction_map t922
		 WHERE t214.c205_part_number_id = v205.c205_part_number_id
		   AND t214.c901_type = t922.c901_type
		   AND c922_category = 'Loaner'
		   AND c214_id = p_trans_id
		   AND C214_job_run_fl IS NULL
       	   AND c922_void_fl IS NULL
	  GROUP BY t214.c214_id,c922_value,t214.c901_type,c922_plus_warehouse,c922_minus_warehouse,
      		   c922_addl_plus_warehouse,c922_addl_minus_warehouse,trunc(c214_created_dt);
	
	BEGIN	
		
		FOR v_trans_data IN fch_trans_data
		LOOP
			BEGIN
			gm_fch_transaction_dtl(v_trans_data.transid,v_trans_data.plusWH,v_trans_data.minusWH,v_trans_data.addplusWH,
								   v_trans_data.addminusWH,v_trans_data.transval,v_trans_data.ttype,v_trans_data.cdate);	
			EXCEPTION WHEN OTHERS THEN
				 v_error_msg := v_error_msg || v_trans_data.transid || ' - Error Message:' || SQLERRM || '\n';								   
			END;		
			
			gm_upd_transaction_job_flag(v_trans_data.transid,v_trans_data.ttype);
		END LOOP;
		
		IF v_error_msg IS NOT NULL THEN
			to_mail := get_rule_value('LOT_TRACK_JOB','ERR_MAIL');
			subject := 'Lot Track ETL Job Error';
			mail_body := v_error_msg;
			
			gm_com_send_email_prc (to_mail, subject, mail_body);
	    END IF;
	    
	END gm_fch_loaner_lot_track;
	
END gm_pkg_op_lot_track_data;
/