--@"C:\Database\Packages\Operations\gm_pkg_op_multi_process_consign.bdy";
CREATE OR REPLACE
PACKAGE BODY gm_pkg_op_multi_process_consign
IS
--
	/*********************************************************
	* Description : This procedure used to fetch request details
	* Author : Raja
	*********************************************************/
	PROCEDURE gm_fch_request_details (
	     p_request_id           IN    t520_request.c520_request_id%TYPE
	   , p_consignment_id       IN    t504_consignment.c504_consignment_id%TYPE
	   , p_out_request_dtls 	OUT   CLOB)
	 AS
	 v_comp_dt_fmt VARCHAR2 (20); 
     BEGIN
	    SELECT get_compdtfmt_frm_cntx() INTO v_comp_dt_fmt FROM DUAL;
	    
	    SELECT JSON_ARRAYAGG(
	   JSON_OBJECT( 
		   'requestdate' 	       value TO_CHAR (t520.c520_request_date, v_comp_dt_fmt), 
		   'requireddate' 	       value TO_CHAR (t520.c520_required_date, v_comp_dt_fmt), 
		   'planShipDate' 	       value TO_CHAR (t520.c520_planned_ship_date, v_comp_dt_fmt), 
		   'requestsourceid'       value t520.c901_request_source,
		   'requestsource'         value get_code_name (t520.c901_request_source), 
		   'requesttxnid' 	       value DECODE (c901_request_source, 50616, gm_pkg_op_sheet.get_demandsheet_name(t520.c520_request_txn_id),t520.c520_request_txn_id), 
		   'setid'  	           value t520.c207_set_id,
       	   'setname'   	           value get_set_name (t520.c207_set_id),
       	   'requesttype'           value get_code_name (t520.c901_request_by_type),
       	   'reqfor'                value t520.c520_request_for,
       	   'requestto'             value DECODE (t520.c520_request_for, 40021,get_distributor_name (t520.c520_request_to), 40025, get_account_name(t520.c520_request_to),102930, get_distributor_name (t520.c520_request_to), 40022, get_user_name (
                                    t520.c520_ship_to_id)),
           'requeststatus'         value gm_pkg_op_request_summary.get_request_status (t520.c520_status_fl),
       	   'consignid'             value t504.c504_consignment_id,
       	   'statusflag'            value t504.c504_status_fl,
       	   'requeststatusflag'     value t520.c520_status_fl,
       	   'requestfor'            value get_code_name (t520.c520_request_for),
       	   'reqto'                 value t520.c520_request_to,
       	   'requestshipto'         value t520.c901_ship_to,
           'requestshiptoid'       value t520.c520_ship_to_id,
       	   'shipto'                value get_code_name (t520.c901_ship_to),
       	   'shiptoid'              value get_code_name (t520.c520_ship_to_id),
       	   'strRequestPurpose'     value NVL(c901_purpose,0),
           'deliverymode'          value get_code_name(t907.c901_delivery_mode), 
           'deliverymodeid'        value t907.c901_delivery_mode, 
           'deliverycarrier'       value get_code_name(t907.c901_delivery_carrier), 
           'deliverycarrierid'     value t907.c901_delivery_carrier, 
           'trackingno'            value t907.c907_tracking_number, 
           'addressid'             value t907.c106_address_id,
           'shipinstr'             value t907.c907_ship_instruction,
           'overrideattn'          value t907.c907_override_attn_to,
           'reprocessid'           value t504.c504_reprocess_id
   		   )
   ORDER BY t520.c520_request_txn_id  RETURNING CLOB)
   	   INTO p_out_request_dtls
 	   FROM t520_request t520,
            t504_consignment t504,
            t907_shipping_info t907
      WHERE t520.c520_request_id = p_request_id 
        AND t520.c520_request_id = t504.c520_request_id
        AND t520.c520_void_fl IS NULL 
        AND t504.c504_void_fl IS NULL
	    AND t504. C504_CONSIGNMENT_ID = p_consignment_id
        AND T907.C907_REF_ID (+) = T520.C520_REQUEST_ID
        AND T907.C907_VOID_FL(+) IS NULL;
       
     END gm_fch_request_details;
        
    /*********************************************************
	* Description : This procedure used to process multiple consignment
	*  Author : Raja
	*********************************************************/
	PROCEDURE gm_process_multi_consign_sets(
        p_input_string      IN      VARCHAR2
	  , p_source	        IN		t907_shipping_info.c901_source%TYPE
	  , p_shipto	        IN		t907_shipping_info.c901_ship_to%TYPE
	  , p_shiptoid          IN		t907_shipping_info.c907_ship_to_id%TYPE
	  , p_shipcarr          IN		t907_shipping_info.c901_delivery_carrier%TYPE
	  , p_shipmode          IN		t907_shipping_info.c901_delivery_mode%TYPE
	  , p_addressid         IN		t907_shipping_info.c106_address_id%TYPE
	  , p_attn	            IN	    t907_shipping_info.c907_override_attn_to%TYPE 
	  , p_ship_instruction  IN	    t907_shipping_info.c907_ship_instruction%TYPE 
      , p_type	            IN 	    t504_consignment.c504_type%TYPE
      , p_purpose		    IN 	    t504_consignment.c504_inhouse_purpose%TYPE
      , p_billto		    IN 	    t504_consignment.c701_distributor_id%TYPE
      , p_shipfl		    IN 	 	t504_consignment.c504_ship_req_fl%TYPE
      , p_userid		    IN 	  	t504_consignment.c504_last_updated_by%TYPE
      , p_plan_ship_date	IN	  	T520_REQUEST.C520_PLANNED_SHIP_DATE%TYPE 
      , p_required_date	    IN	  	T520_REQUEST.C520_REQUIRED_DATE%TYPE
      , p_out_success_msg   OUT 	VARCHAR2
      , p_out_error_msg     OUT 	VARCHAR2)
      AS
	     v_string	          VARCHAR2 (30000) := p_input_string;
	     v_substring          VARCHAR2 (30000);
	     v_consign_status_fl  VARCHAR2 (300);
	     v_req_refid          VARCHAR2 (300);
	     v_consign_id	      VARCHAR2 (300);
	     v_shipid             VARCHAR2 (300);
	     v_consignment_id     VARCHAR2 (300);
	     v_refid              VARCHAR2 (300);
	     v_out_msg            VARCHAR2 (300);
	     v_req_status         NUMBER (3);
	     v_date_fmt           VARCHAR2(30);
	    
     BEGIN
	    
	  v_date_fmt := NVL(get_compdtfmt_frm_cntx(),get_rule_value('DATEFMT','DATEFORMAT'));
	     
     IF (p_input_string IS NOT NULL) THEN
        WHILE INSTR (v_string, '|') <> 0 LOOP
           v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
           v_string := SUBSTR (v_string, INSTR (v_string, '|') + 1);
        
           v_req_refid := NULL;
		  v_consign_id := NULL;
           
           v_req_refid := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
		   v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
		  v_consign_id := v_substring;
           
	    END LOOP;
	    
		    BEGIN
	          SELECT t504.c504_status_fl 
	            INTO v_consign_status_fl
	            FROM t504_consignment t504
	           WHERE t504.C504_CONSIGNMENT_ID = v_consign_id
	             AND t504.C504_VOID_FL IS NULL;
	       EXCEPTION
	            WHEN NO_DATA_FOUND THEN
	          v_consign_status_fl := 0;
	       END;
        
          BEGIN   
          SELECT c520_status_fl 
            INTO v_req_status 
            FROM t520_request
           WHERE C520_REQUEST_ID = v_req_refid
             AND C520_VOID_FL IS NULL;
           EXCEPTION
	            WHEN NO_DATA_FOUND THEN
	          v_req_status := 0;
	       END;

          IF ((v_req_status NOT IN (10,15,20)) OR (v_req_status = 20 AND (NVL(v_consign_status_fl,0) <> '2'))) THEN  -- Consignment status not in built set - 2 , Request status not in Ready to consign - 20 / backlog - 15 / backorder - 10
	       p_out_error_msg := v_req_refid;    
           RETURN;
          END IF;
          
        -- Lock the req id & consign id
       IF v_req_status <> 0  AND v_consign_status_fl <> 0 THEN 
        
          SELECT t520.C520_REQUEST_ID, t504.C504_CONSIGNMENT_ID
            INTO v_refid, v_consignment_id
            FROM t520_request t520, t504_consignment t504
           WHERE t520.C520_REQUEST_ID = v_req_refid
             AND t504.C504_CONSIGNMENT_ID = v_consign_id
             AND t520.C520_REQUEST_ID  = t504.C520_REQUEST_ID  
             AND t504.C504_VOID_FL IS NULL 
             AND t520.C520_VOID_FL IS NULL
             FOR UPDATE;
       ELSIF v_req_status <> 0 THEN   
        
          SELECT t520.C520_REQUEST_ID
            INTO v_refid
            FROM t520_request t520
           WHERE t520.C520_REQUEST_ID = v_req_refid
             AND t520.C520_VOID_FL IS NULL
             FOR UPDATE;
       END IF;
             
          UPDATE t520_request 
             SET C520_REQUIRED_DATE = p_required_date,
                 C520_PLANNED_SHIP_DATE = p_plan_ship_date
           WHERE C520_REQUEST_ID  = v_refid;
           
         BEGIN
	      
		    IF (v_req_status = 15  OR v_req_status = 10) THEN  --Request status -  backlog - 15, backorder - 10
		     
		       gm_pkg_op_request_master.gm_sav_request_edit_info(TO_CHAR(p_required_date,v_date_fmt), p_billto, p_userid, v_refid, p_shipto, p_shiptoid, NULL, p_plan_ship_date);
		
		    END IF;
	        -- fetch the shipid 
             v_shipid := gm_pkg_cm_shipping_info.get_shipping_id(v_refid, p_source,'fetch');
           
             gm_pkg_cm_shipping_trans.gm_sav_shipping(v_refid, p_source, p_shipto, p_shiptoid, p_shipcarr, p_shipmode, NULL, p_addressid, NULL, p_userid, v_shipid, p_attn, p_ship_instruction);
       
             IF (v_req_status = 20) THEN -- Request status - Ready to consign -20
             
              gm_update_set_consign(v_consignment_id, p_type, p_purpose, p_billto, p_shipto, p_shiptoid, NULL, p_shipfl, NULL, p_userid ,0,0,NULL,v_out_msg,NULL,p_plan_ship_date);
             
             END IF;
             
             IF (v_req_status = 15  OR v_req_status = 10) THEN    --Request status -  backlog - 15, backorder - 10
            
             gm_pkg_op_split.gm_sav_multi_consg_split(v_refid, p_userid);

             END IF;
             
             p_out_success_msg := v_refid;
             
        EXCEPTION WHEN OTHERS THEN
            -- When there is a problem - request (request invalid) updated value is rollback.  
              Rollback;  
               p_out_error_msg := v_req_refid;
        END;
     
 END IF;
     
 END gm_process_multi_consign_sets;
  
END gm_pkg_op_multi_process_consign;
/