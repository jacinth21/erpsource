-- @"c:\database\packages\operations\gm_pkg_op_lot_detail_report.bdy"

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_lot_detail_report
IS
--
 /************************************************************************************
  * Description : Procedure to fetch lot track product information for detail report
  * Author      : Agilan Singaravel
 **********************************************************************************/
 PROCEDURE gm_fch_lot_detail_prod_info (
      p_lot_num				IN		t5080_lot_track_inv.c2550_control_number%type, 
      p_part_num			IN		t205_part_number.C205_part_number_id%type,
      p_like_search			IN		varchar2,
      p_lot_prod_info		OUT		CLOB
 )
 AS
 v_date_fmt		VARCHAR2 (20);
 BEGIN
	 SELECT get_compdtfmt_frm_cntx ()
	   INTO v_date_fmt
	   FROM DUAL;
	
	   
	 SELECT JSON_ARRAYAGG(JSON_OBJECT('PARTNO' VALUE partno, 
	 								  'PARTDESC' VALUE partdesc, 
	 								  'CNTROLNO' VALUE cntrolno, 
	 								  'EXPIRYDATE' VALUE expirydate, 
	 								  'VENDOR' VALUE vendor
	 					 ) RETURNING CLOB) INTO p_lot_prod_info
	  FROM (
        	SELECT t205.c205_part_number_id     partno,
            	   regexp_replace(t205.c205_part_num_desc, '[^ ,0-9A-Za-z]', '') partdesc,
            	   t5080.c2550_control_number   cntrolno,
            	   to_char(t2550.c2550_expiry_date, v_date_fmt) expirydate,
            	   regexp_replace(t301.c301_vendor_name , '[^ ,0-9A-Za-z]', '')  vendor
        	  FROM T5080_LOT_TRACK_INV t5080,
            	   T205_PART_NUMBER t205,
            	   T2550_PART_CONTROL_NUMBER  t2550,
            	   T301_VENDOR t301
             WHERE t2550.c301_vendor_id = t301.c301_vendor_id
               AND t2550.c205_part_number_id = t205.c205_part_number_id
               AND t2550.c2550_control_number = t5080.c2550_control_number
               AND t2550.c205_part_number_id = t5080.c205_part_number_id
               AND t5080.c205_part_number_id like decode(p_like_search,'LIT',upper(p_part_num),
               				'LIKEPRE', upper(p_part_num||'%'),
               				'LIKESUF', upper('%'||p_part_num),
               				upper('%'||p_part_num||'%'))
              AND t5080.c2550_control_number =  upper(p_lot_num)
              AND t301.c301_active_fl IS NULL
              AND t5080.c5080_void_fl IS NULL
         GROUP BY t205.c205_part_number_id,t205.c205_part_num_desc,t5080.c2550_control_number,t2550.c2550_expiry_date,t301.c301_vendor_name
        UNION
        
	        SELECT
	            t5072.c205_part_number_id    partno,
	            regexp_replace(t205.c205_part_num_desc, '[^ ,0-9A-Za-z]', '') partdesc,
	            t5072.c5072_control_number   cntrolno,
	            to_char(t2550.c2550_expiry_date,v_date_fmt) expirydate,
	            t301.c301_vendor_name        vendor
	        FROM
	            t5072_set_part_lot_qty      t5072,
	            t205_part_number            t205,
	            t2550_part_control_number   t2550,
	            t5070_set_lot_master        t5070,
	            t301_vendor                 t301,
	            V205_STERILE_PARTS 			v205
	        WHERE
	            t2550.c205_part_number_id = t205.c205_part_number_id
	            AND t205.c205_part_number_id = t5072.c205_part_number_id
	            AND t5072.c5072_control_number = t2550.c2550_control_number
	            AND t5072.c5070_set_lot_master_id = t5070.c5070_set_lot_master_id
	            AND t2550.c301_vendor_id = t301.c301_vendor_id
				AND t5072.c205_part_number_id = v205.c205_part_number_id
	            AND t5072.c205_part_number_id like decode(p_like_search,'LIT',upper(p_part_num),
               				'LIKEPRE', upper(p_part_num||'%'),
               				'LIKESUF', upper('%'||p_part_num),
               				upper('%'||p_part_num||'%'))
	            AND t5072.c5072_control_number =  upper(p_lot_num)
	            AND t5072.c5072_void_fl IS NULL
	            AND c5070_void_fl IS NULL
	       GROUP BY t5072.c205_part_number_id,t205.c205_part_num_desc,t5072.c5072_control_number,
	       			t2550.c2550_expiry_date,t301.c301_vendor_name
	    			);  
		       
 END gm_fch_lot_detail_prod_info;
 
  /************************************************************************************
   * Description : Procedure to fetch lot track Receiving information for detail report
   * Author      : Agilan Singaravel
  **********************************************************************************/
 PROCEDURE gm_fch_lot_detail_receiving_info (
      p_lot_num				IN		t5080_lot_track_inv.c2550_control_number%type, 
      p_part_num			IN		t205_part_number.C205_part_number_id%type,
      p_like_search			IN		varchar2,
      p_lot_rec_info		OUT		CLOB
 )
 AS
 v_date_fmt  VARCHAR2(20);
 BEGIN
	 SELECT get_compdtfmt_frm_cntx ()
	   INTO v_date_fmt
	   FROM DUAL;
	 
	   SELECT JSON_ARRAYAGG(JSON_OBJECT(
	   				'DHRID' VALUE DHRID,
  				  	'QTY' VALUE QTY,
  				  	'PACKSLIP' VALUE PACKSLIP,
  				  	'VENDOR' VALUE VENDOR,
  				  	'RECEIVEDDATE' VALUE RECEIVEDDATE,
  				  	'RELEASEDDATE' VALUE RELEASEDDATE,
  				  	'STATUS' VALUE STATUS
  			 )RETURNING CLOB) INTO p_lot_rec_info
        FROM (
        	SELECT t408.c408_dhr_id DHRID,
        		   t408.c408_qty_received QTY, 
        		   t408.c408_packing_slip PACKSLIP,
        		   regexp_replace(t301.c301_vendor_name, '[^ ,0-9A-Za-z]', '') VENDOR,
         		   TO_CHAR(t408.c408_created_date,v_date_fmt) RECEIVEDDATE,
         		   TO_CHAR(t408.c408_verified_ts,v_date_fmt)  RELEASEDDATE,
                   gm_pkg_op_lotcode_rpt.get_dhr_statusnm(t408.c408_status_fl) STATUS
              FROM T408_DHR t408, T301_VENDOR t301
	         WHERE t408.c301_vendor_id = t301.c301_vendor_id
	           AND t408.c408_control_number =  upper(p_lot_num)
               AND t408.c205_part_number_id like decode(p_like_search,'LIT',upper(p_part_num),
               				'LIKEPRE', upper(p_part_num||'%'),
               				'LIKESUF', upper('%'||p_part_num),
               				upper('%'||p_part_num||'%'))         	   
               AND t408.c408_void_fl IS NULL
		  GROUP BY t408.c408_dhr_id ,t408.c408_qty_received,t408.c408_packing_slip,t301.c301_vendor_name,
		  		   t408.c408_created_date,t408.c408_verified_ts,t408.c408_status_fl
	 UNION
	 		SELECT t408.c408_dhr_id DHRID,
        		   t408.c408_qty_received QTY, 
        		   t408.c408_packing_slip PACKSLIP,
        		   regexp_replace(t301.c301_vendor_name, '[^ ,0-9A-Za-z]', '') VENDOR,
         		   TO_CHAR(t408.c408_created_date,v_date_fmt) RECEIVEDDATE,
         		   TO_CHAR(t408.c408_verified_ts,v_date_fmt)  RELEASEDDATE,
                   gm_pkg_op_lotcode_rpt.get_dhr_statusnm(t408.c408_status_fl) STATUS
              FROM T408_DHR t408, T301_VENDOR t301, T408A_DHR_CONTROL_NUMBER t408a
	         WHERE t408.c408_dhr_id = t408a.c408_dhr_id
               AND t408.c301_vendor_id = t301.c301_vendor_id
               AND t408a.c205_part_number_id like decode(p_like_search,'LIT',upper(p_part_num),
               				'LIKEPRE', upper(p_part_num||'%'),
               				'LIKESUF', upper('%'||p_part_num),
               				upper('%'||p_part_num||'%'))
         	   AND t408a.c408a_conrol_number =  upper(p_lot_num)
               AND t408.c408_void_fl IS NULL
		       AND t408a.c408a_void_fl IS NULL
		  GROUP BY t408.c408_dhr_id,t408.c408_qty_received,t408.c408_packing_slip,t301.c301_vendor_name,
		  		   t408.c408_created_date,t408.c408_verified_ts,t408.c408_status_fl);
         
 END gm_fch_lot_detail_receiving_info;
 
  /*******************************************************************
   * Description : Procedure to fetch lot track inventory information 
   * Author      : Agilan Singaravel
  ********************************************************************/
 PROCEDURE gm_fch_lot_detail_inventory_info (
      p_lot_num				IN		t5080_lot_track_inv.c2550_control_number%type, 
      p_part_num			IN		t205_part_number.C205_part_number_id%type,
      p_like_search			IN		varchar2,
      p_lot_inv_info		OUT		CLOB
 )
 AS
 BEGIN
	 
	 SELECT JSON_ARRAYAGG(JSON_OBJECT(
	 		        'WAREHOUSEID' VALUE t5080.c901_warehouse_type,
	   				'WAREHOUSETYPE' VALUE get_code_name (t5080.c901_warehouse_type),
  				  	'QTY' VALUE SUM(t5080.c5080_qty),
  				  	'COMPANYNAME' VALUE get_company_name(t5080.c1900_company_id),
  				  	'PLANTNAME' VALUE get_plant_name(t5080.c5040_plant_id),
  				  	'COMPANYID' VALUE t5080.c1900_company_id,
  				  	'PLANTID' VALUE t5080.c5040_plant_id
  			 )RETURNING CLOB) INTO p_lot_inv_info 
	    FROM T5080_LOT_TRACK_INV t5080, T2550_PART_CONTROL_NUMBER t2550
	   WHERE t2550.c205_part_number_id = t5080.c205_part_number_id  
         AND t5080.c2550_control_number = t2550.c2550_control_number
         AND t5080.c901_warehouse_type NOT IN (4000339,56002,106221)         --FS, Account, Loaner
         AND t5080.c205_part_number_id like decode(p_like_search,'LIT',upper(p_part_num),
               				'LIKEPRE', upper(p_part_num||'%'),
               				'LIKESUF', upper('%'||p_part_num),
               				upper('%'||p_part_num||'%'))
         AND upper(t5080.c2550_control_number) = upper(p_lot_num)
         AND t5080.c5080_void_fl IS NULL
    GROUP BY t5080.c901_warehouse_type, t5080.c1900_company_id, t5080.c5040_plant_id;
	 
 END gm_fch_lot_detail_inventory_info;
 
   /************************************************************************************
   * Description : Procedure to fetch lot track detail report for field sales/Account
   * Author      : Agilan Singaravel
  **********************************************************************************/
 PROCEDURE gm_fch_lot_detail_fieldsales_info (
      p_lot_num					IN		t5080_lot_track_inv.c2550_control_number%type, 
      p_part_num				IN		t205_part_number.C205_part_number_id%type,
      p_like_search				IN		varchar2,
      p_lot_fieldsales_info		OUT		CLOB
 )
 AS
 BEGIN
	 	 
	 SELECT JSON_ARRAYAGG(JSON_OBJECT(
	   				'NAME' VALUE t5080.c5080_location_name,
  				  	'QTY' VALUE SUM(t5080.c5080_qty),
  				  	'COMPANYNAME' VALUE get_company_name(t5080.c1900_company_id),
  				  	'COMPANYID' VALUE t5080.c1900_company_id
  			 )RETURNING CLOB) INTO p_lot_fieldsales_info 
	    FROM T5080_LOT_TRACK_INV t5080
	   WHERE upper(t5080.c2550_control_number) = upper(p_lot_num)
	     AND t5080.c205_part_number_id like decode(p_like_search,'LIT',upper(p_part_num),
               				'LIKEPRE', upper(p_part_num||'%'),
               				'LIKESUF', upper('%'||p_part_num),
               				upper('%'||p_part_num||'%'))
		 AND t5080.c901_warehouse_type IN (4000339,56002)         --FS, Account
         AND t5080.c5080_void_fl IS NULL
    GROUP BY t5080.c5080_location_name, t5080.c1900_company_id;
	 
	 
 END gm_fch_lot_detail_fieldsales_info;
 
  /*******************************************************************************
   * Description : Procedure to fetch lot track detail report based on consignment
   * Author      : Agilan Singaravel
  **********************************************************************************/
 PROCEDURE gm_fch_lot_detail_consignment_info (
      p_lot_num					IN		t5080_lot_track_inv.c2550_control_number%type, 
      p_part_num				IN		t205_part_number.C205_part_number_id%type,
      p_like_search				IN		varchar2,
      p_lot_consignment_info	OUT		CLOB
 )
 AS
 BEGIN
	 
	 SELECT JSON_ARRAYAGG(JSON_OBJECT(
	   				'SETID' VALUE t5081.c207_set_id,
  				  	'SETNAME' VALUE t5081.c207_set_name,
  				  	'QTY' VALUE SUM(t5081.c5081_txn_qty),
  				  	'COMPANYNAME' VALUE get_company_name(t5081.c1900_company_id),
  				  	'PLANTNAME' VALUE get_plant_name(t5081.c5040_plant_id),
  				  	'COMPANYID' VALUE t5081.c1900_company_id,
  				  	'PLANTID' VALUE t5081.c5040_plant_id
  			 )RETURNING CLOB) INTO p_lot_consignment_info 
	    FROM T5081_LOT_TRACK_INV_LOG t5081
	   WHERE upper(t5081.c2550_control_number) = upper(p_lot_num)
	     AND t5081.c205_part_number_id like decode(p_like_search,'LIT',upper(p_part_num),
               				'LIKEPRE', upper(p_part_num||'%'),
               				'LIKESUF', upper('%'||p_part_num),
               				upper('%'||p_part_num||'%'))
	     AND t5081.c901_warehouse_type = 26240819  --consignment Qty
         AND t5081.c5081_void_fl IS NULL
    GROUP BY t5081.c207_set_id, t5081.c1900_company_id, t5081.c5040_plant_id,t5081.c207_set_name;
	 
 END gm_fch_lot_detail_consignment_info;
 
   /**********************************************************************
   * Description : Procedure to fetch lot track detail report for loaner
   * Author      : Agilan Singaravel
  ************************************************************************/
 PROCEDURE gm_fch_lot_detail_loaner_info (
      p_lot_num					IN		t5080_lot_track_inv.c2550_control_number%type, 
      p_part_num				IN		t205_part_number.C205_part_number_id%type,
      p_like_search				IN		varchar2,
      p_lot_loaner_info		    OUT		CLOB
 )
 AS
 BEGIN
	 
--	
 SELECT JSON_ARRAYAGG(JSON_OBJECT(
	   				'SETID' VALUE t504.c207_set_id,
  				  	'SETNAME' VALUE get_set_name(t504.c207_set_id),
  				  	'QTY' VALUE SUM(t5072a.c5072a_new_qty),
  				  	'COMPANYNAME' VALUE get_company_name(t504.c1900_company_id),
  				  	'PLANTNAME' VALUE get_plant_name(t504.c5040_plant_id),
  				  	'COMPANYID' VALUE t504.c1900_company_id,
  				  	'PLANTID' VALUE t504.c5040_plant_id
  			 )RETURNING CLOB) INTO p_lot_loaner_info 
	    FROM t5072_set_part_lot_qty t5072, t5072a_set_part_lot_qty_log t5072a , t504_consignment t504,V205_STERILE_PARTS v205 
	   WHERE t5072a.c5072_set_part_lot_qty_id = t5072.c5072_set_part_lot_qty_id
	     AND t5072.c205_part_number_id  = t5072a.c205_part_number_id 
	     AND t5072.c205_part_number_id  = v205.c205_part_number_id 
	     AND t5072a.c5072a_txn_id = t504.c504_consignment_id
	     AND t5072a.c205_part_number_id like decode(p_like_search,'LIT',upper(p_part_num),
               				'LIKEPRE', upper(p_part_num||'%'),
               				'LIKESUF', upper('%'||p_part_num),
               				upper('%'||p_part_num||'%'))
	     AND upper(t5072a.c5072_control_number) = upper(p_lot_num)
        -- AND T5081.C901_WAREHOUSE_TYPE IN(106221)  -- Loaner Inventory
   GROUP BY t504.c207_set_id,t504.c1900_company_id,t504.c5040_plant_id;
	 
 END gm_fch_lot_detail_loaner_info;
 
  /******************************************************************************
   * Description : Procedure to fetch lot track detail report transaction history
   * Author      : Agilan Singaravel
  *********************************************************************************/
 PROCEDURE gm_fch_lot_txn_history (
      p_lot_num					IN		t5080_lot_track_inv.c2550_control_number%type, 
      p_part_num				IN		t205_part_number.C205_part_number_id%type,
      p_inv_type				IN		varchar2,
      p_like_search				IN		varchar2,
      p_company_id				IN		varchar2,
      p_name					IN		varchar2,
      p_set_id					IN		varchar2,
      p_lot_txnhis_info		    OUT		CLOB
 )
 AS
  v_date_fmt		VARCHAR2 (20);
 BEGIN
	 SELECT get_compdtfmt_frm_cntx ()
	   INTO v_date_fmt
	   FROM DUAL;
	   
	 IF p_inv_type = '4000339' THEN  --Field Sales
	 		
	 	 SELECT JSON_ARRAYAGG(JSON_OBJECT(
	   				'TRANSID' VALUE t5081.c5081_last_update_trans_id,
  				  	'PARTNUM' VALUE t5081.c205_part_number_id,
  				  	'QTY' VALUE t5081.c5081_txn_qty,
  				  	'CREATEDDATE' VALUE TO_CHAR(t5081.c5081_created_date,v_date_fmt),
  				  	'TRANSTYPE' VALUE get_code_name(t5081.c901_last_transaction_type),
  				  	'WAREHOSETYPE' VALUE get_code_name(t5081.c901_warehouse_type),
  				  	'LOCATIONNAME' VALUE t5081.c5080_location_name,
  				  	'COMPANYNAME' VALUE get_company_name(t5081.c1900_company_id)
  			 )RETURNING CLOB) INTO p_lot_txnhis_info 
	    FROM T5080_LOT_TRACK_INV t5080,T5081_LOT_TRACK_INV_LOG t5081
	   WHERE t5080.c5080_control_number_inv_id = t5081.c5080_control_number_inv_id
	     AND t5080.c205_part_number_id = t5081.c205_part_number_id
	     AND T5080.c901_warehouse_type IN (4000339,56002) --FS/Account
	     AND t5081.c205_part_number_id like decode(p_like_search,'LIT',upper(p_part_num),
               				'LIKEPRE', upper(p_part_num||'%'),
               				'LIKESUF', upper('%'||p_part_num),
               				upper('%'||p_part_num||'%'))
	     AND upper(t5081.c2550_control_number) = upper(p_lot_num)
	     AND t5081.c1900_company_id = p_company_id
	     AND t5081.c5080_location_name = p_name
         AND t5081.c5081_void_fl IS NULL
         AND t5080.c5080_void_fl IS NULL;
	 
      ELSIF p_inv_type = '4311' THEN  --Consignment
       	 
      	 SELECT JSON_ARRAYAGG(JSON_OBJECT(
	   				'TRANSID' VALUE t5081.c5081_last_update_trans_id,
  				  	'TAGID' VALUE t5081.c5010_tag_id,
  				  	'QTY' VALUE t5081.c5081_txn_qty,
  				  	'SETID' VALUE t5081.c207_set_id,
  				  	'SETNAME' VALUE t5081.c207_set_name,
  				  	'LOCATIONNAME' VALUE t5081.c2550_control_number,
  				  	'COMPANYNAME' VALUE get_company_name(t5081.c1900_company_id),
  				  	'PLANTNAME' VALUE get_plant_name(t5081.c5040_plant_id)
  			 )RETURNING CLOB) INTO p_lot_txnhis_info 
	    FROM T5081_LOT_TRACK_INV_LOG t5081
	   WHERE upper(t5081.c2550_control_number) = upper(p_lot_num)
	     AND t5081.c205_part_number_id like decode(p_like_search,'LIT',upper(p_part_num),
               				'LIKEPRE', upper(p_part_num||'%'),
               				'LIKESUF', upper('%'||p_part_num),
               				upper('%'||p_part_num||'%'))
	     AND t5081.c901_warehouse_type = '26240819'  --Consignment Qty	     
         AND t5081.c1900_company_id = p_company_id
         AND t5081.c5040_plant_id = p_name
         AND nvl(t5081.c207_set_id,-999) = nvl(p_set_id,-999)
         AND t5081.c5081_void_fl IS NULL;
      
      ELSE
      
	  SELECT JSON_ARRAYAGG(JSON_OBJECT(
	  				'TRANSID' VALUE t5072a.c5072a_txn_id,
	  				'TAGID' VALUE '',
	  				'QTY' VALUE t5072a.c5072a_new_qty,
	   				'SETID' VALUE t504.c207_set_id,
  				  	'SETNAME' VALUE get_set_name(t504.c207_set_id),
  				  	'LOCATIONNAME' VALUE t5072a.c5072_control_number,
  				  	'COMPANYNAME' VALUE get_company_name(t504.c1900_company_id),
  				  	'PLANTNAME' VALUE get_plant_name(t504.c5040_plant_id)
  			 )RETURNING CLOB) INTO p_lot_txnhis_info 
	    FROM t5072_set_part_lot_qty t5072, t5072a_set_part_lot_qty_log t5072a , t504_consignment t504,V205_STERILE_PARTS v205 
	   WHERE t5072a.c5072_set_part_lot_qty_id = t5072.c5072_set_part_lot_qty_id
	     AND t5072.c205_part_number_id  = t5072a.c205_part_number_id 
	     AND t5072.c205_part_number_id = v205.c205_part_number_id
	     AND t5072a.c5072a_txn_id = t504.c504_consignment_id
	     AND t5072a.c205_part_number_id like decode(p_like_search,'LIT',upper(p_part_num),
               				'LIKEPRE', upper(p_part_num||'%'),
               				'LIKESUF', upper('%'||p_part_num),
               				upper('%'||p_part_num||'%'))
	     AND upper(t5072a.c5072_control_number) = upper(p_lot_num)
	     AND t504.c1900_company_id = p_company_id
	     AND t504.c5040_plant_id = p_name
	     AND nvl(t504.c207_set_id,-999) = nvl(p_set_id,-999)
	     AND t504.c504_void_fl is null;
        -- AND T5081.C901_WAREHOUSE_TYPE IN(106221);  -- Loaner Inventory	
      
      END IF;
 END gm_fch_lot_txn_history;
 
 /*********************************************************************************
* Description : This procedure is used to fetch Lot Track details PMT#54256
* Author      : Vinoth
* Parameters  : Control Number Inv Id
**********************************************************************************/
PROCEDURE gm_fch_lot_track_details (
        p_cntrl_num_inv_id   IN  t5080_lot_track_inv.c5080_control_number_inv_id%TYPE,
        p_lot_track_details  OUT TYPES.cursor_type)
AS
  v_date_fmt   VARCHAR2 (20) ;
  
BEGIN

    OPEN p_lot_track_details 
      FOR 
         SELECT C5080_CONTROL_NUMBER_INV_ID CNTRLINVID, C205_PART_NUMBER_ID PNUMID, GET_PARTNUM_DESC(C205_PART_NUMBER_ID) PARTDESC, 
         		C2550_CONTROL_NUMBER CNUM, C5080_QTY QTY, C901_WAREHOUSE_TYPE WHTYPE, 
         		C5080_LOCATION_NAME LOCNM, C5080_LOCATION_ID LOCID, C1900_COMPANY_ID CMPID, C5040_PLANT_ID PLNTID
		   FROM t5080_lot_track_inv
		  WHERE C5080_CONTROL_NUMBER_INV_ID = p_cntrl_num_inv_id
			AND C5080_VOID_FL IS NULL;
  END gm_fch_lot_track_details;

/*****************************************************************************************
   * Description : Procedure to used to update or insert partnum details on t5080 and t5081
   * Author      : Vinoth
  *****************************************************************************************/
	PROCEDURE gm_upd_lot_track_inventory(
		p_cntrl_inv_id	IN		t5080_lot_track_inv.c5080_control_number_inv_id%TYPE,
		p_part_num		IN		t5080_lot_track_inv.c205_part_number_id%TYPE,
		p_control_num	IN		t5080_lot_track_inv.c2550_control_number%TYPE,
		p_txn_qty		IN		t5080_lot_track_inv.c5080_qty%TYPE,
		p_whType		IN		t5080_lot_track_inv.c901_warehouse_type%TYPE,
		p_location_id	IN		t5080_lot_track_inv.c5080_location_id%TYPE,
		p_company_id	IN		t5080_lot_track_inv.c1900_company_id%TYPE,
		p_plant_id		IN		t5080_lot_track_inv.c5040_plant_id%TYPE
	)
	AS
	v_location_name	t5080_lot_track_inv.c5080_location_name%TYPE;
	v_location_id	t5080_lot_track_inv.c5080_location_id%TYPE;
	v_company_id 	t5080_lot_track_inv.c1900_company_id%TYPE;
  	v_plant_id   	t5080_lot_track_inv.c5040_plant_id%TYPE;
	BEGIN
		
		SELECT get_compid_frm_cntx(), get_plantid_frm_cntx() 
		  INTO v_company_id, v_plant_id
		  FROM DUAL;
		  
		IF p_whType = 4000339 THEN
        SELECT get_distributor_name(p_location_id) INTO v_location_name FROM DUAL;
        v_location_id := p_location_id;
        ELSIF p_whType = 56002 THEN
        SELECT get_account_name(p_location_id) INTO v_location_name FROM DUAL;
        v_location_id := p_location_id;
        ELSE 
        BEGIN
        SELECT c5052_location_id INTO v_location_id
          FROM t5052_location_master
         WHERE c5052_location_cd = p_location_id
           AND c5040_plant_id = p_plant_id
           AND c5052_void_fl is NULL;
           v_location_name := p_location_id;
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
        v_location_name := p_location_id;
        v_location_id := NULL;
        END;
        END IF;

        IF p_location_id IS NULL THEN
        SELECT c5080_location_id, c5080_location_name
          INTO v_location_id, v_location_name FROM t5080_lot_track_inv
         WHERE c5080_control_number_inv_id = p_cntrl_inv_id;
        END IF;
		
		UPDATE T5080_LOT_TRACK_INV
	   	   SET c2550_control_number = p_control_num,
	   	   	   c205_part_number_id = p_part_num,
	  	   	   c5080_qty = p_txn_qty,
	   	   	   c901_warehouse_type = p_whType,
	   	   	   c5080_location_id = v_location_id,
	   	   	   c5080_location_name = v_location_name,
	   	   	   c901_last_transaction_type = 4316,
	   	   	   c1900_company_id = p_company_id,
	   	   	   c5040_plant_id = p_plant_id,
	   	   	   c5080_last_updated_by = '30301',
	   	   	   c5080_last_updated_date = current_date
	 	 WHERE c5080_control_number_inv_id = p_cntrl_inv_id
           AND c5080_void_fl IS NULL;
           
	 IF (SQL%ROWCOUNT = 0) THEN
	 
			INSERT INTO T5080_LOT_TRACK_INV(c5080_control_number_inv_id,c2550_control_number,c205_part_number_id,c5080_qty,c901_warehouse_type,c5080_location_id,
										    c5080_location_name,c5080_last_update_trans_id,c901_last_transaction_type,c1900_company_id,c5040_plant_id,c5080_created_by,c5080_created_date) 
	     		 VALUES (S5080_LOT_TRACK_INV.nextval,p_control_num,p_part_num,p_txn_qty,p_whType,v_location_id,v_location_name,'INV_ADJ',4316,p_company_id,p_plant_id,'30301',current_date);
	 END IF;
	 		
	END gm_upd_lot_track_inventory;

/********************************************************
   * Description : Procedure to used to fetch Account Lists
   * Author      : Vinoth
  ***********************************************************/
PROCEDURE gm_fetch_account_list (
    p_message	  OUT	VARCHAR2
  , p_recordset   OUT	TYPES.cursor_type
  , p_acct_type_id IN VARCHAR2 DEFAULT NULL
)
AS
/*
	  Description			:This procedure is called for Project Reporting
	  Parameters			:p_column
							:p_recordset
*/
	v_str		   VARCHAR2 (2000);
	v_company_id   t704_account.c1900_company_id%TYPE;
	v_plant_id     t5040_plant_master.c5040_plant_id%TYPE;
	v_comp_lang_id T1900_COMPANY.C901_LANGUAGE_ID%TYPE;
	v_com_plant_Fl  VARCHAR2(20);
	v_condition     CLOB;
	v_query         CLOB;
	
BEGIN
	--
     SELECT get_compid_frm_cntx (),get_plantid_frm_cntx() INTO v_company_id , v_plant_id  FROM DUAL;
	 SELECT NVL(get_complangid_frm_cntx(),GET_RULE_VALUE('DEFAULT_LANGUAGE','ONEPORTAL')) INTO  v_comp_lang_id FROM DUAL;
     BEGIN
     	SELECT GET_RULE_VALUE(v_company_id,'LOANERINITFILTER') INTO v_com_plant_Fl FROM DUAL;
     EXCEPTION WHEN NO_DATA_FOUND THEN
		v_com_plant_Fl := NULL;
	 END;
	 
     IF v_com_plant_Fl =  'Plant'   THEN
		 v_company_id := null;
	 ELSE
	 	 v_plant_id := null;
	 END IF;      	
	
	OPEN p_recordset
	 FOR 
	  SELECT c704_account_id ID, DECODE(v_comp_lang_id,'103097',NVL(c704_account_nm_en,c704_account_nm),c704_account_nm) NAME, c703_sales_rep_id repid
		   , get_dist_rep_name (c703_sales_rep_id) dname, get_rep_name (c703_sales_rep_id) rname
		   , get_account_gpo_id (c704_account_id) gpoid, get_distributor_id (c703_sales_rep_id) did,C101_DEALER_ID dealid
	    FROM t704_account
	   WHERE c704_void_fl IS NULL
		 AND c901_account_type = nvl(p_acct_type_id,c901_account_type)
		 AND (c901_ext_country_id is NULL	
		  OR c901_ext_country_id  in (select country_id from v901_country_codes)) 
	   ORDER BY UPPER(NAME);
	    
EXCEPTION
	WHEN OTHERS
	THEN
		p_message	:= '1000';
END gm_fetch_account_list;	

--
END gm_pkg_op_lot_detail_report;
/