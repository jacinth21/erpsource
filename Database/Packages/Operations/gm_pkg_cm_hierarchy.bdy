/* Formatted on 2009/07/30 17:56 (Formatter Plus v4.8.0) */
-- @"c:\database\packages\operations\gm_pkg_cm_hierarchy.bdy"
-- show err  Package Body gm_pkg_cm_hierarchy
-- exec gm_pkg_op_ld_demand.gm_op_ld_demand_main(19);
/*
 DELETE FROM T4042_DEMAND_SHEET_DETAIL;
 DELETE FROM T4041_DEMAND_SHEET_MAPPING;
 DELETE FROM T4040_DEMAND_SHEET;
 COMMIT ;
 exec gm_pkg_op_ld_demand.gm_op_ld_demand_main(5);

*/

CREATE OR REPLACE PACKAGE BODY gm_pkg_cm_hierarchy
IS
	/*******************************************************
	* Purpose: This procedure will be used to generate and email
	*			group depending in the type and id (ref id)
	*******************************************************/
	PROCEDURE gm_cm_mail_grp (
		p_type		   IN	t9352_hierarchy_flow.c901_ref_type%TYPE
	  , p_ref_id	   IN	t9352_hierarchy_flow.c9352_ref_id%TYPE
	  , p_subject	   IN	VARCHAR2
	  , p_mail_body    IN	VARCHAR2
	  , p_request_by   IN	t4020_demand_master.c4020_created_by%TYPE
	)
	AS
		--
		v_hierarchy_id t9350_hierarchy.c9350_hierarchy_id%TYPE;
		v_to_mail	   VARCHAR2 (4000);
		v_current_user_email_id VARCHAR2 (4100);
	--
	BEGIN
		--
		DBMS_OUTPUT.put_line ('Inside gm_cm_mail_grp	****' || p_type || '- -' || p_ref_id);

		BEGIN
			--
			/*	Currently below code not used
			 SELECT c9350_hierarchy_id
			  INTO v_hierarchy_id
			  FROM t9352_hierarchy_flow
			 WHERE c901_ref_type = p_type AND c9352_ref_id = p_ref_id;

			 v_to_mail	:= gm_pkg_cm_hierarchy.get_cm_fch_grp_email_id (v_hierarchy_id);

			 */

			--
			v_to_mail	:= get_rule_value ('ORDERPLAN', 'EMAIL');

			SELECT get_user_emailid (p_request_by)
			  INTO v_current_user_email_id
			  FROM DUAL;

			v_current_user_email_id := v_to_mail || ',' || v_current_user_email_id;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				v_to_mail	:= get_rule_value ('ORDERPLAN', 'EMAIL');
		END;

		--
		gm_com_send_email_prc (v_current_user_email_id, p_subject, p_mail_body);
		DBMS_OUTPUT.put_line ('Email Name to completed	   ****' || v_to_mail);
	--
	END gm_cm_mail_grp;

	/*******************************************************
	* Purpose: This Function will be used to fetch group
	*			 email id information
	*******************************************************/
	FUNCTION get_cm_fch_grp_email_id (
		p_hierarchy_id	 t9350_hierarchy.c9350_hierarchy_id%TYPE
	)
		RETURN VARCHAR2
	IS
		--
		v_email_id	   VARCHAR2 (4000);

		CURSOR email_cur
		IS
			SELECT t101.c101_email_id email_id
			  FROM t9351_hierarchy_team t9351, t101_user t101
			 WHERE c9350_hierarchy_id = p_hierarchy_id AND t9351.c101_user_id = t101.c101_user_id;
	BEGIN
		--
		v_email_id	:= '';

		FOR email_val IN email_cur
		LOOP
			--
			v_email_id	:= v_email_id || email_val.email_id || ',';
		END LOOP;

		v_email_id	:= SUBSTR (v_email_id, 1, LENGTH (v_email_id) - 1);
		DBMS_OUTPUT.put_line ('Email Name is   ****' || v_email_id);
		RETURN v_email_id;
	--
	END get_cm_fch_grp_email_id;

	/*******************************************************
	 Purpose: This procedure fetch the hierarchy list
	*******************************************************/
	PROCEDURE gm_cm_fch_hierarchy_list (
		p_hierarchy_list   OUT	 TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_hierarchy_list
		 FOR
			 SELECT t9350.c9350_hierarchy_id ID, t9350.c9350_hierarchy_nm NAME
			   FROM t9350_hierarchy t9350
			  WHERE t9350.c9350_void_fl IS NULL;
	END gm_cm_fch_hierarchy_list;

	 /*******************************************************
	* Purpose: This procedure fetch the hierarchy list
	*******************************************************/
	PROCEDURE gm_cm_sav_hierarchyflow (
		p_hierarchyid	IN	 t9350_hierarchy.c9350_hierarchy_id%TYPE
	  , p_type			IN	 t9352_hierarchy_flow.c901_ref_type%TYPE
	  , p_ref_id		IN	 t9352_hierarchy_flow.c9352_ref_id%TYPE
	  , p_user_id		IN	 t9352_hierarchy_flow.c9352_ref_id%TYPE
	)
	AS
	BEGIN
		UPDATE t9352_hierarchy_flow
		   SET c9350_hierarchy_id = p_hierarchyid
			 , c9352_last_updated_by = p_user_id
			 , c9352_last_updated_date = SYSDATE
		 WHERE c901_ref_type = p_type AND c9352_ref_id = p_ref_id;

		IF (SQL%ROWCOUNT = 0)
		THEN
			INSERT INTO t9352_hierarchy_flow
						(c9352_hierarchy_flow_id, c9350_hierarchy_id, c901_ref_type, c9352_ref_id, c9352_created_by
					   , c9352_created_date
						)
				 VALUES (s9352_hierarchy_flow.NEXTVAL, p_hierarchyid, p_type, p_ref_id, p_user_id
					   , SYSDATE
						);
		END IF;
	END gm_cm_sav_hierarchyflow;
END gm_pkg_cm_hierarchy;
/
