create or replace PACKAGE BODY GM_PKG_BACK_ORDER_LOCK AS
 /**********************************************************************
  *Purpose: Procedure to call Back Orders  for current period
   Author: Andrews Stanley
  **********************************************************************/
  PROCEDURE gm_bo_lock_main 
  AS
      v_out_bo_lock_id NUMBER;
      v_date        DATE;
  BEGIN
  
     SELECT sysdate INTO v_date from dual;  
     GM_PKG_BACK_ORDER_LOCK.GM_LOCK_BACK_ORDER(v_date, v_out_bo_lock_id);
     DBMS_OUTPUT.PUT_LINE('Lock intiated');
     gm_pkg_back_order_lock.gm_lock_us_back_order_details(v_out_bo_lock_id);
     DBMS_OUTPUT.PUT_LINE('US Back Orders Locked');
     GM_PKG_BACK_ORDER_LOCK.gm_lock_ous_sales_back_order_details(v_out_bo_lock_id);
     DBMS_OUTPUT.PUT_LINE('OUS Back Orders Locked');
     gm_pkg_back_order_lock.gm_lock_ack_back_order_details(v_out_bo_lock_id);
     DBMS_OUTPUT.PUT_LINE('ACK Back Orders Locked');
     GM_PKG_BACK_ORDER_LOCK.gm_lock_loaner_item_back_order_details(v_out_bo_lock_id);
     DBMS_OUTPUT.PUT_LINE('Loaner Items Back Order Locked');
     gm_pkg_back_order_lock.gm_lock_dc_items_back_order_details(v_out_bo_lock_id);
     DBMS_OUTPUT.PUT_LINE('DC Item Back Orders Locked');
     GM_PKG_BACK_ORDER_LOCK.gm_lock_us_ifss_back_order_details(v_out_bo_lock_id);
     DBMS_OUTPUT.PUT_LINE('US IFSS Back Orders Locked');
     gm_pkg_back_order_lock.gm_lock_ous_ifss_back_order_details(v_out_bo_lock_id);
     DBMS_OUTPUT.PUT_LINE('OUS IFSS Back Orders Locked');
     gm_pkg_back_order_lock.gm_lock_us_item_back_order_details(v_out_bo_lock_id);
     DBMS_OUTPUT.PUT_LINE('US Items Back Orders Locked');
     DBMS_OUTPUT.PUT_LINE('All Done');
  END gm_bo_lock_main;

   /**********************************************************************
  *Purpose: Procedure to lock Back Order Lock for the current Period 
   Author: Andrews Stanley
  **********************************************************************/ 
  PROCEDURE gm_lock_back_order(
      p_date T2590_BACK_ORDER_LOCK.C2590_BACK_ORDER_LOCK_DATE%TYPE ,
      p_out_bo_lock_id OUT T2590_BACK_ORDER_LOCK.C2590_BACK_ORDER_LOCK_ID%TYPE ) AS
  BEGIN
     SELECT S2590_BACK_ORDER_LOCK.nextval INTO p_out_bo_lock_id FROM dual;
     
  INSERT
  INTO T2590_BACK_ORDER_LOCK
    (
      C2590_BACK_ORDER_LOCK_ID,
      C2590_BACK_ORDER_LOCK_DATE,
      C2590_LOCKED_BY
    )
    VALUES
    (
      p_out_bo_lock_id,
      TRUNC(SYSDATE),
      30301
     );
  END gm_lock_back_order;

/**********************************************************************
  *Purpose: Procedure to lock US Back Order Transactions
   Author: Andrews Stanley
  **********************************************************************/

  PROCEDURE gm_lock_us_back_order_details(
      p_bo_lock_id T2590_BACK_ORDER_LOCK.C2590_BACK_ORDER_LOCK_ID%TYPE) 
  AS
  BEGIN
    INSERT INTO T2591_BACK_ORDER_DETAILS
    (
    C2590_BACK_ORDER_LOCK_ID,
    C205_PART_NUMBER_ID,
    C2591_BACK_ORDER_TYPE,
    C2591_QTY
    )
   SELECT max(p_bo_lock_id), t502.c205_part_number_id num,Max(109420), SUM (t502.c502_item_qty) qty
    FROM t501_order t501, t502_item_order t502
    WHERE t501.c501_order_id = t502.c501_order_id
     AND t501.c501_delete_fl IS NULL
     AND t501.c501_void_fl IS NULL
     AND t501.c501_status_fl = 0
     AND t502.c502_void_fl IS NULL
     AND (t501.c1900_company_id = 1000 OR t501.c5040_plant_id = 1000)
     GROUP BY t502.c205_part_number_id;


  END gm_lock_us_back_order_details;

/**********************************************************************
  *Purpose: Procedure to lock OUS Sales Back Order Transactions
   Author: Andrews Stanley
  **********************************************************************/
  
  PROCEDURE gm_lock_ous_sales_back_order_details(
      p_bo_lock_id T2590_BACK_ORDER_LOCK.C2590_BACK_ORDER_LOCK_ID%TYPE) 
  AS
  BEGIN
    INSERT INTO T2591_BACK_ORDER_DETAILS
    (
    C2590_BACK_ORDER_LOCK_ID,
    C205_PART_NUMBER_ID,
    C2591_BACK_ORDER_TYPE,
    C2591_QTY
    )
    SELECT   max(p_bo_lock_id),num, max(109421), sum(qty)
    FROM (
    SELECT t520.c520_request_id ID, t520.c520_master_request_id master_id, '' aname
               , t521.c205_part_number_id num, t521.c521_qty qty, get_partnum_desc (t521.c205_part_number_id) dsc
            FROM t520_request t520
               , t521_request_detail t521
           WHERE t520.c520_request_id = t521.c520_request_id
             AND t520.c520_status_fl in(10,30)
             AND t520.c520_request_to IS NOT NULL
             AND t520.c520_void_fl IS NULL
             AND (t520.c1900_company_id = 1000 OR t520.c5040_plant_id = 1000)
             AND t520.c207_set_id IS NULL
             AND t520.C901_REQUEST_SOURCE  = 4000121
            --AND t520.c901_purpose = 4000097
             AND (   t520.c520_master_request_id IS NULL 
                  OR t520.c520_master_request_id IN (SELECT c520_request_id
                                                       FROM t520_request
                                                      WHERE c207_set_id IS NULL)
                 )
               AND
   (
    T520.C520_MASTER_REQUEST_ID  NOT LIKE  UNISTR('GM%')
    OR
    (
     T520.C520_MASTER_REQUEST_id  Is Null  
     AND
     T520.C520_REQUEST_id NOT LIKE  UNISTR('GM%')
    )
   )  
    )
    GROUP BY NUM
    ORDER BY num;

END gm_lock_ous_sales_back_order_details;

/**********************************************************************
  *Purpose: Procedure to lock ACK Back Order Transactions
   Author: Andrews Stanley
  **********************************************************************/
  PROCEDURE gm_lock_ack_back_order_details(
      p_bo_lock_id T2590_BACK_ORDER_LOCK.C2590_BACK_ORDER_LOCK_ID%TYPE) 
  AS
  BEGIN
    INSERT INTO T2591_BACK_ORDER_DETAILS
    (
    C2590_BACK_ORDER_LOCK_ID,
    C205_PART_NUMBER_ID,
    C2591_BACK_ORDER_TYPE,
    C2591_QTY
    )
   SELECT max(p_bo_lock_id), PNUM, Max(109422), sum(PENDQTY)
  FROM (SELECT pnum, get_partnum_desc (pnum) AS pnumdesc, accid, get_account_name (accid) AS accname, custpo, ordid
             , ordqty, get_released_qty (pnum, ordid, price) shipqty
             , (ordqty - get_released_qty (pnum, ordid, price)) pendqty
          FROM (SELECT   t502.c205_part_number_id pnum, t501.c704_account_id accid, t501.c501_customer_po custpo
                       , t501.c501_order_id ordid, SUM (t502.c502_item_qty) ordqty, t502.c502_item_price price
                       , t501.c501_order_date order_date
                    FROM t501_order t501, t502_item_order t502
                   WHERE t502.c501_order_id = t501.c501_order_id
                     AND t501.c901_order_type = '101260'
                     AND t501.c501_status_fl <> '3'
                     AND t501.c501_void_fl IS NULL
                     AND t502.c502_void_fl IS NULL
                     AND t501.c5040_plant_id = '3000'
                GROUP BY t502.c205_part_number_id
                       , t501.c704_account_id
                       , t501.c501_customer_po
                       , t501.c501_order_id
                       , t502.c502_item_price
                       , t501.c1900_company_id
                       , t501.c501_order_date))
                WHERE pendqty > 0
        group by pnum;
  END gm_lock_ack_back_order_details;

/**********************************************************************
  *Purpose: Procedure to lock Loaner Items Back Order Transactions
   Author: Andrews Stanley
  **********************************************************************/
  PROCEDURE gm_lock_loaner_item_back_order_details(
      p_bo_lock_id T2590_BACK_ORDER_LOCK.C2590_BACK_ORDER_LOCK_ID%TYPE) 
  AS
  BEGIN
    INSERT INTO T2591_BACK_ORDER_DETAILS
    (
    C2590_BACK_ORDER_LOCK_ID,
    C205_PART_NUMBER_ID,
    C2591_BACK_ORDER_TYPE,
    C2591_QTY
    )
     SELECT   MAX(p_bo_lock_id),t413.c205_part_number_id num, MAX(109423), SUM(t413.c413_item_qty) qty               
            FROM t412_inhouse_transactions t412, t413_inhouse_trans_items t413, t504a_loaner_transaction t504a
           WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
             AND t412.c504a_loaner_transaction_id = t504a.c504a_loaner_transaction_id(+)
             AND c412_status_fl = 0
             AND t412.c412_type = 100062
             AND t412.c5040_plant_id = 3000
             AND t412.c5040_plant_id = t504a.c5040_plant_id(+)
             AND t413.c413_void_fl IS NULL
             AND c412_void_fl IS NULL
        GROUP BY t413.c205_part_number_id;
  END gm_lock_loaner_item_back_order_details;
 
 /**********************************************************************
  *Purpose: Procedure to lock DC Items Back Order Transactions
   Author: Andrews Stanley
  **********************************************************************/ 
  PROCEDURE gm_lock_dc_items_back_order_details(
      p_bo_lock_id T2590_BACK_ORDER_LOCK.C2590_BACK_ORDER_LOCK_ID%TYPE) 
  AS
  BEGIN
    INSERT INTO T2591_BACK_ORDER_DETAILS
    (
    C2590_BACK_ORDER_LOCK_ID,
    C205_PART_NUMBER_ID,
    C2591_BACK_ORDER_TYPE,
    C2591_QTY
    )
    
        SELECT   max(p_bo_lock_id),t521.c205_part_number_id num, Max(109424),SUM (t521.c521_qty) qty
    FROM t520_request t520, t521_request_detail t521, t701_distributor t701
   WHERE t520.c520_request_id = t521.c520_request_id
     AND t520.c520_status_fl in ( 10,15) --10,'Back Order', 15,'Back Log'
     AND t520.c520_request_to IS NOT NULL
     AND t520.c520_void_fl IS NULL
     AND t520.c520_request_to = t701.c701_distributor_id
     AND (t520.c1900_company_id = 1000 OR t520.c5040_plant_id = 1000)
     AND T520.C901_SHIP_TO =  4120 -- Distributor
     AND T520.C520_REQUEST_FOR != 40022 --In-House Consignment
      AND T520.C520_REQUEST_TO in (
         select D_ID    
        FROM V700_TERRITORY_MAPPING_DETAIL V700
        WHERE DIVID  IN (100824)        )
 --    AND NVL (T701.C1900_COMPANY_ID, -999) != 1000
 --    AND t520.C901_REQUEST_SOURCE  != 4000121 --OUS Sales Restock (ETL)
     AND t520.c207_set_id IS NULL
    AND (t520.c520_master_request_id IS NULL OR t520.c520_master_request_id IN (SELECT c520_request_id
                                                                                   FROM t520_request
                                                                                  WHERE c207_set_id IS NULL))
                                                                                             AND
   (
    T520.C520_MASTER_REQUEST_ID   LIKE  UNISTR('GM%')
    OR
    (
     T520.C520_MASTER_REQUEST_id  Is Null
     AND
     T520.C520_REQUEST_id  LIKE  UNISTR('GM%')
    )
   ) 
   group by t521.c205_part_number_id;
END gm_lock_dc_items_back_order_details;
  
  /**********************************************************************
  *Purpose: Procedure to lock US IFSS Back Order Transactions
   Author: Andrews Stanley
  **********************************************************************/
  PROCEDURE gm_lock_us_ifss_back_order_details(
      p_bo_lock_id T2590_BACK_ORDER_LOCK.C2590_BACK_ORDER_LOCK_ID%TYPE) 
  AS
  BEGIN
    INSERT INTO T2591_BACK_ORDER_DETAILS
    (
    C2590_BACK_ORDER_LOCK_ID,
    C205_PART_NUMBER_ID,
    C2591_BACK_ORDER_TYPE,
    C2591_QTY
    )
   SELECT   MAX(p_bo_lock_id),t521.c205_part_number_id num, max(109425),SUM(t521.c521_qty) qty
    FROM t520_request t520
       , t521_request_detail t521
       , t701_distributor t701
     WHERE t520.c520_request_id = t521.c520_request_id
     AND t520.c520_status_fl = 10
     AND t520.c520_request_to IS NOT NULL
     AND t520.c520_request_to =  t701.c701_distributor_id
     AND t520.c520_void_fl IS NULL
     AND NVL (t701.c1900_company_id, -999) = 1000
	 AND t701.c901_distributor_type !=  70103
     AND (t520.c1900_company_id = 1000 OR t520.c5040_plant_id = 1000)
     AND t520.c520_master_request_id IN (SELECT c520_request_id
                                           FROM t520_request
                                          WHERE c520_status_fl = 40 AND c207_set_id IS NOT NULL)
    GROUP BY t521.c205_part_number_id;
  END gm_lock_us_ifss_back_order_details;
  
  /**********************************************************************
  *Purpose: Procedure to lock OUS IFSS Back Order Transactions
   Author: Andrews Stanley
  **********************************************************************/
  PROCEDURE gm_lock_ous_ifss_back_order_details(
      p_bo_lock_id T2590_BACK_ORDER_LOCK.C2590_BACK_ORDER_LOCK_ID%TYPE) 
  AS
  BEGIN
    INSERT INTO T2591_BACK_ORDER_DETAILS
    (
    C2590_BACK_ORDER_LOCK_ID,
    C205_PART_NUMBER_ID,
    C2591_BACK_ORDER_TYPE,
    C2591_QTY
    )
   SELECT   MAX(p_bo_lock_id),t521.c205_part_number_id num, Max(109426),SUM(t521.c521_qty) qty
    FROM t520_request t520
       , t521_request_detail t521
       , t701_distributor t701
     WHERE t520.c520_request_id = t521.c520_request_id
     AND t520.c520_status_fl = 10
     AND t520.c520_request_to IS NOT NULL
     AND t520.c520_request_to =  t701.c701_distributor_id
     AND t520.c520_void_fl IS NULL
     AND NVL (t701.c1900_company_id, -999) != 1000
     AND (t520.c1900_company_id = 1000 OR t520.c5040_plant_id = 1000)
     AND t520.c520_master_request_id IN (SELECT c520_request_id
                                           FROM t520_request
                                          WHERE c520_status_fl = 40 AND c207_set_id IS NOT NULL)
    GROUP BY t521.c205_part_number_id; 
  END gm_lock_ous_ifss_back_order_details;
  
  /**********************************************************************
  *Purpose: Procedure to lock US Item Back Order Transactions
   Author: Andrews Stanley
  **********************************************************************/
  PROCEDURE gm_lock_us_item_back_order_details(
      p_bo_lock_id T2590_BACK_ORDER_LOCK.C2590_BACK_ORDER_LOCK_ID%TYPE) 
  AS
  BEGIN
    INSERT INTO T2591_BACK_ORDER_DETAILS
    (
    C2590_BACK_ORDER_LOCK_ID,
    C205_PART_NUMBER_ID,
    C2591_BACK_ORDER_TYPE,
    C2591_QTY
    )
    SELECT   MAX(p_bo_lock_id),t521.c205_part_number_id num, max(109427),SUM (t521.c521_qty) qty
    FROM t520_request t520, t521_request_detail t521, t701_distributor t701
   WHERE t520.c520_request_id = t521.c520_request_id
     AND t520.c520_status_fl = 10
     AND t520.c520_request_to IS NOT NULL
     AND t520.c520_void_fl IS NULL
     AND t520.c520_request_to = t701.c701_distributor_id
     AND (t520.c1900_company_id = 1000 OR t520.c5040_plant_id = 1000)
     AND NVL (t701.c1900_company_id, -999) = 1000
     AND t520.c207_set_id IS NULL
     AND (t520.c520_master_request_id IS NULL OR t520.c520_master_request_id IN (SELECT c520_request_id
                                                                                   FROM t520_request
                                                                                  WHERE c207_set_id IS NULL))
     GROUP BY t521.c205_part_number_id;
  END gm_lock_us_item_back_order_details;
 
    
END GM_PKG_BACK_ORDER_LOCK;