CREATE OR REPLACE PACKAGE BODY gm_pkg_op_process_po_edc
IS
	PROCEDURE gm_sav_process_po_edc
	AS
		v_po_id 	   t401_purchase_order.c401_purchase_ord_id%TYPE;
		v_vendor_id    t301_vendor.c301_vendor_id%TYPE;
		v_comments	   VARCHAR2 (500);
		v_po_type	   t401_purchase_order.c401_type%TYPE := 3100;
		v_packslip	   t408_dhr.c408_packing_slip%TYPE;
		v_inhouse_trans_type VARCHAR2 (100) := '100065';
		v_user_id	   NUMBER := 30301;
		v_set_id	   t208_set_details.c207_set_id%TYPE;
		v_distributor_id VARCHAR2 (100);
		v_new_req_id   t520_request.c520_request_id%TYPE;
		v_new_consign_id t504_consignment.c504_consignment_id%TYPE;
		v_request_status t520_request.c520_status_fl%TYPE;
		v_source_consign_id etl_consignment_data.c504_consignment_id%TYPE;
		v_dateformat   VARCHAR2 (10);
		v_etl_ld_status_id NUMBER;

		CURSOR cur_source_consign
		IS
			/*SELECT DISTINCT c504_consignment_id source_cons_id
					   FROM etl_consignment_data
					  WHERE c504_consignment_id IS NOT NULL
						AND c207_consignment_set_id IS NOT NULL;
			*/
			SELECT DISTINCT c504_consignment_id source_cons_id
					   FROM etl_consignment_data etl,
					   etl_cons_ld_status stat
					  WHERE etl.c504_consignment_id IS NOT NULL
						AND etl.c207_consignment_set_id IS NOT NULL
						AND etl.c504_consignment_id = stat.c504_cons_id_src(+)
						AND stat.lD_COMPLETE IS NULL;
	BEGIN
		FOR source_consign IN cur_source_consign
		LOOP
			v_source_consign_id := source_consign.source_cons_id;

			SELECT set_id
			  INTO v_set_id
			  FROM (SELECT c207_consignment_set_id set_id
					  FROM etl_consignment_data
					 WHERE c504_consignment_id = v_source_consign_id AND ROWNUM = 1);

			SELECT setl_cons_ld_status.NEXTVAL
			  INTO v_etl_ld_status_id
			  FROM DUAL;

			INSERT INTO etl_cons_ld_status
						(etl_ld_status_id, ld_start_time, c504_cons_id_src, c207_cons_set_id, ld_complete
						)
				 VALUES (v_etl_ld_status_id, SYSDATE, v_source_consign_id, v_set_id, 'N'
						);

			create_po_wo (v_source_consign_id, v_po_type, v_user_id, v_po_id, v_vendor_id);
			--DBMS_OUTPUT.put_line (' v_vendor_id ' || v_vendor_id || 'PO ID' || v_po_id || ' SCID' || v_source_consign_id);
			gm_load_dhr_for_edc (v_source_consign_id
							   , v_po_id
							   , v_vendor_id
							   , v_comments
							   , v_packslip
							   , v_po_type
							   , v_inhouse_trans_type
							   , v_user_id
								);
			v_dateformat := get_rule_value ('DATEFMT', 'DATEFORMAT');
			gm_pkg_op_process_request.gm_sav_initiate_set (''
														 , TO_CHAR (TRUNC (SYSDATE), v_dateformat)
														 , '50618'
														 , ''
														 , v_set_id
														 , 40021
														 , v_distributor_id
														 , 50626
														 , v_user_id
														 , '4123'
														 , '50625'
														 , ''
														 , 15
														 , v_user_id
														 , '50061'
														 , null  -- Planned_ship_date
														 , v_new_req_id
														 , v_new_consign_id
														  );
			DBMS_OUTPUT.put_line (' v_new_req_id ' || v_new_req_id || 'v_new_consign_id' || v_new_consign_id);
			gm_upd_control_number (v_source_consign_id, v_new_consign_id, v_set_id);

			UPDATE t520_request
			   SET c520_status_fl = 20
			   , C520_LAST_UPDATED_BY = v_user_id
			   , C520_LAST_UPDATED_DATE = sysdate
			 WHERE c520_request_id = v_new_req_id;


			UPDATE t504_consignment
			   SET c504_status_fl = 2
				 , c504_verify_fl = 1
				 , c504_verified_date = SYSDATE
				 , c504_verified_by = v_user_id
				 , c504_last_updated_by = v_user_id
				 , c504_last_updated_date = SYSDATE
				 , c504_comments = v_source_consign_id	 -- comments
				 , c504_update_inv_fl = 1
			 WHERE c504_consignment_id = v_new_consign_id;

			gm_update_bulk_qty(v_new_consign_id,4302,4311); -- To Reduce quantity from Bulk.

			UPDATE etl_cons_ld_status
			   SET ld_complete = 'Y'
				 , c504_cons_id_tgt = v_new_consign_id
				 , ld_complete_date = SYSDATE
			 WHERE etl_ld_status_id = v_etl_ld_status_id;
			 
			 /* -- Commenting this as the UI is fixed MNTTASK-5030
			 -- Void BO Requests created during ETL
			 UPDATE t520_request 
			 	SET c520_void_fl='Y',
			 		c520_delete_fl='Y',
			 		C520_LAST_UPDATED_BY = v_user_id,
			 		C520_LAST_UPDATED_DATE = SYSDATE 
			 	WHERE 
			 		c520_master_request_id = v_new_req_id  and c520_status_fl = 10; 
			*/
		END LOOP;
	END gm_sav_process_po_edc;

	PROCEDURE create_po_wo (
		p_source_consign_id   IN	   VARCHAR2
	  , p_po_type			  IN	   t401_purchase_order.c401_type%TYPE
	  , p_user_id			  IN	   NUMBER
	  , p_po_id 			  OUT	   VARCHAR2
	  , p_vendor_id 		  OUT	   t301_vendor.c301_vendor_id%TYPE
	)
	AS
		v_wo_id 	   NUMBER;
		v_string	   VARCHAR2 (20);
		v_id_string    VARCHAR2 (20);
		v_id		   NUMBER;
		v_cost_price   NUMBER;
--ADDED BY RAJESH
		v_exclude_pnum etl_consignment_data.c205_part_number_id%TYPE;
		v_exclude_qty  etl_consignment_data.c505_item_qty%TYPE;
		v_po_id 	   t401_purchase_order.c401_purchase_ord_id%TYPE;
		v_msg		   VARCHAR2 (100);
		v_exclude_pkey etl_consignment_data.etl_primary_key%TYPE;

		CURSOR cur_etl_cons_data
		IS
			SELECT	 c205_part_number_id pnum, SUM (c505_item_qty) qty
				FROM etl_consignment_data
			   WHERE etl_primary_key NOT IN (v_exclude_pkey) AND c504_consignment_id = p_source_consign_id
			GROUP BY c205_part_number_id;
	BEGIN
		IF p_source_consign_id IS NULL
		THEN
			GM_RAISE_APPLICATION_ERROR('-20999','259','');
		END IF;

		BEGIN
			SELECT pnum, qty, etl_pkey
			  INTO v_exclude_pnum, v_exclude_qty, v_exclude_pkey
			  FROM (SELECT c205_part_number_id pnum, c505_item_qty qty, etl_primary_key etl_pkey
					  FROM etl_consignment_data
					 WHERE c504_consignment_id = p_source_consign_id AND ROWNUM = 1) temp;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				v_exclude_pnum := '';
				v_exclude_qty := 0;
				GM_RAISE_APPLICATION_ERROR('-20999','260','');
		END;

		IF (v_exclude_qty > 0 AND v_exclude_pnum IS NOT NULL)
		THEN
			BEGIN
				SELECT c301_vendor_id
				  INTO p_vendor_id
				  FROM t301_vendor
				 WHERE c301_vendor_name = 'Globus Audubon';
			EXCEPTION
				WHEN NO_DATA_FOUND
				THEN
					p_vendor_id := 8;
			END;

			--DBMS_OUTPUT.put_line (v_exclude_pnum || 'vid' || p_vendor_id);
			BEGIN
				SELECT c405_cost_price * v_exclude_qty
				  INTO v_cost_price
				  FROM t405_vendor_pricing_details
				 WHERE c205_part_number_id = v_exclude_pnum AND c405_active_fl = 'Y' AND c301_vendor_id = p_vendor_id;
			EXCEPTION WHEN NO_DATA_FOUND
			THEN
				v_cost_price := 0;
			END ;
		--For MNTTASK-5199   For creating the PO (like 'COUNTRYCODE-PO-XXXX'),Since this body file is executed manually only in EDC, hot coding the country code as 'en'.
		--  Part of Global Harmonization,remove it
			gm_place_po (p_vendor_id, v_cost_price, p_user_id, p_po_type, p_po_id, v_msg); 
			--DBMS_OUTPUT.put_line (' New PO' || p_po_id);
			gm_create_work_order (p_po_id, p_vendor_id, v_exclude_pnum, v_exclude_qty, p_user_id);
		END IF;

		FOR etl_cons_data IN cur_etl_cons_data
		LOOP
			--DBMS_OUTPUT.put_line (' etl_cons_data.pnum' || etl_cons_data.pnum||' etl_cons_data.qty'||etl_cons_data.qty);
			gm_create_work_order (p_po_id, p_vendor_id, etl_cons_data.pnum, etl_cons_data.qty, p_user_id);
		END LOOP;
	END create_po_wo;

	PROCEDURE gm_load_dhr_for_edc (
		p_source_consign_id    IN	VARCHAR2
	  , p_po_id 			   IN	t401_purchase_order.c401_purchase_ord_id%TYPE
	  , p_vendid			   IN	t301_vendor.c301_vendor_id%TYPE
	  , p_comments			   IN	t408_dhr.c408_comments%TYPE
	  , p_packslip			   IN	t408_dhr.c408_packing_slip%TYPE
	  , p_po_type			   IN	NUMBER
	  , p_inhouse_trans_type   IN	VARCHAR2
	  , p_userid			   IN	t408_dhr.c408_created_by%TYPE
	)
	AS
		p_str		   VARCHAR2 (4000);
		p_recdate	   VARCHAR2 (20);
		p_manfdate	   VARCHAR2 (20);
		p_pdt_id	   VARCHAR2 (20);
		p_message	   VARCHAR2 (4000);
		v_wo_id 	   t402_work_order.c402_work_order_id%TYPE;
		v_qty		   NUMBER;
		p_inputstr	   VARCHAR2 (4000);
		v_pnum		   VARCHAR2 (20);
		v_trans_id	   VARCHAR2 (20);
		v_dhr_wo_cnt   NUMBER;
		v_wo_uom_str   VARCHAR2 (100);
		v_wo_cnt	   NUMBER;
		v_wo_uom_qty   NUMBER;
		v_cnt		   NUMBER;

		CURSOR pop_val
		IS
			SELECT c205_part_number_id pnum, c505_control_number cntrl, c505_item_qty qty
			  FROM etl_consignment_data
			 WHERE c504_consignment_id = p_source_consign_id;
	BEGIN
		SELECT TO_CHAR (SYSDATE,get_rule_value('DATEFMT','DATEFORMAT'))
		  INTO p_recdate
		  FROM DUAL;
		
		-- Do not remove the hardcoded format.
		SELECT TO_CHAR (SYSDATE,'MM/DD/YYYY')
		  INTO p_manfdate
		  FROM DUAL;

		FOR rad_val IN pop_val
		LOOP
			v_pnum		:= rad_val.pnum;
			v_cnt		:= v_cnt + 1;

			--DBMS_OUTPUT.put_line ('v_pnum' || v_pnum || ' CNT ' || v_cnt || 'po' || p_po_id);
			SELECT c402_work_order_id
			  INTO v_wo_id
			  FROM t402_work_order
			 WHERE c205_part_number_id = v_pnum AND c402_void_fl IS NULL AND c401_purchase_ord_id = p_po_id;

			--DBMS_OUTPUT.put_line ('PNUM' || rad_val.pnum || ' WO ' || v_wo_id );

			-- Create the DHR
			gm_create_dhr (v_wo_id
						 , p_vendid
						 , rad_val.pnum
						 , rad_val.cntrl
						 , rad_val.qty
						 , p_manfdate  -- MANFDATE
						 , p_packslip
						 , p_recdate
						 , p_source_consign_id	 -- Comments field.
						 , p_po_type
						 , p_userid
						 , p_pdt_id
						 , p_message
						  );
			p_inputstr	:= p_inhouse_trans_type || ',' || TO_CHAR (rad_val.qty) || '|';
			-- Create the In House Transaction
			gm_pkg_op_dhr.gm_op_sav_ihtxns (p_pdt_id, p_inputstr, rad_val.pnum, rad_val.cntrl, p_userid, v_qty);

			-- Verify the DHR
			UPDATE t408_dhr
			   SET c408_verified_by = p_userid
				 , c408_verified_ts = SYSDATE
				 , c408_qty_on_shelf = rad_val.qty
				 , c408_status_fl = '4'
				 , c408_last_updated_by = p_userid
				 , c408_last_updated_date = SYSDATE
			 WHERE c408_dhr_id = p_pdt_id;

			-- Verify the Inhouse Transaction
			SELECT c412_inhouse_trans_id
			  INTO v_trans_id
			  FROM t412_inhouse_transactions t412
			 WHERE t412.c412_ref_id = p_pdt_id AND t412.c412_void_fl IS NULL;

			UPDATE t412_inhouse_transactions
			   SET c412_update_inv_fl = '1'
				 , c412_verified_by = p_userid
				 , c412_verified_date = SYSDATE
				 , c412_verify_fl = '1'
				 , c412_status_fl = '4'
				 , c412_last_updated_date = SYSDATE
				 , c412_last_updated_by = p_userid
			 WHERE c412_inhouse_trans_id = v_trans_id;

			-- To Update the part Number Table and do the posting
			gm_pkg_op_inventory_qty.gm_sav_inventory_main (v_trans_id, p_inhouse_trans_type, p_userid);

			-- Close Work Order
			UPDATE t402_work_order t402
			   SET c402_status_fl = 3
				 , c402_completed_date = TRUNC (SYSDATE)
				 , c402_last_updated_by = p_userid
				 , c402_last_updated_date = SYSDATE
			 WHERE c402_work_order_id = v_wo_id;
		END LOOP;
	END gm_load_dhr_for_edc;

	PROCEDURE gm_create_work_order (
		p_po_id 	  IN   t401_purchase_order.c401_purchase_ord_id%TYPE
	  , p_vendor_id   IN   t301_vendor.c301_vendor_id%TYPE
	  , p_part_num	  IN   t205_part_number.c205_part_number_id%TYPE
	  , p_quantity	  IN   NUMBER
	  , p_userid	  IN   t408_dhr.c408_created_by%TYPE
	)
	AS
		v_wo_id 	   t402_work_order.c402_work_order_id%TYPE;
		v_id_string    VARCHAR2 (100);
		v_pricing_id   t405_vendor_pricing_details.c405_pricing_id%TYPE;
		v_cost_price   t405_vendor_pricing_details.c405_cost_price%TYPE;
	BEGIN
		SELECT s402_work.NEXTVAL
		  INTO v_wo_id
		  FROM DUAL;

		--
		IF LENGTH (v_wo_id) = 1
		THEN
			v_id_string := '0' || v_wo_id;
		ELSE
			v_id_string := v_wo_id;
		END IF;

		--
		SELECT 'GM-WO-' || v_id_string
		  INTO v_wo_id
		  FROM DUAL;

		SELECT c405_pricing_id, c405_cost_price
		  INTO v_pricing_id, v_cost_price
		  FROM t405_vendor_pricing_details
		 WHERE c205_part_number_id = p_part_num AND c301_vendor_id = p_vendor_id AND c405_active_fl = 'Y';

		UPDATE t402_work_order
		   SET c402_qty_ordered = c402_qty_ordered + p_quantity
		 WHERE c401_purchase_ord_id = p_po_id AND c205_part_number_id = p_part_num AND c301_vendor_id = p_vendor_id;

		IF (SQL%ROWCOUNT = 0)
		THEN
			INSERT INTO t402_work_order
						(c402_work_order_id, c401_purchase_ord_id, c205_part_number_id, c301_vendor_id
					   , c402_created_date, c402_status_fl, c402_cost_price, c402_created_by, c402_last_updated_date
					   , c402_last_updated_by, c402_qty_ordered, c402_rev_num, c402_critical_fl
					   , c402_sub_component_fl, c402_sterilization_fl, c408_dhr_id, c402_far_fl, c402_void_fl
					   , c405_pricing_id, c402_completed_date, c402_qty_history_fl, c402_price_history_fl
					   , c402_split_percentage, c402_posting_fl, c402_rc_fl, c402_split_cost, c402_rm_inv_fl
						)
				 VALUES (v_wo_id, p_po_id, p_part_num, p_vendor_id
					   , SYSDATE, '1', v_cost_price, p_userid, NULL
					   , NULL, p_quantity, 'A', NULL
					   , NULL, NULL, NULL, NULL, NULL
					   , v_pricing_id, NULL, NULL, NULL
					   , NULL, NULL, NULL, NULL, NULL
						);
		END IF;
	END gm_create_work_order;

	PROCEDURE gm_upd_control_number (
		p_source_consign_id   IN   t505_item_consignment.c504_consignment_id%TYPE
	  , p_new_consign_id	  IN   t505_item_consignment.c504_consignment_id%TYPE
	  , p_set_id			  IN   etl_consignment_data.c207_consignment_set_id%TYPE
	)
	AS
		CURSOR cur_src_cns_item
		IS
			SELECT c205_part_number_id pnum, c505_control_number cntrl, c505_item_qty qty, c5010_tag_id tag_id
			  FROM etl_consignment_data
			 WHERE c504_consignment_id = p_source_consign_id;
	BEGIN
		DELETE FROM t505_item_consignment
			  WHERE c504_consignment_id = p_new_consign_id;

		FOR src_cns_item IN cur_src_cns_item
		LOOP
			INSERT INTO t505_item_consignment
						(c505_item_consignment_id, c504_consignment_id, c205_part_number_id, c505_control_number
					   , c505_item_qty, c505_item_price
						)
				 VALUES (s504_consign_item.NEXTVAL, p_new_consign_id, src_cns_item.pnum, src_cns_item.cntrl
					   , src_cns_item.qty, get_part_price (src_cns_item.pnum, 'C')
						);

			IF (src_cns_item.tag_id IS NOT NULL)
			THEN
				INSERT INTO t5010_tag
							(c5010_tag_id, c5010_control_number, c5010_last_updated_trans_id, c5010_location_id
						   , c5010_history_fl, c5010_created_by, c5010_created_date, c205_part_number_id
						   , c901_trans_type, c207_set_id, c901_status
							)
					 VALUES (src_cns_item.tag_id, src_cns_item.cntrl, p_new_consign_id, 52098
						   , 'Y', '30301', SYSDATE, src_cns_item.pnum
						   , 51000, p_set_id, 51012
							);
			END IF;
		END LOOP;
					
		--When ever the detail is updated, the master table has to be updated ,so ETL can Sync it to GOP.
		  UPDATE t504_consignment
             SET c504_last_updated_by = '30301'
	             ,c504_last_updated_date = SYSDATE
		  WHERE c504_consignment_id = p_new_consign_id;
	END gm_upd_control_number;
END gm_pkg_op_process_po_edc; 
/

