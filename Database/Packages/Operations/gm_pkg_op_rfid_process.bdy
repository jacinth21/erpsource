/* Formatted on 2011/04/28 10:36 (Formatter Plus v4.8.0) */
--@"c:\database\packages\operations\gm_pkg_op_rfid_process.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_rfid_process
IS

 /***********************************************************************
  * Description : Procedure to save the RFID JSON string in master table
  * Author   	: karthik
  ***********************************************************************/
PROCEDURE gm_sav_batch_details
  (
    p_input_string  IN CLOB,
	p_user_id       IN T9660_RFID_BATCH.C9660_RFID_BATCH_CREATED_BY%TYPE,
	p_rfid_batch_id OUT T9660_RFID_BATCH.C9660_RFID_BATCH_ID%TYPE
  )
AS
v_rfid_seq 				T9660_RFID_BATCH.C9660_RFID_BATCH_ID%TYPE;
v_seq_num  				NUMBER;
v_batch_response_data	T9660_RFID_BATCH.C9660_RFID_BATCH_RESPONSE_DATA%TYPE;
v_company_id			T9660_RFID_BATCH.C1900_COMPANY_ID%TYPE;
v_plant_id				T9660_RFID_BATCH.C5040_PLANT_ID%TYPE;

BEGIN
	SELECT 'GM'|| TO_CHAR((TRUNC(CURRENT_DATE, 'DD')),'yyyymmdd') || '-'
			INTO v_rfid_seq
		FROM dual;
		
	SELECT COUNT(C9660_RFID_BATCH_ID) +1
			INTO v_seq_num
		FROM T9660_RFID_BATCH
	WHERE C9660_RFID_BATCH_ID LIKE '%'||v_rfid_seq||'%';
	
	v_rfid_seq:= v_rfid_seq||v_seq_num;
	
	
	  INSERT
	INTO T9660_RFID_BATCH
	  (
	    C9660_RFID_BATCH_ID,
	    C9660_RFID_BATCH_DATA,
	    C9660_RFID_BATCH_RESPONSE_DATA,
	    C9660_RFID_BATCH_CREATED_DATE,
	    C9660_RFID_BATCH_VOID_FL,
	    C9660_RFID_BATCH_CREATED_BY,
	    C9660_RFID_BATCH_LAST_UPDATED_BY,
	    C9660_RFID_BATCH_LAST_UPDATED_DATE,
	    C1900_COMPANY_ID,
	    c5040_plant_id
	  )
	  VALUES
	  (
	  v_rfid_seq,
	  p_input_string,
	  v_batch_response_data,
	  CURRENT_DATE,
	  null,
	  p_user_id,
	  p_user_id,
	  CURRENT_DATE,
	  v_company_id,
	  v_plant_id
	  );
	  
	  p_rfid_batch_id := v_rfid_seq;

END gm_sav_batch_details;

/**************************************************************************************
  * Description : Procedure to save the RFID tag Details in the RFID tag details table
  * Author   	: karthik
  **************************************************************************************/
PROCEDURE gm_sav_tag_details
  (
    p_rfid_batch_id  IN T9660_RFID_BATCH.C9660_RFID_BATCH_ID%TYPE,
	p_user_id        IN T9660_RFID_BATCH.C9660_RFID_BATCH_CREATED_BY%TYPE
   )
   AS
   v_json_data  			CLOB;
   v_json 					json_object_t;
   v_batchid				T9660_RFID_BATCH.C9660_RFID_BATCH_ID%TYPE;
   v_globus_batchid			T9660_RFID_BATCH.C9660_RFID_BATCH_ID%TYPE;
   v_batch_created			T9660_RFID_BATCH.C9660_RFID_BATCH_ID%TYPE;
   v_batch_response_date	T9660_RFID_BATCH.C9660_RFID_BATCH_ID%TYPE;
   v_batch_statusid			T9660_RFID_BATCH.C9660_RFID_BATCH_ID%TYPE;
   v_batch_status			t9661_rfid_batch_tag_detail.C901_STATUS%TYPE;
   v_tagids					CLOB;
   v_substring      		VARCHAR2 (4000) ;
   v_tag_id					T5010_TAG.C5010_TAG_ID%TYPE;
   BEGIN
	   
	 BEGIN
		SELECT c9660_rfid_batch_data
				INTO v_json_data
			FROM T9660_RFID_BATCH
			WHERE C9660_RFID_BATCH_ID     = p_rfid_batch_id
			AND c9660_rfid_batch_void_fl IS NULL ;
	   END;
   
	  v_json := json_object_t.parse(v_json_data);
	  v_batchid := v_json.get_String('batchid');
      --v_globus_batchid := v_json.get_String('globusbatchid');
      --v_batch_created := v_json.get_String('batchcreated');
      --v_batch_response_date := v_json.get_String('batchresponsedate');
      --v_batch_statusid := v_json.get_String('batchstatusid');
      --v_batch_status := v_json.get_String('batchstatus');
      v_tagids := v_json.get_String('tagids');
      v_tagids:= v_tagids || ',';
      
	      IF v_tagids  IS NOT NULL THEN
	        WHILE INSTR (v_tagids, ',')         <> 0
	        LOOP
	            --
	            v_substring := SUBSTR (v_tagids, 1, INSTR (v_tagids, ',') - 1) ;
	            v_tagids    := SUBSTR (v_tagids, INSTR (v_tagids, ',')    + 1) ;
	            --
	            v_tag_id    := NULL;
	            --
	            v_tag_id    := REGEXP_REPLACE(v_substring, '[^0-9A-Za-z]', '');
	            
	            /*save the rfid tag and batch details*/
	            gm_sav_rfid_batch_tag_details(p_rfid_batch_id,v_tag_id,v_batch_status,p_user_id);
	            
	           END LOOP;
	         END IF;
	         
	         	/*validate the tag id and update the status*/
	         	gm_validate_tag_id(p_rfid_batch_id,p_user_id);
	         	
	         	/*save the loaner status*/
	         	gm_sav_tag_loaner_status(p_rfid_batch_id,p_user_id);
   END gm_sav_tag_details;
   
/***********************************************************************
  * Description : Procedure to save the RFID batch tag details
  * Author   	: karthik
  ***********************************************************************/
PROCEDURE gm_sav_rfid_batch_tag_details
  (
    p_rfid_batch_id  IN T9661_RFID_BATCH_TAG_DETAIL.C9660_RFID_BATCH_ID%TYPE,
    p_tag_id		 IN T9661_RFID_BATCH_TAG_DETAIL.C5010_TAG_ID%TYPE,
    p_status		 IN T9661_RFID_BATCH_TAG_DETAIL.C901_STATUS%TYPE,
	p_user_id        IN T9661_RFID_BATCH_TAG_DETAIL.C9661_CREATED_BY%TYPE
  )
AS
BEGIN

	UPDATE T9661_RFID_BATCH_TAG_DETAIL
	SET c901_status           = p_status,
	  c9661_last_update_by    = p_user_id,
	  c9661_last_updated_date = CURRENT_DATE
	WHERE c9660_rfid_batch_id = p_rfid_batch_id
	AND c5010_tag_id          = p_tag_id
	AND c9661_void_fl        IS NULL;

	IF (SQL%ROWCOUNT = 0) THEN
		INSERT
		INTO T9661_RFID_BATCH_TAG_DETAIL
		  (
		    C9661_RFID_BATCH_DETAIL_ID,
		    C9660_RFID_BATCH_ID,
		    C5010_TAG_ID,
		    C901_STATUS,
		    C9661_COMMENTS,
		    C9661_VOID_FL,
		    C9661_CREATED_BY,
		    C9661_CREATED_DATE,
		    C9661_LAST_UPDATE_BY,
		    C9661_LAST_UPDATED_DATE
		  )
		  VALUES
		  (
		  S9661_RFID_BATCH_TAG_DETAIL.NEXTVAL,
		  p_rfid_batch_id,
		  p_tag_id,
		  NULL,
		  NULL,
		  NULL,
		  p_user_id,
		  CURRENT_DATE,
		  p_user_id,
		  CURRENT_DATE
		  );
		 END IF;
	  
END gm_sav_rfid_batch_tag_details;

/***********************************************************************
  * Description : Procedure to validate the tag id available for the RFID
  * Author   	: karthik
  ***********************************************************************/
PROCEDURE gm_validate_tag_id
  (
    p_rfid_batch_id  IN T9660_RFID_BATCH.C9660_RFID_BATCH_ID%TYPE,
	p_user_id        IN T9660_RFID_BATCH.C9660_RFID_BATCH_CREATED_BY%TYPE
  )
AS
 v_tag_id			T5010_TAG.C5010_TAG_ID%TYPE;
 v_count			NUMBER;
 --v_txn_id			t5010_tag.c5010_last_updated_trans_id%TYPE;
   
CURSOR v_batch_tag_details
	IS 
	SELECT c5010_tag_id tagid
		FROM T9661_RFID_BATCH_TAG_DETAIL
		WHERE c9660_rfid_batch_id = p_rfid_batch_id
		AND c9661_void_fl        IS NULL;
	
BEGIN

	 FOR v_index IN v_batch_tag_details
			LOOP
			v_tag_id 		   := v_index.tagid;
			
			--check the tag in T5010 table,if the tag is not available then  update the tag status to 30004
	              SELECT count(1)
	              		INTO v_count
	              	FROM t5010_tag 
	              	WHERE c5010_tag_id = v_tag_id
	              	AND c5010_void_fl IS NULL ;
	              	
	              	IF v_count = 0 THEN
	              		 gm_sav_rfid_batch_tag_details(p_rfid_batch_id,v_tag_id,30004,p_user_id);
	              	ELSE	              	
			--if the tag is valid and the status of the tag is pending return then update the tag status to 30006
			--if the tag is valid and the status of the tag is not in pending return then update the tag status to 30007
						SELECT count(1)
	              				INTO v_count
							FROM t5010_tag t5010,
							  t504a_consignment_loaner t504a
							WHERE t5010.c5010_tag_id  = v_tag_id
							AND t5010.c5010_last_updated_trans_id= t504a.c504_consignment_id
							AND t5010.c5010_void_fl  IS NULL 
							AND t504a.c504a_status_fl = 20; -- Pending Return
					
						IF v_count >0 THEN
	              		 	gm_sav_rfid_batch_tag_details(p_rfid_batch_id,v_tag_id,30006,p_user_id);
	              		 ELSE 
	              		 	gm_sav_rfid_batch_tag_details(p_rfid_batch_id,v_tag_id,30007,p_user_id);
	              		 	  --if the tag is valid and the status of the tag is not in pending return and the tag is already updated in previous batch
							  -- then update the tag status to 30008
								SELECT count(1)
			              				INTO v_count
									FROM T9661_RFID_BATCH_TAG_DETAIL 
									WHERE C901_STATUS = '30006' 
									AND c5010_tag_id = v_tag_id
									AND TRUNC(c9661_created_date) = TRUNC(SYSDATE)
									AND C9661_VOID_FL IS NULL;								
									
										IF v_count > 0 THEN
											gm_sav_rfid_batch_tag_details(p_rfid_batch_id,v_tag_id,30008,p_user_id);
										END IF;
	              		END IF;
	              	END IF;
			END LOOP;
	  
END gm_validate_tag_id;

/***********************************************************************
  * Description : Procedure to save the RFID tag loaner status 
  * Author   	: karthik
  ***********************************************************************/
PROCEDURE gm_sav_tag_loaner_status
  (
    p_rfid_batch_id  IN T9660_RFID_BATCH.C9660_RFID_BATCH_ID%TYPE,
	p_user_id        IN T9660_RFID_BATCH.C9660_RFID_BATCH_CREATED_BY%TYPE
  )
AS
BEGIN  
		  /*
		   * Updating loaner status from pending return to pending acceptance for 
		   * tags which received through RFID reader.
		   *It will not change the status from pending return to pending acceptance for
		   *the tags which has loaner date as today
		   */
		  UPDATE t504a_consignment_loaner
			SET c504a_status_fl        = 24, -- Pending Acceptance
			  c504a_last_updated_by    = p_user_id,
			  c504a_last_updated_date  = CURRENT_DATE
			WHERE c504_consignment_id IN
			  (SELECT t504a.c504_consignment_id
			  FROM t5010_tag t5010,
			    t504a_consignment_loaner t504a,
			    t9661_rfid_batch_tag_detail t9661
			  WHERE t5010.c5010_tag_id = t9661.c5010_tag_id
			  AND c9660_rfid_batch_id = p_rfid_batch_id
			  AND t9661.c901_status                = 30006 --Valid tag
			  AND t9661.c9661_void_fl             IS NULL
			  AND t5010.c5010_last_updated_trans_id= t504a.c504_consignment_id
			  AND t5010.c5010_void_fl             IS NULL
			  AND t504a.c504a_status_fl            = 20 -- Pending Return
			  GROUP BY t504a.c504_consignment_id
			  )
			AND c504a_void_fl          IS NULL
			AND TRUNC(c504a_loaner_dt) <> TRUNC(SYSDATE) ;

	  
END gm_sav_tag_loaner_status;

PROCEDURE gm_fch_loaner_set_accept_rpt
  (
  	p_loan_type 	IN	VARCHAR2,
  	p_start_dt		IN 	VARCHAR2,
  	p_end_dt		IN	VARCHAR2,
  	p_commission_fl IN  VARCHAR2,
    p_loaner_set	 OUT   TYPES.cursor_type
   )
   AS
   v_company_id t1900_company.c1900_company_id%TYPE;
   v_plant_id	T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
   v_start_dt	t504a_loaner_status_log.C504A_FROM_STATUS_DATE%TYPE;
   v_end_dt		t504a_loaner_status_log.C504A_TO_STATUS_DATE%TYPE;
   v_commssion_fl t504a_loaner_status_log.c504a_commission_fl%TYPE;
   BEGIN
   select get_compid_frm_cntx(),get_plantid_frm_cntx() into v_company_id,v_plant_id from dual;
   v_start_dt := to_date(p_start_dt,'mm/dd/yyyy');
   v_end_dt := to_date(p_end_dt,'mm/dd/yyyy');
		OPEN p_loaner_set
		FOR
			
			SELECT t504a.c504_consignment_id consignid, 
 t504a.c5010_tag_id tagid, 
 decode(t504a.c504a_commission_fl,'Y','YES','NO') comstat, 
 SUM(decode(t504a.c504a_from_status,24,1,0)) acccnt, 
 SUM(decode(t504a.c504a_from_status,20,1,0)) naccCnt 
 FROM t504a_loaner_status_log t504a, t504_consignment t504 
 WHERE t504a.C504A_VOID_FL IS NULL 
 and t504.C504_VOID_FL IS NULL 
 and t504a.c504_consignment_id = t504.c504_consignment_id 
 and t504.c504_type = p_loan_type 
 and trunc(t504a.C504A_TO_STATUS_DATE) >= trunc(to_date(p_start_dt,'mm/dd/yyyy')) 
 and trunc(t504a.C504A_TO_STATUS_DATE) <= trunc(to_date(p_end_dt,'mm/dd/yyyy')) 
 AND NVL(t504a.c504a_commission_fl,'N') = NVL(p_commission_fl,'N') 
 AND t504a.c5040_plant_id = v_plant_id 
 GROUP BY t504a.c504_consignment_id, 
 t504a.C5010_TAG_ID, 
 t504a.c504a_commission_fl 
 ORDER BY consignid;
   
   END gm_fch_loaner_set_accept_rpt;


   /***********************************************************************************************
  * Description : Procedure to rollback loaner status to pending return from pending acceptance 
  * Author   	: APrasath
  *************************************************************************************************/
PROCEDURE gm_rollback_pending_acceptance
  (
    p_txn_id  IN t504a_consignment_loaner.c504_consignment_id%TYPE,
	p_user_id        IN t504a_consignment_loaner.c504a_last_updated_by%TYPE
  )
AS
BEGIN  
		  UPDATE t504a_consignment_loaner 
			SET c504a_status_fl = '20', --Pending Return 
			  c504a_last_updated_by = p_user_id, 
			  c504a_last_updated_date = sysdate 
			WHERE c504_consignment_id = p_txn_id 
			AND c504a_status_fl = '24'; --Pending Acceptance
				  
END gm_rollback_pending_acceptance;
END gm_pkg_op_rfid_process;
/