/*
 * Formatted on 2011/08/23 11:49 (Formatter Plus v4.8.0)
 * @"C:\Database\Packages\Operations\gm_pkg_op_dhr.bdy";
 */

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_dhr
IS
--
   PROCEDURE gm_op_sav_rc
   AS
      /********************************************************************
         * Description : Procedure to create RC for raw material and posting
         * Author       : Xun
         ********************************************************************/
      v_msg       VARCHAR2 (1000);
      v_rc_id     VARCHAR2 (20);
      v_post_id   VARCHAR2 (20);
      v_price     NUMBER (15, 2);

      --
      CURSOR cur_wos
      IS
         SELECT t401.c301_vendor_id vendorid, t402.c205_part_number_id pnum,
                t402.c402_work_order_id woid, t402.c402_qty_ordered qty,
                NVL (t402.c402_rm_inv_fl, 'N') rm_invoice_fl,
                t402.c402_split_percentage split_pert,
                t402.c402_split_cost split_cost,
                t401.c1900_company_id comp_id,
                t401.c1900_source_company_id source_comp_id
           FROM t401_purchase_order t401, t402_work_order t402
          WHERE t401.c401_purchase_ord_id = t402.c401_purchase_ord_id
            AND t401.c401_void_fl IS NULL
            AND t402.c402_void_fl IS NULL
            AND TRUNC (t401.c401_purchase_ord_date) <=
                     TRUNC (SYSDATE)
                   - get_rule_value ('Sec199_Jobs', 'Sec199_Rule')        --14
            AND t402.c402_posting_fl IS NULL
            AND t402.c402_split_cost IS NOT NULL
            AND t402.c402_qty_ordered > 0;
--
   BEGIN
      --new part 100.000 raw material cost
      FOR var_wos IN cur_wos
      LOOP
         IF (var_wos.rm_invoice_fl = 'Y')
         THEN
            --create RC
            gm_create_dhr (var_wos.woid,
                           var_wos.vendorid,
                           '100.000',                          --var_wos.pnum,
                           'NOC#',
                           var_wos.qty,
                           TO_CHAR (SYSDATE, get_rule_value('DATEFMT','DATEFORMAT')),
                           var_wos.pnum,
                           TO_CHAR (SYSDATE, get_rule_value('DATEFMT','DATEFORMAT')),
                           'Invoice Receipt for raw material',
                           'RC',
                           30301,
                           v_rc_id,
                           v_msg
                          );

            IF (v_rc_id IS NOT NULL)
            THEN
               UPDATE t408_dhr t408
                  SET t408.c408_verified_by = 30301,
                      t408.c408_verified_ts = SYSDATE
                WHERE t408.c408_dhr_id = v_rc_id;

               UPDATE t402_work_order t402
                  SET t402.c402_rc_fl = 'Y'
                WHERE t402.c402_work_order_id = var_wos.woid;
            END IF;
-- v_rc_id, v_msg are outputs
-- v_WO,p_VendId,v_PNum,v_Control,v_Qty,v_ManfDate,p_PackSlip,p_RecDate,p_Comments,p_Type,p_UserId,v_pdt_id,v_msg
         END IF;
        -- to get the price
		v_price := get_currency_conversion (get_vendor_currency (var_wos.vendorid), get_comp_curr (var_wos.comp_id), SYSDATE,  NVL (var_wos.split_cost, 0));
		--
         gm_save_ledger_posting
             (48122,                                              --v_post_id,
              CURRENT_DATE,
              '100.000',                                       --var_wos.pnum,
              var_wos.vendorid,
              var_wos.woid,
              var_wos.qty,
              v_price,
              30301,
              var_wos.source_comp_id,
              NVL(var_wos.source_comp_id, var_wos.comp_id),
              v_price
             );

         UPDATE t402_work_order t402
            SET t402.c402_posting_fl = 'Y'
          WHERE t402.c402_work_order_id = var_wos.woid;
      END LOOP;
   END gm_op_sav_rc;

--

   /********************************************************************
     * Description : Procedure to create RC for raw material and posting
                 when receive extra parts
     * Author     : Xun
     ******************************************************************/
   PROCEDURE gm_op_sav_extra_rc (
      p_dhrid        IN   t408_dhr.c408_dhr_id%TYPE,
      p_partnum      IN   t408_dhr.c205_part_number_id%TYPE,
      p_woid         IN   t408_dhr.c402_work_order_id%TYPE,
      p_qty          IN   NUMBER,
      p_transby      IN   NUMBER,
      p_partyid      IN   t301_vendor.c301_vendor_id%TYPE,
      p_split_cost   IN   NUMBER
   )
   AS
      v_qty_verify    NUMBER;
      v_qty_ordered   NUMBER;
      v_diff          NUMBER;
      v_rm_inv_fl     VARCHAR2 (1);
      v_msg           VARCHAR2 (1000);
      v_rc_id         VARCHAR2 (20);
      v_company_id    t1900_company.c1900_company_id%TYPE;
      v_source_company_id    t1900_company.c1900_company_id%TYPE;
      v_po_id	t401_purchase_order.c401_purchase_ord_id%TYPE;
      v_price     NUMBER (15, 2);
   BEGIN
      SELECT NVL (SUM (t408.c408_qty_received), 0)
        INTO v_qty_verify
        FROM t408_dhr t408, t402_work_order t402
       WHERE t408.c402_work_order_id = p_woid
         AND t408.c402_work_order_id = t402.c402_work_order_id
         AND t408.c901_type IS NULL;

      SELECT t402.c402_qty_ordered, NVL (c402_rm_inv_fl, 'N'), c401_purchase_ord_id
        INTO v_qty_ordered, v_rm_inv_fl, v_po_id
        FROM t402_work_order t402
       WHERE t402.c402_work_order_id = p_woid;
	--
	 SELECT c1900_company_id, c1900_source_company_id
	 	INTO v_company_id, v_source_company_id
	   FROM t401_purchase_order
	  WHERE c401_purchase_ord_id = v_po_id;
  	--
      v_diff := v_qty_verify - v_qty_ordered;

      IF (v_diff > 0)
      THEN
         IF (v_diff > p_qty)
         THEN
            v_diff := p_qty;
         END IF;

         IF (v_rm_inv_fl = 'Y')
         THEN
            --create RC
            gm_create_dhr (p_woid,
                           p_partyid,
                           '100.000',                          --var_wos.pnum,
                           'NOC#',
                           v_diff,
                           TO_CHAR (SYSDATE, get_rule_value('DATEFMT','DATEFORMAT')),
                           p_partnum,                              --pack slip
                           TO_CHAR (SYSDATE, get_rule_value('DATEFMT','DATEFORMAT')),
                           'Invoice Receipt for raw material',
                           'RC',
                           30301,
                           v_rc_id,
                           v_msg
                          );

            IF (v_rc_id IS NOT NULL)
            THEN
               UPDATE t408_dhr t408
                  SET t408.c408_verified_by = 30301,
                      t408.c408_verified_ts = SYSDATE
                WHERE t408.c408_dhr_id = v_rc_id;
            END IF;
         END IF;
		--
		v_price := get_currency_conversion (get_vendor_currency (p_partyid), get_comp_curr (v_company_id), CURRENT_DATE,  NVL (p_split_cost, 0));
		--
         gm_save_ledger_posting
                    (48122,                                       --v_post_id,
                     CURRENT_DATE,
                     '100.000',                                --var_wos.pnum,
                     p_partyid,
                     p_woid,
                     v_diff,
                     v_price,
                     30301,
                     v_source_company_id,
                     NVL(v_source_company_id, v_company_id),
                     v_price
                    );
      END IF;
   END gm_op_sav_extra_rc;

   /********************************************************************
    * Description : Procedure to Void Manf Dhrs/WIP during MA creation for physical Audit

    * Author     : Mihir
    ******************************************************************/
   PROCEDURE gm_op_void_mfg_dhrs (
      p_dhrid     IN   t408_dhr.c408_dhr_id%TYPE,
      p_user_id   IN   t408_dhr.c408_last_updated_by%TYPE
   )
   AS
      CURSOR mreq_cur
      IS
         SELECT c303_material_request_id mreqid
           FROM t303_material_request t303
          WHERE t303.c408_dhr_id = p_dhrid AND t303.c303_delete_fl IS NULL;

      CURSOR mreq_det_cur (
         p_reqid   IN   t303_material_request.c303_material_request_id%TYPE
      )
      IS
         SELECT c304_material_request_item_id mitemid
           FROM t304_material_request_item
          WHERE c303_material_request_id = p_reqid AND c304_delete_fl IS NULL;

      v_cnt       NUMBER;
      v_ncmr_id   t409_ncmr.c409_ncmr_id%TYPE;
   BEGIN
      BEGIN
         SELECT c409_ncmr_id
           INTO v_ncmr_id
           FROM t409_ncmr t409
          WHERE t409.c408_dhr_id = p_dhrid
            AND t409.c409_void_fl IS NULL
            AND t409.c409_status_fl < 3
            AND c409_void_fl IS NULL;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            v_ncmr_id := NULL;
      END;

      IF (v_ncmr_id IS NOT NULL)
      THEN
         UPDATE t409_ncmr
            SET c409_last_updated_by = p_user_id,
                c409_last_updated_date = SYSDATE,
                c409_void_fl = 'Y'
          WHERE c409_ncmr_id = v_ncmr_id AND c409_void_fl IS NULL;
      END IF;

      FOR mreq IN mreq_cur
      LOOP
         UPDATE t303_material_request
            SET c303_delete_fl = 'Y',
                c303_last_updated_date = SYSDATE,
                c303_last_updated_by = p_user_id
          WHERE c303_material_request_id = mreq.mreqid
            AND c303_delete_fl IS NULL;

         FOR mreqdet IN mreq_det_cur (mreq.mreqid)
         LOOP
            UPDATE t304_material_request_item
               SET c304_delete_fl = 'Y'
             WHERE c304_material_request_item_id = mreqdet.mitemid
               AND c304_delete_fl IS NULL;
         END LOOP;
      END LOOP;

      SELECT COUNT (1)
        INTO v_cnt
        FROM t408_dhr
       WHERE c408_dhr_id = p_dhrid AND c408_void_fl IS NULL;

      IF (v_cnt <> 0)
      THEN
         UPDATE t408_dhr t408
            SET t408.c408_last_updated_by = p_user_id,
                t408.c408_void_fl = 'Y',
                t408.c408_last_updated_date = SYSDATE
          WHERE t408.c408_dhr_id = p_dhrid AND t408.c408_void_fl IS NULL;
      END IF;
   END gm_op_void_mfg_dhrs;

   /********************************************************
    * To Create Inhouse transactions while DHR is processed
    * Author : VPrasath
    ********************************************************/
   PROCEDURE gm_op_sav_ihtxns (
      p_dhrid       IN       t408_dhr.c408_dhr_id%TYPE,
      p_input_str   IN       VARCHAR2,
      p_pnum        IN       t205_part_number.c205_part_number_id%TYPE,
      p_cnum        IN       VARCHAR2,
      p_userid      IN       t412_inhouse_transactions.c412_last_updated_by%TYPE,
      p_total_qty   OUT      NUMBER
   )
   AS
      v_string          VARCHAR2 (4000)                        := p_input_str;
      v_substring       VARCHAR2 (4000);
      v_txn_type        VARCHAR2 (20);
      v_qty             VARCHAR2 (20);
      v_txn_id          VARCHAR2 (20);
      v_input_str       VARCHAR2 (200);
      v_total_qty       NUMBER                                    := 0;
      v_message         VARCHAR2 (2000);
      v_reason          t901_code_lookup.c901_code_id%TYPE;
      v_partyid         t401_purchase_order.c301_vendor_id%TYPE;
      v_wo_uom_str      VARCHAR2 (100);
      v_amt             t402_work_order.c402_cost_price%TYPE;
      v_uomqty          NUMBER;
      v_price           NUMBER (15, 2);
      v_comments        VARCHAR2 (4000);
      v_received_date   DATE;
      v_po_type         VARCHAR2 (20);
      vprodclass        VARCHAR2 (20);
      --
      v_trans_company_id NUMBER;
      
   BEGIN
      SELECT b.c301_vendor_id, get_wo_uom_type_qty (a.c402_work_order_id),
             NVL (DECODE (b.c401_type, 3104, c.c205_cost, a.c402_cost_price),
                  a.c402_cost_price
                 ),
             c408_received_date--Removed created date because received date was used in currency conversion, the difference in date will result in different rates, hence diff. amount.
             ,get_potype_from_woid(t408.C402_Work_Order_Id),c.c205_product_class -- Get the PO type and part number product class
             , b.c1900_company_id
        INTO v_partyid, v_wo_uom_str,
             v_amt,
             v_received_date,v_po_type,vprodclass
             , v_trans_company_id
        FROM t402_work_order a,
             t401_purchase_order b,
             t205_part_number c,
             t408_dhr t408
       WHERE t408.c408_dhr_id = p_dhrid
         AND a.c402_work_order_id = t408.c402_work_order_id
         AND a.c401_purchase_ord_id = b.c401_purchase_ord_id
         AND a.c205_part_number_id = c.c205_part_number_id 
         AND a.C402_VOID_FL is null and b.C401_VOID_FL is null and c.C205_ACTIVE_FL = 'Y' and t408.C408_VOID_FL is null;

      v_uomqty :=
         TO_NUMBER (NVL (SUBSTR (v_wo_uom_str, INSTR (v_wo_uom_str, '/') + 1),
                         1
                        )
                   );
       
   		--Added the currency conversion function because the removed code and currency function fetched different values for conversion due to the difference in the removed SQL and function
   	  v_price := (get_currency_conversion (get_vendor_currency (v_partyid), get_comp_curr(v_trans_company_id), v_received_date, v_amt) / v_uomqty);
      WHILE INSTR (v_string, '|') <> 0
      LOOP
         v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
         v_string := SUBSTR (v_string, INSTR (v_string, '|') + 1);
         v_txn_type := NULL;
         v_qty := 0;
         v_txn_type := SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
         v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
         v_qty :=
                 NVL (SUBSTR (v_substring, INSTR (v_substring, ',') + 1),
                      '0');
         v_input_str :=
             p_pnum || ',' || ROUND(v_qty,2) || ',' || p_cnum || ',' || v_price || '|';
         -- If the part is non- sterile,PO type is reowrk,cnum is NOC# then the transaction should not create DHFG,DHBL.   
        IF vprodclass = '4031' AND v_po_type='3101' AND UPPER(p_cnum)='NOC#' AND (v_txn_type = '100065' OR v_txn_type = '100067')  THEN
          GM_RAISE_APPLICATION_ERROR('-20999','196','');
        END IF;
       
        SELECT get_next_consign_id (v_txn_type)
           INTO v_txn_id
           FROM DUAL;

         IF v_txn_type = '100070'
         THEN
          WITH v_txns AS (
                 SELECT get_party_name(t204.c101_party_id) trans_id FROM t204_project_team t204 WHERE t204.c901_role_id = '92044'
                 AND t204.c202_project_id IN (SELECT c202_project_id FROM t205_part_number WHERE c205_part_number_id = p_pnum)
           )
           SELECT MAX (SYS_CONNECT_BY_PATH (trans_id, ', ')) INTO v_comments
             FROM
               (SELECT trans_id, ROW_NUMBER () OVER (ORDER BY trans_id) AS curr
                  FROM v_txns)
             CONNECT BY curr - 1 = PRIOR curr
            START WITH curr = 1;
         v_comments := SUBSTR (v_comments, 2);
         ELSE
            v_comments := NULL;
         END IF;

         -- Create the InHouse Transaction
         gm_ls_upd_inloanercsg (v_txn_id,                    -- transaction id
                                v_txn_type,                -- transaction type
                                41278,                       -- from DHR Split
                                NULL,              -- release to is not needed
                                NULL,           -- release to id is not needed
                                v_comments,    -- final comments is not needed
                                p_userid,                           -- user id
                                v_input_str,                   -- Input String
                                p_dhrid,                             -- ref id
                                'PlaceOrder',
                                v_message
                               );
         -- Release the InHouse Transaction for Verification
         gm_ls_upd_inloanercsg (v_txn_id,                    -- transaction id
                                v_txn_type,                -- transaction type
                                41278,                       -- from DHR Split
                                NULL,              -- release to is not needed
                                NULL,           -- release to id is not needed
                                NULL,          -- final comments is not needed
                                p_userid,                           -- user id
                                v_input_str,                   -- Input String
                                p_dhrid,                             -- ref id
                                'ReleaseControl',
                                v_message
                               );
         v_total_qty := v_total_qty + v_qty;
      END LOOP;

      UPDATE t412_inhouse_transactions
         SET c412_status_fl = '0',
             c412_last_updated_date = SYSDATE,
             c412_last_updated_by = p_userid
       WHERE c412_ref_id = p_dhrid;

      p_total_qty := v_total_qty;
   END gm_op_sav_ihtxns;

   /********************************************************
    * To Save the posting for DHR
    * Author : VPrasath
    ********************************************************/
   PROCEDURE gm_op_sav_dhr_posting (
      p_dhrid     IN   t408_dhr.c408_dhr_id%TYPE,
      p_qty       IN   t408_dhr.c408_qty_received%TYPE,
      p_post_id   IN   VARCHAR2,
      p_userid    IN   t412_inhouse_transactions.c412_last_updated_by%TYPE
   )
   AS
      v_qty_to_use      NUMBER;
      v_chkdhrvoidfl    t408_dhr.c408_void_fl%TYPE;
      v_chkstatusfl     NUMBER;
      v_control         t408_dhr.c408_control_number%TYPE;
      v_qtyrec          t408_dhr.c408_qty_received%TYPE;
      v_pnum            t408_dhr.c205_part_number_id%TYPE;
      v_workordid       t408_dhr.c402_work_order_id%TYPE;
      v_partyid         t401_purchase_order.c301_vendor_id%TYPE;
      v_wo_uom_str      VARCHAR2 (100);
      v_amt             t402_work_order.c402_cost_price%TYPE;
      v_uomqty          NUMBER;
      v_received_date   DATE;
      v_price           NUMBER (15, 2);
      v_closeout_qty	t409_ncmr.c409_closeout_qty%TYPE;
      --
      v_owner_company  NUMBER;
      v_trans_company_id NUMBER;
      v_new_trans_company_id NUMBER;
      v_local_cost NUMBER (15,2);
      v_trans_plant_id NUMBER;
      v_vendor_std_cost t302_vendor_standard_cost.c302_standard_cost%TYPE;
      v_vendor_cost_type t301_vendor.c901_vendor_cost_type%TYPE;
      v_po_type   t401_purchase_order.c401_type%TYPE;
      --
      v_post_id NUMBER;
   BEGIN
	  v_post_id :=  p_post_id;
      -- Getting the closeout qty of the NCMR if it is available
	   BEGIN
		   SELECT C409_CLOSEOUT_QTY INTO v_closeout_qty
		   		FROM T409_NCMR 
		   WHERE C408_DHR_ID = p_dhrid
		   AND C409_VOID_FL IS NULL;
		   EXCEPTION
		   WHEN NO_DATA_FOUND
		   THEN
		   		v_closeout_qty := NULL;
	   END;
	   
	   SELECT t408.c408_void_fl, TO_NUMBER (t408.c408_status_fl),
             t408.c408_control_number,
               NVL (t408.c408_qty_received, 0)
             - NVL (T408.C408_QTY_REJECTED, 0)
             + NVL(v_closeout_qty, NVL(t408.c408_qty_rejected,0)) qty,-- if the NCMR is voided, then need to include the rejected qty also for posting
             T408.C205_PART_NUMBER_ID, T408.C402_WORK_ORDER_ID,
             C408_RECEIVED_DATE
             INTO v_chkdhrvoidfl, v_chkstatusfl,
             	v_control,
             	v_qtyrec,
             	v_pnum, v_workordid,
             	v_received_date
        FROM T408_DHR T408
       WHERE T408.C408_DHR_ID = p_dhrid;

      /* If Qty to post is passed from out side, then use that qty to post
       * otherwise, use the DHR Received qty
       */
      IF p_qty IS NULL
      THEN
         v_qty_to_use := v_qtyrec;
      ELSE
         v_qty_to_use := p_qty;
      END IF;

      -- Fetching Values for Postings
      SELECT b.c301_vendor_id, get_wo_uom_type_qty (c402_work_order_id),
             NVL (DECODE (b.c401_type, 3104, c.c205_cost, a.c402_cost_price),
                  a.c402_cost_price
                 ), b.c1900_source_company_id, b.c1900_company_id,
                 NVL (DECODE (b.c401_type, 3104, c.c205_cost, a.c402_cost_price),
                  a.c402_cost_price
                 ), b.c5040_plant_id, b.c401_type
                 , gm_pkg_pur_vendor_cost.get_vendor_cost_type(b.c301_vendor_id)
        INTO v_partyid, v_wo_uom_str,
             v_amt, v_owner_company, v_trans_company_id,
             v_local_cost, v_trans_plant_id, v_po_type,
             v_vendor_cost_type
        FROM t402_work_order a, t401_purchase_order b, t205_part_number c
       WHERE c402_work_order_id = v_workordid
         AND a.c401_purchase_ord_id = b.c401_purchase_ord_id
         AND a.c205_part_number_id = c.c205_part_number_id;

      v_uomqty :=
         TO_NUMBER (NVL (SUBSTR (v_wo_uom_str, INSTR (v_wo_uom_str, '/') + 1),
                         1
                        )
                   );
        -- Only Regular PO - get the Standard Cost
        --106561 Standard Cost
        IF NVL(v_vendor_cost_type, '-999') = '106561' AND v_po_type = 3100
        THEN
        	SELECT gm_pkg_pur_vendor_cost.get_vendor_std_cost (v_partyid, v_pnum)
        		INTO v_vendor_std_cost
        	FROM DUAL;
        	--
   			v_amt := v_vendor_std_cost;
   			-- getting the posting id (Standard Cost) - code changed for this PMT-20749
   			-- Based on regular posting id - to get the standard cost posting id from rules table 
   			SELECT NVL (get_rule_value (v_post_id, 'STD_COST_POSTING'), v_post_id)
			   INTO v_post_id
			   FROM dual;
   		END IF;	-- end vendor cost type

      -- to convert the transaction company currency format
      v_price :=(get_currency_conversion (get_vendor_currency (v_partyid), get_comp_curr (v_trans_company_id), v_received_date, v_amt) / v_uomqty);
      --
      gm_save_ledger_posting
                  (v_post_id,
                   CURRENT_DATE,
                   v_pnum,
                   v_partyid,
                   p_dhrid,
                   v_qty_to_use,
                   v_price,
                   p_userid,
                   v_owner_company,
                   NVL(v_owner_company, v_trans_company_id),
                   v_price
                  );
   END gm_op_sav_dhr_posting;

   PROCEDURE gm_fch_split_locations (
      p_dhr_id   IN       t412_inhouse_transactions.c412_ref_id%TYPE,
      p_grp      IN       t901_code_lookup.c901_code_grp%TYPE,
      p_out      OUT      TYPES.cursor_type
   )
   AS
   BEGIN
      OPEN p_out
       FOR
          SELECT   c902_code_nm_alt codeid, t901.c901_code_nm codenm,
                   t412.qty qty, trans_id,
                   get_code_name (trans_type) trans_type
              FROM (SELECT *
                      FROM t901_code_lookup t901
                     WHERE t901.c901_code_grp = p_grp AND c901_active_fl = 1) t901,
                   (SELECT   t412.c412_inhouse_trans_id trans_id,
                             t412.c412_type trans_type,
                             SUM (t413.c413_item_qty) qty
                        FROM t412_inhouse_transactions t412,
                             t413_inhouse_trans_items t413
                       WHERE c412_ref_id = p_dhr_id
                         AND t412.c412_inhouse_trans_id =
                                                    t413.c412_inhouse_trans_id
                         AND t412.c412_void_fl IS NULL
                         AND t413.c413_void_fl IS NULL
                    GROUP BY t412.c412_inhouse_trans_id, t412.c412_type) t412
             WHERE t901.c902_code_nm_alt = trans_type(+)
          ORDER BY t901.c901_code_seq_no;
   END gm_fch_split_locations;

   /********************************************************
    * To fecth the records for Contol# validation
    * Author : dreddy
    ********************************************************/
   PROCEDURE gm_control_validation (
      p_part_id      IN       t408_dhr.c205_part_number_id%TYPE,
      p_control_id   IN       t408_dhr.c408_control_number%TYPE,
      p_recordset    OUT      TYPES.cursor_type,
      p_out          OUT      VARCHAR2
   )
   AS
      v_count   NUMBER;
   BEGIN
      --   
      gm_pkg_common.gm_chk_cntrl_num_exists(p_part_id,p_control_id,p_out);  --Able to recognize the lot number from the lot number override screen
      IF p_out IS NOT NULL
      THEN
         p_out := 'Does not exists';
      END IF;

      OPEN p_recordset
       FOR
          SELECT p_part_id pnum, p_control_id cnum,
                 get_partnum_desc (p_part_id) partdesc
            FROM DUAL;
   END gm_control_validation;
   
    /*****************************************************************
    * Description: This function will get di number available flag
    * Author: Bala
    *****************************************************************/
	FUNCTION get_di_no_from_vendor(
		p_pnum	IN	T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE
	) RETURN VARCHAR
	IS
		v_di_avil VARCHAR2(2) := 'N';
		v_count   NUMBER;
	BEGIN
		BEGIN
			 SELECT count(c205d_attribute_value) INTO v_count
		       FROM t205d_part_attribute
		      WHERE c205_part_number_id = p_pnum 
                AND c901_attribute_type = '4000517' -- Vendor Design 
                AND c205d_attribute_value = '103420' -- Yes
                AND c205d_void_fl is NULL;
          EXCEPTION WHEN NO_DATA_FOUND 
          THEN
          	v_di_avil := 'N';
          END;
          
          IF v_count > 0 THEN
          	v_di_avil := 'Y';
          END IF;       
          RETURN v_di_avil;          
	END get_di_no_from_vendor;

	/********************************************************
    * To Save DI Number for the DHR ID
    * Author : Bala
    ********************************************************/
   PROCEDURE gm_save_di_no (
     p_dhr_id	   IN	t408_dhr.c408_dhr_id %TYPE,
     p_di_num      IN   t408_dhr.c408_udi_no%TYPE,
     p_user_id     IN 	t408_dhr.c408_last_updated_by%TYPE
   )AS
   BEGIN
	   UPDATE t408_dhr 
	      SET c408_udi_no = p_di_num
	        , c408_last_updated_by = p_user_id
	        , c408_last_updated_date = SYSDATE 
	    WHERE c408_dhr_id = p_dhr_id 
	      AND c408_void_fl IS NULL;	   
   END gm_save_di_no;

  
    /********************************************************************
    * Description : This Procedure save donor number in master table
    * Author       : Xun
    ********************************************************************/
   PROCEDURE gm_op_sav_donor_number (
    p_donor_number       IN   t2540_donor_master.c2540_donor_number%TYPE
  , p_vendor_id	         IN   t2540_donor_master.c301_vendor_id%TYPE
  , p_user				 IN   t2540_donor_master.c2540_last_updated_by%TYPE
  , p_donor_id           OUT  t2540_donor_master.c2540_donor_id%TYPE 
	)
	AS
		v_count	    NUMBER;
		v_donor_id  t2540_donor_master.c2540_donor_id%TYPE;
		v_company_id         T1900_COMPANY.C1900_COMPANY_ID%TYPE;
	BEGIN
		
		SELECT get_compid_frm_cntx() INTO v_company_id FROM DUAL;
	    --
		    SELECT  count(1) 
				INTO v_count
			FROM t2540_donor_master 
			WHERE c301_vendor_id = p_vendor_id  
			AND c2540_donor_number = p_donor_number
			AND c1900_company_id = v_company_id
			AND c2540_void_fl IS NULL;
		--	
			BEGIN 
				SELECT   c2540_donor_id
					INTO  v_donor_id
				FROM t2540_donor_master 
				WHERE c301_vendor_id = p_vendor_id  
				AND c2540_donor_number = p_donor_number
				AND c1900_company_id = v_company_id
				AND c2540_void_fl IS NULL; 
				EXCEPTION
			        WHEN NO_DATA_FOUND THEN
			            v_donor_id := '';
			END;
		--	
			IF v_count > 0 THEN
	    		p_donor_id := v_donor_id;
	    		
	    	ELSE
				--Get the Donor ID and pass it to the common insert procedure
				SELECT s2540_donor.NEXTVAL
					INTO p_donor_id 
				FROM DUAL;
				
		        gm_pkg_op_dhr.gm_op_sav_donor_master(p_donor_number,p_vendor_id,NULL,NULL,NULL,NULL,p_user,p_donor_id);
	   		END IF;
	END gm_op_sav_donor_number;   
   
   
    /********************************************************************
    * Description : This Procedure save donor detail in master table
    * Author       : Xun
    ********************************************************************/
   PROCEDURE gm_op_sav_donor_master (
    p_donor_number       IN		t2540_donor_master.c2540_donor_number%TYPE
  , p_vendor_id	         IN   	t2540_donor_master.c301_vendor_id%TYPE
  , p_age                IN   	t2540_donor_master.c2540_age%TYPE
  , p_sex				 IN   	t2540_donor_master.c901_sex%TYPE
  , p_international_use  IN   	t2540_donor_master.c901_international_use%TYPE
  , p_research			 IN	  	t2540_donor_master.c901_research%TYPE
  , p_user				 IN   	t2540_donor_master.c2540_last_updated_by%TYPE
  , p_donor_id			 IN   	t2540_donor_master.c2540_donor_id%TYPE DEFAULT NULL
	)
	AS
	v_company_id         T1900_COMPANY.C1900_COMPANY_ID%TYPE;
	BEGIN
		-- If the donor number is not there, then no need to save donor parameter values
		IF p_donor_number IS NULL OR p_donor_number = ''
		THEN
			RETURN;
		END IF;
		
		SELECT get_compid_frm_cntx() INTO v_company_id FROM DUAL;
	    --
	    UPDATE t2540_donor_master
	       SET c2540_age = NVL(p_age,c2540_age)
	         , c901_sex = NVL(p_sex,c901_sex)
	         , c901_international_use = NVL(p_international_use,c901_international_use)
	         , c901_research = NVL(p_research,c901_research)
		 	 , c2540_last_updated_by = p_user
		 	 , c2540_last_updated_date = CURRENT_DATE
		 WHERE NVL(c301_vendor_id,'-9999') = NVL(p_vendor_id,'-9999')
		 AND c2540_donor_number = p_donor_number
		 AND c1900_company_id = v_company_id
		 AND c2540_void_fl IS NULL; 
		 
	    IF (SQL%ROWCOUNT = 0)
	    THEN 
	    
	        INSERT INTO t2540_donor_master
	                    (c2540_donor_id, c2540_donor_number, c2540_age, c901_sex, c301_vendor_id 
	                   , c901_international_use, c901_research, c2540_last_updated_by, c2540_last_updated_date , c1900_company_id
	                    )
	             VALUES (NVL(p_donor_id,s2540_donor.NEXTVAL), p_donor_number, p_age, p_sex
	                   , p_vendor_id, p_international_use, p_research, p_user, CURRENT_DATE , v_company_id
	                    );
	    END IF;
	END gm_op_sav_donor_master;


 /********************************************************************
  * Description : This Procedure save donor info and dhr control number
  * Author       : Xun
  ********************************************************************/
	PROCEDURE gm_op_sav_donor_info(
		p_dhr_id			IN	    t408_dhr.c408_dhr_id%TYPE,
		p_part_num			IN		t408a_dhr_control_number.c205_part_number_id%TYPE,
		p_donor_number		IN		t2540_donor_master.c2540_donor_number%TYPE,		
		p_vendor_id	        IN		t2540_donor_master.c301_vendor_id%TYPE,
		p_age				IN		t2540_donor_master.c2540_age%TYPE,
		p_sex		        IN		t2540_donor_master.c901_sex%TYPE,
		p_international_use	IN		t2540_donor_master.c901_international_use%TYPE,
		p_research			IN		t2540_donor_master.c901_research%TYPE,
		p_input_str			IN		CLOB,
		p_user_id			IN 		t2540_donor_master.c2540_last_updated_by%TYPE 
	)
	AS
		v_strlen	    	NUMBER := NVL (LENGTH (p_input_str), 0);
		v_string	    	CLOB := p_input_str;
		v_substring	    	CLOB;
		v_control_no	    t408a_dhr_control_number.c408a_conrol_number%TYPE;
		v_custom_size	    t408a_dhr_control_number.c408a_custom_size%TYPE; 

	BEGIN		
		
		-- If the donor number is not there, then no need to save donor parameter values
		IF p_donor_number IS NOT NULL OR p_donor_number != ''
		THEN
			gm_op_sav_donor_master (p_donor_number, p_vendor_id, p_age, p_sex,p_international_use,p_research, p_user_id);
		END IF;
		
		 UPDATE t408a_dhr_control_number
	       SET c408a_void_fl = 'Y',
	       c408a_last_updated_by = p_user_id,
	       c408a_last_updated_date = SYSDATE
		 WHERE c408_dhr_id = p_dhr_id AND c408a_void_fl IS NULL;
		
		WHILE INSTR(v_string,'|') <> 0 
			LOOP
		    	v_substring 	:= SUBSTR(v_string,1,INSTR(v_string,'|')-1);
		    	v_string    	:= SUBSTR(v_string,INSTR(v_string,'|')+1);
				v_control_no	:= NULL;
				v_custom_size	:= NULL; 
				v_control_no	:= SUBSTR(v_substring,1,INSTR(v_substring,'^')-1);
				v_substring 	:= SUBSTR(v_substring,INSTR(v_substring,'^')+1); 		  
				v_custom_size 	:= v_substring;
				
			gm_op_sav_dhr_control_num(p_dhr_id, p_part_num, v_control_no, v_custom_size, p_user_id);
						  
			END LOOP;			
	END gm_op_sav_donor_info;



	 /********************************************************************
	  * Description : This Procedure save dhr control number
	  * Author       : Xun
	  ********************************************************************/
	PROCEDURE gm_op_sav_dhr_control_num (
	    p_dhr_id		 IN   t408_dhr.c408_dhr_id%TYPE
	  , p_part_num		 IN   t408a_dhr_control_number.c205_part_number_id%TYPE 
	  , p_control_no     IN   t408a_dhr_control_number.c408a_conrol_number%TYPE
	  , p_custom_size	 IN   t408a_dhr_control_number.c408a_custom_size%TYPE 
	  , p_user			 IN   t408a_dhr_control_number.c408a_last_updated_by%TYPE
	  , p_status		 IN   t408a_dhr_control_number.c901_status%TYPE DEFAULT NULL
	)
	AS
		v_count	    NUMBER;
	BEGIN 
			SELECT  count(1) 
				INTO v_count 
			FROM t408a_dhr_control_number 
			WHERE c408_dhr_id = p_dhr_id 
			AND c408a_conrol_number = p_control_no;
		--	
			IF v_count > 0 THEN
				UPDATE t408a_dhr_control_number
			       SET c408a_void_fl = NULL,
			       c408a_custom_size = p_custom_size,
			       c901_status = p_status,
			       c408a_last_updated_by = p_user,
			       c408a_last_updated_date = SYSDATE
				 WHERE c408_dhr_id = p_dhr_id AND c408a_conrol_number = p_control_no;
			ELSE
				INSERT INTO t408a_dhr_control_number
			    (c408a_dhr_control_number_id, c408_dhr_id, c408a_conrol_number, c408a_custom_size, c205_part_number_id 
			   , c408a_last_updated_by, c408a_last_updated_date, c901_status
			    )
			     VALUES (s408a_dhr_control.NEXTVAL, p_dhr_id, p_control_no, p_custom_size
				   , p_part_num, p_user, SYSDATE , p_status
				    );
			END IF; 
		
	END gm_op_sav_dhr_control_num;



	 /********************************************************************
	  * Description : This Procedure fetch  donor info  
	  * Author       : Xun
	  ********************************************************************/
	PROCEDURE gm_fch_donor_info (
			p_dhr_id		IN	   t408_dhr.c408_dhr_id%TYPE,
			p_donordtls		OUT	   TYPES.cursor_type,
			p_ctrldtls		OUT	   TYPES.cursor_type
		)
		AS
		v_date_fmt	t906_rules.c906_rule_value%TYPE;
        v_company_id t1900_Company.c1900_company_id%type;
		BEGIN
			
			SELECT get_compid_frm_cntx() ,get_compdtfmt_frm_cntx()
			  INTO v_company_id ,v_date_fmt
			  FROM DUAL;
			  
			OPEN p_donordtls FOR
		         SELECT  t408.c205_part_number_id partnum
		              , t408.c408_qty_received qtyrec
		              , t408.c408_qty_packaged qtypack
					  , t408.c408_qty_rejected rejqty
					  , get_total_received_qty(p_dhr_id) totalqty
		              , to_char(t408.c2550_expiry_date,v_date_fmt) expdate
		              , t2540.c2540_donor_number donorno 
				      , t2540.c2540_age age
				      , t2540.c901_sex sex
				      , get_code_name(t2540.c901_sex) sexcodename
				      , t2540.c901_international_use internationaluse
				      , get_code_name(t2540.c901_international_use) internationalusenm
				      , t2540.c901_research forresearch
				      , get_code_name(t2540.c901_research) forresearchnm
				      , t408.c408_status_fl dhrstatus
				      , GET_PARTNUM_MATERIAL_TYPE(T408.C205_PART_NUMBER_ID) matType
              		  , GET_PART_ATTR_VALUE_BY_COMP(T408.C205_PART_NUMBER_ID,106040,v_company_id) SerialNumFl
		          FROM  t408_dhr t408, t2540_donor_master t2540   
		         WHERE t408.c408_dhr_id = p_dhr_id
			   	   AND t408.c2540_donor_id = t2540.c2540_donor_id(+)
			   	   AND t2540.C1900_COMPANY_ID(+) = v_company_id
			   	   AND t408.C1900_COMPANY_ID = v_company_id
		           AND t408.c408_void_fl IS NULL  
		           AND t2540.c2540_void_fl(+) IS NULL;
			   
		    gm_fch_lot_numbers(p_dhr_id, p_ctrldtls) ;
		    
	END gm_fch_donor_info;

	/********************************************************************
	  * Description : This Procedure is to fetch  the donor parameter details  
	  ********************************************************************/
	PROCEDURE gm_fch_donor_master_info(
		p_donor_id		IN	   t2540_donor_master.c2540_donor_number%TYPE,
		p_vendor_id		IN	   t2540_donor_master.c301_vendor_id%TYPE,
		p_donordtls		OUT	   TYPES.cursor_type
	)
	AS
		v_context_cmp_id    T1900_Company.c1900_company_id%TYPE;
		BEGIN
			
			SELECT get_compid_frm_cntx() INTO v_context_cmp_id FROM DUAL;
			
			OPEN p_donordtls FOR
		         SELECT t2540.c2540_donor_number donorno 
				      , t2540.c2540_age age
				      , t2540.c901_sex sex
				      , get_code_name(t2540.c901_sex) sexcodename
				      , t2540.c901_international_use internationaluse
				      , get_code_name(t2540.c901_international_use) internationalusenm
				      , t2540.c901_research forresearch
				      , get_code_name(t2540.c901_research) forresearchnm
		          FROM  t2540_donor_master t2540   
		         WHERE t2540.c2540_donor_number = p_donor_id
		           AND t2540.c1900_company_id = v_context_cmp_id
		           AND NVL(t2540.c301_vendor_id,'-9999') = NVL(p_vendor_id,'-9999')
		           AND t2540.c2540_void_fl IS NULL;
			   		    
	END gm_fch_donor_master_info;

	
	 /********************************************************************
	  * Description : This Procedure fetch DHR lot numbers
	  * Author       : Xun
	  ********************************************************************/
	PROCEDURE gm_fch_lot_numbers (
	      p_dhr_id   IN       t408_dhr.c408_dhr_id%TYPE, 
	      p_out      OUT      TYPES.cursor_type
	   )
	   AS
	   BEGIN
	      OPEN p_out
	       FOR
	          SELECT   c408a_conrol_number ctrlno, c408a_custom_size custsize 
	              FROM t408a_dhr_control_number
	             WHERE c408_dhr_id = p_dhr_id
	             AND c408a_void_fl IS NULL
	             AND NVL(c901_status, -999) != 1401
	          ORDER BY c408a_dhr_control_number_id;
	           
	   END gm_fch_lot_numbers;
	   
	/********************************************************************
	 * Description : This Procedure fetch DHR split types
  	 * Author       : Xun
     ********************************************************************/
	PROCEDURE gm_fch_dhr_split_types (
    	p_dhr_id   	IN      t408_dhr.c408_dhr_id%TYPE
      , p_grp		IN		T901_CODE_LOOKUP.c901_code_grp%TYPE
      , p_out      	OUT     TYPES.cursor_type
  	)
   	AS
	BEGIN
		OPEN p_out
	    FOR
	    	SELECT   c902_code_nm_alt codeid,  get_code_name (trans_type) codenm,
               t412.qty qty, trans_type, trans_id
            FROM (SELECT *
            	FROM t901_code_lookup t901
                WHERE t901.c901_code_grp = p_grp AND c901_active_fl = 1) t901,
                (SELECT   t412.c412_inhouse_trans_id trans_id,
                   	t412.c412_type trans_type,
                    SUM (t413.c413_item_qty) qty
                    FROM t412_inhouse_transactions t412,
                    	t413_inhouse_trans_items t413
                    WHERE c412_ref_id = p_dhr_id
                    AND t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
                    AND t412.c412_void_fl IS NULL
                    AND t413.c413_void_fl IS NULL
                    GROUP BY t412.c412_inhouse_trans_id, t412.c412_type) t412
            WHERE t901.c902_code_nm_alt = trans_type 
          	ORDER BY t901.c901_code_seq_no;
	           
	   END gm_fch_dhr_split_types;
	
	   
	/********************************************************************
  	* Description : This Procedure save DHR split types
  	* Author       : Xun
  	********************************************************************/
	PROCEDURE gm_sav_dhr_split (
    	p_inputstr   	IN      CLOB
      , p_user_id		IN		t413_inhouse_trans_items.c413_last_updated_by%TYPE
      , p_dhr_id   		IN      t408_dhr.c408_dhr_id%TYPE
  	)
  	AS
		v_string	    CLOB := p_inputstr;
		v_substring	    CLOB;
  		v_count			NUMBER;
  		v_txn_id		t413_inhouse_trans_items.c412_inhouse_trans_id%TYPE;
  		v_control_no	t413_inhouse_trans_items.c413_control_number%TYPE;
  		v_count1			NUMBER :=0;
	BEGIN 
		 	
		 UPDATE t413_inhouse_trans_items
		    SET C413_VOID_FL     = 'Y', 
		    C413_LAST_UPDATED_BY = p_user_id, 
		    C413_LAST_UPDATED_DATE = SYSDATE
		  WHERE c412_inhouse_trans_id IN
		    (
		         SELECT c412_inhouse_trans_id
		           FROM t412_inhouse_transactions
		          WHERE c412_ref_id = p_dhr_id
		          AND C412_VOID_FL IS NULL
		    )
		    AND C413_VOID_FL IS NULL ;	
	
		WHILE INSTR(v_string,'|') <> 0 
		LOOP
		    	v_substring 	:= SUBSTR(v_string,1,INSTR(v_string,'|')-1);
		    	v_string    	:= SUBSTR(v_string,INSTR(v_string,'|')+1);
				v_txn_id		:= NULL;
				v_control_no	:= NULL; 
				v_control_no	:= SUBSTR(v_substring,1,INSTR(v_substring,'^')-1);
				v_substring 	:= SUBSTR(v_substring,INSTR(v_substring,'^')+1); 		  
				v_txn_id	 	:= v_substring;
				 
		 
				INSERT INTO t413_inhouse_trans_items
                		(c413_trans_id, c412_inhouse_trans_id, c205_part_number_id,
                        c413_item_qty, c413_control_number, c413_item_price, c413_created_date
                        )
				SELECT s413_inhouse_item.NEXTVAL, v_txn_id, c205_part_number_id,
                		1, v_control_no, c413_item_price, SYSDATE
                FROM t413_inhouse_trans_items 
                WHERE c412_inhouse_trans_id = v_txn_id 
             --   AND c413_void_fl IS NULL 
                AND ROWNUM = 1;  
                
	END LOOP;	           
	END gm_sav_dhr_split;	
	
	/********************************************************************
	 * Description : This Procedure fetch DHR split details
  	 * Author       : Xun
     ********************************************************************/
	PROCEDURE gm_fch_dhr_split_dtls (
    	p_dhr_id   	IN      t408_dhr.c408_dhr_id%TYPE 
      , p_out      	OUT     TYPES.cursor_type
  	)
   	AS
	BEGIN
		OPEN p_out
	    FOR
	    	 SELECT t408a.c408a_conrol_number ctrlno, t408a.c408a_custom_size custsize, t413.c412_inhouse_trans_id txnid
			  , get_code_name (t412.c412_type) txntype
			   FROM t408a_dhr_control_number t408a, t413_inhouse_trans_items t413, t412_inhouse_transactions t412
			  WHERE t408a.c408_dhr_id          = p_dhr_id
			    AND t413.c413_control_number(+)   = t408a.c408a_conrol_number
			    AND t413.c412_inhouse_trans_id = t412.c412_inhouse_trans_id(+)
			    and t408a.c408_dhr_id = T412.C412_REF_ID
			    AND t408a.c408a_void_fl       IS NULL
			    AND t413.c413_void_fl         IS NULL
			    AND t412.c412_void_fl         IS NULL
			    ORDER BY t408a.c408a_conrol_number;
	   END gm_fch_dhr_split_dtls;
	
	/********************************************************************
	 * Description : This Procedure fetch lot numbers excludes rejected ones
  	 * Author       : Xun
     ********************************************************************/  
	 PROCEDURE gm_fch_combined_lotnums (
	      p_dhr_id   IN       t408_dhr.c408_dhr_id%TYPE, 
	      p_out      OUT      TYPES.cursor_type
	   )
	   AS
	   		v_count			NUMBER;
	   		v_rs_count		NUMBER;
	   		v_bulkid		t4082_bulk_dhr_mapping.c4081_dhr_bulk_id%TYPE;
	   		v_company_id    T1900_Company.c1900_company_id%TYPE;
	   BEGIN
		  SELECT get_compid_frm_cntx()    INTO v_company_id    FROM DUAL;
		   -- to check whether the dhr id is associated with RS id or not
		   BEGIN
              SELECT gm_pkg_op_bulk_dhr_appr.get_bulk_shipment_id(p_dhr_id)
				INTO v_rs_count	
				FROM DUAL;
				
				EXCEPTION WHEN OTHERS
	      	 	  THEN
      	 	  	v_count := '0';
      	 	END;
      	 	
      	 	-- Get the RS id, if the dhr id is associated with DHR id
      	 	IF v_rs_count > 0
      	 	THEN
      	 		BEGIN
	      	 		SELECT t4082.c4081_dhr_bulk_id INTO v_bulkid
					FROM t4082_bulk_dhr_mapping t4082 , t4081_dhr_bulk_receive t4081
					WHERE t4082.c4081_dhr_bulk_id = t4081.c4081_dhr_bulk_id
					AND t4082.c408_dhr_id = p_dhr_id
					AND t4081.c1900_company_id = v_company_id
					AND t4082.c4082_void_fl IS NULL
					AND t4081.c4081_void_fl IS NULL;
				EXCEPTION WHEN OTHERS
	      	 	  THEN
      	 	  	v_bulkid := NULL;
      	 	END;
      	 	END IF;
      	 	
		  SELECT COUNT (1)
		  	INTO v_count
		   FROM globus_app.t408a_dhr_control_number t408a, t413_inhouse_trans_items t413, t412_inhouse_transactions t412
		  WHERE t408a.c408_dhr_id        = p_dhr_id
		    AND t413.c413_control_number = t408a.c408a_conrol_number
			AND t413.c412_inhouse_trans_id = t412.c412_inhouse_trans_id
			 and t412.C412_REF_ID        = NVL(v_bulkid,p_dhr_id) -- fetch the count based on the ref id of inhouse trasactions table to avoid wrong count
		    AND t408a.c408a_void_fl     IS NULL
		    AND t413.c413_void_fl       IS NULL
		    AND t412.c412_void_fl       IS NULL; 
		   
		 IF (v_count >0 )
		 THEN
			 OPEN p_out
		       FOR
				 SELECT t408a.c408a_conrol_number ctrlno 
				   FROM globus_app.t408a_dhr_control_number t408a, t413_inhouse_trans_items t413, t412_inhouse_transactions t412
				  WHERE t408a.c408_dhr_id          = p_dhr_id
				    AND t413.c413_control_number   = t408a.c408a_conrol_number
				    AND t413.c412_inhouse_trans_id = t412.c412_inhouse_trans_id
				     and t412.C412_REF_ID        = NVL(v_bulkid,p_dhr_id)-- fetch the count based on the ref id of inhouse trasactions table to avoid wrong record
				    AND t408a.c408a_void_fl       IS NULL
				    AND t413.c413_void_fl         IS NULL
				    AND t412.c412_void_fl         IS NULL
				ORDER BY t408a.c408a_conrol_number;
		 ELSE		    
		      OPEN p_out
		       FOR
		          SELECT   c408a_conrol_number ctrlno 
		              FROM t408a_dhr_control_number
		             WHERE c408_dhr_id = p_dhr_id
		             AND c408a_void_fl IS NULL
		          ORDER BY c408a_dhr_control_number_id;
	     END IF;  	          
	   END gm_fch_combined_lotnums;
	   
	/****************************************************************************
	 * Description : This Procedure is used to get the partnumber for control no
	 * Author	   : aprasath
	 ****************************************************************************/
	PROCEDURE gm_chk_control_number_part (	 
	  p_control_num		IN		t2550_part_control_number.c2550_control_number%TYPE
	, p_num   			IN OUT	t2550_part_control_number.c205_part_number_id%TYPE	
	)
	AS
	    v_part_num         t2550_part_control_number.c205_part_number_id%TYPE;
	    v_prod_material    t205_part_number.c205_product_material%TYPE;	
	BEGIN	
		BEGIN
			SELECT T2550.C205_PART_NUMBER_ID,get_partnum_material_type(T2550.C205_PART_NUMBER_ID)
				INTO v_part_num,v_prod_material
			   FROM T2550_PART_CONTROL_NUMBER t2550
			  WHERE T2550.C2550_CONTROL_NUMBER = UPPER(p_control_num)
			   AND T2550.C205_PART_NUMBER_ID = NVL(p_num,T2550.C205_PART_NUMBER_ID);
			EXCEPTION
				WHEN NO_DATA_FOUND THEN
					v_part_num := NULL;
					v_prod_material := NULL;
        		WHEN TOO_MANY_ROWS THEN
        			v_part_num := '0';
					v_prod_material := NULL;
    	END;
	    p_num := v_part_num || '|' || v_prod_material;
	   END gm_chk_control_number_part;
	   
	   /**************************************************************************************************
	  	* Description: This function will return the actual qty which will be accepted after NCMR closeout
	  	***************************************************************************************************/
		FUNCTION get_total_received_qty(
			p_dhr_id	t408_dhr.c408_dhr_id%TYPE
		) RETURN VARCHAR2
		IS 
			v_qty VARCHAR2(20);
		BEGIN
			BEGIN
				SELECT   NVL (t408.c408_qty_received, 0)
		             - NVL (t408.c408_qty_rejected, 0)
		             + NVL (t409.c409_closeout_qty, 0)
		        	INTO v_qty
		        FROM T408_DHR T408, T409_NCMR T409
		       	WHERE t408.c408_dhr_id = p_dhr_id
		        	AND c409_void_fl(+) IS NULL
		         	AND t408.c408_dhr_id = t409.c408_dhr_id(+);
				EXCEPTION
					WHEN NO_DATA_FOUND
				THEN
					v_qty := '0';
			END;
			RETURN v_qty;
		END get_total_received_qty; 
		
		
     /****************************************************************************
	 * Description  : This Procedure is the main one called from front end to validate and get the scanned control number and /Or part number. 
   *                Out put msg contains partnum:XXX, cotrolnum:YYY, prodmaterial:ZZZ, txnwarehouse:nnnn,  curwarehouse:mmmm, msg:<validation msg incase of error> empty in case of no error
	 * Author	      : Yoga
	 ****************************************************************************/
  PROCEDURE gm_chk_control_scan (	 
      p_control_num	    IN    t2550_part_control_number.c2550_control_number%TYPE
      , p_txn_type		  IN    t901_code_lookup.c901_code_id%TYPE		
      , p_num           IN    t2550_part_control_number.c205_part_number_id%TYPE DEFAULT NULL
      , p_out_rtn_msg   IN OUT   VARCHAR2    
	)
	AS
    v_partnum       t2550_part_control_number.c205_part_number_id%TYPE;
    v_prod_mtrl     t205_part_number.c205_product_material%TYPE;
    v_msg           VARCHAR2(2000);
    v_txn_wh_type		NUMBER;
    v_cur_wh_type		NUMBER;
	BEGIN	
    -- Fetch the Product material and part(in case not given)
    gm_fch_prod_material(p_control_num, v_partnum, v_prod_mtrl);
    
   IF (p_txn_type IS NOT NULL) THEN 
      -- Fetch the txn wh and current wh 
      gm_fch_control_number_wh (p_control_num, p_txn_type,v_partnum, v_txn_wh_type, v_cur_wh_type);
    END IF;
    
    -- Validate product material and warehouse for the txn
    gm_validate_scan_data (p_control_num, p_txn_type, v_partnum, v_prod_mtrl, v_txn_wh_type, v_cur_wh_type, v_msg);
    
    -- Build the message string and return it to client
    gm_build_return_msg (p_control_num, p_txn_type, v_partnum, v_prod_mtrl, v_txn_wh_type, v_cur_wh_type, v_msg);
   
    p_out_rtn_msg:=v_msg;

  END;
  
   /****************************************************************************
	 * Description  : This Procedure is used to get the product material for given control number. If part number not given that will be fetched using the control number
	 * Author	      : Yoga
	 ****************************************************************************/
	PROCEDURE gm_fch_prod_material (	 
	  p_control_num		IN		  t2550_part_control_number.c2550_control_number%TYPE
    , p_num   			IN OUT  t2550_part_control_number.c205_part_number_id%TYPE	
    , p_prod_mtrl		OUT      t205_part_number.c205_product_material%TYPE
	)
	AS
	    v_part_num         t2550_part_control_number.c205_part_number_id%TYPE;
	    v_prod_material    t205_part_number.c205_product_material%TYPE;
	BEGIN	
		BEGIN
      SELECT T2550.C205_PART_NUMBER_ID,get_partnum_material_type(T2550.C205_PART_NUMBER_ID)
				INTO v_part_num,v_prod_material
			   FROM T2550_PART_CONTROL_NUMBER t2550
			  WHERE T2550.C2550_CONTROL_NUMBER = UPPER(p_control_num)
			   AND T2550.C205_PART_NUMBER_ID = NVL(p_num,T2550.C205_PART_NUMBER_ID);
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        v_part_num := NULL;
        v_prod_material := NULL;
      WHEN TOO_MANY_ROWS THEN
        v_part_num := '0';
        v_prod_material := NULL;
    END;

    p_num := v_part_num;
    p_prod_mtrl := v_prod_material;

  END gm_fch_prod_material;
  
  /****************************************************************************
	 * Description : This Procedure is used to get the Txn warehouse and current Lot warehouse
	 * Author	   : Yoga
	 ****************************************************************************/
  PROCEDURE gm_fch_control_number_wh (	 
	  p_control_num		IN	t2550_part_control_number.c2550_control_number%TYPE
  , p_txn_type		  IN  t901_code_lookup.c901_code_id%TYPE		
  , p_num   			  IN  t2550_part_control_number.c205_part_number_id%TYPE
  , p_txn_wh_type   OUT    NUMBER
  , p_cur_wh_type   OUT    NUMBER
	)
	AS
	    v_part_num          t2550_part_control_number.c205_part_number_id%TYPE;
	    v_prod_material     t205_part_number.c205_product_material%TYPE;
	    v_txn_wh_type       t5051_inv_warehouse.C5051_INV_WAREHOUSE_NM%TYPE;
	    v_cur_wh_type		    t5051_inv_warehouse.C5051_INV_WAREHOUSE_NM%TYPE;
      v_count             NUMBER;
      v_bucket            VARCHAR2(20);
	BEGIN	
    -- Get Warehouse for the given txn type. If below count is >0 then Pick location is available. Have it as FG and check next using MINUS
    SELECT get_rule_value('SRC_WH',TO_CHAR(p_txn_type))  
      INTO v_txn_wh_type
    FROM DUAL;
         
    IF (v_txn_wh_type IS NOT NULL) THEN
      BEGIN

        SELECT t5060.c901_warehouse_type 
          INTO v_cur_wh_type
          FROM t5060_control_number_inv t5060, t901_code_lookup t901
        WHERE t5060.c901_warehouse_type = t901.c901_code_id
          AND t5060.c205_part_number_id = NVL(p_num,t5060.c205_part_number_id)
          AND t5060.c5060_control_number = p_control_num
          --AND t5060.c901_warehouse_type = v_txn_wh_type
          AND t5060.c5060_qty >0;

      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          v_cur_wh_type:= NULL;
        WHEN TOO_MANY_ROWS THEN
          v_cur_wh_type:= 0;
      END;
    END IF;
    
    p_txn_wh_type:=v_txn_wh_type;
    p_cur_wh_type:=v_cur_wh_type;

  END gm_fch_control_number_wh;
     
  /****************************************************************************
	 * Description  : This Procedure validates scan data and form the error message
	 * Author	      : Yoga
	 ****************************************************************************/
  PROCEDURE gm_validate_scan_data (	 
    p_control_num	        IN    t2550_part_control_number.c2550_control_number%TYPE
    , p_txn_type		      IN    t901_code_lookup.c901_code_id%TYPE		
    , p_num               IN    t2550_part_control_number.c205_part_number_id%TYPE
    , p_prod_mtrl		      IN    t205_part_number.c205_product_material%TYPE
    , p_txn_wh_type       IN    NUMBER
    , p_cur_wh_type       IN    NUMBER
    , p_out_rtn_msg       IN OUT   VARCHAR2    
	)
	AS
    v_txn_wh_type_name	VARCHAR2(20);
    v_cur_wh_type_name	VARCHAR2(20);
    v_out_rtn_msg       VARCHAR2(1000); 
    v_skip_validation   CHAR(1);
    v_status            VARCHAR2(50);
    v_company_id        NUMBER;
	BEGIN
		
	SELECT get_compid_frm_cntx() INTO v_company_id FROM DUAL;
    -- Part validation   
 		
    IF (p_num = '0') THEN -- Lot allocated to more than one part in t2550
      v_out_rtn_msg:=  'Entered control # <B>' || p_control_num || '</B> is allocated for multiple parts, please <B>scan</B> or use format: <B>part number^control number<B>';
    END IF; 
    
    -- Not available in t2550
    IF (p_num IS NULL) THEN 
      v_out_rtn_msg:= 'Invalid control # <B>' || p_control_num || '</B>';
    END IF; 
    
    -- Check whether WH validation can fir or not
    SELECT get_rule_value_by_company('SKIP_SCAN_VALIDATION','SKIP_VALIDATION',v_company_id)  
      INTO v_skip_validation
    FROM DUAL;

	-- Environments where v_skip_validation is 'Y', skip Warehouse check
	-- In case of transaction type is not known also skip Warehouse check
  -- If txn Wh is null, Txn should not be validated ex:IAFG 
    IF (v_skip_validation IS NOT NULL AND p_txn_type IS NOT NULL AND p_txn_wh_type IS NOT NULL) THEN

      -- Lot qty is 0 in all warehouse
      IF (p_cur_wh_type IS NULL) THEN
        v_out_rtn_msg:=  'Invalid control # <B>' || p_control_num || '</B>';
      END IF; 
  
      -- Lot available in some other warehouse or no warehouse currently
      IF ((p_txn_wh_type != p_cur_wh_type) OR p_cur_wh_type IS NULL) THEN
        IF ((p_num IS NOT NULL) AND p_cur_wh_type IS NULL) THEN -- If Part not found no need to show this 
          -- Get the status of the Lot
          v_status := gm_pkg_op_lotcode_rpt.get_lot_Status(p_control_num);
  
          -- Get Lot status from the get_lot_current_status function, if returns null get_lot_Status function where it get from t2550 
          SELECT DECODE(v_status,NULL,gm_pkg_op_lotcode_rpt.get_lot_current_status(p_control_num), v_status) 
          INTO v_status FROM DUAL;
  
          IF (v_status IS NOT NULL) THEN
            v_status := 'Status of the Lot is ' || v_status;
          END IF;
  
          v_out_rtn_msg:=  'Entered control # <B>' || p_control_num || '</B> is not available in ' || GET_CODE_NAME_ALT(p_txn_wh_type) || '. ' || v_status || '' ;
        ELSIF (p_num IS NOT NULL) THEN
          v_out_rtn_msg:=  'Entered control # <B>' || p_control_num || '</B> is not available in ' || GET_CODE_NAME_ALT(p_txn_wh_type) || '. But It is available in ' || GET_CODE_NAME_ALT(p_cur_wh_type) || '. Please move it to ' || GET_CODE_NAME_ALT(p_txn_wh_type) || ' and try again ';
        END IF;
      END IF;
  
      -- Control number validation
      IF (p_cur_wh_type = 0) THEN
        v_out_rtn_msg:=  'Entered control # <B>' || p_control_num || '</B> is available in more than one warehouse. Please check in Lot code Report. ';
      END IF; 
      
      -- If the lot is in Account / FS, without processing return it cannnot be used in FG/QN and Repack transaction
      IF ((p_cur_wh_type = 4000339 OR p_cur_wh_type = 56002) and (p_txn_wh_type = 90800 OR p_txn_wh_type = 90813 OR p_txn_wh_type = 90815)) THEN
        v_out_rtn_msg:=  'Entered control # <B>' || p_control_num || '</B> is consigned out. Without processing return, it cannot be used. ';
      END IF;

    END IF;
    
    p_out_rtn_msg:=v_out_rtn_msg;

  END gm_validate_scan_data;
  
  /****************************************************************************
	 * Description  : This Procedure validates scan data and form the error message
	 * Author	      : Yoga
	 ****************************************************************************/
  PROCEDURE gm_build_return_msg (	 
      p_control_num	    IN      t2550_part_control_number.c2550_control_number%TYPE
      , p_txn_type		  IN      t901_code_lookup.c901_code_id%TYPE		
      , p_num           IN      t2550_part_control_number.c205_part_number_id%TYPE
      , p_prod_mtrl		  IN      t205_part_number.c205_product_material%TYPE
      , p_txn_wh_type   IN      NUMBER
      , p_cur_wh_type   IN      NUMBER
      , p_out_rtn_msg   IN OUT  VARCHAR2     
	)
	AS
    v_partnum     t2550_part_control_number.c205_part_number_id%TYPE;
    v_prod_mtrl		t205_part_number.c205_product_material%TYPE;
    v_out_rtn_msg       VARCHAR2(2000);
	BEGIN	
  
  p_out_rtn_msg:= 'partnum:' || p_num || ',lotnum:' ||  p_control_num || ',prodmaterial:'  || p_prod_mtrl || ',txnwh:'  || GET_CODE_NAME_ALT(p_txn_wh_type) || ',curwh:' || GET_CODE_NAME_ALT(p_cur_wh_type) || ',msg:' ||  p_out_rtn_msg;
   
  END gm_build_return_msg;
END gm_pkg_op_dhr;
/