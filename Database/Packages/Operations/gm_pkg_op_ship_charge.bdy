--@"C:\Database\Packages\Operations\gm_pkg_op_ship_charge.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_ship_charge
IS
  /*******************************************************************
   * Description : Function to get the cost of the shipping package
   *			   , for FedEx or UPS Carrier.
   * Author      : VPrasath
   *******************************************************************/
--
FUNCTION get_ship_cost (
	    p_origin   		IN  t9071_shipping_zone.c9071_origin%TYPE
	  , p_destination	IN	t9071_shipping_zone.c9071_destination%TYPE
	  , p_carrier	 	IN	t9071_shipping_zone.c901_carrier%TYPE
	  , p_ship_mode		IN	t9072_shipping_cost.c901_shipping_mode%TYPE
	  , p_pkg_weight	IN	t9072_shipping_cost.c9072_package_weight%TYPE	  
)
RETURN NUMBER
IS
		v_ship_cost t9072_shipping_cost.c9072_package_cost%TYPE;
  BEGIN
	
	BEGIN
		select NVL(c9072_package_cost, 0)
		  INTO v_ship_cost
		  from t9071_shipping_zone t9071
			 , t9072_shipping_cost t9072
		 where t9071.c9071_zone = t9072.c9072_zone
		   and t9071.c901_carrier = p_carrier
		   and t9071.c9071_origin = p_origin
		   and t9071.c9071_destination = p_destination
		   and t9072.c901_shipping_mode = p_ship_mode
		   and t9072.c9072_package_weight = p_pkg_weight
		   and t9071.c9071_void_fl is NULL
		   and t9072.c9072_void_fl is NULL;
  EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			v_ship_cost := 0;

	END;
	
		RETURN 	v_ship_cost;
END get_ship_cost;		

  /*******************************************************************
   * Description : Procedure to get the cost of the given transaction.
   * Author      : VPrasath
   *******************************************************************/

PROCEDURE gm_fch_ship_cost_for_order (
	    p_acct_id		IN	t501_order.c704_account_id%TYPE	
	  , p_ship_to 		IN	t907_shipping_info.c901_ship_to%TYPE
	  , p_ship_to_id	IN	t907_shipping_info.c907_ship_to_id%TYPE
	  , p_address_id	IN	t907_shipping_info.c106_address_id%TYPE
	  , p_carrier	 	IN	t9071_shipping_zone.c901_carrier%TYPE
	  , p_ship_mode		IN	t9072_shipping_cost.c901_shipping_mode%TYPE
	  , p_pkg_weight	IN	t9072_shipping_cost.c9072_package_weight%TYPE
	  , p_rtn			OUT	t9072_shipping_cost.c9072_package_cost%TYPE
)
AS
v_company_id    t501_order.c1900_company_id%TYPE;
v_rule_id		t906_rules.c906_rule_id%TYPE;
v_pkg_name		t906_rules.c906_rule_value%TYPE;
v_exec_string 	VARCHAR2 (4000);
BEGIN
	 
	--Getting company id from context.   
    SELECT get_compid_frm_cntx()
	  INTO v_company_id 
	FROM DUAL;
	
	v_rule_id := v_company_id||'_SHIPCOST';
	
	
	SELECT get_rule_value_by_company (v_rule_id,'SHIPCOSTPRC', v_company_id)
		INTO v_pkg_name
	FROM DUAL;
	
	IF v_pkg_name IS NOT NULL
	THEN
		v_exec_string := 'BEGIN ' || v_pkg_name || '(:p_acct_id,:p_ship_to,:p_ship_to_id,:p_address_id,:p_carrier,:p_ship_mode,:p_pkg_weight,:p_rtn); END;'; 
		EXECUTE IMMEDIATE v_exec_string using p_acct_id ,p_ship_to,p_ship_to_id,p_address_id,p_carrier , p_ship_mode, p_pkg_weight,  out p_rtn;
	ELSE
		p_rtn := 0;
	END IF;
		
END gm_fch_ship_cost_for_order;

  /*******************************************************************
   * Description : Function that decides whether to use 
   *               group shipping parameter or account 
   *               shipping parameters for the given account id.
   * Author      : VPrasath
   *******************************************************************/

FUNCTION get_gpb_or_acc_party_id(
	p_acct_id	IN	t501_order.c704_account_id%TYPE	
)
RETURN VARCHAR2
IS
	v_party_id t9080_ship_party_param.c101_party_id%TYPE;	
	v_out_party_id t9080_ship_party_param.c101_party_id%TYPE;	
BEGIN

 -- Account Shipping Parameters takes precedence over the group shipping parameters

	BEGIN
		select t9080.c101_party_id 
		  INTO v_party_id
		  from t9080_ship_party_param t9080
			 , t704_account t704
		 where t704.c101_party_id = t9080.c101_party_id
		   and t9080.c9080_void_fl is null
		   and t704.c704_void_fl is null
		   and t704.c704_account_id = p_acct_id
		   and (t9080.c901_standard_ship_payee is not null OR t9080.c9080_third_party IS NOT NULL);		
	EXCEPTION WHEN NO_DATA_FOUND THEN
			v_party_id := NULL;
	END;   

-- Group Shipping Parameters will be considered only if the account shipping parameters are not set up

	BEGIN
		select t740.c101_party_id 
		  INTO v_party_id
		  from t740_gpo_account_mapping t740
			 , t9080_ship_party_param t9080
	     where t740.c704_account_id = p_acct_id
		   and t740.c740_void_fl is null
	       and t740.c101_party_id = t9080.c101_party_id
		   and t9080.c9080_void_fl is null  
		   and (t9080.c901_standard_ship_payee is not null OR t9080.c9080_third_party IS NOT NULL)		
		   and v_party_id IS NULL;
		   
	EXCEPTION WHEN NO_DATA_FOUND THEN
		  v_out_party_id := v_party_id;
	END;	   
		
		  v_out_party_id := v_party_id;
		
   RETURN v_out_party_id;
   
END get_gpb_or_acc_party_id;

/*******************************************************************
   * Description : Function that decides whether to use 
   *               group shipping parameter or account 
   *               shipping parameters for the given account id.
   * Author      : VPrasath
   *******************************************************************/

FUNCTION get_party_id_third_party(
	p_acct_id	IN	t501_order.c704_account_id%TYPE	
)
RETURN VARCHAR2
IS
	v_party_id t9080_ship_party_param.c101_party_id%TYPE;	
	v_out_party_id t9080_ship_party_param.c101_party_id%TYPE;	
BEGIN

 -- Account Shipping Parameters takes precedence over the group shipping parameters

	BEGIN
		select t9080.c101_party_id 
		  INTO v_party_id
		  from t9080_ship_party_param t9080
			 , t704_account t704
		 where t704.c101_party_id = t9080.c101_party_id
		   and t9080.c9080_void_fl is null
		   and t704.c704_void_fl is null
		   and t704.c704_account_id = p_acct_id
		   and t9080.c9080_third_party is not null;		
	EXCEPTION WHEN NO_DATA_FOUND THEN
			v_party_id := NULL;
	END;   

-- Group Shipping Parameters will be considered only if the account shipping parameters are not set up

	BEGIN
		select t740.c101_party_id 
		  INTO v_party_id
		  from t740_gpo_account_mapping t740
			 , t9080_ship_party_param t9080
	     where t740.c704_account_id = p_acct_id
		   and t740.c740_void_fl is null
	       and t740.c101_party_id = t9080.c101_party_id
		   and t9080.c9080_void_fl is null  
		   and t9080.c9080_third_party is not null
		   and v_party_id IS NULL;
		   
	EXCEPTION WHEN NO_DATA_FOUND THEN
		  v_out_party_id := v_party_id;
	END;	   
		
		  v_out_party_id := v_party_id;
		
   RETURN v_out_party_id;
   
END get_party_id_third_party;

/*******************************************************************
   * Description : Function that decides whether to use 
   *               group shipping parameter or account 
   *               shipping parameters for the given account id.
   * Author      : VPrasath
   *******************************************************************/

FUNCTION get_party_id_for_carrier(
	p_acct_id	IN	t501_order.c704_account_id%TYPE	
)
RETURN VARCHAR2
IS
	v_party_id t9080_ship_party_param.c101_party_id%TYPE;	
	v_out_party_id t9080_ship_party_param.c101_party_id%TYPE;	
BEGIN

 -- Account Shipping Parameters takes precedence over the group shipping parameters

	BEGIN
		select t9080.c101_party_id 
		  INTO v_party_id
		  from t9080_ship_party_param t9080
			 , t704_account t704
		 where t704.c101_party_id = t9080.c101_party_id
		   and t9080.c9080_void_fl is null
		   and t704.c704_void_fl is null
		   and t704.c704_account_id = p_acct_id
		   and t9080.c901_ship_carrier is not null;		
	EXCEPTION WHEN NO_DATA_FOUND THEN
			v_party_id := NULL;
	END;   

-- Group Shipping Parameters will be considered only if the account shipping parameters are not set up

	BEGIN
		select t740.c101_party_id 
		  INTO v_party_id
		  from t740_gpo_account_mapping t740
			 , t9080_ship_party_param t9080
	     where t740.c704_account_id = p_acct_id
		   and t740.c740_void_fl is null
	       and t740.c101_party_id = t9080.c101_party_id
		   and t9080.c9080_void_fl is null  
		   and t9080.c901_ship_carrier is not null
		   and v_party_id IS NULL;
		   
	EXCEPTION WHEN NO_DATA_FOUND THEN
		  v_out_party_id := v_party_id;
	END;	   
		
		  v_out_party_id := v_party_id;
		
   RETURN v_out_party_id;
   
END get_party_id_for_carrier;

  /*******************************************************************
   * Description : Fetch the shipping parameters based on 
   *			   the selected ship mode, if ship mode falls under 
   *			   "STANDARD" category, fetch the Standard Parameters
   * Author      : VPrasath
   *******************************************************************/

PROCEDURE gm_fch_std_ship_prty_param (
	   p_party_id     	  IN   t9080_ship_party_param.c101_party_id%TYPE
	 , p_ship_payee		  OUT  t9080_ship_party_param.c901_standard_ship_payee%TYPE
	 , p_ship_chg_typ	  OUT  t9080_ship_party_param.c901_standard_ship_charge_type%TYPE
	 , p_ship_val_pct     OUT  t9080_ship_party_param.c9080_standard_ship_value_pct%TYPE	
)
AS
BEGIN	
		 BEGIN	
			select c901_standard_ship_payee
				 , c901_standard_ship_charge_type 
				 , NVL(c9080_standard_ship_value_pct,0)
			  INTO p_ship_payee
                 , p_ship_chg_typ
				 , p_ship_val_pct
			  from t9080_ship_party_param t9080
			 where t9080.c101_party_id = p_party_id
			   and t9080.c9080_void_fl is null;
		EXCEPTION WHEN NO_DATA_FOUND THEN
				p_ship_payee := NULL;
                p_ship_chg_typ := NULL;
				p_ship_val_pct := NULL;
		END;
		
END gm_fch_std_ship_prty_param;

 /*******************************************************************
   * Description : Fetch the shipping parameters based on 
   *			   the selected ship mode, if ship mode falls under 
   *			   "RUSH" category, fetch the Rush Parameters
   * Author      : VPrasath
   *******************************************************************/
   
PROCEDURE gm_fch_rush_ship_prty_param (
	   p_party_id     	  IN   t9080_ship_party_param.c101_party_id%TYPE
	 , p_ship_payee		  OUT  t9080_ship_party_param.c901_standard_ship_payee%TYPE
	 , p_ship_chg_typ	  OUT  t9080_ship_party_param.c901_standard_ship_charge_type%TYPE
	 , p_ship_val_pct     OUT  t9080_ship_party_param.c9080_standard_ship_value_pct%TYPE	
)
AS
BEGIN
		BEGIN
			select c901_rush_ship_payee
				 , c901_rush_ship_charge_type
				 , NVL(c9080_rush_ship_value_pct,0)
			  INTO p_ship_payee
				 , p_ship_chg_typ
				 , p_ship_val_pct
			  from t9080_ship_party_param t9080
			 where t9080.c101_party_id = p_party_id
			   and t9080.c9080_void_fl is null;
		EXCEPTION WHEN NO_DATA_FOUND THEN
				p_ship_payee := NULL;
                p_ship_chg_typ := NULL;
				p_ship_val_pct := NULL;
		END;		
END gm_fch_rush_ship_prty_param;	

  /*******************************************************************
   * Description : Non Contracted Accounts will not have shipping 
   *			   parameters, need to charge the Company Standard 
   *               amount, need to fetch the amount configured 
   *		       for the mode and return
   * Author      : VPrasath
   *******************************************************************/

PROCEDURE gm_fch_default_rate ( 
	   p_ship_val_pct	IN  t9080_ship_party_param.c9080_standard_ship_value_pct%TYPE
	 , p_actual_rate	IN  t9072_shipping_cost.c9072_package_cost%TYPE
	 , p_ship_mode		IN	t9072_shipping_cost.c901_shipping_mode%TYPE
	 , p_out_ship_cost	OUT t9072_shipping_cost.c9072_package_cost%TYPE
)
AS
BEGIN
		p_out_ship_cost := TO_NUMBER(NVL (TRIM (get_code_name_alt (p_ship_mode)), 0));

END gm_fch_default_rate; 	

  /*******************************************************************
   * Description : If Payee is Globus, then there is no shipping charge
   *			   for customer, so need to return "0", in front end
   *			   this will be displayed as "N/A"
   * Author      : VPrasath
   *******************************************************************/
   
PROCEDURE gm_fch_globus_rate ( 
	   p_ship_val_pct	IN  t9080_ship_party_param.c9080_standard_ship_value_pct%TYPE
	 , p_actual_rate	IN  t9072_shipping_cost.c9072_package_cost%TYPE
	 , p_ship_mode		IN	t9072_shipping_cost.c901_shipping_mode%TYPE
	 , p_out_ship_cost	OUT t9072_shipping_cost.c9072_package_cost%TYPE
)
AS
BEGIN
	   p_out_ship_cost := 0;
	   
END gm_fch_globus_rate; 	 

 /*******************************************************************
   * Description : When the charge type selected for the account is 
   *               flat, return the configured value as such
   * Author      : VPrasath
   *******************************************************************/
   
PROCEDURE gm_fch_flat_rate(
	   p_ship_val_pct	IN  t9080_ship_party_param.c9080_standard_ship_value_pct%TYPE
	 , p_actual_rate	IN  t9072_shipping_cost.c9072_package_cost%TYPE
	 , p_ship_mode		IN	t9072_shipping_cost.c901_shipping_mode%TYPE
	 , p_out_ship_cost	OUT t9072_shipping_cost.c9072_package_cost%TYPE
)
AS
BEGIN
	p_out_ship_cost := p_ship_val_pct;
END gm_fch_flat_rate;

  /*******************************************************************
   * Description : When the charge type selected for the account is 
   *               actual, return the value from the cost table, 
   *			   currently, cost table only supports the carriers
   *			   FedEx and UPS, for all other carriers, it returns
   *			   "0"
   * Author      : VPrasath
   *******************************************************************/
   
PROCEDURE gm_fch_actual_rate(
	   p_ship_val_pct	IN  t9080_ship_party_param.c9080_standard_ship_value_pct%TYPE
	 , p_actual_rate	IN  t9072_shipping_cost.c9072_package_cost%TYPE
	 , p_ship_mode		IN	t9072_shipping_cost.c901_shipping_mode%TYPE
	 , p_out_ship_cost	OUT t9072_shipping_cost.c9072_package_cost%TYPE
)
AS
BEGIN
	p_out_ship_cost := p_actual_rate; 
END gm_fch_actual_rate;

  /*******************************************************************
   * Description : When the charge type selected for the account is 
   *               actual with cap, get the value from the cost table, 
   *			   compare with the configured cap value, return the 
   *  			   one that has lesser cost.currently, cost table 
   *			   only supports the carriers FedEx and UPS, 
   * 			   for all other carriers, it returns "0"
   * Author      : VPrasath
   *******************************************************************/
   
PROCEDURE gm_fch_actual_rate_with_cap(
	   p_ship_val_pct	IN  t9080_ship_party_param.c9080_standard_ship_value_pct%TYPE
	 , p_actual_rate	IN  t9072_shipping_cost.c9072_package_cost%TYPE
	 , p_ship_mode		IN	t9072_shipping_cost.c901_shipping_mode%TYPE
	 , p_out_ship_cost	OUT t9072_shipping_cost.c9072_package_cost%TYPE
)
AS
	
BEGIN
	
	IF 	p_actual_rate <=  p_ship_val_pct THEN
			p_out_ship_cost := p_actual_rate;
	ELSE
			p_out_ship_cost := p_ship_val_pct;
	END IF;
	
END gm_fch_actual_rate_with_cap;

  /*******************************************************************
   * Description : When the charge type selected for the account is 
   *               actual with percentage, get the value from the cost table, 
   *			   add or subtract with the configured percent and return that 
   *			   , cost table only supports the carriers FedEx and UPS, 
   * 			   for all other carriers, it returns "0"
   * Author      : VPrasath
   *******************************************************************/
   
PROCEDURE gm_fch_actual_rate_with_pct(
	   p_ship_val_pct	IN  t9080_ship_party_param.c9080_standard_ship_value_pct%TYPE
	 , p_actual_rate	IN  t9072_shipping_cost.c9072_package_cost%TYPE
	 , p_ship_mode		IN	t9072_shipping_cost.c901_shipping_mode%TYPE
	 , p_out_ship_cost	OUT t9072_shipping_cost.c9072_package_cost%TYPE
)
AS
BEGIN

	p_out_ship_cost := p_actual_rate + (p_actual_rate * ( p_ship_val_pct / 100 ));
	
END gm_fch_actual_rate_with_pct;

  /*******************************************************************
   * Description : Get the cost from the cost table
   *			   , cost table only supports the carriers FedEx and UPS, 
   * 			   for all other carriers, it returns "0"
   * Author      : VPrasath
   *******************************************************************/

FUNCTION get_actual_rate (
	    p_ship_to 		IN	t907_shipping_info.c901_ship_to%TYPE
	  , p_ship_to_id	IN	t907_shipping_info.c907_ship_to_id%TYPE
	  , p_address_id	IN	t907_shipping_info.c106_address_id%TYPE
	  , p_carrier	 	IN	t9071_shipping_zone.c901_carrier%TYPE
	  , p_ship_mode		IN	t9072_shipping_cost.c901_shipping_mode%TYPE
	  , p_pkg_weight	IN	t9072_shipping_cost.c9072_package_weight%TYPE
)
RETURN NUMBER
IS
	   v_origin   	  t9071_shipping_zone.c9071_origin%TYPE;
	   v_destination  t9071_shipping_zone.c9071_destination%TYPE;	  
	   v_pkg_weight	  t9072_shipping_cost.c9072_package_weight%TYPE;
	   v_ship_cost 	  t9072_shipping_cost.C9072_PACKAGE_COST%TYPE;
BEGIN

	 BEGIN
		v_origin :=  get_rule_value('ORIGIN' , 'ZIPCODE');
		v_destination := get_dest_zip_code( p_ship_to, p_ship_to_id, p_address_id);
		v_pkg_weight := NVL(p_pkg_weight, get_rule_value('DEFAULT','PKGWGT'));
		v_ship_cost := get_ship_cost(v_origin, v_destination, p_carrier, p_ship_mode, v_pkg_weight);
		--DBMS_OUTPUT.PUT_LINE('v_ship_cost : ' || v_ship_cost );
		RETURN v_ship_cost;
	  EXCEPTION WHEN NO_DATA_FOUND THEN
		RETURN 0;
	  END;
	  
END get_actual_rate;

  /*******************************************************************
   * Description : Get the Zip Code for Rep, Account, Distributor
   *		       and employee. if not able to find the Zipcode 
   *			   this function will return 'N/A'
   * Author      : VPrasath
   *******************************************************************/

 FUNCTION get_zip_code (
      p_shipto     IN   t907_shipping_info.c901_ship_to%TYPE,
      p_shiptoid   IN   t907_shipping_info.c907_ship_to_id%TYPE
   )
      RETURN VARCHAR2
   IS
      v_ship_zip   VARCHAR2 (1000);
   BEGIN
	   
      IF p_shipto = 4120 
      THEN  
         SELECT t701.c701_ship_zip_code
           INTO v_ship_zip
           FROM t701_distributor t701
          WHERE t701.c701_distributor_id = p_shiptoid 
		    AND c701_void_fl IS NULL;
      ELSIF p_shipto = 4122 
      THEN                                                        
         SELECT t704.c704_ship_zip_code
           INTO v_ship_zip
           FROM t704_account t704
          WHERE t704.c704_account_id = p_shiptoid 
		    AND c704_void_fl IS NULL;
      ELSIF p_shipto = 4123 
      THEN                              
      		v_ship_zip := get_emp_zipcode(NULL,p_shiptoid);
      ELSIF p_shipto = 4121 
      THEN                                                         
        SELECT c106_zip_code                                           
              INTO v_ship_zip
              FROM t101_user t101
			     , t106_address t106
             WHERE t101.c101_party_id = t106.c101_party_id
               AND t106.c106_address_id = TO_NUMBER (p_shiptoid);
	  ELSE
		v_ship_zip := NULL;
      END IF;
      RETURN v_ship_zip;
	  EXCEPTION 
	  	 WHEN NO_DATA_FOUND 
	  	 THEN
	  		v_ship_zip := NULL;
			RETURN v_ship_zip;
   END get_zip_code;
   

  /*******************************************************************
   * Description : Get the Zip Code for employee. Currently called from the
   *               get_zip_code function.  if not able to find the Zipcode 
   *			   this function will return 'N/A'
   * Author      : VPrasath
   *******************************************************************/
	FUNCTION get_emp_zipcode (
		p_address_id	    IN		t907_shipping_info.c106_address_id%TYPE,
		p_emp_id			IN      t907_shipping_info.c907_ship_to_id%TYPE	 
	)
	RETURN VARCHAR2
	IS 
		v_addressid	   t907_shipping_info.c106_address_id%TYPE;
		v_ship_zip      VARCHAR2(1000);
	BEGIN
		
		IF p_address_id  IS NULL  THEN	
		BEGIN
			SELECT c106_address_id
			  INTO v_addressid
			  FROM t101_user t101, t106_address t106
             WHERE t101.c101_party_id     = t106.c101_party_id            
			   AND t101.c101_user_id = p_emp_id
               AND t106.c106_primary_fl   = 'Y'
               AND t106.c106_void_fl IS NULL ;
        EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               v_addressid := NULL;
        	 END;
        ELSE 
        	v_addressid := p_address_id;        	
        END IF; 
        
        IF v_addressid  IS NULL  THEN	
			v_ship_zip := 'N/A';
        ELSE
			SELECT c106_zip_code                                           
              INTO v_ship_zip
              FROM t101_user t101, t106_address t106
             WHERE t101.c101_party_id 	= t106.c101_party_id
               AND t106.c106_address_id = TO_NUMBER (v_addressid);
        END IF;
      RETURN v_ship_zip;         
	END get_emp_zipcode;
	
  /*******************************************************************
   * Description : Function to get the destination zip code based the
   *			   shipping parameter entered.
   * Author      : VPrasath
   *******************************************************************/
--
FUNCTION get_dest_zip_code (
		p_ship_to 		IN	t907_shipping_info.c901_ship_to%TYPE
	  , p_ship_to_id	IN	t907_shipping_info.c907_ship_to_id%TYPE
	  , p_address_id	IN	t907_shipping_info.c106_address_id%TYPE
)
RETURN VARCHAR2
IS
	v_destination t9071_shipping_zone.c9071_destination%TYPE;
BEGIN

	BEGIN
		IF p_address_id IS NOT NULL THEN
			select SUBSTR(c106_zip_code,0,3)
			  into v_destination
			  from t106_address t106 
			 where t106.c106_address_id = p_address_id
			   and t106.c106_void_fl is null;
		ELSE
				SELECT SUBSTR (get_zip_code(p_ship_to, p_ship_to_id),0,3)
				into v_destination
				from dual;
		END IF;
	  EXCEPTION WHEN OTHERS THEN
		v_destination := NULL;
	 END;

	RETURN v_destination;
END get_dest_zip_code;

	/*******************************************************************
   * Description : Procedure to get the cost of the given transaction.
   *******************************************************************/

	PROCEDURE gm_fch_ship_cost_for_order (
		    p_ref_id		IN	t501_order.c704_account_id%TYPE
		  , p_source		IN	t907_shipping_info.c901_source%TYPE
		  , p_ship_to 		IN	t907_shipping_info.c901_ship_to%TYPE
		  , p_ship_to_id	IN	t907_shipping_info.c907_ship_to_id%TYPE
		  , p_address_id	IN	t907_shipping_info.c106_address_id%TYPE
		  , p_carrier	 	IN	t9071_shipping_zone.c901_carrier%TYPE
		  , p_ship_mode		IN	t9072_shipping_cost.c901_shipping_mode%TYPE
		  , p_pkg_weight	IN	t9072_shipping_cost.c9072_package_weight%TYPE DEFAULT NULL
		  , p_rtn			OUT	t9072_shipping_cost.c9072_package_cost%TYPE
	)
	AS
		v_acc_id		t501_order.c704_account_id%TYPE;
		v_parent_order_id t501_order.c501_parent_order_id%TYPE;
	BEGIN
		
		-- Get the account id to calculate the shipping charge
		BEGIN
			SELECT C704_ACCOUNT_ID, c501_parent_order_id 
			  INTO v_acc_id, v_parent_order_id
			  FROM T501_ORDER 
			WHERE c501_order_id = p_ref_id
			  AND c501_void_fl IS NULL;
		EXCEPTION 
		WHEN NO_DATA_FOUND THEN
			v_acc_id := NULL;
		END;
		--if order is child order, ship cost should be 0, no need to fetch ship cost.
	 	IF  TRIM(v_parent_order_id) IS NULL 
	 	THEN
		-- Calculate the shipping charge
		gm_pkg_op_ship_charge.gm_fch_ship_cost_for_order(v_acc_id,p_ship_to,p_ship_to_id,p_address_id,p_carrier,p_ship_mode,p_pkg_weight,p_rtn);
		  
	 	ELSE
	 		p_rtn :=0;
	 	END IF;
		 
	END gm_fch_ship_cost_for_order;
	
	
END gm_pkg_op_ship_charge;
/