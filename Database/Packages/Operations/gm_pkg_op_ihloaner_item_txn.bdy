/*
 * @"C:\Database\Packages\Operations\gm_pkg_op_ihloaner_item_txn.bdy";
 */

create or replace
PACKAGE BODY gm_pkg_op_ihloaner_item_txn
IS

/*
 * Description: This procedure is used to create the GM-IH missing transaction from the Returns for the IH Loaner Return
 */
PROCEDURE gm_sav_inih_missing_item (
	p_raid		 IN   t506_returns.c506_rma_id%TYPE,
	p_ret_type	 IN	  t506_returns.C506_type%TYPE,
	p_userid	 IN	  T412_INHOUSE_TRANSACTIONS.C412_CREATED_BY%TYPE,
	p_out 		 OUT  VARCHAR2
)
AS
CURSOR v_returns_cur
		IS
			SELECT pnum, ra.iniqty - ra.rqty qty, 'Q' sfl,cnum
			  FROM (SELECT	 c205_part_number_id pnum,C507_Control_number cnum, SUM (DECODE (c507_status_fl, 'Q', c507_item_qty, 0)) iniqty
						   , SUM (DECODE (c507_status_fl, 'R', c507_item_qty, 0)) rqty
						FROM t507_returns_item
					   WHERE c506_rma_id = p_raid
					GROUP BY c205_part_number_id,C507_Control_number) ra
			 WHERE ra.iniqty - ra.rqty > 0;
	v_cnt	NUMBER := 0;
	v_transid	T412_INHOUSE_TRANSACTIONS.C412_INHOUSE_TRANS_ID%TYPE := '';
	v_comments	VARCHAR2(2000) := 'Missing Transaction created from the RA ID : ' || p_raid;
	v_price		VARCHAR2(20); 
	v_ratype	VARCHAR2(20) := p_ret_type;
	v_type		VARCHAR2(20) := '9112'; -- IHMP
	v_purpose 	VARCHAR2(20) := '50107'; --	 Inhouse set/item missing
	v_ihlntxn	T506_RETURNS.C506_REF_ID%TYPE;
	v_loanto	t7103_schedule_info.c901_loan_to%TYPE := NULL;
	v_loantoid	t7103_schedule_info.c101_loan_to_id%TYPE := NULL;
	
	v_add_month    NUMBER;
	v_day		   VARCHAR2 (10);
	v_final_dd	   VARCHAR2 (2);
BEGIN
	
	SELECT COUNT(1) INTO v_cnt
	  FROM (SELECT	 c205_part_number_id pnum, SUM (DECODE (c507_status_fl, 'Q', c507_item_qty, 0)) iniqty
				   , SUM (DECODE (c507_status_fl, 'R', c507_item_qty, 0)) rqty
				FROM t507_returns_item
			   WHERE c506_rma_id = p_raid
			GROUP BY c205_part_number_id) ra
	 WHERE ra.iniqty - ra.rqty > 0;
	 
	 BEGIN
		 SELECT C506_REF_ID INTO v_ihlntxn FROM T506_RETURNS
		 	WHERE C506_RMA_ID = p_raid AND C506_VOID_FL IS NULL;
	 EXCEPTION WHEN NO_DATA_FOUND
	 THEN
	 	v_ihlntxn := NULL;
	 END;
	
	 IF v_cnt <> 0 
	 THEN
	 	IF v_ratype = '3309' -- PL Item
	 	THEN
	 		v_type := '9115'; -- GM-LN missing for Product Loaner Item
	 		v_purpose := '50127'; -- Loaner Set/Item Missing
	 	END IF;
	 	-- Fetching Event Loan To and Loan To Id 
	 	IF v_ihlntxn IS NOT NULL
	 	THEN
	 		v_loanto := gm_pkg_op_ihloaner_item_txn.get_ih_loanto(v_ihlntxn);
	 		v_loantoid := gm_pkg_op_ihloaner_item_txn.get_ih_loantoid(v_ihlntxn);
	 	END IF;
	 	
	 	gm_pkg_op_request_master.gm_sav_inhouse_txn(v_comments,'0',v_purpose,v_type,p_raid,p_ret_type,v_loanto,v_loantoid,p_userid,v_transid);
	 	
	 		-- For In House Missing (IHMP), update the date that this transaction should get written off
			 BEGIN
				 SELECT TO_CHAR (SYSDATE, 'DD'), get_rule_value ('FINAL', 'EMAIL')
				  INTO v_day, v_final_dd
				  FROM DUAL;
			  EXCEPTION WHEN NO_DATA_FOUND THEN
			  		v_day := 0;
			   v_final_dd := 0;
			  END;
	
			IF v_day >= v_final_dd
			THEN
				v_add_month := 1;
			ELSE
				v_add_month := 0;
			END IF;
	
				UPDATE t412_inhouse_transactions
				   SET c412_expected_return_date =
						   LAST_DAY(TO_DATE (   TO_CHAR (ADD_MONTHS (SYSDATE, v_add_month), 'MM')
									|| '/01/'
									|| TO_CHAR (ADD_MONTHS (SYSDATE, v_add_month), 'YYYY')
								  , 'MM/DD/YYYY'
								   ))
					 , c412_last_updated_date = SYSDATE
					 , c412_last_updated_by = p_userid
				 WHERE c412_inhouse_trans_id = v_transid 
				   AND c412_void_fl IS NULL; 
	 	
	 	FOR rad_val IN v_returns_cur
		LOOP
			v_price 	:= get_part_price (rad_val.pnum, 'E');
			gm_pkg_op_request_master.gm_sav_inhouse_txn_dtl(v_transid,rad_val.pnum,rad_val.qty,rad_val.cnum,v_price,rad_val.sfl,p_userid);			
			
		END LOOP;			 	
	 END IF;
	 p_out := v_transid;
END gm_sav_inih_missing_item;

/*
 * Description:	This procedure is used to rollback the RA items and IHMP
 * Author: Rajkumar
 */
PROCEDURE  gm_rbck_ra_ig_item(
	p_raid		 IN   t506_returns.c506_rma_id%TYPE,
	p_userid	 IN	  T412_INHOUSE_TRANSACTIONS.C412_CREATED_BY%TYPE
)
AS
		CURSOR v_cur
			IS
			SELECT t413.c205_part_number_id pnum, NVL(SUM (t413.c413_item_qty),0) qty
			   FROM t412_inhouse_transactions t412, t413_inhouse_trans_items t413
			  WHERE t413.c412_inhouse_trans_id = t412.c412_inhouse_trans_id
			    AND t413.c413_status_fl        = 'Q'
			    AND t412.c412_ref_id           = p_raid
			    AND t412.c412_void_fl         IS NULL
			    AND t413.c413_void_fl         IS NULL
			GROUP BY t413.c205_part_number_id;
			
			
BEGIN
	
		FOR v_rad IN v_cur
				 	LOOP
		
				 		UPDATE T507_RETURNS_ITEM SET C507_ITEM_QTY = C507_ITEM_QTY + v_rad.qty
				 			WHERE C205_PART_NUMBER_ID = v_rad.pnum
				 			AND C507_STATUS_FL = 'R' 
		 			AND C506_RMA_ID = p_raid;
					
		END LOOP;
			 	
		--When ever the child is updated, the parent table has to be updated ,so ETL can Sync it to GOP.
	 	UPDATE t506_returns
	  	   SET C506_LAST_UPDATED_BY = p_userid, 
			   C506_LAST_UPDATED_DATE = SYSDATE
	     WHERE C506_rma_Id = p_raid;
     
		UPDATE T412_INHOUSE_TRANSACTIONS 
			SET 
				C412_VOID_FL = 'Y',
				C412_LAST_UPDATED_DATE = SYSDATE,
				C412_LAST_UPDATED_BY = p_userid
		 		WHERE C412_REF_ID = p_raid;
END gm_rbck_ra_ig_item;
/*
 * Description:	This procedure is used to reconfigure BO request and release parts from Back order
 * Author: Gopinathan
 */
PROCEDURE gm_sav_rel_ihbo_dtl(
	p_txnid					IN	T412_INHOUSE_TRANSACTIONS.C412_INHOUSE_TRANS_ID%TYPE,
	p_txntype				IN	T412_INHOUSE_TRANSACTIONS.C412_TYPE%TYPE,
	p_fgstr					IN	VARCHAR2,
	p_blstr					IN	VARCHAR2,
	p_ihstr					IN	VARCHAR2,
	p_vdstr					IN	VARCHAR2,
	p_userid	 			IN	T412_INHOUSE_TRANSACTIONS.C412_CREATED_BY%TYPE,
	p_inhouse_txn_ids		OUT VARCHAR2,
	p_txn_ids				OUT VARCHAR2
)
AS
	v_outtxnid				VARCHAR2(20);   
    v_inhouse_txn_ids		VARCHAR2(400);
    v_txn_ids				VARCHAR2(400);
BEGIN
	v_outtxnid := NULL;
	 -- This will be FGIH(9106),BLIH(9140), IHRQ(1006483), IHLN(9110), VDIH(9117)
	IF p_fgstr IS NOT NULL THEN	
		gm_sav_ihbo_dtl_str(p_txnid,p_txntype,p_fgstr,p_userid,v_outtxnid);
		v_inhouse_txn_ids := v_inhouse_txn_ids ||',' || v_outtxnid;
	END IF;
	IF p_blstr IS NOT NULL THEN	
		gm_sav_ihbo_dtl_str(p_txnid,p_txntype,p_blstr,p_userid,v_outtxnid);
		v_inhouse_txn_ids := v_inhouse_txn_ids ||',' || v_outtxnid;
	END IF;
	IF p_ihstr IS NOT NULL THEN	
		gm_sav_ihbo_dtl_str(p_txnid,p_txntype,p_ihstr,p_userid,v_outtxnid);
		v_txn_ids := v_txn_ids ||',' || v_outtxnid;
	END IF;
	IF p_vdstr IS NOT NULL THEN	
		gm_sav_ihbo_dtl_str(p_txnid,p_txntype,p_vdstr,p_userid,v_outtxnid);
		v_txn_ids := v_txn_ids ||',' || v_outtxnid;		
	END IF;	
	p_txn_ids := v_txn_ids;
	p_inhouse_txn_ids := v_inhouse_txn_ids;
	
	gm_sav_ihbo_req_status(p_txnid,p_txntype,p_userid);
	
END gm_sav_rel_ihbo_dtl;

/*
 * Description:	This procedure is used to save the released parts to proper transactions.
 * Author: Gopinathan
 */
PROCEDURE gm_sav_ihbo_dtl_str(
	p_txnid			IN	T412_INHOUSE_TRANSACTIONS.C412_INHOUSE_TRANS_ID%TYPE,
	p_txntype		IN	T412_INHOUSE_TRANSACTIONS.C412_TYPE%TYPE,
	p_str			IN	VARCHAR2,
	p_userid	 	IN	T412_INHOUSE_TRANSACTIONS.C412_CREATED_BY%TYPE,
	p_txn_id		OUT VARCHAR2
)
AS
	v_string		VARCHAR2(4000) := p_str;
	v_substring		VARCHAR2(4000);
   	v_pnum      	T413_INHOUSE_TRANS_ITEMS.C205_PART_NUMBER_ID%TYPE;
    v_qty           T413_INHOUSE_TRANS_ITEMS.C413_ITEM_QTY%TYPE;  			
    v_outtxnid		VARCHAR2(20);
    v_type			NUMBER;
    V_ITEM_QTY      T413_INHOUSE_TRANS_ITEMS.C413_ITEM_QTY%TYPE;
     v_company_id Number;
BEGIN

 SELECT get_compid_frm_cntx()
	  INTO v_company_id 
	  FROM DUAL;

	WHILE INSTR(v_string,'|') <> 0
	LOOP
		v_substring := substr(v_string,0,INSTR(v_string,'|') - 1);
	    v_string := substr(v_string,INSTR(v_string,'|') + 1);
	    
	    v_pnum	:= null;
	   	v_qty	:= null;
	   	
	   	v_pnum := substr(v_substring,0,INSTR(v_substring,'^') - 1); 
	   	v_substring := substr(v_substring,INSTR(v_substring,'^') + 1);
	   	v_qty	:= substr(v_substring,0,INSTR(v_substring,'^') - 1);
	   	v_substring := substr(v_substring,INSTR(v_substring,'^') + 1);
	   	v_type	:=	TO_NUMBER(v_substring); -- This will be FGIH(9106),BLIH(9140), IHRQ(1006483), IHLN(9110) , BOIH(9116)  
	   	
	   	gm_pkg_sm_loaner_item_alloc.gm_chk_inhouse_txn(p_txnid, p_txntype,v_pnum, v_type, p_userid, v_qty,v_outtxnid);
	   	
	   	IF v_type = '9116' THEN	   	-- This procedure call will help to map The BOIH part Qty's to the IHLN transaction
	   			gm_pkg_op_ihloaner_item_txn.gm_sav_boih_txn_map(v_outtxnid,v_type,v_pnum,v_qty,p_userid);
	   	END IF;
	   	
        -- Get current item qty
	   	SELECT c413_item_qty
		  INTO v_item_qty
		  FROM t413_inhouse_trans_items
		WHERE c412_inhouse_trans_id = p_txnid
	   	    AND c205_part_number_id = v_pnum
	   	    AND C413_VOID_FL IS NULL
		FOR UPDATE;

        -- Check current screen qty and item qty
		IF v_item_qty >= v_qty THEN
			UPDATE t413_inhouse_trans_items
			   SET c413_item_qty = (c413_item_qty - v_qty)
				 , c413_last_updated_by = p_userid
				 , c413_last_updated_date = SYSDATE
			WHERE c412_inhouse_trans_id = p_txnid
			  AND c205_part_number_id = v_pnum
			  AND c413_void_fl IS NULL;
		ELSE 
                  GM_RAISE_APPLICATION_ERROR('-20999','203',v_pnum);
	   	END IF;
	   	
	 END LOOP;
	 			  
	--When ever the child is updated, the parent table has to be updated ,so ETL can Sync it to GOP.
       UPDATE t412_inhouse_transactions
         SET c412_last_updated_date = SYSDATE,
             c412_last_updated_by = p_userid
       WHERE C412_INHOUSE_TRANS_ID = P_TXNID
       and C1900_COMPANY_ID = v_company_id;
       
	 p_txn_id := v_outtxnid;
END gm_sav_ihbo_dtl_str; 

/*******************************************************
   * Description : Procedure is used to update the request status to approved when all parts are released.
   * Author	  : Ritesh Shah
   *******************************************************/	
PROCEDURE gm_sav_ihbo_req_status (
		p_txnid	 	 IN		t412_inhouse_transactions.c412_inhouse_trans_id%TYPE,	
		p_type       IN 	t412_inhouse_transactions.c412_type%TYPE,
		p_userId     IN     t102_user_login.c102_user_login_id%TYPE	  
	)
	AS
	v_qty             NUMBER;
	V_CNT             NUMBER;
   v_company_id Number;
BEGIN

  SELECT get_compid_frm_cntx()
	  INTO v_company_id 
	  FROM DUAL;

	SELECT SUM(c413_item_qty) 
	  INTO v_qty 
	  FROM t413_inhouse_trans_items 
	 WHERE C412_INHOUSE_TRANS_ID = P_TXNID
	   AND c413_void_fl is null;
	 
	 SELECT count(c412_inhouse_trans_id)
	  INTO v_cnt
	  FROM t412_inhouse_transactions
	 WHERE c412_ref_id = p_txnid
	   AND C412_TYPE NOT IN (9117)
     and C1900_COMPANY_ID = v_company_id
	   AND c412_void_fl IS NULL;
	   
	  IF v_qty = 0 THEN
	  		IF v_cnt > 0 THEN	  		   
			 	UPDATE t412_inhouse_transactions
		   		   SET c412_status_fl = DECODE(c412_type,1006572,3, 4) -- 3-REVIEWED, 4-CLOSED  
		   			 , c412_last_updated_by   = p_userId
		   			 , c412_last_updated_date = SYSDATE
		  		 WHERE C412_INHOUSE_TRANS_ID  = P_TXNID
            and C1900_COMPANY_ID = v_company_id
		  		   AND c412_void_fl IS NULL;
		  	ELSE
		  		UPDATE t412_inhouse_transactions
		   		   SET c412_status_fl = 3 -- REVIEWED 
		   		     , c412_void_fl = 'Y' 
		   			 , c412_last_updated_by   = p_userId
		   			 , c412_last_updated_date = SYSDATE
		  		 WHERE C412_INHOUSE_TRANS_ID  = P_TXNID
            and C1900_COMPANY_ID = v_company_id;	
		 	END IF;
		ELSE
			UPDATE t412_inhouse_transactions
		   		   SET c412_status_fl = 3 -- REVIEWED  
		   			 , c412_last_updated_by   = p_userId
		   			 , c412_last_updated_date = SYSDATE
		  		 WHERE C412_INHOUSE_TRANS_ID  = P_TXNID
            and C1900_COMPANY_ID = v_company_id
		  		   AND c412_void_fl IS NULL;
	  END IF; 
END gm_sav_ihbo_req_status;
/*
 * Description:	This procedure is used to link In-house transaction to IHLN
 * Author: Yoga
 */
PROCEDURE gm_sav_ihtxn_map(
    p_txnid     IN  T412_INHOUSE_TRANSACTIONS.C412_INHOUSE_TRANS_ID%TYPE,
    p_userid    IN  T412_INHOUSE_TRANSACTIONS.C412_CREATED_BY%TYPE,
    p_txntype   IN  T412_INHOUSE_TRANSACTIONS.C412_TYPE%TYPE
)
AS
  v_ref_id       t525_product_request.c525_product_request_id%TYPE;
  v_ref_type     T412_INHOUSE_TRANSACTIONS.c901_ref_type%TYPE;
  v_cur_req_det  TYPES.cursor_type; -- To get t413 details
  v_ihln_id      t504_consignment.c504_consignment_id%TYPE;
  v_ref_qty      t413_inhouse_trans_items.c413_item_qty%TYPE;
  v_part_num     t413_inhouse_trans_items.c205_part_number_id%TYPE;
  v_txn_id       VARCHAR2(20);
  v_event_status t7100_case_information.c901_case_status%TYPE;
BEGIN
  -- Get the Parent Request id which is the t525 id and type 
  BEGIN
    SELECT t412.C412_REF_ID, t412.C901_REF_TYPE
      INTO v_ref_id, v_ref_type
      FROM t412_inhouse_transactions t412
     WHERE t412.c412_inhouse_trans_id= p_txnid
        AND t412.c412_status_fl = 4 -- Verified
        AND t412.c412_void_fl is null
        AND t412.c412_type = p_txntype;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    RETURN;
  END;

  -- If the Parent Request id is Back order, get its Parent id which is the t525 id and type
  IF (v_ref_type = 1006483) --It may be IHRQ - 1006483
  THEN
  	--gm_pkg_op_ihloaner_item_txn.gm_sav_released_ihbo_req(v_ref_id,v_ref_type,p_userid);
    BEGIN
      SELECT t412.C412_REF_ID, t412.C901_REF_TYPE
        INTO v_ref_id, v_ref_type
        FROM t412_inhouse_transactions t412
       WHERE t412.c412_inhouse_trans_id= v_ref_id
         AND t412.c412_void_fl IS NULL;
       
    EXCEPTION WHEN NO_DATA_FOUND THEN
      RETURN;
    END;
  END IF;
  -- IF event is cancelled, should return from this point
  v_event_status := gm_pkg_sm_eventbook_txn.get_event_status(TO_NUMBER(v_ref_id));
  
  IF v_event_status = 19525 THEN -- 19525 is cancelled 
  	GM_RAISE_APPLICATION_ERROR('-20999','204','');
  END IF;	
  
  IF (v_ref_type = 4119 OR v_ref_type = 4127)  
  THEN -- Call only for Inhouse Loaner or Product loaner
    gm_pkg_op_ihloaner_item_txn.gm_fch_ih_request_dtl(p_txnid, v_cur_req_det);

    LOOP  -- Loop each child request details
      FETCH v_cur_req_det
        INTO v_part_num, v_ref_qty;

        EXIT WHEN v_cur_req_det%NOTFOUND;

        IF (v_ref_qty>0)
        THEN
         gm_pkg_sm_loaner_item_alloc.gm_chk_inhouse_txn(v_ref_id,v_ref_type,v_part_num,9110, p_userid, v_ref_qty, v_txn_id);-- 9110>IHLN  
        END IF;

    END LOOP;
  END IF;

END gm_sav_ihtxn_map; 

/*********************************
   Purpose: This procedure is to fetch data from t413.
   Author: Yoga
**********************************/
--
PROCEDURE gm_fch_ih_request_dtl(
    p_ih_trans_id  IN  T413_INHOUSE_TRANS_ITEMS.C412_INHOUSE_TRANS_ID%TYPE,
    p_out_req_det  OUT TYPES.cursor_type
  )
AS
BEGIN
  OPEN p_out_req_det 
    FOR
      SELECT t413.c205_part_number_id, t413.c413_item_qty 
        FROM T413_INHOUSE_TRANS_ITEMS t413
       WHERE t413.C412_INHOUSE_TRANS_ID = p_ih_trans_id
         AND t413.c413_void_fl is null;
    EXCEPTION WHEN NO_DATA_FOUND THEN
      RETURN;
END gm_fch_ih_request_dtl;
--
 /*************************************************************************
   * Description : This procedure is used to create the RA id in t506 table
   * Author      : Hreddi
   **************************************************************************/
   PROCEDURE gm_sav_ihloaner_item_return (
      p_txn_id          IN   VARCHAR2,
      p_source			IN   t907_shipping_info.c901_source%TYPE,
      p_userid          IN   t506_returns.c506_last_updated_by%TYPE,
      p_return_id       OUT  VARCHAR2
   )
   AS     
   	v_part_str  VARCHAR2(4000);
   	v_qty_str   VARCHAR2(4000); 
   	v_exp_dt 	VARCHAR2(20);
   	v_txn_id 	t504_consignment.c504_consignment_id%TYPE;
   	v_return_id t506_returns.c506_rma_id%TYPE;
   	v_loanto		t7103_schedule_info.c901_loan_to%TYPE;
   	v_loantoid		t7103_schedule_info.c101_loan_to_id%TYPE;
	CURSOR c_all_data 
		IS
		 SELECT t505.c205_part_number_id PNUM
             , t505.c505_item_qty QTY, T505.C505_CONTROL_NUMBER CNUM
          FROM t504_consignment t504, t505_item_consignment t505
         WHERE t505.c504_consignment_id = t504.c504_consignment_id
           AND t504.c504_consignment_id = p_txn_id
           AND t504.c504_type = DECODE(p_source,50186,9110,50182,4127,p_source) -- Inhouse Loaner(50186)->IHLN(9110),Loaner(50182)->PL(4127)
           AND t504.c504_void_fl IS NULL
           AND t505.c505_void_fl IS NULL;
   
	BEGIN
	        BEGIN   
			     SELECT TO_CHAR(t907.c525_product_request_id) INTO v_txn_id 
				   FROM t907_shipping_info t907,t504_consignment t504
				  WHERE t907.c907_ref_id = p_txn_id
				    AND t907.c907_ref_id = t504.c504_consignment_id
		            AND t907.c525_product_request_id = t504.c504_ref_id
		            AND t504.c504_type = DECODE(p_source,50186,9110,50182,4127,p_source) -- Inhouse Loaner(50186)->IHLN(9110),Loaner(50182)->PL(4127)
		            AND t504.c504_void_fl IS NULL
				    AND t907.c901_source   = p_source --Inhouser Loaners - source
				    AND t907.c907_void_fl IS NULL;
				EXCEPTION
					WHEN NO_DATA_FOUND
					THEN
						RETURN; -- Inhouser Loaner set
			END; 
	         
	        FOR cur IN c_all_data
			LOOP
				v_qty_str 	:= v_qty_str || cur.QTY || ',';
				v_part_str 	:= v_part_str || cur.PNUM || ',';				
			END LOOP;   
			
			BEGIN
				 SELECT TO_CHAR (C526_Exp_Return_Date, 'mm/dd/yyyy')
				   INTO v_exp_dt
				   FROM T526_Product_Request_Detail
				  WHERE C525_Product_Request_Id = v_txn_id
				    AND C205_Part_Number_Id    IN
				    (
				         SELECT C205_Part_Number_Id
				           FROM T505_Item_Consignment
				          WHERE C504_Consignment_Id = p_txn_id
				            AND C505_Void_Fl       IS NULL
				    )
				    AND C526_Void_Fl IS NULL
				    AND ROWNUM        = 1; 
				
				EXCEPTION
					WHEN NO_DATA_FOUND
					THEN
						SELECT TO_CHAR(SYSDATE+6, 'mm/dd/yyyy') 
						  INTO v_exp_dt 
						  FROM  DUAL;
			END; 
			
			IF v_part_str IS NOT NULL AND v_qty_str IS NOT NULL THEN 
			    --create RA# for IHLN
           		gm_pkg_op_return.gm_op_sav_return( '01' --distId
           		                                 , v_exp_dt
           		                                 , '3252' --Reason
           		                                 , p_userid
           		                                 , '3308' --IH Loaner - Items
           		                                 , NULL
           		                                 , v_qty_str
           		                                 , v_part_str
           		                                 , NULL
           		                                 , v_return_id
           		                                 ); 
           		                                 
           END IF;
           
           v_loantoid := gm_pkg_op_ihloaner_item_txn.get_ih_loantoid(p_txn_id);
           
           UPDATE t506_returns
           	  SET c506_ref_id = p_txn_id
           	    , c901_ref_type = DECODE(p_source,50186,9110,50182,4127,p_source) -- Inhouse Loaner(50186)->IHLN(9110),Loaner(50182)->PL(4127)
           	    , c101_user_id = v_loantoid
           	    , c506_last_updated_by = p_userid
           	    , c506_last_updated_date = SYSDATE
            WHERE c506_rma_id = v_return_id
              AND c506_void_fl IS NULL;
              
           FOR v_cur IN c_all_data
		   LOOP
           		UPDATE t507_returns_item
					SET C507_CONTROL_NUMBER     = v_cur.CNUM
					  WHERE C205_PART_NUMBER_ID = v_cur.PNUM
					    AND C507_ITEM_QTY       = v_cur.QTY;
		   END LOOP;
           p_return_id := v_return_id;   
	END  gm_sav_ihloaner_item_return ;
  /*************************************************************************
   * Description : This procedure is used to rollback IHLN to pending control status
   * Author      : Gopinathan
   **************************************************************************/
   PROCEDURE gm_sav_ihloaner_item_rollback(
   	  p_txn_id          IN   t907_shipping_info.c907_ref_id%TYPE,
      p_user_id          IN   t907_shipping_info.c907_last_updated_by%TYPE
   )
   AS
   	v_txn_id 		t907_shipping_info.c525_product_request_id%TYPE;
   BEGIN
	        BEGIN   
			     SELECT TO_CHAR(t907.c525_product_request_id) INTO v_txn_id 
				   FROM t907_shipping_info t907,t504_consignment t504
				  WHERE t907.c907_ref_id = p_txn_id
				    AND t907.c907_ref_id = t504.c504_consignment_id
		            AND t907.c525_product_request_id = t504.c504_ref_id
		            AND t504.c504_type = 9110 -- ihln
		            AND t504.c504_void_fl IS NULL
				    AND t907.c901_source   = 50186 --Inhouser Loaners - source
				    AND t907.c907_void_fl IS NULL FOR UPDATE;
				EXCEPTION
					WHEN NO_DATA_FOUND
					THEN
						RETURN; -- Inhouser Loaner set
			END; 
			
				--rollback shipping info
				UPDATE t907_shipping_info
				   SET c907_status_fl = 15	 -- pending Control
					 , c907_last_updated_date = SYSDATE
					 , c907_last_updated_by = p_user_id
				 WHERE c907_ref_id = p_txn_id
				   AND c901_source   = 50186 --Inhouser Loaners - source
				   AND c907_void_fl IS NULL;
				
				--rollback IHLN to pending control status
				UPDATE t504_consignment
				   SET c504_status_fl 	= '2'
				     , C504_SHIP_REQ_FL = 'Y'
				     , c504_verify_fl 	= NULL
				     , c504_verified_by = NULL
	  				 , c504_verified_date = NULL
	  				 , c504_last_updated_by = p_user_id
	  				 , c504_last_updated_date = SYSDATE
	             WHERE c504_consignment_id = p_txn_id
	  			   AND c504_void_fl IS NULL;
	  			   
   END gm_sav_ihloaner_item_rollback;
   
    /*****************************************************************
    * Description	: Procedure to Process  voiding the Inhouse Transactions from Reconfigure Scrren
    * Author		: 
    ******************************************************************/
	PROCEDURE gm_sav_ihbo_map (
		p_trans_id	 IN		t412_inhouse_transactions.c412_inhouse_trans_id%TYPE,	
		p_ref_type   IN 	t412_inhouse_transactions.c901_ref_type%TYPE,
		p_ref_id     IN 	t412_inhouse_transactions.c412_ref_id%TYPE,
		p_userId     IN     t102_user_login.c102_user_login_id%TYPE
	)
	AS
		v_ref_id   		t412_inhouse_transactions.c412_inhouse_trans_id%TYPE;
		v_ref_qty       T413_INHOUSE_TRANS_ITEMS.c413_item_qty%TYPE;
		v_cnt           NUMBER;		
		
		CURSOR qty_details_cur IS
			   SELECT t413.c205_part_number_id partnum , t413.c413_item_qty itemQty
                 FROM T413_INHOUSE_TRANS_ITEMS t413
                WHERE t413.C412_INHOUSE_TRANS_ID = p_trans_id
                  AND t413.c413_void_fl IS NULL;        			
		BEGIN			 	 		     
		   	  FOR v_cur_req_det IN qty_details_cur
			  LOOP  -- Loop each child request details      			
				v_ref_qty   := v_cur_req_det.itemQty;							   			 
        		 	IF (v_ref_qty >0) THEN -- UPDATING THE VOIDED QTY TO THE MAIN REQUEST IHRQ
	         			 UPDATE T413_INHOUSE_TRANS_ITEMS
	       					SET C413_ITEM_QTY = NVL (C413_ITEM_QTY, 0) + v_ref_qty   
	       					  , c413_last_updated_date = SYSDATE
					          , c413_last_updated_by   = p_userid 
	      				  WHERE C412_INHOUSE_TRANS_ID  = p_ref_id
	         				AND C205_PART_NUMBER_ID    = v_cur_req_det.partnum;
        		 	END IF;
    		  END LOOP;
      -- UPDATING THE IHRQ STATUS TO UN APPROVED AFTER VOIDING THE FGIH/BLIH TRANSACTION
      		   SELECT C412_INHOUSE_TRANS_ID
      		     INTO v_ref_id
      		     FROM t412_inhouse_transactions
      		   WHERE  C412_INHOUSE_TRANS_ID  = p_ref_id
      		     AND  C412_void_fl IS NULL
      		     FOR UPDATE;
      		     
               UPDATE t412_inhouse_transactions 
    		  	 SET c412_status_fl = '3' -- REVIEWED 
    		  	   , c412_last_updated_date = SYSDATE
				   , c412_last_updated_by   = p_userid
			   WHERE C412_INHOUSE_TRANS_ID  = p_ref_id
			   	 AND C412_void_fl IS NULL; 	
        
	END gm_sav_ihbo_map;
	
	 /*****************************************************************
    * Description	: Procedure to Process the Inhouse Transactions
    * Author		: 
    ******************************************************************/
	PROCEDURE gm_sav_void_ihbo_req (
		p_trans_id	 IN		t412_inhouse_transactions.c412_inhouse_trans_id%TYPE,	
		p_type       IN 	t412_inhouse_transactions.c412_type%TYPE,
		p_userId     IN     t102_user_login.c102_user_login_id%TYPE	  
	)
	AS
		v_ref_id   		t412_inhouse_transactions.c412_inhouse_trans_id%TYPE;
		v_count         NUMBER;
		v_cur_req_det   TYPES.cursor_type; -- To get t413 details		
		
		CURSOR transaction_details_cur IS
			  	SELECT C412_INHOUSE_TRANS_ID transID
		  		  FROM t412_inhouse_transactions 
		 		 WHERE c412_ref_id = p_trans_id
		   		   AND c412_void_fl IS NULL 
		           AND c412_status_fl = 2;   		
	BEGIN
			SELECT count(1) 
		      INTO v_count 
		      FROM t412_inhouse_transactions 
		     WHERE c412_ref_id = p_trans_id
		       AND c412_status_fl IN ('3','4') -- Pending Verification
		       AND c412_void_fl IS NULL ;
		  IF v_count <> 0 THEN
				GM_RAISE_APPLICATION_ERROR('-20999','205','');
		  END IF;		
		  FOR v_cur_req_det IN transaction_details_cur
			  LOOP   
	 -- UPDATING THE VOID FLAG AS Y TO THE ASSOCIATED INHOUSE TRANSACTION FOR THE IHRQ TRANSACTION WHICH ARE IN PENDING CONTROLLED STATE
	 			SELECT C412_INHOUSE_TRANS_ID
      		     INTO v_ref_id
      		     FROM t412_inhouse_transactions
      		   WHERE  C412_INHOUSE_TRANS_ID  = v_cur_req_det.transID
      		     AND  C412_void_fl IS NULL
      		     FOR UPDATE;     
      		     
         		UPDATE t412_inhouse_transactions
       			   SET c412_void_fl = 'Y'  
       				 , c412_last_updated_by   = p_userId
       				 , c412_last_updated_date = SYSDATE
      		     WHERE C412_INHOUSE_TRANS_ID  = v_cur_req_det.transID
      		       AND c412_void_fl IS NULL; 
      		    
      		    UPDATE t413_inhouse_trans_items t413
			   	   SET t413.c413_void_fl = 'Y'
	       			 , c413_last_updated_date = SYSDATE
					 , c413_last_updated_by   = p_userid 			   	   
			     WHERE t413.c412_inhouse_trans_id = v_cur_req_det.transID;   
    		  END LOOP;		    		     		 
	END gm_sav_void_ihbo_req;
  /*******************************************************
   * Description : Procedure to added boih parts to IHLN
   * Author	  : Gopinathan
   *******************************************************/	  
PROCEDURE gm_sav_boih_txn_map(
    p_txnid     IN  T412_INHOUSE_TRANSACTIONS.C412_INHOUSE_TRANS_ID%TYPE,
    p_txntype   IN  T412_INHOUSE_TRANSACTIONS.C412_TYPE%TYPE,
    p_pnum      IN  t413_inhouse_trans_items.c205_part_number_id%TYPE,
    p_qty       IN  t413_inhouse_trans_items.c413_item_qty%TYPE,
    p_userid    IN  T412_INHOUSE_TRANSACTIONS.C412_CREATED_BY%TYPE
)
AS
  v_ref_id       t525_product_request.c525_product_request_id%TYPE;
  v_ref_type     T412_INHOUSE_TRANSACTIONS.c901_ref_type%TYPE;
  v_txn_id       VARCHAR2(20);
BEGIN
  -- Get the Parent Request id which is the t525 id and type 
  BEGIN
    SELECT t412.C412_REF_ID, t412.C901_REF_TYPE
      INTO v_ref_id, v_ref_type
      FROM t412_inhouse_transactions t412
     WHERE t412.c412_inhouse_trans_id= p_txnid
        AND t412.c412_status_fl = 4 -- Verified
        AND t412.c412_void_fl is null
        AND t412.c412_type = p_txntype;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    RETURN;
  END;

  -- If the Parent Request id is Back order, get its Parent id which is the t525 id and type
  IF (v_ref_type = 1006483) --It may be IHRQ - 1006483
  THEN
  	--gm_pkg_op_ihloaner_item_txn.gm_sav_released_ihbo_req(v_ref_id,v_ref_type,p_userid);
    BEGIN
      SELECT t412.C412_REF_ID, t412.C901_REF_TYPE
        INTO v_ref_id, v_ref_type
        FROM t412_inhouse_transactions t412
       WHERE t412.c412_inhouse_trans_id= v_ref_id
         AND t412.c412_void_fl IS NULL;
       
    EXCEPTION WHEN NO_DATA_FOUND THEN
      RETURN;
    END;
  END IF;

  IF (v_ref_type = 4119 OR v_ref_type = 4127) 
  THEN -- Call only for Inhouse Loaner or Product loaner
        IF (p_qty>0)
        THEN
         gm_pkg_sm_loaner_item_alloc.gm_chk_inhouse_txn(v_ref_id,v_ref_type,p_pnum,9110, p_userid, p_qty, v_txn_id);-- 9110>IHLN  
        END IF;
  END IF;

END gm_sav_boih_txn_map;    
   /*******************************************************
   * Description : Procedure to void event item transactions
   * Author	  : Gopinathan
   *******************************************************/	
   PROCEDURE gm_sav_void_event_item_txns (
		p_caseinfoid   IN	t7100_case_information.c7100_case_info_id%TYPE
	  , p_userid	   IN	t7100_case_information.c7100_created_by%TYPE
   )
   AS
   	v_ref_id 		t525_product_request.c525_product_request_id%TYPE;
   	v_ref_type 		t525_product_request.c901_request_for%TYPE;
   	v_ihrq_id 		t412_inhouse_transactions.c412_inhouse_trans_id%TYPE;
   	v_ihrq_type 	t412_inhouse_transactions.c412_type%TYPE;
   	v_txn_id		t504_consignment.c504_consignment_id%TYPE;
   	v_status		t504_consignment.c504_status_fl%TYPE;
   	v_cur           TYPES.cursor_type;
   BEGIN
	   BEGIN
	         SELECT t525.c525_product_request_id, t525.c901_request_for
	           INTO v_ref_id, v_ref_type
	           FROM t525_product_request t525, t526_product_request_detail t526, t7104_case_set_information t7104
	          WHERE t525.c525_product_request_id =  t526.c525_product_request_id
                AND t526.c526_product_request_detail_id = t7104.c526_product_request_detail_id
	            AND t7104.c7100_case_info_id = p_caseinfoid
	            AND t526.c526_void_fl IS NULL
	            AND t526.c205_part_number_id IS NOT NULL -- To avoid set requests
	            AND t7104.c7104_void_fl  IS NULL
           GROUP BY t525.c525_product_request_id, t525.c901_request_for;
 		EXCEPTION WHEN NO_DATA_FOUND THEN
      		RETURN;
       END;
       
	   BEGIN
	         SELECT t412.c412_inhouse_trans_id, t412.c412_type
	           INTO v_ihrq_id, v_ihrq_type
	           FROM t412_inhouse_transactions t412
	          WHERE t412.c412_ref_id = v_ref_id
	            AND t412.c412_void_fl IS NULL
	            AND t412.c412_type = DECODE(v_ref_type,4119,1006483,v_ref_type); -- IHRQ(1006483)
	    EXCEPTION WHEN NO_DATA_FOUND THEN
	    	v_ihrq_id		:= NULL;
	    	v_ihrq_type		:= NULL;
	   END;
	   
	   IF v_ihrq_id IS NOT NULL THEN
		   --voiding IHRQ linked txns
		   gm_sav_void_mapped_txns(v_ihrq_id,v_ihrq_type,p_userid);
	   END IF;
	   --voiding request(t525) linked txns
	   gm_sav_void_mapped_txns(v_ref_id,v_ref_type,p_userid);
	   
	   
	   OPEN v_cur 
    		FOR
	  			SELECT c504_consignment_id, c504_status_fl
			      FROM t504_consignment 
			     WHERE c504_ref_id = v_ref_id
			       AND c901_ref_type = v_ref_type
			       AND c504_void_fl IS NULL;
	   LOOP
	   FETCH v_cur
	      INTO v_txn_id, v_status;
	
	   EXIT WHEN v_cur%NOTFOUND;	   
	   	   IF v_status IN ('4') THEN
	   	  		GM_RAISE_APPLICATION_ERROR('-20999','206',v_txn_id); 
	   	   END IF;
                
	        UPDATE t504_consignment
			   SET c504_void_fl = 'Y'
				 , c504_last_updated_by = p_userid
				 , c504_last_updated_date = SYSDATE
			 WHERE c504_consignment_id = v_txn_id
			   AND c504_void_fl IS NULL;
		 
	   END LOOP;
	   
   END gm_sav_void_event_item_txns;
   /*******************************************************
   * Description : Procedure to void event item transactions
   * Author	  : Gopinathan
   *******************************************************/	
   PROCEDURE gm_sav_void_mapped_txns (
		p_ref_id   IN	t412_inhouse_transactions.c412_ref_id%TYPE
	  , p_ref_type IN	t412_inhouse_transactions.c901_ref_type%TYPE	
	  , p_userid   IN	t412_inhouse_transactions.c412_last_updated_by%TYPE
   )
   AS
   		CURSOR v_cur_txn 
   		    IS 
    	     SELECT t412.c412_inhouse_trans_id txnid
	           FROM t412_inhouse_transactions t412
	          WHERE t412.c412_ref_id = p_ref_id
	            AND t412.c901_ref_type = p_ref_type
	            AND t412.c412_void_fl IS NULL;    		
   BEGIN
       
	   FOR v_cur IN v_cur_txn
	   LOOP
       		gm_pkg_common_cancel_op.gm_op_sav_void_inhousetxn(v_cur.txnid,p_userid);
       END LOOP;
       
   END gm_sav_void_mapped_txns;
   
   /*
    *	Description: Function to fetch the Loan To from IHLN transactions
    *	Author	: Rajkumar J 
    */
   FUNCTION get_ih_loanto(
		p_ihtxnid		T504_CONSIGNMENT.C504_CONSIGNMENT_ID%TYPE
   ) RETURN VARCHAR2
   IS
   	v_loanto		t7103_schedule_info.c901_loan_to_org%TYPE;
   BEGIN
	   BEGIN
		   SELECT t7103.c901_loan_to_org INTO v_loanto
			   FROM t504_consignment t504, t525_product_request t525, t526_product_request_detail t526
			  , t7104_case_set_information t7104, t7103_schedule_info t7103 , t7100_case_information t7100 
			  WHERE t504.c504_consignment_id             = p_ihtxnid
			    AND t504.c504_type                       = '9110'
			    AND t525.c525_product_request_id         = t504.c504_ref_id
			    AND t526.c525_product_request_id         = t525.c525_product_request_id
			    AND t7104.c526_product_request_detail_id = t526.c526_product_request_detail_id
			    AND t7103.c7100_case_info_id             = t7104.c7100_case_info_id 
			    AND t7103.c7100_case_info_id = t7100.c7100_case_info_id
			   	AND t7100.c901_case_status != 19526 --  19526 - Rescheduled
			    AND t7100.c7100_void_fl IS NULL 
			    AND t7100.c901_type = 1006504 --Event
			    AND t504.c504_void_fl                   IS NULL
			    AND t525.c525_void_fl                   IS NULL
			    AND t526.c526_void_fl                   IS NULL
			    AND t7104.c7104_void_fl                 IS NULL
			    AND ROWNUM                               = 1;
		EXCEPTION WHEN NO_DATA_FOUND
		THEN
				v_loanto := NULL;
		END;	   
		
		RETURN v_loanto;
   END get_ih_loanto;
   /*
    *	Description: Function to fetch the Loan To Id from IHLN transactions
    *	Author	: Rajkumar J 
    */
   FUNCTION get_ih_loantoid(
		p_ihtxnid		T504_CONSIGNMENT.C504_CONSIGNMENT_ID%TYPE
   )  RETURN VARCHAR2
   IS
   	v_loantoid		t7103_schedule_info.c101_loan_to_id%TYPE;
   BEGIN
	   BEGIN
		   SELECT t7103.c101_loan_to_id INTO v_loantoid
			   FROM t504_consignment t504, t525_product_request t525, t526_product_request_detail t526
			  , t7104_case_set_information t7104, t7103_schedule_info t7103 , t7100_case_information t7100 
			  WHERE t504.c504_consignment_id             = p_ihtxnid
			    AND t504.c504_type                       = '9110'
			    AND t525.c525_product_request_id         = t504.c504_ref_id
			    AND t526.c525_product_request_id         = t525.c525_product_request_id
			    AND t7104.c526_product_request_detail_id = t526.c526_product_request_detail_id
			    AND t7103.c7100_case_info_id             = t7104.c7100_case_info_id 
			    AND t7103.c7100_case_info_id = t7100.c7100_case_info_id
			    AND t7100.c901_case_status != 19526 --  19526 - Rescheduled
			    AND t7100.c7100_void_fl IS NULL 
			    AND t7100.c901_type = 1006504--Event
			    AND t504.c504_void_fl                   IS NULL
			    AND t525.c525_void_fl                   IS NULL
			    AND t526.c526_void_fl                   IS NULL
			    AND t7104.c7104_void_fl                 IS NULL
			    AND ROWNUM                               = 1;
		EXCEPTION WHEN NO_DATA_FOUND
		THEN
				v_loantoid := NULL;
		END;
		
		RETURN v_loantoid;
   END get_ih_loantoid;
      /*
    * Description: Function to Function the Loaned Qty for the Loaned to Id 
    * Author:	Rajkumar J
    */
   FUNCTION get_csg_part_qty_for_dist(
      		p_lontoid		T506_RETURNS.C101_USER_ID%TYPE,
      		p_pnum			T507_RETURNS_ITEM.C205_PART_NUMBER_ID%TYPE
   ) RETURN NUMBER
   IS
   	V_QTY	NUMBER := 0;
    v_company_id Number;
   BEGIN
   	--Getting company id from context.   
    SELECT get_compid_frm_cntx()
	  INTO v_company_id 
	  FROM DUAL;

	    SELECT NVL (SUM (t507.c507_item_qty), 0) INTO v_qty
		   FROM t506_returns t506, t507_returns_item t507
		  WHERE t506.c901_ref_type       = '9110'
		    AND t506.c506_status_fl      = 0
		    AND t506.c101_user_id        = p_lontoid
		    AND t507.c506_rma_id         = t506.c506_rma_id
		    AND t507.c205_part_number_id = p_pnum
		    AND T507.C507_STATUS_FL      = 'Q'
        AND t506.C1900_COMPANY_ID  = v_company_id
		    AND t506.c506_void_fl       IS NULL;
		RETURN v_qty;
   END get_csg_part_qty_for_dist;
	/************************************************************************
		 * Purpose: Procedure to update item request to close based on shippedout qty
		 * Author : Gopinathan
	************************************************************************/
	PROCEDURE gm_sav_update_item_req_status (
	        p_requestid IN t525_product_request.c525_product_request_id%TYPE,
	        p_refid     IN t907_shipping_info.c907_ref_id%TYPE,
	        p_type      IN t504_consignment.c504_type%TYPE,
	        p_userid    IN t526_product_request_detail.c526_last_updated_by%TYPE)
	AS
	    CURSOR v_cur_ship
	    IS
	         SELECT t505.c205_part_number_id pnum, SUM (t505.c505_item_qty) shipping_qty
	           FROM t504_consignment t504, t505_item_consignment t505
	          WHERE t504.c504_consignment_id = t505.c504_consignment_id
	            AND t504.c504_consignment_id = p_refid
	            AND t504.c504_type           = p_type
	            AND t505.c505_void_fl       IS NULL
	            AND t504.c504_void_fl       IS NULL
	       GROUP BY t505.c205_part_number_id;
	    v_shipped_qty NUMBER;
	    v_total_qty   NUMBER;
	BEGIN
	    FOR v_cur IN v_cur_ship
	    LOOP
	        BEGIN
	             SELECT SUM (t505.c505_item_qty)
	               INTO v_shipped_qty --getting previously shipped out IHLN qty
	               FROM t504_consignment t504, t505_item_consignment t505, t907_shipping_info t907
	              WHERE t504.c504_consignment_id = t505.c504_consignment_id
	                AND t504.c504_consignment_id = t907.c907_ref_id
	                AND t907.c907_status_fl     IN ('40') --shipped out
	                AND t907.c907_void_fl       IS NULL
	                AND t504.c504_ref_id         = p_requestid
	                AND t505.c205_part_number_id = v_cur.pnum
	                AND t504.c504_type           = p_type
	                AND t505.c505_void_fl       IS NULL
	                AND t504.c504_void_fl       IS NULL
	           GROUP BY t505.c205_part_number_id;
	        EXCEPTION
	        WHEN NO_DATA_FOUND THEN
	            v_shipped_qty := 0;
	        END;
	        v_total_qty := (v_cur.shipping_qty + v_shipped_qty) ;
	         UPDATE t526_product_request_detail
	        SET c526_status_fl              = '30' --closed
	          , c526_last_updated_by        = p_userid, c526_last_updated_date = SYSDATE
	          WHERE c525_product_request_id = p_requestid
	            AND c205_part_number_id    IS NOT NULL
	            AND c526_qty_requested      = v_total_qty --checking request qty with total shipped out qty.
	            AND c205_part_number_id     = v_cur.pnum
	            AND c526_void_fl           IS NULL;
	    END LOOP;
	    --update master request in t525
	    gm_pkg_op_loaner.gm_sav_update_req_status (p_requestid, p_userid) ;
	END gm_sav_update_item_req_status;  
	/************************************************************************
		 * Purpose: Procedure to update item request ship date 
		 *          if new ship date is with 4 days and statu is open then call allocation
		 * Author : Gopinathan
	************************************************************************/
	PROCEDURE gm_sav_item_req_ship_date (
	        p_requestid  IN t525_product_request.c525_product_request_id%TYPE,
	        p_req_dtl_id IN t526_product_request_detail.c526_product_request_detail_id%TYPE,
	        p_status 	 IN t526_product_request_detail.c526_status_fl%TYPE,
	        p_req_type   IN t526_product_request_detail.c901_request_type%TYPE,
	        p_pnum       IN t526_product_request_detail.c205_part_number_id%TYPE,
	        p_ship_date  IN t526_product_request_detail.c526_required_date%TYPE,
	        p_userid     IN t526_product_request_detail.c526_last_updated_by%TYPE)
	AS
	    CURSOR v_cur_ship
	    IS
	         SELECT T504.C504_Consignment_Id ihln
	           FROM t504_consignment t504, t505_item_consignment t505, t907_shipping_info t907
	          WHERE T504.C504_Consignment_Id = T505.C504_Consignment_Id
	            AND T504.C504_Consignment_Id = T907.C907_Ref_Id
	            AND T907.C907_Status_Fl NOT IN ('40') --shipped out
	            AND t907.c907_void_fl       IS NULL
	            AND T504.C504_Ref_Id         = p_requestid
	            AND T505.C205_Part_Number_Id = p_pnum
	            AND t504.c504_type           = 9110
	            AND t505.c505_void_fl       IS NULL
	            AND T504.C504_Void_Fl       IS NULL
	       GROUP BY T504.C504_Consignment_Id, t505.c205_part_number_id;
	BEGIN
		FOR v_cur IN v_cur_ship
		LOOP
			/* update all IHLN plan ship date which is having the request part and IHLN is not yet shipped.*/
		     UPDATE t907_shipping_info
		        SET c907_ship_to_dt = p_ship_date
		          , c907_last_updated_by = p_userid
		          , c907_last_updated_date  = SYSDATE
		      WHERE c907_ref_id         = v_cur.ihln
		        AND c525_product_request_id = p_requestid
		        AND c907_ship_to_dt <> p_ship_date
		        AND c901_source = DECODE(p_req_type,'4119','50186',p_req_type)
		        AND c907_status_fl NOT IN ('40') --closed
		        AND C907_Void_Fl       IS NULL;
		END LOOP;
		
		/*update the request plan ship date */
	     UPDATE t526_product_request_detail
	        SET c526_required_date   = p_ship_date
	          , c526_last_updated_by = p_userid
	          , c526_last_updated_date = SYSDATE
	      WHERE c526_product_request_detail_id = p_req_dtl_id
	        AND c205_part_number_id           IS NOT NULL
	        AND c526_void_fl                  IS NULL;
	
		IF p_status IN ('10') THEN
			/* If the request required date is within 4days then this prc will allocate the items to the request.*/
			gm_pkg_sm_loaner_item_alloc.gm_sav_loaner_item_req(p_req_dtl_id,p_req_type,p_userid);
		END IF;
	
	END gm_sav_item_req_ship_date;
	/************************************************************************
		 * Purpose: Procedure to verify IHLN
		 * Author : Gopinathan
	************************************************************************/
	PROCEDURE gm_sav_verify_ihln(
		p_ihlnid    IN t504_consignment.c504_consignment_id%TYPE,
	    p_type      IN t504_consignment.c504_type%TYPE,
	    p_userid    IN t504_consignment.c504_last_updated_by%TYPE
		)	
	AS
	    CURSOR v_cur_ihln
	    IS
	         SELECT t505.c205_part_number_id pnum, t505.c505_item_qty qty,c505_control_number cnum
	           FROM t504_consignment t504, t505_item_consignment t505
	          WHERE t504.c504_consignment_id = t505.c504_consignment_id
	            AND t504.c504_consignment_id = p_ihlnid
	            AND t504.c504_type           = p_type
	            AND t505.c505_void_fl       IS NULL
	            AND t504.c504_void_fl       IS NULL;
		v_inputstr VARCHAR2(4000);	            
	BEGIN
		FOR v_cur IN v_cur_ihln
		LOOP
			v_inputstr := v_inputstr || v_cur.pnum ||','|| v_cur.qty ||','|| v_cur.cnum ||',|';
		END LOOP;

		IF v_inputstr IS NOT NULL THEN
			/*call common verification procedure to verify IHLN*/
			gm_pkg_op_item_control_txn.gm_sav_item_verify_main (p_ihlnid,p_type,v_inputstr,93345,p_userid);
		END IF;
		
	END gm_sav_verify_ihln;	
END gm_pkg_op_ihloaner_item_txn;
/