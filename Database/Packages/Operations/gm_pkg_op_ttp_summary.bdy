/* Formatted on 2009/11/24 19:18 (Formatter Plus v4.8.0) */
-- @"c:\database\packages\operations\gm_pkg_op_ttp_summary.bdy"

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_ttp_summary
IS
--
  /*******************************************************
   * Description : Main Procedure to generate ttp monthly summary cross tab report
   * Author 	 : Joe P Kumar
   *******************************************************/
--
	PROCEDURE gm_fc_fch_ttp_mon_summary (
		p_ttpid 		   IN		t4050_ttp.c4050_ttp_id%TYPE
	  , p_monyear		   IN		VARCHAR2
	  , p_demandsheetids   IN		VARCHAR2
	  , p_inventoryid	   IN OUT	t250_inventory_lock.c250_inventory_lock_id%TYPE
	  , p_forecastmonth    IN		t4052_ttp_detail.c4052_forecast_period%TYPE
	  , p_outfcheader	   OUT		TYPES.cursor_type
	  , p_outfcdetails	   OUT		TYPES.cursor_type
	  , p_outinvheader	   OUT		TYPES.cursor_type
	  , p_outinvdetails    OUT		TYPES.cursor_type
	  , p_outtotalreqqty   OUT		TYPES.cursor_type
	  , p_outtotordqty	   OUT		TYPES.cursor_type
	  , p_outunitprice	   OUT		TYPES.cursor_type
	  , p_sheetname 	   OUT		TYPES.cursor_type
	  , p_status		   OUT		VARCHAR2
	  , p_finaldate 	   OUT		VARCHAR2
	  , p_ttp_type		   OUT		VARCHAR2
	  , p_parentsubdtl	   OUT		TYPES.cursor_type
	  , p_ds_status 	   OUT		VARCHAR2
	)
	AS
		v_ttp_detail_id t4052_ttp_detail.c4052_ttp_detail_id%TYPE;
		v_demand_sheetids VARCHAR2 (5000);
		v_string	   VARCHAR2 (100);
		v_inventoryid  t250_inventory_lock.c250_inventory_lock_id%TYPE;
		v_forecast_period t4052_ttp_detail.c4052_forecast_period%TYPE;
		v_fc_start_date DATE;

		CURSOR cur_demandsheetids
		IS
			SELECT c4040_demand_sheet_id ID
			  FROM t4040_demand_sheet
			 WHERE c4052_ttp_detail_id = v_ttp_detail_id;
	BEGIN
		SELECT c901_ttp_type
		  INTO p_ttp_type
		  FROM t4050_ttp
		 WHERE c4050_ttp_id = p_ttpid;

		IF (p_demandsheetids IS NULL OR p_monyear <> TO_CHAR (SYSDATE, 'MM/YYYY'))
		THEN
			v_demand_sheetids := 0;

			-- Fetch TTP Summary information
			SELECT c4052_ttp_detail_id, c901_status, c4052_forecast_period
			  INTO v_ttp_detail_id, p_status, v_forecast_period
			  FROM t4052_ttp_detail
			 WHERE c4050_ttp_id = p_ttpid AND TO_CHAR (c4052_ttp_link_date, 'MM/YYYY') = p_monyear;

			-- Fetch Inventory information
			SELECT c250_inventory_lock_id
			  INTO v_inventoryid
			  FROM t252_inventory_lock_ref
			 WHERE c252_ref_id = v_ttp_detail_id AND c901_ref_type = 20480;

			p_inventoryid := v_inventoryid;

			FOR cur_val IN cur_demandsheetids
			LOOP
				v_demand_sheetids := cur_val.ID || ',' || v_demand_sheetids;
			END LOOP;
		--
		ELSE
			v_demand_sheetids := p_demandsheetids;
			v_forecast_period := p_forecastmonth;
			p_status	:= NULL;
			v_inventoryid := p_inventoryid;
		END IF;

		my_context.set_my_inlist_ctx (v_demand_sheetids);

		SELECT DECODE (COUNT (1), 0, 'false', 'true')
		  INTO p_ds_status
		  FROM t4040_demand_sheet t4040
		 WHERE c4040_demand_sheet_id IN (SELECT *
										   FROM v_in_list) AND t4040.c901_status <> 50551;

		-- Below code to fecth total order qty
		gm_fc_fch_totreqqty_ttp (v_ttp_detail_id, p_outtotordqty);
		-- Below procedure called to fetch ttp
		gm_fc_fch_demandsheet_name (p_sheetname);
		gm_fc_fch_fcheader (v_forecast_period, p_outfcheader);
		gm_fc_fch_fcdetails (v_forecast_period, p_outfcdetails);
		gm_fc_fch_invheader (p_outinvheader);
		gm_fc_fch_invdetails (v_inventoryid, p_outinvdetails);
		-- Code to fetch unit cost
		gm_fc_fch_unitprice (v_ttp_detail_id, p_outunitprice);
		-- Below sql to fetch total required qty
		gm_fc_fch_totreqqty (v_forecast_period, v_inventoryid, p_outtotalreqqty);
		gm_ld_parent_sub_parts (p_parentsubdtl);

		SELECT TO_CHAR (MAX (c4042_period), 'Mon YY')	-- '8/1/2007'
		  INTO p_finaldate
		  FROM t4042_demand_sheet_detail t4042
		 WHERE t4042.c901_type = 50563 AND t4042.c4040_demand_sheet_id IN (SELECT *
																			 FROM v_in_list);
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			raise_application_error (-20900, '');	-- No Demand Sheets have been mapped
	END gm_fc_fch_ttp_mon_summary;

--
  /*******************************************************
   * Description : Procedure to fetch Demand Sheet information
   *			   header and footer information
   *******************************************************/
--
	PROCEDURE gm_fc_fch_demandsheet_name (
		p_sheetname   OUT	TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_sheetname
		 FOR
			 SELECT   t4040.c4020_demand_master_id dmid, t4040.c4040_demand_sheet_id ID, t4020.c4020_demand_nm NAME
				 FROM t4020_demand_master t4020, t4040_demand_sheet t4040
				WHERE t4040.c4040_demand_sheet_id IN (SELECT *
														FROM v_in_list)
				  AND t4020.c4020_demand_master_id = t4040.c4020_demand_master_id
			 ORDER BY NAME;
	END gm_fc_fch_demandsheet_name;

--
  /*******************************************************
   * Description : Procedure to generate header for forecast
   * Author 	 : Joe P Kumar
   *******************************************************/
--
	PROCEDURE gm_fc_fch_fcheader (
		p_forecastmonth   IN	   t4052_ttp_detail.c4052_forecast_period%TYPE
	  , p_outfcheader	  OUT	   TYPES.cursor_type
	)
	AS
		v_fc_start_date DATE;
	BEGIN
		SELECT MIN (c4042_period)	-- '8/1/2007'
		  INTO v_fc_start_date
		  FROM t4042_demand_sheet_detail t4042
		 WHERE t4042.c901_type = 50563 AND t4042.c4040_demand_sheet_id IN (SELECT *
																			 FROM v_in_list);

		OPEN p_outfcheader
		 FOR
			 SELECT   period, seq_no, c4042_period
				 FROM (SELECT 'Description' period, 0 seq_no, TO_DATE ('01/01/2000', 'MM/DD/YYYY') c4042_period
						 FROM DUAL
					   UNION ALL
					   SELECT DISTINCT DECODE (t4042.c901_type
											 , 50563, TO_CHAR (c4042_period, 'Mon YY')
											 , get_code_name (t4042.c901_type)
											  ) period
									 , t901.c901_code_seq_no seq_no, c4042_period
								  FROM t4042_demand_sheet_detail t4042, t901_code_lookup t901
								 WHERE t4042.c901_type = t901.c901_code_id
								   AND t4042.c901_type IN
										   (50563, 50567, 50568, 50569)   -- Forecast , Pending Back Order, Pending Back Log, Pending Consign and Ship
								   AND t4042.c4040_demand_sheet_id IN (SELECT *
																		 FROM v_in_list)
								   AND t4042.c4042_period BETWEEN v_fc_start_date
															  AND ADD_MONTHS (v_fc_start_date, (p_forecastmonth - 1)))
			 ORDER BY seq_no, c4042_period;
	END gm_fc_fch_fcheader;

--
  /*******************************************************
   * Description : Procedure to generate details seection for forecast
   * Author 	 : Joe P Kumar
   *******************************************************/
--
	PROCEDURE gm_fc_fch_fcdetails (
		p_forecastmonth   IN	   t4052_ttp_detail.c4052_forecast_period%TYPE
	  , p_outfcdetails	  OUT	   TYPES.cursor_type
	)
	AS
		v_fc_start_date DATE;
	BEGIN
		SELECT MIN (c4042_period)	-- '8/1/2007'
		  INTO v_fc_start_date
		  FROM t4042_demand_sheet_detail t4042
		 WHERE t4042.c901_type = 50563 AND t4042.c4040_demand_sheet_id IN (SELECT *
																			 FROM v_in_list);

		OPEN p_outfcdetails
		 FOR
			 SELECT   DECODE (t4042.c205_part_number_id
							, NULL, DECODE (t205.c205_product_family
										  , NULL, DECODE (GROUPING_ID (t205.c205_product_family
																	 , t4042.c205_part_number_id
																	  )
														, 3, 'Total'
														, 'Group Missing'
														 )
										  , t205.c205_product_family
										   )
							, t4042.c205_part_number_id
							 ) num
					, DECODE (t4042.c205_part_number_id
							, NULL, DECODE (t205.c205_product_family
										  , NULL, DECODE (GROUPING_ID (t205.c205_product_family
																	 , t4042.c205_part_number_id
																	  )
														, 3, 'Total'
														, 'Group Missing'
														 )
										  , get_code_name (t205.c205_product_family)
										   )
							, t4042.c205_part_number_id
							 ) pnum
					, DECODE (t4042.c901_type
							, 50563, TO_CHAR (c4042_period, 'Mon YY')
							, get_code_name (t4042.c901_type)
							 ) period
					, SUM (DECODE (refid, NULL, c4042_qty, 0)) qty
					--, SUM (c4042_qty) qty
					  ,GROUPING_ID (t205.c205_product_family, t4042.c205_part_number_id) grpid
					, NVL (c205_crossover_fl, 'N') crossoverfl
					, DECODE (t4042.c205_part_number_id
							, NULL, DECODE (t205.c205_product_family
										  , NULL, DECODE (GROUPING_ID (t205.c205_product_family
																	 , t4042.c205_part_number_id
																	  )
														, 3, 'Total'
														, 'Group Missing'
														 )
										  , get_code_name (t205.c205_product_family)
										   )
							, t205.c205_part_num_desc
							 ) description
				 FROM
					  -- Below code to fetch demand sheet based on par value
					  (SELECT t4042.c4040_demand_sheet_id, t4042.c205_part_number_id, t4042.c4042_period
							, t4042.c901_type
							, DECODE (t4042.c901_type
									, 50563, DECODE (parvalue.c205_part_number_id
												   , NULL, t4042.c4042_qty
												   , DECODE (parvalue.parperiod
														   , t4042.c4042_period, parvalue.parvalue
														   , 0
															)
													)
									, t4042.c4042_qty
									 ) c4042_qty
							, t4042.c4042_parent_ref_id refid
						 FROM t4042_demand_sheet_detail t4042
							, (SELECT *
								 FROM (SELECT	t4042.c4040_demand_sheet_id, t4042.c205_part_number_id
											  , MIN (t4042.c4042_period) parperiod
											  , SUM (DECODE (t4042.c901_type, 50563, t4042.c4042_qty, 0)) demandvalue
											  , SUM (DECODE (t4042.c901_type, 50566, t4042.c4042_qty, 0)) parvalue
										   FROM t4042_demand_sheet_detail t4042
										  WHERE t4042.c4040_demand_sheet_id IN (SELECT *
																				  FROM v_in_list)
											AND t4042.c901_type IN (50563, 50566)
											AND t4042.c4042_period BETWEEN v_fc_start_date
																	   AND ADD_MONTHS (v_fc_start_date
																					 , (p_forecastmonth - 1)
																					  )
									   GROUP BY t4042.c4040_demand_sheet_id, t4042.c205_part_number_id)
								WHERE parvalue > demandvalue) parvalue	 -- Above to fetch part which has value more than par
						WHERE t4042.c4040_demand_sheet_id IN (SELECT *
																FROM v_in_list)
						  AND t4042.c901_type IN (50563, 50567, 50568, 50569)
						  AND t4042.c4042_period <= ADD_MONTHS (v_fc_start_date, (p_forecastmonth - 1))
						  AND t4042.c4040_demand_sheet_id = parvalue.c4040_demand_sheet_id(+)
						  AND t4042.c205_part_number_id = parvalue.c205_part_number_id(+)) t4042
					, t205_part_number t205
				WHERE t4042.c205_part_number_id = t205.c205_part_number_id
			 GROUP BY c4042_period
					, t4042.c901_type
					, ROLLUP (t205.c205_product_family
							, (t4042.c205_part_number_id, t205.c205_part_num_desc, t205.c205_crossover_fl))
			 ORDER BY t205.c205_product_family, grpid, num, period;
	END gm_fc_fch_fcdetails;

--
  /*******************************************************
   * Description : Procedure to generate header seection for inventory
   * Author 	 : Joe P Kumar
   *******************************************************/
--
	PROCEDURE gm_fc_fch_invheader (
		p_outinvheader	 OUT   TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_outinvheader
		 FOR
			 SELECT   c901_code_nm period, c901_code_seq_no seq
				 FROM t901_code_lookup
				WHERE c901_code_id IN (20464, 20462, 20460, 20477, 20454, 20455)
			 UNION
			 SELECT   'Total Inventory' period, 100 seq
				 FROM DUAL
			 ORDER BY seq;
	END gm_fc_fch_invheader;

--
  /*******************************************************
   * Description : Procedure to generate details section for inventory
   * Author 	 : Joe P Kumar
   *******************************************************/
--
	PROCEDURE gm_fc_fch_invdetails (
		p_inventoryid	  IN	   t250_inventory_lock.c250_inventory_lock_id%TYPE
	  , p_outinvdetails   OUT	   TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_outinvdetails
		 FOR
			 SELECT   num, pnum, period, SUM (qty) qty, grpid, pfamily
				 FROM (SELECT	DECODE (t251.c205_part_number_id
									  , NULL, DECODE (t205.c205_product_family
													, NULL, DECODE (GROUPING_ID (t205.c205_product_family
																			   , t251.c205_part_number_id
																				)
																  , 3, 'Total'
																  , 'Group Missing'
																   )
													, t205.c205_product_family
													 )
									  , t251.c205_part_number_id
									   ) num
							  , DECODE (t251.c205_part_number_id
									  , NULL, DECODE (t205.c205_product_family
													, NULL, DECODE (GROUPING_ID (t205.c205_product_family
																			   , t251.c205_part_number_id
																				)
																  , 3, 'Total'
																  , 'Group Missing'
																   )
													, get_code_name (t205.c205_product_family)
													 )
									  , t251.c205_part_number_id
									   ) pnum
							  , DECODE (t251.c901_type
									  , NULL, 'Total Inventory'
									  , get_code_name (t251.c901_type)
									   ) period, SUM (t251.c251_qty) qty
							  , GROUPING_ID (t205.c205_product_family, t251.c205_part_number_id) grpid
							  , t205.c205_product_family pfamily
						   FROM t251_inventory_lock_detail t251
							  , (SELECT DISTINCT t4042.c205_part_number_id
											FROM t4042_demand_sheet_detail t4042
										   WHERE t4042.c4040_demand_sheet_id IN (SELECT *
																				   FROM v_in_list)
											 AND t4042.c901_type IN (50563, 50567, 50568, 50569)) t4042
							  , t205_part_number t205
						  WHERE t251.c901_type IN (20464, 20462, 20460, 20461)
							AND t251.c205_part_number_id = t4042.c205_part_number_id
							AND t251.c205_part_number_id = t205.c205_part_number_id
							AND t251.c250_inventory_lock_id = p_inventoryid
					   GROUP BY ROLLUP (t251.c901_type), ROLLUP (t205.c205_product_family, t251.c205_part_number_id)
					   UNION ALL
					   SELECT	DECODE (t251.c205_part_number_id
									  , NULL, DECODE (t205.c205_product_family
													, NULL, DECODE (GROUPING_ID (t205.c205_product_family
																			   , t251.c205_part_number_id
																				)
																  , 3, 'Total'
																  , 'Group Missing'
																   )
													, t205.c205_product_family
													 )
									  , t251.c205_part_number_id
									   ) num
							  , DECODE (t251.c205_part_number_id
									  , NULL, DECODE (t205.c205_product_family
													, NULL, DECODE (GROUPING_ID (t205.c205_product_family
																			   , t251.c205_part_number_id
																				)
																  , 3, 'Total'
																  , 'Group Missing'
																   )
													, get_code_name (t205.c205_product_family)
													 )
									  , t251.c205_part_number_id
									   ) pnum
							  , DECODE (t251.c901_type
									  , NULL, 'Total Inventory'
									  , get_code_name (t251.c901_type)
									   ) period, DECODE (t251.c901_type, NULL, 0, SUM (t251.c251_qty)) qty
							  , GROUPING_ID (t205.c205_product_family, t251.c205_part_number_id) grpid
							  , t205.c205_product_family pfamily
						   FROM t251_inventory_lock_detail t251
							  , (SELECT DISTINCT t4042.c205_part_number_id
											FROM t4042_demand_sheet_detail t4042
										   WHERE t4042.c4040_demand_sheet_id IN (SELECT *
																				   FROM v_in_list)
											 AND t4042.c901_type IN (50563, 50567, 50568, 50569)) t4042
							  , t205_part_number t205
						  WHERE t251.c901_type IN (20477)
							AND t251.c205_part_number_id = t4042.c205_part_number_id
							AND t251.c205_part_number_id = t205.c205_part_number_id
							AND t251.c250_inventory_lock_id = p_inventoryid
					   GROUP BY ROLLUP (t251.c901_type), ROLLUP (t205.c205_product_family, t251.c205_part_number_id)
					   UNION ALL
					   SELECT	DECODE (t251.c205_part_number_id
									  , NULL, DECODE (t205.c205_product_family
													, NULL, DECODE (GROUPING_ID (t205.c205_product_family
																			   , t251.c205_part_number_id
																				)
																  , 3, 'Total'
																  , 'Group Missing'
																   )
													, t205.c205_product_family
													 )
									  , t251.c205_part_number_id
									   ) num
							  , DECODE (t251.c205_part_number_id
									  , NULL, DECODE (t205.c205_product_family
													, NULL, DECODE (GROUPING_ID (t205.c205_product_family
																			   , t251.c205_part_number_id
																				)
																  , 3, 'Total'
																  , 'Group Missing'
																   )
													, get_code_name (t205.c205_product_family)
													 )
									  , t251.c205_part_number_id
									   ) pnum
							  , DECODE (t251.c901_type
									  , NULL, 'Total Inventory'
									  , get_code_name (t251.c901_type)
									   ) period, DECODE (t251.c901_type, NULL, 0, SUM (t251.c251_qty)) qty
							  , GROUPING_ID (t205.c205_product_family, t251.c205_part_number_id) grpid
							  , t205.c205_product_family pfamily
						   FROM t251_inventory_lock_detail t251
							  , (SELECT DISTINCT t4042.c205_part_number_id
											FROM t4042_demand_sheet_detail t4042
										   WHERE t4042.c4040_demand_sheet_id IN (SELECT *
																				   FROM v_in_list)
											 AND t4042.c901_type IN (50563, 50567, 50568, 50569)) t4042
							  , t205_part_number t205
						  WHERE t251.c901_type IN (20454, 20455)
							AND t251.c205_part_number_id = t4042.c205_part_number_id
							AND t251.c205_part_number_id = t205.c205_part_number_id
							AND t251.c250_inventory_lock_id = p_inventoryid
					   GROUP BY ROLLUP (t251.c901_type), ROLLUP (t205.c205_product_family, t251.c205_part_number_id))
			 GROUP BY num, pnum, period, grpid, pfamily
			 ORDER BY pfamily, grpid, num, period;
	END gm_fc_fch_invdetails;

--
  /*******************************************************
   * Description : Procedure to generate details section for Total Required Qty
   * Author 	 : Joe P Kumar
   *******************************************************/
--
	PROCEDURE gm_fc_fch_totreqqty (
		p_forecastmonth 	  IN	   t4052_ttp_detail.c4052_forecast_period%TYPE
	  , p_inventory_lock_id   IN	   t250_inventory_lock.c250_inventory_lock_id%TYPE
	  , p_outordqtydetails	  OUT	   TYPES.cursor_type
	)
	AS
		v_fc_start_date DATE;
	BEGIN
		SELECT MIN (c4042_period)	-- '8/1/2007'
		  INTO v_fc_start_date
		  FROM t4042_demand_sheet_detail t4042
		 WHERE t4042.c901_type = 50563 AND t4042.c4040_demand_sheet_id IN (SELECT *
																			 FROM v_in_list);

		OPEN p_outordqtydetails
		 FOR
			 SELECT   DECODE (t205.c205_part_number_id
							, NULL, DECODE (t205.c205_product_family
										  , NULL, DECODE (GROUPING_ID (t205.c205_product_family
																	 , t205.c205_part_number_id
																	  )
														, 3, 'Total'
														, 'Group Missing'
														 )
										  , t205.c205_product_family
										   )
							, t205.c205_part_number_id
							 ) ID
					, DECODE (t205.c205_part_number_id
							, NULL, DECODE (t205.c205_product_family
										  , NULL, DECODE (GROUPING_ID (t205.c205_product_family
																	 , t205.c205_part_number_id
																	  )
														, 3, 'Total'
														, 'Group Missing'
														 )
										  , get_code_name (t205.c205_product_family)
										   )
							, t205.c205_part_number_id
							 ) NAME
					, 1 pqty, SUM (DECODE (refid, NULL, c4042_qty, 0)) forecast_qty
					, SUM (NVL (DECODE (refid, NULL, inv_qty, 0), 0)) total_pending_request
					, (SUM (DECODE (refid, NULL, c4042_qty, 0)) + SUM (NVL (DECODE (refid, NULL, inv_qty, 0), 0))
					  ) total_required
					, GROUPING_ID (t205.c205_product_family, t205.c205_part_number_id) grpid
				 FROM	-- Below code to fetch demand sheet based on par value
					  (SELECT	t4042.c205_part_number_id
							  , SUM (DECODE (t4042.c901_type
										   , 50563, DECODE (parvalue.c205_part_number_id
														  , NULL, t4042.c4042_qty
														  , DECODE (parvalue.parperiod
																  , t4042.c4042_period, parvalue.parvalue
																  , 0
																   )
														   )
										   , 0
											)
									) c4042_qty
							  , t4042.c4042_parent_ref_id refid
						   FROM t4042_demand_sheet_detail t4042
							  , (SELECT *
								   FROM (SELECT   t4042.c4040_demand_sheet_id, t4042.c205_part_number_id
												, MIN (t4042.c4042_period) parperiod
												, SUM (DECODE (t4042.c901_type, 50563, t4042.c4042_qty, 0)) demandvalue
												, SUM (DECODE (t4042.c901_type, 50566, t4042.c4042_qty, 0)) parvalue
											 FROM t4042_demand_sheet_detail t4042
											WHERE t4042.c4040_demand_sheet_id IN (SELECT *
																					FROM v_in_list)
											  AND t4042.c4042_period BETWEEN v_fc_start_date
																		 AND ADD_MONTHS (v_fc_start_date
																					   , (p_forecastmonth - 1)
																						)
											  AND t4042.c901_type IN (50563, 50566)
										 GROUP BY t4042.c4040_demand_sheet_id, t4042.c205_part_number_id)
								  WHERE parvalue > demandvalue) parvalue   -- Above to fetch part which has value more than par
						  WHERE t4042.c4040_demand_sheet_id IN (SELECT *
																  FROM v_in_list)
							AND t4042.c901_type IN (50563, 50567, 50568, 50569)
							AND t4042.c4042_period BETWEEN v_fc_start_date
													   AND ADD_MONTHS (v_fc_start_date, (p_forecastmonth - 1))
							AND t4042.c4040_demand_sheet_id = parvalue.c4040_demand_sheet_id(+)
							AND t4042.c205_part_number_id = parvalue.c205_part_number_id(+)
					   GROUP BY t4042.c205_part_number_id, t4042.c4042_parent_ref_id) forecast
					, (SELECT	t251.c205_part_number_id, SUM (t251.c251_qty) inv_qty
						   FROM t250_inventory_lock t250, t251_inventory_lock_detail t251
						  WHERE t250.c250_inventory_lock_id = t251.c250_inventory_lock_id
							AND t250.c250_inventory_lock_id = p_inventory_lock_id
							AND c901_type IN (50567, 50568, 50569)
					   GROUP BY c205_part_number_id) inventory
					, t205_part_number t205
				WHERE t205.c205_part_number_id = forecast.c205_part_number_id AND t205.c205_part_number_id = inventory.c205_part_number_id(+)
			 GROUP BY ROLLUP (t205.c205_product_family, t205.c205_part_number_id)
			 ORDER BY t205.c205_product_family, grpid, NAME;
	END gm_fc_fch_totreqqty;

--
--
  /*******************************************************
   * Description : Procedure to generate details section for Total Required Qty for TTP Summary
   * Author 	 : D James
   *******************************************************/
--
	PROCEDURE gm_fc_fch_totreqqty_ttp (
		p_ttpdetailid		 IN 	  t4053_ttp_part_detail.c4052_ttp_detail_id%TYPE
	  , p_outordqtydetails	 OUT	  TYPES.cursor_type
	)
	AS
		v_fc_start_date VARCHAR2 (20);
	BEGIN
		OPEN p_outordqtydetails
		 FOR
			 SELECT   DECODE (t4053.c205_part_number_id
							, NULL, DECODE (t205.c205_product_family
										  , NULL, DECODE (GROUPING_ID (t205.c205_product_family
																	 , t4053.c205_part_number_id
																	  )
														, 3, 'Total'
														, 'Group Missing'
														 )
										  , t205.c205_product_family
										   )
							, t4053.c205_part_number_id
							 ) ID
					, DECODE (t4053.c205_part_number_id
							, NULL, DECODE (t205.c205_product_family
										  , NULL, DECODE (GROUPING_ID (t205.c205_product_family
																	 , t4053.c205_part_number_id
																	  )
														, 3, 'Total'
														, 'Group Missing'
														 )
										  , get_group_name (t205.c205_product_family)
										   )
							, t4053.c205_part_number_id
							 ) NAME
					, SUM (t4053.c4053_qty) qty
					, GROUPING_ID (t205.c205_product_family, t4053.c205_part_number_id) grpid
					, SUM (t4053.c4053_qty * t4053.c4053_cost_price) totalcost
				 FROM t4053_ttp_part_detail t4053, t205_part_number t205
				WHERE t4053.c205_part_number_id = t205.c205_part_number_id
					  AND t4053.c4052_ttp_detail_id = p_ttpdetailid
			 GROUP BY ROLLUP (t205.c205_product_family, t4053.c205_part_number_id)
			 ORDER BY t205.c205_product_family, grpid, NAME;
	END gm_fc_fch_totreqqty_ttp;

--
 /*******************************************************
   * Description : Procedure to generate Unit Price for each part
   * Author 	 : Joe P Kumar
   *******************************************************/
--
	PROCEDURE gm_fc_fch_unitprice (
		p_ttpdetailid	 IN 	  t4053_ttp_part_detail.c4052_ttp_detail_id%TYPE
	  , p_outunitprice	 OUT	  TYPES.cursor_type
	)
	AS
	BEGIN
		IF (p_ttpdetailid IS NULL)
		THEN
			OPEN p_outunitprice
			 FOR
				 SELECT   t4042.c205_part_number_id ID, t205.c205_part_num_desc nm
						, MAX (NVL (t405.c405_cost_price, 0) / NVL (c405_uom_qty, 1)) unit_price
					 FROM t205_part_number t205
						, t4040_demand_sheet t4040
						, t4042_demand_sheet_detail t4042
						, t405_vendor_pricing_details t405
					WHERE t4040.c4040_demand_sheet_id = t4042.c4040_demand_sheet_id
					  AND t205.c205_part_number_id = t4042.c205_part_number_id
					  AND t205.c205_part_number_id = t405.c205_part_number_id(+)
					  AND t405.c405_active_fl(+) = 'Y'
					  AND t405.c405_void_fl IS NULL
					  AND t4042.c901_type IN (50563, 50567, 50568, 50569)	--Forecast, PBO, PBL, PCS
					  AND t4040.c4040_demand_sheet_id IN (SELECT *
															FROM v_in_list)
				 GROUP BY t4042.c205_part_number_id, t205.c205_part_num_desc;
		ELSE
			OPEN p_outunitprice
			 FOR
				 SELECT   t4053.c205_part_number_id ID, '' nm, t4053.c4053_cost_price unit_price
					 FROM t4053_ttp_part_detail t4053, t205_part_number t205
					WHERE t4053.c4052_ttp_detail_id = p_ttpdetailid
					  AND t205.c205_part_number_id = t4053.c205_part_number_id
				 ORDER BY t205.c205_product_family, t205.c205_part_number_id;
		END IF;
	END gm_fc_fch_unitprice;

		--
  /*******************************************************
   * Description : Procedure to fetch demand sheets associated with the part
   * Author 	 : Joe P Kumar
   *******************************************************/
--
	PROCEDURE gm_fc_fch_partdemandsheetlist (
		p_demandsheetids   IN		VARCHAR2
	  , p_pnum			   IN		t205_part_number.c205_part_number_id%TYPE
	  , p_outheader 	   OUT		TYPES.cursor_type
	  , p_outdetails	   OUT		TYPES.cursor_type
	  , p_partdetails	   OUT		VARCHAR2
	)
	AS
	BEGIN
		-- The input String in the form of 5,8 will be set into this temp view.
		my_context.set_my_inlist_ctx (p_demandsheetids);

		SELECT c205_part_number_id || ' - ' || c205_part_num_desc
		  INTO p_partdetails
		  FROM t205_part_number
		 WHERE c205_part_number_id = p_pnum;

		OPEN p_outheader
		 FOR
			 SELECT DISTINCT DECODE (t4042.c901_type
								   , 50566, get_code_name (50566)
								   , TO_CHAR (t4042.c4042_period, 'Mon YY')
									) period
						   , t4042.c4042_period period1
						FROM t4040_demand_sheet t4040, t4042_demand_sheet_detail t4042
					   WHERE t4040.c4040_demand_sheet_id = t4042.c4040_demand_sheet_id
						 AND t4040.c4040_demand_sheet_id IN (SELECT *
															   FROM v_in_list)
						 AND t4042.c205_part_number_id = p_pnum
						 AND t4042.c901_type IN (50563, 50566)
					ORDER BY period1, period DESC;

		OPEN p_outdetails
		 FOR
			 SELECT   t4040.c4020_demand_master_id dmdid
					, gm_pkg_op_sheet.get_demandsheet_name (t4040.c4020_demand_master_id) dmdsheetname
					, DECODE (t4042.c901_type
							, 50566, get_code_name (50566)
							, TO_CHAR (t4042.c4042_period, 'Mon YY')
							 ) period
					, SUM (t4042.c4042_qty) qty
				 FROM t4040_demand_sheet t4040, t4042_demand_sheet_detail t4042
				WHERE t4040.c4040_demand_sheet_id = t4042.c4040_demand_sheet_id
				  AND t4040.c4040_demand_sheet_id IN (SELECT *
														FROM v_in_list)
				  AND t4042.c205_part_number_id = p_pnum
				  AND t4042.c901_type IN (50563, 50566)
			 GROUP BY t4040.c4020_demand_master_id, t4042.c901_type, t4042.c4042_period
			 ORDER BY t4040.c4020_demand_master_id, t4042.c901_type, t4042.c4042_period;
	END gm_fc_fch_partdemandsheetlist;

		--
  /*******************************************************
   * Description : Procedure to fetch request associated
				   with the demand sheets
   * Author 	 : VPrasath
   *******************************************************/
--
	PROCEDURE gm_fc_fch_request (
		p_demandsheetids   IN		VARCHAR2
	  , p_outdetails	   OUT		TYPES.cursor_type
	)
	AS
		v_ttp_detail_id t4052_ttp_detail.c4052_ttp_detail_id%TYPE;
	BEGIN
		-- The input String in the form of 5,8 will be set into this temp view.
		my_context.set_my_inlist_ctx (p_demandsheetids);

		OPEN p_outdetails
		 FOR
			 SELECT t4020.c4020_demand_nm ds_name, t4020.c4020_request_period req_period, c4030_ref_id ID
				  , DECODE (t4030.c901_ref_type
						  , 20296, get_set_name (c4030_ref_id)
						  , get_partnum_desc (c4030_ref_id)
						   ) NAME
				  , TO_CHAR (LAST_DAY (c4043_start_date), 'Mon-YY') required_date, t4043.c4043_value required_qty
			   FROM t4020_demand_master t4020
				  , t4040_demand_sheet t4040
				  , t4043_demand_sheet_growth t4043
				  , t4030_demand_growth_mapping t4030
			  WHERE t4020.c4020_demand_master_id = t4040.c4020_demand_master_id
				AND t4040.c4040_demand_sheet_id IN (SELECT *
													  FROM v_in_list)
				AND t4040.c4040_demand_sheet_id = t4043.c4040_demand_sheet_id
				AND t4030.c4030_demand_growth_id = t4043.c4030_demand_growth_id
				AND c4043_start_date BETWEEN t4040.c4040_demand_period_dt
										 AND ADD_MONTHS (t4040.c4040_demand_period_dt
													   , (t4040.c4040_request_period - 1))
				AND t4043.c4043_value > 0
				AND t4040.c901_demand_type IN ('40021', '40022');
	END gm_fc_fch_request;

--
  /*******************************************************
   * Description : Procedure to fetch request for TPR from
   *			   locked data
   * Author 	 : VPrasath
   *******************************************************/
--
	PROCEDURE gm_fc_fch_tprdetails (
		p_pnum				 IN 	  t205_part_number.c205_part_number_id%TYPE
	  , p_inv_lock_id		 IN 	  t250_inventory_lock.c250_inventory_lock_id%TYPE
	  , p_status			 IN 	  t253c_request_lock.c520_status_fl%TYPE
	  , p_demandsheetid 	 IN 	  t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	  , p_demandmasterid	 IN 	  t4040_demand_sheet.c4020_demand_master_id%TYPE
	  , p_setid 			 IN 	  t253c_request_lock.c207_set_id%TYPE
	  , p_outdetails		 OUT	  TYPES.cursor_type
	  , p_out_void_details	 OUT	  TYPES.cursor_type
	  , p_desc				 OUT	  VARCHAR2
	  , p_salesbo_details	 OUT	  TYPES.cursor_type
	  , p_loanerbo_details   OUT	  TYPES.cursor_type
	)
--
	AS
		v_inv_lock_id  t250_inventory_lock.c250_inventory_lock_id%TYPE;
	BEGIN
		IF p_demandsheetid IS NOT NULL
		THEN
			SELECT c250_inventory_lock_id
			  INTO v_inv_lock_id
			  FROM t4040_demand_sheet
			 WHERE c4040_demand_sheet_id = p_demandsheetid;
		ELSE
			v_inv_lock_id := p_inv_lock_id;
		END IF;

		OPEN p_outdetails
		 FOR
			 SELECT   t253d.c520_request_id reqid, '' cons_id
					, TO_CHAR (t253c.c520_request_date, 'MM/DD/YYYY') request_date
					, TO_CHAR (t253c.c520_required_date, 'MM/DD/YYYY') required_date
					, gm_pkg_op_request_summary.get_request_status (t253c.c520_status_fl) sfl, t253d.c521_qty qty
					, get_user_name (t253c.c520_last_updated_by) updated_by
					, TO_CHAR (t253c.c520_last_updated_date, 'MM/DD/YYYY') updated_date
					--, gm_pkg_op_sheet.get_demandsheet_name (t253c.c520_request_txn_id) sheet
					,NVL(decode(gm_pkg_op_sheet.get_demandsheet_name(t253c.c520_request_txn_id)
					,'0',t253c.c520_master_request_id
					,gm_pkg_op_sheet.get_demandsheet_name (t253c.c520_request_txn_id))
					,'') sheet
					, get_code_name (t253c.c901_request_source) SOURCE
					, DECODE (t253c.c520_request_for
							, 40021, get_distributor_name (t253c.c520_request_to)
							, 40022, get_user_name (t253c.c520_ship_to_id)
							 ) req_to
				 FROM t253c_request_lock t253c, t253d_request_detail_lock t253d, t250_inventory_lock t250
				WHERE t250.c250_inventory_lock_id = v_inv_lock_id
				  AND t250.c250_inventory_lock_id = t253c.c250_inventory_lock_id
				  AND t250.c250_inventory_lock_id = t253d.c250_inventory_lock_id
				  AND t253c.c520_required_date < t250.c250_lock_date
				  AND t253d.c205_part_number_id = p_pnum
				  AND t253c.c520_request_id = t253d.c520_request_id
				  AND t250.c250_inventory_lock_id = t253c.c250_inventory_lock_id
				  AND t253c.c901_request_source <> 50617
				  AND t253c.c520_void_fl IS NULL
				  AND NVL (t253c.c520_request_txn_id, '1') =
															NVL (p_demandmasterid, NVL (t253c.c520_request_txn_id, '1'))
				  AND t253c.c520_status_fl = NVL (p_status, t253c.c520_status_fl)
				  AND DECODE (p_setid, NULL, '1', NVL (t253c.c207_set_id, '-9999')) = NVL (p_setid, '1')
			 UNION ALL
			 SELECT   t253a.c520_request_id req_id, t253a.c504_consignment_id cons_id
					, TO_CHAR (t253c.c520_request_date, 'MM/DD/YYYY') request_date
					, TO_CHAR (t253c.c520_required_date, 'MM/DD/YYYY') required_date
					, gm_pkg_op_request_summary.get_request_status (t253c.c520_status_fl) sfl, t253b.c505_item_qty qty
					, get_user_name (t253c.c520_last_updated_by) updated_by
					, TO_CHAR (t253c.c520_last_updated_date, 'MM/DD/YYYY') updated_date
					--, gm_pkg_op_sheet.get_demandsheet_name (t253c.c520_request_txn_id) sheet
					,NVL(decode(gm_pkg_op_sheet.get_demandsheet_name(t253c.c520_request_txn_id)
					,'0',t253c.c520_master_request_id
					,gm_pkg_op_sheet.get_demandsheet_name (t253c.c520_request_txn_id))
					,'') sheet
					, get_code_name (t253c.c901_request_source) SOURCE
					, DECODE (t253c.c520_request_for
							, 40021, get_distributor_name (t253c.c520_request_to)
							, 40022, get_user_name (t253c.c520_ship_to_id)
							 ) req_to
				 FROM t253a_consignment_lock t253a
					, t253b_item_consignment_lock t253b
					, t253c_request_lock t253c
					, t250_inventory_lock t250
				WHERE t250.c250_inventory_lock_id = v_inv_lock_id
				  AND t250.c250_inventory_lock_id = t253a.c250_inventory_lock_id
				  AND t250.c250_inventory_lock_id = t253b.c250_inventory_lock_id
				  AND t250.c250_inventory_lock_id = t253c.c250_inventory_lock_id
				  AND t253c.c520_required_date < t250.c250_lock_date
				  AND t253b.c205_part_number_id = p_pnum
				  AND t253a.c520_request_id = t253c.c520_request_id
				  AND t253a.c504_consignment_id = t253b.c504_consignment_id
				  AND t253c.c901_request_source <> 50617
				  AND t253c.c520_void_fl IS NULL
				  AND t253a.c504_void_fl IS NULL
				  AND NVL (t253c.c520_request_txn_id, '1') =
															NVL (p_demandmasterid, NVL (t253c.c520_request_txn_id, '1'))
				  AND t253c.c520_status_fl = NVL (p_status, t253c.c520_status_fl)
				  AND DECODE (p_setid, NULL, '1', NVL (t253a.c207_set_id, '-9999')) = NVL (p_setid, '1')
			 ORDER BY reqid, cons_id, request_date, required_date, sfl;

		OPEN p_out_void_details
		 FOR
			 SELECT   t253d.c520_request_id reqid, '' cons_id
					, TO_CHAR (t253c.c520_request_date, 'MM/DD/YYYY') request_date
					, TO_CHAR (t253c.c520_required_date, 'MM/DD/YYYY') required_date
					, gm_pkg_op_request_summary.get_request_status (t253c.c520_status_fl) sfl, t253d.c521_qty qty
					, get_user_name (t253c.c520_last_updated_by) updated_by
					, TO_CHAR (t253c.c520_last_updated_date, 'MM/DD/YYYY') updated_date
					, gm_pkg_op_sheet.get_demandsheet_name (t253c.c520_request_txn_id) sheet
					, get_code_name (t253c.c901_request_source) SOURCE
					, DECODE (t253c.c520_request_for
							, 40021, get_distributor_name (t253c.c520_request_to)
							, 40022, get_user_name (t253c.c520_ship_to_id)
							 ) req_to
				 FROM t253c_request_lock t253c, t253d_request_detail_lock t253d, t250_inventory_lock t250
				 WHERE t250.c250_inventory_lock_id = v_inv_lock_id
				  AND t250.c250_inventory_lock_id = t253c.c250_inventory_lock_id
				  AND t250.c250_inventory_lock_id = t253d.c250_inventory_lock_id
				  AND t253c.c520_required_date < t250.c250_lock_date
				  AND t253d.c205_part_number_id = p_pnum
				  AND t253c.c520_request_id = t253d.c520_request_id
				  AND t250.c250_inventory_lock_id = t253c.c250_inventory_lock_id
				  AND t253c.c901_request_source <> 50617
				  AND t253c.c520_void_fl IS NOT NULL
				  AND NVL (t253c.c520_request_txn_id, '1') =
															NVL (p_demandmasterid, NVL (t253c.c520_request_txn_id, '1'))
				  AND t253c.c520_status_fl = NVL (p_status, t253c.c520_status_fl)
				  AND DECODE (p_setid, NULL, '1', NVL (t253c.c207_set_id, '-9999')) = NVL (p_setid, '1')
			 UNION ALL
			 SELECT   t253a.c520_request_id req_id, t253a.c504_consignment_id cons_id
					, TO_CHAR (t253c.c520_request_date, 'MM/DD/YYYY') request_date
					, TO_CHAR (t253c.c520_required_date, 'MM/DD/YYYY') required_date
					, gm_pkg_op_request_summary.get_request_status (t253c.c520_status_fl) sfl, t253b.c505_item_qty qty
					, get_user_name (t253c.c520_last_updated_by) updated_by
					, TO_CHAR (t253c.c520_last_updated_date, 'MM/DD/YYYY') updated_date
					, gm_pkg_op_sheet.get_demandsheet_name (t253c.c520_request_txn_id) sheet
					, get_code_name (t253c.c901_request_source) SOURCE
					, DECODE (t253c.c520_request_for
							, 40021, get_distributor_name (t253c.c520_request_to)
							, 40022, get_user_name (t253c.c520_ship_to_id)
							 ) req_to
				 FROM t253a_consignment_lock t253a
					, t253b_item_consignment_lock t253b
					, t253c_request_lock t253c
					, t250_inventory_lock t250
				WHERE t250.c250_inventory_lock_id = v_inv_lock_id
				  AND t250.c250_inventory_lock_id = t253a.c250_inventory_lock_id
				  AND t250.c250_inventory_lock_id = t253b.c250_inventory_lock_id
				  AND t250.c250_inventory_lock_id = t253c.c250_inventory_lock_id
				  AND t253c.c520_required_date < t250.c250_lock_date
				  AND t253b.c205_part_number_id = p_pnum
				  AND t253a.c520_request_id = t253c.c520_request_id
				  AND t253a.c504_consignment_id = t253b.c504_consignment_id
				  AND t253c.c901_request_source <> 50617
				  AND t253c.c520_void_fl IS NOT NULL
				  AND t253a.c504_void_fl IS NOT NULL
				  AND NVL (t253c.c520_request_txn_id, '1') =
															NVL (p_demandmasterid, NVL (t253c.c520_request_txn_id, '1'))
				  AND t253c.c520_status_fl = NVL (p_status, t253c.c520_status_fl)
				  AND DECODE (p_setid, NULL, '1', NVL (t253a.c207_set_id, '-9999')) = NVL (p_setid, '1')
			 ORDER BY reqid, cons_id, request_date, required_date, sfl;

		SELECT get_partnum_desc (p_pnum)
		  INTO p_desc
		  FROM DUAL;
		  
-- Removed columns updated by,updated date and added columns  Requested by, requested date for MNITASK TASK 2347.
		OPEN p_salesbo_details
		 FOR
			 SELECT t253e.c501_order_id ordid, get_account_name (t253e.c704_account_id) accid, t253f.c502_item_qty qty
			   --  , get_user_name (t253e.c501_last_updated_by) updated_by
				--  , TO_CHAR (t253e.c501_last_updated_date, 'MM/DD/YYYY') updated_date
			    , TO_CHAR(t253e.c501_order_date,'MM/DD/YYYY') request_date
				  , GET_REP_NAME(t253e.C703_SALES_REP_ID) requested_name
			   FROM 
			   	 /* Commented for MNTTASK-3724 to avoid performance issue 	
			   	  v700_territory_mapping_detail v700
				  , (SELECT DISTINCT c4021_ref_id
								FROM t4021_demand_mapping t4021
							   WHERE t4021.c901_ref_type = 40032
								 AND t4021.c4020_demand_master_id = NVL (NULL, t4021.c4020_demand_master_id)) t4021
				  , */ 
				  t253e_order_lock t253e
				  , t253f_item_order_lock t253f
			  WHERE t253e.c250_inventory_lock_id = v_inv_lock_id
				AND t253e.c250_inventory_lock_id = t253f.c250_inventory_lock_id
				AND t253e.c501_order_id = t253f.c501_order_id
				/*	AND t4021.c4021_ref_id = v700.region_id
					AND t253e.c704_account_id = v700.ac_id
				*/
				AND NVL (t253e.c901_order_type, -999) = 2525
				AND t253e.c501_status_fl = 0
				AND t253e.c501_void_fl IS NULL
			    AND t253f.c205_part_number_id = p_pnum;
			    
		OPEN p_loanerbo_details
		FOR
		SELECT t253g.c412_inhouse_trans_id inhouse_trans_id, t253g.c412_ref_id refid, t253h.c413_item_qty qty
			   FROM 
			   	  t253g_inhouse_transaction_lock t253g
				  , t253h_inhouse_trans_items_lock t253h
			  WHERE t253g.c250_inventory_lock_id = v_inv_lock_id
				AND t253g.c250_inventory_lock_id = t253h.c250_inventory_lock_id
				AND t253g.c412_inhouse_trans_id = t253h.c412_inhouse_trans_id
				AND t253g.c412_type=100062 
				AND t253g.c412_status_fl='0' 
				AND t253g.c412_void_fl IS NULL
				AND t253h.c413_void_fl IS NULL
			    AND t253h.c205_part_number_id = p_pnum;
	END gm_fc_fch_tprdetails;

		
	/*****************************************************
	  *Description : Procedure to fetch Parent Part Qty Details
	  *
	  * Author		: VPrasath
	************************************************************/
	PROCEDURE gm_fc_fch_ppqdetails (
		p_pnum					 IN 	  t205_part_number.c205_part_number_id%TYPE
	  , p_inv_lock_id			 IN 	  t251_inventory_lock_detail.c250_inventory_lock_id%TYPE
	  , p_desc					 OUT	  VARCHAR2
	  , p_parentpartqtydetails	 OUT	  TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_parentpartqtydetails
		 FOR
			 SELECT   pnum, des, SUM (instock) instock, SUM (opendhr) opendhr, MAX (sub_part_qty) sub_part_qty
					, SUM (total_instock) total_instock, SUM (total_opendhr) total_opendhr
					, (NVL (SUM (total_instock), 0) + NVL (SUM (total_opendhr), 0)) total
				 FROM (SELECT t251.c205_part_number_id pnum, get_partnum_desc (t251.c205_part_number_id) des
							, DECODE (t251.c901_type, 20464, t251.c251_qty) instock
							, DECODE (t251.c901_type, 20461, t251.c251_qty) opendhr, t205a.sub_part_qty
							, DECODE (t251.c901_type, 20464, (t251.c251_qty * t205a.sub_part_qty)) total_instock
							, DECODE (t251.c901_type, 20461, (t251.c251_qty * t205a.sub_part_qty)) total_opendhr
						 FROM t251_inventory_lock_detail t251
							, (SELECT	  t205a.c205_from_part_number_id, t205a.c205a_qty sub_part_qty
									 FROM t205a_part_mapping t205a
							   START WITH t205a.c205_to_part_number_id = p_pnum
									  AND t205a.c205_to_part_number_id IN (SELECT t205d.c205_part_number_id
																			 FROM t205d_sub_part_to_order t205d)
							   CONNECT BY PRIOR t205a.c205_from_part_number_id = t205a.c205_to_part_number_id
							   AND t205a.c205a_void_fl is null) t205a   ---  added new column c205a_void_fl in t205a used to PMT-50777 - Create BOM and sub-component Mapping
						WHERE t205a.c205_from_part_number_id = t251.c205_part_number_id
						  AND t251.c250_inventory_lock_id = p_inv_lock_id
						  AND t251.c901_type IN (20464, 20461)
						  AND t251.c251_qty > 0)
			 GROUP BY pnum, des;

		SELECT get_partnum_desc (p_pnum)
		  INTO p_desc
		  FROM DUAL;
	END gm_fc_fch_ppqdetails;

/*****************************************************
   *Description : Procedure to fetch Parent-Sub PartDetails
   *
   * Author 	 : RShah
 ************************************************************/
	PROCEDURE gm_ld_parent_sub_parts (
		p_parentsubpartdetails	 OUT   TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_parentsubpartdetails
		 FOR
			 SELECT DISTINCT t4042.c205_parent_part_num_id parentpart, c205_part_number_id subpart
						   , ROUND(t4042.c4042_parent_qty_needed,0) qty
						FROM t4042_demand_sheet_detail t4042
					   WHERE t4042.c4040_demand_sheet_id IN (SELECT *
															   FROM v_in_list)
						 AND t4042.c205_parent_part_num_id IS NOT NULL
						 AND t4042.c901_type IN (50563, 50567, 50568, 50569);
	END gm_ld_parent_sub_parts;

	 /*******************************************************
	* Description : Main Procedure to get ttp sheet details
	* Author	  : Ritesh
	*******************************************************/
	PROCEDURE gm_fc_fch_ttp_summary_sheets (
		p_ttpid 		   IN		t4050_ttp.c4050_ttp_id%TYPE
	  , p_monyear		   IN		VARCHAR2
	  , p_demandsheetids   IN		VARCHAR2
	  , p_inventoryid	   IN OUT	t250_inventory_lock.c250_inventory_lock_id%TYPE
	  , p_forecastmonth    IN		t4052_ttp_detail.c4052_forecast_period%TYPE
	  , p_sheetname 	   OUT		TYPES.cursor_type
	  , p_status		   OUT		VARCHAR2
	  , p_finaldate 	   OUT		VARCHAR2
	  , p_ds_status 	   OUT		VARCHAR2
	)
	AS
		v_ttp_detail_id t4052_ttp_detail.c4052_ttp_detail_id%TYPE;
		v_demand_sheetids VARCHAR2 (5000);
		v_string	   VARCHAR2 (100);
		v_inventoryid  t250_inventory_lock.c250_inventory_lock_id%TYPE;
		v_forecast_period t4052_ttp_detail.c4052_forecast_period%TYPE;
		v_fc_start_date DATE;

		CURSOR cur_demandsheetids
		IS
			SELECT c4040_demand_sheet_id ID
			  FROM t4040_demand_sheet
			 WHERE c4052_ttp_detail_id = v_ttp_detail_id;
	BEGIN
		IF (p_demandsheetids IS NULL OR p_monyear <> TO_CHAR (SYSDATE, 'MM/YYYY'))
		THEN
			v_demand_sheetids := 0;

			-- Fetch TTP Summary information
			SELECT c4052_ttp_detail_id, c901_status, c4052_forecast_period
			  INTO v_ttp_detail_id, p_status, v_forecast_period
			  FROM t4052_ttp_detail
			 WHERE c4050_ttp_id = p_ttpid AND TO_CHAR (c4052_ttp_link_date, 'MM/YYYY') = p_monyear;

			-- Fetch Inventory information
			SELECT c250_inventory_lock_id
			  INTO v_inventoryid
			  FROM t252_inventory_lock_ref
			 WHERE c252_ref_id = v_ttp_detail_id AND c901_ref_type = 20480;

			p_inventoryid := v_inventoryid;

			FOR cur_val IN cur_demandsheetids
			LOOP
				v_demand_sheetids := cur_val.ID || ',' || v_demand_sheetids;
			END LOOP;
		--
		ELSE
			v_demand_sheetids := p_demandsheetids;
			v_forecast_period := p_forecastmonth;
			p_status	:= NULL;
			v_inventoryid := p_inventoryid;
		END IF;

		my_context.set_my_inlist_ctx (v_demand_sheetids);

		SELECT DECODE (COUNT (1), 0, 'false', 'true')
		  INTO p_ds_status
		  FROM t4040_demand_sheet t4040
		 WHERE c4040_demand_sheet_id IN (SELECT *
										   FROM v_in_list) AND t4040.c901_status <> 50551;

		gm_fc_fch_demandsheet_name (p_sheetname);

		SELECT TO_CHAR (MAX (c4042_period), 'Mon YY')	-- '8/1/2007'
		  INTO p_finaldate
		  FROM t4042_demand_sheet_detail t4042
		 WHERE t4042.c901_type = 50563 AND t4042.c4040_demand_sheet_id IN (SELECT *
																			 FROM v_in_list);
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			raise_application_error (-20900, '');	-- No Demand Sheets have been mapped
	END gm_fc_fch_ttp_summary_sheets;
END gm_pkg_op_ttp_summary;
/
