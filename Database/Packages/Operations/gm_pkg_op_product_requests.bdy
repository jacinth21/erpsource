/* Formatted on 05/17/2010 16:26 (Formatter Plus v4.8.0) */
-- @"c:\database\packages\operations\gm_pkg_op_product_requests.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_product_requests
IS
--
/************************************************************************
 * Purpose: Package holds Product Requests Related Save and Fetch Methods
 * Created By DJames
 ************************************************************************/
--
--
 /*******************************************************
 * Purpose: This Procedure is used fetch data for viewing Open
 *		requests for Products
	This prc is not used anymore and is moved to java
 *******************************************************/
--
	PROCEDURE gm_fch_product_openrequests (
		p_setid 	IN		 t526_product_request_detail.c207_set_id%TYPE
	  , p_partnum	IN		 t526_product_request_detail.c205_part_number_id%TYPE
	  , p_type		IN		 t526_product_request_detail.c901_request_type%TYPE
	  , p_dist_id	IN		 t525_product_request.c525_request_for_id%TYPE
	  , p_req_cur	OUT 	 TYPES.cursor_type
	)
	AS
		--
		v_set_id	   t526_product_request_detail.c207_set_id%TYPE;
		v_dist_id	   t525_product_request.c525_request_for_id%TYPE;
	BEGIN
		v_set_id	:= p_setid;
		v_dist_id	:= p_dist_id;

		IF v_set_id = ''
		THEN
			v_set_id	:= NULL;
		END IF;

		--
		IF v_dist_id = '' OR v_dist_id IS NULL
		THEN
			v_dist_id	:= NULL;
		END IF;

		--
		OPEN p_req_cur
		 FOR
			 SELECT   a.c525_product_request_id pdtreqid, TO_CHAR (b.c525_requested_date, GET_RULE_VALUE('DATEFMT', 'DATEFORMAT')) reqsdt
					, get_distributor_name (b.c525_request_for_id) dname, get_code_name (b.c901_ship_to) shiptonm
					, get_user_name (b.c525_created_by) cuser, TO_CHAR (b.c525_created_date, GET_RULE_VALUE('DATEFMT', 'DATEFORMAT')) cdate
					, a.c526_qty_requested qty, TO_CHAR (a.c526_required_date, GET_RULE_VALUE('DATEFMT', 'DATEFORMAT')) reqrdt
					, b.c525_request_for_id reqforid, b.c901_ship_to shipto
					, a.c526_product_request_detail_id reqdetailid, a.c207_set_id setid
					, get_set_name (a.c207_set_id) setnm, t907.c907_ship_to_id shiptoid
					, t907.c901_delivery_carrier shipcarr, t907.c901_delivery_mode shipmode
					, t907.c106_address_id shipaddid
				 FROM t526_product_request_detail a, t525_product_request b, t907_shipping_info t907
				WHERE c207_set_id = NVL (v_set_id, c207_set_id)
				  AND c901_request_type = p_type
				  AND a.c525_product_request_id = b.c525_product_request_id
				  AND b.c525_product_request_id = t907.c907_ref_id(+)
				  AND b.c525_void_fl IS NULL
				  AND t907.c907_void_fl IS NULL
				  -- AND a.c526_qty_requested > 0
				  AND a.c526_status_fl = 10
				  AND b.c525_status_fl != 40
				  AND b.c525_request_for_id = NVL (v_dist_id, b.c525_request_for_id)
			 ORDER BY b.c525_created_date;
	END gm_fch_product_openrequests;

--
/*******************************************************
 * Purpose: This Procedure is used to save/update the T525_PRODUCT_REQUEST table
 *******************************************************/
--
	PROCEDURE gm_sav_product_requests (
		p_surgery_date	 IN 	  Date
	  , p_req_for		 IN 	  t525_product_request.c901_request_for%TYPE
	  , p_req_for_id	 IN 	  t525_product_request.c525_request_for_id%TYPE
	  , p_req_by_type	 IN 	  t525_product_request.c901_request_by_type%TYPE
	  , p_req_by_id 	 IN 	  t525_product_request.c525_request_by_id%TYPE
	  , p_ship_to		 IN 	  t525_product_request.c901_ship_to%TYPE
	  , p_ship_to_id	 IN 	  t525_product_request.c525_ship_to_id%TYPE
	  , p_user_id		 IN 	  t525_product_request.c525_created_by%TYPE
	  , p_input_str 	 IN 	  VARCHAR2
	  , p_rep_id		 IN 	  t525_product_request.c703_sales_rep_id%TYPE
	  , p_acc_id		 IN 	  t525_product_request.c704_account_id%TYPE
	  , p_req_id		 IN OUT	  VARCHAR2
	  , p_att_val		 IN 	  VARCHAR2 default NULL
	  , p_assoc_rep_id	 IN	      t525_product_request.c703_ass_rep_id%TYPE default NULL
	)
	AS
		--
		v_pdt_id	   VARCHAR2 (20);
		v_pdt_id_num   NUMBER;
		v_strlen	   NUMBER := NVL (LENGTH (p_input_str), 0);
		v_string	   VARCHAR2 (30000) := p_input_str;
		v_substring    VARCHAR2 (1000);
		v_setid 	   VARCHAR2 (20);
		v_pnum		   t526_product_request_detail.c205_part_number_id%TYPE;
		v_reqqty	   t208_set_details.c208_set_qty%TYPE;
		v_reqdate	   VARCHAR2 (40);
		v_ln_ship_day  T906_RULES.C906_RULE_VALUE%TYPE;
		v_status	   NUMBER;
		v_request_fl   VARCHAR2(10);
		v_company_id  t1900_company.c1900_company_id%TYPE;
		v_plant_id T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
		v_dist_company_id  t1900_company.c1900_company_id%TYPE;
		v_loaner_filter   VARCHAR2(20);
		
--
	BEGIN
		
		select get_compid_frm_cntx(),get_plantid_frm_cntx() into v_company_id,v_plant_id from dual;
		
		v_ln_ship_day := get_rule_value ('LOANERSHIPDAY', 'LOANER');
		
		IF (TRUNC(p_surgery_date - (CURRENT_DATE)) > v_ln_ship_day OR TRUNC(p_surgery_date - (CURRENT_DATE)) < 0)
		THEN
		raise_application_error (-20940, '');
		END IF;
		
		IF p_req_id IS NULL
		THEN
			SELECT s525_product_request.NEXTVAL
			  INTO v_pdt_id_num
			  FROM DUAL;

			--
			p_req_id	:= v_pdt_id_num;
		
			SELECT get_transaction_company_id (v_company_id,p_req_for_id,'LOANERINITFILTER','DIST') INTO v_dist_company_id FROM DUAL;
 			
			INSERT INTO t525_product_request
						(c525_product_request_id, c525_requested_date, c901_request_for, c525_request_for_id
					   , c901_request_by_type, c525_request_by_id, c901_ship_to, c525_ship_to_id, c525_created_by
					   , c525_created_date, c525_surgery_date, c704_account_id, c703_sales_rep_id, c525_status_fl
					   , c703_ass_rep_id ,c1900_company_id ,c5040_plant_id
						)
				 VALUES (v_pdt_id_num, TRUNC (CURRENT_DATE), p_req_for, p_req_for_id
					   , p_req_by_type, p_req_by_id, p_ship_to, p_ship_to_id, p_user_id
					   , CURRENT_DATE, p_surgery_date, DECODE(p_acc_id,'0',null,p_acc_id), p_rep_id, 10
					   , p_assoc_rep_id,v_dist_company_id,v_plant_id
						);
		my_context.set_my_inlist_ctx ('Y') ;
		END IF;
		
		IF v_strlen > 0
		THEN
			WHILE INSTR (v_string, '|') <> 0
			LOOP
				--
				v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
				v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
				--
				v_setid 	:= NULL;
				v_reqqty	:= NULL;
				v_reqdate	:= NULL;
				v_pnum		:= NULL;
				--
				v_setid 	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_reqqty	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				
				INSERT INTO my_temp_list values (v_setid);
				
				IF INSTR (v_substring, '^')  <> 0 THEN
					v_reqdate	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
					v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
					IF INSTR (v_substring, '^')  <> 0 THEN -- pnum add at end of string (status^pnum|)
						v_status := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
						v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
						v_pnum    := v_substring;
					ELSE
						v_status    := v_substring;
					END IF;
				ELSE
					v_reqdate	:= v_substring;
					v_status	:= 5;
				END IF;
				--
				gm_sav_product_requests_detail (p_req_id, p_req_for, v_reqdate, v_setid, v_pnum, v_reqqty, v_status);
			--
			END LOOP;
		END IF;
		
		IF  p_att_val IS NOT NULL THEN
			gm_sav_attribute_req_det(v_pdt_id_num,p_att_val,p_user_id);
		END IF;
		
		-- Call priority engine to prioritize set based on request
		gm_pkg_op_ln_priority_engine.gm_sav_main_ln_set_priority(p_req_id,p_user_id);
	END gm_sav_product_requests;

--
	/***************************************************************************************
	* Author		: Rajkumar
	* Description	: This procedure is used to save the attibute values for the Loaner Request, 
	* the attributes like Surgeon Name  Time of Surgery	
	* this procedure merged from OUS for product request attribute
	****************************************************************************************/
	PROCEDURE gm_sav_attribute_req_det(
		p_requestid IN t525_product_request.c525_product_request_id%TYPE,
		p_att_val1	IN VARCHAR2,
		p_user_id	 IN	t101_user.c101_user_id%TYPE
	)
	AS
	v_att_id	VARCHAR2(20);
	v_att_val	VARCHAR2(50);
	v_string VARCHAR2 (4000) := p_att_val1;
 	v_substring VARCHAR2 (4000);
	BEGIN
		WHILE INSTR(v_string,'|') <> 0
		LOOP
			v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
 			v_string := SUBSTR (v_string, INSTR (v_string, '|') + 1);

 			v_att_id := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1); -- Attribute ID like 401102
 			v_att_val := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);	-- Attribut Value is Surgeon Name Entered

 			UPDATE T525A_PRODUCT_REQUEST_ATTRIB 
				SET
					C525A_ATTRIBUTE_VALUE = v_att_val,
					C525A_LAST_UPDATED_BY = p_user_id,
					C525A_LAST_UPDATED_DATE = CURRENT_DATE
					WHERE c525_product_request_id = p_requestid
					AND C901_ATTRIBUTE_ID = v_att_id 
					AND C525A_VOID_FL IS NULL;
					
			IF (SQL%ROWCOUNT = 0) THEN 
				INSERT INTO T525A_PRODUCT_REQUEST_ATTRIB(C525A_PROD_REQ_ATTRIB_ID,c525_product_request_id,C901_ATTRIBUTE_ID,C525A_ATTRIBUTE_VALUE,C525A_CREATED_BY,C525A_CREATED_DATE) 
				VALUES(S525A_PROD_REQ_ATTRIB.nextval,p_requestid,v_att_id,v_att_val,p_user_id,CURRENT_DATE);
			END IF;
		END LOOP;
	END gm_sav_attribute_req_det;
	
	
--
/*******************************************************
 * Purpose: This Procedure is used to save/update the T526_PRODUCT_REQUEST_DETAIL table
 *******************************************************/
--
	PROCEDURE gm_sav_product_requests_detail (
		p_reqid 		 IN   t526_product_request_detail.c525_product_request_id%TYPE
	  , p_req_type		 IN   t526_product_request_detail.c901_request_type%TYPE
	  , p_request_date	 IN   VARCHAR2
	  , p_set_id		 IN   t526_product_request_detail.c207_set_id%TYPE
	  , p_partnum		 IN   t526_product_request_detail.c205_part_number_id%TYPE
	  , p_req_qty		 IN   t526_product_request_detail.c526_qty_requested%TYPE
	  , p_status		 IN   t526_product_request_detail.c526_status_fl%TYPE DEFAULT 10 	  
	)
	AS
--
	    v_request_fl     t526_product_request_detail.c526_request_flag%TYPE;
	    v_company_id  t1900_company.c1900_company_id%TYPE;
		v_plant_id T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
		v_dateformat  varchar2(100);
		v_dist_company_id  t1900_company.c1900_company_id%TYPE;
	BEGIN
		/* Code added for setting the NULL value to the C526_REQUEST_FLAG after calling second time.
		  that means After one case is booked if again need to add some more sets, then this flag will update the 'Y' value and in Jasper Print we show that second time set as **SET_ID
		  Added for PMT-254[Edit Loaner Requests Screen changes.]
	    */
		SELECT DECODE(SYS_CONTEXT ('inlist_ctx', 'txt', 4000),'Y',NULL,'Y') INTO v_request_fl  FROM DUAL;		
		
		select get_compid_frm_cntx(),get_plantid_frm_cntx(),get_compdtfmt_frm_cntx() into v_company_id,v_plant_id,v_dateformat from dual;
		SELECT NVL(c1900_company_id,v_company_id) 
		  INTO v_dist_company_id  
		  FROM t525_product_request 
		 WHERE c525_product_request_id = p_reqid 
		   AND c525_void_fl IS NULL ;
		
		INSERT INTO t526_product_request_detail
					(c526_product_request_detail_id, c525_product_request_id, c526_required_date, c901_request_type
				   , c207_set_id, c205_part_number_id, c526_qty_requested, c526_status_fl,c526_request_flag,c1900_company_id,c5040_plant_id
					)
			 VALUES (s626_seq_id.NEXTVAL, p_reqid, TO_DATE (p_request_date,v_dateformat), p_req_type
				   , p_set_id, p_partnum, p_req_qty, p_status,v_request_fl,v_dist_company_id,v_plant_id);
	END gm_sav_product_requests_detail;

--
--
 /*******************************************************
 * Purpose: This Procedure is used fetch Total Open Requests
 *		by Set ID/Part Num
 *******************************************************/
--
	FUNCTION gm_fch_product_openreq_count (
		p_setid 	IN	 t526_product_request_detail.c207_set_id%TYPE
	  , p_partnum	IN	 t526_product_request_detail.c205_part_number_id%TYPE
	  , p_type		IN	 t526_product_request_detail.c901_request_type%TYPE
	)
		RETURN NUMBER
	IS
		--
		v_cnt		   NUMBER;
--
	BEGIN
		SELECT	 SUM (c526_qty_requested)
			INTO v_cnt
			FROM t526_product_request_detail
		   WHERE c207_set_id = p_setid AND c901_request_type = p_type AND c526_status_fl = 10 AND c526_void_fl IS NULL
		GROUP BY c207_set_id;

		RETURN v_cnt;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN 0;
	END gm_fch_product_openreq_count;

--
--
/************************************************************************
 * Purpose: Procedure to get the request detail based on request id and
   the child records from table t526 for that request id
   Created By Rshah
 ************************************************************************/
	PROCEDURE gm_fch_loaner_request_detail (
		p_requestid 	 IN 	  t525_product_request.c525_product_request_id%TYPE
	  , p_txn_type		 IN       t526_product_request_detail.C901_REQUEST_TYPE%TYPE 
	  , p_req_edit_cur	 OUT	  TYPES.cursor_type
	  , p_req_cur		 OUT	  TYPES.cursor_type
	)
	AS
	
	v_company_id  t1900_company.c1900_company_id%TYPE;
	v_plant_id T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
	v_loaner_filetr T906_RULES.C906_RULE_VALUE%TYPE;
	BEGIN
		
		select get_compid_frm_cntx(),get_plantid_frm_cntx() into v_company_id,v_plant_id from dual;
		
		SELECT DECODE(GET_RULE_VALUE(v_company_id,'LOANEREDITREQFILTER'), null, v_company_id, v_plant_id) --LOANEREDITREQFILTER- To fetch the records for EDC entity countries based on plant
	      INTO v_loaner_filetr 
	      FROM DUAL;
		
	OPEN p_req_edit_cur
		 FOR
			 SELECT ROWNUM NO,detail.* from (SELECT t526.c526_product_request_detail_id reqdtlid
				  , DECODE (t526.c526_status_fl
						  , '20', DECODE(t526.c205_part_number_id,null,
									gm_pkg_op_product_requests.get_loanerreq_consignment_id (t526.c526_product_request_detail_id, NULL)
									, t526.c526_product_request_detail_id)
                          , '30', DECODE(t526.c205_part_number_id,null,gm_pkg_op_product_requests.get_loanerreq_consignment_id (t526.c526_product_request_detail_id, null)
								, t526.c526_product_request_detail_id)
						  , t526.c526_product_request_detail_id
						   ) reqcnid
				  , NVL(t526.c207_set_id,t526.c205_part_number_id) setid
				  , DECODE(t526.c207_set_id,NULL,get_partnum_desc(t526.c205_part_number_id),get_set_name (t526.c207_set_id)) setnm
				  , t526.c526_qty_requested  qty
				  , t526.c526_required_date requireddt
				  , t526.c526_loaner_escalation_fl ESCLLOGFLAG  
                  , get_loaner_log_flag(t526.c526_product_request_detail_id, '26240693') ASCFL   --PC 137 Escalation Log-Escalation Log
				  , gm_pkg_op_product_requests.get_req_loaner_etch_id (t526.c526_product_request_detail_id) etchid
				  , get_loaner_request_status(t526.c526_product_request_detail_id) req_status
				  , DECODE(t526.c526_status_fl, '20',gm_pkg_op_product_requests.get_set_status (t526.c526_product_request_detail_id), '') cn_status -- (Consignment status added for NSP-415)
				  , t526.c526_status_fl statusid, get_log_flag (t526.c526_product_request_detail_id, 1248) LOG
				  , DECODE(t526.c205_part_number_id,null,
	                        gm_pkg_op_product_requests.get_loanerreq_consignment_id (t526.c526_product_request_detail_id,NULL)
	                        , NVL(DECODE(inln_txn.c504_consignment_id,NULL,'', inln_txn.c504_consignment_id || '(' || inln_txn.c505_item_qty || ')'),gm_pkg_sm_eventbook_rpt.get_open_txn (t526.C525_Product_Request_Id, t526.c205_part_number_id))) consgid 
				  , DECODE(t526.c526_status_fl,20,1,10,2,5,3,40,4,30,5,6) sortorder
				  , decode(t526.c207_set_id,NULL,1,0) seq, t526.c205_part_number_id pnum, t526.c207_set_id set_id
			   FROM t526_product_request_detail t526, t525_product_request t525,
					( SELECT t504.c504_consignment_id , t505.c205_part_number_id , t505.c505_item_qty   
                    FROM t504_consignment t504, t505_item_consignment t505
                    WHERE t504.c504_consignment_id= t505.c504_consignment_id
                      AND t504.c504_ref_id = p_requestid
                      AND t504.c504_type = 9110
        	      	  AND t505.c505_void_fl IS NULL
        	          AND t504.c504_void_fl IS NULL ) inln_txn
			  WHERE t525.c525_product_request_id = p_requestid
			    AND t525.c525_product_request_id = t526.c525_product_request_id
			    AND t525.c901_request_for = DECODE(p_txn_type,'4119',p_txn_type,t525.c901_request_for)
             	AND t526.c901_request_type = DECODE(p_txn_type,'4127',p_txn_type,t526.c901_request_type)  
			    AND t526.c205_part_number_id =  inln_txn.c205_part_number_id (+) 
			    AND t526.c526_void_fl IS NULL
			    AND t525.c525_void_fl IS NULL
			    AND (t525.C1900_company_id = v_loaner_filetr OR t525.c5040_plant_id = v_loaner_filetr )
				AND t525.C1900_company_id = t526.C1900_company_id
				AND t525.c5040_plant_id = t526.c5040_plant_id
	       ORDER BY seq,sortorder,reqdtlid
			  ) detail;  
		---the following fetch query for OUS code merge Delivery/Collection Slip	  
		OPEN p_req_cur
		 FOR
			 SELECT ROWNUM NO, t526.c526_product_request_detail_id reqdtlid
				 , DECODE (t526.c526_status_fl
						 , '20', get_loanerreq_consignment_id (t526.c526_product_request_detail_id, '10')
						 , t526.c526_product_request_detail_id
						 ) reqcnid
				 , t526.c207_set_id setid, get_set_name (t526.c207_set_id) setnm, t526.c526_qty_requested qty
				 ,t526.c526_required_date requireddt
				 , NVL(t526.c5010_tag_id,get_req_loaner_etch_id (t526.c526_product_request_detail_id)) etchid
				 , NVL(t526.c5010_tag_id,get_req_loaner_etch_id (t526.c526_product_request_detail_id)) etchid1
				 , gm_pkg_op_product_requests.get_set_no(get_req_loaner_etch_id (t526.c526_product_request_detail_id),get_loanerreq_consignment_id (t526.c526_product_request_detail_id, '')) REFID 
				 , DECODE (t526.c526_status_fl
				 		 , '5', 'Pending Approval'
						 , '10', 'Open'
						 , '20', 'Allocated'
						 , '30', 'Closed'
						 , '40', 'Cancelled'
						 , t526.c526_status_fl
						 ) status
				 , t526.c526_status_fl statusid, get_log_flag (t526.c526_product_request_detail_id, 1248) LOG
				 , get_shipinfo_cnt(NVL(get_loanerreq_consignment_id (t526.c526_product_request_detail_id, '10')
				 						,t526.c526_product_request_detail_id)) SHIP_INFO_CNT
			 FROM t526_product_request_detail t526
			 WHERE t526.c525_product_request_id = p_requestid AND t526.c526_void_fl IS NULL;
	END gm_fch_loaner_request_detail;
         
 


	
	/***********************************************************************************
	 * Author		: Rajkumar
	 * Description	: This Function is used to fetch the Set # associated for the Tag ID
	 * 
	 ************************************************************************************/
	FUNCTION get_set_no(
		p_tag_id	t504a_consignment_loaner.c504a_etch_id%TYPE,
		p_consign_id  t504a_consignment_loaner.C504_CONSIGNMENT_ID%TYPE
	) RETURN VARCHAR2
	IS
		v_set_no	VARCHAR2(20);
	BEGIN
		SELECT nvl(c504a_ref_id,' ') 
		   INTO v_set_no
		   FROM t504a_consignment_loaner
		   WHERE nvl(c504a_etch_id,'-999999') = nvl(p_tag_id,'-999999')
		  AND C504_CONSIGNMENT_ID = p_consign_id  
		  AND c504a_void_fl is null
		  AND C504a_Status_Fl != 60;
		RETURN v_set_no;
	END get_set_no;	
	
	
/*******************************************************
 * Purpose: This function is used fetch etch id
 * Created by Rshah
 *******************************************************/
--
	FUNCTION get_req_loaner_etch_id (
		p_requestid   IN   t526_product_request_detail.c526_product_request_detail_id%TYPE
	)
		RETURN VARCHAR2
	IS
		v_etch_id	   VARCHAR2 (20);
	BEGIN
		SELECT t504ac.c504a_etch_id
		  INTO v_etch_id
		  FROM t504a_consignment_loaner t504ac, t504a_loaner_transaction t504al
		 WHERE t504al.c526_product_request_detail_id = p_requestid
		   AND t504al.c504_consignment_id = t504ac.c504_consignment_id
		   and t504al.c504a_void_fl is null
		   AND ROWNUM = 1;

		RETURN v_etch_id;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN '';
	END get_req_loaner_etch_id; 

/*******************************************************
 * Purpose: This function is used fetch set status
 * By request detail id
 *******************************************************/
--
	FUNCTION get_set_status (
		p_requestid   IN   t526_product_request_detail.c526_product_request_detail_id%TYPE
	)
		RETURN VARCHAR2
	IS
		v_status	   VARCHAR2 (20);
	BEGIN
		SELECT gm_pkg_op_loaner.get_loaner_status(t504ac.c504a_status_fl)
		  INTO v_status
		  FROM t504a_consignment_loaner t504ac, t504a_loaner_transaction t504al
		 WHERE t504al.c526_product_request_detail_id = p_requestid
		   AND t504al.c504_consignment_id = t504ac.c504_consignment_id
		   AND t504al.c504a_void_fl IS NULL
		   AND t504ac.c504a_void_fl IS NULL
		   AND ROWNUM = 1;

		RETURN v_status;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN '';
	END get_set_status;
	
	
 /************************************************************************
 * Purpose: Procedure to save the request detail in parent and chile table and
   voiding of the records from the detail table.
   Created By Rshah
 ************************************************************************/
	PROCEDURE gm_sav_loanerrequest_detail (
		p_requestid    IN	t525_product_request.c525_product_request_id%TYPE
	  , p_surgery_dt   IN	Date
	  , p_inputstr	   IN	CLOB
	  , p_userid	   IN	t101_user.c101_user_id%TYPE
	)
	AS
		v_string	   CLOB := p_inputstr;
		v_substring    VARCHAR2 (4000);
		v_substr1	   VARCHAR2 (4000);
		v_substr2	   VARCHAR2 (4000);
		v_substr3	   VARCHAR2 (4000);
		v_substr4	  VARCHAR2 (4000);
		v_id		   VARCHAR2 (20);
		v_voidfl	   VARCHAR2 (1);
		v_voidreason   VARCHAR2 (20);
		v_shipped_dt   VARCHAR2 (10);
		v_statusfl	   VARCHAR2 (10);
		v_setid 	   t526_product_request_detail.c207_set_id%TYPE;
		v_consign_id   t504a_loaner_transaction.c504_consignment_id%TYPE;
		v_consignment_id   t504a_loaner_transaction.c504_consignment_id%TYPE;
		v_loaner_req_day T906_RULES.C906_RULE_VALUE%TYPE;
		v_etch_id        t504a_consignment_loaner.c504a_etch_id%TYPE;
		v_req_type		NUMBER;
		v_txn_type		NUMBER;
		v_void_reason_group  VARCHAR2 (10);
		v_planned_ship_dt   DATE;
		v_old_planned_ship_dt   DATE;
		v_pnum 		t526_product_request_detail.c205_part_number_id%TYPE;
		v_work_day_count  NUMBER;
		v_cn_status		VARCHAR2 (10); 
		v_pre_set_status NUMBER;
		v_set_alloc_to_new_req NUMBER;
		v_req_det_id t526_product_request_detail.c526_product_request_detail_id%TYPE;
		v_date_fmt VARCHAR2(100);
		v_ship_date VARCHAR2(100);
	BEGIN
		
		v_date_fmt := NVL(get_rule_value('DATEFMT','DATEFORMAT'),'MM/dd/yyyy');
		UPDATE t525_product_request
		   SET c525_surgery_date = p_surgery_dt
			 , c525_last_updated_date = CURRENT_DATE
			 , c525_last_updated_by = p_userid
		 WHERE c525_product_request_id = p_requestid;

		WHILE INSTR (v_string, '|') <> 0
		LOOP
			v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
			v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
			v_id		:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
			v_substr1	:= SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
			v_voidfl	:= SUBSTR (v_substr1, 1, INSTR (v_substr1, '^') - 1);
			v_substr2	:= SUBSTR (v_substr1, INSTR (v_substr1, '^') + 1);
			v_voidreason := SUBSTR (v_substr2, 1, INSTR (v_substr2, '^') - 1);
			v_substr3	:= SUBSTR (v_substr2, INSTR (v_substr2, '^') + 1);
			v_shipped_dt := SUBSTR (v_substr3, 1, INSTR (v_substr3, '^') - 1);
			v_etch_id := SUBSTR (v_substr3, INSTR (v_substr3, '^') + 1);
				
				BEGIN
				    SELECT NVL (c526_status_fl, -999), c901_request_type, c205_part_number_id, c526_required_date
					  INTO v_statusfl, v_req_type, v_pnum, v_old_planned_ship_dt
					  FROM t526_product_request_detail
					 WHERE c526_void_fl IS NULL 
					   AND c526_product_request_detail_id = v_id
					   AND c205_part_number_id IS NOT NULL
					 FOR UPDATE;  
				EXCEPTION
					WHEN NO_DATA_FOUND
					THEN
						v_statusfl 			  := NULL;
						v_req_type 			  := NULL; 
						v_pnum	   			  := NULL;
						v_old_planned_ship_dt := NULL;
				END;
				
				/**
				 * Taking the CN and the status and store it before processing 
				 * the request. If the current status is Pending Shipping or
				 * Pending Putaway, the set need to be in the same status
				 * after the request is processed.
				 */
				
				BEGIN
					SELECT t504cn.c504_consignment_id, t504cn.c504a_status_fl
					  INTO  v_consignment_id, v_pre_set_status
					  FROM t504a_loaner_transaction t504a
					     , t504a_consignment_loaner t504cn
					     , t504_consignment t504
					 WHERE t504a.c504_consignment_id = t504cn.c504_consignment_id 
					   AND t504.c504_consignment_id = t504a.c504_consignment_id
					   AND t504a.c504a_void_fl IS NULL 
					   AND t504cn.c504a_void_fl IS NULL 
					   AND t504.c504_void_fl IS NULL
					   AND t504a.c504a_return_dt IS NULL
					   AND t504a.c526_product_request_detail_id = v_id
					   FOR UPDATE;
				EXCEPTION
						WHEN NO_DATA_FOUND
				THEN
				 	v_consignment_id := NULL;
					v_pre_set_status := NULL;
				END;
				
			IF (v_voidfl = 'Y')
			THEN
			   v_txn_type := get_request_type(v_id);
			   
			   IF(v_txn_type = '4127')
			   THEN
			   v_void_reason_group := 'VDLON';
			   ELSIF(v_txn_type = '4119')
			   THEN
			   v_void_reason_group := 'VDIHLN';
			   END IF;
			   
			   gm_pkg_common_cancel.gm_cm_sav_cancelrow (v_id, TO_NUMBER (v_voidreason), v_void_reason_group, '', p_userid);
			   		   
			ELSIF v_req_type = '4119' AND v_pnum IS NOT NULL THEN /* This block will execute for Inhouse Loaner Item only.*/

				v_planned_ship_dt := TO_DATE (v_shipped_dt, get_compdtfmt_frm_cntx());
				--
				IF v_old_planned_ship_dt <> v_planned_ship_dt THEN
					/*update item request ship date if new ship date is with 4 days and status is open then it will call allocation prc*/
					gm_pkg_op_ihloaner_item_txn.gm_sav_item_req_ship_date(p_requestid,v_id,v_statusfl,v_req_type,v_pnum,v_planned_ship_dt,p_userid);
				END IF;
				
			ELSE			 
			 
				SELECT	   NVL (c526_status_fl, -999), c207_set_id, C901_REQUEST_TYPE, c526_required_date
					  INTO v_statusfl, v_setid, v_req_type, v_old_planned_ship_dt
					  FROM t526_product_request_detail
					 WHERE c526_void_fl IS NULL AND c526_product_request_detail_id = v_id
				FOR UPDATE;

				IF v_statusfl IN (30, 40)
				THEN
					raise_application_error (-20941, '');
				END IF;
				
				/*
				 * If the request is in allocated status and the planned ship date is not 
				 * changed by the user, then do not proceed on that request. 
				 */
				IF v_statusfl IN (20) AND v_old_planned_ship_dt = TO_DATE(v_shipped_dt,get_compdtfmt_frm_cntx())
				THEN
					GOTO end_loop;
				END IF;
				
				/*
				 * If the set is in 'Packing In Progress' or 'Ready for Pickup'
				 * do not proceed, as the set needed to be rolled back to
				 * do the processing.
				 */
				
				IF v_pre_set_status IN ('13','16') 
				THEN
					GM_RAISE_APPLICATION_ERROR('-20999','267',v_consignment_id);
				END IF;
				
				
				v_loaner_req_day := TO_NUMBER(get_rule_value (v_req_type, 'LOANERREQDAY')); 
				v_ship_date := to_char(TO_DATE (v_shipped_dt, get_compdtfmt_frm_cntx()),v_date_fmt); 
				v_work_day_count := get_work_days_count(TO_CHAR(CURRENT_DATE,v_date_fmt),v_ship_date);
            	            
		    	IF v_etch_id IS NOT NULL AND v_work_day_count > v_loaner_req_day THEN
			  		GM_RAISE_APPLICATION_ERROR('-20999','268',''); 
				END IF;
				
				v_planned_ship_dt := TO_DATE (v_shipped_dt, get_compdtfmt_frm_cntx());
				
				UPDATE t526_product_request_detail
				   SET c526_required_date = v_planned_ship_dt
					 , c526_last_updated_by = p_userid
					 , c526_last_updated_date = CURRENT_DATE
				 WHERE c526_product_request_detail_id = v_id;
				 
				 -- update allocated fl and planned ship date in t504a
				gm_pkg_op_loaner_allocation.gm_loaner_allocation_update(v_id, 'ADD', p_userid); 
				
				/*
				 *  To swap the CN from an
				 * an allocated request to a open request,
				 * the following procedure is called by passing
				 * the new request id and the etch id from the
				 * old request id
				 */
				
				IF v_etch_id IS NOT NULL THEN
				   gm_upd_tag_loaners(v_id, v_etch_id, p_userid);
				END IF;             

				-- If need to ship today 
				IF v_work_day_count <= v_loaner_req_day
				THEN
					IF v_statusfl = 20 AND TO_DATE (v_shipped_dt, get_compdtfmt_frm_cntx()) = TRUNC (CURRENT_DATE)-- already allocated
					THEN
						gm_pkg_op_loaner.gm_sav_status_update (v_id, p_userid);
					ELSIF v_statusfl = 10 AND v_setid IS NOT NULL	-- still open
					THEN					 
						gm_pkg_op_loaner.gm_sav_allocate (v_setid, v_req_type, p_userid);
					END IF;
				/*
				 * If the planned ship date of a request is changed to a future date
				 * and the set is in allocated status, deallocate the set 
				 * from the request, and run the allocation engine for the 
				 * set to find any other open request
				 */
				
				ELSIF v_statusfl = 20
				THEN      
				
					BEGIN 
						SELECT c504_consignment_id 
						   INTO v_consign_id
						   FROM t504a_loaner_transaction 
						  WHERE c526_product_request_detail_id=v_id
						    AND c504a_void_fl IS NULL;
					EXCEPTION WHEN NO_DATA_FOUND THEN
							v_consign_id := NULL;
					END;
					
					IF v_consign_id IS NOT NULL THEN
					
				          SELECT t504cl.c504a_status_fl
						   INTO v_cn_status
						   FROM t504a_loaner_transaction t504a, t526_product_request_detail t526, t504a_consignment_loaner t504cl
						  WHERE t526.c526_product_request_detail_id = t504a.c526_product_request_detail_id
						    AND t504cl.c504_consignment_id          = t504a.c504_consignment_id
						    AND t526.c526_product_request_detail_id = v_id
						    AND t504cl.c504a_void_fl               IS NULL
						    AND t504a.c504a_void_fl                IS NULL
						    AND t526.c526_status_fl NOT            IN (30, 40)
						    AND t526.c901_request_type              = v_req_type ;
                
            				IF v_cn_status = '13'
                            THEN
                                  GM_RAISE_APPLICATION_ERROR('-20999','269',v_consign_id);
                            END IF;
                            
                            IF v_cn_status = '16'
                            THEN
                                  GM_RAISE_APPLICATION_ERROR('-20999','269',v_consign_id);
                            END IF; 
                    
						gm_pkg_op_loaner.gm_sav_deallocate(v_consign_id,p_userid);
						--after deallocated , should auto allocate the set to any open request
						gm_pkg_op_loaner.gm_sav_allocate (v_setid, v_req_type, p_userid);
					END IF;
				   
				END IF;

				/*
				 * If the set is in Pending Shipping, there will be a entry in shipping table
				 * but if the de allocation is called above then the shipping need to be 
				 * rolled back, following code is used to rollback the shipping.
				 */
				
				IF TO_DATE (v_shipped_dt, get_compdtfmt_frm_cntx()) > TRUNC (CURRENT_DATE)
				THEN
					v_consign_id := gm_pkg_op_product_requests.get_loanerreq_consignment_id (v_id, 10);   --pending shipping

					IF v_consign_id IS NOT NULL
					THEN
						gm_pkg_common_cancel.gm_cs_sav_rollback_loaner_ship (v_consign_id, p_userid,'N');
					END IF;
				END IF;
			END IF;
			
			/**
			 * Check if the CN is allocated to a new request, 
			 * need to retain the status of the CN if it was in 
			 * "Pending Shipping" or "Pending Putaway"
			 */
			
			BEGIN
				SELECT t504a.C526_Product_Request_Detail_Id
				  INTO v_req_det_id
				  FROM t504a_loaner_transaction t504a
				     , t504a_consignment_loaner t504cn
				 WHERE t504a.c504_consignment_id = t504cn.c504_consignment_id 
				   AND t504a.c504a_void_fl IS NULL 
				   AND t504cn.c504a_void_fl IS NULL 
				   AND t504a.c504a_return_dt IS NULL
				   AND t504a.c504_consignment_id = v_consignment_id
				   AND ROWNUM = 1;

			EXCEPTION WHEN NO_DATA_FOUND THEN
				v_req_det_id := NULL;
			END;
		   
			   -- 10 Pending Shipping, 58 Pending Putaway
			    
		   	IF  v_req_det_id IS NOT NULL AND v_pre_set_status in ( 10 , 58) 
		   	THEN 		   
			   	 gm_pkg_op_loaner_set_txn.gm_upd_consign_loaner_status (v_consignment_id, v_pre_set_status, p_userid) ;
				 
				 --after set allocated to new request, need update ship record
				 IF v_pre_set_status in ( 10 ) 
				 THEN
					  --update shiping info refid to CN ID
					 gm_pkg_op_loaner_set_txn.gm_upd_shipping_ref (v_consignment_id, v_req_det_id, p_userid) ;
					 -- to call the shipping package
					 gm_pkg_cm_shipping_trans.gm_sav_release_shipping (v_consignment_id, 50182, p_userid, NULL) ; --50182 - Loaner
				 END IF;
		   	END IF;
		   	
		    <<end_loop>>  -- not allowed unless an executable statement follows
   			NULL; -- add NULL statement to avoid error

		END LOOP;
	 	-- Call the priority engine to prioritize the set
		gm_pkg_op_ln_priority_engine.gm_sav_main_ln_set_priority(p_requestid,p_userid);
	END gm_sav_loanerrequest_detail;

	/************************************************************************
	* Purpose: Procedure to call allocate process after loaner initiate
	  Created By Bgupta
	************************************************************************/
	PROCEDURE gm_sav_allocate_requests (
		p_requestid   IN   t525_product_request.c525_product_request_id%TYPE
	  , p_type		  IN   t525_product_request.c901_request_for%TYPE
	  , p_userid	  IN   t101_user.c101_user_id%TYPE
	)
	AS
		CURSOR request_detail_cursor
		IS
			SELECT	 c207_set_id setid	 --, c526_product_request_detail_id reqdetailid
				FROM t526_product_request_detail
			   WHERE c525_product_request_id = p_requestid AND c526_void_fl IS NULL AND c901_request_type = p_type
			ORDER BY c525_product_request_id;
	BEGIN
		FOR curindex IN request_detail_cursor
		LOOP
			gm_pkg_op_loaner.gm_sav_allocate (curindex.setid, p_type, p_userid);
		END LOOP;
	END gm_sav_allocate_requests;

/*******************************************************
 * Purpose: This function is used fetch consignment id for specific request id
 * Created by Rshah
 *******************************************************/
--
	FUNCTION get_loanerreq_consignment_id (
		p_requestid   IN   t526_product_request_detail.c526_product_request_detail_id%TYPE
	  , p_status	  IN   t504a_consignment_loaner.c504a_status_fl%TYPE
	)
		RETURN VARCHAR2
	IS
		v_cn_id 	   VARCHAR2 (20);
	BEGIN
		SELECT trim(t504al.c504_consignment_id)
		  INTO v_cn_id
		  FROM t504a_consignment_loaner t504ac, t504a_loaner_transaction t504al
		 WHERE t504al.c526_product_request_detail_id = p_requestid
		   AND t504al.c504_consignment_id = t504ac.c504_consignment_id
		   AND t504ac.c504a_status_fl = NVL(p_status,t504ac.c504a_status_fl)
		   AND t504al.C504a_void_fl IS NULL
		   AND t504ac.C504a_void_fl IS NULL
		   AND ROWNUM = 1;

		RETURN v_cn_id;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN '';
	END;

--

	/************************************************************************
	* Purpose: Procedure that calculates the number of times the missing parts email is sent
	  Created By BrinalG
	************************************************************************/
	PROCEDURE gm_sav_email_counter (
		p_request_detailid   IN   t526_product_request_detail.C526_PRODUCT_REQUEST_DETAIL_ID%TYPE
	  , p_userid	  IN   t101_user.c101_user_id%TYPE
	)
	AS
		CURSOR inhouse_trans
		IS
			SELECT t412.c412_inhouse_trans_id inhousetransid
			  FROM t412_inhouse_transactions t412, t504a_loaner_transaction t504a, t526_product_request_detail t526
			 WHERE t412.c412_status_fl < 4
			   AND t412.c412_type = 50159
			   AND t526.c526_product_request_detail_id = p_request_detailid
			   AND t504a.c504a_loaner_transaction_id = t412.c504a_loaner_transaction_id
			   AND t526.c526_product_request_detail_id = t504a.c526_product_request_detail_id
			   AND t412.c412_void_fl IS NULL
			   AND t504a.c504a_void_fl IS NULL
			   AND t526.c526_void_fl IS NULL;

		v_count 	   NUMBER;
		v_add_month    NUMBER;
		v_day		   VARCHAR2 (10);
		v_final_dd	   VARCHAR2 (2);
	BEGIN
		UPDATE t525_product_request
		   SET c525_email_count = NVL (c525_email_count, 0) + 1
			 , c525_email_date = TRUNC (CURRENT_DATE)
			 , c525_last_updated_by = p_userid
			 , c525_last_updated_date = CURRENT_DATE
		 WHERE c525_product_request_id IN (SELECT c525_product_request_id FROM t526_product_request_detail WHERE c526_product_request_detail_id = p_request_detailid) AND c525_void_fl IS NULL;

-- changing the logic of expected return date from +30 days to 8th of following month.
-- changing the logic of expected return date from 8th of following month to 1st
-- eg - if 1st email on 5/10/2010	then exp ret date shud be 6/1/2010
--		if 1st email on 5/25/2010	then exp ret date shud be 7/1/2010

		--if CURRENT_DATE.day	>= 21 then add add 2 months else add 1 month
		SELECT TO_CHAR (CURRENT_DATE, 'DD'), get_rule_value ('FINAL', 'EMAIL')
		  INTO v_day, v_final_dd
		  FROM DUAL;

		IF v_day >= v_final_dd
		THEN
			v_add_month := 2;
		ELSE
			v_add_month := 1;
		END IF;

		FOR currindex IN inhouse_trans
		LOOP
			UPDATE t412_inhouse_transactions
			   SET c412_expected_return_date =
					   TO_DATE (   TO_CHAR (ADD_MONTHS (CURRENT_DATE, v_add_month), 'MM')
								|| '/01/'
								|| TO_CHAR (ADD_MONTHS (CURRENT_DATE, v_add_month), 'YYYY')
							  , GET_RULE_VALUE('DATEFMT', 'DATEFORMAT')
							   )
				 , c412_last_updated_date = CURRENT_DATE
				 , c412_last_updated_by = p_userid
			 WHERE c412_inhouse_trans_id = currindex.inhousetransid AND c412_void_fl IS NULL;
		END LOOP;
	END gm_sav_email_counter;
	
	/************************************************************************
	* Purpose: Procedure for loaner Reconciliation sumarry report
	  Author : dreddy
	************************************************************************/
	PROCEDURE gm_find_requests(
	p_dist_id   IN  t525_product_request.c525_request_for_id%TYPE,
	p_rep_id    IN  t525_product_request.c703_sales_rep_id%TYPE,
	p_status_fl IN  VARCHAR2,
	p_from_date IN  VARCHAR2,
	p_to_date   IN  VARCHAR2,
	p_cnt 		OUT NUMBER
	)
	AS
	
	v_company_id  t1900_company.c1900_company_id%TYPE;
	v_date_fmt VARCHAR2(100);
	v_plant_id T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
    v_filter T906_RULES.C906_RULE_VALUE%TYPE;
	BEGIN
	
	select get_compid_frm_cntx(),get_compdtfmt_frm_cntx(),get_plantid_frm_cntx into v_company_id,v_date_fmt,v_plant_id from dual;
	  
	SELECT DECODE(GET_RULE_VALUE(v_company_id,'REQUEST_SUMMARY'), null, v_company_id, v_plant_id) --REQUEST_SUMMARY- To fetch the records for EDC entity countries based on plant
      INTO v_filter 
      FROM DUAL;

       DELETE FROM my_temp_list;
	 
	 INSERT INTO my_temp_list
	 SELECT t525.c525_product_request_id 
  		   	 FROM t525_product_request t525
			WHERE t525.c525_request_for_id = NVL (p_dist_id, c525_request_for_id)
              AND t525.c703_sales_rep_id = NVL (p_rep_id, t525.c703_sales_rep_id)
              AND NVL (t525.c525_status_fl, -999) =
                 (CASE
                     WHEN (p_status_fl = '0')
                     THEN NVL (t525.c525_status_fl, -999)
               		 WHEN (p_status_fl = '10057') -- 'Reconciled' 
                 	 THEN 40
                     WHEN (p_status_fl = '10058') -- 'UnReconciled'
                     THEN (SELECT DISTINCT t525.c525_status_fl
                             FROM t525_product_request
                            WHERE t525.c525_status_fl IS NOT NULL
                              AND t525.c525_status_fl != 40)
           END
           )
           AND  t525.c525_requested_date
       BETWEEN decode(p_from_date,'',t525.c525_requested_date,to_date(p_from_date, v_date_fmt))
           AND decode(p_to_date,'',t525.c525_requested_date,to_date(p_to_date,  v_date_fmt))
           AND t525.c525_void_fl is NULL
           AND (t525.c1900_company_id = v_filter
		   OR t525.c5040_plant_id   = v_filter);
           
           SELECT COUNT(1) INTO p_cnt  FROM my_temp_list;
           
END gm_find_requests;
	/************************************************************************
	 * Purpose: Procedure to swap the tags
	 * Created By Gopinathan
	 ************************************************************************/
	PROCEDURE gm_upd_tag_loaners (
	   p_requestid 	IN	t526_product_request_detail.c526_product_request_detail_id%TYPE
	 , p_new_tag	IN	t504a_consignment_loaner.c504a_etch_id%TYPE
	 , p_user_id	IN	t101_user.c101_user_id%TYPE
	)
	AS
		v_statusfl	 		VARCHAR2 (10);
		v_new_tag_requestid t526_product_request_detail.c526_product_request_detail_id%TYPE;
		v_consign_id 		t504a_loaner_transaction.c504_consignment_id%TYPE;
		v_loaner_dt			t504a_consignment_loaner.c504a_loaner_dt%TYPE;
		v_exp_return_dt 	t504a_consignment_loaner.c504a_expected_return_dt%TYPE;
		v_status_fl			t504a_consignment_loaner.c504a_status_fl%TYPE;
		v_dist_id			t504_consignment.c701_distributor_id%TYPE;
		v_rep_id            t525_product_request.c703_sales_rep_id%TYPE;
		v_acc_id			t504_consignment.c704_account_id%TYPE;
		v_new_tag_parent_req_id t526_product_request_detail.c525_product_request_id%TYPE;
		v_parent_req_id 	t526_product_request_detail.c525_product_request_id%TYPE;
		v_set_id 			t526_product_request_detail.c207_set_id%TYPE;
		v_assoc_rep_id		t525_product_request.c703_ass_rep_id%TYPE;
		v_comments 			VARCHAR2(100);
		v_ref_id 			VARCHAR2(20);
		v_message 			VARCHAR2(100);
		v_req_type          NUMBER;
		v_req_by_type       NUMBER;
		v_req_for_id        NUMBER;
		v_company_id  t1900_company.c1900_company_id%TYPE;
  		v_plant_id 	  t5040_plant_master.c5040_plant_id%TYPE;
        v_req_company_id  t1900_company.c1900_company_id%TYPE;   

	BEGIN
		
		SELECT get_compid_frm_cntx(), get_plantid_frm_cntx()
     	INTO v_company_id, v_plant_id
     	FROM DUAL;
     

     	SELECT	 NVL (c526_status_fl, -999), c207_set_id , c901_request_type , c1900_company_id
 		  INTO v_statusfl, v_set_id , v_req_type , v_req_company_id
		  FROM t526_product_request_detail
         WHERE c526_void_fl IS NULL 
           AND c526_product_request_detail_id = p_requestid;

		IF v_statusfl NOT IN (10) THEN -- check if the old request is in status open, if not then throw an error
			raise_application_error (-30410,''); 

		ELSE
			BEGIN
			
				-- get the new CN# using etch_id
				SELECT t504a.c504_consignment_id
				  INTO v_consign_id
				  FROM t504a_consignment_loaner t504a, t504_consignment t504
				 WHERE LOWER(C504A_ETCH_ID) LIKE LOWER('%'||p_new_tag)
				   AND t504a.c504_consignment_id = t504.c504_consignment_id
				   AND t504.c207_set_id = v_set_id
                   AND t504.c504_void_fl IS NULL
				   AND t504a.c504a_void_fl IS NULL
				   AND t504a.c504a_status_fl IN (5,7,10) --available/pending pick/allocated
				   AND t504.c5040_plant_id = v_plant_id 
				   FOR UPDATE;
				
				SELECT t526.c525_product_request_id, t526.c526_product_request_detail_id
			  	  INTO v_new_tag_parent_req_id, v_new_tag_requestid
			      FROM t504a_loaner_transaction t504a, t526_product_request_detail t526
			     WHERE t504a.c504_consignment_id           = v_consign_id
			       AND t526.c526_product_request_detail_id = t504a.c526_product_request_detail_id
			       AND t526.c526_status_fl                 = 20
			       AND t526.c901_request_type              = v_req_type
			       AND t526.c526_void_fl                  IS NULL
			       AND t504a.c504a_void_fl                IS NULL
			       FOR UPDATE;
			       
				EXCEPTION
				WHEN NO_DATA_FOUND
				THEN
					GM_RAISE_APPLICATION_ERROR('-20999','270',p_new_tag||'#$#'||p_requestid||'#$#'||v_set_id); -- New Consignment ID doesn't exist/allocated.
	           WHEN TOO_MANY_ROWS 
	           THEN
	           GM_RAISE_APPLICATION_ERROR('-20999','344','');
			END;						

			-- reset allocated fl and planned ship date in t504a
			gm_pkg_op_loaner_allocation.gm_loaner_allocation_update(v_new_tag_requestid, 'CLEAR', p_user_id);	
			
			--allocated request reverted to open status	
			UPDATE t504a_loaner_transaction
			   SET C504a_void_fl = 'Y'
				 , C504a_last_updated_date = CURRENT_DATE
				 , c504a_last_updated_by = p_user_id
			 WHERE c526_product_request_detail_id = v_new_tag_requestid
			   AND C504a_void_fl IS NULL;  
						   
			UPDATE t526_product_request_detail
			   SET c526_status_fl = '10' --open
				 , c526_last_updated_by = p_user_id
				 , c526_last_updated_date = CURRENT_DATE
			 WHERE c526_product_request_detail_id = v_new_tag_requestid 
			   AND c901_request_type =  v_req_type
			   AND c526_void_fl IS NULL; 
						   
			UPDATE T907_SHIPPING_INFO
			   SET c907_ref_id = TO_CHAR (v_new_tag_requestid)
			     , c907_status_fl = 15
				 , c907_last_updated_by = p_user_id
				 , c907_last_updated_date = CURRENT_DATE
			 WHERE c525_product_request_id = v_new_tag_parent_req_id
			   AND c907_status_fl IN(15,30)
			   AND c907_ref_id = v_consign_id
			   AND c907_void_fl IS NULL;
				
			--T504A_CONSIGNMENT_LOANER table - update c504a_loaner_dt, c504a_expected_return_dt and status_fl for new cn# and clear these fields for the CN# the system allocated.

			 SELECT t525.c525_product_request_id, t526.c526_required_date,t525.c525_request_for_id, t525.c704_account_id, t525.c703_sales_rep_id
			 		, t526.c526_exp_return_date , t525.c901_request_by_type , t525.c525_request_for_id, t525.c703_ass_rep_id
               INTO v_parent_req_id, v_loaner_dt, v_dist_id, v_acc_id, v_rep_id ,v_exp_return_dt 
               		, v_req_by_type , v_req_for_id, v_assoc_rep_id 
               FROM t525_product_request t525,t526_product_request_detail t526
              WHERE t526.c526_product_request_detail_id = p_requestid
                AND t525.c525_product_request_id = t526.c525_product_request_id
                AND t526.c526_void_fl IS NULL
                AND t525.c525_void_fl IS NULL;
       
        IF v_req_type = '4127' THEN
            SELECT gm_pkg_op_loaner.get_loaner_expected_return_dt(v_loaner_dt)--v_loaner_dt + get_rule_value ('DATE','EXPRTDT')
              INTO v_exp_return_dt
              FROM DUAL;
        END IF;
        -- Call the common procedure to update the status
        gm_pkg_op_loaner_set_txn.gm_upd_consign_loaner_status (v_consign_id, 5, p_user_id) ;
        
			UPDATE T504A_CONSIGNMENT_LOANER
			   SET c504a_loaner_dt = v_loaner_dt
			     , c504a_expected_return_dt = v_exp_return_dt
			     , C504a_last_updated_date = CURRENT_DATE
			     , c504a_last_updated_by = p_user_id
			 WHERE c504_consignment_id = v_consign_id
			   AND C504a_void_fl IS NULL;

			--T504A_LOANER_TRANSACTION - update the new CN# in C504_CONSIGNMENT_ID field for the relevant product_request_detail_id

			UPDATE t504a_loaner_transaction
			   SET c504_consignment_id = v_consign_id
			     , C504a_last_updated_date = CURRENT_DATE
			     , c504a_last_updated_by = p_user_id
			 WHERE c526_product_request_detail_id = p_requestid
			   AND C504a_void_fl IS NULL;

		    IF (SQL%ROWCOUNT = 0) THEN 		
		    
			      INSERT INTO t504a_loaner_transaction
			                  (c504a_loaner_transaction_id, c504_consignment_id,
			                   c504a_loaner_dt, c504a_expected_return_dt,
			                   c901_consigned_to, c504a_consigned_to_id,
			                   c504a_created_by, c504a_created_date, c703_sales_rep_id,
			                   c704_account_id, c526_product_request_detail_id,
			                   c703_ass_rep_id,c1900_company_id,c5040_plant_id
			                  )
			           VALUES (s504a_loaner_trans_id.NEXTVAL, v_consign_id,
			                   v_loaner_dt, v_exp_return_dt,
			                   DECODE(v_req_by_type,'50625','50172','50626','50170'), 
			                   v_req_for_id,
			                   p_user_id, CURRENT_DATE, v_rep_id,
			                   v_acc_id, p_requestid,
			                   v_assoc_rep_id,v_req_company_id,v_plant_id
			                  );
			END IF;

			--T504_CONSIGNMENT - update C701_DISTRIBUTOR_ID,C704_ACCOUNT_ID for the new consignment id and clear these field for the old CN

			UPDATE t504_consignment
			   SET c701_distributor_id = DECODE(v_req_by_type,'50625',NULL,v_req_for_id)
			     , c704_account_id = v_acc_id
			     , c504_last_updated_date = CURRENT_DATE
			     , c504_last_updated_by = p_user_id
			 WHERE c504_consignment_id = v_consign_id
			   AND c504_void_fl IS NULL;

			IF TRUNC(v_loaner_dt) = TRUNC(CURRENT_DATE) THEN
        		gm_pkg_op_loaner.gm_sav_status_update (p_requestid, p_user_id);
        	ELSE
	        	UPDATE t907_shipping_info
		           SET c907_ref_id = TO_CHAR (p_requestid),
		               c907_last_updated_date = CURRENT_DATE,
		               c907_last_updated_by = p_user_id
		         WHERE c907_ref_id = TO_CHAR (p_requestid)
		           AND c901_source = 50182
		           AND c907_void_fl IS NULL;
        	END IF;
        	
        	UPDATE t526_product_request_detail
			   SET c526_status_fl = '20', --allocated
				   c526_last_updated_by = p_user_id,
				   c526_last_updated_date = CURRENT_DATE
			 WHERE c526_product_request_detail_id = p_requestid 
			   AND c526_void_fl IS NULL; 
			   
			-- reset allocated fl and planned ship date in t504a
			gm_pkg_op_loaner_allocation.gm_loaner_allocation_update(p_requestid, 'ADD', p_user_id); 
		END IF;
		
		-- Need to call the engine to prioritize the set for old request
		gm_pkg_op_ln_priority_engine.gm_sav_main_ln_set_priority(v_new_tag_parent_req_id,p_user_id);
	END gm_upd_tag_loaners;
	
	--
 /*******************************************************
 * Purpose: This Procedure is used fetch Total pending approval Requests
 *		by Set ID/Part Num
 *******************************************************/
--
	FUNCTION gm_fch_product_pndaprreq_count (
		p_setid 	IN	 t526_product_request_detail.c207_set_id%TYPE
	  , p_partnum	IN	 t526_product_request_detail.c205_part_number_id%TYPE
	  , p_type		IN	 t526_product_request_detail.c901_request_type%TYPE
	)
		RETURN NUMBER
	IS
		--
		v_cnt		   NUMBER;
--
	BEGIN
		SELECT	 SUM (c526_qty_requested)
			INTO v_cnt
			FROM t526_product_request_detail
		   WHERE c207_set_id = p_setid AND c901_request_type = p_type AND c526_status_fl = 5 AND c526_void_fl IS NULL
		GROUP BY c207_set_id;

		RETURN v_cnt;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN 0;
	END gm_fch_product_pndaprreq_count;
--
/************************************************************************
	 * Purpose: Procedure to fetch the Shipping Details for the print only to print sets not items
	 * Created By Hreddi
	 ************************************************************************/
PROCEDURE gm_fch_loaner_shipping_prntdtl (
	   p_requestid 		 IN	t526_product_request_detail.c526_product_request_detail_id%TYPE
	 , p_print_fl		 IN	VARCHAR2	
	 , p_txn_type		 IN t526_product_request_detail.C901_REQUEST_TYPE%TYPE 
	 , p_out_shipDetails OUT TYPES.cursor_type
	)
	AS
		v_query	 		VARCHAR2(4000);
		v_condition 	VARCHAR2(500):='';
		v_txn_type	    VARCHAR2(100):='';
		v_date_fmt      VARCHAR2(50);
	BEGIN
		v_date_fmt := get_rule_value('DATEFMT','DATEFORMAT');
		
		IF p_print_fl = 'Y' THEN
		    	v_condition := ' AND t526.c526_status_fl NOT IN (40) ORDER BY MODEGROUP,reqdtlid';	   
	    ELSE
	    		v_condition := ' ORDER BY seq,sortorder,reqdtlid';
	    END IF;
	    --PC-3890 Add Pending Put away status in decode condition to include status in print paperwork
		v_query := 'SELECT ROWNUM NO
				  ,	t526.c526_product_request_detail_id reqdtlid
				  , NVL(DECODE (t526.c526_status_fl,''20'', gm_pkg_op_product_requests.get_loanerreq_consignment_id(t526.c526_product_request_detail_id,NULL), NULL),''N'') reqcnid
				  , NVL(t526.c207_set_id,T526.C205_Part_Number_Id ) setid
				  , DECODE(t526.c207_set_id,NULL,Get_Partnum_Desc (T526.C205_Part_Number_Id),get_set_name (t526.c207_set_id)) setnm
				  , t526.c526_request_flag reqfl
			      , DECODE(t526.c526_request_flag,''Y'',''**''||t526.c207_set_id ,t526.c207_set_id) set_id_print
				  , gm_pkg_op_product_requests.get_req_loaner_etch_id (t526.c526_product_request_detail_id) loaneretchid
				  , t526.c526_status_fl statusid
				  , to_char(t526.c526_required_date,'''|| v_date_fmt ||''') shipdate
				  , t906.C906_RULE_VALUE delmodealt
                  , get_code_name(t907.c901_delivery_carrier)delcarrier
				  , DECODE(t907.c901_ship_to,''4121'',Gm_pkg_cm_shipping_info.get_salesrepaddress(t907.c106_address_id,t907.c907_ship_to_id)
					,''4123'',Gm_pkg_cm_shipping_info.get_empaddress(t907.c106_address_id,t907.c907_ship_to_id)
					,gm_pkg_cm_shipping_info.get_address(t907.c901_ship_to,t907.c907_ship_to_id)) shipaddress
                  , t907.c907_tracking_number trackingno
                  , get_loaner_request_status(t526.c526_product_request_detail_id) status
				  , gm_pkg_op_product_requests.get_loanerreq_consignment_id (t526.c526_product_request_detail_id,NULL) consgid 
                  , DECODE(t526.c207_set_id,NULL,1,0) seq, DECODE(t526.c526_status_fl,20,1,10,2,5,3,40,4,30,5,6) sortorder
				  , DECODE(t907.c901_ship_to,''4121'',Gm_pkg_cm_shipping_info.get_salesrepaddress(t907.c106_address_id,t907.c907_ship_to_id),gm_pkg_cm_shipping_info.get_address(t907.c901_ship_to,t907.c907_ship_to_id)) ||'',''|| t906.C906_RULE_VALUE MODEGROUP
				  , t907.c901_source source
               FROM (SELECT t526.* , NVL(DECODE (t526.c526_status_fl, ''20'',
                  					DECODE(gm_pkg_op_product_requests.get_set_status (t526.c526_product_request_detail_id),''Pending Pick'',TO_CHAR(t526.c526_product_request_detail_id),''Pending Putaway'', to_char(t526.c526_product_request_detail_id),t504a.c504_consignment_id),''30'' , 
                  					t504a.c504_consignment_id,t526.c526_product_request_detail_id),t526.c526_product_request_detail_id) ref_id
  						FROM t526_product_request_detail t526 
    						, t504a_loaner_transaction t504a
  						WHERE t526.c526_product_request_detail_id = t504a.c526_product_request_detail_id (+)
  						AND t526.c525_product_request_id          = '|| p_requestid||'
  						AND t526.c526_void_fl is null 
  						AND t504a.c504a_void_fl is null) t526 
                  , t907_shipping_info t907
                  , t906_rules t906
                  , t525_product_request t525
			  WHERE t525.c525_product_request_id = '|| p_requestid||'
				AND t525.c525_product_request_id = t526.c525_product_request_id	 
				AND t525.c901_request_for = DECODE('||p_txn_type||',''4119'','||p_txn_type||',t525.c901_request_for)
             	AND t526.c901_request_type = DECODE('||p_txn_type||',''4127'','||p_txn_type||',t526.c901_request_type)  
				AND t525.c525_void_fl IS NULL
				AND t907.c525_product_request_id = t526.c525_product_request_id 			                    
                AND t907.c907_ref_id 			= t526.ref_id
                AND t906.C906_RULE_GRP_ID(+) = ''SHIPMODEALT'' 
                AND t907.c901_delivery_mode = t906.C906_RULE_ID(+)
                AND t526.c207_set_id IS NOT NULL
                AND t906.c906_void_fl(+) IS NULL
				AND t526.c526_void_fl IS NULL
				AND t907.c907_void_fl IS NULL ';
				
        OPEN p_out_shipDetails FOR v_query || v_condition ;

	END gm_fch_loaner_shipping_prntdtl;	
/************************************************************************
	 * Purpose: Procedure to fetch the Shipping Details
	 * Created By Hreddi
	 ************************************************************************/	
	PROCEDURE gm_fch_loaner_shipping_details (
	   p_requestid 		 IN	t526_product_request_detail.c526_product_request_detail_id%TYPE
	 , p_print_fl		 IN	VARCHAR2	
	 , p_txn_type		 IN t526_product_request_detail.C901_REQUEST_TYPE%TYPE 
	 , p_out_shipDetails OUT TYPES.cursor_type
	)
	AS
		v_query	 		VARCHAR2(4000);
		v_condition 	VARCHAR2(500):='';
		v_txn_type	    VARCHAR2(100):='';
		v_company_id  t1900_company.c1900_company_id%TYPE;
		v_plant_id T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
		v_loaner_filter  VARCHAR2(20);
	BEGIN
		
		select get_compid_frm_cntx(),get_plantid_frm_cntx() into v_company_id,v_plant_id from dual;
		
		SELECT DECODE(GET_RULE_VALUE(v_company_id,'LOANEREDITREQFILTER'), null, v_company_id, v_plant_id) --LOANEREDITREQFILTER- To fetch the records for EDC entity countries based on plant
          INTO v_loaner_filter 
          FROM DUAL;
		
		OPEN p_out_shipDetails
		FOR 
			SELECT ROWNUM NO
				  ,	t526.c526_product_request_detail_id reqdtlid
				  , NVL(DECODE (t526.c526_status_fl,'20', gm_pkg_op_product_requests.get_loanerreq_consignment_id(t526.c526_product_request_detail_id,NULL), NULL),'N') reqcnid
				  , NVL(t526.c207_set_id,T526.C205_Part_Number_Id ) setid
				  , DECODE(t526.c207_set_id,NULL,Get_Partnum_Desc (T526.C205_Part_Number_Id),get_set_name (t526.c207_set_id)) setnm
				  , t526.c526_request_flag reqfl
			      , DECODE(t526.c526_request_flag,'Y','**'||t526.c207_set_id ,t526.c207_set_id) set_id_print
				  , gm_pkg_op_product_requests.get_req_loaner_etch_id (t526.c526_product_request_detail_id) loaneretchid
				  , t526.c526_status_fl statusid
				  , t526.c526_required_date shipdate
				  , t906.c906_rule_value delmodealt
                  , get_code_name(t907.c901_delivery_carrier)delcarrier
				  , DECODE(t907.c901_ship_to,4121,Gm_pkg_cm_shipping_info.get_salesrepaddress(t907.c106_address_id,t907.c907_ship_to_id)
					,4123,Gm_pkg_cm_shipping_info.get_empaddress(t907.c106_address_id,t907.c907_ship_to_id)
					,gm_pkg_cm_shipping_info.get_address(t907.c901_ship_to,t907.c907_ship_to_id)) shipaddress
                  , t907.c907_tracking_number trackingno
                  , get_loaner_request_status(t526.c526_product_request_detail_id) status
				  , gm_pkg_op_product_requests.get_loanerreq_consignment_id (t526.c526_product_request_detail_id,NULL) consgid 
                  , DECODE(t526.c207_set_id,NULL,1,0) seq, DECODE(t526.c526_status_fl,20,1,10,2,5,3,40,4,30,5,6) sortorder
				  , DECODE(t907.c901_ship_to,4121,Gm_pkg_cm_shipping_info.get_salesrepaddress(t907.c106_address_id,t907.c907_ship_to_id)
								,gm_pkg_cm_shipping_info.get_address(t907.c901_ship_to,t907.c907_ship_to_id)) ||','|| t906.C906_RULE_VALUE MODEGROUP
				  , t907.c901_source source
               FROM (SELECT t526.* , NVL(DECODE (t526.c526_status_fl, '20',
                  					DECODE(gm_pkg_op_product_requests.get_set_status (t526.c526_product_request_detail_id),'Pending Pick',TO_CHAR(t526.c526_product_request_detail_id),t504a.c504_consignment_id),'30' , 
                  					t504a.c504_consignment_id,t526.c526_product_request_detail_id),t526.c526_product_request_detail_id) ref_id
  						FROM t526_product_request_detail t526 
    						, t504a_loaner_transaction t504a
  						WHERE t526.c526_product_request_detail_id = t504a.c526_product_request_detail_id (+)
  						AND t526.c525_product_request_id          = p_requestid 
  						AND t526.c526_void_fl is null 
  						AND t504a.c504a_void_fl is null) t526 
  				  , t907_shipping_info t907
                  , t906_rules t906
                  , t525_product_request t525 --Shipping details was not coming, when the set/item is in pending pick
			  WHERE t525.c525_product_request_id =  p_requestid
			    AND t525.c525_product_request_id = t526.c525_product_request_id
				AND t525.c901_request_for = DECODE(p_txn_type,'4119',p_txn_type,t525.c901_request_for)
             	AND t526.c901_request_type = DECODE(p_txn_type,'4127',p_txn_type,t526.c901_request_type)
				AND t525.c525_void_fl IS NULL
				AND t907.c525_product_request_id 	= t526.c525_product_request_id
				AND t907.c907_ref_id 				= t526.ref_id
                AND t906.C906_RULE_GRP_ID(+) 		= 'SHIPMODEALT' 
                AND t907.c901_delivery_mode			= t906.C906_RULE_ID(+)
                AND t906.c906_void_fl(+) 			IS NULL
				AND t526.c526_void_fl 			 	IS NULL 
				AND t907.c907_void_fl 			 	IS NULL 
				AND t907.c907_active_fl				IS NULL
			--	AND t525.C1900_company_id = v_company_id		
				AND (t525.C1900_company_id = v_loaner_filter OR  t525.c5040_plant_id = v_loaner_filter )
				AND t525.C1900_company_id = t526.C1900_company_id				
				
			UNION ALL
			SELECT ROWNUM NO , 0 reqdtlid , t504.c504_consignment_id reqcnid
					, 'Multiple' setid
					, NULL setnm
					, NULL reqfl
					, NULL set_id_print
                    , NULL loaneretchid
					, DECODE(t504.C504_STATUS_FL, '4', 30,  20) STATUSID
					, t907.c907_ship_to_dt shipdate
					, t906.c906_rule_value delmodealt,
				   get_code_name (t907.c901_delivery_carrier) delcarrier,
				   DECODE(t907.c901_ship_to,
					   4121, gm_pkg_cm_shipping_info.get_salesrepaddress(t907.c106_address_id,t907.c907_ship_to_id),
					   4123, gm_pkg_cm_shipping_info.get_empaddress(t907.c106_address_id,t907.c907_ship_to_id),
					   gm_pkg_cm_shipping_info.get_address (t907.c901_ship_to,t907.c907_ship_to_id)) shipaddress,
					t907.c907_tracking_number trackingno,
					DECODE(t504.C504_STATUS_FL, '4', 'Closed',  'Allocated') Status  ,          
					t504.c504_consignment_id consgid,
				   1 seq,
				   1 sortorder,
					  DECODE(t907.c901_ship_to,4121
						, gm_pkg_cm_shipping_info.get_salesrepaddress(t907.c106_address_id,t907.c907_ship_to_id),
						  gm_pkg_cm_shipping_info.get_address (t907.c901_ship_to,t907.c907_ship_to_id)) || ','|| t906.c906_rule_value modegroup,
				   t907.c901_source SOURCE    
			  FROM t504_consignment t504
				  , t907_shipping_info t907
				  , t906_rules t906
			 WHERE t504.c504_ref_id = TO_CHAR(p_requestid)
			   AND t504.c504_type = 9110
			   AND t907.c907_ref_id = t504.c504_consignment_id  
			   AND t504.c504_void_fl IS NULL
			   AND t906.c906_rule_grp_id(+) = 'SHIPMODEALT'
			   AND t907.c901_delivery_mode = t906.c906_rule_id(+)
			   AND t906.c906_void_fl(+) IS NULL
			   AND t907.c907_void_fl IS NULL              
			ORDER BY seq,sortorder,reqdtlid;

	END gm_fch_loaner_shipping_details;
	
/************************************************************************
	 * Purpose: Procedure to fetch the Loaner Request Header Details
	 * Created By  Hreddi
	 ************************************************************************/
	PROCEDURE gm_fch_loaner_request_header(
	   p_requestid 	       IN  t526_product_request_detail.c526_product_request_detail_id%TYPE	 
	 , p_user_id		   IN  VARCHAR2
	 , p_txn_type		   IN  t525_product_request.C901_REQUEST_FOR %TYPE 
	 , p_out_headerDetails OUT TYPES.cursor_type
	)
	AS	
		v_date_fmt   VARCHAR2(50);
		v_company_id  t1900_company.c1900_company_id%TYPE;
   		v_plant_id T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
   		v_loaner_filter   VARCHAR2(20);
	BEGIN
		
		
		select get_compdtfmt_frm_cntx(),get_compid_frm_cntx(),get_plantid_frm_cntx() into v_date_fmt,v_company_id,v_plant_id from dual;
		
	    SELECT DECODE(GET_RULE_VALUE(v_company_id,'LOANEREDITREQFILTER'), null, v_company_id, v_plant_id) --LOANEREDITREQFILTER- To fetch the records for EDC entity countries based on plant
          INTO v_loaner_filter 
          FROM DUAL;
      
		OPEN p_out_headerDetails FOR 
		 SELECT t525.c525_product_request_id requestid
				  , to_char(t525.c525_requested_date,v_date_fmt) requesteddt
				  , t525.c525_request_for_id requestforid, get_distributor_name (t525.c525_request_for_id) requestfornm
				  , t525.c703_sales_rep_id salesrepid, get_rep_name (t525.c703_sales_rep_id) salesrepnm
				  , t525.c704_account_id accountid, get_account_name (t525.c704_account_id) accountnm
				  , to_char(t525.c525_surgery_date,v_date_fmt) surgerydt
				  , DECODE(t907.c901_ship_to,'4121',Gm_pkg_cm_shipping_info.get_salesrepaddress(t907.c106_address_id,t907.c907_ship_to_id)
				                            ,'4123',Gm_pkg_cm_shipping_info.get_empaddress(t907.c106_address_id,t907.c907_ship_to_id)
				    ,gm_pkg_cm_shipping_info.get_address(t907.c901_ship_to,t907.c907_ship_to_id)) loanto
				  , get_user_name(p_user_id) Printby
				  , to_char(CURRENT_DATE,v_date_fmt || ' HH:MM:SS AM') Printdttime
				  , t525.c703_ass_rep_id assocrepid, get_rep_name (t525.c703_ass_rep_id) assocrepnm
			   FROM t525_product_request t525, t907_shipping_info t907
			  WHERE t525.c525_product_request_id = p_requestid
				AND t525.c525_void_fl 			 IS NULL
				AND t907.c907_void_fl 			 IS NULL
				--AND t525.c901_ship_to 			 = t907.c901_ship_to(+)
				AND t525.c525_product_request_id = t907.c907_ref_id(+)
				AND t525.C901_REQUEST_FOR        = p_txn_type
				AND (t525.C1900_company_id = v_loaner_filter OR  t525.c5040_plant_id = v_loaner_filter )
				AND t907.c901_source 			 = 50185;		
	END gm_fch_loaner_request_header;	 

/************************************************************************
	 * Purpose: Procedure to fetch the Loaner Request Voided Details
	 * Created By  Hreddi
************************************************************************/
	PROCEDURE gm_fch_loaner_voided_request(
	   p_requestid 	        IN	t526_product_request_detail.c526_product_request_detail_id%TYPE		
	 , p_txn_type		    IN  t526_product_request_detail.C901_REQUEST_TYPE%TYPE 
	 , p_out_VoidReqDetails OUT TYPES.cursor_type
	)
	AS
	v_company_id  t1900_company.c1900_company_id%TYPE;
	v_plant_id T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
	v_loaner_filter   VARCHAR2(20);
	BEGIN
		
		select get_compid_frm_cntx() ,get_plantid_frm_cntx()into v_company_id,v_plant_id from dual;
		SELECT DECODE(GET_RULE_VALUE(v_company_id,'LOANEREDITREQFILTER'), null, v_company_id, v_plant_id) --LOANEREDITREQFILTER- To fetch the records for EDC entity countries based on plant
          INTO v_loaner_filter 
          FROM DUAL;
		OPEN p_out_VoidReqDetails FOR 
		   SELECT ROWNUM NO
		   		, t526.c526_product_request_detail_id reqdtlid
		        , t504al.c504_consignment_id reqcnid
		        , t504ac.c504a_etch_id loaneretchid
  				, get_code_name (t907.c901_cancel_cd) voidreason
  				, NVL(t526.c207_set_id,t526.c205_part_number_id) setid
  				, DECODE(t526.c207_set_id,NULL,get_partnum_desc(t526.c205_part_number_id),get_set_name (t526.c207_set_id)) setnm
  				, t526.c526_qty_requested qty
  				, t526.c526_required_date requireddt
  				, DECODE (c526_status_fl, '5', 'Pending Approval','10', 'Open', '20', 'Allocated', '30', 'Closed', '40', 'Rejected', c526_status_fl) status
  				, get_log_flag (t526.c526_product_request_detail_id, 1248) LOG
  				, DECODE (t907.c901_cancel_cd, 90766, 1, 90767, 2, 90760, 3, 90761, 4, 90762, 5, 90765, 6, 90763, 7, 90764, 8, 9) sortorder
  				, DECODE(t526.c207_set_id,NULL,1,0) seq
   			FROM  t504a_consignment_loaner t504ac
   			    , t504a_loaner_transaction t504al
   			    , t526_product_request_detail t526 
   			    , t907_cancel_log t907
   			    , t525_product_request t525
 		   WHERE  t504al.c526_product_request_detail_id (+) = t526.c526_product_request_detail_id
             AND  t525.c525_product_request_id = p_requestid
             AND  t525.c525_product_request_id = t526.c525_product_request_id  
             AND  t525.c901_request_for = DECODE(p_txn_type,'4119',p_txn_type,t525.c901_request_for)
             AND  t526.c901_request_type = DECODE(p_txn_type,'4127',p_txn_type,t526.c901_request_type)
    		 AND  t526.c526_void_fl IS NOT NULL
    		 AND  t504al.c504_consignment_id = t504ac.c504_consignment_id(+)
    		 AND  t504ac.C504a_void_fl IS NULL
    		 AND  t907.c901_type =  DECODE(p_txn_type,'4119','1006650','90198')
    		 AND  t907.c907_ref_id  = t526.c526_product_request_detail_id
    		 AND  t907.c907_void_fl IS NULL
    		 AND (t525.C1900_company_id = v_loaner_filter OR  t525.c5040_plant_id = v_loaner_filter )
    	ORDER BY  seq,sortorder,reqdtlid;
	END gm_fch_loaner_voided_request;
	
     /*******************************************************
	* Description : Procedure to update item request
	* Author	  : Gopinathan
	*******************************************************/	
	PROCEDURE gm_sav_req_item (
		  p_req_dtl_id IN t526_product_request_detail.c526_product_request_detail_id%TYPE
		, p_pnum       IN t526_product_request_detail.c205_part_number_id%TYPE
		, p_qty        IN t526_product_request_detail.c526_qty_requested%TYPE
		, p_userid     IN t526_product_request_detail.c526_last_updated_by%TYPE
	)
	AS
		v_prod_req_id t526_product_request_detail.c525_product_request_id%TYPE;
	    v_req_type    t526_product_request_detail.c901_request_type%TYPE;
	    v_qty_req     t526_product_request_detail.c526_qty_requested%TYPE;
	    v_req_status  t526_product_request_detail.c526_qty_requested%TYPE;
	    v_outtxnid    t412_inhouse_transactions.c412_inhouse_trans_id%TYPE;
	    v_cnt         NUMBER;
	    v_add_qty     NUMBER;
	BEGIN
		BEGIN
		     SELECT c525_product_request_id,c901_request_type, c526_qty_requested qty_req, c526_status_fl
		       INTO v_prod_req_id, v_req_type, v_qty_req, v_req_status
		       FROM t526_product_request_detail
		      WHERE c526_product_request_detail_id = p_req_dtl_id
		        AND c205_part_number_id            = p_pnum
		        AND c205_part_number_id IS NOT NULL
		        AND c526_void_fl IS NULL;
		     EXCEPTION
			 WHEN NO_DATA_FOUND THEN
			 	RETURN;
	    END;     
	        	IF v_req_status IN ('5','10') THEN -- pending approval, Open
	        
			         UPDATE t526_product_request_detail
			            SET c526_qty_requested   = p_qty
			              , c526_status_fl = '5'
			              , c526_exp_return_date = NULL -- new shipping date will update
			              , c526_last_updated_by = p_userid
			              , c526_last_updated_date = CURRENT_DATE
			          WHERE c526_product_request_detail_id = p_req_dtl_id
			            AND c205_part_number_id IS NOT NULL
			            AND c526_void_fl IS NULL;
		        ELSIF v_req_status IN ('20','30') THEN  -- Allocated,Closed
		        	IF p_qty < v_qty_req THEN -- qty reduced in request
		        		 raise_application_error (-20586, '');                                                     
		        	ELSIF p_qty > v_qty_req THEN -- add qty  in request
		        	
		        	 v_add_qty := p_qty - v_qty_req;
		        	 --Increased qty add into IHRQ
		        	 gm_pkg_sm_loaner_item_alloc.gm_chk_inhouse_txn(v_prod_req_id, v_req_type,p_pnum, 1006483, p_userid, v_add_qty,v_outtxnid);
		        	 
			         UPDATE t526_product_request_detail
			            SET c526_qty_requested   = p_qty
			              , c526_status_fl = '20'
			              , c526_last_updated_by = p_userid
			              , c526_last_updated_date = CURRENT_DATE
			          WHERE c526_product_request_detail_id = p_req_dtl_id
			            AND c205_part_number_id IS NOT NULL
			            AND c526_void_fl IS NULL;		        		
		        	END IF;
		        END IF;
	END gm_sav_req_item;  
	/****************************************************************
	 * Description : This procedure throw message if the set in obsolete status
	 * Author      : arockia prasath
	*****************************************************************/
	PROCEDURE gm_validate_set_obsolete_sts
	AS
		v_obs_set	   VARCHAR2(4000);
	BEGIN	
		SELECT RTRIM(XMLAGG(XMLELEMENT(e,c207_set_id || ',')).EXTRACT('//text()').getclobval(),',') INTO v_obs_set 
				   FROM t207_set_master 
				      WHERE c207_set_id IN (SELECT my_temp_txn_id FROM my_temp_list) 
				      AND c207_void_fl IS NULL 
				      AND c901_status_id = '20369'; --obsolete
		DELETE FROM my_temp_list;
		IF v_obs_set IS NOT NULL
		THEN
			GM_RAISE_APPLICATION_ERROR('-20999','271',v_obs_set);
		END IF;
	END gm_validate_set_obsolete_sts;
	
	/*
	* Author		: Rajkumar
	* Description	: This procedure is used to fetch the attibute values for the Loaner Request, 
	* 					the attributes like Surgeon Name  Time of Surgery
	*	
	*/
	PROCEDURE gm_fch_attribute_req_det(
		p_requestid IN  t525_product_request.c525_product_request_id%TYPE,
		p_out		OUT TYPES.cursor_type
	)
	AS
	BEGIN
			OPEN p_out FOR
	              SELECT decode(t525a.c901_attribute_id,null,t901.c901_code_nm,get_code_name(t525a.c901_attribute_id)) ATTBNAME, 
						decode(nvl(t525a.c525a_attribute_value,''),'','',t525a.c525a_attribute_value) ATTBVAL
						 FROM t901_code_lookup t901, t525a_product_request_attrib t525a
						 WHERE t901.c901_code_id = t525a.c901_attribute_ID(+)
						 AND t901.c901_code_grp = 'LNATB'
						 AND t901.c901_void_fl IS NULL
						 AND t525a.c525a_void_fl(+) IS NULL
						 AND t901.c901_active_fl = '1'
						 AND t525a.c525_product_request_id(+) = p_requestid 
						 ORDER BY t901.c901_code_seq_no;

	END	gm_fch_attribute_req_det;
	
	/*******************************************************************************
     * Purpose: Function to fetch COnsignment Status in Number By request detail id
       Author : HReddi
     *******************************************************************************/
	FUNCTION get_set_status_id (
		p_requestid   IN   t526_product_request_detail.c526_product_request_detail_id%TYPE
	)
		RETURN VARCHAR2
	IS
		v_status_id	   VARCHAR2 (20);
	BEGIN
		SELECT t504ac.c504a_status_fl
		  INTO v_status_id
		  FROM t504a_consignment_loaner t504ac, t504a_loaner_transaction t504al
		 WHERE t504al.c526_product_request_detail_id = p_requestid
		   AND t504al.c504_consignment_id = t504ac.c504_consignment_id
		   AND t504al.c504a_void_fl IS NULL
		   AND t504ac.c504a_void_fl IS NULL
		   AND ROWNUM = 1;

		RETURN v_status_id;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN '';
	END get_set_status_id;
	/*************************************************************************************************
     * Purpose: The Below Procedure is used to Validate the Product Request ID Based on the Company ID
       Author : Mrajasekaran
     ************************************************************************************************/
	PROCEDURE gm_validate_prod_request(
		p_requestid    IN  t525_product_request.c525_product_request_id%TYPE,
		p_request_type IN  VARCHAR2
	)
	AS
	v_company_id    T1900_COMPANY.C1900_COMPANY_ID%TYPE;
	v_company_name  T1900_COMPANY.C1900_COMPANY_NAME%TYPE;
	v_request_company_id  T1900_COMPANY.C1900_COMPANY_ID%TYPE;
	v_plant_id      T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
	v_request_plant_id T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
	v_plant_name    T5040_PLANT_MASTER.c5040_plant_name%TYPE;
	v_loaner_filter VARCHAR2(20);
	v_filter_name   VARCHAR2(20);
	
	BEGIN
		v_filter_name := 'LOANEREDITREQFILTER';
		SELECT get_compid_frm_cntx() , get_plantid_frm_cntx  INTO v_company_id , v_plant_id FROM dual;	 
			
		IF p_request_type = 'LOANER' THEN  -- Vaidation for Process Transaction Screen
			 BEGIN
				SELECT t504a.c1900_company_id, t504.c5040_plant_id  
				  INTO v_request_company_id , v_request_plant_id
                  FROM t504_consignment t504 , t504a_loaner_transaction  t504a
                 WHERE t504.c504_consignment_id = P_REQUESTID 
                   AND t504.c504_consignment_id = t504a.c504_consignment_id
                   AND t504a.c504a_return_dt is null
                   AND c504_void_fl IS NULL 
                   AND t504a.c504a_void_fl is null
              GROUP BY t504a.c1900_company_id , t504.c5040_plant_id ;
	         EXCEPTION WHEN NO_DATA_FOUND THEN
	           BEGIN
		           SELECT c1900_company_id, c5040_plant_id  
		             INTO v_request_company_id , v_request_plant_id 
		             FROM t504_consignment 
		             WHERE c504_consignment_id = P_REQUESTID
		               AND c504_void_fl IS NULL  ;
	           EXCEPTION WHEN NO_DATA_FOUND THEN	         
	      		v_request_company_id := NULL;
	      		v_request_plant_id   := NULL;
	      		END;
	      	 END;
	      	 v_filter_name := 'PROCESSTRANSFILTER';
	      	 
      	ELSE
	      	BEGIN
	    		SELECT C1900_COMPANY_ID , C5040_PLANT_ID
	    		  INTO v_request_company_id , v_request_plant_id
	    		  FROM T525_PRODUCT_REQUEST 
	   			 WHERE C525_PRODUCT_REQUEST_ID = P_REQUESTID
	   			   AND C901_REQUEST_FOR = P_REQUEST_TYPE
		    	   AND C525_VOID_FL is null;
		    EXCEPTION WHEN NO_DATA_FOUND THEN
	      		v_request_company_id := NULL;
	      		v_request_plant_id   := NULL;
	      	END;      	 
		END IF;
					     
	      BEGIN 
	      	SELECT GET_RULE_VALUE(v_company_id,v_filter_name)   INTO v_loaner_filter  FROM DUAL;
	      EXCEPTION WHEN NO_DATA_FOUND THEN
					v_loaner_filter := NULL;
		  END;
	     
		  IF v_loaner_filter IS NOT NULL THEN
	      	IF v_request_plant_id IS NOT NULL THEN
	      		v_plant_name := get_plant_name(v_request_plant_id);
	      		IF (v_plant_id != v_request_plant_id) THEN
	      			GM_RAISE_APPLICATION_ERROR('-20999','272',p_requestid||'#$#'||v_plant_name);
	      		END IF;
	      	END IF;
	      ELSE			
	      	IF v_request_company_id is NOT NULL THEN
	      		v_company_name := get_company_name(v_request_company_id);
	      		IF (v_company_id != v_request_company_id) THEN
	      			GM_RAISE_APPLICATION_ERROR('-20999','272',p_requestid||'#$#'||v_company_name);
	      		END IF;
	      	END IF;
		  END IF;
	END	gm_validate_prod_request;
END gm_pkg_op_product_requests;
/
