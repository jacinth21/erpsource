/* Formatted on 2009/07/06 15:57 (Formatter Plus v4.8.0) */
--@"c:\database\packages\operations\gm_pkg_op_charges.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_charges
IS
/******************************************************************
   * Description : This Procedure is used to update the usage flg when processing loaner
   * Author 	: Brinal G
  ****************************************************************/
	PROCEDURE gm_sav_usage_flg_processing (
		p_loantransid	IN	 t504a_loaner_transaction.c504a_loaner_transaction_id%TYPE
	  , p_userid		IN	 t504a_loaner_transaction.c504a_last_updated_by%TYPE
	)
	AS
		v_inhousetransid t412_inhouse_transactions.c412_inhouse_trans_id%TYPE;
		v_prodreqid    t525_product_request.c525_product_request_id%TYPE;
		v_count 	   NUMBER;
		v_status	   NUMBER;
	BEGIN
		BEGIN
			v_prodreqid := gm_pkg_op_charges.get_prod_request_id (p_loantransid);

			SELECT c412_inhouse_trans_id
			  INTO v_inhousetransid
			  FROM t412_inhouse_transactions
			 WHERE c504a_loaner_transaction_id = p_loantransid
			   AND c412_type = 50159
			   AND c412_void_fl IS NULL
			   AND ROWNUM = 1;	 -- in case of loaner extn transaction multiple recs can be present
		EXCEPTION
			WHEN NO_DATA_FOUND	 -- no parts entered as missing
			THEN
				-- chk if all the loaners in the request are processed
				SELECT COUNT (c504a_loaner_transaction_id)
				  INTO v_count
				  FROM t504a_loaner_transaction t504al
				 WHERE t504al.c526_product_request_detail_id IN (
													SELECT c526_product_request_detail_id
													  FROM t526_product_request_detail
													 WHERE c525_product_request_id = v_prodreqid
														   AND c526_void_fl IS NULL)
				   AND c504a_processed_date IS NULL
				   AND c504a_void_fl IS NULL;

				IF v_count = 0
				THEN
					-- all loaners are processed and status is still 0,  updating the status to non-usage
					SELECT	   NVL (c525_usage_status, 0)
						  INTO v_status
						  FROM t525_product_request
						 WHERE c525_product_request_id = v_prodreqid AND c525_void_fl IS NULL
					FOR UPDATE;

					IF v_status = 0
					THEN
						gm_pkg_op_charges.gm_usage_status_update (v_prodreqid, 20, p_userid);	-- non-usage
					END IF;
				END IF;

				RETURN;
		END;

		gm_pkg_op_charges.gm_usage_status_update (v_prodreqid, 10, p_userid);	-- update to missing
	END gm_sav_usage_flg_processing;

	  /******************************************************************
	 * Description : This Procedure contains the common code to update the usage flg
	 * Author		: Brinal G
	****************************************************************/
	PROCEDURE gm_usage_status_update (
		p_requestid   IN   t525_product_request.c525_product_request_id%TYPE
	  , p_status	  IN   NUMBER
	  , p_userid	  IN   t504a_loaner_transaction.c504a_last_updated_by%TYPE
	)
	AS
		v_status	   NUMBER;
	BEGIN
		SELECT	   NVL (c525_usage_status, 0)
			  INTO v_status
			  FROM t525_product_request
			 WHERE c525_product_request_id = p_requestid AND c525_void_fl IS NULL
		FOR UPDATE;

		IF v_status != 30	-- usage
		THEN
			UPDATE t525_product_request
			   SET c525_usage_status = p_status
				 , c525_last_updated_by = p_userid
				 , c525_last_updated_date = SYSDATE
			 WHERE c525_product_request_id = p_requestid AND c525_void_fl IS NULL;
		END IF;
	END gm_usage_status_update;

			/******************************************************************
	 * Description : This func returns the prod req id when loan trans id is passed
	 * Author		: Brinal G
	****************************************************************/
	FUNCTION get_prod_request_id (
		p_loantransid	IN	 t504a_loaner_transaction.c504a_loaner_transaction_id%TYPE
	)
		RETURN NUMBER
	AS
		v_prodreqid    NUMBER;
	BEGIN
		BEGIN
			SELECT NVL (c525_product_request_id, 0)
			  INTO v_prodreqid
			  FROM t504a_loaner_transaction t504a, t526_product_request_detail t526
			 WHERE c504a_loaner_transaction_id = p_loantransid
			   AND t504a.c526_product_request_detail_id = t526.c526_product_request_detail_id
			   AND c504a_void_fl IS NULL
			   AND c526_void_fl IS NULL;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				RETURN 0;
		END;

		RETURN v_prodreqid;
	END get_prod_request_id;

	  /******************************************************************
	 * Description : This Procedure is used to update the usage flg during reconcilation
	 * Author		: Brinal G
	****************************************************************/
	PROCEDURE gm_sav_usage_flg_reconcilation (
		p_inhousetransid   IN	t412_inhouse_transactions.c412_inhouse_trans_id%TYPE
	  , p_userid		   IN	t412_inhouse_transactions.c412_last_updated_by%TYPE
	)
	AS
		v_qty		   NUMBER;
		v_prodreqid    NUMBER;
		v_count 	   NUMBER;
		v_usage_status NUMBER;
		v_status	   t412_inhouse_transactions.c412_status_fl%TYPE;
		v_loantransid  t504a_loaner_transaction.c504a_loaner_transaction_id%TYPE;
	BEGIN
		SELECT SUM (c413_item_qty)
		  INTO v_qty
		  FROM t413_inhouse_trans_items
		 WHERE c412_inhouse_trans_id = p_inhousetransid AND c413_status_fl = 'S' AND c413_void_fl IS NULL;

		SELECT	   c412_status_fl, c504a_loaner_transaction_id
			  INTO v_status, v_loantransid
			  FROM t412_inhouse_transactions
			 WHERE c412_inhouse_trans_id = p_inhousetransid AND c412_type = 50159 AND c412_void_fl IS NULL
		FOR UPDATE;

		v_prodreqid := get_prod_request_id (v_loantransid);

		IF v_qty > 0
		THEN   --its an usage
			gm_usage_status_update (v_prodreqid, 30, p_userid);   --usage
			RETURN;
		END IF;

		IF v_status = '4'	--complete
		THEN
			SELECT COUNT (c504a_loaner_transaction_id)
			  INTO v_count
			  FROM t504a_loaner_transaction t504al
			 WHERE t504al.c526_product_request_detail_id IN (
													SELECT c526_product_request_detail_id
													  FROM t526_product_request_detail
													 WHERE c525_product_request_id = v_prodreqid
														   AND c526_void_fl IS NULL)
			   AND c504a_processed_date IS NULL
			   AND c504a_void_fl IS NULL;
		ELSE
			RETURN;
		END IF;

		IF v_count = 0
		THEN   -- all processed
			SELECT COUNT (*)
			  INTO v_count
			  FROM t412_inhouse_transactions
			 WHERE c504a_loaner_transaction_id IN (
							SELECT c504a_loaner_transaction_id loanid
							  FROM t504a_loaner_transaction t504al
							 WHERE t504al.c526_product_request_detail_id IN (
																			 SELECT c526_product_request_detail_id
																			   FROM t526_product_request_detail
																			  WHERE c525_product_request_id =
																											 v_prodreqid))
			   AND c412_status_fl < 4
			   AND c412_type = 50159;
		ELSE
			RETURN;
		END IF;

		IF v_count = 0	 -- all processed and reeconciled
		THEN
			gm_usage_status_update (v_prodreqid, 20, p_userid);   -- non-usage
		END IF;
	END gm_sav_usage_flg_reconcilation;

	/******************************************************************
	 * Description : This Procedure is used to create the non-usage charges
	 * Author		: Brinal G
	****************************************************************/
	PROCEDURE gm_sav_non_usage_charge
	AS
		v_nonusgcnt    NUMBER;
		v_discount	   NUMBER (10, 2);
		v_charge1	   NUMBER (10, 2);
		v_charge2	   NUMBER (10, 2) := 0;
		v_distid	   VARCHAR2 (20);
		v_incident_value VARCHAR2 (2000);
		v_incident_id  t9200_incident.c9200_incident_id%TYPE;
		v_hvu		   VARCHAR2 (10);
		v_hvu_disc	   VARCHAR2 (10);
		v_lvu_disc	   VARCHAR2 (10);
		v_nuchrg	   VARCHAR2 (10);
		v_nuchrgperiod VARCHAR2 (10);
		v_volm_clsfn   NUMBER;
		v_chrg_dtl_id  NUMBER;
		CURSOR rep_cursor
		IS
			SELECT	 c703_sales_rep_id repid, COUNT (c525_product_request_id) reqcnt
				FROM t525_product_request
			   WHERE c525_usage_date < TRUNC (SYSDATE, v_nuchrgperiod)
				 AND c525_charge_status IS NULL
				 AND c525_charge_date IS NULL
				 AND c703_sales_rep_id NOT IN (
						 SELECT c703_sales_rep_id repid
						   FROM t525_product_request
						  WHERE c525_usage_status = 10	 --open
							AND c525_usage_date < TRUNC (SYSDATE, v_nuchrgperiod)
							AND c525_charge_status IS NULL
							AND c525_charge_date IS NULL
							AND c525_void_fl IS NULL)
				 AND c525_void_fl IS NULL 
				 AND c901_request_for = 4127 
			GROUP BY c703_sales_rep_id
			ORDER BY repid;
	BEGIN
		v_hvu		:= get_rule_value ('HVU', 'CHARGES');	-- high volume user 6
		v_hvu_disc	:= get_rule_value ('HVUDISC', 'CHARGES');	-- high volume user discount	35%
		v_lvu_disc	:= get_rule_value ('LVUDISC', 'CHARGES');	-- low volume user discount 2
		v_nuchrg	:= get_rule_value ('NUCHRG', 'CHARGES');   -- non usage charges $300
		v_nuchrgperiod := get_rule_value ('NUPERIOD', 'CHARGES');	-- Returns Q or MM

		FOR currindex IN rep_cursor
		LOOP
			BEGIN
				SELECT	 get_distributor_id (c703_sales_rep_id) distid, COUNT (c525_product_request_id) nonusgcnt
					INTO v_distid, v_nonusgcnt
					FROM t525_product_request
				   WHERE c525_usage_date < TRUNC (SYSDATE, v_nuchrgperiod)
					 AND c525_charge_status IS NULL
					 AND c525_charge_date IS NULL
					 AND c525_usage_status = 20   --non-usage
					 AND c703_sales_rep_id = currindex.repid
					 AND c525_void_fl IS NULL
				GROUP BY c703_sales_rep_id;
			EXCEPTION
				WHEN NO_DATA_FOUND
				THEN
					v_nonusgcnt := 0;
			END;

			IF currindex.reqcnt >= v_hvu
			THEN   -- High volume user
				/*
				eg -
				if reqcnt = 10 (total requests made for the quarter)
				non_usage_reqcnt = 4, (total non-usage requests made for the quarter)
				discount  = 4-((35/100)*10) = 4-3.5 =.5
				charge = .5 * 300 = 150
				*/
				v_discount	:= (v_hvu_disc / 100) * currindex.reqcnt;
				v_charge1	:= v_nonusgcnt - v_discount;
				v_charge2	:= v_charge1 * v_nuchrg;
				v_incident_value :=
					   'Total Requests: '
					|| currindex.reqcnt
					|| '<br>Total Non-Usages: '
					|| v_nonusgcnt
					|| '<br>Waived Instances:'
					|| '0'
					|| '<br>(('
					|| v_nonusgcnt
					|| ' - '
					|| '0'
					|| ') - '
					|| v_discount
					|| ') * $'
					|| v_nuchrg
					|| ' = $'
					|| v_charge2;
				v_volm_clsfn:=1006426;
			ELSE   -- currindex.reqcnt > v_lvu_disc
				-- Low Volume User
					/*
					eg -
					if reqcnt = 5 (total requests made for the quarter)
					non_usage_reqcnt = 3, (total non-usage requests made for the quarter)
					charge = (3-2)* 300) = 100
					*/
				v_discount	:= v_nonusgcnt - v_lvu_disc;
				v_charge2	:= v_discount * v_nuchrg;
				v_incident_value :=
					   'Total Requests: '
					|| currindex.reqcnt
					|| '<br>Total Non-Usages: '
					|| v_nonusgcnt
					|| '<br>Waived Instances:'
					|| '0'
					|| '<br>(('
					|| v_nonusgcnt
					|| ' - '
					|| '0'
					|| ') - '
					|| v_lvu_disc
					|| '(waveoff))* $'
					|| v_nuchrg
					|| ' = $'
					|| v_charge2;
			   v_volm_clsfn:=1006427;
			END IF;
			--50890- non-usage
			--92073 - non-usage
			IF v_charge2 > 0   -- if +ve charges are present only then enter the record
			THEN
				gm_pkg_op_loaner.gm_sav_incidents (50890
												 , ''
												 , 92073
												 , v_incident_value
												 , 30301
												 , TRUNC (SYSDATE)
												 , v_incident_id
												  );
				gm_pkg_op_loaner.gm_sav_charge_details (v_incident_id, currindex.repid, v_distid, v_charge2, 20, 30301);
			 BEGIN
				 SELECT c9201_charge_details_id
				   INTO v_chrg_dtl_id
				   FROM t9201_charge_details
				  WHERE c9200_incident_id = v_incident_id
				    AND c9201_void_fl    IS NULL;
			 EXCEPTION
				WHEN NO_DATA_FOUND
				THEN
					v_chrg_dtl_id := NULL;
			 END;
				gm_pkg_op_charges.gm_save_nonusg_chg_details('',v_chrg_dtl_id,currindex.reqcnt,v_nonusgcnt,v_nuchrg,v_volm_clsfn,0);
				
			END IF;

			UPDATE t525_product_request
			   SET c525_charge_status = 'Y'
				 , c525_charge_date = TRUNC (SYSDATE)
				 , c525_last_updated_by = 30301
				 , c525_last_updated_date = SYSDATE
			 WHERE c703_sales_rep_id = currindex.repid
			   AND c525_usage_date < TRUNC (SYSDATE, v_nuchrgperiod)
			   AND c525_void_fl IS NULL
			   AND c525_charge_status IS NULL
			   AND c525_charge_date IS NULL;
		END LOOP;
	END gm_sav_non_usage_charge;
	
/******************************************************************
 * Description : procedure to save loaner late charge
 * Author      : dreddy
 *****************************************************************/
PROCEDURE gm_sav_late_charges (
        p_request_id IN 	t9200_incident.c9200_ref_id%TYPE,
        p_status     IN 	t9201_charge_details.c9201_status%TYPE,
        p_user_id    IN 	t9201_charge_details.c9201_created_by%TYPE,
        p_consign_id IN     t504a_loaner_transaction.c504_consignment_id%TYPE,
        p_job_run_date 		IN    DATE,
        p_prod_req_dtl_id   IN   T526_Product_Request_Detail.C526_Product_Request_Detail_Id%TYPE DEFAULT NULL)
AS
    v_late_fee_by_set t906_rules.c906_rule_value%TYPE := get_rule_value('LOANER_CHARGES_BY_SET','LATE_FEE_CHARGES');
BEGIN
	/*Loaner Late fee charge is calculated based on the Set level available in the loaners
	 * based on the Rule entry call the below proceudres and calculate the loaner late fee charges*/
	
	IF v_late_fee_by_set = 'Y' THEN
			gm_pkg_op_charges.gm_sav_late_charges_by_set(p_request_id,p_status,p_user_id,p_consign_id,p_job_run_date,p_prod_req_dtl_id);
	ELSE
			gm_pkg_op_charges.gm_sav_late_charges_by_request(p_request_id,p_status,p_user_id,p_consign_id,p_job_run_date);
	END IF;

END gm_sav_late_charges;

/***********************************************************
 * Description : This Procedure is used to create month end loaner late charges
 * Author	   : Dhanareddy
 ***********************************************************/
PROCEDURE gm_sav_loaner_late_charge (
        p_date IN DATE DEFAULT SYSDATE,
        p_request_id   IN   t525_product_request.c525_product_request_id%TYPE DEFAULT NULL,
        p_prod_req_dtl_id   IN   T526_Product_Request_Detail.C526_Product_Request_Detail_Id%TYPE DEFAULT NULL
)
AS
BEGIN  
 		--The loaner late fee charges calculation daily in pending return and overdue date
        gm_pkg_op_loaner_charges.gm_sav_loaner_late_charge(p_date, p_request_id,p_prod_req_dtl_id);

END gm_sav_loaner_late_charge;

/*********************************************
 * Description : This Procedure is used to create waived 
 * instances of non-usage charges
 * Author	   : Dhanareddy
 **********************************************/
PROCEDURE gm_save_nonusg_chg_details (
   p_non_usg_chg_det_id   IN   t9202_nonusg_chg_details.c9202_nonusg_chg_details_id%TYPE,
   p_charge_dtlid         IN   t9201_charge_details.c9201_charge_details_id%TYPE,
   p_tot_req_cnt          IN   t9202_nonusg_chg_details.c9202_total_request_count%TYPE,
   p_tot_nonusg_cnt       IN   t9202_nonusg_chg_details.c9202_total_nonusg_count%TYPE,
   p_chrg_amt             IN   t9202_nonusg_chg_details.c9202_charge_amount%TYPE,
   p_volm_clfn            IN   t9202_nonusg_chg_details.c901_volume_classification%TYPE,
   p_inst_waived          IN   t9202_nonusg_chg_details.c9202_instance_waived%TYPE
)
AS
BEGIN
   UPDATE t9202_nonusg_chg_details
      SET c9201_charge_details_id = p_charge_dtlid,
          c9202_total_request_count = p_tot_req_cnt,
          c9202_total_nonusg_count = p_tot_nonusg_cnt,
          c9202_charge_amount = p_chrg_amt,
          c901_volume_classification = p_volm_clfn,
          c9202_instance_waived = p_inst_waived
    WHERE c9202_nonusg_chg_details_id = p_non_usg_chg_det_id;

   IF SQL%ROWCOUNT = 0
   THEN
      INSERT INTO t9202_nonusg_chg_details
                  (c9202_nonusg_chg_details_id, c9201_charge_details_id,
                   c9202_total_request_count, c9202_total_nonusg_count,
                   c9202_charge_amount, c901_volume_classification,
                   c9202_instance_waived
                  )
           VALUES (s9202_nonusg_chg_details.NEXTVAL, p_charge_dtlid,
                   p_tot_req_cnt, p_tot_nonusg_cnt,
                   p_chrg_amt, p_volm_clfn,
                   p_inst_waived
                  );
   END IF;
END gm_save_nonusg_chg_details;

/******************************************************************
 * Description : This function is used to get month count for loaner month end job 
 * Author	   : Dhanareddy 
 ******************************************************************/
FUNCTION get_month_count (
        p_date       IN DATE,
        p_job_run_dt IN DATE)
    RETURN NUMBER
AS
    v_month_cnt    NUMBER;
    v_days_cnt NUMBER;
BEGIN
    BEGIN
         SELECT TRUNC (last_day (p_date) - TRUNC (p_date)) INTO v_days_cnt FROM dual;
         IF v_days_cnt = 0 THEN
             SELECT ROUND (MONTHS_BETWEEN (p_date, p_job_run_dt))+1
               INTO v_month_cnt
               FROM DUAL;
        ELSE
             SELECT ROUND (MONTHS_BETWEEN (p_date, p_job_run_dt))
               INTO v_month_cnt
               FROM DUAL;
        END IF;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_month_cnt := 0;
    END;
    RETURN v_month_cnt;
END get_month_count;

/*************************************************************************
 * Description : This function is used to get loaner month end job run date
 * Author	   : Dhanareddy 
 ***********************************************************************/
FUNCTION get_run_date (
        p_date DATE ,
        p_month_cnt NUMBER)
    RETURN DATE
AS
v_return_dt DATE;
v_days_cnt NUMBER;
BEGIN
     SELECT TRUNC (last_day (p_date) - TRUNC (p_date)) INTO v_days_cnt FROM dual;
    IF v_days_cnt = 0  AND p_month_cnt = 1  THEN
         v_return_dt := p_date;
    ELSIF v_days_cnt = 0 AND p_month_cnt > 1 
    THEN
    	SELECT last_day (add_months (p_date, -(p_month_cnt-1))) INTO v_return_dt FROM dual;
    ELSE
    	SELECT last_day (add_months (p_date, -p_month_cnt)) INTO v_return_dt FROM dual;
    END IF;
   RETURN v_return_dt;
END get_run_date;

/******************************************************************
 * Description : procedure to save loaner late charge by set
 * Author      : Karthik
 *****************************************************************/
PROCEDURE gm_sav_late_charges_by_set (
        p_request_id IN 	t9200_incident.c9200_ref_id%TYPE,
        p_status     IN 	t9201_charge_details.c9201_status%TYPE,
        p_user_id    IN 	t9201_charge_details.c9201_created_by%TYPE,
        p_consign_id IN     t504a_loaner_transaction.c504_consignment_id%TYPE,
        p_job_run_date IN    DATE,
        p_prod_req_dtl_id   IN   T526_Product_Request_Detail.C526_Product_Request_Detail_Id%TYPE DEFAULT NULL)
AS
    v_incident_id 	VARCHAR2(20);
    v_dist_id     	NUMBER;
    v_rep_id      	NUMBER;
    v_late_fee    	VARCHAR2(50);
    v_expectetd_dt  DATE;
    v_return_dt     DATE;
    v_chrge_dt      DATE;
    v_job_run_date  DATE;
    v_assoc_rep_id  t504a_loaner_transaction.c703_ass_rep_id%TYPE;
    v_company_id NUMBER;
    v_req_company_id  NUMBER;
    v_etch_id t504a_consignment_loaner.c504a_etch_id%TYPE;
    v_first_day	DATE := TO_DATE(Trunc(SYSDATE) - (To_Number(to_char(SYSDATE,'DD')) - 1)); --Referred GET_ACCOUNT_MONTH_SALES.fnc
BEGIN
	  v_job_run_date:=p_job_run_date;
	  
	  /*Get the Rep ,distributor and associate rep details based on the product request detail id*/
     BEGIN
     	  SELECT get_compid_frm_cntx()
			INTO v_company_id
			FROM DUAL;
     
         SELECT c504a_consigned_to_id, c703_sales_rep_id, c703_ass_rep_id , c1900_company_id
           	INTO v_dist_id, v_rep_id, v_assoc_rep_id , v_req_company_id
		   FROM t504a_loaner_transaction
		  WHERE c526_product_request_detail_id = p_prod_req_dtl_id
		  AND c504a_void_fl IS NULL
		  AND ROWNUM = 1;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_dist_id := NULL;
        v_rep_id  := NULL;
        v_req_company_id := v_company_id;
    END;
    
	/*Get the expected return date ,return date and etch id based on the product request detail id*/
    /*Get the Max value of Loaner Transaction ID since the extension loaner records will have two entries in t504a_loaner_transaction table */
      SELECT expected_return_dt, return_dt , etchid
        INTO v_expectetd_dt, v_return_dt , v_etch_id
	    FROM
	    (SELECT  t504a.c504a_expected_return_dt expected_return_dt, NVL ( (t504a.c504a_return_dt),
	        TRUNC(SYSDATE)) return_dt, (TRUNC (NVL ( (t504a.c504a_return_dt), SYSDATE)) - TRUNC (t504a.c504a_expected_return_dt)) diff ,t504a.c504_consignment_id consignment_id ,t504.c504a_etch_id etchid
	       FROM t526_product_request_detail t526, t504a_loaner_transaction t504a, t525_product_request t525,t504a_consignment_loaner t504
	       WHERE t526.c526_product_request_detail_id = p_prod_req_dtl_id
	       AND T504a.C504a_Loaner_Transaction_Id = (SELECT Max(TO_NUMBER(T504a.C504a_Loaner_Transaction_Id)) 
                                                       FROM T504a_Loaner_Transaction T504a
                                                       WHERE T504a.C504a_Void_Fl IS NULL
                                                       AND T504a.C526_Product_Request_Detail_Id = t526.c526_product_request_detail_id
                                                       GROUP BY T504a.C526_Product_Request_Detail_Id)
	        AND t526.c526_product_request_detail_id = t504a.c526_product_request_detail_id
	        AND t504.c504_consignment_id            = t504a.c504_consignment_id
	        AND t525.c525_product_request_id        = t526.c525_product_request_id
	        AND t504.c504a_missed_fl               IS NULL
	        AND t504a.c504a_void_fl                IS NULL
	        AND t526.c526_void_fl                  IS NULL
	        AND t525.c525_void_fl                  IS NULL
	   ORDER BY diff DESC  
	    )
	  WHERE rownum = 1;
	  
	  /*if the job run date value as null then assign the v_return_dt to the job run date*/
	  IF TRUNC(v_return_dt) < NVL(TRUNC(v_job_run_date),TRUNC(v_return_dt)) THEN
	       v_job_run_date:=v_return_dt;
	  END IF;
  
	 /*Get the Loaner date based on the weekdays*/
		 SELECT cdate
		   INTO v_chrge_dt
		   FROM
		    (SELECT cal_date cdate
		       FROM date_dim
		      WHERE cal_date   >= NVL(TRUNC(v_job_run_date),TRUNC(v_return_dt)) - 8
		        AND cal_date   <= NVL(TRUNC(v_job_run_date),TRUNC(v_return_dt))
		        AND DAY NOT IN ('Sat','Sun')
		        AND date_key_id NOT IN (
				              	SELECT date_key_id 
				              	  FROM COMPANY_HOLIDAY_DIM 
				              	 WHERE HOLIDAY_DATE   >= NVL(TRUNC(v_job_run_date),TRUNC(v_return_dt)) - 8
				              	   AND HOLIDAY_DATE   <= NVL(TRUNC(v_job_run_date),TRUNC(v_return_dt))
				              	   AND HOLIDAY_FL = 'Y'
				              	    AND C1900_COMPANY_ID = v_req_company_id
				              	    )
		   ORDER BY cal_date DESC
		    )
		  WHERE rownum = 1;
	   
		  /*if the First day of the month is greater than the expected retun date then assign the expected return day value as the first day of the month
		   * if the First day of the month is Less than the expected retun date then assign the expected return day value as the actual return date
		   * */
		  
	  		SELECT 
			   CASE 
		       	WHEN v_first_day >= v_expectetd_dt
		       THEN v_first_day
		       	WHEN v_first_day <= v_expectetd_dt
		       THEN v_expectetd_dt  END
		       		INTO v_expectetd_dt
		      FROM DUAL;
	      
	    /*Pass the Expected return date,charge date and the product request detail id to the below function and get the
	    * loaner late fee charges based on the set level*/
		 /* Calculate the Loaner Fee charge based on the days return from the expected return date 
		   * Example calculation for Loaner Fee charge based on the set
		   * Request #249780 has 8 sets with expected date of 05/06/2019
		   * 5 sets returned on 05/03/2019 so there will be No charge for those 5 sets
		   * 6th set is returned on 05/07/2019 ,so it's 1 day delay , so request 249780 will have a charge of $50 for 6th set
		   * 7th set is returned on 05/08/2019 , 2 day delay for the 7th set , so request 249780 will have a charge of $100 for 7th set
		   * 8th set is returned on 05/09/2019 , 3 day delay for the 8th set , so request 249780 will have a charge of $200 for 8th set
		   * 
		  */
		  
    	 v_late_fee := gm_pkg_op_loaner.get_loaner_late_charge_by_set(TRUNC(v_expectetd_dt), TRUNC(v_chrge_dt),p_request_id,p_prod_req_dtl_id) ;
		
    	 /*Get the Incident Id for the Product request detail id from T9200_incident table,
		 * Update the Amount and sysdate for the incidents if the record is available for the current month
		 * */
    	 BEGIN
	         SELECT c9200_incident_id
	           INTO v_incident_id
	           FROM t9200_incident
	          WHERE c9200_ref_id       = TO_CHAR(p_prod_req_dtl_id)
	            AND c9200_ref_type     = 92068  -- Loaner LateFee(BySet)
	            AND c9200_void_fl     IS NULL
	            AND TO_CHAR(c9200_incident_date,'MMYYYY') = TO_CHAR(SYSDATE,'MMYYYY')
	            AND ROWNUM = 1;            
	     EXCEPTION
		     WHEN NO_DATA_FOUND THEN
		        v_incident_id := NULL;
	     END;

    	 IF v_incident_id IS NOT NULL THEN         
	    	 UPDATE T9200_Incident 
				Set c9200_incident_date = SYSDATE, 
				  C9200_Updated_By = p_user_id,
				  C9200_Updated_Date = SYSDATE 
	  			WHERE C9200_Incident_Id = v_incident_id
	  			AND C9200_Void_Fl  IS NULL;
	  			
	         UPDATE t9201_charge_details
	            SET c9201_amount= v_late_fee
	            , C9201_UPDATED_BY = p_user_id                                                                                                                                                                                  
				, C9201_UPDATED_DATE = SYSDATE                        
	          WHERE C9200_Incident_Id = v_incident_id
	            AND C9201_VOID_FL IS NULL;
	
   		 ELSE
         	 IF v_incident_id IS NULL   AND v_late_fee > 0 THEN 
         	  SELECT 'GM-IC-' || s9200_incident.NEXTVAL 
         		INTO v_incident_id 
         	   FROM DUAL;
         
		         /*Insert the Charge and charge details details in the below tables*/
		         INSERT
		           INTO t9200_incident
		            (
		                c9200_incident_id, c9200_ref_type, c9200_ref_id,
		                c901_incident_type, c9200_incident_value, c9200_created_by,
		                c9200_created_date, c9200_incident_date,c1900_company_id
		            )
		            VALUES
		            (
		                v_incident_id, 92068,TO_CHAR(p_prod_req_dtl_id),
		                92068, v_etch_id, p_user_id,
		                SYSDATE, NVL(v_job_run_date,v_return_dt),v_req_company_id
		            ) ;
			
		           INSERT
		           INTO t9201_charge_details
		            (
		                c9201_charge_details_id, c9200_incident_id, c703_sales_rep_id,
		                c701_distributor_id, c9201_amount, c9201_status,
		                c9201_created_by, c9201_created_date, c703_ass_rep_id
		            )
		            VALUES
		            (
		                s9201_charge_details.NEXTVAL, v_incident_id, v_rep_id,
		                v_dist_id, v_late_fee, p_status,
		                p_user_id, SYSDATE, v_assoc_rep_id
		            ) ;
          END IF;
	END IF;
END gm_sav_late_charges_by_set;

/******************************************************************
 * Description : procedure to save loaner late charge
 * Author      : dreddy
 *****************************************************************/
PROCEDURE gm_sav_late_charges_by_request (
        p_request_id IN 	t9200_incident.c9200_ref_id%TYPE,
        p_status     IN 	t9201_charge_details.c9201_status%TYPE,
        p_user_id    IN 	t9201_charge_details.c9201_created_by%TYPE,
        p_consign_id IN     t504a_loaner_transaction.c504_consignment_id%TYPE,
        p_job_run_date IN    DATE)
AS
    v_incident_id 	VARCHAR2(20);
    v_dist_id     	NUMBER;
    v_rep_id      	NUMBER;
    v_late_fee    	VARCHAR2(50);
    v_detail_id   	t9201_charge_details.c9201_charge_details_id%TYPE;
    v_chg_status  	t9201_charge_details.c9201_status%TYPE;
    v_expectetd_dt  DATE;
    v_return_dt     DATE;
    v_amount        NUMBER;
    --v_rul_dt        DATE;
    v_chrge_dt      DATE;
    v_job_run_date  DATE;
    v_assoc_rep_id  t504a_loaner_transaction.c703_ass_rep_id%TYPE;
    v_company_id NUMBER;
    v_req_company_id  NUMBER;
BEGIN
	 -- rule value for testing purpose 
	--v_rul_dt := TO_DATE(get_rule_value ('LATEFEECAL', 'LATEFEECAL'),'MM/DD/YYYY'); 
	  v_job_run_date:=p_job_run_date;
	  
     BEGIN
     
     	  SELECT get_compid_frm_cntx()
			INTO v_company_id
			FROM DUAL;
     
         SELECT c504a_consigned_to_id, c703_sales_rep_id, c703_ass_rep_id , c1900_company_id
           INTO v_dist_id, v_rep_id, v_assoc_rep_id , v_req_company_id
		   FROM t504a_loaner_transaction
		  WHERE c526_product_request_detail_id IN
		    (SELECT c526_product_request_detail_id
		       FROM t526_product_request_detail
		      WHERE c525_product_request_id = p_request_id
		        AND c526_void_fl IS NULL
		    )
		AND c504a_void_fl IS NULL
		AND ROWNUM = 1;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_dist_id := NULL;
        v_rep_id  := NULL;
        v_req_company_id := v_company_id;
    END;
      SELECT expected_return_dt, return_dt
        INTO v_expectetd_dt, v_return_dt
	    FROM
	    (SELECT NVL (c525_l_fee_chgd_date, t504a.c504a_expected_return_dt) expected_return_dt, NVL ( (t504a.c504a_return_dt),
	        TRUNC(SYSDATE)) return_dt, (TRUNC (NVL ( (t504a.c504a_return_dt), SYSDATE)) - TRUNC (NVL (c525_l_fee_chgd_date,
	        t504a.c504a_expected_return_dt))) diff ,t504a.c504_consignment_id consignment_id
	       FROM t526_product_request_detail t526, t504a_loaner_transaction t504a, t525_product_request t525,t504a_consignment_loaner t504
	      WHERE t526.c525_product_request_id        = p_request_id
	        AND t526.c526_product_request_detail_id = t504a.c526_product_request_detail_id
	        AND t504.c504_consignment_id            = t504a.c504_consignment_id
	        AND t525.c525_product_request_id        = t526.c525_product_request_id
	        AND t504.c504a_missed_fl               IS NULL
	        AND t504a.c504a_void_fl                IS NULL
	        AND t526.c526_void_fl                  IS NULL
	        AND t525.c525_void_fl                  IS NULL
	   ORDER BY diff DESC  
	    )
	  WHERE rownum = 1;
	  
	  IF TRUNC(v_return_dt) < NVL(TRUNC(v_job_run_date),TRUNC(v_return_dt)) THEN
	       v_job_run_date:=v_return_dt;
	  END IF;
	  
	  BEGIN
         SELECT MAX(c9200_incident_id)
           INTO v_incident_id
           FROM t9200_incident
          WHERE c9200_ref_id       = p_request_id
            AND c9200_ref_type     = 4127  -- Loaner
            AND c901_incident_type = 92072 -- LateFee
            AND c9200_void_fl     IS NULL
            AND TO_CHAR(c9200_incident_date,'MMYYYY') = TO_CHAR(NVL(v_job_run_date,SYSDATE),'MMYYYY');            
     EXCEPTION
     WHEN NO_DATA_FOUND THEN
        v_incident_id := NULL;
     END;
	 -- get the last working day
		 SELECT cdate
		   INTO v_chrge_dt
		   FROM
		    (SELECT cal_date cdate
		       FROM date_dim
		      WHERE cal_date   >= NVL(TRUNC(v_job_run_date),TRUNC(v_return_dt)) - 8
		        AND cal_date   <= NVL(TRUNC(v_job_run_date),TRUNC(v_return_dt))
		        AND DAY NOT IN ('Sat','Sun')
		        AND date_key_id NOT IN (
				              	SELECT date_key_id 
				              	  FROM COMPANY_HOLIDAY_DIM 
				              	 WHERE HOLIDAY_DATE   >= NVL(TRUNC(v_job_run_date),TRUNC(v_return_dt)) - 8
				              	   AND HOLIDAY_DATE   <= NVL(TRUNC(v_job_run_date),TRUNC(v_return_dt))
				              	   AND HOLIDAY_FL = 'Y'
				              	    AND C1900_COMPANY_ID = v_req_company_id
				              	    )
		   ORDER BY cal_date DESC
		    )
		  WHERE rownum = 1;
	  
    v_late_fee := gm_pkg_op_loaner.get_loaner_late_charge(TRUNC(v_expectetd_dt), TRUNC(v_chrge_dt),p_request_id) ;
    BEGIN
         SELECT c9201_status ,c9201_charge_details_id ,NVL(c9201_amount,0) -- get late fee amount of the current month existing record
           INTO v_chg_status, v_detail_id ,v_amount
           FROM t9201_charge_details
          WHERE c9200_incident_id = v_incident_id
            AND c9201_void_fl IS NULL
            AND c9201_status  <= 20
            AND ROWNUM        = 1;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_chg_status := NULL;
        v_detail_id  := NULL;
        v_amount     :=0;
    END;
    IF v_chg_status IS NOT NULL AND v_chg_status <= 20 THEN
         -- update late fee with (existing + new)amount of current month    
         UPDATE t9201_charge_details
            SET c9201_amount= v_late_fee+v_amount
            , C9201_UPDATED_BY = p_user_id                                                                                                                                                                                  
			, C9201_UPDATED_DATE = SYSDATE                        
          WHERE c9201_charge_details_id = v_detail_id
            AND c9201_void_fl IS NULL;
	
    ELSE
      -- insert new charge record if there is no record for request OR current month OR charge record cancelled
       IF (v_incident_id IS NULL  OR (v_incident_id IS NOT NULL AND v_chg_status > 20)) AND v_late_fee > 0 THEN 
         SELECT 'GM-IC-' || s9200_incident.NEXTVAL INTO v_incident_id FROM DUAL;
         INSERT
           INTO t9200_incident
            (
                c9200_incident_id, c9200_ref_type, c9200_ref_id,
                c901_incident_type, c9200_incident_value, c9200_created_by,
                c9200_created_date, c9200_incident_date,c1900_company_id
            )
            VALUES
            (
                v_incident_id, 4127, p_request_id,
                92072, p_consign_id, p_user_id,
                SYSDATE, NVL(v_job_run_date,v_return_dt),v_req_company_id
            ) ;
       END IF;
         INSERT
           INTO t9201_charge_details
            (
                c9201_charge_details_id, c9200_incident_id, c703_sales_rep_id,
                c701_distributor_id, c9201_amount, c9201_status,
                c9201_created_by, c9201_created_date, c703_ass_rep_id
            )
            VALUES
            (
                s9201_charge_details.NEXTVAL, v_incident_id, v_rep_id,
                v_dist_id, v_late_fee, p_status,
                p_user_id, SYSDATE, v_assoc_rep_id
            ) ;
    END IF;
    
    -- update request with charge date
        UPDATE t525_product_request t525
           SET c525_l_fee_chgd_date = v_chrge_dt           
         WHERE c525_product_request_id = p_request_id;
END gm_sav_late_charges_by_request;

END gm_pkg_op_charges;
/
