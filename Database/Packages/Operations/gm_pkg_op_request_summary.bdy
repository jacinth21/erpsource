/* Formatted on 2010/02/04 11:32 (Formatter Plus v4.8.0) */
-- @"c:\database\packages\operations\gm_pkg_op_request_summary.bdy"
CREATE OR REPLACE
PACKAGE BODY gm_pkg_op_request_summary
IS
    --
    /*******************************************************
    * Description : Procedure to get request header
    * Author   : Rakhi Gandhi
    *******************************************************/
    --
PROCEDURE gm_fch_request_header (
        p_request_id IN t520_request.c520_request_id%TYPE,
        p_outfcheader OUT TYPES.cursor_type)
AS
    v_request_id t520_request.c520_request_id%TYPE;
    v_comp_dt_fmt VARCHAR2 (20); 
BEGIN
	  SELECT get_compdtfmt_frm_cntx() INTO v_comp_dt_fmt FROM DUAL;
	
     SELECT c520_request_id
       INTO v_request_id
       FROM t520_request
      WHERE c520_request_id = p_request_id
        AND c520_void_fl   IS NULL;
    OPEN p_outfcheader FOR SELECT TO_CHAR (t520.c520_request_date, v_comp_dt_fmt) requestdate,
    TO_CHAR (t520.c520_required_date, v_comp_dt_fmt) requireddate,
    TO_CHAR (t520.c520_planned_ship_date, v_comp_dt_fmt) planShipDate,
    t520.c901_request_source requestsourceid,
    get_code_name (t520.c901_request_source) requestsource,
    DECODE (c901_request_source, 50616, gm_pkg_op_sheet.get_demandsheet_name (t520.c520_request_txn_id),
    t520.c520_request_txn_id) requesttxnid,
    t520.c207_set_id setid,
    get_set_name (t520.c207_set_id) setname,
    get_code_name (t520.c901_request_by_type) requesttype,
    t520.c520_request_for reqfor,
    DECODE (t520.c520_request_for, 40021,get_distributor_name (t520.c520_request_to), 40025, get_account_name(t520.c520_request_to),102930, get_distributor_name (t520.c520_request_to), 40022, get_user_name (
    t520.c520_ship_to_id)) requestto,
    gm_pkg_op_request_summary.get_request_status (t520.c520_status_fl) requeststatus,
    get_user_name (t504.c504_created_by) iniby,
    TO_CHAR (t504.c504_created_date,  v_comp_dt_fmt ||' HH:MI AM') inidate,
    t504.c504_verify_fl vfl,
    t520.C520_RA_FL RAFLAG, 
    t520.C506_RMA_ID RMAID,
    get_user_name (t504.c504_last_updated_by) lastupdnm,
    TO_CHAR (t504.c504_last_updated_date, v_comp_dt_fmt ||' HH:MI AM') lastupddt,
    DECODE (t504.c504_status_fl, '0', 'Initiated', '1', 'WIP', '1.10','Completed set', '1.20', 'Pending Putaway','2', DECODE (t504.c504_verify_fl, NULL,'Pending Control Number', 0, 'Completed set', 1,
    'Built set'),'2.20','Pending Pick', '3', 'Pending Shipping', '3.30', 'Packing In Progress', '3.60', 'Ready For Pickup', '4', 'Closed','5', 'Transferred', t504.c504_status_fl) consignflag,
    t504.c504_consignment_id consignid,
    t504.c504_status_fl statusflag,
    t520.c520_req_dt_history_fl historyfl,
    t520.c520_status_fl requeststatusflag,
    t504.c504_verify_fl verifyflag,
    get_code_name (t520.c520_request_for) requestfor,
    t520.c520_request_to reqto,
    t520.c901_ship_to requestshipto,
    t520.c520_ship_to_id requestshiptoid,
    get_code_name (t520.c901_ship_to) shipto,
    get_code_name (t520.c520_ship_to_id) shiptoid,
    CEIL (TRUNC (t520.c520_required_date) - CURRENT_DATE) daysdiff,
    t520.c1910_division_id division_id,
    get_rule_value ('SETCONSIGNCOUNTDOWN', 'COUNTDOWN') daysdiffrule, NVL(c901_purpose,0) strRequestPurpose,
    t520.C525_Product_Request_Id loanerreqid  -- PMT-32450 - Getting loaner request Id for Editing the Request 
    FROM t520_request t520,
    t504_consignment t504 WHERE t520.c520_request_id = p_request_id AND t520.c520_request_id = t504.c520_request_id(+)
    AND t520.c520_void_fl                           IS NULL AND t504.c504_void_fl IS NULL;
EXCEPTION
WHEN NO_DATA_FOUND THEN
    raise_application_error ( - 20903, '') ;
END gm_fch_request_header;
-- , t504.c504_status_fl statusflag
--
/*******************************************************
* Description : Procedure to get screen for material request reconfigure
* Author   : Rakhi Gandhi
*******************************************************/
--
PROCEDURE gm_fch_pending_request_detail (
        p_request_id IN t520_request.c520_request_id%TYPE,
        p_outboheader OUT TYPES.cursor_type,
        p_outfcdetail OUT TYPES.cursor_type)
AS
    v_mr_status_fl t520_request.c520_status_fl%TYPE;
    v_bo_request_id t520_request.c520_request_id%TYPE;
    v_consign_id t504_consignment.c504_consignment_id%TYPE;
    v_temp VARCHAR2 (10) ;
    v_div_type		VARCHAR2(20) := '100823'; -- US Distributor
    v_request_to	t520_request.c520_request_to%TYPE;
BEGIN
	
	
       BEGIN
	       SELECT C520_REQUEST_TO INTO v_request_to 
	       		FROM T520_REQUEST 
	       	WHERE C520_REQUEST_ID = p_request_id AND C520_VOID_FL IS NULL;
	   EXCEPTION WHEN NO_DATA_FOUND
	   THEN
	   		v_request_to := NULL;
	   END;
	   v_div_type := NVL(get_distributor_division(v_request_to),'100823'); -- Fetch the US/OUS Distributor
    --get bo header
    OPEN p_outboheader FOR SELECT t520.c520_request_id boreqid,
    gm_pkg_op_request_summary.get_request_status (t520.c520_status_fl) bostatus,
    get_user_name (t520.c520_created_by) bocreatedby,
    TO_CHAR (t520.c520_created_date, GET_RULE_VALUE('DATEFMT', 'DATEFORMAT')) bocreateddate FROM t520_request t520 WHERE (t520.c520_request_id =
        p_request_id OR t520.c520_master_request_id                                                                 =
        p_request_id) AND t520.c520_status_fl                                                                       =
    '10' AND t520.c520_void_fl                                                                                     IS
    NULL;
    --get the details
    OPEN p_outfcdetail FOR SELECT t521.c205_part_number_id pnum,
    get_partnum_desc (t521.c205_part_number_id) pdesc,
    t521.c521_qty qty,
    get_qty_in_bulk (c205_part_number_id) bulkqty,
    DECODE(v_div_type,'100824',(get_available_qty_for_cn (p_request_id, c205_part_number_id)+get_qty_in_inv(c205_part_number_id,'56001')),get_available_qty_for_cn (p_request_id, c205_part_number_id)) available FROM t520_request t520,
    t521_request_detail t521 WHERE t520.c520_request_id = t521.c520_request_id AND t520.c520_void_fl IS NULL AND (
        t520.c520_request_id                            = p_request_id OR t520.c520_master_request_id = p_request_id)
    ORDER BY t521.c205_part_number_id;
    --   and t520.C520_MASTER_REQUEST_ID = p_request_id AND t520.C520_STATUS_FL='10'  ;
END gm_fch_pending_request_detail;
--
/*******************************************************
* Purpose: function is used to request status
author Xun
*******************************************************/
--
FUNCTION get_request_status (
        p_statusflag IN t520_request.c520_status_fl%TYPE)
    RETURN VARCHAR2
IS
    v_status_name VARCHAR2 (100) ;
BEGIN
     SELECT DECODE (p_statusflag, - 20, 'Rejected', - 10, 'Cancelled'
        --, -5, 'On Hold'
        , 0, 'Requested', 10, 'Back Order', 15, 'Back Log', 20, 'Ready to Consign', 30, 'Ready to Ship', 40,
        'Completed', 45, 'Returned Stock', 50, 'On Hold',60,'Transferred')
       INTO v_status_name
       FROM DUAL;
    RETURN v_status_name;
END get_request_status;
--
/**************************************************************************
* Description : Procedure to fetch request detail
author: Xun
*************************************************************************/
PROCEDURE gm_fch_summary_csg_req_detail (
        p_requestid IN t520_request.c520_request_id%TYPE,
        p_detail OUT TYPES.cursor_type)
AS
    v_consignment_id t504_consignment.c504_consignment_id%TYPE;
BEGIN
    --
    gm_pkg_op_request_summary.gm_fch_csgid_from_mrid (p_requestid, v_consignment_id) ;
    -- Change details
    OPEN p_detail FOR SELECT t521.c205_part_number_id pnum,
    get_partnum_desc (t521.c205_part_number_id) pdesc,
    t521.c521_qty qty FROM t520_request t520,
    t521_request_detail t521 WHERE t520.c520_request_id = p_requestid AND t520.c520_request_id = t521.c520_request_id
    AND t520.c520_void_fl                              IS NULL
  UNION ALL
     SELECT t505.c205_part_number_id pnum, get_partnum_desc (t505.c205_part_number_id) pdesc, SUM (t505.c505_item_qty)
        qty
       FROM t505_item_consignment t505
      WHERE t505.c504_consignment_id = v_consignment_id
   GROUP BY t505.c205_part_number_id ORDER BY pnum;
END gm_fch_summary_csg_req_detail;
/**************************************************************************
* Description : Procedure to fetch associated request detail
author: Xun
*************************************************************************/
PROCEDURE gm_fch_request_assc_txn (
        p_requestid IN t520_request.c520_request_id%TYPE,
        p_header OUT TYPES.cursor_type,
        p_detail OUT TYPES.cursor_type)
AS
BEGIN
    -- Change details
    OPEN p_header FOR SELECT t520.c520_request_id reqid,
    gm_pkg_op_request_summary.get_request_status (t520.c520_status_fl) status FROM t520_request t520,
    t504_consignment t504 WHERE t520.c520_master_request_id = p_requestid AND t520.c520_request_id =
    t504.c520_request_id(+) AND t520.c520_void_fl          IS NULL;
    OPEN p_detail FOR SELECT t520.c520_request_id,
    t521.c205_part_number_id pnum,
    get_partnum_desc (t521.c205_part_number_id) pdesc,
    t521.c521_qty qty FROM t520_request t520,
    t521_request_detail t521 WHERE t520.c520_master_request_id = p_requestid AND t520.c520_request_id =
    t521.c520_request_id AND t520.c520_void_fl                IS NULL
      UNION
     SELECT t520.c520_request_id, t505.c205_part_number_id pnum, get_partnum_desc (t505.c205_part_number_id) pdesc
      , SUM (t505.c505_item_qty) qty
       FROM t520_request t520, t504_consignment t504, t505_item_consignment t505
      WHERE t520.c520_master_request_id = p_requestid
        AND t520.c520_request_id        = t504.c520_request_id
        AND t504.c504_consignment_id    = t505.c504_consignment_id
        AND t520.c520_void_fl          IS NULL
        AND t504.c504_void_fl          IS NULL
   GROUP BY t520.c520_request_id, t505.c205_part_number_id;
END gm_fch_request_assc_txn;
--
/**************************************************************************
* Description : Procedure to fetch Consignment details for set building
* Author : Joe P Kumar
*************************************************************************/
PROCEDURE gm_fch_summary_csg_detail (
        p_consignmentid  IN t504_consignment.c504_consignment_id%TYPE,
        p_ref_consign_id IN t504_consignment.c504_consignment_id%TYPE,
        p_outdetail OUT TYPES.cursor_type)
AS
BEGIN
    OPEN p_outdetail FOR SELECT t505.c505_item_consignment_id itemcsgid,
    t505.c205_part_number_id pnum,
    get_partnum_desc (t505.c205_part_number_id) pdesc,
    t505.c505_item_qty qty,
    DECODE (p_ref_consign_id, '', t505.c505_control_number, NVL (get_control_number (p_ref_consign_id,
    t505.c205_part_number_id, t505.c505_item_qty), 'TBE')) controlnum,
    get_qty_in_bulk (t505.c205_part_number_id) bulkqty,
    get_set_qty (t504.c207_set_id, t505.c205_part_number_id) setqty,
    get_part_attribute_value (t505.c205_part_number_id, 92340) tagfl FROM t505_item_consignment t505,
    t504_consignment t504 WHERE t505.c504_consignment_id = p_consignmentid AND t504.c504_consignment_id =
    t505.c504_consignment_id AND t505.c505_void_fl      IS NULL AND t504.c504_void_fl IS NULL ORDER BY
    t505.c205_part_number_id;
END gm_fch_summary_csg_detail;
--
/**************************************************************************
* Description : Save for Material request -
* Author : Rakhi Gandhi
*************************************************************************/
PROCEDURE gm_sav_reconfig_part (
        p_request_id       IN t520_request.c520_request_id%TYPE,
        p_part_to_reconfig IN VARCHAR2,
        p_user             IN t520_request.c520_created_by%TYPE,
        p_out_newrequest_id OUT t520_request.c520_request_id%TYPE,
        p_out_newconsign_id OUT t504_consignment.c504_consignment_id%TYPE,
        p_out_inhtxn_id OUT t412_inhouse_transactions.c412_inhouse_trans_id%TYPE)
AS
    v_newmr t520_request.c520_request_id%TYPE         := NULL;
    v_newcn t504_consignment.c504_consignment_id%TYPE := NULL;
    v_request_id t520_request.c520_request_id%TYPE;
    v_bo_request_id t520_request.c520_request_id%TYPE;
    v_strlen NUMBER           := NVL (LENGTH (p_part_to_reconfig), 0) ;
    v_string VARCHAR2 (30000) := p_part_to_reconfig;
    v_pnum t205_part_number.c205_part_number_id%TYPE;
    v_qty         NUMBER;
    v_action      VARCHAR2 (20) ;
    v_substring   VARCHAR2 (1000) ;
    v_msg         VARCHAR2 (1000) ;
    v_consigntype VARCHAR2 (1000) ; -- ( built set , 2 WIP , 3 item -consignment from UI perspective )
    v_consign_id t504_consignment.c504_consignment_id%TYPE;
    v_reconfigure_log_id t530_reconfigure_log.c530_reconfigure_log_id%TYPE;
    v_out_reconfig_log_detail_id t531_reconfigure_log_detail.c531_reconfigure_log_detail_id%TYPE;
    v_mr_status_fl t520_request.c520_status_fl%TYPE;
    v_data_exists     NUMBER;
    v_releast_present NUMBER;
    v_current_bo_status t520_request.c520_status_fl%TYPE;
    v_new_temp_consignment_id t504_consignment.c504_consignment_id%TYPE;
    v_new_bo_request_id t520_request.c520_request_id%TYPE;
    v_temp_request_id t520_request.c520_request_id%TYPE;
    v_parent_request_id t520_request.c520_request_id%TYPE;
    v_allocate_fl VARCHAR2 (1) ;
    v_reqatt      VARCHAR2 (2000) ;
    v_account_id t520_request.c704_account_id%TYPE;
    v_new_consign_cnt t504_consignment.c504_consignment_id%TYPE;
BEGIN
    v_releast_present := 0;
    -- 3. Get the backorder MR ID --
    BEGIN
         SELECT c520_request_id, NVL (c520_master_request_id, c520_request_id), c704_account_id
           INTO v_bo_request_id, v_parent_request_id, v_account_id
           FROM t520_request t520
          WHERE (c520_request_id      = p_request_id
            OR c520_master_request_id = p_request_id)
            AND t520.c520_status_fl   = '10'
            AND t520.c520_void_fl    IS NULL;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        raise_application_error ( - 20903, '') ; -- no backorder items found
    END;
    -- 0. get consignmet ID for it - how will this work for item consignment etc. Also if consingment ID is not yet
    -- created then ? - todo
    BEGIN
         SELECT c504_consignment_id
           INTO v_consign_id
           FROM t504_consignment t504
          WHERE t504.c520_request_id = v_parent_request_id
            AND t504.c504_void_fl   IS NULL;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_consign_id := NULL; -- ( for a consignment where no CN created yet since nothing was available on initiation
        -- of set )
    END;
    -- 1. Get consignment type
    v_consigntype := gm_pkg_op_request_summary.get_consignment_status (v_parent_request_id) ; --'1'  ;   --TODO: change
    -- this to use function  - todo
    IF (v_consigntype = '2' OR v_consigntype = '3') THEN -- IN WIP  SET / WIP item consignment
        v_newcn      := v_consign_id;
    END IF;
    -- Loop through the parts and reconfigure them
    WHILE INSTR (v_string, '|') <> 0
    LOOP
        v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
        v_string    := SUBSTR (v_string, INSTR (v_string, '|')    + 1) ;
        v_pnum      := NULL;
        v_qty       := NULL;
        v_action    := NULL;
        v_pnum      := SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1) ;
        v_substring := SUBSTR (v_substring, INSTR (v_substring, ',')    + 1) ;
        v_qty       := SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1) ;
        v_substring := SUBSTR (v_substring, INSTR (v_substring, ',')    + 1) ;
        v_action    := v_substring;
        gm_pkg_op_request_summary.gm_sav_reconfig_request_cons (p_request_id --user entered req id
        , v_bo_request_id -- correct bo mr id for this mr
        , v_pnum, v_qty, v_action, v_consigntype -- builtset/wip/item consignment
        , v_consign_id -- CN id for this MR id - null if no CN - directly from consignment table
        , p_user, v_newcn, v_newmr, p_out_inhtxn_id) ;
    END LOOP;
    -- Check if CN exists for current bo mr id
    BEGIN
         SELECT c504_consignment_id
           INTO v_new_temp_consignment_id
           FROM t504_consignment t504
          WHERE t504.c520_request_id = v_bo_request_id
            AND t504.c504_void_fl   IS NULL;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_new_temp_consignment_id := NULL;
    END;
    IF (v_new_temp_consignment_id IS NULL) THEN
         SELECT COUNT (c520_request_id)
           INTO v_data_exists
           FROM t521_request_detail
          WHERE c520_request_id = v_bo_request_id;
          
        IF v_data_exists = 0 THEN
             UPDATE t520_request
                SET c520_void_fl        = 'Y'
                  , c520_last_updated_by = p_user
                  , c520_last_updated_date = CURRENT_DATE
              WHERE c520_request_id = v_bo_request_id; --void it
              
        END IF;
        
    END IF;
    p_out_newrequest_id           := '';
    p_out_newconsign_id           := v_consign_id; --could be null
    
    IF (v_new_temp_consignment_id IS NOT NULL) THEN
    
        gm_sav_move_backorder_items (p_request_id, v_bo_request_id, v_new_bo_request_id, p_user) ;
        
         UPDATE t520_request
            SET c520_status_fl = '15'
              , c520_last_updated_by = p_user
              , c520_last_updated_date = CURRENT_DATE 
          WHERE c520_request_id = v_bo_request_id; --BLog
          
     -- Start of updating the Request Attribute Values from the Parent Request
      SELECT distinct RTRIM(XMLAGG(XMLELEMENT(e,(C901_ATTRIBUTE_TYPE || '^' || C520A_ATTRIBUTE_VALUE )|| '|')).EXTRACT('//text()'),',')
        INTO v_reqatt 
        FROM T520A_REQUEST_ATTRIBUTE
       WHERE C520_REQUEST_ID = p_request_id
         AND C520A_VOID_FL  IS NULL;
    
    IF v_reqatt            IS NOT NULL THEN
        gm_pkg_op_request_master.gm_sav_request_attribute (v_bo_request_id, v_reqatt, p_user) ;
    END IF;
    
    -- END
    p_out_newrequest_id := v_bo_request_id;
    p_out_newconsign_id := v_new_temp_consignment_id;
    -- if only item consignment then system should allocate the CN to Device
    v_consigntype    := gm_pkg_op_request_summary.get_consignment_status (p_request_id) ;
     BEGIN
        SELECT count(1)
           INTO v_new_consign_cnt
           FROM t504_consignment t504
          WHERE t504.c504_consignment_id = p_out_newconsign_id
            AND t504.c504_status_fl <= '2'
            AND t504.c504_type = '4110'
            AND t504.c207_set_id IS NULL
            AND t504.c504_void_fl IS NULL;
    END;
    /*Need to Pick the item consignments in device 
     *when we used master request to release backorders from shipped set.
     * */
    IF (v_consigntype = '3' OR v_new_consign_cnt > 0) THEN
        -- WHEN REQUEST IS RECONFIGURED, THE NEW CN SHOULD BE ALLOCATED TO DEVICE FOR PICKING.
        -- CHANGE FOR MNTTASK-751
        v_allocate_fl              := gm_pkg_op_inventory.chk_txn_details (p_out_newconsign_id, '93002') ;
        IF NVL (v_allocate_fl, 'N') = 'Y' THEN
            gm_pkg_allocation.gm_ins_invpick_assign_detail ('93002', p_out_newconsign_id, p_user) ;
        END IF;
    END IF;
END IF;
-- India country (account data sync)
gm_pkg_op_order_master.gm_sav_sync_acc_attribute(p_out_newconsign_id , v_account_id, 'CONS', p_user);
-- update lastupdated date for consignment
-- Ask should request table have date filed or what ?
-- TODO: Later uncomment following line please
-- UPDATE t504_consignment   SET c504_last_updated_date = CURRENT_DATE WHERE c504_consignment_id = p_consign_id;
END gm_sav_reconfig_part;
--
PROCEDURE gm_sav_move_backorder_items (
        p_request_id IN t520_request.c520_request_id%TYPE --user entered
        ,
        p_bo_request_id IN t520_request.c520_request_id%TYPE --bo mr
        ,
        p_new_bo_request_id OUT t520_request.c520_request_id%TYPE,
        p_user IN VARCHAR2)
AS
    v_bo_exists NUMBER := 0;
    v_out_distributor_req_id t520_request.c520_request_to%TYPE;
    v_out_bo_ship_to t520_request.c901_ship_to%TYPE;
    v_out_bo_shipto_id t520_request.c520_ship_to_id%TYPE;
    v_distributor_id t520_request.c520_request_to%TYPE;
BEGIN
     SELECT COUNT (c520_request_id)
       INTO v_bo_exists
       FROM t521_request_detail
      WHERE c520_request_id = p_bo_request_id;
    IF v_bo_exists          > 0 THEN
     
          --get the distributor id from the back order request id(PMT-35132).
       			 gm_fch_bo_ship_dtl(p_request_id,v_out_distributor_req_id,v_out_bo_ship_to,v_out_bo_shipto_id);
  
		     IF v_out_distributor_req_id IS NOT NULL THEN	    		
					gm_sav_create_new_request (p_bo_request_id, p_new_bo_request_id, p_user) ; --
		     ELSE 
        			gm_sav_create_new_request (p_request_id, p_new_bo_request_id, p_user) ; -- ask what should be the parent mr -- user entered, or bo
        	END IF;
  
         UPDATE t521_request_detail
        SET c520_request_id     = p_new_bo_request_id
          WHERE c520_request_id = p_bo_request_id;

		--Whenever the detail is updated, the request table has to be updated ,so ETL can Sync it to GOP.
        UPDATE t520_request
           SET c520_last_updated_date = CURRENT_DATE
             , c520_last_updated_by = p_user
         WHERE c520_request_id = p_bo_request_id;           
    END IF;
END gm_sav_move_backorder_items;
/*******************************************************
* Description : Procedure  material request reconfigure
* Author   : Rakhi Gandhi
*******************************************************/
PROCEDURE gm_sav_reconfig_request_cons (
        p_request_id IN t520_request.c520_request_id%TYPE -- user entered mr
        ,
        p_bo_request_id IN t520_request.c520_request_id%TYPE -- bo mr for this mr
        ,
        p_part_num         IN t205_part_number.c205_part_number_id%TYPE,
        p_qty              IN VARCHAR2,
        p_action           IN VARCHAR2,
        p_consignment_type IN VARCHAR2 --- built set / WIP / item consignment
        ,
        p_consign_id IN t504_consignment.c504_consignment_id%TYPE -- directly from consign table. could be null if no
        -- CN created yet - how to copy in this case.
        ,
        p_user               IN t520_request.c520_created_by%TYPE,
        p_out_new_consign_id IN OUT t504_consignment.c504_consignment_id%TYPE -- NEW CN
        ,
        p_out_new_request_id IN OUT t520_request.c520_request_id%TYPE -- NEW MR
        ,
        p_out_new_inhtxn_id IN OUT t412_inhouse_transactions.c412_inhouse_trans_id%TYPE)
AS
    v_allocqty t505_item_consignment.c505_item_qty%TYPE;
    v_out_remaining_qty t505_item_consignment.c505_item_qty%TYPE;
    v_out_item_consign_id t505_item_consignment.c505_item_consignment_id%TYPE;
    v_new_consignment_created VARCHAR2 (2) ;
    v_avail_qty               NUMBER;
    v_qty_to_process          NUMBER;
    v_input_str               VARCHAR2 (50) ;
    v_message                 VARCHAR2 (2) ;
    v_div_type 	VARCHAR2(20) := '100823'; -- US Distributor
    v_rwqty		   t208_set_details.c208_set_qty%TYPE;	
    v_request_to	t520_request.c520_request_to%TYPE;
    v_company_id   t504_consignment.c1900_company_id%TYPE;
    v_source 		t907_shipping_info.c901_source%TYPE;
    v_allocrwqty 	NUMBER := 0;
	v_left 			NUMBER := 0;
	v_request_for t520_request.c520_request_for%TYPE;
	v_lot_num        t505_item_consignment.C505_CONTROL_NUMBER%TYPE;
	v_lot_trans_fl   VARCHAR2(10);
	v_product_type t205_part_number.c205_product_family%TYPE;
	v504_type t504_consignment.c504_type%TYPE;
	v_out_distributor_req_id t520_request.c520_request_to%TYPE;
    v_out_bo_ship_to t520_request.c901_ship_to%TYPE;
    v_out_bo_shipto_id t520_request.c520_ship_to_id%TYPE;
BEGIN
    v_qty_to_process := p_qty;

    --   If type = release and get_avail_qty(type,partnum-stock etc.) < pqty , then pqty = avail qty
     SELECT get_available_qty_for_cn (p_request_id, p_part_num), get_qty_in_inv(p_part_num,'56001')
       INTO v_avail_qty, v_rwqty
       FROM DUAL;
       
         --PMT-20074 Stock transfer fullfill, get availale qty by action Shelf(106703)/Bulk(106704)
       IF (p_action = '106703' OR p_action = '106704')
       THEN
	     SELECT DECODE (p_action, '106703', get_qty_in_stock (p_part_num),'106704', get_qty_in_bulk (p_part_num)) 
	       INTO v_avail_qty
	       FROM DUAL;
	   END IF;
       BEGIN
	       SELECT C520_REQUEST_TO , C520_REQUEST_FOR ,c1900_company_id
	       INTO v_request_to , v_request_for ,v_company_id
	       		FROM T520_REQUEST 
	       	WHERE C520_REQUEST_ID = p_request_id AND C520_VOID_FL IS NULL;
	   EXCEPTION WHEN NO_DATA_FOUND
	   THEN
	   		v_request_to := NULL;
	   		v_request_for := NULL;
	   		v_company_id := NULL;
	   END;
           
	    -- If the request is stock transfer , need to update the log if qty is modified
	   IF v_request_for = '26240420'
       THEN
    	gm_rmv_items_from_ST_req(p_bo_request_id, p_part_num, v_qty_to_process, p_user);
	   END IF;
	   
		     -- 0. if qty = 0 return
	    IF v_qty_to_process = 0 THEN
	        RETURN;
	    END IF;
       
       v_div_type := NVL(get_distributor_division(v_request_to),'100823'); -- Fetch the US/OUS Distributor
       
       IF v_div_type = '100824' -- OUS Distributor
       THEN
       		IF v_rwqty < 0 
       		THEN
       			v_rwqty := 0;
       		END IF;
       	ELSE -- US Distributor
       		v_rwqty := 0;	
       	END IF;
       
    IF (p_action = '90809' AND (NVL(v_avail_qty,0) + NVL(v_rwqty,0)) < v_qty_to_process) THEN
        raise_application_error ( - 20079, '') ;
    END IF;
    --   1 . remove parts
 
    IF v_request_for <> '26240420'
    THEN
    	gm_sav_remove_from_request (p_bo_request_id, p_part_num, v_qty_to_process, p_action, v_out_remaining_qty) ;
    END IF;
    
    IF p_action = '90810' THEN -- todo: If action is void, return
        RETURN;
    END IF;
    -- Remaining code for Release action
    IF p_consignment_type = 1 THEN -- consignment status is 2 and verify flag is 1
        --2. create an Inhouse transaction
        IF p_out_new_inhtxn_id  IS NULL THEN
            p_out_new_inhtxn_id := get_next_consign_id ('50160') ;
            gm_save_inhouse_item_consign (p_out_new_inhtxn_id, 50160, NULL, 50625, p_user, NULL, p_user, NULL,
            v_message) ;
            --
            SELECT c1900_company_id
			   INTO v_company_id
			   FROM t504_consignment
			  WHERE c504_consignment_id = p_consign_id
			    AND c504_void_fl       IS NULL;
			
		    v_company_id := NVL(v_company_id,get_compid_frm_cntx());
			    
             UPDATE t412_inhouse_transactions
            SET c1900_company_id = NVL(v_company_id,c1900_company_id)
              , c412_ref_id               = p_consign_id
              , c412_last_updated_by = p_user
			  , c412_last_updated_date = CURRENT_DATE
              WHERE c412_inhouse_trans_id = p_out_new_inhtxn_id;
        END IF;
        gm_pkg_op_inhouse_master.gm_sav_inhouse_trans_items (p_out_new_inhtxn_id, p_part_num, v_qty_to_process, 'TBE',
        get_part_price (p_part_num, 'C')) ;
    ELSE
        -- 2. create new CN
        IF p_out_new_consign_id IS NULL THEN
            gm_sav_create_new_consign (p_consign_id, p_request_id, p_bo_request_id, p_out_new_consign_id, p_user,p_action) ;
            
            -- if request for is Stock transfer (26240420) then ship to source is Stock trasfer(26240435) else consignment(50181)
            SELECT DECODE(v_request_for,'26240420',26240435,50181) 
	              INTO v_source
	            FROM DUAL;
	            
       			  gm_fch_bo_ship_dtl(p_bo_request_id,v_out_distributor_req_id,v_out_bo_ship_to,v_out_bo_shipto_id);   
	       	             
	        IF v_out_distributor_req_id IS NOT NULL THEN
		    		gm_pkg_op_process_request.gm_sav_cpy_req_shipping (p_bo_request_id, p_out_new_consign_id, v_source, p_user) ;
		    ELSE 
        			gm_pkg_op_process_request.gm_sav_cpy_req_shipping (p_request_id, p_out_new_consign_id, v_source, p_user) ;
        	END IF;    
           
        END IF;
        --3. Add item to item_consignment with NEWCN
        -- Getting RW Qty if it is from OUS
         IF v_rwqty >= v_qty_to_process
		 THEN
	      	v_allocrwqty := v_qty_to_process; 
	      	v_left := 0;      
	   	 ELSIF v_rwqty > 0   AND v_rwqty < v_qty_to_process 
	     THEN 
	     	v_allocrwqty := v_rwqty; 
	     	v_left := v_qty_to_process - v_rwqty; 
	     ELSE
	     	v_left := v_qty_to_process;
	   	 END IF;
	   	 
	   	 IF v_avail_qty >0 AND v_avail_qty < v_left
	   	 THEN
	   	 	v_left := v_avail_qty;
	   	 END IF;
	   	 v_lot_num := 'TBE';
		 -- ALW_INS_PART_NOC -Controlling the instrument part to allow NOC#, the NOC# is passing v_lot_num in save consignment details.			   	
					   		SELECT get_partnum_product(p_part_num) INTO v_product_type FROM DUAL;
							   	IF get_rule_value_by_company(v_product_type,'ALW_INS_PART_NOC',v_company_id)='Y'  THEN
							   		
									v_lot_num := 'NOC#';
								
								END IF;
					   	
	   	 IF v_allocrwqty > 0
	   	 THEN
        	gm_pkg_op_request_master.gm_sav_consignment_detail (NULL, p_out_new_consign_id, p_part_num, v_lot_num,
        		v_qty_to_process, get_part_price (p_part_num, 'C'), NULL, NULL, NULL, '56001',v_out_item_consign_id) ;
         END IF;
         IF v_left >0 
         THEN
         	gm_pkg_op_request_master.gm_sav_consignment_detail (NULL, p_out_new_consign_id, p_part_num, v_lot_num,
        		v_qty_to_process, get_part_price (p_part_num, 'C'), NULL, NULL, NULL,'90800', v_out_item_consign_id) ;
         END IF;
    END IF;
    
    --When ever the child is updated, the consignment table has to be updated ,so ETL can Sync it to GOP.
	UPDATE t504_consignment
	   SET c504_last_updated_by   = p_user
	     , c504_last_updated_date = CURRENT_DATE
	 WHERE c504_consignment_id = p_out_new_consign_id;	
	 
    -- 5. current MR set to ready to ship if any items moved - for release not for void
    /*UPDATE t520_request
    SET c520_status_fl = '30'
    WHERE c520_request_id = p_bo_request_id;*/
END gm_sav_reconfig_request_cons;
----
/*******************************************************
* Description : Procedure  material request reconfigure
* Author  : Rakhi Gandhi
*******************************************************/
PROCEDURE gm_sav_remove_from_request (
        p_bo_request_id IN t520_request.c520_request_id%TYPE,
        p_part_num      IN t205_part_number.c205_part_number_id%TYPE,
        p_qty           IN NUMBER,
        p_action        IN VARCHAR2,
        p_out_remaining_qty OUT VARCHAR2)
AS
    v_c521_request_detail_id t521_request_detail.c521_request_detail_id%TYPE;
    v_qty NUMBER ;
BEGIN
     SELECT c521_request_detail_id, c521_qty
       INTO v_c521_request_detail_id, v_qty
       FROM t521_request_detail t521
      WHERE c520_request_id          = p_bo_request_id
        AND t521.c205_part_number_id = p_part_num;
    p_out_remaining_qty             := v_qty - p_qty; --todo test for 1
    --even if remaining qty, delete the row and do not update it ,as it will be put in new MR.
    IF NVL(v_qty,0) > NVL(p_qty,0)  THEN
         UPDATE t521_request_detail t521
        SET t521.c521_qty              = v_qty - p_qty
          WHERE c521_request_detail_id = v_c521_request_detail_id;

		--Whenever the detail is updated, the request table has to be updated ,so ETL can Sync it to GOP.
        UPDATE t520_request
           SET c520_last_updated_date = CURRENT_DATE
         WHERE C520_request_Id = p_bo_request_id;           
    END IF;
    IF NVL(v_qty,0) = NVL(p_qty,0) THEN
         DELETE
           FROM t521_request_detail t521
          WHERE c521_request_detail_id = v_c521_request_detail_id;

		--Whenever the detail is updated, the request table has to be updated ,so ETL can Sync it to GOP.
        UPDATE t520_request
           SET c520_last_updated_date = CURRENT_DATE
         WHERE C520_request_Id = p_bo_request_id;           
    END IF;
    --
    --throw error if >
    --
    -- Return  remaining quantity ; done
END gm_sav_remove_from_request;
--
/*******************************************************
* Description : Procedure  material request reconfigure
* Procedure to Create New CN
* Author   : Rakhi Gandhi
*******************************************************/
PROCEDURE gm_sav_create_new_consign (
        p_original_consign_id IN t504_consignment.c504_consignment_id%TYPE,
        p_original_request_id IN t520_request.c520_request_id%TYPE -- mr user enters
        ,
        p_original_bo_request_id IN t520_request.c520_request_id%TYPE -- backorder mr for user entered mr
        ,
        p_out_consign_id OUT t504_consignment.c504_consignment_id%TYPE,
        p_user IN VARCHAR2,
        p_action IN VARCHAR2 DEFAULT NULL)
AS
    v_distributor_id t504_consignment.c701_distributor_id%TYPE;
    v_ship_date t504_consignment.c504_ship_date%TYPE;
    v_return_date t504_consignment.c504_return_date%TYPE;
    v_comments t504_consignment.c504_comments%TYPE;
    v_total_cost t504_consignment.c504_total_cost%TYPE;
    v_status_fl t504_consignment.c504_status_fl%TYPE;
    v_void_fl t504_consignment.c504_void_fl%TYPE;
    v_account_id t504_consignment.c704_account_id%TYPE;
    v_ship_to t504_consignment.c504_ship_to%TYPE;
    v_set_id t504_consignment.c207_set_id%TYPE;
    v_ship_to_id t504_consignment.c504_ship_to_id%TYPE;
    v_final_comments t504_consignment.c504_final_comments%TYPE;
    v_inhouse_purpose t504_consignment.c504_inhouse_purpose%TYPE;
    v_type t504_consignment.c504_type%TYPE;
    v_delivery_carrier t504_consignment.c504_delivery_carrier%TYPE;
    v_delivery_mode t504_consignment.c504_delivery_mode%TYPE;
    v_tracking_number t504_consignment.c504_tracking_number%TYPE;
    v_ship_req_fl t504_consignment.c504_ship_req_fl%TYPE;
    v_verify_fl t504_consignment.c504_verify_fl%TYPE;
    v_verified_date t504_consignment.c504_verified_date%TYPE;
    v_verified_by t504_consignment.c504_verified_by%TYPE;
    v_update_inv_fl t504_consignment.c504_update_inv_fl%TYPE;
    v_reprocess_id t504_consignment.c504_reprocess_id%TYPE;
    v_master_consignment_id t504_consignment.c504_master_consignment_id%TYPE;
    v_request_id t504_consignment.c520_request_id%TYPE;
    --
    v_request_for t520_request.c520_request_for%TYPE;
    v_request_to t520_request.c520_request_to%TYPE;
    v_request_by_type t520_request.c901_request_by_type%TYPE;
    v_request_by t520_request.c520_request_by%TYPE;
    v504_type t504_consignment.c504_type%TYPE;
    v_shipto_id t520_request.c520_ship_to_id%TYPE;
    v_user t504_consignment.c504_created_by%TYPE;
    v_status_flag t504_consignment.c504_status_fl%TYPE;
    v_verify_flag t504_consignment.c504_verify_fl%TYPE;
    v_purpose t520_request.c901_purpose%TYPE;
    v_div_type   VARCHAR2 (50);
    v_out_consign_id t504_consignment.c504_consignment_id%TYPE;
    v_count NUMBER;
    v_out_distributor_req_id t520_request.c520_request_to%TYPE;
    v_out_bo_ship_to t520_request.c901_ship_to%TYPE;
    v_out_bo_shipto_id t520_request.c520_ship_to_id%TYPE;
    -- reverting the changes of BUG-4161 for PMT-7461
BEGIN
    -- TODO: handle null situation for input consignid - when creating CN for the first time.
    --for first time consignment id creation - get values from MR
    IF p_original_consign_id IS NULL THEN
         SELECT c520_request_to, c520_request_for, c901_ship_to
          , c520_ship_to_id, c207_set_id, c520_request_by
          , c901_request_by_type, c520_created_by, c901_purpose
           INTO v_request_to, v_request_for, v_ship_to
          , v_ship_to_id, v_set_id, v_request_by
          , v_request_by_type, v_user, v_purpose
           FROM t520_request
          WHERE c520_request_id = p_original_request_id
            AND c520_void_fl   IS NULL;
            
         SELECT DECODE (v_request_for, 40021, 4110, 40022, 4112, 4110) -- we are not using the value 40021/4110 (
            -- Consignment) of DMDTP. 20021 - Sales. 40022 - InHouse
          , DECODE (v_request_for, 40022, '01', NULL), DECODE (v_request_for, 40021, v_ship_to_id, 40022, v_request_by,
            v_ship_to_id)
           INTO v504_type, v_account_id, v_shipto_id
           FROM DUAL;
           
         SELECT DECODE (v_set_id, NULL, '2', '0') -- Status
          , DECODE (v_set_id, NULL, NULL, '0') -- Verified flag
           INTO v_status_flag, v_verify_flag
           FROM DUAL;
			
			-- PMT-20074 Stock Transfer fullfill , change type using action
			IF  p_action = '106703' OR p_action = '106704'
			THEN
				v504_type := p_action;
			END IF;
			
           	IF v_request_for = '40025' -- Account Consignment
			THEN
				gm_pkg_op_account_req_master.gm_sav_consignment (v_request_to, NULL, NULL, NULL, NULL, v_status_flag, NULL,
			        v_request_to, v_ship_to, v_set_id, v_shipto_id, NULL, v_purpose --NULL
			        , v504_type,NULL , NULL, NULL, '1', v_verify_flag, NULL, NULL, NULL, NULL, NULL, p_original_bo_request_id --
			        -- p_out_request_id
			        , v_user, p_out_consign_id) ;
			ELSE
		        gm_pkg_op_request_master.gm_sav_consignment (v_request_to, NULL, NULL, NULL, NULL, v_status_flag, NULL,
		        v_account_id, v_ship_to, v_set_id, v_shipto_id, NULL, v_purpose --NULL
		        , v504_type,NULL , NULL, NULL, '1', v_verify_flag, NULL, NULL, NULL, NULL, NULL, p_original_bo_request_id --
		        -- p_out_request_id
		        , v_user, p_out_consign_id) ;
			END IF;
    END IF;
    -- TODO: need to validate below code
    IF p_original_consign_id IS NOT NULL THEN
         SELECT DECODE (c504_type, 40057, 4110, c504_type), c504_ship_to, c704_account_id
          , c701_distributor_id, c504_ship_to_id, c504_ship_date
          , c504_inhouse_purpose
            /*, c504_created_date*/
           INTO v_type, v_ship_to, v_account_id
          , v_distributor_id, v_ship_to_id, v_ship_date
          , v_inhouse_purpose
           FROM t504_consignment t504
          WHERE t504.c504_consignment_id = p_original_consign_id
            AND c504_void_fl            IS NULL;
        
            --get the distributor id from the back order request id(PMT-35132).
       			gm_fch_bo_ship_dtl(p_original_bo_request_id,v_out_distributor_req_id,v_out_bo_ship_to,v_out_bo_shipto_id);
       	   		  
		     IF v_out_distributor_req_id IS NOT NULL THEN
		    		v_distributor_id := v_out_distributor_req_id;
		    		v_ship_to := v_out_bo_ship_to;
					v_ship_to_id := v_out_bo_shipto_id;
		     END IF;
		  
		        gm_pkg_op_request_master.gm_sav_consignment (v_distributor_id, NULL, NULL, NULL, NULL, 2, NULL, v_account_id,
		        v_ship_to, NULL, v_ship_to_id, NULL, v_inhouse_purpose, v_type, NULL, NULL, NULL, '1', 1, NULL, NULL, NULL,
		        NULL, p_original_consign_id, p_original_bo_request_id, p_user, p_out_consign_id) ;
		      
    END IF;
     UPDATE t504_consignment
    	SET c504_verify_fl          = NULL
    	  , c504_last_updated_by = p_user
		  , c504_last_updated_date = CURRENT_DATE
      WHERE c504_consignment_id = p_out_consign_id
        AND c207_set_id        IS NULL
        AND c504_void_fl       IS NULL;
  /*  v_div_type := NVL(get_distributor_division(v_request_to),'100823'); -- Fetch the US/OUS Distributor
         IF v_div_type = '100824' AND v_purpose = '4000097' -- OUS Distributor, 4000097:OUS Sales Replenishment
					THEN
						v_out_consign_id := p_out_consign_id;
						-- To fetch consignment id based on OUS distribute like 'UK-CN-XXXX'
						gm_pkg_op_request_master.gm_pkg_fetch_ous_trans_id(p_out_consign_id,v_request_to,p_user);
						
						select count(1) INTO v_count  from t505_item_consignment where c504_consignment_id = p_out_consign_id and c505_void_fl is null;
						IF (v_count > 0) THEN
						  	UPDATE t505_item_consignment
	         					SET c504_consignment_id = p_out_consign_id
    	               		WHERE c504_consignment_id = v_out_consign_id;
         				END IF;
						UPDATE t504_consignment SET c504_consignment_id = p_out_consign_id,
							C504_LAST_UPDATED_DATE = CURRENT_DATE,C504_LAST_UPDATED_BY = p_user
						WHERE c504_consignment_id = v_out_consign_id;
		END IF;*/
END gm_sav_create_new_consign;
/*******************************************************
* Description : Procedure  material request reconfigure
*  Procedure to create new MR
* Author  : Rakhi Gandhi
*******************************************************/
PROCEDURE gm_sav_create_new_request (
        p_original_request_id IN t520_request.c520_request_id%TYPE,
        p_out_request_id OUT t520_request.c520_request_id%TYPE,
        p_user IN VARCHAR2)
AS
    v_request_source t520_request.c901_request_source%TYPE;
    v_request_txn_id t520_request.c520_request_txn_id%TYPE;
    v_set_id t520_request.c207_set_id%TYPE;
    v_request_for t520_request.c520_request_for%TYPE;
    v_request_to t520_request.c520_request_to%TYPE;
    v_request_by_type t520_request.c901_request_by_type%TYPE;
    v_request_by t520_request.c520_request_by%TYPE;
    v_ship_to t520_request.c901_ship_to%TYPE;
    v_ship_to_id t520_request.c520_ship_to_id%TYPE;
    v_created_date t520_request.c520_created_date%TYPE;
    v_master_req_id t520_request.c520_request_id%TYPE;
    v_required_date t520_request.c520_required_date%TYPE;
    v_prod_req_id  t520_request.c525_product_request_id%TYPE;
BEGIN
     SELECT t520_request.c901_request_source, t520_request.c207_set_id, t520_request.c520_request_for
      , t520_request.c520_request_to, t520_request.c901_request_by_type, t520_request.c520_request_by
      , t520_request.c901_ship_to, t520_request.c520_ship_to_id, c520_created_date
      , NVL (c520_master_request_id, c520_request_id), t520_request.c520_required_date
      , t520_request.c525_product_request_id 
       INTO v_request_source, v_set_id, v_request_for
      , v_request_to, v_request_by_type, v_request_by
      , v_ship_to, v_ship_to_id, v_created_date
      , v_master_req_id, v_required_date,v_prod_req_id
       FROM t520_request
      WHERE c520_request_id = p_original_request_id
        AND c520_void_fl   IS NULL;
    gm_pkg_op_request_master.gm_sav_request (NULL, NVL (v_required_date, TRUNC (CURRENT_DATE)), v_request_source,
    p_original_request_id, v_set_id, v_request_for, v_request_to, v_request_by_type, v_request_by, v_ship_to,
    v_ship_to_id, v_master_req_id, '10', p_user, NULL, p_out_request_id, NULL, v_prod_req_id) ;                  -- v_prod_req_id Used to PBUG-4036 - PMT-45058 : Ability to link Loaner Request to Item consignment transaction        
    gm_pkg_op_process_request.gm_sav_cpy_req_shipping (p_original_request_id, p_out_request_id, 50184, p_user) ;
END gm_sav_create_new_request;
/*******************************************************
* Description : Procedure  material request reconfigure
* Author  : Rakhi Gandhi
*******************************************************/
PROCEDURE gm_sav_add_part_to_reqdetail (
        p_bo_request_id IN t520_request.c520_request_id%TYPE,
        p_part_num      IN t205_part_number.c205_part_number_id%TYPE,
        p_qty           IN VARCHAR2)
AS
    v_out_request_detail_id t521_request_detail.c521_request_detail_id%TYPE;
BEGIN
    gm_pkg_op_request_master.gm_sav_request_detail (p_bo_request_id, p_part_num, p_qty, v_out_request_detail_id) ;
END gm_sav_add_part_to_reqdetail;
/*******************************************************
* Description : Procedure  material request reconfigure
* Author  : Rakhi Gandhi
* function to get status of consignment - Built set 1, WIP 2 , itemconsignment 3 - WIP(status 2) ,
* 4 item consignment - completed (Status 3)
* If user enters child MR, this is not the status of child CN but the master of child CN
*******************************************************/
FUNCTION get_consignment_status (
        p_user_request_id IN t520_request.c520_request_id%TYPE)
    RETURN VARCHAR2
IS
    v_status VARCHAR2 (10) ;
    v_consign_id_master t504_consignment.c504_consignment_id%TYPE;
    v_consign_id_for_status t504_consignment.c504_consignment_id%TYPE;
    v_set_id t504_consignment.c207_set_id%TYPE;
    v_status_fl VARCHAR2 (100) ;
    v_verify_fl VARCHAR2 (100) ;
    v_consign_id t504_consignment.c504_consignment_id%TYPE;
BEGIN
    v_status := '0';
    --could do outer join with request but is inefficient until necessary
    BEGIN
         SELECT t504.c207_set_id, c504_master_consignment_id, c504_status_fl
          , c504_verify_fl, c504_consignment_id
           INTO v_set_id, v_consign_id_master, v_status_fl
          , v_verify_fl, v_consign_id
           FROM t504_consignment t504
          WHERE t504.c520_request_id = p_user_request_id
            AND t504.c504_void_fl   IS NULL;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_consign_id := NULL; -- ( for a consignment where no CN created yet since nothing was available on initiation
        -- of set )
    END;
    IF v_consign_id IS NULL THEN -- for initiataed set with CN not present yet
         SELECT c207_set_id
           INTO v_set_id
           FROM t520_request
          WHERE c520_request_id = p_user_request_id;
        IF v_set_id            IS NULL THEN
            v_status           := '3'; --wip item consignment
        ELSE
            v_status := '2'; --wip set
        END IF;
        RETURN v_status;
    END IF;
    IF v_set_id IS NULL --item consignment case
        THEN
        IF v_status_fl = 2 THEN
            v_status  := '3'; -- wip item consignment
        ELSE
            IF v_status_fl = 3 OR v_status_fl = 4 THEN -- completed item consignment
                v_status  := '4'; -- completed item consignment
            END IF;
        END IF;
        --END IF;
    ELSE -- WIP or Built sets master or child consignments
        --IF v_set_id IS NOT NULL
        --THEN
        IF v_consign_id_master IS NOT NULL THEN
             SELECT c504_status_fl, c504_verify_fl
               INTO v_status_fl, v_verify_fl
               FROM t504_consignment t504
              WHERE t504.c504_consignment_id = v_consign_id_master;
        END IF;
        IF (v_status_fl = 0 OR v_status_fl = 1 OR v_status_fl = 2) -- WIP Set
            THEN
            v_status       := '2'; --
            IF (v_verify_fl = 1 AND v_status_fl = 2) THEN
                v_status   := '1'; -- built
            END IF;
          -- For Set, when try to reconfigure Parent request (which has status fl =4) handle for completed status 
          ELSE 
            IF v_status_fl = 4 THEN 
              v_status := '4'; -- completed
            END IF;
        END IF;
    END IF;

    RETURN v_status;
END get_consignment_status;
--
/******************************************************
Function to get available quantity
Author: Rakhi Gandhi
******************************************************/
FUNCTION get_available_qty_for_cn (
        p_request_id IN t520_request.c520_request_id%TYPE --from cn table
        --    , p_consign_id  IN t504_consignment.c504_consignment_id%TYPE   --from cn table
        ,
        p_part_num IN t205_part_number.c205_part_number_id%TYPE)
    RETURN NUMBER
IS
    v_avail_qty NUMBER:=0;
BEGIN
     SELECT DECODE (gm_pkg_op_request_summary.get_consignment_status (p_request_id), '1', get_qty_in_stock (p_part_num)
        , --built set
        '2', get_qty_in_bulk (p_part_num), -- wip
        '3', get_qty_in_stock (p_part_num), --item consignment 3
        '4', get_qty_in_stock (p_part_num)) available --item consignmet 4
       INTO v_avail_qty
       FROM DUAL;
    RETURN v_avail_qty;
END get_available_qty_for_cn;
--
FUNCTION get_total_part_qty_in_cons (
        p_consign_id t504_consignment.c504_consignment_id%TYPE,
        p_part_num t205_part_number.c205_part_number_id%TYPE)
    RETURN NUMBER
IS
    /* Parameters: set consignment id , partid- return total quantity for the part
    */
    v_pnm t205_part_number.c205_part_number_id%TYPE;
    v_item_cons_qty NUMBER;
    v_total_qty     NUMBER;
BEGIN
    BEGIN
         SELECT c205_part_number_id, SUM (c505_item_qty)
           INTO v_pnm, v_item_cons_qty
           FROM t505_item_consignment t505
          WHERE c504_consignment_id = p_consign_id
            AND c205_part_number_id = p_part_num
       GROUP BY t505.c205_part_number_id;
         SELECT v_item_cons_qty + get_part_backorder_qty (p_consign_id, p_part_num)
           INTO v_total_qty
           FROM DUAL;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN 0;
    END;
    RETURN v_total_qty;
END get_total_part_qty_in_cons;
--
FUNCTION get_part_backorder_qty (
        p_consign_id t504_consignment.c504_consignment_id%TYPE,
        p_part_num t205_part_number.c205_part_number_id%TYPE)
    RETURN NUMBER
IS
    /* Parameters: set consignment id , partid- return backorder quantity for the part
    */
    v_request_id t520_request.c520_request_id%TYPE;
    v_bo_request_id t520_request.c520_request_id%TYPE;
    v_part_qty NUMBER;
BEGIN
    BEGIN
        -- Get MR from consignment table - Master MR ID
         SELECT c520_request_id
           INTO v_request_id
           FROM t504_consignment
          WHERE c504_consignment_id = p_consign_id -- FOR UPDATE;
            AND c504_void_fl       IS NULL;
        -- Check if parts are in backorder if so get the backorder entry
         SELECT c520_request_id
           INTO v_bo_request_id
           FROM t520_request t520
          WHERE (c520_master_request_id = v_request_id
            OR c520_request_id          = v_request_id)
            AND t520.c520_status_fl     = '10' -- Backorder 10
            AND t520.c520_void_fl      IS NULL;
        --get bo quantity for the part
         SELECT t521.c521_qty partboqty
           INTO v_part_qty
           FROM t521_request_detail t521
          WHERE t521.c520_request_id     = v_bo_request_id
            AND t521.c205_part_number_id = p_part_num;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN 0;
    END;
    RETURN v_part_qty;
END get_part_backorder_qty;
--
/*******************************************************
* Description : Procedure to get backorder report
* Author    : Rakhi Gandhi
*******************************************************/
PROCEDURE gm_fch_backorder_report (
        p_request_id IN t520_request.c520_request_id%TYPE,
        p_outdetail OUT TYPES.cursor_type)
AS
BEGIN
    -- Check if parts are in backorder if so get the backorder entry
    OPEN p_outdetail FOR SELECT t205.c205_part_number_id pnum,
    t205.c205_part_num_desc pdesc,
    t521.c521_qty boqty,
    t521.c520_request_id boreqid FROM t521_request_detail t521,
    t205_part_number t205 WHERE t521.c205_part_number_id = t205.c205_part_number_id AND t521.c520_request_id IN
    (
         SELECT c520_request_id
           FROM t520_request t520
          WHERE (c520_master_request_id = p_request_id
            OR c520_request_id          = p_request_id)
            AND t520.c520_status_fl     = '10'
            AND t520.c520_void_fl      IS NULL
    )
    ORDER BY t205.c205_part_number_id;
END gm_fch_backorder_report;
/*******************************************************
* Description : Procedure to fetch consignment ID from MR id
* Author    : Joe P Kumar
*******************************************************/
/*******************************************************
* Description : Procedure to fetch consignment ID from MR id
* Author     : Joe P Kumar
*******************************************************/
PROCEDURE gm_fch_csgid_from_mrid (
        p_request_id IN t520_request.c520_request_id%TYPE,
        p_out_consignid OUT t504_consignment.c504_consignment_id%TYPE)
AS
BEGIN
     SELECT c504_consignment_id
       INTO p_out_consignid
       FROM t504_consignment t504
      WHERE t504.c520_request_id = p_request_id
        AND t504.c504_void_fl   IS NULL;
EXCEPTION
WHEN NO_DATA_FOUND THEN
    p_out_consignid := '';
END gm_fch_csgid_from_mrid;
--
/*******************************************************
* Description : Procedure to fetch MR id
* Author    : Rakhi Gandhi
*******************************************************/
PROCEDURE gm_fch_mrid_from_csnid (
        p_consignid IN t504_consignment.c504_consignment_id%TYPE,
        p_out_request_id OUT t520_request.c520_request_id%TYPE)
AS
BEGIN
     SELECT c520_request_id mrid
       INTO p_out_request_id
       FROM t504_consignment
      WHERE c504_consignment_id = p_consignid
        AND c504_void_fl       IS NULL;
END gm_fch_mrid_from_csnid;
--
/*******************************************************
* Description : Procedure to fetch Details of Child Request
* Author    : Xun Qu
*******************************************************/
PROCEDURE gm_fch_child_request (
        p_request_id IN t520_request.c520_request_id%TYPE,
        p_outdetail OUT TYPES.cursor_type)
AS
BEGIN
    OPEN p_outdetail FOR SELECT t505.c205_part_number_id pnum,
    get_partnum_desc (t505.c205_part_number_id) pdesc,
    t505.c505_item_qty crqty,
    t505.c505_control_number controlnum,
    gm_pkg_op_request_summary.get_request_status (t520.c520_status_fl) status_fl,
    t504.c504_consignment_id cid,
    t520.c520_request_id reqid FROM t520_request t520,
    t504_consignment t504,
    t505_item_consignment t505 WHERE t520.c520_master_request_id = p_request_id AND t504.c520_request_id =
    t520.c520_request_id AND t505.c504_consignment_id            = t504.c504_consignment_id AND t520.c520_void_fl IS
    NULL
  UNION ALL
     SELECT t521.c205_part_number_id pnum, get_partnum_desc (t521.c205_part_number_id) pdesc, t521.c521_qty
      , ' ' controlnum, 'Open' status_fl, ' ' cid
      , t520.c520_request_id reqid
       FROM t520_request t520, t521_request_detail t521
      WHERE t520.c520_master_request_id = p_request_id
        AND t520.c520_request_id        = t521.c520_request_id
        AND t520.c520_void_fl          IS NULL ORDER BY pnum;
END gm_fch_child_request;
--
/*******************************************************
* Purpose: function is used to get the consignment
critical count.
author lakshmi
*******************************************************/
FUNCTION get_consg_crit_cnt (
        p_consign_id t504_consignment.c504_consignment_id%TYPE)
    RETURN NUMBER
IS
    v_crit_cnt NUMBER;
BEGIN
     SELECT COUNT (1)
       INTO v_crit_cnt
       FROM t504_consignment t504, t505_item_consignment t505, t208_set_details t208
      WHERE t504.c504_consignment_id  = p_consign_id
        AND t504.c504_consignment_id  = t505.c504_consignment_id
        AND t504.c207_set_id          = t208.c207_set_id
        AND t505.c205_part_number_id  = t208.c205_part_number_id
        AND t208.c208_critical_fl     = 'Y'
        AND t505.c505_control_number <> 'TBE';
    RETURN v_crit_cnt;
    --
EXCEPTION
WHEN NO_DATA_FOUND THEN
    RETURN 0;
    --
END get_consg_crit_cnt;
--
/*******************************************************
* Purpose: function is used to get the consignment
complete count.
author lakshmi
*******************************************************/
FUNCTION get_consg_comp_cnt (
        p_consign_id t504_consignment.c504_consignment_id%TYPE)
    RETURN NUMBER
IS
    v_con_compl_cnt NUMBER;
BEGIN
     SELECT COUNT (1)
       INTO v_con_compl_cnt
       FROM t505_item_consignment t505
      WHERE t505.c504_consignment_id  = p_consign_id
        AND t505.c505_control_number <> 'TBE';
    RETURN v_con_compl_cnt;
EXCEPTION
WHEN NO_DATA_FOUND THEN
    RETURN 0;
END get_consg_comp_cnt;
--

/*******************************************************
* Description : Procedure to fetch Details of BBA Child Request
* Author    : HReddi
*******************************************************/
PROCEDURE gm_fch_BBA_child_request (
        p_request_id IN t520_request.c520_request_id%TYPE,
        p_outdetail OUT TYPES.cursor_type)
AS
BEGIN
  OPEN p_outdetail FOR      
	SELECT t505.c205_part_number_id pnum,
    get_partnum_desc (t505.c205_part_number_id) pdesc,
    t505.c505_item_qty crqty,
    t505.c505_control_number ALLOGRAFT,
	t505.c505_control_number CONTROLNUM,
    v205.lblsize PRODSIZE,
    gm_pkg_op_request_summary.get_request_status (t520.c520_status_fl) status_fl,
    t504.c504_consignment_id cid,
    t520.c520_request_id reqid 
	FROM t520_request t520,
    t504_consignment t504,
    t505_item_consignment t505 , v205_lbl_param_for_printing v205
    WHERE t520.c520_master_request_id = p_request_id
    AND t504.c520_request_id =  t520.c520_request_id 
    AND t505.c504_consignment_id            = t504.c504_consignment_id 
    AND t505.c205_part_number_id = v205.productno(+)
    AND t505.c505_control_number = v205.allograftcode(+)
    AND t520.c520_void_fl IS  NULL
  UNION ALL
  SELECT t521.c205_part_number_id pnum
	   , get_partnum_desc (t521.c205_part_number_id) pdesc
	   , t521.c521_qty
	   , ' ' ALLOGRAFT
       , ' ' CONTROLNUM
	   , '' PRODSIZE
	   , 'Open' status_fl
	   , ' ' cid
       , t520.c520_request_id reqid
   FROM t520_request t520
      , t521_request_detail t521
  WHERE t520.c520_master_request_id = p_request_id
    AND t520.c520_request_id = t521.c520_request_id
    AND t520.c520_void_fl IS NULL ORDER BY pnum;    
END gm_fch_BBA_child_request;
--
/****************************************************************
* Description : Procedure to remove line item from t521
*               and add log for modified part quantity on t521a
* Author    : Dinesh Rajavel
*****************************************************************/  
PROCEDURE gm_rmv_items_from_ST_req (
        p_request_id    IN t520_request.c520_request_id%TYPE,
        p_part_num      IN t205_part_number.c205_part_number_id%TYPE,
        p_qty           IN NUMBER,
        p_user		    IN VARCHAR2)
AS
    v_c521_request_detail_id t521_request_detail.c521_request_detail_id%TYPE;
    v_qty NUMBER ;
BEGIN
     SELECT c521_request_detail_id, c521_qty
       INTO v_c521_request_detail_id, v_qty
       FROM t521_request_detail t521
      WHERE c520_request_id          = p_request_id
        AND t521.c205_part_number_id = p_part_num;
        
    	-- If processing qty is less than requested qty then store the actual qty in log table.
    	-- Used when voiding consignment and rollback the request back to requested
   		IF NVL(v_qty,0) > NVL(p_qty,0)  THEN
    		gm_pkg_op_request_master.gm_sav_request_log_detail(p_request_id,p_part_num,v_qty,p_user);
    	END IF;
    	
    	DELETE FROM t521_request_detail t521
		    WHERE c521_request_detail_id = v_c521_request_detail_id;
		    
END gm_rmv_items_from_ST_req;

/*******************************************************************
* Description : Procedure to create child request for stock transfer
*               from master request   
* Author    : Dinesh Rajavel
*****************************************************************/ 
PROCEDURE gm_create_child_ST_request (
        p_master_request_id IN t520_request.c520_request_id%TYPE,
        p_out_request_id OUT t520_request.c520_request_id%TYPE,
        p_user IN VARCHAR2)
AS
    v_request_source t520_request.c901_request_source%TYPE;
    v_request_txn_id t520_request.c520_request_txn_id%TYPE;
    v_set_id t520_request.c207_set_id%TYPE;
    v_request_for t520_request.c520_request_for%TYPE;
    v_request_to t520_request.c520_request_to%TYPE;
    v_request_by_type t520_request.c901_request_by_type%TYPE;
    v_request_by t520_request.c520_request_by%TYPE;
    v_ship_to t520_request.c901_ship_to%TYPE;
    v_ship_to_id t520_request.c520_ship_to_id%TYPE;
    v_created_date t520_request.c520_created_date%TYPE;
    v_master_req_id t520_request.c520_request_id%TYPE;
    v_required_date t520_request.c520_required_date%TYPE;
    v_status t520_request.C520_Status_Fl%TYPE;
    v_company_id t520_request.C1900_Company_Id%TYPE;
    v_plant_id t520_request.C5040_Plant_Id%TYPE;--26240435
    v_ship_source t907_shipping_info.c901_source%TYPE;
    v_fullfill_company T5201_Requested_Company.C1900_Company_Id%TYPE;
    v_request_dt T5201_Requested_Company.C5201_Requested_Date%TYPE;
    v_bo_exists NUMBER := 0;
BEGIN
     SELECT t520_request.c901_request_source, t520_request.c207_set_id, t520_request.c520_request_for
      , t520_request.c520_request_to, t520_request.c901_request_by_type, t520_request.c520_request_by
      , t520_request.c901_ship_to, t520_request.c520_ship_to_id, c520_created_date
      , NVL (c520_master_request_id, c520_request_id), t520_request.c520_required_date
      , t520_request.C1900_Company_Id,t520_request.C5040_Plant_Id
       INTO v_request_source, v_set_id, v_request_for
      , v_request_to, v_request_by_type, v_request_by
      , v_ship_to, v_ship_to_id, v_created_date
      , v_master_req_id, v_required_date
      , v_company_id, v_plant_id
       FROM t520_request
      WHERE c520_request_id = p_master_request_id
        AND c520_void_fl   IS NULL;
    
	BEGIN
	SELECT C1900_Company_Id,C5201_Requested_Date
	 INTO v_fullfill_company,v_request_dt
		FROM T5201_Requested_Company
		WHERE C520_Request_Id = p_master_request_id
		AND C5201_Void_Fl    IS NULL;
	EXCEPTION
	WHEN NO_DATA_FOUND THEN
		v_fullfill_company := NULL;
		v_request_dt := NULL;
	END;
        
	-- As the fullfill company for stock transfer creating the child request, need to create the
	-- child request from master request company
    gm_pkg_cor_client_context.gm_sav_client_context(v_company_id,v_plant_id);
    
    --create child request
    gm_pkg_op_request_master.gm_sav_request (NULL, NVL (v_required_date, TRUNC (CURRENT_DATE)), v_request_source,
    p_master_request_id, v_set_id, v_request_for, v_request_to, v_request_by_type, v_request_by, v_ship_to,
    v_ship_to_id, v_master_req_id, '0', p_user, NULL, p_out_request_id) ;
    
   --create copy ship details
    gm_pkg_op_process_request.gm_sav_cpy_req_shipping (p_master_request_id, p_out_request_id, '26240435', p_user) ;
   
    --create request fullfill details
    gm_pkg_op_request_fulfill.gm_sav_request_fulfill_company(p_out_request_id,v_fullfill_company,v_request_dt,p_user);
    
    --Update master request ID
    UPDATE t521_request_detail
       SET c520_request_id     = p_out_request_id
     WHERE c520_request_id = p_master_request_id;     


END gm_create_child_ST_request;
/*******************************************************************
* Description : Procedure to fetch distributor id, ship to and ship to id from bo
* Author      : Agilan Singaravel
*****************************************************************/ 
PROCEDURE gm_fch_bo_ship_dtl (
       p_original_bo_request_id 	IN 		t520_request.c520_request_id%TYPE,
       p_distributor_req_id     	OUT  	t520_request.c520_request_to%TYPE,
       p_bo_ship_to 				OUT		t520_request.c901_ship_to%TYPE,
       p_bo_shipto_id 				OUT		t520_request.c520_ship_to_id%TYPE
)
AS
BEGIN
    --get the distributor id from the back order request id(PMT-35132).
          BEGIN
			 SELECT t520.c520_request_to, t520.c901_ship_to, t520.c520_ship_to_id  
			   INTO p_distributor_req_id, p_bo_ship_to, p_bo_shipto_id
               FROM t520_request t520, t701_distributor t701
              WHERE t520.c520_request_to = t701.c701_distributor_id
                AND t520.c520_request_id = p_original_bo_request_id
                AND t520.C520_VOID_FL IS NULL 
                AND t701.C701_VOID_FL IS NULL;
		  EXCEPTION
   		  WHEN NO_DATA_FOUND THEN
        		p_distributor_req_id := NULL;
        		p_bo_ship_to := NULL;
        		p_bo_shipto_id := NULL;
   		  END;  
    		  	     
END gm_fch_bo_ship_dtl;
END gm_pkg_op_request_summary;
/
