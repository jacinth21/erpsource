/* Formatted on 11/30/2009 13:00 (Formatter Plus v4.8.0) */
--@"c:\database\packages\operations\gm_pkg_op_purchasing.bdy"

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_purchasing
IS
--
 /******************************************************************
   * Description : This Procedure returns the work order details for first article
  ****************************************************************/
	PROCEDURE gm_fch_far_wo (
		p_po		IN		 t402_work_order.c401_purchase_ord_id%TYPE
	  , p_req_cur	OUT 	 TYPES.cursor_type
	)
	AS
	    v_dateformat VARCHAR2(100);
	BEGIN
			   
        SELECT NVL(get_compdtfmt_frm_cntx(),GET_RULE_VALUE('DATEFMT','DATEFORMAT')) 
          INTO v_dateformat 
          FROM dual;
		
	     OPEN p_req_cur
		 FOR
			 SELECT c401_purchase_ord_id poid, c402_work_order_id woid, c205_part_number_id pnum
				  , get_partnum_desc (c205_part_number_id) pdesc
				  , get_project_name (get_project_id_from_part (c205_part_number_id)) prjname
				  , get_vendor_name (c301_vendor_id) vname, get_user_name (c402_created_by) crby
				  ,TO_CHAR (c402_created_date,  v_dateformat||'hh24:mi:ss') crdt
			   FROM t402_work_order
			  WHERE c401_purchase_ord_id = p_po AND c402_far_fl = 'Y' AND c402_void_fl IS NULL;
	END gm_fch_far_wo;
	
	/*************************************************************************************************
     * Purpose: The Below Procedure is used to Validate the Product Request ID Based on the Company ID
       Author : Mrajasekaran
     ************************************************************************************************/
	PROCEDURE gm_validate_po(
		p_poid    IN  T401_PURCHASE_ORDER.C401_PURCHASE_ORD_ID%TYPE
		
	)
	AS
	v_company_id    T1900_COMPANY.C1900_COMPANY_ID%TYPE;
	v_company_name  T1900_COMPANY.C1900_COMPANY_NAME%TYPE;
	v_po_company_id  T1900_COMPANY.C1900_COMPANY_ID%TYPE;
	BEGIN
		
		SELECT get_compid_frm_cntx() 
		  INTO v_company_id 
		  FROM dual;	 
		  
		BEGIN
    		SELECT C1900_COMPANY_ID
    			INTO v_po_company_id
    			FROM T401_PURCHASE_ORDER 
   				WHERE C401_PURCHASE_ORD_ID = p_poid
	    		AND C401_VOID_FL is null;
	    	EXCEPTION
 				WHEN NO_DATA_FOUND
    			THEN
      				v_po_company_id := NULL;
      			END;
      			
      	IF v_po_company_id is NOT NULL THEN
      		v_company_name := get_company_name(v_po_company_id);
      		IF (v_company_id != v_po_company_id) THEN
      			GM_RAISE_APPLICATION_ERROR('-20999','273',p_poid||'#$#'||v_company_name);
      		END IF;
      	END IF;

	END	gm_validate_po;
	
	/*************************************************************************************************
     * Purpose: The Below Procedure is used to Validate the PO based on the plant
	************************************************************************************************/
	
		PROCEDURE gm_validate_po_plant(
		p_poid    IN  T401_PURCHASE_ORDER.C401_PURCHASE_ORD_ID%TYPE
	)
	AS
		v_company_id    	T1900_COMPANY.C1900_COMPANY_ID%TYPE;
		v_po_company_id  	T1900_COMPANY.C1900_COMPANY_ID%TYPE;
		v_plant_id			T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
		v_po_plant_id		T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
		v_plant_nm			T5040_PLANT_MASTER.C5040_PLANT_NAME%TYPE;
		v_po_plant_nm		T5040_PLANT_MASTER.C5040_PLANT_NAME%TYPE;
	BEGIN
		
		SELECT get_compid_frm_cntx(), get_plantid_frm_cntx()
		  INTO v_company_id, v_plant_id
		  FROM dual;	 
		
		BEGIN
			SELECT t401.c1900_company_id compid ,
			  t401.c5040_plant_id plantid,
			  get_plant_name(t401.c5040_plant_id) plant_nm ,
			  get_plant_name(v_plant_id) curr_plant_nm
			  INTO v_po_company_id, v_po_plant_id, v_po_plant_nm, v_plant_nm
			FROM t401_purchase_order T401
			WHERE t401.c401_purchase_ord_id = p_poid
			AND t401.c401_void_fl          IS NULL
			AND t401.c1900_company_id       = v_company_id;
		EXCEPTION
		WHEN NO_DATA_FOUND THEN
			v_po_plant_id := NULL;
		END;
		
		/*
		 * If the PO Plant id is not null and if the Plant id from context is not equal to PO plant id
		 * Then validate PO based on the plant
		 */
		
		IF v_po_plant_id IS NOT NULL AND v_plant_id <> v_po_plant_id
		THEN
			GM_RAISE_APPLICATION_ERROR('-20999','274',p_poid||'#$#'||v_po_plant_nm||'#$#'||v_plant_nm); 
		END IF;
		
	END gm_validate_po_plant;
	
	/***************************************************************************************************
     * Purpose: Procedure to validate the PO based on plant, project and partnumber while initiating PO
	****************************************************************************************************/
	PROCEDURE gm_validate_product_plant (
		p_project_id   IN  T202_PROJECT.C202_PROJECT_ID%TYPE
	  , p_wo_inpstr	   IN  CLOB
		)
		AS
		v_company_id    	T1900_COMPANY.C1900_COMPANY_ID%TYPE;
		v_plant_id			T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
		v_viacell_plant_fl  T906_RULES.C906_RULE_VALUE%TYPE;
		v_viacell_cmpny_fl  T906_RULES.C906_RULE_VALUE%TYPE;
		v_strlen 			NUMBER := NVL (LENGTH (p_wo_inpstr), 0);
		v_string 			CLOB   := p_wo_inpstr;
		v_substring 		VARCHAR2 (4000);
		v_part_num			T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE;
		v_proj_id			T202_PROJECT.C202_PROJECT_ID%TYPE;
		v_viacell_fl 		T906_RULES.C906_RULE_VALUE%TYPE;
		BEGIN
		
		SELECT get_compid_frm_cntx(), get_plantid_frm_cntx()
		  INTO v_company_id, v_plant_id
		  FROM dual;
		
		 --validating PO is available for company 
		SELECT NVL(get_rule_value(v_company_id,'VIACELLCMPNY'),'N')
			INTO v_viacell_cmpny_fl 
			FROM DUAL;
		  
	   	IF v_viacell_cmpny_fl IS NULL OR v_viacell_cmpny_fl <> 'Y' 
	   	THEN
		   RETURN;
	    END IF;
	   
		--Get the viacell plant flag from rules table by passing the rule id as plant id and rule group as �VIACELLPLANT�
		SELECT NVL(get_rule_value(v_plant_id,'VIACELLPLANT'),'N')
			INTO v_viacell_plant_fl 
			FROM DUAL;

		--To validate the project and plant selected
		IF p_project_id <> '0' AND p_project_id IS NOT NULL THEN
			v_viacell_fl 	:= NVL(get_rule_value(p_project_id,'VIACELL'),'N');  --Rule value := 'Y'
			/*
			 * If the v_viacell_fl is coming as 'Y' and the v_viacell_plant_fl  is not equal to 'Y'
			 * or if the v_viacell_fl is not equal to 'Y' and if the v_viacell_plant_fl is equal to 'Y'
			 * Then validate PO based on the plant
			 */
			IF (v_viacell_fl = 'Y' AND v_viacell_plant_fl != 'Y') OR (v_viacell_fl != 'Y' AND v_viacell_plant_fl = 'Y')	THEN
				GM_RAISE_APPLICATION_ERROR('-20999','275','');
			END IF;
		END IF;
		
		/* Input string is coming as like pnum^projectid^..if we have single row in input string 
		 * Then Append the pipe symbol after the last character of the string
		 */
		IF v_string IS NOT NULL THEN
		  v_substring := SUBSTR (v_string,- 1) ; --Getting the last character of the string
			IF v_substring <> '|' THEN
		 	 v_string := CONCAT(v_string,'|');
			END IF;
		END IF;
		
		IF v_strlen > 0 THEN
	       WHILE INSTR (v_string, '|') <> 0
	        LOOP
	            v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
	     	    v_string    := SUBSTR (v_string, INSTR (v_string, '|')    + 1) ;
	      		v_part_num  := NULL;
	            --
	            v_part_num  :=  SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
	            --	
	            BEGIN            
			     SELECT c202_project_id 
					INTO v_proj_id	
				  FROM t205_part_number 
				  WHERE c205_part_number_id = v_part_num;
				 	EXCEPTION
					WHEN NO_DATA_FOUND THEN
					v_proj_id := NULL;
				END;
	       	 	 v_viacell_fl 	:= NVL(get_rule_value(v_proj_id,'VIACELL'),'N');  --Rule value := 'Y'
				/*
				 * If the v_viacell_fl is coming as 'Y' and the v_viacell_plant_fl  is not equal to 'Y'
				 * or if the v_viacell_fl is not equal to 'Y' and if the v_viacell_plant_fl is equal to 'Y'
				 * Then validate PO based on the plant
				 */
				IF (v_viacell_fl = 'Y' AND v_viacell_plant_fl != 'Y') OR (v_viacell_fl != 'Y' AND v_viacell_plant_fl = 'Y')	THEN
					GM_RAISE_APPLICATION_ERROR('-20999','275',''); 
				END IF;
	         END LOOP;
	     END IF;
   
	END gm_validate_product_plant;

		
	/********************************************************************************************************************************
     * Purpose: Procedure to Validate the PO based on the plant [overloaded procedure of gm_validate_product_plant()] 
     * Calling the below procedure from GmPurchaseBean.validatePOProdPlant() to capture the error message and show it in front end.
	*********************************************************************************************************************************/
	PROCEDURE gm_validate_product_plant (
		p_project_id  		IN  T202_PROJECT.C202_PROJECT_ID%TYPE
	  , p_wo_inpstr			IN  CLOB
	  , p_out_str			OUT VARCHAR2
		)
		AS
		BEGIN
			p_out_str := NULL;
			BEGIN
			    gm_validate_product_plant(p_project_id,p_wo_inpstr);
				EXCEPTION 
				WHEN OTHERS THEN
				p_out_str := SQLERRM ;
			END;
	   END gm_validate_product_plant;
/******************************************************************
* Description : This Procedure is used to Raise the PO for around 400 parts
****************************************************************/
PROCEDURE gm_sav_initiate_po(
    p_vendorid IN t401_purchase_order.c301_vendor_id%TYPE ,
    p_po_type  IN t401_purchase_order.c401_type%TYPE,
    p_pnumstr  IN CLOB ,
    p_user_id  IN t101_user.c101_user_id%TYPE ,
    p_poid OUT VARCHAR2 ,
    p_msg OUT CLOB,
    p_po_comments IN t401_purchase_order.c401_po_notes%type DEFAULT NULL )
AS
  v_vendid t401_purchase_order.c301_vendor_id%TYPE;
  v_pototal t401_purchase_order.c401_po_total_amount%TYPE;
  v_userid t401_purchase_order.c401_created_by%TYPE  ;
  v_type t401_purchase_order.c401_type%TYPE   := p_po_type;
  v_po_id   VARCHAR2(100);
  v_message VARCHAR2(1000);
  v_pnum t402_work_order.c205_part_number_id%TYPE;
  v_rev t402_work_order.c402_rev_num%TYPE;
  v_qty t402_work_order.c402_qty_ordered%TYPE;
  v_cost t402_work_order.c402_cost_price%TYPE;
  v_critfl t402_work_order.c402_critical_fl%TYPE       :='';
  v_dhrid t402_work_order.c408_dhr_id%TYPE             :='';
  v_farfl t402_work_order.c402_far_fl%TYPE             :='';
  v_validateFl t402_work_order.c402_validation_fl%TYPE :='N';
  v_lotCount NUMBER                                    :=0;
  v_priceid t402_work_order.c405_pricing_id%TYPE;
  v_project_id t202_project.c202_project_id%TYPE;
  v_invalid_partstr CLOB;
  v_strlen          NUMBER := NVL (LENGTH (p_pnumstr), 0) ;
  v_string CLOB            := p_pnumstr;
  v_substring VARCHAR2 (4000) ;
  v_invalid_cost_partstr CLOB;
  v_cnt_cost_partstr CLOB;
  v_cnt_part_price NUMBER;
  V_non_wo_dco_parts CLOB;
  --
  v_company_id    	T1900_COMPANY.C1900_COMPANY_ID%TYPE;
  v_po_notes        T401_PURCHASE_ORDER.C401_PO_NOTES%TYPE;
  v_out_log_message VARCHAR2(4000);
  
   CURSOR v_cur_batch_po IS
  SELECT MY_TEMP_TXN_ID  C205_PART_NUMBER_ID,SUM(MY_TEMP_TXN_KEY) QTY, MY_TEMP_TXN_VALUE FAR from my_temp_key_value GROUP BY MY_TEMP_TXN_ID, MY_TEMP_TXN_VALUE;
/*********************************************************************************************************
* PC-5657 Batch PO allowing to create PO for incomplete WO DCO Parts 
* Description : In Batch PO purchase order done for incomplete WO DOC part NO for alert pop up, query moved there 
********************************************************************************************************/ 
BEGIN
  SELECT get_compid_frm_cntx() 
		  INTO v_company_id 
		  FROM dual;
  v_pototal := 0;
  v_userid := p_user_id;  
  --
  GM_PLACE_PO(p_vendorid,v_pototal,v_userid,v_type, NULL,v_po_id,v_message);
  
  IF p_po_comments IS NOT NULL THEN
  	GM_UPDATE_LOG(v_po_id,p_po_comments,'1203',v_userid,v_out_log_message);
  END IF;
  
  WHILE INSTR (v_string, '|') <> 0
  LOOP
    v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
    v_string    := SUBSTR (v_string, INSTR (v_string, '|')    + 1) ;
    v_pnum      := NULL;
    v_qty       := NULL;
    v_farfl		:= NULL;
    v_pnum      := SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1) ;
    v_substring := SUBSTR (v_substring, INSTR (v_substring, ',')    + 1) ;
    v_qty       := SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1) ;
    --
    v_substring := SUBSTR (v_substring, INSTR (v_substring, ',')    + 1) ;
    v_farfl 	:= v_substring ;
    --
    -- PMT-14530 added below code to avoid Multiple Work Order created for a Part on a PO
    -- inserting values into temp table 
    INSERT INTO my_temp_key_value (MY_TEMP_TXN_ID,MY_TEMP_TXN_KEY,MY_TEMP_TXN_VALUE) VALUES(v_pnum,v_qty,v_farfl);
       
  	END LOOP;
  	
  	--Before creating the WO, need to check if the WO DCO is complete for the Parts.
  	-- Output from the procedure returns the Parts that are in the Batch which do not have the WO DCO.
  	gm_pkg_op_purchasing.gm_validate_WO_DCO(p_po_type,v_company_id,V_non_wo_dco_parts);
  	
  	-- PMT-14530 added below code to avoid Multiple Work Order created for a Part on a PO
    -- fetching values from temp table 
    FOR v_cur_part_num IN v_cur_batch_po
    LOOP
		
		v_pnum := v_cur_part_num.C205_PART_NUMBER_ID;
		v_qty  := v_cur_part_num.QTY;
		v_farfl := v_cur_part_num.FAR;
		

    SELECT TRIM(get_project_id_from_part(v_pnum)) INTO v_project_id FROM DUAL;
    --Forming the part number string for invalid parts.
    IF v_project_id IS NULL THEN
    IF (v_invalid_partstr IS NULL OR instr(v_invalid_partstr,v_pnum||',') <= 0) THEN
      v_invalid_partstr := v_invalid_partstr ||v_pnum ||', ';
    END IF;
      v_qty             := 0 ;
    END IF;
    
    IF v_qty IS NOT NULL THEN
      v_qty  := TO_NUMBER(v_qty);
    END IF;
    --Proceed with WO Creation only when the part quantity is >0
    IF (v_qty > 0) THEN
      SELECT C205_REV_NUM
      INTO v_rev
      FROM T205_PART_NUMBER
      WHERE C205_PART_NUMBER_ID = v_pnum;
      BEGIN
	      SELECT COUNT(1) INTO v_cnt_part_price 
        FROM T405_VENDOR_PRICING_DETAILS
       WHERE C405_COST_PRICE  IS NOT NULL
         AND C405_ACTIVE_FL      = 'Y'
         AND c301_vendor_id = p_vendorid
         AND C205_PART_NUMBER_ID = v_pnum;
         
      SELECT
    c405_vendor_price,
    c405_pricing_id
INTO
    v_cost,
    v_priceid
FROM
    globus_dm_operation.st405_vendor_active_pricing
WHERE
    c301_vendor_id = p_vendorid
    AND c205_part_number_id = v_pnum
    AND c405_to_qty >= v_qty
    AND ROWNUM = 1
ORDER BY
    c405_to_qty;
        
        
      EXCEPTION
      WHEN OTHERS THEN
        v_cost    := null;
        v_priceid := null;
       END ;
       
 -- disabled for PC-5631      
/*       
        IF (v_cnt_part_price > 1) THEN
        v_cnt_cost_partstr := v_cnt_cost_partstr ||v_pnum ||', '; 
        v_cost := NULL ;
      END IF;
*/
       --Forming the part number string for part contain no price.
       IF (v_cost  IS NULL AND v_cnt_cost_partstr IS NULL)THEN
          IF (v_invalid_cost_partstr IS NULL OR instr(v_invalid_cost_partstr,v_pnum||',') <= 0) THEN
             v_invalid_cost_partstr := v_invalid_cost_partstr ||v_pnum ||', ';
         END IF;
         v_cost := NULL ;
         END IF;
     
    --placing the PO for the part contain only price and PMT-23233 Batch PO to accept $0 Vendor Price.     
      IF (v_cost IS NOT NULL AND v_cost >= 0) THEN
        GM_PLACE_WO(v_po_id,v_pnum,v_rev,p_vendorid,v_qty,v_cost,v_critfl,v_dhrid,v_farfl,v_validateFl,v_lotCount,v_priceid,v_type,v_userid,v_message);
        v_pototal := v_pototal+(v_cost*v_qty); 
       END IF;
      v_priceid := '';
      v_cost    := '';
    END IF;
  END LOOP;
  -- delete my_temp_key_value temp table values
  DELETE FROM my_temp_key_value;	
 --fetching po notes from rule table 
		  SELECT NVL(get_rule_value_by_company('PO_COMMENTS','Batch_PO_Comments',v_company_id),'PO Created From Cut Plan Load')
			INTO v_po_notes 
			FROM DUAL;
			
  UPDATE T401_PURCHASE_ORDER
  SET C401_PO_TOTAL_AMOUNT   =v_pototal , 
    C401_PO_NOTES            = nvl(p_po_comments,v_po_notes) --PC-4383: TTP By Vendor-PO creation for Branch
  WHERE C401_PURCHASE_ORD_ID = v_po_id;
  --Checking for invalid parts, parts contain no price .
 IF v_invalid_partstr IS NOT NULL THEN
    v_invalid_partstr := SUBSTR(v_invalid_partstr,1,LENGTH(v_invalid_partstr)-2);
    p_msg  := p_msg || 'The following are invalid parts :('||v_invalid_partstr||').';
 END IF;
 IF  v_invalid_cost_partstr IS NOT NULL THEN
    v_invalid_cost_partstr := SUBSTR(v_invalid_cost_partstr,1,LENGTH(v_invalid_cost_partstr)-2);
    p_msg := p_msg || '<BR>The following part number(s) contain no price :('||v_invalid_cost_partstr||').';
 END IF;
 
  -- disabled for PC-5631 
 /*
 IF v_cnt_cost_partstr IS NOT NULL THEN
    v_cnt_cost_partstr := SUBSTR(v_cnt_cost_partstr,1,LENGTH(v_cnt_cost_partstr)-2); 
    p_msg  := p_msg || '<BR>The following part number(s) contain more than one price :('||v_cnt_cost_partstr||').';
 END IF;
 */
 
 
  IF V_non_wo_dco_parts IS NOT NULL THEN
  	p_msg  := p_msg || '<BR>PO cannot be raised for the following part number(s) because the Work Order DCO has not been done. :('||V_non_wo_dco_parts||').';
  END IF;
 
 IF p_msg IS NOT NULL THEN 
     p_msg  := p_msg ||	' <BR>Please remove the part number(s) mentioned from your excel in order to save. For Parts unable to load,Please check with Product Manager';
  END IF;
  p_poid := v_po_id;
END gm_sav_initiate_po;
/******************************************************************
* Description : This Procedure is used to fetch the PO details for Batch PO screen
****************************************************************/
PROCEDURE gm_fch_po_details(
    p_poid IN t402_work_order.c401_purchase_ord_id%TYPE ,
    p_po_details OUT TYPES.cursor_type )
AS
BEGIN
  OPEN p_po_details FOR SELECT C401_PURCHASE_ORD_ID POID,
  GET_VENDOR_NAME(C301_VENDOR_ID) VNAME ,
  C401_DATE_REQUIRED REQDT,
  C401_PO_TOTAL_AMOUNT TOTAL,
  GET_USER_NAME(C401_CREATED_BY) CUSER,
  C301_VENDOR_ID VID,
  GET_CODE_NAME(GET_VENDOR_CURRENCY(C301_VENDOR_ID)) VENDCURR FROM T401_PURCHASE_ORDER WHERE C401_PURCHASE_ORD_ID IN (p_poid) ORDER BY C401_PURCHASE_ORD_ID;
END gm_fch_po_details;	

/******************************************************************
* PMT-35876  - Load Receive Shipment Screen from Scanning Delivery Router or Branch PO 
* Description : This Procedure is used to fetch the PO ID  for Get PO Details used DR ID
****************************************************************/
PROCEDURE gm_validate_dr_id(
  p_drid      IN   t4200_delivery_router.C4200_DR_ID%TYPE,  
  p_out_poid  OUT  t401_purchase_order.C401_PURCHASE_ORD_ID%TYPE  
)
AS
v_drid t4200_delivery_router.c4200_dr_id%TYPE;
BEGIN
	BEGIN
      SELECT C401_PURCHASE_ORD_ID 
        INTO v_drid
        FROM T4200_DELIVERY_ROUTER 
       WHERE C4200_VOID_FL IS NULL
         AND C4200_DR_ID = p_drid;
   EXCEPTION 
		     WHEN NO_DATA_FOUND THEN
		     v_drid := null;	
END;
    p_out_poid := v_drid;
       
END gm_validate_dr_id;

/******************************************************************
* Description : This Procedure is used to Validate if the Parts have the Work Order DCO is done.
* Changes for PMT-49117 -in this PMT  need to Validate the DCO part before PO generation.so removed PO ID .so removed PO ID
* Instead of PO ID pass PO type and company id
****************************************************************/
PROCEDURE gm_validate_WO_DCO(
	p_po_type    IN T401_PURCHASE_ORDER.C401_TYPE%TYPE ,
    p_company_id IN T401_PURCHASE_ORDER.C1900_COMPANY_ID%TYPE,
    P_NON_WO_DCO_PARTS OUT CLOB 
)
AS
v_comp_id  T401_PURCHASE_ORDER.C1900_COMPANY_ID%TYPE ;
v_po_type  T401_PURCHASE_ORDER.C401_TYPE%TYPE ;
v_validate_comp_id  VARCHAR2(10);
v_validate_PO_Type  VARCHAR2(10);

BEGIN
	
	BEGIN
	
		dbms_output.put_line('p_company_id:'||p_company_id||':p_po_type:'||p_po_type);
		
		SELECT 
			NVL(GET_RULE_VALUE(p_company_id,'WO_DCO_VLDN_COMPANY'),'N'),
			NVL(GET_RULE_VALUE(p_po_type,'WO_DCO_VLDN_POTYPE'),'N')
		INTO	 v_validate_comp_id  ,  v_validate_PO_Type 
		FROM DUAL;
		
		dbms_output.put_line('v_validate_comp_id:'||v_validate_comp_id||':v_validate_PO_Type:'||v_validate_PO_Type);
		-- WO DCO Validation to be done only for certain Company and Certain PO Type.
		IF v_validate_comp_id = 'Y' AND v_validate_PO_Type = 'Y'
		THEN
	
	
			SELECT RTRIM (XMLAGG (XMLELEMENT (e, pnum || ',')) .EXTRACT ('//text()'), ',') 
		            INTO P_NON_WO_DCO_PARTS  
		            FROM  
		                (
							SELECT MY_TEMP_TXN_ID pnum 
							FROM my_temp_key_value
							WHERE MY_TEMP_TXN_ID IS NOT NULL
							MINUS
							SELECT C205_PART_NUMBER_ID 
							FROM T205D_PART_ATTRIBUTE T205D , my_temp_key_value TEMP
							WHERE T205D.C205D_VOID_FL IS NULL
							AND T205D.C901_ATTRIBUTE_TYPE=103380
							AND T205D.C205D_ATTRIBUTE_VALUE='103360'
							AND C205_PART_NUMBER_ID = TEMP.MY_TEMP_TXN_ID
							AND MY_TEMP_TXN_ID IS NOT NULL
						);
		
		END IF;
	EXCEPTION WHEN OTHERS THEN
		 NULL;
	END;
END gm_validate_WO_DCO;
	
	/************************************************************************
    * Purpose: This function is used to fetch AutoPublish Flag for the vendor
    * author: Vinoth
    **************************************************************************/
	FUNCTION get_auto_publish_fl (
        p_poid 	   IN  T401_PURCHASE_ORDER.C401_PURCHASE_ORD_ID%TYPE ,
        p_vendorid IN  t401_purchase_order.c301_vendor_id%TYPE )
    RETURN  VARCHAR2
	IS
    v_auto_publish_fl t301_vendor.C301_AUTO_PUBLISH_FL%TYPE;
	BEGIN
	--
		BEGIN
     	SELECT t301.c301_AUTO_PUBLISH_FL
      	  INTO v_auto_publish_fl
       	  FROM t301_vendor t301, t401_purchase_order t401
         WHERE t301.C301_VENDOR_ID = t401.c301_vendor_id
           AND t301.C301_VENDOR_ID = p_vendorid
  	       AND t401.C401_PURCHASE_ORD_ID = p_poid
           AND t401.C401_VOID_FL IS NULL
           AND t301.C301_ACTIVE_FL IS NULL;
      
		EXCEPTION
		WHEN OTHERS THEN
    		RETURN NULL;
		END;
	
	 	RETURN v_auto_publish_fl;

END get_auto_publish_fl;
/**********************************************************************************************
* Description : This Procedure is used to set the part number string to clob list and call gm_validate_WO_DCO.
************************************************************************************************/
PROCEDURE gm_validate_vendor_WO_DCO(
    p_part_num_str IN CLOB,
    p_po_type      IN t401_purchase_order.c401_type%TYPE ,
    p_company_id   IN t401_purchase_order.c1900_company_id%TYPE,
    p_non_wo_dco_parts OUT CLOB )
AS
BEGIN
  my_context.set_my_cloblist(p_part_num_str||',');
  
  -- Delete the temporary table entry initially
  DELETE FROM MY_TEMP_KEY_VALUE;
  
  INSERT INTO MY_TEMP_KEY_VALUE
    (MY_TEMP_TXN_ID
    )
  SELECT c205_part_number_id FROM my_t_part_list;
  --Before creating the PO and WO, need to check if the WO DCO is complete for the Parts.
  -- Output from the procedure returns the Parts that are in the Batch which do not have the WO DCO.
  gm_pkg_op_purchasing.gm_validate_WO_DCO(p_po_type,p_company_id,P_NON_WO_DCO_PARTS);
  
END gm_validate_vendor_WO_DCO;
/**********************************************************************************************
* Description : This Procedure is used to Raise PO and WO and then update the qty, po amount to t401 table
************************************************************************************************/
PROCEDURE gm_sav_ttp_by_vendor_initiate_po(
    p_vendor_id     IN t401_purchase_order.c301_vendor_id%TYPE,
    p_po_type       IN t401_purchase_order.C401_TYPE%TYPE,
    p_po_str        IN VARCHAR2,
    p_po_comments   IN VARCHAR2,
    p_company_id    IN t401_purchase_order.c1900_company_id%TYPE,
    p_user_id       IN t101_user.c101_user_id%TYPE,
    p_out_po_id    OUT VARCHAR2,
    p_out_po_dtls  OUT VARCHAR2)
AS
  v_pototal         NUMBER;
  v_totalqty        NUMBER;
  v_wo_cnt          NUMBER;
  v_out_msg         VARCHAR2(4000);
  v_out_log_message VARCHAR2(4000);
  v_message         VARCHAR2(4000);
  v_string CLOB;
  v_substring VARCHAR2 (3000);
 
  --
  v_part_num t402_work_order.c205_part_number_id%TYPE;
  v_part_revision t402_work_order.c402_rev_num%TYPE;
  v_qty t402_work_order.c402_qty_ordered%TYPE;
  v_price t402_work_order.c402_cost_price%TYPE;
  v_price_id t402_work_order.c405_pricing_id%TYPE;
  --
  v_critfl t402_work_order.c402_critical_fl%TYPE       :='';
  v_dhrid t402_work_order.c408_dhr_id%TYPE             :='';
  v_farfl t402_work_order.c402_far_fl%TYPE             :='';
  v_validateFl t402_work_order.c402_validation_fl%TYPE :='';
  v_lotCount NUMBER                                    :=0;
  
BEGIN
  v_pototal := 0;
 
 --passing parameters vendor id, po total, user id, po type, company id, out - po id , message
  GM_PLACE_PO(p_vendor_id,v_pototal,p_user_id,p_po_type,p_company_id,p_out_po_id,v_out_msg);
 
  GM_UPDATE_LOG(p_out_po_id,p_po_comments,'1203',p_user_id,v_out_log_message);
  
  v_totalqty                  :=0;
  v_wo_cnt                    :=0;
  v_pototal                   :=0;
  v_string                    := p_po_str;

  WHILE INSTR (v_string, '^') <> 0
  LOOP
    v_substring     :=(SUBSTR (v_string, 1, INSTR (v_string, '^') - 1));
    v_string        := (SUBSTR (v_string, INSTR (v_string, '^')   + 1));
     
    v_part_num      := NULL;
    v_qty           := NULL;
    v_part_revision := NULL;
    v_price_id      := NULL;
    v_part_num      := SUBSTR (v_substring, 1, instr (v_substring, ',')           - 1);
    v_substring     := SUBSTR (v_substring, instr (v_substring, ',')              + 1);
    v_price         := SUBSTR (v_substring, 1, instr (v_substring, ',')           - 1);
    v_substring     := SUBSTR (v_substring, instr (v_substring, ',')              + 1);
    v_qty           := TO_NUMBER(SUBSTR (v_substring, 1, instr (v_substring, ',') - 1));
    v_substring     := SUBSTR (v_substring, instr (v_substring, ',')              + 1);
    v_part_revision := SUBSTR (v_substring, 1, instr (v_substring, ',')           - 1);
    v_substring     := SUBSTR (v_substring, instr (v_substring, ',')              + 1);
    v_price_id 		:= v_substring; -- PC-295 spliting pricing id in PO creation string
    

    --parameters are -po id, part num , part revision , vendor id,qty,price, critical flag, dhr id, far flag,validate flag , lot count, price id,PO Type,user id ,out message
    GM_PLACE_WO(p_out_po_id,v_part_num,v_part_revision,p_vendor_id,v_qty,v_price,v_critfl,v_dhrid,v_farfl,v_validateFl,v_lotCount,v_price_id,p_po_type,p_user_id,v_message);

    v_totalqty := v_totalqty +v_qty; --calculate total raise po qty
    v_wo_cnt   := v_wo_cnt   +1;     --to calcultate total work order count
   
  END LOOP;--END while LOOP
  
  SELECT GET_PO_TOTAL_AMOUNT(p_out_po_id) INTO v_pototal FROM DUAL;
 
  --Update the total to t401 table
   gm_pkg_op_purchasing.gm_upd_po_total_amt(p_out_po_id ,v_pototal , p_user_id);
 
  -- form th PO details string
  p_out_po_dtls             := v_pototal ||'^'|| v_totalqty ||'^'|| v_wo_cnt ||'|';
 
END gm_sav_ttp_by_vendor_initiate_po;
/**********************************************************************************************
* Description : This Procedure is used to update the po amount to t401 table
************************************************************************************************/
PROCEDURE gm_upd_po_total_amt(
   p_po_id     IN t401_purchase_order.c401_purchase_ord_id%type,
   p_po_amount IN t401_purchase_order.c401_po_total_amount%type,
   p_user_id   IN t101_user.c101_user_id%TYPE
)
AS
BEGIN

	 UPDATE t401_purchase_order
        SET c401_po_total_amount   = p_po_amount,
            c401_last_updated_date = CURRENT_DATE,
            c401_last_updated_by   = p_user_id
      WHERE c401_purchase_ord_id   = p_po_id
        AND c401_void_fl          IS NULL;
END gm_upd_po_total_amt;
/**********************************************************************************************
* Description : This Procedure is used to split the PO ids and 
* call gm_pkg_common_cancel.gm_op_sav_void_purchpo to void the po and WO
************************************************************************************************/
PROCEDURE gm_void_vendor_po_dtls(
 p_po_ids     IN CLOB,
 p_user_id   IN t101_user.c101_user_id%TYPE
)
AS
CURSOR po_id_cur
		IS
		SELECT c205_part_number_id poid  FROM my_t_part_list;
BEGIN
	  my_context.set_my_cloblist(p_po_ids||',');
	
  FOR po_id IN po_id_cur
     LOOP 
       gm_pkg_common_cancel.gm_op_sav_void_purchpo(po_id.poid,p_user_id);
  
  END LOOP;
	  
END gm_void_vendor_po_dtls;
END gm_pkg_op_purchasing;
/

