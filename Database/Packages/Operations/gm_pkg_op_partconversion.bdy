/* Formatted on 2008/09/19 12:16 (Formatter Plus v4.8.0) */
CREATE OR REPLACE PACKAGE BODY gm_pkg_op_partconversion
IS
--
/*******************************************************
	 * Description : Procedure to fetch details about the part which has to be transformed
	 * Author	: Joe P Kumar
 *******************************************************/
	PROCEDURE gm_fch_frompartdetails (
		p_partnum	   IN		t205_part_number.c205_part_number_id%TYPE
	  , p_invtype	   IN		t901_code_lookup.c901_code_id%TYPE
	  , p_outdetails   OUT		TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_outdetails
		 FOR
			 SELECT t205.c205_part_number_id partnumber, t205.c205_part_num_desc partdescription
				  --, get_qty_in_stock (p_partnum) invqoh
					,get_partnumber_qty (p_partnum, 90802) invqoh
				  , DECODE (p_invtype
						  , 90802, get_ac_cogs_value (p_partnum, '4909')
						  , get_ac_cogs_value (p_partnum, '4900')
						   ) cogscost
			   FROM t205_part_number t205
			  WHERE t205.c205_part_number_id = p_partnum;
	END gm_fch_frompartdetails;

	   /*******************************************************
		* Description : Procedure to fetch details about the part to which p_partnum has to be converted to
		* Author   : Joe P Kumar
	*******************************************************/
	PROCEDURE gm_fch_topartdetails (
		p_partnum	   IN		t205_part_number.c205_part_number_id%TYPE
	  , p_invtype	   IN		t901_code_lookup.c901_code_id%TYPE
	  , p_outdetails   OUT		TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_outdetails
		 FOR
			 SELECT   t205a.c205_to_part_number_id topartnum
					, get_partnum_desc (t205a.c205_to_part_number_id) topartdescription
					, DECODE (p_invtype
							, 90802, get_ac_cogs_value (p_partnum, '4909')
							, get_ac_cogs_value (p_partnum, '4900')
							 ) tocogscost
				 FROM t205_part_number t205, t205a_part_mapping t205a
				WHERE t205.c205_part_number_id = t205a.c205_from_part_number_id
				  AND t205a.c901_type = 30032
				  AND t205a.c205_from_part_number_id = p_partnum
				  AND t205a.c205a_void_fl is null  ---  added new column c205a_void_fl in t205a used to PMT-50777 - Create BOM and sub-component Mapping	
			 ORDER BY t205a.c205_to_part_number_id;
	END gm_fch_topartdetails;

/*******************************************************
	* Description : Procedure to save part conversion details
	* Author   : Joe P Kumar
	* Algorithm:
		   1. validations - p_convqty <= get_qty_in_stock(p_partnum)(type is FG) or the new function (if type is RM)
		   2. Insert 'from pnum' data into T217
		   3. Decrease part qty in 205c or 205 depending on the type
			   If 205, then regular update stmnt. Else gm_cm_sav_partqty
		   4. call posting - gn_Save_ledger_posting
		   5. Insert input string (will contain data as to_pnum^to_qty^to_cost^to_type|)data into t218
		   6. Increase part qty in	205c or 205 depending on the type
			   If 205, then regular update stmnt. Else gm_cm_sav_partqty
		   7. call posting - gn_Save_ledger_posting (in loop)
		   8. calculate the difference and post it to WIP change

*******************************************************/
	PROCEDURE gm_sav_topartdetails (
		p_partnum		 IN 	  t205_part_number.c205_part_number_id%TYPE
	  , p_partconvdesc	 IN 	  t217_part_conversion.c217_comments%TYPE
	  , p_inputstr		 IN 	  VARCHAR2
	  , p_convqty		 IN 	  t218_part_conversion_detail.c218_conversion_qty%TYPE
	  , p_totalprice	 IN 	  t218_part_conversion_detail.c218_conversion_cost%TYPE
	  , p_invtype		 IN 	  t901_code_lookup.c901_code_id%TYPE
	  , p_userid		 IN 	  t217_part_conversion.c217_created_by%TYPE
	  , p_outpcid		 OUT	  t217_part_conversion.c217_part_conversion_id%TYPE
	)
	AS
		v_strlen	   NUMBER := NVL (LENGTH (p_inputstr), 0);
		v_string	   VARCHAR2 (30000) := p_inputstr;
		v_substring    VARCHAR2 (1000);
		v_seq		   NUMBER;
		v_pcid		   t217_part_conversion.c217_part_conversion_id%TYPE;
		v_convqty	   t218_part_conversion_detail.c218_conversion_cost%TYPE;
		v_pnum		   t218_part_conversion_detail.c205_to_part_number_id%TYPE;
		v_price 	   t218_part_conversion_detail.c218_conversion_qty%TYPE;
		v_qty		   t218_part_conversion_detail.c218_conversion_cost%TYPE;
		v_invtype	   t901_code_lookup.c901_code_id%TYPE;
		v_frompartqty  t820_costing.c820_qty_on_hand%TYPE;
		v_part_conver_detail_id t218_part_conversion_detail.c218_part_conver_detail_id%TYPE;
		v_frm_posting_type t804_posting_ref.c901_transaction_type%TYPE;
		v_posting_type t804_posting_ref.c901_transaction_type%TYPE;
		v_action       t205c_part_qty.c901_action%TYPE;
		v_sumtoconvamt NUMBER;
		v_sumfromconvamt NUMBER;
		v_diff		   NUMBER;
		v_totconvqty   NUMBER;
	BEGIN
		-- 1. validations - p_convqty < get_qty_in_stock(p_partnum)(type is FG) or the new function (if type is RM)
		IF p_invtype = 90800
		THEN
			v_posting_type := 4900;
		    v_action := 4301;
			--
			SELECT get_qty_in_stock (p_partnum)
			  INTO v_frompartqty
			  FROM DUAL;
		ELSIF p_invtype = 90802
		THEN
			v_posting_type := 4909;
		    v_action := 4302;
			--
			SELECT get_partnumber_qty (p_partnum, p_invtype)
			  INTO v_frompartqty
			  FROM DUAL;
		END IF;

		IF (p_convqty > v_frompartqty)
		THEN
			raise_application_error ('-20939', '');
		END IF;

		--	2. Insert 'from pnum' data into T217
		SELECT 'GM-PC-' || s217_part_conversion.NEXTVAL
		  INTO v_pcid
		  FROM DUAL;

		INSERT INTO t217_part_conversion
					(c217_part_conversion_id, c205_from_part_number_id, c217_from_part_cost, c217_conversion_dt
				   , c217_comments, c901_type, c217_status_fl, c217_update_inv_fl, c217_created_by, c217_created_date
					)
			 VALUES (v_pcid, p_partnum, get_ac_cogs_value (p_partnum, v_posting_type), TRUNC (SYSDATE)
				   , p_partconvdesc, p_invtype, 2	-- 2 is completed
												 , 1   -- inventory updated
													, p_userid, SYSDATE
					);

		-- 3. Decrease part qty in 205c or 205 depending on the type. If 205, then regular update stmnt. Else gm_cm_sav_partqty
		v_convqty	:= p_convqty * -1;

		
		gm_cm_sav_partqty (p_partnum, v_convqty, v_pcid, p_userid, p_invtype, v_action, 4320);	 -- 4301 -> plus 4302 -> minus. 4320 -> part conversion
		

		-- 4. call posting - gm_Save_ledger_posting
		gm_save_ledger_posting (get_rule_value (p_invtype, 'PC-CONV-FROM')
							  , SYSDATE
							  , p_partnum
							  , NULL
							  , v_pcid
							  , p_convqty
							  , p_convqty * get_ac_cogs_value (p_partnum, v_posting_type)
							  , p_userid
							   );

		--	5. Insert input string (will contain data as to_pnum^to_qty^to_cost^to_type|)data into t218
		IF v_strlen > 0
		THEN
			WHILE INSTR (v_string, '|') <> 0
			LOOP
				--
				v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
				v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
				--
				v_pnum		:= NULL;
				v_qty		:= NULL;
				v_price 	:= NULL;
				v_invtype	:= NULL;
				--
				v_pnum		:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_qty		:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_price 	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_invtype	:= v_substring;

				SELECT s218_part_conver_detail_id.NEXTVAL
				  INTO v_part_conver_detail_id
				  FROM DUAL;

				INSERT INTO t218_part_conversion_detail
							(c218_part_conver_detail_id, c217_part_conversion_id, c205_to_part_number_id
						   , c218_conversion_qty, c218_conversion_cost
							)
					 VALUES (v_part_conver_detail_id, v_pcid, v_pnum
						   , v_qty, v_price
							);

				-- 6. Increase part qty in	205c or 205 depending on the type. If 205, then regular update stmnt. Else gm_cm_sav_partqty
				gm_cm_sav_partqty (v_pnum, v_qty, v_pcid, p_userid, v_invtype, 4301, 4320);	 -- 4301 -> plus , 4320 -> part conversion
				--	7. call posting - gn_Save_ledger_posting (in loop)
				gm_save_ledger_posting (get_rule_value (p_invtype, 'PC-CONV-TO')
									  , SYSDATE
									  , v_pnum
									  , NULL
									  , v_pcid
									  , v_qty
									  , v_price
									  , p_userid
									   );
			--
			END LOOP;
		END IF;

		--	 8. calculate the difference and post it to WIP change
		SELECT SUM (c218_conversion_qty * c218_conversion_cost)
			 , SUM ((c218_conversion_qty * get_ac_cogs_value (p_partnum, v_posting_type))), SUM (c218_conversion_qty)
		  INTO v_sumtoconvamt
			 , v_sumfromconvamt, v_totconvqty
		  FROM t218_part_conversion_detail
		 WHERE c217_part_conversion_id = v_pcid;

		v_diff		:= v_sumtoconvamt - v_sumfromconvamt;

		IF (v_diff < 0)
		THEN
			v_diff		:= v_diff * -1;
			gm_save_ledger_posting (48058, SYSDATE, 'MFGEXPENSE', NULL, v_pcid, 1, v_diff, p_userid);
		ELSE
			gm_save_ledger_posting (48053, SYSDATE, 'MFGEXPENSE', NULL, v_pcid, 1, v_diff, p_userid);
		END IF;

		p_outpcid	:= v_pcid;
	END gm_sav_topartdetails;

	   /*******************************************************
		* Description : Procedure to fetch part conversion
		* Author   : Joe P Kumar
	*******************************************************/
	PROCEDURE gm_ac_fch_pcdetails (
		p_pcid			IN		 t217_part_conversion.c217_part_conversion_id%TYPE
	  , p_outpcheader	OUT 	 TYPES.cursor_type
	  , p_outpcdetail	OUT 	 TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_outpcheader
		 FOR
			 SELECT c217_part_conversion_id
				  , c205_from_part_number_id || ' - ' || get_partnum_desc (c205_from_part_number_id) partdescription
				  , get_code_name (c901_type) frominvtype, c217_comments partconversioncomments
				  , TO_CHAR (c217_conversion_dt, get_rule_value('DATEFMT','DATEFORMAT')) createddate, get_user_name (c217_created_by) createdby
			   FROM t217_part_conversion
			  WHERE c217_part_conversion_id = p_pcid;

		OPEN p_outpcdetail
		 FOR
			 SELECT c205_to_part_number_id pnum, get_partnum_desc (c205_to_part_number_id) partdesc
				  , c218_conversion_qty pcqty, c218_conversion_cost pcprice
				  , c218_conversion_qty * c218_conversion_cost extcost
			   FROM t218_part_conversion_detail
			  WHERE c217_part_conversion_id = p_pcid;
	END gm_ac_fch_pcdetails;
END gm_pkg_op_partconversion;
/
