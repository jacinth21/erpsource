--@"C:\PMT\BitBucket_WorkSpaces\erpglobus\Database\Packages\Operations\gm_pkg_vendor_portal_map.bdy";
/* Description : This is used to Vendor Mapping save, fetch, remove 
 * PMT-35870   : Mapping Vendor Portal User to Vendor
 */

CREATE OR REPLACE PACKAGE BODY gm_pkg_vendor_portal_map
IS
   /* Description : This is used to fetch the vendor users
    * PMT-35870   : Mapping Vendor Portal User to Vendor
    */
   PROCEDURE gm_pkg_fch_vendor_users(
		p_party_id 	  IN 	t108_party_to_party_link.c101_from_party_id%TYPE,
		p_out_cur	  OUT	TYPES.cursor_type
   )
	AS
	BEGIN
	  OPEN p_out_cur FOR
	
	  SELECT t108.c108_party_party_link_id partylinkid, t108.c101_from_party_id,t108.c101_to_party_id topartyid, t101u.c101_party_id, 
	         c101_first_nm firstname, c101_last_nm lastname, c102_login_username username, c101_email_id emailid, get_code_name_alt(c901_dept_id) dept 
	    FROM t101_party t101p, t101_user t101u, t102_user_login t102, t108_party_to_party_link t108 
	   WHERE t101p.c101_party_id = t101u.c101_party_id 
	     AND t102.c101_user_id = t101u.c101_user_id
	     AND t108.c101_to_party_id = t101u.c101_party_id 
	     AND t101p.c101_void_fl is null
	     AND t108.c108_void_fl is null
	     AND t101p.c901_party_type = '26240605'    /* 26240605 - Vendor Portal Map  */   
	     AND t108.c101_from_party_id = p_party_id
	     order by firstname;
	  
	END gm_pkg_fch_vendor_users;
   
	/* Description : This is used to void the vendor users
    * PMT-35870   : Mapping Vendor Portal User to Vendor
    */
   PROCEDURE gm_pkg_void_vendor_user(
		p_user_id				IN		t108_party_to_party_link.C108_LAST_UPDATED_BY%TYPE,
		p_party_link_id			IN		t108_party_to_party_link.C108_PARTY_PARTY_LINK_ID%TYPE 
    )
	AS
	BEGIN
		
	UPDATE t108_party_to_party_link 
	   SET c108_void_fl = 'Y', c108_last_updated_by=p_user_id, c108_last_updated_date = current_date
	 WHERE c108_party_party_link_id = p_party_link_id;
	 
   END gm_pkg_void_vendor_user;

   /* Description : This is used to save the vendor users
    * PMT-35870   : Mapping Vendor Portal User to Vendor */
	PROCEDURE gm_pkg_sav_vendor_user(
	   p_from_party_id		IN		t108_party_to_party_link.C101_FROM_PARTY_ID%TYPE,
	   p_to_party_id		IN		t108_party_to_party_link.C101_TO_PARTY_ID%TYPE,
	   p_created_by			IN		t108_party_to_party_link.C108_CREATED_BY%TYPE
	)
	AS
	BEGIN

			  INSERT INTO t108_party_to_party_link 
			                ( c108_party_party_link_id,
			                  c101_from_party_id,
			                  c101_to_party_id,
			                  c108_primary_fl,
			                  c108_void_fl,
			                  c108_created_by,
			                  c108_created_date,
			                  c108_last_updated_by,
			                  c108_last_updated_date )
		          VALUES ( s108_party_to_party_link.nextval,
		                   p_from_party_id,
		                   p_to_party_id,
		                   null,
		                   null,
		                   p_created_by,
		                   current_date,
		                   null,
		                   null );
	
	END gm_pkg_sav_vendor_user;

END gm_pkg_vendor_portal_map;
/