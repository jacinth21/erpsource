/* Formatted on 2011/04/19 15:09 (Formatter Plus v4.8.0) */
-- @"c:\database\packages\operations\gm_pkg_op_process_request.bdy"

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_process_request
IS
--
/**********************************************************************
   * Description : Procedure to initiate a set
   * Author 	 : Joe P Kumar
   Algorithm:
   1. Get Bulk Qty and Set Qty for all parts in the set
   2. validation. If set id is NULL, throw Error
   3. Creating MR since we have to create it in any case
   4. Since we do orderby on bulkqty and the first bulkqty is > 0, we initiate a consignment
   5. If consignment is created then a child MR has to be created
   6. Consign the parts which has sufficient bulkqty
   7. Calculate backorderQty and update request detail and item consignment accordingly
  **********************************************************************/
	PROCEDURE gm_sav_initiate_set (
		p_request_id		  IN	   t520_request.c520_request_id%TYPE
	  , p_required_date 	  IN	   VARCHAR2
	  , p_request_source	  IN	   t520_request.c901_request_source%TYPE
	  , p_request_txn_id	  IN	   t520_request.c520_request_txn_id%TYPE
	  , p_set_id			  IN	   t520_request.c207_set_id%TYPE
	  , p_request_for		  IN	   t520_request.c520_request_for%TYPE
	  , p_request_to		  IN	   t520_request.c520_request_to%TYPE
	  , p_request_by_type	  IN	   t520_request.c901_request_by_type%TYPE
	  , p_request_by		  IN	   t520_request.c520_request_by%TYPE
	  , p_ship_to			  IN	   t520_request.c901_ship_to%TYPE
	  , p_ship_to_id		  IN	   t520_request.c520_ship_to_id%TYPE
	  , p_master_request_id   IN	   t520_request.c520_master_request_id%TYPE
	  , p_status_fl 		  IN	   t520_request.c520_status_fl%TYPE
	  , p_userid			  IN	   t520_request.c520_created_by%TYPE
	  , p_purpose			  IN	   t520_request.c901_purpose%TYPE
	  , p_planned_ship_dt     IN       t520_request.c520_planned_ship_date%TYPE
	  , p_out_request_id	  OUT	   t520_request.c520_request_id%TYPE
	  , p_out_consign_id	  OUT	   t504_consignment.c504_consignment_id%TYPE
	)
	AS
		v_partnum	   t205_part_number.c205_part_number_id%TYPE;
		v_bulkqty	   t208_set_details.c208_set_qty%TYPE;
		v_setqty	   t208_set_details.c208_set_qty%TYPE;
		v_child_material_request_bool BOOLEAN := FALSE;
		v_consign_first_time_flag BOOLEAN := FALSE;
		v504_type	   t504_consignment.c504_type%TYPE;
		v_account_id   t520_request.c520_request_to%TYPE;
		v_out_item_consign_id t505_item_consignment.c505_item_consignment_id%TYPE;
		v_out_request_detail_id t521_request_detail.c521_request_detail_id%TYPE;
		v_backorderqty t208_set_details.c208_set_qty%TYPE;
		v_child_material_request_id t520_request.c520_request_id%TYPE;
		v_request_id   t520_request.c520_request_id%TYPE;
		v_required_date t520_request.c520_required_date%TYPE;
        v_comp_date_fmt  varchar2(100);
        v_country_code varchar2(10);
        v_company_id	t1900_company.c1900_company_id%TYPE;
		-- 1. Get Bulk Qty and Set Qty for all parts in the set
		CURSOR cur_set_details
		IS
			SELECT	   t208.c205_part_number_id pnum, DECODE(v_country_code,'en',0, NVL (get_qty_in_bulk (t208.c205_part_number_id), 0)) bulkqty -- For US: to remove the auto allocation, hard coded the bulk qty to 0. For OUS: use function
					 , NVL (t208.c208_set_qty, 0) setqty
				  FROM t208_set_details t208, t205_part_number t205
				 WHERE t208.c207_set_id = p_set_id
				   AND t208.c208_set_qty <> 0
				   AND t208.c208_inset_fl = 'Y'
				   AND t208.c208_void_fl IS NULL
				   AND t208.c205_part_number_id = t205.c205_part_number_id
			  ORDER BY bulkqty DESC
			FOR UPDATE;
	BEGIN
		
		SELECT get_compdtfmt_frm_cntx() INTO v_comp_date_fmt FROM DUAL;
		SELECT 	get_compid_frm_cntx() INTO v_company_id  FROM 	DUAL;
		
		v_country_code:= get_rule_value_by_company('CURRENT_DB','COUNTRYCODE',v_company_id);
		
		--				2. validation. If set id is NULL, throw Error
		IF (p_set_id IS NULL)
		THEN
			raise_application_error (-20915, '');	-- SET ID cant be null while initiating MR
		END IF;

		SELECT DECODE
				   (p_request_for
				  , 40021, 4110
				  , 40022, 4112
				  , p_request_for
				   )   -- we are not using the value 40021/4110 (Consignment) of DMDTP. 20021 - Sales. 40022 - InHouse
			 , DECODE (p_request_for, 40022, '01', NULL)
		  INTO v504_type
			 , v_account_id
		  FROM DUAL;

		  IF p_request_source != 4000122 THEN --( SET PAR )
				SELECT DECODE (p_required_date, NULL, TRUNC (CURRENT_DATE), TO_DATE (p_required_date,v_comp_date_fmt))
				  INTO v_required_date
				  FROM DUAL;
				  
				  -- The Required Date could be null only when the company date format is empty.
				  -- For GOP, we are hard coding the Date Format when the Source is Order Planning.  
				  
				  IF v_required_date IS NULL AND p_request_source = 50616
				  THEN
				  	SELECT DECODE (p_required_date, NULL, TRUNC (CURRENT_DATE), TO_DATE (p_required_date,'MM/DD/YYYY'))
				  		INTO v_required_date
				  	FROM DUAL;
				  END IF;
		  END IF;
		  
		--				3. Creating MR since we have to create it in any case
		gm_pkg_op_request_master.gm_sav_request (p_request_id
											   , v_required_date											   
											   , p_request_source
											   , p_request_txn_id
											   , p_set_id
											   , p_request_for
											   , p_request_to
											   , p_request_by_type
											   , p_request_by
											   , p_ship_to
											   , p_ship_to_id
											   , p_master_request_id
											   , 15
											   , p_userid
											   , p_purpose
											   , p_out_request_id
											   , p_planned_ship_dt
												);
		v_request_id := p_out_request_id;

		IF p_request_to IS NULL AND p_request_source <> '50616'
		THEN
			gm_pkg_cm_shipping_trans.gm_sav_shipping_status_log (v_request_id, 50184, p_userid, 'Initiated');
		END IF;

		--
		FOR loop_set_details IN cur_set_details
		LOOP
			v_partnum	:= loop_set_details.pnum;
			v_bulkqty	:= loop_set_details.bulkqty;
			v_setqty	:= loop_set_details.setqty;

			IF v_bulkqty < 0
			THEN
				v_bulkqty	:= 0;
			END IF;

			-- 4. Since we do orderby on bulkqty and the first bulkqty is > 0, we initiate a consignment
			IF (NOT v_consign_first_time_flag)
			THEN
				IF v_bulkqty > 0
				THEN
					gm_pkg_op_request_master.gm_sav_consignment (p_request_to
															   , NULL
															   , NULL
															   , NULL
															   , NULL
															   , '0'   -- initiated
															   , NULL
															   , v_account_id
															   , p_ship_to
															   , p_set_id
															   , p_ship_to_id
															   , NULL
															   , p_purpose
															   , v504_type
															   , NULL
															   , NULL
															   , NULL
															   , '1'   --Ship Req Flag
															   , '0'
															   , NULL
															   , NULL
															   , NULL
															   , NULL
															   , NULL
															   , p_out_request_id
															   , p_userid
															   , p_out_consign_id
																);
					v_child_material_request_bool := TRUE;

					IF p_request_to IS NULL AND p_request_source <> '50616'
					THEN
						gm_pkg_cm_shipping_trans.gm_sav_shipping_status_log (p_out_consign_id
																		   , 50181
																		   , p_userid
																		   , 'Initiated'
																			);
					END IF;
				ELSE
					-- The request is a backorder since none of the parts have set qty > bulk qty
					UPDATE t520_request
					   SET c520_status_fl = 10
					     , c520_last_updated_by = p_userid
					     , c520_last_updated_date = CURRENT_DATE
					 WHERE c520_request_id = p_out_request_id;
				END IF;
			END IF;

			-- 5. If consignment is created then a child MR has to be created
			IF (v_child_material_request_bool) AND v_bulkqty < v_setqty
			THEN
				gm_pkg_op_request_master.gm_sav_request (NULL
													   , v_required_date
													   , p_request_source
													   , p_request_txn_id
													   , p_set_id
													   , p_request_for
													   , p_request_to
													   , p_request_by_type
													   , p_request_by
													   , p_ship_to
													   , p_ship_to_id
													   , p_out_request_id
													   , 10
													   , p_userid
													   , p_purpose
													   , v_child_material_request_id
														);
				v_child_material_request_bool := FALSE;
				v_request_id := v_child_material_request_id;

				IF p_request_to IS NULL AND p_request_source <> '50616'
				THEN
					gm_pkg_cm_shipping_trans.gm_sav_shipping_status_log (v_child_material_request_id
																	   , 50181
																	   , p_userid
																	   , 'Initiated'
																		);
				END IF;
			END IF;

			-- 6. Consign the parts which has sufficient bulkqty
			IF v_bulkqty >= v_setqty
			THEN
				gm_pkg_op_request_master.gm_sav_consignment_detail (NULL
																  , p_out_consign_id
																  , v_partnum
																  , 'TBE'
																  , v_setqty
																  , get_part_price (v_partnum, 'C')
																  , NULL
																  , NULL
																  , NULL
																  , v_out_item_consign_id
																   );
			END IF;

			IF v_bulkqty < v_setqty
			THEN
				-- 7. Calculate backorderQty and update request detail and item consignment accordingly
				v_backorderqty := v_setqty - v_bulkqty;
				gm_pkg_op_request_master.gm_sav_request_detail (v_request_id
															  , v_partnum
															  , v_backorderqty
															  , v_out_request_detail_id
															   );

				IF v_bulkqty <> 0
				THEN
					gm_pkg_op_request_master.gm_sav_consignment_detail (NULL
																	  , p_out_consign_id
																	  , v_partnum
																	  , 'TBE'
																	  , v_bulkqty
																	  , get_part_price (v_partnum, 'C')
																	  , NULL
																	  , NULL
																	  , NULL
																	  , v_out_item_consign_id
																	   );
				END IF;
			END IF;

			v_consign_first_time_flag := TRUE;
		END LOOP;
		
		IF (p_out_consign_id IS NOT NULL)
		THEN
			--When ever the child is updated, the consignment table has to be updated ,so ETL can Sync it to GOP.
			UPDATE t504_consignment
			   SET c504_last_updated_by   = p_userid
			     , c504_last_updated_date = CURRENT_DATE
			 WHERE c504_consignment_id = p_out_consign_id;	
		END IF;
	END gm_sav_initiate_set;

-------------------------

	---------------------------
/**********************************************************************
   * Description : Procedure to reconfigure set
   * Author 	 : Rakhi Gandhi
  **********************************************************************/
	PROCEDURE gm_sav_reconfig_part_to (
		p_consign_id		  IN	   t504_consignment.c504_consignment_id%TYPE
	  , p_part_to_reconfig	  IN	   VARCHAR2
	  , p_backorder_present   IN	   VARCHAR2
	  , p_insert_backorder	  IN	   VARCHAR2
	  , p_user				  IN	   t520_request.c520_created_by%TYPE
	  , p_addpart			  IN	   VARCHAR2
	  , p_void_fl			  OUT	   VARCHAR2
	  , p_out_txn_id		  OUT	   VARCHAR2
	)
	AS
		v_request_id   t520_request.c520_request_id%TYPE;
		v_bo_request_id t520_request.c520_request_id%TYPE;
		v_strlen	   NUMBER := NVL (LENGTH (p_part_to_reconfig), 0);
		v_string	   VARCHAR2 (30000) := p_part_to_reconfig;
		v_pnum		   t205_part_number.c205_part_number_id%TYPE;
		v_qty		   NUMBER;
		v_control	   VARCHAR2 (20);
		v_action	   VARCHAR2 (20);
		v_releaseto    VARCHAR2 (20);
		v_substring    VARCHAR2 (1000);
		v_msg		   VARCHAR2 (1000);
		v_request_source t520_request.c901_request_source%TYPE;
		v_request_txn_id t520_request.c520_request_txn_id%TYPE;
		v_set_id	   t520_request.c207_set_id%TYPE;
		v_request_for  t520_request.c520_request_for%TYPE;
		v_request_to   t520_request.c520_request_to%TYPE;
		v_request_by_type t520_request.c901_request_by_type%TYPE;
		v_request_by   t520_request.c520_request_by%TYPE;
		v_ship_to	   t520_request.c901_ship_to%TYPE;
		v_ship_to_id   t520_request.c520_ship_to_id%TYPE;
		v_created_date t520_request.c520_created_date%TYPE;
		v_prod_req_id  t520_request.c525_product_request_id%TYPE;
		v_out_consign  t504_consignment.c504_consignment_id%TYPE;
		v_consign_status_fl t504_consignment.c504_status_fl%TYPE;
		v_verify_fl    t504_consignment.c504_verify_fl%TYPE;
		v_settype	   VARCHAR2 (1000);
		v_reconfigure_log_id t530_reconfigure_log.c530_reconfigure_log_id%TYPE;
		v_out_reconfig_log_detail_id t531_reconfigure_log_detail.c531_reconfigure_log_detail_id%TYPE;
		v_item_exists  NUMBER := 0;
		v_void_fl	   t504_consignment.c504_void_fl%TYPE;
		v_errmsg	   VARCHAR2 (100);
		v_bs_id 	   VARCHAR2 (20);
		v_bq_id 	   VARCHAR2 (20);
		v_deptid	   t101_user.c901_dept_id%TYPE;
		v_cntrl_flg		VARCHAR2(20);
		v_psfgstr		VARCHAR2(4000):='';
		v_psrwstr		VARCHAR2(4000):='';
		v_comments		VARCHAR2(2000) := '';
		v_id			VARCHAR2(20) := '';
		v_txnsid		VARCHAR2(2000) := '';
		v_shipto		t504_consignment.C504_SHIP_TO%TYPE;
		v_uptfl         char(1);
		v_company_id    t504_consignment.c1900_company_id%TYPE;
	BEGIN
		-- If the consignment is not in wip/built set/initiated throw error.
		SELECT	   c504_status_fl, c504_verify_fl, c520_request_id, c504_void_fl, c207_set_id, c504_ship_to, c1900_company_id 
			  INTO v_consign_status_fl, v_verify_fl, v_request_id, v_void_fl, v_set_id, v_shipto, v_company_id
			  FROM t504_consignment
			 WHERE c504_consignment_id = p_consign_id
		FOR UPDATE;
		
		v_company_id := NVL(v_company_id,get_compid_frm_cntx());

		IF (v_consign_status_fl > 2)   --todo
		THEN
			raise_application_error (-20916, '');	--status err
		END IF;

		IF v_void_fl = 'Y'
		THEN
			raise_application_error (-20917, '');
		END IF;

		IF v_consign_status_fl >= 1
		THEN
			SELECT t101.c901_dept_id
			  INTO v_deptid
			  FROM t101_user t101
			 WHERE c101_user_id = p_user;
			
			v_uptfl := gm_pkg_cm_shipping_trans.get_user_security_event_status(p_user, 'RECONFIG_ACCESS');
			
			
			IF (v_deptid = 2001 OR v_uptfl = 'Y') AND v_set_id IS NULL   --need to pass if custservice and item consg
			THEN
				v_set_id	:= '';
		 	ELSIF  v_uptfl != 'Y'
			THEN
				-- Error message is Sorry, this transaction cannot be voided.Contact Logistics for changes.
				raise_application_error (-20838, ''); 
			END IF;
		END IF;

		IF (v_consign_status_fl = 2 AND v_verify_fl = 1 AND (v_set_id IS NOT NULL OR v_set_id != ''))
		THEN
			v_settype	:= 'Builtset';
		ELSE
			v_settype	:= '';
		END IF;

		-- Check if parts are in backorder if so get the backorder entry
		BEGIN
			SELECT c520_request_id
			  INTO v_bo_request_id
			  FROM t520_request t520
			 WHERE (c520_master_request_id = v_request_id OR c520_request_id = v_request_id)
			   AND t520.c520_status_fl = '10'	-- Backorder 10
			   AND t520.c520_void_fl IS NULL;
		-- if no data and backorder, create new MR with backorder status
		--PBUG-4036 adding the product request id column to get the request id in paperwork ofPMT-45058
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				IF p_backorder_present = '1'
				THEN
					SELECT t520_request.c901_request_source, t520_request.c207_set_id, t520_request.c520_request_for
						 , t520_request.c520_request_to, t520_request.c901_request_by_type
						 , t520_request.c520_request_by, t520_request.c901_ship_to, t520_request.c520_ship_to_id
						 , c520_created_date,c525_product_request_id
					  INTO v_request_source, v_set_id, v_request_for
						 , v_request_to, v_request_by_type
						 , v_request_by, v_ship_to, v_ship_to_id
						 , v_created_date,v_prod_req_id
					  FROM t520_request
					 WHERE c520_request_id = v_request_id AND c520_void_fl IS NULL;
					 
				 
					gm_pkg_op_request_master.gm_sav_request (NULL
														   , CURRENT_DATE
														   , v_request_source
														   , p_consign_id
														   , v_set_id
														   , v_request_for
														   , v_request_to
														   , v_request_by_type
														   , v_request_by
														   , v_ship_to
														   , v_ship_to_id
														   , v_request_id
														   , '10'
														   , p_user
														   , NULL
														   , v_bo_request_id
														   ,NULL
														   ,v_prod_req_id
															);
					gm_sav_cpy_req_shipping (v_request_id, v_bo_request_id, 50184, p_user);
				END IF;
		END;

			--
		--log reconfig data
		gm_pkg_op_log_request.gm_log_reconfig_request (90808
													 , p_consign_id
													 , v_bo_request_id
													 , p_user
													 , v_reconfigure_log_id
													  );

		-----

		-- Loop through the parts and reconfigure them
		WHILE INSTR (v_string, '|') <> 0
		LOOP
			v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
			v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
			v_pnum		:= NULL;
			v_qty		:= NULL;
			v_control	:= NULL;
			v_action	:= NULL;
			v_releaseto := NULL;
			v_pnum		:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_qty		:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_control	:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_action	:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_releaseto := v_substring;
			--
			
			IF v_control IS NOT NULL AND v_control <> 'TBE'
			THEN
				v_cntrl_flg := 'Y';
			END IF;
			
			IF v_releaseto = '90805' 
			THEN
				v_psfgstr := v_psfgstr || v_pnum || '^' || v_control || '^' || v_qty || '|';
			ELSIF v_releaseto = '90822'
			THEN
				v_psrwstr := v_psrwstr || v_pnum || '^' || v_control || '^' || v_qty || '|'; 
			END IF;

			gm_pkg_op_process_request.gm_sav_reconfig_cons_request (p_consign_id
																  , v_bo_request_id
																  , v_pnum
																  , v_control
																  , v_qty
																  , v_action
																  , v_releaseto
																  , v_settype
																  , p_user
																  , p_out_txn_id
																  , v_bs_id
																  , v_bq_id
																   );
			gm_pkg_op_log_request.gm_log_reconfig_request_detail (v_reconfigure_log_id
																, v_pnum
																, v_control
																, v_qty
																, v_action
																, v_out_reconfig_log_detail_id
																 );
		END LOOP;
		IF v_cntrl_flg = 'Y' AND v_set_id IS NULL AND v_shipto IS NULL --v_shipto is null for the CN which was released to shipping and rollbacked.
		THEN
			IF v_psfgstr IS NOT NULL
			THEN
					v_comments := 'Pending shipping to shelf transaction for Item Consignment: ' || p_consign_id;
					gm_pkg_op_request_master.gm_sav_inhouse_txn(
      					v_comments,
      					3,
      					100408,	-- Transaction Purpose
      					100406,	-- PSFG
      					p_consign_id,
      					4110,
      					NULL,
      					NULL,
      					p_user,
      					v_id
      				);
			
					gm_pkg_op_request_master.gm_sav_inhouse_txn_string(
																v_id,
																v_psfgstr,
																p_user
															);	
                          gm_pkg_allocation.gm_ins_invpick_assign_detail(100407,v_id,p_user);  --- to insert PSFG entry in t5050
                         UPDATE t412_inhouse_transactions 
							   SET c1900_company_id=NVL(v_company_id,c1900_company_id)
							     , c412_last_updated_by   = p_user
							     , c412_last_updated_date = CURRENT_DATE
							 WHERE c412_inhouse_trans_id  = v_id;
				v_txnsid := v_id || ',' ;		
		END IF;
		
		IF v_psrwstr IS NOT NULL
		THEN
			
			v_comments := 'Pending shipping to RW transaction for Item Consignment: ' || p_consign_id;
				gm_pkg_op_request_master.gm_sav_inhouse_txn(
  					v_comments,
  					3, 
  					4000117,-- Transaction Purpose
  					56028,	-- PSRW
  					p_consign_id,
  					4110,
  					NULL,
  					NULL,
  					p_user,
  					v_id
  				);
		
				gm_pkg_op_request_master.gm_sav_inhouse_txn_string(
															v_id,
															v_psrwstr,
															p_user
														);                            
    gm_pkg_allocation.gm_ins_invpick_assign_detail(56088,v_id,p_user);  --- to insert PSRW entry in t5050        
   						 UPDATE t412_inhouse_transactions 
							   SET c1900_company_id=NVL(v_company_id,c1900_company_id)
							     , c412_last_updated_by   = p_user
							     , c412_last_updated_date = CURRENT_DATE
							 WHERE c412_inhouse_trans_id  = v_id;                
				v_txnsid :=  v_txnsid || v_id;
		END IF;
		--update control number table records and update corresponding location qty.
		gm_sav_control_number_dtl(p_consign_id,v_psfgstr||'|'||v_psrwstr,'Y',p_user);
		
	ELSIF v_cntrl_flg = 'Y'AND v_shipto IS NOT NULL AND (v_psrwstr IS NOT NULL OR v_psfgstr IS NOT NULL)
	   THEN
	    --update control number table records
		gm_sav_control_number_dtl(p_consign_id,v_psfgstr||'|'||v_psrwstr,'N',p_user);
	END IF;
		IF v_txnsid IS NOT NULL
		THEN
			p_out_txn_id := p_out_txn_id || '|' || v_txnsid;
		END IF;
		--Process Add Parts section
		IF (p_insert_backorder = '1')
		THEN
			gm_sav_addpart_to_request (p_consign_id, p_addpart, p_user, v_reconfigure_log_id, v_errmsg);
		END IF;

		--update lastupdated date for consignment
		UPDATE t504_consignment
		   SET c504_last_updated_date = CURRENT_DATE
			 , c504_last_updated_by = p_user
		 WHERE c504_consignment_id = p_consign_id;

		gm_pkg_op_process_request.gm_sav_chk_to_void_consignmnet (p_consign_id, p_user, p_void_fl, v_action);
		
		IF v_bs_id IS NOT NULL
		THEN
			--code for allocation
			gm_pkg_allocation.gm_ins_invpick_assign_detail (93328, v_bs_id, p_user);
		END IF;		
	END gm_sav_reconfig_part_to;
 /**********************************************************************
   * Description : Procedure to update control number detail and location qty
   * Author 	 : Gopinathan
  **********************************************************************/
	PROCEDURE gm_sav_control_number_dtl(
		p_ref_id		      IN	   T5055_Control_Number.C5055_REF_ID%TYPE
	  , p_rem_string		  IN       VARCHAR2
	  , p_loc_qty_upd_fl	  IN 	   CHAR
	  , p_user_id			  IN	   T5055_Control_Number.C5055_LAST_UPDATED_BY%TYPE
	)
	AS
		v_string		VARCHAR2(2000) := p_rem_string;
	   	v_substring		VARCHAR2(2000);
	   	v_pnum      	T5055_Control_Number.C205_PART_NUMBER_ID%TYPE;
	    v_qty           T5055_Control_Number.C5055_QTY%TYPE;
	    v_cnum   		T5055_Control_Number.C5055_CONTROL_NUMBER%TYPE;
	    v_stringtemp    VARCHAR2 (4000)                            := '';
	BEGIN
	   WHILE INSTR(v_string,'|') <> 0
	   LOOP
	   		v_substring := substr(v_string,0,INSTR(v_string,'|') - 1);
	   		v_string := substr(v_string,INSTR(v_string,'|') + 1);
	   		
	   		v_pnum	:= null;
	   		v_qty	:= null;
	   		v_cnum	:= null;
	   		
	   		v_pnum := substr(v_substring,0,INSTR(v_substring,'^') - 1); 
	   		v_substring := substr(v_substring,INSTR(v_substring,'^') + 1);
	   		v_cnum	:= substr(v_substring,0,INSTR(v_substring,'^') - 1);
	   		v_substring := substr(v_substring,INSTR(v_substring,'^') + 1);
	   		v_qty	:=	TO_NUMBER(v_substring);		
			
	   		FOR cur IN(	SELECT C5055_CONTROL_NUMBER_ID ctrlnumid, C5055_QTY allocqty, c5052_location_id loc_id
						  FROM T5055_Control_Number
						 WHERE C5055_REF_ID = p_ref_id
						   AND C205_PART_NUMBER_ID = v_pnum
						   AND C5055_CONTROL_NUMBER = v_cnum
						   ORDER BY C5055_QTY)
			LOOP
				IF cur.allocqty > v_qty
				THEN
					IF cur.loc_id IS NOT NULL
	                THEN
	                     v_stringtemp :=
	                           v_stringtemp
	                        || v_pnum
	                        || ','
	                        || v_qty
	                        || ','
	                        || cur.loc_id
	                        || '|';
	                END IF;
	                
					UPDATE T5055_Control_Number 
					   SET C5055_QTY = cur.allocqty - v_qty
					     , c5055_last_updated_by = p_user_id
					     , c5055_last_updated_date = CURRENT_DATE
					 WHERE C5055_CONTROL_NUMBER_ID = cur.ctrlnumid;
					 EXIT;
				ELSIF cur.allocqty < v_qty
				THEN
					IF cur.loc_id IS NOT NULL
	                THEN
	                     v_stringtemp :=
	                           v_stringtemp
	                        || v_pnum
	                        || ','
	                        || cur.allocqty
	                        || ','
	                        || cur.loc_id
	                        || '|';
	                END IF;
					DELETE FROM T5055_Control_Number
						  WHERE C5055_CONTROL_NUMBER_ID = cur.ctrlnumid;
					v_qty := v_qty - cur.allocqty;
				ELSIF cur.allocqty = v_qty
				THEN
					IF cur.loc_id IS NOT NULL
	                THEN
	                     v_stringtemp :=
	                           v_stringtemp
	                        || v_pnum
	                        || ','
	                        || cur.allocqty
	                        || ','
	                        || cur.loc_id
	                        || '|';
	                END IF;				
				
					DELETE FROM T5055_Control_Number
						  WHERE C5055_CONTROL_NUMBER_ID = cur.ctrlnumid;
					EXIT;
				END IF;
			
			END LOOP;
		END LOOP;
		
		IF p_loc_qty_upd_fl = 'Y' THEN
		      gm_pkg_op_inv_scan.gm_upd_inventory_loc_qty (p_ref_id,
                                                   v_stringtemp,
                                                   93326,
                                                   p_user_id
                                                  );
			  
              --Need to update update_inv_fl as null. Because, we updated partial qty
			  UPDATE t5055_control_number
		         SET c5055_update_inv_fl = NULL,
		             c5055_last_updated_date = CURRENT_DATE,
		             c5055_last_updated_by = p_user_id
		       WHERE c5055_ref_id = p_ref_id
		         AND c901_type = 93343
		         AND c5055_void_fl IS NULL
		         AND c5055_update_inv_fl IS NOT NULL;                                                  
		END IF;
	END gm_sav_control_number_dtl;
--
/**********************************************************************
   * Description : Procedure to check and void the consignment if all the parts
				   associated to the consignment are in back order
   * Author 	 : VPrasath
  **********************************************************************/
	PROCEDURE gm_sav_chk_to_void_consignmnet (
		p_consign_id   IN		t504_consignment.c504_consignment_id%TYPE
	  , p_user		   IN		t520_request.c520_created_by%TYPE
	  , p_void_fl	   OUT		VARCHAR2
	  , p_action	   IN       	VARCHAR2 default null
	)
	AS
		v_item_exists  NUMBER := 0;
		v_request_id   t520_request.c520_request_id%TYPE;
	BEGIN
		SELECT COUNT (c504_consignment_id)
		  INTO v_item_exists
		  FROM t505_item_consignment
		 WHERE c504_consignment_id = p_consign_id;

		IF v_item_exists = 0
		THEN
			SELECT c520_request_id
			  INTO v_request_id
			  FROM t504_consignment t504
			 WHERE t504.c504_consignment_id = p_consign_id;

			-- Since all the parts are back ordered get all the rows from child Request in to parent request and delete the details of child request
			INSERT INTO t521_request_detail (c521_request_detail_id,c520_request_id,c521_qty,c205_part_number_id)
				SELECT s521_request_detail.NEXTVAL, v_request_id, t521.c521_qty, t521.c205_part_number_id
				  FROM t520_request t520, t521_request_detail t521
				 WHERE t520.c520_request_id = t521.c520_request_id
				   AND t520.c520_master_request_id = v_request_id
				   AND t520.c520_void_fl IS NULL;

			DELETE FROM t521_request_detail
				  WHERE c520_request_id IN (
							SELECT t521.c520_request_id
							  FROM t520_request t520, t521_request_detail t521
							 WHERE t520.c520_request_id = t521.c520_request_id
							   AND t520.c520_master_request_id = v_request_id
							   AND t520.c520_void_fl IS NULL);

			-- Void the Child Request
			UPDATE t520_request t520
			   SET t520.c520_void_fl = 'Y'
				 , t520.c520_last_updated_by = p_user
				 , t520.c520_last_updated_date = CURRENT_DATE
			 WHERE t520.c520_master_request_id = v_request_id;

			-- Back Order the Request
			--(90804)Realease and remove
			UPDATE t520_request t520
			   SET t520.c520_status_fl = 10
			     , t520.c520_void_fl = DECODE(p_action,90804,'Y',c520_void_fl)
				 , t520.c520_last_updated_by = p_user
				 , t520.c520_last_updated_date = CURRENT_DATE
			 WHERE t520.c520_request_id = v_request_id;

			-- Void the Consignment
			UPDATE t504_consignment
			   SET c504_void_fl = 'Y'
				 , c520_request_id = ''
				 , c504_last_updated_date = CURRENT_DATE
				 , c504_last_updated_by = p_user
			 WHERE c504_consignment_id = p_consign_id;

			gm_pkg_common_cancel.gm_void_shipping (p_consign_id, p_user, 50181);
			p_void_fl	:= 'Y';
		END IF;
	END gm_sav_chk_to_void_consignmnet;

----------------------------
/**********************************************************************
   * Description : Procedure to reconfigure set cosign to request
   * Author 	 : Rakhi Gandhi
  **********************************************************************/
	PROCEDURE gm_sav_reconfig_cons_request (
		p_consign_id		  IN	   t504_consignment.c504_consignment_id%TYPE
	  , p_bo_request_id 	  IN	   t520_request.c520_request_id%TYPE
	  , p_part_num			  IN	   t205_part_number.c205_part_number_id%TYPE
	  , p_control_num		  IN	   t505_item_consignment.c505_control_number%TYPE
	  , p_qty				  IN	   VARCHAR2
	  , p_action			  IN	   VARCHAR2
	  , p_release_to		  IN	   VARCHAR2
	  , p_settype			  IN	   VARCHAR2
	  , p_user				  IN	   t520_request.c520_created_by%TYPE
	  , p_out_new_inhtxn_id   IN OUT   VARCHAR2
	  , p_new_bs_inhtxn_id	  IN OUT   VARCHAR2
	  , p_new_bq_inhtxn_id	  IN OUT   VARCHAR2
	)
	AS
--todo
		v_item_consignment_id t505_item_consignment.c505_item_consignment_id%TYPE;
		v_allocqty	   t505_item_consignment.c505_item_qty%TYPE;
		v_out_request_detail t521_request_detail.c521_request_detail_id%TYPE;
		v_party_id	   t101_party.c101_party_id%TYPE;
		v_message	   VARCHAR2 (2);
		v_bs_first_time_flag BOOLEAN := FALSE;
		v_bq_first_time_flag BOOLEAN := FALSE;
		v_new_txn_id	VARCHAR2(20);
		v_company_id     t504_consignment.c1900_company_id%TYPE;
	BEGIN
--case1
		--IF p_control_num IS NOT NULL		 --'TBE' -- verify one more time
		BEGIN
			SELECT	 c505_item_consignment_id, c505_item_qty, c1900_company_id
				INTO v_item_consignment_id, v_allocqty, v_company_id 
				FROM t505_item_consignment t505, t504_consignment t504
			   WHERE t505.c504_consignment_id = p_consign_id
				 AND t504.c504_consignment_id = t505.c504_consignment_id
				 AND t504.c504_void_fl is null
			   	 AND t505.c205_part_number_id = p_part_num
				 AND NVL(TRIM(c505_control_number),'-999') = NVL(TRIM(p_control_num),'-999')   -- added trim remove extra space to control number used to PC-3860 
				 AND c505_item_qty >= p_qty
				 AND ROWNUM = 1
			ORDER BY c505_item_qty;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				raise_application_error (-20677, '');	-- No data found.
		END;
		
		v_company_id := NVL(v_company_id,get_compid_frm_cntx());

		IF v_allocqty > p_qty
		THEN
			UPDATE t505_item_consignment t505
			   SET t505.c505_item_qty = v_allocqty - p_qty
			 WHERE c505_item_consignment_id = v_item_consignment_id;
			 
			--Whenever the detail is updated, the consignment table has to be updated ,so ETL can Sync it to GOP.
		 	UPDATE t504_consignment
			   SET c504_last_updated_by = p_user
		         , c504_last_updated_date = CURRENT_DATE
		  	 WHERE C504_Consignment_Id = p_consign_id;
		END IF;

		IF v_allocqty = p_qty
		THEN
			DELETE FROM t505_item_consignment t505
				  WHERE c505_item_consignment_id = v_item_consignment_id;
		    
			--Whenever the detail is updated, the consignment table has to be updated ,so ETL can Sync it to GOP.
		 	UPDATE t504_consignment
			   SET c504_last_updated_by = p_user
		         , c504_last_updated_date = CURRENT_DATE
		  	 WHERE C504_Consignment_Id = p_consign_id;				  
		END IF;

		IF v_allocqty < p_qty
		THEN
			raise_application_error (-20913, '');	-- reconfigure quantity cannot be greater than set qty
		END IF;

		-- ..
		IF (p_action = '90803')   --Release and BackOrder
		THEN
			UPDATE t521_request_detail
			   SET c521_qty = c521_qty + p_qty
			 WHERE c520_request_id = p_bo_request_id AND c205_part_number_id = p_part_num;

			IF (SQL%ROWCOUNT = 0)
			THEN
				gm_pkg_op_request_master.gm_sav_request_detail (p_bo_request_id
															  , p_part_num
															  , p_qty
															  , v_out_request_detail
															   );
			END IF;
			
			--When ever the detail is updated, the request table has to be updated ,so ETL can Sync it to GOP.
 			UPDATE t520_request
			   SET c520_last_updated_by = p_user
  				 , c520_last_updated_date = CURRENT_DATE
  			 WHERE C520_request_Id = p_bo_request_id; 
		END IF;

		-- END IF;
		IF (p_settype = 'Builtset')
		THEN
			IF p_release_to = '90805'
			THEN
				IF (p_new_bs_inhtxn_id IS NULL)
				THEN
					p_new_bs_inhtxn_id := get_next_consign_id ('50161');
					--gm_procedure_log ('p_new_bs_inhtxn_id', p_new_bs_inhtxn_id);
					gm_save_inhouse_item_consign (p_new_bs_inhtxn_id
												, 50161
												, NULL
												, 50625
												, p_user
												, NULL
												, p_user
												, NULL
												, v_message
												 );

					--
					UPDATE t412_inhouse_transactions
					   SET c1900_company_id = NVL(v_company_id,c1900_company_id)
					     , c412_ref_id = p_consign_id
					     , c412_last_updated_by = p_user
						 , c412_last_updated_date = CURRENT_DATE					   
					 WHERE c412_inhouse_trans_id = p_new_bs_inhtxn_id;
				END IF;

				gm_pkg_op_inhouse_master.gm_sav_inhouse_trans_items (p_new_bs_inhtxn_id
																   , p_part_num
																   , p_qty
																   , p_control_num
																   , get_part_price (p_part_num, 'C')
																	);
			ELSIF p_release_to = '90811'
			THEN
				IF (p_new_bq_inhtxn_id IS NULL)
				THEN
					p_new_bq_inhtxn_id := get_next_consign_id ('50162');
					--gm_procedure_log ('p_new_bq_inhtxn_id', p_new_bq_inhtxn_id);
					gm_save_inhouse_item_consign (p_new_bq_inhtxn_id
												, 50162
												, NULL
												, 50625
												, p_user
												, NULL
												, p_user
												, NULL
												, v_message
												 );

					--
					UPDATE t412_inhouse_transactions
					   SET c1900_company_id = NVL(v_company_id,c1900_company_id)
					     , c412_ref_id = p_consign_id
					   	 , c412_last_updated_by = p_user
						 , c412_last_updated_date = CURRENT_DATE
					 WHERE c412_inhouse_trans_id = p_new_bq_inhtxn_id;

					v_bq_first_time_flag := TRUE;
				END IF;

				gm_pkg_op_inhouse_master.gm_sav_inhouse_trans_items (p_new_bq_inhtxn_id
																   , p_part_num
																   , p_qty
																   , p_control_num
																   , get_part_price (p_part_num, 'C')
																	);
			END IF;

			p_out_new_inhtxn_id := p_new_bs_inhtxn_id || ' , ' || p_new_bq_inhtxn_id;
		--gm_procedure_log ('p_out_new_inhtxn_id -- ', p_out_new_inhtxn_id);
		/*ELSE
		
			IF p_release_to = '90805'
			THEN
					v_new_txn_id := get_next_consign_id ('100406'); -- PSFG
					--gm_procedure_log ('p_new_bq_inhtxn_id', p_new_bq_inhtxn_id);
					gm_save_inhouse_item_consign (v_new_txn_id
												, 50162
												, NULL
												, 50625
												, p_user
												, NULL
												, p_user
												, NULL
												, v_message
												 );
					--
					UPDATE t412_inhouse_transactions
					   SET c412_ref_id = p_consign_id
					 WHERE c412_inhouse_trans_id = v_new_txn_id;
						 
					gm_pkg_op_inhouse_master.gm_sav_inhouse_trans_items (v_new_txn_id
																   , p_part_num
																   , p_qty
																   , p_control_num
																   , get_part_price (p_part_num, 'C')
																	);
			ELSIF p_release_to = '90822'
			THEN
					v_new_txn_id := get_next_consign_id ('56028'); -- PSRW
					--gm_procedure_log ('p_new_bq_inhtxn_id', p_new_bq_inhtxn_id);
					gm_save_inhouse_item_consign (v_new_txn_id
												, 50162
												, NULL
													, 50625
												, p_user
												, NULL
												, p_user
												, NULL
												, v_message
												 );
					--
					UPDATE t412_inhouse_transactions
					   SET c412_ref_id = p_consign_id
					 WHERE c412_inhouse_trans_id = v_new_txn_id;
						 
					gm_pkg_op_inhouse_master.gm_sav_inhouse_trans_items (v_new_txn_id
																   , p_part_num
																   , p_qty
																   , p_control_num
																   , get_part_price (p_part_num, 'C')
																	);
																		END IF;*/

		
		END IF;
	END gm_sav_reconfig_cons_request;
  
	/**********************************************************************
	 * Description : Procedure to reconfigure set - add parts section
	 * Author	   : Rakhi Gandhi
	 **********************************************************************/
	PROCEDURE gm_sav_addpart_to_request (
		p_consign_id		   IN		t504_consignment.c504_consignment_id%TYPE
	  , p_part_to_reconfig	   IN		VARCHAR2
	  , p_user				   IN		t520_request.c520_created_by%TYPE
	  , p_reconfigure_log_id   IN		t530_reconfigure_log.c530_reconfigure_log_id%TYPE
	  , p_errmsg			   OUT		VARCHAR2
	)
	AS
		v_request_id   t520_request.c520_request_id%TYPE;
		v_bo_request_id t520_request.c520_request_id%TYPE;
		v_strlen	   NUMBER := NVL (LENGTH (p_part_to_reconfig), 0);
		v_string	   VARCHAR2 (30000) := p_part_to_reconfig;
		v_pnum		   t205_part_number.c205_part_number_id%TYPE;
		v_qty		   NUMBER;
		v_control	   VARCHAR2 (20);
		v_action	   VARCHAR2 (20);
		v_releaseto    VARCHAR2 (20);
		v_substring    VARCHAR2 (1000);
		v_msg		   VARCHAR2 (1000);
		v_bulkqty	   NUMBER;
		v_addpartqty   NUMBER;
		v_sav_in_qty   NUMBER;
		v_tempaddpartqty NUMBER;
		v_bo_qty	   NUMBER := 0;
		v_setqty	   NUMBER;
		v_request_source t520_request.c901_request_source%TYPE;
		v_request_txn_id t520_request.c520_request_txn_id%TYPE;
		v_set_id	   t520_request.c207_set_id%TYPE;
		v_request_for  t520_request.c520_request_for%TYPE;
		v_request_to   t520_request.c520_request_to%TYPE;
		v_request_by_type t520_request.c901_request_by_type%TYPE;
		v_request_by   t520_request.c520_request_by%TYPE;
		v_ship_to	   t520_request.c901_ship_to%TYPE;
		v_ship_to_id   t520_request.c520_ship_to_id%TYPE;
		v_created_date t520_request.c520_created_date%TYPE;
		v_out_consign  t504_consignment.c504_consignment_id%TYPE;
		v_verify_fl    t504_consignment.c504_verify_fl%TYPE := 0;
		v_settype	   VARCHAR2 (1000);
		v_item_consignment_id t505_item_consignment.c505_item_consignment_id%TYPE;
		v_item_cons_qty NUMBER;
		v_out_item_consign_id t505_item_consignment.c505_item_consignment_id%TYPE;
		v_out_request_detail t521_request_detail.c521_request_detail_id%TYPE;
		v_out_reconfig_log_detail_id t531_reconfigure_log_detail.c531_reconfigure_log_detail_id%TYPE;
	BEGIN
		-- Get MR from consignment table - Master MR ID
		SELECT c520_request_id, c504_verify_fl
		  INTO v_request_id, v_verify_fl
		  FROM t504_consignment
		 WHERE c504_consignment_id = p_consign_id;	 -- FOR UPDATE;

		-- Check if parts are in backorder if so get the backorder entry
		BEGIN
			SELECT c520_request_id
			  INTO v_bo_request_id
			  FROM t520_request t520
			 WHERE (c520_master_request_id = v_request_id OR c520_request_id = v_request_id)
			   AND t520.c520_status_fl = '10'	-- Backorder 10
			   AND t520.c520_void_fl IS NULL;
		-- if no data and backorder, create new MR with backorder status
		--
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				v_bo_request_id := NULL;
		END;

		-- Loop through the parts and reconfigure them - if bulk available, add to item-consignment with TBE control number else backorder it.
		WHILE INSTR (v_string, '|') <> 0
		LOOP
			v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
			v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
			v_pnum		:= NULL;
			v_qty		:= NULL;
			v_bulkqty	:= NULL;
			v_addpartqty := NULL;
			v_setqty	:= NULL;
			v_pnum		:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_bulkqty	:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_setqty	:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_addpartqty := v_substring;
			--
			v_sav_in_qty := v_addpartqty;

			--

			----- get bulk from database
			SELECT	   DECODE (v_verify_fl, 1, 0, get_qty_in_bulk (t205.c205_part_number_id))
				  INTO v_bulkqty
				  FROM t205_part_number t205
				 WHERE t205.c205_part_number_id = v_pnum
			FOR UPDATE;

			IF v_bulkqty < 0
			THEN   -- to see if bo is required
				v_bulkqty	:= 0;
			END IF;

			IF v_addpartqty > v_bulkqty
			THEN   --bo required
				IF v_bo_request_id IS NULL
				THEN   -- no bo mr id created yet, create new and put it in v_bo_request_id
					SELECT t520_request.c901_request_source, t520_request.c207_set_id, t520_request.c520_request_for
						 , t520_request.c520_request_to, t520_request.c901_request_by_type
						 , t520_request.c520_request_by, t520_request.c901_ship_to, t520_request.c520_ship_to_id
						 , c520_created_date
					  INTO v_request_source, v_set_id, v_request_for
						 , v_request_to, v_request_by_type
						 , v_request_by, v_ship_to, v_ship_to_id
						 , v_created_date
					  FROM t520_request
					 WHERE c520_request_id = v_request_id AND c520_void_fl IS NULL;

					gm_pkg_op_request_master.gm_sav_request (NULL
														   , CURRENT_DATE
														   , v_request_source
														   , p_consign_id
														   , v_set_id
														   , v_request_for
														   , v_request_to
														   , v_request_by_type
														   , v_request_by
														   , v_ship_to
														   , v_ship_to_id
														   , v_request_id
														   , '10'
														   , p_user
														   , NULL
														   , v_bo_request_id
															);
				END IF;
			END IF;

			IF v_addpartqty > v_bulkqty
			THEN
				--1. backorder the needed quantity
				v_bo_qty	:= v_addpartqty - v_bulkqty;

				-- verify if this operation would make the total qty in cons exceed setqty
				IF v_bo_qty + gm_pkg_op_request_summary.get_total_part_qty_in_cons (p_consign_id, v_pnum) > v_setqty
				THEN
					raise_application_error (-20914, '');	-- total quantity (backorder + allocated) cannot exceed setQty
				END IF;

				 --When ever the detail is updated, the request table has to be updated ,so ETL can Sync it to GOP.
			    UPDATE T520_request
			       SET c520_last_updated_by    = p_user
			         , c520_last_updated_date = CURRENT_DATE
			  	 WHERE C520_request_Id = v_bo_request_id;	
				
				UPDATE t521_request_detail
				   SET c521_qty = c521_qty + v_bo_qty
				 WHERE c520_request_id = v_bo_request_id AND c205_part_number_id = v_pnum;

				 IF (SQL%ROWCOUNT = 0)
				THEN
					gm_pkg_op_request_master.gm_sav_request_detail (v_bo_request_id
																  , v_pnum
																  , v_bo_qty
																  , v_out_request_detail
																   );
				END IF;

				--2. Set the quantity that will now be put in alloc table
				v_addpartqty := v_bulkqty;
			END IF;

			--backorder over

			--3. Add quantity in alloc table
			IF (v_addpartqty > 0)	-- add to alloc table
			THEN
				v_item_cons_qty := 0;
				v_item_consignment_id := NULL;

				--IF v_item_cons_qty + v_addpartqty + gm_pkg_op_request_summary.get_part_backorder_qty (p_consign_id, v_pnum) > v_setqty
				IF v_addpartqty + gm_pkg_op_request_summary.get_total_part_qty_in_cons (p_consign_id, v_pnum) >
																											   v_setqty
				THEN
					raise_application_error (-20914, '');	-- quantity to allocate in item_consignment cannot exceed setQty
				END IF;

				-- call proc to save gm_pkg_op_request_master.gm_sav_consignment_detail
				gm_pkg_op_request_master.gm_sav_consignment_detail (v_item_consignment_id
																  , p_consign_id
																  , v_pnum
																  , 'TBE'
																  --, (v_item_cons_qty + v_addpartqty)
				,													(v_addpartqty)
																  , get_part_price (v_pnum, 'C')
																  , NULL
																  , NULL
																  , NULL
																  , v_out_item_consign_id
																   );
			END IF;

--						GM_PKG_OP_PROCESS_REQUEST.GM_SAV_RECONFIG_CONS_REQUEST( p_consign_id, v_bo_request_id, v_PNum, v_Control, v_Qty, v_Action, v_ReleaseTo, v_settype );
			gm_pkg_op_log_request.gm_log_reconfig_request_detail (p_reconfigure_log_id
																, v_pnum
																, 'TBE'
																, v_sav_in_qty
																, '90807'
																, v_out_reconfig_log_detail_id
																 );
		END LOOP;
		--When ever the child is updated, the consignment table has to be updated ,so ETL can Sync it to GOP.
		UPDATE t504_consignment
		   SET c504_last_updated_by   = p_user
		     , c504_last_updated_date = CURRENT_DATE
		 WHERE c504_consignment_id = p_consign_id;	
		 
	END gm_sav_addpart_to_request;

--
--

	/*******************************************************
	* Description : Procedure to get add part section
	* Author	: Rakhi Gandhi
	*******************************************************/
	PROCEDURE gm_fch_parts_to_add (
		p_consign_id	IN		 t504_consignment.c504_consignment_id%TYPE
	  , p_outaddparts	OUT 	 TYPES.cursor_type
	)
	AS
		v_request_id   t520_request.c520_request_id%TYPE;
		v_bo_request_id t520_request.c520_request_id%TYPE;
		v_set_id	   t504_consignment.c207_set_id%TYPE;
	BEGIN
		-- Get MR from consignment table - Master MR ID
		SELECT c520_request_id, c207_set_id
		  INTO v_request_id, v_set_id
		  FROM t504_consignment
		 WHERE c504_consignment_id = p_consign_id;	 -- FOR UPDATE;

		OPEN p_outaddparts
		 FOR
			 SELECT c205_part_number_id pnum, get_partnum_desc (c205_part_number_id) pdesc, c208_set_qty qty
				  , pendingqty, bulkqty
			   FROM (SELECT t208.c205_part_number_id, t208.c208_set_qty
						  , t208.c208_set_qty - (NVL (req.reqqty, 0) + NVL (con.conqty, 0)) pendingqty
						  , get_qty_in_bulk (t208.c205_part_number_id) bulkqty
					   FROM t208_set_details t208
						  , (SELECT t521.c205_part_number_id, t521.c521_qty reqqty
							   FROM t521_request_detail t521, t520_request t520
							  WHERE (t520.c520_request_id = v_request_id OR c520_master_request_id = v_request_id)
								AND t521.c520_request_id = t520.c520_request_id
								AND t520.c520_void_fl IS NULL) req
						  , (SELECT   t505.c205_part_number_id, SUM (t505.c505_item_qty) conqty
								 FROM t504_consignment t504, t505_item_consignment t505
								WHERE	--t504.c504_consignment_id = p_consign_id
									  t504.c520_request_id = v_request_id
								  AND t505.c504_consignment_id = t504.c504_consignment_id
								  AND t504.c504_void_fl IS NULL
							 GROUP BY t505.c205_part_number_id) con
					  WHERE t208.c207_set_id = v_set_id
						AND t208.c205_part_number_id = req.c205_part_number_id(+)
						AND t208.c205_part_number_id = con.c205_part_number_id(+)
						AND t208.c208_set_qty > 0
						AND t208.c208_void_fl IS NULL)
			  WHERE pendingqty > 0;
	END gm_fch_parts_to_add;

----
--
 /*******************************************************
   * Description : Procedure to Back Order the Items
   * Author 	 : Vprasath
   *******************************************************/
--
	PROCEDURE gm_sav_backorder_item (
		p_required_date 	  IN	   t520_request.c520_required_date%TYPE
	  , p_request_source	  IN	   t520_request.c901_request_source%TYPE
	  , p_request_txn_id	  IN	   t520_request.c520_request_txn_id%TYPE
	  , p_part_number_id	  IN	   t205_part_number.c205_part_number_id%TYPE
	  , p_request_for		  IN	   t520_request.c520_request_for%TYPE
	  , p_request_to		  IN	   t520_request.c520_request_to%TYPE
	  , p_request_by_type	  IN	   t520_request.c901_request_by_type%TYPE
	  , p_request_by		  IN	   t520_request.c520_request_by%TYPE
	  , p_ship_to			  IN	   t520_request.c901_ship_to%TYPE
	  , p_ship_to_id		  IN	   t520_request.c520_ship_to_id%TYPE
	  , p_master_request_id   IN	   t520_request.c520_master_request_id%TYPE
	  , p_created_by		  IN	   t520_request.c520_created_by%TYPE
	  , p_required_qty		  IN	   t521_request_detail.c521_qty%TYPE
	  , p_out_request_id	  OUT	   t520_request.c520_request_id%TYPE
	)
	AS
		v_out_request_detail t521_request_detail.c521_request_detail_id%TYPE;
	BEGIN
		gm_pkg_op_request_master.gm_sav_request (''
											   , p_required_date
											   , p_request_source
											   , p_request_txn_id
											   , NULL
											   , p_request_for
											   , p_request_to
											   , p_request_by_type
											   , p_request_by
											   , p_ship_to
											   , p_ship_to_id
											   , p_master_request_id
											   , 10
											   , p_created_by
											   , NULL
											   , p_out_request_id
												);
		gm_pkg_op_request_master.gm_sav_request_detail (p_out_request_id
													  , p_part_number_id
													  , p_required_qty
													  , v_out_request_detail
													   );
	END gm_sav_backorder_item;

--
  /*******************************************************
   * Description : Procedure to Void the Consignment and
   *			   back order the request associated to
   *			   that consignment
   * Author 	 : Vprasath
   *******************************************************/
--
	PROCEDURE gm_sav_void_cons (
		p_consign_id   IN	t504_consignment.c504_consignment_id%TYPE
	  , p_user		   IN	t520_request.c520_created_by%TYPE
	)
	AS
		v_consign_status_fl t504_consignment.c504_status_fl%TYPE;
		v_verify_fl    t504_consignment.c504_verify_fl%TYPE;
		v_request_id   t520_request.c520_request_id%TYPE;
		v_void_fl	   t504_consignment.c504_void_fl%TYPE;
		v_settype	   VARCHAR2 (20);
		v_reconfigure_log_id t530_reconfigure_log.c530_reconfigure_log_id%TYPE;
		v_action	   VARCHAR2 (20) := '90803';   -- Release and back order
		v_release_to   VARCHAR2 (20) := '90805';   -- Release to Shelf
		v_bs_id 	   VARCHAR2 (20);
		v_bq_id 	   VARCHAR2 (20);
		v_out_reconfig_log_detail_id t531_reconfigure_log_detail.c531_reconfigure_log_detail_id%TYPE;
		v_out_txn_id   VARCHAR2 (20);

		CURSOR consg_cur
		IS
			SELECT t505.c205_part_number_id pnum, t505.c505_control_number control, t505.c505_item_qty qty
			  FROM t505_item_consignment t505
			 WHERE t505.c504_consignment_id = p_consign_id;
	BEGIN
		SELECT	   c504_status_fl, c504_verify_fl, c520_request_id, c504_void_fl
			  INTO v_consign_status_fl, v_verify_fl, v_request_id, v_void_fl
			  FROM t504_consignment
			 WHERE c504_consignment_id = p_consign_id
		FOR UPDATE;

		IF (v_consign_status_fl = 2 AND v_verify_fl = 1)
		THEN
			v_settype	:= 'Builtset';
		ELSE
			v_settype	:= '';
		END IF;

		--log reconfig data
		gm_pkg_op_log_request.gm_log_reconfig_request (90808, p_consign_id, v_request_id, p_user, v_reconfigure_log_id);

		-- Loop through the parts and reconfigure them
		FOR cons IN consg_cur
		LOOP
			gm_pkg_op_process_request.gm_sav_reconfig_cons_request (p_consign_id
																  , v_request_id
																  , cons.pnum
																  , cons.control
																  , cons.qty
																  , v_action
																  , v_release_to
																  , v_settype
																  , p_user
																  , v_out_txn_id
																  , v_bs_id
																  , v_bq_id
																   );
			gm_pkg_op_log_request.gm_log_reconfig_request_detail (v_reconfigure_log_id
																, cons.pnum
																, cons.control
																, cons.qty
																, v_action
																, v_out_reconfig_log_detail_id
																 );
		END LOOP;

		UPDATE t412_inhouse_transactions
		   SET c412_void_fl = 'Y'
		     , c412_last_updated_by = p_user
			 , c412_last_updated_date = CURRENT_DATE		   
		 WHERE c412_ref_id = p_consign_id AND c412_status_fl < 4;

		gm_pkg_op_process_request.gm_sav_chk_to_void_consignmnet (p_consign_id, p_user, v_void_fl);
	END gm_sav_void_cons;

		/******************************************************************
	 Description : This procedure is used to fetch details of requests
	 and child requests which need to be swapped
	****************************************************************/
	PROCEDURE gm_fch_req_to_swap (
		p_reqfrom	   IN		t520_request.c520_request_id%TYPE
	  , p_reqto 	   IN		t520_request.c520_request_id%TYPE
	  , p_skip_void_fl IN VARCHAR2
	  , p_outreqfrom   OUT		TYPES.cursor_type
	  , p_outreqto	   OUT		TYPES.cursor_type
	)
	AS
		v_company_id   t1900_company.c1900_company_id%TYPE;
		v_plant_id	   t5040_plant_master.c5040_plant_id%TYPE;
		v_filter 	   T906_RULES.C906_RULE_VALUE%TYPE;
	BEGIN
		
		--PC-2319: Swap RQ - new flow
		-- After the swap to skip the RQ void validation (becuase some times we voided the Source RQ).
		
		IF NVL(p_skip_void_fl, 'N') ='N'
		THEN
			gm_pkg_op_request_validate.gm_swap_req_validate (p_reqfrom, p_reqto);
		END IF;
		
		SELECT get_compid_frm_cntx(), get_plantid_frm_cntx() INTO v_company_id, v_plant_id FROM DUAL;
		
		SELECT NVL2(GET_RULE_VALUE(v_company_id,'REQUEST_SWAP'),v_plant_id,v_company_id) --REQUEST_SWAP:=rule value-plant
 			INTO v_filter 
			FROM DUAL;
			
		OPEN p_outreqfrom
		 FOR
			 SELECT t520.c520_request_id reqid
				  , NVL (t504.c504_consignment_id, 'A') consid	 --'A' here for Wrapper if consignment is null
				  , get_log_flag (t520.c520_request_id, 1235) clog
				  , DECODE (c520_master_request_id, NULL, 'Master', 'Child') master_child
				  , t520.c520_request_date reqtdate
				  , t520.c520_required_date reqdate
				  , get_code_name (t520.c901_request_source) reqsource
				  , DECODE (c901_request_source
						  , 50616, gm_pkg_op_sheet.get_demandsheet_name (t520.c520_request_txn_id)
						  , t520.c520_request_txn_id
						   ) txnnm
				  , t520.c207_set_id setid, get_set_name (t520.c207_set_id) setnm
				  , gm_pkg_op_request_summary.get_request_status (t520.c520_status_fl) reqstatus
			   FROM t520_request t520, t504_consignment t504
			  WHERE (t520.c520_request_id = p_reqfrom OR t520.c520_master_request_id = p_reqfrom
					)	--IN (p_reqfrom, p_reqto)
				AND t520.c520_request_id = t504.c520_request_id(+)
				AND t520.c520_void_fl IS NULL
				AND t504.c504_void_fl IS NULL
				AND (t520.c1900_company_id = v_filter OR t520.c5040_plant_id = v_filter);

		OPEN p_outreqto
		 FOR
			 SELECT t520.c520_request_id reqid, NVL (t504.c504_consignment_id, 'A') consid
				  , get_log_flag (t520.c520_request_id, 1235) clog
				  , DECODE (c520_master_request_id, NULL, 'Master', 'Child') master_child
				  , t520.c520_request_date reqtdate
				  , t520.c520_required_date reqdate
				  , get_code_name (t520.c901_request_source) reqsource
				  , DECODE (c901_request_source
						  , 50616, gm_pkg_op_sheet.get_demandsheet_name (t520.c520_request_txn_id)
						  , t520.c520_request_txn_id
						   ) txnnm
				  , t520.c207_set_id setid, get_set_name (t520.c207_set_id) setnm
				  , gm_pkg_op_request_summary.get_request_status (t520.c520_status_fl) reqstatus
			   FROM t520_request t520, t504_consignment t504
			  WHERE (t520.c520_request_id = p_reqto OR t520.c520_master_request_id = p_reqto)	--IN (p_reqfrom, p_reqto)
				AND t520.c520_request_id = t504.c520_request_id(+)
				AND t520.c520_void_fl IS NULL
				AND t504.c504_void_fl IS NULL
				AND (t520.c1900_company_id = v_filter OR t520.c5040_plant_id = v_filter);
	END gm_fch_req_to_swap;

	/******************************************************************
		Description : This procedure is used to swap the requests
		and child requests
		****************************************************************/
	PROCEDURE gm_sav_swap_req (
		p_reqfrom	IN	 t520_request.c520_request_id%TYPE
	  , p_reqto 	IN	 t520_request.c520_request_id%TYPE
	  , p_userid	IN	 t520_request.c520_last_updated_by%TYPE
	)
	AS
		v_txn_id_from  t520_request.c520_request_txn_id%TYPE;
		v_reqdate_from t520_request.c520_required_date%TYPE;
		v_reqtdate_from t520_request.c520_request_date%TYPE;
		v_source_from  t520_request.c901_request_source%TYPE;
		v_txn_id_to    t520_request.c520_request_txn_id%TYPE;
		v_reqdate_to   t520_request.c520_required_date%TYPE;
		v_reqtdate_to  t520_request.c520_request_date%TYPE;
		v_source_to    t520_request.c901_request_source%TYPE;
		v_from_request_for t520_request.c520_request_for%TYPE;
		v_from_request_to t520_request.c520_request_to%TYPE;
		v_to_request_for t520_request.c520_request_for%TYPE;
		v_to_request_to t520_request.c520_request_to%TYPE;
		v_strfrom	   VARCHAR2 (1000);
		v_strto 	   VARCHAR2 (1000);
		v_sheetname_from VARCHAR2 (100);
		v_sheetname_to VARCHAR2 (100);
		v_purpose_from t520_request.c901_purpose%TYPE;
		v_purpose_to   t520_request.c901_purpose%TYPE;
		v_from_shipto  t520_request.c901_ship_to%TYPE;
		v_from_shiptoid t520_request.c520_ship_to_id%TYPE;
		v_to_shipto		t520_request.c901_ship_to%TYPE;
		v_to_shiptoid	t520_request.c520_ship_to_id%TYPE;
		v_from_consign_id t504_consignment.c504_consignment_id %TYPE;
		v_to_consign_id   t504_consignment.c504_consignment_id %TYPE;
		--PMT-41534 - Priority Id swap during RQ swap
		v_priority_id_from t520_request.c5403_request_priority %TYPE;
		v_priority_id_to t520_request.c5403_request_priority %TYPE;
		-- PC-2319: Request swap changes (declare local varialble)
		v_from_req_status_id t520_request.c520_status_fl%TYPE;
		v_to_req_status_id t520_request.c520_status_fl%TYPE;
		--
	BEGIN
		SELECT	   t520.c520_request_date, t520.c520_required_date, t520.c901_request_source
				 , t520.c520_request_txn_id, t520.c520_request_for, t520.c520_request_to, t520.c901_purpose ,c901_ship_to,c520_ship_to_id
				 ,t520.c5403_request_priority
				 , t520.c520_status_fl
			  INTO v_reqtdate_from, v_reqdate_from, v_source_from
				 , v_txn_id_from, v_from_request_for, v_from_request_to, v_purpose_from ,v_from_shipto,v_from_shiptoid,v_priority_id_from
				 , v_from_req_status_id
			  FROM t520_request t520
			 WHERE t520.c520_request_id = p_reqfrom AND t520.c520_void_fl IS NULL
		FOR UPDATE;

		SELECT	   t520.c520_request_date, t520.c520_required_date, t520.c901_request_source, t520.c520_request_txn_id
				 , t520.c520_request_for, t520.c520_request_to, t520.c901_purpose ,c901_ship_to,c520_ship_to_id,t520.c5403_request_priority
				 , t520.c520_status_fl
			  INTO v_reqtdate_to, v_reqdate_to, v_source_to, v_txn_id_to
				 , v_to_request_for, v_to_request_to, v_purpose_to ,v_to_shipto ,v_to_shiptoid ,v_priority_id_to
				 , v_to_req_status_id
			  FROM t520_request t520
			 WHERE t520.c520_request_id = p_reqto AND t520.c520_void_fl IS NULL
		FOR UPDATE;

		gm_pkg_op_request_validate.gm_swap_req_validate (p_reqfrom, p_reqto);

		IF v_txn_id_from IS NOT NULL
		THEN
			v_sheetname_from := gm_pkg_op_sheet.get_demandsheet_name (v_txn_id_from);
		END IF;

		v_strfrom	:=
			   'Transaction ID: '
			|| v_sheetname_from
			|| ', '
			|| 'Required date: '
			|| v_reqdate_from
			|| ', '
			|| 'Request date: '
			|| v_reqtdate_from
			|| ', '
			|| 'Source: '
			|| get_code_name (v_source_from);

		UPDATE t520_request t520
		   SET t520.c520_status_fl = v_to_req_status_id
		     , t520.c520_comments = 'REQUEST SWAP: From '|| p_reqfrom ||' To '|| p_reqto
		   	 , t520.c520_last_updated_by = p_userid
			 , t520.c520_last_updated_date = CURRENT_DATE
		 WHERE c520_request_id = p_reqfrom AND t520.c520_void_fl IS NULL;

		IF v_txn_id_to IS NOT NULL
		THEN
			v_sheetname_to := gm_pkg_op_sheet.get_demandsheet_name (v_txn_id_to);
		END IF;

		v_strto 	:=
			   'Transaction ID: '
			|| v_sheetname_to
			|| ', '
			|| 'Required date: '
			|| v_reqdate_to
			|| ', '
			|| 'Request date: '
			|| v_reqtdate_to
			|| ', '
			|| 'Source: '
			|| get_code_name (v_source_to);

		UPDATE t520_request t520
		   SET t520.c520_status_fl = v_from_req_status_id
		   	 , t520.c520_comments = 'REQUEST SWAP: From '|| p_reqfrom ||' To '|| p_reqto
		   	 , t520.c520_last_updated_by = p_userid
			 , t520.c520_last_updated_date = CURRENT_DATE
		 WHERE c520_request_id = p_reqto AND t520.c520_void_fl IS NULL;
		 
      BEGIN 
        SELECT t504.c504_consignment_id
   		  INTO v_from_consign_id
   		  FROM t504_consignment t504
         WHERE t504.c520_request_id = p_reqfrom
           AND t504.c504_void_fl       IS NULL;
      EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
        v_from_consign_id:=NULL;
      END;
      BEGIN
        SELECT t504.c504_consignment_id
   		  INTO v_to_consign_id
   		  FROM t504_consignment t504
         WHERE t504.c520_request_id = p_reqto
           AND t504.c504_void_fl       IS NULL;
       EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
        v_to_consign_id:=NULL;
      END;
      IF v_from_consign_id IS NOT NULL THEN
       UPDATE t504_consignment t504
		  SET t504.c701_distributor_id = v_to_request_to
		    , t504.c504_ship_to = v_to_shipto
			, t504.c504_ship_to_id = v_to_shiptoid
			, t504.c504_inhouse_purpose = v_purpose_to
			, t504.c504_type =  DECODE(v_to_request_for ,40021,4110,40022,4112,v_to_request_for)
            , t504.c520_request_id = p_reqto
			, t504.c504_last_updated_by = p_userid
			, t504.c504_last_updated_date = CURRENT_DATE
			--, t504.c703_location_sales_rep_id= v_to_lo_repid
   		WHERE t504.c504_consignment_id  = v_from_consign_id
   		  AND t504.c504_void_fl IS NULL;
   	 END IF;
   	 IF v_to_consign_id IS NOT NULL THEN
       UPDATE t504_consignment t504
		  SET t504.c701_distributor_id = v_from_request_to
            , t504.c520_request_id = p_reqfrom
			, t504.c504_ship_to = v_from_shipto
			, t504.c504_ship_to_id =v_from_shiptoid
			, t504.c504_inhouse_purpose = v_purpose_from
			, t504.c504_type = DECODE(v_from_request_for ,40021,4110,40022,4112,v_from_request_for)
			, t504.c504_last_updated_by = p_userid
			, t504.c504_last_updated_date = CURRENT_DATE				
			--, t504.c703_location_sales_rep_id= v_from_lo_repid
   		WHERE t504.c504_consignment_id  = v_to_consign_id
   		  AND t504.c504_void_fl IS NULL;
      END IF;
      
      -- to update the child request
      gm_pkg_op_process_request.gm_upd_swap_child_request_dtls (p_reqfrom, p_reqto, p_userid);

		--also swap the shipping details
		gm_pkg_cm_shipping_trans.gm_sav_swap_ship_details (p_reqfrom, p_reqto, p_userid);
		
		-- to void request (Source - CS/ set building and status Backorder)
		--PC-2740 back order swap handling, and void fix
       gm_pkg_op_process_request.gm_void_swap_request (p_reqfrom, p_userid);
       gm_pkg_op_process_request.gm_void_swap_request (p_reqto, p_userid);
      
		gm_op_req_swap_email (p_reqfrom, p_reqto, p_userid, v_strfrom, v_strto, v_txn_id_from, v_txn_id_to);
	END gm_sav_swap_req;

	/***********************************************************
	* Description : Procedure to send Email when request swapped
	************************************************************/
	PROCEDURE gm_op_req_swap_email (
		p_reqfrom		IN	 t520_request.c520_request_id%TYPE
	  , p_reqto 		IN	 t520_request.c520_request_id%TYPE
	  , p_userid		IN	 VARCHAR2
	  , p_strfrom		IN	 VARCHAR2
	  , p_strto 		IN	 VARCHAR2
	  , p_txn_id_from	IN	 t520_request.c520_request_txn_id%TYPE
	  , p_txn_id_to 	IN	 t520_request.c520_request_txn_id%TYPE
	)
	AS
		subject 	   VARCHAR2 (100);
		to_mail 	   VARCHAR2 (200);
		priusrmail_from VARCHAR2 (200);
		priusrmail_to  VARCHAR2 (200);
		mail_body	   VARCHAR2 (4000);
		v_username	   VARCHAR2 (40);
		v_sheetname_from VARCHAR2 (200);
		v_sheetname_to VARCHAR2 (200);
		v_usrmail	   VARCHAR2 (200);
		crlf		   VARCHAR2 (2) := CHR (13) || CHR (10);
		v_owner_from   VARCHAR2 (200);
		v_owner_to	   VARCHAR2 (200);
		v_priuserid_from VARCHAR2 (40);
		v_priusername_from VARCHAR2 (40);
		v_priuserid_to VARCHAR2 (40);
		v_priusername_to VARCHAR2 (40);
	BEGIN
		SELECT t4020.c4020_primary_user, t4020.c4020_demand_nm
		  INTO v_priuserid_from, v_sheetname_from
		  FROM t4020_demand_master t4020
		 WHERE t4020.c4020_demand_master_id = p_txn_id_from;

		SELECT t4020.c4020_primary_user, t4020.c4020_demand_nm
		  INTO v_priuserid_to, v_sheetname_to
		  FROM t4020_demand_master t4020
		 WHERE t4020.c4020_demand_master_id = p_txn_id_to;

		v_priusername_from := get_user_name (v_priuserid_from);
		priusrmail_from := get_user_emailid (v_priuserid_from);

		IF p_txn_id_from IS NOT NULL
		THEN
			v_owner_from := 'The owner of ' || v_sheetname_from || ' sheet is, ' || v_priusername_from;
		END IF;

		v_priusername_to := get_user_name (v_priuserid_to);
		priusrmail_to := get_user_emailid (v_priuserid_to);

		IF p_txn_id_to IS NOT NULL
		THEN
			v_owner_to	:= 'The owner of ' || v_sheetname_to || ' sheet is, ' || v_priusername_to;
		END IF;

		--	v_setname	:= get_set_name (p_setid);
			-- Person who change action set
		v_username	:= get_user_name (p_userid);
		v_usrmail	:= get_user_emailid (p_userid);
		-- Email Area
		to_mail 	:=
				get_rule_value ('RQTSWAP', 'EMAIL') || ',' || v_usrmail || ',' || priusrmail_from || ','
				|| priusrmail_to;
		subject 	:= ' Request Swapped ';
		mail_body	:=
			   'Following Two Requests have been swapped '
			|| crlf
			|| crlf
			|| 'Request '
			|| p_reqfrom
			|| ' has been swapped with Request '
			|| p_reqto
			|| crlf
			|| crlf
			|| p_reqfrom
			|| ' : '
			|| crlf
			|| p_strfrom
			|| crlf
			|| crlf
			|| 'To '
			|| crlf
			|| p_strto
			|| crlf
			|| crlf
			|| crlf
			|| crlf
			|| p_reqto
			|| ' : '
			|| crlf
			|| p_strto
			|| crlf
			|| 'To '
			|| crlf
			|| p_strfrom
			|| crlf
			|| crlf
			|| v_owner_from
			|| crlf
			|| crlf
			|| v_owner_to;
		gm_com_send_email_prc (to_mail, subject, mail_body);
	END gm_op_req_swap_email;

/***********************************************************
	* Description : Procedure to copy the shipping details from one request to another
	************************************************************/
	PROCEDURE gm_sav_cpy_req_shipping (
		p_reqfrom	IN	 t520_request.c520_request_id%TYPE
	  , p_reqto 	IN	 t520_request.c520_request_id%TYPE
	  , p_source	IN	 t907_shipping_info.c901_source%TYPE   -- source for reqto
	  , p_userid	IN	 t907_shipping_info.c907_last_updated_by%TYPE
	)
	AS
		v_shiptodate   VARCHAR2 (100);
		v_shipto	   t907_shipping_info.c901_ship_to%TYPE;
		v_names 	   t907_shipping_info.c907_ship_to_id%TYPE;
		v_shipcarrier  t907_shipping_info.c901_delivery_carrier%TYPE;
		v_shipmode	   t907_shipping_info.c901_delivery_mode%TYPE;
		v_trackingno   t907_shipping_info.c907_tracking_number%TYPE;
		v_addressid    t907_shipping_info.c106_address_id%TYPE;
		v_freightamt   t907_shipping_info.c907_frieght_amt%TYPE;
		v_flg		   VARCHAR2 (100);
		v_company_id    t907_shipping_info.c1900_company_id%TYPE;
		v_plant_id      t907_shipping_info.c5040_plant_id%TYPE;
		v_req_company_id t1900_company.c1900_company_id%TYPE;
		v_source t907_shipping_info.c901_source%TYPE;
		v_prod_req_id  t526_product_request_detail.c525_product_request_id%TYPE;
	BEGIN
		v_flg		:= 'true';

		-- Getting the company and plant id
			 SELECT NVL (GET_COMPID_FRM_CNTX (), GET_RULE_VALUE ('DEFAULT_COMPANY', 'ONEPORTAL')), NVL (GET_PLANTID_FRM_CNTX (),
    				GET_RULE_VALUE ('DEFAULT_PLANT', 'ONEPORTAL'))
   				INTO v_company_id, v_plant_id
   				FROM DUAL;
   		 -- The following code is added for getting the Company ID based on the Saved Consignment Request ID		
		SELECT DECODE(p_source
		              ,'50181' , get_transaction_company_id (v_company_id,p_reqfrom,'SET_SHIP_FILTER','CONSIGNMENTREQUEST') --p_reqfrom :=Request id
		              , v_company_id) 
		  INTO v_req_company_id FROM DUAL;		
		-- if request for is Stock transfer (26240420) then ship to source is Stock trasfer(26240435) else request(50184)
		SELECT DECODE (p_source,'26240435','26240435','50184')
		INTO v_source
		FROM DUAL;
   				
   		--		
		BEGIN
			SELECT c901_ship_to shipto, c907_ship_to_id names, TO_CHAR (c907_ship_to_dt, GET_RULE_VALUE('DATEFMT', 'DATEFORMAT')) shiptodate
				 , c901_delivery_mode shipmode, c901_delivery_carrier shipcarrier, c907_tracking_number trackingno
				 , c106_address_id addressid, c907_frieght_amt freightamt
			  INTO v_shipto, v_names, v_shiptodate
				 , v_shipmode, v_shipcarrier, v_trackingno
				 , v_addressid, v_freightamt
			  FROM t907_shipping_info t907
			 WHERE t907.c907_ref_id = p_reqfrom AND t907.c901_source = v_source AND t907.c907_void_fl is NULL;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				v_flg		:= 'false';
		END;
		--PMT-32450 --  Get Lonaer Request ID from T520 Request table and Insert to T907 Shipping Table
			BEGIN
			SELECT c525_product_request_id INTO v_prod_req_id
			FROM T520_REQUEST WHERE C520_REQUEST_ID = p_reqfrom 
			AND c520_void_fl IS NULL;
			EXCEPTION 
			WHEN OTHERS THEN 
			v_prod_req_id		:= '';
			END;

		IF v_flg = 'true'
		THEN
			INSERT INTO t907_shipping_info
						(c907_shipping_id, c907_ref_id, c901_ship_to, c907_ship_to_id, c907_ship_to_dt
					   , c901_source, c901_delivery_mode, c901_delivery_carrier, c907_tracking_number
					   , c106_address_id, c907_status_fl, c907_frieght_amt, c907_created_by, c907_created_date
					   , c907_active_fl, c1900_company_id, c5040_plant_id,c525_product_request_id
						)
				 VALUES (s907_ship_id.NEXTVAL, p_reqto, v_shipto, v_names, TO_DATE (v_shiptodate, GET_RULE_VALUE('DATEFMT', 'DATEFORMAT'))
					   , p_source, v_shipmode, v_shipcarrier, v_trackingno
					   , v_addressid, 15, v_freightamt, p_userid, CURRENT_DATE
					   , DECODE (p_source, '50184', 'N', NULL), v_req_company_id, v_plant_id ,v_prod_req_id
						);
		END IF;
	END gm_sav_cpy_req_shipping;
	
	/*
	 * Description: Fetch the created Transactions
	 * Author: Rajkumar
	 */
	PROCEDURE gm_fch_inhouse_txn(
		p_consign_id		  IN	   t504_consignment.c504_consignment_id%TYPE,
		p_out				  OUT		VARCHAR2
	)
	AS
		v_txnids	VARCHAR2(200);
	BEGIN
	
	  WITH v_txns AS (
             SELECT c412_inhouse_trans_id trans_id
   					FROM t412_inhouse_transactions
  				WHERE c412_ref_id = p_consign_id
  				AND C412_VOID_FL IS NULL
       )
       SELECT MAX (SYS_CONNECT_BY_PATH (trans_id, ', ')) INTO v_txnids
         FROM
           (SELECT trans_id, ROW_NUMBER () OVER (ORDER BY trans_id) AS curr
              FROM v_txns)
         CONNECT BY curr - 1 = PRIOR curr
        START WITH curr = 1;
     	
        IF v_txnids IS NOT NULL
        THEN
        	v_txnids := SUBSTR (v_txnids, 2);
        ELSE
        	v_txnids := '';
        END IF;
        
        p_out := v_txnids;	
        
	END gm_fch_inhouse_txn;
	
	/*
	 *	DESCRIPTION: THis function is used to fetch the allocated qty for the CN
	 *	Author: Rajkumar 
	 * 
	 */
	FUNCTION get_allocated_qty (
		p_refid		VARCHAR2,
		p_reftype   t504_consignment.c504_type%TYPE,
		p_pnum		t505_item_consignment.C205_PART_NUMBER_ID%TYPE,
		p_warehouse	t505_item_consignment.C901_WAREHOUSE_TYPE%TYPE
	)
	RETURN NUMBER
	IS
	v_qty	NUMBER :=0;
	BEGIN
		 SELECT SUM (t505.c505_item_qty) INTO v_qty
                   FROM t504_consignment t504, t505_item_consignment t505
                  WHERE t504.c504_consignment_id = t505.c504_consignment_id
                  	 AND t505.c504_consignment_id NOT IN (p_refid) 
                     AND t504.c504_verified_date IS NULL
                     AND T504.C504_Void_Fl       IS NULL
                     AND T504.C504_status_fl     < 4
                     AND T505.C505_Void_Fl       IS NULL
                     AND t504.C207_set_id        IS NULL
                     AND t504.c504_type          IN (p_reftype) -- item consignment,Inhouse-Consignment. Add Above code if new transaction comes
                     AND T505.c901_warehouse_type = p_warehouse
                     AND T505.C205_Part_Number_Id = p_pnum
                 GROUP BY t505.c205_part_number_id; 
		
        RETURN NVL(v_qty,0);
    EXCEPTION WHEN OTHERS THEN
     	RETURN 0;    
	END get_allocated_qty;
  /*
	 *	DESCRIPTION: THis function is used to fetch the allocated req qty for the CN
	 *	Author: selvan 
	 * 
	 */
	FUNCTION get_cn_txn_alloc (
		p_refid		VARCHAR2,
		p_reftype   t504_consignment.c504_type%TYPE,
		p_pnum		t505_item_consignment.C205_PART_NUMBER_ID%TYPE,
		p_warehouse	t505_item_consignment.C901_WAREHOUSE_TYPE%TYPE
	)
	RETURN NUMBER
	IS
	v_qty	NUMBER :=0;
	BEGIN
		 SELECT SUM (t505.c505_item_qty) INTO v_qty
                   FROM t504_consignment t504, t505_item_consignment t505
                  WHERE t504.c504_consignment_id = t505.c504_consignment_id
                  	 AND t505.c504_consignment_id  IN (p_refid) 
                     AND t504.c504_verified_date IS NULL
                     AND T504.C504_Void_Fl       IS NULL
                     AND T504.C504_status_fl     < 4
                     AND T505.C505_Void_Fl       IS NULL
                     AND t504.C207_set_id        IS NULL
                     AND t504.c504_type          IN (p_reftype) -- item consignment,Inhouse-Consignment. Add Above code if new transaction comes
                     AND T505.c901_warehouse_type = p_warehouse
                     AND T505.C205_Part_Number_Id = p_pnum
                 GROUP BY t505.c205_part_number_id; 
		
        RETURN NVL(v_qty,0);
    EXCEPTION WHEN OTHERS THEN
     	RETURN 0;    
	END get_cn_txn_alloc;
    /*
	 *	DESCRIPTION: THis function is used to fetch the total qty for the part
	 *	Author: Selvan 
	 * 
	 */
	FUNCTION get_total_warehouse_qty (
	  p_partnum       IN   t205c_part_qty.c205_part_number_id%TYPE,  
       	  p_wh_type       IN   t901_code_lookup.c901_code_id%TYPE
	)
	RETURN NUMBER
	IS
	v_total_qty	NUMBER :=0;
	BEGIN 
        
	SELECT NVL (get_partnumber_qty(p_partnum,p_wh_type), 0)
	  INTO v_total_qty
	  FROM dual;
                 
        RETURN v_total_qty;
    EXCEPTION WHEN OTHERS THEN
     	RETURN 0;    
	END get_total_warehouse_qty;
/*
 *  Description: Procedure to Create the PSFG and PSRW for voided request
 *  Author:	Rajkumar
 */
PROCEDURE gm_save_voided_ps_request(
	p_consign_id		  IN	   t504_consignment.c504_consignment_id%TYPE,
	p_user		   IN	t520_request.c520_created_by%TYPE
)
AS

CURSOR v_cur 
IS
SELECT T505.C205_PART_NUMBER_ID PNUM, T505.C505_CONTROL_NUMBER CNUM, T505.C505_ITEM_QTY QTY
  , T505.C901_WAREHOUSE_TYPE WHTYPE
   FROM T505_ITEM_CONSIGNMENT T505, T504_CONSIGNMENT T504
  WHERE T505.C504_CONSIGNMENT_ID = T504.C504_CONSIGNMENT_ID
    AND T504.C504_CONSIGNMENT_ID = p_consign_id
    AND T505.C505_VOID_FL       IS NULL
    AND T504.C504_VOID_FL IS NULL;
    
		v_psfgstr		VARCHAR2(4000):='';
		v_psrwstr		VARCHAR2(4000):='';
		v_comments		VARCHAR2(2000) := '';
		v_id			VARCHAR2(20) := '';
		v_cntrl_flg		VARCHAR2(20) := 'N';
BEGIN

	FOR v_item IN v_cur
	LOOP
			IF v_item.CNUM IS NOT NULL AND v_item.CNUM <> 'TBE'
			THEN
				v_cntrl_flg := 'Y';
			END IF;
			IF v_item.WHTYPE = '90800' 
			THEN
				v_psfgstr := v_psfgstr || v_item.PNUM || '^' || v_item.CNUM || '^' || v_item.QTY || '|';
			ELSIF v_item.WHTYPE = '56001'
			THEN
				v_psrwstr := v_psrwstr || v_item.PNUM || '^' || v_item.CNUM || '^' || v_item.QTY || '|'; 
			END IF;
	END LOOP;
	
	IF v_cntrl_flg = 'Y'
		THEN
			 
			IF v_psfgstr IS NOT NULL
			THEN
					v_comments := 'Pending shipping to shelf transaction for Item Consignment: ' || p_consign_id;
					gm_pkg_op_request_master.gm_sav_inhouse_txn(
      					v_comments,
      					3,
      					100408,	-- Transaction Purpose
      					100406,	-- PSFG
      					p_consign_id,
      					4110,
      					NULL,
      					NULL,
      					p_user,
      					v_id
      				);
			
					gm_pkg_op_request_master.gm_sav_inhouse_txn_string(
																v_id,
																v_psfgstr,
																p_user
															);													
		END IF;
		
		IF v_psrwstr IS NOT NULL
		THEN
			
			v_comments := 'Pending shipping to RW transaction for Item Consignment: ' || p_consign_id;
				gm_pkg_op_request_master.gm_sav_inhouse_txn(
  					v_comments,
  					3, 
  					4000117,-- Transaction Purpose
  					56028,	-- PSRW
  					p_consign_id,
  					4110,
  					NULL,
  					NULL,
  					p_user,
  					v_id
  				);
		
				gm_pkg_op_request_master.gm_sav_inhouse_txn_string(
															v_id,
															v_psrwstr,
															p_user
														);
		END IF;
	END IF;
END gm_save_voided_ps_request;

	/*
	* This procedure used to swap the child request details.
	* Based on from and To request- get the child request.
	* Update child request details
	*
	*/
	PROCEDURE gm_upd_swap_child_request_dtls (
	        p_reqfrom IN t520_request.c520_request_id%TYPE,
	        p_reqto   IN t520_request.c520_request_id%TYPE,
	        p_userid  IN t520_request.c520_last_updated_by%TYPE)
	AS
	    --
	    -- PC-2319: Request swap changes (declare local varialble)
	    v_from_child_req_ids VARCHAR2 (4000) ;
	    v_to_child_req_ids   VARCHAR2 (4000) ;
	    --
	    v_from_request_date t520_request.c520_request_date%TYPE;
	    v_from_reqired_date t520_request.c520_required_date%TYPE;
	    v_from_req_source t520_request.c901_request_source%TYPE;
	    --
	    v_to_request_date t520_request.c520_request_date%TYPE;
	    v_to_reqired_date t520_request.c520_required_date%TYPE;
	    v_to_req_source t520_request.c901_request_source%TYPE;
	    --
	    v_from_request_txn_id t520_request.c520_request_txn_id%TYPE;
	    v_from_request_priority t520_request.c5403_request_priority%TYPE;
	    --
	    v_to_request_txn_id t520_request.c520_request_txn_id%TYPE;
	    v_to_request_priority t520_request.c5403_request_priority%TYPE;
	    v_from_status_fl t520_request.c520_status_fl%TYPE;
	    v_to_status_fl t520_request.c520_status_fl%TYPE;
	    --
	BEGIN
	    -- From Request master data
	    BEGIN
	     SELECT t520.c520_request_date, t520.c520_required_date reqdate, t520.c901_request_source
	     	, c520_request_txn_id, c5403_request_priority,c520_status_fl
	       INTO v_from_request_date, v_from_reqired_date, v_from_req_source
	       	,v_from_request_txn_id, v_from_request_priority,v_from_status_fl
	       FROM t520_request t520
	      WHERE c520_request_id = p_reqfrom
	        AND c520_void_fl   IS NULL;
	     EXCEPTION
	    WHEN OTHERS THEN
	        v_from_request_date := NULL;
	        v_from_reqired_date := NULL;
	        v_from_req_source := NULL;
	       	v_from_request_txn_id := NULL;
	       	v_from_request_priority := NULL;
	    END;   
	    -- To Request master data
	    BEGIN
	     SELECT t520.c520_request_date, t520.c520_required_date reqdate, t520.c901_request_source
	     	, c520_request_txn_id, c5403_request_priority,c520_status_fl
	       INTO v_to_request_date, v_to_reqired_date, v_to_req_source
	       	, v_to_request_txn_id, v_to_request_priority,v_to_status_fl
	       FROM t520_request t520
	      WHERE c520_request_id = p_reqto
	        AND c520_void_fl   IS NULL;
	     EXCEPTION WHEN OTHERS
	     THEN
	     	v_to_request_date := NULL;
	     	v_to_reqired_date := NULL;
	     	v_to_req_source := NULL;
	     	v_to_request_txn_id := NULL;
	     	v_to_request_priority := NULL;
	     END;
	     
	    -- to get the child request data.
	    BEGIN
	         SELECT RTRIM (XMLAGG (XMLELEMENT (e, c520_request_id || ',')) .EXTRACT ('//text()'), ',')
	           INTO v_from_child_req_ids
	           FROM t520_request
	          WHERE c520_master_request_id = p_reqfrom
	          	AND c520_status_fl > 0
				AND c520_status_fl < 40
	            AND c520_void_fl          IS NULL ;
	    EXCEPTION
	    WHEN OTHERS THEN
	        v_from_child_req_ids := NULL;
	    END;
	    --
	    BEGIN
	         SELECT RTRIM (XMLAGG (XMLELEMENT (e, c520_request_id || ',')) .EXTRACT ('//text()'), ',')
	           INTO v_to_child_req_ids
	           FROM t520_request
	          WHERE c520_master_request_id = p_reqto
	          	AND c520_status_fl > 0
				AND c520_status_fl < 40
	            AND c520_void_fl          IS NULL ;
	    EXCEPTION
	    WHEN OTHERS THEN
	        v_to_child_req_ids := NULL;
	    END;
	    
	    -- to update the child request details
	     UPDATE t520_request
	    SET c520_master_request_id                   = p_reqto, c520_request_date = v_to_request_date
	      , c520_required_date = v_to_reqired_date, c901_request_source = v_to_req_source
	        , c520_request_txn_id = v_to_request_txn_id, c5403_request_priority = v_to_request_priority
	        , c520_last_updated_by = p_userid , c520_last_updated_date   = CURRENT_DATE
	      WHERE c520_request_id                     IN (v_from_child_req_ids)
	      	AND c520_void_fl          IS NULL;
	      
	    --
	     UPDATE t520_request
	    SET c520_master_request_id                   = p_reqfrom, c520_request_date = v_from_request_date
	    	, c520_required_date =v_from_reqired_date, c901_request_source = v_from_req_source
	        , c520_request_txn_id = v_from_request_txn_id, c5403_request_priority = v_from_request_priority
	        , c520_last_updated_by = p_userid, c520_last_updated_date  = CURRENT_DATE
	      WHERE c520_request_id                     IN (v_to_child_req_ids)
	      	AND c520_void_fl          IS NULL;
	      
	      -- to store the default comments
	      
	      gm_pkg_op_process_request.gm_upd_swap_child_req_auto_comments (v_from_child_req_ids, p_reqfrom, p_reqto, p_userid);
	      gm_pkg_op_process_request.gm_upd_swap_child_req_auto_comments (v_to_child_req_ids, p_reqfrom, p_reqto, p_userid);
	      
	    ----PC-2740 back order swap handling, and void fix
       /* After the swap,if the source request status is back order, then all the request details associated to the
		* "To Request ID" should be updated to "From Req ID", this will avoid "To Request" still holding the line items after the swap.
		*/
		IF (v_from_status_fl = 10)
		THEN
			UPDATE T521_REQUEST_DETAIL SET c520_request_id = p_reqfrom
			WHERE c520_request_id = p_reqto ;
		END IF;
	    
		/* After the swap,if the target request status is back order, then all the request details associated to the
		* "From Request ID" should be updated to "To Req ID", this will avoid "From Request" still holding the line items after the swap.
		*/
		IF (v_to_status_fl = 10)
		THEN
			UPDATE T521_REQUEST_DETAIL SET c520_request_id = p_reqto
			WHERE c520_request_id = p_reqfrom ;
		END IF;
	END gm_upd_swap_child_request_dtls;
	
	
	/*
	* This procedure used to voide request- based on below condition.
	* 1. Source is Set building and status is Back order and not assigned Request (or)
	* 2. Source is Cust. service and not assigned request - Void the RQ
	*
	*/
	PROCEDURE gm_void_swap_request (
	        p_req_id             IN t520_request.c520_request_id%TYPE,
	        p_userid              IN t520_request.c520_last_updated_by%TYPE)
	AS
	v_request_source  t520_request.c901_request_source%TYPE;
	v_request_to      t520_request.c520_request_to%TYPE;
	v_request_status  t520_request.c520_status_fl%TYPE;
	BEGIN
		
		Begin
			SELECT C520_REQUEST_TO,C520_STATUS_FL,C901_REQUEST_SOURCE 
			INTO v_request_to,v_request_status,v_request_source
			FROM T520_REQUEST where c520_request_id=p_req_id
			AND C520_VOID_FL IS NULL;
			
		end;
		 
	        
	    -- void flow
	    -- 50617 set building
	    -- 50618 Customer service
	    -- from RQ
	    IF ( (v_request_source = 50617 OR v_request_source = 50618) AND v_request_status = 10 AND
	        v_request_to        IS NULL) THEN
	        -- Txn id, reason, type, comments, user id
	        --90753	Request Cancelled

	        gm_pkg_common_cancel.gm_cm_sav_cancelrow (p_req_id, 90753, 'VDREQ', 'RQ voided as part of Request Swap', p_userid) ;
	        --
	    END IF;
	    --
	END gm_void_swap_request;
	
	/*
	 * This procedure used to store child request swap log details
	 */
	PROCEDURE gm_upd_swap_child_req_auto_comments (
	        p_request_ids IN VARCHAR2,
	        p_reqfrom     IN t520_request.c520_request_id%TYPE,
	        p_reqto       IN t520_request.c520_request_id%TYPE,
	        p_user_id     IN t520_request.c520_last_updated_by%TYPE)
	AS
	    --
	    v_out_message VARCHAR2 (4000) ;
	    --
	    CURSOR v_child_req_cur
	    IS
	         SELECT token child_id FROM v_in_list ;
	    --
	BEGIN
	    -- to set the RQ ids to context
	    my_context.set_my_inlist_ctx (p_request_ids) ;
	    --
	    FOR v_child_req IN v_child_req_cur
	    LOOP
	        GM_UPDATE_LOG (v_child_req.child_id, 'REQUEST SWAP: From '||p_reqfrom ||' To '|| p_reqto, 1235, p_user_id,
	        v_out_message) ;
	    END LOOP;
	    --
	END gm_upd_swap_child_req_auto_comments;

END gm_pkg_op_process_request;
/
