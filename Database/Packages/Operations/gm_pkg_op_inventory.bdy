 /* Formatted on 2010/10/29 08:26 (Formatter Plus v4.8.0) */
 --@"C:\DATABASE\packages\operations\gm_pkg_op_inventory.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_inventory
IS
--
  /*******************************************************
   * Description : Procedure to get Inventory types
   * Author 	 : D James
   *******************************************************/
--
	PROCEDURE gm_fc_fch_invtypes (
		p_outinvtypes	OUT   TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_outinvtypes
		 FOR
			 SELECT c901_code_id codeid, c901_code_nm codenm
			   FROM t901_code_lookup
			  WHERE c901_code_grp = 'INVTY';
	END gm_fc_fch_invtypes;

--
  /*******************************************************
   * Description : Procedure to get Inventory lock Id's
   * Author 	 : D James
   *******************************************************/
--
	PROCEDURE gm_fc_fch_invlockids (
		p_locktype		  IN	   t250_inventory_lock.c901_lock_type%TYPE
	  , p_outinvlockids   OUT	   TYPES.cursor_type
	)
	AS
		v_comp_date_fmt	VARCHAR2 (20);
		v_company_id  t1900_company.c1900_company_id%TYPE; 
        v_plant_id    t5040_plant_master.C5040_PLANT_ID %TYPE;
	BEGIN
	    select get_compid_frm_cntx(),get_plantid_frm_cntx() 
		into v_company_id,v_plant_id 
		from dual;
	
		SELECT get_compdtfmt_frm_cntx()
		INTO v_comp_date_fmt
		FROM dual;
		
		OPEN p_outinvlockids
		 FOR
			 SELECT c250_inventory_lock_id lockid, TO_CHAR (c250_lock_date, v_comp_date_fmt) lockdt
			   FROM t250_inventory_lock
			  WHERE c901_lock_type = p_locktype AND c250_void_fl IS NULL
			  AND c5040_plant_id    = v_plant_id;
	END gm_fc_fch_invlockids;

--
  /*******************************************************
   * Description : Procedure to generate header section for inventory
   * Author 	 : D James
   *******************************************************/
--
	PROCEDURE gm_fc_fch_invheader (
		p_param 		 IN 	  VARCHAR2
	  , p_outinvheader	 OUT	  TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_outinvheader
		 FOR
			 SELECT c901_code_nm
			   FROM t901_code_lookup
			  WHERE c901_code_grp = 'INVTY' AND c901_code_id IN (NVL (c901_code_id, p_param)) AND c901_active_fl = 1;
	END gm_fc_fch_invheader;

--
--
  /*******************************************************
   * Description : Procedure to generate details section for inventory
   * Author 	 : D James
   *******************************************************/
--
	PROCEDURE gm_fc_fch_invdetails (
		p_invid 		  IN	   NUMBER
	  , p_partnum		  IN	   VARCHAR2
	  , p_projid		  IN	   VARCHAR2
	  , p_outinvdetails   OUT	   TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_outinvdetails
		 FOR
			 SELECT   t205.c205_part_number_id ID, t205.c205_part_number_id || ':' || t205.c205_part_num_desc NAME
					, get_code_name (t251.c901_type) itype, c251_qty qty
				 FROM t251_inventory_lock_detail t251, t202_project t202, t205_part_number t205
				WHERE c250_inventory_lock_id = p_invid
				  AND regexp_like (t205.c205_part_number_id, NVL (REGEXP_REPLACE(p_partnum,'[+]','\+'), REGEXP_REPLACE(t205.c205_part_number_id,'[+]','\+')))
				  AND t202.c202_project_id = NVL (p_projid, t202.c202_project_id)
				  AND t205.c202_project_id = t202.c202_project_id
				  AND t205.c205_part_number_id = t251.c205_part_number_id
			 ORDER BY t205.c205_part_number_id, itype;
	END gm_fc_fch_invdetails;

--

	--
  /*******************************************************
   * Description : Procedure to generate details Inhouse Aging Summary
   * Author 	 : Murali
   *******************************************************/
--
	PROCEDURE gm_fch_aging_summary (
		p_out_inhouse_aging   OUT	TYPES.cursor_type
	)
	AS
		v_plant_id     	t5040_plant_master.c5040_plant_id%TYPE;
	BEGIN
		SELECT NVL(get_plantid_frm_cntx(),get_rule_value('DEFAULT_PLANT','ONEPORTAL')) INTO  v_plant_id FROM DUAL;
		OPEN p_out_inhouse_aging
		 FOR
			 SELECT typname, txntype, CURRVAL, seven_15, sixteen_21, greater_21
				  , (CURRVAL + seven_15 + sixteen_21 + greater_21) total
			   FROM (SELECT   get_code_name (c504_type) typname, c504_type txntype
							, NVL (COUNT (DECODE (SIGN (TRUNC (SYSDATE) - TRUNC (c504_created_date) - 7), -1, 1))
								 , 0
								  ) CURRVAL
							, NVL (COUNT (DECODE (SIGN (TRUNC (SYSDATE) - TRUNC (c504_created_date) - 6)
												, 1, DECODE (SIGN (TRUNC (SYSDATE) - TRUNC (c504_created_date) - 16)
														   , -1, 1
															)
												 )
										 )
								 , 0
								  ) seven_15
							, NVL (COUNT (DECODE (SIGN (TRUNC (SYSDATE) - TRUNC (c504_created_date) - 15)
												, 1, DECODE (SIGN (TRUNC (SYSDATE) - TRUNC (c504_created_date) - 21)
														   , -1, 1
															)
												 )
										 )
								 , 0
								  ) sixteen_21
							, NVL (COUNT (DECODE (SIGN (TRUNC (SYSDATE) - TRUNC (c504_created_date) - 20), 1, 1))
								 , 0
								  ) greater_21
						 FROM t504_consignment t504
						WHERE t504.c504_status_fl > 1
						  AND t504.c504_status_fl < 4
						  AND t504.c207_set_id IS NULL
						  AND t504.c504_void_fl IS NULL
						  AND t504.c704_account_id = '01'
						  AND t504.c504_type NOT IN (40051, 40052, 40053, 40054, 40055, 40056)
						  AND t504.c5040_plant_id = v_plant_id
					 GROUP BY c504_type
					 UNION
					 SELECT   get_code_name (c412_type) typename, c412_type txntype
							, NVL (COUNT (DECODE (SIGN (TRUNC (SYSDATE) - TRUNC (c412_created_date) - 7), -1, 1))
								 , 0
								  ) CURRVAL
							, NVL (COUNT (DECODE (SIGN (TRUNC (SYSDATE) - TRUNC (c412_created_date) - 6)
												, 1, DECODE (SIGN (TRUNC (SYSDATE) - TRUNC (c412_created_date) - 16)
														   , -1, 1
															)
												 )
										 )
								 , 0
								  ) seven_15
							, NVL (COUNT (DECODE (SIGN (TRUNC (SYSDATE) - TRUNC (c412_created_date) - 15)
												, 1, DECODE (SIGN (TRUNC (SYSDATE) - TRUNC (c412_created_date) - 21)
														   , -1, 1
															)
												 )
										 )
								 , 0
								  ) sixteen_21
							, NVL (COUNT (DECODE (SIGN (TRUNC (SYSDATE) - TRUNC (c412_created_date) - 20), 1, 1))
								 , 0
								  ) greater_21
						 FROM t412_inhouse_transactions
						WHERE c412_void_fl IS NULL
						  AND c412_status_fl > 1
						  and C412_STATUS_FL < 4
						  AND c412_type NOT IN (select C906_RULE_VALUE from T906_RULES where C906_RULE_GRP_ID = 'IHAGING')
						  AND c5040_plant_id = v_plant_id
					 GROUP BY c412_type);
	END gm_fch_aging_summary;

--
/*******************************************************
   * Description : Function to Check if the Transaction can be allocated to Device.
   * Author 	: Rajeshwaran
   *******************************************************/
	FUNCTION chk_txn_details (
		p_txn_id	VARCHAR2
	  , p_txn_typ	VARCHAR2
	)
		RETURN VARCHAR
	IS
		v_allocate_fl  VARCHAR2 (1) := 'Y';
		v_total_cnt    NUMBER;
		v_check_cnt    NUMBER;
		v_lit_cnt	   NUMBER;
		v_bm_cnt	   NUMBER;
		v_usage_cnt NUMBER;
	BEGIN
		IF p_txn_typ = '93001'	 -- ORDER
		THEN
			SELECT COUNT (c205_part_number_id), COUNT (DECODE (c901_type, 50301, 1))   -- LOANER
			,COUNT (DECODE (gm_pkg_op_inv_scan.get_part_display_status (c205_part_number_id, 'INV-PART-DISPLAY', '999'),'H',1))
			  INTO v_total_cnt, v_check_cnt,v_usage_cnt
			  FROM t502_item_order t502
			 WHERE c501_order_id = p_txn_id AND c502_delete_fl IS NULL;

			 v_check_cnt := v_check_cnt+v_usage_cnt;
		ELSIF p_txn_typ = '93002'	-- CONSIGNMENT
		THEN
			SELECT COUNT (t505.c205_part_number_id), COUNT (DECODE (t205.c205_product_family, 4056, 1))
				 , COUNT (DECODE (gm_pkg_op_inv_scan.get_part_display_status (t505.c205_part_number_id
																			, 'INV-BM-DISPLAY'
																			, '4053'
																			 )
								, 4053, 1
								 )
						 )
			  INTO v_total_cnt, v_lit_cnt
				 , v_bm_cnt
			  FROM t505_item_consignment t505, t205_part_number t205
			 WHERE t505.c504_consignment_id = p_txn_id
			   AND t505.c505_void_fl IS NULL
			   AND t505.c205_part_number_id = t205.c205_part_number_id;

			v_check_cnt := v_lit_cnt + v_bm_cnt;
		--if any other type , should return 'Y'
		ELSE
			RETURN 'Y';
		END IF;
		
		DBMS_OUTPUT.PUT_LINE('v_total_cnt >>'||v_total_cnt);
		DBMS_OUTPUT.PUT_LINE('v_check_cnt>>>'||v_check_cnt);
		IF v_total_cnt = v_check_cnt
		THEN
			v_allocate_fl := 'N';
		END IF;
		
		DBMS_OUTPUT.PUT_LINE('v_allocate_fl>>>'||v_allocate_fl);
		RETURN v_allocate_fl;
	END chk_txn_details;

  /*******************************************************
   * Description : Function to returns the qty for the given partnumber depending on the type thats passed
   * Author    : Brinal
   *******************************************************/
--
   FUNCTION get_qty_forpart (
      p_part_num   t205_part_number.c205_part_number_id%TYPE,
      p_type       t901_code_lookup.c901_code_id%TYPE
   )
      RETURN NUMBER
   IS
      v_qty   NUMBER := 0;
   BEGIN
      IF p_type = 100040 --shelf
      THEN
         v_qty := get_qty_in_stock (p_part_num);
      ELSIF p_type = 100041  --bulk
      THEN
         v_qty := GET_QTY_IN_BULK (p_part_num);     
      ELSIF p_type =  100039  -- In-House
      THEN
      	   v_qty := GET_QTY_IN_INHOUSEINV (p_part_num);
      END IF;

      RETURN v_qty;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN '0';
   END get_qty_forpart;
--
PROCEDURE gm_fch_partnum_inv_rpt(
	 p_pnum  IN CLOB
	,p_subcomp_fl IN T205_PART_NUMBER.c205_sub_component_fl%TYPE
	,p_obselete IN VARCHAR2
	,p_projid IN T205_PART_NUMBER.C202_PROJECT_ID%TYPE
	,p_type  IN VARCHAR2
	,p_excinv IN VARCHAR2
	,p_set_id IN t207_set_master.c207_set_id%TYPE           
	,p_partnum_list OUT Types.cursor_type
)
AS
v_pnum	CLOB := UPPER(p_pnum);
v_company_id t1900_company.C1900_COMPANY_ID%TYPE;
v_count number;
	
	BEGIN

			SELECT get_compid_frm_cntx()
			   INTO v_company_id
			   FROM DUAL;
			my_context.set_my_inlist_ctx(p_excinv);
			DELETE from my_temp_part_list;
			IF p_type = 'BACKORDER'
			THEN
				my_context.set_my_cloblist (v_pnum);
				
				INSERT INTO my_temp_part_list (c205_part_number_id)
				SELECT t205.c205_part_number_id pnum 
				FROM t205_part_number t205 ,v_clob_list my_temp ,t2023_part_company_mapping t2023
				WHERE t205.C205_PART_NUMBER_ID IS NOT NULL
				AND t205.C205_PART_NUMBER_ID = my_temp.token
				AND my_temp.token IS NOT NULL
				AND t205.C205_PART_NUMBER_ID = t2023.c205_part_number_id 
				AND t2023.c2023_void_fl IS NULL
				AND t2023.c1900_company_id = v_company_id
				AND rownum < 2002;
				--Rownum is added as there is a validation to fetch only upto 2000 records.
			
			ELSIF p_set_id IS NOT NULL
			THEN
			INSERT INTO my_temp_part_list (c205_part_number_id)
			SELECT t205.c205_part_number_id pnum
			FROM t205_part_number t205 ,
			t2023_part_company_mapping t2023,
			t207_set_master t207 ,
			t208_set_details t208
			WHERE t205.c205_part_number_id = t2023.c205_part_number_id
			AND t205.c205_part_number_id   = t208.c205_part_number_id
			AND t207.c207_set_id           = t208.c207_set_id
			AND t2023.c2023_void_fl       IS NULL
			AND t2023.c1900_company_id    = v_company_id
			AND t207.c207_set_id          IN (p_set_id)
			AND t207.c207_void_fl IS NULL
			AND t208.c208_void_fl IS NULL
			AND REGEXP_LIKE(t205.c205_part_number_id, CASE WHEN TO_CHAR(v_pnum) IS NOT NULL THEN TO_CHAR(REGEXP_REPLACE(v_pnum,'[+]','\+')) ELSE REGEXP_REPLACE(t205.c205_part_number_id,'[+]','\+') END)
			AND NVL(t205.c205_sub_component_fl,'-9999') = CASE WHEN NVL(p_subcomp_fl,'N') = 'Y' THEN NVL(t205.c205_sub_component_fl,'-9999') ELSE   '-9999' END
			AND NVL(t205.c901_status_id,'-9999') <> CASE WHEN p_obselete IS NULL THEN '20369' ELSE '-9999' END
	        AND t205.c202_project_id = CASE WHEN NVL(p_projid,'0') !='0' THEN p_projid ELSE t205.C202_PROJECT_ID END
	        AND t208.C208_SET_QTY <> 0
            AND rownum < 2002;

            ELSE
	            INSERT INTO my_temp_part_list (c205_part_number_id)
				SELECT t205.c205_part_number_id pnum 
				FROM t205_part_number t205 ,t2023_part_company_mapping t2023
				WHERE t205.C205_PART_NUMBER_ID IS NOT NULL
				AND REGEXP_LIKE(t205.c205_part_number_id, CASE WHEN TO_CHAR(v_pnum) IS NOT NULL THEN TO_CHAR(REGEXP_REPLACE(v_pnum,'[+]','\+')) ELSE REGEXP_REPLACE(t205.c205_part_number_id,'[+]','\+') END)
				AND NVL(t205.c205_sub_component_fl,'-9999') = CASE WHEN NVL(p_subcomp_fl,'N') = 'Y' THEN NVL(t205.c205_sub_component_fl,'-9999') ELSE   '-9999' END
				AND  NVL(t205.c901_status_id,'-9999') <> CASE WHEN p_obselete IS NULL THEN '20369' ELSE '-9999' END
				AND t205.c202_project_id = CASE WHEN NVL(p_projid,'0') !='0' THEN p_projid ELSE t205.C202_PROJECT_ID END
				AND t205.c205_part_number_id = t2023.c205_part_number_id 
				AND t2023.c2023_void_fl IS NULL
				AND t2023.c1900_company_id = v_company_id
				AND rownum < 2002;
				--Rownum is added as there is a validation to fetch only upto 2000 records.
			END IF;
			
			SELECT count(c205_part_number_id) into v_count from my_temp_part_list;	
			-- This validation is added to fetch only upto 2000 parts in Main Inventory Report.
			
			if v_count > 2000 then
				raise_application_error (-20673, '');
			end if;
			
			OPEN  p_partnum_list FOR
			SELECT 
				c205_part_number_id pnum	,
				c205_part_num_desc pdesc	,
				rmqty	rmqty,
				c205_inquaran inquaran	,
				c205_inbulk inbulk	,
				c205_in_wip_set in_wip_set	,
				c205_tbe_alloc,
				inreturns	inreturns,
				c205_drawrev drawrev	,
				c205_popend popend	,
				sales_allocated	sales_allocated,
				c205_cons_alloc cons_alloc	,
				c205_quar_alloc_self_quar quar_alloc_self_quar	,
				c205_quar_alloc_to_scrap 	,
				c205_quar_alloc_to_inv quar_alloc_to_inv	,
				c205_bulk_alloc_to_set bulk_alloc_to_set	,
				c205_bulk_alloc_to_inv bulk_alloc_to_inv	,
				c205_inv_alloc_to_pack inv_alloc_to_pack	,
				c205_inshelf INSHELF	,
				c205_shelfalloc SHELFALLOC	,
				c205_quaralloc QUARALLOC	,
				c205_bulkalloc BULKALLOC	,
				rm_alloc_qty	rm_alloc_qty,
				mfg_tbr	mfg_tbr,
				mfg_release	mfg_release,
				c205_powip powip	,
				dhr_wip	dhr_wip,
				dhr_pen_qc_ins	dhr_pen_qc_ins,
				dhr_pen_qc_rel	dhr_pen_qc_rel,
				dhr_qc_appr	dhr_qc_appr,
				build_set_qty	build_set_qty
				,c205_returnavail RETURNAVAIL,
				c205_returnalloc RETURNALLOC
				,c205_repackavail repackavail
				,c205_repackalloc  repackalloc
				,c205_dhralloc dhralloc 
				,c205_shipalloc SHIPAllOC
				,c205_ih_avail_qty IH_AVAIL_QTY
				,c205_ih_alloc_qty IH_ALLOC_QTY
				,c205_ih_set_qty IH_SET_QTY
				,c205_ih_item_qty IH_ITEM_QTY 
				,c205_loanqty LOANQTY
				,(CASE 
					WHEN c205_rw_avail_qty < 0 
					THEN
						0 
					ELSE
						c205_rw_avail_qty  
					END) inrw
				,c205_rw_alloc_qty RWALLOC
				,c205_intrans_avail_qty INTRANS_AVAIL_QTY
				,c205_intrans_alloc_qty INTRANS_ALLOC_QTY 
			from v205_qty_in_stock;
END gm_fch_partnum_inv_rpt;

/******************************************************************
 * Description : procedure to fetch loaner bo parts by loaner CN#
 * Author      : Dhana Reddy
 ******************************************************************/
PROCEDURE gm_fch_loaner_bo_parts (
        p_ref_id IN t412_inhouse_transactions.c412_ref_id%TYPE,
        p_bo_parts OUT TYPES.cursor_type)
AS
BEGIN
	
 OPEN p_bo_parts FOR
	SELECT t413.c205_part_number_id pnum, sum (t413.c413_item_qty) boqty
      FROM t412_inhouse_transactions t412, t413_inhouse_trans_items t413 ,t504a_loaner_transaction t504a
     WHERE t412.c412_inhouse_trans_id       = t413.c412_inhouse_trans_id
       AND t412.c504a_loaner_transaction_id = t504a.c504a_loaner_transaction_id (+)
       AND c412_status_fl                   = 0
       AND t412.c412_type                   = 100062
       AND t413.c413_void_fl               IS NULL
       AND c412_void_fl                    IS NULL
       AND t412.c412_ref_id = p_ref_id 
       AND t504a.c504a_void_fl IS NULL
  GROUP BY t413.c205_part_number_id;  
  
END gm_fch_loaner_bo_parts;

END gm_pkg_op_inventory;
/