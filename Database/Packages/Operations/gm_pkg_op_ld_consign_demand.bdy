/* Formatted on 2009/12/28 18:08 (Formatter Plus v4.8.0) */
--@"c:\database\packages\operations\gm_pkg_op_ld_consign_demand.bdy"
-- show err  Package Body gm_pkg_op_ld_consign_demand
-- exec gm_pkg_op_ld_demand.gm_op_ld_demand_main(33);
/*
DELETE FROM T4042_DEMAND_SHEET_DETAIL;
DELETE FROM T4041_DEMAND_SHEET_MAPPING;
DELETE FROM T4040_DEMAND_SHEET;
COMMIT ;
exec gm_pkg_op_ld_demand.gm_op_ld_demand_main(20);
*/
CREATE OR REPLACE
PACKAGE BODY gm_pkg_op_ld_consign_demand
IS
    /*************************************************************************
    * Purpose: Procedure used to load sales demand, calculated value and
    * Forecast information
    *************************************************************************/
PROCEDURE gm_op_ld_main (
        p_demand_id       IN t4020_demand_master.c4020_demand_master_id%TYPE,
        p_demand_sheet_id IN t4040_demand_sheet.c4040_demand_sheet_id%TYPE,
        p_demand_type     IN t4020_demand_master.c901_demand_type%TYPE,
        p_demand_period   IN t4020_demand_master.c4020_demand_period%TYPE,
        p_forecast_period IN t4020_demand_master.c4020_forecast_period%TYPE,
        p_inc_current_fl  IN t4020_demand_master.c4020_include_current_fl%TYPE,
        p_inventory_id    IN t250_inventory_lock.c250_inventory_lock_id%TYPE)
AS
    --
    v_from_period DATE;
    v_to_period DATE;
    --
BEGIN
    --
    -- Below code to fetch demand (from and to period)
    gm_pkg_op_ld_demand.gm_op_calc_period (p_demand_period, p_inc_current_fl, 'D', v_from_period, v_to_period) ;
    --
    DBMS_OUTPUT.put_line ('From and To Date  ****' || v_from_period || ' - ' || v_to_period) ;
    --
    -- Below procedure called to load sales demand information
    IF (p_demand_type = 40021) THEN
        gm_pkg_op_ld_consign_demand.gm_op_ld_demand_sales (p_demand_id, p_demand_sheet_id, v_from_period, v_to_period)
        ;
    ELSE
        gm_pkg_op_ld_consign_demand.gm_op_ld_demand_inhouse (p_demand_id, p_demand_sheet_id, v_from_period, v_to_period
        ) ;
    END IF;
    --
    -- Below code to fetch forecast (from and to period)
    --DBMS_OUTPUT.put_line ('Before calling gm_op_calc_period');
    gm_pkg_op_ld_demand.gm_op_calc_period (p_forecast_period, p_inc_current_fl, 'F', v_from_period, v_to_period) ;
    --
    -- Below procedure called to load forecast information
    --DBMS_OUTPUT.put_line ('Before calling gm_op_ld_forecast');
    gm_pkg_op_ld_consign_demand.gm_op_ld_forecast (p_demand_id, p_demand_sheet_id, v_from_period, v_to_period) ;
    --
    -- Below code to load par information
    --DBMS_OUTPUT.put_line ('Before calling gm_op_ld_par_value');
    --DBMS_OUTPUT.put_line ('Before calling gm_op_ld_forecast - ' || p_demand_id || ' -- ' || p_demand_sheet_id);
    gm_pkg_op_ld_consign_demand.gm_op_ld_par_value (p_demand_id, p_demand_sheet_id, v_from_period) ;
    -- Below code to load Pending Requests
    gm_pkg_op_ld_consign_demand.gm_op_ld_pending_request (p_demand_id, p_demand_sheet_id, v_from_period, p_inventory_id
    ) ;
    --
    -- Below procedure called to load sales Weighted and Avg sales information
    --gm_pkg_op_ld_consign_demand.gm_op_ld_forecast_calc (p_demand_sheet_id, v_from_period);
    --
    --
END gm_op_ld_main;
--
/*************************************************************************
* Purpose: Procedure used to load sales consignment demand for selected period
* 40030 maps to Group Mapping and 40032 maps to Region Mapping
*************************************************************************/
PROCEDURE gm_op_ld_demand_sales (
        p_demand_id       IN t4020_demand_master.c4020_demand_master_id%TYPE,
        p_demand_sheet_id IN t4040_demand_sheet.c4040_demand_sheet_id%TYPE,
        p_from_period     IN DATE,
        p_to_period       IN DATE)
AS
    --
    -- Only fetch set information
    -- Item consignment load will be seperate SQL
    CURSOR demand_set_cur
    IS
         SELECT partinfo.c207_set_id, partinfo.c205_part_number_id, partinfo.month_value
          , NVL (c_qty, 0) item_qty
           FROM
            (
                SELECT DISTINCT t208.c207_set_id, t208.c205_part_number_id, v9001.month_value
                   FROM t208_set_details t208, t4021_demand_mapping t4021, v9001_month_list v9001
                  WHERE t4021.c4020_demand_master_id = p_demand_id
                    AND t4021.c901_ref_type          = 40031
                    AND t208.c208_inset_fl           = 'Y'
                    AND t4021.c4021_ref_id           = t208.c207_set_id
                    AND v9001.month_value           >= p_from_period
                    AND v9001.month_value           <= p_to_period
                    AND t208.c208_void_fl           IS NULL
            )
        partinfo, (
             SELECT t504.c207_set_id setid, TO_DATE ('01/' || TO_CHAR (t504.c504_ship_date, 'MM/YYYY'), 'DD/MM/YYYY')
                sdate, t505.c205_part_number_id p_num, SUM (t505.c505_item_qty) c_qty
               FROM t504_consignment t504, t505_item_consignment t505, (
                    SELECT DISTINCT v700.d_id d_id
                       FROM v700_territory_mapping_detail v700, t4021_demand_mapping t4021
                      WHERE t4021.c4020_demand_master_id = p_demand_id
                        AND t4021.c901_ref_type          = 40032
                        AND t4021.c4021_ref_id           = v700.region_id
                )
                distlist, (
                     SELECT t4021.c4021_ref_id
                       FROM t4021_demand_mapping t4021
                      WHERE t4021.c4020_demand_master_id = p_demand_id
                        AND t4021.c901_ref_type          = 40031
                )
                setlist
              WHERE t504.c701_distributor_id IS NOT NULL
                AND t504.c504_consignment_id  = t505.c504_consignment_id
                AND t504.c701_distributor_id  = distlist.d_id
                AND t504.c207_set_id         IS NOT NULL
                AND t504.c207_set_id          = setlist.c4021_ref_id
                -- Remove the consignment transfers
                AND NOT EXISTS
                (
                     SELECT c921_ref_id
                       FROM t920_transfer t920, t921_transfer_detail t921
                      WHERE t920.c920_transfer_id    = t921.c920_transfer_id
                        AND c921_ref_id              = t504.c504_consignment_id
                        AND t920.c920_void_fl       IS NULL
                        AND t920.c920_transfer_date >= p_from_period
                        AND t920.c920_transfer_date <= p_to_period
                        AND t921.c901_link_type      = 90360
                )
                AND t504.c504_ship_date             >= p_from_period
                AND t504.c504_ship_date             <= p_to_period
                AND t504.c504_status_fl              = 4
                AND t504.c504_type                  IN (4110, 40057)
                AND TRIM (t505.c505_control_number) IS NOT NULL
                AND t504.c504_void_fl               IS NULL
                AND (t505.c901_type                 IS NULL
                OR t505.c901_type                   <> 100880) --Exclude  Consignments Adjusted for Tag
           GROUP BY t504.c207_set_id, t505.c205_part_number_id, TO_CHAR (t504.c504_ship_date, 'MM/YYYY')
        )
        consinfo
      WHERE partinfo.c205_part_number_id = consinfo.p_num(+)
        AND partinfo.month_value         = consinfo.sdate(+)
        AND partinfo.c207_set_id         = consinfo.setid(+);
    -- Only Item Consignment
    CURSOR demand_item_cur
    IS
         SELECT partinfo.c205_part_number_id, partinfo.month_value, NVL (c_qty, 0) item_qty
           FROM
            (
                SELECT DISTINCT t208.c205_part_number_id, v9001.month_value
                   FROM t208_set_details t208, t4021_demand_mapping t4021, v9001_month_list v9001
                  WHERE t4021.c4020_demand_master_id = p_demand_id
                    AND t4021.c901_ref_type          = 40031
                    AND t4021.c4021_ref_id           = t208.c207_set_id
                    AND v9001.month_value           >= p_from_period
                    AND v9001.month_value           <= p_to_period
                    AND t208.c208_void_fl           IS NULL
            )
        partinfo, (
             SELECT TO_DATE ('01/' || TO_CHAR (t504.c504_ship_date, 'MM/YYYY'), 'DD/MM/YYYY') sdate,
                t505.c205_part_number_id p_num, SUM (t505.c505_item_qty) c_qty
               FROM t504_consignment t504, t505_item_consignment t505, (
                    SELECT DISTINCT v700.d_id d_id
                       FROM v700_territory_mapping_detail v700, t4021_demand_mapping t4021
                      WHERE t4021.c4020_demand_master_id = p_demand_id
                        AND t4021.c901_ref_type          = 40032
                        AND t4021.c4021_ref_id           = v700.region_id
                )
                distlist
              WHERE t504.c701_distributor_id     IS NOT NULL
                AND t504.c504_consignment_id      = t505.c504_consignment_id
                AND t504.c701_distributor_id      = distlist.d_id
                AND t504.c504_consignment_id NOT IN
                (
                     SELECT a.c504_consignment_id a
                       FROM t504a_consignment_excess a
                      WHERE a.c504_consignment_id = t504.c504_consignment_id
                )
                AND t504.c207_set_id IS NULL
                -- Remove the consignment transfers
                AND NOT EXISTS
                (
                     SELECT c921_ref_id
                       FROM t920_transfer t920, t921_transfer_detail t921
                      WHERE t920.c920_transfer_id    = t921.c920_transfer_id
                        AND c921_ref_id              = t504.c504_consignment_id
                        AND t920.c920_void_fl       IS NULL
                        AND t920.c920_transfer_date >= p_from_period
                        AND t920.c920_transfer_date <= p_to_period
                        AND t921.c901_link_type      = 90360
                )
                AND t504.c504_ship_date             >= p_from_period
                AND t504.c504_ship_date             <= p_to_period
                AND t504.c504_status_fl              = 4
                AND t504.c504_type                  IN (4110, 40057)
                AND TRIM (t505.c505_control_number) IS NOT NULL
                AND t504.c504_void_fl               IS NULL
                AND (t505.c901_type                 IS NULL
                OR t505.c901_type                   <> 100880) --Exclude  Consignments Adjusted for Tag
           GROUP BY t505.c205_part_number_id, TO_CHAR (t504.c504_ship_date, 'MM/YYYY')
        )
        consinfo
      WHERE partinfo.c205_part_number_id = consinfo.p_num(+)
        AND partinfo.month_value         = consinfo.sdate(+);
BEGIN
    --
    -- load demand set information
    FOR set_val IN demand_set_cur
    LOOP
        --
        --DBMS_OUTPUT.put_line ('Sheet ID  ****' || set_val.c205_part_number_id || ' - ' || set_val.item_qty);
        gm_pkg_op_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id, set_val.c207_set_id, 50560, set_val.month_value,
        set_val.c205_part_number_id, set_val.item_qty, NULL, NULL, NULL) ;
        --
    END LOOP;
    --
    -- load demand item information
    FOR set_val IN demand_item_cur
    LOOP
        --
        --DBMS_OUTPUT.put_line ('Sheet ID  ****' || set_val.c205_part_number_id || ' - ' || set_val.item_qty);
        gm_pkg_op_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id, '-9999', 50560, set_val.month_value,
        set_val.c205_part_number_id, set_val.item_qty, NULL, NULL, NULL) ;
        --
    END LOOP;
    --
    -- Delete the item consignment Part which does not contain any item consignment
    -- for selected period
     DELETE
       FROM t4042_demand_sheet_detail
      WHERE c901_type             = 50560
        AND c4040_demand_sheet_id = p_demand_sheet_id
        AND c4042_ref_id          = '-9999'
        AND c205_part_number_id  IN
        (
             SELECT t4042.c205_part_number_id
               FROM t4042_demand_sheet_detail t4042
              WHERE t4042.c901_type             = 50560
                AND t4042.c4040_demand_sheet_id = p_demand_sheet_id
                AND t4042.c4042_ref_id          = '-9999'
           GROUP BY t4042.c205_part_number_id
            HAVING SUM (c4042_qty) = 0
        ) ;
    --
END gm_op_ld_demand_sales;
/*************************************************************************
* Purpose: Procedure used to load inhouse consignment demand for selected period
* 40030 maps to Group Mapping and 40032 maps to Region Mapping
*************************************************************************/
PROCEDURE gm_op_ld_demand_inhouse (
        p_demand_id       IN t4020_demand_master.c4020_demand_master_id%TYPE,
        p_demand_sheet_id IN t4040_demand_sheet.c4040_demand_sheet_id%TYPE,
        p_from_period     IN DATE,
        p_to_period       IN DATE)
AS
    --
    -- Only fetch set information
    -- Item consignment load will be seperate SQL
    CURSOR demand_set_cur
    IS
         SELECT partinfo.c207_set_id, partinfo.c205_part_number_id, partinfo.month_value
          , NVL (c_qty, 0) item_qty
           FROM
            (
                SELECT DISTINCT t208.c207_set_id, t208.c205_part_number_id, v9001.month_value
                   FROM t208_set_details t208, t4021_demand_mapping t4021, v9001_month_list v9001
                  WHERE t4021.c4020_demand_master_id = p_demand_id
                    AND t4021.c901_ref_type          = 40031
                    AND t208.c208_inset_fl           = 'Y'
                    AND t4021.c4021_ref_id           = t208.c207_set_id
                    AND v9001.month_value           >= p_from_period
                    AND v9001.month_value           <= p_to_period
                    AND t208.c208_void_fl           IS NULL
            )
        partinfo, (
             SELECT t504.c207_set_id setid, TO_DATE ('01/' || TO_CHAR (t504.c504_ship_date, 'MM/YYYY'), 'DD/MM/YYYY')
                sdate, t505.c205_part_number_id p_num, SUM (t505.c505_item_qty) c_qty
               FROM t504_consignment t504, t505_item_consignment t505, (
                     SELECT t4021.c4021_ref_id
                       FROM t4021_demand_mapping t4021
                      WHERE t4021.c4020_demand_master_id = p_demand_id
                        AND t4021.c901_ref_type          = 40031
                )
                setlist
              WHERE t504.c701_distributor_id IS NULL
                AND t504.c504_consignment_id  = t505.c504_consignment_id
                AND t504.c704_account_id      = '01'
                AND t504.c207_set_id         IS NOT NULL
                AND t504.c207_set_id          = setlist.c4021_ref_id
                -- Remove the consignment transfers
                AND NOT EXISTS
                (
                     SELECT c921_ref_id
                       FROM t920_transfer t920, t921_transfer_detail t921
                      WHERE t920.c920_transfer_id    = t921.c920_transfer_id
                        AND c921_ref_id              = t504.c504_consignment_id
                        AND t920.c920_void_fl       IS NULL
                        AND t920.c920_transfer_date >= p_from_period
                        AND t920.c920_transfer_date <= p_to_period
                        AND t921.c901_link_type      = 90360
                )
                AND t504.c504_ship_date             >= p_from_period
                AND t504.c504_ship_date             <= p_to_period
                AND t504.c504_status_fl              = 4
                AND TRIM (t505.c505_control_number) IS NOT NULL
                AND t504.c504_void_fl               IS NULL
           GROUP BY t504.c207_set_id, t505.c205_part_number_id, TO_CHAR (t504.c504_ship_date, 'MM/YYYY')
        )
        consinfo
      WHERE partinfo.c205_part_number_id = consinfo.p_num(+)
        AND partinfo.month_value         = consinfo.sdate(+)
        AND partinfo.c207_set_id         = consinfo.setid(+);
    -- Only Item Consignment
    CURSOR demand_item_cur
    IS
         SELECT partinfo.c205_part_number_id, partinfo.month_value, NVL (c_qty, 0) item_qty
           FROM
            (
                SELECT DISTINCT t208.c205_part_number_id, v9001.month_value
                   FROM t208_set_details t208, t4021_demand_mapping t4021, v9001_month_list v9001
                  WHERE t4021.c4020_demand_master_id = p_demand_id
                    AND t4021.c901_ref_type          = 40031
                    AND t4021.c4021_ref_id           = t208.c207_set_id
                    AND v9001.month_value           >= p_from_period
                    AND v9001.month_value           <= p_to_period
                    AND t208.c208_void_fl           IS NULL
            )
        partinfo, (
             SELECT TO_DATE ('01/' || TO_CHAR (t504.c504_ship_date, 'MM/YYYY'), 'DD/MM/YYYY') sdate,
                t505.c205_part_number_id p_num, SUM (t505.c505_item_qty) c_qty
               FROM t504_consignment t504, t505_item_consignment t505
              WHERE t504.c701_distributor_id IS NULL
                AND t504.c504_consignment_id  = t505.c504_consignment_id
                AND t504.c704_account_id      = '01'
                AND t504.c207_set_id         IS NULL
                -- Remove the consignment transfers
                AND NOT EXISTS
                (
                     SELECT c921_ref_id
                       FROM t920_transfer t920, t921_transfer_detail t921
                      WHERE t920.c920_transfer_id    = t921.c920_transfer_id
                        AND c921_ref_id              = t504.c504_consignment_id
                        AND t920.c920_void_fl       IS NULL
                        AND t920.c920_transfer_date >= p_from_period
                        AND t920.c920_transfer_date <= p_to_period
                        AND t921.c901_link_type      = 90360
                )
                AND t504.c504_ship_date             >= p_from_period
                AND t504.c504_ship_date             <= p_to_period
                AND t504.c504_status_fl              = 4
                AND TRIM (t505.c505_control_number) IS NOT NULL
                AND t504.c504_void_fl               IS NULL
           GROUP BY t505.c205_part_number_id, TO_CHAR (t504.c504_ship_date, 'MM/YYYY')
        )
        consinfo
      WHERE partinfo.c205_part_number_id = consinfo.p_num(+)
        AND partinfo.month_value         = consinfo.sdate(+);
BEGIN
    --
    -- load demand set information
    FOR set_val IN demand_set_cur
    LOOP
        --
        --DBMS_OUTPUT.put_line ('Sheet ID  ****' || set_val.c205_part_number_id || ' - ' || set_val.item_qty);
        gm_pkg_op_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id, set_val.c207_set_id, 50560, set_val.month_value,
        set_val.c205_part_number_id, set_val.item_qty, NULL, NULL, NULL) ;
        --
    END LOOP;
    --
    -- load demand item information
    FOR set_val IN demand_item_cur
    LOOP
        --
        --DBMS_OUTPUT.put_line ('Sheet ID  ****' || set_val.c205_part_number_id || ' - ' || set_val.item_qty);
        gm_pkg_op_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id, '-9999', 50560, set_val.month_value,
        set_val.c205_part_number_id, set_val.item_qty, NULL, NULL, NULL) ;
        --
    END LOOP;
    --
    -- Delete the item consignment Part which does not contain any item consignment
    -- for selected period
     DELETE
       FROM t4042_demand_sheet_detail
      WHERE c901_type             = 50560
        AND c4040_demand_sheet_id = p_demand_sheet_id
        AND c4042_ref_id          = '-9999'
        AND c205_part_number_id  IN
        (
             SELECT t4042.c205_part_number_id
               FROM t4042_demand_sheet_detail t4042
              WHERE t4042.c901_type             = 50560
                AND t4042.c4040_demand_sheet_id = p_demand_sheet_id
                AND t4042.c4042_ref_id          = '-9999'
           GROUP BY t4042.c205_part_number_id
            HAVING SUM (c4042_qty) = 0
        ) ;
    --
    -- This procedure will capture the demand for all the Write off parts during Loaner Reconciliation process .
    gm_pkg_op_ld_consign_demand.gm_op_ld_demand_lnr_writeoff(p_demand_id,p_demand_sheet_id,p_from_period,p_to_period);
END gm_op_ld_demand_inhouse;
--
/*************************************************************************
* Purpose: Procedure used to load sales forecast information
* Based on the demand weighted will load forecase information
*************************************************************************/
PROCEDURE gm_op_ld_forecast (
        p_demand_id       IN t4020_demand_master.c4020_demand_master_id%TYPE,
        p_demand_sheet_id IN t4040_demand_sheet.c4040_demand_sheet_id%TYPE,
        p_from_period     IN DATE,
        p_to_period       IN DATE)
AS
    --
    CURSOR forecast_cur_set
    IS
         SELECT partinfo.c207_set_id groupid, partinfo.c205_part_number_id, partinfo.month_value
          , (partinfo.c208_set_qty * setinfo.c4031_value) item_qty
           FROM
            (
                 SELECT t208.c207_set_id, t208.c205_part_number_id, v9001.month_value
                  , t208.c208_set_qty
                   FROM t208_set_details t208, t4021_demand_mapping t4021, v9001_month_list v9001
                  WHERE t4021.c4020_demand_master_id = p_demand_id
                    AND t4021.c901_ref_type          = 40031
                    AND t4021.c4021_ref_id           = t208.c207_set_id
                    AND t208.c208_inset_fl           = 'Y'
                    AND t208.c208_set_qty           <> 0
                    AND v9001.month_value           >= p_from_period
                    AND v9001.month_value           <= p_to_period
                    AND t208.c208_void_fl           IS NULL
            )
        partinfo, (
            SELECT DISTINCT t4030.c4030_ref_id set_id, t4031.c4031_start_date, t4031.c4031_end_date
              , t4031.c901_ref_type, t4031.c4031_value
               FROM t4020_demand_master t4020, t4030_demand_growth_mapping t4030, t4031_growth_details t4031
              WHERE t4020.c4020_demand_master_id = p_demand_id
                AND t4020.c4020_demand_master_id = t4030.c4020_demand_master_id
                AND t4031.c4030_demand_growth_id = t4030.c4030_demand_growth_id
                AND t4030.c901_ref_type          = 20296
                AND t4031.c4031_start_date      >= p_from_period
                AND t4031.c4031_end_date        <= p_to_period
                AND t4030.c4030_void_fl         IS NULL
                AND t4031.c4031_value           IS NOT NULL
        )
        setinfo
      WHERE partinfo.c207_set_id = setinfo.set_id
        AND partinfo.month_value = setinfo.c4031_start_date;
    CURSOR forecast_cur_part
    IS
        SELECT DISTINCT t4030.c4030_ref_id part_id, v9001.month_value, (NVL (t4031.c4031_value, 0) + DECODE (
            t4031.c901_ref_type, 20382, gm_pkg_op_ld_consign_demand.get_demand_item_consig_avg (p_demand_sheet_id,
            t4030.c4030_ref_id), 0)) item_qty
           FROM t4020_demand_master t4020, t4030_demand_growth_mapping t4030, t4031_growth_details t4031
          , v9001_month_list v9001
          WHERE t4020.c4020_demand_master_id = p_demand_id
            AND t4030.c901_ref_type          = 20298
            AND t4020.c4020_demand_master_id = t4030.c4020_demand_master_id
            AND t4031.c4030_demand_growth_id = t4030.c4030_demand_growth_id
            AND v9001.month_value           >= p_from_period
            AND v9001.month_value           <= p_to_period
            AND v9001.month_value            = t4031.c4031_start_date(+)
            AND t4030.c4030_void_fl         IS NULL
            AND t4031.c4031_value           IS NOT NULL;
    -- Only Item Consignment Consigned but not forecasted
    CURSOR notforecast_cur_part
    IS
        SELECT DISTINCT t4042.c205_part_number_id part_id, v9001.month_value, 0 item_qty
           FROM t4042_demand_sheet_detail t4042, v9001_month_list v9001
          WHERE t4042.c4040_demand_sheet_id    = p_demand_sheet_id
            AND t4042.c4042_ref_id             = '-9999'
            AND v9001.month_value             >= p_from_period
            AND v9001.month_value             <= p_to_period
            AND t4042.c901_type                = 50560
            AND t4042.c205_part_number_id NOT IN
            (
                SELECT DISTINCT b.c205_part_number_id
                   FROM t4042_demand_sheet_detail b
                  WHERE b.c4040_demand_sheet_id = p_demand_sheet_id
                    AND b.c4042_ref_id          = '-9999'
                    AND b.c901_type             = 50563
            ) ;
    -- SQL to fetch par level not consigned not forcasted
    CURSOR notforecast_cur_par
    IS
        SELECT DISTINCT c205_part_number_id part_id, v9001.month_value, 0 item_qty
           FROM t4022_demand_par_detail t4022, v9001_month_list v9001
          WHERE c4020_demand_master_id         = p_demand_id
            AND v9001.month_value             >= p_from_period
            AND v9001.month_value             <= p_to_period
            AND t4022.c205_part_number_id NOT IN
            (
                SELECT DISTINCT b.c205_part_number_id
                   FROM t4042_demand_sheet_detail b
                  WHERE b.c4040_demand_sheet_id = p_demand_sheet_id
                    AND b.c4042_ref_id          = '-9999'
                    AND b.c901_type             = 50563
            ) ;
BEGIN
    --
    -- load forecast set information
    FOR set_val IN forecast_cur_set
    LOOP
        --
        -- Save the value
        gm_pkg_op_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id, set_val.groupid, 50563, set_val.month_value,
        set_val.c205_part_number_id, set_val.item_qty, NULL, NULL, NULL) ;
        --
    END LOOP;
    --
    -- load forecast part information
    FOR set_val IN forecast_cur_part
    LOOP
        --
        -- Save the value
        gm_pkg_op_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id, '-9999', 50563, set_val.month_value,
        set_val.part_id, set_val.item_qty, NULL, NULL, NULL) ;
        --
    END LOOP;
    --
    -- load forecast part that not part it item consignment load
    FOR set_val IN notforecast_cur_part
    LOOP
        --
        -- Save the value
        gm_pkg_op_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id, '-9999', 50563, set_val.month_value,
        set_val.part_id, set_val.item_qty, NULL, NULL, NULL) ;
        --
    END LOOP;
    --
    -- load forecast part part not part of item consignment
    FOR set_val IN notforecast_cur_par
    LOOP
        --
        -- Save the value
        gm_pkg_op_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id, '-9999', 50563, set_val.month_value,
        set_val.part_id, set_val.item_qty, NULL, NULL, NULL) ;
        --
    END LOOP;
    --
    gm_pkg_op_ld_consign_demand.gm_op_ld_frcst_lnr_writeoff(p_demand_id,p_demand_sheet_id,p_from_period,p_to_period);
END gm_op_ld_forecast;
/*******************************************************
* Purpose: function is used to fetch consignment avg for the specified periof
*******************************************************/
FUNCTION get_demand_item_consig_avg (
        p_demand_sheet_id IN t4040_demand_sheet.c4040_demand_sheet_id%TYPE,
        p_partnum         IN t205_part_number.c205_part_number_id%TYPE)
    RETURN NUMBER
IS
    v_avg_consignment_qty NUMBER;
BEGIN
     SELECT NVL (ROUND (AVG (c4042_qty), 0), 0)
       INTO v_avg_consignment_qty
       FROM t4042_demand_sheet_detail t4042
      WHERE t4042.c4040_demand_sheet_id = p_demand_sheet_id
        AND t4042.c205_part_number_id   = p_partnum
        AND t4042.c4042_ref_id          = '-9999'
        AND t4042.c901_type             = 50560;
    RETURN v_avg_consignment_qty;
EXCEPTION
WHEN NO_DATA_FOUND THEN
    RETURN 0;
END get_demand_item_consig_avg;
/*************************************************************************
* Purpose: Procedure used to load sheet par information
*************************************************************************/
PROCEDURE gm_op_ld_par_value (
        p_demand_id       IN t4020_demand_master.c4020_demand_master_id%TYPE,
        p_demand_sheet_id IN t4040_demand_sheet.c4040_demand_sheet_id%TYPE,
        p_to_period       IN DATE)
AS
    --
    CURSOR forecast_calc_cur
    IS
        SELECT DISTINCT t4042.c4042_ref_id, t4042.c205_part_number_id, NVL (pvalue.parvalue, 0) parvalue
           FROM t4042_demand_sheet_detail t4042, (
                 SELECT c205_part_number_id, c4022_par_value parvalue
                   FROM t4022_demand_par_detail
                  WHERE c4020_demand_master_id = p_demand_id
            )
        pvalue
      WHERE t4042.c4040_demand_sheet_id = p_demand_sheet_id
        AND c4042_ref_id                = '-9999'
        AND c901_type                   = 50563
        AND t4042.c205_part_number_id   = pvalue.c205_part_number_id(+);
BEGIN
    -- load par information
    FOR set_val IN forecast_calc_cur
    LOOP
        -- Load par Info
        gm_pkg_op_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id, set_val.c4042_ref_id, 50566, p_to_period,
        set_val.c205_part_number_id, set_val.parvalue, NULL, NULL, NULL) ;
        --
    END LOOP;
    --
END gm_op_ld_par_value;
--
--
/*************************************************************************
* Purpose: Procedure used to load Pending Requests
*************************************************************************/
PROCEDURE gm_op_ld_pending_request (
        p_demand_master_id IN t4020_demand_master.c4020_demand_master_id%TYPE,
        p_demand_sheet_id  IN t4040_demand_sheet.c4040_demand_sheet_id%TYPE,
        p_from_period      IN DATE,
        p_inventory_id     IN t250_inventory_lock.c250_inventory_lock_id%TYPE)
AS
    --
    CURSOR prequest_calc_cur
    IS
         SELECT temp.ref_id ref_id, temp.p_num p_num, SUM (NVL (temp.qty, 0)) qty
          , DECODE (temp.sfl, 10, 50567, 15, 50568, 20, 50569) code_id
           FROM
            (
                 SELECT NVL (t520.c207_set_id, - 9999) ref_id, t521.c205_part_number_id p_num, t521.c521_qty qty
                  , DECODE (c520_status_fl, 30, 20, c520_status_fl) sfl, t520.c520_required_date rdate
                   FROM t253c_request_lock t520, t253d_request_detail_lock t521
                  WHERE t520.c520_request_txn_id    = p_demand_master_id
                    AND t520.c901_request_source    = '50616'
                    AND t520.c520_request_id        = t521.c520_request_id
                    AND t520.c250_inventory_lock_id = p_inventory_id
                    AND t520.c250_inventory_lock_id = t521.c250_inventory_lock_id
                    AND t520.c520_void_fl          IS NULL
                    --AND t520.c520_delete_fl IS NULL
                    AND t520.c520_status_fl     > 0
                    AND t520.c520_status_fl     < 40
                    AND t520.c520_required_date < p_from_period
          UNION ALL
             SELECT NVL (t520.c207_set_id, - 9999) ref_id, t505.c205_part_number_id p_num, t505.c505_item_qty qty
              , DECODE (c520_status_fl, 30, 20, c520_status_fl) sfl, t520.c520_required_date rdate
               FROM t253c_request_lock t520, t253a_consignment_lock t504, t253b_item_consignment_lock t505
              WHERE t520.c520_request_txn_id    = p_demand_master_id
                AND t520.c901_request_source    = '50616'
                AND t520.c250_inventory_lock_id = p_inventory_id
                AND t520.c520_request_id        = t504.c520_request_id
                AND t520.c250_inventory_lock_id = t504.c250_inventory_lock_id
                AND t504.c504_consignment_id    = t505.c504_consignment_id
                AND t504.c250_inventory_lock_id = t505.c250_inventory_lock_id
                AND t520.c520_void_fl          IS NULL
                --AND t520.c520_delete_fl IS NULL
                AND t520.c520_status_fl     > 0
                AND t520.c520_status_fl     < 40
                AND t520.c520_required_date < p_from_period
            )
            temp
       GROUP BY temp.ref_id, temp.p_num, temp.sfl;
        CURSOR open_requests
        IS
             SELECT c520_request_id request_id, c520_status_fl sfl
               FROM t253c_request_lock t520
              WHERE t520.c520_request_txn_id    = p_demand_master_id
                AND t520.c250_inventory_lock_id = p_inventory_id
                AND t520.c901_request_source    = '50616'
                AND t520.c520_void_fl          IS NULL
                --AND t520.c520_delete_fl IS NULL
                AND t520.c520_status_fl          > 0
                AND t520.c520_status_fl          < 40
                AND t520.c520_master_request_id IS NULL;
    BEGIN
        -- load pending request information
        FOR val IN prequest_calc_cur
        LOOP
            gm_pkg_op_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id, val.ref_id, val.code_id, p_from_period,
            val.p_num, val.qty, NULL, NULL, NULL) ;
            --
        END LOOP;
        FOR val IN open_requests
        LOOP
            gm_pkg_op_ttp_process_request.gm_sav_demand_sheet_request (p_demand_sheet_id, val.request_id, 50632,
            val.sfl) ;
        END LOOP;
        --
    END gm_op_ld_pending_request;
    --
    --
    /*************************************************************************
    * Purpose: Procedure used to capture the DEMAND of all the written off Parts in the
    * Loaner Reconciliation process.
    *************************************************************************/
PROCEDURE gm_op_ld_demand_lnr_writeoff (
        p_demand_id       IN t4020_demand_master.c4020_demand_master_id%TYPE,
        p_demand_sheet_id IN t4040_demand_sheet.c4040_demand_sheet_id%TYPE,
        p_from_period     IN DATE,
        p_to_period       IN DATE)
AS
    CURSOR write_off_part_cur
    IS
         SELECT partinfo.c205_part_number_id --, partinfo.month_value
         ,consinfo.sdate, NVL (c_qty, 0) item_qty
           FROM
            (
                SELECT DISTINCT t208.c205_part_number_id --, v9001.month_value
                   FROM t208_set_details t208, t4021_demand_mapping t4021, v9001_month_list v9001
                  WHERE t4021.c4020_demand_master_id = p_demand_id
                    AND t4021.c901_ref_type          = 40031
                    AND t4021.c4021_ref_id           = t208.c207_set_id
--                    AND v9001.month_value           >= p_from_period
                    --AND v9001.month_value           <= p_to_period
                    AND t208.c208_void_fl           IS NULL
            )
        partinfo, (
             SELECT TO_DATE ('01/' || TO_CHAR (t413.c413_created_date, 'MM/YYYY'), 'DD/MM/YYYY') sdate,
               t413.c205_part_number_id p_num, SUM (t413.c413_item_qty) c_qty
               FROM t412_inhouse_transactions t412, t413_inhouse_trans_items t413
              WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
                AND t412.c412_status_fl        = 4
                AND t412.c412_void_fl         IS NULL
                AND t413.c413_status_fl        = 'W'
                AND t413.c413_void_fl         IS NULL
                   AND t413.c413_created_date >= p_from_period
                   AND t413.c413_created_date <= p_to_period
                 GROUP BY t413.c205_part_number_id, TO_CHAR (t413.c413_created_date, 'MM/YYYY')
        )
        consinfo
      WHERE partinfo.c205_part_number_id = consinfo.p_num
        --AND partinfo.month_value         = consinfo.sdate(+)
        --AND NVL (c_qty, 0)               > 0
        ;
BEGIN
    FOR write_off_parts IN write_off_part_cur
    LOOP
        gm_pkg_op_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id, '-99991', -- Write off part group id
        50560, -- DEMAND
        --write_off_parts.month_value,
        write_off_parts.sdate,
        write_off_parts.c205_part_number_id, write_off_parts.item_qty, NULL, NULL, NULL) ;
    END LOOP;
END gm_op_ld_demand_lnr_writeoff;

/*************************************************************************
    * Purpose: Procedure used to capture the FORECAST of all the written off Parts in the
    * Loaner Reconciliation process captured in the DEMAND.
    *************************************************************************/
PROCEDURE gm_op_ld_frcst_lnr_writeoff(
		p_demand_id       IN t4020_demand_master.c4020_demand_master_id%TYPE,
        p_demand_sheet_id IN t4040_demand_sheet.c4040_demand_sheet_id%TYPE,
        p_from_period     IN DATE,
        p_to_period       IN DATE)
AS
	v_dmd_from_period DATE;
	v_dmd_to_period DATE;
	v_demand_period t4020_demand_master.c4020_demand_period%TYPE;
	v_inc_current_fl t4020_demand_master.c4020_include_current_fl%TYPE;

	CURSOR write_off_parts_cur
	IS
	
SELECT --DISTINCT 
t4042.c205_part_number_id part_id,v9001.month_value, 
-- When the part is not shared, then directly get the average forecast .	
DECODE(NVL(t205.c205_crossover_fl,'N'),'N',ROUND(SUM(c4042_qty)/v_demand_period),
	DECODE(gm_pkg_op_ld_consign_demand.get_cmp_part_demand_ttp(p_demand_sheet_id,t4042.c205_part_number_id)
		,'Y',ROUND(SUM(c4042_qty)/v_demand_period,0)))item_qty
           FROM t4042_demand_sheet_detail t4042,v9001_month_list v9001,t205_part_number t205
          WHERE t4042.c4040_demand_sheet_id    = p_demand_sheet_id
            AND t4042.c4042_ref_id             = '-99991'
            AND v9001.month_value             >= p_from_period
            AND v9001.month_value              <= p_to_period
            AND t4042.c901_type                = 50560
            AND t205.c205_part_number_id = t4042.c205_part_number_id
            GROUP BY t4042.c205_part_number_id,v9001.month_value,t205.c205_crossover_fl
            ;   
BEGIN
	
	SELECT	   c4020_demand_period , c4020_include_current_fl
			  INTO   v_demand_period , v_inc_current_fl
			  FROM t4020_demand_master
			 WHERE c4020_demand_master_id = p_demand_id;
			 
	-- Get the Demand Period , this will be used to get the write off parts in the demand period,to calculate the forecast.
	gm_pkg_op_ld_demand.gm_op_calc_period (v_demand_period, v_inc_current_fl, 'D', v_dmd_from_period, v_dmd_to_period) ;
	
	  FOR write_off_parts IN write_off_parts_cur
    LOOP
        --
        -- Save the value
        gm_pkg_op_ld_demand.gm_op_sav_sheet_detail (p_demand_sheet_id, '-99991', 50563, write_off_parts.month_value,
        write_off_parts.part_id, write_off_parts.item_qty, NULL, NULL, NULL) ;
        --
    END LOOP;
    
END gm_op_ld_frcst_lnr_writeoff;
 
/*  This function will get the TTP ID of the part and compare it with the TTP ID of the Demand Sheet.
 *  If the part is shared, then from the part find the primary group the part belongs to.
 *  From the primary group, get the demand master it is mapped to.
 *  From the Demand Master get the TTP ID.
 *  For the passed in Demand Sheet id, get the Demand Master and get the TTP ID.
 *  If the TTP of the Part and the Demand Master is same, then return the flag.
 */
FUNCTION get_cmp_part_demand_ttp(p_demand_sheet_id IN t4040_demand_sheet.c4040_demand_sheet_id%TYPE,
	p_part_num IN T205_PART_NUMBER.c205_part_number_id%TYPE)
	RETURN VARCHAR
IS	
	v_avg_forecast NUMBER;
	v_shared_part VARCHAR2(1);
	v_group_ID t4011_group_detail.c4010_group_id%TYPE;
	v_part_dm_id t4021_demand_mapping.c4020_demand_master_id%TYPE;
	v_part_ttp_id t4050_ttp.c4050_ttp_id%TYPE;
	v_dmd_ttp_id t4021_demand_mapping.c4020_demand_master_id%TYPE;
	v_load_forecast VARCHAR2(1)  :='Y';
	v_is_ttp_same VARCHAR2(1) := 'Y';
BEGIN
	SELECT NVL(c205_crossover_fl,'N') INTO v_shared_part FROM T205_PART_NUMBER WHERE C205_PART_NUMBER_ID=p_part_num;
	
	IF v_shared_part ='Y'
	THEN
		v_load_forecast := '';
		
		BEGIN
			-- For the passed in Part, get the Primary Group the part belongs to.
			select to_char(t4010.c4010_group_id) INTO v_group_ID from t4011_group_detail t4011, t4010_group t4010
			where t4011.c205_part_number_id=p_part_num
			and t4010.c4010_group_id = t4011.c4010_group_id
			and t4010.c4010_void_fl is null 
			and t4011.c901_part_pricing_type = 52080
			and t4010.c901_type=40045 ;
			
			-- From the Primary group,get the Demand Sheet the group is mapped to.
			select t4021.c4020_demand_master_id into v_part_dm_id from t4021_demand_mapping t4021,t4020_demand_master t4020
			where t4021.c4021_ref_id =to_char(v_group_ID)
			and t4020.c4020_void_fl is null
			and t4020.c4020_inactive_fl is null
			and t4020.c4020_demand_master_id = t4021.c4020_demand_master_id;
			
			-- From the Demand Master ID, get the TTP ID.
			select t4050.c4050_ttp_id INTO v_part_ttp_id from t4051_ttp_demand_mapping t4051,t4050_ttp t4050 where 
			t4051.c4020_demand_master_id = v_part_dm_id 
			and t4050.c4050_void_fl is null and t4051.c4050_ttp_id = t4050.c4050_ttp_id;
			
			-- From the Demand Sheet ID, get the TTP ID 
			select t4050.c4050_ttp_id INTO v_dmd_ttp_id from t4051_ttp_demand_mapping t4051,t4050_ttp t4050
			where 
			t4051.c4020_demand_master_id = (select c4020_demand_master_id FROM t4040_demand_sheet where
			C4040_VOID_FL IS NULL AND 
			c4040_demand_sheet_id= p_demand_sheet_id) 
			and t4050.c4050_void_fl is null and t4051.c4050_ttp_id = t4050.c4050_ttp_id;
			
			-- If the TTP ID matches, then the forecast should be loaded in the current demand sheet.
			IF (v_dmd_ttp_id = v_part_ttp_id)
			THEN
				v_is_ttp_same := 'Y';
			ELSE 
				v_is_ttp_same := 'N';
			END IF;
		EXCEPTION WHEN OTHERS -- To handle the exact fetch error when the group is mapped to multple demand sheet.
		THEN
			v_is_ttp_same := 'N';
		END;
	END IF;
	
	RETURN v_is_ttp_same;
	
END get_cmp_part_demand_ttp;
END gm_pkg_op_ld_consign_demand;
/
