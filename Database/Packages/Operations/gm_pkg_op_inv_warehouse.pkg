--@"c:\database\packages\operations\GM_PKG_OP_INV_WAREHOUSE.PKG";

create or replace
PACKAGE gm_pkg_op_inv_warehouse
IS
 /**********************************************************************
  * Description :  This function will get the Warehouse Name for the passed in Warehouse ID or the Location ID.
  * Author  : Elango
  **********************************************************************/
FUNCTION GET_INV_WAREHOUSE_NM (
    p_location_id t5052_location_master.c5052_location_id%TYPE ,
    p_warehouse_id T5051_INV_WAREHOUSE.C5051_INV_WAREHOUSE_ID%TYPE 
  )
  RETURN VARCHAR2;
  /*******************************************************
   * Description : Function to get warehouse Short name
   * Author 		  : Yoga
   *******************************************************/
  FUNCTION get_warehouse_sh_name (
    p_wh_type  IN  t5051_inv_warehouse.C901_WAREHOUSE_TYPE%TYPE,
    p_wh_id  IN  t5051_inv_warehouse.C5051_INV_WAREHOUSE_ID%TYPE
  )
  RETURN VARCHAR;
  
  /*******************************************************
   * Description : Procedure to fetch Warehouse details fo given transaction id
   * author      : Yoga
   *******************************************************/
  PROCEDURE gm_fch_warehouse_dtl (
    p_txn_type	   IN		NUMBER,
    p_out_dtl	   OUT		TYPES.cursor_type
  );

  /*******************************************************
   * Description : Function to get warehouse type
   * Author 		  : Yoga
   *******************************************************/
  FUNCTION get_warehouse_name (
    p_wh_type  IN  t901_code_lookup.C901_code_id%TYPE,
    p_wh_id  IN  t5051_inv_warehouse.C5051_INV_WAREHOUSE_ID%TYPE
  )
  RETURN varchar;
  /*******************************************************
   * Description : Function to get warehouse Id
   * Author      : Yoga
   *******************************************************/
  FUNCTION get_warehouse_id (
    p_wh_type  IN  t901_code_lookup.C901_code_id%TYPE
  )
  RETURN NUMBER;
  /**************************************************************************************
   * Description : This Procedure is used to fetch the excluded inventory buckets mapping 
   * by sutracting the included and excluded inventory buckets
   * Author      : HReddi 
   **************************************************************************************/
   PROCEDURE gm_fch_InvExclude_Map(
      p_includeInvBuckets   IN   VARCHAR2,
      p_excludeInvBuckets   IN   VARCHAR2,
      p_excluded_mapping    OUT  TYPES.cursor_type
   ); 
   
      /*******************************************************
   *	Description : Procedure to fetch Warehouse details
   *	Author		: Jignesh Shah
   *******************************************************/
   PROCEDURE gm_fetch_inv_warehouse_dtls(
     p_txn_type		IN		NUMBER,
     p_code_grp		IN		VARCHAR2,
     p_dept_id		IN		NUMBER,
     p_out_dtl		OUT		TYPES.cursor_type
   );
   
   /*********************************************************************************************************************
    * Description : This procedure will get the warehouse address details for FGRP-paerwork if warehouse difference only.
    * Author  : HReddi
    **********************************************************************************************************************/
	PROCEDURE gm_fch_picslip_info(
    p_transid IN t5055_control_number.c5055_ref_id%TYPE ,
    p_transtype IN t5055_control_number.c901_ref_type%TYPE ,
    p_wh_fromaddr OUT VARCHAR2 ,
    p_wh_toaddr OUT VARCHAR2 ,
    p_wh_consign_name OUT t701_distributor. C701_DISTRIBUTOR_NAME%TYPE ,
    p_packslip_header OUT VARCHAR2 );
    
   /*************************************************************************
    * Description : Procedure to fetch Part Reserved Details for Ack Order
    * vAuthor		: Harinadh Reddi
    **************************************************************************/
   PROCEDURE gm_fetch_part_resrve_dtls (
     p_part_number	IN		t5060_control_number_inv.c205_part_number_id%TYPE,
     p_trans_id     IN      VARCHAR2,
     p_out_dtl		OUT		TYPES.cursor_type
   );
   
  /***********************************************************************
   * Description : Procedure to save Part Reserved Details for Ack Order
   * Author		: Harinadh Reddi
   **************************************************************************/
   PROCEDURE gm_save_part_resrve(
     p_input_String	IN		VARCHAR2,
     p_user_id      IN      VARCHAR2    
   );   
   
    /************************************************************************************
   * Description : Function to get the Reserved Quantity for the Corresponding Ack Order
   * Author		 : Harinadh Reddi
   *************************************************************************************/
   	FUNCTION GET_RESERVED_QTY (
    	p_trans_id T5062_Control_Number_Reserve.C5062_Transaction_Id%TYPE ,
    	p_part_num T5060_Control_Number_Inv.C205_Part_Number_Id%TYPE 
   	)
   	RETURN VARCHAR2;
   	
   		/*************************************************************************
    * Description : Procedure to fetch Part Reserved Details for Ack Order
    * Author		: Harinadh Reddi
    **************************************************************************/
   PROCEDURE gm_fch_reserved_qty (
     p_trans_id     IN      VARCHAR2,
     p_out_dtl		OUT		TYPES.cursor_type
   );
   
    /************************************************************************************************
	 * Description : This Procedure is used to fetch the Current Warehouse for input Part and Control
	 * Author	   : HReddi
	 *************************************************************************************************/
	PROCEDURE gm_fetch_curr_warehouse (
	  p_part_num		IN		VARCHAR2
	, p_cntl_num		IN		VARCHAR2
	, p_out_warehouse   OUT	   	VARCHAR2
	);
	
	/************************************************************************************************************
	 * Description : This Procedure is used to check the lot is enabled for the company and check in T2550 table
	 * Author	   : karthik
	 ********************************************************************************************************/
	PROCEDURE gm_chk_lot_track_enable(
	  p_part_num		IN		VARCHAR2
	, p_cntl_num		IN		VARCHAR2
	, p_company_id      IN	   	t1900_company.c1900_company_id%TYPE
	, p_out_flag        OUT     VARCHAR2
	);
   
  END gm_pkg_op_inv_warehouse;
  /