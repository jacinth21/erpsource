/* Formatted on 2010/09/13 18:50 (Formatter Plus v4.8.0) */
-- @"c:\database\packages\operations\gm_pkg_op_account_request.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_account_request
IS
/**********************************************************************
   * Description : Procedure to initiate a request
   * Author 	 : Joe P Kumar
  **********************************************************************/
-- This procedure is calling by ETL Job, please coordinate with DWH team during design phase
	PROCEDURE gm_sav_initiate_request (
		p_request_id		  IN	   t520_request.c520_request_id%TYPE
	  , p_required_date 	  IN	   Date
	  , p_request_source	  IN	   t520_request.c901_request_source%TYPE
	  , p_request_txn_id	  IN	   t520_request.c520_request_txn_id%TYPE
	  , p_set_id			  IN	   t520_request.c207_set_id%TYPE
	  , p_request_for		  IN	   t520_request.c520_request_for%TYPE
	  , p_request_to		  IN	   t520_request.c520_request_to%TYPE
	  , p_request_by_type	  IN	   t520_request.c901_request_by_type%TYPE
	  , p_request_by		  IN	   t520_request.c520_request_by%TYPE
	  , p_ship_to			  IN	   t520_request.c901_ship_to%TYPE
	  , p_ship_to_id		  IN	   t520_request.c520_ship_to_id%TYPE
	  , p_master_request_id   IN	   t520_request.c520_master_request_id%TYPE
	  , p_status_fl 		  IN	   t520_request.c520_status_fl%TYPE
	  , p_userid			  IN	   t520_request.c520_created_by%TYPE
	  , p_inputstring		  IN	   CLOB
	  , p_purpose			  IN	   t520_request.c901_purpose%TYPE
	  , p_rep_id			  IN	   t525_product_request.c703_sales_rep_id%TYPE
	  , p_acc_id			  IN	   t525_product_request.c704_account_id%TYPE
	  , p_inpstr			  IN 	   VARCHAR2
	  , p_planshipdate		  IN 	   t520_request.C520_PLANNED_SHIP_DATE%TYPE
	  , p_assoc_rep_id		  IN	   t525_product_request.c703_ass_rep_id%TYPE
	  , p_out_request_id	  OUT	   t520_request.c520_request_id%TYPE
	  , p_out_consign_id	  OUT	   t504_consignment.c504_consignment_id%TYPE
	  , p_comments 		 	  IN 	   T520_REQUEST.C520_COMMENTS%TYPE DEFAULT NULL
	  , p_loaner_req_id       IN       T520_REQUEST.C525_PRODUCT_REQUEST_ID%TYPE DEFAULT NULL
	)
	-- --p_comments is used for additional info of OUS (loaner/item initiate)
	AS
	BEGIN
		IF p_request_for = 40021 OR p_request_for = 40025 OR p_request_for = 40022 OR p_request_for = 102930
		THEN
					gm_sav_initiate_item (p_request_id
							, p_required_date
							, p_request_source
							, p_request_txn_id
							, p_request_for
							, p_request_to
							, p_request_by_type
							, p_request_by
							, p_ship_to
							, p_ship_to_id
							, p_master_request_id
							, p_status_fl
							, p_userid
							, p_inputstring
							, p_purpose
							, p_inpstr
							, p_planshipdate
							, p_out_request_id
							, p_out_consign_id
							, p_comments
							, p_loaner_req_id
							);
		ELSE
		  DELETE FROM my_temp_list;
					gm_pkg_op_product_requests.gm_sav_product_requests (p_required_date
											  , p_request_for
											  , p_request_to
											  , p_request_by_type
											  , p_request_by
											  , p_ship_to
											  , p_ship_to_id
											  , p_userid
											  , p_inputstring
											  , p_rep_id
											  , p_acc_id
											  , p_out_request_id
											  , p_inpstr
											  , p_assoc_rep_id
											   );
		 --find any obsolete sets are added,It will throw app error
		 gm_pkg_op_product_requests.gm_validate_set_obsolete_sts();
		END IF;
	END gm_sav_initiate_request;

/**********************************************************************
   * Description : Procedure to initiate request for item
   * Author 	 : Joe P Kumar
  **********************************************************************/
	PROCEDURE gm_sav_initiate_item (
		p_request_id		  IN	   t520_request.c520_request_id%TYPE
	  , p_required_date 	  IN	   Date
	  , p_request_source	  IN	   t520_request.c901_request_source%TYPE
	  , p_request_txn_id	  IN	   t520_request.c520_request_txn_id%TYPE
	  , p_request_for		  IN	   t520_request.c520_request_for%TYPE
	  , p_request_to		  IN	   t520_request.c520_request_to%TYPE
	  , p_request_by_type	  IN	   t520_request.c901_request_by_type%TYPE
	  , p_request_by		  IN	   t520_request.c520_request_by%TYPE
	  , p_ship_to			  IN	   t520_request.c901_ship_to%TYPE
	  , p_ship_to_id		  IN	   t520_request.c520_ship_to_id%TYPE
	  , p_master_request_id   IN	   t520_request.c520_master_request_id%TYPE
	  , p_status_fl 		  IN	   t520_request.c520_status_fl%TYPE
	  , p_userid			  IN	   t520_request.c520_created_by%TYPE
	  , p_inputstring		  IN	   CLOB
	  , p_purpose			  IN	   t520_request.c901_purpose%TYPE
	  , p_inpstr			  IN 	   VARCHAR2
	  , p_shipdate			  IN 	   t520_request.C520_PLANNED_SHIP_DATE%TYPE
	  , p_out_request_id	  OUT	   t520_request.c520_request_id%TYPE
	  , p_out_consign_id	  OUT	   t504_consignment.c504_consignment_id%TYPE
	  , p_comments 		 	  IN 	   T520_REQUEST.C520_COMMENTS%TYPE DEFAULT NULL
	  , p_loaner_req_id       IN       T520_REQUEST.C525_PRODUCT_REQUEST_ID%TYPE DEFAULT NULL
	)
	AS
		v_partnum	   t205_part_number.c205_part_number_id%TYPE;
		v_shelfqty	   t208_set_details.c208_set_qty%TYPE;
		v_rwqty		   t208_set_details.c208_set_qty%TYPE;	
		v_reqqty	   t208_set_details.c208_set_qty%TYPE;
		v_child_material_request_bool BOOLEAN := FALSE;
		v_consign_first_time_flag BOOLEAN := FALSE;
		v504_type	   t504_consignment.c504_type%TYPE;
		v_account_id   t520_request.c520_request_to%TYPE;
		v_out_item_consign_id t505_item_consignment.c505_item_consignment_id%TYPE;
		v_out_request_detail_id t521_request_detail.c521_request_detail_id%TYPE;
		v_backorderqty t208_set_details.c208_set_qty%TYPE;
		v_backorder_request_id t520_request.c520_request_id%TYPE;
		v_required_date t520_request.c520_required_date%TYPE;
		v_strlen	   NUMBER := NVL (LENGTH (p_inputstring), 0);
		v_string	   CLOB := p_inputstring;
		v_substring    VARCHAR2 (1000);
		v_seq		   NUMBER;
		v_id		   VARCHAR2 (20);
		v_reqdate	   VARCHAR2 (40);
		v_part_price   NUMBER;
		v_allocate_fl  VARCHAR2 (1);
		v_ref_type 	   varchar2(20);
		v_back_order_master_req_id  t520_request.c520_request_id%TYPE;
		v_div_type 	VARCHAR2(20) := '100823'; -- US Distributor
		
		v_whtype		T505_ITEM_CONSIGNMENT.C901_WAREHOUSE_TYPE%TYPE;
		v_strwhtype		T505_ITEM_CONSIGNMENT.C901_WAREHOUSE_TYPE%TYPE;
		
		v_allocrwqty 	NUMBER := 0;
		v_left 			NUMBER := 0;
		v_ship_carrier 	VARCHAR2(20);
		v_ship_mode    	VARCHAR2(20);
		v_dist_type    	NUMBER;
		v_cntry_inv    	VARCHAR2(20);
		v_cntry	    	VARCHAR2(10);
		v_part_string  	CLOB;
		v_ship_string  	CLOB;
		v_ship_substring VARCHAR2(100);
		v_price_err_str  VARCHAR2(4000);
		v_lot_num        t505_item_consignment.C505_CONTROL_NUMBER%TYPE;
		v_company_id     t504_consignment.C1900_COMPANY_ID%TYPE;
		v_product_family t205_part_number.c205_product_family%TYPE;
	BEGIN
		SELECT DECODE
				   (p_request_for
				  , 40021, 4110
				  , 40025, 4110
				  , 102930, 4110
				  , 40022, 4112
				  , p_request_for
				   )   -- we are not using the value 40021/4110 (Consignment) of DMDTP. 20021 - Sales. 40022 - InHouse
			 , DECODE (p_request_for, 40022, '01', 40025,p_request_to, NULL)
		  INTO v504_type
			 , v_account_id
		  FROM DUAL;
		  
		  SELECT DECODE (p_required_date, NULL, SYSDATE, p_required_date)
		  INTO v_required_date
		  FROM DUAL;

		IF p_request_for <> '40025' THEN
		
		  v_div_type := NVL(get_distributor_division(p_request_to),'100823'); -- Fetch the US/OUS Distributor
		  -- get the distributor type
		 v_dist_type := GET_DISTRIBUTOR_TYPE (p_request_to);
		 IF p_request_for = 102930 AND NVL(v_dist_type,'-999') <> 70103
		 THEN
		 	raise_application_error ('-20999', 'Please select valid distributor for type ICT.'); --Please select valid distributor for type ICT
		 END IF;
		END IF;
		 
		SELECT NVL(GET_RULE_VALUE('US','COUNTRY'),'N') INTO v_cntry FROM DUAL;      
		--			3. Creating MR since we have to create it in any case
		IF v_strlen > 0
		THEN
		   -- following code added for resolving the bug for PMT-4359
			-- when ever we are initiating Item for one part having enough qty and one part is having nothing.
			-- this time Ship mode and Ship carrier is not saving in t504 due to incorrect v_string[124.000^1^09/04/2014^|124.000^1^09/04/2014^5001^5004|] comming from GmRequestTransBean.java
			-- now from bean we are getting the v_string as 124.000^1^09/04/2014^|124.000^1^09/04/2014^|~5001^5004| and we are doing susstring as follows...
			IF INSTR (v_string, '~') <> 0 THEN
				v_part_string := SUBSTR (v_string, 1, INSTR (v_string, '~') - 1);
				v_ship_string := SUBSTR (v_string, INSTR (v_string, '~') + 1);
				v_string := v_part_string;
			ELSE
				v_string := v_string;
			END IF;		
			
			IF INSTR (v_ship_string, '|') <> 0 THEN
				v_ship_substring := SUBSTR (v_ship_string, 1, INSTR (v_ship_string, '|') - 1);
				v_ship_carrier := SUBSTR (v_ship_substring, 1, INSTR (v_ship_substring, '^') - 1);
				v_ship_substring := SUBSTR (v_ship_substring, INSTR (v_ship_substring, '^') + 1);
				v_ship_mode := v_ship_substring;
			END IF;
			
			WHILE INSTR (v_string, '|') <> 0
			LOOP
				--
				v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
				v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
				--
				v_partnum	:= NULL;
				v_reqqty	:= NULL;
				--
				v_partnum	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_reqqty	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_reqdate	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_strwhtype	:= v_substring;
				
				SELECT get_qty_in_stock (v_partnum), get_qty_in_inv(v_partnum,'56001')
					  INTO v_shelfqty, v_rwqty
					  FROM t205_part_number
					 WHERE c205_part_number_id = v_partnum
				FOR UPDATE;
				
				--Below code is for inhouse item consignment US only
				IF v504_type = 4112 AND v_strwhtype = '90800' THEN
					v_rwqty:= 0;
				ELSIF v504_type = 4112 AND v_strwhtype = '56001' THEN
					v_shelfqty := v_rwqty;
				END IF;
				--
				
				--Below code is for item consignment and US/OUS only
				IF v_div_type = '100824' -- OUS Distributor
				THEN
					IF v_rwqty < 0
					THEN
						v_rwqty :=0;
					END IF;
				ELSE -- US Distributor
					v_rwqty := 0;
				END IF;
				--
				
				IF v_shelfqty < 0
				THEN
					v_shelfqty	:= 0;
				END IF;

				-- 4. Since we do orderby on bulkqty and the first bulkqty is > 0, we initiate a consignment
				IF (NOT v_consign_first_time_flag AND (v_shelfqty > 0 OR v_rwqty > 0))
				THEN
					gm_pkg_op_account_req_master.gm_sav_request (p_request_id
														   , v_required_date
														   , p_shipdate
														   , p_request_source
														   , p_request_txn_id
														   , NULL
														   , p_request_for
														   , p_request_to
														   , p_request_by_type
														   , p_request_by
														   , p_ship_to
														   , p_ship_to_id
														   , p_master_request_id
														   , 15
														   , p_userid
														   , p_purpose
														   , p_out_request_id
														   , p_comments
														   ,p_loaner_req_id
															);
				
					
					IF p_inpstr is not null
					THEN
						gm_pkg_op_account_req_master.gm_sav_request_attribute(
																	p_out_request_id,
																	p_inpstr,
																	p_userid
																	);
					END IF;
					gm_pkg_op_account_req_master.gm_sav_consignment (p_request_to
															   , NULL
															   , NULL
															   , NULL
															   , NULL
															   , '2'   -- Status Flag (Pending Control
															   , NULL
															   , v_account_id
															   , p_ship_to
															   , NULL
															   , p_ship_to_id
															   , p_comments
															   , p_purpose
															   , v504_type
															   , v_ship_carrier -- Shipp Carrier
															   , v_ship_mode  -- Ship Mode
															   , NULL
															   , '1'   --Ship Req Flag
															   , NULL
															   , NULL
															   , NULL
															   , NULL
															   , NULL
															   , NULL
															   , p_out_request_id
															   , p_userid
															   , p_out_consign_id
																);
					v_consign_first_time_flag := TRUE;
				-- fetching company id from request id
					BEGIN
					SELECT C1900_COMPANY_ID INTO v_company_id FROM t520_request
					WHERE C520_REQUEST_ID= p_out_request_id;
		  			EXCEPTION
			 		WHEN NO_DATA_FOUND
					 THEN
					 	v_company_id := NULL;
					 END;
					
					  IF     v_backorder_request_id IS NOT NULL
                  AND p_out_request_id IS NOT NULL
                  AND v_back_order_master_req_id IS NULL
               THEN
                  UPDATE t520_request
                     SET c520_master_request_id = p_out_request_id
                       , c520_last_updated_by = p_userid
                       , c520_last_updated_date = SYSDATE
                   WHERE c520_request_id = v_backorder_request_id;

                  v_back_order_master_req_id := p_out_request_id;
               END IF;
					
				END IF;

				-- 5. If we have qty > 0 and shelf has qty lesser than req qty, then we are sure to create a backorder RQ
				IF (NOT v_child_material_request_bool AND (v_shelfqty + v_rwqty) < v_reqqty)
				THEN
					gm_pkg_op_account_req_master.gm_sav_request (NULL
														   , v_required_date
														   , p_shipdate
														   , p_request_source
														   , p_request_txn_id
														   , NULL
														   , p_request_for
														   , p_request_to
														   , p_request_by_type
														   , p_request_by
														   , p_ship_to
														   , p_ship_to_id
														   , p_out_request_id
														   , 10
														   , p_userid
														   , p_purpose
														   , v_backorder_request_id
														   , p_comments
														   ,p_loaner_req_id
															);
					IF p_inpstr is not null
					THEN												
						gm_pkg_op_account_req_master.gm_sav_request_attribute(
																	v_backorder_request_id,
																	p_inpstr,
																	p_userid
																	);
					END IF;
					
					
					-- for issue 4846 add this code for save sipping information
					IF ((p_out_request_id IS NULL) OR (p_out_request_id = ''))
					THEN
						p_out_request_id := v_backorder_request_id;
					END IF;

					v_child_material_request_bool := TRUE;
				END IF;

				v_part_price := get_part_price (v_partnum, 'C');

				IF TRIM (v_part_price) IS NULL
				THEN
					v_price_err_str := v_price_err_str || v_partnum || ', ';
					--raise_application_error (-20092, '');
				END IF;

				-- 6. Consign the parts which has sufficient bulkqty
				IF (v_shelfqty + v_rwqty)  >= v_reqqty
				THEN
				
					 IF v_rwqty >= v_reqqty
					 THEN
				      	v_allocrwqty := v_reqqty; 
				      	v_left := 0;      
				   	 ELSIF v_rwqty > 0   AND v_rwqty < v_reqqty 
				     THEN 
				     	v_allocrwqty := v_rwqty; 
				     	v_left := v_reqqty - v_rwqty; 
				     ELSE
				     	v_left := v_reqqty;
				   	 END IF;
				   	 
				   	 IF v_shelfqty >0 AND v_shelfqty < v_left
				   	 THEN
				   	 	v_left := v_shelfqty;
				   	 END IF;
				   	 	 
				   	v_lot_num := 'TBE';
			        -- ALW_INS_PART_NOC -Controlling the instrument part to allow NOC#, the NOC# is passing v_lot_num in save consignment details.		   	
		   	     	SELECT get_partnum_product(v_partnum) INTO  v_product_family FROM DUAL;
		   		   	IF get_rule_value_by_company(v_product_family,'ALW_INS_PART_NOC',v_company_id)='Y'  THEN
				   		
						v_lot_num := 'NOC#';
					
					END IF;
					
				   	IF v_allocrwqty >0 AND TRIM (v_part_price) IS NOT NULL
				   	THEN
				   		SELECT DECODE(GET_RULE_VALUE('US','COUNTRY'),'Y','56001',NULL) INTO v_cntry_inv FROM DUAL;	
				   		
					   	gm_pkg_op_account_req_master.gm_sav_consignment_detail (NULL
																	  , p_out_consign_id
																	  , v_partnum
																	  , v_lot_num
																	  , v_allocrwqty
																	  , v_part_price
																	  , NULL
																	  , NULL
																	  , NULL
																	  , v_cntry_inv
																	  , v_out_item_consign_id
																	   );
					v_allocrwqty:=0;
					END IF;
					
					IF v_left >0 AND TRIM (v_part_price) IS NOT NULL
				   	THEN
				   	
						IF v_cntry != 'Y' AND v_strwhtype IS NULL 
						THEN						
							v_strwhtype := 90800;	--OUS WAREHOUSE -- As a quick fix, update as 90800  
						
						ELSIF v_cntry = 'Y' AND v_strwhtype IS NULL 
						THEN						
							v_strwhtype := 90800;	--US WAREHOUSE							
						END IF;
				   	
					   	SELECT DECODE(v_cntry ,'Y', DECODE(v504_type, 4112, v_strwhtype,'90800'), v_strwhtype) INTO v_whtype FROM DUAL;
				   	
						gm_pkg_op_account_req_master.gm_sav_consignment_detail (NULL
																	  , p_out_consign_id
																	  , v_partnum
																	  , v_lot_num
																	  , v_left
																	  , v_part_price
																	  , NULL
																	  , NULL
																	  , NULL
																	  , v_whtype-- FG Warehouse
																	  , v_out_item_consign_id
																	   );
																	   
						v_left := 0;																	   
					END IF;
					
					
				END IF;

				IF  (v_shelfqty + v_rwqty)  < v_reqqty
				THEN
					-- 7. Calculate backorderQty and update request detail and item consignment accordingly
					v_backorderqty := v_reqqty - (v_shelfqty +v_rwqty);
					
					gm_pkg_op_account_req_master.gm_sav_request_detail (v_backorder_request_id
																  , v_partnum
																  , v_backorderqty
																  , v_out_request_detail_id
																   );

					IF (v_shelfqty + v_rwqty) > 0
					THEN
						 IF v_rwqty > 0 
						 THEN 
                            v_allocrwqty := v_rwqty; 
                            v_left := v_reqqty - v_allocrwqty; 
                 		 ELSE 
                          	v_left := v_reqqty;             
                 		 END IF; 

                 		 IF v_shelfqty >0 AND v_shelfqty < v_left
				   	 	 THEN
				   	 		v_left := v_shelfqty;
				   	 	 END IF;
				   	 
						IF v_allocrwqty >0 AND TRIM (v_part_price) IS NOT NULL
					   	THEN
					   		gm_pkg_op_account_req_master.gm_sav_consignment_detail (NULL
																		  , p_out_consign_id
																		  , v_partnum
																		  , v_lot_num
																		  , v_allocrwqty
																		  , v_part_price
																		  , NULL
																		  , NULL
																		  , NULL
																		  , '56001' -- RW Warehouse
																		  , v_out_item_consign_id
																		   );
							v_allocrwqty := 0;																		   
						END IF;
						
						IF v_left >0 AND v_shelfqty >0 AND v_left >= v_shelfqty AND TRIM (v_part_price) IS NOT NULL    -- Checked shelf qty count
						THEN
						v_left := v_shelfqty; 
						
							IF v_cntry != 'Y' AND v_strwhtype IS NULL 
							THEN						
								v_strwhtype := 90800;	--OUS WAREHOUSE -- As a quick fix, update as 90800 
							
							ELSIF v_cntry = 'Y' AND v_strwhtype IS NULL 
							THEN						
								v_strwhtype := 90800;	--US WAREHOUSE							
							END IF;
					   	
						   	SELECT DECODE(v_cntry ,'Y', DECODE(v504_type, 4112, v_strwhtype,'90800'), v_strwhtype) INTO v_whtype FROM DUAL; 
						   	
							gm_pkg_op_account_req_master.gm_sav_consignment_detail (NULL
																		  , p_out_consign_id
																		  , v_partnum
																		  , v_lot_num
																		  , v_left
																		  , v_part_price
																		  , NULL
																		  , NULL
																		  , NULL
																		  , v_whtype -- FG Warehouse
																		  , v_out_item_consign_id
																		   );
							v_left := 0;																
						END IF;
					END IF;
				END IF;
			END LOOP;
			
			IF TRIM (v_price_err_str) IS NOT NULL
			THEN
				raise_application_error (-20092, v_price_err_str);
			END IF;
			
		END IF;

		-- Code added for inventory pickup
		IF (p_out_consign_id IS NOT NULL)
		THEN
			-- Code added for allocation logic
			-- If the Transaction has Literature part, it should not be allocated to device.			
			v_allocate_fl := gm_pkg_op_inventory.chk_txn_details (p_out_consign_id, 93002);

			IF NVL (v_allocate_fl, 'N') = 'Y'
			THEN
			  select DECODE(v504_type,4110,93002,4112,93009,93002) INTO v_ref_type from dual;
			  gm_pkg_allocation.gm_ins_invpick_assign_detail (v_ref_type, p_out_consign_id, p_userid);
			END IF;
			
			--When ever the child is updated, the consignment table has to be updated ,so ETL can Sync it to GOP.
			UPDATE t504_consignment
			   SET c504_last_updated_by   = p_userid
			     , c504_last_updated_date = SYSDATE
			 WHERE c504_consignment_id = p_out_consign_id;	
		END IF;
	END gm_sav_initiate_item;
/**********************************************************************
   * Description : Procedure to validate Loaner Request Id   - PMT-32450
  **********************************************************************/
  PROCEDURE gm_Validate_loaner_ref_details (
	p_loaner_req_id         IN       t525_product_request.C525_PRODUCT_REQUEST_ID%TYPE,
	p_account_id            IN       t525_product_request.c704_account_id%TYPE,
	p_request_for           IN       t525_product_request.c525_request_for_id%TYPE DEFAULT NULL, 
	p_out_loaner_req_msg     OUT      VARCHAR2
  )
  AS
     v_company_id   T1900_COMPANY.C1900_COMPANY_ID%TYPE;
     v_comp_name   VARCHAR2(4000) ;
     v_err_msg     VARCHAR2(4000):=NULL;
     v_cmp_id      t525_product_request.c1900_company_id%TYPE DEFAULT NULL;
     v_account_id  t525_product_request.C704_ACCOUNT_ID%TYPE DEFAULT NULL;
     v_void_fl     t525_product_request.C525_VOID_FL%TYPE DEFAULT NULL;
     v_statusfl     t525_product_request.C525_STATUS_FL%TYPE DEFAULT NULL;
     v_account_name  VARCHAR2(4000);
     v_distributer  t525_product_request.c525_request_for_id%TYPE;
     v_distributer_name  VARCHAR2(4000);
     v_request_for VARCHAR2(4000) := p_request_for;
  BEGIN
	  BEGIN
		  SELECT C1900_COMPANY_ID,C704_ACCOUNT_ID,C525_VOID_FL,C525_STATUS_FL,C525_REQUEST_FOR_ID  INTO v_cmp_id ,v_account_id, v_void_fl,v_statusfl,v_distributer
		  FROM t525_product_request
		  where C525_PRODUCT_REQUEST_ID = p_loaner_req_id;
		  EXCEPTION WHEN NO_DATA_FOUND THEN
		  	v_err_msg:= v_err_msg || 'Please enter a valid Loaner Request ID.<BR>';
	  END;
	  
	   SELECT get_compid_frm_cntx () INTO v_company_id FROM DUAL;
	   SELECT get_company_name (v_cmp_id)INTO v_comp_name FROM DUAL;
	   SELECT get_account_name(p_account_id) INTO v_account_name FROM DUAL;
	   SELECT get_distributor_name(v_distributer) INTO v_distributer_name FROM DUAL;
	   --since in item screen we are not having the dropdown for account id(accounts), 
	   --instead of account id we are getting distributor from screen,hence we are checking the below condition 
	   --if the request is of consignment (40021),then we are assigning the distibutor to account_id
	   IF p_request_for = '40021' THEN
	          v_account_id:=  v_distributer ;
	           v_account_name:= v_distributer_name;
	    ELSIF p_request_for IS NULL THEN
	    	v_request_for := '-9999';
	    END IF;
	   --PMT-45058 Ability to link Loaner Request to Item consignment transaction
	 IF (v_err_msg is null)  THEN 
		 --40021= consignment
		
		  IF   p_account_id IS NOT NULL AND v_account_id <> p_account_id THEN
		     v_err_msg :=v_err_msg || 'The entered Request ID is not associated to <b>'||v_account_name ||'</b>.Please enter a valid Request ID<BR>';
		  END IF;
		  
	      IF v_request_for <> '40021' AND v_statusfl = '40'   THEN
		    v_err_msg := v_err_msg || 'The entered Request ID is closed. Please enter a valid Request ID.<BR>'; 
		  END IF;
	      IF v_void_fl = 'Y'   THEN
		    v_err_msg :=v_err_msg || 'The entered Request ID does not exist or has been voided. Please enter a valid Request ID.<BR>'; 
		  END IF;
		  IF v_cmp_id <> v_company_id THEN
		   v_err_msg :=v_err_msg || '<b>'|| p_loaner_req_id || '</b> has been created in  <b>' || v_comp_name ||'</b>.Please enter another request ID or login to the appropriate Company.<BR>'; 
		  END IF;
		  END IF;
	 p_out_loaner_req_msg := v_err_msg;
	
		 
  END gm_Validate_loaner_ref_details;
	
END gm_pkg_op_account_request;
/
