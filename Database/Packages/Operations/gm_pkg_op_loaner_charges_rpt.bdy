/* Formatted on 2009/07/06 15:57 (Formatter Plus v4.8.0) */
--@"c:\database\packages\operations\gm_pkg_op_loaner_charges_rpt.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_loaner_charges_rpt
IS
/***********************************************************
 * Description : This Procedure is used to get the late fee status name
 * Author	   : Sindhu
 ***********************************************************/
FUNCTION get_late_fee_status_name(
	p_status IN t9201_charge_details.c9201_status%TYPE
)
	RETURN VARCHAR2
AS
	v_status VARCHAR2(50);
BEGIN
	SELECT  DECODE(p_status,10,'Accrued',15,'Hold',20,'Submitted',25,'Credit', 40,'Waived','') 
	INTO v_status 
	FROM DUAL;
          
    RETURN v_status;
END get_late_fee_status_name;

/***********************************************************
 * Description : This Procedure is used to get the late fee status name
 * Author	   : Sindhu
 ***********************************************************/
FUNCTION get_late_fee_status_codeid(
	p_status IN t9201_charge_details.c9201_status%TYPE
)
	RETURN NUMBER
AS
	v_code_id NUMBER;
BEGIN
	 SELECT  C901_CODE_ID 
	   INTO v_code_id
	   FROM  T901_CODE_LOOKUP
       WHERE  C902_CODE_NM_ALT = p_status
       AND C901_CODE_GRP = 'FEE_ST';
          
    RETURN v_code_id;
END get_late_fee_status_codeid;

/***********************************************************
 * Description : This Procedure is used to get the working days
 * Author	   : Sindhu
 ***********************************************************/
FUNCTION get_deduction_date(
    p_sales_rep_id IN t703_sales_rep.c703_sales_rep_id%TYPE,
	p_rep_category IN t701_distributor.c901_distributor_type%TYPE,
	p_status       IN NUMBER
) 
	RETURN DATE
AS
	v_working_days      NUMBER;
	v_deduction_date   DATE;
	v_distributor_id t701_distributor.c701_distributor_id%TYPE;
	v_distributor_type t701_distributor.c901_distributor_type%TYPE;
BEGIN

	IF p_rep_category is NULL THEN
		SELECT c701_distributor_id INTO v_distributor_id 
		FROM t703_sales_rep
		WHERE c703_sales_rep_id = p_sales_rep_id;
		
		SELECT c901_distributor_type INTO v_distributor_type 
		FROM t701_distributor
		WHERE c701_distributor_id = v_distributor_id;
	ELSE
		v_distributor_type := p_rep_category;
	END IF;
		SELECT 
		CASE 
		WHEN v_distributor_type = 70100  --Distributor
		THEN get_distrep_deduction_date(p_status) 
	ELSE get_directrep_deduction_date(p_status) 
	END into v_deduction_date FROM DUAL;
	
	RETURN v_deduction_date;
END get_deduction_date;

/***********************************************************
 * Description : This Procedure is used to get the working days
 * Author	   : Sindhu
 ***********************************************************/
FUNCTION get_distrep_deduction_date(
	p_charge_status  IN NUMBER
) 
	RETURN DATE
AS
	v_deduction_date DATE;
	v_day  NUMBER;
BEGIN

          SELECT last_day(add_months(trunc(CURRENT_DATE),1))+15 into v_deduction_date FROM dual;

	RETURN v_deduction_date;

END get_distrep_deduction_date;

/***********************************************************
 * Description : This Procedure is used to get the working days
 * Author	   : Sindhu
 ***********************************************************/
FUNCTION get_directrep_deduction_date(
	p_charge_status  IN NUMBER
) 
	RETURN DATE
AS
	v_deduction_date DATE;
	v_day  NUMBER;
BEGIN
	 SELECT TO_CHAR(current_date, 'DD') INTO v_day FROM DUAL;
	   IF v_day > 15 THEN
		   IF p_charge_status = 25 THEN  --(Credit)
		   	 SELECT last_day(add_months(trunc(CURRENT_DATE),2)) into v_deduction_date FROM dual;
		   ELSE
		  	 SELECT last_day(add_months(trunc(CURRENT_DATE),1))+15 into v_deduction_date FROM dual;
		   END IF;
       ELSE
       	   SELECT last_day(add_months(trunc(CURRENT_DATE),1))+15 into v_deduction_date FROM dual;
       END IF;

	   RETURN v_deduction_date;


END get_directrep_deduction_date;

/*******************************************************
 * Description : Procedure to fetch Division information
*******************************************************/

PROCEDURE gm_fch_division (
     p_company_ID  		IN   t1911_division_company_mapping.c1900_company_id%TYPE,
     p_division_cur     OUT  TYPES.cursor_type
	)
	AS
     BEGIN
         OPEN p_division_cur FOR
      
      	     SELECT  DISTINCT  DIVID DIVID 
			     , DIVNAME NM, COMPID COMPID ,COMPNAME COMPNAME
			     FROM  V700_TERRITORY_MAPPING_DETAIL V700 
			     WHERE V700.DISTTYPEID NOT IN (70103,70106,70104)
			     AND DIVID = decode(p_company_ID,'1000',100823,100824)
			     AND COMPID not in (4000643); -- Bone Bank Allograft
      	
    END gm_fch_division;

/**********************************************************************
   * Description : Procedure to get zone details based on company id
   *********************************************************************/
 PROCEDURE gm_fch_zone_cmp (
      p_compid   	 IN       t504a_loaner_transaction.c1900_company_id%TYPE,
      p_zonedata     OUT      TYPES.cursor_type
   )
   AS
   BEGIN
	   OPEN p_zonedata
	   FOR

	   SELECT  DISTINCT  GP_ID CODEID  , 
	                     GP_NAME || ' (' || VP_NAME  || ')' CODENM  , 
	                     DIVID COMPDIVID, 
	                     COMPID  
	     FROM  V700_TERRITORY_MAPPING_DETAIL V700  
        WHERE  d_compid = p_compid
          AND divid = decode(p_compid,'1000',100823,100824) ORDER BY CODENM ;  --100823:US   100824:OUS


 END gm_fch_zone_cmp;
 
 /**********************************************************************
   * Description : Procedure to get fetch sum of credit amount
   *********************************************************************/
 PROCEDURE  gm_fch_credit_amount_detail(
 p_chargedtlid  IN t9201_charge_details.c9201_master_chrg_det_id%TYPE,
 p_cursor OUT TYPES.cursor_type
 )
 AS
     BEGIN
         OPEN p_cursor FOR
 SELECT
    t9200.c9200_incident_value,
    t9201.c9201_charge_start_date,
    t9201.c9201_charge_end_date,
    t9201.c9201_amount amount,
    credit.amount credit,
    t9201.c9201_amount - nvl(abs(credit.amount), 0) availamount
FROM
    t9200_incident         t9200,
    t9201_charge_details   t9201,
    (
        SELECT
            c9201_master_chrg_det_id chrgdtlid,
            SUM(c9201_amount) amount
        FROM
            t9201_charge_details
        WHERE
            c9201_master_chrg_det_id = p_chargedtlid 
            AND c9201_status = 25 -- Credit
            AND c9201_void_fl IS NULL
        GROUP BY
            c9201_master_chrg_det_id
    ) credit
WHERE
    t9201.c9200_incident_id = t9200.c9200_incident_id
    AND t9201.c9201_charge_details_id = credit.chrgdtlid (+)
    AND t9201.c9201_charge_details_id = p_chargedtlid 
    AND t9201.c9201_void_fl IS NULL
    AND t9200.c9200_void_fl IS NULL;

END gm_fch_credit_amount_detail;
 
END gm_pkg_op_loaner_charges_rpt;
/
