--@C:\pmt\Database\Packages\Operations\dashboard\gm_pkg_op_FG_dashboard.bdy;
CREATE OR REPLACE
PACKAGE BODY gm_pkg_op_FG_dashboard
IS
  /***********************************************************
  * Auther: Mohana
  * Description : Finished Goods Dashboard FOR GMNA/OUS
  * Count For Consignment Item-Pending Control,WIP,Inhouse Transaction,
  * Consignment Set-Pending Control,Loaner Extn Pending Control,
  * Replenishment Pending Control,Loaners Pending Control,
  * Orders-Same Day Pending Control  
  ***********************************************************/
PROCEDURE gm_fch_fg_overall_dash(
    p_item_cnt OUT NUMBER,
    p_wip_ord_cnt OUT NUMBER,
    p_sets_cnt OUT NUMBER,
    p_lnr_ext_cnt OUT NUMBER,
    p_inht_cnt OUT NUMBER,
    p_repl_cnt OUT NUMBER,
    p_lnr_cnt OUT NUMBER,
    p_ord_cnt OUT NUMBER )
AS
  v_company_id t1900_company.c1900_company_id%TYPE;
  v_plant_id t5040_plant_master.c5040_plant_id%TYPE;
BEGIN
  BEGIN
    SELECT get_compid_frm_cntx (),
      get_plantid_frm_cntx
    INTO v_company_id,
      v_plant_id
    FROM dual;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    v_company_id:= NULL;
    v_plant_id  := NULL;
  END;
  p_item_cnt    :=get_consign_item_cnt(v_company_id,v_plant_id);
  p_wip_ord_cnt :=get_wip_order_cnt(v_company_id,v_plant_id);
  p_sets_cnt    :=get_consign_sets_cnt(v_company_id,v_plant_id);
  p_lnr_ext_cnt :=get_loaner_ext_cnt(v_company_id,v_plant_id);
  p_inht_cnt    :=get_inht_cnt(v_company_id,v_plant_id);
  p_repl_cnt    :=get_replish_cnt(v_company_id,v_plant_id);
  p_lnr_cnt     :=get_loaner_cnt(v_company_id,v_plant_id);
  p_ord_cnt     :=get_order_cnt(v_company_id,v_plant_id);
END gm_fch_fg_overall_dash;
/*
* This function is used to get the Count For Consignment Item-Pending Control
*/
FUNCTION get_consign_item_cnt(
    p_comp_id  IN t1900_company.c1900_company_id%TYPE,
    p_plant_id IN t5040_plant_master.c5040_plant_id%TYPE)
  RETURN NUMBER
IS
  v_count NUMBER;
  v_filter T906_RULES.C906_RULE_VALUE%TYPE;
BEGIN
	BEGIN
    SELECT DECODE(GET_RULE_VALUE(p_comp_id,'CONSIGN_ITEM_DASH'), NULL, p_comp_id, p_plant_id) --ORDFILTER- To fetch the records for EDC entity countries based on plant
    INTO v_filter
    FROM DUAL;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    v_filter:= NULL;
  END;
	
    SELECT COUNT (1)
INTO v_count
FROM t504_consignment t504,
  t520_request t520,
  t5057_building t5057
WHERE t504.c504_status_fl                            >= 2
AND t504.c504_status_fl                               < 3
AND ((t504.c701_distributor_id                       IS NOT NULL
OR t504.c704_account_id                               = '01')
OR (get_consign_request_for (t504.c504_consignment_id)='40025'
AND t504.c704_account_id                             IS NOT NULL))
AND t504.c504_type                                   IN (4110,4111,4112,111800,111801,111802,111803,111804,111805)
AND t504.c504_void_fl                                IS NULL
AND t504.c207_set_id                                 IS NULL
AND ( t504.c1900_company_id                           = v_filter
OR t504.c5040_plant_id                                = v_filter)
AND t520.c520_void_fl                                IS NULL
AND t504.c520_request_id                              = t520.c520_request_id
AND t504.c5057_building_id                            = t5057.c5057_building_id(+)
AND t5057.c5057_active_fl(+)                          = 'Y'
AND t5057.c5057_void_fl                              IS NULL;
RETURN v_count;
END get_consign_item_cnt;
/*
* This function is used to get the Count For WIP Orders
*/
FUNCTION get_wip_order_cnt(
    p_comp_id  IN t1900_company.c1900_company_id%TYPE,
    p_plant_id IN t5040_plant_master.c5040_plant_id%TYPE )
  RETURN NUMBER
IS
  v_count NUMBER;
  v_filter T906_RULES.C906_RULE_VALUE%TYPE;
BEGIN
  BEGIN
    SELECT DECODE(GET_RULE_VALUE(p_comp_id,'ORDFILTERDASH'), NULL, p_comp_id, p_plant_id) --ORDFILTER- To fetch the records for EDC entity countries based on plant
    INTO v_filter
    FROM DUAL;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    v_filter:= NULL;
  END;
  SELECT COUNT (1)
  INTO v_count
  FROM t5057_building t5057,
    t501_order t501
  WHERE t501.c501_void_fl     IS NULL
  AND t501.c501_shipping_date IS NULL
  AND (t501.c1900_company_id   = v_filter
  OR t501.c5040_plant_id       = v_filter)
  AND t501.c501_status_fl      = 1
  AND (t501.c901_order_type   IS NULL
  OR t501.c901_order_type     IN
    (SELECT t906.c906_rule_value
    FROM t906_rules t906
    WHERE t906.c906_rule_grp_id = 'WIP_ORDER_GRP'
    AND c906_rule_id            = 'WIP_ORDER'
    AND c906_void_fl           IS NULL
    ))
  AND t501.c501_delete_fl     IS NULL
  AND t5057.c5057_active_fl(+) = 'Y'
  AND t5057.c5057_void_fl     IS NULL
  AND t501.c5057_building_id   = t5057.c5057_building_id(+) ;
  RETURN v_count;
END get_wip_order_cnt;
/*
* This function is used to get the Count For Consignment Set-Pending Control
*/
FUNCTION get_consign_sets_cnt(
    p_comp_id  IN t1900_company.c1900_company_id%TYPE,
    p_plant_id IN t5040_plant_master.c5040_plant_id%TYPE )
  RETURN NUMBER
IS
  v_count NUMBER;
  
BEGIN
  SELECT COUNT(cnid)
  INTO v_count
  FROM
    (SELECT t504.c207_set_id setid,
      t504.c504_consignment_id cnid
    FROM t504_consignment t504,
      t5010_tag t5010,
      t5053_location_part_mapping t5053
    WHERE t504.c504_consignment_id = t5010.c5010_last_updated_trans_id (+)
    AND t5010.c5010_tag_id         = t5053.c5010_tag_id (+)
    AND t504.c504_status_fl        = '2.20' -- Pending Pick
    AND t504.c504_void_fl         IS NULL
    AND t5010.c5010_void_fl(+)    IS NULL
    AND t504.c1900_company_id      = p_comp_id
    AND t504.c5040_plant_id        = p_plant_id
    AND t504.c5040_plant_id        = t5010.c5040_plant_id(+)
    ) t5053;
  RETURN v_count;
END get_consign_sets_cnt;
/*
* This function is used to get the Count For Consignment Set-Pending Control
*/
FUNCTION get_loaner_ext_cnt(
    p_comp_id  IN t1900_company.c1900_company_id%TYPE,
    p_plant_id IN t5040_plant_master.c5040_plant_id%TYPE )
  RETURN NUMBER
IS
  v_count NUMBER;
BEGIN
  SELECT COUNT (1)
  INTO v_count
  FROM t5057_building t5057,
    t412_inhouse_transactions t412
  WHERE t5057.c5057_building_id(+) = t412.c5057_building_id
  AND t412.c412_void_fl        IS NULL
  AND t412.c1900_company_id    = p_comp_id
  AND t412.c5040_plant_id      = p_plant_id
  AND t412.c412_status_fl      IN (2,3)
  AND t5057.c5057_active_fl(+)  = 'Y'
  AND t5057.c5057_void_fl      IS NULL
  AND t412.c412_type            = '50154';
  RETURN v_count;
END get_loaner_ext_cnt;
/*
* This function is used to get the Count For Inhouse Pending Control
*/
FUNCTION get_inht_cnt(
    p_comp_id  IN t1900_company.c1900_company_id%TYPE,
    p_plant_id IN t5040_plant_master.c5040_plant_id%TYPE)
  RETURN NUMBER
IS
  v_count NUMBER;
  v_filter T906_RULES.C906_RULE_VALUE%TYPE;
BEGIN
 BEGIN
  SELECT DECODE(GET_RULE_VALUE(p_comp_id,'IN-HOUSE_TRANS_DASH'), NULL, p_comp_id, p_plant_id) --ORDFILTER- To fetch the records for EDC entity countries based on plant
  INTO v_filter
  FROM DUAL;
EXCEPTION
WHEN NO_DATA_FOUND THEN
  v_filter:= NULL;
END;
SELECT COUNT(1)
INTO v_count
FROM
  (SELECT t504.c504_consignment_id
  FROM t504_consignment t504,
    t5057_building t5057
  WHERE c504_status_fl         > 1
  AND c504_status_fl           < DECODE(c504_type,9110,3,400085,3,400086,3,106703,3,106704,3,4)
  AND c207_set_id             IS NULL
  AND c504_void_fl            IS NULL
  AND t504.c5057_building_id   = t5057.c5057_building_id(+)
  AND t5057.c5057_active_fl(+) = 'Y'
  AND t5057.c5057_void_fl     IS NULL
  AND ( c704_account_id        = '01'
  OR c504_type                IN (400085,400086,106703,106704) )
  AND ( t504.c1900_company_id  = v_filter
  OR t504.c5040_plant_id       = v_filter)
  AND c504_type NOT           IN (40051,40052,40053,40054,40055,40056,100069,100075,100076,103932)
  UNION ALL
  SELECT t412.c412_inhouse_trans_id
  FROM t412_inhouse_transactions t412,
    t5057_building t5057
  WHERE t412.c412_status_fl    > 1
  AND t412.c412_status_fl      < DECODE(c412_type,50154,3,4)
  AND t412.c412_type NOT      IN ( 50159,1006571,1006572,40051,40052,40053,40054,40055,40056,100069,100075,100076,1006483,1006572,56045,56025,103932 )
  AND t412.c412_void_fl       IS NULL
  AND t412.c5057_building_id   = t5057.c5057_building_id(+)
  AND t5057.c5057_active_fl(+) = 'Y'
  AND t5057.c5057_void_fl     IS NULL
  AND ( t412.c1900_company_id  = v_filter
  OR t412.c5040_plant_id       = v_filter)
  );
RETURN v_count;
END get_inht_cnt;
/*
* This function is used to get the Count For Replenishment Pending Control
*/
FUNCTION get_replish_cnt(
    p_comp_id  IN t1900_company.c1900_company_id%TYPE,
    p_plant_id IN t5040_plant_master.c5040_plant_id%TYPE)
  RETURN NUMBER
IS
  v_count NUMBER;
BEGIN
	
  SELECT COUNT(t412.c412_inhouse_trans_id)
  INTO v_count
  FROM t412_inhouse_transactions t412,
    t5057_building t5057
  WHERE t412.c412_status_fl    = 2
  AND t412.c412_type          IN (93341)
  AND t412.c412_void_fl       IS NULL
  AND t412.c5057_building_id   = t5057.c5057_building_id(+)
  AND t5057.c5057_active_fl(+) = 'Y'
  AND t5057.c5057_void_fl     IS NULL
  AND t412.c1900_company_id    = p_comp_id
  AND t412.c5040_plant_id      = p_plant_id;
  RETURN v_count;
END get_replish_cnt;
/*
* This function is used to get the Count For Loaners Pending Control
*/
FUNCTION get_loaner_cnt(
    p_comp_id  IN t1900_company.c1900_company_id%TYPE,
    p_plant_id IN t5040_plant_master.c5040_plant_id%TYPE)
  RETURN NUMBER
IS
  v_count NUMBER;
BEGIN
  SELECT COUNT(1)
  INTO v_count
  FROM
    (SELECT COUNT(1)
    FROM t504a_consignment_loaner t504b,
      t504a_loaner_transaction t504a,
      t525_product_request t525 ,
      t526_product_request_detail t526,
      t5010_tag t5010,
      t5053_location_part_mapping t5053
    WHERE t504b.c504a_status_fl              = 7 --(pending Pick)
    AND t504b.c504a_void_fl                 IS NULL
    AND t504a.c504_consignment_id            = t5010.c5010_last_updated_trans_id (+)
    AND t5010.c5010_tag_id                   = t5053.c5010_tag_id (+)
    AND t504b.c504_consignment_id            = t504a.c504_consignment_id
    AND t504a.c504a_void_fl                 IS NULL
    AND t504a.c526_product_request_detail_id = t526.c526_product_request_detail_id
    AND T525.C525_PRODUCT_REQUEST_ID         = T526.C525_PRODUCT_REQUEST_ID
    AND t525.c525_product_request_id        IN
      (SELECT DISTINCT t525.c525_product_request_id
      FROM t504a_consignment_loaner t504b,
        t504a_loaner_transaction t504a,
        t525_product_request t525 ,
        t526_product_request_detail t526
      WHERE t504b.c504a_status_fl              = 7 --(pending Pick)
      AND t504b.c504a_void_fl                 IS NULL
      AND t504b.c504_consignment_id            = t504a.c504_consignment_id
      AND t504a.c504a_void_fl                 IS NULL
      AND t504a.c504a_return_dt               IS NULL
      AND t504a.c526_product_request_detail_id = t526.c526_product_request_detail_id
      AND t525.c525_product_request_id         = t526.c525_product_request_id
      AND t526.c901_request_type               = 4127
      AND t525.c525_void_fl                   IS NULL
      AND t526.c526_void_fl                   IS NULL
      AND t504a.c1900_company_id               = p_comp_id
      AND t504a.c1900_company_id               = t525.c1900_company_id
      AND t504a.c5040_plant_id                 = t504b.c5040_plant_id
      AND t504a.c5040_plant_id                 = t525.c5040_plant_id
      )
    AND t525.c525_void_fl      IS NULL
    AND t526.c526_void_fl      IS NULL
    AND t504a.c1900_company_id  = p_comp_id
    AND t5010.C5010_Void_Fl(+) IS NULL
    AND t504a.c1900_company_id  = t525.c1900_company_id
    AND t504a.c5040_plant_id    = t504b.c5040_plant_id
    AND t504a.c5040_plant_id    = t525.c5040_plant_id
    GROUP BY t526.c901_request_type,
      t525.c525_product_request_id
    );
  RETURN v_count;
END get_loaner_cnt;
/*
* This function is used to get the Count For Orders-Same Day Pending Control
*/
FUNCTION get_order_cnt(
    p_comp_id  IN t1900_company.c1900_company_id%TYPE,
    p_plant_id IN t5040_plant_master.c5040_plant_id%TYPE)
  RETURN NUMBER
IS
  v_count NUMBER;
BEGIN
  SELECT COUNT (1)
  INTO v_count
  FROM t907_shipping_info t907
  WHERE t907.c901_source      = 50180
  AND t907.c1900_company_id   = p_comp_id
  AND t907.c5040_plant_id     = p_plant_id
  AND t907.c907_status_fl    IN (15)
  AND t907.c907_void_fl      IS NULL
  AND t907.c907_active_fl    IS NULL
  AND t907.c901_delivery_mode = '5038';
  RETURN v_count;
END get_order_cnt;
END gm_pkg_op_FG_dashboard;
/

