/* Formatted on 2011/05/26 19:20 (Formatter Plus v4.8.0) */
--@"c:\database\packages\operations\gm_pkg_op_loaner.bdy";
CREATE OR REPLACE
PACKAGE BODY gm_pkg_op_loaner
IS
  --
  /******************************************************************
  * Description : This Procedure is used to accept loaner returns
  ****************************************************************/
PROCEDURE gm_cs_sav_accept_return
  (
    p_conid    IN t504_consignment.c504_consignment_id%TYPE,
    p_userid   IN t504_consignment.c504_last_updated_by%TYPE,
    p_expretdt IN t504a_loaner_transaction.c504a_return_dt%TYPE )
AS
  v_prod_req_id NUMBER;
  v_count       NUMBER;
  v_date_diff NUMBER;
  v_date_format varchar2(20);
  
BEGIN
	 --PC-4351
	SELECT get_compdtfmt_frm_cntx() 
	  INTO v_date_format 
	  from dual;
	-- check return date	
	SELECT trunc(p_expretdt) - trunc(current_date)
  	  INTO v_date_diff
      FROM dual;
      
     IF v_date_diff > 0 THEN
		raise_application_error (- 20999, ' Return Date ('||to_char(p_expretdt,v_date_format)||') cannot be greater than current date ('||to_char(current_date,v_date_format)||') ');
	 END IF;
	 
  BEGIN
    SELECT NVL (t526.c525_product_request_id, 0)
    INTO v_prod_req_id
    FROM t526_product_request_detail t526,
      t504a_loaner_transaction t504a
    WHERE t504a.c504_consignment_id          = p_conid
    AND c504a_return_dt                     IS NULL
    AND c504a_void_fl                       IS NULL
    AND t504a.c526_product_request_detail_id = t526.c526_product_request_detail_id;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    v_prod_req_id := 0;
  END;
  UPDATE t504a_loaner_transaction
  SET c504a_return_dt       = p_expretdt,
    c504a_last_updated_by   = p_userid,
    c504a_last_updated_date = CURRENT_DATE
  WHERE c504_consignment_id = p_conid
  AND c504a_return_dt      IS NULL
  AND c504a_void_fl        IS NULL;
  --
  -- Call the common procedure to update the status
  gm_pkg_op_loaner_set_txn.gm_upd_consign_loaner_status (p_conid, 25, p_userid) ;
  UPDATE t504a_consignment_loaner
  SET c504a_loaner_dt        = NULL,
    c504a_expected_return_dt = NULL,
    c504a_allocated_fl	     = NULL,
    c504a_last_updated_by    = p_userid,
    c504a_last_updated_date  = CURRENT_DATE
    -- status changes to pending processing after accepting loaner return
  WHERE c504_consignment_id = p_conid
  AND c504a_void_fl        IS NULL;
  
  
  UPDATE t504_consignment
  SET 
     -- c701_distributor_id  = NULL,   -- PMT-24984: Missing loaned details on Loaner paperwork that is printed in the 'Process Check' step
        c704_account_id        = NULL,
        c504_last_updated_by   = p_userid,
        c504_last_updated_date = CURRENT_DATE
   -- , c504_ship_to = NULL
   -- , c504_ship_to_id = NULL
WHERE c504_consignment_id = p_conid
  AND c504_void_fl         IS NULL;
  --
  UPDATE t5010_tag
  SET c704_account_id               = NULL,
    c901_location_type              = 40033,
    c5010_location_id               = 52099,
    c901_sub_location_type          = NULL,
    c5010_sub_location_id           = NULL,
    c5010_last_updated_by           = p_userid,
    c5010_last_updated_date         = CURRENT_DATE,
    c5010_last_updated_trans_id     = p_conid,
    c901_status                     = decode(c901_status,26240614,51012,26240842,51012,c901_status),  --active when missing and Disputed to accept return
    c5010_missing_since				= decode(c901_status,26240614,NULL,26240842,NULL,c5010_missing_since)
  WHERE c5010_last_updated_trans_id = p_conid
  AND c5010_void_fl                IS NULL;
  
  gm_save_status_details (p_conid, '', p_userid, NULL, CURRENT_DATE, 91122, 91103 );
  -- for non-usage charges
  IF (v_prod_req_id != 0) THEN
    SELECT COUNT (c504a_loaner_transaction_id)
    INTO v_count
    FROM t504a_loaner_transaction t504al
    WHERE t504al.c526_product_request_detail_id IN
      (SELECT c526_product_request_detail_id
      FROM t526_product_request_detail
      WHERE c525_product_request_id = v_prod_req_id
      AND c526_void_fl             IS NULL
      )
    AND c504a_return_dt IS NULL;
    IF v_count           = 0 THEN
      UPDATE t525_product_request
      SET c525_usage_date           = TRUNC (CURRENT_DATE),
        c525_last_updated_by        = p_userid,
        c525_last_updated_date      = CURRENT_DATE
      WHERE c525_product_request_id = v_prod_req_id
      AND c525_void_fl             IS NULL;
    END IF;
  END IF;
END gm_cs_sav_accept_return;
/******************************************************************
* Description : This function returns the date when the loaner was returned (returns the max date)
****************************************************************/
FUNCTION get_return_dt
  (
    p_conid IN t504a_loaner_transaction.c504_consignment_id%TYPE )
  RETURN VARCHAR2
IS
  v_date VARCHAR2 (20);
  v_dateformat VARCHAR2(100);
BEGIN
  BEGIN
	select get_compdtfmt_frm_cntx() into v_dateformat from dual;
    SELECT TO_CHAR (c504a_return_dt, NVL(v_dateformat,'mm/dd/yyyy'))
    INTO v_date
    FROM t504a_loaner_transaction
    WHERE c504a_loaner_transaction_id = gm_pkg_op_loaner.get_loaner_trans_id (p_conid);
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    RETURN '';
  END;
  RETURN v_date;
END get_return_dt;
/******************************************************************
* Description : This function returns the Loaner TransID when consignment id is passed (returns max loan trans id)
****************************************************************/
FUNCTION get_loaner_trans_id
  (
    p_conid IN t504a_loaner_transaction.c504_consignment_id%TYPE )
  RETURN VARCHAR2
IS
  v_id VARCHAR2 (20);
BEGIN
  BEGIN
    SELECT MAX (TO_NUMBER (c504a_loaner_transaction_id))
    INTO v_id
    FROM t504a_loaner_transaction
    WHERE c504_consignment_id = p_conid
    AND c504a_void_fl        IS NULL;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    RETURN '';
  END;
  RETURN v_id;
END get_loaner_trans_id;
/******************************************************************
* Description : This function returns the available(status = 0) sets given a setid
****************************************************************/
FUNCTION get_available_sets
  (
    p_setid  IN t207_set_master.c207_set_id%TYPE,
    p_status IN t504a_consignment_loaner.c504a_status_fl%TYPE,
    p_type   IN t504_consignment.c504_type%TYPE )
  RETURN NUMBER
AS
  v_count VARCHAR2 (10);
  v_plant_id t5040_plant_master.c5040_plant_id%TYPE;
BEGIN
	SELECT  get_plantid_frm_cntx() INTO  v_plant_id FROM DUAL;
  BEGIN
    SELECT COUNT (*)
    INTO v_count
    FROM t504a_consignment_loaner t504a,
      t504_consignment t504
    WHERE t504a.c504_consignment_id = t504.c504_consignment_id
    AND t504.c207_set_id            = p_setid
    AND t504a.c504a_status_fl       = p_status
    AND t504a.c504a_void_fl        IS NULL
    AND t504.c504_void_fl          IS NULL
    AND t504.c504_type              = p_type
    AND t504.C5040_PLANT_ID = v_plant_id;
    
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    RETURN '0';
  END;
  RETURN v_count;
END get_available_sets;
/*******************************************************
* Purpose: function is used to request status
*******************************************************/
--
FUNCTION get_loaner_status
  (
    p_statusflag IN t504a_consignment_loaner.c504a_status_fl%TYPE )
  RETURN VARCHAR2
IS
  v_status_name VARCHAR2 (100);
BEGIN
  SELECT DECODE (p_statusflag, 0, 'Available', 5, 'Allocated', 7, 'Pending Pick', 10, 'Pending Shipping', 13, 'Packing In Progress', 16, 'Ready For Pickup', 20, 'Pending Return', 21, 'Disputed', 22, 'Missing', 23, 'Deployed', 25, 'Pending Check', 30, 'WIP - Loaners', 40, 'Pending Verification', 50, 'Pending Process', 55, 'Pending Picture', 58, 'Pending Putaway', 60, 'Inactive', 24, 'Pending Acceptance')
  INTO v_status_name
  FROM DUAL;
  RETURN v_status_name;
END get_loaner_status;
/******************************************************************
* Description : Procedure to save the incident charges for loaners
* Author    : Satyajit
****************************************************************/
PROCEDURE gm_sav_additional_charges
  (
    p_transid   IN VARCHAR2,
    p_transtype IN NUMBER,
    p_inputstr  IN VARCHAR2,
    p_userid    IN NUMBER )
AS
  CURSOR charge_cur
  IS
    SELECT token, tokenii FROM v_double_in_list WHERE token IS NOT NULL;
  
  v_dist_id VARCHAR2 (20);
  v_rep_id  NUMBER;
  v_expected_date DATE;
  v_actual_date DATE;
  v_status_fl_cnt NUMBER;
  v_incident_id t9200_incident.c9200_incident_id%TYPE;
  v_request_id t525_product_request.c525_product_request_id%TYPE; 
  v_Prod_req_dtl_id T526_Product_Request_Detail.C526_Product_Request_Detail_Id%TYPE; 
  v_consign_id t504a_loaner_transaction.c504_consignment_id%TYPE;
  v_assoc_rep_id	 t504a_loaner_transaction.c703_ass_rep_id%TYPE;
BEGIN
  my_context.set_double_inlist_ctx (p_inputstr, ';');
  --raise_application_error('-20999','p_transtype'||p_transtype);
  IF p_transtype = 4127 THEN
    SELECT c504a_consigned_to_id,
      c703_sales_rep_id,
      c504a_return_dt,
      c504a_expected_return_dt,
      c504_consignment_id,
      c703_ass_rep_id
    INTO v_dist_id,
      v_rep_id,
      v_actual_date,
      v_expected_date,
      v_consign_id,
      v_assoc_rep_id
    FROM t504a_loaner_transaction t504a
    WHERE t504a.c504a_loaner_transaction_id = p_transid FOR UPDATE;
  END IF;
  SELECT COUNT (*)
  INTO v_status_fl_cnt
  FROM t9200_incident
  WHERE c9200_ref_id          = p_transid
  AND c9200_status_fl        IS NULL
  AND c9200_void_fl          IS NULL
  AND c901_incident_type NOT IN ('92072','92074','92068'); --90274 - Enter Excess Parts
  IF v_status_fl_cnt          > 0 THEN
    GM_RAISE_APPLICATION_ERROR('-20999','237','');
  END IF;
  FOR current_row IN charge_cur
  LOOP
    gm_sav_incidents (p_transtype, p_transid, current_row.token, current_row.tokenii, p_userid, TRUNC (CURRENT_DATE), v_incident_id );
    -- Has to have an entry in Charges table
    gm_pkg_op_loaner.gm_sav_charge_details (v_incident_id, v_rep_id, v_dist_id, get_rule_value (current_row.token, 'CHARGES' ), 10, p_userid, v_assoc_rep_id );
  END LOOP;
  IF v_actual_date > v_expected_date THEN
    v_request_id  := gm_pkg_op_loaner.get_loaner_request_id(p_transid);
    /* Get the Prodcuct request detail id and pass that also to Loaner late charge package*/
    v_Prod_req_dtl_id  := gm_pkg_op_loaner.get_product_loaner_request_id(p_transid);
    gm_pkg_op_charges.gm_sav_loaner_late_charge(CURRENT_DATE,v_request_id,v_Prod_req_dtl_id);    
  END IF;
END gm_sav_additional_charges;
/******************************************************************
* Description : Procedure to save the incidents for loaner returns
* Author    : Satyajit
****************************************************************/
PROCEDURE gm_sav_incidents
  (
    p_reftype        IN t9200_incident.c9200_ref_type%TYPE,
    p_refid          IN t9200_incident.c9200_ref_id%TYPE,
    p_incident_type  IN t9200_incident.c901_incident_type%TYPE,
    p_incident_value IN t9200_incident.c9200_incident_value%TYPE,
    p_userid         IN t9200_incident.c9200_created_by%TYPE,
    p_incident_date  IN t9200_incident.c9200_incident_date%TYPE,
    p_incident_id    IN OUT t9200_incident.c9200_incident_id%TYPE,
    p_run_dt         IN DATE DEFAULT CURRENT_DATE )
AS
  v_rul_dt DATE;
  v_company_id  t1900_company.c1900_company_id%TYPE;
  v_trans_company_id  t1900_company.c1900_company_id%TYPE;
BEGIN
			
	SELECT get_compid_frm_cntx()
      INTO v_company_id
      FROM DUAL;
      
    -- The following code is added for saving the Company id based on the PLANT instead off COntext company id during saving the info in t9200_incident table.
    SELECT DECODE(get_rule_value(v_company_id,'LOANER_CHARGES_RPT') , 'Plant' , c1900_company_id, v_company_id) 
      INTO  v_trans_company_id
      FROM t504a_loaner_transaction 
     WHERE c504a_loaner_transaction_id = p_refid 
       AND c504a_void_fl IS NULL; 
      
  IF p_reftype = 4127 AND p_incident_type = 92068 THEN
    -- rule value for testing purpose
    --v_rul_dt := TO_DATE(get_rule_value ('LATEFEECAL', 'LATEFEECAL'),'MM/DD/YYYY');
    BEGIN
      SELECT MAX(c9200_incident_id)
      INTO p_incident_id
      FROM t9200_incident
      WHERE c9200_ref_id                    = p_refid
      AND c9200_ref_type                    = 4127  -- Loaner
      AND c901_incident_type                = 92068 -- LateFee
      AND c9200_void_fl                    IS NULL
      AND TO_CHAR(c9200_incident_date,'MM') = TO_CHAR(NVL(p_run_dt,CURRENT_DATE),'MM');
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
      p_incident_id := NULL;
    END;
    IF p_incident_id IS NOT NULL THEN
      UPDATE t9200_incident
      SET c9200_updated_by =p_userid ,
        c9200_updated_date = CURRENT_DATE
        -- , c9200_incident_value = p_incident_value removing CN# update
      WHERE c9200_incident_id               = p_incident_id
      AND c9200_ref_id                      = p_refid
      AND c9200_ref_type                    = 4127  -- Loaner
      AND c901_incident_type                = 92068 -- LateFee
      AND c9200_void_fl                    IS NULL
      AND TO_CHAR(c9200_incident_date,'MM') = TO_CHAR(NVL(p_run_dt,CURRENT_DATE),'MM');
    ELSE
      SELECT 'GM-IC-' || s9200_incident.NEXTVAL INTO p_incident_id FROM DUAL;
      INSERT
      INTO t9200_incident
        (
          c9200_incident_id,
          c9200_ref_type,
          c9200_ref_id,
          c901_incident_type,
          c9200_incident_value,
          c9200_created_by,
          c9200_created_date,
          c9200_incident_date,
          c1900_company_id
        )
        VALUES
        (
          p_incident_id,
          p_reftype,
          p_refid,
          p_incident_type,
          p_incident_value,
          p_userid,
          CURRENT_DATE,
          NVL(v_rul_dt,p_incident_date),
          v_trans_company_id
        );
    END IF;
  ELSE
    SELECT 'GM-IC-' || s9200_incident.NEXTVAL INTO p_incident_id FROM DUAL;
    INSERT
    INTO t9200_incident
      (
        c9200_incident_id,
        c9200_ref_type,
        c9200_ref_id,
        c901_incident_type,
        c9200_incident_value,
        c9200_created_by,
        c9200_created_date,
        c9200_incident_date,
        c1900_company_id
      )
      VALUES
      (
        p_incident_id,
        p_reftype,
        p_refid,
        p_incident_type,
        p_incident_value,
        p_userid,
        CURRENT_DATE,
        p_incident_date,
        v_trans_company_id
      );
  END IF;
END gm_sav_incidents;
/******************************************************************
* Description : Procedure to save the Charge details for loaner returns
* Author    : Brinalg
****************************************************************/
PROCEDURE gm_sav_charge_details
  (
    p_incident_id IN t9200_incident.c9200_incident_id%TYPE,
    p_repid       IN t9201_charge_details.c703_sales_rep_id%TYPE,
    p_distid      IN t9201_charge_details.c701_distributor_id%TYPE,
    p_amount      IN VARCHAR2,
    p_status      IN NUMBER,
    p_userid      IN t9200_incident.c9200_created_by%TYPE,
    p_assoc_rep_id	 IN	  t9201_charge_details.c703_ass_rep_id%TYPE DEFAULT NULL
  )
AS
  v_ref_id t9200_incident.c9200_ref_id%TYPE;
  v_Prod_req_dtl_id T526_Product_Request_Detail.C526_Product_Request_Detail_Id%TYPE; 
BEGIN
  BEGIN
    SELECT c9200_ref_id
    INTO v_ref_id
    FROM t9200_incident
    WHERE c9200_incident_id = p_incident_id
    AND c9200_ref_type      = 4127  -- Loaner
    AND c901_incident_type  = 92068 -- LateFee
    AND c9200_void_fl      IS NULL;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    v_ref_id := NULL;
  END;
  IF v_ref_id IS NOT NULL THEN
   /* Get the Product request detail id and pass that also to Loaner late charge package*/
    v_Prod_req_dtl_id  := gm_pkg_op_loaner.get_product_loaner_request_id(v_ref_id);
    gm_pkg_op_charges.gm_sav_late_charges (v_ref_id, p_status,p_userid,NULL,NULL,v_Prod_req_dtl_id) ;
  ELSE
    INSERT
    INTO t9201_charge_details
      (
        c9201_charge_details_id,
        c9200_incident_id,
        c703_sales_rep_id,
        c701_distributor_id,
        c9201_amount,
        c9201_status,
        c9201_created_by,
        c9201_created_date,
        c703_ass_rep_id
      )
      VALUES
      (
        s9201_charge_details.NEXTVAL,
        p_incident_id,
        p_repid,
        p_distid,
        TO_NUMBER (p_amount),
        p_status,
        p_userid,
        CURRENT_DATE,
        p_assoc_rep_id
      ) ;
  END IF;
END gm_sav_charge_details;
/******************************************************************
* Description : Procedure to update the Charge status from the report
* Author    : Brinalg
****************************************************************/
PROCEDURE gm_sav_charge_status
  (
    p_inputstr IN VARCHAR2,
    p_userid   IN t9201_charge_details.c9201_updated_by%TYPE
  )
AS
  v_discount NUMBER
  (
    10, 2
  )
  ;
  v_charge1 NUMBER
  (
    10, 2
  )
  ;
  v_charge2 NUMBER
  (
    10, 2
  )
                  := 0;
  v_strlen NUMBER := NVL
  (
    LENGTH (p_inputstr), 0
  )
  ;
  v_string VARCHAR2
  (
    4000
  )
  := p_inputstr;
  v_substring VARCHAR2
  (
    1000
  )
  ;
  v_dtl_id VARCHAR2
  (
    10
  )
  ;
  v_status VARCHAR2
  (
    10
  )
  ;
  v_tot_req_cnt T9202_NONUSG_CHG_DETAILS.C9202_TOTAL_REQUEST_COUNT%TYPE;
  v_tot_nonusg_cnt T9202_NONUSG_CHG_DETAILS.C9202_TOTAL_NONUSG_COUNT%TYPE;
  v_chrg_amt T9202_NONUSG_CHG_DETAILS.C9202_CHARGE_AMOUNT%TYPE;
  v_volm_clfn T9202_NONUSG_CHG_DETAILS.C901_VOLUME_CLASSIFICATION%TYPE;
  v_inst_waived T9202_NONUSG_CHG_DETAILS.C9202_INSTANCE_WAIVED%TYPE;
  v_incident_value VARCHAR2
  (
    2000
  )
  ;
  v_hvu      NUMBER;
  v_hvu_disc NUMBER;
  v_lvu_disc NUMBER;
  v_chrg_dtlid T9202_NONUSG_CHG_DETAILS.c9201_charge_details_id%TYPE;
  v_amount      NUMBER;
  v_crd_amount  NUMBER;
  v_credit_date VARCHAR2
  (
    20
  )
  ;
  v_incident_id t9200_incident.c9200_incident_id%TYPE;
  v_date_fmt   VARCHAR2(50);
  v_company_id  t1900_company.c1900_company_id%TYPE;
BEGIN
	
	select get_compdtfmt_frm_cntx(),get_compid_frm_cntx()
	  into v_date_fmt,v_company_id 
	  from dual;
	
  IF v_strlen > 0 THEN
    WHILE INSTR
    (
      v_string, '|'
    )
    <> 0
    LOOP
      --
      v_substring := SUBSTR
      (
        v_string, 1, INSTR (v_string, '|') - 1
      )
      ;
      v_string := SUBSTR
      (
        v_string, INSTR (v_string, '|') + 1
      )
      ;
      v_dtl_id := TRIM
      (
        SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1)
      )
      ;
      v_substring := SUBSTR
      (
        v_substring, INSTR (v_substring, ',') + 1
      )
      ;
      v_status := TRIM
      (
        SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1)
      )
      ;
      v_substring := SUBSTR
      (
        v_substring, INSTR (v_substring, ',') + 1
      )
      ;
      v_crd_amount := TRIM
      (
        SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1)
      )
      ;
      v_substring := SUBSTR
      (
        v_substring, INSTR (v_substring, ',') + 1
      )
      ;
      v_credit_date := v_substring ;  
      IF TRIM
        (
          v_crd_amount
        )
        IS NULL THEN
        UPDATE t9201_charge_details t9201
        SET t9201.c9201_status              = v_status,
          c9201_updated_by                  = p_userid,
          c9201_updated_date                = CURRENT_DATE
        WHERE t9201.c9201_charge_details_id = v_dtl_id;
        IF v_status                         = '50' THEN
          BEGIN
            SELECT c9202_total_request_count,
              c9202_total_nonusg_count,
              c9202_charge_amount,
              c901_volume_classification,
              c9202_instance_waived,
              c9201_charge_details_id
            INTO v_tot_req_cnt,
              v_tot_nonusg_cnt,
              v_chrg_amt,
              v_volm_clfn,
              v_inst_waived ,
              v_chrg_dtlid
            FROM t9202_nonusg_chg_details
            WHERE c9201_charge_details_id = v_dtl_id;
          EXCEPTION
          WHEN NO_DATA_FOUND THEN
            v_chrg_dtlid:=NULL;
          END;
          BEGIN
            SELECT c9201_amount
            INTO v_amount
            FROM t9201_charge_details
            WHERE c9201_charge_details_id = v_dtl_id
            AND c9201_void_fl            IS NULL;
          EXCEPTION
          WHEN NO_DATA_FOUND THEN
            v_amount := 0;
          END;
          IF v_amount <= 0 THEN
            GM_RAISE_APPLICATION_ERROR('-20999','238','');
          END IF;
          IF v_chrg_dtlid IS NOT NULL THEN
            v_hvu         := get_rule_value ('HVU', 'CHARGES');     -- high volume user 6
            v_hvu_disc    := get_rule_value ('HVUDISC', 'CHARGES'); -- high volume user discount 35%
            v_lvu_disc    := get_rule_value ('LVUDISC', 'CHARGES'); -- low volume user discount 2
            IF v_volm_clfn = 1006426 THEN                           -- High volume user
              /*
              eg -
              if reqcnt = 10 (total requests made for the quarter)
              non_usage_reqcnt = 4, (total non-usage requests made for the quarter)
              discount  = 4-1-((35/100)*10) = 4-3.5 =.5 ; 1 waived instances
              charge = .5 * 300 = 150
              */
              v_inst_waived    :=v_inst_waived     +1;
              v_discount       := (v_hvu_disc      / 100) * v_tot_req_cnt;
              v_charge1        := v_tot_nonusg_cnt - (v_inst_waived) - v_discount;
              v_charge2        := v_charge1        * v_chrg_amt;
              v_incident_value := 'Total Requests: ' || v_tot_req_cnt || '<br>Total Non-Usages: ' || v_tot_nonusg_cnt || '<br>Waived Instances:' || v_inst_waived ||'<br>((' || v_tot_nonusg_cnt || ' - ' || v_inst_waived || ') - ' || v_discount || ') * $' || v_chrg_amt || ' = $';
            ELSE -- currindex.reqcnt > v_lvu_disc
              -- Low Volume User
              /*
              eg -
              if reqcnt = 5 (total requests made for the quarter)
              non_usage_reqcnt = 3, (total non-usage requests made for the quarter)
              charge = (3-2)* 300) = 100
              */
              v_inst_waived    :=v_inst_waived     +1;
              v_discount       := v_tot_nonusg_cnt - (v_inst_waived) - v_lvu_disc;
              v_charge2        := v_discount       * v_chrg_amt;
              v_incident_value := 'Total Requests: ' || v_tot_req_cnt || '<br>Total Non-Usages: ' || v_tot_nonusg_cnt ||'<br>Waived Instances: ' || v_inst_waived || '<br>((' || v_tot_nonusg_cnt || ' - ' ||v_inst_waived || ') - ' || v_lvu_disc || '(waveoff))* $' || v_chrg_amt || ' = $';
            END IF;
            IF v_charge2 <= 0 THEN
              v_charge2  := 0;
            END IF;
            v_incident_value := v_incident_value || v_charge2;
            UPDATE t9202_nonusg_chg_details
            SET c9202_instance_waived     = v_inst_waived
            WHERE c9201_charge_details_id = v_dtl_id;
            UPDATE t9201_charge_details
            SET c9201_amount              = v_charge2
            WHERE c9201_charge_details_id = v_dtl_id
            AND c9201_void_fl            IS NULL;
            UPDATE t9200_incident
            SET c9200_incident_value = v_incident_value
            WHERE c9200_incident_id  =
              (SELECT c9200_incident_id
              FROM t9201_charge_details
              WHERE c9201_charge_details_id = v_dtl_id
              AND c9201_void_fl            IS NULL
              )
            AND c9200_void_fl IS NULL;
          END IF;
        END IF;
      ELSE
        -- Validate Credit vs Charged
        gm_pkg_op_msng_chrg.gm_validate_charge (TO_NUMBER(v_dtl_id),v_crd_amount);
        SELECT 'GM-IC-' || s9200_incident.NEXTVAL INTO v_incident_id FROM DUAL;
        INSERT
        INTO t9200_incident
          (
            c9200_incident_id,
            c9200_ref_type,
            c9200_ref_id ,
            c9200_incident_value,
            c9200_status_fl,
            c9200_created_by ,
            c9200_created_date,
            c901_incident_type,
            c9200_incident_date,
            c1900_company_id
            
          )
        SELECT v_incident_id,
          c9200_ref_type,
          c9200_ref_id ,
          c9200_incident_value,
          c9200_status_fl,
          c9200_created_by ,
          c9200_created_date,
          c901_incident_type,
          c9200_incident_date,
          c1900_company_id
        FROM t9200_incident t9200,
          t9201_charge_details t9201
        WHERE t9200.c9200_incident_id     = t9201.c9200_incident_id
        AND t9201.c9201_charge_details_id = v_dtl_id;     
        INSERT
        INTO t9201_charge_details
          (
            c9201_charge_details_id ,
            c9201_amount ,
            c9201_status ,
            c9201_created_by ,
            c9201_created_date,
            c9200_incident_id ,
            c701_distributor_id ,
            c703_sales_rep_id ,
            c205_part_number ,
            c9201_qty_missing ,
            c9201_unit_cost ,
            c9201_credited_date ,
            c901_recon_comments,
            c9201_master_chrg_det_id,
            c703_ass_rep_id
          )
        SELECT s9201_charge_details.NEXTVAL ,
          v_crd_amount ,
          25 ,
          c9201_created_by ,
          c9201_created_date,
          v_incident_id ,
          c701_distributor_id ,
          c703_sales_rep_id ,
          c205_part_number ,
          c9201_qty_missing ,
          c9201_unit_cost ,
          TO_DATE(v_credit_date,v_date_fmt) ,
          NULL,
          v_dtl_id,
          c703_ass_rep_id
        FROM t9201_charge_details
        WHERE c9201_charge_details_id = v_dtl_id;
      END IF;
    END LOOP;
  END IF;
END gm_sav_charge_status;
/***************************************************************
* Description : Procedure to do the allocation for loaners
* Author    : Brinal G
****************************************************************/
PROCEDURE gm_sav_allocate
  (
    p_setid  IN t207_set_master.c207_set_id%TYPE,
    p_type   IN t504_consignment.c504_type%TYPE,
    p_userid IN t504_consignment.c504_created_by%TYPE )
AS
  v_setid t207_set_master.c207_set_id%TYPE;
  v_loaner_req_day T906_RULES.C906_RULE_VALUE%TYPE;
BEGIN
  SELECT c207_set_id
  INTO v_setid
  FROM t207_set_master
  WHERE c207_set_id = p_setid FOR UPDATE;
  --the above query is required to lock the set master table
  v_loaner_req_day := TO_NUMBER(get_rule_value (p_type, 'LOANERREQDAY'));
  gm_pkg_op_loaner_allocation.gm_sav_allocate(p_setid, p_type, v_loaner_req_day, p_userid);
END gm_sav_allocate;
/******************************************************************
* Description : Procedure to do the status update in shipping and conignment_loaner tbl
* Author    : Brinal G
****************************************************************/
PROCEDURE gm_sav_status_update
  (
    p_req_det_id IN t526_product_request_detail.c526_product_request_detail_id%TYPE,
    p_userid     IN t504_consignment.c504_created_by%TYPE )
AS
  v_required_date DATE;
  v_consign_id t504_consignment.c504_consignment_id%TYPE;
  v_exp_return_dt DATE;
  v_type t526_product_request_detail.c901_request_type%TYPE;
  v_country VARCHAR2(10);
  v_status_fl	t504a_consignment_loaner.c504a_status_fl%TYPE;
BEGIN
  BEGIN
    SELECT t526.c526_required_date,
      t504a.c504_consignment_id
    INTO v_required_date,
      v_consign_id
    FROM t526_product_request_detail t526,
      t504a_loaner_transaction t504a
    WHERE t526.c526_product_request_detail_id = p_req_det_id
      -- AND t526.c901_request_type = 4127 ( removed this condition to accomodate in house loaners, do not uncomment )
    AND t504a.c526_product_request_detail_id = t526.c526_product_request_detail_id
    AND t526.c526_status_fl NOT             IN (30, 40) --closed/cancelled
    AND t504a.c504a_void_fl                 IS NULL
    AND t526.c526_void_fl                   IS NULL
    AND t504a.c504a_return_dt               IS NULL;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    RETURN;
  END;
  /* comment following code since loaner set will be changed to pending pick instead of pending shipping
  UPDATE t907_shipping_info
  SET c907_ref_id = v_consign_id,
  c907_status_fl = 30, -- pending shipping
  c907_last_updated_date = CURRENT_DATE,
  c907_last_updated_by = p_userid
  WHERE c907_ref_id = TO_CHAR (p_req_det_id)
  AND c901_source = 50182
  AND c907_void_fl IS NULL;
  */
  -- Calculate / fetch  the expected return date of the set
  v_exp_return_dt := gm_pkg_op_loaner_allocation.get_expected_return_date(p_req_det_id);
  
  -- Get the next status based on current status
  -- if current status is 5 allocated or 0 available, then need update to 7 pending pick, otherwise keep existing status.
  select DECODE(c504a_status_fl,0,7,5,7,c504a_status_fl)
  INTO v_status_fl
  from t504a_consignment_loaner
  WHERE c504_consignment_id  = v_consign_id
  AND c504a_void_fl         IS NULL;
  
  -- Call the common procedures to update the status and allocated flag respectively
  gm_pkg_op_loaner_set_txn.gm_upd_consign_loaner_status (v_consign_id, v_status_fl, p_userid) ;
  gm_pkg_op_loaner_allocation.gm_loaner_allocation_update(p_req_det_id,'ADD',p_userid);
  
  UPDATE t504a_consignment_loaner
  SET c504a_loaner_dt        = v_required_date,
    c504a_expected_return_dt = v_exp_return_dt,
    c504a_last_updated_date  = CURRENT_DATE,
    c504a_last_updated_by    = p_userid
  WHERE c504_consignment_id  = v_consign_id
  AND c504a_void_fl         IS NULL;
  
  --Changes added for PMT-12455 and it's used to trigger an update to iPad
--  gm_pkg_op_loaner_set_txn.GM_SAV_AVAIL_LN_SET_UPDATE (v_consign_id, p_userid); -- consignment_id, user_id, company_id 
  
  --check if country is US or OUS, If it�s OUS, need to skip status �Pending pick� after allocated, should be pending ship
  SELECT get_rule_value ('US', 'COUNTRY')
  INTO v_country
  FROM DUAL;
  IF NVL(v_country, '-999') != 'Y' THEN
    gm_pkg_op_loaner_set_txn.gm_sav_loaner_status (v_consign_id, 'PICK', p_userid) ;
  END IF;
END gm_sav_status_update;
/******************************************************************
* Description : Procedure to update the status of product request table
* Author    : Brinal G
****************************************************************/
PROCEDURE gm_sav_update_req_status
  (
    p_request_detail_id IN t525_product_request.c525_product_request_id%TYPE,
    p_userid            IN t525_product_request.c525_last_updated_by%TYPE )
AS
  v_cnt       NUMBER;
  v_total_cnt NUMBER;
  v_void_cnt  NUMBER;
  v_req_id t525_product_request.c525_product_request_id%TYPE;
BEGIN
  SELECT t526.c525_product_request_id
  INTO v_req_id
  FROM t526_product_request_detail t526
  WHERE t526.c526_product_request_detail_id = p_request_detail_id;
  --AND c526_void_fl IS NULL;
  SELECT NVL (COUNT (1), 0)
  INTO v_cnt
  FROM t526_product_request_detail
  WHERE c525_product_request_id = v_req_id
  AND c526_status_fl           IN (5,10, 20) -- Pending Approval, Open, Allocated
  AND c526_void_fl             IS NULL;
  IF v_cnt                      = 0 THEN
    SELECT NVL (COUNT (1), 0)
    INTO v_total_cnt
    FROM t526_product_request_detail
    WHERE c525_product_request_id = v_req_id;
    SELECT NVL (COUNT (1), 0)
    INTO v_void_cnt
    FROM t526_product_request_detail
    WHERE c525_product_request_id = v_req_id
    AND c526_void_fl              = 'Y';
    IF v_total_cnt                = v_void_cnt THEN
      UPDATE t525_product_request t525
      SET t525.c525_void_fl              = 'Y',
        c525_last_updated_by             = p_userid,
        c525_last_updated_date           = CURRENT_DATE
      WHERE t525.c525_product_request_id = v_req_id
      AND c525_void_fl                  IS NULL;
    ELSE
      UPDATE t525_product_request t525
      SET t525.c525_status_fl = '40' --completed
        ,
        c525_last_updated_by             = p_userid,
        c525_last_updated_date           = CURRENT_DATE
      WHERE t525.c525_product_request_id = v_req_id
      AND c525_void_fl                  IS NULL;
    END IF;
  END IF;
END gm_sav_update_req_status;
/******************************************************************
* Description : Procedure to release the loaner after it is voided/cancelled
* Author    : Brinal G
****************************************************************/
PROCEDURE gm_sav_release_loaner
  (
    p_req_detail_id IN t525_product_request.c525_product_request_id%TYPE,
    p_type          IN t504_consignment.c504_type%TYPE,
    p_userid        IN t525_product_request.c525_last_updated_by%TYPE )
AS
  v_consign_id t504_consignment.c504_consignment_id%TYPE;
  v_loan_trans_id t504a_loaner_transaction.c504a_loaner_transaction_id%TYPE;
  v_setid t207_set_master.c207_set_id%TYPE;
  v_status_fl t504a_consignment_loaner.c504a_status_fl%TYPE;
  v_ref_id t907_shipping_info.c907_ref_id%TYPE;
  v_hold_fl t504_consignment.c504_hold_fl%TYPE;
  v_type t526_product_request_detail.c901_request_type%TYPE;
  v_count NUMBER;
  v_country_fl VARCHAR2(10);
  v_req_det_id		t526_product_request_detail.c526_product_request_detail_id%TYPE;
  v_status	        t504a_consignment_loaner.c504a_status_fl%TYPE;
BEGIN
  BEGIN
    SELECT COUNT (1)
    INTO v_count
    FROM t526_product_request_detail
    WHERE c526_product_request_detail_id = p_req_detail_id
    AND c526_void_fl                     = 'Y'
    AND c526_status_fl                   < 30;
    IF (v_count                          > 0) THEN
      gm_pkg_common_cancel.gm_void_shipping (p_req_detail_id, p_userid, 50182);
    END IF;
    SELECT t504a.c504a_loaner_transaction_id,
      t504a.c504_consignment_id,
      t526.c207_set_id,
      t504cl.c504a_status_fl,
      t504.c504_hold_fl,
      t526.c901_request_type, 
      t526.c526_product_request_detail_id
    INTO v_loan_trans_id,
      v_consign_id,
      v_setid,
      v_status_fl,
      v_hold_fl,
      v_type, 
      v_req_det_id
    FROM t504a_loaner_transaction t504a,
      t526_product_request_detail t526,
      t504a_consignment_loaner t504cl,
      t504_consignment t504
    WHERE t526.c526_product_request_detail_id = t504a.c526_product_request_detail_id
    AND t504cl.c504_consignment_id            = t504a.c504_consignment_id
    AND t504.c504_consignment_id              = t504cl.c504_consignment_id
    AND t526.c526_product_request_detail_id   = p_req_detail_id
    AND t504cl.c504a_void_fl                 IS NULL
    AND t504a.c504a_void_fl                  IS NULL
      -- AND t526.c526_void_fl IS NULL no need to chk void flg here as this prc is called aft voiding t526 tbl
    AND t526.c526_status_fl NOT IN (30, 40)
    AND t526.c901_request_type   = p_type FOR UPDATE;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    RETURN;
  END;
  -- reset allocated fl and planned ship date in t504a
  gm_pkg_op_loaner_allocation.gm_loaner_allocation_update(p_req_detail_id, 'CLEAR', p_userid);
  UPDATE t504a_loaner_transaction
  SET c504a_void_fl                 = 'Y',
    c504a_last_updated_date         = CURRENT_DATE,
    c504a_last_updated_by           = p_userid
  WHERE c504a_loaner_transaction_id = v_loan_trans_id;
  UPDATE t504_consignment
  SET c704_account_id       = '',
    c701_distributor_id     = '',
    c504_last_updated_date  = CURRENT_DATE,
    c504_last_updated_by    = p_userid
  WHERE c504_consignment_id = v_consign_id
  AND c504_void_fl         IS NULL;
  
  SELECT NVL(GET_RULE_VALUE('US','COUNTRY'),'-999') INTO v_country_fl FROM DUAL;
  
  IF (v_status_fl           = 10 OR v_status_fl = 5 OR v_status_fl = 7 OR v_status_fl = 58) THEN
  --58 pending FG, 7 pending pick, 10, pending shipping, 0 available
  SELECT DECODE(v_country_fl,'-999',0, DECODE(v_status_fl, 10, 58, 58, 58, 0)) INTO v_status FROM DUAL;
  -- Call the common procedure to update the status and allocated flag
  gm_pkg_op_loaner_set_txn.gm_upd_consign_loaner_status (v_consign_id, v_status, p_userid) ;
  gm_pkg_op_loaner_allocation.gm_loaner_allocation_update(v_req_det_id,'CLEAR',p_userid);
  
    UPDATE t504a_consignment_loaner
    SET c504a_loaner_dt        = NULL,
      c504a_expected_return_dt = NULL,
      c504a_planned_ship_date  = NULL,
      c504a_allocated_fl       = NULL,
      c504a_last_updated_date  = CURRENT_DATE,
      c504a_last_updated_by    = p_userid
    WHERE c504_consignment_id  = v_consign_id
    AND c504a_void_fl         IS NULL;
  END IF;
  IF v_type = 4127 AND v_status_fl = 10 AND v_country_fl !='999'--Product Loaner previous status is pending shipping
    THEN
    --93604 Pending Putaway , to insert record in the t5050_INVPICK_ASSIGN_DETAIL
    gm_pkg_allocation.gm_ins_invpick_assign_detail (93604, v_consign_id, p_userid);
  END IF;
  IF v_type = 4127 AND v_status_fl = 7 AND v_country_fl !='999'--Product Loaner previous status is pending pick
    THEN
    -- Void Records In t5050_invpick_assign_detail for removing allocation in device.
    gm_pkg_allocation.gm_inv_void_bytxn (v_consign_id, 93602, p_userid);
  END IF;
  /*SELECT DECODE (v_status_fl, 10, v_consign_id, p_req_detail_id)
  INTO v_ref_id
  FROM DUAL;
  */
  gm_pkg_common_cancel.gm_void_shipping (p_req_detail_id, p_userid, 50182);
  gm_pkg_common_cancel.gm_void_shipping (v_consign_id, p_userid, 50182);
  gm_pkg_op_loaner.gm_sav_allocate (v_setid, p_type, p_userid);
  
END gm_sav_release_loaner;
PROCEDURE gm_sav_loaner_transaction
  (
    p_conid          IN t504_consignment.c504_consignment_id%TYPE,
    p_loaner_dt      IN VARCHAR2,
    p_expretdt       IN DATE,
    p_conto          IN t504a_loaner_transaction.c901_consigned_to%TYPE,
    p_contoid        IN t504a_loaner_transaction.c504a_consigned_to_id%TYPE,
    p_userid         IN t504_consignment.c504_last_updated_by%TYPE,
    p_repid          IN t504a_loaner_transaction.c703_sales_rep_id%TYPE,
    p_accid          IN t504a_loaner_transaction.c704_account_id%TYPE,
    p_prd_req_det_id IN VARCHAR2,
    p_assoc_rep_id	 IN	  t504a_loaner_transaction.c703_ass_rep_id%TYPE
   )
AS

    v_company_id  t1900_company.c1900_company_id%TYPE; 
	v_plant_id    T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
    v_req_company_id  t1900_company.c1900_company_id%TYPE;
    v_trans_plant_id  T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
BEGIN

        select get_compid_frm_cntx(),get_plantid_frm_cntx() 
		into v_company_id,v_plant_id 
		from dual;
		
		SELECT NVL(get_transaction_company_id (v_company_id,p_prd_req_det_id,'LOANERPALLOCFILTER','LOANERREQUEST'),v_company_id) INTO v_req_company_id FROM DUAL;

		BEGIN
		  SELECT c5040_plant_id
		  INTO v_trans_plant_id
		  FROM t504_consignment
		      WHERE c504_consignment_id = p_conid
		      AND c504_void_fl         IS NULL;
		  EXCEPTION
		     WHEN NO_DATA_FOUND THEN
		BEGIN
		   SELECT NVL(t520.c5040_plant_id, v_plant_id)
		   INTO v_trans_plant_id
		   FROM t504_consignment t504 ,
		        t520_request t520
		      WHERE t504.c504_consignment_id = p_conid
		      AND t504.c520_request_id       = t520.c520_request_id
		      AND t504.c504_void_fl         IS NULL
		      AND t520.c520_void_fl         IS NULL;
		   EXCEPTION
		       WHEN NO_DATA_FOUND THEN
		         v_trans_plant_id := v_plant_id;
		  END;
		END;
		
  INSERT
  INTO t504a_loaner_transaction
    (
      c504a_loaner_transaction_id,
      c504_consignment_id,
      c504a_loaner_dt,
      c504a_expected_return_dt,
      c901_consigned_to,
      c504a_consigned_to_id,
      c504a_created_by,
      c504a_created_date,
      c703_sales_rep_id,
      c704_account_id,
      c526_product_request_detail_id,
      c703_ass_rep_id, 
      c1900_company_id,
      c5040_plant_id
    )
    VALUES
    (
      s504a_loaner_trans_id.NEXTVAL,
      p_conid,
      p_loaner_dt,
      TRUNC (p_expretdt),
      p_conto,
      p_contoid,
      p_userid,
      CURRENT_DATE,
      p_repid,
      p_accid,
      p_prd_req_det_id,
      p_assoc_rep_id,
      v_req_company_id,
      v_trans_plant_id 
    );
END gm_sav_loaner_transaction;
/******************************************************************
* Description : Procedure to fetch the product request details
* Author    : Satyajit
****************************************************************/
PROCEDURE gm_fch_request_details
  (
    p_requestid IN t525_product_request.c525_product_request_id%TYPE,
    p_request OUT TYPES.cursor_type
  )
AS
v_company_id    t1900_company.c1900_company_id%TYPE;
BEGIN
	 --Getting company id from context.   
    SELECT get_compid_frm_cntx()
	  INTO v_company_id 
	  FROM dual;
	  --
  OPEN p_request FOR SELECT
  (
    'Request No. :- ' || t525.c525_product_request_id
  )
  reqid,
  t526.c207_set_id set_id,
  get_set_name
  (
    t526.c207_set_id
  )
  set_name,
  DECODE
  (
    NVL (t526.c526_status_fl, 10), 10, 'Unavailable', 20, 'Available', 30, 'Shipped', 40, 'Cancelled'
  )
  status,
  TO_CHAR
  (
    t526.c526_required_date, get_compdtfmt_frm_cntx()
  )
  psdate,
  t525.c703_sales_rep_id rep_id,
  get_rep_name
  (
    t525.c703_sales_rep_id
  )
  rep_name,
  t525.c703_ass_rep_id assocrepid,
  get_rep_name
  (
    t525.c703_ass_rep_id
  )
  assocrepnm,
  DECODE(t525.c703_ass_rep_id, NULL, NULL,NVL
  (
    get_cs_ship_email (4121, t525.c703_ass_rep_id), get_rule_value_by_company ('GMCSTOMAILID', 'SHIPDTL', v_company_id)
  ))
  assocrepemail,
  NVL
  (
    get_cs_ship_email (4121, t525.c703_sales_rep_id), get_rule_value_by_company ('GMCSTOMAILID', 'SHIPDTL', v_company_id)
  )
  email FROM t525_product_request t525,
  t526_product_request_detail t526 WHERE t525.c525_product_request_id = t526.c525_product_request_id AND TRUNC
  (
    t525.c525_requested_date
  )
  = TRUNC
  (
    CURRENT_DATE
  )
  AND t525.c525_void_fl IS NULL AND t526.c526_void_fl IS NULL AND t525.c525_product_request_id = p_requestid ORDER BY status DESC;
END gm_fch_request_details;
/******************************************************************
* Description : Procedure to fetch the incident details
* Author    : Ritesh
****************************************************************/
PROCEDURE gm_fch_incident_detail
  (
    p_repid    IN t504a_loaner_transaction.c703_sales_rep_id%TYPE,
    p_fromdt   IN VARCHAR2,
    p_todt     IN VARCHAR2,
    p_inputstr IN VARCHAR2,
    p_reportby IN NUMBER,
    p_type     IN t9200_incident.c9200_ref_type%TYPE,
    p_grp      IN VARCHAR2,
    p_outincidentdtl OUT TYPES.cursor_type,
    p_outcodelist OUT TYPES.cursor_type
  )
AS
 v_company_id      t1900_company.c1900_company_id%TYPE;
 v_token           VARCHAR2(20);
 v_incidentcnt     NUMBER;
 v_date_format     varchar2(20); 
BEGIN
  my_context.set_my_inlist_ctx
  (
    p_inputstr
  )
  ;
  
  SELECT COUNT (token)
    INTO v_incidentcnt
    FROM v_in_list;
  
  SELECT get_compid_frm_cntx() ,get_compdtfmt_frm_cntx()
    INTO v_company_id, v_date_format
    FROM dual;
  
  OPEN p_outincidentdtl FOR SELECT ID,
  NAME,
  incidents,
  SUM (VALUE) num FROM
  (SELECT DECODE (p_reportby, 50851, t526.c207_set_id, 50850, t504a.c703_sales_rep_id, t504a.c703_sales_rep_id ) ID
    --50851,50850 is for the Report By dropdown
    ,
    DECODE (p_reportby, 50851, get_set_name (t526.c207_set_id), 50850, get_rep_name (t504a.c703_sales_rep_id), get_rep_name (t504a.c703_sales_rep_id) ) NAME,
    get_code_name (t9200.c901_incident_type) incidents,
    1 VALUE
  FROM t9200_incident t9200,
    t504a_loaner_transaction t504a,
    t526_product_request_detail t526,
    t703_sales_rep t703
  WHERE t9200.c9200_ref_id                 = t504a.c504a_loaner_transaction_id
  AND t504a.c526_product_request_detail_id = t526.c526_product_request_detail_id(+)
  AND t703.c701_distributor_id             = DECODE (p_repid, '0', t703.c701_distributor_id, p_repid )
  AND t703.c703_sales_rep_id               = t504a.c703_sales_rep_id(+)
  AND t9200.c9200_incident_date BETWEEN NVL (TO_DATE (p_fromdt,v_date_format ), t9200.c9200_incident_date ) AND NVL (TO_DATE (p_todt,v_date_format ), t9200.c9200_incident_date )
  AND t9200.c9200_void_fl     IS NULL
  AND t504a.c504a_void_fl     IS NULL
  AND t526.c526_void_fl       IS NULL
  AND t703.c703_void_fl       IS NULL
  AND T526.c1900_company_id = v_company_id
  AND ( '-999'                 = DECODE (v_incidentcnt, 0, '-999', '-9999' )
  OR t9200.c901_incident_type IN
    (SELECT token FROM v_in_list
    ) )
  AND t9200.c9200_ref_type = NVL (p_type, t9200.c9200_ref_type)
  ) GROUP BY ID,
  NAME,
  incidents ORDER BY ID;
  OPEN p_outcodelist FOR SELECT t901.c901_code_nm incidents FROM t901_code_lookup t901 WHERE t901.c901_code_grp = p_grp ORDER BY t901.c901_code_seq_no;
END gm_fch_incident_detail;
/******************************************************************
* Description : This function returns the etch id for the given CN
****************************************************************/
FUNCTION get_loaner_etch_id
  (
    p_conid IN t504a_consignment_loaner.c504_consignment_id%TYPE )
  RETURN VARCHAR2
IS
  v_etchid VARCHAR2 (20);
BEGIN
  BEGIN
    SELECT c504a_etch_id
    INTO v_etchid
    FROM t504a_consignment_loaner
    WHERE c504_consignment_id = p_conid
    AND c504a_void_fl        IS NULL;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    RETURN ' ';
  END;
  RETURN v_etchid;
END get_loaner_etch_id;
/******************************************************************
* Description : This function returns the CN when the Loan Trans Id is passed
****************************************************************/
FUNCTION get_loaner_con_id
  (
    p_loantransid IN t504a_loaner_transaction.c504a_loaner_transaction_id%TYPE )
  RETURN VARCHAR2
IS
  v_conid VARCHAR2 (20);
BEGIN
  BEGIN
    SELECT c504_consignment_id
    INTO v_conid
    FROM t504a_loaner_transaction
    WHERE c504a_loaner_transaction_id = p_loantransid
    AND c504a_void_fl                IS NULL;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    RETURN ' ';
  END;
  RETURN v_conid;
END get_loaner_con_id;
/*********************************************************************************************
   * Description : This function returns the Product Request id based on the request detail id
   **********************************************************************************************/
FUNCTION get_product_req_dtl_set_id
  (
    p_transid IN t526_product_request_detail.c526_product_request_detail_id%TYPE )
  RETURN VARCHAR2
IS
  v_set_id t526_product_request_detail.C207_Set_Id%TYPE;
BEGIN
	 SELECT C207_Set_Id
    	INTO v_set_id
    FROM t526_product_request_detail
    WHERE c526_product_request_detail_id = p_transid
    AND c526_void_fl                IS NULL;
  RETURN v_set_id;
END get_product_req_dtl_set_id;

/******************************************************************
* Description : This function returns y/n if whether the product request id has any records pending reconciliation
****************************************************************/
FUNCTION get_reconciled_flg
  (
    p_requestid IN t525_product_request.c525_product_request_id%TYPE )
  RETURN VARCHAR2
IS
  v_yesno VARCHAR (2);
  v_count NUMBER;
BEGIN
  BEGIN
    SELECT COUNT (t412.c412_inhouse_trans_id)
    INTO v_count
    FROM t412_inhouse_transactions t412,
      t504a_loaner_transaction t504a,
      t526_product_request_detail t526
    WHERE t412.c504a_loaner_transaction_id   = t504a.c504a_loaner_transaction_id
    AND t504a.c526_product_request_detail_id = t526.c526_product_request_detail_id
    AND t526.c525_product_request_id         = p_requestid
    AND t412.c412_status_fl                  < 4 -- pending reconciliation
    AND t412.c412_void_fl                   IS NULL
    AND t504a.c504a_void_fl                 IS NULL
    AND t526.c526_void_fl                   IS NULL;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    RETURN 'N';
  END;
  IF v_count > 0 THEN
    v_yesno := 'Y';
  ELSE
    v_yesno := 'N';
  END IF;
  RETURN v_yesno;
END get_reconciled_flg;
/******************************************************************
* Description : This function returns the loaner status when consign id is passed
****************************************************************/
FUNCTION get_cs_fch_loaner_status
  (
    p_consign_id t504a_consignment_loaner.c504_consignment_id%TYPE )
  RETURN VARCHAR2
IS
  /* Description    : THIS FUNCTION RETURNS LOANER STATUS GIVEN THE CONSIGN ID
  Parameters       : p_consign_id
  */
  v_status t504a_consignment_loaner.c504a_status_fl%TYPE;
BEGIN
  BEGIN
    SELECT c504a_status_fl
    INTO v_status
    FROM t504a_consignment_loaner
    WHERE c504_consignment_id = p_consign_id
    AND c504a_void_fl        IS NULL;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    RETURN ' ';
  END;
  RETURN v_status;
END get_cs_fch_loaner_status;
/******************************************************************
* Description : Procedure to de-allocate the loaners
* Author    : Brinal G 
****************************************************************/
PROCEDURE gm_sav_deallocate
  (
    p_loaner_consign_id IN t504a_consignment_loaner.c504_consignment_id%TYPE,
    p_userid            IN t504_consignment.c504_created_by%TYPE )
AS
  v_loan_trans_id t504a_loaner_transaction.c504a_loaner_transaction_id%TYPE;
  v_req_det_id t526_product_request_detail.c526_product_request_detail_id%TYPE;
  v_req_type t526_product_request_detail.c901_request_type%TYPE;
  v_status t504a_consignment_loaner.c504a_status_fl%TYPE;
  v_country_fl VARCHAR2(10);
BEGIN
  BEGIN
    SELECT t504al.c504a_loaner_transaction_id,
      t504al.c526_product_request_detail_id
    INTO v_loan_trans_id,
      v_req_det_id
    FROM t504_consignment t504,
      t504a_loaner_transaction t504al
    WHERE t504.c504_consignment_id = t504al.c504_consignment_id
    AND t504al.c504a_return_dt    IS NULL
    AND t504.c504_consignment_id   = p_loaner_consign_id
    AND c504_void_fl              IS NULL
    AND c504a_void_fl             IS NULL;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    GM_RAISE_APPLICATION_ERROR('-20999','239','');
    v_loan_trans_id := 0;
    v_req_det_id    := 0;
  END;
  -- reset allocated fl and planned ship date in t504a
  gm_pkg_op_loaner_allocation.gm_loaner_allocation_update(v_req_det_id, 'CLEAR', p_userid);
  --resetting t907_shipping_info - c907_ref_id from consignment id to prod req detail id.
  SELECT NVL(GET_RULE_VALUE('US','COUNTRY'),'-999') INTO v_country_fl FROM DUAL;
  UPDATE t907_shipping_info
  SET C907_Ref_Id           = v_req_det_id,
    c907_status_fl          = DECODE(c907_status_fl, 30, 15, c907_status_fl), -- 15 pending Control
    c907_last_updated_by    = p_userid,
    c907_last_updated_date  = CURRENT_DATE
  WHERE c907_ref_id         = p_loaner_consign_id
  AND c907_void_fl         IS NULL
  AND c901_source          IN(50182) --50182-Loaners
  AND c907_tracking_number IS NULL
  AND c907_status_fl       <> 40;
  --Void the record in loaner trans table
  UPDATE t504a_loaner_transaction
  SET c504a_void_fl                 = 'Y',
    c504a_last_updated_date         = CURRENT_DATE,
    c504a_last_updated_by           = p_userid
  WHERE c504a_loaner_transaction_id = v_loan_trans_id;
  --change the status of the loaner request back to open
  -- call common procedure to update the status, 10: open
  gm_pkg_op_loaner_allocation.gm_upd_request_status(v_req_det_id, 10, p_userid);
  
  SELECT c504a_status_fl
  INTO v_status
  FROM t504a_consignment_loaner
  WHERE c504_consignment_id = p_loaner_consign_id
  AND c504a_void_fl        IS NULL;
  UPDATE t504a_consignment_loaner
  SET c504a_loaner_dt        = NULL,
    c504a_expected_return_dt = NULL,
    c504a_status_fl          = DECODE(v_country_fl,'-999',0,DECODE(c504a_status_fl, 10, 58 ,7, 0, c504a_status_fl)), --0, available, 7 pending pick, 58, pending putaway, 10 pending shipping
    c504a_last_updated_date  = CURRENT_DATE,
    c504a_last_updated_by    = p_userid
  WHERE c504_consignment_id  = p_loaner_consign_id
  AND c504a_void_fl         IS NULL;
  
  --Changes added for PMT-12455 and it's used to trigger an update to iPad
--  gm_pkg_op_loaner_set_txn.GM_SAV_AVAIL_LN_SET_UPDATE (p_loaner_consign_id, p_userid); -- consignment_id, user_id, company_id   
  
  SELECT C901_REQUEST_TYPE
  INTO v_req_type
  FROM t526_product_request_detail
  WHERE c526_product_request_detail_id = v_req_det_id
  AND c526_void_fl                    IS NULL;
  IF v_req_type                        = 4127 AND v_status = 10 --Product Loaner deallocated from pending shipping to pending putaway
    THEN
    gm_pkg_allocation.gm_ins_invpick_assign_detail (93604, p_loaner_consign_id, p_userid);
    --93604 Pending Putaway , to insert record in the t5050_INVPICK_ASSIGN_DETAIL
  END IF;
  IF v_req_type = 4127 AND v_status = 7 --Product Loaner deallocated from pending pick to available
    THEN
    -- Void Records In t5050_invpick_assign_detail for removing allocation in device.
    gm_pkg_allocation.gm_inv_void_bytxn (p_loaner_consign_id, 93602, p_userid);
  END IF;
  UPDATE t504_consignment
  SET c701_distributor_id   = NULL,
    c704_account_id         = NULL,
    c504_last_updated_date  = CURRENT_DATE,
    c504_last_updated_by    = p_userid
  WHERE c504_consignment_id = p_loaner_consign_id
  AND c504_void_fl         IS NULL;
END gm_sav_deallocate;
/******************************************************************
* created velu Aug 03 2010
* Description : Procedure to get the all values for report
****************************************************************/
PROCEDURE gm_fch_setmap
  (
    p_loanersetid  IN t207b_loaner_set_map.c207_loaner_set_id%TYPE,
    p_consignsetid IN t207b_loaner_set_map.c207_consign_set_id%TYPE,
    p_setmapvalue OUT TYPES.cursor_type )
AS
 v_date_format     varchar2(20); 
BEGIN
	 
  SELECT get_compdtfmt_frm_cntx()
    INTO v_date_format
    FROM dual;
    
  OPEN p_setmapvalue FOR SELECT c207_loaner_set_id loanersetid,
  get_set_name (c207_loaner_set_id) lname,
  c207_consign_set_id consignsetid,
  get_set_name (c207_consign_set_id) cname,
  DECODE (c207b_last_updated_date, NULL, TO_CHAR (c207b_created_date, v_date_format), TO_CHAR (c207b_last_updated_date, v_date_format) ) crdate,
  DECODE (c207b_last_updated_by, NULL, get_user_name (c207b_created_by), get_user_name (c207b_last_updated_by) ) crby FROM t207b_loaner_set_map WHERE c207_consign_set_id = NVL (p_consignsetid, c207_consign_set_id) AND c207_loaner_set_id = NVL (p_loanersetid, c207_loaner_set_id) AND c207b_void_fl IS NULL;
END gm_fch_setmap;
/******************************************************************
* created velu Aug 04 2010
* Description : Procedure to get the particular value
****************************************************************/
PROCEDURE gm_fch_editsetmap
  (
    p_loaner_setid IN t207b_loaner_set_map.c207_loaner_set_id%TYPE,
    p_setmapvalue OUT TYPES.cursor_type )
AS
BEGIN
  OPEN p_setmapvalue FOR SELECT c207_loaner_set_id loanersetid,
  c207_consign_set_id consignsetid FROM t207b_loaner_set_map WHERE c207_loaner_set_id = p_loaner_setid AND c207b_void_fl IS NULL;
END gm_fch_editsetmap;
/******************************************************************
* created velu Aug 04 2010
* Description : Procedure save the LoanerSetId value
****************************************************************/
PROCEDURE gm_sav_setmap
  (
    p_loaner_setid  IN t207b_loaner_set_map.c207_loaner_set_id%TYPE,
    p_consign_setid IN t207b_loaner_set_map.c207_consign_set_id%TYPE,
    p_user_id       IN t207b_loaner_set_map.c207b_created_by%TYPE )
AS
BEGIN
  UPDATE t207b_loaner_set_map
  SET c207_consign_set_id   = p_consign_setid,
    c207b_last_updated_by   = p_user_id,
    c207b_last_updated_date = CURRENT_DATE,
    c207b_void_fl           = NULL
  WHERE c207_loaner_set_id  = p_loaner_setid;
  IF (SQL%ROWCOUNT          = 0) THEN
    INSERT
    INTO t207b_loaner_set_map
      (
        c207_loaner_set_id,
        c207_consign_set_id,
        c207b_created_date,
        c207b_created_by
      )
      VALUES
      (
        p_loaner_setid,
        p_consign_setid,
        CURRENT_DATE,
        p_user_id
      );
  END IF;
END gm_sav_setmap;
/********************************************************************************
* created velu Sep 7 2010
* Description : Procedure to fetch the Open Request values
********************************************************************************/
PROCEDURE gm_fch_open_requests
  (
    p_distid   IN t525_product_request.c525_product_request_id%TYPE,
    p_req_type IN t526_product_request_detail.c901_request_type%TYPE,
    p_open_requests OUT TYPES.cursor_type
  )
AS
v_company_id  t1900_company.c1900_company_id%TYPE;
v_plant_id T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
v_loaner_filetr VARCHAR2(20);
BEGIN
	
	select get_compid_frm_cntx(), get_plantid_frm_cntx() into v_company_id, v_plant_id from dual;
	
	SELECT DECODE(GET_RULE_VALUE(v_company_id,'LOANERTRANSFERFILTER'), null, v_company_id, v_plant_id) --LOANERTRANSFERFILTER- To fetch the records for EDC entity countries based on plant
	  INTO v_loaner_filetr 
	  FROM DUAL;
	  
  OPEN p_open_requests FOR 
  	SELECT t526.c526_product_request_detail_id requestid,t526.c207_set_id setid 
  	FROM t526_product_request_detail t526,t525_product_request t525 
  	WHERE t526.c526_status_fl = 10 --open
  	AND t525.c525_product_request_id = t526.c525_product_request_id AND t525.c525_request_for_id = p_distid 
  	AND t526.c901_request_type = p_req_type 
  	AND t525.c525_void_fl IS NULL 
  	AND (t525.c1900_company_id = v_loaner_filetr OR t525.c5040_plant_id = v_loaner_filetr)
  	AND t525.c1900_company_id = t526.c1900_company_id 
  	AND t526.c526_void_fl IS NULL;
END gm_fch_open_requests;
/********************************************************************************
* created velu Sep 7 2010
* Description : Procedure to fetch the details for process transaction
********************************************************************************/
PROCEDURE gm_fch_process_transaction
  (
    p_conid IN t504a_consignment_loaner.c504_consignment_id%TYPE,
    p_details OUT TYPES.cursor_type
  )
AS
BEGIN
  OPEN p_details FOR SELECT t504.c504_consignment_id conid,
  t504.c207_set_id setid,
  get_set_name
  (
    t504.c207_set_id
  )
  sname,
  t504.c504_status_fl sfl,
  get_code_name
  (
    t504.c504_inhouse_purpose
  )
  pname,
  t504.c504_inhouse_purpose pid,
  t504a.c504a_etch_id etchid,
  t504a.c504a_status_fl lsfl,
  gm_pkg_op_loaner.get_loaner_status
  (
    t504a.c504a_status_fl
  )
  loansfl,
  TO_CHAR
  (
    t504a.c504a_expected_return_dt, GET_RULE_VALUE('DATEFMT', 'DATEFORMAT')
  )
  edate,
  TO_CHAR
  (
    c504a_loaner_dt, GET_RULE_VALUE('DATEFMT', 'DATEFORMAT')
  )
  ldate,
  get_cs_rep_loaner_bill_nm_add
  (
    t504.c504_consignment_id
  )
  loannm FROM t504_consignment t504,
  t504a_consignment_loaner t504a WHERE t504.c504_status_fl = '4' AND t504.c504_verify_fl = '1' AND t504.c504_void_fl IS NULL AND t504.c207_set_id IS NOT NULL AND t504.c504_type = 4127 AND UPPER
  (
    t504.c504_consignment_id
  )
  = UPPER
  (
    p_conid
  )
  AND t504.c504_consignment_id = t504a.c504_consignment_id(+);
END gm_fch_process_transaction;
/****************************
* Author      : Bgupta
* Description : Will move the loaner to pending picture stage
*****************************/
PROCEDURE gm_sav_loaner_process
  (
    p_consignid IN t504a_consignment_loaner.c504_consignment_id%TYPE,
    p_userid    IN t504_consignment.c504_created_by%TYPE,
    p_dplyfl    IN VARCHAR2
  )
AS
  v_type t504_consignment.c504_type%TYPE;
  v_status_fl t504a_consignment_loaner.c504a_status_fl%TYPE;
  v_set_id t504_consignment.c207_set_id%TYPE;
  v_hold_fl t504_consignment.c504_hold_fl%TYPE;
  v_planned_ship_date t504_consignment.c504_ship_date%TYPE;
  v_req_det_id t526_product_request_detail.c526_product_request_detail_id%TYPE;
  v_company_id    t1900_company.c1900_company_id%TYPE;
  v_msg varchar2(4000);
BEGIN
	
  SELECT t504a.c504a_status_fl,
    t504.c504_type,
    t504.c207_set_id,
    t504.c504_hold_fl,
    t504.c1900_company_id
  INTO v_status_fl,
    v_type,
    v_set_id,
    v_hold_fl,
    v_company_id
  FROM t504a_consignment_loaner t504a,
    t504_consignment t504
  WHERE t504.c504_consignment_id = p_consignid
  AND t504.c504_consignment_id   = t504a.c504_consignment_id
  AND t504a.c504a_void_fl       IS NULL
  AND t504.c504_void_fl         IS NULL FOR UPDATE;
  IF v_status_fl                != '50' THEN
    raise_application_error (-20928, '');
  END IF;
  IF p_dplyfl ='Y' AND v_hold_fl = 'Y' THEN
    GM_RAISE_APPLICATION_ERROR('-20999','240','');
  END IF;
  IF v_type = 4127 THEN
  --55: Pending Picture
  	If get_rule_value (v_company_id,'SKIP_PENDING_PICTURE')  = 'Y' THEN
  	UPDATE t504a_consignment_loaner 
  	   set c504a_status_fl='55' 
  	 WHERE c504_consignment_id = p_consignid
       AND c504a_void_fl IS NULL;

		gm_pkg_op_loaner.gm_sav_process_picture(p_consignid, p_userid,v_msg);
  	else
  		gm_pkg_op_loaner_set_txn.gm_upd_consign_loaner_status (p_consignid, 55, p_userid) ;
   END IF;	
  elsif v_type                = 4119 THEN
    IF v_hold_fl              = 'Y' THEN
    BEGIN
      SELECT t526.c526_product_request_detail_id,
        t526.c526_required_date
      INTO v_req_det_id,
        v_planned_ship_date
      FROM t504a_consignment_loaner t504a,
        t504a_loaner_transaction t504al,
        t526_product_request_detail t526
      WHERE t504a.c504_consignment_id           = t504al.c504_consignment_id
      AND t504al.c526_product_request_detail_id =t526.c526_product_request_detail_id
      AND t504al.c504a_return_dt               IS NULL
      AND t504a.c504_consignment_id             = p_consignid
      AND t504a.c504a_void_fl                  IS NULL
      AND t504al.c504a_void_fl                 IS NULL
      AND t526.c526_void_fl                    IS NULL;
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
      	v_req_det_id := NULL;
      	v_planned_ship_date := NULL;
      END;
      UPDATE t504_consignment
      SET c504_hold_fl          = NULL,
        c504_last_updated_date  = CURRENT_DATE,
        c504_last_updated_by    = p_userid
      WHERE c504_consignment_id = p_consignid
      AND c504_void_fl         IS NULL;
      IF v_planned_ship_date   <= TRUNC (CURRENT_DATE)
        -- move the loaner to pend shipping
        THEN
        gm_pkg_op_loaner.gm_sav_status_update (v_req_det_id, p_userid);
      ELSE -- mark it as allocated
      --(58 pending FG, 5 allocated)
      gm_pkg_op_loaner_set_txn.gm_upd_consign_loaner_status (p_consignid, 58, p_userid) ;
      END IF;
    ELSIF p_dplyfl ='Y' THEN
     --23: Deployed; Call the common procedure to update the status
    	gm_pkg_op_loaner_set_txn.gm_upd_consign_loaner_status (p_consignid, 23, p_userid) ;
    	
    UPDATE t504a_consignment_loaner
      SET c504a_deployed_fl       = 'Y',
        c504a_missed_fl         = NULL,
        c504a_last_updated_date = CURRENT_DATE,
        c504a_last_updated_by   = p_userid
      WHERE c504_consignment_id = p_consignid
      AND c504a_void_fl        IS NULL;
    ELSE
    --(58 pending FG, 0 Available)
    	gm_pkg_op_loaner_set_txn.gm_upd_consign_loaner_status (p_consignid, 58, p_userid) ;
    	
      UPDATE t504a_consignment_loaner
      SET c504a_missed_fl         = NULL,
        c504a_deployed_fl       =NULL,
        c504a_last_updated_date = CURRENT_DATE,
        c504a_last_updated_by   = p_userid
      WHERE c504_consignment_id = p_consignid
      AND c504a_void_fl        IS NULL;
      IF v_type             = 4119 -- In-House Loaner
        THEN
        gm_pkg_allocation.gm_ins_invpick_assign_detail (93604, p_consignid, p_userid);--93604 Pending Putaway , to insert record in the t5050_INVPICK_ASSIGN_DETAIL
      END IF;
      gm_pkg_op_loaner.gm_sav_allocate (v_set_id, v_type, p_userid);
    END IF;
  END IF;
END gm_sav_loaner_process;
/****************************
* Author      : Rajkumar
* Description : Pending Picture Process and Allocate the Set To Available Loaner
*****************************/
PROCEDURE gm_sav_process_picture
  (
    p_consignid IN t504a_consignment_loaner.c504_consignment_id%TYPE,
    p_userid    IN t504_consignment.c504_created_by%TYPE,
    p_msg OUT VARCHAR2 )
AS
  v_type t504_consignment.c504_type%TYPE;
  v_setid t504_consignment.c207_set_id%TYPE;
  v_hold_fl t504_consignment.c504_hold_fl%TYPE;
  v_planned_ship_date t504_consignment.c504_ship_date%TYPE;
  v_req_det_id t526_product_request_detail.c526_product_request_detail_id%TYPE;
  v_status_fl t504a_consignment_loaner.c504a_status_fl%TYPE;
  v_allocate_fl t504a_consignment_loaner.c504a_allocated_fl%TYPE;
  v_country VARCHAR2(10);
BEGIN
  SELECT c504a_status_fl
  INTO v_status_fl
  FROM t504a_consignment_loaner
  WHERE c504_consignment_id = p_consignid
  AND c504a_void_fl        IS NULL FOR UPDATE;
  IF v_status_fl           != '55' THEN
    -- Error message is Sorry, this transaction has already been voided.
    raise_application_error (-20929, '');
  END IF;
   --(58 Pending FG) ( 0 available)
  gm_pkg_op_loaner_set_txn.gm_upd_consign_loaner_status (p_consignid, 58, p_userid) ;
  
  SELECT c504_type,
    c504_hold_fl,
    c207_set_id
  INTO v_type,
    v_hold_fl,
    v_setid
  FROM t504_consignment
  WHERE c504_consignment_id = p_consignid
  AND c504_void_fl         IS NULL;
  IF v_hold_fl              = 'Y' THEN
    -- unhold the transaction
    UPDATE t504_consignment
    SET c504_hold_fl          = NULL,
      c504_last_updated_date  = CURRENT_DATE,
      c504_last_updated_by    = p_userid
    WHERE c504_consignment_id = p_consignid
    AND c504_void_fl         IS NULL;
  END IF;
  IF v_type = 4127 -- if product loaner then allocate
    THEN
    gm_pkg_op_loaner.gm_sav_allocate (v_setid, v_type, p_userid);
    p_msg := gm_pkg_op_loaner.get_shipping_message(p_consignid, p_userid);
    --check if country is US or OUS, If it�s OUS, need to skip status �Pending putaway� after allocated, should be available or pending ship
    SELECT get_rule_value ('US', 'COUNTRY')
    INTO v_country
    FROM DUAL;
    IF NVL(v_country, '-999') = 'Y' THEN
      --if set need to ship today, then need change to pending shipping
      IF p_msg IS NOT NULL THEN
        gm_pkg_op_set_txn.gm_sav_set_txn_status (p_consignid, 'PUT', v_type, p_userid);
        gm_pkg_allocation.gm_sav_set_complete(p_consignid, 93604, p_userid, 93006); --Completed. => Update user id in t5050 for the transaction
      ELSE
        --93604 Product Loaner (pending putaway , to insert record in the t5050_INVPICK_ASSIGN_DETAIL
        gm_pkg_allocation.gm_ins_invpick_assign_detail (93604, p_consignid, p_userid);
      END IF;
    ELSE
      gm_pkg_op_loaner_set_txn.gm_sav_loaner_status (p_consignid, 'PUT', p_userid) ;
    END IF;
  END IF;
  
  -- Need to remove the priority while doing process picture
  	gm_pkg_op_ln_priority_engine.gm_rollback_cn_priority(null,p_userid,p_consignid);
  	
END gm_sav_process_picture;
/****************************
* Author      : Brinal
* Description : to update the status of inhouse loaner
*****************************/
PROCEDURE gm_sav_inhouse_loaner
  (
    p_inhouse_trans_id IN t412_inhouse_transactions.c412_inhouse_trans_id%TYPE,
    p_userid           IN t412_inhouse_transactions.c412_created_by%TYPE )
AS
  v_qty   NUMBER;
  v_stock NUMBER;
  v_cnt   NUMBER;
  v_ref_id t412_inhouse_transactions.c412_ref_id%TYPE;
  v_status t504a_consignment_loaner.c504a_status_fl%TYPE;
  v_msg VARCHAR2(4000);
  v_statusfl t412_inhouse_transactions.c412_status_fl%TYPE;
BEGIN
  --Below select has been added to resolve the dead lock condition.
  SELECT c412_status_fl
  INTO v_statusfl
  FROM t412_inhouse_transactions
  WHERE c412_inhouse_trans_id = p_inhouse_trans_id FOR UPDATE;
  SELECT get_qty_in_stock (c205_part_number_id),
    c413_item_qty
  INTO v_stock,
    v_qty
  FROM t413_inhouse_trans_items
  WHERE c412_inhouse_trans_id = p_inhouse_trans_id
  AND c413_void_fl           IS NULL FOR UPDATE;
  IF v_qty                    > v_stock THEN
    raise_application_error (-20792, '');
  END IF;
  UPDATE t412_inhouse_transactions
  SET c412_status_fl          = 2,
    c412_type                 = 50155, -- Move from Shelf to Loaner Set
    c412_last_updated_date    = CURRENT_DATE,
    c412_last_updated_by      = p_userid
  WHERE c412_inhouse_trans_id = p_inhouse_trans_id;
  /*
  * if a back order transaction is released for a Loaner,
  * and if the CN is in Allocated or Pending Shipping status
  *   change the status of the loaner to WIP Loaner and Hold the CN
  * ,else if  it is in Pending Verification / Available / Pending Process / Pending Picture
  *     move to WIP - Loaners
  * else
  *      don't update the status of the loaner
  *
  */
  BEGIN
    SELECT c412_ref_id
    INTO v_ref_id
    FROM t412_inhouse_transactions
    WHERE c412_inhouse_trans_id = p_inhouse_trans_id;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    GM_RAISE_APPLICATION_ERROR('-20999','241',p_inhouse_trans_id);
  END;
  SELECT gm_pkg_op_loaner.get_cs_fch_loaner_status (v_ref_id)
  INTO v_status
  FROM DUAL;
  IF v_ref_id IS NOT NULL AND v_status NOT IN ('20', '25', '60') THEN
    gm_ls_upd_csg_loanerstatus (0, 30, v_ref_id, p_userid);
    gm_update_log (v_ref_id, 'Parts for the Back Order transaction ' || p_inhouse_trans_id || ' has been released. So changing the loaner status to WIP - Loaners' ,1222 ,p_userid ,v_msg );
  END IF;
  -- End of Hold Txn Logic
  -- Entry for T5050_INVPICK_ASSIGN_DETAIL for Move from Shelf to Loaner Set 93323
  gm_pkg_allocation.gm_ins_invpick_assign_detail (93323, p_inhouse_trans_id, 30301 );
END gm_sav_inhouse_loaner;
/****************************
* Author      : Rajkumar
* Description : Fecthing Set List
*****************************/
PROCEDURE gm_fch_set_list
  (
    p_data_out OUT TYPES.cursor_type)
AS
BEGIN
  OPEN p_data_out FOR SELECT c207_set_id ID,
  (c207_set_id || '-' || c207_set_nm) idname FROM t207_set_master WHERE c207_void_fl IS NULL ORDER BY c207_set_id;
END gm_fch_set_list;
PROCEDURE gm_fch_back_order
  (
    p_data_out OUT TYPES.cursor_type)
AS
BEGIN
  OPEN p_data_out FOR SELECT t412.c412_inhouse_trans_id ID,
  c412_ref_id master_id,
  t413.c205_part_number_id num,
  get_partnum_desc (t413.c205_part_number_id) dsc,
  t413.c413_item_qty qty FROM t412_inhouse_transactions t412,
  t413_inhouse_trans_items t413 WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id AND c412_status_fl = 0 AND t413.c413_void_fl IS NULL AND c412_void_fl IS NULL;
END gm_fch_back_order;
PROCEDURE gm_fch_loaner_status_log
  (
    p_userid    IN t905_status_details.c905_updated_by%TYPE,
    p_set_id    IN t207_set_master.c207_set_id%TYPE,
    p_status    IN t905_status_details.c905_status_fl%TYPE,
    p_from_date IN t905_status_details.c905_updated_date%TYPE,
    p_to_date   IN t905_status_details.c905_updated_date%TYPE,
    p_data_out OUT TYPES.cursor_type )
AS
  v_company_id      t1900_company.c1900_company_id%TYPE;
BEGIN
	
    SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL')) 
      INTO v_company_id
      FROM dual;
  OPEN p_data_out FOR SELECT gm_pkg_op_loaner.get_loaner_etch_id (refid) etchid,
  gm_pkg_op_consignment.get_set_id (refid) setid,
  get_set_name (gm_pkg_op_consignment.get_set_id (refid) ) setdesc,
  (SELECT c905_updated_by
  FROM t905_status_details
  WHERE c905_ref_id  = refid
  AND c905_status_fl = 20
  AND c905_status_fl = NVL (p_status, c905_status_fl)
  ) accby,
  (SELECT c905_updated_by
  FROM t905_status_details
  WHERE c905_ref_id  = refid
  AND c905_status_fl = 25
  AND c905_status_fl = NVL (p_status, c905_status_fl)
  ) chkby,
  (SELECT c905_updated_by
  FROM t905_status_details
  WHERE c905_ref_id  = refid
  AND c905_status_fl = 50
  AND c905_status_fl = NVL (p_status, c905_status_fl)
  ) prcby,
  (SELECT c905_updated_by
  FROM t905_status_details
  WHERE c905_ref_id  = refid
  AND c905_status_fl = 55
  AND c905_status_fl = NVL (p_status, c905_status_fl)
  ) picby,
  (SELECT TO_CHAR (c905_updated_date, GET_RULE_VALUE('DATEFMT', 'DATEFORMAT'))
  FROM t905_status_details
  WHERE c905_ref_id  = refid
  AND c905_status_fl = 20
  AND c905_status_fl = NVL (p_status, c905_status_fl)
  ) accdate,
  (SELECT TO_CHAR (c905_updated_date, GET_RULE_VALUE('DATEFMT', 'DATEFORMAT'))
  FROM t905_status_details
  WHERE c905_ref_id  = refid
  AND c905_status_fl = 25
  AND c905_status_fl = NVL (p_status, c905_status_fl)
  ) chkdate,
  (SELECT TO_CHAR (c905_updated_date, GET_RULE_VALUE('DATEFMT', 'DATEFORMAT'))
  FROM t905_status_details
  WHERE c905_ref_id  = refid
  AND c905_status_fl = 50
  AND c905_status_fl = NVL (p_status, c905_status_fl)
  ) prcdate,
  (SELECT TO_CHAR (c905_updated_date, GET_RULE_VALUE('DATEFMT', 'DATEFORMAT'))
  FROM t905_status_details
  WHERE c905_ref_id  = refid
  AND c905_status_fl = 55
  AND c905_status_fl = NVL (p_status, c905_status_fl)
  ) picdate,
  (SELECT TO_CHAR (c905_updated_date, 'HH:MM:SS')
  FROM t905_status_details
  WHERE c905_ref_id  = refid
  AND c905_status_fl = 20
  AND c905_status_fl = NVL (p_status, c905_status_fl)
  ) acctime,
  (SELECT TO_CHAR (c905_updated_date, 'HH:MM:SS')
  FROM t905_status_details
  WHERE c905_ref_id  = refid
  AND c905_status_fl = 25
  AND c905_status_fl = NVL (p_status, c905_status_fl)
  ) chktime,
  (SELECT TO_CHAR (c905_updated_date, 'HH:MM:SS')
  FROM t905_status_details
  WHERE c905_ref_id  = refid
  AND c905_status_fl = 50
  AND c905_status_fl = NVL (p_status, c905_status_fl)
  ) prctime,
  (SELECT TO_CHAR (c905_updated_date, 'HH:MM:SS')
  FROM t905_status_details
  WHERE c905_ref_id  = refid
  AND c905_status_fl = 55
  AND c905_status_fl = NVL (p_status, c905_status_fl)
  ) pictime,
  (SELECT c504a_loaner_transaction_id
  FROM t504a_loaner_transaction
  WHERE c504_consignment_id = refid
  ) transid,
  (SELECT c208_set_qty
  FROM t208_set_details
  WHERE c207_set_id = gm_pkg_op_consignment.get_set_id (refid)
  ) totpart FROM
  (SELECT c905_ref_id refid
  FROM t905_status_details
  WHERE c905_updated_by = NVL (p_userid, c905_updated_by)
  AND c905_status_fl    = NVL (p_status, c905_status_fl)
  AND c905_updated_date BETWEEN NVL (p_from_date, c905_updated_date ) 
  AND NVL (p_to_date, c905_updated_date )
  AND C1900_COMPANY_ID =v_company_id
  );
END gm_fch_loaner_status_log;
/******************************************************************
* Description : Procedure to void Loaner items
* Author    : dreddy
****************************************************************/
PROCEDURE gm_void_inhouse_loaner
  (
    p_inhouse_Trans_id IN t412_inhouse_transactions.c412_inhouse_trans_id%TYPE,
    p_userid           IN t412_inhouse_transactions.C412_CREATED_BY%TYPE )
AS
  v_void_fl T412_INHOUSE_TRANSACTIONS.C412_VOID_FL%TYPE;
  v_inhouse_txn_id T412_INHOUSE_TRANSACTIONS.c412_inhouse_trans_id%TYPE;
  v_status_fl T412_INHOUSE_TRANSACTIONS.c412_status_fl%TYPE;
  v_type T412_INHOUSE_TRANSACTIONS.c412_type%TYPE;
  v_company_id    T412_INHOUSE_TRANSACTIONS.c1900_company_id%TYPE;
  v_allow_txn_void_fl	   VARCHAR2 (1);
BEGIN
		
  SELECT c412_inhouse_trans_id,
    C412_VOID_FL,
    c412_status_fl,c412_type,c1900_company_id
  INTO v_inhouse_txn_id,
    v_void_fl,
    v_status_fl,v_type,v_company_id
  FROM T412_INHOUSE_TRANSACTIONS
  WHERE c412_inhouse_trans_id = p_inhouse_Trans_id FOR UPDATE;
  --back order transaction should voided only for GMNA
  v_allow_txn_void_fl	:= get_rule_value_by_company (v_type, 'INHOUSEVOID',v_company_id);
  
  IF v_void_fl               IS NOT NULL THEN
    GM_RAISE_APPLICATION_ERROR('-20999','242',p_inhouse_Trans_id);
  END IF;
  IF v_status_fl != 0 THEN
    GM_RAISE_APPLICATION_ERROR('-20999','243',p_inhouse_Trans_id);
  END IF;
  
  IF v_allow_txn_void_fl = 'Y' THEN          -- US
  UPDATE T412_INHOUSE_TRANSACTIONS
  SET C412_VOID_FL            = 'Y',
    C412_LAST_UPDATED_DATE    = CURRENT_DATE,
    C412_LAST_UPDATED_BY      = p_userid
  WHERE c412_inhouse_trans_id = p_inhouse_Trans_id;
  ELSE
  	raise_application_error (-20781, ''); --OUS countries 
  END IF;
END gm_void_inhouse_loaner;
/******************************************************************
* Description : Procedure to move the status of a loaner in WIP
*               to pending process
* Author    : dreddy
****************************************************************/
PROCEDURE gm_mov_to_pending_process
  (
    p_conid  IN t504_consignment.c504_consignment_id%TYPE,
    p_userid IN t504_consignment.c504_last_updated_by%TYPE )
AS
  v_status_fl t504a_consignment_loaner.c504a_status_fl%TYPE;
  v_cnt NUMBER;
BEGIN
  SELECT c504a_status_fl
  INTO v_status_fl
  FROM t504a_consignment_loaner t504a
  WHERE c504_consignment_id = p_conid
  AND c504a_void_fl        IS NULL;
  IF v_status_fl           != '30' THEN
    GM_RAISE_APPLICATION_ERROR('-20999','244',p_conid); 
  END IF;
  SELECT COUNT (1)
  INTO v_cnt
  FROM t412_inhouse_transactions
  WHERE c412_status_fl > 1
  AND c412_status_fl   < 4
  AND c412_ref_id      = p_conid
  AND c412_type NOT   IN (50159, 1006571, 40051, 40052, 40053, 40054, 40055, 40056, 100069, 100075, 100076 )
  AND c412_void_fl    IS NULL;
  IF v_cnt             > 0 THEN
    raise_application_error (-20930, '');
  END IF;
  
  gm_pkg_op_loaner_set_txn.gm_upd_consign_loaner_status (p_conid, 50, p_userid) ;
END gm_mov_to_pending_process;

/**************************************************************
* Description : this is used to get the loaner return late charge
* Author      : dreddy
***************************************************************/
FUNCTION get_loaner_late_charge
  (
    p_expected_date IN DATE,
    p_actual_date   IN DATE,
    p_reqid         IN t525_product_request.c525_product_request_id%TYPE)
  RETURN NUMBER
AS
  v_days_late      NUMBER;
  v_least_amt_days NUMBER;
  v_least_amt      NUMBER;
  v_late_charge    NUMBER;
  v_fee_chgd_date DATE;
  v_amount    NUMBER;
  v_no_days   NUMBER;
  v_rule_days NUMBER;
  v_loaner_dt DATE;
  v_ex_rt_dt DATE;
  v_late_charge_date DATE;
BEGIN
  SELECT MAX (t504a.c504a_loaner_dt),
    MAX(t504a.c504a_expected_return_dt),
    MAX(t525.c525_l_fee_chgd_date)
  INTO v_loaner_dt,
    v_ex_rt_dt,
    v_late_charge_date
  FROM t504a_consignment_loaner t504,
    t504a_loaner_transaction t504a,
    t526_product_request_detail t526,
    t525_product_request t525
  WHERE TRUNC (t504a.c504a_expected_return_dt) < TRUNC (CURRENT_DATE)
  AND t504.c504_consignment_id                 = t504a.c504_consignment_id
  AND t504.c504a_missed_fl                    IS NULL
  AND t504a.c504a_void_fl                     IS NULL
  AND t504.c504a_void_fl                      IS NULL
  AND t526.c526_void_fl                       IS NULL
  AND t526.c526_product_request_detail_id      = t504a.c526_product_request_detail_id
  AND t525.c525_product_request_id             = t526.c525_product_request_id
  AND t525.c525_product_request_id             = p_reqid;
  
  v_days_late      := get_weekdaysdiff (p_expected_date, p_actual_date) ;
  v_least_amt_days := get_rule_value ('LATEDAYS', 'LATEDAYS') ;
  v_least_amt      := get_rule_value ('1006425', 'CHARGES') ;
  v_rule_days      := TO_NUMBER (get_rule_value ('DATE', 'EXPRTDT'));
  v_no_days        := get_work_days_count (TO_CHAR (v_loaner_dt, GET_RULE_VALUE('DATEFMT', 'DATEFORMAT')), TO_CHAR (v_ex_rt_dt, GET_RULE_VALUE('DATEFMT', 'DATEFORMAT')) );
  IF v_rule_days    > v_no_days AND v_late_charge_date IS NULL THEN
    v_days_late    := v_days_late - (v_rule_days - v_no_days);
  END IF;
  IF v_days_late <= 0 THEN
    RETURN 0;
  END IF;
  BEGIN
    SELECT c525_l_fee_chgd_date
    INTO v_fee_chgd_date
    FROM t525_product_request
    WHERE c525_product_request_id = p_reqid
    AND c525_void_fl             IS NULL;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    v_fee_chgd_date := NULL;
  END;
  BEGIN
    SELECT SUM (NVL(t9201.c9201_amount,0))
    INTO v_amount
    FROM t9200_incident t9200,
      t9201_charge_details t9201
    WHERE t9200.c9200_ref_id     = p_reqid
    AND t9200.c9200_incident_id  = t9201.c9200_incident_id
    AND c9200_ref_type           = 4127
    AND t9200.c901_incident_type = 92072
    AND t9200.c9200_void_fl     IS NULL
    AND t9201.c9201_void_fl     IS NULL
    GROUP BY t9200.c9200_ref_id;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    v_amount := 0;
  END;
  IF v_fee_chgd_date IS NOT NULL AND v_amount<>0 THEN
    IF v_days_late    > 0 AND v_amount >= 100 THEN
      v_late_charge  := v_days_late * get_rule_value ('92072', 'CHARGES');
    ELSIF v_days_late > 0 AND v_amount = 50 THEN
      v_late_charge  := v_least_amt + ((v_days_late - 1) * get_rule_value ('92072', 'CHARGES'));
    ELSE
      v_late_charge:= 0;
    END IF;
  ELSE
    IF v_days_late   <= v_least_amt_days AND v_days_late <> 0 THEN
      v_late_charge  := v_days_late * v_least_amt ; -- $50 first 2days
    ELSIF v_days_late > v_least_amt_days THEN
      v_late_charge  := v_least_amt_days * v_least_amt ;                                                             -- $50 for first 2days
      v_late_charge  := v_late_charge    + ((v_days_late - v_least_amt_days) * get_rule_value ('92072', 'CHARGES')); -- $100 from 3rd day onwards
    ELSE
      v_late_charge := 0;
    END IF;
  END IF;
  RETURN v_late_charge;
END get_loaner_late_charge;

/**************************************************************
* Description : this is used to get product request id by transactionid
* Author      : dreddy
***************************************************************/
FUNCTION get_loaner_request_id
  (
    p_txn_id IN t504a_loaner_transaction.c504a_loaner_transaction_id%TYPE)
  RETURN NUMBER
AS
  v_request_id NUMBER;
BEGIN
  BEGIN
    SELECT c525_product_request_id
    INTO v_request_id
    FROM t526_product_request_detail
    WHERE c526_product_request_detail_id =
      (SELECT c526_product_request_detail_id
      FROM t504a_loaner_transaction
      WHERE c504a_loaner_transaction_id = p_txn_id
      AND c504a_void_fl                IS NULL
      ) ;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    v_request_id := NULL;
  END;
  RETURN v_request_id;
END get_loaner_request_id;

/**************************************************************
* Description : this is used to get product request id by transactionid
* Author      : dreddy
***************************************************************/
FUNCTION get_product_loaner_request_id
  (
    p_txn_id IN t504a_loaner_transaction.c504a_loaner_transaction_id%TYPE)
  RETURN NUMBER
AS
  v_request_id t526_product_request_detail.c526_product_request_detail_id%TYPE;
BEGIN
  BEGIN
	SELECT c526_product_request_detail_id  
		INTO v_request_id
      FROM t504a_loaner_transaction
      WHERE c504a_loaner_transaction_id = p_txn_id
      AND c504a_void_fl                IS NULL;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    v_request_id := NULL;
  END;
  RETURN v_request_id;
END get_product_loaner_request_id;

/*******************************************************************
*  Description : this procedure is calculate missing part charge at set level
*  Author      : dreddy
*****************************************************************/
PROCEDURE gm_sav_set_level_missing_chrg
  (
    p_repid  VARCHAR2,
    p_distid VARCHAR2
  )
AS
  v_set_charge NUMBER;
  v_incident_id t9200_incident.c9200_incident_id%TYPE;
  CURSOR v_setcursor
  IS
    SELECT DISTINCT t526.c526_product_request_detail_id reqdet,
      t526.c207_set_id inst_value,
      t526.c525_product_request_id ref_id,
      t412.c412_inhouse_trans_id trans_id,
      t504a.c703_ass_rep_id assocrep_id
    FROM t413_inhouse_trans_items t413,
      t412_inhouse_transactions t412,
      t504a_loaner_transaction t504a ,
      t526_product_request_detail t526
    WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
    AND t413.c205_part_number_id    IN
      (SELECT c205_part_number_id
      FROM t208_set_details
      WHERE c207_set_id IN
        (SELECT c906_rule_value
        FROM t906_rules
        WHERE c906_rule_grp_id = 'CHARGES'
        AND c906_rule_id       = 'SETS'
        AND c906_void_fl      IS NULL
        )
    AND c208_void_fl IS NULL
      )
    AND c412_status_fl                       < 4
    AND c412_type                            = 50159
    AND t412.c504a_loaner_transaction_id     = t504a.c504a_loaner_transaction_id
    AND t504a.c526_product_request_detail_id = t526.c526_product_request_detail_id
    AND c412_expected_return_date            = TRUNC (CURRENT_DATE)
    AND t413.c413_void_fl                   IS NULL
    AND t412.c412_void_fl                   IS NULL
    AND t504a.c504a_void_fl                 IS NULL
    AND t526.c526_void_fl                   IS NULL
    ORDER BY t526.c525_product_request_id;
  BEGIN
    v_set_charge := get_rule_value ('SET_CHARGE', 'CHARGES') ;
    FOR v_index  IN v_setcursor
    LOOP
      gm_pkg_op_loaner.gm_sav_incidents (50891, v_index.ref_id, 92069, v_index.inst_value, 30301, TRUNC (CURRENT_DATE), v_incident_id) ;
      gm_pkg_op_loaner.gm_sav_charge_details (v_incident_id, p_repid, p_distid, v_set_charge, 20, 30301, v_index.assocrep_id) ; --
      -- approved
      UPDATE t412_inhouse_transactions
      SET c412_status_fl = 4 --completed
        ,
        c412_verified_date        = CURRENT_DATE ,
        c412_verified_by          = 30301 ,
        c412_last_updated_date    = CURRENT_DATE ,
        c412_last_updated_by      = 30301
      WHERE c412_inhouse_trans_id = v_index.trans_id
      AND c412_void_fl           IS NULL;
      -- END IF;
    END LOOP;
  END gm_sav_set_level_missing_chrg;
  /****************************************************************
  * Description : this function is used to get loaner expected return date
  * Author      : Dhanareddy
  *****************************************************************/
FUNCTION get_loaner_expected_return_dt
  (
    p_date IN DATE)
  RETURN DATE
IS
  v_return_dt DATE;
  v_rule_days  NUMBER;
  v_day_number NUMBER;
  v_company_id NUMBER;
  
BEGIN

	SELECT   get_compid_frm_cntx()
	  INTO   v_company_id
	  FROM   DUAL;
	  
  SELECT TO_NUMBER (get_rule_value ('DATE', 'EXPRTDT'))
  INTO v_rule_days
  FROM dual;
  BEGIN
    SELECT WEEK_DAY_NUMBER
    INTO v_day_number
    FROM date_dim
    WHERE TRUNC (cal_date) = TO_DATE (p_date) 
     AND date_key_id  IN (
				              	SELECT date_key_id 
				              	  FROM COMPANY_HOLIDAY_DIM 
				              	 WHERE HOLIDAY_DATE   = TO_DATE (p_date)
				              	   AND HOLIDAY_FL = 'Y'
				              	    AND C1900_COMPANY_ID = v_company_id
								)
    ;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    v_day_number := 0;
  END;
  IF v_day_number NOT               IN (0,1,7) THEN
    v_rule_days       := v_rule_days - 1;
  END IF;
  BEGIN
    SELECT caldate
    INTO v_return_dt
    FROM
      (SELECT caldate,
        ROWNUM rnum
      FROM
        (SELECT cal_date caldate
        FROM date_dim
        WHERE DAY NOT IN ('Sat','Sun')
        AND TRUNC (cal_date) >= TRUNC (p_date)
        AND TRUNC (cal_date) <= TRUNC (CURRENT_DATE) + 30
	    AND date_key_id NOT IN (
				              	SELECT date_key_id 
				              	  FROM COMPANY_HOLIDAY_DIM 
				              	 WHERE HOLIDAY_DATE   >= TRUNC (p_date)
								   AND HOLIDAY_DATE   <= TRUNC (CURRENT_DATE) + 30
				              	   AND HOLIDAY_FL = 'Y'
				              	    AND C1900_COMPANY_ID = v_company_id)
								
        ORDER BY TRUNC (cal_date) ASC
        )
      )
    WHERE rnum = v_rule_days +1;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    v_return_dt := NULL;
  END;
  RETURN v_return_dt;
END get_loaner_expected_return_dt;
/*******************************************************************
*  Description : Moving loaner set to Missed status
*  Author      : Dhana Reddy
*****************************************************************/
PROCEDURE gm_upd_loaner_missed
  (
    p_consign_id IN t504a_consignment_loaner.c504_consignment_id%TYPE ,
    p_userid     IN t102_user_login.c102_user_login_id%TYPE )
AS
v_loan_txn_id	t504a_loaner_transaction.c504a_loaner_transaction_id%TYPE;

--Get Tag Id Based on Consignment by Last Updated Trans ID
   CURSOR tag_id_cur
      IS
                SELECT c5010_tag_id tag_id 
                FROM t5010_tag 
                WHERE c5010_last_updated_trans_id = p_consign_id 
                AND c5010_void_fl IS NULL;
BEGIN
	--PC-239 To update missing flag as 'Y' in t504a_loaner_transaction
	SELECT MAX(c504a_loaner_transaction_id) 
	  INTO v_loan_txn_id
   	  FROM t504a_loaner_transaction 
 	 WHERE c504_consignment_id = p_consign_id
  	   AND c504a_void_fl IS NULL;
  	
  	UPDATE t504a_loaner_transaction
       SET c504a_missing_fl        = 'Y',
           c504a_last_updated_date = CURRENT_DATE,
           c504a_last_updated_by   = p_userid
     WHERE c504a_loaner_transaction_id = v_loan_txn_id
       AND c504a_void_fl IS NULL;  
	
	gm_pkg_op_loaner_set_txn.gm_upd_consign_loaner_status (p_consign_id, 22, p_userid) ;
  UPDATE t504a_consignment_loaner
  SET c504a_missed_fl         = 'Y',
    c504a_last_updated_date = CURRENT_DATE,
    c504a_last_updated_by   = p_userid
  WHERE c504_consignment_id = p_consign_id
  AND c504a_void_fl        IS NULL;
  
  --PMT-35507-Flagging Loaner Sets as Missing
   FOR tag_id_upd IN tag_id_cur
    LOOP   
    	--Call gm_save_missing_tag procedure to Update the Status as Missing 	
         gm_pkg_set_missing.gm_save_missing_tag(tag_id_upd.tag_id,CURRENT_DATE,p_consign_id,p_userid);       
    END LOOP;
END gm_upd_loaner_missed;
/******************************************************************
* Description : Function will return message if set need ship today
* ****************************************************************/
FUNCTION get_shipping_message
  (
    p_consignid IN t504a_consignment_loaner.c504_consignment_id%TYPE,
    p_userid    IN t504_consignment.c504_created_by%TYPE)
  RETURN VARCHAR2
IS
  v_planned_ship_date DATE;
  v_set_id t504_consignment.c207_set_id%TYPE;
  v_message VARCHAR2 (100);
  v_alloc_fl VARCHAR2 (10);
BEGIN
  SELECT t504a.c504a_planned_ship_date,
    t504.c207_set_id, t504a.c504a_allocated_fl 
  INTO v_planned_ship_date,
    v_set_id, v_alloc_fl
  FROM t504a_consignment_loaner t504a,
    t504_consignment t504
  WHERE t504a.c504_consignment_id = p_consignid
  AND t504a.c504_consignment_id   = t504.c504_consignment_id
  AND t504a.c504a_void_fl        IS NULL
  AND t504.c504_void_fl          IS NULL;
  IF TRUNC (v_planned_ship_date) <= TRUNC (CURRENT_DATE) AND NVL(v_alloc_fl,'-999') = 'Y'  THEN
    v_message                    := 'The Set ' || v_set_id || ' need to ship today.';
  END IF;
  RETURN v_message;
EXCEPTION
WHEN OTHERS THEN
  RETURN NULL;
END get_shipping_message;


/******************************************************************
   	* Description 	: Procedure to create Back-order Txn for (Consignment --> Loaner) 
   	* Author    	: Elango
   	* this procedure  is migrated from OUS
   	*******************************************************************/
PROCEDURE gm_sav_create_bcklon_item (
        p_consignid IN t504_consignment.c504_consignment_id%TYPE,
        p_type IN t504_consignment.c504_type%TYPE,
        p_shipto IN t504_consignment.c504_ship_to%TYPE,
        p_shiptoid IN t504_consignment.c504_ship_to_id%TYPE,
        p_userid IN t504_consignment.c504_last_updated_by%TYPE,
        p_message OUT VARCHAR2)
AS
    v_comments   VARCHAR2 (50) ;
    v_string     VARCHAR2 (50) ;
    v_consign_id VARCHAR2 (20) ;
    CURSOR cur_val
    IS
         SELECT     T520.C520_REQUEST_ID REQID, T521.C205_PART_NUMBER_ID PARTNUM, T521.C521_QTY QTY
              , DECODE (p_type, '4111', get_part_price (T521.C205_PART_NUMBER_ID, 'L'), get_part_price (
                T521.C205_PART_NUMBER_ID, 'E')) PRICE
               FROM t504_consignment t504, t520_request t520, t521_request_detail t521
              WHERE T504.C520_REQUEST_ID = T520.C520_MASTER_REQUEST_ID
                AND T520.C520_REQUEST_ID = T521.C520_REQUEST_ID
                AND t504.c504_consignment_id = p_consignid
                AND t520.c520_status_fl = 10
                AND T520.C520_VOID_FL IS NULL
                AND t504.c504_void_fl IS NULL;
BEGIN
    v_comments := 'Reprocess Transaction from Loaner ID:'|| p_consignid;
    FOR rad_val IN cur_val
    LOOP
        v_consign_id := NULL;
        v_string := NULL;
        v_string := rad_val.PARTNUM ||','|| rad_val.QTY ||','||''||','||rad_val.PRICE ||'|';
         SELECT get_next_consign_id ('100062') INTO v_consign_id FROM DUAL;
        gm_save_inhouse_item_consign (v_consign_id, '100062', '50123', p_shipto, p_shiptoid, v_comments, p_userid, v_string, p_message) ;
         UPDATE t412_inhouse_transactions
            SET c412_status_fl = '0', c412_ref_id = p_consignid
               ,C412_LAST_UPDATED_BY =p_userid ,C412_LAST_UPDATED_DATE=CURRENT_DATE
              WHERE c412_inhouse_trans_id = v_consign_id;
    END LOOP;
    IF v_consign_id IS NOT NULL THEN
        p_message := 'Loaner back order transaction created for the part(s) missing in this set.';
    ELSE
        p_message := '';
    END IF;
     
END gm_sav_create_bcklon_item;
/***************************************************************************************
 * Description			 : This function returns Set status Pending return or completed 
 *
 ****************************************************************************************/
FUNCTION get_request_surgery_date
(  
p_product_request_id t526_product_request_detail.C526_PRODUCT_REQUEST_DETAIL_ID%TYPE
)
RETURN DATE
IS

v_surgery_dt   DATE;
--
BEGIN
 --
 SELECT  t525.C525_SURGERY_DATE
 INTO v_surgery_dt
 FROM t526_product_request_detail t526 ,
           t525_product_request t525
    WHERE t525.c525_product_request_id  = t526.c525_product_request_id
      AND t526.C526_PRODUCT_REQUEST_DETAIL_ID = p_product_request_id
      AND t526.C526_VOID_FL  IS NULL
      AND t525.C525_VOID_FL IS NULL;

RETURN v_surgery_dt;
EXCEPTION
 WHEN OTHERS THEN
RETURN NULL;
END get_request_surgery_date;
/***************************************************************************************
 * Description			 : This function returns Loaner requested by user name
 *
 ****************************************************************************************/
FUNCTION get_loaner_requested_by
(  
p_product_request_id t526_product_request_detail.C526_PRODUCT_REQUEST_DETAIL_ID%TYPE
)
RETURN VARCHAR2
IS

v_requested_by   varchar2(220);
--
BEGIN
 --
	 SELECT  GET_USER_NAME(t525.C525_REQUEST_BY_ID)
	 INTO v_requested_by
	 FROM t526_product_request_detail t526 ,
	           t525_product_request t525
	    WHERE t525.c525_product_request_id  = t526.c525_product_request_id
	      AND t526.C526_PRODUCT_REQUEST_DETAIL_ID = p_product_request_id
	      AND t526.C526_VOID_FL  IS NULL
	      AND t525.C525_VOID_FL IS NULL;

RETURN v_requested_by;
EXCEPTION
 WHEN OTHERS THEN
RETURN NULL;
END get_loaner_requested_by;
/***************************************************************************************
 * Description			 : This function returns Inhouse Transaction surgery Date(FGLE Sugery Date)
 *
 ****************************************************************************************/
FUNCTION get_fgle_surgery_date
(  
p_product_request_id T412_INHOUSE_TRANSACTIONS.C412_INHOUSE_TRANS_ID%TYPE
)
RETURN DATE
IS
v_surgery_dt   DATE;

	BEGIN
	
	   SELECT  T525.C525_SURGERY_DATE
		 INTO v_surgery_dt
		 FROM T504A_LOANER_TRANSACTION T504A,
		      T526_PRODUCT_REQUEST_DETAIL T526,
		      T525_PRODUCT_REQUEST T525,
		      T412_INHOUSE_TRANSACTIONS t412      
	    WHERE t412.C504A_LOANER_TRANSACTION_ID   = T504A.C504A_LOANER_TRANSACTION_ID(+)
		  AND T504A.C526_PRODUCT_REQUEST_DETAIL_ID   = T526.C526_PRODUCT_REQUEST_DETAIL_ID(+)
		  AND T526.C525_PRODUCT_REQUEST_ID   = T525.C525_PRODUCT_REQUEST_ID(+)
		  AND t412.C412_INHOUSE_TRANS_ID = p_product_request_id
		  AND T504A.c504A_void_fl(+) is null
		  AND T525.c525_void_fl(+) is null
		  AND t412.c412_void_fl(+) is null
	      AND T526.c526_void_fl(+) is null;
	
	RETURN v_surgery_dt;
	EXCEPTION
	 WHEN OTHERS THEN
	RETURN NULL;
END get_fgle_surgery_date;

/************************************************************************
* Description : To get the loaner return late Fee charge based on the set
* Author      : Karthik
**************************************************************************/
FUNCTION get_loaner_late_charge_by_set
  (
    p_expected_date IN DATE,
    p_actual_date   IN DATE,
    p_reqid         IN t525_product_request.c525_product_request_id%TYPE,
    p_prod_req_dtl_id   IN   T526_Product_Request_Detail.C526_Product_Request_Detail_Id%TYPE DEFAULT NULL)
  RETURN NUMBER
AS
  v_days_late      NUMBER;
  v_least_amt_days NUMBER;
  v_least_amt      NUMBER;
  v_late_charge    NUMBER;
  v_fee_chgd_date DATE;
  v_amount    NUMBER;
  v_no_days   NUMBER;
  v_rule_days NUMBER;
  v_loaner_dt DATE;
  v_ex_rt_dt DATE;
  v_late_charge_date DATE;
BEGIN

	/*Get the Loaner date ,expected return date and the late fee charged date for the product req detaild id*/
  SELECT MAX (t504a.c504a_loaner_dt),
    MAX(t504a.c504a_expected_return_dt),
    MAX(t525.c525_l_fee_chgd_date)
  INTO v_loaner_dt,
    v_ex_rt_dt,
    v_late_charge_date
  FROM t504a_consignment_loaner t504,
    t504a_loaner_transaction t504a,
    t526_product_request_detail t526,
    t525_product_request t525
  WHERE TRUNC (t504a.c504a_expected_return_dt) < TRUNC (CURRENT_DATE)
  AND t504.c504_consignment_id                 = t504a.c504_consignment_id
  AND t504.c504a_missed_fl                    IS NULL
  AND t504a.c504a_void_fl                     IS NULL
  AND t504.c504a_void_fl                      IS NULL
  AND t526.c526_void_fl                       IS NULL
  AND t526.c526_product_request_detail_id      = t504a.c526_product_request_detail_id
  AND t525.c525_product_request_id             = t526.c525_product_request_id
  AND t526.c526_product_request_detail_id      = p_prod_req_dtl_id;	 
	 
  /*Get the delayed days based in the expected return day and the actual loan date */
  v_days_late      := get_weekdaysdiff (p_expected_date, p_actual_date) ;
  
  /*Get the Lease amount of days for the loaner set to be return based on the rule value */
  v_least_amt_days := get_rule_value ('LATEDAYS', 'LATEDAYS') ;
  
  /*Get the Least amount  for the loaner set to be return based on the rule value */
  v_least_amt      := get_rule_value ('1006425', 'CHARGES') ;
  
   /*Get the minimum number of days for the loaner charge needs to calculated */
  v_rule_days      := TO_NUMBER (get_rule_value ('DATE', 'EXPRTDT'));
  
   /* Get the work days count based on the expected return date and the loaned date */
  v_no_days        := get_work_days_count (TO_CHAR (v_loaner_dt, GET_RULE_VALUE('DATEFMT', 'DATEFORMAT')), TO_CHAR (v_ex_rt_dt, GET_RULE_VALUE('DATEFMT', 'DATEFORMAT')) );
  	 
  IF v_rule_days    > v_no_days AND v_late_charge_date IS NULL THEN
    v_days_late    := v_days_late - (v_rule_days - v_no_days);
  END IF;
    
  /* Return from the procedure if there is no delay days */
  IF v_days_late <= 0 THEN
    RETURN 0;
  END IF;

  /* Calculate the Loaner Fee charge based on the days return from the expected return date 
   */
  
    IF v_days_late   <= v_least_amt_days AND v_days_late <> 0 THEN
      v_late_charge  := v_days_late * v_least_amt ; -- $50 first 2days
    ELSIF v_days_late > v_least_amt_days THEN
      v_late_charge  := v_least_amt_days * v_least_amt ;                                                             -- $50 for first 2days
      v_late_charge  := v_late_charge    + ((v_days_late - v_least_amt_days) * get_rule_value ('92072', 'CHARGES')); -- $100 from 3rd day onwards
    ELSE
      v_late_charge := 0;
    END IF;
    
  RETURN v_late_charge;
END get_loaner_late_charge_by_set;

/******************************************************************
* Description  : Procedure to Post ISMP & IHMP Transaction While Creating
* Author       : Mohana
*******************************************************************/
PROCEDURE gm_post_inih_missing_item(
    p_id IN T412_INHOUSE_TRANSACTIONS.C412_INHOUSE_TRANS_ID%TYPE)
AS
  v_out VARCHAR2 (20);
  v_company_id T1900_COMPANY.C1900_COMPANY_ID%TYPE;
  v_plant_id T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
  v_type T412_INHOUSE_TRANSACTIONS.C412_TYPE%TYPE;
  
  
  CURSOR item_cursor
  IS
    SELECT t413.c412_inhouse_trans_id trans_id,
      t413.c205_part_number_id pnum ,
      t413.c413_item_qty qty ,
      get_part_price_list (t413.c205_part_number_id, '') price 
    FROM t413_inhouse_trans_items t413 
    WHERE 
    t413.c413_void_fl                 IS NULL
    AND t413.c412_inhouse_trans_id          = p_id
    GROUP BY t413.c205_part_number_id,
    t413.c413_item_qty,
    t413.c412_inhouse_trans_id
   HAVING t413.c413_item_qty > 0;
BEGIN
	---Selecting IHMP and ISMP Type
	SELECT C412_TYPE
	INTO v_type
	FROM T412_INHOUSE_TRANSACTIONS
	WHERE c412_inhouse_trans_id = p_id; 
	
	BEGIN
  SELECT NVL(get_compid_frm_cntx(),GET_RULE_VALUE('DEFAULT_COMPANY','ONEPORTAL')),
    NVL(get_plantid_frm_cntx(),GET_RULE_VALUE('DEFAULT_PLANT','ONEPORTAL'))
  INTO v_company_id,
    v_plant_id
  FROM DUAL;
  -- IF Company and plant id not found raising application error
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    GM_RAISE_APPLICATION_ERROR('-20999','','Company ID and Plant ID Not found');
  END;  
  
  gm_pkg_cor_client_context.gm_sav_client_context('1000','US/Eastern','MM/DD/YYYY','');
  -- Save Inhouse Trasaction items based on Type
  FOR currindex IN item_cursor
  LOOP
  
    BEGIN
      gm_pkg_op_process_recon.gm_sav_inhouse_recon( p_id,                         -- ISMP-xxxx,IHMP-xxxx
      v_type,                                                             -- ISMP,IHMP
      NULL,                                                                       -- No Need for sold String
      NULL,                                                                       -- No Need for Consign String
      (currindex.pnum||','||currindex.qty||',NOC#'||','||currindex.price || '|'), -- [Write Off String - pnum,qty,control,price|]
      NULL,                                                                       -- No Need for Return String
      '30301',                                                                    -- '30301'
      v_out                                                                       -- ''
      );
       UPDATE t412_inhouse_transactions
  SET c412_status_fl = 4 -- Completed
    ,
    c412_verified_date        = CURRENT_DATE ,
    c412_verified_by          = 30301 ,
    c412_last_updated_date    = CURRENT_DATE ,
    c412_last_updated_by      = 30301
  WHERE c412_inhouse_trans_id = p_id
  AND c412_void_fl           IS NULL;
    END;
    
  END LOOP;
 
END gm_post_inih_missing_item;
END gm_pkg_op_loaner;
/