--@"c:\database\packages\operations\gm_pkg_op_loaner_allocation.bdy";

/*
 * This package is used for allocating a loaner cn to a request
 * Flow for this package is available in
 * http://confluence.spineit.net/display/PRJ/2.+Class+Diagram+%28IST%29
 * under the section "Allocation Logic Proposed Flow"
 */

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_loaner_allocation
IS

/***************************************************************
* Description : Procedure to do the allocation for loaners
* Author    : V Prasath
****************************************************************/
   PROCEDURE gm_sav_allocate (
      p_setid      IN   t207_set_master.c207_set_id%TYPE,
      p_type       IN   t504_consignment.c504_type%TYPE,
      p_no_of_days IN   NUMBER,
      p_userid     IN   t504_consignment.c504_created_by%TYPE
   )
   AS
      v_consign_id          t504_consignment.c504_consignment_id%TYPE;
      v_req_det_id          t526_product_request_detail.c526_product_request_detail_id%TYPE;
      v_req_id              t525_product_request.c525_product_request_id%TYPE;
      v_planned_ship_date   DATE;
      v_set_id              t526_product_request_detail.c207_set_id%TYPE;
      v_status_fl           t526_product_request_detail.c526_status_fl%TYPE;
      v_req_for_id          t525_product_request.c525_request_for_id%TYPE;
      v_acc_id              t525_product_request.c704_account_id%TYPE;
      v_rep_id              t525_product_request.c703_sales_rep_id%TYPE;
      v_exp_return_dt		DATE;
      v_con_to				t504a_loaner_transaction.c901_consigned_to%TYPE;
      v_request_for			t525_product_request.c901_request_for%TYPE;
      v_assoc_rep_id		t525_product_request.c703_ass_rep_id%TYPE;
   BEGIN    
	   -- Fetch CN for allocation
      BEGIN
			 v_consign_id := get_cn_for_allocation(p_setid,p_type);
      		 --need to lock the consignment loaner table before allocating this loaner CN
	         SELECT     t504a.c504_consignment_id
	               INTO v_consign_id
	               FROM t504a_consignment_loaner t504a
	              WHERE t504a.c504_consignment_id = v_consign_id
	         FOR UPDATE;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            RETURN;
      END;
        
      -- Fetch Request for allocation
		v_req_det_id := get_request_for_allocation (p_setid,p_type,p_no_of_days);
        
		IF v_req_det_id = 0
         THEN
            RETURN;
         END IF;

       -- Fetch details from request table to make an entry in loaner transaction table
        BEGIN 
	        -- need to lock the request details id 
         SELECT     c526_status_fl, c525_product_request_id,
                    c526_required_date
               INTO v_status_fl, v_req_id,
                    v_planned_ship_date
               FROM t526_product_request_detail
              WHERE c526_product_request_detail_id = v_req_det_id
                AND c526_void_fl IS NULL
         FOR UPDATE;
        EXCEPTION WHEN NO_DATA_FOUND THEN
	        RETURN ;
        END;

		BEGIN
	      SELECT c525_request_for_id, c704_account_id, c703_sales_rep_id, c901_request_for, c703_ass_rep_id
	        INTO v_req_for_id, v_acc_id, v_rep_id, v_request_for, v_assoc_rep_id
	        FROM t525_product_request
	       WHERE c525_product_request_id = v_req_id 
	       AND c525_void_fl IS NULL;
	   EXCEPTION WHEN NO_DATA_FOUND THEN
	   		RETURN;
	   END;
	   
	   -- Calculate / fetch  the expected return date of the set
	   v_exp_return_dt := get_expected_return_date(v_req_det_id);
	   
	    -- Assign "Consigned To" based on the loaner type, 50170 dist, 50172 Inhouse employee
	   IF v_request_for = '4119' THEN
	   			BEGIN
	   	  			SELECT DECODE(t7103.c901_loan_to,19518,50170,50172) ,
	             		   DECODE(t7103.c901_loan_to,19518,get_distributor_id(t7103.c101_loan_to_id),t7103.c101_loan_to_id),
	             		    DECODE(t7103.c901_loan_to,19518,t7103.c101_loan_to_id,'')
	            	  INTO v_con_to,v_req_for_id,v_rep_id
	                  FROM t7104_case_set_information t7104
	                     , t7103_schedule_info t7103
	                     , t7100_case_information t7100
	                 WHERE T7104.c526_product_request_detail_id = v_req_det_id
	                   AND t7104.c7100_case_info_id = t7103.c7100_case_info_id 
	                   AND t7103.c7100_case_info_id = t7100.c7100_case_info_id
			   		   AND t7100.c901_case_status = 19524	-- Active
			           AND t7100.c7100_void_fl IS NULL 
			           AND t7100.c901_type = 1006504
	                   AND t7104.c7104_void_fl IS NULL;
	            EXCEPTION WHEN NO_DATA_FOUND THEN
	            	RETURN;
	            END;
        gm_pkg_allocation.gm_ins_invpick_assign_detail (93603,v_consign_id,p_userid); ---IH loaner Set (INVPT).-- Insert record in t5050 for inventory device
        ELSE
        		v_con_to := '50170';
        END IF;            
	   

	   -- make an entry in loaner transaction table with the values fetched above
      gm_pkg_op_loaner.gm_sav_loaner_transaction (v_consign_id,
                                                  v_planned_ship_date,
                                                  v_exp_return_dt,
                                                  v_con_to,                                                        
                                                  v_req_for_id,
                                                  p_userid,
                                                  v_rep_id,
                                                  v_acc_id,
                                                  v_req_det_id,
                                                  v_assoc_rep_id
                                                 );

       -- Update the status for loaner set and request                                          	
       gm_sav_alloc_status (v_consign_id, v_req_det_id, p_userid);   
        
   END gm_sav_allocate;

   /***************************************************************************
	* Description : Function to calculate or fetch the expected 
	*               return date based on the type of the loaner
	* Author    : V Prasath
	***************************************************************************/
   
   FUNCTION get_expected_return_date (
  	  p_req_det_id  IN   t526_product_request_detail.c526_product_request_detail_id%TYPE
   ) RETURN DATE
   IS
	   v_exp_return_dt t526_product_request_detail.c526_required_date %TYPE;
	   v_planned_ship_date t526_product_request_detail.c526_required_date %TYPE;
	   v_type t504_consignment.c504_type%TYPE;
   BEGIN
	   
	    	 SELECT c526_required_date, c901_request_type, c526_exp_return_date
               INTO v_planned_ship_date, v_type, v_exp_return_dt -- default the expected return date for in house loaner
               FROM t526_product_request_detail
              WHERE c526_product_request_detail_id = p_req_det_id
                AND c526_void_fl IS NULL;
                
	   -- if type is Product Loaner calculate the expected return date from the planned ship date
	   IF v_type = '4127' THEN 
	   		v_exp_return_dt := gm_pkg_op_loaner.get_loaner_expected_return_dt(v_planned_ship_date) ;	 
	   END IF;
	   
	   RETURN v_exp_return_dt;	   
	   
   END get_expected_return_date;
   
   /***************************************************************************
	* Description : Function to fetch the CN that can be allocated to a request
	* Author    : V Prasath
	***************************************************************************/
   FUNCTION get_cn_for_allocation (
      p_setid      IN   t207_set_master.c207_set_id%TYPE,
      p_type       IN   t504_consignment.c504_type%TYPE
   )
    RETURN VARCHAR2
   IS
   v_consign_id	t504_consignment.c504_consignment_id%TYPE;
   v_status	NUMBER;
   v_date_fmt VARCHAR2(20);
   v_default_rt_dt VARCHAR2(20);
   v_plant_id T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
   BEGIN
   
   /* Business Logic to get a CN from the available pool
	* ----------------------------------------------------
	* 1. Set need to be in available status or pending put away(Pending FG) status
	* 2. If more than one set in above mentioned status, return the one with old returned date
	* 3. If the set is new and it does not have a returned date, 
 	*    ignore the available sets with returned date and return this CN 
 	* 4. If there are more than one sets that are new, and does not have returned date
	*    ignore the available sets with returned date and return a CN with 
 	*    old created date with in the new sets
	*/
   	   	
	   BEGIN
	   v_date_fmt := NVL(get_rule_value('DATEFMT','DATEFORMAT'),'MM/DD/YYYY');
	   v_default_rt_dt := NVL(get_rule_value('DEFAULTDT','LOANER'),'01/01/2004');
	   
	   select get_plantid_frm_cntx(),get_compdtfmt_frm_cntx() into v_plant_id,v_date_fmt from dual;
	   
	    SELECT c504_consignment_id INTO v_consign_id
		FROM
			(SELECT t504a.c504_consignment_id
			      , MAX(NVL(DECODE(t504b.c504_consignment_id,NULL,CURRENT_DATE,t504b.c504a_return_dt), TO_DATE (v_default_rt_dt,v_date_fmt))) Return_date
			      , MIN(t504.c504_created_date) CREATED_DATE
			FROM t504a_consignment_loaner t504a
			    , t504_consignment t504
			    , t504a_loaner_transaction t504b
			WHERE t504.c504_consignment_id = t504a.c504_consignment_id
			AND t504.c504_void_fl is NULL
			AND t504a.c504a_status_fl IN ( 0, 58 ) -- Available, Pending FG
			AND t504.c207_set_id =  p_setid
			AND t504.c504_type = p_type
			AND t504a.c504_consignment_id = t504b.c504_consignment_id(+) 
			AND t504b.c504a_void_fl(+) is NULL
			AND t504a.c504a_allocated_fl IS NULL  -- ( Pending FG - not allocated )
			AND t504a.c504a_void_fl IS NULL
			AND t504a.c5040_plant_id =v_plant_id
			GROUP BY t504a.c504_consignment_id
			ORDER BY RETURN_DATE, CREATED_DATE ASC)
		WHERE ROWNUM = 1;

      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            v_consign_id := NULL;
      END;
		   RETURN v_consign_id;
   END get_cn_for_allocation;
   
    /***************************************************************************
	* Description : Function to fetch the Request that can 
	*               be allocated to a Consignment
	* Author    : V Prasath
	***************************************************************************/
   FUNCTION get_request_for_allocation (
      p_setid       IN   t207_set_master.c207_set_id%TYPE,
      p_type        IN   t504_consignment.c504_type%TYPE,
      p_alloc_days	IN	NUMBER
   )
    RETURN VARCHAR2
   IS
     v_req_det_id t526_product_request_detail.c526_product_request_detail_id%TYPE;     
     v_plant_id T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
     v_date_fmt VARCHAR2(100);
     
   BEGIN
	   select get_plantid_frm_cntx()into v_plant_id from dual;
	   v_date_fmt := NVL(get_rule_value('DATEFMT','DATEFORMAT'),'MM/dd/yyyy');
	   
	   SELECT NVL (MIN (c526_product_request_detail_id), 0)
           INTO v_req_det_id
           FROM t526_product_request_detail
          WHERE c207_set_id = p_setid
            AND c526_void_fl IS NULL
            AND c526_status_fl = 10 --open
            AND c901_request_type = p_type
            AND c5040_plant_id =v_plant_id
            AND (get_work_days_count(TO_CHAR(CURRENT_DATE,v_date_fmt),TO_CHAR(c526_required_date,v_date_fmt))<= p_alloc_days 
            AND TRUNC(c526_required_date - (CURRENT_DATE))>=0);
            
            RETURN v_req_det_id;
            
   END get_request_for_allocation;
   
    /***************************************************************************
	* Description : Procedure to update the status in T504, T504A, T526 tables
	*               after the allocation of a CN to a Request
	* Author    : V Prasath
	***************************************************************************/
   PROCEDURE gm_sav_alloc_status (
      p_cn_id       IN   t504_consignment.c504_consignment_id%TYPE,
      p_req_det_id  IN   t526_product_request_detail.c526_product_request_detail_id%TYPE,
      p_userid     IN   t504_consignment.c504_created_by%TYPE
   )
   AS
   v_planned_ship_date	t526_product_request_detail.c526_required_date%TYPE;
   v_req_id	t525_product_request.c525_product_request_id%TYPE;
   v_req_for_id t525_product_request.c525_request_for_id%TYPE;
   v_acc_id t525_product_request.c704_account_id%TYPE;
   v_status NUMBER;
   v_type t526_product_request_detail.c901_request_type%TYPE;
   v_consigned_to NUMBER;
   v_con_id	t504_consignment.c504_consignment_id%TYPE;
   BEGIN
	 
	   BEGIN
		   SELECT  t525.c525_product_request_id, t526.c526_required_date
		         , t525.c525_request_for_id, t525.c704_account_id, t525.c901_request_for 
	         INTO v_req_id, v_planned_ship_date, v_req_for_id, v_acc_id, v_type
	         FROM t526_product_request_detail t526, t525_product_request t525
	        WHERE t526.c526_product_request_detail_id = p_req_det_id
	          AND t525.c525_product_request_id = t526.c525_product_request_id
	          AND t526.c526_void_fl IS NULL
	          AND t525.c525_void_fl IS NULL
	          FOR UPDATE;
         EXCEPTION WHEN NO_DATA_FOUND THEN
	           v_req_id := NULL;
	           v_planned_ship_date := NULL;
	           v_req_for_id := NULL;
	           v_acc_id := NULL;
         END;
       --need to lock the record for update
       BEGIN
         SELECT t504.c504_consignment_id
        	 INTO v_con_id
         FROM t504a_consignment_loaner t504a, t504_consignment t504
         WHERE t504a.c504_consignment_id = t504.c504_consignment_id
	         AND t504.c504_consignment_id = p_cn_id 
	         AND t504a.c504a_void_fl IS NULL 
	         AND t504.c504_void_fl IS NULL
	         FOR UPDATE;
	     EXCEPTION WHEN NO_DATA_FOUND THEN
	     	v_con_id:= NULL;
	     END;
         
        v_status := get_loaner_next_status(p_cn_id, p_req_det_id);
        
        IF v_status IS NULL THEN
        	RETURN;
        END IF;
         
        gm_pkg_op_loaner_allocation.gm_loaner_allocation_update(p_req_det_id, 'ADD', p_userid); -- reset allocated fl and planned ship date in t504a
        -- Call the common procedure to update the status
        gm_pkg_op_loaner_set_txn.gm_upd_consign_loaner_status(p_cn_id,v_status,p_userid);
         UPDATE t504a_consignment_loaner
            SET c504a_loaner_dt = v_planned_ship_date,                                         
                c504a_last_updated_date = CURRENT_DATE,
                c504a_last_updated_by = p_userid
          WHERE c504_consignment_id = p_cn_id 
            AND c504a_void_fl IS NULL;
		--Changes added for PMT-12455 and it's used to trigger an update to iPad
--        gm_pkg_op_loaner_set_txn.GM_SAV_AVAIL_LN_SET_UPDATE (p_cn_id, p_userid); -- consignment_id, user_id, company_id    

		--20: allocated     
		gm_pkg_op_loaner_allocation.gm_upd_request_status(p_req_det_id, 20, p_userid);
         
      	 BEGIN
			   SELECT DECODE(c901_consigned_to,50170,4120,50172,4123)
			   		, c504a_consigned_to_id
			     INTO v_consigned_to
			     	, v_req_for_id
  				 FROM t504a_loaner_transaction t504a  
			    WHERE T504a.c504_consignment_id = p_cn_id 
   				  AND t504a.c504a_return_dt IS NULL
   				  AND t504a.c504a_void_fl IS NULL;	   				  
		   EXCEPTION WHEN NO_DATA_FOUND THEN
		   		v_consigned_to := NULL;
		   END;

      --dist id needs to be updated when the product loaner (4127) request moves to allocated or pending shipping
      IF v_type = 4127 THEN
	      UPDATE t504_consignment
	         SET c701_distributor_id = DECODE(v_consigned_to, 4120, v_req_for_id, NULL),
	             c704_account_id = DECODE(v_consigned_to, 4120, v_acc_id, NULL),
	             c504_ship_to = v_consigned_to,
		         c504_ship_to_id = v_req_for_id,
		         c504_last_updated_by = p_userid,
				 c504_last_updated_date = CURRENT_DATE
	       WHERE c504_consignment_id = p_cn_id 
	         AND c504_void_fl IS NULL;
	       
			   -- Make an entry in t5050 for product loaners for the hand held device to pick (7 )/ put (58) 
		      IF v_status = 58  
		       THEN
		       		gm_pkg_allocation.gm_ins_invpick_assign_detail(93604, p_cn_id, p_userid); -- 93604 - Pending FG
		       END IF;
		      IF  v_status = 7 
		       THEN
		       		gm_pkg_allocation.gm_ins_invpick_assign_detail(93602, p_cn_id, p_userid); -- 93602 - Loaner Set (Pending PIck)
		       END IF;
	  ELSIF v_type = 4119 THEN
	  		IF v_consigned_to IS NOT NULL THEN
		       UPDATE t504_consignment
			      SET c701_distributor_id = DECODE(v_consigned_to, 4120, v_req_for_id, NULL)
			        , c704_account_id = DECODE(v_consigned_to, 4120, v_acc_id, NULL)
		            , c504_ship_to = v_consigned_to
		            , c504_ship_to_id = v_req_for_id
		            , c504_last_updated_by = p_userid
				    , c504_last_updated_date = CURRENT_DATE
			    WHERE c504_consignment_id = p_cn_id 
			      AND c504_void_fl IS NULL;
			END IF;
	  END IF;
	  
	  -- Update the consignment loaner table for the loaner status and expected return date 
	  	gm_pkg_op_loaner.gm_sav_status_update(p_req_det_id, p_userid);
	 
   END 	gm_sav_alloc_status;   
 
   /***************************************************************************
	* Description : Functin to fetch the next status of a given loaner set
	* Author    : V Prasath
	***************************************************************************/
   FUNCTION get_loaner_next_status (
    p_cn_id       IN   t504_consignment.c504_consignment_id%TYPE,
    p_req_det_id  IN   t526_product_request_detail.c526_product_request_detail_id%TYPE
    )
    RETURN NUMBER
    IS
    v_cur_status_fl NUMBER;
    v_nxt_status_fl NUMBER;
    v_planned_ship_date t526_product_request_detail.c526_required_date%TYPE;
    BEGIN
	   BEGIN 
   		 SELECT  c526_required_date
           INTO v_planned_ship_date
           FROM t526_product_request_detail
          WHERE c526_product_request_detail_id = p_req_det_id
            AND c526_void_fl IS NULL;
       EXCEPTION WHEN NO_DATA_FOUND THEN
       		v_planned_ship_date := NULL;
       END;
       
       BEGIN
            SELECT c504a_status_fl 
              INTO v_cur_status_fl  
              FROM t504a_consignment_loaner T504a 
             where t504a.c504_consignment_id = p_cn_id;
       EXCEPTION WHEN NO_DATA_FOUND THEN
       		v_cur_status_fl := NULL;
       END;
      
       IF  v_cur_status_fl = 58 THEN --  PENDING Putaway
       		v_nxt_status_fl := 58 ; -- PENDING Putaway		
       ELSIF v_cur_status_fl = 0 THEN -- AVAILABLE , product loaner , once allocated, status should be  pending pick
            v_nxt_status_fl := 7 ; -- PENDING PICK
       ELSE
       		v_nxt_status_fl := NULL;
       END IF;
       
   /*   IF v_cur_status_fl = 0 THEN
      	 	v_nxt_status_fl := 5; -- ALLOCATED
        IF  v_planned_ship_date = TRUNC(CURRENT_DATE) THEN -- AVAILABLE AND PLANNED SHIP DATE = CURRENT DATE 		
            v_nxt_status_fl := 10 ; -- PENDING SHIPPING	 
        END IF;
      END IF; 	*/	
       
       RETURN v_nxt_status_fl;
       
	END get_loaner_next_status;
	
	/******************************************************************
	* Description : Procedure update allocated fl and planned ship date 
	* in the t504a_consignment_loaner
	* ****************************************************************/
	PROCEDURE gm_loaner_allocation_update (
	      p_req_det_id   IN   t526_product_request_detail.c526_product_request_detail_id%TYPE,
	      p_operation    IN   VARCHAR2,
	      p_userid       IN   t504_consignment.c504_created_by%TYPE
	   )
	   AS
	      v_required_date   DATE;
	      v_consign_id      t504_consignment.c504_consignment_id%TYPE; 
	   BEGIN
	      BEGIN
	         SELECT t526.c526_required_date, t504a.c504_consignment_id
	           INTO v_required_date, v_consign_id
	           FROM t526_product_request_detail t526,
	                t504a_loaner_transaction t504a
	          WHERE t526.c526_product_request_detail_id = p_req_det_id
	            AND t504a.c526_product_request_detail_id = t526.c526_product_request_detail_id
	            AND t504a.c504a_void_fl IS NULL
	            AND t526.c526_void_fl IS NULL;
	      EXCEPTION
	         WHEN NO_DATA_FOUND
	         THEN
	            RETURN;
	      END;
	
	       IF p_operation = 'ADD'
	       THEN
	                  UPDATE t504a_consignment_loaner 
	                  SET c504a_allocated_fl = 'Y'
	                        , c504a_planned_ship_date = v_required_date
	                        , c504a_last_updated_by = p_userid
	                        , c504a_last_updated_date = CURRENT_DATE
	                  WHERE C504_CONSIGNMENT_ID = v_consign_id 
	                    AND c504a_void_fl IS NULL;
	             -- Call the rollback procedure to remove the priority and reprioritize other sets in request       
	             gm_pkg_op_ln_priority_engine.gm_rollback_cn_priority(null,p_userid,v_consign_id);
	  	 
	       END IF;
	       
	       IF p_operation = 'CLEAR'
	       THEN
	                  UPDATE t504a_consignment_loaner 
	                  SET c504a_allocated_fl = NULL
	                        , c504a_planned_ship_date = NULL
	                        , c504a_last_updated_by = p_userid
	                        , c504a_last_updated_date = CURRENT_DATE
	                  WHERE C504_CONSIGNMENT_ID = v_consign_id 
	                    AND c504a_void_fl IS NULL;
	                    
	                  -- Call the priority engine to prioritize the set
	                  gm_pkg_op_ln_priority_engine.gm_sav_main_ln_set_priority(v_consign_id,p_userid);
	       END IF;      
	       
	END gm_loaner_allocation_update;

   
	/******************************************************************************************************
	* Description : Procedure to load the setlist dropdown without Demo Sets by adding the Demo sets type.
	* Author	   : Karthik
	*******************************************************************************************************/
	
	PROCEDURE gm_fch_loaner_req_active_sets (
	        p_sets OUT TYPES.cursor_type)
	AS
	v_plant_id T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
	BEGIN
		SELECT get_plantid_frm_cntx() 
			INTO v_plant_id 
				FROM dual;
				
	    OPEN p_sets FOR
	    SELECT DISTINCT (t504.c207_set_id) SETID, 
	    		t504.c207_set_id ||' / '|| C207_SET_NM SNAME
	          , t504.c207_set_id ID
	          , t504.c207_set_id ||' / '|| C207_SET_NM IDNAME
	          , C207_SET_NM NAME
	           FROM t504_consignment t504,
	           t504a_consignment_loaner t504a ,
	           T207_SET_MASTER t207,
	           T2080_SET_COMPANY_MAPPING t2080
	          WHERE t504.c504_consignment_id = t504a.c504_consignment_id
	            AND t207.c207_set_id = t504.c207_set_id
	            AND T504.C504_VOID_FL IS NULL
	            AND t504.c504_type = 4127 -- only product loaners
	            AND t504.c504_status_fl = '4'
	            AND t504a.C504A_STATUS_FL != '60'
	            AND t504a.c504a_void_fl IS NULL
	            AND t207.C207_VOID_FL IS NULL
	            AND T2080.C207_SET_ID = T207.C207_SET_ID
	            AND t2080.c2080_void_fl IS NULL
	            AND T2080.C1900_COMPANY_ID IN (SELECT C1900_COMPANY_ID
	            							   FROM T5041_PLANT_COMPANY_MAPPING WHERE C5041_PRIMARY_FL = 'Y' AND C5040_PLANT_ID= V_PLANT_ID
	            							   AND C5041_VOID_FL IS NULL)
	            AND t504a.C5040_PLANT_ID = v_plant_id 
	            AND t207.C207_TYPE != 4078 --Demo Sets
	       ORDER BY t504.c207_set_id;
	END gm_fch_loaner_req_active_sets;

	/***********************************************************
	* Description : Procedure to update the status of request
	* Author	  : Arajan
	************************************************************/
	PROCEDURE gm_upd_request_status(
		p_prod_req_det_id		IN		t526_product_request_detail.c526_product_request_detail_id%TYPE,
		p_status_fl				IN		t526_product_request_detail.c526_status_fl%TYPE,
		p_user_id				IN		t526_product_request_detail.c526_last_updated_by%TYPE
	)
	AS
		v_prod_req_id	T526_Product_Request_Detail.C525_Product_Request_Id%TYPE;
	BEGIN
		UPDATE t526_product_request_detail
		   SET c526_status_fl = p_status_fl
			 , c526_last_updated_by = p_user_id
			 , c526_last_updated_date = CURRENT_DATE
		 WHERE c526_product_request_detail_id = p_prod_req_det_id 
		   AND c526_void_fl IS NULL;
		   
		-- Get the parent request id to pass to engine
		BEGIN
			SELECT C525_Product_Request_Id
			INTO v_prod_req_id
			FROM T526_Product_Request_Detail
			WHERE C526_Product_Request_Detail_Id = p_prod_req_det_id
			AND c526_void_fl                    IS NULL;
		EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN;
		END;
		--Call priority engine to prioritize the set while changing the request status
		gm_pkg_op_ln_priority_engine.gm_sav_main_ln_set_priority(v_prod_req_id, p_user_id);
		
	END gm_upd_request_status;
	
END gm_pkg_op_loaner_allocation;
/