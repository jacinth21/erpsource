/* Formatted on 2009/11/25 13:32 (Formatter Plus v4.8.0) */
-- @"C:\Database\Packages\Operations\Gm_pkg_op_ttp.bdy"

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_ttp
IS
--
	PROCEDURE gm_fc_fch_ttp_demandsheetmap (
		p_ttpid 		  IN	   t4050_ttp.c4050_ttp_id%TYPE
	  , p_outunselected   OUT	   TYPES.cursor_type
	  , p_outselected	  OUT	   TYPES.cursor_type
	)
	AS
	/*******************************************************
	  * Description : Procedure to fetch ttp group mapping information
	  * Author				: Joe P Kumar
	  *******************************************************/
	BEGIN
		OPEN p_outunselected
		 FOR
			 SELECT   t4020.c4020_demand_master_id ID, t4020.c4020_demand_nm NAME
				 FROM t4020_demand_master t4020
				WHERE t4020.c4020_demand_master_id NOT IN (SELECT NVL (t4051.c4020_demand_master_id, 1)
															 FROM t4051_ttp_demand_mapping t4051)
				  AND t4020.c4020_void_fl IS NULL
			 ORDER BY t4020.c4020_demand_nm;

		OPEN p_outselected
		 FOR
			 SELECT   t4020.c4020_demand_master_id ID, t4020.c4020_demand_nm NAME
				 FROM t4051_ttp_demand_mapping t4051, t4020_demand_master t4020
				WHERE t4051.c4020_demand_master_id = t4020.c4020_demand_master_id(+) AND t4051.c4050_ttp_id = p_ttpid
					  AND t4020.c4020_void_fl IS NULL
			 ORDER BY t4020.c4020_demand_nm;
	END gm_fc_fch_ttp_demandsheetmap;

--
	PROCEDURE gm_fc_sav_ttpdsmap (
		p_ttpid 	 IN 	  t4050_ttp.c4050_ttp_id%TYPE
	  , p_ttpname	 IN 	  t4050_ttp.c4050_ttp_nm%TYPE
	  , p_primuser	 IN 	  t4050_ttp.c4050_primary_user%TYPE
	  , p_userid	 IN 	  t4050_ttp.c4050_created_by%TYPE
	  , p_inputstr	 IN 	  VARCHAR2
	  , p_outttpid	 OUT	  t4050_ttp.c4050_ttp_id%TYPE
	)
	AS
	/*******************************************************
	 * Description : Procedure to save		  / update the demand sheets associated to a TTP
	 * Author		  : Joe P Kumar
	  *******************************************************/
--
		v_ttpid 	   t4050_ttp.c4050_ttp_id%TYPE;
	BEGIN
		-- The input String in the form of 5,8 will be set into this temp view.
		my_context.set_my_inlist_ctx (p_inputstr);
		p_outttpid	:= p_ttpid;

		UPDATE t4050_ttp
		   SET c4050_ttp_nm = p_ttpname
			 , c4050_primary_user = p_primuser
			 , c4050_last_updated_by = p_userid
			 , c4050_last_updated_date = SYSDATE
		 WHERE c4050_ttp_id = p_ttpid;

		IF (SQL%ROWCOUNT = 0)
		THEN
			SELECT s4050_ttp.NEXTVAL
			  INTO p_outttpid
			  FROM DUAL;

			INSERT INTO t4050_ttp
						(c4050_ttp_id, c4050_ttp_nm, c4050_primary_user, c4050_created_by, c4050_created_date
						)
				 VALUES (p_outttpid, p_ttpname, p_primuser, p_userid, SYSDATE
						);
		END IF;

		DELETE FROM t4051_ttp_demand_mapping t4051
			  WHERE t4051.c4050_ttp_id = p_outttpid;

		INSERT INTO t4051_ttp_demand_mapping t4051
					(t4051.c4051_po_ttp_map_id, c4050_ttp_id, c4020_demand_master_id)
			SELECT s4051_ttp_demand_mapping.NEXTVAL, p_outttpid, tmptable.refid
			  FROM (SELECT token refid
					  FROM v_in_list) tmptable;
	END gm_fc_sav_ttpdsmap;

--
	PROCEDURE gm_fc_fch_ttp_report (
		p_report   OUT	 TYPES.cursor_type
	)
	 /*******************************************************
	* Description : Procedure to fetch ttp report
	* Author		: Joe P Kumar
	*******************************************************/
	AS
	BEGIN
		OPEN p_report
		 FOR
			 SELECT   t4050.c4050_ttp_id ttpid, t4050.c4050_ttp_nm ttpnm, t4020.c4020_demand_nm dmname
					, get_code_name (t4021.c901_ref_type) maptype
					, DECODE (t4021.c901_ref_type
							, 40030, get_group_name (t4021.c4021_ref_id)
							, 40031, get_set_name (t4021.c4021_ref_id)
							, 40032, get_code_name (t4021.c4021_ref_id)
							, 40033, get_code_name (t4021.c4021_ref_id)
							 ) mapdata
					, get_code_name (c901_action_type) stackfl
				 FROM t4050_ttp t4050
					, t4051_ttp_demand_mapping t4051
					, t4020_demand_master t4020
					, t4021_demand_mapping t4021
				WHERE t4050.c4050_ttp_id = t4051.c4050_ttp_id
				  AND t4020.c4020_demand_master_id = t4021.c4020_demand_master_id
				  AND t4051.c4020_demand_master_id = t4020.c4020_demand_master_id
				  AND t4050.c4050_void_fl IS NULL
				  AND t4020.c4020_void_fl IS NULL
			 GROUP BY t4050.c4050_ttp_id
					, t4050.c4050_ttp_nm
					, t4020.c4020_demand_nm
					, t4021.c901_ref_type
					, t4021.c4021_ref_id
					, c901_action_type
			 ORDER BY ttpnm, dmname, maptype;
	END gm_fc_fch_ttp_report;

--
	PROCEDURE gm_fc_fch_monthsheet_report (
		p_sheetnmregex	 IN 	  VARCHAR2
	  , p_status		 IN 	  t4040_demand_sheet.c901_status%TYPE
	  , p_sheettype 	 IN 	  t4020_demand_master.c901_demand_type%TYPE
	  , p_month 		 IN 	  VARCHAR2
	  , p_year			 IN 	  VARCHAR2
	  , p_ttpid 		 IN 	  t4050_ttp.c4050_ttp_id%TYPE
	  , p_primary_user	 IN 	  t4040_demand_sheet.c4040_primary_user%TYPE
	  , p_report		 OUT	  TYPES.cursor_type
	)
	 /*******************************************************
	* Description : Procedure to fetch monthly sheet report
	* Author		: D James
	*******************************************************/
	AS
	BEGIN
		OPEN p_report
		 FOR
			 SELECT   t4020.c4020_demand_master_id dmid, t4020.c4020_demand_nm demnm
					, get_code_name (t4020.c901_demand_type) dtype, get_user_name (t4040.c4040_primary_user) cuser
					, TO_CHAR (c4040_load_dt, get_rule_value('DATEFMT','DATEFORMAT')) ldate, c4040_demand_period dper
					, get_code_name (c901_status) dstat, c4040_forecast_period fper
					, get_user_name (c4040_last_updated_by) luser, t4040.c4040_demand_sheet_id dmsheetid
					, TO_CHAR (c4040_load_dt, 'MM') ldtmonth, TO_CHAR (c4040_load_dt, 'YYYY') ldtyear
					, TO_CHAR (t4040.c4040_demand_period_dt, get_rule_value('DATEFMT','DATEFORMAT')) perioddate, TO_CHAR (c901_status) statusid
				 FROM t4040_demand_sheet t4040, t4020_demand_master t4020
				WHERE t4040.c4020_demand_master_id = t4020.c4020_demand_master_id
				  AND t4040.c4040_void_fl IS NULL
				  AND t4020.c4020_void_fl IS NULL
				  AND t4020.c4020_demand_nm LIKE DECODE (p_sheetnmregex, '', t4020.c4020_demand_nm, p_sheetnmregex)
				  AND c901_status = DECODE (p_status, '0', c901_status, p_status)
				  AND t4020.c901_demand_type = DECODE (p_sheettype, '0', t4020.c901_demand_type, p_sheettype)
				  AND t4040.c4040_demand_period_dt =
						  DECODE (p_month
								, '0', t4040.c4040_demand_period_dt
								, TO_DATE (p_month || '/' || p_year, 'MM/YYYY')
								 )
				  AND NVL (c4052_ttp_detail_id, 1) = DECODE (p_ttpid, '0', NVL (c4052_ttp_detail_id, 1), p_ttpid)
				  AND t4040.c4040_primary_user = DECODE (p_primary_user, '0', t4040.c4040_primary_user, p_primary_user)
			 ORDER BY demnm;
	END gm_fc_fch_monthsheet_report;

--
	/*******************************************************
   * Description : Procedure to fetch demand sheet for that TTP
   * Author 	 : Joe P Kumar
   *******************************************************/
--
	PROCEDURE gm_fc_fch_ttp_dsmonthly (
		p_ttpid 	IN		 t4050_ttp.c4050_ttp_id%TYPE
	  , p_monyear	IN		 VARCHAR2
	  , p_output	OUT 	 TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_output
		 FOR
			 SELECT   t4040.c4040_demand_sheet_id ID
					,	 NVL (t4040.c4040_demand_sheet_id, 'No Id')
					  || ' : '
					  || gm_pkg_op_sheet.get_demandsheet_name (t4051.c4020_demand_master_id)
					  || ' - '
					  || DECODE (t4040.c901_status, NULL, 'Not Loaded', get_code_name (t4040.c901_status)) NAME
					, DECODE (t4040.c901_status, 50550, 'false', 50551, 'false', 'true') status
					, t4040.c901_status statusid
				 FROM t4040_demand_sheet t4040, t4051_ttp_demand_mapping t4051
				WHERE t4051.c4020_demand_master_id = t4040.c4020_demand_master_id(+)
				  AND t4051.c4050_ttp_id = p_ttpid
				  AND TO_CHAR (t4040.c4040_load_dt, 'MM/YYYY') = p_monyear
				  AND t4040.c4040_void_fl IS NULL
			 UNION
			 SELECT   t4040.c4040_demand_sheet_id ID
					,	 NVL (t4040.c4040_demand_sheet_id, 'No Id')
					  || ' : '
					  || gm_pkg_op_sheet.get_demandsheet_name (t4040.c4020_demand_master_id)
					  || ' - '
					  || DECODE (t4040.c901_status, NULL, 'Not Loaded', get_code_name (t4040.c901_status)) NAME
					, DECODE (t4040.c901_status, 50551, 'false', 'true') status, t4040.c901_status statusid
				 FROM t4040_demand_sheet t4040
				WHERE t4040.c4052_ttp_detail_id IN (SELECT c4052_ttp_detail_id
													  FROM t4052_ttp_detail
													 WHERE c4050_ttp_id = p_ttpid)
				  AND TO_CHAR (t4040.c4040_load_dt, 'MM/YYYY') = p_monyear
				  AND t4040.c4040_void_fl IS NULL
			 ORDER BY NAME;
	END gm_fc_fch_ttp_dsmonthly;

--
	/*******************************************************
   * Description : Procedure to fetch all approved monthly demand sheet
   * Author 	 : Joe P Kumar
   *******************************************************/
--
	PROCEDURE gm_fc_fch_ttp_approvedds (
		p_ttpid 	IN		 t4050_ttp.c4050_ttp_id%TYPE
	  , p_monyear	IN		 VARCHAR2
	  , p_output	OUT 	 TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_output
		 FOR
			 SELECT   t4040.c4040_demand_sheet_id ID
					, gm_pkg_op_sheet.get_demandsheet_name (t4040.c4020_demand_master_id) NAME
				 FROM t4040_demand_sheet t4040
				WHERE t4040.c901_status = 50551   -- approved status
				  AND t4040.c4020_demand_master_id NOT IN (SELECT t4051.c4020_demand_master_id
															 FROM t4051_ttp_demand_mapping t4051
															WHERE t4051.c4050_ttp_id = p_ttpid)
				  AND TO_CHAR (t4040.c4040_load_dt, 'MM/YYYY') = p_monyear
				  AND t4040.c4040_void_fl IS NULL
				  AND t4040.c4040_demand_sheet_id NOT IN (
						  SELECT t4040.c4040_demand_sheet_id ID
							FROM t4040_demand_sheet t4040
						   WHERE t4040.c4052_ttp_detail_id IN (SELECT c4052_ttp_detail_id
																 FROM t4052_ttp_detail
																WHERE c4050_ttp_id = p_ttpid)
							 AND TO_CHAR (t4040.c4040_load_dt, 'MM/YYYY') = p_monyear
							 AND t4040.c4040_void_fl IS NULL)
			 ORDER BY NAME;
	END gm_fc_fch_ttp_approvedds;

	--
	/*******************************************************
   * Description : Procedure to fetch monthly sheet report
   * Author 	 : Joe P Kumar
   *******************************************************/
--
	PROCEDURE gm_fc_fch_inventoryidlist (
		p_lock_type 		IN		 t250_inventory_lock.c901_lock_type%TYPE
	  , p_inventoryidlist	OUT 	 TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_inventoryidlist
		 FOR
			 SELECT   t250.c250_inventory_lock_id ID, TO_CHAR (t250.c250_lock_date, get_rule_value('DATEFMT','DATEFORMAT')) NAME
					, TRUNC (t250.c250_lock_date) dt
				 FROM t250_inventory_lock t250
				WHERE t250.c250_void_fl IS NULL AND t250.c901_lock_type = p_lock_type	--main inventory lock  20430
			 ORDER BY dt DESC;
	END gm_fc_fch_inventoryidlist;

	/*******************************************************
   * Description : Procedure to fetch TTP List
   * Author 	 : Joe P Kumar
   *******************************************************/
--
	PROCEDURE gm_fc_fch_ttplist (
		p_output   OUT	 TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_output
		 FOR
			 SELECT   c4050_ttp_id codeid, c4050_ttp_nm codenm
				 FROM t4050_ttp
				WHERE c4050_void_fl IS NULL
			 ORDER BY c4050_ttp_nm;
	END gm_fc_fch_ttplist;

		--
	/*******************************************************
   * Description : Procedure to save monthly TTP grouping
   * Author 	 : Joe P Kumar
   Algorithm:
	0. Check if the finalizing is already done for that month. If so, throw an exception
	1. Create a new entry onto t4052_ttp_detail
	2. Update the v_ttp_detail_id created into t4040_demand_sheet for all the selected demand sheets
	3. Load part, qty, historyfl information into t4053_ttp_part_detail for the selected demand sheets from t4042_demand_sheet_detail
	4. Insert into t252_inventory_lock_ref with v_ttp_detail_id and p_inventorydate
   *******************************************************/
--
	PROCEDURE gm_fc_sav_ttp_dsmonthly (
		p_ttpid 		   IN		t4050_ttp.c4050_ttp_id%TYPE
	  , p_demandsheetids   IN		VARCHAR2
	  , p_forecastmonths   IN		VARCHAR2
	  , p_inventoryid	   IN		t252_inventory_lock_ref.c250_inventory_lock_id%TYPE
	  , p_userid		   IN		t4052_ttp_detail.c4052_created_by%TYPE
	  , p_string		   IN		VARCHAR2
	  , p_cnt			   IN		VARCHAR2
	  , p_ttp_dtl_id	   IN OUT	t4052_ttp_detail.c4052_ttp_detail_id%TYPE
	)
	AS
		v_ttp_detail_id t4052_ttp_detail.c4052_ttp_detail_id%TYPE;
		v_fc_start_date DATE;
		v_cnt_link_date NUMBER;
		v_string	   VARCHAR2 (4000) := p_string;
		v_substring    VARCHAR2 (4000);
		v_partnumber   VARCHAR2 (20);
		v_qty		   VARCHAR2 (10);
		v_id		   NUMBER;
		v_price 	   NUMBER;
	BEGIN
		-- The input String in the form of 5,8 will be set into this temp view.
		my_context.set_my_inlist_ctx (p_demandsheetids);

		--
		/*
			In Java code if string is more then 4000 chars then this proc will be called more then once.
			So for 2nd time it should not call the update and insert. so the following code is written in if.
		*/
		--
		IF p_ttp_dtl_id IS NULL
		THEN
			SELECT	   t4050.c4050_ttp_id
				  INTO v_id
				  FROM t4050_ttp t4050
				 WHERE t4050.c4050_ttp_id = p_ttpid
			FOR UPDATE;

			SELECT s4052_ttp_detail.NEXTVAL
			  INTO v_ttp_detail_id
			  FROM DUAL;

			-- 1. Create a new entry onto t4052_ttp_detail
			INSERT INTO t4052_ttp_detail
						(c4052_ttp_detail_id, c4050_ttp_id, c4052_ttp_link_date, c901_status, c4052_created_by
					   , c4052_created_date, c4052_forecast_period
						)
				 VALUES (v_ttp_detail_id, p_ttpid, TO_DATE (TO_CHAR (SYSDATE, 'MM/YYYY'), 'MM/YYYY'), 50581, p_userid
					   , SYSDATE, p_forecastmonths
						);

			-- 2. Update the v_ttp_detail_id created into t4040_demand_sheet for all the selected demand sheets
			UPDATE t4040_demand_sheet t4040
			   SET t4040.c4052_ttp_detail_id = v_ttp_detail_id
				 , t4040.c901_status = 50552
				 , t4040.c4040_last_updated_by = p_userid
				 , t4040.c4040_last_updated_date = SYSDATE
			 WHERE t4040.c4040_demand_sheet_id IN (SELECT *
													 FROM v_in_list);

			p_ttp_dtl_id := v_ttp_detail_id;
		END IF;

		SELECT COUNT (1)
		  INTO v_cnt_link_date
		  FROM t4052_ttp_detail
		 WHERE c4050_ttp_id = p_ttpid AND c4052_ttp_link_date = TO_DATE (TO_CHAR (SYSDATE, 'MM/YYYY'), 'MM/YYYY');

		IF (v_cnt_link_date > 1)
		THEN
			raise_application_error (-20067, '');	-- TTP sheet for this month has already been finalized
		END IF;

		-- 3. Load part, qty, historyfl information into t4053_ttp_part_detail for the selected demand sheets from t4042_demand_sheet_detail
		-- lock the inventory information
		SELECT MIN (c4042_period)	-- '8/1/2007'
		  INTO v_fc_start_date
		  FROM t4042_demand_sheet_detail t4042
		 WHERE t4042.c901_type = 50563 AND t4042.c4040_demand_sheet_id IN (SELECT *
																			 FROM v_in_list);

		WHILE INSTR (v_string, '|') <> 0
		LOOP
--
			v_partnumber := NULL;
			v_qty		:= NULL;
			v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
			v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
			v_partnumber := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
			v_qty		:= SUBSTR (v_substring, INSTR (v_substring, '^') + 1);

			BEGIN
				SELECT	 MAX (NVL (t405.c405_cost_price, 0) / NVL (c405_uom_qty, 1))
					INTO v_price
					FROM t405_vendor_pricing_details t405
				   WHERE t405.c405_active_fl = 'Y' AND t405.c205_part_number_id = v_partnumber AND t405.c405_void_fl IS NULL
				GROUP BY t405.c205_part_number_id;
			EXCEPTION
				WHEN NO_DATA_FOUND
				THEN
					v_price 	:= 0;
			END;

			INSERT INTO t4053_ttp_part_detail t4053
						(c4053_ttp_part_id, c4052_ttp_detail_id, c205_part_number_id, c4053_qty, c4053_cost_price
					   , c4053_history_fl, c4053_created_by, c4053_created_date
						)
				 VALUES (s4053_ttp_part_detail.NEXTVAL, p_ttp_dtl_id, v_partnumber, v_qty, v_price
					   , 'N', p_userid, SYSDATE
						);
		END LOOP;

		IF p_cnt = '0'
		THEN
			-- 4. Insert into t252_inventory_lock_ref with v_ttp_detail_id and p_inventorydate
			INSERT INTO t252_inventory_lock_ref
						(c252_lock_ref_id, c250_inventory_lock_id, c901_ref_type, c252_ref_id
						)
				 VALUES (s252_inventory_lock_ref.NEXTVAL, p_inventoryid, 20480, v_ttp_detail_id
						);
		END IF;
	END gm_fc_sav_ttp_dsmonthly;

--
	--
	/*******************************************************
   * Description : Procedure to save monthly TTP order quantities
   * Author 	 : D James
	Algorithm:
	1. Get TTP Detail ID
	2. Join with Inveotory Lock table to get values for InStock, built Set, Open PO and Open DHR
	3. Load the positive difference between inventory values and qty to order into t205c table
	4. update status of TTP Detail id to 'locked'
   *******************************************************/
--
	PROCEDURE gm_fc_sav_ttplock (
		p_ttpid    IN	t4050_ttp.c4050_ttp_id%TYPE
	  , p_month    IN	VARCHAR2
	  , p_year	   IN	VARCHAR2
	  , p_userid   IN	t4052_ttp_detail.c4052_created_by%TYPE
	)
	AS
		v_ttp_detail_id t4052_ttp_detail.c4052_ttp_detail_id%TYPE;
		v_qty		   NUMBER;
		v_pnum		   VARCHAR2 (20);
		v_crossoverfl  t205_part_number.c205_crossover_fl%TYPE;
		v_string	   VARCHAR2 (1000);
		v_ttp_type	   t4050_ttp.c901_ttp_type%TYPE;

		CURSOR cur_invqty
		IS
			SELECT	 t4053.c205_part_number_id pnum, t4053.c4053_qty qty, t205.c205_crossover_fl crossoverfl
				FROM t4053_ttp_part_detail t4053, t205_part_number t205
			   WHERE t4053.c4052_ttp_detail_id = v_ttp_detail_id
				 AND t4053.c205_part_number_id = t205.c205_part_number_id
				 AND t4053.c4053_qty <> 0
			ORDER BY t4053.c205_part_number_id;
	BEGIN
		SELECT c4052_ttp_detail_id, t4050.c901_ttp_type
		  INTO v_ttp_detail_id, v_ttp_type
		  FROM t4052_ttp_detail t4052, t4050_ttp t4050
		 WHERE t4050.c4050_ttp_id = p_ttpid
		   AND t4050.c4050_ttp_id = t4052.c4050_ttp_id
		   AND TO_CHAR (c4052_ttp_link_date, 'MM/YYYY') = p_month || '/' || p_year;

		--
		FOR cur_val IN cur_invqty
		LOOP
			v_qty		:= cur_val.qty;
			v_pnum		:= cur_val.pnum;
			v_crossoverfl := cur_val.crossoverfl;

			--
			IF v_qty < 0
			THEN
				v_qty		:= 0;
			END IF;

			IF (v_crossoverfl IS NULL OR v_ttp_type = '50310')
			THEN
				gm_cm_sav_partqty (v_pnum, v_qty, v_ttp_detail_id, p_userid, 90801, 4301, 4318);
			END IF;
		END LOOP;

		IF v_ttp_type IS NULL
		THEN
			v_string	:= gm_pkg_op_sheet.get_demand_sheet_ids (v_ttp_detail_id);
			gm_pkg_op_sheet.gm_op_sav_multipartsheet (v_string, p_userid);
		END IF;

		--updated date, by
		UPDATE t4052_ttp_detail
		   SET c901_status = 50582
			 , c4052_last_updated_by = p_userid
			 , c4052_last_updated_date = TRUNC (SYSDATE)
		 WHERE c4052_ttp_detail_id = v_ttp_detail_id;

		--
		-- Back Order functionality
		-- The following should be executed in a scheduled time, for now its directly called here
		gm_pkg_op_ttp_process_request.gm_sav_process_request (v_ttp_detail_id);
	END gm_fc_sav_ttplock;
--	
END gm_pkg_op_ttp;
/
