/* Formatted on 2010/05/20 10:14 (Formatter Plus v4.8.0) */
--@"C:\database\Packages\Operations\gm_pkg_op_loaner_extension.pkg";

CREATE OR REPLACE PACKAGE gm_pkg_op_loaner_extension
IS
--
 /******************************************************************
   * Description : Procedure to fetch loaner extension summary
   ****************************************************************/
	PROCEDURE gm_op_fch_le_summary (
		p_consignmentid   IN	   t504a_loaner_transaction.c504_consignment_id%TYPE
	  , p_outdata		  OUT	   TYPES.cursor_type
	);

	/******************************************************************
	* Description : Procedure to save loaner extension
	****************************************************************/
	PROCEDURE gm_op_sav_loaner_extension (
		p_consignmentid   IN   t504a_loaner_transaction.c504_consignment_id%TYPE
	  , p_extensiondate   IN   Date
	  , p_reasontype	  IN   t504b_loaner_extension_detail.c901_reason_type%TYPE
	  , p_comments		  IN   t504b_loaner_extension_detail.c504b_comments%TYPE
	  , p_createdby 	  IN   t504b_loaner_extension_detail.c504b_created_by%TYPE
	  , p_reqextfl		  IN   VARCHAR2
	  , p_surg_date       IN   t525_product_request.C525_SURGERY_DATE%TYPE DEFAULT NULL,
	   p_approve_fl       IN   VARCHAR2 DEFAULT NULL
	);

/******************************************************************
   * Description : Procedure to save loaner extension when more than once to extension w/ Replenish
   ****************************************************************/
	PROCEDURE gm_op_sav_multi_le (
		p_parentltxnid		  IN   t504b_loaner_extension_detail.c504a_loaner_transaction_id%TYPE
	  , p_new_loaner_txn_id   IN   t504b_loaner_extension_detail.c504a_loaner_transaction_id%TYPE
	);

/******************************************************************
   * Description : Procedure to save loaner extension
   ****************************************************************/
	PROCEDURE gm_op_sav_le_detail (
		p_loanertxnid	IN	 t504b_loaner_extension_detail.c504a_loaner_transaction_id%TYPE
	  , p_expectdate	IN	 t504b_loaner_extension_detail.c504b_expected_dt%TYPE
	  , p_reasontype	IN	 t504b_loaner_extension_detail.c901_reason_type%TYPE
	  , p_comments		IN	 t504b_loaner_extension_detail.c504b_comments%TYPE
	  , p_createdby 	IN	 t504b_loaner_extension_detail.c504b_created_by%TYPE
	  , p_status		  IN   t504b_loaner_extension_detail.c901_status_fl%TYPE
      , p_req_dtl_id    IN   t504b_loaner_extension_detail.C526_PRODUCT_REQUEST_DETAIL_ID%TYPE
	);

/******************************************************************
   * Description : Procedure to fetch loaner Request Detail
   ****************************************************************/
	PROCEDURE gm_op_fch_req_det (
		p_consignmentid   IN	   t504a_loaner_transaction.c504_consignment_id%TYPE
	  , p_reqextfl		  IN	   VARCHAR2
	  , p_outdata		  OUT	   TYPES.cursor_type
	);

	/******************************************************************
	  * Description : This function returns product request ID for gien CN
	  ****************************************************************/
	FUNCTION get_product_request_id (
		p_conid   IN   t504a_loaner_transaction.c504_consignment_id%TYPE
	)
		RETURN VARCHAR2;
		
   /*****************************************************************************
    * Description : Procedure to fetch loaner extension Pending Approval Requests
    * Author : HReddi
    *****************************************************************************/
	PROCEDURE gm_op_fch_lnext_req_det(
	  	p_outdata		  OUT	   TYPES.cursor_type
	);
	
	/*******************************************************************
	* Description : Procedure to save the Approval Loaner Extension Request
	* Author      : HReddi
	************************************************************************/
	PROCEDURE gm_op_sav_lnext_req_det(
	        p_req_id      IN    t525_product_request.c525_product_request_id%TYPE,
           p_req_det_id  IN    t526_product_request_detail.c526_product_request_detail_id%TYPE,
           p_exp_ret_dt	 IN	   t504a_loaner_transaction.c504a_expected_return_dt%TYPE,
           p_approv_cmts IN    t504a_loaner_transaction.c504a_appr_rej_comments%TYPE,
           p_user_id     IN    t2656_audit_entry.c2656_created_by%TYPE,
           p_surg_date   IN    t504a_loaner_transaction.c504a_surgery_date%TYPE,
           p_approve_fl  IN    VARCHAR2,
           p_out_err_msg OUT   CLOB
    );
	
    /************************************************************
	* Description : Procedure to Reject Loaner Extension Request
	* Author      : HReddi
	*************************************************************/
	PROCEDURE gm_op_sav_loaner_ext_rejt(
        p_req_id        IN    t525_product_request.c525_product_request_id%TYPE,
        p_req_det_id    IN    t526_product_request_detail.c526_product_request_detail_id%TYPE,
        p_rej_cmts      IN    t504a_loaner_transaction.c504a_appr_rej_comments%TYPE,
        p_user_id       IN    t2656_audit_entry.c2656_created_by%TYPE,
        p_out_err_msg   OUT   CLOB
    );
    
    /*************************************************************************************
	* This procedure is to send mail while Rejecting the Loaner Extension Request Details
	* Author : HReddi
	***************************************************************************************/
	PROCEDURE gm_op_loaner_ext_rejt_email (
		p_str			IN		 CLOB
	  , p_addr			IN		 VARCHAR2
	  , p_user_id 		IN		 t106_address.c106_created_by%TYPE
	);
	
	/*************************************************************************************
	* This procedure is to fetch the Loaner Extension Details based on the Prod Details ID
	* Author : HReddi
	***************************************************************************************/
	PROCEDURE gm_op_fch_lnext_req_requests (
		p_str			IN		 CLOB
	  ,	p_extnDetails		OUT	   TYPES.cursor_type
	  ,	p_newExtnDetails	OUT	   TYPES.cursor_type		
	);
	
	/********************************************************************************
	* This procedure is used to fetch the New Extn Retun date based on the Day Count
	* Author : HReddi
	*********************************************************************************/
	PROCEDURE gm_op_fch_req_new_exprtdate (
		p_input_str			IN		 CLOB
	  , p_newExtnDetails	OUT	   TYPES.cursor_type
	);
	
	/***************************************************************
	* This procedure is used to Extend the Loaner Extension Request
	* Author : HReddi
	****************************************************************/
	PROCEDURE gm_op_sav_loaner_ext_only (
		p_input_str			IN		CLOB
	  ,	p_user_id		    IN 		t106_address.c106_created_by%TYPE
	  ,	p_commnets          IN 		VARCHAR2
      , p_out_consignids     OUT     VARCHAR2
	);
	
	/***************************************************************
	* This procedure is used to Extend the Loaner Extension Request
	* Author : HReddi
	****************************************************************/
	PROCEDURE gm_op_sav_le_only_detail (
		p_req_dtl_id	IN		t526_product_request_detail.c526_product_request_detail_id%TYPE
	  ,	p_extn_date		IN 		t504a_consignment_loaner.c504a_expected_return_dt%TYPE
	  ,	p_reason_type   IN 		VARCHAR2
	  ,	p_status        IN      t504b_loaner_extension_detail.c901_status_fl%TYPE
	  ,	p_commnets      IN      VARCHAR2
	  ,	p_user_id       IN 		t106_address.c106_created_by%TYPE
	);
  /**************************************************************************
   * Description : This function returns Product Request ID Loaner extn count
   **************************************************************************/
   FUNCTION get_parent_lnrextn_cnt (
      p_requestids   IN   t526_product_request_detail.c525_product_request_id%TYPE
   )
      RETURN NUMBER;
      
 /*************************************************************************************
 * Description : Procedure to void FGLE Transactions
 *  Author      : Prabhu vigneshwaran M D  
 **************************************************************************************/   
   PROCEDURE gm_op_update_fgle(
	    p_req_det_id   IN    t526_product_request_detail.c526_product_request_detail_id%TYPE,
        p_status       IN    t504a_loaner_transaction.c901_status%TYPE,
        p_user_id      IN    t2656_audit_entry.c2656_created_by%TYPE);   
 /*************************************************************************************
 * Description : Procedure to get the Email Notification Details
 *  Author      : Tamizhthangam 
 **************************************************************************************/       
 PROCEDURE gm_fch_loaner_details(
   p_input_str		 IN		CLOB,
   p_user_id 		 IN		t106_address.c106_created_by%TYPE,
   p_status          IN     t504a_loaner_transaction.c901_status%TYPE,
   p_out_loaner_dtls OUT    TYPES.cursor_type,
   p_out_header_dtls OUT    TYPES.cursor_type
);

END gm_pkg_op_loaner_extension;
/
