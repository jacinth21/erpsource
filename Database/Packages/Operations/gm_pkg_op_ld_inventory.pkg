/* Formatted on 2010/11/16 11:54 (Formatter Plus v4.8.0) */
--@"c:\database\packages\operations\gm_pkg_op_ld_inventory.pkg"
-- show err  Package gm_pkg_op_ld_inventory
CREATE OR REPLACE
PACKAGE gm_pkg_op_ld_inventory
IS
    /*******************************************************
    * Purpose:
    *  Used to lock inventory status -- Inventory, Build Set, PO
    *  DHR (Depending on the type)
    *
    * modification history
    * ====================
    * Person   Date Comments
    * ---------   ------ ------------------------------------------
    * Richardk    20070820  Initial Version
    *******************************************************/
    /*******************************************************
    * Purpose: This procedure will be main procedure to
    *   load inventory information
    *******************************************************/
PROCEDURE gm_op_ld_demand_main (
        p_lock_type IN t250_inventory_lock.c901_lock_type%TYPE,
        p_company_id IN t1900_company.c1900_company_id%TYPE,
        p_plant_id IN t5040_plant_master.c5040_plant_id%TYPE) ;
    /*************************************************************************
    * Purpose: Procedure used to save inventory details (For different type)
    *************************************************************************/
PROCEDURE gm_op_sav_inventory_detail (
        p_inventory_lock_id IN t250_inventory_lock.c250_inventory_lock_id%TYPE,
        p_part_number IN t251_inventory_lock_detail.c205_part_number_id%TYPE,
        p_type IN t251_inventory_lock_detail.c901_type%TYPE,
        p_qty IN t251_inventory_lock_detail.c251_qty%TYPE) ;
    /*************************************************************************
    * Purpose: Procedure used to load inventory information
    *************************************************************************/
PROCEDURE gm_op_ld_inshelf (
        p_inventory_lock_id IN t250_inventory_lock.c250_inventory_lock_id%TYPE,
        p_lock_type IN t250_inventory_lock.c901_lock_type%TYPE) ;
    /*************************************************************************
    * Purpose: Procedure used to load Open PO Info
    *************************************************************************/
PROCEDURE gm_op_ld_openpo (
        p_inventory_lock_id IN t250_inventory_lock.c250_inventory_lock_id%TYPE) ;
    /*************************************************************************
    * Purpose: Procedure used to load Open DHR Info
    *************************************************************************/
PROCEDURE gm_op_ld_opendhr (
        p_inventory_lock_id IN t250_inventory_lock.c250_inventory_lock_id%TYPE) ;
    /*************************************************************************
    * Purpose: Procedure used to load Build Set Qty
    *************************************************************************/
PROCEDURE gm_op_ld_buildset (
        p_inventory_lock_id IN t250_inventory_lock.c250_inventory_lock_id%TYPE) ;
    /*************************************************************************
    * Purpose: Procedure used to load Open DHR and PO (mainly used for TTP load)
    *************************************************************************/
PROCEDURE gm_op_ld_openpodhr (
        p_inventory_lock_id IN t250_inventory_lock.c250_inventory_lock_id%TYPE) ;
    /*************************************************************************
    * Purpose: Procedure used to load Open DHR and PO (mainly used for TTP load)
    *************************************************************************/
PROCEDURE gm_op_ld_par (
        p_inventory_lock_id IN t250_inventory_lock.c250_inventory_lock_id%TYPE) ;
    --
    /*************************************************************************
    * Purpose: Procedure used to load Pending Requests
    *************************************************************************/
PROCEDURE gm_op_ld_pending_request (
        p_inventory_lock_id IN t250_inventory_lock.c250_inventory_lock_id%TYPE,
        p_from_period IN DATE) ;
    --
    /*************************************************************************
    * Purpose: Procedure used to load Pending Requests detail
    * Will lock open request, used for further reference
    *************************************************************************/
PROCEDURE gm_op_ld_pending_req_detail (
        p_inventory_lock_id IN t250_inventory_lock.c250_inventory_lock_id%TYPE) ;
    --
    /***************************************************************
    * Purpose: Procedure used to load inhouse loaners
    ***************************************************************/
PROCEDURE gm_op_ld_loaners (
        p_inventory_lock_id IN t250_inventory_lock.c250_inventory_lock_id%TYPE) ;
    --
    /***************************************************************
    * Purpose: Procedure used to load consignments
    ***************************************************************/
PROCEDURE gm_op_ld_consignments (
        p_inventory_lock_id IN t250_inventory_lock.c250_inventory_lock_id%TYPE) ;
    --
    --
    /***************************************************************
    * Purpose: Procedure used to load consignment lock detail
    ***************************************************************/
    --
PROCEDURE gm_op_sav_cons_lock_detail (
        p_inventory_lock_id IN t250_inventory_lock.c250_inventory_lock_id%TYPE,
        p_part_number IN t205_part_number.c205_part_number_id%TYPE,
        p_consignment_id IN t504_consignment.c504_consignment_id%TYPE,
        p_qty IN t253b_item_consignment_lock.c505_item_qty%TYPE,
	p_location_type     IN t253a_consignment_lock.c901_location_type%TYPE DEFAULT NULL) ;
    --
    --
    /***************************************************************
    * Purpose: Procedure used to load inhouse consignment lock
    ***************************************************************/
    --
PROCEDURE gm_op_sav_cons_lock (
        p_inventory_lock_id IN t250_inventory_lock.c250_inventory_lock_id%TYPE,
        p_consignment_id IN t253a_consignment_lock.c504_consignment_id%TYPE,
        p_distributor_id IN t253a_consignment_lock.c701_distributor_id%TYPE,
        p_status_fl IN t253a_consignment_lock.c504_status_fl%TYPE,
        p_void_fl IN t253a_consignment_lock.c504_void_fl%TYPE,
        p_account_id IN t253a_consignment_lock.c704_account_id%TYPE,
        p_set_id IN t253a_consignment_lock.c207_set_id%TYPE,
        p_type IN t253a_consignment_lock.c504_type%TYPE,
        p_verify_fl IN t253a_consignment_lock.c504_verify_fl%TYPE,
        p_verified_date IN t253a_consignment_lock.c504_verified_date%TYPE,
        p_verified_by IN t253a_consignment_lock.c504_verified_by%TYPE,
        p_request_id IN t253a_consignment_lock.c520_request_id%TYPE,
        p_master_consignment_id IN t253a_consignment_lock.c504_master_consignment_id%TYPE,
        p_last_updated_by IN t253a_consignment_lock.c504_last_updated_by%TYPE,
        p_last_updated_date IN t253a_consignment_lock.c504_last_updated_date%TYPE,
        p_location_type IN t253a_consignment_lock.c901_location_type%TYPE,
        p_pnum IN t253a_consignment_lock.c205_part_number_id%TYPE,
        p_controlnum IN t253a_consignment_lock.c408_control_number%TYPE,
        p_qty IN t253a_consignment_lock.c408_qty_received%TYPE,
		p_tag_id                IN t5010_tag.c5010_tag_id%TYPE DEFAULT NULL,
        p_inv_loc_id            IN t5052_location_master.c5052_location_id%TYPE DEFAULT NULL) ;
    --
    --
    /*************************************************************************
    * Purpose: Procedure used to load Sub Components
    *************************************************************************/
    --
PROCEDURE gm_op_ld_sub_components (
        p_inventory_lock_id IN t250_inventory_lock.c250_inventory_lock_id%TYPE) ;
    --
    /*************************************************************************
    * Purpose: Procedure used to delete Inserts and labels from locked Raw material  for PA
    *************************************************************************/
PROCEDURE gm_op_ld_del_rm_parts (
        p_inventory_lock_id IN t250_inventory_lock.c250_inventory_lock_id%TYPE) ;
    /********************************************************************************************
    * Description  :This function returns the WIP Qty for a Part for purchase DHR
    and not Manf. Dhr
    * Change Details:
    Mihir Since new Phy Audit Locations Manf WIP
    and Manf. Dhr was added as a part of 2010 Phy Audit
    Added condition NOT LIKE 'GM-MWO%' to exclude MDHR
    *********************************************************************************************/
    FUNCTION get_pnum_pdhr_wip_qty (
            p_partnum IN t402_work_order.c205_part_number_id%TYPE)
        RETURN VARCHAR2;

    /********************************************************************************************
    * Description  :This function returns the DHR Qty for a Part and based on the DHR ID as well
    and not Manf. Dhr
    *********************************************************************************************/
	
        FUNCTION get_pi_pnum_pdhr_wip_qty (
        p_partnum IN t402_work_order.c205_part_number_id%TYPE,
        p_dhr_id IN t408_dhr.c408_dhr_id%TYPE
        )RETURN VARCHAR2;
        
        /***************************************************************
        * Purpose: Procedure used to load Manufacturing WIP and DHRs
        ***************************************************************/
    PROCEDURE gm_op_ld_manufacturing (
            p_inventory_lock_id IN t250_inventory_lock.c250_inventory_lock_id%TYPE) ;
        /***************************************************************
        * Purpose: Procedure used for Inv. Locations for warehouse shelf
        * The locations loaded will behave as transactions for PI.
        * Loading table  t251_inventory_lock_detail  with Parts in stock  has been handled by
        gm_pkg_op_ld_inventory.gm_op_ld_inshelf for PI 2011
        ***************************************************************/
    PROCEDURE gm_op_ld_inv_locations (
            p_inventory_lock_id IN t250_inventory_lock.c250_inventory_lock_id%TYPE) ;
			
	/********************************************************************************************
	* Description :This function returns the WIP Qty for a Part for purchase DHR
	*and not Manf. Dhr. This function is added for PI only which is designed to exclude GM-MWO from DHR Lock.
	* the detail of this function  is taken from original function "get_pnum_pdhr_wip_qty version 55221".
	*********************************************************************************************/
    FUNCTION get_pi_pnum_pdhr_wip_qty (
            p_partnum IN t402_work_order.c205_part_number_id%TYPE)
        RETURN VARCHAR2;
 
    
    /*************************************************************************
	* Purpose: Procedure used to Load the Sales Data to the Order Lock and Item Order Lock Table.
	*************************************************************************/
    PROCEDURE gm_op_ld_sales_orders (
 		p_inventory_lock_id IN t250_inventory_lock.c250_inventory_lock_id%TYPE,
        p_from_period       IN DATE);
        
PROCEDURE GM_OP_LD_DHR_TXN (
        p_inventory_lock_id IN t250_inventory_lock.c250_inventory_lock_id%TYPE);
        
        END gm_pkg_op_ld_inventory;
   /
