--@"c:\database\packages\operations\gm_pkg_op_loaner_set_txn.bdy";
CREATE OR REPLACE
PACKAGE BODY gm_pkg_op_loaner_set_txn
IS
    /****************************************************************
    * Description : this procedure is used to update the Pending Pick Loaner status
    * Author      :
    *****************************************************************/
PROCEDURE gm_sav_loaner_status (
        p_txnid     IN t504a_consignment_loaner.c504_consignment_id%TYPE,
        p_operation IN VARCHAR2,
        p_user_id   IN t504a_consignment_loaner.c504a_last_updated_by%TYPE)
AS
    v_planned_ship_date t526_product_request_detail.c526_required_date%TYPE;
    v_req_det_id t526_product_request_detail.c526_product_request_detail_id%TYPE;
    v_hold_fl t504_consignment.c504_hold_fl%TYPE;
    v_txn_type     VARCHAR2 (40) ;
    v_allocated_fl VARCHAR2 (1) := 'Y';
    v_plant_id    t5040_plant_master.c5040_plant_id%TYPE;
BEGIN
    BEGIN
 		SELECT  NVL(get_plantid_frm_cntx(),get_rule_value('DEFAULT_PLANT','ONEPORTAL')) INTO  v_plant_id FROM DUAL;

         SELECT t526.c526_product_request_detail_id, t526.c526_required_date
           INTO v_req_det_id, v_planned_ship_date
           FROM t504a_consignment_loaner t504a, t504a_loaner_transaction t504al, t526_product_request_detail t526
          WHERE t504a.c504_consignment_id             = t504al.c504_consignment_id
            AND t504al.c526_product_request_detail_id = t526.c526_product_request_detail_id
            AND t504al.c504a_return_dt               IS NULL
            AND t504a.c504_consignment_id             = p_txnid
            AND t504a.c504a_void_fl                  IS NULL
            AND t504al.c504a_void_fl                 IS NULL
            AND t526.c526_void_fl                    IS NULL
            AND t504a.c5040_plant_id				  = v_plant_id;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_allocated_fl := 'N' ;
    END;
    IF (p_operation = 'PICK') THEN
        -- update the status as 'Pending Shipping'
        gm_pkg_op_loaner_set_txn.gm_upd_consign_loaner_status (p_txnid, '10', p_user_id) ;
        --update shiping info refid to CN ID
        gm_pkg_op_loaner_set_txn.gm_upd_shipping_ref (p_txnid, v_req_det_id, p_user_id) ;
        -- to call the shipping package
        gm_pkg_cm_shipping_trans.gm_sav_release_shipping (p_txnid, 50182, p_user_id, NULL) ; --50182 - Loaner
    ELSE -- PUT
        IF v_allocated_fl           = 'Y' THEN
            IF v_planned_ship_date <= TRUNC (CURRENT_DATE) THEN
                -- update status to 'Pending Shipping'
                gm_pkg_op_loaner_set_txn.gm_upd_consign_loaner_status (p_txnid, '10', p_user_id) ;
                --update shiping info refid to CN ID
                gm_pkg_op_loaner_set_txn.gm_upd_shipping_ref (p_txnid, v_req_det_id, p_user_id) ;
                -- to call the shipping package
                gm_pkg_cm_shipping_trans.gm_sav_release_shipping (p_txnid, 50182, p_user_id, NULL) ; --50182 -
                -- Loaner
            ELSE
                -- mark it as allocated
                gm_pkg_op_loaner_set_txn.gm_upd_consign_loaner_status (p_txnid, '5', p_user_id) ;
            END IF;
        ELSE
            -- update the status as 'Available'
            gm_pkg_op_loaner_set_txn.gm_upd_consign_loaner_status (p_txnid, '0', p_user_id) ;
        END IF;
    END IF;-- p_operation
END gm_sav_loaner_status;
/****************************************************************
* Description : this procedure is used to update loaner status
* Author      :
*****************************************************************/
PROCEDURE gm_upd_consign_loaner_status (
        p_txnid   IN t504a_consignment_loaner.c504_consignment_id%TYPE,
        p_status  IN t504a_consignment_loaner.c504a_status_fl%TYPE,
        p_user_id IN t504a_consignment_loaner.c504a_last_updated_by%TYPE)
AS
BEGIN
    -- Update the cn loaner status.
     UPDATE t504a_consignment_loaner
    SET c504a_status_fl         = p_status, c504a_last_updated_by = p_user_id, c504a_last_updated_date = CURRENT_DATE
      WHERE c504_consignment_id = p_txnid
        AND c504a_void_fl      IS NULL;
        
        -- Call the priority engine to prioritize the set
        gm_pkg_op_ln_priority_engine.gm_sav_main_ln_set_priority(p_txnid,p_user_id);
        
 --Changes added for PMT-12455 and it's used to trigger an update to iPad
--   gm_pkg_op_loaner_set_txn.GM_SAV_AVAIL_LN_SET_UPDATE (p_txnid, p_user_id); -- consignment_id, user_id, company_id

END gm_upd_consign_loaner_status;
/****************************************************************
* Description : this procedure is used to update shipping ref id
* Author      :
*****************************************************************/
PROCEDURE gm_upd_shipping_ref (
        p_txnid   IN t504a_consignment_loaner.c504_consignment_id%TYPE,
        p_req_id  IN t526_product_request_detail.c526_product_request_detail_id%TYPE,
        p_user_id IN t504a_consignment_loaner.c504a_last_updated_by%TYPE)
AS
BEGIN
     UPDATE t907_shipping_info
    SET c907_ref_id            = p_txnid, c907_status_fl = 30, -- pending shipping
        c907_last_updated_date = CURRENT_DATE, c907_last_updated_by = p_user_id
      WHERE c907_ref_id        = TO_CHAR (p_req_id)
        AND c901_source        = 50182
        AND c907_void_fl      IS NULL;
END gm_upd_shipping_ref;

/********************************************************
 * Description : This function is used to return the quantity of Loaner set.
 * Parameters  : p_consignmentid, p_setid
********************************************************/
	
FUNCTION get_loaner_pool_set_qty (
p_consignmentid	     	t504a_consignment_loaner.c504_consignment_id%TYPE,
p_setid	     	        t504_consignment.c207_set_id%TYPE
)
	RETURN NUMBER
	IS
	v_cnt		   NUMBER;
	BEGIN
	    SELECT count(1)
	    	INTO   v_cnt
           FROM t504_consignment t504, T207_SET_MASTER t207,t504a_consignment_loaner t504a
          WHERE t504.c504_consignment_id = t504a.c504_consignment_id
            AND t207.c207_set_id = t504.c207_set_id
            AND T504.C504_VOID_FL IS NULL
            AND t504.c504_type = 4127 -- only product loaners
            AND t504.c504_status_fl = '4'
            AND t504a.C504A_STATUS_FL != '60'
            AND t504a.c504a_void_fl IS NULL
            AND t207.C207_VOID_FL IS NULL
            AND t504.c504_consignment_id <> p_consignmentid
            AND t504.c207_set_id = p_setid;
	Return v_cnt;

	EXCEPTION
	 WHEN NO_DATA_FOUND
		THEN
			RETURN NULL;
	END get_loaner_pool_set_qty;

/********************************************************
 * Description : This Procedure is check the quantity of availale Loaner sets and update trigger to iPad
 * Parameters  : p_consignmentid, p_userid
 * Author      : jgurunathan
********************************************************/	
	
PROCEDURE GM_SAV_AVAIL_LN_SET_UPDATE (
	p_consgid		  	  t504_consignment.c504_consignment_id%TYPE
  , p_userid		  	  t504_consignment.c504_last_updated_by%TYPE
  , p_new_cn		  	  VARCHAR2 DEFAULT NULL
)
AS
	v_company_id  		  t1900_company.c1900_company_id%TYPE;
	v_ln_setid      	  t504_consignment.c207_set_id%TYPE;
	v_plant_id 			  T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
	v_qty                 NUMBER;
	v_cnt                 NUMBER;
	v_new_cn			  VARCHAR2 (20);
	v_cmp_exc_fl    t906_rules.c906_rule_value%TYPE;
    v_set_sync_val  t906_rules.c906_rule_value%TYPE;
--
BEGIN
	
	
	  SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL')) INTO v_company_id FROM dual;
	  
--Fetch company exclude flag
	SELECT NVL(get_rule_value_by_company(v_company_id,'EXC_SET_IPAD_FL',v_company_id),'N')INTO v_cmp_exc_fl FROM DUAL;
	
--Fetch exclude flag value
	SELECT get_rule_value(v_cmp_exc_fl,'IPAD_SET_SYNC')INTO v_set_sync_val FROM DUAL;
		
    v_ln_setid := get_set_id_from_cn(p_consgid);
    
   	SELECT COUNT(t207.C207_SET_ID) INTO v_cnt
	   	FROM t207_set_master t207, T2080_SET_COMPANY_MAPPING T2080
	  	WHERE T207.c901_set_grp_type  = 1601
	    AND T207.c901_status_id     = '20367' --Approved status
	    AND T207.C207_SET_ID        = T2080.C207_SET_ID
	    AND T207.C207_SET_ID        = v_ln_setid
	    AND T2080.C2080_VOID_FL    IS NULL  
	    AND T2080.C1900_COMPANY_ID = v_company_id
	 -- AND t207.c207_exclude_ipad_fl IS NULL
	 	AND DECODE(v_cmp_exc_fl,'Y', v_set_sync_val ,NVL(T207.C207_EXCLUDE_IPAD_FL,v_set_sync_val)) = v_set_sync_val
	    AND t207.c207_void_fl      IS NULL;
	    
	    IF (p_new_cn IS NULL) THEN
	    	v_new_cn := p_consgid;
	    ELSE
	    	v_new_cn := '-999';
	    END IF;
      
	v_qty := gm_pkg_op_loaner_set_txn.get_loaner_pool_set_qty(v_new_cn,v_ln_setid);

   		IF (v_qty = 1 AND p_new_cn = 'Y') THEN
			gm_pkg_op_set_master.gm_sav_set_attribute(v_ln_setid,'104740', 'Y', NULL,p_userid);
			IF v_cnt > 0 THEN
				gm_pkg_pdpc_prodcattxn.gm_sav_master_data_upd (v_ln_setid, '4000736', '103122', '103097', '103100',p_userid, NULL,'Y', v_company_id) ;
			END IF;
		ELSIF (v_qty = 0) THEN
			gm_pkg_op_set_master.gm_sav_set_attribute(v_ln_setid,'104740', 'N', NULL,p_userid);
			IF v_cnt > 0 THEN
				gm_pkg_pdpc_prodcattxn.gm_sav_master_data_upd (v_ln_setid, '4000736', '4000412', '103097', '103100',p_userid, NULL,'Y', v_company_id) ;
			END IF;
		END IF;
		
 END GM_SAV_AVAIL_LN_SET_UPDATE;
 
 /********************************************************
 * Description : This Procedure is return the shipped date. (PMT-13839)
 * Parameters  : p_txn_id
 * Author      :   
********************************************************/	
 
 FUNCTION GET_LOANER_SHIP_DATE(
 	p_txn_id	t412_inhouse_transactions.c412_inhouse_trans_id%TYPE
 )
 RETURN 	t907_shipping_info.c907_shipped_dt%TYPE
 IS
 	v_shipped_dt 	t907_shipping_info.c907_shipped_dt%TYPE;
 BEGIN
	 
	 BEGIN
		 SELECT t907.c907_shipped_dt INTO v_shipped_dt
			FROM t412_inhouse_transactions t412,
			  t504a_loaner_transaction t504a,
			  t526_product_request_detail t526,
			  t907_shipping_info t907
			WHERE t412.c412_inhouse_trans_id         = p_txn_id
			AND t412.c504a_loaner_transaction_id     = t504a.c504a_loaner_transaction_id
			AND t504a.c526_product_request_detail_id = t526.c526_product_request_detail_id
			AND t526.c525_product_request_id         = t907.c525_product_request_id
			AND t504a.c504_consignment_id 			 = t907.c907_ref_id
			AND t412.c412_void_fl                   IS NULL
			AND t504a.c504a_void_fl                 IS NULL
			AND t526.c526_void_fl                   IS NULL
			AND t907.c907_void_fl                   IS NULL;
		 EXCEPTION
		 WHEN NO_DATA_FOUND THEN
		 	v_shipped_dt := null;
	 END;
		
	 RETURN v_shipped_dt;
	 
 END GET_LOANER_SHIP_DATE;
 
 /****************************************************************
* Description : this procedure is used to fetch the set name
* Author      : DSandeep
*****************************************************************/
PROCEDURE get_act_loaner_set_name (
        p_set_id   IN  T207_SET_MASTER.c207_set_id%TYPE,
        p_set_name   OUT T207_SET_MASTER.C207_SET_NM%TYPE
        )
AS
V_PLANT_ID 	  t5040_plant_master.c5040_plant_id%TYPE;
v_set_Name    T207_SET_MASTER.C207_SET_NM%TYPE;
BEGIN
   BEGIN
     SELECT get_plantid_frm_cntx() into V_PLANT_ID from dual;
     
     SELECT DISTINCT(C207_SET_NM) INTO v_set_Name
           FROM t504_consignment t504, t504a_consignment_loaner t504a , T207_SET_MASTER 
                   t207,T2080_SET_COMPANY_MAPPING t2080
           WHERE t504.c504_consignment_id = t504a.c504_consignment_id
            AND t207.c207_set_id = t504.c207_set_id
            AND T207.C207_SET_ID = p_set_id
            AND T504.C504_VOID_FL IS NULL
            AND t504.c504_type = 4127 -- only product loaners
            AND t504.c504_status_fl = '4'
            AND t504a.C504A_STATUS_FL != '60'
            AND t504a.c504a_void_fl IS NULL
            AND t207.C207_VOID_FL IS NULL
            AND T2080.C207_SET_ID = T207.C207_SET_ID
            AND t2080.c2080_void_fl is null
            AND T2080.C1900_COMPANY_ID in (SELECT C1900_COMPANY_ID
            		FROM T5041_PLANT_COMPANY_MAPPING    WHERE C5041_PRIMARY_FL = 'Y' AND C5040_PLANT_ID= V_PLANT_ID
            	             AND C5041_VOID_FL IS NULL)
            AND t504a.C5040_PLANT_ID = V_PLANT_ID
            AND ROWNUM <= 1;
            EXCEPTION
		    WHEN NO_DATA_FOUND THEN
		 	v_set_Name := null;
       END;
       p_set_name:=v_set_Name;   
            
END get_act_loaner_set_name;
/**********************************************************************************
* Description : This procedure is used to update loaner status as return to missing
* Author      : Agilan
***********************************************************************************/
PROCEDURE gm_upd_missing_loaner_status (
        p_txnid   IN t504a_consignment_loaner.c504_consignment_id%TYPE,
        p_status  IN t5010_tag.c901_status%TYPE,
        p_user_id IN t504a_consignment_loaner.c504a_last_updated_by%TYPE)
AS
	v_missing_since		t5010_tag.c5010_missing_since%type;
	v_location_id       t5010_tag.c5010_location_id%type;
BEGIN
	BEGIN
	SELECT c5010_missing_since, c5011_location_id  into v_missing_since, v_location_id  from (SELECT c5010_missing_since, c5011_location_id
      			    FROM t5011_tag_log
			       WHERE c5011_last_updated_trans_id = p_txnid
					 AND c5010_MISSING_SINCE IS NOT NULL
				ORDER BY c5011_tag_txn_log_id DESC) 
	WHERE rownum =1;
    
    EXCEPTION WHEN NO_DATA_FOUND THEN
      v_missing_since :='';
      v_location_id   :='';
    END;

        UPDATE t5010_tag
           SET c901_status = p_status ,   -- Missing --Disputed
           	   c5010_location_id = DECODE(p_status,'26240614',get_rule_value('DISTRIBUTOR','TAGST'),v_location_id),   --Missing Loaction Id
               c5010_missing_since = DECODE(p_status,'51012',NULL,v_missing_since),--51012--Active
               c5010_last_updated_by = p_user_id,
               c901_location_type = '4120',
               C5010_LAST_UPDATED_DATE = current_date
         WHERE c5010_last_updated_trans_id = p_txnid
           AND c5010_void_fl is null;
    
END gm_upd_missing_loaner_status;

 
END gm_pkg_op_loaner_set_txn;
/