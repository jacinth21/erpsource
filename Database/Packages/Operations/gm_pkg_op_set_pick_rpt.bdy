--@"c:\database\packages\operations\gm_pkg_op_set_pick_rpt.bdy";
CREATE OR REPLACE 
PACKAGE BODY gm_pkg_op_set_pick_rpt
IS
    /****************************************************************
    * Description : this procedure is used to fetch the Pending Pick Dashboard
    * Author      :
    *****************************************************************/
PROCEDURE gm_fch_pending_pick_requests (
        p_type IN VARCHAR2, 
        p_out_rpt OUT TYPES.cursor_type)
AS 
v_plant_id T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
v_datefmt  VARCHAR2(200);
v_comp_id T1900_COMPANY.C1900_COMPANY_ID%TYPE;
v_filter T906_RULES.C906_RULE_VALUE%TYPE;
BEGIN

	  SELECT NVL (get_plantid_frm_cntx(), get_rule_value ('DEFAULT_PLANT', 'ONEPORTAL')), NVL (get_compdtfmt_frm_cntx(),
	    get_rule_value ('DATEFMT', 'DATEFORMAT')),get_compid_frm_cntx()
	   INTO v_plant_id, v_datefmt, v_comp_id
	   FROM dual;
	  
	  SELECT DECODE(GET_RULE_VALUE(v_comp_id,'PENDING_PICK_DASH'), null, v_comp_id, v_plant_id) --PENDING_PICK_DASH- To fetch the records for EDC entity countries based on plant
       INTO v_filter 
       FROM DUAL;
       
	--Fix pending pick stuck issue, remove join t5050 , rearrange order of tables  in query
    OPEN p_out_rpt FOR
	SELECT *
	  FROM (
		--Loaner (in house loaner, product loaner) Pending Pick
		 SELECT get_code_name (t526.c901_request_type) TYPE, t526.c901_request_type type_id, DECODE ( p_type, 'IN',
		    gm_pkg_sm_eventbook_txn.get_event_name (t525.c525_product_request_id), t525.c525_product_request_id)
		    req_eventid, t525.c525_product_request_id reqid, '' field_sales
		  , '' fid, 'O' status, '' user_id
		  , '' Short_nm, TO_CHAR (MIN (t526.c526_required_date), v_datefmt) planshipdt
		   FROM t504a_consignment_loaner t504b, t504a_loaner_transaction t504a, t525_product_request t525
		  , t526_product_request_detail t526
		  WHERE t504b.c504a_status_fl                = 7 --(pending Pick)
		    AND t504b.c504a_void_fl                 IS NULL
		    AND t504b.c504_consignment_id            = t504a.c504_consignment_id
		    AND t504a.c504a_void_fl                 IS NULL
		    AND t504a.c504a_return_dt               IS NULL
		    AND t504a.c526_product_request_detail_id = t526.c526_product_request_detail_id
		    AND t525.c525_product_request_id         = t526.c525_product_request_id
		    AND t526.c901_request_type               = DECODE (p_type, 'LN', 4127, 'CNLN', 4127, 'IN', 4119, - 9999)
		    AND t525.c525_void_fl                   IS NULL
		    AND t526.c526_void_fl                   IS NULL
		    AND (t504a.c1900_company_id = v_filter
		    OR t504a.c5040_plant_id   = v_filter)
		    AND t504a.C1900_COMPANY_ID = t525.C1900_COMPANY_ID
         	AND t504a.C5040_PLANT_ID  = t504b.C5040_PLANT_ID
        	AND t504a.C5040_PLANT_ID = t525.C5040_PLANT_ID
	       GROUP BY T526.C901_Request_Type, T525.C525_Product_Request_Id
	       ORDER BY t525.c525_product_request_id DESC
	    )
	UNION ALL
	    --consignment Pending Pick (To map the t5010_tag table - to avoid the item CN)
	    --here one scenario is , pending pick could come from built set rollback, in that case no distributor, so how as '
	    -- Set Building'
	    (
		 SELECT get_code_name (t504.c504_type) TYPE, t504.c504_type type_id, '' req_eventid
		  , '' reqid, DECODE (GET_DISTRIBUTOR_NAME (t504.c701_distributor_id), ' ', 'Set Building',
		    GET_DISTRIBUTOR_NAME (t504.c701_distributor_id)) field_sales, t504.c701_distributor_id fid, 'O' status
		  , '' user_id, '' Short_nm, NULL
		   FROM t504_consignment t504, t5010_tag t5010
		  WHERE t504.c504_status_fl = '2.20'
		    AND t504.c504_type      = DECODE (p_type, 'CN', t504.c504_type, 'CNLN', t504.c504_type, - 9999)
		    AND t504.c207_set_id   IS NOT NULL
		    AND T504.C504_Void_Fl  IS NULL
		    AND (t504.c1900_company_id = v_filter
	 		OR t504.c5040_plant_id   = v_filter)
	       GROUP BY t504.c701_distributor_id, t504.c504_type
	    ) ;

END gm_fch_pending_pick_requests;

/****************************************************************
* Description : this procedure is used to fetch the Pick details
* Author      :
*****************************************************************/
PROCEDURE gm_fch_pick_request_dtls (
        p_ref_id IN VARCHAR2,
        p_type   IN VARCHAR2,
        p_out_rpt OUT TYPES.cursor_type)
AS
BEGIN
    IF p_type = '4110' OR  p_type ='26240144' -- Consignment ,Return set
        THEN
        gm_fch_consigned_set_dtls (p_ref_id, p_out_rpt) ;
    ELSIF p_type = '4127' -- Product Loaner
        THEN
        gm_fch_loaner_set_dtls (p_ref_id, p_out_rpt) ;
    ELSIF p_type = '4119' -- In house Loaner
        THEN
        gm_fch_inhouse_loaner_set_dtls (p_ref_id, p_out_rpt) ;
    END IF;
    -- 4112 - Inhouse Consignment , 111802 - Finished Goods - Inhouse,  111805- Restricted Warehouse - Inhouse
    IF (p_type =  '4112' OR  p_type = '111802' OR p_type = '111805' )
       THEN
           raise_application_error ('-20955', '');-- Throw error for In House Consignment
       END IF;
    
END gm_fch_pick_request_dtls;
/****************************************************************
* Description : this procedure is used to fetch Loaner set details.
* Author      :
*****************************************************************/
PROCEDURE gm_fch_loaner_set_dtls (
        p_req_id IN t525_product_request.c525_product_request_id%TYPE,
        p_out_loaner OUT TYPES.cursor_type)
AS
v_plant_id T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
v_comp_id T1900_COMPANY.C1900_COMPANY_ID%TYPE;
v_filter T906_RULES.C906_RULE_VALUE%TYPE;
BEGIN
	SELECT  NVL(get_plantid_frm_cntx(),GET_RULE_VALUE('DEFAULT_PLANT','ONEPORTAL')) , get_compid_frm_cntx()
	INTO  v_plant_id,v_comp_id 
	FROM DUAL;
	
	SELECT DECODE(GET_RULE_VALUE(v_comp_id,'PENDING_PICK_DASH'), null, v_comp_id, v_plant_id) --PENDING_PICK_DASH- To fetch the records for EDC entity countries based on plant
      INTO v_filter 
      FROM DUAL;
	
    OPEN p_out_loaner FOR 
    SELECT t5010.c5010_tag_id tagid, t504.c207_set_id setid, t504a.c504_consignment_id cnid
	  , t5053.c5052_location_id locationid, GET_LOC_CD_FROM_ID (t5053.c5052_location_id) locationcd
	  , get_set_name (t504.c207_set_id) setdesc, t504a.c504a_etch_id etchid
	   FROM t504a_consignment_loaner t504a, t504_consignment t504, t504a_loaner_transaction t504a1, t526_product_request_detail t526
	  , t525_product_request t525, t5010_tag t5010, t5053_location_part_mapping t5053
	  WHERE t504a.c504_consignment_id = t504.c504_consignment_id
        AND t504a.c504_consignment_id = t504a1.c504_consignment_id
	    AND t504a.c504_consignment_id = t5010.c5010_last_updated_trans_id (+)
	    AND t5010.c5010_tag_id                = t5053.c5010_tag_id (+)
	    AND t504a.c504a_status_fl             = 7
	    -- Pending Pick
	    AND t525.c525_product_request_id        = t526.c525_product_request_id
	    AND t526.c526_product_request_detail_id = t504a1.c526_product_request_detail_id
	    AND t525.c525_product_request_id        = p_req_id
	    AND t525.c525_void_fl                  IS NULL
	    AND t504a.c504a_void_fl                IS NULL
	    AND t504a1.c504a_void_fl               IS NULL
	    AND T526.C526_Void_Fl                  IS NULL
	    AND T5010.C5010_Void_Fl(+)             IS NULL
	    AND (t504a1.c1900_company_id = v_filter
		 OR t504a1.c5040_plant_id   = v_filter) --used t504a_loaner_transaction table to fetch the records in Pending pick request screen
	ORDER BY locationid, tagid;
END gm_fch_loaner_set_dtls;
/****************************************************************
* Description : this procedure is used to fetch Consignment set details.
* Author      :
* Change      : Added location master mapping (PMT-12516)
*****************************************************************/
PROCEDURE gm_fch_consigned_set_dtls (
        p_dist_id IN t504_consignment.c701_distributor_id%TYPE,
        p_out_consigned OUT TYPES.cursor_type)
AS
		v_plant_id  T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;		
		v_comp_id   T1900_COMPANY.C1900_COMPANY_ID%TYPE;
		v_filter    T906_RULES.C906_RULE_VALUE%TYPE;
BEGIN
    SELECT  NVL(get_plantid_frm_cntx(),GET_RULE_VALUE('DEFAULT_PLANT','ONEPORTAL')) , get_compid_frm_cntx() INTO  v_plant_id,v_comp_id FROM DUAL;
	  
     SELECT DECODE(GET_RULE_VALUE(v_comp_id,'PENDING_PICK_DASH'), null, v_comp_id, v_plant_id) --PENDING_PICK_DASH- To fetch the records for EDC entity countries based on plant
      INTO v_filter 
      FROM DUAL;
      
    OPEN p_out_consigned FOR
     SELECT t5053.*
     FROM    
	  (SELECT t5010.c5010_tag_id tagid, t504.c207_set_id setid, t504.c504_consignment_id cnid
	  , t5053.c5052_location_id locationid, GET_LOC_CD_FROM_ID (t5053.c5052_location_id) locationcd ,'' setdesc, '' etchid
	  ,t504.c5040_plant_id plant_id
	   FROM t504_consignment t504, t5010_tag t5010, t5053_location_part_mapping t5053
	  WHERE t504.c504_consignment_id               = t5010.c5010_last_updated_trans_id (+)
	    AND t5010.c5010_tag_id                     = t5053.c5010_tag_id (+)
	    AND t504.c504_status_fl                    = '2.20' -- Pending Pick
	    AND NVL (T504.C701_Distributor_Id, '-999') = NVL (p_dist_id, '-999')
	    AND T504.C504_Void_Fl                     IS NULL
	    AND T5010.C5010_Void_Fl(+)                  IS NULL
	    AND (t504.c1900_company_id = v_filter
		 OR t504.c5040_plant_id   = v_filter)
	   AND t504.c5040_plant_id	  = t5010.c5040_plant_id(+)) T5053,
	   t5052_location_master t5052
	   WHERE t5053.locationid      =t5052.c5052_location_id(+)
       AND t5052.c5052_void_fl(+) IS NULL
       AND t5053.plant_id          = t5052.c5040_plant_id(+)	   
       ORDER BY locationid, tagid;
END gm_fch_consigned_set_dtls;
/****************************************************************
* Description : this procedure is used to fetch the In house loaner details
* Author      :
*****************************************************************/
PROCEDURE gm_fch_inhouse_loaner_set_dtls (
        p_req_id IN t525_product_request.c525_product_request_id%TYPE,
        p_out_loaner OUT TYPES.cursor_type)
AS
		v_plant_id    t5040_plant_master.c5040_plant_id%TYPE;
		v_comp_id T1900_COMPANY.C1900_COMPANY_ID%TYPE;
		v_filter T906_RULES.C906_RULE_VALUE%TYPE;
BEGIN
    SELECT  NVL(get_plantid_frm_cntx(),GET_RULE_VALUE('DEFAULT_PLANT','ONEPORTAL')) , get_compid_frm_cntx() INTO  v_plant_id,v_comp_id FROM DUAL;
    
     SELECT DECODE(GET_RULE_VALUE(v_comp_id,'PENDING_PICK_DASH'), null, v_comp_id, v_plant_id) --PENDING_PICK_DASH- To fetch the records for EDC entity countries based on plant
      INTO v_filter 
      FROM DUAL;

    OPEN p_out_loaner FOR 
	 SELECT t5010.c5010_tag_id tagid, t504.c207_set_id setid, t504a.c504_consignment_id cnid
	  , t5053.c5052_location_id locationid, GET_LOC_CD_FROM_ID (t5053.c5052_location_id) locationcd
	  , get_set_name (t504.c207_set_id) setdesc, t504a.c504a_etch_id etchid
	   FROM t504a_consignment_loaner t504a,t504_consignment t504, t504a_loaner_transaction t504a1, t526_product_request_detail t526
	  , t525_product_request t525, t5010_tag t5010, t5053_location_part_mapping t5053
	  WHERE t504a.c504_consignment_id = t504.c504_consignment_id
        AND t504a.c504_consignment_id = t504a1.c504_consignment_id
	    AND t504a.c504_consignment_id = t5010.c5010_last_updated_trans_id (+)
	    AND t5010.c5010_tag_id                = t5053.c5010_tag_id (+)
	    AND t504a.c504a_status_fl             = 7
	  	AND (t504a1.c1900_company_id = v_filter
		OR	t504a1.c5040_plant_id   = v_filter)
		AND t504a1.c1900_company_id	= t504.c1900_company_id
		AND t504a1.c1900_company_id	=	t525.c1900_company_id
 		AND t504a1.c5040_plant_id	= t504.c5040_plant_id
		AND t504a1.c5040_plant_id	= t525.c5040_plant_id
		AND t504.c5040_plant_id		= t504a.c5040_plant_id
	    -- Pending Pick
	    AND t525.c525_product_request_id        = t526.c525_product_request_id
	    AND t526.c526_product_request_detail_id = t504a1.c526_product_request_detail_id
	    AND t525.c525_product_request_id        = p_req_id
	    AND t525.c525_void_fl                  IS NULL
	    AND T504a.C504a_Void_Fl                IS NULL
	    AND T504a1.C504a_Void_Fl               IS NULL
	    AND t526.c526_void_fl                  IS NULL
	    AND t5010.c5010_void_fl (+)               IS NULL;
END gm_fch_inhouse_loaner_set_dtls;


/*******************************************************
* Description : function to fetch location_cd by location id
* Author    :
*******************************************************/
FUNCTION GET_LOC_CD_FROM_ID (
        p_loc_id t5052_location_master.c5052_location_id%TYPE)
    RETURN VARCHAR2
IS
    v_loc_cd t5052_location_master.c5052_location_cd%TYPE;
BEGIN
     SELECT c5052_location_cd
       INTO v_loc_cd
       FROM t5052_location_master
      WHERE c5052_location_id = p_loc_id;
    RETURN v_loc_cd;
EXCEPTION
WHEN NO_DATA_FOUND THEN
    RETURN NULL;
END GET_LOC_CD_FROM_ID;

END gm_pkg_op_set_pick_rpt;
/	