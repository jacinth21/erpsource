/* Formatted on 3/18/2021 5:12:52 PM (QP5 v5.362) */
CREATE OR REPLACE PACKAGE BODY it_pkg_order_tag_do_classification
IS
    /*********************************************************
   * Description : This is a wrapper for calling all
   * DO Classification procedures
   *********************************************************/
    PROCEDURE gm_sav_order_tag_do_main
    AS
	v_to_email_id    VARCHAR2 (1000);
	v_subject        VARCHAR2 (1000);
	v_body           VARCHAR2 (1000);
	crlf        	 VARCHAR2 (2) := CHR (13) || CHR (10);	
    BEGIN
		-- Clear the temp before starting the process
		execute immediate 'TRUNCATE TABLE t501c_order_tag_do_classification_temp';
		
		BEGIN
        -- check and clear classification
        it_pkg_order_tag_do_classification.gm_sav_check_clear_classification ();
        
        -- Load the temp
        it_pkg_order_tag_do_classification.gm_sav_order_tag_do_class_tmp ();
        
        -- Load the process table from temp
        it_pkg_order_tag_do_classification.gm_sav_order_tag_do_class ();
        
        -- Update critical system flag
        it_pkg_order_tag_do_classification.gm_sav_order_update_tag_do_class ();
		
		-- Mark the orders as processsed to it won't be considered for the next run
        UPDATE t501_order t501
           SET t501.c501_do_classified_fl = 'Y'
         WHERE c501_do_classified_fl IS NULL
		 AND (t501.c901_order_type IS NULL OR t501.c901_order_type NOT IN (2518, 2524, 2535, 2533, 2534, 2519))
		 AND t501.c501_void_fl   IS NULL;
		 
		COMMIT;

		--Update this to NULL ,so for it will take all the record before delete for those orders
        UPDATE t501c_order_tag_do_classification
		SET c501c_record_processed_fl = NULL
		WHERE C501_ORDER_ID          IN
		  (SELECT DISTINCT c501_order_id FROM t501c_order_tag_do_classification_temp
		  );

		-- check and remove duplicates from t501c_order_tag_do_classification
		DELETE
		FROM t501c_order_tag_do_classification t501c
		WHERE t501c.rowid IN
		  (SELECT t501c.rowid
		  FROM t501c_order_tag_do_classification t501c,
			( SELECT DISTINCT c501_order_id FROM t501c_order_tag_do_classification_temp
			) t501c_tmp
		  WHERE t501c.c501_order_id = t501c_tmp.c501_order_id
		  AND t501c.rowid NOT      IN
			(SELECT MAX(t501c.rowid)
			FROM t501c_order_tag_do_classification t501c,
			  ( SELECT DISTINCT c501_order_id FROM t501c_order_tag_do_classification_temp
			  ) t501c_tmp
			WHERE t501c.c501_order_id = t501c_tmp.c501_order_id
			GROUP BY t501c.c501_order_id,
			  t501c.c207_set_id,
			  t501c.c207_system_id,
			  t501c.c5010_tag_id
			)
		  );
			  
		-- Update the processesd Flag
        UPDATE t501c_order_tag_do_classification
           SET c501c_record_processed_fl = 'X' --Update this to 'X' and will finally update to 'Y' on the new do class validation job
         WHERE c501c_record_processed_fl IS NULL; 

		COMMIT;
		
	EXCEPTION WHEN OTHERS THEN

 		v_to_email_id := get_rule_value ('TO_EMAIL_ID', 'DO_TAG_NOTIFICATION');
		v_subject := get_rule_value ('EMAIL_SUBJECT', 'DO_TAG_NOTIFICATION');
		v_body := get_rule_value ('EMAIL_BODY', 'DO_TAG_NOTIFICATION');
		
		v_body := v_body
		|| crlf
		|| ' Error message :- '
		|| SQLERRM;
		
		gm_com_send_email_prc (v_to_email_id, v_subject, v_body);
		
		ROLLBACK;
		
	END;
    END gm_sav_order_tag_do_main;

    /*********************************************************
  * Description : Check and clear classification if any DO's
  * are modified from the last run
  *********************************************************/
    PROCEDURE gm_sav_check_clear_classification
    AS
        v_last_sync_date   t9300_job.C9300_START_DATE%TYPE := NULL;
        v_job_id           t9300_job.C9300_JOB_ID%TYPE := 1062; --1062 is the job id for gm_sav_order_tag_do_main
        v_user_id          t9300_job.C9300_LAST_UPDATED_BY%TYPE := 30301;
    BEGIN
        --fetch the last sync date and update startdate as currentdate of job.
        gm_pkg_device_job_run_date.gm_update_job_details (v_job_id,v_user_id,
                                                          v_last_sync_date);
		/*
		 *  2518	Draft
			2519	Pending CS Confirmation
			2524	Price Adjustment
			2533	ICS
			2534	ICT returns Order
			2535	Sales Discount
		 */
        /*if the classify flag is null then delete the order from the table 
         * (This below Delete statement is mainly added for reclassifying the order by FAM team)
         */
        DELETE FROM t501c_order_tag_do_classification t501c
        WHERE t501c.c501_order_id IN
          (SELECT NVL (t501.c501_parent_order_id, t501.c501_order_id) orderid
              FROM T501_ORDER t501
              WHERE t501.c501_do_classified_fl IS NULL
              AND t501.c501_void_fl            IS NULL
              AND (t501.c901_order_type        IS NULL
              OR t501.c901_order_type NOT      IN (2518, 2524, 2535, 2533, 2534, 2519))
          );
        
		--Delete the Order from do tag classification for the newly created tags
        DELETE FROM t501c_order_tag_do_classification T501C
        WHERE t501c.c501_order_id IN
                    (
                      SELECT DISTINCT NVL (t501.c501_parent_order_id, t501.c501_order_id)
						FROM t501_order t501,
						  t501d_order_tag_usage_details t501d
						WHERE t501.c501_order_id = t501d.c501_order_id
						AND t501d.c501d_created_date >= v_last_sync_date
						AND (t501.c901_order_type IS NULL OR t501.c901_order_type NOT IN (2518, 2524, 2535, 2533, 2534, 2519))
						AND t501.c501_void_fl   IS NULL
						AND t501d.c501d_void_fl IS NULL
					);

		--Update the classify flag for the Order which has newly created tags.
        UPDATE t501_order T501
           SET t501.c501_do_classified_fl = NULL
         WHERE t501.c501_do_classified_fl IS NOT NULL
               AND T501.c501_order_id IN
                       (
                         SELECT c501_order_id
                          FROM t501d_order_tag_usage_details t501d
                         WHERE t501d.c501d_created_date >=  v_last_sync_date
                        )
               AND (t501.c901_order_type IS NULL OR t501.c901_order_type NOT IN (2518, 2524, 2535, 2533, 2534, 2519))
               AND t501.c501_void_fl IS NULL;
		
		--Update the classify flag for the parent Order if the tag is entered for child order
		UPDATE t501_order T501
		SET t501.c501_do_classified_fl    = NULL
		WHERE t501.c501_do_classified_fl IS NOT NULL
		AND T501.c501_order_id           IN
		  (SELECT c501_parent_order_id
		  	FROM t501_order
		  WHERE c501_order_id IN
		    (SELECT c501_order_id
		    	FROM t501d_order_tag_usage_details t501d
		    WHERE t501d.c501d_created_date >= v_last_sync_date
		    )
		  )
		AND (t501.c901_order_type   IS NULL OR t501.c901_order_type NOT IN (2518, 2524, 2535, 2533, 2534, 2519))
		AND t501.c501_void_fl       IS NULL; 
		
		--Update the classify flag for all the child orders available for the parent Order if the tag entered for child order
		UPDATE t501_order T501
		SET t501.c501_do_classified_fl    = NULL
		WHERE t501.c501_do_classified_fl IS NOT NULL
		AND T501.c501_parent_order_id   IN
		  (SELECT c501_parent_order_id
		  	FROM t501_order
		  WHERE c501_order_id IN
		    (SELECT c501_order_id
		   	 FROM t501d_order_tag_usage_details t501d
		    WHERE t501d.c501d_created_date >=v_last_sync_date
		    )
		  )
		AND (t501.c901_order_type   IS NULL OR t501.c901_order_type NOT IN (2518, 2524, 2535, 2533, 2534, 2519))
		AND t501.c501_void_fl       IS NULL; 

		/*update the classify flag for all the child orders available for the parent order if the 
			tag is entered for PARENT ORDER
		*/
		UPDATE t501_order T501
		SET t501.c501_do_classified_fl    = NULL
		WHERE t501.c501_do_classified_fl IS NOT NULL
		AND T501.c501_order_id           IN
		  (SELECT c501_order_id
		  FROM t501_order
		  WHERE c501_parent_order_id IN
		    (SELECT c501_order_id
		    FROM t501d_order_tag_usage_details t501d
		    WHERE t501d.c501d_created_date >= v_last_sync_date
		    )
		  )
		AND (t501.c901_order_type   IS NULL OR t501.c901_order_type NOT IN (2518, 2524, 2535, 2533, 2534, 2519))
		AND t501.c501_void_fl       IS NULL; 
		
		--The below two updates used to solve the tag not picking in the job because of child / parent order date issue				 
		DELETE FROM t501c_order_tag_do_classification t501c
        WHERE t501c.c501_order_id IN
                        (SELECT c501_parent_order_id
							FROM t501_order
						 WHERE c501_parent_order_id IS NOT NULL
						 AND c501_void_fl           IS NULL
						 AND (c901_order_type IS NULL OR c901_order_type NOT IN (2518, 2524, 2535, 2533, 2534, 2519))
						 AND c501_created_date >= v_last_sync_date);
        --
        UPDATE t501_order T501
           SET t501.c501_do_classified_fl = NULL
         WHERE t501.c501_do_classified_fl IS NOT NULL
               AND T501.C501_ORDER_ID IN
                       (SELECT c501_parent_order_id
							FROM t501_order
						 WHERE c501_parent_order_id IS NOT NULL
						 AND c501_void_fl           IS NULL
						 AND (c901_order_type IS NULL OR c901_order_type NOT IN (2518, 2524, 2535, 2533, 2534, 2519))
						 AND c501_created_date >= v_last_sync_date);
        -- 
    END gm_sav_check_clear_classification;


    /*********************************************************
    * Description : This Procedure is used to load the orders
    * that are already classified by DO Classification system
    * and load the orders that has tags recorded
    * to a temp table.
    *********************************************************/
    PROCEDURE gm_sav_order_tag_do_class_tmp
    AS
    BEGIN
        -- Load DO Classification details to the temp table
        INSERT INTO t501c_order_tag_do_classification_temp (c501_order_id,
                                                            c207_system_id,
                                                            c501c_class_type)
            (SELECT DISTINCT
                    NVL (t501.c501_parent_order_id, t501b.c501_order_id)  c501_order_id,
                    t501b.c207_system_id  systemid,
                    'D'
               FROM t501b_order_by_system t501b, t501_order t501
              WHERE     t501.c501_order_id = t501b.c501_order_id
                    AND t501.c501_void_fl IS NULL
                    AND t501.c501_delete_fl IS NULL
                    AND t501b.c501b_void_fl IS NULL
					AND (t501.c901_order_type IS NULL OR t501.c901_order_type NOT IN (2518, 2524, 2535, 2533, 2534, 2519))
                    AND t501.c501_do_classified_fl IS NULL);

        -- Load Tag details to the temp table
        INSERT INTO t501c_order_tag_do_classification_temp (c501_order_id,
                                                            c207_system_id,
                                                            c501c_class_type)
            SELECT DISTINCT
                   NVL (t501.c501_parent_order_id, t501d.c501_order_id)  c501_order_id,
                   t207.c207_set_sales_system_id systemid,
                   'T'
              FROM t501d_order_tag_usage_details  t501d,
                   t207_set_master                t207,
                   t501_order                     t501
             WHERE  t501d.c501_order_id = t501.c501_order_id
                   AND t501d.c207_set_id = t207.c207_set_id(+)
                   AND t501d.c501d_void_fl IS NULL
                   AND t501.c501_void_fl IS NULL
                   AND t501.c501_delete_fl IS NULL
                   AND t207.c207_void_fl IS NULL
                   AND t501d.c5010_tag_id IS NOT NULL
				   AND (t501.c901_order_type IS NULL OR t501.c901_order_type NOT IN (2518, 2524, 2535, 2533, 2534, 2519))
                   AND t501.c501_do_classified_fl IS NULL;

        -- Remove the duplicates if order is available in both DO Class and Tag [ In Such Case remove the tag record ]
        DELETE FROM t501c_order_tag_do_classification_temp temp
         WHERE     (temp.c501_order_id, temp.c207_system_id) IN
                            (  SELECT c501_order_id, c207_system_id
                                 FROM t501c_order_tag_do_classification_temp
                             GROUP BY c501_order_id, c207_system_id
                               HAVING COUNT (1) > 1)
           AND  temp.c501c_class_type = 'T';       
    END gm_sav_order_tag_do_class_tmp;

    /*********************************************************
    * Description : This Procedure is used to load the orders
    * from the temp table to the regular table along with the
    * details of order
    *********************************************************/
    PROCEDURE gm_sav_order_tag_do_class
    AS
    BEGIN
        /* Read from the temp table and load the details of
        * the tags in classification table
        */

        INSERT INTO t501c_order_tag_do_classification (
                        c501_order_id,
                        c207_system_id,
                        c501c_class_type,
                        c207_set_id,
                        c5010_tag_id,
                        c501d_tag_created_by_name,
                        c501d_tag_created_date)
            SELECT main_order.c501_order_id,
                   main_order.c207_system_id,
                   main_order.c501c_class_type,
                   tag_info.c207_set_id,
                   tag_info.c5010_tag_id,
                   tag_info.c501d_tag_created_by_name,
                   tag_info.c501d_created_date
              FROM t501c_order_tag_do_classification_temp  main_order,
                   (SELECT DISTINCT
                           NVL (t501.c501_parent_order_id,
                                t501d.c501_order_id)
                               c501_order_id,
                           t207.c207_set_sales_system_id
                               systemid,
                           t501d.c5010_tag_id,
                           t207.c207_set_nm,
                           t501d.c207_set_id,
                           t207.c901_cons_rpt_id,
                           t501d.c501d_created_by,
                           (   t101.c101_user_f_name
                            || ' '
                            || t101.c101_user_l_name)
                               c501d_tag_created_by_name,
                           t501d.c501d_created_date
                      FROM t501d_order_tag_usage_details  t501d,
                           t207_set_master                t207,
                           t501_order                     t501,
                           t101_user                      t101
                     WHERE     t501d.c501_order_id = t501.c501_order_id
                           AND t501d.c207_set_id = t207.c207_set_id(+)
                           AND t501d.c501d_void_fl IS NULL
                           AND t207.c207_void_fl IS NULL
                           AND t501d.c5010_tag_id IS NOT NULL
                           AND t501.c501_do_classified_fl IS NULL
						   AND (t501.c901_order_type IS NULL OR t501.c901_order_type NOT IN (2518, 2524, 2535, 2533, 2534, 2519))
                           AND T501D.c501d_created_by = t101.c101_user_id(+))
                   Tag_Info
             WHERE     main_order.c501_order_id = Tag_Info.c501_order_id(+)
                   AND main_order.C207_SYSTEM_ID = Tag_Info.systemid(+);
    END gm_sav_order_tag_do_class;

    /*********************************************************
    * Description : This Procedure is used to analyze DOs
    * for Critical System, Suspect as Incorrect, Critical System
    * with Issue and update the same in the classification
    *********************************************************/
    PROCEDURE gm_sav_order_update_tag_do_class
    AS
    BEGIN
        -- SQL to Flag DO with Critical System
        UPDATE t501c_order_tag_do_classification t501c
           SET t501c.c501c_do_with_critical_sys = 'Y'
         WHERE t501c.c501c_record_processed_fl IS NULL
          AND  t501c.c501_order_id in
                   (SELECT DISTINCT t501ct.c501_order_id
                      FROM t501c_order_tag_do_classification  t501ct,
                           t207_set_master                    t207
                     WHERE  t501ct.c207_system_id = t207.c207_set_id
                      AND t207.c207_critical_system_fl IS NOT NULL
                      AND t501ct.c501c_record_processed_fl IS NULL);

        -- Flag the DO with Critical Flag with Issue
        UPDATE t501c_order_tag_do_classification t501c
           SET t501c.c501c_do_with_critical_sys_issue = 'I'
         WHERE t501c.c501c_record_processed_fl IS NULL
           AND t501c.c501_order_id IN
                       (SELECT DISTINCT c501_order_id
                          FROM t501c_order_tag_do_classification  t501ct,
                               t207_set_master                    t207
                         WHERE  t501ct.c501c_do_with_critical_sys  IS NOT NULL
                           AND t501ct.c501c_class_type = 'T'
                           AND t501ct.c207_set_id = t207.c207_set_id
                           AND t207.c901_cons_rpt_id = 20100
                           AND t501ct.c501c_record_processed_fl IS NULL);

        -- Flag the DO with Suspect
        UPDATE t501c_order_tag_do_classification t501c
           SET t501c.c501c_suspect_fl = 'Y'
         WHERE t501c.c501c_record_processed_fl IS NULL
           AND (t501c.c501_order_id, t501c.c207_set_id) IN
                   (  SELECT t501cx.c501_order_id, t501cx.c207_set_id
                        FROM t501c_order_tag_do_classification t501cx
                        WHERE t501cx.c501c_record_processed_fl IS NULL
						AND t501cx.c207_set_id IS NOT NULL
                    GROUP by t501cx.c501_order_id, t501cx.c207_set_id
                      HAVING COUNT (1) > 1);
    END gm_sav_order_update_tag_do_class;
END it_pkg_order_tag_do_classification;
/