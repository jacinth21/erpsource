/* Formatted on 2011/04/28 10:36 (Formatter Plus v4.8.0) */
--@"c:\database\packages\operations\gm_pkg_op_bo_txn.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_bo_txn
IS
--
/***********************************************************************************
* Description : Procedure to release the back orders
* Author  : karthiks
************************************************************************************/

  PROCEDURE gm_process_sales_backorder (
   p_orderid   		  IN       t501_order.c501_order_id%TYPE,
   p_pnum   		  IN       T502_ITEM_ORDER.c205_part_number_id%TYPE ,
   p_bo_Release_qty   IN       T502_ITEM_ORDER.C502_ITEM_QTY%TYPE,
   p_user_id   		  IN       t501_order.c501_last_updated_by%TYPE,
   p_out_err_msg      OUT      CLOB
)
AS
--
   v_item_qty      NUMBER;
   v_excess_qty    NUMBER;
   v_stock         NUMBER;
   v_pnum          t502_item_order.c205_part_number_id%TYPE;
   v_price		   t502_item_order.c502_item_price%TYPE;
   v_status_fl	   t501_order.c501_status_fl%TYPE;
   v_err_msg	   VARCHAR2(4000) := '';
   v_out_err_msg   CLOB := '';
   v_order_info_id VARCHAR2(20);
   v_invoice_id    t501_order.C503_Invoice_Id%TYPE;
--
BEGIN
---------- VALIDATION STARTS -----------------
-- Check order status and if not in back order throw error
-- Getting Invoice Id column for the PC-3491 changes
BEGIN
   SELECT c501_status_fl, c503_invoice_id 
      INTO v_status_fl, v_invoice_id
     FROM t501_order
    WHERE c501_order_id = p_orderid 
    AND c501_VOID_FL IS NULL
    FOR UPDATE;
EXCEPTION WHEN NO_DATA_FOUND THEN
	v_err_msg := 'Order is not available to release';
	v_out_err_msg := v_out_err_msg || v_err_msg || '; ';
END;

	IF v_status_fl > 0 THEN
		v_err_msg := 'Order status is not in Back Order';
		v_out_err_msg := v_out_err_msg || v_err_msg || '; ';
	END IF;
-- 
 BEGIN
   SELECT c205_part_number_id, SUM(c502_item_qty) 
     INTO v_pnum, v_item_qty 
     FROM t502_item_order
    where c501_order_id = p_orderid 
    AND c205_part_number_id = p_pnum
    AND c502_VOID_FL IS NULL 
    GROUP BY c205_part_number_id;
EXCEPTION WHEN NO_DATA_FOUND THEN
	v_err_msg := 'Part number is not available to release for this order';
	v_out_err_msg := v_out_err_msg || v_err_msg || '; ';
END;

    /* Release qty cannot be empty
	 */
	 
   IF p_bo_Release_qty IS NULL OR p_bo_Release_qty = ''
   THEN
      v_err_msg := 'Qty to Release cannot be empty';
      v_out_err_msg := v_out_err_msg || v_err_msg || '; ';
   END IF;
   
    /* if the release qty is greater then the item quantity OR
	 * if the item quantity is less than Zero 
	 */
	 
   IF p_bo_Release_qty > v_item_qty OR p_bo_Release_qty <= 0
   THEN
      v_err_msg := 'Qty to Release should be less than or equal to Backorder Quantity';
      v_out_err_msg := v_out_err_msg || v_err_msg || '; ';
   END IF;
   
   	 BEGIN
	     SELECT get_qty_in_stock (v_pnum) 
	       INTO v_stock
	       FROM t205_part_number
	      WHERE c205_part_number_id = v_pnum;
        END;
        
	/* if the release qty is greater than FG qty OR
	*/
   IF p_bo_Release_qty > v_stock
   THEN
      v_err_msg := 'Qty in FG is less than the Qty to be released';
      v_out_err_msg := v_out_err_msg || v_err_msg || '; ';
   END IF;
   
    /*
	 * Remove the last semi-colon in the error detail messages
	 */
	IF v_out_err_msg IS NOT NULL THEN 
		SELECT SUBSTR(trim(v_out_err_msg) , 1, INSTR(trim(v_out_err_msg) , ';', -1)-1)
    		INTO v_out_err_msg 
    		FROM dual;
    		p_out_err_msg := v_out_err_msg;
    		RETURN;
    END IF;
   
    ----- VALIDATION ENDS ---------------------
    
   v_excess_qty :=  v_item_qty - p_bo_Release_qty; 
   
  	 BEGIN
		   SELECT c205_part_number_id , c502_item_price, c500_order_info_id 
		      INTO v_pnum,v_price ,v_order_info_id
		     FROM t502_item_order
		    where c501_order_id = p_orderid 
		    AND c205_part_number_id = p_pnum
		    AND c502_VOID_FL IS NULL 
		    GROUP BY c205_part_number_id,c502_item_price,c500_order_info_id;
			 EXCEPTION WHEN NO_DATA_FOUND THEN
			  v_price := 0;
		END;
   
   /*if the order contains excess qty ,then create new order and sacve the excess quantity for the newly created order*/
   IF v_excess_qty > 0 THEN
   	 gm_wrapper_create_new_BO(p_orderid, p_pnum, p_user_id,v_excess_qty,v_price,p_bo_Release_qty,v_order_info_id); 
   END IF;
   
 /*if the release qty meets the BO qty and the part qty available in FG , then release the order*/
   gm_wrapper_update_existing_bo(p_orderid,p_pnum,p_bo_Release_qty,v_price,p_user_id,v_out_err_msg);
   
   /* PC-3491 Partial back orders are creating Invoice Discrepancies. 
    * Generate the invoice again
    * */
   IF v_invoice_id IS NOT NULL
    THEN
      gm_pkg_ac_invoice_txn.gm_sav_invoice_amount(v_invoice_id,p_user_id);
   END IF;
   
END gm_process_sales_backorder;

/***********************************************************************************
	* Description : Procedure to release the old back orders
	* Author  : karthiks
************************************************************************************/

PROCEDURE gm_wrapper_update_existing_bo(
   p_orderid   			IN       T501_ORDER.C501_ORDER_ID%TYPE,
   p_pnum      			IN       T502_ITEM_ORDER.C205_PART_NUMBER_ID%TYPE,
   p_bo_Release_qty   	IN       T502_ITEM_ORDER.C502_ITEM_QTY%TYPE,
   p_price			  	IN	   	 T502_ITEM_ORDER.c502_item_price%TYPE,
   p_user_id   			IN       T501_ORDER.C501_LAST_UPDATED_BY%TYPE,
   p_errmsg    			OUT      VARCHAR2
   )
AS
v_out_err_msg   CLOB := '';
v_plant_id     T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
v_plant_fl     VARCHAR2(1);
BEGIN
	
	SELECT  get_plantid_frm_cntx()
	 INTO  v_plant_id
	  FROM DUAL;
  
	SELECT get_rule_value(v_plant_id,'BUILDINGPLANTMAP') 
	 INTO v_plant_fl 
	  FROM DUAL;

	/*Update the Order status flag*/
	gm_process_backorder(p_orderid , p_user_id ,v_out_err_msg);
	
	/*Update the total cost for the order*/
   	gm_update_order_total_cost(p_orderid ,p_pnum,p_bo_Release_qty,p_price,p_user_id);
   	
   	p_errmsg := v_out_err_msg;
   	
  --update building order id for PMT-33510 for PBUG-3503  	
  IF v_plant_fl = 'Y' THEN
   gm_pkg_op_storage_building.gm_update_building_order_id(p_orderid, p_user_id, NULL); 
   --  PMT-47807-allocate the priority for the transaction in device
   gm_pkg_op_txn_priority_allocation.gm_allocate_order_priority(p_orderid);
  END IF;
    
END gm_wrapper_update_existing_bo;
  
/***********************************************************************************
* Description : Procedure to create new back orders
* Author  : karthiks
************************************************************************************/

PROCEDURE gm_wrapper_create_new_BO(
   p_orderid   		  IN       t501_order.c501_order_id%TYPE,
   p_pnum   		  IN       T502_ITEM_ORDER.c205_part_number_id%TYPE ,
   p_user_id   		  IN       T501_ORDER.C501_LAST_UPDATED_BY%TYPE,
   p_excess_qty       IN       T502_ITEM_ORDER.C502_ITEM_QTY%TYPE,
   p_price			  IN	   T502_ITEM_ORDER.c502_item_price%TYPE,
   p_bo_Release_qty   IN	   T502_ITEM_ORDER.C502_ITEM_QTY%TYPE,
   p_order_info_id    IN       T502_ITEM_ORDER.C500_order_info_id%TYPE DEFAULT NULL
   )
AS
v_out_boid	  		VARCHAR2(100) := '';
v_excess_qty   		T502_ITEM_ORDER.C502_ITEM_QTY%TYPE := p_excess_qty ;
v_out_log	   		VARCHAR2 (100);
v_parent_order_date T501_ORDER.c501_order_date%TYPE;
v_plant_id     T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
v_plant_fl     VARCHAR2(1);
v_order_usage_fl  VARCHAR2(1);
BEGIN
	 SELECT  get_plantid_frm_cntx()
	 INTO  v_plant_id
	  FROM DUAL;
  
	SELECT get_rule_value(v_plant_id,'BUILDINGPLANTMAP') 
	 INTO v_plant_fl 
	  FROM DUAL;
	  
	  /*Create new Back Order for the excess quantity*/
      gm_save_item_ord_backorder (p_orderid, p_pnum, v_excess_qty, p_price, null, null,v_out_boid);	 
      
       /*Updating unit price,adjustment price,DO unit price for new order*/
	  gm_pkg_op_bo_txn.gm_update_unit_price(v_out_boid,p_user_id);
	  
	    UPDATE t502_item_order
	      SET c502_item_qty     = p_bo_Release_qty
	    WHERE c501_order_id     = p_orderid
	    AND c205_part_number_id = p_pnum
	    AND c502_void_fl IS NULL;
	    
	    /*if it's any pricing discripency then update the order to Hold Order*/
	    gm_pkg_op_do_process_trans.gm_cs_sav_order_hold(v_out_boid,'100020','414553','Hold order,when perform price discrepancy ',p_user_id) ;
	    
	    /*Add Insert's for newly created order*/
	    gm_pkg_op_sav_insert_txn.gm_sav_insert_rev_detail(v_out_boid,106760,93342,p_user_id,NULL);
	    
	    -- PMT-28394 (Job will update the DS order amount)
		-- update the rebate process flag - So, that job will pick the order and calculate discount

		gm_pkg_ac_order_rebate_txn.gm_update_rebate_fl (p_orderid, NULL, NULL);
		gm_pkg_ac_order_rebate_txn.gm_void_discount_order (p_orderid, p_user_id);
		
		 /* Order date should be same as the parent order date for partial orders 
		  */
		BEGIN
		   SELECT c501_order_date,get_rule_value_by_company (c1900_company_id, 'ORDER_USAGE', c1900_company_id) 
		   		INTO v_parent_order_date,v_order_usage_fl
		   	FROM t501_order 
		   	WHERE c501_order_id = p_orderid
		   	AND c501_void_fl IS NULL;
		  EXCEPTION WHEN NO_DATA_FOUND THEN
			v_parent_order_date := NULL;
		   END;
	    
		   UPDATE t501_order
			SET c501_order_date      = NVL(v_parent_order_date,c501_order_date),
			  c501_last_updated_by   = p_user_id,
			  c501_last_updated_date = CURRENT_DATE
			WHERE c501_order_id      = v_out_boid
			AND c501_void_fl        IS NULL;
			
		/* Add a log entry in log table for the partial new back order
		 */
	    gm_update_log (v_out_boid,'Partial Back Order Created from SalesBackOrder screen for the parent order id: ' || p_orderid,'1200',p_user_id,v_out_log);
		
	    /*update the tax amount to the  partially created order if the invoice is already available*/
	    gm_update_sales_tax_amt(p_orderid , v_out_boid , p_pnum , p_user_id);
	    
	    IF v_order_usage_fl = 'Y' THEN
	        --save existing back order qty in t502b
		    gm_pkg_cs_usage_lot_number.gm_upd_backorder_qty (p_order_info_id, v_excess_qty, p_user_id);
		    -- insert into  T500 and T502b tables for new back order
		    gm_pkg_cs_usage_lot_number.gm_sav_order_info (v_out_boid, 'Y', p_user_id) ;
		    -- to carry the lot and ddt number from existing orders
			gm_pkg_cs_usage_lot_number.gm_sync_back_order_usage_lot (v_out_boid, p_order_info_id, p_user_id);
		END IF;
	    
	     --update building order id for PMT-33510 for PBUG-3503  	
		IF v_plant_fl = 'Y' THEN
         gm_pkg_op_storage_building.gm_update_building_order_id(p_orderid, p_user_id, NULL); 
        -- PMT-47807-allocate the priority for the transaction in device 
         gm_pkg_op_txn_priority_allocation.gm_allocate_order_priority(p_orderid);
	    END IF;
END gm_wrapper_create_new_BO;

/***********************************************************************************
* Description : Procedure to update the backorder total cost details
* Author  : karthiks
************************************************************************************/
PROCEDURE gm_update_order_total_cost (
   p_orderid   		  IN       t501_order.c501_order_id%TYPE,
   p_pnum   		  IN       T502_ITEM_ORDER.c205_part_number_id%TYPE ,
   p_bo_Release_qty   IN       T502_ITEM_ORDER.C502_ITEM_QTY%TYPE,
   p_price			  IN	   T502_ITEM_ORDER.c502_item_price%TYPE,
   p_user_id   		  IN       t501_order.c501_last_updated_by%TYPE
)
AS
BEGIN
   UPDATE t501_order
      SET c501_total_cost = (p_bo_Release_qty * p_price),
          c501_last_updated_by = p_user_id,
          c501_last_updated_date = CURRENT_DATE
    WHERE c501_order_id = p_orderid
    AND   c501_void_fl IS NULL;
    
END gm_update_order_total_cost;
/***********************************************************************************
* Description : Procedure to update the unit price
* Author  : karthiks
************************************************************************************/

 PROCEDURE gm_update_unit_price (
	  p_orderid  IN t501_order.c501_order_id%TYPE
    , p_userid   IN t907_shipping_info.c907_last_updated_by%TYPE
	)
	AS
		v_unit_price t502_item_order.c502_item_price%TYPE;
		v_account_price VARCHAR2(50);
		v_adj_code_value VARCHAR2(50);
        
		 CURSOR v_bo_cur IS
	         SELECT t501.c501_order_id ord_id,
                t501.c501_parent_order_id parent_ord_id , t502.c502_item_order_id itemid , t502.c205_part_number_id partid 
                , t502.c502_item_price itemPrice , t502.c502_unit_price_adj_value adjValue , t502.c901_unit_price_adj_code adjCode
                , c502_list_price listPrice
           FROM t501_order t501 , t502_item_order t502
          WHERE (t501.c501_order_id = p_orderid OR t501.c501_parent_order_id = p_orderid )
            AND t501.c501_order_id = t502.c501_order_id
            AND t501.c501_void_fl IS NULL
            AND t502.c502_void_fl IS NULL;
	BEGIN		
		FOR v_bo_index IN v_bo_cur
         LOOP
         	 BEGIN
	         	SELECT get_account_part_pricing(t501.c704_account_id, t502.c205_part_number_id,NULL) 
	         	  INTO v_unit_price
	           	  FROM t502_item_order t502
	           	     , T501_order t501
	          	 WHERE t501.c501_order_id = t502.c501_order_id
	          	   AND t501.c501_order_id = v_bo_index.ord_id
                   AND t502.c502_item_order_id = v_bo_index.itemid
	               AND t502.c502_void_fl IS NULL;
         	 EXCEPTION WHEN OTHERS THEN
		 		v_unit_price := 0;
		 	 END;
		 	 

		 	 UPDATE t502_item_order 
	   		   SET c502_unit_price = v_bo_index.itemPrice
	             , c502_unit_price_adj_value = 0
	             , c901_unit_price_adj_code = NULL
	             , c502_list_price = v_bo_index.listPrice
	             , c502_discount_offered = NULL
	             , c901_discount_type = NULL
	             , c502_system_cal_unit_price = v_unit_price
	             , C502_DO_UNIT_PRICE = v_unit_price
	             , C502_CORP_ITEM_PRICE = v_bo_index.itemPrice
	             , c502_adj_code_value = 0
	         WHERE c502_item_order_id = v_bo_index.itemid;         
         END LOOP;
	END gm_update_unit_price; 
	
/*************************************************************************************************************************
* Description : Procedure to update the tax amount to the  partially created order if the invoice is already available
* Author  : karthiks
**************************************************************************************************************************/
PROCEDURE gm_update_sales_tax_amt (
   p_old_orderid   		  IN       t501_order.c501_order_id%TYPE,
   p_new_orderid   		  IN       t501_order.c501_order_id%TYPE,
   p_pnum 				  IN       T502_ITEM_ORDER.c205_part_number_id%TYPE,
   p_user_id   		  	  IN       t501_order.c501_last_updated_by%TYPE
)
AS
v_invoice_id  		t501_order.C503_Invoice_Id%TYPE;
v_tax_amt 	  		T502_ITEM_ORDER.C502_Tax_Amt%TYPE;
v_vat_rate	  		T502_ITEM_ORDER.C502_Vat_Rate%TYPE;
v_order_desc  		t501_order.C501_Order_Desc%TYPE;
v_ship_dt  			t501_order.C501_Shipping_Date%TYPE;
v_delivery_carrier	t501_order.C501_Delivery_Carrier%TYPE;
v_delivery_mode  	t501_order.C501_Delivery_Mode%TYPE;
v_del_fl  			t501_order.C501_Delete_Fl%TYPE;
v_ship_charge_fl  	t501_order.C501_Ship_Charge_Fl%TYPE;
v_ship_cost  		t501_order.C501_Ship_Cost%TYPE;
v_upd_inv_fl  		t501_order.C501_Update_Inv_Fl%TYPE;
v_reason_typ  		t501_order.C901_Reason_Type%TYPE;
v_parent_ord_id  	t501_order.C901_Parent_Order_Id%TYPE;
v_Rma_Id  			t501_order.C506_Rma_Id%TYPE;
v_Hold_Fl 			t501_order.C501_Hold_Fl%TYPE;
v_Case_Info_Id  	t501_order.C7100_Case_Info_Id%TYPE;
v_Distributor_Id 	t501_order.C501_Distributor_Id%TYPE;
v_AD_ID 			t501_order.C501_AD_ID%TYPE;
v_Vp_Id				t501_order.C501_Vp_Id%TYPE;
v_Customer_Po_Date 	t501_order.C501_Customer_Po_Date%TYPE;
v_Hard_Po_Fl		t501_order.C501_Hard_Po_Fl%TYPE;
v_Tax_Total_Cost 	t501_order.C501_Tax_Total_Cost%TYPE;
v_Ref_Id 			t501_order.C501_Ref_Id%TYPE;
v_Po_Amount 		t501_order.C501_Po_Amount%TYPE;
v_Copay 			t501_order.C501_Copay%TYPE;
v_Cap_Amount 		t501_order.C501_Cap_Amount%TYPE;
v_Order_Hold_Fl		t501_order.C501_Order_Date_His_Fl%TYPE;
v_Hist_Hold_Fl 		t501_order.C501_Hist_Hold_Fl%TYPE;
v_Do_Doc_Upload_Fl 	t501_order.C501_Do_Doc_Upload_Fl%TYPE;
V_Hold_process_Fl 	t501_order.C501_Hold_Order_Process_Fl%TYPE;
v_Hold_PROCESS_DATE t501_order.C501_HOLD_ORDER_PROCESS_DATE%TYPE;

BEGIN
     	 BEGIN
			SELECT C503_Invoice_Id ,
			  C501_Order_Desc,
			  C501_Shipping_Date,
			  C501_Delivery_Carrier,
			  C501_Delivery_Mode,
			  C501_Delete_Fl,
			  C501_Ship_Charge_Fl,
			  C501_Ship_Cost,
			  C501_Update_Inv_Fl,
			  C901_Reason_Type,
			  C901_Parent_Order_Id ,
			  C506_Rma_Id,
			  C501_Hold_Fl,
			  C7100_Case_Info_Id ,
			  C501_Distributor_Id ,
			  C501_AD_ID,
			  C501_Vp_Id ,
			  C501_Customer_Po_Date,
			  C501_Hard_Po_Fl ,
			  C501_Tax_Total_Cost ,
			  C501_Ref_Id,
			  C501_Po_Amount,
			  C501_Copay ,
			  C501_Cap_Amount,
			  C501_Order_Date_His_Fl,
			  C501_Hist_Hold_Fl ,
			  C501_Do_Doc_Upload_Fl ,
			  C501_Hold_Order_Process_Fl,
			  C501_HOLD_ORDER_PROCESS_DATE
			INTO v_invoice_id ,
			  v_order_desc,
			  v_ship_dt,
			  v_delivery_carrier,
			  v_delivery_mode,
			  v_del_fl,
			  v_ship_charge_fl,
			  v_ship_cost,
			  v_upd_inv_fl,
			  v_reason_typ,
			  v_parent_ord_id,
			  v_Rma_Id,
			  v_Hold_Fl,
			  v_Case_Info_Id,
			  v_Distributor_Id,
			  v_AD_ID,
			  v_Vp_Id,
			  v_Customer_Po_Date,
			  v_Hard_Po_Fl,
			  v_Tax_Total_Cost,
			  v_Ref_Id,
			  v_Po_Amount,
			  v_Copay,
			  v_Cap_Amount,
			  v_Order_Hold_Fl,
			  v_Hist_Hold_Fl,
			  v_Do_Doc_Upload_Fl,
			  V_Hold_process_Fl,
			  v_Hold_PROCESS_DATE
			FROM T501_Order
			WHERE C501_Order_Id= p_old_orderid
			AND C501_Void_Fl  IS NULL;
		END;
		
		--update invoice id and other details for new partially created new back order
		UPDATE T501_Order
		SET C503_Invoice_Id            = v_invoice_id,
		  C501_Shipping_Date           = V_Ship_Dt,
		  C501_Delivery_Carrier        = v_delivery_mode ,
		  C501_Delivery_Mode           = V_Delivery_Mode,
		  C501_Update_Inv_Fl           = v_upd_inv_fl,
		  C901_Parent_Order_Id         = v_parent_ord_id ,
		  C7100_Case_Info_Id           = v_Case_Info_Id ,
		  C501_Distributor_Id          = v_Distributor_Id,
		  C501_AD_ID                   = v_AD_ID,
		  C501_Vp_Id                   = V_Vp_Id,
		  C501_Customer_Po_Date        = v_Customer_Po_Date ,
		  C501_Hard_Po_Fl              = v_Hard_Po_Fl,
		  C501_Ref_Id                  = v_Ref_Id,
		  C501_Last_Updated_By         = p_user_id ,
		  C501_Last_Updated_Date       = CURRENT_DATE
		WHERE C501_Order_Id            = P_New_Orderid
		AND C501_Void_Fl              IS NULL;
			
	 	 IF v_invoice_id IS NOT NULL THEN
	 	 	--Get the VAT rate of the old order for the part number
	 	 	SELECT C502_Vat_Rate 
				INTO v_vat_rate 
			FROM T502_Item_Order
			WHERE C501_Order_Id     = p_old_orderid
			AND C205_Part_Number_Id = p_pnum
			AND C502_Void_Fl       IS NULL
			AND ROWNUM              = 1;
			
			--update tax amount and vat rate for the new back order 
			UPDATE T502_Item_Order 
			SET C502_Vat_Rate = v_vat_rate  , 
				C502_Tax_Amt = ROUND(C502_ITEM_QTY * NVL(C502_ITEM_PRICE,0) * (NVL(v_vat_rate,0)/100),2)
			WHERE C501_Order_Id = p_new_orderid
			AND C205_Part_Number_Id = p_pnum
			AND C502_Void_Fl       IS NULL;
			
			--Get the tax amount of the new order for the part number
			SELECT  C502_Tax_Amt
			 	INTO v_tax_amt
			FROM T502_Item_Order
			WHERE C501_Order_Id     = p_new_orderid
			AND C205_Part_Number_Id = p_pnum
			AND C502_Void_Fl       IS Null
			AND ROWNUM              =1;
			
			--Update tax amount for the old order 
			UPDATE T502_Item_Order 
			SET C502_Tax_Amt = ROUND(C502_Tax_Amt - v_tax_amt,2)		  	
			WHERE C501_Order_Id = p_old_orderid
			AND C205_Part_Number_Id = p_pnum
			AND C502_Void_Fl       IS NULL;
			
	 	 END IF;
    
END gm_update_sales_tax_amt;

END gm_pkg_op_bo_txn;
/