--@"C:\Database\Packages\Operations\gm_pkg_op_lot_track_txn.BDY"

CREATE OR REPLACE
PACKAGE BODY gm_pkg_op_lot_track_txn
IS

/*****************************************************************************************************************************
	* Author      : APrasath
	* Description : Update the lot information from TRG_T5060_CONTROL_NUMBER_INV.trg while inserting t5060_control_number_inv 
	* based on part number, control number, warehouse, Ref id and Ref type 
******************************************************************************************************************************/
PROCEDURE gm_sav_lot_inventory (
    p_part_num				IN   t2550_part_control_number.c205_part_number_id%TYPE
  , p_control_number		IN   t2550_part_control_number.c2550_control_number%TYPE
  , p_warehouse_type		IN   t2550_part_control_number.c901_last_updated_warehouse_type%TYPE
  , p_qty					IN   t5060_control_number_inv.c5060_qty%TYPE
  , p_last_trans_id			IN   t2550_part_control_number.c2550_last_updated_trans_id%TYPE
  , p_last_trans_type       IN   t2550_part_control_number.c901_last_updated_trans_type%TYPE
  , p_ref_type 				IN   t2550_part_control_number.c901_ref_type%TYPE
  , p_ref_id 				IN   t2550_part_control_number.c2550_ref_id%TYPE
  , p_user					IN   t101_user.c101_user_id%TYPE
  , p_company_id			IN   t2550_part_control_number.c1900_company_id%TYPE
  , p_plant_id				IN   t2550_part_control_number.c5040_plant_id%TYPE
  , p_shipto                IN   t501_order.c501_ship_to%TYPE
  , p_shiptoid              IN   t501_order.c501_ship_to_id%TYPE 
  , p_shippeddate           IN   t501_order.c501_shipping_date%TYPE
  , p_customerpo            IN   t501_order.c501_customer_po%TYPE
)
AS
 v_lot_status t2550_part_control_number.c2550_lot_status%TYPE;
 v_rule_grp t906_rules.c906_rule_grp_id%TYPE;
 v_warehouse_type t2551_part_control_number_log.c901_last_updated_warehouse_type%TYPE;
 v_txn_type t2550_part_control_number.c901_last_updated_trans_type%TYPE;
BEGIN
	-- Identifying rule group based on transaction qty 
	SELECT  (CASE WHEN (p_qty < 0) THEN 'LOTSTATUSNEG' 
      WHEN (p_qty > 0) THEN 'LOTSTATUSPOS' END) INTO v_rule_grp 
	   FROM DUAL;
	--Getting lot status based on warehouse and transaction type
	BEGIN	 
	SELECT c906_rule_value INTO v_lot_status from T906_RULES 
	    WHERE c906_rule_grp_id = v_rule_grp
	     AND c906_rule_id=p_warehouse_type 
	     AND c901_rule_type=p_last_trans_type 
	     AND c906_void_fl IS NULL;
    EXCEPTION WHEN NO_DATA_FOUND THEN
   	v_lot_status := NULL;
    END;
   
   v_warehouse_type := p_warehouse_type;
   v_txn_type := p_last_trans_type;
   --Below changes used to get status of specfic transaction type Direct order and QNSC
   IF v_lot_status = '999999' THEN     
    v_txn_type := get_transaction_type(p_last_trans_id);    
    	BEGIN	 
		SELECT c906_rule_value INTO v_lot_status from T906_RULES 
		    WHERE c906_rule_grp_id = v_rule_grp
		     AND c906_rule_id=p_warehouse_type 
		     AND c901_rule_type=v_txn_type 
		     AND c906_void_fl IS NULL;
	    EXCEPTION WHEN NO_DATA_FOUND THEN
	   	v_lot_status := NULL;
	    END;	    
   END IF;
   --while decreasing lot qty, We increase warehouse qty for some of the transaction type
   IF v_rule_grp = 'LOTSTATUSNEG' THEN
	   BEGIN
	    SELECT c906_rule_value INTO v_warehouse_type from T906_RULES 
		    WHERE c906_rule_grp_id = 'LOTWHNEG'
		     AND c906_rule_id=p_warehouse_type 
		     AND c901_rule_type=v_txn_type 
		     AND c906_void_fl IS NULL;
	   EXCEPTION WHEN NO_DATA_FOUND THEN
	   	v_warehouse_type := p_warehouse_type;
	   END;      
  END IF;    
	   		
  -- Update lot status and transaction information when lot status is available	      
  IF v_lot_status IS NOT NULL THEN 	     
  -- If same Part number, Control number, warehouse, ref_id and ref_type 
  UPDATE t2550_part_control_number t2550
    SET 
      c2550_lot_status = v_lot_status,	
      c901_last_updated_warehouse_type = v_warehouse_type,
      c2550_last_updated_trans_id = p_last_trans_id,
      c901_last_updated_trans_type = p_last_trans_type,
      c2550_last_updated_trans_date = sysdate,
      c704_account_nm = DECODE(p_ref_type,103923,get_account_name(p_ref_id),NULL),
      c701_distributor_name = DECODE(p_ref_type,103922,get_distributor_name(p_ref_id),NULL),
      c901_ref_type = p_ref_type,
      c2550_ref_id = p_ref_id,
      c2550_last_updated_by = p_user, 
      c2550_last_updated_date = sysdate,
      c1900_company_id = p_company_id ,
      c5040_plant_id = p_plant_id,
      c901_ship_to_type = p_shipto,
      c5060_ship_to_id = p_shiptoid,
      c5060_shipped_dt = p_shippeddate,
      c5060_customer_po = p_customerpo
   WHERE t2550.c205_part_number_id = p_part_num
     AND t2550.c2550_control_number = p_control_number;
 END IF;   	
 
  
END gm_sav_lot_inventory;


/*********************************************************************************************************************************************************
	* Author      : APrasath
	* Description : Insert t2551_part_control_number_log while updating the lot status and transaction information from  trg_t2550_part_control_number.trg
**********************************************************************************************************************************************************/  
PROCEDURE gm_sav_lot_inventory_log (
		p_control_number_inv_id		IN   t2550_part_control_number.c2550_part_control_number_id%TYPE
	  , p_part_num					IN   t2551_part_control_number_log.c205_part_number_id%TYPE
	  , p_control_number			IN   t2551_part_control_number_log.c2550_control_number%TYPE
	  , p_lot_status			    IN   t2551_part_control_number_log.c2550_lot_status%TYPE
	  , p_warehouse_type			IN   t2551_part_control_number_log.c901_last_updated_warehouse_type%TYPE	  
	  , p_last_trans_id				IN   t2551_part_control_number_log.c2550_last_update_trans_id%TYPE
	  , p_created_by				IN   t2551_part_control_number_log.c2551_created_by%TYPE
	  , p_last_transaction_type     IN   t2551_part_control_number_log.c901_last_updated_trans_type%TYPE
	  , p_last_updated_trans_date   IN   t2551_part_control_number_log.c2550_last_updated_trans_date%TYPE
	  , p_ref_id 					IN   t2551_part_control_number_log.c2550_ref_id%TYPE
      , p_ref_type 					IN   t2551_part_control_number_log.c901_ref_type%TYPE
	  , p_account_nm                IN   t2551_part_control_number_log.c704_account_nm%TYPE
	  , p_distributor_name          IN   t2551_part_control_number_log.c701_distributor_name%TYPE
	  , p_company_id			    IN   t2551_part_control_number_log.c1900_company_id%TYPE
  	  , p_plant_id				    IN   t2551_part_control_number_log.c5040_plant_id%TYPE	  
	)
AS 
BEGIN      			
             
	       INSERT INTO t2551_part_control_number_log
		        (
		            c2551_log_id,  c2550_part_control_number_id,c205_part_number_id, c2550_control_number
		          , c2550_lot_status, c901_last_updated_warehouse_type, c2551_created_by
		          , c2551_created_date, c2550_last_update_trans_id, c901_last_updated_trans_type
		          , c2550_last_updated_trans_date, c2550_ref_id, c901_ref_type
		          , c704_account_nm , c701_distributor_name, c1900_company_id , c5040_plant_id
		        )
		        VALUES
		        (
		            s5061_control_number_inv_log.nextval, p_control_number_inv_id, p_part_num, p_control_number
		          , p_lot_status, p_warehouse_type, p_created_by
		          , SYSDATE,p_last_trans_id ,p_last_transaction_type
		          , p_last_updated_trans_date, p_ref_id, p_ref_type 
		          , p_account_nm , p_distributor_name , p_company_id,p_plant_id
		        ); 
	   			
END gm_sav_lot_inventory_log;

/*******************************************************
* Description : function to get transaction type for the passing transaction id
* Author    : APrasath
*******************************************************/
FUNCTION get_transaction_type (
 p_last_transaction_id VARCHAR)
    RETURN NUMBER
IS
    v_trans_type NUMBER;
BEGIN   
      SELECT transtype INTO v_trans_type
		FROM
		  (SELECT NVL(c901_order_type,2521) transtype
		  FROM t501_order
		  WHERE c501_order_id =p_last_transaction_id
		  AND c501_void_fl   IS NULL
		  UNION
		  SELECT c504_type transtype
		  FROM t504_consignment
		  WHERE c504_consignment_id=p_last_transaction_id
		  AND c504_void_fl        IS NULL
		  UNION
		  SELECT c412_type transtype
		  FROM t412_inhouse_transactions
		  WHERE c412_inhouse_trans_id=p_last_transaction_id
		  AND c412_void_fl          IS NULL
		  );      
    RETURN v_trans_type;
EXCEPTION
WHEN NO_DATA_FOUND THEN
    RETURN NULL;
END get_transaction_type;

/*********************************************************************************************************************************************************
* Author      : Karthik
* Description : This Procedure is used to insert the entry in T5060 table for the control number
	 				shipping out from Set Consignment
**********************************************************************************************************************************************************/ 

PROCEDURE gm_pkg_op_lot_track_for_sets(
p_txn_id IN T504_Consignment.C504_Consignment_Id%TYPE ,
P_userid IN T101_User.C101_User_Id%TYPE
)
AS 
 v_company_id  t1900_company.c1900_company_id%TYPE;
 v_flag VARCHAR2(1);
 
 CURSOR cur_consign
   IS
      SELECT   t505.c205_part_number_id ID,
               t505.c901_warehouse_type whtype
          FROM T504_Consignment t504 ,
          	   t505_item_consignment t505 ,
          	   T205d_Part_Attribute t205d
         WHERE t505.c504_consignment_id = p_txn_id
           AND t504.C504_Consignment_Id = T505.C504_Consignment_Id
           AND t504.C504_Void_Fl IS NULL
           AND TRIM (t505.c505_control_number) IS NOT NULL
           AND t505.c505_void_fl IS NULL
           AND c505_item_qty > 0
           AND t505.c205_part_number_id = t205d.C205_Part_Number_Id
           AND t205d.C901_Attribute_Type = 104480 --Lot Track Enable Flag
           AND t205d.C1900_Company_Id      = v_company_id
           AND t205d.C205d_Attribute_Value = 'Y'
           AND t205d.c205d_void_fl        IS NULL
      GROUP BY t505.c205_part_number_id,t505.c901_warehouse_type;
		
BEGIN
		 SELECT get_compid_frm_cntx()
     	  INTO v_company_id
      	  FROM DUAL;
      		
      	 SELECT get_rule_value(v_company_id,'LOT_TRACK') INTO v_flag FROM DUAL; 
	   
		 IF v_flag IS NULL OR v_flag <> 'Y' 
		  THEN
			 RETURN;
		  END IF;
		  
		 --Sets Shipout--------------------
		 FOR rad_val IN cur_consign 
	      LOOP   
		 	gm_pkg_op_lot_track.gm_lot_track_main(rad_val.ID,
	                               1,
	                               p_txn_id,
	                               P_userid,
	                               90800, -- FG  Qty
	                               4302,-- Minus
	                               4311 -- Consignment
	                               );
	                               
	    	gm_pkg_op_lot_track.gm_lot_track_main(rad_val.ID,
	                           1,
	                           p_txn_id,
	                           P_userid,
	                           90800, -- FG  Qty
	                           4301,-- PLUS
	                           4311 -- Consignment
	                           );
	     END LOOP;
END gm_pkg_op_lot_track_for_sets;

/*********************************************************************************************************************************************************
* Author      : Karthik
* Description : This Procedure is used to insert the entry in T5060 table for the control number
					returning from Set Consignment
**********************************************************************************************************************************************************/

PROCEDURE gm_pkg_op_lot_track_for_returns(
p_txn_id IN t507_returns_item.c506_rma_id%TYPE , 
P_userid IN T101_User.C101_User_Id%TYPE
)
AS 
v_company_id  t1900_company.c1900_company_id%TYPE;
v_flag VARCHAR2(1);

CURSOR cur_credit
IS
	SELECT t507.c205_part_number_id ID,
				t507.c901_type itype
		FROM T506_Returns t506 ,
			 t507_returns_item t507 ,
			 T205d_Part_Attribute t205d
	   WHERE t507.c506_rma_id = p_txn_id 
	     AND t506.C506_Rma_Id = t507.C506_Rma_Id
	     AND t506.C506_Void_Fl IS NULL
	   	 AND t507.c507_status_fl = 'C'
         AND t507.c205_part_number_id = t205d.C205_Part_Number_Id
       	 AND t205d.C901_Attribute_Type = 104480 --Lot Track Enable Flag
       	 AND t205d.C1900_Company_Id      = v_company_id
       	 AND t205d.C205d_Attribute_Value = 'Y'
       	 AND c205d_void_fl IS NULL
	GROUP BY t507.c205_part_number_id, t507.c901_type;
      
BEGIN
     SELECT get_compid_frm_cntx()
     	  INTO v_company_id
      	  FROM DUAL;
      	  
     SELECT get_rule_value(v_company_id,'LOT_TRACK') INTO v_flag FROM DUAL; 
	   
	 IF v_flag IS NULL OR v_flag <> 'Y' 
	  THEN
		 RETURN;
	  END IF;
	   
     --Consignment RETURNS--------------------
        FOR rad_val IN cur_credit 
	      LOOP   
      		gm_pkg_op_lot_track.gm_lot_track_main(rad_val.ID,
                               1,
                               p_txn_id,
                               P_userid,
                               4000339, -- FS  Qty
                               4302,-- Minus
                               4312   -- Return
                               );
                               
        	gm_pkg_op_lot_track.gm_lot_track_main(rad_val.ID,
                               1,
                               p_txn_id,
                               P_userid,
                               90812, -- returns_hold Qty
                               4301,-- PLUS
                               4312   -- Return                               
                               );
        END LOOP;                               
END gm_pkg_op_lot_track_for_returns;

END gm_pkg_op_lot_track_txn;
/