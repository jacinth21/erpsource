/* Formatted on 2011/04/28 10:36 (Formatter Plus v4.8.0) */
--@"c:\database\packages\operations\gm_pkg_op_backorder_txn.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_backorder_txn
IS
--
  
	/**********************************************************
	* Description : Procedure to save the Back Order Details
	* Author  : arajan
	**********************************************************/
	PROCEDURE gm_save_backorder_email (
		        p_rep_id  		IN      t501_order.c703_sales_rep_id%TYPE,
		        p_bo_string  	IN      CLOB,
		        p_user_id	 	IN      t501a_order_attribute.c501a_created_by%TYPE)
	AS
	
		CURSOR v_bo_dtls
			IS
				SELECT T501.C703_SALES_REP_ID REP_ID,GET_REP_NAME(T501.C703_SALES_REP_ID) SALES_REP_NAME,T501.C501_ORDER_ID ID, 
				 	GET_ACCOUNT_NAME (T501.C704_ACCOUNT_ID) ANAME, GET_DIST_REP_NAME (T501.C703_SALES_REP_ID) REPDISTNM
				 	, TO_CHAR (T501.C501_ORDER_DATE, get_rule_value ('DATEFMT', 'DATEFORMAT')) DT, T501.C501_TOTAL_COST + NVL (T501.C501_SHIP_COST, 0) SALES, GET_PARTNUMBER_PO_PEND_QTY (T502.C205_PART_NUMBER_ID) POPEND
				  	, T501.C704_ACCOUNT_ID ACCID, T501.C501_PARENT_ORDER_ID MASTER_ID, T502.C205_PART_NUMBER_ID NUM
				  	, T502.C502_ITEM_QTY QTY, T502.C502_ITEM_PRICE PRICE, get_partnum_desc (T502.C205_PART_NUMBER_ID) DSC, T501A.C501A_ATTRIBUTE_VALUE attr_val, T501A.C501A_ORDER_ATTRIBUTE_ID attr_id
				   FROM T501_ORDER T501, T502_ITEM_ORDER T502, T501A_ORDER_ATTRIBUTE t501a
				WHERE T501.C501_ORDER_ID         = T502.C501_ORDER_ID
				    AND (T501.c901_ext_country_id IS NULL
				    	OR T501.c901_ext_country_id   IN(
				         SELECT country_id FROM v901_country_codes)
				    )
				    AND T501.C901_ORDER_TYPE  = 2525 -- BackOrder
				    AND T501.C501_DELETE_FL  IS NULL
				    AND T501.C501_VOID_FL    IS NULL
				    AND T502.C502_VOID_FL    IS NULL 
				 	AND T501.C501_STATUS_FL   = 0
					AND T501.C501_ORDER_ID IN (SELECT TOKEN FROM V_IN_LIST)
					AND T501A.C501_ORDER_ID(+) = T501.C501_ORDER_ID
					AND T501A.C501A_VOID_FL IS NULL
		 			AND T501A.C901_ATTRIBUTE_TYPE (+)=4000764 -- Sales BackOrder Email Time
				ORDER BY T501.C703_SALES_REP_ID,T502.C205_PART_NUMBER_ID, T501.C501_ORDER_DATE ;
	
	BEGIN
		
		my_context.set_my_inlist_ctx (p_bo_string);
		
		FOR v_rec IN v_bo_dtls
	    LOOP
	
	        GM_PKG_CM_ORDER_ATTRIBUTE.GM_SAV_ORDER_ATTRIBUTE( v_rec.id
														  , 4000764  -- Sales BackOrder Email Time
	                                           			  , TO_CHAR (SYSDATE,GET_RULE_VALUE('DATEFMT','DATEFORMAT')||' HH:MI:SS')
	                                           			  , v_rec.attr_id
	                                           			  , p_user_id );
	    END LOOP;
		
	END gm_save_backorder_email;

	/*******************************************************************************
	* Description : Procedure to convert Back Order to Bill Only Sales Consignment
	* Author  : arajan
	********************************************************************************/
	PROCEDURE gm_upd_bo_to_bill_only (
		        p_order_id  		IN      t501_order.c501_order_id%TYPE,
		        p_user_id		  	IN      t501_order.c501_last_updated_by%TYPE)
	AS
	
	v_status_fl    t501_order.c501_status_fl%TYPE;
	v_ord_type	   t501_order.c901_order_type%TYPE;
	v_void_fl	   t501_order.c501_void_fl%TYPE;
	v_inv_fl	   t501_order.c501_update_inv_fl%TYPE;
	v_party_id	   t810_posting_txn.c810_party_id%TYPE;
	v_dist_id	   t701_distributor.c701_distributor_id%TYPE;
	v_dist_name    t701_distributor.c701_distributor_name%TYPE;
	v_acc_id       t704_account.c704_account_id%TYPE;
	v_location_id  t704_account.c5052_location_id%TYPE;

	CURSOR cur_sales
	IS
		SELECT c205_part_number_id pnum, c502_item_qty qty, c502_item_price price
		  FROM t502_item_order
		 WHERE c501_order_id = p_order_id AND C502_VOID_FL IS NULL;
		 
	BEGIN
		
		SELECT t501.c501_status_fl, t501.c901_order_type, t501.c501_void_fl, t501.c501_update_inv_fl
			 , get_distributor_id (t501.c703_sales_rep_id), get_distributor_name(get_distributor_id (t501.c703_sales_rep_id))
			 , t704.c704_account_id, t704.c5052_location_id
		  INTO v_status_fl, v_ord_type, v_void_fl, v_inv_fl, v_dist_id, v_dist_name, v_acc_id, v_location_id
		   FROM t501_order t501, t704_account t704
		   WHERE t501.c501_order_id = p_order_id 
		      AND t704.c704_account_id = t501.c704_account_id AND t704.c704_void_fl IS NULL
		FOR UPDATE;
		--
		-- Throw error if the status flag is not 0 or 1
		IF v_status_fl > 1
		THEN
			GM_RAISE_APPLICATION_ERROR('-20999','189','');
		END IF;
	
		--
		-- Throw error if the order is voided
		IF v_void_fl IS NOT NULL
		THEN
			GM_RAISE_APPLICATION_ERROR('-20999','190',''); 
		END IF;
	
		--
		-- Throw error if the posting has already happened
		IF v_inv_fl IS NOT NULL
		THEN
			GM_RAISE_APPLICATION_ERROR('-20999','191','');
		END IF;
	
		--
		-- To get Party Id for Ledger Posting
		SELECT get_acct_party_id (p_order_id, 'S')
		  INTO v_party_id
		  FROM DUAL;
	
		-- Updating Order type to -'Bill Only - From Sales Consignments' and status flag to 3
		UPDATE t501_order
		   SET c901_order_type = 2532
			 , c501_status_fl = 3
			 , c501_update_inv_fl = 1
			 , c501_shipping_date = SYSDATE
			 , c501_last_updated_by = p_user_id
			 , c501_last_updated_date = SYSDATE
		 WHERE c501_order_id = p_order_id;
		
		 v_ord_type := '2532';
		        -- update Account warehouse Qty if location id available in t704 otherwise update Field sales Qty. 
				-- 4302 Minus
				-- 102900 - Consignment
				-- 4000337 - Bill Only Consignment
				IF v_acc_id IS NOT NULL AND v_location_id IS NOT NULL THEN
					gm_pkg_op_inv_field_sales_tran.gm_update_fs_inv(p_order_id, 'T501', v_acc_id, 102900, 4302, 4000337, p_user_id ,v_ord_type);
				ELSE
					gm_pkg_op_inv_field_sales_tran.gm_update_fs_inv(p_order_id, 'T501', v_dist_id, 102900, 4302, 4000337, p_user_id);
				END IF;				
		--
		-- Posting from Sales Consignment to COGS[48085]
		FOR rad_val IN cur_sales
		LOOP
			gm_save_ledger_posting (48085
								  , CURRENT_DATE
								  , rad_val.pnum
								  , v_party_id
								  , p_order_id
								  , rad_val.qty
								  , rad_val.price
								  , p_user_id
								   );
		--
		
		END LOOP;
		
		-- void the shipping record
		UPDATE t907_shipping_info
			SET c907_void_fl = 'Y'
				, c907_last_updated_by = p_user_id
				, c907_last_updated_date = SYSDATE
		WHERE c907_ref_id = p_order_id
			AND c907_void_fl is null;
			
		-- update the control number of the part
		UPDATE t502_item_order
				SET c502_control_number = 'NOC#'
			WHERE c501_order_id = p_order_id
				AND c901_type = '50300' --Sales Consignment
				AND c502_void_fl IS NULL
				AND c502_delete_fl IS NULL;
	END gm_upd_bo_to_bill_only;
	
/*******************************************************************************
	* Description : Procedure to convert Back Order to Bill Only Return Sales Consignment
	* Author      : rajan
	********************************************************************************/
PROCEDURE gm_upd_bo_to_bill_only_return(
        p_txn_id		 IN 	  t501_order.c501_order_id%TYPE
      , p_comments		 IN 	  t907_cancel_log.c907_comments%TYPE
	  , p_cancelreason	 IN 	  t907_cancel_log.c901_cancel_cd%TYPE
	  , p_canceltype	 IN 	  t901_code_lookup.c902_code_nm_alt%TYPE
	  , p_userid		 IN 	  t102_user_login.c102_user_login_id%TYPE
	  , p_out_msg		OUT	      VARCHAR2 )
AS
   v_out_ra_id        VARCHAR2(20);
   v_rule_comments    VARCHAR2(200);
   v_canceltypecode   t907_cancel_log.c901_type%TYPE;
   v_ordercount       NUMBER;
   v_pnum             t502_item_order.C205_PART_NUMBER_ID%TYPE;
   v_qty              t502_item_order.c502_item_qty%TYPE;
   v_itmtyp           t506_returns.c506_type%TYPE;
   v_accid            t501_order.c704_account_id%TYPE;
   v_unitprice        NUMBER(15,2);
   v_loanerinputstr   VARCHAR2(4000);
   v_adjcode          NUMBER;
   v_ctrl             t502_item_order.c502_control_number%TYPE;
   v_orgctrl          t502_item_order.c502_control_number%TYPE;
   v_cancelid         t907_cancel_log.c907_cancel_log_id%TYPE;
   v_out_inv_id       t503_invoice.c503_invoice_id%TYPE;
   v_price            t502_item_order.c502_item_price%TYPE;
   v_orderinputstr    VARCHAR2(4000);
   v_comp_date_fmt    VARCHAR2(100);
   v_unitprice_adj    t502_item_order.c502_unit_price_adj_value%TYPE;
  
BEGIN

      SELECT NVL(get_compdtfmt_frm_cntx(), get_rule_value('DATEFMT','DATEFORMAT')) 
        INTO v_comp_date_fmt
        FROM dual; 

	  --SELECT count(1) INTO v_ordercount
        --FROM t501_order 
       --WHERE c501_order_id = p_txn_id 
         --AND c501_order_date BETWEEN TRUNC (CURRENT_DATE,'MONTH') AND TRUNC (CURRENT_DATE)
         --AND c501_void_fl IS NULL;
      
  gm_pkg_common_cancel.gm_sav_cancel_log(NULL,p_txn_id,p_comments,p_cancelreason,'90217',p_userid);   --90217  - Void Sales Order
   
      BEGIN
		  SELECT max(c907_cancel_log_id) INTO v_cancelid
		    FROM t907_cancel_log
		   WHERE c907_ref_id = p_txn_id
		     AND C907_VOID_FL IS NULL; 
	   EXCEPTION WHEN NO_DATA_FOUND     -- NO DATA FOUND EXCEPTION
	   THEN
   		v_cancelid := '';
   	   END; 
   	   
 -- IF v_ordercount > 0 THEN
         
  	 --gm_pkg_op_order_master.gm_cs_sav_void_order(p_txn_id,p_cancelreason,v_cancelid, p_userid);  --This procedure used to current month order to update to bill only retun to void the backorder
  
  --ELSE
  
     gm_pkg_op_backorder_txn.gm_upd_bo_to_bill_only(p_txn_id,p_userid);  -- This procedure to convert Back Order to Bill Only Sales Consignment
       
        BEGIN
       SELECT t502.c205_part_number_id, t502.c502_item_qty, t502.c502_control_number,
              t502.c502_control_number,'3300',nvl(t502.c502_item_price,0),t501.c704_account_id,        -- 3300 - Sales - Items
              nvl(t502.c502_unit_price,0),nvl(t502.c901_unit_price_adj_code,0),t502.c502_unit_price_adj_value
         INTO v_pnum, v_qty, v_ctrl, v_orgctrl, v_itmtyp,v_price,v_accid, v_unitprice, v_adjcode,v_unitprice_adj
         FROM t501_order t501, t502_item_order t502 
        WHERE t501.C501_ORDER_ID = t502.C501_ORDER_ID 
          AND t502.C502_VOID_FL IS NULL 
          AND t501.C501_VOID_FL IS NULL
          AND t501.c501_order_id = p_txn_id;  
    EXCEPTION WHEN NO_DATA_FOUND     -- NO DATA FOUND EXCEPTION  
         THEN
	   		v_pnum := NULL;
	   		v_qty := NULL;
	   		v_ctrl := NULL;
	   		v_orgctrl := NULL;
	   		v_itmtyp := NULL;
	   		v_price := NULL;
	   		v_accid := NULL;
	   		v_unitprice := NULL;
	   		v_adjcode := NULL;
	   		v_unitprice_adj := NULL;
   	     END;    
          
         v_loanerinputstr := v_pnum||','||v_qty||','||v_ctrl||','||v_orgctrl||','||v_itmtyp||','||v_price||','||v_unitprice||','||v_unitprice_adj||','||v_adjcode||'|';

          --sample data --  v_loanerinputstr >>>111 658.969S,1,NOC#,NOC#,50300,0,0,0,|
                                                                                                                                                         
         gm_pkg_op_return.gm_op_sav_order_return(v_itmtyp, p_cancelreason, v_accid, p_comments,'',p_txn_id,p_userid,'','',v_loanerinputstr,v_out_ra_id);   --This Procedure will be called to initiate order return.
         
         gm_pkg_op_return.gm_sav_order_loaner_return(v_out_ra_id,v_loanerinputstr,TO_CHAR(CURRENT_DATE,v_comp_date_fmt),1,p_comments,p_txn_id,p_userid);   --This procedure used to save order loaner return - to process the RA automatically. 
         
         gm_pkg_op_return.gm_sav_return_reconf_lot_num(v_out_ra_id,'101723',v_loanerinputstr,'',p_userid,'93343');    --101723 - Control Number Attribute, 93343 - pick, Procedure to Save the lot number in t507 table for validation purpose.
         
         v_orderinputstr := v_pnum||'^'||v_qty||'^'||v_ctrl||'^'||v_price||'^'||v_itmtyp||'^'||v_unitprice||'^'||v_unitprice_adj||'^'||v_adjcode||'|';
         
         gm_save_credit_order(v_out_ra_id,CURRENT_DATE,p_comments,p_userid,v_orderinputstr,v_out_inv_id,'Y');     -- This procedure called to created order return process.
         
         	UPDATE t506_returns                                            
			   SET c506_status_fl = 1
				 , c506_return_date = CURRENT_DATE
				 , c506_last_updated_by = p_userid
				 , c506_last_updated_date = CURRENT_DATE
			 WHERE c506_rma_id = v_out_ra_id;
			--
			UPDATE t507_returns_item                           --- updated the status flag completed in t507.  
			   SET c507_status_fl = 'C'
				 , c507_control_number = 'NOC#'
			 WHERE c506_rma_id = v_out_ra_id AND c507_status_fl = 'R';
  --END IF;

END gm_upd_bo_to_bill_only_return;	

	
END gm_pkg_op_backorder_txn;
/