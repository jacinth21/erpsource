--@"C:\Database\Packages\Operations\gm_pkg_op_cpc_rpt.bdy";

create or replace
PACKAGE BODY gm_pkg_op_cpc_rpt
IS
--

/*********************************************************
	* Description : This procedure to fetch Cpc Name and Id
	* 
	*********************************************************/
  PROCEDURE gm_fch_cpc_data(
  p_out_cur_dtls OUT TYPES.cursor_type
	) 
  AS
	  v_company_id     NUMBER;
	  v_plant_id       NUMBER;
  BEGIN
	  
  SELECT get_compid_frm_cntx(), get_plantid_frm_cntx()
  INTO v_company_id, v_plant_id 
  FROM DUAL;
	  
  OPEN p_out_cur_dtls FOR
  SELECT c704b_account_cpc_map_id CODEID 
        ,c704b_cpc_name CODENM
    FROM t704b_account_cpc_mapping 
   WHERE c704b_void_fl IS NULL 
     AND C1900_COMPANY_ID = v_company_id 
     AND C5040_PLANT_ID = v_plant_id  
ORDER BY CODENM;
  END gm_fch_cpc_data;
  
  /*********************************************************
	* Description : This procedure to fetch Cpc Items 
	* 
	*********************************************************/
  PROCEDURE gm_fch_cpc_item(
   p_cpc_id	    IN t704b_account_cpc_mapping.c704b_account_cpc_map_id%TYPE,
   p_out_cur_dtls OUT TYPES.cursor_type
	) 
  AS
	  v_company_id     NUMBER;
	  v_plant_id       NUMBER;
  BEGIN
	  
  SELECT get_compid_frm_cntx(), get_plantid_frm_cntx()
  INTO v_company_id, v_plant_id 
  FROM DUAL;	  
	  
  OPEN p_out_cur_dtls FOR
  SELECT c704b_account_cpc_map_id cpcid 
        ,c704b_cpc_name cpcname
        ,c704b_cpc_address cpcaddr
        ,c704b_cpc_barcode_lbl_ver cpcbarcode
        ,c704b_cpc_final_cpc_ver cpcfinal
        ,DECODE(c704b_active_fl,'Y','80130','N','80131',c704b_active_fl)  activefl
    FROM t704b_account_cpc_mapping  
   WHERE c704b_account_cpc_map_id = p_cpc_id
     AND c1900_company_id = v_company_id
     AND c5040_plant_id = v_plant_id
     AND c704b_void_fl IS NULL;
END gm_fch_cpc_item; 
     
/*********************************************************
	* Description : This Funtion to get the count of Cpc Items 
	* 
*********************************************************/
FUNCTION get_cpc_count(
   p_cpc_name	t704b_account_cpc_mapping.c704b_cpc_name%TYPE )
  RETURN NUMBER
IS 
	  v_count NUMBER;
	  v_company_id     NUMBER;
	  v_plant_id       NUMBER;
BEGIN
  SELECT get_compid_frm_cntx(), get_plantid_frm_cntx()
  INTO v_company_id, v_plant_id 
  FROM DUAL;
  
  BEGIN
     SELECT COUNT(c704b_cpc_name) 
       INTO v_count
       FROM t704b_account_cpc_mapping 
      WHERE c704b_cpc_name = p_cpc_name 
        AND c704b_void_fl IS NULL
        AND c1900_company_id = v_company_id
        AND c5040_plant_id = v_plant_id;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    RETURN 0;
  END;
  RETURN v_count;
END get_cpc_count;
  
END gm_pkg_op_cpc_rpt;
/