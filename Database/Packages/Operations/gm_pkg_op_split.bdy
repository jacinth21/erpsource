/* Formatted on 2012/03/19 22:09 (Formatter Plus v4.8.0) */
--@"C:\Database\Packages\Operations\gm_pkg_op_split.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_split
IS
   PROCEDURE gm_sav_do_split (
      p_order_id   IN   t501_order.c501_order_id%TYPE,
      p_user_id    IN   t501_order.c501_created_by%TYPE
   )
   AS
-- Get all the distinct project id's that have external vendors for the given order id
      CURSOR part_num_cur
      IS
         SELECT DISTINCT c202_project_id project_id
                    FROM t502_item_order t502,
                    	 t501_order t501 ,
                         V205_PARTS_WITH_EXT_VENDOR v205
                   WHERE t502.c501_order_id = p_order_id    --'1149-120306-12'
                  	 AND t501.c501_order_id = t502.c501_order_id
                     AND v205.c205_part_number_id = t502.c205_part_number_id
                     AND t501.c1900_company_id    = v205.c1900_company_id
                     AND t502.c502_void_fl IS NULL
                ORDER BY c202_project_id;

      CURSOR back_order_cur
      IS
         SELECT   t501.c501_order_id order_id, c202_project_id project_id
             FROM t502_item_order t502,
                  V205_PARTS_WITH_EXT_VENDOR v205,
                  t501_order t501
            WHERE t501.c501_parent_order_id = p_order_id    --'1149-120306-12'
              AND t501.c901_order_type = 2525                    -- BACK ORDER
              AND t501.c501_order_id = t502.c501_order_id
              AND v205.c205_part_number_id = t502.c205_part_number_id
              AND t501.c1900_company_id    = v205.c1900_company_id
              AND t502.c502_void_fl IS NULL
              AND t501.c501_void_fl IS NULL
              AND t501.c501_delete_fl IS NULL
		      AND (t501.c901_ext_country_id IS NULL OR t501.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH
         ORDER BY c202_project_id;

      v_cur               TYPES.cursor_type;
      v_back_cur          TYPES.cursor_type;
      v_parent_order_id   t501_order.c501_order_id%TYPE;
      v_status            VARCHAR2 (20);
      v_order_id          VARCHAR2 (20);
   BEGIN
      v_status := get_do_split_status (p_order_id);

      FOR v_back_cur IN back_order_cur
      LOOP
         gm_sav_order_project (v_back_cur.order_id,
                               v_back_cur.project_id,
                               p_user_id
                              );
    
         gm_sav_plant_update  (v_back_cur.order_id,
                               v_back_cur.project_id,
                               p_user_id
                              );    
        -- PMT - 40241 : Mobile Device - Tissue parts shipping address changes in order confirmation
		gm_update_shipping_info(v_back_cur.order_id,   
                                 v_back_cur.project_id,
                                 p_user_id
                              );							  
      END LOOP;

      IF v_status = 'DONTPROCEED'
      THEN
         RETURN;
      END IF;
      
      IF v_status = 'SPLITNOW'
      THEN
         v_parent_order_id := p_order_id;
      END IF;

      FOR v_cur IN part_num_cur
      LOOP
         IF v_parent_order_id IS NOT NULL
         THEN
            v_order_id := get_next_order_id (NULL);

            gm_sav_create_split_order (v_order_id,
                                       v_parent_order_id,
                                       v_cur.project_id,
                                       p_user_id
                                      );
            gm_sav_upd_split_do_parts (v_order_id, v_cur.project_id,
                                       p_user_id);
                                       
            gm_sav_plant_update (v_order_id,
                                 v_cur.project_id,
                                 p_user_id
                              );       
            gm_update_shipping_info(v_order_id,   
                                 v_cur.project_id,
                                 p_user_id
                              );                                  -- PMT - 40241 : Mobile Device - Tissue parts shipping address changes in order confirmation
                  
         ELSE
            IF v_status = 'SPLITLATER'
            THEN
               gm_sav_order_project (p_order_id, v_cur.project_id, p_user_id);
               
               /* If the DO with only Viacell parts ,then call the below procedure to update the parent order plant id to 3013 */
               gm_sav_plant_update (p_order_id,
                                	v_cur.project_id,
                                	p_user_id
                              	   ); 
             -- PMT - 40241 : Mobile Device - Tissue parts shipping address changes in order confirmation
				gm_update_shipping_info(p_order_id,   
                                 v_cur.project_id,
                                 p_user_id
                              );								   
            END IF;
         END IF;

         v_parent_order_id := p_order_id;
     END LOOP;
    
    gm_update_do_type(p_order_id,p_user_id);
	gm_upd_order_type(p_order_id,p_user_id);
   END gm_sav_do_split;
   
 -- Changes added to accomodate viacell parts  
   
   PROCEDURE gm_sav_plant_update (
      p_order_id     IN   t501_order.c501_order_id%TYPE,
      p_project_id   IN   t205_part_number.c202_project_id%TYPE,
      p_user_id      IN   t501_order.c501_created_by%TYPE
   )
    AS
      v_viacell_Prj_fl     t906_rules.c906_rule_value%TYPE;
      v_plant_id     	   t5040_plant_master.C5040_PLANT_ID%TYPE;
      v_ipad_skipfl	t906_rules.c906_rule_value%TYPE;
     v_Order_Mode	t906_rules.c906_rule_value%TYPE;
     v_ipad_tissue_skipfl	t906_rules.c906_rule_value%TYPE;
     v_company_id t1900_company.c1900_company_id%TYPE;
     v_ipad_sterlie_plant	t906_rules.c906_rule_value%TYPE;
     

   BEGIN
	   
	  	BEGIN 
		          SELECT c501_receive_mode 
		          		INTO v_Order_Mode 
		          	FROM t501_order
		          	WHERE c501_order_id= p_order_id 
		          	AND c501_void_fl IS NULL ;
				EXCEPTION
				      WHEN no_data_found THEN
				        v_Order_Mode:=NULL;
		  END;
		  
		  	SELECT get_rule_value (v_Order_Mode,'IPADSKIPFL')    
			  	 	INTO v_ipad_skipfl
			  	 FROM DUAL;
			 
			 
		-- get the company id from context for PMT-40221
           SELECT get_compid_frm_cntx() 
		     INTO v_company_id
		   FROM DUAL; 
		   	 
		--get the rule value to retain the order type based on project for PMT-40221
       BEGIN
         SELECT get_rule_value_by_company (p_project_id,'TISSUESKIPORDERTYPE', v_company_id) 
          INTO v_ipad_tissue_skipfl  
            FROM DUAL;
       EXCEPTION
            WHEN NO_DATA_FOUND THEN
                v_ipad_tissue_skipfl := NULL;
            END; 
            
       BEGIN
         SELECT get_rule_value_by_company (p_project_id,'STERILEORDTYPBYPLANT', v_company_id) 
           INTO v_ipad_sterlie_plant  
           FROM DUAL;
       EXCEPTION
            WHEN NO_DATA_FOUND THEN
                v_ipad_sterlie_plant := 'N';
       END;

		BEGIN
		  SELECT DISTINCT v205.c906_rule_value
			  INTO v_plant_id 
		  FROM t501_order t501 ,
		    t502_item_order t502 ,
		    V205_PARTS_WITH_EXT_VENDOR v205
		  WHERE t501.c501_order_id     = p_order_id
		  AND t501.c501_order_id       = t502.c501_order_id
		  AND t502.c205_part_number_id = v205.c205_part_number_id
		  AND t501.c1900_company_id    = v205.c1900_company_id
		  AND t501.c501_void_fl IS NULL
		  AND t502.c502_void_fl IS NULL;
		EXCEPTION
		WHEN NO_DATA_FOUND THEN
		  v_plant_id := NULL;
		END;

	  IF v_plant_id IS NOT NULL AND v_ipad_skipfl = 'Y' THEN  
		
	  IF v_ipad_tissue_skipfl = 'Y' THEN  --code change for PMT-40221
	  /*Call the below procedure to update the order type to Bill Only - From Sales Consignments */
	        gm_sav_back_ord_to_boc_ord (p_order_id,p_user_id ); -- based on rule
	    END IF;
	    
	    IF v_ipad_sterlie_plant = 'Y' THEN  --code change for PC-2575
	  /*Call the below procedure to update the order type to Bill & ship and Backorder */
	        gm_pkg_op_split_process.gm_update_order_type(p_order_id,v_plant_id,p_user_id); 
	    END IF;
	    
      		UPDATE T501_ORDER 
      		   SET c5040_plant_id = v_plant_id 
     	 	WHERE c501_order_id = p_order_id
     	 	  AND c501_order_id IN ( SELECT DISTINCT t502.c501_order_id
     	 	                            FROM V205_PARTS_WITH_EXT_VENDOR V205
     	 	                               , t502_item_order t502
     	 	                               , t501_order t501 
     	 	                           WHERE t502.c205_part_number_id = v205.c205_part_number_id
     	 	                           	 AND t501.c501_order_id       = t502.c501_order_id
     	 	                           	 AND t501.c1900_company_id    = v205.c1900_company_id
     	 	                             AND t502.c501_order_id = p_order_id
     	 	                         )  
       		 AND c501_void_fl IS NULL;
       		 
       	   UPDATE T907_SHIPPING_INFO 
       	      SET c5040_plant_id = v_plant_id 
       		WHERE c907_ref_id = p_order_id
       		  AND c907_ref_id IN ( SELECT DISTINCT t502.c501_order_id
    	 	                         FROM V205_PARTS_WITH_EXT_VENDOR V205
     	 	                            , T502_ITEM_ORDER T502
     	 	                            , t501_order t501 
     	 	                        WHERE t502.c205_part_number_id = v205.c205_part_number_id
     	 	                        	AND t501.c501_order_id       = t502.c501_order_id
     	 	                           	AND t501.c1900_company_id    = v205.c1900_company_id
    	 	                            AND t502.c501_order_id = p_order_id
     	 	                         ) 
      		  AND c907_void_fl IS NULL;
      		  
      		   --update plant in t5063_lot_error_info table for IPAD orders for PMT-29681
      		  UPDATE t5063_lot_error_info 
       	      SET c5040_plant_id = v_plant_id 
       		WHERE c5063_trans_id = p_order_id
       		  AND c5063_trans_id IN ( SELECT DISTINCT t502.c501_order_id
    	 	                         FROM V205_PARTS_WITH_EXT_VENDOR V205
     	 	                            , T502_ITEM_ORDER T502
     	 	                            , t501_order t501 
     	 	                        WHERE t502.c205_part_number_id = v205.c205_part_number_id
     	 	                        	AND t501.c501_order_id       = t502.c501_order_id
     	 	                           	AND t501.c1900_company_id    = v205.c1900_company_id
    	 	                            AND t502.c501_order_id = p_order_id
     	 	                         ) 
      		  AND c5063_void_fl IS NULL;
      		  
    	 END IF;      	
   END 	gm_sav_plant_update;
   
   PROCEDURE gm_sav_order_project (
      p_order_id     IN   t501_order.c501_order_id%TYPE,
      p_project_id   IN   t205_part_number.c202_project_id%TYPE,
      p_user_id      IN   t501_order.c501_created_by%TYPE
   )
   AS
     v_ipad_tissue_skipfl	t906_rules.c906_rule_value%TYPE;
     v_company_id t1900_company.c1900_company_id%TYPE;
   BEGIN  
    -- get the company id from context for PMT-40221
           SELECT get_compid_frm_cntx() 
		     INTO v_company_id
		   FROM DUAL; 
		   
    --get the rule value to retain the order type based on project for PMT-40221
       BEGIN
         SELECT get_rule_value_by_company (p_project_id,'TISSUESKIPORDERTYPE', v_company_id) 
          INTO v_ipad_tissue_skipfl  
            FROM DUAL;
       EXCEPTION
            WHEN NO_DATA_FOUND THEN
                v_ipad_tissue_skipfl := NULL;
       END; 
	   
	IF v_ipad_tissue_skipfl = 'Y' THEN  --code change for PMT-40221
	 /*Call the below procedure to update the order type to Bill Only - From Sales Consignments */
      UPDATE t5050_invpick_assign_detail
         SET c5050_void_fl = 'Y',
             c5050_last_updated_by = p_user_id,
             c5050_last_updated_date = SYSDATE
       WHERE c5050_ref_id = p_order_id AND c901_ref_type = 93001;
		   

      gm_pkg_op_do_master.gm_sav_order_attribute_master (NULL,
                                                         p_order_id,
                                                         '11241',
                                                         p_project_id,
                                                         p_user_id
                                                        );
     END IF;                                                   
   END gm_sav_order_project;

   PROCEDURE gm_sav_create_split_order (
      p_order_id          IN   t501_order.c501_order_id%TYPE,
      p_parent_order_id   IN   t501_order.c501_order_id%TYPE,
      p_project_id        IN   t205_part_number.c202_project_id%TYPE,
      p_user_id           IN   t501_order.c501_created_by%TYPE
   )
   AS
      v_order_id           t501_order.c501_order_id%TYPE;
      v_account_id         t501_order.c704_account_id%TYPE;
      v_sales_rep_id       t501_order.c703_sales_rep_id%TYPE;
      v_order_desc         t501_order.c501_order_desc%TYPE;
      v_customer_po        t501_order.c501_customer_po%TYPE;
      v_order_date         t501_order.c501_order_date%TYPE;
      v_shipping_date      t501_order.c501_shipping_date%TYPE;
      v_total_cost         t501_order.c501_total_cost%TYPE;
      v_delivery_carrier   t501_order.c501_delivery_carrier%TYPE;
      v_delivery_mode      t501_order.c501_delivery_mode%TYPE;
      v_tracking_number    t501_order.c501_tracking_number%TYPE;
      v_ship_to            t501_order.c501_ship_to%TYPE;
      v_status_fl          t501_order.c501_status_fl%TYPE;
      v_receive_mode       t501_order.c501_receive_mode%TYPE;
      v_received_from      t501_order.c501_received_from%TYPE;
      v_ship_charge_fl     t501_order.c501_ship_charge_fl%TYPE;
      v_comments           t501_order.c501_comments%TYPE;
      v_ship_to_id         t501_order.c501_ship_to_id%TYPE;
      v_ship_cost          t501_order.c501_ship_cost%TYPE;
      v_update_inv_fl      t501_order.c501_update_inv_fl%TYPE;
      v_order_date_time    t501_order.c501_order_date_time%TYPE;
      v_order_type         t501_order.c901_order_type%TYPE;
      v_reason_type        t501_order.c901_reason_type%TYPE;
      v_parent_order_id    t501_order.c501_parent_order_id%TYPE;
      v_rma_id             t501_order.c506_rma_id%TYPE;
      v_invoice_id         t501_order.c503_invoice_id%TYPE;
      v_hold_fl            t501_order.c501_hold_fl%TYPE;
      v_case_info_id       t501_order.c7100_case_info_id%TYPE;
      v_addressid          t907_shipping_info.c106_address_id%TYPE;
      v_shipid             t907_shipping_info.c907_ship_to_id%TYPE;
      v_surg_date		   t501_order.c501_surgery_date%TYPE;
   BEGIN
-- Fetch parent order details and store in variables
      SELECT c501_order_id, c704_account_id, c703_sales_rep_id,
             c501_order_desc, c501_customer_po, c501_order_date,
             c501_shipping_date, c501_total_cost, c501_delivery_carrier,
             c501_delivery_mode, c501_tracking_number, c501_ship_to,
             c501_status_fl, c501_receive_mode, c501_received_from,
             c501_ship_charge_fl, c501_comments, c501_ship_to_id,
             c501_ship_cost, c501_update_inv_fl, c501_order_date_time,
             c901_order_type, c901_reason_type, c501_parent_order_id,
             c506_rma_id, c503_invoice_id, c501_hold_fl, c7100_case_info_id,c501_surgery_date
        INTO v_order_id, v_account_id, v_sales_rep_id,
             v_order_desc, v_customer_po, v_order_date,
             v_shipping_date, v_total_cost, v_delivery_carrier,
             v_delivery_mode, v_tracking_number, v_ship_to,
             v_status_fl, v_receive_mode, v_received_from,
             v_ship_charge_fl, v_comments, v_ship_to_id,
             v_ship_cost, v_update_inv_fl, v_order_date_time,
             v_order_type, v_reason_type, v_parent_order_id,
             v_rma_id, v_invoice_id, v_hold_fl, v_case_info_id,v_surg_date
        FROM t501_order
       WHERE c501_order_id = p_parent_order_id
         AND c501_void_fl IS NULL
         AND c501_delete_fl IS NULL;

      gm_pkg_op_do_master.gm_sav_order_master (p_order_id,
                                               v_account_id,
                                               v_sales_rep_id,
                                               v_order_desc,
                                               v_customer_po,
                                               v_order_date,
                                               v_shipping_date,
                                               v_total_cost,
                                               v_delivery_carrier,
                                               v_delivery_mode,
                                               v_tracking_number,
                                               v_ship_to,
                                               v_status_fl,
                                               v_receive_mode,
                                               v_received_from,
                                               v_ship_charge_fl,
                                               v_comments,
                                               v_ship_to_id,
                                               NULL,
                                               v_update_inv_fl,
                                               v_order_date_time,
                                               v_order_type,
                                               v_reason_type,
                                               p_parent_order_id,
                                               v_rma_id,
                                               NULL,
                                               v_hold_fl,
                                               v_case_info_id,
                                               p_user_id,
                                               NULL,
                                               v_surg_date
                                              );
      gm_sav_order_project (p_order_id, p_project_id, p_user_id);

      -- Insert Shipping information for child order
      BEGIN
         SELECT c106_address_id
           INTO v_addressid
           FROM t907_shipping_info t907
          WHERE t907.c907_ref_id = p_parent_order_id
            AND t907.c907_void_fl IS NULL;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            v_addressid := NULL;
      END;

      gm_pkg_cm_shipping_trans.gm_sav_shipping (p_order_id,
                                                50180                 --orders
                                                     ,
                                                v_ship_to,
                                                v_ship_to_id,
                                                v_delivery_carrier,
                                                v_delivery_mode,
                                                NULL,
                                                v_addressid,
                                                NULL,
                                                p_user_id,
                                                v_shipid
                                               );
   END gm_sav_create_split_order;

   PROCEDURE gm_sav_upd_split_do_parts (
      p_order_id     IN   t501_order.c501_order_id%TYPE,
      p_project_id   IN   t205_part_number.c202_project_id%TYPE,
      p_user_id      IN   t501_order.c501_created_by%TYPE
   )
   AS
      v_parent_ord_id   t501_order.c501_order_id%TYPE;
      v_total_cost      t501_order.c501_total_cost%TYPE;
	  v_ordtype         t501_order.c901_order_type%TYPE;
	  v_order_pick_rule_val t906_rules.c906_rule_value%TYPE;
	  v_allocate_fl  VARCHAR2 (1);
	  v_count        NUMBER :=0;
   BEGIN
      SELECT c501_parent_order_id,NVL(c901_order_type,2521)
        INTO v_parent_ord_id,v_ordtype
        FROM t501_order t501
       WHERE t501.c501_order_id = p_order_id;

	  --PC-2561	Do not update the new order incase of order has only one part
      IF v_parent_ord_id IS NOT NULL 
      THEN
	  	SELECT COUNT(1) 
	  	  INTO v_count 
	  	  FROM t502_item_order 
	  	 WHERE c501_order_id = v_parent_ord_id
	  	   AND c502_void_fl IS NULL;
	  END IF;
       

      UPDATE t502_item_order t502
         SET c501_order_id = decode(v_count,1,c501_order_id,p_order_id)
       WHERE c501_order_id = v_parent_ord_id
         AND t502.c205_part_number_id IN (
                SELECT v205.c205_part_number_id
                  FROM t502_item_order t502,
                  	   V205_PARTS_WITH_EXT_VENDOR v205 ,
                  	   t501_order t501
                 WHERE t502.c501_order_id = v_parent_ord_id
                   AND v205.c205_part_number_id = t502.c205_part_number_id
                   AND v205.c202_project_id = p_project_id
                   AND t501.c501_order_id = t502.c501_order_id
                   AND t501.c1900_company_id    = v205.c1900_company_id
                   AND t502.c502_void_fl IS NULL
                   AND t502.c502_delete_fl IS NULL);

      SELECT SUM (NVL (t502.c502_item_qty, 0) * NVL (t502.c502_item_price, 0))
        INTO v_total_cost
        FROM t502_item_order t502
       WHERE t502.c501_order_id = p_order_id
         AND t502.c502_void_fl IS NULL
         AND t502.c502_delete_fl IS NULL;

      -- Update Total Cost for child order and parent order
      UPDATE t501_order t501
         SET t501.c501_total_cost = v_total_cost
           , c501_last_updated_by = p_user_id
		   , c501_last_updated_date = SYSDATE
       WHERE t501.c501_order_id = p_order_id
         AND t501.c501_void_fl IS NULL
         AND t501.c501_delete_fl IS NULL;

      UPDATE t501_order t501
         SET t501.c501_total_cost =
                          NVL (t501.c501_total_cost, 0)
                          - NVL (v_total_cost, 0)
           , c501_last_updated_by = p_user_id
		   , c501_last_updated_date = SYSDATE
       WHERE t501.c501_order_id = v_parent_ord_id
         AND t501.c501_void_fl IS NULL
         AND t501.c501_delete_fl IS NULL;
		 
	    -- Adding shipping record - PMT-40221
		v_order_pick_rule_val := NVL(get_rule_value(v_ordtype, 'ORDER_INV_PICK'), 'N');
		--
		IF v_order_pick_rule_val = 'Y'
		THEN
			-- Code added for allocation logic

			-- Uncommented the code to handle order which has a loaner part + 999 part.
			-- As both are not picked, the order shdn't be allocated.
			v_allocate_fl := gm_pkg_op_inventory.chk_txn_details (p_order_id, 93001);

			IF NVL (v_allocate_fl, 'N') = 'Y'
			THEN
				gm_pkg_allocation.gm_ins_invpick_assign_detail (93001, p_order_id, p_user_id);
			END IF;
		END IF;
   END gm_sav_upd_split_do_parts;

   FUNCTION get_do_split_status (p_order_id IN t501_order.c501_order_id%TYPE)
      RETURN VARCHAR2
   IS
      v_return       VARCHAR2 (20);
      v_return_cnt   NUMBER;
      v_tot_count    NUMBER;
      v_count        NUMBER;
   BEGIN
-- Get the total number of parts and number of parts in new projects,
-- this will be used to decide whether / not to create a child order.
      SELECT COUNT (1)
        INTO v_tot_count
        FROM t502_item_order t502
       WHERE t502.c501_order_id = p_order_id
         AND t502.c502_void_fl IS NULL
         AND t502.c502_delete_fl IS NULL;                   --'1149-120306-12'

      SELECT COUNT (pnum) cnt
        INTO v_count
        FROM (SELECT t502.c501_order_id, v205.c205_part_number_id pnum
                FROM t502_item_order t502,
                     V205_PARTS_WITH_EXT_VENDOR v205,
                      t501_order t501
               WHERE t502.c501_order_id = p_order_id        --'1149-120306-12'
                 AND v205.c205_part_number_id = t502.c205_part_number_id
                 AND t501.c501_order_id       = t502.c501_order_id
 				 AND t501.c1900_company_id    = v205.c1900_company_id
                 AND t502.c502_void_fl IS NULL
                 AND t502.c502_delete_fl IS NULL);

      v_return_cnt := v_tot_count - v_count;

      IF v_count = 0
      THEN
         v_return := 'DONTPROCEED';
      ELSIF v_return_cnt > 0
      THEN
         v_return := 'SPLITNOW';
      ELSE
         v_return := 'SPLITLATER';
      END IF;

      RETURN v_return;
   END get_do_split_status;
   
   PROCEDURE gm_fch_order_details (
      p_order_id         IN       t501_order.c501_order_id%TYPE,
      p_project_id       IN       t205_part_number.c202_project_id%TYPE,
      p_ordsummary_cur   OUT      TYPES.cursor_type,
      p_orddtl_cur       OUT      TYPES.cursor_type
   )
   AS
      v_order_id   t501_order.c501_order_id%TYPE;
   BEGIN
        OPEN p_ordsummary_cur
        FOR
         SELECT DISTINCT  c501_order_id ORDID, c205_part_number_id pnum, get_partnum_desc (c205_part_number_id) pdesc
                                    , c502_item_qty qty, lvl
                           FROM
                           (
                              WITH data AS
                              (
                                SELECT t502.c501_order_id, t502.c205_part_number_id, c502_item_qty qty
                              FROM t502_item_order t502,
                                   V205_PARTS_WITH_EXT_VENDOR v205,
                                   t501a_order_attribute t501a,
                                   t501_order t501
                             WHERE (   t501.c501_order_id = p_order_id
                                    OR t501.c501_parent_order_id = p_order_id
                                   )
                               AND t502.c502_void_fl IS NULL
                               AND t501.c501_order_id = t502.c501_order_id
                               AND t501a.c501_order_id = t502.c501_order_id
                               AND t501a.c901_attribute_type = 11241
                               AND t502.c205_part_number_id = v205.c205_part_number_id
                               AND v205.c202_project_id = p_project_id
                               AND t501.c501_void_fl IS NULL
                               AND t501a.c501a_void_fl IS NULL
                               AND t501.c501_delete_fl IS NULL
                         	   AND (t501.c901_ext_country_id IS NULL OR t501.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH
                               AND t502.c502_delete_fl IS NULL
                              )
                                 SELECT c501_order_id, c205_part_number_id,  1 c502_item_qty, LEVEL lvl
                                 FROM data
                                 CONNECT BY LEVEL <= QTY )
                                 order by ORDID, pnum, pdesc;
      BEGIN
         SELECT DISTINCT t502.c501_order_id
                    INTO v_order_id
                    FROM t501_order t501,
                         t502_item_order t502,
                         t501a_order_attribute t501a,
                         V205_PARTS_WITH_EXT_VENDOR v205
                   WHERE (   t501.c501_order_id = p_order_id
                          OR t501.c501_parent_order_id = p_order_id
                         )
                     AND t501.c501_order_id = t502.c501_order_id
                     AND t501a.c501_order_id = t502.c501_order_id
                     AND t501a.c901_attribute_type = 11241
                     AND t502.c205_part_number_id = v205.c205_part_number_id
                     AND v205.c202_project_id = p_project_id
                     AND t501.c501_void_fl IS NULL
                     AND t502.c502_void_fl IS NULL
                     AND t501.c501_delete_fl IS NULL
                     AND (t501.c901_ext_country_id IS NULL OR t501.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH
                     AND t502.c502_delete_fl IS NULL;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            v_order_id := NULL;
         WHEN TOO_MANY_ROWS
         THEN
            SELECT MAX(t502.c501_order_id)
                       INTO v_order_id
                       FROM t501_order t501,
                            t502_item_order t502,
                            t501a_order_attribute t501a,
                            V205_PARTS_WITH_EXT_VENDOR v205
                      WHERE (   t501.c501_order_id = p_order_id
                             OR t501.c501_parent_order_id = p_order_id
                            )
                        AND t501.c501_order_id = t502.c501_order_id
                        AND t501a.c501_order_id = t502.c501_order_id
                        AND t501a.c901_attribute_type = 11241
                        AND t502.c205_part_number_id =
                                                      v205.c205_part_number_id
                        AND v205.c202_project_id = p_project_id
                        AND t501.c501_void_fl IS NULL
                        AND t502.c502_void_fl IS NULL
                        AND t501.c501_delete_fl IS NULL
                        AND (t501.c901_ext_country_id IS NULL OR t501.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH
                        AND t502.c502_delete_fl IS NULL;
                      --  AND NVL (t501.c901_order_type, -999) != 2525;
      END;

      OPEN p_orddtl_cur
       FOR
          SELECT t501.c501_order_id ID,
                 get_cs_ship_email (4121, t501.c703_sales_rep_id) repemail,
                 get_rep_name (t501.c703_sales_rep_id) repnm,
                 DECODE(t501.c901_order_type,2532,get_rep_name (t501.c703_sales_rep_id),gm_pkg_cm_shipping_info.get_ship_add (v_order_id,
                                                       '50180'
                                                      ))shipadd,
                 t501.c501_order_date odt,
                 gm_pkg_cm_shipping_info.get_carrier (v_order_id,
                                                      '50180'
                                                     ) carrier,
                 gm_pkg_cm_shipping_info.get_ship_mode (v_order_id,
                                                        '50180'
                                                       ) shipmode,
                 t501.c901_order_type ordertype, t501.c501_customer_po custpo
            FROM t501_order t501
           WHERE t501.c501_order_id = v_order_id
             AND t501.c501_void_fl IS NULL
             AND t501.c501_delete_fl IS NULL;
   END gm_fch_order_details;

   PROCEDURE gm_fch_rules_lookup (
      p_grpid   IN       t906_rules.c906_rule_grp_id%TYPE,
      p_out     OUT      TYPES.cursor_type
   )
   AS
   BEGIN
      OPEN p_out
       FOR
          SELECT c906_rule_id ID, c906_rule_grp_id rulegrpid,
                 c906_rule_desc description, c906_rule_value rulevalue
            FROM t906_rules
           WHERE c906_rule_grp_id = p_grpid AND c906_void_fl IS NULL;
   END gm_fch_rules_lookup;

   /* procedure to split RA by vendor
     *
     *
     */
   PROCEDURE gm_sav_ra_split (
      p_ra_id     IN   t506_returns.c506_rma_id%TYPE,
      p_user_id   IN   t506_returns.c101_user_id%TYPE
   )
   AS
-- Get all the distinct project id's  that has external vendors for the given order id
      CURSOR proj_cur
      IS
         SELECT DISTINCT c202_project_id project_id
                    FROM t507_returns_item t507,
                         V205_PARTS_WITH_EXT_VENDOR v205
                   WHERE t507.c506_rma_id = p_ra_id            --'GM-RA-56855'
                     AND v205.c205_part_number_id = t507.c205_part_number_id
                     AND t507.c507_hold_fl IS NULL
                ORDER BY c202_project_id;

      v_parent_ra_id   VARCHAR2 (20);
      v_status         VARCHAR2 (20);
      v_ra_id          t506_returns.c506_rma_id%TYPE;
      v_cnt            NUMBER;
   BEGIN
      v_status := get_ra_split_status (p_ra_id);

      IF (v_status = 'SPLITNOW')
      THEN
         v_parent_ra_id := p_ra_id;
      END IF;

      FOR v_cur IN proj_cur
      LOOP
         IF v_parent_ra_id IS NOT NULL
         THEN
            SELECT 'GM-CRA-' || s506_return.NEXTVAL
              INTO v_ra_id
              FROM DUAL;

            gm_sav_create_split_ra (v_ra_id,
                                    v_parent_ra_id,
                                    v_cur.project_id,
                                    p_user_id
                                   );
            gm_sav_upd_split_ra_parts (v_ra_id, v_cur.project_id, p_user_id);
         ELSIF v_status = 'SPLITLATER'
         THEN
            gm_sav_ra_project (p_ra_id, v_cur.project_id, p_user_id);
         END IF;

         v_parent_ra_id := p_ra_id;
      END LOOP;
   END gm_sav_ra_split;

   FUNCTION get_ra_split_status (p_ra_id IN t506_returns.c506_rma_id%TYPE)
      RETURN VARCHAR2
   IS
      v_return       VARCHAR2 (20);
      v_return_cnt   NUMBER;
      v_tot_count    NUMBER;
      v_count        NUMBER;
   BEGIN
      -- Get the total number of parts and number of parts in new projects,
      -- this will be used to decide whether / not to create a child ra
      SELECT COUNT (1)
        INTO v_tot_count
        FROM t507_returns_item t507
       WHERE t507.c506_rma_id = p_ra_id AND t507.c507_hold_fl IS NULL;

      SELECT COUNT (pnum) cnt
        INTO v_count
        FROM (SELECT c506_rma_id, v205.c205_part_number_id pnum
                FROM t507_returns_item t507,
                     V205_PARTS_WITH_EXT_VENDOR v205
               WHERE t507.c506_rma_id = p_ra_id              --'1149-120306-12
                 AND v205.c205_part_number_id = t507.c205_part_number_id
                 AND t507.c507_hold_fl IS NULL);

      v_return_cnt := v_tot_count - v_count;

      IF v_count = 0
      THEN
         v_return := 'DONTPROCEED';
      ELSIF v_return_cnt > 0
      THEN
         v_return := 'SPLITNOW';
      ELSE
         v_return := 'SPLITLATER';
      END IF;

      RETURN v_return;
   END get_ra_split_status;

   PROCEDURE gm_sav_create_split_ra (
      p_ra_id         IN   t506_returns.c506_rma_id%TYPE,
      p_ra_order_id   IN   t506_returns.c506_parent_rma_id%TYPE,
      p_project_id    IN   t205_part_number.c202_project_id%TYPE,
      p_user_id       IN   t506_returns.c101_user_id%TYPE
   )
   AS
      v_ra_id                  t506_returns.c506_rma_id%TYPE;
      v_comments               t506_returns.c506_comments%TYPE;
      v_invoice_id             t506_returns.c503_invoice_id%TYPE;
      v_created_date           t506_returns.c506_created_date%TYPE;
      v_type                   t506_returns.c506_type%TYPE;
      v_distributor_id         t506_returns.c701_distributor_id%TYPE;
      v_reason                 t506_returns.c506_reason%TYPE;
      v_status_fl              t506_returns.c506_status_fl%TYPE;
      v_created_by             t506_returns.c506_created_by%TYPE;
      v_credit_memo_id         t506_returns.c506_credit_memo_id%TYPE;
      v_expected_date          t506_returns.c506_expected_date%TYPE;
      v_set_id                 t506_returns.c207_set_id%TYPE;
      v_last_updated_date      t506_returns.c506_last_updated_date%TYPE;
      v_last_updated_by        t506_returns.c506_last_updated_by%TYPE;
      v_account_id             t506_returns.c704_account_id%TYPE;
      v_credit_date            t506_returns.c506_credit_date%TYPE;
      v_return_date            t506_returns.c506_return_date%TYPE;
      v_update_inv_fl          t506_returns.c506_update_inv_fl%TYPE;
      v_order_id               t506_returns.c501_order_id%TYPE;
      v_qb_consignment_txnid   t506_returns.qb_consignment_txnid%TYPE;
      v_reprocess_date         t506_returns.c501_reprocess_date%TYPE;
      v_user_id                t506_returns.c101_user_id%TYPE;
      v_ac_credit_dt           t506_returns.c506_ac_credit_dt%TYPE;
      v_void_fl                t506_returns.c506_void_fl%TYPE;
      v_parent_rma_id          t506_returns.c506_parent_rma_id%TYPE;
      v_sales_rep_id           t506_returns.c703_sales_rep_id%TYPE;
      v_associated_rma_id      t506_returns.c506_associated_rma_id%TYPE;
   BEGIN
-- Fetch parent ra details and store in variables
      SELECT c506_rma_id, c506_comments, c503_invoice_id, c506_created_date,
             c506_type, c701_distributor_id, c506_reason, c506_status_fl,
             c506_created_by, c506_credit_memo_id, c506_expected_date,
             c207_set_id, c506_last_updated_date, c506_last_updated_by,
             c704_account_id, c506_credit_date, c506_return_date,
             c506_update_inv_fl, c501_order_id, qb_consignment_txnid,
             c501_reprocess_date, c101_user_id, c506_ac_credit_dt,
             c506_void_fl, c506_parent_rma_id, c703_sales_rep_id,
             c506_associated_rma_id
        INTO v_ra_id, v_comments, v_invoice_id, v_created_date,
             v_type, v_distributor_id, v_reason, v_status_fl,
             v_created_by, v_credit_memo_id, v_expected_date,
             v_set_id, v_last_updated_date, v_last_updated_by,
             v_account_id, v_credit_date, v_return_date,
             v_update_inv_fl, v_order_id, v_qb_consignment_txnid,
             v_reprocess_date, v_user_id, v_ac_credit_dt,
             v_void_fl, v_parent_rma_id, v_sales_rep_id,
             v_associated_rma_id
        FROM t506_returns
       WHERE c506_rma_id = p_ra_order_id AND c506_void_fl IS NULL;

      gm_pkg_op_ra_master.gm_sav_ra_master (p_ra_id,
                                            v_comments,
                                            v_invoice_id,
                                            v_created_date,
                                            v_type,
                                            v_distributor_id,
                                            v_reason,
                                            v_status_fl,
                                            v_created_by,
                                            v_credit_memo_id,
                                            v_expected_date,
                                            v_set_id,
                                            v_last_updated_date,
                                            v_last_updated_by,
                                            v_account_id,
                                            v_credit_date,
                                            v_return_date,
                                            v_update_inv_fl,
                                            v_order_id,
                                            v_qb_consignment_txnid,
                                            v_reprocess_date,
                                            v_user_id,
                                            v_ac_credit_dt,
                                            v_void_fl,
                                            p_ra_order_id,
                                            v_sales_rep_id,
                                            v_associated_rma_id
                                           );
      gm_sav_ra_project (p_ra_id, p_project_id, p_user_id);
   END gm_sav_create_split_ra;

   PROCEDURE gm_sav_ra_project (
      p_ra_id        IN   t506_returns.c506_rma_id%TYPE,
      p_project_id   IN   t205_part_number.c202_project_id%TYPE,
      p_user_id      IN   t506_returns.c101_user_id%TYPE
   )
   AS
      v_raid   VARCHAR2 (20);
   BEGIN
      gm_pkg_op_ra_master.gm_sav_ra_attribute_maste (NULL,
                                                     p_ra_id,
                                                     '11241',
                                                     p_project_id,
                                                     p_user_id
                                                    );
   END gm_sav_ra_project;

   PROCEDURE gm_sav_upd_split_ra_parts (
      p_ra_id        IN   t506_returns.c506_rma_id%TYPE,
      p_project_id   IN   t205_part_number.c202_project_id%TYPE,
      p_user_id      IN   t506_returns.c101_user_id%TYPE
   )
   AS
      v_parent_ra_id   t506_returns.c506_parent_rma_id%TYPE;
   BEGIN
      SELECT c506_parent_rma_id
        INTO v_parent_ra_id
        FROM t506_returns t506
       WHERE t506.c506_rma_id = p_ra_id AND t506.c506_void_fl IS NULL;

      UPDATE t507_returns_item t507
         SET t507.c506_rma_id = p_ra_id
       WHERE t507.c506_rma_id = v_parent_ra_id
         AND t507.c205_part_number_id IN (
                SELECT t507.c205_part_number_id
                  FROM t507_returns_item t507, V205_PARTS_WITH_EXT_VENDOR v205
                 WHERE t507.c506_rma_id = v_parent_ra_id
                   AND v205.c205_part_number_id = t507.c205_part_number_id
                   AND v205.c202_project_id = p_project_id
                   AND t507.c507_hold_fl IS NULL);
                   
       	--When ever the detail is updated, the consignment table has to be updated ,so ETL can Sync it to GOP.
	 	UPDATE t506_returns
		   SET C506_LAST_UPDATED_BY = p_user_id, 
       		   C506_LAST_UPDATED_DATE = SYSDATE
	  	 WHERE c506_rma_id  IN ( p_ra_id, v_parent_ra_id);
   END gm_sav_upd_split_ra_parts;

   PROCEDURE gm_fch_ra_details (
      p_ra_id           IN       t506_returns.c506_rma_id%TYPE,
      p_project_id      IN       t205_part_number.c202_project_id%TYPE,
      p_rasummary_cur   OUT      TYPES.cursor_type,
      p_radtl_cur       OUT      TYPES.cursor_type
   )
   AS
      v_ra_id   t506_returns.c506_rma_id%TYPE;
   BEGIN
      OPEN p_rasummary_cur
       FOR
          SELECT   SUM (t507.c507_item_qty) qty,
                   t507.c205_part_number_id pnum,
                   get_partnum_desc (t507.c205_part_number_id) pdesc,
                   c506_rma_id ID
              FROM t507_returns_item t507, V205_PARTS_WITH_EXT_VENDOR v205
             WHERE c506_rma_id IN (
                      SELECT c506_rma_id
                        FROM t506_returns t506
                       WHERE t506.c506_rma_id = p_ra_id
                          OR t506.c506_parent_rma_id = p_ra_id)
               AND c507_status_fl IN ('C', 'W', 'R', 'M')
               AND t507.c205_part_number_id = v205.c205_part_number_id
               AND v205.c202_project_id = p_project_id
               AND t507.c507_hold_fl IS NULL
          GROUP BY t507.c205_part_number_id, c506_rma_id;

      BEGIN
         SELECT DISTINCT t507.c506_rma_id
                    INTO v_ra_id
                    FROM t506_returns t506,
                         t507_returns_item t507,
                         t506a_returns_attribute t506a,
                         V205_PARTS_WITH_EXT_VENDOR v205
                   WHERE (   t506.c506_rma_id = p_ra_id
                          OR t506.c506_parent_rma_id = p_ra_id
                         )
                     AND t506.c506_rma_id = t507.c506_rma_id
                     AND t506a.c506_rma_id = t507.c506_rma_id
                     AND t506a.c901_attribute_type = 11241
                     AND t507.c507_status_fl IN ('C', 'W', 'R', 'M')
                     AND t507.c205_part_number_id = v205.c205_part_number_id
                     AND v205.c202_project_id = p_project_id
                     AND t506.c506_void_fl IS NULL
                     AND t507.c507_hold_fl IS NULL
                     AND t506a.c506a_void_fl IS NULL;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            v_ra_id := NULL;
      END;

      OPEN p_radtl_cur
       FOR
          SELECT a.c506_rma_id raid,
                 get_cs_ship_email (4121, a.c703_sales_rep_id) emailadd,
                 TO_CHAR (a.c506_credit_date, 'mm/dd/yyyy') creditemaildate,
                 a.c506_created_date cdate,
                 TO_CHAR (a.c506_ac_credit_dt, 'mm/dd/yyyy') creditdate,
                 a.c207_set_id setid, a.c501_order_id order_id,
                 get_rep_name (a.c703_sales_rep_id) repnm,
                 get_rule_value (p_project_id, 'EXVNDADD') shipadd
            FROM t506_returns a
           WHERE a.c506_rma_id = v_ra_id AND a.c506_void_fl IS NULL;
   END gm_fch_ra_details;
   
    PROCEDURE gm_sav_multi_consg_split (
      p_request_id   IN   t520_request.c520_request_id%TYPE,
      p_user_id      IN   t504_consignment.c504_created_by%TYPE
   )
   AS
   	 v_string VARCHAR2(4000):=p_request_id||',';
   	 v_request_id   t520_request.c520_request_id%TYPE;
   BEGIN
	    WHILE INSTR (v_string, ',') <> 0
		LOOP
		v_request_id := SUBSTR (v_string, 1, INSTR (v_string, ',') - 1);
       	v_string	:= SUBSTR (v_string, INSTR (v_string, ',') + 1);
	       	IF v_request_id IS NOT NULL THEN
		   	gm_pkg_op_split.gm_sav_consg_split(v_request_id,p_user_id);
		   	END IF;
	   END LOOP;
	   	
   END gm_sav_multi_consg_split;

   PROCEDURE gm_sav_consg_split (
      p_request_id   IN   t520_request.c520_request_id%TYPE,
      p_user_id      IN   t504_consignment.c504_created_by%TYPE
   )
   AS
      CURSOR proj_cur
      IS
         SELECT DISTINCT v205.c202_project_id project_id
                    FROM V205_PARTS_WITH_EXT_VENDOR v205,
                         (SELECT t521.c205_part_number_id
                            FROM t520_request t520, t521_request_detail t521
                           WHERE t520.c520_request_id = t521.c520_request_id
                             AND t520.c520_void_fl IS NULL
                             AND t520.c520_request_id = p_request_id
                          UNION
                          SELECT t505.c205_part_number_id
                            FROM t520_request t520,
                                 t504_consignment t504,
                                 t505_item_consignment t505
                           WHERE t520.c520_request_id = t504.c520_request_id
                             AND t504.c504_consignment_id =
                                                      t505.c504_consignment_id
                             AND t504.c504_void_fl IS NULL
                             AND t520.c520_void_fl IS NULL
                             AND t520.c520_request_id = p_request_id) pnum
                   WHERE v205.c205_part_number_id = pnum.c205_part_number_id
                ORDER BY project_id;

      v_status               VARCHAR2 (20);
      v_master_request_id    VARCHAR2 (20);
      v_request_id           VARCHAR2 (20);
      v_consignment_id       VARCHAR2 (20);
      v_set_id               VARCHAR2 (20);
      v_out_consignment_id   VARCHAR2 (20);
      v_ship_to_id           VARCHAR2 (20);
      v_email_fl             VARCHAR2 (20);
      v_req_count            NUMBER;
      v_ship_count           NUMBER;
      v_back_order_request_id  VARCHAR2 (20);
      v_split_count			NUMBER;
      v_request_by			t520_request.c520_created_by%TYPE;
   BEGIN
      BEGIN
         SELECT t504.c207_set_id, t504.c504_consignment_id,
                t520.c520_ship_to_id, t520.c520_email_fl , t520.c520_created_by
           INTO v_set_id, v_consignment_id,
                v_ship_to_id, v_email_fl , v_request_by
           FROM t504_consignment t504, t520_request t520
          WHERE t520.c520_request_id = t504.c520_request_id(+)
            AND t520.c520_request_id = p_request_id
            AND t520.c520_void_fl IS NULL
            AND t504.c504_void_fl(+) IS NULL;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            v_set_id := NULL;
            v_consignment_id := NULL;
            v_request_by := NULL;
      END;

      SELECT COUNT (1)
        INTO v_req_count
        FROM t520_request
       WHERE c520_master_request_id = p_request_id
         AND c520_void_fl IS NULL
         AND c520_email_fl = 'Y';

      SELECT COUNT (1)
        INTO v_ship_count
        FROM t907_shipping_info
       WHERE c907_ref_id = p_request_id
         AND c907_ship_to_id IS NOT NULL
         AND c907_void_fl IS NULL;

      IF v_ship_count = 0 OR v_email_fl IS NOT NULL OR v_req_count > 0
      THEN
         RETURN;
      END IF;

      v_status := get_consg_split_status (p_request_id);

      IF (v_status = 'SPLITNOW')
      THEN
         v_master_request_id := p_request_id;
      END IF;

      FOR v_cur IN proj_cur
      LOOP
         IF v_master_request_id IS NOT NULL
         THEN
            gm_sav_split_request (v_master_request_id,
                                  v_cur.project_id,
                                  NVL(p_user_id,v_request_by),
                                  v_request_id
                                 );
            gm_sav_split_request_pts (v_request_id,
                                      p_request_id,
                                      v_cur.project_id,
                                      NVL(p_user_id,v_request_by)
                                     );
            IF v_consignment_id IS NOT NULL
            THEN
               gm_sav_split_consg (v_consignment_id,
                                   v_request_id,
                                   v_cur.project_id,
                                   NVL(p_user_id,v_request_by),
                                   v_out_consignment_id
                                  );
               gm_sav_split_consg_parts (v_out_consignment_id,
                                         v_consignment_id,
                                         v_cur.project_id,
                                          NVL(p_user_id,v_request_by)
                                        );
                                        
             gm_sav_plant_req_update(v_request_id,v_cur.project_id, NVL(p_user_id,v_request_by));
             gm_sav_plant_consgn_update(v_out_consignment_id,v_cur.project_id, NVL(p_user_id,v_request_by));

            END IF;
         ELSIF v_status = 'SPLITLATER'
         THEN
            IF v_set_id IS NOT NULL
            THEN
               UPDATE t520_request
                  SET c202_project_id = v_cur.project_id
                    , c520_last_updated_by =  NVL(p_user_id,v_request_by)
                    , c520_last_updated_date = SYSDATE
                WHERE (   c520_request_id = p_request_id
                       OR c520_master_request_id = p_request_id
                      )
                  AND c520_void_fl IS NULL;
            ELSE
               UPDATE t520_request
                  SET c202_project_id = v_cur.project_id
                    , c520_last_updated_by =  NVL(p_user_id,v_request_by)
                    , c520_last_updated_date = SYSDATE
                WHERE c520_request_id = p_request_id 
                  AND c520_void_fl IS NULL;
            END IF;
            /* call the below prc to Update the plant id for backlog request*/
            IF v_consignment_id IS NULL  AND p_request_id IS NOT NULL 
            THEN
             gm_sav_plant_req_bo_update(p_request_id,v_cur.project_id, NVL(p_user_id,v_request_by));
            END IF;
            
            IF v_consignment_id IS NOT NULL
            THEN
            	/* Commenting the below prc call  to avoid the issue while sending shipping info mail 
            	 */
               --gm_sav_consg_project (v_consignment_id,v_cur.project_id,p_user_id);
               gm_sav_plant_req_update(p_request_id,v_cur.project_id, NVL(p_user_id,v_request_by));
               gm_sav_plant_consgn_update(v_consignment_id,v_cur.project_id, NVL(p_user_id,v_request_by));
            END IF;
         END IF;

         v_master_request_id := p_request_id;
      END LOOP;

      BEGIN
         SELECT c520_request_id
           INTO v_back_order_request_id
           FROM t520_request t520
          WHERE t520.c520_master_request_id = p_request_id
            AND c520_status_fl = 10
            AND c202_project_id is null
            AND C520_VOID_FL is NULL;            
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            v_back_order_request_id := NULL;
      END;

      IF v_back_order_request_id IS NOT NULL
      THEN
         SELECT COUNT (1)
           INTO v_split_count
           FROM t521_request_detail t521,
                V205_PARTS_WITH_EXT_VENDOR v205
          WHERE t521.c205_part_number_id = v205.c205_part_number_id
            AND t521.c520_request_id = v_back_order_request_id;

         IF v_split_count > 0
         THEN
            gm_sav_consg_split (v_back_order_request_id,  NVL(p_user_id,v_request_by));

            UPDATE t520_request
            SET C520_MASTER_REQUEST_ID = p_request_id
              , c520_last_updated_by =  NVL(p_user_id,v_request_by)
              , c520_last_updated_date = SYSDATE
            where C520_MASTER_REQUEST_ID = v_back_order_request_id;

         END IF;
      END IF;
   END gm_sav_consg_split;

   FUNCTION get_consg_split_status (
      p_request_id   IN   t504_consignment.c504_consignment_id%TYPE
   )
      RETURN VARCHAR2
   IS
      v_return       VARCHAR2 (20);
      v_return_cnt   NUMBER;
      v_tot_count    NUMBER;
      v_count        NUMBER;
   BEGIN
      SELECT SUM (qty_count)
        INTO v_tot_count
        FROM (SELECT COUNT (t505.c205_part_number_id) qty_count
                FROM t520_request t520,
                     t504_consignment t504,
                     t505_item_consignment t505
               WHERE t520.c520_request_id = t504.c520_request_id
                 AND t504.c504_consignment_id = t505.c504_consignment_id
                 AND t504.c504_void_fl IS NULL
                 AND t520.c520_void_fl IS NULL
                 AND t520.c520_request_id = p_request_id
              UNION
              SELECT COUNT (1) qty_count
                FROM t521_request_detail t521
               WHERE t521.c520_request_id = p_request_id);

      SELECT COUNT (v205.c205_part_number_id)
        INTO v_count
        FROM V205_PARTS_WITH_EXT_VENDOR v205,
             (SELECT t521.c205_part_number_id
                FROM t520_request t520, t521_request_detail t521
               WHERE t520.c520_request_id = t521.c520_request_id
                 AND t520.c520_void_fl IS NULL
                 AND t520.c520_request_id = p_request_id
              UNION
              SELECT t505.c205_part_number_id
                FROM t520_request t520,
                     t504_consignment t504,
                     t505_item_consignment t505
               WHERE t520.c520_request_id = t504.c520_request_id
                 AND t504.c504_consignment_id = t505.c504_consignment_id
                 AND t504.c504_void_fl IS NULL
                 AND t520.c520_void_fl IS NULL
                 AND t520.c520_request_id = p_request_id) pnum
       WHERE v205.c205_part_number_id = pnum.c205_part_number_id;

      v_return_cnt := v_tot_count - v_count;

      IF v_count = 0
      THEN
         v_return := 'DONTPROCEED';
      ELSIF v_return_cnt > 0
      THEN
         v_return := 'SPLITNOW';
      ELSE
         v_return := 'SPLITLATER';
      END IF;

      RETURN v_return;
   END get_consg_split_status;

   PROCEDURE gm_sav_split_request (
      p_master_request_id   IN       t520_request.c520_request_id%TYPE,
      p_project_id          IN       t205_part_number.c202_project_id%TYPE,
      p_user_id             IN       t504_consignment.c504_created_by%TYPE,
      p_out_request_id      OUT      t520_request.c520_request_id%TYPE
   )
   AS
      v_required_date      t520_request.c520_required_date%TYPE;
      v_request_source     t520_request.c901_request_source%TYPE;
      v_request_txn_id     t520_request.c520_request_txn_id%TYPE;
      v_set_id             t520_request.c207_set_id%TYPE;
      v_request_for        t520_request.c520_request_for%TYPE;
      v_request_to         t520_request.c520_request_to%TYPE;
      v_request_by_type    t520_request.c901_request_by_type%TYPE;
      v_request_by         t520_request.c520_request_by%TYPE;
      v_ship_to            t520_request.c901_ship_to%TYPE;
      v_ship_to_id_by      t520_request.c520_ship_to_id%TYPE;
      v_status_fl          t520_request.c520_status_fl%TYPE;
      v_purpose            t520_request.c901_purpose%TYPE;
      v_req_count          NUMBER;
      v_addressid          t907_shipping_info.c106_address_id%TYPE;
      v_delivery_carrier   t504_consignment.c504_delivery_carrier%TYPE;
      v_delivery_mode      t504_consignment.c504_delivery_mode%TYPE;
      v_shipid             VARCHAR2 (20);
      v_reqatt             VARCHAR2 (2000);
      v_prod_req_id        t520_request.c525_product_request_id%TYPE;

   BEGIN
      SELECT c520_required_date, c901_request_source, c520_request_txn_id,
             c207_set_id, c520_request_for, c520_request_to,
             c901_request_by_type, c520_request_by, c901_ship_to,
             c520_ship_to_id, c520_status_fl, c901_purpose,c525_product_request_id
        INTO v_required_date, v_request_source, v_request_txn_id,
             v_set_id, v_request_for, v_request_to,
             v_request_by_type, v_request_by, v_ship_to,
             v_ship_to_id_by, v_status_fl, v_purpose,v_prod_req_id
        FROM t520_request
       WHERE c520_request_id = p_master_request_id AND c520_void_fl IS NULL;

       --PBUG-4036 adding the product request id column to get the request id in paperwork after split GM-CN of PMT-45058
       gm_pkg_op_request_master.gm_sav_request (NULL,
                                               v_required_date,
                                               v_request_source,
                                               v_request_txn_id,
                                               v_set_id,
                                               v_request_for,
                                               v_request_to,
                                               v_request_by_type,
                                               v_request_by,
                                               v_ship_to,
                                               v_ship_to_id_by,
                                               p_master_request_id,
                                               v_status_fl,
                                               p_user_id,
                                               v_purpose,
                                               p_out_request_id,
                                               NULL,
                                               v_prod_req_id
                                              );
                                              
	-- Start of updating the Request Attribute Values from the Parent Request 
   
     SELECT distinct RTRIM(XMLAGG(XMLELEMENT(e,(C901_ATTRIBUTE_TYPE || '^' || C520A_ATTRIBUTE_VALUE )|| '|')).EXTRACT('//text()'),',') INTO v_reqatt 
       FROM T520A_REQUEST_ATTRIBUTE
      WHERE C520_REQUEST_ID = p_master_request_id
        AND C520A_VOID_FL  IS NULL;
   
    IF v_reqatt            IS NOT NULL THEN
        gm_pkg_op_request_master.gm_sav_request_attribute (p_out_request_id, v_reqatt, p_user_id) ;
    END IF;
    
    -- END
      SELECT COUNT (1)
        INTO v_req_count
        FROM t907_shipping_info
       WHERE c907_ref_id = p_master_request_id AND c907_void_fl IS NULL;

      IF v_req_count > 0
      THEN
         BEGIN
            SELECT c106_address_id, c901_delivery_mode, c901_delivery_carrier,c907_ship_to_id
              INTO v_addressid, v_delivery_mode, v_delivery_carrier,v_ship_to_id_by
              FROM t907_shipping_info t907
             WHERE t907.c907_ref_id = p_master_request_id
               AND t907.c907_void_fl IS NULL;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               v_addressid := NULL;
         END;

         gm_pkg_cm_shipping_trans.gm_sav_shipping (p_out_request_id,
                                                   50184,           --request,
                                                   v_ship_to,
                                                   v_ship_to_id_by,
                                                   v_delivery_carrier,
                                                   v_delivery_mode,
                                                   NULL,
                                                   v_addressid,
                                                   NULL,
                                                   p_user_id,
                                                   v_shipid
                                                  );
      END IF;

      UPDATE t520_request
         SET c202_project_id = p_project_id
           , c520_last_updated_by = p_user_id
           , c520_last_updated_date = SYSDATE
       WHERE c520_request_id = p_out_request_id 
         AND c520_void_fl IS NULL;
   END gm_sav_split_request;

   PROCEDURE gm_sav_split_request_pts (
      p_request_id      IN   t520_request.c520_request_id%TYPE,
      p_master_req_id   IN   t520_request.c520_request_id%TYPE,
      p_project_id      IN   t205_part_number.c202_project_id%TYPE,
      p_user_id         IN   t520_request.c520_created_by%TYPE
   )
   AS
   BEGIN
      UPDATE t521_request_detail t521
         SET c520_request_id = p_request_id
       WHERE c520_request_id = p_master_req_id
         AND t521.c205_part_number_id IN (
                SELECT v205.c205_part_number_id
                  FROM t521_request_detail t521, V205_PARTS_WITH_EXT_VENDOR v205
                 WHERE t521.c520_request_id = p_master_req_id
                   AND v205.c205_part_number_id = t521.c205_part_number_id
                   AND v205.c202_project_id = p_project_id);
   END gm_sav_split_request_pts;

   PROCEDURE gm_sav_split_consg (
      p_consignment_id   IN       t504_consignment.c504_consignment_id%TYPE,
      p_request_id       IN       t520_request.c520_request_id%TYPE,
      p_project_id       IN       t205_part_number.c202_project_id%TYPE,
      p_user_id          IN       t504_consignment.c504_created_by%TYPE,
      p_out_consign_id   OUT      t504_consignment.c504_consignment_id%TYPE
   )
   AS
      v_distributor_id          t504_consignment.c701_distributor_id%TYPE;
      v_ship_date               t504_consignment.c504_ship_date%TYPE;
      v_return_date             t504_consignment.c504_return_date%TYPE;
      v_comments                t504_consignment.c504_comments%TYPE;
      v_total_cost              t504_consignment.c504_total_cost%TYPE;
      v_status_fl               t504_consignment.c504_status_fl%TYPE;
      v_void_fl                 t504_consignment.c504_void_fl%TYPE;
      v_account_id              t504_consignment.c704_account_id%TYPE;
      v_ship_to                 t504_consignment.c504_ship_to%TYPE;
      v_set_id                  t504_consignment.c207_set_id%TYPE;
      v_ship_to_id              t504_consignment.c504_ship_to_id%TYPE;
      v_final_comments          t504_consignment.c504_final_comments%TYPE;
      v_inhouse_purpose         t504_consignment.c504_inhouse_purpose%TYPE;
      v_type                    t504_consignment.c504_type%TYPE;
      v_delivery_carrier        t504_consignment.c504_delivery_carrier%TYPE;
      v_delivery_mode           t504_consignment.c504_delivery_mode%TYPE;
      v_tracking_number         t504_consignment.c504_tracking_number%TYPE;
      v_ship_req_fl             t504_consignment.c504_ship_req_fl%TYPE;
      v_verify_fl               t504_consignment.c504_verify_fl%TYPE;
      v_verified_date           t504_consignment.c504_verified_date%TYPE;
      v_verified_by             t504_consignment.c504_verified_by%TYPE;
      v_update_inv_fl           t504_consignment.c504_update_inv_fl%TYPE;
      v_reprocess_id            t504_consignment.c504_reprocess_id%TYPE;
      v_master_consignment_id   t504_consignment.c504_master_consignment_id%TYPE;
      v_request_id              t504_consignment.c520_request_id%TYPE;
      v_shipid                  VARCHAR2 (20);
      v_addressid               t907_shipping_info.c106_address_id%TYPE;
   BEGIN
      -- -- Fetching parent Consignment details
      SELECT c701_distributor_id, c504_ship_date, c504_return_date,
             c504_comments, c504_total_cost, c504_status_fl, c504_void_fl,
             c704_account_id, c504_ship_to, c207_set_id, c504_ship_to_id,
             c504_final_comments, c504_inhouse_purpose, c504_type,
             c504_delivery_carrier, c504_delivery_mode,
             c504_tracking_number, c504_ship_req_fl, c504_verify_fl,
             c504_verified_date, c504_verified_by, c504_update_inv_fl,
             c504_reprocess_id, c504_master_consignment_id, c520_request_id
        INTO v_distributor_id, v_ship_date, v_return_date,
             v_comments, v_total_cost, v_status_fl, v_void_fl,
             v_account_id, v_ship_to, v_set_id, v_ship_to_id,
             v_final_comments, v_inhouse_purpose, v_type,
             v_delivery_carrier, v_delivery_mode,
             v_tracking_number, v_ship_req_fl, v_verify_fl,
             v_verified_date, v_verified_by, v_update_inv_fl,
             v_reprocess_id, v_master_consignment_id, v_request_id
        FROM t504_consignment
       WHERE c504_consignment_id = p_consignment_id AND c504_void_fl IS NULL;

      gm_pkg_op_request_master.gm_sav_consignment (v_distributor_id,
                                                   v_ship_date,
                                                   v_return_date,
                                                   v_comments,
                                                   v_total_cost,
                                                   v_status_fl,
                                                   v_void_fl,
                                                   v_account_id,
                                                   v_ship_to,
                                                   v_set_id,
                                                   v_ship_to_id,
                                                   v_final_comments,
                                                   v_inhouse_purpose,
                                                   v_type,
                                                   v_delivery_carrier,
                                                   v_delivery_mode,
                                                   v_tracking_number,
                                                   v_ship_req_fl,
                                                   v_verify_fl,
                                                   v_verified_date,
                                                   v_verified_by,
                                                   v_update_inv_fl,
                                                   v_reprocess_id,
                                                   p_consignment_id,
                                                   p_request_id,
                                                   p_user_id,
                                                   p_out_consign_id
                                                  );
      gm_sav_consg_project (p_out_consign_id, p_project_id, p_user_id);

      BEGIN
         SELECT c106_address_id, c901_delivery_mode, c901_delivery_carrier ,c907_ship_to_id
           INTO v_addressid, v_delivery_mode, v_delivery_carrier,v_ship_to_id
           FROM t907_shipping_info t907
          WHERE t907.c907_ref_id = p_consignment_id
            AND t907.c907_void_fl IS NULL;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            v_addressid := NULL;
      END;

      gm_pkg_cm_shipping_trans.gm_sav_shipping (p_out_consign_id,
                                                50181,         --consignmants,
                                                v_ship_to,
                                                v_ship_to_id,
                                                v_delivery_carrier,
                                                v_delivery_mode,
                                                NULL,
                                                v_addressid,
                                                NULL,
                                                p_user_id,
                                                v_shipid
                                               );
   END gm_sav_split_consg;

   PROCEDURE gm_sav_split_consg_parts (
      p_consignment_id   IN   t504_consignment.c504_consignment_id%TYPE,
      p_parent_con_id    IN   t504_consignment.c504_consignment_id%TYPE,
      p_project_id       IN   t205_part_number.c202_project_id%TYPE,
      p_user_id          IN   t504_consignment.c504_created_by%TYPE
   )
   AS
   BEGIN
      UPDATE t505_item_consignment t505
         SET t505.c504_consignment_id = p_consignment_id
       WHERE t505.c504_consignment_id = p_parent_con_id
         AND t505.c205_part_number_id IN (
                SELECT t505.c205_part_number_id
                  FROM t505_item_consignment t505, V205_PARTS_WITH_EXT_VENDOR v205
                 WHERE t505.c504_consignment_id = p_parent_con_id
                   AND v205.c205_part_number_id = t505.c205_part_number_id
                   AND v205.c202_project_id = p_project_id
                   AND t505.c505_void_fl IS NULL);
     
 		--When ever the detail is updated, the consignment table has to be updated ,so ETL can Sync it to GOP.
	 	UPDATE t504_consignment
		   SET c504_last_updated_by    = p_user_id
	  		 , c504_last_updated_date  = SYSDATE
	  	 WHERE c504_consignment_id  IN ( p_parent_con_id, p_consignment_id);			  
   END gm_sav_split_consg_parts;

   PROCEDURE gm_sav_consg_project (
      p_consignment_id   IN   t504_consignment.c504_consignment_id%TYPE,
      p_project_id       IN   t205_part_number.c202_project_id%TYPE,
      p_user_id          IN   t504_consignment.c504_created_by%TYPE
   )
   AS
   BEGIN
      gm_pkg_op_consignment.gm_sav_consign_attribute (NULL,
                                                      p_consignment_id,
                                                      '11241',
                                                      p_project_id,
                                                      p_user_id
                                                     );
   END gm_sav_consg_project;

   PROCEDURE gm_fch_consg_req_details (
      p_request_id         IN       t520_request.c520_request_id%TYPE,
      p_project_id         IN       t205_part_number.c202_project_id%TYPE,
      p_cnsg_summary_cur   OUT      TYPES.cursor_type,
      p_cnsgnmat_cur       OUT      TYPES.cursor_type
   )
   AS
      v_request_id   t520_request.c520_request_id%TYPE;
      v_req_count    NUMBER;
   BEGIN
       OPEN p_cnsg_summary_cur
       FOR
        SELECT DISTINCT  rqid,  pnum, get_partnum_desc (pnum) pdesc
                                       , qty, lvl
                              FROM
                              (
                                 WITH data AS
                                 (SELECT t520.c520_request_id rqid, t521.c205_part_number_id pnum,
               t521.c521_qty qty
          FROM t520_request t520, t521_request_detail t521
         WHERE t520.c520_request_id = t521.c520_request_id(+)
           AND (   t520.c520_request_id = p_request_id
                OR t520.c520_master_request_id = p_request_id
               )
           AND t520.c202_project_id = p_project_id
           AND t520.c520_void_fl IS NULL
        UNION
        SELECT t520.c520_request_id reqid, t505.c205_part_number_id pnum,
               t505.c505_item_qty qty
          FROM t520_request t520,
               t504_consignment t504,
               t505_item_consignment t505,
               t504d_consignment_attribute t504d
         WHERE t520.c520_request_id = t504.c520_request_id(+)
           AND t504.c504_consignment_id = t505.c504_consignment_id(+)
           AND (   t520.c520_request_id = p_request_id
                OR t520.c520_master_request_id = p_request_id
               )
           AND t504.c504_consignment_id = t505.c504_consignment_id
           AND t504.c504_consignment_id = t504d.c504_consignment_id
           AND t504d.c901_attribute_type = 11241
           AND t504d.c504d_attribute_value = p_project_id
           AND t504.c504_void_fl IS NULL
           AND t505.c505_void_fl IS NULL
           AND t504d.c504d_void_fl IS NULL)
           SELECT rqid, pnum,  1 qty, LEVEL lvl
                                    FROM data
                                    CONNECT BY LEVEL <= QTY )
                                    WHERE pnum IS NOT NULL
                                    order by rqid, pnum, pdesc;
      BEGIN
         SELECT reqid
           INTO v_request_id
           FROM (SELECT DISTINCT t520.c520_request_id reqid
                            FROM t520_request t520,
                                 t504_consignment t504,
                                 t505_item_consignment t505,
                                 t504d_consignment_attribute t504d
                           WHERE t520.c520_request_id = t504.c520_request_id(+)
                             AND t504.c504_consignment_id = t505.c504_consignment_id(+)
                             AND (   t520.c520_request_id = p_request_id
                                  OR t520.c520_master_request_id =
                                                                  p_request_id
                                 )
                             AND t504.c504_consignment_id =
                                                      t505.c504_consignment_id
                             AND t504.c504_consignment_id =
                                                     t504d.c504_consignment_id
                             AND t504d.c901_attribute_type = 11241
                             AND t504d.c504d_attribute_value = p_project_id
                             AND t504.c504_void_fl IS NULL
                             AND t505.c505_void_fl IS NULL
                             AND t504d.c504d_void_fl IS NULL
                 UNION
                 SELECT DISTINCT t520.c520_request_id reqid
                            -- INTO v_request_id
                 FROM            t520_request t520, t521_request_detail t521
                           WHERE t520.c520_request_id = t521.c520_request_id(+)
                             AND (   t520.c520_request_id = p_request_id
                                  OR t520.c520_master_request_id =
                                                                  p_request_id
                                 )
                             AND t520.c202_project_id = p_project_id
                             AND t520.c520_void_fl IS NULL);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            v_request_id := NULL;
         WHEN TOO_MANY_ROWS
         THEN
            SELECT  MAX (t520.c520_request_id)
                       INTO v_request_id
                       FROM t520_request t520, t521_request_detail t521
                      WHERE t520.c520_request_id = t521.c520_request_id(+)
                        AND (   t520.c520_request_id = p_request_id
                             OR t520.c520_master_request_id = p_request_id
                            )
                        AND t520.c202_project_id = p_project_id
                        AND t520.c520_void_fl IS NULL;
      END;

      OPEN p_cnsgnmat_cur
       FOR
          SELECT t520.c520_request_id rqid, t520.c520_created_date cdate,
                 t520.c207_set_id setid,
                 get_request_ship_details (v_request_id,'MAIL') repemail,
                 DECODE(t520.c901_ship_to,4123,get_request_ship_details (v_request_id,'ADD'),
                 gm_pkg_cm_shipping_info.get_ship_add (v_request_id,
                                                       50184
                                                      )) shipadd,
                 gm_pkg_cm_shipping_info.get_ship_mode
                                                      (v_request_id,
                                                       50184
                                                      ) shipmode,
                 gm_pkg_cm_shipping_info.get_carrier (v_request_id,
                                                      50184
                                                     ) carrier
            FROM t520_request t520
           WHERE t520.c520_request_id = v_request_id
             AND t520.c520_void_fl IS NULL
             AND t520.c202_project_id = p_project_id
             AND t520.c520_email_fl IS NULL;

      -- AND t520.c520_ship_to_id IS NOT NULL;
      UPDATE t520_request t520
         SET t520.c520_email_fl = 'Y'
           , c520_last_updated_date = SYSDATE 
       WHERE ( t520.c520_request_id = v_request_id)-- OR t520.c520_master_request_id =  v_request_id)
         AND t520.c520_void_fl IS NULL;
   END gm_fch_consg_req_details;
   
	/********************************************************
	* Author      : Dhana Reddy
	* Description : update order type to Bill only loaner if all the parts are loaner parts .
	* ******************************************************/
	PROCEDURE gm_update_do_type (
	   p_order_id IN t501_order.c501_order_id%TYPE,
	   p_user_id  IN t501_order.c501_created_by%TYPE)
	AS
	    v_tot_csgn_item NUMBER;
	    v_ship_cost     NUMBER;
	    v_msg           VARCHAR2(20);
	    CURSOR order_cur
	    IS
	         SELECT t501.c501_order_id order_id, NVL (c901_order_type, '-999999') order_type
	           FROM t501_order t501
	          WHERE
	            (
	                t501.c501_parent_order_id = p_order_id
	                OR t501.c501_order_id     = p_order_id
	            )                                                 --'1149-120306-12'
	            AND NVL (t501.c901_order_type, '-999999') <> 2525 -- BACK ORDER
	            AND t501.c501_void_fl                     IS NULL
	            AND t501.c501_delete_fl                   IS NULL
	            AND (t501.c901_ext_country_id              IS NULL OR t501.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes));-- To include ext country id for GH
	BEGIN
	    FOR v_order_cur IN order_cur
	    LOOP
	        IF v_order_cur.order_type = '-999999' THEN
	             SELECT COUNT (1)
	               INTO v_tot_csgn_item
	               FROM t502_item_order
	              WHERE c501_order_id = v_order_cur.order_id
	                AND c901_type     = 50300 -- Consignment
	                AND c502_void_fl IS NULL;
	            IF (v_tot_csgn_item   = 0) THEN
	             UPDATE t501_order
	                SET c501_last_updated_by = p_user_id, c501_last_updated_date = SYSDATE, c901_order_type = 2530 -- Bill
	                    -- Only Loaner
	                    , c501_status_fl  = 3, c501_shipping_date = SYSDATE
	              WHERE c501_order_id = v_order_cur.order_id
	                AND c501_void_fl IS NULL;
	                
	        	--get ship cost of parent and pass below procedure to keep shipping cost at parent level.
			      BEGIN
			           SELECT c501_ship_cost
			              INTO v_ship_cost
			              FROM t501_order
			              WHERE c501_order_id = p_order_id
			                AND c501_void_fl IS NULL;
			       EXCEPTION 
			          WHEN NO_DATA_FOUND THEN
			          v_ship_cost :=0;
			      END;
 				-- to update invflag and posting.
 				gm_pkg_cm_shipping_trans.gm_sav_order_ship(v_order_cur.order_id,v_ship_cost,p_user_id,v_msg);
 				
	             UPDATE t502_item_order
	                SET c502_control_number = 'NOC#'
	              WHERE c501_order_id   = v_order_cur.order_id
	                AND c502_void_fl IS NULL;
	            END IF;
	        END IF;
	    END LOOP;
	END gm_update_do_type;
	
	/*****************************************************************************************
	Description : Procedure to change the order type to Bill Only - From Sales Consignments
	Author : Karthik 
 	****************************************************************************************/
	
	PROCEDURE gm_sav_back_ord_to_boc_ord (
  		 p_order_id       IN      t501_order.c501_order_id%TYPE,
  		 p_user_id		  IN      t501_order.c501_last_updated_by%TYPE )
	AS
	v_ord_type t501_order.c901_order_type%TYPE;
	BEGIN
		
		BEGIN
			SELECT c901_order_type 
				INTO v_ord_type
				FROM t501_order
			WHERE C501_ORDER_ID = p_order_id
				AND C501_VOID_FL  IS NULL;
			EXCEPTION WHEN NO_DATA_FOUND THEN
				v_ord_type := NULL;
		END;
		
      --PMT-39847 loaner-parts-type-in-bill---on
      --checking if the order type is not back order - 2525 and loaner - 2530
		/* IF it's not back order then it will not update the order type */  
		IF v_ord_type != '2525' AND v_ord_type != '2530'
	   	THEN
		   RETURN;
	    END IF;
	    
		-- Updating Order type to -'Bill Only - From Sales Consignments' and status flag to 3
		UPDATE t501_order
		   SET c901_order_type = 2532
			 , c501_status_fl = 3
			 , c501_update_inv_fl = 1
			 , c501_shipping_date = SYSDATE
			 , c501_last_updated_by = p_user_id
			 , c501_last_updated_date = SYSDATE
		 WHERE c501_order_id = p_order_id
		 	AND c501_void_fl IS NULL;
		 
		 -- update the control number of the part
		UPDATE t502_item_order
			SET c502_control_number = 'NOC#',
			c901_type = '50300'
		 WHERE c501_order_id = p_order_id
			--AND c901_type = '50300' --Sales Consignment
			AND c502_void_fl IS NULL
			AND c502_delete_fl IS NULL;
		 
		 -- void the shipping record
		UPDATE t907_shipping_info
			SET c907_void_fl = 'Y'
				, c907_last_updated_by = p_user_id
				, c907_last_updated_date = SYSDATE
		 WHERE c907_ref_id = p_order_id
			AND c907_void_fl IS NULL;
			
	END gm_sav_back_ord_to_boc_ord; 
	
	/*****************************************************************************************
	Description : Below Procedure is used  to update the plant in Request and Shipping table
	Author      : Karthik 
 	****************************************************************************************/
   
   PROCEDURE gm_sav_plant_req_update (
      p_req_id	     IN   t520_request.c520_request_id%TYPE,
      p_project_id   IN   t205_part_number.c202_project_id%TYPE,
      p_user_id      IN   t501_order.c501_created_by%TYPE
   )
    AS
      v_viacell_Prj_fl     t906_rules.c906_rule_value%TYPE;
      v_plant_id     	   t5040_plant_master.C5040_PLANT_ID%TYPE;

   BEGIN
		BEGIN
			 SELECT DISTINCT V205.C906_RULE_VALUE
          		INTO v_plant_id 
        	FROM T504_CONSIGNMENT t504 ,
        		 T505_ITEM_CONSIGNMENT T505 ,
        		 V205_PARTS_WITH_EXT_VENDOR V205
         WHERE T504.C520_REQUEST_ID = p_req_id
            AND T504.C504_CONSIGNMENT_ID = T505.C504_CONSIGNMENT_ID
         	AND T505.C205_PART_NUMBER_ID = V205.C205_PART_NUMBER_ID
            AND T504.C1900_COMPANY_ID    = V205.C1900_COMPANY_ID
            AND T504.C504_VOID_FL IS NULL
            AND T505.C505_VOID_FL IS NULL;
		EXCEPTION
		WHEN NO_DATA_FOUND THEN
		  v_plant_id := NULL;
		END;

		IF v_plant_id IS NOT NULL THEN  
	       UPDATE T520_REQUEST 
      		   SET C5040_PLANT_ID = v_plant_id 
     	 	WHERE C520_REQUEST_ID = p_req_id
     	 	  AND C520_REQUEST_ID IN ( SELECT DISTINCT T504.C520_REQUEST_ID
     	 	                            FROM V205_PARTS_WITH_EXT_VENDOR V205
     	 	                               , T504_CONSIGNMENT t504
     	 	                               , T505_ITEM_CONSIGNMENT t505 
     	 	                           WHERE t505.c205_part_number_id = v205.c205_part_number_id
     	 	                           	 AND T504.C504_CONSIGNMENT_ID  = t505.C504_CONSIGNMENT_ID
     	 	                           	 AND t504.c1900_company_id    = v205.c1900_company_id
     	 	                             AND t504.C520_REQUEST_ID = p_req_id
     	 	                         )  
       		 AND C520_VOID_FL IS NULL;
       		 
		 UPDATE T907_SHIPPING_INFO 
       	      SET c5040_plant_id = v_plant_id 
       		WHERE C907_REF_ID = p_req_id
       		  AND C907_REF_ID IN  ( SELECT DISTINCT T504.C520_REQUEST_ID
     	 	                            FROM V205_PARTS_WITH_EXT_VENDOR V205
     	 	                               , T504_CONSIGNMENT t504
     	 	                               , T505_ITEM_CONSIGNMENT t505 
     	 	                           WHERE t505.c205_part_number_id = v205.c205_part_number_id
     	 	                           	 AND T504.C504_CONSIGNMENT_ID  = t505.C504_CONSIGNMENT_ID
     	 	                           	 AND t504.c1900_company_id    = v205.c1900_company_id
     	 	                             AND t504.C520_REQUEST_ID = p_req_id
     	 	                         )  
       		 AND c907_void_fl IS NULL;
    	 END IF;      	
   END 	gm_sav_plant_req_update;
   
   /******************************************************************************************************
	Description : Below Procedure is used  to update the plant in Request table if the request is backlog
	Author      : Karthik 
 	******************************************************************************************************/
   
   PROCEDURE gm_sav_plant_req_bo_update (
      p_bo_req_id	     IN   t520_request.c520_request_id%TYPE,
      p_project_id   IN   t205_part_number.c202_project_id%TYPE,
      p_user_id      IN   t501_order.c501_created_by%TYPE
   )
    AS
      v_viacell_Prj_fl     t906_rules.c906_rule_value%TYPE;
      v_plant_id     	   t5040_plant_master.C5040_PLANT_ID%TYPE;

   BEGIN
		BEGIN
			  SELECT DISTINCT V205.C906_RULE_VALUE
          		INTO v_plant_id 
        	FROM T520_REQUEST T520 ,
        		 T521_REQUEST_DETAIL t521 ,
        		 V205_PARTS_WITH_EXT_VENDOR V205
         WHERE T520.C520_REQUEST_ID = p_bo_req_id
            AND T520.C520_REQUEST_ID = t521.C520_REQUEST_ID
         	AND t521.C205_PART_NUMBER_ID = V205.C205_PART_NUMBER_ID
            AND T520.C1900_COMPANY_ID    = V205.C1900_COMPANY_ID
            AND T520.C520_VOID_FL IS NULL;
		EXCEPTION
		WHEN NO_DATA_FOUND THEN
		  v_plant_id := NULL;
		END;

		IF v_plant_id IS NOT NULL THEN 
		UPDATE T520_REQUEST 
      		   SET C5040_PLANT_ID = v_plant_id
     	 	WHERE C520_REQUEST_ID = p_bo_req_id
     	 	  AND C520_REQUEST_ID IN ( SELECT DISTINCT t521.C520_REQUEST_ID
     	 	                            FROM V205_PARTS_WITH_EXT_VENDOR V205
     	 	                               , T521_REQUEST_DETAIL t521
     	 	                               , T520_REQUEST T520
     	 	                           WHERE t521.c205_part_number_id = v205.c205_part_number_id
     	 	                           	 AND t521.C520_REQUEST_ID  = T520.C520_REQUEST_ID
     	 	                             AND t521.C520_REQUEST_ID = p_bo_req_id
     	 	                         )  
       		 AND C520_VOID_FL IS NULL;
       		 
		 UPDATE T907_SHIPPING_INFO 
       	      SET c5040_plant_id = v_plant_id 
       		WHERE C907_REF_ID = p_bo_req_id
       		  AND C907_REF_ID IN  ( SELECT DISTINCT t521.C520_REQUEST_ID
     	 	                            FROM V205_PARTS_WITH_EXT_VENDOR V205
     	 	                               , T521_REQUEST_DETAIL t521
     	 	                               , T520_REQUEST T520
     	 	                           WHERE t521.c205_part_number_id = v205.c205_part_number_id
     	 	                           	 AND t521.C520_REQUEST_ID  = T520.C520_REQUEST_ID
     	 	                             AND t521.C520_REQUEST_ID = p_bo_req_id
     	 	                         )  
       		 AND c907_void_fl IS NULL;
    	 END IF;      	
   END 	gm_sav_plant_req_bo_update;
   
    /*********************************************************************************************
	Description : Below Procedure is used  to update the plant in Consignment and Shipping table
	Author      : Karthik 
 	**********************************************************************************************/
   PROCEDURE gm_sav_plant_consgn_update (
      p_consignment_id	     IN   t504_consignment.c504_consignment_id%TYPE,
      p_project_id   		 IN   t205_part_number.c202_project_id%TYPE,
      p_user_id      		 IN   t501_order.c501_created_by%TYPE
   )
    AS
      v_viacell_Prj_fl     t906_rules.c906_rule_value%TYPE;
      v_plant_id     	   t5040_plant_master.C5040_PLANT_ID%TYPE;

   BEGIN
		BEGIN
			 SELECT DISTINCT V205.C906_RULE_VALUE
          		INTO v_plant_id 
        	FROM T504_CONSIGNMENT t504 ,
        		 T505_ITEM_CONSIGNMENT T505 ,
        		 V205_PARTS_WITH_EXT_VENDOR V205
         WHERE T504.C504_CONSIGNMENT_ID = p_consignment_id
            AND T504.C504_CONSIGNMENT_ID = T505.C504_CONSIGNMENT_ID
         	AND T505.C205_PART_NUMBER_ID = V205.C205_PART_NUMBER_ID
            AND T504.C1900_COMPANY_ID    = V205.C1900_COMPANY_ID
            AND T504.C504_VOID_FL IS NULL
            AND T505.C505_VOID_FL IS NULL;
		EXCEPTION
		WHEN NO_DATA_FOUND THEN
		  v_plant_id := NULL;
		END;

	  IF v_plant_id IS NOT NULL THEN  
	       UPDATE T504_CONSIGNMENT 
      		   SET C5040_PLANT_ID = v_plant_id 
     	 	WHERE C504_CONSIGNMENT_ID = p_consignment_id
     	 	  AND C504_CONSIGNMENT_ID IN ( SELECT DISTINCT T504.C504_CONSIGNMENT_ID
     	 	                            FROM V205_PARTS_WITH_EXT_VENDOR V205
     	 	                               , T504_CONSIGNMENT t504
     	 	                               , T505_ITEM_CONSIGNMENT t505 
     	 	                           WHERE t505.c205_part_number_id = v205.c205_part_number_id
     	 	                           	 AND T504.C504_CONSIGNMENT_ID  = t505.C504_CONSIGNMENT_ID
     	 	                           	 AND t504.c1900_company_id    = v205.c1900_company_id
     	 	                             AND t504.C504_CONSIGNMENT_ID = p_consignment_id
     	 	                         )  
       		 AND C504_VOID_FL IS NULL;
       		 
		 UPDATE T907_SHIPPING_INFO 
       	      SET c5040_plant_id = v_plant_id 
       		WHERE C907_REF_ID = p_consignment_id
       		  AND C907_REF_ID IN  ( SELECT DISTINCT T504.C504_CONSIGNMENT_ID
     	 	                            FROM V205_PARTS_WITH_EXT_VENDOR V205
     	 	                               , T504_CONSIGNMENT t504
     	 	                               , T505_ITEM_CONSIGNMENT t505 
     	 	                           WHERE t505.c205_part_number_id = v205.c205_part_number_id
     	 	                           	 AND T504.C504_CONSIGNMENT_ID  = t505.C504_CONSIGNMENT_ID
     	 	                           	 AND t504.c1900_company_id    = v205.c1900_company_id
     	 	                             AND t504.C504_CONSIGNMENT_ID = p_consignment_id
     	 	                         )  
       		 AND c907_void_fl IS NULL;
    	 END IF;      	
   END 	gm_sav_plant_consgn_update;
   
   
   /*********************************************************************************************
	Description : Below Procedure is used  to create new consignment id in t04_consignment table for PMT-33513
	Author      : Swetha 
 	**********************************************************************************************/
   PROCEDURE gm_save_item_consign  (
     p_consignment_id            IN t504_consignment.c504_consignment_id%TYPE,
     p_distributor_id            IN t504_consignment.c701_distributor_id%TYPE,
     p_ship_date                 IN t504_consignment.c504_ship_date%TYPE,
     p_return_date               IN t504_consignment.c504_return_date%TYPE,
     p_comments                  IN t504_consignment.c504_comments%TYPE,
     p_total_cost                IN t504_consignment.c504_total_cost%TYPE,
     p_status_fl                 IN t504_consignment.c504_status_fl%TYPE,
     p_void_fl                   IN t504_consignment.c504_void_fl%TYPE,
     p_account_id                IN t504_consignment.c704_account_id%TYPE,
     p_ship_to                   IN t504_consignment.c504_ship_to%TYPE,
     p_set_id                    IN t504_consignment.c207_set_id%TYPE,
     p_ship_to_id                IN t504_consignment.c504_ship_to_id%TYPE,
     p_final_comments            IN t504_consignment.c504_final_comments%TYPE,
     p_inhouse_purpose           IN t504_consignment.c504_inhouse_purpose%TYPE,
     p_type                      IN t504_consignment.c504_type%TYPE,
     p_delivery_carrier          IN t504_consignment.c504_delivery_carrier%TYPE,
     p_delivery_mode             IN t504_consignment.c504_delivery_mode%TYPE,
     p_tracking_number           IN t504_consignment.c504_tracking_number%TYPE,
     p_ship_req_fl               IN t504_consignment.c504_ship_req_fl%TYPE,
     p_verify_fl                 IN t504_consignment.c504_verify_fl%TYPE,
     p_verified_date             IN t504_consignment.c504_verified_date%TYPE,
     p_verified_by               IN t504_consignment.c504_verified_by%TYPE,
     p_update_inv_fl             IN t504_consignment.c504_update_inv_fl%TYPE,
     p_reprocess_id              IN t504_consignment.c504_reprocess_id%TYPE,
     p_master_consignment_id     IN t504_consignment.c504_master_consignment_id%TYPE,
     p_request_id                IN t504_consignment.c520_request_id%TYPE,
     p_created_by                IN t504_consignment.c504_created_by%TYPE,
     p_ref_id                    IN t504_consignment.c504_ref_id%TYPE,
     p_reftype                   IN t504_consignment.c901_ref_type%TYPE,
     p_out_consignment_id        OUT t504_consignment.c504_consignment_id%TYPE
   )

    AS
    
    v_company_id  t1900_company.c1900_company_id%TYPE;
    v_plant_id 	  t5040_plant_master.c5040_plant_id%TYPE;
    v_default_company t1900_company.c1900_company_id%TYPE;	
    v_purpose_flag    t504_consignment.c504_inhouse_purpose%TYPE;
    v_shiptoid_flag   t504_consignment.c504_ship_to_id%TYPE;
    v_new_consign_id  t504_consignment.c504_consignment_id%TYPE;
    v_acc_cmp_id  t1900_company.c1900_company_id%TYPE;
    v_user_plant_id 	  t5040_plant_master.c5040_plant_id%TYPE;
    
    BEGIN
	   
	    BEGIN
           --getting company id and plant id mapped from account
        SELECT c1900_company_id,gm_pkg_op_plant_rpt.get_default_plant_for_comp(gm_pkg_it_user.get_default_party_company(p_created_by))
		   INTO v_acc_cmp_id, v_user_plant_id  
		   FROM T704_account 
		WHERE c704_account_id = p_account_id 
		AND c704_void_fl IS NULL;
		EXCEPTION
           WHEN NO_DATA_FOUND
           THEN
           v_acc_cmp_id := NULL;
           v_user_plant_id := NULL;
        END;
        
	SELECT  GET_COMPID_FRM_CNTX(), GET_PLANTID_FRM_CNTX()
        INTO v_company_id, v_plant_id
        FROM DUAL;	

	IF p_inhouse_purpose = '0'
	THEN
		v_purpose_flag		:= '';
	ELSE
		v_purpose_flag		:= p_inhouse_purpose;
	END IF;


	IF p_ship_to_id = '0'
	THEN
		v_shiptoid_flag 	:= '';
	ELSE
		v_shiptoid_flag 	:= p_ship_to_id;
	END IF;
	
	
	SELECT get_plant_parent_comp_id(v_plant_id)
	          INTO v_default_company 
	          FROM DUAL;
	          
	IF v_default_company IS NOT NULL  AND v_default_company !=  v_company_id THEN
	   
	   v_company_id := v_default_company;
	   
	   gm_pkg_cor_client_context.gm_sav_client_context(v_default_company,v_plant_id);
	   
	END IF;
	
	v_new_consign_id := get_next_consign_id(p_type);   
	

	INSERT INTO t504_consignment
				(c504_consignment_id, c701_distributor_id, c504_ship_date, c504_return_date, c504_comments, c504_total_cost, c504_status_fl, 
				 c504_void_fl, c704_account_id, c504_ship_to, c207_set_id, c504_ship_to_id, c504_final_comments, c504_inhouse_purpose, c504_type, 
		         c504_delivery_carrier, c504_delivery_mode, c504_tracking_number, c504_ship_req_fl, c504_verify_fl, c504_verified_date,
		         c504_verified_by, c504_update_inv_fl, c504_reprocess_id, c504_master_consignment_id, c520_request_id, c504_created_by,
		         c504_ref_id, c901_ref_type, c504_created_date, c504_last_updated_by, c504_last_updated_date, c1900_company_id, c5040_plant_id
		        )
        VALUES (v_new_consign_id, p_distributor_id, p_ship_date, p_return_date, p_comments, p_total_cost, p_status_fl, 
		         p_void_fl, p_account_id, p_ship_to, p_set_id, v_shiptoid_flag, p_final_comments, v_purpose_flag, p_type,
		         p_delivery_carrier, p_delivery_mode, p_tracking_number, p_ship_req_fl, p_verify_fl, p_verified_date,
		         p_verified_by, p_update_inv_fl, p_consignment_id, p_master_consignment_id, p_request_id, p_created_by,
		         p_ref_id, p_reftype, CURRENT_DATE, p_created_by, CURRENT_DATE, nvl(v_company_id,v_acc_cmp_id), nvl(v_plant_id,v_user_plant_id)
		       );
	
	IF v_new_consign_id IS NOT NULL THEN
	    p_out_consignment_id := v_new_consign_id;
    END IF;
	
	
 END  gm_save_item_consign;
	
    /*********************************************************************************************
	Description : Below Procedure is used  to update the hospital address for tissue orders for PMT - 40241
	Author      : RAJA 
 	**********************************************************************************************/
   PROCEDURE gm_update_shipping_info (
      p_order_id     IN   t501_order.c501_order_id%TYPE,
      p_project_id   IN   t205_part_number.c202_project_id%TYPE,
      p_user_id      IN   t501_order.c501_created_by%TYPE
       )
    AS
    v_replenishment_skipfl  t906_rules.c906_rule_value%TYPE;
    v_company_id          t1900_company.c1900_company_id%TYPE;
    v_count        		  NUMBER;
    v_part_count		  NUMBER;	
   BEGIN
	   
	   -- to get the company id 
      SELECT get_compid_frm_cntx () INTO v_company_id FROM DUAL;
	  --identify the projects which does not need replenishment
	    BEGIN
		   SELECT get_rule_value_by_company (p_project_id,'TISSUESKIPORDERTYPE', v_company_id) 
	         INTO v_replenishment_skipfl  
	         FROM DUAL;
	       EXCEPTION
	            WHEN NO_DATA_FOUND THEN
	                v_replenishment_skipfl := NULL;
        END; 
    IF v_replenishment_skipfl IS NULL THEN
       	BEGIN
			SELECT count(1)
			  INTO v_count
			  FROM t501_order t501,
			       t907_shipping_info t907,
			       t106_address t106
			 WHERE t501.c501_void_fl              IS NULL
			   AND t501.c501_order_id                = p_order_id
			   AND t907.c907_ref_id                  = t501.c501_order_id
			   AND t907.c907_void_fl                IS NULL
			   AND (t907.c901_ship_to               <> 4122 --Hospital
			   AND (t907.c901_ship_to                = 4121 -- Sales Rep
			   AND NVL(t106.c901_address_type,-999) <> 26240675)) --Hospital
			   AND t907.c106_address_id              = t106.c106_address_id (+)
			   AND t106.C106_VOID_FL(+)             IS NULL;
	    EXCEPTION
	            WHEN NO_DATA_FOUND THEN
	                v_count := 0;
	    END;
	    
	    --PMT#54408
	    SELECT COUNT (pnum) cnt
        INTO v_part_count
        FROM (SELECT t502.c501_order_id, v205.c205_part_number_id pnum
                FROM t502_item_order t502,
                     V205_PARTS_WITH_EXT_VENDOR v205,
                      t501_order t501
               WHERE t502.c501_order_id = p_order_id        
                 AND v205.c205_part_number_id = t502.c205_part_number_id
                 AND t501.c501_order_id       = t502.c501_order_id
 				 AND t501.c1900_company_id    = v205.c1900_company_id
                 AND t502.c502_void_fl IS NULL
                 AND  T502.C901_TYPE = 50300  --Sales Consignment
                 AND t502.c502_delete_fl IS NULL);          
	    
      END IF;
     
      IF v_count > 0 AND v_part_count > 0 THEN
    		GM_RAISE_APPLICATION_ERROR('-20999','451', p_order_id);
      END IF;
      
   END gm_update_shipping_info;
/*********************************************************************************************
Description : Below Procedure is used  to update the hospital address for tissue loaners
Author      : APrasath
**********************************************************************************************/
PROCEDURE gm_update_loaner_shipping_info(
    p_prod_req_id   IN t525_product_request.c525_product_request_id%TYPE,
    p_prod_req_detailid IN t907_shipping_info.c907_ref_id%TYPE,
    p_user_id    IN t525_product_request.c525_created_by%TYPE )
AS
  v_count    NUMBER;
BEGIN

    BEGIN 
      SELECT count(1)
		   INTO v_count
	  FROM t526_product_request_detail t526,
		   t907_shipping_info t907,
		   t106_address t106
	 WHERE t526.c526_void_fl              IS NULL
	   AND t526.c526_product_request_detail_id   = p_prod_req_detailid
	   AND t907.c907_ref_id                  = TO_CHAR(t526.c526_product_request_detail_id)
	   AND t907.c907_void_fl                IS NULL
	   AND (t907.c901_ship_to               <> 4122 --Hospital
	   AND (t907.c901_ship_to                = 4121
	   AND NVL(t106.c901_address_type,-999) <> 26240675)) --Hospital
	   AND t907.c106_address_id              = t106.c106_address_id (+)
	   AND t106.c106_void_fl(+)             IS NULL;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
      v_count := 0;
    END;
    
	IF v_count > 0 THEN
		GM_RAISE_APPLICATION_ERROR('-20999','451', p_prod_req_detailid);
	END IF;

END gm_update_loaner_shipping_info;
/*************************************************************************************************
	* Description : This procedure is used convert order types to Bill Only - From Sales Consignments
	* Author      : APrasath
	**************************************************************************************************/	
	PROCEDURE gm_upd_order_type(
	  p_order_id     IN   t501_order.c501_order_id%TYPE,
      p_user_id      IN   t501_order.c501_created_by%TYPE
	)
	AS
  		v_part_cnt    NUMBER;
	BEGIN
				
		   SELECT COUNT(t502.c205_part_number_id)
         	 INTO v_part_cnt  
		    FROM t502_item_order t502 , t501_order t501
		   WHERE t502.c501_order_id = p_order_id
        	 AND t501.c501_order_id = t502.c501_order_id
        	 AND t501.c501_void_fl IS NULL
		     AND t502.c502_void_fl IS NULL
		     AND t502.c502_delete_fl IS NULL;		
    
		/*
		 * if the part numbers are not available for the orderand the price is $0 value in WIP dash
		 * Then we are removing the order's from WIP Dash by changing the order type to 2532 -- Bill Only - From Sales Consignments
		 */
		IF v_part_cnt = 0 THEN
			UPDATE t501_order t501
			   SET t501.c901_order_type =  2532 -- Bill Only - From Sales Consignments
				 , t501.c501_last_updated_by = p_user_id
				 , t501.c501_last_updated_date = SYSDATE
			 WHERE t501.c501_order_id = p_order_id 
	     		AND c501_void_fl IS NULL;
		END IF;		
	END gm_upd_order_type;
	
	/*******************************************************************************
	* Author      : Gomathi Palani 
	* Description : Check if the posting is executed for the Order transaction.  
	* ******************************************************/
	PROCEDURE gm_sav_txn_posting (
        p_order_id IN t501_order.c501_order_id%TYPE,
        p_user_id  in t501_order.c501_created_by%type)
        
AS

    v_msg VARCHAR2 (100) ;
    v_tmp_order_id t501_order.c501_order_id%type;
    v_posting_txn_count NUMBER;
     
    
    CURSOR order_cur
    IS
         SELECT t501.c501_order_id order_id, NVL (c901_order_type, '-999999') order_type, t501.c501_parent_order_id parent_order_id
           FROM t501_order t501
          WHERE (t501.c501_parent_order_id             = p_order_id
            OR t501.c501_order_id                      = p_order_id)
            AND NVL (t501.c901_order_type, '2521')  IN ('2532', '2530') -- 2532 Bill Only sales consignment
            AND t501.c501_void_fl                     IS NULL
            AND t501.c501_delete_fl                   IS NULL
            AND (t501.c901_ext_country_id             IS NULL
            OR t501.c901_ext_country_id               IN
            (
                 SELECT country_id FROM v901_country_codes
            )) ;-- To include ext country id for GH
            
    BEGIN
    
        FOR v_order_cur IN order_cur
        
        LOOP
        
            v_tmp_order_id := v_order_cur.order_id;                      
            
             SELECT COUNT (1)
               INTO v_posting_txn_count
               FROM t810_posting_txn
              WHERE c810_txn_id     = v_tmp_order_id
                AND NVL(c810_delete_fl,'N')='N';
                
            IF (v_posting_txn_count =0) THEN
            
                gm_update_inventory (v_tmp_order_id, 'S', '', p_user_id, v_msg) ;
                
            END IF;
            
        END LOOP;
    END gm_sav_txn_posting;
    
   
END gm_pkg_op_split;
/
