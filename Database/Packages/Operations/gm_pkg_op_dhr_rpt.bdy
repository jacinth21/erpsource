/* Formatted on 2009/01/21 14:37 (Formatter Plus v4.8.0) */
--@"c:\database\packages\operations\gm_pkg_op_dhr_rpt.bdy";
CREATE OR REPLACE
PACKAGE BODY gm_pkg_op_dhr_rpt
IS
    --
    /************************************************************************
    * Description : Procedure to calculate DHR Counts of different statuses
    * Author   : bvidyasankar
    ************************************************************************/
    --
PROCEDURE gm_fch_dhr_status_cnt (
        p_out_proj_cur OUT TYPES.cursor_type)
AS
v_company_id  t1900_company.c1900_company_id%TYPE;
v_plant_id T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
BEGIN
	SELECT	get_compid_frm_cntx(), NVL(get_plantid_frm_cntx(),3000)
	INTO	v_company_id, v_plant_id
	FROM	dual;
	
    OPEN p_out_proj_cur FOR SELECT NVL(SUM (t408.pi),0) picnt, NVL(SUM (t408.pp),0) ppcnt, NVL(SUM (t408.pv),0) pvcnt FROM
    (SELECT     DECODE (c408_status_fl, '1', 1, 0) pi, DECODE (c408_status_fl, '2', 1, 0) pp, DECODE (c408_status_fl,
            '3', 1, 0) pv
           FROM t408_dhr
          WHERE c408_void_fl IS NULL
            AND c408_dhr_id LIKE 'GM-DHR%'
            AND c1900_company_id = v_company_id
        	AND C5040_PLANT_ID =v_plant_id
    ) t408;
END gm_fch_dhr_status_cnt;
PROCEDURE gm_validate_control_no (
        p_str IN CLOB)
AS
    v_strlen     NUMBER := NVL (LENGTH (p_str), 0) ;
    v_string     CLOB := p_str;
    v_PNum       VARCHAR2 (20) ;
    v_Control    t408_dhr.c408_control_number%TYPE ;
    v_substring  VARCHAR2 (1000) ;
    v_error      VARCHAR2 (4000) ;
    v_count      NUMBER := 0;
    v_cnumFmt    VARCHAR2 (20) ;
    v_fullcnum   t408_dhr.c408_control_number%TYPE ;
    v_dupltCnum  VARCHAR2 (20) ;
    v_wordordid  VARCHAR2 (20) ;
    v_po_id      VARCHAR2 (20);
    v_po_type    t401_purchase_order.C401_TYPE%TYPE;
    v_vend_code  VARCHAR2(2);
    v_count_val  NUMBER := 0;
    v_skip_count NUMBER := 0;
    v_vend_id    t402_work_order.c301_vendor_id%TYPE;
    vprodclass   t205_part_number.c205_product_class%TYPE;
    v_prod_type  VARCHAR2(20);
    v_company_id		t1900_company.c1900_company_id%TYPE;	
    v_serial_need_fl	t205d_part_attribute.c205d_attribute_value%TYPE;
    v_skip_part_count  NUMBER := 0;
BEGIN
	
	SELECT	get_compid_frm_cntx()
	INTO	v_company_id
	FROM	dual;
	
    --
    WHILE INSTR (v_string, '|') <> 0
    LOOP
        v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
        v_string := SUBSTR (v_string, INSTR (v_string, '|') + 1) ;
        v_PNum := NULL;
        v_Control := NULL;
       
        v_PNum := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
        v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1) ;
        v_wordordid := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
        v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1) ;
        v_fullcnum := UPPER (SUBSTR (v_substring, INSTR (v_substring, '^', 1, 1) + 1, ( (INSTR (v_substring, '^', 1, 2)
        - INSTR (v_substring, '^', 1, 1)) - 1))) ;
       v_prod_type := SUBSTR (v_substring,INSTR(v_substring,'^',-1)+1);
        v_cnumFmt := UPPER (get_part_attribute_value (v_PNum, 80134)) ;
        IF v_cnumFmt = 'XXXXXX' OR v_cnumFmt = 'XXXXXX%' THEN
            v_Control := SUBSTR (v_fullcnum, 1, 6) || '%' ;
        ELSE
            v_Control := v_fullcnum ;
        END IF;
      	if  v_prod_type != 100845  
      	THEN
      	
       	BEGIN
		  
    		SELECT t401.c401_purchase_ord_id,  c401_type, t402.c301_vendor_id
		     INTO v_po_id , v_po_type , v_vend_id 
		     FROM t402_work_order t402, t401_purchase_order t401
		    WHERE t401.c401_purchase_ord_id = t402.c401_purchase_ord_id
		      AND t402.c402_work_order_id = v_wordordid
		      AND ROWNUM = 1;
	        EXCEPTION
		        WHEN NO_DATA_FOUND THEN
		            v_po_id := '';
		END;
		
        IF v_Control != 'NOC#' THEN
        
        	v_dupltCnum :=get_part_attribute_value (v_PNum, 80135);
        	IF v_dupltCnum != '80131' THEN 
		SELECT COUNT (1) 
				INTO v_count_val 
				FROM t205_part_number 
				where c205_part_number_id = v_PNum 
				and c205_product_material -- Here we avoiding control number validation for material types (Bone Graft, Tissue)
				IN (select c906_rule_value from t906_rules where c906_rule_grp_id = 'SKIP_DHR_VALIDATION' and c906_void_fl is null); 
		 IF (v_count_val != 0) THEN
    		RETURN;
 			END IF;
	             SELECT     COUNT (1)
	                   INTO v_count
	                   FROM t408_dhr
	                  WHERE UPPER (C408_CONTROL_NUMBER) LIKE v_Control
	                    AND c205_part_number_id != v_PNum
	                    AND C408_VOID_FL IS NULL;
            END IF;
            
            IF (v_count <> 0 ) AND
                (
                    instr (v_error, (v_PNum ||'-'||v_fullcnum)) IS NULL OR instr (v_error, (v_PNum ||'-'||v_fullcnum))
                    = 0
                )
                THEN
                v_error := v_error || (v_PNum ||'-'||v_fullcnum||'<br>') ;
            END IF;
        ELSE
        --106040: Serial Number Needed?
        SELECT c205_product_class, GET_PART_ATTR_VALUE_BY_COMP(c205_part_number_id,106040,v_company_id)
			  INTO vprodclass, v_serial_need_fl
			  FROM t205_part_number
			 WHERE c205_part_number_id = v_PNum;
			 -- If the part is non- sterile,PO type is reowrk,cnum is NOC# then skip the below application error.
	        IF vprodclass = 4031 AND v_po_type = 3101 AND v_Control = 'NOC#'
	        THEN
	        	RETURN;
	        ELSIF v_serial_need_fl = 'Y'
	        THEN
	        	-- Even if the product type is not tissue, if the serial number number flag is 'Y', no need to throw control number validation
	        	RETURN;
	        ELSE        
	        	raise_application_error ('-20908', '') ;
	        END IF;	
        END IF;
        
        
        
        -- Code to check if the lot code is valid or not.
        IF v_po_type = 3101
        THEN
        	v_count := 0;
        	v_skip_count := 0;
        	v_skip_part_count := 0;
        	
        	SELECT SUBSTR(v_Control,0,2) INTO v_vend_code FROM DUAL;
        	
        	SELECT COUNT(1) INTO v_count FROM T301_VENDOR
        	WHERE c301_lot_code = UPPER(v_vend_code);
        	
        	-- SKIP VENDOR LOTCODE VALIDATION FOR SPECIFIC VENDOR FROM RULE
        	SELECT COUNT(1) INTO v_skip_count FROM T906_RULES
        	    WHERE C906_RULE_GRP_ID='SKIP_VENDOR_VALIDATE' 
        	      AND C906_RULE_ID = UPPER(v_vend_id) 
        	      AND C906_VOID_FL IS NULL;
        	      
        	-- Added SKIP_REWORK_VALIDATE for PC-4551-Skip lot validation for specific stelkast parts in rework po process      
        	SELECT COUNT(1) INTO v_skip_part_count FROM T906_RULES      
                   WHERE C906_RULE_GRP_ID='SKIP_REWORK_VALIDATE' 
                     AND C906_RULE_ID = UPPER(v_PNum) 
                     AND C906_VOID_FL IS NULL;
        	
        	-- The entered vendor code is invalid.
        	IF v_count = 0 AND v_skip_count = 0 AND v_skip_part_count = 0
        	THEN
        		GM_RAISE_APPLICATION_ERROR('-20999','197',v_vend_code);
        	END IF;
        END IF;
        END IF;
    END LOOP;
    IF (LENGTH (v_error) > 0) THEN
        v_error := SUBSTR (v_error, 1, LENGTH (v_error) - 1) ;
        GM_RAISE_APPLICATION_ERROR('-20999','198',v_error);
    END IF;
END gm_validate_control_no;

/****************************************************************
 * Description : procedure is used to fetch the DHR dashboard data
 * Author      : Dhanareddy
 *****************************************************************/
PROCEDURE gm_fch_dhr_dashboard (
        p_potype  IN VARCHAR2,
        p_partnum IN t205_part_number.c205_part_number_id%TYPE,
        p_dhr_cur OUT TYPES.cursor_type)
AS
v_company_id  t1900_company.c1900_company_id%TYPE;
v_plant_id T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
BEGIN
	SELECT	get_compid_frm_cntx(), NVL(get_plantid_frm_cntx(),3000)
	INTO	v_company_id, v_plant_id
	FROM	dual;
    my_context.set_my_inlist_ctx (p_potype) ;
    OPEN p_dhr_cur 
     FOR
     SELECT t408.c408_dhr_id ID, get_log_flag (t408.c408_dhr_id, 1205) clog, t408.c301_vendor_id vid,
        c408_status_fl fl, t408.c205_part_number_id partnum, t205.c205_part_num_desc pdesc,
        t301.c301_vendor_name vname, c408_created_date cdate, c408_last_updated_date ldate, 
        t408.c408_control_number cnum, get_mf_wo_matreq_status (t408.c408_dhr_id) matsfl,
        t408.c408_qty_received qtyrec, t408.c408_shipped_qty qtyship
        --duplication of backorder flag is removed
        , NVL (DECODE (t408.c408_status_fl, '1', t408.c408_qty_received, '2', t408.c408_qty_inspected, '3',
        t408.c408_qty_packaged, NULL), 0) qty, (NVL (t408.c408_qty_received, 0) - NVL (t408.c408_qty_rejected, 0) + NVL
        ( dhr_ncmr.close_qty, 0)) tot_qty, get_code_name(t408.c901_dhr_priority) dhr_priority
       FROM t408_dhr t408, t402_work_order t402, t401_purchase_order t401,
        t205_part_number t205, t301_vendor t301,
        (SELECT SUM (c409_closeout_qty) close_qty, t409.c408_dhr_id
           FROM t409_ncmr t409
           -- NCMR processed qty
          WHERE t409.c409_void_fl IS NULL
       GROUP BY t409.c408_dhr_id
        ) dhr_ncmr
      WHERE c408_status_fl            < 4
        AND t408.c408_void_fl        IS NULL
        AND t402.c402_work_order_id   = t408.c402_work_order_id
        AND t401.c401_purchase_ord_id = t402.c401_purchase_ord_id
        AND t401.c401_type           IN
        (SELECT token FROM v_in_list WHERE token IS NOT NULL
        ) -- type check for 3100 Product, 3101 Rework
        AND t408.c408_dhr_id         = dhr_ncmr.c408_dhr_id(+)
        AND t408.c205_part_number_id = t205.c205_part_number_id
        AND t408.c301_vendor_id      = t301.c301_vendor_id(+)
        AND T408.c205_part_number_id = NVL (p_partnum, T408.c205_part_number_id)
        AND T408.c1900_company_id = v_company_id
        AND T408.C5040_PLANT_ID = v_plant_id
	ORDER BY  c901_dhr_priority , c408_created_date ASC;
END gm_fch_dhr_dashboard;

/**************************************************************************************
 * Description : procedure is used to get the generated control number count of a DHR
 * Author      : arajan
 ****************************************************************************************/
PROCEDURE gm_validate_generated_lotNum(
	  p_dhr_id		IN   		t408_dhr.c408_dhr_id%TYPE
	, p_serial_fl	IN			t205d_part_attribute.c205d_attribute_value%TYPE DEFAULT NULL
	, p_donor_id	IN			t408_dhr.c2540_donor_id%TYPE DEFAULT NULL
)
AS
v_count		NUMBER;
BEGIN
	
	BEGIN
		SELECT COUNT(1) INTO v_count
			FROM T408A_DHR_CONTROL_NUMBER 
		WHERE C408_DHR_ID = p_dhr_id
			AND C408A_VOID_FL IS NULL;
		-- For a Not-Tissue / Serialized Parts need to throw the validation if the lot number is not generated
		-- Serial Number is missing, Please enter Serial number to proceed
		IF p_donor_id IS NULL AND p_serial_fl = 'Y' AND v_count = 0
		THEN
			GM_RAISE_APPLICATION_ERROR('-20999','372','');
		END IF;
			
		IF v_count = 0 
        THEN
        		GM_RAISE_APPLICATION_ERROR('-20999','199','');
        END IF;
	END; 
	
END gm_validate_generated_lotNum;

/**************************************************************************************
 * Description : procedure is used to get the assigned control number count of a DHR
 * Author      : arajan
 ****************************************************************************************/
PROCEDURE gm_validate_assign_lotNum(
	p_dhr_id		IN   		t408_dhr.c408_dhr_id%TYPE 
)
AS
v_count		NUMBER;
BEGIN
	
	BEGIN
		SELECT count(1) INTO  v_count
			FROM T413_INHOUSE_TRANS_ITEMS 
		WHERE C412_INHOUSE_TRANS_ID IN (
			SELECT c412_inhouse_trans_id
				FROM t412_inhouse_transactions
			WHERE C412_REF_ID   = p_dhr_id
				AND c412_void_fl IS NULL
			)
		AND C413_CONTROL_NUMBER IS NULL
		AND C413_VOID_FL IS NULL;
			
		IF v_count > 0  
        THEN
        		GM_RAISE_APPLICATION_ERROR('-20999','200','');
        END IF;
	END; 
	
END gm_validate_assign_lotNum;

END gm_pkg_op_dhr_rpt;
/