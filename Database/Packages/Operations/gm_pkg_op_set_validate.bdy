--@"c:\database\packages\operations\gm_pkg_op_set_validate.bdy";
create or replace
PACKAGE BODY gm_pkg_op_set_validate
IS
 /****************************************************************
	 * Description : this procedure is used to validate scan tag
	 * Author      : Yogabalakrishnan
	 *****************************************************************/
PROCEDURE gm_validate_scan_tag (
        p_tag_id IN t5010_tag.c5010_tag_id%TYPE,
        p_operation IN VARCHAR2,
        p_user_id	 IN 	  t5051_allocate_user.c101_user_id%TYPE,
        p_out_loc_id OUT t5053_location_part_mapping.c5052_location_id%TYPE,
        p_out_loc_cd OUT t5052_location_master.c5052_location_cd%TYPE,
        p_out_txn_id OUT t504_consignment.c504_consignment_id%TYPE,
        p_out_txn_type OUT t504_consignment.c504_type%TYPE,
        p_out_today_ship OUT VARCHAR2)
AS
v_loc_id VARCHAR2(50);
BEGIN

  --Fetch tag details: Location_id, transaction_id, transaction_type
  gm_pkg_ac_tag_info.gm_fch_tag_info(p_tag_id,p_out_loc_id, p_out_txn_id, p_out_txn_type) ;

  gm_pkg_op_inv_scan.gm_validate_device_access(p_out_txn_type,p_user_id); -- to validate device access permission

  --IF(v_loc_id IS NOT NULL) THEN
  p_out_loc_cd := gm_pkg_op_inv_scan.GET_LOCATION_CODE(p_out_loc_id); --- Location Code
 -- END IF;

  IF (UPPER(p_operation) <> 'MOVE') THEN
   /* IF (p_out_txn_type= 4119 OR  p_out_txn_type= 4112) THEN -- InHouse Loaner OR InHouse Consignment
      raise_application_error (- 20501, 'Invalid tag') ;
    END IF;

    IF (p_out_txn_type<> 4127 AND  p_out_txn_type<> 4110) THEN -- not Loaner AND Consignment
      raise_application_error (- 20501, 'Invalid tag') ;
    END IF;*/

    GM_PKG_OP_SET_VALIDATE.gm_validate_set_txn_status(p_out_txn_id, p_out_txn_type); -- Validate the tag whther in the required list

    GM_PKG_OP_SET_VALIDATE.gm_validate_ship_date(p_out_txn_id, p_out_txn_type); -- Validate today is ship date or not

  ELSE
    BEGIN
      GM_PKG_OP_SET_VALIDATE.gm_validate_ship_date(p_out_txn_id, p_out_txn_type); -- Validate today is ship date or not

      EXCEPTION
        WHEN OTHERS THEN
          p_out_today_ship :='Y';
    END;
  END IF;

END gm_validate_scan_tag;

/****************************************************************
	 * Description : this procedure throw message if today is ship date else do nothing for Loaner
	 * Author      : Yogabalakrishnan
	 *****************************************************************/
PROCEDURE gm_fch_loaner_ship_date (
        p_txn_id IN t5010_tag.c5010_last_updated_trans_id%TYPE,
        p_txn_type   IN t504_consignment.c504_type%TYPE)
AS
  v_count    NUMBER;
  v_status VARCHAR2 (400) ;
BEGIN

  /*SELECT COUNT (1)
      INTO v_count
      FROM t504_consignment t504, t520_request t520
     WHERE t504.c504_consignment_id    = p_txn_id
       AND t504.c520_request_id        = t520.c520_request_id
       AND TO_CHAR(t520.c520_planned_ship_date,' MM/DD/YYYY')   = TO_CHAR(CURRENT_DATE,' MM/DD/YYYY')
       AND t504.c504_void_fl IS NULL
       AND t520.c520_void_fl IS NULL;*/
BEGIN
	--get_loaner_future_status pass two paras -- tag id and cn id, if no tag id ,get the next status by cn id
   SELECT gm_pkg_op_set_put_rpt.get_loaner_future_status (t5010.c5010_tag_id, p_txn_id) nextstatus into v_status
   FROM t504a_consignment_loaner t504a, t504_consignment t504, t5010_tag t5010
   WHERE t5010.c5010_last_updated_trans_id = t504a.c504_consignment_id
    AND t504a.c504_consignment_id         = t504.c504_consignment_id
    AND t504.c504_consignment_id = p_txn_id
    AND t504.c504_type                    = p_txn_type
    AND t504a.c504a_status_fl             = 58 --pending FG
    AND t504a.c504a_void_fl              IS NULL
    AND t504.c504_void_fl                IS NULL
    AND t5010.c5010_void_fl              IS NULL
    AND ROWNUM=1;--To avoid exact fetch error, need to revisit later as t5010_tag may not be used
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_status := 'Available' ;
    END;
    IF v_status = 'Pending Shipping' THEN

    IF p_txn_type = 4127 THEN -- Loaner Set
        GM_RAISE_APPLICATION_ERROR('-20999','286',''); 

    ELSIF p_txn_type = 4119 THEN
        GM_RAISE_APPLICATION_ERROR('-20999','287',''); 
     END IF;

    END IF;

END gm_fch_loaner_ship_date;

/****************************************************************
	 * Description : this procedure throw message if today is ship date else do nothing for Consignment set
	 * Author      : Yogabalakrishnan
	 *****************************************************************/
PROCEDURE gm_fch_consignment_ship_date (
        p_txn_id IN t5010_tag.c5010_last_updated_trans_id%TYPE,
        p_txn_type   IN t504_consignment.c504_type%TYPE)
AS
  v_count    NUMBER;
BEGIN

  /* SELECT COUNT (1)
           INTO v_count
           FROM t504_consignment t504, t520_request t520
          WHERE t504.c504_consignment_id    = p_txn_id
            AND t504.c520_request_id        = t520.c520_request_id
            AND TO_CHAR(t520.c520_planned_ship_date,'MM/DD/YYYY')   >= TO_CHAR(CURRENT_DATE,'MM/DD/YYYY')
            AND t504.c504_void_fl IS NULL;*/

     SELECT COUNT (1) into v_count
	FROM t520_request t520, t504_consignment t504
  WHERE  t504.c520_request_id = t520.c520_request_id
  AND t504.c504_consignment_id  = p_txn_id
  AND t504.c504_type = p_txn_type
	AND t504.c504_status_fl = '1.20' --pending FG
  AND t520.c520_planned_ship_date <= TRUNC (CURRENT_DATE)
	AND t520.c520_void_fl IS NULL
	AND t504.c504_void_fl IS NULL;

      IF v_count > 0  THEN
        GM_RAISE_APPLICATION_ERROR('-20999','288','');
      END IF;

END gm_fch_consignment_ship_date;

/****************************************************************
	 * Description : This procedure throw message if today is ship date else do nothing
	 * Author      : Yogabalakrishnan
*****************************************************************/
PROCEDURE gm_validate_ship_date (
        p_txn_id IN t5010_tag.c5010_last_updated_trans_id%TYPE,
        p_txn_type   IN t504_consignment.c504_type%TYPE)
AS
BEGIN
    IF (p_txn_type = 4127 OR p_txn_type = 4119) THEN -- Product Loaner OR Inhouse Loaner.
      gm_fch_loaner_ship_date(p_txn_id,p_txn_type);
    END IF;

    IF (p_txn_type = 4110) THEN --Consignment
        gm_fch_consignment_ship_date(p_txn_id,p_txn_type);
    END IF;

END gm_validate_ship_date;

/****************************************************************
	 * Description : This procedure throw message if the tag id scanned is not in the txn status
	 * Author      : Yogabalakrishnan
*****************************************************************/
PROCEDURE gm_validate_set_txn_status (
        p_txn_id IN t5010_tag.c5010_last_updated_trans_id%TYPE,
        p_txn_type   IN t504_consignment.c504_type%TYPE)
AS
  v_count    NUMBER;
  v_status VARCHAR2 (400) ;
  v_code_nm VARCHAR2 (400) ;
  v_plant_id    t5040_plant_master.c5040_plant_id%TYPE;
  
BEGIN
  SELECT  NVL(get_plantid_frm_cntx(),get_rule_value('DEFAULT_PLANT','ONEPORTAL')) INTO  v_plant_id FROM DUAL;

  IF (p_txn_type = 4127 or p_txn_type = 4119) THEN -- Product Loaner set
    BEGIN
      SELECT c504a_status_fl
        INTO v_status
        FROM t504a_consignment_loaner t504a
       WHERE c504_consignment_id = p_txn_id
       -- AND c504a_status_fl ='58' -- Pending FG
        AND c504a_void_fl IS NULL
        AND c5040_plant_id= v_plant_id;

     SELECT gm_pkg_op_loaner.get_loaner_status(v_status)
     INTO v_code_nm FROM DUAL;

       IF v_status <> '58' THEN  -- Pending FG
         GM_RAISE_APPLICATION_ERROR('-20999','289',v_code_nm); 
       END IF;
    END;
  END IF;

  IF (p_txn_type = 4110) THEN -- Consignment set
    BEGIN

    SELECT c504_status_fl
      INTO v_status
        FROM t504_consignment t504
       WHERE c504_consignment_id = p_txn_id
       -- AND c504_status_fl ='1.20' -- Pending FG
        AND c504_void_fl IS NULL
        AND c5040_plant_id= v_plant_id;

        SELECT DECODE(v_status,0,'Iniitiated',1,'WIP',1.10,'Completed Set',1.20,'Pending Putaway', 2,'Built Set',2.20,'Pending Pick',3,'Pending Shipping',4,'Shipped','Closed')
        into v_code_nm
        FROM DUAL;

        IF v_status <> '1.20' THEN --Pending putaway
           GM_RAISE_APPLICATION_ERROR('-20999','289',v_code_nm);
        END IF;
    END;
  END IF;
END gm_validate_set_txn_status;

/****************************************************************
* Description : This procedure is used to validate the location
* Author      : Yogabalakrishnan
*****************************************************************/
PROCEDURE gm_validate_location (
        p_location_id IN t5052_location_master.c5052_location_id%TYPE,
        p_txn_type   IN t504_consignment.c504_type%TYPE )
AS
    v_count    NUMBER;
    v_loc_type NUMBER;
    v_plant_id t5040_plant_master.c5040_plant_id%TYPE;
BEGIN
   BEGIN
	   SELECT  NVL(get_plantid_frm_cntx(),get_rule_value('DEFAULT_PLANT','ONEPORTAL')) INTO  v_plant_id FROM DUAL;
     SELECT c901_location_type
       INTO v_loc_type
       FROM t5052_location_master t5052
      WHERE t5052.c5052_location_id = p_location_id
        AND t5052.c901_status       = '93310' -- Active
        AND t5052.c5052_void_fl    IS NULL
        AND t5052.c5040_plant_id = v_plant_id;
       EXCEPTION
      WHEN NO_DATA_FOUND THEN
        GM_RAISE_APPLICATION_ERROR('-20999','280',''); 
    END;

    IF p_txn_type = 4110 OR p_txn_type = '26240144' THEN -- Consignment,Return set
        IF v_loc_type <> 93501 THEN -- Location type=Consignment
            GM_RAISE_APPLICATION_ERROR('-20999','281',''); 
        END IF;
    ELSIF p_txn_type   = 4127 THEN --Loaner
        IF v_loc_type <> 93502 THEN -- Location type=Loaner
            GM_RAISE_APPLICATION_ERROR('-20999','282','');
        END IF;
     ELSIF p_txn_type   = 4119 THEN --Inhouse Loaner
        IF v_loc_type <> 93503 THEN -- Location type=Inhouse Loaner
            GM_RAISE_APPLICATION_ERROR('-20999','283','');
        END IF;
    END IF;
END gm_validate_location;
/****************************************************************
* Description : this procedure is used to validate the location id
* Author      :
*****************************************************************/
PROCEDURE gm_validate_location_id (
        p_location_id IN t5052_location_master.c5052_location_id%TYPE,
	p_warehouse_id IN T5051_INV_WAREHOUSE.C5051_INV_WAREHOUSE_ID%TYPE,
        p_out_cur OUT TYPES.cursor_type
        )
AS
v_plant_id t5040_plant_master.c5040_plant_id%TYPE;
BEGIN
	-- PMT-43045: Unable to perform manual Loaner Putaway
	SELECT get_plantid_frm_cntx()
	 INTO v_plant_id
	 FROM DUAL;
	 
	-- 
	 
    OPEN p_out_cur FOR
    SELECT get_code_name (t5052.c901_location_type) typeNm, t5052.c901_location_type typeId, t5052.C5052_LOCATION_ID locid
   FROM t5052_location_master t5052 WHERE t5052.c5052_location_cd = p_location_id
   		AND t5052.c901_status = '93310' -- Active
   		AND t5052.c5040_plant_id = v_plant_id
    	AND t5052.c5052_void_fl IS NULL
		AND t5052.c5051_inv_warehouse_id = p_warehouse_id;	--IH, FG, RW warehouse
END gm_validate_location_id;

/****************************************************************
* Description : this procedure is used to get the Tissue Set Flag
* Author      : Mselvamani
*****************************************************************/
PROCEDURE gm_fch_set_tissue_fl(
    p_input_str IN VARCHAR2 ,
    p_out_cur OUT CLOB )
AS
  v_in_list VARCHAR2(3000);
BEGIN
  my_context.set_my_inlist_ctx(p_input_str);
  SELECT JSON_ARRAYAGG ( JSON_OBJECT('setid' VALUE t207.C207_SET_ID, 'tissuefl' VALUE t207.C207_TISSUE_FL) )
  INTO p_out_cur
  FROM t207_set_master t207
  WHERE t207.c207_set_id IN
    (SELECT token FROM v_in_list WHERE token IS NOT NULL
    )
  AND t207.C207_TISSUE_FL = 'Y'
  AND t207.c207_void_fl  IS NULL;
END gm_fch_set_tissue_fl;

END gm_pkg_op_set_validate;
/