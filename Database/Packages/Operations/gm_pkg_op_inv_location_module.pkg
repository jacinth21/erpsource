/* Formatted on 2011/03/10 11:33 (Formatter Plus v4.8.0) */

-- @"C:\Database\Packages\Operations\gm_pkg_op_inv_location_module.pkg"

create or replace
PACKAGE gm_pkg_op_inv_location_module
IS
	PROCEDURE gm_fch_inv_loc_details (
		p_location_cd	IN		 t5052_location_master.c5052_location_id%TYPE   -- peramater is changed from p_locationstr to P_Location_cd for MNTTASK-7108
	 	, p_warehouseid IN 		 t5051_inv_warehouse.c5051_inv_warehouse_id%type
	 	, p_building_id IN       t5052_location_master.c5057_building_id%TYPE DEFAULT NULL-- added for PMT-33507
		, p_out_cur		OUT 	 TYPES.cursor_type
	);
 
	PROCEDURE gm_fch_inv_loc_part_mapping (
		p_location_cd	IN		 t5052_location_master.c5052_location_id%TYPE  --- peramater is changed from p_locationstr to P_Location_cd for MNTTASK-7108
		, p_partnum		IN		 t205_part_number.c205_part_number_id%TYPE 
		, p_warehouseid IN 		 t5051_inv_warehouse.c5051_inv_warehouse_id%type
		, p_building_id IN       t5052_location_master.c5057_building_id%TYPE DEFAULT NULL -- added for PMT-33507
		, p_out_cur		OUT 	 TYPES.cursor_type

	);
	
	PROCEDURE gm_get_part_num_desc (
		p_partnumstr   IN		t205_part_number.c205_part_number_id%TYPE
	  , p_out_cur	   OUT		TYPES.cursor_type
	);

	PROCEDURE gm_sav_inv_location (
		p_inputstr	 IN   VARCHAR2
	  , p_zonestr	 IN   t5052_location_master.c901_zone%TYPE
	  , p_aislestr	 IN   t5052_location_master.c5052_aisle%TYPE
	  , p_user_id	 IN   t5052_location_master.c5052_created_by%TYPE
	  , p_shelf 	 IN   t5052_location_master.c5052_shelf%TYPE
	  , p_status     IN   t5052_location_master.c901_status%TYPE
	  , p_type       IN   t5052_location_master.c901_location_type%TYPE
	  , p_warehouseid IN T5051_INV_WAREHOUSE.C5051_INV_WAREHOUSE_ID%TYPE
	  , p_building_id IN t5052_location_master.c5057_building_id%TYPE DEFAULT NULL     -- added for PMT-33507
	);

	PROCEDURE gm_sav_inv_loc_part_details (
		p_inputstr	 IN   VARCHAR2
	  , p_user_id	 IN   t5052_location_master.c5052_created_by%TYPE
	);

	PROCEDURE gm_sav_inv_loc_part_detail (
		p_inputstr		IN	 VARCHAR2
	  , p_locationstr	IN	 t5052_location_master.c5052_location_id%TYPE
	  , p_typestr		IN	 t5052_location_master.c901_location_type%TYPE
	  , p_statusstr 	IN	 t5052_location_master.c901_status%TYPE
	  , p_user_id		IN	 t5052_location_master.c5052_created_by%TYPE
	  , p_row_ids		IN	 VARCHAR2
	  , p_warehouseid   IN  T5051_INV_WAREHOUSE.C5051_INV_WAREHOUSE_ID%TYPE
	  , p_building_id   IN  t5052_location_master.c5057_building_id%TYPE     -- added for PMT-33507
	);

	PROCEDURE gm_sav_inv_loc_log (
		p_created_date	 IN   t5054_inv_location_log.c5054_created_date%TYPE
	  , p_created_by	 IN   t5054_inv_location_log.c5054_created_by%TYPE
	  , p_part_num		 IN   t5054_inv_location_log.c205_part_number_id%TYPE
	  , p_loc_id		 IN   t5054_inv_location_log.c5052_location_id%TYPE
	  , p_org_qty		 IN   t5054_inv_location_log.c5054_orig_qty%TYPE
	  , p_txn_id		 IN   t5054_inv_location_log.c5054_txn_id%TYPE
	  , p_txn_qty		 IN   t5054_inv_location_log.c5054_txn_qty%TYPE
	  , p_new_qty		 IN   t5054_inv_location_log.c5054_new_qty%TYPE
	  , p_action		 IN   t5054_inv_location_log.c901_action%TYPE
	  , p_type			 IN   t5054_inv_location_log.c901_type%TYPE
	  , p_txn_typ		 IN   t5054_inv_location_log.c901_txn_type%TYPE	  
	  , p_company_id  	 IN   t5054_inv_location_log.c1900_company_id%TYPE
	  , p_plant_id     	 IN   t5054_inv_location_log.c5040_plant_id%TYPE
	  , p_tag_id 		 IN   t5054_inv_location_log.c5010_tag_id%TYPE DEFAULT NULL
      , p_control_number IN   t5054_inv_location_log.c5054_TXN_CONTROL_NUMBER%TYPE default null
	);
	
	 /**********************************************************************
	 * PMT-34027   : Inventory device - Dashboard and Transaction List Filter 
	 * Description : Procedure to get the building id for the passing partyid
	 * Author	   : N Raja
	**********************************************************************/
	PROCEDURE gm_fch_user_building_id(
		   p_partyid     IN  T101_USER.C101_PARTY_ID%TYPE
	     , p_groupid     IN  T1500_GROUP.C1500_GROUP_ID%TYPE
	     , p_buildingid  OUT T1501_GROUP_MAPPING.C5057_BUILDING_ID%TYPE
		);

	  /**********************************************************************
	 * Description : function to get the total bulk qty for the respective part
	 * Author	   : Murali
	**********************************************************************/
	FUNCTION get_total_bulkloc_qty (
		p_part_num	 t5053_location_part_mapping.c205_part_number_id%TYPE
	  , p_whtype	 t5051_inv_warehouse.c901_warehouse_type%TYPE
	)
		RETURN NUMBER;

	  /**********************************************************************
	 * Description : function to get the open transaction count
	 * Author	   : Ritesh
	**********************************************************************/
	FUNCTION get_open_txn_count (
		p_part_num	 t5053_location_part_mapping.c205_part_number_id%TYPE
	  , p_lcn_id	 t5053_location_part_mapping.c5052_location_id%TYPE
	  , p_whtype	 t5051_inv_warehouse.c901_warehouse_type%TYPE
	)
		RETURN NUMBER;
		
	 /**********************************************************************
	 * Description : Procedure to Save the new Zone
	 * Author	   : Mselvamani
	**********************************************************************/	
		PROCEDURE gm_add_zone(
		  p_zone IN  t901_code_lookup.c901_code_nm%TYPE
		, p_created_by	 IN   t901_code_lookup.C901_CREATED_BY%TYPE
		, p_out OUT t901_code_lookup.c901_code_id%TYPE
		
		);
		
	/**********************************************************************
	 * Description : Procedure to Save the new Zone in access table
	 * Author	   : Smariyappan
	**********************************************************************/		
		PROCEDURE GM_ADD_ZONE_ACCESS(
		   p_user_id   IN  T101_user.c101_user_id%TYPE
		 , p_zone 	   IN  t901a_lookup_access.C901_CODE_ID%TYPE
		);
END gm_pkg_op_inv_location_module;
/