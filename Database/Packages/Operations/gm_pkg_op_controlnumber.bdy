/* Formatted on 2010/11/17 11:24 (Formatter Plus v4.8.0) */
--@"C:\Database\Packages\Operations\gm_pkg_op_controlnumber.bdy";
/*************************************************************************
	* Description 	: This procedure is used to fetch the records for the report 
	* 
	* Author		: Rajkumar
	**************************************************************************/
CREATE OR REPLACE PACKAGE BODY gm_pkg_op_controlnumber
IS
   PROCEDURE gm_fch_controlnumber (
      p_part_number_id   IN       t2550_part_control_number.c205_part_number_id%TYPE,
      p_part_desc        IN       t205_part_number.c205_part_num_desc%TYPE,
      p_data             OUT      TYPES.cursor_type
   )
   AS
   BEGIN
      OPEN p_data
       FOR
          SELECT   t2550.c2550_part_control_number_id id,
                   t2550.c205_part_number_id partid,
                   GET_PARTNUM_DESC (t2550.c205_part_number_id) pdesc,
                   t2550.c2550_control_number controlid,
                   GET_USER_NAME (t2550.c2550_last_updated_by) createdby,
                   TO_CHAR (t2550.c2550_last_updated_date,
                            NVL(GET_RULE_VALUE('DATEFMT', 'DATEFORMAT'),'mm/dd/yyyy')
                           ) createddate,
                   GET_LOG_FLAG (NVL (t2550.c2550_part_control_number_id, ''),
                                 92092
                                ) LOGFLG
          FROM t2550_part_control_number t2550
          WHERE  t2550.c205_part_number_id IN (
          		SELECT c205_part_number_id
                  FROM t205_part_number
                WHERE c205_part_number_id LIKE
                                   '%'
                                || NVL (p_part_number_id, c205_part_number_id)
                                || '%'
                         AND LOWER (c205_part_num_desc) LIKE
                                   '%'
                                || NVL (LOWER (p_part_desc),
                                        LOWER (c205_part_num_desc)
                                       )
                                || '%')
          ORDER BY t2550.c2550_last_updated_date DESC;

   END gm_fch_controlnumber;
/*************************************************************************
	* Description 	: This Procedure is used to save the control number. before saving 
	* the control number, it will check for Vendor Code, Year and Day. If all the things
	* are validated and all are satisfies the criteria, then it will save   
	* 
	* Author		: Rajkumar
	**************************************************************************/

   PROCEDURE gm_sav_control_num_detail (
      p_ctrl_asd_id   IN OUT   t2550_part_control_number.c2550_part_control_number_id%TYPE,
      p_part_id       IN       t2550_part_control_number.c205_part_number_id%TYPE,
      p_ctrl_id       IN       t2550_part_control_number.c2550_control_number%TYPE,
      p_user_id       IN       t2550_part_control_number.c2550_last_updated_by%TYPE
   )
   AS
      v_ctrl_asd_id   NUMBER;
      v_count         NUMBER                                             := 0;
      v_rule_id       VARCHAR2 (2);
      v_vendor_id     VARCHAR2 (4);
      v_year          VARCHAR2 (5);
      v_day           NUMBER;
      v_cur_year      VARCHAR2 (5);
      v_ctrl_id       t2550_part_control_number.c2550_control_number%TYPE;
      v_attr_val      VARCHAR2 (10);
      v_validate      VARCHAR2 (2);
      v_julian_rule   NUMBER;
      v_company_id    T1900_COMPANY.C1900_COMPANY_ID%TYPE;
      v_plant_id      T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
   BEGIN
      v_ctrl_id := UPPER (p_ctrl_id);

      /*IF LENGTH (v_ctrl_id) < 6
      THEN
         IF (v_ctrl_id != 'NOC#')
         THEN
            raise_application_error ('-20326', '');
         END IF;
      END IF;*/

        SELECT get_compid_frm_cntx(), get_plantid_frm_cntx()  
          INTO v_company_id, v_plant_id 
          FROM DUAL; 
          
		-- check if the part# and control# combination already exists.
		SELECT COUNT (1)
	   		INTO v_count   
	   	FROM t2550_part_control_number
	  	WHERE c205_part_number_id = p_part_id
	    	AND c2550_control_number = v_ctrl_id 
	    	AND C1900_COMPANY_ID =v_company_id 
	    	AND C5040_PLANT_ID = v_plant_id;


      IF v_count > 0
      THEN
         GM_RAISE_APPLICATION_ERROR('-20999','192',v_ctrl_id||'#$#'||p_part_id); 
      END IF;

      v_count := 0;

          /* no more giving the access to update the record. only void and insert
           UPDATE t4080_exception_controlnumber
               SET c205_part_number_id = p_part_id,
                   c4080_control_id = v_ctrl_id,
                   c4080_last_updated_by = p_user_id,
                   c4080_last_updated_date = SYSDATE
             WHERE c4080_control_asd_id = p_ctrl_asd_id AND c4080_void_fl IS NULL;
      */
      SELECT COUNT (c205_part_number_id)
        INTO v_count
        FROM t205_part_number
       WHERE c205_part_number_id = p_part_id AND c205_active_fl = 'Y';

      SELECT get_rule_value (p_part_id, 'SKIPCONTROLNUMVAL')
        INTO v_rule_id
        FROM DUAL;
      SELECT get_part_attribute_value(p_part_id,'80132')
        INTO v_attr_val
        FROM DUAL;
        
      SELECT TO_NUMBER (get_rule_value ('JULIAN_DATE', 'LOTVALIDATION'))
		INTO v_julian_rule
	    FROM DUAL;
	    
      IF (v_count = 0)
      THEN
         raise_application_error ('-20904', '');
      END IF;
      
	  IF (INSTR(LOWER(p_ctrl_id),'noc#')> 0)
	  THEN
	  GM_RAISE_APPLICATION_ERROR('-20999','193','');
	  END IF;
	 IF v_rule_id = 'Y' OR v_attr_val = '80130' OR LENGTH (p_part_id) != 8
	 THEN
    	v_validate:='N';
	 ELSIF LENGTH (p_part_id) = 8 
	 THEN
    	v_validate:='Y';
	 END IF;
      IF v_validate = 'Y'
      THEN
         --Splitting Control Number into Vendor Id, Year and Days
         BEGIN
            v_vendor_id := SUBSTR (v_ctrl_id, 1, 2);
            v_year := SUBSTR (v_ctrl_id, 3, 3);
            v_day := TO_NUMBER (SUBSTR (v_ctrl_id, 4, 3));
         EXCEPTION
            WHEN OTHERS
            THEN
               v_day := -1;
         END;

        --Retreiving Vendor Details
         SELECT COUNT (c301_lot_code)
           INTO v_count
           FROM t301_vendor
          WHERE c301_lot_code = v_vendor_id;

         IF (v_count = 0)
         THEN
            raise_application_error ('-20905', '');
         END IF;

         --Retrieving Year
         SELECT get_rule_value (SUBSTR (v_ctrl_id, 3, 1), 'MFGYEAR')
           INTO v_year
           FROM DUAL;

         SELECT TO_CHAR (SYSDATE, 'YYYY')
           INTO v_cur_year
           FROM DUAL;

         IF (v_year < 2003 AND v_year > v_cur_year) OR v_year IS NULL
         THEN
            raise_application_error ('-20906', '');
         END IF;

         IF (v_day < 0 OR (v_day > 366 AND v_day < v_julian_rule+1) OR v_day > v_julian_rule+366)
         THEN
            raise_application_error ('-20907', '');
         END IF;
      END IF;

      SELECT S2550_PART_CONTROL_NUMBER.NEXTVAL
        INTO v_ctrl_asd_id
        FROM DUAL;

      INSERT INTO t2550_part_control_number
                  (c2550_part_control_number_id, c205_part_number_id, c2550_control_number
                  , c2550_last_updated_by, c2550_last_updated_date,c1900_company_id,c5040_plant_id
                  )
           VALUES (v_ctrl_asd_id, p_part_id, v_ctrl_id
                   , p_user_id, SYSDATE,v_company_id,v_plant_id
                  );

      p_ctrl_asd_id := TO_CHAR (v_ctrl_asd_id);
   END gm_sav_control_num_detail;
	/*************************************************************************
	* Description :  This procedure is used to fetch all the records from the Table
	*
	* Author		: Rajkumar
	**************************************************************************/

   PROCEDURE gm_fch_control_num_detail (
      p_ctrl_asd_id   IN       t2550_part_control_number.c2550_part_control_number_id%TYPE,
      p_data          OUT      TYPES.cursor_type
   )
   AS
   BEGIN
      OPEN p_data
       FOR
          SELECT c2550_part_control_number_id ctrlasd_id, c205_part_number_id partnm,
                 c2550_control_number controlnm
            FROM t2550_part_control_number
           WHERE c2550_part_control_number_id = p_ctrl_asd_id;
   END gm_fch_control_num_detail;

   PROCEDURE gm_chk_lock_fl (
      p_ctrl_asd_id   IN       t4080_exception_controlnumber.c4080_control_asd_id%TYPE,
      p_data          OUT      VARCHAR2
   )
   AS
   BEGIN
      /*SELECT
         C4080_LOCK_FL INTO p_data
      FROM
         T4080_exception_controlnumber
      WHERE
         c4080_control_asd_id=p_ctrl_asd_id;
       */
      p_data := '';
   END gm_chk_lock_fl;
/*************************************************************************
	* Description :  This procedure is used to void the control number
	*
	* Author		: Rajkumar
	**************************************************************************/

   PROCEDURE gm_void_control_number (
      p_txn_id    IN   t4080_exception_controlnumber.c4080_control_asd_id%TYPE,
      p_user_id   IN   t4080_exception_controlnumber.c4080_created_by%TYPE
   )
   AS
   BEGIN
      UPDATE t4080_exception_controlnumber
         SET c4080_void_fl = 'Y',
             c4080_last_updated_by = p_user_id,
             c4080_last_updated_date = SYSDATE
       WHERE c4080_control_asd_id = p_txn_id;
   END gm_void_control_number;
/*************************************************************************
	* Description : This procedure is used to save the control number	
	* Author	  : Bala
**************************************************************************/

   PROCEDURE gm_save_control_number (
      p_part_id       IN       t2550_part_control_number.c205_part_number_id%TYPE,
      p_ctrl_id       IN       t2550_part_control_number.c2550_control_number%TYPE,
      p_user_id       IN       t2550_part_control_number.c2550_last_updated_by%TYPE,
      p_expiry_date   IN 	   t2550_part_control_number.c2550_expiry_date%TYPE DEFAULT NULL,
      p_vendor_id     IN 	   t2550_part_control_number.c301_vendor_id%TYPE DEFAULT NULL,
      p_mfg_date      IN 	   t2550_part_control_number.c2550_mfg_date%TYPE DEFAULT NULL,
      p_donor_id      IN 	   t2550_part_control_number.c2540_donor_id%TYPE DEFAULT NULL,
      p_udi_hrf       IN 	   t2550_part_control_number.c2550_udi_hrf%TYPE DEFAULT NULL,
      p_udi_mrf       IN 	   t2550_part_control_number.c2550_udi_mrf%TYPE DEFAULT NULL,
      p_custom_size   IN 	   t2550_part_control_number.c2550_custom_size%TYPE DEFAULT NULL,
      p_pack_slip     IN       t408_dhr.c408_packing_slip%TYPE DEFAULT NULL,
      p_dhr_id    	  IN	   t408_dhr.c408_dhr_id%TYPE DEFAULT NULL 
   )
   AS
      v_company_id    T1900_COMPANY.C1900_COMPANY_ID%TYPE;
      v_plant_id      T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
   BEGIN
	   
	  SELECT get_compid_frm_cntx(), get_plantid_frm_cntx()  INTO v_company_id, v_plant_id FROM DUAL;  
	   
      UPDATE t2550_part_control_number
         SET c2550_control_number = p_ctrl_id,
         	 c2550_expiry_date = NVL(p_expiry_date, c2550_expiry_date),
         	 c301_vendor_id = NVL(p_vendor_id, c301_vendor_id),
             c2550_mfg_date = NVL(p_mfg_date, c2550_mfg_date),
         	 c2540_donor_id = NVL(p_donor_id, c2540_donor_id),
         	 c2550_udi_hrf = NVL(p_udi_hrf, c2550_udi_hrf),
         	 c2550_udi_mrf = NVL(p_udi_mrf, c2550_udi_mrf),
         	 c2550_custom_size = NVL(p_custom_size, c2550_custom_size),
             c2550_last_updated_by = p_user_id,
             c2550_last_updated_date = SYSDATE,
             c1900_company_id   = v_company_id,
             c5040_plant_id     = v_plant_id
       WHERE c205_part_number_id = p_part_id
         AND c2550_control_number = p_ctrl_id;
       
    IF (SQL%ROWCOUNT = 0)
    THEN 
	
      INSERT INTO t2550_part_control_number
      			  (c2550_part_control_number_id, c205_part_number_id, c2550_control_number, c2550_expiry_date, c301_vendor_id, c2550_mfg_date,
      			   c2550_custom_size, c2540_donor_id, c2550_udi_hrf, c2550_udi_mrf, c2550_last_updated_date, c2550_last_updated_by, c2550_packing_slip, 
      			   c2550_last_updated_trans_id, c1900_company_id, c5040_plant_id)
           VALUES (s2550_part_control_number.nextval, p_part_id, p_ctrl_id, p_expiry_date, p_vendor_id, p_mfg_date,
           		   p_custom_size, p_donor_id, p_udi_hrf, p_udi_mrf, SYSDATE, p_user_id, p_pack_slip, 
           		   p_dhr_id, v_company_id, v_plant_id);
     END IF;             
   END gm_save_control_number;    
   /*************************************************************************
	* Description : This procedure is used to save the override transaction id for skip control no validation	
	* Author	  : Aprasath
**************************************************************************/

   PROCEDURE gm_sav_skip_cnum_expiry_date (
      p_part_id       IN       t2550_part_control_number.c205_part_number_id%TYPE,
      p_ctrl_id       IN       t2550_part_control_number.c2550_control_number%TYPE,
      p_user_id       IN       t2550_part_control_number.c2550_last_updated_by%TYPE,
      p_txn_id        IN       varchar2,
      p_ctrl_asd_id   OUT   t2550_part_control_number.c2550_part_control_number_id%TYPE
   )
   AS
      v_ctrl_asd_id   NUMBER;
      v_company_id    T1900_COMPANY.C1900_COMPANY_ID%TYPE;
      v_plant_id      T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
   BEGIN
	   
	  SELECT get_compid_frm_cntx(), get_plantid_frm_cntx()  INTO v_company_id, v_plant_id FROM DUAL; 
	  
	  IF p_txn_id IS NOT NULL THEN
	      UPDATE t2550_part_control_number
	         SET c2550_expdate_skip_transid = p_txn_id,
	             c2550_last_updated_by = p_user_id,
	             c2550_last_updated_date = SYSDATE             
	       WHERE c205_part_number_id = p_part_id
	         AND c2550_control_number = p_ctrl_id
	         AND c1900_company_id   = v_company_id
	         AND c5040_plant_id     = v_plant_id;
      END IF; 
      
      BEGIN 
	  SELECT c2550_part_control_number_id
		 INTO v_ctrl_asd_id
		FROM t2550_part_control_number
		WHERE c205_part_number_id = p_part_id
		AND c2550_control_number  = p_ctrl_id 
		AND c1900_company_id   = v_company_id 
		AND c5040_plant_id     = v_plant_id;
	  EXCEPTION WHEN NO_DATA_FOUND THEN
	      v_ctrl_asd_id := NULL;
	  END;
	 
		p_ctrl_asd_id := v_ctrl_asd_id;          
END gm_sav_skip_cnum_expiry_date; 

/******************************************************************************************************************************
	* Description : This procedure is used to validate and get the 5 digit lot incase if lot is not available in t2550- PC-277	
	* Author	  : Aprasath
*******************************************************************************************************************************/
	FUNCTION gm_fch_steklast_cnum (
	  p_part_num       IN       t2550_part_control_number.c205_part_number_id%TYPE,
      p_ctrl_num       IN       t2550_part_control_number.c2550_control_number%TYPE
	)
		RETURN VARCHAR
	IS
		v_count NUMBER;
		v_old_cnum_count NUMBER;
		v_part_type VARCHAR2(10);
		v_control_number t2550_part_control_number.c2550_control_number%TYPE;
	BEGIN
		v_control_number := p_ctrl_num;
		/* validating stelkast part number*/
		SELECT COUNT(1) INTO v_count
		FROM t205_part_number t205,
		  t202_project t202
		WHERE t205.c202_project_id  = t202.c202_project_id
		AND t202.c1910_division_id  =2007 --STELKAST DIVISION
		AND t205.c205_part_number_id =  p_part_num
		AND t205.c205_active_fl      = 'Y'
		AND t202.c202_void_fl       IS NULL;
		
		IF v_count = 0 THEN
		RETURN p_ctrl_num;
		END IF;
		
		/* verify the part and control numbers are labeled in globus labels or stelkast labels, Globus labels are new
			and keep the UDI in regular format, for that we  dont need to check the below*/
		BEGIN
		SELECT (
		  CASE
			WHEN instr(t2550.c2550_udi_hrf,'(17)',1) < instr(t2550.c2550_udi_hrf,'(10)',1)
			THEN 'NEW '
			ELSE 'OLD'
		  END) INTO v_part_type
		FROM t2550_part_control_number t2550
		  WHERE t2550.c205_part_number_id = p_part_num
		  AND t2550.c2550_control_number = p_ctrl_num 
		  AND ROWNUM=1;
		EXCEPTION WHEN NO_DATA_FOUND THEN
	      v_part_type := 'OLD';
		END;
		/* Skip below process incase control no has - symbol. These are old control number which not able to scan in device*/
		SELECT NVL(instr(p_ctrl_num,'-',1),0) INTO v_old_cnum_count FROM dual;
		
		/* if it is stelkast part then checking is it available in t2550 if not skip the exp date and return as control number*/
		IF v_count > 0 AND LENGTH(p_ctrl_num) >= 10 AND v_part_type = 'OLD' AND v_old_cnum_count =0 THEN
		BEGIN
		  SELECT DECODE(COUNT(1),0,SUBSTR(p_ctrl_num, 0, LENGTH(p_ctrl_num) - 6),p_ctrl_num)
		  INTO v_control_number
		  FROM t2550_part_control_number t2550
		  WHERE t2550.c205_part_number_id = p_part_num
		  AND t2550.c2550_control_number = p_ctrl_num 
		  AND ROWNUM=1;
		EXCEPTION WHEN NO_DATA_FOUND THEN
	      v_control_number := p_ctrl_num;
		END;
		END IF;
		
		RETURN v_control_number;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN p_ctrl_num;
	END gm_fch_steklast_cnum;    
END gm_pkg_op_controlnumber;
/
