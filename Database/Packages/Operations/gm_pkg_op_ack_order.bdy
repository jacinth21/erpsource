--@"c:\database\packages\operations\gm_pkg_op_ack_order.bdy"
CREATE OR REPLACE
PACKAGE BODY gm_pkg_op_ack_order
IS
/*
  * Description	:	Procedure to validate ack order qty
  * Author		:	Bala 
 */ 
PROCEDURE gm_op_save_bba_order(
	p_order_id	IN	t501_order.c501_order_id%TYPE,
    p_userid    IN  t501_order.c501_created_by%TYPE	
)
  AS
	v_pnum		   VARCHAR2 (20);
	v_qty		   NUMBER;
	v_control	   VARCHAR2 (20);
	v_price 	   NUMBER (15, 2);	
	v_refid 	   VARCHAR2 (40);
	v_order_id     t501_order.c501_order_id%TYPE;
	v_ackord_id    t501_order.c501_order_id%TYPE;
	v_ord_type     t502_item_order.c901_type%TYPE;
	v_loop_cnt     NUMBER;
	v_validate_cnt NUMBER;
		
	CURSOR v_ord_cursor 
    IS
     SELECT *
        FROM t502_item_order 
      WHERE c501_order_id = p_order_id        
        AND c502_void_fl IS NULL;
       
    -- Get the all reserved control num,qty for the order (Ack)     
    CURSOR v_res_cursor
    IS
     SELECT t5060.c205_part_number_id pnum, t5060.c5060_control_number cnum , t5062.c5062_reserved_qty qty
        FROM t5062_control_number_reserve t5062 
            ,t5060_control_number_inv t5060                  
       WHERE t5062.c5062_transaction_id = v_ackord_id-- ACK-XXXX
         AND t5060.c5060_control_number_inv_id = t5062.c5060_control_number_inv_id
         AND t5060.c205_part_number_id = v_pnum
         AND t5060.c5060_control_number NOT IN (SELECT t502.c502_control_number rescnum
                                                  FROM t502_item_order t502   
                                                 WHERE t502.c501_order_id = p_order_id  -- BBA-XXXX
                                                   AND t502.c502_control_number IS NOT NULL
                                                   AND t502.c205_part_number_id =  v_pnum
                                                   AND C502_Void_Fl IS NULL)
         AND c901_status != '103962' -- Closed
         AND c5062_void_fl IS NULL;         
 
  BEGIN	
	--	    	
		SELECT c501_order_id,c501_ref_id 
		  into v_order_id,v_ackord_id
           FROM t501_order 
          WHERE c501_order_id = p_order_id 
            AND c501_void_fl is null
        FOR UPDATE;
        
        /* Following code is added for validating the Released Quantity againist the Reserved Quantity for that Part */
		v_validate_cnt := gm_pkg_op_ack_order.gm_validate_release_qty(p_order_id);
		
		IF v_validate_cnt > 0 THEN
			GM_RAISE_APPLICATION_ERROR('-20999','188','');
		END IF;
		
        
        FOR  v_ord_index IN  v_ord_cursor 
        LOOP
            v_pnum      := v_ord_index.c205_part_number_id;
            v_qty       := v_ord_index.c502_item_qty;
            v_price     := v_ord_index.c502_item_price;
            v_ord_type  := v_ord_index.c901_type;            
             -- If the order has same part first loop insert the corresponding qty only for the part instead of all reserved qty.   
            v_loop_cnt  :=v_qty;
            
           FOR v_res_index IN v_res_cursor
           LOOP
             IF v_loop_cnt >0 THEN
                gm_pkg_op_order_master.gm_op_sav_order_detail(p_order_id
                                                            ,v_pnum
                                                            ,v_res_index.qty
                                                            ,v_res_index.cnum
                                                            ,v_price
                                                            ,v_ord_type
                                                            ,null);
               v_qty := v_qty - v_res_index.qty;
               v_loop_cnt := v_loop_cnt -v_res_index.qty;
             END IF;     
          END LOOP;
 
             IF v_qty > 0 THEN
                gm_pkg_op_order_master.gm_op_sav_order_detail(p_order_id
                                                           ,v_pnum
                                                           ,v_qty
                                                           ,null
                                                           ,v_price
                                                           ,v_ord_type
                                                           ,null);
             END IF;           
          END LOOP;
          
          /*
           *  No Insert is required after this deletion.
           */
        
       DELETE FROM t502_item_order 
        WHERE c501_order_id = p_order_id 
          AND c502_control_number = 'NOC#'
          AND c502_void_fl is null;          
    
     -- UPDATING THE STATUS AND TRANSACTION ID DURING LI-XXXX INITIATION FROM AB-XXXXX IN t5062_control_number_reserve
       UPDATE t5062_control_number_reserve
          SET c5062_transaction_id =  p_order_id 
            , C901_STATUS = '103961' -- In Progress
            , c5062_last_updated_by = p_userid
     	    , c5062_last_updated_date = SYSDATE
        WHERE c5062_transaction_id =  v_ackord_id
          AND c901_status != '103962' -- Closed
          AND C5062_VOID_FL IS NULL;     
          
        -- To validate Ack order Qty
		gm_pkg_op_ack_order.gm_op_validate_ack_order(p_order_id);
	
		-- To save ship instructions from Ack To BBA
		gm_pkg_op_ack_order.gm_copy_ship_info_ack_to_bba(p_order_id,p_userid);        
          
  END gm_op_save_bba_order;  
  
/*
  * Description	:	Procedure to validate ack order qty
  * Author		:	Bala 
*/ 
PROCEDURE gm_op_validate_ack_order(
		p_order_id	IN	T501_ORDER.C501_ORDER_ID%TYPE
)
	AS
		v_ack_order_id t501_order.c501_order_id%type;		
		v_diff_qty number;
	BEGIN	
		BEGIN
		  SELECT c501_order_id
		    INTO v_ack_order_id
		    FROM t501_order t501 
		   WHERE c901_order_type = 101260 -- Acknowledgement Order
		   START WITH t501.c501_order_id =  p_order_id 
		   CONNECT BY PRIOR c501_ref_id = c501_order_id;
		EXCEPTION WHEN NO_DATA_FOUND THEN
	 		RETURN;
		END;

		-- VALIDATION #1 ( CHECK IF THE QTY IN THE LI ORDERS DOES NOT EXCEED AB ORDER'S QTY )
		BEGIN
		SELECT NVL(SUM(diff_qty),0) INTO v_diff_qty
		  FROM (SELECT (CASE WHEN nvl(ab_order.qty,0) - nvl(li_order.qty,0) < 0 THEN 1 ELSE 0 END) diff_qty
				  FROM (SELECT t502.c205_part_number_id
	        				 , SUM(t502.c502_item_qty) qty
	   					  FROM t502_item_order t502 
	   					 WHERE t502.c501_order_id = v_ack_order_id -- AB-16304
	     				   AND t502.c502_delete_fl is null
	     				   AND t502.c502_void_fl is null
	   				  GROUP BY t502.c205_part_number_id) ab_order ,
	   				   (SELECT t502.c205_part_number_id
	        				 , SUM(t502.c502_item_qty) qty
	   					  FROM t501_order t501, t502_item_order t502 
	   					 WHERE t501.c501_ref_id = v_ack_order_id -- AB-16304 
	     				   AND t502.c502_delete_fl IS NULL
	     				   AND t502.c502_void_fl IS NULL
	     				   AND t501.c501_order_id = t502.c501_order_id 
	   				  GROUP BY t502.c205_part_number_id) li_order
	   				  where ab_order.c205_part_number_id = li_order.c205_part_number_id);
	   		EXCEPTION WHEN NO_DATA_FOUND THEN
	 			  v_diff_qty :='0';
	   	END;
	
		IF v_diff_qty > 0 THEN
		  GM_RAISE_APPLICATION_ERROR('-20999','187',v_ack_order_id);
		END IF;
		
END gm_op_validate_ack_order;

/*
  * Description	:	Procedure to save the Exchange rate value for the order 
  * Author		:	Bala 
 */ 	
PROCEDURE gm_copy_ship_info_ack_to_bba(
			p_order_id	IN	T501_ORDER.C501_ORDER_ID%TYPE,
			p_userid	IN	   t501_order.c501_created_by%TYPE
)
	AS
		v_shipto	   t907_shipping_info.c901_ship_to%TYPE;
	    v_shiptoid	   t907_shipping_info.c907_ship_to_id%TYPE;
	    v_shipcarr     t907_shipping_info.c901_delivery_carrier%TYPE;
	    v_shipmode	   t907_shipping_info.c901_delivery_mode%TYPE;
	    v_addressid    t907_shipping_info.c106_address_id%TYPE;
		v_ship_instruc t907_shipping_info.c907_ship_instruction%TYPE;
		v_attn		   t907_shipping_info.c907_override_attn_to%TYPE;
		v_shipid       VARCHAR2 (100);
		v_ack_order_id t501_order.c501_order_id%type;  
		v_frieght_amt	t907_shipping_info.c907_frieght_amt%TYPE;
		BEGIN
			BEGIN
			--Get the frieght amount which is saved from the BBA order generate screen
				SELECT NVL(c907_frieght_amt,'0') into  v_frieght_amt
				FROM t907_shipping_info
				WHERE c907_ref_id = p_order_id 
				   AND c907_void_fl IS NULL
				   AND c907_status_fl NOT IN ('40','30');
				EXCEPTION WHEN NO_DATA_FOUND THEN
			 		v_frieght_amt := '0';
			 END;
			 
			-- Delete old records for this order. Its created in save_item_order procedure.Ack ship info need to carry here.
			  -- First delete child record and then only delete parent record t907_shipping_info data. 
			DELETE 
			  FROM t907a_shipping_status_log
			 WHERE c907a_ref_id = p_order_id 
			   AND c907a_status NOT IN ('40','30');  
			   
			DELETE 
			  FROM t907_shipping_info 
			 WHERE c907_ref_id = p_order_id 
			   AND c907_void_fl IS NULL
			   AND c907_status_fl NOT IN ('40','30');
			   
		 BEGIN
		  SELECT c501_order_id
		    INTO v_ack_order_id
		    FROM t501_order t501 
		   WHERE c901_order_type = 101260 -- Acknowledgement Order
		   AND c501_void_fl is null
		   START WITH t501.c501_order_id =  p_order_id 
		   CONNECT BY PRIOR c501_ref_id = c501_order_id;
		  EXCEPTION WHEN NO_DATA_FOUND THEN
	 		RETURN;
		 END;
			
			BEGIN				 
			 SELECT c901_ship_to,c907_ship_to_id,c106_address_id,c907_override_attn_to
			 ,c901_delivery_mode,c901_delivery_carrier,c907_ship_instruction       
			 INTO v_shipto,v_shiptoid,v_addressid,v_attn,v_shipmode,v_shipcarr,v_ship_instruc 
               FROM t501_order t501, t907_shipping_info t907 
              WHERE t501.c501_order_id =  t907.c907_ref_id 
                AND t907.c907_ref_id   = v_ack_order_id
                AND t907.c907_status_fl <> '40' 
                AND t907.c907_void_fl IS NULL;		
			EXCEPTION
				WHEN NO_DATA_FOUND
				THEN
				    v_shipto       := null;
				    v_shiptoid     := null;
				    v_addressid    := null;
				    v_attn         := null;
				    v_shipmode     := null;
				    v_shipcarr     := null;
					v_ship_instruc := null;					
			END;
			
			-- Delete old records for this order id
				
			-- To save ship info
			gm_pkg_cm_shipping_trans.gm_sav_ship_info (p_order_id
												 , '50180'
												 , v_shipto
												 , v_shiptoid
												 , v_shipcarr
												 , v_shipmode
												 , null
												 , v_addressid
												 , v_frieght_amt
												 , p_userid
												 , v_shipid
												 , v_attn
												 , v_ship_instruc
												 , null
												 , null
												  );
	
END gm_copy_ship_info_ack_to_bba;

/*
  * Description	:	Procedure to get the delivery notes detail
  * Author		:	Bala 
*/ 	

PROCEDURE gm_fch_delivery_notes(
	p_orderid	IN	T501_ORDER.C501_ORDER_ID%TYPE,
	p_out		OUT	TYPES.cursor_type 
)
AS
v_plant_id		T501_ORDER.C5040_PLANT_ID%TYPE;
BEGIN
--Getting company id and plant id from context.   

	    SELECT get_plantid_frm_cntx()
		  INTO v_plant_id
		  FROM dual;
		  
	OPEN p_out FOR
		 SELECT 
	        c501_order_id ordid, 	
	        DECODE(C501_STATUS_FL,0,'Back Order',1,'Processing',2,DECODE(C501_SHIPPING_DATE,NULL,'Controlled','Shipped'),'Shipped') SFL,
	        DECODE(c501_delete_fl,'Y','Draft',get_code_name(c901_order_type)) ordertype, 
	        get_user_name (c501_created_by) createdby, 
	        c501_created_date createddate
	 	  FROM t501_order
	  	WHERE c501_ref_id = p_orderid
	  	AND C5040_PLANT_ID = v_plant_id
	  	AND C501_VOID_FL IS NULL;
END gm_fch_delivery_notes;

/*
  * Description	:	Procedure to get the attribute values based on order id 
  * Author		:	Bala 
*/ 	

FUNCTION get_order_attribute_string(
 	p_orderid	T501_ORDER.C501_ORDER_ID%TYPE
 ) RETURN VARCHAR2
 IS
 	v_ordatt	VARCHAR2(2000) :='';
 BEGIN
	 		WITH v_txns 
					AS (
					SELECT ('^' || C901_ATTRIBUTE_TYPE || '^' || C501A_ATTRIBUTE_VALUE  ) trans_id
					FROM T501A_ORDER_ATTRIBUTE
					WHERE C501_ORDER_ID = p_orderid)
					SELECT MAX (SYS_CONNECT_BY_PATH (trans_id,'|')) INTO v_ordatt
					FROM  (SELECT trans_id, rownum curr FROM v_txns)
					CONNECT BY curr -1 = PRIOR curr START WITH curr = 1;
					
					v_ordatt := substr(v_ordatt,2) || '|';
					RETURN v_ordatt; 
END get_order_attribute_string;


	/***************************************************************************************
    * Description   : Function to validate the Released Quantity againist the Reserved Quantity.
    * Author		: Harinadh Reddi
    ****************************************************************************************/

	FUNCTION gm_validate_release_qty(
		p_order_id	T501_ORDER.C501_ORDER_ID%TYPE
	) RETURN NUMBER
	IS		
		v_ack_order_id  t501_order.c501_order_id%type;
		v_reserved_qty 	NUMBER;
		v_diff_qty 		NUMBER;
	BEGIN
		BEGIN 
			SELECT c501_order_id
			  INTO v_ack_order_id 
			  FROM t501_order t501 
			 WHERE c901_order_type = 101260 -- Acknowledgement Order
			 START WITH t501.c501_order_id =  p_order_id
			 CONNECT BY PRIOR c501_ref_id = c501_order_id;
		 EXCEPTION WHEN NO_DATA_FOUND THEN
	 		v_ack_order_id := NULL;
		END;
		BEGIN
			SELECT SUM(diff_qty)  INTO v_diff_qty
			  FROM
			(SELECT (CASE WHEN NVL(reservQty, 0) >  NVL(qty,0) THEN 1 ELSE 0 END) diff_qty
			  FROM
				 (SELECT t5060.c205_part_number_id
					   , SUM(t5062.c5062_reserved_qty) reservQty
			  		FROM t5062_control_number_reserve t5062
			     	   , t5060_control_number_inv t5060
				   WHERE c901_transaction_type = 103980
					 AND c5062_transaction_id = v_ack_order_id
					 AND t5060.c5060_control_number_inv_id = t5062.c5060_control_number_inv_id
					 AND t5062.c5062_void_fl IS NULL
				GROUP BY t5060.c205_part_number_id) ord_reserve ,
			   	 (SELECT t502.c205_part_number_id
			           , SUM(t502.c502_item_qty) qty
			        FROM t502_item_order t502 
			       WHERE t502.c501_order_id = p_order_id
			         AND t502.c502_delete_fl IS NULL
			         AND t502.c502_void_fl IS NULL
			    GROUP BY t502.c205_part_number_id) ord_release
			       WHERE ord_reserve.c205_part_number_id = ord_release.c205_part_number_id (+));
		EXCEPTION WHEN NO_DATA_FOUND THEN
	 		v_diff_qty := '0';
		END;		
		RETURN v_diff_qty;		
	END gm_validate_release_qty;
	
	/********************************************************************
     * Description : This function returns the Reserved Quantity for Part
     * Bala
     ********************************************************************/

	FUNCTION get_reserved_qty (
		p_trans_id	T501_ORDER.C501_ORDER_ID%TYPE,
		p_pnum	 	t4011_group_detail.c205_part_number_id%TYPE
	)
		RETURN VARCHAR2
	IS
		v_qty   VARCHAR2(100);
		v_plant_id		t5040_plant_master.c5040_plant_id%TYPE;
	--
	BEGIN
		 --Getting company id and plant id from context.   

	    SELECT get_plantid_frm_cntx()
		  INTO v_plant_id
		  FROM dual;
		  
	      SELECT NVL(SUM(T5062.c5062_reserved_qty),0) INTO  v_qty	        
		    FROM T5060_Control_Number_Inv T5060,T5062_Control_Number_Reserve T5062
		   WHERE t5060.C5060_Control_Number_Inv_Id  = t5062.C5060_Control_Number_Inv_Id 
		     AND T5062.C5062_Transaction_Id = p_trans_id
	         AND T5060.C205_Part_Number_Id  = p_pnum
	         AND T5060.c5040_plant_id = v_plant_id
	         AND T5062.c5040_plant_id = v_plant_id
		     AND T5062.c5062_void_fl IS NULL;         
		RETURN v_qty;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN '0';
	END get_reserved_qty;
	
	/***************************************************************************************
    * Description   : Procedure to validate the ack released quantity and close the ab order 
    * Author		: Bala
    ****************************************************************************************/
	PROCEDURE gm_chk_and_close_ack_order(
		p_refid  	IN	t501_order.c501_order_id%TYPE
	  , p_userid	IN 	t501_order.c501_last_updated_by%TYPE
    )
    AS
	    v_diff_qty 	   NUMBER;
	    BEGIN
	    -- Get the related order shipped qty difference. If the difference is Zero then update the Ack order status as closed.
			v_diff_qty := gm_pkg_op_ack_order.gm_chk_ack_released_qty(p_refid);
			IF v_diff_qty = 0 THEN
			    UPDATE T907_Shipping_Info
                   SET C907_Status_Fl = '40'
                 WHERE C907_Ref_Id    = (SELECT c501_ref_id
                                           FROM T501_Order
                                          WHERE C501_Order_Id = p_refid
                                            AND c501_void_fl IS NULL) 
                                            AND c907_status_fl !=40 AND c907_void_fl IS NULL; 
                                            
				UPDATE t501_order
                   SET c501_status_fl = '3' -- Shipped
                      ,c501_last_updated_by = p_userid
                      ,c501_last_updated_date = SYSDATE
                 WHERE c501_order_id = (SELECT c501_ref_id
                                           FROM T501_Order
                                          WHERE c501_order_id = p_refid                                            
                                            AND c501_void_fl IS NULL); 
                                            
		    END IF;
	    	
		    
	    
	END gm_chk_and_close_ack_order;

	/***************************************************************************************
     * Description : This function returns the Quantity difference betweeen ack and bba order 
     * Author	   : Bala
     ***************************************************************************************/	
	
	FUNCTION gm_chk_ack_released_qty(
		p_order_id	T501_ORDER.C501_ORDER_ID%TYPE
	) RETURN NUMBER
	IS		
		v_ack_order_id  t501_order.c501_order_id%type;		
		v_diff_qty 		NUMBER;
	BEGIN
		BEGIN 
			SELECT c501_order_id
			  INTO v_ack_order_id 
			  FROM t501_order t501 
			 WHERE c901_order_type = 101260 -- Acknowledgement Order
			 START WITH t501.c501_order_id =  p_order_id
			 CONNECT BY PRIOR c501_ref_id = c501_order_id;
		 EXCEPTION WHEN NO_DATA_FOUND THEN
	 		v_ack_order_id := NULL;
		END;
		
		BEGIN
			SELECT SUM( NVL(ack_qty,0)-NVL(bba_qty,0)) rem_qty   
			  INTO v_diff_qty
			  FROM
			     (SELECT t501.c501_order_id, t502.c205_part_number_id, SUM(t502.c502_item_qty) ack_qty
					FROM t501_order t501 , t502_item_order t502
				   WHERE t501.c501_order_id = t502.c501_order_id 
					 AND t501.c901_order_type = 101260
					 AND t501.c501_order_id = v_ack_order_id
				   GROUP BY t501.c501_order_id, t502.c205_part_number_id
			    ) ack_order
			  , (SELECT t501.c501_ref_id, t502.c205_part_number_id, SUM(t502.c502_item_qty) bba_qty
				   FROM t501_order t501  , t502_item_order t502
				  WHERE t501.c501_order_id = t502.c501_order_id 
				    AND t501.c501_ref_id = v_ack_order_id
				    AND t501.c501_status_fl = 3 --shipped
				  GROUP BY t501.c501_ref_id, t502.c205_part_number_id
			    ) bba_order
			  WHERE ack_order.c501_order_id = bba_order.c501_ref_id (+)
			    AND ack_order.c205_part_number_id = bba_order.c205_part_number_id (+);
		EXCEPTION WHEN NO_DATA_FOUND THEN
	 		v_diff_qty := '1';
		END;		
		RETURN v_diff_qty;
	END gm_chk_ack_released_qty;	
	
	/***************************************************************************************
    * Description   : Procedure to update ack order price when bba order price updated.
    * Author		: Bala
    ****************************************************************************************/
	PROCEDURE gm_save_ack_item_ord_price(
		p_orderid	 IN 	  t501_order.c501_order_id%TYPE,
  	    p_str		 IN 	  varchar2,
  	    p_userid	 IN 	  t501_order.c501_last_updated_by%TYPE
    )
    AS
    v_strlen	   NUMBER := NVL (LENGTH (p_str), 0);
	v_string	   VARCHAR2 (30000) := p_str;
	v_substring    VARCHAR2 (1000);
	v_itemid	   t502_item_order.c502_item_order_id%TYPE;
	v_part_num     t502_item_order.c205_part_number_id%TYPE;
	v_price 	   t502_item_order.c502_item_price%TYPE;	
	v_olditempr    t502_item_order.c502_item_price%TYPE;
	v_list_price 	t502_item_order.c502_list_price%TYPE;
	v_partordtyp   t502_item_order.c901_type%TYPE;
	v_capfl 	   t502_item_order.c502_construct_fl%TYPE;
	v_ack_ord_qty  t502_item_order.c502_item_qty%TYPE;
	v_bba_ord_qty  t502_item_order.c502_item_qty%TYPE;
	v_ack_order_id t501_order.c501_order_id%type;
	v_unit_price   		t502_item_order.c502_do_unit_price%TYPE;
	v_unit_price_adj   	t502_item_order.c502_unit_price_adj_value%TYPE;
	v_adj_code     		t502_item_order.c901_unit_price_adj_code%TYPE;
	v_adj_code_per   	VARCHAR2(10);
	v_adj_code_val		t502_item_order.C502_ADJ_CODE_VALUE%TYPE;
	v_acc_price  		VARCHAR2(10);
    BEGIN
	  	-- If the BBA order price is edited then need to update the ack order price too.  
		BEGIN 
		SELECT c501_order_id
		  INTO v_ack_order_id 
		  FROM t501_order t501 
		 WHERE c901_order_type = 101260 -- Acknowledgement Order
		 START WITH t501.c501_order_id =  p_orderid
	     CONNECT BY PRIOR c501_ref_id = c501_order_id;
	    EXCEPTION WHEN NO_DATA_FOUND THEN
		 RETURN;
        END;
	    
	IF v_strlen > 0
	  THEN
		WHILE INSTR (v_string, '|') <> 0
		LOOP
			v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
			v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
			v_itemid	:= NULL;
			v_price 	:= NULL;
			v_unit_price := NULL;
			v_unit_price_adj := NULL;
			v_adj_code := NULL;
			v_itemid	:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_price 	:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_unit_price	:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_unit_price_adj	:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);			
			v_adj_code 	:= v_substring;
			
			SELECT	c502_item_price , c502_item_qty , c205_part_number_id 
			        , c901_type, c502_construct_fl
			  INTO  v_olditempr , v_bba_ord_qty , v_part_num  
				    , v_partordtyp ,v_capfl 
			  FROM t502_item_order
			 WHERE c502_item_order_id = v_itemid 
			   AND c502_void_fl IS NULL
			FOR UPDATE;

			IF v_olditempr <> v_price
			THEN				
		    	IF v_ack_order_id IS NOT NULL THEN
		    	
		    		-- Need to save the adjustment details
		    		v_adj_code_per := gm_pkg_sm_adj_txn.get_adj_code_value(p_orderid,v_part_num,v_itemid, v_adj_code);
					v_adj_code_val	:= ((NVL(v_unit_price,0) - NVL(v_unit_price_adj,0)) * NVL(v_adj_code_per,0)/100);
					-- Get the unit price using below formula, net unit price + unit adjustment value + adjustment code value
			        v_acc_price := NVL(v_price,0) + NVL(v_unit_price_adj,0) + NVL(v_adj_code_val,0);
	        		
			       --PC-643 :: show list price on invoices
			        SELECT TRIM(GET_PART_PRICE_LIST(v_part_num,'L')) INTO v_list_price FROM DUAL;
			        
					SELECT	c502_item_qty
					  INTO  v_ack_ord_qty
					  FROM t502_item_order
					 WHERE c501_order_id = v_ack_order_id 
					   AND c502_item_price = v_olditempr
					   AND c502_item_order_id = v_itemid
					   AND c502_void_fl IS NULL;	
					   
					 IF v_ack_ord_qty != v_bba_ord_qty THEN
					     UPDATE t502_item_order				   		    
				   		    SET c502_item_qty = v_ack_ord_qty - v_bba_ord_qty
				          WHERE c501_order_id = v_ack_order_id
				            AND c205_part_number_id = v_part_num
				            AND c502_item_price = v_olditempr
				            AND c502_void_fl IS NULL;
				            
				        INSERT 
				          INTO t502_item_order
							   (c502_item_order_id, c501_order_id, c205_part_number_id, c502_item_qty, c502_control_number
					    	   , c502_item_price, c901_type, c502_construct_fl, c502_ref_id, c502_unit_price, c502_unit_price_adj_value
					    	   , c901_unit_price_adj_code, c502_do_unit_price, c502_adj_code_value, c502_list_price
							   )
				       	VALUES (s502_item_order.NEXTVAL, v_ack_order_id, v_part_num, v_bba_ord_qty, null
					           , v_price, v_partordtyp, v_capfl, null, v_acc_price, NVL(v_unit_price_adj,0)
					           , DECODE(v_adj_code, '0', null,v_adj_code), v_unit_price, v_adj_code_val, v_list_price
						       );   
					 
					 ELSE
						 UPDATE t502_item_order
				   		    SET c502_item_price = v_price
				   		    	 , c502_unit_price = v_acc_price
							     , c502_unit_price_adj_value = NVL(v_unit_price_adj,0)
							     , c901_unit_price_adj_code = DECODE(v_adj_code, '0', null,v_adj_code)
							     , C502_DO_UNIT_PRICE = v_unit_price
							     , C502_ADJ_CODE_VALUE = v_adj_code_val
							     , c502_list_price = v_list_price
				          WHERE c501_order_id = v_ack_order_id
				            AND c205_part_number_id = v_part_num
				            AND c502_item_price = v_olditempr
				            AND c502_void_fl IS NULL;					 
					 END IF;
		    	END IF;						
			END IF;
		END LOOP;	
	  END IF;
	END gm_save_ack_item_ord_price;    	
	
	/***************************************************************************************
     * Description : This function returns the ACK order has ability to raise OUS distributor order 
     * Author	   : APrasath
     ***************************************************************************************/	
	
	FUNCTION gm_validate_ack_ous_dist(
		p_order_id	T501_ORDER.C501_ORDER_ID%TYPE
	) RETURN VARCHAR
	IS		
		v_company_id   t1900_company.c1900_company_id%TYPE;	
		v_flag VARCHAR(1);
	BEGIN
		SELECT get_compid_frm_cntx() 
      INTO v_company_id  
      FROM dual;
		BEGIN 
			SELECT 
			   DECODE(get_rule_value_by_company('ACKORD','OUSDIST',v_company_id),'Y', 
			   DECODE(get_distributor_type(get_distributor_id(c703_sales_rep_id)),70105,'Y',NULL)
			   ,NULL) INTO v_flag
			FROM t501_order
			WHERE c501_order_id = p_order_id
			AND c501_void_fl  IS NULL;
		 EXCEPTION WHEN NO_DATA_FOUND THEN
	 		v_flag := NULL;
		END;		
		RETURN v_flag;
	END gm_validate_ack_ous_dist;
	
	/***************************************************************************************
     * Description : This procedure is to update price for ack order price when bba order price updated for PMT-38606
     * Author	   : SwethaS
     ***************************************************************************************/	

PROCEDURE gm_save_ack_item_ord_price_for_all_orders(
	    p_orderid	 IN 	  t501_order.c501_order_id%TYPE,
  	    p_str		 IN 	  varchar2,
  	    p_userid	 IN 	  t501_order.c501_last_updated_by%TYPE
    )
    AS
      v_strlen	   NUMBER := NVL (LENGTH (p_str), 0);
	v_string	   VARCHAR2 (30000) := p_str;
	v_substring    VARCHAR2 (1000);
	v_itemid	   t502_item_order.c502_item_order_id%TYPE;
	v_part_num     t502_item_order.c205_part_number_id%TYPE;
	v_price 	   t502_item_order.c502_item_price%TYPE;	
	v_olditempr    t502_item_order.c502_item_price%TYPE;
	v_partordtyp   t502_item_order.c901_type%TYPE;
	v_capfl 	   t502_item_order.c502_construct_fl%TYPE;
	v_ack_ord_qty  t502_item_order.c502_item_qty%TYPE;
	v_bba_ord_qty  t502_item_order.c502_item_qty%TYPE;
	v_ack_order_id t501_order.c501_order_id%type;
	v_unit_price   		t502_item_order.c502_do_unit_price%TYPE;
	v_unit_price_adj   	t502_item_order.c502_unit_price_adj_value%TYPE;
	v_adj_code     		t502_item_order.c901_unit_price_adj_code%TYPE;
	v_adj_code_per   	VARCHAR2(10);
	v_adj_code_val		t502_item_order.C502_ADJ_CODE_VALUE%TYPE;
	v_acc_price  		VARCHAR2(10);

CURSOR v_order_list
   IS
      SELECT t501.c501_order_id porderid 
        FROM t501_order t501
      WHERE (t501.C501_ORDER_ID = p_orderid OR  c501_parent_order_id = p_orderid)
        AND NVL(t501.c901_order_type,2521) NOT IN (SELECT C906_RULE_VALUE FROM T906_RULES WHERE C906_RULE_GRP_ID =  'EXCHANGEPRICINGORD')
     AND t501.c501_void_fl IS NULL
     AND t501.c501_delete_fl IS NULL;

    BEGIN


   FOR var_ordlist IN v_order_list
  LOOP   

	  	-- If the BBA order price is edited then need to update the ack order price too.  
	
	BEGIN 
		SELECT c501_order_id
		  INTO v_ack_order_id 
		  FROM t501_order t501 
		 WHERE c901_order_type = 101260 -- Acknowledgement Order
		 START WITH t501.c501_order_id = var_ordlist.porderid
	     CONNECT BY PRIOR c501_ref_id = c501_order_id;
	    EXCEPTION WHEN NO_DATA_FOUND THEN
		 RETURN;
        END;
	    
	IF v_strlen > 0
	  THEN
		WHILE INSTR (v_string, '|') <> 0
		LOOP
			v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
			v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
			v_itemid	:= NULL;
			v_price 	:= NULL;
			v_unit_price := NULL;
			v_unit_price_adj := NULL;
			v_adj_code := NULL;
			v_itemid	:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_price 	:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_unit_price	:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_unit_price_adj	:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);			
			v_adj_code 	:= v_substring;
			
			SELECT	c502_item_price , c502_item_qty , c205_part_number_id 
			        , c901_type, c502_construct_fl
			  INTO  v_olditempr , v_bba_ord_qty , v_part_num  
				    , v_partordtyp ,v_capfl 
			  FROM t502_item_order
			 WHERE c502_item_order_id = v_itemid 
			   AND c502_void_fl IS NULL
			FOR UPDATE;

			IF v_olditempr <> v_price
			THEN				
		    	IF v_ack_order_id IS NOT NULL THEN
		    	
		    		-- Need to save the adjustment details
		    		v_adj_code_per := gm_pkg_sm_adj_txn.get_adj_code_value(var_ordlist.porderid,v_part_num,v_itemid, v_adj_code);
v_adj_code_val	:= ((NVL(v_unit_price,0) - NVL(v_unit_price_adj,0)) * NVL(v_adj_code_per,0)/100);
-- Get the unit price using below formula, net unit price + unit adjustment value + adjustment code value
 v_acc_price := NVL(v_price,0) + NVL(v_unit_price_adj,0) + NVL(v_adj_code_val,0);
	        
	SELECT	c502_item_qty
	  INTO  v_ack_ord_qty
	FROM t502_item_order
					 WHERE c501_order_id = v_ack_order_id 
					   AND c502_item_price = v_olditempr
					   AND c502_item_order_id = v_itemid
					   AND c502_void_fl IS NULL;	
					   
					 IF v_ack_ord_qty != v_bba_ord_qty THEN
					     UPDATE t502_item_order				   		    
				   		    SET c502_item_qty = v_ack_ord_qty - v_bba_ord_qty
				          WHERE c501_order_id = v_ack_order_id
				            AND c205_part_number_id = v_part_num
				            AND c502_item_price = v_olditempr
				            AND c502_void_fl IS NULL;
				            
				        INSERT 
				          INTO t502_item_order
							   (c502_item_order_id, c501_order_id, c205_part_number_id, c502_item_qty, c502_control_number
					    	   , c502_item_price, c901_type, c502_construct_fl, c502_ref_id, c502_unit_price, c502_unit_price_adj_value
					    	   , c901_unit_price_adj_code, c502_do_unit_price, c502_adj_code_value
							   )
				       	VALUES (s502_item_order.NEXTVAL, v_ack_order_id, v_part_num, v_bba_ord_qty, null
					           , v_price, v_partordtyp, v_capfl, null, v_acc_price, NVL(v_unit_price_adj,0)
					           , DECODE(v_adj_code, '0', null,v_adj_code), v_unit_price, v_adj_code_val
						       );   
					 
					 ELSE
						 UPDATE t502_item_order
				   		    SET c502_item_price = v_price
				   		    	 , c502_unit_price = v_acc_price
							     , c502_unit_price_adj_value = NVL(v_unit_price_adj,0)
							     , c901_unit_price_adj_code = DECODE(v_adj_code, '0', null,v_adj_code)
							     , C502_DO_UNIT_PRICE = v_unit_price
							     , C502_ADJ_CODE_VALUE = v_adj_code_val
				          WHERE c501_order_id = v_ack_order_id
				            AND c205_part_number_id = v_part_num
				            AND c502_item_price = v_olditempr
				            AND c502_void_fl IS NULL;					 
					 END IF;
		    	END IF;						
			END IF;
		END LOOP;	
	  END IF;
END LOOP;
	END gm_save_ack_item_ord_price_for_all_orders;    	
	
	
 END gm_pkg_op_ack_order;
/
