--@'C:\oneportal\db\Packages\Operations\gm_pkg_op_plant_rpt.bdy';
CREATE OR REPLACE PACKAGE BODY gm_pkg_op_plant_rpt
IS
	/*******************************************************
   * Description : Procedure to fetch Plant information
   *******************************************************/
--
	PROCEDURE gm_fch_plant_info (
      p_out  OUT      TYPES.cursor_type
	) AS
	BEGIN
		  OPEN p_out FOR
			SELECT c5040_plant_id plantid,c5040_plant_name plantnm,c5040_plant_cd plantcd 
			  FROM T5040_PLANT_MASTER 
			 WHERE c5040_void_fl IS NULL;
	END gm_fch_plant_info;
	/*******************************************************
   * Description : Procedure to fetch Plant company mapping information
   *******************************************************/
	--	
	PROCEDURE gm_fch_plant_comp_mapping (
	      p_out  OUT      TYPES.cursor_type
		) AS
		v_company_id VARCHAR2(100);
		BEGIN
		
		select SYS_CONTEXT('CLIENTCONTEXT','COMPANY_ID') INTO v_company_id FROM DUAL; 
		
		  OPEN p_out FOR
			SELECT T5040.c5040_plant_id plantid,
			  T5040.c5040_plant_name plantnm,
			  T5040.c5040_plant_cd plantcd,
			  t5041.c1900_company_id COMPID,
			  t5041.c5041_primary_fl primaryfl
			FROM T5040_PLANT_MASTER T5040,
			  T5041_PLANT_COMPANY_MAPPING T5041
			  WHERE  T5040.c5040_plant_id= T5041.c5040_plant_id
			  AND t5041.c1900_company_id = NVL(v_company_id,t5041.c1900_company_id)
			  AND t5041.c5041_void_fl IS NULL
			  AND  t5040.c5040_void_fl IS NULL
			  ORDER BY t5041.c1900_company_id,UPPER(T5040.c5040_plant_name);
	END gm_fch_plant_comp_mapping;	
	/*******************************************************
   * Description : Function to get default Plant for company
   *******************************************************/
	--	
	FUNCTION get_default_plant_for_comp (
	        p_company_id T1900_COMPANY.C1900_COMPANY_ID%TYPE)
	    RETURN NUMBER
	IS
	    v_plant_id T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
	BEGIN
	    BEGIN
	         SELECT T5041.c5040_plant_id
	           INTO v_plant_id
	           FROM T5041_PLANT_COMPANY_MAPPING T5041
	          WHERE t5041.c5041_void_fl   IS NULL
	            AND t5041.c1900_company_id = p_company_id
	            AND T5041.C5041_PRIMARY_FL = 'Y';
	    EXCEPTION
	    WHEN OTHERS THEN
	        v_plant_id := NULL;
	    END;
	    RETURN V_PLANT_ID;
	END get_default_plant_for_comp;	

	/*****************************************************************
   * Description : Procedure to load plant name based on Party ID info
   * Version 	 : 2.0 (PC-4485)
   * Author 	 : Jayaprasanth Gurunathan
   ********************************************************************/	
	PROCEDURE gm_fch_party_plant_mapping(
	    p_party_id T5040_PLANT_MASTER.c5040_plant_id%TYPE ,
	    p_company_id T1900_COMPANY.C1900_COMPANY_ID%TYPE ,
	    p_out_plant_list_cur OUT TYPES.cursor_type)
	AS
	  v_cnt NUMBER;
	BEGIN
	  SELECT COUNT (1)
	  INTO v_cnt
	  FROM t5042_plant_company_party_map
	  WHERE C101_PARTY_ID  = p_party_id
	  AND c1900_COMPANY_ID = p_company_id
	  AND C5042_VOID_FL   IS NULL;
	  
	  IF v_cnt             = 0 THEN
	    gm_fch_plant_comp_mapping(p_out_plant_list_cur); 
	    -- calling existing Procedure if party id and company is not available in t5042_plant_company_party_map
	  ELSE
	    OPEN p_out_plant_list_cur FOR 
	    SELECT T5040.c5040_plant_id plantid,
	    T5040.c5040_plant_name plantnm,
	    T5040.c5040_plant_cd plantcd,
	    t5042.c1900_company_id COMPID,
	    gm_pkg_op_plant_rpt.get_company_plant_primary_flg(t5042.c1900_company_id,T5040.c5040_plant_id) primaryfl
	    FROM T5040_PLANT_MASTER T5040,
	    t5042_plant_company_party_map T5042 
	    WHERE t5040.c5040_plant_id = T5042.c5040_plant_id
	    AND T5042.C1900_COMPANY_ID = p_company_id 
	    AND T5042.c101_party_id = p_party_id 
	    AND t5040.c5040_void_fl IS NULL 
	    AND T5042.c5042_void_fl IS NULL 
	    order by t5042.C1900_COMPANY_ID,
	    UPPER(T5040.c5040_plant_name);
	  END IF;
	END gm_fch_party_plant_mapping;
	
	
	/**********************************************************************
	  * Description     : Functions returns primary flag combination based on Company ID and Plant ID
	  * Parameters   	: p_company_id, p_plant_id
	  * Author 			: Jayaprasanth Gurunathan
	  * Task			: PC-4485
	  ***********************************************************************/
	FUNCTION get_company_plant_primary_flg(
	   	p_company_id T5041_PLANT_COMPANY_MAPPING.C1900_COMPANY_ID%TYPE,
	    p_plant_id T5041_PLANT_COMPANY_MAPPING.C5040_PLANT_ID%TYPE)
	  RETURN T5041_PLANT_COMPANY_MAPPING.C5041_PRIMARY_FL%TYPE
		
	IS
	  v_primary_flag T5041_PLANT_COMPANY_MAPPING.C5041_PRIMARY_FL%TYPE;
	BEGIN
		BEGIN
	    SELECT C5041_PRIMARY_FL
	    	INTO v_primary_flag
	    FROM T5041_PLANT_COMPANY_MAPPING
	    WHERE C1900_COMPANY_ID = p_company_id
	    AND C5040_PLANT_ID     = p_plant_id
	    AND C5041_VOID_FL     IS NULL
	    AND C5041_PRIMARY_FL   = 'Y';
	    RETURN v_primary_flag;
	  EXCEPTION
	  WHEN OTHERS THEN
	    RETURN NULL;
	  END;
	END get_company_plant_primary_flg;
	
	
		/**********************************************************************
 	* Description : Procedure used to fetch plantId based on partyId
 	* Author      : saravanan M
	************************************************************************/
PROCEDURE gm_fch_party_asstcompany_plants (
	p_party_id				IN		t704_account.c101_party_id%TYPE,
	p_company_id			IN		t5506_robot_quotation.c1900_company_id%TYPE,
	p_out_plant_list		OUT		CLOB)

AS
BEGIN
		
		SELECT JSON_ARRAYAGG(JSON_OBJECT('plantId' VALUE t.plantid,'plantName' VALUE t.plantnm )order by t.plantnm) INTO p_out_plant_list
		FROM
		(SELECT  t5040.C5040_PLANT_ID plantid, 
		t5040.C5040_PLANT_NAME plantnm
		FROM T5041_PLANT_COMPANY_MAPPING t5041, 
		T1019_PARTY_COMPANY_MAP t1019, 
		T5040_PLANT_MASTER t5040
		WHERE t5041.C1900_COMPANY_ID = t1019.C1900_COMPANY_ID
		AND t5040.C5040_PLANT_ID = t5041.C5040_PLANT_ID
		AND t5041.C1900_COMPANY_ID = DECODE(p_company_id, NULL, t5041.C1900_COMPANY_ID, p_company_id )
		AND t1019.C101_PARTY_ID = p_party_id
		AND t5040.C5040_VOID_FL IS NULL
		AND t5041.C5041_VOID_FL IS NULL
		AND t1019.C1019_VOID_FL IS NULL
        GROUP BY t5040.C5040_PLANT_ID,t5040.C5040_PLANT_NAME
		ORDER BY t5040.C5040_PLANT_NAME)t;
END gm_fch_party_asstcompany_plants;
	
END gm_pkg_op_plant_rpt;
/
