/* Formatted on 2008/08/21 18:13 (Formatter Plus v4.8.0) */
-- @"C:\Database\Packages\Operations\gm_pkg_op_ttp_process_request.bdy";
CREATE OR REPLACE PACKAGE BODY gm_pkg_op_ttp_process_request
IS
--
	/*******************************************************
   * Description : Procedure to process the forecast data from ttp
				   and initiate the set or initiate the back order
   * Author 	 : VPrasath
   *******************************************************/
--
	PROCEDURE gm_sav_process_request (
		p_ttp_detail_id   IN   t4052_ttp_detail.c4052_ttp_detail_id%TYPE
	)
	AS
		v_userid	   t4052_ttp_detail.c4052_created_by%TYPE;
		v_request_by   t4040_demand_sheet.c4040_primary_user%TYPE;
		v_demand_type  t4040_demand_sheet.c901_demand_type%TYPE;
		v_demand_sheet_id t4040_demand_sheet.c4040_demand_sheet_id%TYPE;

		CURSOR v_demand_check_cur
		IS
			SELECT t4040.c4020_demand_master_id txn_id, TRUNC (SYSDATE) request_date
				 , LAST_DAY (c4043_start_date) required_date, c4030_ref_id ref_id
				 , t4043.c4043_value required_qty
				 , t4030.c901_ref_type ref_type   -- to identify if its set or item
			  FROM t4040_demand_sheet t4040
			  	 , t4043_demand_sheet_growth t4043
			  	 , t4030_demand_growth_mapping t4030
			 WHERE t4040.c4052_ttp_detail_id = p_ttp_detail_id
			   AND t4040.c4040_demand_sheet_id = t4043.c4040_demand_sheet_id
			   AND t4030.c4030_demand_growth_id = t4043.c4030_demand_growth_id
			   AND c4043_start_date BETWEEN t4040.c4040_demand_period_dt
										AND ADD_MONTHS (t4040.c4040_demand_period_dt, (t4040.c4040_request_period - 1))
			   AND t4043.c4043_value > 0
			   AND t4030.c4030_void_fl IS NULL
			   AND t4040.c4040_void_fl IS NULL
			   AND t4040.c901_demand_type IN ('40021', '40022');
	BEGIN
		SELECT NVL (c4052_last_updated_by, c4052_created_by)
		  INTO v_userid
		  FROM t4052_ttp_detail
		 WHERE c4052_ttp_detail_id = p_ttp_detail_id;

		FOR cur_val IN v_demand_check_cur
		LOOP
			SELECT c4040_primary_user, c901_demand_type, c4040_demand_sheet_id
			  INTO v_request_by, v_demand_type, v_demand_sheet_id
			  FROM t4040_demand_sheet
			 WHERE c4020_demand_master_id = cur_val.txn_id 
			   AND c4052_ttp_detail_id = p_ttp_detail_id;

			IF cur_val.ref_type = 20296   /* SET */
			THEN
				gm_pkg_op_ttp_process_request.gm_sav_request_set (cur_val.txn_id
																, 50616
																, cur_val.ref_id
																, cur_val.request_date
																, cur_val.required_date
																, cur_val.required_qty
																, v_userid
																, v_request_by
																, v_demand_type
																, v_demand_sheet_id
																 );
			ELSIF cur_val.ref_type = 20298	 /*ITEM REQUEST */
			THEN
				gm_pkg_op_ttp_process_request.gm_sav_request_item (cur_val.txn_id
																 , 50616
																 , cur_val.ref_id
																 , cur_val.request_date
																 , cur_val.required_date
																 , cur_val.required_qty
																 , v_userid
																 , v_request_by
																 , v_demand_type
																 , v_demand_sheet_id
																  );
			END IF;
		END LOOP;
	END gm_sav_process_request;

--
	/*******************************************************
   * Description : Procedure to Initiate the request
				   to build or back order the set
   * Author 	 : VPrasath
   *******************************************************/
--
	PROCEDURE gm_sav_request_set (
		p_request_txn_id	IN	 t520_request.c520_request_txn_id%TYPE
	  , p_request_source	IN	 t520_request.c901_request_source%TYPE
	  , p_set_id			IN	 t520_request.c207_set_id%TYPE
	  , p_request_date		IN	 t520_request.c520_request_date%TYPE
	  , p_required_date 	IN	 t520_request.c520_required_date%TYPE
	  , p_required_qty		IN	 NUMBER
	  , p_userid			IN	 t4052_ttp_detail.c4052_created_by%TYPE
	  , p_request_by		IN	 t4040_demand_sheet.c4040_primary_user%TYPE
	  , p_demand_type		IN	 t4040_demand_sheet.c901_demand_type%TYPE
	  , p_demand_sheet_id	IN	 t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	)
	AS
		v_no_sets	   NUMBER := p_required_qty;
		v_req_count    NUMBER;
		v_request_id   t520_request.c520_request_id%TYPE;
		v_consign_id   t504_consignment.c504_consignment_id%TYPE;
		v_status_fl    t520_request.c520_status_fl%TYPE;
	BEGIN
		SELECT COUNT (c520_request_id)
		  INTO v_req_count
		  FROM t520_request
		 WHERE c520_request_txn_id = p_request_txn_id
		   AND c901_request_source = p_request_source
		   AND c207_set_id = p_set_id
		   AND c520_required_date BETWEEN TRUNC (p_required_date, 'mm') 
		   							  AND LAST_DAY (p_required_date)
		   AND c520_status_fl > 0	--ONHOLD
		   AND c520_master_request_id IS NULL
		   AND c520_void_fl IS NULL;

		-- IF there are no entries in the request table, create one MR for each set
		-- IF the entries in the Request Table is less than the No of Sets from the  p_demand_check cur then create MR for the remaining set count
		IF (v_req_count = 0 OR v_no_sets > v_req_count)
		THEN
			WHILE (v_no_sets - v_req_count > 0)
			LOOP
				BEGIN
					SELECT c520_request_id
					  INTO v_request_id
					  FROM (SELECT	 t520.c520_request_id, t520.c520_status_fl
								FROM t520_request t520
							   WHERE t520.c520_request_txn_id IS NULL	--t520.c901_request_source <> 50616
								 AND t520.c520_request_to IS NULL
								 AND t520.c520_void_fl IS NULL
								 AND t520.c520_delete_fl IS NULL
								 AND t520.c520_request_to IS NULL
								 AND t520.c207_set_id = p_set_id
								 AND t520.c520_master_request_id IS NULL
								 AND t520.C520_STATUS_FL <> '40' -- Exclude Sets in Complete Status.
							ORDER BY t520.c520_status_fl DESC)
					 WHERE ROWNUM = 1;

					UPDATE t520_request t520
					   SET t520.c520_request_for = p_demand_type
						 , t520.c901_request_source = p_request_source
						 , t520.c520_required_date = p_required_date
						 , t520.c520_request_txn_id = p_request_txn_id
						 , t520.c520_last_updated_by = p_userid
						 , t520.c520_last_updated_date = SYSDATE
					 WHERE (t520.c520_request_id = v_request_id OR t520.c520_master_request_id = v_request_id);
				EXCEPTION
					WHEN NO_DATA_FOUND
					THEN
						gm_pkg_op_process_request.gm_sav_initiate_set (''
																	 , TO_CHAR (p_required_date, 'MM/DD/YYYY')
																	 , p_request_source
																	 , p_request_txn_id
																	 , p_set_id
																	 , p_demand_type
																	 , NULL
																	 , 50625
																	 , p_request_by
																	 , NULL
																	 , NULL
																	 , NULL
																	 , 0
																	 , p_userid
																	 , NULL	
																	 , NULL	--plan ship date
																	 , v_request_id
																	 , v_consign_id
																	  );
				END;

				SELECT c520_status_fl
				  INTO v_status_fl
				  FROM t520_request
				 WHERE c520_request_id = v_request_id;

				gm_pkg_op_ttp_process_request.gm_sav_demand_sheet_request (p_demand_sheet_id
																		 , v_request_id
																		 , 50631
																		 , v_status_fl
																		  );   -- Need to insert into T4044_DEMAND_SHEET_REQUEST (seperate procedure)
				v_no_sets	:= v_no_sets - 1;
			END LOOP;
		END IF;
	END gm_sav_request_set;
--
	/*******************************************************
   * Description : Procedure to Initiate the request in web globus
				   to build or back order the set
   * Author 	 : VPrasath
   *******************************************************/
--
	PROCEDURE gm_sav_ttp_set_request (
		p_request_txn_id	IN	 t520_request.c520_request_txn_id%TYPE
	  , p_request_source	IN	 t520_request.c901_request_source%TYPE
	  , p_set_id			IN	 t520_request.c207_set_id%TYPE
	  , p_request_date		IN	 t520_request.c520_request_date%TYPE
	  , p_required_date 	IN	 t520_request.c520_required_date%TYPE
	  , p_required_qty		IN	 NUMBER
	  , p_userid			IN	 t4052_ttp_detail.c4052_created_by%TYPE
	  , p_request_by		IN	 t4040_demand_sheet.c4040_primary_user%TYPE
	  , p_demand_type		IN	 t4040_demand_sheet.c901_demand_type%TYPE
	  , p_demand_sheet_id	IN	 t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	)
	AS
		v_no_sets	   NUMBER := p_required_qty;
		v_req_count    NUMBER;
		v_request_id   t520_request.c520_request_id%TYPE;
		v_consign_id   t504_consignment.c504_consignment_id%TYPE;
		v_status_fl    t520_request.c520_status_fl%TYPE;
		
	BEGIN
		
        	
		 SELECT COUNT (c520_request_id)
			  INTO v_req_count
			  FROM t520_request
			 WHERE c520_request_txn_id = p_request_txn_id
			   AND c901_request_source = p_request_source
			   AND c207_set_id = p_set_id
	   		 --For SET PAR the Required Date will be null, hence added this to handle null scenario.
			   AND NVL(c520_required_date,SYSDATE) BETWEEN TRUNC (NVL(p_required_date,SYSDATE), 'mm') 
			   							  AND LAST_DAY (NVL(p_required_date,SYSDATE))
			   AND c520_status_fl > 0	--ONHOLD
			   -- For SET PAR RQ we should not consider Closed RQ, it should be considered for OP Req Source
			   -- So using any number more than 40 will consider the closed RQ as well.
			   AND c520_status_fl < DECODE(p_request_source,4000122,40,50)
			   AND c520_master_request_id IS NULL
			   AND C1900_COMPANY_ID = 1000
			   AND c520_void_fl IS NULL;
			   
		-- IF there are no entries in the request table, create one MR for each set
		-- IF the entries in the Request Table is less than the No of Sets from the  p_demand_check cur then create MR for the remaining set count
		-- p_request_source != 4000122 --> using this condition because... for set PAR new requests have to created.. should not update old records 
		IF (v_req_count = 0 OR v_no_sets > v_req_count)
		THEN
			WHILE (v_no_sets - v_req_count > 0)
			LOOP
				BEGIN
					SELECT c520_request_id
					  INTO v_request_id
					  FROM (SELECT	 t520.c520_request_id, t520.c520_status_fl
								FROM t520_request t520
							   WHERE t520.c520_request_txn_id IS NULL	--t520.c901_request_source <> 50616
								 AND t520.c520_request_to IS NULL
								 AND t520.c520_void_fl IS NULL
								 AND t520.c520_delete_fl IS NULL
								 AND t520.c520_request_to IS NULL
								 AND t520.c207_set_id = p_set_id
								 AND t520.c520_master_request_id IS NULL
								 AND t520.C520_STATUS_FL <> '40' -- Exclude Sets in Complete Status.
								 AND p_request_source != 4000122 -- SET PAR
								 AND C1900_COMPANY_ID = 1000
							ORDER BY t520.c520_status_fl DESC)
					 WHERE ROWNUM = 1;

					UPDATE t520_request t520
					   SET t520.c520_request_for = p_demand_type
						 , t520.c901_request_source = p_request_source
						 , t520.c520_required_date = p_required_date
						 , t520.c520_request_txn_id = p_request_txn_id
						 , t520.c520_last_updated_by = p_userid
						 , t520.c520_last_updated_date = SYSDATE
					 WHERE (t520.c520_request_id = v_request_id OR t520.c520_master_request_id = v_request_id);
				EXCEPTION
					WHEN NO_DATA_FOUND
					THEN
						gm_pkg_op_process_request.gm_sav_initiate_set ('' -- Request Id
																	 , TO_CHAR (p_required_date, 'MM/DD/YYYY')
																	 , p_request_source
																	 , p_request_txn_id
																	 , p_set_id
																	 , p_demand_type
																	 , NULL --Request To
																	 , 50625 -- Employee ( Request By Type )
																	 , p_request_by 
																	 , NULL -- Ship To
																	 , NULL -- Ship To ID
																	 , NULL -- Master Request ID
																	 , 0 -- Status Flag
																	 , p_userid
																	 , NULL -- Purpose
																	 , NULL	-- plan ship date
																	 , v_request_id -- OUT Param
																	 , v_consign_id -- OUT Param
																	  );
				END;

				SELECT c520_status_fl
				  INTO v_status_fl
				  FROM t520_request
				 WHERE c520_request_id = v_request_id;

				 -- Commented this procedure call as this will throw a integrity constraint in web globus for the demand sheet id from dm operations during lock and generate
				 
				 /*
					gm_pkg_op_ttp_process_request.gm_sav_demand_sheet_request (p_demand_sheet_id
																		 , v_request_id
																		 , 50631
																		 , v_status_fl
																		  );   -- Need to insert into T4044_DEMAND_SHEET_REQUEST (seperate procedure)
				*/
				v_no_sets	:= v_no_sets - 1;
			END LOOP;
		END IF;
	END gm_sav_ttp_set_request;
--
  /*******************************************************
   * Description : Procedure to Initiate a request for Item
   * Author 	 : VPrasath
   *******************************************************/
--
	PROCEDURE gm_sav_request_item (
		p_request_txn_id	IN	 t520_request.c520_request_txn_id%TYPE
	  , p_request_source	IN	 t520_request.c901_request_source%TYPE
	  , p_part_number_id	IN	 t205_part_number.c205_part_number_id%TYPE
	  , p_request_date		IN	 t520_request.c520_request_date%TYPE
	  , p_required_date 	IN	 t520_request.c520_required_date%TYPE
	  , p_required_qty		IN	 NUMBER
	  , p_userid			IN	 t4052_ttp_detail.c4052_created_by%TYPE
	  , p_request_by		IN	 t4040_demand_sheet.c4040_primary_user%TYPE
	  , p_demand_type		IN	 t4040_demand_sheet.c901_demand_type%TYPE
	  , p_demand_sheet_id	IN	 t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	)
	AS
		v_request_count NUMBER;
		v_consignment_count NUMBER;
		v_total_required_count NUMBER;
		v_status_fl    t520_request.c520_status_fl%TYPE;
		v_request_id   t520_request.c520_request_id%TYPE;
	BEGIN
		SELECT NVL (SUM (t521.c521_qty), 0) qty
		  INTO v_request_count
		  FROM t520_request t520, t521_request_detail t521
		 WHERE t520.c520_request_txn_id = p_request_txn_id
		   AND t520.c901_request_source = p_request_source	 -- Order planning
		   AND t520.c207_set_id IS NULL
		   AND t520.c520_required_date = p_required_date
		   AND t520.c520_request_id = t521.c520_request_id
		   AND t520.c520_status_fl > 0	 --ONHOLD
		   AND t521.c205_part_number_id = p_part_number_id
		   AND c520_void_fl IS NULL;

		SELECT NVL (SUM (c505_item_qty), 0) qty
		  INTO v_consignment_count
		  FROM t504_consignment t504, t505_item_consignment t505
		 WHERE t504.c504_consignment_id = t505.c504_consignment_id
		   AND t504.c504_void_fl IS NULL
		   AND c520_request_id IN (
				   SELECT t520.c520_request_id
					 FROM t520_request t520, t521_request_detail t521
					WHERE t520.c520_request_txn_id = p_request_txn_id
					  AND t520.c901_request_source = p_request_source	-- Order planning
					  AND t520.c207_set_id IS NULL
					  AND t520.c520_required_date = p_required_date
					  AND t520.c520_request_id = t521.c520_request_id
					  AND t520.c520_status_fl > 0	--ONHOLD
					  AND t521.c205_part_number_id = p_part_number_id
					  AND t520.c520_void_fl IS NULL);

		v_total_required_count := v_request_count + v_consignment_count;

		IF (v_total_required_count = 0 OR p_required_qty > v_total_required_count)
		THEN
			gm_pkg_op_process_request.gm_sav_backorder_item (p_required_date
														   , p_request_source
														   , p_request_txn_id
														   , p_part_number_id
														   , p_demand_type
														   , NULL
														   , 50625
														   , p_request_by
														   , NULL
														   , NULL
														   , NULL
														   , p_userid
														   , p_required_qty - v_total_required_count
														   , v_request_id
															);	 -- call new item consignment request procedure and TOTal required qty (v_required_qty - req_count )

			SELECT c520_status_fl
			  INTO v_status_fl
			  FROM t520_request
			 WHERE c520_request_id = v_request_id;

			gm_pkg_op_ttp_process_request.gm_sav_demand_sheet_request (p_demand_sheet_id
																	 , v_request_id
																	 , 50631
																	 , v_status_fl
																	  );   -- Need to insert into T4044_DEMAND_SHEET_REQUEST (seperate procedure)
		END IF;
	END gm_sav_request_item;

--
	/*******************************************************
   * Description : Procedure to save demand sheet request
   * Author 	 : VPrasath
   *******************************************************/
--
	PROCEDURE gm_sav_demand_sheet_request (
		p_demand_sheet_id	IN	 t4044_demand_sheet_request.c4040_demand_sheet_id%TYPE
	  , p_request_id		IN	 t4044_demand_sheet_request.c520_request_id%TYPE
	  , p_source_type		IN	 t4044_demand_sheet_request.c901_source_type%TYPE
	  , p_status_fl 		IN	 t4044_demand_sheet_request.c4044_lock_status_fl%TYPE
	)
	AS
	BEGIN
		INSERT INTO t4044_demand_sheet_request
					(c4044_demand_sheet_request_id, c4040_demand_sheet_id, c520_request_id, c901_source_type
				   , c4044_lock_status_fl
					)
			 VALUES (s4044_demand_sheet_request.NEXTVAL, p_demand_sheet_id, p_request_id, p_source_type
				   , p_status_fl
					);
	END gm_sav_demand_sheet_request;
	
	
--
	/*******************************************************
   * Description : Procedure to Initiate the request in web globus
				   to build or back order the set
   * Author 	 : gpalani
   *******************************************************/
--
	PROCEDURE gm_sav_ttp_set_par (
		p_request_txn_id	IN	 t520_request.c520_request_txn_id%TYPE
	  , p_request_source	IN	 t520_request.c901_request_source%TYPE
	  , p_set_id			IN	 t520_request.c207_set_id%TYPE
	  , p_request_date		IN	 t520_request.c520_request_date%TYPE
	  , p_required_date 	IN	 t520_request.c520_required_date%TYPE
	  , p_required_qty		IN	 NUMBER
	  , p_userid			IN	 t4052_ttp_detail.c4052_created_by%TYPE
	  , p_request_by		IN	 t4040_demand_sheet.c4040_primary_user%TYPE
	  , p_demand_type		IN	 t4040_demand_sheet.c901_demand_type%TYPE
	  , p_demand_sheet_id	IN	 t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	)
	AS
		v_no_sets	   NUMBER := p_required_qty;
		v_req_count    NUMBER;
		v_request_id   t520_request.c520_request_id%TYPE;
		v_consign_id   t504_consignment.c504_consignment_id%TYPE;
		v_status_fl    t520_request.c520_status_fl%TYPE;
		
	BEGIN
		
       	
		 SELECT COUNT (c520_request_id)
			  INTO v_req_count
			  FROM t520_request
			 WHERE 
			    c207_set_id = p_set_id
	   		   AND NVL(c520_required_date,SYSDATE) BETWEEN TRUNC (NVL(p_required_date,SYSDATE), 'mm') 
			   AND LAST_DAY (NVL(p_required_date,SYSDATE))
         	 AND c520_request_to IS NULL
			   AND c520_status_fl > 0	--ONHOLD
		       AND c520_status_fl < 40
			   AND c520_master_request_id IS NULL
			   AND C1900_COMPANY_ID = 1000
	      	   AND c901_request_source = 4000122 -- SET PAR
    		   AND c520_void_fl IS NULL;
			   
			   
		-- IF there are no entries in the request table, create one MR for each set
		-- IF the entries in the Request Table is less than the No of Sets from the  p_demand_check cur then create MR for the remaining set count
		-- p_request_source != 4000122 --> using this condition because... for set PAR new requests have to created.. should not update old records 
		IF (v_req_count = 0 OR v_no_sets > v_req_count)
		THEN
			WHILE (v_no_sets - v_req_count > 0)
			LOOP

				BEGIN
					SELECT c520_request_id
					  INTO v_request_id
					  FROM (SELECT	 t520.c520_request_id, t520.c520_status_fl
								FROM t520_request t520
							   WHERE 
							     (c901_request_source <> 50616 OR c520_request_txn_id IS NULL)
								 AND t520.c520_request_to IS NULL
								 AND t520.c520_void_fl IS NULL
								 AND t520.c520_delete_fl IS NULL
								 AND t520.c207_set_id = p_set_id
								 AND t520.c520_master_request_id IS NULL
								 AND c901_request_source != 4000122 -- SET PAR
								 AND t520.C520_STATUS_FL <> '40' -- Exclude Sets in Complete Status.
								 AND C1900_COMPANY_ID = 1000
							ORDER BY t520.c520_status_fl DESC)
					 WHERE ROWNUM = 1;

					UPDATE t520_request t520
					   SET t520.c520_request_for = p_demand_type
						 , t520.c901_request_source = p_request_source
						 , t520.c520_required_date = NULL
						 , t520.c520_request_txn_id = p_request_txn_id
						 , t520.c520_last_updated_by = p_userid
						 , t520.c520_last_updated_date = SYSDATE
					 WHERE (t520.c520_request_id = v_request_id OR t520.c520_master_request_id = v_request_id);
				EXCEPTION
					WHEN NO_DATA_FOUND
					THEN
						gm_pkg_op_process_request.gm_sav_initiate_set ('' -- Request Id
																	 , TO_CHAR (p_required_date, 'MM/DD/YYYY')
																 , p_request_source
																	 , p_request_txn_id
																	 , p_set_id
																	 , p_demand_type
																	 , NULL --Request To
																	 , 50625 -- Employee ( Request By Type )
																	 , p_request_by 
																	 , NULL -- Ship To
																	 , NULL -- Ship To ID
																	 , NULL -- Master Request ID
																	 , 0 -- Status Flag
																	 , p_userid
																	 , NULL -- Purpose
																	 , NULL	-- plan ship date
																	 , v_request_id -- OUT Param
																	 , v_consign_id -- OUT Param
																	  );
				END;

				SELECT c520_status_fl
				  INTO v_status_fl
				  FROM t520_request
				 WHERE c520_request_id = v_request_id;

				 -- Commented this procedure call as this will throw a integrity constraint in web globus for the demand sheet id from dm operations during lock and generate
				 
				 /*
					gm_pkg_op_ttp_process_request.gm_sav_demand_sheet_request (p_demand_sheet_id
																		 , v_request_id
																		 , 50631
																		 , v_status_fl
																		  );   -- Need to insert into T4044_DEMAND_SHEET_REQUEST (seperate procedure)
				*/
				v_no_sets	:= v_no_sets - 1;
			END LOOP;
		END IF;
	END gm_sav_ttp_set_par;
--

END gm_pkg_op_ttp_process_request;
/
