/* Formatted on 2019/12/09 12:30 (Formatter Plus v4.8.0) */
--@"C:\pmt\bit\SpineIT-ERP\Database\Packages\Operations\shipping\gm_pkg_op_shipping_validate.bdy";
create or replace
PACKAGE BODY gm_pkg_op_shipping_validate
IS
	/*************************************************************************
   	* Description : Procedure to get tissue part detail based on shipping id
   	* Author      : Ramachandiran T.S PMT-40555
   	**************************************************************************/

	PROCEDURE gm_fch_tissue_flag_by_id(
		p_ship_id        IN       t907_shipping_info.c907_shipping_id%TYPE,
		p_tissue_flag    OUT      t207_set_master.c207_tissue_fl%TYPE
	)
	AS
		v_source		t907_shipping_info.c901_source%TYPE;
		v_ref_id		t907_shipping_info.c907_ref_id%TYPE;
		v_company_id   	t1900_company.c1900_company_id%TYPE;
		v_set_count		NUMBER;
		v_flag_count 	NUMBER;
		v_txn_type_count NUMBER;
		v_plant_id		t5040_plant_master.c5040_plant_id%TYPE;
		v_company_tissue_fl    VARCHAR2(20);
		
	BEGIN
	
		 SELECT NVL (get_compid_frm_cntx (), get_rule_value ('DEFAULT_COMPANY', 'ONEPORTAL'))
       		INTO v_company_id
       		FROM DUAL;     
       		  
     	SELECT NVL (get_plantid_frm_cntx (), get_rule_value ('DEFAULT_PLANT', 'ONEPORTAL'))
       		INTO v_plant_id
       		FROM DUAL;
       		
-- Added the below code so that the procedure will not invoke for other than GMNA.
       	
       	v_company_tissue_fl := NVL(get_rule_value(v_company_id,'TISSUE_VALIDATION'),'N');

       	IF(v_company_tissue_fl='N') THEN

       		RETURN;
       	
       	END IF;
       	
       		
		BEGIN
		
			SELECT t907.c907_ref_id, t907.c901_source
        		INTO v_ref_id, v_source
       	 		FROM t907_shipping_info t907
       		WHERE t907.c907_shipping_id 		= p_ship_id
        		AND t907.c907_void_fl    	IS NULL
       			AND t907.c1900_company_id 	= v_company_id;
       			
		EXCEPTION
		
        	WHEN OTHERS
        	THEN
        		v_source := null;
        		v_ref_id := null;
        		RETURN;
    	END;
    	
	IF v_source = 50181 -- Consignment
	
	THEN
	
		BEGIN	
		
			SELECT COUNT (1)
   				INTO v_set_count
   				FROM t504_consignment t504
  			WHERE t504.c207_set_id        	IS NOT NULL
    			AND t504.c504_consignment_id = v_ref_id
    			AND t504. c504_void_fl       	IS NULL
    			AND t504.c1900_company_id    = v_company_id;
    			
    	EXCEPTION
        	WHEN NO_DATA_FOUND
        	THEN
        		v_set_count := 0;
        END;	
        	
    IF 	v_set_count = 0
    
    THEN
    	BEGIN
    		SELECT count( DISTINCT t504.c504_consignment_id) 
    			INTO v_flag_count 
    			FROM t504_consignment t504, 
    			t505_item_consignment t505, 
    			v205_parts_with_ext_vendor v205p
  			WHERE t504.c504_consignment_id  	= t505.c504_consignment_id
    			AND t504.c504_void_fl 			IS NULL
    			AND t505.c505_void_fl 			IS NULL
    			AND v205p.c205_part_number_id 	= t505.c205_part_number_id
    			AND t504.c504_consignment_id  	= v_ref_id
    			GROUP BY t504.c504_consignment_id;
    			
    	EXCEPTION
    		WHEN NO_DATA_FOUND
        	THEN
        		v_flag_count := 0;		
    	END;	
    			
    ELSE
    	BEGIN	
    		SELECT count(1) 
    			INTO v_flag_count 
    			FROM t504_consignment t504, 
    			t207_set_master t207
    		WHERE 
			    t504.c504_void_fl   		IS NULL
				AND t207.c207_set_id		=	t504.c207_set_id
				AND t207.c207_tissue_fl 	IS NOT null
				AND t207.c207_void_fl 		IS NULL
				AND t504.c504_consignment_id =	v_ref_id;
				
		EXCEPTION
			WHEN NO_DATA_FOUND
        	THEN
        		v_flag_count := 0;		
    		END;	
    	END IF;
    ELSIF v_source = 50182 -- Loaner
    THEN	
		BEGIN		
	-- Distinct and Group by used to get a single record.	
	
		SELECT count(DISTINCT t504.c504_consignment_id) 
    			INTO v_flag_count
				FROM t504_consignment t504,
				t505_item_consignment t505, 
				t207_set_master t207
			WHERE t504.c504_consignment_id		=	t505.c504_consignment_id
				AND t504.c504_void_fl 			IS NULL
				AND t207.c207_set_id			=	t504.c207_set_id
				AND t207.c207_tissue_fl  		IS NOT null
				AND t207.c207_void_fl 			IS NULL
				AND t505.c505_void_fl 			IS NULL
				AND t504.c504_consignment_id	=v_ref_id
		        GROUP BY t504.c504_consignment_id;
		 
		 EXCEPTION
			WHEN OTHERS
        	THEN
        	v_flag_count:=0;
        	
        END;
        
        IF(v_flag_count=0) THEN
        
	    	BEGIN
	  /*
	  Below Code is added to check if the transaction contains character like GM-CN. This is added to avoid the below select statement
	  which will result in invalid error	  
	  */  	
			SELECT  COUNT (1) INTO  v_txn_type_count FROM DUAL WHERE REGEXP_LIKE(v_ref_id, '[A-Za-z]');
		
			IF(v_txn_type_count=1) THEN
		
				RETURN;
		
			END IF;
		
    				
		  SELECT COUNT (1)
		      INTO v_flag_count
		   FROM t526_product_request_detail t526, t525_product_request t525, t207_set_master t207
	   	   WHERE t526.c525_product_request_id        = t525.c525_product_request_id
		    AND t525.c525_void_fl                  IS NULL
		    AND t526.c526_void_fl                  IS NULL
		    AND t526.c207_set_id                    = t207.c207_set_id
		    AND t207.c207_tissue_fl                IS NOT NULL
		    AND T207.C207_VOID_FL                  IS NULL
		    AND t526.c526_product_request_detail_id = v_ref_id;
		    
	-- Group by not needed since we are checking against the Product request detail id
		    
		EXCEPTION
			WHEN NO_DATA_FOUND
        	THEN
        		v_flag_count := 0;		
    	END;
    	
   	     END IF;
     
     
    
	ELSIF v_source = 50180 -- Order
	
	THEN
		BEGIN
		-- Distinct and Group by used to get a single record.
			SELECT count(DISTINCT t501.c501_order_id) 
				INTO v_flag_count 
				FROM t501_order t501, 
				t502_item_order t502,  
				v205_parts_with_ext_vendor v205p
			WHERE t501.c501_order_id			=	t502.c501_order_id
				AND t501.c501_void_fl 			IS NULL
				AND t502.c502_void_fl 			IS NULL
				AND t502.c205_part_number_id	=	v205p.c205_part_number_id
				AND t501.c501_order_id			=	v_ref_id
				AND t502.c502_delete_fl 		IS NULL
				GROUP BY t501.c501_order_id;
				
		EXCEPTION
			WHEN NO_DATA_FOUND
        	THEN
        		v_flag_count := 0;		
    	END;
    	
	ELSIF v_source = 50183 -- Loaner Extn
	
	THEN
		BEGIN 
		
		-- Distinct and Group by used to get a single record.
		
			SELECT COUNT( DISTINCT t412.c412_inhouse_trans_id) 
				INTO v_flag_count
				FROM t412_inhouse_transactions t412, 
				t413_inhouse_trans_items t413, 
				v205_parts_with_ext_vendor v205p
			WHERE t412.c412_inhouse_trans_id		=	t413.c412_inhouse_trans_id
					AND T413.c205_part_number_id 	=	 v205p.c205_part_number_id
					AND t412.c412_ref_id 			= 	v_ref_id
					AND t412.c412_void_fl 			IS NULL
					AND t413.c413_void_fl 			IS NULL
					AND t412.c1900_company_id		=	v_company_id
					GROUP BY t412.c412_inhouse_trans_id;
			EXCEPTION
				WHEN NO_DATA_FOUND
        		THEN
        		v_flag_count := 0;		
    		END;
    END IF;
    
    	IF v_flag_count = 1
    THEN
    	p_tissue_flag := 'Y';
    ELSE
    	p_tissue_flag := 'N';
    END IF;
    
  END gm_fch_tissue_flag_by_id;
  
  /******************************************************************
   * Description : Procedure to fetch tissue flag 
 ****************************************************************/	
  
  PROCEDURE gm_fch_tissue_fl_frm_device(
	p_refid 		IN		 t907_shipping_info.c907_ref_id%TYPE,
	p_source		IN		 t907_shipping_info.c901_source%TYPE
)
AS
	v_ship_id    t907_shipping_info.c907_shipping_id%TYPE;
	v_company_tissue_fl    VARCHAR2(1);
	v_ship_to		t907_shipping_info.c901_ship_to%TYPE;
	v_addr_type     t106_address.c901_address_type%TYPE;
	v_tissue_flag   t207_set_master.c207_tissue_fl%TYPE;
BEGIN

	SELECT  t907.c907_shipping_id, c901_ship_to, c901_address_type
		INTO v_ship_id, v_ship_to,v_addr_type
		FROM t907_shipping_info t907, t106_address t106
	WHERE t907.c907_ref_id = p_refid
		AND t907.c901_source = p_source
		AND c907_void_fl IS NULL
		AND c907_status_fl < 40
		AND  t907.c106_address_id = t106.c106_address_id
		AND t106.c106_void_fl   IS NULL;
	
		--get tissue flag
		gm_fch_tissue_flag_by_id(v_ship_id, v_tissue_flag);
		
		IF v_ship_to <> 26240675 THEN 
			IF v_tissue_flag = 'Y' AND v_ship_to = '4121' AND v_addr_type <> '26240675' THEN   --(4121-Sales Rep,26240675-hospital)
				GM_RAISE_APPLICATION_ERROR('-20999','450','');
			END IF;
		END IF;

END gm_fch_tissue_fl_frm_device;

END gm_pkg_op_shipping_validate;
/