/* Formatted on 2012/03/21 12:39 (Formatter Plus v4.8.0) */
--@"C:\Database\Packages\Operations\shipping\gm_pkg_scan_cntrl_item_rpt.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_scan_cntrl_item_rpt
IS
/******************************************************************
   * Description : Procedure is used to fetch the item consignment information 
	 * Also this method is only used for device change. Cannot be used anywhere else.
   ****************************************************************/
	PROCEDURE gm_fch_consign_item (
		p_txnid 		IN		 VARCHAR2
	  , p_txninfocur	OUT 	 TYPES.cursor_type
	)
	AS
		v_plant_id    t5040_plant_master.c5040_plant_id%TYPE;
	BEGIN
  		SELECT  NVL(get_plantid_frm_cntx(),get_rule_value('DEFAULT_PLANT','ONEPORTAL')) INTO  v_plant_id FROM DUAL;

		OPEN p_txninfocur
        FOR
          SELECT t505.c205_part_number_id pnum, get_partnum_desc (t505.c205_part_number_id) pdesc
		       , SUM (NVL (t505.c505_item_qty, 0)) iqty, 'NOC#' cnum ,gm_pkg_pd_rpt_udi.get_part_di_number(t505.c205_part_number_id) dinum
		    FROM t504_consignment t504, t505_item_consignment t505
		   WHERE t504.c504_consignment_id = p_txnid
		     AND t504.c504_consignment_id = t505.c504_consignment_id
		     AND t505.c505_void_fl IS NULL
		     AND t504.c5040_plant_id= v_plant_id
		GROUP BY t505.c205_part_number_id
		ORDER BY t505.c205_part_number_id;
	END gm_fch_consign_item;
/******************************************************************
   * Description : Procedure is used to fetch the ORDER ITEM information 
	 * Also this method is only used for device change. Cannot be used anywhere else.
   ****************************************************************/
	PROCEDURE gm_fch_order_items (
		p_txnid 		IN		 VARCHAR2
	  , p_txninfocur	OUT 	 TYPES.cursor_type
	)
	AS
		v_plant_id    t5040_plant_master.c5040_plant_id%TYPE;
	BEGIN
  		SELECT  NVL(get_plantid_frm_cntx(),get_rule_value('DEFAULT_PLANT','ONEPORTAL')) INTO  v_plant_id FROM DUAL;

		OPEN p_txninfocur
        FOR
        SELECT   t502.c205_part_number_id ID
                     , get_partnum_desc (t502.c205_part_number_id) pdesc
                    , SUM (t502.c502_item_qty) pickqty
                 , 'NOC#' cnum ,  gm_pkg_pd_rpt_udi.get_part_di_number(t502.c205_part_number_id) dinum
              FROM t502_item_order t502, t501_order t501
             WHERE t501.c501_order_id = p_txnid
               AND t501.c501_order_id = t502.c501_order_id
               AND t502.c502_void_fl IS NULL
               AND t501.c5040_plant_id= v_plant_id
               AND NVL (c901_type, 99999) != 50301
               AND (t501.c901_ext_country_id IS NULL
               OR  t501.c901_ext_country_id  in (select country_id from v901_country_codes))                  
          GROUP BY t502.c205_part_number_id
          ORDER BY t502.c205_part_number_id;
  
	END gm_fch_order_items;
/******************************************************************
   * Description : Procedure is used to fetch the LOANER ITEM information 
	 * Also this method is only used for device change. Cannot be used anywhere else.
   ****************************************************************/
	PROCEDURE gm_fch_loaned_item (
		p_txnid 		IN		 VARCHAR2
	  , p_txninfocur	OUT 	 TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_txninfocur
        FOR
          SELECT   c205_part_number_id pnum, get_partnum_desc (c205_part_number_id) pdesc
          		, SUM (c413_item_qty) iqty, 'NOC#' cnum , gm_pkg_pd_rpt_udi.get_part_di_number(c205_part_number_id) dinum
		    FROM t413_inhouse_trans_items
		   WHERE c412_inhouse_trans_id = p_txnid 
		     AND c413_void_fl IS NULL
		GROUP BY c205_part_number_id
		ORDER BY c205_part_number_id;

	END gm_fch_loaned_item;

END gm_pkg_scan_cntrl_item_rpt;
/

