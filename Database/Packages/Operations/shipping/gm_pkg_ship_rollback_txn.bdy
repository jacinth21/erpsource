--@"c:\database\packages\operations\shipping\gm_pkg_ship_rollback_txn.bdy";
/*
 *  This package is used only for BBA, and only for order and consignment
 */
 
CREATE OR REPLACE
PACKAGE BODY gm_pkg_ship_rollback_txn
IS

	/*****************************************************************************************************************
	    * Description : This Procedure is to rollback the transaction from Pending shipping to Pending control number
	    * Author      : Arajan
	*******************************************************************************************************************/
	PROCEDURE gm_rollback_txn (
		  p_txn_id	IN 	t907_shipping_info.c907_ref_id%TYPE
		, p_user_id	IN 	t907_shipping_info.c907_last_updated_by%TYPE
	)
	AS
		v_source	t907_shipping_info.c901_source%TYPE;
	BEGIN
		-- Get the source from transaction id
		BEGIN
			SELECT c901_source 
			  INTO v_source
			  FROM t907_shipping_info
			 WHERE c907_ref_id = p_txn_id
			   AND c907_void_fl IS NULL;
		EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			v_source := NULL;
			RETURN;
		END;
		
		/*
		 * If the source is 50180, then call the procedure to roll back the order
		 * If the source is 50181,26240435(stock transfer) then call the procedure to roll back the consignment 
		 */
		CASE v_source
		    WHEN '50180' 
		    	THEN gm_pkg_ship_rollback_txn.gm_rollback_order_txn(p_txn_id, v_source, p_user_id);
		    WHEN '50181' 
		    	THEN gm_pkg_ship_rollback_txn.gm_rollback_consignment(p_txn_id, v_source, p_user_id);
		    WHEN '26240435' 
		    	THEN gm_pkg_ship_rollback_txn.gm_rollback_consignment(p_txn_id, v_source, p_user_id);
		    ELSE RETURN;
		  END CASE;
		
		 
	END gm_rollback_txn;
	
	/*****************************************************************************************************************
	    * Description : This Procedure is to rollback the order from Pending shipping to Pending control number
	    * Author      : Arajan
	*******************************************************************************************************************/
	PROCEDURE gm_rollback_order_txn (
		  p_txn_id	IN 	t907_shipping_info.c907_ref_id%TYPE
		, p_source	IN	t907_shipping_info.c901_source%TYPE
		, p_user_id	IN 	t907_shipping_info.c907_last_updated_by%TYPE
	)
	AS
		v_out_cur  TYPES.cursor_type;
	BEGIN
		
		UPDATE t501_order
		   SET c501_status_fl = 1
			 , c501_last_updated_by = p_user_id
			 , c501_last_updated_date = CURRENT_DATE
		 WHERE c501_order_id = p_txn_id;
		 
		 -- Call the below procedure to update the shipping table
		gm_pkg_ship_rollback_txn.gm_sav_rollback_frm_shipping(p_txn_id, p_user_id, p_source);
		
		-- Fetch the trasaction details of order
		gm_pkg_ship_rollback_txn.gm_fch_order_txn_details(p_txn_id, v_out_cur);
		-- Update the lot status to null from controlled
		gm_pkg_ship_rollback_txn.gm_sav_rollback_lot_status(v_out_cur,p_user_id);
		
		--pc-5499-Part number not listing while control manually moved backorder
		   UPDATE t502_item_order 
		   SET c502_control_number = NULL
		   WHERE c501_order_id = p_txn_id 
		   AND c901_TYPE = 50300
		   AND c502_void_fl IS NULL;
		
	END gm_rollback_order_txn;
	
	/*****************************************************************************************************************
	    * Description : This Procedure is to rollback the consignment from Pending shipping to Pending control number
	    * Author      : Arajan
	*******************************************************************************************************************/
	PROCEDURE gm_rollback_consignment (
		  p_txn_id	IN 	t907_shipping_info.c907_ref_id%TYPE
		, p_source	IN	t907_shipping_info.c901_source%TYPE
		, p_user_id	IN 	t907_shipping_info.c907_last_updated_by%TYPE
	)
	AS
		v_set_id	    t520_request.c207_set_id%TYPE;
		v_req_id		t504_consignment.c520_request_id%TYPE;
		v_out_cur  		TYPES.cursor_type;
	BEGIN
		
		BEGIN
			SELECT c520_request_id
				INTO v_req_id
			FROM t504_consignment 
	 		WHERE c504_consignment_id = p_txn_id
	 			AND c504_void_fl IS NULL;
	 	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN;
		END;
		
		gm_pkg_common_cancel.gm_status_rollback(v_req_id,p_user_id);
		
		-- Fetch the trasaction details of consignment
		gm_pkg_ship_rollback_txn.gm_fch_consign_txn_details(p_txn_id, v_out_cur);
		-- Update the lot status to null from controlled
		gm_pkg_ship_rollback_txn.gm_sav_rollback_lot_status(v_out_cur,p_user_id);
		
	END gm_rollback_consignment;
	
	/*****************************************************************************************
		* Description : Procedure to update shipping table to pending control number status
		* Author      : Arajan
	*****************************************************************************************/
	PROCEDURE gm_sav_rollback_frm_shipping (
		p_txn_id	IN	 t907_cancel_log.c907_ref_id%TYPE
	  , p_user_id	IN	 t102_user_login.c102_user_login_id%TYPE
	  , p_source	IN	 t907_shipping_info.c901_source%TYPE
	)
	AS
	BEGIN
		
		UPDATE t907_shipping_info
		   SET c907_status_fl = 15			 
			 , c907_last_updated_by = p_user_id
			 , c907_last_updated_date = CURRENT_DATE
		 WHERE c907_ref_id = p_txn_id 
		 	AND c901_source = p_source;

	END gm_sav_rollback_frm_shipping;
	
	/*******************************************************************
	* Description : Procedure to fetch the details of order transaction
	* Author      : Arajan
	* *****************************************************************/
	PROCEDURE gm_fch_order_txn_details(
	   p_trans_id      IN   	t205c_part_qty.c205_last_update_trans_id%TYPE
	  , p_out_cur 		OUT 	TYPES.cursor_type
	)
	AS
	BEGIN
		
	  OPEN p_out_cur FOR   
	    SELECT c205_part_number_id PNUM,
		  c502_control_number QTY
		FROM t502_item_order
		WHERE c501_order_id = p_trans_id
		AND c502_void_fl   IS NULL;
		
	END gm_fch_order_txn_details;
	
	/**************************************************************************
	* Description : Procedure to fetch the details of consignment transaction
	* Author      : Arajan
	* ************************************************************************/
	PROCEDURE gm_fch_consign_txn_details(
	   p_trans_id      IN   	t205c_part_qty.c205_last_update_trans_id%TYPE
	  , p_out_cur 		OUT 	TYPES.cursor_type
	)
	AS
	BEGIN
		
	  OPEN p_out_cur FOR   
	    SELECT c205_part_number_id PNUM,
		  c505_control_number QTY
		FROM t505_item_consignment
		WHERE c504_consignment_id = p_trans_id
		and c505_void_fl         IS NULL;
		
	END gm_fch_consign_txn_details;
	
	
	/**************************************************************************
	* Description : Procedure to fetch the details of In-House transaction
	* Author      : HReddi
	* ************************************************************************/
	PROCEDURE gm_fch_inhouse_txn_details(
	    p_trans_id      IN   	t205c_part_qty.c205_last_update_trans_id%TYPE
	  , p_out_cur 		OUT 	TYPES.cursor_type
	)
	AS
		v_trans_count  NUMBER;
	BEGIN
		
	  SELECT COUNT(1) 
	    INTO v_trans_count 
	    FROM t412_inhouse_transactions 
	   WHERE c412_inhouse_trans_id = p_trans_id
	     AND c412_void_fl IS NULL ;

	  IF v_trans_count > 0 THEN
	  
	  	OPEN p_out_cur FOR
		    SELECT c205_part_number_id PNUM,
			       c413_control_number CNUM
			  FROM t413_inhouse_trans_items
			 WHERE c412_inhouse_trans_id = p_trans_id
			   AND c413_void_fl         IS NULL;	
	  ELSE
	  	OPEN p_out_cur FOR
		  	SELECT c205_part_number_id PNUM,
				   c505_control_number QTY
			  FROM t505_item_consignment
		      WHERE c504_consignment_id = p_trans_id
			    AND c505_void_fl         IS NULL;
	  
	  END IF;
		
	END gm_fch_inhouse_txn_details;
	
	/**************************************************************************
	* Description : Procedure to fetch the details of Returns transaction
	* Author      : HReddi
	* ************************************************************************/
	PROCEDURE gm_fch_return_details(
	    p_trans_id      IN   	t205c_part_qty.c205_last_update_trans_id%TYPE
	  , p_out_cur 		OUT 	TYPES.cursor_type
	)
	AS
		v_trans_count  NUMBER;
	BEGIN
		OPEN p_out_cur FOR
		  	SELECT c205_part_number_id PNUM,
				   c507_control_number QTY
			  FROM t507_returns_item
		      WHERE c506_rma_id = p_trans_id
                AND c507_status_fl = 'R';
		
	END gm_fch_return_details;
	
	/*************************************************************************************
	* Description : Procedure to fetch the details of order attribute transaction details
	* Author      : HREDDI
	* ************************************************************************************/
	PROCEDURE gm_fch_order_attr_txn_details(
	    p_trans_id      IN   	t205c_part_qty.c205_last_update_trans_id%TYPE
	  , p_out_cur 		OUT 	TYPES.cursor_type
	)
	AS
	BEGIN
	
	  OPEN p_out_cur FOR
	  	 SELECT t502a.c205_part_number_id PARTNUM, t502a.c502a_attribute_value CNUM,  t501.c901_order_type ORDTYPE , t501.c501_order_id ORDERID
			   FROM t502a_item_order_attribute t502a, t501_order t501, t2550_part_control_number t2550
			  WHERE t501.c501_order_id                    = t502a.c501_order_id		
			    AND t501.c501_order_id                    = p_trans_id			   
			    AND t502a.c502a_control_number_update_fl IS NULL
			    AND t502a.c901_attribute_type             = 101723 -- Control Number Attribute			   
			    AND t2550.c205_part_number_id  = t502a.c205_part_number_id
			    AND t2550.c2550_control_number = t502a.c502a_attribute_value
			    AND t2550.c1900_company_id     = t501.c1900_company_id
			    AND t2550.c5040_plant_id       = t501.c5040_plant_id
			    AND t2550.c2550_lot_status IN('105006','105000')--sold and implanted
			    AND t502a.c502a_void_fl       IS NULL
			    AND t501.c501_void_fl         IS NULL;	
	END gm_fch_order_attr_txn_details;
	
	/*****************************************************************************************
		* Description : Procedure to update shipping table to pending control number status
		* Author      : Arajan
	*****************************************************************************************/
	PROCEDURE gm_sav_rollback_lot_status (
		p_txn_cur	IN	 TYPES.cursor_type
	  , p_user_id	IN	 t102_user_login.c102_user_login_id%TYPE
	)
	AS
		v_pnum		t2550_part_control_number.c205_part_number_id%TYPE;
		v_control	t2550_part_control_number.c2550_control_number%TYPE;
	BEGIN
		
		LOOP	
		    FETCH p_txn_cur INTO v_pnum, v_control;
		    EXIT
		    WHEN p_txn_cur%NOTFOUND;
			
			UPDATE t2550_part_control_number
			SET C901_LOT_CONTROLLED_STATUS = NULL
			  , c2550_last_updated_by = p_user_id
			  , c2550_last_updated_date = CURRENT_DATE
			WHERE c205_part_number_id = v_pnum
				AND c2550_control_number = v_control
				AND C901_LOT_CONTROLLED_STATUS = '105008';--Controlled
		END LOOP;
		
		IF p_txn_cur%ISOPEN THEN
		  	 CLOSE p_txn_cur;
		END IF;
		
		EXCEPTION WHEN OTHERS THEN
		
		IF p_txn_cur%ISOPEN THEN
		  	 CLOSE p_txn_cur;
		END IF;
		
		GM_RAISE_APPLICATION_ERROR('-20999','305','');

	END gm_sav_rollback_lot_status;
	
	/************************************************************************************************
		* Description : Procedure to UPDATE the LOT Status from SOLD to NULL after voiding the Order
		* Author      : Hari
	*************************************************************************************************/
	PROCEDURE gm_sav_rb_ord_lot_status (
		p_txn_cur	IN	 TYPES.cursor_type
	  , p_user_id	IN	 t102_user_login.c102_user_login_id%TYPE
	)
	AS
		v_pnum			t2550_part_control_number.c205_part_number_id%TYPE;
		v_control		t2550_part_control_number.c2550_control_number%TYPE;
		v_txn_type  	t501_order.c901_order_type%TYPE;
		v_order_id  	t501_order.c501_order_id%TYPE;
		v_plant_id     	t5040_plant_master.c5040_plant_id%TYPE;      
        v_company_id	t1900_company.c1900_company_id%TYPE;
        v_inv_id		t5060_control_number_inv.c5060_control_number_inv_id%TYPE;
	BEGIN
	   --validating lot tracking is available for company 
	   SELECT  NVL(get_plantid_frm_cntx(),get_rule_value('DEFAULT_PLANT','ONEPORTAL'))
		     , NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL'))
		 INTO  v_plant_id, v_company_id 
		 FROM DUAL;

		LOOP	
		    FETCH p_txn_cur INTO v_pnum, v_control, v_txn_type , v_order_id;
		    EXIT
		    WHEN p_txn_cur%NOTFOUND;
			
			  UPDATE t2550_part_control_number
			    SET c2550_lot_status = NULL, 
			    c2550_last_updated_trans_id = v_order_id,
			    c2550_last_updated_date = CURRENT_DATE,
			    c2550_last_updated_by = p_user_id
			  WHERE c205_part_number_id = v_pnum
			    AND c2550_control_number = v_control
			    AND c2550_lot_status IN('105006','105000') --sold and impanted
			    and c1900_company_id = v_company_id
			    and c5040_plant_id = v_plant_id;	        
	        
	        SELECT MAX (t5060.c5060_control_number_inv_id) INTO v_inv_id
			   FROM t5060_control_number_inv t5060
			  WHERE t5060.c901_warehouse_type IN (56002, 4000339)--Account and FS warehouse
			    AND t5060.c205_part_number_id  = v_pnum
			    AND t5060.c5060_control_number = v_control
			    AND c5060_qty                  = 0
			    AND c1900_company_id           = v_company_id
			    AND c5040_plant_id             = v_plant_id;
			    
		 	IF v_inv_id IS NOT NULL THEN 
	       -- increasing lot qty in FS or account warehouse
			   UPDATE t5060_control_number_inv t5060
				  SET c5060_qty                             = 1
				    , c5060_last_update_trans_id = v_order_id
				    , c901_last_transaction_type = v_txn_type
			        , c5060_last_updated_by                 = p_user_id
			        , c5060_last_updated_date = CURRENT_DATE
			    WHERE t5060.c205_part_number_id         = v_pnum
			      AND t5060.c5060_control_number        = v_control
			      AND t5060.c5060_control_number_inv_id = v_inv_id;
		    END IF;			    
		END LOOP;	

	END gm_sav_rb_ord_lot_status;
	
END gm_pkg_ship_rollback_txn;
/