/* Formatted on 2012/03/21 12:39 (Formatter Plus v4.8.0) */
--@"C:\Database\Packages\Operations\shipping\gm_pkg_ship_scan_trans.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_ship_scan_trans
IS
/******************************************************************
   * Description : Procedure to save the tote and station id
   ****************************************************************/
	PROCEDURE gm_sav_ship_station (
		p_refid 		IN		 t907_shipping_info.c907_ref_id%TYPE
	  , p_source		IN		 t907_shipping_info.c901_source%TYPE
	  , p_shiptoteid	IN		 t907_shipping_info.c907_ship_tote_id%TYPE
	  , p_shipstationid IN		t907_shipping_info.c907_pack_station_id%TYPE
	  , p_userid   		IN		t907_shipping_info.c907_last_updated_by%TYPE
	)
	AS
	v_status_fl    t907_shipping_info.c907_status_fl%TYPE;
	v_shipid	   VARCHAR2 (100);
	v_hardcopy 	   t501_order.c501_hard_po_fl%TYPE;
	v_source 		t907_shipping_info.c901_source%TYPE;
	v_plant_id      t5040_plant_master.c5040_plant_id%TYPE;
	v_txn_id 		t412_inhouse_transactions.c412_inhouse_trans_id%TYPE;
	v_company_tissue_fl    VARCHAR2(1);
	v_ship_to		t907_shipping_info.c901_ship_to%TYPE;
	v_addr_type     t106_address.c901_address_type%TYPE;
	v_company_id   t1900_company.c1900_company_id%TYPE;
	BEGIN

	    SELECT  NVL(get_plantid_frm_cntx(),get_rule_value('DEFAULT_PLANT','ONEPORTAL')),NVL (get_compid_frm_cntx (), get_rule_value ('DEFAULT_COMPANY', 'ONEPORTAL'))
	    	INTO  v_plant_id,v_company_id 
	    FROM DUAL;
		
	    SELECT get_associate_txn_id(p_refid) 
			INTO v_txn_id 
		   FROM DUAL;
		   
		   v_company_tissue_fl := NVL(get_rule_value(v_company_id,'TISSUE_VALIDATION'),'N');
		   
		   IF v_company_tissue_fl = 'Y' THEN
				SELECT t106.c901_address_type,t907.c901_ship_to
			     INTO v_addr_type,v_ship_to
			   FROM t907_shipping_info t907, t106_address t106
			  WHERE t907.c106_address_id = t106.c106_address_id
			    AND t907.c907_void_fl   IS NULL
			    AND t106.c106_void_fl   IS NULL
			    AND t907.c907_ref_id = p_refid;
			    
			    IF v_source in ('50180','50181','50182','50183') AND v_company_tissue_fl = 'Y' AND v_ship_to <> '4122' THEN   --(4122-hospital,50180-Orders,50181-Consignments,50182-Loaners,50183-Loaner Extn)
			    		IF v_ship_to = '4121' AND v_addr_type <> '26240675' THEN       --(4121-Sales Rep,26240675-hospital)
			    			GM_RAISE_APPLICATION_ERROR('-20999','450','');
			    		END IF;
			    END IF;
		   END IF;
		   
		   IF v_txn_id IS NOT NULL AND v_txn_id != ' ' THEN
		   	 raise_application_error('-20999','Transactions is on hold due to associated transaction '|| v_txn_id || ' is Open.');
		   END IF;
		   
		v_shipid	:= gm_pkg_cm_shipping_info.get_shipping_id (p_refid, p_source, '');		
			
		--check c901_temp_source , if it's not null that means consignment set in place of loaner set.
		SELECT c907_status_fl, NVL(c901_temp_source, c901_source)
		  INTO v_status_fl, v_source
		  FROM t907_shipping_info
		 WHERE c907_shipping_id = v_shipid AND c907_void_fl IS NULL AND c5040_plant_id= v_plant_id
		   FOR UPDATE;
		   
		--check one source of the ship_tote_id, if it is different then in parameter(source) throw error.
		-- 2nd check, scan the station and tote and then check the source.
		gm_validate_station_tote(v_source, p_shiptoteid, p_shipstationid,p_refid);
	 
		UPDATE t907_shipping_info
		   SET c907_ship_tote_id = p_shiptoteid
		     , c907_status_fl = '33' -- Packing in progress
			 , c907_pack_station_id = p_shipstationid
			 , c907_last_updated_by = p_userid
			 , c907_last_updated_date = SYSDATE
		 WHERE c907_shipping_id = v_shipid 
		   AND c907_void_fl IS NULL
		   AND c907_status_fl IN ('30','33');
		 
		   IF v_status_fl = '30' 
		   THEN
		 		--below procedure is used to update the status for T501,T504 and t412.
		 		gm_upd_trans_status(p_refid,p_source,'33',p_userid);
		   END IF;
	END gm_sav_ship_station;

	/******************************************************************
   * Description : Procedure to validate the tote and station id
   ****************************************************************/
	PROCEDURE gm_validate_station_tote (
		p_source		IN		 t907_shipping_info.c901_source%TYPE
	  , p_shiptoteid	IN		 t907_shipping_info.c907_ship_tote_id%TYPE
	  , p_shipstationid IN		 t907_shipping_info.c907_pack_station_id%TYPE
 	  , p_ref_id		IN		 t907_shipping_info.c907_ref_id%TYPE 
	)
	AS
	v_source 		t907_shipping_info.c901_source%TYPE;
	v_shiptote 		t907_shipping_info.c907_ship_tote_id%TYPE;
	v_shiptoid		t907_shipping_info.c907_ship_to_id%TYPE;
	v_ship_toid		t907_shipping_info.c907_ship_to_id%TYPE;
	v_shipto		t907_shipping_info.c901_ship_to%TYPE;
	v_ship_to		t907_shipping_info.c901_ship_to%TYPE;
	v_address_id	t907_shipping_info.c106_address_id%TYPE;
	v_addressid		t907_shipping_info.c106_address_id%TYPE;
	v_count			NUMBER;
	v_ref_id        t907_shipping_info.c907_ref_id%TYPE;
	v_old_chk_set_fl   		VARCHAR2(10);
	v_new_chk_set_fl   		VARCHAR2(10);
	v_status_fl				VARCHAR2(10);
	v_pack_station_id		t907_shipping_info.c907_pack_station_id%TYPE;
	v_attn_to  				t907_shipping_info.c907_override_attn_to%TYPE;
	v_attnto  				t907_shipping_info.c907_override_attn_to%TYPE;
	BEGIN 
		
			/**
			 * Below query is used to validate 3 scenarios
			 * 1. We can't put different source in same tote
			 * 2. If tote is completed should not allow to add sets into the completed tote
			 * 3. We can't place one tote in two stations
			 * 4. We can't assign same Tote/Station for different Override Attention To names
        	**/
		BEGIN
            SELECT  NVL(t907.c901_temp_source, t907.c901_source), t907.c907_status_fl, t907.c907_pack_station_id 
                  INTO  v_source, v_status_fl, v_pack_station_id                    
            FROM  t907_shipping_info t907
            WHERE t907.c907_void_fl IS NULL
                  AND  t907.c907_pack_station_id IS NOT NULL
                  AND  t907.c907_ship_tote_id IS NOT NULL
                  AND  t907.c907_status_fl < '40'
                  AND  t907.c907_ship_tote_id = p_shiptoteid
                  AND ROWNUM =1;
            IF v_source <> p_source   --We can't put different source in same tote
            THEN
                  GM_RAISE_APPLICATION_ERROR('-20999','310','');
            END IF;
            IF v_status_fl = '36'    --If tote is completed should not allow to add sets into the completed tote
            THEN
                  GM_RAISE_APPLICATION_ERROR('-20999','311','');
            END IF;
            IF v_pack_station_id <> p_shipstationid --We can't place one tote in two stations
            THEN
                  GM_RAISE_APPLICATION_ERROR('-20999','312','');
            END IF;
        	EXCEPTION WHEN NO_DATA_FOUND
            THEN
                  v_shiptote := '';    
			END;

		
		BEGIN
			SELECT 	t907.c907_ship_tote_id,t907.c901_ship_to,t907.c907_ship_to_id,DECODE(c106_address_id, 0,-999, NULL,-999, c106_address_id), t907.c907_ref_id ,t907.c907_override_attn_to 
			  INTO  v_shiptote,v_ship_to,v_ship_toid,v_address_id ,v_ref_id ,v_attnto
			  FROM  t907_shipping_info t907
			 WHERE 	t907.c907_pack_station_id = p_shipstationid
			   AND  t907.c907_void_fl IS NULL
			   AND  t907.c907_pack_station_id IS NOT NULL
			   AND  t907.c907_ship_tote_id IS NOT NULL
			   AND  t907.c907_status_fl < '36'
			   AND  ROWNUM = 1; --ROWNUM should be removed provided one station id is mapped to only one ship toteid
			IF v_shiptote <>  p_shiptoteid 
			THEN
				GM_RAISE_APPLICATION_ERROR('-20999','312',''); 
			END IF;	
		EXCEPTION WHEN NO_DATA_FOUND
		THEN
			v_shiptote := ''; 
		END;
		
		-- (NVL(v_attnto,-999) <>  NVL(v_attn_to,-999) AND v_attnto IS NOT NULL) is added for validating the while asigning the same Tote/Station for different Override Attention to names.
		-- NVL is added for to check whether override value is NULL or not,IF v_attnto is NOT NULL only we are checking the above condition
		-- if Override Attention To is Null for One order/transaction and second one is NOT NULL, this scenario also we are throwing validation
		BEGIN
			SELECT 	t907.c901_ship_to ,t907.c907_ship_to_id, DECODE(c106_address_id, 0,-999, NULL,-999, c106_address_id),t907.c907_override_attn_to
			  INTO  v_shipto,v_shiptoid,v_addressid ,v_attn_to
			  FROM  t907_shipping_info t907
			 WHERE 	t907.c901_source = p_source
			 	AND t907.c907_ref_id = p_ref_id 		
			   AND  t907.c907_void_fl IS NULL
			   AND  t907.c907_status_fl < '40'
			   AND  ROWNUM = 1; --ROWNUM Should be one transcation id pending shipping in one ship tot id.
			-- No need to include "Attention To" for grouping the transaction, we can use the location even if the attention to is different
			IF v_ship_to <> v_shipto OR v_ship_toid  <> v_shiptoid OR v_address_id <> v_addressid --OR (NVL(v_attnto,-999) <>  NVL(v_attn_to,-999) AND v_attnto IS NOT NULL) 
			THEN
				GM_RAISE_APPLICATION_ERROR('-20999','306','');
			END IF;
		EXCEPTION WHEN NO_DATA_FOUND
		THEN
			v_shiptote := '';
		END;
		
	 /*IF both Set Consignments are come to scan means, we can process these two.
	  IF both Item Consignments are come to scan means, we can process these two.
	  IF One Set Consignment and one Item Consignment are come to scan means, 
	  	we are throwing app error by adding the highlighted code to existing condition.*/
	   IF v_ref_id IS NOT NULL AND p_ref_id IS NOT NULL  THEN
	  		SELECT DECODE(p_source, 50181, gm_pkg_ship_scan_rpt.chk_set_item_cn(v_ref_id), NULL)
	       		 , DECODE(p_source, 50181, gm_pkg_ship_scan_rpt.chk_set_item_cn(p_ref_id), NULL) 
	  		  INTO v_old_chk_set_fl , v_new_chk_set_fl
	  		  FROM DUAL;
	  
	  		IF p_source = 50181 AND NVL(v_old_chk_set_fl,-999) <>  NVL(v_new_chk_set_fl,-999) THEN
				GM_RAISE_APPLICATION_ERROR('-20999','307','');
	  		END IF;
		END IF;
	END gm_validate_station_tote;
	
/******************************************************************
   * Description : Procedure to complete the transaction.
 ****************************************************************/
	PROCEDURE gm_sav_complete_tote (
		p_scanid 		IN		 VARCHAR2
	  , p_scantype		IN		 VARCHAR2
	  , p_refid 		IN		 t907_shipping_info.c907_ref_id%TYPE
	  , p_source		IN		 t907_shipping_info.c901_source%TYPE
	  , p_userid 		IN 	     VARCHAR2
	  , p_trackno	    IN		 t907_shipping_info.c907_tracking_number%TYPE
	  , p_return_trackno IN      t907_shipping_info.c907_return_tracking_no%TYPE
	)
	AS
		v_cnt		 NUMBER;		
		v_shippingid t907_shipping_info.c907_shipping_id%TYPE;
		v_del_mode   t907_shipping_info.c901_delivery_mode%TYPE;
		v_statusfl   t907_shipping_info.c907_status_fl%TYPE;
		v_trackno 	 t907_shipping_info.c907_tracking_number%TYPE;
		v_scan_id 	 VARCHAR2(50);
		v_lncncnt	 NUMBER;
		v_plant_id   t5040_plant_master.c5040_plant_id%TYPE;
		CURSOR shiptotetxns
		IS
		SELECT t907.c907_ship_tote_id shiptoteid
		  FROM t907_shipping_info t907
		 WHERE t907.c907_pack_station_id LIKE v_scan_id   
		   AND t907.c907_void_fl IS NULL
		   AND t907.c907_status_fl = '33';
	BEGIN
  		SELECT  NVL(get_plantid_frm_cntx(),get_rule_value('DEFAULT_PLANT','ONEPORTAL')) INTO  v_plant_id FROM DUAL;

		IF p_scantype = '102980' --TOTE
		THEN
			SELECT COUNT (1)
			  INTO v_cnt
    		  FROM t907_shipping_info
    		 WHERE c907_ship_tote_id = p_scanid
        	   AND c901_delivery_mode IN (select c906_rule_value 
                                			from t906_rules 
                                			where c906_rule_id = 'DELMODEEXCL' 
                                			and C906_RULE_GRP_ID = 'DELMODE') --Hand Delivered
        	   AND c907_void_fl IS NULL
        	   AND c907_status_fl = '33'
        	   AND c5040_plant_id= v_plant_id;
			
        	IF v_cnt > 0 THEN
        	  -- updating status flag '33'[Pick In Progress],for the transactions with Hand Delivered
        		gm_sav_track_status(p_scanid,p_trackno,'33',p_refid, p_userid, p_return_trackno);
        	  -- stright away shipout the Hand Deliverd transaction.
        		gm_sav_handdel_shipout(p_scanid,'33',p_refid, p_userid); 
        	ELSE
        	  -- updating status flag '36'[Ready For Pickup],for other than the Hand Delivered Transactions.
        		gm_sav_track_status(p_scanid,p_trackno,'36',p_refid, p_userid, p_return_trackno);
        		gm_sav_trans_status(p_scanid,102980,NULL,NULL,p_userid);
			END IF;

	    ELSIF p_scantype IN ('102983','102982') --STATIONROW/Station
	    THEN
	    	IF p_scanid IS NULL OR trim(p_scanid) = '' THEN
	    		GM_RAISE_APPLICATION_ERROR('-20999','308','');
	    	END IF;
	        IF p_scantype = '102982' -- STATION
		    THEN
		   		v_scan_id := p_scanid;
		    ELSIF p_scantype = '102983' -- STATIONROW
		    THEN
		    	v_scan_id := p_scanid || '%';
		    	SELECT COUNT (1)
				  INTO v_lncncnt
	    		  FROM t907_shipping_info
	    		 WHERE c907_pack_station_id LIKE v_scan_id
	        	   AND c907_void_fl IS NULL
	        	   AND c907_status_fl = '33'
	        	   AND c5040_plant_id= v_plant_id
	        	   AND c901_source NOT IN (50180,DECODE (c901_source, 50181, DECODE (get_trans_type (c907_ref_id),'ITEM','50181','-999'),'-999')); -- As of now only excluded order  but it can be other item transactions.
	        	IF v_lncncnt > 0 
			    THEN
			    	GM_RAISE_APPLICATION_ERROR('-20999','309','');
			    END IF;
		    END IF;	
	    	
	    	FOR v_rec IN shiptotetxns
	    	LOOP
	    		SELECT DECODE(p_scantype, '102983',v_rec.shiptoteid,p_trackno)
	    		  INTO v_trackno
	    		  FROM DUAL;
	    	    -- When station is scanned it has to pass the tracking no associated to the tote.
	    	    -- When station row is scanned it will not have p_trackno, so it will update tote id  as tracking no for loose items.
	    	    -- updating status flag '36'[Ready For Pickup],for other than the Hand Delivered Transactions.		  
				gm_sav_track_status(v_rec.shiptoteid,v_trackno,'36',p_refid, p_userid, p_return_trackno);
				gm_sav_trans_status(v_rec.shiptoteid,102980,NULL,NULL,p_userid);    
			END LOOP;
		ELSIF p_refid IS NOT NULL
		THEN
			SELECT c907_shipping_id, c901_delivery_mode, c907_status_fl
              INTO v_shippingid , v_del_mode, v_statusfl
              FROM t907_shipping_info
             WHERE c907_ref_id = p_refid
               AND c907_void_fl IS NULL
               AND c907_status_fl IN ('30','33')
               AND c5040_plant_id= v_plant_id;
        	   
        	IF v_del_mode = 5030 OR v_del_mode = 10304738 THEN
         -- updating status flag '33'[Pick In Progress],for the transactions with Hand Delivered
                gm_sav_track_status(null,p_trackno,v_statusfl,p_refid, p_userid, p_return_trackno);
         -- stright away shipout the Hand Deliverd transaction.
                gm_sav_handdel_shipout (NULL, v_statusfl, v_shippingid, p_userid);
            ELSE
         -- updating status flag '36'[Ready For Pickup],for other than the Hand Delivered Transactions.    
                gm_sav_track_status(null,p_trackno,'36',p_refid, p_userid, p_return_trackno);
            END IF;
		END IF;		
	END gm_sav_complete_tote;
/******************************************************************
   * Description : Procedure to save the new staus for the respective tables.
 ****************************************************************/	
PROCEDURE gm_sav_trans_status (
		p_scanid 		IN		 VARCHAR2
	  , p_scantype		IN		 VARCHAR2
	  , p_refid 		IN		 t907_shipping_info.c907_ref_id%TYPE
	  , p_source		IN		 t907_shipping_info.c901_source%TYPE
	  , p_userid 		IN 	     VARCHAR2  
	)
	AS
		CURSOR shiptotetxns
		IS	
		SELECT t907.c907_ref_id txnid, t907.c901_source source, t907.c907_status_fl	shipstatus           	   
		  FROM t907_shipping_info t907
		 WHERE t907.c907_ship_tote_id = p_scanid
		   AND t907.c907_void_fl IS NULL
		   AND t907.c907_ref_id IS NOT NULL
		   AND c907_status_fl = '36';
	BEGIN
		IF p_scantype = '102980' -- TOTE
		THEN
			FOR v_rec IN shiptotetxns
			LOOP
				gm_upd_trans_status(v_rec.txnid,v_rec.source, v_rec.shipstatus, p_userid);
			END LOOP;		
		END IF;		   
	END gm_sav_trans_status;
/******************************************************************
   * Description : Procedure to save the new staus for the respective tables.
   * For details of status refer below link
   * http://confluence.spineit.net/display/PRJ/8.3+High+Level+Analysis+%28NSP%29
 ****************************************************************/	
PROCEDURE gm_upd_trans_status (
		p_refid 		IN		 t907_shipping_info.c907_ref_id%TYPE
	  , p_source		IN		 t907_shipping_info.c901_source%TYPE
	  , p_shipstatus	IN		 t907_shipping_info.c907_status_fl%TYPE
	  , p_userid 		IN 	     VARCHAR2  
	)
	AS
	v_status varchar2(10);
	BEGIN
		
		SELECT DECODE(	p_source,50180, DECODE(p_shipstatus,'30','2','33','2.30','36','2.60',p_shipstatus)
								,50181, DECODE(p_shipstatus,'30','3','33','3.30','36','3.60',p_shipstatus)
								,50182, DECODE(p_shipstatus,'30','10','33','13'  ,'36','16'  ,p_shipstatus)
								,50186, DECODE(p_shipstatus,'30','3','33','3.30','36','3.60',p_shipstatus)								
					   ,p_shipstatus
					 )
		  INTO v_status		 
		  FROM DUAL;
		--Order status update
		IF p_source = 50180
		THEN
			UPDATE t501_order
			   SET c501_status_fl = v_status--'2.30'
				 , c501_last_updated_date = SYSDATE
				 , c501_last_updated_by = p_userid
			 WHERE c501_order_id = p_refid
			   AND c501_void_fl IS NULL;
		-- CN and IHLN status update
		ELSIF p_source IN (50181,50186)
		THEN
			UPDATE t504_consignment
			   SET c504_status_fl = v_status--'3.30'
				 , c504_last_updated_date = SYSDATE
				 , c504_last_updated_by = p_userid
			 WHERE c504_consignment_id = p_refid
			   AND c504_void_fl IS NULL;
			-- Loaner status update
		ELSIF p_source = 50182
		THEN
			-- Call the common procedure to update the status	
			gm_pkg_op_loaner_set_txn.gm_upd_consign_loaner_status(p_refid,v_status,p_userid);	
		END IF;   
	END gm_upd_trans_status;

/******************************************************************
   * Description : Procedure to save the new staus for the respective tables.
 ****************************************************************/	
PROCEDURE gm_sav_scan_shipout_trans (
		p_shipping_id 		IN		 VARCHAR2
	  , p_user_id           IN       VARCHAR2
	  , p_status_fl			IN       t907_shipping_info.c907_status_fl%TYPE
	  , p_auto_ship_fl		IN		 VARCHAR2 DEFAULT NULL
	)
	AS
		v_userid varchar2(20);  
		v_uptfl        char(1) :='N';
        v_company_id   t1900_company.c1900_company_id%TYPE;
		CURSOR shiptotetxns
		IS	
		SELECT t907.c907_ref_id refid, t907.c901_source source, t907.c907_status_fl	shipstatus
			 , t907.c901_ship_to shipto, t907.c907_ship_to_id shiptoid, t907.c901_delivery_carrier shipcarr
			 , t907.c901_delivery_mode shipmode, t907.c907_tracking_number trackno, t907.c106_address_id addressid
			 , t907.c907_frieght_amt freightamt, t907.c907_shipping_id shipid, t907.c907_override_attn_to attnto
			 , t907.c907_ship_instruction ship_instruc, t907.c907_pack_station_id pack_station, t907.c907_ship_tote_id shiptoteid
			 , '' updateaction, '' custponum, 'true' shipflag
			 , t907.c907_last_updated_by lastupdby
		  FROM t907_shipping_info t907
		 WHERE t907.c907_shipping_id = p_shipping_id
		   AND t907.c907_void_fl IS NULL
		   AND t907.c1900_company_id  = v_company_id
		   AND t907.c907_status_fl = p_status_fl; -- 36 used becuase if it is called from different place will be good./ p_status_fl is added for calling the same procuedure for new functionolity
	BEGIN
		 SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL'))
            INTO v_company_id
            FROM DUAL;
		--IF p_scantype = '102980' -- TOTE
		--THEN
			FOR v_rec IN shiptotetxns
			LOOP
				v_userid := v_rec.lastupdby;
				v_uptfl := GM_PKG_CM_SHIPPING_TRANS.get_user_security_event_status(v_userid,'SHIPOUT_ACCESS');			
				IF v_uptfl <> 'Y' THEN
					v_userid := '303201'; -- John Luner to be changed
				END IF;

				GM_PKG_CM_SHIPPING_TRANS.gm_sav_shipout( 	v_rec.refid
														  , v_rec.source
														  , v_rec.shipto
														  , v_rec.shiptoid
														  , v_rec.shipcarr
														  , v_rec.shipmode
														  , v_rec.trackno
														  , v_rec.addressid
														  , v_rec.freightamt
														  , v_userid	
														  , v_rec.updateaction
														  , v_rec.shipid
														  , v_rec.custponum 
														  , v_rec.shipflag	
														  , v_rec.attnto		
														  , v_rec.ship_instruc
														  , v_rec.pack_station
														  , v_rec.shiptoteid
														  , p_auto_ship_fl
														 );
			END LOOP;		
		--END IF;		   
	END gm_sav_scan_shipout_trans;
	

	/******************************************************************
	   * Description : Procedure to save printer mapping to respective user
	 ****************************************************************/
	PROCEDURE gm_sav_print_params(
	   p_regprn_nm 		IN   	VARCHAR2
     , p_zplprn_nm 		IN 		VARCHAR2
     , p_userid			IN		t906_rules.c906_created_by%TYPE
	)
	AS
		
	BEGIN
		--call below procedure to save the regular printer mapped with user
		gm_pkg_cm_rule_params.gm_sav_rule_params(p_userid,'PRINTER',p_regprn_nm,p_userid); --userid and rule id is same
		
		--call below procedure to save the ZPL printer mapped with user
		gm_pkg_cm_rule_params.gm_sav_rule_params(p_userid,'ZEBRA_PRINTER',p_zplprn_nm,p_userid);
		
	END gm_sav_print_params;
	
	/******************************************************************
     * Description : Procedure to save the tracking number and status.
     ****************************************************************/	
		PROCEDURE gm_sav_track_status (
				p_scanid	IN		 VARCHAR2
			  ,	p_trackno	IN 	     VARCHAR2
			  , p_status	IN 	     VARCHAR2
			  , p_refid 	IN		 t907_shipping_info.c907_ref_id%TYPE
			  , p_userid	IN 	     VARCHAR2	  
			  , p_return_trackno IN  t907_shipping_info.c907_return_tracking_no%TYPE
		)
		AS		
		BEGIN
			IF p_refid IS NOT NULL
			THEN
				UPDATE t907_shipping_info
				   SET c907_tracking_number = p_trackno
				     , c907_return_tracking_no = p_return_trackno
				     , c907_status_fl = p_status -- Ready to Pick
					 , c907_last_updated_by = p_userid
					 , c907_last_updated_date = SYSDATE
				 WHERE c907_ref_id = p_refid 
				   AND c907_void_fl IS NULL
				   AND c907_status_fl IN ('30','33');		
			ELSE
				UPDATE t907_shipping_info
				   SET c907_tracking_number = NVL(p_trackno,NVL(c907_tracking_number,p_scanid))
				     , c907_return_tracking_no = p_return_trackno
				   	 , c907_status_fl = p_status
				   	 , c907_last_updated_by = p_userid
					 , c907_last_updated_date = SYSDATE
				 WHERE c907_ship_tote_id = p_scanid 
				   AND c907_void_fl IS NULL
				   AND c907_status_fl = '33';
			END IF;	   
		END gm_sav_track_status;
		
	/***********************************************************************************
     * Description : Procedure to save the status to 40 for hand delivered transactions.
     ***********************************************************************************/	
		PROCEDURE gm_sav_handdel_shipout (
			p_scanid		IN		 VARCHAR2
		  , p_status		IN		 VARCHAR2
		  , p_shipping_id	IN		 VARCHAR2
		  , p_userid		IN		 VARCHAR2
		)	
		AS
				CURSOR shiptotetxns	IS
			       SELECT t907.c907_shipping_id shippingid
			           	, t907.c907_status_fl statusfl
			            , t907.c907_ref_id  transid          
			         FROM t907_shipping_info t907
			        WHERE t907.c907_status_fl 	= '33' 
			          AND t907.c907_void_fl 		is null
			          AND (t907.c907_ship_tote_id = p_scanid 
			           OR t907.c907_pack_station_id LIKE p_scanid || '%')  ;
		BEGIN
			IF p_shipping_id IS NOT NULL THEN
				gm_sav_scan_shipout_trans(p_shipping_id,p_userid,p_status);
			ELSE
				FOR v_rec IN shiptotetxns
				LOOP		
				-- Calling the follwoing procedure for shipping out the Hand Delicerd Transactions..
					gm_sav_scan_shipout_trans(v_rec.shippingid,p_userid,v_rec.statusfl);
				END LOOP;
			END IF;
		END gm_sav_handdel_shipout;
/***********************************************************************************
 Description : Procedure to validate Whether Set or Item Transaction
***********************************************************************************/
FUNCTION get_trans_type
(
      p_ref_id VARCHAR2
)
    RETURN VARCHAR
  IS
    v_type VARCHAR (20);
    v_cnt  NUMBER;
  BEGIN
    SELECT COUNT(1)
    INTO v_cnt
    FROM t907_shipping_info t907 ,
      t504_consignment t504
    WHERE t907.c907_ref_id  = p_ref_id
    AND t907.c907_ref_id    = t504.c504_consignment_id
  --  AND t907.c907_status_fl > '30'
    AND t907.c901_source = 50181
    AND t504.c504_void_fl  IS NULL
    AND t504.c207_set_id   IS NOT NULL
    AND t907.c907_void_fl  IS NULL; 
   IF v_cnt > 0 THEN
    v_type     := 'SET';
   ELSE
    v_type := 'ITEM';
   END IF;
    RETURN v_type ;
  END get_trans_type;


/*******************************************************
	* Purpose: This Function will be used to get print name 
*******************************************************/
	FUNCTION get_mapped_printer_name (
		   p_userid    IN t906_rules.c906_rule_id%TYPE
		 , p_type      IN t906_rules.c906_rule_grp_id%TYPE )
		RETURN VARCHAR2
	IS
		--
		v_codealtnm	  t901_code_lookup.c902_code_nm_alt%TYPE;
		v_codenm      t901_code_lookup.c901_code_nm%TYPE;
	BEGIN

	   v_codealtnm := get_rule_value(p_userid, p_type); 
	   v_codenm := v_codealtnm;
		
		IF p_type = 'PRINTER' THEN
		BEGIN
            SELECT c901_code_nm 
              INTO v_codenm
              FROM t901_code_lookup 
             WHERE c901_code_grp = 'REGPRN' 
               AND c902_code_nm_alt = v_codealtnm 
               AND c901_void_fl IS null;
         EXCEPTION WHEN NO_DATA_FOUND
         THEN
            v_codenm := v_codealtnm;
         END;
               
       ELSIF p_type = 'ZEBRA_PRINTER' THEN
       BEGIN
          SELECT c901_code_nm 
            INTO v_codenm
            FROM t901_code_lookup
           WHERE c901_code_grp = 'ZEBPRN' 
             AND c902_code_nm_alt = v_codealtnm 
             AND c901_void_fl IS null;
       EXCEPTION WHEN NO_DATA_FOUND
         THEN
           v_codenm := v_codealtnm;
         END;
             
       END IF;
			 
		RETURN v_codenm;	 
	--		 
	END get_mapped_printer_name;
END gm_pkg_ship_scan_trans;
/

