/* Formatted on 2012/03/21 12:39 (Formatter Plus v4.8.0) */
--@"C:\Database\Packages\Operations\shipping\gm_pkg_ship_scan_rpt.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_ship_scan_rpt
IS
/******************************************************************
   * Description : Procedure to get the txnid and type based on scanid and scantype
   ****************************************************************/
	PROCEDURE gm_fch_ship_scan_trans (
		p_scanid 		IN		 t5010_tag.c5010_tag_id%TYPE
	  , p_scantype		IN		 VARCHAR2
	  , p_txninfocur	OUT 	 TYPES.cursor_type
	)
	AS
	v_plant_id    t5040_plant_master.c5040_plant_id%TYPE;
	BEGIN
  		SELECT  NVL(get_plantid_frm_cntx(),get_rule_value('DEFAULT_PLANT','ONEPORTAL')) INTO  v_plant_id FROM DUAL;

		IF p_scantype = '102985' -- TAG
		THEN
			OPEN p_txninfocur
	        FOR
	        SELECT t504.c504_consignment_id txnid, t504.c504_type txntype
	           	  ,DECODE(t5010.c901_inventory_type
	           	  		  ,10001, DECODE(t504.c504_type,4119,50182,50181)
	           	  		  ,10004, 50182
	           	  		  ,10003, DECODE(t504.c504_type,4119,50182,50181)
	           	  		  ,'') source 
			  FROM t5010_tag t5010
			      ,t504_consignment t504
			 WHERE c5010_tag_id = p_scanid
			   AND t504.c504_consignment_id = t5010.c5010_last_updated_trans_id
			   AND t504.c504_void_fl IS NULL
			   AND t504.c5040_plant_id= v_plant_id;
		ELSIF p_scantype = '102980' -- TOTE
		THEN
			OPEN p_txninfocur
	        FOR
	         SELECT txnid,txntype,source,get_shipping_division_id(txnid,source) DIVISION_ID      	
			  	  FROM
			    	(
				        SELECT t907.c907_ref_id txnid, '' txntype, t907.c901_source source   
						  FROM t907_shipping_info t907
						 WHERE t907.c907_ship_tote_id = p_scanid
						   AND t907.c907_void_fl IS NULL 
						   AND t907.c907_ref_id IS NOT NULL
						   AND t907.c907_status_fl < 40
						   AND t907.c5040_plant_id= v_plant_id
						  ORDER BY t907.c901_source DESC
						  )
   			   WHERE ROWNUM = 1;
   		ELSIF p_scantype = '102982' -- STATION
        THEN
        	OPEN p_txninfocur
            FOR
              SELECT txnid,txntype,source,get_shipping_division_id(txnid,source) DIVISION_ID
			  	  FROM
			    	(
			            SELECT t907.c907_ref_id txnid, '' txntype, t907.c901_source source                        
			            	FROM t907_shipping_info t907
			            WHERE t907.c907_pack_station_id = p_scanid
			            	AND t907.c907_void_fl IS NULL
			                AND t907.c907_ref_id IS NOT NULL
			                AND t907.c907_status_fl < 40  
			                AND t907.c5040_plant_id= v_plant_id
			              ORDER BY t907.c901_source DESC
					   )
   			   WHERE ROWNUM = 1;

   		ELSIF p_scantype = '102986' -- TRANSACTION
   		THEN
   			OPEN p_txninfocur
	        FOR
	       	 	  SELECT t907.c907_ref_id txnid, '' txntype, t907.c901_source source , get_shipping_division_id(t907.c907_ref_id,t907.c901_source) DIVISION_ID,
	       	 	  -- gm_pkg_ship_scan_rpt.get_consign_division_id(t907.c907_ref_id) DIVISION_ID, 
	       	 	   get_company_dtfmt(T504.c1900_company_id) CMPDFMT	
					  FROM t907_shipping_info t907 , t504_consignment t504
					 WHERE t907.c907_ref_id = p_scanid
                       AND t907.c907_ref_id = t504.c504_consignment_id(+)
					   AND t907.c907_void_fl IS NULL
                       AND t504.c504_void_fl(+) IS NULL
					   AND t907.c907_ref_id IS NOT NULL
					   AND t907.c907_status_fl < 40 
					   AND t907.c5040_plant_id= v_plant_id
					   AND t907.c5040_plant_id= t504.c5040_plant_id(+)
   			  		   AND ROWNUM = 1;
   		ELSIF p_scantype = '102988' -- FGBIN
   		THEN
   			OPEN p_txninfocur
	        FOR
	        	SELECT txnid,txntype,source, sourcenm,get_shipping_division_id(txnid,source) DIVISION_ID
			  	  FROM
			    	(
		   		 	  SELECT t907.c907_ref_id txnid, '' txntype, t907.c901_source source, get_code_name(t907.c901_source) sourcenm
						FROM t907c_fgbin_trans_detail T907c
						   , t907_shipping_info t907
					   WHERE t907.c907_ref_id = t907c.c907c_ref_id
						 AND t907.c907_void_fl IS NULL
						 AND t907c.c907c_void_fl IS NULL
						 AND c907c_bin_id = p_scanid
						 AND t907.c907_ref_id IS NOT NULL
					     AND t907.c907_status_fl < 33 --We Can Reuse the FG Bin for other order also.
					     AND t907.c5040_plant_id= v_plant_id
					   ORDER BY t907c.c907c_created_date DESC
   			     	)
				WHERE ROWNUM = 1;
		END IF;
	END gm_fch_ship_scan_trans;
/******************************************************************
   * Description : Procedure to get the total count, currnt txn count, suggested tote and suggested station.
   ****************************************************************/
	PROCEDURE gm_fch_ship_station_dtl (
		p_txnid 		IN		 t907_shipping_info.c907_ref_id%TYPE
	  , p_source		IN		 t907_shipping_info.c901_source%TYPE
	  , p_currtxncnt    OUT 	 NUMBER
	  , p_tottxncnt     OUT 	 NUMBER
	  , p_suggtoteid	OUT 	 VARCHAR2
	  , p_sugstationid  OUT 	 VARCHAR2
	)
	AS
		v_shipto       t907_shipping_info.c901_ship_to%TYPE;
      	v_shipto_id    t907_shipping_info.c907_ship_to_id%TYPE;
      	v_address_id   t907_shipping_info.c106_address_id%TYPE;
      	v_prdreq_id    t907_shipping_info.c525_product_request_id%TYPE;
      	v_currtxncnt   NUMBER:=0;
      	v_tottxncnt    NUMBER:=0;
      	v_suggtoteid   VARCHAR2(50):='';
	    v_sugstationid VARCHAR2(50):='';
	    v_carrier	   t907_shipping_info.c901_delivery_carrier%TYPE;	
	    v_mode         t907_shipping_info.c901_delivery_mode%TYPE;
	    v_chk_set_fl   VARCHAR2(10);
	    v_source	   t907_shipping_info.c901_source%TYPE;
	    v_overd_attn   t907_shipping_info.c907_override_attn_to%TYPE;
	    v_temp	       VARCHAR2(30);
	BEGIN
		-- Back order is still an open item for orders
		-- Consignment is also order.
		 SELECT t907.c901_ship_to, t907.c907_ship_to_id,
                c106_address_id, t907.c525_product_request_id
               ,t907.c901_delivery_carrier,t907.c901_delivery_mode
               ,t907.c907_override_attn_to
           INTO v_shipto, v_shipto_id,
                v_address_id, v_prdreq_id, v_carrier,v_mode,v_overd_attn
           FROM t907_shipping_info t907
          WHERE t907.c907_ref_id = p_txnid
            AND t907.c901_source = p_source
            AND t907.c907_status_fl < 40
            AND c907_void_fl IS NULL;
        --check to see if consignsignment link to loaner
        v_source := p_source;
        
        IF p_source = 50181 
        THEN
        	v_source := NVL(get_cn_link_source(p_txnid, p_source), p_source);
        END IF;
        
		--Add check for item and set consignemnt for 50181
		SELECT DECODE(p_source, 50181, chk_set_item_cn(p_txnid), NULL) 
		  INTO v_chk_set_fl
		  FROM DUAL;
        
        SELECT DECODE(v_overd_attn,NULL,'-999','-9999') 
  		  INTO v_temp 
		  FROM dual;
        
		IF v_chk_set_fl = 'SET' THEN
			gm_fch_set_station_dtl(	  p_txnid
									, v_source
									, v_shipto
									, v_shipto_id
									, v_address_id
									, v_prdreq_id
									, v_carrier
									, v_mode
									, v_overd_attn
									, v_currtxncnt
									, v_tottxncnt
									, v_suggtoteid
									, v_sugstationid									
									);
		ELSIF v_chk_set_fl = 'ITEM' THEN
			gm_fch_item_station_dtl(  p_txnid
									, p_source
									, v_shipto
									, v_shipto_id
									, v_address_id
									, v_prdreq_id
									, v_carrier
									, v_mode
									, v_overd_attn
									, v_currtxncnt
									, v_tottxncnt
									, v_suggtoteid
									, v_sugstationid									
									);
		ELSE
		-- No need to use "Attention to" for grouping the transaction
			SELECT count(1) , count(DECODE(sign(c907_status_fl-30),-1,NULL,0,NULL,1)) +1
	          INTO v_tottxncnt, v_currtxncnt
	          FROM t907_shipping_info t907
	         WHERE t907.c907_ship_to_id = v_shipto_id
	           AND t907.c901_ship_to = v_shipto
	           AND ( t907.c106_address_id = v_address_id
	            OR   t907.c106_address_id IS NULL
	            OR   t907.c106_address_id = 0 )
	           AND  NVL(t907.c525_product_request_id,-999)= NVL(v_prdreq_id,-999)	            
	           AND (t907.c901_source = v_source 
	           	OR t907.c901_temp_source = v_source)
	           AND t907.c907_status_fl < 40
	           AND t907.c901_delivery_carrier = v_carrier 
	           AND t907.c901_delivery_mode = v_mode
	           --AND (t907.c907_override_attn_to = v_overd_attn
               --     OR  v_temp = NVL(t907.c907_override_attn_to,'-999')
                --   )
	           AND t907.c907_void_fl IS NULL;
	           --Begin code block is added because very first time below select returns no data found whereas above select has count.
	        BEGIN
		        SELECT c907_pack_station_id, c907_ship_tote_id
		          INTO v_sugstationid, v_suggtoteid
		          FROM
		        (SELECT c907_pack_station_id, c907_ship_tote_id, count(c907_ship_tote_id) cnt          
		          FROM t907_shipping_info t907
		         WHERE t907.c907_ship_to_id = v_shipto_id
		           AND t907.c901_ship_to = v_shipto
		           AND ( t907.c106_address_id = v_address_id
		            OR   t907.c106_address_id IS NULL
		            OR   t907.c106_address_id = 0)
		           AND  NVL(t907.c525_product_request_id,-999)= NVL(v_prdreq_id,-999)
		           AND (t907.c901_source = v_source 
	           	   OR t907.c901_temp_source = v_source)
		           AND t907.c907_status_fl < 36
		           AND t907.c901_delivery_carrier = v_carrier 
		           AND t907.c901_delivery_mode = v_mode
		           AND t907.c907_void_fl IS NULL
		           AND c907_ship_tote_id IS NOT NULL
		           --AND (t907.c907_override_attn_to = v_overd_attn
                   -- OR  v_temp = NVL(t907.c907_override_attn_to,'-999')
                   --)
		           group by  c907_pack_station_id, c907_ship_tote_id
		           ORDER BY cnt)
		          WHERE rownum = 1;
	        EXCEPTION
	         WHEN NO_DATA_FOUND
	         THEN
	         	p_suggtoteid:='';
	            p_sugstationid:='';
	        END;	        
		END IF;
		p_currtxncnt := v_currtxncnt;
      	p_tottxncnt := v_tottxncnt;
        p_suggtoteid := v_suggtoteid;
        p_sugstationid := v_sugstationid;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            p_currtxncnt:=0;
            p_tottxncnt:=0;
            p_suggtoteid:='';
            p_sugstationid:='';
	END gm_fch_ship_station_dtl;
	
	/******************************************************************
   * Description : Procedure to get the transaction list 
   * 			   for the tote 
   ****************************************************************/
	PROCEDURE gm_fch_tote_txn_list (
		p_txnid 	IN		 t907_shipping_info.c907_ref_id%TYPE
	  , p_source 	IN		 t907_shipping_info.c901_source%TYPE
	  , p_txn_dtl   OUT 	 TYPES.cursor_type
	)
   AS
   		v_shipto       t907_shipping_info.c901_ship_to%TYPE;
      	v_shipto_id    t907_shipping_info.c907_ship_to_id%TYPE;
      	v_ship_add     VARCHAR2 (1000);
      	v_address_id   t907_shipping_info.c106_address_id%TYPE;
      	v_prdreq_id    t907_shipping_info.c525_product_request_id%TYPE;
      	v_carrier	   t907_shipping_info.c901_delivery_carrier%TYPE;
      	v_chk_set_fl   VARCHAR2(10);
      	v_source	   t907_shipping_info.c901_source%TYPE;
      	v_overd_attn   t907_shipping_info.c907_override_attn_to%TYPE;
      	v_temp	       VARCHAR2(30);
      	v_plant_id     t5040_plant_master.c5040_plant_id%TYPE;
   BEGIN
  	   SELECT  NVL(get_plantid_frm_cntx(),get_rule_value('DEFAULT_PLANT','ONEPORTAL')) INTO  v_plant_id FROM DUAL;

	 SELECT t907.c901_ship_to, t907.c907_ship_to_id,
            c106_address_id, t907.c525_product_request_id
          , t907.c901_delivery_carrier, t907.c907_override_attn_to
       INTO v_shipto, v_shipto_id,
            v_address_id, v_prdreq_id, v_carrier,v_overd_attn
       FROM t907_shipping_info t907
      WHERE t907.c907_ref_id = p_txnid
        AND t907.c901_source = p_source
        AND t907.c907_status_fl < 40
        AND c907_void_fl IS NULL
        AND c5040_plant_id= v_plant_id;	
        
       v_source := p_source;
       
     --check to see if consignsignment link to loaner
        IF p_source = 50181 
        THEN
        	v_source := NVL(get_cn_link_source(p_txnid, p_source), p_source);
        END IF;   
        
	SELECT DECODE(p_source, 50181, chk_set_item_cn(p_txnid), NULL) 
		  INTO v_chk_set_fl
		  FROM DUAL;
	
	SELECT DECODE(v_overd_attn,NULL,'-999','-9999') 
  		  INTO v_temp 
		  FROM dual;
		  
	IF v_chk_set_fl = 'SET' THEN
		OPEN p_txn_dtl FOR
   		SELECT t907.c907_ref_id txnid 
          FROM t907_shipping_info t907, t504_consignment t504
         WHERE t907.c907_ship_to_id = v_shipto_id
           AND t907.c901_ship_to = v_shipto
           AND ( t907.c106_address_id = v_address_id
            OR   t907.c106_address_id IS NULL
            OR   t907.c106_address_id = 0 )
           AND (t907.c901_source = v_source 
	        OR t907.c901_temp_source = v_source)
           AND t907.c907_status_fl < 40
           AND t907.c901_delivery_carrier = v_carrier 
           AND t907.c907_void_fl IS NULL
           AND (t907.c907_override_attn_to = v_overd_attn
                    OR  v_temp = NVL(t907.c907_override_attn_to,'-999')
                   )
           AND t504.c504_consignment_id = t907.c907_ref_id
       	   AND t504.c207_set_id IS NOT NULL
           AND t504.c504_void_fl IS NULL
           AND t907.c5040_plant_id= v_plant_id
           AND t907.c5040_plant_id= t504.c5040_plant_id;
	ELSIF v_chk_set_fl = 'ITEM' THEN
	
		OPEN p_txn_dtl FOR
   		SELECT t907.c907_ref_id txnid 
          FROM t907_shipping_info t907, t504_consignment t504
         WHERE t907.c907_ship_to_id = v_shipto_id
           AND t907.c901_ship_to = v_shipto
           AND ( t907.c106_address_id = v_address_id
            OR   t907.c106_address_id IS NULL
            OR   t907.c106_address_id = 0 )
           AND t907.c901_source = p_source
           AND t907.c907_status_fl < 40
           AND t907.c901_delivery_carrier = v_carrier 
           AND t907.c907_void_fl IS NULL
           AND (t907.c907_override_attn_to = v_overd_attn
                    OR  v_temp = NVL(t907.c907_override_attn_to,'-999')
                   )
           AND t504.c504_consignment_id = t907.c907_ref_id
           AND t504.c207_set_id IS NULL
           AND t504.c504_void_fl IS NULL
           AND t907.c5040_plant_id= v_plant_id
           AND t907.c5040_plant_id= t504.c5040_plant_id;
	ELSE
   		OPEN p_txn_dtl FOR
   		SELECT t907.c907_ref_id txnid ,gm_pkg_op_loaner.get_loaner_etch_id(t907.c907_ref_id) etchid
          FROM t907_shipping_info t907
         WHERE t907.c907_ship_to_id = v_shipto_id
           AND t907.c901_ship_to = v_shipto
           AND ( t907.c106_address_id = v_address_id
            OR   t907.c106_address_id IS NULL
            OR   t907.c106_address_id = 0 )
           AND  NVL(t907.c525_product_request_id,-999)= NVL(v_prdreq_id,-999)
           AND (t907.c901_source = v_source 
	        OR t907.c901_temp_source = v_source)
           AND t907.c907_status_fl < 40
           AND t907.c901_delivery_carrier = v_carrier 
           AND (t907.c907_override_attn_to = v_overd_attn
                    OR  v_temp = NVL(t907.c907_override_attn_to,'-999')
                   )
           AND t907.c907_void_fl IS NULL
           AND t907.c5040_plant_id= v_plant_id;
      END IF;     
	EXCEPTION WHEN OTHERS THEN
    	v_shipto := '';
 	END gm_fch_tote_txn_list;
	
  /******************************************************************
   * Description : fucntion to fetch the total weight of tote
   ****************************************************************/
   FUNCTION get_tote_bin_weight (
      p_scanid     IN   VARCHAR2
    , p_scantype   IN   VARCHAR2     
   )
      RETURN NUMBER
   IS
   v_toteweight NUMBER;
   CURSOR shiptotetxns
		IS
	SELECT t907.c907_ref_id txnid, t907.c901_source source	           	   
	  FROM t907_shipping_info t907
	 WHERE t907.c907_ship_tote_id = p_scanid
	   AND t907.c907_void_fl IS NULL
	   AND t907.c907_status_fl < 40 
	   AND t907.c907_ref_id IS NOT NULL;
   BEGIN
	   	   
	   v_toteweight := 0;
	   IF p_scantype = '102980' -- TOTE
		THEN
			v_toteweight := 5;
			FOR v_rec in shiptotetxns
	        LOOP
	        	v_toteweight := v_toteweight + get_txn_set_weight(v_rec.txnid, v_rec.source);
	        END LOOP;
	    ELSIF p_scantype = '102981' --BOX
	     THEN
	     	SELECT count(1)
	     	  INTO v_toteweight
	    	  FROM t907_shipping_info
	    	 WHERE c907_ship_tote_id = p_scanid
			   AND c907_void_fl IS NULL
			   AND c907_status_fl < 40 
			   AND c907_ref_id IS NOT NULL;
		END IF;	 
		
		RETURN v_toteweight;
   EXCEPTION
   	WHEN NO_DATA_FOUND THEN
   		RETURN v_toteweight;   		
   END get_tote_bin_weight;
   /******************************************************************
   * Description : fucntion to fetch the weight of set.
   ****************************************************************/
   FUNCTION get_txn_set_weight (
      p_txnid 	   IN   t907_shipping_info.c907_ref_id%TYPE
	, p_source	   IN	t907_shipping_info.c901_source%TYPE
   )
      RETURN NUMBER
   IS
   	 v_weight NUMBER;	
   	 v_count NUMBER;
   BEGIN
	   --check for set id condition 
	   IF p_source IN (50182, 50181) --50182: Loaner, 50181: Consignment
	    THEN
	    
	    	BEGIN
		    	SELECT c207c_attribute_value 
				  INTO v_weight
				  FROM t207c_set_attribute t207c, t504_consignment t504
				 WHERE t207c.c901_attribute_type = 102861
				   AND t207c.c207_set_id = t504.c207_set_id
				   AND t504.c504_consignment_id = p_txnid
				   AND t504.c207_set_id IS NOT NULL
				   AND t207c.c207c_void_fl IS NULL
				   AND t504.c504_void_fl IS NULL;
			EXCEPTION
			WHEN NO_DATA_FOUND 
			THEN
			  	SELECT count(1) 
				  INTO v_count
				  FROM t504_consignment t504
				 WHERE t504.c504_consignment_id = p_txnid
				   AND T504.C504_TYPE = 4110 -- consignment
				   AND t504.c207_set_id IS NULL
				   AND t504.c504_void_fl IS NULL;
				   
				   IF v_count > 0 -- For Item consignment
				   THEN
				   		v_weight := 1;
				   END IF;
			END;
		ELSE
			v_weight := 1;
		END IF;	
		RETURN v_weight;  
	EXCEPTION
	WHEN NO_DATA_FOUND 
	THEN
	   RETURN 0;
   END get_txn_set_weight;
 
   /******************************************************************
   * Description : Procedure to get the associated transaction detail
   ****************************************************************/
	PROCEDURE gm_fch_assoc_txn_dtl (
		p_txnid 		IN		 t907_shipping_info.c907_ref_id%TYPE
	  , p_source		IN		 t907_shipping_info.c901_source%TYPE
	  , p_accoctxn      OUT 	 TYPES.cursor_type
	)
   AS
  	    v_shipto       t907_shipping_info.c901_ship_to%TYPE;
      	v_shipto_id    t907_shipping_info.c907_ship_to_id%TYPE;
      	v_address_id   t907_shipping_info.c106_address_id%TYPE;
      	v_prdreq_id    t907_shipping_info.c525_product_request_id%TYPE;
      	v_source	   t907_shipping_info.c901_source%TYPE;
      	v_overd_attn   t907_shipping_info.c907_override_attn_to%TYPE;
      	v_carrier	   t907_shipping_info.c901_delivery_carrier%TYPE;	
	    v_mode         t907_shipping_info.c901_delivery_mode%TYPE;
      	v_temp		   VARCHAR2(30);
      	v_plant_id     t5040_plant_master.c5040_plant_id%TYPE;
   BEGIN
	     SELECT  NVL(get_plantid_frm_cntx(),get_rule_value('DEFAULT_PLANT','ONEPORTAL')) INTO  v_plant_id FROM DUAL;

	     SELECT t907.c901_ship_to, t907.c907_ship_to_id,
                c106_address_id, t907.c525_product_request_id,t907.c907_override_attn_to
                ,t907.c901_delivery_carrier,t907.c901_delivery_mode
           INTO v_shipto, v_shipto_id,
                v_address_id, v_prdreq_id,v_overd_attn,v_carrier,v_mode
           FROM t907_shipping_info t907
          WHERE t907.c907_ref_id = p_txnid
            AND t907.c901_source = p_source
            AND t907.c907_status_fl < 40
            AND c901_ship_to IS NOT NULL
            AND c907_void_fl IS NULL
            AND t907.c5040_plant_id= v_plant_id;
	  	
	  	v_source := p_source;
	  	
	  	SELECT DECODE(v_overd_attn,NULL,'-999','-9999') 
  		  INTO v_temp 
		  FROM dual;
		  
	   --check to see if consignsignment link to loaner
        IF p_source = 50181 
        THEN
        	v_source := NVL(get_cn_link_source(p_txnid, p_source), p_source);
        END IF;   	
		
       OPEN p_accoctxn FOR
     	SELECT t907.c907_pack_station_id stationid, t907.c907_ship_tote_id toteid, count(1) cnt
          FROM t907_shipping_info t907
         WHERE t907.c907_ship_to_id = v_shipto_id
           AND t907.c901_ship_to = v_shipto
           AND ( t907.c106_address_id = v_address_id
            OR   t907.c106_address_id IS NULL)
           AND  NVL(t907.c525_product_request_id,-999)= NVL(v_prdreq_id,-999)
           AND (t907.c901_source = v_source 
	        OR t907.c901_temp_source = v_source)
           AND t907.c907_status_fl < 36         		           
           AND t907.c907_void_fl IS NULL
           AND t907.c907_pack_station_id IS NOT NULL
           AND t907.c907_ship_tote_id IS NOT NULL
           AND t907.c901_delivery_carrier = v_carrier 
           AND t907.c901_delivery_mode = v_mode
           AND t907.c5040_plant_id = v_plant_id
         --  AND (t907.c907_override_attn_to = v_overd_attn
         --           OR  v_temp = NVL(t907.c907_override_attn_to,'-999')
         --          )
           GROUP BY t907.c907_pack_station_id, t907.c907_ship_tote_id
           ORDER BY stationid, toteid;
   END 	gm_fch_assoc_txn_dtl;   
   
   
  /******************************************************************
   * Description : Procedure to get the station details
   ****************************************************************/
	PROCEDURE gm_fch_station_dtl (
		p_station_id 	IN		 t907_shipping_info.c907_pack_station_id%TYPE
	  , p_station_dtl   OUT 	 TYPES.cursor_type
	)
   AS
   		v_plant_id    t5040_plant_master.c5040_plant_id%TYPE;
   BEGIN
	   SELECT  NVL(get_plantid_frm_cntx(),get_rule_value('DEFAULT_PLANT','ONEPORTAL')) INTO  v_plant_id FROM DUAL;

       OPEN p_station_dtl FOR
     	SELECT t907.c907_pack_station_id stationid
     	     , t907.c907_ship_tote_id toteid
     	     , COUNT(1) cnt
          FROM t907_shipping_info t907
         WHERE t907.c907_void_fl IS NULL
           AND t907.c907_pack_station_id LIKE p_station_id || '%'
           AND t907.c907_status_fl = '33'
           AND t907.c5040_plant_id= v_plant_id
           GROUP BY t907.c907_pack_station_id, t907.c907_ship_tote_id
           ORDER BY stationid, toteid;
   END 	gm_fch_station_dtl;   
 
 /******************************************************************
   * Description : Procedure to get the transaction list 
   *                 for the searched feild
   ****************************************************************/
      PROCEDURE gm_fch_search_txn_list (
          p_scanid    IN       t907_shipping_info.c907_pack_station_id%TYPE
        , p_scantype  IN       VARCHAR2
        , p_txn_dtl   OUT      TYPES.cursor_type
      )
   AS
   v_scan_id VARCHAR2(50);
   BEGIN
	   IF p_scantype = '102985' -- TAG
		THEN
			OPEN p_txn_dtl
	        FOR
	        SELECT t504.c504_consignment_id txnid
	           	 , DECODE(t5010.c901_inventory_type,10001,50181,10004,50182,'') source
	           	 , gm_pkg_op_loaner.get_loaner_etch_id(t504.c504_consignment_id) etchid
			  FROM t5010_tag t5010
			     , t504_consignment t504
			     , t907_shipping_info t907 
			 WHERE c5010_tag_id = p_scanid
			   AND t504.c504_consignment_id = t5010.c5010_last_updated_trans_id
			   AND t504.c504_void_fl IS NULL
			   AND t504.c504_consignment_id = t907.c907_ref_id
           	   AND t907.c907_void_fl IS NULL;
		ELSE
		   IF p_scantype = '102982' -- STATION
		   THEN
		   		v_scan_id := p_scanid;
		   ELSIF p_scantype = '102983' -- STATIONROW
		   THEN
		   		v_scan_id := p_scanid || '%';
		   END IF;
		   OPEN p_txn_dtl FOR
		   SELECT t907.c907_ref_id txnid
		        , t907.c901_source source
		        , gm_pkg_op_loaner.get_loaner_etch_id(t907.c907_ref_id) etchid
		        , t907.c907_tracking_number trackingno
			FROM t907_shipping_info t907 
		   WHERE t907.c907_void_fl IS NULL
			 AND t907.c907_status_fl < DECODE(p_scantype,'102982',36,40) -- 102982: station, if scantype is 102982, need to display the records less than RFP
			 AND ( c907_ship_tote_id = p_scanid 
		     	   OR c525_product_request_id = p_scanid
		      	   OR c907_tracking_number = p_scanid
		      	   OR c907_pack_station_id LIKE v_scan_id
		         )
		     ORDER BY source;
		END IF;
 END gm_fch_search_txn_list;
 
 /******************************************************************
   * Description : function to check the CN is item cn or set cn.   
   ****************************************************************/	
   FUNCTION chk_set_item_cn(
		p_txnid 	IN		 t907_shipping_info.c907_ref_id%TYPE	  
	)
	RETURN VARCHAR
   IS
   v_set_item_fl VARCHAR2(10);
   BEGIN	   
	   SELECT DECODE(t504.c207_set_id, NULL, 'ITEM','SET')
	     INTO v_set_item_fl
		 FROM t504_consignment t504
		WHERE t504.c504_consignment_id = p_txnid
		  AND t504.c504_void_fl IS NULL;
		RETURN v_set_item_fl;	
   EXCEPTION
   		WHEN OTHERS THEN
   			RETURN '';   			
   END chk_set_item_cn;
   
   /******************************************************************
   * Description : Procedure to fetch the set CN station details. 
   * 			   for the tote 
   ****************************************************************/
	PROCEDURE gm_fch_set_station_dtl (
		p_txnid 		IN		 t907_shipping_info.c907_ref_id%TYPE
	  , p_source		IN		 t907_shipping_info.c901_source%TYPE
	  , p_shipto        IN		 t907_shipping_info.c901_ship_to%TYPE
      ,	p_shipto_id     IN 		 t907_shipping_info.c907_ship_to_id%TYPE
      , p_address_id    IN		 t907_shipping_info.c106_address_id%TYPE
      ,	p_prdreq_id     IN		 t907_shipping_info.c525_product_request_id%TYPE
      , p_carrier	    IN		 t907_shipping_info.c901_delivery_carrier%TYPE
      , p_mode	        IN		 t907_shipping_info.c901_delivery_mode%TYPE
      , p_overd_attn    IN		 t907_shipping_info.c907_override_attn_to%TYPE
	  , p_currtxncnt    OUT 	 NUMBER
	  , p_tottxncnt     OUT 	 NUMBER
	  , p_suggtoteid	OUT 	 VARCHAR2
	  , p_sugstationid  OUT 	 VARCHAR2
	)
   AS
   		v_shipto       t907_shipping_info.c901_ship_to%TYPE;
      	v_shipto_id    t907_shipping_info.c907_ship_to_id%TYPE;
      	v_address_id   t907_shipping_info.c106_address_id%TYPE;
      	v_prdreq_id    t907_shipping_info.c525_product_request_id%TYPE;
      	v_currtxncnt   NUMBER:=0;
      	v_tottxncnt    NUMBER:=0;
      	v_suggtoteid   VARCHAR2(50):='';
	    v_sugstationid VARCHAR2(50):='';
	    v_carrier	   t907_shipping_info.c901_delivery_carrier%TYPE; 
	    v_temp		   VARCHAR2(30);
	    
   BEGIN
   		SELECT DECODE(p_overd_attn,NULL,'-999','-9999') 
  		  INTO v_temp 
		  FROM dual;
 		-- this procedure only called from inside this package,if called from other place then need
 		-- call get_cn_link_source to check if cn link to ln, same check as in gm_fch_ship_station_dtl
	   SELECT count(1) , count(DECODE(sign(c907_status_fl-30),-1,NULL,0,NULL,1)) +1
          INTO v_tottxncnt, v_currtxncnt
          FROM t907_shipping_info t907, t504_consignment t504
         WHERE t907.c907_ship_to_id = p_shipto_id
           AND t907.c901_ship_to = p_shipto
           AND ( t907.c106_address_id = p_address_id
            OR   t907.c106_address_id IS NULL
            OR   t907.c106_address_id = 0 )
           AND (t907.c901_source = p_source 
	        OR t907.c901_temp_source = p_source)
           AND t907.c907_status_fl < 40
           AND t907.c901_delivery_carrier = p_carrier 
           AND t907.c901_delivery_mode = p_mode
           --AND (t907.c907_override_attn_to = p_overd_attn
           --         OR  v_temp = NVL(t907.c907_override_attn_to,'-999')
           --        )
           AND t504.c504_consignment_id = t907.c907_ref_id
           AND t504.c207_set_id IS NOT NULL
           AND t504.c504_void_fl IS NULL
           AND t907.c907_void_fl IS NULL;
           --Begin code block is added because very first time below select returns no data found whereas above select has count.
        BEGIN
	        SELECT c907_pack_station_id, c907_ship_tote_id
	          INTO v_sugstationid, v_suggtoteid
	          FROM
	        (SELECT c907_pack_station_id, c907_ship_tote_id, count(c907_ship_tote_id) cnt          
	          FROM t907_shipping_info t907, t504_consignment t504
	         WHERE t907.c907_ship_to_id = p_shipto_id
	           AND t907.c901_ship_to = p_shipto
	           AND ( t907.c106_address_id = p_address_id
	            OR   t907.c106_address_id IS NULL
	            OR   t907.c106_address_id = 0)
	           AND (t907.c901_source = p_source 
	           OR t907.c901_temp_source = p_source)
	           AND t907.c907_status_fl < 36
	           AND t907.c901_delivery_carrier = p_carrier 
	           AND t907.c901_delivery_mode = p_mode
	           --AND (t907.c907_override_attn_to = p_overd_attn
               --     OR  v_temp = NVL(t907.c907_override_attn_to,'-999')
               --    )
	           
	           AND t907.c907_void_fl IS NULL
	           AND c907_ship_tote_id IS NOT NULL
	           AND t504.c504_consignment_id = t907.c907_ref_id
           	   AND t504.c207_set_id IS NOT NULL
               AND t504.c504_void_fl IS NULL
	           group by  c907_pack_station_id, c907_ship_tote_id
	           ORDER BY cnt)
	          WHERE rownum = 1;
        EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
         	p_suggtoteid:='';
            p_sugstationid:='';
        END;
        p_currtxncnt := v_currtxncnt;
      	p_tottxncnt := v_tottxncnt;
        p_suggtoteid := v_suggtoteid;
        p_sugstationid := v_sugstationid;       
        
    EXCEPTION
     WHEN NO_DATA_FOUND
     THEN
        p_currtxncnt:=0;
        p_tottxncnt:=0;
        p_suggtoteid:='';
        p_sugstationid:='';
       
   END gm_fch_set_station_dtl;
   
    /******************************************************************
   * Description : Procedure to fetch the set CN station details. 
   * 			   for the tote 
   ****************************************************************/
	PROCEDURE gm_fch_item_station_dtl (
		p_txnid 		IN		 t907_shipping_info.c907_ref_id%TYPE
	  , p_source		IN		 t907_shipping_info.c901_source%TYPE
	  , p_shipto        IN		 t907_shipping_info.c901_ship_to%TYPE
      ,	p_shipto_id     IN 		 t907_shipping_info.c907_ship_to_id%TYPE
      , p_address_id    IN		 t907_shipping_info.c106_address_id%TYPE
      ,	p_prdreq_id     IN		 t907_shipping_info.c525_product_request_id%TYPE
      , p_carrier	    IN		 t907_shipping_info.c901_delivery_carrier%TYPE	
      , p_mode	        IN		 t907_shipping_info.c901_delivery_mode%TYPE
      , p_overd_attn    IN		 t907_shipping_info.c907_override_attn_to%TYPE
	  , p_currtxncnt    OUT 	 NUMBER
	  , p_tottxncnt     OUT 	 NUMBER
	  , p_suggtoteid	OUT 	 VARCHAR2
	  , p_sugstationid  OUT 	 VARCHAR2
	)
   AS
   		v_shipto       t907_shipping_info.c901_ship_to%TYPE;
      	v_shipto_id    t907_shipping_info.c907_ship_to_id%TYPE;
      	v_address_id   t907_shipping_info.c106_address_id%TYPE;
      	v_prdreq_id    t907_shipping_info.c525_product_request_id%TYPE;
      	v_currtxncnt   NUMBER:=0;
      	v_tottxncnt    NUMBER:=0;
      	v_suggtoteid   VARCHAR2(50):='';
	    v_sugstationid VARCHAR2(50):='';
	    v_carrier	   t907_shipping_info.c901_delivery_carrier%TYPE;
	    v_temp		   VARCHAR2(30);
   BEGIN
   
   		SELECT DECODE(p_overd_attn,NULL,'-999','-9999') 
  		  INTO v_temp 
		  FROM dual;
   		
	    SELECT count(1) , count(DECODE(sign(c907_status_fl-30),-1,NULL,0,NULL,1)) +1
          INTO v_tottxncnt, v_currtxncnt
          FROM t907_shipping_info t907, t504_consignment t504
         WHERE t907.c907_ship_to_id = p_shipto_id
           AND t907.c901_ship_to = p_shipto
           AND ( t907.c106_address_id = p_address_id
            OR   t907.c106_address_id IS NULL
            OR   t907.c106_address_id = 0 )
           AND t907.c901_source = p_source
           AND t907.c907_status_fl < 40
           AND t907.c901_delivery_carrier = p_carrier 
           AND t907.c901_delivery_mode = p_mode
           --AND (t907.c907_override_attn_to = p_overd_attn
           --   OR  v_temp = NVL(t907.c907_override_attn_to,'-999')
           --)
           
           AND t504.c504_consignment_id = t907.c907_ref_id
           AND t504.c207_set_id IS NULL
           AND t504.c504_void_fl IS NULL
           AND t907.c907_void_fl IS NULL;
           --Begin code block is added because very first time below select returns no data found whereas above select has count.
        BEGIN
	        SELECT c907_pack_station_id, c907_ship_tote_id
	          INTO v_sugstationid, v_suggtoteid
	          FROM
	        (SELECT c907_pack_station_id, c907_ship_tote_id, count(c907_ship_tote_id) cnt          
	          FROM t907_shipping_info t907, t504_consignment t504
	         WHERE t907.c907_ship_to_id = p_shipto_id
	           AND t907.c901_ship_to = p_shipto
	           AND ( t907.c106_address_id = p_address_id
	            OR   t907.c106_address_id IS NULL
	            OR   t907.c106_address_id = 0)
	           AND t907.c901_source = p_source
	           AND t907.c907_status_fl < 36
	           AND t907.c901_delivery_carrier = p_carrier 
	           AND t907.c901_delivery_mode = p_mode
	           --AND (t907.c907_override_attn_to = p_overd_attn
               --     OR  v_temp = NVL(t907.c907_override_attn_to,'-999')
               --    )	           
	           AND t907.c907_void_fl IS NULL
	           AND c907_ship_tote_id IS NOT NULL
	           AND t504.c504_consignment_id = t907.c907_ref_id
           	   AND t504.c207_set_id IS NULL
               AND t504.c504_void_fl IS NULL
	           group by  c907_pack_station_id, c907_ship_tote_id
	           ORDER BY cnt)
	          WHERE rownum = 1;
        EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
         	p_suggtoteid:='';
            p_sugstationid:='';
        END;
        p_currtxncnt := v_currtxncnt;
      	p_tottxncnt := v_tottxncnt;
        p_suggtoteid := v_suggtoteid;
        p_sugstationid := v_sugstationid;       
        
    EXCEPTION
     WHEN NO_DATA_FOUND
     THEN
        p_currtxncnt:=0;
        p_tottxncnt:=0;
        p_suggtoteid:='';
        p_sugstationid:='';
        
   END gm_fch_item_station_dtl;
   
   /*******************************************************************************************
	 * Description : Procedure to get the Transaction Details which are ready to pick status
     * Author: HReddi 
    ********************************************************************************************/
   PROCEDURE gm_fch_readytopick_trans(
   	   p_career    IN       VARCHAR2,
   	   p_txn_dtl   OUT      TYPES.cursor_type   	   
   )       
   AS  
   v_company_id   t1900_company.c1900_company_id%TYPE;
   BEGIN
	   SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL'))
            INTO v_company_id
            FROM DUAL;
            
	   IF (to_char(p_career) = 'FEDEX') THEN
	   OPEN p_txn_dtl FOR
		      SELECT c907_shipping_id shippingid
		         , c907_ref_id  transid
	             , c907_tracking_number  tracknumber
	             , c901_source  source
	             , c901_delivery_carrier shipmode 
	             , c907_status_fl statusfl
	          FROM t907_shipping_info 
	         WHERE c907_status_fl = '36' 
	           AND c901_delivery_carrier != '5000'      
	           AND c907_void_fl is null 
	           AND c1900_company_id  = v_company_id
	           AND c907_tracking_number IS NOT NULL;
	    ELSE
	     OPEN p_txn_dtl FOR
		      SELECT c907_shipping_id shippingid
		         , c907_ref_id  transid
	             , c907_tracking_number  tracknumber
	             , c901_source  source
	             , c901_delivery_carrier shipmode 
	             , c907_status_fl statusfl
	          FROM t907_shipping_info 
	         WHERE c907_status_fl = '36'
	           AND c901_delivery_carrier = '5000'   
	           AND c907_void_fl is null 
	           AND c1900_company_id  = v_company_id
	           AND c907_tracking_number IS NOT NULL;
	    END IF;
   END gm_fch_readytopick_trans;
 
    /******************************************************************
   * Description : Function to get Consignment link loaner source
   ****************************************************************/
   FUNCTION get_cn_link_source (
   	p_refid 		IN		 t907_shipping_info.c907_ref_id%TYPE,
   	p_source 		IN 		 t907_shipping_info.c901_source%TYPE
   	)
      RETURN NUMBER
   IS
      v_source   NUMBER;
   BEGIN
       SELECT c901_temp_source
              INTO v_source
              FROM t907_shipping_info
          WHERE c907_ref_id = p_refid
               AND c901_source = p_source
               AND c907_void_fl IS NULL;

      RETURN v_source;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN NULL;
   END get_cn_link_source;
   
   /******************************************************************
   * Description : Function to get Division id for the Consignment
   ****************************************************************/
   FUNCTION get_consign_division_id (
   	p_consignemnt_id t504_consignment.c504_consignment_id%TYPE
   	)
      RETURN VARCHAR2
   IS
   v_division_id   t504_consignment.c1910_division_id%TYPE;
BEGIN
     BEGIN
       SELECT c1910_division_id 
         INTO v_division_id  
         FROM t504_consignment where c504_consignment_id = p_consignemnt_id 
          AND c504_void_fl IS NULL ;
   EXCEPTION
      WHEN NO_DATA_FOUND
   THEN
      RETURN ' ';
      END;
     RETURN v_division_id;
   END get_consign_division_id;
   
   
END gm_pkg_ship_scan_rpt;
/
