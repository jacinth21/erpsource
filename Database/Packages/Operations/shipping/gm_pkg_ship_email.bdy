 
--@"C:\Database\Packages\Operations\shipping\gm_pkg_ship_email.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_ship_email
IS
 
    /******************************************************************
   * Description : Function to get ship ref id by GUID
   ****************************************************************/
   FUNCTION get_ship_refid_by_guid (
   	 p_guid   IN T907_SHIPPING_INFO.C907_GUID%TYPE 
   	)
      RETURN VARCHAR2
   IS
      v_refid     T907_SHIPPING_INFO.C907_REF_ID%TYPE;
   BEGIN
       SELECT t907.C907_REF_ID
		   INTO v_refid
		   FROM T907_SHIPPING_INFO t907
		  WHERE t907.C907_GUID = p_guid ;

      RETURN v_refid;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN NULL;
   END get_ship_refid_by_guid;
END gm_pkg_ship_email;
/

