--@"c:\database\packages\operations\shipping\gm_pkg_ship_rollback.bdy";

CREATE OR REPLACE
PACKAGE BODY gm_pkg_ship_rollback
IS

	/****************************************************************
	    * Description : This Procedure will Rollback Order details
	    * Author      : Jignesh Shah
	*****************************************************************/
	PROCEDURE gm_rollback_order (
		p_txn_id	IN t907_shipping_info.c907_ref_id%TYPE,
		p_user_id	IN t907_shipping_info.c907_last_updated_by%TYPE
	)
	AS
		v_strlen			NUMBER := NVL (LENGTH (p_txn_id), 0);
		v_string	   		VARCHAR2 (4000) := p_txn_id;
		v_substring    		VARCHAR2 (4000);
		v_txn_id			t907_shipping_info.c907_ref_id%TYPE;
		v_status_flag		VARCHAR2 (10);
		
		v_ship_id			t907_shipping_info.c907_shipping_id%TYPE;
		v907_status_fl		t907_shipping_info.c907_status_fl%TYPE;
		
	BEGIN
		IF v_strlen > 0
		THEN
			WHILE INSTR (v_string, '|') <> 0
	        LOOP
				v_txn_id		:= NULL;
				v_status_flag	:= NULL;
				
				v_txn_id		:= SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
				v_string		:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
				v_substring		:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
				v_status_flag	:= v_substring;
				
				BEGIN
					SELECT NVL(t907.c907_shipping_id,0),NVL(t907.c907_status_fl,0)
						INTO v_ship_id, v907_status_fl
					FROM t907_shipping_info t907
					WHERE t907.c901_source = 50180
					AND t907.c907_ref_id = v_txn_id
					AND t907.c907_status_fl IN ('33','36')
					AND t907.C907_VOID_FL IS NULL
					FOR UPDATE;
			       	
					EXCEPTION
					WHEN NO_DATA_FOUND THEN
						raise_application_error(-20602,'');	-- Consignment / Request is not in correct status.
				END;

				IF v_status_flag = v907_status_fl
				THEN
					raise_application_error(-20602,''); -- Consignment / Request is not in correct status.
				END IF;
				
				BEGIN
					UPDATE t907_shipping_info
					SET c907_status_fl = DECODE (v_status_flag,NULL,'30',v_status_flag)
						, c907_ship_tote_id = DECODE(v_status_flag,'33',c907_ship_tote_id,NULL)
						, c907_pack_station_id = DECODE(v_status_flag,'33',c907_pack_station_id,NULL)
						, c907_tracking_number = DECODE(v_status_flag,'33',c907_tracking_number,NULL)
						, c907_last_updated_date = SYSDATE
						, c907_last_updated_by = p_user_id
					WHERE c907_shipping_id = v_ship_id
					AND c907_void_fl IS NULL;
					
					UPDATE t501_order
					SET c501_status_fl = DECODE(v_status_flag,NULL,'2','33','2.30','2')
						, c501_last_updated_date = SYSDATE
						, c501_last_updated_by = p_user_id
					WHERE c501_order_id = v_txn_id
					AND c501_void_fl IS NULL;
				END;
			END LOOP;
		END IF;
	END gm_rollback_order;
	
	/********************************************************************
	    * Description : This Procedure will Rollback consignment details
	    * Author      : Jignesh Shah
	*********************************************************************/
	PROCEDURE gm_rollback_cn (
		p_txn_id	IN t907_shipping_info.c907_ref_id%TYPE,
		p_user_id	IN t907_shipping_info.c907_last_updated_by%TYPE
	)
	AS
		v_strlen			NUMBER := NVL (LENGTH (p_txn_id), 0);
		v_string	   		VARCHAR2 (4000) := p_txn_id;
		v_substring    		VARCHAR2 (4000);
		v_txn_id			t907_shipping_info.c907_ref_id%TYPE;
		v_status_flag		VARCHAR2 (10);
		
		v_ship_id			t907_shipping_info.c907_shipping_id%TYPE;
		v907_status_fl		t907_shipping_info.c907_status_fl%TYPE;
	
	BEGIN
		IF v_strlen > 0
		THEN
			WHILE INSTR (v_string, '|') <> 0
	        LOOP
				v_txn_id		:= NULL;
				v_status_flag	:= NULL;
				
				v_txn_id		:= SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
				v_string		:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
				v_substring		:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
				v_status_flag	:= v_substring;
				
				BEGIN
					SELECT NVL(t907.c907_shipping_id,0),NVL(t907.c907_status_fl,0)
						INTO v_ship_id, v907_status_fl
					FROM t907_shipping_info t907
					WHERE t907.c901_source = 50181
					AND t907.c907_ref_id = v_txn_id
					AND t907.c907_status_fl IN ('33','36')
					AND t907.C907_VOID_FL IS NULL
					FOR UPDATE;
			       	
					EXCEPTION
					WHEN NO_DATA_FOUND THEN
						raise_application_error(-20602,'');	-- Consignment / Request is not in correct status.
				END;

				IF v_status_flag = v907_status_fl
				THEN
					raise_application_error(-20602,''); -- Consignment / Request is not in correct status.
				END IF;
				
				BEGIN
					UPDATE t907_shipping_info
					SET c907_status_fl = DECODE(v_status_flag,NULL,'30',v_status_flag)
						, c907_ship_tote_id = DECODE(v_status_flag,'33',c907_ship_tote_id,NULL)
						, c907_pack_station_id = DECODE(v_status_flag,'33',c907_pack_station_id,NULL)
						, c907_tracking_number = DECODE(v_status_flag,'33',c907_tracking_number,NULL)
						, c907_last_updated_date = SYSDATE
						, c907_last_updated_by = p_user_id
					WHERE c907_shipping_id = v_ship_id
					AND c907_void_fl IS NULL;
			
					UPDATE t504_consignment
					SET c504_status_fl = DECODE(v_status_flag,NULL,'3','33','3.30','3')
						, c504_last_updated_date = SYSDATE
						, c504_last_updated_by = p_user_id
					WHERE c504_consignment_id = v_txn_id
					AND c504_void_fl IS NULL;
			
				END;
			END LOOP;
		END IF;
	  
	END gm_rollback_cn;
	
	/************************************************************************
	    * Description : This Procedure will Rollback Loaner details
	    * Author      : Jignesh Shah
	*************************************************************************/
	PROCEDURE gm_rollback_ln (
		p_txn_id	IN t907_shipping_info.c907_ref_id%TYPE,
		p_user_id	IN t907_shipping_info.c907_last_updated_by%TYPE
	)
	AS
		v_strlen			NUMBER := NVL (LENGTH (p_txn_id), 0);
		v_string	   		VARCHAR2 (4000) := p_txn_id;
		v_substring    		VARCHAR2 (4000);
		v_txn_id			t907_shipping_info.c907_ref_id%TYPE;
		v_status_flag		VARCHAR2 (10);
		v_status			t504a_consignment_loaner.c504a_status_fl%TYPE;
		
		v_ship_id			t907_shipping_info.c907_shipping_id%TYPE;
		v907_status_fl		t907_shipping_info.c907_status_fl%TYPE;
	
	BEGIN
		IF v_strlen > 0
		THEN
			WHILE INSTR (v_string, '|') <> 0
	        LOOP
				v_txn_id		:= NULL;
				v_status_flag	:= NULL;
				
				v_txn_id		:= SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
				v_string		:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
				v_substring		:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
				v_status_flag	:= v_substring;
				
				BEGIN
					SELECT NVL(t907.c907_shipping_id,0),NVL(t907.c907_status_fl,0)
						INTO v_ship_id, v907_status_fl
					FROM t907_shipping_info t907
					WHERE t907.c901_source = 50182
					AND t907.c907_ref_id = v_txn_id
					AND t907.c907_status_fl IN ('33','36')
					AND t907.C907_VOID_FL IS NULL
					FOR UPDATE;
			       	
					EXCEPTION
					WHEN NO_DATA_FOUND THEN
						raise_application_error(-20602,'');	-- Consignment / Request is not in correct status.
				END;

				IF v_status_flag = v907_status_fl
				THEN
					raise_application_error(-20602,''); -- Consignment / Request is not in correct status.
				END IF;
				
				BEGIN
					UPDATE t907_shipping_info
					SET c907_status_fl = DECODE(v_status_flag,NULL,'30',v_status_flag)
						, c907_ship_tote_id = DECODE(v_status_flag,'33',c907_ship_tote_id,NULL)
						, c907_pack_station_id = DECODE(v_status_flag,'33',c907_pack_station_id,NULL)
						, c907_tracking_number = DECODE(v_status_flag,'33',c907_tracking_number,NULL)
						, c907_last_updated_date = SYSDATE
						, c907_last_updated_by = p_user_id
					WHERE c907_shipping_id = v_ship_id
					AND c907_void_fl IS NULL;
					
					select DECODE(v_status_flag,NULL,'10','33','13','10') INTO v_status FROM DUAL;
					-- Call the common procedure to update status
					gm_pkg_op_loaner_set_txn.gm_upd_consign_loaner_status(v_txn_id,v_status,p_user_id);

				END;
			END LOOP;
		END IF;
	  
	END gm_rollback_ln;
	
	/************************************************************************
	    * Description : This Procedure will Rollback Loaner Extn details
	    * Author      : Jignesh Shah
	*************************************************************************/
	PROCEDURE gm_rollback_fgle (
		p_txn_id	IN t907_shipping_info.c907_ref_id%TYPE,
		p_user_id	IN t907_shipping_info.c907_last_updated_by%TYPE
	)
	AS
		v_strlen			NUMBER := NVL (LENGTH (p_txn_id), 0);
		v_string	   		VARCHAR2 (4000) := p_txn_id;
		v_substring    		VARCHAR2 (4000);
		v_txn_id			t907_shipping_info.c907_ref_id%TYPE;
		v_status_flag		VARCHAR2 (10);
		
		v_ship_id			t907_shipping_info.c907_shipping_id%TYPE;
		v907_status_fl		t907_shipping_info.c907_status_fl%TYPE;
	
	BEGIN
		IF v_strlen > 0
		THEN
			WHILE INSTR (v_string, '|') <> 0
	        LOOP
				v_txn_id		:= NULL;
				v_status_flag	:= NULL;
				
				v_txn_id		:= SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
				v_string		:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
				v_substring		:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
				v_status_flag	:= v_substring;
				
				BEGIN
					SELECT NVL(t907.c907_shipping_id,0),NVL(t907.c907_status_fl,0)
						INTO v_ship_id, v907_status_fl
					FROM t907_shipping_info t907
					WHERE t907.c901_source = 50183
					AND t907.c907_ref_id = v_txn_id
					AND t907.c907_status_fl IN ('33','36')
					AND t907.C907_VOID_FL IS NULL
					FOR UPDATE;
			       	
					EXCEPTION
					WHEN NO_DATA_FOUND THEN
						raise_application_error(-20602,'');	-- Consignment / Request is not in correct status.
				END;

				IF v_status_flag = v907_status_fl
				THEN
					raise_application_error(-20602,''); -- Consignment / Request is not in correct status.
				END IF;
				
				BEGIN
					UPDATE t907_shipping_info
					SET c907_status_fl = DECODE(v_status_flag,NULL,'30',v_status_flag)
						, c907_ship_tote_id = DECODE(v_status_flag,'33',c907_ship_tote_id,NULL)
						, c907_pack_station_id = DECODE(v_status_flag,'33',c907_pack_station_id,NULL)
						, c907_tracking_number = DECODE(v_status_flag,'33',c907_tracking_number,NULL)
						, c907_last_updated_date = SYSDATE
						, c907_last_updated_by = p_user_id
					WHERE c907_shipping_id = v_ship_id
					AND c907_void_fl IS NULL;
			
					UPDATE t412_inhouse_transactions
					SET c412_status_fl = DECODE(v_status_flag,NULL,'3','33','3.30','3')
						, c412_last_updated_date = SYSDATE
						, c412_last_updated_by = p_user_id
					WHERE c412_inhouse_trans_id = v_txn_id
					AND c412_void_fl IS NULL;
			
				END;
			END LOOP;
		END IF;
	  
	END gm_rollback_fgle;
	
	
	/*******************************************************************************
	    * Description : This Procedure will Rollback In-house Loaner (RBLNIT) details
	    * Author      : Jignesh Shah
	********************************************************************************/
	PROCEDURE gm_rollback_ihln (
		p_txn_id	IN t907_shipping_info.c907_ref_id%TYPE,
		p_user_id	IN t907_shipping_info.c907_last_updated_by%TYPE
	)
	AS
		v_strlen			NUMBER := NVL (LENGTH (p_txn_id), 0);
		v_string	   		VARCHAR2 (4000) := p_txn_id;
		v_substring    		VARCHAR2 (4000);
		v_txn_id			t907_shipping_info.c907_ref_id%TYPE;
		v_status_flag		VARCHAR2 (10);
		
		v_ship_id			t907_shipping_info.c907_shipping_id%TYPE;
		v907_status_fl		t907_shipping_info.c907_status_fl%TYPE;
	
	BEGIN
		IF v_strlen > 0
		THEN
			WHILE INSTR (v_string, '|') <> 0
	        LOOP
				v_txn_id		:= NULL;
				v_status_flag	:= NULL;
				
				v_txn_id		:= SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
				v_string		:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
				v_substring		:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
				v_status_flag	:= v_substring;
				
				BEGIN
					SELECT NVL(t907.c907_shipping_id,0),NVL(t907.c907_status_fl,0)
						INTO v_ship_id, v907_status_fl
					FROM t907_shipping_info t907
					WHERE t907.c901_source = 50186
					AND t907.c907_ref_id = v_txn_id
					AND t907.c907_status_fl IN ('33','36')
					AND t907.C907_VOID_FL IS NULL
					FOR UPDATE;
			       	
					EXCEPTION
					WHEN NO_DATA_FOUND THEN
						raise_application_error(-20602,'');	-- Consignment / Request is not in correct status.
				END;

				IF v_status_flag = v907_status_fl
				THEN
					raise_application_error(-20602,''); -- Consignment / Request is not in correct status.
				END IF;
				
				BEGIN
					UPDATE t907_shipping_info
					SET c907_status_fl = DECODE(v_status_flag,NULL,'30',v_status_flag)
						, c907_ship_tote_id = DECODE(v_status_flag,'33',c907_ship_tote_id,NULL)
						, c907_pack_station_id = DECODE(v_status_flag,'33',c907_pack_station_id,NULL)
						, c907_tracking_number = DECODE(v_status_flag,'33',c907_tracking_number,NULL)
						, c907_last_updated_date = SYSDATE
						, c907_last_updated_by = p_user_id
					WHERE c907_shipping_id = v_ship_id
					AND c907_void_fl IS NULL;
			
					UPDATE t504_consignment
					SET c504_status_fl = DECODE(v_status_flag,NULL,'3','33','3.30','3')
						, c504_last_updated_date = SYSDATE
						, c504_last_updated_by = p_user_id
					WHERE c504_consignment_id = v_txn_id
					AND c504_void_fl IS NULL;
			
				END;
			END LOOP;
		END IF;
	  
	END gm_rollback_ihln;
	
	/**************************************************************************
     * Description : Procedure to save the new staus for the respective tables.
     **************************************************************************/	
     PROCEDURE gm_sav_ship_rollback (
		p_refid 		IN		 t907_shipping_info.c907_ref_id%TYPE
	  , p_source		IN		 t907_shipping_info.c901_source%TYPE
	  , p_userid 		IN 	     VARCHAR2
	  , p_stationid		IN		 t907_shipping_info.c907_pack_station_id%TYPE
	 )
	 AS
		v_status_fl 	varchar2(10);
		v_shipid	   	VARCHAR2 (100);
		v_rma_id        VARCHAR2 (50);
		v_plant_id      t5040_plant_master.c5040_plant_id%TYPE;
		
	 BEGIN
 		 SELECT  NVL(get_plantid_frm_cntx(),get_rule_value('DEFAULT_PLANT','ONEPORTAL')) INTO  v_plant_id FROM DUAL;

		v_shipid	:= gm_pkg_cm_shipping_info.get_shipping_id (p_refid, p_source, '');
		
		SELECT DECODE(c907_status_fl,'36','33','33','30',c907_status_fl)
		  INTO v_status_fl
		  FROM t907_shipping_info
		 WHERE c907_shipping_id = v_shipid AND c907_void_fl IS NULL AND c5040_plant_id= v_plant_id
		   FOR UPDATE;
		
		UPDATE t907_shipping_info
		   SET c907_status_fl = v_status_fl
		     , c907_pack_station_id = DECODE(v_status_fl,'30',NULL,p_stationid) -- 30:Pending Shipping 
		     , c907_ship_tote_id = DECODE(v_status_fl,'30',NULL,c907_ship_tote_id)
		     , c907_tracking_number = DECODE(v_status_fl,'30',NULL,c907_tracking_number)
			 , c907_last_updated_by = p_userid
			 , c907_last_updated_date = SYSDATE
		 WHERE c907_shipping_id = v_shipid AND c907_void_fl IS NULL;
		 --below procedure is used to update the status for T501,T504 and t412.
		 gm_pkg_ship_scan_trans.gm_upd_trans_status(p_refid,p_source,v_status_fl,p_userid);
		 -- below code is for voiding the RMA ID for the IHLN which is generated at the time of Moving this Transaction from Pending Shipping[30] to Packing In Progress[33].
		 -- because we are Roll backing the IHLN-XXXX Packing In Progress[33] to Pending Shipping[30]. 
		 -- This time we are not having the Return Summary Transaction for the IHLN-XXXX .
			v_rma_id := gm_pkg_op_ihloaner_item_rpt.get_associated_ra(p_refid);
		 IF p_source = 50186 AND v_status_fl = '30' THEN
			gm_pkg_op_return.gm_cs_sav_void_return(v_rma_id,p_userid);
	     END IF;
	 END gm_sav_ship_rollback;
	 
/******************************************************************
    * Description : Procedure to save the Roll back status of Tote by Ref ID.
    **************************************************************************/	
	PROCEDURE gm_sav_ship_rollback_tote(
		p_scanid 		IN		 VARCHAR2
	  , p_scantype		IN		 VARCHAR2	
	  , p_userid 		IN 	     VARCHAR2
	  , p_stationid		IN		 t907_shipping_info.c907_pack_station_id%TYPE
	)
	AS
		v_status_fl 	varchar2(10);
		v_shipid	   	VARCHAR2 (100);
		v_source		VARCHAR2 (100);
		
	CURSOR shiptotetxns
		IS	
		SELECT t907.c907_ref_id txnid, t907.c901_source source, t907.c907_status_fl	shipstatus           	   
		  FROM t907_shipping_info t907
		 WHERE t907.c907_ship_tote_id = p_scanid
		   AND t907.c907_void_fl IS NULL
		   AND t907.c907_ref_id IS NOT NULL
		   AND t907.c907_status_fl < '40';
	BEGIN
			FOR v_rec IN shiptotetxns
			LOOP
				gm_pkg_ship_rollback.gm_sav_ship_rollback(v_rec.txnid, v_rec.source, p_userid, p_stationid);
				v_source := v_rec.source; -- get the source to pass it to validate procedure
			END LOOP;

			gm_pkg_ship_scan_trans.gm_validate_station_tote(v_source, p_scanid, p_stationid, NULL);
	END gm_sav_ship_rollback_tote;
	
END gm_pkg_ship_rollback;
/