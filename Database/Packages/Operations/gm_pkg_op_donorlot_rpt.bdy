--@"c:\database\packages\operations\gm_pkg_op_donorlot_rpt.bdy";
CREATE OR REPLACE PACKAGE BODY gm_pkg_op_donorlot_rpt
IS
  /****************************************************************************
  * Description : This procedure is to fetch the Lot details based on the Donor
  * Author      : Hreddi
  *****************************************************************************/
	PROCEDURE gm_fch_lot_info(
		p_donor_no		IN T2540_DONOR_MASTER.C2540_DONOR_NUMBER%TYPE,
	    p_out_lot_dtls OUT TYPES.cursor_type
	)
	AS	 
	  v_dt_fmt VARCHAR2(20);
	  v_company_id   	t1900_company.c1900_company_id%TYPE;
   	  v_plant_id 	  	t5040_plant_master.c5040_plant_id%TYPE;
	BEGIN
		
	SELECT  get_compid_frm_cntx() ,get_plantid_frm_cntx(),NVL(get_compdtfmt_frm_cntx(),get_rule_value ('DATEFMT', 'DATEFORMAT'))
			INTO  v_company_id,v_plant_id,v_dt_fmt
			FROM DUAL;
	  
	  OPEN p_out_lot_dtls FOR
	  	 SELECT ROWNUM Sno, A.pnum, A.cnum, A.pdesc, A.expdate,A.txnid, A.txntype, A.status, A.packslip, A.reldate, A.recdate, A.txndate
			FROM
			(
			 SELECT t2550.c205_part_number_id pnum, t2550.c2550_control_number cnum, t205.C205_PART_NUM_DESC pdesc
			  , to_char(t2550.c2550_expiry_date,v_dt_fmt) expdate, get_last_trans_id_type (t2550.c2550_control_number, 'ID') txnid, 
			  gm_pkg_op_lotcode_rpt.get_intxn_type(get_last_trans_id_type ( t2550.c2550_control_number, 'TYPE')) txntype,
			  gm_pkg_op_lotcode_rpt.get_lot_current_status(t2550.c2550_control_number) status,
			  t2550.C2550_PACKING_SLIP packslip, NVL(get_dhr_release_date(t2550.c2550_last_updated_trans_id), to_char(t2550.c2550_last_updated_date,v_dt_fmt)) reldate,
			  to_char(t2550.C2550_MFG_DATE,v_dt_fmt) recdate,
			  to_char(t2550.c2550_last_updated_date,v_dt_fmt) txndate
			   FROM t2550_part_control_number t2550, t2540_donor_master t2540, t205_part_number t205
			  WHERE t2540.c2540_donor_number  = p_donor_no
			    AND t2540.c2540_donor_id      = t2550.c2540_donor_id
			    AND t2550.c205_part_number_id = t205.c205_part_number_id
			    --AND t2550.c1900_company_id = v_company_id
          		--AND t2550.c5040_plant_id = v_plant_id
          		AND NOT EXISTS
          		(                
                    SELECT t408a.c408a_conrol_number		  
                      FROM t408_dhr t408 ,
                           t408a_dhr_control_number t408a ,
                           t2540_donor_master t2540
                     WHERE t408a.c408_dhr_id      = t408.c408_dhr_id
                       AND t2540.c2540_donor_id = t408.c2540_donor_id
                       AND t408.c1900_company_id = t2540.c1900_company_id
                       AND t408.c408_void_fl   IS NULL
                       AND t408a.c408a_void_fl IS NULL
                       AND t2540.c2540_void_fl IS NULL
                       AND t408.c408_status_fl != 4
                       AND t408.c1900_company_id = v_company_id
                       AND t408.c5040_plant_id = v_plant_id   
                       AND t2540.c2540_donor_number = p_donor_no
                       AND t2550.c2550_control_number = t408a.c408a_conrol_number
                )
			 UNION ALL
			 SELECT t408.c205_part_number_id pnum, t408a.c408a_conrol_number cnum, t205.C205_PART_NUM_DESC pdesc
			  , to_char(t408.c2550_expiry_date,v_dt_fmt) expdate, NVL (t4082.c4081_dhr_bulk_id, t408a.c408_dhr_id) txnid, DECODE (
			    t4082.c4081_dhr_bulk_id, NULL, 'DHR', 'RS') txntype,
			    gm_pkg_op_lotcode_rpt.get_lot_current_status(t408a.c408a_conrol_number) status,
			    t408.c408_packing_slip packslip, to_char(t408.c408_verified_ts, v_dt_fmt) reldate, to_char(t408.c408_received_date, v_dt_fmt) recdate,
			    to_char(NVL(t408.c408_last_updated_date,t408.c408_created_date), v_dt_fmt) txndate
			   FROM t408_dhr t408, t408a_dhr_control_number t408a, t2540_donor_master t2540
			  , t4082_bulk_dhr_mapping t4082, t205_part_number t205
			  WHERE t2540.c2540_donor_number = p_donor_no	
			    AND t2540.c2540_donor_id     = t408.c2540_donor_id
			    AND t408.c408_dhr_id         = t408a.c408_dhr_id
			    AND t408.c205_part_number_id = t205.c205_part_number_id
			    AND t408.c408_status_fl      < 4
			    AND t408.c408_dhr_id         = t4082.c408_dhr_id(+)
			    AND t408.c408_void_fl       IS NULL
			    AND t408a.c408a_void_fl     IS NULL
			    AND t2540.c2540_void_fl     IS NULL
			    AND t408.c1900_company_id = v_company_id
          		AND t408.c5040_plant_id = v_plant_id
			    ) A
			    ORDER BY A.cnum, A.pnum;  
             
	END gm_fch_lot_info;
	
	
 /*********************************************************************************
  * Description: This function will return verified date of DHR
  * Author:		Xun
  *********************************************************************************/
	FUNCTION get_dhr_release_date(
		p_dhr_id	VARCHAR2
	) RETURN VARCHAR2
	IS 
		v_rel_date VARCHAR2(20);
		v_dt_fmt t906_rules.c906_rule_value%TYPE;

		BEGIN
		
		SELECT NVL(get_compdtfmt_frm_cntx(),get_rule_value('DATEFMT', 'DATEFORMAT'))
		  INTO v_dt_fmt 
		  FROM DUAL;

		  SELECT to_char(c408_verified_ts, v_dt_fmt)
		  INTO v_rel_date
		   FROM t408_dhr
		  WHERE c408_dhr_id   = NVL(p_dhr_id, '-999')
		    AND c408_void_fl IS NULL; 
		
		RETURN v_rel_date;
		EXCEPTION
			WHEN NO_DATA_FOUND
		THEN
			RETURN NULL;			
	END get_dhr_release_date; 

	/****************************************************************************************************
	  * Description: This function will get last transaction id or transaction type based on the Control 
	  * number and input type
	  * Author:		Xun
	  ****************************************************************************************************/
	FUNCTION get_last_trans_id_type(
		p_control_num	VARCHAR2,
        p_type          VARCHAR2
	) RETURN VARCHAR2
	IS
		v_trans_id_type VARCHAR2(30); 
		v_company_id   	t1900_company.c1900_company_id%TYPE;
   		v_plant_id 	  	t5040_plant_master.c5040_plant_id%TYPE;
     BEGIN
	     SELECT  get_compid_frm_cntx() ,get_plantid_frm_cntx()
			INTO  v_company_id,v_plant_id
			FROM DUAL;
	     
         SELECT A.last_txn_id_type 
             INTO v_trans_id_type 
         FROM (
          SELECT DECODE(p_type, 'ID', t5061.c5061_last_update_trans_id, DECODE(t5061.c901_last_transaction_type,'50982','',t5061.c901_last_transaction_type))  last_txn_id_type 
	  			  FROM t5061_control_number_inv_log t5061 
	 			 WHERE t5061.c5060_control_number = p_control_num
	 			 AND t5061.c1900_company_id = v_company_id
         		 AND t5061.c5040_plant_id = v_plant_id
                  ORDER BY t5061.c5061_created_date DESC) A
	   			   WHERE ROWNUM = 1;
         RETURN v_trans_id_type;
         EXCEPTION
                WHEN NO_DATA_FOUND THEN  
                BEGIN
	            SELECT B.last_txn_id_type   
	                INTO v_trans_id_type
                         FROM (
	                SELECT DECODE(p_type, 'ID',  NVL (t4082.c4081_dhr_bulk_id, t408a.c408_dhr_id), DECODE(t4082.c4081_dhr_bulk_id, NULL, 'DHR', 'RS') ) last_txn_id_type 
                          FROM  t408a_dhr_control_number t408a 
                              , t4082_bulk_dhr_mapping t4082,t408_dhr t408
                         where t408a.c408a_conrol_number =  p_control_num  
                           AND t408a.c408_dhr_id = t4082.c408_dhr_id(+) 
                           AND t408a.c408_dhr_id = t408.c408_dhr_id
                           AND t408a.c408a_void_fl IS NULL
                           AND t4082.c4082_void_fl IS NULL
                           AND t408.c408_void_fl IS NULL
                           AND t408.c1900_company_id = v_company_id
                           AND t408.c5040_plant_id = v_plant_id
                           ORDER BY t408.c408_created_date DESC) B
                           WHERE ROWNUM = 1;
	             RETURN v_trans_id_type;
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            RETURN ''; 
    END; 
END get_last_trans_id_type;


 /****************************************************************************************************
  * Description: This function will get the transaction id and transaction type based on the Donor LOt
  * Author:		HReddi
  ****************************************************************************************************/
	FUNCTION get_lot_txn_id(
		p_control_num	VARCHAR2
	) RETURN VARCHAR2
	IS
		v_transID    VARCHAR2 (50);	
		v_company_id   	t1900_company.c1900_company_id%TYPE;
   		v_plant_id 	  	t5040_plant_master.c5040_plant_id%TYPE;
		BEGIN
			SELECT  get_compid_frm_cntx() ,get_plantid_frm_cntx()
				INTO  v_company_id,v_plant_id
				FROM DUAL;
			
			SELECT B.TXNID || '|' || B.TXNTYPE  INTO v_transID
              FROM(
              	   SELECT A.TXNTYPE, A.TXNID 
                     FROM(
		                 SELECT T408.C408_DHR_ID TXNID, NVL (T408.C408_LAST_UPDATED_DATE, T408.C408_CREATED_DATE) TXNDATE,
		                    'DHR' TXNTYPE
		                   FROM t408_dhr t408, t408a_dhr_control_number t408a
		                  WHERE t408a.c408a_conrol_number = p_control_num
		                    AND T408.C408_DHR_ID          = T408A.C408_DHR_ID
		                    AND t408.c408_void_fl        IS NULL
		                    AND T408A.C408A_VOID_FL      IS NULL
		                    AND t408.c1900_company_id = v_company_id
                            AND t408.c5040_plant_id = v_plant_id
		              UNION ALL
		                 SELECT T506.C506_RMA_ID TXNID, NVL (T506.C506_LAST_UPDATED_DATE, T506.C506_CREATED_DATE) TXNDATE,
		                    'Returns' txntype
		                   FROM t506_returns t506, t507_returns_item t507
		                  WHERE t507.c507_control_number = p_control_num
		                    AND t506.c506_rma_id         = t507.c506_rma_id
		                    AND t506.C506_VOID_FL       IS NULL 
		                    AND t506.c1900_company_id = v_company_id
                            AND t506.c5040_plant_id = v_plant_id
		              UNION ALL
		                 SELECT T412.C412_INHOUSE_TRANS_ID TXNID, NVL (T412.C412_LAST_UPDATED_DATE, T412.C412_CREATED_DATE)
		                    txndate, GET_CODE_NAME (t412.C412_TYPE) txntype
		                   FROM t412_inhouse_transactions t412, t413_inhouse_trans_items t413
		                  WHERE t413.c413_control_number   = p_control_num
		                    AND t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
		                    AND t412.c412_void_fl         IS NULL
		                    AND t413.c413_void_fl         IS NULL
		                    AND t412.c1900_company_id = v_company_id
                            AND t412.c5040_plant_id = v_plant_id
		              UNION ALL
		                 SELECT T501.C501_ORDER_ID TXNID, NVL (T501.C501_LAST_UPDATED_DATE, T501.C501_CREATED_DATE) TXNDATE,
		                    'Order' txntype
		                   FROM t501_order t501, t502_item_order t502
		                  WHERE t501.c501_order_id       = t502.c501_order_id
		                    AND t502.c502_control_number = p_control_num
		                    AND t501.c501_void_fl       IS NULL
		                    AND t502.c502_void_fl       IS NULL
		                    AND t501.c1900_company_id = v_company_id
                            AND t501.c5040_plant_id = v_plant_id
		              UNION ALL
		                 SELECT T5062.C5062_TRANSACTION_ID TXNID, NVL (T5062.C5062_LAST_UPDATED_DATE, T5062.C5062_RESERVED_DATE
		                    ) TXNDATE, 'Reserved' txntype
		                   FROM t5060_control_number_inv t5060, t5062_control_number_reserve t5062
		                  WHERE t5060.c5060_control_number        = p_control_num
		                    AND t5060.c5060_control_number_inv_id = t5062.c5060_control_number_inv_id
		                    AND T5060.C5060_QTY                   > 0
		                    AND t5062.c5062_void_fl              IS NULL
		                    AND t5060.c1900_company_id = v_company_id
                            AND t5060.c5040_plant_id = v_plant_id
		              UNION ALL
		                 SELECT T504.C504_CONSIGNMENT_ID TXNID, NVL (T504.C504_LAST_UPDATED_DATE, T504.C504_CREATED_DATE)
		                    txndate, 'Consignment' txntype
		                   FROM t504_consignment t504, t505_item_consignment t505
		                  WHERE t505.c505_control_number = p_control_num
		                    AND t504.c504_type           = 4110
		                    AND t504.c504_consignment_id = t505.c504_consignment_id
		                    AND T504.C504_VOID_FL       IS NULL
		                    AND T505.C505_VOID_FL       IS NULL
		                    AND t504.c1900_company_id = v_company_id
                            AND t504.c5040_plant_id = v_plant_id
		               ORDER BY txndate
            			) A
       	           ORDER BY A.TXNDATE DESC )B
         	 WHERE ROWNUM = 1 ;  
		RETURN v_transID;
		EXCEPTION
			WHEN NO_DATA_FOUND
		THEN
			RETURN '';
			
	END get_lot_txn_id;
	
 /***********************************************************************************
  * Description: This function will get the transaction Status based on the Donor LOt
  * Author:		HReddi
  ************************************************************************************/
	/*FUNCTION get_lot_txn_status(
		p_trans_id	VARCHAR2
	) RETURN VARCHAR2
	IS
		v_transID    VARCHAR2 (50);
		v_dhr_id VARCHAR2(100);
		v_trans_id VARCHAR2(100);
		BEGIN
			v_trans_id := replace(p_trans_id,' ','');
			v_dhr_id := SUBSTR (v_trans_id, 1, INSTR (v_trans_id, '|') - 1);
			SELECT B.STATUS  INTO v_transID
              FROM(
              	   SELECT A.STATUS
                     FROM(
		                 SELECT DECODE(T408.C408_STATUS_FL, '1', 'Pending Inspection', '2', 'Pending Process','3', 'Pending Verify','4', 'Verified') STATUS
		                   FROM t408_dhr t408
		                  WHERE t408.c408_dhr_id = v_dhr_id
		                    AND t408.c408_void_fl        IS NULL
		              UNION ALL
		                 SELECT DECODE(t506.c506_status_fl,0,'Pending Return',1,'Pending Return',2,'Returned','') STATUS
		                   FROM t506_returns t506
		                  WHERE t506.c506_rma_id  = v_dhr_id
		                    AND t506.C506_VOID_FL IS NULL 
		              UNION ALL
		                 SELECT DECODE(t412.C412_STATUS_FL,
				                  2,   'Pending Control Number',
				                  3,   'Pending Verification',
				                  3.30,'Packing In Progress',
				                  3.60,'Ready For Pickup',	
				                  4 ,  'Completed'				               
				                 )
		                   FROM t412_inhouse_transactions t412
		                  WHERE t412.c412_inhouse_trans_id = v_dhr_id
		                    AND t412.c412_void_fl         IS NULL
		              UNION ALL
		                 SELECT DECODE (t501.C501_STATUS_FL,
				   				  0,    'Back Order',
				                  1,    'Pending Control Number',
				                  2,    'Pending Shipping',
				                  2.30, 'Packing In Progress',
				                  2.60, 'Ready For Pickup',	
				                  3 ,   'Completed'
				                 ) STATUS
		                   FROM t501_order t501
		                  WHERE t501.c501_order_id       = v_dhr_id
		                    AND t501.c501_void_fl       IS NULL
		              UNION ALL
		                 SELECT get_code_name(t5062.c901_status) STATUS
		                   FROM t5062_control_number_reserve t5062
		                  WHERE t5062.c5062_transaction_id       = v_dhr_id
		                    AND t5062.c5062_void_fl              IS NULL
		              UNION ALL
		                 SELECT DECODE (t504.C504_STATUS_FL,
			                      0,    'Initiated',
			                      1,    'WIP',
			                      1.10, 'Completed Set',
			                      1.20, 'Pending Putaway',
			                      2,    'Built Set/Pending Control Number',
			                      2.20, 'Pending Pick',
			                      3,    'Pending Shipping',
			                      3.30, 'Packing In Progress',
			                      3.60, 'Ready For Pickup',
			                      4,    'Completed'
			                    ) STATUS
		                   FROM t504_consignment t504
		                  WHERE t504.c504_consignment_id = v_dhr_id
		                    AND t504.c504_type           = 4110
		                    AND T504.C504_VOID_FL       IS NULL
            			) A
       	            )B;          	
		RETURN v_transID;
		EXCEPTION
			WHEN NO_DATA_FOUND
		THEN
			RETURN '';
			
	END get_lot_txn_status;*/
	
 /*********************************************************************
  * Description: This function will get Vendor ID based on the DHR id
  * Author:		HReddi
  *********************************************************************/
	FUNCTION get_vendor_id(
		p_dhr_id	VARCHAR2
	) RETURN VARCHAR2
	IS
		v_venndor_id    VARCHAR2 (50);
		v_WO_id    VARCHAR2 (50);
		v_dhr_id   VARCHAR2(100);
		BEGIN
			v_dhr_id := SUBSTR (p_dhr_id, 1, INSTR (p_dhr_id, '|') - 1);
			SELECT t301.c301_vendor_id INTO v_venndor_id 
			  FROM t408_dhr t408, t301_vendor t301
			 WHERE t408.c301_vendor_id = t301.c301_vendor_id
			   AND t408.C408_DHR_ID = v_dhr_id
			   AND t408.c408_void_fl IS NULL;
		RETURN v_venndor_id;
		EXCEPTION
			WHEN NO_DATA_FOUND
		THEN
			RETURN '';			
	END get_vendor_id;
	
 /*******************************************************************************
  * Description: This function will return the Work Order ID based on the DHR id
  * Author:		HReddi
  *******************************************************************************/
	FUNCTION get_workorder_id(
		p_dhr_id	VARCHAR2
	) RETURN VARCHAR2
	IS
		v_WO_id    VARCHAR2 (50);
		v_dhr_id   VARCHAR2(100);
		BEGIN
			v_dhr_id := SUBSTR (p_dhr_id, 1, INSTR (p_dhr_id, '|') - 1);
			SELECT t402.c402_work_order_id INTO v_WO_id 
			  FROM t408_dhr t408, t402_work_order t402
			 WHERE t408.c402_work_order_id = t402.c402_work_order_id
			   AND t408.C408_DHR_ID = v_dhr_id
			   AND t408.c408_void_fl IS NULL
			   AND t402.c402_void_fl IS NULL;
		RETURN v_WO_id;
		EXCEPTION
			WHEN NO_DATA_FOUND
		THEN
			RETURN '';			
	END get_workorder_id;
	
 /*********************************************************************************
  * Description: This function will return Transaction TYpe and Transaction Ref ID
  * Author:		HReddi
  *********************************************************************************/
	FUNCTION get_trans_type(
		p_dhr_id	VARCHAR2
	) RETURN VARCHAR2
	IS
		v_dhr_id   VARCHAR2(100);
		v_trans_data VARCHAR2(100);
		BEGIN
			v_dhr_id := SUBSTR (p_dhr_id, 1, INSTR (p_dhr_id, '|') - 1);
			BEGIN
				SELECT t412.c412_type  INTO v_trans_data 
				  FROM t412_inhouse_transactions t412
				 WHERE t412.c412_inhouse_trans_id = v_dhr_id
				   AND t412.c412_void_fl IS NULL;
			   EXCEPTION WHEN NO_DATA_FOUND
			   THEN
			 		v_trans_data := NULL;	
			END;
			
			IF v_trans_data IS NULL THEN
				SELECT t504.c504_type  INTO v_trans_data 
				  FROM t504_consignment t504
				 WHERE t504.c504_consignment_id = v_dhr_id
				   AND t504.c504_void_fl IS NULL;
			END IF;

		RETURN v_trans_data;
		EXCEPTION
			WHEN NO_DATA_FOUND
		THEN
			RETURN '';			
	END get_trans_type;
	
  /*********************************************************************************
  * Description: This function will return Transaction TYpe and Transaction Ref ID
  * Author:		HReddi
  *********************************************************************************/
	FUNCTION get_trans_ref_id(
		p_dhr_id	VARCHAR2
	) RETURN VARCHAR2
	IS
		v_dhr_id   VARCHAR2(100);
		v_trans_data VARCHAR2(100);
		BEGIN
			v_dhr_id := SUBSTR (p_dhr_id, 1, INSTR (p_dhr_id, '|') - 1);
			BEGIN
				SELECT t412.c412_ref_id INTO v_trans_data 
				  FROM t412_inhouse_transactions t412
				 WHERE t412.c412_inhouse_trans_id = v_dhr_id
				   AND t412.c412_void_fl IS NULL;
			EXCEPTION WHEN NO_DATA_FOUND THEN
				v_trans_data := NULL;
			END;	
		IF v_trans_data IS NULL THEN
			SELECT t504.c504_ref_id  INTO v_trans_data 
			  FROM t504_consignment t504
			 WHERE t504.c504_consignment_id = v_dhr_id
			   AND t504.c504_void_fl IS NULL;
		END IF;
		
		RETURN v_trans_data;
		EXCEPTION
			WHEN NO_DATA_FOUND
		THEN
			RETURN '';			
	END get_trans_ref_id; 
END gm_pkg_op_donorlot_rpt;
/
