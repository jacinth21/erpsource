--@"c:\database\packages\operations\gm_pkg_op_set_master.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_set_master
IS

	/********************************************************************
 	* Purpose: This procedure save the Weight Details for Set's
 	* Created By Jignesh Shah
 	*********************************************************************/
	PROCEDURE gm_sav_set_weight (
	  p_input_str	IN	VARCHAR2
	, p_user_id		IN	t207c_set_attribute.c207c_last_updated_by%TYPE
	)
	AS
		v_strlen			NUMBER := NVL (LENGTH (p_input_str), 0);
	    v_string	   		VARCHAR2 (4000) := p_input_str;
	    v_substring    		VARCHAR2 (4000);
	    v_demandsheet_id	t4022_demand_par_detail.c4020_demand_master_id%TYPE;
	  	v_set_id			t207c_set_attribute.c207_set_id%TYPE;
	  	v_weight_value		t207c_set_attribute.c207c_attribute_value%TYPE;
	  	v_demandshtid		t4022_demand_par_detail.c4020_demand_master_id%TYPE;
	  	v_count				NUMBER;
	
	BEGIN
		IF v_strlen > 0
		THEN
			WHILE INSTR (v_string, '|') <> 0
	      	LOOP
	      		v_set_id		:= NULL;
				v_weight_value	:= NULL;
				
	      		v_substring		:= SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
				v_string		:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
				v_set_id		:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring		:= SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_weight_value	:= v_substring;
				
			   SELECT COUNT(1) 
			     INTO v_count 
			     FROM t207c_set_attribute 
				WHERE c207_set_id = v_set_id; 

			/* IF Condition is added for, from JS we are getting the input string for empty Sets also. so we should not updated or Insert for which we are not submitted.
			 * SO we are checking the condition that if WEIGHT is aleady setted means then it has to update for that set id  or else it has to Insert the new Record  */
				
				IF v_weight_value IS NOT NULL THEN
			  		--The Below Code is added for PMT-5969.The Update and insert script are moved to below procedure.
					gm_sav_set_attribute(v_set_id, '102861', v_weight_value, NULL, p_user_id);
				END IF;
	      	END LOOP;
	    END IF;
	END gm_sav_set_weight;

	/********************************************************************
 	* Purpose: This procedure is fetching the Weight Details for Set's
 	* Created By Jignesh Shah
 	*********************************************************************/
	PROCEDURE gm_fch_set_weight(
	  p_set_id			IN	 t207c_set_attribute.C207_SET_ID%TYPE
	, p_set_name   		IN	 VARCHAR2
	, p_set_type		IN	 t207_set_master.c207_type%TYPE
	, p_out_cur	    	OUT	 TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_out_cur
			 FOR
				SELECT 
						t207.c207_set_id setid 
						, t207c.c901_attribute_type attrtype
						, t207.c207_set_nm setdesc
						, t207c.c207c_attribute_value weight
						, t207.c207_type settype
						, get_user_name (t207c.c207c_created_by) createdby
						, t207c.c207c_created_date createddate
						, get_user_name (t207c.c207c_last_updated_by) updatedby
						, TO_CHAR (t207c.c207c_last_updated_date, 'MM/dd/yyyy') updateddate
				FROM t207_set_master t207
					, ( SELECT c207_set_id setid
							, c901_attribute_type  ,c207c_attribute_value
							, c207c_created_by, c207c_created_date
							, c207c_last_updated_by, c207c_last_updated_date
						FROM t207c_set_attribute t207c
						WHERE  c207c_void_fl IS NULL
						AND c901_attribute_type = 102861
						) t207c
				WHERE t207.c207_set_id = t207c.setid (+)
				AND t207.c207_void_fl IS NULL
    	   		AND t207.c207_set_id 			LIKE '%' || nvl (p_set_id, t207.c207_set_id) || '%'
    	   		AND upper (t207.c207_set_nm)	LIKE '%' || nvl (upper (p_set_name), '') || '%'
    	   		AND t207.c207_type = DECODE(p_set_type,'0',t207.c207_type,p_set_type)
      			ORDER BY t207.c207_set_id,updateddate ;
	END gm_fch_set_weight;
	/********************************************************************
 	* Purpose: This procedure is used to save Loaner Pool Set Attribute.
 	* Created By Manikandan Rajasekaran
 	*********************************************************************/
	PROCEDURE gm_sav_set_attribute(
	  p_set_id			IN	 t207c_set_attribute.c207_set_id%TYPE
	, p_attrtype   		IN	 t207c_set_attribute.c901_attribute_type%TYPE
	, p_attrvalue		IN	 t207c_set_attribute.c207c_attribute_value%TYPE
	, p_histflag	    IN	 t207c_set_attribute.c207c_set_history_fl%TYPE
	, p_userid	        IN	 t207c_set_attribute.c207c_created_by%TYPE
	)
	AS
	BEGIN
		
		UPDATE t207c_set_attribute t207c
		SET    t207c.c207c_attribute_value = p_attrvalue, 
               t207c.c207c_last_updated_by = p_userid, 
               t207c.c207c_last_updated_date = SYSDATE,
               t207c.c207c_set_history_fl = NVL(p_histflag, c207c_set_history_fl)
         WHERE t207c.c207_set_id = p_set_id
		 AND   t207c.c901_attribute_type = p_attrtype
         AND   t207c.C207C_VOID_FL IS NULL;
         
         IF (SQL%ROWCOUNT = 0) 
         THEN
			INSERT INTO t207c_set_attribute (c207c_set_attribute_id, c207_set_id, c901_attribute_type,
											 c207c_attribute_value,c207c_created_by,c207c_created_date,c207c_set_history_fl)
			VALUES (s4022_demand_par_detail.NEXTVAL, p_set_id, p_attrtype, p_attrvalue,p_userid,SYSDATE,p_histflag);
		END IF;
	END gm_sav_set_attribute;

END gm_pkg_op_set_master;
/
