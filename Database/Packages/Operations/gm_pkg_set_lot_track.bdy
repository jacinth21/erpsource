/* Formatted on 2011/06/14 17:57 (Formatter Plus v4.8.0) */
--@"C:\Database\Packages\Operations\gm_pkg_set_lot_track.bdy";
create or replace
PACKAGE BODY gm_pkg_set_lot_track
IS
    /*   GM_SAV_LOANER_NET - This procedure will upate location and part mapping
    input: Deliminted String*/
    /**********************************************************************
    * Description : Procedure to store the loaner set details in Set Lot Master and Set Part Lot Qty table . This
    procedure
    is called from the JMS class
    * Author    : Gomathi
    **********************************************************************/
PROCEDURE GM_PKG_SET_LOT_UPDATE (
        P_TRANS_ID       IN VARCHAR2,
        p_user_id        IN t101_user.c101_user_id%TYPE,
        P_REF_ID         IN T5052_LOCATION_MASTER.C5052_REF_ID%TYPE DEFAULT NULL,
        p_trans_type     IN t5054_inv_location_log.c901_txn_type%TYPE DEFAULT NULL,
        p_warehouse_type IN T5052_LOCATION_MASTER.c5051_inv_warehouse_id%TYPE DEFAULT NULL,
        p_action 		 IN T901_code_lookup.c901_code_id%TYPE DEFAULT NULL,
        p_loaner_rollback 		 IN VARCHAR2 DEFAULT NULL)
AS
    v_location_id t5052_location_master.c5052_location_id%TYPE;
    v_location_type t5052_location_master.c901_location_type%TYPE;
    v_set_part_qty_id t5071_set_part_qty.c5071_set_part_qty_id%TYPE;
    v_set_lot_master_id t5070_set_lot_master.c5070_set_lot_master_id%TYPE;
    v_user_id NUMBER;
    v_ref_id t412_inhouse_transactions.C412_REF_ID%TYPE;
    v_trans_type t412_inhouse_transactions.C412_TYPE%TYPE;
    v_control_num t5054_inv_location_log.c5054_txn_control_number%TYPE;
    v_order_type t501_order.c901_order_type%TYPE;
    v_txn_qty t5054_inv_location_log.c5054_txn_qty%TYPE;
    v_part_number_id t205_part_number.c205_part_number_id%TYPE;
    v_sign NUMBER;
    v_return_type t5054_inv_location_log.c901_txn_type%TYPE;
    v_txn_type T5054_INV_LOCATION_LOG.C901_TXN_TYPE%TYPE;
    v_type	   T5054_INV_LOCATION_LOG.C901_TYPE%TYPE;
    v_action  T901_code_lookup.c901_code_id%TYPE DEFAULT NULL;
    
    V_TRANS_ID varchar2(200);
    v_string  CLOB;
    v_temp_txn_id T412_INHOUSE_TRANSACTIONS.C412_INHOUSE_TRANS_ID%TYPE;
    v_cnt NUMBER;
    v_parent_order_id varchar2(200);
	v_parent_order_type  t501_order.c901_order_type%TYPE;
	v_insert_sign NUMBER;
	v_insert_action T901_code_lookup.c901_code_id%TYPE DEFAULT NULL;
	v_rule_value t906_rules.c906_rule_value%TYPE;
	v_company_id  t1900_company.c1900_company_id%TYPE;
	
    CURSOR inv_loc
    IS
         SELECT C205_PART_NUMBER_ID, C5054_TXN_QTY * DECODE(p_loaner_rollback,'loaner_rollback',-1,1) QTY, C5054_TXN_CONTROL_NUMBER
          , C5054_TXN_ID, C5054_INV_LOCATION_LOG_ID, C901_TYPE, C901_TXN_TYPE
           FROM T5054_INV_LOCATION_LOG
          WHERE C5052_LOCATION_ID    = V_LOCATION_ID
            AND C5054_TXN_ID         = V_TRANS_ID
            AND DECODE(p_loaner_rollback,'loaner_rollback','1',NVL(C5054_LOT_STATUS_FL,'2')) = DECODE(p_loaner_rollback,'loaner_rollback','1','2')
            ;
            
    CURSOR usage_lot
    IS
         SELECT C205_PART_NUMBER_ID, SUM (C502B_ITEM_QTY) C502B_ITEM_QTY, C502B_USAGE_LOT_NUM
           FROM T502B_ITEM_ORDER_USAGE
          WHERE C501_ORDER_ID        = V_TRANS_ID
            AND C502B_USAGE_LOT_NUM IS NOT NULL
            AND C502B_VOID_FL IS NULL
       GROUP BY C205_PART_NUMBER_ID, C502B_USAGE_LOT_NUM;
       
      
       
BEGIN
      V_TRANS_ID := P_TRANS_ID;
      v_string  := P_TRANS_ID;
      v_action  := p_action;
      -- Loaner Set to Loaner Sales txn will have comma in the transaction ID variable.
      -- Check if the variable is having ',', which means it will be like GM-LN-1000,FGLN-1001
      --If comma is not there , then the actual variable contains only transaction id.
      
      
      IF (p_trans_type = '50159')
      THEN
            IF (INSTR(P_TRANS_ID,',')=0)
            THEN
                  V_TRANS_ID := P_TRANS_ID;
            ELSE
                  WHILE INSTR (v_string, ',') <> 0
            LOOP
                  v_temp_txn_id := SUBSTR (v_string, 1, INSTR (v_string, ',') - 1);
                  v_string    := SUBSTR (v_string, INSTR (v_string, ',') + 1);
                  
                  -- Check if the transaction is Loaner set to loaner sales.
                  SELECT COUNT(1)
                  INTO   v_cnt
                  FROM T412_INHOUSE_TRANSACTIONS
                  WHERE C412_VOID_FL IS NULL
                  AND c412_type='50159'
                  AND C412_INHOUSE_TRANS_ID = v_temp_txn_id;
                  
                  IF v_cnt =1
                  THEN
                        V_TRANS_ID := v_temp_txn_id;
                  END IF;
                  
            END LOOP;
      END IF;
END IF;

	-- PMT-14376, Net Number by qty and lot build for Account Warehouse:Japan
	-- Fetch Acct ID for the Consignment and order transaction.
	-- 50181 - Acct Consignment
	-- 50180 - Order
	

	IF (p_warehouse_type='5') THEN
				
    	BEGIN
        	
	           	IF (p_trans_type='50181') THEN	    
		
				SELECT C704_ACCOUNT_ID INTO V_REF_ID FROM T504_CONSIGNMENT
				WHERE C504_CONSIGNMENT_ID=V_TRANS_ID;
			
				END IF;
				
				IF (p_trans_type='50180') THEN
		
				SELECT  C704_ACCOUNT_ID INTO V_REF_ID FROM T501_ORDER
				WHERE C501_ORDER_ID=V_TRANS_ID;
				
				END IF;
				
				IF (p_trans_type='3304') THEN
		
				SELECT  C704_ACCOUNT_ID, C506_TYPE INTO V_REF_ID, V_RETURN_TYPE FROM T506_RETURNS
				WHERE C506_RMA_ID=V_TRANS_ID;
				
								
	
				END IF;
		
           END;
 
     BEGIN
        -- Getting Location ID, Location Type for the CN id
         SELECT t5052.c5052_location_id, T5052.c901_location_type
           INTO v_location_id, v_location_type
           FROM T5052_LOCATION_MASTER t5052
          WHERE C5052_REF_ID           = v_ref_id
            AND C5052_VOID_FL         IS NULL
            AND c5051_inv_warehouse_id = p_warehouse_type;
        -- Checking the location id
    EXCEPTION
        -- raise App error
    WHEN no_data_found THEN
        v_location_id   := NULL;
        v_location_type := NULL;
    END;
    
  IF(v_location_id is not null)  THEN
  
    IF (p_trans_type='3304') THEN
    
  	  v_trans_type:=V_RETURN_TYPE;
    
    ELSE
    
    v_trans_type:=p_trans_type;
    
   END IF;
   
    BEGIN
         SELECT c5070_set_lot_master_id
           INTO v_set_lot_master_id
           FROM T5070_SET_LOT_MASTER
          WHERE C5052_LOCATION_ID  = v_location_id
            AND c901_location_type = v_location_type
            AND C901_TXN_ID        = v_ref_id
            AND C5070_VOID_FL     IS NULL;
    EXCEPTION
    WHEN no_data_found THEN
        v_set_lot_master_id := NULL;
    END;
    
    IF (v_set_lot_master_id IS NULL) THEN
    
         SELECT TO_CHAR (S5070_SET_LOT_MASTER.NEXTVAL)
           INTO v_set_lot_master_id
           FROM DUAL;
           
         INSERT
           INTO t5070_SET_LOT_master
            (
                C5070_SET_LOT_MASTER_ID, C901_TXN_ID, C901_TXN_TYPE
              , C5052_LOCATION_ID, C901_LOCATION_TYPE, C5070_VOID_FL
              , C5070_LAST_UPDATED_BY, C5070_LAST_UPDATED_DATE
            )
            VALUES
            (
                v_set_lot_master_id, v_ref_id, v_trans_type
              , v_location_id, v_location_type, NULL
              , p_user_id, CURRENT_DATE
            ) ;
    END IF;
      
   -- The below block will execute during Acct Item consignment Shipout, Returns Credit - Only for Acct.
   
    IF (v_trans_type='50181' OR v_trans_type='3304') THEN
    
        FOR inv_loc_parts IN inv_loc
        LOOP
            v_txn_qty        := inv_loc_parts.QTY;
            v_part_number_id := inv_loc_parts.c205_part_number_id;
            v_control_num    := inv_loc_parts.C5054_TXN_CONTROL_NUMBER;
            v_txn_type       := inv_loc_parts.C901_TXN_TYPE;
            v_type			 := inv_loc_parts.C901_TYPE;
            --to update qty bsaed on the v_action
             SELECT DECODE (v_action, 4302, - 1, 1) INTO v_insert_sign FROM DUAL;

             UPDATE T5071_SET_PART_QTY
             
                SET c5071_curr_qty            = c5071_curr_qty + (v_insert_sign * v_txn_qty), c5071_last_updated_txn_id = V_TRANS_ID,
                c5053_last_txn_control_number = v_control_num, c5053_last_updated_by = p_user_id,
                c5053_last_updated_date       = CURRENT_DATE ,
                C901_TXN_TYPE				  = v_txn_type ,
                C901_TYPE                     = v_type
                WHERE c205_part_number_id       = v_part_number_id
                AND c5070_set_lot_master_id   = v_set_lot_master_id
                AND c5071_void_fl IS NULL;
                
            IF (SQL%ROWCOUNT                  = 0) THEN
                 SELECT TO_CHAR (S5071_SET_PART_QTY_ID.NEXTVAL)
                   INTO V_SET_PART_QTY_ID
                   FROM DUAL;
                 INSERT
                   INTO T5071_SET_PART_QTY
                    (
                        C5071_SET_PART_QTY_ID, C5070_SET_LOT_MASTER_ID, C205_PART_NUMBER_ID
                      , C5071_CURR_QTY, C5053_LAST_TXN_CONTROL_NUMBER, C5071_LAST_UPDATED_TXN_ID
                      , C5053_LAST_UPDATED_BY, C5053_LAST_UPDATED_DATE, C901_TXN_TYPE, C901_TYPE
                    )
                    VALUES
                    (
                        v_set_part_qty_id, v_set_lot_master_id, v_part_number_id
                      , v_txn_qty, v_control_num, V_TRANS_ID
                      , p_user_id, CURRENT_DATE,v_txn_type,v_type
                    ) ;
            END IF;
             UPDATE t5054_inv_location_log
            SET c5054_lot_status_fl           = 'Y'
              WHERE c5054_inv_location_log_id = inv_loc_parts.c5054_inv_location_log_id;
        END LOOP;
    END IF;
    

  -- Checking the Order Type (Bill only consignment or Bill & Ship or Bill only Loaner)
      IF (v_trans_type = '50180') THEN
       BEGIN
       v_insert_action := 4302; -- 4302 Minus, negative qty for BOSC order type 
       
         SELECT NVL (c901_order_type, 2521),c501_parent_order_id
           INTO v_order_type,v_parent_order_id
           FROM t501_order
          WHERE c501_order_id = V_TRANS_ID;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_order_type := NULL;
        v_parent_order_id := NULL;
    END;
    END IF;

    --if order is sales adjustment,to get the parent order type from order table.
    IF (v_order_type = '2529' AND v_parent_order_id is not NULL) THEN -- Sales adjustment
     BEGIN
     	v_insert_action:= 4301;-- 4301 Plus , Positive qty for sales adjustment orders
     	
         SELECT c901_order_type
           INTO v_parent_order_type
           FROM t501_order
          WHERE c501_order_id = v_parent_order_id
          AND c501_void_fl IS NULL;
    EXCEPTION
    WHEN OTHERS THEN 
        v_parent_order_type := NULL;
    END;
    END IF;
   
       --if order is duplicate,to get the parent order type from order attribute table.
    IF (v_order_type = '2522' AND v_parent_order_id is not NULL) THEN  --duplicate order
     BEGIN
     	 v_action:= 4301; -- 4301 Plus , Positive qty for duplicate orders
     	 v_insert_action:= 4301;-- 4301 Plus , Positive qty for duplicate orders
     	 
         SELECT c501a_attribute_value
           INTO v_parent_order_type
           FROM t501a_order_attribute
          WHERE c501_order_id = v_parent_order_id
          AND c901_attribute_type ='101720'--Original Order Type
          AND c501a_void_fl is null
          AND ROWNUM=1;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_parent_order_type := NULL;
    END;
    END IF;

    -- The below block will execute during Order: Bill only Sales consignment - Only for Acct.

     IF (v_order_type        = '2532' OR v_parent_order_type = '2532') THEN
    
     SELECT DECODE (v_action, 4302, - 1, 4301, 1,  1) INTO v_sign FROM DUAL;
     SELECT DECODE (v_insert_action, 4302, - 1, 4301, 1,  1) INTO v_insert_sign FROM DUAL;
     
        FOR usage_part_lot IN usage_lot
        LOOP
            v_txn_qty        := usage_part_lot.c502b_item_qty;
            v_part_number_id := usage_part_lot.c205_part_number_id;
            v_control_num    := usage_part_lot.c502b_usage_lot_num;
            v_txn_type       := '50180';
            v_type	     := '4000337';
            
             UPDATE T5071_SET_PART_QTY
            SET c5071_curr_qty                = c5071_curr_qty - (v_sign * v_txn_qty), c5071_last_updated_txn_id = V_TRANS_ID,
                c5053_last_txn_control_number = v_control_num, c5053_last_updated_by = p_user_id,
                c5053_last_updated_date       = CURRENT_DATE,
                C901_TXN_TYPE				  = v_txn_type,
                C901_TYPE                     = v_type
              WHERE c205_part_number_id       = v_part_number_id
                AND c5070_set_lot_master_id   = v_set_lot_master_id
                AND c5071_void_fl IS NULL;
                
            IF (SQL%ROWCOUNT                  = 0) THEN
                 SELECT TO_CHAR (S5071_SET_PART_QTY_ID.NEXTVAL)
                   INTO V_SET_PART_QTY_ID
                   FROM DUAL;
                 INSERT
                   INTO T5071_SET_PART_QTY
                    (
                        C5071_SET_PART_QTY_ID, C5070_SET_LOT_MASTER_ID, C205_PART_NUMBER_ID
                      , C5071_CURR_QTY, C5053_LAST_TXN_CONTROL_NUMBER, C5071_LAST_UPDATED_TXN_ID
                      , C5053_LAST_UPDATED_BY, C5053_LAST_UPDATED_DATE, C901_TXN_TYPE, C901_TYPE
                    )
                    VALUES
                    (
                        v_set_part_qty_id, v_set_lot_master_id, v_part_number_id
                      , v_insert_sign * v_txn_qty, v_control_num, V_TRANS_ID
                      , p_user_id, CURRENT_DATE, v_txn_type, v_type
                    ) ;
            END IF;
            
           
             UPDATE t5054_inv_location_log
            SET c5054_lot_status_fl = 'Y'
              WHERE c5054_txn_id    = V_TRANS_ID;
        END LOOP;
    END IF;
  END IF;
END IF; 
  
  -- Net Number by qty and lot build only handled for Loaner Warehouse
  -- ATEC-310
  IF (p_warehouse_type='6') THEN
			
			BEGIN
			SELECT c1900_company_id INTO v_company_id
				FROM t504_consignment
			WHERE c504_consignment_id = V_TRANS_ID AND c504_void_fl IS NULL;
			EXCEPTION
				WHEN no_data_found THEN
					v_company_id     := NULL;					
			END;
			BEGIN
             SELECT c901_rule_type INTO v_rule_value
			   FROM t906_rules
			  WHERE c906_rule_id = 'TRANTYPE'
			    AND c906_rule_grp_id = 'SKIPLOTTRACK'
			    AND c1900_company_id = v_company_id
				AND c906_void_fl IS NULL;
			EXCEPTION
				WHEN no_data_found THEN
					v_rule_value     := NULL;					
			END;
			    
		IF(v_rule_value = p_trans_type) THEN
			RETURN;
		END IF;
	    
  BEGIN
        BEGIN
            v_ref_id     := P_REF_ID;
            v_trans_type := p_trans_type;
            IF (P_REF_ID IS NULL OR p_trans_type IS NULL) THEN
                 SELECT c412_ref_id, c412_type
                   INTO v_ref_id, v_trans_type
                   FROM t412_inhouse_transactions
                  WHERE c412_inhouse_trans_id = V_TRANS_ID;
            END IF;
        EXCEPTION
        WHEN no_data_found THEN
            v_ref_id     := NULL;
            v_trans_type := NULL;
        END;
        -- Getting Location ID, Location Type for the CN id
         SELECT t5052.c5052_location_id, T5052.c901_location_type
           INTO v_location_id, v_location_type
           FROM T5052_LOCATION_MASTER t5052
          WHERE C5052_REF_ID           = v_ref_id
            AND C5052_VOID_FL         IS NULL
            AND c5051_inv_warehouse_id = p_warehouse_type;
        -- Checking the location id
    EXCEPTION
    WHEN no_data_found THEN
        -- raise App error
        v_location_id   := NULL;
        v_location_type := NULL;
    END;
   
  IF(v_location_id is not null)  THEN
    BEGIN
         SELECT c5070_set_lot_master_id
           INTO v_set_lot_master_id
           FROM T5070_SET_LOT_MASTER
          WHERE C5052_LOCATION_ID  = v_location_id
            AND c901_location_type = v_location_type
            AND C901_TXN_ID        = v_ref_id
            AND C5070_VOID_FL     IS NULL;
    EXCEPTION
    WHEN no_data_found THEN
        v_set_lot_master_id := NULL;
    END;
    IF (v_set_lot_master_id IS NULL) THEN
         SELECT TO_CHAR (S5070_SET_LOT_MASTER.NEXTVAL)
           INTO v_set_lot_master_id
           FROM DUAL;
         INSERT
           INTO t5070_SET_LOT_master
            (
                C5070_SET_LOT_MASTER_ID, C901_TXN_ID, C901_TXN_TYPE
              , C5052_LOCATION_ID, C901_LOCATION_TYPE, C5070_VOID_FL
              , C5070_LAST_UPDATED_BY, C5070_LAST_UPDATED_DATE
            )
            VALUES
            (
                v_set_lot_master_id, v_ref_id, v_trans_type
              , v_location_id, v_location_type, NULL
              , p_user_id, CURRENT_DATE
            ) ;
    END IF;
    
    FOR inv_loc_parts IN inv_loc
    
    LOOP
    
         UPDATE T5071_SET_PART_QTY
        SET c5071_curr_qty              = c5071_curr_qty + inv_loc_parts.QTY
        , c5071_last_updated_txn_id     =  inv_loc_parts.c5054_txn_id
        , c5053_last_txn_control_number = inv_loc_parts.C5054_txn_control_number
        , c5053_last_updated_by         = p_user_id
        , c5053_last_updated_date       = CURRENT_DATE
        , c901_txn_type				    = inv_loc_parts.c901_txn_type
        , c901_type                     = inv_loc_parts.c901_type
        
        WHERE c205_part_number_id     = inv_loc_parts.c205_part_number_id
        AND c5070_set_lot_master_id = v_set_lot_master_id
        AND c5071_void_fl IS NULL;
        
        IF (SQL%ROWCOUNT                = 0) THEN
        
             SELECT TO_CHAR (S5071_SET_PART_QTY_ID.NEXTVAL)
               INTO V_SET_PART_QTY_ID
               FROM DUAL;
               
             INSERT
               INTO T5071_SET_PART_QTY
                (
                    C5071_SET_PART_QTY_ID, C5070_SET_LOT_MASTER_ID, C205_PART_NUMBER_ID
                  , C5071_CURR_QTY, C5053_LAST_TXN_CONTROL_NUMBER, C5071_LAST_UPDATED_TXN_ID
                  , C5053_LAST_UPDATED_BY, C5053_LAST_UPDATED_DATE, C901_TXN_TYPE, C901_TYPE
                )
                VALUES
                (
                    v_set_part_qty_id, v_set_lot_master_id, inv_loc_parts.C205_part_number_id
                  , inv_loc_parts.QTY, inv_loc_parts.C5054_TXN_CONTROL_NUMBER, inv_loc_parts.QTY
                  , p_user_id, CURRENT_DATE, inv_loc_parts.c901_txn_type,inv_loc_parts.c901_type
                ) ;
        END IF;
           
         UPDATE t5054_inv_location_log
         SET c5054_lot_status_fl           = 'Y'
         where C5054_INV_LOCATION_LOG_ID = INV_LOC_PARTS.C5054_INV_LOCATION_LOG_ID;
         
    END LOOP;
  END IF; 
 END IF;
  
  
  
END GM_PKG_SET_LOT_UPDATE;

/**********************************************************************
* Description : Procedure to store the Set Part by Qty log details
* Author    : Gomathi
**********************************************************************/
PROCEDURE GM_SAV_SET_PART_QTY_LOG (
        P_SET_PART_QTY_ID   IN T5071_SET_PART_QTY.C5071_SET_PART_QTY_ID%TYPE,
        P_SET_LOT_MASTER_ID IN T5070_SET_LOT_MASTER.C5070_SET_LOT_MASTER_ID%TYPE,
        P_PART_NUM          IN T5071A_SET_PART_QTY_LOG.C205_PART_NUMBER_ID%TYPE,
        p_txn_id            IN T5071a_set_part_qty_log.c7071a_txn_id%TYPE,
        P_ORG_QTY           IN T5071A_SET_PART_QTY_LOG.C7071A_ORIG_QTY%TYPE,
        P_TXN_QTY           IN T5071A_SET_PART_QTY_LOG.C7071A_TXN_QTY%TYPE,
        P_NEW_QTY           IN T5071A_SET_PART_QTY_LOG.C7071A_NEW_QTY%TYPE,
        P_CONTROL_NUMBER    IN T5054_INV_LOCATION_LOG.C5054_TXN_CONTROL_NUMBER%TYPE,
        P_CREATED_DATE      IN T5071A_SET_PART_QTY_LOG.C7071A_CREATED_DATE%TYPE,
        p_created_by        IN T5071a_set_part_qty_log.c7071a_created_by%TYPE,
        p_txn_type			IN T5071_SET_PART_QTY.c901_txn_type%TYPE,
        p_type				IN T5071_SET_PART_QTY.c901_type%TYPE)
AS
    V_TRANS_TYPE t5071a_set_part_qty_log.c901_txn_type%TYPE;
BEGIN
    
     INSERT
       INTO t5071a_set_part_qty_log
        (
            C5071A_SET_PART_QTY_LOG_ID, C5071_SET_PART_QTY_ID, C5070_SET_LOT_MASTER_ID
          , C205_PART_NUMBER_ID, C7071A_TXN_QTY, C7071A_ORIG_QTY
          , C7071A_TXN_CONTROL_NUMBER, C7071A_NEW_QTY, C7071A_TXN_ID
          , C901_TXN_TYPE, C7071A_CREATED_BY, C7071A_CREATED_DATE,c901_type
        )
        VALUES
        (
            s5071a_set_part_qty_log.NEXTVAL, p_set_part_qty_id, p_set_lot_master_id
          , p_part_num, p_txn_qty, p_org_qty
          , p_control_number, p_new_qty, p_txn_id
          , p_txn_type , p_created_by, p_created_date, p_type
        ) ;
END gm_sav_set_part_qty_log;
/**********************************************************************
* Description : Procedure to store the Set Part by Lot details
* Author    : Gomathi
**********************************************************************/
PROCEDURE gm_sav_set_part_lot_qty
    (
        p_set_part_qty_id   IN t5071_set_part_qty.c5071_set_part_qty_id%TYPE,
        p_set_lot_master_id IN t5070_set_lot_master.c5070_set_lot_master_id%TYPE,
        p_part_num          IN t5072_SET_PART_LOT_QTY.c205_part_number_id%TYPE,
        p_txn_qty           IN t5072_SET_PART_LOT_QTY.c5072_curr_qty%TYPE,
        p_txn_id            IN t5072_SET_PART_LOT_QTY.c5072_last_updated_txn_id%TYPE,
        p_control_number    IN t5072_SET_PART_LOT_QTY.c5072_CONTROL_NUMBER%TYPE,
        p_last_updated_date IN t5072_SET_PART_LOT_QTY.c5072_last_updated_date%TYPE,
        p_last_updated_by   IN t5072_SET_PART_LOT_QTY.c5072_last_updated_by%TYPE,
        p_txn_type			IN T5071_SET_PART_QTY.c901_txn_type%TYPE,
        p_type				IN T5071_SET_PART_QTY.c901_type%TYPE
    )
AS
BEGIN
     UPDATE t5072_set_part_lot_qty
    SET c5072_CURR_QTY        = c5072_CURR_QTY + p_txn_qty,
    c5072_LAST_UPDATED_TXN_ID = p_txn_id,
    c5072_LAST_UPDATED_BY     = p_last_updated_by, 
    c5072_LAST_UPDATED_DATE   = p_last_updated_date,
    c901_txn_type		      = p_txn_type,
    c901_type                 = p_type
      WHERE C5072_CONTROL_NUMBER                   = p_control_number
        AND C5071_SET_PART_QTY_ID                  = p_set_part_qty_id
        AND C5070_SET_LOT_MASTER_ID                = p_set_lot_master_id
        AND C205_PART_NUMBER_ID                    = p_part_num
        AND c5072_void_fl IS NULL
        ;
    IF (SQL%ROWCOUNT                               = 0) THEN
         INSERT
           INTO t5072_set_part_lot_qty
            (
                C5072_SET_PART_LOT_QTY_ID, C5071_SET_PART_QTY_ID, C5070_SET_LOT_MASTER_ID
              , C205_PART_NUMBER_ID, C5072_CONTROL_NUMBER, C5072_CURR_QTY
              , C5072_LAST_UPDATED_TXN_ID, C5072_LAST_UPDATED_BY, C5072_LAST_UPDATED_DATE ,c901_txn_type , c901_type
            )
            VALUES
            (
                s5072_set_part_lot_qty.NEXTVAL, p_set_part_qty_id, p_set_lot_master_id
              , p_part_num, p_control_number, p_txn_qty
              , p_txn_id, p_last_updated_by, p_last_updated_date, p_txn_type, p_type
            ) ;
    END IF;
END gm_sav_set_part_lot_qty;
/**********************************************************************
* Description : Procedure to store the Set Part Lot Qty log details
* Author    : Gomathi
**********************************************************************/
PROCEDURE gm_sav_set_part_lot_qty_log
    (
        p_set_part_lot_qty_id IN T5072A_SET_PART_LOT_QTY_LOG.C5072_SET_PART_LOT_QTY_ID%TYPE,
        p_set_lot_master_id   IN T5070_SET_lot_master.C5070_SET_LOT_MASTER_ID%TYPE,
        p_set_part_qty_id     IN T5072A_SET_PART_LOT_QTY_LOG.C5071_SET_PART_QTY_ID%TYPE,
        p_part_num            IN T5072A_SET_PART_LOT_QTY_LOG.C205_PART_NUMBER_ID%TYPE,
        p_control_number      IN T5072A_SET_PART_LOT_QTY_LOG.C5072_CONTROL_NUMBER%TYPE,
        p_txn_qty             IN T5072A_SET_PART_LOT_QTY_LOG.C5072A_TXN_QTY%TYPE,
        p_new_qty             IN T5072A_SET_PART_LOT_QTY_LOG.C5072A_NEW_QTY%TYPE,
        p_orig_qty            IN T5072A_SET_PART_LOT_QTY_LOG.C5072A_ORIG_QTY%TYPE,
        p_created_date        IN T5072A_SET_PART_LOT_QTY_LOG.C5072A_created_date%TYPE,
        p_created_by          IN T5072A_SET_PART_LOT_QTY_LOG.C5072A_created_by%TYPE,
        p_txn_id			  IN T5072A_SET_PART_LOT_QTY_LOG.C5072A_TXN_ID%TYPE,
        p_txn_type			  IN T5071_SET_PART_QTY.c901_txn_type%TYPE,
        p_type				  IN T5071_SET_PART_QTY.c901_type%TYPE
    )
AS
    V_TRANS_TYPE t5072A_SET_PART_LOT_QTY_LOG.C901_TXN_TYPE%TYPE;
BEGIN
    
     INSERT
       INTO t5072a_set_part_lot_qty_log
        (
            C5072A_SET_PART_LOT_QTY_LOG_ID, C5072_SET_PART_LOT_QTY_ID, C5070_SET_LOT_MASTER_ID
          , C5071_SET_PART_QTY_ID, C205_PART_NUMBER_ID, c5072_CONTROL_NUMBER
          , C901_TXN_TYPE, C5072A_TXN_QTY, C5072A_NEW_QTY
          , C5072A_ORIG_QTY, C5072A_CREATED_BY, C5072A_CREATED_DATE, C5072A_TXN_ID,  c901_type
        )
        VALUES
        (
            s5072a_set_part_lot_qty_log.NEXTVAL, p_set_part_lot_qty_id, p_set_lot_master_id
          , p_set_part_qty_id, p_part_num, p_control_number
          , p_txn_type, p_txn_qty, p_new_qty
          , p_orig_qty, p_created_by, p_created_date, p_txn_id ,p_type
        ) ;
END GM_SAV_SET_PART_LOT_QTY_LOG;


/**********************************************************************
* Description : Procedure to replace the lot for loaner set
* Author    : DineshR
**********************************************************************/
PROCEDURE GM_PKG_UPDATE_LOANER_LOT
    (
        p_trans_id   IN t5072_set_part_lot_qty.c5072_LAST_UPDATED_TXN_ID%TYPE,
        p_user_id    IN t101_user.c101_user_id%TYPE,
        p_input_str  IN VARCHAR2
    )
AS 
    v_strlen      NUMBER          := NVL (LENGTH (P_input_str), 0) ;
    v_string      VARCHAR2 (4000) := p_input_str;
    v_substring   VARCHAR2 (4000) ;
    v_location_id NUMBER;
    v_control_num_new t5010_tag.c5010_control_number%TYPE := NULL;
    v_control_num_old t5010_tag.c5010_control_number%TYPE := NULL;
    v_partnum t5010_tag.c205_part_number_id%TYPE      := NULL;
    v_recon_qty             NUMBER                                      := '0';
    v_set_lot_master_id t5070_set_lot_master.c5070_set_lot_master_id%TYPE;
    v_location_type   t5052_location_master.c901_location_type%TYPE;
	v_type t504_consignment.c504_type%TYPE;
	v_ware_house      VARCHAR2 (20) ;
    v_ware_house_type VARCHAR2 (20) ;
    v_set_part_qty_id T5071_SET_PART_QTY.C5071_SET_PART_QTY_ID%TYPE;
    
    v_item_qty t505_item_consignment.c505_item_qty%TYPE  := '0';
    v_curr_qty T5072_SET_PART_LOT_QTY.c5072_CURR_QTY%TYPE := '0';

	v_invalid_partstr    CLOB;
	
	v_txn_type 		   t5071_set_part_qty.C901_TXN_TYPE%TYPE:=NULL;
    v_set_type	   t5071_set_part_qty.C901_TYPE%TYPE:=NULL;
	
BEGIN
	-- Getting consignment type to get warehouse
	SELECT c504_type
	  INTO v_type
	FROM t504_consignment
	WHERE c504_consignment_id = p_trans_id
	AND c504_void_fl         IS NULL ;	
	
	v_ware_house      := get_rule_value (v_type,'RULEWH') ;
	v_ware_house_type := get_rule_value (v_type,'RULEWHTYPE') ;
	
		
    IF v_ware_house   IS NULL OR v_ware_house_type IS NULL THEN
     GM_RAISE_APPLICATION_ERROR ('-20999', '210', '') ;
    END IF;
	--Get Location ID,Type to get the set lot master ID
	BEGIN		
       SELECT t5052.c5052_location_id, T5052.c901_location_type
       INTO v_location_id, v_location_type
       FROM T5052_LOCATION_MASTER t5052
       WHERE C5052_REF_ID           = p_trans_id
        AND C5052_VOID_FL         IS NULL
        AND c5051_inv_warehouse_id = v_ware_house;
	
    EXCEPTION
		WHEN no_data_found THEN
		raise_application_error ('-20500', 'Cannot update lot for CN: '|| p_trans_id) ;
	END;
	BEGIN
     SELECT c5070_set_lot_master_id
       INTO v_set_lot_master_id
       FROM T5070_SET_LOT_MASTER
      WHERE C5052_LOCATION_ID  = v_location_id
        AND c901_location_type = v_location_type
        AND C901_TXN_ID        = p_trans_id
        AND C5070_VOID_FL     IS NULL;
    EXCEPTION
    WHEN no_data_found THEN
      raise_application_error ('-20501', 'Cannot update lot for CN: '|| p_trans_id) ;
    END;

    WHILE INSTR (v_string, '|') <> 0
    LOOP
        v_substring       := SUBSTR (v_string, 1, INSTR (v_string, '|')       - 1) ;
        v_string          := SUBSTR (v_string, INSTR (v_string, '|')          + 1) ;
        v_partnum         := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
        v_substring       := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        v_recon_qty       := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
        v_substring       := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        v_control_num_old := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
        v_substring       := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
		v_control_num_new := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
        v_substring       := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
      
 		--IF the Current Qty is greater than Reconfigure Qty ,then reduce the Current Qty for the old lot.
 		--Else void old lot line item and insert the new lot line item
 		begin
 		SELECT c5072_CURR_QTY
 		  INTO v_curr_qty
  		  FROM T5072_SET_PART_LOT_QTY
 		 WHERE c5070_set_lot_master_id = v_set_lot_master_id
	     AND C205_PART_NUMBER_ID     = v_partnum
	     AND C5072_CONTROL_NUMBER    = v_control_num_old
	     AND c5072_void_fl IS NULL;
	     EXCEPTION
	    WHEN no_data_found THEN
			v_curr_qty := 0;
	    END;

	   IF v_control_num_old != '0' and v_control_num_new != '0' THEN    --to avoid positive Qty to removed for Remove 
	     IF (v_curr_qty > v_recon_qty)
 		THEN
	 		UPDATE T5072_SET_PART_LOT_QTY
	           SET c5072_CURR_QTY = c5072_CURR_QTY - v_recon_qty
	         WHERE c5070_set_lot_master_id = v_set_lot_master_id
		     AND C205_PART_NUMBER_ID     = v_partnum
		     AND C5072_CONTROL_NUMBER    = v_control_num_old
		     AND c5072_void_fl IS NULL; 
 		ELSE
		 		UPDATE T5072_SET_PART_LOT_QTY
			      SET c5072_void_fl = 'Y',c5072_last_updated_by = p_user_id,c5072_last_updated_date = CURRENT_DATE
			    WHERE c5070_set_lot_master_id = v_set_lot_master_id
			     AND C205_PART_NUMBER_ID     = v_partnum
			     AND C5072_CONTROL_NUMBER    = v_control_num_old
			     AND c5072_void_fl IS NULL; 
 		END IF;     
      END IF;  

      IF (v_control_num_new is null) and (v_curr_qty < v_recon_qty)  THEN -- to remove the negative Qty 
      UPDATE T5072_SET_PART_LOT_QTY
		      SET c5072_void_fl = 'Y',c5072_last_updated_by = p_user_id,c5072_last_updated_date = CURRENT_DATE
		    WHERE c5070_set_lot_master_id = v_set_lot_master_id
		     AND C205_PART_NUMBER_ID     = v_partnum
		     AND C5072_CONTROL_NUMBER    = v_control_num_old
		     AND c5072_void_fl IS NULL; 
	  ELSIF(v_control_num_new is null)   THEN --to remove the positive qty if lot qty doesnot match set list qty
	  		IF((v_recon_qty < v_curr_qty )) THEN
	  			UPDATE T5072_SET_PART_LOT_QTY
		           SET c5072_CURR_QTY = c5072_CURR_QTY - v_recon_qty
		         WHERE c5070_set_lot_master_id = v_set_lot_master_id
			     AND C205_PART_NUMBER_ID     = v_partnum
			     AND C5072_CONTROL_NUMBER    = v_control_num_old
			     AND c5072_void_fl IS NULL; 
		     ELSIF(v_recon_qty = v_curr_qty) THEN
	  			UPDATE T5072_SET_PART_LOT_QTY
			      SET c5072_void_fl = 'Y',
			      c5072_last_updated_by = p_user_id,
			      c5072_last_updated_date = CURRENT_DATE
			    WHERE c5070_set_lot_master_id = v_set_lot_master_id
			     AND C205_PART_NUMBER_ID     = v_partnum
			     AND C5072_CONTROL_NUMBER    = v_control_num_old
			     AND c5072_void_fl IS NULL; 
	  		END IF;
      end if;
       BEGIN
	    	SELECT c5071_set_part_qty_id ,c901_type
	    	  INTO v_set_part_qty_id,v_set_type
	    	  FROM t5071_set_part_qty
			 WHERE c205_part_number_id = v_partnum
	  		   AND c5070_set_lot_master_id = v_set_lot_master_id
	  		   AND c5071_void_fl IS NULL;
	  	EXCEPTION
	    WHEN no_data_found THEN
	
	            raise_application_error ('-20502', 'cannot update lot for CN: '|| p_trans_id) ;
	    END;
	    --for adding the lot, checking the below condition
  IF v_control_num_new IS NOT NULL THEN      
    	-- To insert/update new lot on T5072_SET_PART_LOT_QTY
    	gm_pkg_set_lot_track.gm_sav_set_part_lot_qty(v_set_part_qty_id ,v_set_lot_master_id,v_partnum,v_recon_qty,p_trans_id,v_control_num_new,CURRENT_DATE,p_user_id,'106450',v_set_type);
	   UPDATE T5071_SET_PART_QTY 
	    SET c5071_last_updated_txn_id =  p_trans_id,C901_TXN_TYPE = '106450'
	      , c5053_last_txn_control_number = v_control_num_new
	 	  , c5053_last_updated_by  = p_user_id, c5053_last_updated_date = CURRENT_DATE
	  WHERE c205_part_number_id     = v_partnum
		AND c5070_set_lot_master_id = v_set_lot_master_id
		AND c5071_void_fl IS NULL;
 END IF;
	END LOOP;  
	GM_PKG_VALIDATE_LOT(p_trans_id,v_set_lot_master_id);--this procedure is used for validation
END GM_PKG_UPDATE_LOANER_LOT;
/********************************************************************
 * Description : This is procedure is used for validation of the lot numbers
 * Author: SSharmila
 ***********************************************************************/

PROCEDURE GM_PKG_VALIDATE_LOT (
 		p_trans_id  	        IN  t5072_set_part_lot_qty.c5072_last_updated_txn_id%TYPE,
 		p_set_lot_master_id 	IN  t5070_set_lot_master.c5070_set_lot_master_id%TYPE
 		)
AS
v_qty_in_set 	NUMBER    := 0;
v_err_cnt		NUMBER	  := 0;
v_err			varchar2(4000);
CURSOR cur_lot_validate IS
	select c205_part_number_id,sum(c5072_curr_qty)c5072_curr_qty
	FROM T5072_SET_PART_LOT_QTY
	where c5070_set_lot_master_id = p_set_lot_master_id
	     --AND c5072_last_updated_txn_id = p_trans_id--since this column always updating the new txnid so commenting this PMT-42614
		 AND c5072_void_fl IS NULL
		  group by c205_part_number_id;
BEGIN
	FOR chng_lot IN cur_lot_validate
        LOOP
        -- getting Qty in set
	         v_qty_in_set := get_qty_in_set(p_trans_id,chng_lot.c205_part_number_id);

   --Lot Qty is greater than  Qty in Set for the part 	
     IF v_qty_in_set < (chng_lot.c5072_curr_qty) THEN
	  	--raise_application_error ('-20502', 'cannot Add lot for CN: '|| p_trans_id) ;
	  	v_err_cnt := v_err_cnt+1;
	  	v_err := v_err || chng_lot.c205_part_number_id || ',';
	  END IF;
	----Qty in Set is greater than Lot Qty for the part 	 
	  IF v_qty_in_set > (chng_lot.c5072_curr_qty) THEN
	  	v_err_cnt := v_err_cnt+1;
	  	v_err := v_err || chng_lot.c205_part_number_id || ',';
	  END IF;
	  
    END LOOP; 
    
    IF v_err_cnt >0 THEN
    	raise_application_error ('-20999' , v_err || ' Entered Qty for the PartNumbers doesnot match the Set list qty ') ;
    END IF;
END GM_PKG_VALIDATE_LOT;

PROCEDURE GM_PKG_FETCH_LOANER_SALES (
        p_trans_id  	          IN t5072_set_part_lot_qty.c5072_last_updated_txn_id%TYPE,
        p_loaner_sales_txn_id     OUT t412_inhouse_transactions.c412_inhouse_trans_id%TYPE,
        p_loaner_sales_txn_type   OUT t412_inhouse_transactions.c412_type%TYPE
        )
AS
    v_loaner_txn_id t412_inhouse_transactions.c504a_loaner_transaction_id%TYPE;
    
BEGIN
	-- Fetching Loaner Transaction ID from the given FGLN 
	BEGIN
		
     SELECT c504a_loaner_transaction_id
       INTO v_loaner_txn_id
       FROM t412_inhouse_transactions
      WHERE c412_inhouse_trans_id = p_trans_id
        AND c412_void_fl         IS NULL;
        
		EXCEPTION WHEN OTHERS THEN
		v_loaner_txn_id := NULL;
	END;    

	BEGIN

	-- Query to fetch the Loaner to Sales transaction id (GM-LN) 
	
     SELECT t412.c412_inhouse_trans_id, t412.c412_type
       INTO p_loaner_sales_txn_id, p_loaner_sales_txn_type
       FROM t412_inhouse_transactions t412
      WHERE
        t412.C412_TYPE                   = '50159' -- 50159 -> Loaner Set to Loaner Sales
        AND t412.c412_void_fl is null
        AND t412.c504a_loaner_transaction_id = v_loaner_txn_id;
        
	EXCEPTION WHEN OTHERS THEN
	 p_loaner_sales_txn_id:= NULL;
	 p_loaner_sales_txn_type:= NULL;
	
	END;
	
 END GM_PKG_FETCH_LOANER_SALES;


END gm_pkg_set_lot_track;
/
