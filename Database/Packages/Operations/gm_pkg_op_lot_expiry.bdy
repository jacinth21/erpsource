CREATE OR REPLACE
PACKAGE BODY GM_PKG_OP_LOT_EXPIRY
IS
/********************************************************************
* Description : Procedure to save expiry date
* Author	  : gpalani			
*************************************************************************/
	PROCEDURE GM_SAV_EXP_DATE(
		    p_company_id	IN t2550_part_control_number.c1900_company_id%TYPE,
   	   	   	p_user_id       IN t2550_part_control_number.C2550_LAST_UPDATED_BY%TYPE,
			p_inputstr 	    IN CLOB
	)
	AS
	 v_string    CLOB := p_inputstr;
	 v_substring VARCHAR2 (4000) ;
	 v_part_substring VARCHAR2(4000);
	 v_part_num_substr VARCHAR2(4000);
	 v_expirydate_substr VARCHAR2(4000);
	 v_lot_num VARCHAR2 (4000) ;
	 v_part_num VARCHAR2 (4000) ;
	 v_expiry_date VARCHAR2 (4000) ;
	 v_err_cnt NUMBER;
	 v_err_msg VARCHAR2 (4000);
	 v_email_err_msg VARCHAR2 (4000);
	 v_email VARCHAR2 (4000);
	 v_email_flag VARCHAR2 (4000) :='N';
	 v_email_sub VARCHAR2 (4000);
     v_expiry_dt DATE;
  	 v_email_null_val VARCHAR2 (4000);

	     
     
	BEGIN
		WHILE INSTR (v_string, '|') <> 0
		
        	LOOP
        		  
        		 v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
            	 v_string    := substr (v_string, instr (v_string, '|')    + 1) ;
            	 WHILE INSTR (v_substring, ',') <> 0
            	 
            	   LOOP
            	 		 v_part_num := substr (v_substring, 1, instr (v_substring, ',') - 1) ;
            	 		 v_part_num_substr  := substr (v_substring, instr (v_substring, ',')+ 1) ;	
            	 		 v_lot_num:=substr (v_part_num_substr, 1, instr (v_part_num_substr, ',') - 1) ;
            	 		 v_expirydate_substr:=substr (v_part_num_substr, instr (v_part_num_substr, ',') + 1) ;
            	 		 v_expiry_date:=substr (v_expirydate_substr, 1, instr (v_expirydate_substr, ',') - 1) ;
            	 		 
                 		  v_substring:=substr (v_expirydate_substr, instr (v_expirydate_substr, ',') + 1) ;
                		  
                   		  v_expiry_date := to_date(v_expiry_date, 'mm/dd/yyyy');
                  
		-- Check if there is any null value in the data sent from Excel
			IF(v_part_num IS NULL OR v_lot_num IS NULL OR v_expiry_date IS NULL) THEN
			
			  			SELECT get_rule_value('LOT_EMAIL_SUB','LOT_EXPIRY') 
			            INTO v_email_sub FROM DUAL;
			
			       -- Fetching  Email address of the user who uploaded the file
			            SELECT GET_USER_EMAIL_ADDRESS(p_user_id) 
			            into v_email from dual;
			       -- Call the email procedure to send out the email notification.
			       
			           SELECT get_rule_value('LOT_EMAIL_NULL_VAL','LOT_EXPIRY') 
			            INTO v_email_null_val FROM DUAL;
			       
			         	gm_com_send_html_email(v_email,v_email_sub,NULL,v_email_null_val);
			END IF;
					-- Calling the validation procedure
            	    gm_pkg_op_lot_expiry.gm_sav_exp_date_validation(v_part_num,v_lot_num,v_expiry_date,v_err_cnt,v_err_msg);
          	   
					-- The below procedure will be called only if the validation is success for that loop data
            	 	IF v_err_cnt <1 THEN
            	 		gm_pkg_op_lot_expiry.gm_sav_exp_date_part_num(p_user_id,to_number(p_company_id),v_lot_num,v_part_num,v_expiry_date);
     				
					-- If the error count is greater than 0 set the email error messages.
     				ELSIF v_err_cnt>0 THEN
     				   v_email_err_msg:=v_email_err_msg||v_err_msg;
     				   v_email_flag:='Y';
     				   v_err_cnt:=0;
 				   
     				END IF;
     END LOOP;
   END LOOP;

    
		IF(v_email_flag='Y') THEN
          
			       -- Fetching  Email Subject for failure scenarios
			       
			            SELECT get_rule_value('LOT_EMAIL_SUB','LOT_EXPIRY') 
			            INTO v_email_sub FROM DUAL;
			
			       -- Fetching  Email address of the user who uploaded the file
			            SELECT GET_USER_EMAIL_ADDRESS(p_user_id) 
			            into v_email from dual;
			       -- Call the email procedure to send out the email notification.
			       
			         	gm_com_send_html_email(v_email,v_email_sub,NULL,v_email_err_msg);
			         	v_email_flag:='N';
          	
        ELSE 
          -- Fetching email details for success scenario
	          SELECT GET_USER_EMAIL_ADDRESS(p_user_id) 
	            INTO v_email FROM dual;
	            
	          SELECT get_rule_value('LOT_EMAIL_SUB_SUCCESS','LOT_EXPIRY') 
	            INTO v_email_sub FROM DUAL;
	            
	          SELECT get_rule_value('LOT_EMAIL_MSG','LOT_EXPIRY') 
	            INTO v_err_msg FROM DUAL;
	                        
				            
	        -- Success Email procedure call
		   gm_com_send_html_email(v_email,v_email_sub,NULL,v_err_msg);
	   END IF;
		
	END GM_SAV_EXP_DATE;
	
	
/********************************************************************
* Description : Procedure to validate the Lot Expiry date
* Author	  : gpalani			
*************************************************************************/
PROCEDURE GM_SAV_EXP_DATE_VALIDATION(
		    p_part_num		IN  t2550_part_control_number.c205_part_number_id%TYPE,
   	   	   	p_lot_num       IN  t2550_part_control_number.C2550_CONTROL_NUMBER%TYPE,
			p_expiry 	    IN  t2550_part_control_number.C2550_EXPIRY_DATE%TYPE,
		    p_err_count     OUT NUMBER,          
		    p_errmsg	    OUT CLOB

	)
AS
    v_lot_count  NUMBER;
    v_expiry_dte Number;
    v_err_msg      VARCHAR2 (4000);
    v_lot_err_msg  varchar2 (4000);
    v_qry1  varchar2(4000);
    v_part_cnt number;

 BEGIN
-- Part Number validation

      SELECT COUNT (1)
       INTO v_part_cnt
       FROM t205_part_number
      WHERE C205_Part_Number_Id = p_part_num
      AND c205_active_fl='Y';
      
     p_err_count:=0; 
   -- Fetch Error message from rule  
      IF(v_part_cnt<1) THEN
      
       v_err_msg:=NULL;
          SELECT get_rule_value('PART_NUM_CHK','LOT_EXPIRY') 
            INTO v_err_msg FROM DUAL;
            v_part_cnt :=1;
            v_err_msg:=v_err_msg || ':'||p_part_num||'. <br>';
            p_err_count:=v_part_cnt;
  
      END IF;

-- Part Number/Lot Number validation
     SELECT COUNT (1)
       INTO v_lot_count
       FROM T2550_PART_CONTROL_NUMBER
      WHERE C205_PART_NUMBER_ID  = p_part_num
        AND C2550_CONTROL_NUMBER = p_lot_num;
             
-- Fetch Error message from rule
        IF(v_lot_count<1) THEN
       		v_lot_err_msg:=NULL;
       		
          SELECT get_rule_value('LOT_NUM_CHK','LOT_EXPIRY') 
            INTO v_lot_err_msg FROM DUAL;
            v_lot_count := 1;
            v_lot_err_msg:=v_lot_err_msg||':'||p_lot_num||'. <br>';
            p_err_count:=p_err_count+v_lot_count; 
      END IF;
         
     
-- Append all the error messge
	p_errmsg:=v_err_msg||v_lot_err_msg;
	
END GM_SAV_EXP_DATE_VALIDATION;
	

           
/********************************************************************
* Description :  Procedure to save lot expiry date
* Author	  : gpalani			
*************************************************************************/
	PROCEDURE GM_SAV_EXP_DATE_PART_NUM (
	  	p_user_id         IN t2550_part_control_number.C2550_LAST_UPDATED_BY%TYPE
	   ,p_company_id	  IN t2550_part_control_number.c1900_company_id%TYPE 
	   ,p_lot_num		  IN t2550_part_control_number.c2550_control_number%TYPE
	   ,p_part_num        IN t2550_part_control_number.C205_PART_NUMBER_ID%TYPE
	   ,p_expiry_date	  IN t2550_part_control_number.c2550_expiry_date%TYPE
	)
	AS	
 	
  BEGIN
 		 
        UPDATE t2550_part_control_number
        SET c2550_last_updated_by = p_user_id,
        c2550_expiry_date = p_expiry_date 
        ,c2550_last_updated_date = CURRENT_DATE
        WHERE c2550_control_number = p_lot_num AND 
        c205_part_number_id = p_part_num 
        AND c1900_company_id    = p_company_id;
        
	END GM_SAV_EXP_DATE_PART_NUM;
END GM_PKG_OP_LOT_EXPIRY;
/