--@"c:\database\packages\operations\gm_pkg_op_loaner_dashboard.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_loaner_dashboard
IS

/***************************************************************
* Description : Procedure to show count on loaner dashboard
* Author    : Vinoth
****************************************************************/
	
	PROCEDURE gm_fch_loaner_overall_dash(
		p_open_request     OUT VARCHAR,	
		p_allocated_sets   OUT VARCHAR,	
	    p_pending_pick     OUT VARCHAR,
	    p_pending_putaway  OUT VARCHAR,
	    p_pending_ship     OUT VARCHAR,
	    p_open_sameday     OUT VARCHAR,	
		p_topick_sameday   OUT VARCHAR,	
	    p_toship_sameday   OUT VARCHAR
	)
	AS
	BEGIN
	gm_fch_loaner_open_request(p_open_request);  -- This procedure used to take count of loaner in open status
	gm_fch_loaner_allocated_sets(p_allocated_sets);  -- This procedure used to take count of loaner in allocated status
	gm_fch_loaner_pending_pick(p_pending_pick);  -- This procedure used to take count of loaner in pending pick status
	gm_fch_loaner_pending_putaway(p_pending_putaway);  -- This procedure used to take count of loaner in pending putaway status
	gm_fch_loaner_pending_ship(p_pending_ship);  -- This procedure used to take count of loaner in pending shipping status
	gm_fch_loaner_open_sameday(p_open_sameday);  -- This procedure used to take count of loaner in same day mode and open status
	gm_fch_loaner_topick_sameday(p_topick_sameday);  -- This procedure used to take count of loaner in same day mode and pending pick status
	gm_fch_loaner_toship_sameday(p_toship_sameday);  -- This procedure used to take count of loaner in same day mode and pending shipping status

	END gm_fch_loaner_overall_dash;
	
/***************************************************************
* Description : Procedure to take count of open request of loaner
* Author    : Vinoth
****************************************************************/
	
	PROCEDURE gm_fch_loaner_open_request(
		p_open_request OUT VARCHAR	
	)
	AS 
	
	v_company_id    T1900_COMPANY.C1900_COMPANY_ID%TYPE;
	v_plant_id      t526_product_request_detail.c5040_plant_id%TYPE;
	v_open_request  VARCHAR2(10);
	
	BEGIN
	
			SELECT get_compid_frm_cntx(), get_plantid_frm_cntx() 
			  INTO v_company_id ,v_plant_id
			  FROM DUAL;
	      
	SELECT COUNT(t526.c526_product_request_detail_id) INTO v_open_request FROM t526_product_request_detail t526, t525_product_request t525
	 WHERE c901_request_type=4127   --Product Loaner
	   AND c526_status_fl=10        -- Open status
	   AND TRUNC(t526.c526_required_date) BETWEEN TRUNC(SYSDATE-NVL(get_rule_value('OPEN','LOANERDASHDAYS'),0)) AND TRUNC(SYSDATE)
	   AND t525.c525_product_request_id = t526.c525_product_request_id  
	   AND t526.c1900_company_id = v_company_id
	   AND t526.c5040_plant_id = v_plant_id
	   AND t526.c526_void_fl is null
	   AND t525.c525_void_fl is null;
	   
	   p_open_request := v_open_request;
	   
	END gm_fch_loaner_open_request;

/***************************************************************
* Description : Procedure to take count of allocated sets of loaner
* Author    : Vinoth
****************************************************************/
	
	PROCEDURE gm_fch_loaner_allocated_sets(
		p_allocated_sets OUT VARCHAR	
	)
	AS 
	
	v_company_id      T1900_COMPANY.C1900_COMPANY_ID%TYPE;
	v_plant_id        t526_product_request_detail.c5040_plant_id%TYPE;
	v_allocated_sets  VARCHAR2(10);
	
	BEGIN
	
			SELECT get_compid_frm_cntx(), get_plantid_frm_cntx() 
			  INTO v_company_id ,v_plant_id
			  FROM DUAL;
	      
	SELECT COUNT(t526.c526_product_request_detail_id) INTO v_allocated_sets FROM t526_product_request_detail t526, t525_product_request t525
	 WHERE c901_request_type=4127   --Product Loaner
	   AND c526_status_fl=20 		--Allocated status
	   AND TRUNC(t526.c526_required_date) BETWEEN TRUNC(SYSDATE-NVL(get_rule_value('ALLOCATED','LOANERDASHDAYS'),0)) AND TRUNC(SYSDATE)
	   AND t525.c525_product_request_id = t526.c525_product_request_id  
	   AND t526.c1900_company_id = v_company_id
	   AND t526.c5040_plant_id = v_plant_id
	   AND t526.c526_void_fl is null
	   AND t525.c525_void_fl is null;
	   
	     p_allocated_sets := v_allocated_sets;
	     
	END gm_fch_loaner_allocated_sets;
	
	/***************************************************************
* Description : Procedure to take count of pending pick of loaner
* Author    : Vinoth
****************************************************************/
	
	PROCEDURE gm_fch_loaner_pending_pick(  
		p_pending_pick OUT VARCHAR	
	)
	AS 
	v_company_id   T1900_COMPANY.C1900_COMPANY_ID%TYPE;
	v_plant_id     t526_product_request_detail.c5040_plant_id%TYPE;
	v_pending_pick  VARCHAR2(10);
	BEGIN
			SELECT get_compid_frm_cntx(), get_plantid_frm_cntx() 
			  INTO v_company_id ,v_plant_id
			  FROM DUAL;
	      
		SELECT COUNT(t526.c526_product_request_detail_id) INTO v_pending_pick FROM t526_product_request_detail t526,
		  t525_product_request t525,
		  t907_shipping_info t907,
		  t504a_consignment_loaner t504acl ,
		  t504a_loaner_transaction t504alt  
     	 WHERE t526.c901_request_type            = 4127
            AND t907.C901_SOURCE                    = 50185
			AND t504acl.c504A_status_fl             = 7 -- Pending Pick status
			AND t526.c525_product_request_id        = t525.c525_product_request_id
			AND t525.c525_product_request_id        = t907.c907_ref_id
			AND t525.c525_void_fl                  IS NULL
			AND t526.c526_void_fl                  IS NULL
			AND t526.C526_PRODUCT_REQUEST_DETAIL_ID = t504alt.C526_PRODUCT_REQUEST_DETAIL_ID
			AND TRUNC(t526.c526_required_date) BETWEEN TRUNC(SYSDATE-NVL(get_rule_value('PICK','LOANERDASHDAYS'),0)) AND TRUNC(SYSDATE)
			AND t504alt.c504_consignment_id         = t504acl.c504_consignment_id
			AND t907.c907_void_fl              IS NULL
			AND t504alt.c504a_void_fl IS NULL 
			AND t525.C1900_company_id             = v_company_id
			AND t525.C5040_plant_id                  = v_plant_id
			AND t526.c526_status_fl                IN (20) --ALLOCATED
			; 
	   
	     p_pending_pick := v_pending_pick;
	     
	END gm_fch_loaner_pending_pick;
	
	/***************************************************************
* Description : Procedure to take count of pending putaway of loaner
* Author    : Vinoth
****************************************************************/
	
	PROCEDURE gm_fch_loaner_pending_putaway(
		p_pending_putaway OUT VARCHAR	
	)
	AS 
	v_company_id   T1900_COMPANY.C1900_COMPANY_ID%TYPE;
	v_plant_id     t526_product_request_detail.c5040_plant_id%TYPE;
	v_pending_putaway  VARCHAR2(10);
	BEGIN
			SELECT get_compid_frm_cntx(), get_plantid_frm_cntx() 
			  INTO v_company_id ,v_plant_id
			  FROM DUAL;
	      
		SELECT COUNT(t526.c526_product_request_detail_id) INTO v_pending_putaway FROM t526_product_request_detail t526,
		  t525_product_request t525,
		  t907_shipping_info t907,
		  t504a_consignment_loaner t504acl ,
		  t504a_loaner_transaction t504alt  
     	 WHERE t526.c901_request_type            = 4127
            AND t907.C901_SOURCE                    = 50185
			AND t504acl.c504A_status_fl             = 58       -- Pending PutAway status
			AND t526.c525_product_request_id        = t525.c525_product_request_id
			AND t525.c525_product_request_id        = t907.c907_ref_id
			AND t525.c525_void_fl                  IS NULL
			AND t526.c526_void_fl                  IS NULL
			AND t526.C526_PRODUCT_REQUEST_DETAIL_ID = t504alt.C526_PRODUCT_REQUEST_DETAIL_ID
			AND TRUNC(t526.c526_required_date) BETWEEN TRUNC(SYSDATE-NVL(get_rule_value('PUTAWAY','LOANERDASHDAYS'),0)) AND TRUNC(SYSDATE)
			AND t504alt.c504_consignment_id         = t504acl.c504_consignment_id
			AND t907.c907_void_fl              IS NULL 
			AND t504alt.c504a_void_fl IS NULL 
			AND t525.C1900_company_id             = v_company_id
			AND t525.C5040_plant_id                  = v_plant_id
			AND t526.c526_status_fl                IN (20)--ALLOCATED
			; 	
			
           
	     p_pending_putaway := v_pending_putaway;
	     
	END gm_fch_loaner_pending_putaway;
	
/************************************************************************
* Description : Procedure to take count of pending shipping status of loaner
* Author    : Vinoth
*************************************************************************/
	
	PROCEDURE gm_fch_loaner_pending_ship(
		p_pending_ship OUT VARCHAR	
	)
	AS 
	
	v_company_id   T1900_COMPANY.C1900_COMPANY_ID%TYPE;
	v_plant_id     t526_product_request_detail.c5040_plant_id%TYPE;
	v_pending_ship  VARCHAR2(10);
	
	BEGIN
	
			SELECT get_compid_frm_cntx(), get_plantid_frm_cntx() 
			  INTO v_company_id ,v_plant_id
			  FROM DUAL;
	      
	 SELECT COUNT(t526.c526_product_request_detail_id) INTO v_pending_ship FROM t526_product_request_detail t526,
		  t525_product_request t525,
		  t907_shipping_info t907,
		  t504a_consignment_loaner t504acl ,
		  t504a_loaner_transaction t504alt  
     	 WHERE t526.c901_request_type            = 4127
            AND t907.C901_SOURCE                    = 50185
			AND t504acl.c504A_status_fl             = 10      --Pending Shipping
			AND t526.c525_product_request_id        = t525.c525_product_request_id
			AND t525.c525_product_request_id        = t907.c907_ref_id
			AND t525.c525_void_fl                  IS NULL
			AND t526.c526_void_fl                  IS NULL
			AND t526.C526_PRODUCT_REQUEST_DETAIL_ID = t504alt.C526_PRODUCT_REQUEST_DETAIL_ID
			AND TRUNC(t526.c526_required_date) BETWEEN TRUNC(SYSDATE-NVL(get_rule_value('SHIP','LOANERDASHDAYS'),0)) AND TRUNC(SYSDATE)
			AND t504alt.c504_consignment_id         = t504acl.c504_consignment_id
			AND t907.c907_void_fl              IS NULL 
			AND t504alt.c504a_void_fl IS NULL 
			AND t525.C1900_company_id             = v_company_id
			AND t525.C5040_plant_id                  = v_plant_id
			AND t526.c526_status_fl                IN (20)--ALLOCATED
			;
	   
	     p_pending_ship := v_pending_ship;
	     
	END gm_fch_loaner_pending_ship;
	

	
/*************************************************************************
* Description : Procedure to take count of open request status of same day mode of loaner
* Author    : Vinoth
**************************************************************************/
	
	PROCEDURE gm_fch_loaner_open_sameday(
		p_open_sameday  OUT VARCHAR
	)
	AS
	
	v_company_id   T1900_COMPANY.C1900_COMPANY_ID%TYPE;
	v_plant_id     t526_product_request_detail.c5040_plant_id%TYPE;
	v_open_sameday  VARCHAR2(10);
	
	BEGIN
			SELECT get_compid_frm_cntx(), get_plantid_frm_cntx() 
			  INTO v_company_id ,v_plant_id
			  FROM DUAL;
	
	SELECT COUNT(t526.c526_product_request_detail_id) INTO v_open_sameday FROM t526_product_request_detail t526, t525_product_request t525, t907_shipping_info t907
	 where c901_request_type=4127        --Product Loaner
	   AND t526.c526_status_fl=10 		 -- Open Status
       AND TRUNC(t526.c526_required_date) BETWEEN TRUNC(SYSDATE-NVL(get_rule_value('SAMEDAY','LOANERDASHDAYS'),0)) AND TRUNC(SYSDATE) 
	   AND TO_CHAR(t526.c526_product_request_detail_id) = t907.c907_ref_id
	   AND t525.c525_product_request_id = t526.c525_product_request_id  
       AND t526.c525_product_request_id = t907.c525_product_request_id  
       AND t907.c901_delivery_mode = 5038  -- delivery mode same day
	   AND t526.c1900_company_id = v_company_id
	   AND t526.c5040_plant_id = v_plant_id
	   AND t526.c526_void_fl is null
	   AND t525.c525_void_fl is null
	   AND t907.c907_void_fl is null;
	   
	   p_open_sameday := v_open_sameday;
	   
	END gm_fch_loaner_open_sameday;
	
/**********************************************************************
* Description : Procedure to take count of Pending Pick status in same day mode loaner
* Author    : Vinoth
****************************************************************************/
	
	PROCEDURE gm_fch_loaner_topick_sameday(
		p_topick_sameday OUT VARCHAR
	)
	AS
	
	v_company_id      T1900_COMPANY.C1900_COMPANY_ID%TYPE;
	v_plant_id        t526_product_request_detail.c5040_plant_id%TYPE;
	v_topick_sameday  VARCHAR2(10);
	
	BEGIN
	
			SELECT get_compid_frm_cntx(), get_plantid_frm_cntx() 
			  INTO v_company_id ,v_plant_id
			  FROM DUAL;
	      
	 SELECT COUNT(t526.c526_product_request_detail_id) INTO v_topick_sameday FROM t526_product_request_detail t526,
		  t525_product_request t525,
		  t907_shipping_info t907,
		  t504a_consignment_loaner t504acl ,
		  t504a_loaner_transaction t504alt  
     	 WHERE t526.c901_request_type            = 4127
            AND t907.C901_SOURCE                    = 50185
			AND t504acl.c504A_status_fl             = 7		   --Pending Pick
			AND t907.c901_delivery_mode = 5038     --delivery mode same day
			AND t526.c525_product_request_id        = t525.c525_product_request_id
			AND t525.c525_product_request_id        = t907.c907_ref_id
			AND t525.c525_void_fl                  IS NULL
			AND t526.c526_void_fl                  IS NULL
			AND t526.C526_PRODUCT_REQUEST_DETAIL_ID = t504alt.C526_PRODUCT_REQUEST_DETAIL_ID
			AND TRUNC(t526.c526_required_date) BETWEEN TRUNC(SYSDATE-NVL(get_rule_value('SAMEDAY','LOANERDASHDAYS'),0)) AND TRUNC(SYSDATE)
			AND t504alt.c504_consignment_id         = t504acl.c504_consignment_id
			AND t907.c907_void_fl              IS NULL
			AND t504alt.c504a_void_fl IS NULL 
			AND t525.C1900_company_id             = v_company_id
			AND t525.C5040_plant_id                  = v_plant_id
			AND t526.c526_status_fl                IN (20)--ALLOCATED
			;
        
		
		p_topick_sameday := v_topick_sameday;

	END gm_fch_loaner_topick_sameday;
	
/******************************************************************************
* Description : Procedure to take count of pending shipping status in same day mode of loaner
* Author    : Vinoth
***********************************************************************************/
	
	PROCEDURE gm_fch_loaner_toship_sameday(
	    p_toship_sameday   OUT VARCHAR
	)
	AS
	
	v_company_id      T1900_COMPANY.C1900_COMPANY_ID%TYPE;
	v_plant_id        t526_product_request_detail.c5040_plant_id%TYPE;
	v_toship_sameday  VARCHAR2(10);
	
	BEGIN
	
			SELECT get_compid_frm_cntx(), get_plantid_frm_cntx() 
			  INTO v_company_id ,v_plant_id
			  FROM DUAL;
	      
	 SELECT COUNT(t526.c526_product_request_detail_id) INTO v_toship_sameday FROM t526_product_request_detail t526,
		  t525_product_request t525,
		  t907_shipping_info t907,
		  t504a_consignment_loaner t504acl ,
		  t504a_loaner_transaction t504alt  
     	 WHERE t526.c901_request_type            = 4127
            AND t907.C901_SOURCE                    = 50185
			AND t504acl.c504A_status_fl             = 10       -- pending shipping status
			AND t907.c901_delivery_mode = 5038     --delivery mode same day
			AND t526.c525_product_request_id        = t525.c525_product_request_id
			AND t525.c525_product_request_id        = t907.c907_ref_id
			AND t525.c525_void_fl                  IS NULL
			AND t526.c526_void_fl                  IS NULL
			AND t526.C526_PRODUCT_REQUEST_DETAIL_ID = t504alt.C526_PRODUCT_REQUEST_DETAIL_ID
			AND TRUNC(t526.c526_required_date) BETWEEN TRUNC(SYSDATE-NVL(get_rule_value('SAMEDAY','LOANERDASHDAYS'),0)) AND TRUNC(SYSDATE)
			AND t504alt.c504_consignment_id         = t504acl.c504_consignment_id
			AND t907.c907_void_fl              IS NULL 
			AND t504alt.c504a_void_fl IS NULL 
			AND t525.C1900_company_id             = v_company_id
			AND t525.C5040_plant_id                  = v_plant_id
			AND t526.c526_status_fl                IN (20)--ALLOCATED
			;
		
		p_toship_sameday := v_toship_sameday;
		
	END gm_fch_loaner_toship_sameday;
	
END gm_pkg_op_loaner_dashboard;
/