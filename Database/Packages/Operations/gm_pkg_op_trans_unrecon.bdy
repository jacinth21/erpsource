--@"C:\Database\Packages\Operations\gm_pkg_op_trans_unrecon.bdy";
CREATE OR REPLACE
PACKAGE BODY gm_pkg_op_trans_unrecon
IS
    /*
    * Description: This procedure used to save the un reconciled transaction
    * Author:
    */
PROCEDURE gm_unreconcile_ln_txn (
        p_txnid		IN t413_inhouse_trans_items.C413_TRANS_ID%TYPE,
        p_userid    IN t412_inhouse_transactions.C412_CREATED_BY%TYPE)
AS
    v_txn_id 	   t412_inhouse_transactions.c412_inhouse_trans_id%TYPE;
    v_part 		   T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE;
    v_qty    	   NUMBER;
    v_status 	   VARCHAR2 (400) ;
    v_refid		   VARCHAR2(20); 	
    v_status_fl    t412_inhouse_transactions.c412_status_fl%TYPE;
    v_lpnr_id 	   t412_inhouse_transactions.c412_inhouse_trans_id%TYPE;
    v_distid 	   t504_consignment.c701_distributor_id%TYPE;
    v_accid 	   t504_consignment.c704_account_id%TYPE;
    v_setid 	   t504_consignment.c207_set_id%TYPE := null;
    v_rma_id 	   t506_returns.c506_rma_id%TYPE;
    v_reason 	   t506_returns.c506_reason%TYPE;
    v_ord_part 	   T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE;
    v_cnt		   NUMBER := 0;
    v_comments	   t907_cancel_log.C907_COMMENTS%TYPE;
    v_msg		   VARCHAR2(20) := '';
    v_types 	   VARCHAR2(20) ;
    v_dtl_id	   t9201_charge_details.c9201_charge_details_id%TYPE;
    v_chrg_amt     t9201_charge_details.c9201_amount%TYPE;
    v_incident_id  t9201_charge_details.c9200_incident_id%TYPE;
    v_date_fmt     VARCHAR2(30);
BEGIN
	
	              SELECT NVL(get_compdtfmt_frm_cntx(),GET_RULE_VALUE('DATEFMT','DATEFORMAT')) 
	                INTO v_date_fmt 
	                FROM dual;
	
	BEGIN
		SELECT t413.c412_inhouse_trans_id, t413.c205_part_number_id, t413.c413_item_qty
		  , t413.c413_status_fl, t413.c413_ref_id, NVL(t413.c205_rec_part_number_id,t413.c205_part_number_id)
		  INTO v_txn_id,v_part,v_qty,v_status,v_refid,v_ord_part
		   FROM t413_inhouse_trans_items t413
		  WHERE t413.c413_trans_id = p_txnid
		    AND t413.c413_void_fl IS NULL
		    FOR UPDATE;
	EXCEPTION WHEN NO_DATA_FOUND
	THEN
		v_txn_id 	:= null;
		v_part	 	:= null;
		v_qty	 	:= null;
		v_status 	:= null;	
		v_refid	 	:= null;
		v_ord_part	:= null;
	END;
	
	SELECT COUNT(1) INTO v_cnt 
		FROM t413_inhouse_trans_items t413
		WHERE  t413.c413_trans_id = p_txnid
		   AND t413.c413_void_fl IS NOT NULL;
		   
    IF v_cnt >0
    THEN
	 	GM_RAISE_APPLICATION_ERROR('-20999','290',v_txn_id||'#$#'||v_part);
	END IF;
           
            IF v_status  = 'R' THEN
                BEGIN
             		SELECT t412.c412_status_fl
                     		INTO v_status_fl
					   FROM t412_inhouse_transactions t412
					  WHERE t412.c412_inhouse_trans_id = v_refid
					    AND t412.c412_void_fl         IS NULL;
                EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    v_status_fl := '0';
                END;
                IF v_status_fl = '4' THEN
                    GM_RAISE_APPLICATION_ERROR('-20999','291',v_refid||'#$#'||v_txn_id);
                END IF;
                gm_revert_ln (v_txn_id, p_txnid, p_userid) ;
                -- Voided the LRPN- for this Returned Transaction
                UPDATE t412_inhouse_transactions
				SET c412_void_fl              = 'Y',
					c412_last_updated_by = p_userid, 
					c412_last_updated_date=CURRENT_DATE
				  WHERE c412_inhouse_trans_id = v_refid
				    AND c412_void_fl         IS NULL;
            ELSIF v_status = 'S' THEN
                gm_revert_ln_do (v_txn_id, p_txnid, v_refid, v_ord_part, p_userid) ;
            ELSIF v_status = 'W' THEN
            	SELECT c412_type INTO v_types
				   FROM t412_inhouse_transactions
				  WHERE c412_inhouse_trans_id = v_txn_id;
            	IF v_types IN ('9112','9115','1006571') -- IH Loaner Item, Pl Item and IH Set Missing Types
            	THEN
	                
	                UPDATE t412_inhouse_transactions
						SET c412_void_fl              = 'Y',
						c412_last_updated_by = p_userid, 
						c412_last_updated_date=CURRENT_DATE
				  	WHERE c412_inhouse_trans_id = v_refid
				    	AND c412_void_fl         IS NULL;
	                
                END IF;
            
                -- WriteOff Posting 
                IF v_types IN ('9112','1006571')
                THEN
                	gm_save_ledger_posting (1006610, CURRENT_DATE, v_part, '01', v_txn_id, v_qty, NULL, p_userid) ;
                ELSE                
                	gm_save_ledger_posting (481000, CURRENT_DATE, v_part, '01', v_txn_id, v_qty, NULL, p_userid) ;	
                END IF;
                gm_revert_ln (v_txn_id, p_txnid, p_userid) ;
            ELSIF v_status = 'C' THEN
                BEGIN
                      SELECT T504A.C504A_CONSIGNED_TO_ID
                      	INTO v_distid
					   FROM T504A_LOANER_TRANSACTION T504A, T412_INHOUSE_TRANSACTIONS T412, T504_CONSIGNMENT T504
					  WHERE T412.C412_INHOUSE_TRANS_ID       = v_txn_id
					    AND T412.C504A_LOANER_TRANSACTION_ID = T504A.C504A_LOANER_TRANSACTION_ID
					    AND T504.C504_CONSIGNMENT_ID         = T504A.C504_CONSIGNMENT_ID
					    AND T412.C412_VOID_FL               IS NULL
					    AND T504A.C504A_VOID_FL             IS NULL
					    AND T504.C504_VOID_FL               IS NULL;
                EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    v_distid := NULL;
                    v_setid  := NULL;
                END;
                
	            gm_pkg_op_return.gm_op_sav_return (v_distid, to_char(CURRENT_DATE,v_date_fmt), '3251', p_userid, '3302', null, v_qty, v_part,
                v_setid, v_rma_id);
                -- Reverse the Consigned Posting - Consignment Return Sales Rep
                gm_save_ledger_posting (481001, CURRENT_DATE, v_part, '01', v_rma_id, v_qty, NULL, p_userid);
				
                v_comments := 'Reverse Posting created for ' || v_refid || ' that created when reconcile the ' ||  v_txn_id;
								
                -- Verified the GM-RA 
                UPDATE T506_RETURNS 
                	SET c506_status_fl = 2,
                		c506_credit_date = CURRENT_DATE,
                		C506_AC_CREDIT_DT = CURRENT_DATE,
                		c506_comments = v_comments,
                		C506_LAST_UPDATED_BY = p_userid, 
                		C506_LAST_UPDATED_DATE = CURRENT_DATE
                	WHERE C506_RMA_ID = v_rma_id;
                	
                 -- Setting the Parts are returned (Consigned) by setting status "C"
                  UPDATE t507_returns_item
					SET c507_status_fl      = 'C'
					  , c507_control_number = 'NOC#'
					  WHERE c506_rma_id     = v_rma_id
					    AND c507_status_fl  = 'R';
					    
				 -- Removing the Missing Returns, cause its returned
					 DELETE
					   FROM t507_returns_item
					  WHERE c506_rma_id    = v_rma_id
					    AND c507_status_fl = 'Q';

                gm_revert_ln (v_txn_id, p_txnid, p_userid) ;
                
                GM_UPDATE_LOG (v_rma_id,v_comments,'1216',p_userid,v_msg);
                
                -- PC-1779 - To decrease field sales inventory while doing UnReconcile
                gm_pkg_op_inv_field_sales_tran.gm_update_fs_inv(v_rma_id, 'T506', v_distid, 102901, 4302, 102908, p_userid);
                	
                v_comments :='';
            END IF;
            
            BEGIN
			  SELECT c907_comments
			 	 INTO v_comments
			  FROM
			    (SELECT c907_comments
			      FROM t907_cancel_log
			     WHERE c907_ref_id = TO_CHAR(p_txnid)
			     AND c907_void_fl IS NULL
			     ORDER BY c907_cancel_date DESC
			    )
			  WHERE ROWNUM = 1;
			EXCEPTION
			WHEN NO_DATA_FOUND THEN
			  v_comments := NULL;
			END;
			
	        IF v_comments IS NOT NULL
	        THEN
	        	GM_UPDATE_LOG (v_txn_id,v_comments,'1246',p_userid,v_msg);
	        END IF;

	        -- Commenting the code as we don't need the auto credit.
	        
	/*	SELECT MAX(DECODE(t9201.c9201_status,20,t9201.c9201_charge_details_id,NULL))
		     , (SUM(DECODE(t9201.c9201_status,20, t9201.c9201_amount,0))
		   	   - SUM((DECODE(t9201.c9201_status,25,t9201.c9201_amount,0) + DECODE(t9201.c9201_status,40,t9201.c9201_amount,0)))
		   	   )
		  INTO v_dtl_id, v_chrg_amt 	   
		  FROM t412_inhouse_transactions t412
		     , t504a_loaner_transaction t504a
		     , t526_product_request_detail t526
		     , t9200_incident t9200
		     , t9201_charge_details t9201
		WHERE t412.c504a_loaner_transaction_id = t504a.c504a_loaner_transaction_id
		  AND t504a.c526_product_request_detail_id = t526.c526_product_request_detail_id
		  AND t526.c525_product_request_id = t9200.c9200_ref_id
		  AND t9200.c9200_incident_id = t9201.c9200_incident_id
		  AND t412.c412_inhouse_trans_id = v_txn_id
		  AND t9200.c901_incident_type = 92069 
		  AND t9200.c9200_void_fl IS NULL
		  AND t9201.c205_part_number = v_part
		  AND t412.c412_void_fl IS NULL
		  AND t504a.c504a_void_fl IS NULL
		  AND t526.c526_void_fl IS NULL
		  AND t9200.c9200_void_fl IS NULL
		  AND t9201.c9201_void_fl IS NULL;
  
  IF v_chrg_amt > 0 THEN
  			   SELECT 'GM-IC-' || s9200_incident.NEXTVAL 
				 INTO v_incident_id 
			     FROM DUAL;
           
           	 INSERT INTO t9200_incident( c9200_incident_id, c9200_ref_type, c9200_ref_id
    		      	   , c9200_incident_value, c9200_status_fl, c9200_created_by
    				   , c9200_created_date, c901_incident_type, c9200_incident_date) 
	     	      SELECT v_incident_id,  c9200_ref_type, c9200_ref_id
    		      	   , c9200_incident_value, c9200_status_fl, p_userid
    				   , CURRENT_DATE, c901_incident_type, c9200_incident_date
    			    FROM t9200_incident t9200, t9201_charge_details t9201
    		       WHERE t9200.c9200_incident_id = t9201.c9200_incident_id
    		     	 AND t9201.c9201_charge_details_id = v_dtl_id;  
	     	           
    		INSERT INTO t9201_charge_details(c9201_charge_details_id , c9201_amount , c9201_status
                      , c9201_created_by , c9201_created_date, c9200_incident_id
                      , c701_distributor_id , c703_sales_rep_id , c205_part_number                   
                      , c9201_qty_missing , c9201_unit_cost , c9201_credited_date 
                      , c901_recon_comments, c9201_master_chrg_det_id )
                 SELECT s9201_charge_details.NEXTVAL , v_chrg_amt , 25
                      , p_userid , CURRENT_DATE, v_incident_id
                      , c701_distributor_id , c703_sales_rep_id , c205_part_number                   
                      , c9201_qty_missing , c9201_unit_cost, LAST_DAY(CURRENT_DATE)
                      , 102748, v_dtl_id
                   FROM t9201_charge_details
                  WHERE c9201_charge_details_id =  v_dtl_id;
  END IF;*/
	        
END gm_unreconcile_ln_txn;
/*
* Description: This procedure used to fetch ln do
* Author:
*/
PROCEDURE gm_revert_ln_do (
        p_txnid  IN t412_inhouse_transactions.c412_inhouse_trans_id%TYPE,
        p_transid IN T413_INHOUSE_TRANS_ITEMS.c413_trans_id%TYPE,
        p_ordid  IN t501_order.c501_order_id%TYPE,
        p_ord_pnum   IN t205_part_number.c205_part_number_id%TYPE,
        p_userid IN T413_INHOUSE_TRANS_ITEMS.C413_LAST_UPDATED_BY%TYPE)
AS
BEGIN
    gm_revert_ln (p_txnid,p_transid, p_userid) ;
    gm_revert_do (p_ordid,p_ord_pnum,p_userid) ;
END gm_revert_ln_do;
/*
* Description: This procedure used to revert the do
* Author:
*/
PROCEDURE gm_revert_do (
        p_ordid  IN t501_order.c501_order_id%TYPE,
        p_ord_pnum   IN t205_part_number.c205_part_number_id%TYPE,
        p_userid IN T413_INHOUSE_TRANS_ITEMS.C413_LAST_UPDATED_BY%TYPE
)
AS
BEGIN
		UPDATE t502a_item_order_attribute
		SET c502a_attribute_value   = 'N',
			c502a_last_updated_by = p_userid, 
			c502a_last_updated_date = CURRENT_DATE
		  WHERE c501_order_id       = p_ordid
		    AND c205_part_number_id = p_ord_pnum
		    AND c901_attribute_type = '50822'
		    AND c502a_void_fl      IS NULL;
END gm_revert_do;
/*
* Description: This procedure used to revert the ln
* Author:
*/
PROCEDURE gm_revert_ln (
        p_txnid  IN t412_inhouse_transactions.c412_inhouse_trans_id%TYPE,
        p_transid IN T413_INHOUSE_TRANS_ITEMS.c413_trans_id%TYPE,
        p_userid IN T413_INHOUSE_TRANS_ITEMS.C413_LAST_UPDATED_BY%TYPE)
AS
BEGIN
    gm_remove_ln_item (p_transid,p_userid) ;
    gm_pkg_op_trans_recon.gm_sav_verify_ln(p_txnid,'N', p_userid);
    gm_pkg_op_trans_recon.gm_sav_ln_status (p_txnid, 3, p_userid) ;    
END gm_revert_ln;
/*
* Description: This procedure used to revert the ln items
* Author:
*/
PROCEDURE gm_remove_ln_item (
        p_transid	IN T413_INHOUSE_TRANS_ITEMS.c413_trans_id%TYPE,
        p_userid 	IN T413_INHOUSE_TRANS_ITEMS.C413_LAST_UPDATED_BY%TYPE)
AS
BEGIN
	     UPDATE T413_INHOUSE_TRANS_ITEMS
	    	SET C413_VOID_FL              = 'Y', 
	    	C413_LAST_UPDATED_BY = p_userid, 
	    	C413_LAST_UPDATED_DATE = CURRENT_DATE
	      WHERE C413_TRANS_ID = p_transid; 
          
	     --Whenever the detail is updated, the inhouse transactions table has to be updated ,so ETL can Sync it to GOP.
		UPDATE t412_inhouse_transactions 
		   SET c412_last_updated_by = p_userid
		     , c412_last_updated_date = CURRENT_DATE
		 WHERE c412_inhouse_trans_id = to_char(p_transid);	
END gm_remove_ln_item;
END gm_pkg_op_trans_unrecon;
/