--@"c:\database\packages\operations\gm_pkg_op_set_put_txn.bdy";
CREATE OR REPLACE
PACKAGE BODY gm_pkg_op_set_put_txn
IS
/****************************************************************
* Description : this procedure is used split and call the procedure
* Author      :
*****************************************************************/
PROCEDURE gm_sav_set_put_batch (
        p_inputstr  IN CLOB,
        p_user_id   IN t5052_location_master.c5052_created_by%TYPE)
AS
    v_string CLOB   := p_inputstr;
    v_strlen      NUMBER := NVL (LENGTH (p_inputstr), 0) ;
    v_substring   VARCHAR2 (30000) ;
    v_location_id VARCHAR2 (40) ;
    v_tagid       VARCHAR2 (40) ;
    v_cnid        VARCHAR2 (40) ;
    v_count       NUMBER;
BEGIN	
	 
    IF v_strlen > 0 THEN
	    
        /* Loop the Delminted String and reterive the values */
        WHILE INSTR (v_string, '|') <> 0
        LOOP
            v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
            v_string    := SUBSTR (v_string, INSTR (v_string, '|')    + 1) ;
            --
            v_cnid        := NULL;
            v_location_id := NULL;
            v_tagid       := NULL;
            --
            v_cnid        := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            v_substring   := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
            v_location_id := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            v_substring   := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
            v_tagid       := v_substring;
	      
            gm_pkg_op_set_put_txn.gm_sav_set_put (v_tagid, v_location_id, p_user_id, v_cnid) ;
	    
        END LOOP;
    END IF;
END gm_sav_set_put_batch;


/****************************************************************
* Description : this procedure is used to save the Put details
* Author      :
*****************************************************************/
PROCEDURE gm_sav_set_put (
        p_tag_id IN t5010_tag.c5010_tag_id%TYPE,
        p_loc_id IN t5052_location_master.c5052_location_id%TYPE,
        p_user_id  IN t101_user.c101_user_id%TYPE,
        p_cn	 IN t504_consignment.c504_consignment_id%TYPE DEFAULT NULL
        )
AS
  v_error_code NUMBER;
  v_txn_id VARCHAR2 (20);
  v_txn_type NUMBER;
  v_loc_id VARCHAR2 (20);
  v_out_param1 varchar2(20);
  v_out_param2 varchar2(20);
 BEGIN
  IF p_tag_id IS NOT NULL THEN
 		 gm_pkg_ac_tag_info.gm_fch_tag_info(p_tag_id,v_loc_id, v_txn_id, v_txn_type) ; -- Get txn_id, txn_type and Location_id   
   ELSE
  	  v_txn_type := get_tag_inv_type(p_cn);  
  	  v_txn_id := p_cn;
  END IF; 

  gm_pkg_op_set_validate.gm_validate_set_txn_status(v_txn_id, v_txn_type);	-- Tag validation

  gm_pkg_op_set_txn.gm_sav_set_txn_status (v_txn_id, 'PUT', v_txn_type, p_user_id); -- Validate to day is ship date, If so custom error is thrown so flow shift to exception block
 
  BEGIN
	  --no need validate ship date is today or not, since it shows next status on front
  	--  gm_pkg_op_set_validate.gm_validate_ship_date(v_txn_id,v_txn_type); 
  	
    --location could be empty if next ststus is pending shipping
    IF p_loc_id IS NOT NULL THEN
    gm_pkg_op_set_txn.gm_sav_tag_location(p_tag_id, p_loc_id, v_txn_id, v_txn_type, p_user_id, 'PUT'); 
    END IF;
    
    EXCEPTION WHEN OTHERS THEN -- Exception block is needed to continue the code
      v_error_code :=SQLCODE;
      
      IF v_error_code =-20511 THEN
        GM_RAISE_APPLICATION_ERROR('-20999','280',''); 
      END IF;

      IF v_error_code =-20512 THEN
            GM_RAISE_APPLICATION_ERROR('-20999','281','');
      END IF;
   
      IF v_error_code =-20513 THEN
            GM_RAISE_APPLICATION_ERROR('-20999','282',''); 
      END IF;
      
      IF v_error_code =-20514 THEN
           GM_RAISE_APPLICATION_ERROR('-20999','283',''); 
      END IF;

      IF v_error_code =-20503 THEN
         GM_RAISE_APPLICATION_ERROR('-20999','284','');
      END IF;
  END;
     
  -- gm_pkg_allocation.gm_sav_set_allocation(v_txn_id, v_txn_type, p_user_id,93006, 'PUT'); --Completed. => Update user id in t5050 for the transaction
  
   gm_pkg_allocation.gm_sav_set_complete(v_txn_id, 93604, p_user_id,93006); --Completed. => Update user id in t5050 for the transaction
  
END gm_sav_set_put ;

END gm_pkg_op_set_put_txn;
/