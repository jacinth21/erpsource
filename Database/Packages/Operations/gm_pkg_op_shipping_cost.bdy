--@"C:\Database\Packages\Operations\gm_pkg_op_shipping_cost.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_shipping_cost
IS

  /*******************************************************************
   * Description : Procedure to get the cost of the given transaction.
   * Author      : VPrasath
   *******************************************************************/

PROCEDURE gm_fch_ship_cost_for_order_us (
	    p_acct_id		IN	t501_order.c704_account_id%TYPE	
	  , p_ship_to 		IN	t907_shipping_info.c901_ship_to%TYPE
	  , p_ship_to_id	IN	t907_shipping_info.c907_ship_to_id%TYPE
	  , p_address_id	IN	t907_shipping_info.c106_address_id%TYPE
	  , p_carrier	 	IN	t9071_shipping_zone.c901_carrier%TYPE
	  , p_ship_mode		IN	t9072_shipping_cost.c901_shipping_mode%TYPE
	  , p_pkg_weight	IN	t9072_shipping_cost.c9072_package_weight%TYPE
	  , p_rtn			OUT	t9072_shipping_cost.c9072_package_cost%TYPE
)
AS
		v_rtn t9072_shipping_cost.c9072_package_cost%TYPE;
		v_party_id t9080_ship_party_param.c101_party_id%TYPE;
		v_shipmodetp t906_rules.c906_rule_value%TYPE;
		v_ship_payee t9080_ship_party_param.c901_standard_ship_payee%TYPE;
		v_ship_chg_typ t9080_ship_party_param.c901_standard_ship_charge_type%TYPE;
		v_ship_val_pct t9080_ship_party_param.c9080_standard_ship_value_pct%TYPE;
		v_exec_string VARCHAR2(1000); 
		v_prc_name VARCHAR2(1000); 
		v_actual_rate t9072_shipping_cost.c9072_package_cost%TYPE;
		V_third_pty_cnt  NUMBER;
BEGIN
	 p_rtn := 0;
	 
	 BEGIN 
		/* 
		 *	Check if account is associated to a group and that group 
		 *	has shipping parameters setup, if yes, then get the 
		 *	party id for that group otherwise, get the party id 
		 *	for the account.
		 */
		
		v_party_id := gm_pkg_op_ship_charge.get_gpb_or_acc_party_id(p_acct_id);
		/* The following Condtition is added for not calculating the Shipping cost for the Coustomers who has the 
		 * FedEx Third Party Account during the Order creation as part of the PMT-8120 */
				SELECT COUNT(1) INTO V_third_pty_cnt
                  FROM t9080_ship_party_param 
                 WHERE c101_party_id = v_party_id 
                   AND c9080_third_party IS NOT NULL 
                   AND c9080_void_fl IS NULL;
		
        IF V_third_pty_cnt > 0 THEN
            p_rtn := 0;
        	RETURN;
        END IF;   
		-- For Contracted Accounts, find out the mode type for the selected ship mode
		v_shipmodetp := get_rule_value(p_ship_mode, 'MODTYP');
		
		/* 
		 *  Fetch the shipping parameters based on the selected ship mode,
		 *  if ship mode falls under "STANDARD" category, fetch the Standard Parameters, 
		 *  ( gm_fch_std_ship_prty_param will be called )
		 *  if ship mode falls under "RUSH" category, fetch the Rush Parameters
		 *  ( gm_fch_rush_ship_prty_param will be called ) 
		 */
		
		v_prc_name := get_rule_value (v_shipmodetp, 'SHPPPAR');
		IF v_prc_name IS NOT NULL THEN
			v_exec_string := 'BEGIN ' || v_prc_name || '(:v_party_id, :v_ship_payee, :v_ship_chg_typ,:v_ship_val_pct); END;'; 
			EXECUTE IMMEDIATE v_exec_string using v_party_id, out v_ship_payee,out v_ship_chg_typ,out v_ship_val_pct;
		
			/* 
			 * If Payee is Globus/Customer then the following gets executed otherwise should not. 
			 * If a Payee is being added, need to insert new procedure names in rules table.
			 * If Payee is not set for an account, Payee is considered 'DEFAULT', following code handles that as well
			 */
		
			v_prc_name := get_rule_value (nvl(to_char(v_ship_chg_typ),'-9999'), nvl(to_char(v_ship_payee),'DEFAULT'));	
			IF v_prc_name IS NOT NULL THEN
				-- Fetch the Carrier Rate
	
				v_actual_rate := gm_pkg_op_ship_charge.get_actual_rate (p_ship_to,p_ship_to_id, p_address_id, p_carrier, p_ship_mode, p_pkg_weight);

				v_exec_string := 'BEGIN ' || v_prc_name || '(:v_ship_val_pct, :v_actual_rate, :p_ship_mode, :p_rtn); END;'; 
				EXECUTE IMMEDIATE v_exec_string using v_ship_val_pct, v_actual_rate, p_ship_mode, out p_rtn;
			END IF;
		END IF;
	 EXCEPTION WHEN OTHERS THEN
		p_rtn := 0;
	 END;
	  
END gm_fch_ship_cost_for_order_us;

PROCEDURE gm_fch_ship_cost_for_order_za (
	    p_acct_id		IN	t501_order.c704_account_id%TYPE	
	  , p_ship_to 		IN	t907_shipping_info.c901_ship_to%TYPE
	  , p_ship_to_id	IN	t907_shipping_info.c907_ship_to_id%TYPE
	  , p_address_id	IN	t907_shipping_info.c106_address_id%TYPE
	  , p_carrier	 	IN	t9071_shipping_zone.c901_carrier%TYPE
	  , p_ship_mode		IN	t9072_shipping_cost.c901_shipping_mode%TYPE
	  , p_pkg_weight	IN	t9072_shipping_cost.c9072_package_weight%TYPE
	  , p_rtn			OUT	t9072_shipping_cost.c9072_package_cost%TYPE
)
AS
		v_company_id    t501_order.c1900_company_id%TYPE;
		v_rule_id		t906_rules.c906_rule_id%TYPE;
		v_ship_cost		t906_rules.c906_rule_value%TYPE;
BEGIN
	 p_rtn := 0;
	 
	 --Getting company id from context.   
    SELECT get_compid_frm_cntx()
	  INTO v_company_id 
	FROM DUAL;
	
	-- To calculate Shipping Cost
	SELECT NVL (GET_ACCOUNT_ATTRB_VALUE (p_acct_id,'91981'),-1)
	  INTO v_ship_cost
	FROM DUAL;
	
	IF v_ship_cost = -1
	THEN
		v_rule_id := v_company_id||'_'||p_ship_mode;
		-- Get the shipcost from rules table
		BEGIN
			SELECT get_rule_value_by_company(v_rule_id,'SHIPCOST', v_company_id)
				INTO v_ship_cost
			FROM DUAL;
		EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			v_ship_cost := 0;
		END;
	END IF;
	
	p_rtn := NVL(v_ship_cost,0);
	
END gm_fch_ship_cost_for_order_za;

END gm_pkg_op_shipping_cost;
/