--@"C:\projects\Branches\jboss-migration\Database\Packages\Operations\requests\gm_pkg_op_loaner_request.bdy";
CREATE OR REPLACE PACKAGE BODY gm_pkg_op_loaner_request
IS
  
/*******************************************************
* Description : Procedure is used to update escalation flag in loaner request 
* Author  : sindhu
*******************************************************/	
	PROCEDURE  gm_upd_loaner_esclfl (
	    p_request_dtl_id    IN t526_product_request_detail.c525_product_request_id%TYPE,
        p_escl_fl           IN t526_product_request_detail.c526_loaner_escalation_fl%TYPE,
        p_log               IN t526a_product_request_log.c526a_comments%TYPE,
	    p_type              IN t526a_product_request_log.c901_type%TYPE,
        p_user_id           IN t526_product_request_detail.c526_last_updated_by%TYPE
	)
	AS
		v_status_fl			t526_product_request_detail.c526_status_fl%TYPE;			
	BEGIN
		SELECT c526_status_fl
			INTO v_status_fl
		FROM t526_product_request_detail
		WHERE c526_product_request_detail_id = p_request_dtl_id
		AND c526_void_fl                    IS NULL
		FOR UPDATE;
		
		IF v_status_fl <> 10
		THEN
			RETURN;
		END IF;
		
		UPDATE t526_product_request_detail
		SET c526_loaner_escalation_fl        = p_escl_fl,
		c526_last_updated_by = p_user_id,
        c526_last_updated_date = current_date
		WHERE c526_product_request_detail_id = p_request_dtl_id
		AND c526_void_fl                    IS NULL;		
				
		gm_pkg_op_loaner_request.gm_sav_loaner_request_log(p_request_dtl_id, p_log, p_type, p_user_id);
    END gm_upd_loaner_esclfl;
  
   
/*******************************************************
* Description : Procedure is used to save escalation log details in loaner request
* Author  : sindhu
*******************************************************/	
	PROCEDURE  gm_sav_loaner_request_log (
	    p_request_dtl_id       IN t526_product_request_detail.c525_product_request_id%TYPE,
	    p_log                  IN t526a_product_request_log.c526a_comments%TYPE,
	    p_type                 IN t526a_product_request_log.c901_type%TYPE,
	    p_user_id              IN t526_product_request_detail.c526_last_updated_by%TYPE
	)
	AS
	BEGIN
	
		INSERT INTO t526a_product_request_log(c526a_product_request_log_id,c526_product_request_detail_id, c526a_comments, c901_type,c526a_last_updated_by, c526a_last_updated_date)
		VALUES(s526a_product_request_log_id.NEXTVAL,p_request_dtl_id,p_log,p_type,p_user_id,CURRENT_DATE);	 
					
    END gm_sav_loaner_request_log;
    
    /****************************************************
 * Description : Procedure is used to  update the loaner escalation flag to 'N'  in  loaner request/PMT-37633
 * Author  : Angeline
 * ***************************************************/
	PROCEDURE gm_upd_clear_loaner_escl(
	p_company_id	t526_product_request_detail.c1900_company_id%type
	)
	AS
	 	CURSOR loaner_escl_cur
	    IS
			SELECT c526_product_request_detail_id req_det_id 
			FROM t526_product_request_detail
			WHERE c526_loaner_escalation_fl = 'Y'
			AND c526_status_fl = 10
			AND c526_void_fl IS NULL
			AND c1900_company_id = p_company_id;

		BEGIN
			
			-- user_id=30301,type for loaner request =  26240693, open status = 10
  			FOR req_det IN loaner_escl_cur
    			LOOP
				     UPDATE t526_product_request_detail
		             SET c526_loaner_escalation_fl   = 'N',
		             c526_last_updated_by = 30301,
		             c526_last_updated_date = current_date
		             WHERE c526_product_request_detail_id = req_det.req_det_id
		             AND c526_loaner_escalation_fl = 'Y'
		             AND c526_status_fl = 10
		             AND c1900_company_id = p_company_id	
					 AND c526_void_fl IS NULL;

					 gm_pkg_op_loaner_request.gm_sav_loaner_request_log(req_det.req_det_id, 'Escalation unflagged', 26240693, 30301);
    	       END LOOP;
    	       commit;
END gm_upd_clear_loaner_escl;
  
END gm_pkg_op_loaner_request;
/