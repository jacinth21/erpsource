/* Formatted on 07/02/2018 10:18 (Formatter Plus v4.8.0) */
/* @ "C:\database\Packages\Operations\requests\gm_pkg_op_stock_transfer.bdy" */
CREATE OR REPLACE
PACKAGE body gm_pkg_op_stock_transfer
IS

   /*******************************************************
* Description : Procedure is used to reconfigure the parts
* Author      : ppandiyan
*******************************************************/

    PROCEDURE gm_sav_reconfig_request ( 
        p_request_id       IN t520_request.c520_request_id%TYPE,
        p_part_to_reconfig IN VARCHAR2,
        p_user             IN t520_request.c520_created_by%TYPE,
        p_req_void_fl      OUT VARCHAR2
       )
AS

	    v_strlen NUMBER           := NVL (LENGTH (p_part_to_reconfig), 0) ;
	    v_string VARCHAR2 (30000) := p_part_to_reconfig;
	    v_pnum t205_part_number.c205_part_number_id%TYPE;
	    v_qty         NUMBER;
	    v_action      VARCHAR2 (20) ;
	    v_substring   VARCHAR2 (1000) ;
	    v_data_exists     NUMBER;


  
BEGIN
	p_req_void_fl := '';
	IF v_strlen > 0
	THEN
    -- Loop through the parts and reconfigure them
	    WHILE INSTR (v_string, '|') <> 0
	    LOOP
	        v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
	        v_string    := SUBSTR (v_string, INSTR (v_string, '|')    + 1) ;
	        v_pnum      := NULL;
	        v_qty       := NULL;
	        v_action    := NULL;
	        v_pnum      := SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1) ;
	        v_substring := SUBSTR (v_substring, INSTR (v_substring, ',')    + 1) ;
	        v_qty       := SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1) ;
	        v_substring := SUBSTR (v_substring, INSTR (v_substring, ',')    + 1) ;
	        v_action    := v_substring;
	        gm_sav_reconfig_part (p_request_id, v_pnum, v_qty, v_action, p_user) ; -- to edit/void each line parts
	    END LOOP;
	END IF;
			--checking the parts(line item) of the requests
	         SELECT COUNT (c520_request_id)
	           INTO v_data_exists
	           FROM t521_request_detail
	          WHERE c520_request_id = p_request_id;
			--voiding the Request if there is no parts available under the request
	          IF v_data_exists = 0 THEN
	          p_req_void_fl := 'Y'; --to throw the validation of all the parts has been voided
	          
	             UPDATE t520_request
	                SET c520_void_fl = 'Y'
	                  , c520_last_updated_by = p_user
	                  , c520_last_updated_date = CURRENT_DATE
	              WHERE c520_request_id = p_request_id; --void it
	              
	             UPDATE T5201_REQUESTED_COMPANY
	                SET C5201_VOID_FL = 'Y'
	                  , C5201_LAST_UPDATED_BY = p_user
	                  , C5201_LAST_UPDATED_DATE = CURRENT_DATE
	              WHERE C520_REQUEST_ID = p_request_id; --void it
	   		  END IF;

END gm_sav_reconfig_request;

   /*******************************************************
* Description : Procedure is used to edit or void each parts of the request
* Author      : ppandiyan
*******************************************************/
PROCEDURE gm_sav_reconfig_part (
        p_request_id       IN t520_request.c520_request_id%TYPE, 
        p_part_num         IN t205_part_number.c205_part_number_id%TYPE,
        p_qty              IN VARCHAR2,
        p_action           IN VARCHAR2,
        p_user             IN t520_request.c520_created_by%TYPE)

AS
   v_qty_to_process          NUMBER;
   v_out_remaining_qty t505_item_consignment.c505_item_qty%TYPE; 

BEGIN
    v_qty_to_process := p_qty;

    -- 0. if qty = 0 return
    IF v_qty_to_process = 0 THEN
        RETURN;
    END IF;
    	
    		--106721 -- VOID
    		--106720 -- EDIT
    		IF p_action = '106721' THEN
    		 	gm_pkg_op_request_summary.gm_sav_remove_from_request (p_request_id, p_part_num, v_qty_to_process, '90810', v_out_remaining_qty) ; --90810 - Void-- to execute the existing flow
    		ELSIF  p_action = '106720' THEN
    		 	gm_sav_edit_request (p_request_id, p_part_num, v_qty_to_process, p_user) ;
    		END IF;
   END gm_sav_reconfig_part;

   
   /*******************************************************
* Description : Procedure  is used for request reconfigure to update the edited Qty
* Author      : ppandiyan
*******************************************************/
PROCEDURE gm_sav_edit_request (
        p_request_id    IN t520_request.c520_request_id%TYPE,
        p_part_num      IN t205_part_number.c205_part_number_id%TYPE,
        p_qty           IN NUMBER,
        p_user          IN t520_request.c520_created_by%TYPE)
AS
    v_c521_request_detail_id t521_request_detail.c521_request_detail_id%TYPE;
    v_qty NUMBER ;
BEGIN
     SELECT c521_request_detail_id, c521_qty
       INTO v_c521_request_detail_id, v_qty
       FROM t521_request_detail t521
      WHERE c520_request_id          = p_request_id
        AND t521.c205_part_number_id = p_part_num;
 
    IF NVL(v_qty,0) <> NVL(p_qty,0)  THEN
         UPDATE t521_request_detail t521
        SET t521.c521_qty              = p_qty
          WHERE c521_request_detail_id = v_c521_request_detail_id;

		--Whenever the detail is updated, the request table has to be updated.
        UPDATE t520_request
           SET c520_last_updated_date = CURRENT_DATE,
               c520_last_updated_by   = p_user
         WHERE C520_request_Id = p_request_id;           
    END IF;

END gm_sav_edit_request;

   /*******************************************************
* Description : Procedure  is used to fetch the shipping 
				details of the Stock transfer initiate request
* Author  	  : ppandiyan
*******************************************************/
PROCEDURE gm_fch_request_ship_details (
        p_request_id    IN t520_request.c520_request_id%TYPE,
        p_outfchdetails OUT TYPES.cursor_type)
AS
	v_comp_dt_fmt VARCHAR2 (20); 
BEGIN
	SELECT get_compdtfmt_frm_cntx() INTO v_comp_dt_fmt FROM DUAL;
	
    OPEN p_outfchdetails FOR SELECT 
    		get_code_name (t520.c520_request_for) requestfor, TO_CHAR (t520.c520_required_date, v_comp_dt_fmt) requireddate,
	        get_user_name (t520.c520_created_by) RQinitby,
	        get_company_name (T520.C1900_Company_Id) RQCompNm,
	        t520.c520_status_fl requeststatusflag,t520.c520_Master_Request_Id masterRequestId,
		    GM_PKG_CM_SHIPPING_INFO.GET_ADDRESS(T520.C901_SHIP_TO,T520.C520_SHIP_TO_ID) SHIPADD,
		    get_code_name(T907.C901_DELIVERY_MODE) SMODE, 
		    get_code_name(T907.C901_DELIVERY_CARRIER) SCARR
	
	    FROM t520_request t520,
	    	 T907_SHIPPING_INFO T907 
	    WHERE t520.c520_request_id = p_request_id 
	    AND T907.C907_REF_ID = T520.C520_REQUEST_ID
	    AND t520.c520_void_fl  IS NULL 
	    AND T907.C907_VOID_FL IS NULL;

EXCEPTION
WHEN NO_DATA_FOUND THEN
    raise_application_error ( - 20903, '') ;
END gm_fch_request_ship_details;
--
END gm_pkg_op_stock_transfer;
/