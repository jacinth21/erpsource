--@"C:\projects\Branches\jboss-migration\Database\Packages\Operations\requests\gm_pkg_op_loaner_request.bdy";
CREATE OR REPLACE PACKAGE BODY gm_pkg_op_loaner_request_rpt
IS
  
/*******************************************************
* Description : Procedure is used to fetch escalation log details in loaner request
* Author  : Sindhu
*******************************************************/	
	PROCEDURE  gm_fch_loaner_log_dtls (
	    p_request_dtl_id       IN t526_product_request_detail.c525_product_request_id%TYPE,
	    p_type                 IN t526a_product_request_log.c901_type%TYPE,
	    p_log				   OUT  TYPES.cursor_type
	)
	AS
		v_date_fmt VARCHAR2 (20) ;  					
	BEGIN
	   SELECT get_compdtfmt_frm_cntx ()
       INTO v_date_fmt
       FROM dual;
	OPEN p_log
		 FOR
			SELECT get_user_name(c526a_last_updated_by) UNAME,
			c526a_comments COMMENTS,
			c526_product_request_detail_id ID,
			TO_CHAR(c526a_last_updated_date,v_date_fmt|| ':HH:MI:SS') DT
			FROM t526a_product_request_log
			WHERE c526_product_request_detail_id = p_request_dtl_id and c901_type = p_type
			ORDER BY c526a_last_updated_date desc;
			
				
    END gm_fch_loaner_log_dtls;
    

  
END gm_pkg_op_loaner_request_rpt;
/