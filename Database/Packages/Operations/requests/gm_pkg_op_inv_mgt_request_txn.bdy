--@"C:\projects\Branches\jboss-migration\Database\Packages\Operations\requests\gm_pkg_op_inv_mgt_request_txn.bdy";
CREATE OR REPLACE PACKAGE BODY gm_pkg_op_inv_mgt_request_txn
IS
  /**********************************************************************
   * Description : 
   * Author 	 : Elango
   **********************************************************************/
   PROCEDURE gm_sav_request_info (
	    p_case_id	       IN	   t7100_case_information.c7100_case_id%TYPE
	  , p_case_info_id	   IN	   t7100_case_information.c7100_case_info_id%TYPE
	  , p_surgery_date 	   IN	   t7100_case_information.c7100_surgery_date%TYPE
	  , p_surgery_time	   IN	   VARCHAR2
	  , p_dist_id	       IN	   t7100_case_information.c701_distributor_id%TYPE
	  , p_rep_id		   IN	   t7100_case_information.c703_sales_rep_id%TYPE
	  , p_account_id	   IN	   t7100_case_information.c704_account_id%TYPE
	  , p_status		   IN	   t7100_case_information.c901_case_status%TYPE
	  , p_type	 		   IN	   t7100_case_information.c901_type%TYPE
	  , p_personal_note	   IN	   t7100_case_information.c7100_personal_notes%TYPE
	  , p_user			   IN	   t7100_case_information.c7100_created_by%TYPE
	  , p_out_case_id	   OUT	   t7100_case_information.c7100_case_id%TYPE
	  , p_out_case_infoid  OUT	   t7100_case_information.c7100_case_info_id%TYPE
	)
   AS	
       v_case_id	   VARCHAR2 (100) := NULL;
	   v_rep_id  t7100_case_information.c703_sales_rep_id%TYPE;
	   v_surgery_date  t7100_case_information.c7100_surgery_date%TYPE;
	   v_surgery_time VARCHAR2 (40);
   BEGIN   	   	    	    
		--validating code lookup values in EVETYP Group.
		gm_pkg_pdpc_prodcattxn.gm_validate_code_id(p_type,'EVETYP','-20638');  
		
		v_surgery_time := p_surgery_time;
		v_surgery_date := TRUNC(p_surgery_date);
		
		--1006506- Set request --1006507- Item request 
		IF p_type = 1006506 OR p_type = 1006507 
		THEN
			v_rep_id := TO_NUMBER(p_dist_id);
			v_surgery_date := TRUNC(CURRENT_DATE);
		ELSIF  p_type = 107286  --107286 Account Item Consignment
		THEN
		   v_surgery_date := TRUNC(CURRENT_DATE);
			v_rep_id := TO_NUMBER(NVL(p_account_id,0));
		ELSE
			v_rep_id := TO_NUMBER(p_rep_id);
		END IF;

		--Get the request type case key.
		IF p_case_id IS NULL AND p_case_info_id IS NULL
		THEN
	    	gm_fch_next_case_key (v_surgery_date, p_type, v_rep_id, v_case_id); 
	    END IF;
	    
	    --To generate case id we need surgery date, but this should be NULL in t7100.thats why v_surgery_date is mnade as NULL here. 
	    IF p_type = 1006506 OR p_type = 1006507 OR p_type = 107286  --107286 Account Item Consignment
		THEN
			v_surgery_date := NULL;
			v_surgery_time := NULL;
		END IF;

	    --Save/ update the request.
	    gm_sav_case_info( NVL(p_case_id, v_case_id )
	   					, p_case_info_id
					    , v_surgery_date
					    , v_surgery_time
					    , p_dist_id
					    , p_rep_id
					    , p_account_id
					    , p_status
					    , p_type
					    , p_personal_note
					    , p_user
					    , p_out_case_infoid);
	    				    
		IF p_case_info_id IS NOT NULL AND v_case_id IS NULL
		THEN
			v_case_id := get_case_id(p_out_case_infoid);			
		END IF;
		
	    p_out_case_id := NVL(p_case_id, v_case_id );
	    
	    --p_out_case_infoid := p_out_case_infoid;
	    
   END gm_sav_request_info;
   
   /**********************************************************************
   * Description :  Used to generate the case id
   * Author 	 : Elango
  **********************************************************************/	
	PROCEDURE gm_fch_next_case_key (
	    p_start_date 	   IN	   t7100_case_information.c7100_surgery_date%TYPE
	  , p_type	 		   IN	   t7100_case_information.c901_type%TYPE
	  , p_ref_id		   IN	   t7100_case_information.c703_sales_rep_id%TYPE
	  , p_case_id		   OUT	   t7100_case_information.c7100_case_id%TYPE
	)
	AS
	v_seq 	NUMBER;
	v_id    t906_rules.c906_rule_value%TYPE;
	BEGIN 
		v_id := GET_RULE_VALUE(p_type,'DRETYP');
		
		SELECT NVL (MAX(TO_NUMBER (SUBSTR (C7100_CASE_ID,INSTR(C7100_CASE_ID, '-', -1)+1))), 0) +1  			
		  INTO v_seq
		  FROM t7100_case_information
		 WHERE c7100_case_id LIKE v_id||'-' || p_ref_id || '-' || TO_CHAR (p_start_date, 'yyyymmdd') || '%'
		   AND c7100_void_fl IS NULL 
		   AND c901_type = p_type; 
           
        -- MIM ID Allotment
		p_case_id	:=   v_id
		                || '-' 
		    			|| p_ref_id
		    			|| '-' 
		                || TO_CHAR (p_start_date, 'yyyymmdd')	
		    			|| '-'
		    			|| v_seq; 
		    			
	END gm_fch_next_case_key;
 /**********************************************************************
   * Description : used to save the request info's.
   * Author 	 : Elango
  **********************************************************************/	
	PROCEDURE gm_sav_case_info (
	    p_case_id		   IN	   t7100_case_information.c7100_case_id%TYPE
	  , p_case_info_id	   IN	   t7100_case_information.c7100_case_info_id%TYPE
	  , p_surgery_date 	   IN	   t7100_case_information.c7100_surgery_date%TYPE
	  , p_surgery_time	   IN	   VARCHAR2
	  , p_dist_id	       IN	   t7100_case_information.c701_distributor_id%TYPE
	  , p_rep_id		   IN	   t7100_case_information.c703_sales_rep_id%TYPE
	  , p_account_id	   IN	   t7100_case_information.c704_account_id%TYPE
	  , p_status		   IN	   t7100_case_information.c901_case_status%TYPE
	  , p_type	 		   IN	   t7100_case_information.c901_type%TYPE
	  , p_personal_note	   IN	   t7100_case_information.c7100_personal_notes%TYPE
	  , p_user			   IN	   t7100_case_information.c7100_created_by%TYPE
	  , p_out_case_infoid  OUT	   t7100_case_information.c7100_case_info_id%TYPE
	)
	AS
	v_company_id  t1900_company.c1900_company_id%TYPE;
	v_date_fmt				VARCHAR2(20);
	BEGIN
		
		SELECT  get_compid_frm_cntx(),get_compdtfmt_frm_cntx()
	INTO	 v_company_id,v_date_fmt
	FROM	dual;
	
		
	    -- Updating event Info for existing case Info ID 
		UPDATE t7100_case_information
		   SET c7100_case_id = NVL( p_case_id, c7100_case_id)
		     , c7100_surgery_date = p_surgery_date
		     , c7100_surgery_time =  to_timestamp_tz(p_surgery_time, 'MM/DD/YYYY HH24:MI:SS TZH:TZM')	
		     , c701_distributor_id = p_dist_id
		     , c703_sales_rep_id = p_rep_id
		     , c704_account_id = p_account_id
		     , c901_case_status = p_status	
		     , c7100_personal_notes = p_personal_note
		     , c7100_last_updated_by = p_user	
		     , c7100_last_updated_date = CURRENT_DATE
		 WHERE c7100_case_info_id 	= p_case_info_id 
		   AND c7100_void_fl IS NULL 
		   AND c901_type = p_type;
		   
		 
		-- Inserting new record if no record updated from above.
		IF (SQL%ROWCOUNT = 0)
		THEN
			SELECT s7100_case_information.NEXTVAL
			  INTO p_out_case_infoid
			  FROM DUAL; 
			  
			INSERT INTO t7100_case_information
						(c7100_case_info_id, c7100_case_id, c7100_surgery_date, 	c7100_surgery_time,	
						c701_distributor_id, c703_sales_rep_id, c704_account_id, c901_case_status , 
						c901_type, c7100_personal_notes, c7100_created_by, c7100_created_date,c1900_company_id
						)
				 VALUES (p_out_case_infoid, p_case_id, p_surgery_date,  to_timestamp_tz(p_surgery_time, v_date_fmt||' HH24:MI:SS TZH:TZM')	,  
					     p_dist_id, p_rep_id, p_account_id, p_status, 
					     p_type, p_personal_note, p_user, CURRENT_DATE,v_company_id
						);  
		END IF;			

		p_out_case_infoid := NVL(p_case_info_id, p_out_case_infoid);
		
	END gm_sav_case_info;   
	
  /**********************************************************************
   * Description : used to save the request ship info's.
   * Author 	 : Elango
  **********************************************************************/	
	PROCEDURE gm_update_request_ship (
	    p_case_info_id	   IN	   t7100_case_information.c7100_case_info_id%TYPE
	  , p_type	 		   IN	   t7100_case_information.c901_type%TYPE
	)
	AS    
	    v_req_dtl_id      NUMBER;
		v_req_id      	  T526_PRODUCT_REQUEST_DETAIL.C525_PRODUCT_REQUEST_ID%TYPE;
		v_count NUMBER   :=1;
		v_void_fl VARCHAR2(1);
		v_req_source      NUMBER;
		
		CURSOR c_case_set_shipinfo
		IS
			SELECT c7104_case_set_id casesetid, c526_required_date reqdt, c907_ship_to_dt shiptodt
                  ,c901_source shipsrc, c901_ship_to shipto, c907_ship_to_id shiptoid
                  ,c901_delivery_carrier shipcarrier, c901_delivery_mode shipmode, c106_address_id shipaddress
                  ,c907_override_attn_to attnto, c907_ship_instruction shipinstruction
             FROM my_temp_case_ship_det;		    
		       
	BEGIN			
			
		FOR v_rec IN c_case_set_shipinfo
		LOOP

			SELECT C526_PRODUCT_REQUEST_DETAIL_ID 
 			  INTO v_req_dtl_id
              FROM T7104_CASE_SET_INFORMATION
             WHERE C7104_CASE_SET_ID = v_rec.casesetid;
	             
			IF v_count= 1
			THEN
		          	SELECT t525.c525_product_request_id, DECODE( t525.c901_request_for, 4127, 50185, 50181) -- 4127, 50185 is for loaner / prod loaner
		          	  INTO v_req_id, v_req_source
		           	  FROM T526_PRODUCT_REQUEST_DETAIL t526, t525_product_request t525
		          	 WHERE t526.c525_product_request_id = t525.c525_product_request_id
                     AND c526_product_request_detail_id = v_req_dtl_id 
                     AND t525.c525_void_fl IS NULL
   					 AND t526.c526_void_fl IS NULL;

          	     	gm_update_request_shipinfo(TO_CHAR(v_req_id)
					          	, v_rec.shiptodt
					          	, v_req_source
					          	, v_rec.shipto
					          	, v_rec.shiptoid
					          	, v_rec.shipcarrier
					          	, v_rec.shipmode
					          	, v_rec.shipaddress
					          	, v_rec.attnto
					          	, v_rec.shipinstruction
					          	, v_void_fl
					          	, 'N'   --To update only the 525 records.
					          	);	
	        END IF;	 
	        
	        IF p_type = 1006507 OR p_type = 107286  -- 1006507--Item consignment,107286--Account Item consignment
	        THEN
	        	v_void_fl := NULL;
	        	--v_void_fl := 'Y';
	        ELSE
		        v_void_fl := NULL;
          	END IF;	
          	
          	gm_update_request_shipinfo(TO_CHAR(v_req_dtl_id)
							          	, v_rec.shiptodt
							          	, v_rec.shipsrc
							          	, v_rec.shipto
							          	, v_rec.shiptoid
							          	, v_rec.shipcarrier
							          	, v_rec.shipmode
							          	, v_rec.shipaddress
							          	, v_rec.attnto
							          	, v_rec.shipinstruction
							          	, v_void_fl
							          	,''
							          	);
    			   
          	v_count := v_count+1;
		 IF p_type = 1006506  OR p_type = 1006507 OR p_type = 107286 --Set/Item consignment,107286--Account Item consignment
	     THEN
          	--Updating the planned ship date for Consignmetn Set/Item.
          	 UPDATE t526_product_request_detail
				SET c526_planned_ship_dt           = v_rec.shiptodt
			  WHERE c526_product_request_detail_id = v_req_dtl_id
			    AND c526_void_fl                  IS NULL;
		 END IF;	
		END LOOP;	
	END gm_update_request_ship;
	
  /**********************************************************************
   * Description : used to save the request ship info's.
   * Author 	 : Elango
  **********************************************************************/		
	PROCEDURE gm_update_request_shipinfo (
	     p_ref_id	       IN	   t907_shipping_info.c907_ref_id%TYPE
	  ,  p_ship_to_dt	   IN	   t907_shipping_info.c907_ship_to_dt%TYPE
	  ,  p_ship_source     IN	   t907_shipping_info.c901_source%TYPE
	  ,  p_ship_to	       IN	   t907_shipping_info.c901_ship_to%TYPE
	  ,  p_ship_to_id  	   IN	   t907_shipping_info.c907_ship_to_id%TYPE
	  ,  p_ship_carrier	   IN	   t907_shipping_info.c901_delivery_carrier%TYPE
	  ,  p_ship_mode	   IN	   t907_shipping_info.c901_delivery_mode%TYPE
	  ,  p_ship_address    IN	   t907_shipping_info.c106_address_id%TYPE
	  ,  p_ship_attnto     IN	   t907_shipping_info.c907_override_attn_to%TYPE
	  ,  p_ship_instruction IN	   t907_shipping_info.c907_ship_instruction%TYPE
	  ,  p_void_fl	       IN	   t907_shipping_info.c907_void_fl%TYPE
	  ,  p_active_fl	   IN	   t907_shipping_info.c907_active_fl%TYPE
	)
	AS
	BEGIN
	
	--While updating the t525 records it is updating the 526 record with the same sequence.to avoid adding the is Flag Check.
			UPDATE t907_shipping_info 
					   SET c907_ship_to_dt =  p_ship_to_dt			   
					     , c901_source = p_ship_source
					     , c901_ship_to =  p_ship_to
					     , c907_ship_to_id =  p_ship_to_id
					     , c901_delivery_carrier =  p_ship_carrier
					     , c901_delivery_mode =  p_ship_mode
					     , c106_address_id =  p_ship_address
					     , c907_override_attn_to =  p_ship_attnto
					     , c907_ship_instruction =  p_ship_instruction
					     , c907_void_fl = p_void_fl
					     , c907_last_updated_date = CURRENT_DATE
					 WHERE c907_ref_id = p_ref_id
					   AND NVL(c907_active_fl,'-9999') = NVL(p_active_fl,'-9999') 
					   AND c907_void_fl IS NULL;
					   
	END gm_update_request_shipinfo;
		
	/****************************************************************************************************
	 * Description    : This function will return set turns for completed 3 month
	 *                  Set ID and Distributor ID passed as in parameter
	 *                  Will calculate set tunrs for completed 3 months
	 *  Parameters    : p_set_id -- Should be consignment set
	 *                : p_distributor_id    -- Distributor ID
	 *******************************************************************************************************/
	FUNCTION get_set_turns (
	    p_set_id    t207_set_master.c207_set_id%TYPE
	  , p_dist_id   t701_distributor.c701_distributor_id%TYPE
	)
	    RETURN NUMBER
	IS
	    v_associated_system_id t207_set_master.c207_set_id%TYPE;
	    v_associated_system_name t207_set_master.c207_set_nm%TYPE;
	
	    v_baseline_set_count NUMBER;
	    v_procedure_count NUMBER;
	
	    v_start_date   DATE;
	    v_end_date     DATE;
	
	BEGIN
	    -- GET 3 month date range
	    SELECT ADD_MONTHS (TRUNC (CURRENT_DATE, 'mm'), -3), LAST_DAY (ADD_MONTHS (TRUNC (CURRENT_DATE, 'mm'), -1))
	      INTO v_start_date, v_end_date
	      FROM DUAL;
	
	    -- Get Associated System
	    SELECT v207a.c207_set_id, v207a.c207_set_nm
	      INTO v_associated_system_id, v_associated_system_name
	      FROM v207a_set_consign_link v207a
	     WHERE v207a.c207_actual_set_id = p_set_id;
	
	    -- GET BASELINE SET COUNT
	    SELECT COUNT (1)
	      INTO v_baseline_set_count
	      FROM t730_virtual_consignment
	     WHERE c207_set_id IN (
	               SELECT v207a2.c207_actual_set_id
	                 FROM v207a_set_consign_link v207a1, v207a_set_consign_link v207a2, t207_set_master t207
	                WHERE v207a1.c207_set_id = v207a2.c207_set_id
	                  AND v207a2.c207_actual_set_id = t207.c207_set_id
	                  AND v207a1.c207_actual_set_id = p_set_id
	                  AND v207a2.c901_link_type = 20100   -- baseline set
	                  AND t207.c207_type = 4070   -- consignment
	               )
	       AND c701_distributor_id = p_dist_id;
	
	    -- Get 3 month Procedure count, Will exclude bill only loaner (2530)
	    SELECT COUNT (t501b.c501_order_id) / 3
	      INTO v_procedure_count
	      FROM t501b_order_by_system t501b
	         , t501_order t501
	         , (SELECT DISTINCT rep_id, d_id
	                       FROM v700_territory_mapping_detail
	                      WHERE d_id = p_dist_id) v700
	     WHERE t501b.c501_order_id = t501.c501_order_id
	       AND v700.rep_id = t501.c703_sales_rep_id
	       AND t501b.c207_system_id = v_associated_system_id
	       AND t501.c501_order_date > = v_start_date   -- # change based on number of days
	       AND t501.c501_order_date <= v_end_date
	       AND t501.c501_void_fl IS NULL
	       AND t501.c501_delete_fl IS NULL
	       AND NVL (t501.c901_order_type, -9999) NOT IN ('2533', '2518', '2519', '2530')
	       AND t501b.c501b_void_fl IS NULL;
		
		-- If no baseline set then return zero 
	    IF (v_baseline_set_count = 0) 
		THEN 
			RETURN 0;
		END IF;
		--
		RETURN v_procedure_count / v_baseline_set_count;
		
	EXCEPTION
	    WHEN NO_DATA_FOUND
	    THEN
	        RETURN 0;
	END get_set_turns;
	
	/****************************************************************************************************
	 * Description    : This function will return set turns Approved/Rejected Status 
	 *                  If Approved will Pass A
	 *                  if Rejected will Pass R
	 *  Parameters    : p_set_id -- Should be consignment set
	 *                : p_distributor_id    -- Distributor ID
	 *******************************************************************************************************/
	FUNCTION get_set_turns_check (
	    p_set_id    t207_set_master.c207_set_id%TYPE
	  , p_dist_id   t701_distributor.c701_distributor_id%TYPE
	  , p_apprv_rule_id VARCHAR2
	)
	    RETURN VARCHAR
	IS
	
	    v_associated_system_id t207_set_master.c207_set_id%TYPE;
	    v_associated_system_name t207_set_master.c207_set_nm%TYPE;
	    --
	    v_cons_loan_set_turn_fl VARCHAR2(20);
	    v_system_set_turn NUMBER;
	    v_dist_set_turns  NUMBER;
	    --
	    v_start_date   DATE;
	    v_end_date     DATE;
	--
	BEGIN
	
	    -- Get Associated System
	    SELECT v207a.c207_set_id, v207a.c207_set_nm
	      INTO v_associated_system_id, v_associated_system_name
	      FROM v207a_set_consign_link v207a
	     WHERE v207a.c207_actual_set_id = p_set_id;
	
	    -- Get System level (override) turn if any 
		SELECT TO_NUMBER(GET_RULE_VALUE(v_associated_system_id,'SET_TURNS'))
		  INTO v_system_set_turn 	
		  FROM DUAL;
		
		-- If no override then get Global Set Turns 
		IF (v_system_set_turn IS NULL) 
		THEN 
			--
			SELECT TO_NUMBER(GET_RULE_VALUE('ALL','SET_TURNS'))
			  INTO v_system_set_turn 	
			  FROM DUAL;
			--
		END IF;
		
		-- Get Distributor Set Turn 
		SELECT gm_pkg_op_inv_mgt_request_txn.get_set_turns(p_set_id, p_dist_id) 
		  INTO v_dist_set_turns  	
		  FROM DUAL;
		  
		-- Set turn auto approve can be enabled/disabled based on the below rule value.
		SELECT GET_RULE_VALUE(p_apprv_rule_id,'CONS_SET_TURNS')
			  INTO v_cons_loan_set_turn_fl 	
			  FROM DUAL;
		
		-- If the v_cons_loan_set_turn_fl is NULL , we should to auto approval, so return R.
		IF ( v_dist_set_turns < v_system_set_turn ) OR (v_cons_loan_set_turn_fl IS NULL)
		THEN 
			RETURN 'R';
		ELSE 
			RETURN 'A';
		END IF;
		
		
	EXCEPTION
	    WHEN OTHERS
	    THEN
	        RETURN 'R';
	END get_set_turns_check;
	
	/****************************************************************************************
	 * Description        :This procedure called to update loaner status based on set turns 
	 *****************************************************************************************/
	PROCEDURE  gm_sav_req_update(
		  p_prod_req_detail_id  IN       t526_product_request_detail.c526_product_request_detail_id%TYPE
		, p_set_turns			IN       VARCHAR2
		, p_status			    IN       t526_product_request_detail.c526_status_fl%TYPE
		, p_updated_by			IN       t526_product_request_detail.c526_last_updated_by%TYPE
	)
	AS
	BEGIN
	
			IF ( p_set_turns = 'A') 
			THEN 
				--If Approved
				-- Call common procedure to update the status
				gm_pkg_op_loaner_allocation.gm_upd_request_status(p_prod_req_detail_id, p_status, p_updated_by);
				UPDATE t526_product_request_detail
				   SET c526_approve_reject_by = p_updated_by,
					   c526_approve_reject_date = CURRENT_DATE
				 WHERE c526_product_request_detail_id = p_prod_req_detail_id  
				   AND c526_void_fl IS NULL; 	
	
			ELSE
				-- If Rejected			
				gm_pkg_op_loaner_allocation.gm_upd_request_status(p_prod_req_detail_id, p_status, p_updated_by);	
			END IF;
	
	END gm_sav_req_update;

	/****************************************************************************************
	 * Description        : This is used to get pending Loaner approval rqsts and 
	 * 						based on system parameter will either auto approval or 
	 * 						move the same for manual approval 
	 ******************************************************************************************/
	PROCEDURE  gm_sav_auto_appr_loaner_req(	
	  p_case_info_id	   IN	   t7100_case_information.c7100_case_info_id%TYPE
  	, p_status		   	   IN	   VARCHAR2
	, p_appr_prreqdet_ids  OUT     VARCHAR2
	, p_rej_prreqdet_ids   OUT     VARCHAR2
	)
	AS
		CURSOR cur_pending_loaner_approval 
		IS
			SELECT   t526.c526_product_request_detail_id
					, t525.c525_request_for_id
					, t526.c207_set_id
				FROM t525_product_request t525, t526_product_request_detail t526, t7104_case_set_information t7104
			   WHERE t525.c525_product_request_id = t526.c525_product_request_id
	             AND t7104.c526_product_request_detail_id = t526.c526_product_request_detail_id
	             AND t7104.c7100_case_info_id = NVL(p_case_info_id,t7104.c7100_case_info_id)
				 AND t525.c901_request_for = 4127   -- Product_Loaner
				 AND t526.c526_status_fl =  NVL(p_status, t526.c526_status_fl)  -- Pending System approval 
				 AND t526.c526_void_fl IS NULL
			ORDER BY t525.c525_product_request_id;
			
			v_ln_set_id    t207_set_master.c207_set_id%TYPE;
			v_cn_set_id    t207_set_master.c207_set_id%TYPE;
		
			v_dist_id			t701_distributor.c701_distributor_id%TYPE;
			v_set_turns			CHAR(1); 	
			v_prod_req_detail_id	t526_product_request_detail.c526_product_request_detail_id%TYPE;
			v_appr_prreqdet_ids CLOB;
			v_rej_prreqdet_ids  CLOB;
	BEGIN 
		FOR var_pending_loaner_app IN cur_pending_loaner_approval
		LOOP
			
			v_ln_set_id	:= var_pending_loaner_app.c207_set_id;
			v_dist_id	:= var_pending_loaner_app.c525_request_for_id;
			v_prod_req_detail_id := var_pending_loaner_app.c526_product_request_detail_id;
			
			--*******************************************
			-- Get Associated consignment information 
			--*******************************************
		  BEGIN
			SELECT t207b.c207_consign_set_id
			  INTO v_cn_set_id	 	
			  FROM t207b_loaner_set_map t207b
			 WHERE t207b.c207_loaner_set_id = v_ln_set_id;
	
			 SELECT gm_pkg_op_inv_mgt_request_txn.get_set_turns_check(v_cn_set_id, v_dist_id,'LOAN_SET') 
			   INTO v_set_turns  	
			   FROM DUAL;			
	      EXCEPTION WHEN NO_DATA_FOUND
	      THEN
	       		v_set_turns :='R';
	      END;
	      
	      	--Form the output string based on the Set Turns
	      	IF ( v_set_turns = 'A') THEN 
				 v_appr_prreqdet_ids := v_appr_prreqdet_ids || v_prod_req_detail_id ||',';
				 gm_sav_req_update (v_prod_req_detail_id, v_set_turns, 10, 30301);
			ELSE
				 v_rej_prreqdet_ids := v_rej_prreqdet_ids || v_prod_req_detail_id ||',';	
				 gm_sav_req_update (v_prod_req_detail_id, v_set_turns, 5, 30301);	
			END IF;
				
			--Call Loaner allocation prc if set turn is approved.
			IF ( v_set_turns = 'A') THEN 
				 gm_pkg_op_loaner.gm_sav_allocate(v_ln_set_id,4127, 30301);	
			END IF;	
		END LOOP;	
		
		p_appr_prreqdet_ids  := SUBSTR(v_appr_prreqdet_ids, 0, LENGTH(v_appr_prreqdet_ids)-1) ;
		p_rej_prreqdet_ids   := SUBSTR(v_rej_prreqdet_ids, 0, LENGTH(v_rej_prreqdet_ids)-1);
		
	END;
	/****************************************************************************************
	 * Description        : This is used to get pending set approval rqsts and 
	 * 						based on system parameter will either auto approval or 
	 * 						move the same for manual approval 
	 ******************************************************************************************/	
	PROCEDURE  gm_sav_auto_appr_set_req(
		  p_case_info_id	   IN	   t7100_case_information.c7100_case_info_id%TYPE
	  	, p_status		   	   IN	   VARCHAR2
		, p_appr_prreqdet_ids  OUT     VARCHAR2
		, p_rej_prreqdet_ids   OUT     VARCHAR2
	)AS
		CURSOR cur_pending_set_approval 
		IS
			SELECT   t526.c526_product_request_detail_id
					, t525.c525_request_for_id
					, t526.c207_set_id
				FROM t525_product_request t525, t526_product_request_detail t526, t7104_case_set_information t7104
			   WHERE t525.c525_product_request_id = t526.c525_product_request_id
	             AND t7104.c526_product_request_detail_id = t526.c526_product_request_detail_id
	             AND t7104.c7100_case_info_id = NVL(p_case_info_id,t7104.c7100_case_info_id)
				 AND t525.c901_request_for = 400087   -- Set Request 
				 AND t526.c526_status_fl = NVL(p_status, t526.c526_status_fl) -- Pending System approval 
				 AND t526.c207_set_id IS NOT NULL
				 AND t526.c526_void_fl IS NULL
			ORDER BY t525.c525_product_request_id;
			
		v_cn_set_id    t207_set_master.c207_set_id%TYPE;
		v_dist_id			t701_distributor.c701_distributor_id%TYPE;
		v_set_turns			CHAR(1); 	
		v_prod_req_detail_id	t526_product_request_detail.c526_product_request_detail_id%TYPE;
		v_appr_prreqdet_ids CLOB;
		v_rej_prreqdet_ids  CLOB;
	
	BEGIN
		FOR var_pending_set_app IN cur_pending_set_approval
		LOOP			
			v_cn_set_id	:= var_pending_set_app.c207_set_id;
			v_dist_id	:= var_pending_set_app.c525_request_for_id;
			v_prod_req_detail_id := var_pending_set_app.c526_product_request_detail_id;
			
			 SELECT gm_pkg_op_inv_mgt_request_txn.get_set_turns_check(v_cn_set_id, v_dist_id,'CONS_SET') 
			   INTO v_set_turns  	
			   FROM DUAL;
			   
			--Form the output string based on the Set Turns
			IF ( v_set_turns = 'A') 
			THEN 	
				v_appr_prreqdet_ids := v_appr_prreqdet_ids || v_prod_req_detail_id ||',';
				gm_sav_req_update (v_prod_req_detail_id, v_set_turns, 10, 30301);	
			ELSE
				v_rej_prreqdet_ids := v_rej_prreqdet_ids || v_prod_req_detail_id ||',';
				gm_sav_req_update (v_prod_req_detail_id, v_set_turns, 5, 30301);	
			END IF;		
		END LOOP;
				
		p_appr_prreqdet_ids  := SUBSTR(v_appr_prreqdet_ids, 0, LENGTH(v_appr_prreqdet_ids)-1) ;
		p_rej_prreqdet_ids   := SUBSTR(v_rej_prreqdet_ids, 0, LENGTH(v_rej_prreqdet_ids)-1);
	END;
	/****************************************************************************************
	 * Description        : This is used to get pending item rqsts and 
	 * 						based on system parameter will either auto approval or 
	 * 						move the same for manual approval 
	 ******************************************************************************************/	
	PROCEDURE  gm_sav_auto_appr_item_req(
		  p_case_info_id	   IN	   t7100_case_information.c7100_case_info_id%TYPE
	  	, p_status		   	   IN	   VARCHAR2
		, p_appr_prreqdet_ids  OUT     VARCHAR2
		, p_rej_prreqdet_ids   OUT     VARCHAR2
	)
	AS
	v_request_id VARCHAR2(4000);
	v_con_id	 VARCHAR2(4000);
	-- Added for PMT-29408, to pass account item consignment type
		CURSOR cur_pending_item_approval(p_req_for t525_product_request.c901_request_for%TYPE) 
		IS
			SELECT     t526.c526_product_request_detail_id
					 , t525.c525_request_for_id
					 , t526.c205_part_number_id
					 , t205.c205_product_family
					 , t526.c526_qty_requested
				FROM   t525_product_request t525, t526_product_request_detail t526, t7104_case_set_information t7104
					 , t205_part_number t205 
			   WHERE t525.c525_product_request_id = t526.c525_product_request_id
	             AND t7104.c526_product_request_detail_id = t526.c526_product_request_detail_id
	             AND t7104.c7100_case_info_id = NVL(p_case_info_id,t7104.c7100_case_info_id)
				 AND t525.c901_request_for = p_req_for   
				 AND t526.c526_status_fl = NVL(p_status, t526.c526_status_fl) -- Pending System approval 
				 AND t526.c205_part_number_id IS NOT NULL
				 AND t526.c526_void_fl IS NULL
				 AND t526.c205_part_number_id = t205.c205_part_number_id
			ORDER BY t525.c525_product_request_id;

		v_set_turns			CHAR(1); 	
	    v_global_liter_turn NUMBER;    
		v_part_number_id	t205_part_number.c205_part_number_id%TYPE;
		v_dist_id			t701_distributor.c701_distributor_id%TYPE;
		v_product_family	t205_part_number.c205_product_family%TYPE;
		v_prod_req_detail_id	t526_product_request_detail.c526_product_request_detail_id%TYPE;
		v_requested_qty		t526_product_request_detail.c526_qty_requested%TYPE;
		v_appr_prreqdet_ids CLOB;
		v_rej_prreqdet_ids  CLOB;
		v_price  T205_PART_NUMBER.C205_CONSIGNMENT_PRICE%TYPE;
		v_req_for    t7100_case_information.C901_TYPE%TYPE;      
	BEGIN 
	
	SELECT TO_NUMBER(GET_RULE_VALUE('ALL','ITEM_LIT_AUTO_VALUE'))
	  INTO v_global_liter_turn 	
	 FROM DUAL;
	 
	 -- to fetch case request type 
	BEGIN
	 SELECT DECODE(C901_TYPE,'107286','40050','400088')--400088 Item Consignment,40050 Hospital Consignment
	 INTO v_req_for
	 FROM T7100_CASE_INFORMATION 
	 WHERE C7100_CASE_INFO_ID = p_case_info_id
	 AND C7100_VOID_FL IS NULL;
		EXCEPTION WHEN NO_DATA_FOUND THEN
			v_req_for := '400088';
		END;
	 

	FOR var_pending_item_app IN cur_pending_item_approval(v_req_for) 
	LOOP
		
		v_part_number_id	:= var_pending_item_app.c205_part_number_id;
		v_dist_id			:= var_pending_item_app.c525_request_for_id;
		v_product_family	:= var_pending_item_app.c205_product_family;
		v_prod_req_detail_id := var_pending_item_app.c526_product_request_detail_id;
		v_requested_qty		:= var_pending_item_app.c526_qty_requested;
		v_set_turns := NULL;
		
		SELECT NVL(get_part_price(v_part_number_id, 'C'),0) INTO v_price FROM DUAL;
		
		--Form the output string based on the Set Turns
		-- below v_price condition is add for remove AD item cosignment email notification  
			IF ( v_product_family = 4056 and v_requested_qty <= v_global_liter_turn and v_price > 0)  --Litreature & req Qty is < 10(10 is defined in rules)
			THEN
			    ---Set the status as 5-Pending PD Approval whenever Literature part don't have consignment price. 
			    --Else Approve the request. 
					v_appr_prreqdet_ids := v_appr_prreqdet_ids || v_prod_req_detail_id ||',';
					gm_sav_req_update (v_prod_req_detail_id, 'A', 10 , 30301);				
			ELSE
			    --- Add below v_rej_prreqdet_ids to get item detail without AD approval and remove else condition to avoid AD item cosignment email notification  
				    v_rej_prreqdet_ids := v_rej_prreqdet_ids || v_prod_req_detail_id ||',';
					gm_sav_req_update (v_prod_req_detail_id, 'R', 5 , 30301);		
			END IF;
			
		
	END LOOP;
		p_appr_prreqdet_ids  := SUBSTR(v_appr_prreqdet_ids, 0, LENGTH(v_appr_prreqdet_ids)-1) ;
					
		IF p_appr_prreqdet_ids IS NOT NULL
		THEN
			--To create consign item request for the approved requests.
			gm_pkg_sm_casebook_txn.gm_sav_request_frm_case(p_appr_prreqdet_ids,v_request_id,v_con_id);
		END IF;
		
		p_rej_prreqdet_ids   := SUBSTR(v_rej_prreqdet_ids, 0, LENGTH(v_rej_prreqdet_ids)-1);
	END;
	/****************************************************************************************
	 * Description        : This is used to update void flag for Item/Loaner/Set Initiated 
	 * 						From Device Manually.
	 ******************************************************************************************/	
	PROCEDURE  gm_sav_request_void(
		  p_case_info_id	   IN	   t7100_case_information.c7100_case_info_id%TYPE
	  	, p_user_id		   	   IN	   VARCHAR2
	)
	AS
	BEGIN
	
	UPDATE T7100_CASE_INFORMATION
  	SET C7100_VOID_FL = 'Y',
        C7100_LAST_UPDATED_BY =p_user_id ,
        C7100_LAST_UPDATED_DATE = CURRENT_DATE
  	WHERE C7100_CASE_INFO_ID = p_case_info_id;
  
  	UPDATE T7102_CASE_ATTRIBUTE
  	SET  C7102_VOID_FL = 'Y',
         C7102_LAST_UPDATED_BY = p_user_id,
         C7102_LAST_UPDATED_DATE = CURRENT_DATE
  	WHERE C7100_CASE_INFO_ID = p_case_info_id;
  
  	UPDATE T7104_CASE_SET_INFORMATION
  	SET C7104_VOID_FL = 'Y',
  		C7104_LAST_UPDATED_BY = p_user_id,
      	C7104_LAST_UPDATED_DATE = CURRENT_DATE
  	WHERE C7100_CASE_INFO_ID = p_case_info_id;
  
    UPDATE T526_PRODUCT_REQUEST_DETAIL
  	SET C526_VOID_FL = 'Y',
        C526_LAST_UPDATED_BY = p_user_id,
        C526_LAST_UPDATED_DATE =CURRENT_DATE
  	WHERE C526_PRODUCT_REQUEST_DETAIL_ID IN (SELECT  C526_PRODUCT_REQUEST_DETAIL_ID FROM T7104_CASE_SET_INFORMATION WHERE C7100_CASE_INFO_ID = p_case_info_id);
  
   UPDATE T525_PRODUCT_REQUEST
  	SET C525_VOID_FL = 'Y',
        C525_LAST_UPDATED_BY = p_user_id,
        C525_LAST_UPDATED_DATE = CURRENT_DATE
  	WHERE C525_PRODUCT_REQUEST_ID IN
  	(
  		SELECT C525_PRODUCT_REQUEST_ID
		 	FROM T526_PRODUCT_REQUEST_DETAIL
				WHERE C526_PRODUCT_REQUEST_DETAIL_ID IN
  				( 
  					SELECT C526_PRODUCT_REQUEST_DETAIL_ID
  						FROM T7104_CASE_SET_INFORMATION
  							WHERE C7100_CASE_INFO_ID = p_case_info_id
  				)
  	);
  	UPDATE T907_SHIPPING_INFO
  	SET C907_VOID_FL = 'Y',
        C907_LAST_UPDATED_BY = p_user_id,
        C907_LAST_UPDATED_DATE = CURRENT_DATE
  	WHERE C525_PRODUCT_REQUEST_ID IN 
  	(
  		SELECT C525_PRODUCT_REQUEST_ID
			FROM T526_PRODUCT_REQUEST_DETAIL
				WHERE C526_PRODUCT_REQUEST_DETAIL_ID IN
  				(
  					SELECT C526_PRODUCT_REQUEST_DETAIL_ID
  						FROM T7104_CASE_SET_INFORMATION
  							WHERE C7100_CASE_INFO_ID = p_case_info_id
  				)
  	);
	END;
		
END gm_pkg_op_inv_mgt_request_txn;
/