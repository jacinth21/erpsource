--@"C:\projects\Branches\jboss-migration\Database\Packages\Operations\requests\gm_pkg_op_inv_mgt_request_rpt.bdy";
CREATE OR REPLACE PACKAGE BODY gm_pkg_op_inv_mgt_request_rpt
IS
  
 /*****************************************************************************************
   * Description : used to fetch the case request info's to do auto approval
   * Author 	 : Elango
  *****************************************************************************************/	
	PROCEDURE  gm_fch_case_request_details (
	    p_status		        IN	 VARCHAR2
	  ,	p_case_loaner_cnt       OUT  NUMBER
	  , p_case_consign_set_cnt  OUT  NUMBER
	  , p_case_consign_itm_cnt  OUT  NUMBER
	)
	AS
		v_count					NUMBER;
	BEGIN
		
		-- Product_Loaner
		BEGIN
		      SELECT COUNT(1)  INTO v_count
			    FROM t525_product_request t525, t526_product_request_detail t526, t7104_case_set_information t7104
			   WHERE t525.c525_product_request_id = t526.c525_product_request_id
	             AND t7104.c526_product_request_detail_id = t526.c526_product_request_detail_id
	             AND t7104.c7100_case_info_id = t7104.c7100_case_info_id
				 AND t525.c901_request_for = 4127  
				 AND t526.c526_status_fl =  NVL(p_status, t526.c526_status_fl)  -- Pending System approval 
				 AND t526.c526_void_fl IS NULL
			ORDER BY t525.c525_product_request_id; 
		EXCEPTION WHEN NO_DATA_FOUND
	    THEN
	       		v_count := 0;
	    END;
	      
	    p_case_loaner_cnt := v_count;
		
	    -- Set Request 
		BEGIN
		      SELECT COUNT(1)     INTO v_count
				FROM t525_product_request t525, t526_product_request_detail t526, t7104_case_set_information t7104
			   WHERE t525.c525_product_request_id = t526.c525_product_request_id
	             AND t7104.c526_product_request_detail_id = t526.c526_product_request_detail_id
	             AND t7104.c7100_case_info_id = t7104.c7100_case_info_id
				 AND t525.c901_request_for = 400087   -- Set Request 
				 AND t526.c526_status_fl = NVL(p_status, t526.c526_status_fl) -- Pending System approval 
				 AND t526.c207_set_id IS NOT NULL
				 AND t526.c526_void_fl IS NULL
			ORDER BY t525.c525_product_request_id;
		EXCEPTION WHEN NO_DATA_FOUND
	    THEN
	       		v_count := 0;
	    END;
	      
	    p_case_consign_set_cnt := v_count;
	      
		-- Item Request 
		BEGIN		
			  SELECT COUNT(1)     INTO v_count
				FROM   t525_product_request t525, t526_product_request_detail t526, t7104_case_set_information t7104
					 , t205_part_number t205 
			   WHERE t525.c525_product_request_id = t526.c525_product_request_id
	             AND t7104.c526_product_request_detail_id = t526.c526_product_request_detail_id
	             AND t7104.c7100_case_info_id = t7104.c7100_case_info_id
				 AND t525.c901_request_for = 400088   -- Item Request 
				 AND t526.c526_status_fl = NVL(p_status, t526.c526_status_fl) -- Pending System approval 
				 AND t526.c205_part_number_id IS NOT NULL
				 AND t526.c526_void_fl IS NULL
				 AND t526.c205_part_number_id = t205.c205_part_number_id
			ORDER BY t525.c525_product_request_id;
		EXCEPTION WHEN NO_DATA_FOUND
	    THEN
	       		v_count := 0;
	    END;
	      
	    p_case_consign_itm_cnt := v_count;
		
    END gm_fch_case_request_details;
    
END gm_pkg_op_inv_mgt_request_rpt;
/