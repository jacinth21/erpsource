--@C:\pmt\Database\Packages\Operations\gm_op_fch_trans_detail.bdy;

CREATE OR REPLACE PACKAGE BODY gm_op_fch_trans_detail
IS
/***********************************************************
 	 *Auther: Mahavishnu 
     * Description : Procedure to validate source transaction to copy target transaction 
     * by Void flag, verify flag and already  source transaction is copied.
     ***********************************************************/
PROCEDURE  gm_validate_trans_type (
 		p_transids   IN CLOB,
 		p_tgt_trans_type  IN T504_CONSIGNMENT.C504_TYPE%TYPE,
        pout     OUT VARCHAR2 )
  AS
     	v_string	   	CLOB := p_transids;
	    v_company_id	t1900_company.c1900_company_id%TYPE;
        v_trans_id 		T504_CONSIGNMENT.C504_CONSIGNMENT_ID%TYPE;
        v_type      	T504_CONSIGNMENT.C504_TYPE%TYPE;
        v_verifyfl    	T504_CONSIGNMENT.c504_VERIFY_FL%TYPE;
        v_voidfl		T504_CONSIGNMENT.c504_VOID_FL%TYPE;
        v_trans_err		VARCHAR2 (3000) DEFAULT NULL;
        v_void_err		VARCHAR2 (3000) DEFAULT NULL;
        v_verify_err	VARCHAR2 (3000) DEFAULT NULL;
        v_copy_err      VARCHAR2 (3000) DEFAULT NULL;
        v_rule_grp      t906_rules.C906_RULE_GRP_ID%TYPE;
        v_count         NUMBER;
        v_copy_trans_id T504_CONSIGNMENT.C504_CONSIGNMENT_ID%TYPE;
        v_copy_voidfl   T504_CONSIGNMENT.c504_VOID_FL%TYPE;
             
	    
        
	 BEGIN
		   SELECT  get_compid_frm_cntx()  INTO v_company_id
			  FROM DUAL;
			
			 
			  WHILE INSTR (v_string, ',') <> 0
			  LOOP
			  v_trans_id := SUBSTR (v_string, 1, INSTR (v_string, ',') - 1);
			  v_string     := SUBSTR (v_string, INSTR (v_string, ',')    + 1);
			
			 BEGIN
				 select CTYPE,VLFL,VDFL 
				 INTO v_type,v_verifyfl,v_voidfl 
				 from
			   (SELECT  C504_TYPE CTYPE, C504_VERIFY_FL VLFL, C504_VOID_FL VDFL 
                   FROM T504_CONSIGNMENT 
                  WHERE  C504_CONSIGNMENT_ID =  v_trans_id AND C1900_COMPANY_ID = v_company_id 
                  UNION
                  SELECT    C412_TYPE CTYPE, C412_VERIFY_FL VLFL, C412_VOID_FL VDFL
                   FROM T412_INHOUSE_TRANSACTIONS
                  WHERE 
                     C412_INHOUSE_TRANS_ID = v_trans_id AND C1900_COMPANY_ID = v_company_id );

               EXCEPTION
             WHEN NO_DATA_FOUND THEN   
             	
                v_trans_err := v_trans_err || v_trans_id || ','; -- Add tansaction id in v_trans_err to show invalid transaction id 
                v_type := NULL;
			  END;
			  		 
			-- If v_type is null then transaction is not valid 
			IF(v_type IS NOT NULL)THEN 
			  
			-- checks transaction type 4116 - Quarantine To Shelf and 400083- Quarantine to Repack and 400085- Quarantine to Returns than rule group QTOSR
			  IF(p_tgt_trans_type = '4116' OR p_tgt_trans_type = '400083' OR p_tgt_trans_type = '400085') THEN
			  
			  	v_rule_grp :='QTOSR'; -- QTOSR- Rule group to checks valid transaction type for Quarantine transaction
			  
			  ELSIF(p_tgt_trans_type = '400063')THEN
			  -- checks transaction type 400063 - Repack to Shelf
			  	v_rule_grp :='RETOS';-- RETOS- Rule group to checks valid transaction type for Repackage transaction
			  
			  	ELSIF(p_tgt_trans_type = '400086') THEN
			  -- checks transaction type 400086 -  Shelf to Returns
			  	v_rule_grp :='FGTORE';  -- FGTORE- Rule group to checks valid transaction type for Returns transaction
			  
			  END IF;

			  -- get counts by rule group type and source transaction type
			  SELECT COUNT(1) INTO v_count FROM T906_RULES WHERE C906_RULE_ID= v_type AND C906_RULE_GRP_ID = v_rule_grp AND C906_VOID_FL IS NULL;

			  -- v_count is 0 then its not a valid transaction to copy 
			  IF(v_count = 0) THEN
			  
			  	v_trans_err := v_trans_err || v_trans_id || ','; --Add tansaction id in v_trans_err to show invalid transaction id 
			  
			  ELSE
			   		IF(v_voidfl = 'Y') THEN -- checks source transaction is voided 
			 
			 			 v_void_err := v_void_err || v_trans_id || ',';--Add tansaction id in v_void_err to show transaction voided validation
			 			 
					ELSE
					
						 IF(v_verifyfl is  NULL) THEN-- checks source transaction is verified 
			 	 		
			 					 v_verify_err := v_verify_err || v_trans_id || ',';--Add tansaction id in v_verify_err to show transaction verify validation
			 		 
			 				 END IF;
			  
			 		 END IF;
			  
			 		 
			 		 		 BEGIN 
				  -- to checks source transaction is already copied 
					 SELECT C414_TARGET_TXN_ID INTO v_copy_trans_id FROM t414_transaction_log WHERE C414_SOURCE_TXN_ID = v_trans_id AND C414_VOID_FL IS NULL;
					  EXCEPTION
             		 WHEN NO_DATA_FOUND THEN   
             	
                	 v_copy_trans_id := NULL;
				  
			  	END;
			  
			  	-- v_copy_trans_id is not null then transaction is already copied  
			  	IF(v_copy_trans_id IS NOT NULL) THEN
			  	
			  	BEGIN
				  	-- to check target transaction is voided 
			  		SELECT NVL(VDFL,NULL) INTO v_copy_voidfl	FROM
			 			 (SELECT   C504_VOID_FL  VDFL
               		    FROM T504_CONSIGNMENT 
               		   WHERE  C504_CONSIGNMENT_ID =  v_copy_trans_id  
               		   UNION
                 		 SELECT   C412_VOID_FL VDFL
                  		 FROM T412_INHOUSE_TRANSACTIONS
                 		 WHERE 
                  		   C412_INHOUSE_TRANS_ID = v_copy_trans_id);
                 	 	EXCEPTION
            		 WHEN NO_DATA_FOUND THEN   
             	
             			  v_copy_voidfl:= NULL;
				  END;	   
                     
                  		   
                  	IF(v_copy_voidfl = 'Y') THEN
    				-- v_copy_voidfl is Y then update t414_transaction_log void flag as Y
                  	
                  	UPDATE t414_transaction_log SET C414_VOID_FL='Y' WHERE C414_TARGET_TXN_ID = v_copy_trans_id AND C414_SOURCE_TXN_ID= v_trans_id;
                  	
                  	ELSE
                  		v_copy_err:= v_copy_err || v_trans_id || ',';-- target transaction is not voided, shows already transaction copied validation
                  	END IF;
			  
                END IF;
			 		 
			 	 
			 END IF;
                
			  END IF; 
			  END LOOP;
			 
			  -- If all transactions are valid pass out as empty
			  IF(v_trans_err IS NULL AND v_void_err IS NULL AND v_verify_err IS NULL AND v_copy_err IS NULL)THEN
			  	pout := '';
			  ELSE 
			  	pout := v_trans_err || '|' || v_void_err || '|' || v_verify_err || '|' || v_copy_err;
			  END IF;

END gm_validate_trans_type;

 /***********************************************************
 	 *Auther: Mahavishnu 
     * Description : Procedure to save copied transaction records in t414_transaction_log table
     ***********************************************************/
  PROCEDURE  gm_save_trans_details (
 		p_target_id  IN T414_TRANSACTION_LOG.C414_TARGET_TXN_ID%TYPE,
 		p_source_ids    IN CLOB,
 		p_userid	IN T414_TRANSACTION_LOG.C414_CREATED_BY%TYPE)
  AS
     	v_string	   	CLOB := p_source_ids;
        v_trans_id 		T414_TRANSACTION_LOG.C414_SOURCE_TXN_ID%TYPE;
        
             
	    
        
	 BEGIN
		  WHILE INSTR (v_string, ',') <> 0
			  LOOP
			  v_trans_id := SUBSTR (v_string, 1, INSTR (v_string, ',') - 1);
			  v_string     := SUBSTR (v_string, INSTR (v_string, ',')    + 1);
			  
			  INSERT INTO t414_transaction_log (C414_TRANSACTION_SOURCE,C414_TARGET_TXN_ID,C414_SOURCE_TXN_ID,C414_CREATED_DATE,C414_CREATED_BY,C414_LAST_UPDATED_DATE_UTC)
			  VALUES(s414_transaction_log.nextval,p_target_id,v_trans_id,SYSDATE,p_userid,CURRENT_DATE);
			  
			  END LOOP;
			
END gm_save_trans_details;

END gm_op_fch_trans_detail;
/