/* Formatted on 2009/05/06 14:44 (Formatter Plus v4.8.0) */
--@"c:\database\packages\operations\gm_pkg_op_loaner_charges.pkg"

CREATE OR REPLACE PACKAGE gm_pkg_op_loaner_charges
IS

/***********************************************************
 * Description : This Procedure is used to create daily loaner late charges
 * Author	   : Sindhu
 ***********************************************************/
	PROCEDURE gm_sav_loaner_late_charge(
	  	p_date IN DATE DEFAULT CURRENT_DATE,
	  	p_request_id   IN   t525_product_request.c525_product_request_id%TYPE DEFAULT NULL,
	  	p_prod_req_dtl_id   IN   T526_Product_Request_Detail.C526_Product_Request_Detail_Id%TYPE DEFAULT NULL);
	  	
/***********************************************************
 * Description : This Procedure is used to calculate the late fee 
 * Author	   : Sindhu
 ***********************************************************/
	FUNCTION get_loaner_late_fee(
		p_expected_return_date IN Date,
		p_charge_date IN Date
	)
	RETURN NUMBER;

/***********************************************************
 * Description : This Procedure is used to get the working days
 * Author	   : Sindhu
 ***********************************************************/	
	FUNCTION get_work_days_count(
		p_start_date IN DATE,
		p_end_date IN DATE,
		p_comp_id   IN t1900_company.c1900_company_id%TYPE DEFAULT NULL
	) 
	RETURN NUMBER;

/***********************************************************
 * Description : This Procedure is used to save the loaner fee details in incident table
 * Author	   : Sindhu
 ***********************************************************/
	PROCEDURE gm_sav_charge_detail(
		p_incident_id          IN t9201_charge_details.c9200_incident_id%TYPE,
		p_rep_id               IN t9201_charge_details.c703_sales_rep_id%TYPE,
		p_dist_id              IN t9201_charge_details.c701_distributor_id%TYPE,
		p_assoc_rep_id         IN t9201_charge_details.c703_ass_rep_id%TYPE,
		p_user_id              IN t9201_charge_details.c9201_credited_by%TYPE,
		p_late_fee             IN t9201_charge_details.c9201_amount%TYPE,
		p_charge_start_date    IN t9201_charge_details.c9201_charge_start_date%TYPE,
		p_charge_end_date      IN t9201_charge_details.c9201_charge_end_date%TYPE,
		p_charge_details_id    OUT t9201_charge_details.c9201_charge_details_id%TYPE
	);


/***********************************************************
 * Description : This Procedure is used to save the loaner late fee details in charge details table
 * Author	   : Sindhu
 ************************************************************/		
	PROCEDURE gm_sav_incident(
		p_incident_id IN OUT t9200_incident.c9200_incident_id%TYPE,
		p_reftype IN t9200_incident.c9200_ref_type%TYPE,
		p_req_dtl_id IN t9200_incident.c9200_ref_id%TYPE,
		p_incident_type IN t9200_incident.c901_incident_type%TYPE,
		p_incident_value IN t9200_incident.c9200_incident_value%TYPE,
		p_incident_date IN t9200_incident.c9200_incident_date%TYPE,
		p_late_fee IN t9201_charge_details.c9201_amount%TYPE,
		p_status IN t9201_charge_details.c9201_status%TYPE,
		p_user_id IN t9200_incident.c9200_created_by%TYPE,
		p_company_id IN t9200_incident.c1900_company_id%TYPE,
		p_charge_start_date IN t9201_charge_details.c9201_charge_start_date%TYPE,
		p_charge_end_date IN t9201_charge_details.c9201_charge_end_date%TYPE,
		p_charge_details_id OUT t9201_charge_details.c9201_charge_details_id%TYPE
	);
	
/***********************************************************
 * Description : This Procedure is used to save the loaner late fee deduction date
 * Author	   : Gopi
 ************************************************************/		
	PROCEDURE gm_sav_deduction_date(
		p_charge_details_id IN t9201_charge_details.c9201_charge_details_id%TYPE,
		p_statusid          IN T901_CODE_LOOKUP.C901_CODE_NM%TYPE,
		p_user_id           IN t9201_charge_details.c9201_credited_by%TYPE
	);
END gm_pkg_op_loaner_charges;
/
