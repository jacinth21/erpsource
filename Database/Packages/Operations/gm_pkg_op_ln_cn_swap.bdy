/* Formatted on 2010/09/01 17:34 (Formatter Plus v4.8.0) */

--@"C:\Database\Packages\Operations\gm_pkg_op_ln_cn_swap.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_ln_cn_swap
AS
   --
   /******************************************************************
   * Description : This is main Procedure called initially to do loaner to consignment swap
   ****************************************************************/
   PROCEDURE gm_op_sav_loaner_to_consign (
      p_loaner_cn_id   IN       t504_consignment.c504_consignment_id%TYPE,
      p_userid         IN       t101_user.c101_user_id%TYPE,
      p_out_msg        OUT      VARCHAR2
   )
   AS
      v_ln_type                 t504_consignment.c504_type%TYPE;
      v_request_id              t504_consignment.c520_request_id%TYPE;
      v_ln_setid                t504_consignment.c207_set_id%TYPE;
      v_consg_setid             t504_consignment.c207_set_id%TYPE;
      v_ln_status_fl            t504a_consignment_loaner.c504a_status_fl%TYPE;
      v_ln_settyp               t207_set_master.c207_type%TYPE;
      v_out_master_request_id   t520_request.c520_request_id%TYPE;
      v_rep_name                t703_sales_rep.c703_sales_rep_name%TYPE;
      v_out_msg                 VARCHAR2 (1000);
      v_loanid                  VARCHAR2 (20);
      v_count_log_recs          NUMBER;
      v_qty                     NUMBER;
      v_company_id              t1900_company.c1900_company_id%TYPE;
   	  v_plant_id                t5040_plant_master.c5040_plant_id%TYPE;
      v_trans_company_id        t1900_company.c1900_company_id%TYPE;
      v_trans_plant_id          t5040_plant_master.c5040_plant_id%TYPE;
   BEGIN
      -- Fetch loaner type and  loaner status. If not product loaner and loaner not in available/pending return or allocated status then throw error
      SELECT     t504.c504_type lntyp, c520_request_id, t504.c207_set_id,
                 t504.c701_distributor_id, t504.c704_account_id,
                 t504a.c504a_status_fl status_fl, t207.c207_type,
                 t504.c1900_company_id
            INTO v_ln_type, v_request_id, v_ln_setid,
                 g_dist_id, g_acc_id,
                 v_ln_status_fl, v_ln_settyp,v_trans_company_id
            FROM t504_consignment t504,
                 t504a_consignment_loaner t504a,
                 t207_set_master t207
           WHERE t504.c504_status_fl = 4
             AND t504.c504_consignment_id = p_loaner_cn_id
             AND t504a.c504_consignment_id = t504.c504_consignment_id
             AND t207.c207_set_id = t504.c207_set_id
             AND t504a.c504a_void_fl IS NULL
             AND t504.c504_void_fl IS NULL
             AND c207_void_fl IS NULL
      FOR UPDATE;
      
      SELECT  get_compid_frm_cntx(),get_plantid_frm_cntx()
			  INTO  v_company_id,v_plant_id
			  FROM DUAL;
	 -- setting transaction company id into context incase of mismatch  
	  IF (v_trans_company_id IS NOT NULL) AND (v_company_id != v_trans_company_id) THEN
         gm_pkg_cor_client_context.gm_sav_client_context(v_trans_company_id,v_plant_id);
      END IF;     

      IF     v_ln_status_fl != '0'
         AND v_ln_status_fl != '20'
         AND v_ln_status_fl != '5'
         AND v_ln_status_fl = '60'
      THEN
         raise_application_error ('-20924', '');
      --Cannot perform swap as status is invalid
      END IF;

      IF (v_ln_type NOT IN (4127,4119))  --PC-5501 - Added In-House Loaner type(4119) in this condition 
      THEN
         raise_application_error ('-20925', '');
      --Cannot perform swap as loaner type is not product loaner
      END IF;

      -- Fetch the mapped set id
      IF v_ln_settyp = 4074
      THEN                                                          --(loaner)
         BEGIN
            SELECT c207_consign_set_id
              INTO v_consg_setid
              FROM t207b_loaner_set_map
             WHERE c207_loaner_set_id = v_ln_setid AND c207b_void_fl IS NULL;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               v_consg_setid := NULL;
         END;

         IF v_consg_setid IS NULL
         THEN
            raise_application_error ('-20926', '');
         --Cannot proceed. Set id mapping is not done
         END IF;
      ELSE
         v_consg_setid := v_ln_setid;
      END IF;

      SELECT COUNT (1)
        INTO v_count_log_recs
        FROM t504b_loaner_swap_log
       WHERE c504_consignment_id = p_loaner_cn_id 
       AND c504d_void_fl IS NULL
       AND C901_TYPE != 100100;

      IF v_count_log_recs > 0
      THEN
         raise_application_error ('-20927', '');
      END IF;

      -- if status is 20 then fetch shipping detailsto be used later
      IF v_ln_status_fl = '20'
      THEN
         SELECT c703_sales_rep_id
           INTO g_rep_id
           FROM t504a_loaner_transaction
          WHERE c504_consignment_id = p_loaner_cn_id
            AND c504a_return_dt IS NULL
            AND c504a_void_fl IS NULL;
            
        BEGIN
         SELECT c901_ship_to, c907_ship_to_id, c106_address_id
           INTO g_ship_to, g_shipto_id, g_address_id
           FROM t907_shipping_info
          WHERE c907_shipping_id IN (
                   SELECT MAX (c907_shipping_id)
                     FROM t907_shipping_info
                    WHERE c907_ref_id = p_loaner_cn_id
                      AND c901_source = 50182
                      AND c907_void_fl IS NULL);
        EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
	         g_ship_to := NULL;
	         g_shipto_id := NULL;
	         g_address_id :=NULL;
        END;
      END IF;

      -- Reverse posting from Product loaner to FG
      gm_pkg_op_ln_cn_swap.gm_sav_reverse_posting (p_loaner_cn_id,
                                                   v_ln_status_fl,
                                                   v_consg_setid,
                                                   p_userid
                                                  );

      -- If loaner is pending return then accept it or if its already allocated to a request then release it
      IF v_ln_status_fl = '20'
      THEN
      --To avoid account name and sub location type to be null
         gm_pkg_op_ln_swap.gm_cs_sav_accept_return (p_loaner_cn_id,
                                                   p_userid,
                                                   CURRENT_DATE
                                                  );

         v_loanid := gm_pkg_op_loaner.get_loaner_trans_id (p_loaner_cn_id);

         UPDATE t504a_loaner_transaction
            SET c504a_processed_date = TRUNC (CURRENT_DATE),
                c504a_last_updated_date = CURRENT_DATE,
                c504a_last_updated_by = p_userid
          WHERE c504a_loaner_transaction_id = v_loanid;
      ELSIF v_ln_status_fl = '5'
      THEN
         -- RELEASE loaner request
         gm_pkg_op_loaner.gm_sav_deallocate (p_loaner_cn_id, p_userid);
      END IF;

      -- Create new Consignment
      gm_sav_main_cn (p_loaner_cn_id,
                      v_ln_status_fl,
                      v_consg_setid,
                      p_userid,
                      v_out_master_request_id,
                      v_out_msg
                     );
      p_out_msg := v_out_msg;
      --save xtra parts in the loaner set
      gm_sav_xtra_parts_loaner (p_loaner_cn_id,
                                v_ln_status_fl,
                                v_consg_setid,
                                p_userid,
                                v_out_msg
                               );
      p_out_msg := p_out_msg || ' <br> ' || v_out_msg;
	--60:inactive
      gm_pkg_op_loaner_set_txn.gm_upd_consign_loaner_status(p_loaner_cn_id,60,p_userid);
      
--Changes added for PMT-12455 and it's used to trigger an update to iPad when loaner to sonsignment swap is done.
       gm_pkg_op_loaner_set_txn.GM_SAV_AVAIL_LN_SET_UPDATE (p_loaner_cn_id, p_userid); -- consignment_id, user_id, company_id
       
      UPDATE t504_consignment
         SET c701_distributor_id = NULL,
             c504_ship_to = NULL,
             c504_ship_to_id = NULL,
             c504_last_updated_date = CURRENT_DATE,
             c504_last_updated_by = p_userid
       WHERE c504_consignment_id = p_loaner_cn_id;

       -- Voiding Duplicate Backorders for this Loaner with C412_REF_ID from T412 table
		UPDATE T412_INHOUSE_TRANSACTIONS
			SET C412_VOID_FL = 'Y',
				C412_LAST_UPDATED_DATE = CURRENT_DATE,
				C412_LAST_UPDATED_BY = p_userid
		WHERE C412_REF_ID = p_loaner_cn_id
		AND c412_type = '100062'
		AND c412_status_fl ! = 4;
		--The Below Code is added for PMT-5969 and this is used to trigger an update to device when loaner to sonsignment swap is done.
		-- Commented for this changes has handled in PMT-12455 with company id
	/*	v_qty := gm_pkg_op_loaner_set_txn.get_loaner_pool_set_qty(p_loaner_cn_id,v_ln_setid);
		
		IF (v_qty <= 0) THEN
			gm_pkg_op_set_master.gm_sav_set_attribute(v_ln_setid,'104740', 'N', NULL,p_userid);
			gm_pkg_pdpc_prodcattxn.gm_sav_master_data_upd (v_ln_setid, '4000736', 4000412, '103097', '103100',p_userid, NULL,'Y') ;
		END IF;
		*/
      -- reallocate
      IF v_ln_status_fl = '5'
      THEN
         gm_pkg_op_loaner.gm_sav_allocate (v_ln_setid, 4127, p_userid);
      END IF;

      -- Send an email to rep if loaner status is pending return
      IF v_ln_status_fl = 20
      THEN
         gm_lncnswap_send_email (p_loaner_cn_id);
         v_rep_name := get_rep_name (g_rep_id);
         p_out_msg := p_out_msg || ' <br> Email sent to ' || v_rep_name;
         gm_sav_lncnswap_log (p_loaner_cn_id, 51106, g_rep_id, p_userid);
      ELSE
         gm_sav_lncnswap_log (p_loaner_cn_id, 51106, '91960', p_userid);
      --N/A
      END IF;
   END gm_op_sav_loaner_to_consign;

/******************************************************************
* Description : This Procedure is called to reverse the posting for the loaner
****************************************************************/
   PROCEDURE gm_sav_reverse_posting (
      p_loaner_cn_id   IN   t504_consignment.c504_consignment_id%TYPE,
      p_ln_status_fl   IN   t504a_consignment_loaner.c504a_status_fl%TYPE,
      p_consg_setid    IN   t504_consignment.c207_set_id%TYPE,
      p_userid         IN   t101_user.c101_user_id%TYPE
   )
   AS
      CURSOR curr_ln_set_details
      IS
         SELECT c205_part_number_id pnum, c205_qty qty
           FROM my_temp_part_list;

      CURSOR cur_setlist
      IS
         SELECT setlist.pnum pnum, setlist.qty setqty,
                DECODE (SIGN (NVL (loanerlist.qty, 0)),
                        -1, 0,
                        NVL (loanerlist.qty, 0)
                       ) loanerqty,
                DECODE
                   (NVL (loanerlist.qty, 0) - setlist.qty,
                    0, setlist.qty,
                    DECODE (SIGN (NVL (loanerlist.qty, 0) - setlist.qty),
                            -1, DECODE (SIGN (NVL (loanerlist.qty, 0)),
                                        -1, 0,
                                        NVL (loanerlist.qty, 0)
                                       ),
                            setlist.qty
                           )
                   ) newsetlistqty
           FROM (SELECT   c205_part_number_id pnum, c208_set_qty qty
                     FROM t208_set_details
                    WHERE c208_inset_fl = 'Y'
                      AND c207_set_id = p_consg_setid
                      AND t208_set_details.c208_set_qty > 0
                      AND c208_void_fl IS NULL
                 ORDER BY c205_part_number_id) setlist,
                (SELECT mtemplist.c205_part_number_id pnum,
                        mtemplist.c205_qty qty
                   FROM my_temp_part_list mtemplist) loanerlist
          WHERE setlist.pnum = loanerlist.pnum(+);

      v_posting_type   NUMBER;
      v_set_type t207_set_master.c207_type%TYPE;
   BEGIN
      DELETE FROM my_temp_part_list;

      INSERT INTO my_temp_part_list
         SELECT   t205.c205_part_number_id pnum,
                  cons.qty - NVL (miss.qty, 0) qty
             FROM t205_part_number t205,
                  (SELECT   t505.c205_part_number_id,
                            SUM (t505.c505_item_qty) qty
                       FROM t505_item_consignment t505
                      WHERE t505.c504_consignment_id = p_loaner_cn_id
                        AND t505.c505_void_fl IS NULL
                   GROUP BY t505.c205_part_number_id) cons,
                  (SELECT   t413.c205_part_number_id,
                            SUM (DECODE (t413.c413_status_fl,
                                         'Q', t413.c413_item_qty,
                                         0
                                        )
                                ) qty
                       FROM t412_inhouse_transactions t412,
                            t413_inhouse_trans_items t413
                      WHERE t412.c412_inhouse_trans_id =
                                                    t413.c412_inhouse_trans_id
                        AND t412.c412_ref_id = p_loaner_cn_id
                        AND t412.c412_void_fl IS NULL
                        AND t413.c413_void_fl IS NULL
                   GROUP BY t413.c205_part_number_id) miss
            WHERE t205.c205_part_number_id = cons.c205_part_number_id
              AND t205.c205_part_number_id = miss.c205_part_number_id(+)
         ORDER BY t205.c205_part_number_id;

   		BEGIN
			SELECT c207_type INTO v_set_type  
			FROM t207_set_master 
			WHERE c207_set_id = p_consg_setid 
			AND c207_void_fl IS NULL;
		EXCEPTION WHEN NO_DATA_FOUND THEN
			v_set_type := NULL;
		END;
		
	  SELECT NVL(get_rule_value (v_set_type, 'LNDMO-FG'),4846) INTO v_posting_type FROM DUAL;                                  --Loaner to FG

      IF p_ln_status_fl = '20'
      THEN                                                --(pending   RETURN)
         FOR currindex IN curr_ln_set_details
         LOOP
            gm_save_ledger_posting (v_posting_type,
                                    CURRENT_DATE,
                                    currindex.pnum,
                                    NULL,
                                    p_loaner_cn_id,
                                    currindex.qty,
                                    NULL,
                                    p_userid
                                   );
         END LOOP;
      ELSIF p_ln_status_fl = '0' OR p_ln_status_fl = '5'
      THEN                                          --(allocated or available)
         FOR currindex IN cur_setlist
         LOOP
            IF currindex.newsetlistqty > 0
            THEN
               gm_save_ledger_posting (v_posting_type,
                                       CURRENT_DATE,
                                       currindex.pnum,
                                       NULL,
                                       p_loaner_cn_id,
                                       currindex.newsetlistqty,
                                       NULL,
                                       p_userid
                                      );
            END IF;
         END LOOP;
      END IF;
   END gm_sav_reverse_posting;

/******************************************************************
* Description : This Procedure is called to do the posting for the parts in the CN thats passed in the in param
****************************************************************/
   PROCEDURE gm_sav_posting (
      p_cn_id          IN   t504_consignment.c504_consignment_id%TYPE,
      p_posting_type   IN   NUMBER,
      p_userid         IN   t101_user.c101_user_id%TYPE
   )
   AS
      CURSOR curr_posting_details
      IS
         SELECT c205_part_number_id pnum, c505_item_qty qty
           FROM t505_item_consignment
          WHERE c504_consignment_id = p_cn_id AND c505_void_fl IS NULL;
   BEGIN
      FOR currindex IN curr_posting_details
      LOOP
         gm_save_ledger_posting (p_posting_type,
                                 CURRENT_DATE,
                                 currindex.pnum,
                                 NULL,
                                 p_cn_id,
                                 currindex.qty,
                                 NULL,
                                 p_userid
                                );
      END LOOP;
   END gm_sav_posting;

      /******************************************************************
   * Description : This Procedure creates the main CN
   ****************************************************************/
   PROCEDURE gm_sav_main_cn (
      p_loaner_cn_id            IN       t504_consignment.c504_consignment_id%TYPE,
      p_ln_status_fl            IN       t101_user.c101_user_id%TYPE,
      p_consg_setid             IN       t504_consignment.c207_set_id%TYPE,
      p_userid                  IN       t101_user.c101_user_id%TYPE,
      p_out_master_request_id   OUT      t520_request.c520_request_id%TYPE,
      p_out_msg                 OUT      VARCHAR2
   )
   AS
      CURSOR cur_setlist
      IS
         SELECT setlist.pnum, setlist.qty setqty,
                DECODE (SIGN (NVL (loanerlist.qty, 0)),
                        -1, 0,
                        NVL (loanerlist.qty, 0)
                       ) loanerqty,
                DECODE
                     (NVL (loanerlist.qty, 0) - setlist.qty,
                      0, setlist.qty,
                      DECODE (SIGN (NVL (loanerlist.qty, 0) - setlist.qty),
                              -1, DECODE (SIGN (NVL (loanerlist.qty, 0)),
                                          -1, 0,
                                          NVL (loanerlist.qty, 0)
                                         ),
                              setlist.qty
                             )
                     ) newsetlist
           FROM (SELECT   c205_part_number_id pnum, c208_set_qty qty
                     FROM t208_set_details
                    WHERE c208_inset_fl = 'Y'
                      AND c207_set_id = p_consg_setid
                      AND t208_set_details.c208_set_qty > 0
                      AND c208_void_fl IS NULL
                 ORDER BY c205_part_number_id) setlist,
                (SELECT mtemplist.c205_part_number_id pnum,
                        mtemplist.c205_qty qty
                   FROM my_temp_part_list mtemplist) loanerlist
          WHERE setlist.pnum = loanerlist.pnum(+);
        
       CURSOR v_tag_cur
       IS
	   	 SELECT c5010_tag_id tagid
	       FROM t5010_tag
	      WHERE c5010_last_updated_trans_id = p_loaner_cn_id
	        AND c5010_void_fl IS NULL;

      v_boqty                     NUMBER;
      v_consign_first_time_flag   NUMBER                                  := 0;
      v_out_new_cn_id             t504_consignment.c504_consignment_id%TYPE;
      v_out_bo_request_id         t520_request.c520_request_id%TYPE;
      v_out_req_detail_id         t520_request.c520_request_id%TYPE;
      v_tag_id                    t5010_tag.c5010_tag_id%TYPE;
      v_request_to                t520_request.c520_request_to%TYPE;
      v_void_fl                   t520_request.c520_void_fl%TYPE;
      v_plant_id                  t5040_plant_master.C5040_PLANT_ID %TYPE;
	  v_company_id                t1900_company.C1900_COMPANY_ID%TYPE;
	  v_set_type t207_set_master.c207_type%TYPE;
	  v_posting_type 			  NUMBER;	  
	  v_out_shipid                VARCHAR2 (100);
   BEGIN
	   
	   SELECT get_plantid_frm_cntx()  INTO  v_plant_id   FROM DUAL;            
	   SELECT get_compid_frm_cntx() INTO  v_company_id FROM DUAL;
	   
	   BEGIN
			SELECT c207_type INTO v_set_type  
			FROM t207_set_master 
			WHERE c207_set_id = p_consg_setid 
			AND c207_void_fl IS NULL;
		EXCEPTION WHEN NO_DATA_FOUND THEN
			v_set_type := NULL;
		END;
		
	   	-- Validate the validate Tag details for p_loaner_cn_id 
	   gm_pkg_op_ln_cn_swap.gm_validate_tag(p_loaner_cn_id);
      -- Create CN
      gm_sav_rqcn (p_loaner_cn_id,
                   p_ln_status_fl,
                   p_userid,
                   p_consg_setid,
                   v_out_new_cn_id,
                   p_out_master_request_id
                  );

      IF p_ln_status_fl = 20
      THEN
         v_request_to := g_dist_id;
      END IF;

      -- Load parts with control# into t505 for the new CN
      FOR currindex IN cur_setlist
      LOOP
         IF currindex.newsetlist > 0
         THEN
            gm_sav_item_consign_details (p_loaner_cn_id,
                                         v_out_new_cn_id,
                                         currindex.pnum,
                                         currindex.newsetlist
                                        );
         END IF;

         IF currindex.setqty > currindex.loanerqty
         THEN                                        -- enter qty in backorder
            v_boqty := currindex.setqty - currindex.loanerqty;

            IF v_consign_first_time_flag = 0
            THEN
               gm_pkg_op_request_master.gm_sav_request
                                                    (NULL,
                                                     TRUNC (CURRENT_DATE),
                                                     50618,
                                                     NULL,
                                                     p_consg_setid,
                                                     40021,     -- consignment
                                                     v_request_to,
                                                     NULL,
                                                     p_userid,
                                                     NULL,
                                                     NULL,
                                                     p_out_master_request_id,
                                                     10,
                                                     p_userid,
                                                     50060,
                                                     v_out_bo_request_id
                                                    );
               v_consign_first_time_flag := v_consign_first_time_flag + 1;
            END IF;

            gm_pkg_op_request_master.gm_sav_request_detail
                                                         (v_out_bo_request_id,
                                                          currindex.pnum,
                                                          v_boqty,
                                                          v_out_req_detail_id
                                                         );
                                                         
           --- PC-3828 Shipping Error Items from Shipped Sets Release 
           gm_pkg_cm_shipping_trans.gm_sav_ship_info (v_out_bo_request_id,
							                          50184,          --Request
							                          g_ship_to,
							                          g_shipto_id,
							                          5040,
							                          5031,
							                          NULL,
							                          g_address_id,
							                          NULL,
							                          p_userid,
							                          v_out_shipid
							                       );  
         END IF;

         UPDATE my_temp_part_list
            SET c205_qty = c205_qty - currindex.newsetlist
          WHERE c205_part_number_id = currindex.pnum;
      END LOOP;

         /*   IF v_consign_first_time_flag > 0 AND p_ln_status_fl = '20'
            THEN
      -- move the extra parts in the consignment set from backorder to item consignment if parts are available in stock
               gm_sav_xtra_parts_consignment (p_loaner_cn_id,
                                              p_ln_status_fl,
                                              v_out_bo_request_id,
                                              p_userid,
                                              p_out_msg
                                             );
               p_out_msg := p_out_msg;
            END IF;

            IF v_consign_first_time_flag > 0
            THEN
               SELECT c520_void_fl
                 INTO v_void_fl
                 FROM t520_request
                WHERE c520_request_id = v_out_bo_request_id;

               IF v_void_fl != 'Y'
               THEN
                  gm_sav_lncnswap_log (p_loaner_cn_id,
                                       51103,
                                       v_out_bo_request_id,
                                       p_userid
                                      );
                  p_out_msg :=
                        p_out_msg
                     || ' <br> Created a Back Order Request for Extra parts in Consignment Set '
                     || v_out_bo_request_id;
               END IF;
            END IF;
            */

      -- do the posting for the above created new CN
      IF p_ln_status_fl = '20'
      THEN
      	 SELECT NVL(get_rule_value (v_set_type, 'LNCN_SWAP_FG_SALES'),4822) INTO v_posting_type FROM DUAL;
         gm_sav_posting (v_out_new_cn_id, v_posting_type, p_userid);     -- FG to sales
         -- update the FS warehouse
         -- Qty:4301 Plus,102900 - Consignment,102907 - Consignment
			IF g_dist_id IS NOT NULL
			THEN
                gm_pkg_op_inv_field_sales_tran.gm_update_fs_inv(v_out_new_cn_id, 'T505', g_dist_id, 102900, 4301, 102907, p_userid);
            END IF; -- end if g_dist_id
      ELSIF p_ln_status_fl = '0' OR p_ln_status_fl = '5'
      THEN
      	 SELECT NVL(get_rule_value (v_set_type, 'FGDMO'),4812) INTO v_posting_type FROM DUAL;
         gm_sav_posting (v_out_new_cn_id, v_posting_type, p_userid);        -- FG to BS
      END IF;

      -- Copy the tag id
      --  For now updating the main CN only
      FOR v_tag IN v_tag_cur
	  LOOP
	  	 v_tag_id := v_tag.tagid;
	  	 
	      IF p_ln_status_fl = '20'
	      THEN
	         UPDATE t5010_tag
	            SET c5010_last_updated_trans_id = v_out_new_cn_id,
	                c207_set_id = p_consg_setid,
	                c901_location_type = 4120,                             -- dist
	                c5010_location_id = g_dist_id,
	                c901_trans_type='51000',  --51000 Consignment
	                c5010_last_updated_by = p_userid,
	                c5010_last_updated_date = CURRENT_DATE,
	                c1900_company_id = v_company_id,
                    c5040_plant_id    = v_plant_id
	          WHERE c5010_tag_id = v_tag_id AND c5010_void_fl IS NULL;
	      ELSE                                     -- move the tag to set building
	         UPDATE t5010_tag
	            SET c5010_last_updated_trans_id = v_out_new_cn_id,
	            	c207_set_id = p_consg_setid,
	                c901_location_type = 40033,
	                c5010_location_id = 52098,
	                c901_trans_type='51000',  --51000 Consignment
	                c5010_last_updated_by = p_userid,
	                c5010_last_updated_date = CURRENT_DATE,
	                c1900_company_id = v_company_id,
                    c5040_plant_id    = v_plant_id
	          WHERE c5010_tag_id = v_tag_id AND c5010_void_fl IS NULL;
	      END IF;
		END LOOP;
		
	   
      -- Update the log table with records for status/newCn/Backorder
      gm_sav_lncnswap_log (p_loaner_cn_id, 51107, p_ln_status_fl, p_userid);
      gm_sav_lncnswap_log (p_loaner_cn_id, 51101, v_out_new_cn_id, p_userid);
      p_out_msg := p_out_msg || ' <br> Created a new CN ' || v_out_new_cn_id;

      IF v_consign_first_time_flag = 1
      THEN
         gm_sav_lncnswap_log (p_loaner_cn_id,
                              51103,
                              v_out_bo_request_id,
                              p_userid
                             );
         p_out_msg :=
               p_out_msg
            || ' <br> Created a Back Order Request for Extra parts in Consignment Set '
            || v_out_bo_request_id;
      ELSE
         gm_sav_lncnswap_log (p_loaner_cn_id, 51103, '91960', p_userid);
      --N/A
      END IF;
   END gm_sav_main_cn;

    /******************************************************************
   * Description : this procedure calls existing procedures to create the RQ,CN and Shipping record
   ****************************************************************/
   PROCEDURE gm_sav_rqcn (
      p_loaner_cn_id     IN       t504_consignment.c504_consignment_id%TYPE,
      p_ln_status_fl     IN       t101_user.c101_user_id%TYPE,
      p_userid           IN       t101_user.c101_user_id%TYPE,
      p_consg_setid      IN       t504_consignment.c207_set_id%TYPE,
      p_out_cn_id        OUT      t504_consignment.c504_consignment_id%TYPE,
      p_out_request_id   OUT      t520_request.c520_request_id%TYPE
   )
   AS
      v_out_shipid   VARCHAR2 (100);
   BEGIN
      -- Create a request
      gm_pkg_op_request_master.gm_sav_request (NULL,
                                               TRUNC (CURRENT_DATE),
                                               50618,                     --cs
                                               NULL,
                                               p_consg_setid,
                                               40021,         --   consignment
                                               NULL,
                                               NULL,
                                               p_userid,
                                               NULL,
                                               NULL,
                                               NULL,
                                               20,
                                               p_userid,
                                               50060,                --regular
                                               p_out_request_id
                                              );
      --  Create a consignment
      gm_pkg_op_request_master.gm_sav_consignment
                                    (NULL,
                                     NULL,
                                     NULL,
                                        'CN Created while converting loaner '
                                     || p_loaner_cn_id
                                     || ' to consignment',
                                     NULL,
                                     2,
                                     NULL,
                                     NULL,
                                     NULL,
                                     p_consg_setid,
                                     NULL,
                                     NULL,
                                     50060,
                                     4110,
                                     NULL,
                                     NULL,
                                     NULL,
                                     1,                      --by default is 1
                                     1,
                                     TRUNC (CURRENT_DATE),
                                     p_userid,
                                     1,
                                     NULL,
                                     NULL,
                                     p_out_request_id,
                                     p_userid,
                                     p_out_cn_id
                                    );

      IF p_ln_status_fl = 20                  ---AND p_consg_setid IS NOT NULL
      THEN
         --updating values in request/consignment table
         UPDATE t520_request
            SET c520_request_to = g_dist_id,
                c520_status_fl = 40,
                c520_last_updated_by = p_userid,
                c520_last_updated_date = CURRENT_DATE
          WHERE c520_request_id = p_out_request_id;

         UPDATE t504_consignment
            SET c701_distributor_id = g_dist_id,
                c504_ship_date = TRUNC (CURRENT_DATE),
                --MNTTASK-2796 Duplicates in Part Num Transaction Search (CS Report) , No need to update Account id while doing Loaner to Consignment Swap. 
                --c704_account_id = g_acc_id,
                c504_ship_to = g_ship_to,
                c504_ship_to_id = g_shipto_id,
                c504_delivery_carrier = 5040,
                c504_delivery_mode = 5031,
                c504_tracking_number = 'loanertoconsignswp',
                c504_status_fl = 4,
                c504_last_updated_date = CURRENT_DATE,
                c504_last_updated_by = p_userid
          WHERE c504_consignment_id = p_out_cn_id;

         --  Enter shipping record
         gm_pkg_cm_shipping_trans.gm_sav_ship_info (p_out_cn_id,
                                                    50181,           --consign
                                                    g_ship_to,
                                                    g_shipto_id,
                                                    5040,
                                                    5031,
                                                    'LOANERTOCONSIGNSWP',
                                                    g_address_id,
                                                    NULL,
                                                    p_userid,
                                                    v_out_shipid
                                                   );

         UPDATE t907_shipping_info
            SET c907_status_fl = 40,
                c907_shipped_dt = TRUNC (CURRENT_DATE),
                c907_email_update_fl = 'Y',
                c907_email_update_dt = TRUNC (CURRENT_DATE),
                c907_last_updated_date = CURRENT_DATE,
                c907_last_updated_by = p_userid
          WHERE c907_shipping_id = v_out_shipid;

         gm_pkg_cm_shipping_trans.gm_sav_shipping_status_log (p_out_cn_id,
                                                              50181,
                                                              p_userid,
                                                              'Shipped'
                                                             );
      END IF;
   END gm_sav_rqcn;

   /******************************************************************
   * Description : Procedure called when there are parts in the loaner set and not in the consignment set list
   if loaner status  - pending return then will create item consignment/ra for these parts
   if loaner status - available/pending return then will create set to shelf record for these parts
   ****************************************************************/
   PROCEDURE gm_sav_xtra_parts_loaner (
      p_loaner_cn_id   IN       t504_consignment.c504_consignment_id%TYPE,
      p_ln_status_fl   IN       t101_user.c101_user_id%TYPE,
      p_consg_setid    IN       t504_consignment.c207_set_id%TYPE,
      p_userid         IN       t101_user.c101_user_id%TYPE,
      p_out_msg        OUT      VARCHAR2
   )
   AS
      CURSOR lnparts
      IS
         SELECT c205_part_number_id pnum, c205_qty qty
           FROM my_temp_part_list
          WHERE c205_qty > 0;

      v_txn_id           VARCHAR2 (30);
      v_out_cn_id        t504_consignment.c504_consignment_id%TYPE;
      v_out_request_id   t520_request.c520_request_id%TYPE;
      v_price            t205_part_number.c205_consignment_price%TYPE;
      v_pnum             t205_part_number.c205_part_number_id%TYPE;
      v_qty              my_temp_part_list.c205_qty%TYPE;
      v_company_id   t1900_company.c1900_company_id%TYPE;
      v_plant_id     t5040_plant_master.c5040_plant_id%TYPE;
      v_set_type t207_set_master.c207_type%TYPE;
	  v_posting_type 			  NUMBER;
   BEGIN
      SELECT get_compid_frm_cntx(), get_plantid_frm_cntx()
      INTO v_company_id, v_plant_id
      FROM DUAL; 
      
      BEGIN
			SELECT c207_type INTO v_set_type  
			FROM t207_set_master 
			WHERE c207_set_id = p_consg_setid 
			AND c207_void_fl IS NULL;
	  EXCEPTION WHEN NO_DATA_FOUND THEN
			v_set_type := NULL;
	  END;
      
      OPEN lnparts;

      FETCH lnparts
       INTO v_pnum, v_qty;

      IF lnparts%NOTFOUND
      THEN
         gm_sav_lncnswap_log (p_loaner_cn_id, 51102, '91960', p_userid);
         --N/A
         gm_sav_lncnswap_log (p_loaner_cn_id, 51104, '91960', p_userid);
         --N/A
         gm_sav_lncnswap_log (p_loaner_cn_id, 51105, '91960', p_userid);
         --N/A
         RETURN;
      END IF;

      CLOSE lnparts;

      IF p_ln_status_fl = 20
      THEN                                   -- Create RA and item consignment
         SELECT 'GM-RA-' || s506_return.NEXTVAL
           INTO v_txn_id
           FROM DUAL;

         INSERT INTO t506_returns
                     (c506_rma_id, c701_distributor_id, c506_type,
                      c506_reason, c506_expected_date, c506_status_fl,
                      c704_account_id, c101_user_id, c207_set_id,
                      c506_created_by, c506_created_date, c703_sales_rep_id,
                      c506_comments, c1900_company_id, c5040_plant_id
                     )
              VALUES (v_txn_id, g_dist_id, 3302,
                      3313,                               --consingment return
                           TRUNC (CURRENT_DATE), '0',
                      g_acc_id, NULL, p_consg_setid,
                      p_userid, CURRENT_DATE, g_rep_id,
                         'RA Created when converting loaner '
                      || p_loaner_cn_id
                      || ' to Consignment'
                      , v_company_id, v_plant_id
                     );

         p_out_msg := 'Initiated an RA  ' || v_txn_id;
         -- Create CN for item consignment
         gm_sav_rqcn (p_loaner_cn_id,
                      p_ln_status_fl,
                      p_userid,
                      NULL,
                      v_out_cn_id,
                      v_out_request_id
                     );
         p_out_msg :=
                  p_out_msg || ' <br> Created a new Item CN   ' || v_out_cn_id;

         --   Loop Cursor to create records in t505 and t507_return_item
         FOR currindex IN lnparts
         LOOP
            gm_sav_item_consign_details (p_loaner_cn_id,
                                         v_out_cn_id,
                                         currindex.pnum,
                                         currindex.qty
                                        );
            --Fetch part price
            v_price := get_part_price (currindex.pnum, 'C');

            INSERT INTO t507_returns_item
                        (c507_returns_item_id, c506_rma_id,
                         c205_part_number_id, c507_item_qty, c507_item_price,
                         c507_status_fl
                        )
                 VALUES (s507_return_item.NEXTVAL, v_txn_id,
                         currindex.pnum, currindex.qty, v_price,
                         'Q'
                        );

            INSERT INTO t507_returns_item
                        (c507_returns_item_id, c506_rma_id,
                         c205_part_number_id, c507_item_qty, c507_item_price,
                         c507_status_fl
                        )
                 VALUES (s507_return_item.NEXTVAL, v_txn_id,
                         currindex.pnum, currindex.qty, v_price,
                         'R'
                        );
         END LOOP;

         --call posting for the item CN created - FG to sales consignment
         SELECT NVL(get_rule_value (v_set_type, 'LNCN_SWAP_FG_SALES'),4822) INTO v_posting_type FROM DUAL;
         gm_sav_posting (v_out_cn_id, v_posting_type, p_userid);
         gm_sav_lncnswap_log (p_loaner_cn_id, 51102, v_out_cn_id, p_userid);
         gm_sav_lncnswap_log (p_loaner_cn_id, 51104, v_txn_id, p_userid);
         gm_sav_lncnswap_log (p_loaner_cn_id, 51105, '91960', p_userid); --N/A
         -- update the FS warehouse
         -- Qty:4301 Plus,102900 - Consignment,102907 - Consignment
		 IF g_dist_id IS NOT NULL
		 THEN
             gm_pkg_op_inv_field_sales_tran.gm_update_fs_inv(v_out_cn_id, 'T505', g_dist_id, 102900, 4301, 102907, p_userid);
         END IF; -- end if g_dist_id
      -- if its allocated or available
      ELSE
         SELECT get_next_consign_id ('50157')
           INTO v_txn_id
           FROM DUAL;
           
       
         INSERT INTO t412_inhouse_transactions
                     (c412_inhouse_trans_id, c412_type, c412_inhouse_purpose,
                      c412_ref_id, c412_release_to, c412_release_to_id,
                      c412_comments,
                      c412_status_fl, c412_created_by, c412_created_date, c1900_company_id, 
					  c5040_plant_id
                     )
              VALUES (v_txn_id, 50157, 50130,
                      p_loaner_cn_id, g_ship_to, g_shipto_id,
                         'LN Created when converting loaner '
                      || p_loaner_cn_id
                      || ' to Consignment',
                      2, p_userid, CURRENT_DATE, v_company_id, 
					  v_plant_id
                     );

--LOOP Cursor to create records in t413
         FOR currindex IN lnparts
         LOOP
            --Fetch part price
            v_price := get_part_price (currindex.pnum, 'E');
            gm_pkg_op_inhouse_master.gm_sav_inhouse_trans_items
                                                             (v_txn_id,
                                                              currindex.pnum,
                                                              currindex.qty,
                                                              'NOC#',
                                                              v_price
                                                             );
         END LOOP;

         p_out_msg := ' <br> Created a set to shelf transaction ' || v_txn_id;
         gm_sav_lncnswap_log (p_loaner_cn_id, 51105, v_txn_id, p_userid);
         gm_sav_lncnswap_log (p_loaner_cn_id, 51102, '91960', p_userid); --N/A
         gm_sav_lncnswap_log (p_loaner_cn_id, 51104, '91960', p_userid); --N/A
      END IF;
   END gm_sav_xtra_parts_loaner;

    /******************************************************************
   * Description : This procedure is called to save item consignment details.
   It enters the control# and qty for the consignment.
   ****************************************************************/
   PROCEDURE gm_sav_item_consign_details (
      p_loaner_cn_id     IN   t504_consignment.c504_consignment_id%TYPE,
      p_new_consign_id   IN   t504_consignment.c504_consignment_id%TYPE,
      p_pnum             IN   t205_part_number.c205_part_number_id%TYPE,
      p_qty              IN   t505_item_consignment.c505_item_qty%TYPE
   )
   AS
      CURSOR part_details
      IS
         SELECT c205_part_number_id pnum, c505_control_number cno,
                c505_item_qty qty, c505_ref_id refid
           FROM t505_item_consignment
          WHERE c504_consignment_id = p_loaner_cn_id
            AND c205_part_number_id = p_pnum
            AND c505_item_qty != 0;

      v_out_item_consigment_id   t505_item_consignment.c505_item_consignment_id%TYPE;
      v_tot_qty                  NUMBER;
      v_price                    t205_part_number.c205_consignment_price%TYPE;
   BEGIN
      SELECT SUM (c505_item_qty) qty
        INTO v_tot_qty
        FROM t505_item_consignment
       WHERE c504_consignment_id = p_loaner_cn_id
         AND c205_part_number_id = p_pnum;

      --Fetch part price
      v_price := get_part_price (p_pnum, 'C');

      FOR currindex IN part_details
      LOOP
         --insert record with NOC# and p_qty if
         -- part is replenished
         -- total qty available in the t505 table for the part is not same as the passed qty
         IF currindex.refid IS NOT NULL OR v_tot_qty != p_qty
         THEN
            --    dbms_output.put_line('part replenished ' ||p_pnum);
            gm_pkg_op_request_master.gm_sav_consignment_detail
                                                    (NULL,
                                                     p_new_consign_id,
                                                     p_pnum,
                                                     'NOC#',
                                                     p_qty,
                                                     v_price,
                                                     NULL,
                                                     NULL,
                                                     NULL,
                                                     v_out_item_consigment_id
                                                    );
            RETURN;
         END IF;
      END LOOP;

      -- Else enter records with cn# from the t505 table
                                   -- means no rows were inserted above
      FOR currindex IN part_details
      LOOP
         --       dbms_output.put_line('entering the control number' ||p_pnum);
         gm_pkg_op_request_master.gm_sav_consignment_detail
                                                    (NULL,
                                                     p_new_consign_id,
                                                     currindex.pnum,
                                                     currindex.cno,
                                                     currindex.qty,
                                                     v_price,
                                                     NULL,
                                                     NULL,
                                                     NULL,
                                                     v_out_item_consigment_id
                                                    );
      END LOOP;
      
      ----When ever the child is updated, the consignment table has to be updated ,so ETL can Sync it to GOP.
	  UPDATE t504_consignment
	     SET c504_last_updated_date = CURRENT_DATE
	   WHERE c504_consignment_id = p_new_consign_id;
	   
   END gm_sav_item_consign_details;

    /******************************************************************
   * Description : Procedure to log the details when loaner to consignment swap is done
   ****************************************************************/
   PROCEDURE gm_sav_lncnswap_log (
      p_loaner_consign_id   IN   t504a_consignment_loaner.c504_consignment_id%TYPE,
      p_type                IN   t504b_loaner_swap_log.c901_type%TYPE,
      p_ref_id              IN   t504b_loaner_swap_log.c504d_ref_id%TYPE,
      p_userid              IN   t101_user.c101_user_id%TYPE
   )
   AS
   BEGIN
      INSERT INTO t504b_loaner_swap_log
                  (c504d_loaner_swap_log_id, c504_consignment_id,
                   c901_type, c504d_ref_id, c504d_created_date,
                   c504d_created_by
                  )
           VALUES (s504b_loaner_swap_log.NEXTVAL, p_loaner_consign_id,
                   p_type, p_ref_id, CURRENT_DATE,
                   p_userid
                  );
   END gm_sav_lncnswap_log;

    /******************************************************************
   * Description : Procedure to send an email
   ****************************************************************/
   PROCEDURE gm_lncnswap_send_email (
      p_loaner_consign_id   IN   t504a_consignment_loaner.c504_consignment_id%TYPE
   )
   AS
      v_tomail           VARCHAR2 (500);
      v_subject          VARCHAR2 (100);
      v_body             VARCHAR2 (10000);
      v_maincn           t504a_consignment_loaner.c504_consignment_id%TYPE;
      v_itemcn           t504a_consignment_loaner.c504_consignment_id%TYPE;
      v_borq             t504a_consignment_loaner.c504_consignment_id%TYPE;
      v_raid             t504a_consignment_loaner.c504_consignment_id%TYPE;
      v_settoshelf       t504a_consignment_loaner.c504_consignment_id%TYPE;
      v_setid            t207_set_master.c207_set_id%TYPE;
      v_setname          t207_set_master.c207_set_nm%TYPE;
      v_etchid           t504a_consignment_loaner.c504a_etch_id%TYPE;
      v_converted_by     VARCHAR2 (30);
      v_converted_date   VARCHAR2 (30);
      v_tagnumber        VARCHAR2(2000) := '';
      crlf               VARCHAR2 (2)                  := CHR (13)
                                                          || CHR (10);

      CURSOR curr_log_details
      IS
         SELECT c901_type typ, decode(t504b.c504d_ref_id,'91960','',t504b.c504d_ref_id) refid,
                TO_CHAR (t504b.c504d_created_date,
                         'mm/dd/yyyy hh24:mi:ss'
                        ) crdate,
                get_user_name (t504b.c504d_created_by) crby
           FROM t504b_loaner_swap_log t504b
          WHERE c504_consignment_id = p_loaner_consign_id
            AND c504d_void_fl IS NULL;
   BEGIN
      BEGIN
         SELECT c504a_etch_id, t504.c207_set_id setid,
                get_set_name (t504.c207_set_id) setname,
                get_cs_ship_email (4121, g_rep_id) repemail
           INTO v_etchid, v_setid,
                v_setname,
                v_tomail
           FROM t504a_consignment_loaner t504a, t504_consignment t504
          WHERE t504.c504_consignment_id = t504a.c504_consignment_id
            AND t504a.c504a_status_fl = 60                          --inactive
            AND c504a_void_fl IS NULL
            AND t504a.c504_consignment_id = p_loaner_consign_id;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            GM_RAISE_APPLICATION_ERROR('-20999','234','');
      END;

      IF v_etchid IS NULL
      THEN
         GM_RAISE_APPLICATION_ERROR('-20999','235','');
      END IF;

-- fetch the maincn id , itemCN and RA details.
      FOR currindex IN curr_log_details
      LOOP
         IF currindex.typ = 51101
         THEN
            v_converted_by := currindex.crby;
            v_converted_date := currindex.crdate;
            v_maincn := currindex.refid;
         END IF;

         IF currindex.typ = 51102
         THEN
            v_itemcn := currindex.refid;
         END IF;

                 /* IF currindex.typ = 5110
                  THEN
                     v_borq := currindex.refid;
                  END IF;
         */
         IF currindex.typ = 51104
         THEN
            v_raid := currindex.refid;
         END IF;
      END LOOP;

-- fetch the tag id
      BEGIN
        SELECT DISTINCT RTRIM (XMLAGG (XMLELEMENT (e, c5010_tag_id || ',')) .EXTRACT ('//text()'), ',')
		   INTO v_tagnumber
		   FROM t5010_tag
		  WHERE c5010_last_updated_trans_id = v_maincn 
		    AND c5010_void_fl              IS NULL;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            v_tagnumber := NULL;
      END;

      v_tomail := v_tomail || ',vprasath@globusmedical.com';
      v_subject :=
            'Loaner '
         || p_loaner_consign_id
         || ' was converted to a consignment '
         || v_maincn
         || ' successfully.';
      v_body :=
            'The following set that has been loaned to you is now consigned to you as of today''s date. Please make note of this for you records.'
         || crlf
         || crlf
         || 'Set Name: '
         || v_setid
         || ' - '
         || v_setname
         || crlf
         || crlf
         || 'Loaner Number: '
         || v_etchid
         || ' Tag Number(s): '
         || v_tagnumber
         || crlf
         || crlf
         || 'New CN#: '
         || v_maincn
         || crlf
         || crlf
         || 'Converted By: '
         || v_converted_by
         || crlf
         || crlf
         || 'Converted Date: '
         || v_converted_date
         || crlf;

      IF v_itemcn IS NOT NULL
      THEN
         v_body :=
               v_body
            || crlf
            || crlf
            || 'Created an Item Consignment and Initiated an RA to return the excess parts in the Loaner'
            || crlf
            || crlf
            || 'RA Id: '
            || v_raid
            || ' Refer Email "Initiate RA - '
            || v_raid
            || '" for part details.'
            || crlf
            || crlf
            || 'Item CN#: '
            || v_itemcn;
      END IF;

      gm_com_send_email_prc (v_tomail, v_subject, v_body);
   END gm_lncnswap_send_email;

   /******************************************************************
   * Description : function to fetch RA id aft the loaner to consignment swap is done
   ****************************************************************/
   FUNCTION get_ra_id (
      p_loaner_consign_id   IN   t504a_consignment_loaner.c504_consignment_id%TYPE
   )
      RETURN VARCHAR2
   IS
      v_ra_id   t504a_consignment_loaner.c504_consignment_id%TYPE;
   BEGIN
      BEGIN
         SELECT c504d_ref_id
           INTO v_ra_id
           FROM t504b_loaner_swap_log
          WHERE c504_consignment_id = p_loaner_consign_id
                AND c901_type = 51104;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            RETURN '';
      END;

      RETURN v_ra_id;
   END get_ra_id;

   /******************************************************************
   * Description : Procedure called when there are excess parts in the consignment set and not in the loaner set list
   if parts are not available in shelf then will create a bo request
   ****************************************************************/
   PROCEDURE gm_sav_xtra_parts_consignment (
      p_loaner_cn_id        IN       t504_consignment.c504_consignment_id%TYPE,
      p_ln_status_fl        IN       t101_user.c101_user_id%TYPE,
      p_out_bo_request_id   IN       t520_request.c520_request_id%TYPE,
      p_userid              IN       t101_user.c101_user_id%TYPE,
      p_out_msg             OUT      VARCHAR2
   )
   AS
      CURSOR cnparts
      IS
         SELECT c205_part_number_id pnum, c521_qty qty,
                get_qty_in_stock (c205_part_number_id) stkqty
           FROM t521_request_detail
          WHERE c520_request_id = p_out_bo_request_id;

      v_partnum                   t205_part_number.c205_part_number_id%TYPE;
      v_setqty                    t208_set_details.c208_set_qty%TYPE;
      v_stkqty                    t208_set_details.c208_set_qty%TYPE;
      v_tot_boqty                 t208_set_details.c208_set_qty%TYPE;
      v_backorderqty              t208_set_details.c208_set_qty%TYPE;
      v_out_cn_id                 t504_consignment.c504_consignment_id%TYPE;
      v_out_request_id            t520_request.c520_request_id%TYPE;
      v_out_item_consign_id       t504_consignment.c504_consignment_id%TYPE;
      v_out_shipid                VARCHAR2 (100);
      v_consign_first_time_flag   BOOLEAN                            := FALSE;
   BEGIN
      --  open cnparts;
      RETURN;
   --for now commenting below code. need not be called now
   /*FOR currindex IN cnparts
   LOOP
      v_partnum := currindex.pnum;
      v_stkqty := currindex.stkqty;
      v_setqty := currindex.qty;

      IF v_stkqty < 0
      THEN
         v_stkqty := 0;
      END IF;

      IF v_stkqty > 0
      THEN
         IF (NOT v_consign_first_time_flag)
         THEN                                          -- create rq and cn
            gm_sav_rqcn (p_loaner_cn_id,
                         p_ln_status_fl,
                         p_userid,
                         NULL,
                         v_out_cn_id,
                         v_out_request_id
                        );

            UPDATE t504_consignment
               SET c504_verify_fl = NULL,
                   c504_verified_date = NULL,
                   c504_verified_by = NULL,
                   c504_last_updated_date = CURRENT_DATE,
                   c504_last_updated_by = p_userid
             WHERE c504_consignment_id = v_out_cn_id;

            IF p_ln_status_fl = 20
            THEN           -- assign the dist id and create shipping record
               UPDATE t520_request
                  SET c520_request_to = g_dist_id,
                      c520_last_updated_by = p_userid,
                      c520_last_updated_date = CURRENT_DATE
                WHERE c520_request_id = v_out_request_id;

               UPDATE t504_consignment
                  SET c701_distributor_id = g_dist_id,
                      c504_last_updated_date = CURRENT_DATE,
                      c504_last_updated_by = p_userid
                WHERE c504_consignment_id = v_out_cn_id;

               --  Enter shipping record
               gm_pkg_cm_shipping_trans.gm_sav_ship_info (v_out_cn_id,
                                                          50181,  --consign
                                                          g_ship_to,
                                                          g_shipto_id,
                                                          5040,
                                                          5031,
                                                          NULL,
                                                          g_address_id,
                                                          NULL,
                                                          p_userid,
                                                          v_out_shipid
                                                         );
            END IF;

            v_consign_first_time_flag := TRUE;
            gm_sav_lncnswap_log (p_loaner_cn_id,
                                 51108,
                                 v_out_cn_id,
                                 p_userid
                                );
            p_out_msg :=
                  ' <br> Created an Item transaction for Extra parts in Consignment Set '
               || v_out_cn_id;
         END IF;

         IF v_stkqty >= v_setqty
         THEN                                     -- then enter in t505 tbl
            gm_pkg_op_request_master.gm_sav_consignment_detail
                                              (NULL,
                                               v_out_cn_id,
                                               v_partnum,
                                               'TBE',
                                               v_setqty,
                                               get_part_price (v_partnum,
                                                               'C'
                                                              ),
                                               NULL,
                                               NULL,
                                               NULL,
                                               v_out_item_consign_id
                                              );

            DELETE FROM t521_request_detail
                  WHERE c520_request_id = p_out_bo_request_id
                    AND c205_part_number_id = v_partnum
                    AND c521_qty = v_setqty;
         END IF;

         IF v_stkqty < v_setqty AND v_stkqty <> 0
         THEN
            v_backorderqty := v_setqty - v_stkqty;
            gm_pkg_op_request_master.gm_sav_consignment_detail
                                              (NULL,
                                               v_out_cn_id,
                                               v_partnum,
                                               'TBE',
                                               v_stkqty,
                                               get_part_price (v_partnum,
                                                               'C'
                                                              ),
                                               NULL,
                                               NULL,
                                               NULL,
                                               v_out_item_consign_id
                                              );

            UPDATE t521_request_detail
               SET c521_qty = v_backorderqty
             WHERE c520_request_id = p_out_bo_request_id
               AND c205_part_number_id = v_partnum;
         END IF;

         SELECT COUNT (1)
           INTO v_tot_boqty
           FROM t521_request_detail
          WHERE c520_request_id = p_out_bo_request_id;

         IF v_tot_boqty = 0
         THEN
            UPDATE t520_request
               SET c520_void_fl = 'Y'
             WHERE c520_request_id = p_out_bo_request_id;
         END IF;
      END IF;
   END LOOP;
   */
   END gm_sav_xtra_parts_consignment;

   /******************************************************************
     * created velu Aug 10 2010
     * Description : Procedure to get the all values loaner consignsetid
     ****************************************************************/
   PROCEDURE gm_fch_lnswap_log_details (
      p_loaner_cn_id   IN       t504b_loaner_swap_log.c504_consignment_id%TYPE,
      p_request        OUT      TYPES.cursor_type
   )
   AS
   BEGIN
      OPEN p_request
       FOR
          SELECT c901_type, get_code_name_alt (c901_type) labelnm,
                 DECODE
                    (c504d_ref_id,
                     '91960', get_code_name (c504d_ref_id),
                     DECODE (c901_type,
                             51106, get_rep_name (c504d_ref_id),
                             51107, gm_pkg_op_loaner.get_loaner_status
                                                                 (c504d_ref_id),
                             c504d_ref_id
                            )
                    ) labelvalue
            FROM t504b_loaner_swap_log
           WHERE c504_consignment_id = p_loaner_cn_id
             AND c504d_void_fl IS NULL;
   END gm_fch_lnswap_log_details;
    /*
    * Description:	Procedure to validate the TAG for the given transaction
    * Author:		Rajkumar
    */
   PROCEDURE gm_validate_tag(
   		p_txn_id	IN	t5010_tag.c5010_last_updated_trans_id%TYPE,
   		p_set_id	IN	t208_set_details.c207_set_id%TYPE DEFAULT NULL
   )
   AS
   	v_set_id	t208_set_details.c207_set_id%TYPE;
   	v_tagpart_cnt	NUMBER := 0;
   	v_tag_cnt		NUMBER := 0;
   BEGIN
-- select DISTINCT SUBSTR(TRIM(c5010_last_updated_trans_id),1,5) from t5010_tag t5010

		-- GM-CN, GM-PN, QNFG, PNFG, GM-QN, QNSC, GM-IN, BLQN
		SELECT NVL(SUM(c505_item_qty),-1) INTO  v_tagpart_cnt
		FROM t504_consignment t504, t505_item_consignment t505, t205d_part_attribute t205d
		where t504.c504_consignment_id = t505.c504_consignment_id
		AND t505.c205_part_number_id  = t205d.c205_part_number_id
		AND t504.c504_consignment_id = p_txn_id
		AND t205d.c901_attribute_type = 92340 -- Taggable part
		AND c205d_attribute_value = 'Y'
		AND t205d.c205d_void_fl IS NULL
		AND t504.c504_void_fl is null
		and t505.c505_void_fl is null;
	
		 SELECT COUNT (1) INTO v_tag_cnt
	       FROM t5010_tag
	      WHERE c5010_last_updated_trans_id = p_txn_id
	        AND c5010_void_fl IS NULL;

	 IF v_tagpart_cnt != -1  AND  v_tag_cnt > v_tagpart_cnt
	 THEN
	 	GM_RAISE_APPLICATION_ERROR('-20999','236',p_txn_id);
	 END IF;
	END gm_validate_tag;
	
END gm_pkg_op_ln_cn_swap;
/
