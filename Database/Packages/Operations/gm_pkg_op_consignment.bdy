/* Formatted on 2010/12/06 12:50 (Formatter Plus v4.8.0) */
-- @"c:\database\packages\operations\gm_pkg_op_consignment.bdy"

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_consignment 
IS
/******************************************************************
   * Description : Function to return the consignment type whether set or item
   ****************************************************************/
   FUNCTION get_consign_type (
      p_consignid   IN   t504_consignment.c504_consignment_id%TYPE
   )
      RETURN VARCHAR2
   IS
      v_name   VARCHAR2 (150);
   BEGIN
      BEGIN
         SELECT DECODE (c207_set_id, NULL, 'ITEM', 'SET')
           INTO v_name
           FROM t504_consignment
          WHERE c504_consignment_id = p_consignid AND c504_void_fl IS NULL;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            RETURN '';
      END;

      RETURN v_name;
   END get_consign_type;

    /******************************************************************
   * Description : Function to return the set id given a consignment id
   ****************************************************************/
   FUNCTION get_set_id (
      p_consignid   IN   t504_consignment.c504_consignment_id%TYPE
   )
      RETURN VARCHAR2
   IS
      v_name   VARCHAR2 (255);
   BEGIN
      BEGIN
         SELECT NVL (c207_set_id, NULL) setid
           INTO v_name
           FROM t504_consignment
          WHERE c504_consignment_id = p_consignid AND c504_void_fl IS NULL;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            RETURN '';
      END;

      RETURN v_name;
   END get_set_id;

/******************************************************************
   * Description : Procedure to update the hold flg in the consignment table
****************************************************************/
   PROCEDURE gm_sav_hold_fl (
      p_consignid   IN   t504_consignment.c504_consignment_id%TYPE,
      p_userid      IN   t504_consignment.c504_created_by%TYPE
   )
   IS
   BEGIN
      UPDATE t504_consignment
         SET c504_hold_fl = 'Y',
             c504_last_updated_date = SYSDATE,
             c504_last_updated_by = p_userid
       WHERE c504_consignment_id = p_consignid AND c504_void_fl IS NULL;
   END gm_sav_hold_fl;
	
	/******************************************************************
	* Description : Procedure is used to update the notes on paperwork
	****************************************************************/
	PROCEDURE gm_sav_consign_notes(
		p_consignid		IN		t504_consignment.c504_consignment_id%TYPE,
		p_notes			IN		t504_consignment.c504_comments%TYPE,
		p_userid		IN		t504_consignment.C504_LAST_UPDATED_BY%TYPE
	)
	AS
	BEGIN
		UPDATE T504_consignment
				SET 
					C504_COMMENTS=p_notes, 
					C504_LAST_UPDATED_BY=p_userid, 
					C504_LAST_UPDATED_DATE=SYSDATE 
				WHERE 
					c504_consignment_id = p_consignid AND c504_void_fl IS NULL;
	END gm_sav_consign_notes;
	/******************************************************************
	 * Author		:	Rajkumar Jayakumar 
	 * 
	 * Description	: Fetch the hold flag status by passing the consignment id
	****************************************************************/
	FUNCTION get_hold_flg(
		p_consignid   IN   t504_consignment.c504_consignment_id%TYPE
	)
		RETURN VARCHAR2
	IS
		v_hold_fl T504_consignment.c504_hold_fl%TYPE;
	BEGIN
		SELECT NVL(C504_hold_fl,NULL) INTO v_hold_fl
		FROM T504_consignment WHERE c504_consignment_id=p_consignid AND C504_void_fl IS NULL;
		return v_hold_fl;
	END get_hold_flg;

	/******************************************************************
	* Author  : Rajeshwaran
	*
	* Description : THIS PROCEDURE WILL UPDATE THE CONSIGNMENT ATTRIBUTE,
	* IF THE RECORD DOES NOT EXISTS, IT WILL CREATE A NEW CONSIGNMENT ATTRIBUTE
	****************************************************************/
	PROCEDURE gm_sav_consign_attribute (
	        P_CONS_ATRB_ID IN T504D_CONSIGNMENT_ATTRIBUTE.C504D_CONSIGNMENT_ATTRIBUTE_ID%TYPE,
	        P_CONS_ID      IN T504D_CONSIGNMENT_ATTRIBUTE.C504_CONSIGNMENT_ID%TYPE,
	        P_ATRB_TYPE    IN T504D_CONSIGNMENT_ATTRIBUTE.C901_ATTRIBUTE_TYPE%TYPE,
	        P_ATRB_VALUE   IN T504D_CONSIGNMENT_ATTRIBUTE.C504D_ATTRIBUTE_VALUE%TYPE,
	        p_userid       IN T504D_CONSIGNMENT_ATTRIBUTE.C504D_CREATED_BY%TYPE)
	AS
	BEGIN
	     UPDATE T504D_CONSIGNMENT_ATTRIBUTE	     
	    SET C901_ATTRIBUTE_TYPE                = P_ATRB_TYPE, 
	    C504D_ATTRIBUTE_VALUE = P_ATRB_VALUE, 
	    C504D_LAST_UPDATED_BY = p_userid, 
	    C504D_LAST_UPDATED_DATE = SYSDATE
	      WHERE C504_CONSIGNMENT_ID            = P_CONS_ID
	        AND C901_ATTRIBUTE_TYPE            = P_ATRB_TYPE
	        AND C504D_VOID_FL                 IS NULL;
	        
	    IF (SQL%ROWCOUNT = 0) THEN
	         INSERT
	           INTO T504D_CONSIGNMENT_ATTRIBUTE
	            (
	                C504D_CONSIGNMENT_ATTRIBUTE_ID, C504_CONSIGNMENT_ID, C901_ATTRIBUTE_TYPE
	              , C504D_ATTRIBUTE_VALUE, C504D_CREATED_BY, C504D_CREATED_DATE
	            )
	            VALUES
	            (
	                S504D_CONSIGNMENT_ATTRIBUTE_ID.NEXTVAL, P_CONS_ID, P_ATRB_TYPE
	              , P_ATRB_VALUE, p_userid, SYSDATE
	            );
	    END IF;
	END gm_sav_consign_attribute;

/***************************************************************************
* Description : Procedure used to fetch the consignment details information
****************************************************************************/
PROCEDURE gm_fch_ous_replenish_cn (
        p_out_cur OUT TYPES.cursor_type )
AS
BEGIN
    OPEN p_out_cur 
    FOR
    SELECT  t520.c520_request_id REQID, t504.c504_consignment_id CID
   	  FROM  t520_request t520, t504_consignment t504
 	 WHERE  t520.c520_request_id  = t504.c520_request_id
       AND  t520.c901_purpose     = 4000097                       --OUS Sales Replenishment type
       AND  TRUNC (t520.c520_request_date) = TRUNC (SYSDATE - 1)
       AND  C520_VOID_FL  IS NULL
       AND  C504_VOID_FL  IS NULL;
END gm_fch_ous_replenish_cn;

	/****************************************************************
	* Description : this procedure is used to save the Consignment Status (Pending Pick/FG)
	* Author      :
	*****************************************************************/
PROCEDURE gm_sav_consignment_status (
        p_txnid     IN t504_consignment.c504_consignment_id%TYPE,
        p_operation IN t504_consignment.c504_status_fl%TYPE,
        p_user_id   IN t504_consignment.c504_last_updated_by%TYPE)
AS
    v_shipto_id		VARCHAR2 (40) ;
    v_request_to	t520_request.c520_request_to%TYPE;
    v_req_id		t520_request.c520_request_id%TYPE;
    v_req_status	t520_request.c520_status_fl%TYPE;
    v_plant_id      t5040_plant_master.c5040_plant_id%TYPE;
BEGIN
	 BEGIN
 			 SELECT  NVL(get_plantid_frm_cntx(),get_rule_value('DEFAULT_PLANT','ONEPORTAL')) INTO  v_plant_id FROM DUAL;

             SELECT t520.c520_request_to, t520.c520_request_id, t520.c520_status_fl
               INTO v_request_to, v_req_id, v_req_status
               FROM t504_consignment t504, t520_request t520
              WHERE t520.c520_request_id     = t504.c520_request_id
                AND t504.c504_consignment_id = p_txnid
                AND t520.c520_void_fl       IS NULL
                AND t504.c504_void_fl       IS NULL
                AND t504.c5040_plant_id      = v_plant_id;
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_request_to := NULL;
            v_req_id  := '';
     END; 
    IF (p_operation = 'PICK') THEN
        BEGIN
             SELECT C907_SHIP_TO_ID
               INTO v_shipto_id
               FROM t907_shipping_info
              WHERE c907_ref_id   = p_txnid
                AND c907_void_fl IS NULL AND c5040_plant_id      = v_plant_id;
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_shipto_id := NULL;
        END;
        IF v_req_status != '15' THEN	 
            -- to update the CN status as Pending Shipping
            gm_pkg_op_consignment.gm_upd_consignment_status (p_txnid, '3', p_user_id) ;
             -- to update the request status as Ready to ship
            gm_pkg_op_consignment.gm_upd_request_status (v_req_id, '30', p_user_id) ;
            -- to call the shipping package
            gm_pkg_cm_shipping_trans.gm_sav_release_shipping (p_txnid, 50181, p_user_id, NULL) ; --50181 - Consignment
        ELSE
            -- to update the CN status as WIP ('15' is backlog, it means it's from builtset rollback, need update to WIP)
            gm_pkg_op_consignment.gm_upd_consignment_status (p_txnid, '1', p_user_id) ;
             -- to update the request status as Back Log
            gm_pkg_op_consignment.gm_upd_request_status (v_req_id, '15', p_user_id) ;
        END IF;
    ELSIF (p_operation = 'PUT') THEN
        IF v_request_to IS NOT NULL THEN
            -- to update the CN status as Pending Shipping
            gm_pkg_op_consignment.gm_upd_consignment_status (p_txnid, '3', p_user_id) ;
            -- to update the request status as Ready to ship
            gm_pkg_op_consignment.gm_upd_request_status (v_req_id, '30', p_user_id) ;
            -- to call the shipping package
            gm_pkg_cm_shipping_trans.gm_sav_release_shipping (p_txnid, 50181, p_user_id, NULL) ; --50181 - Consignment
        ELSE
            -- to update the CN status as Build Set
            gm_pkg_op_consignment.gm_upd_consignment_status (p_txnid, '2', p_user_id) ;
        END IF;
    END IF;
END gm_sav_consignment_status;
	/****************************************************************
	* Description : this procedure is used to update the Consignment Status
	* Author      :
	*****************************************************************/
PROCEDURE gm_upd_consignment_status (
        p_txnid   IN t504_consignment.c504_consignment_id%TYPE,
        p_status  IN t504_consignment.c504_status_fl%TYPE,
        p_user_id IN t504_consignment.c504_last_updated_by%TYPE)
AS
BEGIN
    -- update the CN status
     UPDATE t504_consignment
    SET c504_status_fl          = p_status, c504_last_updated_by = p_user_id, c504_last_updated_date = SYSDATE
      WHERE c504_consignment_id = p_txnid
        AND c504_void_fl       IS NULL;
END gm_upd_consignment_status;

	/****************************************************************
	* Description : this procedure is used to update the Request Status
	* Author      :
	*****************************************************************/
PROCEDURE gm_upd_request_status (
        p_reqid   IN t520_request.c520_request_id%TYPE,
        p_status  IN t520_request.c520_status_fl%TYPE,
        p_user_id IN t504_consignment.c504_last_updated_by%TYPE)
AS
BEGIN
    -- update the request status
     UPDATE t520_request t520
    SET t520.c520_status_fl          = p_status, c520_last_updated_by = p_user_id, c520_last_updated_date = SYSDATE
      WHERE t520.c520_request_id = p_reqid 
        AND t520.c520_void_fl       IS NULL;
END gm_upd_request_status;
	/*********************************************************************
	* Author :
	*
	* Description : Procedure to save consignment attribute details batch
	**********************************************************************/
PROCEDURE gm_sav_consign_attribute_batch (
        p_consignment_id IN t504_consignment.c504_consignment_id%TYPE,
        p_userid         IN t504_consignment.c504_last_updated_by%TYPE,
        p_str            IN VARCHAR2)
AS
    v_strlen    NUMBER           := NVL (LENGTH (p_str), 0) ;
    v_string    VARCHAR2 (30000) := p_str;
    v_substring VARCHAR2 (1000) ;
    v_attrtype t501a_order_attribute.c901_attribute_type%TYPE;
    v_attrval t501a_order_attribute.c501a_attribute_value%TYPE;
    v_ordatbid t501a_order_attribute.c501a_order_attribute_id%TYPE;
BEGIN
    WHILE INSTR (v_string, '|') <> 0
    LOOP
        v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
        v_string    := SUBSTR (v_string, INSTR (v_string, '|')    + 1) ;
        v_ordatbid  := NULL;
        v_attrtype  := NULL;
        v_attrval   := NULL;
        v_ordatbid  := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
        v_substring := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        v_attrtype  := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
        v_substring := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        v_attrval   := v_substring;
        --
        gm_sav_consignment_attribute (p_consignment_id, v_attrtype, v_attrval, v_ordatbid, p_userid) ;
        --
    END LOOP;
END gm_sav_consign_attribute_batch;
	/*********************************************************************
	* Author : 
	* Description : Procedure to save consignment attribute details
	**********************************************************************/
PROCEDURE gm_sav_consignment_attribute (
        p_consignid IN t504_consignment.c504_consignment_id%TYPE,
        p_attrtype  IN t501a_order_attribute.c901_attribute_type%TYPE,
        p_attrval   IN t501a_order_attribute.c501a_attribute_value%TYPE,
        p_caid      IN t501a_order_attribute.c501a_order_attribute_id%TYPE,
        p_userid    IN t501a_order_attribute.c501a_created_by%TYPE)
AS
    v_con_attr_id t504a_cons_attribute.c504a_cons_attribute_id%TYPE;
BEGIN
     UPDATE t504a_cons_attribute
    SET c504_consignment_id         = p_consignid, c901_attribute_type = p_attrtype, c504a_attribute_value = p_attrval
      , c504a_last_updated_by       = p_userid, c504a_last_updated_date = SYSDATE
      WHERE c504a_cons_attribute_id = p_caid
        AND c504a_void_fl          IS NULL;
    --    
    IF (SQL%ROWCOUNT                = 0) THEN
         SELECT s504a_CONS_attribute.NEXTVAL INTO v_con_attr_id FROM DUAL;
         INSERT
           INTO t504a_cons_attribute
            (
                c504a_cons_attribute_id, c504_consignment_id, c901_attribute_type
              , c504a_attribute_value, c504a_created_by, c504a_created_date
            )
            VALUES
            (
                v_con_attr_id, p_consignid, p_attrtype
              , p_attrval, p_userid, SYSDATE
            ) ;
    END IF;
END gm_sav_consignment_attribute;

/***************************************************************************************
 * Description			 : This procedure fetches italy Consignment header info
 * Author                : rajan
 ****************************************************************************************/
PROCEDURE gm_op_it_fch_consigndetails(
    p_inputstr	    IN	   VARCHAR2
  , p_prod_req_id   IN     t907_shipping_info.c525_product_request_id%TYPE
  , p_recordset    OUT	   TYPES.cursor_type
)
AS
v_datefmt VARCHAR2(10);

BEGIN
	
	SELECT NVL(get_compdtfmt_frm_cntx(),get_rule_value('DATEFMT','DATEFORMAT')) INTO v_datefmt FROM DUAL;

--
	IF p_inputstr IS NOT NULL
	THEN
		my_context.set_my_inlist_ctx (p_inputstr);

		OPEN p_recordset
		 FOR
			SELECT t504.c504_consignment_id consignid, get_set_name (t504.c207_set_id) setname
				  , t504.c207_set_id setid, get_code_name (t504.c504_inhouse_purpose) settype, c504_type ctype
				  , t504a.c504a_etch_id etchid, t504a.c504a_expected_return_dt edate
				  , TO_CHAR(t504a.c504a_expected_return_dt,v_datefmt) eredate
				  , t504a.c504a_loaner_dt ldate
				  , gm_pkg_op_loaner.get_loaner_status (t504a.c504a_status_fl) loansfl
				  , t504a.c504a_status_fl loansflcd
				  , DECODE (get_cs_rep_loaner_bill_nm_add (t504.c504_consignment_id)
						  , ' ', 'Globus Medical'
						  , get_cs_rep_loaner_bill_nm_add (t504.c504_consignment_id)
						   ) billnm
				  , t504.c701_distributor_id distid,GET_DISTRIBUTOR_NAME(t504.c701_distributor_id) distnm
				  , t504al.c703_sales_rep_id repid,get_rep_name(t504al.c703_sales_rep_id) repnm
				  , t504al.c704_account_id accid,get_account_name(t504al.c704_account_id) accnm
				  , get_party_name(get_dealer_id_by_acct_id(t504al.c704_account_id)) delearnm 
				  , DECODE (t504a.c504a_status_fl, 25, gm_pkg_op_loaner.get_return_dt (vlist.token), '') returndt
				  , gm_pkg_op_loaner.get_loaner_trans_id (vlist.token) loantransid
				  , get_cs_ship_email (4121, t504al.c703_sales_rep_id) emailadd
				  , t504.C504_HOLD_FL holdfl
				  , get_compid_from_distid(t504.C701_DISTRIBUTOR_ID) COMPANYID
				  , get_code_name(c504_type) txntypenm
				  , gm_pkg_op_loaner.get_loaner_request_id(t504al.c504a_loaner_transaction_id) txnparentid
				  , t504al.c703_ass_rep_id assocrepid,get_rep_name(t504al.c703_ass_rep_id) assocrepnm
				  , t504.c504_inhouse_purpose setpurpose
			   -- , gm_pkg_op_loaner.get_loaner_set_list_change (vlist.token) setlistfl
			      , t504.c1910_division_id DIVISION_ID
                  , t504.c1900_company_id COMPANYCD
                  , get_company_dtfmt(T504.c1900_company_id) CMPDFMT
                  , t504al.c1900_company_id TRANS_COMPANY_ID
                 -- PC-3784 Surgery date is not updated on loaner stock sheet working in progress
                  , NVL(t504al.c504a_surgery_date,gm_pkg_op_loaner.get_request_surgery_date(t504al. C526_PRODUCT_REQUEST_DETAIL_ID)) surgerydate
                  , tshipinfo.lnshipdate shipdate
                  , gm_pkg_op_loaner.get_loaner_requested_by(t504al. C526_PRODUCT_REQUEST_DETAIL_ID) requestedby
                  , GET_TAG_ID(t504.c504_consignment_id) tagid, t504.c207_set_id||' '||get_set_name (t504.c207_set_id) setidname
                  , GET_LATEST_LOG_COMMENTS(t504.c504_consignment_id ,'1222') logcomments
                  , tshipinfo.HOSPACCNTID HospAccountid 
                  , tshipinfo.shiptonm shiptoname
                  , tshipinfo.shipadd shipaddress
			 FROM	t504_consignment t504
				  , t504a_consignment_loaner t504a
				  , t504a_loaner_transaction t504al
				  , v_in_list vlist
				  , ( SELECT t504a.c504_consignment_id, t907.c907_ship_to_dt lnshipdate,
				       DECODE(t907.C901_SHIP_TO, 4122, t907.c907_ship_to_id, '') HOSPACCNTID, 
				       get_cs_ship_name (t907.c901_ship_to, t907.c907_ship_to_id) shiptonm,
				       gm_pkg_cm_shipping_info.get_loaner_ship_add(p_inputstr,50182,p_prod_req_id,'') shipadd,
					   t907.c907_ref_id
                         FROM t504a_consignment_loaner t504a
                         , t504a_loaner_transaction t504al
                         , t526_product_request_detail t526
                         , t907_shipping_info t907
                        WHERE t504a.c504a_status_fl                 = 20 -- PENDING RETURN
                          AND t504al.c504a_return_dt               IS NULL
                          AND t504al.c504a_void_fl                 IS NULL
                          AND t504a.c504_consignment_id             = t504al.c504_consignment_id
                          AND t504al.c526_product_request_detail_id = t526.c526_product_request_detail_id
                          AND t526.c525_product_request_id          = t907.c525_product_request_id
                          AND t504a.c504_consignment_id             = t907.c907_ref_id
			              AND t907.c907_ref_id                      = p_inputstr
			              AND t907.c525_product_request_id          = p_prod_req_id  
                          AND t504a.c504a_void_fl                  IS NULL
                          AND t907.c907_void_fl                    IS NULL
                          AND t526.C526_VOID_FL                    IS NULL) tshipinfo
                      

			  WHERE t504.c504_consignment_id = vlist.token
				AND t504.c504_consignment_id = t504a.c504_consignment_id
				AND t504al.c504_consignment_id(+) = t504.c504_consignment_id
				AND t504.c504_consignment_id = tshipinfo.c504_consignment_id
				AND tshipinfo.c907_ref_id = t504.c504_consignment_id
				AND t504al.c504a_loaner_transaction_id(+) = gm_pkg_op_loaner.get_loaner_trans_id (t504.c504_consignment_id);
				
 END IF;
--
END gm_op_it_fch_consigndetails;

/**********************************************************************
	 * Description : Procedure to clear the Location while shipout
	 * Author	   : Karthik Somanathan
**********************************************************************/	
PROCEDURE gm_clear_wip_loc(
  p_ref_id  IN  t504_consignment.C504_CONSIGNMENT_ID%TYPE
, p_user_id IN  t504_consignment.C504_LAST_UPDATED_BY%TYPE
)
AS
BEGIN
	
	--To fetch the consignment types mapped to Set Build Type location
   	my_context.set_my_cloblist (get_rule_value('SB_CN_TYPE','SB_WIP_CN_TYPE')|| ',');
   	
   	IF p_ref_id IS NOT NULL THEN
		UPDATE t504_consignment
		SET c5052_location_id    = NULL,
		  c504_last_updated_by   = p_user_id ,
		  c504_last_updated_date = CURRENT_DATE
		WHERE C207_SET_ID    IS NOT NULL
		AND C504_TYPE      IN (SELECT * FROM  v_clob_list)
		AND C504_VOID_FL   IS NULL
		AND c504_consignment_id = p_ref_id;
	END IF;
			
END gm_clear_wip_loc;

END gm_pkg_op_consignment;
/
