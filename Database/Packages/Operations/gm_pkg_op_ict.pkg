/* Formatted on 2010/01/12 17:06 (Formatter Plus v4.8.0) */
-- @ "C:\database\Packages\Operations\gm_pkg_op_ict.pkg"

CREATE OR REPLACE PACKAGE gm_pkg_op_ict
AS
/******************************************************************************
   NAME:		gm_pkg_op_ict
   PURPOSE:    This package holds the procedures for ICT project

   REVISIONS:
   Ver		  Date		  Author			 Description
   ---------  ----------  ---------------  ------------------------------------
   1.0		  6/26/2008 	 Brinal 		 1. Created this package.
******************************************************************************/

	/*******************************************************
 * Purpose: This Procedure is main procedure
 *******************************************************/
--
	PROCEDURE gm_op_sav_initiateict (
		p_ref_id	   IN		t504_consignment.c504_consignment_id%TYPE
	  , p_type		   IN		t504_consignment.c504_type%TYPE
	  , p_poid		   IN OUT	t501_order.c501_customer_po%TYPE
	  , p_freightamt   IN		t501_order.c501_ship_cost%TYPE
	  , p_userid	   IN		t504_consignment.c504_created_by%TYPE
	  , p_dist_id	   IN		t504_consignment.c701_distributor_id%TYPE
	  , p_purpose	   IN		t504_consignment.c504_inhouse_purpose%TYPE
	);

   /*******************************************************
 * Purpose: This Procedure is used to generate freight order
 *******************************************************/
--
	PROCEDURE gm_op_sav_ict_order (
		p_ref_id	   IN		t504_consignment.c504_consignment_id%TYPE
	  , p_order_type   IN		t501_order.c901_order_type%TYPE --102080 --OUS Distributor 2533 --ICS
	  , p_type		   IN		t504_consignment.c504_type%TYPE
	  , p_userid	   IN		t504_consignment.c504_created_by%TYPE
	  , p_dist_id	   IN		t504_consignment.c701_distributor_id%TYPE
	  , p_freightamt   IN		t501_order.c501_ship_cost%TYPE
	  , p_order_id	   OUT		t501_order.c501_order_id%TYPE
	  , p_poid		   OUT		t501_order.c501_customer_po%TYPE
	);

/*******************************************************
 * Purpose: This Procedure is used to generate	order for OUS diributor
 **********************************************************/
--
	PROCEDURE gm_op_sav_ous_dist_order (
		p_ref_id	   IN		t501_order.c501_ref_id%TYPE --ACK order
	  , p_order_type   IN		t501_order.c901_order_type%TYPE --102080 --OUS Distributor 2533 --ICS	  
	  , p_userid	   IN		t101_user.c101_user_id%TYPE
	  , p_freightamt   IN		t501_order.c501_ship_cost%TYPE
	  , p_poid		   IN		t501_order.c501_customer_po%TYPE
	 ,  p_input_str    IN       CLOB	
	 ,p_order_id		OUT 	t501_order.C501_ORDER_ID%TYPE 
	);
   /*******************************************************
 * Purpose: This Procedure is used to generate invoice
 *******************************************************/
--
	PROCEDURE gm_op_sav_ict_invoice (
		p_order_id	 IN 	  t501_order.c501_order_id%TYPE
	  , p_invtype	 IN 	  t503_invoice.c901_invoice_type%TYPE
	  , p_inv_id	 OUT	  t503_invoice.c503_invoice_id%TYPE
	);

   /*******************************************************
 * Purpose: This Procedure is used to generate	consignment invoice (distributor invoice)
 *******************************************************/
--
	PROCEDURE gm_op_sav_dist_csg_invoice (
		p_poid		   IN	t501_order.c501_customer_po%TYPE
	  , p_csg_id	   IN	t504_consignment.c504_consignment_id%TYPE
	  , p_userid	   IN	t504_consignment.c504_created_by%TYPE
	  , p_freightamt   IN	t501_order.c501_ship_cost%TYPE
	  , p_dist_order_id	 IN  t501_order.c501_order_id%TYPE
	  , p_dist_account_id  IN t501_order.c704_account_id%TYPE
	);

/*******************************************************
 * Purpose: This Procedure is used to generate	invoice from ous distributor order while ship out)
 * Created by : APrasath
 * Created Date : 09/13/2018
 *******************************************************/
--
	PROCEDURE gm_op_sav_ous_dist_invoice (
	    p_dist_order_id	 IN  t501_order.c501_order_id%TYPE
	  ,	p_poid		   IN	t501_order.c501_customer_po%TYPE
	  , p_userid	   IN	t504_consignment.c504_created_by%TYPE
	);
	
/*******************************************************
 * Purpose: This Procedure is used to generate return invoice (distributor invoice)
 *******************************************************/
--
	PROCEDURE gm_op_sav_dist_ret_invoice (
		p_return_id   IN	   t506_returns.c506_rma_id%TYPE
	  , p_userid	  IN	   t506_returns.c506_created_by%TYPE
	  , p_dist_order_id	 IN  t501_order.c501_order_id%TYPE
	  , p_dist_account_id  IN t501_order.c704_account_id%TYPE
	  , p_outinvid	  OUT	   t503_invoice.c503_invoice_id%TYPE
	);

	/*******************************************************
 * Purpose: This Procedure is used to generate	invoice and
 *******************************************************/
--
	PROCEDURE gm_op_sav_dist_invoice (
		p_poid		   IN		t501_order.c501_customer_po%TYPE
	  , p_distid	   IN		t701_distributor.c701_distributor_id%TYPE
	  , p_ref_id	   IN		t503a_invoice_txn.c503a_ref_id%TYPE
	  , p_created_by   IN		t504_consignment.c504_created_by%TYPE
	  , p_userid	   IN		t101_user.c101_user_id%TYPE
	  , p_totalcost    IN		t503a_invoice_txn.c503a_total_amt%TYPE
	  , p_freightamt   IN		t501_order.c501_ship_cost%TYPE
	  , p_invtype	   IN		t503_invoice.c901_invoice_type%TYPE
	  , p_reftype	   IN		t503a_invoice_txn.c901_ref_type%TYPE
	  , p_dist_order_id	 IN     t501_order.c501_order_id%TYPE
	  , p_dist_account_id IN    t501_order.c704_account_id%TYPE
	  , p_outinvid	   OUT		t503_invoice.c503_invoice_id%TYPE
	);

  /*******************************************************
 * Purpose: This Procedure is used to fetch part detail
 *******************************************************/
--
	PROCEDURE gm_op_fch_part_detail (
		p_ref_id		 IN 	  t504_consignment.c504_consignment_id%TYPE
	  , p_type			 IN 	  t504_consignment.c504_type%TYPE
	  , p_account_id	 IN 	  t501_order.c704_account_id%TYPE
	  , p_dist_type 	 IN 	  t701_distributor.c901_distributor_type%TYPE
	  , p_party_id		 IN 	  t101_party.c101_party_id%TYPE
	  , p_part_details	 OUT	  TYPES.cursor_type
	);

 /*******************************************************
 * Purpose: This Procedure is used to save part detail
 *******************************************************/
	PROCEDURE gm_op_sav_ous_dist_part_detail(
		p_order_id	   t501_order.c501_order_id%TYPE,
		p_account_id   t501_order.c704_account_id%TYPE,
		p_userid	   IN		t101_user.c101_user_id%TYPE ,
		p_input_str    IN     CLOB,
		p_rowid		  OUT 		T502_ITEM_ORDER.C502_ITEM_ORDER_ID%type
	);
	
/*******************************************************
 * Purpose: This Procedure is used to fetch ICT Summary
 *******************************************************/
--
	PROCEDURE gm_op_fch_ict_summary (
		p_ref_id	IN		 t504_consignment.c504_consignment_id%TYPE
	  , p_summary	OUT 	 TYPES.cursor_type
	);

		/*******************************************************
	* Purpose: This Procedure is used to save ICT returns
	*******************************************************/
	PROCEDURE gm_po_sav_ict_returns (
		p_raid		  IN   t506_returns.c506_rma_id%TYPE
	  , p_distid	  IN   t701_distributor.c701_distributor_id%TYPE
	  , p_userid	  IN   t506_returns.c506_last_updated_by%TYPE
	  , p_dist_type   IN   t701_distributor.c901_distributor_type%TYPE
	);

		/*******************************************************
	* Purpose: This Procedure emails the details of the ICT returns
	*******************************************************/
	PROCEDURE gm_op_mail_ict_return (
		p_raid			   IN	t506_returns.c506_rma_id%TYPE
	  , p_ict_invoice_id   IN	t503_invoice.c503_invoice_id%TYPE
	  , p_fd_invoice_id    IN	t503_invoice.c503_invoice_id%TYPE
	  , p_order_id		   IN	t501_order.c501_order_id%TYPE
	  , p_total_cost	   IN	t501_order.c501_total_cost%TYPE
	);

		/*******************************************************
	* Purpose: Function to get the total cost of ICT
	*******************************************************/
	FUNCTION get_csg_ship_cost (
		p_ref_id   IN	t503a_invoice_txn.c503a_ref_id%TYPE
	)
		RETURN NUMBER;

	/*******************************************************
	* Purpose: Function  to get the release of shipment. For ICT Change
	
	*******************************************************/
	FUNCTION get_ship_date (
		p_ref_id   IN	t907_shipping_info.c907_ref_id%TYPE
	)
		RETURN VARCHAR2;
	/*******************************************************
	* Purpose: This Procedure used to fetch all the ICT country
	*******************************************************/
	PROCEDURE gm_fch_posting_by_country (
		p_rule_grp		IN      t906_rules.c906_rule_grp_id%TYPE,
		p_country_list	OUT 	 TYPES.cursor_type
	);	
	
	/*******************************************************
	* Purpose: This Procedure used to fetch ICS bank details
	*******************************************************/
	PROCEDURE gm_fch_ics_bank_dtls (
		p_dist_id		IN      t701_distributor.c701_distributor_id%TYPE,
		p_party_id	    IN      t101_party.c101_party_id%TYPE, 
		p_out_bank_dtls	OUT 	 TYPES.cursor_type
	);	
	/*******************************************************
	* Purpose: To Release OUS dist Order
	*******************************************************/	
	PROCEDURE gm_release_ous_dist_order (
	p_orderid	  IN	   t501_order.c501_order_id%TYPE 
	, p_userid	  IN   t501_order.c501_last_updated_by%TYPE
	);
 /*********************************************************
 * Purpose: This Procedure is used to validate Parts ICT pricing
 **********************************************************/
  PROCEDURE gm_op_validate_dist_price (
  	  p_refid	   IN	   t907_shipping_info.c907_ref_id%TYPE
  	 ,p_dist_id	   IN	   t701_distributor.c701_distributor_id%TYPE
  	 ,p_partnum	   OUT	   varchar2
  	 ,p_dist_name  OUT     t701_distributor.c701_distributor_name%TYPE 
   );
 /*********************************************************
 * Purpose: This Procedure is used to validate Parts ICT pricing and send email
 **********************************************************/
    PROCEDURE gm_order_price_check_email (
     p_orderid	  	   IN	   t501_order.c501_order_id%TYPE
  	 ,p_dist_id	   IN		t701_distributor.c701_distributor_id%TYPE
    );
END gm_pkg_op_ict;
/