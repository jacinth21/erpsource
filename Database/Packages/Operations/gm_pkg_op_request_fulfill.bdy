/* Formatted on 06/11/2018 10:18 (Formatter Plus v4.8.0) */
/* @ "C:\database\Packages\Operations\gm_pkg_op_request_fulfill.bdy" */
CREATE OR REPLACE
PACKAGE body gm_pkg_op_request_fulfill
IS
    --
    /*******************************************************
    * Author  : ppandiyan
    * Description: Used to save the fulfill company of the requested Comapny
    *******************************************************/
    --
PROCEDURE gm_sav_request_fulfill_company (
        p_request_id           IN   T5201_REQUESTED_COMPANY.C520_REQUEST_ID%TYPE ,
        p_fulfill_comp_id      IN   T5201_REQUESTED_COMPANY.c1900_company_id%TYPE ,
        p_required_date        IN   T5201_REQUESTED_COMPANY.C5201_REQUESTED_DATE%TYPE ,
        p_user_id              IN   T101_USER.C101_USER_ID%TYPE
        
)
AS
    v_request_company_id 	T5201_REQUESTED_COMPANY.C5201_REQUESTED_COMPANY_ID%TYPE;
 	v_company_id t1900_company.c1900_company_id%TYPE; 

BEGIN	
SELECT GET_COMPID_FRM_CNTX() 
   INTO v_company_id
 FROM DUAL;

  UPDATE T5201_REQUESTED_COMPANY
         SET C1900_COMPANY_ID = P_fulfill_comp_id,
             C5201_REQUESTED_DATE = p_required_date,
             C5201_LAST_UPDATED_BY = p_user_id,
             C5201_LAST_UPDATED_DATE = CURRENT_DATE
       WHERE C520_REQUEST_ID = p_request_id;

      IF (SQL%ROWCOUNT = 0)
      THEN
         SELECT s5201_REQUESTED_COMPANY_ID.NEXTVAL
           INTO v_request_company_id
           FROM DUAL;

         INSERT INTO T5201_REQUESTED_COMPANY
                     (C5201_REQUESTED_COMPANY_ID, C520_REQUEST_ID,
                      C1900_COMPANY_ID, C5201_REQUESTED_DATE,
                      C5201_VOID_FL, C5201_LAST_UPDATED_BY,
                      C5201_LAST_UPDATED_DATE
                     )
              VALUES (v_request_company_id, p_request_id,
                      p_fulfill_comp_id, p_required_date,
                      null,p_user_id,
                      CURRENT_DATE
                     );
    --to save the corresponding attribute value
	gm_pkg_sav_snapshot_info.gm_sav_snapshot_info('REQUEST',p_request_id,'SNAPSHOT_INFO',v_company_id,'REQUEST_TXN',p_user_id);
      END IF;


 END gm_sav_request_fulfill_company;
 
/****************************************************************
* Author  : rdinesh
* Description: create the transaction for stock transfer request
****************************************************************/
PROCEDURE gm_sav_reconfig_RQST_Consign (
        p_request_id       IN t520_request.c520_request_id%TYPE,
        p_part_to_reconfig IN VARCHAR2,
        p_user             IN t520_request.c520_created_by%TYPE)
AS
    v_newmr t520_request.c520_request_id%TYPE         := NULL;
    v_newcn t504_consignment.c504_consignment_id%TYPE := NULL;
    v_out_inhtxn_id t412_inhouse_transactions.c412_inhouse_trans_id%TYPE := NULL;
    v_strlen NUMBER           := NVL (LENGTH (p_part_to_reconfig), 0) ;
    v_string VARCHAR2 (30000) := p_part_to_reconfig;
    v_pnum t205_part_number.c205_part_number_id%TYPE;
    v_qty         NUMBER;
    v_action      VARCHAR2 (20) ;
    v_substring   VARCHAR2 (1000) ;
    v_consigntype VARCHAR2 (1000) ; 
    v_consign_id t504_consignment.c504_consignment_id%TYPE := NULL;
    v_new_temp_consignment_id t504_consignment.c504_consignment_id%TYPE;
    v_allocate_fl VARCHAR2 (1) ;
    v_data_exists     NUMBER;

BEGIN
    
    v_consigntype := gm_pkg_op_request_summary.get_consignment_status (p_request_id) ; --'1'  ;   

    -- Loop through the parts and reconfigure them
    WHILE INSTR (v_string, '|') <> 0
    LOOP
        v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
        v_string    := SUBSTR (v_string, INSTR (v_string, '|')    + 1) ;
        v_pnum      := NULL;
        v_qty       := NULL;
        v_action    := NULL;
        v_pnum      := SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1) ;
        v_substring := SUBSTR (v_substring, INSTR (v_substring, ',')    + 1) ;
        v_qty       := SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1) ;
        v_substring := SUBSTR (v_substring, INSTR (v_substring, ',')    + 1) ;
        v_action    := v_substring;
        gm_pkg_op_request_summary.gm_sav_reconfig_request_cons (p_request_id, p_request_id 
        , v_pnum, v_qty, v_action, v_consigntype, v_consign_id, p_user, v_newcn, v_newmr, v_out_inhtxn_id) ;
    END LOOP;
  
    BEGIN
         SELECT c504_consignment_id
           INTO v_new_temp_consignment_id
           FROM t504_consignment t504
          WHERE t504.c520_request_id = p_request_id
            AND t504.c504_void_fl   IS NULL;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_new_temp_consignment_id := NULL;
    END;
    
    IF (v_new_temp_consignment_id IS NULL) THEN
         SELECT COUNT (c520_request_id)
           INTO v_data_exists
           FROM t521_request_detail
          WHERE c520_request_id = p_request_id;
          
        IF v_data_exists = 0 THEN
             UPDATE t520_request
                SET c520_void_fl        = 'Y'
                  , c520_last_updated_by = p_user
                  , c520_last_updated_date = CURRENT_DATE
              WHERE c520_request_id = p_request_id; --void it
              
        END IF;
        
    END IF;
    
    IF (v_new_temp_consignment_id IS NOT NULL) THEN
    
         UPDATE t520_request
            SET c520_status_fl = '15'
              , c520_last_updated_by = p_user
              , c520_last_updated_date = CURRENT_DATE 
          WHERE c520_request_id = p_request_id; --BLog
          
	END IF;

  -- allocate the stock transfer to Device
    v_consigntype    := gm_pkg_op_request_summary.get_consignment_status (p_request_id) ;
    IF (v_consigntype = '3') THEN

        v_allocate_fl              := gm_pkg_op_inventory.chk_txn_details (v_new_temp_consignment_id, '93002') ;
        IF NVL (v_allocate_fl, 'N') = 'Y' THEN
            gm_pkg_allocation.gm_ins_invpick_assign_detail ('93002', v_new_temp_consignment_id, p_user) ;
        END IF;
    END IF;


	-- India country (account data sync)
	gm_pkg_op_order_master.gm_sav_sync_acc_attribute(v_new_temp_consignment_id , NULL, 'CONS', p_user);

END gm_sav_reconfig_RQST_Consign;

/****************************************************************
    * Author  : rdinesh
    * Description: create the stock transfer invoice
****************************************************************/
PROCEDURE gm_initiate_ST_Invoice (
     p_fulfill_trans_id  IN T504_Consignment.C504_Consignment_Id%TYPE,
     p_user              IN T504_Consignment.c504_last_updated_by%TYPE
 )
 AS

v_invoice_seq_id   VARCHAR2 (20);
v_seq_prefix_id VARCHAR2(20);
v_company_id t1900_company.c1900_company_id%TYPE; 
v_invoice_id T504_Consignment.c504_stock_transfer_invoice_id%TYPE;

v_post_qty t810_posting_txn.c810_qty%TYPE;
v_cons_qty t505_item_consignment.c505_item_qty%TYPE;
v_cons_used_qty t505_item_consignment.c505_item_qty%TYPE;
v_cons_avaiable_qty t505_item_consignment.c505_item_qty%TYPE;
v_out_item_consign_id t505_item_consignment.c505_item_consignment_id%TYPE;

 
 
 	CURSOR CUR_T810 IS
		SELECT c205_part_number_id post_part_num,c810_qty post_qty,c810_local_company_cr_amt post_price 
		FROM t810_posting_txn 
		WHERE c810_txn_id = p_fulfill_trans_id
		AND NVL(c810_delete_fl,'N') <> 'Y'
		AND c810_local_company_cr_amt <> '0'
		ORDER BY c205_part_number_id,c810_qty,c810_local_company_cr_amt;
	
	CURSOR CUR_TEMP_T505 IS
		SELECT c205_part_number_id temp_part_num,c502_control_number temp_cnum,c502_item_qty temp_qty
		 FROM my_temp_t520_item_order
		WHERE c501_order_id = p_fulfill_trans_id
		  AND c502_void_fl IS NULL
	 ORDER BY c205_part_number_id,c502_item_qty;
	
 BEGIN
 
 SELECT GET_COMPID_FRM_CNTX() 
   INTO v_company_id
 FROM DUAL;
 
 SELECT get_rule_value_by_company(v_company_id,'ST_INVOICE_PREFIX',v_company_id) 
	INTO v_seq_prefix_id 
 FROM DUAL;

 EXECUTE IMMEDIATE
 'SELECT  s504_invoice' || '_' || v_company_id || '.NEXTVAL from DUAL'
 INTO v_invoice_seq_id;	

 IF v_invoice_seq_id IS NULL
 THEN
	GM_RAISE_APPLICATION_ERROR('-20999','182','');
 END IF;
		
 v_invoice_id :=v_seq_prefix_id||v_invoice_seq_id;
 --update the stock transfer invoice ID
 UPDATE T504_Consignment SET 
	  c504_stock_transfer_invoice_id = v_invoice_id,
	  c504_last_updated_by  = p_user,
	  c504_last_updated_date = CURRENT_DATE
 WHERE c504_consignment_id = p_fulfill_trans_id
 AND c504_void_fl IS NULL; 	
 
 -- Set IGST(26240115) to 5% 
 gm_pkg_op_consignment.gm_sav_consign_attribute(s504d_CONSIGNMENT_ATTRIBUTE_ID.NEXTVAL,p_fulfill_trans_id,'26240115','5',p_user);
 	
 -- using t520 temp for t505
 DELETE FROM my_temp_t520_item_order;
 
 INSERT
 INTO my_temp_t520_item_order
 (c502_item_order_id,
		c501_order_id,
		c205_part_number_id,
		c502_item_qty,
		c502_control_number,
		c502_item_price,
		c502_void_fl,
		c502_delete_fl,
		c502_item_seq_no,
		c901_type,
		c502_construct_fl,
		c502_ref_id,
		c502_vat_rate,
		c502_attribute_id,
		c502_list_price,
		c502_unit_price,
		c901_unit_price_adj_code,
		c502_unit_price_adj_value,
		c901_discount_type,
		c502_system_cal_unit_price,
		c502_discount_offered,
		c502_do_unit_price,
		c502_adj_code_value,
		c500_order_info_id,
		c502_ext_item_price,
		c502_tax_amt,
		c502_igst_rate,
		c502_cgst_rate,
		c502_sgst_rate,
		c502_hsn_code,
		c7200_rebate_rate,
		c502_rebate_process_fl)
 SELECT c505_item_consignment_id,
	    c504_consignment_id,
	    c205_part_number_id,
	    c505_item_qty,
	    c505_control_number,
	    c505_item_price,
	    c505_void_fl,
	    NULL, NULL,
	    c901_warehouse_type,
	    NULL,NULL,NULL,NULL,NULL,NULL,NULL,
        NULL,NULL,NULL,NULL,NULL,NULL,NULL,
        NULL,NULL,NULL,NULL,NULL,NULL,
        -- PMT-28394 (Added the column for Rebate Management)
        NULL, NULL
  FROM t505_item_consignment
  WHERE c504_consignment_id = p_fulfill_trans_id
  AND c505_void_fl           IS NULL;
  
  DELETE t505_item_consignment
  WHERE C504_CONSIGNMENT_ID = p_fulfill_trans_id;
  
  -- Update the price for consignment from posting table(Cost layer price) 
  
  FOR VAR_T810 IN CUR_T810
  LOOP
  	
  	v_post_qty := VAR_T810.post_qty;
  	
  	FOR VAR_T505 IN CUR_TEMP_T505
	LOOP
		
		IF (v_post_qty <= 0) THEN
			EXIT;
		END IF;
		
		v_cons_qty := VAR_T505.temp_qty;
		
		BEGIN
			SELECT SUM (c505_item_qty)
				INTO v_cons_used_qty
				FROM t505_item_consignment
				WHERE c504_consignment_id     = p_fulfill_trans_id
				AND c205_part_number_id = VAR_T505.temp_part_num
				AND c505_control_number = VAR_T505.temp_cnum
				GROUP BY c205_part_number_id,c505_control_number;
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
		v_cons_used_qty := 0;
      END;
      
      v_cons_avaiable_qty := v_cons_qty - v_cons_used_qty;
      
      -- compare the quantity to update the price from the posting table
      IF (v_cons_avaiable_qty > 0 ) THEN
      		
      		IF (v_post_qty = v_cons_avaiable_qty) THEN
				
				gm_pkg_op_request_master.gm_sav_consignment_detail (NULL, p_fulfill_trans_id, VAR_T810.post_part_num, VAR_T505.temp_cnum,
        		v_post_qty, VAR_T810.post_price, NULL, NULL, NULL,'90800', v_out_item_consign_id) ;	
        		
        		v_post_qty := 0;
        		v_cons_avaiable_qty := 0;
        	ELSIF (v_post_qty < v_cons_avaiable_qty) THEN
        		
        		gm_pkg_op_request_master.gm_sav_consignment_detail (NULL, p_fulfill_trans_id, VAR_T810.post_part_num, VAR_T505.temp_cnum,
        		v_post_qty, VAR_T810.post_price, NULL, NULL, NULL,'90800', v_out_item_consign_id) ;	
        		
        		v_cons_avaiable_qty := v_cons_avaiable_qty - v_post_qty;
        	    v_post_qty := 0;
            ELSIF (v_post_qty  > v_cons_avaiable_qty) THEN
            
            	gm_pkg_op_request_master.gm_sav_consignment_detail (NULL, p_fulfill_trans_id, VAR_T810.post_part_num, VAR_T505.temp_cnum,
        		v_cons_avaiable_qty, VAR_T810.post_price, NULL, NULL, NULL,'90800', v_out_item_consign_id) ;	
        		
        		v_post_qty := v_post_qty - v_cons_avaiable_qty;
        	    v_cons_avaiable_qty := 0;
      		END IF;
      END IF;
		
	END LOOP;
  
  END LOOP;
 
 END gm_initiate_ST_Invoice;
 
  /****************************************************************
    * Author  : rdinesh
    * Description: rollback stock transfer request
    ****************************************************************/
    PROCEDURE gm_rollback_stock_trans_req (
    	 p_fulfill_trans_id  IN T504_Consignment.C504_Consignment_Id%TYPE,
     	 p_user              IN T504_Consignment.c504_last_updated_by%TYPE
    )
    AS
    	 v_request_id t520_request.c520_request_id%TYPE;
    
    	CURSOR cur_st_req IS
    		SELECT NVL(req_id,cons_req_id) trans_id,cons_id fullfill_id,
				  cons_part_num pnum, NVL(qty,cons_qty) requested_qty
				FROM
				  (SELECT t520.c520_request_id req_id,
				    t521a.c205_part_number_id part_num ,
				    t521a.c521a_qty qty
				  FROM t520_request t520,
				    t521a_request_log t521a
				  WHERE t520.c520_request_id  IN (
				    select c520_request_id from t504_consignment where c504_consignment_id = p_fulfill_trans_id
				  ) 
				  AND t520.c520_request_id       = t521a.c520_request_id
				  AND t520.c520_void_fl         IS NULL
				  AND t521a.c521a_void_fl       IS NULL
				  ) t520_req,
				  (SELECT t504.c520_request_id cons_req_id,
				    t504.c504_consignment_id cons_id ,
				    t505.c205_part_number_id cons_part_num,
				    t505.c505_item_qty cons_qty
				  FROM t504_consignment t504,
				    t505_item_consignment t505
				  WHERE t505.c504_consignment_id = t504.c504_consignment_id
				  AND t505.c504_consignment_id = p_fulfill_trans_id
				  AND t504.c504_void_fl IS NULL
				  AND t505.c505_void_fl IS NULL
				  ) t504_cons
				WHERE t520_req.req_id(+) = t504_cons.cons_req_id
				AND t520_req.part_num(+) = t504_cons.cons_part_num
				order by trans_id,fullfill_id,pnum;
	
	BEGIN
		
		SELECT c520_request_id  
			INTO v_request_id
		FROM t504_consignment
		WHERE c504_consignment_id = p_fulfill_trans_id;
		
		-- inserting actual requested part and qty in t521
		FOR var_req IN cur_st_req
  		LOOP
  			gm_pkg_op_request_summary.gm_sav_add_part_to_reqdetail(var_req.trans_id,var_req.pnum,var_req.requested_qty);
  		END LOOP;
  		
  		UPDATE t521a_request_log SET
  		  c521a_void_fl = 'Y',
  		  c521a_last_updated_by = p_user,
  		  c521a_last_updated_date = CURRENT_DATE
  		 WHERE c520_request_id = v_request_id;
  		 
		-- set request status back to requested	
		UPDATE t520_request SET 
		 c520_status_fl = 0,
		 c520_last_updated_by = p_user,
		 c520_last_updated_date = CURRENT_DATE
		WHERE c520_request_id = v_request_id;
    
    END gm_rollback_stock_trans_req;
 
 
END gm_pkg_op_request_fulfill;
/