/* Formatted on 2011/04/01 14:47 (Formatter Plus v4.8.0) */
--@"c:\database\packages\operations\gm_pkg_op_set_build.bdy"

/* Description: Multiple CN's can be completed or 
 * verified using this procedure, this procedure is used to 
 * update the consignment status when it is beign completed 
 * or verified, while completing, the status of the consignment 
 * will be updated to "Pending Verification" 
 * ( Consignment Status Fl =2 and Verify Fl = 0 )
 * and while verifying the following gets updated
 * 
 * 1. Consignment Status = 2 & Verify Fl = 1 ( Ready to Consign )
 * 
 * 2. if the consignment has the details of whom it is getting 
 *    shipped to then Consignment Status will be updated to 
 *    Ready to Ship ( Consignment Status = 3 & Verify Fl = 1 ).
 *     
 * 3. if the consignment has the details of whom it is getting
 *    shipped to then a shipping record gets created / updated
 *    to pending shipping status.
 * 
 * 4. Inventory Fl gets updated saying the parts are taken out
 *    from inventory and Bulk Qty in part number table gets reduced
 * 
 * 5. Posting happens for Built Set ( 4812 ) 
 */

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_set_build
IS
   PROCEDURE gm_sav_dashboard (
      p_userid     IN   t4022_demand_par_detail.c4022_updated_by%TYPE,
      p_inputstr   IN   VARCHAR2
   )
   AS
/*******************************************************
  * Description : Procedure to save Dashboard
  * Author     : Xun
  *******************************************************/
      v_strlen         NUMBER                 := NVL (LENGTH (p_inputstr), 0);
      v_string         VARCHAR2 (30000)                         := p_inputstr;
      v_substring      VARCHAR2 (1000);
      v_consignid      t504_consignment.c504_consignment_id%TYPE;
      v_flag           NUMBER;
      v_invfl          t504_consignment.c504_update_inv_fl%TYPE;
      v_status         t504_consignment.c504_status_fl%TYPE;
      v_shipto         t504_consignment.c504_ship_to_id%TYPE;
      v_mrid           t504_consignment.c520_request_id%TYPE;
      v_msg            VARCHAR2 (1000);
      v_voidfl         VARCHAR2 (100);
      v_controlcount   VARCHAR2 (100);
      v_out_msg			   VARCHAR2 (2000);	
   BEGIN
      IF v_strlen > 0
      THEN
         WHILE INSTR (v_string, '|') <> 0
         LOOP
            --
            v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
            v_string := SUBSTR (v_string, INSTR (v_string, '|') + 1);
            --
            v_consignid := NULL;
            v_flag := NULL;
            --
            v_consignid :=
                         SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
            v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
            v_flag := v_substring;

            SELECT     c504_void_fl, c504_status_fl, c504_update_inv_fl,
                       c504_ship_to, c520_request_id
                  INTO v_voidfl, v_status, v_invfl,
                       v_shipto, v_mrid
                  FROM t504_consignment
                 WHERE c504_consignment_id = v_consignid
            FOR UPDATE;

            SELECT COUNT (1)
              INTO v_controlcount
              FROM t505_item_consignment
             WHERE c504_consignment_id = v_consignid
               AND (c505_control_number IS NULL OR c505_control_number = 'TBE'
                   );

            IF (v_controlcount > 0)
            THEN
               raise_application_error (-20090, v_consignid);
            END IF;

            IF (v_voidfl IS NOT NULL)
            THEN
               raise_application_error (-20770, v_consignid);
            END IF;

            -- value from the  screem
            IF (v_flag = 1)
            THEN
               IF (v_status > v_flag)
               THEN
                  raise_application_error (-20070, v_consignid);
               END IF;

               IF (v_status > 2)
               THEN
                  raise_application_error (-20073, v_consignid);
               END IF;
	               gm_save_set_build(v_consignid,NULL,NULL,2,0,p_userid,v_out_msg,'SETBUILD');
            ELSIF (v_flag = 2)
            THEN
               IF v_invfl IS NULL OR v_invfl = ''
               THEN
                	gm_save_set_build(v_consignid,NULL,NULL,2,1,p_userid,v_out_msg,'SETBUILD');
               ELSE
                  raise_application_error (-20071, v_consignid);
               END IF;
            END IF;
         END LOOP;
      END IF;
   END gm_sav_dashboard;

/***********************************************************
 * Purpose: Function to get the count of sets shipped
 ***********************************************************/
--
   FUNCTION get_shippedset_count (
      p_set_id   IN   t207_set_master.c207_set_id%TYPE
   )
      RETURN NUMBER
   IS
      v_count   NUMBER;
   BEGIN
      BEGIN
         SELECT COUNT (1)
           INTO v_count
           FROM t504_consignment t504
          WHERE c207_set_id = p_set_id
            AND c504_status_fl = 4                                 --completed
            AND c504_void_fl IS NULL;
      EXCEPTION
         WHEN OTHERS
         THEN
            v_count := 0;
      END;

      RETURN v_count;
   END get_shippedset_count;

/*
 *    DESCRIPTION: This procedure is used to rollback the verified sets to "WIP" status
 *              and it will revert the postings as well. This will rollback the Request
 *              and the Consign to the "WIP" Status.
 *    1. WIP status for Consign is 1
 *  2.   WIP status for Request is 15
 *
 * Author: Rajkumar J
 */
   PROCEDURE gm_rollback_verified_set (
      p_consign_id   IN   t505_item_consignment.c504_consignment_id%TYPE,
      p_user_id      IN   t504_consignment.c504_last_updated_by%TYPE
   )
   AS
      v_msg          VARCHAR2 (100);
      v_req_id       VARCHAR2 (100);
      v_verify_fl    t504_consignment.c504_verify_fl%TYPE;
      v_status_fl    t504_consignment.c504_status_fl%TYPE;
      v_req_status   t520_request.c520_status_fl%TYPE;
      v_status		 t504_consignment.c504_status_fl%TYPE;
      
      v_strlen			NUMBER := NVL (LENGTH (p_consign_id), 0);
      v_string	   		VARCHAR2 (4000) := p_consign_id;
      v_substring    	VARCHAR2 (4000);
      v_txn_id			t505_item_consignment.c504_consignment_id%TYPE;
      v_status_flag		VARCHAR2 (10);
      v_country_code 	VARCHAR2(10);
      v_verified_by     VARCHAR2(10);
      v_verified_date   VARCHAR2(10);
      v_inv_fl          VARCHAR2(10);
      v_request_fl      VARCHAR2(10);
     
   BEGIN
	   -- Below while loop added to add validation to portal to avoid timing complexity issue.
	   IF v_strlen > 0
		THEN
			WHILE INSTR (v_string, '|') <> 0
	        LOOP
				v_txn_id		:= NULL;
				v_status_flag	:= NULL;
				
				v_txn_id		:= SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
				v_string		:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
				v_substring		:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
				v_status_flag	:= v_substring;
	   
	   		END LOOP;
	  END IF;
      SELECT t504.c504_verify_fl, t504.c504_status_fl, c520_request_id
            INTO v_verify_fl, v_status_fl, v_req_id
            FROM t504_consignment t504
           WHERE t504.c504_consignment_id = v_txn_id
      FOR UPDATE;
      
      -- getting country code 
		SELECT GET_RULE_VALUE('CURRENT_DB','COUNTRYCODE')
		INTO v_country_code
		FROM DUAL;
      
     
		-- CN status : 1.20-PendingPutaway, 1-WIP, 2-Built set, 2.20-Pending Pick, 3-Pending Shipping
      SELECT DECODE(v_status_fl, '1.20', '1', '2', DECODE(v_country_code,'en', '2.20', '1')) INTO v_status FROM DUAL;
      
      SELECT c520_status_fl
            INTO v_req_status
            FROM t520_request
           WHERE c520_request_id = v_req_id AND c520_void_fl IS NULL
      FOR UPDATE;
     
      IF v_verify_fl != '1' OR (v_status_fl != '1.20' AND v_status_fl != '2' AND v_status_fl != '1.10')
      THEN
         raise_application_error ('-20405', '');
      END IF;
      
      IF v_status_flag = 1.20 AND v_status_flag <> v_status_fl
      THEN
         GM_RAISE_APPLICATION_ERROR('-20999','278','');
      END IF;
      
      IF v_status_flag = 2 AND v_status_flag <> v_status_fl
      THEN
         GM_RAISE_APPLICATION_ERROR('-20999','279',''); 
      END IF;
      
      IF v_status_fl = '1.10' THEN
     v_status := '2';
     v_verify_fl:= '1';
     v_verified_by:= p_user_id;
     v_verified_date:= SYSDATE;
     v_inv_fl := '1';
     v_request_fl := '20';
     
     ELSE
     v_verify_fl:= '0';
      v_verified_by:= '';
      v_verified_date:= '';
      v_inv_fl := '';
      v_request_fl := '15';
      END IF;
      

		--SELECT DECODE(v_status_flag, '1.20', '15', '2', '20') INTO v_rq_status FROM DUAL;      
-- Rollback the Request to WIP Status

      gm_pkg_op_request_master.gm_sav_request_status (v_req_id,
                                                      v_request_fl,
                                                      p_user_id
                                                     );

-- Rollback the Consign ID to WIP Status
      UPDATE t504_consignment
         SET c504_status_fl = v_status,
             c504_verify_fl = v_verify_fl,
             c504_verified_by = v_verified_by,
             c504_verified_date = v_verified_date,
             c504_update_inv_fl = v_inv_fl,
             c504_last_updated_by = p_user_id,
             c504_last_updated_date = SYSDATE
       WHERE c504_consignment_id = v_txn_id AND c504_void_fl IS NULL;

      -- Rollback is now Revert the Posting
      gm_update_inventory (v_txn_id, 'CROLL', 'PLUS', p_user_id, v_msg);
      
      IF (v_status_fl = '1.20' AND v_status_fl = 1.10)	-- Rollback from Pending Putaway to WIP
      THEN	-- Void Records In t5050_invpick_assign_detail for removing allocation in device.
      	gm_pkg_allocation.gm_inv_void_bytxn(v_txn_id, 93604, p_user_id);
      END IF;
      
      IF v_status_fl = '2'	AND v_country_code = 'en'-- Rollback from Built set to Pending Pick
      THEN	 
      	gm_pkg_allocation.gm_ins_invpick_assign_detail (93601, v_txn_id, p_user_id);
      	--93601 Consignment , to insert record in the t5050_INVPICK_ASSIGN_DETAIL
      END IF;
           
--
   END gm_rollback_verified_set;
   
   PROCEDURE gm_rollback_pendingpick_set(
    p_consign_id   IN   t504_consignment.c504_consignment_id%TYPE,
    p_user_id      IN   t504_consignment.c504_last_updated_by%TYPE
   )
   AS
   BEGIN
				-- Rollback the Consign ID to build set Status
      UPDATE t504_consignment
         SET c504_status_fl = '2',
             c504_last_updated_by = p_user_id,
             c504_last_updated_date = SYSDATE
       WHERE c504_consignment_id = p_consign_id AND c504_void_fl IS NULL;
       
       gm_pkg_allocation.gm_inv_void_bytxn(p_consign_id, 93604, p_user_id);
       
   END gm_rollback_pendingpick_set;
   
END gm_pkg_op_set_build;
 /
