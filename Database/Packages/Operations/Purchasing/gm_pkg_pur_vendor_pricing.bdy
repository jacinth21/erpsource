--@"c:/Database/Packages/Operations/Purchasing/gm_pkg_pur_vendor_pricing.bdy";
CREATE OR REPLACE PACKAGE BODY gm_pkg_pur_vendor_pricing
IS
    --
    /************************************************************************
    * Description : Procedure to fetch Vendor Pricing Details
    * Author  	  : Anilkumar
    ************************************************************************/
    --
    PROCEDURE gm_pur_fch_vendor_pricing( 
	    p_project_id    IN   	t202_project.c202_project_id%TYPE , 
	    p_vendor_id  	IN  	t405_vendor_pricing_details.c301_vendor_id%TYPE,
	    p_part_num      IN  	CLOB,
	    p_sub_copm_fl   IN   	t205_part_number.c205_sub_component_fl%TYPE,
	    p_lock_fl   	IN  	t405_vendor_pricing_details.c405_lock_fl%TYPE,
	    p_active_fl     IN  	t405_vendor_pricing_details.c405_active_fl%TYPE,
	    p_ven_cost_type IN      t301_vendor.c901_vendor_cost_type%TYPE, -- Added for PMT-51082 
	    p_out_cur   	OUT  	TYPES.cursor_type  
    )
    AS
    v_company_id  t1900_company.c1900_company_id%TYPE;
    BEGIN
	    
	    SELECT  get_compid_frm_cntx()
    	INTO    v_company_id
    	FROM    DUAL;
    	
	    OPEN p_out_cur FOR
	SELECT
        C405_PRICING_ID PID,
        T405.C205_PART_NUMBER_ID PNUM,
        t205.C205_PART_NUM_DESC PARTDESC ,
        T405.C301_VENDOR_ID VID,
        t301.C301_VENDOR_NAME vname,
        GET_CODE_NAME (t301.C301_CURRENCY) VCURR,
        C405_COST_PRICE CPRICE,
        C405_QTY_QUOTED QTYQ,
        C405_DELIVERY_FRAME DFRAME ,
        C405_VENDOR_QUOTE_ID QID,
        GET_CODE_NAME (C901_UOM_ID) UOM,
        C901_UOM_ID UOMID  ,
        C405_UOM_QTY UOMQTY,
        C405_LOCK_FL LFL,
        C405_ACTIVE_FL AFL    ,
        GET_VALIDATED_STATUS (T405.C205_PART_NUMBER_ID,
        T405.C301_VENDOR_ID) VALIDATE_FL,
        GET_VERIFIED_STATUS (  T405.C205_PART_NUMBER_ID) VERIFIED_FL     
    FROM
        t405_vendor_pricing_details T405,
        t205_part_number T205,
        t301_vendor t301    
    WHERE
        T405.c205_part_number_id = T205.c205_part_number_id      
        AND t301.c301_vendor_id      = T405.c301_vendor_id 
        AND T301.c1900_company_id = v_company_id
        AND t205.c202_project_id     = NVL(p_project_id, t205.c202_project_id)      
        AND T405.c301_vendor_id      = NVL(p_vendor_id, T405.c301_vendor_id)    
        AND c901_vendor_cost_type = NVL(p_ven_cost_type,c901_vendor_cost_type) -- Added for PMT-51082,to check vendor cost type
        AND T405.c405_void_fl IS NULL  
           AND REGEXP_LIKE (t205.c205_part_number_id,           CASE
           WHEN TO_CHAR (p_part_num) IS NOT NULL THEN TO_CHAR (REGEXP_REPLACE(p_part_num,'[+]','\+'))         
            ELSE REGEXP_REPLACE(t205.c205_part_number_id,'[+]','\+')
        END)      
        AND NVL(t205.c205_sub_component_fl, '-9999') =      CASE 
            WHEN NVL(p_sub_copm_fl, 'N') = 'Y'           THEN NVL(t205.c205_sub_component_fl, '-9999')     
            ELSE '-9999'       
        END  
        AND NVL(t405.c405_lock_fl,'-9999')= NVL(p_lock_fl,NVL(t405.c405_lock_fl,'-9999'))  
        AND NVL(t405.c405_active_fl,'-9999')=NVL(p_active_fl,NVL(t405.c405_active_fl,  '-9999'))  
    ORDER BY
        PNUM,
        UPPER (VNAME) ;  
    END gm_pur_fch_vendor_pricing;
    
    /*******************************************************
   * Description : Procedure to save Vendor Pricing Details
   * Author 	 : Anilkumar
   *******************************************************/
	--	
	PROCEDURE gm_pur_save_vendor_pricing (
    p_project_id 	IN t202_project.c202_project_id%TYPE,
    p_vendor_id 	IN t301_vendor.c301_vendor_id%TYPE,
    p_input_str  	IN CLOB,
    p_userid     	IN NUMBER,
    p_error_str 	OUT CLOB
    )
	AS
	  v_strlen 		NUMBER := NVL (LENGTH (p_input_str), 0);
	  v_string 		CLOB   := p_input_str;
	  v_substring VARCHAR2 (1000);
	  v_partnum   VARCHAR2 (20);
	  v_vid       VARCHAR2 (20);
	  v_vend_nm   VARCHAR2 (200);
	  v_pid       NUMBER;
	  v_cost      VARCHAR2 (20);
	  v_price_id  NUMBER;
	  v_qtyq      VARCHAR2 (4000);
	  v_dframe    VARCHAR2 (72);
	  v_qid       VARCHAR2 (20);
	  v_uom       VARCHAR2 (20);
	  v_uom_nm    VARCHAR2 (200); 
	  v_uomqty    VARCHAR2 (20);
	  v_lockfl    CHAR (1);
	  v_activefl  CHAR (1);
	  v_wocount   NUMBER;
	  v_oldcost   NUMBER;
	  v_prc_count NUMBER;
	  v_grid_rowid VARCHAR2 (200);
	  v_void_fl CHAR (1);
	  v_grid_err  CLOB := NULL;
	  v_company_id  t1900_company.c1900_company_id%TYPE;
	  --v_err_price_str CLOB;
	  --v_err_wo_str CLOB;
	BEGIN
		
	  SELECT get_compid_frm_cntx()	INTO v_company_id FROM DUAL;
		
      --Call a separate prc to validate the input fields. This will rerun Error Object in below format:
      --EG Format  : grid row id^vm file column id^vm file column id^vm file column id|....
	  --Sample data: 1429076954996^COST^UOMQTY^QQTY^|1429076954997^PNUM^UOMQTY^UOM^QQTY^COST^VENDORNM^|  
	  gm_validate_vendor_price(p_project_id, p_vendor_id, p_input_str, v_grid_err);

	  DELETE FROM MY_TEMP_KEY_VALUE;
	  --If above validated Err(v_grid_err) string is not null then call below part to update the price details. 
	  --else return the erid data(v_grid_err).
	  IF v_strlen  > 0 AND v_grid_err IS NULL  THEN
	    WHILE INSTR (v_string, '|') <> 0
	    LOOP
	      v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
	      v_string    := SUBSTR (v_string, INSTR (v_string, '|')    + 1);
	      v_partnum   := NULL;
	      v_vid       := NULL;
	      v_vend_nm   := NULL;
	      v_cost      := NULL;
	      v_qtyq      := NULL;
	      v_dframe    := NULL;
	      v_qid       := NULL;
	      v_uom       := NULL;
	      v_uom_nm    := NULL;
	      v_uomqty    := NULL;
	      v_lockfl    := NULL;
	      v_activefl  := NULL;
	      v_pid       := NULL;
	      v_price_id  := NULL;
	      v_grid_rowid:= NULL;
	      v_void_fl:= NULL;
	      --Eg Input str:  pnum^vendor_id^cost^qty_quoted^df_time^quote_id^UOM_id^UOM_qty^Lock_fl^Active_fl^Pricing_id^Grid_rowId|
	      v_partnum   := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
	      v_substring := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1);
	      v_vend_nm   := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
	      v_substring := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1);
	      v_cost      := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
	      v_substring := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1);
	      v_qtyq      := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
	      v_substring := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1);
	      v_dframe    := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
	      v_substring := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1);
	      v_qid       := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
	      v_substring := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1);
	      v_uom_nm    := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
	      v_substring := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1);
	      v_uomqty    := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
	      v_substring := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1);
	      v_lockfl    := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
	      v_substring := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1);
	      v_activefl  := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
	      v_substring := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1);
	      v_pid       := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
	      v_substring := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1);
	      v_grid_rowid := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
		  v_substring := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1);
		  v_void_fl := TO_CHAR(v_substring);
		  
		-- If row is locked we are just updating void fl and skipping rest of the things
		  IF v_void_fl ='Y' THEN
	        UPDATE t405_vendor_pricing_details
	            SET c405_void_fl       = v_void_fl,
	              c405_last_updated_by   = p_userid,
	              c405_last_updated_date = CURRENT_DATE
	            WHERE c405_pricing_id    = v_pid;
	      ELSE
			--From the input Vendor Name get the vendor id.
			BEGIN
			    SELECT c301_vendor_id INTO v_vid
			      FROM t301_vendor 
			     WHERE upper(trim(C301_VENDOR_NAME)) = upper (trim (v_vend_nm));
			EXCEPTION WHEN NO_DATA_FOUND 
			THEN
			  	  v_vid := NULL;
			END;

			--From the input UOM Name get the UOM code id.
			BEGIN	        
				 SELECT C901_code_id INTO v_uom
				   FROM t901_code_lookup
				  WHERE C901_code_grp               = 'UOM'
					AND c901_active_fl              = '1'
				    AND upper (trim (c901_code_nm)) = upper (trim (v_uom_nm)) ;
			EXCEPTION WHEN NO_DATA_FOUND 
			THEN
			  	  v_uom := NULL;
			END;
			
	        --Check for the vendor, part, cost combination.
	        SELECT COUNT (C405_PRICING_ID)
	        INTO v_prc_count
	        FROM t405_vendor_pricing_details
	        WHERE c301_vendor_id    = v_vid
	        AND c205_part_number_id = v_partnum
	        AND c405_cost_price     = TO_NUMBER(v_cost)
	        AND c405_void_fl IS NULL;
	        
	      --Based on the pricing ID, the data can be updated / Inserted.
	      IF v_pid IS NOT NULL THEN
	      
	        SELECT c405_cost_price
	        INTO v_oldcost
	        FROM t405_vendor_pricing_details
	        WHERE c405_pricing_id = v_pid
	        AND c405_void_fl IS NULL;
	        
	        IF v_lockfl = 'Y' 
	         THEN
	         UPDATE t405_vendor_pricing_details
	            SET c405_active_fl       = v_activefl,
	              c405_last_updated_by   = p_userid,
	              c405_last_updated_date = CURRENT_DATE
	            WHERE c405_pricing_id    = v_pid
 				AND c405_void_fl IS NULL;
	        ELSE
		        IF v_oldcost   <> TO_NUMBER (v_cost)
		        THEN		        
		          SELECT COUNT (c402_work_order_id)
		          INTO v_wocount
		          FROM t402_work_order
		          WHERE c405_pricing_id = v_pid
		          AND c402_void_fl     IS NULL;
		          
		          IF v_wocount          > 0
		          THEN		           
		            /*If already PO is raised & user is trying to change vendor pricing, to give consolidated error msg, 
		             *just insert the vendor, part details in my_temp_key_value with the MY_TEMP_TXN_VALUE= WO_ERR. */           
		           
		            INSERT INTO my_temp_key_value (MY_TEMP_TXN_ID,MY_TEMP_TXN_VALUE)
		            SELECT v_grid_rowid,'WO_ERR' FROM DUAL;		         
		          END IF;
		        END IF;
		        
		        IF v_oldcost = TO_NUMBER(v_cost) OR v_wocount <= 0
		        THEN
		        --condition for check duplicate price
		        -- OR condition is added to avoid cost duplication error msg even we are changing ohter columns apart from cost.
		            IF v_prc_count = 0 OR v_oldcost = TO_NUMBER(v_cost) 
			           THEN 
				          UPDATE t405_vendor_pricing_details
				          SET c301_vendor_id       = v_vid ,
				            c405_cost_price        = v_cost ,
				            c405_qty_quoted        = v_qtyq ,
				            c405_delivery_frame    = v_dframe ,
				            c405_vendor_quote_id   = v_qid ,
				            c901_uom_id            = v_uom ,
				            c405_uom_qty           = v_uomqty ,
				            c405_active_fl         = v_activefl ,
				            c405_last_updated_by   = p_userid ,
				            c405_last_updated_date = CURRENT_DATE
				          WHERE c405_pricing_id    = v_pid
				          AND c405_void_fl IS NULL;
				    ELSE
				       /* If already vendor, part, cost combination exists to give consolidated error msg,
				           just insert the vendor, part details in my_temp_key_value with the MY_TEMP_TXN_VALUE = DUP_PRC_ERR.  */
				          INSERT INTO my_temp_key_value ( MY_TEMP_TXN_ID, MY_TEMP_TXN_VALUE )
		            		SELECT v_grid_rowid,'DUP_PRC_ERR' FROM DUAL;
				    END IF;
		        END IF;
	        END IF;
	      ELSE
	        --IF pricing id is not there, add the prcie.
	        IF v_prc_count = 0 THEN
	          SELECT s405_vend_price_id.NEXTVAL INTO v_price_id FROM DUAL;
	          INSERT INTO t405_vendor_pricing_details
	                (c405_pricing_id,c301_vendor_id, c205_part_number_id,c405_cost_price,c405_active_fl,c405_created_date,c405_created_by, 
	                c405_qty_quoted,c405_delivery_frame ,c405_vendor_quote_id, c901_uom_id,c405_uom_qty,c405_lock_fl, c1900_company_id )
	          VALUES (v_price_id,v_vid,v_partnum,v_cost,v_activefl,CURRENT_DATE,p_userid,v_qtyq, v_dframe, v_qid,v_uom, v_uomqty, v_lockfl, v_company_id );
	          
	        ELSE
	        /* If already vendor, part, cost combination exists  to give consolidated error msg,
	           just insert the vendor, part details in MY_TEMP_TXN_VALUE = DUP_PRC_ERR.  */
	          INSERT INTO my_temp_key_value( MY_TEMP_TXN_ID, MY_TEMP_TXN_VALUE )
	         SELECT v_grid_rowid,'DUP_PRC_ERR' FROM DUAL;
	          
	        END IF;
	      END IF;
	     END IF;
	   END LOOP;
	    --To get the consolidated error msg,we are calling below procedure.
	    gm_pkg_pur_vendor_pricing.gm_fch_vendor_price_err(p_error_str);

	  END IF;
	  
	  IF v_grid_err IS NOT NULL 
	  THEN	  
	  	p_error_str := v_grid_err;
	  END IF;
	  	  
	  DELETE FROM MY_TEMP_KEY_VALUE;
	END gm_pur_save_vendor_pricing;
	
	/************************************************************************
    * Description : Procedure to get Error Vendor Pricing Details with consolidated error msg
    * Author  	  : Anilkumar
    ************************************************************************/
    --
    PROCEDURE gm_fch_vendor_price_err( 
	    p_invalid_str    OUT  CLOB  
    )
    AS
	    v_parts_err_str CLOB := NULL;
    BEGIN
	BEGIN
    	 SELECT RTRIM (XMLAGG (XMLELEMENT (N, errcolumn || '|') .EXTRACT ('//text()')).GetClobVal(), ', ')
	           INTO v_parts_err_str
	           FROM
	            (
	                 SELECT temp.my_temp_txn_id ||'^'|| RTRIM (XMLAGG (XMLELEMENT (N, temp.my_temp_txn_value || '^'))
	                    .EXTRACT ( '//text()'), ', ') errcolumn
	                   FROM my_temp_key_value temp
	                  WHERE my_temp_txn_id IS NOT NULL
	               GROUP BY my_temp_txn_id
	               ORDER BY temp.my_temp_txn_id
	            ) ;
    EXCEPTION
	    WHEN NO_DATA_FOUND THEN
	        v_parts_err_str := NULL;
	END;
	    p_invalid_str := v_parts_err_str;

	END gm_fch_vendor_price_err;  
 
	/*****************************************************************************************************************
	* Description : Procedure to get validate Vendor Pricing Details provided by user from UI
	*  Since here dealing with BULK data, doing all front end validations are at the back end for better performance.
	* Author     : Elango
	******************************************************************************************************************/
	PROCEDURE gm_validate_vendor_price (
			p_project_id 	IN  t202_project.c202_project_id%TYPE,
    		p_vendor_id 	IN  t301_vendor.c301_vendor_id%TYPE,
	        p_input_str 	IN  CLOB,
	        p_error_str 	OUT CLOB
	)
	AS
	    v_strlen NUMBER := NVL (LENGTH (p_input_str), 0) ;
	    v_string CLOB   := p_input_str;
	    v_substring  VARCHAR2 (1000) ;
	    v_partnum    VARCHAR2 (20) ;
	    v_vnm        VARCHAR2 (200) ;
	    v_pid        NUMBER;
	    v_costn      NUMBER;
	    v_cost       VARCHAR2 (20) ;
	    v_price_id   NUMBER;
	    v_qtyq       VARCHAR2 (4000) ; 
	    v_qtyqn       NUMBER;
	    v_dframe     VARCHAR2 (72) ;
	    v_qid        VARCHAR2 (20) ;
	    v_uom        VARCHAR2 (200) ;
	    v_uomqty     VARCHAR2 (20) ; 
	    v_uomqtyn     NUMBER;
	    v_lockfl     CHAR (1) ;
	    v_activefl   CHAR (1) ;
	    v_grid_rowid VARCHAR2 (200) ;
	    v_void_fl CHAR (1);
	    v_grid_err_str CLOB;
	    v_count NUMBER :=0;
	BEGIN
		
	     DELETE FROM my_temp_vendor_det;
	     DELETE FROM my_temp_key_value;
	     
	  
	    IF v_strlen > 0 THEN
	        WHILE INSTR (v_string, '|') <> 0
	        LOOP
	            v_substring  := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
	            v_string     := SUBSTR (v_string, INSTR (v_string, '|')    + 1) ;
	            v_partnum    := NULL;
	            v_vnm        := NULL;
	            v_cost       := NULL;
	            v_qtyq       := NULL;
	            v_dframe     := NULL;
	            v_qid        := NULL;
	            v_uom        := NULL;
	            v_uomqty     := NULL;
	            v_lockfl     := NULL;
	            v_activefl   := NULL;
	            v_pid        := NULL;
	            v_price_id   := NULL;
	            v_grid_rowid := NULL;
	            v_void_fl:= NULL;
	            --Eg Input str:
	            -- pnum^vendor_nm^cost^qty_quoted^df_time^quote_id^UOM_id^UOM_qty^Lock_fl^Active_fl^Pricing_id^Grid_rowId|
	            v_partnum    := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
	            v_substring  := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
	            v_vnm        := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
	            v_substring  := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
	            v_cost       := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
	            v_substring  := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
	            v_qtyq       := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
	            v_substring  := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
	            v_dframe     := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
	            v_substring  := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
	            v_qid        := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
	            v_substring  := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
	            v_uom        := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
	            v_substring  := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
	            v_uomqty     := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
	            v_substring  := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
	            v_lockfl     := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
	            v_substring  := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
	            v_activefl   := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
	            v_substring  := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
	            v_pid        := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
	            v_substring  := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
	            v_grid_rowid := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1);
				v_void_fl := TO_CHAR(v_substring);
	           
	           -- If row is locked we are just updating void fl and skipping validation part
				IF v_void_fl <> 'Y'  THEN
	            BEGIN		              
		            --Inserting all the input data into the temp table, before start validating
		             INSERT  INTO my_temp_vendor_det
		                (
		                    ROW_ID, C205_PART_NUMBER_ID, C301_VENDOR_NAME
		                  , C405_COST_PRICE, C405_QTY_QUOTED, UOM_NAME
		                  , C405_UOM_QTY
		                )
		                VALUES
		                (
		                    trim(v_grid_rowid), trim(v_partnum), trim(v_vnm)
		                  , trim(v_cost), trim(v_qtyq), trim(v_uom)
		                  , trim(v_uomqty)
		                ) ;
		        --For Cost,Quote Qty,UOM Qty if the incorrect/special values are entered, 
		        --catch the exception & insert the respective error type in temp atble. 
	            EXCEPTION WHEN  INVALID_NUMBER 
	            THEN
	           		 BEGIN
			              SELECT TO_NUMBER(v_cost)  INTO v_costn   FROM DUAL;
			         EXCEPTION WHEN  INVALID_NUMBER 
	           		 THEN
		           		  INSERT INTO my_temp_key_value (MY_TEMP_TXN_ID, MY_TEMP_TXN_VALUE )
		        			 SELECT v_grid_rowid, 'COST' FROM DUAL;
	           		 END;
	           		 
	           		 BEGIN
			              SELECT TO_NUMBER(v_qtyq)  INTO v_qtyqn  FROM DUAL;
			         EXCEPTION WHEN  INVALID_NUMBER 
	           		 THEN
		           		  INSERT INTO my_temp_key_value (MY_TEMP_TXN_ID, MY_TEMP_TXN_VALUE )
		        			 SELECT v_grid_rowid, 'QTY_QUOTED' FROM DUAL;
	           		 END;
	           		 
	           		 BEGIN
			              SELECT TO_NUMBER(v_uomqty) INTO v_uomqtyn  FROM DUAL;
			         EXCEPTION WHEN  INVALID_NUMBER 
	           		 THEN
		           		  INSERT INTO my_temp_key_value (MY_TEMP_TXN_ID, MY_TEMP_TXN_VALUE )
		        			 SELECT v_grid_rowid, 'UOM_QTY' FROM DUAL;
	           		 END;
	            END;
	          END IF;
	        END LOOP;
	        
	        /************************
	         * Below is validation part
	         * this will isnert the Grid row with respective error types in 
	         */

	        --Validation for input part numbers 
	        --1. incorrect part numbers  2. parts thate are not mapped to the project.
	         INSERT INTO my_temp_key_value (MY_TEMP_TXN_ID, MY_TEMP_TXN_VALUE )
	             SELECT ROW_ID, 'PART_NO'
                   FROM my_temp_vendor_det
                  WHERE upper(C205_PART_NUMBER_ID) NOT IN
                    (                            
                         SELECT upper(trim(C205_PART_NUMBER_ID))
						   FROM T205_PART_NUMBER
						  WHERE C202_PROJECT_ID = p_project_id
                           --AND c205_active_fl IS NOT NULL
						  UNION
						 SELECT upper(trim(C205_PART_NUMBER_ID))
						   FROM T209_GENERIC_PARTNUMBER
						  WHERE C202_PROJECT_ID = p_project_id
                    ) ;
	            
                    select count(MY_TEMP_TXN_VALUE) into v_count
                    from my_temp_key_value 
                    where MY_TEMP_TXN_VALUE='PART_NO';
                    
	         --Validation for input vendor name   
	         INSERT INTO my_temp_key_value (MY_TEMP_TXN_ID, MY_TEMP_TXN_VALUE )
	         SELECT ROW_ID, 'VENDOR'
	           FROM my_temp_vendor_det
	          WHERE ROW_ID IN
	            (
	                 SELECT ROW_ID
	                   FROM my_temp_vendor_det
	                  WHERE upper (trim (C301_VENDOR_NAME)) NOT IN
	                    (
	                         SELECT upper (trim (C301_VENDOR_NAME)) FROM t301_vendor
	                          WHERE C301_VENDOR_ID = DECODE( NVL(p_vendor_id, 0), 0, C301_VENDOR_ID, p_vendor_id) --Added to fix BUG-5372
	                    )
	            ) ;

	         --Validation for cost <= 0     
	         INSERT INTO my_temp_key_value (MY_TEMP_TXN_ID, MY_TEMP_TXN_VALUE )
	         SELECT ROW_ID, 'COST'
	           FROM my_temp_vendor_det
	          WHERE ROW_ID IN
	            (
	                 SELECT ROW_ID
	                   FROM my_temp_vendor_det
	                  WHERE C405_COST_PRICE IS NULL	-- as per the existing vendor pricing screen we are not validating for -ve cost
	            ) ;

	         --Validation for QUOTE QTY <= 0    and it is not mandatory.
	         INSERT INTO my_temp_key_value (MY_TEMP_TXN_ID, MY_TEMP_TXN_VALUE )
	         SELECT ROW_ID, 'QTY_QUOTED'
	           FROM my_temp_vendor_det
	          WHERE ROW_ID IN
	            (
	                 SELECT ROW_ID FROM my_temp_vendor_det 
	                  WHERE C405_QTY_QUOTED IS NOT NULL 
	                    AND NVL (C405_QTY_QUOTED, - 99) <= 0
	            ) ;

	         --Validation for UOM  
	         INSERT INTO my_temp_key_value (MY_TEMP_TXN_ID, MY_TEMP_TXN_VALUE )
	         SELECT ROW_ID, 'UOM'
	           FROM my_temp_vendor_det
	          WHERE ROW_ID IN
	            (
	                 SELECT ROW_ID
	                   FROM my_temp_vendor_det
	                  WHERE upper (trim (UOM_NAME)) NOT IN
	                    (
	                         SELECT upper (trim (c901_code_nm))
	                           FROM t901_code_lookup
	                          WHERE C901_code_grp  = 'UOM'     --UOM code group
	                            AND c901_active_fl = '1'
	                    ) OR upper (trim (UOM_NAME)) IS NULL
	            ) ;
	  
	         --Validation for UOM  QTY <=0  
	         INSERT INTO my_temp_key_value (MY_TEMP_TXN_ID, MY_TEMP_TXN_VALUE )
	         SELECT ROW_ID, 'UOM_QTY'
	           FROM my_temp_vendor_det
	          WHERE ROW_ID IN
	            (
	                 SELECT ROW_ID FROM my_temp_vendor_det WHERE NVL (C405_UOM_QTY, -99) <= 0
	            ) ;
	    END IF;
	    
	  
	    gm_fch_vendor_price_err(v_grid_err_str);	       
	  	  
	    p_error_str := v_grid_err_str;
	    
	END gm_validate_vendor_price; 
END gm_pkg_pur_vendor_pricing;
/
