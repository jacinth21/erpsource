CREATE OR REPLACE PACKAGE BODY GM_PKG_OP_SET_PRIORITY_ALLOCATION
IS

-- Private Procedure Declaration 
PROCEDURE GM_SAV_DEMAND_SHEET_ID(
    p_set_priority_id IN T5400_SET_PRIORITY.C5400_SET_PRIORITY_ID%TYPE );
    
 
PROCEDURE GM_SAV_TMP_DEMAND_SHEET_ID(
    p_country_id IN T5402_SET_PRIORITY_MAPPING.C901_COUNTRY_ID%TYPE );
    
PROCEDURE GM_SAV_SP_ALLOCATION(
    p_set_priority_id IN T5400_SET_PRIORITY.C5400_SET_PRIORITY_ID%TYPE,
    p_user_id         IN T5400_SET_PRIORITY.C5400_LAST_UPDATED_BY%TYPE);


PROCEDURE GM_SAV_SP_REQUEST(
    p_sp_map_id IN T5402_SET_PRIORITY_MAPPING.C5402_SET_PRIORITY_MAP_ID%TYPE,
    p_user_id   IN T5402_SET_PRIORITY_MAPPING.C5402_LAST_UPDATED_BY%TYPE );
  

PROCEDURE GM_SAV_REQ_TO_ALLOCATION(
    p_sp_map_id  IN T5402_SET_PRIORITY_MAPPING.C5402_SET_PRIORITY_MAP_ID%TYPE,
    p_request_id IN T520_REQUEST.C520_REQUEST_ID%TYPE,
    p_user_id   IN T5402_SET_PRIORITY_MAPPING.C5402_LAST_UPDATED_BY%TYPE );

PROCEDURE GM_UPD_SET_MAPPING_DTLS(
    p_sp_map_id IN T5402_SET_PRIORITY_MAPPING.C5402_SET_PRIORITY_MAP_ID%TYPE,
    p_required_qty T5402_SET_PRIORITY_MAPPING.C5402_REQUIRED_QTY%TYPE,
    p_user_id IN T5402_SET_PRIORITY_MAPPING.C5402_LAST_UPDATED_BY%TYPE
  );
  
-- END Private Procedure Declaration 
     
  /*********************************************************************
  * Author  : Jayaprasanth Gurunathan
  * Description : This is main procedure to call set priority allocation
  * and set up the status and confirm the set priority
  *********************************************************************/
PROCEDURE GM_ALLOCATE_SET_PRIORITY_MAIN(
    p_set_priority_id IN T5400_SET_PRIORITY.C5400_SET_PRIORITY_ID%TYPE )
    
AS
  v_submitted_by      VARCHAR2(20);
  v_sp_status      NUMBER;
  v_sp_status_new NUMBER;
  
BEGIN

BEGIN
  -- get status and submitted by
  SELECT C5400_SUBMITED_BY,
    C901_SET_PRIORITY_STATUS
  INTO v_submitted_by,
    v_sp_status
  FROM T5400_SET_PRIORITY
  WHERE C5400_SET_PRIORITY_ID = p_set_priority_id
  AND C5400_VOID_FL          IS NULL
  FOR UPDATE;
  EXCEPTION
  	WHEN NO_DATA_FOUND THEN
    	v_submitted_by := NULL;
        v_sp_status := NULL;
 END;


  -- Check the status values not equal to Allocation in progress or Confirm in progress
  -- 108302 Allocation In Progress, 108305 Confirmation In Progress
  IF v_sp_status <> 108302 AND v_sp_status <> 108305 THEN
    RETURN;
  END IF;
  
    -- 108305 Confirmation In Progress
  IF v_sp_status = 108305 THEN
    GM_PKG_OP_SET_PRIORITY_TRANS.GM_VOID_SET_PRIORITY_ALLOCATION(p_set_priority_id, v_submitted_by);
  END IF;
  
  GM_SAV_DEMAND_SHEET_ID(p_set_priority_id);
  
  GM_SAV_SP_ALLOCATION(p_set_priority_id, v_submitted_by);
  
  -- get status 
  SELECT C901_SET_PRIORITY_STATUS
    INTO v_sp_status_new
  FROM T5400_SET_PRIORITY
    WHERE C5400_SET_PRIORITY_ID = p_set_priority_id
  	AND C5400_VOID_FL          IS NULL;
  
  
  --108304 Allocated , 108305 Confirmation In Progress
  IF v_sp_status_new = 108304 AND v_sp_status = 108305 THEN
    GM_SAV_ALLOCATION_CONFIRM(p_set_priority_id, v_submitted_by);
  END IF;

  
END GM_ALLOCATE_SET_PRIORITY_MAIN;


/*********************************************************************
* Author  : Jayaprasanth Gurunathan
* Description : By use set priority id and get mapped country list
*********************************************************************/
PROCEDURE GM_SAV_DEMAND_SHEET_ID(
    p_set_priority_id IN T5400_SET_PRIORITY.C5400_SET_PRIORITY_ID%TYPE )
AS
  v_country_id T5402_SET_PRIORITY_MAPPING.C5400_SET_PRIORITY_ID%TYPE;
  
  
  -- Get country id from set priority id
  CURSOR v_country_cur
  IS
    SELECT C901_COUNTRY_ID country
    FROM T5402_SET_PRIORITY_MAPPING
    WHERE C5400_SET_PRIORITY_ID = p_set_priority_id
    AND C5402_VOID_FL IS NULL
    GROUP BY C901_COUNTRY_ID;
    
    
BEGIN

-- Delete the temporary table entry initially
  DELETE FROM MY_TEMP_KEY_VALUE;
  	
  --
	INSERT INTO MY_TEMP_KEY_VALUE
	  (MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY
	  )
	SELECT t4015.C901_LEVEL_VALUE,
	  t4020.C4020_DEMAND_MASTER_ID
	FROM T4020_DEMAND_MASTER t4020,
	  T4015_GLOBAL_SHEET_MAPPING t4015
	WHERE t4015.C4015_GLOBAL_SHEET_MAP_ID = t4020.C4015_GLOBAL_SHEET_MAP_ID
	AND t4015.C901_LEVEL_ID               = 102580 -- worldwide
	AND t4020.C4020_VOID_FL              IS NULL
	AND c4015_void_fl                    IS NULL
	AND t4020.C901_DEMAND_TYPE            IN (40021,40022);
	
--
  FOR v_index IN v_country_cur
  LOOP
    GM_SAV_TMP_DEMAND_SHEET_ID(v_index.country);
  END LOOP;
  --
END GM_SAV_DEMAND_SHEET_ID;


/*********************************************************************
* Author  : Jayaprasanth Gurunathan
* Description : This procedure used to save the Set priority details to allocation table
*
*********************************************************************/
PROCEDURE GM_SAV_SP_ALLOCATION(
    p_set_priority_id IN T5400_SET_PRIORITY.C5400_SET_PRIORITY_ID%TYPE,
    p_user_id         IN T5400_SET_PRIORITY.C5400_LAST_UPDATED_BY%TYPE)
    
AS
	v_status_count number;
	v_status_value number;
	
  CURSOR set_priority_map
  IS
    SELECT C901_COUNTRY_ID country,
      C5402_SET_PRIORITY_MAP_ID set_map_id,
      C5402_REQUIRED_QTY required_qty
    FROM T5402_SET_PRIORITY_MAPPING
    WHERE C5400_SET_PRIORITY_ID = p_set_priority_id
    AND C5402_VOID_FL          IS NULL;
    
BEGIN

  -- Delete the temporary table entry initially
  DELETE FROM MY_TEMP_REQUEST;

-- Procedure call for save request info to temporary table
  GM_SAV_OPEN_REQUEST_TO_TEMP(p_set_priority_id);
  
  FOR v_index IN set_priority_map
  LOOP
   	GM_SAV_SP_REQUEST(v_index.set_map_id, p_user_id);
    GM_UPD_SET_MAPPING_DTLS(v_index.set_map_id, v_index.required_qty, p_user_id);
  END LOOP;
 
---  Get status count from mapping table
	SELECT COUNT(1) INTO v_status_count
	FROM T5402_SET_PRIORITY_MAPPING
	WHERE C5400_SET_PRIORITY_ID = p_set_priority_id
	AND C901_SET_MAPPING_STATUS = 108287 -- 108287	Failed	SPMSTA
	AND C5402_VOID_FL          IS NULL;

---	
	IF v_status_count = 0
	THEN
		v_status_value := 108304;	-- Allocated	SPRSTA
	ELSE
		v_status_value := 108303;	-- Allocation Failed	SPRSTA
	END IF;
---
		
	-- update set priority status		
	gm_pkg_op_set_priority_trans.gm_upd_set_priority_status(p_set_priority_id, v_status_value, p_user_id);
  
END GM_SAV_SP_ALLOCATION;


/*********************************************************************
* Author  : Jayaprasanth Gurunathan
* Description : This procedure used to validate the request and save the details to allocation table.
*
*********************************************************************/
PROCEDURE GM_SAV_SP_REQUEST(
    p_sp_map_id IN T5402_SET_PRIORITY_MAPPING.C5402_SET_PRIORITY_MAP_ID%TYPE,
    p_user_id   IN T5402_SET_PRIORITY_MAPPING.C5402_LAST_UPDATED_BY%TYPE )
    
AS

  v_set_id T5402_SET_PRIORITY_MAPPING.C207_SET_ID%TYPE;
  v_country_id   NUMBER;
  v_required_qty NUMBER;
  v_country_string varchar2(4000);
  -- request allocation cursor
  CURSOR cur_req
  IS
    SELECT request_id, req_date
    FROM
      (SELECT T520.C520_REQUEST_ID request_id,
        T520.C520_REQUEST_DATE req_date
      FROM MY_TEMP_REQUEST T520,
        MY_TEMP_KEY_VALUE tmp_demand, v_in_list tmp_country
      WHERE tmp_demand.MY_TEMP_TXN_KEY = T520.C520_REQUEST_TXN_ID
      AND tmp_demand.MY_TEMP_TXN_ID    = tmp_country.token
      AND T520.C207_SET_ID      = v_set_id
      AND T520.C901_REQUEST_SOURCE IN (50616,4000122) -- Order Planing, Safety Stock
      AND NOT EXISTS
        (SELECT T543.C520_REQUEST_ID
        FROM T5403_SET_PRIORITY_ALLOCATION T543
        WHERE T543.C520_REQUEST_ID = T520.C520_REQUEST_ID
        AND T543.C5403_VOID_FL IS NULL
        )
    UNION
    SELECT T520.C520_REQUEST_ID,
      T520.C520_REQUEST_DATE
    FROM MY_TEMP_REQUEST T520
    WHERE T520.C207_SET_ID = v_set_id
    AND T520.C901_REQUEST_SOURCE NOT IN (50616,4000122) -- Order Planing, Safety Stock
    AND NOT EXISTS
      (SELECT *
      FROM T5403_SET_PRIORITY_ALLOCATION T543
      WHERE T520.C520_REQUEST_ID = T543.C520_REQUEST_ID
      AND T543.C5403_VOID_FL IS NULL
      )
      )
    WHERE ROWNUM <= v_required_qty
    ORDER BY req_date;
    
  BEGIN
  
    -- get set id, country id and required quantity for the map id.
    SELECT C207_SET_ID,
      C901_COUNTRY_ID,
      C5402_REQUIRED_QTY
    INTO v_set_id,
      v_country_id,
      v_required_qty
    FROM T5402_SET_PRIORITY_MAPPING
    WHERE C5402_SET_PRIORITY_MAP_ID = p_sp_map_id
    AND C5402_VOID_FL              IS NULL ;
    
    
	SELECT RTRIM (XMLAGG (XMLELEMENT (e, sheet_county_id
	  || ',')) .EXTRACT ('//text()'), ',') country_id
	INTO v_country_string
	FROM
	  (SELECT C901_LEVEL_VALUE sheet_county_id
	  FROM T4015_GLOBAL_SHEET_MAPPING
	  WHERE C901_LEVEL_ID = 102580
	  AND c4015_void_fl  IS NULL
	  GROUP BY C901_LEVEL_VALUE
	  UNION
	  SELECT v_country_id sheet_county_id FROM dual
	  ) ; 
	  
    my_context.set_my_inlist_ctx (v_country_string);
    
    -- Inserting request to allocation table
    FOR v_index IN cur_req 
    LOOP
      GM_SAV_REQ_TO_ALLOCATION(p_sp_map_id, v_index.request_id, p_user_id);
    END LOOP;
    
    
  END GM_SAV_SP_REQUEST;
  
  
  
  /*********************************************************************
  * Author  : Jayaprasanth Gurunathan
  * Description : This is used for inserting set request details to allocation table
  *
  *********************************************************************/
PROCEDURE GM_SAV_REQ_TO_ALLOCATION(
    p_sp_map_id  IN T5402_SET_PRIORITY_MAPPING.C5402_SET_PRIORITY_MAP_ID%TYPE,
    p_request_id IN T520_REQUEST.C520_REQUEST_ID%TYPE,
    p_user_id   IN T5402_SET_PRIORITY_MAPPING.C5402_LAST_UPDATED_BY%TYPE )
AS
BEGIN
--
  INSERT
  INTO T5403_SET_PRIORITY_ALLOCATION
    (
      C5403_SET_PRIORITY_ALLOCATION_ID,
      C5402_SET_PRIORITY_MAP_ID,
      C520_REQUEST_ID,
      C5403_LAST_UPDATED_BY,
      C5403_LAST_UPDATED_DATE
    )
    VALUES
    (
      S5403_SET_PRIORITY_ALLOCATION.NEXTVAL,
      p_sp_map_id,
      p_request_id,
      p_user_id,
      SYSDATE
    );
--
END GM_SAV_REQ_TO_ALLOCATION;


/*********************************************************************
* Author  : Jayaprasanth Gurunathan
* Description : This procedure used to update the request Qty to new table (Set priority mapping table)
*
*********************************************************************/
PROCEDURE GM_UPD_SET_MAPPING_DTLS(
    p_sp_map_id IN T5402_SET_PRIORITY_MAPPING.C5402_SET_PRIORITY_MAP_ID%TYPE,
    p_required_qty T5402_SET_PRIORITY_MAPPING.C5402_REQUIRED_QTY%TYPE,
    p_user_id IN T5402_SET_PRIORITY_MAPPING.C5402_LAST_UPDATED_BY%TYPE
  )
AS

  v_required_qty  	NUMBER;
  v_request_qty   	NUMBER;
  v_sp_map_status 	NUMBER;
  v_set_id			T520_REQUEST.C207_SET_ID%TYPE;
  v_country_id 		NUMBER;
  
BEGIN

  -- get the request quantity from temporary table
  SELECT COUNT(1) INTO v_request_qty
	FROM T5403_SET_PRIORITY_ALLOCATION
  WHERE C5402_SET_PRIORITY_MAP_ID = p_sp_map_id
	AND C5403_VOID_FL IS NULL;
	 
  
  -- get set id, country id and required quantity for the map id.
  SELECT C207_SET_ID,
    C901_COUNTRY_ID,
    C5402_REQUIRED_QTY
  INTO v_set_id,
    v_country_id,
    v_required_qty
  FROM T5402_SET_PRIORITY_MAPPING
  WHERE C5402_SET_PRIORITY_MAP_ID = p_sp_map_id
  AND C5402_VOID_FL              IS NULL ;

  
  IF v_required_qty = v_request_qty THEN
    v_sp_map_status := 108286; -- 108286 Available SPMSTA
  ELSE
    v_sp_map_status := 108287; -- 108287 Failed SPMSTA
  END IF;
  
  -- To update the set status on the set mapping table
  GM_UPD_SET_PRIORITY_MAPPING_DTLS(p_sp_map_id,v_sp_map_status, v_request_qty, p_user_id );

  
END GM_UPD_SET_MAPPING_DTLS;



/*******************************************************
* Author  : Jayaprasanth Gurunathan
* Description : This procedure used to update the set priority mapping status and 
* and request qty
*******************************************************/
PROCEDURE GM_UPD_SET_PRIORITY_MAPPING_DTLS(
    p_sp_map_id IN T5402_SET_PRIORITY_MAPPING.C5402_SET_PRIORITY_MAP_ID%TYPE,
    p_sp_map_status IN T5402_SET_PRIORITY_MAPPING.C901_SET_MAPPING_STATUS%TYPE,
    p_request_qty T5402_SET_PRIORITY_MAPPING.C5402_REQUEST_QTY%TYPE,
    p_user_id IN T5402_SET_PRIORITY_MAPPING.C5402_LAST_UPDATED_BY%TYPE )
AS
BEGIN
---
  UPDATE T5402_SET_PRIORITY_MAPPING
  SET C901_SET_MAPPING_STATUS     = p_sp_map_status,
    C5402_REQUEST_QTY             = p_request_qty,
    C5402_LAST_UPDATED_BY         = p_user_id,
    C5402_LAST_UPDATED_DATE       = CURRENT_DATE
  WHERE C5402_SET_PRIORITY_MAP_ID = p_sp_map_id;
---
END GM_UPD_SET_PRIORITY_MAPPING_DTLS;


/*********************************************************************
* Author  : Jayaprasanth Gurunathan
* Description : Save demand sheet values to MY_TEMP_KEY_VALUE table
*
*********************************************************************/
PROCEDURE GM_SAV_TMP_DEMAND_SHEET_ID(
    p_country_id IN T5402_SET_PRIORITY_MAPPING.C901_COUNTRY_ID%TYPE )
AS

BEGIN

---
  INSERT INTO MY_TEMP_KEY_VALUE
    (MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY
    )
  SELECT T4015.C901_LEVEL_VALUE country_id,
    T4020.C4020_DEMAND_MASTER_ID demand_master_id
  FROM T4020_DEMAND_MASTER T4020,
    T4015_GLOBAL_SHEET_MAPPING T4015
  WHERE T4020.C4015_GLOBAL_SHEET_MAP_ID = T4015.C4015_GLOBAL_SHEET_MAP_ID
  AND T4015.C901_LEVEL_VALUE            = p_country_id
  AND T4020.C901_DEMAND_TYPE            IN (40021,40022)  -- 40021 Consignment,Inhouse DMDTP
  AND T4020.C4020_VOID_FL              IS NULL
  AND T4015.C4015_VOID_FL              IS NULL;

---  
END GM_SAV_TMP_DEMAND_SHEET_ID;


/*********************************************************************
* Author  : Jayaprasanth Gurunathan
* Description : Save request to temporary table
*
*********************************************************************/
PROCEDURE GM_SAV_OPEN_REQUEST_TO_TEMP(
    p_set_priority_id IN T5400_SET_PRIORITY.C5400_SET_PRIORITY_ID%TYPE )
AS
BEGIN

-- SET_PRIORITY_SOURCE : 	50616	Order Planning, 50618	Customer Service, 4000122	Safety Stock
my_context.set_my_inlist_ctx (get_rule_value('SET_PRIORITY_SOURCE', 'SET_PRIORITY'));

  -- get open status set priority id's
  INSERT
  INTO MY_TEMP_REQUEST
    (
      C520_REQUEST_ID,
      C207_SET_ID,
      C520_REQUEST_DATE,
      C520_REQUIRED_DATE,
      C520_REQUEST_TXN_ID,
      C901_REQUEST_SOURCE
    )
  SELECT T520.C520_REQUEST_ID,
    T520.C207_SET_ID,
    T520.C520_REQUEST_DATE,
    T520.C520_REQUIRED_DATE,
    T520.C520_REQUEST_TXN_ID,
    T520.C901_REQUEST_SOURCE
  FROM T520_REQUEST T520, v_in_list v_list, T207_SET_MASTER t207
  WHERE T520.C901_REQUEST_SOURCE    = v_list.token
  AND t207.C207_SET_ID = T520.C207_SET_ID
  AND T520.C520_MASTER_REQUEST_ID IS NULL -- to avoid back order request
  AND T520.C520_STATUS_FL       <> 40 -- closed
  AND T520.C1900_COMPANY_ID        = 1000
  AND T520.C207_SET_ID            IS NOT NULL
  AND T520.C520_VOID_FL           IS NULL
  AND T520.C520_DELETE_FL 		 IS NULL
  AND T520.C5403_REQUEST_PRIORITY IS NULL;  
  

---
END GM_SAV_OPEN_REQUEST_TO_TEMP;


 /*******************************************************
    * Author  : ppandiyan
    * Description: This Procedure is used to confirm
    * the set priority 
    *******************************************************/	
	PROCEDURE gm_sav_allocation_confirm(
 		p_set_priority_id  IN  T5400_SET_PRIORITY.c5400_set_priority_id%TYPE
	  , p_user_id   	   IN  t101_user.c101_user_id%TYPE
)
AS

	v_predef_qty       T5400_SET_PRIORITY.c5400_predefined_qty%TYPE;
	v_quarter	       T5400_SET_PRIORITY.c901_quarter%TYPE;
	v_pri_seq	  	   T5400_SET_PRIORITY.c5400_set_priority_seq%TYPE;
	v_pri_year	       T5400_SET_PRIORITY.c901_year%TYPE;
	v_year_quarter	   VARCHAR2(10);
	v_tot_pre_qty	   NUMBER;
	v_cnt			   NUMBER;
	v_req_pri_id       VARCHAR2(100);
	v_sp_status		   T5400_SET_PRIORITY.c901_set_priority_status%TYPE;
	v_sequence         VARCHAR2(100);
	v_prefix_len	   VARCHAR2(100);
	
 CURSOR set_priority_map_dtls
  IS
    SELECT c5402_set_priority_map_id map_id
    FROM T5402_SET_PRIORITY_MAPPING 
    WHERE C5400_SET_PRIORITY_ID = p_set_priority_id 
    AND C901_SET_MAPPING_STATUS = 108286 -- 108286	Available	SPMSTA
    AND C5402_VOID_FL  IS NULL
    ORDER BY c5402_set_priority_map_id ASC;
    
    
 CURSOR set_priority_alloc_dtls (v_set_map_id t5402_set_priority_mapping.C5402_SET_PRIORITY_MAP_ID%TYPE)
  IS
	 SELECT c520_request_id req_id
	 FROM T5403_SET_PRIORITY_ALLOCATION 
	 WHERE C5402_SET_PRIORITY_MAP_ID=v_set_map_id 
	 AND C5403_VOID_FL IS NULL
	 ORDER BY C5402_SET_PRIORITY_MAP_ID ASC;

BEGIN

  BEGIN  
		--getting set priority details
		select c5400_predefined_qty ,c901_quarter,c5400_set_priority_seq ,c901_year,
			c901_set_priority_status , to_char(to_date(c901_year, 'YYYY'), 'YY') || GET_CODE_NAME_ALT(c901_quarter) ----getting year quarter in the format YYQ (193)--2019 year quarter 3
		
		  into v_predef_qty,v_quarter,v_pri_seq,v_pri_year,v_sp_status,v_year_quarter
		from T5400_SET_PRIORITY  
		where c5400_set_priority_id = p_set_priority_id
		and c5400_void_fl is null;
		
		IF v_sp_status <> 108304    --if not in 'Allocated', then return
		THEN
			RETURN;
		END IF;
		
		--sum of predefined qty from the previous sequence
		select NVL(SUM(c5400_predefined_qty),0) 
		  into v_tot_pre_qty
		from T5400_SET_PRIORITY  
		where c5400_set_priority_seq < v_pri_seq
		and c901_quarter =  v_quarter
		and c901_year = v_pri_year
		and c5400_void_fl is null;
		
		
		
		  EXCEPTION  WHEN NO_DATA_FOUND THEN
   			v_predef_qty := NULL;
   			v_quarter := NULL;
   			v_pri_seq := NULL;
   			v_pri_year := NULL;
   			v_year_quarter := NULL;
   			v_tot_pre_qty := 0;
   			
   		  END;	
   		  
   		  Select DECODE(v_tot_pre_qty,0,v_predef_qty,v_tot_pre_qty) into v_prefix_len from dual;

		--looping the set priority map ids
		FOR set_map_dtls IN set_priority_map_dtls
		LOOP
		--based on the map id get the requset from the allocation table
			FOR set_pri_alloc IN set_priority_alloc_dtls(set_map_dtls.map_id)
	        LOOP
	            v_tot_pre_qty := v_tot_pre_qty + 1;
	            ---to get the value as 001 format
	            SELECT LPAD(v_tot_pre_qty, length(v_prefix_len), '0') INTO v_sequence FROM DUAL;
	            
	     		v_req_pri_id := v_year_quarter||'-'||v_sequence;
	     		
				gm_upd_request_priority_dtls(set_map_dtls.map_id,set_pri_alloc.req_id,v_req_pri_id,p_user_id);
				
			END LOOP;
		END LOOP;
	
 
        --allocation table failed count check based on the set priority id
        select count(t5403.c5402_set_priority_map_id)
        	into v_cnt
        from T5403_SET_PRIORITY_ALLOCATION t5403, t5402_set_priority_mapping t5402
        where t5403.c520_request_id is null or t5403.c5403_request_priority is null 
        and   t5402.c5400_set_priority_id =  p_set_priority_id
        and   t5403.c5402_set_priority_map_id = t5402.c5402_set_priority_map_id
        and   t5403.c5403_void_fl is null
        and   t5402.c5402_void_fl is null;
        
        --if it is not failed, then update the status as confirmed
        IF v_cnt = 0
        THEN
        		gm_pkg_op_set_priority_trans.gm_upd_set_priority_status(p_set_priority_id,'108306',p_user_id);
        END IF;
		  
END gm_sav_allocation_confirm;


 /*******************************************************
    * Author  : ppandiyan
    * Description: This Procedure is used to update the priority  number
    * for the request id and the map id
    *******************************************************/
 	PROCEDURE gm_upd_request_priority_dtls(
		p_set_map_id    IN T5400_SET_PRIORITY.c5400_set_priority_id%TYPE
	  , p_request_id    IN t520_request.C520_REQUEST_ID%TYPE
	  , p_priority_no   IN T5403_SET_PRIORITY_ALLOCATION.C5403_REQUEST_PRIORITY%TYPE
	  , p_user_id 		IN t101_user.c101_user_id%TYPE
)  
AS
BEGIN
	

		UPDATE T5403_SET_PRIORITY_ALLOCATION 
		SET C5403_REQUEST_PRIORITY = p_priority_no,
		C5403_LAST_UPDATED_BY=p_user_id , 
		C5403_LAST_UPDATED_DATE = CURRENT_DATE 
		WHERE C520_REQUEST_ID=p_request_id 
		AND C5402_SET_PRIORITY_MAP_ID=p_set_map_id
		AND C5403_VOID_FL IS NULL; 
		
		
		update t520_request 
		set c5403_request_priority = p_priority_no , 
		c520_last_updated_by=p_user_id,
		c520_last_updated_date = current_date 
		where c520_request_id=p_request_id
		and c520_void_fl is null; 
          
		  
END gm_upd_request_priority_dtls;


-- End of Package --
END GM_PKG_OP_SET_PRIORITY_ALLOCATION;
/