/* Formatted on 2009/11/25 13:32 (Formatter Plus v4.8.0) */
-- @"c:\database\packages\operations\Purchasing\Globus_App\gm_pkg_gop_ttp_lock.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_gop_ttp_lock
IS
--
	/*******************************************************
     * Description : Procedure to save monthly order qty
     * Author 	 : VPrasath
	 * Algorithm:
	 *	1. Create a new entry onto t4052_ttp_detail
	 * 	2. Load part, qty, historyfl information into t205c table
   	 *******************************************************/
--
	PROCEDURE gm_fc_sav_ord_qty (
	    p_string		   IN		VARCHAR2
	  , p_dm_ttp_dtl_id	   IN		t4052_ttp_detail.c4052_ttp_detail_id%TYPE	
	  , p_dm_ttp_type	   IN 	    t4050_ttp.c901_ttp_type%TYPE
	  , p_userid		   IN		t4052_ttp_detail.c4052_created_by%TYPE	  
	)
	AS
		v_string	   VARCHAR2 (4000) := p_string;
		v_substring    VARCHAR2 (4000);
		v_partnumber   VARCHAR2 (20);
		v_qty		   VARCHAR2 (10);
		v_crossoverfl  VARCHAR2(20);	
	BEGIN
		
		WHILE INSTR (v_string, '|') <> 0
		LOOP
			v_partnumber := NULL;
			v_qty		 := NULL;
			v_substring  := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
			v_string	 := SUBSTR (v_string, INSTR (v_string, '|') + 1);
			v_partnumber := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
			v_qty		 := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
			
			BEGIN
			SELECT	t205.c205_crossover_fl 
			    INTO v_crossoverfl
				FROM t205_part_number t205
			   WHERE t205.c205_part_number_id = v_partnumber;
			EXCEPTION WHEN NO_DATA_FOUND THEN
				v_crossoverfl := NULL;
			END;

			IF (v_crossoverfl IS NULL OR p_dm_ttp_type = '50310')
			THEN
				gm_cm_sav_partqty (v_partnumber, v_qty, p_dm_ttp_dtl_id, p_userid, 90801, 4301, 4318);
			END IF;
			
		END LOOP;
		
	END gm_fc_sav_ord_qty;
--
	/*******************************************************
     * Description : Procedure to save Request qty
     * Author 	 : VPrasath
	 * Logic:
	 *	1. Create a Request
	 * 	2. Attach it to World Wide Sheet / Child Sheet
	 *  3. If the set par is reduced since last month,
	 *     need to move the request to common pool.
   	 *******************************************************/
--	
	PROCEDURE gm_fc_sav_request_qty (
		 p_demand_id     IN  t4040_demand_sheet.c4040_demand_sheet_id%TYPE 
	   , p_dmd_master_id IN  t4040_demand_sheet.c4020_demand_master_id%TYPE
	   , p_period	     IN  t4042_demand_sheet_detail.c4042_period%TYPE
	   , p_qty	         IN  t205c_part_qty.c205_available_qty%TYPE
	   , p_set_id        IN  t207_set_master.c207_set_id%TYPE
	   , p_request_by    IN  t4052_ttp_detail.c4052_created_by%TYPE
	   , p_demand_type   IN  t901_code_lookup.c901_code_id%TYPE	
	   , p_user_id       IN  t4052_ttp_detail.c4052_created_by%TYPE
	   , p_error_req     OUT CLOB
	)
	AS
	v_required_date DATE;
	v_req_count NUMBER;
	
	CURSOR v_req_cur 
	IS
	SELECT req_id FROM
	(
		SELECT c520_request_id REQ_ID
		   FROM t520_request t520 
		  WHERE c520_request_txn_id = p_dmd_master_id
		    AND t520.c207_set_id = p_set_id
		    AND c901_request_source = 50616
		    AND c520_required_date BETWEEN TRUNC (v_required_date, 'mm') 
			AND LAST_DAY (v_required_date)
			AND c520_status_fl > 0	--ONHOLD
			AND c520_master_request_id IS NULL
			AND C1900_COMPANY_ID = 1000
			AND c520_void_fl IS NULL   						   
	   ORDER BY c520_status_fl ASC 
	)
	WHERE ROWNUM <= (v_req_count-p_qty);
	
	BEGIN
		
		-- PMT-32804: TTP Summary lock and generate
		-- to catch the exception and throws in bean class
		
		p_error_req := NULL;
		
		
		v_required_date := LAST_DAY(p_period);
	
	    SELECT COUNT (c520_request_id)
		  INTO v_req_count
		  FROM t520_request
		 WHERE c520_request_txn_id = p_dmd_master_id
		   AND c901_request_source = 50616
		   AND c207_set_id = p_set_id
		   AND C1900_COMPANY_ID = 1000
		   AND c520_required_date BETWEEN TRUNC (v_required_date, 'mm') 
		   							  AND LAST_DAY (v_required_date)
		   AND c520_status_fl > 0	--ONHOLD
		   AND c520_master_request_id IS NULL
		   AND c520_void_fl IS NULL;

		   
		IF v_req_count > p_qty 
		THEN
	  
			FOR v_index IN v_req_cur 
			LOOP
				-- to handle the exception
				BEGIN		
					gm_pkg_op_request_master.gm_move_req_to_compool(v_index.req_id, p_user_id);
					
				EXCEPTION WHEN OTHERS
				THEN
					p_error_req := p_error_req || v_index.req_id ||', ';
				END;	
			END LOOP;
			
			ELSE
			gm_pkg_op_ttp_process_request.gm_sav_ttp_set_request (p_dmd_master_id
															  , 50616 --  Order Planning ( Source ) 
															  , p_set_id
															  , TRUNC (SYSDATE) -- Request Date
															  , v_required_date -- Required Date
															  , p_qty
															  , p_user_id
															  , p_request_by
															  , p_demand_type
															  , p_demand_id
															 );
		END IF;	   
		
		
		
	END gm_fc_sav_request_qty;	
--
	/*******************************************************
     * Description : Procedure to save Request qty for the 
     * 				 set PAR
     * Author 	 : VPrasath
	 * Logic:
	 *	1. Check the open request's qty with set par, 
	 * 	2. if set par is more than the available requests,
	 *     create set requests for the difference.
	 *  3. If the set par is reduced since last month,
	 *     need to move the request to common pool.
   	 *******************************************************/
--	
	PROCEDURE gm_fc_sav_set_par (
		 p_demand_id     IN  t4040_demand_sheet.c4040_demand_sheet_id%TYPE 
	   , p_dmd_master_id IN  t4040_demand_sheet.c4020_demand_master_id%TYPE
	   , p_qty	         IN  t205c_part_qty.c205_available_qty%TYPE
	   , p_set_id        IN  t207_set_master.c207_set_id%TYPE
	   , p_request_by    IN  t4052_ttp_detail.c4052_created_by%TYPE
	   , p_demand_type   IN  t901_code_lookup.c901_code_id%TYPE	
	   , p_user_id       IN  t4052_ttp_detail.c4052_created_by%TYPE
	   , p_error_set_par     OUT CLOB
	)
	AS

	v_req_count NUMBER;
	
	CURSOR v_req_cur 
	IS
	SELECT req_id FROM
	(
		SELECT c520_request_id REQ_ID
		   FROM t520_request t520 
		  WHERE c520_request_txn_id = p_dmd_master_id
		    AND t520.c207_set_id = p_set_id
		    AND (c901_request_source <> 50616 OR c520_request_txn_id IS NULL)
		    AND C520_REQUEST_TO IS NULL -- Excluding the Set Par RQ which are assigned to a field Sales
	        AND c520_status_fl > 0	--ONHOLD
			AND c520_status_fl < 40	--Closed
			AND c520_master_request_id IS NULL
			AND c520_void_fl IS NULL   						   
	   ORDER BY c520_required_date 
	)
	WHERE ROWNUM <= (v_req_count-p_qty);
	
	BEGIN
		-- PMT-32804: TTP Summary lock and generate
		-- to catch the exception and throws in bean class
		
		p_error_set_par := NULL;
		
	  SELECT COUNT (c520_request_id)
		  INTO v_req_count
		  FROM t520_request
		 WHERE c520_request_txn_id = p_dmd_master_id
		   AND (c901_request_source <> 50616 OR c520_request_txn_id IS NULL)
		   AND C520_REQUEST_TO IS NULL -- Excluding the Set Par RQ which are assigned to a field Sales
		   AND c207_set_id = p_set_id
		   AND c520_status_fl > 0	--ONHOLD
		   AND c520_status_fl < 40	--Closed
		   AND c520_master_request_id IS NULL
		   AND c520_void_fl IS NULL;
		
		IF v_req_count > p_qty 
		THEN
	  
		FOR v_index IN v_req_cur 
		LOOP
			-- to handle the exception
			BEGIN
			 gm_pkg_op_request_master.gm_move_req_to_compool(v_index.req_id, p_user_id);
			 
			EXCEPTION WHEN OTHERS
			THEN
			p_error_set_par := p_error_set_par || v_index.req_id ||', ';
			
			END; 
		END LOOP;
		
		ELSE
		gm_pkg_op_ttp_process_request.gm_sav_ttp_set_par (p_dmd_master_id
													  , 4000122 -- SET PAR ( Source )
													  , p_set_id
													  , TRUNC (SYSDATE) -- Request Date
													  , NULL -- Required Date
													  , p_qty
													  , p_user_id
													  , p_request_by
													  , p_demand_type
													  , p_demand_id
													 );
		END IF;
		
	END gm_fc_sav_set_par;	
	
END gm_pkg_gop_ttp_lock;
/
