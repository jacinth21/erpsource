--@"C:\Database\Packages\Operations\purchasing\gm_pkg_wo_lead_time_txn.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_wo_lead_time_txn
IS
/****************************************************************************
 * Description: This procedure is used to fetch lead time for the part number
 * Author	  : Agilan Singaravel
 ****************************************************************************/
PROCEDURE gm_fch_part_lead_time(
	p_part_num      IN    t205_part_number.c205_part_number_id%type,
	p_vendor_id		IN	  t301_vendor.c301_vendor_id%type,
	p_lead_date	    OUT	  date
)
AS
v_lead_time		DATE;
BEGIN
	
	BEGIN
		SELECT trunc(current_date + (c3011_lead_time * 7))
		  INTO v_lead_time 
		  FROM T3011_VENDOR_LEAD_TIME 
		 WHERE c205_part_number_id = p_part_num
		   AND c301_vendor_id = p_vendor_id
		   AND c3011_void_fl is null;
	EXCEPTION WHEN NO_DATA_FOUND THEN
	BEGIN
		SELECT trunc(current_date + (c205d_attribute_value * 7))
		  INTO v_lead_time 
		  FROM T205D_PART_ATTRIBUTE
		 WHERE c901_attribute_type = 4000101 --part lead time
		   AND c205_part_number_id = p_part_num
		   AND c205d_void_fl is null;
	EXCEPTION WHEN NO_DATA_FOUND THEN
		v_lead_time := trunc(current_date + (7*7));
	END;
	END;
	
	p_lead_date := v_lead_time;
	
END gm_fch_part_lead_time;
/**************************************************************
   * Description : Procedure to update work order due date
   * Author 	 : Agilan Singaravel
   ************************************************************/
	PROCEDURE gm_upd_wo_due_date_process(
		p_wo_str	  		IN	 CLOB, 
		p_input_str     	IN	 CLOB,	
		p_user_id			IN	 t101_user.c101_user_id%type,
		p_out_err			OUT	 CLOB,
		p_out_po_dtl        OUT  CLOB,
		p_out_email_str		OUT	 CLOB		
	)
	AS
	v_strlen   		NUMBER := NVL (LENGTH (p_input_str), 0) ;
  	v_string   		CLOB := p_input_str;
  	v_substring     VARCHAR2(4000);
  	v_count			NUMBER;
  	v_wo_id			varchar2(20);
    v_wo_duedt		varchar2(20);
    v_wo_err		CLOB;
    v_count_wo		NUMBER;
    v_wo_invalid	CLOB;
	BEGIN
	--validate wo
   	gm_validate_wo_due_date_upd(p_wo_str||',');
 
   	IF v_strlen > 0 THEN
    WHILE INSTR (v_string, '|') <> 0
    LOOP
      v_substring              := SUBSTR (v_string, 0, INSTR (v_string, '|') - 1) ;
      v_string                 := SUBSTR (v_string, INSTR (v_string, '|')    + 1) ;
      
      v_wo_id				   := null;
      v_wo_duedt			   := null;
      
      v_wo_id				   := SUBSTR (v_substring, 0, INSTR (v_substring, '^') - 1) ;
      v_substring              := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
      v_wo_duedt               := v_substring;
 
      SELECT count(1)
        INTO v_count
        FROM my_temp_list
       WHERE my_temp_txn_id = v_wo_id;
       
       SELECT count(1)
         INTO v_count_wo
         FROM t402_work_order
        WHERE c402_work_order_id = v_wo_id
          AND c402_void_fl is null;

      IF (v_count > 0) THEN
      		gm_upd_wo_due_date(v_wo_id,v_wo_duedt,p_user_id);
      ELSIF (v_count_wo = 0) THEN
      		v_wo_invalid := v_wo_invalid || ',' || v_wo_id;
      ELSE
      		v_wo_err := v_wo_err || ',' ||v_wo_id;
      END IF;
   
    END LOOP;
    
  		p_out_err := v_wo_invalid || '~' ||v_wo_err;
    	gm_fch_wo_due_date_response(p_out_po_dtl,p_out_email_str);
    END IF;
  
	END gm_upd_wo_due_date_process;
   /**************************************************************************
   * Description : Procedure is used to validate wo id and insert temp table
   * Author 	 : Agilan Singaravel
   **************************************************************************/
	PROCEDURE gm_validate_wo_due_date_upd(
		p_wo_str	  		IN	 CLOB		
	)
	AS
	v_company_id		t1900_company.c1900_company_id%type;
	v_token	varchar2(4000);
	BEGIN
		SELECT  get_compid_frm_cntx()
    	INTO    v_company_id
    	FROM    DUAL;
    	
		my_context.set_my_cloblist(p_wo_str);
		DELETE FROM my_temp_list;

		INSERT INTO my_temp_list(
		SELECT t402.c402_work_order_id FROM t401_purchase_order t401,t402_work_order t402, v_clob_list v_clob
		 WHERE t401.c401_purchase_ord_id = t402.c401_purchase_ord_id
		   AND t402.c402_work_order_id = v_clob.token 
		   AND t401.c1900_company_id = v_company_id
		   AND t401.c401_void_fl is null
		   AND t402.c402_void_fl is null
		  -- AND t401.c901_status <> 108365 -- closed
		   AND t402.c402_status_fl <> 3
		  --AND t402.c402_work_order_id in (select token from v_clob_list)
		);

	END gm_validate_wo_due_date_upd;
 /***********************************************************************
   * Description : Procedure to update work order due date in t402 table
   * Author 	 : Agilan Singaravel
   *********************************************************************/
	PROCEDURE gm_upd_wo_due_date(
		p_wo_id	  		IN	 	t402_work_order.c402_work_order_id%type,
		p_wo_duedt		IN		varchar2,
		p_user_id		IN		t101_user.c101_user_id%type
	)
	AS	
	v_cmp_dtfmt			varchar2(20);
	BEGIN
		
	SELECT get_compdtfmt_frm_cntx() INTO v_cmp_dtfmt FROM dual;
	
		UPDATE T402_WORK_ORDER
		   SET c402_wo_due_date = TO_DATE(p_wo_duedt,v_cmp_dtfmt)
		     , c402_last_updated_by = p_user_id
		     , c402_last_updated_date = current_date
		 WHERE c402_work_order_id = p_wo_id
		   AND C402_VOID_FL IS NULL;
		   
	END gm_upd_wo_due_date;
 /******************************************************************************
   * Description : Procedure is used to get po and vendor name detail based on WO
   * Author 	 : Agilan Singaravel
   ******************************************************************************/
	PROCEDURE gm_fch_wo_due_date_response(
		p_out_po		OUT	 	CLOB,
		p_out_email		OUT		CLOB
	)
	AS
	v_company_id		t1900_company.c1900_company_id%type;
	v_cmp_dtfmt			varchar2(20);
	BEGIN
		SELECT  get_compid_frm_cntx(),get_compdtfmt_frm_cntx()
    	INTO    v_company_id,v_cmp_dtfmt
    	FROM    DUAL;
		
		SELECT JSON_ARRAYAGG(JSON_OBJECT(
		'woid' value t402.c402_work_order_id,
		'woduedt' value to_char(t402.c402_wo_due_date,v_cmp_dtfmt),
		'purchaseid' value t401.c401_purchase_ord_id,
		'vendornm' value t301.c301_vendor_name
		)RETURNING CLOB) INTO p_out_po
		FROM t401_purchase_order t401,t402_work_order t402,t301_vendor t301, my_temp_list my_temp
       WHERE t401.c401_purchase_ord_id = t402.c401_purchase_ord_id
        AND t402.c301_vendor_id = t301.c301_vendor_id
		AND t401.c1900_company_id = v_company_id
		AND t402.c402_work_order_id = my_temp.my_temp_txn_id
		AND t401.c401_void_fl is null
		AND t402.c402_void_fl is null;
		--AND t402.c402_work_order_id in (select my_temp_txn_id FROM my_temp_list);
		
     --this out is used to send the email for work order due date changed
	 SELECT RTRIM (XMLAGG (XMLELEMENT (e, my_temp_txn_id || ',')) .EXTRACT ('//text()'), ',')
       INTO p_out_email
       FROM my_temp_list;
              
	END gm_fch_wo_due_date_response;
	
  /**************************************************************
   * Description : Procedure to update work order ack date
   * Author 	 : Agilan Singaravel
   ************************************************************/
	PROCEDURE gm_upd_wo_ack_date_process(
		p_wo_str	  		IN	 CLOB, 
		p_input_str     	IN	 CLOB,	
		p_user_id			IN	 t101_user.c101_user_id%type,
		p_out_err			OUT	 CLOB	
	)
	AS
	v_strlen   		NUMBER := NVL (LENGTH (p_input_str), 0) ;
  	v_string   		CLOB := p_input_str;
  	v_substring     VARCHAR2(4000);
  	v_count			NUMBER;
  	v_wo_id			varchar2(20);
    v_wo_ackdt		varchar2(20);
    v_wo_qty		NUMBER;
    v_wo_err		CLOB;
    v_count_wo		NUMBER;
    v_wo_invalid	CLOB;
    v_company_id	t1900_company.c1900_company_id%type;
    v_out_error 	CLOB;
	BEGIN
	
	SELECT  get_compid_frm_cntx()
    	INTO    v_company_id
    	FROM    DUAL;
    	
 		DELETE FROM MY_TEMP_KEY_VALUE;
   	IF v_strlen > 0 THEN
    WHILE INSTR (v_string, '|') <> 0
    LOOP
      v_substring              := SUBSTR (v_string, 0, INSTR (v_string, '|') - 1) ;
      v_string                 := SUBSTR (v_string, INSTR (v_string, '|')    + 1) ;
      
      v_wo_id				   := null;
      v_wo_ackdt			   := null;
      v_wo_qty				   := null;
      
      v_wo_id				   := SUBSTR (v_substring, 0, INSTR (v_substring, '^') - 1) ;
      v_substring              := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
     
      v_wo_ackdt               := SUBSTR (v_substring, 0, INSTR (v_substring, '^') - 1) ;
      v_substring              := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
      v_wo_qty				   := v_substring;
 
 	INSERT
		INTO MY_TEMP_KEY_VALUE(
		
		SELECT t402.c402_work_order_id,v_wo_qty,v_wo_ackdt
		FROM t401_purchase_order t401,
		t402_work_order t402
		WHERE t401.c401_purchase_ord_id = t402.c401_purchase_ord_id
		AND t401.c1900_company_id = v_company_id
		AND t401.c401_void_fl is null
		AND t402.c402_void_fl is null
		AND t402.c402_work_order_id = v_wo_id
		AND t402.c402_status_fl <> 3
		);
 	
      SELECT count(1)
        INTO v_count
        FROM MY_TEMP_KEY_VALUE
       WHERE my_temp_txn_id = v_wo_id;
      
       SELECT count(1)
         INTO v_count_wo
         FROM t402_work_order
        WHERE c402_work_order_id = v_wo_id
          AND c402_void_fl is null;

      IF (v_count_wo = 0) THEN
      		v_wo_invalid := v_wo_invalid || ',' || v_wo_id;
      END IF;
      IF (v_count = 0) THEN
      		v_wo_err := v_wo_err || ',' ||v_wo_id;
      END IF;   
    END LOOP;

    IF (v_count_wo > 0) THEN 
    	gm_check_pend_qty(v_out_error);
   	END IF;
   	
    IF v_wo_invalid IS NOT NULL OR v_wo_err IS NOT NULL OR v_out_error IS NOT NULL THEN
  		p_out_err := v_wo_invalid || '~' ||v_wo_err|| '~' ||v_out_error;
  	END IF;
  	 
  	IF p_out_err IS NULL THEN
  		gm_void_wo_ack(p_user_id);
  		gm_sav_wo_ack_date(p_input_str,p_user_id);
  	END IF;
  	
    END IF;
  
	END gm_upd_wo_ack_date_process;
	 /**************************************************************
   * Description : Procedure to check the pending quntity
   * Author 	 : Saravanan M
   ************************************************************/
	PROCEDURE gm_check_pend_qty(
		p_error		OUT		CLOB
	)
	AS
	v_pendng_qty	NUMBER;
	v_error 		CLOB;
	v_sum_pend_qty	NUMBER;
	CURSOR fch_excess
	is
		select MY_TEMP_txn_id woid,sum(MY_TEMP_TXN_KEY) ackqty from
		MY_TEMP_KEY_VALUE group by MY_TEMP_txn_id;
	BEGIN
		FOR fch_excess_wo in fch_excess
	LOOP
		v_error := null;
	BEGIN
		SELECT get_wo_pend_qty (c402_work_order_id, c402_qty_ordered)
		INTO v_pendng_qty --10
		FROM t402_work_order
		WHERE c402_work_order_id = fch_excess_wo.woid
		      AND c301_vendor_id NOT IN (
			SELECT c906_rule_value
			FROM t906_rules
			WHERE c906_rule_id = 'SKIP_QTY' 
			      AND c906_rule_grp_id = 'SKIPACKQTYVALIDATION'
			      AND c906_void_fl IS NULL
		)
		      AND c402_void_fl IS NULL;
		 EXCEPTION
	 	 WHEN NO_DATA_FOUND THEN
	     v_pendng_qty := 0;
	     v_error := 'Y';
	    END;
    IF (v_error <> 'Y' OR v_error IS NULL) THEN
		v_sum_pend_qty := v_pendng_qty+((v_pendng_qty*10)/100);
	
		IF (fch_excess_wo.ackqty > v_sum_pend_qty) THEN
			v_error := fch_excess_wo.woid;
		END IF;
		IF v_error is not null then
			p_error := p_error ||','|| v_error;
		END IF;
	END IF;
	END LOOP;

END gm_check_pend_qty;
	
  /*******************************************************************************
   * Description : Procedure to insert work order ack date with qty in t402d table
   * Author 	 : Agilan Singaravel
   *******************************************************************************/
	PROCEDURE gm_sav_wo_ack_date(
		p_input_str		IN		CLOB,
		p_user_id		IN		t101_user.c101_user_id%type
	)
	AS	
	v_cmp_dtfmt		varchar2(20);
	v_strlen   		NUMBER := NVL (LENGTH (p_input_str), 0) ;
  	v_string   		CLOB := p_input_str;
  	v_substring     VARCHAR2(4000);
  	v_wo_id			varchar2(20);
    v_wo_ackdt		varchar2(20);
    v_wo_qty		NUMBER;
	BEGIN
		
	SELECT get_compdtfmt_frm_cntx() INTO v_cmp_dtfmt FROM dual;
	
	IF v_strlen > 0 THEN
    WHILE INSTR (v_string, '|') <> 0
    LOOP
      v_substring              := SUBSTR (v_string, 0, INSTR (v_string, '|') - 1) ;
      v_string                 := SUBSTR (v_string, INSTR (v_string, '|')    + 1) ;
      
      v_wo_id				   := null;
      v_wo_ackdt			   := null;
      v_wo_qty				   := null;
      
      v_wo_id				   := SUBSTR (v_substring, 0, INSTR (v_substring, '^') - 1) ;
      v_substring              := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
     
      v_wo_ackdt               := SUBSTR (v_substring, 0, INSTR (v_substring, '^') - 1) ;
      v_substring              := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
      v_wo_qty				   := v_substring;
 
     
        INSERT INTO T4027_WORK_ORDER_ACK_DATE(c402_work_order_id,c4027_wo_ack_date,c4027_qty,c4027_created_by,c4027_created_date)
		     VALUES (v_wo_id,TO_DATE(v_wo_ackdt,v_cmp_dtfmt),v_wo_qty,p_user_id,current_date);
    END LOOP;
    END IF;
	   
	END gm_sav_wo_ack_date;
  /**************************************************************************************
   * Description : Procedure to update work order void fl to Y in t402d table based on WO
   * Author 	 : Agilan Singaravel
   **************************************************************************************/
	PROCEDURE gm_void_wo_ack(
		p_user_id	IN	 t101_user.c101_user_id%type
	)
	AS
	CURSOR temp_wo_id
	IS
		SELECT my_temp_txn_id wo_id
          FROM MY_TEMP_KEY_VALUE;
	BEGIN
		FOR temp_wo_dtls IN temp_wo_id
		LOOP
			UPDATE T4027_WORK_ORDER_ACK_DATE
		   	   SET c4027_void_fl = 'Y'
		         , c4027_last_updated_by  = p_user_id
		         , c4027_last_updated_date = current_date
		     WHERE c402_work_order_id = temp_wo_dtls.wo_id
		       AND c4027_void_fl IS NULL;
    END LOOP;
	END gm_void_wo_ack;
END gm_pkg_wo_lead_time_txn;
/