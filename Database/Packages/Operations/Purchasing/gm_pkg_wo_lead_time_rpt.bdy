--@"C:\Database\Packages\Operations\purchasing\gm_pkg_wo_lead_time_rpt.pkg";

CREATE OR REPLACE PACKAGE BODY gm_pkg_wo_lead_time_rpt
IS
/**********************************************************************
   * Description : Procedure to fetch Vendor Lead Time Details (PC-3204)
   * Author 	 : Agilan Singaravel
   ********************************************************************/
--
	PROCEDURE gm_fch_vendor_wo_lead_time_rpt(
		p_vendor_id	  		IN	 t301_vendor.c301_vendor_id%TYPE, 
		p_part_num      	IN	 varchar2,	
		p_purchase_agent	IN	 t301_vendor.c301_purchasing_agent_id%type,
		p_division_id		IN	 t1910_division.c1910_division_id%type,
		p_project_id        IN   t202_project.c202_project_id%TYPE,
		p_out_cur			OUT	 CLOB		 
	)
	AS
	v_cmp_dtfmt			varchar2(20);
	BEGIN
		
	SELECT get_compdtfmt_frm_cntx() INTO v_cmp_dtfmt FROM dual;
		--JSON QUERY
   SELECT JSON_ARRAYAGG(JSON_OBJECT(
		'vendornm' value t301.c301_vendor_name,
		'purchaseAgentnm' value t101.c101_user_f_name || ' '||t101.c101_user_l_name,
		'partnum' value t205.c205_part_number_id,
		'pdesc' value REGEXP_REPLACE(t205.c205_part_num_desc,'[^0-9A-Za-z]', ' '),
		'projnm' value t202.c202_project_nm,
		'divnm' value t1910.c1910_division_name,
		'leadtime' value t3011.c3011_lead_time,
		'updatedby' value get_user_name(t3011.c3011_last_updated_by),
		'updateddate' value to_char(t3011.c3011_last_updated_date,v_cmp_dtfmt)
		)  ORDER BY t301.c301_vendor_name,t205.c205_part_number_id
  RETURNING CLOB) INTO p_out_cur
   FROM t301_vendor t301,t3011_vendor_lead_time t3011,t205_part_number t205,t202_project t202,t1910_division t1910,
   		t101_user t101
  WHERE t3011.c301_vendor_id = t301.c301_vendor_id
    AND t3011.c205_part_number_id = t205.c205_part_number_id
    AND t205.c202_project_id = t202.c202_project_id
    AND t101.c101_user_id = t301.c301_purchasing_agent_id
    AND t1910.c1910_division_id = t202.c1910_division_id
    AND regexp_like (t205.c205_part_number_id, NVL (REGEXP_REPLACE(p_part_num,'[+]','+'), REGEXP_REPLACE(t205.c205_part_number_id,'[+]','+')))
    AND t301.c301_vendor_id = nvl(p_vendor_id,t301.c301_vendor_id)
    AND t202.c202_project_id = nvl(p_project_id,t202.c202_project_id)
    AND NVL(C301_PURCHASING_AGENT_ID,'-999') = DECODE (p_purchase_agent, 0, NVL(c301_purchasing_agent_id,'-999'), p_purchase_agent)
    AND t1910.c1910_division_id = decode(p_division_id,0,t1910.c1910_division_id,p_division_id)	
    AND t202.C202_VOID_FL is NULL
   	AND t1910.C1910_VOID_FL is NULL
   	AND t3011.c3011_void_fl is null;	
   	
	END gm_fch_vendor_wo_lead_time_rpt;

END gm_pkg_wo_lead_time_rpt;
/