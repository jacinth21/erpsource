--@"c:/Database/Packages/Operations/Purchasing/gm_pkg_pur_vendor_cost.bdy";
CREATE OR REPLACE PACKAGE BODY gm_pkg_pur_vendor_cost
IS
    /*************************************************************************
    * Description : Procedure to save the standard cost information to master table (T302).
    *     This information come from Branch Medical - using AX, below field will be update.
    * Author   : Manikandan
    *************************************************************************/
PROCEDURE gm_sav_standard_cost (
        p_vendor_id   IN t302_vendor_standard_cost.c301_vendor_id%TYPE,
        p_part_number IN t302_vendor_standard_cost.c205_part_number_id%TYPE,
        p_std_cost    IN t302_vendor_standard_cost.c302_standard_cost%TYPE,
        p_user_id     IN t302_vendor_standard_cost.c302_last_updated_by%TYPE
        )
AS
BEGIN



     UPDATE t302_vendor_standard_cost
     SET c302_standard_cost      = p_std_cost, c302_last_updated_by = p_user_id, c302_last_updated_date = CURRENT_DATE
      WHERE c301_vendor_id      = p_vendor_id
        AND c205_part_number_id = p_part_number;
    --
    IF (SQL%ROWCOUNT = 0) THEN
         INSERT
           INTO t302_vendor_standard_cost
            (
                c302_vendor_std_cost_id, c301_vendor_id, c205_part_number_id
              , c302_standard_cost, c302_last_updated_by, c302_last_updated_date
            )
            VALUES
            (
                s302_vendor_standard_cost.nextval, p_vendor_id, p_part_number
              , p_std_cost, p_user_id, CURRENT_DATE
            ) ;
    END IF;
     
END gm_sav_standard_cost;
/*************************************************************************
* Description : Procedure to save the standard cost information to log table
*     Once Master table update then, using trigger (trg_t302_standard_cost_history) this procedure will call
automaticllay.
* Author   : Manikandan
*************************************************************************/
PROCEDURE gm_sav_std_cost_history
    (
        p_vendor_id   IN t302a_vendor_standard_cost_log.c301_vendor_id%TYPE,
        p_std_cost_id IN t302a_vendor_standard_cost_log.c302_vendor_std_cost_id%TYPE,
        p_part_number IN t302a_vendor_standard_cost_log.c205_part_number_id%TYPE,
        p_std_cost    IN t302a_vendor_standard_cost_log.c302_standard_cost%TYPE,
        p_updated_by  IN t302a_vendor_standard_cost_log.c302a_last_updated_by%TYPE
    )
AS
BEGIN
     INSERT
       INTO t302a_vendor_standard_cost_log
        (
            c302a_vendor_std_cost_log_id, c302_vendor_std_cost_id, c301_vendor_id
          , c205_part_number_id, c302_standard_cost, c302a_last_updated_by
          , c302a_last_updated_date
        )
        VALUES
        (
            s302A_VENDOR_STANDARD_COST_LOG.nextval, p_std_cost_id, p_vendor_id
          , p_part_number, p_std_cost, p_updated_by
          , CURRENT_DATE
        ) ;
END gm_sav_std_cost_history;
/*************************************************************************
* Description : Procedure used to send the COGS error email
* Author   : Manikandan
*************************************************************************/
PROCEDURE gm_send_cogs_error_email
    (
        p_txn_id        IN t822_costing_error_log.c822_txn_id%TYPE,
        p_part_number   IN t822_costing_error_log.c205_part_number_id%TYPE,
        p_qty           IN t822_costing_error_log.c822_qty%TYPE,
        p_post_type     IN t822_costing_error_log.c901_posting_type%TYPE,
        p_party_id      IN t822_costing_error_log.c810_party_id%TYPE,
        p_user_id       IN t822_costing_error_log.c822_created_by%TYPE,
        p_cogs_err_type IN t822_costing_error_log.c901_error_type%TYPE
    )
AS
    v_from_cost_type t820_costing.c901_costing_type%TYPE;
    v_to_cost_type t820_costing.c901_costing_type%TYPE;
    --
    v_company_cnt_id t1900_company.c1900_company_id%TYPE;
    v_plant_cnt_id t5040_plant_master.c5040_plant_id%TYPE;
BEGIN
     SELECT get_compid_frm_cntx (), get_plantid_frm_cntx ()
       INTO v_company_cnt_id, v_plant_cnt_id
       FROM dual;
    BEGIN
         SELECT c901_from_costing_type, c901_to_costing_type
           INTO v_from_cost_type, v_to_cost_type
           FROM t804_posting_ref
          WHERE c901_transaction_type = p_post_type
            AND c804_delete_fl        = 'N';
    EXCEPTION
    WHEN OTHERS THEN
        v_from_cost_type := NULL;
        v_to_cost_type   := NULL;
    END;
    -- to call the COGS error
	-- PC-991: To pass the local cost information and it used at repost COGS error
	
    gm_save_costing_error_log (p_part_number, p_qty, p_txn_id, v_from_cost_type, v_to_cost_type, p_post_type,
    p_party_id, p_user_id, NULL, NULL, p_cogs_err_type, v_company_cnt_id, v_plant_cnt_id, v_company_cnt_id, NULL,
    v_company_cnt_id, NULL) ;
END gm_send_cogs_error_email;
/*************************************************************************
* Description : Function used to fetch the standard cost information- based on vendor and part #
* Author   : Manikandan
*************************************************************************/
FUNCTION get_vendor_std_cost (
        p_vendor_id   IN t302_vendor_standard_cost.c301_vendor_id%TYPE,
        p_part_number IN t302_vendor_standard_cost.c205_part_number_id%TYPE)
    RETURN NUMBER
AS
    v_std_cost t302_vendor_standard_cost.c302_standard_cost%TYPE;
BEGIN
     SELECT c302_standard_cost
       INTO v_std_cost
       FROM t302_vendor_standard_cost
      WHERE c301_vendor_id      = p_vendor_id
        AND c205_part_number_id = p_part_number
        AND c302_void_fl       IS NULL;
    --
    RETURN v_std_cost;
EXCEPTION
WHEN OTHERS THEN
    RETURN NULL;
END get_vendor_std_cost;
/*************************************************************************
* Description : Function used to fetch the vendor costing type - based on vendor
* Author   : Manikandan
*************************************************************************/
FUNCTION get_vendor_cost_type (
        p_vendor_id IN t302_vendor_standard_cost.c301_vendor_id%TYPE)
    RETURN NUMBER
AS
    v_vendor_cost_type t301_vendor.c901_vendor_cost_type%TYPE;
BEGIN
     SELECT c901_vendor_cost_type
       INTO v_vendor_cost_type
       FROM t301_vendor
      WHERE c301_vendor_id = p_vendor_id;
    RETURN v_vendor_cost_type;
EXCEPTION
WHEN OTHERS THEN
    RETURN NULL;
END get_vendor_cost_type;
/*************************************************************************
* Description : Function used to fetch the standard cost 
* Author   : Prabhu Vigneshwaran
*************************************************************************/
PROCEDURE gm_fch_standard_cost
   (
   		 p_part_id   IN  T302_VENDOR_STANDARD_COST.C205_PART_NUMBER_ID%TYPE,
    	 p_vendor_id IN  T302_VENDOR_STANDARD_COST.c301_vendor_id%TYPE,
    	 p_project_id IN t205_part_number.C202_PROJECT_ID%TYPE,
   		 P_report_cursor OUT Types.cursor_type
   	)

   	
AS
BEGIN
	
	OPEN P_report_cursor
						FOR
  	  			
 						SELECT t405.C205_PART_NUMBER_ID PNUM, 
 							   t205.C205_PART_NUM_DESC Part_desc, 
 							   t202.C202_PROJECT_NM project_name,
  							   t901_part_family.c901_code_nm prod_family, 
  							   t301.C301_VENDOR_ID vendor_id, 
  							   t301.C301_VENDOR_NAME vendor_name, 
  							   t302.C302_STANDARD_COST standard_cost,
  							   t302.C302_HISTORY_FL history_fl
  					   FROM   T405_VENDOR_PRICING_DETAILS t405, 
  					          T302_VENDOR_STANDARD_COST t302, 
  					          T205_PART_NUMBER t205,
                              T202_PROJECT t202, 
                              T301_VENDOR t301, 
                              T901_CODE_LOOKUP t901_part_family
 					  WHERE   t301.C301_VENDOR_ID           = t405.C301_VENDOR_ID
   						 AND t405.C301_VENDOR_ID           = t302.C301_VENDOR_ID (+)
   						 AND t205.c205_part_number_id      = t405.C205_PART_NUMBER_ID
  					     AND t202.C202_PROJECT_ID          = t205.C202_PROJECT_ID
      					 AND t405.C205_PART_NUMBER_ID      = t302.C205_PART_NUMBER_ID (+)
      					 AND t901_part_family.c901_code_id = t205.C205_PRODUCT_FAMILY
    					-- AND t405.C405_ACTIVE_FL           = 'Y'
   						 AND t301.c901_VENDOR_COST_TYPE    = 106561--Standard Cost
   						 AND t405.C301_VENDOR_ID           = DECODE (p_vendor_id, 0, t405.C301_VENDOR_ID, p_vendor_id)
   						 AND t205.C202_PROJECT_ID          = DECODE (p_project_id,'0', t205.C202_PROJECT_ID, p_project_id)
    					 AND t405.C405_VOID_FL            IS NULL
   					     AND t302.C302_VOID_FL(+)         IS NULL
   					     AND REGEXP_LIKE (t205.c205_part_number_id, CASE
   					    WHEN TO_CHAR (p_part_id) IS NOT NULL
       					THEN TO_CHAR (REGEXP_REPLACE(p_part_id,'[+]','\+'))
        				ELSE REGEXP_REPLACE(t205.c205_part_number_id,'[+]','\+')
    					END)
						GROUP BY t405.C205_PART_NUMBER_ID, t205.C205_PART_NUM_DESC, t202.C202_PROJECT_NM
 								 , t901_part_family.c901_code_nm,t301.C301_VENDOR_ID, t301.C301_VENDOR_NAME, t302.C302_STANDARD_COST
 								 , t302.C302_HISTORY_FL ;


END gm_fch_standard_cost;
    /*************************************************************************
    * Description : Function used to fetch standard cost history
    * Author   : prabhu vigneshwaran M D
    *************************************************************************/
PROCEDURE gm_fch_standard_cost_history
			(
				 p_part_num_id IN  T302A_VENDOR_STANDARD_COST_LOG.C205_PART_NUMBER_ID %TYPE,
				 p_vendor_id IN  T302_VENDOR_STANDARD_COST.c301_vendor_id%TYPE,
				 p_cost_cursor OUT Types.cursor_type
			)
AS
		v_date_fmt    VARCHAR2 (20);
BEGIN
	SELECT get_compdtfmt_frm_cntx() 
	INTO v_date_fmt   FROM dual; 
	 OPEN p_cost_cursor
	 	FOR
						
 			SELECT 	t205.C205_PART_NUMBER_ID pnum,
 					t205.C205_PART_NUM_DESC part_desc, 
 					t301.C301_VENDOR_NAME vendor_name,
 				    t302a.C302_STANDARD_COST cost, 
 					get_user_name(t302a.C302A_LAST_UPDATED_BY) updated_by,    
 					TO_CHAR (t302a.C302A_LAST_UPDATED_DATE,v_date_fmt||' HH:MI:SS AM') updated_date,
  				    t302a.C302A_VENDOR_STD_COST_LOG_ID cost_log_id
  			 FROM   T302A_VENDOR_STANDARD_COST_LOG t302a, 
  			 		T205_PART_NUMBER t205, 
  			 		t301_vendor t301
 			 WHERE 	t205.C205_PART_NUMBER_ID = t302a.C205_PART_NUMBER_ID
    				AND t301.C301_VENDOR_ID      = t302a.C301_VENDOR_ID
   				 	AND t301.C301_VENDOR_ID      = p_vendor_id
   				 	AND t205.C205_PART_NUMBER_ID = p_part_num_id
				ORDER BY t302a.C302A_LAST_UPDATED_DATE DESC;

 		
 		
END gm_fch_standard_cost_history;
  /*************************************************************************
    * Description : Function used to Edit standard cost Details
    * Author   : prabhu vigneshwaran M D
    *************************************************************************/

PROCEDURE gm_fch_std_cost_edit_dtls
							(
									p_part_id IN  T302_VENDOR_STANDARD_COST.C205_PART_NUMBER_ID%TYPE,
									 p_vendor_id IN  T302_VENDOR_STANDARD_COST.c301_vendor_id%TYPE,
									p_edit_cursor OUT Types.cursor_type
							)
AS
BEGIN
	
	OPEN p_edit_cursor
	FOR
		      SELECT  get_vendor_name(p_vendor_id) vendorname, 
					  p_vendor_id  vendorid, 
					  t205.C205_PART_NUMBER_ID pnum,
   					  t205.C205_PART_NUM_DESC partdesc, 
  					  t302.C302_STANDARD_COST cost, 
 					  t302.C302_HISTORY_FL historyfl
   		 		FROM  T302_VENDOR_STANDARD_COST t302, 
   		 		 	  T205_PART_NUMBER t205
  				WHERE 
  						t205.C205_PART_NUMBER_ID   = t302.C205_PART_NUMBER_ID (+)
   					AND t205.C205_PART_NUMBER_ID   = p_part_id
    				AND NVL(t302.C301_VENDOR_ID, -999)        = DECODE (t302.C301_VENDOR_ID, NULL, -999, p_vendor_id)
    				AND t302.C302_VOID_FL (+)        IS NULL;

 
 END gm_fch_std_cost_edit_dtls;
 /********************************************************************
* Description : This Procedure Used to save standard cost and partnumber
* 			    in t302 table in bulk upload basis.
* Author	  : Prabhuvigneshwaran			
*************************************************************************/
PROCEDURE gm_sav_standard_cost_bulk_upload (
        p_vendor_id      IN    t302_vendor_standard_cost.c301_vendor_id%TYPE,
        p_partinputstr   IN    CLOB,
        p_inputstr       IN    CLOB,
        p_user_id        IN    t302_vendor_standard_cost.c302_last_updated_by%TYPE,
        p_out_msg        OUT   CLOB
    ) AS

        v_string             CLOB := p_inputstr;
        v_substring          VARCHAR2(4000);
       
    --Invalid part
        v_msg_invalid_part   VARCHAR2(4000);
        v_msg_prt_cmpny      VARCHAR2(4000);
    --part number and cost  split
        v_cost               VARCHAR2(4000);
        v_out_status         VARCHAR2(1) := 'N';
        v_partnumber         t302_vendor_standard_cost.c205_part_number_id%TYPE;
    BEGIN
	 
	--To call Invalid part Number validation 
		gm_pkg_pur_vendor_cost.gm_validate_part_mapping(p_vendor_id, p_partinputstr, v_msg_invalid_part);
        
        IF v_msg_invalid_part IS NOT NULL THEN         
	     p_out_msg := v_out_status || '##' || v_msg_invalid_part;
	     --
        END IF;
	
      --If error message is  null split partnumber and cost.
      --Format:partnumber^cost,|partnumber^cost.  
        
        IF v_msg_invalid_part IS NULL THEN
        --String Split
            WHILE instr(v_string, '|') <> 0 LOOP
                v_substring := substr(v_string, 1, instr(v_string, '|') - 1);
                v_string := substr(v_string, instr(v_string, '|') + 1);
				--
                v_partnumber := NULL;
				v_cost := NULL;
				--
                v_partnumber := substr(v_substring, 1, instr(v_substring, '^') - 1);
                v_substring := substr(v_substring, instr(v_substring, '^') + 1);
                v_cost := v_substring;
                --
				gm_pkg_pur_vendor_cost.gm_sav_standard_cost(p_vendor_id, v_partnumber, v_cost, p_user_id);

            END LOOP;--String loop
			
		--set flag  y for success message
            v_out_status := 'Y';
            p_out_msg := v_out_status;
        END IF;--if loop end

    END gm_sav_standard_cost_bulk_upload;  
 
	/****************************************************************
    * Description : Procedure to validate the Part number mapped with Vendor
    * Author      : Jayaprasanth Gurunathan
    *****************************************************************/    
    
   PROCEDURE gm_validate_part_mapping (
        p_vendor_id                         IN T301_VENDOR.c301_vendor_id%type,
        p_partinputstr                      IN  CLOB,
        p_out_invalid_part_mapping          OUT CLOB
      )
    AS
     v_invalid_part_mapping CLOB;
    
  BEGIN
      	my_context.set_my_cloblist (p_partinputstr);  
                
       SELECT RTRIM (XMLAGG (XMLELEMENT (e, token || ',')) .EXTRACT ('//text()'), ',')
       INTO v_invalid_part_mapping
       FROM v_clob_list
       WHERE TO_CHAR (token) NOT IN
        (
			SELECT T405.C205_PART_NUMBER_ID
			FROM T405_VENDOR_PRICING_DETAILS T405,
			  T301_VENDOR T301
			WHERE T405.C405_COST_PRICE IS NOT NULL
			AND T405.C405_VOID_FL      IS NULL
			AND T405.C405_ACTIVE_FL     = 'Y'
			AND T301.C301_VENDOR_ID     = T405.C301_VENDOR_ID
			AND T405.C301_VENDOR_ID     = p_vendor_id
        ) ;
        
  IF v_invalid_part_mapping IS NOT NULL THEN
       p_out_invalid_part_mapping       := v_invalid_part_mapping ;
  END IF;
END gm_validate_part_mapping;   
    
    
END gm_pkg_pur_vendor_cost;
/
