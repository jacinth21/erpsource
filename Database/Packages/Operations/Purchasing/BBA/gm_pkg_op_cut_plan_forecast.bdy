create or replace PACKAGE BODY gm_pkg_op_cut_plan_forecast
IS

/*******************************************************
  * Author  : gpalani
  * Description: This is a wrapper procedure to create the Cut Plan job
  *******************************************************/
PROCEDURE gm_create_cut_plan_report
(
p_company_id  IN t5041_plant_company_mapping.c1900_company_id%TYPE,
p_plant_id 	  IN t5041_plant_company_mapping.c5040_plant_id%TYPE,
p_user_id     IN t101_user.c101_user_id%TYPE
)

AS
 v_date_fmt				t906_rules.c906_rule_value%TYPE;
 v_email_to 			t906_rules.c906_rule_value%TYPE;
 v_email_msg 			VARCHAR2 (2000);
 v_email_sub 			VARCHAR2 (2000);
 
 

BEGIN
 
-- Assigning the company id and Plant id to the global variable so that the company id and plant id can be accessible in all the procedure.
 v_company_id := p_company_id;
 v_plant_id := p_plant_id;
 
-- Company and Plant id is hard coded since the Primary table (t2560_thb_load) is not having the company id stored.
    gm_pkg_cor_client_context.gm_sav_client_context (v_company_id, v_plant_id) ;

--  Create Master table for each week Cut Plan
	gm_pkg_op_cut_plan_forecast.gm_create_cut_plan_master(p_user_id);
	
--  Fetch BACK_ORD_QTY, Due in 3 weeks, Due after 3 weeks data
    gm_pkg_op_cut_plan_forecast.gm_sav_open_ack_order_data;

--  3 month sale qty and one month avg qty 
    gm_pkg_op_cut_plan_forecast.gm_sav_sav_3_month_sale_qty;

--  Update Project Category for the part number   
    gm_pkg_op_cut_plan_forecast.gm_sav_project_category(v_cut_plan_master_id);

--  Insert data in temp table
    gm_pkg_op_cut_plan_forecast.gm_update_temp_table(v_cut_plan_master_id);

--  Update the inventory value for each part number
    gm_pkg_op_cut_plan_forecast.gm_update_inventory_data(v_cut_plan_master_id);
    
--  Update the THB WIP inventory value for each part number
    gm_pkg_op_cut_plan_forecast.gm_update_thb_inventory_data(v_cut_plan_master_id);   

--  Update the THB week released value for each part number    
    gm_pkg_op_cut_plan_forecast.gm_update_thb_released_data(v_cut_plan_master_id);

--  Find Need Qty for each part
    gm_pkg_op_cut_plan_forecast.gm_find_need_qty(v_cut_plan_master_id);

--  Week 1 qty for each part
    gm_pkg_op_cut_plan_forecast.gm_update_week_qty(v_cut_plan_master_id);

-- Commit added at the end of all the data updates to avoid if there is any issue with the email component so that the job will get executed.

commit;

SELECT get_rule_value('EMAIL_TO','CUT_PLAN_BBA') 
		    INTO v_email_to FROM DUAL;
		
		SELECT get_rule_value('EMAIL_SUCCESS_SUB','CUT_PLAN_BBA') 
		    INTO v_email_sub FROM DUAL;
		            
		SELECT get_rule_value('EMAIL_SUCCESS_MSG','CUT_PLAN_BBA') 
		    INTO v_email_msg FROM DUAL;
		
		v_email_msg:=v_email_msg ||'. <br>';
		
		-- Call the email procedure to send email to the user
	gm_com_send_html_email(v_email_to,v_email_sub,NULL,v_email_msg);
		
   EXCEPTION WHEN OTHERS
      THEN 
	  SELECT get_rule_value('EMAIL_TO','CUT_PLAN_BBA') 
		    INTO v_email_to FROM DUAL;
		
		SELECT get_rule_value('EMAIL_ERROR_SUB','CUT_PLAN_BBA') 
		    INTO v_email_sub FROM DUAL;
		            	
		SELECT get_rule_value('EMAIL_ERROR_MSG','CUT_PLAN_BBA') 
		    INTO v_email_msg FROM DUAL;
		
		v_email_msg:=v_email_msg ||'. <br>';
		
		-- Call the email procedure to send email to the user
		gm_com_send_html_email(v_email_to,v_email_sub,NULL,v_email_msg);
 END gm_create_cut_plan_report;

/*******************************************************
  * Author  : gpalani
  * PMT-55067
  * Description:  procedure to create Cut Plan Master table
  *******************************************************/
PROCEDURE gm_create_cut_plan_master
(
p_user_id t101_user.c101_user_id%TYPE
)
	AS

v_cut_plan_load_master_id T4025_CUT_PLAN_LOCK.C4025_CUT_PLAN_LOCK_ID%TYPE ;
v_cut_plan_lock_id T4025_CUT_PLAN_LOCK.C4025_CUT_PLAN_LOCK_ID%TYPE ;

BEGIN

-- Updating the previous week cut plan to closed.Only one Cut Plan lock will be in-progress.

	 UPDATE t4025_cut_plan_lock 
	   SET c901_status='109581', C4025_LAST_UPDATED_DATE=current_date,C4025_LAST_UPDATED_BY=p_user_id
	 WHERE c901_status=109580
     AND c4025_void_fl IS NULL;
	    

	INSERT
	       INTO T4025_CUT_PLAN_LOCK
	        (
	            C4025_CUT_PLAN_LOCKED_DATE, C4025_CUT_PLAN_START_DATE
	          , C4025_CUT_PLAN_END_DATE, C901_STATUS, C4025_CREATED_BY,C4025_CREATED_DATE
	          , c1900_company_id
	        )
	        VALUES
	        (   current_date, current_date, current_date+15, '109580', 30301 -- 109580 Cut PLan Started 30301- Manual Load
	           ,current_date,v_company_id) 
	           RETURNING C4025_CUT_PLAN_LOCK_ID INTO v_cut_plan_load_master_id; 
	   /* Returning Primary key is to avoid additional select statement to fetch the master lock id.
	      v_cut_plan_master_id is a global variable declared in the package. This is to avoid passing this to multiple procedures.
	      The master lock id can be accessible from any procedure within the same package.
	   */
		v_cut_plan_master_id:=v_cut_plan_load_master_id;
		
END gm_create_cut_plan_master;

/*******************************************************
  * Author  : gpalani
  * PMT-55067
  * Description:  procedure to fetch and save the open Ack order data.
*******************************************************/
PROCEDURE gm_sav_open_ack_order_data
 
AS
    v_bo_txn_cursor TYPES.CURSOR_TYPE;
    v_part_num t205_part_number.c205_part_number_id%TYPE;
    v_temp_part_num t205_part_number.c205_part_number_id%TYPE;
    v_acct_id t704_account.c704_account_id%TYPE;
    v_acct_nm t704_account.c704_account_nm%TYPE;
    v_ref_type    VARCHAR2 (50) ;
    v_ref_id      VARCHAR2 (20) ;
    v_req_date    VARCHAR2 (20) ;
    v_pend_qty    NUMBER;
    v_shelf_qty   NUMBER;
    v_credit_type VARCHAR2 (20) ;
    v_ord_qty     NUMBER;
    v_ship_qty    NUMBER;
    v_cust_po t401_purchase_order.c401_purchase_ord_id%TYPE;
    v_back_ord_qty NUMBER:='0';
    v_less_than_3_weeks NUMBER:='0';
    v_greater_than_3_weeks NUMBER:='0';
    v_part_num_desc t205_part_number.c205_part_num_desc%type;
    nRowsUpdated NUMBER;
       
BEGIN
     my_context.set_my_inlist_ctx ('70018,70002,70003,70019,70005,70007,70023,70006,70008,70004,70014,70022,70009,70013,70020,70001') ;
    
     gm_pkg_cor_client_context.gm_sav_client_context (v_company_id, v_plant_id) ;

     DELETE FROM MY_TEMP_PART_LIST;
     
    /* Fetch Back Order Details from existing portal procedure
    * V_BO_TXN_CURSOR is the out cursor from the existing procedure and not passing account id since we need all part
    * Pass the out cursor to IT_FCH_BCK_ORD_TXN procedure and save the back order transaction
    */
    -- Fetch Open AB Transactions. Portal Procedure
    gm_pkg_op_backorder_rpt.gm_fch_ord_ack_bo ('', v_bo_txn_cursor) ;
    
    LOOP
        -- Fetch the out Cursor
        FETCH v_bo_txn_cursor
           INTO v_part_num, v_part_num_desc, v_acct_id
          , v_acct_nm, v_cust_po, v_ref_id
          , v_shelf_qty, v_ord_qty, v_ship_qty
          , v_pend_qty, v_req_date, v_credit_type;
          
        EXIT
        
    WHEN V_BO_TXN_CURSOR%NOTFOUND;
        -- Below method will insert the Back Order Transaction
        
        BEGIN
        
        v_req_date:=to_date(v_req_date,'MM/DD/YYYY');
        
            IF (v_req_date <=CURRENT_DATE) THEN
                 
                v_back_ord_qty:=v_pend_qty;
    
            END IF;
            
          
            IF (v_req_date >CURRENT_DATE  AND  v_req_date<=CURRENT_DATE + 21)THEN
                 
                v_less_than_3_weeks:=v_pend_qty;
    
            END IF;
            
         IF (v_req_date >CURRENT_DATE+21) THEN
                 
                v_greater_than_3_weeks:=v_pend_qty;
    
            END IF;
            
           
         SELECT NVL (t205d.c205d_attribute_value, v_part_num)
               INTO v_temp_part_num
               FROM t205d_part_attribute t205d,  t2023_part_company_mapping t2023
              WHERE t205d.c205_part_number_id = v_part_num
               AND t205d.c205d_attribute_valuE <> 'N/A'
               AND t205d.c901_attribute_type = 103730
               AND t2023.c1900_company_id=v_company_id
               AND t2023.c205_part_number_id=t205d.c205d_attribute_value
               AND t205d.c205d_void_fl      IS NULL;
                
        EXCEPTION
        WHEN OTHERS THEN
            v_temp_part_num := v_part_num;
        END;
                     
        
        BEGIN
        
        UPDATE t4026_cut_plan_lock_detail
            SET c4026_back_order_qty   = c4026_back_order_qty + v_back_ord_qty, 
            c4026_less_than_3_weeks    = c4026_less_than_3_weeks+v_less_than_3_weeks, 
            c4026_greater_than_3_weeks = c4026_greater_than_3_weeks+v_greater_than_3_weeks
        WHERE c205_part_number_id = v_temp_part_num
        AND c4025_cut_plan_lock_id=v_cut_plan_master_id;
        	
   		nRowsUpdated := sql%rowcount;
   
           IF (nRowsUpdated= 0) THEN

         INSERT INTO t4026_cut_plan_lock_detail
                (
                    c4025_cut_plan_lock_id,c205_part_number_id, c4026_back_order_qty,c4026_less_than_3_weeks,c4026_greater_than_3_weeks
                )
                VALUES
                (
                    v_cut_plan_master_id,v_temp_part_num, v_back_ord_qty,v_less_than_3_weeks,v_greater_than_3_weeks
                ) ;
                
            END IF;
          END;
            v_back_ord_qty:=0;
            v_less_than_3_weeks:=0;
            v_greater_than_3_weeks:=0;
            
             BEGIN 
         UPDATE my_temp_part_list
        SET c205_part_number_id                = v_temp_part_num
          WHERE c205_part_number_id = v_temp_part_num;
          
        IF (SQL%ROWCOUNT            = 0) THEN
             INSERT
               INTO my_temp_part_list
                (
                    c205_part_number_id
                )
                VALUES
                (
                    v_temp_part_num
                ) ;
        END IF;
      
      END;  
    END LOOP;
    
    CLOSE v_bo_txn_cursor;
EXCEPTION
WHEN OTHERS THEN
    CLOSE v_bo_txn_cursor;
    RAISE;

END gm_sav_open_ack_order_data;

/*******************************************************
  * Author  : gpalani
  * PMT-55067
  * Description:  procedure to update the 3 month sales qty, 1 month sales qty and safety stock.
*******************************************************/
PROCEDURE gm_sav_sav_3_month_sale_qty

AS

v_safety_stock_count t4026_cut_plan_lock_detail.c4026_safety_stock%type:='0';

CURSOR THREE_MONTH_SALE_PARTS
IS
  SELECT DISTINCT THREEMONSALES.pnum,
                  CEIL(THREEMONSALES.item_qty / 3) one_month_avg_sale_qty ,
                  THREEMONSALES.item_qty three_month_sale_qty ,T205.c205_part_num_desc Part_desc
            FROM
         (SELECT t502.c205_part_number_id pnum,
                 SUM (DECODE (c901_order_type, 2524, NULL, t502.c502_item_qty)) item_qty ,
                 SUM (t502.c502_item_price * t502.c502_item_qty) SALES,
                 t205.c205_part_num_desc part_desc
            FROM t501_order t501, t502_item_order t502,t205_part_number t205
			WHERE t501.c501_order_id            = t502.c501_order_id
			 AND NVL (t501.c901_order_type,  - 9999) NOT IN ('2533', '2518', '2519', '101260')
			 AND TRUNC (t501.c501_order_date)    >= add_months(trunc(sysdate, 'month'), - 3) 
             AND t501.c501_order_date            < trunc(sysdate, 'month')
			-- AND TRUNC (t501.c501_order_date)           >= CURRENT_DATE - 90
			 --AND TRUNC (t501.c501_order_date)            < CURRENT_DATE
			 AND t205.c205_product_class                <> 4033 -- Excluding Demo Parts
			 AND t502.c205_part_number_id                =t205.c205_part_number_id
			 AND t502.c205_part_number_id NOT           IN
         (SELECT t205d.c205_part_number_id private_part
            FROM t205d_part_attribute t205d,t2023_part_company_mapping t2023
           WHERE t205d.c901_attribute_type  = 103730
		    AND t205d.c205d_attribute_value IS NOT NULL
		    AND t205d.c205d_attribute_value <> 'N/A'
		    AND t2023.c1900_company_id       = v_company_id
		    AND t2023.c205_part_number_id    =t205d.c205d_attribute_value
		    AND T205D.C205D_VOID_FL         IS NULL
		    )
	  AND t501.c501_void_fl    IS NULL
	  AND t501.c501_delete_fl  IS NULL
	  AND t501.c1900_company_id = v_company_id
	  AND t502.c502_void_fl    IS NULL
	  AND t502.c502_delete_fl  IS NULL
 GROUP BY t502.c205_part_number_id,T205.C205_PART_NUM_DESC ) THREEMONSALES, T205_PART_NUMBER T205 -- 70814
    WHERE threemonsales.sales   > 0
      AND T205.C205_PART_NUMBER_ID = threemonsales.pnum
GROUP BY threemonsales.pnum,threemonsales.item_qty,T205.c205_part_num_desc;

BEGIN

FOR V_THREE_MONTH_SALE_PARTS IN THREE_MONTH_SALE_PARTS

LOOP

		IF(v_three_month_sale_parts.three_month_sale_qty<=10) THEN
		
		v_safety_stock_count:=v_three_month_sale_parts.three_month_sale_qty;
			
			
		ELSE
		v_safety_stock_count:=CEIL(v_three_month_sale_parts.three_month_sale_qty/3);
		
		END IF;


 	 UPDATE t4026_cut_plan_lock_detail
 	  SET 
 	  c4026_one_month_avg_sale_qty= v_three_month_sale_parts.one_month_avg_sale_qty,
  	  c4026_three_months_sale_qty =v_three_month_sale_parts.three_month_sale_qty,
  	  C4026_SAFETY_STOCK=v_safety_stock_count
 	 WHERE c205_part_number_id=v_three_month_sale_parts.pnum
 	 AND C4025_CUT_PLAN_LOCK_ID=v_cut_plan_master_id;
  
            IF (SQL%ROWCOUNT            = 0) THEN
            
            INSERT
               INTO t4026_cut_plan_lock_detail
                (
                    c4025_cut_plan_lock_id,c205_part_number_id,c4026_one_month_avg_sale_qty,c4026_three_months_sale_qty,c4026_safety_stock)
            VALUES (
            v_cut_plan_master_id,v_three_month_sale_parts.pnum,v_three_month_sale_parts.one_month_avg_sale_qty,v_three_month_sale_parts.three_month_sale_qty,v_safety_stock_count
            );
        END IF;
  
END LOOP;

END gm_sav_sav_3_month_sale_qty;

/*******************************************************
  * Author  : gpalani
  * PMT-55067
  * Description:  Procedure to save the project category
  *******************************************************/
  
PROCEDURE gm_sav_project_category(
p_cut_plan_master_id IN t4025_cut_plan_lock.c4025_cut_plan_lock_id%type
)

AS

 CURSOR project_cat
 IS
	 SELECT t205.c202_project_id project_id, t4026.c205_part_number_id part_num
		 FROM t4026_cut_plan_lock_detail t4026, t205_part_number t205
	  	 WHERE t4026.c4025_cut_plan_lock_id =p_cut_plan_master_id
         AND t4026.c205_part_number_id = t205.c205_part_number_id; 

BEGIN

 FOR  v_project_cat IN project_cat
 
 LOOP
 
 UPDATE t4026_cut_plan_lock_detail
  SET 
  c202_project_category=v_project_cat.project_id
 WHERE c205_part_number_id=v_project_cat.part_num
  AND c4025_cut_plan_lock_id=p_cut_plan_master_id;

 END LOOP;  
 
 
END gm_sav_project_category;
/*******************************************************
  * Author  : gpalani
  * PMT-55067
  * Description:  procedure to update the part numbers in temp table
*******************************************************/
PROCEDURE gm_update_temp_table(
p_cut_plan_master_id IN t4025_cut_plan_lock.c4025_cut_plan_lock_id%type
)
  
  AS
  
  v_sample_part_count NUMBER;
  
    CURSOR tmp_cut_plan_temp_table_insert
    IS
        SELECT c205_part_number_id part_num FROM t4026_cut_plan_lock_detail
    UNION ALL
    
    SELECT t205d.c205_part_number_id  part_num
         FROM t205d_part_attribute t205d, t4026_cut_plan_lock_detail t4026
         WHERE t205d.c901_attribute_type    = 103730
         AND t205d.c205d_attribute_value=t4026.c205_part_number_id
         AND t205d.c205d_attribute_value IS NOT NULL
         AND t4026.c4025_cut_plan_lock_id=p_cut_plan_master_id
         AND t205d.c205d_attribute_value <> 'N/A'        
         AND t205d.c205d_void_fl  IS NULL;
    
              
    BEGIN  
         DELETE FROM my_temp_part_list;     
         FOR    v_tmp_cut_plan in tmp_cut_plan_temp_table_insert
         
         LOOP
        -- Checking and removing the Sample Part number from the temp table insert
         
         SELECT COUNT (1)
  			 INTO v_sample_part_count
   		 FROM t205d_part_attribute t205d
         WHERE t205d.c901_attribute_type = '103745'
         AND 
         (t205d.c205_part_number_id  = v_tmp_cut_plan.part_num
         OR t205d.c205d_attribute_value  = v_tmp_cut_plan.part_num
         )
         AND t205d.c205d_attribute_value = 'Y';
         
         -- If the count above query is greater than 0 we are skipping and continuing to the next loop.
         IF(v_sample_part_count>0) THEN
         
         CONTINUE;
         
         END IF;
          
           UPDATE my_temp_part_list
            SET c205_part_number_id=v_tmp_cut_plan.part_num
              WHERE c205_part_number_id = v_tmp_cut_plan.part_num;
              
            IF (SQL%ROWCOUNT            = 0) THEN
                 INSERT
                   INTO my_temp_part_list
                    (
                        c205_part_number_id
                    )
                    VALUES
                    (
                        v_tmp_cut_plan.part_num
                    ) ;
            END IF;
            -- Reset the value
            v_sample_part_count:='0';
        END LOOP;  
    END gm_update_temp_table; 
    

/*******************************************************
  * Author  : gpalani
  * PMT-55067
  * Description:  procedure to update the part numbers in temp table
**********************************/
PROCEDURE gm_update_inventory_data(
p_cut_plan_master_id IN t4025_cut_plan_lock.c4025_cut_plan_lock_id%type
)
AS
  v_company_id  T1900_COMPANY.C1900_COMPANY_ID%TYPE;
  v_plant_id T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
CURSOR TMP_CUT_PLAN_PTRD_QTY
IS
  SELECT  t413.c205_part_number_id  pnum, NVL(SUM (t413.c413_item_qty),0)redegn_qty
    FROM t412_inhouse_transactions t412, t413_inhouse_trans_items t413,my_temp_part_list tmp_cut
   WHERE c412_type  = '103932' -- 103932: ptrd transaction type
     AND c412_void_fl IS NULL
     AND t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
     AND t413.c205_part_number_id=tmp_cut.c205_part_number_id
     AND c412_verify_fl     is null
     AND c412_status_fl     <> 4
     AND t412.c412_update_inv_fl is null
     AND t412.c1900_company_id    = v_company_id
     AND t412.c5040_plant_id      = v_plant_id
GROUP BY t413.c205_part_number_id;

 
CURSOR INV_QTY
  IS
   SELECT v205.c205_part_number_id pnum, NVL(SUM(c205_repackavail), 0) re_pack_qty, NVL(SUM(c205_inshelf), 0) shelf_qty
   , SUM(c205_dhrqty) dhr_qty
    FROM v205_qty_in_stock v205
    GROUP BY v205.c205_part_number_id;
    
    
CURSOR INV_QTY_PRIVATE_PART
  IS
  
  SELECT NVL (t205d.c205d_attribute_value, v205.c205_part_number_id) pnum, NVL (SUM (c205_repackavail), 0) re_pack_qty,
    NVL (SUM (c205_inshelf), 0) shelf_qty, SUM (c205_dhrqty) dhr_qty
   FROM v205_qty_in_stock v205, t205d_part_attribute t205d , my_temp_part_list tmp
  WHERE NVL (t205d.c901_attribute_type, '-9999')    = NVL (103730, - '9999')
    AND NVL (t205d.c205d_attribute_value, '-9999') IS NOT NULL
    AND tmp.c205_part_number_id=t205d.c205d_attribute_value
    AND v205.c205_part_number_id= t205d.c205_part_number_id (+)
    AND NVL (t205d.c205d_attribute_value, '-9999') <> 'N/A'
    AND T205D.C205D_VOID_FL (+)                    IS NULL
GROUP BY NVL (t205d.c205d_attribute_value, v205.c205_part_number_id) ;
     
BEGIN
   SELECT get_compid_frm_cntx(),get_plantid_frm_cntx() INTO v_company_id,v_plant_id FROM dual;
   
      my_context.set_my_inlist_ctx ('70018,70002,70003,70019,70005,70007,70023,70006,70008,70004,70014,70022,70009,70013,70020,70001') ;
    
  
FOR v_ptrd_qty IN tmp_cut_plan_ptrd_qty

LOOP

  UPDATE t4026_cut_plan_lock_detail set 
   c4026_ptrd_qty=c4026_ptrd_qty+NVL(v_ptrd_qty.redegn_qty,0)
  WHERE c205_part_number_id=v_ptrd_qty.pnum
  AND c4025_cut_plan_lock_id=p_cut_plan_master_id;
  
END LOOP;

FOR v_inv_qty IN inv_qty

LOOP
  UPDATE t4026_cut_plan_lock_detail set
   c4026_dhr_qty=c4026_dhr_qty+v_inv_qty.dhr_qty,
   c4026_shelf_qty=c4026_shelf_qty+v_inv_qty.shelf_qty,
   c4026_re_pack_qty=c4026_re_pack_qty+v_inv_qty.re_pack_qty
  WHERE c205_part_number_id=v_inv_qty.pnum
  AND c4025_cut_plan_lock_id=p_cut_plan_master_id;
  
END LOOP;

for v_inv_qty_private_part IN inv_qty_private_part

LOOP

  UPDATE t4026_cut_plan_lock_detail set 
   c4026_dhr_qty=c4026_dhr_qty+v_inv_qty_private_part.dhr_qty,
   c4026_shelf_qty=c4026_shelf_qty+v_inv_qty_private_part.shelf_qty,
   c4026_re_pack_qty=c4026_re_pack_qty+v_inv_qty_private_part.re_pack_qty
  WHERE c205_part_number_id=v_inv_qty_private_part.pnum
  AND c4025_cut_plan_lock_id=p_cut_plan_master_id;
  
END LOOP;

END gm_update_inventory_data;


/*************************************************************************
  * Author  : gpalani
  * PMT-55067
  * Description:  procedure to find the final Need Qty for the part numbers 
***************************************************************************/
PROCEDURE gm_update_thb_inventory_data(
p_cut_plan_master_id IN t4025_cut_plan_lock.c4025_cut_plan_lock_id%type
)
AS
    
CURSOR thb_inv_qty_lyo
  IS
           
  SELECT t2562.c205_part_number_id part_num, t2562.c2562_qty qty	
   FROM t2562_thb_wip_inventory_status T2562, t4026_cut_plan_lock_detail t4026
  WHERE t2562.c901_status ='109760' -- Fetching the Status ID Lyo_Run and Pending Rad_Run
  AND t2562.c2562_void_fl IS NULL
  AND t4026.c4025_cut_plan_lock_id=p_cut_plan_master_id
  AND t4026.c205_part_number_id=t2562.c205_part_number_id
  AND t2562.c2562_void_fl IS NULL;

CURSOR THB_INV_QTY_Rad_Run
  IS
           
  SELECT t2562.c205_part_number_id part_num, t2562.c2562_qty qty 
   FROM t2562_thb_wip_inventory_status T2562, t4026_cut_plan_lock_detail t4026
  WHERE t2562.c901_status ='109761' -- Fetching the Status ID Lyo_Run and Pending Rad_Run
  AND t2562.c2562_void_fl IS NULL
  AND t4026.c4025_cut_plan_lock_id=p_cut_plan_master_id
  AND t4026.c205_part_number_id=t2562.c205_part_number_id
  AND t2562.c2562_void_fl IS NULL;
  
BEGIN

FOR v_thb_inv IN THB_INV_QTY_lyo

LOOP

	UPDATE t4026_cut_plan_lock_detail
	   SET c4026_thb_lyo_pending=v_thb_inv.qty
	WHERE  c4025_cut_plan_lock_id=p_cut_plan_master_id
      AND c205_part_number_id=v_thb_inv.part_num;

END LOOP;

FOR v_thb_inv_rad_run IN THB_INV_QTY_Rad_Run

LOOP

	UPDATE t4026_cut_plan_lock_detail
	   SET c4026_thb_rad_run=v_thb_inv_rad_run.qty
	WHERE  c4025_cut_plan_lock_id=p_cut_plan_master_id
     AND c205_part_number_id=v_thb_inv_rad_run.part_num;

END LOOP;

END gm_update_thb_inventory_data;


/*************************************************************************
  * Author  : gpalani
  * PMT-55067
  * Description:  procedure to find the final Need Qty for the part numbers 
***************************************************************************/
PROCEDURE gm_update_thb_released_data(
p_cut_plan_master_id IN t4025_cut_plan_lock.c4025_cut_plan_lock_id%type
)
AS

CURSOR thb_week1_released_date
IS
 SELECT t2563.c205_part_number_id part_num, t2563.C2563_RECEIVED_COUNT qty
   FROM t2563_thb_weekly_capacity t2563,  t4026_cut_plan_lock_detail t4026
  WHERE t2563.c2563_received_week_of >= current_date - 7
  AND t2563.c205_part_number_id=t4026.c205_part_number_id
  AND t4026.c4025_cut_plan_lock_id=p_cut_plan_master_id;
  
  
 CURSOR thb_week2_released_date
 IS
 SELECT t2563.c205_part_number_id part_num, t2563.c2563_received_count qty
   FROM t2563_thb_weekly_capacity t2563,  t4026_cut_plan_lock_detail t4026
  WHERE t2563.C2563_RECEIVED_WEEK_OF >= CURRENT_DATE - 14
  AND t2563.C2563_RECEIVED_WEEK_OF <= CURRENT_DATE - 7
  AND t4026.c4025_cut_plan_lock_id=p_cut_plan_master_id
  AND t4026.c205_part_number_id=t2563.c205_part_number_id;
   
    
BEGIN

	FOR v_thb_week1_date IN thb_week1_released_date

	LOOP

	UPDATE t4026_cut_plan_lock_detail
	   SET c4026_thb_week1_released_qty=v_thb_week1_date.qty
	WHERE  c4025_cut_plan_lock_id=p_cut_plan_master_id
     AND c205_part_number_id=v_thb_week1_date.part_num;

	END LOOP;



	FOR v_thb_week2_date IN thb_week2_released_date

	LOOP

	UPDATE t4026_cut_plan_lock_detail
	   SET c4026_thb_week2_released_qty=v_thb_week2_date.qty
	WHERE  c4025_cut_plan_lock_id=p_cut_plan_master_id
     AND c205_part_number_id=v_thb_week2_date.part_num;

	END LOOP;

END gm_update_thb_released_data;
  

/*************************************************************************
  * Author  : gpalani
  * PMT-55067
  * Description:  procedure to find the final Need Qty for the part numbers 
***************************************************************************/
PROCEDURE gm_find_need_qty(
p_cut_plan_master_id IN t4025_cut_plan_lock.c4025_cut_plan_lock_id%type
)

AS
V_NEED_QTY t4026_cut_plan_lock_detail.C4026_NEED_QTY%TYPE :='0';
v_back_order_qty t4026_cut_plan_lock_detail.C4026_BACK_ORDER_QTY%TYPE;
v_less_than_3_weeks t4026_cut_plan_lock_detail.C4026_LESS_THAN_3_WEEKS%TYPE;
v_greater_than_3_weeks t4026_cut_plan_lock_detail.C4026_GREATER_THAN_3_WEEKS%TYPE;
v_three_month_avg_sale_qty t4026_cut_plan_lock_detail.C4026_THREE_MONTHS_SALE_QTY%TYPE;
v_safety_stock t4026_cut_plan_lock_detail.C4026_SAFETY_STOCK%TYPE;
v_re_pack_qty t4026_cut_plan_lock_detail.C4026_RE_PACK_QTY%TYPE;
v_shelf_qty t4026_cut_plan_lock_detail.C4026_SHELF_QTY%TYPE;
v_dhr_qty t4026_cut_plan_lock_detail.C4026_DHR_QTY%TYPE;
v_ptrd_qty t4026_cut_plan_lock_detail.C4026_PTRD_QTY%TYPE;
v_lyo_pending t4026_cut_plan_lock_detail.C4026_THB_LYO_PENDING%TYPE;
v_rad_run t4026_cut_plan_lock_detail.C4026_THB_RAD_RUN%TYPE;
v_future_need_qty t4026_cut_plan_lock_detail.c4026_future_need_qty%TYPE;
 
CURSOR TMP_CUT_PLAN_NEED_QTY
IS
 select c205_part_number_id part_num, c4026_back_order_qty back_ord_qty, c4026_greater_than_3_weeks greater_than_3_weeks
  , c4026_less_than_3_weeks less_than_3_weeks, c4026_three_months_sale_qty three_month_avg_sale_qty, c4026_ptrd_qty ptrd_qty
  , c4026_re_pack_qty re_pack_qty, c4026_shelf_qty shelf_qty, c4026_dhr_qty dhr_qty,c4026_safety_stock safety_stock, C4026_THB_Lyo_pending lyo_pending,
  c4026_thb_rad_run rad_run,NVL(c4026_future_need_qty,0) future_need
   from t4026_cut_plan_lock_detail
   WHERE c4025_cut_plan_lock_id=p_cut_plan_master_id;
   
BEGIN

FOR v_tmp_cut_plan_need_qty IN tmp_cut_plan_need_qty

LOOP
v_back_order_qty:=v_tmp_cut_plan_need_qty.back_ord_qty;
v_less_than_3_weeks:=v_tmp_cut_plan_need_qty.less_than_3_weeks;
v_greater_than_3_weeks:=v_tmp_cut_plan_need_qty.greater_than_3_weeks;
v_three_month_avg_sale_qty:=v_tmp_cut_plan_need_qty.three_month_avg_sale_qty;
v_safety_stock:=v_tmp_cut_plan_need_qty.safety_stock;
v_re_pack_qty:=v_tmp_cut_plan_need_qty.re_pack_qty;
v_shelf_qty:=v_tmp_cut_plan_need_qty.shelf_qty;
v_dhr_qty:=v_tmp_cut_plan_need_qty.dhr_qty;
v_ptrd_qty:=v_tmp_cut_plan_need_qty.ptrd_qty;
v_lyo_pending:=v_tmp_cut_plan_need_qty.lyo_pending;
v_rad_run:=v_tmp_cut_plan_need_qty.rad_run;
v_future_need_qty :=v_tmp_cut_plan_need_qty.future_need;


IF (v_back_order_qty + v_less_than_3_weeks + v_greater_than_3_weeks+v_three_month_avg_sale_qty+v_safety_stock+v_future_need_qty) 
-  (v_re_pack_qty + v_shelf_qty + v_dhr_qty + v_ptrd_qty+v_lyo_pending+v_rad_run) <= 0 THEN 


 
  v_need_qty:=0;
 
 ELSE
 
  v_need_qty:= (v_back_order_qty + v_less_than_3_weeks + v_greater_than_3_weeks+v_three_month_avg_sale_qty+v_safety_stock+v_future_need_qty) 
              -(v_re_pack_qty + v_shelf_qty + v_dhr_qty + v_ptrd_qty+v_lyo_pending+v_rad_run);
    
END IF;

  UPDATE t4026_cut_plan_lock_detail set c4026_need_qty=v_need_qty
  WHERE c205_part_number_id=v_tmp_cut_plan_need_qty.part_num
  AND c4025_cut_plan_lock_id=p_cut_plan_master_id;


  v_need_qty:='0';
  
END LOOP;

END gm_find_need_qty;


/*************************************************************************
  * Author  : gpalani
  * PMT-55067
  * Description:  procedure to find the  weeks qty to be produced.  
***************************************************************************/
PROCEDURE gm_update_week_qty(
p_cut_plan_master_id IN t4025_cut_plan_lock.c4025_cut_plan_lock_id%type
)

AS

v_week1_qty t4026_cut_plan_lock_detail.C4026_WEEK1%TYPE :='0';
v_week2_qty t4026_cut_plan_lock_detail.C4026_WEEK2%TYPE :='0';
v_week3_qty t4026_cut_plan_lock_detail.C4026_WEEK3%TYPE :='0';
v_week4_qty t4026_cut_plan_lock_detail.C4026_WEEK4%TYPE :='0';
v_temp_inv_qty NUMBER:='0';


	
	CURSOR TMP_CUT_PLAN_WEEK
	IS
	 SELECT DISTINCT c205_part_number_id part_num,  c4026_need_qty total_need
	   FROM t4026_cut_plan_lock_detail
	   WHERE C4025_CUT_PLAN_LOCK_ID=p_cut_plan_master_id;
	   
	BEGIN
	
	FOR v_tmp_cut_plan_week IN tmp_cut_plan_week
	
	LOOP
	
	IF(v_tmp_cut_plan_week.total_need<=10) THEN
	
		v_week1_qty:=v_tmp_cut_plan_week.total_need;
		v_week2_qty:=0;
		v_week3_qty:=0;
		v_week4_qty:=0;
	
	
	ELSE
		v_week1_qty:=CEIL(v_tmp_cut_plan_week.total_need/4);
		v_week2_qty:=v_week1_qty;
		v_week3_qty:=v_week1_qty;
		v_week4_qty:=v_week1_qty;
	
	END IF;
	


	  UPDATE t4026_cut_plan_lock_detail set 
	   c4026_week1=v_week1_qty, 
	   c4026_week2=v_week2_qty, 
	   c4026_week3=v_week3_qty,
	   c4026_week4=v_week4_qty
	  WHERE c205_part_number_id=v_tmp_cut_plan_week.part_num
	  AND c4025_cut_plan_lock_id=p_cut_plan_master_id;
	
	-- Resetting the values for the next loop.
		v_week1_qty:='0';  
		v_week2_qty:='0';
		v_week3_qty:='0';
		v_week4_qty:='0';
	END LOOP;
	
	
END gm_update_week_qty;

END gm_pkg_op_cut_plan_forecast;
/