CREATE OR REPLACE PACKAGE BODY gm_pkg_cutplan_txn IS 

/*************************************************************************
  * Description : This procedure is used to send the cutplan summary details mail
 *  **************************************************************************/

    PROCEDURE gm_cutplan_summary_send_mail (
        p_cutplan_lock_id   IN   t4026_cut_plan_lock_detail.c4025_cut_plan_lock_id%type,
        p_additional_note   IN   VARCHAR2
    ) AS

        v_email_to      t906_rules.c906_rule_value%TYPE;
        v_email_msg     VARCHAR2(4000);
        v_email_sub     t906_rules.c906_rule_value%TYPE;
        v_str           CLOB;
        v_mail_body     CLOB;
        v_col_str       VARCHAR2(1000);
        v_end_row_str   VARCHAR2(1000);
        v_table_str     CLOB;
        v_add_notes     VARCHAR2(4000);
        
        CURSOR fetch_summary_dtls IS
        SELECT
            c205_part_number_id   partnum,
            regexp_replace(get_partnum_desc(c205_part_number_id), '[^0-9A-Za-z]', ' ') partdesc,
            c4026_need_qty        needqty,
            c4026_week1           wk1,
            c4026_week2           wk2,
            c4026_week3           wk3,
            c4026_week4           wk4,
            c4026_comments        comments,
            c4026_cut_plan_lock_detail_id
        FROM
            globus_app.t4026_cut_plan_lock_detail
        WHERE
            c4025_cut_plan_lock_id = p_cutplan_lock_id
            AND c4026_comments IS NOT NULL
            AND c4026_void_fl IS NULL;

    BEGIN
       -- my_context.set_my_cloblist(p_cutplan_dtls_id);
       
        FOR v_fetch_summarydrls IN fetch_summary_dtls LOOP
			  
          -- below string is the ACK order Required date  details
         v_str := v_str || '<TR ><TD style="border: 1px solid  #676767;background-color:#AEAAAA;">'|| v_fetch_summarydrls.partnum
                        || '</TD><TD style="border: 1px solid  #676767;background-color:#AEAAAA;">'|| v_fetch_summarydrls.partdesc
                        || '</TD><TD style="border: 1px solid  #676767;background-color:greenyellow;text-align:right;">'|| v_fetch_summarydrls.needqty
                        || '</TD><TD style="border: 1px solid  #676767;background-color:#FFF2CC;text-align:right;">'|| v_fetch_summarydrls.wk1
                        || '</TD><TD style="border: 1px solid  #676767;background-color:#FFE699;text-align:right;">'|| v_fetch_summarydrls.wk2
                        || '</TD><TD style="border: 1px solid  #676767;background-color:#FFF2CC;text-align:right;">'|| v_fetch_summarydrls.wk3
                        || '</TD><TD style="border: 1px solid  #676767;background-color:#FFE699;text-align:right;">'|| v_fetch_summarydrls.wk4
                        || '</TD><TD style="border: 1px solid  #676767;background-color:#DDEBF7;">'|| v_fetch_summarydrls.comments
                        || '</TD></TR>';
        END LOOP;

        SELECT
            get_rule_value('CUT_PLAN_ANA_SUB', 'CUT_PLAN_ANALY_EMAIL')
        INTO v_email_sub
        FROM
            dual;

        SELECT
            get_rule_value('CUT_PLAN_ANA_EMAIL_TO', 'CUT_PLAN_ANALY_EMAIL')
        INTO v_email_to
        FROM
            dual;

        IF ( p_additional_note = 'undefined' OR p_additional_note = 'null' ) THEN
            v_add_notes := '';
        ELSE
            v_add_notes := p_additional_note;
        END IF;

        v_email_msg := '<b>Additional Notes :</b><br>' || v_add_notes;
        v_table_str := '<TABLE style="border: 1px solid  #676767; border-collapse: collapse;">'
                       || '<TR><TD width=150 style="border: 1px solid  #676767;color:#0286c1; text-align: center;"><b>Part Number</b></TD><TD width=250 style="border: 1px solid  #676767;color:#0286c1; text-align: center;"><b>Part Desc</b></TD><TD width=150 style="border: 1px solid  #676767;color:#0286c1; text-align: center;"><b>Need Qty</b></TD>'
                       || '<TD width=150 style="border: 1px solid  #676767;color:#0286c1; text-align: center;"><b>Wk1 Qty</b></TD><TD width=150 style="border: 1px solid  #676767;color:#0286c1; text-align: center;"><b>Wk2 Qty</b></TD><TD width=150 style="border: 1px solid  #676767;color:#0286c1; text-align: center;"><b>Wk3 Qty</b></TD>'
                       || '<TD width=150 style="border: 1px solid  #676767;color:#0286c1; text-align: center;"><b>Wk4 Qty</b></TD><TD width=150 style="border: 1px solid  #676767;color:#0286c1; text-align: center;"><b>Notes</b></TD></TR>'
                       ;
        v_mail_body := '<style>TD{ FONT-SIZE: 11px; COLOR: #000000; FONT-FAMILY: verdana, arial, sans-serif;}</style><font face=arial size="2"></font>';
        v_mail_body := v_mail_body
                       || '<br>'
                       || v_table_str
                       || v_str
                       || '</TABLE> <br><br>'
                       || v_email_msg;
           
          -- call the email procedure to send email to the user
           v_email_sub := v_email_sub ||' - ' || TO_CHAR(CURRENT_DATE ,'mm/dd/yyyy' ||' HH:MI:SS AM' ) ;
        gm_com_send_html_email(v_email_to, v_email_sub, NULL, v_mail_body);
    END gm_cutplan_summary_send_mail;
/*************************************************************************
 * Description : This procedure is used to Update future need qty and week qty
***************************************************************************/
PROCEDURE gm_update_future_need_qty(
 p_cutplan_lock_dtl_id  IN T4026_CUT_PLAN_LOCK_DETAIL.c4026_cut_plan_lock_detail_id%type,
 p_cutplan_lock_id      IN t4025_cut_plan_lock.c4025_cut_plan_lock_id%type,
 p_future_need          IN T4026_CUT_PLAN_LOCK_DETAIL.c4026_future_need_qty%type,
 p_user_id              IN t101_user.c101_user_id%TYPE
)
AS
BEGIN

	UPDATE T4026_CUT_PLAN_LOCK_DETAIL
	   SET C4026_FUTURE_NEED_QTY = p_future_need,
	       C4026_LAST_UPDATED_DATE = current_date,
	       c4026_last_updated_by = p_user_id
	 WHERE c4026_cut_plan_lock_detail_id = p_cutplan_lock_dtl_id
	   AND c4026_void_fl is null;
	   
--  Find Need Qty for each part
    gm_pkg_op_cut_plan_forecast.gm_find_need_qty(p_cutplan_lock_id);

--  Week 1 qty for each part
    gm_pkg_op_cut_plan_forecast.gm_update_week_qty(p_cutplan_lock_id);

commit;
END gm_update_future_need_qty;   
/*************************************************************************
 * Description : This procedure is used to Update the status in cut plan lock table
***************************************************************************/
PROCEDURE gm_upd_cutplan_status(
 p_cutplan_lock_id IN t4025_cut_plan_lock.c4025_cut_plan_lock_id%type,
 p_lock_fl         IN t4025_cut_plan_lock.c4025_lock_fl%type,
 p_user_id         IN t101_user.c101_user_id%TYPE
 )
 AS
 BEGIN
	 UPDATE t4025_cut_plan_lock
	   SET c4025_lock_fl             = p_lock_fl,
	       c4025_last_updated_date = current_date,
	       c4025_last_updated_by   = p_user_id
	 WHERE c4025_cut_plan_lock_id = p_cutplan_lock_id
	   AND c4025_void_fl is null;
END gm_upd_cutplan_status;
/*************************************************************************
 * Description : This procedure is used to Update future need qty and week qty
***************************************************************************/
PROCEDURE gm_sav_new_part_num(
 p_cutplan_lock_id  IN t4025_cut_plan_lock.c4025_cut_plan_lock_id%type,
 p_part_num         IN t4026_cut_plan_lock_detail.c205_part_number_id%type,
 p_future_need      IN t4026_cut_plan_lock_detail.c4026_future_need_qty%type,
 p_company_id       IN T1900_COMPANY.C1900_COMPANY_ID%TYPE,
 p_plant_id         IN T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE,
 p_user_id          IN t101_user.c101_user_id%TYPE,
 p_invalid_part    OUT VARCHAR2
)
AS
v_project_id   T202_project.c202_project_id%TYPE;
v_invalid_partstr    VARCHAR2(3000);
v_count        NUMBER;
v_back_ord_qty NUMBER:='0';
v_less_than_3_weeks NUMBER:='0';
v_greater_than_3_weeks NUMBER:='0';
v_email_to 			t906_rules.c906_rule_value%TYPE;
v_email_msg 			VARCHAR2 (2000);
v_email_sub 			VARCHAR2 (2000);
 nRowsUpdated NUMBER;
BEGIN
	 DELETE FROM my_temp_part_list;   
	 
	SELECT get_project_id_from_part(p_part_num)
		  INTO v_project_id
		  FROM DUAL;
		-- We can allow the part, If the part is RFS for that company only.
		SELECT count(1)
		  INTO v_count
		  FROM t205_part_number t205, t2023_part_company_mapping t2023
		 WHERE t2023.c205_part_number_id = p_part_num
		   AND t205.c205_part_number_id = t2023.c205_part_number_id
		   AND t2023.c1900_company_id = p_company_id
		   AND t2023.c2023_void_fl IS NULL;

	IF v_project_id = ' ' OR v_count = '0'
		THEN
			 v_invalid_partstr := 'Please Enter Valid Part Number';     
		ELSE
		   
		BEGIN
         ----  Fetch BACK_ORD_QTY, Due in 3 weeks, Due after 3 weeks data  
        UPDATE t4026_cut_plan_lock_detail
            SET c4026_back_order_qty   = c4026_back_order_qty + v_back_ord_qty, 
            c4026_less_than_3_weeks    = c4026_less_than_3_weeks+v_less_than_3_weeks, 
            c4026_greater_than_3_weeks = c4026_greater_than_3_weeks+v_greater_than_3_weeks
        WHERE c205_part_number_id = p_part_num
        AND c4025_cut_plan_lock_id=p_cutplan_lock_id
        AND c4026_void_fl IS NULL;
        	
   		nRowsUpdated := sql%rowcount;
   
           IF (nRowsUpdated= 0) THEN

             INSERT INTO t4026_cut_plan_lock_detail
                (
                    c4025_cut_plan_lock_id,c205_part_number_id, c4026_back_order_qty,c4026_less_than_3_weeks,c4026_greater_than_3_weeks,
                     c4026_future_need_qty,c4026_last_updated_by,c4026_last_updated_date
                )
                VALUES
                (
                    p_cutplan_lock_id,p_part_num, v_back_ord_qty,v_less_than_3_weeks,v_greater_than_3_weeks,p_future_need,p_user_id,current_date
                ) ;
                
           END IF;
          END;
            v_back_ord_qty:=0;
            v_less_than_3_weeks:=0;
            v_greater_than_3_weeks:=0;
         
       BEGIN 
       
         UPDATE my_temp_part_list
            SET c205_part_number_id     = p_part_num
          WHERE c205_part_number_id = p_part_num;
          
        IF (SQL%ROWCOUNT            = 0) THEN
             INSERT
               INTO my_temp_part_list
                (
                    c205_part_number_id
                )
                VALUES
                (
                    p_part_num
                ) ;
        END IF;
      END; 
 -- Company and Plant id is hard coded since the Primary table (t2560_thb_load) is not having the company id stored.
    gm_pkg_cor_client_context.gm_sav_client_context (p_company_id, p_plant_id) ;
--  Update Project Category for the part number   
    gm_pkg_op_cut_plan_forecast.gm_sav_project_category(p_cutplan_lock_id);
    --  Insert data in temp table
    gm_pkg_op_cut_plan_forecast.gm_update_temp_table(p_cutplan_lock_id);

--  Update the inventory value for each part number
    gm_pkg_op_cut_plan_forecast.gm_update_inventory_data(p_cutplan_lock_id);
    
--  Update the THB WIP inventory value for each part number
    gm_pkg_op_cut_plan_forecast.gm_update_thb_inventory_data(p_cutplan_lock_id);   

--  Update the THB week released value for each part number    
    gm_pkg_op_cut_plan_forecast.gm_update_thb_released_data(p_cutplan_lock_id);

--  Find Need Qty for each part
    gm_pkg_op_cut_plan_forecast.gm_find_need_qty(p_cutplan_lock_id);

--  Week 1 qty for each part
    gm_pkg_op_cut_plan_forecast.gm_update_week_qty(p_cutplan_lock_id);
      
	END IF;	   
p_invalid_part := v_invalid_partstr;

SELECT get_rule_value('EMAIL_TO','CUT_PLAN_BBA') 
		    INTO v_email_to FROM DUAL;
		
		SELECT get_rule_value('EMAIL_SUCCESS_SUB','CUT_PLAN_PART_BBA') 
		    INTO v_email_sub FROM DUAL;
		            
		SELECT get_rule_value('EMAIL_SUCCESS_MSG','CUT_PLAN_PART_BBA') 
		    INTO v_email_msg FROM DUAL;
		
		v_email_msg:=v_email_msg ||'. <br>';
		
		-- Call the email procedure to send email to the user
	gm_com_send_html_email(v_email_to,v_email_sub,NULL,v_email_msg);
		
   EXCEPTION WHEN OTHERS
      THEN 
	  SELECT get_rule_value('EMAIL_TO','CUT_PLAN_BBA') 
		    INTO v_email_to FROM DUAL;
		
		SELECT get_rule_value('EMAIL_ERROR_SUB','CUT_PLAN_BBA') 
		    INTO v_email_sub FROM DUAL;
		            	
		SELECT get_rule_value('EMAIL_ERROR_MSG','CUT_PLAN_BBA') 
		    INTO v_email_msg FROM DUAL;
		
		v_email_msg:=v_email_msg ||'. <br>';
		
		-- Call the email procedure to send email to the user
		gm_com_send_html_email(v_email_to,v_email_sub,NULL,v_email_msg);

END gm_sav_new_part_num;
END gm_pkg_cutplan_txn;
/