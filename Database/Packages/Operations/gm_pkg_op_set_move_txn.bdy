--@"c:\database\packages\operations\gm_pkg_op_set_move_txn.bdy";
CREATE OR REPLACE
PACKAGE BODY gm_pkg_op_set_move_txn
IS

/****************************************************************
* Description : this procedure is used to save the Move Set Transaction
* Author      :
*****************************************************************/
PROCEDURE gm_sav_set_move (
        p_tag_id IN t5010_tag.c5010_tag_id%TYPE,
        p_loc_id IN t5052_location_master.c5052_location_id%TYPE,
        p_user_id  IN t101_user.c101_user_id%TYPE
        )
AS
  v_error_code NUMBER;
  v_txn_id VARCHAR2 (20) ;
  v_txn_type NUMBER;
  v_loc_id VARCHAR2 (20) ;
  v_out_param1 varchar2(20);
  v_out_param2 varchar2(20);
  v_loc_cd VARCHAR2 (20) ;
  --ship_today EXCEPTION;
  --PRAGMA EXCEPTION_INIT(Ship_Today,'- 20503');
BEGIN
  
  gm_pkg_ac_tag_info.gm_fch_tag_info(p_tag_id,v_loc_id, v_txn_id, v_txn_type) ; -- Get txn_id, txn_type and Location_id  
  
  --v_loc_cd := gm_pkg_op_inv_scan.GET_LOCATION_CODE(v_loc_id);
  
  BEGIN
    gm_pkg_op_set_validate.gm_validate_ship_date(v_txn_id,v_txn_type); -- Validate to day is ship date, If so custom error is thrown so flow shift to exception block
    
    gm_pkg_op_set_txn.gm_sav_tag_location(p_tag_id, v_loc_id, v_txn_id, v_txn_type, p_user_id,'PICK'); -- For Move Save Pick first
    gm_pkg_op_set_txn.gm_sav_tag_location(p_tag_id, p_loc_id, v_txn_id, v_txn_type, p_user_id,'PUT'); -- For Move Save PUT Next. Skip these two when shipping is today
     EXCEPTION WHEN OTHERS THEN -- Exception block is needed to continue the code 
       v_error_code :=SQLCODE;
       
       IF v_error_code =-20511 THEN
         GM_RAISE_APPLICATION_ERROR('-20999','280','');
       END IF;

       IF v_error_code =-20512 THEN
         GM_RAISE_APPLICATION_ERROR('-20999','281',''); 
       END IF;
   
       IF v_error_code =-20513 THEN
         GM_RAISE_APPLICATION_ERROR('-20999','282',''); 
       END IF;
      IF v_error_code =-20514 THEN
         GM_RAISE_APPLICATION_ERROR('-20999','283','');
       END IF;
       IF v_error_code =-20503 THEN -- -20503 =>Today is Ship Date
         GM_RAISE_APPLICATION_ERROR('-20999','284',''); 
       END IF;
  END;
      
END gm_sav_set_move ;

END gm_pkg_op_set_move_txn;
/