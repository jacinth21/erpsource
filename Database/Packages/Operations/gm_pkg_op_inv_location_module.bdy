/* Formatted on 2011/06/14 17:57 (Formatter Plus v4.8.0) */
--@"C:\Database\Packages\Operations\gm_pkg_op_inv_location_module.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_inv_location_module
IS
/* gm_sav_location_part_detail - This procedure will upate location and part mapping
   input: Deliminted String
*/
	PROCEDURE gm_fch_inv_loc_details (
		p_location_cd	IN		 t5052_location_master.c5052_location_id%TYPE   -- peramater is changed from p_locationstr to P_Location_cd for MNTTASK-7108
	 	, p_warehouseid IN 		 t5051_inv_warehouse.c5051_inv_warehouse_id%type
	 	, p_building_id IN       t5052_location_master.c5057_building_id%TYPE DEFAULT NULL  -- added for PMT-33507
		, p_out_cur		OUT 	 TYPES.cursor_type
	)
	AS
	v_plant_id     	t5040_plant_master.c5040_plant_id%TYPE;
	BEGIN
		
		SELECT  NVL(get_plantid_frm_cntx(),get_rule_value('DEFAULT_PLANT','ONEPORTAL')) INTO  v_plant_id FROM DUAL;
		OPEN p_out_cur
		 FOR
			 SELECT t5052.c5052_location_id ID, t5052.c901_location_type location_type, t5052.c901_status status ,t5052.C5051_INV_WAREHOUSE_ID WAREHOUSEID
				 , t5052.c5052_void_fl v_fl, get_rule_value(t5052.c901_zone,'ZONE_CATEGORY') ruleType, t5052.c5057_building_id buildid
			 FROM t5052_location_master t5052
			 WHERE NVL(t5052.c5052_location_cd,t5052.c5052_location_id) = p_location_cd AND  t5052.c5051_inv_warehouse_id = p_warehouseid  AND NVL(t5052.c5057_building_id,'-999') = NVL(p_building_id, NVL(t5052.c5057_building_id,'-999')) AND t5052.c5052_void_fl IS NULL
			 AND t5052.c5040_plant_id = v_plant_id;
			 
	END gm_fch_inv_loc_details;

	PROCEDURE gm_fch_inv_loc_part_mapping (
		  p_location_cd	IN		 t5052_location_master.c5052_location_id%TYPE  --- paramater is changed from p_locationstr to P_Location_cd for MNTTASK-7108
		, p_partnum		IN		 t205_part_number.c205_part_number_id%TYPE 
		, p_warehouseid IN 		 t5051_inv_warehouse.c5051_inv_warehouse_id%type
		, p_building_id IN       t5052_location_master.c5057_building_id%TYPE DEFAULT NULL  -- added for PMT-33507
		, p_out_cur		OUT 	 TYPES.cursor_type
	)
	AS
	   v_locationstr_id t5052_location_master.c5052_location_id%TYPE;
	   v_plant_id     	t5040_plant_master.c5040_plant_id%TYPE;
	BEGIN
		SELECT  NVL(get_plantid_frm_cntx(),get_rule_value('DEFAULT_PLANT','ONEPORTAL')) INTO  v_plant_id FROM DUAL;
		IF p_partnum IS  NULL
		THEN
			BEGIN
				SELECT C5052_LOCATION_ID 
				  INTO v_locationstr_id 
				  FROM T5052_LOCATION_MASTER t5052 
				 WHERE t5052.C5052_LOCATION_CD  = p_location_cd
				 AND t5052.c5051_inv_warehouse_id = p_warehouseid
				 AND NVL(t5052.c5057_building_id,'-999') = NVL(p_building_id, NVL(t5052.c5057_building_id,'-999'))  -- added for PMT-33507
				 AND t5052.C5052_VOID_FL is NULL
				 AND t5052.c5040_plant_id = v_plant_id;  
			EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				v_locationstr_id := p_location_cd;
			END;
		ELSE
			v_locationstr_id := p_location_cd;
		END IF;
		
		OPEN p_out_cur
		 FOR
			SELECT t5053.c5053_location_part_map_id ID
			     , NVL(t5052.c5052_location_cd,t5053.c5052_location_id) locationid
			     , t5053.c5052_location_id LOC_ID
				 , t5053.c205_part_number_id part
				 , t205.c205_part_num_desc part_desc
				 , DECODE (t5052.c901_status , 93310, t5053.c5053_curr_qty) orgqty
				 , DECODE (t5052.c901_status , 93310,gm_pkg_op_item_control_rpt.get_part_location_cur_qty (t5053.c205_part_number_id , t5053.c5052_location_id	), t5053.c5053_curr_qty) currqty
				 , DECODE (t5052.c901_status , 93310,(t5053.c5053_curr_qty - gm_pkg_op_item_control_rpt.get_part_location_cur_qty (t5053.c205_part_number_id , t5052.c5052_location_id)), 0) allocqty
				 , t5053.c5053_max_qty maxqty
				 , t5053.c5053_min_qty minqty
				 , get_code_name (t5052.c901_location_type) locationtype
				 , DECODE (t5052.c901_location_type, 93336, 0, (t5053.c5053_max_qty - t5053.c5053_curr_qty)) suggqty
			   FROM t5053_location_part_mapping t5053, t205_part_number t205, t5052_location_master t5052
			  WHERE t205.c205_part_number_id     = t5053.c205_part_number_id
				AND t5053.c5052_location_id      = t5052.c5052_location_id
				AND t5053.c205_part_number_id    = NVL (p_partnum, t5053.c205_part_number_id)
				AND t5052.c5052_location_id      = v_locationstr_id			-- paramater is changed from p_locationstr to P_Location_cd for MNTTASK-7108
				AND t5052.c5051_inv_warehouse_id = p_warehouseid
			    AND NVL(t5052.c5057_building_id,'-999') = NVL(p_building_id, NVL(t5052.c5057_building_id,'-999'))  -- added for PMT-33507
				AND t5052.c5040_plant_id		 = v_plant_id;
				
	END gm_fch_inv_loc_part_mapping;

	PROCEDURE gm_get_part_num_desc (
		p_partnumstr   IN		t205_part_number.c205_part_number_id%TYPE
	  , p_out_cur	   OUT		TYPES.cursor_type
	)
	AS
	
		v_company_id t1900_company.c1900_company_id%TYPE;
	
	BEGIN
		
		SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL')) 
    		INTO v_company_id
   			FROM DUAL;
   	--To fetch the part mapping type based on RFS.In this we are allowing the part which is having map type created or released.	
   	my_context.set_my_cloblist (get_rule_value('RFS','PART_MAP_TYPE')|| ',');
    
		OPEN p_out_cur
		 FOR
			 SELECT c205_part_num_desc part_desc
			   FROM t205_part_number t205, t2023_part_company_mapping t2023
			  WHERE t205.c205_part_number_id = p_partnumstr
			    AND t205.c205_part_number_id = t2023.c205_part_number_id 
				AND t2023.c2023_void_fl IS NULL
				AND t2023.c1900_company_id = v_company_id
				AND t2023.c901_part_mapping_type IN (select * from  v_clob_list)
			    AND t205.c205_active_fl = 'Y';
	END gm_get_part_num_desc;


	PROCEDURE gm_sav_inv_location (
		p_inputstr	 IN   VARCHAR2
	  , p_zonestr	 IN   t5052_location_master.c901_zone%TYPE
	  , p_aislestr	 IN   t5052_location_master.c5052_aisle%TYPE
	  , p_user_id	 IN   t5052_location_master.c5052_created_by%TYPE
	  , p_shelf 	 IN   t5052_location_master.c5052_shelf%TYPE
	  , p_status     IN   t5052_location_master.c901_status%TYPE
	  , p_type       IN   t5052_location_master.c901_location_type%TYPE
	  , p_warehouseid IN T5051_INV_WAREHOUSE.C5051_INV_WAREHOUSE_ID%TYPE
	  , p_building_id IN t5052_location_master.c5057_building_id%TYPE DEFAULT NULL  -- added for PMT-33507
	)
	AS
		v_inputstr	   VARCHAR2 (30000) := p_inputstr;
		v_string	   VARCHAR2 (30000) := p_inputstr;
		v_substring    VARCHAR2 (30000);
		v_count 	   NUMBER;
        v_plant_id     t5040_plant_master.c5040_plant_id%TYPE;
	BEGIN
		IF INSTR (p_inputstr, '$') = 0
		THEN
			v_inputstr	:= p_inputstr || '$';
		END IF;
	
	    SELECT  NVL(get_plantid_frm_cntx(),get_rule_value('DEFAULT_PLANT','ONEPORTAL')) 
			  INTO  v_plant_id
			  FROM DUAL;

		IF INSTR (v_inputstr, '$') <> 0
		THEN
			v_string	:= SUBSTR (v_inputstr, 1, INSTR (v_inputstr, '$') - 1);
			v_inputstr	:= SUBSTR (v_inputstr, INSTR (v_inputstr, '$') + 1);

			/* Loop the Delminted String and reterive the values */
			WHILE INSTR (v_string, '^') <> 0
			LOOP
				v_substring := SUBSTR (v_string, 1, INSTR (v_string, '^') - 1);
				v_string	:= SUBSTR (v_string, INSTR (v_string, '^') + 1);

				IF v_substring IS NOT NULL
				THEN
					SELECT COUNT (c5052_location_cd)
					 INTO v_count
					 FROM t5052_location_master
					 WHERE (c5052_location_cd = v_substring  or c5052_location_id = v_substring) 
					 AND c5051_inv_warehouse_id = p_warehouseid
					 AND c901_zone = p_zonestr
					 AND NVL(c5057_building_id,'-999') = NVL(p_building_id, NVL(c5057_building_id,'-999')) --added for PMT-33507
					 AND c5040_plant_id = v_plant_id; 
					 
					-- By default, the status is Intiated (93202)
					IF v_count = 0
					THEN
						INSERT INTO t5052_location_master
									(c5052_location_id, c5052_aisle, c901_status, c5052_created_by
								   , c5052_created_date, c901_zone, c5052_shelf, c901_location_type
								   ,c5051_inv_warehouse_id,c5052_location_cd,c5040_plant_id,c5057_building_id   -- added for PMT-33507
									)
							 VALUES (to_char(s5052_location_MASTER.nextval), p_aislestr, p_status, p_user_id
								   , CURRENT_DATE, p_zonestr, p_shelf, p_type 
								   ,p_warehouseid,v_substring,v_plant_id,p_building_id   -- added for PMT-33507
									);
									
					END IF;
				END IF;
			END LOOP;
		END IF;
	END gm_sav_inv_location;

	PROCEDURE gm_sav_inv_loc_part_details (
		p_inputstr	 IN   VARCHAR2
	  , p_user_id	 IN   t5052_location_master.c5052_created_by%TYPE
	)
	AS
		v_string	   VARCHAR2 (30000) := p_inputstr;
		v_substring    VARCHAR2 (30000);
		v_inputstr	   VARCHAR2 (30000) := p_inputstr;
		v_row_data	   VARCHAR2 (30000);
		v_location_id  VARCHAR2 (30000);
		v_type		   VARCHAR2 (30000);
		v_status	   VARCHAR2 (30000);
		v_part		   VARCHAR2 (30000);
		v_id		   VARCHAR2 (30000);
		v_min		   VARCHAR2 (30000) := 0;
		v_max		   VARCHAR2 (30000) := 0;

	BEGIN
		IF INSTR (p_inputstr, '$') = 0
		THEN
			v_inputstr	:= p_inputstr || '$';
		END IF;

		IF INSTR (v_inputstr, '$') <> 0
		THEN
			v_row_data	:= SUBSTR (v_inputstr, 1, INSTR (v_inputstr, '$') - 1);
			v_inputstr	:= SUBSTR (v_inputstr, INSTR (v_inputstr, '$') + 1);
			v_string	:= v_row_data;

			/* Loop the Delminted String and reterive the values */
			WHILE INSTR (v_string, '|') <> 0
			LOOP
				v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
				v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
				v_id		:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_location_id := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_type		:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_status	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_part		:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_min		:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_max		:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);

				IF v_location_id IS NOT NULL
				THEN
					IF v_id IS NULL
					THEN
						SELECT s5053_location_part_mapping.NEXTVAL
						  INTO v_id
						  FROM DUAL;

						INSERT INTO t5053_location_part_mapping
									(c5053_location_part_map_id, c5052_location_id, c205_part_number_id, c5053_max_qty
								   , c5053_min_qty, c5053_created_by, c5053_created_date
									)
							 VALUES (v_id, v_location_id, v_part, v_max
								   , v_min, p_user_id, CURRENT_DATE
									);
					ELSE
						UPDATE t5053_location_part_mapping
						   SET c205_part_number_id = v_part
							 , c5053_max_qty = v_max
							 , c5053_min_qty = v_min
							 , c5053_last_updated_by = p_user_id
							 , c5053_last_updated_date = CURRENT_DATE
							 , C5053_LAST_UPDATE_TRANS_ID = 'INV-ADJ'
							 , C901_LAST_TRANSACTION_TYPE = '4316'
						 WHERE c5053_location_part_map_id = v_id;
					END IF;

					
					-- Update T5052_LOCATION_MASTER Table
					IF v_type IS NOT NULL AND v_status IS NOT NULL
					THEN
						UPDATE t5052_location_master
						   SET c901_location_type = v_type
							 , c901_status = v_status
							 , c5052_last_updated_by = p_user_id
							 , c5052_last_updated_date = CURRENT_DATE
						 WHERE c5052_location_id = v_location_id;
					END IF;
				END IF;
			END LOOP;
		END IF;
	END gm_sav_inv_loc_part_details;

	PROCEDURE gm_sav_inv_loc_part_detail (
		p_inputstr		IN	 VARCHAR2
	  , p_locationstr	IN	 t5052_location_master.c5052_location_id%TYPE
	  , p_typestr		IN	 t5052_location_master.c901_location_type%TYPE
	  , p_statusstr 	IN	 t5052_location_master.c901_status%TYPE
	  , p_user_id		IN	 t5052_location_master.c5052_created_by%TYPE
	  , p_row_ids		IN	 VARCHAR2
	  , p_warehouseid   IN  T5051_INV_WAREHOUSE.C5051_INV_WAREHOUSE_ID%TYPE
	  , p_building_id   IN  t5052_location_master.c5057_building_id%TYPE   -- added for PMT-33507
	)  
	AS
		v_string	 VARCHAR2 (30000) := p_inputstr;
		v_substring VARCHAR2 (30000);
		v_inputstr	 VARCHAR2 (30000) := p_inputstr;
		v_row_data	 VARCHAR2 (30000);
		v_location_id VARCHAR2 (30000);
		v_type		 VARCHAR2 (30000);
		v_part		 VARCHAR2 (30000);
		v_part_desc	 VARCHAR2 (30000);
		v_id		 VARCHAR2 (30000);
		v_min		 VARCHAR2 (30000) := 0;
		v_max		 VARCHAR2 (30000) := 0;
		v_cnum		 VARCHAR2 (30000) := 0;
		v_cur_qty	 VARCHAR2 (30000) := 0;
		v_lcation_cd VARCHAR2(3000);
		v_cnt		   NUMBER := 0;	
	BEGIN
		my_context.set_my_inlist_ctx (p_row_ids);

		IF p_row_ids IS NOT NULL
		THEN
			DELETE FROM t5053_location_part_mapping
				  WHERE c5053_location_part_map_id IN (SELECT token
														 FROM v_in_list);
		END IF;

		IF INSTR (p_inputstr, '$') = 0
		THEN
			v_inputstr	:= p_inputstr || '$';
		END IF;

		IF INSTR (v_inputstr, '$') <> 0
		THEN
			v_row_data	:= SUBSTR (v_inputstr, 1, INSTR (v_inputstr, '$') - 1);
			v_inputstr	:= SUBSTR (v_inputstr, INSTR (v_inputstr, '$') + 1);
			v_string	:= v_row_data;

			/* Loop the Delminted String and reterive the values */
			WHILE INSTR (v_string, '|') <> 0
			LOOP
				v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
				v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
				v_id		:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_location_id := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_part		:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
			/*	
				v_part_desc := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				
				v_cnum      := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				*/
				v_cur_qty    := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);

				v_min		:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				
				v_max		:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				
				IF v_location_id IS NULL
				THEN
					v_location_id := p_locationstr; -- for inserting the  new Record based on the C5052_LOCATION_ID and not on C5052_LOCATION_CD
				END IF;

				IF v_part IS NOT NULL
				THEN
					IF v_id IS NULL
					THEN
						SELECT s5053_location_part_mapping.NEXTVAL
						  INTO v_id
						  FROM DUAL;

						INSERT INTO t5053_location_part_mapping
									(c5053_location_part_map_id, c5052_location_id, c205_part_number_id, c5053_max_qty
								 , c5053_min_qty, c5053_created_by, c5053_created_date,C5053_CURR_QTY,c5053_last_update_trans_id,C901_LAST_TRANSACTION_TYPE
									)
							 VALUES (v_id, v_location_id, v_part, v_max
								 , v_min, p_user_id, CURRENT_DATE, v_cur_qty,'INV-ADJ','4316'
									);
					ELSE
						UPDATE t5053_location_part_mapping
						 SET c205_part_number_id = v_part
							 , c5053_max_qty = v_max
							 , c5053_min_qty = v_min
							 , C5053_CURR_QTY = v_cur_qty
							 , c5053_last_updated_by = p_user_id
							 , c5053_last_updated_date = CURRENT_DATE							 
							 , C5053_LAST_UPDATE_TRANS_ID = 'INV-ADJ'
							 , C901_LAST_TRANSACTION_TYPE = '4316'
						 WHERE c5053_location_part_map_id = v_id;
					END IF;
				END IF;
			END LOOP;
		END IF;
/*
		-- Update T5052_LOCATION_MASTER Table
		SELECT c5052_location_id INTO v_lcation_cd 
			FROM t5052_location_master 
			WHERE c5052_location_cd = p_locationstr 
				AND C5051_INV_WAREHOUSE_ID = p_warehouseid
				AND c5052_void_fl IS NULL;
	*/	
		IF p_typestr IS NOT NULL AND p_statusstr IS NOT NULL
		THEN
				SELECT COUNT(1) INTO v_cnt
				   FROM
				    (
				         SELECT gm_pkg_op_item_control_rpt.get_part_location_cur_qty (t5053.c205_part_number_id,
				            t5052.c5052_location_id, t5051.c901_warehouse_type) qty
				           FROM t5053_location_part_mapping t5053, t5052_location_master t5052, t5051_inv_warehouse t5051
				          WHERE t5051.c5051_inv_warehouse_id = t5052.c5051_inv_warehouse_id
				            AND t5053.c5052_location_id      = t5052.c5052_location_id
				            AND t5052.c5052_location_id      = p_locationstr
				            AND (t5052.c901_status           = '93310'
				            AND p_statusstr                  <> '93310')
				    )
				    LPM
				  WHERE LPM.qty > 0;
				
				IF v_cnt > 0 THEN
					raise_application_error ('-20956','');
				END IF;
					
			UPDATE t5052_location_master
			   SET c901_location_type = p_typestr
				 , c901_status = p_statusstr
				 , c5052_last_updated_by = p_user_id
				 , c5052_last_updated_date = CURRENT_DATE
--				 , C5051_INV_WAREHOUSE_ID = p_warehouseid
--				 , C5057_building_id = NVL(p_building_id,C5057_building_id)	  -- added for PMT-33507
			 WHERE c5052_location_id = p_locationstr;

		END IF;
	END gm_sav_inv_loc_part_detail;

	PROCEDURE gm_sav_inv_loc_log (
		p_created_date	 IN   t5054_inv_location_log.c5054_created_date%TYPE
	  , p_created_by	 IN   t5054_inv_location_log.c5054_created_by%TYPE
	  , p_part_num		 IN   t5054_inv_location_log.c205_part_number_id%TYPE
	  , p_loc_id		 IN   t5054_inv_location_log.c5052_location_id%TYPE
	  , p_org_qty		 IN   t5054_inv_location_log.c5054_orig_qty%TYPE
	  , p_txn_id		 IN   t5054_inv_location_log.c5054_txn_id%TYPE
	  , p_txn_qty		 IN   t5054_inv_location_log.c5054_txn_qty%TYPE
	  , p_new_qty		 IN   t5054_inv_location_log.c5054_new_qty%TYPE
	  , p_action		 IN   t5054_inv_location_log.c901_action%TYPE
	  , p_type			 IN   t5054_inv_location_log.c901_type%TYPE
	  , p_txn_typ		 IN   t5054_inv_location_log.c901_txn_type%TYPE	 
	  , p_company_id  	 IN   t5054_inv_location_log.c1900_company_id%TYPE -- CompanyId
	  , p_plant_id     	 IN   t5054_inv_location_log.c5040_plant_id%TYPE  --PlantId
	  , p_tag_id 		 IN   t5054_inv_location_log.c5010_tag_id%TYPE DEFAULT NULL
	  , p_control_number IN   t5054_inv_location_log.c5054_TXN_CONTROL_NUMBER%TYPE default null
	  
	)
	AS
	BEGIN	
		INSERT INTO t5054_inv_location_log
					(c5054_inv_location_log_id, c5054_created_date, c5054_created_by, c205_part_number_id
				   , c5052_location_id, c5054_orig_qty, c5054_txn_id, c5054_txn_qty, c5054_new_qty, c901_action
				   , c901_type, c901_txn_type, c5010_tag_id, c1900_company_id, c5040_plant_id,c5054_TXN_CONTROL_NUMBER
					) 
			 VALUES (s5054_inv_location_log.NEXTVAL, p_created_date, p_created_by, p_part_num
				   , p_loc_id, p_org_qty, p_txn_id, p_txn_qty, p_new_qty, p_action
				   , p_type, p_txn_typ, p_tag_id, p_company_id, p_plant_id,p_control_number
					);
	END gm_sav_inv_loc_log;
	
	 /**********************************************************************
	 * PMT-34027   : Inventory device - Dashboard and Transaction List Filter 
	 * Description : Procedure to get the building id for the passing partyid
	 * Author	   : N Raja
	**********************************************************************/
	
	PROCEDURE gm_fch_user_building_id(
           p_partyid     IN  T101_USER.C101_PARTY_ID%TYPE
	     , p_groupid     IN  T1500_GROUP.C1500_GROUP_ID%TYPE
	     , p_buildingid  OUT T1501_GROUP_MAPPING.C5057_BUILDING_ID%TYPE
		)
		AS
	      v_company_id   T1900_COMPANY.C1900_COMPANY_ID%TYPE;
	      v_building_id  T1501_GROUP_MAPPING.C5057_BUILDING_ID%TYPE;
	      v_plant_id     T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
	      v_plant_fl	 VARCHAR2(1);
	      
        BEGIN
	        SELECT get_compid_frm_cntx()
		      INTO v_company_id
		   	  FROM DUAL;
		   	  
		   	SELECT NVL(get_plantid_frm_cntx(),get_rule_value('DEFAULT_PLANT','ONEPORTAL')) 
		   	  INTO v_plant_id 
		   	  FROM DUAL;
		   	  
		   	SELECT get_rule_value(v_plant_id,'BUILDINGPLANTMAP') 
              INTO v_plant_fl 
              FROM DUAL;
		   	  
	   IF v_plant_fl = 'Y' THEN
	   
	   BEGIN
			SELECT c5057_building_id 
			  INTO v_building_id
			  FROM t1501_group_mapping
		   	 WHERE c1500_group_id = p_groupid        --INVENTORY  '100000113' (p_group_id), SHIPPING '100025' (p_group_id)
			   AND c1501_void_fl IS NULL
			   AND c101_mapped_party_id= p_partyid    -- Inventory party id - 303153, Shipping party id - 3617156
			   AND c1900_company_id = v_company_id;
	     EXCEPTION
	          WHEN OTHERS THEN		
	           v_building_id := null;	
	   END;
	   END IF;
		p_buildingid := v_building_id;
    END gm_fch_user_building_id;

	 /**********************************************************************
	 * Description : function to get the total bulk qty for the respective part
	 * Author	   : Murali
	**********************************************************************/
	FUNCTION get_total_bulkloc_qty (
		p_part_num	 t5053_location_part_mapping.c205_part_number_id%TYPE
	  , p_whtype	 t5051_inv_warehouse.c901_warehouse_type%TYPE
	)
		RETURN NUMBER
	IS
		v_bulk_qty	   NUMBER;
		v_plant_id     t5040_plant_master.c5040_plant_id%TYPE;
	BEGIN
		SELECT  NVL(get_plantid_frm_cntx(),get_rule_value('DEFAULT_PLANT','ONEPORTAL')) INTO  v_plant_id FROM DUAL;
		
		SELECT NVL (loc.qty, 0) - NVL (inhouse.qty, 0)
		  INTO v_bulk_qty
		  FROM (SELECT	 SUM (NVL (t5053.c5053_curr_qty, 0)) qty, t5053.c205_part_number_id pnum
					FROM t5053_location_part_mapping t5053, t5052_location_master t5052, t5051_inv_warehouse t5051
				   WHERE t5052.c5052_location_id = t5053.c5052_location_id
				     AND t5051.c5051_inv_warehouse_id = t5052.c5051_inv_warehouse_id				     
		             AND t5051.c901_status_id = 1
		             AND t5051.c901_warehouse_type = p_whtype
					 AND t5052.c901_location_type = '93336'   -- Bulk
					 AND t5052.c901_status = '93310'   -- ACTIVE LOCATION
					 AND t5053.c205_part_number_id = p_part_num
					 AND t5052.c5052_void_fl IS NULL
					 AND t5052.c5040_plant_id = v_plant_id
				GROUP BY t5053.c205_part_number_id) loc
			 , (SELECT	 SUM (NVL (t413.c413_item_qty, 0)) qty, t413.c205_part_number_id pnum
					FROM t413_inhouse_trans_items t413, t412_inhouse_transactions t412
				   WHERE t412.c412_status_fl < 4
					 AND t412.c412_void_fl IS NULL
					 AND t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
					 AND t412.c412_type = DECODE(p_whtype,56001, 56010,93341) -- replenishment
					 AND t413.c205_part_number_id = p_part_num
					 AND t412.c5040_plant_id = v_plant_id
				GROUP BY t413.c205_part_number_id) inhouse
		 WHERE loc.pnum = inhouse.pnum(+);

		RETURN v_bulk_qty;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN 0;
	END get_total_bulkloc_qty;

	/**********************************************************************
	 * Description : function to get the open transaction count
	 * Author	   : Ritesh
	**********************************************************************/
	FUNCTION get_open_txn_count (
		p_part_num	 t5053_location_part_mapping.c205_part_number_id%TYPE
	  , p_lcn_id	 t5053_location_part_mapping.c5052_location_id%TYPE
	  , p_whtype	 t5051_inv_warehouse.c901_warehouse_type%TYPE
	)
		RETURN NUMBER
	IS
		v_cnt		   NUMBER;
		v_plant_id     t5040_plant_master.c5040_plant_id%TYPE;
	BEGIN
		SELECT  NVL(get_plantid_frm_cntx(),get_rule_value('DEFAULT_PLANT','ONEPORTAL')) 
			  INTO  v_plant_id
			  FROM DUAL;
		
		SELECT COUNT (1)
		  INTO v_cnt
		  FROM t5053_location_part_mapping t5053
		  	 , t5055_control_number t5055
		  	 , t5050_invpick_assign_detail t5050
		 WHERE t5053.c5052_location_id = p_lcn_id
		   AND t5053.c205_part_number_id = p_part_num
		   AND t5053.c205_part_number_id = t5055.c205_part_number_id
		   AND t5053.c5052_location_id = t5055.c5052_location_id
		   AND t5055.c901_ref_type = DECODE(p_whtype,56001, 56010,93341) 	 -- FG transfer
		   AND t5055.c5055_ref_id = t5050.c5050_ref_id
		   AND NVL (t5050.c901_status, 0) <> 93006	 -- Not completed
		   AND t5050.c5050_void_fl IS NULL
		   AND t5055.c5055_void_fl IS NULL
		   AND t5055.c5040_plant_id = v_plant_id;

		RETURN v_cnt;
	END get_open_txn_count;
	 /**********************************************************************
	 * Description : Procedure to Save the new Zone
	 * Author	   : Mselvamani
	**********************************************************************/	
		PROCEDURE gm_add_zone(
		  p_zone IN  t901_code_lookup.c901_code_nm%TYPE
		, p_created_by	 IN   t901_code_lookup.C901_CREATED_BY%TYPE
		, p_out OUT t901_code_lookup.c901_code_id%TYPE 
		)
		AS
		v_code_id NUMBER;
		BEGIN
		   SELECT s901_code_lookup.NEXTVAL
           INTO v_code_id
           FROM DUAL;

         INSERT INTO t901_code_lookup
                     (c901_code_id, c901_code_nm, c901_code_grp,
                      c902_code_nm_alt,c901_created_date, c901_created_by,c901_active_fl
                     )
              VALUES (v_code_id, p_zone, 'LOCZT',
                     p_zone,CURRENT_DATE, p_created_by,'1'
                     );
                     
          IF p_created_by IS NOT NULL THEN
          		GM_PKG_OP_INV_LOCATION_MODULE.GM_ADD_ZONE_ACCESS(p_created_by,v_code_id);
          END IF;
          
          p_out:= v_code_id;          
	   END gm_add_zone;
	  
/**********************************************************************
	 * Description : Procedure to Save the new Zone in access table
	 * Author	   : Smariyappan
	**********************************************************************/	
PROCEDURE GM_ADD_ZONE_ACCESS(
  p_user_id	   IN  T101_user.c101_user_id%TYPE
, p_zone 	   IN  t901a_lookup_access.C901_CODE_ID%TYPE
)
AS
	v_company_id   T1900_COMPANY.C1900_COMPANY_ID%TYPE;
	v_access_id    NUMBER;
	v_dept_id 	   t101_user.C901_DEPT_ID%TYPE;
BEGIN
	--Get the Department id based on the user id
	BEGIN
		SELECT get_dept_id(p_user_id) 
			INTO v_dept_id 
		FROM DUAL;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
			 v_dept_id := '';
	 END;
	
	IF v_dept_id IS NOT NULL THEN	
		
		SELECT get_compid_frm_cntx()
	      INTO v_company_id
	  	FROM DUAL;
	  	
	  	SELECT s901a_lookup_access.NEXTVAL
	       INTO v_access_id
	    FROM DUAL;
		
	 	INSERT
		INTO t901a_lookup_access
		  (
		    c901a_code_access_id,
		    c901_access_code_id,
		    c901_code_id,
		    c1900_company_id
		  )
		  VALUES
		  (
		    v_access_id,
		    v_dept_id,
		    p_zone,
		    v_company_id
		  );
		  
	END IF;
			
END GM_ADD_ZONE_ACCESS;

END gm_pkg_op_inv_location_module;
/
