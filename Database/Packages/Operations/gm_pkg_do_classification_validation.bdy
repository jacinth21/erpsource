/* Formatted on 5/11/2021 5:12:52 PM (QP5 v5.362) */
CREATE OR REPLACE PACKAGE BODY gm_pkg_do_classification_validation
IS
    /************************************************************************
	* Description : This Main Procedure is used to update the
	* allowed tag column on the DO Tag details from the DO Classifcation 
	* Author : Karthik Somanathan
	*************************************************************************/
	PROCEDURE gm_sav_tag_do_class_validation
	AS
	CURSOR Order_Items
	IS
		SELECT c501_order_id orderid,
		c5010_tag_id tagid,
		DECODE(c501c_class_type,'D','Y','N') classtype
		FROM t501c_order_tag_do_classification
		WHERE c501c_record_processed_fl = 'X';
		
	  v_to_email_id    VARCHAR2 (1000);
	  v_subject        VARCHAR2 (1000);
	  v_body           VARCHAR2 (1000);
	  crlf        	   VARCHAR2 (2) := CHR (13) || CHR (10);	
      
	BEGIN
		BEGIN
			FOR Ord IN Order_Items
			LOOP
				UPDATE t501d_order_tag_usage_details
				SET c501d_allowed_tag_in_scorecard = Ord.classtype,
				c501d_last_updated_date = CURRENT_DATE
				WHERE c501_order_id = Ord.orderid
				AND c5010_tag_id 	= Ord.tagid;
			END LOOP;
			
			 --Update the score card flag in t501d table for all the orders associated with the Bulk DO accounts.
			 UPDATE t501d_order_tag_usage_details
			 SET c501d_allowed_tag_in_scorecard = 'Y'
			 WHERE c901_order_category          = 111161 --Bulk DO
			 AND c501_order_id                 IN
			   (SELECT t501.c501_order_id
			   FROM t704a_account_attribute t704a ,
				 t501_order t501
			   WHERE t704a.c704_account_id     = t501.c704_account_id
			   AND t704a.c901_attribute_type   = 111161
			   AND t704a.c704a_attribute_value = 'Y'
			   AND t704a.c704a_void_fl        IS NULL
			   AND t501.c501_void_fl          IS NULL
			   )
			 AND c501d_allowed_tag_in_scorecard IS NULL  
			 AND c501d_void_fl IS NULL ;
			
			 --Update the processesd Flag
			 UPDATE t501c_order_tag_do_classification
			   SET c501c_record_processed_fl = 'Y'
			 WHERE c501c_record_processed_fl = 'X'; 
			 
			COMMIT;
		EXCEPTION WHEN OTHERS THEN

 		v_to_email_id := get_rule_value ('TO_EMAIL_ID', 'DO_TAG_NOTIFICATION');
		v_subject := get_rule_value ('EMAIL_SUBJECT', 'DO_TAG_NOTIFICATION');
		v_body := get_rule_value ('EMAIL_BODY', 'DO_TAG_NOTIFICATION');
		
		v_body := v_body
		|| crlf
		|| ' Error message :- '
		|| SQLERRM;
		
		gm_com_send_email_prc (v_to_email_id, v_subject, v_body);
		
		ROLLBACK;
		
	END;
	END gm_sav_tag_do_class_validation;

	/************************************************************************
	* Description : This procedure will loop through the unique 
	* main system id from the new table and call another new procedure
	* Author : Sai Sheshank Vaidya
	*************************************************************************/
	PROCEDURE gm_upd_tag_do_classification_type_main
	AS
	  CURSOR v_class_sys
	  IS
	    SELECT DISTINCT c207_do_class_system_id do_class_systemid
	    FROM T501E_DO_CLASS_SYSTEM_CREDIT_MAPPING
	    WHERE C501E_VOID_FL IS NULL;
	BEGIN
	  FOR class_sys IN v_class_sys
	  LOOP
	    gm_pkg_do_classification_validation.gm_upd_tag_do_system_classification_type(class_sys.do_class_systemid);
	  END LOOP;
	  COMMIT;
	END gm_upd_tag_do_classification_type_main;
	
	/************************************************************************
	* Description : In this procedure we are getting the list of orders that
	* has class type "T" for the Creo and Creo Amp systems and update to "D"
	* Author : Sai Sheshank Vaidya
	*************************************************************************/

	PROCEDURE gm_upd_tag_do_system_classification_type(
    	p_do_class_system_id T501E_DO_CLASS_SYSTEM_CREDIT_MAPPING.c207_do_class_system_id%TYPE )
	AS
	BEGIN
	  UPDATE T501C_ORDER_TAG_DO_CLASSIFICATION
	  SET C501C_CLASS_TYPE             = 'D',
	    c501c_record_processed_fl      = 'X',
	    C501C_CREATED_DATE             = SYSDATE
	  WHERE C501C_ORD_TAG_DO_CLASS_ID IN
	    (SELECT t501c.C501C_ORD_TAG_DO_CLASS_ID
	    FROM t501c_order_tag_do_classification T501C
	    WHERE t501c.c207_system_id IN
	      (SELECT DISTINCT c207_addl_credit_system_id
	      FROM T501E_DO_CLASS_SYSTEM_CREDIT_MAPPING
	      WHERE c207_do_class_system_id = p_do_class_system_id
	      AND C501E_VOID_FL            IS NULL
	      )
	    AND t501c.c501c_class_type = 'T'
	    AND t501c.c501_order_id   IN
	      (SELECT t501.c501_order_id
	      FROM T501_ORDER T501 ,
	        (SELECT c501_order_id,
	          MAX(MIS) MIS,
	          MAX(MIS_TYPE) MIS_TYPE,
	          MAX(AMP) AMP,
	          MAX(AMP_TYPE) AMP_TYPE
	        FROM
	          (SELECT t501c.c501_order_id ,
	            DECODE (t501c.c207_system_id, p_do_class_system_id, t501c.c207_system_id,NULL) MIS ,
	            DECODE (t501c.c207_system_id, p_do_class_system_id, C501C_Class_Type,NULL) MIS_TYPE ,
	            DECODE (t501c.c207_system_id, p_do_class_system_id, NULL ,t501c.c207_system_id) AMP ,
	            DECODE (t501c.c207_system_id, p_do_class_system_id, NULL,C501C_Class_Type) AMP_TYPE
	          FROM t501c_order_tag_do_classification t501c ,
	            (SELECT t501c.c501_order_id
	            FROM t501c_order_tag_do_classification t501c ,
	              T501_Order t501
	            WHERE t501.C501_ORDER_ID = t501c.c501_order_id
	            AND t501.c501_order_date_time >= TO_DATE(get_rule_value('DO_CLASS_DATE','DO_CLASS_SYSTEM') ,'MM/DD/YYYY')
	            AND t501c.c207_system_id = p_do_class_system_id
	            AND C501C_Class_Type     = 'D'
	            ) t501
	          WHERE t501c.c207_system_id IN
	            (SELECT DISTINCT c207_addl_credit_system_id
	            FROM T501E_DO_CLASS_SYSTEM_CREDIT_MAPPING
	            WHERE c207_do_class_system_id = p_do_class_system_id
	            AND C501E_VOID_FL            IS NULL
	            )
	          AND t501c.c501_order_id = t501.c501_order_id
	          )
	        GROUP BY c501_order_id
	        ) T501C
	      WHERE T501.c501_order_id = T501C.c501_order_id
	      AND MIS                 IS NOT NULL
	      AND AMP                 IS NOT NULL
	      AND MIS_TYPE             = 'D'
	      AND AMP_TYPE             = 'T'
	      )
	    );
	END gm_upd_tag_do_system_classification_type;


	
	/************************************************************************
	* Description : This procedure is to clear the classify flag to Null - New
	* Author : Sai Sheshank Vaidya
	*************************************************************************/
	
	PROCEDURE gm_chk_clear_classification_flag(
    	p_orderid IN t501_order.c501_order_id%TYPE )
	AS
	BEGIN
	  	UPDATE t501_order t501
		SET t501.c501_do_classified_fl    = NULL
		WHERE t501.c501_order_id          = p_orderid;
		
		UPDATE t501_order t501
		SET t501.c501_do_classified_fl    = NULL
		WHERE t501.c501_order_id           IN
		  (SELECT t501.c501_parent_order_id
		  	FROM t501_order t501
		   WHERE t501.c501_order_id = p_orderid
		   AND t501.c501_void_fl IS NULL
		  );
		  --PC-5722 DO Classification - Clear classify flag for Parent Order 
		  UPDATE t501_order t501
          SET t501.c501_do_classified_fl = NULL
		  WHERE t501.c501_parent_order_id IN
			  (SELECT t501.c501_parent_order_id
			   FROM t501_order t501
			  WHERE t501.c501_order_id = p_orderid  
			  AND t501.c501_parent_order_id IS NOT NULL			  
			  AND t501.c501_void_fl IS NULL
			  )
			AND (t501.c901_order_type IS NULL
			OR t501.c901_order_type NOT IN (2518, 2524, 2535, 2533, 2534, 2519));
		  
			UPDATE t501_order t501
				SET t501.c501_do_classified_fl  = NULL
			WHERE t501.c501_parent_order_id = p_orderid
			AND t501.c501_parent_order_id  IS NOT NULL
			AND t501.c501_void_fl          IS NULL
			AND (t501.c901_order_type      IS NULL
			OR t501.c901_order_type NOT    IN (2518, 2524, 2535, 2533, 2534, 2519));
	  
	END gm_chk_clear_classification_flag;
	
END gm_pkg_do_classification_validation;
/