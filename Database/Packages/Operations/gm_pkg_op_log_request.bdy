/* Formatted on 2008/04/09 16:48 (Formatter Plus v4.8.5) */
-- @"c:\database\packages\operations\gm_pkg_op_log_request.bdy"

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_log_request
IS
--
   /*******************************************************
     * Description : Procedure to log reconfigure requests
     * Author     : Rakhi Gandhi
     *******************************************************/
--
   PROCEDURE gm_log_reconfig_request (
      p_reconfig_source           IN  NUMBER, 
      p_consign_id               IN       t504_consignment.c504_consignment_id%TYPE,
      p_bo_request_id            IN       t520_request.c520_request_id%TYPE,
      p_user                     IN       VARCHAR2,
      p_out_reconfigure_log_id   OUT      t530_reconfigure_log.c530_reconfigure_log_id%TYPE
   )
   AS
      v_reconfigure_log_id   t530_reconfigure_log.c530_reconfigure_log_id%TYPE;
   BEGIN
      SELECT s530_reconfigure_log.NEXTVAL
        INTO p_out_reconfigure_log_id
        FROM DUAL;

      INSERT INTO t530_reconfigure_log
                  (c530_reconfigure_log_id, c901_reconfigure_source,
                   c530_from_ref_id, c530_to_ref_id, c530_created_by,
                   c530_created_date, c530_void_fl
                  )
           VALUES (p_out_reconfigure_log_id, p_reconfig_source,
                   p_consign_id, p_bo_request_id, p_user,
                   SYSDATE, NULL
                  );
   END gm_log_reconfig_request;

--
   PROCEDURE gm_log_reconfig_request_detail (
      p_reconfigure_log_id           IN       t530_reconfigure_log.c530_reconfigure_log_id%TYPE,
      p_part_num                     IN       t205_part_number.c205_part_number_id%TYPE,
      p_control_num                  IN       t505_item_consignment.c505_control_number%TYPE,
      p_qty                          IN       NUMBER,
      p_action                       IN       VARCHAR2,
      p_out_reconfig_log_detail_id   OUT      t531_reconfigure_log_detail.c531_reconfigure_log_detail_id%TYPE
   )
   AS
   BEGIN
      SELECT s531_reconfigure_log_detail.NEXTVAL
        INTO p_out_reconfig_log_detail_id
        FROM DUAL;

      INSERT INTO t531_reconfigure_log_detail
                  (c531_reconfigure_log_detail_id, c530_reconfigure_log_id,
                   c205_part_number_id, c530_qty, c530_control_number,
                   c901_action
                  )
           VALUES (p_out_reconfig_log_detail_id, p_reconfigure_log_id,
                   p_part_num, p_qty, p_control_num,
                   p_action
                  );
   END gm_log_reconfig_request_detail;
--
END gm_pkg_op_log_request;