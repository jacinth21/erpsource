--@"C:\Database\Packages\Operations\gm_pkg_op_udi_label.bdy"

CREATE OR REPLACE
PACKAGE BODY gm_pkg_op_udi_label

IS
/********************************************************
	* Author      : Yoga
	* Description : Update HRF and MRF columns in t2550  from t408
* ******************************************************/
PROCEDURE gm_ld_update_udi_data
AS
  v_hrf VARCHAR2(200);
  v_mrf VARCHAR2(200);
  v_count NUMBER:=1;
   	 
  CURSOR dhr_cur
  IS
    SELECT t408.c408_dhr_id dhr,  t408.c408_packing_slip packingslip
      , t408.c408_control_number controlnum, t408.c205_part_number_id partnum
      , t408.c301_vendor_id vendor_id 
      , t408.c2540_donor_id donor_id
      , t2060.c2060_di_number di_number
      , get_expiry_date(t408.c205_part_number_id, t408.c408_control_number) expiry_date
    FROM t408_dhr t408 , t2060_di_part_mapping t2060
    WHERE  t408.c205_part_number_id = t2060.c205_part_number_id
      AND t408.c408_void_fl IS NULL
      AND t408.c408_status_fl = '4'		-- Verified
      AND t408.c301_vendor_id !=345 ; -- BBA
BEGIN

  FOR v_index IN dhr_cur
  loop
	  BEGIN
  
      IF (v_index.expiry_date IS NULL) THEN
        v_hrf := '(01)'|| v_index.di_number||'(10)'||v_index.controlnum;
        v_mrf := '<FNC1>01'|| v_index.di_number||'10'||v_index.controlnum;
      ELSE
        v_hrf := '(01)'|| v_index.di_number||'(17)'||to_char(v_index.expiry_date,'yyMMdd')   ||'(10)' ||v_index.controlnum;
        v_mrf := '<FNC1>01'|| v_index.di_number||'17'||to_char(v_index.expiry_date,'yyMMdd') ||'10' ||v_index.controlnum;
      END IF;
   
	    UPDATE t2550_part_control_number
		  SET c2550_last_updated_by   = 30301,
        c2550_last_updated_date   = SYSDATE,
        c2550_udi_hrf             = v_hrf, 
        c2550_udi_mrf             = v_mrf
		  WHERE c205_part_number_id   = v_index.partnum
		    AND c2550_control_number  = v_index.controlnum;
     
      --dbms_output.put_line (v_count || ' Update Part ' || v_index.partnum || ', control num ' || v_index.controlnum || ', DHR ' || v_index.dhr || ', HRF ' || v_hrf || ', MRF ' || v_mrf);

    v_count:=v_count+1;
	EXCEPTION
  WHEN OTHERS THEN
    dbms_output.put_line ('Error: For part ' || v_index.partnum || ', control num ' || v_index.controlnum || ', DHR ' || v_index.dhr || ' Error Desc:' || sqlerrm);
  END;

  END loop;

END gm_ld_update_udi_data;

/********************************************************
	* Author      : Yoga
	* Description : Update HRF and MRF columns in t2550  from t408A
* ******************************************************/
PROCEDURE gm_ld_update_udi_tissue_data
AS
  v_hrf VARCHAR2(200);
  v_mrf VARCHAR2(200);
  v_count NUMBER:=1;
   	 
  CURSOR dhr_cur
  IS
    SELECT t408a.c408_dhr_id dhr
      , t408a.c408a_conrol_number controlnum, t408a.c205_part_number_id partnum
      , t2060.c2060_di_number di_number
      , get_expiry_date(t408a.c205_part_number_id, t408a.c408a_conrol_number) expiry_date
    FROM  globus_app.t408a_dhr_control_number t408a, t408_dhr t408 , t2060_di_part_mapping t2060
    WHERE t408.c205_part_number_id = t2060.c205_part_number_id
      AND t408.c408_dhr_id = t408a.c408_dhr_id
      AND t408.c408_void_fl IS NULL
      AND t408a.c408a_void_fl IS NULL;
BEGIN

  FOR v_index IN dhr_cur
  loop
	  BEGIN
  
      IF (v_index.expiry_date IS NULL) THEN
        v_hrf := '(01)'|| v_index.di_number||'(10)'||v_index.controlnum;
        v_mrf := '<FNC1>01'|| v_index.di_number||'10'||v_index.controlnum;
      ELSE
        v_hrf := '(01)'|| v_index.di_number||'(17)'||to_char(v_index.expiry_date,'yyMMdd')   ||'(10)' ||v_index.controlnum;
        v_mrf := '<FNC1>01'|| v_index.di_number||'17'||to_char(v_index.expiry_date,'yyMMdd') ||'10' ||v_index.controlnum;
      END IF;
   
	    UPDATE t2550_part_control_number
		  SET c2550_last_updated_by   = 30301,
        c2550_last_updated_date   = SYSDATE,
        c2550_udi_hrf             = v_hrf, 
        c2550_udi_mrf             = v_mrf
		  WHERE c205_part_number_id   = v_index.partnum
		    AND c2550_control_number  = v_index.controlnum;
     
      --dbms_output.put_line (v_count || ' Update Part ' || v_index.partnum || ', control num ' || v_index.controlnum || ', DHR ' || v_index.dhr || ', HRF ' || v_hrf || ', MRF ' || v_mrf);

    v_count:=v_count+1;
	EXCEPTION
  WHEN OTHERS THEN
    dbms_output.put_line ('Error: For part ' || v_index.partnum || ', control num ' || v_index.controlnum || ', DHR ' || v_index.dhr || ' Error Desc:' || sqlerrm);
  END;

  END loop;

END gm_ld_update_udi_tissue_data;

/******************************************************************************
	* Author      : Yoga
	* Description : Get expiry Date from t2550 for Part Number and Control Number
* *****************************************************************************/
FUNCTION get_expiry_date
(
    p_part_num t205_part_number.c205_part_number_id%TYPE,
		p_ctrl_num t2550_part_control_number.c2550_control_number%TYPE
)
RETURN DATE
IS
  v_exp_dt t2550_part_control_number.C2550_EXPIRY_DATE%TYPE;
  
BEGIN
	
	BEGIN
		SELECT t2550.C2550_EXPIRY_DATE
			INTO v_exp_dt
		FROM t2550_part_control_number t2550
	 WHERE t2550.c2550_control_number = p_ctrl_num
		 AND t2550.c205_part_number_id = p_part_num;

	EXCEPTION 
	WHEN NO_DATA_FOUND THEN
		v_exp_dt:=NULL;
	END;

	RETURN v_exp_dt;
END get_expiry_date;

/******************************************************************************
	* Author      : Karthick
	* Description : Get Manufacturing Date from t408 for Part Number and Control Number
* *****************************************************************************/
FUNCTION get_manf_date_from_dhr 
(
    p_part_num t408_dhr.c205_part_number_id%TYPE,
    p_lot_num t408_dhr.c408_control_number%TYPE
)
RETURN DATE
IS
  v_manf_dt t408_dhr.c408_manf_date%TYPE;
  
BEGIN
	
		BEGIN
		SELECT MANFDATE
		   INTO v_manf_dt
		FROM
		  (SELECT c408_created_date MANFDATE
		  FROM t408_dhr
		  WHERE c205_part_number_id = p_part_num
                           AND c408_control_number = NVL( p_lot_num, c408_control_number)
                           AND c408_void_fl IS NULL
		  ORDER BY c408_created_date ASC
		  )
		WHERE ROWNUM = 1;

	EXCEPTION 
	WHEN NO_DATA_FOUND THEN
		v_manf_dt:= NULL;
	END;

	RETURN v_manf_dt;
END get_manf_date_from_dhr;

/******************************************************************************
	* Author      : tmuthusamy
	* Description : Get expiry Date from t2550 for Part Number and Control Number as Char
* *****************************************************************************/
FUNCTION get_expiry_date_as_char
(
    p_part_num t205_part_number.c205_part_number_id%TYPE,
	p_ctrl_num t2550_part_control_number.c2550_control_number%TYPE,
	p_date_fmt t2550_part_control_number.C2550_REF_ID%TYPE DEFAULT  'YYMMDD'
)
RETURN varchar2
IS
  v_exp_dt t2550_part_control_number.C2550_REF_ID%TYPE;
  
BEGIN
	
	BEGIN
		SELECT to_char(t2550.C2550_EXPIRY_DATE,p_date_fmt)
			INTO v_exp_dt
		FROM t2550_part_control_number t2550
	 WHERE t2550.c2550_control_number = p_ctrl_num
		 AND t2550.c205_part_number_id = p_part_num;

	EXCEPTION 
	WHEN NO_DATA_FOUND THEN
		v_exp_dt:=NULL;
	END;

	RETURN v_exp_dt;
END get_expiry_date_as_char;


END gm_pkg_op_udi_label;
/