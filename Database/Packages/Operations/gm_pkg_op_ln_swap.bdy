/* Formatted on 2011/05/26 19:20 (Formatter Plus v4.8.0) */
--@"c:\database\packages\operations\gm_pkg_op_ln_swap";
CREATE OR REPLACE
PACKAGE BODY gm_pkg_op_ln_swap
IS
  --
  /******************************************************************
  * Description : This Procedure is used to avoid 
  * account name and sub location to be null
  * Author     : Jeeva Balaraman
  ****************************************************************/
PROCEDURE gm_cs_sav_accept_return
  (
    p_conid    IN t504_consignment.c504_consignment_id%TYPE,
    p_userid   IN t504_consignment.c504_last_updated_by%TYPE,
    p_expretdt IN t504a_loaner_transaction.c504a_return_dt%TYPE )
AS
  v_prod_req_id NUMBER;
  v_count       NUMBER;
  v_date_diff NUMBER;
  v_date_format varchar2(20);
BEGIN
	
	 --PC-4351
	 SELECT get_compdtfmt_frm_cntx() 
	  INTO v_date_format 
	  from dual;
	  
	-- check return date	
	SELECT trunc(p_expretdt) - trunc(current_date)
  	  INTO v_date_diff
      FROM dual;
      
     IF v_date_diff > 0 THEN
		raise_application_error (- 20999, ' Return Date ('||to_char(p_expretdt,v_date_format)||') cannot be greater than current date ('||to_char(current_date,v_date_format)||') ');
	 END IF;
	
  BEGIN
	  
    SELECT NVL (t526.c525_product_request_id, 0)
    INTO v_prod_req_id
    FROM t526_product_request_detail t526,
      t504a_loaner_transaction t504a
    WHERE t504a.c504_consignment_id          = p_conid
    AND c504a_return_dt                     IS NULL
    AND c504a_void_fl                       IS NULL
    AND t504a.c526_product_request_detail_id = t526.c526_product_request_detail_id;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    v_prod_req_id := 0;
  END;
  
  UPDATE t504a_loaner_transaction
  SET c504a_return_dt       = p_expretdt,
    c504a_last_updated_by   = p_userid,
    c504a_last_updated_date = CURRENT_DATE
  WHERE c504_consignment_id = p_conid
  AND c504a_return_dt      IS NULL
  AND c504a_void_fl        IS NULL;
  --
  -- Call the common procedure to update the status
gm_pkg_op_loaner_set_txn.gm_upd_consign_loaner_status (p_conid, 25, p_userid) ;

  UPDATE t504a_consignment_loaner
  SET c504a_loaner_dt        = NULL,
    c504a_expected_return_dt = NULL,
    c504a_allocated_fl         = NULL,
    c504a_last_updated_by    = p_userid,
    c504a_last_updated_date  = CURRENT_DATE
    -- status changes to pending processing after accepting loaner return
  WHERE c504_consignment_id = p_conid
  AND c504a_void_fl        IS NULL;
  
  UPDATE t504_consignment
  SET 
     -- c701_distributor_id  = NULL,   -- PMT-24984: Missing loaned details on Loaner paperwork that is printed in the 'Process Check' step
        c704_account_id        = NULL,
        c504_last_updated_by   = p_userid,
        c504_last_updated_date = CURRENT_DATE
   -- , c504_ship_to = NULL
   -- , c504_ship_to_id = NULL
WHERE c504_consignment_id = p_conid
  AND c504_void_fl         IS NULL;
  --
  
  UPDATE t5010_tag
  SET 
  --c704_account_id               = NULL,      //to avoid acc name null
    c901_location_type              = 40033,
    c5010_location_id               = 52099,
    --c901_sub_location_type          = NULL,  //to avoid sub location type to be null
    --c5010_sub_location_id           = NULL,  //to avoid sub location id to be null
    c5010_last_updated_by           = p_userid,
    c5010_last_updated_date         = CURRENT_DATE,
    c5010_last_updated_trans_id     = p_conid,
    c901_status                     = decode(c901_status,26240614,51012,c901_status),  --active when missing to accept return
    c5010_missing_since                = decode(c901_status,26240614,NULL,c5010_missing_since)
  WHERE c5010_last_updated_trans_id = p_conid
  AND c5010_void_fl                IS NULL;
  
  gm_save_status_details (p_conid, '', p_userid, NULL, CURRENT_DATE, 91122, 91103 );
  -- for non-usage charges
  IF (v_prod_req_id != 0) THEN
    SELECT COUNT (c504a_loaner_transaction_id)
    INTO v_count
    FROM t504a_loaner_transaction t504al
    WHERE t504al.c526_product_request_detail_id IN
      (SELECT c526_product_request_detail_id
      FROM t526_product_request_detail
      WHERE c525_product_request_id = v_prod_req_id
      AND c526_void_fl             IS NULL
      )
    AND c504a_return_dt IS NULL;
    IF v_count           = 0 THEN
      UPDATE t525_product_request
      SET c525_usage_date           = TRUNC (CURRENT_DATE),
        c525_last_updated_by        = p_userid,
        c525_last_updated_date      = CURRENT_DATE
      WHERE c525_product_request_id = v_prod_req_id
      AND c525_void_fl             IS NULL;
    END IF;
  END IF;
END gm_cs_sav_accept_return;


END gm_pkg_op_ln_swap;
/