/* Formatted on 2011/04/28 10:36 (Formatter Plus v4.8.0) */
--@"c:\database\packages\operations\gm_pkg_op_backorder_rpt.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_backorder_rpt
IS
--
  /*******************************************************
   * Description : Main Procedure to generate set overview cross tab report
   * Author     : Xun
   *******************************************************/
--
--
  /*******************************************************************
   * Description : Procedure to generate  Yet to build (Back Order SQL)
   * Author     : Xun
   *******************************************************************/
--
   PROCEDURE gm_fch_cons_forecast_count (
      p_forecastmonth          IN       t4052_ttp_detail.c4052_forecast_period%TYPE,
      p_outmonthcountheader    OUT      TYPES.cursor_type,
      p_outmonthcountdetails   OUT      TYPES.cursor_type
   );

--
 /*******************************************************
   * Description : Procedure to generate header and details section
   * Author     : Xun
   *******************************************************/
--
   PROCEDURE gm_fch_cons_open_set_status (
      p_outsetheader    OUT   TYPES.cursor_type,
      p_outsetdetails   OUT   TYPES.cursor_type
   );

--
   /*******************************************************
   * Description : Procedure to generate Avg Net Consignment count
   * Author     : Xun
   *******************************************************/
--
   PROCEDURE gm_fch_cons_avg_net_count (p_outavgdetails OUT TYPES.cursor_type);

--
   /*******************************************************
   * Description : Procedure to generate  Yet to build (Back Order SQL)
   * Author     : Xun
   *******************************************************/
--
   PROCEDURE gm_fch_cons_yet_to_build_count (
      p_outtobuilddetails   OUT   TYPES.cursor_type
   );

   /*******************************************************
     * Description : Procedure to fetch the shipout history
                  from 2003 and last 4 month info.
     * Author   : Ritesh
     *******************************************************/
   PROCEDURE gm_fch_history_shipout (
      p_out_shipoutheader    OUT   TYPES.cursor_type,
      p_out_shipouthistdtl   OUT   TYPES.cursor_type
   );

   /*******************************************************
     * Description : Procedure to fetch the Owe details
     * Author   : Ritesh
     *******************************************************/
   PROCEDURE gm_fch_owe_details (p_out_owedtl OUT TYPES.cursor_type);

--
 /*******************************************************
   * Description : Procedure to generate Inhosue Loaner, Sales/Cons Loaner section
   * Author     : Xun
   *******************************************************/
--
   PROCEDURE gm_fch_cons_loaner_count (
      p_outloanerdetails   OUT   TYPES.cursor_type
   );

   PROCEDURE gm_fch_set_overview (
      p_setid                  IN       VARCHAR2,
      p_forecastmonth          IN       t4052_ttp_detail.c4052_forecast_period%TYPE,
      p_outmonthcountheader    OUT      TYPES.cursor_type,
      p_outmonthcountdetails   OUT      TYPES.cursor_type,
      p_outsetheader           OUT      TYPES.cursor_type,
      p_outsetdetails          OUT      TYPES.cursor_type,
      p_outloanerdetails       OUT      TYPES.cursor_type,
      --p_outavgdetails        OUT      TYPES.cursor_type,
      p_setpardetails	  	   OUT      TYPES.cursor_type,
      p_outtobuilddetails      OUT      TYPES.cursor_type,
      p_outshipouthistheader   OUT      TYPES.cursor_type,
      p_outshipouthistdetail   OUT      TYPES.cursor_type,
      p_outowedetails          OUT      TYPES.cursor_type
   )
   AS
      v_strlen   NUMBER          := NVL (LENGTH (p_setid), 0);
      v_flag     VARCHAR2 (10);
      v_setids   VARCHAR2 (4000);

      CURSOR v_set_ids
      IS
         SELECT   c207_set_id v_set_id
             FROM t207_set_master
            WHERE c901_set_grp_type = '1601'
              AND c207_void_fl IS NULL
              AND c207_obsolete_fl IS NULL
              AND (   (    c207_set_id =
                                 DECODE (v_flag,
                                         'CHECKED', c207_set_id,
                                         -9999
                                        )
                       AND c207_set_id IN (SELECT *
                                             FROM v_in_list)
                      )
                   OR (    c207_set_id =
                               DECODE (v_flag,
                                       'UNCHECKED', c207_set_id,
                                       -9999
                                      )
                       AND c207_set_id NOT IN (SELECT *
                                                 FROM v_in_list)
                      )
                   OR (c207_set_id = DECODE (v_flag,
                                             'ALL', c207_set_id,
                                             -9999
                                            )
                      )
                  )
         ORDER BY c207_set_id;
   BEGIN
      IF v_strlen <= 0
      THEN
         raise_application_error ('-20887', '');    --'No setId(s) to display
      END IF;

      DELETE FROM my_temp_list;

      v_setids := SUBSTR (p_setid, INSTR (p_setid, '|', 1, 1) + 1);
                                         --list of tagids with comma separated
      v_flag := SUBSTR (p_setid, 1, INSTR (p_setid, '|') - 1);

      IF v_setids IS NOT NULL
      THEN
         my_context.set_my_inlist_ctx (v_setids);
      END IF;

      FOR v_set IN v_set_ids
      LOOP
         INSERT INTO my_temp_list
              VALUES (v_set.v_set_id);
      END LOOP;

      --my_context.set_my_inlist_ctx (v_ids);

      -- Below procedure called to fetch Forecast Month
      gm_fch_cons_forecast_count (p_forecastmonth,
                                  p_outmonthcountheader,
                                  p_outmonthcountdetails
                                 );
      gm_fch_cons_open_set_status (p_outsetheader, p_outsetdetails);
      -- Below code to fecth Inhosue Loaner, Sales/Cons Loaner
      gm_fch_cons_loaner_count (p_outloanerdetails);
      -- Below procedure called to fetch Avg Net Consignment count
      --gm_fch_cons_avg_net_count (p_outavgdetails);
      
      --Below Procedure to calculate the SET PAR
      gm_fch_SET_PAR_count (p_setpardetails);
      -- Below procedure called to fetch Yet to build
      gm_fch_cons_yet_to_build_count (p_outtobuilddetails);
      -- Used to fetch the shipout history from the 2003 and last 4 months from the current date.
      gm_fch_history_shipout (p_outshipouthistheader, p_outshipouthistdetail);
      -- used to fetch the owe details.
      gm_fch_owe_details (p_outowedetails);
   END gm_fch_set_overview;

 /*******************************************************
   * Description : Procedure to generate header and details section
   * Author     : Xun
   *******************************************************/
--
   PROCEDURE gm_fch_cons_open_set_status (
      p_outsetheader    OUT   TYPES.cursor_type,
      p_outsetdetails   OUT   TYPES.cursor_type
   )
   AS
   	  v_plant_id      t5040_plant_master.c5040_plant_id%TYPE;
   BEGIN
	  SELECT  NVL(get_plantid_frm_cntx(),get_rule_value('DEFAULT_PLANT','ONEPORTAL')) INTO  v_plant_id FROM DUAL;
      OPEN p_outsetheader
       FOR
          SELECT   'Initiated', 6 seq
              FROM DUAL
          UNION
          SELECT   'WIP', 5 seq
              FROM DUAL
          UNION
          SELECT   'Pending Verification', 4 seq
              FROM DUAL
          UNION
          SELECT   'Ready To Consign', 3 seq
              FROM DUAL
          UNION
          SELECT   'Pending Shipping', 2 seq
              FROM DUAL
          UNION
          SELECT   'Shipped', 1 seq
              FROM DUAL
          ORDER BY seq;

--=================
--Details
--=================

	/*  
	 * Below Query is to get he count of Consignment set status for the Set Overview Report screen
	 * We have the columns WIP, Pending Verification, Ready to Consign, Pending Shipping and Shipped status in the report
	 */	


      OPEN p_outsetdetails
       FOR
          SELECT   setid, setname, status, SUM (COUNT)
              FROM (SELECT   t504.c207_set_id setid,
                             get_set_name (t504.c207_set_id) setname, T504.C504_STATUS_FL,
                             DECODE (t504.c504_status_fl,
                                     0, 'WIP',                                -- 0 - initiated(consignment status)
                                     1, 'WIP',								  -- 1 - WIP
                                     1.10, 'Pending Verification',			  -- 1.10 - Completed
                                     1.20, 'Ready To Consign',				  -- 1.20 - Pending Putaway
                                     2, DECODE (t504.c504_verify_fl,          -- 2	- Built Set
                                                0, 'Pending Verification',
                                                1, 'Ready To Consign'
                                               ),
                                     2.20, DECODE (t504.c504_verify_fl,       -- 2.20 - Pending Pick
                                                0, 'WIP',
                                                1, 'Ready To Consign'
                                               ),
                                     3, 'Pending Shipping',                   -- 3 - Pending Shipping
                                     3.30, 'Pending Shipping',				  -- 3.30 - Packing in progress
                                     3.60, 'Pending Shipping',				  -- 3.60 - Ready For Pickup
                                     4, 'Shipped'							  -- 4 - Completed
                                    ) status,
                             COUNT (t520.c207_set_id) COUNT
                        FROM t520_request t520, t504_consignment t504
                       WHERE t504.c520_request_id = t520.c520_request_id
                         AND t520.c207_set_id IN (SELECT my_temp_txn_id
                                                    FROM my_temp_list)
                         AND t520.c520_status_fl > 10
                         AND t520.c520_status_fl <= 30
                         AND t520.c520_void_fl IS NULL
                         AND t520.c520_delete_fl IS NULL
                         AND t504.c504_void_fl IS NULL
                         AND t504.c207_set_id IS NOT NULL
                         AND t520.c520_master_request_id IS NULL
                         AND t520.c5040_plant_id = v_plant_id
                    GROUP BY t504.c207_set_id,
                             t504.c504_status_fl,
                             t504.c504_verify_fl
                    UNION
                    SELECT   t504.c207_set_id setid,
                             get_set_name (t504.c207_set_id) setname, T504.C504_STATUS_FL,
                             DECODE (t504.c504_status_fl,
                                     4, 'Shipped'
                                    ) status, COUNT (t520.c207_set_id) COUNT
                        FROM t520_request t520, t504_consignment t504
                       WHERE t504.c520_request_id = t520.c520_request_id
                         AND t520.c207_set_id IN (SELECT my_temp_txn_id
                                                    FROM my_temp_list)
                         AND t520.c520_status_fl = 40
                         AND t504.c504_ship_date BETWEEN TRUNC (CURRENT_DATE, 'MM')
                                                     AND LAST_DAY (CURRENT_DATE)
                         AND t520.c520_void_fl IS NULL
                         AND t520.c520_delete_fl IS NULL
                         AND t504.c504_void_fl IS NULL
                         AND t504.c207_set_id IS NOT NULL
                         AND t520.c520_master_request_id IS NULL
                         AND t520.c5040_plant_id = v_plant_id
                    GROUP BY t504.c207_set_id,
                             t504.c504_status_fl,
                             t504.c504_verify_fl)
          GROUP BY setid, setname, status
          ORDER BY setid;
   END gm_fch_cons_open_set_status;

 /*******************************************************
   * Description : Procedure to generate Inhosue Loaner, Sales/Cons Loaner section
   * Author     : Xun
   *******************************************************/
--
   PROCEDURE gm_fch_cons_loaner_count (p_outloanerdetails OUT TYPES.cursor_type)
   AS
    v_plant_id      t5040_plant_master.c5040_plant_id%TYPE;
   BEGIN
	  SELECT  NVL(get_plantid_frm_cntx(),get_rule_value('DEFAULT_PLANT','ONEPORTAL')) INTO  v_plant_id FROM DUAL;
      OPEN p_outloanerdetails
       FOR
          SELECT   setid ID, SUM (iloaner) inhouse_loaner,
                   SUM (ploaner) product_loaner
              FROM (SELECT t504.c207_set_id setid,
                           DECODE (t504.c504_type, 4119, 1, 0) iloaner,
                           DECODE (t504.c504_type, 4127, 1, 0) ploaner
                      FROM t504_consignment t504,
                           t504a_consignment_loaner t504a
                     WHERE t504.c504_consignment_id =
                                                     t504a.c504_consignment_id
                       AND t504.c504_type IN (4119, 4127)
                       AND t504.c207_set_id IN (SELECT my_temp_txn_id
                                                  FROM my_temp_list)
                       AND t504.c504_void_fl IS NULL
                       AND t504.c207_set_id IS NOT NULL
                       AND t504a.c504a_void_fl IS NULL
                       AND t504a.c504a_status_fl != 60              --inactive
                       AND t504.c5040_plant_id = v_plant_id
                       AND t504.c5040_plant_id = t504a.c5040_plant_id
                                                      )
          GROUP BY setid
          ORDER BY setid;
   END gm_fch_cons_loaner_count;
	
      /*******************************************************
   * Description : Procedure to generate Avg Net Consignment count
   * Author     : Xun
   *******************************************************/
--
   PROCEDURE gm_fch_cons_avg_net_count (p_outavgdetails OUT TYPES.cursor_type)
   AS
   v_plant_id      t5040_plant_master.c5040_plant_id%TYPE;
   BEGIN
	  SELECT  NVL(get_plantid_frm_cntx(),get_rule_value('DEFAULT_PLANT','ONEPORTAL')) INTO  v_plant_id FROM DUAL;
      OPEN p_outavgdetails
       FOR
          SELECT   cons.setid ID,
                   (cons.COUNT - NVL (rtn.COUNT, 0)) / 6 avg_net
              FROM (SELECT   t504.c207_set_id setid,
                             COUNT (t504.c207_set_id) COUNT
                        FROM t504_consignment t504
                       WHERE t504.c207_set_id IN (SELECT my_temp_txn_id
                                                    FROM my_temp_list)
                         AND t504.c504_status_fl = 4
                         AND t504.c504_type = 4110
                         AND t504.c504_ship_date
                                BETWEEN TRUNC (ADD_MONTHS (CURRENT_DATE, -6))
                                    AND LAST_DAY (ADD_MONTHS (CURRENT_DATE, -1))
                         AND t504.c504_void_fl IS NULL
                         AND t504.c5040_plant_id = v_plant_id
                    GROUP BY t504.c207_set_id) cons,
                   (SELECT   t506.c207_set_id setid,
                             COUNT (t506.c207_set_id) COUNT
                        FROM t506_returns t506
                       WHERE t506.c506_status_fl = 2
                         AND t506.c506_void_fl IS NULL
                         AND t506.c506_type IN (3301, 3302, 3306)
                         AND t506.c701_distributor_id IS NOT NULL
                         AND t506.c207_set_id IN (SELECT my_temp_txn_id
                                                    FROM my_temp_list)
                         AND t506.c506_return_date
                                BETWEEN TRUNC (ADD_MONTHS (CURRENT_DATE, -6))
                                    AND LAST_DAY (ADD_MONTHS (CURRENT_DATE, -1))
                         AND t506.c506_void_fl IS NULL
                         AND t506.c5040_plant_id = v_plant_id
                    GROUP BY t506.c207_set_id) rtn
             WHERE cons.setid = rtn.setid(+)
          ORDER BY cons.setid;
   END gm_fch_cons_avg_net_count;

   /*******************************************************
   * Description : Procedure to Get the SET PAR Count
   * Author     : Rajeshwaran
   *******************************************************/
--
   PROCEDURE gm_fch_SET_PAR_count (p_setpardetails OUT TYPES.cursor_type)
   AS
   BEGIN
      OPEN p_setpardetails
       FOR
        SELECT   t207.c207_set_id ID,
                   t207.c207_set_id || ' - ' || t207.c207_set_nm setname,
                   NVL (DEMAND.COUNT, 0) set_par
              FROM t207_set_master t207,
                   (select SET_ID ID,
		       			period mon, 
		       			SUM (qty) COUNT from (
						SELECT   t4061.C207_SET_ID SET_ID,
		               		 TO_CHAR (t4061.C4061_PERIOD, 'Mon-YY') period, 
		               		 t4061.C4061_QTY QTY
				        ,ROW_NUMBER () OVER (PARTITION BY t4061.C207_SET_ID ORDER BY t4061.C4061_PERIOD desc) row_num
			          	FROM T4061_SET_FORECAST_QTY t4061 
			         	WHERE t4061.C207_SET_ID IN (SELECT my_temp_txn_id
	                                                      FROM my_temp_list)
						AND C901_FORECAST_TYPE IN (4000113) --SET PAR
                      ) WHERE row_num=1
			      GROUP BY SET_ID,period) DEMAND
             WHERE t207.c207_set_id IN (SELECT my_temp_txn_id
                                          FROM my_temp_list)
               AND t207.c207_set_id = DEMAND.ID(+)
               AND t207.c207_void_fl IS NULL
          ORDER BY t207.c207_set_id;     
      
   END gm_fch_SET_PAR_count;

   /*******************************************************
   * Description : Procedure to generate  Yet to build (Back Order SQL)
   * Author     : Xun
   *******************************************************/
--
   PROCEDURE gm_fch_cons_yet_to_build_count (
      p_outtobuilddetails   OUT   TYPES.cursor_type
   )
   AS
   v_plant_id      t5040_plant_master.c5040_plant_id%TYPE;
   BEGIN
	  SELECT  NVL(get_plantid_frm_cntx(),get_rule_value('DEFAULT_PLANT','ONEPORTAL')) INTO  v_plant_id FROM DUAL;
      OPEN p_outtobuilddetails
       FOR
          SELECT   t520.c207_set_id ID, COUNT (t520.c207_set_id)
                                                                yet_to_build
              FROM t520_request t520
             WHERE c520_status_fl = 10
               AND c520_master_request_id IS NULL
               AND t520.c207_set_id IN (SELECT my_temp_txn_id
                                          FROM my_temp_list)
               AND t520.c520_void_fl IS NULL
               AND t520.c520_delete_fl IS NULL
               AND t520.c207_set_id IS NOT NULL
               AND t520.c5040_plant_id = v_plant_id
               AND t520.c520_required_date >=TRUNC(CURRENT_DATE,'MON')
          GROUP BY t520.c207_set_id
          ORDER BY t520.c207_set_id;
   END gm_fch_cons_yet_to_build_count;

   /*******************************************************
   * Description : Procedure to generate  Yet to build (Back Order SQL)
   * Author     : Xun
   *******************************************************/
--
   PROCEDURE gm_fch_cons_forecast_count (
      p_forecastmonth          IN       t4052_ttp_detail.c4052_forecast_period%TYPE,
      p_outmonthcountheader    OUT      TYPES.cursor_type,
      p_outmonthcountdetails   OUT      TYPES.cursor_type
   )
   AS
   BEGIN
      OPEN p_outmonthcountheader
       FOR
          SELECT to_char(month_value,'Mon-YY')mon 
          FROM v9001_month_list 
          WHERE month_value  BETWEEN TRUNC (CURRENT_DATE, 'MONTH') AND 
          							LAST_DAY(ADD_MONTHS(CURRENT_DATE,p_forecastmonth-1))
			order by month_value
			;
      
      OPEN p_outmonthcountdetails
       FOR    
	      SELECT   t207.c207_set_id ID,
                   t207.c207_set_id || ' - ' || t207.c207_set_nm setname,
                   NVL (DEMAND.mon, TO_CHAR (CURRENT_DATE, 'Mon-YY')) mon,
                   NVL (DEMAND.COUNT, 0) COUNT
              FROM t207_set_master t207,
                   (SELECT   t4061.C207_SET_ID ID,
		               		 TO_CHAR (t4061.C4061_PERIOD, 'Mon-YY') mon, 
		               		 SUM (t4061.C4061_QTY) COUNT
			          FROM T4061_SET_FORECAST_QTY t4061 
			         WHERE t4061.C207_SET_ID IN (SELECT my_temp_txn_id
	                                                      FROM my_temp_list)
	                  AND C901_FORECAST_TYPE IN (40021,40022) -- Consignment, In-House
	                  AND t4061.c4061_period BETWEEN TRUNC (CURRENT_DATE,
	                                                                   'MONTH'
	                                                                  )
	                                                        AND LAST_DAY
	                                                              (ADD_MONTHS
	                                                                  (CURRENT_DATE,
	                                                                     p_forecastmonth
	                                                                   - 1
	                                                                  )
	                                                              )
			      GROUP BY t4061.C207_SET_ID, t4061.c4061_period
		      ORDER BY t4061.C207_SET_ID) DEMAND
             WHERE t207.c207_set_id IN (SELECT my_temp_txn_id
                                          FROM my_temp_list)
               AND t207.c207_set_id = DEMAND.ID(+)
               AND t207.c207_void_fl IS NULL
          ORDER BY t207.c207_set_id;     
          
   END gm_fch_cons_forecast_count;

--
--
  /*******************************************************************
   * Description : Procedure to fetch the part fulfill report
   * Author     : Prasath
   *******************************************************************/
--
   PROCEDURE gm_fch_part_fulfill (
      p_setstr       IN       VARCHAR2,
      p_partstr      IN       VARCHAR2,
      p_type         IN       VARCHAR2,
      p_report_out   OUT      TYPES.cursor_type
   )
   AS
      v_strlen    NUMBER          := NVL (LENGTH (p_setstr), 0);
      v_partlen   NUMBER          := NVL (LENGTH (p_partstr), 0);
      v_flag      VARCHAR2 (10);
      v_setids    VARCHAR2 (4000);
      v_part      VARCHAR2 (4000) := p_partstr;
      v_set_fl BOOLEAN := FALSE;
      v_plant_id      t5040_plant_master.c5040_plant_id%TYPE;
      v_company_id 	  t1900_company.c1900_company_id%TYPE;
		
      CURSOR v_set_ids
      IS
         SELECT   t207.c207_set_id v_set_id
             FROM t207_set_master t207, t2080_set_company_mapping t2080
            WHERE c901_set_grp_type = '1601'
              AND t207.c207_set_id = t2080.c207_set_id
              AND t2080.c1900_company_id = v_company_id
              AND t2080.c2080_void_fl IS NULL
              AND t207.c207_void_fl IS NULL
              AND t207.c207_obsolete_fl IS NULL
              AND (   (    t207.c207_set_id =
                                 DECODE (v_flag,
                                         'CHECKED', t207.c207_set_id,
                                         -9999
                                        )
                       AND t207.c207_set_id IN (SELECT *
                                             FROM v_in_list)
                      )
                   OR (    t207.c207_set_id =
                               DECODE (v_flag,
                                       'UNCHECKED', t207.c207_set_id,
                                       -9999
                                      )
                       AND t207.c207_set_id NOT IN (SELECT *
                                                 FROM v_in_list)
                      )
                   OR (t207.c207_set_id = DECODE (v_flag,
                                             'ALL', t207.c207_set_id,
                                             -9999
                                            )
                      )
                  )
         ORDER BY t207.c207_set_id;
   BEGIN
      SELECT  NVL(get_plantid_frm_cntx(),get_rule_value('DEFAULT_PLANT','ONEPORTAL')) INTO  v_plant_id FROM DUAL;
      --
      SELECT  NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL')) INTO  v_company_id FROM DUAL;
      --
      IF v_strlen <= 0 AND v_partlen <= 0
      THEN
         raise_application_error ('-20887', '');    --'No setId(s) to display
      END IF;

      DELETE FROM my_temp_part_list;

      DELETE FROM my_temp_list;
      -- remove the set ids
      my_context.set_double_inlist_ctx ('');
      my_context.set_my_inlist_ctx ('');

      v_setids := SUBSTR (p_setstr, INSTR (p_setstr, '|', 1, 1) + 1);
                                         --list of tagids with comma separated
      v_flag := SUBSTR (p_setstr, 1, INSTR (p_setstr, '|') - 1);

      IF v_setids IS NOT NULL
      THEN
         my_context.set_my_inlist_ctx (v_setids);
      END IF;

      IF v_part IS NOT NULL
      THEN
         my_context.set_double_inlist_ctx (v_part);
      END IF;

      FOR v_set IN v_set_ids
      LOOP
         INSERT INTO my_temp_list
              VALUES (v_set.v_set_id);
          v_set_fl := TRUE;    
      END LOOP;
      --
	 IF v_part IS NOT NULL AND (v_setids IS NOT NULL OR v_set_fl = TRUE)
	 THEN
	     SELECT RTRIM (XMLAGG (XMLELEMENT (e, 'PART,'||tokenii || '|')) .EXTRACT ('//text()') .getclobval (), ',')
	       INTO v_part
	       FROM v_double_in_list
	      WHERE token    = 'PART'
	        AND tokenii IN
	        (
	             SELECT t208.c205_part_number_id
	               FROM t208_set_details t208, my_temp_list my
	              WHERE t208.c207_set_id IN (my.my_temp_txn_id)
	        ) ;
	     -- remove the parts and sets   
	     DELETE FROM my_temp_list;
	     my_context.set_double_inlist_ctx ('') ;
	END IF;
      --

      IF v_part IS NOT NULL
      THEN
         my_context.set_double_inlist_ctx (v_part);
      END IF;
      
      IF p_type = '50242' OR p_type = '50243'
      THEN
         IF p_type = '50242'                                          -- ITEM
         THEN
            INSERT INTO my_temp_part_list
                        (c205_part_number_id, c205_qty)
               SELECT   t521.c205_part_number_id,
                        (NVL (SUM (t521.c521_qty), 0))
                   FROM t520_request t520, t521_request_detail t521
                  WHERE t520.c520_request_id = t521.c520_request_id
                    AND t520.c520_void_fl IS NULL
                    AND t520.c520_delete_fl IS NULL
                    AND t520.c520_status_fl = 10
                    AND t520.c207_set_id IS NULL
                    AND t520.c5040_plant_id = v_plant_id
                    AND t520.c520_request_to IS NOT NULL
                    AND (   t520.c520_master_request_id IS NULL
                         OR t520.c520_master_request_id IN (
                                                     SELECT c520_request_id
                                                       FROM t520_request
                                                      WHERE c207_set_id IS NULL)
                        )
                    AND t521.c205_part_number_id IN (
                           SELECT t208.c205_part_number_id pnum
                             FROM t208_set_details t208, my_temp_list my
                            WHERE t208.c207_set_id IN (my.my_temp_txn_id)
                           UNION
                           SELECT tokenii pnum
                             FROM v_double_in_list
                            WHERE token = 'PART')
               GROUP BY t521.c205_part_number_id;
         ELSIF p_type = '50243'                                              --
         THEN
            INSERT INTO my_temp_part_list
                        (c205_part_number_id, c205_qty)
               SELECT   mainq.pnum pnum, SUM (mainq.qty) qty
                   FROM (SELECT   t521.c205_part_number_id pnum,
                                  (NVL (SUM (t521.c521_qty), 0)) qty
                             FROM t520_request t520, t521_request_detail t521
                            WHERE t520.c520_request_id = t521.c520_request_id
                              AND t520.c520_void_fl IS NULL
                              AND t520.c520_delete_fl IS NULL
                              AND t520.c207_set_id IS NULL
                              AND t520.c520_request_to IS NOT NULL
                              AND t520.c5040_plant_id = v_plant_id
                              AND (   t520.c520_master_request_id IS NULL
                                   OR t520.c520_master_request_id IN (
                                                     SELECT c520_request_id
                                                       FROM t520_request
                                                      WHERE c207_set_id IS NULL)
                                  )
                              AND t521.c205_part_number_id IN (
                                     SELECT t208.c205_part_number_id pnum
                                       FROM t208_set_details t208,
                                            my_temp_list my
                                      WHERE t208.c207_set_id IN
                                                          (my.my_temp_txn_id)
                                     UNION
                                     SELECT tokenii pnum
                                       FROM v_double_in_list
                                      WHERE token = 'PART')
                         GROUP BY t521.c205_part_number_id
                         UNION
                         SELECT t205.c205_part_number_id pnum, 0 qty
                           FROM t205_part_number t205
                          WHERE t205.c205_part_number_id IN (
                                                         SELECT tokenii pnum
                                                           FROM v_double_in_list
                                                          WHERE token = 'PART')) mainq
               GROUP BY mainq.pnum;
         END IF;
		  OPEN p_report_out
          FOR
             SELECT   pnum, description, NVL (bo_qty, 0) bo_qty,
                      NVL (shelf, 0) shelf, NVL (bulkqty, 0) bulkqty,
                      NVL (wip, 0) wip, NVL (dhr, 0) dhr,
                      ROUND (NVL (wgtd_avg, 0), 2) wgtd_avg,
                      NVL (set_need, 0) set_need,
                      ROUND (  (  NVL (shelf, 0)
                                + NVL (bulkqty, 0)
                                + NVL (wip, 0)
                                + NVL (dhr, 0)
								+ NVL (rwqty, 0)
                               )
                             - (NVL (wgtd_avg, 0) + NVL (set_need, 0)),
                             2
                            ) fulfill_qty,
                      NVL (allocin, 0) allocatedin,
                      NVL (pendpo_qty, 0) pendpoqty,
                      NVL(rwqty,0) rwqty
                 FROM (SELECT v205.c205_part_number_id pnum,
                              v205.c205_part_num_desc description,
                              mylist.c205_qty bo_qty, v205.c205_inshelf shelf,
                              v205.c205_inbulk bulkqty,
                              v205.c205_in_wip_set wip,
                              v205.c205_inv_alloc_to_pack allocin,
                              v205.c205_popend pendpo_qty,
							  v205.c205_rw_avail_qty rwqty
                         FROM my_temp_part_list mylist,
                              v205_qty_in_stock v205
                        WHERE mylist.c205_part_number_id =
                                                      v205.c205_part_number_id) mainsql,
                      (SELECT   t408.c205_part_number_id,
                                  NVL (SUM (t408.c408_shipped_qty), 0)
                                - NVL (SUM (t408.c408_qty_rejected), 0)
                                + NVL (SUM (t409.c409_closeout_qty), 0) dhr
                           FROM t408_dhr t408,
                                t409_ncmr t409,
                                my_temp_part_list mylist
                          WHERE t408.c408_dhr_id = t409.c408_dhr_id(+) 
                            AND t409.c409_void_fl IS NULL
                            AND t408.c408_status_fl < 4
                            AND t408.c205_part_number_id =
                                                    mylist.c205_part_number_id
                            AND t408.c5040_plant_id = t409.c5040_plant_id(+)
                            AND t408.c5040_plant_id = v_plant_id
                            AND t408.c408_void_fl IS NULL
                       GROUP BY t408.c205_part_number_id) powip,
                     /* (SELECT   t521.c205_part_number_id,
                                NVL (SUM (t521.c521_qty), 0) set_need
                           FROM t520_request t520,
                                t521_request_detail t521,
                                my_temp_part_list mylist
                          WHERE t520.c520_request_id = t521.c520_request_id
                            AND t520.c520_void_fl IS NULL
                            AND t520.c520_delete_fl IS NULL
                            AND t520.c520_status_fl = 10
                            AND t521.c205_part_number_id =
                                                    mylist.c205_part_number_id
                            AND t520.c207_set_id IS NOT NULL
                       GROUP BY t521.c205_part_number_id) setneed,
                       */
                       (SELECT   t4042.c205_part_number_id,
                                     SUM (c4042_qty) set_need
                                FROM t4040_demand_sheet t4040,
                                     t4042_demand_sheet_detail t4042,
                                     my_temp_part_list mylist,
                                     (SELECT MAX
                                                (c250_inventory_lock_id
                                                ) c250_inventory_lock_id
                                        FROM t250_inventory_lock t250
                                       WHERE c901_lock_type = 20430
                                         AND c250_void_fl IS NULL) t250
                               WHERE t4040.c250_inventory_lock_id =
                                                   t250.c250_inventory_lock_id
                                 AND t4040.c4040_demand_sheet_id =
                                                   t4042.c4040_demand_sheet_id
                                 AND t4042.c205_part_number_id =
                                                    mylist.c205_part_number_id
                                 AND t4040.c901_demand_type NOT IN
                                                               (40020, 40023)
                                 AND t4042.c4042_period BETWEEN CURRENT_DATE
                                                            AND ADD_MONTHS
                                                                     (CURRENT_DATE,
                                                                      2
                                                                     )
                                 AND t4040.c4040_void_fl IS NULL
                                 and t4040.c4020_demand_master_id in (
                                 SELECT c4020_demand_master_id
								  FROM t4020_demand_master
								  WHERE c4020_demand_master_id  IN
								    (SELECT c9352_ref_id
								    FROM t9352_hierarchy_flow
								    WHERE c901_ref_type   ='60260'
								    AND c9350_hierarchy_id in 
								    (select c906_rule_value from t906_rules where c906_rule_id='DMD_SHT_RGN' and c906_rule_grp_id='US_DMD_SHT')
								    )
								  AND c4020_inactive_fl IS NULL)                                 
                            GROUP BY t4042.c205_part_number_id) setneed,
                      (SELECT   c205_part_number_id,
                                --NVL (SUM (qty * mul_value) / 3, 0) wgtd_avg
                                NVL (SUM (qty * mul_value) / sum(mul_value), 0) wgtd_avg
                           FROM (SELECT   t502.c205_part_number_id,
                                          TO_CHAR (t501.c501_order_date,
                                                   --'Mon YY'
                                                   'YYMM'
                                                  ),
                                          SUM (t502.c502_item_qty) qty,
                                          ROW_NUMBER () OVER (PARTITION BY t502.c205_part_number_id ORDER BY TO_CHAR
                                                        (t501.c501_order_date,
                                                        --'Mon YY'
                                                        'YYMM'
                                                        )) mul_value
                                     FROM t501_order t501,
                                          t502_item_order t502,
                                          my_temp_part_list mylist
                                    WHERE t501.c501_order_id =
                                                            t502.c501_order_id
                                      AND NVL (t501.c901_order_type, 0) NOT IN
                                                                       (2524,2533) -- Price Adjustment,ICS Order
                                      AND NVL (c901_order_type, -9999) NOT IN (
								                SELECT t906.c906_rule_value
								                  FROM t906_rules t906
								                 WHERE t906.c906_rule_grp_id = 'ORDTYPE'
								                   AND c906_rule_id = 'EXCLUDE')                                 
                                      AND t501.c501_delete_fl IS NULL
                                      AND t501.c5040_plant_id = v_plant_id
                                      AND t501.c501_void_fl IS NULL
                                      AND (t501.c901_ext_country_id IS NULL OR t501.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH
                                      AND t502.c205_part_number_id =
                                                    mylist.c205_part_number_id
                                      AND t501.c501_order_date
                                             BETWEEN TRUNC
                                                        (ADD_MONTHS (CURRENT_DATE,
                                                                     -3
                                                                    ),
                                                         'MM'
                                                        )
                                                 AND TRUNC
                                                       (LAST_DAY
                                                           (ADD_MONTHS
                                                                     (CURRENT_DATE,
                                                                      -1
                                                                     )
                                                           )
                                                       )
                                 GROUP BY t502.c205_part_number_id,
                                          TO_CHAR (t501.c501_order_date,
                                                   --'Mon YY'
                                                   'YYMM'
                                                  )order by  TO_NUMBER(TO_CHAR(t501.c501_order_date,
                                                   'YYMM'
                                                   )))
								

                       GROUP BY c205_part_number_id) wghtd_avd_in_sales
                WHERE mainsql.pnum = powip.c205_part_number_id(+)
                  AND mainsql.pnum = setneed.c205_part_number_id(+)
                  AND mainsql.pnum = wghtd_avd_in_sales.c205_part_number_id(+)
             ORDER BY pnum, description;
      ELSIF p_type = '50241'                                       -- SET NEED
      THEN
         INSERT INTO my_temp_part_list
                     (c205_part_number_id, c205_qty)
            SELECT   t521.c205_part_number_id,
                     (NVL (SUM (t521.c521_qty), 0))
                FROM t520_request t520, t521_request_detail t521
               WHERE t520.c520_request_id = t521.c520_request_id
                 AND t520.c520_void_fl IS NULL
                 AND t520.c520_delete_fl IS NULL
                 AND t520.c5040_plant_id = v_plant_id
                 AND t520.c520_status_fl = 10
                 -- AND t520.c207_set_id IS NOT NULL
                 AND t520.c520_request_to IS NOT NULL
                 AND (   t520.c520_master_request_id IS NULL
                      OR t520.c520_master_request_id IN (
                                                     SELECT c520_request_id
                                                       FROM t520_request
                                                      WHERE c520_status_fl = 40
                                                          AND c207_set_id IS NOT NULL)
                     )
                 AND t521.c205_part_number_id IN (
                        SELECT t208.c205_part_number_id pnum
                          FROM t208_set_details t208, my_temp_list my
                         WHERE t208.c207_set_id IN (my.my_temp_txn_id)
                        UNION
                        SELECT tokenii pnum
                          FROM v_double_in_list
                         WHERE token = 'PART')
            GROUP BY t521.c205_part_number_id;

         OPEN p_report_out
          FOR
             SELECT   pnum, description, NVL (bo_qty, 0) bo_qty,
                      NVL (shelf, 0) shelf, NVL (bulkqty, 0) bulkqty,
                      NVL (wip, 0) wip, NVL (dhr, 0) dhr,
                      ROUND (NVL (wgtd_avg, 0), 2) wgtd_avg,
                      NVL (set_need, 0) set_need,
                      ROUND (  (  NVL (shelf, 0)
                                + NVL (bulkqty, 0)
                                + NVL (wip, 0)
                                + NVL (dhr, 0)
                                + NVL (rwqty, 0)
                               )
                             - NVL (wgtd_avg, 0),
                             2
                            ) fulfill_qty,
                      NVL (allocin, 0) allocatedin,
                      NVL (pendpo_qty, 0) pendpoqty,
                      NVL(rwqty,0) rwqty
                 FROM (SELECT v205.c205_part_number_id pnum,
                              v205.c205_part_num_desc description,
                              mylist.c205_qty bo_qty, v205.c205_inshelf shelf,
                              v205.c205_inbulk bulkqty,
                              v205.c205_in_wip_set wip,
                              v205.c205_inv_alloc_to_pack allocin,
                              v205.c205_popend pendpo_qty,
                              v205.c205_rw_avail_qty rwqty
                         FROM my_temp_part_list mylist,
                              v205_qty_in_stock v205
                        WHERE mylist.c205_part_number_id =
                                                      v205.c205_part_number_id) mainsql,
                      (SELECT   t408.c205_part_number_id,
                                  NVL (SUM (t408.c408_shipped_qty), 0)
                                - NVL (SUM (t408.c408_qty_rejected), 0)
                                + NVL (SUM (t409.c409_closeout_qty), 0) dhr
                           FROM t408_dhr t408,
                                t409_ncmr t409,
                                my_temp_part_list mylist
                          WHERE t408.c408_dhr_id = t409.c408_dhr_id(+) 
                            AND t409.c409_void_fl IS NULL
                            AND t408.c408_status_fl < 4
                            AND t408.c205_part_number_id =
                                                    mylist.c205_part_number_id
                            AND t408.c5040_plant_id = t409.c5040_plant_id(+)
                            AND t408.c5040_plant_id = v_plant_id                        
                            AND t408.c408_void_fl IS NULL
                       GROUP BY t408.c205_part_number_id) powip,
                      /*(SELECT   t521.c205_part_number_id,
                                NVL (SUM (t521.c521_qty), 0) set_need
                           FROM t520_request t520,
                                t521_request_detail t521,
                                my_temp_part_list mylist
                          WHERE t520.c520_request_id = t521.c520_request_id
                            AND t520.c520_void_fl IS NULL
                            AND t520.c520_delete_fl IS NULL
                            AND t520.c520_status_fl = 10
                            AND t521.c205_part_number_id =
                                                    mylist.c205_part_number_id
                            AND t520.c207_set_id IS NOT NULL
                       GROUP BY t521.c205_part_number_id) setneed,
                       */
                      (SELECT   t4042.c205_part_number_id,
                                     SUM (c4042_qty) set_need
                                FROM t4040_demand_sheet t4040,
                                     t4042_demand_sheet_detail t4042,
                                     my_temp_part_list mylist,
                                     (SELECT MAX
                                                (c250_inventory_lock_id
                                                ) c250_inventory_lock_id
                                        FROM t250_inventory_lock t250
                                       WHERE c901_lock_type = 20430
                                         AND c250_void_fl IS NULL) t250
                               WHERE t4040.c250_inventory_lock_id =
                                                   t250.c250_inventory_lock_id
                                 AND t4040.c4040_demand_sheet_id =
                                                   t4042.c4040_demand_sheet_id
                                 AND t4042.c205_part_number_id =
                                                    mylist.c205_part_number_id
                                 AND t4040.c901_demand_type NOT IN
                                                               (40020, 40023)
                                 AND t4042.c4042_period BETWEEN CURRENT_DATE
                                                            AND ADD_MONTHS
                                                                     (CURRENT_DATE,
                                                                      2
                                                                     )
                                 AND t4040.c4040_void_fl IS NULL
                                 and t4040.c4020_demand_master_id in (
                                 SELECT c4020_demand_master_id
								  FROM t4020_demand_master
								  WHERE c4020_demand_master_id  IN
								    (SELECT c9352_ref_id
								    FROM t9352_hierarchy_flow
								    WHERE c901_ref_type   ='60260'
								    AND c9350_hierarchy_id in 
								    (select c906_rule_value from t906_rules where c906_rule_id='DMD_SHT_RGN' and c906_rule_grp_id='US_DMD_SHT')
								    )
								  AND c4020_inactive_fl IS NULL)
                                 
                            GROUP BY t4042.c205_part_number_id) setneed, 
                      (SELECT   c205_part_number_id,
                                --NVL (SUM (qty * mul_value) / 3, 0) wgtd_avg
                                NVL (SUM (qty * mul_value) / sum(mul_value), 0) wgtd_avg
                           FROM (SELECT   t502.c205_part_number_id,
                                          TO_CHAR (t501.c501_order_date,
                                                   --'Mon YY'
                                                   'YYMM'
                                                  ),
                                          SUM (t502.c502_item_qty) qty,
                                          ROW_NUMBER () OVER (PARTITION BY t502.c205_part_number_id ORDER BY TO_CHAR
                                                        (t501.c501_order_date,
                                                         --'Mon YY'
                                                         'YYMM'
                                                        )) mul_value
                                     FROM t501_order t501,
                                          t502_item_order t502,
                                          my_temp_part_list mylist
                                    WHERE t501.c501_order_id =
                                                            t502.c501_order_id
                                      AND NVL (t501.c901_order_type, 0) NOT IN
                                                                       (2524,2533) -- Price Adjustment,ICS Order
                                      AND NVL (c901_order_type, -9999) NOT IN (
							                SELECT t906.c906_rule_value
							                  FROM t906_rules t906
							                 WHERE t906.c906_rule_grp_id = 'ORDTYPE'
							                   AND c906_rule_id = 'EXCLUDE')                                 
                                      AND t501.c501_delete_fl IS NULL
                                      AND t501.c501_void_fl IS NULL
                                      AND t501.c5040_plant_id = v_plant_id
								      AND (t501.c901_ext_country_id IS NULL OR t501.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH
                                      AND t502.c205_part_number_id =
                                                    mylist.c205_part_number_id
                                      AND t501.c501_order_date
                                             BETWEEN TRUNC
                                                        (ADD_MONTHS (CURRENT_DATE,
                                                                     -3
                                                                    ),
                                                         'MM'
                                                        )
                                                 AND TRUNC
                                                       (LAST_DAY
                                                           (ADD_MONTHS
                                                                     (CURRENT_DATE,
                                                                      -1
                                                                     )
                                                           )
                                                       )
                                 GROUP BY t502.c205_part_number_id,
                                          TO_CHAR (t501.c501_order_date,
                                                   --'Mon YY'
                                                   'YYMM'
                                                  )order by  TO_NUMBER(TO_CHAR(t501.c501_order_date,
                                                   'YYMM'
                                                   )))
                       GROUP BY c205_part_number_id) wghtd_avd_in_sales
                WHERE mainsql.pnum = powip.c205_part_number_id(+)
                  AND mainsql.pnum = setneed.c205_part_number_id(+)
                  AND mainsql.pnum = wghtd_avd_in_sales.c205_part_number_id(+)
             ORDER BY pnum, description;
      ELSIF p_type = '50240'                                          -- SALES
      THEN
         INSERT INTO my_temp_part_list
                     (c205_part_number_id, c205_qty)
            SELECT   t502.c205_part_number_id,
                     (NVL (SUM (t502.c502_item_qty), 0))
                FROM t501_order t501, t502_item_order t502
               WHERE t501.c501_order_id = t502.c501_order_id
                 AND t501.c901_order_type = 2525
                 AND t501.c501_delete_fl IS NULL
                 AND t501.c5040_plant_id = v_plant_id
                 AND t501.c501_void_fl IS NULL
		         AND (t501.c901_ext_country_id IS NULL OR t501.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH
                 AND t501.c501_status_fl = 0
                 AND (   t502.c205_part_number_id IN (SELECT tokenii
                                                        FROM v_double_in_list
                                                       WHERE token = 'PART')
                      OR t502.c205_part_number_id IN (
                            SELECT t208.c205_part_number_id
                              FROM t207_set_master t207,
                                   t208_set_details t208
                             WHERE t207.c207_set_id = t208.c207_set_id
                               AND t207.c207_set_id IN (SELECT my_temp_txn_id
                                                          FROM my_temp_list))
                     )
            GROUP BY t502.c205_part_number_id;

         OPEN p_report_out
          FOR
             SELECT   pnum, description, NVL (bo_qty, 0) bo_qty,
                      NVL (shelf, 0) shelf, NVL (bulkqty, 0) bulkqty,
                      NVL (wip, 0) wip, NVL (dhr, 0) dhr,
                      ROUND (NVL (wgtd_avg, 0), 2) wgtd_avg,
                      NVL (set_need, 0) set_need,
                      ROUND ((  NVL (shelf, 0)
                              + NVL (bulkqty, 0)
                              + NVL (wip, 0)
                              + NVL (dhr, 0)
                              + NVL (rwqty,0)
                             ),
                             2
                            ) fulfill_qty,
                      NVL (allocin, 0) allocatedin,
                      NVL (pendpo_qty, 0) pendpoqty,
                      NVL(rwqty,0) rwqty
                 FROM (SELECT v205.c205_part_number_id pnum,
                              v205.c205_part_num_desc description,
                              mylist.c205_qty bo_qty, v205.c205_inshelf shelf,
                              v205.c205_inbulk bulkqty,
                              v205.c205_in_wip_set wip,
                              v205.c205_inv_alloc_to_pack allocin,
                              v205.c205_popend pendpo_qty,
                              v205.c205_rw_avail_qty rwqty
                         FROM my_temp_part_list mylist,
                              v205_qty_in_stock v205
                        WHERE mylist.c205_part_number_id =
                                                      v205.c205_part_number_id) mainsql,
                      (SELECT   t408.c205_part_number_id,
                                  NVL (SUM (t408.c408_shipped_qty), 0)
                                - NVL (SUM (t408.c408_qty_rejected), 0)
                                + NVL (SUM (t409.c409_closeout_qty), 0) dhr
                           FROM t408_dhr t408,
                                t409_ncmr t409,
                                my_temp_part_list mylist
                          WHERE t408.c408_dhr_id = t409.c408_dhr_id(+) 
                            AND t409.c409_void_fl IS NULL
                            AND t408.c408_status_fl < 4
                            AND t408.c205_part_number_id =
                                                    mylist.c205_part_number_id
                            AND t408.c5040_plant_id = t409.c5040_plant_id(+)
                            AND t408.c5040_plant_id = v_plant_id                       
                            AND t408.c408_void_fl IS NULL
                       GROUP BY t408.c205_part_number_id) powip,
                      /*(SELECT   t521.c205_part_number_id,
                                NVL (SUM (t521.c521_qty), 0) set_need
                           FROM t520_request t520,
                                t521_request_detail t521,
                                my_temp_part_list mylist
                          WHERE t520.c520_request_id = t521.c520_request_id
                            AND t520.c520_void_fl IS NULL
                            AND t520.c520_delete_fl IS NULL
                            AND t520.c520_status_fl = 10
                            AND t521.c205_part_number_id =
                                                    mylist.c205_part_number_id
                            AND t520.c207_set_id IS NOT NULL
                       GROUP BY t521.c205_part_number_id) setneed,
                       */
						(SELECT   t4042.c205_part_number_id,
                                     SUM (c4042_qty) set_need
                                FROM t4040_demand_sheet t4040,
                                     t4042_demand_sheet_detail t4042,
                                     my_temp_part_list mylist,
                                     (SELECT MAX
                                                (c250_inventory_lock_id
                                                ) c250_inventory_lock_id
                                        FROM t250_inventory_lock t250
                                       WHERE c901_lock_type = 20430
                                         AND c250_void_fl IS NULL) t250
                               WHERE t4040.c250_inventory_lock_id =
                                                   t250.c250_inventory_lock_id
                                 AND t4040.c4040_demand_sheet_id =
                                                   t4042.c4040_demand_sheet_id
                                 AND t4042.c205_part_number_id =
                                                    mylist.c205_part_number_id
                                 AND t4040.c901_demand_type NOT IN
                                                               (40020, 40023)
                                 AND t4042.c4042_period BETWEEN CURRENT_DATE
                                                            AND ADD_MONTHS
                                                                     (CURRENT_DATE,
                                                                      2
                                                                     )
                                 AND t4040.c4040_void_fl IS NULL
                                 and t4040.c4020_demand_master_id in (
                                 SELECT c4020_demand_master_id
								  FROM t4020_demand_master
								  WHERE c4020_demand_master_id  IN
								    (SELECT c9352_ref_id
								    FROM t9352_hierarchy_flow
								    WHERE c901_ref_type   ='60260'
								    AND c9350_hierarchy_id in 
								    (select c906_rule_value from t906_rules where c906_rule_id='DMD_SHT_RGN' and c906_rule_grp_id='US_DMD_SHT')
								    )
								  AND c4020_inactive_fl IS NULL)
                                 
                            GROUP BY t4042.c205_part_number_id) setneed,                       
                      (SELECT   c205_part_number_id,
                                --NVL (SUM (qty * mul_value) / 3, 0) wgtd_avg
                                NVL (SUM (qty * mul_value) / sum(mul_value), 0) wgtd_avg
                           FROM (SELECT   t502.c205_part_number_id,
                                          TO_CHAR (t501.c501_order_date,
                                                   --'Mon YY'
                                                   'YYMM'
                                                  ),
                                          SUM (t502.c502_item_qty) qty,
                                          ROW_NUMBER () OVER (PARTITION BY t502.c205_part_number_id ORDER BY TO_CHAR
                                                        (t501.c501_order_date,
                                                         --'Mon YY'
                                                         'YYMM'
                                                        )) mul_value
                                     FROM t501_order t501,
                                          t502_item_order t502,
                                          my_temp_part_list mylist
                                    WHERE t501.c501_order_id =
                                                            t502.c501_order_id
                                      AND NVL (t501.c901_order_type, 0) NOT IN
                                                                       (2524,2533) -- Price Adjustment,ICS Order
                                      AND NVL (c901_order_type, -9999) NOT IN (
							                SELECT t906.c906_rule_value
							                  FROM t906_rules t906
							                 WHERE t906.c906_rule_grp_id = 'ORDTYPE'
							                   AND c906_rule_id = 'EXCLUDE')                                 
                                      AND t501.c501_delete_fl IS NULL
                                      AND t501.c501_void_fl IS NULL
                                      AND t501.c5040_plant_id = v_plant_id
				                      AND (t501.c901_ext_country_id IS NULL OR t501.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH
                                      AND t502.c205_part_number_id =
                                                    mylist.c205_part_number_id
                                      AND t501.c501_order_date
                                             BETWEEN TRUNC
                                                        (ADD_MONTHS (CURRENT_DATE,
                                                                     -3
                                                                    ),
                                                         'MM'
                                                        )
                                                 AND TRUNC
                                                       (LAST_DAY
                                                           (ADD_MONTHS
                                                                     (CURRENT_DATE,
                                                                      -1
                                                                     )
                                                           )
                                                       )
                                 GROUP BY t502.c205_part_number_id,
                                          TO_CHAR (t501.c501_order_date,
                                                   --'Mon YY'
                                                   'YYMM'
                                                  )order by  TO_NUMBER(TO_CHAR(t501.c501_order_date,
                                                   'YYMM'
                                                   )))
                                                  
                       GROUP BY c205_part_number_id) wghtd_avd_in_sales
                WHERE mainsql.pnum = powip.c205_part_number_id(+)
                  AND mainsql.pnum = setneed.c205_part_number_id(+)
                  AND mainsql.pnum = wghtd_avd_in_sales.c205_part_number_id(+)
             ORDER BY pnum, description;
      END IF;
   END gm_fch_part_fulfill;

   --
   /*******************************************************************
   * Description : Procedure which accepts part number and returns part
               status.This report is mainly used for DHR
   * Author     : Lakshmi
   *******************************************************************/
   PROCEDURE gm_fch_bo_dhr (
      p_tokeni       IN       VARCHAR,
      p_report_out   OUT      TYPES.cursor_type
   )
   AS
   v_plant_id      t5040_plant_master.c5040_plant_id%TYPE;
   BEGIN
	  SELECT  NVL(get_plantid_frm_cntx(),get_rule_value('DEFAULT_PLANT','ONEPORTAL')) INTO  v_plant_id FROM DUAL;
      my_context.set_double_inlist_ctx (p_tokeni);

      INSERT INTO my_temp_part_list
                  (c205_part_number_id, c205_qty)
         SELECT DISTINCT t205.c205_part_number_id,
                           NVL (SUM (t408.c408_shipped_qty), 0)
                         - NVL (SUM (t408.c408_qty_rejected), 0)
                    FROM t408_dhr t408, t205_part_number t205
                   WHERE t408.c408_status_fl IN (0, 1, 2, 3)
                     AND t408.c408_void_fl IS NULL
                     AND t408.c205_part_number_id = t205.c205_part_number_id
                     AND (   t205.c205_part_number_id IN (
                                                         SELECT tokenii
                                                           FROM v_double_in_list
                                                          WHERE token = 'PART')
                          OR t205.c202_project_id IN (SELECT tokenii
                                                        FROM v_double_in_list
                                                       WHERE token = 'PROJECT')
                         )
                GROUP BY t205.c205_part_number_id;

      OPEN p_report_out
       FOR
          SELECT   c205_part_number_id pnum, c205_part_num_desc description,
                   sales_backorder sales_bo_qty, dhrqty,
                   cons_backorder cons_bo_qty, (cust_req+set_need
                                               ) set_need, forecast_qty,
                   (forecast_qty / 2) avg_forecast_qty,
                   c205_inshelf qty_in_shelf, c205_inbulk qty_in_bulk,
                   tbv_qty tbv_qty_in_dhr, shelf_alloc_in,
                   ROUND (NVL (DECODE (SIGN (  ((forecast_qty / 2) * 1.5)
                                             - (  c205_inshelf
                                                + NVL (tbv_qty, 0)
                                                + NVL (shelf_alloc_in, 0)
                                               )
                                            ),
                                       -1, 0,
                                       (  ((forecast_qty / 2) * 1.5)
                                        - (  c205_inshelf
                                           + NVL (tbv_qty, 0)
                                           + NVL (shelf_alloc_in, 0)
                                          )
                                       )
                                      ),
                               0
                              )
                         ) to_shelf,
                   DECODE(get_part_inset(c205_part_number_id),'N',0,
                   -- Removed set_need from formula for skipping the demand qty, qty is based on the request
                   NVL (DECODE (SIGN ((cust_req) - c205_inbulk),
                                -1, 0,
                                (cust_req ) - c205_inbulk
                               ),
                        0
                       )) to_bulk, '0' to_rm
              FROM (SELECT v205.c205_part_number_id, v205.c205_part_num_desc,
                           mylist.c205_qty dhrqty,
                           NVL (c205_in_wip_set, 0) c205_in_wip_set,
                           (  NVL (c205_inbulk, 0)
                      --      + NVL (v205.c205_in_wip_set, 0) -- commented wip set qty addition for PMT-2233
                           ) c205_inbulk,
                           NVL (c205_inshelf, 0) c205_inshelf, tbv_qty,
                           NVL (sales_backorder, 0) sales_backorder,
                           NVL (cons_backorder, 0) cons_backorder,
                           NVL (set_need, 0) set_need,
                           NVL (forecast_qty, 0) forecast_qty,
                           NVL (shelf_alloc_in, 0) shelf_alloc_in,
                           NVL (cust_req, 0) cust_req
                      FROM my_temp_part_list mylist,
                           v205_qty_in_stock v205,
                           (SELECT   t408.c205_part_number_id pnum,
                                       NVL
                                          (SUM (t408.c408_shipped_qty),
                                           0
                                          )
                                     - NVL (SUM (t408.c408_qty_rejected), 0)
                                     + NVL (SUM (t409.c409_closeout_qty), 0)
                                                                      tbv_qty
                                FROM t408_dhr t408, t409_ncmr t409
                               WHERE t408.c408_dhr_id = t409.c408_dhr_id(+)
                                 AND t409.c409_void_fl IS NULL
                                 AND t408.c408_status_fl = 3
                                 AND t408.c408_void_fl IS NULL
                                 AND t408.c5040_plant_id = t409.c5040_plant_id(+)
                            	 AND t408.c5040_plant_id = v_plant_id
                            GROUP BY t408.c205_part_number_id) t408a,
                           (SELECT   t502.c205_part_number_id,
                                     NVL
                                        (SUM (t502.c502_item_qty),
                                         0
                                        ) sales_backorder
                                FROM t501_order t501,
                                     t502_item_order t502,
                                     my_temp_part_list mylist
                               WHERE t501.c501_order_id = t502.c501_order_id
                                 AND t501.c901_order_type = 2525
                                 AND t501.c501_delete_fl IS NULL
                                 AND t501.c501_void_fl IS NULL
				                 AND (t501.c901_ext_country_id IS NULL OR t501.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH
                                 AND t501.c501_status_fl = 0
                                 AND t502.c205_part_number_id =
                                                    mylist.c205_part_number_id
                                 AND t501.c5040_plant_id = v_plant_id                   
                            GROUP BY t502.c205_part_number_id) sbackorder,
                           (SELECT   t521.c205_part_number_id,
                                     NVL (SUM (t521.c521_qty),
                                          0
                                         ) cons_backorder
                                FROM t520_request t520,
                                     t521_request_detail t521,
                                     my_temp_part_list mylist
                               WHERE t520.c520_request_id =
                                                          t521.c520_request_id
                                 AND t520.c520_void_fl IS NULL
                                 AND t520.c520_delete_fl IS NULL
                                 AND t521.c205_part_number_id =
                                                    mylist.c205_part_number_id
                                 AND t520.c207_set_id IS NULL
                                 AND t520.c5040_plant_id = v_plant_id
                            GROUP BY t521.c205_part_number_id) consbackorder,
                           (SELECT c205_part_number_id, NVL (SUM (cust_req), 0) cust_req
								   FROM
								    (SELECT t521.c205_part_number_id, NVL (SUM (t521.c521_qty), 0) cust_req
								       FROM t520_request t520, t521_request_detail t521, my_temp_part_list mylist
								      WHERE t520.c520_request_id     = t521.c520_request_id
								        AND t521.c205_part_number_id = mylist.c205_part_number_id
								        AND
								        ((
								            t520.c520_status_fl             IN (10, 15)
								            AND t520.c520_master_request_id IS NULL       
								     )
								        OR
								        (
								            t520.c520_status_fl              = 10
								            AND t520.c520_master_request_id IN
								            (SELECT c520_request_id
								               FROM t520_request
								              WHERE c520_status_fl IN (15, 20, 40) --include  15 Back Log, 20 Ready to Consign
								                AND c207_set_id    IS NOT NULL
								                AND c520_void_fl   IS NULL
								                AND t520.c5040_plant_id = v_plant_id
								            )
								        ))
								        -- Commenting - should be call the Request, Based on if the request is in BO it doesn't matter
								        -- AND t520.c520_request_to IS NOT NULL
								        AND t520.c520_void_fl IS NULL
								       AND t520.c207_set_id            IS NOT NULL
								       AND t520.c5040_plant_id = v_plant_id
								   GROUP BY t521.c205_part_number_id
								    )
								GROUP BY c205_part_number_id)cust_req,
                           (SELECT   t4042.c205_part_number_id,
                                     SUM (c4042_qty) set_need
                                FROM t4040_demand_sheet t4040,
                                     t4042_demand_sheet_detail t4042,
                                     my_temp_part_list mylist,
                                     (SELECT MAX
                                                (c250_inventory_lock_id
                                                ) c250_inventory_lock_id
                                        FROM t250_inventory_lock t250
                                       WHERE c901_lock_type = 20430
                                         AND c250_void_fl IS NULL) t250
                               WHERE t4040.c250_inventory_lock_id =
                                                   t250.c250_inventory_lock_id
                                 AND t4040.c4040_demand_sheet_id =
                                                   t4042.c4040_demand_sheet_id
                                 AND t4042.c205_part_number_id =
                                                    mylist.c205_part_number_id
                                 AND t4040.c901_demand_type NOT IN
                                                               (40020, 40023)
                                 AND t4042.c4042_period BETWEEN CURRENT_DATE
                                                            AND ADD_MONTHS
                                                                     (CURRENT_DATE,
                                                                      2
                                                                     )
				 -- should exclude item here.
                                 AND t4042.c4042_ref_id != '-9999'	
                                 AND t4040.c4040_void_fl IS NULL
                                 and t4040.c4020_demand_master_id in (
                                 SELECT c4020_demand_master_id
								  FROM t4020_demand_master
								  WHERE c4020_demand_master_id  IN
								    (SELECT c9352_ref_id
								    FROM t9352_hierarchy_flow
								    WHERE c901_ref_type   ='60260'
								    AND c9350_hierarchy_id in 
								    (select c906_rule_value from t906_rules where c906_rule_id='DMD_SHT_RGN' and c906_rule_grp_id='US_DMD_SHT')
								    )
								  AND c4020_inactive_fl IS NULL)
                                 
                            GROUP BY t4042.c205_part_number_id) setneed,
                           (SELECT   t4042.c205_part_number_id,
                                     SUM (c4042_qty) forecast_qty
                                FROM t4040_demand_sheet t4040,
                                     t4042_demand_sheet_detail t4042,
                                     my_temp_part_list mylist,
                                     (SELECT MAX
                                                (c250_inventory_lock_id
                                                ) c250_inventory_lock_id
                                        FROM t250_inventory_lock t250
                                       WHERE c901_lock_type = 20430
                                         AND c250_void_fl IS NULL) t250
                               WHERE t4040.c250_inventory_lock_id =
                                                   t250.c250_inventory_lock_id
                                 AND t4040.c4040_demand_sheet_id =
                                                   t4042.c4040_demand_sheet_id
                                 AND t4042.c205_part_number_id =
                                                    mylist.c205_part_number_id
                                 AND t4040.c901_demand_type = 40020
                                 AND t4042.c4042_period BETWEEN CURRENT_DATE
                                                            AND ADD_MONTHS
                                                                     (CURRENT_DATE,
                                                                      2
                                                                     )
                                 AND t4040.c4040_void_fl IS NULL
                            GROUP BY t4042.c205_part_number_id) sales_need,
                           (SELECT   c205_part_number_id,
                                     SUM (qty) shelf_alloc_in
                                FROM (SELECT t505.c205_part_number_id,
                                             t505.c505_item_qty qty
                                        FROM t504_consignment t504,
                                             t505_item_consignment t505,
                                             my_temp_part_list mylist
                                       WHERE t504.c504_consignment_id =
                                                      t505.c504_consignment_id
                                         AND t505.c205_part_number_id =
                                                    mylist.c205_part_number_id
                                         AND c504_status_fl > 1
                                         AND c504_status_fl < 4
                                         AND c207_set_id IS NULL
                                         AND c504_void_fl IS NULL
                                         AND c704_account_id = '01'
                                         AND t504.c5040_plant_id = v_plant_id
                                         AND c504_type IN (4116, 400063)
                                      UNION ALL
                                      SELECT t413.c205_part_number_id,
                                             t413.c413_item_qty qty
                                        FROM t412_inhouse_transactions t412,
                                             t413_inhouse_trans_items t413,
                                             my_temp_part_list mylist
                                       WHERE t412.c412_inhouse_trans_id =
                                                    t413.c412_inhouse_trans_id
                                         AND t413.c205_part_number_id =
                                                    mylist.c205_part_number_id
                                         AND c412_status_fl > 1
                                         AND c412_status_fl < 4
                                         AND t412.c5040_plant_id = v_plant_id
                                         AND c412_type IN
                                                (50157,
                                                 50152,
                                                 50161,
                                                 4118,
                                                 400068,
                                                 40053,
                                                 100406,
                                                 400063,
                                                 4116
                                                )
                                         AND c412_void_fl IS NULL)
                            GROUP BY c205_part_number_id) shelf
                     WHERE mylist.c205_part_number_id = t408a.pnum(+)
                       AND mylist.c205_part_number_id =
                                                      v205.c205_part_number_id
                       AND mylist.c205_part_number_id = sbackorder.c205_part_number_id(+)
                       AND mylist.c205_part_number_id = consbackorder.c205_part_number_id(+)
                       AND mylist.c205_part_number_id = setneed.c205_part_number_id(+)
                       AND mylist.c205_part_number_id = shelf.c205_part_number_id(+)
                       AND mylist.c205_part_number_id = sales_need.c205_part_number_id(+)
                       AND mylist.c205_part_number_id = cust_req.c205_part_number_id(+))
          ORDER BY pnum;
   END gm_fch_bo_dhr;

/*******************************************************
   * Description : Procedure to generate  fetch screen
   * Author     : Xun
   *******************************************************/
--
   PROCEDURE gm_fch_trend_summary (
      p_trendcal             IN       VARCHAR2,
      p_pnum                 IN       VARCHAR2,
      p_setid                IN       VARCHAR2,
      p_salesgrptype         IN       VARCHAR2,
      p_trendtype            IN       VARCHAR2,
      p_setidSearch          IN       VARCHAR2,
      p_outforecastheader    OUT      TYPES.cursor_type,
      p_outforecastdetails   OUT      TYPES.cursor_type,
      p_outdemandheader      OUT      TYPES.cursor_type,
      p_outdemanddetails     OUT      TYPES.cursor_type,
      p_outcurrentheader     OUT      TYPES.cursor_type,
      p_outcurrentdetails    OUT      TYPES.cursor_type,
      p_outbodetails         OUT      TYPES.cursor_type,
      p_outshelfpodetails    OUT      TYPES.cursor_type,
      p_outpartdetails       OUT      TYPES.cursor_type,
      p_outforecastlevelheader OUT      TYPES.cursor_type,
      p_outforecastleveldetails OUT      TYPES.cursor_type
      
   )
   AS
      v_inventory_date   VARCHAR2 (50);
   BEGIN
      SELECT TO_CHAR (MAX (c250_lock_date), 'MM/DD/YYYY')
        INTO v_inventory_date
        FROM t250_inventory_lock t250
       WHERE t250.c250_void_fl IS NULL AND t250.c901_lock_type = 20430;
	  
       my_context.set_my_inlist_ctx (p_setidSearch);
      -- Below procedure called to fetch Forecast Month
      gm_sav_trend_insert_part (p_pnum, p_setid, p_salesgrptype);
      my_context.set_double_inlist_ctx (p_trendcal);
      --my_context.set_my_inlist_ctx (p_trendcal);
      -- Below procedure called to fetch  part Details
      gm_fch_trend_part_detail (p_outpartdetails);
      -- Below code to fecth Forecast
      gm_fch_trend_forecast_detail (v_inventory_date,
                                    p_outforecastheader,
                                    p_outforecastdetails
                                   );
      -- Below code to fecth Demand
      gm_fch_trend_demand_detail (p_outdemandheader, p_outdemanddetails);
      -- Below code to fecth Current
      gm_fch_trend_current_detail (p_outcurrentheader, p_outcurrentdetails);
      -- Below code to fecth backorder
      gm_fch_trend_bo_detail (p_outbodetails);
      my_context.set_my_inlist_ctx ('70006,70007,70010,70011,70012,70013,70014,70016');
      -- Below code to fecth shelfpo
      gm_fch_trend_shelfpo_detail (p_outshelfpodetails);
      
      gm_fch_trend_forecast_lvl_dtl(p_outforecastlevelheader,p_outforecastleveldetails);
   END gm_fch_trend_summary;

 /*******************************************************
   * Description : Procedure to generate  temp table to store screen info
   * Author     : Xun
   *******************************************************/
--
   PROCEDURE gm_sav_trend_insert_part (
      p_pnum           IN   VARCHAR2,
      p_setid          IN   VARCHAR2,
      p_salesgrptype   IN   VARCHAR2
   )
   AS
   v_plant_id      t5040_plant_master.c5040_plant_id%TYPE;
   v_count         NUMBER;
   BEGIN
	  SELECT  NVL(get_plantid_frm_cntx(),get_rule_value('DEFAULT_PLANT','ONEPORTAL')) INTO  v_plant_id FROM DUAL;
      IF p_salesgrptype = '50271'                                 --all parts
      THEN
         --UNION changed to UNION ALL
         INSERT INTO my_temp_part_list
                     (c205_part_number_id)
            SELECT c205_part_number_id
              FROM (
            SELECT t205.c205_part_number_id
              FROM t205_part_number t205
             WHERE t205.c205_part_number_id IN (
                      SELECT c205_part_number_id
                        FROM t208_set_details t208
                       WHERE t208.c207_set_id = p_setid)
                         AND t205.c205_sub_component_fl is null 	-- exclude subcomponent
		   
            UNION ALL
            SELECT t205.c205_part_number_id
              FROM t205_part_number t205
             WHERE 
             REGEXP_LIKE(t205.c205_part_number_id, CASE WHEN TO_CHAR(p_pnum) IS NOT NULL THEN TO_CHAR(p_pnum) END)
             UNION ALL
            SELECT t205.c205_part_number_id
              FROM t205_part_number t205
             WHERE t205.c205_part_number_id IN (
                      SELECT c205_part_number_id
                        FROM t208_set_details t208
                       WHERE t208.c207_set_id IN (SELECT *
                                                  FROM v_in_list)
						AND c208_INSET_FL='Y' AND C208_SET_QTY >0                                                  
                                                  )
                         AND t205.c205_sub_component_fl is null
                         )
               where rownum < 2002;
		       --Rownum is added as there is a validation to fetch only upto 2000 records.                           
      ELSIF p_salesgrptype = '50272'                       --80% selling parts
      THEN
         INSERT INTO my_temp_part_list
                     (c205_part_number_id)
            SELECT c205_part_number_id
              FROM (SELECT c205_part_number_id, itemqty, sales_per,
                           SUM (sales_per) OVER (PARTITION BY '1' ORDER BY sales_per DESC ROWS UNBOUNDED PRECEDING)
                                                             AS cum_sales_per
                      FROM (SELECT   t502.c205_part_number_id,
                                     SUM (t502.c502_item_qty) itemqty,
                                     ROUND
                                        (  100
                                         * ratio_to_report
                                                      (SUM (t502.c502_item_qty)
                                                      ) OVER (),
                                         2
                                        ) sales_per
                                FROM t501_order t501,
                                     t502_item_order t502,
                                     (SELECT t205.c205_part_number_id
                                        FROM t205_part_number t205
                                       WHERE t205.c205_part_number_id IN (
                                                SELECT c205_part_number_id
                                                  FROM t208_set_details t208
                                                 WHERE t208.c207_set_id =
                                                                       p_setid)
                                      UNION ALL
                                      SELECT t205.c205_part_number_id
                                        FROM t205_part_number t205
                                       WHERE 
                                        REGEXP_LIKE(t205.c205_part_number_id, CASE WHEN TO_CHAR(p_pnum) IS NOT NULL THEN TO_CHAR(p_pnum) END)
							UNION ALL
							            SELECT t205.c205_part_number_id
							              FROM t205_part_number t205
							             WHERE t205.c205_part_number_id IN (
							                      SELECT c205_part_number_id
							                        FROM t208_set_details t208
							                       WHERE t208.c207_set_id IN (SELECT *
							                                                  FROM v_in_list)
												   AND c208_INSET_FL='Y' AND C208_SET_QTY >0							                                                 
							                                                  )
							                                                  ) mylist
                               WHERE t501.c501_order_id = t502.c501_order_id
                                 AND t501.c501_void_fl IS NULL
                                 AND t501.c501_delete_fl IS NULL
                                 AND t501.c5040_plant_id = v_plant_id
								 AND NVL (c901_order_type, 0) <> 2524
                                 AND NVL (c901_order_type, -9999) NOT IN (
					                SELECT t906.c906_rule_value
					                  FROM t906_rules t906
					                 WHERE t906.c906_rule_grp_id = 'ORDTYPE'
					                   AND c906_rule_id = 'EXCLUDE')
                                 AND mylist.c205_part_number_id =
                                                      t502.c205_part_number_id
                                 AND t501.c501_order_date
                                        BETWEEN TRUNC (ADD_MONTHS (CURRENT_DATE,
                                                                   -3),
                                                       'MONTH'
                                                      )
                                            AND TRUNC (CURRENT_DATE, 'MONTH') - 1
                            GROUP BY t502.c205_part_number_id))
             WHERE cum_sales_per <= 80 and rownum < 2002;
		     --Rownum is added as there is a validation to fetch only upto 2000 records.
      ELSIF p_salesgrptype = '50273'       -- 80% not including crossover part
      THEN
         INSERT INTO my_temp_part_list
                     (c205_part_number_id)
            SELECT c205_part_number_id
              FROM (SELECT c205_part_number_id, itemqty, sales_per,
                           SUM (sales_per) OVER (PARTITION BY '1' ORDER BY sales_per DESC ROWS UNBOUNDED PRECEDING)
                                                             AS cum_sales_per
                      FROM (SELECT   t502.c205_part_number_id,
                                     SUM (t502.c502_item_qty) itemqty,
                                     ROUND
                                        (  100
                                         * ratio_to_report
                                                      (SUM (t502.c502_item_qty)
                                                      ) OVER (),
                                         2
                                        ) sales_per
                                FROM t501_order t501,
                                     t502_item_order t502,
                                     (SELECT t205.c205_part_number_id
                                        FROM t205_part_number t205
                                       WHERE t205.c205_crossover_fl IS NULL
                                         AND t205.c205_part_number_id IN (
                                                SELECT c205_part_number_id
                                                  FROM t208_set_details t208
                                                 WHERE t208.c207_set_id =
                                                                       p_setid)
                                      UNION ALL
                                      SELECT t205.c205_part_number_id
                                        FROM t205_part_number t205
                                       WHERE 
                                       --t205.c205_part_number_id IN (SELECT *FROM v_in_list)
                                       REGEXP_LIKE(t205.c205_part_number_id, CASE WHEN TO_CHAR(p_pnum) IS NOT NULL THEN TO_CHAR(p_pnum) END)
                                        AND t205.c205_crossover_fl IS NULL                          
                                    UNION ALL
							            SELECT t205.c205_part_number_id
							              FROM t205_part_number t205
							             WHERE t205.c205_part_number_id IN (
							                      SELECT c205_part_number_id
							                        FROM t208_set_details t208
							                       WHERE t208.c207_set_id IN (SELECT *
							                                                  FROM v_in_list)
												   AND c208_INSET_FL='Y' AND C208_SET_QTY >0									                                                  
							                                                  )
							                      -- AND t205.c205_sub_component_fl is null
							                       
							                                                  ) mylist
                               WHERE t501.c501_order_id = t502.c501_order_id
                                 AND t501.c501_void_fl IS NULL
                                 AND t501.c501_delete_fl IS NULL
                                 AND t501.c5040_plant_id = v_plant_id
								 AND NVL (c901_order_type, 0) <> 2524
                                 AND NVL (c901_order_type, -9999) NOT IN (
						                SELECT t906.c906_rule_value
						                  FROM t906_rules t906
						                 WHERE t906.c906_rule_grp_id = 'ORDTYPE'
						                   AND c906_rule_id = 'EXCLUDE')
                                 AND mylist.c205_part_number_id =
                                                      t502.c205_part_number_id
                                 AND t501.c501_order_date
                                        BETWEEN TRUNC (ADD_MONTHS (CURRENT_DATE,
                                                                   -3),
                                                       'MONTH'
                                                      )
                                            AND TRUNC (CURRENT_DATE, 'MONTH') - 1
                            GROUP BY t502.c205_part_number_id))
             WHERE cum_sales_per <= 80 and rownum < 2002;
		     --Rownum is added as there is a validation to fetch only upto 2000 records.
      END IF;
      SELECT count(c205_part_number_id) into v_count from my_temp_part_list;	
	  -- This validation is added to fetch only upto 2000 parts in Trend Report.
	  if v_count > 2000 then
		raise_application_error (-20673, '');
	  end if;     
      
   END gm_sav_trend_insert_part;

 /*******************************************************
   * Description : Procedure to fetch part details
   * Author     : Xun
   *******************************************************/
--
   PROCEDURE gm_fch_trend_part_detail (p_outpartdetails OUT TYPES.cursor_type)
   AS
   BEGIN
      OPEN p_outpartdetails
       FOR
          SELECT t205.c205_part_number_id ID,
                    t205.c205_part_number_id
                 || ' - '
                 || t205.c205_part_num_desc pdesc
            FROM t205_part_number t205, my_temp_part_list mylist
           WHERE t205.c205_part_number_id = mylist.c205_part_number_id;
   END gm_fch_trend_part_detail;

    
 /*******************************************************
   * Description : Procedure to fetch forecast details
   * Author     : Xun
   *******************************************************/
--
   PROCEDURE gm_fch_trend_forecast_detail (
      p_inventory_date   IN       VARCHAR2,
      p_outheader        OUT      TYPES.cursor_type,
      p_outdetails       OUT      TYPES.cursor_type
   )
   AS
   BEGIN
      OPEN p_outheader
       FOR
          SELECT TO_CHAR (v9001.month_value, 'Mon YY') forecast_date
            FROM v9001_month_list v9001
           WHERE v9001.month_value BETWEEN TRUNC (CURRENT_DATE, 'MONTH')
                                       AND TRUNC
                                              (LAST_DAY (ADD_MONTHS (CURRENT_DATE,
                                                                     2
                                                                    )
                                                        )
                                              );

      OPEN p_outdetails
       FOR
       
        SELECT   t4060.c205_part_number_id ID,
               t4060.c205_part_number_id NAME,
               TO_CHAR (t4060.c4060_period, 'Mon YY') forecast_date,
               NVL (CEIL (SUM (t4060.c4060_qty)), 0) qty
               ,NVL (CEIL (SUM (t4060.c4060_qty)), 0) part_qty
	          FROM t4060_part_forecast_qty t4060, 
	               my_temp_part_list mylist
	         WHERE t4060.c4060_period BETWEEN TRUNC (CURRENT_DATE, 'MONTH')
	                                      AND TRUNC
	                                            (LAST_DAY
	                                                     (ADD_MONTHS (CURRENT_DATE,
	                                                                  2
	                                                                 )
	                                                     )
	                                            ) 
	           AND t4060.C901_FORECAST_TYPE IN (SELECT tokenii
                                                FROM v_double_in_list
                                               WHERE token = 'TREND') 
               AND t4060.c901_level_value='100800' -- WorldWide Only
               AND t4060.c205_part_number_id = mylist.c205_part_number_id
	      GROUP BY t4060.c205_part_number_id, t4060.c4060_period
	      ORDER BY t4060.c205_part_number_id, t4060.c4060_period;
       /*
          SELECT   t4042.c205_part_number_id ID,
                   t4042.c205_part_number_id NAME,
                   TO_CHAR (t4042.c4042_period, 'Mon YY') forecast_date,
                   NVL (CEIL (SUM (t4042.c4042_qty)), 0) qty
                   ,NVL (CEIL (SUM (t4042.c4042_qty)), 0) part_qty
                   ,t4040.c901_demand_type DMD_TYP
              FROM t4040_demand_sheet t4040,
                   t4042_demand_sheet_detail t4042,
                   my_temp_part_list mylist
             WHERE t4040.C4040_demand_period_dt =
                                      TO_DATE (p_inventory_date, 'MM/DD/YYYY')
               AND t4040.c4040_demand_sheet_id = t4042.c4040_demand_sheet_id
               AND t4042.c4042_period BETWEEN TRUNC (CURRENT_DATE, 'MONTH')
                                          AND TRUNC
                                                (LAST_DAY
                                                         (ADD_MONTHS (CURRENT_DATE,
                                                                      2
                                                                     )
                                                         )
                                                )
               AND t4040.c4040_void_fl IS NULL
               AND t4042.c901_type = '50563'                       -- Forecast
               AND t4040.c901_demand_type IN (SELECT tokenii
                                                FROM v_double_in_list
                                               WHERE token = 'TREND')
               AND t4042.c205_part_number_id = mylist.c205_part_number_id
          	   AND t4040.c4020_demand_master_id in (
               select c4020_demand_master_id  From T4020_demand_master Where
               c4020_demand_master_id in (select C9352_ref_id From T9352_hierarchy_flow 
               where c901_ref_type='60260' --Demand Sheet
               and c9350_hierarchy_id in 
               (SELECT C906_RULE_VALUE FROM T906_RULES WHERE C906_RULE_ID='DMD_SHT_RGN' AND C906_RULE_GRP_ID ='US_DMD_SHT') 
               							  )
               							  			)	
          GROUP BY t4042.c205_part_number_id, t4042.c4042_period,t4040.c901_demand_type
          ORDER BY t4042.c205_part_number_id, t4042.c4042_period;
          */
   END gm_fch_trend_forecast_detail;

 /*******************************************************
   * Description : Procedure to fetch demand details
   * Author     : Xun
   *******************************************************/
--
PROCEDURE gm_fch_trend_demand_detail (
      p_outdemandheader    OUT   TYPES.cursor_type,
      p_outdemanddetails   OUT   TYPES.cursor_type
   )
   AS
   BEGIN
      OPEN p_outdemandheader
       FOR
       	SELECT CURRENT_DATE FROM DUAL WHERE 1=0;
       OPEN p_outdemanddetails
       FOR
       	 SELECT CURRENT_DATE FROM DUAL WHERE 1=0;
  END gm_fch_trend_demand_detail;    
 	
/*******************************************************
   * Description : Procedure to fetch current month details
   * Author     : Xun
   *******************************************************/
--
   PROCEDURE gm_fch_trend_current_detail (
      p_outcurrentheader    OUT   TYPES.cursor_type,
      p_outcurrentdetails   OUT   TYPES.cursor_type
   )
   AS
   v_plant_id      t5040_plant_master.c5040_plant_id%TYPE;
   BEGIN
	  SELECT  NVL(get_plantid_frm_cntx(),get_rule_value('DEFAULT_PLANT','ONEPORTAL')) INTO  v_plant_id FROM DUAL;
      OPEN p_outcurrentheader
       FOR
          SELECT 'Current'
            FROM v9001_month_list v9001
           WHERE v9001.month_value BETWEEN TRUNC (CURRENT_DATE, 'MONTH')
                                       AND TRUNC (CURRENT_DATE);

      OPEN p_outcurrentdetails
       FOR
          SELECT   c205_part_number_id ID, c205_part_number_id NAME,
                   'Current', SUM (c_qty) qty
              FROM (SELECT   t502.c205_part_number_id,
                             TO_CHAR (t501.c501_order_date,
                                      'Mon YY') curmonth,
                             SUM (t502.c502_item_qty) c_qty
                        FROM t501_order t501,
                             t502_item_order t502,
                             my_temp_part_list mylist
                       WHERE t501.c501_order_date BETWEEN TRUNC (CURRENT_DATE,
                                                                 'MONTH'
                                                                )
                                                      AND TRUNC (CURRENT_DATE)
                         AND t501.c501_order_id = t502.c501_order_id
                         AND t502.c205_part_number_id =
                                                    mylist.c205_part_number_id
                         AND t501.c501_void_fl IS NULL
                         AND t501.c5040_plant_id = v_plant_id
                         AND NVL (t501.c901_order_type, -999) <> 2524
                         AND NVL (c901_order_type, -9999) NOT IN (
				                SELECT t906.c906_rule_value
				                  FROM t906_rules t906
				                 WHERE t906.c906_rule_grp_id = 'ORDTYPE'
				                   AND c906_rule_id = 'EXCLUDE')
                         AND '40020' IN (SELECT tokenii
                                           FROM v_double_in_list
                                          WHERE token = 'TREND')
                    GROUP BY t502.c205_part_number_id,
                             TO_CHAR (t501.c501_order_date, 'Mon YY')
                    UNION ALL
                    SELECT   t505.c205_part_number_id,
                             TO_CHAR (t504.c504_ship_date, 'Mon YY') curmonth,
                             SUM (t505.c505_item_qty) c_qty
                        FROM t504_consignment t504,
                             t505_item_consignment t505,
                             my_temp_part_list mylist
                       WHERE t504.c701_distributor_id IS NOT NULL
                         AND t504.c504_consignment_id =
                                                      t505.c504_consignment_id
                         AND t505.c205_part_number_id =
                                                    mylist.c205_part_number_id
                         --AND t504.c207_set_id IS NOT NULL
                         AND t504.c504_ship_date BETWEEN TRUNC (CURRENT_DATE,
                                                                'MONTH'
                                                               )
                                                     AND TRUNC (CURRENT_DATE)
                         AND t504.c504_status_fl = 4
                         AND t504.c504_type IN (4110)
                         AND TRIM (t505.c505_control_number) IS NOT NULL
                         AND c504_void_fl IS NULL
                         AND t504.c5040_plant_id = v_plant_id
                         AND '40021' IN (SELECT tokenii
                                           FROM v_double_in_list
                                          WHERE token = 'TREND')
                    GROUP BY t505.c205_part_number_id,
                             TO_CHAR (t504.c504_ship_date, 'Mon YY')
                    UNION ALL
                    SELECT   t505.c205_part_number_id,
                             TO_CHAR (t504.c504_ship_date, 'Mon YY') curmonth,
                             SUM (t505.c505_item_qty) c_qty
                        FROM t504_consignment t504,
                             t505_item_consignment t505,
                             my_temp_part_list mylist
                       WHERE t504.c701_distributor_id IS NULL
                         AND t504.c704_account_id = '01'
                         AND t504.c504_consignment_id =
                                                      t505.c504_consignment_id
                         AND t505.c205_part_number_id =
                                                    mylist.c205_part_number_id
                         --AND t504.c207_set_id IS NOT NULL
                         AND t504.c504_ship_date BETWEEN TRUNC (CURRENT_DATE,
                                                                'MONTH'
                                                               )
                                                     AND TRUNC (CURRENT_DATE)
                         AND t504.c504_status_fl = 4
                         AND TRIM (t505.c505_control_number) IS NOT NULL
                         AND c504_void_fl IS NULL
                         AND t504.c5040_plant_id = v_plant_id
                         AND '40022' IN (SELECT tokenii
                                           FROM v_double_in_list
                                          WHERE token = 'TREND')
                    GROUP BY t505.c205_part_number_id,
                             TO_CHAR (t504.c504_ship_date, 'Mon YY'))
          GROUP BY c205_part_number_id, curmonth
          ORDER BY c205_part_number_id, TO_DATE (curmonth, 'Mon YY');
   END gm_fch_trend_current_detail;

   /*******************************************************
   * Description : Procedure to generate   Back Order details
   * Author     : Xun
   *******************************************************/
--
   PROCEDURE gm_fch_trend_bo_detail (p_out_bo_details OUT TYPES.cursor_type)
   AS
   v_plant_id      t5040_plant_master.c5040_plant_id%TYPE;
   BEGIN
	  SELECT  NVL(get_plantid_frm_cntx(),get_rule_value('DEFAULT_PLANT','ONEPORTAL')) INTO  v_plant_id FROM DUAL;
      OPEN p_out_bo_details
       FOR
          SELECT   c205_part_number_id ID, SUM (qty) bo_qty
              FROM (SELECT   t502.c205_part_number_id,
                             SUM (t502.c502_item_qty) qty
                        FROM t501_order t501,
                             t502_item_order t502,
                             my_temp_part_list mylist
                       WHERE t501.c501_order_id = t502.c501_order_id
                         AND mylist.c205_part_number_id =
                                                      t502.c205_part_number_id
                         AND t501.c901_order_type = 2525
                         AND t501.c501_delete_fl IS NULL
                         AND t501.c501_void_fl IS NULL
                         AND t501.c5040_plant_id = v_plant_id
						 AND (t501.c901_ext_country_id IS NULL OR t501.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH
                         AND t501.c501_status_fl = 0
                         AND '40020' IN (SELECT tokenii
                                           FROM v_double_in_list
                                          WHERE token = 'TREND')
                    GROUP BY t502.c205_part_number_id
                    UNION
                    SELECT   t521.c205_part_number_id, SUM (t521.c521_qty)
                                                                          qty
                        FROM t520_request t520,
                             t521_request_detail t521,
                             my_temp_part_list mylist
                       WHERE t520.c520_request_id = t521.c520_request_id
                         AND mylist.c205_part_number_id =
                                                      t521.c205_part_number_id
                         AND t520.c520_status_fl = 10
                         AND t520.c520_void_fl IS NULL
                         AND t520.c520_delete_fl IS NULL
                         AND t520.c5040_plant_id = v_plant_id
                         AND t520.c520_request_for = '40021'
                         AND t520.c520_required_date < TRUNC (CURRENT_DATE)
                         AND t520.c520_request_to IS NOT NULL
                         AND '40021' IN (SELECT tokenii
                                           FROM v_double_in_list
                                          WHERE token = 'TREND')
                    GROUP BY t521.c205_part_number_id
                    UNION
                    SELECT   t521.c205_part_number_id, SUM (t521.c521_qty)
                                                                          qty
                        FROM t520_request t520,
                             t521_request_detail t521,
                             my_temp_part_list mylist
                       WHERE t520.c520_request_id = t521.c520_request_id
                         AND mylist.c205_part_number_id =
                                                      t521.c205_part_number_id
                         AND t520.c520_status_fl = 10
                         AND t520.c520_void_fl IS NULL
                         AND t520.c520_delete_fl IS NULL
                         AND t520.c520_request_to IS NOT NULL
                         AND t520.c5040_plant_id = v_plant_id
                         AND t520.c520_request_for = '40022'
                         AND t520.c520_required_date < TRUNC (CURRENT_DATE)
                         AND '40022' IN (SELECT tokenii
                                           FROM v_double_in_list
                                          WHERE token = 'TREND')
                    GROUP BY t521.c205_part_number_id)
          GROUP BY c205_part_number_id;
   END gm_fch_trend_bo_detail;

/*****************************************************************
   * Description : Procedure to generate Shelf and other qty details
   * Author     : Xun
   ***************************************************************/
--
   PROCEDURE gm_fch_trend_shelfpo_detail (
      p_out_shelf_podetail   OUT   TYPES.cursor_type
   )
   AS
   v_plant_id      t5040_plant_master.c5040_plant_id%TYPE;
   BEGIN
	  SELECT  NVL(get_plantid_frm_cntx(),get_rule_value('DEFAULT_PLANT','ONEPORTAL')) INTO  v_plant_id FROM DUAL;
      OPEN p_out_shelf_podetail
       FOR
          
       		SELECT v205.ID ID,(V205.SHELF+NVL(shelf_allocin.QTY,0))shelf
       		,V205.RW_QTY+NVL(rw_allocin.QTY,0) RW_QTY
       		,V205.OTHER+ NVL(bulk_allocin.QTY,0) + NVL(repack_allocin.QTY,0) OTHER
       		,V205.PO PO,  v205.DHR DHR 
       		FROM (
       		SELECT c205_part_number_id ID,
                 (NVL (c205_inshelf, 0)) shelf,
                 c205_popend po, (DHR_PEN_QC_INS+DHR_PEN_QC_REL+DHR_QC_APPR+DHR_WIP) dhr,
                 (
                 	  NVL(rmqty,0)				-- Raw Material
                 	  
                 	+ NVL(c205_inbulk,0) 		-- Bulk Inventory  
                 	+ NVL(c205_in_wip_set,0)
                    + NVL(c205_tbe_alloc,0)
                 	+ NVL(build_set_qty,0)		-- Built Set
                 	
                 	+ NVL(c205_repackavail,0)	-- Repack
                 	
                 	
                 	+ NVL(c205_returnavail,0)	-- Return Hold
                 	
                 	+ NVL(c205_shipalloc,0)		-- Shipping Hold
                 ) other,
                 NVL(c205_rw_avail_qty,0)
                 rw_qty                 
            FROM v205_qty_in_stock)v205,
            (SELECT   c205_part_number_id ID,
                                     SUM (qty) QTY
                                FROM (SELECT t505.c205_part_number_id,
                                             NVL(t505.c505_item_qty,0) qty
                                        FROM t504_consignment t504,
                                             t505_item_consignment t505,
                                             my_temp_part_list mylist
                                       WHERE t504.c504_consignment_id =
                                                      t505.c504_consignment_id
                                         AND t505.c205_part_number_id =
                                                    mylist.c205_part_number_id
                                         AND t504.c5040_plant_id = v_plant_id
                                         AND c504_status_fl > 1
                                         AND c504_status_fl < 4
                                         AND c207_set_id IS NULL
                                         AND c504_void_fl IS NULL
                                         AND c704_account_id = '01'
                                         AND c504_type IN (4116, 400063)
                                      UNION ALL
                                      SELECT t413.c205_part_number_id,
                                             NVL(t413.c413_item_qty,0) qty
                                        FROM t412_inhouse_transactions t412,
                                             t413_inhouse_trans_items t413,
                                             my_temp_part_list mylist
                                       WHERE t412.c412_inhouse_trans_id =
                                                    t413.c412_inhouse_trans_id
                                         AND t413.c205_part_number_id =
                                                    mylist.c205_part_number_id
                                         AND t412.c5040_plant_id = v_plant_id
                                         AND c412_status_fl > 1
                                         AND c412_status_fl < 4
                                         AND c412_type IN
                                                (50157,
                                                 50152,
                                                 50161,
                                                 4118,
                                                 400068,
                                                 40053,
                                                 100406,
                                                 9101,
                                                 56040,
                                                 400063,
                                                 4116,100067
                                                )
                                         AND c412_void_fl IS NULL)
                            GROUP BY c205_part_number_id)shelf_allocin,                            
				(SELECT   c205_part_number_id ID,
                                     SUM (qty) QTY
                                FROM (
                                      SELECT t413.c205_part_number_id,
                                             NVL(t413.c413_item_qty,0) qty
                                        FROM t412_inhouse_transactions t412,
                                             t413_inhouse_trans_items t413,
                                             my_temp_part_list mylist
                                       WHERE t412.c412_inhouse_trans_id =
                                                    t413.c412_inhouse_trans_id
                                         AND t413.c205_part_number_id =
                                                    mylist.c205_part_number_id
                                         AND t412.c5040_plant_id = v_plant_id
                                         AND c412_status_fl > 1
                                         AND c412_status_fl < 4
                                         AND c412_type IN
                                                (56021,
												56020,
												56024,
												56026,
												56023,
												56022,
												56027,
												56025
                                                )
                                         AND c412_void_fl IS NULL)
                            GROUP BY c205_part_number_id)rw_allocin 
            ,(SELECT   c205_part_number_id ID,
                     SUM (qty) QTY
                FROM (
                      SELECT t413.c205_part_number_id,
                             NVL(t413.c413_item_qty,0) qty
                        FROM t412_inhouse_transactions t412,
                             t413_inhouse_trans_items t413,
                             my_temp_part_list mylist
                       WHERE t412.c412_inhouse_trans_id =
                                    t413.c412_inhouse_trans_id
                         AND t413.c205_part_number_id =
                                    mylist.c205_part_number_id
                         AND t412.c5040_plant_id = v_plant_id           
                         AND c412_status_fl > 1
                         AND c412_status_fl < 4
                         AND c412_type IN
                                (400076,
                                 100065,
								400080,
								9100,
								56041,
								100072
                                )
                         AND c412_void_fl IS NULL)
                            GROUP BY c205_part_number_id)bulk_allocin, 
		(SELECT   c205_part_number_id ID,
                                     SUM (qty) QTY
                                FROM (SELECT t505.c205_part_number_id,
                                             NVL(t505.c505_item_qty,0) qty
                                        FROM t504_consignment t504,
                                             t505_item_consignment t505,
                                             my_temp_part_list mylist
                                       WHERE t504.c504_consignment_id =
                                                      t505.c504_consignment_id
                                         AND t505.c205_part_number_id =
                                                    mylist.c205_part_number_id
                                         AND t504.c5040_plant_id = v_plant_id           
                                         AND c504_status_fl > 1
                                         AND c504_status_fl < 4
                                         AND c207_set_id IS NULL
                                         AND c504_void_fl IS NULL
                                         AND c704_account_id = '01'
                                         AND c504_type IN (400058, 400060)
                                      UNION ALL
                                      SELECT t413.c205_part_number_id,
                                             NVL(t413.c413_item_qty,0) qty
                                        FROM t412_inhouse_transactions t412,
                                             t413_inhouse_trans_items t413,
                                             my_temp_part_list mylist
                                       WHERE t412.c412_inhouse_trans_id =
                                                    t413.c412_inhouse_trans_id
                                         AND t413.c205_part_number_id =
                                                    mylist.c205_part_number_id
                                         AND t412.c5040_plant_id = v_plant_id           
                                         AND c412_status_fl > 1
                                         AND c412_status_fl < 4
                                         AND c412_type IN
                                                (56043,100064,100066,400082,400083
                                                )
                                         AND c412_void_fl IS NULL)
                            GROUP BY c205_part_number_id)repack_allocin                            
               WHERE V205.ID = shelf_allocin.ID(+)
               	AND V205.ID = rw_allocin.ID(+)
               	AND V205.ID = bulk_allocin.ID(+)
               	AND V205.ID = repack_allocin.ID(+)
            
            ;
   END gm_fch_trend_shelfpo_detail;

/*******************************************************
   * Description : Procedure to returnall parts on backorder
   * Author     : James
   *******************************************************/
--
   PROCEDURE gm_fch_part_backorder (
      p_projid          IN       VARCHAR2,
      p_pnumstr         IN       VARCHAR2,
      p_out_bo_report   OUT      TYPES.cursor_type
   )
   AS
   v_plant_id      t5040_plant_master.c5040_plant_id%TYPE;
   BEGIN
	  SELECT  NVL(get_plantid_frm_cntx(),get_rule_value('DEFAULT_PLANT','ONEPORTAL')) INTO  v_plant_id FROM DUAL;
      OPEN p_out_bo_report
       FOR
          SELECT *
            FROM (SELECT   t205.c205_part_number_id pnum,
                           t205.c205_part_num_desc pdesc,
                           NVL (sales.scount, 0) salescount,
                           NVL (item.iscount, 0) itemcount,
                           NVL (sitem.icount, 0) setitemcount,
                           NVL (setitem.setcount, 0) setcount,
                             NVL (sales.scount, 0)
                           + NVL (item.iscount, 0)
                           + NVL (sitem.icount, 0)
                           + NVL (setitem.setcount, 0) totalcount
                      FROM t205_part_number t205
-- SALES SQL
                  ,
                           (SELECT   t502.c205_part_number_id,
                                     SUM (t502.c502_item_qty) scount
                                FROM t501_order t501,
                                     t502_item_order t502,
                                     (SELECT DISTINCT ad_id, ad_name,
                                                      region_id, region_name,
                                                      d_id, d_name, rep_id,
                                                      rep_name, d_active_fl
                                                 FROM v700_territory_mapping_detail v700) v700
                               WHERE t501.c501_order_id = t502.c501_order_id
                                 AND t501.c901_order_type = 2525
                                 AND t501.c501_delete_fl IS NULL
                                 AND t501.c501_void_fl IS NULL
                                 AND t501.c5040_plant_id = v_plant_id
								 AND (t501.c901_ext_country_id IS NULL OR t501.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH
                                 AND t501.c501_status_fl = 0
                                 AND v700.rep_id = t501.c703_sales_rep_id
                            GROUP BY t502.c205_part_number_id) sales
                                                                    -- Item Consignment
                  ,
                           (SELECT   t521.c205_part_number_id,
                                     SUM (t521.c521_qty) icount
                                FROM t520_request t520,
                                     t521_request_detail t521,
                                     (SELECT DISTINCT ad_id, ad_name,
                                                      region_id, region_name,
                                                      d_id, d_name,
                                                      v700.d_active_fl
                                                 FROM v700_territory_mapping_detail v700) v700
                               WHERE t520.c520_request_id =
                                                          t521.c520_request_id
                                 AND t520.c520_status_fl = 10
                                 AND t520.c520_request_to IS NOT NULL
                                 AND t520.c520_void_fl IS NULL
                                 AND t520.c5040_plant_id = v_plant_id
                                 AND NVL (t520.c520_request_to, 0) = v700.d_id
                                 AND t520.c520_master_request_id IN (
                                        SELECT c520_request_id
                                          FROM t520_request
                                         WHERE c520_status_fl = 40
                                           AND c207_set_id IS NOT NULL)
                            --AND t521.c205_part_number_id = '665.614'
                            GROUP BY t521.c205_part_number_id) sitem
                                                                    -- Set Item Consignment
                  ,
                           (                  -- SET  Pending Item CONSIGNMENT
                            SELECT   t521.c205_part_number_id,
                                     SUM (t521.c521_qty) iscount
                                FROM t520_request t520,
                                     t521_request_detail t521,
                                     (SELECT DISTINCT ad_id, ad_name,
                                                      region_id, region_name,
                                                      d_id, d_name,
                                                      v700.d_active_fl
                                                 FROM v700_territory_mapping_detail v700) v700
                               WHERE t520.c520_request_id =
                                                          t521.c520_request_id
                                 AND t520.c520_status_fl = 10
                                 AND t520.c520_request_to IS NOT NULL
                                 AND t520.c520_void_fl IS NULL
                                 AND t520.c520_request_to = v700.d_id
                                 AND t520.c207_set_id IS NULL
                                 AND t520.c5040_plant_id = v_plant_id
                                 AND (   t520.c520_master_request_id IS NULL
                                      OR t520.c520_master_request_id IN (
                                                     SELECT c520_request_id
                                                       FROM t520_request
                                                      WHERE c207_set_id IS NULL)
                                     )
                            GROUP BY t521.c205_part_number_id) item
                                                                   -- SET   Pending
                  ,
                           (SELECT   t521.c205_part_number_id,
                                     SUM (t521.c521_qty) setcount
                                FROM t520_request t520,
                                     t521_request_detail t521,
                                     (SELECT DISTINCT ad_id, ad_name,
                                                      region_id, region_name,
                                                      d_id, d_name,
                                                      v700.d_active_fl
                                                 FROM v700_territory_mapping_detail v700) v700
                               WHERE t520.c520_status_fl = 10
                                 AND t520.c520_request_to IS NOT NULL
                                 AND t520.c520_request_id =
                                                          t521.c520_request_id
                                 AND t520.c5040_plant_id = v_plant_id                      
                                 AND t520.c520_void_fl IS NULL
                                 AND t520.c207_set_id IS NOT NULL
                                 AND t520.c520_master_request_id IS NULL
                                 AND t520.c520_request_to = v700.d_id
                            GROUP BY t521.c205_part_number_id) setitem
                     WHERE t205.c205_part_number_id = sales.c205_part_number_id(+)
                       AND t205.c205_part_number_id = item.c205_part_number_id(+)
                       AND t205.c205_part_number_id = sitem.c205_part_number_id(+)
                       AND t205.c205_part_number_id = setitem.c205_part_number_id(+)
                       AND t205.c205_part_number_id =
                                     NVL (trim(p_pnumstr), t205.c205_part_number_id)
                  ORDER BY t205.c205_part_number_id)
           WHERE totalcount > 0;
   END gm_fch_part_backorder;

       /*******************************************************
   * Description : Procedure to fetch the shipout history
                from 2003 and last 4 month info.
   * Author    : Ritesh
   *******************************************************/
--
   PROCEDURE gm_fch_history_shipout (
      p_out_shipoutheader    OUT   TYPES.cursor_type,
      p_out_shipouthistdtl   OUT   TYPES.cursor_type
   )
   AS
   	  v_plant_id      t5040_plant_master.c5040_plant_id%TYPE;
   BEGIN
	  SELECT  NVL(get_plantid_frm_cntx(),get_rule_value('DEFAULT_PLANT','ONEPORTAL')) INTO  v_plant_id FROM DUAL;
      OPEN p_out_shipoutheader
       FOR
          SELECT   tdate
              FROM (SELECT TO_CHAR (v9001.month_value, 'MM/YYYY') tdate
                      FROM v9001_month_list v9001
                     WHERE v9001.month_value BETWEEN TRUNC
                                                         (ADD_MONTHS (CURRENT_DATE,
                                                                      -4
                                                                     )
                                                         )
                                                 AND LAST_DAY
                                                         (ADD_MONTHS (CURRENT_DATE,
                                                                      -1
                                                                     )
                                                         )
                    UNION
                    SELECT TO_CHAR (TO_DATE ('01/2003', 'MM/YYYY'),
                                    'MM/YYYY'
                                   ) tdate
                      FROM DUAL)
          ORDER BY TO_DATE (tdate, 'MM/YYYY');

      OPEN p_out_shipouthistdtl
       FOR
          SELECT   mainq.setid, get_set_name (mainq.setid) setname,
                   mainq.tdate, mainq.csgret
              FROM (SELECT   setid, SUM (countc - countr) csgret, tdate
                        FROM (SELECT   t504.c207_set_id setid,
                                       COUNT (t504.c207_set_id) countc,
                                       0 countr,
                                       TO_CHAR (t504.c504_ship_date,
                                                'MM/YYYY'
                                               ) tdate
                                  FROM t504_consignment t504
                                 WHERE t504.c504_status_fl = 4
                                   AND t504.c504_type = 4110
                                   AND t504.c207_set_id IN (
                                                         SELECT my_temp_txn_id
                                                           FROM my_temp_list)
                                   AND t504.c207_set_id IS NOT NULL
                                   AND t504.c504_ship_date
                                          BETWEEN TRUNC (ADD_MONTHS (CURRENT_DATE,
                                                                     -3
                                                                    )
                                                        )
                                              AND LAST_DAY
                                                         (ADD_MONTHS (CURRENT_DATE,
                                                                      -1
                                                                     )
                                                         )
                                   AND t504.c504_void_fl IS NULL
                                   AND t504.c5040_plant_id = v_plant_id
                              GROUP BY t504.c207_set_id,
                                       TO_CHAR (t504.c504_ship_date,
                                                'MM/YYYY')
                              UNION
                              SELECT   t506.c207_set_id setid, 0 countc,
                                       COUNT (t506.c207_set_id) countr,
                                       TO_CHAR (t506.c506_return_date,
                                                'MM/YYYY'
                                               ) tdate
                                  FROM t506_returns t506
                                 WHERE t506.c506_status_fl = 2
                                   AND t506.c506_void_fl IS NULL
                                   AND t506.c207_set_id IN (
                                                         SELECT my_temp_txn_id
                                                           FROM my_temp_list)
                                   AND t506.c207_set_id IS NOT NULL
                                   AND t506.c506_type IN (3301, 3302, 3306)
                                   AND t506.c701_distributor_id IS NOT NULL
                                   AND t506.c506_return_date
                                          BETWEEN TRUNC (ADD_MONTHS (CURRENT_DATE,
                                                                     -3
                                                                    )
                                                        )
                                              AND LAST_DAY
                                                         (ADD_MONTHS (CURRENT_DATE,
                                                                      -1
                                                                     )
                                                         )
                                   AND t506.c506_void_fl IS NULL
                                   AND t506.c5040_plant_id = v_plant_id
                              GROUP BY t506.c207_set_id,
                                       TO_CHAR (t506.c506_return_date,
                                                'MM/YYYY'
                                               )) inneq
                    GROUP BY setid, tdate
                    UNION ALL
                    SELECT inputval.token setid,
                           (NVL (csg.csgcnt, 0) - NVL (ret.retcnt, 0)
                           ) csgret,
                           TO_CHAR (TO_DATE ('01/2003', 'MM/YYYY'),
                                    'MM/YYYY'
                                   ) tdate
                      FROM (SELECT   t504.c207_set_id ID, COUNT (1) csgcnt
                                FROM t504_consignment t504
                               WHERE t504.c207_set_id IS NOT NULL
                                 AND t504.c504_status_fl = '4'
                                 AND t504.c504_void_fl IS NULL
                                 AND t504.c504_ship_date
                                        BETWEEN TO_DATE ('01/01/2000',
                                                         'mm/dd/yyyy'
                                                        )
                                            AND LAST_DAY (ADD_MONTHS (CURRENT_DATE,
                                                                      -3
                                                                     )
                                                         )
                                 AND t504.c207_set_id IN (
                                                         SELECT my_temp_txn_id
                                                           FROM my_temp_list)
                                 AND t504.c701_distributor_id IS NOT NULL
                                 AND t504.c5040_plant_id = v_plant_id
                            GROUP BY t504.c207_set_id) csg,
                           (SELECT   c207_set_id ID, COUNT (1) retcnt
                                FROM t506_returns
                               WHERE c207_set_id IN (SELECT my_temp_txn_id
                                                       FROM my_temp_list)
                                 AND c506_credit_date
                                        BETWEEN TO_DATE ('01/01/2000',
                                                         'mm/dd/yyyy'
                                                        )
                                            AND LAST_DAY (ADD_MONTHS (CURRENT_DATE,
                                                                      -3
                                                                     )
                                                         )
                                 AND c506_status_fl = '2'
                                 AND c506_void_fl IS NULL
                                 AND c5040_plant_id = v_plant_id
                                 --AND c506_type IN (3301, 3306)
                                 AND c701_distributor_id IN (
                                        SELECT c701_distributor_id
                                          FROM t506_returns
                                         WHERE c506_type IN (3301, 3306)
                                           AND c207_set_id IN (
                                                         SELECT my_temp_txn_id
                                                           FROM my_temp_list))
                            GROUP BY c207_set_id) ret,
                           (SELECT my_temp_txn_id token
                              FROM my_temp_list) inputval
                     WHERE inputval.token = csg.ID(+) AND inputval.token = ret.ID(+)) mainq
          ORDER BY setid, TO_DATE (tdate, 'MM/YYYY');
   END gm_fch_history_shipout;

    /*******************************************************
   * Description : Procedure to fetch the Owe details
   * Author   : Ritesh
   *******************************************************/
   PROCEDURE gm_fch_owe_details (p_out_owedtl OUT TYPES.cursor_type)
   AS
   v_plant_id      t5040_plant_master.c5040_plant_id%TYPE;
   BEGIN
	  SELECT  NVL(get_plantid_frm_cntx(),get_rule_value('DEFAULT_PLANT','ONEPORTAL')) INTO  v_plant_id FROM DUAL;
      OPEN p_out_owedtl
       FOR
          SELECT   t520.c207_set_id ID, COUNT (t520.c207_set_id) owe
              FROM t520_request t520
             WHERE c520_status_fl = 10
               AND c520_master_request_id IS NULL
               AND t520.c207_set_id IN (SELECT my_temp_txn_id
                                          FROM my_temp_list)
               AND t520.c520_void_fl IS NULL
               AND t520.c520_delete_fl IS NULL
               AND t520.c207_set_id IS NOT NULL
               AND t520.c520_required_date <=
                                            LAST_DAY (ADD_MONTHS (CURRENT_DATE, -1))
               AND t520.c5040_plant_id = v_plant_id
          GROUP BY t520.c207_set_id
          ORDER BY t520.c207_set_id;
   END gm_fch_owe_details;
 /*
 * Description	: Function will get the count of parts that is IN SET, then this will return 'Y' when the part is in set otherwise return 'N'
 * Author		: Rajkumar
 */
	FUNCTION get_part_inset(
		p_partnum	T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE
	)RETURN VARCHAR2
	IS
	v_cnt NUMBER := 0;
	v_retval	VARCHAR2 (20) := 'N';
	BEGIN
		 BEGIN
			SELECT COUNT(*) INTO v_cnt
			   FROM t208_set_details
			  WHERE c205_part_number_id = p_partnum
			    AND c208_inset_fl       = 'Y'
			    AND c208_void_fl       IS NULL;
		  EXCEPTION
		  WHEN NO_DATA_FOUND
		  THEN
		  	v_cnt := 0;
		  END;
		IF v_cnt > 0 
		THEN
			v_retval := 'Y';
		END IF;
		RETURN v_retval;
	END get_part_inset;
	
	 /*******************************************************
   * Description : Procedure to fetch forecast details by Worldwide and OUS
   * Author     : Rajeshwaran
   *******************************************************/
--
   PROCEDURE gm_fch_trend_forecast_lvl_dtl (
   	  p_outheader	     OUT      TYPES.cursor_type,
      p_outdetails       OUT      TYPES.cursor_type
   )
   AS
   
   BEGIN
	   
	 OPEN p_outheader
       FOR
          SELECT TO_CHAR (v9001.month_value, 'Mon YY') forecast_date
            FROM v9001_month_list v9001
           WHERE v9001.month_value BETWEEN TRUNC (CURRENT_DATE, 'MONTH')
                                       AND TRUNC(LAST_DAY (ADD_MONTHS (CURRENT_DATE,2)));   
	OPEN p_outdetails
        FOR
		 		 /*SELECT   t4060.c205_part_number_id ID,
			       t4060.c205_part_number_id NAME,
			       TO_CHAR (t4060.c4060_period, 'Mon YY') forecast_date,
			       NVL (CEIL (SUM (t4060.c4060_qty)), 0) qty
			       ,t4060.c901_level_value lvl_value
				  FROM t4060_part_forecast_qty t4060, 
				  my_temp_part_list mylist
				 WHERE t4060.c4060_period BETWEEN TRUNC (CURRENT_DATE, 'MONTH')
							      AND TRUNC
								    (LAST_DAY
									     (ADD_MONTHS (CURRENT_DATE,
											  2
											 )
									     )
								    ) 
				   AND t4060.C901_FORECAST_TYPE IN  (SELECT tokenii
                                                FROM v_double_in_list
                                               WHERE token = 'TREND') 
			       AND t4060.c901_level_value in ('100800','100824') -- WorldWide,OUS Only
			       AND t4060.c205_part_number_id = mylist.c205_part_number_id
			      GROUP BY t4060.c205_part_number_id, t4060.c4060_period,t4060.c901_level_value
			      ORDER BY t4060.c205_part_number_id, t4060.c4060_period;*/
        
        select us.id ID,us.name NAME,us.id pnum,
        (NVL(us.qty,0)+NVL(ous.qty,0)) qty,		
		us.qty us_qty,ous.qty ous_qty
		from (
		 SELECT   t4060.c205_part_number_id ID,
			       t4060.c205_part_number_id NAME,
			       NVL (CEIL (SUM (t4060.c4060_qty)), 0) qty
                   FROM t4060_part_forecast_qty t4060, 
				  my_temp_part_list mylist
				 WHERE t4060.c4060_period BETWEEN TRUNC (CURRENT_DATE, 'MONTH')
							      AND TRUNC
								    (LAST_DAY
									     (ADD_MONTHS (CURRENT_DATE,
											  2
											 )
									     )
								    ) 
				   AND t4060.C901_FORECAST_TYPE IN  (SELECT tokenii
                                                FROM v_double_in_list
                                               WHERE token = 'TREND')
			       AND t4060.c901_level_value in ('100823') -- US Only
			       AND t4060.c205_part_number_id = mylist.c205_part_number_id                  
			      GROUP BY t4060.c205_part_number_id)us,                 
                   (SELECT   t4060.c205_part_number_id ID,
			       t4060.c205_part_number_id NAME,
			       NVL (CEIL (SUM (t4060.c4060_qty)), 0) qty
			       FROM t4060_part_forecast_qty t4060, 
				  my_temp_part_list mylist
				 WHERE t4060.c4060_period BETWEEN TRUNC (CURRENT_DATE, 'MONTH')
							      AND TRUNC
								    (LAST_DAY
									     (ADD_MONTHS (CURRENT_DATE,
											  2
											 )
									     )
								    ) 
				   AND t4060.C901_FORECAST_TYPE IN  (SELECT tokenii
                                                FROM v_double_in_list
                                               WHERE token = 'TREND')
			       AND t4060.c901_level_value in ('100824') -- OUS Only
			       AND t4060.c205_part_number_id = mylist.c205_part_number_id                   
			      GROUP BY t4060.c205_part_number_id)ous
                  where us.id = ous.id(+)
			      ORDER BY us.id;
  END gm_fch_trend_forecast_lvl_dtl;
  
  
	/**********************************************************
	* Description : Procedure to Fetch the BackOrder Details 
	* Author  : arajan
	**********************************************************/
	PROCEDURE gm_fch_backorder_email (
		        p_rep_id  		IN      t501_order.c703_sales_rep_id%TYPE,
		        p_bo_string  	IN      CLOB,
		        p_out_dtls     	OUT 	TYPES.cursor_type,
		        p_tomail_id		OUT		VARCHAR2)
	AS
	
		v_party_id		t501_order.c703_sales_rep_id%TYPE;
		v_to_email_id	CLOB;
		v_plant_id      t5040_plant_master.c5040_plant_id%TYPE;
		
	BEGIN
		SELECT  NVL(get_plantid_frm_cntx(),get_rule_value('DEFAULT_PLANT','ONEPORTAL')) INTO  v_plant_id FROM DUAL;
		my_context.set_my_inlist_ctx (p_bo_string);
		
		OPEN 
	    p_out_dtls 
	    FOR 
		    SELECT T501.C703_SALES_REP_ID REP_ID,GET_REP_NAME(T501.C703_SALES_REP_ID) SALES_REP_NAME,T501.C501_ORDER_ID ID, 
			 	GET_ACCOUNT_NAME (T501.C704_ACCOUNT_ID) ANAME, GET_DIST_REP_NAME (T501.C703_SALES_REP_ID) REPDISTNM
			 	, TO_CHAR (T501.C501_ORDER_DATE, get_rule_value ('DATEFMT', 'DATEFORMAT')) DT, T501.C501_TOTAL_COST + NVL (T501.C501_SHIP_COST, 0) SALES, GET_PARTNUMBER_PO_PEND_QTY (T502.C205_PART_NUMBER_ID) POPEND
			  	, T501.C704_ACCOUNT_ID ACCID, T501.C501_PARENT_ORDER_ID MASTER_ID, T502.C205_PART_NUMBER_ID NUM
			  	, T502.C502_ITEM_QTY QTY, T502.C502_ITEM_PRICE PRICE, get_partnum_desc (T502.C205_PART_NUMBER_ID) DSC, T501A.C501A_ATTRIBUTE_VALUE attr_val, T501A.C501A_ORDER_ATTRIBUTE_ID attr_id
			  	, (DECODE(T501.C501_TOTAL_COST + NVL (T501.C501_SHIP_COST, 0),
		  	          '0',TO_CHAR(T501.C501_TOTAL_COST + NVL (T501.C501_SHIP_COST, 0),'90.99'),TO_CHAR(T501.C501_TOTAL_COST + NVL (T501.C501_SHIP_COST, 0),'9,999.99'))) TOTSALES
			   FROM T501_ORDER T501, T502_ITEM_ORDER T502, T501A_ORDER_ATTRIBUTE t501a
			WHERE T501.C501_ORDER_ID         = T502.C501_ORDER_ID
			    AND (T501.c901_ext_country_id IS NULL
			    	OR T501.c901_ext_country_id   IN(
			         SELECT country_id FROM v901_country_codes)
			    )
			    AND T501.C901_ORDER_TYPE  = 2525 -- BackOrder
			    AND T501.C501_DELETE_FL  IS NULL
			    AND T501.C501_VOID_FL    IS NULL
			    AND T501.C5040_PLANT_ID = v_plant_id
			 	AND T501.C501_STATUS_FL   = 0
				AND T501.C501_ORDER_ID IN (SELECT TOKEN FROM V_IN_LIST)
				AND T501A.C501_ORDER_ID(+) = T501.C501_ORDER_ID
				AND T501A.C501A_VOID_FL IS NULL
	 			AND T501A.C901_ATTRIBUTE_TYPE (+)=4000764 -- Sales BackOrder Email Time
			ORDER BY T501.C703_SALES_REP_ID,T502.C205_PART_NUMBER_ID, T501.C501_ORDER_DATE ;
	    
			
			
		v_party_id	:= TRIM (get_rep_party_id (p_rep_id));
		v_to_email_id	:= TRIM (gm_pkg_cm_contact.get_contact_value (v_party_id, 90452)); -- to get email id of sales rep
		p_tomail_id		:= v_to_email_id;
		v_to_email_id	:= TRIM (gm_pkg_cm_contact.get_contact_value (v_party_id, 4000716)); -- to get sales Back order email id
		
		IF v_to_email_id != ' '
		THEN
			p_tomail_id		:= p_tomail_id || ','  || v_to_email_id;
		END IF;
		
	END gm_fch_backorder_email;
	
	/**********************************************************
	* Description : Procedure to Fetch the BO List from Acknowledgement Order
	* Author  : Rajkumar
	**********************************************************/
	PROCEDURE gm_fch_ord_ack_bo (
	        p_acc_id  IN      t501_order.c704_account_id%TYPE,
	        p_out     OUT TYPES.cursor_type)
	AS
		v_plant_id		t5040_plant_master.c5040_plant_id%TYPE;
		v_company_id	t1900_company.c1900_company_id%TYPE;
	BEGIN
		 --Getting company id and plant id from context.   
	    SELECT get_plantid_frm_cntx(), get_compid_frm_cntx()
		  INTO v_plant_id, v_company_id
		  FROM dual;
	  
	    OPEN p_out 
	      FOR 
	   SELECT * FROM (
	      	SELECT PNUM, GET_PARTNUM_DESC (pnum) PNUMDESC,
          		ACCID,GET_ACCOUNT_NAME (accid) ACCNAME,CUSTPO,
	               ORDID,NVL(GET_QTY_IN_STOCK (pnum),0) SHELFQTY,ORDQTY,
	               GET_RELEASED_QTY (pnum, ordid, price) shipqty,
	               (ordqty - GET_RELEASED_QTY (pnum, ordid, price)) PENDQTY,
	               GET_ORDER_ATTRIBUTE_VALUE(ordid,'103926') REQUIREDATE 
	               , CREDITTYPE
	        FROM
	      	(SELECT t502.c205_part_number_id pnum, t501.c704_account_id accid,t501.c501_customer_po custpo
	               , t501.c501_order_id ordid,SUM(t502.c502_item_qty) ordqty, t502.c502_item_price price
	               , t704a.attribute_name credittype
	          FROM t501_order t501,t502_item_order t502 ,
	          (SELECT T704a.C704_Account_Id, T901.C901_Code_nm attribute_name, T704a.C704a_Attribute_Value
	           FROM T704a_Account_Attribute T704a, T901_Code_Lookup t901
	          WHERE T704a.c704a_void_fl        IS NULL
	            AND T704a.C704a_Attribute_Value = T901.C901_Code_Id
	            AND T704a.C901_Attribute_Type   = '101184') t704a --101184 = Credit type.
	          WHERE t502.c501_order_id = t501.c501_order_id 
	           AND t501.c901_order_type = '101260' 
	           AND t501.c704_account_id = t704a.C704_Account_Id(+)
	           AND t501.c704_account_id = DECODE(p_acc_id,'',T501.c704_account_id,p_acc_id)
	           AND t501.c501_status_fl <> '3'
	           AND t501.c1900_company_id = v_company_id
	           AND t501.c501_void_fl IS NULL 
	           AND t502.c502_void_fl IS NULL 
	           GROUP BY t502.c205_part_number_id,t501.c704_account_id,t501.c501_customer_po
	           		,t501.c501_order_id,t502.c502_item_price , t704a.attribute_name
	        )) WHERE PENDQTY >0 
	        	ORDER BY PNUM;
	END gm_fch_ord_ack_bo;

END gm_pkg_op_backorder_rpt;
/