/* Formatted on 2011/04/13 15:26 (Formatter Plus v4.8.0) */
--@"C:\database\packages\operations\gm_pkg_op_order_txn.pkg";
CREATE OR REPLACE
PACKAGE gm_pkg_op_order_txn
IS

	/******************************************************************************************************
	* Description : Procedure to release the order from shipping if the order is having only loaner parts.
	*******************************************************************************************************/
	PROCEDURE gm_sav_release_lnorder(
	    p_trans_id   	IN 		t412_inhouse_transactions.c412_inhouse_trans_id%TYPE,
	    p_trans_type	IN		t412_inhouse_transactions.c412_type%TYPE,
	    p_userid     	IN 		t101_user.c101_user_id%TYPE
	);
	
	/************************************************************
	* Description : Procedure to void the Acknowledgement order.
	*************************************************************/
	PROCEDURE gm_void_ack_order(
		  p_ack_order_id   	IN 		t501_order.c501_order_id%TYPE
		, p_userid     		IN 		t101_user.c101_user_id%TYPE
		, p_custom_msg 		OUT		VARCHAR2
	);
	
	/*********************************************************************
	* Description : Procedure to fetch the Acknowledgement order details
	**********************************************************************/
	PROCEDURE gm_fch_order_dtls(
		  p_ack_order_id   	IN 		t501_order.c501_order_id%TYPE
		, p_out_detail		OUT 	TYPES.cursor_type
	);
	
	/************************************************************
	* Description : Procedure to void the Acknowledgement order.
	*************************************************************/
	PROCEDURE gm_void_ack_order_items(
		  p_ack_order_id   	IN 		t501_order.c501_order_id%TYPE
		, p_userid     		IN 		t101_user.c101_user_id%TYPE
	);
	
	/************************************************************************************************
	* Description : Procedure to edit the quantity of Acknowledgement order based on the release qty
	**************************************************************************************************/
	PROCEDURE gm_upd_ack_order_items(
		  p_ack_order_id   	IN 		t501_order.c501_order_id%TYPE
		, p_userid     		IN 		t101_user.c101_user_id%TYPE
		, p_ord_detail_cur	IN		TYPES.cursor_type
	);
	
	/******************************************************************************************************
	* Description : Procedure to save the details of Acknowledegent order from Edit screen
	*******************************************************************************************************/
	PROCEDURE gm_sav_ack_order (
	  p_ordid 		  IN	   t501_order.c501_order_id%TYPE
  	, p_accid 		  IN	   t501_order.c704_account_id%TYPE
	, p_po			  IN	   t501_order.c501_customer_po%TYPE
   	, p_userid		  IN	   t501_order.c501_created_by%TYPE
  	, p_omode 		  IN	   t501_order.c501_receive_mode%TYPE
  	, p_pname 		  IN	   t501_order.c501_received_from%TYPE
	, p_comments	  IN	   t501_order.c501_comments%TYPE
  	, p_parentorderid IN	   t501_order.c501_parent_order_id%TYPE
  	, p_salesrep	  IN	   t501_order.c703_sales_rep_id%TYPE
  	, p_rmqtystr	  IN	   CLOB
  	, p_hard_po	  	  IN	   t501_order.c501_hard_po_fl%TYPE
  	, p_quotestr		 IN  CLOB
  	, p_rmqtymsgstr	  OUT	   CLOB
	);
	
	/******************************************************************************************************
	* Description : Procedure to save the edited quanitiy of Acknowledegent order from Edit screen
	*******************************************************************************************************/
	PROCEDURE gm_sav_order_qty (
		p_ordid 		IN		 t501_order.c501_order_id%TYPE
	  , p_userid		IN		 t501_order.c501_created_by%TYPE
	  , p_rmqtystr		IN		 CLOB
	  , p_rmqtymsgstr	OUT 	 CLOB
	);
	/******************************************************************************************************
	* Description : Procedure to update hold process for orders not completed
	*******************************************************************************************************/	
	PROCEDURE gm_upd_process_hold (
        p_ordid          IN   t501_order.c501_order_id%TYPE
       ,p_userid         IN   t501_order.c501_last_updated_by%TYPE
	);
	
	/******************************************************************************************************
	* Description : Procedure to save the recorded tags for the DO raised from Globus App
	* Author: Karthik
	*******************************************************************************************************/
 	
 	 PROCEDURE gm_save_DO_record_tag_list (
  		p_order_id       IN    t501_order.C501_Order_Id%TYPE,
	    p_record_tag_str IN	   CLOB,
	    p_userid         IN    t501_order.C501_Created_By%TYPE
	);
	
 	/******************************************************************************************************
	* Description : Procedure to save the recorded tags for the DO
	* Author	  : Karthik
	*******************************************************************************************************/
 	
 	 PROCEDURE gm_save_DO_record_tags (
 	    p_tag_id    	 IN	   t5010_tag.c5010_tag_id%TYPE,
 	    p_set_id	     IN    T207_Set_Master.c207_set_id%TYPE,
  		p_order_id       IN    t501_order.C501_Order_Id%TYPE,
	    p_usage_fl   	 IN    t501d_order_tag_usage_details.C501D_TAG_USAGE_FL%TYPE,
	    p_tag_latitude   IN    t501d_order_tag_usage_details.c501d_tag_latitude%TYPE,
	    p_tag_longtitude IN    t501d_order_tag_usage_details.c501d_tag_longtitude%TYPE,
	    p_userid         IN    t501_order.C501_Created_By%TYPE
	);
	
	/**********************************************************************************************************
	* Description :  The below procedure is used to save the tag with Location details from the DO
	* Author	  :  Karthik
	*******************************************************************************************************/
 	
 	  PROCEDURE gm_save_tag_location_details (
 	    p_tag_id    	 IN	   t5010_tag.c5010_tag_id%TYPE,
 	    p_set_id	     IN    T207_Set_Master.c207_set_id%TYPE,
 	    p_order_id       IN    t501_order.C501_Order_Id%TYPE,
	    p_tag_latitude   IN    t501d_order_tag_usage_details.c501d_tag_latitude%TYPE,
	    p_tag_longtitude IN    t501d_order_tag_usage_details.c501d_tag_longtitude%TYPE,
	    p_userid         IN    t501_order.C501_Created_By%TYPE
	);
	
	/**************************************************************************************************************
	* Description :  The below procedure is used to save the log for Location details for the corresponding Tag
	* Author	  :  Karthik
	***********************************************************************************************************/
 	
 	  PROCEDURE gm_save_tag_location_log (
 	    p_tag_id    	 IN	   t5010_tag.c5010_tag_id%TYPE,
 	    p_set_id	     IN    T207_Set_Master.c207_set_id%TYPE,
 	    p_account_id	 IN    T704_ACCOUNT.C704_Account_Id%TYPE,
 	    p_rep_id	     IN    T703_Sales_Rep.C703_SALES_REP_ID%TYPE,
	    p_tag_latitude   IN    T5010B_TAG_LOCATION_DETAILS.C5010B_TAG_LATITUDE%TYPE,
	    p_tag_longtitude IN    T5010B_TAG_LOCATION_DETAILS.C5010B_TAG_LONGTITUDE%TYPE,
	    p_tag_geo_location IN    T5010B_TAG_LOCATION_DETAILS.C5010B_TAG_GEO_LOCATION%TYPE,
	    p_company_id 	   IN    T1900_COMPANY.C1900_COMPANY_ID%TYPE,
	    p_plant_id 		   IN    T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE,
	    p_userid           IN    t501_order.C501_Created_By%TYPE
	);
	
	/******************************************************************************************************
	* Description : Procedure to save the recorded tags for the order raised from Portal
	* Author: Vinoth
	*******************************************************************************************************/
 	
 	 PROCEDURE gm_save_record_tag(
  		p_tag_id    	 IN	   t5010_tag.c5010_tag_id%TYPE,
 	    p_set_id	     IN    T207_Set_Master.c207_set_id%TYPE,
  		p_order_id       IN    t501_order.C501_Order_Id%TYPE,
  		p_usage_fl       IN    t501d_order_tag_usage_details.c501d_tag_usage_fl%TYPE DEFAULT NULL,
  		p_tag_latitude   IN    t501d_order_tag_usage_details.c501d_tag_latitude%TYPE DEFAULT NULL,
		p_tag_longtitude IN    t501d_order_tag_usage_details.c501d_tag_longtitude%TYPE DEFAULT NULL,
  		p_userid         IN    t501_order.C501_Created_By%TYPE
   );
   
  /******************************************************************************************************
  * Description : Procedure to save the recorded tags for Bulk DO
  * Author	  : Yoga
  * PC  : 3534
  *******************************************************************************************************/
 	
  PROCEDURE gm_save_DO_record_tags (
    p_tag_id    	 IN	   t5010_tag.c5010_tag_id%TYPE,
    p_set_id	     IN    T207_Set_Master.c207_set_id%TYPE,
    p_order_id       IN    t501_order.C501_Order_Id%TYPE,
    p_usage_fl   	 IN    t501d_order_tag_usage_details.C501D_TAG_USAGE_FL%TYPE,
    p_tag_latitude   IN    t501d_order_tag_usage_details.c501d_tag_latitude%TYPE,
    p_tag_longtitude IN    t501d_order_tag_usage_details.c501d_tag_longtitude%TYPE,
    p_userid         IN    t501_order.C501_Created_By%TYPE,
    p_system_id        IN   t207_set_master.C207_set_id%TYPE,
    p_system_usage    IN t501d_order_tag_usage_details.c501d_system_usage%TYPE,
    p_order_category    IN t501d_order_tag_usage_details.c901_order_Category%TYPE
  );
  
  /******************************************************************************************************
	* Description : Procedure to save the recorded tags for the order raised from Portal if the order is valid
	* Author: Paveethra
	*******************************************************************************************************/
 	 PROCEDURE gm_save_do_record_tag_dtls(
  		p_tag_id    	 IN	   t5010_tag.c5010_tag_id%TYPE,
 	    p_set_id	     IN    T207_Set_Master.c207_set_id%TYPE,
  		p_order_id       IN    t501_order.C501_Order_Id%TYPE,
  		p_usage_fl       IN    t501d_order_tag_usage_details.c501d_tag_usage_fl%TYPE DEFAULT NULL,
  		p_tag_latitude   IN    t501d_order_tag_usage_details.c501d_tag_latitude%TYPE DEFAULT NULL,
		p_tag_longtitude IN    t501d_order_tag_usage_details.c501d_tag_longtitude%TYPE DEFAULT NULL,
  		p_userid         IN    t501_order.C501_Created_By%TYPE
);
 
END gm_pkg_op_order_txn;
/
