-- @"C:\Database\Packages\Sync\Device\System\gm_pkg_device_set_sync_to_temp.bdy";
CREATE OR REPLACE PACKAGE BODY GM_PKG_DEVICE_SET_SYNC_TO_TEMP
IS
    /**********************************************************************************************************
    Description : This procedure is used to inserting set related sync data into temp table
    Author      : jgurunathan
    ***********************************************************************************************************/
PROCEDURE GM_SAV_SET_MASTER_SYNC_MAIN
AS
    v_temp_count NUMBER;
    --
    v_start_time DATE;
BEGIN
	--
	v_start_time := CURRENT_DATE;
	--
	BEGIN
	--
     DELETE FROM MY_TEMP_SET_SYNC_UPDATE;
    
    --1 to update the ipad flag as processing  
    -- ref type 103091 set, set group type 1601
    GM_PKG_DEVICE_COMMON_SYNC.GM_UPD_MASTER_SYNC_FLAG (1601, 'P', 103091, '30301') ;
    COMMIT;
    
    --2 to store set master details to temp table
    gm_pkg_device_set_sync_to_temp.GM_SAV_SET_MASTER_TEMP (103091, 1601) ;
    COMMIT;
    
    --3 to store set release details to temp table
    gm_pkg_device_set_sync_to_temp.GM_SAV_SET_RELEASE_TEMP (103091, 1601) ;
    COMMIT;
    
    --4 to store set language details to temp table
    gm_pkg_device_set_sync_to_temp.GM_SAV_SET_LANGUAGE_TEMP (103091, 1601) ;
    COMMIT;
    
    
    --5 checking the count in temp table and called the master JSON
     SELECT COUNT (1) INTO v_temp_count FROM MY_TEMP_SET_SYNC_UPDATE;
    IF v_temp_count > 0 THEN
        GM_PKG_DEVICE_SET_SYNC_TO_MASTER.GM_SAV_SET_MASTER_JSON_MAIN;
        COMMIT;
    END IF;
    
    --6 to update the set attribute details to JSON master table
    GM_PKG_DEVICE_SET_SYNC_TO_MASTER.GM_SAV_SET_ATTRIBUTE_JSON_MAIN;
    COMMIT;
    
    --7 to update the set details to JSON master table 
    GM_PKG_DEVICE_SET_SYNC_TO_MASTER.GM_SAV_SET_DETAILS_JSON_MAIN;
    COMMIT;
     --
    EXCEPTION WHEN OTHERS
    THEN
      dbms_output.put_line (' Error GM_SAV_SET_MASTER_SYNC_MAIN @@@@@@@@@ ****** ' || SQLERRM||chr(10)||dbms_utility.format_error_backtrace);
      -- 8 to notify the error 
      gm_pkg_cm_job.gm_cm_notify_load_info (v_start_time, CURRENT_DATE, 'E', SQLERRM||chr(10)||dbms_utility.format_error_backtrace, 'gm_pkg_device_set_sync_to_temp.gm_sav_set_master_sync_main') ;
    END;
    --
END GM_SAV_SET_MASTER_SYNC_MAIN;

/**********************************************************************************************************
Description : This procedure is used to inserting set master table sync data into temporary table
Author      : jgurunathan
***********************************************************************************************************/
PROCEDURE GM_SAV_SET_MASTER_TEMP (
        p_ref_type     IN T2000_MASTER_METADATA.C901_REF_TYPE%TYPE,
        p_set_grp_type IN T207_SET_MASTER.C901_SET_GRP_TYPE%TYPE)
AS
BEGIN
	--
     INSERT
       INTO MY_TEMP_SET_SYNC_UPDATE
        (
            C207_SET_ID, C1900_COMPANY_ID, C901_LANGUAGE_ID
          , C207_SET_NAME, C207_SET_DESC, C207_SET_DTLS_DESC
          , C901_SET_STATUS, C901_SET_TYPE, C207_SYSTEM_ID
          , C901_CATEGORY_ID, C207_TYPE, C207_HIERARCHY_ID
          , C207_IPAD_VOID_FL
        )
     SELECT T207.C207_SET_ID, T2080.C1900_COMPANY_ID, T2000.C901_LANGUAGE_ID
      , T2000.C2000_REF_NAME, T2000.C2000_REF_DESC, T2000.C2000_REF_DTL_DESC
      , T207.C901_STATUS_ID, T207.C901_SET_GRP_TYPE, T207.C207_SET_SALES_SYSTEM_ID
      , T207.C207_CATEGORY, T207.C207_TYPE, T207.C901_HIERARCHY
      , DECODE (T2080.C901_IPAD_RELEASED_STATUS, '105361', T2080.C2080_VOID_FL, 'Y')
       FROM T2000_MASTER_METADATA T2000, T2080_SET_COMPANY_MAPPING T2080, T1900_COMPANY T1900
      , T207_SET_MASTER T207
      WHERE T1900.C1900_COMPANY_ID = T2080.C1900_COMPANY_ID
        AND T2080.C207_SET_ID      = T2000.C2000_REF_ID
        AND T207.C207_SET_ID       = T2080.C207_SET_ID
        AND T207.C207_SET_ID       = T2000.C2000_REF_ID
        AND T2000.C901_LANGUAGE_ID = gm_pkg_device_common_sync.get_ipad_lang_id (103091, T2080.C207_SET_ID,
        T1900.C901_LANGUAGE_ID)
        AND T2000.C901_REF_TYPE        = p_ref_type
        AND T207.C901_SET_GRP_TYPE     = p_set_grp_type
        AND T207.C207_IPAD_SYNCED_FLAG = 'P'
        AND T1900.C1900_VOID_FL       IS NULL;
       -- 
END GM_SAV_SET_MASTER_TEMP;
/**********************************************************************************************************
Description : This procedure is used to inserting set release table sync data into temporary table
Author      : jgurunathan
***********************************************************************************************************/
PROCEDURE GM_SAV_SET_RELEASE_TEMP (
        p_ref_type     IN T2000_MASTER_METADATA.C901_REF_TYPE%TYPE,
        p_set_grp_type IN T207_SET_MASTER.C901_SET_GRP_TYPE%TYPE)
AS
BEGIN
	--
     INSERT
       INTO MY_TEMP_SET_SYNC_UPDATE
        (
            C207_SET_ID, C1900_COMPANY_ID, C901_LANGUAGE_ID
          , C207_SET_NAME, C207_SET_DESC, C207_SET_DTLS_DESC
          , C901_SET_STATUS, C901_SET_TYPE, C207_SYSTEM_ID
          , C901_CATEGORY_ID, C207_TYPE, C207_HIERARCHY_ID
          , C207_IPAD_VOID_FL
        )
     SELECT T207.C207_SET_ID, T2080.C1900_COMPANY_ID, T2000.C901_LANGUAGE_ID
      , T2000.C2000_REF_NAME, T2000.C2000_REF_DESC, T2000.C2000_REF_DTL_DESC
      , T207.C901_STATUS_ID, T207.C901_SET_GRP_TYPE, T207.C207_SET_SALES_SYSTEM_ID
      , T207.C207_CATEGORY, T207.C207_TYPE, T207.C901_HIERARCHY
      , DECODE (T2080.C901_IPAD_RELEASED_STATUS, '105361', T2080.C2080_VOID_FL, 'Y')
       FROM T2000_MASTER_METADATA T2000, T2080_SET_COMPANY_MAPPING T2080, T1900_COMPANY T1900
      , T207_SET_MASTER T207
      WHERE T1900.C1900_COMPANY_ID = T2080.C1900_COMPANY_ID
        AND T2080.C207_SET_ID      = T2000.C2000_REF_ID
        AND T207.C207_SET_ID       = T2080.C207_SET_ID
        AND T207.C207_SET_ID       = T2000.C2000_REF_ID
        AND T2000.C901_LANGUAGE_ID = gm_pkg_device_common_sync.get_ipad_lang_id (103091, T2080.C207_SET_ID,
        T1900.C901_LANGUAGE_ID)
        AND T2000.C901_REF_TYPE          = p_ref_type
        AND T207.C901_SET_GRP_TYPE       = p_set_grp_type
        AND T2080.C2080_IPAD_SYNCED_FLAG = 'P'
        AND T1900.C1900_VOID_FL         IS NULL
        -- to avoid the duplicate set
        AND (T2080.C207_SET_ID) NOT     IN
        (
             SELECT C207_SET_ID FROM MY_TEMP_SET_SYNC_UPDATE
        ) ;
       -- 
END GM_SAV_SET_RELEASE_TEMP;
/**********************************************************************************************************
Description : This procedure is used to inserting set language table sync data into temporary table
Author      : jgurunathan
***********************************************************************************************************/
PROCEDURE GM_SAV_SET_LANGUAGE_TEMP (
        p_ref_type     IN T2000_MASTER_METADATA.C901_REF_TYPE%TYPE,
        p_set_grp_type IN T207_SET_MASTER.C901_SET_GRP_TYPE%TYPE)
AS
BEGIN
	--
     INSERT
       INTO MY_TEMP_SET_SYNC_UPDATE
        (
            C207_SET_ID, C1900_COMPANY_ID, C901_LANGUAGE_ID
          , C207_SET_NAME, C207_SET_DESC, C207_SET_DTLS_DESC
          , C901_SET_STATUS, C901_SET_TYPE, C207_SYSTEM_ID
          , C901_CATEGORY_ID, C207_TYPE, C207_HIERARCHY_ID
          , C207_IPAD_VOID_FL
        )
     SELECT T207.C207_SET_ID, T2080.C1900_COMPANY_ID, T2000.C901_LANGUAGE_ID
      , T2000.C2000_REF_NAME, T2000.C2000_REF_DESC, T2000.C2000_REF_DTL_DESC
      , T207.C901_STATUS_ID, T207.C901_SET_GRP_TYPE, T207.C207_SET_SALES_SYSTEM_ID
      , T207.C207_CATEGORY, T207.C207_TYPE, T207.C901_HIERARCHY
      , DECODE (T2080.C901_IPAD_RELEASED_STATUS, '105361', T2080.C2080_VOID_FL, 'Y')
       FROM T2000_MASTER_METADATA T2000, T2080_SET_COMPANY_MAPPING T2080, T1900_COMPANY T1900
      , T207_SET_MASTER T207
      WHERE T1900.C1900_COMPANY_ID = T2080.C1900_COMPANY_ID
        AND T2080.C207_SET_ID      = T2000.C2000_REF_ID
        AND T207.C207_SET_ID       = T2080.C207_SET_ID
        AND T207.C207_SET_ID       = T2000.C2000_REF_ID
        AND T2000.C901_LANGUAGE_ID = gm_pkg_device_common_sync.get_ipad_lang_id (103091, T2080.C207_SET_ID,
        T1900.C901_LANGUAGE_ID)
        AND T2000.C901_REF_TYPE                              = p_ref_type
        AND T207.C901_SET_GRP_TYPE                           = p_set_grp_type
        AND T2080.C901_IPAD_RELEASED_STATUS                  = 105361 --<released value>
        AND T2000.C2000_IPAD_SYNCED_FLAG                     = 'P'
        AND T1900.C1900_VOID_FL                             IS NULL
        -- to avoid duplicate set and company id
        AND (T2080.C207_SET_ID, T2080.C1900_COMPANY_ID) NOT IN
        (
             SELECT C207_SET_ID, C1900_COMPANY_ID FROM MY_TEMP_SET_SYNC_UPDATE
        ) ;
        --
END GM_SAV_SET_LANGUAGE_TEMP;
END GM_PKG_DEVICE_SET_SYNC_TO_TEMP;
/