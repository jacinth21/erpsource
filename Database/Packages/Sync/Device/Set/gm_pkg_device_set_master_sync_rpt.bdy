/* Formatted on 2019/11/19 15:32 (Formatter Plus v4.8.0) */
-- @"C:\database\Packages\Sync\Set\gm_pkg_device_set_master_sync_rpt.bdy"

CREATE OR REPLACE PACKAGE BODY gm_pkg_device_set_master_sync_rpt
IS

	/************************************************************
 	* Description : Procedure used to fetch the set sync detail
 	* Author      : Ramachandiran T.S
	**************************************************************/

PROCEDURE gm_fch_sets_prodcat  (
		p_token  		IN 		t101a_user_token.c101a_token_id%TYPE,
		p_langid		IN		NUMBER,
		p_page_no		IN		NUMBER,
	    p_uuid		    IN      t9150_device_info.c9150_device_uuid%TYPE,
	    p_comp_id	    IN	    t1900_company.c1900_company_id%TYPE,
		p_out_system	OUT		CLOB
	)
	AS
	
	v_device_infoid    			t9150_device_info.c9150_device_info_id%TYPE;
    v_sync_count       			NUMBER;
    v_set_header       			CLOB;
    v_set_out_dtl       		CLOB;
    v_json_out         			JSON_OBJECT_T;
    v_device_last_sync_date 	t9151_device_sync_dtl.c9151_last_sync_date%TYPE;
    keys_string        			CLOB;  
   	BEGIN   
	   	--get the device info id based on user token
		v_device_infoid := get_device_id(p_token, p_uuid);
		--get the device update details based on sync type
		gm_pkg_device_sync_update_dtls.gm_fch_device_updates(v_device_infoid,p_langid,103111,v_device_last_sync_date,v_sync_count);  --103111 - Set Device Sync Type 
		
		IF v_sync_count = 0 THEN
      		gm_pkg_device_set_sync_rpt.gm_fch_all_set_tosync(p_comp_id,1601,v_set_out_dtl); -- 1601 - Set Report Grp
     	ELSE
      		gm_pkg_device_set_sync_rpt.gm_fch_set_update_tosync(p_comp_id,1601,v_device_last_sync_date,v_set_out_dtl); -- 1601 - Set Report Grp
    	END IF;  
 --Get the HeaderDetails
  BEGIN
  SELECT replace(JSON_OBJECT( 'pagesize'    VALUE TO_CHAR(COUNT(1)),
					  'totalsize'   VALUE TO_CHAR(COUNT(1)),
					  'pageno'      VALUE TO_CHAR(1),
					  'totalpages'  VALUE TO_CHAR(1),
					  'token'       VALUE TO_CHAR(p_token),
					  'langid'      VALUE TO_CHAR(p_langid),
					  'reftype'     VALUE TO_CHAR(103111), --set Ref Type
					  'svrcnt'      VALUE '', 'lastrec' VALUE '' ),'null','""')
	INTO v_set_header
	FROM t207s_set_master_sync t207s
  	WHERE t207s.c1900_company_id     	= p_comp_id
 	AND t207s.c901_set_group_type       = 1601-- 1601 - Set Report Group
 	AND t207s.c207s_void_fl  IS NULL;
 	
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    v_set_header := NULL;
  END;
		
  --Get the Set string and form the JSON object as an array
	keys_string := v_set_out_dtl;
	keys_string   := REPLACE(keys_string , '[[' , '['); 
    keys_string   := REPLACE(keys_string , '],[' , ',');
    keys_string   := REPLACE(keys_string , ']]' , ']');
  	v_json_out  := JSON_OBJECT_T.parse(v_set_header);
  	
  -- Check the JSON Array is not null and put the JSON Array value in JSON_OBJECT_T 
  IF keys_string IS NOT NULL THEN
    v_json_out.put('listGmSetMasterVO',JSON_ARRAY_T(keys_string));
  ELSE
    v_json_out.put('listGmSetMasterVO',JSON_ARRAY_T());
  END IF; 
  --Change the JSON OBJECT to Clob data type
  p_out_system := v_json_out.to_clob;
END gm_fch_sets_prodcat;	

	/**********************************************************************
 	* Description : Procedure used to fetch the set attribute sync detail
 	* Author      : Ramachandiran T.S
	************************************************************************/
PROCEDURE gm_fch_setattr_tosync  (
		p_token  		IN 		t101a_user_token.c101a_token_id%TYPE,
		p_langid		IN		NUMBER,
		p_page_no		IN		NUMBER,
	    p_uuid		    IN      t9150_device_info.c9150_device_uuid%TYPE,
	    p_comp_id	    IN	    t1900_company.c1900_company_id%TYPE,
		p_out_system	OUT		CLOB
	)
	AS
	v_device_infoid    			t9150_device_info.c9150_device_info_id%TYPE;
    v_sync_count       			NUMBER;
    v_set_header       			CLOB;
    v_set_out_dtl       		CLOB;
    v_json_out         			JSON_OBJECT_T;
    v_device_last_sync_date 	t9151_device_sync_dtl.c9151_last_sync_date%TYPE;
    keys_string        			CLOB;
    BEGIN
	    --get the device info id based on user token
		v_device_infoid := get_device_id(p_token, p_uuid);
		--get the device update details based on sync type
		gm_pkg_device_sync_update_dtls.gm_fch_device_updates(v_device_infoid,p_langid,4000736,v_device_last_sync_date,v_sync_count);  --4000736 - Set attribute Device Sync Type 	
		
		IF v_sync_count = 0 THEN
      		gm_pkg_device_set_sync_rpt.gm_fch_all_set_attribute_tosync(p_comp_id,1601,v_set_out_dtl); -- 1601 - Set Report Grp
     	ELSE
     		gm_pkg_device_set_sync_rpt.gm_fch_set_attribute_update_to_sync(p_comp_id,1601,v_device_last_sync_date,v_set_out_dtl); -- 1601 - Set Report Grp
    	END IF;
    --Get the HeaderDetails
  BEGIN
  SELECT replace(JSON_OBJECT( 'pagesize'    VALUE TO_CHAR(COUNT(1)),
					  'totalsize'   VALUE TO_CHAR(COUNT(1)),
					  'pageno'      VALUE TO_CHAR(1),
					  'totalpages'  VALUE TO_CHAR(1),
					  'token'       VALUE TO_CHAR(p_token),
					  'langid'      VALUE TO_CHAR(p_langid),
					  'reftype'     VALUE TO_CHAR(4000736), --set attribute Ref Type
					  'svrcnt'      VALUE '', 'lastrec' VALUE '' ),'null','""')
	INTO v_set_header
	FROM t207s_set_attribute_sync t207sa, t207s_set_master_sync t207s
  	WHERE t207s.c207_set_id				= t207sa.c207_set_id
  	AND t207s.c1900_company_id     		=  p_comp_id
  	AND t207sa.c901_set_group_type    	=  1601 -- 1601 - Set Report Grp
  	AND t207s.c207s_void_fl  IS NULL
 	AND t207sa.c207s_void_fl IS NULL;
  
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    v_set_header := NULL;
  END;
		
  --Get the Set string and form the JSON object as an array
  	keys_string := v_set_out_dtl;
  	keys_string   := REPLACE(keys_string , '[[' , '['); 
    keys_string   := REPLACE(keys_string , '],[' , ',');
    keys_string   := REPLACE(keys_string , ']]' , ']');
  	v_json_out  := JSON_OBJECT_T.parse(v_set_header);
  	 
  -- Check the JSON Array is not null and put the JSON Array value in JSON_OBJECT_T
  IF keys_string IS NOT NULL THEN
    v_json_out.put('listGmSetAttributeVO',JSON_ARRAY_T(keys_string));
     ELSE
    v_json_out.put('listGmSetAttributeVO',JSON_ARRAY_T());
  END IF;
  --Change the JSON OBJECT to Clob data type
  p_out_system := v_json_out.to_clob;
  END gm_fch_setattr_tosync;
   
   
   /**********************************************************************
 	* Description : Procedure used to fetch the set part sync detail
 	* Author      : Ramachandiran T.S
	************************************************************************/

PROCEDURE gm_fch_setpartdtls_prodcat  (
		p_token  		IN 		t101a_user_token.c101a_token_id%TYPE,
		p_langid		IN		NUMBER,
		p_page_no		IN		NUMBER,
	    p_uuid		    IN      t9150_device_info.c9150_device_uuid%TYPE,
	    p_comp_id	    IN	    t1900_company.c1900_company_id%TYPE,
		p_out_system	OUT		CLOB
	)
	AS
	v_device_infoid    			t9150_device_info.c9150_device_info_id%TYPE;
    v_sync_count       			NUMBER;
    v_set_header       			CLOB;
    v_set_out_dtl       		CLOB;
    v_json_out         			JSON_OBJECT_T;
    v_device_last_sync_date 	t9151_device_sync_dtl.c9151_last_sync_date%TYPE;
    keys_string        			CLOB;
    
    BEGIN
	    	--get the device info id based on user token
		v_device_infoid := get_device_id(p_token, p_uuid);
		--get the device update details based on sync type
		gm_pkg_device_sync_update_dtls.gm_fch_device_updates(v_device_infoid,p_langid,4000409,v_device_last_sync_date,v_sync_count);  --4000409 - Set attribute Device Sync Type 
		IF v_sync_count = 0 THEN
    		gm_pkg_device_set_sync_rpt.gm_fch_all_set_part_tosync(p_comp_id,1601,v_set_out_dtl); -- 1601 - Set Report Grp
     	ELSE
      		gm_pkg_device_set_sync_rpt.gm_fch_set_part_update_tosync(p_comp_id,1601,v_device_last_sync_date,v_set_out_dtl); -- 1601 - Set Report Grp
    	END IF;
    --Get the HeaderDetails
  BEGIN
  SELECT replace(JSON_OBJECT( 'pagesize'    VALUE TO_CHAR(COUNT(1)),
					  'totalsize'   VALUE TO_CHAR(COUNT(1)),
					  'pageno'      VALUE TO_CHAR(1),
					  'totalpages'  VALUE TO_CHAR(1),
					  'token'       VALUE TO_CHAR(p_token),
					  'langid'      VALUE TO_CHAR(p_langid),
					  'reftype'     VALUE TO_CHAR(4000409), --set part Ref Type
					  'svrcnt'      VALUE '', 'lastrec' VALUE '' ),'null','""')
	INTO v_set_header
	FROM t207s_set_master_sync t207s, t207s_set_details_sync t207sd
  	WHERE t207s.c207_set_id 			= t207sd.c207_set_id
    AND t207s.c901_set_group_type      	= 1601 -- 1601 - Set Report Grp
    AND t207s.c1900_company_id     		= p_comp_id
    AND t207s.c207s_void_fl  IS NULL
    AND t207sd.c207s_void_fl IS NULL;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    v_set_header := NULL;
  END;	
  --Get the Set string and form the JSON object as an array
  keys_string := v_set_out_dtl;
   keys_string   := REPLACE(keys_string , '[[' , '['); 
   keys_string   := REPLACE(keys_string , '],[' , ',');
   keys_string   := REPLACE(keys_string , ']]' , ']');  
  v_json_out  := JSON_OBJECT_T.parse(v_set_header);
  -- Check the JSON Array is not null and put the JSON Array value in JSON_OBJECT_T
  IF keys_string IS NOT NULL THEN
    v_json_out.put('listGmSetPartVO',JSON_ARRAY_T(keys_string));
    ELSE
    v_json_out.put('listGmSetPartVO',JSON_ARRAY_T());
  END IF;
  --Change the JSON OBJECT to Clob data type
  p_out_system := v_json_out.to_clob;
   END gm_fch_setpartdtls_prodcat;
END gm_pkg_device_set_master_sync_rpt;
/