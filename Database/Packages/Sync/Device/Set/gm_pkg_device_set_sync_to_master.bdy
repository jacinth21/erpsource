-- @"C:\Database\Packages\Sync\Device\System\gm_pkg_device_set_sync_to_master.bdy";
CREATE OR REPLACE PACKAGE BODY GM_PKG_DEVICE_SET_SYNC_TO_MASTER
IS
    /******************************************************************
    Description : This procedure is used for forming JSON data from temporary table into Set Master Sync Table
    Author      : jgurunathan
    ****************************************************************/
PROCEDURE GM_SAV_SET_MASTER_JSON_MAIN
AS
    CURSOR set_master_sync_data
    IS
        --forming JSON data from temp table
         SELECT C207_SET_ID SET_ID, C1900_COMPANY_ID COMPID, C901_LANGUAGE_ID LANGID
          , C901_SET_TYPE SETTYPE, JSON_OBJECT ('sid' VALUE C207_SET_ID, 'snm' VALUE C207_SET_NAME, 'sdc' VALUE
            C207_SET_DESC, 'sdd' VALUE C207_SET_DTLS_DESC, 'sts' VALUE TO_CHAR(C901_SET_STATUS), 'ssi' VALUE C207_SYSTEM_ID,
            'cid' VALUE TO_CHAR(C901_CATEGORY_ID), 'typ' VALUE TO_CHAR(C207_TYPE), 'hid' VALUE C207_HIERARCHY_ID, 'vfl' VALUE
            C207_IPAD_VOID_FL) MASTER_SYNC_DATA
           FROM MY_TEMP_SET_SYNC_UPDATE;
BEGIN
	--
    FOR sync_data IN set_master_sync_data
    LOOP
        -- '30301' Manual Load,  103122 Newly inserted
        --save to master table
        GM_PKG_DEVICE_SYSTEM_SYNC_TO_MASTER.GM_SAV_SET_MASTER_JSON (sync_data.SET_ID, sync_data.COMPID,
        sync_data.LANGID, sync_data.MASTER_SYNC_DATA, sync_data.SETTYPE, '30301', 103122) ;
        --
    END LOOP;
    
    ---update iPad Synced Flag
     UPDATE T207_SET_MASTER
    SET C207_IPAD_SYNCED_FLAG     = 'Y', C207_IPAD_SYNCED_DATE = CURRENT_DATE
      WHERE C207_IPAD_SYNCED_FLAG = 'P'
        AND C207_SET_ID          IN
        (
             SELECT C207_SET_ID FROM MY_TEMP_SET_SYNC_UPDATE
        ) ;
        
     --   
     UPDATE T2000_MASTER_METADATA
    SET C2000_IPAD_SYNCED_FLAG                = 'Y', C2000_IPAD_SYNCED_DATE = CURRENT_DATE
      WHERE C2000_IPAD_SYNCED_FLAG            = 'P'
        AND (C2000_REF_ID, C901_LANGUAGE_ID) IN
        (
             SELECT C207_SET_ID, C901_LANGUAGE_ID FROM MY_TEMP_SET_SYNC_UPDATE
        ) ;
        
     --   
     UPDATE T2080_SET_COMPANY_MAPPING
    SET C2080_IPAD_SYNCED_FLAG               = 'Y', C2080_IPAD_SYNCED_DATE = CURRENT_DATE
      WHERE C2080_IPAD_SYNCED_FLAG           = 'P'
        AND (C207_SET_ID, C1900_COMPANY_ID) IN
        (
             SELECT C207_SET_ID, C1900_COMPANY_ID FROM MY_TEMP_SET_SYNC_UPDATE
        ) ;
        
    --Delete temp table
     DELETE FROM MY_TEMP_SET_SYNC_UPDATE;
END GM_SAV_SET_MASTER_JSON_MAIN;

/************************************************************************************************
Description :This procedure is used for forming JSON data from temporary table into Set  Attr Sync Table
Author      : ppandiyan
*************************************************************************************************/
PROCEDURE GM_SAV_SET_ATTRIBUTE_JSON_MAIN
AS
    CURSOR set_master_attr_data
    IS
        --getting comma separated all attribute values for single set
         SELECT T207C.C207_SET_ID SETID, JSON_ARRAYAGG (JSON_OBJECT ('sid' VALUE T207C.C207_SET_ID, 'att' VALUE
            TO_CHAR(T207C.C901_ATTRIBUTE_TYPE), 'atv' VALUE T207C.C207C_ATTRIBUTE_VALUE, 'vfl' VALUE T207C.C207C_VOID_FL)
            RETURNING CLOB) ATTR_JSON_DATA
           FROM T207C_SET_ATTRIBUTE T207C, T207_SET_MASTER T207
          WHERE T207C.C207_SET_ID      = T207.C207_SET_ID
            AND T207.C901_SET_GRP_TYPE = 1601--Set
            AND T207C.C207C_IPAD_SYNCED_FLAG = 'P'
   		GROUP BY T207C.C207_SET_ID;
BEGIN
	
    --looping the cursor to save attribute data
    FOR attr_data IN set_master_attr_data
    LOOP
        GM_PKG_DEVICE_SYSTEM_SYNC_TO_MASTER.GM_SAV_SET_ATTRIBUTE_JSON (attr_data.SETID, attr_data.ATTR_JSON_DATA, 1601,
        --Set
        '30301') ;--Manual Load
    END LOOP;
    
    -- update set attribute table
     UPDATE T207C_SET_ATTRIBUTE
    SET C207C_IPAD_SYNCED_FLAG      = 'Y', C207C_IPAD_SYNCED_DATE = CURRENT_DATE
      WHERE C207C_IPAD_SYNCED_FLAG = 'P'
        AND C207_SET_ID            IN
        (
             SELECT T207C.C207_SET_ID
               FROM T207C_SET_ATTRIBUTE T207C, T207_SET_MASTER T207
              WHERE T207C.C207_SET_ID             = T207.C207_SET_ID
                AND T207.C901_SET_GRP_TYPE        = 1601--Set
                AND T207C.C207C_IPAD_SYNCED_FLAG IS NULL
           GROUP BY T207C.C207_SET_ID
        ) ;
        
END GM_SAV_SET_ATTRIBUTE_JSON_MAIN;

/************************************************************************************************
Description :This procedure is used for forming JSON data from temporary table into Set Detail Sync Table
Author      : ppandiyan
*************************************************************************************************/
PROCEDURE GM_SAV_SET_DETAILS_JSON_MAIN
AS
    CURSOR set_master_dtl_data
    IS
        --getting comma separated set details for single set
         SELECT T208.C207_SET_ID SETID, (JSON_ARRAYAGG (JSON_OBJECT ('sid' VALUE T208.C207_SET_ID,
            'pnm' VALUE T208.C205_PART_NUMBER_ID, 'qty' VALUE TO_CHAR(T208.C208_SET_QTY), 'ifl' VALUE T208.C208_INSET_FL, 'vfl'
            VALUE T208.C208_VOID_FL) RETURNING CLOB)) DETAIL_JSON_DATA
           FROM T208_SET_DETAILS T208, T207_SET_MASTER T207
          WHERE T208.C207_SET_ID       = T207.C207_SET_ID
            AND T207.C901_SET_GRP_TYPE = 1601--Set
            AND C208_IPAD_SYNCED_FLAG  = 'P'
	    GROUP BY T208.C207_SET_ID;
BEGIN
	
    -- --looping the cursor to save JSON Detail Data
    FOR dtls_data IN set_master_dtl_data
    LOOP
        GM_SAV_SET_DETAILS_JSON (dtls_data.SETID, dtls_data.DETAIL_JSON_DATA, '30301') ;--Manual Load
    END LOOP;
    
    -- update set details table
     UPDATE T208_SET_DETAILS
    SET C208_IPAD_SYNCED_FLAG      = 'Y', C208_IPAD_SYNCED_DATE = CURRENT_DATE
      WHERE C208_IPAD_SYNCED_FLAG = 'P'
        AND C207_SET_ID           IN
        (
             SELECT T207.C207_SET_ID
               FROM T207_SET_MASTER T207
              WHERE T207.C901_SET_GRP_TYPE      = 1601--Set
        ) ;
     --   
END GM_SAV_SET_DETAILS_JSON_MAIN;

/************************************************************************************************
Description : This procedure is used to insert / update into Set Details Table
Author      : jgurunathan
*************************************************************************************************/
PROCEDURE GM_SAV_SET_DETAILS_JSON (
        p_set_id       IN T207S_SET_DETAILS_SYNC.C207_SET_ID%TYPE,
        p_set_dtl_json IN T207S_SET_DETAILS_SYNC.C207S_SET_DETAILS_SYNC_DATA%TYPE,
        p_user_id      IN T207S_SET_DETAILS_SYNC.C207S_LAST_UPDATED_BY%TYPE)
AS
BEGIN
	--
     UPDATE T207S_SET_DETAILS_SYNC
    SET C207S_SET_DETAILS_SYNC_DATA = p_set_dtl_json, C207S_LAST_UPDATED_BY = p_user_id, C207S_LAST_UPDATED_DATE =
        CURRENT_DATE
      WHERE C207_SET_ID    = p_set_id
        AND C207S_VOID_FL IS NULL;
        
    --    
    IF (SQL%ROWCOUNT       = 0) THEN
         INSERT
           INTO T207S_SET_DETAILS_SYNC
            (
                C207_SET_ID, C207S_SET_DETAILS_SYNC_DATA, C207S_VOID_FL
              , C207S_LAST_UPDATED_BY, C207S_LAST_UPDATED_DATE
            )
            VALUES
            (
                p_set_id, p_set_dtl_json, NULL
              , p_user_id, CURRENT_DATE
            ) ;
    END IF;
    --
END GM_SAV_SET_DETAILS_JSON;

END GM_PKG_DEVICE_SET_SYNC_TO_MASTER;
/