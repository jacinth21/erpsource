/* Formatted on 2019/11/19 15:32 (Formatter Plus v4.8.0) */
-- @"C:\database\Packages\Sync\Set\gm_pkg_device_set_sync_rpt.bdy"

CREATE OR REPLACE PACKAGE BODY gm_pkg_device_set_sync_rpt
IS
/*****************************************************************
 * Description : Procedure to fetch all set master data to sync
 * Author      : Ramachandiran T.S
*******************************************************************/

PROCEDURE gm_fch_all_set_tosync(
	  p_comp_id	     	IN	     t207s_set_master_sync.c1900_company_id%TYPE, 
	  p_system_type	 	IN       t207s_set_master_sync.c901_set_group_type%TYPE, 
	  p_set_out_dtl  	OUT      CLOB
	)
	AS
	
	v_set_out_dtl CLOB;
			
BEGIN		--Get the all system details based on company
  BEGIN
  	SELECT replace(JSON_ARRAYAGG (t207s.c207s_set_master_sync_data RETURNING CLOB),'null','""')
		INTO v_set_out_dtl
  		FROM t207s_set_master_sync t207s
  		WHERE t207s.c1900_company_id     	= p_comp_id
 		AND t207s.c901_set_group_type       = 1601-- 1601 - Set Report Group
 		AND t207s.c207s_void_fl  IS NULL;
  	
  	p_set_out_dtl    := v_set_out_dtl;
  
  	EXCEPTION
  	WHEN NO_DATA_FOUND THEN
    v_set_out_dtl := NULL;
  END;
		
 END gm_fch_all_set_tosync;
 
 
 /*****************************************************************
 	* Description : Procedure to fetch all set attribute data to sync
 	* Author      : Ramachandiran T.S
	*******************************************************************/

PROCEDURE gm_fch_all_set_attribute_tosync(
	  p_comp_id	     	IN	     t207s_set_master_sync.c1900_company_id%TYPE, 
	  p_system_type	 	IN       t207s_set_attribute_sync.c901_set_group_type%TYPE, 
	  p_set_out_dtl  	OUT      CLOB

	)
	AS
	
	v_set_out_dtl CLOB;
			
BEGIN		--Get the all system attribute details based on company
  BEGIN  
  	SELECT replace(JSON_ARRAYAGG (t207sa.c207s_set_attribute_sync_data RETURNING CLOB),'null','""')
		INTO v_set_out_dtl
  		FROM t207s_set_attribute_sync t207sa, t207s_set_master_sync t207s
  		WHERE t207s.c207_set_id				= t207sa.c207_set_id
  		AND t207s.c1900_company_id     		=  p_comp_id
  		AND t207sa.c901_set_group_type    	=  1601 -- 1601 - Set Report Grp
  		AND t207s.c207s_void_fl  IS NULL
 		AND t207sa.c207s_void_fl IS NULL;
  	
  	p_set_out_dtl    := v_set_out_dtl;
  
  	EXCEPTION
  	WHEN NO_DATA_FOUND THEN
    v_set_out_dtl := NULL;
  END;
  		
 END gm_fch_all_set_attribute_tosync;
 
 
  /*****************************************************************
 	* Description : Procedure to fetch all set part data to sync
 	* Author      : Ramachandiran T.S
	*******************************************************************/

PROCEDURE gm_fch_all_set_part_tosync(
	  p_comp_id	     	IN	     t207s_set_master_sync.c1900_company_id%TYPE, 
	  p_system_type	 	IN       t207s_set_master_sync.c901_set_group_type%TYPE, 
	  p_set_out_dtl  	OUT      CLOB

	)
	AS
	
	v_set_out_dtl CLOB;
			
BEGIN		--Get the all set part details based on company
  BEGIN
  	SELECT replace(JSON_ARRAYAGG (t207sd.c207s_set_details_sync_data RETURNING CLOB),'null','""')
		INTO v_set_out_dtl
  		FROM t207s_set_master_sync t207s, t207s_set_details_sync t207sd
  		WHERE t207s.c207_set_id 			= t207sd.c207_set_id
    	AND t207s.c901_set_group_type      	= 1601 -- 1601 - Set Report Grp
    	AND t207s.c1900_company_id     		= p_comp_id
    	AND t207s.c207s_void_fl  IS NULL
    	AND t207sd.c207s_void_fl IS NULL;
  	
  	p_set_out_dtl    := v_set_out_dtl;
  
  	EXCEPTION
  	WHEN NO_DATA_FOUND THEN
    v_set_out_dtl := NULL;
  END;
  		
 END gm_fch_all_set_part_tosync;
 
  	/*********************************************************************
 	* Description : Procedure to fetch the set update's sync data detail
 	* Author      : Ramachandiran T.S
	***********************************************************************/
 
 
 PROCEDURE gm_fch_set_update_tosync(
 	p_comp_id	     			IN	    t207s_set_master_sync.c1900_company_id%TYPE,
 	p_system_type	 			IN      t207s_set_master_sync.c901_set_group_type%TYPE,
 	p_device_last_sync_date 	IN   	t9151_device_sync_dtl.c9151_last_sync_date%TYPE,
 	p_set_out_dtl 				OUT     CLOB
 	)
	AS
	
	v_set_out_dtl CLOB;
BEGIN	
 BEGIN
		
		-- Get the latest update details from the device's last sync time.
  SELECT replace(JSON_ARRAYAGG (t207s.c207s_set_master_sync_data RETURNING CLOB),'null','""')
		INTO v_set_out_dtl
  		FROM t207s_set_master_sync t207s 
  		WHERE t207s.c901_set_group_type      		= p_system_type
    	AND t207s.c1900_company_id     				= p_comp_id
    	AND t207s.c207s_last_updated_date 	>= p_device_last_sync_date
    	AND t207s.c207s_void_fl  IS NULL;
  	
  	p_set_out_dtl            := v_set_out_dtl;
  
  	EXCEPTION
  	WHEN NO_DATA_FOUND THEN
    v_set_out_dtl := NULL;
    END;
    
    
END gm_fch_set_update_tosync;



	/*****************************************************************************
 	* Description : Procedure to fetch the set attribute update's sync data detail
 	* Author      : Ramachandiran T.S
	******************************************************************************/
 
 
 PROCEDURE gm_fch_set_attribute_update_to_sync(
 	p_comp_id	     			IN	    t207s_set_master_sync.c1900_company_id%TYPE,
 	p_system_type	 			IN      t207s_set_attribute_sync.c901_set_group_type%TYPE,
 	p_device_last_sync_date 	IN   	t9151_device_sync_dtl.c9151_last_sync_date%TYPE,
 	p_set_out_dtl 				OUT     CLOB
 	)
	AS
	v_set_out_dtl CLOB;
BEGIN	
 BEGIN
		
		-- Get the latest update details from the device's last sync time. 
	SELECT  replace(JSON_ARRAYAGG (t207sa.c207s_set_attribute_sync_data RETURNING CLOB),'null','""')
  		INTO v_set_out_dtl
		FROM t207s_set_attribute_sync t207sa, t207s_set_master_sync t207s
  		WHERE t207s.c207_set_id						= t207sa.c207_set_id
  		AND t207s.c1900_company_id     				=  p_comp_id
  		AND t207sa.c901_set_group_type    			=  p_system_type
  		AND t207sa.c207s_last_updated_date 			>= p_device_last_sync_date
  		AND t207s.c207s_void_fl  IS NULL
 		AND t207sa.c207s_void_fl IS NULL;
  	

  	p_set_out_dtl            := v_set_out_dtl;
  
  	EXCEPTION
  	WHEN NO_DATA_FOUND THEN
    v_set_out_dtl := NULL;
    END;
    
		
END gm_fch_set_attribute_update_to_sync;


/*********************************************************************
 	* Description : Procedure to fetch the set part update's sync data detail
 	* Author      : Ramachandiran T.S
	***********************************************************************/
 
 
 PROCEDURE gm_fch_set_part_update_tosync(
 	
 	p_comp_id	     			IN	    t207s_set_master_sync.c1900_company_id%TYPE,
 	p_system_type	 			IN      t207s_set_master_sync.c901_set_group_type%TYPE,
 	p_device_last_sync_date 	IN   	t9151_device_sync_dtl.c9151_last_sync_date%TYPE,
 	p_set_out_dtl 				OUT     CLOB
 	)
	AS
	
	v_set_out_dtl CLOB;
BEGIN	
 BEGIN
		
		-- Get the latest update details from the device's last sync time.
	SELECT replace(JSON_ARRAYAGG (t207sd.C207S_SET_DETAILS_SYNC_DATA RETURNING CLOB),'null','""')
  		INTO v_set_out_dtl
	 	FROM t207s_set_master_sync t207s, t207s_set_details_sync t207sd
  		WHERE t207s.c207_set_id 					= t207sd.c207_set_id
    	AND t207s.c901_set_group_type      			= p_system_type
    	AND t207s.c1900_company_id     				= p_comp_id
    	AND t207sd.c207s_last_updated_date 	>= p_device_last_sync_date
    	AND t207s.c207s_void_fl  IS NULL
    	AND t207sd.c207s_void_fl IS NULL;
  	
  		p_set_out_dtl            := v_set_out_dtl;
  
  	EXCEPTION
  	WHEN NO_DATA_FOUND THEN
    v_set_out_dtl := NULL;
    END;
    
		
END gm_fch_set_part_update_tosync;
END gm_pkg_device_set_sync_rpt;
/