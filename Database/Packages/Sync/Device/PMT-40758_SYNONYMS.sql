CREATE OR REPLACE PUBLIC SYNONYM gm_pkg_device_set_sync_to_temp FOR GLOBUS_APP.gm_pkg_device_set_sync_to_temp;
      
CREATE OR REPLACE PUBLIC SYNONYM gm_pkg_device_set_sync_to_master FOR GLOBUS_APP.gm_pkg_device_set_sync_to_master;