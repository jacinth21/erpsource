--@"C:\Database\Packages\Sync\Device\File\gm_pkg_device_file_sync_json_master.bdy";
CREATE OR REPLACE PACKAGE BODY gm_pkg_device_file_sync_json_master
IS


 /***************************************************************************************************************
    Description : This procedure is used to inserting file related(system, set, part, group) sync data into temp table
    Author      : vramanathan
    ****************************************************************************************************************/
PROCEDURE GM_SAV_FILE_SYNC_MAIN
AS
    V_COUNT NUMBER;
    v_ipad_synced_flag t903_upload_file_list.C903_IPAD_SYNCED_FLAG%TYPE;
    --
    v_start_time DATE;
BEGIN
	--
	v_start_time := CURRENT_DATE;
	-- to handle the exception
	BEGIN
    v_ipad_synced_flag := 'P';
    --
      
    --1 update the file processing ipad flag
    dbms_output.put_line ('Start time ==> ' || TO_CHAR (sysdate, 'MM/dd/yyyy HH:MI AM')) ;
    dbms_output.put_line ('******** before start GM_UPD_IPAD_SYNC_FLAG_PROCESSING>> '|| TO_CHAR (sysdate, 'MM/dd/yyyy HH:MI:SS AM')) ;
    gm_pkg_device_file_sync_json_master.GM_UPD_IPAD_SYNC_FLAG_PROCESSING;
    COMMIT;
    dbms_output.put_line ('******** after start GM_UPD_IPAD_SYNC_FLAG_PROCESSING >> '|| TO_CHAR (sysdate, 'MM/dd/yyyy HH:MI:SS AM')) ;
    
    --2 to store file data to JSON master table
    dbms_output.put_line ('******** before start gm_pkg_device_file_sync_json_master>> '|| TO_CHAR (sysdate, 'MM/dd/yyyy HH:MI:SS AM')) ;
     gm_pkg_device_file_sync_json_master.GM_SAV_FILE_MASTER_JSON_MAIN;
     COMMIT;
     
     dbms_output.put_line ('******** after start GM_SAV_FILE_MASTER_JSON_MAIN >> '|| TO_CHAR (sysdate, 'MM/dd/yyyy HH:MI:SS AM')) ;
    --
    EXCEPTION WHEN OTHERS
    THEN
      dbms_output.put_line (' Error GM_SAV_FILE_SYNC_MAIN @@@@@@@@@ ****** ' || SQLERRM||chr(10)||dbms_utility.format_error_backtrace);
      --
      -- 3 to notify the error 
      gm_pkg_cm_job.gm_cm_notify_load_info (v_start_time, CURRENT_DATE, 'E', SQLERRM||chr(10)||dbms_utility.format_error_backtrace, 'gm_pkg_device_file_sync_json_master.gm_sav_file_sync_main') ;
      --
    END;
END GM_SAV_FILE_SYNC_MAIN;


/******************************************************************
Description : This procedure is used to Update Ipad sync flag as 'P'
Author      : vramanathan
****************************************************************/
PROCEDURE GM_UPD_IPAD_SYNC_FLAG_PROCESSING
AS
BEGIN
	--
     UPDATE t903_upload_file_list
    SET C903_IPAD_SYNCED_FLAG      = 'P'
      WHERE C903_IPAD_SYNCED_FLAG IS NULL;
    --  
END GM_UPD_IPAD_SYNC_FLAG_PROCESSING;

/*************************************************************************************************
    Description : This procedure is used for forming JSON data from temporary table into master table
    Author      : vramanathan
    ************************************************************************************************/
PROCEDURE GM_SAV_FILE_MASTER_JSON_MAIN
AS
    v_ref_id t903_upload_file_list.C903_REF_ID%TYPE;
    v_cmp_id t903_upload_file_list.C1900_COMPANY_ID%TYPE;
    v_user_id t903_upload_file_list.C903_LAST_UPDATED_BY%TYPE;
    v_void_fl T903S_FILE_MASTER_SYNC.C903S_VOID_FL%TYPE;
    v_file_json CLOB;
    -- to form JSON data from Temp table
    CURSOR ipad_file_cur
    IS
         SELECT t903.C903_REF_ID ref_id, JSON_ARRAYAGG (JSON_OBJECT ('fid' VALUE TO_CHAR (t903.C903_UPLOAD_FILE_LIST), 'frid'
    VALUE t903.C903_REF_ID, 'fnm' VALUE t903.C903_FILE_NAME, 'vfl' VALUE TO_CHAR (t903.C903_DELETE_FL), 'fcb' VALUE
    t903.C903_CREATED_BY, 'fcd' VALUE TO_CHAR (t903.C903_CREATED_DATE), 'lub' VALUE TO_CHAR (
    t903.C903_LAST_UPDATED_DATE), 'lud' VALUE t903.C903_LAST_UPDATED_BY, 'frt' VALUE TO_CHAR ( t903.C901_REF_TYPE),
    'fst' VALUE TO_CHAR (t903.C901_TYPE), 'frg' VALUE TO_CHAR (t903.C901_REF_GRP), 'fsz' VALUE TO_CHAR (
    t903.C903_FILE_SIZE), 'fsq' VALUE TO_CHAR (t903.C903_FILE_SEQ_NO), 'fti' VALUE DECODE(t903.c901_ref_type,'103114', get_partnum_desc(t903.C205_PART_NUMBER_ID), t903.c901_file_title), 'fpid' VALUE
    t903.C205_PART_NUMBER_ID, 'fsf' VALUE TO_CHAR (t903a.shareable)) RETURNING CLOB) file_json_obj
   FROM t903_upload_file_list t903, (
         SELECT c903_upload_file_list fileid, MAX (DECODE (c901_attribute_type, 103217, c903a_attribute_value))
            shareable
           FROM t903a_file_attribute
          WHERE c903a_void_fl IS NULL
       GROUP BY c903_upload_file_list
    )
    t903a
  WHERE t903.c903_upload_file_list = t903a.fileid (+)
   -- AND C903_IPAD_SYNCED_FLAG = 'P'
    --AND t903.c903_ref_id = '300.161'
    AND t903.c901_ref_type   IN
    (
         SELECT c906_rule_value
           FROM t906_rules
          WHERE c906_rule_id     = 'FILE_TYPE'
            AND c906_rule_grp_id = 'PROD_CAT_SYNC'
            AND c906_void_fl    IS NULL
    ) --Image, Videos, DCOed Documents ,Non DCOed Documents
    AND t903.c901_ref_grp IS NOT NULL
    AND t903.C903_REF_ID IN (select C903_REF_ID from t903_upload_file_list where C903_IPAD_SYNCED_FLAG = 'P' GROUP BY C903_REF_ID)
	GROUP BY t903.C903_REF_ID; 
BEGIN
	--
    FOR file_json_dtls IN ipad_file_cur
    LOOP
    	--
        v_ref_id   := file_json_dtls.ref_id;
        v_file_json := file_json_dtls.file_json_obj;

        --to save data into master table
        --Manual Load 30301
        gm_pkg_device_file_sync_json_master.GM_SAV_FILE_MASTER (v_ref_id, v_file_json, '30301') ;
        COMMIT;
    END LOOP;
    --
    -- Update Ipad sync flag as 'Y' after inserted into master table
    gm_pkg_device_file_sync_json_master.GM_UPD_FILE_SYNC_FLAG_COMPLETED;
    COMMIT;

    --   
END GM_SAV_FILE_MASTER_JSON_MAIN;
/********************************************************************************
Description : This procedure is used to inserting JSON file data into master table
Author      : vramanathan
*********************************************************************************/
PROCEDURE GM_SAV_FILE_MASTER (
        p_ref_id    IN t903s_file_master_sync.c903_ref_id%TYPE,
        p_file_json IN t903s_file_master_sync.c903s_file_master_sync_data%TYPE,
        p_user_id   IN t903s_file_master_sync.c903s_last_updated_by%TYPE)
AS
BEGIN
    --103123 if updated
     UPDATE t903s_file_master_sync
    SET C903S_FILE_MASTER_SYNC_DATA = p_file_json, C901_SYNC_ACTION = 103123
      , C903S_LAST_UPDATED_BY = p_user_id, C903S_LAST_UPDATED_DATE = CURRENT_DATE
      WHERE C903_REF_ID       = p_ref_id
        AND C903S_VOID_FL    IS NULL;
    --    
    IF (SQL%ROWCOUNT          = 0) THEN
        --103122 if newly inserted
         INSERT
           INTO t903s_file_master_sync
            (
                C903_REF_ID, C903S_FILE_MASTER_SYNC_DATA
              , C901_SYNC_ACTION, C903S_VOID_FL, C903S_LAST_UPDATED_BY
              , C903S_LAST_UPDATED_DATE
            )
            VALUES
            (
                p_ref_id, p_file_json
              , 103122, NULL, p_user_id
              , CURRENT_DATE
            ) ;
    END IF;
    --
END GM_SAV_FILE_MASTER;

/****************************************************************
Description : This procedure is used to update ipad sync flag as 'Y'
Author      : vramanathan
******************************************************************/
PROCEDURE GM_UPD_FILE_SYNC_FLAG_COMPLETED
AS
BEGIN
	--
     UPDATE t903_upload_file_list
    SET C903_IPAD_SYNCED_FLAG = 'Y'
      WHERE C903_IPAD_SYNCED_FLAG = 'P';
    -- 
END GM_UPD_FILE_SYNC_FLAG_COMPLETED;

END gm_pkg_device_file_sync_json_master;
/