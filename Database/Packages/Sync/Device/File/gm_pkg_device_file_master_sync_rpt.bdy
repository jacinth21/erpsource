--@"C:\Database\Packages\Sync\Device\gm_pkg_device_file_master_sync_rpt.bdy
CREATE OR REPLACE
PACKAGE BODY gm_pkg_device_file_master_sync_rpt
IS
  --
  /*******************************************************************************************************
  * Description : Procedure to fetch the File data while sync from Globus App
  * Author: N RAJA
  ********************************************************************************************************/
PROCEDURE gm_fch_uploaded_file_info(
    p_token   IN  t101a_user_token.c101a_token_id%TYPE,
    p_cmpid   IN  t903_upload_file_list.c1900_company_id%TYPE,
    p_langid  IN  t9151_device_sync_dtl.c901_language_id%TYPE,
    p_uuid    IN  t9150_device_info.c9150_device_uuid%TYPE,
    p_out_str OUT CLOB)
    AS
    v_deviceid                  t9150_device_info.c9150_device_info_id%TYPE;
    v_device_last_sync_date 	t9151_device_sync_dtl.c9151_last_sync_date%TYPE;
    v_sync_count  				NUMBER;
    v_file_dtl    				CLOB;
    v_file_header 				CLOB;
    keys_string   				CLOB;
    v_json_out    				JSON_OBJECT_T;
    BEGIN
       
	     --get the device info id based on user token
           v_deviceid := get_device_id(p_token, p_uuid);
           
	         --"4000410" - "File" - File Sync Type 
            gm_pkg_device_sync_update_dtls.gm_fch_device_updates(v_deviceid,p_langid,4000410,v_device_last_sync_date,v_sync_count); 
	    
	        IF v_sync_count = 0 THEN
        		gm_pkg_device_file_sync_rpt.gm_fch_all_upladed_file_info(v_file_dtl);
            ELSE
              gm_pkg_device_file_sync_rpt.gm_fch_file_updates_info(v_device_last_sync_date,v_file_dtl);
            END IF;
            
	     --Get the HeaderDetails
		  BEGIN
		       SELECT replace(JSON_OBJECT( 'pagesize'   VALUE TO_CHAR(COUNT(1)),
							       'totalsize'  VALUE TO_CHAR(COUNT(1)),
							       'pageno'     VALUE TO_CHAR(1),
							       'totalpages' VALUE TO_CHAR(1),
							       'token'      VALUE TO_CHAR(p_token),
							       'langid'     VALUE TO_CHAR(p_langid),
							       'reftype'    VALUE TO_CHAR(4000410), --File Ref Type
								   'svrcnt'     VALUE '',
			  					   'lastrec'    VALUE '' ),'null','""')
			   INTO v_file_header
		       FROM t903s_file_master_sync t903s
		      WHERE t903s.c903s_void_fl IS NULL;
		  EXCEPTION
		       WHEN NO_DATA_FOUND THEN
		          v_file_header := NULL;
		 END;
		  
		  --Get the tag string and form the JSON object as an array
	
	     keys_string := v_file_dtl;
		
	     keys_string   := REPLACE(keys_string , '[[' , '[');
         keys_string   := REPLACE(keys_string , '],[' , ',');
         keys_string   := REPLACE(keys_string , ']]' , ']');
  
		 v_json_out    := JSON_OBJECT_T.parse(v_file_header);
		  
		  -- Check the JSON Array is not null and put the JSON Array value in JSON_OBJECT_T
		  IF keys_string IS NOT NULL THEN
		    v_json_out.put('listGmFileVO',JSON_ARRAY_T(keys_string));
		  ELSE
		  	v_json_out.put('listGmFileVO',JSON_ARRAY_T());
		  END IF;
		  
		  --Change the JSON OBJECT to Clob data type
		  p_out_str := v_json_out.to_clob;
		  
   END gm_fch_uploaded_file_info;
--
END gm_pkg_device_file_master_sync_rpt;
/