--@"C:\Database\Packages\Sync\Device\gm_pkg_device_file_sync_rpt.bdy";
CREATE OR REPLACE
PACKAGE BODY gm_pkg_device_file_sync_rpt
IS
     
  /******************************************************************
  Description : This procedure is used to fetch the all uploaded file data to sync
  Author      : N RAJA
  ****************************************************************/
PROCEDURE gm_fch_all_upladed_file_info(
    p_out_file_dtl OUT CLOB)
    AS
    v_file_dtl  CLOB;
	BEGIN  
		--Get the all File details 
		  SELECT replace(JSON_ARRAYAGG (t903s.c903s_file_master_sync_data RETURNING CLOB),'null','""')
			INTO v_file_dtl
		    FROM t903s_file_master_sync t903s
		   	WHERE t903s.c903s_void_fl IS NULL
		  	AND JSON_EXISTS (t903s.c903s_file_master_sync_data, '$.vfl?(@ != "Y")');
	
		     p_out_file_dtl  := v_file_dtl;
		     
       EXCEPTION
            WHEN NO_DATA_FOUND THEN
                 v_file_dtl := NULL;
		
  END gm_fch_all_upladed_file_info;
  
  /******************************************************************
  Description : This procedure is used to fetch the file update's sync data detail
  Author      : N RAJA
  ****************************************************************/
PROCEDURE gm_fch_file_updates_info(
      p_device_last_sync_date  IN  t9151_device_sync_dtl.c9151_last_sync_date%TYPE,
      p_out_file_dtl           OUT CLOB)
    AS
   	 v_file_dtl        CLOB;
	BEGIN  
		 -- Get the latest update details from the device's last sync time.
	    SELECT replace(JSON_ARRAYAGG (t903s.c903s_file_master_sync_data RETURNING CLOB),'null','""')
	      INTO v_file_dtl
	      FROM t903s_file_master_sync t903s
	     WHERE t903s.c903s_last_updated_date >= p_device_last_sync_date
	       AND t903s.c903s_void_fl IS NULL;
		
	       p_out_file_dtl  := v_file_dtl;
	       
	   EXCEPTION
            WHEN NO_DATA_FOUND THEN
                 v_file_dtl := NULL;
	       
  END gm_fch_file_updates_info;
  
END gm_pkg_device_file_sync_rpt;
/