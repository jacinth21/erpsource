/* Formatted on 2019/11/19 15:32 (Formatter Plus v4.8.0) */
-- @"C:\database\Packages\Sync\Group\gm_pkg_device_group_master_sync_rpt.bdy"

CREATE OR REPLACE PACKAGE BODY gm_pkg_device_group_master_sync_rpt
IS
	/************************************************************
 	* Description : Procedure used to fetch the group sync detail
 	* Author      : Ramachandiran T.S
	**************************************************************/
PROCEDURE gm_fch_groups_prodcat  (
		p_token  		IN 		t101a_user_token.c101a_token_id%TYPE,
		p_langid		IN		NUMBER,
		p_page_no		IN		NUMBER,
	    p_uuid		    IN      t9150_device_info.c9150_device_uuid%TYPE,
	    p_comp_id	    IN	    t1900_company.c1900_company_id%TYPE,
		p_out_group		OUT		CLOB
	)
	AS
    v_device_infoid    			t9150_device_info.c9150_device_info_id%TYPE;
    v_sync_count       			NUMBER;
    v_grp_header       			CLOB;
    v_grp_out_dtl       		CLOB;
    v_json_out         			JSON_OBJECT_T;
    v_device_last_sync_date 	t9151_device_sync_dtl.c9151_last_sync_date%TYPE;
    keys_string        			CLOB;
	
	BEGIN
		--get the device info id based on user token
		v_device_infoid := get_device_id(p_token, p_uuid);
			--get the device update details based on sync type
		gm_pkg_device_sync_update_dtls.gm_fch_device_updates(v_device_infoid,p_langid,103110,v_device_last_sync_date,v_sync_count);  --103110 - Group Device Sync Type 
		IF v_sync_count = 0 THEN
      		gm_pkg_device_group_sync_rpt.gm_fch_all_group_tosync(p_comp_id,v_grp_out_dtl);
     	ELSE
      		gm_pkg_device_group_sync_rpt.gm_fch_group_update_tosync(p_comp_id,v_device_last_sync_date,v_grp_out_dtl);
    	END IF;  	
    --Get the HeaderDetails
  BEGIN
  SELECT replace(JSON_OBJECT( 'pagesize'    VALUE TO_CHAR(COUNT(1)),
					  'totalsize'   VALUE TO_CHAR(COUNT(1)),
					  'pageno'      VALUE TO_CHAR(1),
					  'totalpages'  VALUE TO_CHAR(1),
					  'token'       VALUE TO_CHAR(p_token),
					  'langid'      VALUE TO_CHAR(p_langid),
					  'reftype'     VALUE TO_CHAR(103110), --Group Ref Type
					  'svrcnt'      VALUE '', 'lastrec' VALUE '' ),'null','""')
	INTO v_grp_header
  		FROM t4010s_group_master_sync t4010s
  		WHERE t4010s.c1900_company_id = p_comp_id;
  		EXCEPTION
  		WHEN NO_DATA_FOUND THEN
    	v_grp_header := NULL;
  END;
		
  --Get the Group string and form the JSON object as an array
  keys_string := v_grp_out_dtl;
  keys_string   := REPLACE(keys_string , '[[' , '[');
  keys_string   := REPLACE(keys_string , '],[' , ',');
  keys_string   := REPLACE(keys_string , ']]' , ']');
  
  v_json_out  := JSON_OBJECT_T.parse(v_grp_header);
  -- Check the JSON Array is not null and put the JSON Array value in JSON_OBJECT_T
  IF keys_string IS NOT NULL THEN
    v_json_out.put('listGmGroupVO',JSON_ARRAY_T(keys_string));
  ELSE  
  	v_json_out.put('listGmGroupVO',JSON_ARRAY_T());
  END IF;
  --Change the JSON OBJECT to Clob data type
  p_out_group := v_json_out.to_clob;
	
END gm_fch_groups_prodcat;	
	
	
	
	/****************************************************************
 	* Description : Procedure used to fetch the group part sync detail
 	* Author      : Ramachandiran T.S
	******************************************************************/
	
	PROCEDURE gm_fch_group_parts  (
		p_token  		IN 		t101a_user_token.c101a_token_id%TYPE,
		p_langid		IN		NUMBER,
		p_page_no		IN		NUMBER,
	    p_uuid		    IN      t9150_device_info.c9150_device_uuid%TYPE,
	    p_comp_id	    IN	    t1900_company.c1900_company_id%TYPE,
		p_out_group		OUT		CLOB
	)	
	AS
	v_device_infoid    			t9150_device_info.c9150_device_info_id%TYPE;
    v_sync_count       			NUMBER;
    v_grp_header       			CLOB;
    v_grp_out_dtl       		CLOB;
    v_json_out         			JSON_OBJECT_T;
    v_device_last_sync_date 	t9151_device_sync_dtl.c9151_last_sync_date%TYPE;
    keys_string        			CLOB;
	
	BEGIN
		--get the device info id based on user token
		v_device_infoid := get_device_id(p_token, p_uuid);
		--get the device update details based on sync type
		gm_pkg_device_sync_update_dtls.gm_fch_device_updates(v_device_infoid,p_langid,4000408,v_device_last_sync_date,v_sync_count);  --4000408 - Group Part Sync Type 
		IF v_sync_count = 0 THEN
      		gm_pkg_device_group_sync_rpt.gm_fch_all_group_part_tosync(p_comp_id,v_grp_out_dtl);
     	ELSE
      		gm_pkg_device_group_sync_rpt.gm_fch_group_part_update_tosync(p_comp_id,v_device_last_sync_date,v_grp_out_dtl);
    	END IF;
    --Get the HeaderDetails
  BEGIN
  SELECT replace(JSON_OBJECT( 'pagesize'    VALUE TO_CHAR(COUNT(1)),
					  'totalsize'   VALUE TO_CHAR(COUNT(1)),
					  'pageno'      VALUE TO_CHAR(1),
					  'totalpages'  VALUE TO_CHAR(1),
					  'token'       VALUE TO_CHAR(p_token),
					  'langid'      VALUE TO_CHAR(p_langid),
					  'reftype'     VALUE TO_CHAR(4000408), --Group part Ref Type
					  'svrcnt'      VALUE '', 'lastrec' VALUE '' ),'null','""')
	INTO v_grp_header
  		FROM t4011s_group_details_sync t4011s, t4010s_group_master_sync t4010s
  		WHERE t4010s.C4010_GROUP_ID = t4011s.C4010_GROUP_ID
  		AND t4010s.c1900_company_id = p_comp_id;
  		EXCEPTION
  		WHEN NO_DATA_FOUND THEN
    	v_grp_header := NULL;
  END;
		
  --Get the Group part string and form the JSON object as an array
 keys_string 	:= v_grp_out_dtl;
 keys_string   := REPLACE(keys_string , '[[' , '[');
 keys_string   := REPLACE(keys_string , '],[' , ',');
 keys_string   := REPLACE(keys_string , ']]' , ']');
 
 v_json_out  	:= JSON_OBJECT_T.parse(v_grp_header); 
  -- Check the JSON Array is not null and put the JSON Array value in JSON_OBJECT_T
  IF keys_string IS NOT NULL THEN
    v_json_out.put('listGmGroupPartVO',JSON_ARRAY_T(keys_string));
  ELSE
   v_json_out.put('listGmGroupPartVO',JSON_ARRAY_T());
  END IF;
  --Change the JSON OBJECT to Clob data type
  p_out_group := v_json_out.to_clob;
END gm_fch_group_parts;	
	
END gm_pkg_device_group_master_sync_rpt;
/