--@"C:\Database\Packages\Sync\Device\Group\gm_pkg_device_group_sync_temp.bdy";
CREATE OR REPLACE PACKAGE BODY gm_pkg_device_group_sync_temp
IS
    /*****************************************************************************************
    Description : This procedure is used to inserting group related language, Release, master
    sync data into temp table
    Author      : vramanathan
    ***************************************************************************************/
PROCEDURE GM_SAV_GROUP_MASTER_SYNC_MAIN
AS
    v_count NUMBER;
    v_ipad_sync_flag t4010_group.C4010_IPAD_SYNCED_FLAG%TYPE;
    v_ref_type t2000_master_metadata.C901_REF_TYPE%TYPE;
    --
    v_start_time DATE;
BEGIN
    v_ipad_sync_flag := 'P';
    v_ref_type       := 103094;
    --
    v_start_time := CURRENT_DATE;
    -- to handle the exception
	BEGIN
    -- 1 to delete the temp table
    DELETE FROM MY_TEMP_GROUP_SYNC_UPDATE;
    
    
    -- 2 to update the ipad processing flag
    dbms_output.put_line ('Start time ==> ' || TO_CHAR (sysdate, 'MM/dd/yyyy HH:MI AM')) ;
    dbms_output.put_line ('******** before start GM_UPD_IPAD_SYNC_FLAG_PROCESSING>> '|| TO_CHAR (sysdate, 'MM/dd/yyyy HH:MI:SS AM')) ;
    gm_pkg_device_group_sync_temp.GM_UPD_IPAD_SYNC_FLAG_PROCESSING (v_ipad_sync_flag) ;
    COMMIT;
    --
    dbms_output.put_line ('******** after start GM_UPD_IPAD_SYNC_FLAG_PROCESSING >> '|| TO_CHAR (sysdate, 'MM/dd/yyyy HH:MI:SS AM')) ;
    
    
    
    --3 to store group master data to temp table
    dbms_output.put_line ('******** before start GM_SAV_GROUP_MASTER_TMP>> '|| TO_CHAR (sysdate, 'MM/dd/yyyy HH:MI:SS AM')) ;
    gm_pkg_device_group_sync_temp.GM_SAV_GROUP_MASTER_TMP (v_ipad_sync_flag, v_ref_type) ;
    COMMIT;
    dbms_output.put_line ('******** after start GM_SAV_GROUP_MASTER_TMP >> '|| TO_CHAR (sysdate, 'MM/dd/yyyy HH:MI:SS AM')) ;
    
    
    --4 to store group release data to temp table
    dbms_output.put_line ('******** before start GM_SAV_GROUP_RELEASE_TMP>> '|| TO_CHAR (sysdate, 'MM/dd/yyyy HH:MI:SS AM')) ;
    gm_pkg_device_group_sync_temp.GM_SAV_GROUP_RELEASE_TMP (v_ipad_sync_flag, v_ref_type) ;
    COMMIT;
    dbms_output.put_line ('******** after start GM_SAV_GROUP_RELEASE_TMP >> '|| TO_CHAR (sysdate, 'MM/dd/yyyy HH:MI:SS AM')) ;
    
    
    --5 to store group master data to temp table
    dbms_output.put_line ('******** before start GM_SAV_GROUP_LANG_TMP>> '|| TO_CHAR (sysdate, 'MM/dd/yyyy HH:MI:SS AM')) ;
    gm_pkg_device_group_sync_temp.GM_SAV_GROUP_LANG_TMP (v_ipad_sync_flag, v_ref_type) ;
    COMMIT;
    dbms_output.put_line ('******** after start GM_SAV_GROUP_LANG_TMP >> '|| TO_CHAR (sysdate, 'MM/dd/yyyy HH:MI:SS AM')) ;
    
    
    --6 takes count from temp table
     SELECT COUNT (1)
       INTO v_count
       FROM MY_TEMP_GROUP_SYNC_UPDATE;
    IF v_count <> 0 THEN
        dbms_output.put_line ('******** before start GM_SAV_GROUP_MASTER_JSON_MAIN>> '|| TO_CHAR (sysdate, 'MM/dd/yyyy HH:MI:SS AM')) ;
        gm_pkg_device_group_sync_json_master.GM_SAV_GROUP_MASTER_JSON_MAIN;
        COMMIT;
        dbms_output.put_line ('******** after start GM_SAV_GROUP_MASTER_JSON_MAIN >> '|| TO_CHAR (sysdate, 'MM/dd/yyyy HH:MI:SS AM')) ;
    END IF;
    
    
    -- 7 to store group details to JSON master table
    dbms_output.put_line ('******** before start GM_SAV_GROUP_DETAILS_JSON_MAIN>> '|| TO_CHAR (sysdate, 'MM/dd/yyyy HH:MI:SS AM')) ;
    gm_pkg_device_group_sync_json_master.GM_SAV_GROUP_DETAILS_JSON_MAIN (v_ipad_sync_flag, v_ref_type) ;
    COMMIT;
    dbms_output.put_line ('******** after start GM_SAV_GROUP_DETAILS_JSON_MAIN >> '|| TO_CHAR (sysdate, 'MM/dd/yyyy HH:MI:SS AM')) ;
    --
    --
    EXCEPTION WHEN OTHERS
    THEN
      dbms_output.put_line (' Error GM_SAV_GROUP_MASTER_SYNC_MAIN @@@@@@@@@ ****** ' || SQLERRM||chr(10)||dbms_utility.format_error_backtrace);
      --
      -- 8 to notify the error 
      gm_pkg_cm_job.gm_cm_notify_load_info (v_start_time, CURRENT_DATE, 'E', SQLERRM||chr(10)||dbms_utility.format_error_backtrace, 'gm_pkg_device_group_sync_temp.gm_sav_group_master_sync_main') ;
      --
    END;
    --
END GM_SAV_GROUP_MASTER_SYNC_MAIN;

/*****************************************************************************************
Description : This procedure is used to inserting group related Release sync data into temp table
Author      : vramanathan
***************************************************************************************/
PROCEDURE GM_UPD_IPAD_SYNC_FLAG_PROCESSING (
        p_ipad_sync_flag IN t4010_group.C4010_IPAD_SYNCED_FLAG%TYPE)
AS
BEGIN
	-- to update group table
     UPDATE t4010_group
    SET C4010_IPAD_SYNCED_FLAG      = p_ipad_sync_flag
      WHERE C4010_IPAD_SYNCED_FLAG IS NULL;
      
     -- to update group meta 
     UPDATE t2000_master_metadata
    SET C2000_IPAD_SYNCED_FLAG      = p_ipad_sync_flag
      WHERE C2000_IPAD_SYNCED_FLAG IS NULL
      AND C901_REF_TYPE = 103094;
      
     -- to update group company mapping 
     UPDATE t4012_group_company_mapping
    SET C4012_IPAD_SYNCED_FLAG      = p_ipad_sync_flag
      WHERE C4012_IPAD_SYNCED_FLAG IS NULL;
     
      -- to update group details process flag
     UPDATE t4011_group_detail
    SET C4011_IPAD_SYNCED_FLAG      = p_ipad_sync_flag
      WHERE C4011_IPAD_SYNCED_FLAG IS NULL;
     
      --
      
END GM_UPD_IPAD_SYNC_FLAG_PROCESSING;

/******************************************************************************************
Description : This procedure is used to inserting group master related sync data into temp table
Author      : vramanathan
******************************************************************************************/
PROCEDURE GM_SAV_GROUP_MASTER_TMP (
        p_ipad_sync_flag IN t4010_group.C4010_IPAD_SYNCED_FLAG%TYPE,
        p_ref_type       IN t2000_master_metadata.C901_REF_TYPE%TYPE)
AS
BEGIN
    -- ref type 103094 group
     INSERT
       INTO MY_TEMP_GROUP_SYNC_UPDATE
        (
            C4010_GROUP_ID, C1900_COMPANY_ID, C901_LANGUAGE_ID
          , C4010_GROUP_NAME, C4010_GROUP_DESC, C4010_GROUP_DTLS_DESC
          , C4010_GROUP_PUBLISH_FLAG, C901_GROUP_TYPE, C901_GROUP_PRICED_TYPE
          , C207_SYSTEM_ID, C4010_VOID_FL
        )
     SELECT t4010.C4010_GROUP_ID grpid, t4012.C1900_COMPANY_ID cmpid, t2000.C901_LANGUAGE_ID lid
      , T2000.C2000_REF_NAME grpnm, T2000.C2000_REF_DESC grpdesc, T2000.C2000_REF_DTL_DESC grpdtldesc
      , t4010.C4010_PUBLISH_FL pfl, t4010.C901_GROUP_TYPE grptype, t4010.C901_PRICED_TYPE prctype
      , t4010.C207_set_ID sid, NVL(t4010.C4010_VOID_FL,t4012.C4012_VOID_FL) vfl
       FROM t4010_group t4010, t1900_company t1900, t2000_master_metadata t2000
      , t4012_group_company_mapping t4012, (
            SELECT t207.c207_set_id setid
               FROM t207_set_master t207
              WHERE t207.c901_set_grp_type = 1600 --sales report grp
                AND t207.c207_void_fl     IS NULL
        )
        SYSDATA
      WHERE t4010.C4010_GROUP_ID         = T4012.C4010_GROUP_ID
        AND t4010.c207_set_id            = SYSDATA.setid
        AND t4010.c901_type              = 40045 --Forecast/Demand Group
        AND t4012.C1900_COMPANY_ID       = T1900.C1900_COMPANY_ID
        AND t4010.C4010_GROUP_ID         = T2000.C2000_REF_ID
        AND t2000.C901_REF_TYPE          = p_ref_type
        AND t4010.C4010_IPAD_SYNCED_FLAG = p_ipad_sync_flag
        AND t2000.C901_LANGUAGE_ID       = gm_pkg_device_common_sync.get_ipad_lang_id (103094, t4010.C4010_GROUP_ID,
        T1900.C901_LANGUAGE_ID)
        AND t1900.C1900_VOID_FL IS NULL;
END GM_SAV_GROUP_MASTER_TMP;
/*****************************************************************************************
Description : This procedure is used to inserting group related language details into temp table
Author      : vramanathan
***************************************************************************************/
PROCEDURE GM_SAV_GROUP_LANG_TMP (
        p_ipad_sync_flag IN t4010_group.C4010_IPAD_SYNCED_FLAG%TYPE,
        p_ref_type       IN t2000_master_metadata.C901_REF_TYPE%TYPE)
AS
BEGIN
    -- ref type 103094 group
    --105361 released status
     INSERT
       INTO MY_TEMP_GROUP_SYNC_UPDATE
        (
            C4010_GROUP_ID, C1900_COMPANY_ID, C901_LANGUAGE_ID
          , C4010_GROUP_NAME, C4010_GROUP_DESC, C4010_GROUP_DTLS_DESC
          , C4010_GROUP_PUBLISH_FLAG, C901_GROUP_TYPE, C901_GROUP_PRICED_TYPE
          , C207_SYSTEM_ID, C4010_VOID_FL
        )
     SELECT t4010.C4010_GROUP_ID, t4012.C1900_COMPANY_ID, t2000.C901_LANGUAGE_ID
      , T2000.C2000_REF_NAME grpnm, T2000.C2000_REF_DESC grpdesc, T2000.C2000_REF_DTL_DESC grpdtldesc
      , t4010.C4010_PUBLISH_FL, t4010.C901_GROUP_TYPE, t4010.C901_PRICED_TYPE
      , t4010.C207_SET_ID, NVL(t4010.C4010_VOID_FL,t4012.C4012_VOID_FL)
       FROM t4010_group t4010, t2000_master_metadata t2000, t4012_group_company_mapping t4012
      , t1900_company t1900, (
            SELECT DISTINCT (t207.c207_set_id) setid
               FROM t207_set_master t207
              WHERE t207.c901_set_grp_type = 1600 --sales report grp
                AND t207.c207_void_fl     IS NULL
        )
        SYSDATA
      WHERE t4010.C4010_GROUP_ID   = t2000.c2000_ref_id
        AND t4010.c207_set_id      = SYSDATA.setid
        AND t4010.c901_type        = 40045 --Forecast/Demand Group
        AND t4010.C4010_GROUP_ID   = t4012.c4010_group_id
        AND t1900.C1900_COMPANY_ID = t4012.c1900_company_id
      --  AND t4012.C901_GROUP_COM_MAP_TYP = 105361
        AND t2000.C901_REF_TYPE          = p_ref_type
        AND t2000.C2000_IPAD_SYNCED_FLAG = p_ipad_sync_flag
        AND NOT EXISTS
        (
             SELECT C4010_GROUP_ID, C1900_COMPANY_ID
               FROM MY_TEMP_GROUP_SYNC_UPDATE tmp_upd
              WHERE tmp_upd.C4010_GROUP_ID = t4012.C4010_GROUP_ID
              AND tmp_upd.C1900_COMPANY_ID = t4012.C1900_COMPANY_ID
        )
        AND T2000.C901_LANGUAGE_ID       = gm_pkg_device_common_sync.get_ipad_lang_id (103094, t4010.c4010_group_id,
        T1900.C901_LANGUAGE_ID)
        AND t1900.C1900_VOID_FL IS NULL;
        --
END GM_SAV_GROUP_LANG_TMP;
/*****************************************************************************************
Description : This procedure is used to inserting group related Release sync data into temp table
Author      : vramanathan
***************************************************************************************/
PROCEDURE GM_SAV_GROUP_RELEASE_TMP (
        p_ipad_sync_flag IN t4010_group.C4010_IPAD_SYNCED_FLAG%TYPE,
        p_ref_type       IN t2000_master_metadata.C901_REF_TYPE%TYPE)
AS
BEGIN
    -- ref type 103094 group
    --105361 released status
     INSERT
       INTO MY_TEMP_GROUP_SYNC_UPDATE
        (
            C4010_GROUP_ID, C1900_COMPANY_ID, C901_LANGUAGE_ID
          , C4010_GROUP_NAME, C4010_GROUP_DESC, C4010_GROUP_DTLS_DESC
          , C4010_GROUP_PUBLISH_FLAG, C901_GROUP_TYPE, C901_GROUP_PRICED_TYPE
          , C207_SYSTEM_ID, C4010_VOID_FL
        )
     SELECT t4010.C4010_GROUP_ID grpid, t4012.C1900_COMPANY_ID cmpid, t2000.C901_LANGUAGE_ID lid
      , t4010.C4010_GROUP_NM grpnm, t4010.C4010_GROUP_DESC grpdesc, t4010.C4010_GROUP_DTL_DESC grpdtldesc
      , t4010.C4010_PUBLISH_FL pfl, t4010.C901_GROUP_TYPE grptype, t4010.C901_PRICED_TYPE prctype
      , t4010.C207_SET_ID sid, NVL(t4010.C4010_VOID_FL,t4012.C4012_VOID_FL) vfl
       FROM t4010_group t4010, t1900_company t1900, t2000_master_metadata t2000
      , t4012_group_company_mapping t4012, (
            SELECT DISTINCT (t207.c207_set_id) setid
               FROM t207_set_master t207
              WHERE t207.c901_set_grp_type = 1600 --sales report grp
                AND t207.c207_void_fl     IS NULL
        )
        SYSDATA
      WHERE t4010.C4010_GROUP_ID   = t2000.c2000_ref_id
        AND t4010.c207_set_id      = SYSDATA.setid
        AND t4010.c901_type        = 40045 --Forecast/Demand Group
        AND t4010.C4010_GROUP_ID   = t4012.c4010_group_id
        AND t1900.C1900_COMPANY_ID = t4012.c1900_company_id
        AND NOT EXISTS
        (
             SELECT C4010_GROUP_ID
               FROM MY_TEMP_GROUP_SYNC_UPDATE tmp_upd
              WHERE tmp_upd.C4010_GROUP_ID = t4010.C4010_GROUP_ID
        )
        --  AND t4012.C901_GROUP_COM_MAP_TYP = 105361
        AND t2000.C901_REF_TYPE          = p_ref_type
        AND t4012.C4012_IPAD_SYNCED_FLAG = p_ipad_sync_flag
        AND t1900.C1900_VOID_FL         IS NULL;
        --
END GM_SAV_GROUP_RELEASE_TMP;
END gm_pkg_device_group_sync_temp;
/