/* Formatted on 2019/11/19 15:32 (Formatter Plus v4.8.0) */
-- @"C:\database\Packages\Sync\Group\gm_pkg_device_group_sync_rpt.bdy"

CREATE OR REPLACE PACKAGE BODY gm_pkg_device_group_sync_rpt
IS
    /*****************************************************************
 	* Description : Procedure to fetch all group master data to sync
 	* Author      : Ramachandiran T.S
	*******************************************************************/

PROCEDURE gm_fch_all_group_tosync(
	
	   p_comp_id	    IN	    t4010s_group_master_sync.c1900_company_id%TYPE, 
	   p_out_grp_dtl    OUT     CLOB
	
	)
	AS
	
	v_grp_dtl CLOB;
			
BEGIN		--Get the all group details based on company
  BEGIN
  	SELECT replace(JSON_ARRAYAGG (t4010s.c4010s_group_master_sync_data RETURNING CLOB),'null','""')
		INTO v_grp_dtl
  	FROM t4010s_group_master_sync t4010s
  	WHERE t4010s.c1900_company_id         = p_comp_id
  	AND t4010s.C4010S_VOID_FL  IS NULL;
  	
  	EXCEPTION
  	WHEN NO_DATA_FOUND THEN
    v_grp_dtl := NULL;
  	END;
  
  p_out_grp_dtl    := v_grp_dtl;
  	
 END gm_fch_all_group_tosync;
 
 
 	/*****************************************************************
 	* Description : Procedure to fetch all group part data to sync
 	* Author      : Ramachandiran T.S
	*******************************************************************/

 PROCEDURE gm_fch_all_group_part_tosync(
	
	   p_comp_id	    IN	    t4010s_group_master_sync.c1900_company_id%TYPE, 
	   p_out_grp_dtl    OUT     CLOB
	
	)
	AS
	
	v_grp_dtl CLOB;
			
BEGIN		--Get the all group part details based on company
  BEGIN
  	SELECT replace(JSON_ARRAYAGG (T4011S.C4011S_GROUP_DETAILS_SYNC_DATA returning clob),'null','""')
		INTO v_grp_dtl
  	FROM T4010S_GROUP_MASTER_SYNC T4010S, T4011S_GROUP_DETAILS_SYNC T4011S
  	WHERE T4010S.C4010_GROUP_ID = T4011S.C4010_GROUP_ID
    AND T4010S.C1900_COMPANY_ID			= p_comp_id
  	AND T4011S.C4011S_VOID_FL  is null
    AND T4010S.C4010S_VOID_FL  is null;
  
  	EXCEPTION
  	WHEN NO_DATA_FOUND THEN
    v_grp_dtl := NULL;
  	END;
  	
  	p_out_grp_dtl    := v_grp_dtl; 	
  	
 END gm_fch_all_group_part_tosync;
 
 
  	/*********************************************************************
 	* Description : Procedure to fetch the group update's sync data detail
 	* Author      : Ramachandiran T.S
	***********************************************************************/
 
 
 PROCEDURE gm_fch_group_update_tosync(
 	p_comp_id	     			IN	    t4010s_group_master_sync.c1900_company_id%TYPE,
 	p_device_last_sync_date 	IN   	t9151_device_sync_dtl.c9151_last_sync_date%TYPE,
 	p_out_grp_dtl    			OUT    CLOB
 	)
	AS
	
	v_grp_dtl CLOB;
BEGIN	
 BEGIN
		-- Get the latest update details from the device's last sync time.
  SELECT replace(JSON_ARRAYAGG (t4010s.c4010s_group_master_sync_data RETURNING CLOB),'null','""')
  INTO v_grp_dtl
  FROM t4010s_group_master_sync t4010s
  WHERE t4010s.c4010s_last_updated_date         >= p_device_last_sync_date
  AND t4010s.c1900_company_id							= p_comp_id
  AND t4010s.c4010s_void_fl    IS NULL;

  	p_out_grp_dtl            := v_grp_dtl;
  
  	EXCEPTION
  	WHEN NO_DATA_FOUND THEN
    v_grp_dtl := NULL;
    END;
		
END gm_fch_group_update_tosync;
	
	/***************************************************************************
 	* Description : Procedure to fetch the group parts update's sync data detail
 	* Author      : Ramachandiran T.S
	*****************************************************************************/
	
	
PROCEDURE gm_fch_group_part_update_tosync(
 	p_comp_id	     			IN	    t4010s_group_master_sync.c1900_company_id%TYPE,
 	p_device_last_sync_date 	IN   	t9151_device_sync_dtl.c9151_last_sync_date%TYPE,
 	p_out_grp_dtl    			OUT    	CLOB
 	)
	AS
	
	v_grp_dtl CLOB;
	
BEGIN
 BEGIN		
		-- Get the latest update details from the device's last sync time.
  SELECT replace(JSON_ARRAYAGG (t4011s.c4011s_group_details_sync_data RETURNING CLOB),'null','""')
  INTO v_grp_dtl
  FROM t4011s_group_details_sync t4011s,t4010s_group_master_sync t4010s
  WHERE t4011s.c4011s_last_updated_date  >= p_device_last_sync_date
  AND t4011s.c4010_group_id						= t4010s.c4010_group_id
  AND T4010S.c1900_company_id		= p_comp_id				
  AND t4011s.c4011s_void_fl     IS NULL
  AND t4010s.C4010S_VOID_FL		IS NULL;

  	p_out_grp_dtl      := v_grp_dtl;
  
  	EXCEPTION
  	WHEN NO_DATA_FOUND THEN
    v_grp_dtl := NULL;
    END;
    
END gm_fch_group_part_update_tosync;
END gm_pkg_device_group_sync_rpt;
/

