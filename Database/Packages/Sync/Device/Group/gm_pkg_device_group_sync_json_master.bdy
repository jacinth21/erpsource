--@"C:\Database\Packages\Sync\Device\Group\gm_pkg_device_group_sync_json_master.bdy";
CREATE OR REPLACE PACKAGE BODY gm_pkg_device_group_sync_json_master
IS
    /*************************************************************************************************
    Description : This procedure is used for forming JSON data from temporary table into master table
    Author      : vramanathan
    ************************************************************************************************/
PROCEDURE GM_SAV_GROUP_MASTER_JSON_MAIN
AS

    -- to form JSON data from Temp table
    -- C4010_GROUP_DESC, C4010_GROUP_DTL_DESC currently not showing anywhere in iPad, So it is null
    CURSOR my_temp_cur
    IS
         SELECT C4010_GROUP_ID GROUPID, C1900_COMPANY_ID COMPID, C901_LANGUAGE_ID LANGID ,
              (JSON_OBJECT ('gid' VALUE TO_CHAR (C4010_GROUP_ID), 'gnm' VALUE C4010_GROUP_NAME, 'gdes' VALUE '', 'gdd' VALUE TO_CHAR (''), 
              'gpf' VALUE C4010_GROUP_PUBLISH_FLAG, 'gt' VALUE TO_CHAR (C901_GROUP_TYPE), 'gpt' VALUE TO_CHAR (C901_GROUP_PRICED_TYPE),
              'sid' VALUE C207_SYSTEM_ID, 'vfl' VALUE C4010_VOID_FL)) MASTER_SYNC_DATA
            FROM MY_TEMP_GROUP_SYNC_UPDATE;
BEGIN
    FOR group_data IN my_temp_cur
    LOOP
        --to save data into master table
        --Manual Load 30301
        gm_pkg_device_group_sync_json_master.GM_SAV_GROUP_MASTER (group_data.GROUPID, group_data.MASTER_SYNC_DATA, group_data.COMPID, '30301') ;
        COMMIT;
    END LOOP;
    
    
    -- Update Ipad sync flag as 'Y' after inserted into master table
    gm_pkg_device_group_sync_json_master.GM_UPD_GROUP_SYNC_FLAG_COMPLETED;
    COMMIT;
    
    --To Delete Temp table after inserted into master table
   	DELETE FROM MY_TEMP_GROUP_SYNC_UPDATE;
    --   
END GM_SAV_GROUP_MASTER_JSON_MAIN;

/********************************************************************************
Description : This procedure is used to inserting JSON group data into master table
Author      : vramanathan
*********************************************************************************/
PROCEDURE GM_SAV_GROUP_MASTER (
        p_group_id   IN t4010s_group_master_sync.C4010_GROUP_ID%TYPE,
        p_group_json IN t4010s_group_master_sync.C4010S_GROUP_MASTER_SYNC_DATA%TYPE,
        p_company_id IN t4010s_group_master_sync.C1900_COMPANY_ID%TYPE,
        p_user_id    IN t4010s_group_master_sync.C4010S_LAST_UPDATED_BY%TYPE)
AS
BEGIN
    -- 103123 if updated
     UPDATE t4010s_group_master_sync
    SET C4010S_GROUP_MASTER_SYNC_DATA = p_group_json, C901_SYNC_ACTION = 103123, C4010S_LAST_UPDATED_BY = p_user_id
      , C4010S_LAST_UPDATED_DATE      = CURRENT_DATE
      WHERE C4010_GROUP_ID            = p_group_id
        AND C1900_COMPANY_ID          = p_company_id
        AND C4010S_VOID_FL           IS NULL;
        
    -- 103122 if newly added
    
    IF (SQL%ROWCOUNT = 0) THEN
         INSERT
           INTO t4010s_group_master_sync
            (
                C4010_GROUP_ID, C1900_COMPANY_ID, C4010S_GROUP_MASTER_SYNC_DATA
              , C901_SYNC_ACTION, C4010S_VOID_FL, C4010S_LAST_UPDATED_BY
              , C4010S_LAST_UPDATED_DATE
            )
            VALUES
            (
                p_group_id, p_company_id, p_group_json
              , 103122, NULL, p_user_id
              , CURRENT_DATE
            ) ;
    END IF;
    --
END GM_SAV_GROUP_MASTER;

/******************************************************************
Description : This procedure is used to update ipad sync flag as 'Y'
Author      : vramanathan
******************************************************************/
PROCEDURE GM_UPD_GROUP_SYNC_FLAG_COMPLETED
AS
BEGIN
	--
     UPDATE t4010_group
    SET C4010_IPAD_SYNCED_FLAG = 'Y'
      WHERE C4010_IPAD_SYNCED_FLAG = 'P';
      
     -- 
     UPDATE t2000_master_metadata
    SET C2000_IPAD_SYNCED_FLAG = 'Y'
      WHERE C901_REF_TYPE      = 103094
        AND C2000_IPAD_SYNCED_FLAG = 'P';
     
     --   
     UPDATE t4012_group_company_mapping
    SET C4012_IPAD_SYNCED_FLAG = 'Y'
      WHERE C4012_IPAD_SYNCED_FLAG = 'P';
     --
      
END GM_UPD_GROUP_SYNC_FLAG_COMPLETED;

/************************************************************************************************************
Description : This procedure is used to group detail related data to Master table and to update ipad sync flag
Author      : vramanathan
************************************************************************************************************/
PROCEDURE GM_SAV_GROUP_DETAILS_JSON_MAIN (
        p_ipad_sync_flag IN t4010_group.C4010_IPAD_SYNCED_FLAG%TYPE,
        p_ref_type       IN t2000_master_metadata.C901_REF_TYPE%TYPE)
AS
    -- to get group details attribute data
    CURSOR group_details_data
    IS
		SELECT t4011.C4010_GROUP_ID grpid,
		  JSON_ARRAYAGG (JSON_OBJECT ('gid' VALUE TO_CHAR (t4011.C4010_GROUP_ID), 'pid' VALUE t4011.C205_PART_NUMBER_ID,
		  'gpt' VALUE TO_CHAR (t4011.C901_PART_PRICING_TYPE)) RETURNING CLOB) group_sync_data
		FROM t4011_group_detail t4011,
		  (SELECT C4010_GROUP_ID group_id
		  FROM T4010S_GROUP_MASTER_SYNC
		  WHERE C4010S_VOID_FL IS NULL
		  GROUP BY C4010_GROUP_ID
		  ) T4010S
		WHERE t4011.C4010_GROUP_ID = T4010S.group_id
		AND t4011.C4010_GROUP_ID  IN
		  (SELECT C4010_GROUP_ID
		  FROM t4011_group_detail
		  WHERE C4011_IPAD_SYNCED_FLAG = 'P'
		  GROUP BY C4010_GROUP_ID
		  )
		GROUP BY t4011.C4010_GROUP_ID;
   --
BEGIN
	--
    FOR group_data IN group_details_data
    LOOP
        -- '30301' Manual Load
        gm_pkg_device_group_sync_json_master.GM_SAV_GROUP_DETAILS (group_data.grpid, group_data.group_sync_data, '30301') ;
    END LOOP;
    
    --to update ipad flag
    gm_pkg_device_group_sync_json_master.GM_UPD_GROUP_DETAILS_SYNC_FLAG_COMPLETED;
    
END GM_SAV_GROUP_DETAILS_JSON_MAIN;

/*****************************************************************************************************
Description : This procedure is used to inserting group detail related master sync data into master table
Author      : vramanathan
*******************************************************************************************************/
PROCEDURE GM_SAV_GROUP_DETAILS (
        p_group_id          IN t4011s_group_details_sync.C4010_GROUP_ID%TYPE,
        p_group_detail_json IN t4011s_group_details_sync.C4011S_GROUP_DETAILS_SYNC_DATA%TYPE,
        p_user_id           IN t4011s_group_details_sync.C4011S_LAST_UPDATED_BY%TYPE)
AS
BEGIN
	--
     UPDATE t4011s_group_details_sync
    SET C4011S_GROUP_DETAILS_SYNC_DATA = p_group_detail_json, C4011S_LAST_UPDATED_BY = p_user_id,
        C4011S_LAST_UPDATED_DATE       = CURRENT_DATE
      WHERE C4010_GROUP_ID             = p_group_id
        AND C4011S_VOID_FL            IS NULL;
        
    --
        
    IF (SQL%ROWCOUNT                   = 0) THEN
         INSERT
           INTO t4011s_group_details_sync
            (
                C4010_GROUP_ID, C4011S_GROUP_DETAILS_SYNC_DATA, C4011S_LAST_UPDATED_BY, C4011S_LAST_UPDATED_DATE
            )
            VALUES
            (
                p_group_id, p_group_detail_json, p_user_id, CURRENT_DATE
            ) ;
    END IF;
    --
END GM_SAV_GROUP_DETAILS;

/***************************************************************************************
Description : This procedure is used to update ipad sync flag as 'Y' in group detail table
Author      : vramanathan
*****************************************************************************************/
PROCEDURE GM_UPD_GROUP_DETAILS_SYNC_FLAG_COMPLETED
AS
BEGIN
	--
     UPDATE t4011_group_detail
    SET C4011_IPAD_SYNCED_FLAG     = 'Y'
      WHERE C4011_IPAD_SYNCED_FLAG = 'P';
    --  
END GM_UPD_GROUP_DETAILS_SYNC_FLAG_COMPLETED;

END gm_pkg_device_group_sync_json_master;
/