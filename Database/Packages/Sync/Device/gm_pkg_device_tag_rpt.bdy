/* Formatted on 2011/10/26 17:13 (Formatter Plus v4.8.0) */
--@"C:\Database\Packages\Sync\Device\gm_pkg_device_job_run_date.pkg";
CREATE OR REPLACE
PACKAGE BODY gm_pkg_device_tag_rpt
IS
  /*************************************************************************************************
  * Description :  This procedure will set the input p_sync_type id's from the device last sync time.
  * Example     :  This procedure will check is there any modified tags avaiable after last sync for
  *                tags for given device. If updated parts available, then it will add all updated
  *                tags and return last synced page's last record key.
  * Author      :  Karthik
  **************************************************************************************************/
PROCEDURE gm_fch_tag_device_updates(
    p_device_infoid 			IN 		t9151_device_sync_dtl.c9150_device_info_id%TYPE ,
    p_language_id   			IN 		t9151_device_sync_dtl.c901_language_id%TYPE ,
    p_sync_type     			IN 		t9151_device_sync_dtl.c901_sync_type%TYPE ,
    p_device_last_sync_date 	OUT   	t9151_device_sync_dtl.c9151_last_sync_date%TYPE,
   	p_sync_count     			OUT   	NUMBER)
AS
  v_lang_cnt        NUMBER := 0;
  v_device_last_sync_date 	t9151_device_sync_dtl.c9151_last_sync_date%TYPE; 
BEGIN
  
  SELECT COUNT (1)
	INTO v_lang_cnt
  FROM T901_Code_Lookup
  WHERE C901_CODE_ID = p_language_id
  AND C901_Code_Grp  = 'MLANG'
  AND C901_ACTIVE_FL = 1
  AND C901_VOID_FL  IS NULL;
  
  IF v_lang_cnt      = 0 THEN
    raise_application_error (-20606, ''); --No Language Defined
  END IF;
  
  BEGIN 
   SELECT c9151_last_sync_date into v_device_last_sync_date 
   FROM t9151_device_sync_dtl
   WHERE c9150_device_info_id = p_device_infoid
   AND c901_sync_type         = p_sync_type
   AND c901_language_id       = p_language_id
   AND (c901_sync_status      = 103121 --success
   OR ( c901_sync_status      = 103120 --Initiated -- date also return
   AND c9151_last_sync_date  IS NOT NULL))
   AND c9151_void_fl	IS NULL;
   
   p_device_last_sync_date := v_device_last_sync_date;
   
 EXCEPTION
  	WHEN NO_DATA_FOUND THEN
    v_device_last_sync_date := NULL;
  END;
  
   IF v_device_last_sync_date IS NOT NULL
   THEN
   p_sync_count  := 1;
   ELSE
   p_sync_count  := 0;
   END IF;
  
END gm_fch_tag_device_updates;
/********************************************************************
* Description : Procedure to fch all tag tosync
* Author: Karthik
**********************************************************************/
PROCEDURE gm_fch_all_tag_details_tosync(
    p_out_tag_dtl OUT CLOB )
AS
  v_company_id t1900_company.c1900_company_id%TYPE;
  v_tag_dtl CLOB;
BEGIN

  SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL'))
	INTO v_company_id
  FROM dual;
  
  --Get the all tag details based on company
  BEGIN
  SELECT JSON_ARRAYAGG (T5010.C5010d_Device_Tag_Sync_Data RETURNING CLOB)
	INTO v_tag_dtl
  FROM T5010D_DEVICE_TAG_SYNC_MASTER T5010
  WHERE T5010.c1900_company_id              = v_company_id
  AND T5010.C5010D_DEVICE_TAG_SYNC_VOID_FL IS NULL;
  
  p_out_tag_dtl := v_tag_dtl;
  
   EXCEPTION
  WHEN NO_DATA_FOUND THEN
    v_tag_dtl := NULL;
  END;
 
END gm_fch_all_tag_details_tosync;

/********************************************************************
* Description : Procedure to fetch the Tag update's sync data detail
* Author: Ramachandiran T.S
**********************************************************************/
PROCEDURE gm_fch_tag_dtl_update_tosync(
	p_device_last_sync_date 	IN   	t9151_device_sync_dtl.c9151_last_sync_date%TYPE,
    p_out_tag_dtl 				OUT 	CLOB )
 AS
  v_company_id t1900_company.c1900_company_id%TYPE;
  v_tag_dtl CLOB;
  v_cnt NUMBER;
    
  BEGIN

  SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL'))
	INTO v_company_id
  FROM dual;
  
  --Get the updated tag details based on company and last sync date
  BEGIN
	SELECT JSON_ARRAYAGG (T5010.C5010d_Device_Tag_Sync_Data RETURNING CLOB)
		INTO v_tag_dtl
  	FROM T5010D_DEVICE_TAG_SYNC_MASTER T5010
  	WHERE T5010.c1900_company_id          = v_company_id
  	AND T5010.c5010d_last_updated_date 	>= p_device_last_sync_date
  	AND T5010.C5010D_DEVICE_TAG_SYNC_VOID_FL IS NULL;
	
	 p_out_tag_dtl := v_tag_dtl;
	 
   EXCEPTION
  WHEN NO_DATA_FOUND THEN
    p_out_tag_dtl := NULL;
  END;

 END gm_fch_tag_dtl_update_tosync;   
END gm_pkg_device_tag_rpt;
/
