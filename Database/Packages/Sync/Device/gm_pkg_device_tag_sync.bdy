/* Formatted on 2011/10/26 17:13 (Formatter Plus v4.8.0) */
--@"C:\Database\Packages\Sync\Device\gm_pkg_device_tag_sync.bdy";
CREATE OR REPLACE
PACKAGE BODY gm_pkg_device_tag_sync
IS
  --
  /*******************************************************************************************************
  * Description : Procedure to fetch the Tag data in sync fgrom Globus App
  * Author: Karthik
  ********************************************************************************************************/
PROCEDURE gm_fch_tag_details(
    p_token   IN T101A_USER_TOKEN.C101a_Token_Id%TYPE,
    p_langid  IN NUMBER,
    p_page_no IN NUMBER,
    p_uuid    IN t9150_device_info.c9150_device_uuid%TYPE,
    p_out_str OUT CLOB )
AS
  v_deviceid t9150_device_info.c9150_device_info_id%TYPE;
  v_count     NUMBER;
  v_page_no   NUMBER;
  v_page_size NUMBER;
  v_start     NUMBER;
  v_end       NUMBER;
  v_last_rec t9151_device_sync_dtl.C9151_LAST_SYNC_REC%TYPE;
  v_tag_header CLOB;
  v_tag_dtl CLOB;
  v_json_out JSON_OBJECT_T;
  keys_string CLOB;
  v_company_id t1900_company.c1900_company_id%TYPE;
  v_sync_count       			NUMBER;
  v_device_last_sync_date 	t9151_device_sync_dtl.c9151_last_sync_date%TYPE;
BEGIN

  SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL'))
  INTO v_company_id
  FROM dual;
  
  --get the device info id based on user token
  v_deviceid := get_device_id(p_token, p_uuid);
  
  /*below prc check the device's last updated date for codelookup reftype and
  if record exist then it checks is there any codelookup groups updated after the device updated date.
  if updated groups available then add all code groups into v_inlist and return count of v_inlist records*/
  
  gm_pkg_device_tag_rpt.gm_fch_tag_device_updates(v_deviceid,p_langid,4001234,v_device_last_sync_date,v_sync_count); -- 4001234 - Exxample dummy Tag sync reftype
  
  --If sync count is zero call procedure to sync all tag details otherwise update tag details
  IF v_sync_count = 0 THEN
    gm_pkg_device_tag_rpt.gm_fch_all_tag_details_tosync(v_tag_dtl);
  ELSE
    gm_pkg_device_tag_rpt.gm_fch_tag_dtl_update_tosync(v_device_last_sync_date,v_tag_dtl); 
  END IF;
  
  --Get the HeaderDetails
  BEGIN
  SELECT JSON_OBJECT( 'pagesize' VALUE COUNT(1),
					  'totalsize' VALUE COUNT(1),
					  'pageno' VALUE 1,
					  'totalpages' VALUE 1,
					  'token' VALUE p_token,
					  'langid' VALUE p_langid,
					  'reftype' VALUE 4001234, --Tag Ref Type
						'svrcnt' VALUE '', 'lastrec' VALUE '' )
	INTO v_tag_header
  FROM T5010D_DEVICE_TAG_SYNC_MASTER T5010
  WHERE T5010.c1900_company_id = v_company_id;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    v_tag_header := NULL;
  END;
  
  --Get the tag string and form the JSON object as an array
  keys_string := v_tag_dtl;
  keys_string := REPLACE(keys_string , '}{' , '},{');
  --keys_string := concat('[',keys_string); --Commenting the code as the Bracket is appending in Production
  
  v_json_out  := JSON_OBJECT_T.parse(v_tag_header);
  
  -- Check the JSON Array is not null and put the JSON Array value in JSON_OBJECT_T
  
  IF keys_string IS NOT NULL THEN
    v_json_out.put('listGmTagInfoVO',JSON_ARRAY_T(keys_string));
  ELSE
    v_json_out.put('listGmTagInfoVO',JSON_ARRAY_T());
  END IF;
  
  --Change the JSON OBJECT to Clob data type
  p_out_str := v_json_out.to_clob;
  
END gm_fch_tag_details;
END gm_pkg_device_tag_sync;
/
