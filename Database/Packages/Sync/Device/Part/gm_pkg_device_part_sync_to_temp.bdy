-- @"C:\Database\Packages\Sync\Device\System\gm_pkg_device_part_sync_to_temp.bdy";
CREATE OR REPLACE PACKAGE BODY GM_PKG_DEVICE_PART_SYNC_TO_TEMP
IS
    /**********************************************************************************************************
    Description : This procedure is used to inserting Part related sync data into temp table
    Author      : jgurunathan
    ***********************************************************************************************************/
PROCEDURE GM_SAV_PART_MASTER_SYNC_MAIN
AS
    v_temp_count       NUMBER;
    v_master_tmp_count NUMBER;
    --
    v_start_time DATE;
BEGIN
	v_start_time := CURRENT_DATE;
	--
	BEGIN
    dbms_output.put_line ('Start time ==> ' || TO_CHAR (sysdate, 'MM/dd/yyyy HH:MI AM')) ;
    --
     DELETE FROM MY_TEMP_KEY_VALUE;
     DELETE FROM MY_TEMP_PART_SYNC_UPDATE;
    --
    dbms_output.put_line ('end delete the temp table ==> ' || TO_CHAR (sysdate, 'MM/dd/yyyy HH:MI AM')) ;
    
    --**** 1
    dbms_output.put_line ('Start time ==> ' || TO_CHAR (sysdate, 'MM/dd/yyyy HH:MI AM')) ;
    dbms_output.put_line (' before start GM_UPD_IPAD_SYNC_FLAG_PROCESSING ==> ' || TO_CHAR (sysdate, 'MM/dd/yyyy HH:MI AM')) ;
    
    --part number ref type 103090
    gm_pkg_device_part_sync_to_temp.GM_UPD_IPAD_SYNC_FLAG_PROCESSING;
    COMMIT;
    dbms_output.put_line (' after start GM_UPD_IPAD_SYNC_FLAG_PROCESSING ==> ' || TO_CHAR (sysdate, 'MM/dd/yyyy HH:MI AM')) ;
    
    --**** 2
    dbms_output.put_line (' before start GM_SAV_PART_MASTER_TEMP ==> ' || TO_CHAR (sysdate, 'MM/dd/yyyy HH:MI AM')) ;
    gm_pkg_device_part_sync_to_temp.GM_SAV_PART_MASTER_TEMP (103090) ;
    COMMIT;
    
    --**** 3
    dbms_output.put_line (' after start GM_SAV_PART_RELEASE_TEMP ==> ' || TO_CHAR (sysdate, 'MM/dd/yyyy HH:MI AM')) ;
    gm_pkg_device_part_sync_to_temp.GM_SAV_PART_RELEASE_TEMP (103090) ;
    COMMIT;
    dbms_output.put_line (' after start GM_SAV_PART_RELEASE_TEMP ==> ' || TO_CHAR (sysdate, 'MM/dd/yyyy HH:MI AM')) ;
    
    --**** 4
    dbms_output.put_line (' before start GM_SAV_PART_LANGUAGE_TEMP ==> ' || TO_CHAR (sysdate, 'MM/dd/yyyy HH:MI AM')) ;
    gm_pkg_device_part_sync_to_temp.GM_SAV_PART_LANGUAGE_TEMP (103090) ;
    COMMIT;
    dbms_output.put_line (' after start GM_SAV_PART_LANGUAGE_TEMP ==> ' || TO_CHAR (sysdate, 'MM/dd/yyyy HH:MI AM')) ;
    
    
     SELECT COUNT (1) INTO v_temp_count FROM MY_TEMP_PART_SYNC_UPDATE;
    dbms_output.put_line (' v_temp_count ==> ' || v_temp_count) ;
    dbms_output.put_line (' before start GM_SAV_PART_MASTER_JSON_MAIN ==> ' || TO_CHAR (sysdate, 'MM/dd/yyyy HH:MI AM')) ;
    
    --**** 5
    --below code to save in master tables
    IF v_temp_count > 0 THEN
        GM_PKG_DEVICE_PART_SYNC_TO_MASTER.GM_SAV_PART_MASTER_JSON_MAIN;
        COMMIT;
        dbms_output.put_line (' after start GM_SAV_PART_MASTER_JSON_MAIN ==> ' || TO_CHAR (sysdate, 'MM/dd/yyyy HH:MI AM')) ;
    END IF;
    
    --**** 6
    dbms_output.put_line (' before start GM_SAV_PART_ATTRIBUTE_JSON_MAIN ==> ' || TO_CHAR (sysdate,
    'MM/dd/yyyy HH:MI AM')) ;
    --to save attributes
    GM_PKG_DEVICE_PART_SYNC_TO_MASTER.GM_SAV_PART_ATTRIBUTE_JSON_MAIN;
    COMMIT;
    dbms_output.put_line ('End time ==> ' || TO_CHAR (sysdate, 'MM/dd/yyyy HH:MI AM')) ;
    --
    EXCEPTION WHEN OTHERS
    THEN
      dbms_output.put_line (' Error GM_SAV_PART_MASTER_SYNC_MAIN @@@@@@@@@ ****** ' || SQLERRM||chr(10)||dbms_utility.format_error_backtrace);
      --
      --*** 7 to notify the error 
      gm_pkg_cm_job.gm_cm_notify_load_info (v_start_time, CURRENT_DATE, 'E', SQLERRM||chr(10)||dbms_utility.format_error_backtrace, 'gm_pkg_device_part_sync_to_temp.gm_sav_part_master_sync_main') ;
    END;
    --
END GM_SAV_PART_MASTER_SYNC_MAIN;

/******************************************************************
Description : This procedure is used to update ipad sync flag as 'P'
Author      : jgurunathan
*******************************************************************/
PROCEDURE GM_UPD_IPAD_SYNC_FLAG_PROCESSING
AS
BEGIN
    -- 1 to truncate my temp parts
    -- 2 insert into temp
     INSERT
       INTO MY_TEMP_KEY_VALUE
        (
            MY_TEMP_TXN_ID, MY_TEMP_TXN_KEY
        )
     SELECT t205.c205_part_number_id pnum, t207.C207_SET_ID
       FROM t207_set_master t207, t208_set_details t208, T205_PART_NUMBER t205
      WHERE t207.c901_set_grp_type     = '1600' --sales Report Grp
        AND t208.c207_set_id           = t207.c207_set_id
        AND t205.c205_part_number_id   = t208.c205_part_number_id
      --  AND T205.c205_active_fl        = 'Y'
        AND t205.c205_release_for_sale = 'Y'
        AND T205.c205_product_family  IN
        (
             SELECT C906_RULE_VALUE
               FROM T906_RULES
              WHERE C906_RULE_ID     = 'PART_PROD_FAMILY'
                AND C906_RULE_GRP_ID = 'PROD_CAT_SYNC'
                AND c906_void_fl    IS NULL
        )
        AND t207.c207_void_fl IS NULL
   GROUP BY t205.c205_part_number_id, t207.C207_SET_ID ;
   --
    dbms_output.put_line ('before update t2000 time ==> ' || TO_CHAR (sysdate, 'MM/dd/yyyy HH:MI AM')) ;
   -- 
     UPDATE t2000_master_metadata
    SET C2000_IPAD_SYNCED_FLAG      = 'P'
      WHERE C2000_IPAD_SYNCED_FLAG IS NULL
        AND C901_REF_TYPE           = 103090;
   --     
    dbms_output.put_line ('After update t2000 time ==> ' || TO_CHAR (sysdate, 'MM/dd/yyyy HH:MI AM')) ;
    
    dbms_output.put_line ('before update t2023 time ==> ' || TO_CHAR (sysdate, 'MM/dd/yyyy HH:MI AM')) ;
    
     UPDATE t2023_part_company_mapping
    SET C2023_IPAD_SYNCED_FLAG      = 'P'
      WHERE C2023_IPAD_SYNCED_FLAG IS NULL
        AND c205_part_number_id    IN
        (
             SELECT MY_TEMP_TXN_ID FROM MY_TEMP_KEY_VALUE
        ) ;
    --    
    dbms_output.put_line ('After update t2023 time ==> ' || TO_CHAR (sysdate, 'MM/dd/yyyy HH:MI AM')) ;
    dbms_output.put_line ('before update t205 time ==> ' || TO_CHAR (sysdate, 'MM/dd/yyyy HH:MI AM')) ;
    
    --
     UPDATE t205_part_number
    SET C205_IPAD_SYNCED_FLAG      = 'P'
      WHERE C205_IPAD_SYNCED_FLAG IS NULL
        AND c205_part_number_id   IN
        (
             SELECT MY_TEMP_TXN_ID FROM MY_TEMP_KEY_VALUE
        ) ;
    --    
    dbms_output.put_line ('After update t205 time ==> ' || TO_CHAR (sysdate, 'MM/dd/yyyy HH:MI AM')) ;
    dbms_output.put_line ('After update t205d time ==> ' || TO_CHAR (sysdate, 'MM/dd/yyyy HH:MI AM')) ;
    
    --
     UPDATE t205d_part_attribute
    SET C205D_IPAD_SYNCED_FLAG      = 'P'
      WHERE C205D_IPAD_SYNCED_FLAG IS NULL
        AND C901_ATTRIBUTE_TYPE  IN
            (
                 SELECT C906_RULE_VALUE
                   FROM T906_RULES
                  WHERE C906_RULE_ID     = 'PARTATTRTYPE'
                    AND C906_RULE_GRP_ID = 'PARTATTR'
                    AND C906_VOID_FL    IS NULL
            )
        AND c205_part_number_id    IN
        (
             SELECT MY_TEMP_TXN_ID FROM MY_TEMP_KEY_VALUE
        ) ;
    --    
    dbms_output.put_line ('After update t205d time ==> ' || TO_CHAR (sysdate, 'MM/dd/yyyy HH:MI AM')) ;
    --
END GM_UPD_IPAD_SYNC_FLAG_PROCESSING;
/**********************************************************************************************************
Description : This procedure is used to inserting part master table sync data into temporary table
Author      : jgurunathan
***********************************************************************************************************/
PROCEDURE GM_SAV_PART_MASTER_TEMP (
        p_ref_type IN T2000_MASTER_METADATA.C901_REF_TYPE%TYPE)
AS
BEGIN
    -- reftype 103090 part number
     INSERT
       INTO MY_TEMP_PART_SYNC_UPDATE
        (
            C205_PART_NUMBER_ID, C1900_COMPANY_ID, C901_LANGUAGE_ID
          , C205_PART_DESC, C205_PART_DTLS_DESC, C901_PART_STATUS
          , C901_PART_FAMILY, C205_ACTIVE_FL, C207_SYSTEM_ID
          , C205_PRODUCT_MATERIAL, C205_PRODUCT_CLASS, C2060_DI_NUMBER
          , C2060_UDI_PATTERN
        )
     SELECT T205.C205_PART_NUMBER_ID, T2023.C1900_COMPANY_ID, T2000.C901_LANGUAGE_ID
      , T2000.C2000_REF_DESC, T2000.C2000_REF_DTL_DESC, T205.C901_STATUS_ID
      , T205.C205_PRODUCT_FAMILY, T205.C205_ACTIVE_FL, tmp.MY_TEMP_TXN_KEY
      , T205.C205_PRODUCT_MATERIAL, T205.C205_PRODUCT_CLASS, T2060.C2060_DI_NUMBER
      , GM_PKG_DEVICE_COMMON_SYNC.GET_UDI_CONFIG_WITH_NAME (T205.C205_PART_NUMBER_ID)
       FROM T205_PART_NUMBER T205, T2023_PART_COMPANY_MAPPING T2023, T2000_MASTER_METADATA T2000
      , T1900_COMPANY T1900, T2060_DI_PART_MAPPING T2060, MY_TEMP_KEY_VALUE tmp
      WHERE T205.C205_PART_NUMBER_ID  = T2023.C205_PART_NUMBER_ID
        AND T205.C205_PART_NUMBER_ID  = tmp.MY_TEMP_TXN_ID
        AND T2023.C205_PART_NUMBER_ID = tmp.MY_TEMP_TXN_ID
        --AND T205.C205_PART_NUMBER_ID = SYSDATA.PNUM
        AND T205.C205_PART_NUMBER_ID = T2060.C205_PART_NUMBER_ID(+)
        AND T205.C205_PART_NUMBER_ID = T2000.C2000_REF_ID
        AND T1900.C1900_COMPANY_ID   = T2023.C1900_COMPANY_ID
        AND T2000.C901_LANGUAGE_ID   = GM_PKG_DEVICE_COMMON_SYNC.GET_IPAD_LANG_ID (p_ref_type, T205.C205_PART_NUMBER_ID
        , T1900.C901_LANGUAGE_ID)
        AND T2000.C901_REF_TYPE        = p_ref_type
        AND T205.C205_IPAD_SYNCED_FLAG = 'P'
        AND T1900.C1900_VOID_FL       IS NULL;
END GM_SAV_PART_MASTER_TEMP;
/**********************************************************************************************************
Description : This procedure is used to inserting part release table sync data into temporary table
Author      : jgurunathan
***********************************************************************************************************/
PROCEDURE GM_SAV_PART_RELEASE_TEMP (
        p_ref_type IN T2000_MASTER_METADATA.C901_REF_TYPE%TYPE)
AS
BEGIN
    -- reftype 103090 part number
     INSERT
       INTO MY_TEMP_PART_SYNC_UPDATE
        (
            C205_PART_NUMBER_ID, C1900_COMPANY_ID, C901_LANGUAGE_ID
          , C205_PART_DESC, C205_PART_DTLS_DESC, C901_PART_STATUS
          , C901_PART_FAMILY, C205_ACTIVE_FL, C207_SYSTEM_ID
          , C205_PRODUCT_MATERIAL, C205_PRODUCT_CLASS, C2060_DI_NUMBER
          , C2060_UDI_PATTERN
        )
     SELECT T205.C205_PART_NUMBER_ID, T2023.C1900_COMPANY_ID, T2000.C901_LANGUAGE_ID
      , T2000.C2000_REF_DESC, T2000.C2000_REF_DTL_DESC, T205.C901_STATUS_ID
      , T205.C205_PRODUCT_FAMILY, T205.C205_ACTIVE_FL, tmp.MY_TEMP_TXN_KEY
      , T205.C205_PRODUCT_MATERIAL, T205.C205_PRODUCT_CLASS, T2060.C2060_DI_NUMBER
      , GM_PKG_DEVICE_COMMON_SYNC.GET_UDI_CONFIG_WITH_NAME (T205.C205_PART_NUMBER_ID)
       FROM T205_PART_NUMBER T205, T2023_PART_COMPANY_MAPPING T2023, T2000_MASTER_METADATA T2000
      , T1900_COMPANY T1900, T2060_DI_PART_MAPPING T2060, MY_TEMP_KEY_VALUE tmp
      WHERE T205.C205_PART_NUMBER_ID  = T2023.C205_PART_NUMBER_ID
        AND T205.C205_PART_NUMBER_ID  = tmp.MY_TEMP_TXN_ID
        AND T2023.C205_PART_NUMBER_ID = tmp.MY_TEMP_TXN_ID
        AND T205.C205_PART_NUMBER_ID  = T2060.C205_PART_NUMBER_ID(+)
        AND T2000.C2000_REF_ID        = T205.C205_PART_NUMBER_ID
        AND T1900.C1900_COMPANY_ID    = T2023.C1900_COMPANY_ID
        AND NOT EXISTS
        (
             SELECT C205_PART_NUMBER_ID
               FROM MY_TEMP_PART_SYNC_UPDATE tmp_upd
              WHERE tmp_upd.C205_PART_NUMBER_ID = T2023.C205_PART_NUMBER_ID
        )
        --AND T205.C205_PART_NUMBER_ID = '110.040'
        AND T2000.C901_LANGUAGE_ID = GM_PKG_DEVICE_COMMON_SYNC.GET_IPAD_LANG_ID (p_ref_type, T205.C205_PART_NUMBER_ID,
        T1900.C901_LANGUAGE_ID)
        AND T2000.C901_REF_TYPE          = p_ref_type
        AND T2023.C2023_IPAD_SYNCED_FLAG = 'P'
        AND T1900.C1900_VOID_FL         IS NULL;
        --
END GM_SAV_PART_RELEASE_TEMP;
/**********************************************************************************************************
Description : This procedure is used to inserting part language table sync data into temporary table
Author      : jgurunathan
***********************************************************************************************************/
PROCEDURE GM_SAV_PART_LANGUAGE_TEMP (
        p_ref_type IN T2000_MASTER_METADATA.C901_REF_TYPE%TYPE)
AS
BEGIN
    -- reftype 103090 part number
     INSERT
       INTO MY_TEMP_PART_SYNC_UPDATE
        (
            C205_PART_NUMBER_ID, C1900_COMPANY_ID, C901_LANGUAGE_ID
          , C205_PART_DESC, C205_PART_DTLS_DESC, C901_PART_STATUS
          , C901_PART_FAMILY, C205_ACTIVE_FL, C207_SYSTEM_ID
          , C205_PRODUCT_MATERIAL, C205_PRODUCT_CLASS, C2060_DI_NUMBER
          , C2060_UDI_PATTERN
        )
     SELECT T205.C205_PART_NUMBER_ID, T2023.C1900_COMPANY_ID, T2000.C901_LANGUAGE_ID
      , T2000.C2000_REF_DESC, T2000.C2000_REF_DTL_DESC, T205.C901_STATUS_ID
      , T205.C205_PRODUCT_FAMILY, T205.C205_ACTIVE_FL, tmp.MY_TEMP_TXN_KEY
      , T205.C205_PRODUCT_MATERIAL, T205.C205_PRODUCT_CLASS, T2060.C2060_DI_NUMBER
      , GM_PKG_DEVICE_COMMON_SYNC.GET_UDI_CONFIG_WITH_NAME (T205.C205_PART_NUMBER_ID)
       FROM T205_PART_NUMBER T205, T2023_PART_COMPANY_MAPPING T2023, T2000_MASTER_METADATA T2000
      , T1900_COMPANY T1900, T2060_DI_PART_MAPPING T2060, MY_TEMP_KEY_VALUE tmp
      WHERE T205.C205_PART_NUMBER_ID  = T2023.C205_PART_NUMBER_ID
        AND T205.C205_PART_NUMBER_ID  = tmp.MY_TEMP_TXN_ID
        AND T2023.C205_PART_NUMBER_ID = tmp.MY_TEMP_TXN_ID
        AND T2000.C2000_REF_ID        = T205.C205_PART_NUMBER_ID
        AND T205.C205_PART_NUMBER_ID  = T2060.C205_PART_NUMBER_ID(+)
        AND T1900.C1900_COMPANY_ID    = T2023.C1900_COMPANY_ID
        AND NOT EXISTS
        (
             SELECT C205_PART_NUMBER_ID, C1900_COMPANY_ID
               FROM MY_TEMP_PART_SYNC_UPDATE tmp_upd
              WHERE tmp_upd.C205_PART_NUMBER_ID = T2023.C205_PART_NUMBER_ID
                AND tmp_upd.C1900_COMPANY_ID    = T2023.C1900_COMPANY_ID
        )
        AND T2000.C901_LANGUAGE_ID = GM_PKG_DEVICE_COMMON_SYNC.GET_IPAD_LANG_ID (p_ref_type, T205.C205_PART_NUMBER_ID,
        T1900.C901_LANGUAGE_ID)
        AND T2000.C901_REF_TYPE          = p_ref_type
     --   AND T2023.C901_PART_MAPPING_TYPE = 105361 --<RELEASED VALUE>
        AND T2000.C2000_IPAD_SYNCED_FLAG = 'P'
        AND T1900.C1900_VOID_FL         IS NULL;
        --
END GM_SAV_PART_LANGUAGE_TEMP;
END GM_PKG_DEVICE_PART_SYNC_TO_TEMP;
/