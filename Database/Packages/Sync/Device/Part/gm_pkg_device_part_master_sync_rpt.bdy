CREATE OR REPLACE PACKAGE BODY gm_pkg_device_part_master_sync_rpt
IS

/************************************************************
* Description : Procedure used to fetch the part sync detail
* Author      : T.S. Ramachandiran
**************************************************************/
PROCEDURE gm_fch_part_prodcat(
		p_token  		IN 		t101a_user_token.c101a_token_id%TYPE,
		p_langid		IN		NUMBER,
		p_page_no		IN		NUMBER,
	    p_uuid		    IN      t9150_device_info.c9150_device_uuid%TYPE,
	    p_comp_id	    IN	    t1900_company.c1900_company_id%TYPE,
		p_out_part		OUT		CLOB
)
AS

	v_device_infoid    			t9150_device_info.c9150_device_info_id%TYPE;
    v_sync_count       			NUMBER;
    v_part_header       		CLOB;
    v_part_out_dtl       		CLOB;
    v_json_out         			JSON_OBJECT_T;
    v_device_last_sync_date 	t9151_device_sync_dtl.c9151_last_sync_date%TYPE;
    keys_string        			CLOB;
    
   BEGIN   
	   	--get the device info id based on user token
		v_device_infoid := get_device_id(p_token, p_uuid);
		--get the device update details based on sync type
		gm_pkg_device_sync_update_dtls.gm_fch_device_updates(v_device_infoid,p_langid,4000411,v_device_last_sync_date,v_sync_count);  --4000411 - Part num reftype
		IF v_sync_count = 0 THEN
      		gm_pkg_device_part_sync_rpt.gm_fch_all_part_tosync(p_comp_id,v_part_out_dtl);
     	ELSE
      		gm_pkg_device_part_sync_rpt.gm_fch_part_update_tosync(p_comp_id,v_device_last_sync_date,v_part_out_dtl);
    	END IF;  

--Get the HeaderDetails
 BEGIN
  SELECT replace(JSON_OBJECT( 'pagesize'    VALUE TO_CHAR(COUNT(1)),
					  'totalsize'   VALUE TO_CHAR(COUNT(1)),
					  'pageno'      VALUE TO_CHAR(1),
					  'totalpages'  VALUE TO_CHAR(1),
					  'token'       VALUE TO_CHAR(p_token),
					  'langid'      VALUE TO_CHAR(p_langid),
					  'reftype'     VALUE TO_CHAR(4000411), --Part Ref Type
					  'svrcnt'      VALUE '', 'lastrec' VALUE '' ),'null','""')
	INTO v_part_header
	FROM t205s_part_master_sync t205s
  	WHERE t205s.c1900_company_id     	= p_comp_id
 	AND t205s.c205s_void_fl  IS NULL;

 	EXCEPTION
  WHEN NO_DATA_FOUND THEN
    v_part_header := NULL;
  END;
		
  --Get the Set string and form the JSON object as an array
	keys_string := v_part_out_dtl;
	keys_string   := REPLACE(keys_string , '[[' , '['); 
    keys_string   := REPLACE(keys_string , '],[' , ',');
    keys_string   := REPLACE(keys_string , ']]' , ']');
  	v_json_out  := JSON_OBJECT_T.parse(v_part_header);
  	
 -- Check the JSON Array is not null and put the JSON Array value in JSON_OBJECT_T 
  IF keys_string IS NOT NULL THEN
    v_json_out.put('listGmPartNumberVO',JSON_ARRAY_T(keys_string));
    ELSE
    v_json_out.put('listGmPartNumberVO',JSON_ARRAY_T());
  END IF; 
  --Change the JSON OBJECT to Clob data type
  p_out_part := v_json_out.to_clob;
  
 END gm_fch_part_prodcat;


/************************************************************
* Description : Procedure used to fetch the part sync detail
* Author      : vramanathan
**************************************************************/
PROCEDURE gm_fch_partattr_prodcat  (
		p_token  			IN 		t101a_user_token.c101a_token_id%TYPE,
		p_langid			IN		NUMBER,
		p_page_no			IN		NUMBER,
	    p_uuid		    	IN      t9150_device_info.c9150_device_uuid%TYPE,
	    p_comp_id	    	IN	    t1900_company.c1900_company_id%TYPE,
		p_out_part_attr		OUT		CLOB
)
AS
	v_device_infoid    				t9150_device_info.c9150_device_info_id%TYPE;
    v_sync_count       				NUMBER;
    v_part_attr_header       		CLOB;
    v_part_attr_out_dtl       		CLOB;
    v_json_out         				JSON_OBJECT_T;
    v_device_last_sync_date 		t9151_device_sync_dtl.c9151_last_sync_date%TYPE;
    keys_string        				CLOB;
BEGIN
	--get the device info id based on user token
	v_device_infoid := get_device_id(p_token, p_uuid) ;
	--get the device update details based on sync type
	gm_pkg_device_sync_update_dtls.gm_fch_device_updates(v_device_infoid,p_langid,4000827,v_device_last_sync_date,v_sync_count);  --4000827 - Part Attribute sync type
	
	IF v_sync_count = 0 THEN
		gm_pkg_device_part_sync_rpt.gm_fch_all_part_attribute_tosync(p_comp_id,v_part_attr_out_dtl);
	ELSE
		gm_pkg_device_part_sync_rpt.gm_fch_part_attribute_update_tosync(p_comp_id,v_device_last_sync_date,v_part_attr_out_dtl);
	END IF;
	
	BEGIN
	SELECT replace(JSON_OBJECT( 'pagesize'   VALUE TO_CHAR(COUNT(1)),
							       'totalsize'  VALUE TO_CHAR(COUNT(1)),
							       'pageno'     VALUE TO_CHAR(1),
							       'totalpages' VALUE TO_CHAR(1),
							       'token'      VALUE TO_CHAR(p_token),
							       'langid'     VALUE TO_CHAR(p_langid),
							       'reftype'    VALUE TO_CHAR(4000827), --Part Sync Type
								   'svrcnt'     VALUE '',
			  					   'lastrec'    VALUE '' ),'null','""')
		INTO v_part_attr_header
    FROM t205s_part_master_sync t205s, t205s_part_attribute_sync t205sa
    WHERE t205s.c205_part_number_id = t205sa.c205_part_number_id
    	AND t205s.c1900_company_id = p_comp_id
    	AND t205s.c205s_void_fl is null
    	AND t205sa.c205s_void_fl is null;
  	EXCEPTION
  	WHEN NO_DATA_FOUND THEN
	  v_part_attr_header := NULL;
  	END;
  	
  	--Get the tag string and form the JSON object as an array
		keys_string := v_part_attr_out_dtl;
		keys_string   := REPLACE(keys_string , '[[' , '['); 
    	keys_string   := REPLACE(keys_string , '],[' , ',');
    	keys_string   := REPLACE(keys_string , ']]' , ']');
    	 
  		v_json_out  := JSON_OBJECT_T.parse(v_part_attr_header);
		
		  -- Check the JSON Array is not null and put the JSON Array value in JSON_OBJECT_T
		  
		  IF keys_string IS NOT NULL THEN
		    v_json_out.put('listGmPartAttributeVO',JSON_ARRAY_T(keys_string));
		    ELSE
		    v_json_out.put('listGmPartAttributeVO',JSON_ARRAY_T());
		  END IF;
		  
		  --Change the JSON OBJECT to Clob data type
		  p_out_part_attr := v_json_out.to_clob;
		  
END gm_fch_partattr_prodcat;
		  
END gm_pkg_device_part_master_sync_rpt;
/