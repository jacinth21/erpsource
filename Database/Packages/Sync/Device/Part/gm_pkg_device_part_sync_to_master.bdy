-- @"C:\Database\Packages\Sync\Device\System\gm_pkg_device_part_sync_to_master.bdy";
CREATE OR REPLACE PACKAGE BODY GM_PKG_DEVICE_PART_SYNC_TO_MASTER
IS
    /******************************************************************
    Description : This procedure is used for forming JSON data from temporary table into Part Master Sync Table
    Author      : ppandiyan
    ****************************************************************/
PROCEDURE GM_SAV_PART_MASTER_JSON_MAIN
AS
    CURSOR part_master_sync_data
    IS
        --forming JSON data from temp table
         SELECT C205_PART_NUMBER_ID PART_NUM_ID, C1900_COMPANY_ID COMPID, C901_LANGUAGE_ID LANGID
          , JSON_ARRAYAGG (JSON_OBJECT ('pid' VALUE TO_CHAR (C205_PART_NUMBER_ID), 'pdesc' VALUE TO_CHAR (
            C205_PART_DESC), 'pdet' VALUE TO_CHAR (C205_PART_DTLS_DESC), 'pst' VALUE TO_CHAR (C901_PART_STATUS), 'pfm'
            VALUE TO_CHAR (C901_PART_FAMILY), 'paf' VALUE TO_CHAR (C205_ACTIVE_FL), 'sid' VALUE TO_CHAR (C207_SYSTEM_ID
            ), 'prm' VALUE TO_CHAR (C205_PRODUCT_MATERIAL), 'prc' VALUE TO_CHAR (C205_PRODUCT_CLASS), 'pdi' VALUE
            TO_CHAR (C2060_DI_NUMBER), 'udi' VALUE TO_CHAR (C2060_UDI_PATTERN)) RETURNING CLOB) PART_MASTER_SYNC_DATA
           FROM MY_TEMP_PART_SYNC_UPDATE
       GROUP BY C205_PART_NUMBER_ID, C1900_COMPANY_ID, C901_LANGUAGE_ID;
BEGIN
	--
    FOR sync_data IN part_master_sync_data
    LOOP
        -- '30301' Manual Load, '103122' Newly inserted
        --save to master table
        gm_pkg_device_part_sync_to_master.GM_SAV_PART_MASTER_JSON (sync_data.PART_NUM_ID, sync_data.COMPID,
        sync_data.LANGID, sync_data.PART_MASTER_SYNC_DATA, '30301', '103122') ;
        COMMIT;
    END LOOP;
    
    ---update iPad Synced Flag
    gm_pkg_device_part_sync_to_master.GM_UPD_IPAD_SYNC_FLAG_COMPLETED;
    COMMIT;
    
    --Delete temp table
     DELETE FROM MY_TEMP_PART_SYNC_UPDATE;
END GM_SAV_PART_MASTER_JSON_MAIN;

/************************************************************************************************
Description : This procedure is used to insert / update into PART Master Table
Author      : jgurunathan
*************************************************************************************************/
PROCEDURE GM_SAV_PART_MASTER_JSON (
        p_part_num         IN T205S_PART_MASTER_SYNC.C205_PART_NUMBER_ID%TYPE,
        p_company_id       IN T205S_PART_MASTER_SYNC.C1900_COMPANY_ID%TYPE,
        p_language_id      IN T205S_PART_MASTER_SYNC.C901_LANGUAGE_ID%TYPE,
        p_part_master_json IN T205S_PART_MASTER_SYNC.C205S_PART_MASTER_SYNC_DATA%TYPE,
        p_user_id          IN T205S_PART_MASTER_SYNC.C205S_LAST_UPDATED_BY%TYPE,
        p_sync_action      IN T205S_PART_MASTER_SYNC.C901_SYNC_ACTION%TYPE)
AS
BEGIN
	
    -- '103123' if updated
     UPDATE T205S_PART_MASTER_SYNC
    SET C901_LANGUAGE_ID        = p_language_id, C205S_PART_MASTER_SYNC_DATA = p_part_master_json, C901_SYNC_ACTION = 103123
      , C205S_LAST_UPDATED_BY   = p_user_id, C205S_LAST_UPDATED_DATE = CURRENT_DATE
      WHERE C205_PART_NUMBER_ID = p_part_num
      	AND C1900_COMPANY_ID    = p_company_id
        AND C205S_VOID_FL      IS NULL;
    --      
    IF (SQL%ROWCOUNT = 0) THEN
         INSERT
           INTO T205S_PART_MASTER_SYNC
            (
                C205_PART_NUMBER_ID, C1900_COMPANY_ID, C901_LANGUAGE_ID
              , C205S_PART_MASTER_SYNC_DATA, C901_SYNC_ACTION, C205S_VOID_FL
              , C205S_LAST_UPDATED_BY, C205S_LAST_UPDATED_DATE
            )
            VALUES
            (
                p_part_num, p_company_id, p_language_id
              , p_part_master_json, p_sync_action, NULL
              , p_user_id, CURRENT_DATE
            ) ;
    END IF;
    --
END GM_SAV_PART_MASTER_JSON;
/******************************************************************
Description : This procedure is used to update ipad sync flag as 'Y'
Author      : jgurunathan
*******************************************************************/
PROCEDURE GM_UPD_IPAD_SYNC_FLAG_COMPLETED
AS
BEGIN
	--
     UPDATE T205_PART_NUMBER
    SET C205_IPAD_SYNCED_FLAG     = 'Y', C205_IPAD_SYNCED_DATE = CURRENT_DATE
      WHERE C205_IPAD_SYNCED_FLAG = 'P' ;
      
    --  
     UPDATE T2000_MASTER_METADATA
    SET C2000_IPAD_SYNCED_FLAG                = 'Y', C2000_IPAD_SYNCED_DATE = CURRENT_DATE
      WHERE C2000_IPAD_SYNCED_FLAG            = 'P'
        AND C901_REF_TYPE = 103090 ;
        
     --   
     UPDATE T2023_PART_COMPANY_MAPPING
    SET C2023_IPAD_SYNCED_FLAG                       = 'Y', C2023_IPAD_SYNCED_DATE = CURRENT_DATE
      WHERE C2023_IPAD_SYNCED_FLAG                   = 'P';
     -- 
END GM_UPD_IPAD_SYNC_FLAG_COMPLETED;
/************************************************************************************************
Description :This procedure is used for forming JSON data from temporary table into PART  Attr Sync Table
Author      : ppandiyan
*************************************************************************************************/
PROCEDURE GM_SAV_PART_ATTRIBUTE_JSON_MAIN
AS
v_attr_val t205d_part_attribute.C205D_ATTRIBUTE_VALUE%TYPE;
v_part_number_id t205d_part_attribute.C205_PART_NUMBER_ID%TYPE;
v_attr_json CLOB;
  	CURSOR part_attr_main_cur
    IS
        --getting comma separated all attribute values for single part
         SELECT T205D.C205_PART_NUMBER_ID PARTID, C205D_ATTRIBUTE_VALUE attr_val
           FROM t205d_part_attribute T205D
          WHERE C205D_IPAD_SYNCED_FLAG = 'P'
          AND C901_ATTRIBUTE_TYPE = 80180;
          
    CURSOR part_master_attr_data
    IS
        --getting comma separated all attribute values for single part
          SELECT T205D.C205_PART_NUMBER_ID PARTID, T901C.C1900_COMPANY_ID comp_id, JSON_OBJECT ('paid' VALUE
            TO_CHAR(T205D.C2051_PART_ATTRIBUTE_ID), 'pid' VALUE TO_CHAR(T205D.C205_PART_NUMBER_ID), 'pat' VALUE TO_CHAR(T205D.C901_ATTRIBUTE_TYPE)
            , 'pav' VALUE TO_CHAR(T205D.C205D_ATTRIBUTE_VALUE), 'pavf' VALUE TO_CHAR(T205D.C205D_VOID_FL)) ATTR_JSON_DATA
           FROM t205d_part_attribute T205D, T901C_RFS_COMPANY_MAPPING T901C
          WHERE C205D_IPAD_SYNCED_FLAG = 'P'
          AND T205D.C205D_ATTRIBUTE_VALUE = T901C.C901_RFS_ID 
          AND T205D.C205D_ATTRIBUTE_VALUE = v_attr_val
          AND T205D.C205_PART_NUMBER_ID = v_part_number_id
          AND C901_ATTRIBUTE_TYPE = 80180
        
          ;
BEGIN
	
	 FOR attr_main IN part_attr_main_cur
    LOOP
	v_attr_val := attr_main.attr_val;
	v_part_number_id := attr_main.partid;
	--
	
	 --looping the cursor to save attribute data
    FOR attr_data IN part_master_attr_data
    LOOP
        gm_pkg_device_part_sync_to_master.GM_SAV_PART_ATTRIBUTE_JSON (attr_data.PARTID, attr_data.comp_id, attr_data.ATTR_JSON_DATA,
        '30301') ;--Manual Load
        COMMIT;
    END LOOP;
    
    -- RFS 80120 -- US
    IF v_attr_val = 80120 THEN 
    
        SELECT JSON_OBJECT ('paid' VALUE
            TO_CHAR(T205D.C2051_PART_ATTRIBUTE_ID), 'pid' VALUE TO_CHAR(T205D.C205_PART_NUMBER_ID), 'pat' VALUE TO_CHAR(T205D.C901_ATTRIBUTE_TYPE)
            , 'pav' VALUE TO_CHAR(T205D.C205D_ATTRIBUTE_VALUE), 'pavf' VALUE TO_CHAR(T205D.C205D_VOID_FL)) ATTR_JSON_DATA INTO v_attr_json
           FROM t205d_part_attribute T205D
          WHERE   T205D.C205D_ATTRIBUTE_VALUE = v_attr_val
          AND T205D.C205_PART_NUMBER_ID = v_part_number_id
          AND C901_ATTRIBUTE_TYPE = 80180;
    
    gm_pkg_device_part_sync_to_master.GM_SAV_PART_ATTRIBUTE_JSON (v_part_number_id, 1000, v_attr_json, '30301') ;--Manual Load
         COMMIT;
        
    
    END IF;
    
	END LOOP;
	
   
    
    -- update iPad Sync Flag in set attribute table
     UPDATE T205D_PART_ATTRIBUTE
    SET C205D_IPAD_SYNCED_FLAG     = 'Y', C205D_IPAD_SYNCED_DATE = CURRENT_DATE
      WHERE C205D_IPAD_SYNCED_FLAG = 'P';
      
    --Delete temp table
    -- DELETE FROM MY_TEMP_KEY_VALUE;
     --
END GM_SAV_PART_ATTRIBUTE_JSON_MAIN;

/************************************************************************************************
Description : This procedure is used to insert / update into PART Attribute Table
Author      : ppandiyan
*************************************************************************************************/
PROCEDURE GM_SAV_PART_ATTRIBUTE_JSON (
        p_part_num         IN T205S_PART_MASTER_SYNC.C205_PART_NUMBER_ID%TYPE,
        p_company_id       IN T205S_PART_MASTER_SYNC.C1900_COMPANY_ID%TYPE,
        p_part_master_json IN T205S_PART_MASTER_SYNC.C205S_PART_MASTER_SYNC_DATA%TYPE,
        p_user_id          IN T205S_PART_MASTER_SYNC.C205S_LAST_UPDATED_BY%TYPE)
AS
BEGIN
	--
     UPDATE T205S_PART_ATTRIBUTE_SYNC
    SET C205S_PART_ATTRIBUTE_SYNC_DATA = p_part_master_json, C205S_LAST_UPDATED_BY = p_user_id, C205S_LAST_UPDATED_DATE
                                       = CURRENT_DATE
      WHERE C205_PART_NUMBER_ID        = p_part_num
      	AND C1900_COMPANY_ID 		   = p_company_id
        AND C205S_VOID_FL             IS NULL;
    --    
    IF (SQL%ROWCOUNT                   = 0) THEN
         INSERT
           INTO T205S_PART_ATTRIBUTE_SYNC
            (
                C205_PART_NUMBER_ID,C1900_COMPANY_ID, C205S_PART_ATTRIBUTE_SYNC_DATA, C205S_VOID_FL
              , C205S_LAST_UPDATED_BY, C205S_LAST_UPDATED_DATE
            )
            VALUES
            (
                p_part_num, p_company_id, p_part_master_json, NULL
              , p_user_id, CURRENT_DATE
            ) ;
    END IF;
    --
END GM_SAV_PART_ATTRIBUTE_JSON;
END GM_PKG_DEVICE_PART_SYNC_TO_MASTER;
/