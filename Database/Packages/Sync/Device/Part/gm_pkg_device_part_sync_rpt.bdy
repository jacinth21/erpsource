CREATE OR REPLACE
PACKAGE BODY gm_pkg_device_part_sync_rpt
IS

/******************************************************************
 Description : This procedure is used to fetch the part details
 Author      : T.S. Ramachandiran
*******************************************************************/
PROCEDURE gm_fch_all_part_tosync(
	p_comp_id    		IN	    	t205s_part_master_sync.C1900_COMPANY_ID%TYPE,
	p_part_out_dtl 		OUT 		CLOB
)
AS 
	v_part_out_dtl CLOB;
BEGIN
	BEGIN
	SELECT replace(JSON_ARRAYAGG (t205s.C205S_PART_MASTER_SYNC_DATA RETURNING CLOB),'null','""')
		INTO v_part_out_dtl
  		FROM T205S_PART_MASTER_SYNC t205s
 		WHERE t205s.c1900_company_id     	= p_comp_id
 		AND t205s.C205S_VOID_FL  IS NULL
 		AND JSON_EXISTS (C205S_PART_MASTER_SYNC_DATA, '$.paf?(@ == "Y")');
 			
	EXCEPTION
	WHEN NO_DATA_FOUND THEN
	v_part_out_dtl := NULL;
	END;
	
	p_part_out_dtl := v_part_out_dtl;
	
END gm_fch_all_part_tosync;


/*********************************************************************
 Description : This procedure is used to fetch the part update detail
 Author      : T.S. Ramachandiran
**********************************************************************/

PROCEDURE gm_fch_part_update_tosync(
 	p_comp_id	     			IN	    t205s_part_master_sync.C1900_COMPANY_ID%TYPE,
 	p_device_last_sync_date 	IN   	t9151_device_sync_dtl.c9151_last_sync_date%TYPE,
 	p_part_out_dtl 				OUT     CLOB
 	)
	AS
	
	v_part_out_dtl CLOB;
	
BEGIN	
 BEGIN
-- Get the latest update details from the device's last sync time.
  SELECT replace(JSON_ARRAYAGG (t205s.C205S_PART_MASTER_SYNC_DATA RETURNING CLOB),'null','""')
		INTO v_part_out_dtl
 	FROM t205s_part_master_sync t205s 
  	WHERE t205s.c1900_company_id     			= p_comp_id
    	AND t205s.c205s_last_updated_date 	>= p_device_last_sync_date
    	AND t205s.c205s_void_fl  IS null;
    	
  	EXCEPTION
  	WHEN NO_DATA_FOUND THEN
    	v_part_out_dtl := NULL;
    END;
    
    p_part_out_dtl   := v_part_out_dtl;
    
  END gm_fch_part_update_tosync;
  
  
 /**********************************************************************
 Description : This procedure is used to fetch the part attribute detail
 Author      : T.S. Ramachandiran
************************************************************************/
  PROCEDURE gm_fch_all_part_attribute_tosync(
  	p_comp_id    			IN	    	t205s_part_master_sync.C1900_COMPANY_ID%TYPE,
	p_part_attr_out_dtl 	OUT 		CLOB
 )
  AS
  
  v_part_attr_out_dtl CLOB;
  
 BEGIN	
 	BEGIN
	 SELECT replace(JSON_ARRAYAGG (t205sa.c205s_part_attribute_sync_data RETURNING CLOB),'null','""')
			INTO v_part_attr_out_dtl
  		FROM t205s_part_attribute_sync t205sa, t205s_part_master_sync t205s
  		WHERE t205s.c205_part_number_id			= t205sa.c205_part_number_id
  			AND t205s.c1900_company_id     		=  p_comp_id
  			AND t205sa.C1900_COMPANY_ID       	=  p_comp_id
  			AND t205s.c205s_void_fl  IS NULL
 			AND t205sa.c205s_void_fl IS NULL
			AND JSON_EXISTS (C205S_PART_MASTER_SYNC_DATA, '$.paf?(@ == "Y")');
	EXCEPTION
 	WHEN NO_DATA_FOUND THEN
    	v_part_attr_out_dtl := NULL;
 	END;
 	 p_part_attr_out_dtl            := v_part_attr_out_dtl; 
  END gm_fch_all_part_attribute_tosync;
  
  
    
 /*****************************************************************************
 Description : This procedure is used to fetch the part attribute update detail
 Author      : T.S. Ramachandiran
*******************************************************************************/
  
  PROCEDURE gm_fch_part_attribute_update_tosync(
  	p_comp_id    				IN	    	t205s_part_master_sync.c1900_company_id%TYPE,
  	p_device_last_sync_date 	IN   		t9151_device_sync_dtl.c9151_last_sync_date%TYPE,
	p_part_attr_out_dtl 		OUT 		CLOB
 ) 
 AS
 	v_part_attr_out_dtl CLOB;
 	
BEGIN
	BEGIN
		-- Get the latest update details from the device's last sync time. 
		SELECT  replace(JSON_ARRAYAGG (t205sa.c205s_part_attribute_sync_data RETURNING CLOB),'null','""')
  			INTO v_part_attr_out_dtl
		FROM t205s_part_attribute_sync t205sa, t205s_part_master_sync t205s
  		WHERE t205s.c205_part_number_id					= t205sa.c205_part_number_id
  			AND t205s.c1900_company_id     				=  p_comp_id
  			AND t205sa.c1900_company_id       			=  p_comp_id
 			AND t205sa.c205s_last_updated_date 	>= p_device_last_sync_date
  			AND t205s.c205s_void_fl  IS NULL
 			AND t205sa.c205s_void_fl IS NULL;
 		EXCEPTION
  		WHEN NO_DATA_FOUND THEN
    		v_part_attr_out_dtl := NULL;	
		END; 
	
  	p_part_attr_out_dtl            := v_part_attr_out_dtl;
  
  	END gm_fch_part_attribute_update_tosync;
END gm_pkg_device_part_sync_rpt;
/