/* Formatted on 2011/10/26 17:13 (Formatter Plus v4.8.0) */
--@"C:\Database\Packages\Sync\Device\gm_pkg_device_tag_sync_master.bdy";
CREATE OR REPLACE
PACKAGE BODY gm_pkg_device_tag_sync_master
IS
  --
  /******************************************************************
  Description : This procedure is used to Run the job for Record Tags
  Author      : mselvamani
  ****************************************************************/
PROCEDURE gm_sav_tag_master_sync_details(
    p_job_id  IN t9300_job.C9300_JOB_ID%TYPE,
    p_user_id IN VARCHAR2 )
AS
  v_last_sync_date DATE;
BEGIN
  --fetch the last sync date and update startdate as currentdate of job
  gm_pkg_device_job_run_date.gm_update_job_details(p_job_id,p_user_id,v_last_sync_date);
  
  --fetch tag details for Record Tags
  gm_pkg_device_tag_sync_master.gm_fch_tag_sync_records(v_last_sync_date,p_user_id);
  
  -- update job end date
  gm_pkg_device_tag_sync_master.gm_update_end_date_job(p_job_id,p_user_id);
END gm_sav_tag_master_sync_details;
--
/******************************************************************
Description : This procedure is used to fetch tag details for Record Tags
Author      : mselvamani
****************************************************************/
PROCEDURE gm_fch_tag_sync_records(
    p_last_sync_date IN t5010_tag.C5010_Last_Updated_Date%TYPE,
    p_user_id        IN VARCHAR2 )
AS
  v_tagid t5010_tag.c5010_tag_id%TYPE;
  v_cmpid t5010_tag.c1900_company_id%TYPE;
  v_sync_data CLOB;
  v_set_id_out t5010_tag.c207_set_id%TYPE;
  
  CURSOR c_tag_dtl
  IS
    SELECT c5010_tag_id tagid,
      c1900_company_id cmpid,
      ( Json_Object( 'tid' Value C5010_Tag_Id,
					 'sid' Value DECODE(c207_set_id,NULL,gm_pkg_common.get_part_primary_set_id(c205_part_number_id),c207_set_id),
					 'sts' Value C901_Status,
					 'vfl' Value C5010_Void_Fl )) sync_data
    FROM T5010_Tag
    WHERE C5010_Last_Updated_Date >= p_last_sync_date;
	
BEGIN
  FOR tagdet IN c_tag_dtl
  LOOP
    v_tagid     := tagdet.tagid;
    v_cmpid     := tagdet.cmpid;
    v_sync_data := tagdet.sync_data;
    -- Selected tag details save in new table(T5010D_DEVICE_TAG_SYNC_MASTER)
    gm_pkg_device_tag_sync_master.gm_sav_tag_detail_sync(v_tagid,v_sync_data,v_cmpid,NULL,p_user_id);
  END LOOP;
END gm_fch_tag_sync_records;
--
/******************************************************************
Description : This procedure is used to save the tag details  for Record Tags
Author      : mselvamani
****************************************************************/
PROCEDURE gm_sav_tag_detail_sync(
    p_id        IN T5010D_DEVICE_TAG_SYNC_MASTER.C5010_TAG_ID%TYPE,
    p_sync_data IN T5010D_DEVICE_TAG_SYNC_MASTER.C5010D_DEVICE_TAG_SYNC_DATA%TYPE,
    p_comp_id   IN T5010D_DEVICE_TAG_SYNC_MASTER.C1900_Company_Id%TYPE,
    p_void_fl   IN T5010D_DEVICE_TAG_SYNC_MASTER.C5010D_DEVICE_TAG_SYNC_VOID_FL%TYPE,
    p_user_id   IN T5010D_DEVICE_TAG_SYNC_MASTER.C5010D_LAST_UPDATED_BY%TYPE )
AS
BEGIN

  UPDATE T5010D_DEVICE_TAG_SYNC_MASTER
  SET C5010D_DEVICE_TAG_SYNC_DATA = P_Sync_Data ,
    C1900_Company_Id              = p_comp_id,
    C5010D_LAST_UPDATED_BY        = p_user_id,
    C5010D_LAST_UPDATED_DATE      = CURRENT_DATE
  WHERE C5010_Tag_Id              = p_id
   AND C5010D_DEVICE_TAG_SYNC_VOID_FL IS NULL;
   
  IF SQL%ROWCOUNT                 = 0 THEN
    INSERT
    INTO T5010D_DEVICE_TAG_SYNC_MASTER
      (
        C5010_TAG_ID,
        C5010D_DEVICE_TAG_SYNC_DATA,
        C1900_Company_Id,
        C5010D_DEVICE_TAG_SYNC_VOID_FL,
        C5010D_LAST_UPDATED_BY,
        C5010D_LAST_UPDATED_DATE
      )
      VALUES
      (
        p_id ,
        p_sync_data ,
        p_comp_id,
        p_void_fl,
        p_user_id,
        CURRENT_DATE
      );
  END IF;
END gm_sav_tag_detail_sync;
--
/******************************************************************
Description : This procedure is used to update the end date of job
Author      : mselvamani
****************************************************************/
PROCEDURE gm_update_end_date_job
  (
    p_job_id  IN t9300_job.C9300_JOB_ID%TYPE,
    p_user_id IN t9300_job.C9300_LAST_UPDATED_BY%TYPE
  )
AS
BEGIN
  UPDATE t9300_job
  SET c9300_end_date       = CURRENT_DATE,
    C9300_LAST_UPDATED_BY  = p_user_id,
    C9300_LAST_UPDATED_DATE= CURRENT_DATE
  WHERE C9300_job_id       = p_job_id;
END gm_update_end_date_job;
--
END gm_pkg_device_tag_sync_master;
/