/* Formatted on 2019/11/19 15:32 (Formatter Plus v4.8.0) */
-- @"C:\Database\Packages\Sync\Device\Common\gm_pkg_device_sync_update_dtls.bdy"

CREATE OR REPLACE PACKAGE BODY gm_pkg_device_sync_update_dtls
IS

	/*********************************************************************
 	* Description : Procedure used to fetch language and sync count detail
 	* Author      : Ramachandiran T.S
	**********************************************************************/

PROCEDURE gm_fch_device_updates(
	
		p_device_infoid  			IN    t9151_device_sync_dtl.c9150_device_info_id%TYPE,
    	p_language_id    			IN    t9151_device_sync_dtl.c901_language_id%TYPE,
    	p_sync_type      			IN    t9151_device_sync_dtl.c901_sync_type%TYPE,
    	p_device_last_sync_date 	OUT   t9151_device_sync_dtl.c9151_last_sync_date%TYPE,
    	p_sync_count     			OUT   NUMBER
	
	)
	AS
	
	v_hist_sync_count 			NUMBER := 0;
    v_lang_cnt        			NUMBER := 0;
	v_device_last_sync_date 	t9151_device_sync_dtl.c9151_last_sync_date%TYPE;
	
	BEGIN	
	SELECT COUNT (1)
	   INTO v_lang_cnt
    FROM T901_Code_Lookup
    WHERE C901_CODE_ID = p_language_id
    AND C901_Code_Grp  = 'MLANG'
    AND C901_ACTIVE_FL = 1
    AND C901_VOID_FL  IS NULL;

 
    IF v_lang_cnt      = 0 THEN
    raise_application_error (-20606, ''); --No Language Defined
   END IF;

  BEGIN 
   SELECT c9151_last_sync_date into v_device_last_sync_date 
   FROM t9151_device_sync_dtl
   WHERE c9150_device_info_id = p_device_infoid
   AND c901_sync_type         = p_sync_type
   AND c901_language_id       = p_language_id
   AND (c901_sync_status      = 103121 --success
   OR ( c901_sync_status      = 103120 --Initiated -- date also return
   AND c9151_last_sync_date  IS NOT NULL))
   AND c9151_void_fl	IS NULL;
   
   p_device_last_sync_date := v_device_last_sync_date;
   
 EXCEPTION
  	WHEN NO_DATA_FOUND THEN
    v_device_last_sync_date := NULL;
  END;
  
   IF v_device_last_sync_date IS NOT NULL
   THEN
   p_sync_count  := 1;
   ELSE
   p_sync_count  := 0;
   END IF;
     
  END gm_fch_device_updates;
  
 END gm_pkg_device_sync_update_dtls;
 /