-- @"C:\Database\Packages\Sync\Device\Common\gm_pkg_device_common_sync.bdy";
CREATE OR REPLACE PACKAGE BODY GM_PKG_DEVICE_COMMON_SYNC
IS
    /*******************************************************************************************
    * Purpose: This function to fetch the language availability in master meta data table
    * and if not available default return is English
    * author: Jayaprasanth Gurunathan
    *******************************************************************************************/
FUNCTION get_ipad_lang_id (
        p_ref_type IN T2000_MASTER_METADATA.C901_REF_TYPE%TYPE,
        p_ref_id   IN T2000_MASTER_METADATA.C2000_REF_ID%TYPE,
        p_lang_id  IN T2000_MASTER_METADATA.C901_LANGUAGE_ID%TYPE)
    RETURN T2000_MASTER_METADATA.C901_LANGUAGE_ID%TYPE
IS
    v_lang_id T2000_MASTER_METADATA.C901_LANGUAGE_ID%TYPE;
BEGIN
	--
     SELECT t2000.C901_LANGUAGE_ID
       INTO v_lang_id
       FROM T2000_MASTER_METADATA t2000
      WHERE t2000.C901_LANGUAGE_ID = p_lang_id
        AND t2000.C901_REF_TYPE    = p_ref_type
        AND C2000_REF_ID           = p_ref_id
        AND t2000.C2000_VOID_FL   IS NULL;
    --    
    RETURN v_lang_id;
EXCEPTION
WHEN OTHERS THEN
    RETURN 103097;
END get_ipad_lang_id;

/************************************************************************************************
Description : This procedure is used to  update user id into  Master Table
Author      : ppandiyan
*************************************************************************************************/
PROCEDURE GM_UPD_LAST_SYNC_DATE (
        p_set_master_seq_id IN T207S_SET_MASTER_SYNC.C207S_SET_MASTER_SYNC_ID%TYPE,
        p_sync_date         IN T207S_SET_MASTER_SYNC.C207S_LAST_UPDATED_DATE%TYPE,
        p_user_id           IN T207S_SET_MASTER_SYNC.C207S_LAST_UPDATED_BY%TYPE)
AS
BEGIN
	--
     UPDATE T207S_SET_MASTER_SYNC
    SET C207S_LAST_UPDATED_BY        = p_user_id, C207S_LAST_UPDATED_DATE = p_sync_date
      WHERE C207S_SET_MASTER_SYNC_ID = p_set_master_seq_id;
    --  
END GM_UPD_LAST_SYNC_DATE;

/************************************************************************************************
Description : This procedure is used to  update into Master Table
Author      : ppandiyan
*************************************************************************************************/
PROCEDURE GM_UPD_MASTER_SYNC_FLAG (
        p_set_grp_type   IN T207_SET_MASTER.C901_SET_GRP_TYPE%TYPE,
        p_ipad_sync_flag IN CHAR,
        p_ref_type       IN T2000_MASTER_METADATA.C901_REF_TYPE%TYPE,
        p_user_id        IN VARCHAR2)
AS
BEGIN
	
    --updating set master table
     UPDATE T207_SET_MASTER
    SET C207_IPAD_SYNCED_FLAG = p_ipad_sync_flag, C207_LAST_UPDATED_BY = p_user_id, C207_LAST_UPDATED_DATE =
        CURRENT_DATE
      WHERE C901_SET_GRP_TYPE      = p_set_grp_type
        AND C207_IPAD_SYNCED_FLAG IS NULL;
        
    --updating  master meta table
     UPDATE T2000_MASTER_METADATA
    SET C2000_IPAD_SYNCED_FLAG = p_ipad_sync_flag, C2000_LAST_UPDATED_BY = p_user_id, C2000_LAST_UPDATED_DATE =
        CURRENT_DATE
      WHERE C901_REF_TYPE           = p_ref_type
        AND C2000_IPAD_SYNCED_FLAG IS NULL;
        
    --updating set company mapping
     UPDATE T2080_SET_COMPANY_MAPPING
    SET C2080_IPAD_SYNCED_FLAG = p_ipad_sync_flag, C2080_LAST_UPDATED_BY = p_user_id, C2080_LAST_UPDATED_DATE =
        CURRENT_DATE
      WHERE C2080_IPAD_SYNCED_FLAG IS NULL
        AND C207_SET_ID            IN
        (
             SELECT C207_SET_ID
               FROM T207_SET_MASTER
              WHERE C901_SET_GRP_TYPE = p_set_grp_type
        ) ;
        
    -- system type, then updat the set link    
    IF p_set_grp_type = 1600 THEN
         UPDATE T207A_SET_LINK
        SET C207A_IPAD_SYNCED_FLAG      = p_ipad_sync_flag
          WHERE C207A_IPAD_SYNCED_FLAG IS NULL;
    ELSE
    	 UPDATE T208_SET_DETAILS
	    SET C208_IPAD_SYNCED_FLAG      = p_ipad_sync_flag
	      WHERE C208_IPAD_SYNCED_FLAG IS NULL
	        AND C207_SET_ID           IN
	        (
	             SELECT T207.C207_SET_ID
	               FROM T207_SET_MASTER T207
	              WHERE T207.C901_SET_GRP_TYPE      = 1601--Set
	        ) ;
    END IF;
    
    -- System/Set attribute
     UPDATE T207C_SET_ATTRIBUTE
    SET C207C_IPAD_SYNCED_FLAG      = p_ipad_sync_flag
      WHERE C207C_IPAD_SYNCED_FLAG IS NULL
        AND C207_SET_ID            IN
        (
             SELECT T207.C207_SET_ID
               FROM T207_SET_MASTER T207
              WHERE T207.C901_SET_GRP_TYPE = p_set_grp_type--System
        ) ;
        
END GM_UPD_MASTER_SYNC_FLAG;

/*********************************************************
Description : This procedure is used to save set id to temp
Author      : vramanathan
************************************************************/
PROCEDURE GM_SAV_SYSTEM_ID_TO_TMP
AS
BEGIN
     DELETE FROM my_temp_list;
    -- to insert into temp
     INSERT INTO my_temp_list
        (my_temp_txn_id
        )
     SELECT c207_set_id setid
       FROM t207_set_master
      WHERE c901_set_grp_type = '1600' --sales report grp
        AND c207_void_fl     IS NULL;
END GM_SAV_SYSTEM_ID_TO_TMP;

/*********************************************************
Description : This procedure is used to save set id to temp
Author      : vramanathan
************************************************************/
FUNCTION GET_UDI_CONFIG_WITH_NAME (
        p_part_number IN T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE)
    RETURN V1610_ISSUING_AGENCY_CONFIG.C1610_ISSUE_AGENCY_CONFIG_NM%TYPE
IS
    v_config_nm V1610_ISSUING_AGENCY_CONFIG.C1610_ISSUE_AGENCY_CONFIG_NM%TYPE;
BEGIN
     SELECT REPLACE (REPLACE (V1610.ISSUING_AGENCY_CONFIG_WITH_NM, '|', ''), 'DI', T2060.C2060_DI_NUMBER)
       INTO v_config_nm
       FROM T2060_DI_PART_MAPPING T2060, T205_PART_NUMBER T205, T205D_PART_ATTRIBUTE T205D
      , V1610_ISSUING_AGENCY_CONFIG V1610
      WHERE T205.C205_PART_NUMBER_ID     = T2060.C205_PART_NUMBER_ID(+)
        AND T205.C205_PART_NUMBER_ID     = T205D.C205_PART_NUMBER_ID(+)
        AND T205D.C901_ATTRIBUTE_TYPE(+) = 4000539 -- Issuing Agency Configuration
        AND T205D.C205D_ATTRIBUTE_VALUE  = V1610.C1610_ISSUE_AGENCY_CONFIG_ID(+)
        AND T205.C205_PART_NUMBER_ID     = p_part_number
        AND t205d.c205d_void_fl(+)      IS NULL;
    RETURN v_config_nm;
EXCEPTION
WHEN OTHERS THEN
    RETURN NULL;
END GET_UDI_CONFIG_WITH_NAME;

/*********************************************************
Description : This procedure is used update the ipad sync flag to null
Author      :
************************************************************/
PROCEDURE GM_SAV_SYSTEM_PUBLISH_NEW (
        p_system_id IN T207_SET_MASTER.c207_set_id%TYPE)
AS
BEGIN
	--
    dbms_output.put_line ('GM_SAV_SYSTEM_PUBLISH_NEW Start time ==> ' || TO_CHAR (sysdate, 'MM/dd/yyyy HH:MI AM')) ;
    
     GM_UPD_SET_IPAD_SYNC_FLAG(p_system_id);
     GM_UPD_GROUP_IPAD_SYNC_FLAG(p_system_id);
     GM_UPD_PART_IPAD_SYNC_FLAG(p_system_id);
     GM_UPD_FILE_IPAD_SYNC_FLAG(p_system_id);
    
    --   
    dbms_output.put_line ('GM_SAV_SYSTEM_PUBLISH_NEW END time ==> ' || TO_CHAR (sysdate, 'MM/dd/yyyy HH:MI AM')) ;
    --
END GM_SAV_SYSTEM_PUBLISH_NEW;


/*********************************************************
Description : This procedure is used update the system/set related ipad sync flag to null
Author      : jgurunathan
************************************************************/
PROCEDURE GM_UPD_SET_IPAD_SYNC_FLAG (
        p_system_id IN T207_SET_MASTER.c207_set_id%TYPE)
    AS
    BEGIN
 -- to update Set ipad flag based on system id
     UPDATE T207_SET_MASTER
    SET C207_IPAD_SYNCED_FLAG = NULL, C207_IPAD_SYNCED_DATE = NULL
      WHERE C207_SET_ID      IN
        (
             SELECT C207_SET_ID
               FROM T207_SET_MASTER
              WHERE C207_SET_SALES_SYSTEM_ID = p_system_id
              AND c901_status_id = 20367 -- Approved
        )
        AND c207_void_fl IS NULL;
        
    -- to update ipad set details flag based on system id
    
     UPDATE T208_SET_DETAILS
    SET C208_IPAD_SYNCED_FLAG = NULL, C208_IPAD_SYNCED_DATE = NULL
      WHERE C207_SET_ID      IN
        (
             SELECT C207_SET_ID
               FROM T207_SET_MASTER
              WHERE C207_SET_SALES_SYSTEM_ID = p_system_id
              AND c901_status_id = 20367 -- Approved
        )
        AND C208_VOID_FL IS NULL;
        
     -- to update set Link ipad flag based on system id
     UPDATE T207A_SET_LINK
    SET C207A_IPAD_SYNCED_FLAG = NULL, C207A_IPAD_SYNCED_DATE = NULL
      WHERE C207_MAIN_SET_ID   = p_system_id;
     
     -- to update system attribute ipad flag based on system id 
     UPDATE T207C_SET_ATTRIBUTE
    SET C207C_IPAD_SYNCED_FLAG = NULL, C207C_IPAD_SYNCED_DATE = NULL
      WHERE C207_SET_ID        = p_system_id
        AND C207C_VOID_FL     IS NULL;
        
     -- to update set attribute ipad flag based on system id   
     UPDATE T207C_SET_ATTRIBUTE
    SET C207C_IPAD_SYNCED_FLAG = NULL, C207C_IPAD_SYNCED_DATE = NULL
      WHERE C207_SET_ID       IN
        (
             SELECT C207_SET_ID
               FROM T207_SET_MASTER
              WHERE C207_SET_SALES_SYSTEM_ID = p_system_id
              AND c901_status_id = 20367 -- Approved
        )
        AND C207C_VOID_FL IS NULL;
        
    -- to update meta data system ipad flag based on system id
     UPDATE T2000_MASTER_METADATA
    SET C2000_IPAD_SYNCED_FLAG = NULL, C2000_IPAD_SYNCED_DATE = NULL
      WHERE C2000_REF_ID       = p_system_id
        AND C2000_VOID_FL     IS NULL;
        
    -- to update meta data set ipad flag based on system id   
     UPDATE T2000_MASTER_METADATA
    SET C2000_IPAD_SYNCED_FLAG = NULL, C2000_IPAD_SYNCED_DATE = NULL
      WHERE C2000_REF_ID      IN
        (
             SELECT C207_SET_ID
               FROM T207_SET_MASTER
              WHERE C207_SET_SALES_SYSTEM_ID = p_system_id
              AND c901_status_id = 20367 -- Approved
        )
        AND C2000_VOID_FL IS NULL;
        
    -- to update system company mapping ipad flag based on system id
     UPDATE T2080_SET_COMPANY_MAPPING
    SET C2080_IPAD_SYNCED_FLAG = NULL, C2080_IPAD_SYNCED_DATE = NULL
      WHERE C207_SET_ID        = p_system_id
        AND C2080_VOID_FL     IS NULL;
        
     -- to update set company mapping ipad flag based on system id   
     UPDATE T2080_SET_COMPANY_MAPPING
    SET C2080_IPAD_SYNCED_FLAG = NULL, C2080_IPAD_SYNCED_DATE = NULL
      WHERE C207_SET_ID       IN
        (
             SELECT C207_SET_ID
               FROM T207_SET_MASTER
              WHERE C207_SET_SALES_SYSTEM_ID = p_system_id
              AND c901_status_id = 20367 -- Approved
        )
        AND C2080_VOID_FL IS NULL;
          
    
END GM_UPD_SET_IPAD_SYNC_FLAG;

/*********************************************************
Description : This procedure is used update the group related tables ipad sync flag to null
Author      : jgurunathan
************************************************************/
PROCEDURE GM_UPD_GROUP_IPAD_SYNC_FLAG (
        p_system_id IN T207_SET_MASTER.c207_set_id%TYPE)
    AS
   BEGIN
	   --
    
    -- to update group ipad flag based on system id
     UPDATE T4010_GROUP
    SET C4010_IPAD_SYNCED_FLAG = NULL, C4010_IPAD_SYNCED_DATE = NULL
      WHERE C207_SET_ID        = p_system_id
      	AND c901_type          = 40045 --Forecast/Demand Group
      	AND C4010_PUBLISH_FL  IS NOT NULL
        AND c4010_inactive_fl IS NULL
        AND C4010_VOID_FL     IS NULL;
        
    -- to update group details ipad flag based on system id   
     UPDATE T4011_GROUP_DETAIL
    SET C4011_IPAD_SYNCED_FLAG = NULL, C4011_IPAD_SYNCED_DATE = NULL
      WHERE C4010_GROUP_ID    IN
        (
             SELECT C4010_GROUP_ID
               FROM T4010_GROUP
              WHERE C207_SET_ID    = p_system_id
              	AND c901_type          = 40045 --Forecast/Demand Group
		      	AND C4010_PUBLISH_FL  IS NOT NULL
		        AND c4010_inactive_fl IS NULL
                AND C4010_VOID_FL IS NULL
        ) ;
        
      -- to update the group company map ipad flag based on system id 
     UPDATE T4012_GROUP_COMPANY_MAPPING
    SET C4012_IPAD_SYNCED_FLAG = NULL, C4012_IPAD_SYNCED_DATE = NULL
      WHERE C4010_GROUP_ID    IN
        (
             SELECT C4010_GROUP_ID FROM T4010_GROUP WHERE C207_SET_ID = p_system_id
             	AND c901_type          = 40045 --Forecast/Demand Group
		      	AND C4010_PUBLISH_FL  IS NOT NULL
		        AND c4010_inactive_fl IS NULL
                AND C4010_VOID_FL IS NULL
        )
        AND C4012_VOID_FL IS NULL;
        
     -- to update group meta ipad flag based on system id   
     UPDATE T2000_MASTER_METADATA
    SET C2000_IPAD_SYNCED_FLAG = NULL, C2000_IPAD_SYNCED_DATE = NULL
      WHERE C2000_REF_ID      IN
        (
             SELECT TO_CHAR (C4010_GROUP_ID)
               FROM T4010_GROUP
              WHERE C207_SET_ID = p_system_id
              AND c901_type          = 40045 --Forecast/Demand Group
		      	AND C4010_PUBLISH_FL  IS NOT NULL
		        AND c4010_inactive_fl IS NULL
                AND C4010_VOID_FL IS NULL
        )
        AND C2000_VOID_FL IS NULL;
    --
    
END GM_UPD_GROUP_IPAD_SYNC_FLAG;

/*********************************************************
Description : This procedure is used update the part related tables ipad sync flag to null
Author      : jgurunathan
************************************************************/
PROCEDURE GM_UPD_PART_IPAD_SYNC_FLAG (
        p_system_id IN T207_SET_MASTER.c207_set_id%TYPE)
    AS
  BEGIN
	  --
        
    -- to update Part number ipad flag based on system id
     UPDATE T205_PART_NUMBER
    SET C205_IPAD_SYNCED_FLAG    = NULL, C205_IPAD_SYNCED_DATE = NULL
      WHERE C205_PART_NUMBER_ID IN
        (
             SELECT c205_part_number_id
               FROM t208_set_details t208, t207_set_master t207
              WHERE t208.c207_set_id       = t207.c207_set_id
                AND t207.c207_set_id       = p_system_id
                AND t207.c901_set_grp_type = 1600
        )
        AND C205_ACTIVE_FL      = 'Y'
        AND C205_RELEASE_FOR_SALE = 'Y'
        AND C205_PRODUCT_FAMILY IN (SELECT C906_RULE_VALUE
										   FROM T906_RULES
										  WHERE C906_RULE_ID     = 'PART_PROD_FAMILY'
											AND C906_RULE_GRP_ID = 'PROD_CAT_SYNC'
											AND c906_void_fl    IS NULL);

	-- to update part attribute ipad flag based on system id   
     UPDATE T205D_PART_ATTRIBUTE
    SET C205D_IPAD_SYNCED_FLAG   = NULL, C205D_IPAD_SYNCED_DATE = NULL
      WHERE C205_PART_NUMBER_ID IN
        (
             SELECT c205_part_number_id
               FROM t208_set_details t208, t207_set_master t207
              WHERE t208.c207_set_id       = t207.c207_set_id
                AND t207.c207_set_id       = p_system_id
                AND t207.c901_set_grp_type = 1600
        )
        AND C205D_VOID_FL      IS NULL
        AND C901_ATTRIBUTE_TYPE = 80180;
        
     -- to update part company map ipad flag based on system id  
     UPDATE T2023_PART_COMPANY_MAPPING
    SET C2023_IPAD_SYNCED_FLAG   = NULL, C2023_IPAD_SYNCED_DATE = NULL
      WHERE C205_PART_NUMBER_ID IN
        (
             SELECT c205_part_number_id
               FROM t208_set_details t208, t207_set_master t207
              WHERE t208.c207_set_id       = t207.c207_set_id
                AND t207.c207_set_id       = p_system_id
                AND t207.c901_set_grp_type = 1600
        )
        AND C2023_VOID_FL IS NULL;
        
    -- to update part meta ipad flag based on system id
     UPDATE T2000_MASTER_METADATA
    SET C2000_IPAD_SYNCED_FLAG = NULL, C2000_IPAD_SYNCED_DATE = NULL
      WHERE C2000_REF_ID      IN
        (
             SELECT c205_part_number_id
               FROM t208_set_details t208, t207_set_master t207
              WHERE t208.c207_set_id       = t207.c207_set_id
                AND t207.c207_set_id       = p_system_id
                AND t207.c901_set_grp_type = 1600
        )
        AND C2000_VOID_FL IS NULL;
    --
    
    
END GM_UPD_PART_IPAD_SYNC_FLAG;

/*********************************************************
Description : This procedure is used update the file related tables ipad sync flag to null
Author      : jgurunathan
************************************************************/
PROCEDURE GM_UPD_FILE_IPAD_SYNC_FLAG (
        p_system_id IN T207_SET_MASTER.c207_set_id%TYPE)
    AS
    BEGIN
      -- to update file data iPad falg based on system id
		-- System files
		UPDATE T903_UPLOAD_FILE_LIST SET C903_IPAD_SYNCED_FLAG = NULL , C903_IPAD_SYNCED_DATE = NULL
		WHERE C903_REF_ID      = p_system_id
		AND C901_REF_TYPE IN
		  (SELECT C906_RULE_VALUE
		  FROM T906_RULES
		  WHERE C906_RULE_ID   = 'FILE_TYPE'
		  AND C906_RULE_GRP_ID = 'PROD_CAT_SYNC'
		  AND C906_VOID_FL    IS NULL
		  ) --Image, Videos, DCOed Documents ,Non DCOed Documents
		AND C901_REF_GRP   IS NOT NULL
		AND C903_DELETE_FL IS NULL;
		
		-- Set files
		UPDATE T903_UPLOAD_FILE_LIST SET C903_IPAD_SYNCED_FLAG = NULL , C903_IPAD_SYNCED_DATE = NULL
		WHERE C903_REF_ID IN
		(SELECT C207_SET_ID FROM T207_SET_MASTER
		WHERE C207_SET_SALES_SYSTEM_ID  = p_system_id
		AND C901_STATUS_ID              = 20367 -- Approved
		) AND C901_REF_TYPE IN
		(SELECT C906_RULE_VALUE FROM T906_RULES
		WHERE C906_RULE_ID   = 'FILE_TYPE'
		AND C906_RULE_GRP_ID = 'PROD_CAT_SYNC'
		AND C906_VOID_FL    IS NULL
		) --Image, Videos, DCOed Documents ,Non DCOed Documents
		AND C901_REF_GRP IS NOT NULL AND C903_DELETE_FL IS NULL; 
		  
		-- Group files
		UPDATE T903_UPLOAD_FILE_LIST SET C903_IPAD_SYNCED_FLAG = NULL , C903_IPAD_SYNCED_DATE = NULL
		WHERE C903_REF_ID IN
		  (SELECT to_char(C4010_GROUP_ID) FROM T4010_GROUP
		  WHERE C207_SET_ID       = p_system_id
		  AND C901_TYPE          = 40045 --Forecast/Demand Group
		  AND C4010_PUBLISH_FL  IS NOT NULL
		  AND C4010_INACTIVE_FL IS NULL
		  AND C4010_VOID_FL     IS NULL
		  )
		AND C901_REF_TYPE IN
		  (SELECT C906_RULE_VALUE FROM T906_RULES
		  WHERE C906_RULE_ID   = 'FILE_TYPE'
		  AND C906_RULE_GRP_ID = 'PROD_CAT_SYNC'
		  AND C906_VOID_FL    IS NULL
		  ) --Image, Videos, DCOed Documents ,Non DCOed Documents
		AND C901_REF_GRP   IS NOT NULL
		AND C903_DELETE_FL IS NULL;
		
		-- Part files                  
		UPDATE T903_UPLOAD_FILE_LIST SET C903_IPAD_SYNCED_FLAG = NULL , C903_IPAD_SYNCED_DATE = NULL
		WHERE C903_REF_ID IN
		  (SELECT C205_PART_NUMBER_ID FROM T205_PART_NUMBER
		  WHERE C205_PART_NUMBER_ID IN
		    (SELECT C205_PART_NUMBER_ID FROM T208_SET_DETAILS T208,
		      T207_SET_MASTER T207
		    WHERE T208.C207_SET_ID     = T207.C207_SET_ID
		    AND T207.C207_SET_ID        = p_system_id
		    AND T207.C901_SET_GRP_TYPE = 1600
		    )
		  AND C205_ACTIVE_FL        = 'Y'
		  AND C205_RELEASE_FOR_SALE = 'Y'
		  AND C205_PRODUCT_FAMILY  IN
		    (SELECT C906_RULE_VALUE FROM T906_RULES
		    WHERE C906_RULE_ID   = 'PART_PROD_FAMILY'
		    AND C906_RULE_GRP_ID = 'PROD_CAT_SYNC'
		    AND C906_VOID_FL    IS NULL
		    )
		  )
		AND C901_REF_TYPE IN
		  (SELECT C906_RULE_VALUE FROM T906_RULES
		  WHERE C906_RULE_ID   = 'FILE_TYPE'
		  AND C906_RULE_GRP_ID = 'PROD_CAT_SYNC'
		  AND C906_VOID_FL    IS NULL
		  ) --Image, Videos, DCOed Documents ,Non DCOed Documents
		AND C901_REF_GRP   IS NOT NULL
		AND C903_DELETE_FL IS NULL;
        
    --
   
END GM_UPD_FILE_IPAD_SYNC_FLAG;
/*
 
DECLARE
cursor system_upd_cur
IS
  select c207_set_id id from t207_set_master where C901_SET_GRP_TYPE = 1600 and C901_PUBLISHED_STATUS IS NOT NULL;
BEGIN
FOR sys IN system_upd_cur LOOP
    
GM_PKG_DEVICE_COMMON_SYNC.gm_sav_system_publish_new (sys.id);
END LOOP;
END;


exec GM_PKG_DEVICE_COMMON_SYNC.GM_PROCESS_IPAD_SYNC_MAIN;

 */

 /*********************************************************
    Description : This procedure is used call all the sync main procedure
    Author      : 
    ************************************************************/    
    PROCEDURE GM_PROCESS_IPAD_SYNC_MAIN
    AS
    
    BEGIN
	    --
	    dbms_output.put_line ('*********************** Start time GM_PROCESS_IPAD_SYNC_MAIN END time ==> ' || TO_CHAR (sysdate, 'MM/dd/yyyy HH:MI AM')) ;
	    
	    -- 1 File update
	    gm_pkg_device_file_sync_json_master.GM_SAV_FILE_SYNC_MAIN;
	    
	    -- 2 Part update
	    GM_PKG_DEVICE_PART_SYNC_TO_TEMP.GM_SAV_PART_MASTER_SYNC_MAIN;
	    
	    -- 3 Group update
	    gm_pkg_device_group_sync_temp.GM_SAV_GROUP_MASTER_SYNC_MAIN;
	    
	    -- 4 Set Update
	    GM_PKG_DEVICE_SET_SYNC_TO_TEMP.GM_SAV_SET_MASTER_SYNC_MAIN;
	    
	    -- 5 System update
	    GM_PKG_DEVICE_SYSTEM_SYNC_TO_TEMP.GM_SAV_SYSTEM_MASTER_SYNC_MAIN;
	    
	    --
	    dbms_output.put_line ('*********************** End time GM_PROCESS_IPAD_SYNC_MAIN END time ==> ' || TO_CHAR (sysdate, 'MM/dd/yyyy HH:MI AM')) ;
	    
	    END GM_PROCESS_IPAD_SYNC_MAIN;
	    

END GM_PKG_DEVICE_COMMON_SYNC;
/