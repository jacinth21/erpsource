/* Formatted on 2019/11/19 15:32 (Formatter Plus v4.8.0) */
-- @"C:\database\Packages\Sync\System\gm_pkg_device_system_master_sync_rpt.bdy"

CREATE OR REPLACE PACKAGE BODY gm_pkg_device_system_master_sync_rpt
IS

	/************************************************************
 	* Description : Procedure used to fetch the syStem sync detail
 	* Author      : Ramachandiran T.S
	**************************************************************/

PROCEDURE gm_fch_systems_prodcat  (
		p_token  		IN 		t101a_user_token.c101a_token_id%TYPE,
		p_langid		IN		NUMBER,
		p_page_no		IN		NUMBER,
	    p_uuid		    IN      t9150_device_info.c9150_device_uuid%TYPE,
	    p_comp_id	    IN	    t1900_company.c1900_company_id%TYPE,
		p_out_system	OUT		CLOB
	)
	AS
	v_device_infoid    			t9150_device_info.c9150_device_info_id%TYPE;
    v_sync_count       			NUMBER;
    v_system_header    			CLOB;
    v_system_out_dtl       		CLOB;
    v_json_out        	 		JSON_OBJECT_T;
    v_device_last_sync_date 	t9151_device_sync_dtl.c9151_last_sync_date%TYPE;
    keys_string        			CLOB;
    
   	BEGIN
	   
	   	--get the device info id based on user token
		v_device_infoid := get_device_id(p_token, p_uuid);
		--get the device update details based on sync type
		--103108 - System Device Sync Type
		gm_pkg_device_sync_update_dtls.gm_fch_device_updates(v_device_infoid,p_langid,103108,v_device_last_sync_date,v_sync_count); 		


		IF v_sync_count = 0 THEN
			-- 1600 - Sales Report Grp
      		gm_pkg_device_system_sync_rpt.gm_fch_all_system_tosync(p_comp_id,1600,v_system_out_dtl); 
     	ELSE
     	 	-- 1600 - Sales Report Grp SYSTEM-- need to change param and name of v_system_out_dtl
      		gm_pkg_device_system_sync_rpt.gm_fch_system_update_tosync(p_comp_id,1600,v_device_last_sync_date,v_system_out_dtl);
      	END IF;
	   
 --Get the HeaderDetails
  BEGIN
  SELECT replace(JSON_OBJECT( 'pagesize'    VALUE TO_CHAR(COUNT(1)),
					  'totalsize'   VALUE TO_CHAR(COUNT(1)),
					  'pageno'      VALUE TO_CHAR(1),
					  'totalpages'  VALUE TO_CHAR(1),
					  'token'       VALUE TO_CHAR(p_token),
					  'langid'      VALUE TO_CHAR(p_langid),
					  'reftype'     VALUE TO_CHAR(103108), --System Ref Type
					  'svrcnt'      VALUE '', 'lastrec' VALUE '' ),'null','""')
	INTO v_system_header
  		FROM t207s_set_master_sync t207s
  		WHERE t207s.c1900_company_id     		=  p_comp_id
  		AND   t207s.c901_set_group_type       =  1600 -- 1600 - Sales Report Grp
  		AND t207s.c207s_void_fl  IS NULL;

  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    v_system_header := NULL;
  END;
		
  --Get the System string and form the JSON object as an array
  keys_string := v_system_out_dtl;

  v_json_out  := JSON_OBJECT_T.parse(v_system_header);
  
  -- Check the JSON Array is not null and put the JSON Array value in JSON_OBJECT_T
  IF keys_string IS NOT NULL THEN
    v_json_out.put('listGmSystemVO',JSON_ARRAY_T(keys_string));
   ELSE
    v_json_out.put('listGmSystemVO',JSON_ARRAY_T());
  END IF;
  --Change the JSON OBJECT to Clob data type
  p_out_system := v_json_out.to_clob;
END gm_fch_systems_prodcat;	

	/**********************************************************************
 	* Description : Procedure used to fetch the system attribute sync detail
 	* Author      : Ramachandiran T.S
	************************************************************************/

PROCEDURE gm_fch_systems_attribute  (
		p_token  		IN 		t101a_user_token.c101a_token_id%TYPE,
		p_langid		IN		NUMBER,
		p_page_no		IN		NUMBER,
	    p_uuid		    IN      t9150_device_info.c9150_device_uuid%TYPE,
	    p_comp_id	    IN	    t1900_company.c1900_company_id%TYPE,
		p_out_system	OUT		CLOB
	)
	AS
	
	v_device_infoid    			t9150_device_info.c9150_device_info_id%TYPE;
    v_sync_count       			NUMBER;
    v_system_header    			CLOB;
    v_system_out_dtl       		CLOB;
    v_json_out         			JSON_OBJECT_T;
    v_device_last_sync_date 	t9151_device_sync_dtl.c9151_last_sync_date%TYPE;
    keys_string        			CLOB;
    
    BEGIN
	    	--get the device info id based on user token
		v_device_infoid := get_device_id(p_token, p_uuid);
		--get the device update details based on sync type
		--4000416 - System attribute Device Sync Type
		gm_pkg_device_sync_update_dtls.gm_fch_device_updates(v_device_infoid,p_langid,4000416,v_device_last_sync_date,v_sync_count); 

		IF v_sync_count = 0 THEN
		 	-- 1600 - Sales Report Grp
     		gm_pkg_device_system_sync_rpt.gm_fch_all_system_attribute_tosync(p_comp_id,1600,v_system_out_dtl);
    	ELSE
    		-- 1600 - Sales Report Grp
     		gm_pkg_device_system_sync_rpt.gm_fch_system_attribute_update_tosync(p_comp_id,1600,v_device_last_sync_date,v_system_out_dtl);
    	END IF;
    
    --Get the HeaderDetails
  BEGIN
  SELECT replace(JSON_OBJECT( 'pagesize'    VALUE TO_CHAR(COUNT(1)),
					  'totalsize'   VALUE TO_CHAR(COUNT(1)),
					  'pageno'      VALUE TO_CHAR(1),
					  'totalpages'  VALUE TO_CHAR(1),
					  'token'       VALUE TO_CHAR(p_token),
					  'langid'      VALUE TO_CHAR(p_langid),
					  'reftype'     VALUE TO_CHAR(4000416), --System Attribute Ref Type
					  'svrcnt'      VALUE '', 'lastrec' VALUE '' ),'null','""')
	INTO v_system_header
  		FROM t207s_set_attribute_sync t207sa, t207s_set_master_sync t207s
  		WHERE t207s.c207_set_id 			= t207sa.c207_set_id
  		AND t207s.c1900_company_id     	=  p_comp_id
  		AND t207sa.c901_set_group_type    =  1600 -- 1600 - Sales Report Grp
  		AND t207s.c207s_void_fl  IS NULL
  		AND t207sa.c207s_void_fl IS NULL; 
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    v_system_header := NULL;
  END;
		
  --Get the System attribute string and form the JSON object as an array
  keys_string := v_system_out_dtl;
  
  keys_string   := REPLACE(keys_string , '[[' , '[');
  keys_string   := REPLACE(keys_string , '],[' , ',');
  keys_string   := REPLACE(keys_string , ']]' , ']');
    
  v_system_header := REPLACE(v_system_header , 'null' , '""');  
  v_json_out  := JSON_OBJECT_T.parse(v_system_header);
  
  -- Check the JSON Array is not null and put the JSON Array value in JSON_OBJECT_T
  IF keys_string IS NOT NULL THEN
    v_json_out.put('listGmSystemAttributeVO',JSON_ARRAY_T(keys_string));
    ELSE
    v_json_out.put('listGmSystemAttributeVO',JSON_ARRAY_T());
  END IF;
  --Change the JSON OBJECT to Clob data type
  p_out_system := v_json_out.to_clob;
  END gm_fch_systems_attribute;	
END gm_pkg_device_system_master_sync_rpt;
/