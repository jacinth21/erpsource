/* Formatted on 2019/11/19 15:32 (Formatter Plus v4.8.0) */
-- @"C:\database\Packages\Sync\Group\gm_pkg_device_system_sync_rpt.bdy"

CREATE OR REPLACE PACKAGE BODY gm_pkg_device_system_sync_rpt
IS
   /*****************************************************************
 	* Description : Procedure to fetch all system master data to sync
 	* Author      : Ramachandiran T.S
	*******************************************************************/

PROCEDURE gm_fch_all_system_tosync(
	
	  p_comp_id	     	IN	     t207s_set_master_sync.c1900_company_id%TYPE, 
	  p_system_type	 	IN       t207s_set_master_sync.c901_set_group_type%TYPE, 
	  p_out_system_dtl  OUT      CLOB

	)
	AS
	
	v_system_out_dtl CLOB;
			
BEGIN		--Get the all system details based on company
  BEGIN  
	SELECT replace(JSON_ARRAYAGG (t207s.c207s_set_master_sync_data RETURNING CLOB),'null','""')
		INTO v_system_out_dtl
  		FROM t207s_set_master_sync t207s 
  		WHERE t207s.c901_set_group_type       	= p_system_type
    	AND t207s.c1900_company_id     			= p_comp_id
    	AND t207s.c207s_void_fl  IS NULL;
 
  	p_out_system_dtl    := v_system_out_dtl;
  	
  	EXCEPTION
  	WHEN NO_DATA_FOUND THEN
    v_system_out_dtl := NULL;
  END;
		
 END gm_fch_all_system_tosync;
 
 
 /*****************************************************************
 	* Description : Procedure to fetch all system attribute data to sync
 	* Author      : Ramachandiran T.S
	*******************************************************************/

PROCEDURE gm_fch_all_system_attribute_tosync(
	  
	  p_comp_id	     	IN	     t207s_set_master_sync.c1900_company_id%TYPE, 
	  p_system_type	 	IN       t207s_set_attribute_sync.c901_set_group_type%TYPE, 
	  p_out_system_dtl  OUT      CLOB

	)
	AS
	
	v_system_out_dtl CLOB;
			
BEGIN		--Get the all system attribute details based on company
  BEGIN  
	SELECT replace(JSON_ARRAYAGG (t207sa.c207s_set_attribute_sync_data RETURNING CLOB),'null','""')
		INTO v_system_out_dtl
  		FROM t207s_set_master_sync t207s, t207s_set_attribute_sync t207sa
  		WHERE t207sa.c207_set_id 			= t207s.c207_set_id
    	AND t207sa.c901_set_group_type       = p_system_type
    	AND t207s.c1900_company_id     		= p_comp_id
    	AND t207s.c207s_void_fl  IS NULL
    	AND t207sa.c207s_void_fl IS NULL;
  	
  	p_out_system_dtl    := v_system_out_dtl;
  
  	EXCEPTION
  	WHEN NO_DATA_FOUND THEN
    v_system_out_dtl := NULL;
  END;
		
 END gm_fch_all_system_attribute_tosync;
 
 
  	/*********************************************************************
 	* Description : Procedure to fetch the system update's sync data detail
 	* Author      : Ramachandiran T.S
	***********************************************************************/
 
 
 PROCEDURE gm_fch_system_update_tosync(
 	
 	p_comp_id	     			IN	    t207s_set_master_sync.c1900_company_id%TYPE,
 	p_system_type	 			IN      t207s_set_master_sync.c901_set_group_type%TYPE,
 	p_device_last_sync_date 	IN   	t9151_device_sync_dtl.c9151_last_sync_date%TYPE,
 	p_out_system_dtl 			OUT     CLOB
 	)
	AS
	
	v_system_out_dtl CLOB;

BEGIN	
 BEGIN
	 
	 -- Get the latest update details from the device's last sync time.
   SELECT replace(JSON_ARRAYAGG (t207s.c207s_set_master_sync_data RETURNING CLOB),'null','""')
		INTO v_system_out_dtl
  		FROM t207s_set_master_sync t207s 
  		WHERE t207s.c207s_last_updated_date >= p_device_last_sync_date
  		AND t207s.c901_set_group_type      	= p_system_type
    	AND t207s.c1900_company_id     		= p_comp_id
    	AND t207s.c207s_void_fl  IS NULL;
  	
    	
  	p_out_system_dtl            := v_system_out_dtl;
  	  
  	EXCEPTION
  	WHEN NO_DATA_FOUND THEN
    v_system_out_dtl := NULL;
    END;
      
END gm_fch_system_update_tosync;



	/*********************************************************************
 	* Description : Procedure to fetch the system attribute update's sync data detail
 	* Author      : Ramachandiran T.S
	***********************************************************************/
 
 
 PROCEDURE gm_fch_system_attribute_update_tosync(
 	
 	p_comp_id	     			IN	    t207s_set_master_sync.c1900_company_id%TYPE,
 	p_system_type	 			IN      t207s_set_attribute_sync.c901_set_group_type%TYPE,
 	p_device_last_sync_date 	IN   	t9151_device_sync_dtl.c9151_last_sync_date%TYPE,
 	p_out_system_dtl 			OUT     CLOB
 	)
	AS
	
	v_system_out_dtl CLOB;
BEGIN	
 BEGIN	
	 
	-- Get the latest update details from the device's last sync time.
	 SELECT replace(JSON_ARRAYAGG (t207sa.c207s_set_attribute_sync_data RETURNING CLOB),'null','""')
		INTO v_system_out_dtl
  		FROM t207s_set_attribute_sync t207sa, t207s_set_master_sync t207s
  		WHERE t207s.c207_set_id 			= t207sa.c207_set_id
 	 	AND t207s.c1900_company_id     		=  p_comp_id
  		AND t207sa.c207s_last_updated_date 	>= p_device_last_sync_date
  		AND t207sa.c901_set_group_type    	=  1600 -- 1600 - Sales Report Grp
  		AND t207s.c207s_void_fl  IS NULL
  		AND t207sa.c207s_void_fl IS NULL; 
  		
  	p_out_system_dtl            := v_system_out_dtl;
  	 
  	EXCEPTION
  	WHEN NO_DATA_FOUND THEN
    v_system_out_dtl := NULL;
   END;
   
END gm_fch_system_attribute_update_tosync;

END gm_pkg_device_system_sync_rpt;
/