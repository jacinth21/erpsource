-- @"C:\Database\Packages\Sync\Device\System\gm_pkg_device_system_sync_to_master.bdy";
CREATE OR REPLACE PACKAGE BODY GM_PKG_DEVICE_SYSTEM_SYNC_TO_MASTER
IS
    /******************************************************************
    Description : This procedure is used for forming JSON data from temporary table into System Master Sync Table
    Author      : jgurunathan
    ****************************************************************/
PROCEDURE GM_SAV_SYSTEM_MASTER_JSON_MAIN
AS
    CURSOR set_master_sync_data
    IS
        --forming JSON data from temp table
         SELECT C207_SYSTEM_ID SYSTEM_ID, C1900_COMPANY_ID COMPID, C901_LANGUAGE_ID LANGID
          , C901_SYSTEM_GRP_TYPE SYSTEMGRP, (JSON_OBJECT ('sid' VALUE C207_SYSTEM_ID, 'snm' VALUE C207_SYSTEM_NAME,
            'sdc' VALUE C207_SYSTEM_DESC, 'sdd' VALUE C207_SYSTEM_DTLS_DESC, 'sts' VALUE TO_CHAR (C901_SYSTEM_STATUS),
            'vfl' VALUE DECODE (C901_PUBLISHED_STATUS, '108660', C207_IPAD_VOID_FL, 'Y'))) MASTER_SYNC_DATA
           FROM MY_TEMP_SYSTEM_SYNC_UPDATE;
BEGIN
    FOR sync_data IN set_master_sync_data
    LOOP
        --save to master table
        gm_pkg_device_system_sync_to_master.GM_SAV_SET_MASTER_JSON (sync_data.SYSTEM_ID, sync_data.COMPID,
        sync_data.LANGID, sync_data.MASTER_SYNC_DATA, sync_data.SYSTEMGRP, '30301', --Manual Load
        103122) ;--New
    END LOOP;
    
    ---update iPad Synced Flag
    gm_pkg_device_system_sync_to_master.GM_UPD_IPAD_SYNC_FLAG_COMPLETED;
    
    --Delete temp table
     DELETE FROM MY_TEMP_SYSTEM_SYNC_UPDATE;
     
END GM_SAV_SYSTEM_MASTER_JSON_MAIN;

/************************************************************************************************
Description : This procedure is used to insert / update into System Master Table
Author      : jgurunathan
*************************************************************************************************/
PROCEDURE GM_SAV_SET_MASTER_JSON (
        p_set_id          IN T207S_SET_MASTER_SYNC.C207_SET_ID%TYPE,
        p_company_id      IN T207S_SET_MASTER_SYNC.C1900_COMPANY_ID%TYPE,
        p_language_id     IN T207S_SET_MASTER_SYNC.C901_LANGUAGE_ID%TYPE,
        p_set_master_json IN T207S_SET_MASTER_SYNC.C207S_SET_MASTER_SYNC_DATA%TYPE,
        p_set_grp_type    IN T207S_SET_MASTER_SYNC.C901_SET_GROUP_TYPE%TYPE,
        p_user_id         IN T207S_SET_MASTER_SYNC.C207S_LAST_UPDATED_BY%TYPE,
        p_sync_action     IN T207S_SET_MASTER_SYNC.C901_SYNC_ACTION%TYPE)
AS
BEGIN
	--
     UPDATE T207S_SET_MASTER_SYNC
    SET C901_LANGUAGE_ID                 = p_language_id, C207S_SET_MASTER_SYNC_DATA = p_set_master_json, C901_SET_GROUP_TYPE =
        p_set_grp_type, C901_SYNC_ACTION = 103123, --Updated
        C207S_LAST_UPDATED_BY            = p_user_id, C207S_LAST_UPDATED_DATE = CURRENT_DATE
      WHERE C207_SET_ID                  = p_set_id
        AND C207S_VOID_FL               IS NULL
        AND C1900_COMPANY_ID             = p_company_id;
    --    
    IF (SQL%ROWCOUNT                     = 0) THEN
         INSERT
           INTO T207S_SET_MASTER_SYNC
            (
                C207_SET_ID, C1900_COMPANY_ID, C901_LANGUAGE_ID
              , C207S_SET_MASTER_SYNC_DATA, C901_SET_GROUP_TYPE, C901_SYNC_ACTION
              , C207S_VOID_FL, C207S_LAST_UPDATED_BY, C207S_LAST_UPDATED_DATE
            )
            VALUES
            (
                p_set_id, p_company_id, p_language_id
              , p_set_master_json, p_set_grp_type, p_sync_action
              , NULL, p_user_id, CURRENT_DATE
            ) ;
    END IF;
    --
END GM_SAV_SET_MASTER_JSON;
/******************************************************************
Description : This procedure is used to update ipad sync flag as 'Y'
Author      : jgurunathan
*******************************************************************/
PROCEDURE GM_UPD_IPAD_SYNC_FLAG_COMPLETED
AS
BEGIN
	-- to update master 
     UPDATE T207_SET_MASTER
    SET C207_IPAD_SYNCED_FLAG     = 'Y', C207_IPAD_SYNCED_DATE = CURRENT_DATE
      WHERE C207_IPAD_SYNCED_FLAG = 'P';
      
    -- to update meta data  
     UPDATE T2000_MASTER_METADATA
    SET C2000_IPAD_SYNCED_FLAG     = 'Y', C2000_IPAD_SYNCED_DATE = CURRENT_DATE
      WHERE C2000_IPAD_SYNCED_FLAG = 'P'
        AND C2000_REF_ID          IN
        (
             SELECT C207_SET_ID
               FROM T207_SET_MASTER
              WHERE C901_SET_GRP_TYPE = 1600--System
           GROUP BY C207_SET_ID
        ) ;
        
     -- to update the system company   
     UPDATE T2080_SET_COMPANY_MAPPING
    SET C2080_IPAD_SYNCED_FLAG     = 'Y', C2080_IPAD_SYNCED_DATE = CURRENT_DATE
      WHERE C2080_IPAD_SYNCED_FLAG = 'P'
        AND C207_SET_ID           IN
        (
             SELECT C207_SET_ID
               FROM T207_SET_MASTER
              WHERE C901_SET_GRP_TYPE = 1600--System
           GROUP BY C207_SET_ID
        ) ;
        
     -- to update set link  
     UPDATE T207A_SET_LINK
    SET C207A_IPAD_SYNCED_FLAG     = 'Y', C207A_IPAD_SYNCED_DATE = CURRENT_DATE
      WHERE C207A_IPAD_SYNCED_FLAG = 'P';
    --
END GM_UPD_IPAD_SYNC_FLAG_COMPLETED;

/************************************************************************************************
Description :This procedure is used for forming JSON data from temporary table into System  Attr Sync Table
Author      : ppandiyan
*************************************************************************************************/
PROCEDURE GM_SAV_SYSTEM_ATTRIBUTE_JSON_MAIN
AS
    CURSOR set_master_attr_data
    IS
        --getting comma separated all attribute values for single set
         SELECT T207C.C207_SET_ID SYSTEMID, JSON_ARRAYAGG (JSON_OBJECT ('sid' VALUE T207C.C207_SET_ID, 'att' VALUE
            TO_CHAR (T207C.C901_ATTRIBUTE_TYPE), 'atv' VALUE T207C.C207C_ATTRIBUTE_VALUE, 'vfl' VALUE
            T207C.C207C_VOID_FL) RETURNING CLOB) ATTR_JSON_DATA
           FROM T207C_SET_ATTRIBUTE T207C, T207_SET_MASTER T207
          WHERE T207C.C207_SET_ID      = T207.C207_SET_ID
            AND T207.C901_SET_GRP_TYPE = 1600--System
            AND T207C.C207_SET_ID     IN
            (
                 SELECT C207_SET_ID
                   FROM T207C_SET_ATTRIBUTE
                  WHERE C207C_IPAD_SYNCED_FLAG = 'P'
               GROUP BY C207_SET_ID
            )
   GROUP BY T207C.C207_SET_ID;
BEGIN
    --looping the cursor to save attribute data
    FOR attr_data IN set_master_attr_data
    LOOP
        gm_pkg_device_system_sync_to_master.GM_SAV_SET_ATTRIBUTE_JSON (attr_data.SYSTEMID, attr_data.ATTR_JSON_DATA,
        1600, --System
        '30301') ;--Manual Load
    END LOOP;
    
    -- update iPad Sync Flag in set attribute table
     UPDATE T207C_SET_ATTRIBUTE
    SET C207C_IPAD_SYNCED_FLAG     = 'Y', C207C_IPAD_SYNCED_DATE = NULL
      WHERE C207C_IPAD_SYNCED_FLAG = 'P'
        AND C207_SET_ID           IN
        (
             SELECT T207.C207_SET_ID
               FROM T207_SET_MASTER T207
              WHERE T207.C901_SET_GRP_TYPE = 1600--System
           GROUP BY T207.C207_SET_ID
        ) ;
        
END GM_SAV_SYSTEM_ATTRIBUTE_JSON_MAIN;

/************************************************************************************************
Description : This procedure is used to insert / update into System Attribute Table
Author      : ppandiyan
*************************************************************************************************/
PROCEDURE GM_SAV_SET_ATTRIBUTE_JSON (
        p_set_id        IN T207S_SET_ATTRIBUTE_SYNC.C207_SET_ID%TYPE,
        p_set_attr_json IN T207S_SET_ATTRIBUTE_SYNC.C207S_SET_ATTRIBUTE_SYNC_DATA%TYPE,
        p_set_grp_type  IN T207S_SET_ATTRIBUTE_SYNC.C901_SET_GROUP_TYPE%TYPE,
        p_user_id       IN T207S_SET_ATTRIBUTE_SYNC.C207S_LAST_UPDATED_BY%TYPE)
AS
BEGIN
	--
     UPDATE T207S_SET_ATTRIBUTE_SYNC
    SET C207S_SET_ATTRIBUTE_SYNC_DATA      = p_set_attr_json, C901_SET_GROUP_TYPE = p_set_grp_type, C207S_LAST_UPDATED_BY =
        p_user_id, C207S_LAST_UPDATED_DATE = CURRENT_DATE
      WHERE C207_SET_ID                    = p_set_id
        AND C207S_VOID_FL                 IS NULL;
    --    
    IF (SQL%ROWCOUNT                       = 0) THEN
         INSERT
           INTO T207S_SET_ATTRIBUTE_SYNC
            (
                C207_SET_ID, C207S_SET_ATTRIBUTE_SYNC_DATA, C901_SET_GROUP_TYPE
              , C207S_VOID_FL, C207S_LAST_UPDATED_BY, C207S_LAST_UPDATED_DATE
            )
            VALUES
            (
                p_set_id, p_set_attr_json, p_set_grp_type
              , NULL, p_user_id, CURRENT_DATE
            ) ;
    END IF;
    
END GM_SAV_SET_ATTRIBUTE_JSON;
END GM_PKG_DEVICE_SYSTEM_SYNC_TO_MASTER;
/