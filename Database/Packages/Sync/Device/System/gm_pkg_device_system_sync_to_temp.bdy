-- @"C:\Database\Packages\Sync\Device\System\gm_pkg_device_system_sync_to_temp.bdy";
CREATE OR REPLACE PACKAGE BODY GM_PKG_DEVICE_SYSTEM_SYNC_TO_TEMP
IS
    /**********************************************************************************************************
    Description : This procedure is used to inserting system related sync data into temp table
    Author      : jgurunathan
    ***********************************************************************************************************/
PROCEDURE GM_SAV_SYSTEM_MASTER_SYNC_MAIN
AS
    v_temp_count     NUMBER;
    v_ref_type       NUMBER;
    v_ref_group_type NUMBER;
    v_ipad_synced_flag t207_set_master.C207_IPAD_SYNCED_FLAG%TYPE;
    --
    v_start_time DATE;
BEGIN
    --103092 system ref type, 1600 system group type
    v_ref_type         := 103092;
    v_ref_group_type   := 1600;
    v_ipad_synced_flag := 'P';
    v_start_time := CURRENT_DATE;
    --
    BEGIN
	--    
    dbms_output.put_line ('Start time ==> ' || TO_CHAR (sysdate, 'MM/dd/yyyy HH:MI AM')) ;
    
    /* 1. Update ipad Sync status to process */
    dbms_output.put_line ('******** before start GM_UPD_MASTER_SYNC_FLAG>> '|| TO_CHAR (sysdate, 'MM/dd/yyyy HH:MI:SS AM')) ;
    GM_PKG_DEVICE_COMMON_SYNC.GM_UPD_MASTER_SYNC_FLAG (v_ref_group_type, 'P', v_ref_type, '30301') ;
    COMMIT;
    
    dbms_output.put_line ('******** after start GM_UPD_MASTER_SYNC_FLAG >> '|| TO_CHAR (sysdate, 'MM/dd/yyyy HH:MI:SS AM')) ;
    
    
    /* 2. System Master data strore to temp  */
    dbms_output.put_line ('******** before start GM_SAV_SYSTEM_MASTER_TEMP>> '|| TO_CHAR (sysdate, 'MM/dd/yyyy HH:MI:SS AM')) ;
    gm_pkg_device_system_sync_to_temp.GM_SAV_SYSTEM_MASTER_TEMP (v_ref_type, v_ref_group_type, v_ipad_synced_flag) ;
    COMMIT;
    
    dbms_output.put_line ('******** after start GM_SAV_SYSTEM_MASTER_TEMP >> '|| TO_CHAR (sysdate,'MM/dd/yyyy HH:MI:SS AM')) ;
    
    
    /* 3. System Company release data strore to temp  */
    dbms_output.put_line ('******** before start GM_SAV_SYSTEM_RELEASE_TEMP>> '|| TO_CHAR (sysdate, 'MM/dd/yyyy HH:MI:SS AM')) ;
    gm_pkg_device_system_sync_to_temp.GM_SAV_SYSTEM_RELEASE_TEMP (v_ref_type, v_ref_group_type, v_ipad_synced_flag) ;
    COMMIT;
    
    dbms_output.put_line ('******** after start GM_SAV_SYSTEM_RELEASE_TEMP >> '|| TO_CHAR (sysdate,'MM/dd/yyyy HH:MI:SS AM')) ;
    
    
    /* 4. System Language data strore to temp  */
    dbms_output.put_line ('******** before start GM_SAV_SYSTEM_LANGUAGE_TEMP>> '|| TO_CHAR (sysdate, 'MM/dd/yyyy HH:MI:SS AM')) ;
    gm_pkg_device_system_sync_to_temp.GM_SAV_SYSTEM_LANGUAGE_TEMP (v_ref_type, v_ref_group_type, v_ipad_synced_flag) ;
    COMMIT;
    
    dbms_output.put_line ('******** after start GM_SAV_SYSTEM_LANGUAGE_TEMP >> '|| TO_CHAR (sysdate, 'MM/dd/yyyy HH:MI:SS AM')) ;
    
    
    
    /* 5. System temp to JSON convert and store to master table  */
     SELECT COUNT (1)
       INTO v_temp_count
       FROM MY_TEMP_SYSTEM_SYNC_UPDATE;
       
    --below code to save in master tables
    IF v_temp_count > 0 THEN
        dbms_output.put_line ('******** before start GM_SAV_SYSTEM_MASTER_JSON_MAIN>> '|| TO_CHAR (sysdate, 'MM/dd/yyyy HH:MI:SS AM')) ;
        GM_PKG_DEVICE_SYSTEM_SYNC_TO_MASTER.GM_SAV_SYSTEM_MASTER_JSON_MAIN;
        COMMIT;
        --
        dbms_output.put_line ('******** after start GM_SAV_SYSTEM_MASTER_JSON_MAIN >> '|| TO_CHAR (sysdate, 'MM/dd/yyyy HH:MI:SS AM')) ;
    END IF;
    
    dbms_output.put_line ('******** before start GM_SAV_SYSTEM_ATTRIBUTE_JSON_MAIN>> '|| TO_CHAR (sysdate, 'MM/dd/yyyy HH:MI:SS AM')) ;
    
    
    /* 6. System attribute details store to master table  */
    GM_PKG_DEVICE_SYSTEM_SYNC_TO_MASTER.GM_SAV_SYSTEM_ATTRIBUTE_JSON_MAIN;
    COMMIT;
    dbms_output.put_line ('******** after start GM_SAV_SYSTEM_ATTRIBUTE_JSON_MAIN >> '|| TO_CHAR (sysdate, 'MM/dd/yyyy HH:MI:SS AM')) ;
    --
    EXCEPTION WHEN OTHERS
    THEN
      dbms_output.put_line (' Error GM_SAV_SYSTEM_MASTER_SYNC_MAIN @@@@@@@@@ ****** ' || SQLERRM||chr(10)||dbms_utility.format_error_backtrace);
      -- 7 to notify the error 
      gm_pkg_cm_job.gm_cm_notify_load_info (v_start_time, CURRENT_DATE, 'E', SQLERRM||chr(10)||dbms_utility.format_error_backtrace, 'gm_pkg_device_system_sync_to_temp.gm_sav_system_master_sync_main') ;
      --
    END;
    --
END GM_SAV_SYSTEM_MASTER_SYNC_MAIN;


/**********************************************************************************************************
Description : This procedure is used to inserting system master table sync data into temporary table
Author      : jgurunathan
***********************************************************************************************************/
PROCEDURE GM_SAV_SYSTEM_MASTER_TEMP (
        p_ref_type       IN T2000_MASTER_METADATA.C901_REF_TYPE%TYPE,
        p_set_grp_type   IN T207_SET_MASTER.C901_SET_GRP_TYPE%TYPE,
        p_ipad_sync_flag IN T207_SET_MASTER.C207_IPAD_SYNCED_FLAG%TYPE)
AS
BEGIN
     INSERT
       INTO MY_TEMP_SYSTEM_SYNC_UPDATE
        (
            C207_SYSTEM_ID, C1900_COMPANY_ID, C901_LANGUAGE_ID
          , C207_SYSTEM_NAME, C207_SYSTEM_DESC, C207_SYSTEM_DTLS_DESC
          , C901_SYSTEM_STATUS, C901_SYSTEM_GRP_TYPE, C901_PUBLISHED_STATUS
          , C207_IPAD_VOID_FL
        )
     SELECT T207.C207_SET_ID, T2080.C1900_COMPANY_ID, T2000.C901_LANGUAGE_ID
      , T2000.C2000_REF_NAME, T2000.C2000_REF_DESC, T2000.C2000_REF_DTL_DESC
      , T207.C901_STATUS_ID, T207.C901_SET_GRP_TYPE, T207.C901_PUBLISHED_STATUS
      , DECODE (T2080.C901_IPAD_RELEASED_STATUS, '105361', T2080.C2080_VOID_FL, 'Y')
       FROM T2000_MASTER_METADATA T2000, T2080_SET_COMPANY_MAPPING T2080, T1900_COMPANY T1900
      , T207_SET_MASTER T207
      WHERE T1900.C1900_COMPANY_ID = T2080.C1900_COMPANY_ID
        AND T2080.C207_SET_ID      = T2000.C2000_REF_ID
        AND T207.C207_SET_ID       = T2080.C207_SET_ID
        AND T207.C207_SET_ID       = T2000.C2000_REF_ID
        AND T2000.C901_LANGUAGE_ID = gm_pkg_device_common_sync.get_ipad_lang_id (p_ref_type, T2080.C207_SET_ID,
        T1900.C901_LANGUAGE_ID)
        AND T2000.C901_REF_TYPE        = p_ref_type
        AND T207.C901_SET_GRP_TYPE     = p_set_grp_type
        AND T207.C207_IPAD_SYNCED_FLAG = p_ipad_sync_flag
        AND T1900.C1900_VOID_FL       IS NULL;
        
END GM_SAV_SYSTEM_MASTER_TEMP;

/**********************************************************************************************************
Description : This procedure is used to inserting system release table sync data into temporary table
Author      : jgurunathan
***********************************************************************************************************/

PROCEDURE GM_SAV_SYSTEM_RELEASE_TEMP (
        p_ref_type       IN T2000_MASTER_METADATA.C901_REF_TYPE%TYPE,
        p_set_grp_type   IN T207_SET_MASTER.C901_SET_GRP_TYPE%TYPE,
        p_ipad_sync_flag IN T207_SET_MASTER.C207_IPAD_SYNCED_FLAG%TYPE)
AS
BEGIN
     INSERT
       INTO MY_TEMP_SYSTEM_SYNC_UPDATE
        (
            C207_SYSTEM_ID, C1900_COMPANY_ID, C901_LANGUAGE_ID
          , C207_SYSTEM_NAME, C207_SYSTEM_DESC, C207_SYSTEM_DTLS_DESC
          , C901_SYSTEM_STATUS, C901_SYSTEM_GRP_TYPE, C901_PUBLISHED_STATUS
          , C207_IPAD_VOID_FL
        )
     SELECT T207.C207_SET_ID, T2080.C1900_COMPANY_ID, T2000.C901_LANGUAGE_ID
      , T2000.C2000_REF_NAME, T2000.C2000_REF_DESC, T2000.C2000_REF_DTL_DESC
      , T207.C901_STATUS_ID, T207.C901_SET_GRP_TYPE, T207.C901_PUBLISHED_STATUS
      , DECODE (T2080.C901_IPAD_RELEASED_STATUS, '105361', T2080.C2080_VOID_FL, 'Y')
       FROM T2000_MASTER_METADATA T2000, T2080_SET_COMPANY_MAPPING T2080, T1900_COMPANY T1900
      , T207_SET_MASTER T207
      WHERE T1900.C1900_COMPANY_ID = T2080.C1900_COMPANY_ID
        AND T2080.C207_SET_ID      = T2000.C2000_REF_ID
        AND T207.C207_SET_ID       = T2080.C207_SET_ID
        AND T207.C207_SET_ID       = T2000.C2000_REF_ID
        AND T2000.C901_LANGUAGE_ID = gm_pkg_device_common_sync.get_ipad_lang_id (p_ref_type, T2080.C207_SET_ID,
        T1900.C901_LANGUAGE_ID)
        AND T2000.C901_REF_TYPE          = p_ref_type
        AND T207.C901_SET_GRP_TYPE       = p_set_grp_type
        AND T207.C901_PUBLISHED_STATUS   = 108660 -- published
        AND T2080.C2080_IPAD_SYNCED_FLAG = p_ipad_sync_flag
        -- to avoid duplicat record
        AND T2080.C207_SET_ID NOT IN
        (
             SELECT C207_SYSTEM_ID FROM MY_TEMP_SYSTEM_SYNC_UPDATE
        )
        AND T1900.C1900_VOID_FL IS NULL;
        --
END GM_SAV_SYSTEM_RELEASE_TEMP;

/**********************************************************************************************************
Description : This procedure is used to inserting system language table sync data into temporary table
Author      : jgurunathan
***********************************************************************************************************/

PROCEDURE GM_SAV_SYSTEM_LANGUAGE_TEMP (
        p_ref_type       IN T2000_MASTER_METADATA.C901_REF_TYPE%TYPE,
        p_set_grp_type   IN T207_SET_MASTER.C901_SET_GRP_TYPE%TYPE,
        p_ipad_sync_flag IN T207_SET_MASTER.C207_IPAD_SYNCED_FLAG%TYPE)
AS
BEGIN
     INSERT
       INTO MY_TEMP_SYSTEM_SYNC_UPDATE
        (
            C207_SYSTEM_ID, C1900_COMPANY_ID, C901_LANGUAGE_ID
          , C207_SYSTEM_NAME, C207_SYSTEM_DESC, C207_SYSTEM_DTLS_DESC
          , C901_SYSTEM_STATUS, C901_SYSTEM_GRP_TYPE, C901_PUBLISHED_STATUS
          , C207_IPAD_VOID_FL
        )
     SELECT T207.C207_SET_ID, T2080.C1900_COMPANY_ID, T2000.C901_LANGUAGE_ID
      , T2000.C2000_REF_NAME, T2000.C2000_REF_DESC, T2000.C2000_REF_DTL_DESC
      , T207.C901_STATUS_ID, T207.C901_SET_GRP_TYPE, T207.C901_PUBLISHED_STATUS
      , DECODE (T2080.C901_IPAD_RELEASED_STATUS, '105361', T2080.C2080_VOID_FL, 'Y')
       FROM T2000_MASTER_METADATA T2000, T2080_SET_COMPANY_MAPPING T2080, T1900_COMPANY T1900
      , T207_SET_MASTER T207
      WHERE T1900.C1900_COMPANY_ID = T2080.C1900_COMPANY_ID
        AND T2080.C207_SET_ID      = T2000.C2000_REF_ID
        AND T207.C207_SET_ID       = T2080.C207_SET_ID
        AND T207.C207_SET_ID       = T2000.C2000_REF_ID
        AND T2000.C901_LANGUAGE_ID = gm_pkg_device_common_sync.get_ipad_lang_id (p_ref_type, T2080.C207_SET_ID,
        T1900.C901_LANGUAGE_ID)
        AND T2000.C901_REF_TYPE             = p_ref_type
        AND T207.C901_SET_GRP_TYPE          = p_set_grp_type
        AND T207.C901_PUBLISHED_STATUS      = 108660 -- published
        AND T2080.C901_IPAD_RELEASED_STATUS = 105361 --<released value>
        AND T2000.C2000_IPAD_SYNCED_FLAG    = p_ipad_sync_flag
        -- to avoid duplicat record
        AND (T2080.C207_SET_ID, T2080.C1900_COMPANY_ID) NOT IN
        (
             SELECT C207_SYSTEM_ID, C1900_COMPANY_ID FROM MY_TEMP_SYSTEM_SYNC_UPDATE
        )
        AND T1900.C1900_VOID_FL IS NULL;
END GM_SAV_SYSTEM_LANGUAGE_TEMP;

END GM_PKG_DEVICE_SYSTEM_SYNC_TO_TEMP;
/