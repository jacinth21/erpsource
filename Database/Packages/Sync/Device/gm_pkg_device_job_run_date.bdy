/* Formatted on 2011/10/26 17:13 (Formatter Plus v4.8.0) */
--@"C:\Database\Packages\Sync\Device\gm_pkg_device_job_run_date.bdy";
-- No need of Contex since we are not using companyId
CREATE OR REPLACE
PACKAGE BODY gm_pkg_device_job_run_date
IS
  --
  /******************************************************************
  Description : This procedure is used to fetch the last sync date and update start date as current date of job
  Author      : mselvamani
  ****************************************************************/
PROCEDURE gm_update_job_details(
    p_job_id  IN t9300_job.C9300_JOB_ID%TYPE,
    p_user_id IN t9300_job.C9300_LAST_UPDATED_BY%TYPE,
    p_out OUT DATE )
AS
  v_last_sync_date DATE;
BEGIN
  --
  BEGIN
    SELECT C9300_Start_Date
    INTO v_last_sync_date
    FROM T9300_Job
    WHERE C9300_JOB_ID     = p_job_id
    AND C9300_Void_Fl     IS NULL
    AND C9300_Inactive_Fl IS NULL
    FOR UPDATE;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    GM_RAISE_APPLICATION_ERROR('-20999','','Job Start Date is not available for TagSync Job.');
  END;
  --
  UPDATE T9300_Job
  SET C9300_Start_Date     = CURRENT_DATE,
    C9300_LAST_UPDATED_BY  = p_user_id,
    C9300_LAST_UPDATED_DATE= CURRENT_DATE
  WHERE C9300_JOB_ID       = p_job_id
  AND C9300_Void_Fl       IS NULL
  AND C9300_Inactive_Fl   IS NULL;
  p_out                   := v_last_sync_date;
END gm_update_job_details;
END gm_pkg_device_job_run_date;
/