/* Formatted on 2010/09/08 09:30 (Formatter Plus v4.8.0) */
--@"c:\database\packages\IT\gm_pkg_it_login.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_it_login
IS
--
/*******************************************************
    * Description : Function to return total number of Logins
    * Author   : JAMES
 *******************************************************/
   FUNCTION gm_fch_total_logins
      RETURN NUMBER
   IS
      --
      v_cnt   NUMBER;
--
   BEGIN
      SELECT COUNT (*)
        INTO v_cnt
        FROM t103_user_login_track
       WHERE TRUNC (c103_login_ts) = TRUNC (SYSDATE);

      RETURN v_cnt;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN 0;
   END gm_fch_total_logins;

--
/*******************************************************
    * Description : Function to return total number of Unique Users
    * Author   : JAMES
 *******************************************************/
   FUNCTION gm_fch_total_unq_users
      RETURN NUMBER
   IS
      --
      v_cnt   NUMBER;
--
   BEGIN
      SELECT COUNT (*)
        INTO v_cnt
        FROM (SELECT DISTINCT c101_user_id
                         FROM t103_user_login_track
                        WHERE TRUNC (c103_login_ts) = TRUNC (SYSDATE));

      RETURN v_cnt;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN 0;
   END gm_fch_total_unq_users;

--
/*******************************************************
    * Description : Function to return total number of Unique Users-Internal
    * Author   : JAMES
 *******************************************************/
   FUNCTION gm_fch_total_unq_users_int
      RETURN NUMBER
   IS
      --
      v_cnt   NUMBER;
--

   BEGIN
      SELECT COUNT (*)
        INTO v_cnt
        FROM (SELECT DISTINCT c101_user_id
                         FROM t103_user_login_track
                        WHERE TRUNC (c103_login_ts) = TRUNC (SYSDATE)
                          AND c103_client_ip LIKE '192%');

      RETURN v_cnt;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN 0;
   END gm_fch_total_unq_users_int;

--
/*******************************************************
    * Description : Function to return total number of Unique Users-External
    * Author   : JAMES
 *******************************************************/
   FUNCTION gm_fch_total_unq_users_ext
      RETURN NUMBER
   IS
      --
      v_cnt   NUMBER;
--
   BEGIN
      SELECT COUNT (*)
        INTO v_cnt
        FROM (SELECT DISTINCT c101_user_id
                         FROM t103_user_login_track
                        WHERE TRUNC (c103_login_ts) = TRUNC (SYSDATE)
                          AND c103_client_ip NOT LIKE '192%');

      RETURN v_cnt;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN 0;
   END gm_fch_total_unq_users_ext;

   /*******************************************************
      * Description : procedure to save User details in t101_users
      * Author  : Brinal
   *******************************************************/
   PROCEDURE gm_save_user (
      p_user_id           IN       t101_user.c101_user_id%TYPE,
      p_sh_name           IN       t101_user.c101_user_sh_name%TYPE,
      p_f_name            IN       t101_user.c101_user_f_name%TYPE,
      p_l_name            IN       t101_user.c101_user_l_name%TYPE,
      p_email_id          IN       t101_user.c101_email_id%TYPE,
      p_dept_id           IN       t101_user.c901_dept_id%TYPE,
      p_accs_lvl_id       IN       t101_user.c101_access_level_id%TYPE,
      p_created_by        IN       t101_user.c101_created_by%TYPE,
      p_user_status       IN       t101_user.c901_user_status%TYPE,
      p_user_type         IN       t101_user.c901_user_type%TYPE,
      p_party_id          IN       t101_user.c101_party_id%TYPE,
      p_party_type        IN       t101_party.c901_party_type%TYPE,
      p_party_fl          IN       t101_party.c101_active_fl%TYPE,
      p_party_m_initial   IN       t101_party.c101_middle_initial%TYPE,
      p_out_party_id      OUT      t101_user.c101_party_id%TYPE,
      p_out_user_id       OUT      t101_user.c101_user_id%TYPE
   )
   AS
      v_party_id   t101_user.c101_party_id%TYPE      := p_party_id;
      v_group_id   t1500_group.c1500_group_id%TYPE;
      v_datefmt    VARCHAR2(10);
   BEGIN
      IF p_user_id IS NULL
      THEN
         SELECT s101_login.NEXTVAL
           INTO p_out_user_id
           FROM DUAL;
      ELSE
         p_out_user_id := p_user_id;
      END IF;

      IF (p_dept_id != 2005 AND p_user_type = 300
         )                                       -- All employees except Sales
      THEN
         v_party_id := p_out_user_id;
         v_group_id := p_dept_id;
      ELSE
         v_group_id := '2005_Out';
      END IF;
		
      SELECT GET_RULE_VALUE('DATEFMT','DATEFORMAT') INTO v_datefmt FROM DUAL;
      
      gm_pkg_cm_party.gm_cm_sav_party_id (v_party_id,
                                          p_f_name,
                                          p_l_name,
                                          p_party_m_initial,
                                          NULL,
                                          p_party_type,
                                          p_party_fl,
                                          p_created_by,
                                          p_out_party_id
                                         );

      UPDATE t101_party t101
         SET t101.c1500_group_id = v_group_id
        	 ,t101.c101_last_updated_by = p_created_by
             ,t101.c101_last_updated_date = SYSDATE
       WHERE t101.c101_party_id = p_out_party_id
       AND t101.c1500_group_id IS NULL;

      UPDATE t101_user
         SET c101_access_level_id = DECODE(c101_access_level_id,NULL, p_accs_lvl_id, c101_access_level_id),
             c101_user_sh_name = p_sh_name,
             c101_user_f_name = p_f_name,
             c101_user_l_name = p_l_name,
             c101_email_id = DECODE(c101_email_id,NULL,p_email_id,c101_email_id),
             c901_user_status = p_user_status,
             c101_party_id = DECODE(c101_party_id,NULL,p_party_id,c101_party_id), 
             c101_last_updated_by = p_created_by,
             C101_DATE_FORMAT = v_datefmt,
             c101_last_updated_date = SYSDATE
       WHERE c101_user_id = p_user_id;

      IF (SQL%ROWCOUNT = 0)
      THEN
         INSERT INTO t101_user
                     (c101_user_id, c101_user_sh_name, c101_user_f_name,
                      c101_user_l_name, c101_email_id, c101_access_level_id,
                      c101_hire_dt, c101_created_date, c101_created_by,
                      c901_dept_id, c901_user_status, c901_user_type,
                      c101_party_id,C101_DATE_FORMAT
                     )
              VALUES (p_out_user_id, p_sh_name, p_f_name,
                      p_l_name, p_email_id, p_accs_lvl_id,
                      TRUNC (SYSDATE), SYSDATE, p_created_by,
                      p_dept_id, p_user_status, p_user_type,
                      p_out_party_id,  v_datefmt
                     );
      END IF;
   -- p_out_party_id := v_party_id;
   END gm_save_user;

   /*******************************************************
      * Description : procedure to save User details in t102_user_login
      * Author  : Brinal
   *******************************************************/
   PROCEDURE gm_save_user_login (
      p_user_id     IN   t102_user_login.c101_user_id%TYPE,
      p_user_name   IN   t102_user_login.c102_login_username%TYPE,
      p_password    IN   t102_user_login.c102_user_password%TYPE,
      p_auth_type   IN   t102_user_login.c901_auth_type%TYPE,
      p_ldap_id	    IN 	 t102_user_login.c906c_ldap_master_id%TYPE
   )
   AS
   v_login_pin   t102_user_login.c102_login_pin%TYPE; --PMT-29459 Enable User PIN upon new user Creation
   BEGIN
	   --Get the login pin field with the last 4 digits of the UserID. 
	   SELECT NVL(SUBSTR(p_user_id,-4),p_user_id) INTO v_login_pin from dual; --PMT-29459 Enable User PIN upon new user Creation
	   
      INSERT INTO t102_user_login
                  (c102_user_login_id, c101_user_id, c102_login_username,
                   c102_user_password, c901_auth_type, c906c_ldap_master_id,c102_login_pin
                  )
           VALUES (s102_user_login.NEXTVAL, p_user_id, LOWER (p_user_name),
                   p_password, p_auth_type, p_ldap_id,v_login_pin
                  );
   END gm_save_user_login;

   /*******************************************************
      * Description : procedure to check if user's password expired
      * Author  : Xun
   *******************************************************/
   PROCEDURE gm_validate_passexp (
      p_userid    IN       t102_user_login.c101_user_id%TYPE,
      p_message   OUT      VARCHAR2
   )
/*
   Description        :  This component modify the user password.
   Parameters        :  p_userid -- The user id of the employee.
                      p_password -- The password of the employee
                      MESSAGE    -- Out put variable for sucess or failure of procedure
*/
   AS
      v_count   NUMBER;
   BEGIN
      SELECT COUNT (1)
        INTO v_count
        FROM t102_user_login
       WHERE c101_user_id = p_userid AND c102_password_expired_fl = 'Y';

      IF (v_count = 0)
      THEN
         p_message := '';
      ELSE
         p_message := '90890';                             --password expired
      END IF;
   END gm_validate_passexp;

   /*******************************************************
       * Description : procedure to fetch password when user forgot password
       * Author     : Xun
    *******************************************************/
   PROCEDURE gm_fch_password (
      p_usernm   IN   t102_user_login.c102_login_username%TYPE,
      p_email    IN   t101_user.c101_email_id%TYPE
   )
/*
   Description        :  This component send password to the user when user forgot it
   Parameters        :  p_userid -- The user id of the employee.
                      p_email  -- The email of the employee
*/
   AS
      v_pwd       t102_user_login.c102_user_password%TYPE;
      v_userid    t102_user_login.c101_user_id%TYPE;
      v_count     NUMBER;
      subject     VARCHAR2 (100);
      mail_body   VARCHAR2 (300);
      crlf        VARCHAR2 (2)                         := CHR (13)
                                                          || CHR (10);
      v_message   VARCHAR2 (300);
   BEGIN
      SELECT COUNT (1)
        INTO v_count
        FROM t102_user_login t102, t101_user t101
       WHERE t101.c101_user_id = t102.c101_user_id
         AND t101.c101_email_id = p_email
         AND UPPER (t102.c102_login_username) = UPPER (p_usernm);

      IF (v_count = 0)
      THEN
         raise_application_error (-20132, '');
      END IF;

      SELECT c102_user_password, c101_user_id
        INTO v_pwd, v_userid
        FROM t102_user_login
       WHERE UPPER (c102_login_username) = UPPER (p_usernm);

      subject := ' GM Portal - Password Request ';
      mail_body :=
            ' This auto-email is being sent by the Portal Admin off a request that you had submitted via the Forgot Password link. '
         || crlf
         || crlf
         || 'Your password is: '
         || v_pwd
         || crlf
         || crlf
         || 'If you did not raise this request, please contact IT immediately.';
      gm_com_send_email_prc (p_email, subject, mail_body);
      gm_update_log (v_userid, 'Forgot password', '1242', v_userid, v_message);
   END gm_fch_password;

   /******************************************************************
    * Description : Procedure to fetch the party id based on user id from user table
    * Author : Satyajit
    ****************************************************************/
   PROCEDURE gm_fch_partyid (
      p_userid    IN       t101_user.c101_user_id%TYPE,
      p_partyid   OUT      t101_user.c101_party_id%TYPE
   )
   IS
   BEGIN
      SELECT c101_party_id
        INTO p_partyid
        FROM t101_user
       WHERE c101_user_id = p_userid;
   END gm_fch_partyid;

   /******************************************************************
    * Description : Procedure to validate login
    * Author : James
    ****************************************************************/
   PROCEDURE gm_validate_login (
      p_user_nm_id   IN       VARCHAR2,
      p_pwd          IN       VARCHAR2,
      p_type         IN       VARCHAR2,
      p_msg          OUT      VARCHAR2
   )
   AS
      v_pwd          VARCHAR2 (20);
      v_userstatus   t101_user.c901_user_status%TYPE;
      v_count        NUMBER;
      v_userid       t101_user.c101_user_id%TYPE;
      v_passexp      VARCHAR2 (20);
   BEGIN
      SELECT COUNT (1)
        INTO v_count
        FROM t102_user_login
       WHERE c101_user_id = p_user_nm_id OR c102_login_username = p_user_nm_id;

      IF (v_count = 0)
      THEN
         raise_application_error (-1001, '');   -- Invalid Username or UserId
      END IF;

      IF p_type = 'PWD'
      THEN
         SELECT c102_user_password, t101.c901_user_status,
                t101.c101_user_id, c102_password_expired_fl
           INTO v_pwd, v_userstatus,
                v_userid, v_passexp
           FROM t101_user t101, t102_user_login t102
          WHERE t101.c101_user_id = t102.c101_user_id
            AND c102_login_username = p_user_nm_id;
      /* ELSIF p_type = 'PIN'
         THEN
            SELECT c102_user_pin, t101.c901_user_status, t101.c101_user_id, c102_password_expired_fl
              INTO v_pwd, v_userstatus, v_userid, v_passexp
              FROM t101_user t101, t102_user_login t102
             WHERE t101.c101_user_id = t102.c101_user_id AND t101.c101_user_id = TO_NUMBER (p_user_nm_id);*/
      END IF;

      IF v_userstatus = '311'
      THEN
         raise_application_error (-1005, '');       -- Disabled Account check
      END IF;

      IF v_pwd <> p_pwd
      THEN
         raise_application_error (-1002, '');         -- Wrong Password / PIN
      END IF;

      IF v_passexp = 'Y'
      THEN
         raise_application_error (-1019, '');       -- Expired Password / PIN
      END IF;

      p_msg := 'SUCCESS';
   END gm_validate_login;

   /******************************************************************
    * Description : Procedure to timestamp user tracking
    * Author : James
    ****************************************************************/
   PROCEDURE gm_user_tracking_prc (
      p_user_id     IN   t102_user_login.c101_user_id%TYPE,
      p_action      IN   VARCHAR2,
      p_sessid      IN   t103_user_login_track.c103_session_id%TYPE,
      p_client_ip   IN   t103_user_login_track.c103_client_ip%TYPE
   )
   AS
      v_action   VARCHAR2 (20);
   BEGIN
      IF p_action = 'logon'
      THEN
         INSERT INTO t103_user_login_track
                     (c103_tracking_id, c103_login_ts, c101_user_id,
                      c103_session_id, c103_client_ip
                     )
              VALUES (s101_login.NEXTVAL, SYSDATE, p_user_id,
                      p_sessid, p_client_ip
                     );
      ELSE
         UPDATE t103_user_login_track
            SET c103_logoff_ts = SYSDATE
          WHERE c101_user_id = p_user_id
            AND c103_logoff_ts IS NULL
            AND c103_session_id = p_sessid;
      END IF;
   END gm_user_tracking_prc;

   /******************************************************************
    * Description : Procedure to check if user is active
    * Author : Joe
    * 0 - inactive. 1 - active
    ****************************************************************/
   FUNCTION gm_is_useractive (
      p_usernm   IN   t102_user_login.c102_login_username%TYPE
   )
      RETURN VARCHAR2
   IS
      v_count_out   VARCHAR2 (2) := '0';
      v_count       NUMBER       := 0;
--
   BEGIN
      SELECT COUNT (t101.c101_user_id)
        INTO v_count
        FROM t101_user t101, t102_user_login t102
       WHERE t101.c101_user_id = t102.c101_user_id
         AND t101.c901_user_status = 311
         AND t102.c102_login_username = p_usernm;

      IF v_count > 0
      THEN
         v_count_out := '1';
      END IF;

      RETURN v_count_out;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN '0';
   END gm_is_useractive;

   /******************************************************************
    * Description : Procedure to check if users department is setup
    * Author : Joe
    * 1 - new user. 0 - otherwise
    ****************************************************************/
   FUNCTION gm_is_newuser (
      p_usernm   IN   t102_user_login.c102_login_username%TYPE
   )
      RETURN VARCHAR2
   IS
      v_count_out   VARCHAR2 (2) := '0';
      v_count       NUMBER       := 0;
   BEGIN
      SELECT COUNT (t101.c101_user_id)
        INTO v_count
        FROM t101_user t101, t102_user_login t102
       WHERE t101.c101_user_id = t102.c101_user_id
         AND t101.c901_user_status = 311
         AND t101.c901_dept_id = 2027
         AND t102.c102_login_username = p_usernm;

      IF v_count > 0
      THEN
         v_count_out := '1';
      END IF;

      RETURN v_count_out;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN '0';
   END gm_is_newuser;

   /******************************************************************
    * Description : Procedure to check if user exists in database
    * Author : Joe
    * 0 - doesnot exist 1 - exist
    ****************************************************************/
   FUNCTION gm_does_userexist (
      p_usernm   IN   t102_user_login.c102_login_username%TYPE
   )
      RETURN VARCHAR2
   IS
      v_count_out   VARCHAR2 (2) := '0';
      v_count       NUMBER       := 0;
   BEGIN
      SELECT COUNT (t101.c101_user_id)
        INTO v_count
        FROM t101_user t101, t102_user_login t102
       WHERE t101.c101_user_id = t102.c101_user_id
         AND t102.c102_login_username = p_usernm;

      IF v_count > 0
      THEN
         v_count_out := '1';
      END IF;

      RETURN v_count_out;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN '0';
   END gm_does_userexist;

   /******************************************************************
    * Description : Procedure to check if user is marked for AD Authentication
    * Author : Joe
    ****************************************************************/
   FUNCTION gm_is_usr_forad (
      p_usernm   IN   t102_user_login.c102_login_username%TYPE
   )
      RETURN VARCHAR2
   IS
      v_count_out   VARCHAR2 (2) := '0';
      v_auth_type   NUMBER;
   BEGIN
      BEGIN
         SELECT t102.c901_auth_type
           INTO v_auth_type
           FROM t101_user t101, t102_user_login t102
          WHERE t101.c101_user_id = t102.c101_user_id
            AND t102.c102_login_username = p_usernm;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            RETURN '0';
      END;

      IF v_auth_type = 320
      THEN
         v_count_out := '1';
      END IF;

      RETURN v_count_out;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN '0';
   END gm_is_usr_forad;
   
    /******************************************************************
    * Description : This procedure is used for Web Service.
    * 				In this procedure we are passing token id as an i/p
	* 			    parameter and we are checking whether that token is available in DB. 
    * Author : Manikandan Rajasekaran
    ****************************************************************/
 	PROCEDURE gm_fch_db_tokens(
 	p_token     IN  t101a_user_token.c101a_token_id%TYPE,
 	p_token_cur OUT TYPES.cursor_type
 	)
 	AS
 		v_date_fmt  VARCHAR2(20);
 	BEGIN
	 	
	 	v_date_fmt := get_rule_value( 'DATEFMT','DATEFORMAT');
	 	OPEN p_token_cur 
		FOR 
		
	 	SELECT t101.c101_user_id userid ,t101.c101_user_sh_name shname, t102.c102_login_username usernm,
  			   get_code_name(c901_dept_id) deptid,c101_access_level_id acid, c901_dept_id deptseq ,
  			   get_last_login_ts(t101.c101_user_id) logints ,t101.c101_party_id partyid ,
               decode(t101.c101_date_format,null,v_date_fmt,t101.c101_date_format) appldatefmt ,
               get_rule_value(t101.c101_date_format, 'DATEFORMAT') jsdatefmt ,t102.c102_ext_access extaccess,
               get_company_id(c101_access_level_id,c901_dept_id,t101.c101_user_id, gm_pkg_it_user.get_default_party_company(t101.c101_party_id)) companyid ,
               get_division_id(c101_access_level_id,c901_dept_id,t101.c101_user_id, gm_pkg_it_user.get_default_party_company(t101.c101_party_id)) divid ,
               get_rep_id_by_ass_id(t101.c101_user_id) repid,c101_user_f_name fname,c101_user_l_name lname ,
               gm_pkg_alt_alerts.get_unread_alerts_cnt(t101.c101_user_id) alertcnt,t101a.c101a_token_id token
        FROM   t101_user t101,t102_user_login t102,t101a_user_token t101a
		WHERE  t101.c101_user_id      = t102.c101_user_id
		AND    t101.c101_user_id      = t101a.c101_user_id
        AND    t101a.C901_STATUS = '103101'--103101- Active Status
        AND    t101.c901_user_status = 311 -- Active
        AND    t101a.C101A_TOKEN_ID = p_token;
 	
 	END gm_fch_db_tokens;
END gm_pkg_it_login;
/
