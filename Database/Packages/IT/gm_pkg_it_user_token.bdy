--@"c:\database\packages\IT\gm_pkg_it_user_token.bdy";
CREATE OR REPLACE
PACKAGE BODY gm_pkg_it_user_token
IS
    /******************************************************************
    * Description : Procedure to persist the token information
    * Author : Rajeshwaran
    ****************************************************************/
PROCEDURE gm_sav_user_token (
        p_token   IN T101A_USER_TOKEN.C101A_TOKEN_ID%TYPE,
        p_status  IN T101A_USER_TOKEN.C901_STATUS%TYPE,
        p_user_id IN T101_USER.C101_USER_ID%TYPE)
AS
    v_cnt NUMBER := 0;
BEGIN
     UPDATE T101A_USER_TOKEN
        SET C901_STATUS        = p_status
          , C101A_LAST_UPDATED_DATE = SYSDATE
          , C101A_LAST_UPDATED_BY = p_user_id
      WHERE C101A_TOKEN_ID = p_token
        AND C101_USER_ID   = p_user_id
        AND C101A_VOID_FL IS NULL;
        
    IF (SQL%ROWCOUNT       = 0) THEN
        --An user can have more than one token, hence we have to insert token rather than update the token for the user.
         INSERT
           INTO T101A_USER_TOKEN
            (
                C101A_TOKEN_ID, C101_USER_ID, C101A_LAST_UPDATED_DATE
              , C101A_LAST_UPDATED_BY, C901_STATUS
            )
            VALUES
            (
                p_token, p_user_id, SYSDATE
              , p_user_id, 103101
            ) ;
    END IF;
    -- Check how many active tokens are available in the db.
     SELECT COUNT (1)
       INTO v_cnt
       FROM T101A_USER_TOKEN
      WHERE C101A_TOKEN_ID = p_token
        AND C101A_VOID_FL IS NULL
        AND C901_STATUS    = 103101; -- ACTIVE
    --If the token is already available, raise app error, as we cannot have duplicate active tokens in the DB.
    IF v_cnt > 1 THEN
        raise_application_error ( - 20157, 'This token '||p_token||' is already available.') ;
    END IF;
END gm_sav_user_token;
/******************************************************************
* Description : Procedure to persist the Device UUID information
* Author : Rajeshwaran
****************************************************************/
PROCEDURE gm_sav_device_info (
        p_uuid        IN T9150_DEVICE_INFO.C9150_DEVICE_UUID%TYPE,
        p_device_type IN T9150_DEVICE_INFO.C901_DEVICE_TYPE%TYPE,
        p_user_id     IN T101_USER.C101_USER_ID%TYPE,
        p_device_token IN	T9150_DEVICE_INFO.c9150_device_token%TYPE default null)
AS
    v_cnt NUMBER := 0;
BEGIN
	
     UPDATE T9150_DEVICE_INFO
        SET C101_USER_ID        = p_user_id--Based on device id update the user id.So that update/sync works based on device id.//PMT-21959  
          , C9150_LAST_UPDATED_DATE = SYSDATE
          , C9150_LAST_UPDATED_BY = p_user_id
          , c9150_device_token = p_device_token
      WHERE C9150_DEVICE_UUID   = p_uuid        
        AND C9150_VOID_FL      IS NULL;
        
    IF (SQL%ROWCOUNT            = 0) THEN
         INSERT
           INTO T9150_DEVICE_INFO
            (
                C9150_DEVICE_INFO_ID, C9150_DEVICE_UUID,
                C901_DEVICE_TYPE
              , C101_USER_ID, C9150_LAST_UPDATED_DATE, C9150_LAST_UPDATED_BY, c9150_device_token
            )
            VALUES
            (
                S9150_DEVICE_INFO.NEXTVAL, p_uuid, p_device_type
              , p_user_id, SYSDATE, p_user_id, p_device_token
            ) ;
    END IF;
     SELECT COUNT (1)
       INTO v_cnt
       FROM T9150_DEVICE_INFO
      WHERE C9150_DEVICE_UUID = p_uuid
        AND C101_USER_ID      = p_user_id
        AND C9150_VOID_FL    IS NULL;
    IF v_cnt                  > 1 THEN
        raise_application_error ( - 20157, 'This UUID '||p_uuid||' is already available for' ||get_user_name (p_user_id) ||'.') ;
    END IF;
END gm_sav_device_info;

/***********************************************************************
* Description : Procedure to fetch the device token string of sales reps
* Author : arajan
************************************************************************/
PROCEDURE gm_fch_device_token_str(
	  p_ref_id		IN		t501_order.c501_order_id%TYPE
	, p_token_str	OUT		CLOB
)
AS
	v_sales_rep		t501_order.c703_sales_rep_id%TYPE;
	v_created_by	t501_order.c501_created_by%TYPE;
	v_token_str		CLOB;
BEGIN
	/* Get the sales rep id and the created by id based on the ref id
	 * If no data found, then get the values by passing the ref id as the parent order id and the void flag should be Y, 
	 * because if the order is voided, then it will generate another order id with the original one as parent order id
	 */
	BEGIN
		SELECT c703_sales_rep_id,
		  c501_created_by
		  INTO v_sales_rep, v_created_by
		FROM t501_order
		WHERE c501_order_id = p_ref_id
		AND c501_void_fl IS NULL;
	EXCEPTION
	WHEN NO_DATA_FOUND THEN
		BEGIN
			SELECT c703_sales_rep_id,
			  c501_created_by
			INTO v_sales_rep, v_created_by
			FROM t501_order
			WHERE c501_parent_order_id = p_ref_id
			AND c501_void_fl          IS NOT NULL;
		EXCEPTION
		WHEN NO_DATA_FOUND THEN
			v_sales_rep := NULL;
			v_created_by:= NULL;
		END;
	END;
	-- If the DO is raised by associated sales rep, then the device token of both sales rep and associated sales rep should be fetched,
	-- Otherwise only the device token of sales rep need to return
		SELECT RTRIM (XMLAGG (XMLELEMENT (e, t9150.c9150_device_token || ',')) .EXTRACT ('//text()'), ',')
			INTO v_token_str
		FROM t9150_device_info t9150,
		  (SELECT t9150.c101_user_id userid,
		    MAX(t9150.c9150_last_updated_date) updateddate
		  FROM t703_sales_rep t703,
		    t101_user t101,
		    t9150_device_info t9150
		  WHERE t703.c703_sales_rep_id IN (v_sales_rep,v_created_by)
		  AND t703.c101_party_id        = t101.c101_party_id
		  AND t9150.c101_user_id        = t101.c101_user_id
		  AND t703.c703_void_fl        IS NULL
		  AND t9150.c9150_void_fl      IS NULL
		  GROUP BY t9150.c101_user_id
		  ) t101
		WHERE t9150.c101_user_id          = t101.userid
		AND t9150.c9150_last_updated_date = updateddate ;

	p_token_str := v_token_str;
END gm_fch_device_token_str;
END gm_pkg_it_user_token;
/
