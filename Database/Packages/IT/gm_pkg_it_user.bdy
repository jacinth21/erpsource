/* Formatted on 2012/03/20 16:51 (Formatter Plus v4.8.0) */
--@"c:\database\packages\IT\gm_pkg_it_user.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_it_user
IS
   /******************************************************************
    * Description : Procedure to update user login attempt count
    * Author : Hardik
    ****************************************************************/
   PROCEDURE gm_update_user_loginattempt (
      p_userid           IN   t101_user.c101_user_id%TYPE,
      p_usermaxattempt   IN   NUMBER
   )
   AS
      v_loginlock       t102_user_login.c102_login_lock_fl%TYPE;
      v_loginatmptcnt   t102_user_login.c102_login_attempt_cnt%TYPE;
   BEGIN
      SELECT     c102_login_lock_fl, c102_login_attempt_cnt
            INTO v_loginlock, v_loginatmptcnt
            FROM t102_user_login
           WHERE c101_user_id = p_userid
      FOR UPDATE;

      IF v_loginatmptcnt IS NULL
      THEN
         v_loginatmptcnt := 0;
      END IF;

      v_loginatmptcnt := v_loginatmptcnt + 1;

      UPDATE t102_user_login
         SET c102_login_lock_fl =
                DECODE (v_loginatmptcnt,
                        p_usermaxattempt, 'Y',
                        c102_login_lock_fl
                       ),
             c102_login_attempt_cnt = v_loginatmptcnt,
             c102_login_attempt_time = CURRENT_DATE,
             c102_last_updated_by = p_userid,
             c102_last_updated_date = CURRENT_DATE
       WHERE c101_user_id = p_userid;
   END gm_update_user_loginattempt;

   /******************************************************************
    * Description : Procedure to update login lock status for all users given in string.
    * input string will be in form of userid^Y| e.g. 303042^Y|303043^N|
    * Author : Hardik
    ****************************************************************/
   PROCEDURE gm_update_user_lock_list (
      p_inputstring   IN   VARCHAR2,
      p_userid        IN   t101_user.c101_user_id%TYPE
   )
   AS
      v_strlen       NUMBER                := NVL (LENGTH (p_inputstring), 0);
      v_string       VARCHAR2 (4000)                         := p_inputstring;
      v_substring    VARCHAR2 (4000);
      v_user_id      t101_user.c101_user_id%TYPE;
      v_login_lock   t102_user_login.c102_login_lock_fl%TYPE;
   BEGIN
      IF v_strlen > 0
      THEN
         WHILE INSTR (v_string, '|') <> 0
         LOOP
            v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
            v_string := SUBSTR (v_string, INSTR (v_string, '|') + 1);
            --
            v_user_id := NULL;
            v_login_lock := NULL;
            v_user_id := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
            v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
            v_login_lock := v_substring;
            gm_update_user_lock (v_user_id, v_login_lock, p_userid);
         END LOOP;
      END IF;
   END gm_update_user_lock_list;

   /******************************************************************
      * Description : Procedure to update user lock status and reset count if unlock
      * Author : Hardik
      ****************************************************************/
   PROCEDURE gm_update_user_lock (
      p_lockuserid      IN   t101_user.c101_user_id%TYPE,
      p_login_lock_fl   IN   t102_user_login.c102_login_lock_fl%TYPE,
      p_userid          IN   t101_user.c101_user_id%TYPE
   )
   AS
   BEGIN
      UPDATE t102_user_login
         SET c102_login_lock_fl = NVL (p_login_lock_fl, c102_login_lock_fl),
             c102_login_attempt_cnt =
                      DECODE (p_login_lock_fl,
                              'N', 0,
                              c102_login_attempt_cnt
                             ),
             c102_last_updated_by = p_userid,
             c102_last_updated_date = CURRENT_DATE
       WHERE c101_user_id = p_lockuserid;
   END gm_update_user_lock;

   /******************************************************************
   * Description : Procedure to get list of user with their lock status
   * Author : Hardik
   ****************************************************************/
   PROCEDURE gm_fch_user_list (p_userlist OUT TYPES.cursor_type)
   AS
   BEGIN
      OPEN p_userlist
       FOR
          SELECT   t101.c101_user_id userid, c102_login_username username,
                   c101_user_f_name firstnm, c101_user_l_name lastnm,
                   get_code_name_alt (c901_dept_id) deptnm,
                   TO_CHAR (c102_login_attempt_time,
                            GET_RULE_VALUE('DATEFMT', 'DATEFORMAT')||' HH:MM:SS AM'
                           ) locktime,
                   c102_login_lock_fl loginlockfl
              FROM t101_user t101, t102_user_login t102
             WHERE t101.c101_user_id = t102.c101_user_id
               AND t101.c901_user_status = 311
               AND t102.c102_login_lock_fl = 'Y'
          ORDER BY username;
   END gm_fch_user_list;

   /******************************************************************
   * Description : Procedure to create entries for User AD Status update.
   * Author : Hardik
   ****************************************************************/
   PROCEDURE gm_update_user_ad_log (p_input VARCHAR2, p_userid NUMBER)
   AS
      v_strlen      NUMBER          := NVL (LENGTH (p_input), 0);
      v_string      VARCHAR2 (4000) := p_input;
      v_substring   VARCHAR2 (1000);
      v_username    VARCHAR2 (100);
      v_adstatus    NUMBER;
      v_reason      NUMBER;
      v_userid      NUMBER;
   BEGIN
      IF v_strlen > 0
      THEN
         WHILE INSTR (v_string, '|') <> 0
         LOOP
            v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
            v_string := SUBSTR (v_string, INSTR (v_string, '|') + 1);
            --
            v_userid := NULL;
            v_adstatus := NULL;
            v_reason := NULL;
            --
            v_username :=
                         SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
            v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
            v_adstatus :=
               TO_NUMBER (SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1)
                         );
            v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
            v_reason := TO_NUMBER (v_substring);

            BEGIN
               SELECT t102.c101_user_id
                 INTO v_userid
                 FROM t102_user_login t102
                WHERE t102.c102_login_username = v_username;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  v_userid := NULL;
            END;

            INSERT INTO t1015_user_ad_status
                 VALUES (s1015_user_ad_status.NEXTVAL, v_username, p_userid,
                         CURRENT_DATE, v_userid, v_adstatus, v_reason);

            UPDATE t101_user t101
               SET c901_user_status = v_reason,
                   c101_last_updated_date = CURRENT_DATE,
                   c101_last_updated_by = p_userid
             WHERE t101.c101_user_id = v_userid;
         END LOOP;
      END IF;
   END gm_update_user_ad_log;

   /******************************************************************
    * Description : Procedure to get all email address to send notification while AD status update.
    * Author : Hardik
    ****************************************************************/
   PROCEDURE gm_get_ad_emails_recipients (
      p_input                  VARCHAR2,
      p_adgroupdemails   OUT   TYPES.cursor_type,
      p_boemails         OUT   TYPES.cursor_type
   )
   AS
   BEGIN
      OPEN p_adgroupdemails
       FOR
          SELECT get_user_emailid (c101_mapped_party_id) useremail
            FROM t1501_group_mapping
           WHERE c1500_group_id = '20';

      OPEN p_boemails
       FOR
          SELECT c906_rule_value bomail
            FROM t906_rules
           WHERE c906_rule_id IN (
                    SELECT DISTINCT c901_dept_id
                               FROM t101_user t101,
                                    t102_user_login t102,
                                    v_double_in_list vdblilist
                              WHERE t101.c101_user_id = t102.c101_user_id
                                AND t102.c102_login_username = token
                                AND tokenii = 91391)
             AND c906_rule_grp_id = 'BOEMAIL';
   END gm_get_ad_emails_recipients;

   /******************************************************************
   * Description : Procedure to get list of AD Access user
   * Author : Hardik
   ****************************************************************/
   PROCEDURE gm_fch_ad_access_user_list (p_userlist OUT TYPES.cursor_type)
   AS
   BEGIN
      OPEN p_userlist
       FOR
          SELECT      t101.c101_user_f_name
                   || ' '
                   || t101.c101_user_l_name username,
                   t101.c101_user_id userid
              FROM t1501_group_mapping t1501, t101_user t101
             WHERE t1501.c1500_group_id = '20'
               AND t101.c101_party_id = t1501.c101_mapped_party_id
          ORDER BY username;
   END gm_fch_ad_access_user_list;

   /******************************************************************
   * Description : Procedure to get AD Status History Records
   * Author : Hardik
   ****************************************************************/
   PROCEDURE gm_fch_ad_status_list (
      p_userid           NUMBER,
      p_status           NUMBER,
      p_userlist   OUT   TYPES.cursor_type
   )
   AS
   BEGIN
      OPEN p_userlist
       FOR
          SELECT t1015.c1015_user_ad_status_id useradstatusid,
                 t1015.c1015_ad_user_name ldapuname,
                 t101.c101_user_f_name fname, t101.c101_user_l_name lname,
                 get_code_name_alt (t101.c901_dept_id) department,
                 get_code_name (t1015.c901_status) status,
                 get_user_name (t1015.c1015_created_by) statusupdatedby,
                 get_code_name_alt (c901_dept_id) deptnm,
                 get_code_name (c901_reason) reason,
                 TO_CHAR (t1015.c1015_created_date,
                          GET_RULE_VALUE('DATEFMT', 'DATEFORMAT')||' HH:MM:SS AM'
                         ) updateddate
            FROM t101_user t101, t1015_user_ad_status t1015
           WHERE t101.c101_user_id(+) = t1015.c101_user_id
             AND t1015.c1015_created_by =
                                        NVL (p_userid, t1015.c1015_created_by)
             AND t1015.c901_status = NVL (p_status, t1015.c901_status);
   END gm_fch_ad_status_list;

   /******************************************************************
    * Description : Procedure to updated password expired flag if password is not change since long
    * Author : Hardik
    ****************************************************************/
   PROCEDURE gm_update_password_exp_fl
   AS
      v_passexpdays   VARCHAR2 (10);

      CURSOR curr_user
      IS
         SELECT c101_user_id userid
           FROM t101_user t101
          WHERE c901_user_status = 311;

      CURSOR curr_user_day
      IS
         SELECT t101.c101_user_id userid
           FROM t101_user t101,
                t102_user_login t102,
                (SELECT   c101_user_id userid,
                          TRUNC (MAX (c103_login_ts)) login_dt
                     FROM t103_user_login_track
                 GROUP BY c101_user_id) latestdt
          WHERE t101.c101_user_id = latestdt.userid
            AND t101.c101_user_id = t102.c101_user_id
            AND (TRUNC (CURRENT_DATE) - TRUNC (login_dt)) >=
                                 get_rule_value ('LATELOGDAYS', 'LATELOGDAYS')
            AND t101.c901_user_status = 311;
   BEGIN
      v_passexpdays := get_rule_value ('PASSWDEXPDAYS', 'PASSWDEXPDAYS');

      FOR var_user IN curr_user
      LOOP
         UPDATE t102_user_login
            SET c102_password_expired_fl = 'Y',
                c102_last_updated_by = '30301',
                c102_last_updated_date = CURRENT_DATE
          WHERE (  TRUNC (CURRENT_DATE)
                 - NVL (TRUNC (c102_password_updated_date), TRUNC (CURRENT_DATE))
                ) >= v_passexpdays
            AND c101_user_id = var_user.userid
            AND c901_auth_type = 321;
      END LOOP;

      -- To Lock the User, if a user (DB or AD authentication) that hasnt logged in for the last 30 days
      -- to be locked and password expired
      FOR var_user IN curr_user_day
      LOOP
         UPDATE t102_user_login
            SET c102_password_expired_fl = 'Y',
                c102_user_lock_fl = '1',
                c102_login_lock_fl = 'Y',
                c102_last_updated_by = '30301',
                c102_last_updated_date = CURRENT_DATE
          WHERE c101_user_id = var_user.userid
                AND c901_auth_type IN (321); -- MNTTASK-5884- Locked only DB User.
      END LOOP;
   END gm_update_password_exp_fl;

   /******************************************************************
     * Description : Procedure to update navigation link
     * Author : Hardik
     ****************************************************************/
   PROCEDURE gm_update_user_navigation_link (
      p_nodeid         IN   t1520_function_list.c1520_function_id%TYPE,
      p_nodeparentid   IN   t1520_function_list.c1520_inherited_from_id%TYPE,
      p_groupid        IN   t1500_group.c1500_group_id%TYPE,
      p_userid         IN   t101_user.c101_user_id%TYPE
   )
   AS
      v_function_list_id   VARCHAR2 (100)  := 'LM';
      v_request_uri        VARCHAR2 (1000) := NULL;
      v_count              NUMBER          := 0;
      v_fuction_seq_id     NUMBER;
   BEGIN
      INSERT INTO t1530_access
                  (c1530_access_id, c1500_group_id, c1520_function_id,
                   c101_party_id, c1530_update_access_fl,
                   c1530_read_access_fl, c1530_void_access_fl,
                   c1530_void_fl, c1530_created_by, c1530_created_date,
                   c1520_parent_function_id
                  )
           VALUES (s1530_access.NEXTVAL, p_groupid, p_nodeid,
                   p_userid, 'Y',
                   'Y', 'Y',
                   NULL, p_userid, CURRENT_DATE,
                   p_nodeparentid
                  );
   END gm_update_user_navigation_link;

   /******************************************************************
    * Description : Procedure to fetch navigation link
    * Author : Hardik
    ****************************************************************/
   PROCEDURE gm_fetch_user_navigation_link (
      p_groupid          IN       t1500_group.c1500_group_id%TYPE,
      p_user_id          IN       t101_user.c101_user_id%TYPE,
      p_navigationtree   OUT      TYPES.cursor_type
   )
   AS
      v_party_id       NUMBER;
      v_group_id       VARCHAR2 (20);
      v_dept_id        NUMBER;
      v_access_level   NUMBER;
   BEGIN
      SELECT t101.c901_dept_id, t101.c101_access_level_id, t101.c101_party_id
        INTO v_dept_id, v_access_level, v_party_id
        FROM t101_user t101
       WHERE t101.c101_user_id = p_user_id;

      BEGIN
         IF v_dept_id = 2005 AND (v_access_level <= 2 OR v_access_level = 7)
         THEN
            SELECT t101.c101_party_id, t101.c1500_group_id
              INTO v_party_id, v_group_id
              FROM t101_party t101, t703_sales_rep t703
             WHERE t101.c101_party_id = t703.c101_party_id
               AND t703.c703_sales_rep_id = p_user_id;
         ELSE
            SELECT t101p.c1500_group_id
              INTO v_group_id
              FROM t101_party t101p
             WHERE t101p.c101_party_id = v_party_id;
         END IF;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            v_party_id := NULL;
            v_group_id := NULL;
      END;

      IF p_groupid IS NOT NULL
      THEN
         v_group_id := p_groupid;
      END IF;
     OPEN p_navigationtree
      FOR
         SELECT DISTINCT t1520.c1520_function_id function_id,
                  NVL (t1530.c1530_function_nm,
                       t1520.c1520_function_nm
                      ) function_nm,
                  t1520.c1520_request_uri url,
                  t1520.c1520_request_stropt stropt, (lvl) "LEVEL", root, seq_no,
                  DECODE (t1520.c1520_request_uri,
                          '', 'Folder',
                          'File'
                         ) TYPE,t1520.c1520_left_link_path path,v_group_id groupid
             FROM t1520_function_list t1520,
                  (SELECT     t1530.c1520_function_id fun_id, LEVEL lvl,
                              CONNECT_BY_ROOT t1530.c1520_function_id root,
                              t1530.c1530_seq_no seq_no,
                              t1530.c1530_function_nm
                         FROM t1530_access t1530
                        WHERE t1530.c1530_void_fl IS NULL
                          AND (   t1530.c1500_group_id = v_group_id
                               OR (    t1530.c101_party_id = v_party_id
                                   AND (   t1530.c1530_read_access_fl = 'Y'
                                        OR t1530.c1530_update_access_fl = 'Y'
                                       )
                                  )
                              )
                          AND t1530.C1520_PARENT_FUNCTION_ID IS NOT NULL
                   START WITH t1530.c1520_parent_function_id =
                                 ((SELECT MIN
                                             (t1530t.c1520_parent_function_id)
                                     FROM t1530_access t1530t
                                    WHERE t1530t.c1500_group_id = v_group_id))
             CONNECT BY NOCYCLE PRIOR t1530.C1520_FUNCTION_ID = t1530.C1520_PARENT_FUNCTION_ID) t1530
         WHERE    t1520.c1520_function_id = t1530.fun_id
              AND t1520.c1520_void_fl IS NULL
              AND t1520.c1520_function_id NOT IN (
                     SELECT t1530.c1520_function_id
                       FROM t1530_access t1530
                      WHERE t1530.c1530_read_access_fl = 'N'
                        AND t1530.c101_party_id = v_party_id)
              AND t1530.lvl > 0
              AND t1520.c901_type = 92262
         ORDER BY seq_no, lvl;
   END gm_fetch_user_navigation_link;

   /******************************************************************
   * Description : Procedure to fetch module groups
   * Author : Hardik
   ****************************************************************/
   PROCEDURE gm_fetch_module_groups (p_groupslist OUT TYPES.cursor_type)
   AS
   BEGIN
      OPEN p_groupslist
       FOR
          SELECT t1500.c1500_group_id groupid,
                 t1500.c1500_group_nm groupname
            FROM t1500_group t1500
           WHERE t1500.c1500_void_fl IS NULL;
   END gm_fetch_module_groups;

   /******************************************************************
   * Description : Procedure to delete navigation node
   * Author : Hardik
   ****************************************************************/
   PROCEDURE gm_delete_navigation_link (
      p_nodeid    IN   t1520_function_list.c1520_function_id%TYPE,
      p_groupid   IN   t1500_group.c1500_group_id%TYPE,
      p_userid    IN   t101_user.c101_user_id%TYPE
   )
   AS
   BEGIN
      UPDATE t1520_function_list
         SET c1520_void_fl = 'Y',
             c1520_last_updated_by = p_userid,
             c1520_last_updated_date = CURRENT_DATE
       WHERE c1520_function_id = p_nodeid;

      UPDATE t1530_access
         SET c1530_void_fl = 'Y',
             c1530_last_updated_by = p_userid,
             c1530_last_updated_date = CURRENT_DATE
       WHERE c1520_function_id = p_nodeid
         AND c1500_group_id = NVL (p_groupid, c1500_group_id);
   END gm_delete_navigation_link;

   /******************************************************************
   * Description : Procedure to fetch menu items
   * Author : Hardik
   ****************************************************************/
   PROCEDURE gm_fch_menu_names (
      p_menufolderlist   OUT   TYPES.cursor_type,
      p_menufilelist     OUT   TYPES.cursor_type
   )
   AS
   BEGIN
      OPEN p_menufolderlist
       FOR
          SELECT t1520.c1520_function_id function_id,
                    t1520.c1520_function_nm
                 || ' - '
                 || get_code_name_alt (t1520.c901_department_id) folder_name
            FROM t1520_function_list t1520, t1530_access t1530
           WHERE t1520.c1520_request_uri IS NULL
             AND t1530.c1520_function_id = t1520.c1520_function_id
             AND t1520.c901_type = '92262';

      OPEN p_menufilelist
       FOR
          SELECT t1520.c1520_function_id function_id,
                    t1520.c1520_function_nm
                 || ' - '
                 || get_code_name_alt (t1520.c901_department_id) file_name
            FROM t1520_function_list t1520, t1530_access t1530
           WHERE t1520.c1520_request_uri IS NOT NULL
             AND t1530.c1520_function_id = t1520.c1520_function_id
             AND t1520.c901_type = '92262';
   END gm_fch_menu_names;

   /******************************************************************
    * Description : Procedure to fetch the user Group
    * Author : Hardik
    ****************************************************************/
   PROCEDURE gm_fch_user_groups (
      p_user_id      IN       t1501_group_mapping.c101_mapped_party_id%TYPE,
      p_group_list   OUT      TYPES.cursor_type
   )
   AS
   BEGIN
      OPEN p_group_list
       FOR
          SELECT   t1500.c1500_group_id GROUP_ID,
                   t1500.c1500_group_nm group_nm
              FROM t1501_group_mapping t1501,
                   t1500_group t1500,
                   t101_user t101
             WHERE t1500.c1500_group_id = t1501.c1500_group_id
               AND t101.c101_party_id = t1501.c101_mapped_party_id
               AND t101.c101_user_id = p_user_id
               AND t1500.c901_group_type = 92262
               AND t1500.c1500_void_fl IS NULL
               AND t1501.c1501_void_fl IS NULL
          ORDER BY t1500.c1500_group_nm;
   END;

   /******************************************************************
    * Description : Procedure to save the password history
    * Author : VPrasath
    ****************************************************************/
   PROCEDURE gm_sav_user_pwd_history (
      p_created_by      IN   t102a_user_password_history.c102a_created_by%TYPE,
      p_user_login_id   IN   t102a_user_password_history.c102_user_login_id%TYPE,
      p_password        IN   t102a_user_password_history.c102a_password%TYPE
   )
   AS
   BEGIN
      INSERT INTO t102a_user_password_history
                  (c102a_user_pwd_id, c102a_created_by,
                   c102a_created_date, c102_user_login_id, c102a_password
                  )
           VALUES (s102a_user_password_history.NEXTVAL, p_created_by,
                   CURRENT_DATE, p_user_login_id, p_password
                  );
   END gm_sav_user_pwd_history;

/******************************************************************
   * Description : Procedure to fetch user details with ID
   * Author : Kulanthaivelu
   ****************************************************************/
   PROCEDURE gm_fch_user_detail (
      p_user_login_name   IN       t102_user_login.c102_login_username%TYPE,
      p_user_id           IN       t101_user.c101_user_id%TYPE,
      p_user_list         OUT      TYPES.cursor_type
   )
   AS
   BEGIN
      OPEN p_user_list
       FOR
          SELECT t101.c101_user_id userid, utl_i18n.raw_to_char(utl_raw.cast_to_raw(t101.c101_user_f_name), 'utf8') fname,
                 utl_i18n.raw_to_char(utl_raw.cast_to_raw(c101_user_l_name), 'utf8') lname, utl_i18n.raw_to_char(utl_raw.cast_to_raw(c101_user_sh_name), 'utf8') shortname,
                 c101_access_level_id accesslevel, c101_email_id emailid,
                 c901_dept_id departmentid, c101_work_phone workphone,
                 c101_title title,
                 get_code_name (c901_user_status) userstatus,
                 c901_user_status userstatusid, c901_user_type usertype,
                 get_audit_log_flag (t101.c101_user_id, 901) dept_his_flg,
                 get_audit_log_flag (t101.c101_user_id, 900) acclvl_his_flg,
                 LOWER (t102.c102_login_username) userloginname,
                 NVL (t102.c102_ext_access, 'N') externalaccfl,
                 get_code_name (t102.c901_auth_type) authtype,
                 nvl(t101p.c1500_group_id,'0') SECURITYGRP,
                  t101p.c101_party_id partyid,gm_pkg_it_user.get_default_party_company(t101p.c101_party_id) companyid,
                  t102.c906c_ldap_master_id ldapid,
                  t102.c102_login_pin lockPin,--PMT-29459 Enable User PIN upon new user Creation
                  get_login_user_name_by_id (t102.c101_copy_access_user_id) copyusername --PMT-51134--Copy Security Group access
            FROM t101_user t101, t102_user_login t102, t101_party t101p
           WHERE 
           t101p.c101_party_id = t101.c101_party_id (+)
           AND 
           t101.c101_user_id =
                          DECODE (p_user_id,
                                  0, t101.c101_user_id,
                                  p_user_id
                                 )
             AND LOWER (t102.c102_login_username) =
                     NVL (p_user_login_name, LOWER (t102.c102_login_username))
             AND t101.c101_user_id = t102.c101_user_id;
   END gm_fch_user_detail;

/**************************************************************************************************
    * Description : Procedure to update user and update party and called gm_save_user_group_mapping
    * Author : Kulanthaivelu
    ***********************************************************************************************/
   PROCEDURE gm_save_user (
      p_userid            IN OUT   t101_user.c101_user_id%TYPE,
      p_user_login_name   IN       t102_user_login.c102_login_username%TYPE,
      p_password          IN       t102_user_login.c102_user_password%TYPE,
      p_first_name        IN       t101_user.c101_user_f_name%TYPE,
      p_last_name         IN       t101_user.c101_user_l_name%TYPE,
      p_short_name        IN       t101_user.c101_user_sh_name%TYPE,
      p_access            IN       t101_user.c101_access_level_id%TYPE,
      p_email             IN       t101_user.c101_email_id%TYPE,
      p_deptid            IN       t101_user.c901_dept_id%TYPE,
      p_work_phone        IN       t101_user.c101_work_phone%TYPE,
      p_title             IN       t101_user.c101_title%TYPE,
      p_login_user        IN       t101_user.c101_last_updated_by%TYPE,
      p_user_type         IN       t101_user.c901_user_type%TYPE,
      p_date_fmt          IN       t101_user.c101_date_format%TYPE,
      p_ext_access        IN       t102_user_login.c102_ext_access%TYPE,
      p_ldap_id			  IN 	   t102_user_login.c906c_ldap_master_id%TYPE,
      p_sales_rep_id      OUT      t703_sales_rep.c703_sales_rep_id%TYPE,
      p_primary_comp_id   IN       T1900_company.c1900_company_id%TYPE DEFAULT NULL, 
      p_login_pin         IN       T102_USER_LOGIN.C102_LOGIN_PIN%TYPE DEFAULT NULL --PMT-29459 Enable User PIN upon new user Creation
      
   )
   AS
      v_partyid        t101_user.c101_party_id%TYPE;
      v_contactid      t107_contact.c107_contact_id%TYPE;
      v_dist_id        t101_user.c701_distributor_id%TYPE;
      v_out_dist_id    t101_user.c701_distributor_id%TYPE;
      v_out_party_id   t101_user.c101_party_id%TYPE;
      v_out_user_id    t101_user.c101_user_id%TYPE;
      v_user_id        t101_user.c101_user_id%TYPE;
      v_dept_id        t101_user.c901_dept_id%TYPE;
      v_sales_rep_id   t703_sales_rep.c703_sales_rep_id%TYPE;
      v_count          NUMBER;
      v_action         VARCHAR2 (100);
      v_message        VARCHAR2 (100);
      v_user_type      NUMBER;
      v_title          t101_user.c101_title%TYPE;
      v_sales_repfl    VARCHAR2 (5)                            := 'false';
      v_edit_user_id   t703_sales_rep.c703_sales_rep_id%TYPE;
      v_territory_id   t703_sales_rep.C702_TERRITORY_ID%TYPE;
      v_actv_fl	       t703_sales_rep.c703_active_fl%TYPE;
      v_region_id      t701_distributor.c701_region%TYPE;
      v_division	   t1910_division.c1910_division_id%TYPE;
      v_input_str      CLOB;
      v_company_id 	   t1900_company.c1900_company_id%TYPE;
      v_first_name 	   t101_user.c101_user_f_name%TYPE;
      v_last_name      t101_user.c101_user_l_name%TYPE;
      v_auth_type      t102_user_login.c901_auth_type%TYPE;
      
      --PC-446: Redis auto refresh while change primary company id
      v_dist_company_id t1900_company.c1900_company_id%TYPE;
      v_log_out_msg   VARCHAR2 (4000);
      v_party_type	t101_party.c901_party_type%TYPE := 7006; --Party is globus employee
   BEGIN
	
IF(p_primary_comp_id IS NULL) THEN
   		SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL'))
          INTO v_company_id 
          FROM DUAL;
ELSE v_company_id:= p_primary_comp_id;
END IF;
           --If the user type is Vendor agent (109320), then the Authentication type is keycloak (109300) else ADS (320)
      		IF p_user_type = 109320 THEN
			 	v_auth_type:= 109300 ;
				v_party_type:= 26240605;  -- Vendor-Portal Map  
			ELSE
				v_auth_type:= 320;
			END IF;
      SELECT DECODE (p_title, '0', '', p_title)
        INTO v_title
        FROM DUAL;

      BEGIN
         --fetch old dept. ID
         SELECT c101_party_id, c701_distributor_id, c901_dept_id,
                c901_user_type,c101_user_f_name,c101_user_l_name
           INTO v_partyid, v_dist_id, v_dept_id,
                v_user_type,v_first_name,v_last_name
           FROM t101_user
          WHERE c101_user_id = p_userid AND c901_user_status = 311;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            v_dept_id := 0;
      END;

      UPDATE t101_user
         SET c101_user_sh_name = p_short_name,
         	 c101_user_f_name = p_first_name,
         	 c101_user_l_name = p_last_name,
             c101_access_level_id = p_access,
             C101_EMAIL_ID = trim(lower(p_email)),
             c901_dept_id = p_deptid,
             c101_work_phone = p_work_phone,
             c101_title = v_title,
             c101_last_updated_by = p_login_user,
             c101_last_updated_date = CURRENT_DATE,
             c901_user_type = p_user_type,
             c101_date_format = p_date_fmt--,
             --c101_dept_id = get_code_name (p_deptid) -- MNTTASK-5884-We are save the data c901_dept_id. No need this one.
       WHERE t101_user.c101_user_id = p_userid;
		
       -- If its a new user v_dept_id is 0, check first name or last name mismatch
       IF(v_dept_id<>0 AND (v_first_name <> p_first_name OR v_last_name <> p_last_name))
       THEN 
       -- update first name and last name in  T101_PARTY table
       		update t101_party set c101_first_nm=p_first_name,
       		c101_last_nm=p_last_name,
       		c101_last_updated_by=p_login_user,
       		c101_last_updated_date = CURRENT_DATE
       		WHERE c101_party_id = v_partyid;
       
       END IF;
       
      UPDATE t102_user_login
         SET C102_LOGIN_USERNAME = LOWER(p_user_login_name),
         c102_ext_access = NVL (p_ext_access, 'N'),
         c102_login_pin = p_login_pin,
             c102_last_updated_by = p_login_user,
             c102_last_updated_date = CURRENT_DATE
       WHERE c101_user_id = p_userid;

      v_user_id := p_userid;

      IF (SQL%ROWCOUNT = 0)
      THEN
         -- insert values in t101_user and t101_party

         -- for dept (Sales) and user type (Direct Sales Rep) in User edit screen
         IF p_deptid = '2005' AND p_user_type = '301'
         THEN
            SELECT s703_sales_rep.NEXTVAL
              INTO v_edit_user_id
              FROM DUAL;
         END IF;

         gm_pkg_it_login.gm_save_user (v_edit_user_id
--NULL -- New user id (if it's from user edit screen, get sequence from sales rep to make sure user id and rep id will be the same)
         ,
                                       p_short_name,
                                       p_first_name,
                                       p_last_name,
                                       p_email,
                                       p_deptid                     -- dept id
                                               ,
                                       p_access,
                                       p_login_user,
                                       311       -- Status Active for new user
                                          ,
                                       p_user_type,
                                       NULL                         --party_id
                                           ,
                                       v_party_type         
                                           ,
                                       NULL                       --p_activefl
                                           ,
                                       NULL                  --party_m_initial
                                           ,
                                       v_out_party_id,
                                       v_out_user_id
                                      );
         p_userid := v_out_user_id;
         -- insert user values in t102_user_login
         gm_pkg_it_login.gm_save_user_login (v_out_user_id,
                                             p_user_login_name,
                                             NVL (p_password,
                                                  'GOTOAD@745247@5'
                                                 ),
                                             v_auth_type                 --v_auth_type
                                             ,p_ldap_id
                                            );

         UPDATE t102_user_login
            SET c102_new_user_fl = 'Y'
                                      -- , c102_password_expired_fl = 'Y'    should not set expired fl as Y for AD user
         ,
                c102_ext_access = NVL (p_ext_access, 'N'),
                c102_last_updated_by = p_login_user,
                c102_last_updated_date = CURRENT_DATE
          WHERE c101_user_id = v_out_user_id;

         UPDATE t101_user
            SET c101_work_phone = p_work_phone,
                --c101_dept_id = get_code_name (p_deptid),
                c901_dept_id = p_deptid,
				C101_EMAIL_ID = trim(lower(p_email)),
                c101_title = v_title,
                c101_date_format = p_date_fmt,
                c101_last_updated_by = p_login_user,
                c101_last_updated_date = CURRENT_DATE
          WHERE t101_user.c101_user_id = v_out_user_id;
      ELSE
         SELECT c101_party_id, c701_distributor_id
           INTO v_partyid, v_dist_id
           FROM t101_user
          WHERE c101_user_id = p_userid AND c901_user_status = 311;

         UPDATE t101_party t101
            SET t101.c1500_group_id = p_deptid,
                t101.c101_last_updated_by = p_login_user,
                t101.c101_last_updated_date = CURRENT_DATE
          WHERE t101.c101_party_id = p_userid;

         gm_save_user_group_mapping (v_partyid, p_deptid, p_login_user);

         BEGIN
            SELECT c703_sales_rep_id
              INTO v_sales_rep_id
              FROM t703_sales_rep
             WHERE c101_party_id = v_partyid AND c703_void_fl IS NULL;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               v_sales_rep_id := 0;
         END;

         IF     v_dept_id <> p_deptid
            AND v_dept_id = '2005'
            AND v_user_type <> p_user_type
            AND v_user_type = '301'
         THEN
            UPDATE t701_distributor
               SET c701_void_fl = 'Y',
                   c701_active_fl = 'N',
                   c701_distributor_name =
                           'Inactive - ' || p_first_name || ' ' || p_last_name,
                   c701_last_updated_by = p_login_user,
                   c701_last_updated_date = CURRENT_DATE
             WHERE c701_distributor_id = v_dist_id AND c701_void_fl IS NULL;

            UPDATE t101_user
               SET c701_distributor_id = NULL,
                   c101_last_updated_by = p_login_user,
                   c101_last_updated_date = CURRENT_DATE
             WHERE c101_user_id = p_userid;

            v_dist_id := NULL;

            UPDATE t703_sales_rep
               SET c703_active_fl = 'N'
                                       -- I have commented void fl for MNT TASK-551 by velu. The records should display the active and InActive in Sales setup screen when change from active to inactive.
                                       --c703_void_fl = 'Y'
            ,
                   c703_sales_rep_name =
                           'Inactive - ' || p_first_name || ' ' || p_last_name,
                   c703_last_updated_by = p_login_user,
                   c703_last_updated_date = CURRENT_DATE
             WHERE c703_sales_rep_id = v_sales_rep_id AND c703_void_fl IS NULL;
             
            -- Update inactive login user in the T102_USER_LOGIN table
      		gm_pkg_it_user.gm_update_inactv_userloginname(p_userid,p_login_user);
             
         END IF;
      END IF;

      SELECT c101_party_id
        INTO v_partyid
        FROM t101_user
       WHERE c101_user_id = p_userid AND c901_user_status = 311;

      -- for dept (Sales) and user type (Direct Sales Rep) in User edit screen
	  -- PMT-51193: Agent Sales Rep to create the New user (to check the user type Agent and create the sales rep)
	  
      IF p_deptid = '2005' AND (p_user_type = '301' OR p_user_type = '302')
      THEN
         IF p_user_type = '301'
         THEN
            v_user_type := 4021;
		-- PMT-51193: Agent Sales Rep to create the New user	
		 ELSIF p_user_type = '302'
         THEN
         	v_user_type := 4020;	
         END IF;

         BEGIN
            SELECT c107_contact_id
              INTO v_contactid
              FROM t107_contact
             WHERE c101_party_id = v_partyid
               AND c901_mode = '90452'
               AND c107_void_fl IS NULL;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               v_contactid := '0';
         END;

         --    The User Edit screen create primary email id for login credentials.
         gm_pkg_cm_contact.gm_sav_contactinfo (v_contactid,
                                               v_partyid,
                                               '90452',
                                               'Business',
                                               p_email,
                                               NULL,
                                               'Y',
                                               NULL,
                                               p_login_user
                                              );

		-- PMT-51193: Agent Sales Rep to create the New user (to avoid agent sales - to create dist)
         IF v_dist_id IS NULL AND p_user_type = '301'
         THEN
            v_action := 'Add';
            -- If new direct sales rep created ,we get the region value from rule and insert in OUS.
              v_region_id  := NVL(get_rule_value('DIRECT_SALSREP_REGN','REGION'),4067);
              gm_save_distributor (p_first_name || ' ' || p_last_name    --  distnm
                                                                ,
                              p_first_name || ' ' || p_last_name    --  distnmen
                                                                ,
                              v_region_id                        --  region
                                  ,
                              p_first_name || ' ' || p_last_name --  distnames
                                                                ,
                              p_first_name                       --  conperson
                                          ,
                              'TBE'                                 --  billnm
                                   ,
                              NULL                                  --  shipnm
                                  ,
                              'TBE'                               --  billadd1
                                   ,
                              NULL                                --  shipadd1
                                  ,
                              NULL                                --  billadd2
                                  ,
                              NULL                                --  shipadd2
                                  ,
                              'TBE'                               --  billcity
                                   ,
                              NULL                                --  shipcity
                                  ,
                              1059                               --  billstate
                                  ,
                              NULL                               --  shipstate
                                  ,
                              1101                             --  billcountry
                                  ,
                              NULL                             --  shipcountry
                                  ,
                              'TBE'                                --  billzip
                                   ,
                              NULL                                 --  shipzip
                                  ,
                              NULL                                   --  phone
                                  ,
                              NULL                                     --  fax
                                  ,
                              CURRENT_DATE                            --  startdate
                                     ,
                              NULL                                 --  enddate
                                  ,
                              NULL                                --  comments
                                  ,
                              NULL                                --  activefl
                                  ,
                              v_dist_id                             --  distid
                                       ,
                              p_login_user                      --  created by
                                          ,
                              v_action                              --  action
                                      ,
                              NULL                                --  comnperc
                                  ,
                              70101                                --  disttyp
                                   ,
                              NULL                               --  partypric
                                  ,
                              NULL, -- FS Short Name    
                              NULL                               -- Commission Type
                                  ,
                             
                              v_message                            --  message
                                       ,
                              
                              v_out_dist_id,                        --  dist_id
                              
                               v_company_id                         --  Company Id
                              
                             );                                   
                             
         END IF;

         BEGIN
	         -- c702_territory_id is added because any update on user edit screen make the territory as null and then no sales will be visible for that rep.
	         -- c703_active_fl is added because even if he got inactivated, and if any change made from user edit screen will activate him again as per below procedure code.
            SELECT c703_sales_rep_id, c702_territory_id, c703_active_fl, c1910_division_id
              INTO v_sales_rep_id, v_territory_id, v_actv_fl, v_division
              FROM t703_sales_rep
             WHERE c101_party_id = v_partyid AND c703_void_fl IS NULL;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               v_sales_rep_id := p_userid;
               v_sales_repfl := 'true';
               v_territory_id := NULL;
               v_actv_fl := 'N';
               v_division := get_rule_value_by_company('DEFAULT_DIVISION','ONEPORTAL',v_company_id);
         END;
			
			-- To set the default division for the new direct sales rep
			IF v_division IS NULL THEN
			v_division := get_rule_value_by_company('DEFAULT_DIVISION','ONEPORTAL',v_company_id);
			END IF;
			
         /* IF v_sales_rep_id IS NULL THEN
               v_sales_rep_id := v_user_id;
            END IF; */
         
         IF v_out_dist_id is NULL THEN
			-- PMT-51193: Agent Sales Rep to create the New user (to handle the exception)
         	BEGIN
	         SELECT c701_DISTRIBUTOR_ID 
	         INTO v_out_dist_id
	         FROM T703_SALES_REP T703
	         WHERE T703.c703_sales_rep_id = v_sales_rep_id;
	         EXCEPTION WHEN NO_DATA_FOUND
	         THEN
			 -- to get the dummy distributor id from rules
				SELECT get_rule_value('AGENT_DIST_ID', 'AGENT_DIST')
				INTO v_out_dist_id
				FROM DUAL;				
	         END;
	         
         END IF;
         
         gm_save_salesrep_table (p_first_name,
                                 p_last_name,
                                 p_first_name || ' ' || p_last_name, -- Rep Name(En)
                                 v_out_dist_id,
                                 v_user_type                   -- Rep category
                                            ,
                                 v_sales_rep_id                -- sales rep id
                                               ,
                                 p_login_user,
                                 v_territory_id                --Territory Name
                                     ,
                                 v_actv_fl                     -- activefl
                                    ,
                                 v_partyid,
                                 NULL                          -- startdate -- When update the records in hiredate, should not update today date to sales rep.
                                        ,
                                 NULL                               -- enddate
                                     ,
                                 v_title                        --desgination,
                                        ,
                                 'USER_EDIT'                          --source
                                            ,
                                 v_division
                                 	  ,
                                
                                 v_out_user_id,
                                  v_company_id                    --Company Id
                                 
                                );
         
         -- Mapping the division to the sales rep in t703a_salesrep_div_mapping. 105442 - Primary
         v_input_str := v_division|| '^105442|';
         gm_pkg_sm_mapping.gm_sav_salesrep_div_map(v_input_str,v_sales_rep_id,p_login_user);
         
         IF (v_sales_repfl = 'true')
         THEN
            p_sales_rep_id := v_out_user_id;
         END IF;

         UPDATE t101_user
            SET c701_distributor_id = v_out_dist_id,
                c101_last_updated_by = p_login_user,
                c101_last_updated_date = CURRENT_DATE
          WHERE c101_user_id = p_userid;
          
          
		-- PC-446: While changes the primary company to update the distributor company details.
		IF p_user_type = '301' AND v_out_dist_id IS NOT NULL THEN
			--
		    BEGIN
		         SELECT c1900_company_id
		           INTO v_dist_company_id
		           FROM t701_distributor
		          WHERE c701_distributor_id = v_out_dist_id;
		    EXCEPTION
		    WHEN OTHERS THEN
		        v_dist_company_id := v_company_id;
		    END;
		    
		    -- compare primary company and dist company
		    
		    IF v_company_id <> v_dist_company_id THEN
		    	-- to update the company id
		         UPDATE t701_distributor
		        SET c1900_company_id        = v_company_id, 
		        c701_last_updated_by = p_login_user, 
		        c701_last_updated_date = CURRENT_DATE
		          WHERE c701_distributor_id = v_out_dist_id
		            AND c701_void_fl       IS NULL;
		            
		         -- to update the comments logger
		         -- ref id, comments, type, user id, out msg
		          gm_update_log  (v_out_dist_id, 'Distributor Company updated via User Profile screen - Old company id '|| v_dist_company_id, 1210, p_login_user, v_log_out_msg);
		          
		    END IF; -- company id compare
		    
		END IF; -- to update dist company id
          
          
      END IF;
   END gm_save_user;

/******************************************************************
    * Description : Procedure to user group mapping
    * Author : Kulanthaivelu
    ****************************************************************/
   PROCEDURE gm_save_user_group_mapping (
      p_partyid      IN   t101_user.c101_party_id%TYPE,
      p_deptid       IN   t101_user.c901_dept_id%TYPE,
      p_login_user   IN   t101_user.c101_last_updated_by%TYPE
   )
   AS
      v_count   NUMBER;

      CURSOR allocate_cur
      IS
         SELECT t1501.c1500_mapped_group_id map_grp_id
           FROM t1501_group_mapping t1501, t1500_group t1500
          WHERE t1500.c1500_group_nm = TO_CHAR (p_deptid)
            AND t1500.c901_group_type = 92263
            AND t1501.c1500_group_id = t1500.c1500_group_id
            AND t1501.c1501_void_fl IS NULL
            AND t1500.c1500_void_fl IS NULL;
   BEGIN
      -- Checking if the user is having any existing group_mapping record.
      SELECT COUNT (*)
        INTO v_count
        FROM t1501_group_mapping t1501, t1500_group t1500
       WHERE c101_mapped_party_id = p_partyid
         AND t1501.c1500_group_id = t1500.c1500_group_id
         AND t1500.c901_group_type = '92262'
         AND t1500.c1500_void_fl IS NULL
         AND t1501.c1501_void_fl IS NULL;

      -- If the group_mapping exists for this user, delete it and it will be recreated based on the new department he is moving to.
      IF v_count > 0
      THEN
         DELETE FROM t1501_group_mapping
               WHERE c1501_group_mapping_id IN (
                        SELECT c1501_group_mapping_id
                          FROM t1501_group_mapping t1501, t1500_group t1500
                         WHERE c101_mapped_party_id = p_partyid
                           AND t1501.c1500_group_id = t1500.c1500_group_id
                           AND t1500.c901_group_type = '92262');
      END IF;

      FOR allocate_req IN allocate_cur
      LOOP
         gm_save_group_mapping ('',
                                '',
                                allocate_req.map_grp_id,
                                p_partyid,
                                p_login_user
                               );
      END LOOP;
   END gm_save_user_group_mapping;

   /******************************************************************
    * Description : Procedure to save group mapping
    * Author : Kulanthaivelu
    ****************************************************************/
   PROCEDURE gm_save_group_mapping (
      p_group_mapping_id   IN   t1501_group_mapping.c1501_group_mapping_id%TYPE,
      p_mapped_group_id    IN   t1501_group_mapping.c1500_mapped_group_id%TYPE,
      p_group_id           IN   t1500_group.c1500_group_id%TYPE,
      p_partyid            IN   t101_user.c101_party_id%TYPE,
      p_login_user         IN   t101_user.c101_last_updated_by%TYPE
   )
   AS
   BEGIN
      UPDATE t1501_group_mapping
         SET c101_mapped_party_id = p_partyid,
             c1500_mapped_group_id = p_mapped_group_id,
             c1500_group_id = p_group_id,
             c1501_last_updated_by = p_login_user,
             c1501_last_updated_date = CURRENT_DATE
       WHERE c1501_group_mapping_id = p_group_mapping_id;

      IF SQL%ROWCOUNT = 0
      THEN
         INSERT INTO t1501_group_mapping
                     (c1501_group_mapping_id, c101_mapped_party_id,
                      c1500_mapped_group_id, c1500_group_id,
                      c1501_created_by, c1501_created_date
                     )
              VALUES (s1501_group_mapping.NEXTVAL, p_partyid,
                      p_mapped_group_id, p_group_id,
                      p_login_user, CURRENT_DATE
                     );
      END IF;
   END gm_save_group_mapping;

   /******************************************************************
    * Description : Procedure to InActive User
    * Author : gopinathan
    ****************************************************************/
   PROCEDURE gm_user_terminate (
      p_user_id        IN   t101_user.c101_user_id%TYPE,
      p_login_userid   IN   t102_user_login.c102_user_login_id%TYPE
   )
   AS
      v_party_id         t101_party.c101_party_id%TYPE;
      v_dept_id          t101_user.c901_dept_id%TYPE;
      v_title            t101_user.c101_title%TYPE;
      v_distributor_id   t701_distributor.c701_distributor_id%TYPE;
      v_usertype         t101_user.c901_user_type%TYPE;        --VARCHAR2; --
      v_user_mail        t101_user.c101_email_id%TYPE;
      v_to_mail          VARCHAR2 (100);
      v_subject          VARCHAR2 (100);
      v_mail_body        VARCHAR2 (200);
      v_user_full_name   VARCHAR2 (100);
      v_count            NUMBER;
      v_access_level     t101_user.C101_ACCESS_LEVEL_ID%TYPE;
      
   BEGIN
      SELECT c101_party_id, c901_dept_id, c101_title, c701_distributor_id,
             c901_user_type, c101_email_id,c101_access_level_id
        INTO v_party_id, v_dept_id, v_title, v_distributor_id,
             v_usertype, v_user_mail,v_access_level
        FROM t101_user
       WHERE c101_user_id = p_user_id;
	  
      
      UPDATE t101_user
         SET c901_user_status = 317,                          -- User InActive
             c101_last_updated_date = CURRENT_DATE,
             c101_last_updated_by = p_login_userid
       WHERE c101_user_id = p_user_id;

    -- Update inactive login user in the T102_USER_LOGIN table
      gm_pkg_it_user.gm_update_inactv_userloginname(p_user_id,p_login_userid);
    
      UPDATE t101_party
         SET c101_active_fl = 'N',
             c101_last_updated_by = p_login_userid,
             c101_last_updated_date = CURRENT_DATE
       WHERE c101_party_id = v_party_id;

      UPDATE t107_contact
         SET c107_inactive_fl = 'Y',
             c107_last_updated_by = p_login_userid,
             c107_last_updated_date = CURRENT_DATE
       WHERE c101_party_id = v_party_id AND c901_mode = '90452';

      UPDATE t106_address
         SET c106_inactive_fl = 'Y',
             c106_last_updated_by = p_login_userid,
             c106_last_updated_date = CURRENT_DATE
       WHERE c101_party_id = v_party_id;

      UPDATE t1501_group_mapping
         SET c1501_void_fl = 'Y',
             c1501_last_updated_by = p_login_userid,
             c1501_last_updated_date = CURRENT_DATE
       WHERE c101_mapped_party_id = v_party_id;
       
       UPDATE t1019_party_company_map
          SET c1019_void_fl = 'Y',
              c1019_last_updated_by  = p_login_userid,
		      c1019_last_updated_date = CURRENT_DATE
	   WHERE c101_party_id = v_party_id;      

      --Direct Sales Rep AND Agent Sales Rep/Distributor Rep
      IF (v_usertype = '301' OR v_usertype = '302')
      THEN
         UPDATE t703_sales_rep
            SET c703_active_fl = 'N',
                c703_last_updated_by = p_login_userid,
                c703_last_updated_date = CURRENT_DATE
          WHERE c101_party_id = v_party_id;          
      END IF;

      -- Direct Sales Rep
      IF (v_usertype = '301' )
      THEN
         UPDATE t701_distributor
            SET c701_active_fl = 'N',
                c701_last_updated_by = p_login_userid,
                c701_last_updated_date = CURRENT_DATE
          WHERE c701_distributor_id = v_distributor_id;
      END IF;
      
      --This code is added for PMT-5341 and this is called when a Assoc Rep is terminated mapping to from the account need to be romoved.
     gm_pkg_sm_mapping.gm_sav_rem_acct_map(NULL,NULL,v_party_id,p_login_userid);
     
      v_user_full_name := get_user_name (p_user_id);
      -- Send mail to dept. head and HR head
      v_to_mail :=
            get_rule_value ('2017', 'USREMAIL')
         || ','
         || get_rule_value (v_dept_id, 'USREMAIL');
      v_subject := get_rule_value ('SUBJECT', 'INACTEMAIL')
                   || v_user_full_name;
      v_mail_body :=
            'The User '
         || v_user_full_name
         || get_rule_value ('BODY', 'INACTEMAIL');
      gm_com_send_email_prc (v_to_mail, v_subject, v_mail_body);

      --if user email_id present in Rule table then Send mail to Business Team
      SELECT COUNT (*)
        INTO v_count
        FROM t906_rules
       WHERE c906_rule_grp_id = v_user_mail;

      IF v_count > 0
      THEN
         v_to_mail := get_rule_value ('BTMAIL', 'BTMAIL');
         v_subject := get_rule_value ('SUBJECT', 'BTMAIL')
                      || v_user_full_name;
         v_mail_body := get_rule_value ('BODY', 'BTMAIL') || v_user_mail;
         gm_com_send_email_prc (v_to_mail, v_subject, v_mail_body);
      END IF;
   END gm_user_terminate;

   /******************************************************************
   * Description : Procedure to RollBack User
   * Author : gopinathan
   ****************************************************************/
   PROCEDURE gm_user_rollback (
      p_user_id        IN   t101_user.c101_user_id%TYPE,
      p_login_userid   IN   t102_user_login.c102_user_login_id%TYPE,
      p_out            OUT  t102_user_login.C102_LOGIN_USERNAME%TYPE
   )
   AS
      v_party_id         t101_party.c101_party_id%TYPE;
      v_dept_id          t101_user.c901_dept_id%TYPE;
      v_title            t101_user.c101_title%TYPE;
      v_distributor_id   t701_distributor.c701_distributor_id%TYPE;
      v_usertype         t101_user.c901_user_type%TYPE;        --VARCHAR2; --
      v_to_mail          VARCHAR2 (100);
      v_subject          VARCHAR2 (100);
      v_mail_body        VARCHAR2 (200);
      v_user_full_name   VARCHAR2 (100);
      v_user_name        T102_USER_LOGIN.C102_LOGIN_USERNAME%TYPE;
   BEGIN
      SELECT c101_party_id, c901_dept_id, c101_title, c701_distributor_id,
             c901_user_type
        INTO v_party_id, v_dept_id, v_title, v_distributor_id,
             v_usertype
        FROM t101_user
       WHERE C101_USER_ID = p_user_id;
       
       -- Replace user login name as active user login name
       SELECT REPLACE(C102_LOGIN_USERNAME,'_'||p_user_id,'')
       INTO v_user_name
       FROM T102_USER_LOGIN
       WHERE c101_user_id = p_user_id;
       
       --update user login name as  active user name
     	 UPDATE t102_user_login set c102_login_username = LOWER(v_user_name),
		 -- When user is getting inactivated, the LDAP Master ID reference is lost. 
		 -- When the user is reactivated, the ldap master is still blank,hence we are setting it to 1 to denote GlobusLDAP as that's the LDAP auth for 95% of users.
     	 c906c_ldap_master_id = NVL(c906c_ldap_master_id,1),
     	 c901_auth_type = NVL(c901_auth_type,320),
     	 c102_last_updated_by = p_login_userid,
     	 c102_last_updated_date = CURRENT_DATE 
      	 WHERE c101_user_id = p_user_id;
       
       
       UPDATE t101_user
         SET c901_user_status = 311,                          -- active status
             c101_last_updated_date = CURRENT_DATE,
             c101_last_updated_by = p_login_userid
       WHERE c101_user_id = p_user_id;

      UPDATE t101_party
         SET c101_active_fl = 'Y',
             c101_last_updated_by = p_login_userid,
             c101_last_updated_date = CURRENT_DATE
       WHERE c101_party_id = v_party_id;

      UPDATE t107_contact
         SET c107_inactive_fl = 'N',
             c107_last_updated_by = p_login_userid,
             c107_last_updated_date = CURRENT_DATE
       WHERE c101_party_id = v_party_id AND c901_mode = '90452';

      UPDATE t106_address
         SET c106_inactive_fl = 'N',
             c106_last_updated_by = p_login_userid,
             c106_last_updated_date = CURRENT_DATE
       WHERE c101_party_id = v_party_id;

      UPDATE t1501_group_mapping
         SET c1501_void_fl = NULL,
             c1501_last_updated_by = p_login_userid,
             c1501_last_updated_date = CURRENT_DATE
       WHERE c101_mapped_party_id = v_party_id;
       
      UPDATE t1019_party_company_map
          SET c1019_void_fl = NULL,
              c1019_last_updated_by  = p_login_userid,
		      c1019_last_updated_date = CURRENT_DATE
	   WHERE c101_party_id = v_party_id;      

      --Direct Sales Rep AND Agent Sales Rep/Distributor Rep
      IF (v_usertype = '301' OR v_usertype = '302')
      THEN
      	 UPDATE t703_sales_rep
            SET c703_active_fl = 'Y',
                c703_void_fl = NULL,
                c703_last_updated_by = p_login_userid,
                c703_last_updated_date = CURRENT_DATE
          WHERE c101_party_id = v_party_id;
      END IF;
      
      -- Direct Sales Rep
      IF (v_usertype = '301')
      THEN       
       UPDATE t701_distributor
            SET c701_active_fl = 'Y',
                c701_void_fl = NULL,
                c701_last_updated_by = p_login_userid,
                c701_last_updated_date = CURRENT_DATE
          WHERE c701_distributor_id = v_distributor_id;
	  END IF;
	  
      v_user_full_name := get_user_name (p_user_id);
      -- Send mail to dept. head
      v_to_mail :=
            get_rule_value ('2017', 'USREMAIL')
         || ','
         || get_rule_value (v_dept_id, 'USREMAIL');
      v_subject := get_rule_value ('SUBJECT', 'RBEMAIL') || v_user_full_name;
      v_mail_body :=
         'The User ' || v_user_full_name || get_rule_value ('BODY', 'RBEMAIL');
      -- gm_com_send_email_prc (v_to_mail, v_subject, v_mail_body);
      p_out:=v_user_name;
   END gm_user_rollback;

/*************************************/
/****    delete user       ******/
/*************************************/
   PROCEDURE gm_delete_user (p_user_id IN t101_user.c101_user_id%TYPE)
   AS
      v_party_id         t101_party.c101_party_id%TYPE;
      v_user_login_id    t102_user_login.c102_user_login_id%TYPE;
      v_distributor_id   t701_distributor.c701_distributor_id%TYPE;
   BEGIN
      SELECT c101_party_id, c701_distributor_id
        INTO v_party_id, v_distributor_id
        FROM t101_user
       WHERE c101_user_id = p_user_id;

      SELECT c102_user_login_id
        INTO v_user_login_id
        FROM t102_user_login
       WHERE c101_user_id = p_user_id;

      DELETE FROM t1501_group_mapping
            WHERE c101_mapped_party_id = v_party_id;

      DELETE FROM t103_user_login_track
            WHERE c101_user_id = p_user_id;

      DELETE FROM t102a_user_password_history
            WHERE c102_user_login_id = v_user_login_id;

      DELETE FROM t102_user_login
            WHERE c101_user_id = p_user_id;

      DELETE FROM t101_user
            WHERE c101_user_id = p_user_id;

      DELETE FROM t703_sales_rep
            WHERE c101_party_id = v_party_id;

      DELETE FROM t107_contact
            WHERE c101_party_id = v_party_id AND c901_mode = '90452';

      DELETE FROM t101_party
            WHERE c101_party_id = v_party_id;

      DELETE FROM t701_distributor
            WHERE c701_distributor_id = v_distributor_id;
   END gm_delete_user;

--
/*
 * Description: Function to get the group description
 * Author: Rajkumar
 */
   FUNCTION "GET_GROUP_NM" (p_groupid IN t1500_group.c1500_group_id%TYPE)
      RETURN VARCHAR2
   IS
      v_group_name   t1500_group.c1500_group_nm%TYPE;
   BEGIN
      SELECT c1500_group_nm
        INTO v_group_name
        FROM t1500_group
       WHERE c1500_group_id = p_groupid;

      RETURN v_group_name;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN ' ';
   END get_group_nm;

--
/*
 * Description: Function to get the group description
 * Author: Rajkumar
 */
   FUNCTION "GET_GROUP_DESC" (p_groupid IN t1500_group.c1500_group_id%TYPE)
      RETURN VARCHAR2
   IS
      v_group_desc   t1500_group.c1500_group_desc%TYPE;
   BEGIN
      SELECT c1500_group_desc
        INTO v_group_desc
        FROM t1500_group
       WHERE c1500_group_id = p_groupid;

      RETURN v_group_desc;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         v_group_desc := ' ';
         RETURN v_group_desc;
   END get_group_desc;

--
/*
 * Description: Function to detect the user is mapped with any security group
 * Author:  Rajkumar
 */
   PROCEDURE gm_fch_user_sg_cnt (
      p_user_id   IN       t101_user.c101_user_id%TYPE,
      p_out_cnt   OUT      NUMBER
   )
   AS
      v_party_id       NUMBER;
      v_group_id       VARCHAR2 (20);
      v_dept_id        NUMBER;
      v_access_level   NUMBER;
   BEGIN
      -- Code modified to fetch right party id
      SELECT t101.c901_dept_id, t101.c101_access_level_id, t101.c101_party_id
        INTO v_dept_id, v_access_level, v_party_id
        FROM t101_user t101
       WHERE t101.c101_user_id = p_user_id;

      IF v_dept_id = 2005 AND (v_access_level <= 2 OR v_access_level = 7)
      THEN
         SELECT t101.c101_party_id
           INTO v_party_id
           FROM t101_party t101, t703_sales_rep t703
          WHERE t101.c101_party_id = t703.c101_party_id
            AND t703.c703_sales_rep_id = p_user_id;
      END IF;

      SELECT COUNT (1)
        INTO p_out_cnt
        FROM t1501_group_mapping t1501, t1500_group t1500
       WHERE t1500.c1500_group_id = t1501.c1500_group_id
         AND t1501.c101_mapped_party_id = v_party_id
         AND t1500.c901_group_type = 92264
         AND t1500.c1500_void_fl IS NULL
         AND t1501.c1501_void_fl IS NULL;
   END gm_fch_user_sg_cnt;

--
  /*
   * Description: Procedure to fetch the user groups, user is mapped with the security group and
   * security group with module
   */
   PROCEDURE gm_fch_modules (
      p_user_id   IN       t101_user.c101_user_id%TYPE,
      p_out       OUT      TYPES.cursor_type
   )
   AS
      v_party_id       NUMBER;
      v_dept_id        NUMBER;
      v_access_level   NUMBER;
   BEGIN
      SELECT t101.c901_dept_id, t101.c101_access_level_id, t101.c101_party_id
        INTO v_dept_id, v_access_level, v_party_id
        FROM t101_user t101
       WHERE t101.c101_user_id = p_user_id;

      BEGIN
         IF v_dept_id = 2005 AND (v_access_level <= 2 OR v_access_level = 7)
         THEN
            SELECT t101.c101_party_id
              INTO v_party_id
              FROM t101_party t101, t703_sales_rep t703
             WHERE t101.c101_party_id = t703.c101_party_id
               AND t703.c703_sales_rep_id = p_user_id;
         END IF;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            v_party_id := NULL;
      END;

      OPEN p_out
       FOR
          SELECT   t1500.c1500_group_id GROUP_ID,
                   t1500.c1500_group_nm group_nm, '92265' grptype
              FROM t1500_group t1500
             WHERE t1500.c901_group_type IN ('92265')
               AND t1500.c1500_void_fl IS NULL
          UNION
          SELECT   NVL (MAX (t1500.c1500_group_id), 0) GROUP_ID,
                   NVL (MAX (t1500.c1500_group_nm), 'My Favorites') group_nm,
                   '92266' grptype
              FROM t1500_group t1500, t1501_group_mapping t1501
             WHERE t1500.c901_group_type IN ('92266')
               AND t1500.c1500_group_id = t1501.c1500_group_id
               AND t1501.c101_mapped_party_id = v_party_id
               AND t1500.c1500_void_fl IS NULL
          ORDER BY  grptype desc,group_nm, GROUP_ID;

   END gm_fch_modules;

/******************************************************************
 * Description : Procedure to fetch the tasks that the logged in
 *             user has access.
 * Author      : Vprasath
 ****************************************************************/
   PROCEDURE gm_fch_user_task_access (
      p_user_id   IN       t101_user.c101_user_id%TYPE,
      p_out       OUT      TYPES.cursor_type
   )
   AS
      v_party_id       NUMBER;
      v_group_id       VARCHAR2 (20);
      v_dept_id        NUMBER;
      v_access_level   NUMBER;
      v_company_id     NUMBER;
   BEGIN
      SELECT t101.c901_dept_id, t101.c101_access_level_id, t101.c101_party_id
        INTO v_dept_id, v_access_level, v_party_id
        FROM t101_user t101
       WHERE t101.c101_user_id = p_user_id;
       
	 /*Based on the company selected in context, it will load the left menu details*/
	 SELECT get_compid_frm_cntx()
        INTO v_company_id
        FROM DUAL;
	 
	  BEGIN
         IF v_dept_id = 2005 AND (v_access_level <= 2 OR v_access_level = 7)
         THEN
            SELECT t101.c101_party_id, t101.c1500_group_id
              INTO v_party_id, v_group_id
              FROM t101_party t101, t703_sales_rep t703
             WHERE t101.c101_party_id = t703.c101_party_id
               AND t703.c703_sales_rep_id = p_user_id;
         ELSE
            SELECT t101p.c1500_group_id
              INTO v_group_id
              FROM t101_party t101p
             WHERE t101p.c101_party_id = v_party_id;
         END IF;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            v_party_id := NULL;
            v_group_id := NULL;
      END;

      OPEN p_out
       FOR
          SELECT t1530.c1520_function_id fn_id,
                 c1530_update_access_fl upd_acc, c1530_read_access_fl rd_acc,
                 t1530.c1530_void_access_fl vd_acc
            FROM t1500_group t1500,
                 t1501_group_mapping t1501,
                 t1530_access t1530
           WHERE t1500.c1500_group_id = t1501.c1500_group_id
             AND t1530.c1500_group_id = t1500.c1500_group_id
             AND t1501.c101_mapped_party_id = v_party_id
             AND t1501.c1900_company_id = v_company_id
             AND t1500.c901_group_type = 92264
             AND t1500.c1500_void_fl IS NULL
             AND t1501.c1501_void_fl IS NULL
             AND t1530.c1530_void_fl IS NULL;
   END gm_fch_user_task_access;

/*
   * Description: Procedure to remember the logged in user
   * Author     : VPrasath
   */
   PROCEDURE gm_sav_remember_user (
      p_user_id     IN   VARCHAR2,
      p_password    IN   VARCHAR2,
      p_cookie_id   IN   VARCHAR2
   )
   AS
   BEGIN
      UPDATE t101c_remember_user t101c
         SET c101c_void_fl = 'Y',
             c101c_last_updated_date = CURRENT_DATE
       WHERE c101c_user_nm = p_user_id;

      INSERT INTO t101c_remember_user
                  (c101c_user_nm, c101c_password, c101c_cookie_id,
                   c101c_created_date
                  )
           VALUES (p_user_id, p_password, p_cookie_id,
                   CURRENT_DATE
                  );
   END gm_sav_remember_user;

   /*
    * Description: Procedure to fetch the credentials for the saved user
    * Author     : VPrasath
    */
   PROCEDURE gm_fch_saved_user_crdn (
      p_cookie_id   IN       VARCHAR2,
      p_out         OUT      TYPES.cursor_type
   )
   AS
   BEGIN
      OPEN p_out
       FOR
          SELECT c101c_user_nm usernm, c101c_password passwd
            FROM t101c_remember_user
           WHERE c101c_cookie_id = p_cookie_id AND c101c_void_fl IS NULL;
   END gm_fch_saved_user_crdn;

   /*
     * Description: Procedure to forgot the saved user
     * Author     : VPrasath
     */
   PROCEDURE gm_sav_forgot_user (p_user_nm IN VARCHAR2)
   AS
   BEGIN
      UPDATE t101c_remember_user t101c
         SET c101c_void_fl = 'Y',
             c101c_last_updated_date = CURRENT_DATE
       WHERE c101c_user_nm = p_user_nm;
   END gm_sav_forgot_user;

   /*
    *  Description: Procedure to Inactive users
    *               that are inactivated in AD
    *  Author: VPrasath
    */
   PROCEDURE gm_sav_inactive_user (
      p_user_id   IN   t102_user_login.c101_user_id%TYPE
   )
   AS
      v_party_id         t101_party.c101_party_id%TYPE;
      v_title            t101_user.c101_title%TYPE;
      v_distributor_id   t701_distributor.c701_distributor_id%TYPE;
      v_usertype         t101_user.c901_user_type%TYPE;
   BEGIN
	   -- get the user information
	   SELECT c101_party_id,  c101_title, c701_distributor_id,
             c901_user_type
        INTO v_party_id,  v_title, v_distributor_id,
             v_usertype
        FROM t101_user
       WHERE c101_user_id = p_user_id;
       
      UPDATE t101_user
         SET c901_user_status = 317,         	 -- User InActive
             c101_last_updated_by = 30301,
             c101_last_updated_date = CURRENT_DATE
       WHERE c101_user_id = p_user_id;
       
      UPDATE t102_user_login
         SET C901_AUTH_TYPE = null,
             C102_EXT_ACCESS = null,
             C102_USER_LOCK_FL = 'Y',
             c102_last_updated_by = 30301,
             c102_last_updated_date = CURRENT_DATE
       WHERE c101_user_id = p_user_id;
    
      -- to update the party is inactive.    
      UPDATE t101_party
         SET c101_active_fl = 'N',
             c101_last_updated_by = 30301,
             c101_last_updated_date = CURRENT_DATE
       WHERE c101_party_id = v_party_id;
     -- to update the contact inactive.
      UPDATE t107_contact
         SET c107_inactive_fl = 'Y',
             c107_last_updated_by = 30301,
             c107_last_updated_date = CURRENT_DATE
       WHERE c101_party_id = v_party_id AND c901_mode = '90452';
      -- to update the address - inactive.
      UPDATE t106_address
         SET c106_inactive_fl = 'Y',
             c106_last_updated_by = 30301,
             c106_last_updated_date = CURRENT_DATE
       WHERE c101_party_id = v_party_id;
      -- to void the group mapping reports.
      UPDATE t1501_group_mapping
         SET c1501_void_fl = 'Y',
             c1501_last_updated_by = 30301,
             c1501_last_updated_date = CURRENT_DATE
       WHERE c101_mapped_party_id = v_party_id;
	
	  UPDATE t1019_party_company_map
          SET c1019_void_fl = 'Y',
              c1019_last_updated_by  = 30301,
		      c1019_last_updated_date = CURRENT_DATE
	   WHERE c101_party_id = v_party_id;	  
	   	
      --Check the user is Direct Sales Rep OR Agent Sales Rep/Distributor Rep
      IF (v_usertype = '301' OR v_usertype = '302')
      THEN
         UPDATE t703_sales_rep
            SET c703_active_fl = 'N',
                c703_last_updated_by = 30301,
                c703_last_updated_date = CURRENT_DATE
          WHERE c101_party_id = v_party_id;
      END IF;
      
      -- Check if is Direct Sales Rep
      IF(v_usertype = '301')
      THEN
         UPDATE t701_distributor
            SET c701_active_fl = 'N',
                c701_last_updated_by = 30301,
                c701_last_updated_date = CURRENT_DATE
          WHERE c701_distributor_id = v_distributor_id;
      END IF; -- end if v_usertype
      
 	-- Update inactive login user in the T102_USER_LOGIN table
    gm_pkg_it_user.gm_update_inactv_userloginname(p_user_id,'30301');
	-- inactive the (IPAD) token
	-- 103102 - Expired
	 UPDATE t101a_user_token
	SET c901_status      = 103102, c101a_last_updated_by = 30301, c101a_last_updated_date = CURRENT_DATE
	  WHERE c101_user_id = p_user_id;
   END gm_sav_inactive_user;

   /*
    *  Description: Procedure to fetch
    *               active users
    *  Author: VPrasath
    */
   PROCEDURE gm_fch_active_users (
     p_ldap_id IN t102_user_login.c906c_ldap_master_id%TYPE,
     p_out OUT TYPES.cursor_type)
   AS
   BEGIN
      OPEN p_out
       FOR
          SELECT t101.c101_user_id userid, c102_login_username username
            FROM t102_user_login t102, t101_user t101
           WHERE t101.c101_user_id = t102.c101_user_id
             AND t101.c901_user_status = 311
             AND t102.c901_auth_type = 320
             AND t102.c906c_ldap_master_id = p_ldap_id;
   END gm_fch_active_users;
/***************************************************
 * Description : Function to get default group name
 * Author      : DhanaReddy
 ****************************************************/
FUNCTION get_default_group_nm (
        p_user_id IN t101_user.c101_user_id%TYPE)
    RETURN VARCHAR2
IS
    v_default_grp_name VARCHAR2 (200) ;
    v_group_id t101_party.c1500_group_id%TYPE;
BEGIN
    BEGIN
         SELECT t101p.c1500_group_id
           INTO v_group_id
           FROM t101_user t101, t101_party t101p
          WHERE t101p.c101_party_id = t101.c101_party_id
            AND t101.c101_user_id   = p_user_id;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_group_id := NULL;
    END;
    v_default_grp_name := gm_pkg_it_user.GET_GROUP_NM (v_group_id) ;
    RETURN v_default_grp_name;
END get_default_group_nm;

/**************************************************************************************************
    * Description : Procedure to update inactive login user
    * Author : Kulanthaivelu
 **************************************************************************************************/

PROCEDURE gm_update_inactv_userloginname (
 	p_userid            IN t101_user.c101_user_id%TYPE
   ,p_login_user        IN t101_user.c101_last_updated_by%TYPE
)
AS

v_user_loginname  VARCHAR2 (50);
v_index NUMBER;
v_login_name VARCHAR2 (50);
v_extra NUMBER;
BEGIN
	BEGIN
		SELECT c102_login_username INTO v_user_loginname 
    	FROM t102_user_login 
    	WHERE c101_user_id = p_userid;
    EXCEPTION
    	WHEN NO_DATA_FOUND THEN	
    		v_user_loginname := NULL;
    END;
    
    -- If the loginname already having the userid appended, we should not append it again.
   	SELECT INSTR(v_user_loginname,p_userid) INTO v_index FROM DUAL;
    IF v_index >0
    THEN
    	v_user_loginname := NULL;
    END IF;
   	
    IF v_user_loginname IS NOT NULL
      THEN
        v_login_name := v_user_loginname ||'_' || p_userid;      
	    -- Length of c102_login_username is 20 (VARCHAR2(20))
	    -- Truncate the v_user_loginname if the length of v_login_name is greater than 20        
	    IF LENGTH (v_login_name) > 20 THEN 
	            v_extra := LENGTH(v_login_name) - 20; 
	      		v_user_loginname := SUBSTR(v_user_loginname,0,LENGTH(v_user_loginname)-v_extra);           
	    END IF;

       UPDATE t102_user_login
         SET c102_login_username = v_user_loginname||'_'||p_userid
         	 ,c102_last_updated_by = p_login_user
             ,c102_last_updated_date = CURRENT_DATE
       WHERE c101_user_id = p_userid;  
    END IF;
--
END gm_update_inactv_userloginname;
/***************************************************
    * Description : Function to get default company for the given party
    * Author      : rshah
    ****************************************************/
   FUNCTION get_default_party_company (
        p_partyid  IN t101_party.c101_party_id%TYPE)
    RETURN NUMBER
    IS
    v_compid NUMBER;
    BEGIN
    	SELECT c1900_company_id
    	  INTO v_compid
    	  FROM T1019_PARTY_COMPANY_MAP
    	 WHERE c101_party_id = p_partyid
    	   AND c901_party_mapping_type = '105400'   --party mapping type is primary
    	   AND c1019_void_fl IS NULL;
    	RETURN v_compid;   
    EXCEPTION 	
    WHEN NO_DATA_FOUND THEN
      RETURN 0;    
    END get_default_party_company;
/***************************************************
    * Description : Function to get default Group  for the given party
    * Author      : Manoj
    ****************************************************/
  FUNCTION get_Default_group (
           p_partyid  IN t101_party.c101_party_id%TYPE)
    RETURN t1530_access.c1500_group_id%TYPE
    IS
    v_group varchar2(200);
    BEGIN
    	
	    SELECT C1500_GROUP_ID
    	  INTO v_group
    	  FROM T101_PARTY
    	 WHERE c101_party_id = p_partyid
    	   AND C101_VOID_FL IS NULL;
    	      
    	RETURN v_group;   
    EXCEPTION 	
    WHEN NO_DATA_FOUND THEN
      RETURN 0;    
    
      END get_Default_group;	
 /*************************************************************************************
 * Description	:  This Procedure will validate user login name already exists			   
 * Author		:  Mahavishnu
*************************************************************************************/
      PROCEDURE gm_validate_user_name (
      p_user_name IN     t102_user_login.C102_LOGIN_USERNAME%TYPE,
      p_user_cnt   OUT    NUMBER)
   AS
   v_usercnt NUMBER;
   BEGIN
	   
	SELECT COUNT (1) INTO v_usercnt FROM t102_user_login WHERE LOWER(c102_login_username) = LOWER(p_user_name);
   	p_user_cnt := v_usercnt;
	
   END gm_validate_user_name;    
   
 /*************************************************************************************
 * Description	: This Procedure used to get security group details for existing user 
 				  and save table t1501 and also update c101_copy_access_user_id column 		   
 * Author		: pvigneshwaran 
*************************************************************************************/ 
    PROCEDURE gm_copy_access_grp (
        p_to_user_name        IN   t102_user_login.c102_login_username%TYPE,
        p_from_user_name    IN   t102_user_login.c102_login_username%TYPE,
        p_update_user_id   IN   t101_user.c101_user_id%TYPE
    ) AS

        v_to_user_id      t101_user.c101_user_id%TYPE;
        v_to_party_id     t101_user.c101_party_id%TYPE;
        v_from_user_id    t101_user.c101_user_id%TYPE;
        v_from_party_id   t101_user.c101_party_id%TYPE;
        v_grp_id          CLOB;
        
        
        --Cursor Used to fetch security groupdetails for copy access user id
        CURSOR company_id_cur IS
        SELECT
            t1501.c1900_company_id   companyid
        FROM
            t1501_group_mapping   t1501,
            t1500_group           t1500
        WHERE
            t1500.c1500_group_id = t1501.c1500_group_id
            AND t1501.c101_mapped_party_id = v_from_party_id
            AND t1501.c1501_void_fl IS NULL
            AND t1500.c1500_void_fl IS NULL
            AND t1501.C1900_COMPANY_ID IS NOT NULL
            AND t1500.c901_group_type = 92264 --Security Group
        GROUP BY  t1501.c1900_company_id;

    BEGIN
      
	    BEGIN
	    -- Get Userid and partyid based on user name 
        SELECT t101.c101_user_id,t101.c101_party_id
        INTO   v_to_user_id,v_to_party_id
        FROM   t101_user t101,t102_user_login   t102
        WHERE  t102.c101_user_id = t101.c101_user_id
         AND   lower(t102.c102_login_username) = lower(p_to_user_name);
        EXCEPTION WHEN OTHERS
	    THEN 
	    	v_to_user_id := NULL;
	    	v_to_party_id := NULL;
	    END;
	    
	    BEGIN
		-- Get Userid and partyid based on user name 
        SELECT t101.c101_user_id,t101.c101_party_id
        INTO   v_from_user_id,v_from_party_id
        FROM   t101_user t101,t102_user_login   t102
        WHERE  t102.c101_user_id = t101.c101_user_id
         AND   lower(t102.c102_login_username) = lower(p_from_user_name);
		EXCEPTION WHEN OTHERS
	    THEN 
	    	v_from_user_id := NULL;
	    	v_from_party_id := NULL;
	    END;
	    
	    IF v_from_user_id IS NOT NULL THEN
     	--Loop the cursor based on security groups based on copyuser
        FOR company_id IN company_id_cur 
        LOOP 
        	SELECT rtrim(xmlagg(xmlelement(e,t1501.c1500_group_id || ',')).extract('//text()'), ',') || ','
        	  INTO v_grp_id
              FROM t1501_group_mapping   t1501,t1500_group t1500
             WHERE t1500.c1500_group_id = t1501.c1500_group_id
              AND t1501.c101_mapped_party_id = v_from_party_id
              AND t1501.c1501_void_fl IS NULL
              AND t1500.c1500_void_fl IS NULL
              AND t1500.c901_group_type = 92264
              AND t1501.c1900_company_id = company_id.companyid;
			  --
        gm_pkg_cm_accesscontrol.gm_upd_user_group_map(v_grp_id,NULL,v_to_party_id, p_update_user_id, company_id.companyid);
        END LOOP;

        gm_pkg_it_user.gm_upd_copy_user_id(v_to_user_id, v_from_user_id, p_update_user_id);
        
        END IF;
        
    END gm_copy_access_grp;
 
 /*************************************************************************************
 * Description	: This Procedure used to update c101_copy_access_user_id column in 
 				  t102Table		   
 * Author		: pvigneshwaran 
*************************************************************************************/  

    PROCEDURE gm_upd_copy_user_id (
        p_user_id          IN   t101_user.c101_user_id%TYPE,
        p_copy_user_id     IN   t101_user.c101_user_id%TYPE,
        p_update_user_id   IN   t101_user.c101_user_id%TYPE
    ) AS
    BEGIN
	    
	    --This Procedure used to copy access user id in t102 user table.
        UPDATE t102_user_login
        SET
            c101_copy_access_user_id = p_copy_user_id,
            c102_last_updated_by = p_update_user_id,
            c102_last_updated_date = current_date
        WHERE
            c101_user_id = p_user_id;

    END gm_upd_copy_user_id;

   /*
    * Description: Procedure to fetch the micro app list with Authorization flag based on the SECURITYGRP in which the user is mapped to it.
	* Any new Micro App should have an entry for the Rule Group ID MICRO_APP_SEC_GRP
	* Every Micro App should have a security group created  which is assigned as a Rule ID for the respective Micro Apps in Rule Table
	* In this Procedure we are checking and returning access flag if any of the Rule ID is mapped in for the logged in user in t1501_group_mapping table 
	* Rule Descrption: The Rule Description text is being displayed in the Globusone-spineIT App list.
	* Author     : JBalaraman/Gpalani
    */
   PROCEDURE gm_fch_micro_app_user (
      p_user_id   	IN       t101_user.c101_user_id%TYPE,
      p_out         OUT      TYPES.cursor_type
   )
   AS
   BEGIN
      OPEN p_out
       FOR
       SELECT
			RULE.rulevalue,
			RULE.ruleid,
            RULE.ruledesc,
			DECODE(GROUP_MAPPING.C1500_GROUP_ID,'','N','Y') accesslevel
		FROM (
			SELECT t906.c906_rule_value rulevalue,
			  t906.c906_rule_id ruleid,
              t906.c906_rule_desc ruledesc
			FROM t906_rules t906
			WHERE  t906.c906_rule_grp_id = 'MICRO_APP_SEC_GRP'
			 AND    t906.c906_void_fl IS NULL
			) RULE,
			(SELECT 
			T1500.C1500_GROUP_ID, t1500.c1500_group_nm
			FROM 
			  t1500_group t1500,
			  t1501_group_mapping t1501 
			WHERE NVL(t1500.c1500_group_id,'-999') = NVL(t1501.c1500_group_id,'-999')
			AND t1501.c101_mapped_party_id (+)     = p_user_id
			AND t1500.C1500_VOID_FL IS NULL
			AND T1501.C1501_VOID_FL IS NULL
			GROUP BY T1500.C1500_GROUP_ID, t1500.c1500_group_nm
			) GROUP_MAPPING
		WHERE RULE.ruleid  =GROUP_MAPPING.c1500_group_nm (+)
		 ORDER BY RULE.ruledesc ASC;
   END gm_fch_micro_app_user;


   /*
    * Description: Procedure to update or insert the rule value for Globus Apps 
    * Author     : gpalani
    */
   PROCEDURE gm_update_globus_app (
      p_rule_id  	IN       T906_RULES.C906_RULE_ID%TYPE,
	  p_rule_desc	IN	     T906_RULES.C906_RULE_DESC%TYPE,
	  p_rule_value  IN       T906_RULES.C906_RULE_VALUE%TYPE,
	  p_rule_grp_id IN       T906_RULES.C906_RULE_GRP_ID%TYPE,
	  p_user_id     IN	     T906_RULES.C906_CREATED_BY%TYPE,
  	  p_void_fl		IN 		 T906_RULES.C906_VOID_FL%TYPE

	  )
	  
	 AS
   BEGIN
	
	 UPDATE T906_RULES 
	 SET 
	  C906_RULE_ID=p_rule_id,
	  C906_RULE_DESC=p_rule_desc,
	  C906_RULE_VALUE=p_rule_value,
	  C906_VOID_FL=p_void_fl,	
	  C906_LAST_UPDATED_BY=p_user_id,
	  C906_LAST_UPDATED_DATE=CURRENT_DATE	  
	 WHERE C906_RULE_ID=p_rule_id
	  AND C906_RULE_GRP_ID=p_rule_grp_id
	  AND C906_VOID_FL IS NULL;
	 
	  IF (SQL%ROWCOUNT = 0) THEN
		
		INSERT INTO T906_RULES
			(C906_RULE_SEQ_ID,C906_RULE_ID,C906_RULE_DESC,C906_RULE_VALUE,
			  C906_RULE_GRP_ID,C906_CREATED_BY,C906_CREATED_DATE)
		VALUES
		(
		  s906_rule.nextval,p_rule_id,p_rule_desc,p_rule_value,p_rule_grp_id
		  ,p_user_id,CURRENT_DATE
		);
		
     END IF;
	  
   END gm_update_globus_app;
   
END gm_pkg_it_user;
/
