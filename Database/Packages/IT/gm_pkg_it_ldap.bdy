CREATE OR REPLACE
PACKAGE BODY gm_pkg_it_ldap
IS
    --
    /*******************************************************
    * Description : This procedure used to fetch LDAP master details
    * Author   :
    *******************************************************/
PROCEDURE gm_fch_ldap_dtls (
        p_ldap_id   IN t906c_ldap_master.c906c_ldap_master_id%TYPE,
        p_active_fl IN VARCHAR2,
        p_out_ldap_dtls OUT TYPES.cursor_type)
AS
BEGIN
    OPEN p_out_ldap_dtls FOR SELECT c906c_ldap_master_id ID, c906c_ldap_name NAME
	   FROM t906c_ldap_master
	  WHERE c906c_ldap_master_id                              = NVL (p_ldap_id, c906c_ldap_master_id)
	    AND DECODE (p_active_fl, 'Y', c906c_active_fl, '-99') = NVL (p_active_fl, '-99')
	    AND c906c_void_fl                                    IS NULL
	ORDER BY c906c_ldap_master_id;
END gm_fch_ldap_dtls;
/*******************************************************
* Description : This Function used to fetch LDAP id based on User name
* Author   :
*******************************************************/
FUNCTION get_ldap_id_from_user_name (
        p_user_name IN t102_user_login.c102_login_username%TYPE)
    RETURN VARCHAR2
IS
    v_ldap_id VARCHAR2 (20) ;
BEGIN
    BEGIN
         SELECT c906c_ldap_master_id id
           INTO v_ldap_id
           FROM t102_user_login
          WHERE UPPER (c102_login_username) = UPPER (TRIM (p_user_name)) ;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_ldap_id := NULL;
    END;
    RETURN v_ldap_id;
END get_ldap_id_from_user_name;
/*******************************************************
* Description : This Function used to fetch LDAP id based on User ID
* Author   :
*******************************************************/
FUNCTION get_ldap_id_from_user_id (
        p_user_id IN t102_user_login.c101_user_id%TYPE)
    RETURN VARCHAR2
IS
    v_ldap_id VARCHAR2 (20) ;
BEGIN
    BEGIN
         SELECT c906c_ldap_master_id id
           INTO v_ldap_id
           FROM t102_user_login
          WHERE c101_user_id = p_user_id;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_ldap_id := get_rule_value ('DB_AUTH', 'LDAP') ;
    END;
    RETURN v_ldap_id;
END get_ldap_id_from_user_id;
--
/*******************************************************
* Description : This Procedrue used to fetch the user details from DB
* Author   :
*******************************************************/
PROCEDURE gm_fch_db_user_ldap_id (
        p_user_name IN t102_user_login.c102_login_username%TYPE,
        p_user_pass IN t102_user_login.c102_user_password%TYPE,
        p_out_ldap_dtls OUT TYPES.cursor_type)
AS
BEGIN
    OPEN p_out_ldap_dtls FOR SELECT c906c_ldap_master_id ldapid,
    c101_user_id userid FROM t102_user_login WHERE lower (c102_login_username) = lower (p_user_name) AND
    c102_user_password                                                         = p_user_pass;
END gm_fch_db_user_ldap_id;
END gm_pkg_it_ldap;
/