--@"C:\database\Packages\it\GM_PKG_IT_ASSET.bdy";
CREATE OR REPLACE
PACKAGE BODY GM_PKG_IT_ASSET IS
/*************************************************************************
*
* CHECK AND LOAD OF ASSET ATTRIBUTE
*
*************************************************************************/
PROCEDURE GM_CHK_ASS_ATT(
	p_assetid IN t222_asset_attribute.c221_asset_id%TYPE,
	p_attributetype  IN t222_asset_attribute.c901_attribute_type%TYPE,
        p_data OUT Types.cursor_type)
  AS
v_void_fl		CHAR(1);
v_asset_id t222_asset_attribute.c221_asset_id%TYPE;
BEGIN
	-- to check Asset Master Table
    BEGIN
	SELECT c221_asset_id, c221_void_fl
	INTO v_asset_id, v_void_fl
	FROM t221_asset_master
	WHERE c221_asset_id = p_assetid;
     EXCEPTION
          WHEN NO_DATA_FOUND THEN
      		raise_application_error ('-20282', '');
     END;
     
     IF v_void_fl IS NOT NULL THEN
	raise_application_error('-20283', '');
     END IF;

	OPEN  p_data 
	FOR

	SELECT c221_asset_id ASSETID,
		c222_asset_attrib_seq_id ATTID,
		NVL(c222_attribute_value,'') ATTRIBUTEVALUE,
		NVL(c222_active_fl,'') ACTIVEFLAG,
		c901_attribute_type ATTTYP	
   	FROM t222_asset_attribute
   	WHERE c221_asset_id=p_assetid
  	AND c901_attribute_type=p_attributetype;

END GM_CHK_ASS_ATT;
/*************************************************************************
*
* SAVE AND UPDATE OF ASSET ATTRIBUTE
*
*************************************************************************/

PROCEDURE GM_SAV_ASS_ATT(
	p_assetid IN t222_asset_attribute.c221_asset_id%TYPE,
	p_attributetype  IN t222_asset_attribute.c901_attribute_type%TYPE,
	p_attvalue IN t222_asset_attribute.c222_attribute_value%TYPE,
        p_actflag IN t222_asset_attribute.c222_active_fl%TYPE,
        p_userid IN t222_asset_attribute.c222_created_by%TYPE,
        p_attid IN OUT VARCHAR2) 
  AS
  v_attid   NUMBER;
  
   BEGIN
     
	UPDATE t222_asset_attribute
        SET c221_asset_id=p_assetid,
        	c901_attribute_type=p_attributetype,
		c222_attribute_value=p_attvalue,
		c222_active_fl=p_actflag,
		c222_last_updated_by = p_userid,
		c222_last_updated_date = SYSDATE
        WHERE c222_asset_attrib_seq_id = p_attid;

 
       IF (SQL%ROWCOUNT = 0)    THEN 
   		
		SELECT S222_attrib_seq.NEXTVAL INTO v_attid FROM DUAL;
    
   		INSERT INTO t222_asset_attribute(
			c222_asset_attrib_seq_id,
			c221_asset_id,
			c901_attribute_type,
			c222_attribute_value,
			c222_active_fl,
			c222_created_by,
			c222_created_date)
    		VALUES(
			v_attid,
			p_assetid,
			p_attributetype,
			p_attvalue,
			p_actflag,
			p_userid,
			SYSDATE);
   		p_attid:=TO_CHAR (v_attid);
     END IF;
  
  END GM_SAV_ASS_ATT;

/*************************************************************************
*
* TO VOID ASSET ATTRIBUTE
*
*************************************************************************/
PROCEDURE gm_ac_void_asset_att(
		p_txn_id IN t907_cancel_log.c907_ref_id%TYPE,
		p_userid IN t102_user_login.c102_user_login_id%TYPE)
  AS
	v_void_fl t222_asset_attribute.c222_void_fl%TYPE;
BEGIN

	UPDATE t222_asset_attribute
			   SET c222_void_fl = 'Y',
				c222_last_updated_by = p_userid,
				c222_last_updated_date = SYSDATE
			 WHERE  c222_asset_attrib_seq_id = p_txn_id;

  END gm_ac_void_asset_att;


/*************************************************************************
*
* TO FETCH ASSET ATTRIBUTE
*
*************************************************************************/

PROCEDURE GM_FTCH_ASS_ATT(
	p_attid IN t222_asset_attribute.c222_asset_attrib_seq_id%TYPE,
	p_data OUT Types.cursor_type)
AS

BEGIN

OPEN  p_data 
	FOR

	SELECT c221_asset_id ASSETID,
		c222_asset_attrib_seq_id ATTID,
		c222_attribute_value ATTRIBUTEVALUE,
		c222_active_fl ACTIVEFLAG,
		c901_attribute_type ATTTYP	
   	FROM t222_asset_attribute
   	WHERE c222_asset_attrib_seq_id = p_attid;
END GM_FTCH_ASS_ATT;


PROCEDURE GM_FTCH_ASS_ATT_MAIL(p_data OUT Types.cursor_type)
AS

BEGIN

	OPEN  p_data 
	FOR

	SELECT c221_asset_id ASSETID,
		c222_asset_attrib_seq_id ATTID,
		c222_attribute_value ATTRIBUTEVALUE,
		c222_active_fl ACTIVEFLAG,
		c901_attribute_type ATTTYP	
   	FROM t222_asset_attribute;
END GM_FTCH_ASS_ATT_MAIL;
/*
 * Asset Master procedures by Velu
 */
PROCEDURE GM_AC_SAVE_ASSDETAILS
  (
    p_asset_id  IN OUT t221_asset_master.C221_ASSET_ID% TYPE,
  p_asset_name  IN t221_asset_master.C221_ASSET_NM%TYPE,
  p_asset_desc  IN t221_asset_master.C221_ASSET_DESC%TYPE,
  p_active_flag IN t221_asset_master.C221_ACTIVE_FL%TYPE,
  p_user_id     IN t221_asset_master.C221_CREATED_BY%TYPE
  )
AS
BEGIN
  UPDATE t221_asset_master
    SET c221_asset_nm        = p_asset_name,
      c221_asset_desc        = p_asset_desc,
      c221_active_fl         = p_active_flag,
      c221_last_updated_by   = p_user_id,
      c221_last_updated_date = sysdate
    WHERE c221_asset_id      = p_asset_id;
    
    IF (SQL%ROWCOUNT = 0)    THEN 
    INSERT
    INTO t221_asset_master
      (
        c221_asset_id,
        c221_asset_nm,
        c221_asset_desc,
        c221_active_fl,
        c221_created_by,
        c221_created_date
      )
      VALUES
      (
        p_asset_id,
        p_asset_name,
        p_asset_desc,
        p_active_flag,
        p_user_id,
        SYSDATE
      );
  END IF ;
END GM_AC_SAVE_ASSDETAILS;
 
/******************************************************************

 to void user id of asset master table

******************************************************************/
PROCEDURE gm_ac_void_user_id_asset(
		p_txn_id		 IN   t907_cancel_log.c907_ref_id%TYPE
	  , p_userid		 IN   t102_user_login.c102_user_login_id%TYPE
	)
  AS
		v_void_fl	   t221_asset_master.c221_void_fl%TYPE;
	BEGIN

      UPDATE t222_asset_attribute
			   SET c222_void_fl = 'Y',
				c222_last_updated_by = p_userid,
				c222_last_updated_date = SYSDATE
			 WHERE  c221_asset_id = p_txn_id;
       
			UPDATE t221_asset_master
			   SET c221_void_fl = 'Y'
				 , c221_last_updated_by = p_userid
				 , c221_last_updated_date = SYSDATE
			 WHERE c221_asset_id = p_txn_id;

  END gm_ac_void_user_id_asset;
  
    
PROCEDURE gm_it_fch_assdetails(
		p_asset_id         IN t221_asset_master.C221_ASSET_ID% TYPE
	  , p_data		 OUT   TYPES.cursor_type
	)
  AS 
  
 BEGIN
   OPEN p_data
   FOR 
    SELECT 
        c221_asset_id assetid, c221_asset_nm assetname, c221_asset_desc assetdesc,
        c221_active_fl activeflag
    FROM t221_asset_master
    WHERE  c221_asset_id = p_asset_id;
  
  END gm_it_fch_assdetails;
  
END GM_PKG_IT_ASSET;
/