/* Formatted on 2010/10/08 19:37 (Formatter Plus v4.8.0) */
--@"C:\Database\Packages\it\gm_pkg_it_grp_user_map.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_it_grp_user_map
IS
/*******************************************************
   * Description : Procedure to fetch add group/user
   * Author    : Gopinathan
   *******************************************************/
   PROCEDURE gm_fch_add_group_user(
   	  p_function_id IN    t1520_function_list.c1520_function_id%TYPE,
      p_filter_str  IN    VARCHAR2,
      p_out         OUT   TYPES.cursor_type
   )
   AS
   	  v_fun_type t1520_function_list.c901_type%TYPE;
   BEGIN

	SELECT c901_type INTO v_fun_type
	  FROM t1520_function_list
	 WHERE c1520_function_id = p_function_id
	   AND c1520_void_fl IS NULL;   
	   
	my_context.set_my_inlist_ctx (p_filter_str);

	OPEN p_out
       FOR
			SELECT 'U' mtype,
			  TO_CHAR(t101.c101_party_id) id,
			  get_user_name(t101.c101_user_id) name,
			  '' des,
			  t101.c101_user_f_name fname,
			  t101.c101_user_l_name lname,
			  t101p.c1500_group_id grpid,
			  get_code_name_alt(t101.c901_dept_id) deptname 
			FROM t101_user t101, t101_party t101p, 
			  t102_user_login t102, v_in_list list
			WHERE t101.c101_user_id = t102.c101_user_id
			AND LOWER(t102.c102_login_username) LIKE '%'||LOWER(list.token)||'%'
			AND t101.c901_user_status != 317 --inactive user
			AND t101.c101_party_id IS NOT NULL
			AND t101p.c101_PARTY_ID=t101.c101_party_id

			UNION

			SELECT 'G' mtype,
			  c1500_group_id id,
			  c1500_group_nm name,
			  c1500_group_desc des,
			  '' fname,
			  '' lname,
			  c1500_group_id grpid,
			  '' deptname
			FROM t1500_group, v_in_list list
			WHERE LOWER(c1500_group_nm) LIKE '%'||LOWER(list.token)||'%'
			AND c901_group_type NOT IN ('92264','92265') 
			AND v_fun_type NOT IN ('92260') --function type is action
			AND c1500_void_fl IS NULL;
   END gm_fch_add_group_user;
   --
 /***************************************************
 * Description:Procedure to fetch the function list
 * Author	:	Rajkumar
 ***************************************************/
  PROCEDURE gm_fch_grp_list(
		p_grp_type	IN T1500_group.c901_group_type%TYPE,
		p_filter_str  IN    VARCHAR2,
   		p_out	OUT	TYPES.cursor_type
   )
   AS
   BEGIN
	   
	   my_context.set_my_inlist_ctx (p_filter_str);
	   
	OPEN p_out FOR
		SELECT 'G' mtype,c1500_GROUP_ID ID, c1500_GROUP_NM name,
				c1500_GROUP_DESC des, c901_group_type grptype,
				  '' fname,
			  	  '' lname,
			  	  '' deptname,
			  	  c1500_GROUP_ID grpid 
			FROM T1500_GROUP, v_in_list list
			WHERE c901_group_type = p_grp_type 
			AND LOWER(c1500_group_nm) LIKE '%'||LOWER(list.token)||'%'
			AND c1500_VOID_FL IS NULL;
   END gm_fch_grp_list;
   
   --
	PROCEDURE gm_it_fch_fndetails (
		 p_outfndetail	OUT 	 TYPES.cursor_type
	)
	AS
	/*******************************************************
	  * Description : Procedure to fetch function details
	  * Author	   : Arockia Prasath
	  *******************************************************/
	BEGIN
		OPEN p_outfndetail
		 FOR
			 select c1520_function_id fid, c1520_function_nm fname from 
			 t1520_function_list where c1520_void_fl is null order by c1520_function_nm;

	END gm_it_fch_fndetails;

--

	PROCEDURE gm_it_fch_fngrpdetails (
		 p_functionid		IN		t1530_access.c1520_function_id%TYPE,
		 p_grptypid			IN      t1500_group.c901_group_type%TYPE,
		 p_outfndetail		OUT 	TYPES.cursor_type,
		 p_outgrpdetail		OUT 	TYPES.cursor_type
	)
	AS
	/*******************************************************
	  * Description : Procedure to fetch group details
	  * Author	   : Arockia Prasath
	  *******************************************************/
	BEGIN
		OPEN p_outfndetail
		 FOR
			 select  c1520_function_id fid, c1520_function_nm fname,
			 		 c1520_function_desc fdesc, c901_type ctype, 
			 		 get_code_name(c901_type) ftype, c901_department_id deptid 
			   from  t1520_function_list 
			  where  c1520_function_id = p_functionid 
			    and  c1520_void_fl is null 
		   order by  c1520_function_nm;

			 
		OPEN p_outgrpdetail
		 FOR
		 	 select t1530.c1520_function_id fid,t1530.c1530_access_id accessid, 
		 	 		nvl(t1530.c1500_group_id, t101.c101_user_id) groupid,
			 		decode(t1530.c1500_group_id,null, get_user_name(t101.c101_user_id),
			 		t1500.c1500_group_nm) groupnm,decode(t1530.c1500_group_id,null,'',
			 		t1500.c1500_group_desc) groupdesc,t1530.c1530_read_access_fl readfl,
			 		t1530.c1530_update_access_fl updatefl,t1530.c1530_void_access_fl voidfl, 
			 		t1530.c1530_seq_no seqno, get_user_name(t1530.c1530_created_by) creatby, 
			 		t1530.c1530_created_date createdt,decode(t1530.c1500_group_id,null,'U','G')ugstatus 
			  from  t1530_access t1530,t1500_group t1500,T101_user t101 
			 where  t1530.c1520_function_id=p_functionid 
 			   and  t1530.c1530_void_fl is null 
 			   and  t1500.c1500_group_id(+)=t1530.c1500_group_id 
 			   and  t1500.c1500_void_fl is null
        	   and  t101.c101_party_id(+)=t1530.c101_party_id
        	   and  t1500.c901_group_type = DECODE(p_grptypid,'',t1500.c901_group_type,p_grptypid);
 			 
	END gm_it_fch_fngrpdetails;
	
	
	/* Formatted on 2011/07/26 20:14 (Formatter Plus v4.8.0) */


PROCEDURE gm_it_sav_grpdetails (
   p_inputstr   IN   VARCHAR2,
   p_userid     IN   t215_part_qty_adjustment.c215_created_by%TYPE
)
AS
   v_strlen           NUMBER                  := NVL (LENGTH (p_inputstr), 0);
   v_string           VARCHAR2 (30000)                          := p_inputstr;
   v_substring        VARCHAR2 (1000);
   v_rec              NUMBER;
   v_exist            NUMBER;
   v_count            NUMBER;
   v_function_id      t1530_access.c1520_function_id%TYPE;
   db_function_id     t1530_access.c1520_function_id%TYPE;
   v_access_id        t1530_access.c1530_access_id%TYPE;
   v_group_id         t1530_access.c1500_group_id%TYPE;
   v_removefl         VARCHAR (1);
   v_read_fl          t1530_access.c1530_read_access_fl%TYPE;
   v_update_fl        t1530_access.c1530_update_access_fl%TYPE;
   v_void_fl          t1530_access.c1530_void_access_fl%TYPE;
   v_seq_no           t1530_access.c1530_seq_no%TYPE;
   v_fseq_no          t1530_access.c1530_seq_no%TYPE;
   v_new_seqno        t1530_access.c1530_seq_no%TYPE;
   v_party_id         t1530_access.c101_party_id%TYPE;
   v_parent_funcid    t1530_access.c1520_parent_function_id%TYPE;
   db_parent_funcid   t1530_access.c1520_parent_function_id%TYPE;
   v_ftype            t1520_function_list.c901_type%TYPE;
   v_row_status       VARCHAR2 (10);
   v_grp_id           t1530_access.c1500_group_id%TYPE;
   v_prev_seq_no	  NUMBER;
   v_task_type		  VARCHAR2(20);
   v_task_id		  t1530_access.c1520_function_id%TYPE;
   v_cnt			  NUMBER;
   
/*******************************************************
    * Description : Procedure to save group details
    * Author   : Arockia Prasath
 *******************************************************/
BEGIN
   IF v_strlen > 0
   THEN
   
     -- raise_application_error('-20999','v_string: ' || v_string );
   
      WHILE INSTR (v_string, '|') <> 0
      LOOP
         --
         v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
         v_string := SUBSTR (v_string, INSTR (v_string, '|') + 1);
         --
         v_function_id := NULL;
         v_access_id := NULL;
         v_group_id := NULL;
         v_removefl := NULL;
         v_read_fl := NULL;
         v_update_fl := NULL;
         v_void_fl := NULL;
         v_seq_no := NULL;
         db_parent_funcid := NULL;
         db_function_id := NULL;
         v_exist := 1;
         v_count := 0;
         --
         v_function_id := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
         v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
         v_access_id := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
         v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
         v_group_id := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
         v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
         v_grp_id := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
         v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
         v_fseq_no := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
         v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
         v_removefl := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
         v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
         v_read_fl := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
         v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
         v_update_fl := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
         v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
         v_void_fl := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
         v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
         v_seq_no := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
         v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);

         --v_row_status:=v_substring;
         SELECT DECODE (v_substring, 'G', 'Group', 'U', 'User', 'Group'),
                DECODE (v_removefl, '1', 'Y', 'N')
           INTO v_row_status,
                v_removefl
           FROM DUAL;
           
          --  raise_application_error('-20999','v_row_status: ' || v_row_status );

         --if remove flag checked we void the record
         IF v_removefl = 'Y'
         THEN
            UPDATE t1530_access
               SET c1530_void_fl = 'Y'
             WHERE c1520_function_id = v_function_id
               AND c1530_void_fl IS NULL
               AND c1530_access_id = v_access_id;
         ELSE
            
            --checking for existing record
            
           --raise_application_error('-20999','v_function_id: ' || v_function_id || 'v_access_id : ' || v_access_id);
            
            IF v_function_id IS NOT NULL AND v_access_id IS NOT NULL
            THEN
               SELECT c1520_function_id, c1520_parent_function_id
                 INTO db_function_id, v_parent_funcid
                 FROM t1530_access
                WHERE c1520_function_id = v_function_id
                  AND c1530_access_id = v_access_id
                  AND c1530_void_fl IS NULL;
            END IF;
            
           -- raise_application_error('-20999','db_function_id: ' || db_function_id || 'v_parent_funcid : ' || v_parent_funcid);


            SELECT NVL (c901_type, 0)
              INTO v_ftype
              FROM t1520_function_list
             WHERE c1520_function_id = v_function_id AND c1520_void_fl IS NULL;

            IF (v_ftype = '92262')
            THEN
           --    raise_application_error('-20999','v_exist: ' || v_exist || 'v_fseq_no : ' || v_fseq_no || 'v_seq_no: ' || v_seq_no);
               
               WHILE v_exist <> 0 AND NVL (v_fseq_no, 0) != NVL (v_seq_no, 0)
               LOOP
                  BEGIN
                     IF v_row_status = 'Group'
                     THEN
                        v_grp_id := v_group_id;
                     END IF;

                    --  raise_application_error('-20999','v_grp_id: ' || v_grp_id );

                     SELECT DECODE (NVL (t1520.c1520_request_uri, ''),
                                    '', 'Folder',
                                    'Link'
                                   ),
                            t1520.c1520_function_id
                       INTO v_task_type,
                            v_task_id
                       FROM t1530_access t1530, t1520_function_list t1520
                      WHERE t1520.c1520_function_id = t1530.c1520_function_id 
                        AND c1500_group_id = v_grp_id
                        AND c1530_seq_no = v_seq_no
                        AND c1530_void_fl IS NULL
                        AND c1520_void_fl is NULL;
                        
                     --    raise_application_error('-20999','v_grp_id: ' || v_grp_id || 'v_task_type : ' || v_task_type || 'v_task_id: ' || v_task_id);

           
                     IF v_task_type = 'Folder'
                     THEN
                        SELECT NVL (MAX (t1530.c1530_seq_no), 0)
                          INTO v_prev_seq_no
                          FROM t1530_access t1530
                         WHERE t1530.c1520_parent_function_id = v_task_id
                           AND c1500_group_id = v_grp_id;

                        v_parent_funcid := v_task_id;
                       
                        IF v_prev_seq_no = 0
                        THEN
                           v_prev_seq_no := v_seq_no;
                        END IF;

                        v_seq_no := v_prev_seq_no + 1;
                        v_exist := 0;
                        
                   --     raise_application_error('-20999','v_prev_seq_no: ' || v_prev_seq_no || 'v_seq_no : ' || v_seq_no	);
                        
                     ELSE
                        SELECT c1520_parent_function_id
                          INTO db_parent_funcid
                          FROM t1530_access
                         WHERE c1500_group_id = v_grp_id
                           AND c1530_seq_no = v_seq_no
                           AND c1530_void_fl IS NULL;

                        IF v_count = 0
                        THEN
                           v_parent_funcid := db_parent_funcid;
                           v_count := v_count + 1;
                        END IF;

                        v_seq_no := v_seq_no + 1;

                        IF v_parent_funcid != db_parent_funcid
                        THEN
                           raise_application_error (-20409, '');
                        END IF;
                     END IF;
                  EXCEPTION
                     WHEN NO_DATA_FOUND
                     THEN
                        v_exist := 0;
                        raise;
                  END;
               END LOOP;
            END IF;

            SELECT COUNT (1)
              INTO v_rec
              FROM t1500_group
             WHERE c1500_group_id = v_group_id AND c1500_void_fl IS NULL;

            IF v_rec = 0
            THEN
               v_party_id := v_group_id;
               v_group_id := NULL;
            END IF;

            IF db_function_id IS NOT NULL
            THEN
               --update the changes into existing record
               
            SELECT COUNT (1)
              INTO v_rec
              FROM t1500_group
             WHERE c1500_group_id = v_group_id AND c1500_void_fl IS NULL;
               
               UPDATE t1530_access
                  SET c1530_read_access_fl = v_read_fl,
                      c1530_update_access_fl = v_update_fl,
                      c1530_void_access_fl = v_void_fl,
                      c1530_seq_no = v_seq_no,
                      c1520_parent_function_id = v_parent_funcid
                WHERE c1520_function_id = v_function_id
                  AND c1530_void_fl IS NULL
                  AND c1530_access_id = v_access_id;
           
            
            --creating a new record
            ELSE
               INSERT INTO t1530_access
                           (c1530_access_id, c1500_group_id, c101_party_id,
                            c1520_function_id, c1520_parent_function_id,
                            c1530_read_access_fl, c1530_update_access_fl,
                            c1530_void_access_fl, c1530_created_by,
                            c1530_created_date, c1530_seq_no
                           )
                    VALUES (s1530_access.NEXTVAL, v_group_id, v_party_id,
                            v_function_id, v_parent_funcid,
                            v_read_fl, v_update_fl,
                            v_void_fl, p_userid,
                            SYSDATE, v_seq_no
                           );
            END IF;
         END IF;
      END LOOP;
   END IF;
END gm_it_sav_grpdetails;

	
/***************************************************
 * Description:Procedure to fetch the function list
 * Author	:	Rajkumar
 ***************************************************/
PROCEDURE gm_fch_function_list(
		p_type	IN 	t1520_function_list.c901_type%TYPE,
   		p_out	OUT	TYPES.cursor_type
   )
  AS
   BEGIN
	  open p_out FOR
		 SELECT c1520_function_id FUNID,
			  c1520_function_nm FUNNAME,
			  c1520_function_desc FUNDESC,
			  get_code_name(c901_type) FUNTYPE,
			  get_code_name(C901_LEFT_MENU_TYPE) LMTYPE,
			  NVl(C901_LEFT_MENU_TYPE,'') TSKID, 
			  C1520_REQUEST_URI		REQURI,
			  C1520_REQUEST_STROPT STROPT,
			  gm_pkg_it_grp_user_map.get_module_mapped(c1520_function_id) MODULE,
			  get_user_name(c1520_created_by) CREATEDBY,
			  c1520_created_date CREATEDDATE,
			  NVL(C1520_VOID_FL,'N') VDFL,
			  DECODE(C1520_VOID_FL,'Y',NVL(get_cancel_log(c1520_function_id,'4000400'),'0'),'0') CANCELCD
			FROM t1520_function_list
			WHERE c1520_function_id NOT IN 
			  (SELECT c1520_function_id
			  FROM t1520_function_list
			  WHERE c901_type='92262'
			  AND c1520_request_uri is null
			  ) 
			 AND c901_type=p_type;
			 
  END gm_fch_function_list;
 
  
--
/**
 * Description	: Procedure to create the subfolder
 * Author		: Rajkumar 
 */
	PROCEDURE gm_it_create_subfolder(
		p_seq_no	IN 		T1530_ACCESS.c1530_SEQ_NO%TYPE,
		p_grp_nm	IN 		T1500_GROUP.C1500_GROUP_NM%TYPE,	
		p_fold_name	IN 		VARCHAR2,
		p_userid	IN		T1530_ACCESS.C1530_CREATED_BY%TYPE
	)
	AS
		v_parent_id		VARCHAR2(20);
		v_fun_id		VARCHAR2(20);
		v_temp_fn_id	VARCHAR2(20);
		v_seq_no		NUMBER;
		
		CURSOR v_cur(v_grp_id IN T1500_GROUP.C1500_GROUP_ID%TYPE)
		IS 
		SELECT   t1520.c1520_function_id function_id,
                   NVL (t1530.c1530_function_nm,
                        t1520.c1520_function_nm
                       ) function_nm,
                   t1520.c1520_request_uri url,
                   t1520.c1520_request_stropt stropt, (lvl) "LEVEL", root, seq_no,
                   DECODE (t1520.c1520_request_uri,
                           '', 'Folder',
                           'File'
                          ) TYPE, accid 
              FROM t1520_function_list t1520, 
                   (SELECT     t1530.c1520_function_id fun_id, LEVEL lvl,
                               CONNECT_BY_ROOT t1530.c1520_function_id root,
                               t1530.c1530_seq_no seq_no, t1530.C1530_ACCESS_ID accid, 
                               t1530.c1530_function_nm
                          FROM t1530_access t1530
                         WHERE t1530.c1530_void_fl IS NULL
                           AND (   t1530.c1500_group_id = v_grp_id
                                OR (    t1530.c101_party_id IS null 
                                    AND (   t1530.c1530_read_access_fl = 'Y'
                                         OR t1530.c1530_update_access_fl = 'Y'
                                        )
                                   )
                               )
                           AND t1530.C1520_PARENT_FUNCTION_ID IS NOT NULL
                    START WITH t1530.c1520_parent_function_id =
                                  ((SELECT MIN
                                              (t1530t.c1520_parent_function_id)
                                      FROM t1530_access t1530t
                                     WHERE t1530t.c1500_group_id = v_grp_id))
              CONNECT BY NOCYCLE PRIOR t1530.C1520_FUNCTION_ID = t1530.C1520_PARENT_FUNCTION_ID) t1530
          WHERE    t1520.c1520_function_id = t1530.fun_id
               AND t1520.c1520_void_fl IS NULL
               AND t1530.lvl > 0
               AND t1520.c901_type = 92262
          ORDER BY seq_no, lvl;
          
          v_cnt	NUMBER;
          v_grpid	T1500_GROUP.C1500_GROUP_ID%TYPE;
          
	BEGIN
		SELECT count(*) INTO v_cnt 
				FROM T1500_GROUP 
				WHERE gm_pkg_it_user.GET_GROUP_NM(C1500_GROUP_ID) = p_grp_nm 
				AND C1500_VOID_FL IS NULL; 
				
		IF v_cnt = 0 THEN
			raise_application_error ('-20999',
									p_grp_nm 
                                  || ' not exists'
                                 );
		END IF;
				
		SELECT C1500_GROUP_ID INTO v_grpid 
				FROM T1500_GROUP 
				WHERE gm_pkg_it_user.GET_GROUP_NM(C1500_GROUP_ID) = p_grp_nm 
				AND C1500_VOID_FL IS NULL; 
				
		SELECT c1520_parent_function_id INTO v_parent_id 
			FROM t1530_ACCESS 
			WHERE C1500_GROUP_ID=v_grpid AND c1530_seq_no=p_seq_no
			AND C1530_VOID_FL IS NULL;
		
			SELECT S1520_FUNCTION_LIST_TASK.nextval  
				INTO v_fun_id FROM DUAL;
				
				SELECT COUNT(*) INTO v_cnt FROM T1520_FUNCTION_LIST t1520, t1530_ACCESS t1530 
					WHERE t1520.C901_TYPE='92262' 
					AND t1520.C1520_REQUEST_URI IS NULL
          			AND t1520.c901_department_id IS NOT NULL 					
					AND t1520.C1520_FUNCTION_ID NOT IN ('12273800','12273900','12274000')
					AND t1520.C1520_FUNCTION_ID = t1530.C1520_FUNCTION_ID
					AND t1530.C1500_GROUP_ID = v_grpid;
					
				IF v_cnt<=0 THEN 
					FOR var_access IN v_cur(v_grpid)
					LOOP
						UPDATE t1530_access  
							SET c1530_seq_no= (c1530_seq_no*10) 
							WHERE C1530_ACCESS_ID = var_access.accid 
							AND C1530_VOID_FL IS NULL;
					END LOOP;
					v_seq_no := p_seq_no * 10;
				else
					v_seq_no := p_seq_no;
				END IF;
				
					
					SELECT t1520.c1520_function_id INTO v_temp_fn_id
					from  t1520_function_list t1520 where c1520_function_nm = 'TEMP_LINK';    
					
				INSERT INTO t1520_function_list(
					C1520_FUNCTION_ID,C1520_FUNCTION_NM ,C1520_FUNCTION_DESC                                                                  
					,C1520_INHERITED_FROM_ID,C901_TYPE,C1520_VOID_FL,C1520_CREATED_BY                                                              
					,C1520_CREATED_DATE                           ,C1520_LAST_UPDATED_BY                                                                  
					,C1520_LAST_UPDATED_DATE                      ,C1520_REQUEST_URI                                                                 
					,C1520_REQUEST_STROPT                         ,C1520_SEQ_NO                                                        
					,C901_DEPARTMENT_ID)
					 VALUES (v_fun_id,p_fold_name,p_fold_name,null,92262,null,'303149',SYSDATE,null,null,null,null,null,'2016');
					
					INSERT INTO t1530_access(C1530_ACCESS_ID  ,C1500_GROUP_ID                        
					,C1520_FUNCTION_ID                           ,C101_PARTY_ID                             
					,C1530_UPDATE_ACCESS_FL                  ,C1530_READ_ACCESS_FL                        
					,C1530_VOID_ACCESS_FL                        ,C1530_VOID_FL                               
					,C1530_CREATED_BY                            ,C1530_CREATED_DATE                     
					,C1530_LAST_UPDATED_BY                       ,C1530_LAST_UPDATED_DATE                
					,C1520_PARENT_FUNCTION_ID                    ,C1530_FUNCTION_NM                              
					,C1530_SEQ_NO)
					VALUES(s1530_access.nextval,v_grpid,v_fun_id,null,'Y','Y',null,null,null,null,null,null,v_parent_id,null,(v_seq_no+1));
					
					INSERT INTO t1530_access(C1530_ACCESS_ID  ,C1500_GROUP_ID                        
					,C1520_FUNCTION_ID                           ,C101_PARTY_ID                             
					,C1530_UPDATE_ACCESS_FL                 	 ,C1530_READ_ACCESS_FL                        
					,C1530_VOID_ACCESS_FL                        ,C1530_VOID_FL                               
					,C1530_CREATED_BY                            ,C1530_CREATED_DATE                     
					,C1530_LAST_UPDATED_BY                       ,C1530_LAST_UPDATED_DATE                
					,C1520_PARENT_FUNCTION_ID                    ,C1530_FUNCTION_NM                              
					,C1530_SEQ_NO)
					VALUES(s1530_access.nextval,v_grpid,v_temp_fn_id,null,'Y','Y',null,null,null,null,null,null,v_fun_id,null,(v_seq_no+2));
					
	END gm_it_create_subfolder;
 /***************************************************
  * Description : Procedure to fetch the Task ans Security Events for Security Group
  *
  * Author		: Rajkumar J
  * 
  ***************************************************/
  PROCEDURE gm_fch_sec_group_map(
  	p_grp_id	IN		T1500_group.c1500_group_id%TYPE,
  	p_type		IN		T1520_FUNCTION_LIST.c901_type%TYPE,
  	p_out		OUT		TYPES.cursor_type
  )
  AS
  BEGIN
	  OPEN p_out FOR 
	  SELECT t1520.c1520_function_nm groupnm,
	  		 t1520.c1520_function_desc groupdesc,
	  		 t1530.c1530_read_access_fl readfl,
	  		 t1530.c1530_update_access_fl updatefl,
	  		 t1530.c1530_void_access_fl voidfl,
	  		 NVL(get_code_name(t1520.C901_LEFT_MENU_TYPE),'') tasktype,
	  		 get_user_name(t1530.c1530_created_by) CREATBY,
	  		 t1530.c1530_created_date CREATEDT ,gm_pkg_it_grp_user_map.get_module_mapped(t1520.c1520_function_id) MODULE
	  	  FROM t1520_function_list t1520, t1530_access t1530 
        WHERE t1520.c1520_function_id = t1530.c1520_function_id 
        AND t1530.c1500_group_id = p_grp_id  
        AND t1520.c901_type=p_type
        AND t1520.C1520_void_fl is null
        AND t1530.c1530_void_fl is null;
  END gm_fch_sec_group_map;

  /**
 * Description	: Procedure to rename links
 * Author		: VPrasath 
 */
	PROCEDURE gm_it_rename_link(
		p_grp_nm	 IN  T1500_GROUP.C1500_GROUP_NM%TYPE,
		p_link_name	 IN  t1520_function_list.c1520_function_nm%TYPE,
		p_re_name_as IN  t1520_function_list.c1520_function_nm%TYPE,
		p_userid	 IN	 T1530_ACCESS.C1530_CREATED_BY%TYPE
	)
	AS
		v_grp_id		T1500_GROUP.C1500_GROUP_ID%TYPE;
		v_fun_id		VARCHAR2(20);
		v_cnt			NUMBER;
	BEGIN
		
		BEGIN
			SELECT C1500_GROUP_ID INTO v_grp_id
			FROM T1500_GROUP t1500
			where t1500.c1500_group_nm = p_grp_nm;
		EXCEPTION WHEN NO_DATA_FOUND THEN
			raise_application_error('-20999','Group Name-' || p_grp_nm  ||'-does not exist');
		END;
		
		BEGIN
		SELECT t1520.c1520_function_id INTO v_fun_id
		FROM t1520_function_list t1520, t1530_access T1530
		where T1520.c1520_function_id = T1530.c1520_function_id 
		AND t1520.c1520_function_nm = p_link_name
		AND T1530.C1500_GROUP_ID = v_grp_id;
		EXCEPTION WHEN NO_DATA_FOUND THEN
			raise_application_error('-20999','Function Name does not exist');
		END;
		
		UPDATE T1530_ACCESS T1530
		SET T1530.C1530_FUNCTION_NM = p_re_name_as
			, C1530_LAST_UPDATED_BY = p_userid
			, C1530_LAST_UPDATED_DATE = SYSDATE		
		WHERE C1500_GROUP_ID = v_grp_id
		AND C1520_FUNCTION_ID = v_fun_id;
  END gm_it_rename_link;

  /*
   * Description: Create the favorite
   * 
   * Author: Rajkumar
   */
 
PROCEDURE gm_create_favorites (
   p_grp_id   IN   t1500_group.c1500_group_id%TYPE,
   p_fun_id   IN   t1530_access.c1520_function_id%TYPE,
   p_fun_nm   IN   t1530_access.c1530_function_nm%TYPE,
   p_userid   IN   t1530_access.c1530_created_by%TYPE
)
AS
   v_parent_fn_id   t1530_access.c1520_parent_function_id%TYPE;
   v_cnt             NUMBER;
   v_root            NUMBER;
   v_grp_id          VARCHAR2 (20);
   v_seq_no          NUMBER;
   v_fn_seq_no       NUMBER;
   v_temp_ids        VARCHAR2(4000);
   v_party_id       NUMBER;
   v_dept_id        NUMBER;
   v_access_level   NUMBER;
   v_void_cnt       NUMBER;
   
   CURSOR fn_cur
   IS
      SELECT     t1530.c1520_function_id fn_id,
                 c1520_parent_function_id parent_fn_id,
                t1520.c1520_function_nm FN_NAME
                ,  (CASE WHEN c1520_parent_function_id IS NULL THEN t1500.c1500_group_nm ELSE t1530.c1530_function_nm END) T1530_FN_NAME
                ,  DECODE(t1520.c1520_request_uri,'','Folder','Link') LTYPE
            FROM t1530_access t1530,
                 t1520_function_list t1520,
                 t1500_group t1500
           WHERE t1530.c1500_group_id = p_grp_id
             AND t1520.c1520_function_id = t1530.c1520_function_id
             AND t1500.c1500_group_id = t1530.c1500_group_id
             AND t1500.c1500_void_fl is null
             AND t1520.c1520_void_fl is NULL
             AND t1530.c1530_void_fl is NULL
      START WITH t1530.c1520_function_id = p_fun_id
      CONNECT BY NOCYCLE PRIOR  t1530.C1520_PARENT_FUNCTION_ID  = t1530.C1520_FUNCTION_ID
      ORDER BY   t1530.c1530_seq_no;
BEGIN
		BEGIN
			SELECT t101.c901_dept_id, t101.c101_access_level_id
	        INTO v_dept_id, v_access_level
	        FROM t101_user t101
	       WHERE t101.c101_user_id = p_userid;
       	EXCEPTION 
       	WHEN NO_DATA_FOUND
       	THEN
       		v_dept_id := null;
       		v_access_level := null;
       	 END;
       	IF v_dept_id = 2005 AND (v_access_level <= 2 OR v_access_level = 7)
        THEN
        	BEGIN
            SELECT t101.c101_party_id
              INTO v_party_id
              FROM t101_party t101, t703_sales_rep t703
             WHERE t101.c101_party_id = t703.c101_party_id
               AND t703.C703_SALES_REP_ID = p_userid;
             EXCEPTION
	         WHEN NO_DATA_FOUND
	         THEN
	            v_party_id := NULL;
	        END;
         END IF;
         IF v_party_id is null
         THEN
         	BEGIN
             SELECT t101.c101_party_id
              INTO v_party_id
              FROM t101_user t101
             WHERE t101.c101_user_id = p_userid;
            EXCEPTION
	        WHEN NO_DATA_FOUND
	        THEN
	           v_party_id := NULL;
	      	END;
         END IF;
         IF v_party_id IS NULL THEN
          BEGIN
            SELECT t101p.c101_party_id 
              INTO v_party_id 
              FROM t101_party t101p
             WHERE t101p.c101_party_id = p_userid;
    	  EXCEPTION
	         WHEN NO_DATA_FOUND
	         THEN
	            v_party_id := NULL;
	     	 END;
         END IF;

      v_grp_id := '001_'|| v_party_id || '_FAV';

      -- Added for PMT-45143 unable to save left links to favourites
      --Getting add favourite group void flag count
	      SELECT COUNT(1) 
	      	INTO v_void_cnt 
	      	FROM t1501_group_mapping 
	       WHERE c1500_group_id = v_grp_id
	         AND c1501_void_fl IS NOT NULL;
      
      --Checking void flag count is not equal to zero then updating void flag as null
      IF v_void_cnt <> 0 THEN
      
       UPDATE t1501_group_mapping 
          SET c1501_void_fl =NULL,C1501_LAST_UPDATED_BY=v_party_id,
              C1501_LAST_UPDATED_DATE=SYSDATE
        WHERE C1500_GROUP_ID=v_grp_id;
      
      END IF;
      
   SELECT COUNT (1)
     INTO v_cnt
     FROM t1500_group t1500, t1501_group_mapping t1501
    WHERE t1500.c1500_group_id = t1501.c1500_group_id
      AND t1500.c901_group_type = 92266
      AND t1501.c101_mapped_party_id = v_party_id
      AND t1500.c1500_group_id = v_grp_id
      AND t1500.c1500_void_fl IS NULL
      AND t1501.c1501_void_fl IS NULL;

   IF v_cnt = 0
   THEN
      SELECT (MAX (c1530_seq_no) + 20)
        INTO v_seq_no
        FROM t1530_access;

      INSERT INTO t1500_group
                  (c1500_group_id, c1500_group_nm, c1500_group_desc,
                   c1500_created_by, c1500_created_date, c901_group_type
                  )
           VALUES (v_grp_id, 'My Favorites', 'User Favorite Group',
                   v_party_id, SYSDATE, '92266'
                  );

      INSERT INTO t1501_group_mapping
                  (c1501_group_mapping_id, c1500_group_id,
                   c101_mapped_party_id, c1501_created_by, c1501_created_date
                  )
           VALUES (s1501_group_mapping.NEXTVAL, v_grp_id,
                   v_party_id, v_party_id, SYSDATE
                  );

      SELECT (s1520_function_list_task.NEXTVAL) * -1
        INTO v_root
        FROM DUAL;

      INSERT INTO t1520_function_list
                  (c1520_function_id, c1520_function_nm, c1520_function_desc,
                   c901_type, c1520_created_by, c1520_created_date
                  )
           VALUES (v_root, v_grp_id || ' Root ', v_grp_id || ' Root ',
                   92266, v_party_id, SYSDATE
                  );

      INSERT INTO t1530_access
                  (c1530_access_id, c1500_group_id, c1520_function_id,
                   c1530_created_by, c1530_created_date, c1530_seq_no
                  )
           VALUES (s1530_access.NEXTVAL, v_grp_id, v_root,
                   v_party_id, SYSDATE, v_seq_no
                  );
   ELSE
      SELECT t1530.c1520_function_id
        INTO v_root
        FROM t1530_access t1530
       WHERE t1530.c1500_group_id = v_grp_id
         AND c1520_parent_function_id IS NULL
         AND t1530.C1530_void_fl is NULL;
   END IF;

 /*  FOR v_functions IN fn_cur
   LOOP

   	  SELECT COUNT (1)
        INTO v_cnt
        FROM t1530_access t1530
       WHERE t1530.c1500_group_id = v_grp_id
         AND t1530.c1520_function_id = v_functions.fn_id
         AND t1530.c1530_void_fl IS NULL;
         
         v_temp_ids :=  v_functions.fn_id;

      IF v_cnt = 0
      THEN
      
      IF v_functions.parent_fn_id IS NULL
         THEN
            v_parent_fn_id := v_root;
         ELSE
            v_parent_fn_id := v_functions.parent_fn_id;
         END IF;

         SELECT  NVL(MAX(t1530.c1530_seq_no),0)
        INTO  v_seq_no
        FROM t1530_access t1530
       WHERE t1530.c1500_group_id = v_grp_id
         AND c1520_parent_function_id = v_parent_fn_id;
         
         IF v_seq_no = 0 THEN
         SELECT  t1530.c1530_seq_no
        INTO  v_seq_no
        FROM t1530_access t1530
       WHERE t1530.c1500_group_id = v_grp_id
         AND c1520_function_id = v_parent_fn_id;
         END IF;

         IF v_functions.LTYPE = 'Folder' THEN
         
         v_fn_seq_no := v_seq_no + 1000;
         ELSE
         v_fn_seq_no := v_seq_no + 10;
         END IF;
         
         INSERT INTO t1530_access
                     (c1530_access_id, c1500_group_id, c1520_function_id,
                      c1530_created_by, c1530_created_date,
                      c1520_parent_function_id, c1530_seq_no, c1530_function_nm
                     )
              VALUES (s1530_access.NEXTVAL, v_grp_id, v_temp_ids,
                      p_userid, SYSDATE,
                      v_parent_fn_id, v_fn_seq_no, v_functions.T1530_FN_NAME
                     );
      END IF;
   END LOOP;*/
   
    SELECT COUNT (1)
        INTO v_cnt
        FROM t1530_access t1530
       WHERE t1530.c1500_group_id = v_grp_id
         AND t1530.c1520_function_id = p_fun_id
         AND t1530.c1530_void_fl IS NULL;
         
 	  v_parent_fn_id := v_root;
   
 	  IF v_cnt = 0 THEN
    SELECT  NVL(MAX(t1530.c1530_seq_no),0)
        INTO  v_seq_no
        FROM t1530_access t1530
       WHERE t1530.c1500_group_id = v_grp_id
         AND c1520_parent_function_id = v_parent_fn_id
         AND t1530.c1530_void_fl is NULL;
     
         v_fn_seq_no := v_seq_no + 10;
         
     INSERT INTO t1530_access
                     (c1530_access_id, c1500_group_id, c1520_function_id,c1530_function_nm,
                      c1530_created_by, c1530_created_date,
                      c1520_parent_function_id, c1530_seq_no
                     )
              VALUES (s1530_access.NEXTVAL, v_grp_id, p_fun_id,p_fun_nm,
                      v_party_id, SYSDATE,
                      v_parent_fn_id, v_fn_seq_no
                     );     
         
   END IF;
END gm_create_favorites;

/*
 * Description: Remove the favorite
 * 
 * Author: Rajkumar
 */
PROCEDURE gm_remove_favorites
    (
        p_grp_id IN T1500_group.c1500_group_id%TYPE,
        p_fun_id IN t1530_access.c1520_function_id%TYPE,
        p_userid IN T1530_ACCESS.C1530_CREATED_BY%TYPE
    )
AS
BEGIN
   UPDATE T1530_ACCESS T1530
   SET T1530.C1530_VOID_FL = 'Y'
   , T1530.C1530_LAST_UPDATED_BY = p_userid
   , T1530.C1530_LAST_UPDATED_DATE = SYSDATE
   WHERE t1530.c1500_group_id = p_grp_id
         AND t1530.c1520_function_id = p_fun_id;
END gm_remove_favorites;

/*
 * Description: This Function is used to fetch the Module Name that is mapped with the Task
 * 
 * Author: Rajkumar
 */
FUNCTION get_module_mapped(
	p_funid	t1520_function_list.c1520_function_id%TYPE
) RETURN VARCHAR2
IS
v_mod	VARCHAR2(500);
BEGIN
	BEGIN
    SELECT t1500.c1500_group_nm
       INTO v_mod
       FROM t1530_access t1530, t1500_group t1500
      WHERE t1500.c1500_group_id    = t1530.c1500_group_id
        AND t1500.c901_group_type   = '92265'
        AND t1500.c1500_void_fl    IS NULL
        AND t1530.c1520_function_id = p_funid
        AND t1530.c1530_void_fl    IS NULL
        AND ROWNUM = 1;
      EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
        v_mod := '';
      END;
      RETURN v_mod;
END get_module_mapped;

/* 
 *	Description: Procedure to save the Task, Security EVent to Security Event Details
 *	Author: Rajkumar 
 */
PROCEDURE gm_sav_task_details(
	p_voidstr	CLOB,
	p_tskname	CLOB,
	p_taskdesc	CLOB,
	p_tasktype	CLOB,
	p_userid	VARCHAR2
)
AS
v_string 		CLOB;
v_substring 	CLOB;
v_funid			T1520_FUNCTION_LIST.C1520_FUNCTION_ID%TYPE;
v_updval		VARCHAR2(2000);
v_check_id		VARCHAR2(20);
v_substr		CLOB;
BEGIN
	--	Update the Task Name details
	IF p_tskname IS NOT NULL
	THEN
		v_string := p_tskname;

		WHILE INSTR (v_string, '|') <> 0
      	LOOP
         	v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
         	v_string := SUBSTR (v_string, INSTR (v_string, '|') + 1);
			
         	v_funid	:=	SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
         	v_updval := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
         	
         	UPDATE T1520_FUNCTION_LIST 
         			SET C1520_FUNCTION_NM = v_updval, 
         				C1520_LAST_UPDATED_BY = p_userid, 
         				C1520_LAST_UPDATED_DATE = SYSDATE
         		WHERE C1520_FUNCTION_ID = v_funid
         		AND C1520_VOID_FL IS NULL;
         END LOOP;
	END IF;
	-- Update the Task Description
	IF p_taskdesc IS NOT NULL
	THEN
		v_string := p_taskdesc;

		WHILE INSTR (v_string, '|') <> 0
      	LOOP
         	v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
         	v_string := SUBSTR (v_string, INSTR (v_string, '|') + 1);
			
         	v_funid	:=	SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
         	v_updval := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
         	
         	UPDATE T1520_FUNCTION_LIST 
         			SET C1520_FUNCTION_DESC = v_updval, 
         				C1520_LAST_UPDATED_BY = p_userid, 
         				C1520_LAST_UPDATED_DATE = SYSDATE
         		WHERE C1520_FUNCTION_ID = v_funid
         		AND C1520_VOID_FL IS NULL;
         END LOOP;

	END IF;
	-- Update the Task Type
	IF p_tasktype IS NOT NULL
	THEN
		v_string := p_tasktype;
		
		WHILE INSTR (v_string, '|') <> 0
      	LOOP
         	v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
         	v_string := SUBSTR (v_string, INSTR (v_string, '|') + 1);
			
         	v_funid	:=	SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
         	v_updval := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
         	
         	UPDATE T1520_FUNCTION_LIST 
         			SET C901_LEFT_MENU_TYPE = v_updval, 
         				C1520_LAST_UPDATED_BY = p_userid, 
         				C1520_LAST_UPDATED_DATE = SYSDATE
         		WHERE C1520_FUNCTION_ID = v_funid
         		AND C1520_VOID_FL IS NULL;
         END LOOP;
         
	END IF;
	-- Update the Void Flag for Checked and Un-Checked Tasks
	IF p_voidstr IS NOT NULL
	THEN
		v_string := p_voidstr;

		WHILE INSTR (v_string, '|') <> 0
      	LOOP
         	v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
         	v_string := SUBSTR (v_string, INSTR (v_string, '|') + 1);
			
         	v_funid	:=	SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
         	v_substr := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
         	v_updval := SUBSTR (v_substr, 1, INSTR (v_substr, '^') - 1);
         	v_check_id := SUBSTR (v_substr, INSTR (v_substr, '^') + 1); 
         	
         	UPDATE T1520_FUNCTION_LIST 
         			SET C1520_VOID_FL = DECODE(v_check_id,'1','Y','0',NULL), 
         				C1520_LAST_UPDATED_BY = p_userid, 
         				C1520_LAST_UPDATED_DATE = SYSDATE
         		WHERE C1520_FUNCTION_ID = v_funid;
         		
         		IF v_check_id = 1
         		THEN
         			gm_upd_cancel_log(v_funid,v_updval,p_userid);
         		END IF;
         		
         END LOOP;
	END IF;
	--
END gm_sav_task_details;

/*
 * Description: Procedure to update the Cancel Log 
 * Author: Rajkumar
 */
PROCEDURE gm_upd_cancel_log(
	p_refid		VARCHAR2,
	p_cancode 	VARCHAR2,
	p_user_id	VARCHAR2
)
AS
v_company_id    t1900_company.c1900_company_id%TYPE;
BEGIN
	SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL')) INTO v_company_id FROM dual; 
		UPDATE t907_cancel_log 
				SET c907_comments = 'Task Details Voided',
					c901_cancel_cd = p_cancode,
					C907_LAST_UPDATED_BY = p_user_id,
					C907_LAST_UPDATED_DATE = SYSDATE,
					c907_cancel_date = SYSDATE
			WHERE 
				c907_ref_id = p_refid AND
				c901_type = 4000400;
	
	IF (SQL%ROWCOUNT = 0)
	THEN
			INSERT
                   INTO t907_cancel_log
                    (
                        c907_cancel_log_id, c907_ref_id, c907_comments
                      , c901_cancel_cd, c901_type, c907_created_by
                      , c907_created_date, c907_cancel_date, c1900_company_id
                    )
                    VALUES
                    (
                        s907_cancel_log.NEXTVAL, p_refid , 'Task Details Voided', p_cancode, '4000400'
                      , p_user_id, SYSDATE, TRUNC (SYSDATE), v_company_id
                    ) ;
	END IF;                   
END gm_upd_cancel_log;

/***************************************************
  * Description : Procedure to fetch the Security Events for Security Group
  *
  * Author		: Jignesh
  * 
  ***************************************************/
  PROCEDURE gm_fch_sec_event_map(
  	p_grp_id	IN		T1500_group.c1500_group_id%TYPE,
  	p_type		IN		t1520_function_list.c901_type%TYPE,
  	p_out		OUT		TYPES.cursor_type
  )
  AS
  BEGIN
	  OPEN p_out FOR 
	  SELECT t1520.c1520_function_id FUNID, t1520.c1520_function_nm FUNNAME, t1520.c1520_function_desc FUNDESC
	  		, t1530.c1500_group_id groupid, t1500.c1500_group_nm groupnm
	  		, DECODE (t1530.c1500_group_id, NULL, '', t1500.c1500_group_desc) groupdesc
	    	, t1530.c1530_read_access_fl readfl, t1530.c1530_update_access_fl updatefl
	  		, t1530.c1530_void_access_fl voidfl, t1530.c1530_seq_no seqno
	  		, get_user_name(t1520.c1520_created_by) createdby, t1520.c1520_created_date CREATEDDATE
	  		, DECODE (t1530.c1500_group_id, NULL, 'U', 'G') ugstatus
	  FROM t1530_access t1530, t1500_group t1500, t1520_function_list t1520
	  WHERE t1500.c1500_group_id    = NVL(p_grp_id,t1500.c1500_group_id)
	  AND t1520.c1520_function_id = t1530.c1520_function_id
	  AND t1500.c1500_group_id(+) = t1530.c1500_group_id
	  AND t1520.c901_type         = p_type
	  AND t1500.c1500_void_fl IS NULL
	  AND t1530.c1530_void_fl IS NULL
	  AND t1520.c1520_void_fl is null ;
  END gm_fch_sec_event_map;
  
/*
 * Descrition: Function to get the cancel log reason type
 * Author: Rajkumar
 */

FUNCTION get_cancel_log(
	p_refid	VARCHAR2,
	p_type	VARCHAR2
)
RETURN VARCHAR2
IS 
v_cancel_cd		VARCHAR2(20):='0';
BEGIN
	BEGIN
		SELECT c901_cancel_cd INTO v_cancel_cd
   			FROM t907_cancel_log
  	 	WHERE c901_type   = p_type
    		AND c907_ref_id = p_refid;
    EXCEPTION WHEN NO_DATA_FOUND
    THEN
    	v_cancel_cd := '0';
    END;
    
    RETURN v_cancel_cd;
END get_cancel_log;
END gm_pkg_it_grp_user_map;
/ 
