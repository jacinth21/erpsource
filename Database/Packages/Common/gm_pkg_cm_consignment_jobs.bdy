--@"c:\database\packages\common\gm_pkg_cm_consignment_jobs.bdy";
CREATE OR REPLACE
PACKAGE BODY gm_pkg_cm_consignment_jobs
IS
    /******************************************************************
    * Description : Procedure to move built set to Pending Pick
    * Author  : Xun
    ****************************************************************/
PROCEDURE gm_move_builtset_requests
AS
    CURSOR builtset_cur
    IS
         SELECT c504_consignment_id consign_id, c520_request_for req_type
           FROM t504_consignment t504, t520_request t520
          WHERE t504.c520_request_id        = t520.c520_request_id
            AND t504.C504_status_fl         = '2' --(built    SET) 
            AND t520.C520_planned_ship_date = TRUNC (SYSDATE) + 1
            AND t504.c504_void_fl          IS NULL
            AND t520.c520_void_fl          IS NULL;
BEGIN
    FOR builtsetdtl IN builtset_cur
    LOOP
        gm_sav_status_update (builtsetdtl.consign_id, '30301') ;
        UPDATE t5050_invpick_assign_detail
        SET C901_REF_TYPE = 93601,
        	C5050_LAST_UPDATED_BY = '30301',
        	C5050_LAST_UPDATED_DATE = SYSDATE
        WHERE c5050_ref_id = builtsetdtl.consign_id AND c5050_void_fl IS NULL; 
      	--  gm_pkg_allocation.gm_ins_invpick_assign_detail (93601, builtsetdtl.consign_id, '30301');
        --93601 consignment set , to insert record in the t5050_INVPICK_ASSIGN_DETAIL
    END LOOP;
END gm_move_builtset_requests;
/********************************************************************
* Description : Procedure to do the status update in conignment tbl
* Author    : Xun
********************************************************************/
PROCEDURE gm_sav_status_update (
        p_consignid IN t504_consignment.c504_consignment_id%TYPE,
        p_userid    IN t504_consignment.c504_created_by%TYPE)
AS
BEGIN
     UPDATE t504_consignment
    SET c504_status_fl          = '2.20', -- Pending Pick
        c504_last_updated_date  = SYSDATE, c504_last_updated_by = p_userid
      WHERE c504_consignment_id = p_consignid
        AND c504_void_fl       IS NULL;
END gm_sav_status_update;
END gm_pkg_cm_consignment_jobs;
/
