/* Formatted on 2010/04/12 16:30 (Formatter Plus v4.8.0) */
-- @"c:\Database\packages\Common\gm_pkg_cm_override.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_cm_override
IS
--
/*******************************************************
 * Purpose: Package holds Override Procedures
 * Created By VPrasath
 *******************************************************/
--
/**************************************************************************
 * Purpose: This Procedure is used to call the
 * corrresponding procedures for the given override id
 *************************************************************************/
--
	PROCEDURE gm_cm_save_override_details (
		p_type		   IN	VARCHAR2
	  , p_ref_id	   IN	VARCHAR2
	  , p_new_value    IN	VARCHAR2
	  , p_ref_type	   IN	t4031_growth_details.c901_ref_type%TYPE
	  , p_no		   IN	VARCHAR2
	  , p_user_id	   IN	VARCHAR2
	  , p_ref_period   IN	VARCHAR2
	  , p_r_type	   IN	t4031_growth_details.c901_ref_type%TYPE
	  , part_type	   IN	VARCHAR2
	)
--
	AS
		v_id		   NUMBER;
	BEGIN
		IF p_type = '20395'
		THEN
			gm_cm_save_override_growth (p_ref_id, p_ref_period, p_no, p_ref_type, p_new_value, p_r_type, p_user_id);
		ELSIF p_type = '20396'
		THEN
			gm_cm_save_override_sheet (p_ref_id, p_new_value, p_ref_type, p_no, p_user_id, part_type);
		ELSIF p_type = '20397'
		THEN
			gm_cm_save_override_ttp (p_ref_id, p_new_value, p_user_id);
		ELSIF p_type = '20398'
		THEN
			SELECT c4031_growth_details_id
			  INTO v_id
			  FROM t4043_demand_sheet_growth t4043, t4031_growth_details t4031
			 WHERE t4043.c4030_demand_growth_id = t4031.c4030_demand_growth_id
			   AND t4043.c4043_demand_sheet_growth_id = p_ref_id
			   AND t4031.c4031_start_date = TO_DATE (p_ref_period, 'Mon/YY');

			gm_cm_save_override_growth (v_id, p_ref_period, p_no, p_ref_type, p_new_value, p_r_type, p_user_id);
			gm_cm_save_override_group (p_ref_id, p_new_value, p_user_id, p_r_type, p_ref_type);
		ELSIF p_type = '20399'
		THEN
			SELECT c4031_growth_details_id
			  INTO v_id
			  FROM t4043_demand_sheet_growth t4043, t4031_growth_details t4031
			 WHERE t4043.c4030_demand_growth_id = t4031.c4030_demand_growth_id
			   AND t4043.c4043_demand_sheet_growth_id = p_ref_id
			   AND t4031.c4031_start_date = TO_DATE (p_ref_period, 'Mon/YY');

			gm_cm_save_override_growth (v_id, p_ref_period, p_no, p_ref_type, p_new_value, p_r_type, p_user_id);
			gm_cm_save_override_set (p_ref_id, p_new_value, p_user_id, p_r_type, p_ref_type);
		END IF;
	END gm_cm_save_override_details;

--
/**************************************************************************
 * Purpose: This Procedure is used to save the override
 * values for growth
 *************************************************************************/
--
	PROCEDURE gm_cm_save_override_growth (
		p_ref_id	   IN	VARCHAR2
	  , p_ref_period   IN	VARCHAR2
	  , p_no		   IN	VARCHAR2
	  , p_type		   IN	VARCHAR2
	  , p_new_value    IN	VARCHAR2
	  , p_ref_type	   IN	t4031_growth_details.c901_ref_type%TYPE
	  , p_user_id	   IN	VARCHAR2
	)
	AS
		v_value 	   VARCHAR2 (20);
		v_updated_id   VARCHAR2 (20);
		v_dmaster_id   VARCHAR2 (20);
		v_ref_id	   VARCHAR2 (20);
		v_ref_type	   NUMBER;
		v_cnt		   NUMBER := 0;
	BEGIN
		SELECT t4030.c4030_ref_id ref_id, t4030.c4020_demand_master_id dmaster_id, t4030.c901_ref_type ref_type
		  INTO v_ref_id, v_dmaster_id, v_ref_type
		  FROM t4030_demand_growth_mapping t4030, t4031_growth_details t4031
		 WHERE t4031.c4030_demand_growth_id = t4030.c4030_demand_growth_id AND t4031.c4031_growth_details_id = p_ref_id;

		IF v_ref_type = 20295
		THEN
			IF (p_type = '20411')
			THEN
				SELECT COUNT (t4030.c4030_ref_id)
				  INTO v_cnt
				  FROM t4030_demand_growth_mapping t4030, t4031_growth_details t4031
				 WHERE t4030.c4030_ref_id IN (SELECT t4011.c4010_group_id
												FROM t4011_group_detail t4011
											   WHERE c205_part_number_id = v_ref_id)
				   AND t4030.c4020_demand_master_id = v_dmaster_id
				   AND t4030.c4030_void_fl IS NULL
				   AND t4031.c4030_demand_growth_id = t4030.c4030_demand_growth_id
				   AND TO_CHAR (t4031.c4031_start_date, 'Mon/YY') >= p_ref_period
				   AND t4031.c4031_value IS NOT NULL;
			ELSE
				SELECT COUNT (t4030.c4030_ref_id)
				  INTO v_cnt
				  FROM t4030_demand_growth_mapping t4030, t4031_growth_details t4031
				 WHERE t4030.c4030_ref_id IN (SELECT t4011.c4010_group_id
												FROM t4011_group_detail t4011
											   WHERE c205_part_number_id = v_ref_id)
				   AND t4030.c4020_demand_master_id = v_dmaster_id
				   AND t4030.c4030_void_fl IS NULL
				   AND t4031.c4030_demand_growth_id = t4030.c4030_demand_growth_id
				   AND TO_CHAR (t4031.c4031_start_date, 'Mon/YY') = p_ref_period
				   AND t4031.c4031_value IS NOT NULL;

				IF v_cnt = 0
				THEN
					--
					raise_application_error (-20759, '');
				--
				END IF;
			END IF;
		END IF;

		IF p_type = '20410'
		THEN
			UPDATE t4031_growth_details
			   SET c4031_value = p_new_value
				 , c4031_last_updated_by = p_user_id
				 , c4031_last_updated_date = SYSDATE
				 , c901_ref_type = p_ref_type
			 WHERE c4031_growth_details_id = p_ref_id;
		ELSIF p_type = '20411'
		THEN
			UPDATE t4031_growth_details
			   SET c4031_value = p_new_value
				 , c4031_last_updated_by = p_user_id
				 , c4031_last_updated_date = SYSDATE
				 , c901_ref_type = p_ref_type
			 WHERE c4030_demand_growth_id IN (SELECT c4030_demand_growth_id
												FROM t4031_growth_details
											   WHERE c4031_growth_details_id = p_ref_id)
			   AND c4031_start_date >= TO_DATE (p_ref_period, 'Mon/YY');		
          IF v_ref_type = 20295
          THEN   
              IF (SQL%ROWCOUNT <> v_cnt)
              THEN
                raise_application_error (-20759, '');
              END IF;
          END IF;
		END IF;
	END gm_cm_save_override_growth;

--
/**************************************************************************
 * Purpose: This Procedure is used to save the override for Demand sheet
 *************************************************************************/
--
	PROCEDURE gm_cm_save_override_sheet (
		p_ref_id	  IN   VARCHAR2
	  , p_new_value   IN   VARCHAR2
	  , p_type		  IN   t4031_growth_details.c901_ref_type%TYPE
	  , p_no		  IN   VARCHAR2
	  , p_user_id	  IN   VARCHAR2
	  , part_type	  IN   VARCHAR2
	)
	AS
		v_dsmonthid    NUMBER;

		CURSOR cur_ds_detail
		IS
			SELECT c4042_demand_sheet_detail_id dsid
			  FROM t4042_demand_sheet_detail
			 WHERE c4040_demand_sheet_id = v_dsmonthid
			   AND c205_part_number_id = p_no
			   AND c4042_ref_id = part_type
			   AND c901_type = 50563;
	BEGIN
		IF p_type = 20410
		THEN
			UPDATE t4042_demand_sheet_detail
			   SET c4042_qty = p_new_value
				 , c4042_updated_by = p_user_id
				 , c4042_updated_date = SYSDATE
			 WHERE c4042_demand_sheet_detail_id = p_ref_id;

			gm_pkg_cm_override.gm_cm_save_override_subpart (p_ref_id);
		ELSIF p_type = 20411
		THEN
			SELECT c4040_demand_sheet_id
			  INTO v_dsmonthid
			  FROM t4042_demand_sheet_detail
			 WHERE c4042_demand_sheet_detail_id = p_ref_id;

			UPDATE t4042_demand_sheet_detail
			   SET c4042_qty = p_new_value
				 , c4042_updated_by = p_user_id
				 , c4042_updated_date = SYSDATE
			 WHERE c4042_demand_sheet_detail_id IN (
					   SELECT c4042_demand_sheet_detail_id
						 FROM t4042_demand_sheet_detail
						WHERE c4040_demand_sheet_id = v_dsmonthid
						  AND c205_part_number_id = p_no
						  AND c4042_ref_id = part_type
						  AND c901_type = 50563);

			FOR cur_val IN cur_ds_detail
			LOOP
				gm_pkg_cm_override.gm_cm_save_override_subpart (cur_val.dsid);
			END LOOP;
		END IF;
	END gm_cm_save_override_sheet;

--
/**************************************************************************
 * Purpose: This Procedure is used to save the override
 * values for values in the TTP Summary
 *************************************************************************/
--
	PROCEDURE gm_cm_save_override_ttp (
		p_ref_id	  IN   VARCHAR2
	  , p_new_value   IN   VARCHAR2
	  , p_user_id	  IN   VARCHAR2
	)
	AS
		v_value 	   VARCHAR2 (20);
		v_updated_id   VARCHAR2 (20);
	BEGIN
		UPDATE t4053_ttp_part_detail
		   SET c4053_qty = p_new_value
			 , c4053_last_updated_by = p_user_id
			 , c4053_last_updated_date = SYSDATE
		 WHERE c4053_ttp_part_id = p_ref_id;
	END gm_cm_save_override_ttp;

--
/**************************************************************************
 * Purpose: This Procedure is used to fetch the overriden
 * ref id for growth
 *************************************************************************/
--
	PROCEDURE gm_cm_fch_override_refid (
		p_ref_id		 IN 	  VARCHAR2
	  , p_ref_period	 IN 	  VARCHAR2
	  , p_no			 IN 	  VARCHAR2
	  , p_over_type_id	 IN 	  NUMBER
	  , p_dem_sheet_id	 IN 	  NUMBER
	  , p_part_type 	 IN 	  VARCHAR2
	  , p_userid		 IN 	  NUMBER
	  , p_updated_id	 OUT	  VARCHAR2
	)
	AS
		v_updated_id   NUMBER;
	BEGIN
		--
		IF p_over_type_id = 20395
		THEN
			IF p_ref_id = p_no AND p_dem_sheet_id <> 0
			THEN
				SELECT DISTINCT t4031.c4031_growth_details_id
						   INTO p_updated_id
						   FROM t4030_demand_growth_mapping t4030, t4031_growth_details t4031, t4040_demand_sheet t4040
						  WHERE t4030.c4030_demand_growth_id = t4031.c4030_demand_growth_id
							AND t4040.c4020_demand_master_id = t4030.c4020_demand_master_id
							AND t4031.c4031_void_fl IS NULL
							AND c4030_ref_id = p_no
							AND t4040.c4040_demand_sheet_id = p_dem_sheet_id
							AND t4031.c4031_start_date = TO_DATE (p_ref_period, 'Mon/YY');
			ELSE
				SELECT t4031.c4031_growth_details_id
				  INTO p_updated_id
				  FROM t4030_demand_growth_mapping t4030, t4031_growth_details t4031
				 WHERE t4030.c4030_demand_growth_id = t4031.c4030_demand_growth_id
				   AND t4031.c4031_void_fl IS NULL
				   AND t4030.c4030_demand_growth_id = p_ref_id
				   AND c4030_ref_id = p_no
				   AND t4031.c4031_start_date = TO_DATE (p_ref_period, 'Mon/YY');
			END IF;
		ELSIF p_over_type_id = 20396
		THEN
			SELECT t4042.c4042_demand_sheet_detail_id
			  INTO p_updated_id
			  FROM t4040_demand_sheet t4040, t4042_demand_sheet_detail t4042
			 WHERE t4040.c4040_demand_sheet_id = t4042.c4040_demand_sheet_id
			   AND t4040.c4020_demand_master_id = p_ref_id
			   AND t4040.c4040_demand_sheet_id = p_dem_sheet_id
			   AND t4042.c205_part_number_id = p_no
			   AND TO_CHAR (t4042.c4042_period, 'Mon/YY') = p_ref_period
			   AND t4042.c4042_ref_id = p_part_type
			   AND t4042.c901_type = 50563;
		ELSIF p_over_type_id = 20397
		THEN
			SELECT t4053.c4053_ttp_part_id
			  INTO p_updated_id
			  FROM t4052_ttp_detail t4052, t4053_ttp_part_detail t4053
			 WHERE t4052.c4050_ttp_id = p_ref_id
			   AND t4052.c4052_ttp_detail_id = t4053.c4052_ttp_detail_id
			   AND t4053.c205_part_number_id = p_no;
		ELSIF p_over_type_id = 20398 OR p_over_type_id = 20399
		THEN
			SELECT t4043.c4043_demand_sheet_growth_id
			  INTO p_updated_id
			  FROM t4030_demand_growth_mapping t4030, t4043_demand_sheet_growth t4043
			 WHERE t4030.c4030_demand_growth_id = t4043.c4030_demand_growth_id
			   AND t4030.c4030_ref_id = p_ref_id
			   AND TO_CHAR (t4043.c4043_end_date, 'Mon/YY') = p_ref_period
			   AND t4043.c4040_demand_sheet_id = p_dem_sheet_id;
		END IF;
	--
	END gm_cm_fch_override_refid;

--
/**************************************************************************
 * Purpose: This Procedure is used to fetch the overriden history
 *************************************************************************/
--
	PROCEDURE gm_cm_fch_override_history (
		p_ref_id	 IN 	  VARCHAR2
	  , p_trail_id	 IN 	  NUMBER
	  , p_data		 OUT	  TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_data
		 FOR
			 SELECT   c941_audit_trail_log_id logid, c941_value VALUE, c941_ref_id
					, get_user_name (c941_updated_by) updby, TO_CHAR (c941_updated_date, 'mm/dd/yyyy') udate
					, c940_audit_trail_id
				 FROM t941_audit_trail_log
				WHERE c941_ref_id = p_ref_id AND c940_audit_trail_id = p_trail_id
			 ORDER BY c941_audit_trail_log_id DESC;
	END gm_cm_fch_override_history;

--
/**************************************************************************
 * Purpose: This Procedure is used to save the override
 * values for values in the Demand Sheet
 *************************************************************************/
--
	PROCEDURE gm_cm_save_override_group (
		p_ref_id	  IN   VARCHAR2
	  , p_new_value   IN   VARCHAR2
	  , p_user_id	  IN   VARCHAR2
	  , p_r_type	  IN   t4031_growth_details.c901_ref_type%TYPE
	  , p_ref_type	  IN   t4031_growth_details.c901_ref_type%TYPE
	)
	AS
		v_monyr 	   VARCHAR2 (10);
		v_reftype	   NUMBER;
		v_demtype	   NUMBER;
		v_demand_sheetid NUMBER;
		v_qty		   NUMBER;
		v_qtyupd	   NUMBER;
		v_sheetid	   NUMBER;
		v_period	   VARCHAR2 (10);
		v_pnum		   VARCHAR2 (20);
		v_total 	   NUMBER;
		v_var		   NUMBER;
		v_setid 	   VARCHAR2 (20);
		--
		v_part_number  t205_part_number.c205_part_number_id%TYPE;
		v_part_number_curr t205_part_number.c205_part_number_id%TYPE;
		--
		v_previous_qty t4042_demand_sheet_detail.c4042_qty%TYPE;
		--
		v_firstloop    CHAR (1);
		v_divvalue	   NUMBER;
		v_temp_ref_id  VARCHAR2 (20);

		--
		CURSOR cur_demandsheetids
		IS
			SELECT t4042.c4042_demand_sheet_detail_id fid, t4042.c4042_qty qty, t4042.c205_part_number_id part
				 , growthdetail.fqty, dmdweightavg.weight_avg
			  FROM t4042_demand_sheet_detail t4042
				 , (SELECT t4043.c4040_demand_sheet_id, t4030.c4030_ref_id, t4030.c901_ref_type, t4043.c4043_value fqty
					  FROM t4043_demand_sheet_growth t4043, t4030_demand_growth_mapping t4030
					 WHERE t4043.c4043_demand_sheet_growth_id = p_ref_id
					   AND t4030.c4030_demand_growth_id = t4043.c4030_demand_growth_id) growthdetail
				 , (SELECT t4042.c205_part_number_id, t4042.c4042_period, t4042.c4042_qty weight_avg
					  FROM t4042_demand_sheet_detail t4042
					 WHERE t4042.c4040_demand_sheet_id = (SELECT t4043.c4040_demand_sheet_id
															FROM t4043_demand_sheet_growth t4043
														   WHERE t4043.c4043_demand_sheet_growth_id = p_ref_id)
					   AND t4042.c901_type = 50562) dmdweightavg
			 WHERE growthdetail.c4040_demand_sheet_id = t4042.c4040_demand_sheet_id
			   AND t4042.c4042_ref_id =
					   DECODE (growthdetail.c901_ref_type
							 , 20295, t4042.c4042_ref_id
							 , growthdetail.c4030_ref_id
							  )   -- If mapped to group fetch set info
			   AND t4042.c205_part_number_id =
					   DECODE (growthdetail.c901_ref_type
							 , 20295, growthdetail.c4030_ref_id
							 , t4042.c205_part_number_id
							  )   -- If mapped to part fetch part detail
			   AND t4042.c901_type = 50563
			   AND t4042.c205_part_number_id = dmdweightavg.c205_part_number_id
			   AND t4042.c4042_period = DECODE (p_ref_type, '20410', TO_DATE (v_monyr, 'Mon/YY'), t4042.c4042_period)
			   AND t4042.c4042_period >= TO_DATE (v_monyr, 'Mon/YY')
			   AND t4042.c205_part_number_id NOT IN (
					   SELECT DISTINCT c4030_ref_id
								  FROM t4030_demand_growth_mapping t4030
									 , t4031_growth_details t4031
									 , (SELECT t4040.c4020_demand_master_id, t4030.c901_ref_type
										  FROM t4043_demand_sheet_growth t4043
											 , t4040_demand_sheet t4040
											 , t4030_demand_growth_mapping t4030
										 WHERE t4043.c4043_demand_sheet_growth_id = p_ref_id
										   AND t4030.c4030_demand_growth_id = t4043.c4030_demand_growth_id
										   AND t4040.c4040_demand_sheet_id = t4043.c4040_demand_sheet_id) growthdetail
								 WHERE t4030.c4020_demand_master_id = growthdetail.c4020_demand_master_id
								   AND t4030.c901_ref_type = DECODE (growthdetail.c901_ref_type, 20297, 20295, -999)
								   AND t4031.c4030_demand_growth_id = t4030.c4030_demand_growth_id
								   AND t4031.c4031_start_date >= TO_DATE (v_monyr, 'Mon/YY')
								   AND t4031.c4031_end_date <= LAST_DAY (TO_DATE (v_monyr, 'Mon/YY'))
								   AND t4030.c4030_void_fl IS NULL
								   AND t4031.c4031_value IS NOT NULL);

		-- For Forecast Average
		CURSOR cur_forecast
		IS
			SELECT	 t4042.c4042_ref_id refid, c205_part_number_id pnum, ROUND (AVG (t4042.c4042_qty), 0) ftotal
				FROM t4042_demand_sheet_detail t4042
				   , (SELECT t4043.c4040_demand_sheet_id, t4030.c4030_ref_id, t4030.c901_ref_type
						   , t4043.c4043_value fqty
						FROM t4043_demand_sheet_growth t4043, t4030_demand_growth_mapping t4030
					   WHERE t4043.c4043_demand_sheet_growth_id = p_ref_id
						 AND t4030.c4030_demand_growth_id = t4043.c4030_demand_growth_id) growthdetail
			   WHERE growthdetail.c4040_demand_sheet_id = t4042.c4040_demand_sheet_id
				 AND t4042.c901_type = 50563
				 AND t4042.c4042_ref_id =
						 DECODE (growthdetail.c901_ref_type
							   , 20295, t4042.c4042_ref_id
							   , growthdetail.c4030_ref_id
								)	-- If mapped to group fetch set info
				 AND t4042.c205_part_number_id =
						 DECODE (growthdetail.c901_ref_type
							   , 20295, growthdetail.c4030_ref_id
							   , t4042.c205_part_number_id
								)	-- If mapped to part fetch part detail
			GROUP BY t4042.c4042_ref_id, c205_part_number_id;

		CURSOR cur_variance
		IS
			SELECT	 t4042.c205_part_number_id pnum, t4042.c4042_ref_id refid
				   , (	SUM (DECODE (t4042.c901_type, 50561, t4042.c4042_qty, 0))
					  - SUM (DECODE (t4042.c901_type, 50564, t4042.c4042_qty, 0))
					 ) mainval
				   , SUM (DECODE (t4042.c901_type, 50564, t4042.c4042_qty, 0)) divvalue
				FROM t4042_demand_sheet_detail t4042
				   , (SELECT t4043.c4040_demand_sheet_id, t4030.c4030_ref_id, t4030.c901_ref_type
						   , t4043.c4043_value fqty
						FROM t4043_demand_sheet_growth t4043, t4030_demand_growth_mapping t4030
					   WHERE t4043.c4043_demand_sheet_growth_id = p_ref_id
						 AND t4030.c4030_demand_growth_id = t4043.c4030_demand_growth_id) growthdetail
			   WHERE growthdetail.c4040_demand_sheet_id = t4042.c4040_demand_sheet_id
				 AND t4042.c901_type IN (50561, 50564)
				 AND t4042.c4042_ref_id =
						 DECODE (growthdetail.c901_ref_type
							   , 20295, t4042.c4042_ref_id
							   , growthdetail.c4030_ref_id
								)	-- If mapped to group fetch set info
				 AND t4042.c205_part_number_id =
						 DECODE (growthdetail.c901_ref_type
							   , 20295, growthdetail.c4030_ref_id
							   , t4042.c205_part_number_id
								)	-- If mapped to part fetch part detail
			GROUP BY t4042.c205_part_number_id, t4042.c4042_ref_id;

		CURSOR cur_ds_detail_fc
		IS
			SELECT t4042.c4042_demand_sheet_detail_id dsid
			  FROM t4042_demand_sheet_detail t4042
			 WHERE c205_part_number_id = v_pnum
			   AND c901_type = 50564
			   AND c4040_demand_sheet_id = v_sheetid
			   AND c4042_ref_id = v_temp_ref_id;

		CURSOR cur_ds_detail_var
		IS
			SELECT t4042.c4042_demand_sheet_detail_id dsid
			  FROM t4042_demand_sheet_detail t4042
			 WHERE c205_part_number_id = v_pnum
			   AND c901_type = 50565
			   AND c4040_demand_sheet_id = v_sheetid
			   AND c4042_ref_id = v_temp_ref_id;
	BEGIN
		--
		SELECT TO_CHAR (t4043.c4043_end_date, 'Mon/YY') mnyr, t4043.c901_ref_type, t4020.c901_demand_type
			 , t4040.c4040_demand_sheet_id, t4040.c4040_forecast_period fperiod, t4030.c4030_ref_id
		  INTO v_monyr, v_reftype, v_demtype
			 , v_sheetid, v_period, v_setid
		  FROM t4043_demand_sheet_growth t4043
			 , t4040_demand_sheet t4040
			 , t4020_demand_master t4020
			 , t4030_demand_growth_mapping t4030
		 WHERE t4043.c4040_demand_sheet_id = t4040.c4040_demand_sheet_id
		   AND t4020.c4020_demand_master_id = t4040.c4020_demand_master_id
		   AND t4043.c4030_demand_growth_id = t4030.c4030_demand_growth_id
		   AND t4043.c4043_demand_sheet_growth_id = p_ref_id;

		--
		-- SQL to update sheet growth detail for current month
		-- 20410 maps to Current Month	and 20411 maps to following month
		IF p_ref_type = 20410
		THEN
			UPDATE t4043_demand_sheet_growth
			   SET c4043_value = p_new_value
				 , c901_ref_type = p_r_type
			 WHERE c4043_demand_sheet_growth_id = p_ref_id;
		-- SQL to update sheet growth detail for current and following month
		ELSIF p_ref_type = 20411
		THEN
			UPDATE t4043_demand_sheet_growth
			   SET c4043_value = p_new_value
				 , c901_ref_type = p_r_type
			 WHERE c4043_demand_sheet_growth_id IN (
					   SELECT t4043.c4043_demand_sheet_growth_id
						 FROM t4043_demand_sheet_growth t4043, t4030_demand_growth_mapping t4030
						WHERE t4030.c4030_demand_growth_id = t4043.c4030_demand_growth_id
						  AND t4043.c4040_demand_sheet_id = v_sheetid
						  AND t4043.c4043_start_date >= TO_DATE (v_monyr, 'Mon/YY')
						  AND t4030.c4030_ref_id = v_setid);
		END IF;

		--
		--
		v_part_number := '!@';	 -- Default value First time check
		v_firstloop := '';

		--gm_procedure_log ('Set Info ' || ' - ' || p_ref_id, 'p_ref_type - ' || v_monyr);
		FOR cur_val IN cur_demandsheetids
		LOOP
			v_part_number_curr := cur_val.part;

			IF (v_part_number != v_part_number_curr)
			THEN
				v_part_number := v_part_number_curr;
				v_previous_qty := cur_val.weight_avg;
				v_firstloop := 'Y';
			END IF;

			-- 20380 refers to percentage
			IF p_r_type = 20380
			THEN
				v_qtyupd	:= ROUND (v_previous_qty + (v_previous_qty * cur_val.fqty) / 100, 0);
			ELSIF p_r_type = 20381
			THEN
				v_qtyupd	:= cur_val.fqty;
			END IF;

			v_demand_sheetid := cur_val.fid;

			--gm_procedure_log ('Part # ' || ' - ' || cur_val.part, 'Update QTY - ' || v_qtyupd);
			-- Update the caculated valye
			UPDATE t4042_demand_sheet_detail
			   SET c4042_qty = v_qtyupd
				 , c4042_updated_by = p_user_id
				 , c4042_updated_date = SYSDATE
			 WHERE c4042_demand_sheet_detail_id = v_demand_sheetid;

			gm_pkg_cm_override.gm_cm_save_override_subpart (v_demand_sheetid);
			-- Reassign the current qty to previous qty for calcuation
			v_previous_qty := v_qtyupd;
			v_firstloop := 'N';
		--
		END LOOP;

		--
		FOR cur_val IN cur_forecast
		LOOP
			v_pnum		:= cur_val.pnum;
			v_total 	:= cur_val.ftotal;
			v_temp_ref_id := cur_val.refid;

			--
			UPDATE t4042_demand_sheet_detail
			   SET c4042_qty = v_total
				 , c4042_updated_by = p_user_id
				 , c4042_updated_date = SYSDATE
			 WHERE c205_part_number_id = v_pnum
			   AND c901_type = 50564
			   AND c4040_demand_sheet_id = v_sheetid
			   AND c4042_ref_id = cur_val.refid;

			FOR cur_val_temp IN cur_ds_detail_fc
			LOOP
				gm_pkg_cm_override.gm_cm_save_override_subpart (cur_val_temp.dsid);
			END LOOP;
		--
		END LOOP;

		FOR cur_val IN cur_variance
		LOOP
			v_pnum		:= cur_val.pnum;
			v_temp_ref_id := cur_val.refid;

			SELECT DECODE (cur_val.divvalue, 0, 1, cur_val.divvalue)
			  INTO v_divvalue
			  FROM DUAL;

			v_var		:= (cur_val.mainval * 100) / v_divvalue;

			--
			UPDATE t4042_demand_sheet_detail
			   SET c4042_qty = v_var
				 , c4042_updated_by = p_user_id
				 , c4042_updated_date = SYSDATE
			 WHERE c205_part_number_id = v_pnum
			   AND c901_type = 50565
			   AND c4040_demand_sheet_id = v_sheetid
			   AND c4042_ref_id = cur_val.refid;

			FOR cur_val_temp IN cur_ds_detail_var
			LOOP
				gm_pkg_cm_override.gm_cm_save_override_subpart (cur_val_temp.dsid);
			END LOOP;
		--
		END LOOP;
	END gm_cm_save_override_group;

--

	--
/**************************************************************************
 * Purpose: This Procedure is used to save the override
 * values for Sets in the Demand Sheet
 *************************************************************************/
--
	PROCEDURE gm_cm_save_override_set (
		p_ref_id	  IN   VARCHAR2
	  , p_new_value   IN   VARCHAR2
	  , p_user_id	  IN   VARCHAR2
	  , p_r_type	  IN   t4031_growth_details.c901_ref_type%TYPE
	  , p_ref_type	  IN   t4031_growth_details.c901_ref_type%TYPE
	)
	AS
		v_monyr 	   VARCHAR2 (10);
		v_reftype	   NUMBER;
		v_demtype	   NUMBER;
		v_growth_type  NUMBER;
		v_demand_sheetid NUMBER;
		v_qty		   NUMBER;
		v_qtyupd	   NUMBER;
		v_sheetid	   NUMBER;
		v_period	   VARCHAR2 (10);
		v_pnum		   VARCHAR2 (20);
		v_total 	   NUMBER;
		v_var		   NUMBER;
		v_refid 	   VARCHAR2 (20);	-- This variable will hold either set or part value
		v_temp_refid   VARCHAR2 (20);
		v_temp_pnum    VARCHAR2 (20);

		--
		CURSOR cur_set
		IS
			SELECT c205_part_number_id pnum, c208_set_qty qty
			  FROM t208_set_details
			 WHERE c207_set_id = v_refid AND c208_inset_fl = 'Y' AND c208_set_qty > 0;

		-- get the demand sheet detail id, to over ride the sub parts
		CURSOR cur_ds_detail
		IS
			SELECT t4042.c4042_demand_sheet_detail_id dsid
			  FROM t4042_demand_sheet_detail t4042
			 WHERE c205_part_number_id = v_temp_pnum
			   AND c901_type = 50563
			   AND c4040_demand_sheet_id = v_sheetid
			   AND c4042_period = DECODE (p_ref_type, 20410, TO_DATE (v_monyr, 'Mon/YY'), c4042_period)
			   AND c4042_period >= TO_DATE (v_monyr, 'Mon/YY')
			   AND c4042_ref_id = v_temp_refid;
	BEGIN
		--
		SELECT TO_CHAR (t4043.c4043_end_date, 'Mon/YY') mnyr, t4043.c901_ref_type, t4020.c901_demand_type
			 , t4040.c4040_demand_sheet_id, t4040.c4040_forecast_period fperiod, t4030.c4030_ref_id
			 , t4030.c901_ref_type
		  INTO v_monyr, v_reftype, v_demtype
			 , v_sheetid, v_period, v_refid
			 , v_growth_type
		  FROM t4043_demand_sheet_growth t4043
			 , t4040_demand_sheet t4040
			 , t4020_demand_master t4020
			 , t4030_demand_growth_mapping t4030
		 WHERE t4043.c4040_demand_sheet_id = t4040.c4040_demand_sheet_id
		   AND t4020.c4020_demand_master_id = t4040.c4020_demand_master_id
		   AND t4043.c4030_demand_growth_id = t4030.c4030_demand_growth_id
		   AND t4043.c4043_demand_sheet_growth_id = p_ref_id;

		--
		-- SQL to update sheet growth detail for current month and following month
		-- 20410 maps to Current Month	and 20411 maps to following month
		IF p_ref_type = 20410
		THEN
			UPDATE t4043_demand_sheet_growth
			   SET c4043_value = p_new_value
				 , c901_ref_type = p_r_type
			 WHERE c4043_demand_sheet_growth_id = p_ref_id;
		ELSIF p_ref_type = 20411
		THEN
			UPDATE t4043_demand_sheet_growth
			   SET c4043_value = p_new_value
				 , c901_ref_type = p_r_type
			 WHERE c4043_demand_sheet_growth_id IN (
					   SELECT t4043.c4043_demand_sheet_growth_id
						 FROM t4043_demand_sheet_growth t4043, t4030_demand_growth_mapping t4030
						WHERE t4030.c4030_demand_growth_id = t4043.c4030_demand_growth_id
						  AND t4043.c4040_demand_sheet_id = v_sheetid
						  AND t4043.c4043_start_date >= TO_DATE (v_monyr, 'Mon/YY')
						  AND t4030.c4030_ref_id = v_refid);
		END IF;

		--gm_procedure_log ('Set Info ' || ' - ' || p_ref_id, 'p_ref_type - ' || p_ref_type);

		--
		-- If user changing the set value then execute below code
		IF (v_growth_type = 20296)
		THEN
			FOR cur_val IN cur_set
			LOOP
				v_pnum		:= cur_val.pnum;
				v_qty		:= cur_val.qty;

				IF p_r_type = 20381
				THEN
					v_total 	:= v_qty * p_new_value;
					v_temp_refid := v_refid;
					v_temp_pnum := v_pnum;

					--
					-- Based on the p_ref_type it will update current or current and following month
					UPDATE t4042_demand_sheet_detail
					   SET c4042_qty = v_total
						 , c4042_updated_by = p_user_id
						 , c4042_updated_date = SYSDATE
					 WHERE c205_part_number_id = v_pnum
					   AND c901_type = 50563
					   AND c4040_demand_sheet_id = v_sheetid
					   AND c4042_period = DECODE (p_ref_type, 20410, TO_DATE (v_monyr, 'Mon/YY'), c4042_period)
					   AND c4042_period >= TO_DATE (v_monyr, 'Mon/YY')
					   AND c4042_ref_id = v_refid;

					FOR cur_val IN cur_ds_detail
					LOOP
						gm_pkg_cm_override.gm_cm_save_override_subpart (cur_val.dsid);
					END LOOP;
				END IF;
			--
			END LOOP;
		-- If its mapped to part
		ELSIF (v_growth_type = 20298)
		THEN
			--
			v_temp_refid := -9999;
			v_temp_pnum := v_refid;

			UPDATE t4042_demand_sheet_detail
			   SET c4042_qty = p_new_value
				 , c4042_updated_by = p_user_id
				 , c4042_updated_date = SYSDATE
			 WHERE c205_part_number_id = v_refid
			   AND c901_type = 50563
			   AND c4040_demand_sheet_id = v_sheetid
			   AND c4042_period = DECODE (p_ref_type, 20410, TO_DATE (v_monyr, 'Mon/YY'), c4042_period)
			   AND c4042_period >= TO_DATE (v_monyr, 'Mon/YY')
			-- We have to pass the parameter for c4042_ref_id as a valid number that should be in single Quotes 
			-- and error is due to the invalid number that is passed before.
			   AND c4042_ref_id = '-9999';  
			
			FOR cur_val IN cur_ds_detail
			LOOP
				gm_pkg_cm_override.gm_cm_save_override_subpart (cur_val.dsid);
			END LOOP;
		--
		END IF;
	END gm_cm_save_override_set;

--
/**************************************************************************
 * Purpose: This Procedure is used to save the override
 *			the sub parts
 *************************************************************************/
--
	PROCEDURE gm_cm_save_override_subpart (
		p_demand_sheet_detail_id   IN	t4042_demand_sheet_detail.c4042_demand_sheet_detail_id%TYPE
	)
	AS
		v_ds_id 	   t4042_demand_sheet_detail.c4040_demand_sheet_id%TYPE;
		v_pnum		   t4042_demand_sheet_detail.c205_part_number_id%TYPE;
		v_period	   t4042_demand_sheet_detail.c4042_period%TYPE;
		v_ref_id	   t4042_demand_sheet_detail.c4042_ref_id%TYPE;
		v_qty		   t4042_demand_sheet_detail.c4042_qty%TYPE;
		v_type		   t4042_demand_sheet_detail.c901_type%TYPE;
		v_user_id	   t4042_demand_sheet_detail.c4042_updated_by%TYPE;
	BEGIN
		SELECT t4042.c4040_demand_sheet_id, t4042.c205_part_number_id, t4042.c4042_period, t4042.c4042_ref_id
			 , t4042.c4042_qty, t4042.c901_type, t4042.c4042_updated_by
		  INTO v_ds_id, v_pnum, v_period, v_ref_id
			 , v_qty, v_type, v_user_id
		  FROM t4042_demand_sheet_detail t4042
		 WHERE t4042.c4042_demand_sheet_detail_id = p_demand_sheet_detail_id;

		UPDATE t4042_demand_sheet_detail t4042
		   SET t4042.c4042_qty = v_qty * NVL (t4042.c4042_parent_qty_needed, 1)
			 , c4042_updated_by = v_user_id
			 , c4042_updated_date = SYSDATE
		 WHERE t4042.c205_parent_part_num_id = v_pnum
		   AND c901_type = v_ref_id
		   AND c4040_demand_sheet_id = v_ds_id
		   AND c4042_period = v_period
		   AND t4042.c4042_parent_ref_id = v_ref_id;
	END gm_cm_save_override_subpart;
END gm_pkg_cm_override;
/
