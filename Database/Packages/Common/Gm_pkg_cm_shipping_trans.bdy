/* Formatted on 2012/05/24 16:40 (Formatter Plus v4.8.0) */
--@"C:\database\packages\common\GM_PKG_CM_SHIPPING_TRANS.bdy";

CREATE OR REPLACE PACKAGE BODY "GM_PKG_CM_SHIPPING_TRANS"
IS
	 /*****************************************************************************************************
	* Description : This is the Parent Procedure. This will internally call gm_sav_ship_info procedure
	*******************************************************************************************************/
/*This procedure is calling by ETL Job, If there is any changes then please coordinate with DWH team during design phase */
	PROCEDURE gm_sav_shipping (
	    p_refid 	   IN		t907_shipping_info.c907_ref_id%TYPE
	  , p_source	   IN		t907_shipping_info.c901_source%TYPE
	  , p_shipto	   IN		t907_shipping_info.c901_ship_to%TYPE
	  , p_shiptoid	   IN		t907_shipping_info.c907_ship_to_id%TYPE
	  , p_shipcarr	   IN		t907_shipping_info.c901_delivery_carrier%TYPE
	  , p_shipmode	   IN		t907_shipping_info.c901_delivery_mode%TYPE
	  , p_trackno	   IN		t907_shipping_info.c907_tracking_number%TYPE
	  , p_addressid    IN		t907_shipping_info.c106_address_id%TYPE
	  , p_freightamt   IN		VARCHAR2
	  , p_userid	   IN		t907_shipping_info.c907_last_updated_by%TYPE
	  , p_shipid	   IN OUT	VARCHAR2
	  , p_attn		   IN	   t907_shipping_info.c907_override_attn_to%TYPE DEFAULT NULL
	  , p_ship_instruc IN	   t907_shipping_info.c907_ship_instruction%TYPE DEFAULT NULL
	  , p_pack_station IN 	  t907_shipping_info.c907_pack_station_id%TYPE  DEFAULT NULL
	  , p_tote		   IN 	  t907_shipping_info.c907_ship_tote_id%TYPE  DEFAULT NULL
	)
	AS
		v_shipcarr	   t907_shipping_info.c901_delivery_carrier%TYPE;
		v_shipmode	   t907_shipping_info.c901_delivery_mode%TYPE;
		v_shipto	   t907_shipping_info.c901_ship_to%TYPE;
		v_childshipid  VARCHAR2 (100);
		v_shipid	   VARCHAR2 (100);
		v_trackno	   t907_shipping_info.c907_tracking_number%TYPE;
		v_track_no	   t907_shipping_info.c907_tracking_number%TYPE;
		-- velu edited
		v_freightamt   NUMBER;
		v_shiptoid	   VARCHAR2 (20);
		v_parent_shiptoid VARCHAR2 (20);
		v_address	   VARCHAR2 (100);
		v_parentaddress VARCHAR2 (100);
		v_stationid		t907_shipping_info.c907_pack_station_id%TYPE;
		v_shiptoteid	t907_shipping_info.c907_ship_tote_id%TYPE;
		v_company_id    t1900_company.c1900_company_id%TYPE;
		v_plant_id    t5040_plant_master.C5040_PLANT_ID%TYPE;
		v_tissue_count	NUMBER;
		
		CURSOR order_cursor   -- cursor to fetch backorder details for order
		IS
			SELECT c501_order_id
			  FROM t501_order
			 WHERE c501_parent_order_id = p_refid AND c501_void_fl IS NULL;

		CURSOR request_cursor
		-- cursor to fetch backorder details for consignments
		IS
			SELECT c520_request_id
			  FROM t520_request
			 WHERE c520_master_request_id = p_refid AND c520_void_fl IS NULL;

		CURSOR consign_cursor
		-- cursor to fetch the consignment id based on requestid
		IS
			SELECT c504_consignment_id
			  FROM t504_consignment
			 WHERE c520_request_id = p_refid AND c504_void_fl IS NULL;

		CURSOR request_detail_cursor
		-- to insert record into shipping for every rec in request detail tbl
		IS
			SELECT	 c526_product_request_detail_id reqdetailid
				FROM t526_product_request_detail
			   WHERE c525_product_request_id = p_refid
				 AND c526_void_fl IS NULL
				 AND c901_request_type = 4127
				 AND c526_status_fl NOT IN (30, 40)
			ORDER BY c525_product_request_id;
	
		CURSOR req_shipdt_cursor -- cursor to fetch request id and planned shipdate from request id.
		IS
			SELECT c520_request_id,c520_planned_ship_date pl_ship_dt
			  FROM t520_request
			 WHERE c520_request_id = p_refid AND c520_void_fl IS NULL;
			 
	CURSOR cons_shipdt_cursor	-- cursor to fetch consignment id and planned shipdate from request id.
		IS
			  SELECT t504.c504_consignment_id consid,t520.c520_planned_ship_date pl_ship_dt
			  FROM t504_consignment t504,t520_request t520
              WHERE t520.c520_request_id = t504.c520_request_id
			  AND t520.c520_request_id = p_refid AND t504.c504_void_fl IS NULL
              AND t520.c520_void_fl IS NULL;
              
	BEGIN
		SELECT DECODE (p_shipcarr, 0, NULL, p_shipcarr)
		  INTO v_shipcarr
		  FROM DUAL;

		SELECT DECODE (p_shipto, 0, NULL, p_shipto)
		  INTO v_shipto
		  FROM DUAL;   --- if  shipto is choose one, store as null.

		SELECT DECODE (p_shipmode, 0, NULL, p_shipmode)
		  INTO v_shipmode
		  FROM DUAL;

		IF p_source = 50180
		THEN   --order
			BEGIN
				SELECT c907_ship_to_id, c106_address_id
				  INTO v_parent_shiptoid, v_parentaddress
				  FROM t907_shipping_info
				 WHERE c907_ref_id = p_refid AND c907_void_fl IS NULL;
			EXCEPTION
				WHEN NO_DATA_FOUND
				THEN
					v_parent_shiptoid := 0;
					v_parentaddress := 0;
			END;
		ELSE
			v_parent_shiptoid := 0;
			v_parentaddress := 0;
		END IF;

		--Need to check if shipping record already existed.
		IF p_shipid IS NULL AND p_source = 50184
		THEN
			BEGIN
				SELECT c907_ship_to_id
				  INTO v_shipid
				  FROM t907_shipping_info
				 WHERE c907_ref_id = p_refid AND c901_source = p_source AND c907_void_fl IS NULL AND ROWNUM = 1;   --for loaner could be multiple.
			EXCEPTION
				WHEN NO_DATA_FOUND
				THEN
					v_shipid	:= p_shipid;
			END;
		ELSE
			v_shipid	:= p_shipid;
		END IF;

        gm_pkg_common.gm_fch_trans_comp_plant_id(p_source,p_refid,v_company_id,v_plant_id);        
        IF v_company_id IS NOT NULL AND v_plant_id IS NOT NULL THEN             
           gm_pkg_cor_client_context.gm_sav_client_context(v_company_id,v_plant_id);
        END IF;
                
		gm_pkg_cm_shipping_trans.gm_sav_ship_info (p_refid
												 , p_source
												 , v_shipto
												 , p_shiptoid
												 , v_shipcarr
												 , v_shipmode
												 , p_trackno
												 , p_addressid
												 , p_freightamt
												 , p_userid
												 , v_shipid
												 , p_attn
												 , p_ship_instruc
												 , p_pack_station
												 , p_tote
												  );
												  
		
		v_trackno	:= NULL;   --velu edited
		v_freightamt := 0;	 --velu edited

		IF p_source = 50184   -- if request then add a rec in t907 with CN id too
		THEN
			FOR currindex IN consign_cursor
			LOOP
				v_shipid	:= gm_pkg_cm_shipping_info.get_shipping_id (currindex.c504_consignment_id, 50181, 'fetch');
				gm_pkg_cm_shipping_trans.gm_sav_ship_info (currindex.c504_consignment_id
														 , 50181   -- consign
														 , v_shipto
														 , p_shiptoid
														 , v_shipcarr
														 , v_shipmode
														 , v_trackno
														 , p_addressid
														 , v_freightamt
														 , p_userid
														 , v_shipid
														 , p_attn
														 , p_ship_instruc
														 , p_pack_station
												 		 , p_tote
														  );
			END LOOP;
		END IF;

		--enter backorder details, velu edited
		IF p_source = 50180
		THEN   --order
			FOR curindex IN order_cursor
			LOOP
				v_childshipid := gm_pkg_cm_shipping_info.get_shipping_id (curindex.c501_order_id, 50180, 'fetch');

				BEGIN
					SELECT c907_ship_to_id, c106_address_id
						  ,c907_ship_tote_id, c907_pack_station_id
						  ,c907_tracking_number
					  INTO v_shiptoid, v_address
					      ,v_shiptoteid, v_stationid
					      ,v_track_no
					  FROM t907_shipping_info
					 WHERE c907_shipping_id = v_childshipid AND c907_void_fl IS NULL;
				EXCEPTION
					WHEN NO_DATA_FOUND
					THEN
						v_shiptoid	:= 0;
						v_address	:= 0;
						v_track_no := NULL;
						v_shiptoteid:= NULL;
						v_stationid:= NULL;
				END;
				
				--PC-414 - To skip updating shipping address for child orders having tissue parts
				v_tissue_count := NVL(gm_pkg_op_tissues.get_order_tissue_cnt(curindex.c501_order_id, v_shipto, p_addressid), 0);
				
				IF v_parent_shiptoid = v_shiptoid AND v_parentaddress = v_address AND v_tissue_count = 0
				THEN
					--v_track_no, v_stationid, v_shiptoteid is added becuase for same day when user processes parent order it was clearing the details for back order.
					gm_pkg_cm_shipping_trans.gm_sav_ship_info (curindex.c501_order_id
															 , p_source
															 , v_shipto
															 , p_shiptoid
															 , v_shipcarr
															 , v_shipmode
															 , v_track_no
															 , p_addressid
															 , '0'	 -- if backorder the freight amt should be 0
															 , p_userid
															 , v_childshipid
															 , p_attn
															 , p_ship_instruc
															 , v_stationid  -- Passing NULL for not updating the Child Station ID
												 			 , v_shiptoteid  -- Passing NULL for not updating the Child Tote ID
															  );
				END IF;
			END LOOP;
		END IF;

		IF p_source = 50184
		THEN   --request(set and item)
			FOR curindex IN request_cursor
			LOOP
				v_childshipid := gm_pkg_cm_shipping_info.get_shipping_id (curindex.c520_request_id, 50184, 'fetch');
				gm_pkg_cm_shipping_trans.gm_sav_ship_info (curindex.c520_request_id
														 , p_source
														 , v_shipto
														 , p_shiptoid
														 , v_shipcarr
														 , v_shipmode
														 , v_trackno
														 , p_addressid
														 , '0'	 -- if backorder the freight amt should be 0 for consign
														 , p_userid
														 , v_childshipid
														 , p_attn
														 , p_ship_instruc
														 , p_pack_station
												 		 , p_tote
														  );
		END LOOP;
	END IF;

		IF p_source = 50185   -- product loaner
		THEN   --request detail
			FOR curindex IN request_detail_cursor
			LOOP
				v_childshipid := 0;
				gm_pkg_cm_shipping_trans.gm_sav_ship_info (curindex.reqdetailid
														 , 50182
														 , v_shipto
														 , p_shiptoid
														 , v_shipcarr
														 , v_shipmode
														 , v_trackno
														 , p_addressid
														 , p_freightamt
														 , p_userid
														 , v_childshipid
														 , p_attn
														 , p_ship_instruc
														 , p_pack_station
												 		 , p_tote
														  );
			END LOOP;
		END IF;
		
		-- When do item initiate that time need to update planned ship data for all Consignment.
		IF p_source = 50184   -- request
		THEN   
			FOR curindex IN req_shipdt_cursor
			LOOP
				v_childshipid := 0;
				v_childshipid := gm_pkg_cm_shipping_info.get_shipping_id (curindex.c520_request_id, 50184, 'fetch');
				UPDATE t907_shipping_info 
				SET c907_planned_ship_date  = curindex.pl_ship_dt
					,c907_last_updated_date = CURRENT_DATE
					,c907_last_updated_by = p_userid
				WHERE C907_SHIPPING_ID = v_childshipid;
			END LOOP;
			FOR curindex IN cons_shipdt_cursor
			LOOP 
				v_childshipid := 0;
				v_childshipid := gm_pkg_cm_shipping_info.get_shipping_id (curindex.consid, 50181, 'fetch');
				UPDATE t907_shipping_info 
				SET c907_planned_ship_date  = curindex.pl_ship_dt
					,c907_last_updated_date = CURRENT_DATE
					,c907_last_updated_by = p_userid
				WHERE C907_SHIPPING_ID = v_childshipid;
			END LOOP;
		END IF;
		
		/*Update the child productrequest details address if the p_refid is master request*/
		IF p_source = 50181 THEN   -- Item Consignment
				UPDATE t907_shipping_info
					SET c106_address_id      = p_addressid ,
					  c901_ship_to           = v_shipto ,
					  c907_ship_to_id        = p_shiptoid ,
					  c901_delivery_carrier  = v_shipcarr,
					  c901_delivery_mode     = v_shipmode,
					  c907_override_attn_to  = p_attn,
					  c907_ship_instruction  = p_ship_instruc,
					  c907_last_updated_date = CURRENT_DATE ,
					  c907_last_updated_by   = p_userid
					WHERE c907_ref_id  IN
					  (SELECT TO_CHAR(t526.c526_product_request_detail_id)
					  FROM t525_product_request t525,
					    t526_product_request_detail t526
					  WHERE t525.c525_product_request_id = t526.c525_product_request_id
					  AND t525.c525_product_request_id   = p_refid
					  AND t525.c901_request_for          = 400088 --Item Consignment from IPAD request
					  AND t525.c525_void_fl             IS NULL
					  AND t526.c526_void_fl             IS NULL
					  AND t526.c526_status_fl            = 5 --pending approval
					  )
					AND c907_void_fl IS NULL;
		END IF;
	END gm_sav_shipping;

/******************************************************************
   * Description : Procedure to save the shipping info
   ****************************************************************/
	PROCEDURE gm_sav_ship_info (
		p_refid 	   IN		t907_shipping_info.c907_ref_id%TYPE
	  , p_source	   IN		t907_shipping_info.c901_source%TYPE
	  , p_shipto	   IN		t907_shipping_info.c901_ship_to%TYPE
	  , p_shiptoid	   IN		t907_shipping_info.c907_ship_to_id%TYPE
	  , p_shipcarr	   IN		t907_shipping_info.c901_delivery_carrier%TYPE
	  , p_shipmode	   IN		t907_shipping_info.c901_delivery_mode%TYPE
	  , p_trackno	   IN		t907_shipping_info.c907_tracking_number%TYPE
	  , p_addressid    IN		t907_shipping_info.c106_address_id%TYPE
	  , p_freightamt   IN		VARCHAR2
	  , p_userid	   IN		t907_shipping_info.c907_last_updated_by%TYPE
	  , p_shipid	   IN OUT	VARCHAR2
	  , p_attn		   IN		t907_shipping_info.c907_override_attn_to%TYPE DEFAULT NULL
  	  , p_ship_instruc IN		t907_shipping_info.c907_ship_instruction%TYPE DEFAULT NULL
  	  , p_pack_station IN		t907_shipping_info.c907_pack_station_id%TYPE  DEFAULT NULL
	  , p_tote		   IN		t907_shipping_info.c907_ship_tote_id%TYPE  DEFAULT NULL
	)
	AS
		-- v_shipid    VARCHAR2;
		v_prod_req_id  t526_product_request_detail.c525_product_request_id%TYPE;
		v_plant_id      t501_order.c5040_plant_id%TYPE;
		v_company_id    t1900_company.c1900_company_id%TYPE;
		v_req_company_id  t1900_company.c1900_company_id%TYPE;
	BEGIN
		--Getting plant id from context.
		SELECT  get_plantid_frm_cntx(), get_compid_frm_cntx()
		  INTO  v_plant_id, v_company_id
		  FROM DUAL;
	    -- The following code is added for getting the Company ID based on the Saved Loaner Request ID		
		SELECT DECODE(p_source
		              ,'50182' , get_transaction_company_id (v_company_id,p_refid,'LOANERSHIPFILTER','LOANERREQUEST') 
		              ,'50185' , get_transaction_company_id (v_company_id,p_refid,'LOANERSHIPFILTER','LOANERREQUEST')
		              ,'50183' , get_transaction_company_id (v_company_id,p_refid,'LOANERSHIPFILTER','INHOUSE')		             
		              , v_company_id) 
		  INTO v_req_company_id FROM DUAL;
	
		UPDATE t907_shipping_info
		   SET c901_ship_to = p_shipto
			 , c907_ship_to_id = p_shiptoid
			 , c901_delivery_mode = p_shipmode
			 , c901_delivery_carrier = p_shipcarr
			 , c907_tracking_number = p_trackno
			 , c106_address_id = p_addressid
			 , c907_frieght_amt = TO_NUMBER (p_freightamt)
			 , c907_last_updated_by = p_userid
			 , c907_last_updated_date = CURRENT_DATE
			 , c907_override_attn_to = p_attn
			 , c907_ship_instruction = p_ship_instruc
			 , c907_pack_station_id = p_pack_station
			 , c907_ship_tote_id = p_tote
			 , c5040_plant_id = v_plant_id
			 , c1900_company_id = v_req_company_id 
		 -- WHERE c907_ref_id = p_refid AND c901_source = p_source;
		WHERE  c907_shipping_id = p_shipid AND c907_void_fl IS NULL;

		IF (SQL%ROWCOUNT = 0)
		THEN
			SELECT s907_ship_id.NEXTVAL
			  INTO p_shipid
			  FROM DUAL;

			BEGIN
				SELECT	   c525_product_request_id
					  INTO v_prod_req_id
					  FROM t526_product_request_detail
					 WHERE TO_CHAR (c526_product_request_detail_id) = p_refid AND c526_void_fl IS NULL
				FOR UPDATE;
			EXCEPTION
				WHEN NO_DATA_FOUND
				THEN 
			 -- PMT-32450 - As the Ref ID is the GM-CN, we are checking if there is a Loaner request associated to it, 
			          --and updating the shipping table with the loaner req ID for the CN
				  BEGIN  
						SELECT	 t520.c525_product_request_id
						  INTO v_prod_req_id
						  FROM T520_REQUEST t520,T504_CONSIGNMENT t504
						 WHERE t520.C520_REQUEST_ID = t504.C520_REQUEST_ID
						 AND t504.C504_CONSIGNMENT_ID = p_refid 
						 AND t520.c520_void_fl IS NULL
						 AND t504.c504_void_fl IS NULL
						 FOR UPDATE;
						EXCEPTION 
						 WHEN OTHERS THEN 
							 v_prod_req_id := '';
					  END;	
			END;
 
			INSERT INTO t907_shipping_info
						(c907_shipping_id, c907_ref_id, c901_ship_to, c907_ship_to_id, c907_ship_to_dt, c901_source
					   , c901_delivery_mode, c901_delivery_carrier, c907_tracking_number, c106_address_id
					   , c907_status_fl, c907_frieght_amt, c907_created_by, c907_created_date
					   , c907_active_fl, c525_product_request_id, c907_override_attn_to, c907_ship_instruction
					   , c907_pack_station_id, c907_ship_tote_id, c5040_plant_id, c1900_company_id
						)
				 VALUES (TO_NUMBER (p_shipid), p_refid, p_shipto, p_shiptoid, TRUNC (CURRENT_DATE), p_source
					   , p_shipmode, p_shipcarr, p_trackno, p_addressid
					   , 15, TO_NUMBER (p_freightamt), p_userid, CURRENT_DATE
					   , DECODE (p_source, '50184', 'N', '50185', 'N', NULL), v_prod_req_id
					   , p_attn, p_ship_instruc, p_pack_station, p_tote, v_plant_id, v_req_company_id
						);

			gm_sav_shipping_status_log (p_refid, p_source, p_userid, 'Initiated');
		END IF;
	END gm_sav_ship_info;

	   /******************************************************************
	* Description : Procedure to release the transaction to shipping
	****************************************************************/
	PROCEDURE gm_sav_release_shipping (
		p_refid			IN	t907_shipping_info.c907_ref_id%TYPE
	  , p_source		IN	t907_shipping_info.c901_source%TYPE
	  , p_userid		IN	t907_shipping_info.c907_last_updated_by%TYPE
	  , p_logsrc   		IN	VARCHAR2
	  , p_link_ln_req	IN 	t907_shipping_info.c907_ref_id%TYPE DEFAULT NULL
	)
	AS
		v_status_fl		t907_shipping_info.c907_status_fl%TYPE;
		v_shipid		VARCHAR2 (100);
		v_pdt_req_id	t907_shipping_info.c525_product_request_id%TYPE;
	BEGIN
		v_shipid	:= gm_pkg_cm_shipping_info.get_shipping_id (p_refid, p_source, '');
		 
		IF p_link_ln_req IS NOT NULL
		THEN
			BEGIN
				SELECT c525_product_request_id
					INTO v_pdt_req_id
				FROM t526_product_request_detail
				WHERE c526_product_request_detail_id = p_link_ln_req;
				
				EXCEPTION WHEN NO_DATA_FOUND
				THEN
					v_pdt_req_id := NULL;
			END;
					
		END IF;
		
		SELECT	   c907_status_fl
			  INTO v_status_fl
			  FROM t907_shipping_info
			 -- WHERE c907_ref_id = p_refid
		WHERE	   c907_shipping_id = v_shipid AND c907_void_fl IS NULL
		FOR UPDATE;
 
		IF v_status_fl = 15   --inititated
		THEN
			UPDATE t907_shipping_info
			   SET c907_status_fl = 30
				 , c907_release_dt = TRUNC (CURRENT_DATE)
				 , c907_last_updated_by = p_userid
				 , c907_last_updated_date = CURRENT_DATE
				 , c525_product_request_id = NVL(v_pdt_req_id,c525_product_request_id)
			 -- WHERE c907_ref_id = p_refid AND c901_source = p_source;
			WHERE  c907_shipping_id = v_shipid AND c907_void_fl IS NULL;

			IF p_logsrc IS NULL
			THEN
				gm_sav_shipping_status_log (p_refid, p_source, p_userid, 'Controlled');
			END IF;
		END IF;
	END gm_sav_release_shipping;

	/******************************************************************
	* Description : Common Procedure to ShipOut
	****************************************************************/
	PROCEDURE gm_sav_shipout (
		p_refid 		 IN 	  t907_shipping_info.c907_ref_id%TYPE
	  , p_source		 IN 	  t907_shipping_info.c901_source%TYPE
	  , p_shipto		 IN 	  t907_shipping_info.c901_ship_to%TYPE
	  , p_shiptoid		 IN 	  t907_shipping_info.c907_ship_to_id%TYPE
	  , p_shipcarr		 IN 	  t907_shipping_info.c901_delivery_carrier%TYPE
	  , p_shipmode		 IN 	  t907_shipping_info.c901_delivery_mode%TYPE
	  , p_trackno		 IN 	  t907_shipping_info.c907_tracking_number%TYPE
	  , p_addressid 	 IN 	  t907_shipping_info.c106_address_id%TYPE
	  , p_freightamt	 IN 	  VARCHAR2
	  , p_userid		 IN 	  t907_shipping_info.c907_last_updated_by%TYPE
	  , p_updateaction	 IN 	  NUMBER
	  , p_shipid		 IN OUT   VARCHAR2
	  , p_custponum 	 IN 	  t503_invoice.c503_customer_po%TYPE
	  , p_shipflag		 OUT	  VARCHAR2
	  , p_attn		   	 IN	   	  t907_shipping_info.c907_override_attn_to%TYPE DEFAULT NULL
	  , p_ship_instruc   IN	   	  t907_shipping_info.c907_ship_instruction%TYPE DEFAULT NULL
	  , p_pack_station	 IN 	  t907_shipping_info.c907_pack_station_id%TYPE  DEFAULT NULL
	  , p_tote			 IN 	  t907_shipping_info.c907_ship_tote_id%TYPE  DEFAULT NULL 
	  , p_auto_ship_fl	 IN		  VARCHAR2 DEFAULT 'N'	 
	  
	  
	  
	)
	AS
		v_status	   VARCHAR2 (10);
		v_msg		   VARCHAR2 (100);
		v_poid		   t503_invoice.c503_customer_po%TYPE;
		v_recs		   NUMBER;
		v_link_recs    NUMBER;
		v_oldtrackno   t907_shipping_info.c907_tracking_number%TYPE;
		v_recordtyp    NUMBER;
		v_skipfl	   VARCHAR2 (5) := 'false';
		v_trackno	   t907_shipping_info.c907_tracking_number%TYPE;
		v_shipto	   t907_shipping_info.c901_ship_to%TYPE;
		v_shiptoid	   t907_shipping_info.c907_ship_to_id%TYPE;
		v_addressid    t907_shipping_info.c106_address_id%TYPE;
		v_shipcarr	   t504_consignment.c504_delivery_carrier%TYPE;
		v_shipmode	   t504_consignment.c504_delivery_mode%TYPE;
		v_message	   VARCHAR2 (200);
		v_type		   t902_log.c902_type%TYPE;
		v_count 	   NUMBER;
		v_uptfl        char(1) :='N';
		v_invoice_id	t501_order.c503_invoice_id%TYPE;
		v_ship_cost		t9072_shipping_cost.c9072_package_cost%TYPE;
		v_date_fmt     VARCHAR2(20);
		v_ship_to_old		t907_shipping_info.c901_ship_to%TYPE;
		v_ship_to_id_old	t907_shipping_info.c907_ship_to_id%TYPE;
		v_carrier_old		t907_shipping_info.c901_delivery_carrier%TYPE;
		v_mode_old			t907_shipping_info.c901_delivery_mode%TYPE;
		v_address_id_old	t907_shipping_info.c106_address_id%TYPE;
		v_edit_ship_cost_fl	t906_rules.c906_rule_value%TYPE;
		v_company_id    t1900_company.c1900_company_id%TYPE;
		v_request_id    t520_request.C520_REQUEST_ID%type;
		v_ordertype_fl 	VARCHAR2 (20);
		v_substring     VARCHAR2 (1000);
		v_str	   	    VARCHAR2 (30000);
		v_pnum		   VARCHAR2 (20);
		v_flag		   VARCHAR2 (2);
		v_instr			VARCHAR2 (30000);
		v_string_dummy   VARCHAR2 (1000);
		v_order_type     t501_order.C901_ORDER_TYPE%TYPE;
		v_txn_cnt		  NUMBER;
		v_dist_id      t701_distributor.c701_distributor_id%TYPE;
		
		

		--cursor to update the linked recs	when master rec is updated
		CURSOR cursor_linked_recs
		IS
			SELECT c907_shipping_id cshipid, c907_ref_id crefid, c901_source csource, c907_frieght_amt cfreight
				 , c901_ship_to cshipto, c907_ship_to_id cshiptoid, c901_delivery_carrier cshipcarr
				 , c901_delivery_mode cshipmode
				 , DECODE (c901_source, 50181, gm_pkg_ac_invoice.get_invoiceidfromconsigid (c907_ref_id), '') ccustpo
			  FROM t907_shipping_info
			 WHERE c907_tracking_number IN (v_oldtrackno) AND c907_void_fl IS NULL AND c907_shipping_id != p_shipid;
	BEGIN
		
		SELECT NVL(get_compdtfmt_frm_cntx(),'MM/DD/YYYY'), get_compid_frm_cntx() INTO v_date_fmt, v_company_id FROM DUAL;

		IF (p_shipid IS NULL OR p_shipid = 0)
		THEN
			p_shipid	:= gm_pkg_cm_shipping_info.get_shipping_id (p_refid, p_source, '');
		END IF;
		BEGIN
			-- get the shipping details [c901_ship_to, c907_ship_to_id, c901_delivery_carrier, c901_delivery_mode, c106_address_id] before changing the values
			SELECT	   c907_status_fl, c901_record_type, c907_tracking_number, c901_ship_to
					, c907_ship_to_id, c901_delivery_carrier, c901_delivery_mode, c106_address_id
				  INTO v_status, v_recordtyp, v_oldtrackno, v_ship_to_old
				  	, v_ship_to_id_old, v_carrier_old, v_mode_old, v_address_id_old
				  FROM t907_shipping_info
				 -- WHERE c907_ref_id = p_refid AND c901_source = p_source AND c907_void_fl IS NULL
			WHERE	   c907_shipping_id = p_shipid AND c907_void_fl IS NULL
			FOR UPDATE;
		EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			v_status := null;
			v_recordtyp := null;
			v_oldtrackno := null;
			v_ship_to_old := null;
			v_ship_to_id_old := null;
			v_carrier_old := null;
			v_mode_old := null;
			v_address_id_old := null;
		END;
			         
		IF (v_status < 30 AND p_trackno IS NOT NULL)
		THEN
			GM_RAISE_APPLICATION_ERROR('-20999','18',p_refid);
		END IF;
		
		IF (v_status = 40 AND p_trackno IS NULL)
		THEN
			GM_RAISE_APPLICATION_ERROR('-20999','142',p_refid);

		END IF;
		
		IF p_trackno IS NOT NULL
		THEN		
			v_uptfl := get_user_security_event_status(p_userid,'SHIPOUT_ACCESS');			
			IF v_uptfl <> 'Y' THEN
					GM_RAISE_APPLICATION_ERROR('-20999','20','');
			END IF;		
		END IF;
		
		
		
--velu created
		gm_pkg_cm_shipping_trans.gm_sav_shipping (p_refid
												, p_source
												, p_shipto
												, p_shiptoid
												, p_shipcarr
												, p_shipmode
												, p_trackno
												, p_addressid
												, p_freightamt
												, p_userid
												, p_shipid
												, p_attn
												, p_ship_instruc
												, p_pack_station
												, p_tote
												 );
												 

		-- below code marks the record as master/child
		-- Delivery mode condition is added to avoid the records which is having delivery mode as 'Hand Delivered'
		SELECT COUNT (1)
		  INTO v_recs
		  FROM t907_shipping_info
		 WHERE c907_tracking_number = p_trackno
		   AND c907_shipped_dt = TRUNC (CURRENT_DATE)
		   AND c907_void_fl IS NULL
		   AND (c907_shipping_id != p_shipid OR c901_record_type = 50970)
		   AND c1900_company_id = v_company_id
		   AND C901_DELIVERY_MODE NOT IN (SELECT c906_rule_value 
		   									FROM t906_rules 
		   								  WHERE c906_rule_grp_id='EXCLUDE_SHIP_MODE' 
		   								  	AND C906_rule_id='MODE' 
		   								  	AND C906_VOID_FL is null)  -- '5030': Hand delivered
		   ;

		-- FOR UPDATE;

		-- mark record as master
		IF v_recs = 1
		THEN
			UPDATE t907_shipping_info
			   SET c901_record_type = 50970   -- master
				 , c907_last_updated_by = p_userid
				 , c907_last_updated_date = CURRENT_DATE
			 WHERE c907_shipping_id != p_shipid
			   AND c907_tracking_number = p_trackno
			   AND c901_record_type IS NULL
			   AND c1900_company_id = v_company_id
			   AND c907_void_fl IS NULL;
		END IF;

-- copy from master to child(PMT-23031 Wrong Shipping details displaying in the Packing slip)
/*IF v_recs > 0 AND p_auto_ship_fl <> 'Y'
		THEN
			   IF p_trackno != v_oldtrackno
			   THEN
			 gm_procedure_log ('calling gm_sav_copyshipdetail ', '');
			gm_pkg_cm_shipping_trans.gm_sav_copyshipdetails (p_trackno, p_shipid, p_userid);
			v_skipfl	:= 'true';
		END IF;
		END IF;*/

-- link is broken so clear the master/child flag
		IF v_recs = 0 AND v_recordtyp IS NOT NULL
		THEN
			UPDATE t907_shipping_info
			   SET c901_record_type = NULL
				 , c907_last_updated_by = p_userid
				 , c907_last_updated_date = CURRENT_DATE
			 WHERE c907_shipping_id = p_shipid;

			SELECT COUNT (1)
			  INTO v_link_recs
			  FROM t907_shipping_info
			 WHERE c907_tracking_number = v_oldtrackno AND c907_shipped_dt = TRUNC (CURRENT_DATE) AND c907_void_fl IS NULL;

			IF (v_link_recs = 1)
			THEN
				UPDATE t907_shipping_info
				   SET c901_record_type = NULL
					 , c907_last_updated_by = p_userid
					 , c907_last_updated_date = CURRENT_DATE
				 WHERE c907_tracking_number = v_oldtrackno AND c907_shipped_dt = TRUNC (CURRENT_DATE)
					   AND c907_void_fl IS NULL;
			END IF;
		END IF;

		IF ((v_status >= 30 AND v_status < 40)  AND p_trackno IS NOT NULL)   -- shipout and call posting
		THEN
			p_shipflag	:= 'true';

			IF (p_source = 50183)	-- loaners extn
			THEN
				gm_pkg_cm_shipping_trans.gm_sav_loaner_extn_ship (p_refid, p_userid);
			ELSIF (p_source = 50182)   --loaners
			THEN
				gm_pkg_cm_shipping_trans.gm_sav_loaner_ship (p_refid, p_userid);
			ELSIF (p_source = 50180)   --orders
			THEN
				--The Below code is altered for PMT-6402, When shipping out an order updating ship to and ship to id in Order table.
				gm_pkg_cm_shipping_trans.gm_sav_order_ship (p_refid, p_freightamt, p_shipto,p_shiptoid, p_userid, v_msg);
			ELSIF (p_source = 50186) -- Inhouse Loaners
			THEN
				gm_pkg_cm_shipping_trans.gm_sav_loaner_item_ship (p_refid
																, TO_CHAR(CURRENT_DATE,v_date_fmt)
															    , p_shipcarr
															    , p_shipmode
															    , p_trackno
															    , p_shipto
															    , p_shiptoid
															    , p_userid
																);
			ELSIF (p_source = 50181 OR p_source = 26240435)   --50181:consign 26240435:Stock Trasfer
			THEN
				-- gm_part_tag_validation (51000, p_refid);
				v_poid		:= null;--p_custponum;

				gm_pkg_cm_shipping_trans.gm_save_consign_ship (p_refid
															 , TO_CHAR(CURRENT_DATE,v_date_fmt)
															 , p_shipcarr
															 , p_shipmode
															 , p_trackno
															 , p_userid
															 , p_freightamt
															 , v_poid
															 , p_shipto
															 , p_shiptoid
															  );
			ELSIF (p_source = 4000518)   --RETURN
			THEN
				gm_pkg_cm_shipping_trans.gm_sav_return_ship (p_refid, 
															CURRENT_DATE,
															p_shipcarr,
															p_shipmode,
															p_trackno,
															p_freightamt,
															p_shipto,
															p_shiptoid,
															p_userid);
		


				
			END IF;

			UPDATE t907_shipping_info
			   SET c907_status_fl = 40
				 , c907_shipped_dt = TRUNC (CURRENT_DATE)
				 , c907_last_updated_by = p_userid
				 , c907_last_updated_date = CURRENT_DATE
			 --WHERE c907_ref_id = p_refid;
			WHERE  c907_shipping_id = p_shipid AND c907_void_fl IS NULL;

			
			gm_sav_shipping_status_log (p_refid, p_source, p_userid, 'Shipped');
		END IF;

--below code will be called when editing a master record
--if updateaction selected is 50976(Modify Linked Records), then all the linked trans will be modified accordingly
--if updateaction selected is 50977(Clear Linked Records), then all the linked trans will be cleared of the track#
		IF p_updateaction != 0	 -- if master record is being updated
		THEN
			IF p_updateaction = 50977
			THEN
				UPDATE t907_shipping_info
				   SET c901_record_type = NULL
				 WHERE c907_shipping_id = p_shipid AND c907_void_fl IS NULL;
			END IF;

			FOR currindex IN cursor_linked_recs
			LOOP
				SELECT DECODE (p_updateaction, 50976, p_trackno, 'Modify Track#')
					 , DECODE (p_updateaction, 50976, p_shipto, c901_ship_to)
					 , DECODE (p_updateaction, 50976, p_shiptoid, c907_ship_to_id)
					 , DECODE (p_updateaction, 50976, p_addressid, c106_address_id)
					 , DECODE (p_updateaction, 50976, p_shipcarr, c901_delivery_carrier)
					 , DECODE (p_updateaction, 50976, p_shipmode, c901_delivery_mode)
				  INTO v_trackno
					 , v_shipto
					 , v_shiptoid
					 , v_addressid
					 , v_shipcarr
					 , v_shipmode
				  FROM t907_shipping_info
				 WHERE c907_shipping_id = currindex.cshipid AND c907_void_fl IS NULL;

				UPDATE t907_shipping_info
				   SET c907_tracking_number = v_trackno
					 , c901_ship_to = v_shipto
					 , c907_ship_to_id = v_shiptoid
					 , c106_address_id = v_addressid
					 , c901_delivery_carrier = v_shipcarr
					 , c901_delivery_mode = v_shipmode
					 , c901_record_type = DECODE (p_updateaction, 50976, 50971, NULL)
					 , c907_last_updated_by = p_userid
					 , c907_last_updated_date = CURRENT_DATE
				 WHERE c907_shipping_id = currindex.cshipid AND c907_void_fl IS NULL;

-- the details needs to be updated in parent tbl
				IF (currindex.csource = 50180)
				THEN   --orders
					gm_pkg_cm_shipping_trans.gm_sav_order_ship_cost (currindex.crefid, currindex.cfreight, p_userid);
				ELSIF (currindex.csource = 50181)
				THEN
					gm_pkg_cm_shipping_trans.gm_sav_consign_ship_details (currindex.crefid
																		, v_shipcarr
																		, v_shipmode
																		, v_trackno
																		, p_userid
																		, currindex.cfreight
																		, v_shipto
																		, v_shiptoid
																		, currindex.ccustpo
																		 );
				END IF;

				SELECT DECODE (currindex.csource, 50180, 1200, 50181, 1221, 50182, 1222, 50183, 1241)
				  INTO v_type
				  FROM DUAL;

				gm_update_log (currindex.crefid, 'updating details to #' || v_trackno, v_type, p_userid, v_message);
			END LOOP;
		END IF;

		-- if CN already shipped and modified later, the details needs to be updated in parent tbl
		IF (v_skipfl = 'false')
		THEN
			IF (p_source = 50180)
			THEN   --orders

					BEGIN	
						select c503_invoice_id
						  into v_invoice_id
						  from t501_order t501 
						 where t501.c501_order_id = p_refid;
					EXCEPTION WHEN NO_DATA_FOUND then
						  v_invoice_id := NULL;
					 END;
			
					SELECT NVL(get_rule_value_by_company('EDTCHRG', 'SHIPCHARGE',v_company_id),'N') 
						INTO v_edit_ship_cost_fl 
					FROM DUAL;
					
					-- should calculate the ship cost, if any of the shipping details are modified in the Modify Shipping Details screen
					-- And the invoice should not be generated
					-- if the value of v_edit_ship_cost_fl is 'Y', should not calculate the ship cost
					IF  v_invoice_id IS NULL  AND v_edit_ship_cost_fl != 'Y' AND (p_shipto != v_ship_to_old OR p_shiptoid != v_ship_to_id_old 
						OR p_shipcarr != v_carrier_old OR p_shipmode != v_mode_old OR p_addressid != v_address_id_old)
					THEN
						/* Fetch the Ship Cost for the account */
						gm_pkg_op_ship_charge.gm_fch_ship_cost_for_order (
								p_refid		
							  , 50180		
							  , p_shipto 		
							  , p_shiptoid	
							  , v_addressid	
							  , p_shipcarr	 	
							  , p_shipmode		
							  , NULL	
							  , v_ship_cost			
							);

						/* Update the ship Cost, so it reflects on the invoice */
							gm_pkg_op_do_process_trans.gm_update_ship_cost (p_refid, NVL(v_ship_cost,0), p_userid);
					END IF;
			ELSIF (p_source = 50181)
			THEN
				gm_pkg_cm_shipping_trans.gm_sav_consign_ship_details (p_refid
																	, p_shipcarr
																	, p_shipmode
																	, p_trackno
																	, p_userid
																	, p_freightamt
																	, p_shipto
																	, p_shiptoid
																	, p_custponum
																	 );
			END IF;
		END IF;
		
		/* check 50181 - consignment status  */
		IF (p_source = 50181) THEN
		   BEGIN
		     select C520_REQUEST_ID
						  into v_request_id
						  from t504_consignment
						 where C504_CONSIGNMENT_ID = p_refid;
			 EXCEPTION WHEN NO_DATA_FOUND THEN
			 v_request_id := '';
	       	END; 
			      gm_pkg_op_return_txn.gm_sav_return_from_req(v_request_id,p_userid);
			END IF;
			
	 /********************** Direct sales **********************/
		BEGIN
			SELECT C901_ORDER_TYPE INTO v_order_type
			 	FROM T501_ORDER 
			 		WHERE c501_order_id= p_refid 
			 		AND C501_VOID_FL IS NULL;
 			EXCEPTION WHEN NO_DATA_FOUND THEN
			 v_order_type := '';
		 END;
		v_ordertype_fl	:= get_rule_value_by_company (v_order_type,'DOTYPE',v_company_id);
			 
		--get the part string from calling the below method
		gm_pkg_op_do_process_report.gm_fch_DO_part_string (p_refid, v_str);
        --Replace the commas with cap (^)
        v_instr := REPLACE(v_str,',','^'); 
		IF (v_ordertype_fl = 'Y' AND v_instr IS NOT NULL) THEN   /* 26240232 - Direct Sales order type */ 
		
		  gm_pkg_cs_usage_lot_number.gm_sav_usage_ddt_dtls(p_refid,v_instr,'NEW_ORDER',p_userid);
		END IF;
	   
	   /* Update the status of Insert for the enabled transaction[Order,Consignment,FGLE] in T5056_TRANSACTION_INSERT Table
	    */
			BEGIN
			  SELECT COUNT(1)
			   INTO v_txn_cnt
			  FROM T5056_TRANSACTION_INSERT
			  WHERE C5056_REF_ID = p_refid
			  AND C5056_VOID_FL IS NULL;
			EXCEPTION
			WHEN NO_DATA_FOUND THEN
			  v_txn_cnt := 0;
			END;
			
			IF v_txn_cnt > 0 THEN
			  gm_pkg_op_sav_insert_txn.gm_upd_insert_ship_status(p_refid,'93345',p_userid); --93345:= Put Transacted
			END IF;
		
	END gm_sav_shipout;
	
	/***************************************************************************************
    * Description	: FUNCTION to fetch the user security event Status 
    * 				 
    * Author		: APrasath
    ****************************************************************************************/
	FUNCTION  get_user_security_event_status(
      	p_userid   IN	t905_status_details.c905_updated_by%TYPE,
      	p_function_id  IN  t1520_function_list.C1520_FUNCTION_ID%TYPE
    )
    RETURN VARCHAR2
     IS
        v_uptfl        char(1) :='N';
		v_readfl       char(1);
		v_voidfl       char(1);
		v_id		   NUMBER;
		v_return_cur   TYPES.cursor_type;
     BEGIN	     	
	       gm_pkg_cm_accesscontrol.gm_fch_accesspermissions(p_userid,p_function_id,v_return_cur);
			
			LOOP
				FETCH v_return_cur INTO v_id,v_uptfl,v_readfl,v_voidfl;
						EXIT
						WHEN v_return_cur%NOTFOUND;
			END LOOP;			
        RETURN v_uptfl;
    END get_user_security_event_status;

	/******************************************************************
	   * Description : Procedure for saving shipping log
	   ****************************************************************/
	PROCEDURE gm_sav_shipping_status_log (
		p_refid    IN	t905_status_details.c905_ref_id%TYPE
	  , p_source   IN	t907_shipping_info.c901_source%TYPE
	  , p_userid   IN	t905_status_details.c905_updated_by%TYPE
	  , p_action   IN	VARCHAR2
	)
	AS
		v_status	   t901_code_lookup.c901_code_id%TYPE;
		v_cnt		   NUMBER;
		v_req_dt	   DATE;
		v_con_dt	   DATE;
		v_rule_dt	   DATE;
	BEGIN
		IF (p_source = '50183')   -- loaner extn
		THEN
			SELECT DECODE (p_action, 'Initiated', '91110', 'Controlled', '91111', 'Shipped', '91113')
			  INTO v_status
			  FROM DUAL;

			gm_save_status_details (p_refid, '', p_userid, NULL, CURRENT_DATE, v_status, 91104);
		ELSIF (p_source = '50182')	 --loaners
		THEN
			SELECT DECODE (p_action, 'Initiated', '91120', 'Controlled', '', 'Shipped', '91121')
			  INTO v_status
			  FROM DUAL;

			IF (v_status IS NOT NULL)
			THEN
				gm_save_status_details (p_refid, '', p_userid, NULL, CURRENT_DATE, v_status, 91103);
			END IF;
		ELSIF (p_source = '50180')	 --orders
		THEN
			SELECT DECODE (p_action, 'Initiated', '91170', 'Controlled', '91171', 'Shipped', '91173')
			  INTO v_status
			  FROM DUAL;

			gm_save_status_details (p_refid, '', p_userid, NULL, CURRENT_DATE, v_status, 91102);
		ELSIF (p_source = '50184' OR p_source = '50181')   --Request/consign
		THEN
			SELECT DECODE (p_action
						 , 'Initiated', '91160'
						 , 'Controlled', '91161'
						 , 'Verified', '91162'
						 , 'Shipped', '91165'
						  )
			  INTO v_status
			  FROM DUAL;

			IF (v_status = 91160)
			THEN
				SELECT COUNT (1)
				  INTO v_cnt
				  FROM t905_status_details t905
				 WHERE t905.c901_source = 91101 AND t905.c905_ref_id = p_refid AND t905.c901_type = 91160;

				BEGIN
					SELECT t520.c520_created_date
						 , t504.c504_created_date
					  INTO v_req_dt
						 , v_con_dt
					  FROM t520_request t520, t504_consignment t504
					 WHERE t520.c520_request_id = t504.c520_request_id(+)
					   --AND t520.C901_REQUEST_SOURCE = 50616
					   AND (t520.c520_request_id = p_refid OR t504.c504_consignment_id = p_refid);
				EXCEPTION
					WHEN NO_DATA_FOUND
					THEN
						v_req_dt	:= '';
						v_con_dt	:= '';
				END;

				v_rule_dt	:= get_rule_value ('STATUSLOG', 'LOG');

				IF (v_cnt = 0 AND (v_rule_dt < v_req_dt OR v_rule_dt < v_con_dt))
				THEN
					gm_save_status_details (p_refid, '', p_userid, NULL, CURRENT_DATE, v_status, 91101);
				END IF;
			ELSE
				gm_save_status_details (p_refid, '', p_userid, NULL, CURRENT_DATE, v_status, 91101);
			END IF;
		END IF;

		RETURN;
	END gm_sav_shipping_status_log;

	/******************************************************************
	   * Description : Procedure for loaner Extension ShipOut
	   ****************************************************************/
	PROCEDURE gm_sav_loaner_extn_ship (
		p_refid    IN	t907_shipping_info.c907_ref_id%TYPE
	  , p_userid   IN	t907_shipping_info.c907_last_updated_by%TYPE
	)
	AS
	v_txn_cnt NUMBER;
	v_msg     VARCHAR2 (100);
	BEGIN
		
		gm_pkg_op_inv_field_sales_tran.gm_sav_loaner_lot(p_refid,p_userid);
		--Call the below new prc to update the main inventory qty for the FGLE.
			BEGIN
			  SELECT COUNT(1)
			   INTO v_txn_cnt
			  FROM T5056_TRANSACTION_INSERT
			  WHERE C5056_REF_ID = p_refid
			  AND C5056_VOID_FL IS NULL;
			EXCEPTION
			WHEN NO_DATA_FOUND THEN
			  v_txn_cnt := 0;
			END;
			--50154 :=Move from Shelf to Loaner Extension w/Replenish
			IF v_txn_cnt> 0 THEN 
				gm_update_insert_inventory (p_refid, '50154', 'IHL-MINUS', p_userid, v_msg);
			END IF;
		
		RETURN;
	END gm_sav_loaner_extn_ship;

	/******************************************************************
	 * Description : Procedure for loaner	 ShipOut
	 ****************************************************************/
	PROCEDURE gm_sav_loaner_ship (
		p_refid    IN	t907_shipping_info.c907_ref_id%TYPE
	  , p_userid   IN	t907_shipping_info.c907_last_updated_by%TYPE
	)
	AS
		v_req_detail_id t525_product_request.c525_product_request_id%TYPE;
		v_dist_id	   VARCHAR2 (20);
		v_acc_id	   t504_consignment.c704_account_id%TYPE;
		v_loc_accnt_id t504_consignment.c704_location_account_id%TYPE;
		v_loc_rep_id   t504_consignment.c703_location_sales_rep_id%TYPE;
		v_count 	   NUMBER;
		v_cn_type	   t504_consignment.c504_type%TYPE;
		v_consigned_to	t504a_loaner_transaction.c901_consigned_to%TYPE := 4120;
	BEGIN
		-- Call the common procedure to update the status
		gm_pkg_op_loaner_set_txn.gm_upd_consign_loaner_status(p_refid,20,p_userid);
		
		UPDATE t504a_consignment_loaner
		   SET c504a_available_date = NULL
			 , c504a_last_updated_date = CURRENT_DATE
			 , c504a_last_updated_by = p_userid
		 WHERE c504_consignment_id = p_refid 
		   AND c504a_void_fl IS NULL;

		SELECT NVL (c526_product_request_detail_id, 0)
		  INTO v_req_detail_id
		  FROM t504a_loaner_transaction
		 WHERE c504a_loaner_transaction_id = gm_pkg_op_loaner.get_loaner_trans_id (p_refid) 
		   AND c504a_void_fl IS NULL;

		IF v_req_detail_id != 0
		THEN
			gm_pkg_op_loaner_allocation.gm_loaner_allocation_update(v_req_detail_id, 'CLEAR', p_userid); -- reset allocated fl and planned ship date in t504a
			-- 30: closed
			gm_pkg_op_loaner_allocation.gm_upd_request_status(v_req_detail_id, 30, p_userid);

			gm_pkg_op_loaner.gm_sav_update_req_status (v_req_detail_id, p_userid);
		END IF;

		-- updating account id on t5010_tag  -- loaners
		SELECT COUNT (1)
		  INTO v_count
		  FROM t5010_tag
		 WHERE c5010_last_updated_trans_id = p_refid 
		   AND c5010_void_fl IS NULL;

		IF v_count > 0
		THEN
			BEGIN
				-- Getting values to be used later in the procedure
				SELECT NVL(c701_distributor_id,c504_ship_to_id) , NVL (c704_account_id, c704_location_account_id), c704_location_account_id
					 , c703_location_sales_rep_id, c504_type, c504_ship_to
				  INTO v_dist_id, v_acc_id, v_loc_accnt_id
					 , v_loc_rep_id, v_cn_type, v_consigned_to
				  FROM t504_consignment
				 WHERE c504_consignment_id = p_refid AND c504_void_fl IS NULL;
		    EXCEPTION
			WHEN NO_DATA_FOUND THEN
		   	 v_dist_id := NULL;
		   	 v_acc_id := NULL;
		   	 v_loc_accnt_id := NULL;
		   	 v_loc_rep_id := NULL;
		   	 v_cn_type := NULL;
		   	 v_consigned_to := NULL;
			END;
			   
			IF (v_dist_id IS NOT NULL AND  v_consigned_to IS NOT NULL)	 
			THEN
				gm_update_location (v_dist_id, v_consigned_to, p_refid, 51002, p_userid, v_acc_id, v_loc_rep_id);
			END IF;
		END IF;
	END gm_sav_loaner_ship;

	/******************************************************************
		* Description : Procedure for order  ShipOut
		* Procedure is altered for PMT-6402 when shipping out an Order we are updating ship to and ship to id.
	****************************************************************/
		PROCEDURE gm_sav_order_ship (
		p_refid 	 IN 	  t907_shipping_info.c907_ref_id%TYPE
	  , p_shipcost	 IN 	  t501_order.c501_ship_cost%TYPE
	  , p_userid	 IN 	  t907_shipping_info.c907_last_updated_by%TYPE
	  , p_message	 OUT	  VARCHAR2
	)
	AS
	BEGIN
		gm_sav_order_ship(p_refid,p_shipcost,'','',p_userid,p_message);
	END gm_sav_order_ship;

	
	/******************************************************************
	 * Description : Procedure for order  ShipOut
	 ****************************************************************/
	PROCEDURE gm_sav_order_ship (
		p_refid 	 IN 	  t907_shipping_info.c907_ref_id%TYPE
	  , p_shipcost	 IN 	  t501_order.c501_ship_cost%TYPE
	  , p_shipto	 IN 	  t501_order.c501_ship_to%TYPE
	  , p_shiptoid   IN 	  t501_order.C501_SHIP_TO_ID%TYPE
	  , p_userid	 IN 	  t907_shipping_info.c907_last_updated_by%TYPE
	  , p_message	 OUT	  VARCHAR2
	)
	AS
		v_po		   t501_order.c501_customer_po%TYPE;
		v_flag		   VARCHAR2 (1);
		p_msg		   VARCHAR2 (100);
		p_invfl 	   VARCHAR2 (10);
		v_pick_loc	   VARCHAR2 (10);
		v_dist_id 	   t701_distributor.c701_distributor_id%TYPE;
		v_order_type   t501_order.c901_order_type%TYPE;
		v_acc_id       t704_account.c704_account_id%TYPE;
		v_location_id  t704_account.c5052_location_id%TYPE;
		v_txn_cnt	   NUMBER;
		v_partnum		varchar2(4000);
		v_part_numbers_id clob;
		v_dist_name      t701_distributor.c701_distributor_name%TYPE; --PMT-42833
		v_order_congsignment_count	NUMBER; --PC-4815
		v_quote_id		t501_order.c5506_robot_quotation_id%TYPE;
	BEGIN
		SELECT t501.c501_customer_po, get_distributor_id (t501.c703_sales_rep_id), NVL(t501.c901_order_type,-999),
		       t704.c704_account_id, t704.c5052_location_id 
		  INTO v_po, v_dist_id, v_order_type, v_acc_id, v_location_id 
		  FROM t501_order t501, t704_account t704
		 WHERE t501.c501_order_id = p_refid AND t501.c501_void_fl IS NULL
		       AND t704.c704_account_id = t501.c704_account_id AND t704.c704_void_fl IS NULL;

		--this will not used any more since it's checked by RFS rule
		-- gm_pkg_pd_partnumber.gm_val_release_for_sale (p_refid, 50180);

		--Check for the validation before order shipping

		--
		--Need to update status to 3, even if customer PO is null for both US and OUS
		v_flag		:= '3';

		UPDATE t501_order
		   SET c501_ship_cost = p_shipcost
			 , c501_status_fl = v_flag
			 , c501_ship_to = p_shipto
			 , c501_ship_to_id = p_shiptoid
			 , c501_last_updated_by = p_userid
			 , c501_last_updated_date = CURRENT_DATE
			 , c501_shipping_date = CURRENT_DATE
		 WHERE c501_order_id = p_refid AND c501_void_fl IS NULL;

	 BEGIN
		SELECT c501_update_inv_fl,c5506_robot_quotation_id
		  INTO p_invfl,v_quote_id
		  FROM t501_order
		 WHERE c501_order_id = p_refid AND c501_void_fl IS NULL;
     EXCEPTION WHEN NO_DATA_FOUND THEN
     	p_invfl := null;
     	v_quote_id := null;
     END;
		
		SELECT count(1)
		  INTO v_order_congsignment_count
		  FROM T5000_ORDER_CONSIGNMENT_MAPPING
		 WHERE c501_order_id = p_refid
		   AND c5000_void_fl is null;
		--
		IF p_invfl IS NULL AND v_order_congsignment_count = 0
		THEN
			--update for pick_loc  (minus)
			v_pick_loc	:= get_rule_value ('PICK_LOC', '50261');

			IF v_pick_loc = 'YES'
			THEN
				gm_pkg_op_item_control_txn.gm_sav_location_qty (p_refid, 50261, 93343, p_userid); 
				gm_pkg_op_inv_scan.gm_sav_inv_status(p_refid,'93006',null,p_userid); --PMT:24927 -93006(Completed)--saves and updates the inventory status for shipped orders
			END IF;

			UPDATE t501_order
			   SET c501_update_inv_fl = '1'
			     , c501_last_updated_by = p_userid
			     , c501_last_updated_date = CURRENT_DATE
			 WHERE c501_order_id = p_refid AND c501_void_fl IS NULL;


			--
			gm_update_inventory (p_refid, 'S', '', p_userid, p_msg);
			--Call the below new prc to update the main inventory qty for the Inserts.
			BEGIN
			  SELECT COUNT(1)
			   INTO v_txn_cnt
			  FROM T5056_TRANSACTION_INSERT
			  WHERE C5056_REF_ID = p_refid
			  AND C5056_VOID_FL IS NULL;
			EXCEPTION
			WHEN NO_DATA_FOUND THEN
			  v_txn_cnt := 0;
			END;
			IF v_txn_cnt > 0 THEN 
				gm_update_insert_inventory(p_refid, 'S', '', p_userid, p_msg);
			END IF;
			
			-- To validate the ack released quantity and close the ab order
			gm_pkg_op_ack_order.gm_chk_and_close_ack_order(p_refid,p_userid); 
		END IF;	
		
		IF v_order_congsignment_count > 0 THEN
			gm_upd_order_consignment_inventory(p_refid,v_order_type,p_userid);
		END IF;
		
		--to update quote status as shipped when all related order shipped.
		IF v_quote_id IS NOT NULL THEN
			gm_pkg_sm_robotics_order_txn.gm_check_quote_orders_status(v_quote_id,p_userid);
		END IF;
		
		--To validate the Parts ICT Pricing
		IF v_order_type = 102080 THEN
		 gm_pkg_op_ict.gm_op_validate_dist_price(p_refid, v_dist_id, v_partnum,v_dist_name);	--PMT-42833 email changes
			
		IF v_partnum is not null THEN
			GM_RAISE_APPLICATION_ERROR('-20999','417', v_partnum);
		END IF;
		END IF;
		
	END gm_sav_order_ship;

	/******************************************************************
	  * Description : Procedure for to save Freight Amt for order
	  ****************************************************************/
	PROCEDURE gm_sav_order_ship_cost (
		p_refid 	 IN   t907_shipping_info.c907_ref_id%TYPE
	  , p_shipcost	 IN   t501_order.c501_ship_cost%TYPE
	  , p_userid	 IN   t907_shipping_info.c907_last_updated_by%TYPE
	)
	AS
	BEGIN
		UPDATE t501_order
		   SET c501_ship_cost = p_shipcost
			 , c501_last_updated_by = p_userid
			 , c501_last_updated_date = CURRENT_DATE
		 WHERE c501_order_id = p_refid AND c501_void_fl IS NULL;
	END;

	/******************************************************************
	  * Description : Procedure for to save shipping details in consign/request tbls
	  ****************************************************************/
	PROCEDURE gm_sav_consign_ship_details (
		p_consgid	   IN	t504_consignment.c504_consignment_id%TYPE
	  , p_shipcar	   IN	t504_consignment.c504_delivery_carrier%TYPE
	  , p_shipmode	   IN	t504_consignment.c504_delivery_mode%TYPE
	  , p_track 	   IN	t504_consignment.c504_tracking_number%TYPE
	  , p_userid	   IN	t504_consignment.c504_last_updated_by%TYPE
	  , p_freightamt   IN	t504_consignment.c504_ship_cost%TYPE
	  , p_shipto	   IN	t504_consignment.c504_ship_to%TYPE
	  , p_shiptoid	   IN	t504_consignment.c504_ship_to_id%TYPE
	  , p_custponum    IN	t503_invoice.c503_customer_po%TYPE
	)
	AS
		v_req_id	   t520_request.c520_request_id%TYPE;
		v_conv_freight t504_consignment.c504_ship_cost%TYPE;
		v_interpartyid NUMBER;
		v_dist_invoiceid t503_invoice.c503_invoice_id%TYPE;
		v_dist_orderid t501_order.c501_order_id%TYPE;
		v_dist_po t503_invoice.c503_customer_po%TYPE;
		v_dist_type t701_distributor.c901_distributor_type%TYPE;
	BEGIN
		SELECT get_currency_conversion (get_rule_value (50219, 'USD')
									  , get_rule_value (50219
													  , get_rule_value (50218
																	  , get_distributor_inter_party_id (p_shiptoid)
																	   )
													   )
									  , CURRENT_DATE
									  , p_freightamt
									   )
		  INTO v_conv_freight
		  FROM DUAL;

		UPDATE t504_consignment
		   SET c504_ship_to = p_shipto
			 , c504_ship_to_id = p_shiptoid
			 , c504_delivery_mode = p_shipmode
			 , c504_delivery_carrier = p_shipcar
			 , c504_tracking_number = p_track
			 , c504_ship_cost = DECODE (v_conv_freight, NULL, p_freightamt, v_conv_freight)
			 , c504_last_updated_date = CURRENT_DATE
			 , c504_last_updated_by = p_userid
		 WHERE c504_consignment_id = p_consgid AND c504_void_fl IS NULL;

		SELECT c520_request_id
		  INTO v_req_id
		  FROM t504_consignment
		 WHERE c504_consignment_id = p_consgid AND c504_void_fl IS NULL;

		UPDATE t520_request
		   SET c901_ship_to = p_shipto
			 , c520_ship_to_id = p_shiptoid
			 , c520_last_updated_by = p_userid
			 , c520_last_updated_date = CURRENT_DATE
		 WHERE c520_request_id = v_req_id AND c520_void_fl IS NULL;

		UPDATE t520_request
		   SET c901_ship_to = p_shipto
			 , c520_ship_to_id = p_shiptoid
			 , c520_last_updated_by = p_userid
			 , c520_last_updated_date = CURRENT_DATE
		 WHERE c520_master_request_id = v_req_id AND c520_void_fl IS NULL;

-- for ict consignments only need to update the custpo num
		SELECT DECODE (c901_ship_to, 4120, NVL (get_distributor_inter_party_id (c907_ship_to_id), 0), 0) interpartyid
			 , gm_pkg_ac_invoice.get_invoiceidfromconsigid (p_consgid)
			 , get_distributor_type (c907_ship_to_id)
		  INTO v_interpartyid
			 , v_dist_invoiceid
			 , v_dist_type
		  FROM t907_shipping_info
		 WHERE c907_ref_id = p_consgid AND c901_source = 50181 AND c907_void_fl IS NULL;

		IF (v_interpartyid != 0)
		THEN
		v_dist_po := p_custponum;
		
			IF v_dist_type = 70105 -- ICS
			 THEN
				 -- get the OUS Distributor order id and update the PO number.
				  BEGIN
				    SELECT c501_order_id,NVL(v_dist_po,c503a_customer_po)
				     INTO v_dist_orderid, v_dist_po			     
	   			FROM t503a_invoice_txn
	  				WHERE c503_invoice_id = v_dist_invoiceid;
	  			EXCEPTION
				WHEN NO_DATA_FOUND
				THEN
					v_dist_orderid := NULL;
					v_dist_po := NULL;
				END;
				
				 UPDATE t501_order
					SET c501_customer_po  = v_dist_po
					  , c501_last_updated_by = p_userid
					  , c501_last_updated_date = CURRENT_DATE
	  			WHERE c501_order_id = v_dist_orderid
	    		AND c501_void_fl IS NULL;
  			END IF;	-- ICS (70105)
			UPDATE t503_invoice
			   SET c503_customer_po = v_dist_po
				 , c503_last_updated_date = CURRENT_DATE
				 , c503_last_updated_by = p_userid
			 WHERE c503_invoice_id = v_dist_invoiceid AND c503_void_fl IS NULL;

			UPDATE t503a_invoice_txn
			   SET c503a_customer_po = v_dist_po
				 , c503a_last_updated_by = p_userid
				 , c503a_last_updated_date = CURRENT_DATE
			 WHERE c503_invoice_id = v_dist_invoiceid;
		END IF;
	END gm_sav_consign_ship_details;

	/******************************************************************
	   * Description : Procedure for consignment shipout - calls posting
	   ****************************************************************/
	PROCEDURE gm_save_consign_ship (
		p_consgid	   IN		t504_consignment.c504_consignment_id%TYPE
	  , p_shipdt	   IN		VARCHAR2
	  , p_shipcar	   IN		t504_consignment.c504_delivery_carrier%TYPE
	  , p_shipmode	   IN		t504_consignment.c504_delivery_mode%TYPE
	  , p_track 	   IN		t504_consignment.c504_tracking_number%TYPE
	  , p_userid	   IN		t504_consignment.c504_last_updated_by%TYPE
	  , p_freightamt   IN		t501_order.c501_ship_cost%TYPE
	  , p_poid		   IN OUT	t503_invoice.c503_customer_po%TYPE
	  , p_shipto	   IN		t504_consignment.c504_ship_to%TYPE
	  , p_shiptoid	   IN		t504_consignment.c504_ship_to_id%TYPE
	)
	AS
--
		v_flag		   VARCHAR2 (2);
		v_msg		   VARCHAR2 (100);
		v_set_id	   VARCHAR2 (20);
		v_dist_id	   VARCHAR2 (20);
		v_dist_type    VARCHAR2 (20);
		v_inter_party_id t701_distributor.c101_party_intra_map_id%TYPE;
		v_post_id	   VARCHAR2 (20);
		v_party_id	   VARCHAR2 (20);
		v_purpose	   VARCHAR2 (20);
		v_status_fl    VARCHAR2 (5);
		v_void_fl	   VARCHAR2 (10);
		v_type		   t504_consignment.c504_type%TYPE;
		v_rqid		   t504_consignment.c520_request_id%TYPE;
		v_acc_id	   t504_consignment.c704_account_id%TYPE;
		-- v_count_pur NUMBER;
		v_order_id	   t501_order.c501_order_id%TYPE;
		v_count 	   NUMBER;
		v_loc_accnt_id t504_consignment.c704_location_account_id%TYPE;
		v_loc_rep_id   t504_consignment.c703_location_sales_rep_id%TYPE;
		v_pick_loc	   VARCHAR2 (10);
		v_new_type     t504_consignment.c504_type%TYPE;
		v_req_for      t520_request.c520_request_for%TYPE;
		v_date_fmt     VARCHAR2(20);
		-- Internation posting changes
        v_ict_post_id  VARCHAR2 (20);
        v_inter_comp_transfer_fl CHAR(1);
        v_new_status  VARCHAR2(2);
		v_set_type 	  t207_set_master.c207_type%TYPE;
		v_destination_comp_id t1900_company.c1900_company_id%TYPE;
		v_rma_id t506_returns.c506_rma_id%TYPE;
		v_ous_cn_post_id t906_rules.c906_rule_value%TYPE;
		v_international_post_id t906_rules.c906_rule_value%TYPE;
		v_skip_fs_ws_flag T906_RULES.C906_RULE_VALUE%TYPE;
		v_txn_cnt		  NUMBER;
--
		CURSOR cur_consign
		IS
			SELECT	 c205_part_number_id ID, SUM (c505_item_qty) qty, c505_item_price price
				FROM t505_item_consignment
			   WHERE c504_consignment_id = p_consgid AND TRIM (c505_control_number) IS NOT NULL
			GROUP BY c205_part_number_id, c505_item_price;
--
	BEGIN
--
-- Getting values to be used later in the procedure
		SELECT	   c504_void_fl, c504_type, c207_set_id, c701_distributor_id, c504_inhouse_purpose
				 , c504_update_inv_fl, NVL (c504_status_fl, 0), c520_request_id
				 , get_distributor_inter_party_id (c701_distributor_id)
				 , NVL (c704_account_id, c704_location_account_id), c704_location_account_id
				 , c703_location_sales_rep_id, get_distributor_type (c701_distributor_id)
			  INTO v_void_fl, v_type, v_set_id, v_dist_id, v_purpose
				 , v_flag, v_status_fl, v_rqid
				 , v_inter_party_id
				 , v_acc_id, v_loc_accnt_id
				 , v_loc_rep_id, v_dist_type
			  FROM t504_consignment
			 WHERE c504_consignment_id = p_consgid AND c504_void_fl IS NULL
		FOR UPDATE;
		
		 -- to get the Request for (Req Type)
        BEGIN
		  SELECT c520_request_for
		  INTO v_req_for
		  FROM t520_request
		  WHERE c520_request_id = v_rqid
		  AND c520_void_fl IS NULL;
		EXCEPTION
		WHEN NO_DATA_FOUND THEN
		  v_req_for := NULL;
		END;
		
		IF v_void_fl IS NOT NULL
		THEN
			-- Error message is Consignment already Voided.
			raise_application_error (-20770, '');
		END IF;

		--this will not used any more since it's checked by RFS rule
		 /* IF v_inter_party_id IS NULL AND v_dist_id IS NOT NULL
		  THEN
			gm_pkg_pd_partnumber.gm_val_release_for_sale (p_consgid, 4110);
		  END IF;*/
		IF v_type = 4129
		THEN
			UPDATE t504_consignment
			   SET c504_status_fl = 4
				 , c504_last_updated_by = p_userid
				 , c504_last_updated_date = CURRENT_DATE
			 WHERE c504_consignment_id = p_consgid AND c504_void_fl IS NULL;

			--
			RETURN;
		--
		END IF;

		SELECT COUNT (1)
		  INTO v_count
		  FROM t505_item_consignment t505
		 WHERE get_part_attribute_value (t505.c205_part_number_id, 92340) = 'Y' AND t505.c504_consignment_id = p_consgid;

		IF v_type = 4110 AND v_count > 0   --Consignment
		THEN
			gm_update_location (v_dist_id, 4120, p_consgid, 51000, p_userid, v_acc_id, v_loc_rep_id);
		ELSIF v_type = 4112 AND v_count > 0
		THEN   --inhouse consignment
			gm_update_location (p_shiptoid,   -- employee
								4123,	--52097,--employee
								p_consgid, 51000, p_userid, v_acc_id, NULL);
	    ELSIF v_type = 26240144	 --Return to Owner Company
	    THEN  
	        gm_pkg_op_return_txn.gm_op_sav_ra_ict_return(p_consgid,p_userid,v_rma_id);  --Initiate RA(Return to Owner Company)					
		END IF;

		-- Starting validation for status flag. If status flag is not 3, then throw error.
		-- This might happen if someone rollsback a consignment and click back button and perform an update operation
		IF v_status_fl NOT IN (3,3.30,3.60)
		THEN
			-- Error message is Set Consignment is not in Shipped Status and cannot be rolled back.
			raise_application_error (-20771, '');
		END IF;

		IF (v_type = 4110 AND v_dist_id IS NULL AND v_req_for <> '40025')
		THEN
			--Error message b'coz null dist id doesnot post into accounting
			raise_application_error (-20772, '');
		END IF;
		
		-- to assign the consignment type, based on the the party id.
        SELECT DECODE (v_inter_party_id, NULL, c504_type, 1938, c504_type, DECODE (c504_type, 4110, 40057, c504_type)),
               DECODE(get_rule_value(t504.c504_type,'DEMO_CN_TYPE'),'Y',t207.c207_type,NULL)
          INTO v_new_type, v_set_type
   	      FROM t504_consignment t504, t207_set_master t207
		 WHERE c504_consignment_id = p_consgid 
		   AND c504_void_fl IS NULL
		   AND t504.c207_set_id = t207.c207_set_id(+)
		   AND c207_void_fl(+) IS NULL;	
   	  
      -- to assign the consignment type, based on the Request For.
        SELECT DECODE(v_req_for,102930,40057,v_new_type),
        	DECODE(v_req_for,102930,5,4)
        INTO v_new_type,
        	v_new_status
         FROM DUAL;
     	
		-- updating transaction with Shipping info
		-- if request For is (102930 - Inter Company Transfer) then set the CN status as 5 otherwise 4.
		select NVL(get_compdtfmt_frm_cntx(),'MM/DD/YYYY') into v_date_fmt from dual;
		UPDATE t504_consignment
		   SET c504_ship_to = p_shipto
			 , c504_ship_to_id = p_shiptoid
			 , c504_delivery_carrier = p_shipcar
			 , c504_delivery_mode = p_shipmode
			 , c504_tracking_number = p_track
			 , c504_ship_date = TO_DATE (p_shipdt, v_date_fmt)
			 , c504_status_fl = v_new_status
			 , c504_ref_id = v_rma_id
			 , c901_ref_type = NVL2(v_rma_id,'26240144',c901_ref_type) 
			 , c504_last_updated_by = p_userid
			 , c504_last_updated_date = CURRENT_DATE
			 , c504_type = v_new_type
		 WHERE c504_consignment_id = p_consgid AND c504_void_fl IS NULL;

	  -- if request For is (102930 - Inter Company Transfer) then set the Request status as 60 otherwise 40.
		UPDATE t520_request
		   SET c520_status_fl = DECODE(v_req_for,102930,'60',26240144,'45','40')
			 , c901_ship_to = p_shipto
			 , c520_ship_to_id = p_shiptoid
			 , c520_required_date = CASE
									   WHEN c520_required_date > TRUNC (CURRENT_DATE) AND v_set_id is not null --not applicable for item consignment
										   THEN TRUNC (CURRENT_DATE)
									   ELSE c520_required_date
								   END
			 , c520_last_updated_by = p_userid
			 , c520_last_updated_date = CURRENT_DATE
		 WHERE c520_request_id = v_rqid AND c520_void_fl IS NULL;
	
		 UPDATE t520_request t520
		   SET c520_required_date = CASE
									   WHEN c520_required_date > TRUNC (CURRENT_DATE) AND v_set_id is not null --not applicable for item consignment(PBUG-2292)
										   THEN TRUNC (CURRENT_DATE)
									   ELSE c520_required_date
								   END
			 , c520_last_updated_by = p_userid
			 , c520_last_updated_date = CURRENT_DATE
		 WHERE t520.c520_master_request_id = v_rqid AND t520.c520_status_fl < 40 AND c520_void_fl IS NULL;
         
        BEGIN
		     SELECT DECODE (c1900_parent_company_id, NULL, NULL, 'Y')
		     , c1900_parent_company_id
		       INTO v_inter_comp_transfer_fl
		       , v_destination_comp_id
		       FROM t701_distributor
		      WHERE c701_distributor_id = v_dist_id;
		EXCEPTION
		WHEN NO_DATA_FOUND THEN
		    v_inter_comp_transfer_fl := NULL;
		    v_ict_post_id := NULL;
		END;
		
		SELECT NVL(get_rule_value(v_dist_type,'SKIP_FS_WAREHOUSE'), 'N') INTO v_skip_fs_ws_flag FROM DUAL;
    	-- This is called for Item consignments to update inventory
		IF v_set_id IS NULL AND v_flag IS NULL
		THEN
			-- update the FS warehouse Qty
			-- 102900 - Consignment
			-- 102907 - Consignment
			IF v_new_type <> 4112 AND v_new_status = 4 AND v_inter_comp_transfer_fl IS NULL AND v_skip_fs_ws_flag <> 'Y' -- In-House Consignment AND To exclude ICT transfer qty from filed sales inv
			THEN
				IF v_req_for = '40025' THEN
				gm_pkg_op_inv_field_sales_tran.gm_update_fs_inv(p_consgid, 'T505', v_acc_id, 102900, 4301, 102907, p_userid ,v_req_for);
				ELSE
					gm_pkg_op_inv_field_sales_tran.gm_update_fs_inv(p_consgid, 'T505', v_dist_id, 102900, 4301, 102907, p_userid);
				END IF;
			END IF;
			--
			gm_update_inventory (p_consgid, 'CITEM', 'MINUS', p_userid, v_msg);

			--Call the below new prc to update the main inventory qty for the Inserts-Consignments.
			BEGIN
			  SELECT COUNT(1)
			   INTO v_txn_cnt
			  FROM T5056_TRANSACTION_INSERT
			  WHERE C5056_REF_ID = p_consgid
			  AND C5056_VOID_FL IS NULL;
			EXCEPTION
			WHEN NO_DATA_FOUND THEN
			  v_txn_cnt := 0;
			END;
			IF v_txn_cnt> 0 THEN 
				gm_update_insert_inventory (p_consgid, 'CITEM', 'MINUS', p_userid, v_msg);
			END IF;
			--
			UPDATE t504_consignment
			   SET c504_update_inv_fl = '1'
			     , c504_last_updated_by = p_userid
				 , c504_last_updated_date = CURRENT_DATE
			 WHERE c504_consignment_id = p_consgid AND c504_void_fl IS NULL;

			--
			--update for pick_loc  (minus)
			v_pick_loc	:= get_rule_value ('PICK_LOC', v_type);

			IF v_pick_loc = 'YES'
			THEN
				gm_pkg_op_item_control_txn.gm_sav_location_qty (p_consgid, v_type, 93343, p_userid);
			END IF;
			
		END IF;

		SELECT t504.c504_type
		  INTO v_type
		  FROM t504_consignment t504
		 WHERE c504_consignment_id = p_consgid 
		   AND c504_void_fl IS NULL;

		-- For posting Transfers - Sets
		IF v_set_id IS NOT NULL AND v_status_fl <> '4'
		THEN
			-- For Agent Transfers
			IF v_dist_id IS NOT NULL
			THEN
				v_party_id	:= v_dist_id;
				--Demo Consignment
				SELECT NVL(get_rule_value (v_set_type, 'BSDMO'),4814) INTO v_post_id FROM DUAL;   --26240102; -- Demo Consignment
			-- For In-House Transfers
			ELSE
				v_post_id	:= get_rule_value (v_purpose, 'CON-SET-INHOUSE');
				v_party_id	:= 01;
			END IF;

			IF v_type = '40057'
			THEN
				IF v_dist_type = 70105	 -- ICS
				THEN
					v_post_id	:= get_rule_value (50250, 'BSICS');
				ELSE
					-- to get the OUS CN and international posting id
					 SELECT NVL (get_rule_value (v_set_type, 'BSICTDMO'), get_rule_value ('50252', 'BSICT')), NVL (get_rule_value (
					    v_set_type, 'BSICDMO'), get_rule_value ('50252_ICT', 'BSICT'))
					   INTO v_ous_cn_post_id, v_international_post_id
					   FROM dual;
					--
					 SELECT DECODE (v_inter_comp_transfer_fl, NULL, v_ous_cn_post_id, v_international_post_id)
					   INTO v_ict_post_id
					   FROM dual;
         			-- to set the correct posting id 
					v_post_id	:= v_ict_post_id;
				END IF;
			ELSIF v_type = 26240144	 --Return to Owner Company
			THEN
			
				SELECT NVL(get_rule_value (v_set_type, 'RETURN-TO-OWNER'),get_rule_value ('BUILTSET', 'RETURN-TO-OWNER')) INTO v_post_id FROM DUAL;
				
			END IF;
			-- update the FS warehouse Qty
			-- 4301 Plus
			-- 102900 - Consignment
			-- 102907 - Consignment
			IF v_type <> 4112 AND v_new_status = 4 AND v_inter_comp_transfer_fl IS NULL AND v_skip_fs_ws_flag <>'Y' -- In-House Consignment AND To exclude ICT transfer qty from filed sales inv
			THEN
				gm_pkg_op_inv_field_sales_tran.gm_update_fs_inv(p_consgid, 'T505', v_dist_id, 102900, 4301, 102907, p_userid);
			END IF;
			
		    IF get_rule_value (v_destination_comp_id,'SHOW-IN-TRANSIT') = 'Y' -- Check the intrans and demo set (PMT-21114)
		    		THEN
		    	    SELECT get_master_set_type(c207_set_id)
				  	  INTO v_set_type
				      FROM t504_consignment
				     WHERE c504_consignment_id = p_consgid;
				    IF v_set_type = 4078 AND v_type = 40057 -- Demo set (4078) and Intransit (40057) 
				    THEN		
				    		v_post_id := '26240401'; -- Demo set Intrasit posting
				    END IF;
		    END IF;
			
			FOR rad_val IN cur_consign
			LOOP
				--
				gm_save_ledger_posting (v_post_id
									  , CURRENT_DATE
									  , rad_val.ID
									  , v_party_id
									  , p_consgid
									  , rad_val.qty
									  , rad_val.price
									  , p_userid
									  , v_destination_comp_id
									   );
			--
			END LOOP;
			
			--This Procedure is used to insert the entry in T5060 table for the control number shipping out from Set Consignment
			IF v_type = '4110' THEN --Consignment
				gm_pkg_op_lot_track_txn.gm_pkg_op_lot_track_for_sets(p_consgid, p_userid);
			END IF;
		
		END IF;

		-- chk if the dist type is ICT
		IF v_type = 40057 
		THEN
			--gm_update inventory to perform
			gm_pkg_op_ict.gm_op_sav_initiateict (p_consgid, 50250, p_poid, p_freightamt, p_userid, v_dist_id
											   , v_purpose);
			gm_pkg_in_transit_trans.gm_save_intransit_trans(p_consgid,p_userid);									   
		END IF;
		-- for FG-ST(106703) and BL-ST(106704)
		IF v_type = 106703 OR  v_type = 106704
		THEN
			--Generate Stock transfer invoice and populate price from costing layer
			gm_pkg_op_request_fulfill.gm_initiate_ST_Invoice(p_consgid,p_userid);
			gm_pkg_in_transit_trans.gm_save_intransit_trans(p_consgid,p_userid);	-- Call this procedure to save the Intrasit transactions.	
		END IF;

----------------------------------------------------------------------------------------------------
--following code for update item price to 0 if consignment type is ICT and purpose is 'ICT Sample'
   /* SELECT COUNT (1)
	   INTO v_count_pur
	   FROM t504_consignment
	  WHERE c504_consignment_id = p_consgid
		AND c504_inhouse_purpose = 50061
		AND c504_type = 40057
		AND c504_void_fl IS NULL;*/
	END gm_save_consign_ship;

	/******************************************************************
	   Description : This procedure is used to swap the shipping details when requests
	   is swapped
	   ****************************************************************/
	PROCEDURE gm_sav_swap_ship_details (
		p_reqfrom	IN	 t520_request.c520_request_id%TYPE
	  , p_reqto 	IN	 t520_request.c520_request_id%TYPE
	  , p_userid	IN	 t520_request.c520_last_updated_by%TYPE
	)
	AS
		v_shipto_from  t907_shipping_info.c901_ship_to%TYPE;
		v_shiptoid_from t907_shipping_info.c907_ship_to_id%TYPE;
		v_shiptodate_from t907_shipping_info.c907_ship_to_dt%TYPE;
		v_delmode_from t907_shipping_info.c901_delivery_mode%TYPE;
		v_delcarr_from t907_shipping_info.c901_delivery_carrier%TYPE;
		v_amt_from	   t907_shipping_info.c907_frieght_amt%TYPE;
		v_address_id_from t907_shipping_info.c106_address_id%TYPE;
		v_shipto_to    t907_shipping_info.c901_ship_to%TYPE;
		v_shiptoid_to  t907_shipping_info.c907_ship_to_id%TYPE;
		v_shiptodate_to t907_shipping_info.c907_ship_to_dt%TYPE;
		v_delmode_to   t907_shipping_info.c901_delivery_mode%TYPE;
		v_delcarr_to   t907_shipping_info.c901_delivery_carrier%TYPE;
		v_amt_to	   t907_shipping_info.c907_frieght_amt%TYPE;
		v_address_id_to t907_shipping_info.c106_address_id%TYPE;
		v_childreqfrom t520_request.c520_request_id%TYPE;
		v_childreqto   t520_request.c520_request_id%TYPE;
		v_consignfrom  t504_consignment.c504_consignment_id%TYPE;
		v_consignto    t504_consignment.c504_consignment_id%TYPE;
	BEGIN
		BEGIN
			SELECT t907.c901_ship_to, t907.c907_ship_to_id, c106_address_id, t907.c907_ship_to_dt
				 , t907.c901_delivery_mode, t907.c901_delivery_carrier, t907.c907_frieght_amt
			  INTO v_shipto_from, v_shiptoid_from, v_address_id_from, v_shiptodate_from
				 , v_delmode_from, v_delcarr_from, v_amt_from
			  FROM t907_shipping_info t907
			 WHERE t907.c907_ref_id = p_reqfrom AND c907_void_fl IS NULL;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				v_shipto_from := '';
				v_shiptoid_from := '';
				v_address_id_from := '';
				v_shiptodate_from := '';
				v_delmode_from := '';
				v_delcarr_from := '';
				v_amt_from	:= '';
		END;

		BEGIN
			SELECT t907.c901_ship_to, t907.c907_ship_to_id, c106_address_id, t907.c907_ship_to_dt
				 , t907.c901_delivery_mode, t907.c901_delivery_carrier, t907.c907_frieght_amt
			  INTO v_shipto_to, v_shiptoid_to, v_address_id_to, v_shiptodate_to
				 , v_delmode_to, v_delcarr_to, v_amt_to
			  FROM t907_shipping_info t907
			 WHERE t907.c907_ref_id = p_reqto AND c907_void_fl IS NULL;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				v_shipto_to := '';
				v_shiptoid_to := '';
				v_address_id_to := '';
				v_shiptodate_to := '';
				v_delmode_to := '';
				v_delcarr_to := '';
				v_amt_to	:= '';
		END;

		BEGIN
			SELECT c520_request_id
			  INTO v_childreqfrom
			  FROM t520_request
			 WHERE c520_master_request_id = p_reqfrom AND c520_void_fl IS NULL;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				v_childreqfrom := '';
		END;

		BEGIN
			SELECT c520_request_id
			  INTO v_childreqto
			  FROM t520_request
			 WHERE c520_master_request_id = p_reqto AND c520_void_fl IS NULL;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				v_childreqto := '';
		END;

		BEGIN
			SELECT c504_consignment_id
			  INTO v_consignfrom
			  FROM t504_consignment
			 WHERE c520_request_id = p_reqfrom AND c504_void_fl IS NULL;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				v_consignfrom := '';
		END;

		BEGIN
			SELECT c504_consignment_id
			  INTO v_consignto
			  FROM t504_consignment
			 WHERE c520_request_id = p_reqto AND c504_void_fl IS NULL;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				v_consignto := '';
		END;

		UPDATE t907_shipping_info
		   SET c901_ship_to = v_shipto_to
			 , c907_ship_to_id = v_shiptoid_to
			 , c106_address_id = v_address_id_to
			 , c907_ship_to_dt = v_shiptodate_to
			 , c901_delivery_mode = v_delmode_to
			 , c901_delivery_carrier = v_delcarr_to
			 , c907_frieght_amt = v_amt_to
			 , c907_last_updated_by = p_userid
			 , c907_last_updated_date = CURRENT_DATE
		 WHERE c907_ref_id IN (p_reqfrom, v_childreqfrom, v_consignfrom) AND c907_void_fl IS NULL;

		UPDATE t907_shipping_info
		   SET c901_ship_to = v_shipto_from
			 , c907_ship_to_id = v_shiptoid_from
			 , c106_address_id = v_address_id_from
			 , c907_ship_to_dt = v_shiptodate_from
			 , c901_delivery_mode = v_delmode_from
			 , c901_delivery_carrier = v_delcarr_from
			 , c907_frieght_amt = v_amt_from
			 , c907_last_updated_by = p_userid
			 , c907_last_updated_date = CURRENT_DATE
		 WHERE c907_ref_id IN (p_reqto, v_childreqto, v_consignto) AND c907_void_fl IS NULL;
	END gm_sav_swap_ship_details;

	/******************************************************************
	   Description : This procedure is used to update the flag for which the mail has been sent.
	   and insert the records into status table
	   ****************************************************************/
	PROCEDURE gm_sav_shipped_today (
		p_refid   IN   VARCHAR2
	)
	AS
	BEGIN
		my_context.set_double_inlist_ctx (p_refid, ',');

		UPDATE t907_shipping_info
		   SET c907_email_update_fl = 'Y'
			 , c907_email_update_dt = TRUNC (CURRENT_DATE)
		 WHERE c907_shipping_id IN (SELECT c907_shipping_id
									  FROM t907_shipping_info t907, v_double_in_list v_list
									 WHERE t907.c907_ref_id = v_list.token AND t907.c901_source = v_list.tokenii)
		   AND c907_email_update_fl IS NULL
		   AND c907_active_fl IS NULL
		   AND c907_status_fl = 40;
	END gm_sav_shipped_today;

	/******************************************************************
	  Description : This procedure is used to copy the details from the tracking# record to shipid record
	  ****************************************************************/
	PROCEDURE gm_sav_copyshipdetails (
		p_trackno	IN	 t907_shipping_info.c907_tracking_number%TYPE
	  , p_shipid	IN	 t907_shipping_info.c907_shipping_id%TYPE
	  , p_userid	IN	 t907_shipping_info.c907_last_updated_by%TYPE
	)
	AS
		v_shipto	   t907_shipping_info.c901_ship_to%TYPE;
		v_shipto_by_shipid t907_shipping_info.c901_ship_to%TYPE;
		v_shiptoid	   t907_shipping_info.c907_ship_to_id%TYPE;
		v_toshiptoid   t907_shipping_info.c907_ship_to_id%TYPE;
		v_addressid_by_trk_no    t907_shipping_info.c106_address_id%TYPE;
		v_address_by_shipid    t907_shipping_info.c106_address_id%TYPE;
		v_shipcarr	   t504_consignment.c504_delivery_carrier%TYPE;
		v_shipmode	   t504_consignment.c504_delivery_mode%TYPE;
		v_refid 	   t907_shipping_info.c907_ref_id%TYPE;
		v_freight	   t907_shipping_info.c907_frieght_amt%TYPE;
		v_source	   t907_shipping_info.c901_source%TYPE;
		v_type		   t902_log.c902_type%TYPE;
		v_message	   VARCHAR2 (200);
		v_custpo	   t503_invoice.c503_customer_po%TYPE;
		v_company_id    t1900_company.c1900_company_id%TYPE;

	BEGIN
		SELECT get_compid_frm_cntx() INTO v_company_id FROM DUAL;

		BEGIN
			SELECT c901_ship_to, c907_ship_to_id, NVL(c106_address_id,0), c901_delivery_mode, c901_delivery_carrier
			  INTO v_shipto, v_shiptoid, v_addressid_by_trk_no, v_shipmode, v_shipcarr
			  FROM t907_shipping_info
			 WHERE c907_tracking_number = p_trackno
			   AND c907_void_fl IS NULL
			   AND (c901_record_type IS NULL OR c901_record_type = 50970)	--master
			   AND c907_shipping_id != p_shipid
			   AND c1900_company_id = v_company_id
			   -- adding this line for testing
			   AND c907_shipped_dt = TRUNC (CURRENT_DATE);
--if no record found then return
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				RETURN;
		END;

		BEGIN
			--no need to update the frightamt and custpo. so fetching the same values and passing them agin as the prc takes them as param
			SELECT c907_ship_to_id, c907_ref_id, c901_source, c907_frieght_amt
				 , DECODE (c901_source
						 , 50181, gm_pkg_ac_invoice.get_cust_po_num
																   (gm_pkg_ac_invoice.get_invoiceidfromconsigid (c907_ref_id)
																   )
						 , ''
						  ), c106_address_id, c901_ship_to
			  INTO v_toshiptoid, v_refid, v_source, v_freight
				 , v_custpo, v_address_by_shipid, v_shipto_by_shipid
			  FROM t907_shipping_info
			 WHERE c907_shipping_id = p_shipid AND c907_void_fl IS NULL
		 --Even Ship_to_Id is different, with same track number more than one transaction can be shipped if Address are same. So check for address	
		 --If Ship to are different do not raise as error
		  AND (NVL(c106_address_id,0) = v_addressid_by_trk_no OR c901_ship_to<> v_shipto);      
     
		EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			raise_application_error (-20773, '');
		END;
     
/*
		--Even Ship_to_Id is different, with same track number more than one transaction can be shipped if Address are same. So check for address		
		--IF v_toshiptoid != v_shiptoid
		IF v_addressid_by_trk_no != v_address_by_shipid
		THEN
			IF (v_shipto = v_shipto_by_shipid) THEN -- If Ship to are different do not raise as error
				raise_application_error (-20168, '');
			END IF;
		END IF;
*/

		UPDATE t907_shipping_info
		   SET c901_ship_to = v_shipto
			 , c907_ship_to_id = v_shiptoid
			 , c106_address_id = v_addressid_by_trk_no
			 , c901_delivery_carrier = v_shipcarr
			 , c901_delivery_mode = v_shipmode
			 , c907_tracking_number = p_trackno
			 , c901_record_type = 50971   -- child
			 , c907_last_updated_by = p_userid
			 , c907_last_updated_date = CURRENT_DATE
		 WHERE c907_shipping_id = p_shipid 
		   AND c907_void_fl IS NULL ;

		-- the details needs to be updated in parent tbl as well
		IF (v_source = 50180)
		THEN   --orders
			gm_pkg_cm_shipping_trans.gm_sav_order_ship_cost (v_refid, v_freight, p_userid);
		ELSIF (v_source = 50181)
		THEN
			gm_pkg_cm_shipping_trans.gm_sav_consign_ship_details (v_refid
																, v_shipcarr
																, v_shipmode
																, p_trackno
																, p_userid
																, v_freight
																, v_shipto
																, v_shiptoid
																, v_custpo
																 );
		END IF;

		SELECT DECODE (v_source, 50180, 1200, 50181, 1221, 50182, 1222, 50183, 1241)
		  INTO v_type
		  FROM DUAL;

		gm_update_log (v_refid, 'copying details from tracking #' || p_trackno, v_type, p_userid, v_message);
	END gm_sav_copyshipdetails;

/***************************************************************************************
 * Description		: This procedure will update the tag table with the location id and type
 * Author			: Brinal
 *
 ****************************************************************************************/
	PROCEDURE gm_update_location (
		p_locationid	 IN   t5010_tag.c5010_location_id%TYPE
	  , p_locationtype	 IN   t5010_tag.c901_location_type%TYPE
	  , p_lastupdtrans	 IN   t5010_tag.c5010_last_updated_trans_id%TYPE
	  , p_transtype 	 IN   t5010_tag.c901_trans_type%TYPE
	  , p_userid		 IN   t5010_tag.c5010_created_by%TYPE
	  , p_acc_id		 IN   t504_consignment.c704_account_id%TYPE
	  , p_sales_rep_id	 IN   t504_consignment.c703_location_sales_rep_id%TYPE
	)
	AS
		v_sub_location_type NUMBER;
		v_sub_location_id VARCHAR2 (40);
		v_add_id	       t907_shipping_info.c106_address_id%TYPE;
		v_rqid             t504_consignment.c520_request_id%TYPE;
        v_req_for          t520_request.c520_request_for%TYPE;
        v_dist_type        t701_distributor.c901_distributor_type%TYPE;
        v_new_tag_status   t5010_tag.C901_STATUS%TYPE;
        
	BEGIN
		BEGIN
			SELECT DECODE (t907.c901_ship_to, 4120, 50221, 4121, 50222, 4122, 11301) sub_loc_type
				 , t907.c907_ship_to_id sub_loc_id, t907.c106_address_id add_id
			  INTO v_sub_location_type
				 , v_sub_location_id, v_add_id
			  FROM t907_shipping_info t907
			 WHERE t907.c907_void_fl IS NULL AND t907.c907_ref_id = p_lastupdtrans;
		EXCEPTION
			WHEN OTHERS
			THEN
				v_sub_location_type := NULL;
				v_sub_location_id := NULL;
				v_add_id	:= NULL;
		END;
		
		IF p_locationtype = 4120 AND p_transtype = 51000 THEN
      	-- to get the request id based on the CN
      	BEGIN
	      SELECT c520_request_id
	        INTO v_rqid
	        FROM t504_consignment
	       WHERE c504_consignment_id = p_lastupdtrans AND c504_void_fl IS NULL;
	    EXCEPTION
	    WHEN NO_DATA_FOUND THEN
	        v_rqid := NULL;
	    END;
	    IF v_rqid IS NOT NULL
	    THEN
		    SELECT c520_request_for
		      INTO v_req_for
		      FROM t520_request WHERE c520_request_id = v_rqid AND c520_void_fl IS NULL;
		END IF; -- v_rqid IS null      
      END IF; -- p_locationid and p_transtype check
      
            
      SELECT DECODE(v_req_for,102930,102401,NULL) INTO v_new_tag_status FROM DUAL;
      
      BEGIN 
       SELECT T701.C901_DISTRIBUTOR_TYPE
        INTO  v_dist_type
   		 FROM T701_DISTRIBUTOR t701, t504_consignment t504
	    WHERE T504.C701_DISTRIBUTOR_ID = T701.C701_DISTRIBUTOR_ID
	    AND T504.C504_CONSIGNMENT_ID = p_lastupdtrans
	    AND T504.C701_DISTRIBUTOR_ID = T701.C701_DISTRIBUTOR_ID
	    AND T504.C504_VOID_FL       IS NULL
	    AND t701.c701_void_fl       IS NULL;
	    
	    IF (v_dist_type='70105') AND p_transtype = 51000  THEN -- set p_transtype = 51000 for  PMT-31652 Tags updating as sold for inhouse loaner demo shipped to ICS distributors
      --If the Dist type is ICS (70105) assign the value to the below variable. This variable is used in the  below update query in the tag table (Decode condition)
	    	v_new_tag_status:='26240343';
	    END IF;
   EXCEPTION
	    WHEN NO_DATA_FOUND THEN
	        v_new_tag_status := NULL;
	    END;

      
		UPDATE t5010_tag
		   SET c5010_location_id = p_locationid
			 , c901_location_type = p_locationtype
			 , c5010_last_updated_by = p_userid
			 , c5010_last_updated_date = CURRENT_DATE
			 , c5010_lock_fl = 'Y'
			 , c901_sub_location_type = v_sub_location_type
			 , c5010_sub_location_id = DECODE(v_sub_location_id,0, NULL,v_sub_location_id)
			 , c106_address_id = DECODE (v_add_id, 0, get_address_id (p_sales_rep_id), v_add_id)
			 , c704_account_id = p_acc_id
			 , c703_sales_rep_id = DECODE(p_sales_rep_id,0,NULL,p_sales_rep_id)
			 , c901_status = DECODE(v_new_tag_status,NULL,c901_status,v_new_tag_status)
             , c901_trans_type = DECODE(v_req_for,102930,102402,c901_trans_type)
		 WHERE c5010_last_updated_trans_id = p_lastupdtrans AND c5010_void_fl IS NULL;

		IF (SQL%ROWCOUNT = 0)
		THEN
			raise_application_error ('-20774', '');
		END IF;
	END gm_update_location;

/***************************************************************************************
 * Description		: Procedure to change the transaction status to Shipping to Initiated
 * Author			: Gopinathan
 *
 ****************************************************************************************/
	PROCEDURE gm_sav_remove_shipping (
		p_refid    IN	t907_shipping_info.c907_ref_id%TYPE
	  , p_source   IN	t907_shipping_info.c901_source%TYPE
	  , p_userid   IN	t907_shipping_info.c907_last_updated_by%TYPE
	  , p_logsrc   IN	VARCHAR2
	)
	AS
		v_status_fl    t907_shipping_info.c907_status_fl%TYPE;
		v_shipid	   VARCHAR2 (100);
	BEGIN
		v_shipid	:= gm_pkg_cm_shipping_info.get_shipping_id (p_refid, p_source, '');

		SELECT	   c907_status_fl
			  INTO v_status_fl
			  FROM t907_shipping_info
			 WHERE c907_shipping_id = v_shipid AND c907_void_fl IS NULL
		FOR UPDATE;

		IF v_status_fl = 30   -- Shipping
		THEN
			UPDATE t907_shipping_info
			   SET c907_status_fl = 15
				 ,	 --inititated
				   c907_release_dt = TRUNC (CURRENT_DATE)
				 , c907_last_updated_by = p_userid
				 , c907_last_updated_date = CURRENT_DATE
			 WHERE c907_shipping_id = v_shipid AND c907_void_fl IS NULL;

			IF p_logsrc IS NULL
			THEN
				gm_sav_shipping_status_log (p_refid, p_source, p_userid, 'Initiated');
			END IF;
		END IF;
	END gm_sav_remove_shipping;

/******************************************************************
 * Description : Procedure for loaner item ShipOut
 * Author	   : Gopinathan
 ****************************************************************************************/
	PROCEDURE gm_sav_loaner_item_ship(
		p_refid    	   IN		t907_shipping_info.c907_ref_id%TYPE
	  , p_shipdt	   IN		VARCHAR2
	  , p_shipcar	   IN		t504_consignment.c504_delivery_carrier%TYPE
	  , p_shipmode	   IN		t504_consignment.c504_delivery_mode%TYPE
	  , p_track 	   IN		t504_consignment.c504_tracking_number%TYPE
	  , p_shipto	   IN		t504_consignment.c504_ship_to%TYPE
	  , p_shiptoid	   IN		t504_consignment.c504_ship_to_id%TYPE
	  , p_userid   	   IN	    t907_shipping_info.c907_last_updated_by%TYPE
	)
	AS
		v_req_id        VARCHAR2(50);
		v_date_fmt      VARCHAR2(50);
   BEGIN
	   
	   	/*verify the IHLN before shipout.*/
	    gm_pkg_op_ihloaner_item_txn.gm_sav_verify_ihln(p_refid,9110,p_userid);
	   
	 	BEGIN		   
		 SELECT t907.c525_product_request_id INTO v_req_id 
		   FROM t907_shipping_info t907,t504_consignment t504
		  WHERE t907.c907_ref_id = p_refid
		    AND t907.c907_ref_id = t504.c504_consignment_id
            AND t907.c525_product_request_id = t504.c504_ref_id
            AND t504.c504_type = 9110 -- ihln
            AND t504.c504_void_fl IS NULL
		    AND t907.c901_source   = 50186 --source
		    AND t907.c907_void_fl IS NULL;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				v_req_id := NULL;
		END; 
	    IF  v_req_id IS NOT NULL THEN  
	    	-- update the item request status to closed based on shipped out qty.
			gm_pkg_op_ihloaner_item_txn.gm_sav_update_item_req_status(v_req_id,p_refid,9110,p_userid);
		END IF;
		
		--this update for paper work info. 
		SELECT NVL(get_compdtfmt_frm_cntx(),'MM/DD/YYYY') INTO v_date_fmt FROM dual;
		UPDATE t504_consignment
		   SET c504_ship_to = p_shipto
			 , c504_ship_to_id = p_shiptoid
			 , c504_delivery_carrier = p_shipcar
			 , c504_delivery_mode = p_shipmode
			 , c504_tracking_number = p_track
			 , c504_ship_date = TO_DATE (p_shipdt, v_date_fmt)
			 , c504_last_updated_by = p_userid
			 , c504_last_updated_date = CURRENT_DATE
		 WHERE c504_consignment_id = p_refid AND c504_void_fl IS NULL;
		 --update status log for shipped IHLN
		 gm_save_status_details (p_refid, '', p_userid, NULL, CURRENT_DATE, 93112, 91108) ; --91108	Inhouse Loaner Items type,93112 -Shipped
	END gm_sav_loaner_item_ship;
	 
 /**********************************************************************************************
 * Description      : Procedure to copy the Loaner Request Shipping info details MNTTASK-8116
 * Author           : Hreddi 
 ***********************************************************************************************/
  PROCEDURE gm_copy_loanerreq_ship_info (
      p_shipid    IN   t907_shipping_info.c907_shipping_id%TYPE    
   )
   AS
   	  v_pro_req_id   	t907_shipping_info.c525_product_request_id%TYPE;
   	  v_ref_id          t907_shipping_info.c907_ref_id%TYPE;
   	  v_ship_to		 	t907_shipping_info.c901_ship_to%TYPE;
   	  v_ship_to_id   	t907_shipping_info.c907_ship_to_id%TYPE;
   	  v_deli_mode    	t907_shipping_info.c901_delivery_mode%TYPE;
   	  v_deli_carrier 	t907_shipping_info.c901_delivery_carrier%TYPE;
   	  v_address_id   	t907_shipping_info.c106_address_id%TYPE;   	  
   	  
   BEGIN	      
	  
	  --fetching ship to and ship to id ,delivery mode,carrier,address id from passing ship id 
	  BEGIN 
	   SELECT t907.c907_ref_id 
	        , t907.c901_ship_to 
	        , t907.c907_ship_to_id 
	        , t907.c901_delivery_mode 
	        , t907.c901_delivery_carrier 
	        , t907.c106_address_id 
	     INTO v_ref_id,v_ship_to,v_ship_to_id,v_deli_mode,v_deli_carrier,v_address_id
	     FROM t907_shipping_info t907
	    WHERE t907.c907_shipping_id = p_shipid 
	      AND c907_void_fl IS NULL FOR UPDATE;   	
	   EXCEPTION
         WHEN NO_DATA_FOUND
          THEN
              RETURN;
        END;
        --fetching prod req id from t526_product_request_detail
        BEGIN 
	      	SELECT c525_product_request_id INTO v_pro_req_id 
   			  FROM t526_product_request_detail
  			 WHERE TO_CHAR(c526_product_request_detail_id) = v_ref_id
    		  AND c526_void_fl                  IS NULL;
	    EXCEPTION
         WHEN NO_DATA_FOUND
          THEN
          --fetching prod req id from t526_product_request_detail,t504a_loaner_transaction
            BEGIN
              SELECT t526.C525_Product_Request_Id INTO v_pro_req_id 
   				FROM t504a_loaner_transaction t504a, t526_product_request_detail t526
 				WHERE t504a.c504_consignment_id = v_ref_id
    			 AND t504a.c504a_return_dt    IS NULL
    			 AND t504a.c504a_void_fl      IS NULL
    			 AND t504a.c526_product_request_detail_id=t526.c526_product_request_detail_id
    			 AND T526.C526_Void_Fl is null;
    		 EXCEPTION
         		WHEN NO_DATA_FOUND
          	 THEN	
          	  RETURN;
       		 END;
         END;
        
        
	    -- Updating the Shipping information to the all child requests of a Loaner Request	(inhouse & product)    
	    UPDATE t907_shipping_info  
          SET c901_ship_to 				= v_ship_to
            , c907_ship_to_id 			= v_ship_to_id
            , c901_delivery_mode 		= v_deli_mode
            , c901_delivery_carrier 	= v_deli_carrier
            , c106_address_id 			= v_address_id
        WHERE c525_product_request_id 	= v_pro_req_id
          AND c907_status_fl 			<> '40'
          AND c901_source 				IN ('50182','50186')
          AND c907_void_fl 				IS NULL
          AND c907_tracking_number 		IS NULL
          AND c907_ref_id 				IN (
          SELECT NVL(DECODE (t526.c526_status_fl, '20', 
          	gm_pkg_op_product_requests.get_loanerreq_consignment_id(t526.c526_product_request_detail_id, '10'), NULL), 
          	t526.c526_product_request_detail_id) 
                 FROM t526_product_request_detail t526
                  WHERE t526.c525_product_request_id 	= v_pro_req_id
                  AND t526.c526_status_fl 			IN ('5','10','20')
                  AND t526.c526_void_fl 				IS NULL 
                  AND NVL(gm_pkg_op_tissues.get_tissue_set_fl(c207_set_id, v_ship_to, v_address_id), 'N') = 'N' --PC-414 - To skip updating shipping address for requests having tissue parts
             UNION
           SELECT C504_CONSIGNMENT_ID
			   FROM T504_CONSIGNMENT
			  WHERE C504_REF_ID     = TO_CHAR(v_pro_req_id)
			    AND C504_TYPE       = 9110
			    AND C504_VOID_FL   IS NULL
			    AND C504_STATUS_FL IN (2, 3)                 
                  );
                                             
	END gm_copy_loanerreq_ship_info;
	/******************************************************************
	   * Description : Procedure for Return FGRT/QNRT ShipOut
	   ****************************************************************/	
	PROCEDURE gm_sav_return_ship (
		p_refid    	   IN	    t907_shipping_info.c907_ref_id%TYPE
	  , p_shipdt	   IN		DATE
	  , p_shipcar	   IN		t504_consignment.c504_delivery_carrier%TYPE
	  , p_shipmode	   IN		t504_consignment.c504_delivery_mode%TYPE
	  , p_track 	   IN		t504_consignment.c504_tracking_number%TYPE
	  , p_freightamt   IN		t501_order.c501_ship_cost%TYPE
	  , p_shipto	   IN		t504_consignment.c504_ship_to%TYPE
	  , p_shiptoid	   IN		t504_consignment.c504_ship_to_id%TYPE
	  , p_userid	   IN		t504_consignment.c504_last_updated_by%TYPE
	)
	AS
	  v_pick_loc    VARCHAR2 (20);
	  v_trans_type  t504_consignment.c504_type%TYPE;
	  v_message VARCHAR2 (50);
	  v_assignid t5050_invpick_assign_detail.c5050_invpick_assign_id%TYPE;
	  v_plant_id      t501_order.c5040_plant_id%TYPE;
	  v_rma_id t506_returns.c506_rma_id%TYPE;
	  
	BEGIN
		
		BEGIN
		  SELECT c504_type,c5040_plant_id INTO v_trans_type, v_plant_id 
		    FROM t504_consignment 
		   WHERE c504_consignment_id=p_refid
		     AND c504_void_fl IS NULL;
		EXCEPTION WHEN NO_DATA_FOUND THEN
			GM_RAISE_APPLICATION_ERROR('-20999','18',p_refid);
		END;
		
	  	v_pick_loc      := get_rule_value ('PICK_LOC', v_trans_type);
	  	
	    IF v_pick_loc = 'YES' THEN
	    	gm_pkg_op_item_control_txn.gm_sav_location_qty (p_refid, v_trans_type, 93343, p_userid);
	    END IF;
	    
	      -- update t412 status
	  gm_update_reprocess_material (p_refid, 'FG Location Transfer for : ' || p_refid, v_trans_type, p_userid --
	  -- p_verifiedby
	  , p_userid, v_message -- OUT
	  );
	  -- update t5050 status
	  BEGIN
	    SELECT c5050_invpick_assign_id
	    INTO v_assignid
	    FROM t5050_invpick_assign_detail
	    WHERE c5050_ref_id = p_refid
	    AND c5050_void_fl IS NULL
	    AND c5040_plant_id = v_plant_id;
	  EXCEPTION
	  WHEN NO_DATA_FOUND THEN
	    v_assignid := NULL;
	  END;
	  -- When the putaway save is called from Device, the p_trans_type passed will
	  -- be the c901_code_nm_alt value for the transaction and that value will not be
	  -- in the T5050 table. As C5050_ref_id and void fl combination is unique,we do not
	  -- need to check the c901_ref_type value.
	  --AND c901_ref_type = p_trans_type
	  IF v_assignid IS NOT NULL THEN
	    gm_pkg_op_inv_scan.gm_sav_inv_order_status (v_assignid, 'Completed', p_userid );
	  END IF;
	  --	Initiate RA
	  gm_pkg_op_return_txn.gm_op_sav_ra_ict_return(p_refid,p_userid, v_rma_id);
	  
	  	UPDATE t504_consignment
		   SET c504_ship_to = p_shipto
			 , c504_ship_to_id = p_shiptoid
			 , c504_delivery_carrier = p_shipcar
			 , c504_delivery_mode = p_shipmode
			 , c504_tracking_number = p_track
			 , c504_ship_date = p_shipdt
			 , c504_ref_id = v_rma_id
			 , c901_ref_type = '26240144' --Return to Owner Company 
			 , c504_last_updated_by = p_userid
			 , c504_last_updated_date = CURRENT_DATE
		 WHERE c504_consignment_id = p_refid AND c504_void_fl IS NULL;
	  
	END gm_sav_return_ship;	
	
/*************************************************************
 * Description      : Procedure to update posting for PC-4932
 * Author           : asingaravel 
 *************************************************************/	
	PROCEDURE gm_upd_order_consignment_inventory(
		p_order_id		IN		t501_order.c501_order_id%type,
		p_order_type	IN		t501_order.c901_order_type%type,
		p_user_id		IN		t101_user.c101_user_id%type
	)
	AS
	v_party_id			VARCHAR2(50);
	v_txn_cnt	   	    NUMBER;
	p_msg		   		VARCHAR2 (100);
	v_cn_post_id		NUMBER;
	v_verify_fl			NUMBER;
	
	CURSOR cur_sales
    IS
        SELECT t502.c205_part_number_id ID, SUM (t502.c502_item_qty) qty,
               t502.c502_item_price price, t502.c901_type itype, NVL(t501.c901_order_type, '-9999') ordertype,
               GET_RULE_VALUE (NVL(t501.c901_order_type, '-9999'), 'ORDER_SALES_POSTING') order_sales_post_id
          FROM t502_item_order t502, t501_order t501
         WHERE t502.c501_order_id = p_order_id
           AND t502.c501_order_id = t501.c501_order_id AND t502.C502_VOID_FL IS NULL 
      GROUP BY t502.c205_part_number_id, t502.c502_item_price, t502.c901_type, t501.c901_order_type;
    
    CURSOR fch_cn
    IS
    	SELECT c504_consignment_id cnid,c5000_ref_id quoteid
    	  FROM T5000_ORDER_CONSIGNMENT_MAPPING
		 WHERE c501_order_id = p_order_id
		   AND c5000_void_fl is null;
	BEGIN
		
		FOR v_fch_cn  IN fch_cn
		LOOP
			SELECT nvl(c504_verify_fl,0)
			  INTO v_verify_fl
			  FROM T504_CONSIGNMENT
			 WHERE c504_consignment_id = v_fch_cn.cnid
			   AND c504_void_fl is null;
			
			--To verify completed set and make insert to costing table
			gm_pkg_sm_robotics_order_txn.gm_upd_save_set_build(v_fch_cn.cnid,p_user_id);
		       
			IF v_verify_fl = 0 THEN
			 	UPDATE t504_consignment
				   SET c504_verify_fl = '1',
					   c504_last_updated_date = CURRENT_DATE,
				       c504_last_updated_by = p_user_id
				 WHERE c504_consignment_id = v_fch_cn.cnid
				   AND c504_void_fl is null;
			END IF;
			 --to update verify flag,CN status,Ship date and update request as completed
				gm_pkg_sm_robotics_order_txn.gm_upd_cn_status(p_order_id,v_fch_cn.cnid,'4',p_user_id); --Completed
			
			  --to update tag status based on CN
				gm_pkg_sm_robotics_order_txn.gm_upd_tag_status_by_cn(p_order_id,v_fch_cn.cnid,v_fch_cn.quoteid,'26240343',p_user_id); 
		END LOOP;
		
		IF p_order_type = '26240232' THEN --Direct
			v_cn_post_id := 26241348;  --Built Set to Direct Order Sales
		ELSE
			v_cn_post_id := 26241349;  --Built Set to OUS Direct Order Sales
		END IF;
		
     	FOR rad_val IN cur_sales
     	LOOP
     		--gm_cm_sav_partqty(rad_val.ID,(rad_val.qty* -1),p_order_id,p_user_id,90814,4302,4310);
     		
     		gm_save_ledger_posting(v_cn_post_id,CURRENT_DATE,rad_val.ID,null,p_order_id,rad_val.qty,null,p_user_id);
     		
     	END LOOP;
     		   
     	UPDATE t501_order
		   SET c501_update_inv_fl = '1'
			 , c501_last_updated_by = p_user_id
			 , c501_last_updated_date = CURRENT_DATE
		 WHERE c501_order_id = p_order_id 
		   AND c501_void_fl IS NULL;
		   
		--Call the below new prc to update the main inventory qty for the Inserts.
		 BEGIN
			 SELECT COUNT(1)
			   INTO v_txn_cnt
			   FROM T5056_TRANSACTION_INSERT
			  WHERE C5056_REF_ID = p_order_id
			    AND C5056_VOID_FL IS NULL;
			EXCEPTION
			WHEN NO_DATA_FOUND THEN
			  v_txn_cnt := 0;
			END;
			IF v_txn_cnt > 0 THEN 
				gm_update_insert_inventory(p_order_id, 'S', '', p_user_id, p_msg);
			END IF;
     	
		
	END gm_upd_order_consignment_inventory;
	
END gm_pkg_cm_shipping_trans;
/