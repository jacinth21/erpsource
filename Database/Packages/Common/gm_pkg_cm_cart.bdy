/* Formatted on 2010/02/09 15:11 (Formatter Plus v4.8.0) */
-- @"c:\database\packages\common\gm_pkg_cm_cart.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_cm_cart
IS
--
	PROCEDURE gm_ac_fch_maqty (
		p_partnum	  IN	   t205_part_number.c205_part_number_id%TYPE
	  , p_txntype	  IN	   t901_code_lookup.c901_code_id%TYPE
	  , p_outdetail   OUT	   TYPES.cursor_type
	)
	AS
		/*******************************************************
			 * Description : Procedure to fetch MA qty and cost
			 * Author	: Joe P Kumar
	* if TxnType is from scrap, then get the qty and cost from FG
		*******************************************************/
	v_total_qty number;
	-- International posting changes (PMT-13199)
	v_company_id t1900_company.c1900_company_id%TYPE;
	v_owner_company_id  t1900_company.c1900_company_id%TYPE;
	v_owner_company_cd  t1900_company.c1900_company_cd%TYPE;
	v_owner_currency VARCHAR2 (40);
	v_price NUMBER;
	v_local_price NUMBER;
	v_local_currency VARCHAR2 (40);
	v_plant_id_ctx t5040_plant_master.c5040_plant_id%TYPE;
	v_time_zone t901_code_lookup.c901_code_nm%TYPE;
    v_time_format t901_code_lookup.c901_code_nm%TYPE;
    v_vendor_id t301_vendor.c301_vendor_id%TYPE;
    v_costing_id t820_costing.c820_costing_id%TYPE;
	BEGIN
	--
      SELECT get_plantid_frm_cntx()
      	INTO v_plant_id_ctx
      FROM dual;
    -- to get the paranet company id
    v_company_id := get_plant_parent_comp_id (v_plant_id_ctx);
    --
       SELECT get_code_name(c901_timezone), get_code_name(c901_date_format)
        INTO v_time_zone,v_time_format
        FROM t1900_company
        WHERE c1900_company_id=v_company_id;
	-- to set the company information to context
 		gm_pkg_cor_client_context.gm_sav_client_context(v_company_id,v_time_zone,v_time_format,v_plant_id_ctx);
 		
		IF p_txntype = '4900' then
			DELETE from my_temp_part_list;
				INSERT INTO my_temp_part_list (c205_part_number_id) values(p_partnum);
		    	SELECT  NVL(c205_inshelf,0)+ NVL(c205_shelfalloc,0) +			
						NVL(c205_inbulk,0) + NVL(c205_in_wip_set,0) + NVL(c205_tbe_alloc,0)+ NVL(c205_bulkalloc,0)+
						NVL(c205_returnavail,0)+NVL(c205_returnalloc,0)+
						NVL(c205_repackavail,0)+NVL(c205_repackalloc,0) 
				INTO  v_total_qty
				FROM v205_qty_in_stock;
		END IF;
		-- to get the owner company information
		v_costing_id := get_ac_costing_id (p_partnum, nvl (p_txntype, '4900'));
		
		BEGIN
			SELECT t820.c1900_owner_company_id, t820.c901_owner_currency
			  , t820.c820_purchase_amt, t820.c901_local_company_currency
			  , t820.c820_local_company_cost
			   INTO v_owner_company_id, v_owner_currency
			  , v_price, v_local_currency
			  , v_local_price
			   FROM t820_costing t820
			  WHERE t820.c820_costing_id =v_costing_id;
	   EXCEPTION WHEN NO_DATA_FOUND
	   THEN
	   		v_owner_company_id := NULL;
	   		v_owner_currency := NULL;
	   		v_price := NULL;
	   		v_local_currency := NULL;
	   END;	    
	    --
		OPEN p_outdetail
		 FOR
			 SELECT NVL (get_qty_onhand (t205.c205_part_number_id, p_txntype), -9999) qoh, t205.c205_part_num_desc pdesc
				  ,  v_price price
				  , DECODE (p_txntype
						  , 4900, v_total_qty
						  , 4909, ROUND (get_partnumber_qty (t205.c205_part_number_id, 90802), 4)
						  , 4902, (get_partnumber_qty (t205.c205_part_number_id, 50813) - get_qty_in_transactions_quaran (t205.c205_part_number_id))
						  , -9999
						   ) oprqoh
				, DECODE (p_txntype
						  , 4900, get_qty_in_stock (t205.c205_part_number_id)
						  , 4909, ROUND (get_partnumber_qty (t205.c205_part_number_id, 90802), 4)
						  , 4902, (get_partnumber_qty (t205.c205_part_number_id, 50813) - get_qty_in_transactions_quaran (t205.c205_part_number_id))
						  , -9999
						   ) oprqohop, v_owner_company_cd ownercd, v_owner_currency ownerccy, get_comp_curr (t2023.c1900_company_id) localccy, v_local_price localprice
						  , v_owner_company_id ownerid
				   FROM t205_part_number t205, t2023_part_company_mapping t2023
			  WHERE t205.c205_part_number_id = t2023.c205_part_number_id 
			  AND t2023.c1900_company_id = v_company_id
			   AND t205.c205_part_number_id = p_partnum
			   AND t2023.c2023_void_fl IS NULL;
	END gm_ac_fch_maqty;

	/*******************************************************
		* Description : Procedure to fetch set details
		* Author	 : Brinal G
	*******************************************************/
	PROCEDURE gm_fch_setdetails (
		p_setid 	  IN	   t207_set_master.c207_set_id%TYPE
	  , p_outdetail   OUT	   TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_outdetail
		 FOR
			 SELECT c207_set_id setid, c207_set_nm pdesc, gm_pkg_op_loaner.get_available_sets (p_setid, 0, 4127) qty ,
			        c207_tissue_fl tissue_fl -- PMT-40240 - getting Tissue parts set Flag
			   FROM t207_set_master
			  WHERE c207_set_id = p_setid AND c207_void_fl IS NULL;
	END gm_fch_setdetails;

	/******************************************************************
 * Description : This function returns the flag for the tagid/etch id
 *				 based on the parameter.
 * Author		 : rshah
 ******************************************************************/
--
	FUNCTION get_refid_flag (
		p_refid   IN   t5010_tag.c5010_tag_id%TYPE
	  , p_type	  IN   t501_order.c901_reason_type%TYPE
	  , p_pnum	  IN   t5010_tag.c205_part_number_id%TYPE
	)
		RETURN VARCHAR2
	IS
		v_cnt		   NUMBER;
	BEGIN
		SELECT COUNT (1)
		  INTO v_cnt
		  FROM t5010_tag t5010, t208_set_details t208, t208_set_details t208b
		 WHERE t5010.c5010_tag_id = p_refid   --'0002194'
		   AND t5010.c205_part_number_id = t208.c205_part_number_id
		   AND t208.c208_critical_fl = 'Y'
		   AND t208.c208_void_fl IS NULL
		   AND t5010.c5010_void_fl IS NULL
		   AND t208.c207_set_id NOT LIKE '300%'
		   AND t208b.c207_set_id = t208.c207_set_id
		   AND t208b.c208_void_fl IS NULL
		   AND t208b.c205_part_number_id = p_pnum	--'109.463'
		   AND t5010.c901_status = 51012   -- Active
		   AND t5010.c901_location_type = 4120;   -- Distributor

		IF v_cnt = 0
		THEN
			RETURN '';
		END IF;

		RETURN 'Y';
	END get_refid_flag;
	
		/***************************************************************
	 * Description : This Procedure is used to get part desc
	 * Author	   : aprasath
	 ****************************************************************/
	FUNCTION gm_fch_part_desc (
	  p_num   		t205_part_number.c205_part_number_id%TYPE
	)
		RETURN VARCHAR2
	IS	   
	    v_partdesc t205_part_number.c205_part_num_desc%TYPE;	
	    v_company_id    T1900_COMPANY.C1900_COMPANY_ID%TYPE;	   
	 BEGIN	 	 
		SELECT get_compid_frm_cntx()  INTO v_company_id FROM DUAL;
		BEGIN
			SELECT t205.c205_part_num_desc 
					INTO v_partdesc
				FROM t2023_part_company_mapping T2023,
				  t205_part_number t205
				WHERE t205.c205_active_fl         = 'Y'
				AND t205.c205_part_number_id     IN (p_num)
				AND t205.c205_part_number_id      = T2023.C205_PART_NUMBER_ID
				AND T2023.c2023_void_fl          IS NULL
				AND T2023.c1900_company_id        = v_company_id
				AND T2023.c901_part_mapping_type IN ( 105360,105361 );					
		EXCEPTION
		WHEN NO_DATA_FOUND THEN
			v_partdesc := NULL;
		END;		
        RETURN v_partdesc;        
	END gm_fch_part_desc ;
	
	
	/*******************************************************
		* Description : To fetch all the parts details from a particular consignment/Tag ID
		* Author	  : D Sandeep
	*******************************************************/
	PROCEDURE gm_fch_part_from_consign (
		p_ref_id 	      IN	    t504_consignment.c504_consignment_id%TYPE
	  , p_consigndetail   OUT	    TYPES.cursor_type
	)
	AS
	v_req_id	   t520_request.c520_request_id%TYPE;
	
	BEGIN

		--PC-133 Gets the Request id by using cn id.
    BEGIN
    
         SELECT c520_request_id INTO v_req_id
		   FROM t504_consignment
		   WHERE c504_consignment_id  = p_ref_id
           AND c504_void_fl is null;
		  
		EXCEPTION WHEN NO_DATA_FOUND 
		THEN
            v_req_id := NULL;
	 END;
		OPEN p_consigndetail
	       FOR
	       
		SELECT t505.c205_part_number_id partnum,
               t505.c505_item_qty iqty,
               decode(gm_pkg_op_do_process_trans.get_control_number_needed_fl(t505.c205_part_number_id),'Y',t505.c505_control_number,'') cntrlnum,
               c205_product_class parttype
          FROM t504_consignment t504,
               t505_item_consignment t505,
               t5010_tag t5010, t205_part_number t205
         where t504.c504_consignment_id   = t505.c504_consignment_id
           and t205.c205_part_number_id   = t505.c205_part_number_id
           and t504.c504_consignment_id   = t5010.c5010_last_updated_trans_id(+)
           and (t504.c504_consignment_id  = p_ref_id
            OR t5010.c5010_tag_id         = p_ref_id)
           AND t504.c504_void_fl         IS NULL
           and t505.c505_void_fl         is null
           AND t5010.c5010_void_fl(+)    IS NULL
           
     UNION ALL    --PC-133
        
        SELECT t205.c205_part_number_id partnum,  t521.c521_qty iqty, '' cntrlnum, c205_product_class parttype
          FROM t521_request_detail t521, t205_part_number t205
          WHERE t521.c205_part_number_id = t205.c205_part_number_id 
          AND t521.c520_request_id IN 
            (  SELECT c520_request_id 
               FROM t520_request t520 
               WHERE (c520_master_request_id = v_req_id  OR c520_request_id = v_req_id) 
               AND t520.c520_status_fl = '10'  --Back Order in Open status
               AND t520.c520_void_fl IS NULL );
	       
	END gm_fch_part_from_consign;
	
END gm_pkg_cm_cart;
/
