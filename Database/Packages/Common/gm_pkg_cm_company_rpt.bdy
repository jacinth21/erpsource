CREATE OR REPLACE PACKAGE body gm_pkg_cm_company_rpt
AS
  /**************************************************************************************
    * Description :  This procedure is used to fetch the company details for orders
    * Author      :  asophia
    * PMT#        :  PMT-42246
    * Date        :  20-Dec-2019
    ***************************************************************************************/
    PROCEDURE gm_fch_company_dtls_from_order (
    p_txn_id 	IN    	t501_order.c501_order_id%type,    
    p_out_str 	OUT 	CLOB) 
            
    AS
    BEGIN  
       SELECT JSON_OBJECT('cmpid' VALUE T501.C1900_COMPANY_ID,
				'cmplangid' VALUE T1900.C901_LANGUAGE_ID,
				'plantid'   VALUE T501.C5040_PLANT_ID )
        INTO p_out_str
        FROM T501_ORDER t501,
        T1900_COMPANY t1900
        WHERE C501_ORDER_ID = p_txn_id
        AND T501.C1900_COMPANY_ID = T1900.C1900_COMPANY_ID
        AND T1900.C1900_VOID_FL IS NULL
        AND T501.C501_VOID_FL IS NULL;
		        
     END gm_fch_company_dtls_from_order;  
    /**************************************************************************************
    * Description :  This procedure is used to fetch the company details for invoice
    * Author      :  asophia
    * PMT#        :  PMT-42246
    * Date        :  20-Dec-2019
    ***************************************************************************************/

    PROCEDURE gm_fch_company_dtls_from_invoice (
            p_txn_id 	IN    t503_invoice.c503_invoice_id%type,    
            p_out_str 	OUT   CLOB) 
       
    AS
    BEGIN  
      SELECT JSON_OBJECT('cmpid' VALUE T503.C1900_COMPANY_ID,
				       'cmplangid' VALUE T1900.C901_LANGUAGE_ID
				        )
        INTO p_out_str
        FROM T503_INVOICE t503,
        T1900_COMPANY t1900
        WHERE C503_INVOICE_ID = p_txn_id
        AND T503.C1900_COMPANY_ID = T1900.C1900_COMPANY_ID
        AND T1900.C1900_VOID_FL IS NULL
        AND T503.C503_VOID_FL IS NULL;
        
	       
    END gm_fch_company_dtls_from_invoice;
    
END gm_pkg_cm_company_rpt;
/