/* Formatted on 2009/07/21 14:09 (Formatter Plus v4.8.0) */
--@"C:\database\Packages\Common\gm_pkg_cm_rule_cancel.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_cm_rule_cancel
IS
/*******************************************************
	* Description : Procedure to void consequences.
	* Author		: Ritesh Shah
	*******************************************************/
	PROCEDURE gm_rule_sav_void_consequence (
		p_txn_id   IN	t907_cancel_log.c907_ref_id%TYPE
	  , p_userid   IN	t102_user_login.c102_user_login_id%TYPE
	)
	AS
		v_cnt		   NUMBER;
		v_cons_cnt	   NUMBER;
		v_rule_id	   t9700_rule.c9700_rule_id%TYPE;
	BEGIN

		SELECT DISTINCT t9704.c9700_rule_id
				   INTO v_rule_id
				   FROM t9704_rule_consequence t9704
				  WHERE t9704.c9704_rule_consequence_id = p_txn_id;

		UPDATE t9705_consequence_details
		   SET c9705_void_fl = 'Y'
		 WHERE c9704_rule_consequence_id = p_txn_id;

		UPDATE t9704_rule_consequence
		   SET c9704_void_fl = 'Y'
			 , c9704_last_updated_by = p_userid
			 , c9704_last_updated_date = SYSDATE
		 WHERE c9704_rule_consequence_id = p_txn_id;

		SELECT COUNT (1)
		  INTO v_cons_cnt
		  FROM t9704_rule_consequence t9704
		 WHERE t9704.c9700_rule_id = v_rule_id AND t9704.c9704_void_fl IS NULL;

		IF v_cons_cnt = 0
		THEN
			UPDATE t9700_rule
			   SET c9700_active_fl = ''
			 WHERE c9700_rule_id = v_rule_id;
		END IF;
	END gm_rule_sav_void_consequence;

		/*******************************************************
	* Description : Procedure to void rule.
	* Author		: Ritesh Shah
	*******************************************************/
	PROCEDURE gm_rule_sav_void_rule (
		p_txn_id   IN	t907_cancel_log.c907_ref_id%TYPE
	  , p_userid   IN	t102_user_login.c102_user_login_id%TYPE
	)
	AS
		v_cnt		   NUMBER;
	BEGIN
		SELECT COUNT (*)
		  INTO v_cnt
		  FROM t9700_rule
		 WHERE c9700_rule_id = p_txn_id AND c9700_void_fl IS NOT NULL;

		IF (v_cnt > 0)
		THEN
			raise_application_error (-20129, p_txn_id);
		END IF;

		UPDATE t9705_consequence_details
		   SET c9705_void_fl = 'Y'
		 WHERE c9704_rule_consequence_id IN (SELECT c9704_rule_consequence_id
											   FROM t9704_rule_consequence
											  WHERE c9700_rule_id = p_txn_id);

		UPDATE t9704_rule_consequence
		   SET c9704_void_fl = 'Y'
			 , c9704_last_updated_by = p_userid
			 , c9704_last_updated_date = SYSDATE
		 WHERE c9700_rule_id = p_txn_id;

		UPDATE t9703_rule_transaction
		   SET c9703_void_fl = 'Y'
		 WHERE c9700_rule_id = p_txn_id;

		UPDATE t9702_rule_condition
		   SET c9702_void_fl = 'Y'
			 , c9702_last_updated_by = p_userid
			 , c9702_last_updated_date = SYSDATE
		 WHERE c9700_rule_id = p_txn_id;

		UPDATE t9700_rule
		   SET c9700_void_fl = 'Y'
			 , c9700_last_updated_by = p_userid
			 , c9700_last_updated_date = SYSDATE
		 WHERE c9700_rule_id = p_txn_id;
	END gm_rule_sav_void_rule;
END gm_pkg_cm_rule_cancel;
/
