/*  (Formatter Plus v4.8.0) */
--@"C:\Database\Packages\Common\gm_pkg_cm_npi_process_rpt.bdy";	

CREATE OR REPLACE PACKAGE BODY gm_pkg_cm_npi_process_rpt
IS	
	/*******************************************************
		* Description : Procedure to fetch npi transaction info
		* Parameters	 :
		*******************************************************/
	PROCEDURE gm_fch_npi_transaction (
		p_ref_id     IN    t6640_npi_transaction.c6640_ref_id %TYPE,
		p_siteinfo	 OUT   TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_siteinfo
		 FOR
			SELECT c6640_id npttxnid,
			  c6600_surgeon_npi_id npiid,
			  c6640_surgeon_name surgeonnm,
			  c6640_valid_fl validfl,
        	  c6640_user_validated uservalidfl
			FROM t6640_npi_transaction
			WHERE c6640_ref_id      = p_ref_id
			AND c6640_transaction_fl='Y'
			AND c6640_void_fl      IS NULL;
	END gm_fch_npi_transaction;

	/**********************************************************************************************
		* Description : Procedure to fetch npi transaction info where transaction flag is not 'Y'
		*******************************************************************************************/
	PROCEDURE gm_fch_npi_temp_transaction (
		p_ref_id     IN    t6640_npi_transaction.c6640_ref_id %TYPE,
		p_siteinfo	 OUT   TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_siteinfo
		 FOR
			SELECT c6640_id npttxnid,
			  c6600_surgeon_npi_id npiid,
			  c6640_surgeon_name surgeonnm
			FROM t6640_npi_transaction
			WHERE c6640_ref_id      = p_ref_id
			AND c6640_void_fl      IS NULL
			ORDER BY c6640_id;
	END gm_fch_npi_temp_transaction;
	
	/*******************************************************
	* Description : Procedure to fetch npi history details
	********************************************************/
	PROCEDURE gm_fch_npi_hist_dtls (
		p_ref_id     IN    t6640_npi_transaction.c6640_ref_id %TYPE,
		p_rep_id     IN    t6640_npi_transaction.c703_sales_rep_id %TYPE,
		p_out_cur	 OUT   TYPES.cursor_type
	)
	AS
	v_company_id	t1900_company.c1900_company_id%TYPE;
	v_plant_id 		t5040_plant_master.c5040_plant_id%TYPE;
	v_accid			t501_order.c704_account_id%TYPE;
	v_rep_id		t501_order.c703_sales_rep_id%TYPE := p_rep_id;
	BEGIN
		SELECT get_compid_frm_cntx() , get_plantid_frm_cntx()
	  		INTO v_company_id , v_plant_id
	  	FROM dual;
	  	-- Get the account and sales rep id from transaction
		IF p_rep_id IS NULL OR p_rep_id = ''
		THEN
		  	BEGIN
				SELECT c704_account_id,
				  c703_sales_rep_id
				  INTO v_accid, v_rep_id 
				FROM t501_order
				WHERE c501_order_id = p_ref_id
				AND c501_void_fl   IS NULL;
			EXCEPTION
			WHEN NO_DATA_FOUND THEN
				RETURN;
			END;
		END IF;
		
		OPEN p_out_cur
		FOR
			SELECT DISTINCT c6600_surgeon_npi_id npiid, c6645_surgeon_name surgeonnm
			FROM t6645_npi_account
			WHERE c703_sales_rep_id = v_rep_id
			--AND c704_account_id     = v_accid
			AND c6645_void_fl      IS NULL
			AND c1900_company_id    = v_company_id
			AND c5040_plant_id      = v_plant_id;	
		
	END gm_fch_npi_hist_dtls;
	
END gm_pkg_cm_npi_process_rpt;
/
