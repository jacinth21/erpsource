/* Formatted on 2011/06/17 15:03 (Formatter Plus v4.8.0) */
-- @"c:\database\packages\common\gm_pkg_common_cancel_op.bdy"

CREATE OR REPLACE PACKAGE BODY gm_pkg_common_cancel_op
IS
--
	PROCEDURE gm_fc_sav_void_demandsheet (
		p_demandsheetid   IN   t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_user_id		  IN   t101_user.c101_user_id%TYPE
	)
	AS
		/*******************************************************
			  * Description : Procedure to void Demand Sheet
			  * Parameters		: 1. Demand Sheet Id
			  * 				2. User Id
			  *
			  *******************************************************/
		v_void_fl	   t4020_demand_master.c4020_void_fl%TYPE;
	--
	BEGIN
		SELECT		  c4020_void_fl
				 INTO v_void_fl
				 FROM t4020_demand_master
				WHERE c4020_demand_master_id = p_demandsheetid
		FOR UPDATE OF c4020_void_fl;

		IF v_void_fl IS NOT NULL
		THEN
			-- Error message is Sorry. Demand Sheet has already been voided.
			raise_application_error (-20775, '');
		END IF;

		UPDATE t4020_demand_master
		   SET c4020_void_fl = 'Y'
			 , c4020_last_updated_by = p_user_id
			 , c4020_last_updated_date = CURRENT_DATE
		 WHERE c4020_demand_master_id = p_demandsheetid;
	END gm_fc_sav_void_demandsheet;

	--
	PROCEDURE gm_fc_sav_void_group (
		p_groupid	IN	 t4010_group.c4010_group_id%TYPE
	  , p_user_id	IN	 t101_user.c101_user_id%TYPE
	)
	AS
		/*******************************************************
		 * Description : Procedure to void Group
		 * Parameters  : 1. Group Id
		 *				   2. User Id
		 *
		 *******************************************************/
		v_void_fl	   t4010_group.c4010_void_fl%TYPE;
		v_publish_fl   t4010_group.c4010_publish_fl%TYPE;
	--
	BEGIN
		SELECT		  c4010_void_fl
				 INTO v_void_fl
				 FROM t4010_group
				WHERE c4010_group_id = p_groupid
		FOR UPDATE OF c4010_void_fl;

		IF v_void_fl IS NOT NULL
		THEN
			-- Error message is Sorry.Group has already been voided.
			raise_application_error (-20776, '');
		END IF;

		v_publish_fl := gm_pkg_pd_group_pricing.get_publish_fl (p_groupid);

		IF v_publish_fl = 'Y'
		THEN
			-- Group can not be voided, as it is already published.
			raise_application_error (-20777, '');
		END IF;

		UPDATE t4010_group
		   SET c4010_void_fl = 'Y'
			 , c4010_last_updated_by = p_user_id
			 , c4010_last_updated_date = CURRENT_DATE
		 WHERE c4010_group_id = p_groupid;
        
		 --After the Group is voided, we need to remove the Group Visibility from All Companies.
		 gm_pkg_pd_group_txn.gm_void_group_company_map(p_groupid, '', p_user_id);
	
	END gm_fc_sav_void_group;

--
  --
	PROCEDURE gm_fc_sav_void_ttp (
		p_ttpid 	IN	 t4050_ttp.c4050_ttp_id%TYPE
	  , p_user_id	IN	 t101_user.c101_user_id%TYPE
	)
	AS
		/*******************************************************
		 * Description : Procedure to void TTP
		 * Parameters  : 1. Group Id
		 *				   2. User Id
		 *
		 *******************************************************/
		v_void_fl	   t4050_ttp.c4050_void_fl%TYPE;
	--
	BEGIN
		SELECT		  c4050_void_fl
				 INTO v_void_fl
				 FROM t4050_ttp
				WHERE c4050_ttp_id = p_ttpid
		FOR UPDATE OF c4050_void_fl;

		IF v_void_fl IS NOT NULL
		THEN
			-- Error message is Sorry.TTP has already been voided.
			raise_application_error (-20778, '');
		END IF;

		UPDATE t4050_ttp
		   SET c4050_void_fl = 'Y'
			 , c4050_last_updated_by = p_user_id
			 , c4050_last_updated_date = CURRENT_DATE
		 WHERE c4050_ttp_id = p_ttpid;
	END gm_fc_sav_void_ttp;

--
	PROCEDURE gm_fc_sav_void_part_growth (
		p_refid 	IN	 t4030_demand_growth_mapping.c4030_demand_growth_id%TYPE
	  , p_user_id	IN	 t101_user.c101_user_id%TYPE
	)
	AS
		/*******************************************************
		 * Description : Procedure to void Group Part Growth
		 * Parameters  : 1. Group Id
		 *				   2. User Id
		 *
		 *******************************************************/
		v_void_fl	   t4050_ttp.c4050_void_fl%TYPE;
	--
	BEGIN
		SELECT c4030_void_fl
		  INTO v_void_fl
		  FROM t4030_demand_growth_mapping t4030
		 WHERE c4030_ref_id = p_refid;

		IF v_void_fl IS NOT NULL
		THEN
			-- Error message is Sorry.This has already been voided.
			raise_application_error (-20778, '');
		END IF;

		UPDATE t4030_demand_growth_mapping t4030
		   SET t4030.c4030_void_fl = 'Y'
			 , t4030.c4030_last_updated_by = p_user_id
			 , t4030.c4030_last_updated_date = CURRENT_DATE
		 WHERE t4030.c4020_demand_master_id = 26 AND t4030.c901_ref_type = 20298;
	END gm_fc_sav_void_part_growth;

		/*******************************************************
	* Description : Procedure to execute demand sheet
	* Parameters	: 1. Month demand sheet Id
	*					2. User Id
	*******************************************************/
	PROCEDURE gm_op_exec_dsload (
		p_dsmonthid   IN   t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	  , p_user_id	  IN   t101_user.c101_user_id%TYPE
	)
	AS
		v_status_fl    t4040_demand_sheet.c901_status%TYPE;
		v_master_demand_id t4040_demand_sheet.c4020_demand_master_id%TYPE;
	BEGIN
		SELECT c901_status, c4020_demand_master_id
		  INTO v_status_fl, v_master_demand_id
		  FROM t4040_demand_sheet
		 WHERE c4040_demand_sheet_id = p_dsmonthid;

		-- If Sheet is not Open (50550) then throw error
		IF v_status_fl <> '50550'
		THEN
			-- Error message is Sorry. Only OPEN sheets can be reloaded
			raise_application_error (-20779, '');
		END IF;

		gm_pkg_op_ld_demand.gm_op_sav_load_detail (v_master_demand_id, 91951, p_user_id);
	END gm_op_exec_dsload;

	/*******************************************************
	* Description : Procedure to Rollback a loaner return
	*******************************************************/
	PROCEDURE gm_cs_sav_rollback_loaner_retn (
		p_conid    IN	t504_consignment.c504_consignment_id%TYPE
	  , p_userid   IN	t101_user.c101_user_id%TYPE
	)
	AS
		v_date		   VARCHAR2 (20);	--t504a_loaner_transaction.c504a_return_dt%TYPE;
		v_shipto	   t907_shipping_info.c901_ship_to%TYPE;
		v_shiptoid	   t907_shipping_info.c907_ship_to_id%TYPE;
		v_consignedto  t504a_loaner_transaction.c901_consigned_to%TYPE;
		v_consignedtoid t504a_loaner_transaction.c504a_consigned_to_id%TYPE;
		v_repid 	   t504a_loaner_transaction.c703_sales_rep_id%TYPE;
		v_accid 	   t504a_loaner_transaction.c704_account_id%TYPE;
		v_expdtrt	   t504a_loaner_transaction.c504a_expected_return_dt%TYPE;
		v_loanerdt	   t504a_loaner_transaction.c504a_loaner_dt%TYPE;
		v_loan_txn_id  t504a_loaner_transaction.c504a_loaner_transaction_id%TYPE;
  		v_dateformat   VARCHAR2(100);
  		v_status_fl		t504a_consignment_loaner.c504a_status_fl%TYPE;
  		v_current_status t504a_consignment_loaner.c504a_status_fl%TYPE;
  		v_missing_fl	t504a_loaner_transaction.c504a_missing_fl%TYPE;
  BEGIN
		SELECT NVL(get_compdtfmt_frm_cntx(),'mm/dd/yyyy') INTO v_dateformat FROM DUAL;
		
		SELECT gm_pkg_op_loaner.get_return_dt (p_conid)
		  INTO v_date
		  FROM DUAL;
		
		BEGIN
			
		 SELECT MAX(TO_NUMBER (c504a_loaner_transaction_id)) 
		   INTO v_loan_txn_id
   		   FROM t504a_loaner_transaction t504a 
 		  WHERE t504a.c504_consignment_id = p_conid
  			AND c504a_void_fl IS NULL;
			
		SELECT c901_consigned_to, c504a_consigned_to_id, c703_sales_rep_id, c704_account_id, c504a_expected_return_dt
			 , c504a_loaner_dt, c504a_missing_fl
		  INTO v_consignedto, v_consignedtoid, v_repid, v_accid, v_expdtrt
			 , v_loanerdt, v_missing_fl
		  FROM t504a_loaner_transaction
		 WHERE c504a_loaner_transaction_id = v_loan_txn_id;

		 EXCEPTION WHEN NO_DATA_FOUND 
		THEN
			v_consignedto:= NULL;
			v_consignedtoid := NULL;
			v_repid := NULL;
			v_accid := NULL;
			v_expdtrt := NULL;
			v_loanerdt := NULL;
			v_missing_fl := NULL;
		END;   

		UPDATE t504_consignment
		   SET c701_distributor_id = DECODE(v_consignedto,50170,v_consignedtoid,NULL) -- 50170 - Distributor
			 , c704_account_id = v_accid
			 , c504_last_updated_by = p_userid
			 , c504_last_updated_date = CURRENT_DATE
		 -- , c504_ship_to = v_shipto
		 -- , c504_ship_to_id = v_shiptoid
		WHERE  c504_consignment_id = p_conid AND c504_void_fl IS NULL;

		-- 20 rollback to pending return, 23 Deployed ,22 Missed
		SELECT DECODE(c504a_deployed_fl,'Y',23,DECODE(c504a_missed_fl,'Y',22,'D',21,20)) , c504a_status_fl
		INTO v_status_fl,v_current_status
		FROM t504a_consignment_loaner
		WHERE c504_consignment_id = p_conid;
		
		--22 --Missing 25--Pending Check 21--Disputed
		-- Call the common procedure to update the status
		IF v_current_status = 21  AND v_status_fl = 21 THEN
		gm_pkg_op_loaner_set_txn.gm_upd_consign_loaner_status (p_conid, 20, p_userid) ;
		
			--update missing flag as Disputed	
		  UPDATE t504a_consignment_loaner
		  SET c504a_missed_fl       = null, -- updating the missed fl as null from Disputed
		    c504a_last_updated_date = CURRENT_DATE,
		    c504a_last_updated_by   = p_userid
		  WHERE c504_consignment_id = p_conid
		  AND c504a_void_fl        IS NULL;
		
		ELSIF v_current_status = 25  AND v_status_fl = 21 THEN
		gm_pkg_op_loaner_set_txn.gm_upd_consign_loaner_status (p_conid, 21, p_userid) ;
		ELSE
		gm_pkg_op_loaner_set_txn.gm_upd_consign_loaner_status (p_conid, v_status_fl, p_userid) ;
		END IF;

		--22 --Missing 25--Pending Check 21--Disputed
		IF v_status_fl = 22 THEN
			gm_pkg_op_loaner_set_txn.gm_upd_missing_loaner_status (p_conid, 26240614, p_userid) ; -- Missing
		ELSIF v_current_status = 25 AND v_status_fl = 21 THEN
			gm_pkg_op_loaner_set_txn.gm_upd_missing_loaner_status (p_conid, 26240842, p_userid) ; -- Disputed
		ELSIF v_current_status = 21 AND v_status_fl = 21 THEN
			gm_pkg_op_loaner_set_txn.gm_upd_missing_loaner_status (p_conid, 51012, p_userid) ; -- Active
		END IF;

		--PC-239
		IF v_missing_fl = 'Y' THEN
		gm_pkg_op_loaner.gm_upd_loaner_missed(p_conid, p_userid);
		END IF;
		
		UPDATE t504a_consignment_loaner
		   SET c504a_loaner_dt = v_loanerdt
			 , c504a_expected_return_dt = v_expdtrt
			 , c504a_last_updated_by = p_userid
			 , c504a_last_updated_date = CURRENT_DATE 
		 WHERE c504_consignment_id = p_conid;

		UPDATE t504a_loaner_transaction
		   SET c504a_rollback_fl = NULL     -- PMT-46800 - Lot Track - Rollback loaner changes
		     , c504a_return_dt = NULL
			 , c504a_last_updated_by = p_userid
			 , c504a_last_updated_date = CURRENT_DATE
		 WHERE c504_consignment_id = p_conid
		   AND c504a_return_dt = TO_DATE (v_date, v_dateformat)
		   AND c504a_void_fl IS NULL;

		gm_save_status_details (p_conid, '', p_userid, NULL, CURRENT_DATE, 91125, 91103);
		
		-- Need to remove the priority while doing rollback from Pending check to Pending return
		gm_pkg_op_ln_priority_engine.gm_rollback_cn_priority(null,p_userid,p_conid);
		
	END gm_cs_sav_rollback_loaner_retn;

	/***********************************************************
	* Description : All In House Transactions
	* would get voided  in this proceduere, if the inhouse
	* transaction is from loaners, then the parent's loaner
	* status would get updated accordingly (except for FGLE
	* transaction). If the inhouse txn is from consignments,
	* then the parent consignment status will not get updated.
	************************************************************/
	PROCEDURE gm_op_sav_void_inhousetxn (
		p_txn_id   IN	t907_cancel_log.c907_ref_id%TYPE
	  , p_userid   IN	t102_user_login.c102_user_login_id%TYPE
	)
	AS
		v_ref_id	   t504_consignment.c504_reprocess_id%TYPE;
		v_table 	   VARCHAR (10);
		v_verify_fl    VARCHAR2 (1);
		v_ord_cnt	   NUMBER;
		v_count	       NUMBER;
		v_allow_txn_void_fl	   VARCHAR2 (1);
		v_type		   t412_inhouse_transactions.c412_type%TYPE;
		v_ln_txn_id    t412_inhouse_transactions.c504a_loaner_transaction_id%TYPE;
		v_inhouse_txn_id t412_inhouse_transactions.c412_inhouse_trans_id%TYPE;
		v_status_fl      t412_inhouse_transactions.c412_status_fl%TYPE;
		v_trans_count          NUMBER;
		v_reftype	NUMBER;
		v_con_type		t504_consignment.c504_type%TYPE;
		v_out_cur  		TYPES.cursor_type;
		v_consign_type  t504_consignment.c504_type%TYPE;
		v_company_id    t504_consignment.c1900_company_id%TYPE;
	BEGIN
		BEGIN
			SELECT t504.c504_verify_fl,t504.c1900_company_id
			  INTO v_verify_fl,v_company_id
			  FROM t504_consignment t504
			 WHERE t504.c504_consignment_id = p_txn_id;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				BEGIN
					SELECT t412.c412_verify_fl,t412.c1900_company_id
					  INTO v_verify_fl,v_company_id
					  FROM t412_inhouse_transactions t412
					 WHERE c412_inhouse_trans_id = p_txn_id;
				EXCEPTION
					WHEN NO_DATA_FOUND
					THEN
						BEGIN
							SELECT COUNT (1)
							  INTO v_ord_cnt
							  FROM t501_order t501
							 WHERE t501.c501_order_id = p_txn_id AND t501.c501_void_fl IS NULL;
						END;
				END;
		END;
		
		IF v_ord_cnt > 0
		THEN
			GM_RAISE_APPLICATION_ERROR('-20999','22','');
		END IF;

		IF v_verify_fl IS NOT NULL
		THEN
			raise_application_error (-20780, '');
		END IF;

		BEGIN
			SELECT t504.c504_reprocess_id, 'CONS',t504.c504_type
			  INTO v_ref_id, v_table ,v_consign_type
			  FROM t504_consignment t504
			 WHERE t504.c504_consignment_id = p_txn_id;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				BEGIN
					SELECT c412_ref_id, 'INHOUSE', c412_type, c504a_loaner_transaction_id 
						   ,c412_status_fl, c901_ref_type
					  INTO v_ref_id, v_table, v_type, v_ln_txn_id , v_status_fl, v_reftype
					  FROM t412_inhouse_transactions
					 WHERE c412_inhouse_trans_id = p_txn_id;
				EXCEPTION
				    WHEN NO_DATA_FOUND
				    THEN
                        v_ref_id    := NULL;
                        v_table     := NULL;
                        v_type      := NULL;
                        v_ln_txn_id := NULL;
				END;
		END;
		--FGLN transaction should voided only for US , otherwise the OUS countries cannot void.
        IF v_type = '50155' THEN             --FGLN(50155)
			v_allow_txn_void_fl	:= get_rule_value_by_company (v_type, 'INHOUSEVOID',v_company_id);
		ELSE
			v_allow_txn_void_fl	:= get_rule_value (v_type, 'INHOUSEVOID');
		END IF;
		
		/*- The reason behind adding below code for 4119 is to void the FGIH where the ref id is not IHRQ.
		 - As of now we cannont change the status of request to Open becuase the JOB will again run and create FGIH.
		 - The above same thing will happend when they void IHRQ. If we change the parent request status JOB will again run and creates IHRQ for the same part.
			So cant fix the updating the status of request.
		 * 
		 */
		IF v_type IN ('9106','9104') AND v_reftype NOT IN ('4119')  THEN   -- FGIH(9106), BLIH(9104)	
			IF v_status_fl IN ('3','4') THEN
				GM_RAISE_APPLICATION_ERROR('-20999','23','');
			ELSE
				gm_pkg_op_ihloaner_item_txn.gm_sav_ihbo_map(p_txn_id,v_type,v_ref_id,p_userid);
			END IF;
		ELSIF v_type = '1006483' THEN    -- IHRQ(1006483)		    
			IF v_status_fl IN ('4') THEN
				GM_RAISE_APPLICATION_ERROR('-20999','24',p_txn_id);
			ELSE
				gm_pkg_op_ihloaner_item_txn.gm_sav_void_ihbo_req(p_txn_id,v_type,p_userid);
			END IF;
		END IF;	

		IF v_ref_id IS NOT NULL AND v_allow_txn_void_fl IS NULL AND v_type NOT IN ('9106','9104','1006483')
		THEN
			raise_application_error (-20781, '');
		END IF;
		
		IF v_ref_id IS NOT NULL AND v_consign_type IN ('400058','400064','111640') --400058:=RHPN, 400064:=RHQN, 111640:=RHFG
		THEN
			raise_application_error (-20781, '');
		END IF;
		
		IF v_table = 'INHOUSE'
		THEN
			-- Fetch the trasaction details of In-House Transaction
			gm_pkg_ship_rollback_txn.gm_fch_inhouse_txn_details(p_txn_id, v_out_cur);   
			
			UPDATE t412_inhouse_transactions
			   SET c412_void_fl = 'Y'
				 , c412_last_updated_date = CURRENT_DATE
				 , c412_last_updated_by = p_userid
			 WHERE c412_inhouse_trans_id = p_txn_id
			   AND c412_void_fl IS NULL;

			UPDATE t413_inhouse_trans_items t413
			   SET t413.c413_void_fl = 'Y'
				 , t413.c413_last_updated_by = p_userid
				 , t413.c413_last_updated_date = CURRENT_DATE
			 WHERE t413.c412_inhouse_trans_id = p_txn_id
			   AND t413.c413_void_fl IS NULL;
			   
			UPDATE t5062_control_number_reserve t5062
			   SET t5062.c5062_void_fl = 'Y'
				 , t5062.c5062_last_updated_by = p_userid
				 , t5062.c5062_last_updated_date = SYSDATE
			 WHERE t5062.c5062_transaction_id = p_txn_id
			   AND t5062.c5062_void_fl IS NULL;

			-- Need to change status after void for FGLN 
			-- 50154 Shelf to Loaner Extension w/Replenish
			IF v_type IS NOT NULL AND v_allow_txn_void_fl = 'Y' AND v_type = '50154'
			THEN
			    BEGIN
				    SELECT count(1)
				      INTO v_count
				      FROM t412_inhouse_transactions t412,
				           t413_inhouse_trans_items t413
	                 WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
	                   AND t412.c504a_loaner_transaction_id = v_ln_txn_id
	                   AND t412.c412_type = 50159 -- Loaner Set to Loaner Sales
	                   AND t413.c413_status_fl <> 'Q' -- Pending Reconciliation
	                   AND t412.c412_void_fl IS NULL
	                   AND t413.c413_void_fl IS NULL;	  
                END;
                
              --check the associated loaner txn 'GM-LN-xxx' status, if not reconciled, need void it.
              IF v_count = 0   -- all item txn pending reconciliation
              THEN
	              BEGIN
	                  SELECT t412.c412_inhouse_trans_id
				        INTO v_inhouse_txn_id
				        FROM t412_inhouse_transactions t412
	                   WHERE t412.c504a_loaner_transaction_id = v_ln_txn_id
	                     AND t412.c412_type = 50159 -- Loaner Set to Loaner Sales
	                     AND t412.c412_void_fl IS NULL;
	                  EXCEPTION
		                  WHEN NO_DATA_FOUND
						  THEN
				  		      v_inhouse_txn_id := NULL;
				  END;
	                   
                  UPDATE t412_inhouse_transactions
                     SET c412_void_fl = 'Y',
                         c412_last_updated_date = CURRENT_DATE,
                         c412_last_updated_by = p_userid
                   WHERE c412_inhouse_trans_id = v_inhouse_txn_id;
                   
                    -- Fetch the trasaction details of In-House Transaction
				   gm_pkg_ship_rollback_txn.gm_fch_inhouse_txn_details(v_inhouse_txn_id, v_out_cur);
                   
                  UPDATE t413_inhouse_trans_items t413
                     SET t413.c413_void_fl = 'Y'
                       , t413.c413_last_updated_by = p_userid
				       , t413.c413_last_updated_date = CURRENT_DATE
                   WHERE t413.c412_inhouse_trans_id = v_inhouse_txn_id;
              ELSE
              	GM_RAISE_APPLICATION_ERROR('-20999','25',v_inhouse_txn_id||'#$#'||p_txn_id);
              END IF;
			END IF;

			IF v_type IS NOT NULL AND v_allow_txn_void_fl = 'Y' AND v_type!='50154'	
			THEN
				gm_ls_upd_csg_loanerstatus (4, 50, v_ref_id);
			END IF;
		ELSE
		
			--For getting the consignment type, it should fetch record only for item consignments
			SELECT c504_type
				INTO v_con_type
			FROM t504_consignment
			WHERE c504_consignment_id = p_txn_id
			AND c207_set_id IS NULL
			AND c504_void_fl IS NULL;
			-- if the consignment type is FG-ST(106703) or BL-ST(106704) ,then rollback the request details
			-- and request status to requested
			IF v_con_type = '106703' OR v_con_type = '106704'
			THEN
			
			gm_pkg_op_request_fulfill.gm_rollback_stock_trans_req (p_txn_id,p_userid);
			
			END IF;
			
			
			-- Fetch the trasaction details of Consignment Transaction
			gm_pkg_ship_rollback_txn.gm_fch_consign_txn_details(p_txn_id, v_out_cur);
			
			UPDATE t504_consignment
			   SET c504_void_fl = 'Y'
				 , c504_last_updated_date = CURRENT_DATE
				 , c504_last_updated_by = p_userid
			 WHERE c504_consignment_id = p_txn_id;
			 -- When void in control number enter screen , should be voided for consignment in T907 table.
			gm_pkg_common_cancel.gm_void_shipping (p_txn_id, p_userid, 50181);
		END IF;
		
		-- Update the lot status to NULL from CONTROLLED for Voided transactions
		gm_pkg_ship_rollback_txn.gm_sav_rollback_lot_status(v_out_cur,p_userid);		
		
		--To void the FGBin id corresponding to the transaction, only of Item consignment and FGLE
		--4110: Consignment; 50154: FGLE
		IF v_con_type = '4110' OR v_type = '50154'
		THEN
			gm_pkg_op_item_control_txn.gm_void_FGBinId(p_txn_id,p_userid);
		END IF;
		
	END gm_op_sav_void_inhousetxn;

	/*******************************************************
	 * Description : Procedure to rollback Loaner process (WIP,Pending Veri,Available->Pending Processing)
	 *******************************************************/
	PROCEDURE gm_cs_sav_rollback_loaner_pros (
		p_cons_id		 IN   t504a_consignment_loaner.c504_consignment_id%TYPE
	  , p_cancelreason	 IN   t907_cancel_log.c901_cancel_cd%TYPE
	  , p_canceltype	 IN   t901_code_lookup.c902_code_nm_alt%TYPE
	  , p_comments		 IN   t907_cancel_log.c907_comments%TYPE
	  , p_user_id		 IN   t504a_loaner_transaction.c504a_created_by%TYPE
	)
	AS
		CURSOR loaner_incident_cur (
			p_txn_id   IN	t504a_loaner_transaction.c504a_loaner_transaction_id%TYPE
		)
		IS
			SELECT t9200.c9200_incident_id incid, t9201.c9201_charge_details_id chrid
			  FROM t9200_incident t9200, t9201_charge_details t9201
			 WHERE t9200.c9200_incident_id = t9201.c9200_incident_id
			   AND c9200_ref_id = p_txn_id
			   AND c9200_ref_type = 4127
			   AND t9200.c9200_void_fl IS NULL
			   AND t9201.c9201_void_fl IS NULL;

		v_txn_id	   t504a_loaner_transaction.c504a_loaner_transaction_id%TYPE;
		v_rule_value   t906_rules.c906_rule_value%TYPE;
		v_return_dt    t504a_loaner_transaction.c504a_return_dt%TYPE;
		v_canceltype   t901_code_lookup.c902_code_nm_alt%TYPE;
		v_cancelreason t907_cancel_log.c901_cancel_cd%TYPE;
		v_status_fl    t504a_consignment_loaner.c504a_status_fl%TYPE;
		v_cnt			NUMBER := 0;
		v_rollback_flag	NUMBER;
		v_loaner_sales_id t412_inhouse_transactions.c412_inhouse_trans_id%TYPE;
	BEGIN

	-- Fetch the last returned transaction id and last return date 
	-- and current status of the loaner, based on status, decide whether this consignment can be rolled back
	BEGIN
	   SELECT t504a.c504a_status_fl 
	     INTO v_status_fl
         FROM t504a_consignment_loaner t504a 
        WHERE t504a.c504_consignment_id = p_cons_id;
     EXCEPTION WHEN NO_DATA_FOUND THEN
     	v_status_fl := -1;
     END;
        
	BEGIN			
	SELECT t504a.c504a_loaner_transaction_id, t504a.c504a_return_dt
	  INTO v_txn_id, v_return_dt 	
      FROM t504a_loaner_transaction t504a
     WHERE t504a.c504a_void_fl IS NULL
       AND c504a_loaner_transaction_id IN (SELECT MAX (TO_NUMBER (c504a_loaner_transaction_id))
                                             FROM t504a_loaner_transaction t504a
                                            WHERE t504a.c504_consignment_id = p_cons_id
                                              AND t504a.c504a_void_fl IS NULL);
	EXCEPTION WHEN NO_DATA_FOUND THEN
		v_txn_id := NULL;
		v_return_dt := NULL;
	END;
	
	BEGIN
			SELECT c412_inhouse_trans_id 
			INTO v_loaner_sales_id
			FROM t412_inhouse_transactions
			WHERE c504a_loaner_transaction_id=v_txn_id and c412_type='50159' and c412_void_fl IS NULL;
	EXCEPTION WHEN OTHERS THEN
			v_loaner_sales_id := NULL;
	END;

			IF v_status_fl <> '50' -- Pending Process
		   AND v_status_fl <> '55' -- Pending Picture		   
		   AND v_status_fl <> '30' -- WIP Loaners		   
		THEN
			--Sorry.Cannot Rollback this consignment
			raise_application_error (-20782, '');
		END IF;

		v_rule_value := get_rule_value ('ROLLBACKLOANER', 'ROLLBACK');

		IF TO_DATE (v_rule_value, 'MM/DD/YYYY') > v_return_dt
		THEN
			--Sorry.Cannot Rollback this consignment due to Return Date
			raise_application_error (-20783, '');
		END IF;
		
		-- Rollback Logic		
		v_rollback_flag := get_rollback_flag(p_cons_id, v_txn_id);
		
		
		IF v_rollback_flag = 2 THEN
			GM_RAISE_APPLICATION_ERROR('-20999','26','');
		END IF;
		
		IF v_rollback_flag = 0 THEN
			UPDATE t412_inhouse_transactions t412
			   SET c412_void_fl = 'Y'
				 , c412_last_updated_by = p_user_id
				 , c412_last_updated_date = CURRENT_DATE
			 WHERE t412.c412_ref_id = p_cons_id 
   			   AND t412.c504a_loaner_transaction_id = NVL(v_txn_id, t412.c504a_loaner_transaction_id)
   			   AND t412.c412_void_fl IS NULL;
   			   
	    IF v_loaner_sales_id IS NOT NULL THEN
   			gm_pkg_set_lot_track.gm_pkg_set_lot_update(v_loaner_sales_id,p_user_id,p_cons_id,'50519','6',NULL,'loaner_rollback');	   
	    END IF;
	  
	   END IF;
-- Loaner Update; '25' Pending Check
		gm_pkg_op_loaner_set_txn.gm_upd_consign_loaner_status(p_cons_id,'25',p_user_id);
		UPDATE t504a_consignment_loaner
		   SET c504a_available_date = NULL
			 , c504a_last_updated_by = p_user_id
			 , c504a_last_updated_date = CURRENT_DATE
		 WHERE c504_consignment_id = p_cons_id;

		 --When ever the child is updated, the consignment table has to be updated ,so ETL can Sync it to GOP.
	 	UPDATE t504_consignment
		   SET c504_last_updated_by    = p_user_id
	  		 , c504_last_updated_date = CURRENT_DATE
	  	 WHERE c504_consignment_id = p_cons_id;			 

		UPDATE t504a_loaner_transaction
		   SET c504a_rollback_fl = 'Y'                      -- PMT-46800 - Lot Track - Rollback loaner changes
		     , c504a_processed_date = NULL
			 , c504a_last_updated_by = p_user_id
			 , c504a_last_updated_date = CURRENT_DATE
		 WHERE c504a_loaner_transaction_id = v_txn_id;

-- Loaner Incident Update 
		FOR loaner_incident IN loaner_incident_cur (v_txn_id)
		LOOP
			UPDATE t9200_incident
			   SET c9200_void_fl = 'Y'
				 , c9200_updated_date = CURRENT_DATE
				 , c9200_updated_by = p_user_id
			 WHERE c9200_incident_id = loaner_incident.incid;

			UPDATE t9201_charge_details
			   SET c9201_void_fl = 'Y'
				 , c9201_updated_date = CURRENT_DATE
				 , c9201_updated_by = p_user_id
			 WHERE c9201_charge_details_id = loaner_incident.chrid;
		END LOOP;
	END gm_cs_sav_rollback_loaner_pros;

	PROCEDURE gm_cm_sav_cancelrow (
		p_txn_id		 IN   t907_cancel_log.c907_ref_id%TYPE
	  , p_cancelreason	 IN   t907_cancel_log.c901_cancel_cd%TYPE
	  , p_canceltype	 IN   t901_code_lookup.c902_code_nm_alt%TYPE
	  , p_comments		 IN   t907_cancel_log.c907_comments%TYPE
	  , p_userid		 IN   t102_user_login.c102_user_login_id%TYPE
	)
	AS
/***************************************************************************************
 * Description :This procedure is called to save information about a row which is cancelled (rollback / void / delete)
 *
 ****************************************************************************************/
		v_canceltypecode t907_cancel_log.c901_type%TYPE;
		v_cancelid	   NUMBER;
		v_company_id    t1900_company.c1900_company_id%TYPE;
	-- v_ref_type	   IN t4030_demand_growth_mapping.c4030_ref_id%TYPE
	BEGIN
		SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL')) INTO v_company_id FROM dual; 
		SELECT c901_code_id
		  INTO v_canceltypecode
		  FROM t901_code_lookup
		 WHERE c902_code_nm_alt = p_canceltype;

		--
		SELECT s907_cancel_log.NEXTVAL
		  INTO v_cancelid
		  FROM DUAL;

		--
		INSERT INTO t907_cancel_log
					(c907_cancel_log_id, c907_ref_id, c907_comments, c901_cancel_cd, c901_type, c907_created_by
				   , c907_created_date, c907_cancel_date, c1900_company_id
					)
			 VALUES (v_cancelid, p_txn_id, p_comments, p_cancelreason, v_canceltypecode, p_userid
				   , CURRENT_DATE, TRUNC (CURRENT_DATE), v_company_id
					);
	END gm_cm_sav_cancelrow;

/******************************************************************************************************
 * Description	:	This procedure is used for Loaner Rollback from Pending Puraway to Pending Picture
 * Author		:	Jignesh Shah
********************************************************************************************************/
	PROCEDURE gm_rollback_ln_pend_putaway (
		p_consignid		IN   t504a_consignment_loaner.c504_consignment_id%TYPE,
		p_userid		IN   t504_consignment.c504_created_by%TYPE
	)
	AS
		v_status_fl				t504a_consignment_loaner.c504a_status_fl%TYPE;
		v_allocate_fl			t504a_consignment_loaner.c504a_allocated_fl%TYPE;
		v_type					t504_consignment.c504_type%TYPE;
		v_status				t504a_consignment_loaner.c504a_status_fl%TYPE;
		v_hold_fl				t504_consignment.c504_hold_fl%TYPE;
		v_invpick_assign_id		t5050_invpick_assign_detail.c5050_invpick_assign_id%TYPE;
		v_company_id    		t1900_company.c1900_company_id%TYPE;
	BEGIN
		
		SELECT c504a_status_fl, c504a_allocated_fl
			INTO v_status_fl, v_allocate_fl
		FROM t504a_consignment_loaner
		WHERE c504_consignment_id = p_consignid
		AND c504a_void_fl IS NULL
		FOR UPDATE;

		IF v_status_fl != '58'
		THEN
			-- Error message is Sorry, this transaction is not in status pending putaway.
			GM_RAISE_APPLICATION_ERROR('-20999','21','');
		END IF;
      
		SELECT c504_type, c504_hold_fl , c1900_company_id
		  INTO v_type, v_hold_fl , v_company_id
		  FROM t504_consignment
		 WHERE c504_consignment_id = p_consignid
		   AND c504_void_fl IS NULL;
		
		SELECT DECODE(v_type, 4127, 55, 4119, 50) INTO v_status FROM DUAL; --(55 Pending Picture,  50 Pending Process)
		
		-- To skip pending pic process for OUS Countries.PMT-24984
		IF get_rule_value (v_company_id,'SKIP_PENDING_PICTURE')  = 'Y' THEN
			v_status := 25;
		END IF;
		-- Call the common procedure to update the status
		gm_pkg_op_loaner_set_txn.gm_upd_consign_loaner_status(p_consignid,v_status,p_userid);
		
		--When ever the child is updated, the consignment table has to be updated ,so ETL can Sync it to GOP.
		UPDATE t504_consignment
		SET c504_last_updated_by = p_userid
			, c504_last_updated_date = CURRENT_DATE
		WHERE c504_consignment_id = p_consignid;	

		IF (v_allocate_fl = 'Y')
		THEN
			-- if set is already allocated, need deallocated)
			gm_pkg_op_loaner.gm_sav_deallocate(p_consignid,p_userid);
		END IF;

		IF v_type = 4127	-- if product loaner then need mark it as complete in t5050 for device
		THEN	-- Void Records In t5050_invpick_assign_detail for removing allocation in device.
			gm_pkg_allocation.gm_inv_void_bytxn (p_consignid, 93604, p_userid);
		END IF;
		
	END gm_rollback_ln_pend_putaway;
END gm_pkg_common_cancel_op;
/
