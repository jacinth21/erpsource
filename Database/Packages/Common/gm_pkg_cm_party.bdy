/* Formatted on 2010/06/09 18:10 (Formatter Plus v4.8.0) */
-- @"C:\database\Packages\Common\gm_pkg_cm_party.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_cm_party
IS
--
 /******************************************************************
   * Description : Procedure to save / update party details
   ****************************************************************/
	PROCEDURE gm_cm_sav_party_id (
		p_partyid	  IN	   t101_party.c101_party_id%TYPE
	  , p_firstname   IN	   t101_party.c101_first_nm%TYPE
	  , p_lastname	  IN	   t101_party.c101_last_nm%TYPE
	  , p_middleinitial IN t101_party.c101_middle_initial%TYPE
	  , p_name		  IN	   t101_party.c101_party_nm%TYPE
	  , p_type		  IN	   t101_party.c901_party_type%TYPE
	  , p_activefl	  IN	   t101_party.c101_active_fl%TYPE
	  , p_userid	  IN	   t101_party.c101_created_by%TYPE
	  , p_party_id	  OUT	   NUMBER
	)
	AS
	v_portal_comp_id t101_party.c1900_company_id%TYPE;
	BEGIN
		p_party_id	:= p_partyid;
		--NVL Added Mihir PMT 8227, once all the changes for company are done remove the NVL, because the context will always have company.
		SELECT NVL(get_compid_frm_cntx (),1000) INTO v_portal_comp_id FROM DUAL;
		UPDATE t101_party
		   SET c101_first_nm = p_firstname
			 , c101_last_nm = p_lastname
			 , c101_middle_initial = p_middleinitial
			 , c101_party_nm = p_name
			 , c101_active_fl = p_activefl
			 , c1900_company_id = v_portal_comp_id
			 , c101_last_updated_by = p_userid
			 , c101_last_updated_date = CURRENT_DATE
		 WHERE c101_party_id = DECODE(p_partyid,0,NULL,p_partyid) ;
		 -- Decode added as part of clinical issue which added 0 party id
		 
		IF (SQL%ROWCOUNT = 0)
		THEN
			IF p_party_id IS NULL
			THEN
				SELECT s101_party.NEXTVAL
				  INTO p_party_id
				  FROM DUAL;
			END IF;
			

			INSERT INTO t101_party
						(c101_party_id, c101_last_nm, c101_first_nm,c101_middle_initial, c901_party_type, c101_party_nm, c101_created_by
					   , c101_created_date,c101_active_fl, c1900_company_id
						)
				 VALUES (p_party_id, p_lastname, p_firstname,p_middleinitial, p_type, p_name, p_userid
					   , CURRENT_DATE, p_activefl, v_portal_comp_id
						);
		END IF;
	END gm_cm_sav_party_id;

   /*******************************************************
	 * Description : Procedure to get Party Rule Parameter
	 * Author	   : Angela Xiang
	 *******************************************************/
--
	PROCEDURE gm_cm_fch_partyruleparam (
		p_type		IN		 t901_code_lookup.c901_code_grp%TYPE
	  , p_partyid	IN		 t906_rules.c906_rule_desc%TYPE
	  , p_outdata	OUT 	 TYPES.cursor_type
	)
	AS
		v_type		   t901_code_lookup.c901_code_grp%TYPE;
		v_partyid	   t906_rules.c906_rule_desc%TYPE;
	BEGIN
		OPEN p_outdata
		 FOR
			 SELECT t901.c901_code_nm, t906.c906_rule_value
			   FROM t901_code_lookup t901, t906_rules t906
			  WHERE t901.c901_code_grp = p_type AND t901.c901_code_id || '' = t906.c906_rule_id(+) AND t906.c906_rule_grp_id(+) =
																											  p_partyid;
	END gm_cm_fch_partyruleparam;
	/*******************************************************
	  * Description : This Procedure is used for web service to get Party Details for party id 
	  * which is of type 7000(Surgeon) 
	  * Author	   : Manikandan Rajasekaran
	 *******************************************************/
	PROCEDURE gm_fch_all_parties(
		p_token  			IN 		t101a_user_token.c101a_token_id%TYPE,
		p_langid			IN		NUMBER,
		p_page_no			IN		NUMBER,
	    p_uuid		        IN      t9150_device_info.c9150_device_uuid%TYPE,
		p_partylist_cur		OUT		TYPES.CURSOR_TYPE
	) 
	AS
		v_deviceid  t9150_device_info.c9150_device_info_id%TYPE;
		v_count	    NUMBER;
   		v_last_rec	t9151_device_sync_dtl.C9151_LAST_SYNC_REC%TYPE; 		
	   
	 BEGIN
		
				--get the device info id based on user token
		v_deviceid := get_device_id(p_token, p_uuid);
		
		/*below prc check the device's last updated date for Rules reftype and
		if record exist then it checks is there any Party id  updated after the device updated date.
		if updated groups available then add all Party Id into v_inlist and return count of v_inlist records*/
		
		gm_pkg_pdpc_prodcatrpt.gm_fch_device_updates(v_deviceid,p_langid,4000445,p_page_no,v_count,v_last_rec); -- 4000445 - Party reftype
		
		IF v_count = 0 THEN
			gm_pkg_pdpc_prodcatrpt.gm_fch_all_parites_tosync;
		END IF;
		gm_fch_all_parties_dtl(p_page_no,v_last_rec,p_partylist_cur);
	END gm_fch_all_parties ;
	
	/*******************************************************
	 * Description : This Procedure is used for web service to get Party Details for party id 
	 * which is of type 7000(Surgeon) 
	 * Author	   : Manikandan Rajasekaran
	 *******************************************************/
	PROCEDURE gm_fch_all_parties_dtl(
		p_pageno           IN     NUMBER,
		p_last_rec		   IN 	  t9151_device_sync_dtl.C9151_LAST_SYNC_REC%TYPE,
		p_partylist_dtl_cur  OUT 	  TYPES.cursor_type
    )
    AS
    	v_page_no   NUMBER;
		v_page_size NUMBER;
	  	v_start     NUMBER;
	    v_end       NUMBER;
    BEGIN
   
		v_page_no       := p_pageno;
		-- get the paging details
		gm_pkg_pdpc_prodcatrpt.gm_fch_paging_dtl('PARTY','PAGESIZE',v_page_no,v_start,v_end,v_page_size);
		OPEN p_partylist_dtl_cur 
		FOR 
		SELECT COUNT (1) OVER () pagesize, result_count totalsize, v_page_no pageno, rwnum, CEIL (result_count / v_page_size) totalpages , t101.*
			 	FROM(
					SELECT t101.*, ROWNUM rwnum, COUNT (1) OVER () result_count,MAX(DECODE(t101.partyid,p_last_rec,ROWNUM,NULL)) OVER() LASTREC
			    		FROM(
							SELECT t101.c101_party_id partyid, t101.c101_first_nm firstnm, t101.c101_last_nm lastnm ,t101.c101_party_nm partynm,
								   t101.c901_party_type partytype,t101.c101_delete_fl deletefl ,t101.c101_active_fl activefl,  
								   t101.c101_middle_initial midinitial , NVL(temp.void_fl,t101.c101_void_fl) voidfl
						    FROM   t101_party t101,my_temp_prod_cat_info temp 
							WHERE  t101.c101_party_id  = temp.ref_id
							ORDER BY t101.c101_party_id
			            )t101
			    )t101
			  WHERE RWNUM BETWEEN NVL(LASTREC+1,DECODE(p_last_rec,NULL,v_start,v_start-1)) AND v_end;
	END gm_fch_all_parties_dtl;
	
   /*******************************************************
	  * Description : Procedure to fetch the Party Ship Paramater details
	  * Author	    : HReddi
	 *******************************************************/
    PROCEDURE gm_fch_party_params(
		p_acc_id	      IN     t704_account.c704_account_id%TYPE,
		p_party_id	      IN  	 t101_party.c101_party_id%TYPE,
		p_params_info	  OUT 	 TYPES.cursor_type
    )AS
    BEGIN	 
	    	IF p_acc_id IS NOT NULL
	    	THEN
	    	 	OPEN p_params_info FOR
				    SELECT t9080.c9080_third_party thirdParty
					     , t9080.c901_ship_carrier shipCarrier
					     , t9080.c901_ship_mode shipMode
					     , t9080.c901_standard_ship_payee stdShipPayee
					     , t9080.c901_standard_ship_charge_type stdShipChargeType
					     , t9080.c9080_standard_ship_value_pct stdShipValue
					     , t9080.c901_rush_ship_payee rushShipPayee
					     , t9080.c901_rush_ship_charge_type rushShipChargeType
					     , t9080.c9080_rush_ship_value_pct rushShipValue
					     , t704.c704_account_id accountId, DECODE(GET_ACCOUNT_GPO_ID(p_acc_id), 0, 'N', 'Y') gpofl
					     , t9080.C9080_SHIPMENT_EMAIL shipmentEmail
					 FROM T9080_SHIP_PARTY_PARAM T9080 , t704_account t704
					WHERE t9080.c101_party_id = t704.c101_party_id
					 AND t9080.c101_party_id = NVL(p_party_id,'-999')
					AND t704.c704_account_id = NVL(p_acc_id, '-999')
					AND t9080.c9080_void_fl IS NULL
					AND t704.c704_void_fl IS NULL;			
			ELSE
				 OPEN p_params_info FOR
					SELECT t9080.c9080_third_party thirdParty
					     , t9080.c901_ship_carrier shipCarrier
					     , t9080.c901_ship_mode shipMode
					     , t9080.c901_standard_ship_payee stdShipPayee
					     , t9080.c901_standard_ship_charge_type stdShipChargeType
					     , t9080.c9080_standard_ship_value_pct stdShipValue
					     , t9080.c901_rush_ship_payee rushShipPayee
					     , t9080.c901_rush_ship_charge_type rushShipChargeType
					     , t9080.c9080_rush_ship_value_pct rushShipValue
					     , '' accountId, 'N' gpofl
					     , t9080.C9080_SHIPMENT_EMAIL shipmentEmail
					 FROM T9080_SHIP_PARTY_PARAM T9080
					WHERE t9080.c101_party_id = NVL(p_party_id,'-999') 
					AND t9080.c9080_void_fl IS NULL;
			END IF;
			
		END gm_fch_party_params;
		
	/*******************************************************
	  * Description : Procedure to Save the Party Ship Paramater details
	  * Author	    : HReddi
	 *******************************************************/
    PROCEDURE gm_sav_party_params(
		p_third_party	  IN    T9080_SHIP_PARTY_PARAM.c9080_third_party%TYPE,
		p_ship_carrier    IN  	T9080_SHIP_PARTY_PARAM.c901_ship_carrier%TYPE,
		p_ship_mode       IN 	T9080_SHIP_PARTY_PARAM.c901_ship_mode%TYPE,
		p_std_ship_pay	  IN  	T9080_SHIP_PARTY_PARAM.c901_standard_ship_payee%TYPE,
		p_std_chrg_type   IN  	T9080_SHIP_PARTY_PARAM.c901_standard_ship_charge_type%TYPE,
		p_std_pcnt_val    IN  	T9080_SHIP_PARTY_PARAM.c9080_standard_ship_value_pct%TYPE,
		p_rush_ship_pay   IN  	T9080_SHIP_PARTY_PARAM.c901_rush_ship_payee%TYPE,
		p_rush_chrg_type  IN  	T9080_SHIP_PARTY_PARAM.c901_rush_ship_charge_type%TYPE,
		p_rush_pcnt_val   IN  	T9080_SHIP_PARTY_PARAM.c9080_rush_ship_value_pct%TYPE,
		p_account_id      IN  	t704_account.c704_account_id%TYPE,
		p_party_id	      IN  	t101_party.c101_party_id%TYPE,
		p_user_id         IN  	T9080_SHIP_PARTY_PARAM.C9080_LAST_UPDATED_BY%TYPE,
		p_shipment_email  IN  	T9080_SHIP_PARTY_PARAM.C9080_SHIPMENT_EMAIL%TYPE
    )AS
    	v_party_id        		    T9080_SHIP_PARTY_PARAM.C101_PARTY_ID%TYPE;
    	v_party_param_id            T9080_SHIP_PARTY_PARAM.C9080_SHIP_PARAM_ID%TYPE;
    BEGIN
		
	    IF p_party_id IS NULL THEN
		    SELECT c101_party_id 
		      INTO v_party_id 
		      FROM t704_account 
		     WHERE c704_account_id = p_account_id 
		       AND c704_void_fl IS NULL;
	    ELSE
	    	v_party_id:= p_party_id;
		END IF;	    
	   
	    UPDATE T9080_SHIP_PARTY_PARAM
		   SET c9080_third_party = p_third_party
		     , c901_ship_carrier = DECODE(p_ship_carrier, '0', NULL,p_ship_carrier)
		     , c901_ship_mode = DECODE(p_ship_mode, '0', NULL,p_ship_mode)
		     , c901_standard_ship_payee = DECODE(p_std_ship_pay, '0', NULL,p_std_ship_pay)
		     , c901_standard_ship_charge_type = DECODE(p_std_chrg_type, '0', NULL,p_std_chrg_type)
		     , c9080_standard_ship_value_pct = p_std_pcnt_val
		     , c901_rush_ship_payee =  DECODE(p_rush_ship_pay, '0', NULL,p_rush_ship_pay)
		     , c901_rush_ship_charge_type = DECODE(p_rush_chrg_type, '0', NULL,p_rush_chrg_type)
		     , c9080_rush_ship_value_pct  = p_rush_pcnt_val
		     , C9080_LAST_UPDATED_BY  = p_user_id
		     , C9080_LAST_UPDATED_DATE = CURRENT_DATE
		     , C9080_SHIPMENT_EMAIL = NVL(p_shipment_email,C9080_SHIPMENT_EMAIL)
		 WHERE c101_party_id = v_party_id
		   AND c9080_void_fl IS NULL;
	    	
		    IF (SQL%ROWCOUNT = 0)
			THEN     
		    	INSERT INTO T9080_SHIP_PARTY_PARAM(
		    			C9080_SHIP_PARAM_ID,C101_PARTY_ID,C901_SHIP_CARRIER,C901_SHIP_MODE,C901_STANDARD_SHIP_PAYEE
		    		  , C901_STANDARD_SHIP_CHARGE_TYPE,C9080_STANDARD_SHIP_VALUE_PCT,C901_RUSH_SHIP_PAYEE,C901_RUSH_SHIP_CHARGE_TYPE
		    		  , C9080_RUSH_SHIP_VALUE_PCT,C9080_LAST_UPDATED_BY,C9080_LAST_UPDATED_DATE,c9080_third_party,C9080_SHIPMENT_EMAIL) 
	 				 VALUES (s9080_ship_party_param.nextval,v_party_id,DECODE(p_ship_carrier, '0', NULL,p_ship_carrier),DECODE(p_ship_mode, '0', NULL,p_ship_mode),DECODE(p_std_ship_pay, '0', NULL,p_std_ship_pay)
	 				        ,DECODE(p_std_chrg_type, '0', NULL,p_std_chrg_type),p_std_pcnt_val,DECODE(p_rush_ship_pay, '0', NULL,p_rush_ship_pay),DECODE(p_rush_chrg_type, '0', NULL,p_rush_chrg_type),p_rush_pcnt_val,p_user_id,CURRENT_DATE,p_third_party,p_shipment_email);
		    END IF; 
	END gm_sav_party_params;
	/*******************************************************
	  * Description : Procedure to save the party to company mapping
	  * Author	    : rshah
	 *******************************************************/
    PROCEDURE gm_cm_sav_partycompany(
		p_compid	      IN      t1900_company.C1900_COMPANY_ID%TYPE,
		p_type	  		  IN 	  t901_code_lookup.C901_CODE_ID%TYPE,
		p_partyid	      IN  	  t101_party.c101_party_id%TYPE,
		p_userid	      IN  	  t101_user.c101_user_id%TYPE
    )AS
    BEGIN
    	UPDATE T1019_PARTY_COMPANY_MAP
		   SET c1900_company_id = p_compid
		     , c1019_void_fl = NULL 
		     , c1019_last_updated_by  = p_userid
		     , c1019_last_updated_date = CURRENT_DATE
		 WHERE c101_party_id = p_partyid
		   AND c901_party_mapping_type = p_type; 

		IF (SQL%ROWCOUNT = 0)
			THEN     
		    	INSERT INTO T1019_PARTY_COMPANY_MAP(
		    			c1019_party_company_map_id, c101_party_id,c1900_company_id,c901_party_mapping_type, C1019_LAST_UPDATED_BY, C1019_LAST_UPDATED_DATE) 
	 				 VALUES (s1019_PARTY_COMPANY_MAP.nextval, p_partyid, p_compid, p_type,p_userid, CURRENT_DATE);
		    END IF; 
		/*When primary company is updated, it should void that company if it is in associated company.*/
		
		update t1019_party_company_map 
	       set c1019_void_fl = 'Y', 
	           c1019_last_updated_date = CURRENT_DATE, 
	           c1019_last_updated_by = p_userid 
	     where c101_party_id = p_partyid 
	       and c1900_company_id = p_compid
	       and c901_party_mapping_type = 105401; -- Non primary.
		
		/* When the default company is updated, the security group mapping should be voided for that company also added during changing associated company*/
		gm_upd_group_mapping(p_partyid,p_userid);
		
    END gm_cm_sav_partycompany;
    
    /*******************************************************
	  * Description : Procedure to fetch the non Asscociate company to the Mapping screen.
	  * Author	    : rshah
	 *******************************************************/
    PROCEDURE gm_cm_fch_nonasstcompany(
		
	   p_partyid          IN  	  t101_party.c101_party_id%TYPE		
	  , p_companies  	  OUT 	  TYPES.cursor_type
	  
    )
    AS    
    BEGIN
    	OPEN p_companies FOR  
    	SELECT c1900_company_id companyId, c1900_company_name companynm 
          FROM t1900_company 
         WHERE c1900_company_id IN 
                   ( SELECT c1900_company_id FROM t1900_company WHERE c1900_void_fl IS NULL 
                      MINUS 
        SELECT c1900_company_id 
          FROM T1019_PARTY_COMPANY_MAP 
         WHERE c101_party_id = p_partyid 
           AND c1019_void_fl IS NULL 
                   ) 
           AND c1900_void_fl IS NULL;
    	   
    END gm_cm_fch_nonasstcompany;
        /*******************************************************
	  * Description : Procedure to fetch the company in Header section
	  * Author	    : rshah
	 *******************************************************/
    PROCEDURE gm_cm_fch_partycompany (
		p_partyid	      IN  	  t101_party.c101_party_id%TYPE		
	  , p_type	  		  IN 	  t901_code_lookup.C901_CODE_ID%TYPE
	  , p_companies  	  OUT 	  TYPES.cursor_type
    )
    AS
    v_compid NUMBER;
    BEGIN
	    OPEN p_companies FOR  
	    SELECT t1900.C1900_COMPANY_ID COMPANYID,
	           t1900.C1900_COMPANY_NAME COMPANYNM,
	           get_code_name_alt(C901_DATE_FORMAT) CMPJSFMT
	      FROM t1900_company t1900,
	           t1019_party_company_map t1019
	     WHERE t1019.c1900_company_id      = t1900.c1900_company_id
	       AND t1019.C101_PARTY_ID           = p_partyid
	       AND t1019.c901_party_mapping_type = NVL(p_type, t1019.c901_party_mapping_type)
	       AND c1019_void_fl                IS NULL 
	       ORDER BY NVL(T1900.C1900_COMPANY_SEQ_NO,999), t1900.C1900_COMPANY_NAME;--PMT-31806 Changes to the Company dropdown spineIT
   END gm_cm_fch_partycompany;
   
     /*******************************************************
	  * Description : Procedure to save the Associate company for the user
	  * Author	    : Manoj
	 *******************************************************/
    PROCEDURE gm_cm_sav_asstcompany (
		p_asscompanies	  IN      VARCHAR,
		p_type	  		  IN 	  t901_code_lookup.C901_CODE_ID%TYPE,
		p_partyid	      IN  	  t101_party.c101_party_id%TYPE,
		p_userid	      IN  	  t101_user.c101_user_id%TYPE
    )
    AS
    v_strlen    NUMBER          := NVL (LENGTH (p_asscompanies), 0) ;
    v_string    VARCHAR2 (4000) := p_asscompanies;
    v_substring VARCHAR2 (1000) ;
    v_assigned_to  NUMBER := 0; 
    v_cnt number;
    BEGIN

	update t1019_party_company_map 
       set c1019_void_fl = 'Y', 
         c1019_last_updated_date = CURRENT_DATE, 
         c1019_last_updated_by = p_userid 
   where c101_party_id = p_partyid 
   and  c901_party_mapping_type = p_type;
    
   WHILE INSTR (v_string, ',') <> 0
        LOOP
            v_substring := SUBSTR (v_string, 1, INSTR (v_string, ',') - 1) ;
            v_string    := SUBSTR (v_string, INSTR (v_string, ',')    + 1) ;
            v_assigned_to     := TO_NUMBER (v_substring) ;
            /*For any company if user is not mapped to security group it will throw exception as below.*/	
			SELECT COUNT(1)
			  INTO v_cnt
		      FROM t1501_group_mapping t1501
		     WHERE t1501.c101_mapped_party_id = p_partyid
		       AND t1501.c1900_company_id = v_assigned_to
		       AND t1501.c1501_void_fl IS NULL
		       AND t1501.c1500_group_id IS NOT NULL;
		    IF v_cnt = 0 THEN
		    	raise_application_error ('-20671','');
		    END IF;   
		    
		   gm_sav_party_company_mapping(p_partyid, v_assigned_to, p_type, p_userid);
	        
      END LOOP;
      /* When the default company is updated, the security group mapping should be voided for that company also added during changing associated company*/
	 gm_upd_group_mapping(p_partyid,p_userid);  
   END gm_cm_sav_asstcompany;
   
	/*******************************************************
	  * Description : fetch the non associated companies and update the void flag for 
	  that security group and company.
	  * Author	    : rshah
	 *******************************************************/
    PROCEDURE gm_upd_group_mapping(
	   p_partyid          IN  	  t101_party.c101_party_id%TYPE,
	   p_userid	          IN  	  t101_user.c101_user_id%TYPE	  
    )
    AS
    BEGIN
    	UPDATE t1501_group_mapping
		   SET c1501_void_fl = 'Y',
		       c1501_last_updated_by = p_userid,
               c1501_last_updated_date = current_date
		 WHERE c101_mapped_party_id = p_partyid
		   AND c1900_company_id IN
		        (SELECT c1900_company_id companyId
		           FROM t1900_company
		          WHERE c1900_company_id IN
		                ( SELECT c1900_company_id 
		                    FROM t1900_company 
		                   WHERE c1900_void_fl IS NULL
		                   MINUS
		                  SELECT c1900_company_id
		                    FROM T1019_PARTY_COMPANY_MAP
		                   WHERE c101_party_id = p_partyid
		                     AND c1019_void_fl IS NULL
		                )
		                AND c1900_void_fl IS NULL
		        );
    END gm_upd_group_mapping;
	 /*******************************************************
	  * Description : Procedure to save party and company mapping.
	  * Author	    : Manoj
	 *******************************************************/
    PROCEDURE gm_sav_party_company_mapping (
		p_partyid	      IN  	  t101_party.c101_party_id%TYPE
	  , p_compid		  IN	   t1900_company.C1900_COMPANY_ID%TYPE		
	  , p_type	  		  IN 	  t901_code_lookup.C901_CODE_ID%TYPE
	  , p_userid	      IN  	  t101_user.c101_user_id%TYPE
    )
    AS
    BEGIN
    	UPDATE T1019_PARTY_COMPANY_MAP
		  SET  c1019_void_fl = NULL 
		     , c1019_last_updated_by  = p_userid
		     , c1019_last_updated_date = CURRENT_DATE
		 WHERE c101_party_id = p_partyid
		   AND c1900_company_id = p_compid
		   AND c901_party_mapping_type = p_type;
		    
		 IF (SQL%ROWCOUNT = 0)
			THEN				     
		    	INSERT INTO T1019_PARTY_COMPANY_MAP(
		    			c1019_party_company_map_id, c101_party_id,c1900_company_id,c901_party_mapping_type, C1019_LAST_UPDATED_BY, C1019_LAST_UPDATED_DATE) 
	 				 VALUES (s1019_PARTY_COMPANY_MAP.nextval, p_partyid, p_compid, p_type, p_userid,CURRENT_DATE);	 				 
	     END IF;   
    END gm_sav_party_company_mapping;
    
 /*******************************************************
	* Description : Procedure used to save the party plant mapping
	*  				PMT-34024 - Party plant mapping (Code modified)  
	* 
	* Author     :
	*******************************************************/
	PROCEDURE gm_sav_party_plant_mapping (
	        p_partyid  IN t101_party.c101_party_id%TYPE,
	        p_compid   IN t1900_company.C1900_COMPANY_ID%TYPE,
	        p_plant_id IN t5040_plant_master.c5040_plant_id%TYPE,
	        p_userid   IN t101_user.c101_user_id%TYPE)
	AS
	BEGIN
	    -- to update the party plant map
	    
	     UPDATE t5042_plant_company_party_map
	    SET c5042_void_fl        = NULL, c5042_last_updated_by = p_userid, c5042_last_updated_date = CURRENT_DATE
	      WHERE c101_party_id    = p_partyid
	        AND c1900_company_id = p_compid
	        AND c5040_plant_id   = p_plant_id;
	        
	    -- no record update , then insert the new record
	    IF (SQL%ROWCOUNT = 0) THEN
	         INSERT
	           INTO t5042_plant_company_party_map
	            (
	                c5042_plant_company_party_map_id, c101_party_id, c1900_company_id
	              , c5040_plant_id, c5042_last_updated_by, c5042_last_updated_date
	            )
	            VALUES
	            (
	                s5042_plant_company_party_map.nextval, p_partyid, p_compid
	              , p_plant_id, p_userid, CURRENT_DATE
	            ) ;
	    END IF;
	    
	END gm_sav_party_plant_mapping;
	/*******************************************************
	* Description : Procedure used to void the party plant mapping details
	*  				PMT-34024 - Party plant mapping (Code modified)
	* 
	* Author     :
	*******************************************************/
	PROCEDURE gm_void_party_plant_mapping
	    (
	        p_partyid  IN t101_party.c101_party_id%TYPE,
	        p_compid   IN t1900_company.C1900_COMPANY_ID%TYPE,
	        p_plant_id IN t5040_plant_master.c5040_plant_id%TYPE,
	        p_userid   IN t101_user.c101_user_id%TYPE
	    )
	AS
	
	BEGIN
	     UPDATE t5042_plant_company_party_map
	    SET c5042_void_fl        = 'Y', c5042_last_updated_by = p_userid, c5042_last_updated_date = CURRENT_DATE
	      WHERE c101_party_id    = p_partyid
	        AND c1900_company_id = p_compid
	        AND c5040_plant_id   = p_plant_id;
	        
	END gm_void_party_plant_mapping; 
	    
END gm_pkg_cm_party;
/
