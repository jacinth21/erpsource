/* Formatted on 2010/05/19 11:42 (Formatter Plus v4.8.0) */
-- @"C:\Database\Packages\Common\gm_pkg_cm_rule_engine.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_cm_rule_engine
IS
/******************************************************************
 * Description : This procedure is used to fetch the
 *				 rules for the given conditions and
 *				 transaction.
 * Author	: Vprasath
 ******************************************************************/
	PROCEDURE gm_fch_available_rules (
		p_param_str   IN	   VARCHAR2
	  , p_rule_ids	  IN	   VARCHAR2
	  , p_txn_id	  IN	   NUMBER
	  , p_level 	  IN	   NUMBER
	  , p_out_cur	  OUT	   TYPES.cursor_type
	)
	AS
		CURSOR c_con_cur
		IS
			SELECT token condition, tokenii VALUE
			  FROM v_double_in_list v_list
			 WHERE token IS NOT NULL;

		v_condition_id VARCHAR2 (100);
		v_condition_cnt NUMBER := 0;
		v_str		   VARCHAR2 (32767);
		v_condition_join_param VARCHAR2 (10);
		v_operator_join_param VARCHAR2 (10);
		v_val		   VARCHAR2 (1000);
		v_val_temp	   VARCHAR2 (1000);
		v_operator_count NUMBER := 0;
		v_rule_id	   VARCHAR2 (20);
		v_rule_ids	   VARCHAR2 (32767); -- Changed the size from 4000 to 32767. PMT-38476-unable-to-process-orders
		v_rule_cur	   TYPES.cursor_type;
		v_opr_cur	   TYPES.cursor_type;
		v_operator_id  VARCHAR2 (100);
		v_open_braces  VARCHAR2 (1);
		v_close_braces VARCHAR2 (1);
		v_temp  NUMBER; -- Created the new local variable to store the rule engine condition id (PMT-323)
		v_company_id   T1900_company.c1900_company_id%TYPE;
	BEGIN
		my_context.set_double_inlist_ctx (p_param_str);
		my_context.set_my_inlist_ctx (p_rule_ids);
		v_str		:= ' select DISTINCT c9700_rule_id RULE_ID ';
		v_str		:= v_str || ' from t9702_rule_condition t9702 ';
		v_str		:= v_str || ' where (';
		v_close_braces := ')';

		SELECT get_compid_frm_cntx()  INTO v_company_id   FROM DUAL; 

		FOR c_cur_index IN c_con_cur
		LOOP
			v_condition_cnt := v_condition_cnt + 1;
			v_condition_join_param := '(';

			IF v_condition_cnt <> 1
			THEN
				v_condition_join_param := ' OR (';
				v_open_braces := '';
			END IF;

			-- Fetch the rule engine condition id into v_temp local variable, this change to avoid the direct (PMT-323)
			-- use of gm_pkg_cm_rule_engine.get_condition_id in str:= append (PMT-323)
			select   gm_pkg_cm_rule_engine.get_condition_id (c_cur_index.condition) into v_temp from dual;

			-- appended the v_temp in str:= by remove the existing direct call of the function gm_pkg_cm_rule_engine.get_condition_id (PMT-323)
			v_str		:=
				   v_str
				|| v_condition_join_param
				|| v_open_braces
				|| ' t9702.c9701_condition_id =' || '''' || v_temp || '''';  
			--	|| c_cur_index.condition
			--	|| ''')';
			v_condition_id := c_cur_index.condition;
			v_val		:= c_cur_index.VALUE;

			OPEN v_opr_cur
			 FOR
				 SELECT get_code_name (t9002.c901_operator_id) opr
				   FROM t9001_filter_lookup t9001, t9002_filter_operator t9002, t9701_condition t9701
				  WHERE t9701.c9701_condition_name = v_condition_id
					AND t9001.c9001_filter_id = t9002.c9001_filter_id
					AND t9001.c9001_ref_id = t9701.c9701_condition_id
					AND t9001.c9001_filter_grp = 'CM_RULE';

			v_operator_join_param := 'AND ( ';

			LOOP
				FETCH v_opr_cur
				 INTO v_operator_id;

				EXIT WHEN v_opr_cur%NOTFOUND;

				IF v_operator_id = 'LIKE'
				THEN
					v_val_temp	:= '%' || SUBSTR (v_val, 0, 3) || '%';
					v_str		:=
						   v_str
						|| v_operator_join_param
						|| ' t9702.c9702_condition_value '
						|| v_operator_id
						|| ''''
						|| v_val_temp
						|| '''';
					v_operator_join_param := ' OR ';
					v_val_temp	:= '%' || SUBSTR (v_val, 0, 4) || '%';
					v_str		:=
						   v_str
						|| v_operator_join_param
						|| ' t9702.c9702_condition_value '
						|| v_operator_id
						|| ''''
						|| v_val_temp
						|| '''';
			-- If the part number length is less than 7 below string formation is not needed, so added a length condition check. PMT-38476-unable-to-process-orders
				IF (LENGTH(v_val) >=7) THEN 
					v_val_temp	:= '%' || SUBSTR (v_val, 0, 5) || '%';
					v_str		:=
						   v_str
						|| v_operator_join_param
						|| ' t9702.c9702_condition_value '
						|| v_operator_id
						|| ''''
						|| v_val_temp
						|| '''';
					v_val_temp	:= '%' || SUBSTR (v_val, 0, 6) || '%';
					v_str		:=
						   v_str
						|| v_operator_join_param
						|| ' t9702.c9702_condition_value '
						|| v_operator_id
						|| ''''
						|| v_val_temp
						|| '''';
					v_val_temp	:= '%' || SUBSTR (v_val, 0, 7) || '%';
					v_str		:=
						   v_str
						|| v_operator_join_param
						|| ' t9702.c9702_condition_value '
						|| v_operator_id
						|| ''''
						|| v_val_temp
						|| '''';
					v_val_temp	:= '%' || SUBSTR (v_val, 3, 7) || '%';
					v_str		:=
						   v_str
						|| v_operator_join_param
						|| ' t9702.c9702_condition_value '
						|| v_operator_id
						|| ''''
						|| v_val_temp
						|| '''';
					v_val_temp	:= '%' || SUBSTR (v_val, 4, 7) || '%';
					v_str		:=
						   v_str
						|| v_operator_join_param
						|| ' t9702.c9702_condition_value '
						|| v_operator_id
						|| ''''
						|| v_val_temp
						|| '''';
					v_val_temp	:= '%' || SUBSTR (v_val, 5, 7) || '%';
					v_str		:=
						   v_str
						|| v_operator_join_param
						|| ' t9702.c9702_condition_value '
						|| v_operator_id
						|| ''''
						|| v_val_temp
						|| '''';
					END IF;
				ELSIF v_operator_id = 'IN'
				THEN
					v_val_temp	:= '%' || v_val || '%';
				ELSE
					v_val_temp	:= v_val;
				END IF;

				v_str		:=
					   v_str
					|| v_operator_join_param
					|| ' t9702.c9702_condition_value '
					|| v_operator_id
					|| ''''
					|| v_val_temp
					|| '''';
				v_operator_join_param := ' OR ';
			END LOOP;

			v_str		:= v_str || v_close_braces || v_close_braces;
		END LOOP;

		v_str		:= v_str || ')';

		IF p_rule_ids IS NOT NULL OR p_level <> 1
		THEN
			v_str		:= v_str || ' AND c9700_rule_id in (select token from v_in_list)';
		END IF;

		OPEN v_rule_cur
		 FOR v_str;

		LOOP
			FETCH v_rule_cur
			 INTO v_rule_id;

			EXIT WHEN v_rule_cur%NOTFOUND;
			v_rule_ids	:= v_rule_ids || v_rule_id || ',';
		END LOOP;

		CLOSE v_rule_cur;

		my_context.set_my_inlist_ctx (v_rule_ids);

		OPEN p_out_cur
		 FOR
			 SELECT   t9700.c9700_rule_id ruleid
					, gm_pkg_cm_rule_engine.gm_fch_rule_conditions (t9700.c9700_rule_id) conditions
					, t9700.c9700_rule_level rulelevel
				 FROM t9700_rule t9700, t9703_rule_transaction t9703
				WHERE t9700.c9700_rule_id = t9703.c9700_rule_id
				  --AND t9703.c901_txn_id = 50900
				  AND t9700.c9700_rule_id IN (SELECT token
												FROM v_in_list)
				  AND t9700.c9700_void_fl IS NULL
				  AND t9700.c9700_active_fl = 'Y'
				  AND NVL (t9700.c9700_expiry_date, TRUNC (SYSDATE)) >= TRUNC (SYSDATE)
				  AND t9703.c9703_void_fl IS NULL
				  AND t9703.c901_txn_id = p_txn_id
				  AND t9700.c1900_company_id = v_company_id
			 ORDER BY rulelevel;
	END gm_fch_available_rules;

--
/******************************************************************
 * Description : Get the maximum level of condition
 *				 for the given transaction
 * Author	: Vprasath
 ******************************************************************/
	PROCEDURE gm_fch_max_param_level (
		p_txn_id	  IN	   t901_code_lookup.c901_code_id%TYPE
	  , p_out_level   OUT	   VARCHAR2
	)
	AS
	BEGIN
		SELECT NVL (MAX (t9701.c9701_condition_level), 0)
		  INTO p_out_level
		  FROM t9703_rule_transaction t9703, t9702_rule_condition t9702, t9701_condition t9701
		 WHERE t9703.c901_txn_id = p_txn_id
		   AND t9703.c9700_rule_id = t9702.c9700_rule_id
		   AND t9702.c9701_condition_id = t9701.c9701_condition_id;
	END gm_fch_max_param_level;

--
/******************************************************************
 * Description : Procedure to fetch all the conditions
 * Author	: Vprasath
 ******************************************************************/
--
	PROCEDURE gm_fch_conditions (
		p_out_cursor   OUT	 TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_out_cursor
		 FOR
			 SELECT 	con_level, SUBSTR (condition, NVL (LENGTH (PRIOR condition) + 2, 2)) conditions
				   FROM (SELECT 	con_level, MAX (SYS_CONNECT_BY_PATH (NAME, ',')) condition
								  , ROW_NUMBER () OVER (ORDER BY con_level) AS curr_t
							   FROM (SELECT con_level, NAME, ROW_NUMBER () OVER (ORDER BY con_level, NAME) AS curr
									   FROM (SELECT t9701.c9701_condition_level con_level
												  , t9701.c9701_condition_name NAME
											   FROM t9701_condition t9701
											  WHERE t9701.c9701_void_fl IS NULL AND t9701.c9701_active_fl = 'Y'))
						   GROUP BY con_level
						 CONNECT BY curr - 1 = PRIOR curr
						 START WITH curr = 1)
			 CONNECT BY curr_t - 1 = PRIOR curr_t
			 START WITH curr_t = 1;
	END gm_fch_conditions;

--
/******************************************************************
 * Description : Procedure to fetch the consequences for the given
 *				 rule ids
 * Author		 : Vprasath
 ******************************************************************/
--
	PROCEDURE gm_fch_consequences (
		p_rule_ids		   IN		VARCHAR2
	  , p_txn_status	   IN		VARCHAR2
	  , p_out_cons		   OUT		TYPES.cursor_type
	  , p_out_email_cons   OUT		TYPES.cursor_type
	)
	AS
	BEGIN
		my_context.set_my_inlist_ctx (p_rule_ids);

		OPEN p_out_cons
		 FOR
			 SELECT ID, NAME, MESSAGE, holdtxn, picture, initby, initdate, updby, upddate
			   FROM (SELECT   t9704.c9700_rule_id ID, t9700.c9700_rule_name NAME
							, MAX (DECODE (t9705.c901_param_id, 91351, t9705.c9705_consequence_value, '')) MESSAGE
							, MAX (DECODE (t9705.c901_param_id, 91352, t9705.c9705_consequence_value, '')) holdtxn
							, MAX (DECODE (t9704.c901_consequence_id, 91347, 'Y', 'N')) picture
							, get_user_name (t9700.c9700_initiated_by) initby
							, TO_CHAR (t9700.c9700_initiated_date, 'MM/DD/YYYY') initdate
							, DECODE (t9700.c9700_last_updated_by
									, NULL, get_user_name (t9700.c9700_initiated_by)
									, get_user_name (t9700.c9700_last_updated_by)
									 ) updby
							, DECODE (t9700.c9700_last_updated_date
									, NULL, TO_CHAR (t9700.c9700_initiated_date, 'MM/DD/YYYY')
									, TO_CHAR (t9700.c9700_last_updated_date, 'MM/DD/YYYY')
									 ) upddate
						 FROM t9704_rule_consequence t9704, t9705_consequence_details t9705, t9700_rule t9700
						WHERE t9704.c9700_rule_id IN (SELECT token
														FROM v_in_list
													   WHERE token IS NOT NULL)
						  AND t9704.c9704_rule_consequence_id = t9705.c9704_rule_consequence_id(+)
						  AND t9700.c9700_rule_id = t9704.c9700_rule_id
						  AND t9704.c901_consequence_id <> 91348   -- EMAIL
						  AND t9704.c9704_void_fl IS NULL
						  AND t9705.c9705_void_fl IS NULL
						  AND t9700.c9700_void_fl IS NULL
						  AND t9700.c9700_active_fl = 'Y'
						  AND NVL (t9700.c9700_expiry_date, TRUNC (SYSDATE)) >= TRUNC (SYSDATE)
					 GROUP BY t9704.c9700_rule_id
							, t9700.c9700_rule_name
							, t9700.c9700_initiated_by
							, t9700.c9700_initiated_date
							, t9700.c9700_last_updated_by
							, t9700.c9700_last_updated_date)
			  WHERE holdtxn = DECODE (p_txn_status, 'VERIFY', 'Y', holdtxn);

		OPEN p_out_email_cons
		 FOR
			 SELECT   t9704.c9700_rule_id ID, t9700.c9700_rule_name NAME
					, MAX (DECODE (t9705.c901_param_id, 91365, t9705.c9705_consequence_value, '')) email_to
					, MAX (DECODE (t9705.c901_param_id, 91366, t9705.c9705_consequence_value, '')) email_sub
					, MAX (DECODE (t9705.c901_param_id, 91367, t9705.c9705_consequence_value, '')) email_body
					, get_user_name (t9700.c9700_initiated_by) initby
					, TO_CHAR (t9700.c9700_initiated_date, 'MM/DD/YYYY') initdate
					, get_user_name (t9700.c9700_last_updated_by) updby
					, TO_CHAR (t9700.c9700_last_updated_date, 'MM/DD/YYYY') upddate
				 FROM t9704_rule_consequence t9704, t9705_consequence_details t9705, t9700_rule t9700
				WHERE t9704.c9700_rule_id IN (SELECT token
												FROM v_in_list
											   WHERE token IS NOT NULL)
				  AND t9704.c901_consequence_id = 91348   --EMAIL
				  AND t9704.c9704_rule_consequence_id = t9705.c9704_rule_consequence_id
				  AND t9700.c9700_rule_id = t9704.c9700_rule_id
				  AND p_txn_status = 'VERIFY'
				  AND t9704.c9704_void_fl IS NULL
				  AND t9705.c9705_void_fl IS NULL
				  AND t9700.c9700_void_fl IS NULL
				  AND t9700.c9700_active_fl = 'Y'
				  AND NVL (t9700.c9700_expiry_date, TRUNC (SYSDATE)) >= TRUNC (SYSDATE)
			 GROUP BY t9704.c9700_rule_id
					, t9700.c9700_rule_name
					, t9700.c9700_initiated_by
					, t9700.c9700_initiated_date
					, t9700.c9700_last_updated_by
					, t9700.c9700_last_updated_date;
	END gm_fch_consequences;

	--
--
/******************************************************************
 * Description : Function to fetch the condition id for the given
 *				 Condition name
 * Author		 : Vprasath
 ******************************************************************/
--
	FUNCTION get_condition_id (
		p_condition_name   IN	VARCHAR2
	)
		RETURN NUMBER
	IS
		v_condition_id NUMBER;
	BEGIN
		SELECT t9701.c9701_condition_id
		  INTO v_condition_id
		  FROM t9701_condition t9701
		 WHERE t9701.c9701_condition_name = p_condition_name;

		RETURN v_condition_id;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN 0;
	END get_condition_id;

--
--
/******************************************************************
 * Description : Procedure to fetch the picture consequences
 *				 for the given rule ids
 * Author		 : Vprasath
 ******************************************************************/
--
	PROCEDURE gm_fch_picture_messages (
		p_rule_id	   IN		t9704_rule_consequence.c9700_rule_id%TYPE
	  , p_file_id	   IN		t903_upload_file_list.c903_upload_file_list%TYPE
	  , p_out_cursor   OUT		TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_out_cursor
		 FOR
			 SELECT DECODE (t9705.c901_param_id, 91357, t9705.c9705_consequence_value, '') comments
				  , t903.c903_upload_file_list fileid
			   FROM t9704_rule_consequence t9704, t9705_consequence_details t9705, t903_upload_file_list t903
			  WHERE t9704.c9700_rule_id = p_rule_id
				AND t9704.c901_consequence_id = 91347
				AND t9705.c901_param_id = 91357
				AND t903.c901_ref_type = 91183
				AND t903.c903_upload_file_list = NVL (p_file_id, t903.c903_upload_file_list)
				AND TO_CHAR (t9705.c9705_consequence_details_id) = t903.c903_ref_id
				AND t9704.c9704_rule_consequence_id = t9705.c9704_rule_consequence_id
				AND t9704.c9704_void_fl IS NULL
				AND t9705.c9705_void_fl IS NULL;
	END gm_fch_picture_messages;

--
--
/******************************************************************************
 * Description : Function to get the Conditions for a Rule
 * Author : Vprasath
 ******************************************************************************/
--
	FUNCTION gm_fch_rule_conditions (
		p_rule_id	IN	 t9704_rule_consequence.c9700_rule_id%TYPE
	)
		RETURN VARCHAR2
	IS
	-- Create a new cursor to fetch the condition name^code_nm^condition_value for appending the entire rows into one variable (PMT-323)
	-- this cursor was created to avoid append the bulk rows in a single query. Single query append cause the performance issue (PMT-323)
    CURSOR cur_rule_condition
    IS
    SELECT t9701.c9701_condition_name
                                       || '^'
                                       || c901_code_nm
                                       || '^'
                                       || c9702_condition_value
                                       || '|' con 
                                  FROM t9702_rule_condition t9702,
                                       t9701_condition t9701,
                                       t901_code_lookup t901
                                 WHERE t9701.c9701_condition_id =
                                                          t9702.c9701_condition_id
                                   AND c901_operator_id = c901_code_id
                                   AND c901_code_grp = 'OPERS'
                                   AND t9702.c9700_rule_id = p_rule_id
    ORDER BY t9701.C9701_CONDITION_ID DESC; 

		v_rule_conditions VARCHAR2 (4000) := '';
	BEGIN
			-- Looping the condition string and appending into the v_rule_conditions local variable (PMT-323)
		    FOR v_loc IN cur_rule_condition
    		LOOP        		 
       			    v_rule_conditions :=  v_rule_conditions || v_loc.con; 
    		END LOOP;

		RETURN v_rule_conditions;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN '';
	END gm_fch_rule_conditions;
END gm_pkg_cm_rule_engine;
/
