/* Formatted on 03/19/2010 10:19 (Formatter Plus v4.8.0) */
-- @"C:\database\Packages\Common\GM_PKG_UPLOAD_INFO.bdy"
CREATE OR REPLACE
PACKAGE BODY gm_pkg_upload_info
IS
    --
    /*******************************************************
    * Purpose: This Procedure is used to save
    * the upload file info
    *******************************************************/
    --
PROCEDURE gm_sav_upload_info (
        p_ref_id    IN t903_upload_file_list.c903_ref_id%TYPE,
        p_file_name IN t903_upload_file_list.c903_file_name%TYPE,
        p_user_name IN t903_upload_file_list.c903_created_by%TYPE,
        p_type      IN t903_upload_file_list.c901_ref_type%TYPE)
AS
    v_upload_file_list t903_upload_file_list.c903_upload_file_list%TYPE;
BEGIN
    gm_sav_upload_info (p_ref_id, p_file_name, p_user_name, p_type, v_upload_file_list) ;
END gm_sav_upload_info;
/*******************************************************
* Purpose: This Procedure is used to save
* the upload file info
*******************************************************/
--
PROCEDURE gm_sav_upload_info (
        p_ref_id    IN t903_upload_file_list.c903_ref_id%TYPE,
        p_file_name IN t903_upload_file_list.c903_file_name%TYPE,
        p_user_name IN t903_upload_file_list.c903_created_by%TYPE,
        p_type      IN t903_upload_file_list.c901_ref_type%TYPE,
        p_upload_file_list OUT t903_upload_file_list.c903_upload_file_list%TYPE)
AS
BEGIN
     SELECT s903_upload_file_list.NEXTVAL INTO p_upload_file_list FROM DUAL;
     INSERT
       INTO t903_upload_file_list
        (
            c903_upload_file_list, c903_ref_id, c903_file_name
          , c903_created_by, c903_created_date, c901_ref_type
        )
        VALUES
        (
            p_upload_file_list, p_ref_id, p_file_name
          , p_user_name, SYSDATE, p_type
        ) ;
END gm_sav_upload_info;
/*******************************************************
* Purpose: This Procedure is used to save file from  Quality File upload screen
* and COM/RSR File Upload
*******************************************************/
PROCEDURE gm_sav_upload_unique_info
(
      p_file_id		IN t903_upload_file_list.c903_upload_file_list%TYPE
	, p_ref_id		IN t903_upload_file_list.c903_ref_id%TYPE
	, p_file_name	IN t903_upload_file_list.c903_file_name%TYPE
	, p_user_name	IN t903_upload_file_list.c903_created_by%TYPE
	, p_type		IN t903_upload_file_list.c901_ref_type%TYPE
	, p_file_title	IN t903_upload_file_list.c901_file_title%TYPE
	, p_file_size	IN t903_upload_file_list.c903_file_size%TYPE
	, p_file_seqno	IN t903_upload_file_list.c903_file_seq_no%TYPE
	, p_ref_grp     IN t903_upload_file_list.c901_ref_grp%TYPE
	, p_sub_type	IN t903_upload_file_list.c901_type%TYPE
	, p_title_type  IN t903a_file_attribute.c901_attribute_type%TYPE
	, p_out_file_id OUT t903_upload_file_list.c903_upload_file_list%TYPE
	, p_part_number	IN t903_upload_file_list.c205_part_number_id%TYPE DEFAULT NULL
	
)
AS
 v_attr_string VARCHAR2(1000);
 v_fileid t903_upload_file_list.c903_upload_file_list%TYPE := p_file_id;
 v_seq_no NUMBER;
BEGIN
	
-- PMT#37770- update/insert part number value when any updating happen from file upload page
	
	UPDATE t903_upload_file_list
	SET c903_last_updated_by = p_user_name
		, c903_last_updated_date = SYSDATE
		, c901_file_title	= DECODE(p_title_type,'103216',NULL,p_file_title) -- 103216 - Title Attribute type
		, c903_file_size	= p_file_size
		, c903_file_seq_no	= p_file_seqno
		, c901_ref_grp		= p_ref_grp
		, c901_type         = p_sub_type
		, c903_file_name    = p_file_name
		, c901_ref_type     = p_type
		, c205_part_number_id = p_part_number
	WHERE c903_ref_id	= DECODE(p_file_id,NULL,p_ref_id,c903_ref_id)
	AND c903_file_name	= DECODE(p_file_id,NULL,p_file_name,c903_file_name)
	AND c901_ref_type	= DECODE(p_file_id,NULL,p_type,c901_ref_type)
	AND c903_upload_file_list = NVL(p_file_id,c903_upload_file_list)
	AND c903_delete_fl IS NULL;

	IF (SQL%ROWCOUNT = 0) THEN
	
		IF p_type IS NOT NULL AND p_sub_type IS NOT NULL AND p_file_seqno IS NULL THEN
		BEGIN  
		SELECT MAX(NVL(c903_file_seq_no, 0))
		  INTO v_seq_no
		  FROM t903_upload_file_list
		  WHERE c903_ref_id   = p_ref_id  
		  AND c901_ref_grp    = p_ref_grp 
		  AND C903_DELETE_FL IS NULL
		  GROUP BY c903_ref_id;
		
		EXCEPTION
   			WHEN NO_DATA_FOUND THEN
        	v_seq_no := '0';
        END;
        v_seq_no := v_seq_no+1;	
		ELSE
		  v_seq_no := p_file_seqno;
		END IF;
	
	    SELECT s903_upload_file_list.NEXTVAL INTO v_fileid FROM DUAL;
		INSERT INTO t903_upload_file_list
		(
			c903_upload_file_list, c903_ref_id, c903_file_name, c901_ref_type, c901_file_title
			, c903_file_size, c903_file_seq_no, c901_ref_grp, c901_type, c903_created_by, c903_created_date
			,c205_part_number_id 
		)
		VALUES
		(
			v_fileid, p_ref_id, p_file_name, p_type, p_file_title
			, p_file_size, v_seq_no, p_ref_grp, p_sub_type, p_user_name, SYSDATE, p_part_number
		);
		-- System files (103092)
		IF p_ref_grp = '103092' THEN
		    -- For system based on sub type the file should shareable or not
			gm_sav_shareble_file_subtype(v_fileid,p_sub_type,p_user_name);
		END IF;
	END IF;

	--If file is DCOed Doc and ref type is system then the file title will be partnum dropdown then
	--the partnum will stored as refid in file attribute table 
	IF p_title_type = '103216' AND v_fileid IS NOT NULL THEN --File Title Attribute Type 
	    --save file attribute
		gm_sav_file_attributes(v_fileid,p_title_type,p_file_title,p_user_name);
	END IF;	
	p_out_file_id:= v_fileid;
END gm_sav_upload_unique_info;
--
/*******************************************************
* Purpose: This Procedure is used to fetch
* the upload file info
*******************************************************/
--
PROCEDURE gm_fch_upload_info(
    p_type          IN t903_upload_file_list.c901_ref_type%TYPE,
    p_study_list_id IN t903_upload_file_list.c903_ref_id%TYPE,
    p_ref_grp       IN t903_upload_file_list.c901_ref_grp%TYPE,
    p_sub_type      IN t903_upload_file_list.c901_type%TYPE,
    p_fetchinfo OUT TYPES.cursor_type )
AS
  v_datefmt VARCHAR2(20);
BEGIN
	 SELECT NVL(get_compdtfmt_frm_cntx (), get_rule_value('DATEFMT','DATEFORMAT')) INTO v_datefmt FROM dual; 

  OPEN p_fetchinfo 
  FOR SELECT t903.c903_upload_file_list id, t903.c903_file_name name, 
    get_code_name ( c901_ref_type ) mattype,c901_ref_type reftype, 
    TO_CHAR(NVL(c903_last_updated_date, c903_created_date), v_datefmt) cdate,
    TO_CHAR(NVL(c903_last_updated_date, c903_created_date), v_datefmt||' HH:MI AM') cdatetime , 
    
    NVL ( TRIM (get_user_name (t903.c903_last_updated_by)), TRIM (get_user_name (t903.c903_created_by)) ) uname , t903.c903_ref_id setid,
    DECODE(c901_ref_type,91184,get_set_name(t903.c903_ref_id),'') setname , 
    lower(SUBSTR( t903.c903_file_name, INSTR( t903.c903_file_name,'.', -1, 1)+1)) extention ,
     -- 103092 - System , 103114 - DCOed Documents
    DECODE(T903.C901_REF_GRP,'103092',DECODE(T903.C901_REF_TYPE,'103114',GET_PARTNUM_DESC(T903A.titlerefid),T903.C901_FILE_TITLE),T903.C901_FILE_TITLE) FTITLE , -- will give file title 
    -- 103092 - System , 103114 - DCOed Documents , 103216 - File Title Attribute
    DECODE(T903.C901_REF_GRP,'103092',DECODE(T903.C901_REF_TYPE,'103114','103216')) titletype , T903A.titlerefid titlerefid,
    GET_MBKB_FROM_BYTES(t903.c903_file_size) fsize ,     -- will give file size
    t903.c903_file_size filesize ,   -- will give file size in bytes
    t903.c903_created_by userid ,   -- will give user id of file uploaded
    t903.c903_file_seq_no fseqno ,   -- will give file sequence no 
    t903.c901_ref_grp fgrp ,     -- will give file group
    get_code_name (t903.c901_ref_grp) fgrpnm, get_code_name(t903.c901_type) subtypenm, t903.c901_type subtype,
    t903a.shareable shareable , t2050.keywords keywords,t903.c205_part_number_id partnum FROM t903_upload_file_list t903,
  (SELECT c903_upload_file_list fileid,
    MAX (DECODE (c901_attribute_type, 103216, c903a_attribute_value)) titlerefid,
    MAX (DECODE (c901_attribute_type, 103217, c903a_attribute_value)) shareable
  FROM t903a_file_attribute
  WHERE c903a_void_fl IS NULL
  GROUP BY c903_upload_file_list
  ) t903a,
  (SELECT RTRIM (XMLAGG (XMLELEMENT (N, t2050.c2050_keyword_ref_name
    || ',')) .EXTRACT ('//text()'), ', ') keywords,
    t2050.c2050_ref_id fileid
  FROM t2050_keyword_ref t2050
  WHERE t2050.c901_ref_type = 4000410 --File
  AND t2050.c2050_void_fl  IS NULL
  GROUP BY t2050.c2050_ref_id
  ) t2050 WHERE t903.c901_ref_type = NVL (p_type, t903.c901_ref_type) 
          AND t903.c903_ref_id = NVL (p_study_list_id, t903.c903_ref_id) 
          AND NVL(t903.C901_TYPE, -999) = NVL (p_sub_type, NVL(t903.C901_TYPE, -999))
          AND NVL(t903.c901_ref_grp, -999) = NVL (p_ref_grp, NVL(t903.c901_ref_grp, -999)) 
          AND C903_DELETE_FL IS NULL 
          AND t903.c903_upload_file_list = t903a.fileid(+) 
          AND t903.c903_upload_file_list = t2050.fileid(+) 
          ORDER BY c903_file_seq_no, NVL(c903_last_updated_date, c903_created_date) DESC;
END gm_fch_upload_info;
--
/*******************************************************
* Purpose: This Procedure is used to validating file
* name
*******************************************************/
--
PROCEDURE gm_validate_file_name
    (
        p_type          IN t903_upload_file_list.c901_ref_type%TYPE,
        p_study_list_id IN t903_upload_file_list.c903_ref_id%TYPE,
        p_file_name     IN t903_upload_file_list.c903_file_name%TYPE
    )
AS
    v_validate NUMBER;
BEGIN
     SELECT COUNT (1)
       INTO v_validate
       FROM t903_upload_file_list
      WHERE c903_ref_id    = p_study_list_id
        AND c901_ref_type  = p_type
        AND c903_file_name = p_file_name
        AND C903_delete_fl IS NULL;
    IF v_validate          > 0 THEN
        -- Error message is Can't void site which has patients mapped to it.
        raise_application_error ( - 20784, '') ;
    END IF;
END gm_validate_file_name;
--
/*******************************************************
* Purpose: This Procedure is used to fetch
* the upload file name alone
*******************************************************/
--
PROCEDURE gm_fch_upload_file_name (
        p_id IN t903_upload_file_list.c903_upload_file_list%TYPE,
        p_fetchinfo OUT t903_upload_file_list.c903_file_name%TYPE)
AS
BEGIN
     SELECT c903_file_name
       INTO p_fetchinfo
       FROM t903_upload_file_list
      WHERE c903_upload_file_list = p_id;
END gm_fch_upload_file_name;
--
/*******************************************************
* Purpose: This Procedure is used to Void
* the exising files
*******************************************************/
--
PROCEDURE gm_void_upload_info (
        p_inputstr IN VARCHAR2,
        p_userid   IN t903_upload_file_list.c903_last_updated_by%TYPE)
AS
    v_strlen    NUMBER          := NVL (LENGTH (p_inputstr), 0) ;
    v_string    VARCHAR2 (4000) := p_inputstr;
    v_substring VARCHAR2 (1000) ;
    v_tagid     NUMBER := 0;
BEGIN
    IF v_strlen                      > 0 THEN
        WHILE INSTR (v_string, '|') <> 0
        LOOP
            --
            v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
            v_string    := SUBSTR (v_string, INSTR (v_string, '|')    + 1) ;
            v_tagid     := TO_NUMBER (v_substring) ;
            
             UPDATE t903_upload_file_list
                SET c903_delete_fl            = 'Y', 
                    c903_last_updated_date = Sysdate, 
                    c903_last_updated_by = p_userid
              WHERE c903_upload_file_list = v_tagid;
        END LOOP;
    END IF;
END gm_void_upload_info;

/****************************************************************
* Purpose: This Procedure is used to validating file name status
****************************************************************/
--
PROCEDURE gm_fch_file_name_status(
	p_ref_id 		IN 	t903_upload_file_list.c903_ref_id%TYPE,
	p_type          IN 	t903_upload_file_list.c901_ref_type%TYPE,
    p_file_name     IN t903_upload_file_list.c903_file_name%TYPE,
    p_flag     		OUT CHAR
)
AS
    v_validate NUMBER;
    v_flag  CHAR(1) := 'N';
BEGIN
	--INSERT INTO MY_TEMP_LIST VALUES(p_file_name);
	--commit;
		
     SELECT COUNT (1)
       INTO v_validate
       FROM t903_upload_file_list
      WHERE c903_ref_id    = p_ref_id
        AND c901_ref_type  = p_type
        AND c903_file_name = p_file_name
        AND C903_DELETE_FL IS NULL;
    
    IF v_validate >= 1 THEN
    	v_flag := 'Y';
    END IF;
    p_flag := v_flag;
END gm_fch_file_name_status;

/****************************************************************
* Purpose: This Procedure is used to save details for files.
* In this procedure we are going to pass input string as
* p_inputstr : FileTitle^FileSeqNo^FileListId|FileTitle^FileSeqNo^FileListId|
* 
* Author : Jignesh Shah
****************************************************************/
PROCEDURE gm_save_upload_bulk_info(
	p_inputstr	IN VARCHAR2,
	p_userid	IN t903_upload_file_list.c903_last_updated_by%TYPE
)
AS
    v_strlen    NUMBER          := NVL (LENGTH (p_inputstr), 0) ;
    v_string    VARCHAR2 (4000) := p_inputstr;
    v_substring VARCHAR2 (4000);
 	v_file_title   t903_upload_file_list.c901_file_title%TYPE;
    v_order_by     t903_upload_file_list.c903_file_seq_no%TYPE;
    v_file_list_id t903_upload_file_list.c903_upload_file_list%TYPE;
BEGIN
    IF v_strlen > 0 THEN
        WHILE INSTR (v_string, '|') <> 0
        LOOP
            --
            v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
            v_string    := SUBSTR (v_string, INSTR (v_string, '|')    + 1) ;
            
            v_file_title := NULL;
            v_order_by	 := NULL;
			
            v_file_title := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
			v_substring  := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
			
			v_order_by	 := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
			v_substring  := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
            
			v_file_list_id := TO_NUMBER (v_substring) ;
            
			UPDATE t903_upload_file_list
			SET c901_file_title = v_file_title
				, c903_file_seq_no = v_order_by
				, c903_last_updated_date = Sysdate 
				, c903_last_updated_by = p_userid
			WHERE c903_upload_file_list = v_file_list_id;

        END LOOP;
    END IF;

END gm_save_upload_bulk_info;

/****************************************************************
* Purpose: This Procedure is used to void files
* Author : Jignesh Shah
* p_inputstr : FileListId,FileListId,FileListId,
****************************************************************/
PROCEDURE gm_void_file_upload_info(
	p_inputstr	IN VARCHAR2,
	p_userid	IN t903_upload_file_list.c903_last_updated_by%TYPE
)
AS
    v_strlen    NUMBER          := NVL (LENGTH (p_inputstr), 0) ;
    v_string    VARCHAR2 (4000) := p_inputstr || ',';
    v_file_list_id	VARCHAR2 (40);
    
BEGIN
    IF v_strlen > 0 THEN
		--
		WHILE INSTR (v_string, ',') <> 0
		LOOP
	       	v_file_list_id	:= SUBSTR (v_string, 1, INSTR (v_string, ',') - 1);
	       	v_string 		:= SUBSTR (v_string, INSTR (v_string, ',') + 1);
	         
	       	UPDATE t903_upload_file_list
			SET c903_delete_fl = 'Y'
				, c903_last_updated_date = sysdate 
				, c903_last_updated_by = p_userid
			WHERE c903_upload_file_list = v_file_list_id;
				
		END LOOP;
    END IF;
END gm_void_file_upload_info;


/****************************************************************
* Purpose: This Procedure is used to fetch voided files info
* p_inputString we are passing into my_context list it will give 
* records one by one and based on that get the deleted files inro
* from the table.
* Author : Jignesh Shah
* p_inputString : FileListId,FileListId,FileListId,
****************************************************************/
PROCEDURE gm_fch_upload_file_info(
	p_inputString		IN		CLOB
  , p_void_file_info	OUT		TYPES.cursor_type
)
AS
BEGIN
	my_context.set_my_inlist_ctx(p_inputString);

	OPEN p_void_file_info
	FOR
		SELECT c903_upload_file_list filelistid
			, c903_file_name filename
			, c903_ref_id filerefid
			, c901_ref_type reftype
			, get_code_name(c901_ref_grp) filerefgrp
			, c901_type subtype
		FROM t903_upload_file_list
		WHERE c903_upload_file_list IN (SELECT token FROM v_in_list WHERE token IS NOT NULL);
	
	END gm_fch_upload_file_info;

/****************************************************************
* Purpose: This Function is used to get file size in KB/MB 
* Author : Anilkumar
****************************************************************/
FUNCTION get_mbkb_from_bytes(
      p_bytes NUMBER,
      p_type VARCHAR2 DEFAULT 'KB'
      )
    RETURN VARCHAR2
  IS
    v_megabyte  NUMBER := 1048576;
    v_kilobyte  NUMBER := 1024;
    v_file_size VARCHAR2 (50) ;
  BEGIN
    IF p_bytes IS NULL THEN
      RETURN '';
    ELSIF p_type='MB' THEN
     v_file_size := TO_CHAR ( (p_bytes / v_megabyte), '999,999.00') || ' MB';
    ELSE
     v_file_size := TO_CHAR ( (p_bytes / v_kilobyte), '999,999.00') || ' KB';  
    END IF;
    RETURN v_file_size;
  END;
/****************************************************************
* Purpose: This Procedure is used to save details for files.
* In this procedure we are going to pass input string as
* p_inputstring : FileId^Y^ShareType|FileId^N^ShareType|
* Author : Anilkumar
****************************************************************/
PROCEDURE gm_sav_file_attributes(
    p_inputString IN VARCHAR2,
    p_userId      IN t903_upload_file_list.c903_last_updated_by%TYPE )
AS
  v_file_id		 t903a_file_attribute.c903_upload_file_list%TYPE;
  v_string 		 VARCHAR2 (4000) := p_inputString;
  v_substring	 VARCHAR2 (4000);
  v_attr_value	 t903a_file_attribute.c903a_attribute_value%TYPE;
  v_attr_type	 t903a_file_attribute.c901_attribute_type%TYPE;
  v_update_count NUMBER := 0;
  v_strlen   	 VARCHAR2(4000) := NVL (LENGTH (p_inputString), 0) ;
BEGIN
  IF v_strlen                    > 0 THEN
    WHILE INSTR (v_string, '|') <> 0
    LOOP
	      --
		v_substring  := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
		v_string     := SUBSTR (v_string, INSTR (v_string, '|')    + 1) ;
		
		v_file_id	 := NULL;
		v_attr_value := NULL;
		v_attr_type  := NULL;
		
		v_file_id		:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
		v_substring  	:= SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
		
		v_attr_value	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
		v_substring 	:= SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
		
		v_attr_type 	:= TO_NUMBER (v_substring) ;
	    --update/insert into t903a_file_attribute table
    	gm_sav_file_attributes(v_file_id,v_attr_type,v_attr_value,p_userid);
      END LOOP;
     END IF;  
	END gm_sav_file_attributes; 
/****************************************************************
* Purpose: This Procedure is used to save details for files.
* Author : Anilkumar
****************************************************************/   
PROCEDURE gm_sav_file_attributes(
	P_file_id	IN 	t903a_file_attribute.c903_upload_file_list%TYPE,
	P_attr_type	IN	t903a_file_attribute.c901_attribute_type%TYPE,
	P_attr_value IN	t903a_file_attribute.c903a_attribute_value%TYPE,
    p_userId	IN	t903_upload_file_list.c903_last_updated_by%TYPE 
    )
AS
BEGIN
	  UPDATE t903a_file_attribute
         SET c903a_attribute_value  = p_attr_value,
        	 c903a_last_updated_date  = sysdate,
       		 c903a_last_updated_by   = p_userid
       WHERE c903_upload_file_list = p_file_id
         AND c901_attribute_type = p_attr_type;	
    
       IF (SQL%ROWCOUNT = 0)THEN	
			INSERT
		      INTO t903a_file_attribute(
		           c903a_file_attribute_id,c903_upload_file_list,c903a_attribute_value,c901_attribute_type,c903a_created_by,
		           c903a_created_date
		           )
		       VALUES(s903a_file_attribute.NEXTVAL,p_file_id,p_attr_value,p_attr_type,p_userid,sysdate);
   		END IF; 
    END gm_sav_file_attributes; 
/**********************************************************************************
* Purpose: This Procedure is used to save file shareable attribute based on sub type.
* Author : Gopinathan
**********************************************************************************/
PROCEDURE gm_sav_shareble_file_subtype (
        p_file_id  IN t903_upload_file_list.c903_upload_file_list%TYPE,
        p_sub_type IN t903_upload_file_list.c901_type%TYPE,
        p_userId   IN t903a_file_attribute.c903a_created_by%TYPE)
AS
    v_share_fl t906_rules.c906_rule_value%TYPE;
BEGIN
    BEGIN
        --get sub type is shareable or not from rule table
        --Ex. 360 View,Product Overview and Sales Reference Guide are not shareable
         SELECT NVL (c906_rule_value, 'N')
           INTO v_share_fl
           FROM t906_rules
          WHERE c906_rule_grp_id = 'SHAREGRP'
            AND c906_void_fl    IS NULL
            AND c906_rule_id     = p_sub_type;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_share_fl := 'Y';
    END;
    
    IF v_share_fl = 'Y' THEN --If sub type is shareable then insert Y into attribute table
        --save shareable file attribute
        gm_sav_file_attributes (p_file_id, '103217', v_share_fl, p_userId) ;
    END IF;
END gm_sav_shareble_file_subtype;
/****************************************************************   
* Purpose: This Procedure is used to Update delete flag based on ref id
* Author : Ganeshbabu R 
****************************************************************/
PROCEDURE gm_void_upload_file_name
(
	p_ref_id 		IN 	t903_upload_file_list.c903_ref_id%TYPE,
	p_type          IN 	t903_upload_file_list.c901_ref_type%TYPE,
    p_file_name     IN  t903_upload_file_list.c903_file_name%TYPE,
    p_user_id	    IN  t903_upload_file_list.c903_last_updated_by%TYPE
)
 AS
  BEGIN
	   -- Update the delete flag.
	  UPDATE t903_upload_file_list
			   SET c903_delete_fl = 'Y'
			   , c903_last_updated_by = p_user_id
			   , c903_last_updated_date = SYSDATE
			 WHERE c903_ref_id = p_ref_id
			 AND c903_file_name = p_file_name
			 AND c901_ref_type = p_type
 	     	 AND c903_delete_fl IS NULL;
			
  END gm_void_upload_file_name;
  
  	
	/****************************************************************
	* Purpose: This Procedure is used to fetch upload file info based on file list ids
	* p_inputString we are passing into my_context list it will give
	* records one by one and based on that get the files info
	* from the table (t903).
	* Author : mmuthusamy
	* p_inputString : FileListIds
	* 
	* PC-4295: spineIT file upload info save to azure
	* PC-4143: to handle the URL encode and added the set and group name
	****************************************************************/
	PROCEDURE gm_fch_upload_json_file_info (
	        p_inputString IN CLOB,
	        p_out_json OUT CLOB)
	AS
	v_date_fmt VARCHAR2 (12);
	BEGIN
		-- to get the company date format
		
		SELECT get_compdtfmt_frm_cntx () INTO v_date_fmt
		FROM DUAL;
		
		IF p_inputString IS NOT NULL
		THEN
			--
		    my_context.set_my_cloblist (p_inputString ||',') ;
		    --
	    END IF;
	    --
	     SELECT JSON_ARRAYAGG (JSON_OBJECT ('id' value to_char(t903.c903_upload_file_list),
	     	'fl_li_id' value t903.c903_upload_file_list,
	      	'ref_id' value t903.c903_ref_id,
	     	'fl_nm' value t903.c903_file_name, 
	     	'fl_rf_ty_nm' value get_code_name (c901_ref_type), 
	     	'fl_rf_ty_id' value c901_ref_type,
	     	'cr_dt' value t903.c903_created_date,
	        'ls_ud_dt' value t903.c903_last_updated_date,
	        'cr_dt_ti' value TO_CHAR (c903_created_date, v_date_fmt ||' HH:MI AM'),
	        'ls_ud_dt_ti' value TO_CHAR (c903_last_updated_date, v_date_fmt ||' HH:MI AM'),
	        'cr_by' value t903.c903_created_by,
	        'cr_by_nm' value TRIM (get_user_name (t903.c903_created_by)),
	        'ls_ud_by_nm' value TRIM (get_user_name (t903.c903_last_updated_by)), 
	        'sys_id' value DECODE (T903.c901_ref_grp, 103092, t903.c903_ref_id, NULL), 
	        'sys_nm' value DECODE (T903.c901_ref_grp, 103092, get_set_name (t903.c903_ref_id), ''), 
	        'fl_ext' value lower (SUBSTR (t903.c903_file_name, INSTR (t903.c903_file_name, '.', - 1, 1) + 1)),
	        -- 103092 - System , 103114 - DCOed Documents
	        'fl_tit' value DECODE (T903.C901_REF_GRP, '103092', DECODE (T903.C901_REF_TYPE, '103114', GET_PARTNUM_DESC (T903A.titlerefid), T903.C901_FILE_TITLE), T903.C901_FILE_TITLE), -- will give file title
	        -- 103092 - System , 103114 - DCOed Documents , 103216 - File Title Attribute
	        'fl_tit_ty' value DECODE (T903.C901_REF_GRP, '103092', DECODE (T903.C901_REF_TYPE, '103114', '103216')),
	        'fl_tit_rf_id' value T903A.titlerefid,
	        'fl_si_va' value TRIM(gm_pkg_upload_info.GET_MBKB_FROM_BYTES (t903.c903_file_size)), -- will give file size
	        'fl_si' value t903.c903_file_size, -- will give file size in bytes
	        'ls_upd_by_id' value t903.c903_last_updated_by,
	        'fl_seq_n' value t903.c903_file_seq_no, -- will give file sequence no
	        'fl_rf_gp_id' value t903.c901_ref_grp, -- will give file group
	        'cmp_id' value t903.C1900_COMPANY_ID,
	        'fl_rf_gp_nm' value get_code_name (t903.c901_ref_grp), 
	        'fl_sb_ty_nm' value get_code_name (t903.c901_type), 
	        'fl_sb_ty_id' value t903.c901_type, 
	        'share_fl' value t903a.shareable, 
	        'k_words'value t2050.keywords, 
	        'pnum' value t903.c205_part_number_id, 
	        'pdesc' value GET_PARTNUM_DESC(t903.c205_part_number_id),
	        'set_id' value DECODE (T903.c901_ref_grp, 103091, t903.c903_ref_id, NULL), 
	        'set_nm' value DECODE (T903.c901_ref_grp, 103091, get_set_name (t903.c903_ref_id), NULL), 
	        'group_id' value DECODE (T903.c901_ref_grp, 103094, t903.c903_ref_id, NULL), 
	        'group_nm' value DECODE (T903.c901_ref_grp, 103094, get_group_name (t903.c903_ref_id), NULL), 
	        'vfl' value t903.c903_delete_fl, 
	        'ls_upd_dt_utc' value t903.C903_LAST_UPDATED_DATE_UTC, 
	        'ipad_syn_dt' value C903_IPAD_SYNCED_DATE, 
	        'ipad_syn_fl' value C903_IPAD_SYNCED_FLAG, 
	        'fl_nm_url' value get_code_name(T903.C901_REF_GRP) || '/' || t903.c903_ref_id ||'/' || T903.C901_REF_TYPE || '/' || t903.c901_type || '/' || utl_url.escape(t903.c903_file_name)
	        ) RETURNING CLOB)
	       INTO p_out_json
	       FROM t903_upload_file_list t903, (
	             SELECT c903_upload_file_list fileid, MAX (DECODE (c901_attribute_type, 103216, c903a_attribute_value))
	                titlerefid, MAX (DECODE (c901_attribute_type, 103217, c903a_attribute_value)) shareable
	               FROM t903a_file_attribute
	              WHERE c903_upload_file_list IN
	              (
	              	SELECT token FROM v_clob_list WHERE token IS NOT NULL
	              )
	              AND c903a_void_fl IS NULL
	           GROUP BY c903_upload_file_list
	        )
	        t903a, (
	             SELECT RTRIM (XMLAGG (XMLELEMENT (N, t2050.c2050_keyword_ref_name || ',')) .EXTRACT ('//text()'), ', ')
	                keywords, t2050.c2050_ref_id fileid
	               FROM t2050_keyword_ref t2050
	              WHERE t2050.c901_ref_type  = 4000410 --File
	              	AND t2050.c2050_ref_id IN 
	              	(
	              	SELECT token FROM v_clob_list WHERE token IS NOT NULL
	              	)
	                AND t2050.c2050_void_fl IS NULL
	           GROUP BY t2050.c2050_ref_id
	        )
	        t2050
	      WHERE t903.C903_UPLOAD_FILE_LIST IN
	        (
	             SELECT token FROM v_clob_list WHERE token IS NOT NULL
	        )
	        --AND C903_DELETE_FL            IS NULL
	        AND t903.c903_upload_file_list = t903a.fileid(+)
	        AND t903.c903_upload_file_list = t2050.fileid(+)
	   ORDER BY c903_file_seq_no, NVL (c903_last_updated_date, c903_created_date) DESC;
	   --
	   
	END gm_fch_upload_json_file_info;
	
/****************************************************************
* Purpose: This Procedure is used to fetch upload information based on colud sync flag
* p_code_grp we are passing code group - based on group fetch the PK and store to context
* p_out_upload_json return upload files JSON details
*
* Author : mmuthusamy
* p_code_grp : MREFTY
*
* PC-4295: spineIT file upload info save to azure
****************************************************************/
PROCEDURE gm_fch_upload_json_by_ref_id (
        p_code_grp IN t901_code_lookup.c901_code_grp%TYPE,
        p_ref_id IN t903_upload_file_list.c903_ref_id%TYPE,
        p_out_json OUT CLOB)
AS
BEGIN

	   --1 based on code group to get the upload file list id
    --
     DELETE
       FROM my_t_part_list;
    --
     INSERT INTO my_t_part_list
        (c205_part_number_id
        )
     SELECT t903.c903_upload_file_list
       FROM t903_upload_file_list t903, t901_code_lookup t901
      WHERE t901.c901_code_id            = t903.C901_REF_GRP
        AND t901.c901_code_grp           = p_code_grp
        AND t903.c903_ref_id           = p_ref_id -- Part number
        AND t903.c903_cloud_synced_flag = 'P';
        
    --2 to call the procedure and get the JSON data
    gm_pkg_upload_info.gm_fch_upload_json_file_info (NULL, p_out_json) ;
    --
END gm_fch_upload_json_by_ref_id;

--
/****************************************************************
* Purpose: This Procedure is used to update cloud sync flag to processing
*  based on code group and cloud sync flag empty condition
*
****************************************************************/
PROCEDURE gm_sav_cloud_file_process_fl (
        p_code_grp           IN t901_code_lookup.c901_code_grp%TYPE,
        p_ref_grp IN      t901_code_lookup.c901_code_id%TYPE,
        p_curr_cloud_sync_fl IN t903_upload_file_list.c903_cloud_synced_flag%TYPE,
        p_new_colud_sync_fl  IN t903_upload_file_list.c903_cloud_synced_flag%TYPE,
        p_user_id            IN t903_upload_file_list.c903_last_updated_by%TYPE,
        p_out_total_cnt OUT NUMBER)
AS

BEGIN
    --1 based on code group to get the upload file list id
    --
     DELETE
       FROM my_t_part_list;
    --
     INSERT INTO my_t_part_list
        (c205_part_number_id
        )
     SELECT t903.c903_upload_file_list
       FROM t903_upload_file_list t903, t901_code_lookup t901
      WHERE t901.c901_code_id                      = t903.C901_REF_GRP
        AND t901.c901_code_grp                     = p_code_grp
        AND t903.c901_ref_grp = p_ref_grp
        AND NVL (t903.c903_cloud_synced_flag, 'N') = NVL (p_curr_cloud_sync_fl, 'N') ;
    --
    --2 to update the process flag as P
    gm_pkg_upload_info.gm_upd_cloud_synced_fl (NULL, p_new_colud_sync_fl, p_user_id) ;
    
    -- 3 to get the total count
     SELECT COUNT (1)
       INTO p_out_total_cnt
       FROM t903_upload_file_list
      WHERE c903_cloud_synced_flag = p_new_colud_sync_fl;
      
END gm_sav_cloud_file_process_fl;

/****************************************************************
* Purpose: This Procedure is used to update cloud sync flag P or Y
*  based on upload file list id
*
*/
PROCEDURE gm_upd_cloud_synced_fl (
		p_inputString IN CLOB,
        p_new_cloud_sync_fl IN t903_upload_file_list.c903_cloud_synced_flag%TYPE,
        p_user_id           IN t903_upload_file_list.c903_last_updated_by%TYPE)
AS

BEGIN
	-- to check the upload string and added into clob
	 IF p_inputString IS NOT NULL
	 THEN
			--
		    my_context.set_my_cloblist (p_inputString) ;
		    --
	 END IF;
    -- to check the PK from clob list and update the new cloud sync flag
     UPDATE t903_upload_file_list
    SET c903_cloud_synced_flag = p_new_cloud_sync_fl, c903_last_updated_by = p_user_id
    , c903_last_updated_date = CURRENT_DATE
      WHERE EXISTS
        (
             SELECT token
               FROM v_clob_list
              WHERE token  = c903_upload_file_list
                AND token IS NOT NULL
        ) ;
END gm_upd_cloud_synced_fl;

/****************************************************************
* Purpose: This Procedure is used to update cloud sync flag P or Y
*  based on Ref id
*
****************************************************************/
PROCEDURE gm_upd_cloud_synced_fl_by_ref_id (
		p_ref_id IN t903_upload_file_list.c903_ref_id%TYPE,
		p_current_cloud_sync_fl IN t903_upload_file_list.c903_cloud_synced_flag%TYPE,
        p_new_cloud_sync_fl IN t903_upload_file_list.c903_cloud_synced_flag%TYPE,
        p_user_id           IN t903_upload_file_list.c903_last_updated_by%TYPE)
AS

BEGIN
	
    -- to update the new cloud sync flag based on p_ref_id
     UPDATE t903_upload_file_list
    SET c903_cloud_synced_flag = p_new_cloud_sync_fl, c903_last_updated_by = p_user_id
    , c903_last_updated_date = CURRENT_DATE
      WHERE c903_ref_id = p_ref_id 
      AND c903_cloud_synced_flag = p_current_cloud_sync_fl;
      
END gm_upd_cloud_synced_fl_by_ref_id;

/****************************************************************
* Purpose: This Procedure is used to fetch fetch file sync process
* transaction - based on group type 
*
* Author : mmuthusamy
* p_code_grp : MREFTY
*
* PC-4295: spineIT file upload info save to azure
****************************************************************/
PROCEDURE gm_fch_file_sync_process_txn (
        p_code_grp IN t901_code_lookup.c901_code_grp%TYPE,
        p_ref_grp_type IN t903_upload_file_list.c901_ref_grp%TYPE,
        p_out_txn_dtls	OUT		TYPES.cursor_type)
AS
BEGIN

	   --1 based on code group to get the upload file list id
    --
    OPEN p_out_txn_dtls FOR
     SELECT t903.c903_ref_id ref_id
	   FROM t903_upload_file_list t903, t901_code_lookup t901
	  WHERE t901.c901_code_id           = t903.c901_ref_grp
	    AND t901.c901_code_grp          = p_code_grp
	    AND t903.c901_ref_grp           = p_ref_grp_type -- Part number
	    AND t903.c903_cloud_synced_flag = 'P'
		GROUP BY t903.c903_ref_id;
    --
END gm_fch_file_sync_process_txn;
	
END gm_pkg_upload_info;
/
