/* Formatted on 2010/01/25 14:34 (Formatter Plus v4.8.0) */

--@"C:\Database\Packages\Common\gm_pkg_cm_order_planning_back.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_cm_order_planning_back
IS
	/*****************************************************************************
	Description : Procedure to backup TTP Master record in the temp table
	******************************************************************************/
	PROCEDURE gm_demand_sheet_master_backup (
		p_from_date   IN   VARCHAR2
	  , p_to_date	  IN   VARCHAR2
	)
	AS
	BEGIN
		DELETE FROM my_temp_key_value;

		INSERT INTO my_temp_key_value
					(my_temp_txn_id, my_temp_txn_key, my_temp_txn_value)
			SELECT c4040_demand_sheet_id demandsheetid, c4052_ttp_detail_id ttpdetailid, c250_inventory_lock_id lockid
			  FROM t4040_demand_sheet
			 WHERE c4040_load_dt >= TO_DATE (p_from_date, 'MM/DD/YYYY')
			   AND c4040_load_dt <= TO_DATE (p_to_date, 'MM/DD/YYYY');

		gm_demand_sheet_archive;

		DELETE FROM my_temp_list;

		--- Backup data table t4052_ttp_detail which is not reference in demand sheet table
		INSERT INTO my_temp_list
					(my_temp_txn_id)
			SELECT c4052_ttp_detail_id
			  FROM t4052_ttp_detail
			 WHERE c4052_ttp_link_date >= TO_DATE (p_from_date, 'MM/DD/YYYY')
			   AND c4052_ttp_link_date <= TO_DATE (p_to_date, 'MM/DD/YYYY');

		gm_ttp_archive;

		DELETE FROM my_temp_list;

		--- Backup data table t250_inventory_lock which is not reference in demand sheet table
		INSERT INTO my_temp_list
					(my_temp_txn_id)
			SELECT c250_inventory_lock_id
			  FROM t250_inventory_lock
			 WHERE c250_lock_date >= TO_DATE (p_from_date, 'MM/DD/YYYY')
			   AND c250_lock_date <= TO_DATE (p_to_date, 'MM/DD/YYYY');

		gm_inventory_lock_archive;
	END gm_demand_sheet_master_backup;

	/*****************************************************************************
		Description : Procedure to backup demand sheet record in the temp table and
		delete backed record
		******************************************************************************/
	PROCEDURE gm_demand_sheet_archive
	AS
	BEGIN
		---backup data in  table t4041_demand_sheet_mapping
		INSERT INTO t4041_ds_mapping_back
					(c4041_demand_sheet_map_id, c4040_demand_sheet_id, c901_ref_type, c4041_ref_id, c901_action_type)
			SELECT c4041_demand_sheet_map_id, c4040_demand_sheet_id, c901_ref_type, c4041_ref_id, c901_action_type
			  FROM t4041_demand_sheet_mapping
			 WHERE c4040_demand_sheet_id IN (SELECT my_temp_txn_id
											   FROM my_temp_key_value);

		-- Delete backed record in	table t4041_demand_sheet_mapping
		DELETE FROM t4041_demand_sheet_mapping
			  WHERE c4040_demand_sheet_id IN (SELECT my_temp_txn_id
												FROM my_temp_key_value);

		---backup data in  table t4042_demand_sheet_detail
		INSERT INTO t4042_demand_sheet_detail_back
					(c4042_demand_sheet_detail_id, c4040_demand_sheet_id, c4042_ref_id, c4042_period, c4042_qty
				   , c4042_history_fl, c4042_updated_by, c901_type, c205_part_number_id, c4042_updated_date
				   , c205_parent_part_num_id, c4042_parent_ref_id, c4042_parent_qty_needed)
			SELECT c4042_demand_sheet_detail_id, c4040_demand_sheet_id, c4042_ref_id, c4042_period, ROUND(c4042_qty,0)
				 , c4042_history_fl, c4042_updated_by, c901_type, c205_part_number_id, c4042_updated_date
				 , c205_parent_part_num_id, c4042_parent_ref_id, ROUND(c4042_parent_qty_needed,0)
			  FROM t4042_demand_sheet_detail
			 WHERE c4040_demand_sheet_id IN (SELECT my_temp_txn_id
											   FROM my_temp_key_value);

		-- Delete backed record in	table t4042_demand_sheet_detail
		DELETE FROM t4042_demand_sheet_detail
			  WHERE c4040_demand_sheet_id IN (SELECT my_temp_txn_id
												FROM my_temp_key_value);

		---backup data in  table t4043_demand_sheet_growth
		INSERT INTO t4043_demand_sheet_growth_back
					(c4043_demand_sheet_growth_id, c4040_demand_sheet_id, c4030_demand_growth_id, c901_ref_type
				   , c4043_value, c4043_start_date, c4043_end_date)
			SELECT c4043_demand_sheet_growth_id, c4040_demand_sheet_id, c4030_demand_growth_id, c901_ref_type
				 , c4043_value, c4043_start_date, c4043_end_date
			  FROM t4043_demand_sheet_growth
			 WHERE c4040_demand_sheet_id IN (SELECT my_temp_txn_id
											   FROM my_temp_key_value);

		-- Delete backed record in	table t4043_demand_sheet_growth
		DELETE FROM t4043_demand_sheet_growth
			  WHERE c4040_demand_sheet_id IN (SELECT my_temp_txn_id
												FROM my_temp_key_value);

		---backup data in  table t4044_demand_sheet_request
		INSERT INTO t4044_ds_request_back
					(c4044_demand_sheet_request_id, c4040_demand_sheet_id, c520_request_id, c901_source_type
				   , c4044_lock_status_fl)
			SELECT c4044_demand_sheet_request_id, c4040_demand_sheet_id, c520_request_id, c901_source_type
				 , c4044_lock_status_fl
			  FROM t4044_demand_sheet_request
			 WHERE c4040_demand_sheet_id IN (SELECT my_temp_txn_id
											   FROM my_temp_key_value);

		-- Delete backed record in	table t4044_demand_sheet_request
		DELETE FROM t4044_demand_sheet_request
			  WHERE c4040_demand_sheet_id IN (SELECT my_temp_txn_id
												FROM my_temp_key_value);

		---backup data in  table t4045_demand_sheet_assoc
		INSERT INTO t4045_demand_sheet_assoc_back
					(c4045_demand_sheet_assoc_id, c4040_demand_sheet_id, c4020_demand_master_assoc_id, c901_status
				   , c4045_demand_period_dt, c4045_last_updated_by, c4045_last_updated_date)
			SELECT c4045_demand_sheet_assoc_id, c4040_demand_sheet_id, c4020_demand_master_assoc_id, c901_status
				 , c4045_demand_period_dt, c4045_last_updated_by, c4045_last_updated_date
			  FROM t4045_demand_sheet_assoc
			 WHERE c4040_demand_sheet_id IN (SELECT my_temp_txn_id
											   FROM my_temp_key_value);

		-- Delete backed record in	table t4045_demand_sheet_assoc
		DELETE FROM t4045_demand_sheet_assoc
			  WHERE c4040_demand_sheet_id IN (SELECT my_temp_txn_id
												FROM my_temp_key_value);

		---backup data in  table t4040_demand_sheet
		INSERT INTO t4040_demand_sheet_back
					(c4040_demand_sheet_id, c4020_demand_master_id, c4040_demand_period_dt, c4040_load_dt
				   , c4040_demand_period, c901_status, c4040_void_fl, c4040_approved_by, c4040_forecast_period
				   , c4040_approved_date, c4040_last_updated_by, c4052_ttp_detail_id, c4040_last_updated_date
				   , c4040_primary_user, c4040_request_period, c901_demand_type, c250_inventory_lock_id)
			SELECT c4040_demand_sheet_id, c4020_demand_master_id, c4040_demand_period_dt, c4040_load_dt
				 , c4040_demand_period, c901_status, c4040_void_fl, c4040_approved_by, c4040_forecast_period
				 , c4040_approved_date, c4040_last_updated_by, c4052_ttp_detail_id, c4040_last_updated_date
				 , c4040_primary_user, c4040_request_period, c901_demand_type, c250_inventory_lock_id
			  FROM t4040_demand_sheet
			 WHERE c4040_demand_sheet_id IN (SELECT my_temp_txn_id
											   FROM my_temp_key_value);

		-- Delete backed record in	table t4040_demand_sheet
		DELETE FROM t4040_demand_sheet
			  WHERE c4040_demand_sheet_id IN (SELECT my_temp_txn_id
												FROM my_temp_key_value);
	END gm_demand_sheet_archive;

	/*****************************************************************************
		Description : Procedure to backup TTP Detail record in the temp table and
		delete backed record
		******************************************************************************/
	PROCEDURE gm_ttp_archive
	AS
	BEGIN
		---backup data in  table t4053_ttp_part_detail
		INSERT INTO t4053_ttp_part_detail_back
					(c4053_ttp_part_id, c4052_ttp_detail_id, c205_part_number_id, c4053_qty, c4053_history_fl
				   , c4053_created_by, c4053_created_date, c4053_last_updated_by, c4053_last_updated_date
				   , c4053_cost_price)
			SELECT c4053_ttp_part_id, c4052_ttp_detail_id, c205_part_number_id, c4053_qty, c4053_history_fl
				 , c4053_created_by, c4053_created_date, c4053_last_updated_by, c4053_last_updated_date
				 , c4053_cost_price
			  FROM t4053_ttp_part_detail
			 WHERE c4052_ttp_detail_id IN (SELECT my_temp_txn_key
											 FROM my_temp_key_value) OR c4052_ttp_detail_id IN (SELECT my_temp_txn_id
																								  FROM my_temp_list);

		-- Delete backed record in	table t4053_ttp_part_detail
		DELETE FROM t4053_ttp_part_detail
			  WHERE c4052_ttp_detail_id IN (SELECT my_temp_txn_key
											  FROM my_temp_key_value) OR c4052_ttp_detail_id IN (SELECT my_temp_txn_id
																								   FROM my_temp_list);

		---backup data in  table t4052_ttp_detail
		INSERT INTO t4052_ttp_detail_back
					(c4052_ttp_detail_id, c4050_ttp_id, c4052_ttp_link_date, c4052_forecast_period, c4052_void_fl
				   , c901_status, c4052_created_by, c4052_created_date, c4052_last_updated_by, c4052_last_updated_date)
			SELECT c4052_ttp_detail_id, c4050_ttp_id, c4052_ttp_link_date, c4052_forecast_period, c4052_void_fl
				 , c901_status, c4052_created_by, c4052_created_date, c4052_last_updated_by, c4052_last_updated_date
			  FROM t4052_ttp_detail
			 WHERE c4052_ttp_detail_id IN (SELECT my_temp_txn_key
											 FROM my_temp_key_value) OR c4052_ttp_detail_id IN (SELECT my_temp_txn_id
																								  FROM my_temp_list);

		-- Delete TTP and TTP detail record
		DELETE FROM t4052_ttp_detail
			  WHERE c4052_ttp_detail_id IN (SELECT my_temp_txn_key
											  FROM my_temp_key_value) OR c4052_ttp_detail_id IN (SELECT my_temp_txn_id
																								   FROM my_temp_list);
	END gm_ttp_archive;

	/*****************************************************************************
	Description : Procedure to backup and delete Inventory Lock record in the temp table
	******************************************************************************/
	PROCEDURE gm_inventory_lock_archive
	AS
	BEGIN
		------------------------request lock-------------------------

		---backup data in  table t253c_request_lock
		INSERT INTO t253c_request_lock_back
					(c253c_request_lock_id, c250_inventory_lock_id, c520_request_id, c520_request_date
				   , c520_required_date, c901_request_source, c520_request_txn_id, c207_set_id, c520_request_for
				   , c520_request_to, c901_request_by_type, c520_request_by, c901_purpose, c901_ship_to
				   , c520_ship_to_id, c520_master_request_id, c520_status_fl, c520_void_fl, c520_last_updated_by
				   , c520_last_updated_date)
			SELECT c253c_request_lock_id, c250_inventory_lock_id, c520_request_id, c520_request_date
				 , c520_required_date, c901_request_source, c520_request_txn_id, c207_set_id, c520_request_for
				 , c520_request_to, c901_request_by_type, c520_request_by, c901_purpose, c901_ship_to, c520_ship_to_id
				 , c520_master_request_id, c520_status_fl, c520_void_fl, c520_last_updated_by, c520_last_updated_date
			  FROM t253c_request_lock
			 WHERE c250_inventory_lock_id IN (SELECT my_temp_txn_value
												FROM my_temp_key_value)
				OR c250_inventory_lock_id IN (SELECT my_temp_txn_id
												FROM my_temp_list);

		-- Delete backed record in	table t253c_request_lock
		DELETE FROM t253c_request_lock
			  WHERE c250_inventory_lock_id IN (SELECT my_temp_txn_value
												 FROM my_temp_key_value)
				 OR c250_inventory_lock_id IN (SELECT my_temp_txn_id
												 FROM my_temp_list);

		---backup data in  table t253d_request_detail_lock
		INSERT INTO t253d_request_detail_lock_back
					(c253d_request_detail_lock_id, c250_inventory_lock_id, c520_request_id, c205_part_number_id
				   , c521_qty)
			SELECT c253d_request_detail_lock_id, c250_inventory_lock_id, c520_request_id, c205_part_number_id, c521_qty
			  FROM t253d_request_detail_lock
			 WHERE c250_inventory_lock_id IN (SELECT my_temp_txn_value
												FROM my_temp_key_value)
				OR c250_inventory_lock_id IN (SELECT my_temp_txn_id
												FROM my_temp_list);

		-- Delete backed record in	table t253d_request_detail_lock
		DELETE FROM t253d_request_detail_lock
			  WHERE c250_inventory_lock_id IN (SELECT my_temp_txn_value
												 FROM my_temp_key_value)
				 OR c250_inventory_lock_id IN (SELECT my_temp_txn_id
												 FROM my_temp_list);

		------------------------consignment lock-------------------------
		---backup data in  table t253a_consignment_lock
		INSERT INTO t253a_consignment_lock_back
					(c253a_consignment_lock_id, c250_inventory_lock_id, c504_consignment_id, c701_distributor_id
				   , c504_status_fl, c504_void_fl, c704_account_id, c207_set_id, c504_type, c504_verify_fl
				   , c504_verified_date, c504_verified_by, c520_request_id, c504_master_consignment_id
				   , c504_last_updated_by, c504_last_updated_date, c901_location_type)
			SELECT c253a_consignment_lock_id, c250_inventory_lock_id, c504_consignment_id, c701_distributor_id
				 , c504_status_fl, c504_void_fl, c704_account_id, c207_set_id, c504_type, c504_verify_fl
				 , c504_verified_date, c504_verified_by, c520_request_id, c504_master_consignment_id
				 , c504_last_updated_by, c504_last_updated_date, c901_location_type
			  FROM t253a_consignment_lock
			 WHERE c250_inventory_lock_id IN (SELECT my_temp_txn_value
												FROM my_temp_key_value)
				OR c250_inventory_lock_id IN (SELECT my_temp_txn_id
												FROM my_temp_list);

		-- Delete backed record in	table t253a_consignment_lock
		DELETE FROM t253a_consignment_lock
			  WHERE c250_inventory_lock_id IN (SELECT my_temp_txn_value
												 FROM my_temp_key_value)
				 OR c250_inventory_lock_id IN (SELECT my_temp_txn_id
												 FROM my_temp_list);

		---backup data in  table t253b_item_consignment_lock
		INSERT INTO t253b_item_cons_lock_back
					(c253b_item_consignment_lock_id, c504_consignment_id, c250_inventory_lock_id, c205_part_number_id
				   , c505_item_qty)
			SELECT c253b_item_consignment_lock_id, c504_consignment_id, c250_inventory_lock_id, c205_part_number_id
				 , c505_item_qty
			  FROM t253b_item_consignment_lock
			 WHERE c250_inventory_lock_id IN (SELECT my_temp_txn_value
												FROM my_temp_key_value)
				OR c250_inventory_lock_id IN (SELECT my_temp_txn_id
												FROM my_temp_list);

		-- Delete backed record in	table t253b_item_consignment_lock
		DELETE FROM t253b_item_consignment_lock
			  WHERE c250_inventory_lock_id IN (SELECT my_temp_txn_value
												 FROM my_temp_key_value)
				 OR c250_inventory_lock_id IN (SELECT my_temp_txn_id
												 FROM my_temp_list);

------------------------order lock-------------------------
---backup data in  table t253e_order_lock
		INSERT INTO t253e_order_lock_back
					(c253e_order_lock_id, c250_inventory_lock_id, c501_order_id, c501_status_fl, c501_void_fl
				   , c501_last_updated_by, c501_last_updated_date, c901_order_type, c501_parent_order_id
				   , c501_total_cost, c501_ship_to, c501_ship_to_id, c501_ship_cost, c501_update_inv_fl
				   , c503_invoice_id, c506_rma_id, c501_customer_po, c704_account_id, c501_order_date
				   , c703_sales_rep_id, c501_shipping_date, c501_order_date_time)
			SELECT c253e_order_lock_id, c250_inventory_lock_id, c501_order_id, c501_status_fl, c501_void_fl
				 , c501_last_updated_by, c501_last_updated_date, c901_order_type, c501_parent_order_id, c501_total_cost
				 , c501_ship_to, c501_ship_to_id, c501_ship_cost, c501_update_inv_fl, c503_invoice_id, c506_rma_id
				 , c501_customer_po, c704_account_id, c501_order_date, c703_sales_rep_id, c501_shipping_date
				 , c501_order_date_time
			  FROM t253e_order_lock
			 WHERE c250_inventory_lock_id IN (SELECT my_temp_txn_value
												FROM my_temp_key_value)
				OR c250_inventory_lock_id IN (SELECT my_temp_txn_id
												FROM my_temp_list);

		-- Delete backed record in	table t253e_order_lock
		DELETE FROM t253e_order_lock
			  WHERE c250_inventory_lock_id IN (SELECT my_temp_txn_value
												 FROM my_temp_key_value)
				 OR c250_inventory_lock_id IN (SELECT my_temp_txn_id
												 FROM my_temp_list);

		---backup data in  table t253f_item_order_lock
		INSERT INTO t253f_item_order_lock_back
					(c253f_item_order_lock_id, c250_inventory_lock_id, c501_order_id, c205_part_number_id
				   , c502_item_qty, c502_control_number, c502_item_price, c502_void_fl, c502_item_seq_no, c901_type
				   , c502_construct_fl)
			SELECT c253f_item_order_lock_id, c250_inventory_lock_id, c501_order_id, c205_part_number_id, c502_item_qty
				 , c502_control_number, c502_item_price, c502_void_fl, c502_item_seq_no, c901_type, c502_construct_fl
			  FROM t253f_item_order_lock
			 WHERE c250_inventory_lock_id IN (SELECT my_temp_txn_value
												FROM my_temp_key_value)
				OR c250_inventory_lock_id IN (SELECT my_temp_txn_id
												FROM my_temp_list);

		-- Delete backed record in	table t253f_item_order_lock
		DELETE FROM t253f_item_order_lock
			  WHERE c250_inventory_lock_id IN (SELECT my_temp_txn_value
												 FROM my_temp_key_value)
				 OR c250_inventory_lock_id IN (SELECT my_temp_txn_id
												 FROM my_temp_list);

		------------------------inventory lock-------------------------

		---backup data in  table t251_inventory_lock_detail
		INSERT INTO t251_inv_lock_detail_back
					(c251_inventory_lock_detail_id, c250_inventory_lock_id, c205_part_number_id, c251_qty, c901_type)
			SELECT c251_inventory_lock_detail_id, c250_inventory_lock_id, c205_part_number_id, c251_qty, c901_type
			  FROM t251_inventory_lock_detail
			 WHERE c250_inventory_lock_id IN (SELECT my_temp_txn_value
												FROM my_temp_key_value)
				OR c250_inventory_lock_id IN (SELECT my_temp_txn_id
												FROM my_temp_list);

		-- Delete backed record in	table t251_inventory_lock_detail
		DELETE FROM t251_inventory_lock_detail
			  WHERE c250_inventory_lock_id IN (SELECT my_temp_txn_value
												 FROM my_temp_key_value)
				 OR c250_inventory_lock_id IN (SELECT my_temp_txn_id
												 FROM my_temp_list);

		---backup data in  table t252_inventory_lock_ref
		INSERT INTO t252_inv_lock_ref_back
					(c252_lock_ref_id, c250_inventory_lock_id, c901_ref_type, c252_ref_id)
			SELECT c252_lock_ref_id, c250_inventory_lock_id, c901_ref_type, c252_ref_id
			  FROM t252_inventory_lock_ref
			 WHERE c250_inventory_lock_id IN (SELECT my_temp_txn_value
												FROM my_temp_key_value)
				OR c250_inventory_lock_id IN (SELECT my_temp_txn_id
												FROM my_temp_list);

		-- Delete backed record in	table t252_inventory_lock_ref
		DELETE FROM t252_inventory_lock_ref
			  WHERE c250_inventory_lock_id IN (SELECT my_temp_txn_value
												 FROM my_temp_key_value)
				 OR c250_inventory_lock_id IN (SELECT my_temp_txn_id
												 FROM my_temp_list);

		---backup data in  table t254_inventory_txn_log
		INSERT INTO t254_inventory_txn_log_back
					(c254_inventory_txn_log_id, c250_inventory_lock_id, c254_txn_id, c901_txn_type, c901_action
				   , c254_last_updated_by, c254_last_updated_date)
			SELECT c254_inventory_txn_log_id, c250_inventory_lock_id, c254_txn_id, c901_txn_type, c901_action
				 , c254_last_updated_by, c254_last_updated_date
			  FROM t254_inventory_txn_log
			 WHERE c250_inventory_lock_id IN (SELECT my_temp_txn_value
												FROM my_temp_key_value)
				OR c250_inventory_lock_id IN (SELECT my_temp_txn_id
												FROM my_temp_list);

		-- Delete backed record in	table t254_inventory_txn_log
		DELETE FROM t254_inventory_txn_log
			  WHERE c250_inventory_lock_id IN (SELECT my_temp_txn_value
												 FROM my_temp_key_value)
				 OR c250_inventory_lock_id IN (SELECT my_temp_txn_id
												 FROM my_temp_list);

		---backup data in  table t250_inventory_lock
		INSERT INTO t250_inventory_lock_back
					(c250_inventory_lock_id, c250_lock_date, c901_lock_type, c250_void_fl)
			SELECT c250_inventory_lock_id, c250_lock_date, c901_lock_type, c250_void_fl
			  FROM t250_inventory_lock
			 WHERE c250_inventory_lock_id IN (SELECT my_temp_txn_value
												FROM my_temp_key_value)
				OR c250_inventory_lock_id IN (SELECT my_temp_txn_id
												FROM my_temp_list);

		-- Delete Inventory Lock record
		--	gm_inventory_lock_delete (v_inventory_lockid);
		DELETE FROM t250_inventory_lock
			  WHERE c250_inventory_lock_id IN (SELECT my_temp_txn_value
												 FROM my_temp_key_value)
				 OR c250_inventory_lock_id IN (SELECT my_temp_txn_id
												 FROM my_temp_list);
	END gm_inventory_lock_archive;
END gm_pkg_cm_order_planning_back;
/
