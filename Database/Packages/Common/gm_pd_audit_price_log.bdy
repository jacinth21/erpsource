--@"C:\Database\Packages\Common\gm_pd_audit_price_log.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pd_audit_price_log
IS
   /*******************************************************
   * Description : Procedure to fetch required part/Group price history Info
   * Author      : Elango
   *******************************************************/
--
   PROCEDURE gm_fch_price_history (
      p_ref_id       IN       t942_audit_price_log.C942_REF_ID%TYPE,
      p_audit_id     IN       t940_audit_trail.c940_audit_trail_id%TYPE,
      p_reqhistory   OUT      TYPES.cursor_type
   )
   AS
   V_COMPANY_ID NUMBER;
   BEGIN
	   
	  SELECT get_compid_frm_cntx() INTO V_COMPANY_ID   FROM dual;
	  
      OPEN p_reqhistory
       FOR 
          SELECT DECODE (T942.C940_AUDIT_TRAIL_ID,
          		 1019, GET_PARTNUM_DESC (T942.C942_REF_ID),
			     1041, NVL (GET_GROUP_NAME (T942.C942_REF_ID),	GET_PARTNUM_DESC (T942.C942_REF_ID)), 
			     1030,  DECODE ( GET_PARTNUM_DESC (T942.C942_REF_ID), ' ',GET_GROUP_NAME (T942.C942_REF_ID) , GET_PARTNUM_DESC (T942.C942_REF_ID))) NAME,
			     GET_CODE_NAME (T942.C901_TYPE) TYPE, 
			     T942.C942_VALUE VALUE, 
			     T942.C942_COMMENTS COMMENTS , 
			     GET_USER_NAME (T942.C942_UPDATED_BY) UPDATED_BY, 
			     T942.C942_UPDATED_DATE UPDATED_DATE 
		   FROM T942_AUDIT_PRICE_LOG T942
		  WHERE T942.C940_AUDIT_TRAIL_ID = p_audit_id
		    AND T942.C1900_COMPANY_ID = V_COMPANY_ID
		    AND T942.C942_REF_ID         = p_ref_id
		ORDER BY T942.C901_TYPE DESC, T942.C942_AUDIT_PRICE_LOG_ID DESC;  --The T942.C942_UPDATED_DATE column is changed into allowing NULL values. so the order by is changed T942.C942_AUDIT_PRICE_LOG_ID. 

   END gm_fch_price_history;
   
    /*****************************************************
   * Description : Procedure to fetch common log details
   * Author      : Elango
   *******************************************************/
--
   PROCEDURE gm_fch_common_log_details (
      p_ref_ids       IN       t902_log.C902_REF_ID%TYPE,
      p_ref_type      IN       t902_log.C902_TYPE%TYPE,
      p_log_details   OUT      TYPES.cursor_type
   )
   AS
      v_datefmt     VARCHAR2 (20);
   BEGIN
	 
	 SELECT get_compdtfmt_frm_cntx() INTO v_datefmt   FROM dual;

	  my_context.set_my_inlist_ctx (p_ref_ids);
		
      OPEN p_log_details
       FOR 
		       
       	SELECT T101.C101_USER_F_NAME || ' '|| T101.C101_USER_L_NAME CREATEDBY,
		       		t907.C907_CREATED_DATE CREATEDDATE,
		  			t907.c907_COMMENTS COMMENTS,t501.c501_parent_order_id ID ,
		  			get_code_name (t907.c901_cancel_cd) COMMENTTYPE,
		  			t907.C907_cancel_LOG_ID LOGID,T907.C907_VOID_FL VOIDFL
		FROM t907_cancel_log t907,T101_USER t101,
		  		( SELECT c501_order_id,c501_parent_order_id
		  		  FROM t501_order
		  		  WHERE c501_parent_order_id IN (SELECT token FROM v_in_list)
		  		  AND   c501_parent_order_id not in (select c501_order_id from t501_order where c501_order_id in(SELECT token FROM v_in_list))
		  		  AND c501_void_fl IS NOT NULL
		  		) t501
		WHERE c907_ref_id        = t501.c501_order_id
		AND t907.C907_CREATED_BY = t101.C101_USER_ID
		AND t907.C901_TYPE       = p_ref_type
		AND t907.C907_VOID_FL   IS NULL
		ORDER BY t907.C907_CREATED_DATE DESC; 

   END gm_fch_common_log_details;
   
END gm_pd_audit_price_log;
/
