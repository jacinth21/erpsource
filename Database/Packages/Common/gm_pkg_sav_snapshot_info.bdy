/* Formatted on 05/14/2018 10:18 (Formatter Plus v4.8.0) */
/* @ "C:\database\Packages\Common\gm_pkg_sav_snapshot_info.pkg" */
CREATE OR REPLACE
PACKAGE body gm_pkg_sav_snapshot_info
IS
    --
    /*******************************************************
    * Author  : ppandiyan
    * Description: This Procedure is used to save
    * the transaction attributes into the corresponding 
    * transaction attribute tables
    *******************************************************/
    --
PROCEDURE gm_sav_snapshot_info (
        p_rule_id           IN   t906_rules.c906_rule_id%TYPE ,
        p_trans_id          IN   VARCHAR2,
        p_rule_grp_id       IN   t906_rules.c906_rule_grp_id%TYPE ,
        p_company_id        IN   t1900_company.c1900_company_id%TYPE,
        p_txn_type          IN   VARCHAR2,
        p_user_id           IN   VARCHAR2
)
AS
    v_rule_value 	t906_rules.c906_rule_value%TYPE;
    v_con_attr_id  T504D_CONSIGNMENT_ATTRIBUTE.C504D_CONSIGNMENT_ATTRIBUTE_ID%type;
    v_attr_value   VARCHAR2(100);
    v_strlen	   NUMBER;
    v_string	   VARCHAR2(300);
	v_substring    VARCHAR2(300);
	v_attr_type NUMBER;
	v_rule_string VARCHAR2(100);
BEGIN	
	--getting the snapshot rule value 
	--for example RULE ID=ORDER, RULE_GRP_ID=SHANPSHOT, 
	--RULE_VALUE = GMDRUGLOCALLIC,GMDRUGEXPORTLIC and so on
	BEGIN
	 SELECT c906_rule_value
            INTO v_rule_value
    FROM t906_rules
    WHERE c906_rule_id   = p_rule_id
    AND c906_rule_grp_id = p_rule_grp_id
    AND C1900_COMPANY_ID =p_company_id
    AND C906_VOID_FL    IS NULL ;
    EXCEPTION
		WHEN NO_DATA_FOUND
			THEN
				 v_rule_value := NULL;
					          
			 END;

 v_strlen := NVL (LENGTH (v_rule_value), 0);   
 v_string := v_rule_value || ',';
  IF v_strlen > 0
	THEN  
		 WHILE INSTR (v_string, ',') <> 0   
		  LOOP
			  v_substring := SUBSTR (v_string, 1, INSTR (v_string, ',') - 1);
			  v_string	:= SUBSTR (v_string, INSTR (v_string, ',') + 1);

			  IF p_txn_type = '1200'--getting rule value for orders and inserting into attribute table
				THEN  
					BEGIN
					  SELECT c906_rule_value
					           INTO v_attr_value
					   FROM t906_rules
					    WHERE c906_rule_id   = v_substring
					    AND c906_rule_grp_id = 'ORDER_SNAPSHOT'
					    AND C1900_COMPANY_ID =p_company_id
					   AND C906_VOID_FL    IS NULL ;
					   	  EXCEPTION
					         WHEN NO_DATA_FOUND
					         THEN
					            v_attr_value := NULL;
					          
					      END;
					   --getting the code id to save the attribute type by passing rule value
					    SELECT get_code_id(v_substring,'SNPSHT') INTO v_attr_type FROM DUAL;
					    --to save in the order attribute table
					     gm_pkg_cm_order_attribute.gm_sav_order_attribute (p_trans_id,
                                                              v_attr_type,
                                                              v_attr_value,
                                                              null,
                                                              p_user_id
                                                             );
					   

			       ELSIF  p_txn_type = '1221'--getting rule value for consignments and inserting into attribute table
			       THEN
			                 SELECT s504d_CONSIGNMENT_ATTRIBUTE_ID.NEXTVAL INTO v_con_attr_id FROM DUAL;
			       	BEGIN
				       SELECT c906_rule_value
						           INTO v_attr_value
						   FROM t906_rules
						    WHERE c906_rule_id   = v_substring
						    AND c906_rule_grp_id = 'CONSIGNMENT_SNAPSHOT'
						    AND C1900_COMPANY_ID =p_company_id
						   AND C906_VOID_FL    IS NULL ;
						   	EXCEPTION
					         WHEN NO_DATA_FOUND
					         THEN
					            v_attr_value := NULL;
					          
					      END;
					       --getting the code id to save the attribute type by passing rule value
						    SELECT get_code_id(v_substring,'SNPSHT') INTO v_attr_type FROM DUAL;
						  --to save in the order attribute table  
			       			gm_pkg_op_consignment.gm_sav_consign_attribute(v_con_attr_id,
                                                              p_trans_id,
                                                              v_attr_type,
                                                              v_attr_value,
                                                              p_user_id
                                                             );
			       
			         ELSIF  p_txn_type = 'REQUEST_TXN'--getting rule value for consignments and inserting into attribute table
			       THEN
			               --  SELECT s504d_CONSIGNMENT_ATTRIBUTE_ID.NEXTVAL INTO v_con_attr_id FROM DUAL;
			       	BEGIN
				       SELECT c906_rule_value
						           INTO v_attr_value
						   FROM t906_rules
						    WHERE c906_rule_id   = v_substring
						    AND c906_rule_grp_id = 'REQUEST_SNAPSHOT'
						    AND C1900_COMPANY_ID = p_company_id
						   AND C906_VOID_FL    IS NULL ;
						   	EXCEPTION
					         WHEN NO_DATA_FOUND
					         THEN
					            v_attr_value := NULL;
					          
					      END;
					       --getting the code id to save the attribute type by passing rule value
						    SELECT get_code_id(v_substring,'SNPSHT') INTO v_attr_type FROM DUAL;
						    v_rule_string :=  v_attr_type ||'^'|| v_attr_value ||'|';

						  --to save in the order attribute table  
			       			gm_pkg_op_request_master.gm_sav_request_attribute(p_trans_id,
                                                              v_rule_string,
                                                              p_user_id
                                                             );
			       
			       
			       
			       
			       END IF;
		                     
		  END LOOP;
  
  END IF;

 END gm_sav_snapshot_info;
END gm_pkg_sav_snapshot_info;
/