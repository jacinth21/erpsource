/* Formatted on 2009/09/25 14:22 (Formatter Plus v4.8.0) */
--@"c:\database\packages\common\gm_pkg_cm_address.bdy"

CREATE OR REPLACE PACKAGE BODY gm_pkg_cm_address
IS
/******************************************************************
   * Description : Procedure to get the address info for a specific party
   ****************************************************************/
	PROCEDURE gm_cm_fch_addressinfo (
		p_partyid		 IN 	  t101_party.c101_party_id%TYPE
	  , p_partyinfocur	 OUT	  TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_partyinfocur
		 FOR
			 SELECT   c106_address_id ID, get_code_name (c901_address_type) addresstype
					, c106_add1 || ' ' || c106_add2 billadd, c106_city billcity, get_code_name (c901_state) state
					, get_code_name (c901_country) country, c106_zip_code zipcode, c106_seq_no preference
					, DECODE (UPPER (c106_inactive_fl), 'Y', 'Inactive', 'Active') inactivefl
					, get_code_name(c901_carrier) prefcarrier
					, NVL (c106_primary_fl, 'N') primaryfl,c106_add1 addr1,c106_add2 addr2 --Code Added here to Resolve Bug-3679.To load Address1 and Address2 in jsp. 
					, c106_BILL_NAME billname
				 FROM t106_address
				WHERE c101_party_id = p_partyid AND c106_void_fl IS NULL
			 ORDER BY primaryfl DESC, preference;
	END gm_cm_fch_addressinfo;

		--
	/********************************************************************************
	  * Description : Procedure to get the specific address info for a specific party
	  ********************************************************************************/
	PROCEDURE gm_cm_fch_editaddressinfo (
		p_partyaddressid   IN		t106_address.c106_address_id%TYPE
	  , p_partyinfocur	   OUT		TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_partyinfocur
		 FOR
			 SELECT c106_address_id addid, c901_address_type addresstype, c106_add1 billadd1, c106_add2 billadd2
				  , c106_city billcity, c901_state state, c901_country country, c106_zip_code zipcode
				  , c106_seq_no preference, DECODE (c106_inactive_fl, 'Y', 'on', 'off') inactivefl
				  , DECODE (c106_primary_fl, 'Y', 'on', 'off') primaryfl
				  , c901_carrier carrier     -- Default Preferred Carrier value for a Rep
				  , get_txncount_for_addr(p_partyaddressid) addrcount --to get the no. of transactions associated with the address
				  , c106_BILL_NAME billname
			   FROM t106_address
			  WHERE c106_address_id = p_partyaddressid AND c106_void_fl IS NULL;
	END gm_cm_fch_editaddressinfo;

	--

	/*******************************************************
	  * Description : Procedure to save party address info
	  * Parameters	:		1. p_partyId
						2. p_addressType
						3. p_addLine1
						4. p_addLine2
						5. p_city
						6. p_state
						7. p_country
						8. p_zipcode
						9. p_preference
						10. p_userId
	  *
	  *******************************************************/
	PROCEDURE gm_cm_sav_addressinfo (
		p_partyid		IN		 t101_party.c101_party_id%TYPE
	  , p_addressid 	IN OUT	 t106_address.c106_address_id%TYPE
	  , p_addresstype	IN		 t106_address.c901_address_type%TYPE
	  , p_addline1		IN		 t106_address.c106_add1%TYPE
	  , p_addline2		IN		 t106_address.c106_add2%TYPE
	  , p_city			IN		 t106_address.c106_city%TYPE
	  , p_state 		IN		 t106_address.c901_state%TYPE
	  , p_country		IN		 t106_address.c901_country%TYPE
	  , p_zipcode		IN		 t106_address.c106_zip_code%TYPE
	  , p_preference	IN		 t106_address.c106_seq_no%TYPE
	  , p_userid		IN		 t106_address.c106_created_by%TYPE
	  , p_inactivefl	IN		 t106_address.c106_inactive_fl%TYPE
	  , p_primaryfl 	IN		 t106_address.c106_primary_fl%TYPE
	  , p_prefCarrier 	IN		 t106_address.c901_carrier%TYPE
	  , p_billname 	    IN		 t106_address.c106_BILL_NAME%TYPE
	)
	AS
		v_address_id   t106_address.c106_address_id%TYPE;
		v_count 	   NUMBER;
		v_primaryfl    t106_address.c106_primary_fl%TYPE;
		--to be deleted later
		v_party_type   VARCHAR2 (10);
		v_shipcount    NUMBER;
		v_reccount	   NUMBER := 0;
		v_primarycnt  NUMBER := 0;
		v_txn_addr_count	NUMBER := 0;
		v_address		CLOB;
		v_email_fl		t106_address.c106_inactive_fl%TYPE;
	BEGIN
	-- to be deleted later
		--
		BEGIN
		--Below select query is to get the inactive flag, to know whether it is already inactivated or not
		SELECT c106_inactive_fl
	       INTO v_email_fl
	       FROM t106_address
	      WHERE c106_address_id = p_addressid
	        AND c106_void_fl   IS NULL;
	    EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            v_email_fl := 'N';
        END;
		
		v_primaryfl := p_primaryfl;

			IF p_addline2 IS NULL
			THEN
				SELECT COUNT (1)
				  INTO v_reccount
				  FROM t106_address
				 WHERE c106_address_id = p_addressid
				   AND c101_party_id = p_partyid
				   AND c106_void_fl IS NULL
				   AND c106_add1 = p_addline1
				   -- AND c106_add2 = p_addline2
				   AND c106_city = p_city
				   AND c901_state = p_state
				   AND c901_country = p_country
				   AND c106_zip_code = p_zipcode;
			--AND c106_primary_fl ! = p_primaryfl;
			ELSE
				SELECT COUNT (1)
				  INTO v_reccount
				  FROM t106_address
				 WHERE c106_address_id = p_addressid
				   AND c101_party_id = p_partyid
				   AND c106_void_fl IS NULL
				   AND c106_add1 = p_addline1
				   AND c106_add2 = p_addline2
				   AND c106_city = p_city
				   AND c901_state = p_state
				   AND c901_country = p_country
				   AND c106_zip_code = p_zipcode;
			--AND c106_primary_fl ! = p_primaryfl;
			END IF;		
		
		IF v_primaryfl = 'Y'
		THEN
			SELECT COUNT(1) INTO v_primarycnt
			  FROM t106_address 
			 WHERE c106_address_id = p_addressid AND c106_void_fl IS NULL AND c106_primary_fl = 'Y';
			 
			IF p_inactivefl = 'Y' AND v_primarycnt > 0
			THEN
				raise_application_error ('-20752', ''); --There has to be at least one active primary address available in the system.
			ELSIF p_inactivefl = 'Y' AND p_primaryfl = 'Y'
			THEN
				raise_application_error ('-20756', ''); --You cannot create new inactive primary address.
			END IF;
			
			UPDATE t106_address
			   SET c106_primary_fl = ''
			 WHERE c101_party_id = p_partyid AND c106_void_fl IS NULL AND c106_primary_fl = 'Y';
		END IF;

		UPDATE t106_address
		   SET c901_address_type = p_addresstype
			 , c106_add1 = p_addline1
			 , c106_add2 = p_addline2
			 , c106_city = p_city
			 , c901_state = p_state
			 , c901_country = p_country
			 , c106_zip_code = p_zipcode
			 , c106_seq_no = p_preference
			 , c106_last_updated_by = p_userid
			 , c106_last_updated_date = CURRENT_DATE
			 , c106_inactive_fl = p_inactivefl
			 , c106_primary_fl = p_primaryfl
			 , c901_carrier = p_prefCarrier     -- Updating Preferred Carrier value for a Rep -- PMT-4359
			 , c106_BILL_NAME = p_billname
		 WHERE c106_address_id = p_addressid AND c101_party_id = p_partyid AND c106_void_fl IS NULL;

		IF (SQL%ROWCOUNT = 0)
		THEN
			SELECT s106_address.NEXTVAL
			  INTO p_addressid
			  FROM DUAL;

			SELECT COUNT (1)
			  INTO v_count
			  FROM t106_address
			 WHERE c101_party_id = p_partyid AND c106_void_fl IS NULL;

			IF v_count = 0
			THEN
				v_primaryfl := 'Y';
			END IF;

			INSERT INTO t106_address
						(c106_address_id, c101_party_id, c901_address_type, c106_add1, c106_add2, c106_city
					   , c901_state, c901_country, c106_zip_code, c106_seq_no, c106_created_by, c106_created_date
					   , c106_inactive_fl, c106_primary_fl , c901_carrier     -- Inserting Preferred Carrier value for a New Rep -- PMT-4359
					   , c106_BILL_NAME
						)
				 VALUES (p_addressid, p_partyid, p_addresstype, p_addline1, p_addline2, p_city
					   , p_state, p_country, p_zipcode, p_preference, p_userid, CURRENT_DATE
					   , p_inactivefl, v_primaryfl , p_prefCarrier   -- Inserting Preferred Carrier value for a New Rep -- PMT-4359
					   , p_billname
						);
		ELSE   --if t106_address get updated
			SELECT COUNT (1)
			  INTO v_shipcount
			  FROM t907_shipping_info
			 WHERE c106_address_id = p_addressid AND c907_status_fl = 40 AND c907_void_fl IS NULL;

			IF (v_shipcount > 0 AND v_reccount = 0)
			THEN
				raise_application_error ('-20757', '');
			END IF;
		END IF;

		--below code will be removed later after the shipping proj is done
		IF v_primaryfl = 'Y'
		THEN
			SELECT c901_party_type
			  INTO v_party_type
			  FROM t101_party
			 WHERE c101_party_id = p_partyid;

			IF v_party_type = '7005'
			THEN
				UPDATE t703_sales_rep
				   SET c703_ship_add1 = p_addline1
					 , c703_ship_add2 = p_addline2
					 , c703_ship_city = p_city
					 , c703_ship_state = p_state
					 , c703_ship_country = p_country
					 , c703_ship_zip_code = p_zipcode
					 , c703_last_updated_by = p_userid
					 , c703_last_updated_date = CURRENT_DATE
				 WHERE c101_party_id = p_partyid;
			END IF;
		END IF;
	--
	
	v_txn_addr_count := get_txncount_for_addr(p_addressid);-- To get the no. of transactions associated with the address
	
	-- If inactivating an address and any transactions associated with the address, need to send a mail
	-- v_email_fl is to check whether the address is newly inactivated or not
	-- no need to send mail, if already inactivated, value is getting from JSP[hActiveFlag]
	IF p_inactivefl = 'Y' AND v_txn_addr_count > 0 AND (NVL(v_email_fl,'N') != 'Y')
	THEN
		v_address := p_addline1;
		-- For fetching the transaction informations of the address for sending the mail
        gm_cm_fch_txn_info (p_addressid,  v_address,  p_userid);
	END IF;
	
	
	END gm_cm_sav_addressinfo;

	 /******************************************************************
	* Description : Procedure to get all the addresses for a specific party
	****************************************************************/
	PROCEDURE gm_fch_repaddress (
		p_repid 	   IN		t703_sales_rep.c703_sales_rep_id%TYPE
	  , p_inactive_fl  IN       t106_address.c106_inactive_fl%TYPE
	  , p_repinfocur   OUT		TYPES.cursor_type
	)
	AS
		v_partyid	   t101_party.c101_party_id%TYPE;
	BEGIN
		IF (p_repid != '' OR p_repid != 0)
		THEN
			SELECT c101_party_id
			  INTO v_partyid
			  FROM t703_sales_rep
			 WHERE c703_sales_rep_id = p_repid;
		END IF;

		OPEN p_repinfocur
		 FOR
			 SELECT   c106_address_id ID
					, SUBSTR (get_code_name (c901_address_type) || '-' || c106_add1 || ' ' || c106_add2 || ' '
							  || c106_city
							, 0
							, 50
							 ) NAME
				 FROM t106_address
				WHERE c101_party_id = v_partyid AND c106_void_fl IS NULL
				  AND NVL(c106_inactive_fl,'-9999') != NVL(p_inactive_fl,NVL(c106_inactive_fl,'-9999'))
			 ORDER BY c106_primary_fl;
	END gm_fch_repaddress;
	
	/******************************************************************
	* Description : Procedure to get all the addresses for a specific party
	****************************************************************/
	PROCEDURE gm_fch_empaddress (
		p_empid 	   IN		t101_user.c101_user_id%TYPE
	  , p_inactive_fl  IN       t106_address.c106_inactive_fl%TYPE
	  , p_empinfocur   OUT		TYPES.cursor_type
	)
	AS
		v_partyid	   t101_party.c101_party_id%TYPE;
	BEGIN
		IF (p_empid != '' OR p_empid != 0)
		THEN
			SELECT c101_party_id INTO v_partyid   FROM t101_user
			 WHERE c901_user_type IN (300)  AND 
			 c901_user_status = 311 AND 
			 c101_user_id=p_empid;
		END IF;

		OPEN p_empinfocur
		 FOR
			 SELECT   c106_address_id ID
					, SUBSTR (get_code_name (c901_address_type) || '-' || c106_add1 || ' ' || c106_add2 || ' '
							  || c106_city
							, 0
							, 50
							 ) NAME
				 FROM t106_address
				WHERE c101_party_id = v_partyid AND c106_void_fl IS NULL
				  AND NVL(c106_inactive_fl,'-9999') != NVL(p_inactive_fl,NVL(c106_inactive_fl,'-9999'))
			 ORDER BY c106_primary_fl;
	END gm_fch_empaddress;
	
	
	/*******************************************************
	 * Author     : Dhana Reddy
	 * Decription : procedure to validate the Rep address
	 ******************************************************/
	PROCEDURE gm_validate_rep_addr (
        p_addr_id IN t106_address.c106_address_id%TYPE,
        p_inactive_fl OUT t106_address.c106_inactive_fl%TYPE)
	AS
	BEGIN
		BEGIN
	     SELECT c106_inactive_fl
	       INTO p_inactive_fl
	       FROM t106_address
	      WHERE c106_address_id = p_addr_id
	        AND c106_void_fl   IS NULL;
	    EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            p_inactive_fl := NULL;
       END;
	END gm_validate_rep_addr;
	
	
	/*******************************************************************************************
	 * Author     : Harinadh Reddy
	 * Decription : procedure to fetch default preferred Carrier for the Sales Rep and Account
	 *******************************************************************************************/
	PROCEDURE gm_fch_preferred_carrier (
        p_repacc_id 	IN t703_sales_rep.c703_sales_rep_id%TYPE,
        p_shipto_id 	IN t907_shipping_info.c901_ship_to%TYPE,
        p_addr_id   	IN t106_address.c106_address_id%TYPE,
        p_acct_id   	IN t704_account.c704_account_id%TYPE,
        p_pref_carrier  OUT t106_address.c901_carrier%TYPE,
        p_company_id    IN t1900_company.c1900_company_id%TYPE DEFAULT NULL)
	AS
		v_address_id t106_address.c106_address_id%TYPE;
		v_party_id		T704_ACCOUNT.C101_PARTY_ID%TYPE;
		v_company_id    t1900_company.c1900_company_id%TYPE;
	BEGIN					
		
		--Getting company id from context.
	    SELECT NVL(p_company_id,get_compid_frm_cntx())
		  INTO v_company_id 
		  FROM dual;
		
		IF p_shipto_id = 4121 THEN -- 4121 Sales Rep
			 IF  p_addr_id IS NULL THEN
			 	BEGIN 
			 		 SELECT t106.c106_address_id INTO v_address_id
	    		  	  FROM t106_address t106 , t703_sales_rep t703
                     WHERE t106.c101_party_id = t703.c101_party_id 
	    		 	   AND t703.c703_sales_rep_id = p_repacc_id
	    		   	   AND t106.c106_primary_fl = 'Y' 
	    		   	   AND t106.c106_void_fl IS NULL
                       AND t703.c703_void_fl IS NULL;
	    		EXCEPTION 
	    			WHEN NO_DATA_FOUND THEN
	    				v_address_id := NULL;
	    		END;
	    	ELSE
	    	    v_address_id := p_addr_id;
			END IF;
			 
				BEGIN
					v_party_id := gm_pkg_op_ship_charge.get_party_id_for_carrier(p_acct_id);
				SELECT C901_SHIP_CARRIER
			    	  INTO p_pref_carrier
			    	  FROM T9080_SHIP_PARTY_PARAM 
			    	WHERE C101_PARTY_ID = v_party_id
			    		AND C9080_VOID_FL IS NULL;
				EXCEPTION
	     		WHEN NO_DATA_FOUND THEN
	     			BEGIN
						SELECT c901_carrier INTO p_pref_carrier 
			    	  	  FROM t106_address 
			    	 	 WHERE c106_address_id = v_address_id  
			    	   	   AND c106_void_fl IS NULL;
					EXCEPTION
		         		WHEN NO_DATA_FOUND THEN
		            		p_pref_carrier := NULL;  
		       END; 
           	
           END;
		ELSIF p_shipto_id = 4122 THEN  -- 4122 Hospital
			BEGIN 
				v_party_id := gm_pkg_op_ship_charge.get_party_id_for_carrier(p_repacc_id);
				SELECT C901_SHIP_CARRIER
			    	  INTO p_pref_carrier
			    	  FROM T9080_SHIP_PARTY_PARAM 
			    	WHERE C101_PARTY_ID = v_party_id
			    		AND C9080_VOID_FL IS NULL;
				EXCEPTION
         		WHEN NO_DATA_FOUND THEN
         			BEGIN
						SELECT c901_carrier INTO p_pref_carrier 
						  FROM t704_account 
						 WHERE c704_account_id = p_repacc_id 
						   AND c704_void_fl IS NULL 
						   AND c704_active_fl = 'Y';
					EXCEPTION
		         		WHEN NO_DATA_FOUND THEN
		            		p_pref_carrier := NULL;
		            
		       		END; 
	    	END;
	    ELSIF p_shipto_id = 4000642 THEN  -- 4000642 Shipping Account
	    	IF  p_addr_id IS NULL THEN -- if the address id from screen is null, get the primary address id
				BEGIN
					SELECT t106.c106_address_id INTO v_address_id
		    		  	  FROM t106_address t106 , t101_party t101
	                 WHERE t106.c101_party_id = t101.c101_party_id 
		    		 	   AND t101.c101_party_id = p_repacc_id
		    		   	   AND t106.c106_primary_fl = 'Y' 
		    		   	   AND t106.c106_void_fl IS NULL
	                       AND t101.c101_void_fl IS NULL;
				EXCEPTION 
	    			WHEN NO_DATA_FOUND THEN
	    				v_address_id := NULL;
	    		END;
	    	ELSE
	    	    v_address_id := p_addr_id;
			END IF;
			
			BEGIN
				v_party_id := gm_pkg_op_ship_charge.get_party_id_for_carrier(p_acct_id);
				SELECT C901_SHIP_CARRIER
			    	  INTO p_pref_carrier
			    	  FROM T9080_SHIP_PARTY_PARAM 
			    	WHERE C101_PARTY_ID = v_party_id
			    		AND C9080_VOID_FL IS NULL;
				EXCEPTION
	     		WHEN NO_DATA_FOUND THEN
	     			BEGIN
						SELECT c901_carrier INTO p_pref_carrier 
			    	  	  FROM t106_address 
			    	 	 WHERE c106_address_id = v_address_id  
			    	   	   AND c106_void_fl IS NULL;
					EXCEPTION
		         		WHEN NO_DATA_FOUND THEN
		            		p_pref_carrier := NULL;  
		       END; 
            END;	    		
	    END IF;
	    
			IF p_pref_carrier IS NULL THEN
			
				v_party_id := gm_pkg_op_ship_charge.get_party_id_for_carrier(p_acct_id);
				BEGIN
				SELECT C901_SHIP_CARRIER
			    	  INTO p_pref_carrier
			    	  FROM T9080_SHIP_PARTY_PARAM 
			    	WHERE C101_PARTY_ID = v_party_id
			    		AND C9080_VOID_FL IS NULL;
				EXCEPTION
         		WHEN NO_DATA_FOUND THEN 
						SELECT GET_RULE_VALUE_BY_COMPANY('SHIP_CARRIER','SHIP_CARR_MODE_VAL',v_company_id) INTO p_pref_carrier FROM DUAL;	
				END; 
				    		
			END IF;			
	END gm_fch_preferred_carrier;	
	

/*******************************************************************************************
   * Description : Function to get the count of transactions associated with an address
   * Author      : arajan
*********************************************************************************************/

	FUNCTION get_txncount_for_addr (
		p_addressid   IN   t106_address.c106_address_id%TYPE
	)
		RETURN NUMBER
	IS
		v_count	NUMBER;
	BEGIN
		
		SELECT COUNT (1)
		  INTO v_count
	      FROM t907_shipping_info
		 WHERE c106_address_id = p_addressid
		   AND c907_status_fl < 40				--40: shipped
           AND c901_source NOT IN (50184, 50185)  --- invalid (50184:Request, 50185:ProductLoaner)
		   AND c907_void_fl IS NULL;
		
		RETURN v_count;
	END get_txncount_for_addr;
	
/***********************************************************************************************
   * Description : Procedure to get the informations of transactions associated with an address
   * Author      : arajan
************************************************************************************************/
	PROCEDURE gm_cm_fch_txn_info (
		p_addressid   IN   	t106_address.c106_address_id%TYPE
	  , p_addr  	  IN    CLOB
	  , p_user_id     IN	t106_address.c106_created_by%TYPE	
	)
	AS
		
		v_str				CLOB :='' ;
		v_col_str			VARCHAR2 (1000);
		v_end_row_str		VARCHAR2 (1000);
		
		CURSOR txn_cur 
		IS
		
		-- Order details [50261:Sales]
			SELECT t907.c907_ref_id txnid,
			  get_transaction_status(t501.c501_status_fl, '50261') status,
			  DECODE (t501.c501_last_updated_by, null, get_user_name(t501.c501_created_by), get_user_name(t501.c501_last_updated_by)) usernm,
			  DECODE (t501.c501_last_updated_by, null, get_code_name_alt(get_dept_id(t501.c501_created_by)), get_code_name_alt(get_dept_id(t501.c501_last_updated_by))) dept
			FROM t907_shipping_info t907,
			  t501_order t501
			WHERE t907.c907_ref_id   = t501.c501_order_id
			AND t907.c106_address_id = p_addressid
			AND t907.c907_status_fl  < 40
			AND t907.c901_source     = 50180 ---order
			AND t907.c907_void_fl   IS NULL
			AND t501.c501_void_fl   IS NULL
	
			UNION ALL

		-- Consignment and Inhouse Loaner Item details [4110:Consignment]					
			SELECT t907.c907_ref_id txnid,
			  get_transaction_status(t504.c504_status_fl, '4110') status,
			  DECODE (t504.c504_last_updated_by, null, get_user_name(t504.c504_created_by), get_user_name(t504.c504_last_updated_by)) usernm,
			  DECODE (t504.c504_last_updated_by, null, get_code_name_alt(get_dept_id(t504.c504_created_by)), get_code_name_alt(get_dept_id(t504.c504_last_updated_by))) dept
			FROM t907_shipping_info t907,
			  t504_consignment t504
			WHERE t907.c907_ref_id   = t504.c504_consignment_id
			AND t907.c106_address_id = p_addressid
			AND t907.c907_status_fl  < 40			-- Shipped
			AND t907.c901_source    IN ( 50181, 50186) ---50181 consignment, 50186 Inhouse Loaner Items
			AND t907.c907_void_fl   IS NULL
			AND t504.c504_void_fl   IS NULL
			
			UNION ALL
		
		-- Loaner request details					
			SELECT t907.C907_REF_ID txnid,
			  get_loaner_request_status(t526.c526_product_request_detail_id) status,-- need to get the status of request
			  DECODE (t526.c526_last_updated_by, null, DECODE(t907.c907_last_updated_by, null,get_user_name(t907.c907_created_by),get_user_name(t907.c907_last_updated_by)), get_user_name(t526.c526_last_updated_by)) usernm,
			  DECODE (t526.c526_last_updated_by, null, 
			  			DECODE(t907.c907_last_updated_by, null,get_code_name_alt(get_dept_id(t907.c907_created_by)),get_code_name_alt(get_dept_id(t907.c907_last_updated_by))) 
			  			,get_code_name_alt(get_dept_id(t526.c526_last_updated_by))) dept
			FROM t907_shipping_info t907,
			  t526_product_request_detail t526
			WHERE t907.c907_ref_id       = t526.c526_product_request_detail_id
			AND t907.c106_address_id     = p_addressid
			AND t907.c901_source         = 50182 ---Loaner
			AND t907.c907_status_fl      = 15    ---pending control
			AND t907.c907_void_fl       IS NULL
			AND t526.c526_void_fl       IS NULL
			AND t526.c526_status_fl NOT IN (30, 40 ) ---closed/cancelled
			
			UNION ALL

		-- Loaner details								
			SELECT t907.C907_REF_ID txnid,
			  gm_pkg_op_loaner.get_loaner_status(t504a.c504a_status_fl) status,
			  DECODE (t504a.c504a_last_updated_by, null, get_user_name(t504a.c504a_loaner_create_by), get_user_name(t504a.c504a_last_updated_by)) usernm,
			  DECODE (t504a.c504a_last_updated_by, null, get_code_name_alt(get_dept_id(t504a.c504a_loaner_create_by)), get_code_name_alt(get_dept_id(t504a.c504a_last_updated_by))) dept
			FROM t907_shipping_info t907,
			  t504a_consignment_loaner t504a
			WHERE t907.c907_ref_id   = t504a.c504_consignment_id
			AND t907.c106_address_id = p_addressid
			AND t907.c901_source     = 50182 	--Loaner
			AND t907.c907_status_fl  < 40    	--shipped
			AND t907.c907_void_fl   IS NULL
			AND t504a.c504a_void_fl IS NULL
			
			UNION ALL

		-- Loaner Extension								
			SELECT t907.c907_ref_id txnid,
				--Since the status of FGLE/IHLE is not updating for PIP and RFP, need to get the status from shipping table
			  DECODE(c907_status_fl,15,DECODE(t412.c412_status_fl,'2','Pending Control Number','3','Pending Verification')
            ,30,'Pending Shipping',33,'Packing in Progress',36,'Ready for Pickup',40,'Completed',NULL) status,
			  DECODE (t412.c412_last_updated_by, null, get_user_name(t412.c412_created_by), get_user_name(t412.c412_last_updated_by)) usernm,
			  DECODE (t412.c412_last_updated_by, null, get_code_name_alt(get_dept_id(t412.c412_created_by)), get_code_name_alt(get_dept_id(t412.c412_last_updated_by))) dept
			FROM t907_shipping_info t907,
			  t412_inhouse_transactions t412
			WHERE t907.c907_ref_id   = t412.c412_inhouse_trans_id
			AND t907.c106_address_id = p_addressid
			AND t907.c901_source     = 50183 	--Loaner extension
			AND t907.c907_status_fl  < 40 		--40:shipped
			AND t907.c907_void_fl   IS NULL
			AND t412.c412_void_fl   IS NULL ;
	
	BEGIN
		
			v_col_str := '<TR><TD>';
            v_end_row_str := '</TD><TD>';
        

			FOR curindex IN txn_cur
			LOOP
            -- below string is the transaction details in a table    
				v_str := v_str||v_col_str|| curindex.txnid ||v_end_row_str||curindex.status ||v_end_row_str||curindex.dept||'('||curindex.usernm||')'||'</TD></TR>';
			END LOOP;
			
			IF v_str IS NOT NULL
			THEN
				-- Below procedure is to send mail while inactivating an address which is associated with transaction
				gm_pkg_cm_address.gm_cm_inactive_addr_email(v_str , p_addr,  p_user_id);
			END IF;
			
	END gm_cm_fch_txn_info;
	

/***************************************************************************************************
	* This procedure is to send mail while inactivating an address which is associated with transaction
	* Author : arajan

***************************************************************************************************/
	PROCEDURE gm_cm_inactive_addr_email (
	p_str			IN		 CLOB
  , p_addr			IN		 CLOB
  , p_user_id 		IN	t106_address.c106_created_by%TYPE
	)
	AS		
		v_usrmail	VARCHAR2(200);
		v_to_mail	VARCHAR2(2000);
		v_mail_subject      VARCHAR2 (1000);
		v_table_str			VARCHAR2 (3000);
		v_mail_body         CLOB;
		v_inactivate_dt		VARCHAR2(20);
	BEGIN
		
		v_usrmail := get_user_emailid (p_user_id);
		v_to_mail 	:= get_rule_value ('INACTIVEADDR', 'EMAIL') || ',' || v_usrmail;
		v_inactivate_dt := to_char(CURRENT_DATE,NVL(GET_RULE_VALUE('DATEFMT','DATEFORMAT'),'MM/DD/YYYY'));
		v_mail_subject  := 'Address: "'||p_addr||'" inactivated by ' || get_user_name (p_user_id)|| ' on '||v_inactivate_dt;
		
		--below string is to crete the table header
		v_table_str   := '<TABLE style="border: 1px solid  #676767">'
                        || '<TR><TD width=150><b>Transaction Id</b></TD><TD width=200><b>Status</b></TD>'
                        || '<TD width=250><b>Department</b></TD></TR>';
		
		v_mail_body  :=
        '<style>TD{ FONT-SIZE: 11px; COLOR: #000000; FONT-FAMILY: verdana, arial, sans-serif;}</style><font face=arial size="2">';
        
		v_mail_body := v_mail_body||'Following transactions no longer show the address which is inactivated now. Please do necessary action.<br><br>'
						||v_table_str||p_str;
						
		--Call the common procedure to send mail
		gm_com_send_html_email (v_to_mail, v_mail_subject,'test', v_mail_body);-- Sending mail
		
	END gm_cm_inactive_addr_email;

/******************************************************************************************************************
  * This Procedure is called from webservice which will fetch the Adress Related Information,For Active Address ID. 
******************************************************************************************************************/
	PROCEDURE gm_fch_address_info(
    	p_token   IN t101a_user_token.c101a_token_id%TYPE,
    	p_langid  IN NUMBER,
    	p_page_no IN NUMBER,
    	p_uuid    IN t9150_device_info.c9150_device_uuid%TYPE,
    	p_address_cur OUT TYPES.CURSOR_TYPE )
	AS
  		v_deviceid t9150_device_info.c9150_device_info_id%TYPE;
  		v_count     NUMBER;
  		v_page_no   NUMBER;
  		v_page_size NUMBER;
  		v_start     NUMBER;
  		v_end       NUMBER;
  		v_party_id t101_user.c101_party_id%TYPE;
   		v_last_rec	t9151_device_sync_dtl.C9151_LAST_SYNC_REC%TYPE;   		
	BEGIN
  		--get the device info id based on user token
  		v_deviceid := get_device_id(p_token, p_uuid);
  		--get the user party id from the token
  		
  		v_page_no := p_page_no;
  		
  		-- get the paging details
  		gm_pkg_pdpc_prodcatrpt.gm_fch_paging_dtl('ADDRESS','PAGESIZE',v_page_no,v_start,v_end,v_page_size);
  		/*if updated groups available then add all groups into v_inlist and return count of v_inlist records*/
  		gm_pkg_pdpc_prodcatrpt.gm_fch_device_updates(v_deviceid,p_langid,4000522,v_page_no,v_count,v_last_rec); --4000522 - Address Sync Type
  		--If there is no data available for device then call below prc to add all groups.
  	IF v_count = 0 THEN
    		--insert published systems and systems which are part of rule group into my_temp_publish_sys temp table.
    		--gm_pkg_pdpc_prodcatrpt.gm_fch_publish_system(v_party_id);
   		 gm_pkg_cm_master_data_dtl.gm_fch_address_detail_tosync;
  	END IF;
  	v_page_no := p_page_no;
  OPEN p_address_cur 
  FOR 
  	SELECT COUNT (1) OVER () pagesize, result_count totalsize, v_page_no pageno, rwnum, CEIL (result_count / v_page_size) totalpages , 
  		t106.* 
  		FROM(
  				SELECT t106.*,ROWNUM rwnum,COUNT (1) OVER () result_count, MAX(DECODE(t106.addrid,v_last_rec,ROWNUM,NULL)) OVER() LASTREC
  				FROM(
    					SELECT t106.c101_party_id partyid,t106.c106_add1 addr1,  t106.c106_add2 addr2,
    						   t106.c106_address_id addrid,t106.c901_address_type addrtype,
    						   t106.c106_city city,t106.c901_state state,t106.c901_country country,
    						   t106.c106_zip_code zipcode,t106.c106_primary_fl primaryfl,t106.c106_void_fl voidfl,
    						   t106.c106_inactive_fl inactvfl,t106.c106_seq_no  preference
    					FROM t106_address t106,my_temp_prod_cat_info temp
						WHERE t106.C106_ADDRESS_ID= temp.ref_id
						ORDER BY t106.c106_address_id
						 
    				)t106
  			)t106 
  		WHERE rwnum BETWEEN NVL(LASTREC+1,DECODE(v_last_rec,NULL,v_start,v_start-1)) AND v_end;
	END gm_fch_address_info;

/********************************************************************************
* Description : Procedure to get all the addresses for a specific  party
*********************************************************************************/
	PROCEDURE gm_fch_ship_address (
		p_partyid 	   IN		t101_user.c101_user_id%TYPE
	  , p_inactive_fl  IN       t106_address.c106_inactive_fl%TYPE
	  , p_address      OUT		TYPES.cursor_type
	) 
	AS
	BEGIN
		 
		OPEN p_address
		 FOR
			 SELECT   c106_address_id ID
					, SUBSTR (get_code_name (c901_address_type) || '-' || c106_add1 || ' ' || c106_add2 || ' '
							  || c106_city
							, 0
							, 50
							 ) NAME
				 FROM t106_address
				WHERE c101_party_id = p_partyid AND c106_void_fl IS NULL
				  AND NVL(c106_inactive_fl,'-9999') != NVL(p_inactive_fl,NVL(c106_inactive_fl,'-9999'))
			 ORDER BY c106_primary_fl;
	END gm_fch_ship_address;	
	
	
END gm_pkg_cm_address;
/
