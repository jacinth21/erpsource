/* Formatted on 2009/02/13 13:57 (Formatter Plus v4.8.0) */
-- @"c:\Database\packages\Common\gm_pkg_cm_override.pkg";

CREATE OR REPLACE PACKAGE gm_pkg_cm_override
IS
--
/*******************************************************
 * Purpose: Package holds Override Procedures
 * Created By VPrasath
 *******************************************************/
--
/**************************************************************************
 * Purpose: This Procedure is used to call the
 * corrresponding procedures for the given override id
 *************************************************************************/
--
	PROCEDURE gm_cm_save_override_details (
		p_type		   IN	VARCHAR2
	  , p_ref_id	   IN	VARCHAR2
	  , p_new_value    IN	VARCHAR2
	  , p_ref_type	   IN	t4031_growth_details.c901_ref_type%TYPE
	  , p_no		   IN	VARCHAR2
	  , p_user_id	   IN	VARCHAR2
	  , p_ref_period   IN	VARCHAR2
	  , p_r_type	   IN	t4031_growth_details.c901_ref_type%TYPE
	  , part_type	   IN	VARCHAR2
	);

/**************************************************************************
 * Purpose: This Procedure is used to save the override
 * values for growth
 *************************************************************************/
--
	PROCEDURE gm_cm_save_override_growth (
		p_ref_id	   IN	VARCHAR2
	  , p_ref_period   IN	VARCHAR2
	  , p_no		   IN	VARCHAR2
	  , p_type		   IN	VARCHAR2
	  , p_new_value    IN	VARCHAR2
	  , p_ref_type	   IN	t4031_growth_details.c901_ref_type%TYPE
	  , p_user_id	   IN	VARCHAR2
	);

--
/**************************************************************************
 * Purpose: This Procedure is used to save the override for Demand sheet
 *************************************************************************/
--
	PROCEDURE gm_cm_save_override_sheet (
		p_ref_id	  IN   VARCHAR2
	  , p_new_value   IN   VARCHAR2
	  , p_type		  IN   t4031_growth_details.c901_ref_type%TYPE
	  , p_no		  IN   VARCHAR2
	  , p_user_id	  IN   VARCHAR2
	  , part_type	  IN   VARCHAR2
	);

--
/**************************************************************************
 * Purpose: This Procedure is used to save the override
 * values for values in the TTP Summary
 *************************************************************************/
--
	PROCEDURE gm_cm_save_override_ttp (
		p_ref_id	  IN   VARCHAR2
	  , p_new_value   IN   VARCHAR2
	  , p_user_id	  IN   VARCHAR2
	);

--
/**************************************************************************
 * Purpose: This Procedure is used to fetch the overriden
 * ref id for growth
 *************************************************************************/
--
	PROCEDURE gm_cm_fch_override_refid (
		p_ref_id		 IN 	  VARCHAR2
	  , p_ref_period	 IN 	  VARCHAR2
	  , p_no			 IN 	  VARCHAR2
	  , p_over_type_id	 IN 	  NUMBER
	  , p_dem_sheet_id	 IN 	  NUMBER
	  , p_part_type 	 IN 	  VARCHAR2
	  , p_userid		 IN 	  NUMBER
	  , p_updated_id	 OUT	  VARCHAR2
	);

--
/**************************************************************************
 * Purpose: This Procedure is used to fetch the overriden history
 *************************************************************************/
--
	PROCEDURE gm_cm_fch_override_history (
		p_ref_id	 IN 	  VARCHAR2
	  , p_trail_id	 IN 	  NUMBER
	  , p_data		 OUT	  TYPES.cursor_type
	);

--
/**************************************************************************
 * Purpose: This Procedure is used to save the override
 * values for Sales Groups in the Demand Sheet
 *************************************************************************/
--
	PROCEDURE gm_cm_save_override_group (
		p_ref_id	  IN   VARCHAR2
	  , p_new_value   IN   VARCHAR2
	  , p_user_id	  IN   VARCHAR2
	  , p_r_type	  IN   t4031_growth_details.c901_ref_type%TYPE
	  , p_ref_type	  IN   t4031_growth_details.c901_ref_type%TYPE
	);

--

	--
/**************************************************************************
 * Purpose: This Procedure is used to save the override
 * values for Sets (InHOuse and Consignment) in the Demand Sheet
 *************************************************************************/
--
	PROCEDURE gm_cm_save_override_set (
		p_ref_id	  IN   VARCHAR2
	  , p_new_value   IN   VARCHAR2
	  , p_user_id	  IN   VARCHAR2
	  , p_r_type	  IN   t4031_growth_details.c901_ref_type%TYPE
	  , p_ref_type	  IN   t4031_growth_details.c901_ref_type%TYPE
	);

--
--
/**************************************************************************
 * Purpose: This Procedure is used to save the override
 *			the sub parts
 *************************************************************************/
--
	PROCEDURE gm_cm_save_override_subpart (
		p_demand_sheet_detail_id   IN	t4042_demand_sheet_detail.c4042_demand_sheet_detail_id%TYPE
	);
--
END gm_pkg_cm_override;
/
