CREATE OR REPLACE PACKAGE BODY gm_pkg_in_transit_trans
IS

/******************************************************************
	  * Description : Procedure to save shipout consignment details
	  * Author		: AGILAN 
 ******************************************************************/
PROCEDURE gm_save_intransit_trans(
		p_consignid		IN		t504_consignment.c504_consignment_id%TYPE,
		p_userid        IN      t2023_part_company_mapping.c2023_updated_by%TYPE
	)
AS
	v_source_company_id			t1900_company.c1900_company_id%TYPE;
	v_company_id	        	t1900_company.c1900_company_id%TYPE;
	v_seq_id					T504E_CONSIGNMENT_IN_TRANS.C504E_CONSIGNMENT_IN_TRANS_ID%type;
	v_dist_id					t701_distributor.c701_distributor_id%TYPE;
	v_pnum_count                NUMBER;  
	v_request_id 				t520_request.c520_request_id%TYPE;
	v_request_comp_id			t520_request.c1900_company_id%TYPE;
	v_type						t504_consignment.c504_type%TYPE;
	
	CURSOR cur_consignment
		IS
		SELECT c205_part_number_id pnum
	  	  FROM t505_item_consignment
		 WHERE c504_consignment_id = p_consignid AND c505_control_number IS NOT NULL
	     GROUP BY c205_part_number_id;
BEGIN

	SELECT c701_distributor_id,c1900_company_id,c520_request_id,c504_type
	  INTO v_dist_id,v_source_company_id,v_request_id,v_type
	  FROM t504_consignment 
	 WHERE c504_consignment_id = p_consignid
	   AND c504_void_fl is null;
	--For Stock transfer get the company id from request 
	IF v_type = 106703 OR  v_type = 106704
	THEN
	
		SELECT c1900_company_id 
		  INTO v_company_id 
		  FROM t520_request 
		 WHERE c520_request_id = v_request_id
		   AND c520_void_fl is null;
	ELSE
		SELECT c1900_parent_company_id 
		  INTO v_company_id 
		  FROM t701_distributor 
		 WHERE c701_distributor_id = v_dist_id
		   AND c701_void_fl is null;
	END IF;
	   
    IF get_rule_value (v_company_id,'SHOW-IN-TRANSIT') IS NULL -- Check the company need Intransit transactions.
	THEN
		RETURN;
	END IF;
	
	SELECT s504e_consignment_in_trans.nextval
   	  INTO v_seq_id
   	  FROM DUAL; 
	  
	INSERT INTO T504E_CONSIGNMENT_IN_TRANS(C504E_CONSIGNMENT_IN_TRANS_ID,C504_CONSIGNMENT_ID,C1900_SOURCE_COMPANY_ID,C1900_COMPANY_ID)
		VALUES (v_seq_id,p_consignid,v_source_company_id,v_company_id);

   	FOR rad_val IN cur_consignment
	LOOP			
		SELECT COUNT(1) INTO v_pnum_count
		  FROM t2023_part_company_mapping  
		 WHERE c205_part_number_id = rad_val.pnum 
		   AND C1900_COMPANY_ID = v_company_id; 
		 
		IF (v_pnum_count = 0)
		THEN 
			INSERT INTO T2023_PART_COMPANY_MAPPING (C2023_PART_COMP_MAP_ID,C205_PART_NUMBER_ID,C901_PART_MAPPING_TYPE,C1900_COMPANY_ID,C2023_UPDATED_BY,C2023_UPDATED_DATE)
		    VALUES (S2023_PART_COMPANY_MAPPING.nextval,rad_val.pnum,105361,v_company_id,p_userid,current_date);		
		END IF;
	END LOOP; 	
	
END gm_save_intransit_trans;

/****************************************************************************************
	  * Description : Procedure to show details on consignment or set based on company id
	  * Author		: AGILAN 
 ****************************************************************************************/
PROCEDURE gm_fetch_intransit_trans(
		p_intrasitvalues   OUT	 TYPES.cursor_type
	)
AS
	v_trans_comp_id		t1900_company.c1900_company_id%TYPE;	
	v_trnas_plant_id    T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
BEGIN
	SELECT get_compid_frm_cntx(), get_plantid_frm_cntx() 
      INTO v_trans_comp_id , v_trnas_plant_id 
      FROM DUAL; 
          
	 OPEN p_intrasitvalues FOR
			
	 SELECT decode(t504.c701_distributor_id,'',get_account_name(t504.c704_account_id),get_distributor_name(t504.c701_distributor_id)) dname,
	        get_company_name(t504e.c1900_source_company_id) scid,t504e.c504_consignment_id cid,
	        decode(t504.c504_type,'106703','Stock Transfer','106704','Stock Transfer',decode(t504.c504_inhouse_purpose,4000097,get_code_name(4000097),'ICT Consignment')) stype
	        ,t504e.c1900_company_id comid,t504.c207_set_id setid,get_set_name(t504.c207_set_id) sname,t504.c504_ship_date cdate,
      		DECODE(t504.c504_type,'106703','C','106704','C',nvl(get_rule_value('CON_TYPE',t504.c504_type),'C')) con_type,
      		t504.c504_type ctype,'Y' consfl
       FROM T504E_CONSIGNMENT_IN_TRANS t504e,T504_CONSIGNMENT t504 
      WHERE t504e.c504_consignment_id = t504.c504_consignment_id 
      	AND t504e.c901_status is null
      	AND t504.c504_void_fl is null
      	AND t504e.c504e_void_fl is null
        AND t504e.c1900_company_id = v_trans_comp_id 
        ORDER BY t504.c504_created_date;
       		
END gm_fetch_intransit_trans;

/***************************************************************
	  * Description : Procedure to fetch Intra values for action
	  * Author		: AGILAN 
 ***************************************************************/
PROCEDURE gm_intrasit_fch_codelookup_val(
	p_intrasitcodevalues   OUT	 TYPES.cursor_type,
	p_intrasitcodevaluesRegular   OUT	 TYPES.cursor_type
)
AS
BEGIN
	OPEN p_intrasitcodevalues     FOR
	SELECT c901_code_id CODEID, c901_code_nm CODENAME 
	  FROM t901_code_lookup 
	 WHERE c901_code_grp   = 'INTRA'
	   AND c902_code_nm_alt ='Part'
	   AND c901_active_fl   ='1'
	   AND c901_void_fl is null;
 
 OPEN p_intrasitcodevaluesRegular  FOR
		SELECT c901_code_id CODEID, c901_code_nm CODENAME 
		  FROM t901_code_lookup 
		 WHERE c901_code_grp   = 'INTRA'
		   AND c902_code_nm_alt ='Set'
		   AND c901_active_fl   ='1'
		   AND c901_void_fl is null;

END gm_intrasit_fch_codelookup_val;

/***************************************************************
	  * Description : Procedure to create inhouse transaction from ICT Consignment's
	  * Author		: Bala 
 ***************************************************************/
PROCEDURE gm_intrasit_sav_inhouse_txn(
	p_consignid		IN		t504_consignment.c504_consignment_id%TYPE,
	p_purpose		IN		t412_inhouse_transactions.c412_inhouse_purpose%TYPE,
	p_comments		IN		t412_inhouse_transactions.c412_comments%TYPE,
	p_fg_input_str	IN	    clob,
	p_qn_input_str	IN	    clob,
	p_rp_input_str	IN	    clob,
	p_userid		IN		t412_inhouse_transactions.c412_created_by%TYPE,
	p_out_txns      OUT     varchar2
)
AS
	v_fg_input_str	    	clob := p_fg_input_str;
	v_qn_input_str	    	clob := p_qn_input_str;
	v_rp_input_str	    	clob := p_rp_input_str;
	v_type                  T901_CODE_LOOKUP.C901_CODE_ID%TYPE; 
	v_ship_to               T412_INHOUSE_TRANSACTIONS.C412_RELEASE_TO%TYPE;  
	v_ship_to_id            T412_INHOUSE_TRANSACTIONS.C412_RELEASE_TO_ID%TYPE;
	v_purpose               T504_CONSIGNMENT.C504_INHOUSE_PURPOSE%TYPE;
	v_transid	            T412_INHOUSE_TRANSACTIONS.C412_INHOUSE_TRANS_ID%TYPE;
	v_company_id	        t1900_company.c1900_company_id%TYPE;
	v_plant_id	            t5040_plant_master.c5040_plant_id%TYPE;
	v_fg_txn	    		VARCHAR2(4000);
	v_qn_txn	    		VARCHAR2(4000);
	v_rp_txn	    		VARCHAR2(4000);
	v_out_txn	    		VARCHAR2(4000);
	v_Count_Values			NUMBER;
    v_curr_trans_comp_id t1900_company.c1900_company_id%TYPE;
    v_curr_conv_rate T907_Currency_Conv.C907_Curr_Value%TYPE;
    v_cons_type  t504_consignment.C504_Type%TYPE;
    v_request_id t504_consignment.c520_request_id%TYPE;
			
BEGIN
	
	SELECT NVL(GET_COMPID_FRM_CNTX(),GET_RULE_VALUE('DEFAULT_COMPANY','ONEPORTAL')), NVL(GET_PLANTID_FRM_CNTX(),GET_RULE_VALUE('DEFAULT_PLANT','ONEPORTAL'))
	  INTO v_company_id, v_plant_id
	  FROM DUAL;
	  
	SELECT get_rule_value_by_company(v_company_id,'CURR_CONV_BY_TRANS',v_company_id) 
	  INTO v_curr_trans_comp_id 
      FROM DUAL;
	 
	SELECT COUNT (T412.C412_INHOUSE_TRANS_ID) vcount
	  INTO v_Count_Values
      FROM t412_inhouse_transactions T412
     WHERE C412_REF_ID = p_consignid
	   AND C412_TYPE IN ('26240358','26240359','26240360')
	   AND C412_VOID_FL IS NULL; 
		   
	IF (v_Count_Values <> 0)
		THEN
			raise_application_error (-20952, '');
		END IF;
		  
	SELECT c504_ship_to,c504_ship_to_id,c504_inhouse_purpose,C504_Type,c520_request_id
	  INTO v_ship_to,v_ship_to_id,v_purpose,v_cons_type,v_request_id
	  FROM t504_consignment 
	 WHERE C504_CONSIGNMENT_ID = p_consignid
	   AND c504_void_fl is null;
	   
	-- Check if the transaction is having currency conversion rate else throw error	, 40057 - ICT  
	IF (v_company_id = v_curr_trans_comp_id) AND v_cons_type = '40057'
	THEN
			v_curr_conv_rate := gm_pkg_cm_currency.gm_fch_trans_curr_value(p_consignid);
		
	END IF;
	 
	 
	IF v_fg_input_str is not null then
		SELECT GET_RULE_VALUE('106521','TRANS_TYPE') INTO v_type FROM DUAL;
		
		gm_pkg_op_request_master.gm_sav_inhouse_txn(p_comments,'3',v_purpose,v_type,p_consignid,v_cons_type,v_ship_to,v_ship_to_id,p_userid,v_transid);
		gm_pkg_in_transit_trans.gm_sav_inhouse_txn_string(v_transid,v_fg_input_str,p_userid,v_curr_conv_rate,p_consignid);-- To save In house trans item details  
		gm_pkg_allocation.gm_ins_invpick_assign_detail (26240407,v_transid,p_userid);-- To save the entry for device (PMT-21087)		
	
		v_out_txn := v_transid;
		
	END IF;
	
	IF v_qn_input_str is not null then
		SELECT GET_RULE_VALUE('106522','TRANS_TYPE') INTO v_type FROM DUAL;
	
		gm_pkg_op_request_master.gm_sav_inhouse_txn(p_comments,'3',v_purpose,v_type,p_consignid,v_cons_type,v_ship_to,v_ship_to_id,p_userid,v_transid);
		gm_pkg_in_transit_trans.gm_sav_inhouse_txn_string(v_transid,v_qn_input_str,p_userid,v_curr_conv_rate,p_consignid);		
		
		IF v_fg_input_str is not null then
			v_out_txn := v_out_txn || ', ' || v_transid;		
		ELSE
			v_out_txn := v_transid;
		END IF;
		
	
	END IF;
	
	IF v_rp_input_str is not null then
		SELECT GET_RULE_VALUE('106523','TRANS_TYPE') INTO v_type FROM DUAL;
	
		gm_pkg_op_request_master.gm_sav_inhouse_txn(p_comments,'3',v_purpose,v_type,p_consignid,v_cons_type,v_ship_to,v_ship_to_id,p_userid,v_transid);
		gm_pkg_in_transit_trans.gm_sav_inhouse_txn_string(v_transid,v_rp_input_str,p_userid,v_curr_conv_rate,p_consignid);		
		
		IF (v_fg_input_str is not null OR v_qn_input_str is not null) then
		v_out_txn := v_out_txn || ' & ' || v_transid;
		ELSE
			v_out_txn := v_transid;
		END IF;
		
	END IF;
	
		update t504e_consignment_in_trans set c901_status = '106520', c504e_received_by = p_userid,c504e_received_date=current_date ,
	           c5040_plant_id = v_plant_id, c504e_last_updated_by = p_userid,c504e_last_updated_date = current_date
	     where c504_consignment_id = p_consignid;
	     
	     -- update request status to transfered (60) for FG-ST nd BL-ST
	   	 IF (v_cons_type = '106703' OR v_cons_type = '106704')
	   	 THEN
	          UPDATE t520_request
				 SET c520_status_fl         = '60', 
				     c520_last_updated_by   = p_userid,
				     c520_last_updated_date = current_date
			   WHERE c520_request_id    = v_request_id;
		 END IF;
	     
	     p_out_txns := 'The following In-Transit transaction(s) have been created: '|| v_out_txn; 

END gm_intrasit_sav_inhouse_txn;

/***************************************************************
	  * Description : This Procedure is common procedure used to create the Inhouse Transaction details for the ICT
	  * Author		: Bala 
 ***************************************************************/
PROCEDURE gm_sav_inhouse_txn_string(
   		p_txnid  		 IN T412_INHOUSE_TRANSACTIONS.C412_INHOUSE_TRANS_ID%TYPE,
   		p_str	 		 IN	clob,
   		p_userid 		 IN	T412_INHOUSE_TRANSACTIONS.C412_CREATED_BY%TYPE,
   		p_curr_conv_rate IN	T907_Currency_Conv.C907_Curr_Value%TYPE,
   		p_consignid		 IN	t504_consignment.c504_consignment_id%TYPE
   )
   AS
   	v_string		clob := p_str;
   	v_substring		VARCHAR2(4000);
   	v_pnum      	T413_INHOUSE_TRANS_ITEMS.C205_PART_NUMBER_ID%TYPE;
    v_qty           T413_INHOUSE_TRANS_ITEMS.C413_ITEM_QTY%TYPE;
    v_cnum   		T413_INHOUSE_TRANS_ITEMS.C413_CONTROL_NUMBER%TYPE;			
    v_price			T413_INHOUSE_TRANS_ITEMS.C413_ITEM_PRICE%TYPE;
    v_company_id	t1900_company.c1900_company_id%TYPE;
    v_plant_id	    t5040_plant_master.c5040_plant_id%TYPE;
   
   BEGIN
	   
	SELECT NVL(GET_COMPID_FRM_CNTX(),GET_RULE_VALUE('DEFAULT_COMPANY','ONEPORTAL')), NVL(GET_PLANTID_FRM_CNTX(),GET_RULE_VALUE('DEFAULT_PLANT','ONEPORTAL'))
	  INTO v_company_id, v_plant_id
	  FROM DUAL;
	  
	   WHILE INSTR(v_string,'|') <> 0
	   LOOP
	   		v_substring := substr(v_string,0,INSTR(v_string,'|') - 1);
	   		v_string := substr(v_string,INSTR(v_string,'|') + 1);
	   		
	   		v_pnum	:= null;
	   		v_qty	:= null;
	   		v_cnum	:= null;
	   		
	   		v_pnum := substr(v_substring,0,INSTR(v_substring,'^') - 1); 
	   		v_substring := substr(v_substring,INSTR(v_substring,'^') + 1);
	   		v_cnum	:= substr(v_substring,0,INSTR(v_substring,'^') - 1);
	   		v_substring := substr(v_substring,INSTR(v_substring,'^') + 1);
	   		v_qty	:= substr(v_substring,0,INSTR(v_substring,'^') - 1);
	   		v_substring := substr(v_substring,INSTR(v_substring,'^') + 1);
	   		v_price	:=	v_substring;
	   		
	   		--v_price 	:= get_part_price (v_pnum, 'E');
	   		
            -- If the transaction has the currency conversion rate then convert the price with the same
			v_price := v_price * NVL(p_curr_conv_rate,1);	
	   		
	   		gm_pkg_op_request_master.gm_sav_inhouse_txn_dtl (
	   			p_txnid,
	   			v_pnum,
	   			TO_NUMBER(v_qty),
	   			v_cnum,
	   			v_price,
	   			NULL,
	   			p_userid
	   		);
	   		   		
		   	gm_pkg_in_transit_trans.gm_upd_part_price_mapping (v_pnum,v_price,p_userid,v_company_id);
	   		gm_pkg_in_transit_trans.gm_intrasit_tag_update (v_pnum,v_cnum,null,p_consignid,v_company_id,v_plant_id,p_userid);
	   		
	   END LOOP;
	  
		UPDATE t412_inhouse_transactions 
		   SET c412_last_updated_by   = p_userid
		     , c412_last_updated_date = CURRENT_DATE
		     , c1900_company_id = v_company_id
		 WHERE c412_inhouse_trans_id  = p_txnid;
		 
END gm_sav_inhouse_txn_string;
	
	
/***************************************************************
	  * Description : Procedure to save/create Built set from ICT Consignment's
	  * Author		: Bala 
 ***************************************************************/
PROCEDURE gm_intrasit_sav_build_set(
	p_consignid			IN		t504_consignment.c504_consignment_id%TYPE,
	p_setid         	IN  	t207_set_master.c207_set_id%TYPE,
	p_userid			IN		t412_inhouse_transactions.c412_created_by%TYPE,
    p_out_request_id	OUT	    t520_request.c520_request_id%TYPE,
	p_out_consign_id	OUT	    t504_consignment.c504_consignment_id%TYPE
    )
AS
	v_consignid             varchar2(100);
	v_us_time_zone          t901_code_lookup.c901_code_nm%TYPE;
    v_us_time_format        t901_code_lookup.c901_code_nm%TYPE;
    v_new_req_id   			t520_request.c520_request_id%TYPE;
	v_new_consign_id 		t504_consignment.c504_consignment_id%TYPE;
	v_company_id	        t1900_company.c1900_company_id%TYPE;
	v_plant_id	            t5040_plant_master.c5040_plant_id%TYPE;
	v_comp_cd 				t1900_company.c1900_company_cd%TYPE;
	v_src_cn 				t902_log.c902_comments%TYPE;
	v_distributor_id        varchar2 (100);
	p_message	            t902_log.c902_comments%TYPE;
BEGIN
		
	SELECT NVL(GET_COMPID_FRM_CNTX(),GET_RULE_VALUE('DEFAULT_COMPANY','ONEPORTAL')), NVL(GET_PLANTID_FRM_CNTX(),GET_RULE_VALUE('DEFAULT_PLANT','ONEPORTAL'))
	  INTO v_company_id, v_plant_id
	  FROM DUAL;
	
	SELECT GET_CODE_NAME(C901_TIMEZONE), GET_CODE_NAME(C901_DATE_FORMAT)
      INTO v_us_time_zone,v_us_time_format
      FROM t1900_company
     WHERE c1900_company_id = v_company_id;

	gm_pkg_in_transit_trans.gm_sav_initiate_set (	
									   ''
									 , TO_CHAR (TRUNC (CURRENT_DATE), v_us_time_format)
									 , '50618'
									 , ''
									 , p_setid
									 , 40021
									 , v_distributor_id
									 , 50626
									 , p_userid
									 , NULL
									 , NULL
									 , ''
									 , 15
									 , p_userid
									 , '50061'
                   					 , ''
                   					 , p_consignid
									 , v_new_req_id
									 , v_new_consign_id
									  );

						UPDATE t520_request
						   SET c520_status_fl = 20 ,C520_LAST_UPDATED_BY = p_userid , C520_LAST_UPDATED_DATE = CURRENT_DATE
						 WHERE c520_request_id = v_new_req_id
						   AND c1900_company_id= v_company_id;

						UPDATE t504_consignment
						   SET c504_status_fl = 2
								 , c504_verify_fl = 0
								 , c504_verified_date = NULL
								 , c504_verified_by = NULL
								 , c504_last_updated_by = p_userid
								 , c504_last_updated_date = CURRENT_DATE
								 , c504_comments = p_consignid	 -- comments
								 , c504_reprocess_id = p_consignid
								 , c504_update_inv_fl = NULL
						 WHERE c504_consignment_id = v_new_consign_id
						   AND c1900_company_id=v_company_id;
						
				         BEGIN						
					        SELECT get_company_code(c1900_company_id) INTO v_comp_cd
					        FROM t504_consignment
					        WHERE c504_consignment_id= p_consignid;
					
					        v_src_cn:='From '||v_comp_cd||': '||p_consignid;					   
					        --
					        GM_UPDATE_LOG(v_new_consign_id,v_src_cn,1220,p_userid,p_message); -- set build comments
					        GM_UPDATE_LOG(v_new_consign_id,v_new_req_id,1235,p_userid,p_message); -- Request comments
				            EXCEPTION WHEN NO_DATA_FOUND THEN
					        v_src_cn := NULL;
				         END;
				         
				         UPDATE t504e_consignment_in_trans set c901_status = '106520', c504e_received_by = p_userid,
				                c504e_received_date=current_date ,c5040_plant_id = v_plant_id, c504e_last_updated_by = p_userid,
				                c504e_last_updated_date = current_date
	     				  WHERE c504_consignment_id = p_consignid;	     				  
	     				    
	     
	         p_out_request_id := v_new_req_id;
	         p_out_consign_id := v_new_consign_id;
	   
END gm_intrasit_sav_build_set;

/***************************************************************
	  * Description : Procedure to save/create new Request and Consignment 
	  * for the ICT set 
	  * Author		: Bala 
 ***************************************************************/
PROCEDURE gm_sav_initiate_set (
		p_request_id		  IN	   t520_request.c520_request_id%TYPE
	  , p_required_date 	  IN	   VARCHAR2
	  , p_request_source	  IN	   t520_request.c901_request_source%TYPE
	  , p_request_txn_id	  IN	   t520_request.c520_request_txn_id%TYPE
	  , p_set_id			  IN	   t520_request.c207_set_id%TYPE
	  , p_request_for		  IN	   t520_request.c520_request_for%TYPE
	  , p_request_to		  IN	   t520_request.c520_request_to%TYPE
	  , p_request_by_type	  IN	   t520_request.c901_request_by_type%TYPE
	  , p_request_by		  IN	   t520_request.c520_request_by%TYPE
	  , p_ship_to			  IN	   t520_request.c901_ship_to%TYPE
	  , p_ship_to_id		  IN	   t520_request.c520_ship_to_id%TYPE
	  , p_master_request_id   IN	   t520_request.c520_master_request_id%TYPE
	  , p_status_fl 		  IN	   t520_request.c520_status_fl%TYPE
	  , p_userid			  IN	   t520_request.c520_created_by%TYPE
	  , p_purpose			  IN	   t520_request.c901_purpose%TYPE
	  , p_planned_ship_dt     IN       t520_request.c520_planned_ship_date%TYPE
	  , p_consignid		      IN	   t504_consignment.c504_consignment_id%TYPE
	  , p_out_request_id	  OUT	   t520_request.c520_request_id%TYPE
	  , p_out_consign_id	  OUT	   t504_consignment.c504_consignment_id%TYPE
	)
	AS
		v_partnum	   			t505_item_consignment.c205_part_number_id%TYPE;
		v_cntnum       			t505_item_consignment.C505_CONTROL_NUMBER%TYPE;
		v_price        			t505_item_consignment.C505_ITEM_PRICE%TYPE;
		v_qty          			t505_item_consignment.c505_item_qty%TYPE;		
		v504_type	   			t504_consignment.c504_type%TYPE; 
		v_account_id   			t520_request.c520_request_to%TYPE;		
		v_out_item_consign_id   t505_item_consignment.c505_item_consignment_id%TYPE;
		v_out_request_detail_id t521_request_detail.c521_request_detail_id%TYPE;
		v_request_id   			t520_request.c520_request_id%TYPE;
		v_required_date 		t520_request.c520_required_date%TYPE;
        v_comp_date_fmt  		varchar2(100);
        v_country_code 			varchar2(10);
        v_company_id			t1900_company.c1900_company_id%TYPE;
        v_plant_id	            t5040_plant_master.c5040_plant_id%TYPE;
        v_child_material_request_id t520_request.c520_request_id%TYPE;
        v_child_material_request_bool BOOLEAN := FALSE;
		v_consign_first_time_flag     BOOLEAN := FALSE;
		v_bo_request_id         t520_request.c520_request_id%TYPE;
		v_new_consign_id 		t504_consignment.c504_consignment_id%TYPE;
		v_bo_req_detail 		NUMBER := 0;
		v_request_bo_id   		t520_request.c520_request_id%TYPE;
		 boqty                 NUMBER :=0;
		 itmqty 				NUMBER :=0;
		
	
		-- 1. Get all the parts based on set and consignment.
		CURSOR cur_set_details
		IS
		SELECT t505.c205_part_number_id pnum, t505.c505_control_number cnum,t505.c505_item_qty qty, t505.c505_item_price partprice
		  FROM t504_consignment t504,t505_item_consignment t505 
		 WHERE t504.c207_set_id       = p_set_id
		   AND t504.c504_consignment_id = p_consignid
		   AND t504.c504_consignment_id = t505.c504_consignment_id
		   AND t504.c504_void_fl IS NULL
		   AND t505.c505_void_fl IS NULL
		FOR UPDATE;
		
	    CURSOR cur_save_set_initiate
		IS
		SELECT t520.c520_request_id bo_request_id
		  INTO v_bo_request_id
	 	  FROM t520_request t520, t504_consignment t504
		 WHERE (t520.c207_set_id =nvl( p_set_id,'-9999')  or t520.c207_set_id is null)	
		   AND t504.c504_consignment_id = p_consignid
		   AND t520.c520_master_request_id = t504.c520_request_id 
		   AND c520_void_fl IS NULL
		   AND C504_void_fl IS NULL;
		
		CURSOR  cur_save_setid_initiate
		IS		

		SELECT c205_part_number_id  pnum,sum(c208_set_qty) setqty
           FROM t208_set_details t208
          WHERE c207_set_id       = p_set_id
             AND t208.c208_set_qty <> 0
             AND t208.c208_inset_fl = 'Y'
             AND t208.c208_void_fl IS NULL group by c205_part_number_id;
          
      BEGIN
			
		SELECT C520_REQUEST_ID 
		  INTO v_request_bo_id
		  FROM t504_consignment 
		 WHERE c504_consignment_id = p_consignid
		   AND c504_void_fl IS NULL;
		    
		SELECT get_compdtfmt_frm_cntx() INTO v_comp_date_fmt FROM DUAL;
		
		SELECT NVL(GET_COMPID_FRM_CNTX(),GET_RULE_VALUE('DEFAULT_COMPANY','ONEPORTAL')), NVL(GET_PLANTID_FRM_CNTX(),GET_RULE_VALUE('DEFAULT_PLANT','ONEPORTAL'))
	      INTO v_company_id, v_plant_id
	      FROM DUAL;
		
	    v_country_code:= get_rule_value_by_company('CURRENT_DB','COUNTRYCODE',v_company_id);
		
		--	2. validation. If set id is NULL, throw Error
		IF (p_set_id IS NULL)
		THEN
			raise_application_error (-20915, '');	-- SET ID cant be null while initiating MR
		END IF;

		SELECT DECODE
				   (p_request_for
				  , 40021, 4110
				  , 40022, 4112
				  , p_request_for
				   )   -- we are not using the value 40021/4110 (Consignment) of DMDTP. 20021 - Sales. 40022 - InHouse
			 , DECODE (p_request_for, 40022, '01', NULL)
		  INTO v504_type
			 , v_account_id
		  FROM DUAL;

		  IF p_request_source != 4000122 THEN --( SET PAR )
				SELECT DECODE (p_required_date, NULL, TRUNC (CURRENT_DATE), TO_DATE (p_required_date,v_comp_date_fmt))
				  INTO v_required_date
				  FROM DUAL;
				  
				  -- The Required Date could be null only when the company date format is empty.				  
				  
				  IF v_required_date IS NULL AND p_request_source = 50616
				  THEN
				  	SELECT DECODE (p_required_date, NULL, TRUNC (CURRENT_DATE), TO_DATE (p_required_date,'MM/DD/YYYY'))
				  		INTO v_required_date
				  	FROM DUAL;
				  END IF;
		  END IF;
		  
		--	3. Creating MR since we have to create it in any case
		gm_pkg_op_request_master.gm_sav_request (p_request_id
											   , v_required_date											   
											   , p_request_source
											   , p_request_txn_id
											   , p_set_id
											   , p_request_for
											   , p_request_to
											   , p_request_by_type
											   , p_request_by
											   , p_ship_to
											   , p_ship_to_id
											   , p_master_request_id
											   , 15
											   , p_userid
											   , p_purpose
											   , p_out_request_id
											   , p_planned_ship_dt
												);
		v_request_id := p_out_request_id;			  
									
									
		IF p_request_to IS NULL AND p_request_source <> '50616'
		THEN
			gm_pkg_cm_shipping_trans.gm_sav_shipping_status_log (v_request_id, 50184, p_userid, 'Initiated');
		END IF;

		--
		FOR loop_set_details IN cur_set_details
		LOOP
			v_partnum	:= loop_set_details.pnum;
			v_cntnum	:= loop_set_details.cnum;
			v_qty       := loop_set_details.qty;
			v_price	    := loop_set_details.partprice;
						
			IF (NOT v_consign_first_time_flag)
			THEN
					gm_pkg_op_request_master.gm_sav_consignment (p_request_to
															   , NULL
															   , NULL
															   , NULL
															   , NULL
															   , '0'   -- initiated
															   , NULL
															   , v_account_id
															   , p_ship_to
															   , p_set_id
															   , p_ship_to_id
															   , NULL
															   , p_purpose
															   , v504_type
															   , NULL
															   , NULL
															   , NULL
															   , '1'   --Ship Req Flag
															   , '0'
															   , NULL
															   , NULL
															   , NULL
															   , NULL
															   , NULL
															   , p_out_request_id
															   , p_userid
															   , p_out_consign_id
																);
					v_child_material_request_bool := TRUE;
			
					IF p_request_to IS NULL AND p_request_source <> '50616'
					THEN
							gm_pkg_cm_shipping_trans.gm_sav_shipping_status_log (p_out_consign_id
																			   , 50181
																			   , p_userid
																			   , 'Initiated'
																				);
					END IF;		
			END IF;
			
			-- 5. If consignment is created then a child MR has to be created
		FOR loop_initiate_details IN cur_save_set_initiate
		LOOP	
			IF (v_child_material_request_bool) AND loop_initiate_details.bo_request_id IS NOT NULL
			THEN
				gm_pkg_op_request_master.gm_sav_request (NULL
													   , v_required_date
													   , p_request_source
													   , p_request_txn_id
													   , p_set_id
													   , p_request_for
													   , p_request_to
													   , p_request_by_type
													   , p_request_by
													   , p_ship_to
													   , p_ship_to_id
													   , p_out_request_id
													   , 10
													   , p_userid
													   , p_purpose
													   , v_child_material_request_id
														);
				v_child_material_request_bool := FALSE;
				v_request_id := v_child_material_request_id;

				IF p_request_to IS NULL AND p_request_source <> '50616'
				THEN
					gm_pkg_cm_shipping_trans.gm_sav_shipping_status_log (v_child_material_request_id
																	   , 50181
																	   , p_userid
																	   , 'Initiated'
																		);
				END IF;
			END IF;
END LOOP;
			gm_pkg_op_request_master.gm_sav_consignment_detail (NULL
																  , p_out_consign_id
																  , v_partnum
																  , v_cntnum
																  , v_qty
																  , v_price
																  , NULL
																  , NULL
																  , NULL
																  , v_out_item_consign_id
	  														      );

			   gm_pkg_in_transit_trans.gm_upd_part_price_mapping(v_partnum,v_price,p_userid,v_company_id);
			   gm_pkg_in_transit_trans.gm_intrasit_tag_update_ICT(v_partnum,v_cntnum,p_out_consign_id,p_consignid,'52098',v_company_id,v_plant_id,p_userid); -- 52098 Set Building Location Inventory
			   
				   
			   v_consign_first_time_flag := TRUE;
			   
		END LOOP;

		FOR loop_bo_details IN cur_save_setid_initiate
			LOOP
		BEGIN			                   
           	SELECT nvl(sum(c505_item_qty),0) INTO itmqty from t505_item_consignment 
           	WHERE c205_part_number_id= loop_bo_details.pnum 
           	AND c504_consignment_id = p_consignid
           	AND c505_void_fl IS NULL;
       	EXCEPTION WHEN NO_DATA_FOUND THEN
			itmqty := 0;
	    END;
           	boqty := loop_bo_details.setqty - itmqty;
    	IF (boqty >0)
	     THEN
					gm_pkg_op_request_master.gm_sav_request_detail (v_request_id															 
															  , loop_bo_details.pnum
															  , boqty
															  , v_out_request_detail_id
															   );
	END IF;
  END Loop;
		IF (p_out_consign_id IS NOT NULL)
		THEN
			--When ever the child is updated, the consignment table has to be updated.
			UPDATE t504_consignment
			   SET c504_last_updated_by   = p_userid
			     , c504_last_updated_date = CURRENT_DATE
			 WHERE c504_consignment_id = p_out_consign_id;	
		END IF;
	END gm_sav_initiate_set;
	
 /******************************************************************************
   * Description : Procedure to insert / update consignment item details
   * Author     : Bala
  ******************************************************************************/
   PROCEDURE gm_sav_consignment_detail (
      p_item_consignment_id      IN       t505_item_consignment.c505_item_consignment_id%TYPE,
      p_consignment_id           IN       t505_item_consignment.c504_consignment_id%TYPE,
      p_con_intrans_id           IN       t504e_consignment_in_trans.c504_consignment_id%TYPE,
      p_part_number_id           IN       t505_item_consignment.c205_part_number_id%TYPE,
      p_control_number           IN       t505_item_consignment.c505_control_number%TYPE,
      p_item_qty                 IN       t505_item_consignment.c505_item_qty%TYPE,      
      p_void_fl                  IN       t505_item_consignment.c505_void_fl%TYPE,
      p_type                     IN       t505_item_consignment.c901_type%TYPE,
      p_ref_id                   IN       t505_item_consignment.c505_ref_id%TYPE,
      p_out_item_consigment_id   OUT      t505_item_consignment.c505_item_consignment_id%TYPE
   )
   AS
   v_price              t505_item_consignment.c505_item_price%TYPE;
   v_old_price 		    t505_item_consignment.c505_item_price%TYPE;
   v_new_price 			t505_item_consignment.c505_item_price%TYPE;
      
   BEGIN      
   
      BEGIN
	      SELECT max(c505_item_price) 
	        INTO v_old_price
	        FROM t505_item_consignment
	       WHERE c504_consignment_id = p_con_intrans_id
	         AND C205_part_number_id = p_part_number_id
	         AND c505_void_fl is null; 
	  EXCEPTION
	  WHEN NO_DATA_FOUND THEN
	      v_price := 0;		
	  END;
	  --PC-2934: Unable to record lot # for Parts released from BO into a WIP CN it throws
	  			-- cannot insert price values to null.
	  BEGIN
	  	SELECT nvl(max(c505_item_price),0)
		  INTO v_new_price
		  FROM t505_item_consignment
	     WHERE c504_consignment_id = p_consignment_id
		   AND C205_part_number_id = p_part_number_id
		   AND c505_void_fl is null;
	  EXCEPTION
	  WHEN OTHERS THEN
	      v_new_price := 0;		
	  END;
	  
	  v_price := NVL(v_old_price,v_new_price);
	  
      UPDATE t505_item_consignment
         SET c505_item_qty = NVL (c505_item_qty, 0) + p_item_qty,
             c505_item_price = v_price
       WHERE c504_consignment_id = p_consignment_id
         AND c205_part_number_id = p_part_number_id
         AND NVL(c505_control_number,'TBE') = NVL(p_control_number,'TBE');

      IF (SQL%ROWCOUNT = 0)
      THEN
         SELECT s504_consign_item.NEXTVAL
           INTO p_out_item_consigment_id
           FROM DUAL;

         INSERT INTO t505_item_consignment
                     (c901_type, c505_void_fl, c505_item_price,
                      c504_consignment_id, c505_item_consignment_id,
                      c505_ref_id, c505_item_qty, c205_part_number_id,
                      c505_control_number
                     )
              VALUES (p_type, p_void_fl, v_price,
                      p_consignment_id, p_out_item_consigment_id,
                      p_ref_id, p_item_qty, p_part_number_id,
                      UPPER (NVL (p_control_number, 'TBE'))
                     );
      END IF;
         
   END gm_sav_consignment_detail;
   
  /******************************************************************************
   * Description : Procedure to  update tag details
   * Author      : Agilan
  ******************************************************************************/
 PROCEDURE gm_intrasit_tag_update (
 	P_partnum           IN      t5010_tag.c205_part_number_id%TYPE,
 	P_controlnum        IN      t5010_tag.c5010_control_number%TYPE,
	p_new_trans_id 		IN 		t5010_tag.c5010_last_updated_trans_id%TYPE,
	p_old_trans_id 		IN 		t504_consignment.c504_consignment_id%TYPE,
	p_company_id   		IN 		t1900_company.c1900_company_id%TYPE,
	p_plant_id     		IN 		t5040_plant_master.c5040_plant_id%TYPE,
	p_userid			IN		t5010_tag.c5010_last_updated_by%TYPE
)
AS
BEGIN
	 UPDATE t5010_tag 
	    SET c5010_last_updated_trans_id = p_new_trans_id
	    	,c901_location_type ='40033' -- In house 
	    	,c901_status = '51012' --To update tag report status as Active
	    	,c5010_location_id='52100' -- To update the location to Inventory.
	    	,c901_TRANS_TYPE = '51000'   -- Consignment
	        ,c1900_company_id = p_company_id
            ,c5040_plant_id = p_plant_id 
            ,c5010_last_updated_by = p_userid
            ,c5010_last_updated_date = current_date
      WHERE c5010_last_updated_trans_id = p_old_trans_id
        AND c205_part_number_id = P_partnum
        AND c5010_control_number = P_controlnum
        AND c5010_void_fl IS NULL;

END gm_intrasit_tag_update;


   /******************************************************************************
   * Description : Procedure to  update Location ID tag details PMT-39409
   * Author      : gpalani 
   */
   
  PROCEDURE gm_intrasit_tag_update_ICT (
  
 	P_partnum           IN      t5010_tag.c205_part_number_id%TYPE,
 	P_controlnum        IN      t5010_tag.c5010_control_number%TYPE,
	p_new_trans_id 		IN 		t5010_tag.c5010_last_updated_trans_id%TYPE,
	p_old_trans_id 		IN 		t504_consignment.c504_consignment_id%TYPE,
    p_location_id		IN 		t5010_tag.c5010_location_id%TYPE,
	p_company_id   		IN 		t1900_company.c1900_company_id%TYPE,
	p_plant_id     		IN 		t5040_plant_master.c5040_plant_id%TYPE,
	p_userid			IN		t5010_tag.c5010_last_updated_by%TYPE
)
   
 AS
  
BEGIN

		UPDATE t5010_tag 
	    SET  c5010_last_updated_trans_id = p_new_trans_id
	        ,c5010_location_id=p_location_id -- Updating the Location ID to Set building
	    	,c901_location_type ='40033' -- In house 
	    	,c901_status = '51012' --To update tag report status as Active
	    	,c901_TRANS_TYPE = '51000'   -- Consignment
	        ,c1900_company_id = p_company_id
            ,c5040_plant_id = p_plant_id 
            ,c5010_last_updated_by = p_userid
            ,c5010_last_updated_date = current_date
      WHERE  c5010_last_updated_trans_id = p_old_trans_id
         AND c205_part_number_id = P_partnum
         AND c5010_control_number = P_controlnum
        AND  c5010_void_fl IS NULL;
	
	
END gm_intrasit_tag_update_ICT;

 /******************************************************************************
   * Description : Procedure to  update part number price
   * Author      : Agilan
  ******************************************************************************/
  PROCEDURE gm_upd_part_price_mapping ( 
 	p_partnum           IN      T413_INHOUSE_TRANS_ITEMS.C205_PART_NUMBER_ID%TYPE,
	p_price				IN		T413_INHOUSE_TRANS_ITEMS.C413_ITEM_PRICE%TYPE,
	p_user_id			IN		t412_inhouse_transactions.c412_created_by%TYPE,
	p_company_id   		IN 		t1900_company.c1900_company_id%TYPE
  )
  AS
  v_pnum_count      NUMBER;  
  
  BEGIN
	SELECT COUNT(*)
	  INTO v_pnum_count 
	  FROM t2052_part_price_mapping
	 WHERE C205_PART_NUMBER_ID  = p_partnum
	   AND C1900_COMPANY_ID = p_company_id
	   AND C2052_VOID_FL IS NULL;
	   
	   IF (v_pnum_count = 0) THEN
	     		INSERT INTO T2052_PART_PRICE_MAPPING
               				( C2052_PART_PRC_MAP_ID,c205_part_number_id, c2052_equity_price, c2052_loaner_price,
               			     c2052_consignment_price, c2052_updated_by,
               				 c2052_updated_date,  c1900_company_id)
                     VALUES ( S2052_PART_PRICE_MAPPING.NEXTVAL , p_partnum,   p_price,  p_price, 
                              p_price, p_user_id,
                              CURRENT_DATE,  p_company_id );
      ELSE
      	 	UPDATE T2052_PART_PRICE_MAPPING 
			   SET c2052_equity_price = p_price 
				   ,c2052_Loaner_price = p_price
				   ,c2052_consignment_price = p_price
				   ,c2052_last_updated_by= p_user_id
				   ,c2052_last_updated_date=current_date 
		     WHERE c205_part_number_id = p_partnum 
			   AND (c2052_equity_price is null OR c2052_Loaner_price is null OR c2052_consignment_price is null)  
			   AND c1900_company_id = p_company_id
			   AND C2052_VOID_FL IS NULL;
	  END IF;
	  END gm_upd_part_price_mapping;
END gm_pkg_in_transit_trans;
/