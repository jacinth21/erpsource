/* Formatted on 2010/10/05 12:38 (Formatter Plus v4.8.0) */
--@"C:\database\Packages\Common\gm_pkg_cm_rule_conditions.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_cm_rule_conditions
IS
	/*******************************************************
	* Description : Procedure to get vendor list.
	* Author		: Ritesh Shah
	*******************************************************/
	PROCEDURE gm_op_fch_vendorlist (
		p_outvedorlist	 OUT   TYPES.cursor_type
	)
	AS
	BEGIN
--
		OPEN p_outvedorlist
		 FOR
			 SELECT   t301.c301_lot_code codeid, t301.c301_vendor_id ID
					, t301.c301_lot_code || ' - ' || t301.c301_vendor_name codenm
				 FROM t301_vendor t301, t101_party t101
				WHERE t301.c101_party_id = t101.c101_party_id
			 ORDER BY t301.c301_vendor_name;
	END gm_op_fch_vendorlist;

/*******************************************************
	* Description : Procedure to get set list.
	* Author		: Ritesh Shah
	*******************************************************/
	PROCEDURE gm_pd_fch_setlist (
		p_outsetlist   OUT	 TYPES.cursor_type
	)
	AS
	BEGIN
--
		OPEN p_outsetlist
		 FOR
			 SELECT   c207_set_id codeid, c207_set_id || '-' || c207_set_nm codenm
				 FROM t207_set_master
				WHERE c207_void_fl IS NULL AND c901_set_grp_type = '1601' AND c207_obsolete_fl IS NULL
			 ORDER BY c207_set_id;
--
	END gm_pd_fch_setlist;

	/*******************************************************
	* Description : Procedure to get country.
	* Author		: Ritesh Shah
	*******************************************************/
	PROCEDURE gm_fch_country (
		p_consignid   IN	   t504_consignment.c504_consignment_id%TYPE
	  , p_country	  OUT	   VARCHAR2
	)
	AS
		v_country	   VARCHAR2 (30);
		v_ship_to	   NUMBER;
		v_ship_to_id   VARCHAR2 (20);
		v_type	   	   NUMBER;
		v_dist_id      VARCHAR2 (20);
		v_div_type		VARCHAR2(20) := '100823'; -- US Distributor
	BEGIN
		BEGIN
			SELECT t504.c504_ship_to, t504.c504_ship_to_id, t504.c701_distributor_id, t504.c504_type
			  INTO v_ship_to, v_ship_to_id, v_dist_id, v_type
			  FROM t504_consignment t504
			 WHERE t504.c504_consignment_id = p_consignid;
			
			 v_div_type := NVL(get_distributor_division(v_dist_id),'100823');
			
			SELECT get_code_name_alt (gm_pkg_cm_rule_conditions.get_cs_ship_country (v_ship_to, v_ship_to_id, NULL))
			  INTO v_country
			  FROM DUAL;
              
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				v_country	:= '';
		END;
		--Below line of code is added for rules not to execute for OUS consignment.
		SELECT DECODE(v_type,'4110',DECODE(v_div_type,'100824','OUS','US'),v_country) 
		  INTO v_country 
		  FROM DUAL;

		p_country	:= v_country;
	END;

	/*******************************************************
	* Description : Procedure to get shipout country.
	* Author		: Ritesh Shah
	*******************************************************/
	PROCEDURE gm_fch_shioutcountry (
		p_addid 	IN		 t106_address.c106_address_id%TYPE
	  , p_country	OUT 	 VARCHAR2
	)
	AS
	BEGIN
		BEGIN
			SELECT TRIM (get_code_name_alt (t106.c901_country))
			  INTO p_country
			  FROM t106_address t106
			 WHERE t106.c106_address_id = p_addid;
		EXCEPTION
			WHEN OTHERS
			THEN
				p_country	:= '';
		END;
	END gm_fch_shioutcountry;
  
 /**************************************************************************************
 * Description	  : This function returns the country id for given rep who has address.
 *
 ****************************************************************************************/
  FUNCTION get_country_id (
    p_addid   t106_address.c106_address_id%TYPE
  )
    RETURN NUMBER
  IS
    v_country_id   t703_sales_rep.c703_ship_country%TYPE;
  BEGIN
    SELECT t106.c901_country
      INTO v_country_id
      FROM t106_address t106
     WHERE t106.c106_address_id = p_addid;
  
    RETURN v_country_id;
  EXCEPTION
    WHEN NO_DATA_FOUND
    THEN
      RETURN 0;
  END get_country_id;

/***************************************************************************************
 * Description	  : This function returns the country to whom the set
	is shipped to.
 *
 ****************************************************************************************/
  FUNCTION get_cs_ship_country (
    p_ship_to	   NUMBER
    , p_ship_to_id   VARCHAR2
    , p_addid 	   t106_address.c106_address_id%TYPE
  )
    RETURN NUMBER
  IS
    v_rep_id	   VARCHAR2 (20);
    v_country_id   NUMBER;
  BEGIN
  --
    IF p_ship_to = 4120   --if it's distributor, get country by distributor id
    THEN
      v_country_id := gm_pkg_cm_rule_conditions.get_distributor_country (p_ship_to_id);
    ELSIF p_ship_to = 4121  --if it's sals rep, get country by adddress, or get country by rep id without address
    THEN
      IF p_addid IS NOT NULL
      THEN
        v_country_id := gm_pkg_cm_rule_conditions.get_country_id (p_addid);
      ELSE
        v_country_id := gm_pkg_cm_rule_conditions.get_rep_country_id (p_ship_to_id);
      END IF;
    ELSIF p_ship_to = 4122      --if it's hospital, get country by account id
    THEN
    --  v_rep_id	:= TRIM (get_rep_id (p_ship_to_id));
    --  v_country_id := gm_pkg_cm_rule_conditions.get_rep_country_id (v_rep_id);
         v_country_id := gm_pkg_cm_rule_conditions.get_hosp_country_id (p_ship_to_id);
    ELSIF p_ship_to = 4123      --if it's employee, give country US
    THEN 
         v_country_id := NULL;     
    --if ship_to is none of above, need to check if address is null, ex, on order screen, select bill only - Loaner  
    ELSIF p_addid IS NOT NULL
       THEN
        v_country_id := gm_pkg_cm_rule_conditions.get_country_id (p_addid);    
    END IF;
  
    RETURN v_country_id;
  EXCEPTION
    WHEN OTHERS
    THEN
      RETURN 0;
  END get_cs_ship_country;
  
/**************************************************************************************
 * Description	  : This function returns the country for given account (Hospital).
 *
 ****************************************************************************************/
	FUNCTION get_hosp_country_id (
		p_acct_id   t704_account.c704_account_id%TYPE
	)
		RETURN NUMBER
	IS
		v_country_id   t704_account.c704_ship_country%TYPE;
	BEGIN
		BEGIN
			SELECT t704.c704_ship_country
			  INTO v_country_id
			  FROM t704_account t704
			 WHERE t704.c704_account_id = p_acct_id;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				RETURN 0;
		END;

		RETURN v_country_id;
	END get_hosp_country_id;
/**************************************************************************************
 * Description	  : This function returns the country for given distributor.
 *
 ****************************************************************************************/
	FUNCTION get_distributor_country (
		p_dist_id	t701_distributor.c701_distributor_id%TYPE
	)
		RETURN NUMBER
	IS
		v_dist_country t701_distributor.c701_ship_country%TYPE;
	BEGIN
		BEGIN
			SELECT c701_ship_country
			  INTO v_dist_country
			  FROM t701_distributor
			 WHERE c701_distributor_id = p_dist_id;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				RETURN 0;
		END;

		RETURN v_dist_country;
	END get_distributor_country;

/**************************************************************************************
 * Description	  : This function returns the country for given rep.
 *
 ****************************************************************************************/
	FUNCTION get_rep_country_id (
		p_rep_id   t703_sales_rep.c703_sales_rep_id%TYPE
	)
		RETURN NUMBER
	IS
		v_country_id   t703_sales_rep.c703_ship_country%TYPE;
	BEGIN
		BEGIN
			SELECT t703.c703_ship_country
			  INTO v_country_id
			  FROM t703_sales_rep t703
			 WHERE t703.c703_sales_rep_id = p_rep_id;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				RETURN 0;
		END;

		RETURN v_country_id;
	END get_rep_country_id;

/******************************************************************
 * Description : This function returns the type for the loaners.
 *
 * Author		 : rshah
 ******************************************************************/
--
	FUNCTION get_loaner_type (
		p_consignid   IN   t504_consignment.c504_consignment_id%TYPE
	)
		RETURN VARCHAR2
--
	IS
		v_type		   VARCHAR2 (10);
	BEGIN
		BEGIN
			SELECT t504.c504_type
			  INTO v_type
			  FROM t504_consignment t504
			 WHERE t504.c504_consignment_id = p_consignid;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				RETURN '';
		END;

		RETURN v_type;
	END get_loaner_type;

/*******************************************************
 * Purpose: function is used to get Transaction name
   author Xun
 *******************************************************/
--
	FUNCTION get_condition_name (
		p_conid   IN   t9701_condition.c9701_condition_id%TYPE
	)
		RETURN VARCHAR2
	IS
		v_code_name    VARCHAR2 (100);
	BEGIN
		SELECT DECODE (p_conid
					 , 1, 'PARTNUMBER'
					 , 2, 'REVISION'
					 , 3, 'COUNTRY'
					 , 4, 'VENDOR'
					 , 5, 'LOT'
					 , 6, 'MANUFACTUREDDATE'
					 , 7, 'SETID'
					 , 8, 'QTYINSPECTED'
					  )
		  INTO v_code_name
		  FROM DUAL;

		RETURN v_code_name;
	END get_condition_name;
END gm_pkg_cm_rule_conditions;
/
