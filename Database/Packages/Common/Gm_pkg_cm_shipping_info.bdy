create or replace
PACKAGE BODY gm_pkg_cm_shipping_info
IS
/******************************************************************
   * Description : Procedure to get the address address when clicked on address lookup button
   ****************************************************************/
   FUNCTION get_address (
      p_shipto     IN   t907_shipping_info.c901_ship_to%TYPE,
      p_shiptoid   IN   t907_shipping_info.c907_ship_to_id%TYPE,
      p_add_len    IN   NUMBER DEFAULT 50
   )
      RETURN VARCHAR2
   IS
      v_shipadd   VARCHAR2 (1000);
      v_comp_id   t1900_company.c1900_company_id%TYPE;
      v_comp_name  t906_rules.c906_rule_value%TYPE;
   BEGIN
	   
      IF p_shipto = 4120
      THEN                                                    --'Distributor'
         SELECT    t701.c701_distributor_name
                || '<BR>&'||'nbsp;'
                || DECODE(t701.c701_ship_add1,NULL,'',t701.c701_ship_add1 ||'<BR>&'||'nbsp;')
                || DECODE(t701.c701_ship_add2,NULL,'',t701.c701_ship_add2 ||'<BR>&'||'nbsp;') 
                || DECODE (NVL (get_rule_value ('SWAP_ZIP_CODE',
                                                'SWAP_ZIP_CODE'
                                               ),
                                'N'
                               ),
                           'Y', DECODE(t701.c701_ship_zip_code,NULL,'',t701.c701_ship_zip_code||'<BR>&'||'nbsp;'),
                           DECODE(t701.c701_ship_city,NULL,'',t701.c701_ship_city||'<BR>&'||'nbsp;')
                          )
                || DECODE(t701.c701_ship_state,NULL,'',0,'',get_code_name_alt (t701.c701_ship_state)||'&'||'nbsp;')
                || DECODE (NVL (get_rule_value ('SWAP_ZIP_CODE',
                                                'SWAP_ZIP_CODE'
                                               ),
                                'N'
                               ),
                           'Y', t701.c701_ship_city,
                           t701.c701_ship_zip_code
                          )
           INTO v_shipadd
           FROM t701_distributor t701
          WHERE t701.c701_distributor_id = p_shiptoid AND c701_void_fl IS NULL;
      ELSIF p_shipto = 4122
      THEN                                                        --'Hospital'
         SELECT	'&'||'nbsp;'||DECODE (t704.c704_ship_attn_to,NULL,'',t704.c704_ship_attn_to||'<BR>&'||'nbsp;')
         		|| t704.c704_account_nm
                || '<BR>&'||'nbsp;'
                || t704.c704_ship_name
                || '<BR>&'||'nbsp;'
                || DECODE (t704.c704_ship_add1,NULL,'',t704.c704_ship_add1||'<BR>&'||'nbsp;')
                || DECODE (t704.c704_ship_add2,NULL,'',t704.c704_ship_add2||'<BR>&'||'nbsp;')
                || DECODE (NVL (get_rule_value ('SWAP_ZIP_CODE',
                                                'SWAP_ZIP_CODE'
                                               ),
                                'N'
                               ),
                           'Y', t704.c704_ship_zip_code||'<BR>&'||'nbsp;',
                           t704.c704_ship_city||'<BR>&'||'nbsp;'
                          )
                
                || get_code_name_alt (t704.c704_ship_state)
                || '&'||'nbsp;'
                || DECODE (NVL (get_rule_value ('SWAP_ZIP_CODE',
                                                'SWAP_ZIP_CODE'
                                               ),
                                'N'
                               ),
                           'Y', t704.c704_ship_city,
                           t704.c704_ship_zip_code
                          )
           INTO v_shipadd
           FROM t704_account t704
          WHERE t704.c704_account_id = p_shiptoid AND c704_void_fl IS NULL;
      ELSIF p_shipto = 4123
      THEN                                                          --employee
      		v_shipadd := gm_pkg_cm_shipping_info.get_empaddress(NULL,p_shiptoid);
      		
      ELSIF p_shipto = 4121
      THEN                                                         --Sales Rep
        SELECT    t703.c703_sales_rep_name||' '
              || SUBSTR (   get_code_name (c901_address_type)
                           || '-'
                           || c106_add1
                           || ' '
                           || c106_add2
                           || ' '
                           || DECODE (NVL (get_rule_value ('SWAP_ZIP_CODE',
                                                'SWAP_ZIP_CODE'),
                                'N'
                               ),'Y', C106_ZIP_CODE||'<BR>&'||'nbsp;',
                           C106_CITY||'<BR>&'||'nbsp;')
                            || GET_CODE_NAME_ALT (C901_STATE)
                            || DECODE (NVL (get_rule_value ('SWAP_ZIP_CODE',
                                                'SWAP_ZIP_CODE'
                                               ),
                                'N'
                               ),
                           'Y', C106_CITY,
                           C106_ZIP_CODE),
                           0,
                            NVL(p_add_len,50))
         INTO v_shipadd
           FROM t703_sales_rep t703, t106_address t106
          WHERE t703.c101_party_id = t106.c101_party_id
            AND t106.c106_address_id = TO_NUMBER (p_shiptoid)
            AND t703.c703_void_fl IS NULL         -- Need to t703, t106 add void flag for sales Rep in PC-3859
            AND t106.c106_void_fl IS NULL
			AND ROWNUM=1 --PC-5426- Added RowNUm = 1 to avoid exact fetch error while running BBA Shipment Job
			ORDER BY t106.c106_primary_fl DESC;

      ELSIF p_shipto = 4000642	OR p_shipto= 107801							---Shipping Account or Dealer
      THEN       	            
	   		v_shipadd :=  get_shipacct_addr(NULL,p_shiptoid);
	  ELSIF p_shipto = 26240419									---Plant
      THEN  
      --getting company id for the plant id
		      SELECT C1900_PARENT_COMPANY_ID
		      	INTO v_comp_id
		       FROM T5040_PLANT_MASTER 
		       WHERE C5040_PLANT_ID = p_shiptoid AND C5040_VOID_FL IS NULL AND C5040_ACTIVE_FL= 'Y';
		--getting     
		      SELECT GET_RULE_VALUE_BY_COMPANY('COMP_NAME','COMP_DETAILS',v_comp_id)
		        INTO v_comp_name
		        FROM DUAL;
		       
  
       			 SELECT	'&'||'nbsp;'
       			||DECODE (v_comp_name,'','',v_comp_name||'<BR>&'||'nbsp;')
         		|| C5040_PLANT_NAME
                || '<BR>&'||'nbsp;'
                || DECODE (C5040_ADDRESS_1,NULL,'',C5040_ADDRESS_1||'<BR>&'||'nbsp;')
                || DECODE (C5040_ADDRESS_2,NULL,'',C5040_ADDRESS_2||'<BR>&'||'nbsp;')
                 || DECODE (NVL (get_rule_value ('SWAP_ZIP_CODE',
                                                'SWAP_ZIP_CODE'
                                               ),
                                'N'
                               ),
                           'Y', C5040_ZIPCODE||'<BR>&'||'nbsp;',
                           C5040_CITY||'<BR>&'||'nbsp;'
                          )
                
                || DECODE (C901_STATE_ID,NULL,'',get_code_name_alt (C901_STATE_ID)||'<BR>&'||'nbsp;') 
             
                || DECODE (NVL (get_rule_value ('SWAP_ZIP_CODE',
                                                'SWAP_ZIP_CODE'
                                               ),
                                'N'
                               ),
                           'Y', C5040_CITY||'<BR>&'||'nbsp;',
                           C5040_ZIPCODE||'<BR>&'||'nbsp;'
                          )
                

           INTO v_shipadd
           FROM T5040_PLANT_MASTER 
          WHERE C5040_PLANT_ID = p_shiptoid AND C5040_VOID_FL IS NULL AND C5040_ACTIVE_FL= 'Y';
      
      END IF;
      RETURN v_shipadd;
	  EXCEPTION 
	  	 WHEN NO_DATA_FOUND 
	  	 THEN
	  		raise_application_error ('-20752', '');
      
   END get_address;

/******************************************************************
   * Description : Procedure to get the ShipID when refid and source is passed
   ****************************************************************/
   FUNCTION get_shipping_id (
      p_refid    IN   t907_shipping_info.c907_ref_id%TYPE,
      p_source   IN   t907_shipping_info.c901_source%TYPE,
      p_action   IN   VARCHAR2
   )
      RETURN VARCHAR2
   IS
      v_shipid   VARCHAR2 (100);
      v_count    NUMBER;
      v_status_name VARCHAR2(100);
      v_plant_id    t5040_plant_master.c5040_plant_id%TYPE;
   BEGIN
  	   SELECT  NVL(get_plantid_frm_cntx(),get_rule_value('DEFAULT_PLANT','ONEPORTAL')) INTO  v_plant_id FROM DUAL;

      IF p_source = 50182                                          -- loaners
      THEN
         BEGIN
            SELECT c907_shipping_id
              INTO v_shipid
              FROM t907_shipping_info
             WHERE c907_ref_id = p_refid
               AND c901_source = p_source
               AND c907_status_fl < '40'
               AND c907_void_fl IS NULL
               AND c5040_plant_id= v_plant_id;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               v_shipid := 0;
         END;

         IF v_shipid = 0
         THEN
            SELECT COUNT (1)
              INTO v_count
              FROM t907_shipping_info
             WHERE c907_ref_id = p_refid
               AND c901_source = p_source
               AND c907_void_fl IS NULL
               AND c5040_plant_id= v_plant_id;

            IF v_count > 1 OR v_count = 0
            THEN
            -- Raising Customised App Error if the Scanned request is not processed upto Pending Shipping.
               SELECT get_transaction_status(t504a.c504a_status_fl,'4127') INTO v_status_name --4127[Product Loaner]
                 FROM t504a_consignment_loaner t504a 
                WHERE t504a.c504_consignment_id = p_refid
                  AND t504a.c504a_void_fl IS NULL
                  AND t504a.c5040_plant_id= v_plant_id;
                  -- If the status is pending pick, it should allow the user to change the shipping details
                  IF v_status_name != 'Pending Pick'
                  THEN
						GM_RAISE_APPLICATION_ERROR('-20999','18',p_refid||'#$#'||v_status_name);
               	  ELSIF v_status_name = 'Pending Pick'
               	  THEN
               	  		SELECT C907_SHIPPING_ID  
               	  		INTO v_shipid 
               	  		FROM T907_SHIPPING_INFO 
               	  		WHERE C907_REF_ID IN (GET_LN_REQ_DET_ID(p_refid))-- getting the request id corresponding to consignment id
               	  		AND c907_status_fl < '40'
               	  		AND c907_void_fl IS NULL
               	  		AND c5040_plant_id= v_plant_id;
               	  END IF;
            END IF;
         END IF;
      ELSE
         BEGIN
            SELECT c907_shipping_id
              INTO v_shipid
              FROM t907_shipping_info
             WHERE c907_ref_id = p_refid
               AND c901_source = p_source
               AND c907_void_fl IS NULL
               AND c5040_plant_id= v_plant_id;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               IF p_action = 'fetch'
               THEN
                  v_shipid := '';
               ELSE
                  raise_application_error ('-20769', '');
               END IF;
         END;
      END IF;

      RETURN v_shipid;
   END get_shipping_id;

   /******************************************************************
          * Description : Procedure to get the address info for a specific party
          ****************************************************************/
   PROCEDURE gm_fch_ship_details (
      p_refid         IN       t907_shipping_info.c907_ref_id%TYPE,
      p_source        IN       t907_shipping_info.c901_source%TYPE,
      p_action        IN       VARCHAR2,
      p_shipinfocur   OUT      TYPES.cursor_type
   )
   AS
      v_count    NUMBER;
      v_shipid   t907_shipping_info.c907_shipping_id%TYPE;
   BEGIN
      v_shipid :=
         gm_pkg_cm_shipping_info.get_shipping_id (p_refid, p_source,
                                                  p_action);
      gm_pkg_cm_shipping_info.gm_fch_ship_details_byid (v_shipid,
                                                        p_shipinfocur
                                                       );
   END gm_fch_ship_details;

   /******************************************************************
      * Description : Procedure to get the address info for a specific party based on the shipid
      ****************************************************************/
   PROCEDURE gm_fch_ship_details_byid (
      p_shipid        IN       t907_shipping_info.c907_shipping_id%TYPE,
      p_shipinfocur   OUT      TYPES.cursor_type
   )
   AS
   v_source		t907_shipping_info.c901_source%TYPE;
   v_requestid	t520_request.c520_request_id%TYPE;
   v_refid		t907_shipping_info.c907_ref_id%TYPE;
   v_purpose	t901_code_lookup.c901_code_id%TYPE;
   v_datefmt	VARCHAR2(10);
   v_plant_id   t5040_plant_master.c5040_plant_id%TYPE;
   BEGIN
   
   		SELECT NVL(get_compdtfmt_frm_cntx(),'MM/DD/YYYY') INTO v_datefmt FROM DUAL;
  		SELECT  NVL(get_plantid_frm_cntx(),get_rule_value('DEFAULT_PLANT','ONEPORTAL')) INTO  v_plant_id FROM DUAL;
   	
	   BEGIN
		   	SELECT c901_source,c907_ref_id INTO v_source, v_refid
		   		FROM t907_shipping_info t907
	           WHERE 
	           	c907_shipping_id = p_shipid AND c907_void_fl IS NULL
	           	AND t907.c5040_plant_id= v_plant_id;
        EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
        	v_source := null;
        END;
        IF v_source = '50181'
        THEN
	        BEGIN
		     	SELECT t504.c520_request_id,t504.c504_inhouse_purpose INTO v_requestid,v_purpose
		     		FROM t504_consignment t504
		     		WHERE t504.c504_consignment_id = v_refid
		     			AND  t504.c504_void_fl is null AND t504.c5040_plant_id= v_plant_id;
		     EXCEPTION WHEN NO_DATA_FOUND
		     THEN
		     	BEGIN
			     	SELECT t520.c520_request_id,t520.c901_purpose INTO v_requestid,v_purpose
			     		FROM t520_request t520
			     		WHERE t520.c520_request_id = v_refid
			     			AND t520.c520_void_fl is null AND t520.c5040_plant_id= v_plant_id;
			    EXCEPTION
			    WHEN NO_DATA_FOUND
			    THEN
			    	v_requestid := null;
			    END;
		     END;
		   END IF;
      OPEN p_shipinfocur
       FOR
          SELECT c907_shipping_id shipid,get_associate_txn_id(c907_ref_id) assctxnid,
                 
                 --decode(c901_source,'50182',GM_PKG_OP_CONSIGNMENT.get_hold_flg(c907_ref_id),'') holdflag,
                 gm_pkg_cm_shipping_info.get_hold_flg (c907_ref_id,
                                                       c901_source
                                                      ) holdflag,
                 c907_ref_id refid, c907_ref_id hrefid, c901_ship_to shipto,
                 c907_ship_to_id names,
                 TO_CHAR (c907_ship_to_dt, v_datefmt) shiptodate,
                 DECODE (c901_ship_to,'4121', get_distributor_type(get_distributor_id(c907_ship_to_id)),get_distributor_type(c907_ship_to_id)) dtype, 
                 c901_source SOURCE, c901_delivery_mode shipmode,
                 c901_delivery_carrier shipcarrier,
                 c907_tracking_number trackingno, c907_void_fl vflg,
                 c106_address_id addressid, c907_status_fl status,
                 DECODE(c907_status_fl ,30,'Pending Shipping',33,'Packing in Progress',36,'Ready for Pickup',40,'Completed',NULL) statusnm,
                 c907_frieght_amt freightamt,
                 c907_override_attn_to overrideAttnTo,
                 TO_CHAR (c907_release_dt, v_datefmt) realsdt,
                 TO_CHAR (c907_shipped_dt, v_datefmt) shipdate,
                 NVL (TRIM (get_code_name_alt (c901_record_type)),
                      'N'
                     ) ismaster,
                 DECODE
                    (c901_source,
                     50181, DECODE
                        (c901_ship_to,
                         4120, NVL
                             (get_distributor_inter_party_id (c907_ship_to_id),
                              0
                             ),
                         0
                        ),
                     0
                    ) interpartyid,
                 DECODE
                    (c901_source,
                     50181,gm_pkg_op_request_master.get_request_attribute(v_requestid,'1006420'),
                     50180,get_cust_po_id(v_refid),
                     ''
                    ) custpo,
                 DECODE
                    (c901_source,
                     50181,v_requestid,
                     ''
                    ) hreqid , get_txn_value(c907_ref_id,c901_source,'DIST') distid ,get_txn_value(c907_ref_id,c901_source,'REP') repid  
                    ,get_txn_value(c907_ref_id,c901_source,'ACC') accid
                    ,DECODE(c901_source,'50180',get_account_curr_symb(get_txn_value(c907_ref_id,c901_source,'ACC')),'') ACCCURRSYMB
                    ,gm_pkg_sm_eventbook_rpt.get_consignment_type(c907_ref_id) ctype
                    ,DECODE
                    (c901_source,
                     50186,gm_pkg_op_ihloaner_item_rpt.get_item_return_id(c907_ref_id,9110), --getting RA# for IHLN
                     ''
                    ) returnid, v_purpose strPurpose,
                 get_code_name(c901_delivery_mode) shipmodesh,
                 get_code_name(c901_delivery_carrier) shipcarriernm,
                 c907_ship_instruction shipInstruction,
                 DECODE(c901_ship_to, '4120', GET_DISTRIBUTOR_NAME (c907_ship_to_id),'4121', GET_USER_NAME (c907_ship_to_id),'4122', GET_ACCOUNT_NAME (c907_ship_to_id)) shipName,
                 DECODE(c907_status_fl,'33',c907_pack_station_id,NULL) pickStationId,
                 c907_ship_tote_id toteId,
                 get_code_name(c901_source) sourcenm,
                 gm_pkg_op_loaner.get_loaner_etch_id(t907.c907_ref_id) etchid,
                 GET_ACCOUNT_ATTRB_VALUE(c907_ship_to_id,'91980') thirdParty, 
                 NVL(T907.C907_FRIEGHT_AMT, 0) frightamt,
                 Decode(c901_source,'50180',GET_ORDER_TYPE(c907_ref_id),NULL) order_type,
                 c525_product_request_id loanerrequestId
            FROM t907_shipping_info t907
           WHERE c907_shipping_id = p_shipid AND c907_void_fl IS NULL
           AND t907.c5040_plant_id= v_plant_id;
   END gm_fch_ship_details_byid;

   /******************************************************************
	 * Description : function to get shipto
	 * Function is altered for PMT-6402 to handle ship Address changes with BBA and US. 
	 ****************************************************************/
   
   FUNCTION get_ship_add (
      p_refid    IN   t501_order.c501_order_id%TYPE,
      p_source   IN   t907_shipping_info.c901_source%TYPE
   )
   	RETURN VARCHAR2
   	IS
   		v_ship_add     VARCHAR2 (1000);
   	BEGIN
   		v_ship_add:= Gm_pkg_cm_shipping_info.get_ship_add(p_refid,p_source,'');
   		RETURN v_ship_add;
   		
   END get_ship_add;
   /******************************************************************
	 * Description : function to get shipto
	 * Function is altered for PMT-6402 to handle ship Address changes with BBA and US. 
	 ****************************************************************/
   FUNCTION get_ship_add (
      p_refid    IN   t501_order.c501_order_id%TYPE,
      p_source   IN   t907_shipping_info.c901_source%TYPE,
      p_swap_attn_to IN VARCHAR2
   )
      RETURN VARCHAR2
   IS
      v_ship         VARCHAR2 (50);
      v_shipto_id    t907_shipping_info.c907_ship_to_id%TYPE;
      v_ship_add     VARCHAR2 (1000);
      v_address_id   t907_shipping_info.c106_address_id%TYPE;
      v_comp_id T1900_COMPANY.C1900_COMPANY_ID%TYPE;
	  v_delimiter VARCHAR2(10);
      v_zip_code_fl VARCHAR2(10);
      v_plant_comp_id   t1900_company.c1900_company_id%TYPE;
      v_comp_name  t906_rules.c906_rule_value%TYPE;
   BEGIN
	   --
	   SELECT  NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL')) INTO v_comp_id FROM DUAL;
	   --
	   -- This rule will get the Delimiter that needs to be shown in the Zip Code Section. For Country Like Germany, they do not need a delimiter.
		SELECT NVL(get_rule_value_by_company('DELIMITER','ZIP_CODE_DELIM', v_comp_id),',')
		, NVL(get_rule_value_by_company('SWAP_ZIP_CODE','SWAP_ZIP_CODE', v_comp_id),'N')
		INTO v_delimiter, v_zip_code_fl FROM DUAL;
		--
      BEGIN
         SELECT get_code_name (t907.c901_ship_to), t907.c907_ship_to_id,
                c106_address_id
           INTO v_ship, v_shipto_id,
                v_address_id
           FROM t907_shipping_info t907
          WHERE t907.c907_ref_id = p_refid
            AND t907.c901_source = p_source
            AND c907_void_fl IS NULL;
		 /*
		  * The Below Shipping Address code is changed for PMT-6402.
		  * In US DB swap Attention to will be null, In BBA it will be Y.
		  * When Invoice is generated from BBA Attention to will be displayed at last, when generated from US it will work as earlier, it will be displayed first.
		  */
         IF v_ship = 'Distributor'
         THEN
            SELECT '&'||'nbsp;'|| c701_ship_name
                   || '<BR>&'||'nbsp;'
                   || DECODE(p_swap_attn_to,'Y','',DECODE(t907.c907_override_attn_to,'',NULL,'Attn:&'||'nbsp;'|| t907.c907_override_attn_to || '<BR>&'||'nbsp;')) -- Will work foe US.
                   || DECODE(t701.c701_ship_add1,NULL,'',t701.c701_ship_add1 ||'<BR>&'||'nbsp;')
                   || DECODE(t701.c701_ship_add2,NULL,'',t701.c701_ship_add2 ||'<BR>&'||'nbsp;')
                   || DECODE (v_zip_code_fl ,
                              'Y', DECODE(t701.c701_ship_zip_code,NULL,'',t701.c701_ship_zip_code || v_delimiter ||'&'||'nbsp;'),
                              DECODE(t701.c701_ship_city,NULL,'',t701.c701_ship_city || ',&'||'nbsp;')
                             )
                   
                   || DECODE(t701.c701_ship_state ,NULL,'','1057','',get_code_name_alt (t701.c701_ship_state)||'&'||'nbsp;') -- To handle when state is NA , it should not display in invoice. 
                   || DECODE (v_zip_code_fl,
                              'Y', t701.c701_ship_city ||'<BR>&'||'nbsp;',
                              t701.c701_ship_zip_code
                             ||'<BR>&'||'nbsp;')
                   || DECODE (t701.C701_SHIP_COUNTRY, NULL, '', get_code_name (t701.C701_SHIP_COUNTRY) ||'&'||'nbsp;')
                   || DECODE(p_swap_attn_to,'Y',DECODE(t907.c907_override_attn_to,'',NULL,'<BR>'||'Attn:&'||'nbsp;'|| t907.c907_override_attn_to || '<BR>&'||'nbsp;'),'') -- will work for BBA.
              INTO v_ship_add
              FROM t701_distributor t701, t907_shipping_info t907
             WHERE t701.c701_distributor_id = v_shipto_id
             AND t907.c907_ref_id = p_refid
             AND t907.c901_source = p_source               --50180 orders
             AND t907.c907_void_fl IS NULL;
         --AND c701_void_fl IS NULL;
         ELSIF (v_ship = 'Sales Rep' OR v_ship = 'Shipping Account' OR v_ship = 'Dealer')
         THEN
            IF v_address_id IS NULL
            THEN
               SELECT DECODE(v_ship, 'Sales Rep', get_rep_name (v_shipto_id), get_party_name(v_shipto_id))
                 INTO v_ship_add
                 FROM DUAL;
            ELSE
               SELECT '&'||'nbsp;'|| get_party_name (t106.c101_party_id)
                      || '<BR>&'||'nbsp;'
                      || DECODE(p_swap_attn_to,'Y','',DECODE(t907.c907_override_attn_to,'',NULL,'Attn:&'||'nbsp;'|| t907.c907_override_attn_to || '<BR>&'||'nbsp;'))--will work for US.
                      || DECODE(t106.c106_add1,NULL,'',t106.c106_add1 ||'<BR>&'||'nbsp;')
                      || DECODE(t106.c106_add2,NULL,'',t106.c106_add2 ||'<BR>&'||'nbsp;')
                      || DECODE (v_zip_code_fl ,
                                 'Y', t106.c106_zip_code ||'&'||'nbsp;',
                                 t106.c106_city || '&'||'nbsp;'
                                )
                      || DECODE (t106.c901_state,NULL,'','1057','',get_code_name_alt (t106.c901_state)||'&'||'nbsp;') --To handle when state is NA , it should not display in invoice. 
                      || DECODE (v_zip_code_fl ,
                                 'Y', t106.c106_city ||'<BR>&'||'nbsp;',
                                 t106.c106_zip_code
                                ||'<BR>&'||'nbsp;')
                       || DECODE (t106.C901_COUNTRY, NULL, '', get_code_name (t106.C901_COUNTRY) ||'&'||'nbsp;') 
                       || DECODE(p_swap_attn_to,'Y',DECODE(t907.c907_override_attn_to,'',NULL,'<BR>'||'Attn:&'||'nbsp;'|| t907.c907_override_attn_to || '<BR>&'||'nbsp;'),'') -- Will work for BBA.
                 INTO v_ship_add
                 FROM t106_address t106, t907_shipping_info t907
                WHERE t106.c106_address_id = t907.c106_address_id
                  AND t907.c907_ref_id = p_refid
                  AND t907.c901_source = p_source               --50180 orders
                  AND t907.c907_void_fl IS NULL;
            --AND t106.c106_void_fl IS NULL;
            END IF;
         ELSIF v_ship = 'Hospital'
         THEN
           SELECT '&'||'nbsp;'|| t704.c704_ship_name
                   || '<BR>&'||'nbsp;'
                   || DECODE(p_swap_attn_to,'Y','',DECODE(t907.c907_override_attn_to,'',DECODE(t704.c704_ship_attn_to,NULL,NULL,'Attn:&'||'nbsp;'|| t704.c704_ship_attn_to
                   || '<BR>&'||'nbsp;'),'Attn:&'||'nbsp;'|| t907.c907_override_attn_to || '<BR>&'||'nbsp;')) -- will work for US.
                   || DECODE (t704.c704_ship_add1,NULL,'',t704.c704_ship_add1||'<BR>&'||'nbsp;')
                   || DECODE (t704.c704_ship_add2,NULL,'',t704.c704_ship_add2||'<BR>&'||'nbsp;')
                   || DECODE (v_zip_code_fl ,
                              'Y', t704.c704_ship_zip_code|| v_delimiter ||'&'||'nbsp;',
                              t704.c704_ship_city|| ',&'||'nbsp;'
                             )
                   || DECODE(t704.c704_ship_state,NULL,'','1057','',get_code_name (t704.c704_ship_state)||'&'||'nbsp;') --To handle when state is NA , it should not display in invoice. 
                   || DECODE (v_zip_code_fl ,
                              'Y', t704.c704_ship_city ||'<BR>&'||'nbsp;',
                              t704.c704_ship_zip_code
                             ||'<BR>&'||'nbsp;')
                  || DECODE (t704.C704_SHIP_COUNTRY, NULL, '', get_code_name (t704.C704_SHIP_COUNTRY) ||'&'||'nbsp;')
                  || DECODE(p_swap_attn_to,'Y',DECODE(t907.c907_override_attn_to,'',NULL,'<BR>'||'Attn:&'||'nbsp;'|| t907.c907_override_attn_to || '<BR>&'||'nbsp;'),'')-- will work for BBA.
              INTO v_ship_add
              FROM t704_account t704, t907_shipping_info t907
             WHERE t704.c704_account_id = v_shipto_id
             AND t907.c907_ref_id = p_refid
             AND t907.c901_source = p_source               --50180 orders
             AND t907.c907_void_fl IS NULL;
         --AND c704_void_fl IS NULL;
         ELSIF v_ship = 'Employee'
         THEN
            SELECT	'&'||'nbsp;'|| get_user_name (c101_user_id)
                   	|| '<BR>&'||'nbsp;'
            		|| DECODE(p_swap_attn_to,'Y','',DECODE(t907.c907_override_attn_to,'',NULL,'Attn:&'||'nbsp;'|| t907.c907_override_attn_to || '<BR>&'||'nbsp;'))
                   	|| decode(c101_address,null,'','<BR>&'||'nbsp;'||c101_address)
                   	|| DECODE(p_swap_attn_to,'Y',DECODE(t907.c907_override_attn_to,'',NULL,'<BR>'||'Attn:&'||'nbsp;'|| t907.c907_override_attn_to || '<BR>&'||'nbsp;'),'')
              INTO v_ship_add
              FROM t101_user, t907_shipping_info t907
             WHERE c101_user_id = v_shipto_id
             AND t907.c907_ref_id = p_refid
             AND t907.c901_source = p_source
             AND t907.c907_void_fl IS NULL;
          ELSIF v_ship = 'Plant'									---Plant
      	  THEN  
      	        --getting company id for the plant id
		      SELECT C1900_PARENT_COMPANY_ID
		      	INTO v_plant_comp_id
		       FROM T5040_PLANT_MASTER 
		       WHERE C5040_PLANT_ID = v_shipto_id AND C5040_VOID_FL IS NULL AND C5040_ACTIVE_FL= 'Y';
		--getting     
		      SELECT GET_RULE_VALUE_BY_COMPANY('COMP_NAME','COMP_DETAILS',v_plant_comp_id)
		        INTO v_comp_name
		        FROM DUAL;
		       
  
       			 SELECT	'&'||'nbsp;'
       			||DECODE (v_comp_name,'','',v_comp_name||'<BR>&'||'nbsp;')
         		|| C5040_PLANT_NAME
                || '<BR>&'||'nbsp;'
                || DECODE (C5040_ADDRESS_1,NULL,'',C5040_ADDRESS_1||'<BR>&'||'nbsp;')
                || DECODE (C5040_ADDRESS_2,NULL,'',C5040_ADDRESS_2||'<BR>&'||'nbsp;')
                 || DECODE (NVL (get_rule_value ('SWAP_ZIP_CODE',
                                                'SWAP_ZIP_CODE'
                                               ),
                                'N'
                               ),
                           'Y', C5040_ZIPCODE||'<BR>&'||'nbsp;',
                           C5040_CITY||'<BR>&'||'nbsp;'
                          )
                
                || DECODE (C901_STATE_ID,NULL,'',get_code_name_alt (C901_STATE_ID)||'<BR>&'||'nbsp;') 
             
                || DECODE (NVL (get_rule_value ('SWAP_ZIP_CODE',
                                                'SWAP_ZIP_CODE'
                                               ),
                                'N'
                               ),
                           'Y', C5040_CITY||'<BR>&'||'nbsp;',
                           C5040_ZIPCODE||'<BR>&'||'nbsp;'
                          )
                

           INTO v_ship_add
           FROM T5040_PLANT_MASTER 
          WHERE C5040_PLANT_ID = v_shipto_id AND C5040_VOID_FL IS NULL AND C5040_ACTIVE_FL= 'Y';
         END IF;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            RETURN '';
      END;

      RETURN v_ship_add;
   END get_ship_add;

   /******************************************************************
          * Description : function to get shipto
          ****************************************************************/
   FUNCTION get_shipto (
      p_refid    IN   t501_order.c501_order_id%TYPE,
      p_source   IN   t907_shipping_info.c901_source%TYPE
   )
      RETURN VARCHAR2
   IS
      v_name   VARCHAR2 (150);
   BEGIN
      BEGIN
         SELECT get_code_name (c901_ship_to) shipto
           INTO v_name
           FROM t907_shipping_info t907
          WHERE t907.c907_ref_id = p_refid
            AND t907.c901_source = p_source
            AND c907_void_fl IS NULL;                           --50180 orders
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            RETURN '';
      END;

      RETURN v_name;
   END get_shipto;

   /******************************************************************
          * Description : function to get shipto_id
          ****************************************************************/
   FUNCTION get_shipto_id (
      p_refid    IN   t501_order.c501_order_id%TYPE,
      p_source   IN   t907_shipping_info.c901_source%TYPE
   )
      RETURN VARCHAR2
   IS
      v_name   VARCHAR2 (150);
   BEGIN
      BEGIN
         SELECT get_code_name (c907_ship_to_id) shiptoid
           INTO v_name
           FROM t907_shipping_info t907
          WHERE t907.c907_ref_id = p_refid
            AND t907.c901_source = p_source
            AND c907_void_fl IS NULL;                           --50180 orders
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            RETURN '';
      END;

      RETURN v_name;
   END get_shipto_id;

   /******************************************************************
        * Description : function to get_track_no
        ****************************************************************/
   FUNCTION get_track_no (
      p_refid    IN   t501_order.c501_order_id%TYPE,
      p_source   IN   t907_shipping_info.c901_source%TYPE
   )
      RETURN VARCHAR2
   IS
      v_name   VARCHAR2 (150);
   BEGIN
      BEGIN
         SELECT c907_tracking_number trackno
           INTO v_name
           FROM t907_shipping_info t907
          WHERE t907.c907_ref_id = p_refid
            AND t907.c901_source = p_source
            AND c907_void_fl IS NULL;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            RETURN '';
      END;

      RETURN v_name;
   END get_track_no;

   /******************************************************************
      * Description : function to   get_carrier
      ****************************************************************/
   FUNCTION get_carrier (
      p_refid    IN   t501_order.c501_order_id%TYPE,
      p_source   IN   t907_shipping_info.c901_source%TYPE
   )
      RETURN VARCHAR2
   IS
      v_name   VARCHAR2 (150);
   BEGIN
      BEGIN
         SELECT get_code_name (c901_delivery_carrier) carrier
           INTO v_name
           FROM t907_shipping_info t907
          WHERE t907.c907_ref_id = p_refid
            AND t907.c901_source = p_source
            AND c907_void_fl IS NULL;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            RETURN '';
      END;

      RETURN v_name;
   END get_carrier;

   /******************************************************************
      * Description : function to   get_mode
      ****************************************************************/
   FUNCTION get_ship_mode (
      p_refid    IN   t501_order.c501_order_id%TYPE,
      p_source   IN   t907_shipping_info.c901_source%TYPE
   )
      RETURN VARCHAR2
   IS
      v_name   VARCHAR2 (150);
   BEGIN
      BEGIN
         SELECT get_code_name (c901_delivery_mode) shipmode
           INTO v_name
           FROM t907_shipping_info t907
          WHERE t907.c907_ref_id = p_refid
            AND t907.c901_source = p_source
            AND c907_void_fl IS NULL;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            RETURN '';
      END;

      RETURN v_name;
   END get_ship_mode;

   /******************************************************************
     * Description : function to completed_by
     ****************************************************************/
   FUNCTION get_completed_by (
      p_refid    IN   t501_order.c501_order_id%TYPE,
      p_source   IN   t907_shipping_info.c901_source%TYPE,
      p_status   IN   t907_shipping_info.c907_status_fl%TYPE
   )
      RETURN VARCHAR2
   IS
      v_name     VARCHAR2 (150);
      v_source   t901_code_lookup.c901_code_id%TYPE;
      v_status   t901_code_lookup.c901_code_id%TYPE;
   BEGIN
      BEGIN
         SELECT DECODE (p_source,
                        50180, 91102,
                        50181, 91101,
                        50182, 91103,
                        50183, 91104
                       )
           INTO v_source
           FROM DUAL;

         IF (p_source = '50183' AND p_status = 40)              -- loaner extn
         THEN
            SELECT DECODE (p_status,
                           '15', '91110',
                           '30', '91111',
                           '40', '91113'
                          )
              INTO v_status
              FROM DUAL;
         ELSIF (p_source = '50182' AND p_status = 40)                --loaners
         THEN
            -- below is a temp fix for multiple values for loaners
            RETURN '';

            SELECT DECODE (p_status, '15', '91120', '40', '91121')
              INTO v_status
              FROM DUAL;
         ELSIF (p_source = '50180' AND p_status = 40)                 --orders
         THEN
            SELECT DECODE (p_status,
                           '15', '91170',
                           '30', '91171',
                           '40', '91173'
                          )
              INTO v_status
              FROM DUAL;
         ELSIF (p_source = '50181' AND p_status = 40)                --consign
         THEN
            SELECT DECODE (p_status,
                           '15', '91160',
                           '30', '91161',
                           '40', '91165'
                          )
              INTO v_status
              FROM DUAL;
         END IF;

         SELECT get_user_sh_name (t905.c905_updated_by)
           INTO v_name
           FROM t905_status_details t905
          WHERE t905.c905_ref_id = p_refid
            AND t905.c901_type = v_status
            AND t905.c901_source = v_source;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            RETURN '';
      END;

      RETURN v_name;
   END get_completed_by;

      /******************************************************************
   * Description : Procedure to get the shipping info for mailing
   ****************************************************************/
   PROCEDURE gm_fch_shipped_today (
      p_mailshipdtl   OUT   TYPES.cursor_type,
      p_ruledtl       OUT   TYPES.cursor_type
   )
   AS
   v_company_id    t1900_company.c1900_company_id%TYPE;
   v_plant_id      t5040_plant_master.c5040_plant_id%TYPE;
   v_loaner_filter VARCHAR2(20);
   BEGIN
	  --Getting company id from context.   
    SELECT get_compid_frm_cntx() , get_plantid_frm_cntx()
	  INTO v_company_id , v_plant_id
	  FROM dual;
	  
	 SELECT DECODE(
            GET_RULE_VALUE(v_company_id,'LOANESHIPEMAILFILTER'), null, v_company_id, v_plant_id)
      INTO v_loaner_filter 
      FROM DUAL;
	  --
      OPEN p_mailshipdtl
       FOR
          SELECT DISTINCT *
                     FROM (SELECT   t907.c901_ship_to shiptotype,t907.c907_ship_to_id shiptoorg,
                                    DECODE
                                       (t907.c901_ship_to,
                                        4122, get_rep_id (t907.c907_ship_to_id),
                                        t907.c907_ship_to_id
                                       ) shiptoid,
                                    t907.c907_ref_id refidactual,
                                    DECODE (t907.c901_source,
                                            50182, t504a.c504a_etch_id,
                                            t907.c907_ref_id
                                           ) refid,
                                    DECODE
                                       (t907.c901_source,
                                        50181, DECODE (t504.c207_set_id,
                                                       '', 'Item Consignment',
                                                       'Set Consignment'
                                                      ),
                                        50180, DECODE (t501.c901_order_type,
                                                       2525, 'Back Order',
                                                       'Orders'
                                                      ),
                                        get_code_name (t907.c901_source)
                                       ) SOURCE,
                                    t907.c901_source sourceid,
                                    get_cs_ship_name
                                               (t907.c901_ship_to,
                                                t907.c907_ship_to_id
                                               ) shiptonm,
                                    NVL
                                       (get_ship_email_id
                                                         (t907.c901_ship_to,
                                                          t907.c907_ship_to_id,
                                                          93090
                                                         ),
                                        get_rule_value_by_company ('GMCSTOMAILID',
                                                        'SHIPDTL', t907.c1900_company_id
                                                       )
                                       ) shipemail,
                                    get_rule_value
                                       (NVL
                                           (t504.c701_distributor_id,
                                            get_distributor_id
                                                       (t501.c703_sales_rep_id)
                                           ),
                                        'CC-LIST-SHIPMENTS'
                                       ) cclist,
                                    get_code_name
                                               (t907.c901_delivery_mode)
                                                                        smode,
                                    get_code_name
                                            (t907.c901_delivery_carrier)
                                                                        scarr,
                                    t907.c907_tracking_number track,
                                    t504.c207_set_id setid,
                                    get_set_name (t504.c207_set_id) setname,
                                    t504a.c504a_etch_id etchid,
                                     DECODE (t907.c901_ship_to, 4121, Gm_pkg_cm_shipping_info.get_salesrepaddress (
								        t907.c106_address_id, t907.c907_ship_to_id), 4123, Gm_pkg_cm_shipping_info.get_empaddress (
								        t907.c106_address_id, t907.c907_ship_to_id), gm_pkg_cm_shipping_info.get_address (t907.c901_ship_to,
								        t907.c907_ship_to_id)) address ,t907.c901_delivery_carrier dcrr,
								        get_ship_email_id (t907.c901_ship_to, get_rep_id_by_ass_id(t907.c907_ship_to_id),93090) ccrepemail,
								        t504.c504_type TYPE,'' HYPERLINK, t907.C907_GUID guid, get_company_dtfmt(t907.c1900_company_id) transDTFmt
                               FROM t907_shipping_info t907,
                                    t504_consignment t504,
                                    t504a_consignment_loaner t504a,
                                    t906_rules t906,
                                    t501_order t501 --,
                                    --t501a_order_attribute t501a,
                                    --t504d_consignment_attribute t504d
                              WHERE t907.c907_void_fl IS NULL
                                AND t907.c907_active_fl IS NULL
                                AND t907.c907_status_fl = 40
                                AND t907.c907_email_update_fl IS NULL
                                AND t906.c906_rule_grp_id = 'SHIPS'
                                AND TO_NUMBER (t906.c906_rule_id) =
                                       DECODE (t907.c901_source,
                                               50180, DECODE
                                                        (t501.c901_order_type,
                                                         2525, t501.c901_order_type,
                                                         50180
                                                        ),
                                               t907.c901_source
                                              )
                                AND t907.c907_ref_id = t504.c504_consignment_id(+)
                                AND t504.c504_consignment_id = t504a.c504_consignment_id(+)
                               -- AND t504.c504_consignment_id = t504d.c504_consignment_id(+)
                                AND t907.c907_tracking_number IS NOT NULL
                                AND t907.c907_ship_to_id <> 0
                                AND t907.c901_delivery_mode NOT IN (select c906_rule_value 
                                										from t906_rules 
                                										where c906_rule_id = 'DELMODEEXCL' 
                                										and C906_RULE_GRP_ID = 'DELMODE')
                                                         -- for hand delivered
                                AND t907.c907_ref_id = t501.c501_order_id(+)
                                -- AND t501.c501_order_id = t501a.c501_order_id(+)
                                -- AND NVL (t501a.c901_attribute_type, -999) !=11241
                                -- AND NVL (t504d.c901_attribute_type, -999) !=11241 
                                AND (t907.c1900_company_id = v_loaner_filter OR t907.c5040_plant_id = v_loaner_filter) 
                               	)ORDER BY shiptoid ,track;
      
      OPEN p_ruledtl
       FOR
          SELECT c906_rule_id ruleid, c906_rule_desc description,
                 c906_rule_value rulevalue
            FROM t906_rules
           WHERE c906_rule_grp_id = 'SHIPDTL'
           AND c1900_company_id = v_company_id;           
   END gm_fch_shipped_today;

/******************************************************************
* Description : Procedure to get the unique tracking numbers
****************************************************************/
   PROCEDURE gm_fch_unique_tracknos (
      p_shipid         IN       t907_shipping_info.c907_shipping_id%TYPE,
      p_trackinfocur   OUT      TYPES.cursor_type
   )
   AS
      v_shipto      t907_shipping_info.c901_ship_to%TYPE;
      v_shiptoid    t907_shipping_info.c907_ship_to_id%TYPE;
      v_addressid   t907_shipping_info.c106_address_id%TYPE;
   BEGIN
      SELECT c901_ship_to, c907_ship_to_id, c106_address_id
        INTO v_shipto, v_shiptoid, v_addressid
        FROM t907_shipping_info
       WHERE c907_shipping_id = p_shipid AND c907_void_fl IS NULL;

      OPEN p_trackinfocur
       FOR
          SELECT c907_tracking_number ID,
                    c907_tracking_number
                 || '/'
                 || get_code_name (c901_source)
                 || '/'
                 || get_code_name (c901_delivery_mode) NAME
            FROM t907_shipping_info
           WHERE (c901_record_type = 50970 OR c901_record_type IS NULL
                 )                                             --50970(MASTER)
             AND c901_ship_to = v_shipto
             AND c907_ship_to_id = v_shiptoid
             AND c106_address_id = v_addressid
             AND c907_void_fl IS NULL
             AND c907_tracking_number != 'Modify Track#'
             AND c907_shipped_dt = TRUNC (CURRENT_DATE);
   END gm_fch_unique_tracknos;

      /******************************************************************
      * Description : Returns the pending control number status for a given id
   ****************************************************************/
   FUNCTION get_pendcontrol_status (
      p_refid    IN   t907_shipping_info.c907_ref_id%TYPE,
      p_source   IN   t907_shipping_info.c901_source%TYPE
   )
      RETURN NUMBER
   IS
      v_status   t901_code_lookup.c901_code_id%TYPE;
   BEGIN
      IF (p_source = '50180')                                        --orders
      THEN
         SELECT DECODE (t501.c501_status_fl, 1, 15, 0)
           INTO v_status
           FROM t501_order t501
          WHERE t501.c501_void_fl IS NULL AND t501.c501_order_id = p_refid;
      ELSIF (p_source = '50181')                                     --consign
      THEN
         SELECT DECODE (t504.c207_set_id,
                        NULL, DECODE (t504.c504_status_fl, '2', 15, 0),
                        0
                       )
           INTO v_status
           FROM t504_consignment t504
          WHERE t504.c504_void_fl IS NULL
            AND t504.c504_type IN (4110, 40057)
            AND t504.c504_consignment_id = p_refid;
      ELSIF (p_source = '50182')                                     --Loaners
      THEN
         v_status := 0;
      ELSIF (p_source = '50183')                                --Loaners extn
      THEN
         SELECT DECODE (t412.c412_status_fl, 2, 15, 3, 15, 0)
           INTO v_status
           FROM t412_inhouse_transactions t412
          WHERE t412.c412_void_fl IS NULL
            AND t412.c412_status_fl <= 3
            AND t412.c412_inhouse_trans_id = p_refid;
      END IF;

      RETURN v_status;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN 0;
   END get_pendcontrol_status;

/******************************************************************
    * Author      :  Gopinathan
    *
    * Description : Fetch the hold flag status by passing the ref_id and source type
   ****************************************************************/
   FUNCTION get_hold_flg (
      p_id       IN   t504_consignment.c504_consignment_id%TYPE,
      p_source   IN   VARCHAR2
   )
      RETURN VARCHAR2
   IS
      v_hold_fl   t504_consignment.c504_hold_fl%TYPE;
      v_cnt       NUMBER;
   BEGIN
      IF (p_source = '50182')
      THEN
         SELECT NVL (c504_hold_fl, NULL)
           INTO v_hold_fl
           FROM t504_consignment
          WHERE c504_consignment_id = p_id AND c504_void_fl IS NULL;

         RETURN v_hold_fl;
      ELSIF (p_source = '50180')
      THEN
         SELECT COUNT (1)
           INTO v_cnt
           FROM t412_inhouse_transactions t412, t413_inhouse_trans_items t413
          WHERE c412_ref_id = p_id
            AND c412_void_fl IS NULL
            --AND t412.c412_inhouse_trans_id LIKE '%GM-PS%'
            AND c412_type = '100406'              --Pending shippping to Shelf
            AND t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
            AND c413_control_number IS NULL;
      --50181 consignment, if there's open PSFG txn, then count as hold.
      ELSIF (p_source = '50181')
      THEN
      	 SELECT COUNT (1)
           INTO v_cnt
           FROM t412_inhouse_transactions t412, t413_inhouse_trans_items t413
          WHERE c412_ref_id = p_id
            AND c412_void_fl IS NULL 
            AND c412_type = '100406'              --Pending shippping to Shelf
            AND t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id 
            AND c412_status_fl < 4;
      END IF;
      IF v_cnt > 0 THEN
      	RETURN 'Y';
      ELSE
        RETURN '';
      END IF;
   END get_hold_flg;
   /**************************************************************************************************************
	* Description : Procedure to get the Complete[including State,Country and ZIP Code] Address for a Slaes Rep
   **************************************************************************************************************/
	FUNCTION get_salesrepaddress (
		p_address_id	    IN		t907_shipping_info.c106_address_id%TYPE,
		p_rep_id			IN      t907_shipping_info.c907_ship_to_id%TYPE	 
	)
	RETURN VARCHAR2
	IS 
		v_addressid	   t907_shipping_info.c106_address_id%TYPE;
		v_shipadd      VARCHAR2(1000);
	BEGIN
		IF p_address_id  IS NULL  THEN				
			SELECT c106_address_id
			  INTO v_addressid
			  FROM t703_sales_rep t703, t106_address t106
             WHERE t703.c101_party_id     = t106.c101_party_id            
			   AND t703.c703_sales_rep_id = p_rep_id
               AND t106.c106_primary_fl   = 'Y'
               AND t703.c703_active_fl='Y'
               AND t106.c106_void_fl IS NULL 
               AND t703.c703_void_fl IS NULL
               AND ROWNUM = 1;                 
        ELSE 
        	v_addressid := p_address_id;        	
        END IF;    	
			SELECT t703.c703_sales_rep_name
                   || '<BR>&'||'nbsp;'
                   || SUBSTR (get_code_name (c901_address_type)
                           || '-'
                           || DECODE(c106_add1,NULL,'',c106_add1||'<BR>&'||'nbsp;')                        
                           || DECODE(c106_add2,NULL,'',c106_add2||'<BR>&'||'nbsp;')                           
                           || c106_city
                           || '<BR>&'||'nbsp;'
                           || get_code_name_alt(c901_state)
                           || ' '
                           || c106_zip_code,                                           
                           0,
                           220
                          )
              INTO v_shipadd
              FROM t703_sales_rep t703, t106_address t106
             WHERE t703.c101_party_id 	= t106.c101_party_id
               AND t106.c106_address_id = TO_NUMBER (v_addressid)
               AND t703.c703_active_fl='Y'
               AND t703.c703_void_fl IS NULL
               AND ROWNUM = 1;                
      RETURN v_shipadd;         
	END get_salesrepaddress;
	/**************************************************************************************************************
	* Description : Procedure to get the Complete[including State,Country and ZIP Code] Address for a Slaes Rep
   **************************************************************************************************************/
	FUNCTION get_empaddress (
		p_address_id	    IN		t907_shipping_info.c106_address_id%TYPE,
		p_emp_id			IN      t907_shipping_info.c907_ship_to_id%TYPE	 
	)
	RETURN VARCHAR2
	IS 
		v_addressid	   t907_shipping_info.c106_address_id%TYPE;
		v_shipadd      VARCHAR2(1000);
	BEGIN
		
		IF p_address_id  IS NULL  THEN	
		BEGIN
			SELECT c106_address_id
			  INTO v_addressid
			  FROM t101_user t101, t106_address t106
             WHERE t101.c101_party_id     = t106.c101_party_id            
			   AND t101.c101_user_id = p_emp_id
               AND t106.c106_primary_fl   = 'Y'
               AND C901_USER_STATUS = '311'
               AND t106.c106_void_fl IS NULL 
               AND ROWNUM = 1; 
        EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               v_addressid := NULL;
        	 END;
        ELSE 
        	v_addressid := p_address_id;        	
        END IF; 
        
        IF v_addressid  IS NULL  THEN	        
        SELECT get_user_name (c101_user_id) || '<BR>&'||'nbsp;' || c101_address
           INTO v_shipadd
           FROM t101_user
          WHERE c101_user_id = p_emp_id;
        ELSE
			SELECT get_user_name (t101.c101_user_id)
                   || '<BR>&'||'nbsp;'
                   || SUBSTR (get_code_name (c901_address_type)
                           || '-'
                           || DECODE(c106_add1,NULL,'',c106_add1||'<BR>&'||'nbsp;')                        
                           || DECODE(c106_add2,NULL,'',c106_add2||'<BR>&'||'nbsp;')                           
                           || c106_city
                           || '<BR>&'||'nbsp;'
                           || get_code_name_alt(c901_state)
                           || ' '
                           || c106_zip_code,                                           
                           0,
                           220
                          )
              INTO v_shipadd
              FROM t101_user t101, t106_address t106
             WHERE t101.c101_party_id 	= t106.c101_party_id
               AND t106.c106_address_id = TO_NUMBER (v_addressid);
        END IF;
      RETURN v_shipadd;         
	END get_empaddress;
	
	 /******************************************************************
	* Description : Function to get the status of apply all check box status in modify shipping screen
	****************************************************************/
	FUNCTION gm_fch_ship_applyall_sts (
		p_shipid		 IN 	  t907_shipping_info.c907_shipping_id%TYPE
	)
	RETURN VARCHAR2
	IS 		
	v_ship_status      VARCHAR2(1):='N';
	v_status    	   t907_shipping_info.c907_status_fl%TYPE;
	v_req_id           t907_shipping_info.c525_product_request_id%TYPE;
	BEGIN
		BEGIN
		 SELECT c907_status_fl, c525_product_request_id 
		 INTO v_status,v_req_id
		   FROM t907_shipping_info
		   WHERE c907_shipping_id = p_shipid
		   AND c907_void_fl    IS NULL;
		EXCEPTION
         WHEN NO_DATA_FOUND
          THEN
              RETURN v_ship_status;
        END;
    
		 IF v_status <> '40' THEN   
		 	 SELECT (CASE WHEN COUNT (*) > 0 THEN 'Y' ELSE 'N' END) INTO v_ship_status
			    FROM t907_shipping_info
			    WHERE c525_product_request_id = v_req_id
			    AND c907_status_fl         <> 40
			    AND c907_void_fl           IS NULL 
			    AND c907_active_fl         IS NULL 
			    AND c907_shipping_id       <> p_shipid;			    
		 ELSE		 
		    v_ship_status := 'N';		    
		 END IF;
		
		RETURN v_ship_status;         
	END gm_fch_ship_applyall_sts;
	
/****************************************************************************************
* Description : Procedure to get the Consigment , Qty , Price for selected Consigment ids.
*****************************************************************************************/
  PROCEDURE gm_fch_consin_shipment_val(
      p_ConsignIds    IN       VARCHAR2,
      p_shipinfocur   OUT      TYPES.cursor_type
   )
   AS
   v_consign_ids		VARCHAR(4000);
   v_country_code 		VARCHAR (10);
   v_curr_code		VARCHAR (10);
   v_company_id    t1900_company.c1900_company_id%TYPE;
   
   CURSOR ConsignId_Details
		IS
			SELECT c907_ref_id  refid FROM t907_shipping_info WHERE c901_source IN ('50181','50180') -- Consignement, Order
			AND c907_ref_id IN(SELECT token FROM v_in_list WHERE  token IS NOT NULL)
			AND c907_void_fl IS NULL	AND C907_ACTIVE_FL  IS NULL
		--	AND c907_status_fl   = 40 -- Now get consigment id before shipment.
            AND c901_source NOT IN(
                 SELECT c906_rule_value FROM t906_rules WHERE c906_rule_grp_id = 'EXC_SOURCE'
            );
             
   BEGIN
	 --Getting company id from context.   
    SELECT get_compid_frm_cntx()
	  INTO v_company_id 
	  FROM dual;
	
		--in US no CURRCODE enrty, so should be 'USD', here we got CURRENCY CODE instead of Symbol
		v_curr_code := get_comp_curr_symbol (v_company_id);
		
	  my_context.set_my_inlist_ctx (p_ConsignIds);
	   	
  	  FOR currindex1 IN ConsignId_Details
		LOOP
			v_consign_ids := v_consign_ids || ',' || currindex1.refid;
			-- When select shipment value from shipping screen will be display shipment value.
			gm_fetch_shipment_price(currindex1.refid);
		END LOOP;
    
		my_context.set_my_inlist_ctx (v_consign_ids);
		
     OPEN p_shipinfocur
       FOR
        SELECT T504.C504_CONSIGNMENT_ID, SUM(NVL (T505.C505_ITEM_QTY, 0))
            IQTY, SUM(NVL (get_account_part_pricing (NULL, t505.c205_part_number_id, get_distributor_inter_party_id (t504.c701_distributor_id)), 0)) PRICE
            ,SUM(NVL (T505.C505_ITEM_QTY, 0) *  NVL (get_account_part_pricing (NULL, t505.c205_part_number_id, get_distributor_inter_party_id (t504.c701_distributor_id)), 0)) TOTAL,
            DECODE(GET_DISTRIBUTOR_INTER_PARTY_ID(T504.C701_DISTRIBUTOR_ID),NULL, v_curr_code, get_rule_value (50218, get_distributor_inter_party_id (T504.c701_distributor_id))) NEWCURRENCY
          FROM t504_consignment t504,t505_item_consignment t505
    	  WHERE T504.C504_CONSIGNMENT_ID IN(SELECT TOKEN FROM V_IN_LIST WHERE  TOKEN IS NOT NULL)
    		AND T504.C504_CONSIGNMENT_ID = T505.C504_CONSIGNMENT_ID
    		AND T505.C505_VOID_FL       IS NULL
    		AND T504.C504_VOID_FL       IS NULL
            AND NVL(t504.c504_inhouse_purpose, -999) <> 50061 -- ICT Sample 
            --add nvl here in case purpose is null
    	GROUP BY T504.C504_CONSIGNMENT_ID,C701_DISTRIBUTOR_ID
    	--including orders in shipment value
    	UNION      
		  SELECT t501.C501_ORDER_ID C504_CONSIGNMENT_ID,
		  SUM(C502_ITEM_QTY) IQTY,
		  SUM(C502_ITEM_PRICE) PRICE,
		  SUM(C502_ITEM_QTY * C502_ITEM_PRICE) TOTAL,
		  v_curr_code NEWCURRENCY
		FROM T502_ITEM_ORDER T502,
		  t501_order t501
		WHERE t501.C501_ORDER_ID IN (SELECT TOKEN FROM V_IN_LIST WHERE  TOKEN IS NOT NULL)
		AND C502_DELETE_FL      IS NULL
		AND t501.c501_void_fl   IS NULL
		AND t502.c502_void_fl   IS NULL
		AND t501.C501_ORDER_ID   = t502.C501_ORDER_ID
		GROUP BY t501.C501_ORDER_ID;
    
   END gm_fch_consin_shipment_val;
   	
/********************************************************************
* Description : Update planned ship date selected in consignment ids.
*********************************************************************/
  PROCEDURE gm_update_planned_shipdate(
      p_ConsignIds    IN       VARCHAR2,
      p_planedShipDt  IN	   VARCHAR2,
      p_userid	 	  IN	   VARCHAR2
   )
   AS
   v_consign_ids		VARCHAR(4000);
   v_datefmt	VARCHAR2(10);   
   
   CURSOR ConsignId_Details
		IS
			SELECT c907_ref_id  refid FROM t907_shipping_info WHERE c901_source = '50181' -- Consignement
			AND c907_ref_id IN(SELECT token FROM v_in_list WHERE  token IS NOT NULL)
			AND c907_void_fl IS NULL	AND C907_ACTIVE_FL  IS NULL
			AND NVL (c907_status_fl, -999) != 40
            AND c901_source NOT IN(
                 SELECT c906_rule_value FROM t906_rules WHERE c906_rule_grp_id = 'EXC_SOURCE'
            );
   BEGIN
 
   		select sys_context('CLIENTCONTEXT','COMP_DATE_FMT') into v_datefmt from dual;
        my_context.set_my_inlist_ctx (p_ConsignIds);
	
        FOR currindex1 IN ConsignId_Details
			LOOP
			   	UPDATE t907_shipping_info
				   SET C907_PLANNED_SHIP_DATE = TO_DATE (p_planedShipDt, v_datefmt)
					 , c907_last_updated_by = p_userid
					 , c907_last_updated_date = CURRENT_DATE
			 	WHERE c907_ref_id = currindex1.refid;
			 	
   		END LOOP;    
   END gm_update_planned_shipdate;

/********************************************************************
* Description : To get company filter information.
* Author : Velu
* param : Dist_id, CompId, DivID, AreaID, ZoneID
*********************************************************************/
   
   PROCEDURE gm_cs_fch_company_info (   
	   p_dist_id 	IN t701_distributor.c701_distributor_id%TYPE,
	   p_comp_id 	OUT t901_code_lookup.C901_CODE_ID%TYPE,
	   p_div_id 	OUT t901_code_lookup.C901_CODE_ID%TYPE,
	   p_area_id 	OUT t901_code_lookup.C901_CODE_ID%TYPE,
	   p_zone_id 	OUT t901_code_lookup.C901_CODE_ID%TYPE
 )
   AS
     
   BEGIN
		BEGIN   
			SELECT  t710.c901_company_id,t710.c901_division_id,t710.c901_area_id,t710.c901_zone_id
			INTO  	p_comp_id,p_div_id,p_area_id,p_zone_id
			FROM	 t701_distributor t701
			        ,t710_sales_hierarchy t710
					,t901_code_lookup t901
			WHERE	t701.c701_region = t710.c901_area_id
			AND     t901.c901_code_id = t710.c901_area_id
			AND     t710.c710_active_fl = 'Y'
			AND     t701.c701_void_fl IS NULL
			AND     t701.c701_distributor_id = p_dist_id;

		EXCEPTION
			WHEN NO_DATA_FOUND
				THEN
				p_comp_id := 100800; -- Globus Medical India Inc
				p_div_id  := 100823; -- US
				p_area_id := null;
				p_zone_id := null;
		END;	
END gm_cs_fch_company_info;
   
/********************************************************************
* Description : To get company filter information.
* Author : Velu
* param : Dist_id, CompId, DivID, AreaID, ZoneID
*********************************************************************/
  
   PROCEDURE gm_update_compnay_info (   
	   p_userid 	IN VARCHAR
   )
   AS
   CURSOR shipping_infor IS
     	SELECT  c907_shipping_id SID,c907_ref_id refid,c901_source SOURCE
         FROM t907_shipping_info WHERE c907_void_fl  IS NULL
          AND C907_ACTIVE_FL   IS NULL  AND c907_status_fl = 30 -- 30: Pending Shipping
          AND c901_source NOT  IN (SELECT c906_rule_value FROM t906_rules WHERE c906_rule_grp_id = 'EXC_SOURCE' );
            
    v_dist_id 		t501_order.c501_distributor_id%TYPE;
    v_comp_id 		t901_code_lookup.C901_CODE_ID%TYPE:=100800;
	v_div_id 		t901_code_lookup.C901_CODE_ID%TYPE:=100823;
	v_area_id 		t901_code_lookup.C901_CODE_ID%TYPE;
	v_zone_id 		t901_code_lookup.C901_CODE_ID%TYPE;
  	 
  	 
   BEGIN 
	  FOR curindex IN shipping_infor
			LOOP 
			
		  IF(curindex.SOURCE = '50184' OR curindex.SOURCE = '50181') THEN-- Consignment  
	    	  	v_dist_id := get_distid_from_requestid(curindex.refid);
	    	ELSIF(curindex.SOURCE = '50180') THEN-- order
	   			v_dist_id := get_distid_from_orderid(curindex.refid);
	    	ELSIF(curindex.SOURCE = '50183') THEN-- Loaner ext
	   			v_dist_id := get_distid_from_loaner_transid(curindex.refid);
			ELSIF(curindex.SOURCE = '50182' OR curindex.SOURCE = '50186') THEN -- Loaner, InHouse Loaner
				v_dist_id := get_distid_from_cons_loanerid(curindex.refid);
		 END IF;
		
		IF(v_dist_id IS NOT NULL) THEN
			gm_pkg_cm_shipping_info.gm_cs_fch_company_info(v_dist_id,v_comp_id,v_div_id,v_area_id,v_zone_id);
		END IF;
				
				UPDATE t907_shipping_info 
				SET C901_COMPANY_ID  = v_comp_id,
				C901_DIVISION_ID  = v_div_id,
				C901_AREA_ID  = v_area_id,
				C901_ZONE_ID  = v_zone_id,
				c701_distributor_id  = v_dist_id,
				c907_last_updated_date = CURRENT_DATE,
				c907_last_updated_by = p_userid
				WHERE C907_SHIPPING_ID = curindex.SID
				AND c907_ref_id =  curindex.refid;
		END LOOP;
   END gm_update_compnay_info; 
   
/*******************************************************************************************************
* Description : When select shipment value will be display shpiments in Shpping report screen.
* Author	  : Velu
********************************************************************************************************/
  PROCEDURE gm_fetch_shipment_price(
      p_consign_id    IN       t504_consignment.c504_consignment_id%TYPE
   )
   AS
   v_dist_id			t504_consignment.c701_distributor_id%TYPE;
   v_inter_party_id 	t101_party.c101_party_id%TYPE;
   v_err_val 			VARCHAR(4000) := '';
   
   	CURSOR pop_val
	IS
		SELECT b.c505_item_consignment_id seqid
			 , get_account_part_pricing (NULL, b.c205_part_number_id, v_inter_party_id) price
			 , b.c205_part_number_id partnum
		FROM   t504_consignment a, t505_item_consignment b
		 WHERE a.c504_consignment_id = b.c504_consignment_id
		   AND a.c504_consignment_id = p_consign_id
		   AND b.c505_item_qty <> 0;
		   
   BEGIN
	   
  	   BEGIN
	  	    SELECT	c701_distributor_id INTO v_dist_id
  	   		FROM   t504_consignment t504
	   		WHERE t504.c504_consignment_id = p_consign_id
	   		AND t504.c504_void_fl IS NULL;
	   EXCEPTION 
	  	 WHEN NO_DATA_FOUND	 THEN
	  	 	v_dist_id := NULL;
	  	END;
	  	
	  BEGIN
	  	 SELECT  get_distributor_inter_party_id (v_dist_id)
		  INTO  v_inter_party_id  FROM DUAL;
	  EXCEPTION 
	  	 WHEN NO_DATA_FOUND	 THEN
	  	 	v_inter_party_id := NULL;
	  END;

  	FOR rad_val IN pop_val
	LOOP
		IF rad_val.price IS NULL AND v_inter_party_id IS NOT NULL AND v_dist_id IS NOT NULL 
		THEN
			v_err_val := v_err_val || rad_val.partnum ||', ';
		END IF;
	END LOOP;
	
	IF v_err_val  IS NOT NULL OR v_err_val != '' THEN
		GM_RAISE_APPLICATION_ERROR('-20999','15',v_err_val);
	END IF;
   END gm_fetch_shipment_price;

   /*******************************************************************************************************
* Description : Fetch all the txn types that are in pending shipping and their count
* Author	  : VPrasath
********************************************************************************************************/

 PROCEDURE gm_fch_pend_ship_cnt(    
 	p_party_id   IN    t101_user.c101_party_id%TYPE
 	,p_out_cursor OUT   TYPES.cursor_type  
 )
 AS
 	v_plant_id    t5040_plant_master.c5040_plant_id%TYPE;
 	v_building_id t907_shipping_info.c5057_building_id%TYPE;  
 BEGIN
  SELECT  NVL(get_plantid_frm_cntx(),get_rule_value('DEFAULT_PLANT','ONEPORTAL')) INTO  v_plant_id FROM DUAL;
  
	gm_pkg_op_inv_location_module.gm_fch_user_building_id(p_party_id, '100025', v_building_id);  --added for PMT-34028 Shipping Device - Dashboard Filter
	
  OPEN p_out_cursor FOR
    SELECT  t907.source source, t907.txntypenm txntypenm, t907.txncnt txncnt FROM 
      (SELECT c901_source source
            , get_code_name(c901_source) txntypenm
            , COUNT(1) txncnt
            , DECODE(c901_source,50180,1,50182,2,50183,4,50186,6,100) Seq
         FROM t907_shipping_info t907
        WHERE c901_source IN (50182, 50180, 50183, 50186)
          AND c907_status_fl = 30 -- Pending Shipping
          AND c907_void_fl IS NULL
          AND (NVL(t907.c5057_building_id,'-999') = NVL(v_building_id, NVL(t907.c5057_building_id,'-999')) OR t907.c5057_building_id IS NULL)    --added for PMT-34028 Shipping Device - Dashboard Filter
          AND c5040_plant_id = v_plant_id
     GROUP BY c901_source
    UNION ALL
       SELECT c901_source txntype
            , 'Set Consignment' txntypenm
            , COUNT(1) txncnt
            , 5 Seq
         FROM t907_shipping_info t907, t504_consignment t504
        WHERE c901_source IN (50181) -- Consignment
          AND c907_status_fl = 30  -- Pending Shipping
          AND c907_void_fl IS NULL
          AND t907.c907_ref_id = t504.c504_consignment_id
          AND t504.c207_set_id IS NOT NULL -- Set Consignment
          AND (NVL(t907.c5057_building_id,'-999') = NVL(v_building_id, NVL(t907.c5057_building_id,'-999')) OR t907.c5057_building_id IS NULL)   --added for PMT-34028 Shipping Device - Dashboard Filter
          AND t907.c5040_plant_id = v_plant_id
     GROUP BY c901_source
    UNION ALL
       SELECT c901_source txntype
                  , 'Item Consignment' txntypenm
                  , COUNT(1) txncnt
                  , 3 Seq
         FROM t907_shipping_info t907, t504_consignment t504
        WHERE c901_source IN (50181) -- Consignment
          AND c907_status_fl = 30  -- Pending Shipping
          AND c907_void_fl IS NULL
          AND t907.c907_ref_id = t504.c504_consignment_id
          AND t504.c207_set_id IS NULL -- Item Consignment
          AND (NVL(t907.c5057_building_id,'-999') = NVL(v_building_id, NVL(t907.c5057_building_id,'-999')) OR t907.c5057_building_id IS NULL)    --added for PMT-34028 Shipping Device - Dashboard Filter
          AND t907.c5040_plant_id = v_plant_id
     GROUP BY c901_source) t907
     ORDER BY Seq;


 END gm_fch_pend_ship_cnt;	 

  /******************************************************************
   * Description : Procedure to get the address info like city, state, zip etc 
   ****************************************************************/
	PROCEDURE gm_fch_ship_address (
		p_refid 		IN		 t907_shipping_info.c907_ref_id%TYPE
	  , p_source		IN		 t907_shipping_info.c901_source%TYPE
	  , p_shipaddrcur	OUT 	 TYPES.cursor_type
	)
	AS
		v_ship         VARCHAR2 (50);
		v_ship_id      VARCHAR2 (50);
      	v_shipto_id    t907_shipping_info.c907_ship_to_id%TYPE;
      	v_ship_add     VARCHAR2 (1000);
      	v_address_id   t907_shipping_info.c106_address_id%TYPE;
      	v_override_attn t907_shipping_info.c907_override_attn_to%TYPE;
      	v_third_party 	t501_order.c704_account_id%TYPE;
      	v_plant_id      t5040_plant_master.c5040_plant_id%TYPE;
	BEGIN
		BEGIN
  			SELECT  NVL(get_plantid_frm_cntx(),get_rule_value('DEFAULT_PLANT','ONEPORTAL')) INTO  v_plant_id FROM DUAL;
  			
	        IF p_source = 50182 
	        THEN
				SELECT get_code_name (t907.c901_ship_to), t907.c907_ship_to_id,
		                c106_address_id, t907.c907_override_attn_to,t907.c901_ship_to
		           INTO v_ship, v_shipto_id,
		                v_address_id, v_override_attn,v_ship_id
		           FROM t907_shipping_info t907
		          WHERE t907.c907_ref_id = p_refid
		            AND t907.c901_source = p_source
		            AND c907_void_fl IS NULL
		            AND c907_status_fl < 40
		            AND ROWNUM = 1
		            AND t907.c5040_plant_id= v_plant_id
		            ORDER BY t907.c907_last_updated_date desc;
		    ELSE
		    	SELECT get_code_name (t907.c901_ship_to), t907.c907_ship_to_id,
		                c106_address_id, t907.c907_override_attn_to,t907.c901_ship_to
		           INTO v_ship, v_shipto_id,
		                v_address_id, v_override_attn,v_ship_id
		           FROM t907_shipping_info t907
		          WHERE t907.c907_ref_id = p_refid
		            AND t907.c901_source = p_source
		            --AND c907_tracking_number IS NULL
		            AND c907_void_fl IS NULL
		            AND t907.c5040_plant_id= v_plant_id;	            
			END IF;
		EXCEPTION 
		WHEN NO_DATA_FOUND THEN
				GM_RAISE_APPLICATION_ERROR('-20999','16','');
	    END;
		IF v_ship_id IS NULL OR v_ship_id = 'N/A' 
		THEN		
			GM_RAISE_APPLICATION_ERROR('-20999','17','');
		END IF;
		-- The below code is added to fetch the bill to account for third party. Also for back orders to the same account it cannot be third party.
		IF p_source = 50180 
        THEN         	
	     	BEGIN
	         	SELECT c704_account_id
	         	  INTO v_third_party
	         	  FROM t501_order
	         	 WHERE c501_order_id = p_refid
	         	  AND  c501_void_fl IS NULL
	         	  AND  c501_parent_order_id IS NULL
	         	  AND  c5040_plant_id= v_plant_id;
	        EXCEPTION
	        	WHEN NO_DATA_FOUND THEN
	        		v_third_party := '';
	        END;
	    END IF;    
        ---
		IF v_ship = 'Distributor'
         THEN
         	OPEN p_shipaddrcur
       		FOR
            SELECT  t701.c701_distributor_name shipnm 
            	  , v_override_attn attnto
                  , DECODE(t701.c701_ship_add1,NULL,'',t701.c701_ship_add1) addr1
                  , DECODE(t701.c701_ship_add2,NULL,'',t701.c701_ship_add2) addr2
                  , DECODE(t701.c701_ship_zip_code,NULL,'',t701.c701_ship_zip_code) shipzip
                  , DECODE(t701.c701_ship_state ,NULL,'',get_code_name_alt (t701.c701_ship_state)) shipstate
                  , DECODE(t701.c701_ship_city, NULL,'',t701.c701_ship_city) shipcity
                  , DECODE(t701.c701_ship_country,NULL,'',get_code_name_alt(t701.c701_ship_country)) shipcountry
                  , t701.c701_phone_number ph_no
                  , GET_ACCOUNT_ATTRB_VALUE(v_third_party,91980) acctpayorno
                  , NVL(get_rule_value(c701_ship_country,'CNTRYCD'),'US') CountryCode
              FROM t701_distributor t701
             WHERE t701.c701_distributor_id = v_shipto_id;
         --AND c701_void_fl IS NULL;
         ELSIF v_ship = 'Sales Rep' OR v_ship = 'Dealer'
         THEN
            IF v_address_id IS NULL
            THEN
               OPEN p_shipaddrcur
       		   FOR
               SELECT get_rep_name (v_shipto_id) FROM DUAL;
            ELSE
               OPEN p_shipaddrcur
       		   FOR	
               SELECT  get_party_name (t106.c101_party_id) shipnm
               		 , v_override_attn attnto
                     , DECODE(t106.c106_add1,NULL,'',t106.c106_add1) addr1
                     , DECODE(t106.c106_add2,NULL,'',t106.c106_add2) addr2
                     , DECODE(t106.c106_zip_code,NULL,'',t106.c106_zip_code) shipzip
                     , DECODE(t106.c901_state,NULL,'',get_code_name_alt (t106.c901_state)) shipstate
                     , DECODE(t106.c106_city,NULL, '',t106.c106_city) shipcity
                     , DECODE(t106.c901_country,NULL,'',get_code_name_alt(t106.c901_country)) shipcountry
                     , gm_pkg_cm_contact.get_contact_value (t106.c101_party_id, '90450') ph_no
                     , GET_ACCOUNT_ATTRB_VALUE(v_third_party,91980) acctpayorno
                     , NVL(get_rule_value(t106.c901_country,'CNTRYCD'),'US') CountryCode
                 FROM t106_address t106, t907_shipping_info t907
                WHERE t106.c106_address_id = t907.c106_address_id
                  AND t907.c907_ref_id = p_refid
                  AND t907.c901_source = p_source               --50180 orders
                  AND t907.c907_status_fl < 40
                  AND t907.c907_void_fl IS NULL
                  AND t907.c5040_plant_id= v_plant_id;            
            END IF;
         ELSIF v_ship = 'Hospital'
         THEN         	
         	OPEN p_shipaddrcur
       		FOR
            SELECT   t704.c704_account_nm shipnm
            	   , NVL(v_override_attn,c704_ship_attn_to) attnto
                   , DECODE (t704.c704_ship_add1,NULL,'',t704.c704_ship_add1) addr1
                   , DECODE (t704.c704_ship_add2,NULL,'',t704.c704_ship_add2) addr2
                   , DECODE (t704.c704_ship_zip_code,NULL,'',t704.c704_ship_zip_code) shipzip
                   , DECODE(t704.c704_ship_state,NULL,'',get_code_name_alt(t704.c704_ship_state)) shipstate
                   , DECODE (t704.c704_ship_city,NULL,'',t704.c704_ship_city) shipcity
                   , DECODE(t704.c704_ship_country,NULL,'',get_code_name_alt(t704.c704_ship_country)) shipcountry
                   , t704.c704_phone ph_no
                   , GET_ACCOUNT_ATTRB_VALUE(v_third_party,91980) acctpayorno
                   , NVL(get_rule_value(t704.c704_ship_country,'CNTRYCD'),'US') CountryCode
              FROM t704_account t704
             WHERE t704.c704_account_id = v_shipto_id;
         ELSIF v_ship = 'Employee'
         THEN
         	OPEN p_shipaddrcur
       		FOR
            	SELECT  get_party_name (t106.c101_party_id) shipnm
               		 , v_override_attn attnto
                     , DECODE(t106.c106_add1,NULL,'',t106.c106_add1) addr1
                     , DECODE(t106.c106_add2,NULL,'',t106.c106_add2) addr2
                     , DECODE(t106.c106_zip_code,NULL,'',t106.c106_zip_code) shipzip
                     , DECODE(t106.c901_state,NULL,'',get_code_name_alt (t106.c901_state)) shipstate
                     , DECODE(t106.c106_city,NULL, '',t106.c106_city) shipcity
                     , DECODE(t106.c901_country,NULL,'',get_code_name_alt(t106.c901_country)) shipcountry
                     , gm_pkg_cm_contact.get_contact_value (t106.c101_party_id, '90450') ph_no
                     , GET_ACCOUNT_ATTRB_VALUE(v_third_party,91980) acctpayorno
                     , NVL(get_rule_value(t106.c901_country,'CNTRYCD'),'US') CountryCode
                 FROM t106_address t106, t907_shipping_info t907
                WHERE t106.c106_address_id = t907.c106_address_id
                  AND t907.c907_ref_id = p_refid
                  AND t907.c901_source = p_source               
                  AND t907.c907_void_fl IS NULL
                  AND t907.c907_status_fl < 40
                  AND t907.c5040_plant_id= v_plant_id; 
         END IF;       			
	END gm_fch_ship_address;   
	
/************************************************************************
* Description : Fetch all available Ship Modes based on the Ship Carrier
* Author	  : Hreddi
*************************************************************************/

 PROCEDURE gm_fch_ship_car_modes(  
 	p_Ship_Carrier 	IN  	t907_shipping_info.c901_delivery_carrier%TYPE
  , p_acctID		IN      t704_account.C704_ACCOUNT_ID%TYPE
  ,	p_out_cursor 	OUT   	TYPES.cursor_type  
  , p_default_mode 	OUT 	VARCHAR
  ,	p_company_id    IN 		t1900_company.c1900_company_id%TYPE DEFAULT NULL
 )
 AS
 	v_ship_modes varchar2(1000);
 	v_party_id	 t9080_ship_party_param.c101_party_id%TYPE;
 	v_company_id    t1900_company.c1900_company_id%TYPE;
 BEGIN
	 
	 --Getting company id from context.  
    SELECT NVL(p_company_id,get_compid_frm_cntx())
	  INTO v_company_id 
	  FROM dual;
	  
	 BEGIN
		  SELECT (RTRIM(XMLAGG(XMLELEMENT(E,c901_code_id || ',')).EXTRACT('//text()').GETCLOBVAL(),',') )
		    INTO v_ship_modes 
	        FROM ( SELECT c901_code_id FROM t901a_lookup_access WHERE c901_access_code_id = p_Ship_Carrier
	        	AND c1900_company_id = NVL(v_company_id,c1900_company_id));
     EXCEPTION
	    WHEN NO_DATA_FOUND THEN
	       v_ship_modes := '';
	 END;    
	    my_context.set_my_inlist_ctx (v_ship_modes);	
	     
  		OPEN p_out_cursor FOR  		
	        SELECT t901.c901_code_id codeid 
	             , t901.c901_code_nm codenm
	  	      FROM t901_code_lookup t901, 
	  	                 (SELECT rownum sno, token FROM v_in_list WHERE  token IS NOT NULL) codetoken
	  	     WHERE c901_code_id IN (token) 
	  	       AND c901_void_fl IS NULL
	      ORDER BY codetoken.sno;
	      
     BEGIN
    			 v_party_id := gm_pkg_op_ship_charge.get_party_id_for_carrier( p_acctID);
    			 
				SELECT C901_SHIP_MODE
			    	  INTO p_default_mode
			    	  FROM T9080_SHIP_PARTY_PARAM 
			    	WHERE C101_PARTY_ID = v_party_id
			    		AND C9080_VOID_FL IS NULL
			    		AND C901_SHIP_CARRIER = p_Ship_Carrier;
				EXCEPTION
         		WHEN NO_DATA_FOUND THEN
         			BEGIN
					  SELECT c906_rule_value INTO p_default_mode 
			              FROM t906_rules  
			             WHERE c906_rule_id = TO_CHAR(p_Ship_Carrier) 
			             AND c906_rule_grp_id = 'SHIPCARDEFUALTMODE'
			             AND c1900_company_id = v_company_id
			             AND c906_void_fl IS NULL;
					EXCEPTION
		         		WHEN NO_DATA_FOUND THEN
		            	p_default_mode := '';		            
		       END;
       END;
	     
 END gm_fch_ship_car_modes;		
 
/************************************************************************
* Description : get shipping account address
* Author	  : Xun
 ***********************************************************************/
	FUNCTION get_shipacct_addr (
		p_address_id	    IN		t907_shipping_info.c106_address_id%TYPE,
		p_party_id			IN      t907_shipping_info.c907_ship_to_id%TYPE	 
	)
	RETURN VARCHAR2
	IS 
		v_addressid	   t907_shipping_info.c106_address_id%TYPE;
		v_shipadd      VARCHAR2(1000);
	BEGIN
		
		IF p_address_id  IS NULL  THEN	
		BEGIN
			SELECT c106_address_id
			  INTO v_addressid
			  FROM t101_party t101, t106_address t106
             WHERE t101.c101_party_id     = t106.c101_party_id            
			   AND t101.c101_party_id = p_party_id
               AND t106.c106_primary_fl   = 'Y'
               AND t106.c106_void_fl IS NULL 
			   AND ROWNUM = 1;
			   
        EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               v_addressid := NULL;
        	 END;
        ELSE 
        	v_addressid := p_address_id;        	
        END IF; 
        
        IF v_addressid  IS NULL  THEN	        
        SELECT get_party_name (c101_party_id) 
           INTO v_shipadd
           FROM t101_party
          WHERE c101_party_id = p_party_id;
        ELSE
			SELECT get_party_name (t101.c101_party_id)
                   || '<BR>&'||'nbsp;'
                   || SUBSTR (get_code_name (c901_address_type)
                           || '-'
                           || DECODE(c106_add1,NULL,'',c106_add1||'<BR>&'||'nbsp;')                        
                           || DECODE(c106_add2,NULL,'',c106_add2||'<BR>&'||'nbsp;')                           
                           || c106_city
                           || '<BR>&'||'nbsp;'
                           || get_code_name_alt(c901_state)
                           || ' '
                           || c106_zip_code,                                           
                           0,
                           220
                          )
              INTO v_shipadd
              FROM t101_party t101, t106_address t106
             WHERE t101.c101_party_id 	= t106.c101_party_id
               AND t106.c106_address_id = TO_NUMBER (v_addressid);
        END IF;
      RETURN v_shipadd;         
	END get_shipacct_addr;
	
	
  /******************************************************************
   * Description : Procedure to get the shipping info for mailing
   ****************************************************************/
   PROCEDURE gm_fch_shipped_order_today (
      p_mailshipdtl   OUT   TYPES.cursor_type,
      p_ruledtl       OUT   TYPES.cursor_type
   )
   AS
      v_company_id    t1900_company.c1900_company_id%TYPE;
   BEGIN
   	  --Getting company id from context.   
    SELECT get_compid_frm_cntx()
	  INTO v_company_id 
	  FROM dual;
      OPEN p_mailshipdtl
       FOR
          SELECT DISTINCT *
                     FROM (SELECT   T907.C901_SHIP_TO SHIPTOTYPE,t907.c907_ship_to_id shiptoid,
                                    t907.c907_ref_id refidactual,                                 
                                            t907.c907_ref_id
                                            refid,
                                    DECODE
                                       (t907.c901_source,
                                        50180, DECODE (t501.c901_order_type,
                                                       2525, 'Back Order',
                                                       'Orders'
                                                      ),
                                        get_code_name (t907.c901_source)
                                       ) SOURCE,
                                    t907.c901_source sourceid,
                                    get_cs_ship_name
                                               (t907.c901_ship_to,
                                                t907.c907_ship_to_id
                                               ) shiptonm,
                                    NVL
                                      (gm_pkg_cm_contact.get_ship_contact_value
                                                         (NVL(t501.c704_account_id,t504.c704_account_id)),
                                        get_rule_value_by_company ('GMCSTOMAILID',
                                                        'SHIPDTL', v_company_id
                                                       )
                                       ) shipemail,
                                    get_rule_value
                                       (NVL(t501.c704_account_id,t504.c704_account_id),
                                        'CC-LIST-SHIPMENTS'
                                       ) cclist,
                                    get_code_name
                                               (t907.c901_delivery_mode)
                                                                        smode,
                                    get_code_name
                                            (t907.c901_delivery_carrier)
                                                                        scarr,
                                    t907.c907_tracking_number track,
                                    DECODE (t907.c901_ship_to, 4121, Gm_pkg_cm_shipping_info.get_salesrepaddress (
                                                                                                                                        t907.c106_address_id, t907.c907_ship_to_id), 4123, Gm_pkg_cm_shipping_info.get_empaddress (
                                                                                                                                        t907.c106_address_id, t907.c907_ship_to_id), gm_pkg_cm_shipping_info.get_address (t907.c901_ship_to,
                                                                                                                                        t907.c907_ship_to_id)) address ,t907.c901_delivery_carrier dcrr
                                                                                                                                     
                               FROM t907_shipping_info t907,
                                    t906_rules t906,
                                    t501_order t501,
                                    t501a_order_attribute t501a,
                                    t504_consignment t504
                              WHERE t907.c907_void_fl IS NULL
                                AND t907.c907_active_fl IS NULL
                                AND t907.c907_status_fl = 40
                                AND t907.c907_email_update_fl IS NULL
                                AND t906.c906_rule_grp_id = 'SHIPS'
                                AND TO_NUMBER (t906.c906_rule_id) =
                                       DECODE (t907.c901_source,
                                               50180, DECODE
                                                        (t501.c901_order_type,
                                                         2525, t501.c901_order_type,
                                                         50180
                                                        ),
                                               t907.c901_source
                                              )             
                           
                                AND t907.c907_tracking_number IS NOT NULL
                                AND t907.c907_ship_to_id <> 0
                                AND t907.c901_delivery_mode  NOT IN (select c906_rule_value 
                                										from t906_rules 
                                										where c906_rule_id = 'DELMODEEXCL' 
                                										and C906_RULE_GRP_ID = 'DELMODE')
                                                         -- for hand delivered
                                AND t907.c907_ref_id = t501.c501_order_id(+)
                                AND t501.c501_order_id = t501a.c501_order_id(+)
                                AND NVL (t501a.c901_attribute_type, -999) !=     11241   
                                AND t906.c906_void_fl IS NULL
                                AND t907.c907_ref_id = t504.c504_consignment_id (+)
                                AND t504.c504_status_fl(+) = 4
                                AND t504.c504_void_fl(+) is null
                                AND t907.c1900_company_id = v_company_id
                                AND t501.c501_void_fl(+) is null
                          )ORDER BY shiptoid ,track;
      
      OPEN p_ruledtl
       FOR
          SELECT c906_rule_id ruleid, c906_rule_desc description,
                 c906_rule_value rulevalue
            FROM t906_rules
           WHERE c906_rule_grp_id = 'SHIPDTL'
           AND c1900_company_id = v_company_id;
           
   END gm_fch_shipped_order_today;  
   
   
   
   /******************************************************************
	 * Description : function to get shipto loaner address for italy
	 * PMT-42950   : Include Customer # in shipment and Invoice paperwork for Italy
	 *  Author     : rajan
	 ****************************************************************/
   FUNCTION get_loaner_ship_add (
      p_refid        IN   t501_order.c501_order_id%TYPE,
      p_source       IN   t907_shipping_info.c901_source%TYPE,
      p_prod_req_id  IN   t907_shipping_info.c525_product_request_id%TYPE,
      p_swap_attn_to IN VARCHAR2
   )
      RETURN VARCHAR2
   IS
      v_ship           VARCHAR2 (50);
      v_shipto_id      t907_shipping_info.c907_ship_to_id%TYPE;
      v_ship_add       VARCHAR2 (1000);
      v_address_id     t907_shipping_info.c106_address_id%TYPE;
      v_comp_id        t1900_company.c1900_company_id%TYPE;
	  v_delimiter      VARCHAR2(10);
      v_zip_code_fl    VARCHAR2(10);
      v_plant_comp_id  t1900_company.c1900_company_id%TYPE;
      v_comp_name      t906_rules.c906_rule_value%TYPE;
   BEGIN
	   --
	SELECT get_compid_frm_cntx()
	  INTO v_comp_id 
	  FROM dual;
	   --
	   -- This rule will get the Delimiter that needs to be shown in the Zip Code Section. For Country Like Germany, they do not need a delimiter.
		SELECT NVL(get_rule_value_by_company('DELIMITER','ZIP_CODE_DELIM', v_comp_id),',')
		, NVL(get_rule_value_by_company('SWAP_ZIP_CODE','SWAP_ZIP_CODE', v_comp_id),'N')
		INTO v_delimiter, v_zip_code_fl FROM DUAL;
		--
      BEGIN
         SELECT t907.c901_ship_to, t907.c907_ship_to_id,
                c106_address_id
           INTO v_ship, v_shipto_id,
                v_address_id
           FROM t907_shipping_info t907
          WHERE t907.c907_ref_id = p_refid
            AND t907.c901_source = p_source
            AND t907.c525_product_request_id = p_prod_req_id
            AND c907_void_fl IS NULL;
		 /*
		  * The Below Shipping Address code is changed for PMT-6402.
		  * In US DB swap Attention to will be null, In BBA it will be Y.
		  * When Invoice is generated from BBA Attention to will be displayed at last, when generated from US it will work as earlier, it will be displayed first.
		  */
       IF v_ship = 4120   -- 4120 - Distributor
         THEN
            SELECT '&'||'nbsp;'|| c701_ship_name
                   || '<BR>&'||'nbsp;'
                   || DECODE(p_swap_attn_to,'Y','',DECODE(t907.c907_override_attn_to,'',NULL,'Attn:&'||'nbsp;'|| t907.c907_override_attn_to || '<BR>&'||'nbsp;')) -- Will work foe US.
                   || DECODE(t701.c701_ship_add1,NULL,'',t701.c701_ship_add1 ||'<BR>&'||'nbsp;')
                   || DECODE(t701.c701_ship_add2,NULL,'',t701.c701_ship_add2 ||'<BR>&'||'nbsp;')
                   || DECODE (v_zip_code_fl ,
                              'Y', DECODE(t701.c701_ship_zip_code,NULL,'',t701.c701_ship_zip_code || v_delimiter ||'&'||'nbsp;'),
                              DECODE(t701.c701_ship_city,NULL,'',t701.c701_ship_city || ',&'||'nbsp;')
                             )
                   
                   || DECODE(t701.c701_ship_state ,NULL,'','1057','',get_code_name_alt (t701.c701_ship_state)||'&'||'nbsp;') -- To handle when state is NA , it should not display in invoice. 
                   || DECODE (v_zip_code_fl,
                              'Y', t701.c701_ship_city ||'<BR>&'||'nbsp;',
                              t701.c701_ship_zip_code
                             ||'<BR>&'||'nbsp;')
                   || DECODE (t701.C701_SHIP_COUNTRY, NULL, '', get_code_name (t701.C701_SHIP_COUNTRY) ||'&'||'nbsp;')
                   || DECODE(p_swap_attn_to,'Y',DECODE(t907.c907_override_attn_to,'',NULL,'<BR>'||'Attn:&'||'nbsp;'|| t907.c907_override_attn_to || '<BR>&'||'nbsp;'),'') -- will work for BBA.
              INTO v_ship_add
              FROM t701_distributor t701, t907_shipping_info t907
             WHERE t701.c701_distributor_id = v_shipto_id
             AND t907.c907_ref_id = p_refid
             AND t907.c901_source = p_source               --50180 orders
             AND t907.c525_product_request_id = p_prod_req_id
             AND t907.c907_void_fl IS NULL;
         --AND c701_void_fl IS NULL;
         ELSIF (v_ship = 4121 OR v_ship = 4000642 OR v_ship = 107801)  -- Shipping Account : 4000642, Dealer: 107801
         THEN
            IF v_address_id IS NULL
            THEN
               SELECT DECODE(v_ship, 'Sales Rep', get_rep_name (v_shipto_id), get_party_name(v_shipto_id))
                 INTO v_ship_add
                 FROM DUAL;
            ELSE
               SELECT '&'||'nbsp;'|| get_party_name (t106.c101_party_id)
                      || '<BR>&'||'nbsp;'
                      || DECODE(p_swap_attn_to,'Y','',DECODE(t907.c907_override_attn_to,'',NULL,'Attn:&'||'nbsp;'|| t907.c907_override_attn_to || '<BR>&'||'nbsp;'))--will work for US.
                      || DECODE(t106.c106_add1,NULL,'',t106.c106_add1 ||'<BR>&'||'nbsp;')
                      || DECODE(t106.c106_add2,NULL,'',t106.c106_add2 ||'<BR>&'||'nbsp;')
                      || DECODE (v_zip_code_fl ,
                                 'Y', t106.c106_zip_code ||'&'||'nbsp;',
                                 t106.c106_city || '&'||'nbsp;'
                                )
                      || DECODE (t106.c901_state,NULL,'','1057','',get_code_name_alt (t106.c901_state)||'&'||'nbsp;') --To handle when state is NA , it should not display in invoice. 
                      || DECODE (v_zip_code_fl ,
                                 'Y', t106.c106_city ||'<BR>&'||'nbsp;',
                                 t106.c106_zip_code
                                ||'<BR>&'||'nbsp;')
                       || DECODE (t106.C901_COUNTRY, NULL, '', get_code_name (t106.C901_COUNTRY) ||'&'||'nbsp;') 
                       || DECODE(p_swap_attn_to,'Y',DECODE(t907.c907_override_attn_to,'',NULL,'<BR>'||'Attn:&'||'nbsp;'|| t907.c907_override_attn_to || '<BR>&'||'nbsp;'),'') -- Will work for BBA.
                 INTO v_ship_add
                 FROM t106_address t106, t907_shipping_info t907
                WHERE t106.c106_address_id = t907.c106_address_id
                  AND t907.c907_ref_id = p_refid
                  AND t907.c901_source = p_source               --50180 orders
                  AND t907.c525_product_request_id = p_prod_req_id
                  AND t907.c907_void_fl IS NULL;
            --AND t106.c106_void_fl IS NULL;
            END IF;
         ELSIF v_ship = 4122     --Hospital : 4122
         THEN
           SELECT '&'||'nbsp;'|| t704.c704_ship_name
                   || '<BR>&'||'nbsp;'
                   || DECODE(p_swap_attn_to,'Y','',DECODE(t907.c907_override_attn_to,'',DECODE(t704.c704_ship_attn_to,NULL,NULL,'Attn:&'||'nbsp;'|| t704.c704_ship_attn_to
                   || '<BR>&'||'nbsp;'),'Attn:&'||'nbsp;'|| t907.c907_override_attn_to || '<BR>&'||'nbsp;')) -- will work for US.
                   || DECODE (t704.c704_ship_add1,NULL,'',t704.c704_ship_add1||'<BR>&'||'nbsp;')
                   || DECODE (t704.c704_ship_add2,NULL,'',t704.c704_ship_add2||'<BR>&'||'nbsp;')
                   || DECODE (v_zip_code_fl ,
                              'Y', t704.c704_ship_zip_code|| v_delimiter ||'&'||'nbsp;',
                              t704.c704_ship_city|| ',&'||'nbsp;'
                             )
                   || DECODE(t704.c704_ship_state,NULL,'','1057','',get_code_name (t704.c704_ship_state)||'&'||'nbsp;') --To handle when state is NA , it should not display in invoice. 
                   || DECODE (v_zip_code_fl ,
                              'Y', t704.c704_ship_city ||'<BR>&'||'nbsp;',
                              t704.c704_ship_zip_code
                             ||'<BR>&'||'nbsp;')
                  || DECODE (t704.C704_SHIP_COUNTRY, NULL, '', get_code_name (t704.C704_SHIP_COUNTRY) ||'&'||'nbsp;')
                  || DECODE(p_swap_attn_to,'Y',DECODE(t907.c907_override_attn_to,'',NULL,'<BR>'||'Attn:&'||'nbsp;'|| t907.c907_override_attn_to || '<BR>&'||'nbsp;'),'')-- will work for BBA.
              INTO v_ship_add
              FROM t704_account t704, t907_shipping_info t907
             WHERE t704.c704_account_id = v_shipto_id
             AND t907.c907_ref_id = p_refid
             AND t907.c901_source = p_source               --50180 orders
             AND t907.c525_product_request_id = p_prod_req_id
             AND t907.c907_void_fl IS NULL;
         --AND c704_void_fl IS NULL;
         ELSIF v_ship = 4123     -- Employee : 4123
         THEN
            SELECT	'&'||'nbsp;'|| get_user_name (c101_user_id)
                   	|| '<BR>&'||'nbsp;'
            		|| DECODE(p_swap_attn_to,'Y','',DECODE(t907.c907_override_attn_to,'',NULL,'Attn:&'||'nbsp;'|| t907.c907_override_attn_to || '<BR>&'||'nbsp;'))
                   	|| decode(c101_address,null,'','<BR>&'||'nbsp;'||c101_address)
                   	|| DECODE(p_swap_attn_to,'Y',DECODE(t907.c907_override_attn_to,'',NULL,'<BR>'||'Attn:&'||'nbsp;'|| t907.c907_override_attn_to || '<BR>&'||'nbsp;'),'')
              INTO v_ship_add
              FROM t101_user, t907_shipping_info t907
             WHERE c101_user_id = v_shipto_id
             AND t907.c907_ref_id = p_refid
             AND t907.c901_source = p_source
             AND t907.c525_product_request_id = p_prod_req_id
             AND t907.c907_void_fl IS NULL;
          ELSIF v_ship = 26240419	---Plant : 26240419
      	  THEN  
      	        --getting company id for the plant id
		      SELECT C1900_PARENT_COMPANY_ID
		      	INTO v_plant_comp_id
		       FROM T5040_PLANT_MASTER 
		       WHERE C5040_PLANT_ID = v_shipto_id AND C5040_VOID_FL IS NULL AND C5040_ACTIVE_FL= 'Y';
		--getting     
		      SELECT GET_RULE_VALUE_BY_COMPANY('COMP_NAME','COMP_DETAILS',v_plant_comp_id)
		        INTO v_comp_name
		        FROM DUAL;
		       
  
       			 SELECT	'&'||'nbsp;'
       			||DECODE (v_comp_name,'','',v_comp_name||'<BR>&'||'nbsp;')
         		|| C5040_PLANT_NAME
                || '<BR>&'||'nbsp;'
                || DECODE (C5040_ADDRESS_1,NULL,'',C5040_ADDRESS_1||'<BR>&'||'nbsp;')
                || DECODE (C5040_ADDRESS_2,NULL,'',C5040_ADDRESS_2||'<BR>&'||'nbsp;')
                 || DECODE (NVL (get_rule_value ('SWAP_ZIP_CODE',
                                                'SWAP_ZIP_CODE'
                                               ),
                                'N'
                               ),
                           'Y', C5040_ZIPCODE||'<BR>&'||'nbsp;',
                           C5040_CITY||'<BR>&'||'nbsp;'
                          )
                
                || DECODE (C901_STATE_ID,NULL,'',get_code_name_alt (C901_STATE_ID)||'<BR>&'||'nbsp;') 
             
                || DECODE (NVL (get_rule_value ('SWAP_ZIP_CODE',
                                                'SWAP_ZIP_CODE'
                                               ),
                                'N'
                               ),
                           'Y', C5040_CITY||'<BR>&'||'nbsp;',
                           C5040_ZIPCODE||'<BR>&'||'nbsp;'
                          )
                

           INTO v_ship_add
           FROM T5040_PLANT_MASTER 
          WHERE C5040_PLANT_ID = v_shipto_id AND C5040_VOID_FL IS NULL AND C5040_ACTIVE_FL= 'Y';
         END IF;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            RETURN '';
      END;

      RETURN v_ship_add;
   END get_loaner_ship_add;
END gm_pkg_cm_shipping_info;
/