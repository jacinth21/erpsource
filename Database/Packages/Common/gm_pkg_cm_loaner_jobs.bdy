/* Formatted on 05/17/2010 16:25 (Formatter Plus v4.8.0) */
--@"c:\database\packages\common\gm_pkg_cm_loaner_jobs.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_cm_loaner_jobs
IS
	/******************************************************************
	* Description : Procedure to fetch the mail
	* Author		: Satyajit
	****************************************************************/
	PROCEDURE gm_fch_mail_details (
		p_grp_id		IN		 VARCHAR2
	  , p_maildetails	OUT 	 TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_maildetails
		 FOR
			 SELECT c906_rule_id ruleid, c906_rule_desc description, c906_rule_value rulevalue
			   FROM t906_rules
			  WHERE c906_rule_grp_id = p_grp_id;
	END gm_fch_mail_details;

	/******************************************************************
	* Description : Procedure to fetch the rep ids who have requested loaner sets today
	* Author		: Satyajit
	****************************************************************/
	PROCEDURE gm_fch_loaner_received_rep_ids (
		p_repids   OUT	 TYPES.cursor_type
	)
	AS
	v_company_id    t1900_company.c1900_company_id%TYPE;
	BEGIN
	 --Getting company id from context.   
    SELECT get_compid_frm_cntx()
	  INTO v_company_id 
	  FROM dual;
	  --
		OPEN p_repids
		 FOR
			 SELECT   t525.c703_sales_rep_id rep_id, get_rep_name (t525.c703_sales_rep_id) rep_name
					, NVL (get_cs_ship_email (4121, t525.c703_sales_rep_id)
						 , get_rule_value_by_company ('GMCSMAILID', 'SHIPDTL', v_company_id)
						  ) email
				 FROM t525_product_request t525, t526_product_request_detail t526
				WHERE t525.c525_product_request_id = t526.c525_product_request_id
				  AND t525.c525_requested_date = TRUNC (CURRENT_DATE)
				  AND t525.c525_void_fl IS NULL
				  AND t526.c526_void_fl IS NULL
				  AND t525.c703_sales_rep_id IS NOT NULL
			 GROUP BY t525.c703_sales_rep_id;
	END gm_fch_loaner_received_rep_ids;

	/******************************************************************
	* Description : Procedure to the request details
	* Author		: Satyajit
	****************************************************************/
	PROCEDURE gm_fch_received_requests (
		p_repid 	 IN 	  t525_product_request.c703_sales_rep_id%TYPE
	  , p_requests	 OUT	  TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_requests
		 FOR
			 SELECT   'Product Request - ' || t525.c525_product_request_id reqid, t526.c207_set_id set_id
					, get_set_name (t526.c207_set_id) set_name
					, DECODE (NVL (t526.c526_status_fl, 10)
							, 10, 'Unavailable'
							, 20, 'Available'
							, 30, 'Shipped'
							, 40, 'Cancelled'
							 ) status
					, TO_CHAR (t526.c526_required_date, 'MM/DD/YYYY') psdate
				 FROM t525_product_request t525, t526_product_request_detail t526
				WHERE t525.c525_product_request_id = t526.c525_product_request_id
				  AND TRUNC (t525.c525_requested_date) = TRUNC (CURRENT_DATE)
				  AND t525.c525_void_fl IS NULL
				  AND t526.c526_void_fl IS NULL
				  AND t525.c703_sales_rep_id = p_repid
			 ORDER BY status DESC;
	END gm_fch_received_requests;

	/******************************************************************
	* Description : Procedure to fetch the rep ids who have requested unavailable loaners
	* Author		: Satyajit
	****************************************************************/
	PROCEDURE gm_fch_unavailable_loaner_reps (
		p_repids   OUT	 TYPES.cursor_type
	)
	AS
	v_company_id    t1900_company.c1900_company_id%TYPE;
	BEGIN
	--Getting company id from context.   
    SELECT get_compid_frm_cntx()
	  INTO v_company_id 
	  FROM dual;
		OPEN p_repids
		 FOR
			 SELECT   t525.c703_sales_rep_id rep_id, get_rep_name (t525.c703_sales_rep_id) rep_name
					, NVL (get_cs_ship_email (4121, t525.c703_sales_rep_id)
						 , get_rule_value_by_company ('GMCSMAILID', 'SHIPDTL', v_company_id)
						  ) email
				 FROM t525_product_request t525, t526_product_request_detail t526
				WHERE t525.c525_product_request_id = t526.c525_product_request_id
				  AND t526.c526_required_date = TRUNC (CURRENT_DATE) + 1
				  AND t525.c525_void_fl IS NULL
				  AND t526.c526_void_fl IS NULL
				  AND t526.c526_status_fl IN (10)	--(open )
				  AND t525.c703_sales_rep_id IS NOT NULL
			 GROUP BY t525.c703_sales_rep_id;
	END gm_fch_unavailable_loaner_reps;

	/******************************************************************
	* Description : Procedure to the unavailable request details
	* Author		: Satyajit
	****************************************************************/
	PROCEDURE gm_fch_unavailable_requests (
		p_repid 	 IN 	  t525_product_request.c703_sales_rep_id%TYPE
	  , p_requests	 OUT	  TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_requests
		 FOR
			 SELECT 'Product Request - ' || t525.c525_product_request_id reqid, t526.c207_set_id set_id
				  , get_set_name (t526.c207_set_id) set_name
				  , DECODE (NVL (t526.c526_status_fl, 10), 10, 'Unavailable', 'Available') status
				  , TO_CHAR (t526.c526_required_date, 'MM/DD/YYYY') psdate
				  , TO_CHAR (t525.c525_requested_date, 'MM/DD/YYYY') rqdate
			   FROM t525_product_request t525, t526_product_request_detail t526
			  WHERE t525.c525_product_request_id = t526.c525_product_request_id
				AND t526.c526_required_date = TRUNC (CURRENT_DATE) + 1
				AND t525.c525_void_fl IS NULL
				AND t526.c526_void_fl IS NULL
				AND t526.c526_status_fl IN (10)   --(open)
				AND t525.c703_sales_rep_id = p_repid;
	END gm_fch_unavailable_requests;

	--
	/******************************************************************
	* Description : Procedure to
	* Author		: Satyajit
	****************************************************************/
	PROCEDURE gm_fch_cancel_loaner_rep_ids (
		p_reps	 OUT   TYPES.cursor_type
	)
	AS
	v_company_id    t1900_company.c1900_company_id%TYPE;
	BEGIN
	--Getting company id from context.   
    SELECT get_compid_frm_cntx()
	  INTO v_company_id 
	  FROM dual;
	  --
		OPEN p_reps
		 FOR
			 SELECT   t525.c703_sales_rep_id rep_id, get_rep_name (t525.c703_sales_rep_id) rep_name
					, NVL (get_cs_ship_email (4121, t525.c703_sales_rep_id)
						 , get_rule_value_by_company ('GMCSMAILID', 'SHIPDTL', v_company_id)
						  ) email
					, t525.c703_ass_rep_id assocrepid
				    , get_rep_name(t525.c703_ass_rep_id) assocrepnm
				    , DECODE(t525.c703_ass_rep_id, NULL, NULL, NVL(get_cs_ship_email (4121, t525.c703_ass_rep_id), get_rule_value_by_company ('GMCSTOMAILID', 'SHIPDTL', v_company_id))) assocrepemail	  
				 FROM t525_product_request t525, t526_product_request_detail t526
				WHERE t525.c525_product_request_id = t526.c525_product_request_id
				  AND t526.c526_required_date = TRUNC (CURRENT_DATE)
				  AND t525.c525_void_fl IS NULL
				  AND t526.c526_void_fl IS NULL
				  AND t526.c526_status_fl IN (10)	--(open)
				  AND t525.c703_sales_rep_id IS NOT NULL
			 GROUP BY t525.c703_sales_rep_id,t525.c703_ass_rep_id
			 ORDER BY t525.c703_sales_rep_id, t525.c703_ass_rep_id;
	END gm_fch_cancel_loaner_rep_ids;

	/******************************************************************
	* Description : Procedure to
	* Author		: Satyajit
	****************************************************************/
	PROCEDURE gm_fch_cancel_requests (
		p_repid 	 IN 	  t525_product_request.c703_sales_rep_id%TYPE
	  , p_assrepid 	 IN		  t525_product_request.c703_ass_rep_id%TYPE	
	  , p_requests	 OUT	  TYPES.cursor_type
	)
	AS
		CURSOR request_to_cancel
		IS
			SELECT t526.c526_product_request_detail_id reqid
			  FROM t525_product_request t525, t526_product_request_detail t526
			 WHERE t525.c525_product_request_id = t526.c525_product_request_id
			   AND t526.c526_required_date = TRUNC (SYSDATE)
			   AND t525.c525_void_fl IS NULL
			   AND t526.c526_void_fl IS NULL
			   AND t526.c526_status_fl IN (10)	 --(open)
			   AND t525.c703_sales_rep_id = p_repid
			   AND NVL(t525.c703_ass_rep_id,-999) = NVL(p_assrepid,NVL(t525.c703_ass_rep_id,-999));
	BEGIN
		OPEN p_requests
		 FOR
			 SELECT 'Product Request - ' || t525.c525_product_request_id reqid, t526.c207_set_id set_id
				  , get_set_name (t526.c207_set_id) set_name
				  , DECODE (NVL (t526.c526_status_fl, 10), 10, 'UnAvailable', 'Available') status
				  , TO_CHAR (t526.c526_required_date, 'MM/DD/YYYY') psdate
				  , TO_CHAR (t525.c525_requested_date, 'MM/DD/YYYY') rqdate
			   FROM t525_product_request t525, t526_product_request_detail t526
			  WHERE t525.c525_product_request_id = t526.c525_product_request_id
				AND t526.c526_required_date = TRUNC (SYSDATE)
				AND t525.c525_void_fl IS NULL
				AND t526.c526_void_fl IS NULL
				AND t526.c526_status_fl IN (10)   --(open)
				AND t525.c703_sales_rep_id = p_repid
				AND NVL(t525.c703_ass_rep_id,-999) = NVL(p_assrepid,NVL(t525.c703_ass_rep_id,-999));

		FOR requestindex IN request_to_cancel
		LOOP
			gm_sav_cancel_requests (requestindex.reqid);
		END LOOP;
	END gm_fch_cancel_requests;

	/******************************************************************
	* Description : Procedure to
	* Author		: Satyajit
	****************************************************************/
	PROCEDURE gm_sav_cancel_requests (
		p_request_detail_id   IN   t526_product_request_detail.c526_product_request_detail_id%TYPE
	)
	AS
	BEGIN
		gm_pkg_op_loaner_allocation.gm_loaner_allocation_update(p_request_detail_id, 'CLEAR', 30301);-- reset allocated fl and planned ship date in t504a
		
		-- 40 cancel
		gm_pkg_op_loaner_allocation.gm_upd_request_status(p_request_detail_id, 40, '30301');

		gm_pkg_cm_status_log.gm_sav_status_details (p_request_detail_id, '', 30301, '', SYSDATE, 91103, 91126);   -- 91103 - Loaner, Cancelled
		gm_pkg_op_loaner.gm_sav_update_req_status (p_request_detail_id, 30301);
	END gm_sav_cancel_requests;   --

	/******************************************************************
	* Description : Procedure to move allocated request to pending pick
	* Author		: Satyajit
	****************************************************************/
	PROCEDURE gm_move_allocated_requests
	AS
		CURSOR allocate_cur
		IS
			SELECT t526.c526_product_request_detail_id prid, t526.c207_set_id setid, t526.c901_request_type rtype, t504a.c504_consignment_id cnid
			  FROM t525_product_request t525, t526_product_request_detail t526, t504a_loaner_transaction t504a
			 WHERE t525.c525_product_request_id = t526.c525_product_request_id
			   AND t526.c526_product_request_detail_id = t504a.c526_product_request_detail_id
			   AND t526.c526_required_date = TRUNC (CURRENT_DATE) + 1
			   AND t525.c525_void_fl IS NULL
			   AND t526.c526_void_fl IS NULL
			   AND t504a.c504a_void_fl IS NULL
			   AND t526.c526_status_fl IN (20);   --Allocated '50831'
	BEGIN
		FOR allocatereq IN allocate_cur
		LOOP
			gm_pkg_op_loaner.gm_sav_status_update (allocatereq.prid, '30301');
			IF allocatereq.rtype = 4127 --Product Loaner
			THEN
				gm_pkg_allocation.gm_ins_invpick_assign_detail (93602, allocatereq.cnid, '30301');
			END IF;
        	--93602 Product Loaner , to insert record in the t5050_INVPICK_ASSIGN_DETAIL
		END LOOP;
	END gm_move_allocated_requests;

   /******************************************************************
	* Description : Procedure to fetch the missing parts
	* Author		: Brinal G
	****************************************************************/
	PROCEDURE gm_fch_missing_parts (
		p_request_id			IN		 t525_product_request.c525_product_request_id%TYPE
	  , p_request_details_ids	IN		 VARCHAR2
	  , rep_info				OUT 	 TYPES.cursor_type
	  , missing_parts_info		OUT 	 TYPES.cursor_type
	)
	AS
		v_request_details_ids VARCHAR2 (4000);
		v_add_month    NUMBER;
		v_day		   VARCHAR2 (10);
		v_final_dd	   VARCHAR2 (2);
		v_date_format  VARCHAR2 (20);
	BEGIN
		v_date_format := GET_RULE_VALUE('DATEFMT', 'DATEFORMAT');
		-- when called from the reconcilation->transaction screen, the prod_req_detail_id wud be null. so fetching here in a single line as a comma seperated value
		IF p_request_details_ids IS NULL
		THEN
			WITH req_info AS
				 (SELECT c526_product_request_detail_id req_detail_id
					INTO v_request_details_ids
					FROM t526_product_request_detail
				   WHERE c525_product_request_id = p_request_id AND c526_void_fl IS NULL)
			SELECT	   SUBSTR (MAX (SYS_CONNECT_BY_PATH (req_detail_id, ',')), 2) req_detail_id
				  FROM (SELECT req_detail_id, ROW_NUMBER () OVER (ORDER BY req_detail_id) AS curr
						  FROM req_info rinfo)
			CONNECT BY curr - 1 = PRIOR curr
			START WITH curr = 1;
		ELSE
			v_request_details_ids := p_request_details_ids;
		END IF;

		my_context.set_my_inlist_ctx (v_request_details_ids);

		OPEN rep_info
		 FOR
			 SELECT c703_sales_rep_id ID, get_rep_name (c703_sales_rep_id) repname, get_rep_login_name (c703_sales_rep_id) reploginname
				  ,    get_cs_ship_email (4121, c703_sales_rep_id)
					|| ','
					|| get_user_emailid (get_ad_rep_id (c703_sales_rep_id)) repemail,
					t525.c703_ass_rep_id assocrepid,
					get_rep_login_name (t525.c703_ass_rep_id) assreploginname,
					get_rep_name(t525.c703_ass_rep_id) assocrepnm,
    				DECODE(t525.c703_ass_rep_id, NULL, NULL, get_cs_ship_email (4121, t525.c703_ass_rep_id)) assocrepemail
			   FROM t525_product_request t525
			  WHERE t525.c525_product_request_id IN ( SELECT c525_product_request_id FROM t526_product_request_detail WHERE c526_product_request_detail_id IN (SELECT token
	                                                   FROM v_in_list)) AND t525.c525_void_fl IS NULL;

		-- this prc is called only twice. during first email and final email(21st)
		--if sysdate.day >= 21 then add  2 months else add 1 month
		SELECT TO_CHAR (SYSDATE, 'DD'), get_rule_value ('FINAL', 'EMAIL')
		  INTO v_day, v_final_dd
		  FROM DUAL;

		IF v_day >= v_final_dd
		THEN
			v_add_month := 2;
		ELSE
			v_add_month := 1;
		END IF;

		OPEN missing_parts_info
		 FOR	
			SELECT t413.c205_part_number_id
	         || ' '
	         || get_partnum_desc (t413.c205_part_number_id) partnumber,
	           SUM (DECODE (c413_status_fl, 'Q', c413_item_qty, 0))
	         - SUM (DECODE (c413_status_fl, 'Q', 0, c413_item_qty)) missqty,
	         get_part_price_list (t413.c205_part_number_id, '') price,
	           get_part_price_list (t413.c205_part_number_id, '')
	         * (  SUM (DECODE (c413_status_fl, 'Q', c413_item_qty, 0))
	            - SUM (DECODE (c413_status_fl, 'Q', 0, c413_item_qty))
	           ) eprice,
	         TO_CHAR (t504al.c504a_return_dt, v_date_format) retdt,
	         DECODE (t412.c412_expected_return_date,
	                 NULL, TO_CHAR (ADD_MONTHS (SYSDATE, v_add_month), 'MM')
	                  || '/01/'
	                  || TO_CHAR (ADD_MONTHS (SYSDATE, v_add_month), 'YYYY'),
	                 TO_CHAR (t412.c412_expected_return_date, v_date_format)
	                ) expretdt,
	            t526.c207_set_id
	         || '-'
	         || get_set_name (t526.c207_set_id)
	         || '-'
	         || gm_pkg_op_loaner.get_loaner_etch_id (t504al.c504_consignment_id)
	                                                                       setdet
			, t525.c525_product_request_id REQ_ID, t526.c526_product_request_detail_id REQ_DET_ID
	    FROM t412_inhouse_transactions t412,
	         t504a_loaner_transaction t504al,
	         (SELECT MISSING.*
	            FROM (SELECT   t412.c412_ref_id,
	                           SUM (t413.c413_item_qty) c413_item_qty,
	                           t413.c205_part_number_id, t413.c413_status_fl,
	                           t412.c412_inhouse_trans_id,
	                           t412.c504a_loaner_transaction_id
	                      FROM t525_product_request t525,
	                           t526_product_request_detail t526,
	                           t504a_loaner_transaction t504al,
	                           t412_inhouse_transactions t412,
	                           t413_inhouse_trans_items t413
	                   --  WHERE t525.c525_product_request_id = p_request_id
	                     WHERE t526.c526_product_request_detail_id IN (SELECT token
	                                                   FROM v_in_list)
	                       AND t525.c525_product_request_id =
	                                                  t526.c525_product_request_id
	                       AND t504al.c526_product_request_detail_id =
	                                           t526.c526_product_request_detail_id
	                       AND t504al.c504a_loaner_transaction_id =
	                                              t412.c504a_loaner_transaction_id
	                       AND t412.c412_inhouse_trans_id =
	                                                    t413.c412_inhouse_trans_id
	                       AND t412.c412_type = 50159
	                       AND t412.c412_void_fl IS NULL
	                       AND t504al.c504a_void_fl IS NULL
	                       AND t526.c526_void_fl IS NULL
	                       AND t525.c525_void_fl IS NULL
	                       AND t413.c413_void_fl IS NULL
	                  GROUP BY t412.c412_ref_id,
	                           t413.c205_part_number_id,
	                           t413.c413_status_fl,
	                           t412.c412_inhouse_trans_id,
	                           t412.c504a_loaner_transaction_id) MISSING,
	                 (SELECT   t412.c412_ref_id,
	                           SUM (t413.c413_item_qty) c413_item_qty,
	                           t413.c205_part_number_id, t413.c413_status_fl,
	                           t412.c412_inhouse_trans_id,
	                           t412.c504a_loaner_transaction_id
	                      FROM t525_product_request t525,
	                           t526_product_request_detail t526,
	                           t504a_loaner_transaction t504al,
	                           t412_inhouse_transactions t412,
	                           t413_inhouse_trans_items t413
	                 --    WHERE t525.c525_product_request_id = p_request_id
	                 	 WHERE t526.c526_product_request_detail_id IN (SELECT token
	                                                   FROM v_in_list)
	                       AND t525.c525_product_request_id =
	                                                  t526.c525_product_request_id
	                       AND t504al.c526_product_request_detail_id =
	                                           t526.c526_product_request_detail_id
	                       AND t504al.c504a_loaner_transaction_id =
	                                              t412.c504a_loaner_transaction_id
	                       AND t412.c412_inhouse_trans_id =
	                                                    t413.c412_inhouse_trans_id
	                       AND t412.c412_type = 50154
	                       AND t412.c412_void_fl IS NULL
	                       AND t504al.c504a_void_fl IS NULL
	                       AND t526.c526_void_fl IS NULL
	                       AND t525.c525_void_fl IS NULL
	                  GROUP BY t412.c412_ref_id,
	                           t413.c205_part_number_id,
	                           t413.c413_status_fl,
	                           t412.c412_inhouse_trans_id,
	                           t412.c504a_loaner_transaction_id) fgle
	           WHERE MISSING.c504a_loaner_transaction_id = fgle.c504a_loaner_transaction_id(+)
	             AND MISSING.c412_ref_id = fgle.c412_ref_id(+)
	             AND MISSING.c205_part_number_id = fgle.c205_part_number_id(+)
	             AND (MISSING.c413_item_qty - NVL (fgle.c413_item_qty, 0)) > 0) t413,
	         t526_product_request_detail t526,
	         t525_product_request t525
	   WHERE t412.c504a_loaner_transaction_id = t504al.c504a_loaner_transaction_id
	     AND t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
	     AND t412.c412_type = 50159
	     AND t525.c525_product_request_id = t526.c525_product_request_id
	     AND t525.c525_product_request_id = t526.c525_product_request_id
	     AND t526.c526_product_request_detail_id =
	                                         t504al.c526_product_request_detail_id
	   --  AND t525.c525_product_request_id = p_request_id
	     AND t526.c526_product_request_detail_id IN (SELECT token
	                                                   FROM v_in_list)
	     AND t412.c412_void_fl IS NULL
	     AND t504al.c504a_void_fl IS NULL
	     AND t526.c526_void_fl IS NULL
	     AND t525.c525_void_fl IS NULL
	GROUP BY t413.c205_part_number_id,
	         t504al.c504a_return_dt,
	         t412.c412_expected_return_date,
	         t526.c207_set_id,
	         t504al.c504_consignment_id,
	         t525.c525_product_request_id, 
	         t526.c526_product_request_detail_id 
	  HAVING   SUM (DECODE (c413_status_fl, 'Q', c413_item_qty, 0))
	         - SUM (DECODE (c413_status_fl, 'Q', 0, c413_item_qty)) > 0
	 ORDER BY REQ_DET_ID;	
		
	END gm_fch_missing_parts;

		/******************************************************************
	* Description : Procedure to fetch the request details for missing parts. this procedure returns all the request id and its request detail id's in a single line for which the parts are missing.
	* Author		: Brinal G
	****************************************************************/
	PROCEDURE gm_fch_missingparts_req_det (
		p_days			IN		 NUMBER
	  , p_writeoff_fl	IN		 VARCHAR2
	  , request_info	OUT 	 TYPES.cursor_type
	)
	AS
		v_from_dt	   DATE;
		v_to_dt 	   DATE;
		v_date		   DATE;
		v_dd		   VARCHAR2 (2);
		v_final_dd	   VARCHAR2 (2);
		v_second_dd    VARCHAR2 (2);
	BEGIN
		SELECT TO_CHAR (SYSDATE, 'DD'), get_rule_value ('FINAL', 'EMAIL'), get_rule_value ('SECOND', 'EMAIL')
		  INTO v_dd, v_final_dd, v_second_dd
		  FROM DUAL;

		IF v_dd = v_final_dd
		THEN
			SELECT TRUNC (SYSDATE - 1)
			  INTO v_from_dt
			  FROM DUAL;

			SELECT TO_DATE ('01/01/1900', 'MM/DD/YYYY')
			  INTO v_to_dt
			  FROM DUAL;
		ELSE
			SELECT TRUNC (SYSDATE - p_days)
			  INTO v_date
			  FROM DUAL;

			v_from_dt	:= v_date;
			v_to_dt 	:= v_date;
			
		END IF;

		OPEN request_info
		 FOR
			 -- with the new change  of  no second email the skipflg wont be used anymore
			 SELECT DISTINCT (req_id) req_id, DECODE (LEAST (SECOND, third), third, 'Y', 'N') skipflg, req_detail_id
						FROM (SELECT t525.c525_product_request_id req_id
								   , gm_pkg_cm_loaner_jobs.gm_fch_req_det_ids
																		   (t525.c525_product_request_id
																		  , p_writeoff_fl
																		  , v_from_dt
																		  , v_to_dt
																		   ) req_detail_id
								   , t525.c525_email_date + TO_NUMBER (v_second_dd) SECOND
								   , TO_DATE (	 TO_CHAR (ADD_MONTHS (t525.c525_email_date, 1), 'MM')
											  || '/'
											  || TO_NUMBER (v_final_dd)
											  || '/'
											  || TO_CHAR (ADD_MONTHS (t525.c525_email_date, 1), 'YYYY')
											, 'mm/dd/yyyy'
											 ) third
								FROM t412_inhouse_transactions t412
								   , t504a_loaner_transaction t504a
								   , t526_product_request_detail t526
								   , t525_product_request t525
							   WHERE t412.c412_status_fl < 4
								 AND t525.c525_email_date <=
															DECODE (p_writeoff_fl
																  , 'Y', t525.c525_email_date
																  , v_from_dt
																   )
								 AND t525.c525_email_date >= DECODE (p_writeoff_fl, 'Y', t525.c525_email_date, v_to_dt)
								 AND t412.c412_expected_return_date =
											DECODE (p_writeoff_fl
												  , 'Y', TRUNC (SYSDATE)
												  , t412.c412_expected_return_date
												   )
								 AND t525.c525_email_date IS NOT NULL
								 AND t504a.c504a_loaner_transaction_id = t412.c504a_loaner_transaction_id
								 AND t526.c526_product_request_detail_id = t504a.c526_product_request_detail_id
								 AND t525.c525_product_request_id = t526.c525_product_request_id
								 AND t412.c412_type IN (50159,1006571)
								 AND t412.c412_void_fl IS NULL
								 AND t504a.c504a_void_fl IS NULL
								 AND t526.c526_void_fl IS NULL
								 AND t525.c525_void_fl IS NULL);
	END gm_fch_missingparts_req_det;
	/******************************************************************
	* Description : Procedure to fetch the request details for missing parts for IH 
	* Author		: Arockia Prasath
	****************************************************************/
	PROCEDURE gm_fch_missparts_writeoff_req (
		p_writeoff_fl	IN		 VARCHAR2
	  , request_info	OUT 	 TYPES.cursor_type
	)
	AS
	BEGIN
		
		OPEN request_info
		 FOR
			 -- with the new change  of  no second email the skipflg wont be used anymore
			 		SELECT t525.c525_product_request_id req_id
								FROM t412_inhouse_transactions t412
								   , t504a_loaner_transaction t504a
								   , t526_product_request_detail t526
								   , t525_product_request t525
							   WHERE t412.c412_status_fl < 4
								 AND TRUNC(t412.c412_expected_return_date) <=
											DECODE (p_writeoff_fl
												  , 'Y', TRUNC (SYSDATE)
												  , t412.c412_expected_return_date
												   )
								 AND t504a.c504a_loaner_transaction_id = t412.c504a_loaner_transaction_id
								 AND t526.c526_product_request_detail_id = t504a.c526_product_request_detail_id
								 AND t525.c525_product_request_id = t526.c525_product_request_id
								 AND t412.c412_type IN (1006571)
								 AND t412.c412_void_fl IS NULL
								 AND t504a.c504a_void_fl IS NULL
								 AND t526.c526_void_fl IS NULL
								 AND t525.c525_void_fl IS NULL
			 		UNION
					 SELECT t504.c504_ref_id req_id
					   FROM t412_inhouse_transactions t412, t506_returns t506, t504_consignment t504
					  WHERE t412.c412_type                        in (9112)
					    AND t412.c412_status_fl                    < 4
					    AND TRUNC (t412.c412_expected_return_date) <= DECODE (p_writeoff_fl, 'Y', TRUNC (SYSDATE), TRUNC(t412.c412_expected_return_date))
					    AND t412.c412_void_fl                     IS NULL
					    AND t412.c412_ref_id                       = t506.c506_rma_id
					    AND t506.c901_ref_type                     = 9110
					    AND t506.c506_void_fl                     IS NULL
					    AND t504.c504_consignment_id               = t506.c506_ref_id
					    AND t504.c504_type                         = '9110'
					    AND t504.c504_void_fl                     IS NULL;
	END gm_fch_missparts_writeoff_req;
	--
/******************************************************************************
 * Description : Function to return the request detail ids in single row
 * Author : Bgupta
 ******************************************************************************/
--
	FUNCTION gm_fch_req_det_ids (
		p_request_id	IN	 t525_product_request.c525_product_request_id%TYPE
	  , p_writeoff_fl	IN	 VARCHAR2
	  , v_from_dt		IN	 DATE
	  , v_to_dt 		IN	 DATE
	)
		RETURN VARCHAR2
	IS
		v_reqids	   VARCHAR2 (4000);
	BEGIN
		SELECT	   SUBSTR (MAX (SYS_CONNECT_BY_PATH (req_detail_id, ',')), 2) req_detail_id
			  INTO v_reqids
			  FROM (SELECT req_detail_id, ROW_NUMBER () OVER (ORDER BY req_detail_id) AS curr
					  FROM (SELECT t526.c526_product_request_detail_id req_detail_id
							  FROM t412_inhouse_transactions t412
								 , t504a_loaner_transaction t504a
								 , t526_product_request_detail t526
								 , t525_product_request t525
							 WHERE t412.c412_status_fl < 4
							   AND t525.c525_email_date <= DECODE (p_writeoff_fl, 'Y', t525.c525_email_date, v_from_dt)
							   AND t525.c525_email_date >= DECODE (p_writeoff_fl, 'Y', t525.c525_email_date, v_to_dt)
							   AND t412.c412_expected_return_date =
											DECODE (p_writeoff_fl
												  , 'Y', TRUNC (SYSDATE)
												  , t412.c412_expected_return_date
												   )
							   AND t525.c525_email_date IS NOT NULL
							   AND t504a.c504a_loaner_transaction_id = t412.c504a_loaner_transaction_id
							   AND t526.c526_product_request_detail_id = t504a.c526_product_request_detail_id
							   AND t525.c525_product_request_id = t526.c525_product_request_id
							   AND t412.c412_type = 50159
							   AND t412.c412_void_fl IS NULL
							   AND t504a.c504a_void_fl IS NULL
							   AND t526.c526_void_fl IS NULL
							   AND t525.c525_void_fl IS NULL
							   AND t525.c525_product_request_id = p_request_id)) rinfo
		CONNECT BY curr - 1 = PRIOR curr
		START WITH curr = 1;

		RETURN v_reqids;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN '';
	END gm_fch_req_det_ids;

		/******************************************************************
	* Description : Procedure to writeoff the missing parts
	* Author		: Brinal G
	****************************************************************/
	PROCEDURE gm_sav_missingparts_writeoff (
		p_request_id   IN	t525_product_request.c525_product_request_id%TYPE
	  , p_cursor	   OUT  TYPES.cursor_type
	)
	AS
		CURSOR item_cursor
		IS
			SELECT	 t413.c412_inhouse_trans_id trans_id, t413.c205_part_number_id pnum ,t526.c207_set_id set_id
				   ,   SUM (DECODE (t413.c413_status_fl, 'Q', t413.c413_item_qty, 0))
					 - SUM (DECODE (c413_status_fl, 'Q', 0, c413_item_qty)) qty
				   , get_part_price_list (t413.c205_part_number_id, '') price
				   , gm_pkg_cm_loaner_jobs.get_missing_part_price (t413.c205_part_number_id)
			          * (  SUM (DECODE (t413.c413_status_fl, 'Q', t413.c413_item_qty, 0))
			            - SUM (DECODE (c413_status_fl, 'Q', 0, c413_item_qty))
			           ) echarge_price,
					 get_part_price_list (t413.c205_part_number_id, '')
					 * (  SUM (DECODE (t413.c413_status_fl, 'Q', t413.c413_item_qty, 0))
						- SUM (DECODE (c413_status_fl, 'Q', 0, c413_item_qty))
					   ) eprice
				   ,	'Qty:'
					 || (  SUM (DECODE (t413.c413_status_fl, 'Q', t413.c413_item_qty, 0))
						 - SUM (DECODE (c413_status_fl, 'Q', 0, c413_item_qty))
						)
					 || ' * Charge: $'
					 || gm_pkg_cm_loaner_jobs.get_missing_part_price (t413.c205_part_number_id) incidentvalue
				FROM t413_inhouse_trans_items t413
				   , t412_inhouse_transactions t412
				   , t504a_loaner_transaction t504a
				   , t526_product_request_detail t526
			   WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
			   	 AND t413.c205_part_number_id not in (
			         select c205_part_number_id from t208_set_details where  
			         c207_set_id in (  select c906_rule_value
			               from t906_rules
			              where c906_rule_grp_id = 'CHARGES'
			                AND c906_rule_id     = 'SETS'
			                AND c906_void_fl IS NULL)    
			                       AND c208_void_fl is null)
 				 AND c412_status_fl < 4
				 AND c412_type = 50159
				 AND t412.c504a_loaner_transaction_id = t504a.c504a_loaner_transaction_id
				 AND t504a.c526_product_request_detail_id = t526.c526_product_request_detail_id
				 AND t526.c525_product_request_id = p_request_id
				 AND c412_expected_return_date = TRUNC (SYSDATE)
				 AND t413.c413_void_fl IS NULL
				 AND t412.c412_void_fl IS NULL
				 AND t504a.c504a_void_fl IS NULL
				 AND t526.c526_void_fl IS NULL
			GROUP BY t413.c205_part_number_id, t413.c412_inhouse_trans_id,t526.c207_set_id
			  HAVING   SUM (DECODE (t413.c413_status_fl, 'Q', t413.c413_item_qty, 0))
					 - SUM (DECODE (c413_status_fl, 'Q', 0, c413_item_qty)) > 0
			ORDER BY t413.c412_inhouse_trans_id;

		v_repid 	   t703_sales_rep.c703_sales_rep_id%TYPE;
		v_distid	   t701_distributor.c701_distributor_id%TYPE;
		v_incident_id  t9200_incident.c9200_incident_id%TYPE;
		v_assoc_rep_id t525_product_request.c703_ass_rep_id%TYPE;
		v_chrg_dtl_id  NUMBER;
		
	BEGIN
		
		OPEN p_cursor FOR 
		SELECT	t526.c526_product_request_detail_id REQ_DET_ID, t413.c412_inhouse_trans_id trans_id, (t413.c205_part_number_id
	         || ' '
	         || get_partnum_desc (t413.c205_part_number_id)) pnum
				   ,   SUM (DECODE (t413.c413_status_fl, 'Q', t413.c413_item_qty, 0))
					 - SUM (DECODE (c413_status_fl, 'Q', 0, c413_item_qty)) qty
				   , get_part_price_list (t413.c205_part_number_id, '') price
				   ,   get_part_price_list (t413.c205_part_number_id, '')
					 * (  SUM (DECODE (t413.c413_status_fl, 'Q', t413.c413_item_qty, 0))
						- SUM (DECODE (c413_status_fl, 'Q', 0, c413_item_qty))
					   ) eprice
				   ,	'Qty:'
					 || (  SUM (DECODE (t413.c413_status_fl, 'Q', t413.c413_item_qty, 0))
						 - SUM (DECODE (c413_status_fl, 'Q', 0, c413_item_qty))
						)
					 || ' * List Price: $'
					 || get_part_price_list (t413.c205_part_number_id, '') incidentvalue
				FROM t413_inhouse_trans_items t413
				   , t412_inhouse_transactions t412
				   , t504a_loaner_transaction t504a
				   , t526_product_request_detail t526
			   WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
				 AND c412_status_fl < 4
				 AND c412_type = 50159
				 AND t412.c504a_loaner_transaction_id = t504a.c504a_loaner_transaction_id
				 AND t504a.c526_product_request_detail_id = t526.c526_product_request_detail_id
				 AND t526.c525_product_request_id = p_request_id
				 AND c412_expected_return_date = TRUNC (SYSDATE)
				 AND t413.c413_void_fl IS NULL
				 AND t412.c412_void_fl IS NULL
				 AND t504a.c504a_void_fl IS NULL
				 AND t526.c526_void_fl IS NULL
			GROUP BY t413.c205_part_number_id, t413.c412_inhouse_trans_id, t526.c526_product_request_detail_id
			  HAVING   SUM (DECODE (t413.c413_status_fl, 'Q', t413.c413_item_qty, 0))
					 - SUM (DECODE (c413_status_fl, 'Q', 0, c413_item_qty)) > 0
			ORDER BY REQ_DET_ID, t413.c412_inhouse_trans_id;
			
			-- to log the missing charges
			SELECT c703_sales_rep_id, get_distributor_id (c703_sales_rep_id), c703_ass_rep_id
			  INTO v_repid, v_distid, v_assoc_rep_id
			  FROM t525_product_request
			 WHERE c525_product_request_id = p_request_id AND c525_void_fl IS NULL;
			 
		FOR currindex IN item_cursor
		LOOP
			gm_cs_sav_loaner_recon_insert (currindex.trans_id
										 , currindex.pnum
										 , currindex.qty
										 , 'NOC#'
										 , currindex.price
										 , 'W'
										  );
			gm_save_ledger_posting (4845, CURRENT_DATE, currindex.pnum, '01', currindex.trans_id, currindex.qty, NULL, 30301);

			UPDATE t412_inhouse_transactions
			   SET c412_status_fl = 4	--completed
				 , c412_verified_date = SYSDATE
				 , c412_verified_by = 30301
				 , c412_last_updated_date = SYSDATE
				 , c412_last_updated_by = 30301
			 WHERE c412_inhouse_trans_id = currindex.trans_id AND c412_void_fl IS NULL;

			gm_pkg_op_loaner.gm_sav_incidents (50891
											 , p_request_id
											 , 92069
											 , currindex.set_id
											 , 30301
											 , TRUNC (SYSDATE)
											 , v_incident_id
											  );
											  
			gm_pkg_op_loaner.gm_sav_charge_details (v_incident_id, v_repid, v_distid, currindex.echarge_price, 20, 30301,v_assoc_rep_id);   -- approved

			BEGIN
				 SELECT c9201_charge_details_id
				   INTO v_chrg_dtl_id
				   FROM t9201_charge_details
				  WHERE c9200_incident_id = v_incident_id
				    AND c9201_void_fl    IS NULL;
			 EXCEPTION
				WHEN NO_DATA_FOUND
				THEN
					v_chrg_dtl_id := NULL;
			 END;
			
			 IF v_chrg_dtl_id IS NOT NULL
			THEN
			  UPDATE t9201_charge_details
			     SET c205_part_number = currindex.pnum
			   , c9201_unit_cost = currindex.price, c9201_qty_missing = currindex.qty
			   WHERE c9201_charge_details_id = v_chrg_dtl_id
			     AND c9201_void_fl IS NULL; 
			END IF;
		END LOOP;
		
		gm_pkg_op_loaner.gm_sav_set_level_missing_chrg(v_repid,v_distid);
		
	END gm_sav_missingparts_writeoff;
	
	
	/*************************************************************************
	* Description : Procedure to writeoff the inhouse missing parts IHMP,ISMP
	* Author		: Arockia Prasath
	**************************************************************************/
	PROCEDURE gm_sav_ihmissingparts_writeoff
	AS
	CURSOR item_cursor
		IS
	SELECT t413.c412_inhouse_trans_id trans_id, t413.c205_part_number_id pnum 
         , SUM (DECODE (t413.c413_status_fl, 'Q', t413.c413_item_qty, 0))
           - SUM (DECODE (c413_status_fl, 'Q', 0, c413_item_qty)) qty
         , get_part_price_list (t413.c205_part_number_id, '') price
         , t412.c412_type type 
      FROM t413_inhouse_trans_items t413
         , t412_inhouse_transactions t412
     WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
       AND c412_type  IN (1006571, 9112) --ISMP, IHMP
       AND TRUNC (c412_expected_return_date) <= TRUNC (SYSDATE)
       AND t413.c413_void_fl IS NULL
       AND t412.c412_void_fl IS NULL
       AND t412.c412_inhouse_trans_id NOT IN ('ISMP-679105')
  GROUP BY t413.c205_part_number_id, t413.c412_inhouse_trans_id,t412.c412_type 
    HAVING SUM (DECODE (t413.c413_status_fl, 'Q', t413.c413_item_qty, 0))
           - SUM (DECODE (c413_status_fl, 'Q', 0, c413_item_qty)) > 0;           
					 
p_out VARCHAR2(1000);

	BEGIN			 
		FOR currindex IN item_cursor
		LOOP			   		
			gm_pkg_op_process_recon.gm_sav_inhouse_recon(
                currindex.trans_id, --  ISMP-xxxx,IHMP-xxxx
                currindex.type,     --	ISMP,IHMP          
                NULL,               --	No Need for sold String
                NULL,               -- 	No Need for Consign String
                (currindex.pnum||','||currindex.qty||',NOC#'||','||currindex.price || '|'),  -- [Write Off String - pnum,qty,control,price|]
                NULL,               -- No Need for Return String
                '30301',            -- '30301'
                p_out               -- '' 
			);
			
			UPDATE t412_inhouse_transactions
			   SET c412_status_fl = 4	-- Completed
				 , c412_verified_date = SYSDATE
				 , c412_verified_by = 30301
				 , c412_last_updated_date = SYSDATE
				 , c412_last_updated_by = 30301
			 WHERE c412_inhouse_trans_id = currindex.trans_id 
			   AND c412_void_fl IS NULL;
			   
		END LOOP;
	END gm_sav_ihmissingparts_writeoff;

	/******************************************************************
		* Description : Procedure to get Loaners rep ids	whose sets are due
		* at Globus
		* Author		 : Mihir , Sep 02 2010 Velu added two variables and set values. 
		****************************************************************/
	PROCEDURE gm_fch_loaner_pending_retn_rid (
	    p_param    IN    VARCHAR2
	   ,p_repids   OUT	 TYPES.cursor_type
	)
	AS
  v_day varchar2(10);
  v_daystoadd number(2);
  v_date  DATE;
  v_date_fmt t906_rules.c906_rule_value%TYPE;
  v_company_id t1900_company.c1900_company_id%TYPE;
  v_filter T906_RULES.C906_RULE_VALUE%TYPE;
  v_plant_id T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
  
	BEGIN
		
	SELECT NVL(get_compdtfmt_frm_cntx(),get_rule_value('DATEFMT','DATEFORMAT')), get_compid_frm_cntx(), get_plantid_frm_cntx() 
	INTO v_date_fmt ,v_company_id , v_plant_id
	FROM DUAL;
	
	 SELECT DECODE(GET_RULE_VALUE(v_company_id,'LOANERJOBS'), null, v_company_id, v_plant_id) --LOANERJOBS- To fetch the records for EDC entity countries based on plant
       INTO v_filter 
       FROM DUAL;

    SELECT trim(TO_CHAR(CURRENT_DATE,'DAY')) INTO v_day FROM DUAL; 
   IF v_day = 'FRIDAY' 
    THEN
         v_daystoadd := 3;
      ELSE
         v_daystoadd := 1;
     END IF;
      SELECT cdate
	    INTO v_date
	    FROM
	    (SELECT cal_date cdate
	       FROM date_dim
	      WHERE cal_date   >= TRUNC (CURRENT_DATE) + v_daystoadd
	        AND cal_date   <= TRUNC (CURRENT_DATE) + 8
	        AND DAY NOT IN ('Sat','Sun')
	        AND date_key_id NOT IN (
				              	SELECT date_key_id 
				              	  FROM COMPANY_HOLIDAY_DIM 
				              	 WHERE HOLIDAY_DATE   >= TRUNC (CURRENT_DATE) + v_daystoadd
				              	   AND HOLIDAY_DATE   <= TRUNC (CURRENT_DATE) + 8
				              	   AND HOLIDAY_FL = 'Y'
				              	    AND C1900_COMPANY_ID = v_company_id
								)
	   ORDER BY cal_date
	    )
	  WHERE rownum = 1;
	  
	  -- for one day over due sets
	 IF p_param = 'OVERDUE' THEN
	    v_date :=trunc(CURRENT_DATE)-1;
	  END IF;
	  -- for more than one day over due sets
	  IF p_param = 'ALLOVERDUE' THEN
		OPEN p_repids
		 FOR
   			 SELECT DISTINCT rep_id, rep_email, rep_name, assocrepid, assocrepemail, assocrepnm, consigned_toid, transdtfmt
						FROM (SELECT t504atxn.c703_sales_rep_id rep_id
								   , DECODE(t504atxn.c703_sales_rep_id,NULL,get_distributor_name(t504atxn.c504a_consigned_to_id),get_rep_name (t504atxn.c703_sales_rep_id)) rep_name
								   , NVL (DECODE (t504atxn.c703_sales_rep_id, NULL, get_cs_ship_email (4120, t504atxn.c504a_consigned_to_id), get_cs_ship_email (4121
								   , t504atxn.c703_sales_rep_id)), get_rule_value_by_company ('GMCSMAILID', 'SHIPDTL', v_company_id)) rep_email
								   , t504atxn.c504a_expected_return_dt d_date ,t504atxn.c504a_consigned_to_id consigned_toid
								   , get_request_type(t504atxn.c526_product_request_detail_id) reqtype
								   , t504atxn.c703_ass_rep_id assocrepid
								   , get_rep_name(t504atxn.c703_ass_rep_id) assocrepnm
								   , DECODE(t504atxn.c703_ass_rep_id, NULL, NULL,NVL(get_cs_ship_email (4121, t504atxn.c703_ass_rep_id), get_rule_value_by_company ('GMCSTOMAILID', 'SHIPDTL', v_company_id))) assocrepemail
								   , get_company_dtfmt(t504atxn.c1900_company_id) transdtfmt
								FROM t504_consignment t504
								   , t504a_consignment_loaner t504a
								   , t504a_loaner_transaction t504atxn
							   WHERE t504.c504_status_fl = '4'
								 AND t504.c504_verify_fl = '1'
								 AND t504.c504_void_fl IS NULL
								 AND t504a.c504a_void_fl IS NULL
								 AND t504atxn.c504a_void_fl IS NULL
								 AND t504.c207_set_id IS NOT NULL
								 AND t504.c504_type = 4127
								 AND t504a.c504a_status_fl IN ('20')
								 AND t504atxn.c504a_return_dt IS NULL
								 AND trunc(t504atxn.c504a_expected_return_dt) < trunc(CURRENT_DATE)-1
								 AND t504.c504_consignment_id = t504atxn.c504_consignment_id
								 AND t504.c504_consignment_id = t504a.c504_consignment_id
								 AND (t504atxn.c1900_company_id = v_filter
		    					 OR t504atxn.c5040_plant_id   = v_filter))
						WHERE reqtype =4127
					ORDER BY rep_id,assocrepid;
		ELSE
		OPEN p_repids
		 FOR
   			 SELECT DISTINCT rep_id, rep_email, rep_name, assocrepid, assocrepemail, assocrepnm, d_date ,consigned_toid, transdtfmt
						FROM (SELECT t504atxn.c703_sales_rep_id rep_id
								   , DECODE(t504atxn.c703_sales_rep_id,NULL,get_distributor_name(t504atxn.c504a_consigned_to_id),get_rep_name (t504atxn.c703_sales_rep_id)) rep_name
								   , NVL (DECODE (t504atxn.c703_sales_rep_id, NULL, get_cs_ship_email (4120, t504atxn.c504a_consigned_to_id), get_cs_ship_email (4121
								   , t504atxn.c703_sales_rep_id)), get_rule_value_by_company ('GMCSMAILID', 'SHIPDTL' ,v_company_id)) rep_email
								   , t504atxn.c504a_expected_return_dt d_date ,t504atxn.c504a_consigned_to_id consigned_toid
								   , get_request_type(t504atxn.c526_product_request_detail_id) reqtype
								   , t504atxn.c703_ass_rep_id assocrepid
								   , get_rep_name(t504atxn.c703_ass_rep_id) assocrepnm
								   , DECODE(t504atxn.c703_ass_rep_id, NULL, NULL, NVL(get_cs_ship_email (4121, t504atxn.c703_ass_rep_id), get_rule_value_by_company ('GMCSTOMAILID', 'SHIPDTL', v_company_id))) assocrepemail
								   , get_company_dtfmt(t504atxn.c1900_company_id) transdtfmt
								FROM t504_consignment t504
								   , t504a_consignment_loaner t504a
								   , t504a_loaner_transaction t504atxn
							   WHERE t504.c504_status_fl = '4'
								 AND t504.c504_verify_fl = '1'
								 AND t504.c504_void_fl IS NULL
								 AND t504a.c504a_void_fl IS NULL
								 AND t504atxn.c504a_void_fl IS NULL
								 AND t504.c207_set_id IS NOT NULL
								 AND t504.c504_type = 4127
								 AND t504a.c504a_status_fl IN ('20')
								 AND t504atxn.c504a_return_dt IS NULL
								 AND t504atxn.c504a_expected_return_dt = v_date
								 AND t504.c504_consignment_id = t504atxn.c504_consignment_id
								 AND t504.c504_consignment_id = t504a.c504_consignment_id
								 AND (t504atxn.c1900_company_id = v_filter
		    					 OR t504atxn.c5040_plant_id   = v_filter))
						WHERE reqtype =4127
					ORDER BY rep_id,assocrepid;
		END IF;
		
	END gm_fch_loaner_pending_retn_rid;

	/******************************************************************
		* Description : Procedure to get loaners details whose sets are
		* due back at Globus
		* Author		 : Mihir
		****************************************************************/
	PROCEDURE gm_fch_loaner_pending_retn_det (
		p_repid 		IN		 t504a_loaner_transaction.c703_sales_rep_id%TYPE
	  , p_assrepid 		IN		 t504a_loaner_transaction.c703_ass_rep_id%TYPE
	  ,	p_consig_toid   IN       t504a_loaner_transaction.c504a_consigned_to_id%TYPE
	  , p_param         IN       VARCHAR2
	  , p_rep_details	OUT 	 TYPES.cursor_type
	)
	AS
	v_day 			varchar2(10);
	v_daystoadd 	number(2);
	v_date  		DATE;
	v_date_fmt		t906_rules.c906_rule_value%TYPE;
	v_company_id    T1900_COMPANY.C1900_COMPANY_ID%TYPE;
 	v_filter        T906_RULES.C906_RULE_VALUE%TYPE;
 	v_plant_id      T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
 	v_rep_cmp_id	T1900_COMPANY.C1900_COMPANY_ID%TYPE;
 	v_rep_dt_fmt    VARCHAR2(20);
	
	BEGIN
					
		SELECT get_compid_frm_cntx(), get_plantid_frm_cntx()
			INTO   v_company_id , v_plant_id
			FROM DUAL;
			
		 SELECT C1900_COMPANY_ID
		 INTO v_rep_cmp_id 
		 FROM T703_SALES_REP WHERE C703_SALES_REP_ID = NVL(p_repid,p_assrepid);
		 
		 SELECT NVL(get_company_dtfmt(v_rep_cmp_id) ,get_compdtfmt_frm_cntx())
		 INTO v_rep_dt_fmt 
		 FROM DUAL;
					 
	    SELECT DECODE(GET_RULE_VALUE(v_company_id,'LOANERJOBS'), null, v_company_id, v_plant_id) --LOANERJOBS- To fetch the records for EDC entity countries based on plant
       		INTO v_filter 
       		FROM DUAL;
       		
	  SELECT trim(TO_CHAR(sysdate,'DAY')) INTO v_day FROM DUAL; 
   IF v_day = 'FRIDAY' 
    THEN
         v_daystoadd := 3;
      ELSE
         v_daystoadd := 1;
     END IF;
      SELECT cdate
	    INTO v_date
	    FROM
	    (SELECT cal_date cdate
	       FROM date_dim
	      WHERE cal_date   >= TRUNC (SYSDATE) + v_daystoadd
	        AND cal_date   <= TRUNC (SYSDATE) + 8
	        AND DAY NOT IN ('Sat','Sun')
	        AND date_key_id NOT IN (
				              	SELECT date_key_id 
				              	  FROM COMPANY_HOLIDAY_DIM 
				              	 WHERE HOLIDAY_DATE   >= TRUNC (SYSDATE) + v_daystoadd
				              	   AND HOLIDAY_DATE   <= TRUNC (SYSDATE) + 8
				              	   AND HOLIDAY_FL = 'Y'
				              	    AND C1900_COMPANY_ID = v_company_id
								)
	   ORDER BY cal_date
	    )
	  WHERE rownum = 1;
	 -- for one day over due sets
	  IF p_param = 'OVERDUE' THEN
	    v_date :=trunc(sysdate)-1;
	  END IF;
	  -- for more than one day over due sets
	  IF p_param = 'ALLOVERDUE' THEN
		OPEN p_rep_details
		 FOR
			 SELECT t504.c207_set_id set_id, get_set_name (t504.c207_set_id) set_name
				  , TO_CHAR (t504atxn.c504a_expected_return_dt, v_rep_dt_fmt) due_date,t504a.c504a_etch_id etch_id
				  , gm_pkg_op_loaner.get_loaner_request_id(t504atxn.c504a_loaner_transaction_id) reqid
			   FROM t504_consignment t504, t504a_consignment_loaner t504a, t504a_loaner_transaction t504atxn
			  WHERE t504.c504_status_fl = '4'
				AND t504.c504_verify_fl = '1'
				AND t504.c504_void_fl IS NULL
				AND t504a.c504a_void_fl IS NULL
				AND t504atxn.c504a_void_fl IS NULL
				AND t504.c207_set_id IS NOT NULL
				AND t504.c504_type = 4127 	
				AND get_request_type(t504atxn.c526_product_request_detail_id)  = 4127
				AND t504a.c504a_status_fl IN ('20')
				AND t504atxn.c504a_return_dt IS NULL
				AND trunc(t504atxn.c504a_expected_return_dt) < trunc(sysdate)-1
				AND NVL(t504atxn.c703_sales_rep_id,-999) = NVL(p_repid,-999)
				AND NVL(t504atxn.c703_ass_rep_id,-999) = NVL(p_assrepid, -999)
				AND t504atxn.c504a_consigned_to_id = p_consig_toid
				AND t504.c504_consignment_id = t504a.c504_consignment_id
				AND t504.c504_consignment_id = t504atxn.c504_consignment_id
				AND (t504atxn.c1900_company_id = v_filter OR t504atxn.c5040_plant_id   = v_filter)
				ORDER BY reqid;
	  ELSE
		OPEN p_rep_details
		 FOR
			 SELECT t504.c207_set_id set_id, get_set_name (t504.c207_set_id) set_name
				  , TO_CHAR (t504atxn.c504a_expected_return_dt, v_rep_dt_fmt) due_date,t504a.c504a_etch_id etch_id
				  , gm_pkg_op_loaner.get_loaner_request_id(t504atxn.c504a_loaner_transaction_id) reqid
			   FROM t504_consignment t504, t504a_consignment_loaner t504a, t504a_loaner_transaction t504atxn
			  WHERE t504.c504_status_fl = '4'
				AND t504.c504_verify_fl = '1'
				AND t504.c504_void_fl IS NULL
				AND t504a.c504a_void_fl IS NULL
				AND t504atxn.c504a_void_fl IS NULL
				AND t504.c207_set_id IS NOT NULL
				AND t504.c504_type = 4127
				AND get_request_type(t504atxn.c526_product_request_detail_id)  = 4127
				AND t504a.c504a_status_fl IN ('20')
				AND t504atxn.c504a_return_dt IS NULL
				AND t504atxn.c504a_expected_return_dt = v_date
				AND NVL(t504atxn.c703_sales_rep_id,-999) = NVL(p_repid,-999)
				AND NVL(t504atxn.c703_ass_rep_id,-999) = NVL(p_assrepid,-999)
				AND t504atxn.c504a_consigned_to_id = p_consig_toid
				AND t504.c504_consignment_id = t504a.c504_consignment_id
				AND t504.c504_consignment_id = t504atxn.c504_consignment_id
				AND (t504atxn.c1900_company_id = v_filter OR t504atxn.c5040_plant_id   = v_filter)
				ORDER BY reqid;	  
	  END IF;
		
	END gm_fch_loaner_pending_retn_det;
	/******************************************************************
	* Description : Procedure to allocate loaner to open request
	* 
	* Author	  : Dhana Reddy
	****************************************************************/
	
	PROCEDURE gm_move_open_to_allocated
	AS
	v_loaner_req_day     NUMBER;
	v_holiday_flag 		 VARCHAR2 (1) ;
	-- Getting date format from rules table
	v_date_fmt			 t906_rules.c906_rule_value%TYPE:= NVL(get_rule_value('DATEFMT','DATEFORMAT'),'MM/DD/YYYY');  
	v_company_id NUMBER;
	v_time_zone  VARCHAR2 (200);
	v_error_req_set  t207_set_master.c207_set_id%TYPE;
	subject 	   VARCHAR2 (200);
	to_mail 	   VARCHAR2 (400);
	mail_body	   VARCHAR2 (20000);
	
	CURSOR  loanerreq_cur
    IS
		SELECT c526_product_request_detail_id  req_det_id ,c207_set_id  req_set_id, C901_REQUEST_TYPE req_type, get_rule_value (C901_REQUEST_TYPE, 'LOANERREQDAYJOB') no_of_days
          FROM t526_product_request_detail
         WHERE c526_void_fl IS NULL
           AND c526_status_fl = 10 --open           
           AND (get_work_days_count(TO_CHAR(CURRENT_DATE,v_date_fmt),TO_CHAR(c526_required_date,v_date_fmt))<= get_rule_value (C901_REQUEST_TYPE, 'LOANERREQDAYJOB') 
           AND TRUNC(c526_required_date - (CURRENT_DATE))>=0);
 	BEGIN
	 	
	 	v_error_req_set := NULL;
	 	 -- check if the date is holiday.
	 	BEGIN
	 		--PMT-14186 Enable Loaner allocation Job for GM Japan , added time zone as one portal change to support by other countries.
	 		SELECT  get_compid_frm_cntx(),get_comptimezone_frm_cntx()
			  INTO  v_company_id,v_time_zone
			  FROM  DUAL;
			  
			EXECUTE IMMEDIATE 'ALTER SESSION SET TIME_ZONE='''||v_time_zone||'''';
		
		  /* For the Passed in Date if the record is available in
		   	* Company Holiday Day Dim then it's considered as Holiday, else 
			* we will check if its a Sat/Sun from the Date Dim to determine if the passed in date is a holiday.
			*/
		  SELECT NVL(CHD.HOLIDAY_FL,DD.HOLIDAY_IND)
		   INTO v_holiday_flag
	       FROM date_dim DD, COMPANY_HOLIDAY_DIM CHD
	      WHERE DATE_MM_DD_YYYY = TO_CHAR(CURRENT_DATE,'MM/DD/YYYY') 
	        AND DD.date_key_id = CHD.date_key_id(+)
	      	    AND C1900_COMPANY_ID(+) = v_company_id;
		EXCEPTION
		WHEN NO_DATA_FOUND THEN
			v_holiday_flag := 'N';
	    END;
	    
	    -- if the date is not holiday, then call allocation procedure
	 	IF v_holiday_flag != 'Y' 
	 	THEN	 	
		 	FOR opreqconsign IN loanerreq_cur
		    LOOP    	  
		    	 BEGIN
			     
		    	 gm_pkg_op_loaner_allocation.gm_sav_allocate (opreqconsign.req_set_id,opreqconsign.req_type, opreqconsign.no_of_days,'30301' );	    
		    	 
		    	EXCEPTION WHEN OTHERS
                 THEN	 
                   
                 v_error_req_set := opreqconsign.req_set_id;
                
                 to_mail 	:= get_rule_value ('LOANER_ALLOCATION', 'EMAIL');
				 subject 	:= ' Loaner allocation job failure for '||  get_company_name (v_company_id);
				 mail_body	:= ' There are requests not allocated from ' || v_error_req_set || ' due to failure in the loaner allocation job. Exception below ' || sqlcode || ' ' || sqlerrm;
		    
		        gm_com_send_email_prc (to_mail, subject, mail_body);
                
                END;    	 
		    
		    END LOOP;
		END IF;
		
	END gm_move_open_to_allocated;

/***************************************************************
* Description : Procedure to make entries in Ticket Table
* Author	  : Vprasath
****************************************************************/

PROCEDURE gm_sav_jira_ticket (
   p_ticket_id   IN   t9800_jira_ticket.c9800_ticket_id%TYPE,
   p_ref_id      IN   t9800_jira_ticket.c9800_ref_id%TYPE,
   p_ref_type    IN   t9800_jira_ticket.c9800_ref_type%TYPE,
   p_user        IN   t9800_jira_ticket.c9800_created_by%TYPE
)
AS
BEGIN
   UPDATE t9800_jira_ticket
      SET c9800_ref_id = p_ref_id,
          c9800_ref_type = p_ref_type,
          c9800_updated_by = p_user,
          c9800_updated_date = SYSDATE
    WHERE c9800_ticket_id = p_ticket_id
      AND c9800_void_fl IS NULL;

   IF SQL%ROWCOUNT = 0
   THEN
      INSERT INTO t9800_jira_ticket
                  (c9800_ticket_id, c9800_ref_id, c9800_ref_type,
                   c9800_created_by, c9800_created_date
                  )
           VALUES (p_ticket_id, p_ref_id, p_ref_type,
                   p_user, SYSDATE
                  );
   END IF;
END gm_sav_jira_ticket;

/******************************************************************
* Description : Procedure to fetch Ticket ID based on 
*               Ref Type and Ref ID
* Author	  : Vprasath
****************************************************************/

PROCEDURE gm_fch_jira_ticket (
   p_ref_id     IN       t9800_jira_ticket.c9800_ref_id%TYPE,
   p_ref_type   IN       t9800_jira_ticket.c9800_ref_type%TYPE,
   p_out        OUT      t9800_jira_ticket.c9800_ticket_id%TYPE
)
AS
BEGIN
   SELECT c9800_ticket_id
     INTO p_out
     FROM t9800_jira_ticket
    WHERE c9800_ref_id = p_ref_id
      AND c9800_ref_type = p_ref_type
      AND c9800_void_fl IS NULL;
END gm_fch_jira_ticket;

/***************************************************************
 * Description : Function to get missing part price
 * Author      : dreddy
 ***************************************************************/
FUNCTION get_missing_part_price (
        p_part_num T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE)
    RETURN VARCHAR2
IS
    v_price              VARCHAR2 (20) ;
    v_list_price         NUMBER ;
    v_half_price         NUMBER ;
    v_product_family     VARCHAR2 (20) ;
    v_implants_charge    NUMBER ;
    v_instruments_charge NUMBER ;
BEGIN
	v_implants_charge := TO_NUMBER(get_rule_value ('IMPLANTS', 'CHARGES'));
    v_instruments_charge := TO_NUMBER(get_rule_value ('INSTRUMENTS', 'CHARGES'));
    v_list_price := TO_NUMBER(get_part_price_list (p_part_num, '')) ;
    v_half_price := ROUND (v_list_price / 2, 0) ;
    BEGIN
         SELECT c205_product_family
           INTO v_product_family
           FROM T205_PART_NUMBER
          WHERE C205_PART_NUMBER_ID = p_part_num;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_product_family := NULL;
    END;
    IF v_product_family = '4050' --implants
        THEN
        IF v_half_price <= v_implants_charge THEN
            v_price     := v_half_price;
        ELSE
            v_price := v_implants_charge;
        END IF;
    ELSIF v_product_family = '4051' OR v_product_family = '26240095'  --Instruments or Frequently Sold
        THEN
        IF v_half_price <= v_instruments_charge THEN
            v_price     := v_half_price;
        ELSE
            v_price := v_instruments_charge;
        END IF;
    ELSE
        v_price := v_list_price;
    END IF;
    RETURN v_price;
END get_missing_part_price;


	/*************************************************************************
	* Description : Procedure to writeoff the inhouse missing parts IHMP,ISMP
	* Author		: Xun
	**************************************************************************/
	PROCEDURE gm_sav_ihtxn_writeoff
	AS
	v_company_id  t1900_company.c1900_company_id%TYPE;
	v_plant_id  t5040_plant_master.c5040_plant_id%TYPE;
	    p_out VARCHAR2(1000); 
    
         CURSOR item_cursor
		   IS
		SELECT t413.c412_inhouse_trans_id trans_id,
		  t413.c205_part_number_id pnum ,
		  SUM (DECODE (t413.c413_status_fl, 'Q', t413.c413_item_qty, 0)) - SUM (DECODE (c413_status_fl, 'Q', 0, c413_item_qty)) qty ,
		  get_part_price_list (t413.c205_part_number_id, '') price ,
		  t412.c412_type type
		FROM t413_inhouse_trans_items t413 ,
		  t412_inhouse_transactions t412
		WHERE t412.c412_inhouse_trans_id       = t413.c412_inhouse_trans_id
		AND c412_type                         IN (1006571, 9112) --ISMP, IHMP
		AND TRUNC (c412_expected_return_date) <= TRUNC(CURRENT_DATE)
		AND t413.c413_void_fl                 IS NULL
		AND t412.c412_void_fl                 IS NULL
		AND T412.C1900_COMPANY_ID              = v_company_id
		AND T412.C5040_PLANT_ID                = v_plant_id
		GROUP BY t413.c205_part_number_id,
		  t413.c412_inhouse_trans_id,
		  t412.c412_type
		HAVING SUM (DECODE (t413.c413_status_fl, 'Q', t413.c413_item_qty, 0)) - SUM (DECODE (c413_status_fl, 'Q', 0, c413_item_qty)) > 0;
		
	BEGIN
		
	SELECT NVL(get_compid_frm_cntx(),GET_RULE_VALUE('DEFAULT_COMPANY','ONEPORTAL')),
	       NVL(get_plantid_frm_cntx(),GET_RULE_VALUE('DEFAULT_PLANT','ONEPORTAL')) 
	  INTO v_company_id,v_plant_id FROM DUAL;

	gm_pkg_cor_client_context.gm_sav_client_context('1000','US/Eastern','MM/DD/YYYY','');
	
    FOR currindex IN item_cursor
		LOOP                                       
			BEGIN
						gm_pkg_op_process_recon.gm_sav_inhouse_recon(
							currindex.trans_id, --  ISMP-xxxx,IHMP-xxxx
							currindex.type,     --        ISMP,IHMP          
							NULL,               --  No Need for sold String
							NULL,               -- No Need for Consign String
							(currindex.pnum||','||currindex.qty||',NOC#'||','||currindex.price || '|'),  -- [Write Off String - pnum,qty,control,price|]
							NULL,               -- No Need for Return String
							'30301',            -- '30301'
							p_out               -- '' 
						);
						
						UPDATE t412_inhouse_transactions
						   SET c412_status_fl = 4 -- Completed
										, c412_verified_date = CURRENT_DATE
										, c412_verified_by = 30301
										, c412_last_updated_date = CURRENT_DATE
										, c412_last_updated_by = 30301
						WHERE c412_inhouse_trans_id = currindex.trans_id 
						   AND c412_void_fl IS NULL;
						EXCEPTION WHEN OTHERS THEN
						  DBMS_OUTPUT.PUT_LINE('EMAIL_EXCEPTION');
			END;

		END LOOP;
	END gm_sav_ihtxn_writeoff;
	
/********************************************************************************
 * Description : Function to get Jira Ticket ID for Request or Request Detail id
 * Author      : Vinoth
 ********************************************************************************/
FUNCTION get_jira_ticket_id (
    p_request_detail_id	IN	t526_product_request_detail.c526_product_request_detail_id%TYPE,
    p_request_id		IN	t526_product_request_detail.c525_product_request_id%TYPE)
    RETURN VARCHAR2
IS
	v_ticket_id t9800_jira_ticket.c9800_ticket_id%TYPE;
BEGIN

    BEGIN
    	 SELECT c9800_ticket_id INTO v_ticket_id
           FROM t9800_jira_ticket 
          WHERE c9800_ref_id = p_request_detail_id
            AND c9800_ref_type = 10056
            AND c9800_void_fl IS NULL; 
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
    	BEGIN
	        SELECT c9800_ticket_id INTO v_ticket_id
	          FROM t9800_jira_ticket 
	         WHERE c9800_ref_id = p_request_id
	           AND c9800_ref_type = 110521
	           AND c9800_void_fl IS NULL; 
		EXCEPTION
    	WHEN NO_DATA_FOUND THEN
			v_ticket_id := NULL;    	           
   	    END;
   	 END;
    
    RETURN v_ticket_id;
    
END get_jira_ticket_id;

END gm_pkg_cm_loaner_jobs;
/
