/* Formatted on 2011/03/25 16:54 (Formatter Plus v4.8.0) */
--@"c:\Database\Packages\common\gm_pkg_common_history.bdy"
CREATE OR REPLACE PACKAGE BODY gm_pkg_common_history IS
/***************************************************************************************************
  * Description : This Procedure is used to fetch log history details 
  * Author      : Mselvamani
 *****************************************************************************************************/

    PROCEDURE gm_fch_common_history (
        p_txn_id     IN    VARCHAR2,
        p_rule_id    IN    t906_rules.c906_rule_id%TYPE,
        p_date_fmt   IN    VARCHAR2,
        p_type		 IN	   NUMBER,
        p_out        OUT   CLOB
    ) AS
        v_rule_value    VARCHAR2(2000);
        v_exec_string   VARCHAR2(2000);
    BEGIN

-- Dynamic Code for void functionalities added
        SELECT
            get_rule_value(p_rule_id, 'CMNHL')
        INTO v_rule_value
        FROM
            dual;

        IF v_rule_value IS NULL THEN
            gm_raise_application_error('-20999', '58', p_rule_id);
        END IF;
        
        IF p_rule_id = 'SOP_LATE_FEE_DETAIL' THEN
        	v_exec_string := 'BEGIN '
                         || v_rule_value
                         || '(:p_txn_id,:p_date_fmt,:p_type,:p_out); END;';
        EXECUTE IMMEDIATE v_exec_string
            USING p_txn_id, p_date_fmt, p_type, OUT p_out;
            
        ELSE
        v_exec_string := 'BEGIN '
                         || v_rule_value
                         || '(:p_txn_id,:p_date_fmt,:p_out); END;';
        EXECUTE IMMEDIATE v_exec_string
            USING p_txn_id, p_date_fmt, OUT p_out;
            
        END IF;
        
    END gm_fch_common_history;

END gm_pkg_common_history;
/