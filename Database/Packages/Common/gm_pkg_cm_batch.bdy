/* Formatted on 2009/09/17 10:48 (Formatter Plus v4.8.0) */
-- @"C:\Database\Packages\Common\gm_pkg_cm_batch.bdy";
CREATE OR REPLACE
PACKAGE BODY gm_pkg_cm_batch
IS
    --
    /******************************************************************
    * Description : Procedure used to fetch batch report based on the batch ID
    ****************************************************************/
PROCEDURE gm_fch_batch_rpt (
        p_batch_id     IN T9600_BATCH.C9600_BATCH_ID%TYPE,
        p_batch_type   IN T9600_BATCH.C901_BATCH_TYPE%TYPE,
        p_batch_sts    IN T9600_BATCH.C901_BATCH_STATUS%TYPE,
        p_batch_frm_dt IN T9600_BATCH.C9600_CREATED_DATE%TYPE,
        p_batch_to_dt  IN T9600_BATCH.C9600_CREATED_DATE%TYPE,
        p_out_cur OUT TYPES.cursor_type)
AS
 v_company_id        t1900_company.c1900_company_id%TYPE;
BEGIN
	
	 SELECT get_compid_frm_cntx() 
	   INTO v_company_id  
	   FROM dual;
	
    OPEN p_out_cur FOR 
    SELECT t9600.c9600_batch_id batchid, get_code_name (t9600.c901_batch_status) batchStatus,
           get_code_name (t9600.c901_batch_type) batchType, t9600.c901_batch_type bType, 
           get_user_name (t9600.c9600_created_by) batchCreatedBy,
           t9600.c9600_created_date batchCreatedDate, get_user_name (t9600.c9600_initiated_by) batchInitiatedBy, 
           t9600.c9600_initiated_date initiateddate, get_user_name (t9600.c9600_last_update_by) lby,
           t9600.c9600_last_updated_date ldt, get_user_name (t9600.c9600_released_by) batchReleasedBy,
           t9600.c9600_released_date batchReleasedDate, t9600.c901_batch_status bStatus
      FROM t9600_batch t9600 
      WHERE t9600.c9600_batch_id = NVL (p_batch_id,t9600.c9600_batch_id) 
      AND t9600.c901_batch_type  = DECODE (p_batch_type, 0, t9600.c901_batch_type, p_batch_type) 
      AND t9600.c901_batch_status= DECODE (p_batch_sts , 0, t9600.c901_batch_status, p_batch_sts) 
      AND TRUNC (t9600.c9600_created_date) >= TRUNC(DECODE ( p_batch_frm_dt, NULL, t9600.c9600_created_date, p_batch_frm_dt)) 
      AND TRUNC (t9600.c9600_created_date) <= TRUNC(DECODE (p_batch_to_dt, NULL, t9600.c9600_created_date, p_batch_to_dt)) 
      AND t9600.C1900_COMPANY_ID = v_company_id
      AND t9600.c9600_void_fl  IS NULL
 ORDER BY t9600.c901_batch_type,t9600.c9600_batch_id;
 
END gm_fch_batch_rpt;
/******************************************************************
* Description : Procedure used to fetch the batch header
****************************************************************/
PROCEDURE gm_fch_batch_header (
        p_batch_id IN T9600_BATCH.C9600_BATCH_ID%TYPE,
        p_out_cur OUT TYPES.cursor_type)
AS
BEGIN
    OPEN p_out_cur FOR SELECT t9600.c9600_batch_id batchid, t9600.c901_batch_status batchStatus, get_code_name (t9600.c901_batch_status)
    batchStatusNm, t9600.c901_batch_type batchType, get_code_name ( t9600.c901_batch_type) batchTypeNm
  , t9600.c9600_created_by batchCreatedBy, get_user_name (t9600.c9600_created_by) batchCreatedByNm, 
    t9600.c9600_created_date batchCreatedDt, t9600.c9600_initiated_by
    batchInitiatedBy, get_user_name ( t9600.c9600_initiated_by) batchInitiatedByNm, t9600.c9600_initiated_date
     batchInitiatedDt, get_user_name (t9600.c9600_last_update_by) batchlastUpdatedByNm
    , t9600.c9600_last_update_by lastUpdatedBy,  t9600.c9600_last_updated_date lastUpdatedDt, t9600.c9600_released_by batchReleasedBy, get_user_name ( t9600.c9600_released_by)
    batchReleasedByNm, t9600.c9600_released_date batchReleasedDt
   FROM t9600_batch t9600
  WHERE t9600.c9600_batch_id = p_batch_id
    AND t9600.c9600_void_fl IS NULL
ORDER BY t9600.c9600_batch_id;
END gm_fch_batch_header;
/******************************************************************
* Description : Procedure used to save the batch
****************************************************************/
PROCEDURE gm_sav_batch (
        p_batch_type IN T9600_BATCH.c901_batch_type%TYPE,
        p_batch_status IN T9600_BATCH.c901_batch_status%TYPE,
        p_user_id    IN T9600_BATCH.c9600_created_by%TYPE,
        p_batch_id IN OUT T9600_BATCH.c9600_batch_id%TYPE)
AS

BEGIN
	
	gm_pkg_cm_batch.gm_sav_batch(p_batch_type,p_batch_status,p_user_id,'','','',p_batch_id);
	
END gm_sav_batch;

PROCEDURE gm_sav_batch (
        p_batch_type IN T9600_BATCH.c901_batch_type%TYPE,
        p_batch_status IN T9600_BATCH.c901_batch_status%TYPE,
        p_user_id    IN T9600_BATCH.c9600_created_by%TYPE,
        p_inv_close_dt IN T9600_BATCH.C901_INVOICE_CLOSING_DATE%TYPE,
        p_dt_issue_from IN T9600_BATCH.C9600_DATE_OF_ISSUE_FROM%TYPE,
        p_dt_issue_to IN T9600_BATCH.C9600_DATE_OF_ISSUE_TO%TYPE,
        p_batch_id IN OUT T9600_BATCH.c9600_batch_id%TYPE)
AS
 v_company_id        t1900_company.c1900_company_id%TYPE;
 
BEGIN

	 SELECT get_compid_frm_cntx() 
	   INTO v_company_id  
	   FROM dual;
	
	 UPDATE t9600_batch
	    SET c901_batch_status  = p_batch_status
	        , c9600_last_update_by = p_user_id
	        , c9600_last_updated_date = CURRENT_DATE
      WHERE c9600_batch_id = p_batch_id
        AND c9600_void_fl IS NULL;
	--
	IF (SQL%ROWCOUNT                   = 0) THEN
     SELECT s9600_batch.nextval INTO p_batch_id FROM DUAL;
    -- 
     INSERT
       INTO t9600_batch
        (
            c9600_batch_id, c901_batch_type, c901_batch_status
          , c9600_created_by, c9600_created_date, c9600_initiated_by
          , c9600_initiated_date, c1900_company_id
          , c901_invoice_closing_date,c9600_date_of_issue_from,c9600_date_of_issue_to
        )
        VALUES
        (
            p_batch_id, p_batch_type, p_batch_status
          , p_user_id, CURRENT_DATE, p_user_id
          , CURRENT_DATE, v_company_id
          , p_inv_close_dt,p_dt_issue_from,p_dt_issue_to
        ) ;
	END IF;
END gm_sav_batch;
/******************************************************************
* Description : Procedure used to save the batch details
****************************************************************/
PROCEDURE gm_sav_batch_details
    (
        p_batch_id IN T9601_batch_details.c9600_batch_id%TYPE,
        p_refid    IN T9601_batch_details.c9601_ref_id%TYPE,
        p_ref_type IN T9601_batch_details.c901_ref_type%TYPE,
        p_user_id  IN T9601_batch_details.c9601_created_by%TYPE
    )
AS
    v_batch_dtls_id t9601_batch_details.c9601_batch_detail_id%TYPE;
BEGIN
	--
	 UPDATE t9601_batch_details
	SET C9601_LAST_UPDATE_BY  = p_user_id
	 , C9601_LAST_UPDATED_DATE = CURRENT_DATE
  WHERE c9600_batch_id = p_batch_id
  	AND c9601_ref_id = p_refid
  	AND c901_ref_type = p_ref_type
    AND c9601_void_fl IS NULL;
	--
	IF (SQL%ROWCOUNT                   = 0) THEN
	
     SELECT s9601_batch_details.nextval INTO v_batch_dtls_id FROM DUAL;
     INSERT
       INTO t9601_batch_details
        (
            c9601_batch_detail_id, c9600_batch_id, c9601_ref_id
          , c901_ref_type, c9601_created_by, c9601_created_date
        )
        VALUES
        (
            v_batch_dtls_id, p_batch_id, p_refid
          , p_ref_type, p_user_id, CURRENT_DATE
        ) ;
     END IF;   
END gm_sav_batch_details;
/******************************************************************
* Description : Procedure used to void the batch based on batch ID
****************************************************************/
PROCEDURE gm_void_batch
    (
        p_batchid IN t9600_batch.c9600_batch_id%TYPE,
        p_userid  IN t9600_batch.c9600_last_update_by%TYPE
    )
AS
    v_batch_cnt  NUMBER;
    v_batch_type T9600_BATCH.C901_BATCH_TYPE%TYPE;
    v_batch_status T9600_BATCH.C901_BATCH_STATUS%TYPE;
    v_company_id        t1900_company.c1900_company_id%TYPE;
	v_monthly_comp_id   t1900_company.c1900_company_id%TYPE;
	
	  	
BEGIN
    --
      SELECT COUNT (1)
   		INTO v_batch_cnt
   	FROM t9600_batch
  		WHERE c9600_batch_id = p_batchid
    	AND c9600_void_fl IS NULL;
    -- 
    IF 	v_batch_cnt = 0
    THEN
    	GM_RAISE_APPLICATION_ERROR('-20999','8','');
    END IF;	
    --
    SELECT c901_batch_type, c901_batch_status,c1900_company_id
	 INTO v_batch_type, v_batch_status, v_company_id
   FROM t9600_batch
  WHERE c9600_batch_id = p_batchid
    AND c9600_void_fl IS NULL FOR UPDATE;
    
     IF v_batch_status > 18742
    THEN
    	GM_RAISE_APPLICATION_ERROR('-20999','9','');
    END IF;
    
   -- to get monthly invoice companty id
	SELECT get_rule_value(v_company_id,'MONTHLY_INV_COMP') 
	   INTO v_monthly_comp_id 
	FROM DUAL;

	  	
    UPDATE t9601_batch_details
SET c9601_void_fl      = 'Y', c9601_last_update_by = p_userid, c9601_last_updated_date = CURRENT_DATE
  WHERE c9600_batch_id = p_batchid
    AND c9601_void_fl IS NULL;
     UPDATE t9600_batch
    SET c9600_void_fl      = 'Y', c9600_last_update_by = p_userid, c9600_last_updated_date = CURRENT_DATE
      WHERE c9600_batch_id = p_batchid AND c9600_void_fl IS NULL;
      
-- clear PO only for voided batch orders					
	IF v_company_id = v_monthly_comp_id
	THEN 	

-- Update the customer PO as null which was generated by system for the Monthly Invoice  				
			UPDATE T501_Order
				SET C501_Customer_Po     = '',
				  C501_Last_Updated_By   = p_userid,
				  C501_Last_Updated_Date = CURRENT_DATE
				WHERE C501_order_id  IN 
						(SELECT C9601_REF_ID FROM T9601_BATCH_DETAILS  
						 WHERE C9600_BATCH_ID = p_batchid)
				AND c1900_company_id=v_company_id;		

											    
	END IF;
         --
END gm_void_batch;
/******************************************************************
* Description : Procedure used to void the batch details
****************************************************************/
PROCEDURE gm_void_batch_details (
        p_refid    IN t9601_batch_details.c9601_ref_id%TYPE,
        p_ref_type IN T9601_batch_details.c901_ref_type%TYPE,
        p_userid   IN t9600_batch.c9600_last_update_by%TYPE)
AS
    v_cnt NUMBER;
    v_batch_id T9600_BATCH.C9600_BATCH_ID%TYPE;
    v_cust_po t501_order.c501_customer_po%TYPE;
    v_company_id        t1900_company.c1900_company_id%TYPE;
	v_monthly_comp_id   t1900_company.c1900_company_id%TYPE;
BEGIN

    -- same package used to (Edit po also , so remove the validaion here)
    -- validate the order ids
     SELECT COUNT (1)
       INTO v_cnt
       FROM t9601_batch_details
      WHERE c9601_ref_id   = p_refid
        AND c901_ref_type  = p_ref_type
        AND c9601_void_fl IS NULL;
       -- 
    IF v_cnt               = 0 THEN
     -- to get the PO #
    	SELECT c501_customer_po INTO v_cust_po
   			FROM t501_order
  		WHERE c501_order_id   = p_refid
    		AND c501_void_fl   IS NULL
    		AND c501_delete_fl IS NULL;
    		GM_RAISE_APPLICATION_ERROR('-20999','141',v_cust_po);
    END IF;
    
     UPDATE t9601_batch_details
    SET c9601_void_fl      = 'Y', c9601_last_update_by = p_userid, c9601_last_updated_date = CURRENT_DATE
      WHERE c901_ref_type  = p_ref_type
        AND c9601_ref_id   = p_refid
        AND c9601_void_fl IS NULL;
  	--getting the company id
	BEGIN	
		SELECT C1900_COMPANY_ID 
		into v_company_id
		FROM T501_ORDER 
		WHERE C501_ORDER_ID=p_refid
		AND C501_VOID_FL IS NULL;
		
		-- to get monthly invoice companty id
		SELECT get_rule_value(v_company_id,'MONTHLY_INV_COMP') 
		   INTO v_monthly_comp_id 
		FROM DUAL;
		     
 -- clear PO only for voided batch orders					
	IF v_company_id = v_monthly_comp_id
	THEN 	

-- Update the customer PO as null which was generated by system for the Monthly Invoice  				
			UPDATE T501_Order
				SET C501_Customer_Po     = '',
				  C501_Last_Updated_By   = p_userid,
				  C501_Last_Updated_Date = CURRENT_DATE
				WHERE C501_order_id   = p_refid and c1900_company_id=v_company_id;		
   
											    
	END IF;
	EXCEPTION WHEN NO_DATA_FOUND THEN
		 v_company_id := NULL;
	END; 
    --
END gm_void_batch_details;

 /******************************************************************
* Description : Procedure used to void the batch details (Unlink)
****************************************************************/
PROCEDURE gm_void_batch_common (
        p_txn_id       IN VARCHAR2,
        p_comments     IN t907_cancel_log.c907_comments%TYPE,
        p_cancelreason IN t907_cancel_log.c901_cancel_cd%TYPE,
        p_canceltype   IN t901_code_lookup.c902_code_nm_alt%TYPE,
        p_userid       IN t2651_field_sales_details.c2651_last_updated_by%TYPE,
        p_out_msg OUT VARCHAR2)
AS
    v_canceltypecode t907_cancel_log.c901_type%TYPE;
    v_rule_value  VARCHAR2 (2000) ;
    v_exec_string VARCHAR2 (3000) ;
BEGIN
    -- to get the code id from type
     SELECT c901_code_id
       INTO v_canceltypecode
       FROM t901_code_lookup
      WHERE c902_code_nm_alt = p_canceltype;
    -- Dynamic Code - based on code id get the unlink procedure name.
    -- example: code id -18801 (Unlink PO Batch) rule value - gm_pkg_ac_ar_batch_trans.gm_sav_unlink_po
     SELECT get_rule_value (v_canceltypecode, 'UNLINKBATCH')
       INTO v_rule_value
       FROM DUAL;
    IF v_rule_value IS NULL THEN
    	GM_RAISE_APPLICATION_ERROR('-20999','11',v_canceltypecode);
    END IF;
    
    v_exec_string := 'BEGIN ' || v_rule_value ||
    '(:p_txn_id,:p_comments,:p_cancelreason,:p_canceltype,:p_userid,:p_custom_msg); END;';
    
    EXECUTE IMMEDIATE v_exec_string USING p_txn_id,
    p_comments,
    p_cancelreason,
    p_canceltype,
    p_userid,
    OUT p_out_msg;
END gm_void_batch_common;
/******************************************************************
* Description : Procedure used to save the batch status
****************************************************************/
PROCEDURE gm_sav_batch_status (
        p_batch_status IN T9600_BATCH.c901_batch_status%TYPE,
        p_batch_id     IN T9600_BATCH.c9600_batch_id%TYPE,
        p_userid       IN t9602_batch_log.C9602_CREATED_BY%TYPE)
AS
BEGIN
     UPDATE t9600_batch
    SET c901_batch_status  = p_batch_status, c9600_last_update_by = p_userid, c9600_last_updated_date = CURRENT_DATE
      WHERE c9600_batch_id = p_batch_id
        AND c9600_void_fl IS NULL;
END gm_sav_batch_status;
/******************************************************************
* Description : Procedure used to save the batch log
****************************************************************/
PROCEDURE gm_sav_batch_log (
        p_batch_id    IN t9602_batch_log.c9600_batch_id%TYPE,
        p_inv_id      IN t9602_batch_log.C9602_REF_ID%TYPE,
        p_action_type IN t9602_batch_log.C901_ACTION_TYPE%TYPE,
        p_ref_type    IN t9602_batch_log.C901_REF_TYPE%TYPE,
        p_userid      IN t9602_batch_log.C9602_CREATED_BY%TYPE)
AS
 v_company_id        t1900_company.c1900_company_id%TYPE;
 
BEGIN
    
     SELECT get_compid_frm_cntx() 
	   INTO v_company_id  
	   FROM dual;
	
	UPDATE t9602_batch_log
       SET C9602_LAST_UPDATED_BY = p_userid, C9602_LAST_UPDATED_DATE = CURRENT_DATE
     WHERE C9600_BATCH_ID    = p_batch_id
       AND C9602_REF_ID      = p_inv_id
       AND C901_ACTION_TYPE  = p_action_type;
   
       IF (SQL%ROWCOUNT          = 0) THEN
         INSERT
           INTO t9602_batch_log
            (
                C9602_BATCH_LOG_ID, C9600_BATCH_ID, C901_REF_TYPE, C9602_REF_ID
              , C901_ACTION_TYPE, C9602_CREATED_BY, C9602_CREATED_DATE, c1900_company_id
            )
            VALUES
            (
                s9602_batch_log.NEXTVAL, p_batch_id, p_ref_type, p_inv_id
              , p_action_type, p_userid, CURRENT_DATE, v_company_id
            ) ; -- 18751 Invoice Batch
    END IF;
END gm_sav_batch_log;
/******************************************************************
* Description : Procedure used to fetch the batch details information
*******************************************************************/
PROCEDURE gm_fch_batch_dtls (
        p_batch_id IN T9600_BATCH.C9600_BATCH_ID%TYPE,
        p_batch_type   IN T9600_BATCH.C901_BATCH_TYPE%TYPE,
        p_batch_sts    IN T9600_BATCH.C901_BATCH_STATUS%TYPE,
        p_out_cur OUT TYPES.cursor_type)
AS
 v_company_id        t1900_company.c1900_company_id%TYPE;
 
BEGIN
	
	 SELECT get_compid_frm_cntx() 
	   INTO v_company_id  
	   FROM dual;
	   
    OPEN p_out_cur FOR 
    
    SELECT t9601.c9601_batch_detail_id batch_dt_id, t9601.c9600_batch_id batchID
          , t9601.c9601_ref_id refid, t9601.c901_ref_type reftype
          , get_code_name (t9601.c901_ref_type) reftypenm
          , get_user_name (t9601.c9601_created_by) crby, t9601.c9601_created_date crdt
          , get_user_name (t9601.c9601_last_update_by) upby
          , t9601.c9601_last_updated_date updt, t9601.c9601_ref_id INVID
          , get_account_name(t501.c704_account_id) aname
     FROM t9601_batch_details t9601, t9600_batch t9600, t501_order t501
    WHERE t9600.c9600_batch_id    = p_batch_id 
      AND t9600.c9600_batch_id = t9601.c9600_batch_id 
      AND t9600.c901_batch_type = NVL(p_batch_type, t9600.c901_batch_type)
      AND t9600.c901_batch_status = NVL(p_batch_sts, t9600.c901_batch_status)
      AND t9601.c9601_void_fl IS NULL 
      AND t9600.c9600_void_fl IS NULL
      AND t9600.C1900_COMPANY_ID = v_company_id
	  AND t9601.c9601_ref_id = t501.c501_order_id
      AND t501.c501_void_fl is NULL
 ORDER BY aname;
 
END gm_fch_batch_dtls;
 
/******************************************************************
    * Description : Procedure used to save the ous replenish batch
    ****************************************************************/
PROCEDURE gm_sav_ous_replenish_batch (
        p_inputstr IN CLOB,
        p_userid   IN T9600_BATCH.C9600_CREATED_BY%TYPE,
        p_batchid  OUT T9600_BATCH.C9600_BATCH_ID%TYPE)
AS
    v_batchid     T9600_BATCH.C9600_BATCH_ID%TYPE;
    v_batch_type  T9600_BATCH.C901_BATCH_TYPE%TYPE;
    v_strlen NUMBER  := NVL (LENGTH (p_inputstr), 0) ;
    v_string CLOB    := p_inputstr;
    v_substring   VARCHAR2 (4000) ;
    v_consign_id  T504_CONSIGNMENT.C504_CONSIGNMENT_ID%TYPE;
    v_batch_cnt   NUMBER;
BEGIN
	IF v_strlen <>0 
	THEN
	-- to create the new batch #
	gm_pkg_cm_batch.gm_sav_batch(1701,18741,p_userid,v_batchid);

	WHILE INSTR (v_string, '|') <> 0
    LOOP
        v_substring   := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
        v_string      := SUBSTR (v_string, INSTR (v_string, '|')    + 1) ;
        v_consign_id  := NULL;
        --
        v_consign_id  := v_substring;
        -- to call the existing procedure to insert the new record (t9601_batch_details) table.
            gm_pkg_cm_batch.gm_sav_batch_details (v_batchid, v_consign_id, 1701, p_userid) ;
     END LOOP;
        -- batch status updated so, released by and date to be update
       UPDATE t9600_batch
		SET c901_batch_status  = 18743
	 		, c9600_released_by = p_userid
	 		, c9600_released_date = CURRENT_DATE
	 		, c9600_last_update_by = p_userid
	 		, c9600_last_updated_date = CURRENT_DATE
  		WHERE c9600_batch_id = v_batchid
    		AND c9600_void_fl IS NULL;
    END IF; -- v_strlen
    --
    p_batchid :=  v_batchid;
END gm_sav_ous_replenish_batch;

/************************************************************************
* Description : Procedure used to fetch the CN batch details information
*************************************************************************/
PROCEDURE gm_fch_cn_batch_dtls (
        p_batch_id   IN  T9600_BATCH.C9600_BATCH_ID%TYPE,
        p_batch_type IN  T9600_BATCH.C901_BATCH_TYPE%TYPE,
        p_batch_sts  IN  T9600_BATCH.C901_BATCH_STATUS%TYPE,
        p_out_cur    OUT TYPES.cursor_type)
AS
BEGIN
    OPEN p_out_cur FOR SELECT t9601.c9601_batch_detail_id batch_dt_id, t9601.c9600_batch_id batchID
    , t9601.c9601_ref_id refid, t9601.c901_ref_type reftype, get_code_name (t9601.c901_ref_type) reftypenm
    , get_user_name (t9601.c9601_created_by) crby, t9601.c9601_created_date crdt
    , get_user_name (t9601.c9601_last_update_by) upby
    , t9601.c9601_last_updated_date updt, t9601.c9601_ref_id INVID
    , get_account_name(t504.c704_account_id) aname
   FROM t9601_batch_details t9601, t9600_batch t9600, T504_CONSIGNMENT t504
     WHERE t9600.c9600_batch_id    = p_batch_id
         AND t9600.c9600_batch_id = t9601.c9600_batch_id 
         AND t9600.c901_batch_type = NVL(p_batch_type, t9600.c901_batch_type)
         AND t9600.c901_batch_status = NVL(p_batch_sts, t9600.c901_batch_status)
         AND t9601.c9601_void_fl IS NULL 
         AND t9600.c9600_void_fl IS NULL
	     AND t9601.c9601_ref_id = t504.C504_CONSIGNMENT_ID
         AND t504.c504_void_fl IS NULL
    ORDER BY aname;
END gm_fch_cn_batch_dtls;


END gm_pkg_cm_batch;
/
