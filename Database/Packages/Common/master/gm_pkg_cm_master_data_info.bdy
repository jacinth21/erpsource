--@"C:\database\Packages\sales\gm_pkg_cm_master_data_info.bdy";
CREATE OR REPLACE
PACKAGE BODY gm_pkg_cm_master_data_info
IS
/******************************************************************************************************************
  * This Procedure is called from webservice which will fetch the Account Related Information,For Active Account ID. 
*******************************************************************************************************************/
	PROCEDURE gm_fch_accounts_info(
    	p_token   IN t101a_user_token.c101a_token_id%TYPE,
    	p_langid  IN NUMBER,
    	p_page_no IN NUMBER,
    	p_uuid    IN t9150_device_info.c9150_device_uuid%TYPE,
    	p_accounts_cur OUT TYPES.CURSOR_TYPE )
	AS
  		v_deviceid t9150_device_info.c9150_device_info_id%TYPE;
  		v_count     NUMBER;
  		v_page_no   NUMBER;
  		v_page_size NUMBER;
  		v_start     NUMBER;
  		v_end       NUMBER;
   	    v_last_rec	t9151_device_sync_dtl.C9151_LAST_SYNC_REC%TYPE;   		
	BEGIN
  		--get the device info id based on user token
  		v_deviceid := get_device_id(p_token, p_uuid);
  		--get the user party id from the token
  		v_page_no := p_page_no;
  		
  		-- get the paging details
  		gm_pkg_pdpc_prodcatrpt.gm_fch_paging_dtl('ACCOUNTS','PAGESIZE',v_page_no,v_start,v_end,v_page_size);
  		/*if updated groups available then add all groups into v_inlist and return count of v_inlist records*/
  		gm_pkg_pdpc_prodcatrpt.gm_fch_device_updates(v_deviceid,p_langid,4000519,v_page_no,v_count,v_last_rec); --4000519 - Accounts Device Sync Type
  		--If there is no data available for device then call below prc to add all groups.
  	IF v_count = 0 THEN
    		--insert published systems and systems which are part of rule group into my_temp_publish_sys temp table.
    		--gm_pkg_pdpc_prodcatrpt.gm_fch_publish_system(v_party_id);
   		 gm_pkg_cm_master_data_dtl.gm_fch_accts_detail_tosync;
  	END IF;
  	v_page_no := p_page_no;
  OPEN p_accounts_cur 
  FOR 
  	SELECT COUNT (1) OVER () pagesize, result_count totalsize, v_page_no pageno, rwnum, CEIL (result_count / v_page_size) totalpages , 
  		t704.* 
  		FROM(
  				SELECT t704.*,ROWNUM rwnum,COUNT (1) OVER () result_count, MAX(DECODE(t704.acctid,v_last_rec,ROWNUM,NULL)) OVER() LASTREC
  				FROM(
    					SELECT t704.c703_sales_rep_id repid,t704.c704_account_id acctid,t704.c704_account_nm acctname,
      						   t704.c901_company_id compid,t704.c704_bill_add1 billaddr1,t704.c704_active_fl actvfl,
      						   t704.c704_bill_add2 billaddr2,t704.c704_bill_city billcity ,t704.c704_bill_country billcountry,
      						   t704.c704_bill_name billname,t704.c901_account_type acctype,t704.c901_ext_country_id extcountryid,
      						   t704.c704_ship_name shipname,t704.c704_ship_state shipstate,t704.c704_ship_zip_code shipzipcode,
      						   NVL(temp.void_fl,t704.c704_void_fl) voidfl,t704.c704_ship_country shipcountry,t704.c704_bill_state billstate,
      						   t704.c704_bill_zip_code billzipcode,t704.c704_ship_add1 shipaddr1,t704.c704_ship_add2 shipaddr2,
      						   t704.c704_ship_city shipcity,t704.c101_dealer_id dealid
    					FROM T704_Account t704 ,my_temp_prod_cat_info temp
    					WHERE t704.c704_account_id = temp.ref_id
    					ORDER BY t704.c704_account_id
    				)t704
  			)t704 
  		WHERE rwnum BETWEEN NVL(LASTREC+1,DECODE(v_last_rec,NULL,v_start,v_start-1)) AND v_end;
	END gm_fch_accounts_info;
/*********************************************************************************************************************
* This Procedure is called from webservice which will fetch the Sales Rep Related Information,For Active Sales Rep ID. 
**********************************************************************************************************************/
	PROCEDURE gm_fch_salesrep_info(
    	p_token   IN t101a_user_token.c101a_token_id%TYPE,
    	p_langid  IN NUMBER,
    	p_page_no IN NUMBER,
    	p_uuid    IN t9150_device_info.c9150_device_uuid%TYPE,
    	p_salesrep_cur OUT TYPES.CURSOR_TYPE )
	AS
  		v_deviceid t9150_device_info.c9150_device_info_id%TYPE;
  		v_count     NUMBER;
  		v_page_no   NUMBER;
  		v_page_size NUMBER;
  		v_start     NUMBER;
  		v_end       NUMBER;
   	    v_last_rec	t9151_device_sync_dtl.C9151_LAST_SYNC_REC%TYPE;   		
	BEGIN
  		--get the device info id based on user token
  		v_deviceid := get_device_id(p_token, p_uuid);
 		 --get the user party id from the token
  		v_page_no := p_page_no;
  		
  		-- get the paging details
  		gm_pkg_pdpc_prodcatrpt.gm_fch_paging_dtl('SALESREP','PAGESIZE',v_page_no,v_start,v_end,v_page_size);
  		/*if updated groups available then add all groups into v_inlist and return count of v_inlist records*/
  		gm_pkg_pdpc_prodcatrpt.gm_fch_device_updates(v_deviceid,p_langid,4000520,v_page_no,v_count,v_last_rec); --Sales Rep -Device Sync Type
  		--If there is no data available for device then call below prc to add all groups.
  	IF v_count = 0 THEN
    		--insert published systems and systems which are part of rule group into my_temp_publish_sys temp table.
    		--gm_pkg_pdpc_prodcatrpt.gm_fch_publish_system(v_party_id);
    		gm_pkg_cm_master_data_dtl.gm_fch_salesrep_detail_tosync;
  	END IF;
  	v_page_no := p_page_no;
  	OPEN p_salesrep_cur 
  	FOR SELECT COUNT (1) OVER () pagesize, result_count totalsize, v_page_no pageno, rwnum, CEIL (result_count / v_page_size) totalpages , 
  		t703.* 
  			FROM(
  					SELECT t703.*, ROWNUM rwnum,COUNT (1) OVER () result_count, MAX(DECODE(t703.repid,v_last_rec,ROWNUM,NULL)) OVER() LASTREC
  					FROM (
  							SELECT t703.c101_party_id partyid,t703.c701_distributor_id did,t703.c702_territory_id territoryid,
  							t703.c703_active_fl actvfl,t703.c703_sales_rep_id repid,t703.c703_sales_rep_name repname,
  							t703.c901_ext_country_id extcountryid,t703.c703_void_fl voidfl,
  							get_distributor_name (t703.c701_distributor_id) dname, t703.c901_designation desgn
  							FROM t703_sales_rep t703 ,my_temp_prod_cat_info temp
    						WHERE t703.C703_SALES_REP_ID= temp.ref_id
    						ORDER BY t703.c703_sales_rep_id
    					)t703
  		)t703 WHERE rwnum BETWEEN NVL(LASTREC+1,DECODE(v_last_rec,NULL,v_start,v_start-1)) AND v_end;
	END gm_fch_salesrep_info;
/*********************************************************************************************************************
* * This Procedure is called from webservice which will fetch the ASSales Rep Information. 
**********************************************************************************************************************/
	PROCEDURE gm_fch_assocrep_info(
    	p_token   IN t101a_user_token.c101a_token_id%TYPE,
    	p_langid  IN NUMBER,
    	p_page_no IN NUMBER,
    	p_uuid    IN t9150_device_info.c9150_device_uuid%TYPE,
    	p_assocrep_cur OUT TYPES.CURSOR_TYPE )
	AS
  		v_deviceid t9150_device_info.c9150_device_info_id%TYPE;
  		v_count     NUMBER;
  		v_page_no   NUMBER;
  		v_page_size NUMBER;
  		v_start     NUMBER;
  		v_end       NUMBER;
   	    v_last_rec	t9151_device_sync_dtl.C9151_LAST_SYNC_REC%TYPE;    		
	BEGIN
  		--get the device info id based on user token
  		v_deviceid := get_device_id(p_token, p_uuid);
 		 --get the user party id from the token
  		v_page_no := p_page_no;
  		
  		-- get the paging details
  		gm_pkg_pdpc_prodcatrpt.gm_fch_paging_dtl('ASSALESREP','PAGESIZE',v_page_no,v_start,v_end,v_page_size);
  		/*if updated groups available then add all groups into v_inlist and return count of v_inlist records*/
  		gm_pkg_pdpc_prodcatrpt.gm_fch_device_updates(v_deviceid,p_langid,4000629,v_page_no,v_count, v_last_rec); --Sales Rep -Device Sync Type
  		--If there is no data available for device then call below prc to add all groups.
  	
  		IF v_count = 0 THEN
    		--insert published systems and systems which are part of rule group into my_temp_publish_sys temp table.
    		--gm_pkg_pdpc_prodcatrpt.gm_fch_publish_system(v_party_id);
    		gm_pkg_cm_master_data_dtl.gm_fch_assocrep_detail_tosync;
  	END IF;
  
  	v_page_no := p_page_no;
  	OPEN p_assocrep_cur 
  	FOR SELECT COUNT (1) OVER () pagesize, result_count totalsize, v_page_no pageno, rwnum, CEIL (result_count / v_page_size) totalpages , 
  		t704a.* 
  			FROM(
  					SELECT t704a.*, ROWNUM rwnum,COUNT (1) OVER () result_count, MAX(DECODE(t704a.accasrepmapid,v_last_rec,ROWNUM,NULL)) OVER() LASTREC
  					FROM (  							
						  SELECT t704a.C704A_ACCOUNT_REP_MAP_ID accasrepmapid, t704a.C704_ACCOUNT_ID acctid, t704a.C703_SALES_REP_ID repid
						       , t704a.c704a_void_fl voidfl,  t704a.c704a_active_fl activefl
						    FROM t704a_account_rep_mapping t704a, my_temp_prod_cat_info temp
						   WHERE t704a.C704A_ACCOUNT_REP_MAP_ID= temp.ref_id
						   ORDER BY t704a.C704A_ACCOUNT_REP_MAP_ID
    					)t704a
  		)t704a WHERE rwnum BETWEEN NVL(LASTREC+1,DECODE(v_last_rec,NULL,v_start,v_start-1)) AND v_end;
	END gm_fch_assocrep_info;
/**************************************************************************************************************************
*  This Procedure is called from webservice which will fetch the Distributor Related Information,For Active Distributor ID. 
***************************************************************************************************************************/
	PROCEDURE gm_fch_distributor_info(
    	p_token   IN t101a_user_token.c101a_token_id%TYPE,
    	p_langid  IN NUMBER,
    	p_page_no IN NUMBER,
    	p_uuid    IN t9150_device_info.c9150_device_uuid%TYPE,
    	p_distributor_cur OUT TYPES.CURSOR_TYPE	)
	AS
  		v_deviceid t9150_device_info.c9150_device_info_id%TYPE;
  		v_count     NUMBER;
  		v_page_no   NUMBER;
  		v_page_size NUMBER;
  		v_start     NUMBER;
  		v_end       NUMBER;
   	    v_last_rec	t9151_device_sync_dtl.C9151_LAST_SYNC_REC%TYPE;    		
	BEGIN
  		--get the device info id based on user token
  		v_deviceid := get_device_id(p_token, p_uuid);
  		--get the user party id from the token
  		v_page_no := p_page_no;
  		
  		gm_pkg_pdpc_prodcatrpt.gm_fch_paging_dtl('DISTRIBUTORS','PAGESIZE',v_page_no,v_start,v_end,v_page_size);
  		/*if updated groups available then add all groups into v_inlist and return count of v_inlist records*/
  		gm_pkg_pdpc_prodcatrpt.gm_fch_device_updates(v_deviceid,p_langid,4000521,v_page_no,v_count,v_last_rec); --4000521 - Distributor Device Sync Type
  		--If there is no data available for device then call below prc to add all groups.
  		IF v_count = 0 THEN
    		--insert published systems and systems which are part of rule group into my_temp_publish_sys temp table.
    		--gm_pkg_pdpc_prodcatrpt.gm_fch_publish_system(v_party_id);
    		gm_pkg_cm_master_data_dtl.gm_fch_dist_detail_tosync;
  		END IF;
  		v_page_no := p_page_no;
  		
  		OPEN p_distributor_cur 
  		FOR 
  		SELECT COUNT (1) OVER () pagesize, result_count totalsize, v_page_no pageno, rwnum, CEIL (result_count / v_page_size) totalpages , 
  			t701.* FROM(
  						SELECT t701.*,ROWNUM rwnum,COUNT (1) OVER () result_count, MAX(DECODE(t701.did,v_last_rec,ROWNUM,NULL)) OVER() LASTREC
  						FROM(
      							SELECT t701.C701_REGION regionid, t701.C701_DISTRIBUTOR_ID did,t701.c701_void_fl voidfl,t701.c701_ship_add1 shipaddr1,
      							t701.c701_ship_add2 shipaddr2,t701.c701_ship_city shipcity,t701.c701_ship_country shipcountry,
      							t701.c701_ship_state shipstate,t701.c701_ship_zip_code shipzipcode,t701.c701_distributor_name dname,
      							t701.c901_ext_country_id extcountryid,t701.c701_ship_name shipname,t701.c701_active_fl actvfl,
      							t701.c701_bill_add1 billaddr1,t701.c701_bill_add2 billaddr2,t701.c701_bill_city billcity,
      							t701.c701_bill_country billcountry,t701.c701_bill_name billname,t701.c701_bill_state billstate,
      							t701.c701_bill_zip_code billzipcode
      							FROM t701_distributor t701,my_temp_prod_cat_info temp
    							WHERE t701.c701_distributor_id= temp.ref_id
    							ORDER BY t701.c701_distributor_id 
    						)t701
  						)t701 WHERE rwnum BETWEEN NVL(LASTREC+1,DECODE(v_last_rec,NULL,v_start,v_start-1)) AND v_end;
	END gm_fch_distributor_info;
/*****************************************************************************************************************************
*  This Procedure is called from webservice which will fetch the Account Gpo Mapping Related Information,For Active Account ID.  
*******************************************************************************************************************************/
	PROCEDURE gm_fch_acctgpomap_info(
    	p_token   IN t101a_user_token.c101a_token_id%TYPE,
    	p_langid  IN NUMBER,
    	p_page_no IN NUMBER,
    	p_uuid    IN t9150_device_info.c9150_device_uuid%TYPE,
    	p_acctgpomap_cur OUT TYPES.CURSOR_TYPE )
	AS
  		v_deviceid t9150_device_info.c9150_device_info_id%TYPE;
  		v_count     NUMBER;
  		v_page_no   NUMBER;
  		v_page_size NUMBER;
  		v_start     NUMBER;
  		v_end       NUMBER;
   	    v_last_rec	t9151_device_sync_dtl.C9151_LAST_SYNC_REC%TYPE;    		
	BEGIN
  		--get the device info id based on user token
  		v_deviceid := get_device_id(p_token, p_uuid);
  		--get the user party id from the token
  		v_page_no := p_page_no;
  		
  		-- get the paging details
  		gm_pkg_pdpc_prodcatrpt.gm_fch_paging_dtl('ACCTGPOMAP','PAGESIZE',v_page_no,v_start,v_end,v_page_size);
  		/*if updated groups available then add all groups into v_inlist and return count of v_inlist records*/
  		gm_pkg_pdpc_prodcatrpt.gm_fch_device_updates(v_deviceid,p_langid,4000530,v_page_no,v_count,v_last_rec); --4000530 - Account Gpo Map Device Sync Type
  		--If there is no data available for device then call below prc to add all groups.
  	IF v_count = 0 THEN
   		 	--insert published systems and systems which are part of rule group into my_temp_publish_sys temp table.
    		--gm_pkg_pdpc_prodcatrpt.gm_fch_publish_system(v_party_id);
    	gm_pkg_cm_master_data_dtl.gm_fch_acctgpomap_tosync;
  	END IF;
  	v_page_no := p_page_no;
  OPEN p_acctgpomap_cur 
  FOR 
  	SELECT COUNT (1) OVER () pagesize, result_count totalsize, v_page_no pageno, rwnum, CEIL (result_count / v_page_size) totalpages , 
  		t740.* FROM(
  						SELECT t740.*,ROWNUM rwnum,COUNT (1) OVER () result_count, MAX(DECODE(t740.acctgpomapid,v_last_rec,ROWNUM,NULL)) OVER() LASTREC
  						FROM(
    							SELECT t740.c101_party_id partyid,t740.c704_account_id acctid,t740.c740_gpo_account_map_id acctgpomapid,
    							t740.c740_void_fl voidfl,t740.c740_gpo_catalog_id gpocatalogid
    							FROM t740_gpo_account_mapping t740 ,my_temp_prod_cat_info temp
    							WHERE t740.c740_gpo_account_map_id= temp.ref_id
    							ORDER BY c740_gpo_account_map_id
    						)t740
  					)t740 WHERE rwnum BETWEEN NVL(LASTREC+1,DECODE(v_last_rec,NULL,v_start,v_start-1)) AND v_end;
	END gm_fch_acctgpomap_info;
/*****************************************************************************************************************************
*  This Procedure is called from webservice which will fetch the User Related Information,For Active User ID.  
*******************************************************************************************************************************/
	PROCEDURE gm_fch_user_info(
    	p_token   IN t101a_user_token.c101a_token_id%TYPE,
    	p_langid  IN NUMBER,
    	p_page_no IN NUMBER,
    	p_uuid    IN t9150_device_info.c9150_device_uuid%TYPE,
    	p_user_cur OUT TYPES.CURSOR_TYPE )
	AS
  		v_deviceid t9150_device_info.c9150_device_info_id%TYPE;
  		v_count     NUMBER;
  		v_page_no   NUMBER;
  		v_page_size NUMBER;
  		v_start     NUMBER;
  		v_end       NUMBER;
   	    v_last_rec	t9151_device_sync_dtl.C9151_LAST_SYNC_REC%TYPE;    		
	BEGIN
  		--get the device info id based on user token
  		v_deviceid := get_device_id(p_token, p_uuid);
  
  		v_page_no := p_page_no;
  		
  		-- get the paging details
  		gm_pkg_pdpc_prodcatrpt.gm_fch_paging_dtl('USER','PAGESIZE',v_page_no,v_start,v_end,v_page_size);
  		/*if updated User available then add all Users into v_inlist and return count of v_inlist records*/
  		gm_pkg_pdpc_prodcatrpt.gm_fch_device_updates(v_deviceid,p_langid,4000535,v_page_no,v_count,v_last_rec); --4000535 - User Device Sync Type
  		--If there is no data available for device then call below prc to add all groups.
	  	IF v_count = 0 THEN
	    	gm_pkg_cm_master_data_dtl.gm_fch_user_tosync;
	  	END IF;
	  	
	  	OPEN p_user_cur 
	 	FOR 
	  	SELECT COUNT (1) OVER () pagesize, result_count totalsize, v_page_no pageno, rwnum, CEIL (result_count / v_page_size) totalpages , 
	  		t101.* FROM(
	  						SELECT t101.*, ROWNUM rwnum,COUNT (1) OVER () result_count, MAX(DECODE(T101.userid,v_last_rec,ROWNUM,NULL)) OVER() LASTREC
	  						FROM(
								 SELECT c101_user_id userid, c101_user_f_name fname, c101_user_l_name lname
								  , c101_user_sh_name shname, c101_email_id emailid, c901_dept_id deptid
								  , c101_access_level_id accesslvl, c701_distributor_id distid, c101_party_id partyid
								  , c901_user_status status, c901_user_type type, c101_hire_dt hiredt
								  , c101_termination_date termtdt
								   FROM t101_user t101,my_temp_prod_cat_info temp
	    						  WHERE t101.c101_user_id = temp.ref_id
	    						  ORDER BY c101_user_id
	    						)t101
	  					)t101 WHERE rwnum BETWEEN NVL(LASTREC+1,DECODE(v_last_rec,NULL,v_start,v_start-1)) AND v_end;
	END gm_fch_user_info;	
END gm_pkg_cm_master_data_info;
/