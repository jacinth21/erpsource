--@"C:\database\Packages\sales\gm_pkg_cm_master_data_dtl.bdy";
-- Package  has been updated temp to only include GMNA Company Information   
CREATE OR REPLACE
PACKAGE BODY gm_pkg_cm_master_data_dtl
IS
    
/******************************************************************************************************
  * This method will fetch the Active Account ID and with the Account id, Account Details are fetched.
********************************************************************************************************/
	PROCEDURE gm_fch_accts_detail_tosync
	AS 
	v_company_id   	t1900_company.c1900_company_id%TYPE; 
	BEGIN
	SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL')) INTO v_company_id FROM dual; 
		DELETE FROM my_temp_prod_cat_info;
		
			INSERT INTO my_temp_prod_cat_info(ref_id)
			SELECT t704.c704_account_id 
			FROM   t704_account t704 
			WHERE  t704.c704_void_fl IS NULL
            AND t704.c901_ext_country_id IS NULL  
			AND t704.c1900_company_id = v_company_id
            AND t704.c704_active_fl = 'Y' ;
			--AND t704.c901_account_type = 70110;		-- 70110 - Hospital - US
	END gm_fch_accts_detail_tosync;
/**********************************************************************************************************
  * This method will fetch the Active Sales Rep ID and with the Sales Rep id ,Sales Rep Details are fetched.
***********************************************************************************************************/
	PROCEDURE gm_fch_salesrep_detail_tosync
	AS
	v_company_id   	t1900_company.c1900_company_id%TYPE; 
	BEGIN
	SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL')) INTO v_company_id FROM dual; 
		DELETE FROM my_temp_prod_cat_info;
		
			INSERT INTO my_temp_prod_cat_info(ref_id)
			SELECT t703.c703_sales_rep_id
			FROM   t703_sales_rep t703
			WHERE  t703.c703_active_fl = 'Y'
		-- 	AND    t703.c901_ext_country_id IS NULL
            AND    t703.c1900_company_id = v_company_id
			AND    t703.c703_void_fl    IS NULL; 
	END gm_fch_salesrep_detail_tosync;
/**********************************************************************************************************
  * This Procedure will fetch the Active Associate Sales Rep ID's
***********************************************************************************************************/
	PROCEDURE gm_fch_assocrep_detail_tosync
	AS
	v_company_id   	t1900_company.c1900_company_id%TYPE; 
	BEGIN
	SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL')) INTO v_company_id FROM dual; 
		DELETE FROM my_temp_prod_cat_info;
		
			INSERT INTO my_temp_prod_cat_info(ref_id)			
			SELECT t704a.C704A_ACCOUNT_REP_MAP_ID
			  FROM t704a_account_rep_mapping t704a, t703_sales_rep t703
			 WHERE t703.c703_sales_rep_id = t704a.c703_sales_rep_id
			   AND t704a.c704a_void_fl    IS NULL
			   AND t703.c703_void_fl     IS NULL
			   AND t703.c901_ext_country_id IS NULL
               AND t703.c1900_company_id = v_company_id
			   AND t704a.c704a_active_fl='Y'; 
			   
	END gm_fch_assocrep_detail_tosync;
/****************************************************************************************************************
  * This method will fetch the Active Distributor ID and with the Distributor id ,Distributor Details are fetched.
*****************************************************************************************************************/
	PROCEDURE gm_fch_dist_detail_tosync
	AS 
	v_company_id   	t1900_company.c1900_company_id%TYPE; 
	BEGIN
	SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL')) INTO v_company_id FROM dual; 
		DELETE FROM my_temp_prod_cat_info;
		
			INSERT INTO my_temp_prod_cat_info(ref_id)
			SELECT t701.c701_distributor_id
			FROM   t701_distributor t701
			WHERE  t701.c701_void_fl IS NULL
			AND    t701.c901_ext_country_id IS NULL
            AND    t701.c1900_company_id = v_company_id
			AND    t701.c701_active_fl  = 'Y'; 
	END gm_fch_dist_detail_tosync;
/************************************************************************************************************************
  * This method will fetch the Active Gpo Account Map id and with the Gpo Account Map id, Gpo Mapping  Details are fetched.
***************************************************************************************************************************/
	PROCEDURE gm_fch_acctgpomap_tosync
	AS 
	v_company_id   	t1900_company.c1900_company_id%TYPE; 
	BEGIN
	SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL')) INTO v_company_id FROM dual; 
		DELETE FROM my_temp_prod_cat_info;
		
			INSERT INTO my_temp_prod_cat_info(ref_id)
			SELECT t740.c740_gpo_account_map_id
			FROM   t740_gpo_account_mapping t740,
 				   t704_account t704
			WHERE  t740.c704_account_id = t704.c704_account_id
			AND    t704.c704_void_fl     IS NULL
			AND    t740.c740_void_fl     IS NULL
            AND    t704.c1900_company_id = v_company_id
			AND    t704.c704_active_fl    = 'Y';
			--AND    t704.c901_account_type = 70110;  -- 70110 - Hospital - US
	END gm_fch_acctgpomap_tosync;
/***************************************************************************************************************************
  * This method will fetch the Active Address id and with the Address ID, Address  Details are fetched.
***************************************************************************************************************************/
	PROCEDURE gm_fch_address_detail_tosync
	AS 
	v_company_id   	t1900_company.c1900_company_id%TYPE; 
	BEGIN
	SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL')) INTO v_company_id FROM dual; 
		DELETE FROM my_temp_prod_cat_info;
		
			INSERT INTO my_temp_prod_cat_info(ref_id)
			SELECT DISTINCT c106_address_id
From
  (SELECT t106.c106_address_id 
  FROM t703_sales_rep t703 ,
    t106_address t106
  WHERE t703.c703_active_fl  = 'Y'
  AND t703.c101_party_id     = t106.c101_party_id
  AND t106.c106_void_fl     IS NULL
  AND t703.c703_void_fl     IS NULL
  AND T703.C1900_Company_Id  = v_company_id
  AND T106.C106_Inactive_Fl IS NULL
  ) 
Union 
  (SELECT T106.C106_Address_Id
  FROM T101_Party T101,
    T106_Address T106
  WHERE T101.C901_Party_Type = '7000'
  AND T101.C101_Void_Fl     IS NULL
  AND (t101.c101_active_fl is null or t101.c101_active_fl = 'Y')
  AND T106.C106_Void_Fl     IS NULL
  AND  t106.c101_party_id = t101.c101_party_id
  AND T106.C106_Inactive_Fl IS NULL
  )
  Union
  (
  SELECT t106.c106_address_id
	FROM t6630_activity t6630 ,
  	t106_address t106,
  	t6632_activity_attribute t6632
	WHERE t6632.c6630_activity_id   = t6630.c6630_activity_id
	AND t6632.c901_attribute_type   = '7777'
	AND t6632.c6632_attribute_value = t106.c106_address_id
	AND t6630.c6630_void_fl        IS NULL
	AND t6632.c6632_void_fl        IS NULL
	AND t106.c106_void_fl          IS NULL
	AND T106.C106_Inactive_Fl IS NULL
  )
  Union   
  (SELECT T106.C106_Address_Id  
  FROM T101_Party T101,    
  T106_Address T106  
  WHERE T101.C901_Party_Type = '26230725'  
  AND T101.C101_Void_Fl  IS NULL 
  AND  t101.c101_active_fl = 'Y'  
  AND T106.C106_Void_Fl  IS NULL  
  AND  t106.c101_party_id = t101.c101_party_id  
  AND t101.c1900_company_id = v_company_id  
  AND T106.C106_Inactive_Fl IS NULL  
  )
  ;

	END gm_fch_address_detail_tosync;
/***************************************************************************************************************************
  * This method will fetch the Active  User id and with the User id, User  Details are fetched.
***************************************************************************************************************************/
	PROCEDURE gm_fch_user_tosync
	AS 
	v_company_id   	t1900_company.c1900_company_id%TYPE; 
	BEGIN
	SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL')) INTO v_company_id FROM dual; 
		DELETE FROM my_temp_prod_cat_info;
		
			INSERT INTO my_temp_prod_cat_info(ref_id)
			 SELECT u101.c101_user_id userid
			   FROM t101_user u101, t101_party p101
			  WHERE u101.c901_dept_id IN
				      (SELECT c906_rule_value
				      FROM t906_rules
				      WHERE c906_rule_id   = 'USER_DEPT'
				      AND c906_rule_grp_id ='PROD_CAT_SYNC'
				      AND c906_void_fl is null) -- '2005' --Sales
			    AND u101.c901_user_status IN (SELECT c906_rule_value
				      FROM t906_rules
				      WHERE c906_rule_id   = 'USER_STATUS'
				      AND c906_rule_grp_id ='PROD_CAT_SYNC'
				      AND c906_void_fl is null) --311 -Active
				      AND u101.C101_PARTY_ID  = p101.C101_PARTY_ID 
                      AND p101.C1900_COMPANY_ID = v_company_id;
	END gm_fch_user_tosync;	
	

END gm_pkg_cm_master_data_dtl;
/
