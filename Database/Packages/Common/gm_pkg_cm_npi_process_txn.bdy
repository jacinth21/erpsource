/* Formatted on 2010/10/08 19:37 (Formatter Plus v4.8.0) */
--	

CREATE OR REPLACE PACKAGE BODY gm_pkg_cm_npi_process_txn
IS

	/************************************************************************************
    * Description : Procedure to save the NPI details of the transaction in t6640 table
    *************************************************************************************/
	PROCEDURE gm_sav_npi_transaction(
		p_npi_tran_id 		IN OUT  t6640_npi_transaction.c6640_id%TYPE,
		p_ref_id      		IN      t6640_npi_transaction.c6640_ref_id %TYPE,
		p_account_id  		IN      t6640_npi_transaction.c704_account_id%TYPE,
		p_surgeon_npi_id  	IN  	t6640_npi_transaction.c6600_surgeon_npi_id%TYPE,
		p_surgeon_name    	IN  	t6640_npi_transaction.c6640_surgeon_name%TYPE,
		p_tran_flag       	IN  	t6640_npi_transaction.c6640_transaction_fl%TYPE,
		p_userid          	IN  	t6640_npi_transaction.c6640_created_by%TYPE,
		p_submit_type	  	IN		VARCHAR2 DEFAULT NULL,
		p_status            IN      NUMBER DEFAULT 52027
	)
	AS
	v_company_id    	t1900_company.c1900_company_id%TYPE;
	v_plant_id 			t5040_plant_master.c5040_plant_id%TYPE;
	v_party_surgeon_id 	t6640_npi_transaction.c101_party_surgeon_id%TYPE;
	v_sales_rep_id 		t6640_npi_transaction.c703_sales_rep_id%TYPE;
	v_tran_flag 		t6640_npi_transaction.c6640_transaction_fl%TYPE;
	v_account_id		t6640_npi_transaction.c704_account_id%TYPE := p_account_id;
	v_first_nm			t101_party.c101_first_nm%TYPE;
	v_last_nm			t101_party.c101_last_nm%TYPE;
	v_surgeon_name    	t6640_npi_transaction.c6640_surgeon_name%TYPE;
	v_valid_fl      	t6640_npi_transaction.C6640_VALID_FL%TYPE;
	BEGIN
		SELECT get_compid_frm_cntx() , get_plantid_frm_cntx()
	  		INTO v_company_id , v_plant_id
	  	FROM dual;
	  	
	  	v_valid_fl := NULL;
   	  	IF v_company_id <> '1000'
 		THEN
   			v_valid_fl := 'Y';
  		END IF;

   	 	v_tran_flag := NVL(p_tran_flag,'N');
   		
   	  	BEGIN
		 SELECT t6600.c101_party_surgeon_id,
			  get_party_name(t6600.c101_party_surgeon_id)
			INTO v_party_surgeon_id,
			  v_surgeon_name
			FROM t6600_party_surgeon t6600,
			  t101_party t101
			WHERE t6600.c6600_surgeon_npi_id      = p_surgeon_npi_id
			AND t6600.c101_party_surgeon_id       = t101.c101_party_id
			AND NVL(t101.c101_active_fl,'Y')	  = 'Y'
			AND t6600.c6600_void_fl              IS NULL
			AND t101.c101_void_fl                IS NULL;
		  EXCEPTION
		  WHEN NO_DATA_FOUND THEN
			v_party_surgeon_id := NULL;
			v_surgeon_name     := NULL;
		END; 
   	  	
   	  	IF v_surgeon_name IS NULL
   	  	THEN
   	  		v_surgeon_name:= p_surgeon_name;
   	  	END IF;
   	  	
		BEGIN
   	  		SELECT c703_sales_rep_id
				INTO v_sales_rep_id
			FROM t704_account
			WHERE c704_account_id = TO_CHAR(p_account_id)
			AND c704_void_fl IS NULL;
	  	EXCEPTION WHEN NO_DATA_FOUND THEN
	  		BEGIN
	   	   		SELECT c704_account_id,
				  c703_sales_rep_id
				  INTO v_account_id, v_sales_rep_id 
				FROM t501_order
				WHERE c501_order_id = p_ref_id
				AND c501_void_fl   IS NULL;	
		  		EXCEPTION WHEN NO_DATA_FOUND THEN
		  			v_account_id := NULL;
		  			v_sales_rep_id := NULL;
		  	END;
   	  	END;   	  

   	  	IF v_party_surgeon_id IS NULL
   	  	THEN
   	  	BEGIN
   	  		SELECT c101_party_id
   	  		  INTO v_party_surgeon_id
   	  		  FROM V6600_PARTY_SURGEON   	  	
   	  		 WHERE TRIM(partynm) = TRIM(p_surgeon_name);
   	  	EXCEPTION WHEN OTHERS THEN
   	  		v_party_surgeon_id := NULL;
   	  		v_valid_fl := NULL;
   	  	END;
   	  	END IF;

   	  	-- If the npi transaction id(primary key) is passing from front end, update the value otherwise insert new record 
   	  	IF p_npi_tran_id IS NOT NULL THEN
	      UPDATE t6640_npi_transaction
	         SET c6640_ref_id = p_ref_id,
	             c6600_surgeon_npi_id = p_surgeon_npi_id,
	             c101_party_surgeon_id = v_party_surgeon_id,
	             c6640_surgeon_name = v_surgeon_name,
	             c6640_transaction_fl = v_tran_flag,
	             c704_account_id = v_account_id,
	             c703_sales_rep_id = v_sales_rep_id,
	             c6640_last_updated_by = p_userid,
	             c6640_last_updated_date = SYSDATE,
	             c6640_valid_fl = v_valid_fl
	       WHERE c6640_id = p_npi_tran_id;
		ELSE
          SELECT s6640_id.NEXTVAL
           INTO p_npi_tran_id
           FROM DUAL;

         	INSERT INTO t6640_npi_transaction
                     (c6640_id, c6640_ref_id, c6600_surgeon_npi_id,
                      c101_party_surgeon_id, c6640_surgeon_name,c6640_transaction_fl,
                      c704_account_id,c703_sales_rep_id,
                      c6640_created_by, c6640_created_date,
                      c1900_company_id, c5040_plant_id, c901_status,c6640_valid_fl
                     )
              VALUES (p_npi_tran_id, p_ref_id, p_surgeon_npi_id,
                      v_party_surgeon_id, v_surgeon_name,v_tran_flag,
                      v_account_id,v_sales_rep_id,
                      p_userid, SYSDATE
                      , v_company_id, v_plant_id, p_status,v_valid_fl
                     );
      	END IF;
      -- saving account npi mapping
      gm_pkg_cm_npi_process_txn.gm_sav_npi_account(v_account_id,v_sales_rep_id,p_surgeon_npi_id,v_party_surgeon_id,v_surgeon_name,p_userid);
	
      -- If the value is saving from Modify Order screen, need to update the transaction flag also 
      IF(p_submit_type = 'Modify' OR p_submit_type = 'NpiErrorEdit')
      THEN
      	-- TO UPDATE THE NPI TRANSACTION STATUS
      	gm_pkg_cm_npi_process_txn.gm_save_npi_tran_status(p_ref_id, p_userid, 'Y');
      END IF;
      
      -- If the NPI/Surgeon is adding from NPI Error Edit screen, then the record should be marked as valid 
      IF(p_submit_type = 'NpiErrorEdit')
      THEN
      	gm_pkg_cm_npi_process_txn.gm_sav_valid_npi(p_npi_tran_id,p_userid);
      END IF;
      
	END gm_sav_npi_transaction;
	
	/****************************************************************************************
    * Description : Procedure to save the NPI details of the transaction in t6645_npi_account
    *****************************************************************************************/
	PROCEDURE gm_sav_npi_account(	
		p_account_id  		IN     	t6640_npi_transaction.c704_account_id%TYPE,
		p_sales_rep_id 		IN 		t6640_npi_transaction.c703_sales_rep_id%TYPE,
		p_surgeon_npi_id  	IN  	t6640_npi_transaction.c6600_surgeon_npi_id%TYPE,
		p_party_surgeon_id 	IN 		t6640_npi_transaction.c101_party_surgeon_id%TYPE,
		p_surgeon_name    	IN  	t6640_npi_transaction.c6640_surgeon_name%TYPE,
		p_userid         	IN  	t6640_npi_transaction.c6640_created_by%TYPE,
		p_status            IN      NUMBER DEFAULT 52027
	)
	AS
		v_company_id    t1900_company.c1900_company_id%TYPE;	
		v_plant_id 		t5040_plant_master.c5040_plant_id%TYPE;
		v_count 		NUMBER;
		v_id 			NUMBER;
	BEGIN
		  SELECT get_compid_frm_cntx() , get_plantid_frm_cntx()
	  INTO v_company_id , v_plant_id
	  FROM dual;
  
   	  	BEGIN
   	  		SELECT COUNT(1) INTO v_count
				FROM t6645_npi_account
			WHERE C704_ACCOUNT_ID    = p_account_id
			AND C703_SALES_REP_ID    = p_sales_rep_id
			AND C1900_COMPANY_ID     = v_company_id
			AND C6645_VOID_FL       IS NULL
			AND (C6600_SURGEON_NPI_ID= p_surgeon_npi_id
				OR C101_PARTY_SURGEON_ID = p_party_surgeon_id
				OR C6645_SURGEON_NAME LIKE '%'''||p_surgeon_name||'''%');

	  	EXCEPTION WHEN NO_DATA_FOUND THEN
   	   		v_count := 0;
   	  	END;   	     	  
   	  --
   	  	IF v_count = 0 THEN
     
          	SELECT s6645_id.NEXTVAL
           	INTO v_id
           	FROM DUAL;

         	INSERT INTO t6645_npi_account
                     (c6645_id, c6600_surgeon_npi_id,
                      c101_party_surgeon_id, c6645_surgeon_name,
                      c704_account_id,c703_sales_rep_id,
                      c6645_created_by, c6645_created_date,
                      c1900_company_id, c5040_plant_id, c901_status
                     )
              VALUES (v_id, p_surgeon_npi_id,
                      p_party_surgeon_id, p_surgeon_name,
                      p_account_id,p_sales_rep_id,
                      p_userid, SYSDATE
                      , v_company_id, v_plant_id, p_status
                     );
      	END IF;
		
	END gm_sav_npi_account;
	
	/***********************************************************
    * Description : Procedure to save the NPI details from IPAD
    ************************************************************/
	PROCEDURE gm_sav_all_npi_transaction (
		p_ref_id      	 IN    t6640_npi_transaction.c6640_ref_id %TYPE,
	    p_npi_inputstr   IN	   CLOB,
	    p_account_id     IN    t6640_npi_transaction.c704_account_id%TYPE,
	    p_userid         IN    t6640_npi_transaction.c6640_created_by%TYPE
	  
	)
	AS	
		v_npi_tran_id    	t6640_npi_transaction.c6640_id%TYPE;
		v_surgeon_npi_id 	t6640_npi_transaction.c6600_surgeon_npi_id%TYPE;
		v_surgeon_name 		t6640_npi_transaction.c6640_surgeon_name%TYPE;
		v_string	   		CLOB := p_npi_inputstr;
		v_substring    		VARCHAR2 (4000);
		
	BEGIN
		IF (p_npi_inputstr IS NOT NULL)	 THEN
		
			WHILE INSTR (v_string, '|') <> 0 LOOP
				--
				v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
				v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
				--
				v_npi_tran_id := NULL;
				v_surgeon_npi_id 	:= NULL;
				v_surgeon_name	:= NULL;
				--
				v_surgeon_npi_id 	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_surgeon_name	:= v_substring;				
			
				gm_pkg_cm_npi_process_txn.gm_sav_npi_transaction(v_npi_tran_id,p_ref_id,p_account_id,v_surgeon_npi_id,v_surgeon_name,'Y',p_userid);
				
			END LOOP;
		
		END IF;
	END gm_sav_all_npi_transaction;
	
	/**************************************************************************************************
    * Description : Procedure to update the void flag of the transaction while removing from screen
    **************************************************************************************************/
	PROCEDURE gm_cancel_npi_transaction(	
		p_npi_tran_id    IN     t6640_npi_transaction.c6640_id%TYPE,
		p_userid         IN  	t6640_npi_transaction.c6640_created_by%TYPE
	)
	AS
		v_company_id    t1900_company.c1900_company_id%TYPE;
		v_plant_id 		t5040_plant_master.c5040_plant_id%TYPE;
	BEGIN
		  SELECT get_compid_frm_cntx() , get_plantid_frm_cntx()
	  INTO v_company_id , v_plant_id
	  FROM dual;   	  
   	  
   		--
   	  	IF p_npi_tran_id IS NOT NULL THEN
   	  	
      		UPDATE t6640_npi_transaction
         		SET c6640_void_fl = 'Y',
             		c6640_last_updated_by = p_userid,
             		c6640_last_updated_date = SYSDATE
       		WHERE c6640_id = p_npi_tran_id 
         	AND c6640_void_fl IS NULL 
         	AND c1900_company_id = v_company_id
         	AND c5040_plant_id = v_plant_id;	  
      	END IF;      
       		
	END gm_cancel_npi_transaction;
	
	/*******************************************************************************************
    * Description : Procedure to save the transaction flag to 'Y' once the transaction is saved
    *******************************************************************************************/
	PROCEDURE gm_save_npi_tran_status(	
		p_ref_id      IN        t6640_npi_transaction.c6640_ref_id %TYPE,
		p_userid      IN  		t6640_npi_transaction.c6640_created_by%TYPE,
		p_trans_flag  IN		VARCHAR2 DEFAULT NULL
	)
	AS
	v_company_id    t1900_company.c1900_company_id%TYPE;
	v_ref_id		t6640_npi_transaction.c6640_ref_id %TYPE := p_ref_id;
	v_plant_id 		t5040_plant_master.c5040_plant_id%TYPE;	
	BEGIN
		  SELECT get_compid_frm_cntx() , get_plantid_frm_cntx()
	  INTO v_company_id , v_plant_id
	  FROM dual;   	  

	  /*if the Procedure call is from modify order screen then no need to call the below section
	    */
	  IF (p_trans_flag != 'Y') THEN
	   		-- Get the ACK order id, if there any to update the transaction id to BBA order id
	   		BEGIN
		   	 	SELECT c501_order_id
		   	 		INTO v_ref_id
				FROM t501_order
				WHERE c501_order_id IN
				  (SELECT c501_ref_id
				  FROM t501_order
				  WHERE c501_order_id = p_ref_id
				  AND c501_void_fl   IS NULL
				  )
				AND c901_order_type = 101260 -- Ack Order
				AND c501_void_fl   IS NULL;
			EXCEPTION WHEN NO_DATA_FOUND THEN
		   	   	v_ref_id := NULL;
	   	  	END;
   		END IF;

   	  	-- If any ACK order id is there, update the order id to BBA order id
   	  	IF v_ref_id IS NOT NULL 
   	  	THEN
   	  		UPDATE t6640_npi_transaction
         		SET c6640_transaction_fl = 'Y',
         			c6640_ref_id = p_ref_id,
             		c6640_last_updated_by = p_userid,
             		c6640_last_updated_date = SYSDATE
       		WHERE c6640_ref_id = v_ref_id 
         	AND c6640_void_fl IS NULL 
         	AND c1900_company_id = v_company_id 
         	AND c5040_plant_id = v_plant_id;	
         	
   	  	ELSIF p_ref_id IS NOT NULL 
   	  	THEN
      		UPDATE t6640_npi_transaction
         		SET c6640_transaction_fl = 'Y',
             		c6640_last_updated_by = p_userid,
             		c6640_last_updated_date = SYSDATE
       		WHERE c6640_ref_id = p_ref_id 
         	AND c6640_void_fl IS NULL 
         	AND c1900_company_id = v_company_id 
         	AND c5040_plant_id = v_plant_id;	
      	END IF; 
       		
	END gm_save_npi_tran_status;

	/*******************************************************************************************
    * Description : Procedure to update the old transaction id to new transaction id 
    * 				if the order id is modified from New order screen
    *******************************************************************************************/
	PROCEDURE gm_upd_npi_tran_id(	
		p_old_ref_id      IN        t6640_npi_transaction.c6640_ref_id %TYPE,
		p_new_ref_id      IN  		t6640_npi_transaction.c6640_ref_id %TYPE
	)
	AS
	BEGIN
		
  		UPDATE t6640_npi_transaction
     		SET c6640_ref_id = p_new_ref_id
   		WHERE c6640_ref_id = p_old_ref_id 
     	AND c6640_void_fl IS NULL;	
       		
	END gm_upd_npi_tran_id;
	
	/**************************************************************************************************
    * Description : Procedure to validate and update the valid flag of the US NPI records saved for order
    ****************************************************************************************************/
	PROCEDURE gm_upd_npi_valid_data_us
	AS
	BEGIN
		
		DECLARE
		  v_cnt  NUMBER;
		  v_company_id	t1900_company.c1900_company_id%TYPE;
		  /* Get all the records which are not validated yet for the transaction
		   * where NPI and Surgeon details are available
		   */
		  CURSOR NPI_DTLS_CUR
		  IS
		    SELECT c6600_surgeon_npi_id npiid,
			  c101_party_surgeon_id partyid,
			  c6640_ref_id refid,
			  c6640_surgeon_name surgeonname
			FROM t6640_npi_transaction
			WHERE c6640_valid_fl      IS NULL
			AND c6640_user_validated  IS NULL
			AND c6640_transaction_fl   = 'Y'
			AND c6600_surgeon_npi_id  IS NOT NULL
			AND c6640_surgeon_name IS NOT NULL
			AND c1900_company_id IN (SELECT c906_rule_value 
				FROM T906_RULES 
				WHERE c906_rule_id = 'NPINUM' 
				AND c906_rule_grp_id = 'NPIVALID' 
				AND c906_void_fl IS NULL)
			AND c6640_void_fl         IS NULL;
			
		BEGIN
		 -- If NPI number not available, update the reason and valid flag
		  UPDATE t6640_npi_transaction
		  SET c6640_reason          = 'NPI Number not entered'
		  	, c6640_valid_fl	  = 'N'
		  WHERE c6640_valid_fl     IS NULL
		  	AND c6640_user_validated IS NULL
		  	AND c6640_transaction_fl  = 'Y'
		  	AND c6600_surgeon_npi_id IS NULL
		  	AND c1900_company_id IN (SELECT c906_rule_value 
				FROM T906_RULES 
				WHERE c906_rule_id = 'NPINUM' 
				AND c906_rule_grp_id = 'NPIVALID' 
				AND c906_void_fl IS NULL)
		  	AND c6640_void_fl        IS NULL;
			  
		  -- If Surgeon name is null
		  UPDATE t6640_npi_transaction
		  SET c6640_reason          = 'Surgeon Name is not entered'
		  	, c6640_valid_fl	  = 'N'
		  WHERE c6640_valid_fl     IS NULL
			  AND c6640_user_validated IS NULL
			  AND c6640_transaction_fl  = 'Y'
			  AND c1900_company_id IN (SELECT c906_rule_value 
					FROM T906_RULES 
					WHERE c906_rule_id = 'NPINUM' 
					AND c906_rule_grp_id = 'NPIVALID' 
					AND c906_void_fl IS NULL)
			  AND c6600_surgeon_npi_id IS NOT NULL
			  AND c6640_surgeon_name   IS NULL
			  AND c6640_void_fl        IS NULL;
			  
		  -- If Surgeon name is not valid
		  UPDATE t6640_npi_transaction
		  SET c6640_reason          = 'Incorrect Surgeon Name'
		  	, c6640_valid_fl	  = 'N'
		  WHERE c6640_valid_fl     IS NULL
			  AND c6640_user_validated IS NULL
			  AND c6640_transaction_fl  = 'Y'
			  AND c1900_company_id IN (SELECT c906_rule_value 
					FROM T906_RULES 
					WHERE c906_rule_id = 'NPINUM' 
					AND c906_rule_grp_id = 'NPIVALID' 
					AND c906_void_fl IS NULL)
			  AND c101_party_surgeon_id IS NULL
			  AND c6640_surgeon_name   IS NOT NULL
			  AND c6640_void_fl        IS NULL;
			  
		  FOR NPI_DTLS IN NPI_DTLS_CUR
		  LOOP
		    --IF NPI_DTLS.NPIID IS NOT NULL AND NPI_DTLS.PARTYID IS NOT NULL THEN
		      -- Check for NPI Number in subset table
		      SELECT COUNT(1)
		      INTO v_cnt
		      FROM t6600_party_surgeon
		      WHERE c6600_surgeon_npi_id = npi_dtls.npiid
		      AND c1900_company_id 		 IN (SELECT c906_rule_value 
						FROM T906_RULES 
						WHERE c906_rule_id = 'NPINUM' 
						AND c906_rule_grp_id = 'NPIVALID' 
						AND c906_void_fl IS NULL)
		      AND c6600_void_fl         IS NULL;
		      
		      IF v_cnt = 0 THEN
		      
		        UPDATE t6640_npi_transaction
		        SET c6640_reason          = 'Incorrect NPI number'
		        	, c6640_valid_fl	  = 'N'
		        WHERE c6640_valid_fl     IS NULL
		        AND c6640_ref_id          = npi_dtls.refid
		        AND c6600_surgeon_npi_id  = npi_dtls.npiid
		        AND c1900_company_id 	  IN (SELECT c906_rule_value 
						FROM T906_RULES 
						WHERE c906_rule_id = 'NPINUM' 
						AND c906_rule_grp_id = 'NPIVALID' 
						AND c906_void_fl IS NULL)
		        AND c6640_user_validated IS NULL;
		        
		       ELSE
		          UPDATE t6640_npi_transaction
		          SET c6640_valid_fl	    = 'Y'
		          WHERE c6640_valid_fl     IS NULL
		          AND c6640_ref_id          = npi_dtls.refid
		          AND c101_party_surgeon_id = npi_dtls.partyid
		          AND c1900_company_id 		IN (SELECT c906_rule_value 
						FROM T906_RULES 
						WHERE c906_rule_id = 'NPINUM' 
						AND c906_rule_grp_id = 'NPIVALID' 
						AND c906_void_fl IS NULL)
		          AND c6640_user_validated IS NULL;
		    END IF;
		  --END IF;
		END LOOP;
		END;

	END gm_upd_npi_valid_data_us;

	/**************************************************************************************************
    * Description : Procedure to validate and update the valid flag of the OUS NPI records saved for order
    ****************************************************************************************************/
	PROCEDURE gm_upd_npi_valid_data_ous
	AS
	BEGIN
		
		DECLARE
		  v_cnt  NUMBER;
		  /* Get all the records which are not validated yet for the transaction
		   * where NPI and Surgeon details are available
		   */
		  CURSOR NPI_DTLS_CUR
		  IS
		    SELECT c6600_surgeon_npi_id npiid,
			  c101_party_surgeon_id partyid,
			  c6640_ref_id refid,
			  c6640_surgeon_name surgeonname
			FROM t6640_npi_transaction
			WHERE c6640_valid_fl      IS NULL
			AND c6640_user_validated  IS NULL
			AND c6640_transaction_fl   = 'Y'
			--AND c6600_surgeon_npi_id  IS NOT NULL
			AND c6640_surgeon_name IS NOT NULL
			AND c1900_company_id NOT IN (SELECT c906_rule_value 
				FROM T906_RULES 
				WHERE c906_rule_id = 'NPINUM' 
				AND c906_rule_grp_id = 'NPIVALID' 
				AND c906_void_fl IS NULL)
			AND c6640_void_fl         IS NULL;
			
		BEGIN			  
		  -- If Surgeon name is null
		  UPDATE t6640_npi_transaction
		  SET c6640_reason          = 'Surgeon Name is not entered'
		  	, c6640_valid_fl	  = 'N'
		  WHERE c6640_valid_fl     IS NULL
			  AND c6640_user_validated IS NULL
			  AND c6640_transaction_fl  = 'Y'
			  AND c1900_company_id NOT IN (SELECT c906_rule_value 
					FROM T906_RULES 
					WHERE c906_rule_id = 'NPINUM' 
					AND c906_rule_grp_id = 'NPIVALID' 
					AND c906_void_fl IS NULL)
			  AND c6600_surgeon_npi_id IS NOT NULL
			  AND c6640_surgeon_name   IS NULL
			  AND c6640_void_fl        IS NULL;
			  
		  FOR NPI_DTLS IN NPI_DTLS_CUR
		  LOOP
		      
	        -- Check for Surgeon Party number available in subset table or not
	        SELECT COUNT(1)
	        INTO v_cnt
	        FROM t6600_party_surgeon
	        WHERE c101_party_surgeon_id = npi_dtls.partyid
	        AND c1900_company_id 	NOT	IN (SELECT c906_rule_value 
					FROM T906_RULES 
					WHERE c906_rule_id = 'NPINUM' 
					AND c906_rule_grp_id = 'NPIVALID' 
					AND c906_void_fl IS NULL)
	        AND c6600_void_fl          IS NULL;
	        
	        IF V_CNT                    = 0 THEN
	          UPDATE t6640_npi_transaction
	          SET c6640_reason          = 'Incorrect Surgeon Name'
	          	, c6640_valid_fl	    = 'N'
	          WHERE c6640_valid_fl     IS NULL
	          AND c6640_ref_id          = npi_dtls.refid
	          AND c6640_surgeon_name = npi_dtls.surgeonname
	          AND c1900_company_id 	NOT	IN (SELECT c906_rule_value 
					FROM T906_RULES 
					WHERE c906_rule_id = 'NPINUM' 
					AND c906_rule_grp_id = 'NPIVALID' 
					AND c906_void_fl IS NULL)
	          AND c6640_user_validated IS NULL;
	        ELSE
	          UPDATE t6640_npi_transaction
	          SET c6640_valid_fl	    = 'Y'
	          WHERE c6640_valid_fl     IS NULL
	          AND c6640_ref_id          = npi_dtls.refid
	          AND c101_party_surgeon_id = npi_dtls.partyid
	          AND c1900_company_id 	NOT IN (SELECT c906_rule_value 
					FROM T906_RULES 
					WHERE c906_rule_id = 'NPINUM' 
					AND c906_rule_grp_id = 'NPIVALID' 
					AND c906_void_fl IS NULL)
	          AND c6640_user_validated IS NULL;
	        END IF;
		END LOOP;
		END;

	END gm_upd_npi_valid_data_ous;
	
	/**********************************************************
    * Description : Procedure to save the user validated record
    ***********************************************************/
	PROCEDURE gm_sav_valid_npi(
		p_npi_txn_id	IN	t6640_npi_transaction.c6640_id%TYPE,
		p_user_id 		IN	t6640_npi_transaction.c6640_last_updated_by%TYPE
	)
	AS
		v_party_id		t6640_npi_transaction.c101_party_surgeon_id%TYPE;
		v_surgeon_nm	t6640_npi_transaction.c6640_surgeon_name%TYPE;
		v_last_nm		VARCHAR2(200);
		v_first_nm		VARCHAR2(200);
		v_company_id	t6640_npi_transaction.c1900_company_id%TYPE;
		v_surgeon_npi_id	t6640_npi_transaction.c6600_surgeon_npi_id%TYPE;
		v_npi_cnt		NUMBER;
	BEGIN
		
		BEGIN
			SELECT c101_party_surgeon_id, c6640_surgeon_name, c1900_company_id, c6600_surgeon_npi_id
				INTO v_party_id, v_surgeon_nm, v_company_id, v_surgeon_npi_id
			FROM t6640_npi_transaction
			WHERE c6640_id     = p_npi_txn_id
			AND c6640_void_fl IS NULL;
		EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			v_party_id:= NULL;
		END;

		IF v_party_id IS NULL OR v_party_id = ''
		THEN
			SELECT s101_party.NEXTVAL
	           INTO v_party_id
	        FROM DUAL;
	        
			SELECT SUBSTR(v_surgeon_nm, INSTR(v_surgeon_nm,' ',-1) + 1)
				INTO v_last_nm
			FROM dual;
			
			SELECT SUBSTR(v_surgeon_nm, 0, LENGTH(v_surgeon_nm) - LENGTH(v_last_nm)-1)
				INTO v_first_nm
			FROM dual;
		
			INSERT INTO t101_party
                     (c101_party_id, c101_last_nm, c101_first_nm,
                      c101_middle_initial, c901_party_type, c101_party_nm,
                      c101_active_fl, c101_created_by, c101_created_date , c1900_company_id,c101_party_nm_en
                     )
              VALUES (v_party_id, v_last_nm, v_first_nm,
                      NULL, 7000, NULL,
                      NULL, p_user_id, CURRENT_DATE , v_company_id, NULL
                     );
			
		END IF;
		
		UPDATE t6640_npi_transaction
		SET c6640_user_validated  = 'Y' ,
		  c101_party_surgeon_id = v_party_id,
		  c6640_last_updated_by   = p_user_id ,
		  c6640_last_updated_date = CURRENT_DATE
		WHERE NVL(c6600_surgeon_npi_id,-999) = NVL(v_surgeon_npi_id,-999)
			AND NVL(c6640_surgeon_name,-999) = NVL(v_surgeon_nm,-999)
			AND c6640_void_fl        IS NULL;
		
		-- While marking as user validate, if the npi record already available in T6600_PARTY_SURGEON table, do not insert again
		SELECT COUNT(1)
			INTO v_npi_cnt
		FROM T6600_PARTY_SURGEON
		WHERE C6600_SURGEON_NPI_ID = v_surgeon_npi_id
		AND C6600_VOID_FL         IS NULL;
	--	AND C1900_COMPANY_ID       = v_company_id; -- PMT-53468 - For OUS creates surgeon based on company, so need to remove
		
		IF v_npi_cnt = 0
		THEN
			-- Insert user validated record into t6600 table, 106580: Checked
			INSERT
			INTO T6600_PARTY_SURGEON
			  (
			    C6600_SURGEON_NPI_ID,
			    C101_PARTY_SURGEON_ID,
			    C6600_CREATED_BY,
			    C6600_CREATED_DATE,
			    C1900_COMPANY_ID,
			    C5040_PLANT_ID,
			    C6600_STATUS_FL
			  )
			SELECT C6600_SURGEON_NPI_ID,
			  C101_PARTY_SURGEON_ID,
			  p_user_id,
			  CURRENT_DATE,
			  c1900_company_id,
			  c5040_plant_id,
			  106580
			FROM T6640_NPI_TRANSACTION
			WHERE C6640_ID = p_npi_txn_id;
		END IF;
		
	END gm_sav_valid_npi;
	
	/***********************************************************************
    * Description : Procedure to void the NPI records if the order is voided
    ************************************************************************/
	PROCEDURE gm_sav_void_npi
	AS
	BEGIN
		
		-- Void NPI details if the order id is voided
		UPDATE T6640_NPI_TRANSACTION
		SET C6640_VOID_FL         = 'Y',
		  C6640_LAST_UPDATED_BY   = '',
		  C6640_LAST_UPDATED_DATE = sysdate
		WHERE C6640_REF_ID       IN
		  ( SELECT DISTINCT T501.C501_PARENT_ORDER_ID
		  FROM T6640_NPI_TRANSACTION T6640,
		    T501_ORDER T501
		  WHERE T6640.C6640_REF_ID = T501.C501_PARENT_ORDER_ID
		  AND T501.C501_VOID_FL   IS NOT NULL
		  AND C6640_TRANSACTION_FL = 'Y'
		  AND C6640_VOID_FL       IS NULL
		  );
	END gm_sav_void_npi;
	
	/***********************************************************************
    *  Description : Procedure to fetch the valid NPI records
    ************************************************************************/
  
	PROCEDURE  gm_fch_valid_NPI_Dtls(
		p_npi_txn_id	IN	    t6640_npi_transaction.c6640_id%TYPE,
		p_out_cursor    OUT     Types.cursor_type
		)
	AS
	BEGIN
		 OPEN  p_out_cursor
	        FOR 
			SELECT  c6600_surgeon_npi_id SURGEON_NPI_ID,c6640_surgeon_name SURGEON_NAME
			FROM t6640_npi_transaction
			WHERE c6640_id     =  p_npi_txn_id
			AND C6640_VOID_FL IS NULL;
			
	END gm_fch_valid_NPI_Dtls;
	
	PROCEDURE gm_sav_npi(
		p_npi_id	IN	    t6640_npi_transaction.c6600_surgeon_npi_id%TYPE,
		p_ord_id    IN      t6640_npi_transaction.c6640_ref_id%TYPE,
		p_first_nm  IN      t101_party.c101_first_nm%TYPE,
		p_middle_nm IN      t101_party.c101_middle_initial%TYPE,
		p_last_nm   IN      t101_party.c101_last_nm%TYPE,
		p_add1      IN      t106_address.c106_add1%TYPE,
		p_add2      IN      t106_address.c106_add2%TYPE,
		p_city      IN      t106_address.c106_city%TYPE,
		p_zip_code  IN      t106_address.c106_zip_code%TYPE,
		p_stage     IN      VARCHAR2,
		p_country   IN      VARCHAR2,
		p_user_id   IN      t106_address.c106_created_by%TYPE
	)
	AS
		v_surgeon_npi_id	t6640_npi_transaction.c6600_surgeon_npi_id%TYPE;
		v_npi_cnt		    NUMBER;
		v_party_id		    t101_party.c101_party_id%TYPE;
		v_addressid      	t106_address.c106_address_id%TYPE;
		v_company_id        t1900_company.c1900_company_id%TYPE;
		v_plant_id 			t5040_plant_master.c5040_plant_id%TYPE;
		v_first_nm          t101_party.c101_first_nm%TYPE;
		v_middle_nm         t101_party.c101_middle_initial%TYPE;
		v_last_nm           t101_party.c101_last_nm%TYPE;
		v_party_nm          t101_party.c101_party_nm%TYPE;
		v_add1              t106_address.c106_add1%TYPE;
		v_add2              t106_address.c106_add2%TYPE;
		v_city              t106_address.c106_city%TYPE;
		v_state             t106_address.c901_state%TYPE;
		v_zip_code          t106_address.c106_zip_code%TYPE;
		v_country           t106_address.c901_country%TYPE;
		v_npi_txn           t6640_npi_transaction.c6640_id%TYPE;
		v_npi_party_id      t6640_npi_transaction.c101_party_surgeon_id%TYPE;
		v_country_id        t106_address.c901_country%TYPE;
		v_state_id          t106_address.c901_state%TYPE;
	BEGIN
		
		SELECT get_compid_frm_cntx() , get_plantid_frm_cntx()
		 	 INTO v_company_id , v_plant_id
		 FROM dual; 
	  
		SELECT COUNT(1)
			INTO v_npi_cnt
		FROM T6600_PARTY_SURGEON
		WHERE C6600_SURGEON_NPI_ID = p_npi_id
		AND C6600_VOID_FL         IS NULL;
	
	BEGIN
		SELECT c6640_id INTO v_npi_txn
		FROM T6640_NPI_TRANSACTION
		WHERE c6640_ref_id = p_ord_id
		AND c6600_surgeon_npi_id = p_npi_id
		AND c6640_void_fl IS NULL;
	EXCEPTION
      WHEN NO_DATA_FOUND
    THEN
      v_npi_txn := NULL;
    END;	
		--get code id for state
	BEGIN
		SELECT  C901_CODE_ID CODEID
	    INTO v_state_id
	    FROM  T901_CODE_LOOKUP
        WHERE  UPPER(C902_CODE_NM_ALT) = UPPER(p_stage)
        AND c901_code_grp='STATE'
         AND c901_void_fl is null;
    EXCEPTION
      WHEN NO_DATA_FOUND
    THEN
      v_state_id := 0;
    END;
        --get code id for country
    BEGIN
		SELECT  C901_CODE_ID CODEID
	    INTO v_country_id
	    FROM  T901_CODE_LOOKUP
        WHERE  UPPER(C902_CODE_NM_ALT) = UPPER(p_country)
        AND c901_code_grp='CNTY'
        AND c901_void_fl is null;
    EXCEPTION
      WHEN NO_DATA_FOUND
    THEN
      v_country_id := 0;
    END;
		IF v_npi_cnt = 0 THEN
		
			--get party id from dual
		SELECT s101_party.NEXTVAL 
			INTO v_party_id 
		FROM DUAL;
		
			--insert into name details in party table
			INSERT
			INTO t101_party
			  (
			    c101_party_id,
			    c101_last_nm,
			    c101_first_nm,
			    c101_middle_initial,
			    c901_party_type,
			    c101_party_nm,
			    c101_active_fl,
			    c101_created_by,
			    c101_created_date ,
			    c1900_company_id,
			    c101_party_nm_en,
			    c101_api_fl
			  )
			  VALUES
			  (
			    v_party_id,
			    p_last_nm,
			    p_first_nm,
			    p_middle_nm,
			    7000,
			    NULL,
			    NULL,
			    p_user_id,
			    CURRENT_DATE ,
			    v_company_id,
			    NULL,
			    'Y'
			  );
			  
		IF v_npi_txn IS NOT NULL THEN
	      UPDATE t6640_npi_transaction
	         SET c101_party_surgeon_id = v_party_id,
	             c6640_last_updated_by = p_user_id,
	             c6640_last_updated_date = SYSDATE
	       WHERE c6640_id = v_npi_txn;
	    END IF;
			-- Insert user validated record into t6600 table, 106580: Checked
			INSERT
			INTO T6600_PARTY_SURGEON
			  (
			    C6600_SURGEON_NPI_ID,
			    C101_PARTY_SURGEON_ID,
			    C6600_CREATED_BY,
			    C6600_CREATED_DATE,
			    C1900_COMPANY_ID,
			    C5040_PLANT_ID,
			    C6600_STATUS_FL
			  )
			SELECT C6600_SURGEON_NPI_ID,
			  C101_PARTY_SURGEON_ID,
			  p_user_id,
			  CURRENT_DATE,
			  c1900_company_id,
			  c5040_plant_id,
			  106580
			FROM T6640_NPI_TRANSACTION
			WHERE C6640_ID = v_npi_txn;
			
			UPDATE T6640_NPI_TRANSACTION
               SET c6640_valid_fl = 'Y',
                   c6640_surgeon_name =  ( SELECT partynm FROM v6600_party_surgeon  WHERE c101_party_surgeon_id = v_npi_party_id)
             WHERE c6600_surgeon_npi_id = p_npi_id
               AND c101_party_surgeon_id = v_party_id
               AND c6640_void_fl IS NULL;
			
			
			
			--get address id from dual
			SELECT s106_address.NEXTVAL 
			INTO v_addressid
			FROM DUAL;
			
			--insert into address details in address table
			INSERT
			INTO T106_ADDRESS
			  (
			    C106_ADDRESS_ID,
			    C101_PARTY_ID,
			    C106_ADD1,
			    C106_ADD2,
			    C106_CITY,
			    C106_ZIP_CODE,
			    C901_STATE,
			    C901_COUNTRY,
			    C901_ADDRESS_TYPE,
			    C106_PRIMARY_FL,
			    C106_CREATED_BY,
			    C106_CREATED_DATE
			  )
			  VALUES
			  (
			    v_addressid,
			    v_party_id,
			    p_add1,
			    p_add2,
			    p_city,
			    p_zip_code,
			    v_state_id,
			    v_country_id,
			    '90400',
			    NULL,
			    p_user_id,
			    CURRENT_DATE
			  );

       ELSE
       --we need to get party id for npi
       SELECT c101_party_surgeon_id  INTO v_npi_party_id
		FROM t6640_npi_transaction  
		WHERE c6600_surgeon_npi_id= p_npi_id
		AND c6640_id = v_npi_txn
		AND c6640_ref_id      = p_ord_id
		AND c6640_void_fl IS NULL;

        --fetch party name details from db because need to compare
        	SELECT c101_party_nm,c101_first_nm, c101_middle_initial, c101_last_nm
        	INTO v_party_nm,v_first_nm,v_middle_nm,v_last_nm
			FROM t101_party
			WHERE c101_party_id = v_npi_party_id;
			
		--If anything mismatched then update 	
		IF v_first_nm <> p_first_nm OR v_middle_nm <> p_middle_nm OR v_last_nm <> p_last_nm THEN
			UPDATE t101_party
			SET c101_first_nm     = p_first_nm,
			  c101_middle_initial = p_middle_nm,
			  c101_last_nm        = p_last_nm
			WHERE c101_party_id   = v_npi_party_id;
		END IF;	

		--update surgonname and valid flag
			UPDATE T6640_NPI_TRANSACTION
			SET  c6640_valid_fl        = 'Y',
			c6640_surgeon_name = (SELECT partynm 
			                        FROM v6600_party_surgeon 
			                       WHERE c101_party_surgeon_id = v_npi_party_id)     -- surgeon name using v6600_party_surgeon modified in PC-4359 
			WHERE c6640_ref_id      = p_ord_id
			AND c6600_surgeon_npi_id= p_npi_id
			AND C6640_ID = v_npi_txn
			AND c6640_void_fl IS NULL;
			
	END IF;
	
	END gm_sav_npi;
	
	/***********************************************************************
    * Description : Procedure to void the NPI records if the order is voided
    ************************************************************************/
	PROCEDURE gm_sav_npi_master_update(
		p_npi_id	IN	    t6640_npi_transaction.c6600_surgeon_npi_id%TYPE,
		p_ord_id    IN      t6640_npi_transaction.c6640_ref_id%TYPE,
		p_status_fl IN      t6640_npi_transaction.C6640_NPI_MASTER_FL%TYPE,
		p_user_id   IN      t106_address.c106_created_by%TYPE
	)
	AS
	BEGIN
		
		-- Void NPI details if the order id is voided
		UPDATE T6640_NPI_TRANSACTION
		SET c6640_npi_master_fl   = p_status_fl,
		  C6640_LAST_UPDATED_BY   = p_user_id,
		  C6640_LAST_UPDATED_DATE = sysdate
		WHERE c6600_surgeon_npi_id= p_npi_id
		AND c6640_ref_id          = p_ord_id
		AND c6640_void_fl IS NULL;
	END gm_sav_npi_master_update;
	/**********************************************************************************************
    * Description : Procedure to save the NPI and Surgeon details of the transaction in t6640 table
    **********************************************************************************************/
	PROCEDURE gm_sav_npi_partyid_transaction(
		p_npi_tran_id 		IN OUT  t6640_npi_transaction.c6640_id%TYPE,
		p_ref_id      		IN      t6640_npi_transaction.c6640_ref_id %TYPE,
		p_account_id  		IN      t6640_npi_transaction.c704_account_id%TYPE,
		p_surgeon_npi_id  	IN  	t6640_npi_transaction.c6600_surgeon_npi_id%TYPE,
		p_surgeon_name    	IN  	t6640_npi_transaction.c6640_surgeon_name%TYPE,
		p_tran_flag       	IN  	t6640_npi_transaction.c6640_transaction_fl%TYPE,
		p_userid          	IN  	t6640_npi_transaction.c6640_created_by%TYPE,
		p_submit_type	  	IN		VARCHAR2 DEFAULT NULL,
		p_surgeon_id		IN		t6640_npi_transaction.c101_party_surgeon_id%TYPE,
		p_status            IN      NUMBER DEFAULT 52027
	)
	AS
	v_company_id    	t1900_company.c1900_company_id%TYPE;
	v_plant_id 			t5040_plant_master.c5040_plant_id%TYPE;
	v_party_surgeon_id 	t6640_npi_transaction.c101_party_surgeon_id%TYPE;
	v_sales_rep_id 		t6640_npi_transaction.c703_sales_rep_id%TYPE;
	v_tran_flag 		t6640_npi_transaction.c6640_transaction_fl%TYPE;
	v_account_id		t6640_npi_transaction.c704_account_id%TYPE := p_account_id;
	v_first_nm			t101_party.c101_first_nm%TYPE;
	v_last_nm			t101_party.c101_last_nm%TYPE;
	v_surgeon_name    	t6640_npi_transaction.c6640_surgeon_name%TYPE;
	v_valid_fl      	t6640_npi_transaction.C6640_VALID_FL%TYPE;
	BEGIN
		SELECT get_compid_frm_cntx() , get_plantid_frm_cntx()
	  		INTO v_company_id , v_plant_id
	  	FROM dual;
	  	
	  	v_valid_fl := NULL;
   	  	IF v_company_id != '1000'
 		THEN
   			v_valid_fl := 'Y';
  		END IF;

   	 	v_tran_flag := NVL(p_tran_flag,'N');
   		
   	  	BEGIN	
   			SELECT c101_party_surgeon_id, get_party_name(c101_party_surgeon_id)
				INTO v_party_surgeon_id, v_surgeon_name
			FROM t6600_party_surgeon
			WHERE c6600_surgeon_npi_id = p_surgeon_npi_id
			AND c6600_void_fl IS NULL;
   	  	EXCEPTION WHEN NO_DATA_FOUND THEN
	   	   	v_party_surgeon_id := NULL;
	   	   	v_surgeon_name := NULL;
   	  	END;
   	  	
   	  	IF v_surgeon_name IS NULL
   	  	THEN
   	  		v_surgeon_name:= p_surgeon_name;
   	  	END IF;
   	  	
		BEGIN
   	  		SELECT c703_sales_rep_id
				INTO v_sales_rep_id
			FROM t704_account
			WHERE c704_account_id = TO_CHAR(p_account_id)
			AND c704_void_fl IS NULL;
	  	EXCEPTION WHEN NO_DATA_FOUND THEN
	  		BEGIN
	   	   		SELECT c704_account_id,
				  c703_sales_rep_id
				  INTO v_account_id, v_sales_rep_id 
				FROM t501_order
				WHERE c501_order_id = p_ref_id
				AND c501_void_fl   IS NULL;	
		  		EXCEPTION WHEN NO_DATA_FOUND THEN
		  			v_account_id := NULL;
		  			v_sales_rep_id := NULL;
		  	END;
   	  	END;   	  
		
   	  	IF v_party_surgeon_id IS NULL THEN
   	  		v_party_surgeon_id := p_surgeon_id;  	  	
   	  	END IF;
		
--   	  	IF v_party_surgeon_id IS NULL
--   	  	THEN
--   	  	BEGIN
--   	  		SELECT c101_party_id
--   	  		  INTO v_party_surgeon_id
--   	  		  FROM V6600_PARTY_SURGEON   	  	
--   	  		 WHERE partynm = p_surgeon_name;
--   	  	EXCEPTION WHEN OTHERS THEN
--   	  		v_party_surgeon_id := NULL;
--   	  		v_valid_fl := NULL;
--   	  	END;
--   	  	END IF;

   	  	-- If the npi transaction id(primary key) is passing from front end, update the value otherwise insert new record 
   	  	IF p_npi_tran_id IS NOT NULL THEN
	      UPDATE t6640_npi_transaction
	         SET c6640_ref_id = p_ref_id,
	             c6600_surgeon_npi_id = p_surgeon_npi_id,
	             c101_party_surgeon_id = v_party_surgeon_id,
	             c6640_surgeon_name = v_surgeon_name,
	             c6640_transaction_fl = v_tran_flag,
	             c704_account_id = v_account_id,
	             c703_sales_rep_id = v_sales_rep_id,
	             c6640_last_updated_by = p_userid,
	             c6640_last_updated_date = SYSDATE,
	             c6640_valid_fl = v_valid_fl
	       WHERE c6640_id = p_npi_tran_id;
		ELSE
          SELECT s6640_id.NEXTVAL
           INTO p_npi_tran_id
           FROM DUAL;

         	INSERT INTO t6640_npi_transaction
                     (c6640_id, c6640_ref_id, c6600_surgeon_npi_id,
                      c101_party_surgeon_id, c6640_surgeon_name,c6640_transaction_fl,
                      c704_account_id,c703_sales_rep_id,
                      c6640_created_by, c6640_created_date,
                      c1900_company_id, c5040_plant_id, c901_status,c6640_valid_fl
                     )
              VALUES (p_npi_tran_id, p_ref_id, p_surgeon_npi_id,
                      v_party_surgeon_id, v_surgeon_name,v_tran_flag,
                      v_account_id,v_sales_rep_id,
                      p_userid, SYSDATE
                      , v_company_id, v_plant_id, p_status,v_valid_fl
                     );
      	END IF;
      -- saving account npi mapping
      gm_pkg_cm_npi_process_txn.gm_sav_npi_account(v_account_id,v_sales_rep_id,p_surgeon_npi_id,v_party_surgeon_id,v_surgeon_name,p_userid);
	
      -- If the value is saving from Modify Order screen, need to update the transaction flag also 
      IF(p_submit_type = 'Modify' OR p_submit_type = 'NpiErrorEdit')
      THEN
      	-- TO UPDATE THE NPI TRANSACTION STATUS
      	gm_pkg_cm_npi_process_txn.gm_save_npi_tran_status(p_ref_id, p_userid, 'Y');
      END IF;
      
      -- If the NPI/Surgeon is adding from NPI Error Edit screen, then the record should be marked as valid 
      IF(p_submit_type = 'NpiErrorEdit')
      THEN
      	gm_pkg_cm_npi_process_txn.gm_sav_valid_npi(p_npi_tran_id,p_userid);
      END IF;
      
	END gm_sav_npi_partyid_transaction;	
END gm_pkg_cm_npi_process_txn;
/
