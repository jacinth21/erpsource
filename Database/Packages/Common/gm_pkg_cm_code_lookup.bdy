/* Formatted on 2010/10/04 14:42 (Formatter Plus v4.8.0) */
--@"C:\Database\Packages\Common\gm_pkg_cm_code_lookup.bdy";
create or replace PACKAGE BODY gm_pkg_cm_code_lookup
IS
/*******************************************************
   * Description : Master procedure to save code lookup
   * Author      : Gopinathan
*******************************************************/
  PROCEDURE gm_sav_code_lookup_master (
      p_code_id       IN       t901_code_lookup.c901_code_id%TYPE,
      p_code_nm       IN       t901_code_lookup.c901_code_nm%TYPE,
      p_code_grp      IN       t901_code_lookup.c901_code_grp%TYPE,
      p_active_fl     IN       t901_code_lookup.c901_active_fl%TYPE,
      p_seq_no        IN       t901_code_lookup.c901_code_seq_no%TYPE,
      p_code_nm_alt   IN       t901_code_lookup.c902_code_nm_alt%TYPE,
      p_user_id       IN       t901_code_lookup.c901_created_by%TYPE
   )
   AS
   v_code_id NUMBER;
   v_code_nm t901_code_lookup.c901_code_nm%TYPE;
   v_cnt NUMBER;
   BEGIN
	   IF p_code_id IS NOT NULL THEN
	   
	  select COUNT(1) INTO v_cnt
		from T907_CANCEL_LOG t907 
	   where t907.c901_cancel_cd = p_code_id
		and t907.c907_void_fl IS NULL;
		
		SELECT GET_CODE_NAME(p_code_id) into v_code_nm from dual;
		
			IF  v_cnt > 0 AND p_code_nm != v_code_nm  THEN
			GM_RAISE_APPLICATION_ERROR('-20999','12',v_code_nm);
			END IF;
	END IF;
	
      UPDATE t901_code_lookup
         SET c901_code_nm = p_code_nm,
             c901_code_grp = p_code_grp,
             c902_code_nm_alt = p_code_nm_alt,
             c901_code_seq_no = p_seq_no,
             c901_active_fl = p_active_fl,
             c901_last_updated_date = current_date,
             c901_last_updated_by = p_user_id
       WHERE c901_code_id = p_code_id;
      IF (SQL%ROWCOUNT = 0)
      THEN
         SELECT s901_code_lookup.NEXTVAL
           INTO v_code_id
           FROM DUAL;

         INSERT INTO t901_code_lookup
                     (c901_code_id, c901_code_nm, c901_code_grp,
                      c901_active_fl, c901_code_seq_no, c902_code_nm_alt,
                      c901_created_date, c901_created_by
                     )
              VALUES (v_code_id, p_code_nm, p_code_grp,
                      p_active_fl, p_seq_no, p_code_nm_alt,
                      CURRENT_DATE, p_user_id
                     );
      END IF;
   END gm_sav_code_lookup_master;

/*******************************************************
   * Description : Procedure to save code lookup
   * Author      : Gopinathan
*******************************************************/
   PROCEDURE gm_sav_code_lookup (p_inputstr IN CLOB, p_userid IN VARCHAR2)
   AS
      v_code_grp      t901_code_lookup.c901_code_grp%TYPE;
      v_code_nm       t901_code_lookup.c901_code_nm%TYPE;
      v_code_alt_nm   t901_code_lookup.c902_code_nm_alt%TYPE;
      v_control_type  t901_code_lookup.c901_control_type%TYPE;
      v_active_fl     NUMBER;
      v_lock_fl		  NUMBER;
      v_sequence_no   NUMBER :=0;
      v_code_id       NUMBER;
      v_strlen        NUMBER                  := NVL (LENGTH (p_inputstr), 0);
      v_string        CLOB                    := TRIM (p_inputstr);
      v_substring     CLOB;
      v_str           VARCHAR2 (200);
      v_code_nm_tmp	  VARCHAR2 (200);	
      v_cnt			  NUMBER;
   BEGIN
      IF v_strlen > 0
      THEN
         WHILE INSTR (v_string, '|') <> 0
         LOOP
            v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
            v_string := SUBSTR (v_string, INSTR (v_string, '|') + 1);
            v_code_id := NULL;
            v_code_grp := NULL;
            v_code_nm := NULL;
            v_code_alt_nm := NULL;
            v_control_type :=NULL;
            v_active_fl := NULL;
            v_lock_fl := NULL;
            v_sequence_no := NULL;
            v_code_id :=TO_NUMBER (SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1));
            v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
            v_code_grp := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
            v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
            v_code_nm := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
            v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
            v_code_alt_nm :=SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
            v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
            v_control_type :=SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
            v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
            v_active_fl :=TO_NUMBER(SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1));
            v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
            v_lock_fl :=TO_NUMBER(SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1));
            v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
            v_sequence_no := TO_NUMBER (v_substring);
            
    IF v_code_id IS NOT NULL THEN	   
	  select COUNT(1) INTO v_cnt
		from T907_CANCEL_LOG t907 
	   where t907.c901_cancel_cd = v_code_id
		and t907.c907_void_fl IS NULL;
		
		SELECT GET_CODE_NAME(v_code_id) 
		  INTO v_code_nm_tmp 
		  FROM DUAL;
		
			IF  v_cnt > 0 AND v_code_nm_tmp != v_code_nm  THEN
				GM_RAISE_APPLICATION_ERROR('-20999','13',v_code_nm_tmp);
		END IF;
	END IF;
        
        UPDATE t901_code_lookup
         SET c901_code_nm = v_code_nm,
             c901_code_grp = v_code_grp,
             c902_code_nm_alt = v_code_alt_nm,
             c901_control_type = v_control_type,
             c901_code_seq_no = v_sequence_no,
             c901_active_fl = v_active_fl,
              c901_lock_fl = v_lock_fl,   
             c901_last_updated_date = current_date,
             c901_last_updated_by = p_userid
       WHERE c901_code_id = v_code_id;

      IF (SQL%ROWCOUNT = 0)
      THEN
      
        /* Select Query is modified for handling the Parent Key Not Found Error at the time of New Code ID insertion as part of IST Project */    
        SELECT MAX(c901_code_id)+1 
          INTO v_code_id 
          FROM t901_code_lookup;

         INSERT INTO t901_code_lookup
                     (c901_code_id, c901_code_nm, c901_code_grp,
                      c901_active_fl, c901_lock_fl, c901_code_seq_no, c902_code_nm_alt, c901_control_type,
                      c901_created_date, c901_created_by
                     )
              VALUES (v_code_id, v_code_nm, v_code_grp,
                      v_active_fl, v_lock_fl, v_sequence_no, v_code_alt_nm, v_control_type,
                      current_date, p_userid
                     );
      END IF;
            
         END LOOP;
      END IF;
   END gm_sav_code_lookup;
  
   
  /*******************************************************
   * Description : Procedure to save code group
   * Author      : Gopinathan
*******************************************************/ 
   PROCEDURE gm_sav_code_group(p_code_grp_ID IN OUT t901b_code_group_master.c901b_code_grp_id%TYPE,
   	p_code_grp IN t901b_code_group_master.c901b_code_grp%TYPE,
    p_code_des IN t901b_code_group_master.c901b_code_description%TYPE,
    p_lookup_needed IN t901b_code_group_master.c901b_lookup_needed%TYPE,
    p_user_ID IN t901b_code_group_master.c901b_created_by%TYPE)
    AS
      v_code_grp_ID NUMBER;
      v_seq_max 	NUMBER;
      v_seq_inc		NUMBER;
	BEGIN
    	
		SELECT MAX(C901_CODE_SEQ_NO) INTO v_seq_max 
          	FROM T901_CODE_LOOKUP  
          		WHERE c901_code_grp = p_code_grp;
          		
          	if v_seq_max IS NULL THEN
          		v_seq_max:=0;
          	END IF;
          	v_seq_inc:=v_seq_max+1;

          UPDATE  t901b_code_group_master
          SET                    c901b_code_grp = p_code_grp,
                                    c901b_code_description = p_code_des ,
                                    c901b_lookup_needed = c901b_lookup_needed+p_lookup_needed,
                                    c901b_last_updated_by =  p_user_ID,
                                    c901b_last_updated_date = current_date
          WHERE c901b_code_grp_id = p_code_grp_ID
          AND  c901b_void_fl IS NULL;

       	IF (SQL%ROWCOUNT = 0)  THEN
            
          SELECT s901b_code_group_master.NEXTVAL
           INTO  v_code_grp_ID
           FROM DUAL;
          
          INSERT INTO t901b_code_group_master
                         (c901b_code_grp_id,
                          c901b_code_grp,
                          c901b_code_description,
                          c901b_lookup_needed,
                          c901b_created_by,
                         c901b_created_date)
          VALUES (v_code_grp_ID,
                       p_code_grp,
                       p_code_des,
                       p_lookup_needed,
                       p_user_ID,
                       current_date);
        
          FOR inSeq IN 1..p_lookup_needed
          LOOP
                  gm_sav_code_lookup_master(NULL,' ',p_code_grp,0,v_seq_inc,'',p_user_ID);
                  v_seq_inc:=v_seq_inc+1;
          END LOOP;
          p_code_grp_ID := v_code_grp_ID;
         ELSE
         	FOR inSeq IN 1..p_lookup_needed
          	LOOP
                  gm_sav_code_lookup_master(NULL,' ',p_code_grp,0,v_seq_inc,'',p_user_ID);
                  v_seq_inc:=v_seq_inc+1;
          END LOOP;
	END IF;
END gm_sav_code_group;

 /*******************************************************
   * Description : Procedure to check code group availability
   * Author      : Gopinathan
*******************************************************/
    
   PROCEDURE gm_chk_code_group(p_code_grp IN OUT t901b_code_group_master.c901b_code_grp%TYPE)
   AS
	v_count_grp  NUMBER  := 0;
   BEGIN

          SELECT COUNT(c901b_code_grp_id) INTO  v_count_grp
            FROM t901b_code_group_master
            WHERE c901b_code_grp =  p_code_grp
            AND c901b_void_fl IS NULL;
	p_code_grp := v_count_grp;
   END gm_chk_code_group;  

/*******************************************************
   * Description : Procedure to fetch code group
   * Author      : Gopinathan
*******************************************************/
   
   PROCEDURE gm_fch_code_group(p_code_grp_ID IN t901b_code_group_master.c901b_code_grp_id%TYPE,
   	p_out_cursor OUT Types.cursor_type)
   AS
BEGIN

		OPEN p_out_cursor
       		FOR
         	 SELECT c901b_code_grp_id CODEGRPID,
                    c901b_code_grp CODEGRP,
                    c901b_code_description CODEDES,
                     c901b_lookup_needed LOOKUPNEEDED
             FROM t901b_code_group_master
             WHERE c901b_code_grp_id = p_code_grp_ID
             AND c901b_void_fl IS NULL;
END gm_fch_code_group;

   
/*******************************************************
   * Description : Procedure to fetch all the records from Code Group Master and Code Lookup
   * Author      : Rajkumar
*******************************************************/
PROCEDURE gm_fetch_code_group(
	p_grp_name IN T901_CODE_LOOKUP.C901_CODE_GRP%TYPE,
	p_frm_code_id IN T901_CODE_LOOKUP.C901_CODE_ID%TYPE,
	p_to_code_id IN T901_CODE_LOOKUP.C901_CODE_ID%TYPE,
	p_out_data OUT Types.cursor_type
)
AS
BEGIN
	OPEN p_out_data
	FOR
	SELECT
		T901B.C901B_CODE_GRP_ID grpid, NVL(T901B.C901B_VOID_FL,'N') voidfl,
		T901B.C901B_CODE_GRP codegrp,
				(SELECT
			COUNT(C901_CODE_ID)
		 FROM
		 	T901_CODE_LOOKUP
		 WHERE
		 	LOWER(C901_CODE_GRP) = NVL (LOWER(T901B.C901B_CODE_GRP), LOWER(C901_CODE_GRP))
		 	AND (C901_CODE_ID BETWEEN NVL(p_frm_code_id,C901_CODE_ID) AND NVL(p_to_code_id,C901_CODE_ID))
			) countrange,
		GET_USER_NAME(T901B.C901B_LAST_UPDATED_BY) updatedby,
		TO_CHAR(T901B.C901B_LAST_UPDATED_DATE,GET_RULE_VALUE('DATEFMT', 'DATEFORMAT')) updateddate
	FROM
		T901B_CODE_GROUP_MASTER T901B
 	WHERE
		LOWER(C901B_CODE_GRP) LIKE '%' || NVL (LOWER(p_grp_name), LOWER(C901B_CODE_GRP)) || '%'
		AND C901B_VOID_FL IS NULL;		
END gm_fetch_code_group;

/*******************************************************
   * Description : Procedure to fetch code group list
   * Author      : Gopinathan
*******************************************************/
   
PROCEDURE gm_fch_code_group_list( p_code_grp      IN       t901_code_lookup.c901_code_grp%TYPE,
   p_out_data OUT Types.cursor_type)
    AS
BEGIN
OPEN p_out_data
	FOR
	SELECT  c901_code_id CODEID,
	c901_code_nm CODENM,
	c901_code_grp CODEGRP,
	c902_code_nm_alt CODENMALT,
	c901_control_type CONTROLTYP,
	NVL(c901_active_fl,0) ACTFL,
	NVL(c901_lock_fl,0) LOCKFL,
	c901_code_seq_no CODESEQNO,
	c901_created_by CREATEDBY,
	to_char(c901_created_date, GET_RULE_VALUE('DATEFMT', 'DATEFORMAT')) CDATE
	FROM  t901_code_lookup
	WHERE  c901_code_grp=p_code_grp 
	ORDER BY c901_code_seq_no, UPPER(c901_code_nm); 
		
END gm_fch_code_group_list;
PROCEDURE gm_void_code_group(
	p_txn_id IN T901B_CODE_GROUP_MASTER.C901B_CODE_GRP_ID%Type,
	p_userid IN T901B_CODE_GROUP_MASTER.C901B_CREATED_BY%Type
	)
AS
v_cnt NUMBER;
v_code_grp T901B_CODE_GROUP_MASTER.C901B_CODE_GRP%Type;
v_code_nm t901_code_lookup.c901_code_nm%TYPE;
BEGIN
	
	
	
	SELECT C901B_CODE_GRP INTO v_code_grp
		FROM T901B_CODE_GROUP_MASTER 
		WHERE C901B_CODE_GRP_ID=p_txn_id;
	
	v_cnt:=0;
	SELECT 
		COUNT(*) INTO v_cnt 
	FROM 
		t901_code_lookup 
	WHERE 
		c901_active_fl=1 AND c901_code_grp=v_code_grp;
	
	IF v_cnt>0 THEN
		raise_application_error (-20758, '');
	ELSE
		UPDATE 
			T901B_CODE_GROUP_MASTER 
		SET 
			C901B_VOID_FL='Y' 
		WHERE 
			C901B_CODE_GRP_ID = p_txn_id;
			
		UPDATE 
			T901_CODE_LOOKUP 
		SET 
			C901_VOID_FL='Y',
			C901_LAST_UPDATED_DATE = current_date,
			C901_LAST_UPDATED_BY = p_userid
	    WHERE
	    	C901_CODE_GRP=v_code_grp;
	END IF;
		
END gm_void_code_group;
	/******************************************************************
	* Description : Procedure to fetch the code lookup detail for product catalog.
	****************************************************************/
	PROCEDURE gm_fch_codelist_prodcat (
		p_token  			IN 		t101a_user_token.c101a_token_id%TYPE,
		p_langid			IN		NUMBER,
		p_page_no			IN		NUMBER,
	    p_uuid		        IN      t9150_device_info.c9150_device_uuid%TYPE,
		p_codelist_cur		OUT		TYPES.CURSOR_TYPE
	)
	AS
		v_deviceid  t9150_device_info.c9150_device_info_id%TYPE;
		v_date_fmt  VARCHAR2(20);
		v_count	    NUMBER;
		v_page_no   NUMBER;
		v_page_size NUMBER;
	  	v_start     NUMBER;
	    v_end       NUMBER;
	    v_last_rec	t9151_device_sync_dtl.C9151_LAST_SYNC_REC%TYPE; 
	BEGIN
		--get the device info id based on user token
		v_deviceid := get_device_id(p_token, p_uuid);
		v_date_fmt := get_rule_value( 'DATEFMT','DATEFORMAT');
		
		/*below prc check the device's last updated date for codelookup reftype and
		if record exist then it checks is there any codelookup groups updated after the device updated date.
		if updated groups available then add all code groups into v_inlist and return count of v_inlist records*/
		gm_pkg_pdpc_prodcatrpt.gm_fch_device_updates(v_deviceid,p_langid,103107,p_page_no,v_count,v_last_rec); -- 103107 - codelookup reftype
		
		--If there is no data available for device then call below prc to add all catalog related code groups from rules.
		IF v_count = 0 THEN
			gm_pkg_pdpc_prodcatrpt.gm_fch_all_codegroup_tosync;
		END IF;
		
		v_page_no := p_page_no;
		-- get the paging details
		gm_pkg_pdpc_prodcatrpt.gm_fch_paging_dtl('CODE_LOOKUP','PAGESIZE',v_page_no,v_start,v_end,v_page_size);
		
		OPEN p_codelist_cur
			FOR		
			 SELECT COUNT (1) OVER () pagesize, result_count totalsize, v_page_no pageno, rwnum, CEIL (result_count / v_page_size) totalpages
			  , T901.*
			   FROM
			    (
			         SELECT T901.*, ROWNUM rwnum, COUNT (1) OVER () result_count, MAX(DECODE(T901.codeid,v_last_rec,ROWNUM,NULL)) OVER() LASTREC
			           FROM
			            (
							SELECT c901_code_id codeid, c901_code_nm codenm, c901_code_grp codegrp 
							     , c901_active_fl activefl, c901_code_seq_no codeseqno, c902_code_nm_alt codenmalt 
							     , c901_control_type controltype, TO_CHAR(c901_created_date,v_date_fmt) createddate, c901_created_by createdby 
							     , TO_CHAR(c901_last_updated_date,v_date_fmt) lastupdateddate, c901_last_updated_by lastupdatedby, NVL(void_fl,c901_void_fl) voidfl 
							     , c901_lock_fl lockfl 
							  FROM t901_code_lookup, my_temp_prod_cat_info 
							 WHERE c901_code_grp = ref_id 
							   AND (v_count > 0 OR c901_void_fl IS NULL)
							   ORDER BY c901_code_id
			            )
			            T901
			    )
			    T901
			  WHERE RWNUM BETWEEN NVL(LASTREC+1,DECODE(v_last_rec,NULL,v_start,v_start-1)) AND v_end;
		
	END gm_fch_codelist_prodcat;
   /*******************************************************
	 * Description : Procedure to check updates on ProdCatalog mapping Code group
	*******************************************************/	
	PROCEDURE gm_chk_code_lookup_updates (			
        p_rule_id IN t906_rules.C906_RULE_ID%TYPE,			
		p_rule_grpid IN t906_rules.C906_RULE_GRP_ID%TYPE		
	)		
    AS			
	    v_master_data_id t2001_master_data_updates.c2001_master_data_id%TYPE;			
	   			
	   	CURSOR cur_codegrp_detail	---part which will be deleted from group	
			IS	
			 SELECT distinct(c901_code_grp) codegrp	
			   FROM t906_rules t906, t901_code_lookup t901	
			  WHERE t906.c906_rule_value = t901.c901_code_grp	
			    AND t906.c906_rule_id     = p_rule_id  --'SYNC_CODE_GRP'	
			    AND t906.c906_rule_grp_id = p_rule_grpid --'PROD_CAT_SYNC'	
			    AND t906.c906_void_fl IS NULL	
			    --AND t901.c901_active_fl ='1'	
			    AND TRUNC(NVL(t901.c901_last_updated_date,t901.c901_created_date)) = TRUNC(current_date);	
		    	
   BEGIN			
	   		
	FOR var_detail IN cur_codegrp_detail		
	LOOP		
		  UPDATE t2001_master_data_updates	
	         SET c901_action = '103123',   --C901_ACTION--4000412 for voided record  103097 for UPDATE .		
	             c2001_last_updated_date = current_date,
	             c2001_ref_updated_date = current_date
	       WHERE c2001_ref_id = var_detail.codegrp		
	       	 AND c901_ref_type = '103107';  --Code id value for CODE LOOKUP --C901_REF_TYPE.	
	      		
	      IF (SQL%ROWCOUNT = 0)		
	      THEN		
	         SELECT s2001_master_data_updates.NEXTVAL		
	           INTO v_master_data_id		
	           FROM DUAL;		
	         INSERT INTO t2001_master_data_updates		
	                     (c2001_master_data_id, c2001_ref_id, c901_ref_type,		
	                      c901_action, c901_language_id, c2001_ref_updated_date, 
	                      c901_app_id, c2001_last_updated_date, c2001_last_updated_by		
	                     )		
	              VALUES (v_master_data_id, var_detail.codegrp,	103107, --code lookup ref type
	                     103123, 103097, current_date,	-- 103123 Updated, 103097 English	
	                     NULL, current_date, '30301' --JOB USER ID		
	                     );	 
	      END IF;		
	END LOOP;		
   END gm_chk_code_lookup_updates;	
   		
 

   /*******************************************************************************************************
	* Description : Procedure to fetch the code lookup Exclusion detail while syncing the APP Data in Ipad.
	********************************************************************************************************/
	PROCEDURE gm_fch_codelist_exclusion (
		p_token  			IN 		t101a_user_token.c101a_token_id%TYPE,
		p_langid			IN		NUMBER,
		p_page_no			IN		NUMBER,
	    p_uuid		        IN      t9150_device_info.c9150_device_uuid%TYPE,
		p_codelist_cur		OUT		TYPES.CURSOR_TYPE
			)
	AS
		v_deviceid  t9150_device_info.c9150_device_info_id%TYPE;
		v_count	    NUMBER;
		v_page_no   NUMBER;
		v_page_size NUMBER;
	  	v_start     NUMBER;
	    v_end       NUMBER;
	    v_last_rec	t9151_device_sync_dtl.C9151_LAST_SYNC_REC%TYPE; 
	BEGIN
		--get the device info id based on user token
		v_deviceid := get_device_id(p_token, p_uuid);
		
		/*below prc check the device's last updated date for codelookup reftype and
		if record exist then it checks is there any codelookup groups updated after the device updated date.
		if updated groups available then add all code groups into v_inlist and return count of v_inlist records*/
		gm_pkg_pdpc_prodcatrpt.gm_fch_device_updates(v_deviceid,p_langid,26230802,p_page_no,v_count,v_last_rec); -- 26230802 - codelookup exclusion reftype
		
		
		--If there is no data available for device then call below prc to add all catalog related code groups from rules.

		IF v_count = 0 THEN
			gm_pkg_pdpc_prodcatrpt.gm_fch_all_code_excl_tosync;
			
		END IF;
		
		v_page_no := p_page_no;
		-- get the paging details
		gm_pkg_pdpc_prodcatrpt.gm_fch_paging_dtl('CODE_LOOKUP_EXCLUSION','PAGESIZE',v_page_no,v_start,v_end,v_page_size);
		
			OPEN p_codelist_cur
			FOR		
			 SELECT COUNT (1) OVER () pagesize, result_count totalsize, v_page_no pageno, rwnum, CEIL (result_count / v_page_size) totalpages
			  , t901d.*
			   FROM
			    (
			         SELECT t901d.*, ROWNUM rwnum, COUNT (1) OVER () result_count, MAX(DECODE(t901d.lookupexcid,v_last_rec,ROWNUM,NULL)) OVER() LASTREC
			           FROM
			            (
							SELECT t901d.c901d_lookup_exc_id lookupexcid, t901d.c901_code_id codeid, t901d.c1900_company_id cmpid, NVL(void_fl,t901d.c901d_void_fl) voidfl   
							  FROM t901d_code_lkp_company_excln t901d ,t901_code_lookup t901, my_temp_prod_cat_info temp
							 	WHERE t901.c901_code_grp = temp.ref_id 
							   AND t901d.c901_code_id = t901.c901_code_id
							   AND (v_count > 0 OR t901d.c901d_void_fl IS NULL )
							   AND T901.C901_void_fl IS NULL
							   ORDER BY t901d.c901_code_id
			            )
			            t901d
			    )
			    t901d
			  WHERE RWNUM BETWEEN NVL(LASTREC+1,DECODE(v_last_rec,NULL,v_start,v_start-1)) AND v_end;
			  
	END gm_fch_codelist_exclusion;
		
	/***************************************************************************************
		* Description : Procedure to fetch the code lookup Access table detail
	*****************************************************************************************/
	PROCEDURE gm_fch_code_access_list (
		p_token  			IN 		T101A_USER_TOKEN.C101a_Token_Id%TYPE,
		p_langid			IN		NUMBER,
		p_page_no			IN		NUMBER,
	    p_uuid		   		IN      t9150_device_info.c9150_device_uuid%TYPE,
		p_code_acs_list_cur	OUT		TYPES.CURSOR_TYPE
	)
	AS
		v_deviceid  t9150_device_info.c9150_device_info_id%TYPE;
		v_count	    NUMBER;
		v_page_no   NUMBER;
		v_page_size NUMBER;
	  	v_start     NUMBER;
	    v_end       NUMBER;
	    v_last_rec	t9151_device_sync_dtl.C9151_LAST_SYNC_REC%TYPE; 
	BEGIN
		--get the device info id based on user token
		v_deviceid := get_device_id(p_token, p_uuid);
		
		/*below prc check the device's last updated date for codelookup reftype and
		if record exist then it checks is there any codelookup groups updated after the device updated date.
		if updated groups available then add all code groups into v_inlist and return count of v_inlist records*/
		gm_pkg_pdpc_prodcatrpt.gm_fch_device_updates(v_deviceid,p_langid,26230804,p_page_no,v_count,v_last_rec); --26230804  - codelookup exclusion reftype
		
		--If there is no data available for device then call below prc to add all catalog related code groups from rules.

		IF v_count = 0 THEN
			gm_pkg_pdpc_prodcatrpt.gm_fch_all_code_access_tosync;
			
		END IF;
		
		v_page_no := p_page_no;
		-- get the paging details
		gm_pkg_pdpc_prodcatrpt.gm_fch_paging_dtl('CODE_LOOKUP_ACCESS','PAGESIZE',v_page_no,v_start,v_end,v_page_size);
		
			OPEN p_code_acs_list_cur
			FOR		
			 SELECT COUNT (1) OVER () pagesize, result_count totalsize, v_page_no pageno, rwnum, CEIL (result_count / v_page_size) totalpages
			  , t901a.*
			   FROM
			    (
			         SELECT t901a.*, ROWNUM rwnum, COUNT (1) OVER () result_count, MAX(DECODE(t901a.codeaccessid,v_last_rec,ROWNUM,NULL)) OVER() LASTREC
			           FROM
			            (
							SELECT t901a.c901a_code_access_id codeaccessid ,t901a.c901_access_code_id accessid ,
							t901a.c901_code_id codeid,t901a.c1900_company_id cmpid
							 	FROM t901a_lookup_access t901a,t901_code_lookup t901, my_temp_prod_cat_info
							 		WHERE t901.c901_code_grp = ref_id 
							  	 	AND t901a.c901_code_id = t901.c901_code_id
							  	 	AND (v_count > 0 OR t901.c901_void_fl IS NULL )
							  	 	ORDER BY t901a.c901a_code_access_id
			            )
			            t901a
			    )
			    t901a
			  WHERE RWNUM BETWEEN NVL(LASTREC+1,DECODE(v_last_rec,NULL,v_start,v_start-1)) AND v_end;
			  
	END gm_fch_code_access_list;
	
	/*************************************************************************************************
	* Description : Procedure to fetch the security events detail for product catalog.
  *************************************************************************************************/
PROCEDURE gm_fch_security_event_list (
	p_token  			IN 		t101a_user_token.c101a_token_id%TYPE,
	p_langid			IN		NUMBER,
	p_page_no			IN		NUMBER,
	p_uuid		                IN              t9150_device_info.c9150_device_uuid%TYPE,
	p_partyid                       IN              T101_USER.C101_PARTY_ID%TYPE,
	p_codelist_cur		        OUT		TYPES.CURSOR_TYPE
)
AS
	v_deviceid  t9150_device_info.c9150_device_info_id%TYPE;
	v_date_fmt  VARCHAR2(20);
	v_count	    NUMBER;
	v_page_no   NUMBER;
	v_page_size NUMBER;
	v_start     NUMBER;
        v_end       NUMBER;
        v_last_rec	t9151_device_sync_dtl.C9151_LAST_SYNC_REC%TYPE; 
BEGIN
	--get the device info id based on user token
	v_deviceid := get_device_id(p_token, p_uuid);
	v_date_fmt := get_rule_value( 'DATEFMT','DATEFORMAT');
	
	/*below prc check the device's last updated date for codelookup reftype and
	if record exist then it checks is there any codelookup groups updated after the device updated date.
	if updated groups available then add all code groups into v_inlist and return count of v_inlist records*/
	gm_pkg_pdpc_prodcatrpt.gm_fch_device_updates(v_deviceid,p_langid,7000443,p_page_no,v_count,v_last_rec); -- 7000443 - security event reftype
	
	--If there is no data available for device then call below prc to add all catalog related code groups from rules.
	IF v_count = 0 THEN
		gm_pkg_pdpc_prodcatrpt.gm_fch_all_secu_event_tosync;
	END IF;
	
	v_page_no := p_page_no;
	-- get the paging details
	gm_pkg_pdpc_prodcatrpt.gm_fch_paging_dtl('SECURITY_EVENT','PAGESIZE',v_page_no,v_start,v_end,v_page_size);
	
	OPEN p_codelist_cur
		FOR		
		 SELECT COUNT (1) OVER () pagesize, result_count totalsize, v_page_no pageno, rwnum, CEIL (result_count / v_page_size) totalpages
		  , T1530.*
		   FROM
		    (
			 SELECT T1530.*, ROWNUM rwnum, COUNT (1) OVER () result_count, MAX(DECODE(ROWNUM,v_last_rec,ROWNUM,NULL)) OVER() LASTREC
			   FROM
			    (
				  SELECT   T.EVENTNAME EVENTNAME
		                         , T.TAGNAME TAGNAME
		                         , T.PAGENAME PAGENAME
		                         , T.UPDATEACCESSFLG UPDATEACCESSFLG
		                         , T.READACCESSFLG READACCESSFLG
		                         , T.VOIDACCESSFLG VOIDACCESSFLG
		                         , DECODE(T.ACCESSVOIDFLG,'Y','Y',NULL) ACCESSVOIDFLG
		                         , DECODE(T.GROUPMAPPINGVOIDFLG,'Y','Y',NULL) GROUPMAPPINGVOIDFLG
		                         , DECODE(T.RULESVOIDFLG,'Y','Y',NULL) RULESVOIDFLG
				    FROM
				      (SELECT  t906_pages.c906_rule_grp_id EVENTNAME
					      , t906_pages.C906_RULE_ID TAGNAME
					      , t906_pages.C906_RULE_VALUE PAGENAME
					      , MAX(NVL(t1530.c1530_update_access_fl,'N')) UPDATEACCESSFLG
					      , MAX(NVL(t1530.c1530_read_access_fl,'N')) READACCESSFLG
					      , MAX(NVL(t1530.c1530_void_access_fl,'N')) VOIDACCESSFLG
					      , MIN(NVL(t1530.c1530_void_fl,'N')) ACCESSVOIDFLG 
					      , MIN(NVL(t1501.c1501_void_fl,'N')) GROUPMAPPINGVOIDFLG
					      , MIN(NVL(t906_pages.c906_void_fl,'N')) RULESVOIDFLG
					 FROM  t906_rules t906_mobile
					      , t906_rules t906_pages
					      , t1530_access t1530 
					      , t1501_group_mapping t1501
					      , my_temp_prod_cat_info mytem
					WHERE t906_mobile.c906_rule_grp_id = 'MOBILESECURITYEVENTS'
					  AND t906_mobile.c906_rule_id  = t906_pages.c906_rule_grp_id
					  AND t906_pages.c906_rule_grp_id = t1530.c1520_function_id
					  AND t1530.c1500_group_id = t1501.c1500_group_id
					  AND t1501.c101_mapped_party_id =  p_partyid   
					  AND T1530.c1500_group_id  =  mytem.ref_id
					GROUP BY  t906_pages.c906_rule_grp_id
						 , t906_pages.C906_RULE_ID
						 , t906_pages.C906_RULE_VALUE 
				      )T
			    )
			    T1530
		    )
		    T1530
		  WHERE RWNUM BETWEEN NVL(LASTREC+1,DECODE(v_last_rec,NULL,v_start,v_start-1)) AND v_end;
	
END gm_fch_security_event_list;

/***************************************************************************************************************
	 * Description : procedure to fetch code id and check the code id is present in the 
	 * particular column or not then the corresponding codeid belongs to codegroup and codename 
	 * also finded by using the details
	 * 
	**************************************************************************************************************/
	PROCEDURE gm_fch_code_lookup_excln_dtls (
      p_code_id           IN        t901_code_lookup.c901_code_id%TYPE,
      p_code_dtls_cur       OUT       Types.cursor_type,    
      p_excln_incln_cur     OUT       Types.cursor_type
      )
      AS 
      
      v_incl_cnt  NUMBER;
      v_exncl_cnt NUMBER;
      v_query  VARCHAR2(4000);
      v_id_count NUMBER;
     
      
      BEGIN
      
      	
		SELECT count(*)
            INTO v_id_count
          FROM t901_code_lookup 
        WHERE c901_code_id = p_code_id
          AND c901_void_fl IS NULL;
         
          IF v_id_count = 0        
            THEN
              GM_RAISE_APPLICATION_ERROR('-20999','392',p_code_id);  
             
          ELSE
      
      
      
      
			      SELECT count(*) INTO v_incl_cnt
			                                FROM t1900_company
			                                  WHERE c1900_company_id NOT IN
			                                    (SELECT c1900_company_id
			                                    FROM t901d_code_lkp_company_excln
			                                    WHERE c901_code_id = p_code_id
			                                    AND c901d_void_fl IS  NULL)
			                                    AND C1900_VOID_FL IS NULL;
			
			                                    
			                  SELECT count(*) INTO v_exncl_cnt
			                                  FROM t901d_code_lkp_company_excln
			                                  WHERE c901_code_id = p_code_id
			                                  AND c901d_void_fl IS NULL;
			                                  
			                                  
			                v_query := 'SELECT incl.inc_comp_nm inc_comp_nm,
			                                    exncl.excl_comp_nm excl_comp_nm
			                                    FROM
			                            (SELECT rownum inum,
			                                         c1900_company_name inc_comp_nm
			                                         FROM t1900_company
			                                        WHERE c1900_company_id NOT IN
			                            (SELECT c1900_company_id
			                                         FROM t901d_code_lkp_company_excln
			                                        WHERE ' ;
			                                                                                                               
			                                                                                                                    
			                v_query :=           v_query || ' c901_code_id = '|| p_code_id || ''; 
			                
			                v_query :=           v_query || ' AND c901d_void_fl IS NULL
			                                                                         )
			                                                                        )incl,
			                                        (SELECT rownum enum,
			                                         get_company_name(c1900_company_id) excl_comp_nm
			                                         FROM t901d_code_lkp_company_excln
			                                         WHERE ' ;                                                                              
			                                                   
			                
			                v_query :=  v_query || ' c901_code_id = ' ||p_code_id ||'';
			                                                                                                                   
			                v_query := v_query || '    AND c901d_void_fl IS NULL
			                                                                   )exncl';
			
			                IF (v_incl_cnt <= v_exncl_cnt)
			                THEN
			                                v_query := v_query || ' WHERE incl.inum (+) = exncl.enum';
			                ELSE
			                                v_query :=  v_query || ' WHERE incl.inum  = exncl.enum(+)';
			                END IF;
			
			               
			                OPEN p_code_dtls_cur
			                FOR
			                                SELECT c901_code_id codeid,
			                                                   c901_code_nm codenm,
			                                                   c901_code_grp codegrp
			                                  FROM t901_code_lookup
			                                WHERE c901_code_id = p_code_id
			                                   AND c901_void_fl  IS NULL;
			                                   
			
			                                
			                                   OPEN p_excln_incln_cur           FOR v_query  ;
			                                   
			END IF;
     END gm_fch_code_lookup_excln_dtls;    
 /*************************************************************************************************
	 * Description : procedure to save the exclude and include using code id for multicompany
	***********************************************************************************************/    
         
     PROCEDURE gm_sav_code_lookup_excln(
	    p_codeidstr       IN        CLOB,
	    p_companystr      IN        CLOB,
	    p_type            IN        t901_code_lookup.c901_code_id%TYPE,
	    p_userid          IN        VARCHAR2,
	    p_out_code_grp		  OUT	CLOB
	 )
	 
	 AS
	 v_codeidstr       CLOB := p_codeidstr;
	 v_code_id         NUMBER;	 
	 v_type            VARCHAR2 (4000) := p_type;
	 v_invalid_codeid  T901_code_lookup.c901_code_id%TYPE;
	 v_id_count        NUMBER;
	 v_code_grp        T901_code_lookup.c901_code_grp%TYPE;
	 BEGIN
			 WHILE INSTR (v_codeidstr, ',') <> 0 
	 LOOP
	    v_code_id := TO_NUMBER(SUBSTR (v_codeidstr, 1, INSTR (v_codeidstr, ',') - 1));
		v_codeidstr	:= SUBSTR (v_codeidstr, INSTR (v_codeidstr, ',') + 1);
		
		SELECT count(*)
            INTO v_id_count
          FROM t901_code_lookup 
        WHERE c901_code_id = v_code_id
          AND c901_void_fl IS NULL;
         
          IF v_id_count = 0        
            THEN
              v_invalid_codeid :=  v_code_id;
              GM_RAISE_APPLICATION_ERROR('-20999','392',v_invalid_codeid);  
             
          ELSE
          -- to get the code group
          	select c901_code_grp INTO v_code_grp from t901_code_lookup where c901_code_id = v_code_id;
          	p_out_code_grp := p_out_code_grp || v_code_grp || ',';
            IF v_type = 106060
            THEN
            gm_pkg_cm_code_lookup.gm_save_code_excln(v_code_id,p_companystr,p_userid);            
            ELSE 
            gm_pkg_cm_code_lookup.gm_save_code_incln(v_code_id,p_companystr,p_userid);      
         END IF;
             END IF;
     END LOOP;
  END gm_sav_code_lookup_excln;
  /*******************************************************************************************************************
	 * Description : procedure to exclude in the code lookup exclusion table,check already having codeid means unvoided & 
	 * updated for the particular company newly added codeid means inserted for the particular company 
	*******************************************************************************************************************/    
  PROCEDURE gm_save_code_excln(
            p_code_id     IN t901_code_lookup.c901_code_id%TYPE,
            p_companystr  IN CLOB,
            p_userid     IN VARCHAR2            
  )  
  AS
            v_companystr    CLOB := p_companystr;
            v_company_id      NUMBER;            
            v_voided_cnt           NUMBER;
            v_counts           NUMBER;
            
  BEGIN
  WHILE INSTR (v_companystr, ',') <> 0
  LOOP
		v_company_id := TO_NUMBER(SUBSTR (v_companystr, 1, INSTR (v_companystr, ',') - 1));
		v_companystr	:= SUBSTR (v_companystr, INSTR (v_companystr, ',') + 1);
		
		 
	 SELECT COUNT(*)
      INTO  v_voided_cnt
      FROM t901d_code_lkp_company_excln
     WHERE c901_code_id   = p_code_id
       AND c1900_company_id = v_company_id
       AND c901d_void_fl   IS NOT NULL;
       
       IF (v_voided_cnt = 0)
       THEN
	       SELECT COUNT(*)
	     	 INTO v_counts
	     	 FROM t901d_code_lkp_company_excln
	     	WHERE c901_code_id   = p_code_id
	       	  AND c1900_company_id = v_company_id
	       	  AND c901d_void_fl IS NULL;
     	END IF;
     	
       IF (v_counts = 0)
       THEN
	       INSERT INTO t901d_code_lkp_company_excln(C901D_LOOKUP_EXC_ID,C901_CODE_ID,C1900_COMPANY_ID,C901D_UPDATED_DATE,C901D_UPDATED_BY)
	       VALUES (S901D_CODE_LKP_COMPANY_EXCLN.nextval,p_code_id,v_company_id,CURRENT_DATE,p_userid);

       ELSE 
	       update t901d_code_lkp_company_excln set
	              c901d_void_fl = ''
	        where c901_code_id = p_code_id
	          AND c1900_company_id = v_company_id;
       END IF; 
   END LOOP;
 END gm_save_code_excln;
 /*************************************************************************************************************************************************
	 * Description : procedure to include in the code lookup exclusion table already having codeid,if the void flag is null means  
	 * existing codeid voided for the particular company name or the existing codeid becomes unvoided means updated for the particular company name 
	************************************************************************************************************************************************/
 PROCEDURE gm_save_code_incln(
 p_code_id     IN t901_code_lookup.c901_code_id%TYPE,
 p_companystr  IN CLOB,
 p_userid     IN VARCHAR2
 )
 AS
 v_companystr     CLOB := p_companystr; 
 v_company_id     NUMBER;
 v_count	      NUMBER;
 
 BEGIN
 WHILE INSTR (v_companystr, ',') <> 0
 LOOP
        v_company_id := TO_NUMBER(SUBSTR (v_companystr, 1, INSTR (v_companystr, ',') - 1));
		v_companystr:= SUBSTR (v_companystr, INSTR (v_companystr, ',') + 1);
		
 SELECT COUNT(*)
     INTO v_count
     FROM t901d_code_lkp_company_excln
     WHERE c901_code_id   = p_code_id
       AND c1900_company_id = v_company_id
       AND c901d_void_fl   IS NULL;
       
       
        
       
       IF v_count IS NOT NULL
       THEN
       		UPDATE T901D_CODE_LKP_COMPANY_EXCLN  
       		   SET  
       				c901d_void_fl ='Y',
       				C901D_UPDATED_DATE = CURRENT_DATE,
       				C901D_UPDATED_BY = p_userid       
       		  WHERE C901_CODE_ID   = p_code_id
       			AND C1900_COMPANY_ID = v_company_id;      
 	
    END IF;
    
   END LOOP;
 END gm_save_code_incln;
/*************************************************************************************************************************************************
	 * Description : procedure to trigger App updates  
	 * When inserting any new code groups or Updating new code groups update this table for Updates
	 * SO check the Lookup type and Updates is fired
	 * This block will be executed when adding or updating any code Groups related to App Sync
	************************************************************************************************************************************************/ 
 PROCEDURE GM_SAV_CODE_LKP_IPAD
 AS
 CURSOR cur_t901f
		IS
		 	SELECT C901F_LOOKUP_IPAD_ID LKPID,C901F_CODE_GRP  CODEGRP , C901_LOOKUP_TYPE LKPTYPE , DECODE(C901F_VOID_FL, 'Y', '4000412','103123') ACT
		 	FROM T901F_CODE_LKP_IPAD_MAP
		 	WHERE C901F_UPDATE_FL IS NULL;
		BEGIN
		FOR v_cur IN cur_t901f
			LOOP
					GM_PKG_PDPC_PRODCATTXN.GM_SAV_MASTER_DATA_UPD (v_cur.CODEGRP, v_cur.LKPTYPE,v_cur.ACT,
                                                       '103097', '103100',
                                                       '30301',
                                                       NULL,'Y',NULL );
		 		
		 		UPDATE T901F_CODE_LKP_IPAD_MAP 
		 		SET C901F_UPDATE_FL ='Y',C901F_UPDATE_FL_DATE = CURRENT_DATE
		 		WHERE C901F_LOOKUP_IPAD_ID = v_cur.LKPID;
		 		
			END LOOP;
 END GM_SAV_CODE_LKP_IPAD;
 /*************************************************************************************************************************************************
	 * Description : procedure to Fetch Code Group Details
************************************************************************************************************************************************/ 
 
PROCEDURE gm_fch_code_group_id(
 p_code_grp      IN       t901b_code_group_master.C901B_CODE_GRP%TYPE,
 p_out_data OUT Types.cursor_type)
 AS
  begin
	   OPEN p_out_data
	      FOR
	        SELECT C901B_CODE_GRP_ID CODEGRPID ,
	        C901B_CODE_GRP CODEGROUP ,
	        C901B_CODE_DESCRIPTION CODEDESC ,
			C901B_CREATED_BY CREATEDBY,
			to_char(C901B_CREATED_DATE, GET_RULE_VALUE('DATEFMT', 'DATEFORMAT')) CREATEDDATE
			FROM  t901b_code_group_master
			WHERE C901B_CODE_GRP = decode (p_code_grp,'0', C901B_CODE_GRP, p_code_grp)
			AND C901B_VOID_FL IS NULL;
	      
END gm_fch_code_group_id;
 
END gm_pkg_cm_code_lookup;
/