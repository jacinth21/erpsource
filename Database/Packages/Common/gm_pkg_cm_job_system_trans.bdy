/* Formatted on 2009/02/16 12:30 (Formatter Plus v4.8.0) */
-- @"C:\Database\Packages\Common\gm_pkg_cm_job_system_trans.bdy"
CREATE OR REPLACE
PACKAGE body gm_pkg_cm_job_system_trans
IS
  /***************************************************************
  * Description : Procedure is used to save details of job
  * Author   : Mihir
  ****************************************************************/
PROCEDURE gm_sav_job_activity(
    p_jobnm     IN t9030_job_activity.C9030_JOB_NAME%TYPE,
    p_srcnm     IN t9030_job_activity.C9030_SOURCE_NAME%TYPE,
    p_srctp     IN t9030_job_activity.C901_SOURCE_TYPE%TYPE,
    p_srcext    IN t9030_job_activity.C9030_SOURCE_EXTENSION%TYPE,
    p_srccnt    IN t9030_job_activity.C9030_SOURCE_COUNT%TYPE,
    p_passcnt   IN t9030_job_activity.C9030_PASS_COUNT%TYPE,
    p_failcnt   IN t9030_job_activity.C9030_FAIL_COUNT%TYPE,
    p_udi_msgid IN t9030_job_activity.C9030_UDI_MESSAGE_ID%TYPE,
    p_userid    IN t9030_job_activity.C9030_LAST_UPDATED_BY%TYPE,
    p_out_jobactid     OUT t9030_job_activity.C9030_JOB_ACTIVITY_ID%TYPE,
    p_out_jobid  IN   OUT t9030_job_activity.C9030_JOB_ID%TYPE
  )
AS
BEGIN
select s9030_job_activity.nextval INTO p_out_jobactid FROM DUAL;
 IF p_out_jobid IS  NULL
 THEN
 select s9030_job_id.nextval INTO p_out_jobid FROM DUAL;
 END IF;

  INSERT
  INTO
    T9030_JOB_ACTIVITY
    (
      C9030_JOB_ACTIVITY_ID,
      C9030_JOB_ID,
      C9030_JOB_NAME,
      C9030_SOURCE_NAME,
      C901_SOURCE_TYPE,
      C9030_SOURCE_EXTENSION,
      C9030_SOURCE_COUNT,
      C9030_PASS_COUNT,
      C9030_FAIL_COUNT,
      C9030_UDI_MESSAGE_ID,
      C9030_LAST_UPDATED_BY,
      C9030_LAST_UPDATED_DATE
    )
    VALUES
    (
      p_out_jobactid,
      p_out_jobid,
      p_jobnm,
      p_srcnm,
      p_srctp,
      p_srcext,
      p_srccnt,
      p_passcnt,
      p_failcnt,
      p_udi_msgid,
      p_userid,
      SYSDATE
    );
END gm_sav_job_activity;
/***************************************************************
* Description : Procedure is used to save details of job source system
* Author   : Mihir
****************************************************************/
PROCEDURE gm_sav_job_src_system(
    p_job_actid IN t9031_job_source_system.C9030_JOB_ACTIVITY_ID%TYPE,
    p_attrtp    IN t9031_job_source_system.C9031_ATTRIBUTE_TYPE%TYPE,
    p_attrval   IN t9031_job_source_system.C9031_ATTRIBUTE_VALUE%TYPE,
    p_reftp     IN t9031_job_source_system.C901_REF_TYPE%TYPE,
    p_refid     IN t9031_job_source_system.C9031_REF_ID%TYPE,
    p_userid    IN t9031_job_source_system.C9031_REF_ID%TYPE
  )
AS
BEGIN
  INSERT
  INTO
    T9031_JOB_SOURCE_SYSTEM
    (
      C9031_SOURCE_SYSTEM_KEY_ID,
      C9030_JOB_ACTIVITY_ID,
      C9031_ATTRIBUTE_TYPE,
      C9031_ATTRIBUTE_VALUE,
      C901_REF_TYPE,
      C9031_REF_ID,
      C9031_LAST_UPDATED_BY,
      C9031_LAST_UPDATED_DATE
    )
    VALUES
    (
      s9031_job_source_system.nextval,
      p_job_actid,
      p_attrtp,
      p_attrval,
      p_reftp,
      p_refid,
      p_userid,
      SYSDATE
    );
END gm_sav_job_src_system;

/***************************************************************
  * Description : Procedure is used to save details of Screen Info
  * Author   : Mihir Patel
  ****************************************************************/
PROCEDURE gm_sav_job_screen_info(
    p_screen_tp	    IN T9032_JOB_SCREEN_INFO.C901_SCREEN_TYPE%TYPE,
    p_screen_cat    IN T9032_JOB_SCREEN_INFO.C901_SCREEN_CATEGORY%TYPE,
    p_screen_nm	    IN T9032_JOB_SCREEN_INFO.C9032_SCREEN_NAME%TYPE,
    p_screen_desc	  IN T9032_JOB_SCREEN_INFO.C9032_SCREEN_DESCRIPTION%TYPE,
    p_corrective_action       IN T9032_JOB_SCREEN_INFO.C9032_CORRECTIVE_ACTION%TYPE,
    p_corrective_description  IN T9032_JOB_SCREEN_INFO.C9032_CORRECTIVE_DESCRIPTION%TYPE,
    p_additional_info         IN T9032_JOB_SCREEN_INFO.C9032_ADDITIONAL_INFO%TYPE,
    p_userid	    IN T9032_JOB_SCREEN_INFO.C9032_LAST_UPDATED_BY%TYPE,
    p_out_jobsreenid IN OUT T9032_JOB_SCREEN_INFO.C9032_JOB_SCREEN_ID%TYPE,
    p_out_screenid IN OUT T9032_JOB_SCREEN_INFO.C9032_SCREEN_ID%TYPE
  )
  AS
  BEGIN
  select s9032_job_screen_info.nextval INTO p_out_jobsreenid FROM DUAL;
 IF p_out_screenid IS  NULL
 THEN
 select s9032_screen_id.nextval INTO p_out_screenid FROM DUAL;
 END IF;

 INSERT
   INTO
    T9032_JOB_SCREEN_INFO
    (
        C9032_JOB_SCREEN_ID , C9032_SCREEN_ID
      , C901_SCREEN_TYPE , C901_SCREEN_CATEGORY
      , C9032_SCREEN_NAME , C9032_SCREEN_DESCRIPTION
      , C9032_CORRECTIVE_ACTION , C9032_CORRECTIVE_DESCRIPTION
      , C9032_ADDITIONAL_INFO , C9032_LAST_UPDATED_BY
      , C9032_LAST_UPDATED_DATE
    )
    VALUES
    (
        p_out_jobsreenid , p_out_screenid
      , p_screen_tp , p_screen_cat
      , p_screen_nm , p_screen_desc
      , p_corrective_action , p_corrective_description
      , p_additional_info , p_userid
      , SYSDATE
    ) ;

  END gm_sav_job_screen_info; 
  /***************************************************************
  * Description : Procedure is used to save details of error event
  * Author   : Joe P Kumar
  ****************************************************************/
PROCEDURE gm_sav_job_error_event(
    p_error_date    IN T9033_JOB_ERROR_EVENT.C9033_ERROR_DATE%TYPE,
    p_screen_id	    IN T9033_JOB_ERROR_EVENT.C9032_JOB_SCREEN_ID%TYPE,
    p_activity_id   IN T9033_JOB_ERROR_EVENT.C9030_JOB_ACTIVITY_ID%TYPE,
    p_reftype       IN T9033_JOB_ERROR_EVENT.C901_REF_TYPE%TYPE,
    p_refid	        IN T9033_JOB_ERROR_EVENT.C9033_REF_ID%TYPE,
    p_status        IN T9033_JOB_ERROR_EVENT.C901_STATUS%TYPE
  )
  AS
  BEGIN
  INSERT INTO T9033_JOB_ERROR_EVENT (
	C9033_JOB_ERROR_EVENT_ID
	,C9033_ERROR_DATE        
	,C9032_JOB_SCREEN_ID     
	,C9030_JOB_ACTIVITY_ID   
	,C901_REF_TYPE           
	,C9033_REF_ID  
    ,C901_STATUS
) VALUES (
	S9033_JOB_ERROR_EVENT_ID.nextval
	,p_error_date  
	,p_screen_id	  
	,p_activity_id 
	,p_reftype     
	,p_refid
    ,p_status
);

  END gm_sav_job_error_event; 

 /***************************************************************
  * Description : Procedure is used to save details of attribute count
  * Author   : Joe P Kumar
  ****************************************************************/
 PROCEDURE gm_sav_attribute_count_dtls(
    p_attrb_count_wip_id    IN T2071A_UDI_ATTRIBUTE_COUNT_DTL.C2071_UDI_ATTRB_COUNT_WIP_ID%TYPE,
    p_part_number     IN T2071A_UDI_ATTRIBUTE_COUNT_DTL.C205_PART_NUMBER_ID%TYPE,
    p_attribute_type      IN T2071A_UDI_ATTRIBUTE_COUNT_DTL.C901_ATTRIBUTE_TYPE%TYPE,
    p_actual_count       IN T2071A_UDI_ATTRIBUTE_COUNT_DTL.C2071A_AVAILABLE_COUNT%TYPE,
    p_needed_count       IN T2071A_UDI_ATTRIBUTE_COUNT_DTL.C2071A_NEEDED_COUNT%TYPE,
    p_last_updated_by     IN T2071A_UDI_ATTRIBUTE_COUNT_DTL.C2071A_LAST_UPDATED_BY%TYPE
  )
  AS
  BEGIN

  /*MERGE INTO T2071A_UDI_ATTRIBUTE_COUNT_DTL
  USING dual
  ON (C2071_UDI_ATTRB_COUNT_WIP_ID = p_attrb_count_wip_id
  AND C901_ATTRIBUTE_TYPE = p_attribute_type)
  WHEN MATCHED THEN
  UPDATE SET C205_PART_NUMBER_ID = p_part_number
  , C2071A_AVAILABLE_COUNT = p_actual_count
  , C2071A_NEEDED_COUNT = p_needed_count
  , C2071A_LAST_UPDATED_BY = p_last_updated_by
  , C2071A_LAST_UPDATED_DATE = SYSDATE
  WHEN NOT MATCHED THEN
  INSERT (C2071A_UDI_ATTRB_COUNT_DTL_ID
  , C2071_UDI_ATTRB_COUNT_WIP_ID
  , C205_PART_NUMBER_ID
  , C901_ATTRIBUTE_TYPE
  , C2071A_AVAILABLE_COUNT
  , C2071A_NEEDED_COUNT
  , C2071A_LAST_UPDATED_BY
  , C2071A_LAST_UPDATED_DATE)
  VALUES (S2071A_UDI_ATTRB_COUNT_DTL_ID.nextval
  , p_attrb_count_wip_id
  , p_part_number
  , p_attribute_type
  , p_actual_count
  , p_needed_count
  , p_last_updated_by
  ,sysdate);
  */

  INSERT INTO T2071A_UDI_ATTRIBUTE_COUNT_DTL (C2071A_UDI_ATTRB_COUNT_DTL_ID
  , C2071_UDI_ATTRB_COUNT_WIP_ID
  , C205_PART_NUMBER_ID
  , C901_ATTRIBUTE_TYPE
  , C2071A_AVAILABLE_COUNT
  , C2071A_NEEDED_COUNT
  , C2071A_LAST_UPDATED_BY
  , C2071A_LAST_UPDATED_DATE)
  VALUES (S2071A_UDI_ATTRB_COUNT_DTL_ID.nextval
  , p_attrb_count_wip_id
  , p_part_number
  , p_attribute_type
  , p_actual_count
  , p_needed_count
  , p_last_updated_by
  ,sysdate);

  END gm_sav_attribute_count_dtls;
/***************************************************************
  * Description : Procedure is used to save voided records in UDI Data Load
  * Author   : Mihir Patel
  ****************************************************************/
PROCEDURE gm_sav_udi_data_load_void_log(
	p_part_number IN T2070_UDI_DATA_LOAD_LOG.C205_PART_NUMBER_ID%TYPE,
	p_di_number IN T2070_UDI_DATA_LOAD_LOG.C2060_DI_NUMBER%TYPE,
	p_att_type IN T2070_UDI_DATA_LOAD_LOG.C901_ATTRIBUTE_TYPE%TYPE,
	p_att_value IN T2070_UDI_DATA_LOAD_LOG.C2070_ATTRIBUTE_VALUE%TYPE,
	p_void_fl IN T2070_UDI_DATA_LOAD_LOG.C2070_VOID_FL%TYPE,
	p_user_id IN T2070_UDI_DATA_LOAD_LOG.C2070_CREATED_BY%TYPE
  )
  AS
  BEGIN
  INSERT INTO T2070_UDI_DATA_LOAD_LOG (C2070_UDI_DATA_LOAD_LOG_ID
	,C205_PART_NUMBER_ID
	,C2060_DI_NUMBER
	,C901_ATTRIBUTE_TYPE
	,C2070_ATTRIBUTE_VALUE
	,C2070_VOID_FL
	,C2070_CREATED_BY
	,C2070_CREATED_DATE
	)
	values (s2070_UDI_DATA_LOAD_LOG.NEXTVAL
	, p_part_number
	, p_di_number
	, p_att_type
	, p_att_value
	, p_void_fl
	, p_user_id
	, SYSDATE);

  END gm_sav_udi_data_load_void_log; 
END gm_pkg_cm_job_system_trans;
/
