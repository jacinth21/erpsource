--@"C:\database\Packages\Common\gm_pkg_cm_usage_track.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_cm_usage_track
IS
/*******************************************************
	* Description : This procedure is used to capture who is viewing the screen with date and time.
	*               So, we can get the below report
	*                1) Number of time the user using the screen.
	*                2) Repeated user viewing the screen
	*                3) New user view the screen.
	*                4) When user last used the screen.
	* Author		: RanjithS
	*******************************************************/
	
	PROCEDURE Gm_sav_usage_track(	 
	   
		p_user_id         IN	   T9035_SCREEN_USAGE_TRACK.C9035_SCREEN_USAGE_TRACK_ID%TYPE,
		p_screen_nm		  IN	   T9035_SCREEN_USAGE_TRACK.C9035_SCREEN_NM%TYPE,
		p_trans_id	      IN       T9035_SCREEN_USAGE_TRACK.C9035_TRANSACTION_ID%TYPE
	 )  
	as	
BEGIN

    INSERT
    INTO T9035_SCREEN_USAGE_TRACK
     (      
          C9035_SCREEN_USAGE_TRACK_ID,
          C101_USER_ID,
          C9035_SCREEN_NM,
          C9035_TRANSACTION_ID,
          C9035_LAST_UPDATED_DATE_UTC 
                        
       )
         VALUES
      ( 
        S9035_SCREEN_USAGE_TRACK_ID.NEXTVAL,
        p_user_id, 
        p_screen_nm,         
        p_trans_id,
        SYSTIMESTAMP   
       
       );              
      
    
	END Gm_sav_usage_track;
	
	END gm_pkg_cm_usage_track;
/