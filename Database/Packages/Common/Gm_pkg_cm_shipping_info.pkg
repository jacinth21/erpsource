/* Formatted on 2010/12/15 11:40 (Formatter Plus v4.8.0) */
-- @"c:\database\packages\common\gm_pkg_cm_shipping_info.pkg"

CREATE OR REPLACE PACKAGE gm_pkg_cm_shipping_info
IS
/******************************************************************
   * Description : Procedure to get the address address when clicked on address lookup button
   ****************************************************************/
	FUNCTION get_address (
		p_shipto	 IN   t907_shipping_info.c901_ship_to%TYPE
	  , p_shiptoid	 IN   t907_shipping_info.c907_ship_to_id%TYPE
	  , p_add_len    IN   NUMBER DEFAULT 50
	)
		RETURN VARCHAR2;

/******************************************************************
   * Description : Procedure to get the ShipID when refid and source is passed
   ****************************************************************/
	FUNCTION get_shipping_id (
		p_refid    IN	t907_shipping_info.c907_ref_id%TYPE
	  , p_source   IN	t907_shipping_info.c901_source%TYPE
	  , p_action   IN	VARCHAR2
	)
		RETURN VARCHAR2;

/******************************************************************
   * Description : Procedure to get the address info for a specific party
   ****************************************************************/
	PROCEDURE gm_fch_ship_details (
		p_refid 		IN		 t907_shipping_info.c907_ref_id%TYPE
	  , p_source		IN		 t907_shipping_info.c901_source%TYPE
	  , p_action		IN		 VARCHAR2
	  , p_shipinfocur	OUT 	 TYPES.cursor_type
	);

	 /******************************************************************
	* Description : Procedure to get the address info for a specific party based on the shipid
	****************************************************************/
	PROCEDURE gm_fch_ship_details_byid (
		p_shipid		IN		 t907_shipping_info.c907_shipping_id%TYPE
	  , p_shipinfocur	OUT 	 TYPES.cursor_type
	);

	/******************************************************************
	* Description : Procedure to get the shipping info	for mailing
	****************************************************************/
	PROCEDURE gm_fch_shipped_today (
		p_mailshipdtl	OUT   TYPES.cursor_type
	  , p_ruledtl		OUT   TYPES.cursor_type
	);

/******************************************************************
	 * Description : function to get ship address
	 ****************************************************************/
	FUNCTION get_ship_add (
		p_refid    IN	t501_order.c501_order_id%TYPE
	  , p_source   IN	t907_shipping_info.c901_source%TYPE
	)
		RETURN VARCHAR2;
/******************************************************************
	 * Description : function to get shipto
	 * Function is altered for PMT-6402 to handle ship Address changes with BBA and US. 
	 ****************************************************************/
	FUNCTION get_ship_add (
      p_refid    IN   t501_order.c501_order_id%TYPE,
      p_source   IN   t907_shipping_info.c901_source%TYPE,
      p_swap_attn_to IN VARCHAR2
   )RETURN VARCHAR2;
/******************************************************************
	 * Description : function to get shipto
	 ****************************************************************/
	FUNCTION get_shipto (
		p_refid    IN	t501_order.c501_order_id%TYPE
	  , p_source   IN	t907_shipping_info.c901_source%TYPE
	)
		RETURN VARCHAR2;

/******************************************************************
	 * Description : function to get shipto_id
	 ****************************************************************/
	FUNCTION get_shipto_id (
		p_refid    IN	t501_order.c501_order_id%TYPE
	  , p_source   IN	t907_shipping_info.c901_source%TYPE
	)
		RETURN VARCHAR2;

/******************************************************************
	 * Description : function to get Tracking no
	 ****************************************************************/
	FUNCTION get_track_no (
		p_refid    IN	t501_order.c501_order_id%TYPE
	  , p_source   IN	t907_shipping_info.c901_source%TYPE
	)
		RETURN VARCHAR2;

/******************************************************************
	 * Description : function to get Tracking no
	 ****************************************************************/
	FUNCTION get_carrier (
		p_refid    IN	t501_order.c501_order_id%TYPE
	  , p_source   IN	t907_shipping_info.c901_source%TYPE
	)
		RETURN VARCHAR2;

/******************************************************************
	 * Description : function to get Tracking no
	 ****************************************************************/
	FUNCTION get_ship_mode (
		p_refid    IN	t501_order.c501_order_id%TYPE
	  , p_source   IN	t907_shipping_info.c901_source%TYPE
	)
		RETURN VARCHAR2;

/******************************************************************
	 * Description : function to get Tracking no
	 ****************************************************************/
	FUNCTION get_completed_by (
		p_refid    IN	t501_order.c501_order_id%TYPE
	  , p_source   IN	t907_shipping_info.c901_source%TYPE
	  , p_status   IN	t907_shipping_info.c907_status_fl%TYPE
	)
		RETURN VARCHAR2;

	 /******************************************************************
	* Description : Procedure to get the unique tracking numbers
	****************************************************************/
	PROCEDURE gm_fch_unique_tracknos (
		p_shipid		 IN 	  t907_shipping_info.c907_shipping_id%TYPE
	  , p_trackinfocur	 OUT	  TYPES.cursor_type
	);

	 /******************************************************************
		* Description : Returns the pending control number status for a given id
	****************************************************************/
	FUNCTION get_pendcontrol_status (
		p_refid    IN	t907_shipping_info.c907_ref_id%TYPE
	  , p_source   IN	t907_shipping_info.c901_source%TYPE
	)
		RETURN NUMBER;

	/******************************************************************
	 * Author		:	Gopinathan
	 *
	 * Description	: Fetch the hold flag status by passing the ref_id and source type
	****************************************************************/
	FUNCTION get_hold_flg (
		p_id	   IN	t504_consignment.c504_consignment_id%TYPE
	  , p_source   IN	VARCHAR2
	)
		RETURN VARCHAR2;
	/****************************************************************************************************************
	 * Description : Procedure to get all the Complete[including State,Country and ZIP Code] Address for a Slaes Rep
     ****************************************************************************************************************/
	FUNCTION get_salesrepaddress (
		p_address_id	    IN		t907_shipping_info.c106_address_id%TYPE,
		p_rep_id			IN      t907_shipping_info.c907_ship_to_id%TYPE	  
	)
	RETURN VARCHAR2;
	/**************************************************************************************************************
	* Description : Procedure to get the Complete[including State,Country and ZIP Code] Address for a Slaes Rep
   **************************************************************************************************************/
	FUNCTION get_empaddress (
		p_address_id	    IN		t907_shipping_info.c106_address_id%TYPE,
		p_emp_id			IN      t907_shipping_info.c907_ship_to_id%TYPE	 
	)
	RETURN VARCHAR2;
	 /******************************************************************
	* Description : Function to get the status of apply all check box status in modify shipping screen
	****************************************************************/
	FUNCTION gm_fch_ship_applyall_sts (
		p_shipid		 IN 	  t907_shipping_info.c907_shipping_id%TYPE
	)
	RETURN VARCHAR2;

/****************************************************************************************
* Description : Procedure to get the Consigment , Qty , Price for selected Consigment ids.
*****************************************************************************************/	
	PROCEDURE gm_fch_consin_shipment_val(
      p_ConsignIds     IN       VARCHAR2,
      p_shipinfocur   OUT      TYPES.cursor_type
   );

/********************************************************************
* Description : Update planned ship date selected in consignment ids.
*********************************************************************/
   PROCEDURE gm_update_planned_shipdate(
      p_ConsignIds    IN       VARCHAR2,
      p_planedShipDt  IN	   VARCHAR2,
      p_userid	 	  IN	   VARCHAR2
   );
 
/********************************************************************
* Description : To get company filter information.
* Author : Velu
* param : Dist_id, CompId, DivID, AreaID, ZoneID
*********************************************************************/
   
   PROCEDURE gm_cs_fch_company_info (   
	   p_dist_id 	IN t701_distributor.c701_distributor_id%TYPE,
	   p_comp_id 	OUT t901_code_lookup.C901_CODE_ID%TYPE,
	   p_div_id 	OUT t901_code_lookup.C901_CODE_ID%TYPE,
	   p_area_id 	OUT t901_code_lookup.C901_CODE_ID%TYPE,
	   p_zone_id 	OUT t901_code_lookup.C901_CODE_ID%TYPE
 );

/********************************************************************
* Description : To get company filter information like div,area,zone.
* Author : Velu
*********************************************************************/
 PROCEDURE gm_update_compnay_info (   
	   p_userid 	IN VARCHAR
 );
 /*******************************************************************************************************
* Description : When select shipment value will be display shpiments in Shpping report screen.
* Author	  : Velu
********************************************************************************************************/

 PROCEDURE gm_fetch_shipment_price(
      p_consign_id    IN       t504_consignment.c504_consignment_id%TYPE
 );

/*******************************************************************************************************
* Description : Fetch all the txn types that are in pending shipping and their count
* Author	  : VPrasath
********************************************************************************************************/

 PROCEDURE gm_fch_pend_ship_cnt(    
 	p_party_id   IN    t101_user.c101_party_id%TYPE
 	,p_out_cursor OUT   TYPES.cursor_type 
 );

/******************************************************************
   * Description : Procedure to get the address info like city, state, zip etc 
   ****************************************************************/
	PROCEDURE gm_fch_ship_address (
		p_refid 		IN		 t907_shipping_info.c907_ref_id%TYPE
	  , p_source		IN		 t907_shipping_info.c901_source%TYPE
	  , p_shipaddrcur	OUT 	 TYPES.cursor_type
	);
 
/************************************************************************
* Description : Fetch all available Ship Modes based on the Ship Carrier
* Author	  : Hreddi
*************************************************************************/
 PROCEDURE gm_fch_ship_car_modes(  
 	p_Ship_Carrier IN  t907_shipping_info.c901_delivery_carrier%TYPE
  , p_acctID	   IN  t704_account.C704_ACCOUNT_ID%TYPE	
  , p_out_cursor   OUT   TYPES.cursor_type 
  , p_default_mode OUT 	VARCHAR
  , p_company_id   IN   t1900_company.c1900_company_id%TYPE DEFAULT NULL
 );
 
 /************************************************************************
* Description : get shipping account address
* Author	  : Xun
 ***********************************************************************/
	FUNCTION get_shipacct_addr (
		p_address_id	    IN		t907_shipping_info.c106_address_id%TYPE,
		p_party_id			IN      t907_shipping_info.c907_ship_to_id%TYPE	 
	)
	RETURN VARCHAR2;
	
	
   /******************************************************************
	* Description : Procedure to get the shipping info	for mailing
	****************************************************************/
	PROCEDURE gm_fch_shipped_order_today(
		p_mailshipdtl	OUT   TYPES.cursor_type
	  , p_ruledtl		OUT   TYPES.cursor_type
	);
	
	 /******************************************************************
	 * Description : function to get shipto loaner for italy
	 * PMT-42950   : Include Customer # in shipment and Invoice paperwork for Italy
	 *  Author     : rajan
	 ****************************************************************/
	FUNCTION get_loaner_ship_add (
      p_refid        IN   t501_order.c501_order_id%TYPE,
      p_source       IN   t907_shipping_info.c901_source%TYPE,
      p_prod_req_id  IN   t907_shipping_info.c525_product_request_id%TYPE,
      p_swap_attn_to IN   VARCHAR2
   )
      RETURN VARCHAR2;
	
END gm_pkg_cm_shipping_info;
/
