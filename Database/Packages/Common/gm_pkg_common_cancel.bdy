/* Formatted on 2011/03/25 16:54 (Formatter Plus v4.8.0) */
--@"c:\Database\Packages\common\gm_pkg_common_cancel.bdy"

CREATE OR REPLACE PACKAGE BODY gm_pkg_common_cancel
IS
--
/*This procedure is calling by ETL Job, If there is any changes then please coordinate with DWH team during design phase */
	PROCEDURE gm_cm_sav_cancelrow (
		p_txn_id		 IN   VARCHAR2
	  , p_cancelreason	 IN   t907_cancel_log.c901_cancel_cd%TYPE
	  , p_canceltype	 IN   t901_code_lookup.c902_code_nm_alt%TYPE
	  , p_comments		 IN   t907_cancel_log.c907_comments%TYPE
	  , p_userid		 IN   t102_user_login.c102_user_login_id%TYPE
	)
	AS
/***************************************************************************************
 * Description :This procedure is called to save information about a row which is cancelled (rollback / void / delete)
 *
 ****************************************************************************************/
		v_canceltypecode t907_cancel_log.c901_type%TYPE;
		v_cancelid	   NUMBER;
		v_exec_string  VARCHAR2 (2000);
		v_rule_value   VARCHAR2 (2000);
		v_rule_para    VARCHAR2 (20);
		v_string	   VARCHAR2 (2000);
		v_txnid		VARCHAR2 (2000);
		v_out		VARCHAR2 (2000);
		v_company_id    t1900_company.c1900_company_id%TYPE;
	-- v_ref_type	   IN t4030_demand_growth_mapping.c4030_ref_id%TYPE
	BEGIN
		SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL')) INTO v_company_id FROM dual;
		SELECT c901_code_id
		  INTO v_canceltypecode
		  FROM t901_code_lookup
		 WHERE c902_code_nm_alt = p_canceltype;

		--		
		
		  v_string:=p_txn_id || ',';
		--
		
			WHILE INSTR (v_string, ',') <> 0
	        LOOP
	        	v_txnid := SUBSTR (v_string, 1, INSTR (v_string, ',') - 1);
	         	v_string := SUBSTR (v_string, INSTR (v_string, ',') + 1);
	         	SELECT s907_cancel_log.NEXTVAL
		  			INTO v_cancelid
		  			FROM DUAL;
		  			
				INSERT INTO t907_cancel_log
						(c907_cancel_log_id, c907_ref_id, c907_comments, c901_cancel_cd, c901_type, c907_created_by
					   , c907_created_date, c907_cancel_date, c1900_company_id
						)
				 VALUES (v_cancelid, v_txnid, p_comments, p_cancelreason, v_canceltypecode, p_userid
					   , SYSDATE, TRUNC (SYSDATE), v_company_id
						);
			END LOOP;
		
		IF v_canceltypecode = 90200
		-- If the Cancel Type is Rollback consignment. RBSCN
		THEN
			gm_cs_sav_rollback_set_csg (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 90201 OR v_canceltypecode = 90202 OR v_canceltypecode = 90205)
-- If the Cancel Type is Void consignment (Item - VODCN - 90201) / (Set - VDSCN - 90202) / (Dummy Consg - VCOND - 90205)
		THEN
			gm_cs_sav_void_consignment (p_txn_id, p_userid);		
		ELSIF (v_canceltypecode = 90204)   -- If the Cancel Type is Void Transfer(Transfer - VDTSF - 90204)
		THEN
			gm_cs_sav_void_transfer (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 90206)   -- If the Cancel Type is Void Project(Project - VDPRJ - 90206)
		THEN
			gm_cs_sav_void_project (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 90207)   -- If the Cancel Type is Void Set(Set - VDSET - 90207)
		THEN
			gm_cs_sav_void_set (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 90208 OR v_canceltypecode = 90873)
		-- If the Cancel Type is Rollback Return(RBRTN - 90208, CRBTN - 90873)
		THEN
			gm_cs_rollback_return (p_txn_id, p_userid, v_canceltypecode);
		ELSIF (v_canceltypecode = 90209)   -- If the Cancel Type is Void Demand Sheet(VDDMS - 90209)
		THEN
			gm_pkg_common_cancel_op.gm_fc_sav_void_demandsheet (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 90210)   -- If the Cancel Type is void job(VDJBS - 90210)
		THEN
			gm_cm_sav_void_job (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 90211)   -- If the Cancel Type is void group(VDGRP - 90211)
		THEN
			gm_pkg_common_cancel_op.gm_fc_sav_void_group (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 90212)   -- If the Cancel Type is void group(VDTTP - 90212)
		THEN
			gm_pkg_common_cancel_op.gm_fc_sav_void_ttp (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 90213)   -- If the Cancel Type is void group(VDSTE - 90213)
		THEN
			gm_cr_sav_void_site (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 90214)   -- If the Cancel Type is void group(VDSUR - 90214)
		THEN
			gm_cr_sav_void_surgeon (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 90215)   -- If the Cancel Type is void group(VDPAT - 90215)
		THEN
			gm_cr_sav_void_patient (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 90216)   -- If the Cancel Type is void Purchase Order(VDPPO - 90216)
		THEN
			gm_op_sav_void_purchpo (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 90218)
		-- If the Cancel Type is Rollback Loaners Shipping(VDRES - 90217)
		THEN
			gm_cs_sav_rollback_loaner_ship (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 90219)   -- If the Cancel Type is Rollback Loaners Returns(VDRES - 90217)
		THEN
			-- gm_cs_sav_rollback_loaner_retn (p_txn_id, p_userid);
			gm_pkg_common_cancel_op.gm_cs_sav_rollback_loaner_retn (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 90199)   -- If the Cancel Type is Void Request(VDREQ - 90199)
		THEN
			gm_cs_sav_void_request (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 90198)   -- If the Cancel Type is Void Request(VDREQ - 90199)
		THEN
			gm_cs_sav_void_pdtrequest (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 90872)   -- If the Cancel Type is Void Request(VDDSG - 90872)
		THEN
			gm_fc_sav_void_part_growth (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 90197)   -- If the Cancel Type is status rollback (STSRB - 90197)
		THEN
			gm_status_rollback (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 90876)   -- If the Cancel Type is status rollback (STSRB - 90197)
		THEN
			gm_pkg_common_cancel_op.gm_op_exec_dsload (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 90875)   -- If the Type is to execute Demand Sheet
		THEN
			gm_sc_sav_void_patag (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 92148)   -- If the Cancel Type is affiliation(AFFLI - 91203)
		THEN
			gm_pkg_cm_party_cancel.gm_void_affiliation (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 92145)   -- If the Cancel Type is License(LICEN - 91205)
		THEN
			gm_pkg_cm_party_cancel.gm_void_license (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 92142)   -- If the Cancel Type is Achievement(ACHIV - 91204)
		THEN
			gm_pkg_cm_party_cancel.gm_void_achievementinfo (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 90877)   -- If the Cancel Type is void Addres(VDADD)
		THEN
			gm_pkg_cm_party_cancel.gm_void_address (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 90879)   -- If the Cancel Type is Void Session Record(VSESN - 90879)
		THEN
			gm_pkg_etr_cancel.gm_etr_sav_void_session (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 90880)
		-- If the Cancel Type is Void Course map Department(VDMAP - 90880)
		THEN
			gm_pkg_etr_cancel.gm_etr_sav_void_coursedept (p_txn_id);
		ELSIF (v_canceltypecode = 90881)
		-- If the Cancel Type is Void Session Module Record(VSMAP - 90881)
		THEN
			gm_pkg_etr_cancel.gm_etr_sav_void_sesnmodmap (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 90882)   -- If the Cancel Type is Void User Session Record(VSUMP - 90882)
		THEN
			gm_pkg_etr_cancel.gm_etr_sav_void_usermodmap (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 90883)   -- If the Cancel Type is Void Course/Program (VCOSE- 90883)
		THEN
			gm_pkg_etr_cancel.gm_etr_sav_void_course_setup (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 90884)   -- If the Cancel Type is Void Module ('VMODU- 90884)
		THEN
			gm_pkg_etr_cancel.gm_etr_sav_void_module_setup (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 90885)   -- If the Cancel Type is Void Document ('VMDOC- 90885)
		THEN
			gm_pkg_etr_cancel.gm_etr_sav_void_moddoc (p_txn_id);
		ELSIF (v_canceltypecode = 92111)
		-- If the Cancel Type is Void Party Contact Record(VDCON - 92111)
		THEN
			gm_pkg_cm_party_cancel.gm_void_contact (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 92108)   -- If the Cancel Type is Void Party Record(VDPTY - 92108)
		THEN
			gm_pkg_cm_party_cancel.gm_void_party (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 92123)
		-- If the Cancel Type is Void Party Globus Association Record(VDGAS - 921123)
		THEN
			gm_pkg_cm_party_cancel.gm_void_globusinfo (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 92117)
		-- If the Cancel Type is Void Party Education Record(VDEDU - 92117)
		THEN
			gm_pkg_cm_party_cancel.gm_void_education (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 92120)
		-- If the Cancel Type is Void Party Publication Record (VDPUB - 921120)
		THEN
			gm_pkg_cm_party_cancel.gm_void_publication (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 92126)
		-- If the Cancel Type is Void Party Career Record (VDCAR - 921126)
		THEN
			gm_pkg_cm_party_cancel.gm_void_career (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 90196)   -- If the Cancel Type is Void Request(VDINV - 90196)
		THEN
			gm_ac_sav_void_invoice (p_txn_id,p_userid,v_out); -- For this procedure out param is added. so here add one more param.
		ELSIF (v_canceltypecode = 90888)   -- If the Cancel Type is Rollback Orders(RBORD - 90888)
		THEN
			gm_orders_rollback (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 92149)   -- If the Cancel Type is Rollback for download(RPYMT - 92149)
		THEN
			gm_ac_sav_rollback_download (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 90891)   -- If the Cancel Type is Staffing Detail(VSTAF - 90891)
		THEN
			gm_pkg_pd_cancel.gm_pd_sav_void_staffingdtl (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 92150)   -- If the Cancel Type is Void Invoice(CNCLT - VINVC)
		THEN
			gm_ac_sav_void_ap_invoice (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 92057)   -- If the type is change project status
		THEN
			gm_pkg_pd_project_trans.gm_chg_projectstatus (p_txn_id, p_cancelreason, p_userid);
		ELSIF (v_canceltypecode = 92151)   -- If the Cancel Type is Rollback Release Payment(RPYMT - 92151)
		THEN
			gm_ac_sav_rollback_payment (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 90893)   -- If the Cancel Type is VRULE
		THEN
			gm_pkg_cm_rule_cancel.gm_rule_sav_void_rule (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 90892)   -- if the cancel type is VCNSQ for Consequences
		THEN
			gm_pkg_cm_rule_cancel.gm_rule_sav_void_consequence (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 90894)   -- if the cancel type is VINTX for Voiding InHouse Transaction
		THEN
			gm_pkg_common_cancel_op.gm_op_sav_void_inhousetxn (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 90896)   -- for Voiding tags
		THEN
			gm_pkg_common_cancel.gm_void_tag (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 90897)   -- for unlinking tags
		THEN
			gm_pkg_common_cancel.gm_unlink_tag (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 90952)   -- if the cancel type is RBPRS Rollback process
		THEN
			gm_pkg_common_cancel_op.gm_cs_sav_rollback_loaner_pros (p_txn_id
																  , p_cancelreason
																  , p_canceltype
																  , p_comments
																  , p_userid
																   );
		ELSIF (v_canceltypecode = 90899)
-- if the cancel type is RDSST for changing the sheet status from Aprove to open
		THEN
			gm_pkg_common_cancel.gm_op_rollback_status (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 90898)   -- if the cancel type is VSPMP for Voiding Set part mapping
		THEN
			gm_pkg_common_cancel.gm_sav_void_set_part_map (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 90950)   -- if the cancel type is RPORO for PO Reopen
		THEN
			gm_pkg_common_cancel.gm_op_sav_po_reopen (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 90951)   -- if the cancel type is DSHEM for disabling Shipping Email
		THEN
			gm_pkg_common_cancel.gm_op_sav_disable_ship_email (p_txn_id, p_userid);
		/* ELSIF (v_canceltypecode = 90954
			   )	   -- if the cancel type is SYSCON for cancelng the construct
		 THEN
			gm_pkg_common_cancel.gm_pd_void_system_construct (p_txn_id,
															  p_userid);*/
		ELSIF (v_canceltypecode = 92952)
		-- if the cancel type is VDRGU for voiding the radiograph upload.
		THEN
			gm_pkg_common_cancel.gm_cl_sav_void_radiograph_upld (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 90878)
		-- If the Type is Remove Sales Order(RHOLD - 90878) from Hold Status
		THEN
			gm_cs_sav_order_remove_hold (p_txn_id, p_cancelreason, v_cancelid, p_comments, p_userid);
		ELSIF (v_canceltypecode = 92481)   -- to void site mapping
		THEN
			gm_pkg_cm_query_info.gm_sav_void_sitemapping (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 92460)   -- to void query
		THEN
			gm_pkg_cm_query_info.gm_sav_void_query (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 60578 OR v_canceltypecode = 60584)
-- -- if the cancel type is VDSMT or CNSMT then void/cancel  site monitoring record
		THEN
			gm_pkg_cr_site_monitor.gm_cr_cancel_site_monitor (p_txn_id, p_cancelreason, p_canceltype, p_userid);
		ELSIF (v_canceltypecode = 60579)
-- if the cancel type is VSMOBS then voiding the site monitoring observation record
		THEN
			gm_pkg_cr_site_monitor.gm_cr_cancel_site_mtr_obs (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 90960)   -- Void IRB Approval
		THEN
			gm_pkg_common_cancel.gm_cs_sav_void_irb_approval (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 90961)   -- Void IRB Event
		THEN
			gm_pkg_common_cancel.gm_cs_sav_void_irb_event (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 90962)   -- Void IRB Media
		THEN
			gm_pkg_common_cancel.gm_cs_sav_void_irb_media (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 60590)   -- Void Patient Log
		THEN
			gm_pkg_common_cancel.gm_cr_sav_void_patient_log (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 92425)   -- Changing the ststus of Edit Check Reference Id
		THEN
			gm_pkg_cr_edit_check_info.gm_sav_edit_check_summary (p_txn_id, p_cancelreason, p_userid);
		ELSIF (v_canceltypecode = 92415)   -- Void edit Check
		THEN
			gm_pkg_cr_edit_check_info.gm_sav_void_edit_check (p_txn_id, p_cancelreason, p_userid);
		ELSIF (v_canceltypecode = 93049)   -- Void incident
		THEN
			gm_pkg_cr_edit_check_info.gm_sav_ec_incident (p_txn_id, p_cancelreason, p_userid);
		ELSIF (v_canceltypecode = 92953)   -- Void surgeon training
		THEN
			gm_pkg_common_cancel.gm_sav_void_training (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 93075)
		THEN
			gm_pkg_cr_edit_check_info.gm_sav_schedule_ec (p_cancelreason, p_userid);
		ELSIF (v_canceltypecode = 92710)
		THEN
			gm_pkg_cr_pyt.gm_cr_void_payment (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 92720)   -- Void Payment details ID
		THEN
			gm_pkg_cr_pyt.gm_cr_void_payment_details (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 92154)   -- if the cancel type is VCURR for cancelng the construct
		THEN
			gm_pkg_common_cancel.gm_cm_sav_void_curr_conv (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 94000   -- void Group
									   )
		THEN
			gm_pkg_cm_accesscontrol.gm_sav_void_group (p_txn_id, p_userid);
		/*	 ELSIF (v_canceltypecode = 90955)	-- if the cancel type is VDPRRQ for cancelng the construct
		   THEN
				 gm_pkg_common_cancel.gm_pd_void_grp_price_request (p_txn_id, p_userid);*/
		ELSIF (v_canceltypecode = 94010   -- void Group
									   )
		THEN
			gm_pkg_cm_accesscontrol.gm_sav_void_group_mapping (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 91961)   -- for Voiding  Field Audit tags tags (Only Particular user)
		THEN
			gm_pkg_common_cancel.gm_void_field_tag (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 92450)   -- Move Back to BackOrder
		THEN
			gm_pkg_op_order_master.gm_move_do_to_backorder (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 94020)   -- if the cancel type is VCURR for cancelng the construct
		THEN
			gm_pkg_sm_mapping.gm_sav_void_zone (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 94030)
		THEN
			gm_pkg_sm_mapping.gm_sav_void_region (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 94500)   -- if the cancel type is VASST for the Asset Master
		THEN
			gm_pkg_it_asset.gm_ac_void_user_id_asset (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 94600)   -- if the cancel type is VCNEX for the Control Number Exception
		THEN
			gm_pkg_op_controlnumber.gm_void_control_number (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 94510)   -- if the cancel type is VASAT for the Asset Attribute
		THEN
			gm_pkg_it_asset.gm_ac_void_asset_att (p_txn_id, p_userid);
		ELSIF (v_canceltypecode = 92920)   -- If the Cancel Type is Void Call Log(CallLog - VDCLOG - 92920)
		THEN
			gm_cs_sav_void_calllog (p_txn_id, p_userid);	
		ELSE
			-- Dynamic Code for void functionalities added
			SELECT get_rule_value (v_canceltypecode, 'CMNCNCL')
			  INTO v_rule_value
			  FROM DUAL;

			IF v_rule_value IS NULL
			THEN
				GM_RAISE_APPLICATION_ERROR('-20999','58',v_canceltypecode);
			END IF;
			
			-- get parameter numbers from rule table
			SELECT get_rule_value (v_canceltypecode, 'CMNCLPM')
			  INTO v_rule_para
			  FROM DUAL;

			IF (v_rule_para IS NOT NULL AND v_rule_para = '4' )    
			THEN 
				v_exec_string := 'BEGIN ' || v_rule_value || '(''' || p_txn_id || ''','''|| p_cancelreason || ''','''|| v_cancelid||''','||p_userid||'); END;';

			ELSIF (v_rule_para IS NOT NULL AND v_rule_para = '5' )    
			THEN 
				v_exec_string := 'BEGIN ' || v_rule_value || '(''' || p_txn_id || ''','''|| p_cancelreason || ''','''|| v_cancelid|| ''','''|| p_comments ||''','||p_userid||'); END;'; 
			ELSE
				v_exec_string := 'BEGIN ' || v_rule_value || '(''' || p_txn_id || ''',' || p_userid || '); END;';

			END IF;

			EXECUTE IMMEDIATE (v_exec_string);
			
		END IF;
	END gm_cm_sav_cancelrow;

----------------------------
	/*******************************************************
	 * Description : this procedure does the same functionality as the above gm_cm_sav_cancelrow.
	 * The only difference is it returns an out message(customized) which will be shown on the jsp once cancel is done.
	 * Parameters	:added an extra out param
	 *
	 *******************************************************/
	PROCEDURE gm_cm_sav_cancelrow_retn (
		p_txn_id		 IN 	  VARCHAR2
	  , p_cancelreason	 IN 	  t907_cancel_log.c901_cancel_cd%TYPE
	  , p_canceltype	 IN 	  t901_code_lookup.c902_code_nm_alt%TYPE
	  , p_comments		 IN 	  t907_cancel_log.c907_comments%TYPE
	  , p_userid		 IN 	  t102_user_login.c102_user_login_id%TYPE
	  , p_out_msg		 OUT	  VARCHAR2
	)
	AS
/***************************************************************************************
 * Description :This procedure is called to save information about a row which is cancelled (rollback / void / delete)
 *
 ****************************************************************************************/
		v_canceltypecode t907_cancel_log.c901_type%TYPE;
		v_cancelid	   NUMBER;
		v_exec_string VARCHAR2 (2000);
		v_rule_value VARCHAR2 (2000);
		v_rule_comments VARCHAR2(20);
		v_company_id    t1900_company.c1900_company_id%TYPE;
		
	BEGIN
		SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL')) INTO v_company_id FROM dual;
		SELECT c901_code_id
		  INTO v_canceltypecode
		  FROM t901_code_lookup
		 WHERE c902_code_nm_alt = p_canceltype;

		  -- Dynamic Code - p_txn_id string length is more , no need to insert into the log table. 
			SELECT get_rule_value (v_canceltypecode, 'CMNCNCOMMENTS')
			 INTO v_rule_comments
			 FROM DUAL;
			 
		IF v_rule_comments IS NULL   -- single row insert
		THEN
			--
			SELECT s907_cancel_log.NEXTVAL
			  INTO v_cancelid
			  FROM DUAL;

			--
			INSERT INTO t907_cancel_log
						(c907_cancel_log_id, c907_ref_id, c907_comments, c901_cancel_cd, c901_type, c907_created_by
					   , c907_created_date, c907_cancel_date, c1900_company_id
						)
				 VALUES (v_cancelid, p_txn_id, p_comments, p_cancelreason, v_canceltypecode, p_userid
					   , SYSDATE, TRUNC (SYSDATE), v_company_id
						);
		END IF;

		IF v_canceltypecode = 93076
		THEN
			gm_pkg_op_ln_cn_swap.gm_op_sav_loaner_to_consign (p_txn_id, p_userid, p_out_msg);
		ELSE
			-- Dynamic Code for void functionalities added
			SELECT get_rule_value (v_canceltypecode, 'CMNCNCL')
			 INTO v_rule_value
			 FROM DUAL;
       
			IF v_rule_value IS NULL
			THEN
				GM_RAISE_APPLICATION_ERROR('-20999','58',v_canceltypecode);
			END IF;
            
			IF v_rule_comments = 'Y' 
			THEN
			v_exec_string := 'BEGIN ' || v_rule_value || '(:p_txn_id,:p_comments,:p_cancelreason,:p_canceltype,:p_userid,:p_custom_msg); END;'; 
			EXECUTE IMMEDIATE v_exec_string using p_txn_id ,p_comments,p_cancelreason,p_canceltype,p_userid , out p_out_msg;
			ELSE
			v_exec_string := 'BEGIN ' || v_rule_value || '(:p_txn_id,:p_userid,:p_custom_msg); END;'; 
			EXECUTE IMMEDIATE v_exec_string using p_txn_id ,p_userid , out p_out_msg;
			
			END IF;
			
		END IF;															 
		 
				
	END gm_cm_sav_cancelrow_retn;

--------------------------------------------
	PROCEDURE gm_cs_sav_rollback_set_csg (
		p_txn_id   IN	t907_cancel_log.c907_ref_id%TYPE
	  , p_userid   IN	t102_user_login.c102_user_login_id%TYPE
	)
	AS
/***************************************************************************************
 * Description :This procedure is called to rollback a set consignment. The following values would be deleted
 * FROM T504_CONSIGNMENT
 * C701_DISTRIBUTOR_ID ,  C504_SHIP_DATE
 * C504_STATUS_FL to 2 from 3
 * C504_SHIP_TO
 * C504_SHIP_TO_ID
 * C504_DELIVERY_CARRIER
 * C504_DELIVERY_MODE
 * C504_SHIP_REQ_FL
 * C504_type
 * C504_inhouse_purpose
 * C504_final_comments
 * C504_last_updated_by - User ID
 * C504_last_updated_date - SYSDATE
 *
 ****************************************************************************************/
		v_status_fl    t504_consignment.c504_status_fl%TYPE;
	BEGIN
		-- Logic for checking if the Status is 3 (Pending shipping) . Else throw Error message.
		-- This logic is to validate if Rollback is happening through browser history
		SELECT	   NVL (c504_status_fl, 0)
			  INTO v_status_fl
			  FROM t504_consignment
			 WHERE c504_consignment_id = p_txn_id
		FOR UPDATE;

		IF (v_status_fl = 3)
		THEN
			UPDATE t504_consignment
			   SET c701_distributor_id = NULL
				 , c504_ship_date = NULL
				 , c504_status_fl = 2
				 , c504_ship_to = NULL
				 , c504_ship_to_id = NULL
				 , c504_delivery_carrier = NULL
				 , c504_delivery_mode = NULL
				 , c504_ship_req_fl = NULL
				 , c504_type = NULL
				 , c504_inhouse_purpose = NULL
				 , c504_final_comments = NULL
				 , c504_last_updated_by = p_userid
				 , c504_last_updated_date = SYSDATE
			 WHERE c504_consignment_id = p_txn_id;
		ELSIF (v_status_fl <> 3)
		THEN
			-- Error message is Set Consignment is not in Shipped Status and cannot be rolled back.
			raise_application_error (-20771, '');
		END IF;
--
	END gm_cs_sav_rollback_set_csg;

--
	PROCEDURE gm_cs_cancel_loginfo (
		p_txn_id		   IN		t907_cancel_log.c907_ref_id%TYPE
	  , p_from_date 	   IN		VARCHAR2
	  , p_to_date		   IN		VARCHAR2
	  , p_cancel_type	   IN		t901_code_lookup.c902_code_nm_alt%TYPE
	  , p_cancel_reason    IN		t901_code_lookup.c901_code_id%TYPE
	  , p_cancel_user	   IN		t907_cancel_log.c907_created_by%TYPE
	  , p_cancel_comment   IN		t907_cancel_log.c907_comments%TYPE
	  , p_loginfo		   OUT		TYPES.cursor_type
	)
	AS
		   /*******************************************************
		* Description : Procedure to fetch cancel log information. Used by cancel log reports
		* Parameters	 : 1. Transaction Id - Id of the Entity whose transaction state is to be altered. Eg: Consignment Id / Order Id / etc.
		*		 2. From date
		*		 3. To date
		*
		*******************************************************/
		v_cancel_type_from_menu t907_cancel_log.c901_type%TYPE;
		v_dateformat VARCHAR2(100);
		v_company_id    t1900_company.c1900_company_id%TYPE;
		v_filter 		T906_RULES.C906_RULE_VALUE%TYPE;
  		v_plant_id 		T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
	BEGIN
--
		SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL')) ,get_plantid_frm_cntx(),get_compdtfmt_frm_cntx()
			INTO v_company_id ,v_plant_id ,v_dateformat
			FROM dual;
		
		SELECT c901_code_id
		  INTO v_cancel_type_from_menu
		  FROM t901_code_lookup
		 WHERE c902_code_nm_alt = p_cancel_type;
		 /**************************************************************************************************************************************************************
		* NVL2() Description : If the first parameter is NOTNULL it returns the value in second parameter,if the first parameter is NULL it returns the third parameter.
		* Author :karthik
		****************************************************************************************************************************************************************/
       	SELECT NVL2(GET_RULE_VALUE(v_company_id,'ITEM_CANCEL_RPT'),v_plant_id,v_company_id) --ITEM_CANCEL_RPT:=rule value-plant
 			INTO v_filter 
			FROM DUAL;

		OPEN p_loginfo
		 FOR
			 SELECT c907_cancel_log_id logid, c907_ref_id txnid, c907_comments comments
				  , get_code_name (c901_cancel_cd) cancelreason, get_code_name (c901_type) canceltype
				  , get_user_name (c907_created_by) createdby, c907_cancel_date canceldate
			   FROM t907_cancel_log t907c
			  -- If the Input value is NULL, then use the REF_ID itself. So if no transaction ID is provided, no filter gets applied. Same for date filter below
			 WHERE	c907_ref_id = NVL (p_txn_id, c907_ref_id)
				AND c901_type = v_cancel_type_from_menu
				AND c901_cancel_cd = NVL (p_cancel_reason, c901_cancel_cd)
				AND c907_created_by = NVL (p_cancel_user, c907_created_by)
				AND LOWER (NVL(c907_comments,'-999')) LIKE '%' || NVL (LOWER (p_cancel_comment), LOWER (NVL(c907_comments,'-999'))) || '%' 
				AND c907_cancel_date BETWEEN NVL (TO_DATE (p_from_date, v_dateformat), c907_cancel_date)
										 AND NVL (TO_DATE (p_to_date, v_dateformat), c907_cancel_date)
				AND c907_void_fl IS NULL
				AND (t907c.c1900_company_id  = v_company_id OR t907c.c1900_company_id  IN (SELECT c1900_company_id FROM t5041_plant_company_mapping WHERE C5041_PRIMARY_FL = 'Y' AND c5040_plant_id =v_filter AND c5041_void_fl IS NULL));
	END gm_cs_cancel_loginfo;

------
	PROCEDURE gm_cs_sav_void_consignment (
		p_txn_id   IN	t907_cancel_log.c907_ref_id%TYPE
	  , p_userid   IN	t102_user_login.c102_user_login_id%TYPE
	)
	AS
		/*******************************************************
		   * Description : Procedure to void an item / set consignment
		   * Parameters 	 : 1. Transaction Id - Item Consignment ID
		   *		2. User Id
		   *
		   *******************************************************/
		v_void_fl	   t504_consignment.c504_void_fl%TYPE;
		v_txn_type	   t504_consignment.c504_type%TYPE;
		v_upd_inv_fl   t504_consignment.c504_update_inv_fl%TYPE;
	BEGIN
		--
		   --
		   --
		SELECT c504_void_fl, c504_update_inv_fl, c504_type
		  INTO v_void_fl, v_upd_inv_fl, v_txn_type
		  FROM t504_consignment
		 WHERE c504_consignment_id = p_txn_id;

--
		IF (v_void_fl IS NOT NULL)
		THEN
			-- Error message is Consignment already Voided.
			raise_application_error (-20793, '');
		ELSIF (v_upd_inv_fl IS NOT NULL AND v_txn_type <> 4129)
		THEN
			-- Error message is Inventory already updated and Consignment cannot be voided
			raise_application_error (-20802, '');
		END IF;

		UPDATE t504_consignment
		   SET c504_void_fl = 'Y'
			 , c504_last_updated_by = p_userid
			 , c504_last_updated_date = SYSDATE
		 WHERE c504_consignment_id = p_txn_id;
--
	END gm_cs_sav_void_consignment;

 
	PROCEDURE gm_cs_sav_void_transfer (
		p_trns_id	IN	 t920_transfer.c920_transfer_id%TYPE
	  , p_user_id	IN	 t920_transfer.c920_created_by%TYPE
	)
	AS
		   /****************************************************************
			  * This Procedure is to void the transfer initiated,
			  this procedure will be called from verify transfer.
			  * Author : vprasath
			  * Date  : 02/25/07
		   ***************************************************************/
		--
		v_void_fl	   t920_transfer.c920_void_fl%TYPE;
		v_trans_type   t920_transfer.c901_transfer_type%TYPE;
--
	BEGIN
		SELECT		  c920_void_fl, c901_transfer_type
				 INTO v_void_fl, v_trans_type
				 FROM t920_transfer
				WHERE c920_transfer_id = p_trns_id
		FOR UPDATE OF c920_void_fl;

		IF v_void_fl IS NOT NULL
		THEN
			-- Error message is transfer already voided
			raise_application_error (-20785, '');
		END IF;

		UPDATE t920_transfer
		   SET c920_void_fl = 'Y'
			 , c920_last_updated_by = p_user_id
			 , c920_last_updated_date = SYSDATE
		 WHERE c920_transfer_id = p_trns_id;

		-- if not full transfer then disable
		-- the associated consignment and returns
		IF (v_trans_type <> 90350)
		THEN
			UPDATE t504_consignment
			   SET c504_void_fl = 'Y'
			     , c504_last_updated_by = p_user_id
    			 , c504_last_updated_date = SYSDATE
			 WHERE c504_consignment_id IN (SELECT c921_ref_id
											 FROM t921_transfer_detail
											WHERE c920_transfer_id = p_trns_id);

			UPDATE t506_returns
			   SET c506_void_fl = 'Y'
			     , c506_last_updated_by = p_user_id
  				 , c506_last_updated_date = SYSDATE
			 WHERE c506_rma_id IN (SELECT c921_ref_id
									 FROM t921_transfer_detail
									WHERE c920_transfer_id = p_trns_id);
		END IF;
	END gm_cs_sav_void_transfer;

	/*******************************************************
			 * Description : Procedure to void Project
			 * Parameters	: 1. Transaction Id
			 *			 2. User Id
			 *
			 *******************************************************/
	PROCEDURE gm_cs_sav_void_project (
		p_project_id   IN	t202_project.c202_project_id%TYPE
	  , p_user_id	   IN	t202_project.c202_last_updated_by%TYPE
	)
	AS
		--
		v_void_fl	   t202_project.c202_void_fl%TYPE;
--
	BEGIN
		SELECT		  c202_void_fl
				 INTO v_void_fl
				 FROM t202_project
				WHERE c202_project_id = p_project_id
		FOR UPDATE OF c202_void_fl;

		IF v_void_fl IS NOT NULL
		THEN
			-- Error message is project already voided
			raise_application_error (-20803, '');
		END IF;

		UPDATE t202_project
		   SET c202_void_fl = 'Y'
			 , c202_last_updated_by = p_user_id
			 , c202_last_updated_date = SYSDATE
		 WHERE c202_project_id = p_project_id;
/*	   UPDATE t207_set_master
	  SET c207_void_fl = 'Y'
	   WHERE c202_project_id = p_project_id;

   UPDATE t205_part_number
	  SET c205_void_fl = 'Y'
	   WHERE c202_project_id = p_project_id; */
	END gm_cs_sav_void_project;

	/*******************************************************
			 * Description : Procedure to void Set
			 * Parameters	: 1. Set Id
			 *			 2. User Id
			 *
			 *******************************************************/
	PROCEDURE gm_cs_sav_void_set (
		p_set_id	IN	 t207_set_master.c207_set_id%TYPE
	  , p_user_id	IN	 t207_set_master.c207_last_updated_by%TYPE
	)
	AS
		--
		v_void_fl	   t207_set_master.c207_void_fl%TYPE;
		v_set_status   t207_set_master.c901_status_id%TYPE;
--
	BEGIN
		SELECT		  c207_void_fl, c901_status_id
				 INTO v_void_fl, v_set_status
				 FROM t207_set_master
				WHERE c207_set_id = p_set_id
		FOR UPDATE OF c207_void_fl;

		IF v_void_fl IS NOT NULL
		THEN
			-- Error message is set already voided
			raise_application_error (-20804, '');
		END IF;

		IF ((v_set_status <> 20365) OR (v_set_status <> 20366))
		THEN
			-- Error message is Set is in Approved Status.Cannot void
			raise_application_error (-20805, '');
		END IF;

		UPDATE t207_set_master
		   SET c207_void_fl = 'Y'
			 , c207_last_updated_by = p_user_id
			 , c207_last_updated_date = SYSDATE
		 WHERE c207_set_id = p_set_id;
	END gm_cs_sav_void_set;

--
/********************************************************************************************
 * Description	 : This Procedure will rollback the given	RA to the Initiate Status
 *********************************************************************************************/
--
	PROCEDURE gm_cs_rollback_return (
		p_raid			   IN	t506_returns.c506_rma_id%TYPE
	  , p_userid		   IN	t506_returns.c506_last_updated_by%TYPE
	  , p_canceltypecode   IN	t907_cancel_log.c901_type%TYPE
	)
--
	AS
		v_fl		   		NUMBER;
		v_reprocessdate 	DATE;
		v_type		   		t506_returns.c506_type%TYPE;
		v_consign_id   		t504a_consignment_excess.c504_consignment_id%TYPE;
		v_status_fl    		t504a_consignment_excess.c504a_status_fl%TYPE;
		v_txnid				T412_INHOUSE_TRANSACTIONS.C412_INHOUSE_TRANS_ID%TYPE;
		v_status			T412_INHOUSE_TRANSACTIONS.C412_STATUS_FL%TYPE;
		v_dist_id			t506_returns.c701_distributor_id%TYPE;
		v_rettype_tmp		t506_returns.c506_type%TYPE;
		v_fs_id	   			t506_returns.c701_distributor_id%TYPE;
		v_account			t704_account.c704_account_id%TYPE;
		v_credited_cnt		NUMBER;
		v_out_cur  			TYPES.cursor_type;
		v_excess_status_cnt NUMBER;
	BEGIN
		SELECT c506_status_fl, c501_reprocess_date, c506_type, c701_distributor_id, c704_account_id
		  INTO v_fl, v_reprocessdate, v_type, v_dist_id, v_account
		  FROM t506_returns
		 WHERE c506_rma_id = p_raid;

		IF (v_fl <> 2 AND p_canceltypecode = 90208)
		THEN
		
			/* Get the count of credited parts from RA
				if credited parts are there in RA, then dont allow to rollback
			*/
			SELECT COUNT(1) INTO v_credited_cnt
				FROM t507_returns_item
			WHERE c506_rma_id = p_raid
				AND c507_status_fl = 'C';
			
			IF v_credited_cnt > 0
			THEN
				raise_application_error (-20806, '');
			END IF;
		
			-- Fetch the trasaction details of Consignment Transaction
			gm_pkg_ship_rollback_txn.gm_fch_return_details(p_raid, v_out_cur);
			-- Update the lot status to NULL from CONTROLLED for Voided transactions
			gm_pkg_ship_rollback_txn.gm_sav_rollback_lot_status(v_out_cur,p_userid);
			
			UPDATE t506_returns
			   SET c506_status_fl = 0
				 , c506_last_updated_by = p_userid
				 , c506_last_updated_date = SYSDATE
			 WHERE c506_rma_id = p_raid;
			 
			 -- Rollback for IH and Product Loaner Items Missing Transaction
			 IF v_type IN ('3308','3309')
			 THEN			 
			 	gm_pkg_op_ihloaner_item_txn.gm_rbck_ra_ig_item(p_raid,p_userid);
			 END IF;
			 
		ELSIF (v_fl = 2 AND v_reprocessdate IS NULL AND p_canceltypecode = 90873)
		THEN   -- and (v_type=3301 or v_type=3302)) then
				-- we are getting the excess reconciled count
				SELECT	   COUNT(1)
					  INTO v_excess_status_cnt
					  FROM t504a_consignment_excess
					 WHERE c506_rma_id = p_raid
					 AND c504a_status_fl = '1'
					 AND c504a_void_fl IS NULL;			
				-- If excess already reconciled then need to throw the error message
				IF v_excess_status_cnt <> 0
				THEN
					GM_RAISE_APPLICATION_ERROR('-20999','59',TO_CHAR (v_consign_id));
				END IF;
				
			BEGIN
				SELECT c504_consignment_id
				  INTO v_consign_id   -- GM-EX-
				  FROM t504a_consignment_excess
				 WHERE c506_rma_id = p_raid AND c504a_void_fl IS NULL
				 FOR UPDATE;
			EXCEPTION
				WHEN NO_DATA_FOUND
				THEN
					v_consign_id := '';
			END;

			IF v_consign_id IS NOT NULL
			THEN

				UPDATE t504_consignment
				   SET c504_void_fl = 'Y'
					 , c504_last_updated_date = SYSDATE
					 , c504_last_updated_by = p_userid
				 WHERE c504_consignment_id = v_consign_id;

				UPDATE t504a_consignment_excess
				   SET c504a_void_fl = 'Y'
					 , c504a_last_updated_date = SYSDATE
					 , c504a_last_updated_by = p_userid
				 WHERE c506_rma_id = p_raid;
			END IF;
			
			-- Fetch the trasaction details of Consignment Transaction
			gm_pkg_ship_rollback_txn.gm_fch_return_details(p_raid, v_out_cur);
			-- Update the lot status to NULL from CONTROLLED for Voided transactions
			gm_pkg_ship_rollback_txn.gm_sav_rollback_lot_status(v_out_cur,p_userid);
			
			UPDATE t506_returns
			   SET c506_status_fl = 1
				 , c506_last_updated_by = p_userid
				 , c506_last_updated_date = SYSDATE
				 , c506_credit_date = NULL
				 , c506_update_inv_fl = NULL
			 WHERE c506_rma_id = p_raid;
			 
			 -- Rollback for IH and Product Loaner Items Missing Transaction
			 IF v_type IN ('3308','3309')
			 THEN
					BEGIN
						
						SELECT C412_INHOUSE_TRANS_ID, C412_STATUS_FL INTO v_txnid, v_status 
							FROM 
							 T412_INHOUSE_TRANSACTIONS 
							 WHERE 
							 C412_REF_ID = p_raid AND C412_VOID_FL IS NULL
							 FOR UPDATE;

					EXCEPTION WHEN NO_DATA_FOUND
					THEN
						v_txnid := NULL;
					END;

			 		IF v_txnid IS NOT NULL
			 		THEN
			 			IF v_status = '4'
			 			THEN
			 			GM_RAISE_APPLICATION_ERROR('-20999','60',p_raid||'#$#'||v_txnid);
			 			END IF;
			 			gm_pkg_op_trans_recon.gm_sav_ln_status(v_txnid,'0',p_userid);
			 		END IF;
			 END IF;
			 
			 -- check dist id is not null (then only update FS warehouse, otherwise its an Order (Account))
			IF v_dist_id IS NOT NULL OR v_type = '3304' --3304 account consignment - items
			THEN
				-- update the FS warehouse Qty
				-- 102901 - Return
				-- 102908 - Return
				-- 4301 Plus (Rollback the credit RA)
				v_fs_id := v_dist_id;
				SELECT DECODE(v_type,'3304',v_type,NULL) INTO v_rettype_tmp FROM DUAL;
				SELECT DECODE(v_type,'3304',v_account,v_fs_id) INTO v_fs_id FROM DUAL;
			
				gm_pkg_op_inv_field_sales_tran.gm_update_fs_inv(p_raid, 'T506', v_fs_id, 102901, 4301, 102908, p_userid, v_rettype_tmp);
			END IF; -- end v_dist_id
			gm_update_inventory_returns (p_raid, 'CR', NULL, p_userid);
			gm_pkg_ret_pend_credit.gm_void_open_ra_credit(p_raid,p_userid);
		ELSIF (p_canceltypecode = 90873)
		THEN
			gm_pkg_ret_pend_credit.gm_void_open_ra_credit(p_raid,p_userid);
		ELSE
			raise_application_error (-20806, '');
		END IF;
	END gm_cs_rollback_return;

---
	PROCEDURE gm_cm_sav_void_job (
		p_job_id	IN	 t9300_job.c9300_job_id%TYPE
	  , p_user_id	IN	 t101_user.c101_user_id%TYPE
	)
/*******************************************************
   * Description : Procedure to void Job
   * Parameters   : 1. Job Id
   *			2. User Id
   *
   *******************************************************/
	AS
		--
		v_void_fl	   t9300_job.c9300_void_fl%TYPE;
--
	BEGIN
		SELECT		  c9300_void_fl
				 INTO v_void_fl
				 FROM t9300_job
				WHERE c9300_job_id = p_job_id
		FOR UPDATE OF c9300_void_fl;

		IF v_void_fl IS NOT NULL
		THEN
			-- Error message is Sorry. Job has already been voided.
			raise_application_error (-20815, '');
		END IF;

		UPDATE t9300_job
		   SET c9300_void_fl = 'Y'
			 , c9300_last_updated_by = p_user_id
			 , c9300_last_updated_date = SYSDATE
		 WHERE c9300_job_id = p_job_id;
	END gm_cm_sav_void_job;

--
	PROCEDURE gm_cr_sav_void_site (
		p_txn_id	IN	 t614_study_site.c614_study_site_id%TYPE
	  , p_user_id	IN	 t101_user.c101_user_id%TYPE
	)
/*******************************************************
 * Description : Procedure to void Site and Study mapping
 * Parameters  : 1. Study Site Id
 *			 2. User Id
 *******************************************************/
	AS
		--
		v_void_fl	   t614_study_site.c614_void_fl%TYPE;
		v_patient_cnt  NUMBER;
--
	BEGIN
		SELECT COUNT (t621.c621_patient_id)
		  INTO v_patient_cnt
		  FROM t621_patient_master t621, t614_study_site t614
		 WHERE t621.c704_account_id = t614.c704_account_id
		   AND t621.c611_study_id = t614.c611_study_id
		   AND t614.c614_study_site_id = p_txn_id
		   AND t621.c621_delete_fl ='N'
           AND t614.c614_void_fl IS NULL;

		IF v_patient_cnt > 0
		THEN
			-- Error message is Can't void site which has patients mapped to it.
			raise_application_error (-20816, '');
		END IF;

		SELECT		  c614_void_fl
				 INTO v_void_fl
				 FROM t614_study_site
				WHERE c614_study_site_id = p_txn_id
		FOR UPDATE OF c614_void_fl;

		IF v_void_fl IS NOT NULL
		THEN
			-- Error message is Sorry, this transaction has already been voided.
			raise_application_error (-20817, '');
		END IF;

		UPDATE t614_study_site
		   SET c614_void_fl = 'Y'
			 , c614_last_updated_by = p_user_id
			 , c614_last_updated_date = SYSDATE
		 WHERE c614_study_site_id = p_txn_id;
	END gm_cr_sav_void_site;

--
	PROCEDURE gm_cr_sav_void_surgeon (
		p_txn_id	IN	 t615_site_surgeon.c615_site_surgeon_id%TYPE
	  , p_user_id	IN	 t101_user.c101_user_id%TYPE
	)
/*******************************************************
 * Description : Procedure to void Surgeon and Site mapping
 * Parameters  : 1. Site Surgeon Id
 *			 2. User Id
 *******************************************************/
	AS
		--
		v_void_fl	   t615_site_surgeon.c615_void_fl%TYPE;
--
	BEGIN
		SELECT		  c615_void_fl
				 INTO v_void_fl
				 FROM t615_site_surgeon
				WHERE c615_site_surgeon_id = p_txn_id
		FOR UPDATE OF c615_void_fl;

		IF v_void_fl IS NOT NULL
		THEN
			-- Error message is Sorry, this transaction has already been voided.
			raise_application_error (-20817, '');
		END IF;

		UPDATE t615_site_surgeon
		   SET c615_void_fl = 'Y'
			 , c615_last_updated_by = p_user_id
			 , c615_last_updated_date = SYSDATE
		 WHERE c615_site_surgeon_id = p_txn_id;
	END gm_cr_sav_void_surgeon;

--
	PROCEDURE gm_cr_sav_void_patient (
		p_txn_id	IN	 t621_patient_master.c621_patient_ide_no%TYPE
	  , p_user_id	IN	 t101_user.c101_user_id%TYPE
	)
/*******************************************************
 * Description : Procedure to void patient
 * Parameters  : 1. Patient   Id
 *			   2. User Id
 *******************************************************/
	AS
		--
		v_void_fl	   t621_patient_master.c621_delete_fl%TYPE;
		v_patient_id   t621_patient_master.c621_patient_id%TYPE;
--
	BEGIN
		-- new variable v_patient_id added 
		  SELECT c621_delete_fl, c621_patient_id
   			INTO v_void_fl, v_patient_id
   			FROM t621_patient_master
  		   WHERE c621_patient_ide_no = p_txn_id FOR UPDATE OF c621_delete_fl;

		IF v_void_fl = 'Y'
		THEN
			-- Error message is Sorry, this transaction has already been voided.
			raise_application_error (-20817, '');
		END IF;

		UPDATE t621_patient_master
		   SET c621_patient_ide_no = p_txn_id || '-D'
			 , c621_delete_fl = 'Y'
			 , c621_last_updated_by = p_user_id
			 , c621_last_updated_date = SYSDATE
		 WHERE c621_patient_ide_no = p_txn_id;
		 --calling below procedure to delete voided patient from v621_patient_study_list
		 gm_pkg_cr_common.gm_sav_site_patient_details(v_patient_id,NULL,NULL);
	END gm_cr_sav_void_patient;

--
 /*******************************************************
	* Description : Procedure to void Purchase Order (Purchasing)
	* Parameters  : 1. PO Id
	*	   2. User Id
	*******************************************************/
	PROCEDURE gm_op_sav_void_purchpo (
		p_txn_id	IN	 t401_purchase_order.c401_purchase_ord_id%TYPE
	  , p_user_id	IN	 t401_purchase_order.c401_created_by%TYPE
	)
	AS
		v_void_fl	   t401_purchase_order.c401_void_fl%TYPE;
		v_cnt		   NUMBER;
		v_potype	   t401_purchase_order.c401_type%TYPE;
		v_vendorid	   t301_vendor.c301_vendor_id%TYPE;

		CURSOR part_details
		IS
			SELECT c205_part_number_id pnum, c402_qty_ordered qty
			  FROM t402_work_order
			 WHERE c401_purchase_ord_id = p_txn_id AND c402_void_fl IS NULL;
--
	BEGIN
		SELECT		  c401_void_fl, c401_type, c301_vendor_id
				 INTO v_void_fl, v_potype, v_vendorid
				 FROM t401_purchase_order
				WHERE c401_purchase_ord_id = p_txn_id
		FOR UPDATE OF c401_void_fl;

		--
		IF v_void_fl = 'Y'
		THEN
			-- Error message is Sorry, this transaction has already been voided.
			raise_application_error (-20817, '');
		END IF;

		SELECT NVL (COUNT (*), 0)
		  INTO v_cnt
		  FROM t402_work_order t402, t408_dhr t408
		 WHERE t402.c401_purchase_ord_id = p_txn_id
		   AND t402.c402_work_order_id = t408.c402_work_order_id
		   AND t408.c408_void_fl IS NULL;

		IF v_cnt > 0
		THEN
			-- Error message is 'DHR Exists for this PO
			raise_application_error (-20818, '');
		END IF;

		--
		UPDATE t401_purchase_order
		   SET c401_void_fl = 'Y'
			 , c401_last_updated_date = SYSDATE
			 , c401_last_updated_by = p_user_id
		 WHERE c401_purchase_ord_id = p_txn_id;

		--

		-- code to call to reverse the posting from rework to quarentine
		IF v_potype = 3101	 -- rework PO
		THEN
			FOR currindex IN part_details
			LOOP
				gm_save_ledger_posting (48086
									  , CURRENT_DATE
									  , currindex.pnum
									  , v_vendorid
									  , p_txn_id
									  , currindex.qty
									  , NULL
									  , p_user_id
									   );

				gm_cm_sav_partqty (currindex.pnum,
                               currindex.qty,
                               p_txn_id,
                               p_user_id,
                               90813, -- quarantine  Qty
                               4301,-- plus
                               4314 -- purchase
                               );
			END LOOP;
		END IF;

--
		UPDATE t402_work_order
		   SET c402_void_fl = 'Y'
			 , c402_last_updated_date = SYSDATE
			 , c402_last_updated_by = p_user_id
		 WHERE c401_purchase_ord_id = p_txn_id;
	END gm_op_sav_void_purchpo;
 
--
 /*******************************************************
	* Description : Procedure to rollback Loaners Ship
	* Parameters  : 1. PO Id
	*	   2. User Id
	*******************************************************/
	PROCEDURE gm_cs_sav_rollback_loaner_ship (
		p_txn_id	IN	 t504a_loaner_transaction.c504_consignment_id%TYPE
	  , p_user_id	IN	 t504a_loaner_transaction.c504a_created_by%TYPE
	  , p_validate  IN   VARCHAR2 DEFAULT 'Y'
	)
	AS
		v_void_fl	   t504a_loaner_transaction.c504a_void_fl%TYPE;
		v_ship_id	   t907_shipping_info.c907_shipping_id%TYPE;
		v_req_det_id   t526_product_request_detail.c526_product_request_detail_id%TYPE;
		v_req_id	   t526_product_request_detail.c525_product_request_id%TYPE;
		v_status       t526_product_request_detail.c526_status_fl%TYPE;
		v_psd		   DATE;
		v_status_fl    NUMBER;
		v_uptfl        char(1) :='N';
		v_req_type     t526_product_request_detail.c901_request_type%TYPE;
--
	BEGIN
			
		v_uptfl := gm_pkg_cm_shipping_trans.get_user_security_event_status(p_user_id,'SHIPOUT_ACCESS');			
			IF v_uptfl <> 'Y' AND p_validate = 'Y' THEN
					GM_RAISE_APPLICATION_ERROR('-20999','61','');
			END IF;
		--validating shipped out set should not rollback
    	BEGIN
    	 SELECT c504a_status_fl INTO v_status
   			FROM t504a_consignment_loaner
  			WHERE c504_consignment_id = p_txn_id
    		AND c504a_void_fl      IS NULL
    		FOR UPDATE;
    	EXCEPTION WHEN NO_DATA_FOUND THEN
           RETURN;
    	END;
    	
    	IF v_status > '10' --allocated-10
		THEN
			GM_RAISE_APPLICATION_ERROR('-20999','64','');
		END IF;
		
		BEGIN
		SELECT	 t504a.c504a_void_fl, t504a.c526_product_request_detail_id, t526.c526_required_date,t526.C901_REQUEST_TYPE
			  INTO v_void_fl, v_req_det_id, v_psd, v_req_type
			  FROM t504a_loaner_transaction t504a, t526_product_request_detail t526
			 WHERE c504_consignment_id = p_txn_id
			   AND t504a.c526_product_request_detail_id = t526.c526_product_request_detail_id
			   AND t526.c526_void_fl IS NULL
			   AND t504a.c504a_void_fl IS NULL 
			   AND t526.c526_status_fl = 20   --allocated
		FOR UPDATE;
		EXCEPTION WHEN NO_DATA_FOUND THEN
           RETURN;
    	END;   	
    	
		BEGIN
		SELECT t526.c525_product_request_id 
		INTO v_req_id
            FROM t526_product_request_detail t526 
            WHERE c526_product_request_detail_id = v_req_det_id; 
		EXCEPTION WHEN NO_DATA_FOUND THEN
            v_req_id := NULL;
    	END;    

		IF v_void_fl = 'Y'
		THEN
			-- Error message is Sorry, this transaction has already been voided.
			raise_application_error (-20817, '');
		END IF;

		BEGIN
			SELECT	   NVL (c907_shipping_id, 0), c907_status_fl, c907_void_fl
				  INTO v_ship_id, v_status_fl, v_void_fl
				  FROM t907_shipping_info
				 WHERE c901_source = 50182 AND c907_ref_id = p_txn_id AND c907_status_fl = 30 AND NVL(c525_product_request_id,-999) = NVL(v_req_id,-999) 
			FOR UPDATE;

			IF v_void_fl = 'Y'
			THEN
				-- Error message is Sorry, this transaction has already been voided.
				raise_application_error (-20817, '');
			END IF;

			UPDATE t907_shipping_info
			   SET c907_ref_id = TO_CHAR (v_req_det_id)
				 , c907_status_fl = 15	 -- pending Control
				 , c907_last_updated_date = SYSDATE
				 , c907_last_updated_by = p_user_id
			 WHERE c907_shipping_id = v_ship_id;
			--change to 58 (Pending FG)  instead of '5' (Allocated)
			-- Call common procedure to update the status
			gm_pkg_op_loaner_set_txn.gm_upd_consign_loaner_status(p_txn_id,'58',p_user_id);
			UPDATE t504a_consignment_loaner
			   SET c504a_loaner_dt = v_psd
				 , c504a_expected_return_dt = NULL
				 , c504a_last_updated_date = SYSDATE
				 , c504a_last_updated_by = p_user_id
			 WHERE c504_consignment_id = p_txn_id;
			 
			 
 	 
			IF v_req_type = 4127 OR v_req_type = 4119 --Product Loaner || In-House Loaner
			THEN
				gm_pkg_allocation.gm_ins_invpick_assign_detail (93604, p_txn_id, p_user_id);
        		--93604 Pending Putaway , to insert record in the t5050_INVPICK_ASSIGN_DETAIL
			END IF;
			
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				-- Error message is Record does not exists
				raise_application_error (-20837, '');
		END;
	END gm_cs_sav_rollback_loaner_ship;

--
--
 /*******************************************************
	* Description : Procedure to void Loaners REturns
	* Parameters  : 1. PO Id
	*	   2. User Id
	*******************************************************/
	PROCEDURE gm_cs_sav_rollback_loaner_retn (
		p_txn_id	IN	 t504a_loaner_transaction.c504_consignment_id%TYPE
	  , p_user_id	IN	 t504a_loaner_transaction.c504a_created_by%TYPE
	)
	AS
		v_void_fl	   t504a_loaner_transaction.c504a_void_fl%TYPE;
		v_cnt		   NUMBER;
--
	BEGIN
		SELECT	   c504a_void_fl
			  INTO v_void_fl
			  FROM t504a_loaner_transaction
			 WHERE c504_consignment_id = p_txn_id AND c504a_return_dt IS NULL AND c504a_void_fl IS NULL
		FOR UPDATE;

		--
		IF v_void_fl = 'Y'
		THEN
			-- Error message is Sorry, this transaction has already been voided.
			raise_application_error (-20817, '');
		END IF;
--
	END gm_cs_sav_rollback_loaner_retn;

--
 /*******************************************************
	* Description : Procedure to void Request
	*******************************************************/
	PROCEDURE gm_cs_sav_void_request (
		p_txn_id   IN	t907_cancel_log.c907_ref_id%TYPE
	  , p_userid   IN	t102_user_login.c102_user_login_id%TYPE
	)
	AS
		v_status_fl    t520_request.c520_status_fl%TYPE;
		v_void_fl	   t520_request.c520_void_fl%TYPE;
		v_request_for  t520_request.c520_request_for%TYPE;
		v_deptid	   t101_user.c901_dept_id%TYPE;
		v_cnid		   t504_consignment.c504_consignment_id%TYPE;
		v_setid			t504_consignment.C207_SET_ID%TYPE;	
		v_shipto		t504_consignment.C504_SHIP_TO%TYPE;
		v_type				t906_rules.c906_rule_value%TYPE;
	BEGIN
		SELECT	   DECODE (c207_set_id, NULL, DECODE (c520_status_fl, 20, 10, 30, 10, c520_status_fl), c520_status_fl)
				 , c520_void_fl , c520_request_for
			  INTO v_status_fl
				 , v_void_fl
				 , v_request_for
			  FROM t520_request
			 WHERE c520_request_id = p_txn_id
		FOR UPDATE;

		IF (v_status_fl >= 20)
		THEN
			raise_application_error (-20802, '');
		END IF;

		--SELECT GET_CODE_NAME(2014) FROM DUAL;
		IF v_void_fl = 'Y'
		THEN
			-- Error message is Sorry, this transaction has already been voided.
			raise_application_error (-20817, '');
		END IF;

		-- Check if the consignment is initiated and
		-- if user not from Logistic department  then system should throw error
		BEGIN
			SELECT	   t504.c504_status_fl,t504.c504_consignment_id, t504.c207_set_id, t504.C504_SHIP_TO
				  INTO v_status_fl, v_cnid , v_setid, v_shipto
				  FROM t504_consignment t504
				 WHERE t504.c520_request_id = p_txn_id
			FOR UPDATE;

			IF v_status_fl = 1
			THEN
				SELECT t101.c901_dept_id
				  INTO v_deptid
				  FROM t101_user t101
				 WHERE c101_user_id = p_userid;

				IF v_deptid <> 2014
				THEN
					-- Error message is Sorry, this transaction cannot be voided.Contact Logistics for changes.
					raise_application_error (-20838, '');
				END IF;
			END IF;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				NULL;
		END;

		IF v_setid IS NULL and v_shipto IS NULL
		THEN
				gm_pkg_op_process_request.gm_save_voided_ps_request(v_cnid,p_userid);
		END IF;
		UPDATE t520_request
		   SET c520_void_fl = 'Y'
			 , c520_last_updated_by = p_userid
			 , c520_last_updated_date = SYSDATE
		 WHERE c520_request_id = p_txn_id;

		-- Below SQL to update consignment information
		UPDATE t504_consignment
		   SET c504_void_fl = 'Y'
			 , c504_last_updated_by = p_userid
			 , c504_last_updated_date = SYSDATE
		 WHERE c520_request_id = p_txn_id;

		--Below SQL to update child record
		UPDATE t520_request
		   SET c520_void_fl = 'Y'
			 , c520_last_updated_by = p_userid
			 , c520_last_updated_date = SYSDATE
		 WHERE c520_master_request_id = p_txn_id;

		gm_void_shipping (p_txn_id, p_userid, 50184);
		-- void the shipping details
		
		/*During void we do not need to do TPR Clear in JNDI-1. 
		  v_type     := get_rule_value ('ADJUST_TPR', 'OP_REQ_UPDATE') ;
		IF NVL(v_type,'N') = 'S' THEN
			gm_pkg_op_inventory_update.gm_op_sav_tpr_clear_request (p_txn_id, 'void', p_userid, NULL);
		END IF;
		*/
		IF v_cnid IS NOT NULL 
		THEN
		 -- to void child CN#
		UPDATE t504_consignment
		   SET c504_void_fl = 'Y'
			 , c504_last_updated_by = p_userid
			 , c504_last_updated_date = SYSDATE
		 WHERE c504_master_consignment_id=v_cnid;
		 
		 -- below two procedures commented because same job doing by trg_consignment_trans_void.trg
			-- Void Records In t5050_invpick_assign_detail for removing allocation in device.
			--gm_pkg_allocation.gm_inv_void_bytxn (v_cnid, '', p_userid);
			-- As the order is deleted, need to void the record in location table to adjust the location qty.
			--gm_pkg_op_item_control_txn.gm_void_control_number_txn (v_cnid, p_userid);
		END IF;
		
		IF (p_userid <> 30301 AND v_request_for <> 26240420)	 ---manual Load
		THEN
			gm_pkg_common_cancel.gm_cs_notify_void_request (p_txn_id, p_userid);
		END IF;
--
	END gm_cs_sav_void_request;

	/*******************************************************
				* Description : Procedure to void product Request
				*******************************************************/
	PROCEDURE gm_cs_sav_void_pdtrequest (
		p_txn_id   IN	t526_product_request_detail.c526_product_request_detail_id%TYPE
	  , p_userid   IN	t102_user_login.c102_user_login_id%TYPE
	)
	AS
		v_flag		   VARCHAR2 (1);
		v_consignid    t504_consignment.c504_consignment_id%TYPE;
		v_hold_fl	   t504_consignment.c504_hold_fl%TYPE;
		v_count		   NUMBER;
		v_type		   NUMBER;
		v_pnum		   t526_product_request_detail.c205_part_number_id%TYPE; 
		v_prod_req_id  t526_product_request_detail.c525_product_request_id%TYPE; 
		v_req_qty	   t526_product_request_detail.c526_qty_requested%TYPE;
		v_status	   t526_product_request_detail.c526_status_fl%TYPE;
		v_prod_type    t525_product_request.c901_request_for%TYPE;
		
	BEGIN
		
		SELECT	   t526.c526_void_fl, t526.c901_request_type,t526.c525_product_request_id
		     , t526.c205_part_number_id, t526.c526_qty_requested, t526.c526_status_fl, t525.c901_request_for
			  INTO v_flag, v_type, v_prod_req_id, v_pnum, v_req_qty, v_status, v_prod_type
			  FROM t526_product_request_detail t526, t525_product_request t525
			 WHERE t526.c525_product_request_id =t525.c525_product_request_id 
			   AND c526_product_request_detail_id = p_txn_id
		FOR UPDATE;

		IF v_flag = 'Y'
		THEN
			-- Error message is Sorry, this transaction has already been voided.
			raise_application_error (-20817, '');
		END IF; 
		
		--validating shipped out set should not void
    	IF v_status = '30'
		THEN
			GM_RAISE_APPLICATION_ERROR('-20999','66','');
		END IF;
		
		-- Validate for PIP & RFP status
		SELECT count(1)
			INTO v_count
		FROM 
			t526_product_request_detail t526
			, t504a_loaner_transaction t504a
			, t504a_consignment_loaner t504cn 
		WHERE t526.c526_product_request_detail_id = p_txn_id 
		AND t526.c526_product_request_detail_id = t504a.c526_product_request_detail_id 
		AND t504a.c504_consignment_id = t504cn.c504_consignment_id 
		AND t504cn.c504a_status_fl IN ('13','16')  ---Packing in Process  or Ready for Pick' 
		AND t526.c526_void_fl IS NULL 
		AND t504a.c504a_void_fl IS NULL 
		AND t504cn.c504a_void_fl IS NULL ;
		
		IF v_count > 0 THEN
			GM_RAISE_APPLICATION_ERROR('-20999','67',p_txn_id);
		END IF;
		----

		IF v_prod_type <> 4119 THEN  --not in-house request 
		
			UPDATE t7104_case_set_information
		    	SET c901_set_location_type = NULL
		      	, c526_product_request_detail_id = NULL
		      	, c7104_last_updated_by = p_userid
		     	, c7104_last_updated_date = SYSDATE
		  		WHERE c526_product_request_detail_id = p_txn_id
		      	AND c7104_void_fl IS NULL;	
		END IF;
		
		
		
		  UPDATE t526_product_request_detail
		   SET c526_void_fl = 'Y'
			 , c526_last_updated_by = p_userid
			 , c526_last_updated_date = SYSDATE
		  WHERE c526_product_request_detail_id = p_txn_id
			AND c526_status_fl < 30
    	    AND c526_void_fl IS NULL;	
		
		/*If request type is inhouse(4119) and item request then check 
		 *If status is allocated/closed then throw error message as 
		 *"The request has already taken care.The associated transactions have been created for this part."
		 * */
		IF v_type = '4119' AND v_pnum IS NOT NULL THEN
			IF v_status IN ('20','30') THEN
		 		raise_application_error (-20586, '');
		 	END IF;
		 	--if all t526 request are voided then update master request(t525) status 
		 	gm_pkg_op_loaner.gm_sav_update_req_status (p_txn_id, p_userid);
		ELSE
			
			 BEGIN
				SELECT t504.c504_consignment_id, t504.c504_hold_fl
				  INTO v_consignid, v_hold_fl
				  FROM t504a_loaner_transaction t504b, t504_consignment t504
				 WHERE t504b.c526_product_request_detail_id = p_txn_id
				   AND t504.c504_consignment_id = t504b.c504_consignment_id
				   AND t504b.c504a_void_fl IS NULL
				   AND t504.c504_void_fl IS NULL;
				   
				EXCEPTION
				WHEN NO_DATA_FOUND
				THEN
					v_consignid := NULL;
					v_hold_fl := NULL;
			END;
			UPDATE t504_consignment
			   SET c504_hold_fl = NULL
				 , c504_last_updated_date = SYSDATE
				 , c504_last_updated_by = p_userid
			 WHERE c504_consignment_id = v_consignid AND c504_void_fl IS NULL;
	
			gm_pkg_op_loaner.gm_sav_update_req_status (p_txn_id, p_userid);
			gm_pkg_op_loaner.gm_sav_release_loaner (p_txn_id, v_type, p_userid);	 -- 4127 product loaner
		
		END IF;

		gm_pkg_op_ln_priority_engine.gm_sav_main_ln_set_priority(v_prod_req_id,p_userid);

	--
	END gm_cs_sav_void_pdtrequest;

	/***********************************************************
				  * Description : Procedure to send Email when Request voided
				  ************************************************************/
	PROCEDURE gm_cs_notify_void_request (
		p_txn_id   IN	VARCHAR2
	  , p_userid   IN	VARCHAR2
	)
	AS
		subject 	   VARCHAR2 (100);
		to_mail 	   VARCHAR2 (200);
		to_priusrmail  VARCHAR2 (200);
		mail_body	   VARCHAR2 (4000);
		v_username	   VARCHAR2 (40);
		v_priuserid    VARCHAR2 (40);
		v_priusername  VARCHAR2 (40);
		v_sheetname    VARCHAR2 (2000);
		v_usrmail	   VARCHAR2 (2000);
		crlf		   VARCHAR2 (2) := CHR (13) || CHR (10);
		v_requestsource t520_request.c901_request_source%TYPE;
		v_masterid	   t4020_demand_master.c4020_demand_master_id%TYPE;
		v_owner 	   VARCHAR2 (4000);
	BEGIN
		SELECT t520.c901_request_source, t520.c520_request_txn_id
		  INTO v_requestsource, v_masterid
		  FROM t520_request t520
		 WHERE t520.c520_request_id = p_txn_id;

		-- If the request is from order planning then
		-- fetch sheet owner
		IF v_requestsource = '50616'
		THEN
			SELECT t4020.c4020_primary_user, t4020.c4020_demand_nm
			  INTO v_priuserid, v_sheetname
			  FROM t4020_demand_master t4020
			 WHERE t4020.c4020_demand_master_id = v_masterid;

			v_priusername := get_user_name (v_priuserid);
			to_priusrmail := ',' || get_user_emailid (v_priuserid);
			v_owner 	:=
				   'The owner of the request is '
				|| v_priusername
				|| crlf
				|| crlf
				|| 'The request was orginated from '
				|| v_sheetname;
		ELSE
			v_owner 	:= NULL;
			to_priusrmail := NULL;
		END IF;

		-- Person who is voiding the record
		v_username	:= get_user_name (p_userid);
		v_usrmail	:= get_user_emailid (p_userid);
		-- Email Area
		to_mail 	:= get_rule_value ('RQTVOID', 'EMAIL') || ',' || v_usrmail || to_priusrmail;
		subject 	:= 'Request ' || p_txn_id || ' made void';
		mail_body	:=
			   'Request '
			|| p_txn_id
			|| ' was made void by '
			|| v_username
			|| ' on '
			|| TO_CHAR (SYSDATE, 'dd-MM-YYHH24:MI:SS')
			|| crlf
			|| crlf
			|| v_owner;
		gm_com_send_email_prc (to_mail, subject, mail_body);
	END gm_cs_notify_void_request;

--
	PROCEDURE gm_fc_sav_void_part_growth (
		p_refid 	IN	 t4030_demand_growth_mapping.c4020_demand_master_id%TYPE
	  , p_user_id	IN	 t101_user.c101_user_id%TYPE
--	 , p_ref_type  IN t4030_demand_growth_mapping.c4030_ref_id%TYPE
	)
	AS
		v_void_fl	   t4030_demand_growth_mapping.c4030_void_fl%TYPE;
/*******************************************************
 * Description : Procedure to void Group Part Growth
 * Parameters  : 1. Group Id
 *			 2. User Id
 *
 *******************************************************/
	BEGIN
		UPDATE t4030_demand_growth_mapping t4030
		   SET t4030.c4030_void_fl = 'Y'
			 , t4030.c4030_last_updated_by = p_user_id
			 , t4030.c4030_last_updated_date = SYSDATE
		 WHERE t4030.c4020_demand_master_id = p_refid AND (t4030.c901_ref_type = 20295 OR t4030.c901_ref_type = 20298);
	END gm_fc_sav_void_part_growth;

/*******************************************************
 * Description : Procedure to Rollback the Status
 * Parameters  : 1. Request Id
 *			 2. User Id
 *
 *******************************************************/
	PROCEDURE gm_status_rollback (
		p_txn_id	IN	 t520_request.c520_request_id%TYPE
	  , p_user_id	IN	 t101_user.c101_user_id%TYPE
	)
	AS
		v_status	   t520_request.c520_status_fl%TYPE;
		v_cur_status   t520_request.c520_status_fl%TYPE;
		v_void_fl	   t520_request.c520_void_fl%TYPE;
		v_cn_id		   t504_consignment.c504_consignment_id%TYPE;
		v_set_id	   t520_request.c207_set_id%TYPE;
		v_country_code VARCHAR2(10);
		v_request_for  t520_request.c520_request_for%TYPE;
		v_source       t907_shipping_info.c901_source%TYPE;
	BEGIN
	
		-- getting country code 
		SELECT GET_RULE_VALUE('CURRENT_DB','COUNTRYCODE')
		INTO v_country_code
		FROM DUAL;
		
		SELECT	   DECODE (c207_set_id, NULL, 15, 20), c520_void_fl, c520_status_fl, c207_set_id, c520_request_for
			  INTO v_status, v_void_fl, v_cur_status, v_set_id, v_request_for
			  FROM t520_request
			 WHERE c520_request_id = p_txn_id
		FOR UPDATE;
		
		--Comment the following code, which is used to prevent rollback from pending shipping for item consignment
		/*IF (v_status = 15)
		THEN
		--	raise_application_error (-20999, 'Cannot perform the desired action for this Consignment ID at this time');
		END IF;
		*/
		
		IF (v_cur_status != 30)
		THEN
			raise_application_error (-20839, '');
		END IF;

		IF v_void_fl = 'Y'
		THEN
			-- Error message is Sorry, this transaction has already been voided.
			raise_application_error (-20817, '');
		END IF;

		UPDATE t520_request t520
		   SET t520.c520_ship_to_id = DECODE(c520_request_for,'26240420',t520.c520_ship_to_id,NULL) -- For Stock Transfer(26240420) should not remove the ship to id
		  -- , t520.c520_request_to = NULL		--request to should not be reset when rollback
			 , t520.c520_status_fl = v_status
			 , c520_last_updated_by = p_user_id
			 , c520_last_updated_date = SYSDATE
		 WHERE t520.c520_request_id = p_txn_id;
		 
		--change update status to built set if it's item consignment, update to pending putaway if it's set consignment.
		-- For OUS, pending putaway is not applicable, it should go back to Built Set
		UPDATE t504_consignment
		    SET c504_status_fl = DECODE(v_country_code,'en',DECODE(v_set_id, NULL, '2', '1.20'),2) 
			 --, c701_distributor_id = NULL-- Commented for --> Need to remove following condition from update for item consignment to come on dashboard. 
			 , c504_ship_date = NULL
			 , c504_ship_to = DECODE(c504_type,'106703',c504_ship_to,'106704',c504_ship_to,NULL)  -- For Stock Transfer(26240420) should not remove the ship to 
			 , c504_ship_to_id = DECODE(c504_type,'106703',c504_ship_to_id,'106704',c504_ship_to_id,NULL)  -- For Stock Transfer(26240420) should not remove the ship to id
			 , c504_last_updated_by = p_user_id
			 , c504_last_updated_date = SYSDATE
		 WHERE c520_request_id = p_txn_id;
		 
		BEGIN
			SELECT c504_consignment_id
				 INTO v_cn_id
			 	 FROM t504_consignment
			 WHERE c520_request_id = p_txn_id
			 AND c504_void_fl is NULL;
		 EXCEPTION
         WHEN NO_DATA_FOUND THEN
         v_cn_id := NULL;
         END;
		---if it�s item consignment , no need to change to putaway in t5050
        IF v_cn_id IS NOT NULL AND v_set_id IS NOT NULL
		THEN
			gm_pkg_allocation.gm_ins_invpick_assign_detail (93604, v_cn_id, p_user_id);
	      	--93604 Pending Putaway , to insert record in the t5050_INVPICK_ASSIGN_DETAIL
		END IF;	 
		--comment following code which is duplicate
		/*UPDATE t520_request t520
		   SET t520.c520_ship_to_id = NULL
			 , t520.c520_request_to = NULL
			 , c520_last_updated_by = p_user_id
			 , c520_last_updated_date = SYSDATE
		 WHERE c520_master_request_id = p_txn_id;*/
		
	--	comment the code, need to keep shipping info when rollback consignment to (built set / pending Putaway) and only need change status
	--	gm_pkg_common_cancel.gm_sav_rollback_shipping (p_txn_id, p_user_id, 50184);
		-- if request for is Stock transfer (26240420) then ship to source is Stock trasfer(26240435) else consignment(50181)
		SELECT DECODE(v_request_for,'26240420',26240435,50181) 
	              INTO v_source
	            FROM DUAL;
		
		UPDATE t907_shipping_info
			   SET c907_status_fl = 15 
				 , c907_last_updated_by = p_user_id
				 , c907_last_updated_date = SYSDATE
			 WHERE c907_ref_id IN (SELECT c504_consignment_id
									FROM t504_consignment
								   WHERE c520_request_id = p_txn_id) AND c901_source = v_source;
	-- call to rollback shipping info
	END gm_status_rollback;

	/*******************************************************
			 * Description : Procedure to Void Tag
			 *******************************************************/
	PROCEDURE gm_sc_sav_void_patag (
		p_txn_id	IN	 t2605_audit_tag_detail.c2605_audit_tag_id%TYPE
	  , p_user_id	IN	 t2605_audit_tag_detail.c2605_last_updated_by%TYPE
	)
	AS
	BEGIN
		UPDATE t2605_audit_tag_detail
		   SET c2605_void_fl = 'Y'
			 , c2605_last_updated_by = p_user_id
			 , c2605_last_updated_date = SYSDATE
		 WHERE c2605_audit_tag_id = p_txn_id;
	END gm_sc_sav_void_patag;

	/*******************************************************
	   * Description : Procedure to void Open Invoices
	   *******************************************************/
	PROCEDURE gm_ac_sav_void_invoice (
		p_txn_id   IN	t503_invoice.c503_invoice_id%TYPE
	  , p_userid   IN	t503_invoice.c503_last_updated_by%TYPE
	  , p_custom_msg OUT	VARCHAR2 -- This Out Param return the voided Adjustemnt Order
	)
	AS
		v_status_fl    t503_invoice.c503_status_fl%TYPE;
		v_void_fl	   t503_invoice.c503_void_fl%TYPE;
		v_invoice_create_date t503_invoice.c503_created_date%TYPE;
		v_order_id t501_order.C501_ORDER_ID%TYPE;
		v_invoice_type   t503_invoice.c901_invoice_type%TYPE;
		v_company_id        t1900_company.c1900_company_id%TYPE;
		v_monthly_comp_id   t1900_company.c1900_company_id%TYPE;
	BEGIN
	
		SELECT c1900_company_id 
	  	  INTO v_company_id 
	      FROM t503_invoice
	     WHERE c503_invoice_id = p_txn_id
	       AND c503_void_fl IS NULL;
	
	-- to get monthly invoice companty id
		SELECT get_rule_value(v_company_id,'MONTHLY_INV_COMP') 
	  	  INTO v_monthly_comp_id 
	  	FROM DUAL;
	
		SELECT c503_status_fl,c901_invoice_type
		 INTO v_status_fl,v_invoice_type
		  FROM t503_invoice
		 WHERE c503_invoice_id = p_txn_id;

		SELECT TRUNC (c503_created_date)
		  INTO v_invoice_create_date
		  FROM t503_invoice
		 WHERE c503_invoice_id = p_txn_id;

		 	 BEGIN
		 select C501_ORDER_ID 
		 into v_order_id
		 from t501_order
		 where c503_invoice_id = p_txn_id
		 and c901_order_type='2524' and c501_void_fl is null;
		 EXCEPTION
         WHEN OTHERS THEN
         v_order_id := NULL;
            END;
		 
		IF v_invoice_create_date <> TRUNC (CURRENT_DATE)
		THEN
			raise_application_error (-20013, '');
		END IF;

		IF v_status_fl = 1
		THEN
			raise_application_error (-20819, '');
		ELSIF v_status_fl = 2
		THEN
			raise_application_error (-20820, '');
		ELSIF v_status_fl = 0
		THEN
-- Update the INvoice Flag to Y
			UPDATE t503_invoice
			   SET c503_void_fl = 'Y'
				 , c503_last_updated_by = p_userid
				 , c503_last_updated_date = CURRENT_DATE
			 WHERE c503_invoice_id = p_txn_id;

--Update Invoice Batch detail void flag to Y
			 UPDATE t9601_batch_details
				SET c9601_void_fl     = 'Y',
				c9601_last_update_by  = p_userid,
				c9601_last_updated_date = CURRENT_DATE
				  WHERE c9601_ref_id IN
				    (
					 SELECT c501_order_id FROM t501_order WHERE c503_invoice_id = p_txn_id and c501_void_fl is NULL
				    )
				   -- AND c901_ref_type  = 18751	---invoice batch
				    AND c9601_void_fl IS NULL; 

-- clear PO only for the Monthly invoice 						
							IF v_company_id = v_monthly_comp_id
							THEN 	
							--Update Invoice attribute details void flag to Y
							
										UPDATE T503b_Invoice_Attribute
											SET C503b_Void_Fl         = 'Y',
											  C503b_Last_Updated_By   = p_userid,
											  C503b_Last_Updated_Date = CURRENT_DATE
											WHERE C503_Invoice_Id     = p_txn_id;	
							
							-- Update the customer PO as null which was generated by system for the Monthly Invoice  				
										UPDATE T501_Order
											SET C501_Customer_Po     = '',
											  C501_Last_Updated_By   = p_userid,
											  C501_Last_Updated_Date = CURRENT_DATE
											WHERE C503_Invoice_Id   = p_txn_id;			    
											    
							END IF;
				    
-- Update the Order table to set the INvoice Id as NULL
			UPDATE t501_order
			   SET c503_invoice_id = NULL
			     , c501_void_fl = DECODE(v_invoice_type,'50202','Y','50203','Y',c501_void_fl)
				 , c501_last_updated_by = p_userid
				 , c501_last_updated_date = CURRENT_DATE
			 WHERE c503_invoice_id = p_txn_id;
			 
			 
-- Update the file info table void fl
			UPDATE t903_upload_file_list
			   SET c903_delete_fl = 'Y'
			   , c903_last_updated_by = p_userid
			   , c903_last_updated_date = CURRENT_DATE
			 WHERE c903_ref_id = p_txn_id
			 AND c901_ref_type = '90906'; 	
			 

		END IF;
		
		IF v_order_id IS NOT NULL
		THEN
		p_custom_msg := 'And adjustment order '||  v_order_id  ||' also voided.';
		ELSE
		p_custom_msg := '';
		END IF;
--
	END gm_ac_sav_void_invoice;

	/*******************************************************
			 * Description : Procedure to rollback order
	   *******************************************************/
	PROCEDURE gm_orders_rollback (
		p_txn_id   IN	t907_cancel_log.c907_ref_id%TYPE
	  , p_userid   IN	t102_user_login.c102_user_login_id%TYPE
	)
	AS
		v_status_fl    t504_consignment.c504_status_fl%TYPE;
	BEGIN
		-- Logic for checking if the Status is 3 (Pending shipping) . Else throw Error message.
		SELECT	   NVL (c501_status_fl, 0)
			  INTO v_status_fl
			  FROM t501_order
			 WHERE c501_order_id = p_txn_id
		FOR UPDATE;

		IF (v_status_fl = 3)
		THEN
			UPDATE t501_order
			   SET c501_status_fl = 2
				 , c501_last_updated_by = p_userid
				 , c501_last_updated_date = SYSDATE
			 WHERE c501_order_id = p_txn_id;
		ELSIF (v_status_fl <> 3)
		THEN
			-- Error message is Set Consignment is not in Shipped Status and cannot be rolled back.
			raise_application_error (-20771, '');
		END IF;

--
		gm_sav_rollback_shipping (p_txn_id, p_userid, 50180);	--orders
	END gm_orders_rollback;

	/*******************************************************
			 * Description : Procedure to update ship info
	   *******************************************************/
	PROCEDURE gm_sav_rollback_shipping (
		p_txn_id	IN	 t907_cancel_log.c907_ref_id%TYPE
	  , p_user_id	IN	 t102_user_login.c102_user_login_id%TYPE
	  , p_source	IN	 t907_shipping_info.c901_source%TYPE
	)
	AS
	BEGIN
		UPDATE t907_shipping_info
		   SET c901_ship_to = NULL
			 , c907_ship_to_id = NULL
			 , c901_delivery_mode = NULL
			 , c901_delivery_carrier = NULL
			 , c907_tracking_number = NULL
			 , c907_frieght_amt = NULL
			 , c907_shipped_dt = NULL
			 , c907_last_updated_by = p_user_id
			 , c907_last_updated_date = SYSDATE
		 WHERE c907_ref_id = p_txn_id AND c901_source = p_source;

		IF (p_source = 50184)	-- if request
		THEN   --rollback CN
			UPDATE t907_shipping_info
			   SET c907_status_fl = 15
				 , c901_ship_to = NULL
				 , c907_ship_to_id = NULL
				 , c901_delivery_mode = NULL
				 , c901_delivery_carrier = NULL
				 , c907_tracking_number = NULL
				 , c907_frieght_amt = NULL
				 , c907_shipped_dt = NULL
				 , c907_last_updated_by = p_user_id
				 , c907_last_updated_date = SYSDATE
			 WHERE c907_ref_id IN (SELECT c504_consignment_id
									FROM t504_consignment
								   WHERE c520_request_id = p_txn_id) AND c901_source = 50181;

			-- rollback backorder if any
			UPDATE t907_shipping_info
			   SET c901_ship_to = NULL
				 , c907_ship_to_id = NULL
				 , c901_delivery_mode = NULL
				 , c901_delivery_carrier = NULL
				 , c907_tracking_number = NULL
				 , c907_frieght_amt = NULL
				 , c907_shipped_dt = NULL
				 , c907_last_updated_by = p_user_id
				 , c907_last_updated_date = SYSDATE
			 WHERE c907_ref_id IN (SELECT c520_request_id
									FROM t520_request
								   WHERE c520_master_request_id = p_txn_id AND c520_void_fl IS NULL)
			   AND c901_source = p_source;
		END IF;

		IF (p_source = 50180)
		THEN   -- if order then also update the status
			UPDATE t907_shipping_info
			   SET c907_status_fl = 15
				 , c907_last_updated_by = p_user_id
				 , c907_last_updated_date = SYSDATE
			 WHERE c907_ref_id = p_txn_id AND c901_source = p_source;
		END IF;
	END gm_sav_rollback_shipping;

	/*******************************************************
		  * Description : Procedure to Void shipping
	   *******************************************************/
	PROCEDURE gm_void_shipping (
		p_txn_id	IN	 t907_cancel_log.c907_ref_id%TYPE
	  , p_user_id	IN	 t102_user_login.c102_user_login_id%TYPE
	  , p_source	IN	 t907_shipping_info.c901_source%TYPE
	)
	AS
	BEGIN
		UPDATE t907_shipping_info
		   SET c907_void_fl = 'Y'
			 , c907_last_updated_by = p_user_id
			 , c907_last_updated_date = SYSDATE
		 WHERE c907_ref_id = p_txn_id AND c901_source = p_source AND c907_status_fl < 40;

		   /* IF (SQL%ROWCOUNT = 0)
			  THEN
				 raise_application_error (-20152, '');
			  END IF;
		*/
		IF p_source = 50184
		THEN
--fetch CN to void
			UPDATE t907_shipping_info
			   SET c907_void_fl = 'Y'
				 , c907_last_updated_by = p_user_id
				 , c907_last_updated_date = SYSDATE
			 WHERE c907_ref_id IN (SELECT c504_consignment_id
									 FROM t504_consignment
									WHERE c520_request_id = p_txn_id) AND c901_source = 50181 AND c907_status_fl < 40;
									
-- fetch the child CN to void
	 UPDATE t907_shipping_info
		SET c907_void_fl     = 'Y'
		  , c907_last_updated_by = p_user_id
		  , c907_last_updated_date = SYSDATE
		  WHERE c907_ref_id IN
		    (SELECT c504_consignment_id
		       FROM t504_consignment t504, t520_request t520
		      WHERE t504.c520_request_id  = t520.c520_master_request_id
		        AND t520.c520_master_request_id = p_txn_id)
		    AND c901_source = 50181
		    AND c907_status_fl < 40;
		    
-- fetch the backorder to void
			UPDATE t907_shipping_info
			   SET c907_void_fl = 'Y'
				 , c907_last_updated_by = p_user_id
				 , c907_last_updated_date = SYSDATE
			 WHERE c907_ref_id IN (SELECT c520_request_id
									 FROM t520_request
									WHERE c520_master_request_id = p_txn_id)
			   AND c901_source = 50184
			   AND c907_status_fl < 40;
			   ELSIF p_source = 50181 --Consignments
            THEN
			   UPDATE t907_shipping_info
           			SET c907_void_fl = 'Y'
            		, c907_last_updated_by = p_user_id
           			, c907_last_updated_date = current_date
        		WHERE c907_ref_id = p_txn_id AND c901_source = 4000518 --Returns
           		AND c907_status_fl < 40;
		--PC 5725 in this procedure they missed include void statement for consignment  returns, so I added this condition	   
		END IF; 
	END gm_void_shipping;

	 /*******************************************************
          * Description : Procedure to rollback release download
          *******************************************************/
   PROCEDURE gm_ac_sav_rollback_download (
      p_txn_id   IN   t406_payment.c406_payment_id%TYPE,
      p_userid   IN   t102_user_login.c102_user_login_id%TYPE
   )
   AS
      v_status   t406_payment.c406_status_fl%TYPE;

   BEGIN
      SELECT     t406.c406_status_fl
            INTO v_status
            FROM t406_payment t406
           WHERE t406.c406_payment_id = p_txn_id
      FOR UPDATE;

      IF v_status = 30
      THEN
         raise_application_error (-20821, '');
      END IF;

      UPDATE t406_payment
         SET c406_status_fl = 15,
             c406_release_by = NULL,
             c406_release_dt = NULL,
             c406_last_updated_by = p_userid,
             c406_last_updated_date = SYSDATE
       WHERE c406_payment_id = p_txn_id AND c406_void_fl IS NULL;
       
   END gm_ac_sav_rollback_download;


	/*******************************************************
			   * Description : Procedure to Void A/P Invoice
			 *******************************************************/
	PROCEDURE gm_ac_sav_void_ap_invoice (
		p_txn_id   IN	t406_payment.c406_payment_id%TYPE
	  , p_userid   IN	t406_payment.c406_created_by%TYPE
	)
	AS
		v_status_fl    NUMBER;
	BEGIN
		SELECT	   t406.c406_status_fl
			  INTO v_status_fl
			  FROM t406_payment t406
			 WHERE t406.c406_payment_id = TO_NUMBER (p_txn_id)
		FOR UPDATE;

		IF v_status_fl > 10
		THEN
			raise_application_error (-20821, '');
		END IF;

		UPDATE t406_payment
		   SET c406_void_fl = 'Y'
			 , c406_last_updated_by = p_userid
			 , c406_last_updated_date = SYSDATE
		 WHERE c406_payment_id = TO_NUMBER (p_txn_id);
		 
		 /* In existing scenario, when we void a DHR, payment id is updated to null. For Pmt-2600 We need to load voided DHR report which
          * displays Invoice ID and Invoice Date from t406_payment.so to handle this we are not updating 
       	  * payment id . */
		 /* Whenever invoice is voided the DHR should be able to Invoice again, due to above change user cannot create invoice for the DHR.
		  * For PMT-5090, new column added as c406_old_payment_id in t408_DHR to retain payment id.
		  * The c406_payment_id column is updated to null again (the above change is reverted).
		  */
		 UPDATE t408_dhr
   			SET c406_old_payment_id = c406_payment_id
             , c406_payment_id = NULL
             , c408_payment_fl = NULL
			 , c408_last_updated_by = p_userid
			 , c408_last_updated_date = SYSDATE
		 WHERE c406_payment_id = TO_NUMBER (p_txn_id);
	END gm_ac_sav_void_ap_invoice;
	
	/*******************************************************
			 * Description : Procedure to rollback release payment
			 *******************************************************/
	PROCEDURE gm_ac_sav_rollback_payment (
		p_txn_id   IN	t406_payment.c406_payment_id%TYPE
	  , p_userid   IN	t102_user_login.c102_user_login_id%TYPE
	)
	AS
		v_status	   t406_payment.c406_status_fl%TYPE;
	BEGIN
		SELECT	   t406.c406_status_fl
			  INTO v_status
			  FROM t406_payment t406
			 WHERE t406.c406_payment_id = p_txn_id
		FOR UPDATE;

		IF v_status = 20
		THEN
			raise_application_error (-20821, '');
		END IF;

		UPDATE t406_payment
		   SET c406_status_fl = 10
			 , c406_last_updated_by = p_userid
			 , c406_last_updated_date = SYSDATE
		 WHERE c406_payment_id = p_txn_id AND c406_void_fl IS NULL;
	END gm_ac_sav_rollback_payment;

	   /*******************************************************
	* Description : Procedure to void a tag
	*******************************************************/
	PROCEDURE gm_void_tag (
		p_txn_id   IN	t5010_tag.c5010_tag_id%TYPE
	  , p_userid   IN	t5010_tag.c5010_last_updated_by%TYPE
	)
	AS
		v_location_type t5010_tag.c901_location_type%TYPE;
		v_tag_status	 t5010_tag.c901_status%TYPE;
	BEGIN
		SELECT c901_location_type,c901_status
		  INTO v_location_type, v_tag_status
		  FROM t5010_tag
		 WHERE c5010_tag_id = p_txn_id AND c5010_void_fl IS NULL;
		
		IF v_tag_status = 102401 -- Transfered
		THEN
			GM_RAISE_APPLICATION_ERROR('-20999','62',''); -- Cannot Void for Transfered tag
		END IF;
		
		IF v_location_type != 4120
		THEN
			UPDATE t5010_tag
			   SET c5010_void_fl = 'Y'
				 , c5010_last_updated_by = p_userid
				 , c5010_last_updated_date = SYSDATE
			 WHERE c5010_tag_id = p_txn_id AND c5010_void_fl IS NULL;
		ELSE
			raise_application_error (-20822, '');
		END IF;
	END gm_void_tag;

	/*******************************************************
	* Description : Procedure to unlink a tag
	*******************************************************/
	PROCEDURE gm_unlink_tag (
		p_txn_id   IN	t5010_tag.c5010_tag_id%TYPE
	  , p_userid   IN	t5010_tag.c5010_last_updated_by%TYPE
	)
	AS
		v_lockfl	   t5010_tag.c5010_lock_fl%TYPE;
		v_cnum		   t5010_tag.c5010_control_number%TYPE;
		v_trans_id	   t5010_tag.c5010_last_updated_trans_id%TYPE;
		v_locid 	   t5010_tag.c5010_location_id%TYPE;
		v_pnum		   t5010_tag.c205_part_number_id%TYPE;
		v_trans_type   t5010_tag.c901_trans_type%TYPE;
		v_setid 	   t5010_tag.c207_set_id%TYPE;
		v_loctype	   t5010_tag.c901_location_type%TYPE;
		v_status	   t5010_tag.c901_status%TYPE;
		v_inv_type     t5010_tag.c901_inventory_type%TYPE;
		v_tag_log_id   t5011_tag_log.c5011_tag_txn_log_id%TYPE;
		v_updated_trans_id t5011_tag_log.c5011_last_updated_trans_id%TYPE;
		v_tag_status	 t5010_tag.c901_status%TYPE;

	BEGIN
		-- validate the transfered tag.
		SELECT c901_status INTO v_tag_status FROM t5010_tag WHERE c5010_tag_id = p_txn_id;
		
		IF v_tag_status = 102401 -- Transfered
		THEN
			GM_RAISE_APPLICATION_ERROR('-20999','63',''); -- Cannot Unlink for Transfered tag
		END IF;
		
		-- fetch the last updated transaction id (Ref id)  
        SELECT c5011_last_updated_trans_id
        INTO v_updated_trans_id
  		 FROM t5011_tag_log
 		 WHERE c5011_tag_txn_log_id IN
   		 (
         	SELECT MAX (c5011_tag_txn_log_id)
          	 FROM t5011_tag_log
         	 WHERE c5010_tag_id = p_txn_id
    	  ) ;

		SELECT c5011_control_number, c5011_last_updated_trans_id, c5011_location_id, c205_part_number_id
			 , c901_trans_type, c207_set_id, c901_location_type, c901_status, c901_inventory_type
		  INTO v_cnum, v_trans_id, v_locid, v_pnum
			 , v_trans_type, v_setid, v_loctype, v_status, v_inv_type
		  FROM t5011_tag_log
		 WHERE c5011_tag_txn_log_id = (SELECT MIN (c5011_tag_txn_log_id)
										 FROM (SELECT	t5011.c5011_tag_txn_log_id
												   FROM t5011_tag_log t5011
												  WHERE t5011.c5010_tag_id = p_txn_id
												  AND NVL(t5011.c5011_last_updated_trans_id,'-999') !=NVL(v_updated_trans_id,'-999')
												  AND NVL(t5011.c901_location_type,'-999') != 4120
											   ORDER BY c5011_tag_txn_log_id DESC)
										WHERE ROWNUM =1);

		IF v_lockfl IS NULL
		THEN
			UPDATE t5010_tag
			   SET c5010_control_number = v_cnum
				 , c5010_last_updated_trans_id = v_trans_id
				 , c5010_location_id = v_locid
				 , c205_part_number_id = v_pnum
				 , c901_trans_type = v_trans_type
				 , c207_set_id = v_setid
				 , c901_location_type = v_loctype
				 , c5010_last_updated_by = p_userid
				 , c5010_last_updated_date = SYSDATE
				 , c901_status = v_status
				-- , c901_inventory_type  = v_inv_type  -- if status is released (51011) then inv type is not updated. So, here update the inv type.
			 WHERE c5010_tag_id = p_txn_id AND c5010_void_fl IS NULL;
		ELSE
			raise_application_error (-20822, '');
		END IF;
	END gm_unlink_tag;

	/*************************************************
	* Description : Procedure to Rollback Demand Sheet from Approved to Open
	************************************************/
	PROCEDURE gm_op_rollback_status (
		p_txn_id   IN	t4040_demand_sheet.c4040_demand_sheet_id%TYPE
	  , p_userid   IN	t102_user_login.c102_user_login_id%TYPE
	)
	AS
		v_status_fl    t4040_demand_sheet.c901_status%TYPE;
		v_comp_id 	    t4020_demand_master.C901_COMPANY_ID%TYPE;	
		v_level_id 			t4040_demand_sheet.C901_LEVEL_ID%TYPE;
		v_level_value 			t4040_demand_sheet.C901_LEVEL_VALUE%TYPE;
		v_demand_mas_id 	t4040_demand_sheet.C4020_DEMAND_MASTER_ID%TYPE;
		v_dmd_type_id 		t4020_demand_master.C901_DEMAND_TYPE%TYPE;
		v_templpt_id 		t4020_demand_master.C4020_PARENT_DEMAND_MASTER_ID%TYPE;
		v_inventory_id  	t4040_demand_sheet.C250_INVENTORY_LOCK_ID%TYPE;
	BEGIN
		SELECT	   c901_status
			  INTO v_status_fl
			  FROM t4040_demand_sheet
			 WHERE c4040_demand_sheet_id = p_txn_id
		FOR UPDATE;

		-- If Sheet is not Approved (50551) then throw error
		IF v_status_fl <> '50551'
		THEN
			-- Error message is Sorry. Only Approved Sheet Status can be changed
			raise_application_error (-20823, '');
		END IF;

		UPDATE t4040_demand_sheet
		   SET c901_status = '50550'
			 , c4040_last_updated_by = p_userid
			 , c4040_last_updated_date = SYSDATE
		 WHERE c4040_demand_sheet_id = p_txn_id AND c4040_void_fl IS NULL;
		 
		  SELECT C901_LEVEL_ID,C4020_DEMAND_MASTER_ID,C250_INVENTORY_LOCK_ID,C901_LEVEL_VALUE INTO v_level_id,v_demand_mas_id,v_inventory_id,v_level_value
		 FROM t4040_demand_sheet WHERE c4040_demand_sheet_id = p_txn_id;
		 
		 IF v_level_id = '102584' OR  (v_level_id ='102581' AND v_level_value='100823') 
		 --102584  Region    ,102581  Division  ,102582 Intermediate   ,100823  US  
		 THEN
		 
		 SELECT C4020_PARENT_DEMAND_MASTER_ID,C901_DEMAND_TYPE,C901_COMPANY_ID
		 	INTO v_templpt_id,v_dmd_type_id , v_comp_id
		 	FROM t4020_demand_master
		 	WHERE C4020_DEMAND_MASTER_ID = v_demand_mas_id;
		 	
		 	-- When a "Editable" Sheet status is modified to "Open",we should open the status of the Zone, Intermediate, Division level sheet status as well. 
		 	UPDATE t4040_demand_sheet 
		 	SET c901_status = '50550'
		 	 , c4040_last_updated_by = p_userid
		 	 , c4040_last_updated_date = SYSDATE
		 	 WHERE c4040_demand_sheet_id IN (
		 		 SELECT c4040_demand_sheet_id 
					FROM t4020_demand_master t4020
						,t4015_global_sheet_mapping t4015 
						,t4040_demand_sheet t4040
					WHERE t4020.C4015_GLOBAL_SHEET_MAP_ID = t4015.C4015_GLOBAL_SHEET_MAP_ID
					AND t4020.C4020_DEMAND_MASTER_ID = t4040.C4020_DEMAND_MASTER_ID
					AND t4020.c4020_void_fl is NULL and t4020.C4020_INACTIVE_FL IS NULL
					AND t4015.c4015_void_fl is NULL 
					and t4020.C901_COMPANY_ID = t4015.C901_COMPANY_ID
					AND t4020.C4020_PARENT_DEMAND_MASTER_ID = v_templpt_id
			        AND t4040.C250_INVENTORY_LOCK_ID = v_inventory_id
					AND t4020.C901_DEMAND_TYPE =v_dmd_type_id
					 AND  ( t4015.c901_level_value) IN (
							SELECT DISTINCT divid FROM v700_territory_mapping_detail WHERE region_id=v_level_value UNION
							SELECT DISTINCT gp_id FROM v700_territory_mapping_detail WHERE region_id=v_level_value UNION
							SELECT  DECODE(v_level_id,'102584', 102603,-1) FROM dual -- OUS Direct
							UNION
							SELECT v_comp_id FROM DUAL
							));
		 
		 END IF;
	END gm_op_rollback_status;

	/*************************************************
	   * Description : Procedure to void Set part mapping
	************************************************/
	PROCEDURE gm_sav_void_set_part_map (
		p_txn_id   IN	t208_set_details.c208_set_details_id%TYPE
	  , p_userid   IN	t102_user_login.c102_user_login_id%TYPE
	)
	AS
		v_void_fl	   t208_set_details.c208_void_fl%TYPE;
		v_set_id	   t208_set_details.c207_set_id%TYPE;
		v_part_number_id t208_set_details.c205_part_number_id%TYPE;
		v_system_id		t207_set_master.C207_SET_SALES_SYSTEM_ID%TYPE;
		v_count			number; -- PC-5506 Tissue Set update from set mapping

		CURSOR v_demand_master_id_cursor
		IS
			SELECT t4021.c4020_demand_master_id demand_master_id
			  FROM t4021_demand_mapping t4021, t208_set_details t208, t4020_demand_master t4020
			 WHERE t4021.c901_ref_type = 40031
			   AND t208.c207_set_id = v_set_id
			   AND t4021.c4021_ref_id = t208.c207_set_id
			   AND t208.c205_part_number_id = v_part_number_id
			   AND t4020.c4020_demand_master_id = t4021.c4020_demand_master_id
			   AND t4020.c4020_void_fl IS NULL;
	BEGIN
		SELECT	   c208_void_fl, c207_set_id, c205_part_number_id
			  INTO v_void_fl, v_set_id, v_part_number_id
			  FROM t208_set_details
			 WHERE c208_set_details_id = p_txn_id
		FOR UPDATE;

		IF v_void_fl = 'Y'
		THEN
			-- Error message is Sorry, this part has already been voided.
			raise_application_error (-20824, '');
		END IF;

		UPDATE t208_set_details
		   SET c208_void_fl = 'Y'
		 	 , c208_set_qty = 0
		     , c208_inset_fl = 'N'   
			 , c208_last_updated_date = SYSDATE
			 , c208_last_updated_by = p_userid
		 WHERE c208_set_details_id = p_txn_id;
		 
		-- PC-5506 Tissue Set update from set mapping
		-- Their is no Tissue part in set c207_tissue_fl will  change Y to  Null
		 SELECT
		    COUNT(1) INTO v_count
		FROM
		    t208_set_details   t208
		    ,t205_part_number   t205
            ,t207_set_master    t207
		WHERE
		    t208.c205_part_number_id = t205.c205_part_number_id
            AND t207.c207_set_id = t208.c207_set_id
            AND t205.c205_product_material = '100845'
		    AND t208.c207_set_id = v_set_id
		    AND t208.c208_void_fl IS NULL		    
            AND t207.C207_VOID_FL IS NULL;
		
		IF  v_count = 0 THEN
			UPDATE t207_set_master
			    SET
			        c207_last_updated_date = current_date,
			        c207_last_updated_by = p_userid,
			        c207_tissue_fl = decode(v_count, 0, NULL, c207_tissue_fl)
			    WHERE
			        c207_set_id = v_set_id;
		END IF;
		 
		--When ever the detail is updated, the set master table has to be updated ,so ETL can Sync it.
		UPDATE t207_set_master
		   SET c207_last_updated_by = p_userid
		     , c207_last_updated_date = SYSDATE
		 WHERE c207_set_id = v_set_id;
		 
		FOR rec IN v_demand_master_id_cursor
		LOOP
			UPDATE t4022_demand_par_detail
			   SET c4022_par_value = 0
				 , c4022_updated_by = p_userid
				 , c4022_updated_date = SYSDATE
			 WHERE c4020_demand_master_id = rec.demand_master_id AND c205_part_number_id = v_part_number_id;
		END LOOP;
		
		BEGIN
			SELECT C207_SET_SALES_SYSTEM_ID INTO  v_system_id
			FROM T207_SET_MASTER 
			WHERE C207_SET_ID=v_set_id
			AND C207_VOID_FL IS NULL;
			 
			-- When a Part is voided from the Set List,it will be treated as an update for Product Catalog.
			-- This procedure will load the information in T2001_MASTER_DATA_UPDATES
			 -- 4000409 Set Part
			 -- 103123   Updated
			 -- 103097   English
			 -- 103100   Product Catalog
			 gm_pkg_pdpc_prodcattxn.gm_sav_master_data_upd (v_set_id , '4000409', '103123', '103097',
	                                                  			'103100', p_userid
	                                                  			, v_system_id,'Y') ;
	    EXCEPTION WHEN OTHERS
	    THEN
	    	null;
	    END;
	END gm_sav_void_set_part_map;

	/*************************************************
	   * Description : Procedure to reopen PO
	************************************************/
	PROCEDURE gm_op_sav_po_reopen (
		p_txn_id   IN	t402_work_order.c402_work_order_id%TYPE
	  , p_userid   IN	t102_user_login.c102_user_login_id%TYPE
	)
	AS
		v_status_fl    t402_work_order.c402_status_fl%TYPE;
	BEGIN
		SELECT	   c402_status_fl
			  INTO v_status_fl
			  FROM t402_work_order
			 WHERE c402_work_order_id = p_txn_id
		FOR UPDATE;

		IF v_status_fl < 3
		THEN
			raise_application_error (-20825, '');
		END IF;

		UPDATE t402_work_order
		   SET c402_status_fl = 2
			 , c402_last_updated_date = SYSDATE
			 , c402_last_updated_by = p_userid
		 WHERE c402_work_order_id = p_txn_id;
	END gm_op_sav_po_reopen;

	/*************************************************
	   * Description : Procedure to disable sent email
	************************************************/
	PROCEDURE gm_op_sav_disable_ship_email (
		p_txn_id   IN	t907_shipping_info.c907_shipping_id%TYPE
	  , p_userid   IN	t102_user_login.c102_user_login_id%TYPE
	)
	AS
		v_status_fl    t907_shipping_info.c907_email_update_fl%TYPE;
		v_uptfl        char(1) :='N';
	BEGIN
		
		v_uptfl := gm_pkg_cm_shipping_trans.get_user_security_event_status(p_userid,'SHIPOUT_ACCESS');			
			IF v_uptfl <> 'Y' THEN
					GM_RAISE_APPLICATION_ERROR('-20999','20','');
			END IF;		
			
		SELECT	   c907_email_update_fl
			  INTO v_status_fl
			  FROM t907_shipping_info
			 WHERE c907_shipping_id = p_txn_id
		FOR UPDATE;

		IF v_status_fl IS NOT NULL
		THEN
			raise_application_error (-20826, '');
		END IF;

		UPDATE t907_shipping_info
		   SET c907_email_update_fl = 'N'
			 , c907_last_updated_by = p_userid
			 , c907_last_updated_date = SYSDATE
		 WHERE c907_shipping_id = p_txn_id;
	END gm_op_sav_disable_ship_email;

	/*************************************************
			 * Description : Procedure to Cancel System Construct
		************************************************/
/*	PROCEDURE gm_pd_void_system_construct (
		p_txn_id   IN	t7003_system_construct.c7003_system_construct_id%TYPE
	  , p_userid   IN	t102_user_login.c102_user_login_id%TYPE
	)
	AS
		v_status_fl    t7003_system_construct.c7003_void_fl%TYPE;
	 v_construct_avail varchar2(1);
	BEGIN
		SELECT	   c7003_void_fl
			  INTO v_status_fl
			  FROM t7003_system_construct
			 WHERE c7003_system_construct_id = p_txn_id
		FOR UPDATE;


	  IF v_status_fl IS NOT NULL
	  THEN
		 raise_application_error (-20198, '');
	  END IF;

	SELECT gm_pkg_pd_group_pricing.get_multi_construct_avail_flg(p_txn_id)
	  INTO v_construct_avail
	  FROM DUAL;
	IF v_construct_avail = 'N'
	THEN
		--Cannot void the construct, since construct has some published group(s).
		raise_application_error (-20285, '');
	END IF;

	  SELECT gm_pkg_pd_group_pricing.get_multi_construct_avail_flg (p_txn_id)
		INTO v_construct_avail
		FROM DUAL;

	  IF v_construct_avail = 'N'
	  THEN
		 --Cannot void the construct, since construct has some published group(s).
		 raise_application_error (-20285, '');
	  END IF;

	  UPDATE t7003_system_construct
		 SET c7003_void_fl = 'Y',
			 c7003_last_updated_by = p_userid,
			 c7003_last_updated_date = SYSDATE
	   WHERE c7003_system_construct_id = p_txn_id;
   END gm_pd_void_system_construct;*/

	/*************************************************
	   * Description : Procedure to void radiograph upload
	************************************************/
	PROCEDURE gm_cl_sav_void_radiograph_upld (
		p_refid    IN	t624_patient_record.c624_patient_rec_id%TYPE
	  , p_userid   IN	t102_user_login.c102_user_login_id%TYPE
	)
	AS
		v_void_fl	   t624_patient_record.c624_void_fl%TYPE;
	/*******************************************************
	 * Description : Procedure to void Radiograph Upload
	 * Parameters  : 1. Group Id
	 *			 2. User Id
	 *
	 *******************************************************/
	BEGIN
		UPDATE t624_patient_record t624
		   SET t624.c624_void_fl = 'Y'
			 , t624.c624_last_updated_by = p_userid
			 , t624.c624_last_updated_date = SYSDATE
		 WHERE t624.c624_patient_rec_id = p_refid;
	END gm_cl_sav_void_radiograph_upld;

--

--
--
   /**********************************************************
	  * Description : Procedure to remove DO from Hold Status
   ***********************************************************/
	PROCEDURE gm_cs_sav_order_remove_hold (
		p_txn_id		 IN   t907_cancel_log.c907_ref_id%TYPE
	  , p_cancelreason	 IN   t907_cancel_log.c901_cancel_cd%TYPE
	  , v_cancelid		 IN   t907_cancel_log.c907_cancel_log_id%TYPE
	  , p_comments		 IN   t907_cancel_log.c907_comments%TYPE
	  , p_userid		 IN   t102_user_login.c102_user_login_id%TYPE
	)
	AS
		v_id		   VARCHAR2 (30);
		p_newid 	   VARCHAR2 (30);
		v_held_fl	   CHAR (1);
		v_void_fl	   CHAR (1);
		v_invfl 	   CHAR (1);
		v_childcnt	   NUMBER;
		v_msg		   VARCHAR2 (100);
		v_cancelreasoncomments VARCHAR2 (5000);
		v_cust_po  	   T501_order.c501_customer_po%TYPE;
		v_acc_id	   NUMBER;
		v_ac_name	   VARCHAR2 (255);
		subject 	   VARCHAR2 (1000);
		mail_body	   VARCHAR2 (30000);
		crlf		   VARCHAR2 (2) := CHR (13) || CHR (10);
		to_mail 	   t906_rules.c906_rule_value%TYPE;
		v_company_id   t501_order.c1900_company_id%TYPE;
		
	BEGIN
		
			--
		SELECT	   c501_hold_fl, c501_void_fl, c704_account_id, get_account_name (c704_account_id), c1900_company_id
			--	,NVL(c501_customer_po,'-999')
			  INTO v_held_fl, v_void_fl, v_acc_id, v_ac_name, v_company_id
			-- 	,v_cust_po
			  FROM t501_order
			 WHERE c501_order_id = p_txn_id
		FOR UPDATE;

------------------------------------------------------------
		subject 	:= 'DO removed from Hold Status - ' || v_ac_name || ' ' || p_txn_id;

----------------------------------------------------------------
		--
		IF (v_void_fl IS NOT NULL)
		THEN
			-- Error Messaage. This order has already been made void.
			raise_application_error (-20801, '');
		ELSIF (v_held_fl IS NULL)
		THEN
			-- Error Messaage. DO is not in 'Hold' status
			raise_application_error (-20827, '');
		ELSE
		
			BEGIN
				-- Hold should be removed only for the order which is selected and not the child order also
				UPDATE t501_order
				   SET c501_hold_fl = NULL
					 , c501_last_updated_by = p_userid
					 , c501_last_updated_date = SYSDATE
				 WHERE c501_order_id = p_txn_id;    ---- PMT-18138 removed condition because child orders should not be released from hold when parent is released.
	
				----- PMT-18138 (Fix PBUG-2695 Child order with discrepancy should be on hold when parent order is released)
				UPDATE t501_order 
					SET c501_hold_fl = NULL 
					, c501_last_updated_by = p_userid 
					, c501_last_updated_date = CURRENT_DATE 
					WHERE c501_order_id IN ( 
                                select c501_order_id FROM (
         							select c501_order_id, SUM(DECODE(item_price,curr_price,0,1)) diff_price
           							FROM (
                 						select t502.c501_order_id 
                      						, t502.c502_item_price item_price 
                      					, NVL(get_account_part_pricing (t501.c704_account_id, t502.c205_part_number_id , get_account_gpo_id (t501.c704_account_id)), 0) curr_price 
                  						from t501_order t501 
                      					, t502_item_order t502 
                  				where t501.c501_order_id = t502.c501_order_id 
                    			and t501.c501_hold_fl ='Y' 
                    			and t501.c501_parent_order_id = p_txn_id
                    			and t501.C501_VOID_FL IS NULL
                    			and t502.C502_VOID_FL IS NULL
                  				)
                				group by c501_order_id          
            					)
     							where diff_price = 0
                          ); 
 				--
				gm_save_status_details (p_txn_id, '', p_userid, NULL, SYSDATE, 91176, 91102);
				--
				v_cancelreasoncomments := get_code_name (p_cancelreason) || ': ' || p_comments;
				gm_update_log (p_txn_id, v_cancelreasoncomments, 1200, p_userid, v_msg);
				--
				 -- to check this order is part of batch, if batch then void this order to batch.
	              -- gm_pkg_ac_ar_batch_trans.gm_sav_batch_hold_po(p_txn_id,v_cust_po,v_acc_id,'REMOVE_HOLD',p_userid);
	                  
				mail_body	:=
					'<style>TD{	FONT-SIZE: 11px; COLOR: #000000; FONT-FAMILY: verdana, arial, sans-serif;}</style><font face=arial size="2">';
				mail_body	:=
					   mail_body
					|| ' <br> Delivered Order ID <b>'
					|| p_txn_id
					|| '</b> has been removed from Hold status.';
				mail_body	:=
					   mail_body
					|| '<br> Reason: ' ||get_code_name (p_cancelreason);
				mail_body	:=
					   mail_body
					|| '<br> Comments: ' || p_comments ;
				mail_body	:= mail_body || '</font>';
				-- get the mail id based on company
				to_mail 	:= get_rule_value_by_company ('DO_REMOVE_HOLD', 'EMAIL',v_company_id);
				
				IF to_mail IS NULL
				THEN
				-- get the mail id of US if the mail id is not updated in rules table for a company
					to_mail 	:= get_rule_value_by_company ('DO_REMOVE_HOLD', 'EMAIL',1000);
				END IF;
				
				COMMIT;
				EXCEPTION WHEN OTHERS
				THEN
					raise_application_error(-20999, SQLERRM);
				END;
			gm_com_send_html_email (to_mail, subject, 'test', mail_body);
		END IF;
			
			
		--
		
	END gm_cs_sav_order_remove_hold;

	/**********************************************************
	   * Description : Procedure to Void IRB Approval
	***********************************************************/
	PROCEDURE gm_cs_sav_void_irb_approval (
		p_txn_id   IN	t907_cancel_log.c907_ref_id%TYPE
	  , p_userid   IN	t102_user_login.c102_user_login_id%TYPE
	)
	AS
		v_void_fl	   t626_irb.c626_void_fl%TYPE;
	BEGIN
		SELECT c626_void_fl
		  INTO v_void_fl
		  FROM t626_irb
		 WHERE c626_irb_id = p_txn_id;

		IF (v_void_fl IS NOT NULL)
		THEN
			-- Error Messaage. This IRB Approval is already voided.
			raise_application_error (-20828, '');
		ELSE
			UPDATE t626_irb
			   SET c626_void_fl = 'Y'
				 , c626_last_updated_by = p_userid
				 , c626_last_updated_date = SYSDATE
			 WHERE c626_irb_id = p_txn_id;

			UPDATE t627_irb_event
			   SET c627_void_fl = 'Y'
				 , c627_last_updated_by = p_userid
				 , c627_last_updated_date = SYSDATE
			 WHERE c626_irb_id = p_txn_id;

			UPDATE t628_irb_media
			   SET c628_void_fl = 'Y'
				 , c628_last_updated_by = p_userid
				 , c628_last_updated_date = SYSDATE
			 WHERE c626_irb_id = p_txn_id;
		END IF;
	END gm_cs_sav_void_irb_approval;

/**********************************************************
	  * Description : Procedure to Void IRB Event
   ***********************************************************/
	PROCEDURE gm_cs_sav_void_irb_event (
		p_txn_id   IN	t907_cancel_log.c907_ref_id%TYPE
	  , p_userid   IN	t102_user_login.c102_user_login_id%TYPE
	)
	AS
		v_void_fl	   t627_irb_event.c627_void_fl%TYPE;
		v_irb_id	   t626_irb.c626_irb_id%TYPE;
		v_norecords    VARCHAR2 (1);
	BEGIN
		SELECT c627_void_fl
		  INTO v_void_fl
		  FROM t627_irb_event
		 WHERE c627_irb_event_id = p_txn_id;

		IF (v_void_fl IS NOT NULL)
		THEN
			-- Error Messaage. This IRB Event is already voided.
			raise_application_error (-20829, '');
		ELSE
			SELECT c626_irb_id
			  INTO v_irb_id
			  FROM t627_irb_event
			 WHERE c627_irb_event_id = p_txn_id AND c627_void_fl IS NULL;

			/*EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
			   v_norecords :='Y';
			   END;*/
			UPDATE t627_irb_event
			   SET c627_void_fl = 'Y'
				 , c627_last_updated_by = p_userid
				 , c627_last_updated_date = SYSDATE
			 WHERE c627_irb_event_id = p_txn_id;

			IF v_irb_id IS NOT NULL
			THEN
				UPDATE t628_irb_media
				   SET c628_void_fl = 'Y'
					 , c628_last_updated_by = p_userid
					 , c628_last_updated_date = SYSDATE
				 WHERE c626_irb_id = v_irb_id;
			END IF;
		END IF;
	END gm_cs_sav_void_irb_event;

	/**********************************************************
	   * Description : Procedure to Void IRB Media
	***********************************************************/
	PROCEDURE gm_cs_sav_void_irb_media (
		p_txn_id   IN	t907_cancel_log.c907_ref_id%TYPE
	  , p_userid   IN	t102_user_login.c102_user_login_id%TYPE
	)
	AS
		v_void_fl	   t628_irb_media.c628_void_fl%TYPE;
	BEGIN
		SELECT c628_void_fl
		  INTO v_void_fl
		  FROM t628_irb_media
		 WHERE c628_irb_media_id = p_txn_id;

		IF (v_void_fl IS NOT NULL)
		THEN
			-- Error Messaage. This IRB Media is already voided.
			raise_application_error (-20830, '');
		ELSE
			UPDATE t628_irb_media
			   SET c628_void_fl = 'Y'
				 , c628_last_updated_by = p_userid
				 , c628_last_updated_date = SYSDATE
			 WHERE c628_irb_media_id = p_txn_id;
		END IF;
	END gm_cs_sav_void_irb_media;

	/**********************************************************
	   * Description : Procedure to Void IRB Media
	***********************************************************/
	PROCEDURE gm_cr_sav_void_patient_log (
		p_txn_id   IN	t907_cancel_log.c907_ref_id%TYPE
	  , p_userid   IN	t102_user_login.c102_user_login_id%TYPE
	)
	AS
		v_void_fl	   t630_patient_log.c621_void_fl%TYPE;
	BEGIN
		SELECT c621_void_fl
		  INTO v_void_fl
		  FROM t630_patient_log
		 WHERE c630_patient_log_id = p_txn_id;

		IF (v_void_fl IS NOT NULL)
		THEN
			-- Error Messaage. Sorry, this pateint log has alredy been voided.
			raise_application_error (-20831, '');
		ELSE
			UPDATE t630_patient_log
			   SET c621_void_fl = 'Y'
				 , c621_last_updated_by = p_userid
				 , c621_last_updated_date = SYSDATE
			 WHERE c630_patient_log_id = p_txn_id;
		END IF;
	END gm_cr_sav_void_patient_log;

	/**********************************************************
	   * Description : Procedure to Void surgeon Training
	***********************************************************/
	PROCEDURE gm_sav_void_training (
		p_txn_id   IN	t907_cancel_log.c907_ref_id%TYPE
	  , p_userid   IN	t102_user_login.c102_user_login_id%TYPE
	)
	AS
		v_void_fl	   t635_clinical_training_record.c635_void_fl%TYPE;
	BEGIN
		SELECT c635_void_fl
		  INTO v_void_fl
		  FROM t635_clinical_training_record
		 WHERE c635_clinical_training_rcd_id = p_txn_id;

		IF (v_void_fl IS NOT NULL)
		THEN
			--Record already voided.
			raise_application_error (-20832, '');
		ELSE
			UPDATE t635_clinical_training_record
			   SET c635_void_fl = 'Y'
				 , c635_last_updated_by = p_userid
				 , c635_last_updated_date = SYSDATE
			 WHERE c635_clinical_training_rcd_id = p_txn_id;
		END IF;
	END;

	/*************************************************
	   * Description : Procedure to void currency conversion
	   *Author : Himanshu Patel
	************************************************/
	PROCEDURE gm_cm_sav_void_curr_conv (
		p_txn_id   IN	t907_currency_conv.c907_curr_seq_id%TYPE
	  , p_userid   IN	t102_user_login.c102_user_login_id%TYPE
	)
	AS
		v_void_fl	   t208_set_details.c208_void_fl%TYPE;
		v_cnt		   NUMBER;
		v_company_id        t1900_company.c1900_company_id%TYPE;
		v_curr_trans_comp_id   t1900_company.c1900_company_id%TYPE;
	BEGIN
	
		SELECT c1900_company_id 
	  	  INTO v_company_id 
	      FROM t907_currency_conv
	     WHERE c907_curr_seq_id = p_txn_id;
	
		SELECT get_rule_value_by_company(v_company_id,'CURR_CONV_BY_TRANS',v_company_id) 
	  		INTO v_curr_trans_comp_id 
	  		FROM DUAL;
	
	
		SELECT	   c907_void_fl
			  INTO v_void_fl
			  FROM t907_currency_conv
			 WHERE c907_curr_seq_id = p_txn_id
		FOR UPDATE;

		IF v_void_fl = 'Y'
		THEN
			-- Error message is Sorry, this currency conversion has alredy been voided.
			raise_application_error (-20833, '');
		END IF;
		
		-- For the currency conversion by transaction company, checking if the any one of the transaction received from intransit.
		IF (v_company_id = v_curr_trans_comp_id)
		THEN
			SELECT COUNT(1) INTO v_cnt
				FROM T504e_Consignment_In_Trans
				WHERE C504_Consignment_Id IN
				  (SELECT C907a_Ref_Id
				  FROM T907a_Currency_Trans
				  WHERE C907_Curr_Seq_Id = p_txn_id
				  AND C907a_Void_Fl     IS NULL
				  )
				AND C1900_Company_Id = v_company_id
				AND C504e_Void_Fl   IS NULL
				AND C901_Status     IS NOT NULL;				
		ELSE
			SELECT COUNT (1)
			  INTO v_cnt
			  FROM t907a_currency_trans
			 WHERE c907_curr_seq_id = p_txn_id;
			 
		END IF;
		
		IF v_cnt > 0  
		THEN
			-- Error message is Sorry, cannot void this record, value already been used.
			
			raise_application_error (-20834, '');
			
		ELSIF v_cnt = 0 AND (v_company_id = v_curr_trans_comp_id)  -- voiding the transaction details for currency conversion
		THEN
			UPDATE T907a_Currency_Trans 
			   SET C907a_Void_Fl = 'Y'
			WHERE c907_curr_seq_id = p_txn_id;
		
		END IF;

		UPDATE t907_currency_conv
		   SET c907_void_fl = 'Y'
			 , c907_last_updated_date = SYSDATE
			 , c907_last_updated_by = p_userid
		 WHERE c907_curr_seq_id = p_txn_id;
	END gm_cm_sav_void_curr_conv;

   /*************************************************
		   * Description : Procedure to Cancel System Construct
	   ************************************************/
/*	PROCEDURE gm_pd_void_grp_price_request (
		p_txn_id   IN	t7000_account_price_request.c7000_account_price_request_id%TYPE
	  , p_userid   IN	t102_user_login.c102_user_login_id%TYPE
	)
	AS
		v_status_fl    t7000_account_price_request.c7000_void_fl%TYPE;
	BEGIN
		SELECT c7000_void_fl
			  INTO v_status_fl
			  FROM t7000_account_price_request
			 WHERE c7000_account_price_request_id = p_txn_id
		FOR UPDATE;

		IF v_status_fl IS NOT NULL
		THEN
			raise_application_error (-20211, '');
		END IF;

		UPDATE t7000_account_price_request
		   SET c7000_void_fl = 'Y'
			 , c7000_last_updated_by = p_userid
			 , c7000_last_updated_date = SYSDATE
		 WHERE c7000_account_price_request_id = p_txn_id;
	END gm_pd_void_grp_price_request;*/

	/*******************************************************
	* Description : Procedure to void a any Field tag. Only Damien has the permission
	*******************************************************/
	PROCEDURE gm_void_field_tag (
		p_txn_id   IN	t5010_tag.c5010_tag_id%TYPE
	  , p_userid   IN	t5010_tag.c5010_last_updated_by%TYPE
	)
	AS
		v_void_fl	   t5010_tag.c5010_void_fl%TYPE;
		v_status	 t5010_tag.c901_status%TYPE;
	BEGIN
		SELECT c5010_void_fl
		  INTO v_void_fl
		  FROM t5010_tag
		 WHERE c5010_tag_id = p_txn_id;

		IF v_void_fl IS NOT NULL
		THEN
			raise_application_error (-20835, '');
		END IF;
		
		IF v_status = 102401 -- Transfered
		THEN
			GM_RAISE_APPLICATION_ERROR('-20999','65','');-- Cannot Void for Transfered tag
		END IF;

		UPDATE t5010_tag
		   SET c5010_void_fl = 'Y'
			 , c5010_last_updated_by = p_userid
			 , c5010_last_updated_date = SYSDATE
		 WHERE c5010_tag_id = p_txn_id AND c5010_void_fl IS NULL;
	END gm_void_field_tag;

	/**********************************************************
		 * Description : Procedure to Void Comments
	 ***********************************************************/
	PROCEDURE gm_cs_sav_void_calllog (
		p_log_id	IN	 t902_log.c902_log_id%TYPE
	  , p_user_id	IN	 t902_log.c902_last_updated_by%TYPE
	)
	AS
		--
		v_void_fl	   t902_log.c902_void_fl%TYPE;
--
	BEGIN
		SELECT	   c902_void_fl
			  INTO v_void_fl
			  FROM t902_log
			 WHERE c902_log_id = p_log_id
		FOR UPDATE;

		IF v_void_fl IS NOT NULL
		THEN
			raise_application_error (-20836, '');
		ELSE
			UPDATE t902_log
			   SET c902_void_fl = 'Y'
				 , c902_last_updated_by = p_user_id
				 , c902_last_updated_date = SYSDATE
			 WHERE c902_log_id = p_log_id;
		END IF;
	END gm_cs_sav_void_calllog;

	/**********************************************************
		* Description : Procedure for save void Outstanding tag.
	***********************************************************/
	PROCEDURE gm_sav_cancel_log (
		p_cancellogid	   IN	t907_cancel_log.c907_cancel_log_id%TYPE
	  , p_refid 		   IN	t907_cancel_log.c907_ref_id%TYPE
	  , p_comments		   IN	t907_cancel_log.c907_comments%TYPE
	  , p_cancelreason	   IN	t907_cancel_log.c901_cancel_cd%TYPE
	  , p_canceltypecode   IN	t907_cancel_log.c901_type%TYPE
	  , p_userid		   IN	t907_cancel_log.c907_created_by%TYPE
	)
	AS
		v_cancelid	   NUMBER;
		v_company_id   t1900_company.c1900_company_id%TYPE;
	BEGIN
		SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL')) INTO v_company_id FROM dual; 
		UPDATE t907_cancel_log
		   SET c907_ref_id = p_refid
			 , c907_comments = p_comments
			 , c901_cancel_cd = p_cancelreason
			 , c901_type = p_canceltypecode
			 , c907_last_updated_by = p_userid
			 , c907_last_updated_date = SYSDATE
			 , c907_cancel_date = TRUNC (SYSDATE)
		 WHERE c907_cancel_log_id = p_cancellogid;

		IF SQL%ROWCOUNT = 0
		THEN
			SELECT s907_cancel_log.NEXTVAL
			  INTO v_cancelid
			  FROM DUAL;

			INSERT INTO t907_cancel_log
						(c907_cancel_log_id, c907_ref_id, c907_comments, c901_cancel_cd, c901_type, c907_created_by
					   , c907_created_date, c907_cancel_date, c1900_company_id
						)
				 VALUES (v_cancelid, p_refid, p_comments, p_cancelreason, p_canceltypecode, p_userid
					   , SYSDATE, TRUNC (SYSDATE), v_company_id
						);
		END IF;
	END gm_sav_cancel_log;
END gm_pkg_common_cancel;
/