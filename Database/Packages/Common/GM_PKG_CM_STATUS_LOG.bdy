/* Formatted on 2009/08/24 14:41 (Formatter Plus v4.8.0) */
-- @ "C:\database\Packages\Common\gm_pkg_cm_status_log.bdy";


CREATE OR REPLACE PACKAGE BODY gm_pkg_cm_status_log
IS
	/******************************************************************
	  * Description : Procedure to fetch one transaction status log in the database
	  ****************************************************************/
	PROCEDURE gm_fch_status_log_detail (
		p_refid 	IN		 t905_status_details.c905_ref_id%TYPE
	  , p_outdata	OUT 	 TYPES.cursor_type
	)
	AS
	 v_company_id      t1900_company.c1900_company_id%TYPE;
	 v_date_format     varchar2(20);
	 v_plant_id		   t5040_plant_master.c5040_plant_id%TYPE;
	 v_filter 		   T906_RULES.C906_RULE_VALUE%TYPE;
	BEGIN
		  SELECT get_compid_frm_cntx() ,get_compdtfmt_frm_cntx(),get_plantid_frm_cntx()
            INTO v_company_id, v_date_format, v_plant_id
            FROM dual;
            
          --If the first parameter is NOTNULL it returns the value in second parameter,if the first parameter is NULL it returns the third parameter.  
          SELECT NVL2(GET_RULE_VALUE(v_company_id,'STATUS_LOG_DETAIL'),v_plant_id,v_company_id) --STATUS_LOG_DETAIL:=rule value-plant
 			INTO v_filter 
			FROM DUAL;
            
		OPEN p_outdata
		 FOR
			 SELECT   t905.c905_ref_id refid, get_code_name (t905.c901_type) reftype
					, get_code_name (t905.c901_source) SOURCE, get_user_name (t905.c905_updated_by) upduser
					, TO_CHAR (t905.c905_updated_date, v_date_format||' HH24:Mi') upddate, t905.c901_source sourceid
				 FROM t905_status_details t905
				WHERE t905.c905_ref_id = p_refid 
				AND t905.c905_delete_fl IS NULL
				AND (t905.C1900_company_id  = v_company_id OR t905.c1900_company_id  IN (SELECT c1900_company_id FROM t5041_plant_company_mapping WHERE C5041_PRIMARY_FL = 'Y' AND c5041_void_fl IS NULL AND c5040_plant_id =v_filter ))
			 ORDER BY t905.c905_updated_date DESC;
	END gm_fch_status_log_detail;

	 /******************************************************************
	* Description : Procedure to fetch list of transaction status logs in the database
	*************************************************************** */
	PROCEDURE gm_fch_status_log_list (
		p_source	  IN	   t905_status_details.c901_source%TYPE
	  , p_user		  IN	   t905_status_details.c905_updated_by%TYPE
	  , p_startdate   IN	   VARCHAR2
	  , p_enddate	  IN	   VARCHAR2
	  , p_type		  IN	   t905_status_details.c901_type%TYPE
	  , p_ref_id	  IN 	   t905_status_details.c905_ref_id%TYPE
	  , p_outdata	  OUT	   TYPES.cursor_type
	)
	AS
	 v_company_id      t1900_company.c1900_company_id%TYPE;
	 v_date_format     varchar2(20); 
	BEGIN
		
		  SELECT get_compid_frm_cntx() ,get_compdtfmt_frm_cntx()
            INTO v_company_id, v_date_format
            FROM dual;
    
		OPEN p_outdata
		 FOR
			 SELECT   t905.c905_ref_id refid, get_code_name (t905.c901_type) reftype
					, get_code_name (t905.c901_source) SOURCE, get_user_name (t905.c905_updated_by) upduser
					, t905.c901_source sourceid, TO_CHAR (t905.c905_updated_date, v_date_format||' HH24:Mi AM') upddate
				 FROM t905_status_details t905
				WHERE t905.c905_updated_by = NVL (p_user, t905.c905_updated_by)
				  AND TRUNC (t905.c905_updated_date) BETWEEN NVL (TO_DATE (p_startdate, v_date_format)
																, t905.c905_updated_date
																 )
														 AND NVL (TO_DATE (p_enddate, v_date_format)
																, t905.c905_updated_date
																 )
				  AND t905.c901_source = NVL (p_source, t905.c901_source)
				  AND t905.c901_type = NVL(p_type,t905.c901_type)
                  AND t905.c905_ref_id = NVL(p_ref_id,t905.c905_ref_id)
                  AND t905.C1900_COMPANY_ID = v_company_id
			 ORDER BY t905.c905_updated_date DESC;
	END gm_fch_status_log_list;

/***************************************************************************************
 * Description			 :This procedure is used to Save the Details about a specific Status
 *
 ****************************************************************************************/
	PROCEDURE gm_sav_status_details (
		p_refid 		IN	 t905_status_details.c905_ref_id%TYPE
	  , p_statusflag	IN	 t905_status_details.c905_status_fl%TYPE
	  , p_updatedby 	IN	 t905_status_details.c905_updated_by%TYPE
	  , p_deletefl		IN	 t905_status_details.c905_delete_fl%TYPE
	  , p_updateddate	IN	 t905_status_details.c905_updated_date%TYPE
	  , p_type			IN	 t901_code_lookup.c901_code_id%TYPE
	  , p_source		IN	 t901_code_lookup.c901_code_id%TYPE
	)
	AS
	BEGIN
		IF (p_updatedby IS NULL)
		THEN
			gm_save_status_details (p_refid, p_statusflag, p_updatedby, p_deletefl, NULL, p_type, p_source);
		ELSE
			gm_save_status_details (p_refid, p_statusflag, p_updatedby, p_deletefl, SYSDATE, p_type, p_source);
		END IF;
	END gm_sav_status_details;

/***************************************************************************************
 * Description			 :This procedure is used to Update the Details about a specific status
 *
 ****************************************************************************************/
	PROCEDURE gm_upd_status_details (
		p_refid 	  IN   t905_status_details.c905_ref_id%TYPE
	  , p_updatedby   IN   t905_status_details.c905_updated_by%TYPE
	  , p_type		  IN   t901_code_lookup.c901_code_id%TYPE
	  , p_source	  IN   t901_code_lookup.c901_code_id%TYPE
	)
	AS
	BEGIN
		UPDATE t905_status_details
		   SET c905_updated_by = p_updatedby
			 , c905_updated_date = SYSDATE
			 , c901_type = p_type
		 WHERE c905_ref_id = p_refid AND c901_source = p_source AND c905_updated_by IS NULL
			   AND c905_updated_date IS NULL;
	END gm_upd_status_details;
END gm_pkg_cm_status_log;
/
