CREATE OR REPLACE PACKAGE BODY gm_pkg_sav_leftlink
IS  
 /*****************************************************************
  * Description : This procedure used save tooltip on favourties   
  * Author      : Agilan Singaravel
  *****************************************************************/
   procedure gm_pkg_sav_leftlink_path(
       p_grp_id   IN   t1500_group.c1500_group_id%TYPE,
   	   p_fun_id   IN   t1530_access.c1520_function_id%TYPE,
   	   p_userid   IN   t1530_access.c1530_created_by%TYPE
   )
   AS
	v_func_id 		t1530_access.c1520_function_id%TYPE;
	v_func_nm 		VARCHAR2(4000);
	v_all_func_nm 	VARCHAR2(4000);
	v_grp_name 		VARCHAR2(4000);
	vcnt NUMBER := 1;
   BEGIN	   
	v_func_id := p_fun_id;

	WHILE (vcnt > 0) 
	LOOP

 		BEGIN
  			SELECT t1520.c1520_function_nm, t1530.c1520_parent_function_id 
    	  	  INTO v_func_nm, v_func_id
    	  	  FROM t1530_access t1530, t1520_function_list t1520
   	     	 WHERE t1530.c1500_group_id    = to_char(p_grp_id)  		   
     	   	   AND t1530.c1520_function_id = TO_CHAR(v_func_id)
     	   	   AND t1520.c1520_function_id = t1530.c1520_function_id
     	   	   AND t1530.c1520_function_id NOT LIKE '-%'
     	   	   AND t1530.c1530_void_fl is  null 
     	   	   AND t1520.c1520_void_fl is null
     	  ORDER BY t1530.c1520_parent_function_id ASC;
     	   
  		EXCEPTION 
  		WHEN NO_DATA_FOUND THEN
  			vcnt := 0;
  			v_func_nm := null;
  			v_func_id := null;
  			
  		WHEN TOO_MANY_ROWS THEN
  			SELECT t1520.c1520_function_nm, t1530.c1520_parent_function_id 
    	  	  INTO v_func_nm, v_func_id
    	  	  FROM t1530_access t1530, t1520_function_list t1520
   	     	 WHERE t1530.c1500_group_id    = to_char(p_grp_id)  		   
     	   	   AND t1530.c1520_function_id = TO_CHAR(v_func_id)
     	   	   AND t1520.c1520_function_id = t1530.c1520_function_id
     	   	   AND t1530.c1520_function_id NOT LIKE '-%'
     	   	   AND t1530.c1530_void_fl is  null 
     	   	   AND t1520.c1520_void_fl is null
     	   	   AND ROWNUM =1;
		END;

		IF (v_func_nm is NOT NULL) THEN
			v_all_func_nm := v_func_nm || ' >' || v_all_func_nm ;
		END IF;
	END LOOP;

	BEGIN
		SELECT c1500_group_nm
	  	  INTO v_grp_name
	  	  FROM t1500_group
	 	 WHERE c1500_group_id = to_char(p_grp_id)
	   	   AND c1500_void_fl IS NULL;
	   	   
	 EXCEPTION WHEN NO_DATA_FOUND THEN
	 	v_grp_name := '';
	 END;
 
    v_all_func_nm := v_grp_name || ' >' || SUBSTR (v_all_func_nm, 0, LENGTH(v_all_func_nm) - 1);
 
    UPDATE T1520_FUNCTION_LIST
       SET c1520_left_link_path =  v_all_func_nm 
         , c1520_last_updated_by = p_userid
         , C1520_LAST_UPDATED_DATE = CURRENT_DATE
     WHERE C1520_FUNCTION_ID = p_fun_id
       AND c1520_void_fl is null;
    
	END gm_pkg_sav_leftlink_path;
    
 END gm_pkg_sav_leftlink; 
/