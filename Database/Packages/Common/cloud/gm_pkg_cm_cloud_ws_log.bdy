--@"C:\Database\Packages\Common\cloud\gm_pkg_cm_cloud_ws_log.bdy";
CREATE OR REPLACE PACKAGE BODY gm_pkg_cm_cloud_ws_log
IS
    /***************************************************************
    * Description : Procedure used to store the cloud web service call details
    *
    * Author     : mmuthusamy
    *************************************************************/
PROCEDURE gm_sav_cloud_web_service_dtl (
        p_cloud_url     IN T9170_CLOUD_WEB_SERVICE_LOG.C9170_CLOUD_URL%TYPE,
        p_method_name   IN T9170_CLOUD_WEB_SERVICE_LOG.C9170_METHOD_NAME%TYPE,
        p_json_request  IN T9170_CLOUD_WEB_SERVICE_LOG.C9170_JSON_REQUEST_DATA%TYPE,
        p_ref_id		IN T9170_CLOUD_WEB_SERVICE_LOG.C9170_REF_ID%TYPE,
        p_ws_action 	IN T9170_CLOUD_WEB_SERVICE_LOG.C9170_WEB_SERVICE_ACTION%TYPE,
        p_cloud_type    IN T9170_CLOUD_WEB_SERVICE_LOG.C901_CLOUD_TYPE%TYPE,
        p_response_code IN T9170_CLOUD_WEB_SERVICE_LOG.C9170_RESPONSE_CODE%TYPE,
        p_response_msg  IN T9170_CLOUD_WEB_SERVICE_LOG.C9170_RESPONSE_MESSAGE%TYPE,
        p_userid        IN T9170_CLOUD_WEB_SERVICE_LOG.C9170_LAST_UPDATED_BY%TYPE)
AS
BEGIN
     INSERT
       INTO T9170_CLOUD_WEB_SERVICE_LOG
        (
            C9170_CLOUD_URL, C9170_METHOD_NAME, C9170_JSON_REQUEST_DATA
          , C901_CLOUD_TYPE, C9170_RESPONSE_CODE, C9170_RESPONSE_MESSAGE
          , C9170_WEB_SERVICE_ACTION, C9170_REF_ID, C9170_LAST_UPDATED_BY
          , C9170_LAST_UPDATED_DATE
        )
        VALUES
        (
            p_cloud_url, p_method_name, p_json_request
          , p_cloud_type, p_response_code, p_response_msg
          , p_ws_action, p_ref_id, p_userid
          , CURRENT_DATE
        ) ;
    --
END gm_sav_cloud_web_service_dtl;

END gm_pkg_cm_cloud_ws_log;
/
