/* Formatted on 03/29/2010 12:25 (Formatter Plus v4.8.0) */
--@"c:\database\packages\common\gm_pkg_cm_query_info.pkg";

CREATE OR REPLACE PACKAGE gm_pkg_cm_query_info
IS
--
/*******************************************************
	 * Description : Procedure to fetch MA qty and cost
	 * Author	: Joe P Kumar
 *******************************************************/
	FUNCTION get_query_status (
		p_id	 IN   t6510_query.c901_status%TYPE
	  , p_type	 IN   t6510_query.c901_query_level_id%TYPE
	)
		RETURN NUMBER;

	/*******************************************************
	 * Description : Procedure to void sitemapping
	 * Parameters	: 1. sitemapping Id
	 *		  2. User Id
	 *
	 *******************************************************/
	PROCEDURE gm_sav_void_sitemapping (
		p_txn_id   IN	t616_site_party_mapping.c616_site_party_id%TYPE
	  , p_userid   IN	t102_user_login.c102_user_login_id%TYPE
	);

	FUNCTION gm_fch_event_des (
		p_study_period_id	IN	 t622_patient_list.c613_study_period_id%TYPE
	  , p_form_id			IN	 t601_form_master.c601_form_id%TYPE
	  , p_patient_id		IN	 t621_patient_master.c621_patient_id%TYPE
	  , p_patient_list_id	IN	 t622_patient_list.c622_patient_list_id%TYPE
	)
		RETURN VARCHAR2;

	FUNCTION get_display_text (
		p_id   IN	t6511_query_longdesc.c6511_query_longdesc_id%TYPE
	)
		RETURN VARCHAR2;

	/*******************************************************
	 * Description : Procedure to fetch MA qty and cost
	 * Author	: Joe P Kumar
	 *******************************************************/
	PROCEDURE gm_sav_query_details (
		p_query_id				IN OUT	 t6510_query.c6510_query_id%TYPE
	  , p_query_level_id		IN		 t6510_query.c901_query_level_id%TYPE
	  , p_patient_list_id		IN		 t622_patient_list.c622_patient_list_id%TYPE
	  , p_patient_ans_list_id	IN		 t623_patient_answer_list.c623_patient_ans_list_id%TYPE
	  , p_user_id				IN		 t102_user_login.c102_user_login_id%TYPE
	  , p_query_text			IN		 t6511_query_longdesc.c6511_query_text%TYPE
	  , p_status				IN		 t6510_query.c901_status%TYPE
	);

	PROCEDURE gm_sav_query_longdesc (
		p_query_id	   IN OUT	t6510_query.c6510_query_id%TYPE
	  , p_status	   IN		t6510_query.c901_status%TYPE
	  , p_user_id	   IN		t102_user_login.c102_user_login_id%TYPE
	  , p_query_text   IN		t6511_query_longdesc.c6511_query_text%TYPE
	);

	PROCEDURE gm_sav_query (
		p_query_id				IN OUT	 t6510_query.c6510_query_id%TYPE
	  , p_query_level_id		IN		 t6510_query.c901_query_level_id%TYPE
	  , p_study_site_id 		IN		 t614_study_site.c614_study_site_id%TYPE
	  , p_short_desc			IN		 VARCHAR
	  , p_study_id				IN		 t621_patient_master.c611_study_id%TYPE
	  , p_form_id				IN		 t622_patient_list.c601_form_id%TYPE
	  , p_patient_id			IN		 t621_patient_master.c621_patient_id%TYPE
	  , p_study_period_id		IN		 t622_patient_list.c613_study_period_id%TYPE
	  , p_account_id			IN		 t614_study_site.c704_account_id%TYPE
	  , p_patient_list_id		IN		 t622_patient_list.c622_patient_list_id%TYPE
	  , p_patient_ans_list_id	IN		 t623_patient_answer_list.c623_patient_ans_list_id%TYPE
	  , p_status				IN		 t6510_query.c901_status%TYPE
	  , p_user_id				IN		 t102_user_login.c102_user_login_id%TYPE
	);

	/*******************************************************
		* Description : Procedure to void sitemapping
		* Parameters   : 1. Query Id
		*		2. User Id
		*
		*******************************************************/
	PROCEDURE gm_sav_void_query (
		p_txn_id   IN	t6510_query.c6510_query_id%TYPE
	  , p_userid   IN	t102_user_login.c102_user_login_id%TYPE
	);

/*******************************************************
	 * Description : Procedure to fetch outstanding issues(queries)
	 * Parameters	:p_study_site_id
	 *******************************************************/
	FUNCTION gm_fch_outstanding_issues (
		p_study_site_id   IN   t6510_query.c614_study_site_id%TYPE
	)
		RETURN VARCHAR2;

	/*******************************************************
	 * Description : Procedure to CC fetch email ids for query system email
	 * Parameters	:p_study_site_id
	 *******************************************************/
	FUNCTION get_cc_email_ids (
		p_study_site_id   IN   t6510_query.c614_study_site_id%TYPE
	)
		RETURN VARCHAR2;

	/*******************************************************
	 * Description : Procedure to fetch To email ids for query system email
	 * Parameters	:p_study_site_id
	 *******************************************************/
	FUNCTION get_to_email_id (
		p_study_site_id   IN   t6510_query.c614_study_site_id%TYPE
	)
		RETURN VARCHAR2;

	/*******************************************************
	 * Description : Procedure to fetch site info
	 * Parameters	:
	 *******************************************************/
	PROCEDURE gm_fch_site_info (
		p_siteinfo	 OUT   TYPES.cursor_type
	);

	/*******************************************************
	 * Description : Procedure to fetch site info
	 * Parameters	:
	 *******************************************************/
	PROCEDURE gm_fch_patient_info (
		p_study_site_id   IN	   t6510_query.c614_study_site_id%TYPE
	  , p_patientinfo	  OUT	   TYPES.cursor_type
	);
END gm_pkg_cm_query_info;
/
