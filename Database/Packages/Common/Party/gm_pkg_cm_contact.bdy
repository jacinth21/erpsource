/* Formatted on 2009/02/26 18:08 (Formatter Plus v4.8.0) */
-- @"c:\database\packages\common\party\gm_pkg_cm_contact.bdy"

CREATE OR REPLACE PACKAGE BODY gm_pkg_cm_contact
IS
--

	/******************************************************************
	* Description : Procedure to get the contact info for a specific party
	****************************************************************/
	PROCEDURE gm_fch_contactinfo (
		p_partyid				IN		 t101_party.c101_party_id%TYPE
	  , p_partycontactinfocur	OUT 	 TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_partycontactinfocur
		 FOR
			 SELECT   c107_contact_id ID, get_code_name (c901_mode) cmode, c107_contact_type ctype
					, c107_contact_value cvalue, c107_seq_no pref
					, DECODE (UPPER (c107_inactive_fl), 'Y', 'Inactive', 'Active') inactivefl
					, DECODE (UPPER (c107_primary_fl), 'Y', 'Yes', '') primaryfl
				 FROM t107_contact
				WHERE c101_party_id = p_partyid AND c107_void_fl IS NULL
			 ORDER BY c901_mode, c107_primary_fl, c107_seq_no;
	END gm_fch_contactinfo;

	/********************************************************************************
	  * Description : Procedure to get the specific contact info for a specific party
	  ********************************************************************************/
	PROCEDURE gm_fch_editcontactinfo (
		p_partycontactid		IN		 t107_contact.c107_contact_id%TYPE
	  , p_partycontactinfocur	OUT 	 TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_partycontactinfocur
		 FOR
			 SELECT c901_mode contactmode, c107_contact_type contacttype, c107_contact_value contactvalue
				  , c107_seq_no preference, DECODE (UPPER (c107_inactive_fl), 'Y', 'on', 'off') inactiveflag
				  , DECODE (UPPER (c107_primary_fl), 'Y', 'on', 'off') primaryflag
			   FROM t107_contact
			  WHERE c107_void_fl IS NULL AND c107_contact_id = p_partycontactid;
	END gm_fch_editcontactinfo;

	/*******************************************************
	  * Description : Procedure to save party contact info
	  * Parameters	:		1. p_partyId
						2. p_mode
						3. p_contactType
						4. p_contactValue
						5. p_preference
						6. p_inactivefl
	  *
	  *******************************************************/
	PROCEDURE gm_sav_contactinfo (
		p_contactid 	 IN OUT   t107_contact.c107_contact_id%TYPE
	  , p_partyid		 IN 	  t101_party.c101_party_id%TYPE
	  , p_contactmode	 IN 	  t107_contact.c901_mode%TYPE
	  , p_contacttype	 IN 	  t107_contact.c107_contact_type%TYPE
	  , p_contactvalue	 IN 	  t107_contact.c107_contact_value%TYPE
	  , p_preference	 IN 	  t107_contact.c107_seq_no%TYPE
	  , p_primaryfl 	 IN 	  t107_contact.c107_primary_fl%TYPE
	  , p_inactivefl	 IN 	  t107_contact.c107_inactive_fl%TYPE
	  , p_userid		 IN 	  t101_user.c101_user_id%TYPE
	)
	AS
		v_count 	   NUMBER;
		v_primaryfl    VARCHAR2 (1);
		--to be deleted later
		v_party_type   VARCHAR2 (10);
	-- to be deleted later
	BEGIN
		v_primaryfl := p_primaryfl;

		UPDATE t107_contact
		   SET c101_party_id = p_partyid
			 , c901_mode = p_contactmode
			 , c107_contact_type = p_contacttype
			 , c107_contact_value = p_contactvalue
			 , c107_seq_no = p_preference
			 , c107_primary_fl = v_primaryfl
			 , c107_inactive_fl = p_inactivefl
			 , c107_last_updated_by = p_userid
			 , c107_last_updated_date = SYSDATE
		 WHERE c107_contact_id = p_contactid AND c101_party_id = p_partyid AND c107_void_fl IS NULL;

		IF (SQL%ROWCOUNT = 0)
		THEN
			SELECT s107_contact.NEXTVAL
			  INTO p_contactid
			  FROM DUAL;

			SELECT COUNT (*)
			  INTO v_count
			  FROM t107_contact
			 WHERE c101_party_id = p_partyid AND c901_mode = p_contactmode AND c107_primary_fl = 'Y';

			IF (v_count = 0)
			THEN
				v_primaryfl := 'Y';
			END IF;

			INSERT INTO t107_contact
						(c107_contact_id, c101_party_id, c901_mode, c107_contact_type, c107_contact_value, c107_seq_no
					   , c107_primary_fl, c107_inactive_fl, c107_created_by, c107_created_date
						)
				 VALUES (p_contactid, p_partyid, p_contactmode, p_contacttype, p_contactvalue, p_preference
					   , v_primaryfl, p_inactivefl, p_userid, SYSDATE
						);
		END IF;

		IF (v_primaryfl = 'Y')
		THEN
			UPDATE t107_contact
			   SET c107_primary_fl = ''
			 WHERE c901_mode = p_contactmode
			   AND c107_contact_id <> p_contactid
			   AND c101_party_id = p_partyid
			   AND c107_primary_fl = 'Y';
		END IF;

		--below code will be removed later after the shipping proj is done
		IF v_primaryfl = 'Y'
		THEN
			SELECT c901_party_type
			  INTO v_party_type
			  FROM t101_party
			 WHERE c101_party_id = p_partyid;

			IF (v_party_type = '7005' AND p_contactmode = '90450')	 -- 90450 - phone
			THEN
				UPDATE t703_sales_rep
				   SET c703_phone_number = p_contactvalue
				       ,c703_last_updated_by = p_userid
				       ,c703_last_updated_date = SYSDATE
				 WHERE c101_party_id = p_partyid;
			END IF;
			-- MNTTASK-5884 - Added bracked for Party Type. Becase when hit submit for Email and primary flag that time only should update.
			If p_contactmode = 90452 -- 'Email'
				AND (v_party_type = 7007 OR v_party_type = 7005) THEN-- Site Coordinator
					UPDATE t101_user t101
					Set t101.c101_email_id = p_contactvalue
						,t101.c101_last_updated_by = p_userid
						,t101.c101_last_updated_date = SYSDATE
					WHERE t101.C101_PARTY_ID = p_partyid;
			END IF;

			IF v_party_type = '7005' AND p_contactmode = '90451'   -- 90450 - pager
			THEN
				UPDATE t703_sales_rep
				   SET c703_pager_number = p_contactvalue
				       ,c703_last_updated_by = p_userid
				       ,c703_last_updated_date = SYSDATE
				 WHERE c101_party_id = p_partyid;
			END IF;

			IF v_party_type = '7005' AND p_contactmode = '90452'   -- 90450 - email
			THEN
				UPDATE t703_sales_rep
				   SET c703_email_id = p_contactvalue
				       ,c703_last_updated_by = p_userid
				       ,c703_last_updated_date = SYSDATE
				 WHERE c101_party_id = p_partyid;

				UPDATE t101_user t101
				   SET t101.c101_email_id = p_contactvalue
				  	  ,t101.c101_last_updated_by = p_userid
					  ,t101.c101_last_updated_date = SYSDATE
				 WHERE t101.c101_user_id = 
						(SELECT c703_sales_rep_id 
						   FROM t703_sales_rep 
						  WHERE c101_party_id = p_partyid);

			END IF;
		END IF;
	END gm_sav_contactinfo;

	/******************************************************************
		* Description : Function to return the contact value when contact type is passed
		****************************************************************/
	FUNCTION get_contact_value (
		p_partyid		IN	 t107_contact.c101_party_id%TYPE
	  , p_contactmode	IN	 t107_contact.c901_mode%TYPE
	)
		RETURN VARCHAR2
	IS
		v_name	T107_CONTACT.C107_CONTACT_VALUE%TYPE;
	BEGIN
		BEGIN
			SELECT c107_contact_value
			  INTO v_name
			  FROM t107_contact t107
			 WHERE t107.c101_party_id = p_partyid
			   AND c901_mode = p_contactmode
			   AND c107_void_fl IS NULL
			   AND c107_primary_fl = 'Y'
			   AND c107_inactive_fl IS NULL;		
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				RETURN ' ';
		END;

		RETURN v_name;
	END get_contact_value;

	/********************************************************************
 * Description : This function returns the Contact name for the given Type. This is not a part of the Contact Table hence do not pass partyid here
 * Created By BrinalG
 ********************************************************************/
--
	FUNCTION get_contact_name (
		p_id		  IN   t907_shipping_info.c907_ship_to_id%TYPE
	  , p_shipto	  IN   t907_shipping_info.c901_ship_to%TYPE
	  , p_addressid   IN   t907_shipping_info.c106_address_id%TYPE
	)
		RETURN VARCHAR2
	IS
		v_contact_name VARCHAR2 (200);
		v_hfpu		   VARCHAR2 (10);
		v_addtype	   t106_address.c901_address_type%TYPE;
--
	BEGIN
		IF p_shipto = 4120
		THEN
			SELECT c701_contact_person
			  INTO v_contact_name
			  FROM t701_distributor
			 WHERE c701_distributor_id = p_id AND c701_void_fl IS NULL;
		ELSIF p_shipto = 4122
		THEN
			SELECT c704_contact_person
			  INTO v_contact_name
			  FROM t704_account
			 WHERE c704_account_id = p_id AND c704_void_fl IS NULL;
		ELSIF p_shipto = 4121
		THEN
			SELECT c703_sales_rep_name
			  INTO v_contact_name
			  FROM t703_sales_rep
			 WHERE c703_sales_rep_id = p_id AND c703_void_fl IS NULL;
		END IF;

		IF p_addressid != '' OR p_addressid != 0
		THEN
			SELECT c901_address_type
			  INTO v_addtype
			  FROM t106_address
			 WHERE c106_address_id = p_addressid AND c106_void_fl IS NULL;

			IF v_addtype = 90403   -- HFPU
			THEN
				v_contact_name := v_contact_name || ' - HFPU';
			END IF;
		END IF;

		RETURN v_contact_name;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN '';
	END get_contact_name;
	
	/********************************************************************************
		* Description : Function to return the Default Shipment Email for an Account 
		* which is saved from Account Setup Screen - Ship Parameter Tab
		*****************************************************************************/
	FUNCTION get_ship_contact_value (
		p_account_id		IN	 t704_account.c704_account_id%TYPE
	)
		RETURN VARCHAR2
	IS
		v_name		   VARCHAR2 (150);
		v_party_id     t704_account.c101_party_id%TYPE;
	BEGIN
		BEGIN
			BEGIN
				SELECT c101_party_id 
				  INTO v_party_id 
				  FROM t704_account 
				 WHERE c704_account_id = p_account_id 
				   AND c704_void_fl IS NULL;
			EXCEPTION
				WHEN NO_DATA_FOUND
			THEN
				v_party_id := null;
			END;
			
			SELECT C9080_SHIPMENT_EMAIL
			  INTO v_name
  			  FROM T9080_SHIP_PARTY_PARAM
             WHERE c101_party_id = v_party_id
               AND c9080_void_fl IS NULL;		
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				RETURN ' ';
		END;
		RETURN v_name;
	END get_ship_contact_value;	
	
END gm_pkg_cm_contact;
/
