/* Formatted on 2009/05/07 15:52 (Formatter Plus v4.8.0) */
-- @"C:\database\Packages\common\party\gm_pkg_cm_party_info.pkg"

CREATE OR REPLACE PACKAGE gm_pkg_cm_party_info
IS
	/******************************************************************
	  * Description : Procedure to fetch party details
	  ****************************************************************/
	PROCEDURE gm_fch_partyinfo (
		p_partyid	  IN	   t101_party.c101_party_id%TYPE
	  , p_outcursor   OUT	   TYPES.cursor_type
	);

	/******************************************************************
	 * Description : Procedure to fetch the tabs to display on the party setup screen
	 ****************************************************************/
	PROCEDURE gm_fch_setup_displaytabs (
		p_partytype   IN	   t911_action_lookup.c901_party_type%TYPE
	  , p_level 	  IN	   t911_action_lookup.c901_action_level%TYPE
	  , p_outcursor   OUT	   TYPES.cursor_type
	);

	/******************************************************************
	 * Description : Procedure to fetch the comma separated values for party
	 ****************************************************************/
	FUNCTION get_party_other_info (
		p_party_id	 IN   NUMBER
	  , p_type		 IN   NUMBER
	)
		RETURN VARCHAR2;

	/******************************************************************
	 * Description : Procedure to fetch the list of parties for dropdown
	 ****************************************************************/
	PROCEDURE gm_fch_partylist (
      p_partytype   	IN       t101_party.c101_party_id%TYPE,
      p_mastergrpValue 	IN 	 	 VARCHAR2 DEFAULT NULL,
      p_outcursor   	OUT      TYPES.cursor_type
   );

	/******************************************************************
	 * Description : Procedure to fetch the basic info details for the party
	 * summary page
	 ****************************************************************/
	PROCEDURE gm_fch_party_basicinfo (
		p_partyid			  IN	   t101_party.c101_party_id%TYPE
	  , p_nameaddresscursor   OUT	   TYPES.cursor_type
	  , p_contactcursor 	  OUT	   TYPES.cursor_type
	);

	/******************************************************************
	 * Description : Function to fetch the primary address for party
	 ****************************************************************/
	FUNCTION get_party_address (
		p_party_id	 IN   t101_party.c101_party_id%TYPE
	)
		RETURN VARCHAR2;

	/******************************************************************
	 * Description : Procedure to fetch the about me details for the party
	 * for summary page
	 ****************************************************************/
	PROCEDURE gm_fch_party_aboutmeinfo (
		p_partyid	IN		 t101_party.c101_party_id%TYPE
	  , p_cursor	OUT 	 TYPES.cursor_type
	);

	/*****************************************************************************************************
	* This Procedure is to Fetch the Party ID's for the given Search Lookup and Search String
	* Author : Vprasath
	******************************************************************************************************/
	PROCEDURE gm_search_party (
		p_search_str	IN		 VARCHAR2
	  , p_search_type	IN		 VARCHAR2	-- not used now, will contain the id and search type ( '=' or 'LIKE' or 'IN' etc.,)
	  , p_party_type	IN		 t101_party.c901_party_type%TYPE
	  , p_recordset 	OUT 	 TYPES.cursor_type
	);

	/*****************************************************************************************************
	* This Procedure is to Fetch the Party ID's for the given Search Lookup and Search String
	* Author : Vprasath
	******************************************************************************************************/
	PROCEDURE gm_fch_search_lookup (
		p_code_grp	 IN 	  t912_search.c912_search_code_grp%TYPE
	  , p_out_cur	 OUT	  TYPES.cursor_type
	);

	/******************************************************************
	 * Description : Procedure to fetch the fields on party common search screen
	 ****************************************************************/
	PROCEDURE gm_fch_search_filter (
		p_searchgroup	IN		 t912_search.c912_search_grp%TYPE
	  , p_outcursor 	OUT 	 TYPES.cursor_type
	);

	/******************************************************************
	 * Description : Procedure to fetch the filter tabs on common search screen
	 ****************************************************************/
	PROCEDURE gm_fch_search_filter_tabs (
		p_party_type   IN		t101_party.c901_party_type%TYPE
	  , p_outcursor    OUT		TYPES.cursor_type
	);
	/******************************************************************
	 * Description : Function to fetch the third party info (phone, fax)
	 ****************************************************************/
	FUNCTION get_party_contact_info (
		p_party_id	 IN   t101_party.c101_party_id%TYPE
	  , p_mode		 IN   t107_contact.c901_mode%TYPE
	)
		RETURN VARCHAR2;
END gm_pkg_cm_party_info;
/
