/* Formatted on 2008/11/26 17:16 (Formatter Plus v4.8.0) */
--@"C:\database\Packages\common\party\gm_pkg_cm_affiliation.bdy"

CREATE OR REPLACE PACKAGE BODY gm_pkg_cm_affiliation
IS
--
	/******************************************************************
	* Description : Procedure to fetch all affiliations with this party id	in the database
	****************************************************************/
	PROCEDURE gm_fch_affiliationinfo (
		p_partyid	IN		 t1014_party_affiliation.c101_party_id%TYPE
	  , p_outdata	OUT 	 TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_outdata
		 FOR
			 SELECT c1014_party_affiliation_id affid, c101_party_id partyid, c1014_organization_name affname
				  , c1014_committee_name committeename, c1014_role ROLE, c1014_start_year startyear
				  , DECODE (c1014_still_in_this_position, 'Y', 'Present', c1014_end_year) endyear
				  , get_code_name (c901_type) afftype
			   FROM t1014_party_affiliation
			  WHERE c101_party_id = p_partyid AND c1014_void_fl IS NULL;
	END gm_fch_affiliationinfo;

	/******************************************************************
	* Description : Procedure to fetch one affiliation in the database
	****************************************************************/
	PROCEDURE gm_fch_editaffiliationinfo (
		p_partyaffiliationid   IN		t1014_party_affiliation.c1014_party_affiliation_id%TYPE
	  , p_outdata			   OUT		TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_outdata
		 FOR
			 SELECT c1014_party_affiliation_id affid, c101_party_id partyid, c1014_organization_name ORGANIZATION
				  , c1014_committee_name committeename, c1014_role ROLE, c1014_start_year startyear
				  , c1014_end_year endyear, DECODE (c1014_still_in_this_position, 'Y', 'on', 'off') stillinposition
				  , c901_type afftype
			   FROM t1014_party_affiliation
			  WHERE c1014_party_affiliation_id = p_partyaffiliationid AND c1014_void_fl IS NULL;
	END gm_fch_editaffiliationinfo;

	/******************************************************************
	* Description : Procedure to save affiliation
	****************************************************************/
	PROCEDURE gm_sav_affiliationinfo (
		p_partyaffiliationid   IN OUT	t1014_party_affiliation.c1014_party_affiliation_id%TYPE
	  , p_partyid			   IN		t1014_party_affiliation.c101_party_id%TYPE
	  , p_organization		   IN		t1014_party_affiliation.c1014_organization_name%TYPE
	  , p_type				   IN		t1014_party_affiliation.c901_type%TYPE
	  , p_name				   IN		t1014_party_affiliation.c1014_committee_name%TYPE
	  , p_role				   IN		t1014_party_affiliation.c1014_role%TYPE
	  , p_startyear 		   IN		t1014_party_affiliation.c1014_start_year%TYPE
	  , p_endyear			   IN		t1014_party_affiliation.c1014_end_year%TYPE
	  , p_stillinposition	   IN		t1014_party_affiliation.c1014_still_in_this_position%TYPE
	  , p_user				   IN		t1014_party_affiliation.c1014_created_by%TYPE
	)
	AS
	BEGIN
		UPDATE t1014_party_affiliation
		   SET c101_party_id = p_partyid
			 , c1014_organization_name = p_organization
			 , c901_type = p_type
			 , c1014_committee_name = p_name
			 , c1014_role = p_role
			 , c1014_start_year = p_startyear
			 , c1014_end_year = p_endyear
			 , c1014_still_in_this_position = p_stillinposition
			 , c1014_last_updated_by = p_user
			 , c1014_last_updated_date = SYSDATE
		 WHERE c1014_party_affiliation_id = p_partyaffiliationid AND c1014_void_fl IS NULL;

		IF (SQL%ROWCOUNT = 0)
		THEN
			SELECT s1014_party_affiliation.NEXTVAL
			  INTO p_partyaffiliationid
			  FROM DUAL;

			INSERT INTO t1014_party_affiliation
						(c1014_party_affiliation_id, c101_party_id, c1014_organization_name, c901_type
					   , c1014_committee_name, c1014_role, c1014_start_year, c1014_end_year
					   , c1014_still_in_this_position, c1014_created_by, c1014_created_date
						)
				 VALUES (p_partyaffiliationid, p_partyid, p_organization, p_type
					   , p_name, p_role, p_startyear, p_endyear
					   , p_stillinposition, p_user, SYSDATE
						);
		END IF;
	END gm_sav_affiliationinfo;
END gm_pkg_cm_affiliation;
/
