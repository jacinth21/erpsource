/* Formatted on 2008/11/19 12:46 (Formatter Plus v4.8.0) */
-- @"C:\Database\Packages\common\Party\gm_pkg_cm_education.bdy"

CREATE OR REPLACE PACKAGE BODY gm_pkg_cm_education
IS
	/*****************************************************
	 * Procedure to fetch the education info for the party
	 *****************************************************/
	PROCEDURE gm_fch_educationinfo (
		p_partyid	   IN		t1010_education.c101_party_id%TYPE
	  , p_eduinfocur   OUT		TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_eduinfocur
		 FOR
			 SELECT c1010_education_id ID, get_code_name (c901_education_level) educationlevel, c901_education_level educationlevelid
				  , c1010_institute_nm institutename, c1010_class_year classyear, c1010_degree_nm course
				  , get_code_name (c901_degree_designation) designation, c901_degree_designation designationid, c1010_void_fl voidfl
			   FROM t1010_education
			  WHERE c101_party_id = p_partyid AND c1010_void_fl IS NULL ORDER BY TO_NUMBER(c1010_class_year) DESC;
	END gm_fch_educationinfo;

	/*****************************************************
	 * Procedure to fetch the education info for the party. Called when
	 * user clicks on edit
	 *****************************************************/
	PROCEDURE gm_fch_editeducationinfo (
		p_educationid	IN		 t1010_education.c1010_education_id%TYPE
	  , p_eduinfocur	OUT 	 TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_eduinfocur
		 FOR
			 SELECT c1010_education_id ID, c901_education_level educationlevel, c1010_institute_nm institutename
				  , c1010_class_year classyear, c1010_degree_nm course, c901_degree_designation designation
			   FROM t1010_education
			  WHERE c1010_education_id = p_educationid AND c1010_void_fl IS NULL;
	END gm_fch_editeducationinfo;

	/*****************************************************
	 * Procedure to save the education info for the party
	 *****************************************************/
	PROCEDURE gm_sav_educationinfo (
		p_educationid	   IN OUT	t1010_education.c1010_education_id%TYPE
	  , p_partyid		   IN		t1010_education.c101_party_id%TYPE
	  , p_educationlevel   IN		t1010_education.c901_education_level%TYPE
	  , p_institutenm	   IN		t1010_education.c1010_institute_nm%TYPE
	  , p_classyear 	   IN		t1010_education.c1010_class_year%TYPE
	  , p_course		   IN		t1010_education.c1010_degree_nm%TYPE
	  , p_designation	   IN		t1010_education.c901_degree_designation%TYPE
	  , p_userid		   IN		t1010_education.c1010_created_by%TYPE
	  , p_voidfl		   IN		t1010_education.c1010_void_fl%TYPE default null
	)
	AS
	BEGIN
		UPDATE t1010_education
		   SET c901_education_level = p_educationlevel
			 , c1010_institute_nm = p_institutenm
			 , c1010_class_year = p_classyear
			 , c1010_degree_nm = p_course
			 , c901_degree_designation = p_designation
			 , c1010_last_updated_by = p_userid
			 , c1010_last_updated_date = SYSDATE
			 , c1010_void_fl = p_voidfl
		 WHERE c1010_education_id = p_educationid AND c101_party_id = p_partyid;

		IF (SQL%ROWCOUNT = 0)
		THEN
			SELECT s1010_education.NEXTVAL
			  INTO p_educationid
			  FROM DUAL;

			INSERT INTO t1010_education
						(c1010_education_id, c101_party_id, c901_education_level, c1010_institute_nm, c1010_class_year
					   , c1010_degree_nm, c901_degree_designation, c1010_created_by, c1010_created_date
						)
				 VALUES (p_educationid, p_partyid, p_educationlevel, p_institutenm, p_classyear
					   , p_course, p_designation, p_userid, SYSDATE
						);
		END IF;
	END gm_sav_educationinfo;
END gm_pkg_cm_education;
/
