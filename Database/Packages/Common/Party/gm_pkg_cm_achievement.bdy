/* Formatted on 2008/12/01 13:18 (Formatter Plus v4.8.0) */
--@"c:\database\packages\common\party\gm_pkg_cm_achievement.bdy"

CREATE OR REPLACE PACKAGE BODY gm_pkg_cm_achievement
IS
/******************************************************************
   * Description : Procedure to get the achievement info for a specific party
   ****************************************************************/
	PROCEDURE gm_fch_achievementinfo (
		p_partyid	   IN		t101_party.c101_party_id%TYPE
	  , p_outinfocur   OUT		TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_outinfocur
		 FOR
			 SELECT c1013_party_acheivement_id ID, get_code_name (c901_type) type, c101_party_id partyid
				  , c1013_title title, c1013_detail detail, get_code_name (c901_month) || '  ' || c1013_year dt
				  , NVL (get_investigator_names (c1013_party_acheivement_id), '') coinvestigators
			   FROM t1013_party_acheivement
			  WHERE c101_party_id = p_partyid AND c1013_void_fl IS NULL;
	END gm_fch_achievementinfo;

/******************************************************************
   * Description : Procedure to get the achievement info for a specific party for editting
   ****************************************************************/
	PROCEDURE gm_fch_editachievementinfo (
		p_achievementid   IN	   t1013_party_acheivement.c1013_party_acheivement_id%TYPE
	  , p_outinfocur	  OUT	   TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_outinfocur
		 FOR
			 SELECT c1013_party_acheivement_id ID, c901_type type, c101_party_id partyid, c1013_title title
				  , c1013_detail description, c1013_year YEAR, c901_month MONTH
				  , get_investigator_names (c1013_party_acheivement_id) coinvestigators
			   FROM t1013_party_acheivement
			  WHERE c1013_party_acheivement_id = p_achievementid AND c1013_void_fl IS NULL;
	END gm_fch_editachievementinfo;

	/******************************************************************
	* Description : Procedure to save the achievement info
	****************************************************************/
	PROCEDURE gm_sav_achievementinfo (
	    p_achievementid   IN   t1013_party_acheivement.c1013_party_acheivement_id%TYPE
	  ,	p_partyid		  IN   t1013_party_acheivement.c101_party_id%TYPE
	  , p_type			  IN   t1013_party_acheivement.c901_type%TYPE
	  , p_title 		  IN   t1013_party_acheivement.c1013_title%TYPE
	  , p_detail		  IN   t1013_party_acheivement.c1013_detail%TYPE
	  , p_year			  IN   t1013_party_acheivement.c1013_year%TYPE
	  , p_month 		  IN   t1013_party_acheivement.c901_month%TYPE
	  , p_investigators   IN   VARCHAR2	  
	  , p_userid		  IN   t1013_party_acheivement.c1013_last_updated_by%TYPE
	)
	AS
		v_achievement_id 	t1013_party_acheivement.c1013_party_acheivement_id%TYPE := p_achievementid;
		v_strlen	   		NUMBER := NVL (LENGTH (p_investigators), 0);
		v_string	   		VARCHAR2 (3000) := p_investigators;
		v_substring	 		VARCHAR2 (1000);
		v_investigator		t1013a_co_investigator.c1013a_investigator_nm%TYPE;
		v_investigator_id  	t1013a_co_investigator.c1013a_co_investigator_id%TYPE;				
	BEGIN
		--
		UPDATE t1013_party_acheivement
		   SET c101_party_id = p_partyid
			 , c901_type = p_type
			 , c1013_title = p_title
			 , c1013_detail = p_detail
			 , c1013_year = p_year
			 , c901_month = DECODE (p_month, 0, NULL, p_month)
			 , c1013_last_updated_by = p_userid
			 , c1013_last_updated_date = SYSDATE
		 WHERE c1013_party_acheivement_id = v_achievement_id AND c1013_void_fl IS NULL;

		IF (SQL%ROWCOUNT = 0)
		THEN
			SELECT s1013_party_acheivement.NEXTVAL
			  INTO v_achievement_id
			  FROM DUAL;

			INSERT INTO t1013_party_acheivement
						(c1013_party_acheivement_id, c101_party_id, c901_type, c1013_title, c1013_detail, c1013_year
					   , c901_month, c1013_last_updated_by, c1013_last_updated_date
						)
				 VALUES (v_achievement_id, p_partyid, p_type, p_title, p_detail, p_year
					   , DECODE (p_month, 0, NULL, p_month), p_userid, SYSDATE
						);
		END IF;
		
		DELETE FROM t1013a_co_investigator
			  WHERE c1013_party_acheivement_id = v_achievement_id;

		IF (v_strlen > 0)
		THEN
			IF (INSTR (v_string, ';', -1, 1) <> v_strlen)	-- If last char is not ';', then append it
			THEN
				v_string	:= CONCAT (v_string, ';');
			END IF;

			
			WHILE INSTR (v_string, ';') <> 0
			LOOP
				--
				v_substring := SUBSTR (v_string, 1, INSTR (v_string, ';') - 1);
				v_string	:= NVL(SUBSTR (v_string, INSTR (v_string, ';') + 1), '999');
				--
				v_investigator	:= NULL;
				v_investigator	:= TRIM (v_substring);

				SELECT s1013a_co_investigator.NEXTVAL
				  INTO v_investigator_id
				  FROM DUAL;

				INSERT INTO t1013a_co_investigator
							(c1013a_co_investigator_id, c1013_party_acheivement_id, c1013a_investigator_nm
							 , c101_party_id
							)
					 VALUES (v_investigator_id, v_achievement_id, v_investigator
							 , p_partyid
							);
			END LOOP;
		END IF;
	END gm_sav_achievementinfo;

	/*********************************************************
	* Function to fetch the investigator names for a particular patent
	*********************************************************/
	FUNCTION get_investigator_names (
		p_achievementid   IN   t1013_party_acheivement.c1013_party_acheivement_id%TYPE
	)
		RETURN VARCHAR2
	IS
		investigators	VARCHAR2 (32760) := '';
	BEGIN
	WITH investigator as ( 
		SELECT c1013a_investigator_nm name, c1013a_co_investigator_id id
			FROM t1013a_co_investigator
		 WHERE c1013_party_acheivement_id = p_achievementid
	)

	SELECT MAX(SYS_CONNECT_BY_PATH(name, ';'))
	  INTO investigators
	  FROM (
	  	SELECT name, ROW_NUMBER() OVER( ORDER BY id) AS curr
			FROM investigator
	)
	CONNECT BY curr - 1 = PRIOR curr
	START WITH curr = 1;
	investigators := SUBSTR(investigators, 2);
		RETURN NVL (investigators, '');
	END get_investigator_names;	
END gm_pkg_cm_achievement;
/
