/* Formatted on 2010/06/18 16:55 (Formatter Plus v4.8.0) */
-- @"C:\Database\Packages\Common\Party\gm_pkg_cm_party_info.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_cm_party_info
IS
   /******************************************************************
    * Description : Procedure to fetch party details
    ****************************************************************/
   PROCEDURE gm_fch_partyinfo (
      p_partyid     IN       t101_party.c101_party_id%TYPE,
      p_outcursor   OUT      TYPES.cursor_type
   )
   AS
   BEGIN
      OPEN p_outcursor
       FOR
          SELECT c101_first_nm firstname, c101_last_nm lastname,
                 c101_middle_initial middleinitial,
                 DECODE (c101_party_nm,
                         NULL, c101_last_nm
                          || ' '
                          || c101_first_nm
                          || ' '
                          || c101_middle_initial,
                         c101_party_nm
                        ) partyname,
                 DECODE (c101_active_fl, 'N', 'on', 'off') inactiveflag,
                 c101_party_id grppartyid,
                 c101_party_id partyId,
                 c101_party_nm_en partynameen
            FROM t101_party
           WHERE c101_party_id = p_partyid AND c101_void_fl IS NULL;
   END gm_fch_partyinfo;

   /******************************************************************
    * Description : Procedure to fetch the tabs to display on the party setup screen
    ****************************************************************/
   PROCEDURE gm_fch_setup_displaytabs (
      p_partytype   IN       t911_action_lookup.c901_party_type%TYPE,
      p_level       IN       t911_action_lookup.c901_action_level%TYPE,
      p_outcursor   OUT      TYPES.cursor_type
   )
   AS
   BEGIN
      OPEN p_outcursor
       FOR
          SELECT   c911_tab_label tablabel, c911_action_lookup action
              FROM t911_action_lookup
             WHERE c901_party_type = p_partytype
               AND c901_action_level = p_level
               AND c911_void_fl IS NULL
          ORDER BY c911_seq_no;
   END gm_fch_setup_displaytabs;

   /******************************************************************
    * Description : Function to fetch the information for party from t1012_party_other_info
    * table.
    ****************************************************************/
	FUNCTION get_party_other_info (
		p_party_id	 IN   NUMBER
	  , p_type		 IN   NUMBER
	)
		RETURN VARCHAR2
	IS
		otherinfo	   VARCHAR2 (5000);
	BEGIN
		WITH other_info AS (
			SELECT c1012_detail detail, c1012_party_other_info_id id
			  FROM t1012_party_other_info
			 WHERE c101_party_id = p_party_id AND c901_type = p_type
		)
		SELECT	   MAX (SYS_CONNECT_BY_PATH (detail, ';'))
			  INTO otherinfo
			  FROM (SELECT detail, ROW_NUMBER () OVER (ORDER BY ID) AS curr
					  FROM other_info)
		CONNECT BY curr - 1 = PRIOR curr
		START WITH curr = 1;

		RETURN SUBSTR (otherinfo, 2);
	END get_party_other_info;
	
   /******************************************************************
    * Description : Procedure to fetch party list based on party type
    ****************************************************************/
   PROCEDURE gm_fch_partylist (
      p_partytype   	IN       t101_party.c101_party_id%TYPE,
      p_mastergrpValue 	IN 	 	 VARCHAR2 DEFAULT NULL,
      p_outcursor   	OUT      TYPES.cursor_type
   )
   AS
   v_portal_comp_id t101_party.C1900_COMPANY_ID%TYPE;
   v_comp_lang_id T1900_COMPANY.C901_LANGUAGE_ID%TYPE;
   BEGIN
	   SELECT get_compid_frm_cntx (),get_complangid_frm_cntx() 
	    INTO v_portal_comp_id,v_comp_lang_id 
	    FROM DUAL;

      OPEN p_outcursor
       FOR
          SELECT   c101_party_id ID,
                   TRIM (DECODE (DECODE(v_comp_lang_id,'103097',NVL(C101_party_nm_en,C101_PARTY_NM),C101_PARTY_NM),
                                 NULL, c101_last_nm
                                  || DECODE (c101_first_nm,
                                             NULL, '',
                                             ' ' || c101_first_nm
                                            )
                                  || DECODE (c101_middle_initial,
                                             NULL, '',
                                             ' ' || c101_middle_initial
                                            ),
                                 DECODE(v_comp_lang_id,'103097',NVL(C101_party_nm_en,C101_PARTY_NM),C101_PARTY_NM)
                                )
                        ) NAME
              FROM t101_party
             WHERE c901_party_type = p_partytype
               AND c101_void_fl IS NULL
               AND C1900_COMPANY_ID = v_portal_comp_id
               --AND (c101_active_fl != 'N' or c101_active_fl is null)
               AND NVL(c101_active_fl,'Y') = DECODE(p_mastergrpValue,'Y',NVL(c101_active_fl,'Y') ,DECODE(p_mastergrpValue,NULL,'Y'))
          ORDER BY UPPER (NAME);
   END gm_fch_partylist;

   /******************************************************************
    * Description : Procedure to fetch the basic info details for the party
    * summary page
    ****************************************************************/
   PROCEDURE gm_fch_party_basicinfo (
      p_partyid             IN       t101_party.c101_party_id%TYPE,
      p_nameaddresscursor   OUT      TYPES.cursor_type,
      p_contactcursor       OUT      TYPES.cursor_type
   )
   AS
   BEGIN
      OPEN p_nameaddresscursor
       FOR
          SELECT get_party_name (p_partyid) partyname,
                 get_party_address (p_partyid) address
            FROM DUAL;

      OPEN p_contactcursor
       FOR
          SELECT   t901.c901_code_nm cmode, c107_contact_value cvalue
              FROM (SELECT *
                      FROM t107_contact
                     WHERE c101_party_id = p_partyid AND c107_primary_fl = 'Y') t107,
                   t901_code_lookup t901
             WHERE t901.c901_code_grp = 'COTMD' AND t901.c901_code_id = t107.c901_mode(+)
          ORDER BY t901.c901_code_seq_no;
   END gm_fch_party_basicinfo;

   /******************************************************************
    * Description : Function to fetch the primary address for party
    ****************************************************************/
   FUNCTION get_party_address (p_party_id IN t101_party.c101_party_id%TYPE)
      RETURN VARCHAR2
   IS
      address   VARCHAR2 (1000);
   BEGIN
      SELECT    c106_add1
             || DECODE (c106_add2, '', ',', ',<br/> ' || c106_add2 || ',')
             || '<br/> '
             || c106_city
             || ', '
             || get_code_name_alt (c901_state)
             || ', '
             || c106_zip_code
             || DECODE (c901_country,
                        1101, '',
                        '<br/> ' || get_code_name (c901_country)
                       )
        INTO address
        FROM t106_address t106
       WHERE t106.c101_party_id = p_party_id AND t106.c106_primary_fl = 'Y' and t106.c106_void_fl IS NULL;

      RETURN address;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN '';
   END get_party_address;

   /******************************************************************
    * Description : Procedure to fetch the about me details for the party
    * for summary page
    ****************************************************************/
   PROCEDURE gm_fch_party_aboutmeinfo (
      p_partyid   IN       t101_party.c101_party_id%TYPE,
      p_cursor    OUT      TYPES.cursor_type
   )
   AS
   BEGIN
      OPEN p_cursor
       FOR
          SELECT get_party_other_info (p_partyid,
                                       get_code_id ('Hobbies', 'TVALS')
                                      ) hobbies,
                 get_party_other_info (p_partyid,
                                       get_code_id ('Speciality', 'TVALS')
                                      ) speciality,
                 get_party_other_info
                    (p_partyid,
                     get_code_id ('Professional Interests', 'TVALS')
                    ) professionalinterests,
                 c1011_dob dateofbirth,
                 get_code_name (c901_gender) gender,
                 c1011_home_town hometown,
                 c1011_personal_website personalwebsite,
                 get_code_name (c901_relation_status) relationshipstatus,
                 c1011_spouse_details spousedetails,
                 get_code_name (c901_children) children,
                 c1011_children_details childrendetails
            FROM t1011_party_personal
           WHERE c101_party_id = p_partyid AND c1011_void_fl IS NULL;
   END gm_fch_party_aboutmeinfo;

   /*****************************************************************************************************
   * This Procedure is to Fetch the Party ID's for the given Search Lookup and Search String
   * Author : Vprasath
   ******************************************************************************************************/
   PROCEDURE gm_search_party (
      p_search_str    IN       VARCHAR2,
      p_search_type   IN       VARCHAR2
-- not used now, will contain the id and search type ( '=' or 'LIKE' or 'IN' etc.,)
                                       ,
      p_party_type    IN       t101_party.c901_party_type%TYPE,
      p_recordset     OUT      TYPES.cursor_type
   )
   AS
      v_table_name        t912_search.c912_table_nm%TYPE;
      v_column_name       t912_search.c912_table_nm%TYPE;
      v_search_string     VARCHAR2 (2000);
      v_sql               VARCHAR2 (20000);

      CURSOR v_cursor
      IS
         SELECT   c912_table_nm table_nm, c912_column_nm column_nm,
                  DECODE (c912_search_code_grp,
                          '', SUBSTR (tokenii, INSTR (tokenii, '^') + 1),
                          get_code_id (SUBSTR (tokenii,
                                               INSTR (tokenii, '^') + 1
                                              ),
                                       c912_search_code_grp
                                      )
                         ) search_val,
                  SUBSTR (tokenii, 1,
                          INSTR (tokenii, '^') - 1) search_operator
             FROM v_double_in_list vlist, t912_search t912
            WHERE t912.c912_search_id = vlist.token
              AND token IS NOT NULL
              AND tokenii IS NOT NULL
         ORDER BY table_nm;

      v_prev_row_tbl_nm   VARCHAR2 (100);
      v_closed_flag       CHAR (1)                         := 'N';
      v_search_value      VARCHAR2 (2000);
   BEGIN
--
      my_context.set_double_inlist_ctx (p_search_str, ';');
      v_sql :=
         ' select  c101_party_id PARTY_ID, c101_last_nm || '' '' || c101_first_nm PARTY_NM';
      v_sql :=
            v_sql
         || ', c901_party_type PARTY_TYPE, NVL(t903.C903_UPLOAD_FILE_LIST,-999) file_id ';
      v_sql := v_sql || ' from t101_party t101, t903_upload_file_list t903 ';
      v_sql := v_sql || ' where t101.C101_PARTY_ID = t903.C903_REF_ID(+) ';
      v_sql := v_sql || ' and t903.C901_REF_TYPE(+) = 91182 ';
      v_sql := v_sql || ' and t101.c901_party_type = ' || p_party_type;
      v_sql := v_sql || ' and t101.C101_VOID_FL IS NULL ';
      v_sql := v_sql || ' and t903.C903_DELETE_FL IS NULL ';

      --gm_procedure_log ('v_sql', v_sql);
      --COMMIT;
      FOR current_row IN v_cursor
      LOOP
         IF (current_row.search_operator = 'LIKE')
         THEN
            v_search_value := '%' || current_row.search_val || '%';
         ELSE
            v_search_value := current_row.search_val;
         END IF;

         IF    v_prev_row_tbl_nm IS NULL
            OR v_prev_row_tbl_nm <> current_row.table_nm
         THEN
            IF v_prev_row_tbl_nm IS NOT NULL
            THEN
               v_sql := v_sql || ')';
               v_closed_flag := 'Y';
            END IF;

            v_closed_flag := 'N';
            v_sql :=
                  v_sql
               || ' and	t101.C101_PARTY_ID IN ( SELECT c101_party_id from ';
            v_sql :=
                  v_sql
               || current_row.table_nm
               || ' where UPPER('
               || current_row.column_nm
               || ') '
               || current_row.search_operator
               || ' '
               || 'UPPER('''
               || v_search_value
               || ''')';
            v_prev_row_tbl_nm := current_row.table_nm;
         ELSE
            v_closed_flag := 'N';
            v_sql :=
                  v_sql
               || '
				AND UPPER ('
               || current_row.column_nm
               || ') '
               || current_row.search_operator
               || ' '
               || 'UPPER ('''
               || v_search_value
               || ''')';
            v_prev_row_tbl_nm := current_row.table_nm;
         END IF;
      END LOOP;

      IF v_closed_flag = 'N'
      THEN
         v_sql := v_sql || ')';
      END IF;

      --gm_procedure_log ('v_sql', v_sql);
      --COMMIT;
      OPEN p_recordset
       FOR v_sql;
   END gm_search_party;

   /*****************************************************************************************************
   * This Procedure is to Fetch the Party ID's for the given Search Lookup and Search String
   * Author : Vprasath
   ******************************************************************************************************/
   PROCEDURE gm_fch_search_lookup (
      p_code_grp   IN       t912_search.c912_search_code_grp%TYPE,
      p_out_cur    OUT      TYPES.cursor_type
   )
   AS
   BEGIN
      OPEN p_out_cur
       FOR
          SELECT c912_search_id codeid, c912_search_key codenm
            FROM t912_search
           WHERE c912_search_grp = p_code_grp
             AND c912_summary_search_fl = 'Y'
             AND c912_active_fl IS NOT NULL;
   END gm_fch_search_lookup;

   /******************************************************************
    * Description : Procedure to fetch the fields on party common search screen
    ****************************************************************/
   PROCEDURE gm_fch_search_filter (
      p_searchgroup   IN       t912_search.c912_search_grp%TYPE,
      p_outcursor     OUT      TYPES.cursor_type
   )
   AS
   BEGIN
      OPEN p_outcursor
       FOR
          SELECT c912_search_id lookupid, c912_search_key fieldname,
                 c912_search_key_label fieldlabel,
                 c901_search_field_type fieldtype,
                 c912_search_field_attr fieldattr
            FROM t912_search
           WHERE c912_search_grp = p_searchgroup AND c912_active_fl = '1';
   END gm_fch_search_filter;

   /******************************************************************
    * Description : Procedure to fetch the filter tabs on common search screen
    ****************************************************************/
   PROCEDURE gm_fch_search_filter_tabs (
      p_party_type   IN       t101_party.c901_party_type%TYPE,
      p_outcursor    OUT      TYPES.cursor_type
   )
   AS
   BEGIN
      OPEN p_outcursor
       FOR
          SELECT c911_search_grp tab, c911_tab_label tablabel
            FROM t911_action_lookup
           WHERE c901_party_type = p_party_type
             AND c911_search_grp IS NOT NULL
             AND c911_void_fl IS NULL;
   END gm_fch_search_filter_tabs;

   /******************************************************************
    * Description : Function to fetch the third party info (phone, fax)
    ****************************************************************/
   FUNCTION get_party_contact_info (
      p_party_id   IN   t101_party.c101_party_id%TYPE,
      p_mode       IN   t107_contact.c901_mode%TYPE
   )
      RETURN VARCHAR2
   IS
      contact_info   VARCHAR2 (100);
   BEGIN
      SELECT c107_contact_value
        INTO contact_info
        FROM t107_contact
       WHERE c101_party_id = p_party_id
         AND c901_mode = p_mode
         AND c107_primary_fl = 'Y'
         AND c107_void_fl IS NULL;

      RETURN contact_info;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN '';
   END get_party_contact_info;
END gm_pkg_cm_party_info;
/
