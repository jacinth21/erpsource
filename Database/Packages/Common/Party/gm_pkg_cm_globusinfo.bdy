/* Formatted on 2008/12/05 17:39 (Formatter Plus v4.8.0) */
-- @"C:\Database\Packages\common\Party\gm_pkg_cm_globusinfo.bdy"

CREATE OR REPLACE PACKAGE BODY gm_pkg_cm_globusinfo
IS
	/******************************************************************
	* Procedure to fetch the globus association information for party.
	*******************************************************************/
	PROCEDURE gm_fch_globusinfo (
		p_partyid		  IN	   t108_party_to_party_link.c101_from_party_id%TYPE
	  , p_globusinfocur   OUT	   TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_globusinfocur
		 FOR
			 SELECT   t108.c108_party_party_link_id ID, get_party_name (t108.c101_to_party_id) personnelname
					, get_code_name (t101.c901_party_type) personneltype1
					, DECODE (c108_primary_fl, 'Y', 'Yes', '') primaryflag
				 FROM t101_party t101, t108_party_to_party_link t108
				WHERE t108.c101_from_party_id = p_partyid
				  AND t101.c101_party_id = c101_to_party_id
				  AND t108.c108_void_fl IS NULL
			 ORDER BY primaryflag;
	END gm_fch_globusinfo;

	/*******************************************************************
	 * Procedure to fetch the globus association information for party.
	 * Called when user clicks on edit
	 *******************************************************************/
	PROCEDURE gm_fch_editglobusinfo (
		p_globusinfoid	  IN	   t108_party_to_party_link.c108_party_party_link_id%TYPE
	  , p_globusinfocur   OUT	   TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_globusinfocur
		 FOR
			 SELECT t108.c108_party_party_link_id ID, t108.c101_to_party_id personnelname
				  , t101.c901_party_type personneltype, DECODE (c108_primary_fl, 'Y', 'on', 'off') primaryflag
			   FROM t101_party t101, t108_party_to_party_link t108
			  WHERE t108.c108_party_party_link_id = p_globusinfoid
				AND t101.c101_party_id = c101_to_party_id
				AND t108.c108_void_fl IS NULL;
	END gm_fch_editglobusinfo;

	/*******************************************************************
	* Procedure to save the globus association information for the party
	*********************************************************************/
	PROCEDURE gm_sav_globusinfo (
		p_globusinfoid		IN		 t108_party_to_party_link.c108_party_party_link_id%TYPE
	  , p_partyid			IN		 t108_party_to_party_link.c101_from_party_id%TYPE
	  , p_to_partyid		IN		 t108_party_to_party_link.c101_from_party_id%TYPE
	  , p_primaryflag		IN		 t108_party_to_party_link.c108_primary_fl%TYPE
	  , p_userid			IN		 t108_party_to_party_link.c108_created_by%TYPE
	  , p_outglobusinfoid	OUT 	 t1010_education.c1010_education_id%TYPE
	  , p_voidfl		   IN		 t108_party_to_party_link.c108_void_fl%TYPE default null
	)
	AS
		v_globusinfoid t108_party_to_party_link.c108_party_party_link_id%TYPE;
		v_primary	   NUMBER;
		v_primaryfl    t108_party_to_party_link.c108_primary_fl%TYPE;
	BEGIN
		UPDATE t108_party_to_party_link
		   SET c101_from_party_id = p_partyid
			 , c101_to_party_id = p_to_partyid
			 , c108_primary_fl = p_primaryflag
			 , c108_last_updated_by = p_userid
			 , c108_last_updated_date = SYSDATE
			 , c108_void_fl = p_voidfl
		 WHERE c108_party_party_link_id = p_globusinfoid;

		IF (SQL%ROWCOUNT = 0)
		THEN
			SELECT s108_party_to_party_link.NEXTVAL
			  INTO v_globusinfoid
			  FROM DUAL;

			SELECT COUNT (*)
			  INTO v_primary
			  FROM t108_party_to_party_link
			 WHERE c101_from_party_id = p_partyid AND c108_primary_fl = 'Y' AND c108_void_fl IS NULL;

			IF (v_primary = 0)
			THEN
				v_primaryfl := 'Y';
			ELSE
				v_primaryfl := p_primaryflag;
			END IF;

			INSERT INTO t108_party_to_party_link
						(c108_party_party_link_id, c101_from_party_id, c101_to_party_id, c108_primary_fl
					   , c108_created_by, c108_created_date
						)
				 VALUES (v_globusinfoid, p_partyid, p_to_partyid, v_primaryfl
					   , p_userid, SYSDATE
						);
		ELSE
			v_globusinfoid := p_globusinfoid;
		END IF;

		IF (p_primaryflag = 'Y')
		THEN
			UPDATE t108_party_to_party_link
			   SET c108_primary_fl = ''
				 , c108_last_updated_by = p_userid
				 , c108_last_updated_date = SYSDATE
			 WHERE c101_from_party_id = p_partyid AND c108_party_party_link_id <> v_globusinfoid
				   AND c108_primary_fl = 'Y';
		END IF;
	END gm_sav_globusinfo;

	/******************************************************************
	* Procedure to fetch the globus association information for party summary page
	*******************************************************************/
	PROCEDURE gm_fch_globusinfosummary (
		p_partyid		  IN	   t108_party_to_party_link.c101_from_party_id%TYPE
	  , p_globusinfocur   OUT	   TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_globusinfocur
		 FOR
			 SELECT get_associated_party_names (p_partyid) reps, ad_name adname, region_name region
			   FROM t703_sales_rep t703
				  , (SELECT DISTINCT v700.rep_id, v700.ad_name, v700.region_name
								FROM v700_territory_mapping_detail v700) v700
				  , t108_party_to_party_link t108
			  WHERE t108.c101_from_party_id = p_partyid
				AND t108.c101_to_party_id = t703.c101_party_id
				AND t703.c703_sales_rep_id = v700.rep_id
				AND t108.c108_primary_fl = 'Y'
				AND t108.c108_void_fl IS NULL;
	END gm_fch_globusinfosummary;

	/******************************************************************
	 * Description : Function to fetch the associated rep list for party
	 ****************************************************************/
	FUNCTION get_associated_party_names (
		p_partyid	 IN   t108_party_to_party_link.c101_from_party_id%TYPE
	)
		RETURN VARCHAR2
	IS
		repnames	   VARCHAR2 (5000);
	BEGIN
		WITH reptable AS (
			SELECT get_party_name (c101_to_party_id) name, c108_party_party_link_id id
			  FROM t108_party_to_party_link
			 WHERE c101_from_party_id = p_partyid AND c108_void_fl IS NULL
		)
		SELECT MAX (SYS_CONNECT_BY_PATH (name, ','))
		  INTO repnames
		  FROM
			(SELECT name, ROW_NUMBER () OVER (ORDER BY id) AS curr
			   FROM reptable)
	 	 CONNECT BY curr - 1 = PRIOR curr
	     START WITH curr = 1;
		 RETURN SUBSTR (repnames, 2) ;
	END get_associated_party_names;	
	
	
	/******************************************************************
	 * Description : Function to fetch the sales rep  
	 ****************************************************************/
	
	PROCEDURE gm_fch_surgeon_repinfo(
		p_partyid	   IN		T101_PARTY.C101_PARTY_ID%TYPE, 	 
		p_repcursor   OUT		TYPES.cursor_type
	)
	AS
	BEGIN	 
		OPEN p_repcursor
		 FOR
			 SELECT t108.c108_party_party_link_id plinkid, t108.c101_from_party_id partyid, t108.c101_to_party_id repid, rep_name repname, ad_name adnm, ad_id adid, vp_name vpnm, vp_id vpid, t108.c108_primary_fl primaryfl   
			   FROM t703_sales_rep t703  
				  , (SELECT DISTINCT v700.rep_id, v700.rep_name,  v700.ad_name, v700.ad_id, v700.vp_name, v700.vp_id
								FROM v700_territory_mapping_detail v700) v700
				  , t108_party_to_party_link t108
			  WHERE t108.c101_to_party_id = t703.c101_party_id
				AND t703.c703_sales_rep_id = v700.rep_id
                AND t108.c101_from_party_id = p_partyid
				AND t108.c108_void_fl IS NULL
				ORDER BY primaryfl,repname;
                
	END gm_fch_surgeon_repinfo;
	
	
END gm_pkg_cm_globusinfo;
/
