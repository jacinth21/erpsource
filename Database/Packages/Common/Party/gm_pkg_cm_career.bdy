/* Formatted on 2008/11/21 17:33 (Formatter Plus v4.8.0) */
/*-- @"C:\Database\Packages\common\Party\gm_pkg_cm_career.bdy" */

CREATE OR REPLACE PACKAGE BODY gm_pkg_cm_career
IS
	/*******************************************************
	* Purpose: This Procedure is used to fetch the party career info
	*******************************************************/
	PROCEDURE gm_fch_careerinfo (
		p_party_id	  IN	   t1020_career.c101_party_id%TYPE
	  , p_careercur   OUT	   TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_careercur
		 FOR
			 SELECT c1020_career_id careerid, c1020_title title, c1020_company_nm companyname
				  , c1020_company_desc companydesc
				  , get_code_name (c901_start_month) || ' ' || c1020_start_year startdate, c901_start_month startmonth, c1020_start_year startyear
				  , DECODE(to_char(c901_start_month),NULL,to_char(c1020_start_year), GET_CODE_NAME_ALT(c901_start_month) || '/' || c1020_start_year) startday
				  , DECODE (c1020_still_in_this_position
						  , NULL, DECODE(to_char(c901_end_month),NULL,to_char(c1020_end_year), GET_CODE_NAME_ALT(c901_end_month) || '/' || c1020_end_year)
						  , 'Y', 'Present'
						   ) endday, c901_end_month endmonth, c1020_end_year endyear
				  , DECODE (c1020_still_in_this_position
						  , NULL, get_code_name (c901_end_month) || ' ' || c1020_end_year
						  , 'Y', 'Present'
						   ) enddate, c901_end_month endmonth, c1020_end_year endyear
				  , c1020_group_division_nm groupdivision, get_code_name (c901_country) country, c901_country countryid
				  , get_code_name (c901_state_nm) state, c901_state_nm stateid, c1020_city_nm city, get_code_name (c901_industry) industry
				  , c901_industry industryid, c1020_career_id careerid, c1020_still_in_this_position stillinposition
			   FROM t1020_career
			  WHERE c101_party_id = p_party_id AND c1020_void_fl IS NULL ORDER BY c1020_still_in_this_position, c1020_end_year DESC, c901_end_month DESC,
			  c1020_start_year DESC, c901_start_month DESC, companyname ;
	END gm_fch_careerinfo;

	/*******************************************************
	* Purpose: This Procedure is used to updating the party career info
	*******************************************************/
	PROCEDURE gm_fch_editcareerinfo (
		p_careerid	  IN	   t1020_career.c1020_career_id%TYPE
	  , p_careercur   OUT	   TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_careercur
		 FOR
			 SELECT c1020_career_id careerid, c101_party_id partyid, c1020_title title, c1020_company_nm companyname
				  , c1020_company_desc companydesc, c901_start_month startmonth, c1020_start_year startyear
				  , c901_end_month endmonth, c1020_end_year endyear
				  , DECODE (c1020_still_in_this_position, 'Y', 'on', NULL, 'off') stillinposition
				  , c1020_group_division_nm groupdivision, c901_country country, c901_state_nm state
				  , c1020_city_nm city, c901_industry industry
			   FROM t1020_career
			  WHERE c1020_career_id = p_careerid AND c1020_void_fl IS NULL;
	END gm_fch_editcareerinfo;

	/*******************************************************
	* Purpose: This Procedure is used to save the party career info
	*******************************************************/
	PROCEDURE gm_sav_careerinfo (
		p_party_id			IN	 t1020_career.c101_party_id%TYPE
	  , p_title 			IN	 t1020_career.c1020_title%TYPE
	  , p_institute 		IN	 t1020_career.c1020_company_nm%TYPE
	  , p_desc				IN	 t1020_career.c1020_company_desc%TYPE
	  , p_startmonth		IN	 t1020_career.c901_start_month%TYPE
	  , p_startyear 		IN	 t1020_career.c1020_start_year%TYPE
	  , p_endmonth			IN	 t1020_career.c901_end_month%TYPE
	  , p_endyear			IN	 t1020_career.c1020_end_year%TYPE
	  , p_stillinposition	IN	 t1020_career.c1020_still_in_this_position%TYPE
	  , p_group 			IN	 t1020_career.c1020_group_division_nm%TYPE
	  , p_country			IN	 t1020_career.c901_country%TYPE
	  , p_state 			IN	 t1020_career.c901_state_nm%TYPE
	  , p_city				IN	 t1020_career.c1020_city_nm%TYPE
	  , p_industry			IN	 t1020_career.c901_industry%TYPE
	  , p_careerid			IN	 t1020_career.c1020_career_id%TYPE
	  , p_userid			IN	 t1020_career.c1020_created_by%TYPE
	  , p_voidfl			IN	 t1020_career.c1020_void_fl%TYPE default null
	)
	AS
	BEGIN
		UPDATE t1020_career
		   SET c101_party_id = p_party_id
			 , c1020_title = p_title
			 , c1020_company_nm = p_institute
			 , c1020_company_desc = p_desc
			 , c901_start_month = DECODE (p_startmonth, 0, NULL, p_startmonth)
			 , c1020_start_year = p_startyear
			 , c901_end_month = DECODE (p_endmonth, 0, NULL, p_endmonth)
			 , c1020_end_year = p_endyear
			 , c1020_still_in_this_position = p_stillinposition
			 , c1020_group_division_nm = p_group
			 , c901_country = p_country
			 , c901_state_nm = p_state
			 , c1020_city_nm = p_city
			 , c901_industry = DECODE (p_industry, 0, NULL, p_industry)
			 , c1020_last_updated_by = p_userid
			 , c1020_last_updated_date = SYSDATE
			 , c1020_void_fl = p_voidfl
		 WHERE c1020_career_id = p_careerid;

		IF (SQL%ROWCOUNT = 0)
		THEN
			INSERT INTO t1020_career
						(c101_party_id, c1020_title, c1020_company_nm, c1020_company_desc, c901_start_month
					   , c1020_start_year, c901_end_month, c1020_end_year, c1020_still_in_this_position
					   , c1020_group_division_nm, c901_country, c901_state_nm, c1020_city_nm, c901_industry
					   , c1020_career_id, c1020_last_updated_by, c1020_last_updated_date
						)
				 VALUES (p_party_id, p_title, p_institute, p_desc, DECODE (p_startmonth, 0, NULL, p_startmonth)
					   , p_startyear, DECODE (p_endmonth, 0, NULL, p_endmonth), p_endyear, p_stillinposition
					   , p_group, p_country, p_state, p_city, DECODE (p_industry, 0, NULL, p_industry)
					   , s1020_career.NEXTVAL, p_userid, SYSDATE
						);
		END IF;
	END gm_sav_careerinfo;
END gm_pkg_cm_career;
/
