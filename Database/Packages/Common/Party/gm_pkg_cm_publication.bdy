/* Formatted on 2008/11/18 17:47 (Formatter Plus v4.8.0) */
-- @"c:\database\packages\common\party\gm_pkg_cm_publication.bdy"
CREATE OR REPLACE PACKAGE BODY gm_pkg_cm_publication
IS
--
 /******************************************************************
   * Description : Procedure to fetch all articles to specific party ID
   ****************************************************************/
	PROCEDURE gm_fch_publicationinfo (
		p_partyid		IN		 t1040_publication.c101_party_id%TYPE
	  , p_outcursor		OUT 	 TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_outcursor
		 FOR
			 SELECT t1040.c1040_publication_id pubid, t1040.c1040_publication_title title, c1040_reference reference
			 	  , t1040.c1040_publication_abstract pabstract, get_code_name (t1040.c901_publication_type) pubtype
			 	  , t1040.c1040_publication_link pubref, get_author_names(t1040.c1040_publication_id) pauthor
				  , get_code_name (t1040.c1040_publication_month) || ' ' || t1040.c1040_publication_year pubdate
			   FROM t1040_publication t1040
			  WHERE t1040.c101_party_id = p_partyid
				AND t1040.c1040_void_fl IS NULL;
	END gm_fch_publicationinfo;

--
/******************************************************************
   * Description : Procedure to fetch one article detail
   ****************************************************************/
	PROCEDURE gm_fch_editpublicationinfo (
		p_publicationid	IN		t1040_publication.c1040_publication_id%TYPE
	  , p_outcursor		OUT		 TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_outcursor
		 FOR
			 SELECT t1040.c101_party_id partyid, t1040.c1040_publication_title title, c1040_reference reference
			 	  , get_author_names(t1040.c1040_publication_id) author
				  , t1040.c1040_publication_abstract pabstract, t1040.c901_publication_type typeid
				  , t1040.c1040_publication_link refurl, t1040.c1040_publication_month monthid
				  , t1040.c1040_publication_year pyear
			   FROM t1040_publication t1040
			  WHERE t1040.c1040_publication_id = p_publicationid
				AND t1040.c1040_void_fl IS NULL;
	END gm_fch_editpublicationinfo;

--
	/*********************************************************
	* Description : This procedure is used to Save article
	*
	*********************************************************/
	PROCEDURE gm_sav_publicationinfo (
		p_publicationid	 IN OUT	 t1040_publication.c1040_publication_id%TYPE
	  , p_partyid		 IN		 t1040_publication.c101_party_id%TYPE
	  , p_type			 IN	     t1040_publication.c901_publication_type%TYPE
	  , p_title 		 IN	     t1040_publication.c1040_publication_title%TYPE
	  , p_reference		 IN	     t1040_publication.c1040_reference%TYPE	  
	  , p_abst			 IN	     t1040_publication.c1040_publication_abstract%TYPE
	  , p_month 		 IN	     t1040_publication.c1040_publication_month%TYPE
	  , p_year			 IN	     t1040_publication.c1040_publication_year%TYPE
	  , p_author		 IN	     VARCHAR2
	  , p_user_id		 IN		 t1040_publication.c1040_created_by%TYPE
	  , p_refurl		 IN	     t1040_publication.c1040_publication_link%TYPE
	)
	AS
		v_strlen	   NUMBER := NVL (LENGTH (p_author), 0);
		v_string	   VARCHAR2 (3000) := p_author;
		v_substring	 VARCHAR2 (1000);
		v_inventor		t1041_publication_inventor.c1041_inventor_nm%TYPE;
		v_inventor_id  t1041_publication_inventor.c1041_publication_inventor_id%TYPE;
		v_inventortype t1041_publication_inventor.c1041_inventor_type_fl%TYPE;
		v_inventorseq  NUMBER := 0;
	--
	BEGIN
		UPDATE t1040_publication
			 SET c901_publication_type = p_type
			 , c1040_publication_title = p_title
			 , c1040_reference = p_reference
			 , c1040_publication_abstract = p_abst
			 , c1040_publication_month = DECODE (p_month, 0, NULL, p_month)
			 , c1040_publication_year = p_year
			 , c1040_publication_link = p_refurl
			 , c1040_last_updated_by = p_user_id
			 , c1040_last_updated_date = SYSDATE
		 WHERE c1040_publication_id = p_publicationid;

		IF (SQL%ROWCOUNT = 0)
		THEN
			SELECT s1040_publication.NEXTVAL
			  INTO p_publicationid
			  FROM DUAL;

			INSERT INTO t1040_publication
						(c1040_publication_id, c101_party_id, c1040_publication_title, c1040_reference
						, c1040_publication_abstract, c1040_publication_month, c1040_publication_year
						, c1040_publication_link, c901_publication_type, c1040_created_by, c1040_created_date
						)
				 VALUES (p_publicationid, p_partyid, p_title, p_reference, p_abst
						, DECODE (p_month, 0, NULL, p_month), p_year, p_refurl
						, p_type, p_user_id, SYSDATE
						);
		END IF;

		DELETE FROM t1041_publication_inventor
			  WHERE c1040_publication_id = p_publicationid;

		IF (v_strlen > 0)
		THEN
			IF (INSTR (v_string, ';', -1, 1) <> v_strlen)	-- If last char is not ';', then append it
			THEN
				v_string	:= CONCAT (v_string, ';');
			END IF;

			
			WHILE INSTR (v_string, ';') <> 0
			LOOP
				--
				v_substring := SUBSTR (v_string, 1, INSTR (v_string, ';') - 1);
				v_string	:= NVL(SUBSTR (v_string, INSTR (v_string, ';') + 1), '999');
				--
				v_inventor	:= NULL;
				v_inventor	:= TRIM (v_substring);

				v_inventorseq := v_inventorseq + 1;
				
				SELECT s1041_publication_inventor.NEXTVAL
				  INTO v_inventor_id
				  FROM DUAL;

				IF (v_inventorseq = 1)
				THEN
					v_inventortype := 'P';
				ELSIF (v_string = '999')
				THEN
					v_inventortype := 'G';
				ELSE
					v_inventortype := 'O';
				END IF;

				INSERT INTO t1041_publication_inventor
							(c1041_publication_inventor_id, c1040_publication_id, c1041_inventor_nm
							 , c101_party_id, c1041_inventor_seq, c1041_inventor_type_fl
							)
					 VALUES (v_inventor_id, p_publicationid, v_inventor
							 , p_partyid, v_inventorseq, v_inventortype
							);
			END LOOP;
		END IF;
	END gm_sav_publicationinfo;
	
	/*********************************************************
	* Function to fetch the author names for a particular publication
	*********************************************************/
	FUNCTION get_author_names (
		p_publicationid	IN	 t1041_publication_inventor.c1040_publication_id%TYPE
	)
		RETURN VARCHAR2
	IS
		authornames		 VARCHAR2 (32760);
	BEGIN
	WITH inventor as ( 
		SELECT c1041_inventor_nm author, c1041_inventor_seq seq
			FROM t1041_publication_inventor
		 WHERE c1040_publication_id = p_publicationid
	)

	SELECT MAX(SYS_CONNECT_BY_PATH(AUTHOR, ';'))
	  INTO authornames
	  FROM (
	  	SELECT AUTHOR, ROW_NUMBER() OVER( ORDER BY seq) AS curr
			FROM inventor
	)
	CONNECT BY curr - 1 = PRIOR curr
	START WITH curr = 1;
	authornames := SUBSTR(authornames, 2);
		RETURN authornames;
	END get_author_names;
END gm_pkg_cm_publication;
/
