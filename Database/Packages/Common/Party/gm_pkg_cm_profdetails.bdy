/* Formatted on 2008/11/20 15:06 (Formatter Plus v4.8.0) */
--@"C:\database\Packages\common\party\gm_pkg_cm_profdetails.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_cm_profdetails
IS
--
/*******************************************************
 * Purpose: This Procedure is used to fetch
 * the personal info

 *******************************************************/
--
	PROCEDURE gm_fch_profdetails (
		p_party_id		   IN		VARCHAR2
	  , p_profdetailscur   OUT		TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_profdetailscur
		 FOR
			 SELECT t901.c901_code_id codeid, t901.c901_code_nm codenm, t901.c902_code_nm_alt codenmalt
				  , gm_pkg_cm_party_info.get_party_other_info (p_party_id, t901.c901_code_id) VALUE
			   FROM t901_code_lookup t901
			  WHERE t901.c901_code_grp = 'TVALS' AND t901.c901_code_nm <> 'Hobbies';
	END gm_fch_profdetails;
END gm_pkg_cm_profdetails;
/
