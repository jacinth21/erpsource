/* Formatted on 2010/09/02 17:54 (Formatter Plus v4.8.0) */
/*-- @"C:\database\Packages\common\party\gm_pkg_cm_party_trans.bdy";*/

CREATE OR REPLACE PACKAGE BODY gm_pkg_cm_party_trans
IS
   /******************************************************************
     * Description : Procedure to save / update party details
     ****************************************************************/
   PROCEDURE gm_sav_partyinfo (
      p_partyid         IN OUT   t101_party.c101_party_id%TYPE,
      p_firstname       IN       t101_party.c101_first_nm%TYPE,
      p_lastname        IN       t101_party.c101_last_nm%TYPE,
      p_middleinitial   IN       t101_party.c101_middle_initial%TYPE,
      p_name            IN       t101_party.c101_party_nm%TYPE,
      p_type            IN       t101_party.c901_party_type%TYPE,
      p_inactivefl      IN       t101_party.c101_active_fl%TYPE,
      p_userid          IN       t101_party.c101_created_by%TYPE,
      p_party_nm_en     IN       t101_party.C101_PARTY_NM_EN%TYPE
      
   )
   AS
   v_accid_count 	NUMBER;
   v_count 			NUMBER;
   v_address_id		t106_address.c106_address_id%TYPE;
   v_txn_cnt		NUMBER := 0;
   v_company_id    t1900_company.c1900_company_id%TYPE;
   	CURSOR p_partyinfocur
	IS
		SELECT t106.c106_address_id v_address_id
			FROM t106_address t106 , t101_party t101
	    WHERE t106.c101_party_id = t101.c101_party_id 
			AND t101.c101_party_id = p_partyid
			AND t106.c106_void_fl IS NULL
	    	AND t101.c101_void_fl IS NULL;
   
   BEGIN
	   
	SELECT NVL (GET_COMPID_FRM_CNTX (), GET_RULE_VALUE ('DEFAULT_COMPANY', 'ONEPORTAL'))
	  INTO v_company_id 
   		FROM dual;
   		
      UPDATE t101_party
         SET c101_first_nm = p_firstname,
             c101_last_nm = p_lastname,
             c101_middle_initial = p_middleinitial,
             c101_party_nm = p_name,
             c101_party_nm_en = p_party_nm_en,
             c901_party_type = p_type,
             c101_active_fl = p_inactivefl,
             c101_last_updated_by = p_userid,
             c101_last_updated_date = CURRENT_DATE
       WHERE c101_party_id = p_partyid;

       
       IF  p_inactivefl = 'N' AND p_partyid IS NOT NULL -- Here Check the Condition, if InActive CheckBox is checcked Only.
 		THEN
 		
 			IF p_type = '4000641' --Shipping Account
 			THEN
 				
 				FOR addid IN p_partyinfocur
 				LOOP
 					
	 				v_txn_cnt:= gm_pkg_cm_address.get_txncount_for_addr(addid.v_address_id);
	 				
 					IF v_txn_cnt > 0 THEN -- if there is any address with transaction, exit the loop and throw apperror
 						EXIT;
 					END IF;
 				END LOOP;
 				
 				IF v_txn_cnt > 0 THEN
 					raise_application_error ('-20651', '');
			 	END IF;
 			
 			ELSE
		 		SELECT count(1) 
		 			INTO v_accid_count 
		 			FROM t704a_account_attribute 
		 			WHERE C704A_ATTRIBUTE_VALUE = TO_CHAR(p_partyid) 
		 			AND C901_ATTRIBUTE_TYPE='7008';
			 	IF v_accid_count >  0 THEN
			 		raise_application_error ('-20614', '');
			 	END IF;
		 	END IF;
	 	END IF;
       
      IF (SQL%ROWCOUNT = 0)
      THEN
         SELECT s101_party.NEXTVAL
           INTO p_partyid
           FROM DUAL;
           
			
           SELECT count(1) INTO v_count FROM t101_party
			 		WHERE c101_party_nm = p_name AND c901_party_type=p_type AND c101_void_fl is not null;
		 	IF v_count >  0  THEN
				 UPDATE t101_party 
				 SET c101_party_id = p_partyid,
				 	c101_void_fl='',
				 	c101_active_fl= p_inactivefl,
				 	c101_last_updated_by = p_userid,
				 	c101_last_updated_date = CURRENT_DATE
				 WHERE c101_party_nm = p_name;   
				     	
			ELSE
						
		       	INSERT INTO t101_party
	                     (c101_party_id, c101_last_nm, c101_first_nm,
	                      c101_middle_initial, c901_party_type, c101_party_nm,
	                      c101_active_fl, c101_created_by, c101_created_date , c1900_company_id,c101_party_nm_en
	                     )
	              VALUES (p_partyid, p_lastname, p_firstname,
	                      p_middleinitial, p_type, p_name,
	                      p_inactivefl, p_userid, CURRENT_DATE , v_company_id,p_party_nm_en
	                     );
	        END IF;
      END IF;
   END gm_sav_partyinfo;

   /******************************************************************
     * Description : Procedure to save / update party other info in
     * t1012_party_other_info table
     ****************************************************************/
   PROCEDURE gm_sav_party_otherinfo (
      p_partyid   IN   t1012_party_other_info.c101_party_id%TYPE,
      p_type      IN   t1012_party_other_info.c901_type%TYPE,
      p_values    IN   VARCHAR2,
      p_userid    IN   t1012_party_other_info.c1012_created_by%TYPE
   )
   AS
      v_string      VARCHAR2 (3000) := p_values;
      v_substring   VARCHAR2 (1000);
   BEGIN
      DELETE FROM t1012_party_other_info
            WHERE c101_party_id = p_partyid AND c901_type = p_type;

      IF (INSTR (v_string, ';', -1, 1) <> LENGTH (v_string)
         )                          -- If last char is not ';', then append it
      THEN
         v_string := CONCAT (v_string, ';');
      END IF;

      WHILE INSTR (v_string, ';') <> 0
      LOOP
         v_substring := SUBSTR (v_string, 1, INSTR (v_string, ';') - 1);
         v_string := SUBSTR (v_string, INSTR (v_string, ';') + 1);

         INSERT INTO t1012_party_other_info
                     (c1012_party_other_info_id, c101_party_id, c901_type,
                      c1012_detail, c1012_created_by, c1012_created_date
                     )
              VALUES (s1012_party_other_info.NEXTVAL, p_partyid, p_type,
                      v_substring, p_userid, CURRENT_DATE
                     );
      END LOOP;
   END gm_sav_party_otherinfo;

   PROCEDURE gm_sav_user_sc_info (
      p_partyid         IN OUT   t1012_party_other_info.c101_party_id%TYPE,
      p_firstname       IN       VARCHAR2,
      p_middleinitial   IN       VARCHAR2,
      p_lastname        IN       VARCHAR2,
      p_userid          IN       t101_party.c101_created_by%TYPE,
      p_inactive_fl     IN       t101_party.c101_active_fl%TYPE
   )
   AS
      v_created_user_id   NUMBER;
      v_login_exsits      NUMBER;
      v_default_pwd       VARCHAR2 (50);
      v_email_id          VARCHAR2 (50);
      v_mail_subject      VARCHAR2 (1000);
      v_mail_body         VARCHAR2 (2000);
      v_usr_name          VARCHAR2 (50);
      crlf                VARCHAR2 (2)                 := CHR (13)
                                                          || CHR (10);
      v_partyid           t1012_party_other_info.c101_party_id%TYPE
                                                                 := p_partyid;
   BEGIN
      BEGIN
         SELECT t101.c101_user_id
           INTO v_created_user_id
           FROM t101_user t101
          WHERE t101.c101_party_id = p_partyid;

         v_login_exsits := 1;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            BEGIN
               SELECT s101_user_site.NEXTVAL
                 INTO v_created_user_id
                 FROM DUAL;

               v_login_exsits := NULL;
            END;
      END;

      SELECT get_rule_value ('USERLOGIN', 'DEFAULTPWD'),
             get_rule_value ('QUERYSYS', 'EMAIL'),
             get_rule_value ('QUERYSYS', 'EMAILSUBJECT'),
             get_login_user_name (p_firstname, p_lastname)
        INTO v_default_pwd,
             v_email_id,
             v_mail_subject,
             v_usr_name
        FROM DUAL;

      gm_pkg_it_login.gm_save_user (v_created_user_id,
                                    p_firstname,
                                    p_firstname,
                                    p_lastname,
                                    v_email_id,
                                    2007,
                                    1,
                                    p_userid,
                                    311,
                                    305,
                                    p_partyid,
                                    7007,
                                    '',
                                    p_middleinitial,
                                    v_partyid,
                                    v_created_user_id
                                   );
      p_partyid := v_partyid;

      IF v_login_exsits IS NULL
      THEN
         gm_pkg_it_login.gm_save_user_login (v_created_user_id,
                                             v_usr_name,
                                             v_default_pwd,
                                             321,
                                             get_rule_value ('DB_AUTH', 'LDAP')
                                            );

         UPDATE t102_user_login t102
            SET t102.c102_password_expired_fl = 'Y',
                t102.c102_last_updated_by = p_userid,
                t102.c102_last_updated_date = CURRENT_DATE
          WHERE t102.c101_user_id = v_created_user_id;

         v_mail_subject :=
                     v_mail_subject || ' ' || p_firstname || ' ' || p_lastname;
         v_mail_body :=
               p_firstname
            || ' '
            || p_lastname
            || ', a new Site Coordinator, has been added to the Portal with the following login credentials: '
            || crlf
            || crlf
            || crlf
            || 'Username: '
            || LOWER (v_usr_name)
            || crlf
            || 'Password: '
            || v_default_pwd
            || crlf
            || crlf
            || crlf
            || 'This is a temporary password and the Site Coordinator '
            || crlf
            || 'will be prompted to change this on the first login.'
            || crlf
            || 'DO NOT share this password with anyone or use '
            || crlf
            || 'anybody else''s login to access the Intranet.';
         gm_com_send_email_prc (v_email_id, v_mail_subject, v_mail_body);
      END IF;

      IF p_inactive_fl = 'Y'
      THEN
         UPDATE t616_site_party_mapping t616
            SET c616_primary_fl = ''
                                    -- When the user is made in-active, void flag is set for SC.
         ,
                t616.c616_void_fl = 'Y',
                c616_last_updated_by = p_userid,
                c616_last_updated_date = CURRENT_DATE
          WHERE t616.c101_party_id = v_partyid AND t616.c616_void_fl IS NULL;

         UPDATE t101_user t101
            SET t101.c101_email_id = get_rule_value ('QUERYSYS', 'EMAIL')
                ,t101.c101_last_updated_by = p_userid
                ,t101.c101_last_updated_date = CURRENT_DATE
          WHERE t101.c101_party_id = v_partyid;

         UPDATE t102_user_login t102
            SET t102.c102_user_lock_fl = '1',
                c102_last_updated_by = p_userid,
                c102_last_updated_date = CURRENT_DATE
          WHERE t102.c101_user_id = v_created_user_id;

         UPDATE t101_user t101
            SET t101.c901_user_status = '',
                t101.c101_last_updated_by = p_userid,
                t101.c101_last_updated_date = CURRENT_DATE
          WHERE t101.c101_user_id = v_created_user_id;

         UPDATE t101_party t101
            SET t101.c101_active_fl = p_inactive_fl,
                t101.c101_last_updated_by = p_userid,
                t101.c101_last_updated_date = CURRENT_DATE
          WHERE t101.c101_party_id = v_partyid;
      END IF;
   END gm_sav_user_sc_info;
END gm_pkg_cm_party_trans;
/
