/* Formatted on 2008/11/21 18:39 (Formatter Plus v4.8.0) */
--@"C:\database\Packages\common\Party\gm_pkg_cm_personalinfo.bdy"

CREATE OR REPLACE PACKAGE BODY gm_pkg_cm_personalinfo
IS
--
	/*******************************************************
	* Purpose: This Procedure is used to fetch the personal info
	*******************************************************/
	PROCEDURE gm_fch_personalinfo (
		p_party_id		IN		 t1011_party_personal.c101_party_id%TYPE
	  , p_personalcur	OUT 	 TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_personalcur
		 FOR
			 SELECT TO_CHAR (c1011_dob, 'mm/dd/yyyy') dateofbirth, c901_gender gender, c1011_home_town hometown
				  , c1011_personal_website personalwebsite, c901_relation_status relationshipstatus
				  , c1011_spouse_details spousedetails, c901_children children, c1011_children_details childrendetails
				  , gm_pkg_cm_party_info.get_party_other_info (p_party_id, get_code_id ('Hobbies', 'TVALS')) hobbies
			   FROM t1011_party_personal
			  WHERE c101_party_id = p_party_id;
	END gm_fch_personalinfo;

	/*******************************************************
	* Purpose: This Procedure is used to save the personal info
	*******************************************************/
	PROCEDURE gm_sav_personalinfo (
		p_party_id			   IN	t1011_party_personal.c101_party_id%TYPE
	  , p_gender			   IN	t1011_party_personal.c901_gender%TYPE
	  , p_dateofbirth		   IN	VARCHAR2
	  , p_hometown			   IN	t1011_party_personal.c1011_home_town%TYPE
	  , p_personalwebsite	   IN	t1011_party_personal.c1011_personal_website%TYPE
	  , p_relationshipstatus   IN	t1011_party_personal.c901_relation_status%TYPE
	  , p_spousedetails 	   IN	t1011_party_personal.c1011_spouse_details%TYPE
	  , p_children			   IN	t1011_party_personal.c901_children%TYPE
	  , p_childrendetails	   IN	t1011_party_personal.c1011_children_details%TYPE
	  , p_hobbies			   IN	t1012_party_other_info.c1012_detail%TYPE
	  , p_user				   IN	t1011_party_personal.c1011_created_by%TYPE
	)
	AS
		v_children	   t1011_party_personal.c901_children%TYPE;
		v_codeid	   t901_code_lookup.c901_code_id%TYPE;
	BEGIN
		IF p_children = '0'
		THEN
			v_children	:= NULL;
		ELSE
			v_children	:= p_children;
		END IF;

		UPDATE t1011_party_personal
		   SET c1011_dob = TO_DATE (p_dateofbirth, 'mm/dd/yyyy')
			 , c901_gender = DECODE (p_gender, 0, NULL, p_gender)
			 , c1011_home_town = p_hometown
			 , c1011_personal_website = p_personalwebsite
			 , c901_relation_status = DECODE (p_relationshipstatus, 0, NULL, p_relationshipstatus)
			 , c1011_spouse_details = p_spousedetails
			 , c901_children = DECODE (v_children, 0, NULL, v_children)
			 , c1011_children_details = p_childrendetails
			 , c1011_last_updated_by = p_user
			 , c1011_last_updated_date = SYSDATE
		 WHERE c101_party_id = p_party_id;

		IF (SQL%ROWCOUNT = 0)
		THEN
			INSERT INTO t1011_party_personal
						(c101_party_id, c1011_dob, c901_gender
					   , c1011_home_town, c1011_personal_website, c901_relation_status
					   , c1011_spouse_details, c901_children, c1011_children_details, c1011_created_by
					   , c1011_created_date
						)
				 VALUES (p_party_id, TO_DATE (p_dateofbirth, 'mm/dd/yyyy'), DECODE (p_gender, 0, NULL, p_gender)
					   , p_hometown, p_personalwebsite, DECODE (p_relationshipstatus, 0, NULL, p_relationshipstatus)
					   , p_spousedetails, DECODE (v_children, 0, NULL, v_children), p_childrendetails, p_user
					   , SYSDATE
						);
		END IF;

		-- Saving hobbies in other table
		SELECT c901_code_id
		  INTO v_codeid
		  FROM t901_code_lookup
		 WHERE c901_code_grp = 'TVALS' AND c901_code_nm = 'Hobbies';

		gm_pkg_cm_party_trans.gm_sav_party_otherinfo (p_party_id, v_codeid, p_hobbies, p_user);
	END gm_sav_personalinfo;
END gm_pkg_cm_personalinfo;
/
