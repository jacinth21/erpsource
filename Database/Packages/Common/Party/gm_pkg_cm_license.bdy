/* Formatted on 2008/11/19 17:01 (Formatter Plus v4.8.0) */
--@"C:\database\Packages\common\party\gm_pkg_cm_license.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_cm_license
IS
--
 /******************************************************************
   * Description : Procedure to fetch all licence with this party id  in the database
   ****************************************************************/
	PROCEDURE gm_fch_licenseinfo (
		p_partyid	IN		 t1030_license.c101_party_id%TYPE
	  , p_outdata	OUT 	 TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_outdata
		 FOR
			 SELECT c1030_license_id licenseid, c1030_license_title title, c1030_detail description
				  , c101_party_id partyid, get_code_name (c901_country) countryname
				  , get_code_name (c901_state) statename, c901_country country, c901_state state
				  , get_code_name (c1030_issue_month) || ' ' || c1030_issue_year dateissue
			   FROM t1030_license
			  WHERE c101_party_id = p_partyid AND c1030_void_fl IS NULL;
	END gm_fch_licenseinfo;

	 /******************************************************************
	* Description : Procedure to fetch one licence in the database
	****************************************************************/
	PROCEDURE gm_fch_edit_licenseinfo (
		p_licenseid   IN	   t1030_license.c1030_license_id%TYPE
	  , p_outdata	  OUT	   TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_outdata
		 FOR
			 SELECT c1030_license_id licenseid, c1030_license_title title, c1030_issue_month issuemonth
				  , c1030_issue_year issueyear, c1030_detail description, c101_party_id partyid
				  , get_code_name (c901_country) countryname, c901_country country, c901_state state
				  , get_code_name (c901_state) statename
				  , get_code_name (c1030_issue_month) || ', ' || c1030_issue_year dateissue
			   FROM t1030_license
			  WHERE c1030_license_id = p_licenseid AND c1030_void_fl IS NULL;
	END gm_fch_edit_licenseinfo;

	/******************************************************************
	* Description : Procedure to save licence
	****************************************************************/
	PROCEDURE gm_sav_license_info (
		p_licenseid 	 IN OUT   t1030_license.c1030_license_id%TYPE
	  , p_licensetitle	 IN 	  t1030_license.c1030_license_title%TYPE
	  , p_issuemonth	 IN 	  t1030_license.c1030_issue_month%TYPE
	  , p_issueyear 	 IN 	  t1030_license.c1030_issue_year%TYPE
	  , p_detail		 IN 	  t1030_license.c1030_detail%TYPE
	  , p_partyid		 IN 	  t1030_license.c101_party_id%TYPE
	  , p_user			 IN 	  t1030_license.c1030_created_by%TYPE
	  , p_country		 IN 	  t1030_license.c901_country%TYPE
	  , p_state 		 IN 	  t1030_license.c901_state%TYPE
	)
	AS
	BEGIN
		UPDATE t1030_license
		   SET c1030_license_title = p_licensetitle
			 , c1030_issue_month = p_issuemonth
			 , c1030_issue_year = p_issueyear
			 , c1030_detail = p_detail
			 , c101_party_id = p_partyid
			 , c1030_last_updated_by = p_user
			 , c1030_last_updated_date = SYSDATE
			 , c901_country = DECODE (p_country, 0, NULL, p_country)
			 , c901_state = DECODE (p_state, 0, NULL, p_state)
		 WHERE c1030_license_id = p_licenseid AND c1030_void_fl IS NULL;

		IF (SQL%ROWCOUNT = 0)
		THEN
			SELECT s1030_license.NEXTVAL
			  INTO p_licenseid
			  FROM DUAL;

			INSERT INTO t1030_license
						(c1030_license_id, c1030_license_title, c1030_issue_month, c1030_issue_year, c1030_detail
					   , c101_party_id, c1030_created_by, c1030_created_date, c901_country, c901_state
						)
				 VALUES (p_licenseid, p_licensetitle, p_issuemonth, p_issueyear, p_detail
					   , p_partyid, p_user, SYSDATE, DECODE (p_country, 0, NULL, p_country)
					   , DECODE (p_state, 0, NULL, p_state)
						);
		END IF;
	END gm_sav_license_info;
END gm_pkg_cm_license;
/
