/* Formatted on 2008/11/18 15:52 (Formatter Plus v4.8.0) */
 /******************************************************************
   * Description : Package for SIM - profile management void functionalities
   ****************************************************************/
-- @"c:\database\packages\common\party\gm_pkg_cm_party_cancel.bdy";

 /*******************************************************
		* Description : Procedure to Void Address
		*******************************************************/

CREATE OR REPLACE PACKAGE BODY gm_pkg_cm_party_cancel
IS
	/******************************************************************
	* Description : Procedure to void the party
	****************************************************************/
	PROCEDURE gm_void_party (
		p_partyid	IN	 t101_party.c101_party_id%TYPE
	  , p_userid	IN	 t101_party.c101_last_updated_by%TYPE
	)
	AS
	v_count NUMBER;
	v_party_type 	t101_party.c901_party_type%TYPE;
	v_address_id	t106_address.c106_address_id%TYPE;
	v_txn_cnt		NUMBER := 0;
	v_rule_value    VARCHAR2(10);
	
	CURSOR p_partyinfocur
	IS
		SELECT t106.c106_address_id v_address_id
			FROM t106_address t106 , t101_party t101
	    WHERE t106.c101_party_id = t101.c101_party_id 
			AND t101.c101_party_id = p_partyid
			AND t106.c106_void_fl IS NULL
	    	AND t101.c101_void_fl IS NULL;
	
	BEGIN
		
		  -- MNTTASK-6074 - While do the void should not void if already mapped in particular account. Only for Group Pricing Aff,Healthcare Aff. 
		  -- So we have to get party type and check condtion then throw message.
		BEGIN   
			SELECT c901_party_type INTO v_party_type FROM t101_party 
				WHERE c901_party_type IS NOT  NULL
				AND c101_party_id =  p_partyid; 
		
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				v_party_type:=0;
		END;
		
		IF v_party_type = '4000641' -- Shipping Account
        THEN
        
        	FOR addid IN p_partyinfocur
 			LOOP			
	 			v_txn_cnt:= gm_pkg_cm_address.get_txncount_for_addr(addid.v_address_id); -- if transactions are there, should not void the account
	 				
 				IF v_txn_cnt > 0 THEN
 					EXIT;
 				END IF;
 			END LOOP;
 				
 			IF v_txn_cnt > 0 THEN
 				raise_application_error ('-20652', '');
		 	END IF;
		END IF;
		
		IF v_party_type = '26230725' THEN --Dealer party type = 26230725
              SELECT COUNT(1) INTO v_txn_cnt
                FROM t704_account
                WHERE c101_dealer_id=p_partyid
                  AND C704_ACTIVE_FL ='Y'
                  AND c704_void_fl   IS NULL;
                  IF v_txn_cnt >0 THEN 
                      raise_application_error ('-20947', '');
                  END IF;
 	    END IF;
        	
		-- To delete record from t106_address table, before voiding the party
		DELETE FROM t106_address WHERE c101_party_id = p_partyid; 
		
		UPDATE t101_party
		   SET c101_void_fl = 'Y'
			 , c101_last_updated_by = p_userid
			 , c101_last_updated_date = SYSDATE
		 WHERE c101_party_id = p_partyid;

       -- 7010:Group Pricing Aff, 7011:Healthcare Aff, 7008:Account Group, 26241117:RPC Setup, 26241118:Health System Setup
	    v_rule_value := get_rule_value(v_party_type, 'EXCLUDE_PARTY_TYPE'); --PC-2374
          IF v_rule_value = 'Y' THEN
          
		 		SELECT count(1) INTO v_count FROM T704A_ACCOUNT_ATTRIBUTE
		 		WHERE C704A_VOID_FL IS NULL 
            	AND c704a_attribute_value = TO_CHAR(p_partyid);
        	IF v_count >  0 THEN
        		IF v_party_type =  '7008' THEN
          			raise_application_error ('-20613', '');
          		ELSE
          			raise_application_error ('-20753', '');
          		END IF;	
        	END IF;
        	
		END IF;
	END gm_void_party;

	/******************************************************************
	* Description : Procedure to cancel affiliation  info
	*******************************************************************/
	PROCEDURE gm_void_affiliation (
		p_partyaffiliationid   IN	t1014_party_affiliation.c1014_party_affiliation_id%TYPE
	  , p_userid			   IN	t1014_party_affiliation.c1014_last_updated_by%TYPE
	)
	AS
	BEGIN
		UPDATE t1014_party_affiliation
		   SET c1014_void_fl = 'Y'
			 , c1014_last_updated_by = p_userid
			 , c1014_last_updated_date = SYSDATE
		 WHERE c1014_party_affiliation_id = p_partyaffiliationid;
	END gm_void_affiliation;

/**************
	* Description : Procedure to cancel licnese info

*************************/
	PROCEDURE gm_void_license (
		p_licenseid   IN   t1030_license.c1030_license_id%TYPE
	  , p_userid	  IN   t1030_license.c1030_last_updated_by%TYPE
	)
	AS
	BEGIN
		UPDATE t1030_license
		   SET c1030_void_fl = 'Y'
			 , c1030_last_updated_by = p_userid
			 , c1030_last_updated_date = SYSDATE
		 WHERE c1030_license_id = p_licenseid;
	END gm_void_license;

	 /******************************************************************
	* Description : Procedure to cancel the achievement info
	****************************************************************/
	PROCEDURE gm_void_achievementinfo (
		p_achievementid   IN   t1013_party_acheivement.c1013_party_acheivement_id%TYPE
	  , p_userid		  IN   t1013_party_acheivement.c1013_last_updated_by%TYPE
	)
	AS
	BEGIN
		COMMIT;

		UPDATE t1013_party_acheivement
		   SET c1013_void_fl = 'Y'
			 , c1013_last_updated_by = p_userid
			 , c1013_last_updated_date = SYSDATE
		 WHERE c1013_party_acheivement_id = p_achievementid;
	END gm_void_achievementinfo;

	/******************************************************************
	* Description : Procedure to cancel the contact info
	****************************************************************/
	PROCEDURE gm_void_contact (
		p_contactid   IN   t107_contact.c107_contact_id%TYPE
	  , p_userid	  IN   t107_contact.c107_last_updated_by%TYPE
	)
	AS
	v_count NUMBER;
	BEGIN
		SELECT COUNT(1) INTO v_count FROM  t107_contact 
		WHERE c107_primary_fl = 'Y' 
		AND c901_mode = '90452' AND c107_void_fl IS NULL  -- 90452 : Email
		AND c107_contact_id = p_contactid;
		
		IF v_count =  1 THEN
          		raise_application_error ('-20750', ''); -- We cannot void primary email id
        END IF;
        
		UPDATE t107_contact
		   SET c107_void_fl = 'Y'
			 , c107_last_updated_by = p_userid
			 , c107_last_updated_date = SYSDATE
		 WHERE c107_contact_id = p_contactid;
	END gm_void_contact;

	/******************************************************************
	* Description : Procedure to cancel the globus association info
	****************************************************************/
	PROCEDURE gm_void_globusinfo (
		p_globusinfoid	 IN   t108_party_to_party_link.c108_party_party_link_id%TYPE
	  , p_userid		 IN   t108_party_to_party_link.c108_last_updated_by%TYPE
	)
	AS
	BEGIN
		UPDATE t108_party_to_party_link
		   SET c108_void_fl = 'Y'
			 , c108_last_updated_by = p_userid
			 , c108_last_updated_date = SYSDATE
		 WHERE c108_party_party_link_id = p_globusinfoid;
	END gm_void_globusinfo;

	/******************************************************************
	* Description : Procedure to cancel the address info
	****************************************************************/
	PROCEDURE gm_void_address (
		p_addressid   IN   t106_address.c106_address_id%TYPE
	  , p_userid	  IN   t106_address.c106_last_updated_by%TYPE
	)
	AS
	v_shipcount  NUMBER;
	v_primarycnt NUMBER;
	v_inactive_fl	t106_address.c106_inactive_fl%TYPE;
	BEGIN
		
		SELECT COUNT (1)
		  INTO v_shipcount
	      FROM t907_shipping_info
		 WHERE c106_address_id = p_addressid 
		   AND c907_status_fl = 40 
		   AND c907_void_fl IS NULL;
		BEGIN
		-- Get the inactive flag, to find whether the address is inactive or not
			SELECT c106_inactive_fl
				INTO v_inactive_fl
				FROM t106_address
			WHERE c106_address_id = p_addressid
				AND c106_void_fl IS NULL;
			EXCEPTION
	         WHEN NO_DATA_FOUND
	         THEN
	            v_inactive_fl := null;
		END; 
		 -- should be able to void the address without throwing the below error if the address is already inactive  
		IF v_shipcount > 0 AND (NVL(v_inactive_fl,'N') != 'Y')
		THEN
				raise_application_error ('-20751', ''); --There is a shipment details attached with this address, cannot void.
		END IF;
		
		SELECT COUNT(1) INTO v_primarycnt
		  FROM t106_address 
		 WHERE c106_address_id = p_addressid AND c106_void_fl IS NULL AND c106_primary_fl = 'Y';
			 
		IF v_primarycnt > 0
		THEN
			raise_application_error ('-20752', '');
		END IF;
		
		UPDATE t106_address
		   SET c106_void_fl = 'Y'
			 , c106_last_updated_by = p_userid
			 , c106_last_updated_date = SYSDATE
		 WHERE c106_address_id = p_addressid;
	END gm_void_address;

	/******************************************************************
	* Description : Procedure to cancel the education info
	****************************************************************/
	PROCEDURE gm_void_education (
		p_educationid	IN	 t1010_education.c1010_education_id%TYPE
	  , p_userid		IN	 t1010_education.c1010_last_updated_by%TYPE
	)
	AS
	BEGIN
		UPDATE t1010_education
		   SET c1010_void_fl = 'Y'
			 , c1010_last_updated_by = p_userid
			 , c1010_last_updated_date = SYSDATE
		 WHERE c1010_education_id = p_educationid;
	END gm_void_education;

	/******************************************************************
	* Description : Procedure to cancel the publication info
	****************************************************************/
	PROCEDURE gm_void_publication (
		p_publicationid   IN   t1040_publication.c1040_publication_id%TYPE
	  , p_userid		  IN   t1040_publication.c1040_last_updated_by%TYPE
	)
	AS
	BEGIN
		UPDATE t1040_publication
		   SET c1040_void_fl = 'Y'
			 , c1040_last_updated_by = p_userid
			 , c1040_last_updated_date = SYSDATE
		 WHERE c1040_publication_id = p_publicationid;
	END gm_void_publication;

	/******************************************************************
	* Description : Procedure to cancel the career info
	****************************************************************/
	PROCEDURE gm_void_career (
		p_careerid	 IN   t1020_career.c1020_career_id%TYPE
	  , p_userid	 IN   t1020_career.c1020_last_updated_by%TYPE
	)
	AS
	BEGIN
		UPDATE t1020_career
		   SET c1020_void_fl = 'Y'
			 , c1020_last_updated_by = p_userid
			 , c1020_last_updated_date = SYSDATE
		 WHERE c1020_career_id = p_careerid;
	END gm_void_career;
END gm_pkg_cm_party_cancel;
/
