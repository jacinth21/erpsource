/* Formatted on 2008/10/09 15:09 (Formatter Plus v4.8.0) */
--@"C:\Database\Packages\Common\gm_pkg_cm_audit_trail.bdy";


CREATE OR REPLACE PACKAGE BODY gm_pkg_cm_audit_trail
IS
	/*************************************************************************
	 * Purpose: Procedure used to load sales demand, calculated value and
	 *	Forecast information
	 *************************************************************************/
	PROCEDURE gm_cm_sav_audit_log (
		p_ref_id	   IN	t941_audit_trail_log.c941_ref_id%TYPE
	  , p_value 	   IN	t941_audit_trail_log.c941_value%TYPE
	  , p_trail_id	   IN	t941_audit_trail_log.c940_audit_trail_id%TYPE
	  , p_updated_by   IN	t941_audit_trail_log.c941_updated_by%TYPE
	  , p_updated_dt   IN	t941_audit_trail_log.c941_updated_date%TYPE
	)
	AS
	
	BEGIN
			gm_pkg_cm_audit_trail.gm_cm_sav_audit_log(p_ref_id,p_value,p_trail_id,'',p_updated_by,p_updated_dt);
		
	END gm_cm_sav_audit_log;
		/*************************************************************************
	 * Purpose: Procedure used to load sales demand, calculated value and
	 *	Forecast information based on the company
	 *************************************************************************/
	PROCEDURE gm_cm_sav_audit_log (
		p_ref_id	   IN	t941_audit_trail_log.c941_ref_id%TYPE
	  , p_value 	   IN	t941_audit_trail_log.c941_value%TYPE
	  , p_trail_id	   IN	t941_audit_trail_log.c940_audit_trail_id%TYPE
	  , p_company_id   IN   T1900_COMPANY.C1900_COMPANY_ID%TYPE
	  , p_updated_by   IN	t941_audit_trail_log.c941_updated_by%TYPE
	  , p_updated_dt   IN	t941_audit_trail_log.c941_updated_date%TYPE
	)
	AS
		--
		v_from_period  DATE;
		v_to_period    DATE;
	--
	BEGIN
		--
		INSERT INTO t941_audit_trail_log
					(c941_audit_trail_log_id, c941_ref_id, c941_value, c941_updated_by, c941_updated_date
				   , c940_audit_trail_id, C1900_COMPANY_ID
					)
			 VALUES (s941_audit_trail_log.NEXTVAL, p_ref_id, p_value, p_updated_by, p_updated_dt
				   , p_trail_id, p_company_id
					);
	--
	END gm_cm_sav_audit_log;

--
	PROCEDURE gm_fch_parthistory (
		p_demandmastertid	IN		 t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_refid 			IN		 t941_audit_trail_log.c941_ref_id%TYPE
	  , p_detail			OUT 	 TYPES.cursor_type
	)
	AS
	 /*******************************************************
	* Description : Procedure to fetch Par history for a demand sheet
	* Author		: Xun
	*******************************************************/
	BEGIN
		OPEN p_detail
		 FOR
			 SELECT   t941.c941_value pvalue, get_user_name (t941.c941_updated_by) uby
					, TO_CHAR (t941.c941_updated_date, get_rule_value('DATEFMT','DATEFORMAT')||' hh24:mi:ss') udate
					, t4022.c4022_history_fl historyfl
				 FROM t941_audit_trail_log t941, t4022_demand_par_detail t4022
				WHERE t4022.c4022_demand_par_detail_id = t941.c941_ref_id
				  AND t941.c941_ref_id = p_refid
				  AND t941.c940_audit_trail_id = 1005
			 ORDER BY t941.c941_audit_trail_log_id DESC;
	END gm_fch_parthistory;

--

	/*******************************************************
   * Description : Procedure to fetch required date history Info
   * Author 	 : Xun
   *******************************************************/
--
	PROCEDURE gm_fch_history (
		p_ref_id	   IN		t941_audit_trail_log.c941_ref_id%TYPE
	  , p_audit_id	   IN		t940_audit_trail.c940_audit_trail_id%TYPE
	  , p_reqhistory   OUT		TYPES.cursor_type
	)
	AS
	 
	v_comp_dt_fmt VARCHAR2 (20);
	BEGIN 
	     SELECT get_compdtfmt_frm_cntx() 
	       INTO v_comp_dt_fmt 
	       FROM dual;
	       	       
		OPEN p_reqhistory
		 FOR
			 SELECT   t941.c941_ref_id reqid, c941_value rvalue, get_user_name (t941.c941_updated_by) updatedby
					, TO_CHAR (t941.c941_updated_date, v_comp_dt_fmt||' HH24:MI:SS') update_date
				 FROM t940_audit_trail t940, t941_audit_trail_log t941
				WHERE t941.c941_ref_id = p_ref_id
				  AND t941.c940_audit_trail_id = t940.c940_audit_trail_id
				  AND t940.c940_audit_trail_id = p_audit_id
				  AND t940.c940_void_fl IS NULL
			 ORDER BY t941.c941_updated_date desc;
	END gm_fch_history;
	
	/*******************************************************
	* Description : Procedure to get the dynamic procedure based on rules and fetch the Audit trail cursor.
	* Author   : mmuthusamy
	*******************************************************/
	--
	PROCEDURE gm_fch_dynamic_log_history (
	        p_rule_id  IN t906_rules.c906_rule_id%TYPE,
	        p_txn_id   IN t941_audit_trail_log.c941_ref_id%TYPE,
	        p_type     IN t901_code_lookup.c901_code_id%TYPE,
	        p_date_fmt IN t901_code_lookup.c901_code_nm%TYPE,
	        p_out_audit_history OUT TYPES.cursor_type)
	AS
	    v_rule_value  t906_rules.c906_rule_value%TYPE;
	    v_exec_string VARCHAR2 (2000) ;
	BEGIN
		
	    -- Based on Rule id to get the dynamic audit trail log procedure
	    
	     SELECT get_rule_value (p_rule_id, 'DYNAMIC_AUDIT_TRAIL')
	       INTO v_rule_value
	       FROM DUAL;
	       
	    -- No rules found then
	    
	    IF v_rule_value IS NULL THEN
	        gm_raise_application_error ('-20999', '58', v_rule_value) ;
	    END IF;
	    
	    -- to form the procedure call
	    
	    v_exec_string := 'BEGIN ' || v_rule_value || '(:p_txn_id, :p_type, :p_date_fmt, :p_out_audit_history); END;';
	    
	    EXECUTE IMMEDIATE v_exec_string USING p_txn_id,
	    p_type, p_date_fmt, 
	    OUT p_out_audit_history;
	    
	    
	END gm_fch_dynamic_log_history;	
	
END gm_pkg_cm_audit_trail;
/
