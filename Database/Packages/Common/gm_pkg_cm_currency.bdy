/* Formatted on 2010/02/02 15:02 (Formatter Plus v4.8.0) */
-- @"c:\database\packages\common\gm_pkg_cm_currency.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_cm_currency
IS
   /*******************************************************
   * Purpose: This procedure will save the currency detail
   * Author : Himanshu
   *******************************************************/
   PROCEDURE gm_sav_curr_conv_dtl (
      p_curr_id        IN OUT   t907_currency_conv.c907_curr_seq_id%TYPE,
      p_curr_cd_from   IN       t907_currency_conv.c907_curr_cd_from%TYPE,
      p_curr_cd_to     IN       t907_currency_conv.c907_curr_cd_to%TYPE,
      p_from_date      IN       t907_currency_conv.c907_from_date%TYPE,
      p_to_date        IN       t907_currency_conv.c907_to_date%TYPE,
      p_curr_value     IN       t907_currency_conv.c907_curr_value%TYPE,
      p_conv_type      IN       t907_currency_conv.c901_conversion_type%TYPE,
      p_user_id        IN       t907_currency_conv.c907_created_by%TYPE,
      p_transid_str    IN       CLOB
   )
   AS
      v_cnt    NUMBER;
      v_company_id T1900_COMPANY.C1900_Company_Id%TYPE;
      v_curr_trans_comp_id T1900_COMPANY.C1900_Company_Id%TYPE;
      v_trans_valid_fl  VARCHAR2(10) := 'N';
      v_opt VARCHAR2(10); 
   BEGIN
   
   	  SELECT  get_compid_frm_cntx()
		INTO	v_company_id
		FROM	dual; 
		
	--Company which having the currency conversion by transaction
	 SELECT get_rule_value_by_company(v_company_id,'CURR_CONV_BY_TRANS',v_company_id) 
	  INTO v_curr_trans_comp_id 
	  FROM DUAL;
	  
       -- If no record id is passed check to see if it exists except for the company having currency conversion based on the Transaction
       IF (p_curr_id IS NULL) AND (v_company_id != v_curr_trans_comp_id)
       THEN
           SELECT count(1)
             INTO v_cnt
             FROM t907_currency_conv t907
            WHERE c907_void_fl IS NULL
              AND (c907_from_date <= p_from_date AND c907_to_date >= p_to_date)                  
              AND c907_curr_cd_from = p_curr_cd_from
              AND c907_curr_cd_to = p_curr_cd_to
              AND c901_conversion_type = p_conv_type; 
       END IF;
       
       IF (v_cnt > 0)
       THEN
           raise_application_error ('-20585', '');-- Exchange Rate has been entered for this period.
       END IF;
       
      -- to prevent the validation and save part for the transaction at the time of edit 
      IF p_curr_id IS NOT NULL
      THEN
      		v_opt:= 'EDIT';
      END IF;
     
      -- Validate the input string (Transaction Ids) for the company having conversion by transaction
      IF (p_transid_str IS NOT NULL) AND (v_opt IS NULL)
      THEN
        -- to validate the transaction details
       	gm_validate_curr_conv_trans(p_transid_str);
      END IF;
       

      -- Update or insert record if changing the currency value or adding new conversion
      UPDATE t907_currency_conv
         SET c907_curr_cd_from = p_curr_cd_from,
             c907_curr_cd_to = p_curr_cd_to,
             c907_from_date = p_from_date,
             c907_to_date =  p_to_date,
             c907_curr_value = p_curr_value,
             c901_conversion_type = p_conv_type,
             c901_source = '106912',
             c907_last_updated_by = p_user_id,
             c907_last_updated_date = CURRENT_DATE
       WHERE c907_curr_seq_id = p_curr_id
         AND c907_void_fl IS NULL;

      IF (SQL%ROWCOUNT = 0)
      THEN
         SELECT s907_curr_conv.NEXTVAL
           INTO p_curr_id
           FROM DUAL;

         INSERT INTO t907_currency_conv
                     (c907_curr_seq_id, c907_curr_cd_from, c907_curr_cd_to,
                      c907_from_date,
                      c907_to_date, c907_curr_value, c901_conversion_type,c901_source,
                      c907_created_by, c907_created_date, C1900_Company_Id
                     )
              VALUES (p_curr_id, p_curr_cd_from, p_curr_cd_to,
                      p_from_date,
                      p_to_date, p_curr_value, p_conv_type,'106912',
                      p_user_id, CURRENT_DATE , v_company_id
                     );
      END IF;
      
      -- To save the transaction details for the currency conversion 
      IF (p_transid_str IS NOT NULL) AND (v_opt IS NULL)
      THEN
       	-- save transaction details
      	gm_sav_curr_conv_trans_dtls(p_curr_id,p_transid_str,p_user_id);
      END IF;
      
   END gm_sav_curr_conv_dtl;

-------------------------

   -----------------------------
   PROCEDURE gm_fch_curr_conv_dtl (
      p_curr_id     IN       t907_currency_conv.c907_curr_seq_id%TYPE,
      p_recordset   OUT      TYPES.cursor_type,
      p_transrecordset   OUT      TYPES.cursor_type
   )
   AS
/***************************************************************************************************
  * This Procedure is to to Fetch currency detail
  * Author : Himanshu Patel
****************************************************************************************************/
   BEGIN
--	
	  -- Using this procedure getting the currency conversion details and 
	  -- also the total number of transaction to show at the time of Edit and for Popup window 
      OPEN p_recordset
       FOR
          SELECT t907.c907_curr_cd_from currfrom,
                 t907.c907_curr_cd_to currto,
                 t907.c907_from_date dtcurrfromdt,
                 t907.c907_to_date dtcurrtodt,
                 t907.c907_curr_value currvalue,
                 t907.c901_conversion_type convtype,
                 count(t907a.C907a_Ref_Id ) transcnt,
                 get_user_name(T907.C907_Created_By) createdby ,
                 T907.C907_Created_Date dtcreateddate,
                 get_user_name(T907.C907_Last_Updated_By) updatedby,
                 T907.C907_Last_Updated_Date dtupdateddt
            FROM t907_currency_conv t907,T907a_Currency_Trans T907a
           WHERE t907.c907_curr_seq_id = p_curr_id
             AND T907.C907_Curr_Seq_Id = T907a.C907_Curr_Seq_Id(+)
             AND T907a.C907a_Void_Fl(+) IS NULL
             AND t907.c907_void_fl IS NULL
             GROUP BY T907.C907_Curr_Seq_Id, t907.c907_from_date,T907.C907_To_Date,
              T907.C907_Curr_Value,t907.c907_curr_cd_from,t907.c907_curr_cd_to,T907.C901_Conversion_Type,
              T907.C907_Last_Updated_By,T907.C907_Created_By,T907.C907_Last_Updated_Date,T907.C907_Created_Date;
             
        -- To get the transaction details for the currency conversion
	    gm_pkg_cm_currency.gm_fch_curr_conv_trans_dtl(p_curr_id,p_transrecordset);
--
   END gm_fch_curr_conv_dtl;

----------------------------------------------
-----------------------------
   PROCEDURE gm_fch_all_curr_conv_dtl (
      p_curr_cd_from   IN       t907_currency_conv.c907_curr_cd_from%TYPE,
      p_curr_cd_to     IN       t907_currency_conv.c907_curr_cd_to%TYPE,
      p_conv_type      IN       t907_currency_conv.c901_conversion_type%TYPE,
      p_from_date      IN       t907_currency_conv.c907_from_date%TYPE,
      p_to_date        IN       t907_currency_conv.c907_to_date%TYPE,
      p_source         IN       CLOB,
      p_recordset      OUT      TYPES.cursor_type
   )
   AS
   	 v_source CLOB;
   	 v_company_id T1900_COMPANY.C1900_Company_Id%TYPE;
/***************************************************************************************************
  * This Procedure is to to Fetch currency details
  * Author : Himanshu Patel
****************************************************************************************************/
   BEGIN
--
		SELECT  get_compid_frm_cntx()
		INTO	v_company_id
		FROM	dual; 
		
		--Defaulting source to all
		IF p_source IS NULL
		THEN
			v_source := '106910,106911,106912';
		ELSE
			v_source := p_source;
		END IF;
		my_context.set_my_inlist_ctx (v_source);	
	
	  -- This query is used to load the Currncy Conversion Report, 
	  -- Added group by to get the transaction qty if applicable	
      OPEN p_recordset
       FOR
          SELECT   t907.c907_curr_seq_id currid,
                   get_code_name_alt (t907.c907_curr_cd_from) currfrom,
                   get_code_name_alt (t907.c907_curr_cd_to) currto,
                   t907.c907_from_date dtcurrfromdt,
                   t907.c907_to_date dtcurrtodt,
                   t907.c907_curr_value currvalue,
                   (gm_fch_curr_id (t907.c907_curr_seq_id)) curridflag,
                   get_log_flag (t907.c907_curr_seq_id, 1253) clog,
                   get_code_name (t907.c901_conversion_type) convtp,
                   count(t907a.C907a_Ref_Id ) transcnt,
                   t907.c901_source source_id,
                   get_code_name(t907.c901_source) source,
                   t907.c907_failure_fl failure_fl
              FROM t907_currency_conv t907,T907a_Currency_Trans T907a
             WHERE t907.c907_curr_cd_from =
                      DECODE (p_curr_cd_from,
                              0, t907.c907_curr_cd_from,
                              p_curr_cd_from
                             )
               AND t907.c907_curr_cd_to =
                      DECODE (p_curr_cd_to,
                              0, t907.c907_curr_cd_to,
                              p_curr_cd_to
                             )
               AND t907.c907_from_date >=
                      DECODE (p_from_date,
                              NULL, t907.c907_from_date,
                              (p_from_date
                              )
                             )
               AND t907.c907_to_date <=
                      DECODE (p_to_date,
                              NULL, t907.c907_to_date,
                              (p_to_date
                              )
                             )
               AND t907.c901_source IN (SELECT token FROM v_in_list)
               AND t907.c907_void_fl IS NULL
               And T907a.C907a_Void_Fl(+) Is Null
               AND DECODE(v_company_id,1000,NVL(T907.C1900_Company_Id,1000) ,T907.C1900_Company_Id) = v_company_id 
               AND T907.C907_Curr_Seq_Id     = T907a.C907_Curr_Seq_Id(+)
               AND t907.c901_conversion_type = DECODE (p_conv_type, 0, t907.c901_conversion_type, p_conv_type)
          Group By T907.C907_Curr_Seq_Id, t907.c907_from_date,T907.C907_To_Date, T907.C907_Curr_Value,
                t907.c907_curr_cd_from,t907.c907_curr_cd_to,T907.C901_Conversion_Type,t907.c901_source,t907.c907_failure_fl
          ORDER BY t907.c907_from_date DESC, t907.c907_to_date DESC;
--
   END gm_fch_all_curr_conv_dtl;

   /*******************************************************************************************
   * Purpose: This procedure will check whether currId is already exist in t907a_currency_trans
   * Author : Himanshu
   ********************************************************************************************/
   FUNCTION gm_fch_curr_id (
      p_curr_id   IN   t907_currency_conv.c907_curr_seq_id%TYPE
   )
      RETURN VARCHAR2
   IS
      v_cnt    NUMBER;
      v_flag   VARCHAR2 (1);
      v_company_id T1900_COMPANY.C1900_Company_Id%TYPE;
      v_curr_trans_comp_id T1900_COMPANY.C1900_Company_Id%TYPE;
   BEGIN
--

		SELECT  get_compid_frm_cntx()
			INTO	v_company_id
			FROM	dual; 
		
		SELECT get_rule_value_by_company(v_company_id,'CURR_CONV_BY_TRANS',v_company_id) 
		    INTO v_curr_trans_comp_id 
		    FROM DUAL;

	-- Using this flag enabling or disabling the Edit icon.
	-- For the currency conversion by transaction company, checking if the any one of the transaction received from intransit.
       IF (v_company_id = v_curr_trans_comp_id)
		THEN
			SELECT COUNT(1) INTO v_cnt
				FROM T504e_Consignment_In_Trans
				WHERE C504_Consignment_Id IN
				  (SELECT C907a_Ref_Id
				  FROM T907a_Currency_Trans
				  WHERE C907_Curr_Seq_Id = p_curr_id
				  AND C907a_Void_Fl     IS NULL
				  )
				AND C1900_Company_Id = v_company_id
				AND C504e_Void_Fl   IS NULL
				AND C901_Status     IS NOT NULL;				
		ELSE
			SELECT COUNT (1)
			  INTO v_cnt
			  FROM t907a_currency_trans
			 WHERE c907_curr_seq_id = p_curr_id;
			 
		END IF;
      
--
      IF (v_cnt > 0)
      THEN
         v_flag := 'Y';
      END IF;

      RETURN v_flag;
   END;
   
   /****************************************************************
   * Description : Procedure to fetch month rate flag
   * Author      : Rick Schmid
   ****************************************************************/
   PROCEDURE gm_fch_month_ratefl (
       p_curr_from IN t907_currency_conv.C907_CURR_CD_FROM%TYPE,
       p_curr_to   IN t907_currency_conv.C907_CURR_CD_TO%TYPE,
       p_conv_type IN t907_currency_conv.c901_conversion_type%TYPE,
       p_rate_fl   OUT VARCHAR2
   )
   AS
       v_cnt    NUMBER;
       v_company_id T1900_COMPANY.C1900_Company_Id%TYPE;
   BEGIN
		
		SELECT  get_compid_frm_cntx()
		INTO	v_company_id
		FROM	dual; 
		
	   IF (p_conv_type = 101103)  -- Constant
       THEN
           p_rate_fl := '';
       ELSE
           -- For conversion types of Purchase (101104), Average (101102), or Spot (101101), check if record exists
           SELECT count(1)
             INTO v_cnt
             FROM t907_currency_conv t907
            WHERE c907_from_date <= decode(p_conv_type , 101104, CURRENT_DATE,
                  101101, add_months(CURRENT_DATE, -1), 101102,
                  add_months(CURRENT_DATE, -1))
              AND c907_to_date >= decode(p_conv_type , 101104, CURRENT_DATE, 101101,
                  add_months(CURRENT_DATE, -1), 101102, add_months(CURRENT_DATE, -1))
              AND c901_conversion_type = p_conv_type
              AND C907_CURR_CD_FROM = p_curr_from
              AND C907_CURR_CD_TO = p_curr_to
              AND C1900_Company_Id = v_company_id
              AND c907_void_fl IS NULL;
     
           IF (v_cnt > 0)
           THEN
               p_rate_fl := 'Y';
           ELSE
               p_rate_fl := 'N';
           END IF;
       END IF;
       
   END gm_fch_month_ratefl;
   
   /*****************************************************************************************
   * Description : Procedure to validate the transaction id for the currency conversion
   * Author :rdinesh
   *****************************************************************************************/
   
   PROCEDURE gm_validate_curr_conv_trans (
   	p_trans_str IN CLOB
   ) 
   AS
   	v_strlen    NUMBER  := NVL (LENGTH (p_trans_str), 0) ;
    v_trans_string    CLOB := p_trans_str || ',';
    v_trans_id VARCHAR2(100);
    v_company_id T1900_COMPANY.C1900_COMPANY_ID%TYPE;
    v_trans_cnt NUMBER;
    v_exist_cnt NUMBER;
    v_trans_err_str VARCHAR2(4000);
    v_in_transit_err_str VARCHAR2(4000);
    v_exist_err_str VARCHAR2(4000);
    v_trans_status_fl T504e_Consignment_In_Trans.C901_Status%TYPE;
    v_err_msg VARCHAR2(4000);
   
   
   BEGIN
   
   	   --Getting company id from context.   
    SELECT get_compid_frm_cntx()
	  INTO v_company_id 
	  FROM dual;
	  
  	IF (v_strlen >0) 
  	THEN
  		WHILE INSTR (v_trans_string, ',') <> 0
  		LOOP
  			v_trans_id := SUBSTR (v_trans_string, 1, INSTR (v_trans_string, ',') - 1);
  			v_trans_string := SUBSTR (v_trans_string, INSTR (v_trans_string, ',') + 1);
  			
  			-- Get the count to check the consignment is valid, Type should be ICT(40057) and status is shipped (4)
  				SELECT COUNT(*) 
  				    INTO v_trans_cnt
					FROM T504_Consignment T504,T504e_Consignment_In_Trans T504e
					WHERE T504.C504_Consignment_Id = T504e.C504_Consignment_Id
					AND T504.C504_Consignment_Id   = v_trans_id
					AND C504_Status_Fl             = '4' -- Shipped
					AND C504_Type                  = '40057' --ICT
					AND T504e.C1900_Company_Id     = v_company_id
					AND C504_Void_Fl              IS NULL
					AND C504E_Void_Fl             IS NULL;
			
			-- If the consignment is valid, then check the status of consignment is In-Transit.
					IF v_trans_cnt > 0
					THEN
						SELECT C901_Status
						    INTO v_trans_status_fl
							FROM T504e_Consignment_In_Trans
							WHERE C504_Consignment_Id = v_trans_id
							  AND C1900_Company_Id = v_company_id
							  AND C504e_Void_Fl IS NULL;
					END IF;
					
			-- Get the	count to check if the consignment is already having exchange rate.
				SELECT COUNT(*)
				    INTO v_exist_cnt
					FROM T907a_Currency_Trans
					WHERE C907a_Ref_Id = v_trans_id
					AND C907a_Void_Fl IS NULL;
			
			-- To prepare invalid consignment string		
				 IF v_trans_cnt = 0
				 THEN
					 v_trans_err_str := v_trans_err_str || v_trans_id || ',';
					 
			-- To prepare the consignment status is not in intansit string.	
				 ELSIF v_trans_status_fl IS NOT NULL AND v_trans_cnt != 0 
				 THEN
					 v_in_transit_err_str := v_in_transit_err_str || v_trans_id || ',';
		
			-- To prepare string for exchange rate already set for consignment.
				 ELSIF v_exist_cnt > 0
				 THEN
				 	v_exist_err_str := v_exist_err_str || v_trans_id || ',';
				 END IF;
  			
  			
  		END LOOP;
  		
    END IF;
    -- To through error for incorrect tansactions.
    IF v_trans_err_str IS NOT NULL OR v_in_transit_err_str IS NOT NULL OR v_exist_err_str IS NOT NULL
    THEN
    
    	IF v_trans_err_str IS NOT NULL
    	THEN
    		-- to remove last comma
    		v_trans_err_str :=  substr(v_trans_err_str, 0, LENGTH(v_trans_err_str)-1);
    		v_err_msg := 'The Following Transaction(s) are not valid : ' || v_trans_err_str || '<BR>';
    	END IF;	
    	
    	IF v_in_transit_err_str IS NOT NULL
    	THEN
    		-- to remove last comma
    		v_in_transit_err_str :=  substr(v_in_transit_err_str, 0, LENGTH(v_in_transit_err_str)-1);
    		v_err_msg := v_err_msg || '&nbsp;The Following Transaction(s) are not in In-transit status : ' || v_in_transit_err_str || '<BR>';
    	END IF;	
    	
    	IF v_exist_err_str IS NOT NULL
    	THEN
    		-- to remove last comma
    		v_exist_err_str :=  substr(v_exist_err_str, 0, LENGTH(v_exist_err_str)-1);
    		v_err_msg := v_err_msg || '&nbsp;Exchange rate exist for the Following Transaction(s) : ' || v_exist_err_str;
    	END IF;	
    	
    	raise_application_error ('-20999',v_err_msg);
    	
   	END IF;
   	
   END gm_validate_curr_conv_trans;
   
   /*****************************************************************************************
   * Description : Procedure to save the transaction id for the currency conversion
   * Author :rdinesh
   *****************************************************************************************/
   PROCEDURE gm_sav_curr_conv_trans_dtls(
	   p_curr_id 	 IN t907_currency_conv.c907_curr_seq_id%TYPE,
	   p_trans_str IN CLOB,
	   p_user_id 	 IN t907_currency_conv.c907_created_by%TYPE
   )   
    AS
    	v_strlen    NUMBER  := NVL (LENGTH (p_trans_str), 0) ;
	    v_trans_string    CLOB := p_trans_str || ',';
	    v_trans_id VARCHAR2(100);
    
    	BEGIN
    		IF (v_strlen >0) 
		  	THEN
		  		WHILE INSTR (v_trans_string, ',') <> 0
		  		LOOP
		  			v_trans_id := SUBSTR (v_trans_string, 1, INSTR (v_trans_string, ',') - 1);
		  			v_trans_string := SUBSTR (v_trans_string, INSTR (v_trans_string, ',') + 1);
		  			
		  				INSERT INTO t907a_currency_trans
		  						( c907a_curr_trans_id, c907_curr_seq_id, c907a_ref_id,
			                      c901_ref_type, c907a_created_date, c907a_created_by
			                     )
			      		   VALUES( s907a_curr_trans.NEXTVAL, p_curr_id, v_trans_id,
			              		   91380, CURRENT_DATE, p_user_id
							);
		  			
		  		END LOOP;
    		END IF;
    /*******************************************************************************
   * Description : Procedure to fetch  currency conversion and transaction details
   * Author :rdinesh
   ********************************************************************************/  		
    END gm_sav_curr_conv_trans_dtls;
    
    PROCEDURE gm_fch_curr_conv_trans_dtl (
      p_curr_id     IN       t907_currency_conv.c907_curr_seq_id%TYPE,
      p_recordset   OUT      TYPES.cursor_type
   )
   AS

   BEGIN
      OPEN p_recordset
       FOR
          SELECT T907a.C907a_Ref_Id transid
			FROM T907_Currency_Conv T907,
			  T907a_Currency_Trans T907a
			WHERE T907.C907_Curr_Seq_Id = T907a.C907_Curr_Seq_Id
			AND T907.C907_Curr_Seq_Id   = p_curr_id
			AND T907.C907_Void_Fl      IS NULL
			AND T907a.C907a_Void_Fl(+)    IS NULL ;
			
   END gm_fch_curr_conv_trans_dtl;
   
    /*******************************************************************************************
   * Purpose: This function is used to get the currency rate for the transaction
   * Author : rdinesh
   ********************************************************************************************/
   FUNCTION gm_fch_trans_curr_value (
      p_ref_id   IN   T907a_Currency_Trans.C907a_Ref_Id%TYPE
   )
      RETURN VARCHAR2
   IS
      v_curr_conv_rate T907_Currency_Conv.C907_Curr_Value%TYPE;
   BEGIN

      		BEGIN
	   			SELECT DECODE(T907.C907_Curr_Value,0,1,T907.C907_Curr_Value)
	   			    INTO v_curr_conv_rate
					FROM T907_Currency_Conv T907,
					  T907a_Currency_Trans T907a
					WHERE T907.C907_Curr_Seq_Id = T907a.C907_Curr_Seq_Id
					AND T907a.C907a_Ref_Id   = p_ref_id
					AND T907.C907_Void_Fl      IS NULL
					AND T907a.C907a_Void_Fl    IS NULL;
				 EXCEPTION
				 WHEN NO_DATA_FOUND
				 THEN
					v_curr_conv_rate := '';
			END;
			
			IF v_curr_conv_rate IS NULL 
			THEN
				GM_RAISE_APPLICATION_ERROR('-20999','411',p_ref_id);
			END IF;		

      RETURN v_curr_conv_rate;
   END;
    
END gm_pkg_cm_currency;
/
