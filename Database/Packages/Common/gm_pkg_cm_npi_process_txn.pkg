/* Formatted on 2010/09/09 09:29 (Formatter Plus v4.8.0) */
--@"C:\Database\Packages\Common\gm_pkg_cm_npi_process_txn.pkg";

CREATE OR REPLACE PACKAGE gm_pkg_cm_npi_process_txn
IS

	/************************************************************************************
    * Description : Procedure to save the NPI details of the transaction in t6640 table
    *************************************************************************************/
	PROCEDURE gm_sav_npi_transaction(
		p_npi_tran_id 		IN OUT      t6640_npi_transaction.c6640_id%TYPE,
		p_ref_id      		IN          t6640_npi_transaction.c6640_ref_id %TYPE,
		p_account_id  		IN          t6640_npi_transaction.c704_account_id%TYPE,
		p_surgeon_npi_id  	IN  		t6640_npi_transaction.c6600_surgeon_npi_id%TYPE,
		p_surgeon_name    	IN  		t6640_npi_transaction.c6640_surgeon_name%TYPE,
		p_tran_flag       	IN  		t6640_npi_transaction.c6640_transaction_fl%TYPE,
		p_userid          	IN  		t6640_npi_transaction.c6640_created_by%TYPE,
		p_submit_type	  	IN			VARCHAR2 DEFAULT NULL,
		p_status            IN          NUMBER DEFAULT 52027
	);
	
	/****************************************************************************************
    * Description : Procedure to save the NPI details of the transaction in t6645_npi_account
    *****************************************************************************************/
	PROCEDURE gm_sav_npi_account(	
		p_account_id  		IN      t6640_npi_transaction.c704_account_id%TYPE,
		p_sales_rep_id 		IN 		t6640_npi_transaction.c703_sales_rep_id%TYPE,
		p_surgeon_npi_id  	IN  	t6640_npi_transaction.c6600_surgeon_npi_id%TYPE,
		p_party_surgeon_id 	IN 		t6640_npi_transaction.c101_party_surgeon_id%TYPE,
		p_surgeon_name    	IN  	t6640_npi_transaction.c6640_surgeon_name%TYPE,
		p_userid         	IN  	t6640_npi_transaction.c6640_created_by%TYPE,
		p_status            IN      NUMBER DEFAULT 52027
	);
	
	/***********************************************************
    * Description : Procedure to save the NPI details from IPAD
    ************************************************************/
	PROCEDURE gm_sav_all_npi_transaction (
		p_ref_id      	IN      t6640_npi_transaction.c6640_ref_id %TYPE,
	    p_npi_inputstr  IN		CLOB,
	    p_account_id  	IN      t6640_npi_transaction.c704_account_id%TYPE,
	    p_userid        IN  	t6640_npi_transaction.c6640_created_by%TYPE
	  
	);
	
	/**************************************************************************************************
    * Description : Procedure to update the void flag of the transaction while removing from screen
    **************************************************************************************************/
	PROCEDURE gm_cancel_npi_transaction(	
		p_npi_tran_id 	IN      t6640_npi_transaction.c6640_id%TYPE,
		p_userid        IN  	t6640_npi_transaction.c6640_created_by%TYPE
	);
	
	/*******************************************************************************************
    * Description : Procedure to save the transaction flag to 'Y' once the transaction is saved
    *******************************************************************************************/
	PROCEDURE gm_save_npi_tran_status(	
		p_ref_id      IN        t6640_npi_transaction.c6640_ref_id %TYPE,
		p_userid      IN  		t6640_npi_transaction.c6640_created_by%TYPE,
		p_trans_flag  IN		VARCHAR2 DEFAULT NULL
	);

	/*******************************************************************************************
    * Description : Procedure to update the old transaction id to new transaction id 
    * 				if the order id is modified from New order screen
    *******************************************************************************************/
	PROCEDURE gm_upd_npi_tran_id(	
		p_old_ref_id      IN        t6640_npi_transaction.c6640_ref_id %TYPE,
		p_new_ref_id      IN  		t6640_npi_transaction.c6640_ref_id %TYPE
	);
	
	/**************************************************************************************************
    * Description : Procedure to validate and update the valid flag of the NPI records saved for order
    ****************************************************************************************************/
	PROCEDURE gm_upd_npi_valid_data_us;
	
	/**************************************************************************************************
    * Description : Procedure to validate and update the valid flag of the OUS NPI records saved for order
    ****************************************************************************************************/
	PROCEDURE gm_upd_npi_valid_data_ous;
	
	/**********************************************************
    * Description : Procedure to save the user validated record
    ***********************************************************/
	PROCEDURE gm_sav_valid_npi(
		p_npi_txn_id	IN	t6640_npi_transaction.c6640_id%TYPE,
		p_user_id 		IN	t6640_npi_transaction.c6640_last_updated_by%TYPE
	);
	
	/***********************************************************************
    * Description : Procedure to void the NPI records if the order is voided
    ************************************************************************/
	PROCEDURE gm_sav_void_npi;
	/**********************************************************
    * Description : Procedure to fetch the valid NPI records
    ***********************************************************/
	PROCEDURE  gm_fch_valid_NPI_Dtls(
		p_npi_txn_id	IN	    t6640_npi_transaction.c6640_id%TYPE,
		p_out_cursor    OUT     Types.cursor_type
	);
	/**********************************************************
    * Description : Procedure to save the NPI records
    ***********************************************************/	
	PROCEDURE gm_sav_npi(
		p_npi_id	IN	    t6640_npi_transaction.c6600_surgeon_npi_id%TYPE,
		p_ord_id    IN      t6640_npi_transaction.c6640_ref_id%TYPE,
		p_first_nm  IN      t101_party.c101_first_nm%TYPE,
		p_middle_nm IN      t101_party.c101_middle_initial%TYPE,
		p_last_nm   IN      t101_party.c101_last_nm%TYPE,
		p_add1      IN      t106_address.c106_add1%TYPE,
		p_add2      IN      t106_address.c106_add2%TYPE,
		p_city      IN      t106_address.c106_city%TYPE,
		p_zip_code  IN      t106_address.c106_zip_code%TYPE,
		p_stage     IN      VARCHAR2,
		p_country   IN      VARCHAR2,
		p_user_id   IN      t106_address.c106_created_by%TYPE
	);
	/**********************************************************
    * Description : Procedure to update the NPI master flag
    ***********************************************************/	
	PROCEDURE gm_sav_npi_master_update(
		p_npi_id	IN	    t6640_npi_transaction.c6600_surgeon_npi_id%TYPE,
		p_ord_id    IN      t6640_npi_transaction.c6640_ref_id%TYPE,
		p_status_fl IN      t6640_npi_transaction.C6640_NPI_MASTER_FL%TYPE,
		p_user_id   IN      t106_address.c106_created_by%TYPE
	);
	/**********************************************************************************************
    * Description : Procedure to save the NPI and Surgeon details of the transaction in t6640 table
    **********************************************************************************************/
	PROCEDURE gm_sav_npi_partyid_transaction(
		p_npi_tran_id 		IN OUT  t6640_npi_transaction.c6640_id%TYPE,
		p_ref_id      		IN      t6640_npi_transaction.c6640_ref_id %TYPE,
		p_account_id  		IN      t6640_npi_transaction.c704_account_id%TYPE,
		p_surgeon_npi_id  	IN  	t6640_npi_transaction.c6600_surgeon_npi_id%TYPE,
		p_surgeon_name    	IN  	t6640_npi_transaction.c6640_surgeon_name%TYPE,
		p_tran_flag       	IN  	t6640_npi_transaction.c6640_transaction_fl%TYPE,
		p_userid          	IN  	t6640_npi_transaction.c6640_created_by%TYPE,
		p_submit_type	  	IN		VARCHAR2 DEFAULT NULL,
		p_surgeon_id		IN		t6640_npi_transaction.c101_party_surgeon_id%TYPE,
		p_status            IN      NUMBER DEFAULT 52027
	);
END gm_pkg_cm_npi_process_txn;
/
