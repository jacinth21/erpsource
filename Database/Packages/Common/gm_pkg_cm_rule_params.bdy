--@"c:\database\packages\common\gm_pkg_cm_rule_params.bdy";

create or replace PACKAGE BODY "GM_PKG_CM_RULE_PARAMS"
IS
  --
PROCEDURE gm_fch_edit_rule_params_info(
    p_code_grp     IN  t901_code_lookup.c901_code_grp%TYPE
	  , p_rule_grp_id   IN	t906_rules.c906_rule_grp_id%TYPE
    , p_data		 OUT   TYPES.cursor_type    
	)
  AS 
  
 BEGIN
 IF p_code_grp = 'SHPPM' THEN
   OPEN p_data
     FOR 
	 SELECT t901.c901_code_id labelId, t901.c901_code_nm labelNm, t704a.c704a_attribute_value rulvalue,
	    C901_CONTROL_TYPE cntrltyp, C902_CODE_NM_ALT cdgrp
	   FROM t901_code_lookup t901, t704a_account_attribute t704a
	  WHERE t901.c901_code_id   = t704a.c901_attribute_type (+)
	    AND t901.c901_code_grp IN
	    (SELECT c901_code_nm
	       FROM t901_code_lookup
	      WHERE c901_code_grp  = p_code_grp
	        AND c901_active_fl = 1
	    )
	    AND t704a.c704_account_id (+) = p_rule_grp_id
	    AND t901.c901_active_fl       = 1
        AND t704a.c704a_void_fl IS NULL
	ORDER BY  c901_code_grp,c901_code_seq_no ;
  ELSE
  OPEN p_data
     FOR 
     SELECT t901.c901_code_id labelId, t901.c901_code_nm labelNm, t906.c906_rule_value rulvalue,
        C901_CONTROL_TYPE cntrltyp, C902_CODE_NM_ALT cdgrp
       FROM t901_code_lookup t901, t906_rules t906
      WHERE t901.c901_code_id        = TO_NUMBER (t906.c906_rule_id(+))
        AND t901.c901_code_grp       = p_code_grp
        AND t906.c906_rule_grp_id(+) = p_rule_grp_id
        AND t901.c901_active_fl      = 1
   ORDER BY C901_CODE_SEQ_NO ;
  END IF;
  
  END gm_fch_edit_rule_params_info;

PROCEDURE gm_save_ruleparams(
	   p_rule_grp_id   IN	t906_rules.c906_rule_grp_id%TYPE
     ,p_inputstr IN   VARCHAR2
     ,p_user_id IN t906_rules.c906_created_by%TYPE
     
	)
  AS
 
  	v_strlen	   NUMBER := NVL (LENGTH (p_inputstr), 0);
		v_string	   VARCHAR2 (30000) := p_inputstr;
		v_substring  VARCHAR2 (1000);
    v_code_id     VARCHAR2 (1000);
    v_code_value  VARCHAR2 (1000);
    
 BEGIN
		IF v_strlen > 0	THEN
			WHILE INSTR (v_string, '|') <> 0
			LOOP
				--
				v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
				v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
				--
				 v_code_id:= NULL;
         v_code_value:= NULL;
				--		
				v_code_id	   := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring  := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_code_value := v_substring;
				
			
      UPDATE  T906_RULES
      SET c906_rule_value = v_code_value
      ,c906_last_updated_by = p_user_id
      ,c906_last_updated_date = CURRENT_DATE
      WHERE c906_rule_grp_id = p_rule_grp_id
      AND c906_rule_id = v_code_id;
      
      IF(SQL%ROWCOUNT = 0)
      THEN
        INSERT INTO t906_rules(
        c906_rule_seq_id
        ,c906_rule_id
        ,c906_rule_value
        ,c906_rule_grp_id
        ,c906_created_date
        ,c906_created_by) 
        VALUES(s906_rule.nextval,v_code_id,v_code_value,p_rule_grp_id,CURRENT_DATE,p_user_id);
        END IF;
			END LOOP;
		END IF;
	END gm_save_ruleparams;

	/*******************************************************
   * Description : To fetch the transaction rules
   *******************************************************/
	PROCEDURE gm_fch_trans_rules (
		p_rule_id		IN		 t906_rules.c906_rule_id%TYPE
	  , p_rule_grp_id	IN		 t906_rules.c906_rule_grp_id%TYPE
	  , p_rules 		OUT 	 TYPES.cursor_type
	  , p_comp_id		IN		 t1900_company.c1900_company_id%TYPE DEFAULT NULL
	)
	AS
		v_rule_grp	   t906_rules.c906_rule_id%TYPE;
	BEGIN
		
		BEGIN
			SELECT get_rule_value (p_rule_id, 'TRANS_CONTY')
			  INTO v_rule_grp
			  FROM DUAL;
	
			-- THIS WILL HANDLE THE SITUATION WHERE THE CONTROL,VERIFICATION FROM THE DB.
			IF v_rule_grp IS NULL
			THEN
				v_rule_grp	:= p_rule_id;
			END IF;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				RETURN;
		END;
	
		OPEN p_rules
		 FOR
			 SELECT c906_rule_id rule_id, c906_rule_desc rule_desc, c906_rule_value rule_value, c906_rule_grp_id rule_grp
				  , c901_rule_type rule_type
			   FROM t906_rules
			  WHERE c906_rule_grp_id = NVL (v_rule_grp, c906_rule_grp_id)
			  AND DECODE(C1900_COMPANY_ID,NULL,'-9999',C1900_COMPANY_ID) = DECODE(C1900_COMPANY_ID,NULL,'-9999',decode(p_comp_id,NULL,C1900_COMPANY_ID,p_comp_id));
	END gm_fch_trans_rules;
--
  /*******************************************************
   * Description : Procedure to get Device Rules 
   *               If 'p_rule_grp_id' parameter is null, get all the rules
   *               else get the rules for the group
   * Author 	 : Yogabalakrishnan Kannan
   * Modified By : 
   * Modified Dt : 08/17/2012
   *******************************************************/
PROCEDURE gm_fch_rules_bytype(
      p_dev_rule_type   IN     t906_rules.C901_RULE_TYPE%TYPE
     ,p_rule_grp_id	    IN     t906_rules.c906_rule_grp_id%TYPE
     ,p_rules	          OUT 	 TYPES.cursor_type
	)
	AS
	BEGIN
      OPEN p_rules
        FOR
			   SELECT  C906_RULE_ID  Rule_Id, C906_RULE_DESC rule_Desc                                                     
           , C906_RULE_VALUE Rule_value, C906_RULE_GRP_ID Rule_Group_Id, C901_RULE_TYPE Rule_Type     
           FROM T906_RULES                                                                            
          WHERE NVL(C901_RULE_TYPE,-9999) = NVL(p_dev_rule_type,NVL(C901_RULE_TYPE,-9999)) 
            AND C906_RULE_GRP_ID =  NVL(p_rule_grp_id,C906_RULE_GRP_ID)
            AND C906_VOID_FL IS NULL                                                                   
          ORDER BY C906_RULE_ID;                                                                      
	END gm_fch_rules_bytype;
--
	/********************************************************************************
	* Description : Procedure to fetch AD sytem rule values from T906B_SYSTEM_RULE table
	* Author      : Gopinathan
	*********************************************************************************/
	PROCEDURE gm_fch_system_rules (
	        p_group_id IN t906b_system_rule.c906b_group_id%TYPE,
	        p_ldap_id IN t906c_ldap_master.c906c_ldap_master_id%TYPE  DEFAULT NULL,
	        p_rules OUT TYPES.cursor_type)
	AS
	BEGIN
	    OPEN p_rules 
	     FOR SELECT C906b_Rule_Id KEY, C906b_Rule_Value VALUE, c906c_ldap_master_id ldap_id 
	           FROM T906b_System_Rule 
	          WHERE C906B_GROUP_ID = p_group_id
	          AND c906c_ldap_master_id = NVL(p_ldap_id,c906c_ldap_master_id) 
	          AND C906B_VOID_FL IS NULL;
	END gm_fch_system_rules;
	
	/********************************************************************************
	* Description : Procedure to save AD values in T906B_SYSTEM_RULE table
	* Author      : xqu
	*********************************************************************************/
	
	PROCEDURE gm_sav_LDAPCRD (
	        p_name     IN T906B_SYSTEM_RULE.C906B_RULE_VALUE%TYPE,
	        p_username IN T906B_SYSTEM_RULE.C906B_RULE_VALUE%TYPE,
	        p_password IN T906B_SYSTEM_RULE.C906B_RULE_VALUE%TYPE,
	        p_hostname IN T906B_SYSTEM_RULE.C906B_RULE_VALUE%TYPE,
	        p_port     IN T906B_SYSTEM_RULE.C906B_RULE_VALUE%TYPE,
	        p_userdn   IN T906B_SYSTEM_RULE.C906B_RULE_VALUE%TYPE,
	        p_base	   IN T906B_SYSTEM_RULE.C906B_RULE_VALUE%TYPE,
	        p_ldap_id  IN t906c_ldap_master.c906c_ldap_master_id%TYPE,
	        p_grpid    IN T906B_SYSTEM_RULE.C906B_GROUP_ID%TYPE,
	        p_user_id  IN T101_USER.C101_USER_ID%TYPE)
	AS
	BEGIN
	    gm_sav_system_rule_value (p_name, 'NAME', p_ldap_id, p_grpid, p_user_id) ;
	    gm_sav_system_rule_value (p_username, 'USERNAME', p_ldap_id,p_grpid, p_user_id) ;
	    gm_sav_system_rule_value (p_password, 'PASSWORD', p_ldap_id,p_grpid, p_user_id) ;
	    gm_sav_system_rule_value (p_hostname, 'HOSTNAME', p_ldap_id,p_grpid, p_user_id) ;
	    gm_sav_system_rule_value (p_port, 'PORT', p_ldap_id,p_grpid, p_user_id) ;
	    gm_sav_system_rule_value (p_userdn, 'USERDN', p_ldap_id,p_grpid, p_user_id) ;
	    gm_sav_system_rule_value (p_base, 'BASE', p_ldap_id,p_grpid, p_user_id) ;
	END gm_sav_LDAPCRD;
	
	/********************************************************************************
	* Description : Procedure to update rule values in T906B_SYSTEM_RULE table
	* Author      : xqu
	*********************************************************************************/
	PROCEDURE gm_sav_system_rule_value (
	        p_rule_value IN T906B_SYSTEM_RULE.C906B_RULE_VALUE%TYPE,
	        p_rule_id    IN T906B_SYSTEM_RULE.C906B_RULE_ID%TYPE,
	        p_ldap_id  IN t906c_ldap_master.c906c_ldap_master_id%TYPE,
	        p_grp_id     IN T906B_SYSTEM_RULE.C906B_GROUP_ID%TYPE,
	        p_user_id    IN T101_USER.C101_USER_ID%TYPE)
	AS
	BEGIN
	     UPDATE T906B_SYSTEM_RULE
	        SET C906B_RULE_VALUE   = p_rule_value, 
	            C906B_LAST_UPDATED_BY = p_user_id, 
	            C906B_LAST_UPDATED_DATE = CURRENT_DATE
	      WHERE C906B_RULE_ID  = p_rule_id
	        AND C906B_GROUP_ID = p_grp_id
	        AND c906c_ldap_master_id = p_ldap_id
	        AND C906B_VOID_FL IS NULL;
	        
	    IF (SQL%ROWCOUNT       = 0) THEN
	         INSERT
	           INTO T906B_SYSTEM_RULE
	            (
	                C906B_RULE_SEQ_ID, C906B_RULE_ID, C906B_RULE_VALUE
	              , C906B_GROUP_ID, C906B_CREATED_DATE, C906B_CREATED_BY
	              , c906c_ldap_master_id
	            )
	            VALUES
	            (
	                s906B_SYSTEM_RULE.NEXTVAL, p_rule_id, p_rule_value
	              , p_grp_id, CURRENT_DATE, p_user_id
	              , p_ldap_id
	            ) ;
	    END IF;
	END gm_sav_system_rule_value;
	
	/**********************************************************************************
	* Description : Procedure to update/save rules table to map the printer with user
	* Author      : arajan
	**********************************************************************************/
	PROCEDURE gm_sav_rule_params (
			p_rule_id    	IN 		t906_rules.c906_rule_id%TYPE,
	        p_rule_grp_id   IN 		t906_rules.c906_rule_grp_id%TYPE,
	        p_rule_value 	IN 		t906_rules.c906_rule_value%TYPE,
	        p_userid		IN		t906_rules.c906_created_by%TYPE
	)
	AS
	v_rule_value   t906_rules.c906_rule_value%TYPE;
	BEGIN
	
	   v_rule_value := p_rule_value;
		
	  IF p_rule_grp_id = 'PRINTER' THEN
	     BEGIN
              SELECT c902_code_nm_alt 
                INTO v_rule_value
                FROM t901_code_lookup 
               WHERE c901_code_grp ='REGPRN' 
                 AND c901_code_nm = p_rule_value 
                 AND c901_void_fl is null;
           EXCEPTION WHEN NO_DATA_FOUND
         THEN
              v_rule_value := p_rule_value;
         END; 
                 
    ELSIF p_rule_grp_id = 'ZEBRA_PRINTER' THEN
        BEGIN
              SELECT c902_code_nm_alt
                INTO v_rule_value
                FROM t901_code_lookup 
               WHERE c901_code_grp = 'ZEBPRN' 
                 AND c901_code_nm = p_rule_value 
                 AND c901_void_fl is null;
               EXCEPTION WHEN NO_DATA_FOUND
         THEN
              v_rule_value := p_rule_value;
         END; 
    END IF;
		
		UPDATE t906_rules
		SET c906_rule_value = v_rule_value
			, c906_last_updated_by = p_userid
			, c906_last_updated_date = CURRENT_DATE
		WHERE c906_rule_id = p_rule_id
		AND c906_rule_grp_id = p_rule_grp_id;

	        
	    IF (SQL%ROWCOUNT = 0) THEN
	    		INSERT INTO t906_rules 
				   	( c906_rule_seq_id, c906_rule_id, c906_rule_value
				   	, c906_rule_grp_id, c906_created_date, c906_created_by
				) 
				VALUES (s906_rule.NEXTVAL, p_rule_id, v_rule_value
					, p_rule_grp_id, CURRENT_DATE, p_userid
					);

	    END IF;
	END gm_sav_rule_params;

	/*******************************************************
	* Description	 : Procedure to get rules from Rules table
	* Author 	 : Yogabalakrishnan Kannan
	*******************************************************/
	--
	PROCEDURE gm_fch_rules(
	    p_rule_grp	           IN    VARCHAR2
	    ,p_out_rules	   OUT 	 TYPES.cursor_type
	    )
	AS
	BEGIN   

	  -- get comma delimated values to store in v_in_list
	  my_context.set_my_inlist_ctx (p_rule_grp) ;

	  OPEN p_out_rules FOR 
	   SELECT c906_rule_id rule_id,
	      c906_rule_desc rule_desc ,
	      c906_rule_value rule_value,
	      c906_rule_grp_id rule_group_id,
	      t901.c902_code_nm_alt rule_type,
        c906_rule_seq_id seq
	      FROM t906_rules, t901_code_lookup t901
	     WHERE c906_rule_grp_id = to_char(c901_code_id)
               AND c901_code_grp IN 
	         ('EXPRUL','UDISRC','EXPFMT')
	       AND c901_void_fl IS NULL
	       AND c901_active_fl = 1
	       AND c906_void_fl IS NULL 
	       AND c906_active_fl='Y' 
	     
		UNION ALL
	 
		SELECT 
			t1601.c1601_identify_symbol_name  rule_id,
			to_char(t1602.c1602_db_field_size) rule_desc,
			REPLACE(REPLACE(t1601.c1601_identify_symbol,'(',NULL),')',NULL) rule_value,
			to_char(t1601.c1600_issuing_agency_id) rule_group_id,
			'UDI_SOURCE' rule_type,
			T1601.C1601_ISSUE_AG_IDENTIFIER_ID seq
		FROM t1601_issue_agency_identifier t1601, t1602_issue_agency_symbol_dtl t1602, t1600_issue_agency t1600
		WHERE t1602.c1601_issue_ag_identifier_id = t1601.c1601_issue_ag_identifier_id
		AND t1600.c1600_issuing_agency_id = t1601.c1600_issuing_agency_id
		AND t1600.c1600_issuing_agency_id = 101 --GS1
		AND t1601.c1601_issue_ag_identifier_id NOT IN (3,5) -- Exclude MFG date[YYMMDD] and Serial #
		AND t1601.c1601_void_fl IS NULL
		
		UNION ALL
		
		SELECT 
			to_char(t1601.c1600_issuing_agency_id) rule_id,
			t1601.c1601_identify_symbol_name rule_desc,
			t1602.c1602_identifier_format rule_value,
			to_char(t1601.c1601_issue_ag_identifier_id) rule_group_id,
			'EXP_FORMAT' rule_type,
			rownum seq
		FROM t1601_issue_agency_identifier t1601, t1602_issue_agency_symbol_dtl t1602, t1600_issue_agency t1600
		WHERE t1602.c1601_issue_ag_identifier_id = t1601.c1601_issue_ag_identifier_id
		AND t1600.c1600_issuing_agency_id = t1601.c1600_issuing_agency_id
		AND t1600.c1600_issuing_agency_id = 101 -- GS1
		AND T1601.C1601_ISSUE_AG_IDENTIFIER_ID not in (3,5) -- Exclude MFG date[YYMMDD] and Serial #
		AND T1601.C1601_VOID_FL is null
		AND t1602.c1602_identifier_format IS NOT NULL
		ORDER BY rule_type,seq;
		
		END gm_fch_rules;
		
 /*******************************************************
   * Description : Procedure to get rules for inserts 
   * Author 	 : Matt B  
   * Modified By : 
   * Modified Dt : 
   *******************************************************/
PROCEDURE gm_fch_inserts_rules(
     p_rule_grp_id	    IN    T906_RULES.C906_RULE_GRP_ID%TYPE
     ,p_rules	        OUT   TYPES.cursor_type
	)
	AS
	BEGIN
      OPEN p_rules FOR
		  SELECT  C906_RULE_ID  Rule_Id, C906_RULE_DESC rule_Desc                                                     
              , C906_RULE_VALUE Rule_value, C906_RULE_GRP_ID Rule_Group_Id, C901_RULE_TYPE Rule_Type     
          FROM T906_RULES                                                                            
          WHERE C906_RULE_GRP_ID = p_rule_grp_id
            AND C906_VOID_FL IS NULL                                                                   
          ORDER BY C906_RULE_ID;                                                                      
	END gm_fch_inserts_rules;


  /*******************************************************
   * Description : Procedure to get txn id for inserts from rule table 
   * Author 	 : Matt B  
   * Modified By : 
   * Modified Dt : 
   *******************************************************/
PROCEDURE gm_fch_inserts_txn_id(
     p_rule_grp_id	    IN     T906_RULES.C906_RULE_GRP_ID%TYPE
    ,p_ordertype_id    IN     T901a_LOOKUP_ACCESS.C901_CODE_ID%TYPE 
    ,p_company_id      IN     T901a_LOOKUP_ACCESS.C1900_COMPANY_ID%TYPE   
    ,p_rules	        OUT    TYPES.cursor_type
	)
	AS
	BEGIN
      OPEN p_rules FOR
		 SELECT  T906.C906_RULE_ID  Rule_Id, T906.C906_RULE_DESC rule_Desc                                                     
              ,T906.C906_RULE_VALUE Rule_value, T906.C906_RULE_GRP_ID Rule_Group_Id, T906.C901_RULE_TYPE Rule_Type     
          FROM T906_RULES T906, T901a_LOOKUP_ACCESS T901A                                                                            
          WHERE T906.C906_RULE_GRP_ID = p_rule_grp_id
            AND t901a.c901_access_code_id = t906.c906_rule_id
            AND t901a.c901_code_id = p_ordertype_id  
            and t901a.C1900_company_id=p_company_id
            AND T906.C906_VOID_FL IS NULL  
          ORDER BY T906.C906_RULE_ID;                                                                      
	END gm_fch_inserts_txn_id;


END GM_PKG_CM_RULE_PARAMS;
/
