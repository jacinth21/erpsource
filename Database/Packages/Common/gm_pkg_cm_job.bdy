/* Formatted on 2008/09/26 09:51 (Formatter Plus v4.8.0) */
-- @"c:\database\packages\common\gm_pkg_cm_job.bdy"

CREATE OR REPLACE PACKAGE BODY gm_pkg_cm_job
IS
--
	PROCEDURE gm_cm_sav_job (
		p_jobid 		   IN		t9300_job.c9300_job_id%TYPE
	  , p_jobname		   IN		t9300_job.c9300_job_nm%TYPE
	  , p_refid 		   IN		t9301_job_detail.c9301_ref_id%TYPE
	  , p_reftype		   IN		t9301_job_detail.c901_ref_type%TYPE
	  , p_jobscheduleid    IN		t9301_job_detail.c901_load_type%TYPE
	  , p_firstoccurence   IN		VARCHAR2
	  , p_userid		   IN		t9300_job.c9300_created_by%TYPE
	  , p_what			   IN		t9300_job.c9300_what%TYPE
	  , p_inactiveflag	   IN		t9300_job.c9300_inactive_fl%TYPE
	  , p_outjobid		   OUT		t9300_job.c9300_job_id%TYPE
	)
	AS
	v_dateformat varchar2(100);
		--
	/*******************************************************
	  * Description : Procedure to save job info
	  * Author		: Joe P Kumar
	  *******************************************************/
	BEGIN
		p_outjobid	:= p_jobid;
        select get_compdtfmt_frm_cntx() into v_dateformat from dual;
		UPDATE t9300_job t9300
		   SET t9300.c9300_start_date = TO_DATE (p_firstoccurence, v_dateformat)
			 , t9300.c9300_interval = get_code_name_alt (p_jobscheduleid)
			 , t9300.c9300_last_updated_by = p_userid
			 , t9300.c9300_last_updated_date = CURRENT_DATE
			 , t9300.c9300_inactive_fl = p_inactiveflag
		 WHERE t9300.c9300_job_id = p_jobid;

		IF (SQL%ROWCOUNT = 0)
		THEN
			SELECT s9300_job.NEXTVAL
			  INTO p_outjobid
			  FROM DUAL;

			INSERT INTO t9300_job
						(c9300_job_id, c9300_job_nm, c9300_what, c9300_start_date
					   , c9300_interval, c9300_next_date, c9300_created_by
					   , c9300_created_date, c9300_inactive_fl
						)
				 VALUES (p_outjobid, p_jobname, p_what, TO_DATE (p_firstoccurence, v_dateformat)
					   , get_code_name_alt (p_jobscheduleid), TO_DATE (p_firstoccurence, v_dateformat), p_userid
					   , CURRENT_DATE, p_inactiveflag
						);
		END IF;

		UPDATE t9301_job_detail t9301
		   SET t9301.c901_load_type = p_jobscheduleid
		 WHERE t9301.c9300_job_id = p_outjobid;

		IF (SQL%ROWCOUNT = 0)
		THEN
			INSERT INTO t9301_job_detail
						(c9301_job_detail_id, c9300_job_id, c901_ref_type, c9301_ref_id, c901_load_type
						)
				 VALUES (s9301_job_detail.NEXTVAL, p_outjobid, p_reftype, p_refid, p_jobscheduleid
						);
		END IF;

		gm_pkg_cm_job.gm_cm_sav_job_trigger (p_outjobid, TO_DATE (p_firstoccurence, v_dateformat), 90775, 90785);
	END gm_cm_sav_job;

	 /*******************************************************
   * Description : Procedure to fetch job details
   * Author 	 : Joe P Kumar
   *******************************************************/
--
	PROCEDURE gm_cm_fch_job (
		p_refid 	IN		 t9301_job_detail.c9301_ref_id%TYPE
	  , p_reftype	IN		 t9301_job_detail.c901_ref_type%TYPE
	  , p_output	OUT 	 TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_output
		 FOR
			 SELECT t9301.c9301_ref_id refid, t9301.c901_ref_type reftype
				  , get_code_name (t9301.c901_ref_type) reftypetext, t9300.c9300_job_id jobid
				  , t9300.c9300_job_nm jobname, t9301.c901_load_type jobscheduleid
				  , t9300.c9300_start_date firstoccurence
				  , t9300.c9300_start_date firstoccurence
				  , DECODE (t9300.c9300_inactive_fl, 'Y', 'on', 'off') activeflag
				  , gm_pkg_cm_job.get_rerun_status (p_refid, p_reftype) refstatus
			   FROM t9300_job t9300, t9301_job_detail t9301
			  WHERE t9300.c9300_job_id = t9301.c9300_job_id
				AND t9301.c9301_ref_id = p_refid
				AND t9301.c901_ref_type = p_reftype
				AND t9300.c9300_void_fl IS NULL;
	END gm_cm_fch_job;

	 /*******************************************************
   * Description : Procedure to save job trigger
   * Author 	 : Joe P Kumar
   *******************************************************/
--
	PROCEDURE gm_cm_sav_job_trigger (
		p_jobid 		  IN   t9300_job.c9300_job_id%TYPE
	  , p_next_run_date   IN   t9310_trigger.c9310_next_run%TYPE
	  , p_state 		  IN   t9310_trigger.c901_state%TYPE
	  , p_type			  IN   t9310_trigger.c901_type%TYPE
	)
	AS
	BEGIN
		UPDATE t9310_trigger
		   SET c9310_next_run = NVL (p_next_run_date, TRUNC (CURRENT_DATE))
		 WHERE c9300_job_id = p_jobid AND c901_state = p_state AND c901_type = p_type;

		IF (SQL%ROWCOUNT = 0)
		THEN
			INSERT INTO t9310_trigger
						(c9310_trigger_id, c9300_job_id, c9310_next_run, c901_state, c901_type
						)
				 VALUES (s9310_trigger.NEXTVAL, p_jobid, NVL (p_next_run_date, TRUNC (CURRENT_DATE)), p_state, p_type
						);
		END IF;
	END gm_cm_sav_job_trigger;

/*******************************************************
 * Purpose: Facade / Entry point function to fetch the status of ref for rerun functionality
 *******************************************************/
--
	FUNCTION get_rerun_status (
		p_refid 	IN	 t9301_job_detail.c9301_ref_id%TYPE
	  , p_reftype	IN	 t9301_job_detail.c901_ref_type%TYPE
	)
		RETURN VARCHAR2
	IS
		v_rerun_status VARCHAR2 (5);
	BEGIN
		IF p_reftype = 90720
		THEN
			v_rerun_status := gm_pkg_cm_job.get_curr_month_ds_status (p_refid);
		END IF;

		RETURN v_rerun_status;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN 'N';
	END get_rerun_status;

 /*******************************************************
 * Purpose: function is used to fetch the status of demand sheet id for the current month
 *******************************************************/
--
	FUNCTION get_curr_month_ds_status (
		p_demandmasterid   IN	t4020_demand_master.c4020_demand_master_id%TYPE
	)
		RETURN VARCHAR2
	IS
		v_status	   t4040_demand_sheet.c901_status%TYPE;
		v_retn_status  VARCHAR2 (5);
	BEGIN
		SELECT c901_status
		  INTO v_status
		  FROM t4040_demand_sheet
		 WHERE c4020_demand_master_id = p_demandmasterid AND c4040_void_fl IS NULL;

		IF v_status = 50551
		THEN
			v_retn_status := 'Y';
		ELSE
			v_retn_status := 'N';
		END IF;

		RETURN v_retn_status;
	EXCEPTION
		-- Change others to fix Multiple fetch issue 
		WHEN OTHERS
		THEN
			RETURN 'N';
	END get_curr_month_ds_status;

	--
		/***********************************************************************
		* Procedure to email sucess/failure information
		***********************************************************************/
	PROCEDURE gm_cm_notify_load_info (
		p_start_dt	 DATE
	  , p_end_dt	 DATE
	  , p_type		 CHAR
	  , p_message	 VARCHAR2
	  , p_pkg_name	 VARCHAR2
	)
	AS
		--
		subject 	   VARCHAR2 (100) := 'INVENTORY LOAD SUCCESS ';
		mail_body	   VARCHAR2 (3000);
		crlf		   VARCHAR2 (2) := CHR (13) || CHR (10);
		to_mail 	   t906_rules.c906_rule_value%TYPE;
		v_db_name	   VARCHAR2 (100);
	--
	BEGIN
		SELECT SYS_CONTEXT ('USERENV', 'CURRENT_SCHEMA') || ' :'
		  INTO v_db_name
		  FROM DUAL;

		IF (v_db_name = 'GLOBUS_APP :')
		THEN
			v_db_name	:= '';
		END IF;

		IF (p_type = 'E')
		THEN
			--
			subject 	:= v_db_name || '	******** ERROR IN LOAD ' || p_pkg_name;
		ELSIF (p_type = 'S')
		THEN
			--
			subject 	:= 'FROM ' || v_db_name || ' : ' || p_pkg_name || ' LOAD SUCESS';
		--
		END IF;

		-- Final log insert statement
		INSERT INTO t910_load_log_table
					(c910_load_log_id, c910_log_name, c910_state_dt, c910_end_dt, c910_status_details, c910_status_fl
					)
			 VALUES (s910_load_log.NEXTVAL, p_pkg_name, p_start_dt, p_end_dt, p_message, p_type
					);

		--
		mail_body	:=
			   ' LOAD STATUS'
			|| crlf
			|| ' STATUS	 :- '
			|| p_type
			|| crlf
			|| ' START Time	:- '
			|| TO_CHAR (p_start_dt, 'dd/mm/yyyy hh24:mi:ss')
			|| crlf
			|| ' END Time :- '
			|| TO_CHAR (p_end_dt, 'dd/mm/yyyy hh24:mi:ss')
			|| crlf
			|| ' MESSAGE '
			|| p_message;
		--
		to_mail 	:= get_rule_value ('LOADEMAIL', 'EMAIL');
		gm_com_send_email_prc (to_mail, subject, mail_body);
	--
	END gm_cm_notify_load_info;
  
/***********************************************************************
* This procedure is called to send email for set load notification
***********************************************************************/
PROCEDURE gm_cm_notify_set_load_info
AS
    subject VARCHAR2 (200) ;
    to_mail VARCHAR2 (500) ;
    to_priusrmail t101_user.c101_email_id%TYPE;
    mail_body  CLOB ;
    crlf       VARCHAR2 (2) := CHR (13) || CHR (10) ;
    v_str      VARCHAR2(3300) ;
    v_temp     VARCHAR2(3300) ;
    v_src_cnid VARCHAR2 (30) ;
    v_tgt_cnid VARCHAR2 (30) ;
    v_setid    VARCHAR2 (30) ;
    v_setnm    t207_set_master.c207_set_nm%type; --PC-4226 Set name length change for ICT consignment load
    v_tagid    VARCHAR2 (20) ;
    v_cnt     NUMBER :=0;
    v_footer  VARCHAR2(160) ;
	v_company_id	t1900_company.c1900_company_id%TYPE;
	v_company_cd	t1900_company.c1900_company_cd%TYPE;
	
    CURSOR cur_sets
    IS
         SELECT NVL(C8902_OUS_REF_ID,C504_CONSIGN_ID_SRC) src_cn, C504_CONSIGN_ID_TGT  tgt_cn, C207_CONSIGN_SET_ID  setid
          , get_set_name (C207_CONSIGN_SET_ID ) setnm
           FROM TE504a_Consignment_LD_Status
          WHERE CE504A_LD_COMPLETE_FL        = 'Y'
            AND CE504A_EMAIL_NOTIF_FL   IS NULL
            AND C207_CONSIGN_SET_ID IS NOT NULL
			AND C1900_COMPANY_ID = v_company_id
            ORDER BY C504_CONSIGN_ID_SRC, C207_CONSIGN_SET_ID;
BEGIN

	SELECT 	get_compid_frm_cntx() INTO v_company_id  FROM 	DUAL;
	SELECT  get_company_code(v_company_id) INTO v_company_cd FROM DUAL;
		
    FOR var_set_detail IN cur_sets
    LOOP
    	v_cnt      := v_cnt+1;
        v_src_cnid := var_set_detail.src_cn;
        v_tgt_cnid := var_set_detail.tgt_cn;
        v_setid    := var_set_detail.setid;
        v_setnm    := var_set_detail.setnm;
     
        BEGIN
             SELECT C5010_TAG_ID
               INTO v_tagid
               FROM t5010_tag
              WHERE c5010_last_updated_trans_id = v_tgt_cnid
                AND C5010_VOID_FL              IS NULL
                AND rownum                      = 1 ;
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_tagid := '';
        END;
        BEGIN
        	v_temp := v_src_cnid ||'   |      ' || v_tgt_cnid ||'   |      '||NVL(v_tagid,'                ')||'   |      '|| v_setid|| '   |      '||v_setnm|| crlf;
        	 v_str     := v_str || v_temp;
        EXCEPTION WHEN OTHERS
        THEN
        	v_footer := 'There are more Set(s) available in the data load. For complete list, pls login to the Portal....';
        	NULL;
        END;
    END LOOP;

    IF (v_cnt>0) THEN
	    subject   := get_rule_value ('SET_LOAD_NOTIF_SUB', 'DATA_LOAD_NOTIF')||'-'||v_company_cd ;
	    mail_body := 'Please find the details of the Set(s) that is Loaded :'|| crlf || crlf ;
       
       
	    mail_body :=mail_body|| '   SOURCE CN#      |      New CN#      |   TAG ID      |   SET ID      |   SET NAME      ' || crlf || crlf || v_str ||crlf||v_footer;
	    to_mail   := get_rule_value_by_company ('SET_LOAD_NOTIF_TO', 'DATA_LOAD_NOTIF',v_company_id) ;
	    gm_com_send_email_prc (to_mail, subject, mail_body) ;
	    --Update the EMAIL_NOTIFICATION flag to denote the email has been sent.
	    UPDATE TE504a_Consignment_LD_Status
	    SET CE504A_EMAIL_NOTIF_FL        = 'Y'
	      WHERE CE504A_LD_COMPLETE_FL       = 'Y'
	        AND CE504A_EMAIL_NOTIF_FL   IS NULL
	        AND C207_CONSIGN_SET_ID IS NOT NULL
			AND C1900_COMPANY_ID = v_company_id;
	END IF;
END gm_cm_notify_set_load_info;

/***********************************************************************
* This procedure is called to send email for item load notification
***********************************************************************/
PROCEDURE gm_cm_notify_item_load_info
AS
    subject   VARCHAR2 (200) ;
    to_mail   VARCHAR2 (500) ;
    mail_body VARCHAR2(4000) ;
    crlf      VARCHAR2 (2) := CHR (13) || CHR (10) ;
    v_str     VARCHAR2(3300) ;
    v_temp    VARCHAR2(3300) ;
    v_us_cnid VARCHAR2 (30) ;
    v_txn     VARCHAR2 (30) ;
    v_pnum    VARCHAR2 (30) ;
    v_qty     NUMBER ;
    v_cnm     VARCHAR2 (20) ;
    v_cnt     NUMBER:=0;
    v_footer  VARCHAR2(160) ;
	v_company_id	t1900_company.c1900_company_id%TYPE;
	v_company_cd	t1900_company.c1900_company_cd%TYPE;
	
    CURSOR cur_items
    IS
         SELECT NVL(etl_st.C8902_OUS_REF_ID,etl_st.C504_CONSIGN_ID_SRC) USCN, T413.C412_INHOUSE_TRANS_ID TRANS_ID, t413.C205_PART_NUMBER_ID pnum
          , T413.C413_ITEM_QTY Qty, T413.C413_CONTROL_NUMBER cnum
           FROM TE504a_Consignment_LD_Status etl_st, t408_dhr t408, t413_inhouse_trans_items t413
          , t412_inhouse_transactions t412
          WHERE etl_st.C207_CONSIGN_SET_ID   IS NULL
            AND etl_st.CE504A_LD_COMPLETE_FL         = 'Y'
            AND etl_st.CE504A_EMAIL_NOTIF_FL     IS NULL
            AND t408.c408_comments         = etl_st.C504_CONSIGN_ID_SRC -- COMMENTS COLUMN WILL HAVE THE US CN ID.
            AND t408.c408_dhr_id           = t412.c412_ref_id -- DHR ID WILL BE IN THE REF ID COLUMN
            AND t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
            AND t408.c408_void_fl         IS NULL
            AND t412.c412_void_fl         IS NULL
            AND t413.c413_void_fl         IS NULL
			AND etl_st.C1900_COMPANY_ID = v_company_id
       ORDER BY etl_st.C504_CONSIGN_ID_SRC, t413.C205_PART_NUMBER_ID;
BEGIN
	SELECT 	get_compid_frm_cntx() INTO v_company_id  FROM 	DUAL;
	SELECT  get_company_code(v_company_id) INTO v_company_cd FROM DUAL;

    FOR var_item_detail IN cur_items
    LOOP
    	v_cnt 	  := v_cnt+1;
        v_us_cnid := var_item_detail.uscn;
        v_txn     := var_item_detail.trans_id;
        v_pnum    := var_item_detail.pnum;
        v_qty     := var_item_detail.qty;
        v_cnm     := var_item_detail.cnum;

        BEGIN
	        v_temp :=  v_us_cnid ||'   |      ' || v_txn ||'   |      '||v_pnum||'   |      '|| v_qty|| '   |      '||v_cnm|| crlf;
	        v_str     := v_str || v_temp; -- As THere is a limitation in the email component, we cannot send more than 4000 chars in email.
	    EXCEPTION
	    WHEN OTHERS
	    THEN
	    	v_footer := 'There are more part(s) available in the data load. For complete list, pls login to the Portal....';
	    	NULL;
	    END;
    END LOOP;

    IF (v_cnt>0)
    THEN
	    subject   := get_rule_value ('ITEM_LOAD_NOTIF_SUB', 'DATA_LOAD_NOTIF')||'-'||v_company_cd ;
	    mail_body := 'Please find the details of the DHFG transactions created in Pending Verification status displayed on the Inhouse Transactions dashboard.'|| crlf || crlf ;

	    mail_body :=mail_body|| '   SOURCE CN#      |   TXN ID      |   PART NUMBER      |   QTY      |   CONTROL NUMBER      ' || crlf || crlf || v_str ||crlf ||v_footer;
	    to_mail   := get_rule_value_by_company ('ITEM_LOAD_NOTIF_TO', 'DATA_LOAD_NOTIF',v_company_id) ;
	    gm_com_send_email_prc (to_mail, subject, mail_body) ;
	    --Update the EMAIL_NOTIFICATION flag to denote the email has been sent.
	     UPDATE TE504a_Consignment_LD_Status
	    SET CE504A_EMAIL_NOTIF_FL        = 'Y'
	      WHERE CE504A_LD_COMPLETE_FL       = 'Y'
	        AND CE504A_EMAIL_NOTIF_FL   IS NULL
	        AND C207_CONSIGN_SET_ID IS NULL
			AND C1900_COMPANY_ID = v_company_id;
	END IF;
END gm_cm_notify_item_load_info;  
END gm_pkg_cm_job;
/
