--@"C:\database\Packages\Common\gm_pkg_cm_view_refresh.bdy";
CREATE OR REPLACE PACKAGE BODY gm_pkg_cm_view_refresh
IS
    /*******************************************************
    * Description : Procedure used to refresh view
    * Author  : mmuthusamy
    *******************************************************/
PROCEDURE gm_refresh_materialize_view (
        p_view_name VARCHAR2)
AS
    v_cnt NUMBER;
BEGIN
    -- to validate the materialize view
     SELECT COUNT (1)
       INTO v_cnt
       FROM ALL_MVIEWS
      WHERE OWNER      = 'GLOBUS_APP'
        AND MVIEW_NAME = upper (p_view_name) ;
    --    
    IF v_cnt = 0
    THEN
    	raise_application_error ('-20999', 'Invalid Materialze view - Please provide the valid view '|| p_view_name);
    END IF;
    -- refresh the M View
    DBMS_MVIEW.REFRESH (p_view_name) ;
    
    /*
     *V703_REP_MAPPING_DETAIL materialized view created from V700_TERRITORY_MAPPING_DETAIL materialized view.
     *When v700 refresh v703 needs to refresh automatically.
     *V703 used in CRM module.
     */
    IF UPPER(p_view_name ) = UPPER('V700_TERRITORY_MAPPING_DETAIL') THEN
    	gm_refresh_materialize_view('V703_REP_MAPPING_DETAIL');
    END IF;
      
    --
END gm_refresh_materialize_view;
END gm_pkg_cm_view_refresh;
/
