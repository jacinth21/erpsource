-- @"C:\Data Correction\Script\gm_pkg_cm_email_tracking_response_load.bdy";
CREATE OR REPLACE PACKAGE BODY gm_pkg_cm_email_tracking_response_load
IS
	/********************************************************************************
	* Description : Procedure used to save email master details (after send via sendGrid)
	*********************************************************************************/
PROCEDURE gm_sav_email_sent_master (
        p_txn_id             IN T9161_EMAIL_SENT_MASTER.C9161_TRANSACTION_ID%TYPE,
        p_msg_id             IN T9161_EMAIL_SENT_MASTER.C9161_MESSAGE_ID%TYPE,
        p_to_email_ids       IN T9161_EMAIL_SENT_MASTER.C9161_TO_EMAIL_IDS%TYPE,
        p_cc_email_ids       IN T9161_EMAIL_SENT_MASTER.C9161_CC_EMAIL_IDS%TYPE,
        p_subject            IN T9161_EMAIL_SENT_MASTER.C9161_EMAIL_SUBJECT%TYPE,
        p_email_body         IN T9161_EMAIL_SENT_MASTER.C9161_EMAIL_BODY%TYPE,
        p_sent_date          IN T9161_EMAIL_SENT_MASTER.C9161_EMAIL_SENT_DATE%TYPE,
        p_sent_by            IN T9161_EMAIL_SENT_MASTER.C9161_EMAIL_SENT_BY%TYPE,
        p_email_notification IN T9161_EMAIL_SENT_MASTER.C9161_EMAIL_NOTIFICATION_FL%TYPE,
        p_sales_rep_id       IN T9161_EMAIL_SENT_MASTER.C703_SALES_REP_ID%TYPE,
        p_dist_id            IN T9161_EMAIL_SENT_MASTER.C701_DISTRIBUTOR_ID%TYPE,
        p_vp_id              IN T9161_EMAIL_SENT_MASTER.C101_VP_ID%TYPE,
        p_ad_id              IN T9161_EMAIL_SENT_MASTER.C101_AD_ID%TYPE,
        p_user_id            IN T9161_EMAIL_SENT_MASTER.C9161_LAST_UPDATED_BY%TYPE)
AS
BEGIN
     INSERT
       INTO T9161_EMAIL_SENT_MASTER
        (
            C9160_EMAIL_TRACKING_MASTER_ID, C9161_TRANSACTION_ID, C9161_MESSAGE_ID
          , C9161_TO_EMAIL_IDS, C9161_CC_EMAIL_IDS, C9161_EMAIL_SUBJECT
          , C9161_EMAIL_BODY, C9161_EMAIL_SENT_DATE, C9161_EMAIL_SENT_BY
          , C9161_EMAIL_NOTIFICATION_FL, C703_SALES_REP_ID, C701_DISTRIBUTOR_ID
          , C101_VP_ID, C101_AD_ID, C9161_LAST_UPDATED_BY
          , C9161_LAST_UPDATED_DATE
        )
        VALUES
        (
            1, p_txn_id, p_msg_id
          , p_to_email_ids, p_cc_email_ids, p_subject
          , p_email_body, p_sent_date, p_sent_by
          , p_email_notification, p_sales_rep_id, p_dist_id
          , p_vp_id, p_ad_id, p_user_id
          , CURRENT_DATE
        ) ;
END gm_sav_email_sent_master;

	/********************************************************************************
	* Description : Procedure used to save webhook response details 
	*********************************************************************************/
	
PROCEDURE gm_sav_response_log_dtls
    (
        p_response_json_dtls IN T9162_EMAIL_RESPONSE_LOG_DTLS.C9162_RESPONSE_JSON_DTLS%TYPE,
        p_user_id            IN T9162_EMAIL_RESPONSE_LOG_DTLS.C9162_LAST_UPDATED_BY%TYPE
    )
AS
BEGIN
     INSERT
       INTO T9162_EMAIL_RESPONSE_LOG_DTLS
        (
            C9162_RESPONSE_JSON_DTLS, C9162_LAST_UPDATED_BY, C9162_PROCESS_FL
            , C9162_LAST_UPDATED_DATE
        )
        VALUES
        (
            p_response_json_dtls, p_user_id, 'N'
            , CURRENT_DATE
        ) ;
END gm_sav_response_log_dtls;

	/********************************************************************************
	* Description : Procedure used to save recipent details 
	*********************************************************************************/
	
PROCEDURE gm_sav_email_sent_dtls
    (
        p_email_master_id  IN T9163_EMAIL_SENT_DTLS.C9161_EMAIL_SENT_MASTER_ID%TYPE,
        p_email_id         IN T9163_EMAIL_SENT_DTLS.C9163_EMAIL_ID%TYPE,
        p_msg_id           IN T9163_EMAIL_SENT_DTLS.C9163_MESSAGE_ID%TYPE,
        p_party_id         IN T9163_EMAIL_SENT_DTLS.C101_PARTY_ID%TYPE,
        p_open_cnt         IN T9163_EMAIL_SENT_DTLS.C9163_OPEN_COUNT%TYPE,
        p_click_cnt        IN T9163_EMAIL_SENT_DTLS.C9163_CLICKED_COUNT%TYPE,
        p_first_open_date  IN T9163_EMAIL_SENT_DTLS.C9163_FIRST_OPEN_DATE%TYPE,
        p_first_click_date IN T9163_EMAIL_SENT_DTLS.C9163_FIRST_CLICKED_DATE%TYPE,
        p_user_id          IN T9163_EMAIL_SENT_DTLS.C9163_LAST_UPDATED_BY%TYPE
    )
AS
BEGIN
     INSERT
       INTO T9163_EMAIL_SENT_DTLS
        (
            C9161_EMAIL_SENT_MASTER_ID, C9163_EMAIL_ID, C9163_MESSAGE_ID
          , C9163_OPEN_COUNT, C9163_CLICKED_COUNT, C9163_FIRST_OPEN_DATE
          , C9163_FIRST_CLICKED_DATE, C9163_LAST_UPDATED_BY, C9163_LAST_UPDATED_DATE
          , C101_PARTY_ID
        )
        VALUES
        (
            p_email_master_id, p_email_id, p_msg_id
          , p_open_cnt, p_click_cnt, p_first_open_date
          , p_first_click_date, p_user_id, CURRENT_DATE
          , p_party_id
        ) ;
END gm_sav_email_sent_dtls;

	/********************************************************************************
	* Description : Procedure used to save event tracker details (open, click, delivered)
	*********************************************************************************/
	
PROCEDURE gm_sav_email_event_track
    (
        p_email_sent_dtls_id IN T9164_EMAIL_EVENT_TRACKER.C9163_EMAIL_SENT_DTLS_ID%TYPE,
        p_event_type         IN T9164_EMAIL_EVENT_TRACKER.C901_EVENT_TYPE%TYPE,
        p_event_name         IN T9164_EMAIL_EVENT_TRACKER.C9164_EVENT_TYPE_NAME%TYPE,
        p_response_dtls      IN T9164_EMAIL_EVENT_TRACKER.C9164_RESPONSE_JSON_DTLS%TYPE,
        p_response_status    IN T9164_EMAIL_EVENT_TRACKER.C9164_RESPONSE_STATUS%TYPE,
        p_time_stamp         IN T9164_EMAIL_EVENT_TRACKER.C9164_TIME_STAMP%TYPE,
        p_url                IN T9164_EMAIL_EVENT_TRACKER.C9165_URL%TYPE,
        p_user_id            IN T9164_EMAIL_EVENT_TRACKER.C9164_LAST_UPDATED_BY%TYPE
    )
AS
BEGIN
     INSERT
       INTO T9164_EMAIL_EVENT_TRACKER
        (
            C9163_EMAIL_SENT_DTLS_ID, C901_EVENT_TYPE, C9164_EVENT_TYPE_NAME
          , C9164_RESPONSE_JSON_DTLS, C9164_RESPONSE_STATUS, C9164_TIME_STAMP
          , C9165_URL, C9164_LAST_UPDATED_BY, C9164_LAST_UPDATED_DATE
        )
        VALUES
        (
            p_email_sent_dtls_id, p_event_type, p_event_name
          , p_response_dtls, p_response_status, p_time_stamp
          , p_url, p_user_id, CURRENT_DATE
        ) ;
END gm_sav_email_event_track;

/********************************************************************************
* Description : Procedure used to save email master details (after send via sendGrid)
*********************************************************************************/
PROCEDURE gm_sav_email_track_dtls (
        p_txn_id       IN T9161_EMAIL_SENT_MASTER.C9161_TRANSACTION_ID%TYPE,
        p_msg_id       IN T9161_EMAIL_SENT_MASTER.C9161_MESSAGE_ID%TYPE,
        p_to_email_ids IN T9161_EMAIL_SENT_MASTER.C9161_TO_EMAIL_IDS%TYPE,
        p_cc_email_ids IN T9161_EMAIL_SENT_MASTER.C9161_CC_EMAIL_IDS%TYPE,
        p_subject      IN T9161_EMAIL_SENT_MASTER.C9161_EMAIL_SUBJECT%TYPE,
        p_email_body   IN T9161_EMAIL_SENT_MASTER.C9161_EMAIL_BODY%TYPE,
        p_sent_by            IN T9161_EMAIL_SENT_MASTER.C9161_EMAIL_SENT_BY%TYPE,
        p_email_notification IN T9161_EMAIL_SENT_MASTER.C9161_EMAIL_NOTIFICATION_FL%TYPE,
        p_user_id            IN T9161_EMAIL_SENT_MASTER.C9161_LAST_UPDATED_BY%TYPE)
AS
    v_sales_rep_id T9161_EMAIL_SENT_MASTER.C703_SALES_REP_ID%TYPE;
    v_dist_id T9161_EMAIL_SENT_MASTER.C701_DISTRIBUTOR_ID%TYPE;
    v_vp_id T9161_EMAIL_SENT_MASTER.C101_VP_ID%TYPE;
    v_ad_id T9161_EMAIL_SENT_MASTER.C101_AD_ID%TYPE;
    --
    v_access_lvl NUMBER;
    --
    v_share_user_id NUMBER;
BEGIN
    -- based on user to get the Rep/FS/AD and VP details.
    
	-- 0 to get the shared user id from T1016
	
	SELECT c1016_created_by INTO v_share_user_id FROM t1016_share
	WHERE c1016_share_id = p_txn_id;
	
    -- 1. based on user to get the access level
     SELECT c101_access_level_id
       INTO v_access_lvl
       FROM t101_user
      WHERE c101_user_id = v_share_user_id ;
      
    -- access level - 1 (rep), 2 (dist), 3 (ad), 4 (vp)
    IF v_access_lvl = 1 THEN
        BEGIN
            SELECT DISTINCT rep_id, d_id, ad_id
              , vp_id
               INTO v_sales_rep_id, v_dist_id, v_ad_id
              , v_vp_id
               FROM v700_territory_mapping_detail
              WHERE rep_id = v_share_user_id;
        EXCEPTION
        WHEN OTHERS THEN
            v_sales_rep_id := NULL;
            v_dist_id      := NULL;
            v_vp_id        := NULL;
            v_ad_id        := NULL;
        END;
        
    ELSIF v_access_lvl = 2 THEN
        BEGIN
            SELECT DISTINCT NULL, d_id, ad_id
              , VP_ID
               INTO v_sales_rep_id, v_dist_id, v_ad_id
              , v_vp_id
               FROM v700_territory_mapping_detail
              WHERE d_id = v_share_user_id;
        EXCEPTION
        WHEN OTHERS THEN
            v_sales_rep_id := NULL;
            v_dist_id      := NULL;
            v_vp_id        := NULL;
            v_ad_id        := NULL;
        END;
        
    ELSIF v_access_lvl = 3 THEN
        BEGIN
            SELECT DISTINCT NULL, NULL, ad_id
              , vp_id
               INTO v_sales_rep_id, v_dist_id, v_ad_id
              , v_vp_id
               FROM v700_territory_mapping_detail
              WHERE ad_id = v_share_user_id;
        EXCEPTION
        WHEN OTHERS THEN
            v_sales_rep_id := NULL;
            v_dist_id      := NULL;
            v_vp_id        := NULL;
            v_ad_id        := NULL;
        END;
        
    ELSE
        BEGIN
            SELECT DISTINCT NULL, NULL, NULL
              , vp_id
               INTO v_sales_rep_id, v_dist_id, v_ad_id
              , v_vp_id
               FROM v700_territory_mapping_detail
              WHERE vp_id = v_share_user_id;
        EXCEPTION
        WHEN OTHERS THEN
            v_sales_rep_id := NULL;
            v_dist_id      := NULL;
            v_vp_id        := NULL;
            v_ad_id        := NULL;
        END;
    END IF;
    
    --
    
    gm_pkg_cm_email_tracking_response_load.gm_sav_email_sent_master (p_txn_id, p_msg_id, p_to_email_ids, p_cc_email_ids
    , p_subject, p_email_body, CURRENT_DATE, v_share_user_id, p_email_notification, v_sales_rep_id, v_dist_id, v_ad_id,
    v_vp_id, p_user_id) ;
    
END gm_sav_email_track_dtls;

/********************************************************************************
* Description : Procedure used to split JSON data and move to email details and event table
*********************************************************************************/
PROCEDURE gm_process_email_main_response
AS
    v_user_id NUMBER := 30301; -- Manual load
    --
    v_item_array json_array_t;
    v_json_object json_object_t;
    --
    CURSOR json_dtls
    IS
         SELECT c9162_response_json_dtls id
           FROM t9162_email_response_log_dtls
          WHERE c9162_process_fl = 'P';
BEGIN
    -- to update the process flag
    gm_upd_response_log_process_fl ('N', 'P', v_user_id) ;
    
    -- to assign the json object
    FOR json_data_check IN json_dtls
    LOOP
        v_item_array := json_array_t.parse (json_data_check.id) ;
        DBMS_OUTPUT.put_line ('to get the size '|| v_item_array.get_size) ;
        
        -- loop the json object
        FOR indx IN 0 .. v_item_array.get_size - 1
        LOOP
            DBMS_OUTPUT.put_line ('Index: ' || indx) ;
            v_json_object := TREAT (v_item_array.get (indx) AS json_object_t) ;
            
            --1 to process email dtls
            
            gm_process_email_dtls (v_json_object.to_clob, v_user_id) ;

        END LOOP;
        --
        DBMS_OUTPUT.put_line ('end the inner loop ') ;
        
    END LOOP;
    -- to update the process flag
    gm_upd_response_log_process_fl ('P', 'Y', v_user_id) ;
    --
END gm_process_email_main_response;

/********************************************************************************
* Description : Procedure used to process webhook response data to split and store different table 
*********************************************************************************/

PROCEDURE gm_process_email_dtls (
        p_json_dtls IN t9164_email_event_tracker.c9164_response_json_dtls%TYPE,
        p_user_id   IN t9161_email_sent_master.c9161_last_updated_by%TYPE)
AS
    --
    v_event_code_id NUMBER;
    v_msg_cnt       NUMBER;
    v_tmp_message_id t9163_email_sent_dtls.c9163_message_id%TYPE;
    --
    v_email_sent_master_id t9161_email_sent_master.c9161_email_sent_master_id%TYPE;
    --
    v_tmp_email_sent_dtls_id t9163_email_sent_dtls.c9163_email_sent_dtls_id%TYPE;
    --
    v_event_cnt NUMBER;
    --
    v_json_dtls json_object_t := json_object_t.parse (p_json_dtls) ;
    --
    v_party_id NUMBER;
    --
    v_event_name VARCHAR2 (40);
    v_msg_id t9163_email_sent_dtls.c9163_message_id%type;
    v_email_id t107_contact.c107_contact_value%type;
    v_time_stamp_val VARCHAR2 (40);
    v_url VARCHAR2 (400);
BEGIN
    --
    DBMS_OUTPUT.put_line ('email ' || v_json_dtls.get_string ('email')) ;
    DBMS_OUTPUT.put_line ('event ' || v_json_dtls.get_string ('event')) ;
    DBMS_OUTPUT.put_line ('sg_message_id ' || v_json_dtls.get_string ('sg_message_id')) ;
    --
    DBMS_OUTPUT.put_line ('timestamp ' || v_json_dtls.get_string ('timestamp')) ;
    DBMS_OUTPUT.put_line ('response ' || v_json_dtls.get_string ('response')) ;
    DBMS_OUTPUT.put_line ('reason ' || v_json_dtls.get_string ('reason')) ;
    DBMS_OUTPUT.put_line ('status ' || v_json_dtls.get_string ('status')) ;
    DBMS_OUTPUT.put_line ('type ' || v_json_dtls.get_string ('type')) ;
    --
    v_event_name := v_json_dtls.get_string ('event');
    v_msg_id := v_json_dtls.get_string ('sg_message_id');
    v_email_id := v_json_dtls.get_string ('email');
    v_time_stamp_val := v_json_dtls.get_string ('timestamp');
    v_url := v_json_dtls.get_string ('url');
    --
    DBMS_OUTPUT.put_line ('v_event_name' || v_event_name || ' v_msg_id '||  v_msg_id) ;
    -- to get the code id
    BEGIN
         SELECT c901_code_id
           INTO v_event_code_id
           FROM t901_code_lookup
          WHERE c901_code_grp    = 'EEVENT'
            AND c902_code_nm_alt = v_event_name;
    EXCEPTION
    WHEN OTHERS THEN
        v_event_code_id := NULL;
    END;
    
    --
    
    IF v_event_code_id IS NOT NULL THEN
        --1 to get the count (sg_message_id ) t9163
         SELECT COUNT (1)
           INTO v_msg_cnt
           FROM t9163_email_sent_dtls
          WHERE c9163_message_id = v_msg_id
          	AND c9163_void_fl IS NULL;
          --
          DBMS_OUTPUT.put_line ('v_msg_cnt ' || v_msg_cnt) ;
          --
        IF v_msg_cnt             = 0 THEN
            --2 to split the sg_message_id and get the pk - t9161
             SELECT SUBSTR (v_msg_id, 1, INSTR ( v_msg_id, '.') - 1)
               INTO v_tmp_message_id
               FROM dual;
               
               --
               DBMS_OUTPUT.put_line ('v_tmp_message_id ' || v_tmp_message_id) ;
            -- 3 to get the pk (t9161 - based on split msg id and email id
            BEGIN
             SELECT c9161_email_sent_master_id
               INTO v_email_sent_master_id
               FROM t9161_email_sent_master
              WHERE c9161_message_id = v_tmp_message_id;
             EXCEPTION WHEN OTHERS
    		THEN
    			-- master data not avaialble then, exit the process
    			RETURN;
    		END ; 
    		
              DBMS_OUTPUT.put_line ('v_email_sent_master_id ' || v_email_sent_master_id) ;
              
            -- 3.1 to get the party id - based on email id
            BEGIN
               SELECT c101_party_id INTO v_party_id
			   FROM t107_contact
			  WHERE c901_mode          = 90452 -- Email
			    AND c107_contact_value = v_email_id
			    AND c107_primary_fl    = 'Y'
			    AND c107_void_fl      IS NULL;
    		EXCEPTION WHEN OTHERS
    		THEN
    			v_party_id := NULL;
    		END ;
            --4 to call the t9163 (insert the data)
            
            gm_sav_email_sent_dtls (v_email_sent_master_id, v_email_id, v_msg_id,
            v_party_id, -- party id
            NULL, -- open cnt,
            NULL, -- click cnt,
            NULL, -- first open date
            NULL, -- first click date
            p_user_id) ;
            --
            
        END IF;
        
        -- based on sg msg id to get the t9163 - pk
         SELECT c9163_email_sent_dtls_id
           INTO v_tmp_email_sent_dtls_id
           FROM t9163_email_sent_dtls
          WHERE c9163_message_id = v_msg_id 
          	AND c9163_void_fl IS NULL;
          
          	DBMS_OUTPUT.put_line ('v_tmp_email_sent_dtls_id ' || v_tmp_email_sent_dtls_id || ' v_event_code_id ==> '|| v_event_code_id) ;
          	
        --insert data to t9164
        --110696 Clicked
        --110697 Opened
        
        IF v_event_code_id = 110696 OR v_event_code_id = 110697 THEN
             SELECT COUNT (1)
               INTO v_event_cnt
               FROM t9164_email_event_tracker
              WHERE c9163_email_sent_dtls_id = v_tmp_email_sent_dtls_id
                AND c901_event_type          = v_event_code_id
                AND c9164_void_fl           IS NULL;
                --
			DBMS_OUTPUT.put_line (' v_event_cnt ==> '|| v_event_cnt);
			
            IF v_event_cnt                   = 0 THEN
				--
				DBMS_OUTPUT.put_line (' Inside update first data ==> ');
				--
                 UPDATE t9163_email_sent_dtls
                SET c9163_first_open_date        = DECODE (v_event_code_id, 110697, CURRENT_DATE, c9163_first_open_date),
                    c9163_first_clicked_date     = DECODE (v_event_code_id, 110696, CURRENT_DATE, c9163_first_clicked_date),
                    c9163_last_updated_by        = p_user_id, C9163_LAST_UPDATED_DATE = CURRENT_DATE
                  WHERE c9163_email_sent_dtls_id = v_tmp_email_sent_dtls_id;
            END IF;
            --
        END IF;
        
        --
        
        gm_sav_email_event_track ( v_tmp_email_sent_dtls_id, v_event_code_id, v_event_name,
        v_json_dtls.to_clob, NULL, -- response status
        v_time_stamp_val, v_url, p_user_id) ;
        
        --
        
    END IF;
    --
    
END gm_process_email_dtls;

/********************************************************************************
* Description : Procedure used to update the process flag to (t9162) 
*********************************************************************************/

PROCEDURE gm_upd_response_log_process_fl (
        p_curr_proccess_fl IN VARCHAR2,
        p_new_process_fl   IN VARCHAR2,
        p_user_id          IN t9161_email_sent_master.c9161_last_updated_by%TYPE)
AS
BEGIN
    --
     UPDATE t9162_email_response_log_dtls
    SET c9162_process_fl     = p_new_process_fl, c9162_last_updated_by = p_user_id, c9162_last_updated_date = CURRENT_DATE
      WHERE c9162_process_fl = p_curr_proccess_fl;
      --
END gm_upd_response_log_process_fl;

END gm_pkg_cm_email_tracking_response_load;
/
