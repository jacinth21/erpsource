/* Formatted on 2010/06/07 11:23 (Formatter Plus v4.8.0) */
--@"c:\database\packages\common\gm_pkg_cm_query_info.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_cm_query_info
IS
--
	  /*******************************************************
			* Description : Procedure to fetch MA qty and cost
			* Author	 : Joe P Kumar
			* if TxnType is from scrap, then get the qty and cost from FG
	  *******************************************************/
	FUNCTION get_query_status (
		p_id	 IN   t6510_query.c901_status%TYPE
	  , p_type	 IN   t6510_query.c901_query_level_id%TYPE
	)
		RETURN NUMBER
	IS
		v_status	   NUMBER;
	BEGIN
		IF p_type = 92421 OR p_type = 92423
		THEN   -- FORM , Exam Date
			SELECT MIN (t6510.c901_status) c901_status
			  INTO v_status
			  FROM t6510_query t6510
			 WHERE t6510.c901_query_level_id = p_type
			   AND t6510.c6510_void_fl IS NULL
			   AND t6510.c622_patient_list_id = p_id;
		ELSIF p_type = 92422
		THEN   -- Questiom
			SELECT MIN (t6510.c901_status) c901_status
			  INTO v_status
			  FROM t6510_query t6510
			 WHERE t6510.c901_query_level_id = p_type
			   AND t6510.c6510_void_fl IS NULL
			   AND t6510.c623_patient_ans_list_id = p_id;
		END IF;

		RETURN v_status;
	END get_query_status;

	PROCEDURE gm_sav_void_sitemapping (
		p_txn_id   IN	t616_site_party_mapping.c616_site_party_id%TYPE
	  , p_userid   IN	t102_user_login.c102_user_login_id%TYPE
	)
	AS
	BEGIN
		UPDATE t616_site_party_mapping t616
		   SET t616.c616_void_fl = 'Y'
			 , t616.c616_last_updated_date = SYSDATE
			 , t616.c616_last_updated_by = p_userid
		 WHERE t616.c616_site_party_id = p_txn_id;
	END gm_sav_void_sitemapping;

	FUNCTION gm_fch_event_des (
		p_study_period_id	IN	 t622_patient_list.c613_study_period_id%TYPE
	  , p_form_id			IN	 t601_form_master.c601_form_id%TYPE
	  , p_patient_id		IN	 t621_patient_master.c621_patient_id%TYPE
	  , p_patient_list_id	IN	 t622_patient_list.c622_patient_list_id%TYPE
	)
		RETURN VARCHAR2
	IS
		v_event 	   VARCHAR2 (100);
	BEGIN
		BEGIN
			SELECT (t622.c622_patient_event_no || '-' || SUBSTR (t623.c904_answer_ds, 1, 20))
			  INTO v_event
			  FROM t622_patient_list t622, t623_patient_answer_list t623
			 WHERE t622.c621_patient_id = t623.c621_patient_id
			   AND t622.c622_patient_list_id = t623.c622_patient_list_id
			   AND t622.c613_study_period_id = p_study_period_id
			   AND t622.c601_form_id = p_form_id
			   AND t622.c621_patient_id = p_patient_id
			   AND t623.c603_answer_grp_id IN (477, 598)
			   AND t622.c622_patient_list_id = p_patient_list_id
			   AND NVL(t622.C622_DELETE_FL,'N')='N'
			   AND NVL(t623.C623_DELETE_FL,'N')='N';
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				v_event 	:= 'N/A';
		END;

		RETURN v_event;
	END gm_fch_event_des;

	FUNCTION get_display_text (
		p_id   IN	t6511_query_longdesc.c6511_query_longdesc_id%TYPE
	)
		RETURN VARCHAR2
	IS
		v_display_text VARCHAR2 (100);
	BEGIN
		BEGIN
			SELECT DECODE (t6511.c901_status
						 , 92441, 'Query Posted '
						 , 92442, 'Response Posted '
						 , 'Query ' || get_code_name (t6511.c901_status)
						  )
			  INTO v_display_text
			  FROM t6511_query_longdesc t6511
			 WHERE t6511.c6511_query_longdesc_id = p_id AND t6511.c6511_void_fl IS NULL;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				v_display_text := NULL;
		END;

		RETURN v_display_text;
	END get_display_text;

	/*******************************************************
	 * Description : Procedure to fetch MA qty and cost
	 * Author	: Joe P Kumar
	 *******************************************************/
	PROCEDURE gm_sav_query_details (
		p_query_id				IN OUT	 t6510_query.c6510_query_id%TYPE
	  , p_query_level_id		IN		 t6510_query.c901_query_level_id%TYPE
	  , p_patient_list_id		IN		 t622_patient_list.c622_patient_list_id%TYPE
	  , p_patient_ans_list_id	IN		 t623_patient_answer_list.c623_patient_ans_list_id%TYPE
	  , p_user_id				IN		 t102_user_login.c102_user_login_id%TYPE
	  , p_query_text			IN		 t6511_query_longdesc.c6511_query_text%TYPE
	  , p_status				IN		 t6510_query.c901_status%TYPE
	)
	AS
		v_reporter_access_level VARCHAR2 (20);
		v_user_access_level VARCHAR2 (20);
		v_patient_list_id NUMBER;
		v_study_site_id NUMBER;
		v_study_id	   VARCHAR2 (20);
		v_patient_id   NUMBER;
		v_form_id	   NUMBER;
		v_study_period_id NUMBER;
		v_account_id   NUMBER;
		v_id		   NUMBER;
		v_msg		   VARCHAR2 (1000);
		v_status	   NUMBER;
		v_void_fl	   t6510_query.c6510_void_fl%TYPE;
		v_number	   NUMBER;
		v_querydetails VARCHAR2 (4000);
	BEGIN
		IF p_status = 92444
		THEN   -- closed
			SELECT t101.c101_access_level_id
			  INTO v_reporter_access_level
			  FROM t101_user t101, t6510_query t6510
			 WHERE t6510.c101_reporter_id = t101.c101_user_id AND t6510.c6510_query_id = p_query_id;

			SELECT t101.c101_access_level_id
			  INTO v_user_access_level
			  FROM t101_user t101
			 WHERE t101.c101_user_id = p_user_id;
		END IF;

		IF v_user_access_level <> v_reporter_access_level
		THEN
			raise_application_error (-20760, '');
		END IF;

		IF p_query_id IS NULL AND p_status <> 92441
		THEN
			raise_application_error (-20761, '');
		END IF;

		IF p_query_id IS NOT NULL
		THEN
			SELECT c901_status, c6510_void_fl
			  INTO v_status, v_void_fl
			  FROM t6510_query t6510
			 WHERE t6510.c6510_query_id = p_query_id;

			IF v_void_fl IS NOT NULL
			THEN
				raise_application_error (-20762, '');
			END IF;

			--New Query cannot be closed or reissued
			IF v_status = 92441 AND (p_status = 92444 OR p_status = 92443)
			THEN
				raise_application_error (-20763, '');
			END IF;

			-- Answered Query can't be New
			IF v_status = 92442 AND p_status = 92441
			THEN
				raise_application_error (-20764, '');
			END IF;

			-- Answered Query can't be Answered again
			IF v_status = 92442 AND p_status = 92442
			THEN
				raise_application_error (-20765, '');
			END IF;

			--Re-issued cannot be closed or reissued
			IF v_status = 92443 AND (p_status = 92444 OR p_status = 92441)
			THEN
				raise_application_error (-20763, '');
			END IF;

			--Closed cannot be re opened or updated
			IF v_status = 92444 AND (p_status = 92443 OR p_status = 92442 OR p_status = 92441 OR p_status = 92444)
			THEN
				raise_application_error (-20766, '');
			END IF;
		END IF;

		IF p_patient_list_id IS NULL
		THEN
			SELECT t623.c622_patient_list_id
			  INTO v_patient_list_id
			  FROM t623_patient_answer_list t623
			 WHERE t623.c623_patient_ans_list_id = p_patient_ans_list_id AND t623.c623_delete_fl = 'N';
		ELSE
			v_patient_list_id := p_patient_list_id;
		END IF;

		SELECT t614.c614_study_site_id, t621.c611_study_id, t621.c621_patient_id, t622.c601_form_id
			 , t622.c613_study_period_id, t614.c704_account_id
		  INTO v_study_site_id, v_study_id, v_patient_id, v_form_id
			 , v_study_period_id, v_account_id
		  FROM t622_patient_list t622, t621_patient_master t621, t614_study_site t614
		 WHERE t622.c622_patient_list_id = v_patient_list_id
		   AND t621.c621_patient_id = t622.c621_patient_id
		   AND t621.c611_study_id = t614.c611_study_id
		   AND t621.c704_account_id = t614.c704_account_id
		   AND t622.c622_delete_fl = 'N'
		   AND t621.c621_delete_fl = 'N'
		   AND t614.c614_void_fl IS NULL;

		gm_pkg_cm_query_info.gm_sav_query (p_query_id
										 , p_query_level_id
										 , v_study_site_id
										 , SUBSTR (p_query_text, 0, 20)
										 , v_study_id
										 , v_form_id
										 , v_patient_id
										 , v_study_period_id
										 , v_account_id
										 , v_patient_list_id
										 , p_patient_ans_list_id
										 , p_status
										 , p_user_id
										  );
		gm_pkg_cm_query_info.gm_sav_query_longdesc (p_query_id, p_status, p_user_id, p_query_text);

		UPDATE t622_patient_list t622
		   SET t622.c622_verify_fl = ''
			 , t622.c622_last_updated_by = p_user_id
			 , t622.c622_last_updated_date = SYSDATE
		 WHERE t622.c622_patient_list_id = v_patient_list_id AND t622.c622_verify_fl = 'Y';

		v_number	:= SQL%ROWCOUNT;

		--gm_procedure_log ('v_number : ', v_number);
		IF v_number != 0
		THEN
			SELECT s625_patient_verify.NEXTVAL
			  INTO v_id
			  FROM DUAL;

			INSERT INTO t625_patient_verify_list
						(c625_pat_verify_list_id, c622_patient_list_id, c101_user_id, c625_date, c901_verify_level
						)
				 VALUES (v_id, v_patient_list_id, p_user_id, TRUNC (SYSDATE), 6193
						);

			--
			-- Code to save the user entered comments information
			--
			gm_update_log (v_id, 'Query has been raised', 1204, p_user_id, v_msg);
		END IF;

		IF p_patient_ans_list_id IS NOT NULL
		THEN
			UPDATE t623_patient_answer_list t623
			   SET t623.c623_history_flag = 'Y'
				 , t623.c623_last_updated_by = p_user_id
				 , t623.c623_last_updated_date = SYSDATE
			 WHERE t623.c623_patient_ans_list_id = p_patient_ans_list_id;
		END IF;

		-- When a query is raised for exam date, the history flag should be enabled.
		IF p_query_level_id = '92423' AND p_patient_list_id IS NOT NULL
		THEN
			UPDATE t622_patient_list
			   SET c622_date_history_fl = 'Y'
				 , c622_last_updated_by = p_user_id
				 , c622_last_updated_date = SYSDATE
			 WHERE c622_patient_list_id = p_patient_list_id;

			v_querydetails :=
				   'Query ID = '
				|| p_query_id
				|| ', '
				|| SUBSTR (p_query_text, 0, 50)
				|| ', Status='
				|| get_code_name (p_status);
			gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (p_patient_list_id, v_querydetails, 1017, p_user_id, SYSDATE);
		END IF;
	END gm_sav_query_details;

	PROCEDURE gm_sav_query_longdesc (
		p_query_id	   IN OUT	t6510_query.c6510_query_id%TYPE
	  , p_status	   IN		t6510_query.c901_status%TYPE
	  , p_user_id	   IN		t102_user_login.c102_user_login_id%TYPE
	  , p_query_text   IN		t6511_query_longdesc.c6511_query_text%TYPE
	)
	AS
		v_query_longdesc_id NUMBER;
	BEGIN
		SELECT s6511_query_longdesc.NEXTVAL
		  INTO v_query_longdesc_id
		  FROM DUAL;

		INSERT INTO t6511_query_longdesc
					(c6511_query_longdesc_id, c6510_query_id, c6511_query_text, c6511_last_updated_by
				   , c6511_last_updated_date, c901_status
					)
			 VALUES (v_query_longdesc_id, p_query_id, p_query_text, p_user_id
				   , SYSDATE, p_status
					);
	END gm_sav_query_longdesc;

	PROCEDURE gm_sav_query (
		p_query_id				IN OUT	 t6510_query.c6510_query_id%TYPE
	  , p_query_level_id		IN		 t6510_query.c901_query_level_id%TYPE
	  , p_study_site_id 		IN		 t614_study_site.c614_study_site_id%TYPE
	  , p_short_desc			IN		 VARCHAR
	  , p_study_id				IN		 t621_patient_master.c611_study_id%TYPE
	  , p_form_id				IN		 t622_patient_list.c601_form_id%TYPE
	  , p_patient_id			IN		 t621_patient_master.c621_patient_id%TYPE
	  , p_study_period_id		IN		 t622_patient_list.c613_study_period_id%TYPE
	  , p_account_id			IN		 t614_study_site.c704_account_id%TYPE
	  , p_patient_list_id		IN		 t622_patient_list.c622_patient_list_id%TYPE
	  , p_patient_ans_list_id	IN		 t623_patient_answer_list.c623_patient_ans_list_id%TYPE
	  , p_status				IN		 t6510_query.c901_status%TYPE
	  , p_user_id				IN		 t102_user_login.c102_user_login_id%TYPE
	)
	AS
	BEGIN
		UPDATE t6510_query
		   SET c901_status = p_status
			 , c901_query_level_id = p_query_level_id
			 , c611_study_id = p_study_id
			 , c601_form_id = p_form_id
			 , c623_patient_ans_list_id = p_patient_ans_list_id
			 , c622_patient_list_id = p_patient_list_id
			 , c614_study_site_id = p_study_site_id
			 , c621_patient_id = p_patient_id
			 , c613_study_period_id = p_study_period_id
			 , c704_account_id = p_account_id
			 , c6510_last_updated_by = p_user_id
			 , c6510_last_updated_date = SYSDATE
		 WHERE c6510_query_id = p_query_id AND c6510_void_fl IS NULL;

		IF SQL%ROWCOUNT = 0
		THEN
			SELECT s6510_query.NEXTVAL
			  INTO p_query_id
			  FROM DUAL;

			INSERT INTO t6510_query
						(c6510_query_id, c6510_short_desc, c901_status, c6510_created_by, c6510_created_date
					   , c901_query_level_id, c611_study_id, c601_form_id, c623_patient_ans_list_id
					   , c622_patient_list_id, c614_study_site_id, c621_patient_id, c613_study_period_id
					   , c704_account_id, c101_reporter_id, c6510_reported_date
						)
				 VALUES (p_query_id, p_short_desc, p_status, p_user_id, SYSDATE
					   , p_query_level_id, p_study_id, p_form_id, p_patient_ans_list_id
					   , p_patient_list_id, p_study_site_id, p_patient_id, p_study_period_id
					   , p_account_id, p_user_id, SYSDATE
						);
		END IF;
	END gm_sav_query;

	/*******************************************************
	  * Description : Procedure to void sitemapping
	  * Parameters	 : 1. Query Id
	  * 		 2. User Id
	  *
	  *******************************************************/
	PROCEDURE gm_sav_void_query (
		p_txn_id   IN	t6510_query.c6510_query_id%TYPE
	  , p_userid   IN	t102_user_login.c102_user_login_id%TYPE
	)
	AS
		v_status	   NUMBER;
		v_comments	   VARCHAR2 (4000);
		v_reporter_access_level t101_user.c101_access_level_id%TYPE;
		v_user_access_level t101_user.c101_access_level_id%TYPE;
		v_pat_list_id  t622_patient_list.c622_patient_list_id%TYPE;
		v_querydetails VARCHAR2 (4000);
	BEGIN
		SELECT t101.c101_access_level_id, t6510.c622_patient_list_id
		  INTO v_reporter_access_level, v_pat_list_id
		  FROM t101_user t101, t6510_query t6510
		 WHERE t6510.c101_reporter_id = t101.c101_user_id AND t6510.c6510_query_id = p_txn_id;

		SELECT t101.c101_access_level_id
		  INTO v_user_access_level
		  FROM t101_user t101
		 WHERE t101.c101_user_id = p_userid;

		IF v_user_access_level <> v_reporter_access_level
		THEN
			raise_application_error (-20760, '');
		END IF;

		SELECT c901_status
		  INTO v_status
		  FROM t6510_query t6510
		 WHERE t6510.c6510_query_id = p_txn_id;

		--Answered cannot be voided.
		IF v_status != 92441
		THEN
			raise_application_error (-20767, '');
		END IF;

		SELECT get_code_name (t907.c901_cancel_cd) || DECODE (c907_comments, '', '', ', ' || c907_comments)
		  INTO v_comments
		  FROM t907_cancel_log t907
		 WHERE t907.c901_type = 92460 AND t907.c907_ref_id = p_txn_id;

		UPDATE t6510_query
		   SET c6510_void_fl = 'Y'
			 , c6510_last_updated_by = p_userid
			 , c6510_last_updated_date = SYSDATE
		 WHERE t6510_query.c6510_query_id = p_txn_id;

		INSERT INTO t623a_patient_answer_history
					(c623a_patient_ans_his_id, c601_form_id, c602_question_id, c603_answer_grp_id, c605_answer_list_id
				   , c904_answer_ds, c622_patient_list_id, c623_delete_fl, c623_created_by, c623_created_date
				   , c623_patient_ans_list_id, c621_patient_id, c6510_query_id, c6511_query_text, c901_status)
			(SELECT s623a_patient_answer_history.NEXTVAL, t6510.c601_form_id, t623.c602_question_id
				  , t623.c603_answer_grp_id, t623.c605_answer_list_id, t623.c904_answer_ds, t622.c622_patient_list_id
				  , t623.c623_delete_fl, p_userid, SYSDATE, t623.c623_patient_ans_list_id, t623.c621_patient_id
				  , p_txn_id, 'Query ID = ' || p_txn_id || ', ' || v_comments, 92472
			   FROM t6510_query t6510, t622_patient_list t622, t623_patient_answer_list t623
			  WHERE t6510.c622_patient_list_id = t622.c622_patient_list_id
			  	AND NVL(t622.C622_DELETE_FL,'N') = 'N'
			  	AND NVL(t623.C623_DELETE_FL,'N') = 'N'
				AND t6510.c623_patient_ans_list_id = t623.c623_patient_ans_list_id
				AND t6510.c6510_query_id = p_txn_id);

		v_querydetails := 'Query ID = ' || p_txn_id || ', ' || v_comments || ', Status=' || get_code_name (92472);
		gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (v_pat_list_id, v_querydetails, 1017, p_userid, SYSDATE);
	END gm_sav_void_query;

	/*******************************************************
	 * Description : Function to fetch outstanding issues(queries)
	 * Parameters	:p_study_site_id
	 *******************************************************/
	FUNCTION gm_fch_outstanding_issues (
		p_study_site_id   IN   t6510_query.c614_study_site_id%TYPE
	)
		RETURN VARCHAR2
	IS
		v_count 	   VARCHAR2 (100);
	BEGIN
		BEGIN
			SELECT COUNT (1)
			  INTO v_count
			  FROM t6510_query t6510
			 WHERE t6510.c6510_reported_date < TRUNC (SYSDATE)
			   AND t6510.c901_status IN (92441, 92443)
			   AND t6510.c614_study_site_id = p_study_site_id
			   AND t6510.c6510_void_fl IS NULL;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				RETURN '';
		END;

		RETURN v_count;
	END gm_fch_outstanding_issues;

	/*******************************************************
	 * Description : Procedure to fetch email ids for query system email
	 * Parameters	:p_study_site_id
	 *******************************************************/
	FUNCTION get_cc_email_ids (
		p_study_site_id   IN   t6510_query.c614_study_site_id%TYPE
	)
		RETURN VARCHAR2
	IS
		v_email_ids    VARCHAR2 (1000);
		v_cra_email_id VARCHAR2 (1000);

		CURSOR v_email_cur
		IS
			SELECT t101.c101_email_id ID
			  FROM t616_site_party_mapping t616, t101_user t101
			 WHERE t616.c101_party_id = t101.c101_party_id
			   AND t616.c614_study_site_id = p_study_site_id
			   AND t616.c616_primary_fl IS NULL
			   AND t616.c616_void_fl IS NULL;
	BEGIN
		BEGIN
			FOR email IN v_email_cur
			LOOP
				IF email.ID IS NOT NULL
				THEN
					v_email_ids := v_email_ids || ',' || email.ID;
				END IF;
			END LOOP;

			SELECT get_user_emailid (t614.c101_user_id)
			  INTO v_cra_email_id
			  FROM t614_study_site t614
			 WHERE t614.c614_study_site_id = p_study_site_id;

			v_email_ids := v_cra_email_id || v_email_ids;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				RETURN '';
		END;

		RETURN v_email_ids;
	END get_cc_email_ids;

	/*******************************************************
	 * Description : Procedure to fetch To email ids for query system email
	 * Parameters	:p_study_site_id
	 *******************************************************/
	FUNCTION get_to_email_id (
		p_study_site_id   IN   t6510_query.c614_study_site_id%TYPE
	)
		RETURN VARCHAR2
	IS
		v_email_ids    VARCHAR2 (1000);
	BEGIN
		BEGIN
			SELECT t101.c101_email_id ID
			  INTO v_email_ids
			  FROM t616_site_party_mapping t616, t101_user t101
			 WHERE t616.c101_party_id = t101.c101_party_id
			   AND t616.c614_study_site_id = p_study_site_id
			   AND t616.c616_primary_fl = 'Y'
			   AND t616.c616_void_fl IS NULL;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				RETURN '';
		END;

		RETURN v_email_ids;
	END get_to_email_id;

	/*******************************************************
		* Description : Procedure to fetch site info
		* Parameters	 :
		*******************************************************/
	PROCEDURE gm_fch_site_info (
		p_siteinfo	 OUT   TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_siteinfo
		 FOR
			 SELECT t611.c611_study_id studyid, t611.c611_study_nm studynm, t6510.c614_study_site_id site_id
				  , gm_pkg_cm_query_info.get_to_email_id (t6510.c614_study_site_id) to_email_id
				  , gm_pkg_cm_query_info.gm_fch_outstanding_issues (t6510.c614_study_site_id) outstanding_issues
				  , gm_pkg_cm_query_info.get_cc_email_ids (t6510.c614_study_site_id) cc_email_ids
			   FROM t616_site_party_mapping t616
				  , t611_clinical_study t611
				  , (SELECT   t6510.c611_study_id, t6510.c614_study_site_id
						 FROM t6510_query t6510
						WHERE TRUNC (t6510.c6510_reported_date) = TRUNC (SYSDATE) AND t6510.c6510_void_fl IS NULL
					 GROUP BY t6510.c611_study_id, t6510.c614_study_site_id) t6510
			  WHERE t6510.c614_study_site_id = t616.c614_study_site_id
				AND t6510.c611_study_id = t611.c611_study_id
				AND t616.c616_void_fl IS NULL
				AND t616.c616_primary_fl = 'Y'
				AND t611.c611_delete_fl = 'N';
	END gm_fch_site_info;

	/*******************************************************
	 * Description : Procedure to fetch site info
	 * Parameters	:
	 *******************************************************/
	PROCEDURE gm_fch_patient_info (
		p_study_site_id   IN	   t6510_query.c614_study_site_id%TYPE
	  , p_patientinfo	  OUT	   TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_patientinfo
		 FOR
			 SELECT   t621.c621_patient_ide_no patient_id, COUNT (t6510.c6510_query_id) no_of_queries
				 FROM t6510_query t6510, t622_patient_list t622, t621_patient_master t621
				WHERE t6510.c622_patient_list_id = t622.c622_patient_list_id
				  AND t622.c621_patient_id = t621.c621_patient_id
				  AND TRUNC (t6510.c6510_reported_date) = TRUNC (SYSDATE)
				  AND t6510.c6510_void_fl IS NULL
				  AND t622.c622_delete_fl = 'N'
				  AND t621.c621_delete_fl = 'N'
				  AND t6510.c901_status = 92441
				  AND t6510.c614_study_site_id = p_study_site_id
			 GROUP BY t621.c621_patient_ide_no;
	END gm_fch_patient_info;
END gm_pkg_cm_query_info;
/
