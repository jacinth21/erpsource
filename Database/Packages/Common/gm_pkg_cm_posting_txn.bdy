/* Formatted on 2012/05/24 16:40 (Formatter Plus v4.8.0) */
--@"C:\database\packages\common\Gm_pkg_cm_posting_txn.bdy";
CREATE OR REPLACE PACKAGE BODY gm_pkg_cm_posting_txn
IS

/*****************************************************************************************************
* Description : This procedure used to store the posting details based on transactions id
*******************************************************************************************************/
PROCEDURE gm_sav_posting_status (
        p_ref_id     IN t830_posting_status.c830_ref_id%TYPE,
        p_company_id     IN t830_posting_status.c1900_company_id%TYPE,
        p_plant_id IN t830_posting_status.c5040_plant_id%TYPE,
        p_user_id    IN t830_posting_status.c830_last_updated_by%TYPE)
AS
BEGIN
     UPDATE t830_posting_status
    SET c1900_company_id       = p_company_id, c5040_plant_id = p_plant_id
    	, c830_last_updated_by = p_user_id ,c830_last_updated_date = CURRENT_DATE
      WHERE c830_ref_id   = p_ref_id;
    --
    
    IF (SQL%ROWCOUNT = 0) THEN
    
         INSERT
           INTO t830_posting_status
            (
                c830_posting_status_id, c830_ref_id, c1900_company_id
              , c5040_plant_id, c830_last_updated_by, c830_last_updated_date
            )
            VALUES
            (
                s830_posting_status.nextval, p_ref_id, p_company_id
              , p_plant_id, p_user_id, CURRENT_DATE
            ) ;
    END IF;
END gm_sav_posting_status;
/*****************************************************************************************************
* Description : This procedure used to set the company and plant information to context - based on transactions company 
*******************************************************************************************************/

PROCEDURE gm_sav_trans_comp_plant_id (
        p_ref_id     IN t830_posting_status.c830_ref_id%TYPE,
        p_user_id    IN t830_posting_status.c830_last_updated_by%TYPE)
AS
    v_company_id_ctx NUMBER;
    v_plant_id_ctx   NUMBER;
    --
    v_txn_company_id NUMBER;
    v_txn_plant_id   NUMBER;
    --
    v_post_company_id NUMBER;
    v_post_plant_id   NUMBER;
BEGIN
    --1. get the company id from context
     SELECT get_compid_frm_cntx (), get_plantid_frm_cntx ()
       INTO v_company_id_ctx, v_plant_id_ctx
       FROM dual;
    --2. to get the company from posting table   
    BEGIN
         SELECT c1900_company_id, c5040_plant_id
           INTO v_post_company_id, v_post_plant_id
           FROM t830_posting_status
          WHERE c830_ref_id = p_ref_id;
    EXCEPTION
    WHEN OTHERS THEN
        v_post_company_id := NULL;
        v_post_plant_id   := NULL;
    END;
    --3. compare the posting company id details 
    IF v_post_company_id IS NULL THEN
        -- to get the transactions company and plant information
        gm_pkg_common.gm_fch_trans_comp_plant_id (p_ref_id, v_txn_company_id, v_txn_plant_id) ;
        --
         SELECT NVL (v_txn_company_id, v_company_id_ctx), NVL (v_txn_plant_id, v_plant_id_ctx)
           INTO v_txn_company_id, v_txn_plant_id
           FROM DUAL;
        -- to save the company and plant information to Posting status
        gm_pkg_cm_posting_txn.gm_sav_posting_status (p_ref_id, v_txn_company_id, v_txn_plant_id, p_user_id) ;
        -- to update the company and plant
        gm_pkg_cor_client_context.gm_sav_client_context (v_txn_company_id, v_txn_plant_id) ;
    ELSE
        IF v_post_company_id <> v_company_id_ctx OR v_post_plant_id <> v_plant_id_ctx THEN
        	-- to update the company and plant
            gm_pkg_cor_client_context.gm_sav_client_context (v_post_company_id, v_post_plant_id) ;
        END IF;
    END IF;
    --
END gm_sav_trans_comp_plant_id;
--

/*****************************************************************************************************
* Description : This procedure used to save the skipped transactions details.
*     If any parts belogns to family (Construct Code, Non Inventory Items, Non Stock and Usage Code)
*     No Posting will be happen. Using the below procedure - we can track the skipped transactions details.
* Author : mmuthusamy
*******************************************************************************************************/

PROCEDURE gm_sav_skip_posting_dtls (
        p_txn_id      IN t811_skipped_posting_dtls.c811_txn_id%TYPE,
        p_part_number IN t811_skipped_posting_dtls.c205_part_number_id%TYPE,
        p_qty         IN t811_skipped_posting_dtls.c811_qty%TYPE,
        p_trans_type  IN t811_skipped_posting_dtls.c901_transaction_type%TYPE,
        p_product_family IN t811_skipped_posting_dtls.c901_product_family%TYPE,
        p_company_id     IN t811_skipped_posting_dtls.c1900_company_id%TYPE,
        p_user_id     IN t811_skipped_posting_dtls.c811_last_updated_by%TYPE)
AS

BEGIN
    --
     INSERT
       INTO t811_skipped_posting_dtls
        (
            c811_skipped_posting_dtls_id, c811_txn_id, c205_part_number_id
          , c811_qty, c901_transaction_type, c1900_company_id, c901_product_family
          , c811_last_updated_by, c811_last_updated_date, c811_last_updated_date_utc
        )
        VALUES
        (
            s811_skipped_posting_dtls.nextval, p_txn_id, p_part_number
          , p_qty, p_trans_type, p_company_id, p_product_family
          , p_user_id, CURRENT_DATE, SYSTIMESTAMP
        ) ;
        --
END gm_sav_skip_posting_dtls;

END gm_pkg_cm_posting_txn;
/