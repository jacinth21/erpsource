/* Formatted on 2010/10/08 19:37 (Formatter Plus v4.8.0) */
--	

CREATE OR REPLACE PACKAGE BODY gm_pkg_cm_accesscontrol
IS
   /*******************************************************
   * Description : Procedure to fetch party list
   * Author    : Ritesh Shah
   *******************************************************/
   PROCEDURE gm_fch_partylist (
      p_groupid        IN       t1501_group_mapping.c1500_group_id%TYPE,
      p_partyid        IN       t1501_group_mapping.c101_mapped_party_id%TYPE,
      p_outpartylist   OUT      TYPES.cursor_type
   )
   AS 
   BEGIN
	   
	   IF p_groupid IS NULL OR p_partyid IS NULL
	   THEN
      	my_context.set_my_inlist_ctx (p_groupid);

      OPEN p_outpartylist
       FOR
       	  SELECT c101_party_id codeid, GET_PARTY_NAME(c101_party_id ) codenm
		    FROM T101_party
		    WHERE C101_VOID_FL IS NULL
		      AND c101_party_id IN
		        (SELECT c101_mapped_party_id
		           FROM t1501_group_mapping
		          WHERE c1500_group_id IN (SELECT * FROM v_in_list)
		            AND c101_mapped_party_id = NVL (p_partyid, c101_mapped_party_id)
		        )
		    ORDER BY codenm;
       ELSE 
       		-- This will return the records that this party is mapped with the group and having the read Access
       		gm_pkg_cm_accesscontrol.gm_fch_party_accesspermissions(p_partyid,p_groupid,p_outpartylist);       
       END IF;
   END gm_fch_partylist;

   /*******************************************************
   * Description : Procedure to fetch access permissions
   * Author    : Ritesh Shah
   *******************************************************/
   PROCEDURE gm_fch_accesspermissions (
      p_partyid         IN       t1530_access.c101_party_id%TYPE,
      p_functionid      IN       t1530_access.c1520_function_id%TYPE,
      p_outaccessperm   OUT      TYPES.cursor_type
   )
   AS
   BEGIN
      OPEN p_outaccessperm
       FOR
          --PC-2153: SpineIT - Portal login performance improvement
       	 SELECT distinct t1530.c1530_access_id ID, 
       	 		t1530.c1530_update_access_fl updfl, 
       	 		t1530.c1530_read_access_fl readfl,
  				t1530.c1530_void_access_fl voidfl
   		   FROM t1530_access t1530, 
   		   		t1501_group_mapping t1501, 
   		   		t1520_function_list t1520
  		  WHERE t1520.c1520_function_id  = t1530.c1520_function_id
    		AND t1530.c1500_group_id = NVL (t1501.c1500_group_id, t1501.c1500_mapped_group_id)
    		AND (t1501.c101_mapped_party_id = p_partyid
    			OR t1530.c101_party_id = p_partyid)
    		AND t1520.c1520_function_id = p_functionid
    		AND t1530.c1530_void_fl IS NULL
    		AND t1501.C1501_VOID_FL IS NULL
    		AND t1520.C1520_VOID_FL IS NULL;

   END gm_fch_accesspermissions;

   
   
   /*******************************************************
   * Description : Procedure to fetch iPad users app module access permissions
   * Author    : jgurunathan
   *******************************************************/
   PROCEDURE gm_fch_mobileaccesspermissions (
      p_partyid         IN       t1530_access.c101_party_id%TYPE,
      p_outaccessperm   OUT      TYPES.cursor_type
   )
   AS
   BEGIN
      OPEN p_outaccessperm
       FOR
         SELECT DISTINCT t1530.C1520_FUNCTION_ID funcid,
                t1530.C1530_FUNCTION_NM funcnm,
                t1530.c1530_update_access_fl updateacc,
                t1530.c1530_read_access_fl readacc,
                t1530.c1530_void_access_fl voidacc
            FROM t1530_access t1530
           WHERE (    t1530.c1530_void_fl IS NULL
                  AND (   t1530.c101_party_id = p_partyid
                       OR t1530.c1500_group_id IN (
                             SELECT NVL (t1501.c1500_group_id,
                                         t1501.c1500_mapped_group_id
                                        )
                               FROM t1501_group_mapping t1501,
                                    t1530_access t1530
                              WHERE t1501.c101_mapped_party_id = p_partyid
                                AND t1530.c1500_group_id =
                                       NVL (t1501.c1500_group_id,
                                            t1501.c1500_mapped_group_id
                                           )
                                 and c1501_void_fl is null)
                      )
                  AND (   t1530.c1520_function_id IN (SELECT c906_rule_value FROM t906_rules where c906_rule_id = 'MOBILE_APP_ACCESS' and c906_rule_grp_id = 'MOBILE_APP_ACCESS' and c906_void_fl is null)
                       OR t1530.c1520_function_id IN (
                             SELECT t1520.c1520_function_id
                               FROM t1520_function_list t1520
                              WHERE t1520.c1520_inherited_from_id IN (SELECT c906_rule_value FROM t906_rules where c906_rule_id = 'MOBILE_APP_ACCESS' and c906_rule_grp_id = 'MOBILE_APP_ACCESS' and c906_void_fl is null))
                      )
                 );
   END gm_fch_mobileaccesspermissions;

   
   /*******************************************************
   * Description : Procedure to fetch access permissions
   * Author    : Ritesh Shah
   *******************************************************/
   PROCEDURE gm_fch_accesspermission (
      p_partyid         IN       t1530_access.c101_party_id%TYPE,
      p_requesturi      IN       t1520_function_list.c1520_request_uri%TYPE,
      p_outaccessperm   OUT      t1530_access.c1530_update_access_fl%TYPE
   )
   AS
      v_function_id   NUMBER := -999;
   BEGIN
      SELECT c1520_function_id
        INTO v_function_id
        FROM t1520_function_list
       WHERE c1520_request_uri = p_requesturi;

      SELECT t1530.c1530_update_access_fl
        INTO p_outaccessperm
        FROM t1530_access t1530
       WHERE (    t1530.c1530_void_fl IS NULL
              AND (   t1530.c101_party_id = p_partyid
                   OR t1530.c1500_group_id IN (
                         SELECT NVL (t1501.c1500_group_id,
                                     t1501.c1500_mapped_group_id
                                    )
                           FROM t1501_group_mapping t1501, t1530_access t1530
                          WHERE t1501.c101_mapped_party_id = p_partyid
                            AND t1530.c1500_group_id =
                                   NVL (t1501.c1500_group_id,
                                        t1501.c1500_mapped_group_id
                                       ))
                  )
              AND (   t1530.c1520_function_id = v_function_id
                   OR t1530.c1520_function_id IN (
                           SELECT t1520.c1520_function_id
                             FROM t1520_function_list t1520
                            WHERE t1520.c1520_inherited_from_id =
                                                                 v_function_id)
                  )
             );
   END gm_fch_accesspermission;

   /*******************************************************
   * Description : Procedure to fetch clinical toolbar for specific group
   * Author    : Rshah
   *******************************************************/
   PROCEDURE gm_fch_clinical_toolbar (
      p_grpid        IN       t1500_group.c1500_group_id%TYPE,
      p_partyid      IN       t1501_group_mapping.c101_mapped_party_id%TYPE,
      p_outtoolbar   OUT      TYPES.cursor_type
   )
   AS
      v_context     VARCHAR2 (10);
      v_accesslvl   NUMBER;
   BEGIN
      IF p_grpid IN ('HMCRARPT', 'HMSCRPT', 'HMCRATRANS', 'HMSCTRANS')
      THEN
         v_context := 'HOME';
      ELSIF p_grpid IN ('STCRARPT', 'STSCRPT', 'STCRATRANS', 'STSCTRANS')
      THEN
         v_context := 'STUDY';
      ELSIF p_grpid IN ('SLCRARPT', 'SLSCRPT', 'SLCRATRANS', 'SLSCTRANS')
      THEN
         v_context := 'SITE';
      ELSIF p_grpid IN ('PTLCRARPT', 'PTLSCRPT', 'PTLCRATRANS', 'PTLSCTRANS')
      THEN
         v_context := 'PATIENT';
      END IF;

      SELECT t101.c101_access_level_id
        INTO v_accesslvl
        FROM t101_user t101
       WHERE t101.c101_user_id = p_partyid AND t101.c901_user_status = 311;

      IF v_accesslvl < 4
      THEN
         OPEN p_outtoolbar
          FOR
             SELECT   (SELECT CASE
                                 WHEN t1520a.c1520_function_id < 0
                                    THEN ''
                                 ELSE t1520a.c1520_function_nm
                              END
                         FROM t1520_function_list t1520a
                        WHERE t1520a.c1520_function_id =
                                        t1530.c1520_parent_function_id)
                                                                      PARENT,
                         (SELECT CASE
                                    WHEN t1520a.c1520_function_id < 0
                                       THEN ''
                                    ELSE t1520a.c1520_function_nm
                                 END
                            FROM t1520_function_list t1520a
                           WHERE t1520a.c1520_function_id =
                                          t1530.c1520_parent_function_id)
                      || ' -- '
                      || t1520.c1520_function_nm NAME,
                      t1520.c1520_function_nm CHILD,
                      t1520.c1520_function_id ID,
                      t1520.c1520_request_uri action,
                      t1520.c1520_request_stropt stropt,
                      t1530.c1530_access_id accid, t1530.c1530_seq_no seqno,
                      v_context CONTEXT
                 FROM t1500_group t1500,
                      t1501_group_mapping t1501,
                      t1530_access t1530,
                      t1520_function_list t1520
                WHERE t1500.c1500_group_id = p_grpid
                  AND t1500.c1500_group_id = t1501.c1500_group_id
                  AND t1500.c1500_group_id = t1530.c1500_group_id
                  AND t1530.c1520_function_id = t1520.c1520_function_id
                  AND t1520.c1520_inherited_from_id IS NOT NULL
                  AND t1530.c101_party_id IS NULL
                  AND t1530.c1530_void_fl IS NULL
             ORDER BY seqno;
      ELSE
         OPEN p_outtoolbar
          FOR
             SELECT   (SELECT CASE
                                 WHEN t1520a.c1520_function_id < 0
                                    THEN ''
                                 ELSE t1520a.c1520_function_nm
                              END
                         FROM t1520_function_list t1520a
                        WHERE t1520a.c1520_function_id =
                                        t1530.c1520_parent_function_id)
                                                                      PARENT,
                         (SELECT CASE
                                    WHEN t1520a.c1520_function_id < 0
                                       THEN ''
                                    ELSE t1520a.c1520_function_nm
                                 END
                            FROM t1520_function_list t1520a
                           WHERE t1520a.c1520_function_id =
                                          t1530.c1520_parent_function_id)
                      || ' -- '
                      || t1520.c1520_function_nm NAME,
                      t1520.c1520_function_nm CHILD,
                      t1520.c1520_function_id ID,
                      t1520.c1520_request_uri action,
                      t1520.c1520_request_stropt stropt,
                      t1530.c1530_access_id accid, t1530.c1530_seq_no seqno,
                      v_context CONTEXT
                 FROM t1500_group t1500,
                      t1501_group_mapping t1501,
                      t1530_access t1530,
                      t1520_function_list t1520
                WHERE t1500.c1500_group_id = p_grpid
                  AND t1500.c1500_group_id = t1501.c1500_group_id
                  AND t1500.c1500_group_id = t1530.c1500_group_id
                  AND t1530.c1520_function_id = t1520.c1520_function_id
                  AND t1520.c1520_inherited_from_id IS NOT NULL
                  AND t1530.c1530_void_fl IS NULL
             ORDER BY seqno;
      END IF;
   END gm_fch_clinical_toolbar;

   /*******************************************************
   * Description : Procedure to fetch access permission
   * Author    : Satyajit Thadeshwar
   *******************************************************/
   FUNCTION gm_fch_user_grp_chk (
      p_partyid   IN   t1530_access.c101_party_id%TYPE,
      p_grp_id    IN   t1501_group_mapping.c1500_group_id%TYPE
   )
      RETURN VARCHAR2
   IS
      v_status   VARCHAR2 (20);
   BEGIN
      BEGIN
         SELECT TO_CHAR (COUNT (1))
           INTO v_status
           FROM t1501_group_mapping
          WHERE c101_mapped_party_id = p_partyid AND c1500_group_id = p_grp_id
           AND c1501_void_fl IS NULL;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            v_status := '0';
      END;

      RETURN v_status;
   END gm_fch_user_grp_chk;

  
 /*******************************************************
   * Description : Procedure to update/save access control group
   * Author   : Gopinathan
   *******************************************************/

PROCEDURE gm_sav_ac_group (
   p_grp_id     IN       t1500_group.c1500_group_id%TYPE,
   p_grp_name   IN       t1500_group.c1500_group_nm%TYPE,
   p_grp_desc   IN       t1500_group.c1500_group_desc%TYPE,
   p_user_id    IN       t1500_group.c1500_created_by%TYPE,
   p_str_opt    IN       VARCHAR2,
   p_grp_type   IN       t1500_group.c901_group_type%TYPE,
   p_out        OUT      VARCHAR2
)
AS
   v_count    NUMBER;
   v_seq_no   NUMBER;
   v_grp_id   VARCHAR2 (20);
   v_root     NUMBER;
   v_label		VARCHAR2(20);
BEGIN

	v_label:='Module ';
		IF p_grp_type = '92264'
		THEN
			v_label:='Security Group ';
		END IF;
		
   IF p_str_opt = 'edit'
   THEN
    
   	SELECT COUNT (1)
        INTO v_count
        FROM t1500_group
       WHERE c1500_group_id = p_grp_id 
       AND C1500_VOID_FL IS NOT NULL ;

       IF v_count > 0
      THEN
        GM_RAISE_APPLICATION_ERROR('-20999','5',v_label||'#$#'||p_grp_name);
      END IF;
      
      UPDATE t1500_group
         SET c1500_group_nm = p_grp_name,
             c1500_group_desc = p_grp_desc,
             c1500_last_updated_by = p_user_id,
             c1500_last_updated_date = CURRENT_DATE
--                c901_group_type = p_grp_type
      WHERE  c1500_group_id = p_grp_id AND C1500_VOID_FL IS NULL;
      v_grp_id:=p_grp_id ;
      
   ELSE
      SELECT COUNT (1)
        INTO v_count
        FROM t1500_group
       WHERE c1500_group_id = p_grp_id 
       AND C1500_VOID_FL IS NULL ;

      IF v_count > 0
      THEN
         raise_application_error ('-20754', '');
      END IF;

      SELECT COUNT (1)
        INTO v_count
        FROM t1500_group
       WHERE c1500_group_nm = p_grp_name
        AND C1500_VOID_FL IS NULL ;
	
      IF v_count > 0
      THEN
        GM_RAISE_APPLICATION_ERROR('-20999','3',v_label||'#$#'||p_grp_name);
      END IF;

      SELECT s1500_group.NEXTVAL
        INTO v_grp_id
        FROM DUAL;

      INSERT INTO t1500_group
                  (c1500_group_id, c1500_group_nm, c1500_group_desc,
                   c1500_created_by, c1500_created_date, c901_group_type
                  )
           VALUES (v_grp_id, p_grp_name, p_grp_desc,
                   p_user_id, CURRENT_DATE, p_grp_type
                  );

      IF p_grp_type = '92265'
      THEN
         SELECT (MAX (c1530_seq_no) + 20)
           INTO v_seq_no
           FROM t1530_access;

          SELECT  (S1520_FUNCTION_LIST_TASK.NEXTVAL) * -1
           INTO v_root
           FROM DUAL;

         v_root := v_root - 100;

         INSERT INTO t1520_function_list
                     (c1520_function_id, c1520_function_nm,
                      c1520_function_desc, c1520_inherited_from_id,
                      c901_type, c1520_void_fl, c1520_created_by,
                      c1520_created_date, c1520_last_updated_by,
                      c1520_last_updated_date, c1520_request_uri,
                      c1520_request_stropt, c1520_seq_no, c901_department_id
                     )
              VALUES (v_root, p_grp_name || ' Root ',
                      p_grp_name || ' Root ', NULL,
                      92262, NULL, p_user_id,
                      CURRENT_DATE, NULL,
                      NULL, NULL,
                      NULL, NULL, NULL
                     );

 			INSERT INTO t1530_access
                     (c1530_access_id, c1500_group_id, c1520_function_id,
                      c101_party_id, c1530_update_access_fl,
                      c1530_read_access_fl, c1530_void_access_fl,
                      c1530_void_fl, c1530_created_by, c1530_created_date,
                      c1530_last_updated_by, c1530_last_updated_date,
                      c1520_parent_function_id, c1530_function_nm,
                      c1530_seq_no
                     )
              VALUES (s1530_access.NEXTVAL, v_grp_id, v_root,
                      NULL, 'Y',
                      'Y', 'Y',
                      NULL, p_user_id, CURRENT_DATE,
                      NULL, NULL,
                      NULL, NULL,
                      v_seq_no
                     );

		 v_seq_no := v_seq_no + 100;

         --
--         v_seq_no := v_seq_no + 100;

         INSERT INTO t1530_access
                     (c1530_access_id, c1500_group_id, c1520_function_id,
                      c101_party_id, c1530_update_access_fl,
                      c1530_read_access_fl, c1530_void_access_fl,
                      c1530_void_fl, c1530_created_by, c1530_created_date,
                      c1530_last_updated_by, c1530_last_updated_date,
                      c1520_parent_function_id, c1530_function_nm,
                      c1530_seq_no
                     )
              VALUES (s1530_access.NEXTVAL, v_grp_id, '12273900',
                      NULL, 'Y',
                      'Y', 'Y',
                      NULL, p_user_id, CURRENT_DATE,
                      NULL, NULL,
                      v_root, NULL,
                      v_seq_no
                     );

         --
         v_seq_no := v_seq_no + 100;

         INSERT INTO t1530_access
                     (c1530_access_id, c1500_group_id, c1520_function_id,
                      c101_party_id, c1530_update_access_fl,
                      c1530_read_access_fl, c1530_void_access_fl,
                      c1530_void_fl, c1530_created_by, c1530_created_date,
                      c1530_last_updated_by, c1530_last_updated_date,
                      c1520_parent_function_id, c1530_function_nm,
                      c1530_seq_no
                     )
              VALUES (s1530_access.NEXTVAL, v_grp_id, '12274000',
                      NULL, 'Y',
                      'Y', 'Y',
                      NULL, p_user_id, CURRENT_DATE,
                      NULL, NULL,
                      v_root, NULL,
                      v_seq_no
                     );
      END IF;
   END IF;

   p_out := TO_CHAR (v_grp_id);
END gm_sav_ac_group;


   /*******************************************************
   * Description : Procedure to fetch access control group
   * Author   : Gopinathan
   *******************************************************/
   PROCEDURE gm_fch_ac_group_list (
   p_group_type IN t1500_group.c901_group_type%TYPE,
   p_cursor OUT TYPES.cursor_type)
   AS
   BEGIN
      OPEN p_cursor
       FOR
          SELECT   c1500_group_id groupid,
                   c1500_group_nm || ' (' || c1500_group_id || ')' groupname,
                   c1500_group_desc groupdesc ,GET_CODE_NAME(c901_group_type) grouptype
              FROM t1500_group t1500
             WHERE t1500.c1500_void_fl IS NULL 
               AND t1500.c901_group_type = p_group_type
          ORDER BY c1500_group_nm;
   END gm_fch_ac_group_list;

   /*******************************************************
   * Description : Procedure to fetch access control group for specific group
   * Author   : Gopinathan
   *******************************************************/
   PROCEDURE gm_fch_ac_group (
      p_grp_id   IN       t1500_group.c1500_group_id%TYPE,
      p_cursor   OUT      TYPES.cursor_type
   )
   AS
   BEGIN
      OPEN p_cursor
       FOR
          SELECT c1500_group_id groupid, c1500_group_nm groupname,
                 c1500_group_desc groupdesc,c901_group_type grouptype
            FROM t1500_group t1500
           WHERE t1500.c1500_void_fl IS NULL
             AND t1500.c1500_group_id = p_grp_id;
   END gm_fch_ac_group;

   /*******************************************************
   * Description : Procedure to save access control group for void group
   * Author   : Gopinathan
   *******************************************************/
   PROCEDURE gm_sav_void_group (
      p_txn_id    IN   t1500_group.c1500_group_id%TYPE,
      p_user_id   IN   t1500_group.c1500_last_updated_by%TYPE
   )
   AS
      v_cnt   NUMBER;
   BEGIN
      SELECT COUNT (1)
        INTO v_cnt
        FROM t1501_group_mapping
       WHERE c1500_group_id = p_txn_id 
       AND C1501_VOID_FL IS NULL;

      IF v_cnt > 0
      THEN
         raise_application_error ('-20755', '');
      END IF;
	 
	 SELECT COUNT (1)
        INTO v_cnt
        FROM t1530_access 
       WHERE
       c1500_group_id = p_txn_id 
       AND C1530_VOID_FL IS NULL
       AND c1520_function_id not in ('12273900', '12274000')
       AND c1520_parent_function_id is not null;

      IF v_cnt > 0
      THEN
         raise_application_error ('-20755', '');
      END IF;
            
      SELECT COUNT (1)
        INTO v_cnt
       FROM t1500_group
       WHERE c1500_group_id = p_txn_id
       	AND c1500_void_fl IS NOT NULL;
       	
      IF v_cnt > 0
      THEN
         GM_RAISE_APPLICATION_ERROR('-20999','5','');
      END IF;
       	
      UPDATE t1500_group
         SET c1500_void_fl = 'Y',
             c1500_last_updated_by = p_user_id,
             c1500_last_updated_date = CURRENT_DATE
       WHERE c1500_group_id = p_txn_id;
       
       UPDATE t1530_access
          SET c1530_void_fl = 'Y'
          	, c1530_last_updated_by = p_user_id
          	, c1530_last_updated_date = CURRENT_DATE
          WHERE c1500_group_id = p_txn_id;
       
   END gm_sav_void_group;

   /*******************************************************
   * Description : Procedure to fetch user detail for specific department
   * Author   : Gopinathan
   *******************************************************/
   PROCEDURE gm_fch_dept_user_list (
      p_dept_id      IN       t101_user.c901_dept_id%TYPE,
      p_out_cursor   OUT      TYPES.cursor_type
   )
   AS
   BEGIN
      OPEN p_out_cursor
       FOR
          SELECT   t101.c101_party_id partyid,
                      t101.c101_user_f_name
                   || ' '
                   || t101.c101_user_l_name username
              FROM t101_user t101
             WHERE t101.c901_dept_id = NVL (p_dept_id, t101.c901_dept_id)
               AND t101.c901_user_status = 311
               AND t101.c101_party_id IS NOT NULL
               AND t101.c101_party_id != 0
          ORDER BY username, t101.c901_dept_id;
   END gm_fch_dept_user_list;

   /*******************************************************
   * Description : Procedure to update/save user group
   * Author   : Gopinathan
   *******************************************************/
   PROCEDURE gm_sav_user_group (
      p_grp_id            IN       t1501_group_mapping.c1500_group_id%TYPE,
      p_mapping_user_id   IN       t1501_group_mapping.c101_mapped_party_id%TYPE,
      p_default_fl        IN       VARCHAR2,
      p_user_id           IN       t1501_group_mapping.c1501_created_by%TYPE,
      p_compid			  IN	   t1900_company.C1900_COMPANY_ID%TYPE,
      p_grp_mapping_id    IN OUT   VARCHAR2
   )
   AS
      v_default_grp_id   t1500_group.c1500_group_id%TYPE;
      v_grp_mapping_id   t1501_group_mapping.c1501_group_mapping_id%TYPE;
      v_count            NUMBER;
      v_party_id       NUMBER := NULL;
   	  v_dept_id        NUMBER;
      v_access_level   NUMBER;
   BEGIN
      BEGIN
         SELECT   COUNT (1), t101.c1500_group_id
             INTO v_count, v_default_grp_id
             FROM t101_party t101
            WHERE t101.c101_party_id = p_mapping_user_id
              AND t101.c1500_group_id IS NOT NULL 
              AND C101_VOID_FL IS NULL 
         GROUP BY t101.c1500_group_id;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            v_count := 0;
            v_default_grp_id := '';
      END;
      
      ---
         SELECT t101.c901_dept_id, t101.c101_access_level_id
        INTO v_dept_id, v_access_level
        FROM t101_user t101
       WHERE t101.c101_user_id = p_mapping_user_id;

         IF v_dept_id = 2005 AND (v_access_level <= 2 OR v_access_level = 7)
         THEN
         BEGIN
            SELECT t101.c101_party_id
              INTO v_party_id
              FROM t101_party t101, t703_sales_rep t703
             WHERE t101.c101_party_id = t703.c101_party_id
               AND t703.C703_SALES_REP_ID = p_mapping_user_id;
         EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
          BEGIN
            SELECT t101.c101_party_id
              INTO v_party_id
              FROM t101_user t101
             WHERE t101.c101_user_id = p_mapping_user_id;
	      EXCEPTION
    	  WHEN NO_DATA_FOUND
          THEN
            	v_party_id := NULL;
      	  END;
      	END;
       ELSE
      	   BEGIN
            SELECT t101.c101_party_id
              INTO v_party_id
             FROM t101_user t101
             WHERE t101.c101_user_id = p_mapping_user_id;
	      	EXCEPTION
    	    WHEN NO_DATA_FOUND
        	THEN
            	v_party_id := NULL;
      	   	END;
    	END IF;
      
      IF v_party_id IS NULL 
         THEN
         	v_party_id := p_mapping_user_id;
      END IF;

      BEGIN
         SELECT c1501_group_mapping_id
           INTO v_grp_mapping_id
           FROM t1501_group_mapping
          WHERE c1500_group_id = p_grp_id
            AND c101_mapped_party_id = v_party_id
            AND c1501_void_fl IS NULL
            AND c1900_company_id = p_compid;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            v_grp_mapping_id := p_grp_mapping_id;
      END;
      
      UPDATE t1501_group_mapping
         SET c1500_group_id = p_grp_id,
             c101_mapped_party_id = v_party_id,
             c1900_company_id = p_compid,
             c1501_last_updated_by = p_user_id,
             c1501_last_updated_date = CURRENT_DATE
       WHERE c1501_group_mapping_id = v_grp_mapping_id
         AND c1501_void_fl IS NULL;

      IF (SQL%ROWCOUNT = 0)
      THEN
         SELECT s1501_group_mapping.NEXTVAL
           INTO p_grp_mapping_id
           FROM DUAL;
         INSERT INTO t1501_group_mapping
                     (c1501_group_mapping_id, c1500_group_id,
                      c101_mapped_party_id, c1501_created_by,
                      c1501_created_date, c1900_company_id
                     )
              VALUES (p_grp_mapping_id, p_grp_id,
                      v_party_id, p_user_id,
                      CURRENT_DATE, p_compid
                     );
      END IF;

      IF    p_default_fl = 'Y'
         OR (p_grp_id = v_default_grp_id AND p_default_fl IS NULL)
      THEN
         IF p_default_fl IS NULL
         THEN
            v_default_grp_id := '';
         ELSE
            v_default_grp_id := p_grp_id;
         END IF;

      ------
         
         
         UPDATE t101_party t101
            SET t101.c1500_group_id = v_default_grp_id,
                t101.c101_last_updated_by = p_user_id,
                t101.c101_last_updated_date = CURRENT_DATE
          WHERE t101.c101_party_id = v_party_id
          AND C101_VOID_FL IS NULL;
      END IF;
   END gm_sav_user_group;

   /*******************************************************
   * Description : Procedure to save group mapping for void
   * Author   : Gopinathan
   *******************************************************/
   PROCEDURE gm_sav_void_group_mapping (
      p_txn_id    IN   t1501_group_mapping.c1501_group_mapping_id%TYPE,
      p_user_id   IN   t1501_group_mapping.c1501_last_updated_by%TYPE
   )
   AS
   BEGIN
      UPDATE t1501_group_mapping
         SET c1501_void_fl = 'Y',
             c1501_last_updated_by = p_user_id,
             c1501_last_updated_date = CURRENT_DATE
       WHERE c1501_group_mapping_id = p_txn_id;

      UPDATE t101_party t101
         SET t101.c1500_group_id = ''
             ,t101.c101_last_updated_by = p_user_id
             ,t101.c101_last_updated_date = CURRENT_DATE
       WHERE t101.c101_party_id =
                (SELECT t1501.c101_mapped_party_id
                   FROM t1501_group_mapping t1501
                  WHERE c1501_group_mapping_id = p_txn_id
                    AND t101.c1500_group_id = t1501.c1500_group_id);
   END gm_sav_void_group_mapping;

   /*******************************************************
   * Description : Procedure to fetch Group Mapping
   * Author    : Gopinathan
   *******************************************************/
   PROCEDURE gm_fch_user_group (
      p_grp_id            IN       t1501_group_mapping.c1500_group_id%TYPE,
      p_mapping_user_id   IN       t1501_group_mapping.c101_mapped_party_id%TYPE,
      p_user_id           IN       t1501_group_mapping.c1501_created_by%TYPE,
      p_grp_mapping_id    IN       VARCHAR2,
      p_dept              IN       VARCHAR2,
      p_compid			  IN	   t1900_company.C1900_COMPANY_ID%TYPE,
      p_cursor            OUT      TYPES.cursor_type
   )
   AS
   v_comp_lang_id T1900_COMPANY.C901_LANGUAGE_ID%TYPE;
   BEGIN  
   	   SELECT get_complangid_frm_cntx() 
	    INTO v_comp_lang_id 
	    FROM DUAL;
	    --For PMT-55180, Since the invalid number issue is throwing while executing
	    --due to to_number() function is not returning the number,
	    --So changing to_number() function to regexp_replace() function
      OPEN p_cursor
       FOR
          SELECT   t1501.c1501_group_mapping_id grpmappingid, pname rusernm,
                   t1501.c101_mapped_party_id usernm,
                   t1500.c1500_group_nm rgroupname,
                   t1501.c1500_group_id groupname,
                   t1501.c1900_company_id companyid,
                   get_company_name(t1501.c1900_company_id) companynm,
                   get_code_name_alt (dept) ruserdept,
                   NVL (t101p.c1500_group_id,
                        'No Default Group Mapped'
                       ) defaultgrp,
                   dept userdept,
                   DECODE (t101p.c1500_group_id,
                           t1501.c1500_group_id, 'Y',
                           'N'
                          ) defaultgrpforcb,
                     t101p.loginusernm        
              FROM (SELECT t101p.c101_party_id, t101p.c1500_group_id,
              			 --TO_NUMBER (SUBSTR (c1500_group_id, 0, 4)) dept,
                           regexp_replace(SUBSTR (c1500_group_id, 0, 4),'[^0-9]+', '') dept,
                   TRIM (DECODE (DECODE(v_comp_lang_id,'103097',NVL(C101_party_nm_en,C101_PARTY_NM),C101_PARTY_NM),
	               NULL,DECODE (c101_first_nm,NULL, '', ' ' || c101_first_nm)||' '||c101_last_nm || DECODE (c101_middle_initial,
	               NULL, '',' ' || c101_middle_initial),DECODE(v_comp_lang_id,'103097',NVL(C101_party_nm_en,C101_PARTY_NM),C101_PARTY_NM)
	                                 )
                                     ) pname,
                           t102.c102_login_username loginusernm
                      FROM t101_party t101p, t101_user t101, t102_user_login t102
                     WHERE t101p.c101_void_fl IS NULL
                       AND t101p.c101_party_id = t101.c101_party_id
                       AND t101.c101_user_id = t102.c101_user_id
                       ) t101p,
                   t1501_group_mapping t1501,
                   t1500_group t1500
             WHERE t101p.c101_party_id = t1501.c101_mapped_party_id(+)
               AND t1500.c1500_group_id = t1501.c1500_group_id
               AND t1501.c1501_group_mapping_id =
                          NVL (p_grp_mapping_id, t1501.c1501_group_mapping_id)
               AND t1501.c101_mapped_party_id =
                           NVL (p_mapping_user_id, t1501.c101_mapped_party_id)
               AND t1501.c1500_group_id = NVL (p_grp_id, t1501.c1500_group_id)
               AND NVL (dept, -999) = NVL (p_dept, NVL (dept, -999))
               AND NVL(t1501.c1900_company_id,-999) = NVL (p_compid, NVL(t1501.c1900_company_id,-999))
               AND t1501.c1501_void_fl IS NULL
               AND t1500.c1500_void_fl IS NULL
               AND t1500.c901_group_type = 92264
           ORDER BY rusernm,rgroupname, companyid ;
   END gm_fch_user_group;
   
   /*******************************************************
   * Description : Procedure to fetch user access and action control group
   * Author   : dreddy
   *******************************************************/
   PROCEDURE gm_fch_ac_action_group_list (p_cursor OUT TYPES.cursor_type)
   AS
   BEGIN
      OPEN p_cursor
       FOR
          SELECT   c1500_group_id groupid,
                   c1500_group_nm || ' (' || c1500_group_id || ')' groupname,
                   c1500_group_desc groupdesc ,GET_CODE_NAME(c901_group_type) grouptype,c901_group_type grtypeid
              FROM t1500_group t1500
             WHERE t1500.c1500_void_fl IS NULL
               --AND t1500.c901_group_type IN (SELECT c901_code_id from t901_code_lookup 
              -- WHERE c901_code_grp='FNTYP')
          ORDER BY c1500_group_nm;
   END gm_fch_ac_action_group_list;
/*******************************************************
   * Description : Procedure to save the Module Security Group Mapping
   * Author   : Rajkumar
   *******************************************************/
   PROCEDURE gm_sav_secmod_group (
      p_grp_id            IN       t1501_group_mapping.c1500_group_id%TYPE,
      p_mapping_group_id   IN       t1501_group_mapping.c1500_mapped_group_id%TYPE,
      p_user_id           IN       t1501_group_mapping.c1501_created_by%TYPE
   )
	AS
	v_grp_mapping_id t1501_group_mapping.c1501_group_mapping_id%TYPE;
	v_cnt	NUMBER;
   BEGIN
	   SELECT count(*) INTO v_cnt from t1501_group_mapping 
	   	WHERE c1500_group_id = p_grp_id AND c1500_mapped_group_id = p_mapping_group_id;
	   	IF v_cnt>0 then
	   		GM_RAISE_APPLICATION_ERROR('-20999','7','');
	   	END IF;

	   UPDATE t1501_group_mapping
         SET c1500_group_id = p_grp_id,
             c1500_mapped_group_id = p_mapping_group_id,
             c1501_last_updated_by = p_user_id,
             c1501_last_updated_date = CURRENT_DATE
       WHERE c1500_group_id = p_grp_id and c1500_mapped_group_id = p_mapping_group_id
         AND c1501_void_fl IS NULL;

      IF (SQL%ROWCOUNT = 0)
      THEN
         SELECT s1501_group_mapping.NEXTVAL
           INTO v_grp_mapping_id
           FROM DUAL;

         INSERT INTO t1501_group_mapping
                     (c1501_group_mapping_id, c1500_group_id,
                      c1500_mapped_group_id, c1501_created_by,
                      c1501_created_date
                     )
              VALUES (v_grp_mapping_id, p_grp_id,
                      p_mapping_group_id, p_user_id,
                      CURRENT_DATE
                     );
      END IF;
   END gm_sav_secmod_group;
   
  /*******************************************************
   * Description : Procedure to fetch Security group and module list
   * Author   : dreddy
   *******************************************************/
   PROCEDURE gm_fch_security_module_list (
   p_type   IN  t1500_group.c901_group_type%type,
   p_cursor OUT TYPES.cursor_type
   )
   AS
   BEGIN
      OPEN p_cursor
       FOR
          SELECT   c1500_group_id groupid,
                   c1500_group_nm groupname,
                   c1500_group_desc groupdesc ,GET_CODE_NAME(c901_group_type) grouptype, c901_group_type grtypeid
              FROM t1500_group t1500
             WHERE t1500.c1500_void_fl IS NULL
               AND t1500.c901_group_type =p_type
          ORDER BY c1500_group_nm;
   END gm_fch_security_module_list;
   
   /*******************************************************
   * Description : Procedure to fetch Module Security group Mapping list
   * Author   : dreddy
   *******************************************************/
   PROCEDURE gm_fch_sc_module_map_list (
   p_grp_id  IN t1500_group.c1500_group_id%TYPE,
   p_cursor OUT TYPES.cursor_type)
   AS
   BEGIN
      OPEN p_cursor
       FOR
         SELECT t1501.c1501_group_mapping_id grpmappingid,    
                t1501.c1500_group_id modulename,
                t1501.c1500_mapped_group_id groupname,
         		gm_pkg_it_user.GET_GROUP_NM(t1501.c1500_group_id) modulenm,
         		t1500.c1500_group_desc modgrpdesc,
         		gm_pkg_it_user.GET_GROUP_NM(t1501.c1500_mapped_group_id) groupnm,
         		gm_pkg_it_user.GET_GROUP_DESC(t1501.c1500_mapped_group_id) groupdesc
		  FROM t1500_group t1500, t1501_group_mapping t1501
         WHERE t1501.c1500_group_id= DECODE(p_grp_id,'',t1500.c1500_group_id,p_grp_id)
           AND t1500.c901_group_type='92265'
           AND t1501.c1501_void_fl IS NULL
      ORDER BY modulenm;
   END gm_fch_sc_module_map_list;
   /*******************************************************
   * Description : Procedure to fetch Security group Module Mapping list
   * Author   : dreddy
   *******************************************************/
   PROCEDURE gm_fch_module_sc_map_list (
   p_grp_id  IN t1500_group.c1500_group_id%TYPE,
   p_cursor OUT TYPES.cursor_type)
   AS
   BEGIN
      OPEN p_cursor
       FOR
         SELECT t1501.c1501_group_mapping_id grpmappingid,    
                t1501.c1500_group_id modulename,
                t1501.c1500_mapped_group_id groupname,
         		gm_pkg_it_user.GET_GROUP_NM(t1501.c1500_group_id) modulenm,
         		t1500.c1500_group_desc modgrpdesc,
         		gm_pkg_it_user.GET_GROUP_NM(t1501.c1500_mapped_group_id) groupnm,
         		gm_pkg_it_user.GET_GROUP_DESC(t1501.c1500_mapped_group_id) groupdesc
		  FROM t1500_group t1500, t1501_group_mapping t1501
         WHERE t1501.c1500_mapped_group_id= DECODE(p_grp_id,'',t1500.c1500_group_id,p_grp_id)
           AND t1500.c901_group_type='92264'
           AND t1501.c1501_void_fl IS NULL
      ORDER BY modulenm;
   END gm_fch_module_sc_map_list;
/********************************************************
	* Description: This Procedure will Save and Void the User  Security Group Mappings
	* Author: Rajkumar
	********************************************************/
   PROCEDURE gm_upd_user_group_map(   
   	p_inpstr	IN	VARCHAR2,
   	p_opt		IN	VARCHAR2,
   	p_id		IN 	VARCHAR2,
   	p_user_id   IN  t1501_group_mapping.c1501_created_by%TYPE,
   	p_compid	IN	   t1900_company.C1900_COMPANY_ID%TYPE,
   	p_default_grp IN T101_PARTY.C1500_GROUP_ID%TYPE DEFAULT NULL	
   )
   AS
   	v_inpstr	VARCHAR2(2000) := p_inpstr;
   	v_substring VARCHAR2(2000);
   	v_string 	VARCHAR2(2000):= p_inpstr;
   	v_grp_id	VARCHAR2(20) := '';
   	v_cnt	NUMBER :=0;
   	v_comptype number;
   	v_grp_mapping_id t1501_group_mapping.c1501_group_mapping_id%TYPE;
   BEGIN
	v_inpstr := substr(v_inpstr,0,length(v_inpstr)-1);
	my_context.set_my_inlist_ctx (v_inpstr);
	
	IF p_opt = 'SECUSR'
	THEN
		UPDATE T1501_GROUP_MAPPING SET C1501_VOID_FL = 'Y',
				C1501_LAST_UPDATED_BY = p_user_id,
				C1501_LAST_UPDATED_DATE = CURRENT_DATE 
		WHERE C1500_GROUP_ID = p_id
		AND C101_MAPPED_PARTY_ID NOT IN (SELECT *
                                      FROM v_in_list)
		AND C1501_VOID_FL IS NULL
		AND c1900_company_id = p_compid;
	ELSE
	
		  UPDATE T1501_GROUP_MAPPING
			SET C1501_VOID_FL            = 'Y', C1501_LAST_UPDATED_BY = p_user_id, C1501_LAST_UPDATED_DATE = CURRENT_DATE
			  WHERE C101_MAPPED_PARTY_ID = p_id
			    AND C1500_GROUP_ID      IN
			    (
			         SELECT T1500.C1500_GROUP_ID
			           FROM T1500_GROUP T1500
			          WHERE T1500.C1500_GROUP_ID NOT IN
			            (
			                 SELECT TOKEN FROM v_in_list
			            )
			            AND T1500.C901_GROUP_TYPE = '92264'
			    )
			    AND C1501_VOID_FL IS NULL
			    AND c1900_company_id = p_compid;			
	END IF;
	 
	WHILE INSTR (v_string, ',') <> 0
    LOOP
         v_substring := SUBSTR (v_string, 1, INSTR (v_string, ',') - 1);
         v_string := SUBSTR (v_string, INSTR (v_string, ',') + 1);

         /*When user is mapped to security group with company, map that company as associated company.
         if asst. company is voided, unvoid it or create new record.
         */
       
         IF p_opt = 'SECUSR'
         THEN
         
        		gm_pkg_cm_accesscontrol.gm_save_user_group(
        													p_id,
        													v_substring,
        													p_user_id,
        													p_compid
        													);
        		SELECT DECODE(gm_pkg_it_user.get_default_party_company(v_substring),p_compid, 105400,105401)
        		  INTO v_comptype 
        		  FROM DUAL;
        		gm_pkg_cm_party.gm_sav_party_company_mapping(v_substring, p_compid, v_comptype, p_user_id);											
         ELSE
         
        		gm_pkg_cm_accesscontrol.gm_save_user_group(
        													v_substring,
        													p_id,
        													p_user_id,
        													p_compid
        													);															
         END IF;
    END LOOP;
    IF p_opt != 'SECUSR' 
    THEN
    	
    	 SELECT DECODE(gm_pkg_it_user.get_default_party_company(p_id),p_compid, 105400,105401)
        		  INTO v_comptype 
        		  FROM DUAL;        													
         gm_pkg_cm_party.gm_sav_party_company_mapping(p_id, p_compid, v_comptype, p_user_id);
    END IF;     
	END gm_upd_user_group_map;
	/*
	 * Description: Will Save the new Sec-User Mapping and it will skip already mapped 
	 * Author: Rajkumar
	 */
	PROCEDURE gm_save_user_group(
		p_grpid		IN	t1501_group_mapping.c1500_group_id%TYPE,
		p_partyid	IN	t1501_group_mapping.c101_mapped_party_id%TYPE,
		p_user_id	IN	t1501_group_mapping.c1501_created_by%TYPE,
		p_compid	IN	t1900_company.C1900_COMPANY_ID%TYPE
	)
	AS
	v_cnt	NUMBER;
	v_grp_mapping_id	t1501_group_mapping.c1501_group_mapping_id%TYPE;
	BEGIN
		SELECT COUNT(1) INTO v_cnt 
          FROM T1501_GROUP_MAPPING 
         WHERE c1500_group_id = p_grpid 	
		   AND c101_mapped_party_id = p_partyid
		   AND c1900_company_id = p_compid
		   AND C1501_VOID_FL IS NULL;
		
	      IF (v_cnt = 0)
	      THEN
	         SELECT s1501_group_mapping.NEXTVAL
	           INTO v_grp_mapping_id
	           FROM DUAL;
	
	         INSERT INTO t1501_group_mapping
	                     (c1501_group_mapping_id, c1500_group_id,
	                      c101_mapped_party_id, c1501_created_by,
	                      c1501_created_date, c1900_company_id ,C1501_LAST_UPDATED_BY   
	                     )
	              VALUES (v_grp_mapping_id,p_grpid ,
	                      p_partyid, p_user_id,
	                      CURRENT_DATE, p_compid ,p_user_id
	                     );	
		  END IF;		                     
	END gm_save_user_group;
/*
 * Description: This Procedure will be the Replacement of the getPartyList() Procedure, will return the ArrayList, if the party is mapped with the Security Event
 * Author: Rajkumar
 */	
	PROCEDURE gm_fch_party_accesspermissions(
	 p_partyid         IN       t1530_access.c101_party_id%TYPE,
     p_functionid      IN       t1530_access.c1520_function_id%TYPE,
     p_out			   OUT      TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_out FOR
		SELECT t1530.c1530_access_id ID,
                 t1530.c1530_update_access_fl updfl,
                 t1530.c1530_read_access_fl readfl,
                 t1530.c1530_void_access_fl voidfl
            FROM t1530_access t1530
           WHERE (    t1530.c1530_void_fl IS NULL
                  AND (   t1530.c101_party_id = p_partyid
                       OR t1530.c1500_group_id IN (
                             SELECT NVL (t1501.c1500_group_id,
                                         t1501.c1500_mapped_group_id
                                        )
                               FROM t1501_group_mapping t1501,
                                    t1530_access t1530
                              WHERE t1501.c101_mapped_party_id = p_partyid
                                AND t1530.c1500_group_id =
                                       NVL (t1501.c1500_group_id,
                                            t1501.c1500_mapped_group_id
                                           )
                                 and c1501_void_fl is null)
                      )
                  AND (   t1530.c1520_function_id = p_functionid
                       OR t1530.c1520_function_id IN (
                             SELECT t1520.c1520_function_id
                               FROM t1520_function_list t1520
                              WHERE t1520.c1520_inherited_from_id =
                                                                  p_functionid)
                      )
                 ) AND t1530.c1530_read_access_fl = 'Y';
	END gm_fch_party_accesspermissions;


/*************************************************************************************
 * Description	:  This Procedure will be get access flag for mapped users only, 
 * 				   if the party is mapped with the Security Event
 * Author		:  Velu
 *************************************************************************************/	
	PROCEDURE gm_fch_user_accesspermission(
	 p_partyid         IN       t1530_access.c101_party_id%TYPE,
     p_functionid      IN       t1530_access.c1520_function_id%TYPE,
     p_group_id      IN       t1530_access.c1500_group_id%TYPE,
     p_out			   OUT      TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_out FOR
		SELECT t1530.c1530_access_id ID,
                 t1530.c1530_update_access_fl updfl,
                 t1530.c1530_read_access_fl readfl,
                 t1530.c1530_void_access_fl voidfl
            FROM t1530_access t1530
           WHERE (    t1530.c1530_void_fl IS NULL
           			AND t1530.c1500_group_id = p_group_id
                  AND (   t1530.c101_party_id = p_partyid
                       OR t1530.c1500_group_id IN (
                             SELECT NVL (t1501.c1500_group_id,
                                         t1501.c1500_mapped_group_id
                                        )
                               FROM t1501_group_mapping t1501,
                                    t1530_access t1530
                              WHERE t1501.c101_mapped_party_id = p_partyid
                                AND t1530.c1500_group_id =
                                       NVL (t1501.c1500_group_id,
                                            t1501.c1500_mapped_group_id
                                           )
                                 and c1501_void_fl is null
                                 AND t1530.c1500_group_id = p_group_id)
                      )
                  AND (   t1530.c1520_function_id = p_functionid
                       OR t1530.c1520_function_id IN (
                             SELECT t1520.c1520_function_id
                               FROM t1520_function_list t1520
                              WHERE t1520.c1520_inherited_from_id =
                                                                  p_functionid)
                      )
                 ) AND t1530.c1530_read_access_fl = 'Y';
	END gm_fch_user_accesspermission;


/*************************************************************************************
 * Description	:  This Procedure is used to get the security event details based on 
 *                 party id and page names 
 * Author		:  Tejeswara Reddy
*************************************************************************************/
   PROCEDURE gm_fch_security_events(
	 p_partyid         IN       T101_USER.C101_PARTY_ID%TYPE,
     p_page_names      IN       VARCHAR2,
     p_out			   OUT      TYPES.cursor_type
	)
	AS
	BEGIN
	my_context.set_my_inlist_ctx (p_page_names);
	 OPEN p_out FOR
		SELECT  T.EVENTNAME EVENTNAME
		      , T.TAGNAME TAGNAME
		      , T.PAGENAME PAGENAME
		      , T.UPDATEACCESSFLG UPDATEACCESSFLG
		      , T.READACCESSFLG READACCESSFLG
		      , T.VOIDACCESSFLG VOIDACCESSFLG
		      , DECODE(T.ACCESSVOIDFLG,'Y','Y',NULL) ACCESSVOIDFLG
		      , DECODE(T.GROUPMAPPINGVOIDFLG,'Y','Y',NULL) GROUPMAPPINGVOIDFLG
		      , DECODE(T.RULESVOIDFLG,'Y','Y',NULL) RULESVOIDFLG
         FROM (SELECT t906_pages.c906_rule_grp_id EVENTNAME
                     , t906_pages.C906_RULE_ID TAGNAME
                     , t906_pages.C906_RULE_VALUE PAGENAME
		             , MAX(NVL(t1530.c1530_update_access_fl,'N')) UPDATEACCESSFLG
		             , MAX(NVL(t1530.c1530_read_access_fl,'N')) READACCESSFLG
		             , MAX(NVL(t1530.c1530_void_access_fl,'N')) VOIDACCESSFLG
                     , MIN(NVL(t1530.c1530_void_fl,'N')) ACCESSVOIDFLG 
                     , MIN(NVL(t1501.c1501_void_fl,'N')) GROUPMAPPINGVOIDFLG
		             , MIN(NVL(t906_pages.c906_void_fl,'N')) RULESVOIDFLG
		        FROM  t906_rules t906_mobile
                    , t906_rules t906_pages
                    , t1530_access t1530 
                    , t1501_group_mapping t1501
		       WHERE t906_mobile.c906_rule_grp_id = 'MOBILESECURITYEVENTS'
                 AND t906_mobile.c906_rule_id  = t906_pages.c906_rule_grp_id
                 AND t906_pages.C906_RULE_VALUE IN (select token from v_in_list)  
                 AND t906_pages.c906_rule_grp_id = t1530.c1520_function_id
                 AND t1530.c1500_group_id = t1501.c1500_group_id
                 AND t1501.c101_mapped_party_id =  p_partyid   
		      GROUP BY  t906_pages.c906_rule_grp_id
                      , t906_pages.C906_RULE_ID
                      , t906_pages.C906_RULE_VALUE
		 )T;    
		 
	END gm_fch_security_events;

/**************************************************************************************************
 * Description	:  This Procedure is used to get the User Access Information based on the User ID 
 * Author		:   Suganthi
***************************************************************************************************/
PROCEDURE gm_fch_multi_event_access
  ( 
  p_userid         IN      t102_user_login.c101_user_id%type,
  p_out_cur         OUT      TYPES.CURSOR_TYPE
  )
  AS
  v_partyid       t101_party.c101_party_id%type;
  v_company_id    t101_party.c1900_company_id%type;
  BEGIN
  	BEGIN      
      SELECT c101_party_id, DECODE(company_id,0,c1900_company_id,company_id)
                INTO v_partyid, v_company_id
                 FROM(
                 SELECT T101.C101_PARTY_ID,gm_pkg_it_user.get_default_party_company(t101.c101_party_id) company_id,
                  t101a.c1900_company_id 
                FROM t101_user t101,
                  t101_party t101a
                WHERE T101.C101_USER_ID = P_USERID
                AND t101a.c101_party_id = t101.c101_party_id);
       EXCEPTION WHEN NO_DATA_FOUND THEN
         v_partyid := NULL;
         v_company_id := NULL;
  	END;
      OPEN P_OUT_CUR FOR 
				  SELECT *
					FROM
					  (SELECT t1530.c1520_function_id funcid,
					    t906.c906_rule_value funcnm,
					    t1530.c1530_update_access_fl updateacc,
					    t1530.c1530_read_access_fl readacc,
					    t1530.c1530_void_access_fl voidacc
					  FROM t1530_access t1530,
					    t906_rules t906
					  WHERE ( t1530.c1530_void_fl IS NULL
					  AND t906.c906_rule_id        = T1530.C1520_FUNCTION_ID
					  AND ( t1530.c101_party_id    = v_partyid
					  OR t1530.c1500_group_id     IN
					    (SELECT NVL (t1501.c1500_group_id, t1501.c1500_mapped_group_id )
					    FROM t1501_group_mapping t1501,
					      t1530_access t1530
					    WHERE t1501.c101_mapped_party_id = v_partyid
					    AND t1530.c1500_group_id         = NVL (t1501.c1500_group_id, t1501.c1500_mapped_group_id )
					    AND c1501_void_fl               IS NULL
					    AND t1530.c1530_void_fl IS NULL 
					    AND t1501.c1900_company_id = v_company_id
					    ) )
					  AND ( t1530.c1520_function_id IN
					    (SELECT c906_rule_id
					    FROM t906_rules
					    WHERE c906_rule_grp_id = 'MOBILE_APP_ACCESS' 
					    AND c1900_company_id = v_company_id
					    AND c906_void_fl      IS NULL
					    )
					  OR t1530.c1520_function_id IN
					    (SELECT t1520.c1520_function_id
					    FROM t1520_function_list t1520
					    WHERE T1520.C1520_INHERITED_FROM_ID IN
					      (SELECT c906_rule_id
					      FROM t906_rules
					      WHERE c906_rule_grp_id = 'MOBILE_APP_ACCESS' 
					      AND c1900_company_id = v_company_id
					      AND c906_void_fl      IS NULL
					      )
					    ) ) )
					  UNION ALL
					  SELECT c906_rule_id,
					    c906_rule_value,
					    'Y',
					    'Y',
					    'Y'
					  FROM T906_RULES
					  WHERE c906_rule_grp_id = 'MOBILE_APP_ACCESS'
					  AND c906_rule_id       = 'DFL_APP_ACCESS' 
					  AND c1900_company_id = v_company_id
					  AND c906_void_fl      IS NULL
					  UNION ALL
					  SELECT c906_rule_id,
					    C906_Rule_Value,
					    'N',
					    'N',
					    'N'
					  From T906_Rules
					  WHERE c906_rule_grp_id = 'EXCL_APP_MBL_VIEW'
					  AND c906_rule_id       = 'MOBILE_VIEW_APP' 
					  AND c1900_company_id = v_company_id
					  AND c906_void_fl      IS NULL
					  )
					GROUP BY funcid,
					  funcnm,
					  updateacc,
					  readacc,
					  voidacc;
  
  END GM_FCH_MULTI_EVENT_ACCESS;

  /***************************************************************************************
 * Description	:  This Procedure is used to get the Account Id based on the UserId
 * Author		:  Suganthi Sharmila
***************************************************************************************/
  PROCEDURE GM_FCH_APP_VIEW_ACCESS(
    P_USERID    IN T102_USER_LOGIN.C101_USER_ID%type, 
    P_ACCESSLVL IN T101_USER.C101_ACCESS_LEVEL_ID%TYPE,
    P_view_out  OUT types.CURSOR_TYPE,
    p_access_out OUT TYPES.cursor_type)
AS
begin

	MY_CONTEXT.SET_MY_DOUBLE_INLIST_CTX(P_USERID,P_ACCESSLVL);
	
  open P_view_out for 
  SELECT AC_ID FROM V700A_APP_ACCESS;
  
   OPEN p_access_out FOR 
  SELECT C906_RULE_GRP_ID funcid,C906_RULE_VALUE funcnm FROM T906_RULES
  WHERE C906_RULE_GRP_ID IN ('ITEM_APP_ACCESS','SET_APP_ACCESS','LOANER_APP_ACCESS','ORDER_APP_ACCESS')
  AND c906_rule_id = P_ACCESSLVL
  AND C906_VOID_FL IS NULL;
  
END gm_fch_app_view_access;

/***************************************************************************************
 * Description	:  This Procedure is used to get the Log details based on the UserId,grpid,
 * 				   cmpanyid
 * Author		:  Prabhuvigneshwaran M D 
***************************************************************************************/   
     PROCEDURE gm_fch_log_details(
    P_GRP_ID   IN T1501L_GROUP_MAP_LOG.C1501L_GROUP_ID%type,
    P_PARTY_ID  IN T1501L_GROUP_MAP_LOG.C1501L_MAPPED_PARTY_ID%TYPE,
    P_COMPANY_ID  IN T1501L_GROUP_MAP_LOG.C1900_COMPANY_ID%TYPE,
    p_out OUT TYPES.cursor_type)
    AS
    	v_date_fmt    VARCHAR2 (20);
    BEGIN
   
	   SELECT get_compdtfmt_frm_cntx() 
	    INTO v_date_fmt   FROM dual; 
	     OPEN p_out FOR 
	    SELECT 
	    get_party_name(t1501L.C1501L_MAPPED_PARTY_ID) USER_NAME,
	    t1501l.C1501L_MAPPED_PARTY_ID PARTY_ID,
	    t1501l.C1501L_ACTION_FL ACTION_FL,
	    GET_COMPANY_NAME(t1501l.C1900_COMPANY_ID) CMPY_NM,
	    T1500.C1500_GROUP_NM GRP_NM,
	    GET_USER_NAME(t1501L.C1501L_CREATED_BY) UPDATED_BY,
		TO_CHAR(t1501l.C1501L_CREATED_DATE,v_date_fmt||' HH:MI:SS AM') UPDATED_DATE
		FROM T1501L_GROUP_MAP_LOG t1501l,T1500_GROUP t1500
		WHERE T1500.C1500_GROUP_ID = T1501L.C1501L_GROUP_ID 
        AND T1501L.C1501L_MAPPED_PARTY_ID = NVL(P_PARTY_ID,T1501L.C1501L_MAPPED_PARTY_ID)
        AND t1501l.c1501l_group_id = NVL(P_GRP_ID,  t1501l.c1501l_group_id)
        AND t1501l.C1501L_VOID_FL IS NULL
        AND t1500.C1500_VOID_FL IS NULL
        AND t1501l.C1900_COMPANY_ID= P_COMPANY_ID
        ORDER BY t1501l.C1501L_CREATED_DATE DESC;
    END gm_fch_log_details;

END gm_pkg_cm_accesscontrol;
/
