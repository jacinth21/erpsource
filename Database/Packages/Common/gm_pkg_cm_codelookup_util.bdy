/* Formatted on 2009/06/30 10:42 (Formatter Plus v4.8.0) */
--@"c:\database\packages\common\gm_pkg_cm_codelookup_util.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_cm_codelookup_util
IS
--
	/******************************************************************
	* Description : Procedure to fetch the code list with out active flag check.
	* Author		: Ritesh
	****************************************************************/
	PROCEDURE gm_cm_fch_allcodelist (
		p_grp		IN		 t901_code_lookup.c901_code_grp%TYPE
	  , p_outlist	OUT 	 TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_outlist
		 FOR
			 SELECT  t901.c901_code_nm codenm, t901.c901_code_id codeid
				 FROM t901_code_lookup t901
				WHERE t901.c901_code_grp = p_grp
				AND C901_ACTIVE_FL = '1'
			 ORDER BY t901.c901_code_seq_no;
	END gm_cm_fch_allcodelist;

	/******************************************************************
	* Description : Procedure to fetch the code list for the group
					which is the alt name for other code id.
					Ex. for the code group ADDCH there are some codelookup ids we have.
						Some of them have alt name like DMGRP, BHGRP etc...which are
						the code group for some other code lookup values.
						To fetch the code id and the codename for the second code group
						this procedure will be helpful.
	* Author		: Ritesh
	****************************************************************/
	PROCEDURE gm_cm_fch_codelistforalt (
		p_inputstr	 IN 	  VARCHAR2
	  , p_outlist	 OUT	  TYPES.cursor_type
	)
	AS
	BEGIN
		my_context.set_my_inlist_ctx (p_inputstr);

		OPEN p_outlist
		 FOR
			 SELECT   t901.c901_code_id codeid, t901.c901_code_nm codenm
					, t901a.c901_code_nm || ' - ' || t901.c901_code_nm codenmalt
				 FROM t901_code_lookup t901, t901_code_lookup t901a, v_in_list v_list
				WHERE t901a.c901_code_id = v_list.token AND t901.c901_code_grp = t901a.c902_code_nm_alt
			 ORDER BY t901.c901_code_seq_no;
	END gm_cm_fch_codelistforalt;

	/******************************************************************
	* Description : Procedure to fetch the code alt name as id.
	* Author		: Ritesh
	****************************************************************/
	PROCEDURE gm_cm_fch_allcodealtnmlist (
		p_grp		IN		 t901_code_lookup.c901_code_grp%TYPE
	  , p_outlist	OUT 	 TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_outlist
		 FOR
			 SELECT   t901.c902_code_nm_alt codeid, t901.c901_code_nm codenm
				 FROM t901_code_lookup t901
				WHERE t901.c901_code_grp = p_grp
			 ORDER BY t901.c901_code_seq_no;
	END gm_cm_fch_allcodealtnmlist;
END gm_pkg_cm_codelookup_util;
/
