CREATE OR REPLACE PACKAGE BODY gm_pkg_oppr_ttp_common
IS
 
  /*************************************************************************
  * Description : This procedure used to process no of working days by month    
  * 			  based on Year
  * Author      :
  *************************************************************************/
PROCEDURE gm_process_company_working_day_by_mon(
      p_comp_id  		IN   NUMBER,
      p_plant_id        IN   NUMBER,
      p_year        	IN	 VARCHAR2,
      p_user_id         IN NUMBER)
      
 AS
 --Get company id,month,working days from v1900_company_working_days
 cursor get_workingdays_cur
 IS
     SELECT p_comp_id company_id,
     		TRUNC(CAL_DATE,'month') cal_month,
     		count(1) total_working_days
     FROM v1900_company_working_days
     GROUP BY TRUNC(CAL_DATE,'month')
     ORDER BY cal_month;
 
 BEGIN
	 --set context as plant id and company id
	 gm_pkg_cor_client_context.gm_sav_client_context(p_comp_id,p_plant_id);
	 --set inlinst context
	 my_context.set_my_inlist_ctx (p_year);
	 --Inside loop save total number of working days in t1904 table
	 FOR get_workingdays IN get_workingdays_cur
	 LOOP
	 
	 	gm_sav_company_working_day_by_mon(get_workingdays.company_id,get_workingdays.cal_month,get_workingdays.total_working_days,p_user_id);
	 	
	 END LOOP;	 
END gm_process_company_working_day_by_mon;
  /*************************************************************************
  * Description : This Procedure used to save no of working days in T1904 table    
  * Author   :
  *************************************************************************/
PROCEDURE gm_sav_company_working_day_by_mon(
      p_comp_id  		IN   NUMBER,
      p_cal_month       IN	 DATE,
      p_total_working_days IN NUMBER,
      p_user_id         IN NUMBER)
      
 AS
 
 BEGIN
	 --Update t1904 table
	 UPDATE t1904_company_working_day_by_mon
		SET c1904_total_no_working_days = p_total_working_days,
			c1904_last_updated_by=p_user_id,
			c1904_last_updated_date=current_date
		WHERE c1904_month_date = p_cal_month
		AND c1900_company_id = p_comp_id;
		
		IF (SQL%ROWCOUNT = 0) THEN
		INSERT INTO T1904_COMPANY_WORKING_DAY_BY_MON (C1900_COMPANY_ID,C1904_MONTH_DATE,C1904_TOTAL_NO_WORKING_DAYS)
			VALUES(p_comp_id,p_cal_month,p_total_working_days);
		END IF;
		
END gm_sav_company_working_day_by_mon;


/**********************************************************
    * This procedure used to find no.of working days for month
 ***********************************************************/
	PROCEDURE get_no_working_day_by_mon (
		p_load_date	IN DATE,
		p_company_id IN t1904_company_working_day_by_mon.c1900_company_id%type,
        p_total_working_days    OUT NUMBER
    )
	AS
		v_total_working_days NUMBER;
	BEGIN
	--This select used to get number of working days based on month
		BEGIN 
			
			SELECT c1904_total_no_working_days 
			  INTO v_total_working_days
	 		 FROM t1904_company_working_day_by_mon
    		WHERE c1904_month_date = p_load_date
    		AND  c1900_company_id = p_company_id;
    			 
    	EXCEPTION WHEN  NO_DATA_FOUND  THEN
    			v_total_working_days := NULL; 
    	END;
	p_total_working_days := v_total_working_days;
	
	END get_no_working_day_by_mon;
END gm_pkg_oppr_ttp_common;