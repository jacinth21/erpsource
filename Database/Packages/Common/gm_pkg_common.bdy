* Formatted on 2010/10/12 11:08 (Formatter Plus v4.8.0) */
--@"c:\database\packages\common\gm_pkg_common.bdy";
CREATE OR REPLACE
PACKAGE BODY gm_pkg_common
IS
    /******************************************************************
    * Description : This Procedure is used to fetch the month list from V9001_MONTH_LIST
    * Author    : Brinal G
    ****************************************************************/
PROCEDURE gm_fch_date_lst (
        p_months OUT TYPES.cursor_type)
AS
BEGIN
    OPEN p_months FOR SELECT TO_CHAR (month_value, 'mm/dd/yyyy') MONTH, INITCAP (TO_CHAR (month_value, 'mon')) || ' '
    || TO_CHAR (month_value, 'YYYY') mmname FROM v9001_month_list WHERE month_value > ADD_MONTHS (SYSDATE, - 12) ; --
    -- sysdate
    -- for now doing -12 later(2/3 months aft production move) the below commented lines will be uncommented.
    -- OR (  TO_CHAR (month_value, 'mm') = TO_CHAR (SYSDATE, 'mm')
    -- AND TO_CHAR (month_value, 'yyyy') = TO_CHAR (SYSDATE, 'yyyy')
    -- );
END gm_fch_date_lst;
/***************************************************************
* Description : This Procedure is used to fetch the cc email ids
*    from contact for the given sales rep.
* Author    : VPrasath
****************************************************************/
PROCEDURE gm_fch_cc_from_contact (
        p_rep_id IN VARCHAR2,
        p_job_name IN VARCHAR2,
        p_out_email_ids OUT VARCHAR2)
AS
    v_code_id       NUMBER;
    v_out_email_ids VARCHAR2 (4000) := '';
    CURSOR v_cursor
    IS
         SELECT     t107.c107_contact_value email
               FROM t703_sales_rep t703, t107_contact t107
              WHERE t703.c101_party_id = t107.c101_party_id
                AND t703.c703_sales_rep_id = p_rep_id
                AND t107.c901_mode = v_code_id
                AND t107.c107_void_fl IS NULL;
BEGIN
     SELECT     TO_NUMBER (t906.c906_rule_id)
           INTO v_code_id
           FROM t906_rules t906
          WHERE t906.c906_rule_grp_id = 'EMAIL'
            AND t906.c906_rule_value = p_job_name;
    FOR v_index IN v_cursor
    LOOP
        v_out_email_ids := v_index.email || ',' || v_out_email_ids;
    END LOOP;
    p_out_email_ids := v_out_email_ids;
END gm_fch_cc_from_contact;
/***************************************************************
* Description : This Procedure is used to check the entered
*    control number is there in DHR
* Author    : VPrasath
****************************************************************/
PROCEDURE gm_chk_cntrl_num_exists (
        p_pnum IN t2550_part_control_number.c205_part_number_id%TYPE,
        p_cnum IN t2550_part_control_number.c2550_control_number%TYPE,
        pout OUT VARCHAR2)
AS
    v_count NUMBER;
    v_cnum t2550_part_control_number.c2550_control_number%TYPE;
    v_cnumFmt   VARCHAR2 (20) ;
    v_count_sales NUMBER;
    v_order_id T501_ORDER.c501_order_id%TYPE;
BEGIN
    BEGIN

        v_cnum := UPPER (TRIM (p_cnum)) ;
        -- if part does not have the attribute value Accept NOC# Control number value  is YES and Control Number is
        -- NOC# then  raise error message .
        --80133 � code Grp for Accept NOC# , 80130 � code Id for Yes dropdown value
        -- excluding Literature(4056) product family parts from NOC# validation
        IF get_part_attribute_value (p_pnum, '80133') != '80130' AND get_partnum_product(p_pnum) != '4056' AND v_cnum = 'NOC#' THEN
            v_count := 0;
        ELSIF v_cnum != 'NOC#' THEN
            --80140 � code Id for control number format value
            v_cnumFmt := UPPER (get_part_attribute_value (p_pnum, 80134)) ;
          -- ATEC-161: Commented below as we are doing the equal search for the control number
          --  IF v_cnumFmt = 'XXXXXX' OR v_cnumFmt = 'XXXXXX%' THEN
             --   v_cnum := SUBSTR (v_cnum, 1, 6) || '%' ;
        --    END IF;
             
            SELECT COUNT (1)
			  INTO v_count
			FROM t2550_part_control_number
			WHERE c205_part_number_id = p_pnum
			   --AND TRUNC(c2550_expiry_date) >= TRUNC(CURRENT_DATE)
			AND UPPER (TRIM (c2550_control_number)) = UPPER(v_cnum) ; 
         
        END IF;
        IF v_count = 0 THEN       
            pout := 'The Control Number ' || p_cnum || ' entered for the part number ' || p_pnum || ' is not valid' ;
        END IF;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
           pout := 'The Control Number ' || p_cnum || ' entered for the part number ' || p_pnum || ' is not valid';
    END;
END gm_chk_cntrl_num_exists;

/***************************************************************
* Description : This Procedure is used to check the entered
*    control number is there in allow NOC#
* Author    : Velu
****************************************************************/
PROCEDURE gm_chk_cntrl_num (
    p_pnum 		IN 	t2550_part_control_number.c205_part_number_id%TYPE,
    p_cnum 		IN 	t2550_part_control_number.c2550_control_number%TYPE,
    p_txntype 	IN 	VARCHAR2,
    p_company_id IN t1900_company.c1900_company_id%TYPE,
    pout 		OUT VARCHAR2  
)
AS
    v_rulevalue VARCHAR2(20);
    v_company_id T1900_COMPANY.C1900_COMPANY_ID%TYPE;
    v_lot_track_fl   VARCHAR2(3);
    v_disable_lot_fl VARCHAR2(2);
    v_product_family t205_part_number.c205_product_family%TYPE;
    v_cnum t2550_part_control_number.c2550_control_number%TYPE;
    v_part_material   t205_part_number.c205_product_material%TYPE;
BEGIN
	v_cnum := TRIM(p_cnum);
   SELECT GET_RULE_VALUE ('ALLOW_NOC', p_txntype) INTO v_rulevalue FROM DUAL;
   
   SELECT get_partnum_product(p_pnum) INTO v_product_family FROM DUAL;

   SELECT get_compid_frm_cntx() 
   	INTO v_company_id 
   		FROM DUAL;
   		
   		/*if Part number is released for lot track and if the transactions [p_txntype] available in 'DISABLETRACK' rule group
   		 * then RETURN from this PRC
   		 * */
   IF UPPER(p_cnum) = 'NOC#' THEN
   	   SELECT get_part_attr_value_by_comp(p_pnum,'104480',v_company_id)
   	   	INTO v_lot_track_fl 
   	   		FROM DUAL; 
   	   		
   	   SELECT get_rule_value(p_txntype,'DISABLETRACK') 
   	   	INTO v_disable_lot_fl 
   	   		FROM DUAL;

   	   IF(v_lot_track_fl = 'Y' AND v_disable_lot_fl = 'Y') THEN
   	   	RETURN;
   	   END IF;
   END IF;
   -- ALW_INS_PART_NOC -Controlling the instrument part to allow NOC#
   IF v_rulevalue='YES' AND UPPER(p_cnum) ='NOC#' THEN 
   		v_rulevalue:= 'NO';
   ELSIF get_rule_value_by_company(v_product_family,'ALW_INS_PART_NOC',NVL(p_company_id,v_company_id))='Y' 
		AND UPPER(v_cnum) ='NOC#'  THEN
   		v_rulevalue:= 'NO';
   ELSE
   		gm_chk_cntrl_num_exists(p_pnum,p_cnum,pout);
   END IF;
   
  --PMT-47523 PNQN transaction should not accept NOC# as control number in GMNA for tissue parts 
  BEGIN
  SELECT get_partnum_material_type(p_pnum) INTO v_part_material FROM DUAL; --checks if the part is a tissue part
     EXCEPTION
       WHEN NO_DATA_FOUND THEN   
          v_part_material:= NULL;
   END;
   IF get_rule_value_by_company((v_part_material||'-'||p_txntype),'VALID_PART_NOC',NVL(p_company_id,v_company_id)) = 'Y' AND UPPER(v_cnum) = 'NOC#'  THEN  
      pout := 'The Control Number ' || p_cnum || ' entered for the part number ' || p_pnum || ' is not valid';
   END IF; 
END gm_chk_cntrl_num;

/***************************************************************
* Description : This Procedure is used to check the RFS country
for the part is there in the attribute table
* Author    : Xun
****************************************************************/
PROCEDURE gm_chk_rfs_country (
        p_num IN t408_dhr.c205_part_number_id%TYPE,
        p_countryid IN t901_code_lookup.c901_code_id%TYPE,
        p_out_message OUT VARCHAR2)
AS
    v_count      NUMBER;
    v_pnum_count NUMBER;
BEGIN
	--Added void flag condition for MNTTASK-6165
     SELECT     COUNT (1)
           INTO v_count
           FROM t205d_part_attribute
          WHERE c205_part_number_id = p_num
            AND c901_attribute_type = 80180
            AND c205d_void_fl is NULL;
     SELECT     COUNT (1)
           INTO v_pnum_count
           FROM t205_part_number
          WHERE c205_part_number_id = p_num
           AND c205_active_fl='Y'; ---RFS;
    IF v_pnum_count = 0 THEN
        p_out_message := 'The Part Number ''' || p_num || ''' does not exist ';
    ELSIF v_count = 0 THEN
        p_out_message := 'The Part Number ''' || p_num || ''' is not released for sale ';
    ELSE
         SELECT     COUNT (1)
               INTO v_count
               FROM DUAL
              WHERE get_mapping_countryid (to_number (p_countryid)) IN
                (SELECT     c205d_attribute_value
                       FROM t205d_part_attribute
                      WHERE c205_part_number_id = p_num
                        AND c901_attribute_type = 80180 
                        AND c205d_void_fl is NULL 
                ) ; ---RFS;
        IF v_count = 0 THEN
            p_out_message := 'The Part Number ' || p_num || ' is only released for sale in ' ||
            gm_pkg_pd_partnumber.get_rfs_name (p_num) || '';
        END IF;
    END IF;
END gm_chk_rfs_country;
/******************************************************************
* Description : This Procedure is used to fetch the active loaner set list
* Author    : Brinal G
****************************************************************/
PROCEDURE gm_fch_active_loaner_sets (
        p_sets OUT TYPES.cursor_type)
AS
v_plant_id T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
v_comp_id  T1900_COMPANY.C1900_COMPANY_ID%TYPE;
BEGIN
	select get_compid_frm_cntx(),get_plantid_frm_cntx() into v_comp_id,v_plant_id from dual;

	
    OPEN p_sets FOR
    SELECT DISTINCT (t504.c207_set_id) SETID, t504.c207_set_id ||' / '|| C207_SET_NM SNAME
            -- Use the same column name to create different alias for the Loaner Initiate screen and Manage Loaner
            -- report
          , t504.c207_set_id ID      , t504.c207_set_id ||' / '|| C207_SET_NM IDNAME, C207_SET_NM NAME
           FROM t504_consignment t504, t504a_consignment_loaner t504a , T207_SET_MASTER t207,T2080_SET_COMPANY_MAPPING t2080
          WHERE t504.c504_consignment_id = t504a.c504_consignment_id
            AND t207.c207_set_id = t504.c207_set_id
            AND T504.C504_VOID_FL IS NULL
            AND t504.c504_type = 4127 -- only product loaners
            AND t504.c504_status_fl = '4'
            AND t504a.C504A_STATUS_FL != '60'
            AND t504a.c504a_void_fl IS NULL
            AND t207.C207_VOID_FL IS NULL
            AND T2080.C207_SET_ID = T207.C207_SET_ID
            AND t2080.c2080_void_fl is null
            AND T2080.C1900_COMPANY_ID in (SELECT C1900_COMPANY_ID  --PC-3425: Sets not loading in Manage Loaner screen when company mapped to multiple plants
											 FROM T5041_PLANT_COMPANY_MAPPING 
            							    WHERE C5041_PRIMARY_FL = (CASE WHEN v_plant_id = '3004' AND c1900_company_id = '1017' THEN 'N' ELSE 'Y' END) 
            							      AND C5040_PLANT_ID= V_PLANT_ID
											  AND C5041_VOID_FL IS NULL)
            AND t504a.C5040_PLANT_ID = V_PLANT_ID 
       ORDER BY t504.c207_set_id;
END gm_fch_active_loaner_sets;
/*********************************************************************************
* Description : This Procedure is used to fetch the active inhouse loaner set list
* Author    : VPrasath
***********************************************************************************/
PROCEDURE gm_fch_active_ih_loaner_sets (
        p_sets OUT TYPES.cursor_type)
AS
BEGIN
    OPEN p_sets FOR
    SELECT DISTINCT (t504.c207_set_id) SETID, t504.c207_set_id ||' / '|| C207_SET_NM SNAME
            -- Use the same column name to create different alias for the Loaner Initiate screen and Manage Loaner
            -- report
          , t504.c207_set_id ID      , t504.c207_set_id ||' / '|| C207_SET_NM IDNAME, C207_SET_NM NAME
           FROM t504_consignment t504, t504a_consignment_loaner t504a               , T207_SET_MASTER t207
          WHERE t504.c504_consignment_id = t504a.c504_consignment_id
            AND t207.c207_set_id = t504.c207_set_id
            AND T504.C504_VOID_FL IS NULL
            AND t504.c504_type = 4119 -- only inhouse loaners
            AND t504.c504_status_fl = '4'
            AND t504a.C504A_STATUS_FL != '60'
            AND t504a.c504a_void_fl IS NULL
            AND t207.C207_VOID_FL IS NULL
       ORDER BY t504.c207_set_id;
END gm_fch_active_ih_loaner_sets;

/*****************************************************************************
* Description: get count of GM-CRA based on GM-RA (trans_id)
* Author   : JReddy
*******************************************************************************/
FUNCTION GET_TRANS_RA_COUNT
(p_trans_id IN t504a_consignment_loaner.c504_consignment_id%TYPE
)
RETURN VARCHAR2
IS
v_count   VARCHAR2 (50);
BEGIN
     BEGIN
       SELECT COUNT(*)INTO v_count
               FROM t506_returns
              WHERE C506_PARENT_RMA_ID = p_trans_id AND c506_void_fl is NULL;
     EXCEPTION
      WHEN OTHERS THEN
       RETURN NULL;
      END;
     RETURN v_count;
END GET_TRANS_RA_COUNT;

/*****************************************************************************
* Description: This procedure is used to fetch all transactions dynamically
*     by the ref id (trans_id)
* Author   : Rajkumar J
*******************************************************************************/
PROCEDURE gm_fch_trans_details (
        p_trans_id IN t504a_consignment_loaner.c504_consignment_id%TYPE,
        p_data_out OUT TYPES.cursor_type)
AS
    vcnt NUMBER (5) ;
    v_trans_id t504a_consignment_loaner.c504_consignment_id%TYPE;    
BEGIN
	
	BEGIN
          SELECT c5010_last_updated_trans_id
  			INTO  v_trans_id
   			FROM t5010_tag
  		   WHERE c5010_tag_id   = 	p_trans_id
             AND c5010_void_fl IS NULL;
           EXCEPTION
             WHEN NO_DATA_FOUND
           THEN
           v_trans_id := p_trans_id;
           END;
           
	
     SELECT COUNT ( *) INTO vcnt FROM t501_order WHERE c501_order_id = v_trans_id;
    IF vcnt != 0 THEN
        OPEN p_data_out --if order
        FOR SELECT c501_order_id refid,
        c501_status_fl sfl,
        '' CTYPE,
        'ORDER' tblnm FROM t501_order WHERE c501_order_id = v_trans_id;
    ELSE
        	 SELECT count(1)
               INTO vcnt
               FROM t504a_consignment_loaner
              WHERE c504_consignment_id = v_trans_id AND c504a_void_fl is NULL;
        IF vcnt != 0 THEN
            OPEN p_data_out 
            FOR 
	            SELECT t504.c504_consignment_id refid,            	
	            	   t504a.c504a_status_fl sfl,
	            	   t504.c504_type ctype,
	            	   'LOANER' tblnm 
	              FROM t504a_consignment_loaner t504a
	            	 , t504_consignment t504
	             WHERE t504.c504_consignment_id = v_trans_id
	               AND t504.c504_consignment_id = t504a.c504_consignment_id	
	               AND t504a.c504a_void_fl is NULL;
        ELSE
            OPEN p_data_out FOR SELECT * FROM
            (SELECT C504_CONSIGNMENT_ID CID, NVL (GET_RULE_VALUE ('CON_TYPE', C504_TYPE), 'C') CON_TYPE, C504_TYPE
                    CTYPE                      , C504_STATUS_FL SFL, '' RACOUNT, null REPROCESSDATE, '' SETID
                   FROM T504_CONSIGNMENT
                  WHERE C504_VOID_FL IS NULL
                    AND C504_CONSIGNMENT_ID = v_trans_id
              UNION
             SELECT     C412_INHOUSE_TRANS_ID CID, NVL (GET_RULE_VALUE ('CON_TYPE', C412_TYPE), 'I') CON_TYPE,
                    C412_TYPE CTYPE              , C412_STATUS_FL SFL, '' RACOUNT, null REPROCESSDATE, '' SETID
                   FROM T412_INHOUSE_TRANSACTIONS
                  WHERE C412_VOID_FL IS NULL
                    AND C412_INHOUSE_TRANS_ID = v_trans_id
              UNION
              SELECT     C506_RMA_ID CID, NVL (GET_RULE_VALUE ('CON_TYPE', C506_TYPE), 'R') CON_TYPE,
                    C506_TYPE CTYPE              , C506_STATUS_FL SFL, GET_TRANS_RA_COUNT(v_trans_id) RACOUNT,
                    c501_reprocess_date REPROCESSDATE, c207_set_id SETID
                   FROM t506_returns
                  WHERE C506_VOID_FL IS NULL
                    AND C506_RMA_ID = v_trans_id
            ) ;
        END IF;
    END IF;
END gm_fch_trans_details;

/*****************************************************************************
* Description: to fetch the schedule data
*     
* Author   : Dhana reddy
*******************************************************************************/
PROCEDURE gm_fch_schedule(
p_stropt        IN   VARCHAR2,
p_schedule_cur  OUT  TYPES.cursor_type
)
AS

BEGIN 
	IF p_stropt ='LOANERSCHDULE'
	THEN
	OPEN p_schedule_cur
	FOR
	 SELECT c525_surgery_date schdate, count(*) totcount
	       FROM t525_product_request 
	      WHERE c525_void_fl IS NULL 
	        AND c525_surgery_date IS NOT NULL 
	   GROUP BY c525_surgery_date ;
	END IF;
END gm_fch_schedule;
/*****************************************************************************
* Description: For Activity Log Details     
* Author     : Elango
*******************************************************************************/
PROCEDURE gm_pkg_common_activity_log(
p_activity_log_id        IN   VARCHAR2
,p_req_id     IN   t7000_account_price_request.c7000_account_price_request_id%TYPE
,p_ref_type	  IN   VARCHAR2
,p_status     IN   VARCHAR2
,p_desc       IN   VARCHAR2
,p_userid	  IN   VARCHAR2
)
AS
	 v_activity_log_id   T909_ACTIVITY_LOG.C909_ACTIVITY_LOG_ID%TYPE;	 
BEGIN 
		IF p_activity_log_id IS NULL
		THEN
			SELECT S909_ACTIVITY_LOG_ID.nextval INTO v_activity_log_id FROM DUAL;
		ELSE
		   v_activity_log_id := p_activity_log_id;
		END IF;
		
		IF p_ref_type ='52221'  ---For pricing
		THEN
			IF p_status='52186'	THEN
				UPDATE T909_ACTIVITY_LOG
				   SET c909_description = p_desc,
				   c909_created_by   = p_userid,
				   c909_created_date = CURRENT_DATE
				 WHERE c909_ref_id        = p_req_id
				 AND c909_activity_log_id = v_activity_log_id 
				    AND c901_operation    = p_status
				    AND c901_ref_type   = p_ref_type;  
			
				IF SQL%ROWCOUNT = 0 
				THEN				
				INSERT INTO T909_ACTIVITY_LOG
								(c909_activity_log_id,c909_ref_id,c901_ref_type,c901_operation,c909_description,c909_created_by,c909_created_date
								)
						 VALUES (v_activity_log_id,p_req_id,p_ref_type,p_status,p_desc,p_userid,CURRENT_DATE
							    );
				END IF;
			ELSE
				UPDATE T909_ACTIVITY_LOG
				   SET c909_description = p_desc,
				   c909_last_updated_by = p_userid,
				   c909_last_updated_date = CURRENT_DATE
				 WHERE c909_ref_id        = p_req_id
				 AND c909_activity_log_id = v_activity_log_id 
				 AND c901_operation    = p_status
				   AND c901_ref_type   = p_ref_type;  
			
				IF SQL%ROWCOUNT = 0 
				THEN				
				INSERT INTO T909_ACTIVITY_LOG
								(c909_activity_log_id,c909_ref_id,c901_ref_type,c901_operation,c909_description,c909_last_updated_by,c909_last_updated_date
								)
						 VALUES (v_activity_log_id,p_req_id,p_ref_type,p_status,p_desc,p_userid,CURRENT_DATE
							 );
				END IF;
			END IF;
		END IF;
END gm_pkg_common_activity_log;

/********************************************************************************
	  * This procedure is to fetch active employees who have placed orders.
	  * Author : Rick Schmid
	  * Date   : 11/01/2012
********************************************************************************/
PROCEDURE gm_fch_order_by_user ( 
	p_active_cur    OUT    TYPES.cursor_type,
	p_separator_cur OUT    TYPES.cursor_type,
	p_inactive_cur  OUT    TYPES.cursor_type
)
AS
	v_comp_id T1900_COMPANY.C1900_COMPANY_ID%TYPE;  
BEGIN
--
select get_compid_frm_cntx() into v_comp_id from dual;
	OPEN p_active_cur 
	 FOR
	     -- Customer Service
SELECT TO_CHAR(t1501.c101_mapped_party_id) ID, get_user_name(t1501.C101_MAPPED_PARTY_ID) NAME
          FROM t101_user t101, t1501_group_mapping t1501
         WHERE t1501.c1501_void_fl IS NULL
           AND t101.c101_user_id = c101_mapped_party_id
           AND t101.c901_user_status = 311
           AND t1501.c1500_group_id in ( '100022', '100023', '1000001')
           AND t1501.c1900_company_id = v_comp_id

UNION ALL
-- Accounting List, also includes A/R, A/P, Costing, A/c Reporting
SELECT TO_CHAR(t501.c501_created_by) ID, ACCT_USER.NM NAME
          FROM t101_user t101, t501_order t501,(
               select C101_MAPPED_PARTY_ID ID, get_user_name(C101_MAPPED_PARTY_ID) NM
                 from t1501_group_mapping
                where c1501_void_fl IS NULL
                  and c1500_group_id in ( '100009', '100010', '100011',
                                          '100012', '100017', '100019',
                                          '100000040') AND c1900_company_id = v_comp_id) ACCT_USER
	     WHERE t501.c501_void_fl IS NULL
	       AND t501.c501_created_by = ACCT_USER.ID
         AND t101.c101_user_id = ACCT_USER.ID
         AND t101.c901_user_status = 311
     GROUP BY t501.c501_created_by, ACCT_USER.NM

     
UNION ALL

-- For Sales Rep List
SELECT TO_CHAR(t703.c703_sales_rep_id) ID, t703.C703_SALES_REP_NAME NAME
	      FROM t501_order t501, t703_sales_rep t703, t101_user t101
	     WHERE t703.c703_void_fl IS NULL
           AND t101.c101_user_id =  t703.c703_sales_rep_id
           AND t101.c901_user_status = 311
           AND t501.c501_void_fl IS NULL
           AND t703.c703_SALES_REP_ID = t501.c501_created_by
	       AND t703.c703_ACTIVE_FL = 'Y'
	       AND t703.C1900_COMPANY_ID = v_comp_id
	  GROUP BY t703.c703_sales_rep_id, t703.C703_SALES_REP_NAME
ORDER BY NAME;

    OPEN p_separator_cur
     FOR
         SELECT '9999' ID,'-------------------------' NAME FROM DUAL;
      
	OPEN p_inactive_cur
	 FOR
	     -- Customer Service
SELECT TO_CHAR(t1501.c101_mapped_party_id) ID, get_user_name(t1501.C101_MAPPED_PARTY_ID) NAME
          FROM t101_user t101, t1501_group_mapping t1501
         WHERE t1501.c1501_void_fl IS NULL
           AND t101.c101_user_id = c101_mapped_party_id
           AND t101.c901_user_status = 317
           AND t1501.c1500_group_id in ( '100022', '100023', '1000001')
           AND t1501.c1900_company_id = v_comp_id

UNION ALL
-- Accounting List, also includes A/R, A/P, Costing, A/c Reporting
SELECT TO_CHAR(t501.c501_created_by) ID, ACCT_USER.NM NAME
          FROM t101_user t101, t501_order t501,(
               select C101_MAPPED_PARTY_ID ID, get_user_name(C101_MAPPED_PARTY_ID) NM
                 from t1501_group_mapping
                where c1501_void_fl IS NULL
                  and c1500_group_id in ( '100009', '100010', '100011',
                                          '100012', '100017', '100019',
                                          '100000040') AND c1900_company_id = v_comp_id) ACCT_USER
	     WHERE t501.c501_void_fl IS NULL
	       AND t501.c501_created_by = ACCT_USER.ID
         AND t101.c101_user_id = ACCT_USER.ID
         AND t101.c901_user_status = 317
     GROUP BY t501.c501_created_by, ACCT_USER.NM

     
UNION ALL

-- For Sales Rep List
SELECT TO_CHAR(t703.c703_sales_rep_id) ID, t703.C703_SALES_REP_NAME NAME
	      FROM t501_order t501, t703_sales_rep t703, t101_user t101
	     WHERE t703.c703_void_fl IS NULL
           AND t101.c101_user_id =  t703.c703_sales_rep_id
           AND t101.c901_user_status = 317
           AND t501.c501_void_fl IS NULL
           AND t703.c703_SALES_REP_ID = t501.c501_created_by
	       AND t703.c703_ACTIVE_FL = 'Y'
	       AND t703.C1900_COMPANY_ID = v_comp_id
	  GROUP BY t703.c703_sales_rep_id, t703.C703_SALES_REP_NAME
ORDER BY NAME;
--
END gm_fch_order_by_user;
/***************************************************************
	 * Description : This Procedure is used to check the International Use of the part
	 * Author	   : aprasath
	 ****************************************************************/
	PROCEDURE gm_chk_international_flag (
	  p_num   			IN		t2550_part_control_number.c205_part_number_id%TYPE
	, p_control_num		IN		t2550_part_control_number.c2550_control_number%TYPE
	, p_country_code    IN      t901_code_lookup.c901_code_id%type
    , p_out_message   	OUT	   	VARCHAR2
	)
	AS
	    v_international_fl      varchar2(20);
	    v_country_id			t901_code_lookup.c901_code_id%TYPE;
	BEGIN
		
		v_country_id := NVL(GET_RULE_VALUE('COUNTRY','INTERNATIONAL_USE'),'1101'); --1101 - US
		
		BEGIN
			SELECT DECODE(T2540.C901_INTERNATIONAL_USE,'1960','Y','N') --1960: Yes; 1961: No
				INTO v_international_fl
			   FROM T2550_PART_CONTROL_NUMBER t2550, T2540_DONOR_MASTER t2540
			  WHERE T2550.C2540_DONOR_ID       = T2540.C2540_DONOR_ID
			    AND T2540.C2540_VOID_FL       IS NULL
			    AND T2550.C205_PART_NUMBER_ID  = p_num
			    AND T2550.C2550_CONTROL_NUMBER = p_control_num;
			EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				v_international_fl := 'Y';
    	END;

	    IF v_international_fl = 'N' and p_country_code != v_country_id 
	    THEN
            p_out_message := 'Lot # '||p_control_num||' for Part # ' || p_num || ' is not for International Use ';
	    END IF;
	END gm_chk_international_flag;
	
	/***************************************************************
	 * Description : This Procedure is used to check the part expiry
	 * Author	   : aprasath
	 ****************************************************************/
	PROCEDURE gm_chk_part_expiry (
	  p_num   			IN		t2550_part_control_number.c205_part_number_id%TYPE
	, p_control_num		IN		t2550_part_control_number.c2550_control_number%TYPE
	, p_status          IN      VARCHAR2
	, p_type	        IN      VARCHAR2
	, p_tran_id         IN      VARCHAR2 DEFAULT NULL
	, p_out_message   	OUT	   	VARCHAR2
	, p_out_flag   	    OUT	   	VARCHAR2
	, p_out_exp   	    OUT	   	VARCHAR2
	)
	AS
	    v_hard_month     NUMBER;
	    v_soft_month     NUMBER;
	    v_hard_month_fl  VARCHAR2(1);
	    v_soft_month_fl  VARCHAR2(1);
	    v_expiry_fl      VARCHAR2(1);
	    v_flag		     VARCHAR2(1);
	    v_part_type      t906_rules.c906_rule_value%TYPE; 
	    v_expiry_date	VARCHAR2(30); 
	    v_date_fmt 		t906_rules.c906_rule_value%TYPE;
	    v_attrval		T205d_Part_Attribute.C205d_Attribute_Value%TYPE;
	    v_expiry_skip_tran_id t2550_part_control_number.c2550_expdate_skip_transid%TYPE;
	BEGIN
	 	 
		-- No need to show expiry date validation for sample part
		BEGIN
			SELECT NVL(TRIM(C205d_Attribute_Value),'N') INTO v_attrval
			FROM T205d_Part_Attribute
			WHERE C205_Part_Number_Id = p_num
			AND C901_Attribute_Type   = 103745 --Sample
			AND C205d_Void_Fl IS NULL;
		EXCEPTION
		WHEN NO_DATA_FOUND THEN
			v_attrval := 'N';
		END;
		
		IF v_attrval = 'Y'
		THEN
			RETURN;
		END IF;
		
		--if type is return or QNSC, need skip the expiry date validation 
		IF ( NVL(p_type, '-999') != 'RETURN' AND NVL(p_type, '-999') != '4115') 
		THEN
		v_part_type  := get_partnum_material_type(p_num);
		v_hard_month := TO_NUMBER(NVL(GET_RULE_VALUE(v_part_type,'HARD_PART_MONTH'),'0')); --getting hard validation of part expiry before no.of month
		v_soft_month := TO_NUMBER(NVL(GET_RULE_VALUE(v_part_type,'SOFT_PART_MONTH'),'0')); --getting soft validation of part expiry before no.of month
		v_flag := 'N';
		v_date_fmt := NVL(get_rule_value('DATEFMT','DATEFORMAT'),'MM/DD/YYYY');
		
		BEGIN		
		  SELECT (
		    CASE
		        WHEN (ADD_MONTHS (T2550.C2550_EXPIRY_DATE, v_hard_month) <= CURRENT_DATE)
		        THEN 'Y'
		        ELSE 'N'
		    END) , (
		    CASE
		        WHEN (ADD_MONTHS (T2550.C2550_EXPIRY_DATE, v_soft_month) <= CURRENT_DATE)
		        THEN 'Y'
		        ELSE 'N'
		    END) , (
		    CASE
		        WHEN (T2550.C2550_EXPIRY_DATE <= CURRENT_DATE)
		        THEN 'Y'
		        ELSE 'N'
		    END) , 
		    TO_CHAR(T2550.C2550_EXPIRY_DATE,v_date_fmt),t2550.c2550_expdate_skip_transid 
		    INTO v_hard_month_fl,v_soft_month_fl,v_expiry_fl, v_expiry_date,v_expiry_skip_tran_id
		   FROM T2550_PART_CONTROL_NUMBER T2550
		  WHERE T2550.C205_PART_NUMBER_ID  = p_num
		    AND T2550.C2550_CONTROL_NUMBER = p_control_num;
		EXCEPTION
					WHEN NO_DATA_FOUND
					THEN
						v_hard_month_fl := 'N';
						v_soft_month_fl := 'N';
						v_expiry_fl := 'N';
						v_expiry_date := '';
    	END;
    	
    	
	    IF v_expiry_fl = 'Y' THEN	    
            p_out_message := 'The Part Number ' || p_num || ' with control number '|| p_control_num ||' is expired on '||v_expiry_date;
            v_flag := 'Y';
            p_out_exp := 'EXP0M';
        ELSIF v_hard_month_fl = 'Y' THEN
            p_out_message := 'The Part Number ' || p_num || ' with control number '|| p_control_num ||' is going to expired in '||ABS(v_hard_month)||' months';
            p_out_exp := 'EXP1M';
            v_flag := NVL(GET_RULE_VALUE(v_part_type,'HARD_PART_EXPIRY_TXN'),'Y');
            --PMT-15652 Skip the validation for override transaction based on approval
	            IF v_expiry_skip_tran_id = p_tran_id  THEN 
	            v_flag :='N';
	            p_out_message := '';
	            END IF;
        ELSIF v_soft_month_fl = 'Y'  AND p_status <> 'VERIFY' THEN
            p_out_message := 'The Part Number ' || p_num || ' with control number '|| p_control_num ||' is going to expired in '||ABS(v_soft_month)||' months';
            p_out_exp := 'EXP3M';
            v_flag := NVL(GET_RULE_VALUE(v_part_type,'SOFT_PART_EXPIRY_TXN'),'N'); 
        END IF;
        
        p_out_flag := v_flag;
        END IF;
	END gm_chk_part_expiry;

    /*************************************************************************
	 * Description : This Procedure is used to skip the part expiry date validation
	 * Author	   : Bala
	 **************************************************************************/	
	PROCEDURE gm_skip_part_expdate_valid (
	  p_trans_id		IN		VARCHAR2
	, p_out_flag   	    OUT	   	VARCHAR2
	)
	AS
	     v_count   NUMBER;
	BEGIN

		SELECT COUNT(1)
		  INTO v_count
		  FROM t906_rules
		 WHERE c906_rule_value = p_trans_id
		   AND c906_rule_grp_id  = 'SKIP_EXP_DATE_VALID'
		   AND c906_void_fl IS NULL;
	
		IF v_count >0 THEN
		   p_out_flag := 'Y';
		ELSE
		   p_out_flag := 'N';
		END IF;	
	
    END gm_skip_part_expdate_valid;
    
     /*******************************************************************************
	 * Description : This function is used to fetch companyid based on transaction
	 * Author	   : Karthik
	 ********************************************************************************/	
	PROCEDURE gm_fch_trans_comp_id (
	  p_trans_id		IN		VARCHAR2
	, p_out_companyid   OUT	   	VARCHAR2
	)
	AS

	BEGIN
		 SELECT c1900_company_id 
	  			INTO p_out_companyid 
	 			FROM (
			      /*SELECT c1900_company_id
				           FROM t525_product_request 
				           WHERE c525_product_request_id = p_trans_id
			             AND c525_void_fl IS NULL 	  				
			       UNION ALL*/
			       SELECT c1900_company_id  	   					  
			             FROM t504_consignment
			             WHERE c504_consignment_id = p_trans_id
			             AND c504_void_fl is null 
			       UNION ALL
			       SELECT c1900_company_id
			             FROM t501_order 
			             WHERE c501_order_id = p_trans_id 
			             AND c501_void_fl IS NULL 
			      UNION ALL
			      SELECT c1900_company_id
				      	 FROM t412_inhouse_transactions 
				         WHERE c412_inhouse_trans_id  = p_trans_id
				       	 AND c412_void_fl IS NULL
			      UNION ALL
			      SELECT c1900_company_id
			             FROM t408_dhr
			             WHERE c408_dhr_id = p_trans_id
			             AND c408_void_fl IS NULL
			      UNION ALL
			       SELECT c1900_company_id
			             FROM t506_returns
			             WHERE c506_rma_id = p_trans_id
			             AND c506_void_fl IS NULL
			      UNION ALL
			        SELECT c1900_company_id
			             FROM t520_request
			             WHERE c520_request_id = p_trans_id
			             AND c520_void_fl IS NULL
	  			   ) 
		GROUP BY c1900_company_id;	
	END gm_fch_trans_comp_id;
	
	/*******************************************************************************
	 * Description : This function is used to fetch company and plant id based on transaction
	 * Author	   : APrasath
	 ********************************************************************************/	
	PROCEDURE gm_fch_trans_comp_plant_id (
	    p_ref_id 	      IN   VARCHAR2	
	  , p_out_companyid   OUT  t1900_company.c1900_company_id%TYPE
	  , p_out_plantid     OUT  t5040_plant_master.c5040_plant_id%TYPE
	)
	AS
	BEGIN
		BEGIN
		 SELECT c1900_company_id, c5040_plant_id
	  			INTO p_out_companyid, p_out_plantid 
	 			FROM (			     
			       SELECT c1900_company_id,c5040_plant_id
			             FROM t504_consignment
			             WHERE c504_consignment_id = p_ref_id
			             AND c504_void_fl IS NULL 
			       UNION ALL
			       SELECT c1900_company_id,c5040_plant_id
			             FROM t501_order 
			             WHERE c501_order_id = p_ref_id 
			             AND c501_void_fl IS NULL 
			      UNION ALL
			      SELECT c1900_company_id,c5040_plant_id
				      	 FROM t412_inhouse_transactions 
				         WHERE c412_inhouse_trans_id  = p_ref_id
				       	 AND c412_void_fl IS NULL
			      UNION ALL
			      SELECT c1900_company_id,c5040_plant_id
			             FROM t408_dhr
			             WHERE c408_dhr_id = p_ref_id
			             AND c408_void_fl IS NULL
			      UNION ALL
			       SELECT c1900_company_id,c5040_plant_id
			             FROM t506_returns
			             WHERE c506_rma_id = p_ref_id
			             AND c506_void_fl IS NULL
			      UNION ALL
			        SELECT c1900_company_id,c5040_plant_id
			             FROM t520_request
			             WHERE c520_request_id = p_ref_id
			             AND c520_void_fl IS NULL
			      UNION ALL
			        SELECT c1900_company_id,c5040_plant_id
				          FROM t525_product_request 
				        WHERE c525_product_request_id = p_ref_id
			            AND c525_void_fl IS NULL		
			          --  
			      UNION ALL      
	  			   SELECT c1900_company_id,c5040_plant_id
					    FROM T401_PURCHASE_ORDER 
					    WHERE C401_PURCHASE_ORD_ID = p_ref_id
					UNION ALL
					SELECT c1900_company_id, NULL
					    FROM T215_PART_QTY_ADJUSTMENT 
					    WHERE C215_PART_QTY_ADJ_ID = p_ref_id
					UNION ALL
					SELECT c1900_company_id,c5040_plant_id
					    FROM T409_NCMR 
					    WHERE C409_NCMR_ID = p_ref_id
					UNION ALL
					SELECT c1900_company_id,c5040_plant_id
					    FROM T920_TRANSFER 
					    WHERE C920_TRANSFER_ID = p_ref_id);
	  	 EXCEPTION WHEN NO_DATA_FOUND THEN
	  	  p_out_companyid := NULL;
	  	  p_out_plantid := NULL;	  	  
	  	 END;
	END gm_fch_trans_comp_plant_id;
	
	
	 /*******************************************************************************************
	 * Description : This Procedure is used to fetch companyid and plantid based on transaction
	 * Author	   : Tejeswara Reddy
	 ********************************************************************************************/
	PROCEDURE gm_fch_trans_comp_plant_id (
	    p_source	      IN   NUMBER
	  , p_ref_id 	      IN   VARCHAR2	
	  , p_out_companyid   OUT  t1900_company.c1900_company_id%TYPE
	  , p_out_plantid     OUT  t5040_plant_master.C5040_PLANT_ID%TYPE
	)
	AS
	 v_trans_company_id  t1900_company.c1900_company_id%TYPE;
	 v_trans_plant_id    t5040_plant_master.C5040_PLANT_ID%TYPE;
	BEGIN 
               IF p_source = 50181 THEN  -- Consignments
		  BEGIN
		    SELECT c1900_company_id, C5040_PLANT_ID   
		    INTO v_trans_company_id, v_trans_plant_id
		    FROM t504_consignment
		   WHERE c504_consignment_id = p_ref_id
		     AND c504_void_fl IS NULL;		    
		   EXCEPTION WHEN NO_DATA_FOUND THEN
			v_trans_company_id := NULL;
			v_trans_plant_id := NULL;
		  END;
                ELSIF p_source = 50184 THEN   -- Request
		  BEGIN
		    SELECT c1900_company_id, C5040_PLANT_ID
		      INTO v_trans_company_id, v_trans_plant_id
		      FROM t520_request
		     WHERE c520_request_id = p_ref_id
		      AND c520_void_fl IS NULL;
		   EXCEPTION WHEN NO_DATA_FOUND THEN
		     BEGIN
			SELECT c1900_company_id, C5040_PLANT_ID   
			  INTO v_trans_company_id, v_trans_plant_id
			  FROM t504_consignment
			 WHERE c504_consignment_id = p_ref_id
			  AND c504_void_fl is null;
		       EXCEPTION WHEN NO_DATA_FOUND THEN
			v_trans_company_id := NULL;
			v_trans_plant_id := NULL;
		    END;
		   END;
                ELSIF (p_source = 50180 OR p_source = 11389) THEN  -- 50180 - Orders and 11389 - Draft
		   BEGIN
		      SELECT c1900_company_id, C5040_PLANT_ID
	     		INTO v_trans_company_id, v_trans_plant_id
	       	        FROM t501_order 
	      	       WHERE c501_order_id = p_ref_id 
	        	 AND c501_void_fl IS NULL;		   		 			 
		    EXCEPTION WHEN NO_DATA_FOUND THEN
			v_trans_company_id := NULL;
			v_trans_plant_id := NULL;
		    END;
		ELSE
		   v_trans_company_id := NULL;
		   v_trans_plant_id := NULL;
               END IF;
       
           p_out_companyid := v_trans_company_id;
	   p_out_plantid   := v_trans_plant_id;

      END gm_fch_trans_comp_plant_id;

      
/************************************************************************************************************************
* Description : This Procedure is used to check the entered control number is already consigned to some another sales rep
	*                in new order screen while tab out From the lot field
* Author    :    karthik
**********************************************************************************************************************/

PROCEDURE gm_chk_field_sales_lot (
		p_order_id t501_order.c501_order_id%TYPE,
 		p_pnum   IN t2550_part_control_number.c205_part_number_id%TYPE,
        p_cnum   IN t2550_part_control_number.c2550_control_number%TYPE,
        p_rep_id IN t703_sales_rep.c703_sales_rep_id%TYPE,
        pout     OUT VARCHAR2 )
  AS
	    v_usage_rep T703_SALES_REP.C703_SALES_REP_ID%TYPE;
	    v_usage_rep_name T703_SALES_REP.C703_SALES_REP_NAME%TYPE;
	    v_actual_rep T703_SALES_REP.C703_SALES_REP_ID%TYPE;
	    v_company_id	t1900_company.c1900_company_id%TYPE;
        v_flag VARCHAR2(1);
        v_shipped_date T907_SHIPPING_INFO.c907_ship_to_dt%TYPE;
	 BEGIN
		   SELECT  NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL'))
			   INTO v_company_id
			  FROM DUAL;
			  
	   --validating the company is required for lot track
	   SELECT get_rule_value(v_company_id,'LOT_TRACK') INTO v_flag FROM DUAL; 
	   
	   IF v_flag IS NULL OR v_flag <> 'Y' 
	   THEN
		   RETURN;
	   END IF;
	   
		  --validating the part is required to track lot		  	
	   SELECT get_part_attr_value_by_comp(p_pnum,'104480',v_company_id) INTO v_flag FROM DUAL;   
	   
	   IF v_flag IS NULL OR v_flag <> 'Y' 
	   THEN
		   RETURN;
	   END IF;
	   
	   /*p_rep_id is coming as null  in DO summary Edit- Control Numbers screen,
	    * so get the rep id for the order id in T501_order table 
	    */
	   IF p_rep_id IS NULL AND p_order_id IS NOT NULL THEN
	   
	   		BEGIN
				SELECT C703_SALES_REP_id 
					INTO  v_actual_rep
				FROM T501_ORDER
				 WHERE C501_ORDER_ID = p_order_id
				 AND C501_VOID_FL  IS NULL;
			  EXCEPTION
            WHEN NO_DATA_FOUND THEN
                v_actual_rep := p_rep_id;
            END;
            ELSE
            v_actual_rep :=p_rep_id;
          END IF;
            
            
         /* Taking the rep id of the lot consigned*/

			BEGIN
				SELECT REPID , SHIPPEDDATE 
					  INTO v_usage_rep ,v_shipped_date
					FROM
					  ( SELECT REPID , SHIPPEDDATE 
						FROM
						  (
					  SELECT T703.C703_SALES_REP_ID REPID ,
					    T907.C907_SHIP_TO_DT SHIPPEDDATE
					  FROM T505_ITEM_CONSIGNMENT T505,
					    T504_CONSIGNMENT T504 ,
					    T703_SALES_REP T703 ,
					    T907_SHIPPING_INFO T907
					  WHERE T505.C504_CONSIGNMENT_ID = T504.C504_CONSIGNMENT_ID
					  AND T504.C504_CONSIGNMENT_ID   = T907.C907_REF_ID
					  AND T505.c505_control_number   = UPPER(p_cnum)
					  AND T504.C504_VOID_FL         IS NULL
					  AND T504.C504_TYPE             = '4110' --Consignment type
					  AND T504.C504_STATUS_FL        = '4'
					  AND T504.C701_DISTRIBUTOR_ID   = T703.C701_DISTRIBUTOR_ID
					  AND T703.C703_VOID_FL         IS NULL
					  AND T907.c907_void_FL         IS NULL
					  UNION ALL
					  SELECT T501.C703_SALES_REP_ID REPID ,
					    T907.c907_ship_to_dt SHIPPEDDATE
					  FROM T501_ORDER T501 ,
					    T502_ITEM_ORDER T502 ,
					    T907_SHIPPING_INFO T907
					  WHERE T501.C501_ORDER_ID     = T502.C501_ORDER_ID
					  AND T501.C501_ORDER_ID       = T907.C907_REF_ID
					  AND T502.C502_CONTROL_NUMBER = UPPER(p_cnum)
					  AND T501.C501_VOID_FL       IS NULL
					  AND T907.c907_void_FL       IS NULL
					  AND T501.C501_STATUS_FL      = '3'
					   )
						ORDER BY SHIPPEDDATE DESC
						)
					WHERE ROWNUM = 1;
				EXCEPTION
             WHEN NO_DATA_FOUND THEN
                v_usage_rep := NULL;
                v_shipped_date := NULL;
            END;
				
            v_usage_rep_name:=  GET_REP_NAME(v_usage_rep);
			/* The lot is shipped to one rep,but the same lot is used in surgery by some other rep
			 * At that time we are throwing the below error message  */	
				
		IF (v_actual_rep != v_usage_rep) THEN
			pout := 'Allograft ID  ' || UPPER(p_cnum) || ' is currently consigned to another Field Sales. ' ;
		END IF;

END gm_chk_field_sales_lot;


/************************************************************************************************************************
* Description : This Procedure is used to check the entered control number is already billed lot
* Author    :    Dsandeep
**********************************************************************************************************************/

PROCEDURE gm_chk_billed_lot (
 		p_pnum   IN t2550_part_control_number.c205_part_number_id%TYPE,
        p_cnum   IN t2550_part_control_number.c2550_control_number%TYPE,
        pout     OUT VARCHAR2 )
  AS
	    v_company_id	t1900_company.c1900_company_id%TYPE;
        v_flag VARCHAR2(1);
        v_order_id t501_order.c501_order_id%TYPE;
        v_lot_error_msg     VARCHAR2(200);
	 BEGIN
		   SELECT  NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL'))
			   INTO v_company_id
			  FROM DUAL;
			  
	   --validating the company is required for lot track
	   SELECT get_rule_value(v_company_id,'LOT_TRACK') INTO v_flag FROM DUAL; 
	   
	   IF v_flag IS NULL OR v_flag <> 'Y' 
	   THEN
		   RETURN;
	   END IF;
	   
		  --validating the part is required to track lot		  	
	   SELECT get_part_attr_value_by_comp(p_pnum,'104480',v_company_id) INTO v_flag FROM DUAL;   
	   
	   IF v_flag IS NULL OR v_flag <> 'Y' 
	   THEN
		   RETURN;
	   END IF;
	   
	   /*call te below prc to get the error message if the lot is already billed in some order*/
	    gm_pkg_cs_usage_lot_number.gm_fch_billed_loterror_msg(p_pnum ,p_cnum ,v_lot_error_msg );

        pout := v_lot_error_msg ;
        
END gm_chk_billed_lot;

/**********************************************************************
	*Purpose: Function to get set Id when pass part number for Consignments and excluding any other Tag pattern (EUR,JPN, etc..)
    Also Set list qty should greater than ZERO
    Sort by Minimum Sequence Number
	Author: Yoga
	Location: Database\Packages\Common\gm_pkg_common.bdy
	PC: 339
	Code Lookup:	4070 - Consignment, 	1601 - Set Report Group
	**********************************************************************/
FUNCTION get_part_primary_set_id
  (
    p_part_number_id t205_part_number.c205_part_number_id%TYPE
  )
  RETURN t207_set_master.c207_set_id%TYPE
IS
  v_set_id t207_set_master.c207_set_id%TYPE;
BEGIN

    SELECT setid INTO v_set_id
    FROM
      (SELECT t208.c207_set_id setid ,
        t208.c205_part_number_id pnum ,
        MIN(t207.c207_seq_no) seqno
      FROM t208_set_details t208 ,
        t207_set_master t207
      WHERE t208.c205_part_number_id IN
        (SELECT c205_part_number_id
        FROM t5010_tag
        WHERE c5010_void_fl IS NULL
        AND c207_set_id     IS NULL
        GROUP BY c205_part_number_id
        )
      AND t208.c207_set_id         = t207.c207_set_id
      AND t208.c208_void_fl       IS NULL
      AND t207.c207_void_fl       IS NULL
      AND t208.c205_part_number_id = p_part_number_id
      AND t208.c207_set_id NOT LIKE '%EUR'
      AND t208.c207_set_id NOT LIKE '%EURLN'
      AND t208.c207_set_id NOT LIKE '%LN'
      AND t208.c207_set_id NOT LIKE '%JPN'
      AND t208.c207_set_id NOT LIKE '%ED'
      AND t208.c207_set_id NOT LIKE '%DS'
      AND t208.c207_set_id NOT LIKE '%GCARE'
      AND t207.c207_obsolete_fl IS NULL
      AND t207.C901_SET_GRP_TYPE =1601
      AND C207_TYPE          =4070
      AND t208.c208_set_qty > 0
      GROUP BY t208.c207_set_id ,
        t208.c205_part_number_id
      ORDER BY t208.c205_part_number_id ,
        seqno
      )
    WHERE rownum =1;
      
    RETURN v_set_id;
      
  EXCEPTION
  WHEN OTHERS THEN
    RETURN NULL;
END get_part_primary_set_id;

END gm_pkg_common;
/
