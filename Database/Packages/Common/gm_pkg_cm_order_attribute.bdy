/* Formatted on 2011/02/17 12:47 (Formatter Plus v4.8.0) */


CREATE OR REPLACE PACKAGE BODY gm_pkg_cm_order_attribute
IS
/******************************************************************
* Description : Procedure to save order attribute details
* Author    : Brinal
****************************************************************/
   PROCEDURE gm_sav_order_attribute (
      p_ordid      IN   t501_order.c501_order_id%TYPE,
      p_attrtype   IN   t501a_order_attribute.c901_attribute_type%TYPE,
      p_attrval    IN   t501a_order_attribute.c501a_attribute_value%TYPE,
      p_oaid       IN   t501a_order_attribute.c501a_order_attribute_id%TYPE,
      p_userid     IN   t501a_order_attribute.c501a_created_by%TYPE
   )
   AS
      v_order_attr_id   t501a_order_attribute.c501a_order_attribute_id%TYPE;
   BEGIN
	   
	   	   	   
      UPDATE t501a_order_attribute
         SET c501_order_id = p_ordid,
             c901_attribute_type = p_attrtype,
             c501a_attribute_value = p_attrval,
             c501a_last_updated_by = p_userid,
             c501a_last_updated_date = SYSDATE
       WHERE c501a_order_attribute_id = p_oaid                   --Primary key
         AND c501a_void_fl IS NULL;

      IF (SQL%ROWCOUNT = 0)
      THEN
         SELECT s501a_order_attribute.NEXTVAL
           INTO v_order_attr_id
           FROM DUAL;

         INSERT INTO t501a_order_attribute
                     (c501a_order_attribute_id, c501_order_id,
                      c901_attribute_type, c501a_attribute_value,
                      c501a_created_by, c501a_created_date
                     )
              VALUES (v_order_attr_id, p_ordid,
                      p_attrtype, p_attrval,
                      p_userid, SYSDATE
                     );
      END IF;
   END gm_sav_order_attribute;
END gm_pkg_cm_order_attribute;
/
