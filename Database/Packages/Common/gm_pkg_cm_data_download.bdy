/* Formatted on 2010/05/14 15:31 (Formatter Plus v4.8.0) */
--@"C:\Database\Packages\Common\gm_pkg_cm_data_download.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_cm_data_download
IS
--
   /******************************************************************
   * Description : Function to get batch id
   ****************************************************************/
	FUNCTION get_batch_id (
		p_refid 	IN	 t9501_data_log_detail.c9501_data_log_detail_id%TYPE
	  , p_reftype	IN	 t9501_data_log_detail.c901_ref_type%TYPE
	)
		RETURN VARCHAR2
	IS
		v_batchid	   t9500_data_download_log.c9500_download_identifier%TYPE;
	BEGIN
		SELECT t9500.c9500_download_identifier
		  INTO v_batchid
		  FROM t9501_data_log_detail t9501, t9500_data_download_log t9500
		 WHERE t9501.c9500_data_download_id = t9500.c9500_data_download_id
		   AND t9500.c9500_void_fl IS NULL
		   AND t9501.c9501_ref_id = p_refid
		   AND t9501.c901_ref_type = p_reftype;

		RETURN v_batchid;
	END get_batch_id;

	/******************************************************************
	  * Description : Function to get batch date
	  ****************************************************************/
	FUNCTION get_batch_dt (
		p_refid 	IN	 t9501_data_log_detail.c9501_data_log_detail_id%TYPE
	  , p_reftype	IN	 t9501_data_log_detail.c901_ref_type%TYPE
	)
		RETURN DATE
	IS
		v_batchdt	   t9500_data_download_log.c9500_download_dt%TYPE;
	BEGIN
		SELECT TRUNC (t9500.c9500_download_dt)
		  INTO v_batchdt
		  FROM t9501_data_log_detail t9501, t9500_data_download_log t9500
		 WHERE t9501.c9500_data_download_id = t9500.c9500_data_download_id
		   AND t9500.c9500_void_fl IS NULL
		   AND t9501.c9501_ref_id = p_refid
		   AND t9501.c901_ref_type = p_reftype;

		RETURN v_batchdt;
	END get_batch_dt;

/******************************************************************
   * Description : Function to get batch initiated by
   ****************************************************************/
	FUNCTION get_batch_initiated_by (
		p_refid 	IN	 t9501_data_log_detail.c9501_ref_id%TYPE
	  , p_reftype	IN	 t9501_data_log_detail.c901_ref_type%TYPE
	)
		RETURN VARCHAR2
	IS
		v_initiated_by VARCHAR2 (50);
	BEGIN
		SELECT get_user_name (t9500.c9500_initiated_by)
		  INTO v_initiated_by
		  FROM t9501_data_log_detail t9501, t9500_data_download_log t9500
		 WHERE t9501.c9500_data_download_id = t9500.c9500_data_download_id
		   AND t9500.c9500_void_fl IS NULL
		   AND t9501.c9501_ref_id = p_refid
		   AND t9501.c901_ref_type = p_reftype;

		RETURN v_initiated_by;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN ' ';
	END get_batch_initiated_by;

/*******************************************************
	  * Description : Procedure to get batchid and save info
	  in the  t9500, t9501 download table
	  * Author		: Xun
	  *******************************************************/
	PROCEDURE gm_dd_sav_download_dtls (
		p_inputstr		IN		 VARCHAR2
	  , p_type			IN		 t9500_data_download_log.c901_download_type%TYPE
	  , p_value 		IN		 t9500_data_download_log.c9500_value%TYPE
	  , p_userid		IN		 t9500_data_download_log.c9500_initiated_by%TYPE
	  , p_out_batchid	OUT 	 NUMBER
	)
	AS
		v_seq_id	   t9500_data_download_log.c9500_data_download_id%TYPE;
		v_inputstr	   VARCHAR2 (4000) := p_inputstr;
		v_filename	   VARCHAR2 (200);
		v_sage_batchid VARCHAR2 (100);
		v_company_id  t1900_company.c1900_company_id%TYPE;
	BEGIN
		
		SELECT get_compid_frm_cntx()
		  INTO v_company_id 
		  FROM dual;
		  
		SELECT s9500_data_download_log.NEXTVAL
		  INTO v_seq_id
		  FROM DUAL;

		p_out_batchid := v_seq_id;
		--Getting batch_id from common routine, a customized sequence.
		gm_nextval ('09', 50730, v_sage_batchid);	--(refid, reftype, p_out_batchid);
		--An entry in T908_gm_nextval should	be created with c908_ref_id = last two digits of current year, c908_ref_type = 60001 and c908_start_pattern_id = last two digits of current year;

		--This entry should be created every year or more entries can be created for upcoming years.

		--Entry should be made in t901_code_group also.
		v_sage_batchid := SUBSTR (v_sage_batchid, 2);

		SELECT TO_CHAR (CURRENT_DATE, 'yy') || v_sage_batchid || '-' || TO_CHAR (CURRENT_DATE, 'YYYYMMDD') || '.xml'
		  INTO v_filename
		  FROM DUAL;

		v_sage_batchid := TO_CHAR (CURRENT_DATE, 'yy') || v_sage_batchid;

		IF p_type = 50740	--invoice
		THEN
			INSERT INTO t9500_data_download_log
						(c9500_data_download_id, c9500_download_dt, c901_download_type, c9500_initiated_by
					   , c9500_file_name, c9500_value, c9500_created_by, c9500_created_date, c9500_download_identifier,C1900_COMPANY_ID
						)
				 VALUES (v_seq_id, CURRENT_DATE, p_type, p_userid   --p_type 'INVC', for invoice
					   , v_filename, p_value, p_userid, CURRENT_DATE, v_sage_batchid,v_company_id
						);

			--	v_len		:= NVL (LENGTH (v_inputstr), 0);
			--	v_inputstr	:= SUBSTR (v_inputstr, 1, NVL (LENGTH (v_inputstr), 0) - 1);
			--		v_inputstr := SUBSTR (v_inputstr, 1, INSTR (v_inputstr, '^' -1, 1) - 1);
			v_inputstr	:= REPLACE (v_inputstr, '^', ',');
			my_context.set_my_inlist_ctx (v_inputstr);

			INSERT INTO t9501_data_log_detail
						(c9501_data_log_detail_id, c9500_data_download_id, c9501_ref_id, c901_ref_type)
				SELECT s9501_data_log_detail.NEXTVAL, v_seq_id, v_in_list.token, p_type
				  FROM v_in_list
				 WHERE token IS NOT NULL;	-- p_type FROM v_in_list);

			gm_pkg_ac_payment.gm_ac_update_payment (p_inputstr, p_userid);
			gm_pkg_ac_payment.gm_ac_update_purchase (v_inputstr, p_userid);
		END IF;
	END gm_dd_sav_download_dtls;

	/*******************************************************
	  * Description : Procedure to fetch batch data
	  * Author		: Xun
	  *******************************************************/
	PROCEDURE gm_dd_fch_data_download (
		p_batchid		  IN	   t9500_data_download_log.c9500_data_download_id%TYPE
	  , p_out_batchdtls   OUT	   TYPES.cursor_type
	  , p_out_batchlist   OUT	   TYPES.cursor_type
	)
	AS
	BEGIN
		gm_dd_fch_batch_dtls (p_batchid, p_out_batchdtls);
		gm_dd_fch_batch_dtl_list (p_batchid, 50740, p_out_batchlist);
	END gm_dd_fch_data_download;

/*******************************************************
	  * Description : Procedure to fetch batch detail
	  * Author		: Xun
	  *******************************************************/
	PROCEDURE gm_dd_fch_batch_dtls (
		p_batchid			  IN	   t9500_data_download_log.c9500_data_download_id%TYPE
	  , p_out_batchdtls_cur   OUT	   TYPES.cursor_type
	)
	AS
      v_dateformat VARCHAR2(100);
      v_company_id  t1900_company.c1900_company_id%TYPE;
	BEGIN
		
		SELECT get_compid_frm_cntx(),get_compdtfmt_frm_cntx() 
		  INTO v_company_id,v_dateformat 
		  FROM dual;
		
		OPEN p_out_batchdtls_cur
		 FOR
			 SELECT t9500.c9500_download_identifier batch_number, t9500.c9500_file_name file_name
				  , TO_CHAR (t9500.c9500_download_dt
				  , v_dateformat||' HH12:MI:SS AM') batch_datetime
				  , get_user_name (t9500.c9500_initiated_by) initiated_by, t9500.c901_download_type download_type
			   FROM t9500_data_download_log t9500
			  WHERE t9500.c9500_data_download_id = p_batchid 
			    AND t9500.C1900_COMPANY_ID = v_company_id
			    AND t9500.c9500_void_fl IS NULL;
	END gm_dd_fch_batch_dtls;

/*******************************************************
	  * Description : Procedure to get all Detail list of the batch
	  * Author		: Xun
	  *******************************************************/
	PROCEDURE gm_dd_fch_batch_dtl_list (
		p_batchid			 IN 	  t9500_data_download_log.c9500_data_download_id%TYPE
	  , p_type				 IN 	  t9500_data_download_log.c901_download_type%TYPE
	  , p_out_invclist_cur	 OUT	  TYPES.cursor_type
	)
	AS
	  v_dateformat VARCHAR2(100);
      v_company_id  t1900_company.c1900_company_id%TYPE;
	BEGIN
		
		SELECT get_compid_frm_cntx(),get_compdtfmt_frm_cntx() 
		  INTO v_company_id,v_dateformat 
		  FROM dual;
		  
		IF p_type = 50740
		THEN
			OPEN p_out_invclist_cur
			 FOR
				 SELECT get_sage_vendor_id (t406.c301_vendor_id) sage_vendor_id
					  , TO_CHAR (t406.c406_invoice_dt, v_dateformat) invoice_dt, t406.c406_invoice_id invoice_number
					  --	, t406.c406_payment_amount invoice_amount	--, get_batch_id (t406.c406_payment_id, p_type) batch_id
						,	get_currency_conversion (get_vendor_currency (t406.c301_vendor_id)
												   , get_comp_curr (t406.c1900_company_id)
												   , t9500.c9500_download_dt
												   , NVL (t406.c406_payment_amount, 0)
													)
						  * 1 invoice_amount
					  , NVL (t406.c406_payment_amount, 0) local_payment_amt --This code is added for pmt-5167 to save the vendor currency amount into xml file.
					  , (t406.c401_purchase_ord_id || decode (c406_comments,null,null,'-' || c406_comments)) invoice_comment					    
					  ,  get_currency_conversion (get_vendor_currency (t406.c301_vendor_id)
												 , get_comp_curr (t406.c1900_company_id)
												 , t9500.c9500_download_dt
												 , NVL (t406.c406_payment_amount, 0)
												  )
						* 1 invoice_amount_ext
					  , NVL (t406.c406_payment_amount, 0) local_payment_amt_ext
            		  , get_batch_id (t406.c406_payment_id, p_type) batch_number_ext
					  , t406.c406_invoice_id invoice_number_ext, get_vendor_name (t406.c301_vendor_id) vendor_name
					  , TO_CHAR (CURRENT_DATE, v_dateformat) postdate
				   FROM t406_payment t406, t9501_data_log_detail t9501, t9500_data_download_log t9500
				  WHERE t406.c406_payment_id = t9501.c9501_ref_id	
					AND t406.c406_payment_amount > 0 --This code is added for pmt-6575 to not to save the invoice into xml file.
					AND t9500.c9500_data_download_id = t9501.c9500_data_download_id
					AND t406.C1900_COMPANY_ID = v_company_id
					AND t9501.c9500_data_download_id = p_batchid
					AND t9500.C9500_VOID_FL IS NULL
                    AND t406.c406_void_fl IS NULL order by UPPER(vendor_name), t406.c406_invoice_id asc;--(asc order vendor name and invoice id)
		END IF;
	END gm_dd_fch_batch_dtl_list;

/*******************************************************
	  * Description : Procedure to get all batch list
	  * Author		: Xun
	  *******************************************************/
	PROCEDURE gm_dd_fch_batch_list (
		p_batchid			 IN 	  t9500_data_download_log.c9500_data_download_id%TYPE
	  , p_frmdt 			 IN 	  VARCHAR2
	  , p_todt				 IN 	  VARCHAR2
	  , p_out_invclist_cur	 OUT	  TYPES.cursor_type
	)
	AS
	  v_dateformat VARCHAR2(100);
      v_company_id  t1900_company.c1900_company_id%TYPE;
	BEGIN
		
		SELECT get_compid_frm_cntx(),get_compdtfmt_frm_cntx() 
		  INTO v_company_id,v_dateformat 
		  FROM dual;
		
		OPEN p_out_invclist_cur
		 FOR
			 SELECT   t9500.c9500_data_download_id batchnumber
					, TO_CHAR (t9500.c9500_download_dt, v_dateformat||' HH12:MI:SS AM') batch_datetime
					, t9500.c9500_download_identifier download_identifier
					, get_user_name (t9500.c9500_initiated_by) initiated_by, t9500.c9500_value batch_amount
				 FROM t9500_data_download_log t9500
				WHERE t9500.c9500_void_fl IS NULL
				  AND t9500.c9500_data_download_id = DECODE (p_batchid, 0, t9500.c9500_data_download_id, p_batchid)
				  AND TRUNC (t9500.c9500_download_dt) >=
								DECODE (p_frmdt
									  , NULL, TRUNC (t9500.c9500_download_dt)
									  , TO_DATE (p_frmdt, v_dateformat)
									   )
				  AND TRUNC (t9500.c9500_download_dt) <=
								  DECODE (p_todt
										, NULL, TRUNC (t9500.c9500_download_dt)
										, TO_DATE (p_todt, v_dateformat)
										 )
				  AND t9500.C1900_COMPANY_ID = v_company_id
			 ORDER BY t9500.c9500_data_download_id;
	END gm_dd_fch_batch_list;
END gm_pkg_cm_data_download;
/
