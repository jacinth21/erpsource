/* Formatted on 2010/02/02 15:02 (Formatter Plus v4.8.0) */
-- @"c:\database\packages\common\gm_pkg_cm_exchange_rate.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_cm_exchange_rate
IS
     /******************************************************************************
     * Description : Procedure to save real time exchange rate from OANDA API
     ****************************************************************************/
   PROCEDURE gm_sav_exchange_rate_dtl (
      p_base_curr      IN       t907_currency_conv.c907_curr_cd_from%TYPE,
      p_quote_curr     IN       t907_currency_conv.c907_curr_cd_to%TYPE,
      p_curr_value     IN       t907_currency_conv.c907_curr_value%TYPE,
      p_source	       IN       t907_currency_conv.C901_SOURCE%TYPE,
	  p_failure_fl	   IN	    t907_currency_conv.C907_FAILURE_FL%TYPE,
	  p_user_id        IN       t907_currency_conv.c907_created_by%TYPE	
    )
    AS
    	v_curr_value    t907_currency_conv.c907_curr_value%TYPE;
    	v_count NUMBER :=0;
    	v_company_id    t1900_company.c1900_company_id%TYPE;
    	v_sales_avg  t907_currency_conv.c907_curr_value%TYPE;
    	v_diff       NUMBER;
    	v_rule_diff  NUMBER;
   BEGIN
   
   
   		SELECT  get_compid_frm_cntx()
		  INTO  v_company_id
		  FROM DUAL;
	--to get the last three days of the month
   		SELECT TRUNC(LAST_DAY(CURRENT_DATE)) - TRUNC(CURRENT_DATE - 1) 
   		 INTO  v_diff
   		FROM dual; 
   	--to get the sales average days of the month
   		SELECT GET_RULE_VALUE('ER_DT_DIFF','ER_SALES_AVG') 
   		 INTO  v_rule_diff
   		FROM dual;
   
   -- Check if there are any record found for currency for current date.
	BEGIN
	
		SELECT count(1)
             INTO v_count
             FROM t907_currency_conv 
            WHERE c907_void_fl IS NULL
              AND (c907_from_date <= TRUNC(CURRENT_DATE) AND c907_to_date >= TRUNC(CURRENT_DATE))                  
              AND c907_curr_cd_from = p_base_curr
              AND c907_curr_cd_to = p_quote_curr
              AND c901_conversion_type = '101102'; 	--101102 Average			  
	
	
	 EXCEPTION
			 WHEN NO_DATA_FOUND THEN
				v_count := 0;
			 WHEN OTHERS THEN 
			 	v_count := 0;
		END;
	
	-- If record not exist for today , then do insert the Real time exchange rate.
	IF (v_count = 0)
	THEN	
   			--If the OANDA API webservice is failed then take the previous day exchange Rate and save 
   			--Else if the Source is SpineIT Average, then do average for Month 1st to 3 days prior for Month last date (last 3 days of the Month) 
   			--Else do insert the given exchange rate  
		   		IF p_failure_fl = 'Y' OR v_diff < v_rule_diff--get the previous days value for last days of the month to avoid repeated calculation
		   		THEN
		   		
		   		 BEGIN
		     			   SELECT c907_curr_value 
		     			   		 INTO v_curr_value
							 FROM t907_currency_conv 
							WHERE c907_curr_cd_from = p_base_curr
							  AND c907_curr_cd_to = p_quote_curr
							  AND (c907_from_date >= TRUNC(CURRENT_DATE-1) AND c907_to_date <= TRUNC(CURRENT_DATE-1))
							  AND c907_void_fl IS NULL;
		
				   EXCEPTION
						 WHEN NO_DATA_FOUND
						 THEN
							gm_send_exch_rate_failure_mail(p_base_curr,p_quote_curr,SQLERRM);
	 

					END;
		   		ELSIF p_source = '106910' AND v_diff = v_rule_diff--106910 SpineIT Average , get to take the sales from first - 28th  in Local currency and USD and do the sum and calculate the average. 
		   								  -- Sum(USD Sales)/Sum(Local Currency Sales) will be the average and we will use it for next 3 days.
		   		THEN
		   		
		   			BEGIN 
		   				   SELECT ROUND(USD_SALES/LOCAL_SALES,6) 
		   				   INTO v_sales_avg
		   				   FROM (
								 SELECT NVL( SUM( T502.C502_ITEM_PRICE * T502.C502_ITEM_QTY),0) LOCAL_SALES,
								 NVL( SUM( T502.C502_CORP_ITEM_PRICE * T502.C502_ITEM_QTY),0) USD_SALES
								FROM T501_ORDER T501,
								  T502_ITEM_ORDER T502
								WHERE T501.C501_ORDER_DATE BETWEEN trunc(last_day(CURRENT_DATE)-1, 'mm') -- 1st Day of the current month
								AND TRUNC(CURRENT_DATE) -- 3 days prior to last day of the current month
								AND T501.C501_ORDER_ID      = T502.C501_ORDER_ID
								  AND T501.C1900_COMPANY_ID  IN (
												      SELECT C1900_COMPANY_ID
												        FROM T1900_COMPANY
												       WHERE C901_TXN_CURRENCY = p_base_curr
												         AND C1900_VOID_FL      IS NULL
												  )
								AND NVL (T501.c901_order_type, -9999) NOT IN
												  (SELECT C906_RULE_VALUE FROM v901_ORDER_TYPE_GRP
												  )
								AND T501.C501_VOID_FL   IS NULL
								AND T501.C501_DELETE_FL IS NULL
								AND T502.C502_VOID_FL   IS NULL );
		   			EXCEPTION
						 WHEN NO_DATA_FOUND
						 THEN
							gm_send_exch_rate_failure_mail(p_base_curr,p_quote_curr,SQLERRM);
	 
					END;
					
					v_curr_value := v_sales_avg;
		   		
		   		ELSE
		   			v_curr_value := NVL(p_curr_value,1);
		   		END IF;	
		   		

			-- 101102 Average
	         INSERT INTO t907_currency_conv
	                     (c907_curr_seq_id, c907_curr_cd_from, c907_curr_cd_to,
	                      c907_from_date, c907_to_date, c907_curr_value, c901_conversion_type,
	                      c901_source,c907_failure_fl,c907_created_by, c907_created_date
	                     )
	              VALUES (s907_curr_conv.NEXTVAL, p_base_curr, p_quote_curr,
	                      TRUNC(CURRENT_DATE), TRUNC(CURRENT_DATE), v_curr_value, '101102',
	                      p_source,p_failure_fl,p_user_id, CURRENT_DATE 
	                     );
		   		
    ELSE
    	RETURN;
 	END IF;
 	
   END gm_sav_exchange_rate_dtl;
   /******************************************************************************
     * Description : Procedure to send a failure mail notification
     ****************************************************************************/      
PROCEDURE gm_send_exch_rate_failure_mail (
      p_base_curr      IN       t907_currency_conv.c907_curr_cd_from%TYPE,
      p_quote_curr     IN       t907_currency_conv.c907_curr_cd_to%TYPE,
	  p_failure_msg    IN       CLOB
	)
	AS
		--
		v_subject 	   t906_rules.c906_rule_value%TYPE;
		v_mail_body	   t906_rules.c906_rule_value%TYPE;
		v_to_mail 	   t906_rules.c906_rule_value%TYPE;
	--
	BEGIN
		SELECT GET_RULE_VALUE ('EXRT_NOTIF_SUB', 'EXRT_NOTIFICATION'),GET_RULE_VALUE ('EXRT_NOTIF_MSG', 'EXRT_NOTIFICATION'),
			GET_RULE_VALUE ('EXRT_NOTIF_TO', 'EXRT_NOTIFICATION')
		  INTO v_subject, v_mail_body, v_to_mail
		  FROM DUAL;

		
		v_subject := v_subject || ' for Base Currency : ' || GET_CODE_NAME_ALT(p_base_curr);
		--
		v_mail_body	:= v_mail_body||
			   'Base Currency : '||GET_CODE_NAME_ALT(p_base_curr)||' , Quote Currency : '||GET_CODE_NAME_ALT(p_quote_curr)||', Error Meassge : '||p_failure_msg;

		--
		gm_com_send_email_prc (v_to_mail, v_subject, v_mail_body);
	--
	END gm_send_exch_rate_failure_mail;
   /******************************************************************************
     * Description : Procedure to get the differnce date from the current date
     ****************************************************************************/      	
	PROCEDURE gm_get_current_date(
	 p_date_diff   IN NUMBER,
	 p_date_format IN VARCHAR2,
	 p_current_date OUT VARCHAR2
	)
	AS
	BEGIN
		SELECT TO_CHAR (TRUNC(CURRENT_DATE) - p_date_diff, p_date_format) 
			INTO p_current_date
		FROM DUAL;
		EXCEPTION
			WHEN NO_DATA_FOUND
		    THEN
		    p_current_date :=NULL;
	END;
END gm_pkg_cm_exchange_rate;
/
