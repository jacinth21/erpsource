/* Formatted on 2009/06/25 11:32 (Formatter Plus v4.8.0) */
--@"C:\database\Packages\Common\gm_pkg_cm_filter.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_cm_filter
IS
/*******************************************************
	* Description : Procedure to fetch conditions for group
	* Author		: Ritesh Shah
	*******************************************************/
	PROCEDURE gm_fch_conditions (
		p_outconditions   OUT	TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_outconditions
		 FOR
			 SELECT t9701.c9701_condition_id codeid, t9701.c9701_condition_name codenm
			   FROM t9701_condition t9701
			  WHERE t9701.c9701_void_fl IS NULL;
	END gm_fch_conditions;

	/*******************************************************
	* Description : Procedure to fetch operator and values for group
	* Author		: Ritesh Shah
	*******************************************************/
	PROCEDURE gm_fch_operators (
		p_conditionid	 IN 	  t9701_condition.c9701_condition_id%TYPE
	  , p_group 		 IN 	  t9001_filter_lookup.c9001_filter_grp%TYPE
	  , p_outoperators	 OUT	  TYPES.cursor_type
	)
	AS
		v_filter_id    NUMBER;
		v_codeid	   NUMBER;
		v_prc_nm	   VARCHAR2 (100);
		v_inparam	   VARCHAR2 (20);
	BEGIN
		BEGIN
			SELECT t9001.c9001_filter_id, t9001.c901_parameter_type_id, c9001_procedure_name
			  INTO v_filter_id, v_codeid, v_prc_nm
			  FROM t9001_filter_lookup t9001
			 WHERE t9001.c9001_ref_id = p_conditionid AND t9001.c9001_filter_grp = p_group;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				v_filter_id := 0;
		END;

		OPEN p_outoperators
		 FOR
			 SELECT c901_operator_id codeid, get_code_name (c901_operator_id) codenm
			   FROM t9002_filter_operator t9002
			  WHERE t9002.c9001_filter_id = v_filter_id;
	END gm_fch_operators;

	/*******************************************************
	* Description : Procedure to fetch operator and values for group
	* Author		: Ritesh Shah
	*******************************************************/
	PROCEDURE gm_fch_values (
		p_conditionid	IN		 t9701_condition.c9701_condition_id%TYPE
	  , p_group 		IN		 t9001_filter_lookup.c9001_filter_grp%TYPE
	  , p_outvalues 	OUT 	 TYPES.cursor_type
	)
	AS
		v_filter_id    NUMBER;
		v_codeid	   NUMBER;
		v_prc_nm	   VARCHAR2 (100);
		v_inparam	   VARCHAR2 (20);
		v_prc_param    VARCHAR2 (4000);
	BEGIN
		BEGIN
			SELECT t9001.c9001_filter_id, t9001.c901_parameter_type_id, c9001_procedure_name, c9001_proc_in_param
			  INTO v_filter_id, v_codeid, v_prc_nm, v_prc_param
			  FROM t9001_filter_lookup t9001
			 WHERE t9001.c9001_ref_id = p_conditionid AND t9001.c9001_filter_grp = p_group;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				v_filter_id := 0;
		END;

		OPEN p_outvalues
		 FOR
			 SELECT c901_operator_id codeid, get_code_name (c901_operator_id) codenm
			   FROM t9002_filter_operator t9002
			  WHERE t9002.c9001_filter_id = 0;

		IF v_codeid = 6100
		THEN
			IF p_conditionid = 3   --For country
			THEN
				-- change after discussion with prasath.
				gm_pkg_cm_codelookup_util.gm_cm_fch_allcodealtnmlist (v_prc_param, p_outvalues);
			ELSIF p_conditionid = 4   --For vendor list
			THEN
				gm_pkg_cm_rule_conditions.gm_op_fch_vendorlist (p_outvalues);
			ELSIF p_conditionid = 7   -- for set master
			THEN
				gm_pkg_cm_rule_conditions.gm_pd_fch_setlist (p_outvalues);
			END IF;
		END IF;
	END gm_fch_values;
END gm_pkg_cm_filter;
/
