/* Formatted on 2009/09/17 10:48 (Formatter Plus v4.8.0) */
-- @"C:\database\Packages\Accounting\gm_pkg_ac_sales_tax.pkg"
CREATE OR REPLACE
PACKAGE gm_pkg_ac_sales_tax
IS
    --
    /*****************************************************************************************
    * Description : Procedure to fetch the order item details
    ******************************************************************************************/
PROCEDURE gm_fch_order_item_dtls (
        p_order_id IN t501_order.c501_order_id%TYPE,
        p_out_item_dtls OUT TYPES.cursor_type) ;
    /*****************************************************************************************
    * Description : Procedure to update the VAT rate to item order table
    ******************************************************************************************/
PROCEDURE gm_upd_order_item_vat_rate (
        p_invoice_id IN T503_INVOICE.C503_INVOICE_ID%TYPE,
        p_inputstr   IN CLOB,
        p_user_id    IN T503_INVOICE.C503_LAST_UPDATED_BY%TYPE) ;
    /*******************************************************
    * Description : Get the invoice tax flag
    * Author   : Mani
    *******************************************************/
    FUNCTION get_invoice_tax_fl (
            p_invoice_id IN T503_INVOICE.C503_INVOICE_ID%TYPE)
        RETURN VARCHAR2;
        /*******************************************************
        * Description : Get the taxable country
        * Author   : Mani
        *******************************************************/
    FUNCTION get_tax_country_fl (
            p_acc_id IN T501_ORDER.C704_ACCOUNT_ID%TYPE)
        RETURN VARCHAR2;
        /*****************************************************************************************
        * Description : Procedure to fetch taxable country invoice details
        ******************************************************************************************/
    PROCEDURE gm_fch_tax_invoice_dtls (
            p_invoice_id IN t501_order.c503_invoice_id%TYPE,
            p_out_tax_dtls OUT TYPES.cursor_type) ;
        /*******************************************************
        * Description : Based on invoice get the min orders date
        * Author   : Mani
        *******************************************************/
    FUNCTION get_min_orders_date (
            p_invoice_id IN t501_order.c503_invoice_id%TYPE)
        RETURN DATE;
        /*******************************************************
        * Description : Get the code id from code group and alt
        * Author   : Mani
        *******************************************************/
    FUNCTION get_code_id_from_code_alt (
            p_code_grp IN t901_code_lookup.c901_code_grp%TYPE,
            p_code_alt IN t901_code_lookup.c902_code_nm_alt%TYPE)
        RETURN VARCHAR2;
        /*****************************************************************************************
        * Description : Procedure to update the invoice tax flag
        ******************************************************************************************/
    PROCEDURE gm_upd_inv_tax_fl (
            p_invoice_id IN t501_order.c503_invoice_id%TYPE,
            p_flag       IN VARCHAR2,
            p_user_id    IN t503_invoice.c503_last_updated_by%TYPE) ;
        /*****************************************************************************************
        * Description : Procedure to update orders tax cost
        ******************************************************************************************/
    PROCEDURE gm_upd_orders_tax_cost (
            p_invoice_id IN t501_order.c503_invoice_id%TYPE,
            p_user_id    IN t501_order.c501_last_updated_by%TYPE) ;
        /*****************************************************************************************
        * Description : Procedure to void the invoices
        ******************************************************************************************/
    PROCEDURE gm_void_tax_error_invoice (
            p_invoice_id IN t501_order.c503_invoice_id%TYPE,
            p_comments   IN VARCHAR2,
            p_user_id    IN t501_order.c501_last_updated_by%TYPE) ;
        /*****************************************************************************************
        * Description : Procedure to roll back the return details
        ******************************************************************************************/
    PROCEDURE gm_rollback_return (
            p_rma_id  IN t506_returns.c506_rma_id%TYPE,
            p_user_id IN t506_returns.c506_last_updated_by%TYPE) ;
        /*****************************************************************************************
        * Description : Procedure to update valid shipping address to temp table
        ******************************************************************************************/
    PROCEDURE gm_upd_temp_ship_info (
            p_inputstr IN CLOB,
            p_user_id  IN T503_INVOICE.C503_LAST_UPDATED_BY%TYPE) ;
        /*****************************************************************************************
        * Description : Procedure to fetch account details
        ******************************************************************************************/
    PROCEDURE gm_fch_account_dtls (
            p_out_acc_dtls OUT TYPES.cursor_type) ;
/*******************************************************
* Description : Get the exemption flag from order table
* Author   : Mani
*******************************************************/
FUNCTION get_parent_ord_tax_cost  (
        p_invoice_id IN t501_order.c503_invoice_id%TYPE)
    RETURN NUMBER;            
    END gm_pkg_ac_sales_tax;
    /