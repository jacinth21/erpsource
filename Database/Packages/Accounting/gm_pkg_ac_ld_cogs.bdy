/* Formatted on 2008/10/12 12:18 (Formatter Plus v4.8.0) */
--@"C:\database\packages\accounting\gm_pkg_ac_ld_cogs.bdy"
-- exec gm_pkg_ac_ld_cogs.gm_ld_main(20431);

CREATE OR REPLACE PACKAGE BODY gm_pkg_ac_ld_cogs
IS
	/*******************************************************
	* Purpose: This procedure will be main procedure to
	*			load COGS information
	*******************************************************/
	PROCEDURE gm_ld_main (
		p_lock_type   IN   t250_inventory_lock.c901_lock_type%TYPE,
        p_company_id IN t1900_company.c1900_company_id%TYPE,
        p_plant_id IN t5040_plant_master.c5040_plant_id%TYPE
	)
	AS
		v_cogs_lock_id t256_cogs_lock.c256_cogs_lock_id%TYPE;
		v_start_time   DATE;
		v_us_time_zone t901_code_lookup.c901_code_nm%TYPE;
   	 	v_us_time_format t901_code_lookup.c901_code_nm%TYPE;
	BEGIN
		v_start_time := SYSDATE;

		SELECT GET_CODE_NAME(C901_TIMEZONE), GET_CODE_NAME(C901_DATE_FORMAT)
	        INTO v_us_time_zone,v_us_time_format
	     FROM t1900_company
	     WHERE c1900_company_id=p_company_id;
     
     --Set Context
     gm_pkg_cor_client_context.gm_sav_client_context(p_company_id,v_us_time_zone,v_us_time_format,p_plant_id);
     
		SELECT s256_cogs_lock.NEXTVAL
		  INTO v_cogs_lock_id
		  FROM DUAL;

--
		INSERT INTO t256_cogs_lock
					(c256_cogs_lock_id, c256_lock_dt, c901_lock_type,c1900_company_id,c5040_plant_id
					)
			 VALUES (v_cogs_lock_id, TRUNC (SYSDATE), p_lock_type,NULL,p_plant_id
					);

		IF p_lock_type = 20431
		THEN
			gm_pkg_ac_ld_cogs.gm_ld_cogs (v_cogs_lock_id);
		END IF;

		gm_pkg_cm_job.gm_cm_notify_load_info (v_start_time, SYSDATE, 'S', 'SUCCESS', 'gm_pkg_ac_ld_cogs');
		COMMIT;
	EXCEPTION
		WHEN OTHERS
		THEN
			ROLLBACK;
			--
			gm_pkg_cm_job.gm_cm_notify_load_info (v_start_time, SYSDATE, 'E', SQLERRM, 'gm_pkg_ac_ld_cogs');
			COMMIT;
	--
	END gm_ld_main;

	/*******************************************************
	* Purpose: This procedure will be main procedure to
	*			lock COGS
	*******************************************************/
	PROCEDURE gm_ld_cogs (
		p_cogs_lock_id	 IN   t256_cogs_lock.c256_cogs_lock_id%TYPE
	)
	AS
	v_company_id  t1900_company.c1900_company_id%TYPE;
	v_plant_id    t5040_plant_master.c5040_plant_id%TYPE;
		CURSOR cogs_lock_cur_active
		IS
			SELECT t820.c205_part_number_id pnum, c820_purchase_amt purcamt, t820.c820_part_received_dt received_date
			  FROM t820_costing t820, (SELECT	MIN (c820_costing_id) costing_id
										   FROM t820_costing
										  WHERE c901_costing_type IN (select token from v_in_list) 
										  AND c901_status IN (4800, 4801)
										  AND c1900_company_id=v_company_id
									   GROUP BY c205_part_number_id) minpart
			 WHERE t820.c820_costing_id = minpart.costing_id
			 AND t820.c1900_company_id=v_company_id;

		CURSOR cogs_lock_cur_closed
		IS
			SELECT t820.c205_part_number_id pnum, c820_purchase_amt purcamt, t820.c820_part_received_dt received_date
			  FROM t820_costing t820, (SELECT	t820.c205_part_number_id, MAX (t820.c820_costing_id) costing_id
										   FROM t820_costing t820
										  WHERE t820.c901_costing_type IN (select token from v_in_list)
										  AND t820.c1900_company_id=v_company_id
									   GROUP BY t820.c205_part_number_id) maxpart
			 WHERE t820.c820_costing_id = maxpart.costing_id
			   AND t820.c205_part_number_id NOT IN (
												  SELECT t820.c205_part_number_id
													FROM t820_costing t820
												   WHERE c901_costing_type IN (select token from v_in_list)
													 AND c901_status IN (4800, 4801)
													 AND t820.c1900_company_id=v_company_id)
            	AND t820.c1900_company_id=v_company_id;
           --
         CURSOR cogs_lock_cur_closed_archive
		IS
			SELECT t820.c205_part_number_id pnum, c820_purchase_amt purcamt, t820.c820_part_received_dt received_date
			  FROM t820_costing_archive t820, (SELECT	t820.c205_part_number_id, MAX (t820.c820_costing_id) costing_id
										   FROM t820_costing_archive t820
										  WHERE t820.c901_costing_type IN (select token from v_in_list)
										  AND t820.c1900_company_id=v_company_id
									   GROUP BY t820.c205_part_number_id) maxpart
			 WHERE t820.c820_costing_id = maxpart.costing_id
			   AND t820.c205_part_number_id NOT IN (
												  SELECT t820.c205_part_number_id
													FROM t820_costing t820
												   WHERE c901_costing_type IN (select token from v_in_list)
													 --AND c901_status IN (4800, 4801)
													 AND t820.c1900_company_id=v_company_id)
            	AND t820.c1900_company_id=v_company_id;
            	
	BEGIN
		
		SELECT GET_COMPID_FRM_CNTX(), GET_PLANTID_FRM_CNTX()
	      INTO v_company_id, v_plant_id
	      FROM DUAL;
        -- to set the costing type (FG and Mfg Raw material)
      	my_context.set_my_inlist_ctx ('4900,4909');
	      
		FOR current_row IN cogs_lock_cur_active
		LOOP
			gm_pkg_ac_ld_cogs.gm_sav_cogs_details (p_cogs_lock_id
												 , current_row.pnum
												 , current_row.purcamt
												 , current_row.received_date
												 , 20450   -- Available FG
												  );
		END LOOP;

		FOR current_row IN cogs_lock_cur_closed
		LOOP
			gm_pkg_ac_ld_cogs.gm_sav_cogs_details (p_cogs_lock_id
												 , current_row.pnum
												 , current_row.purcamt
												 , current_row.received_date
												 , 20450   -- Raw Material
												  );
		END LOOP;
		
		--
		FOR current_row IN cogs_lock_cur_closed_archive
		LOOP
			gm_pkg_ac_ld_cogs.gm_sav_cogs_details (p_cogs_lock_id
												 , current_row.pnum
												 , current_row.purcamt
												 , current_row.received_date
												 , 20450   -- Raw Material
												  );
		END LOOP;
	END gm_ld_cogs;

	/*******************************************************
	* Purpose: This procedure will be used to
	*		   lock COGS detail
	*******************************************************/
	PROCEDURE gm_sav_cogs_details (
		p_cogs_lock_id	  IN   t256_cogs_lock.c256_cogs_lock_id%TYPE
	  , p_pnum			  IN   t205_part_number.c205_part_number_id%TYPE
	  , p_purcamt		  IN   t820_costing.c820_purchase_amt%TYPE
	  , p_received_date   IN   t820_costing.c820_part_received_dt%TYPE
	  , p_type			  IN   t257_cogs_detail.c901_type%TYPE
	)
	AS
	BEGIN
		INSERT INTO t257_cogs_detail
					(c257_cogs_lock_id, c256_cogs_lock_id, c205_part_number_id, c257_cogs_amount, c901_type
				   , c257_posted_date
					)
			 VALUES (s257_cogs_detail.NEXTVAL, p_cogs_lock_id, p_pnum, p_purcamt, p_type
				   , p_received_date
					);
	END gm_sav_cogs_details;
END gm_pkg_ac_ld_cogs;
/
