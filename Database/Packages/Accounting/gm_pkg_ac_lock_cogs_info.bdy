/* Formatted on 2008/09/26 11:25 (Formatter Plus v4.8.0) */
-- @"C:\DATABASE\packages\Accounting\gm_pkg_ac_lock_cogs_info.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_ac_lock_cogs_info
IS			/*******************************************************
	 * Purpose: Procedure is used to fetch Locked Pricing details
	   Author : Brinal
	 *******************************************************/
--
	PROCEDURE gm_fch_locked_cogdetails (
		p_partnumber   IN		t205_part_number.c205_part_number_id%TYPE
	  , p_project_id   IN		t205_part_number.c202_project_id%TYPE
	  , p_lockid	   IN		t257_cogs_detail.c256_cogs_lock_id%TYPE
	  , p_out_cur	   OUT		TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_out_cur
		 FOR
			 SELECT t257.c205_part_number_id ID, t205.c205_part_num_desc description, t901.c901_code_nm itype
				  , t257.c257_cogs_amount COST, t257.c257_posted_date postdate
			   FROM t257_cogs_detail t257, t205_part_number t205, t901_code_lookup t901
			  WHERE t257.c205_part_number_id = t205.c205_part_number_id
				AND t257.c901_type = t901.c901_code_id
				AND t257.c256_cogs_lock_id = p_lockid
			AND regexp_like (t257.c205_part_number_id, NVL (REGEXP_REPLACE(p_partnumber,'[+]','\+'), REGEXP_REPLACE(t257.c205_part_number_id,'[+]','\+')))
				AND t205.c202_project_id = DECODE (p_project_id, '0', t205.c202_project_id, p_project_id);
	END gm_fch_locked_cogdetails;

--
  /*******************************************************
   * Description : Procedure to fetch cogs lock details
   * Author 	 : Brinal
   *******************************************************/
	PROCEDURE gm_fc_fch_cogslockids (
		p_locktype		  IN	   t250_inventory_lock.c901_lock_type%TYPE
	  , p_outinvlockids   OUT	   TYPES.cursor_type
	)
	AS
	 v_company_id  t1900_company.c1900_company_id%TYPE;
	 v_plant_id    t5040_plant_master.C5040_PLANT_ID %TYPE;
	 v_comp_date_fmt	VARCHAR2 (20);
	
	BEGIN
	    select get_compid_frm_cntx(),get_plantid_frm_cntx(), get_compdtfmt_frm_cntx()
		into v_company_id,v_plant_id, v_comp_date_fmt
		from dual;
	
		OPEN p_outinvlockids
		 FOR
			 SELECT c256_cogs_lock_id lockid, TO_CHAR (c256_lock_dt, v_comp_date_fmt) lockdt
			   FROM t256_cogs_lock
			  WHERE c901_lock_type = p_locktype 
			  AND  c5040_plant_id    = v_plant_id
			  AND c256_void_fl IS NULL;
	END gm_fc_fch_cogslockids;
--
END gm_pkg_ac_lock_cogs_info;
/