/* Formatted on 2009/11/25 16:17 (Formatter Plus v4.8.0) */
-- @"c:\database\packages\accounting\gm_pkg_ac_patag_master_info.bdy"
CREATE OR REPLACE
PACKAGE BODY gm_pkg_ac_patag_master_info
IS
    --
    /*******************************************************
    * Description : Procedure to get all the audit locations
    * Author   : VPrasath
    *******************************************************/
    --
PROCEDURE gm_fch_audit_locations (
        p_physical_audit_id IN t2603_audit_location.c2600_physical_audit_id%TYPE,
        p_out_cur OUT TYPES.cursor_type)
AS
BEGIN
    OPEN p_out_cur FOR SELECT c901_location_id codeid, c2603_identifier || '-' || get_code_name (c901_location_id)
    codenm FROM t2603_audit_location WHERE c2600_physical_audit_id = p_physical_audit_id ORDER BY TO_NUMBER (
    c2603_identifier) ;
END gm_fch_audit_locations;
--
/*******************************************************
* Description : Procedure to get audit locations
*      for the type
* Author   : VPrasath
*******************************************************/
--
PROCEDURE gm_fch_audit_locations_type (
        p_physical_audit_id IN t2603_audit_location.c2600_physical_audit_id%TYPE,
        p_location_id       IN t2603_audit_location.c901_location_id%TYPE,
        p_out_cur OUT TYPES.cursor_type)
AS
    v_type t2603_audit_location.c901_type%TYPE;
BEGIN
     SELECT c901_type
       INTO v_type
       FROM t2603_audit_location
      WHERE c901_location_id        = p_location_id
        AND c2600_physical_audit_id = p_physical_audit_id;
    OPEN p_out_cur FOR SELECT c901_location_id codeid,
    c2603_identifier || '-' || get_code_name (c901_location_id) codenm FROM t2603_audit_location WHERE
    c2600_physical_audit_id = p_physical_audit_id AND c901_type = v_type;
END gm_fch_audit_locations_type;
--
/*******************************************************
* Description : Procedure to get all the audit users
* Author   : VPrasath
*******************************************************/
--
PROCEDURE gm_fch_counted_by (
        p_physical_audit_id IN t2603_audit_location.c2600_physical_audit_id%TYPE,
        p_out_cur OUT TYPES.cursor_type)
AS
BEGIN
    OPEN p_out_cur FOR SELECT c101_user_id codeid,
    c2601_audit_user_id || ' - ' || get_user_name (c101_user_id) codenm FROM t2601_audit_user WHERE
    c2600_physical_audit_id = p_physical_audit_id ORDER BY get_user_name (c101_user_id) ;
END gm_fch_counted_by;
--
/*******************************************************
* Description : Procedure to get all the open audits
* Author  : VPrasath
*******************************************************/
--
PROCEDURE gm_fch_open_physical_audits (
        p_out_cur OUT TYPES.cursor_type)
AS
 v_company_id  t1900_company.c1900_company_id%TYPE;
 v_plant_id    T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
 
    	
BEGIN
select get_compid_frm_cntx(),get_plantid_frm_cntx() 
		into v_company_id,v_plant_id 
		from dual;
  
    OPEN p_out_cur FOR SELECT c2600_physical_audit_id codeid,
    c2600_physical_audit_nm codenm FROM t2600_physical_audit 
    WHERE c901_audit_status = 1 
     AND c1900_company_id  =  v_company_id
     AND c5040_plant_id    = v_plant_id
     AND c2600_void_fl IS NULL
     ORDER BY TO_NUMBER(c2600_physical_audit_id) DESC;
      
END gm_fch_open_physical_audits;
--
/*******************************************************
* Description : Procedure to get MA supervisor
* Author   : VPrasath
*******************************************************/
--
PROCEDURE gm_fch_ma_supervisor (
        p_out_cur OUT TYPES.cursor_type)
AS
BEGIN
    OPEN p_out_cur FOR SELECT c101_mapped_party_id codeid,
    get_user_name (c101_mapped_party_id) codenm FROM t1501_group_mapping WHERE c1500_group_id = 'PA_MA_SUPERVISOR'
    ORDER BY get_user_name (c101_mapped_party_id) ;
END gm_fch_ma_supervisor;
--
END gm_pkg_ac_patag_master_info;
/
