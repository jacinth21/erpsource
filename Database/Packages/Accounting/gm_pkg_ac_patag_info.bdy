/* Formatted on 2010/11/16 11:51 (Formatter Plus v4.8.0) */
--@"C:\Database\Packages\Accounting\gm_pkg_ac_patag_info.bdy";
CREATE OR REPLACE
PACKAGE BODY gm_pkg_ac_patag_info
IS
    --
PROCEDURE gm_fch_lock_tag_info (
        p_physical_audit_id IN t2603_audit_location.c2600_physical_audit_id%TYPE,
        p_txnid             IN t2605_audit_tag_detail.c2605_txn_id%TYPE,
        p_location_id       IN t2603_audit_location.c901_location_id%TYPE,
        p_proj_id           IN t205_part_number.c202_project_id%TYPE,
        p_byPart_flag       IN CHAR,
        p_out_cur OUT TYPES.cursor_type,
        p_ma_status OUT t2603_audit_location.c2603_ma_status_fl%TYPE)
AS
    v_inv_lock_id t2600_physical_audit.c250_inventory_lock_id%TYPE;
    v_cogs_lock_id t2600_physical_audit.c256_cogs_lock_id%TYPE;
    v_location_type t2603_audit_location.c901_type%TYPE;
    v_company_id  t1900_company.c1900_company_id%TYPE;
    v_plant_id    t5040_plant_master.c5040_plant_id%TYPE;
BEGIN
SELECT GET_COMPID_FRM_CNTX(), GET_PLANTID_FRM_CNTX()
	    INTO v_company_id, v_plant_id
	 FROM DUAL;
          
     SELECT c2603_ma_status_fl
       INTO p_ma_status
       FROM t2603_audit_location t2603
      WHERE t2603.c2600_physical_audit_id = p_physical_audit_id
        AND t2603.c901_location_id        = p_location_id;
     
	     SELECT c250_inventory_lock_id, c256_cogs_lock_id
	       INTO v_inv_lock_id, v_cogs_lock_id
	       FROM t2600_physical_audit
	      WHERE c2600_physical_audit_id = p_physical_audit_id
	        AND C1900_COMPANY_ID = v_company_id
	        AND c5040_plant_id    = v_plant_id
	        AND c2600_void_fl          IS NULL;
         
     SELECT c901_type
       INTO v_location_type
       FROM t2603_audit_location
      WHERE c2600_physical_audit_id = p_physical_audit_id
        AND c901_location_id        = p_location_id;
        
    gm_sav_filter_insert (p_physical_audit_id, p_txnid, p_proj_id, p_location_id, v_location_type, v_inv_lock_id,
    p_byPart_flag) ;
    IF (v_location_type = 50660) -- for Part
        THEN
        gm_fch_lock_tag_by_part (p_physical_audit_id, p_location_id, v_inv_lock_id, v_cogs_lock_id, p_out_cur) ;
    ELSIF (v_location_type = 50661) -- for Transaction
        THEN
        gm_fch_lock_tag_by_txn (p_physical_audit_id, p_location_id, v_inv_lock_id, v_cogs_lock_id, p_out_cur) ;
    ELSIF (v_location_type = 50662) -- for Part & Location
        THEN
        gm_fch_lock_tag_by_partref (p_physical_audit_id, p_location_id, v_inv_lock_id, v_cogs_lock_id, p_byPart_flag,
        p_out_cur) ;
    END IF;
END gm_fch_lock_tag_info;
--
/*******************************************************
* Purpose: Procedure is used to insert part#/transaction ID
to my_temp_list table
*******************************************************/
--
PROCEDURE gm_sav_filter_insert (
        p_physical_audit_id IN t2603_audit_location.c2600_physical_audit_id%TYPE,
        p_txnid             IN t2605_audit_tag_detail.c2605_txn_id%TYPE,
        p_proj_id           IN t205_part_number.c202_project_id%TYPE,
        p_location_id       IN t2603_audit_location.c901_location_id%TYPE,
        p_location_type     IN t2603_audit_location.c901_type%TYPE,
        p_inv_lock_id       IN t2600_physical_audit.c250_inventory_lock_id%TYPE,
        p_byPart_flag       IN CHAR)
AS
v_company_id  t1900_company.c1900_company_id%TYPE;
v_plant_id    t5040_plant_master.c5040_plant_id%TYPE;

BEGIN
SELECT GET_COMPID_FRM_CNTX(), GET_PLANTID_FRM_CNTX()
	    INTO v_company_id, v_plant_id
	 FROM DUAL;

     DELETE FROM my_temp_list;
     DELETE FROM MY_TEMP_KEY_VALUE;
    IF (p_location_type = 50660) THEN
         INSERT INTO my_temp_list
            (my_temp_txn_id
            )
         SELECT t251.c205_part_number_id
           FROM t251_inventory_lock_detail t251, t205_part_number t205, T250_INVENTORY_LOCK t250
          WHERE t251.c250_inventory_lock_id = p_inv_lock_id
            AND t251.c901_type              = p_location_id
            AND t251.c250_inventory_lock_id = t250.c250_inventory_lock_id
            AND t250.c5040_plant_id    = v_plant_id
            AND t251.c205_part_number_id    = t205.c205_part_number_id
            AND t205.c202_project_id        = DECODE (p_proj_id, '0', t205.c202_project_id, p_proj_id)
            AND t251.c251_qty               > 0
            AND t251.c205_part_number_id LIKE NVL (p_txnid, '%')
          UNION
         SELECT t2605.c205_part_number_id
           FROM t2605_audit_tag_detail t2605, t205_part_number t205
          WHERE t2605.c2600_physical_audit_id = p_physical_audit_id
            AND t2605.c205_part_number_id     = t205.c205_part_number_id
            AND t205.c202_project_id          = DECODE (p_proj_id, '0', t205.c202_project_id, p_proj_id) ---NVL (
            -- p_proj_id, t205.c202_project_id)
            AND t2605.c2605_qty > 0
            AND t2605.c901_location_id          = p_location_id
            AND t2605.c205_part_number_id LIKE NVL (p_txnid, '%') 
            AND t2605.c5040_plant_id    = v_plant_id;
    ELSIF (p_location_type = 50661) THEN
         INSERT INTO my_temp_list
            (my_temp_txn_id
            )
         SELECT txnid.c504_consignment_id
           FROM
            (
                 SELECT t253a.c504_consignment_id, t253a.c207_set_id
                   FROM t253a_consignment_lock t253a
                  WHERE t253a.c250_inventory_lock_id = p_inv_lock_id
                    AND t253a.c901_location_type     = p_location_id
                    AND t253a.c5040_plant_id    = v_plant_id
            )
            txnid, (
                 SELECT t207.c207_set_id, t207.c207_set_nm, t207.c202_project_id
                   FROM t207_set_master t207
            )
            setnm
          WHERE txnid.c207_set_id                   = setnm.c207_set_id(+)
            AND NVL (setnm.c202_project_id, '-999') = DECODE (p_proj_id, '0', NVL (setnm.c202_project_id, '-999'),
            p_proj_id) --'GM03-01'
            AND (txnid.c207_set_id LIKE NVL (p_txnid, '%')
            OR txnid.c207_set_id IS NULL) ;
    ELSIF (p_location_type        = 50662) THEN
         INSERT INTO MY_TEMP_KEY_VALUE
            (MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
            )
         SELECT t253b.c504_consignment_id, t253b.c205_part_number_id
           FROM t253a_consignment_lock t253a, t253b_item_consignment_lock t253b, t205_part_number t205
          WHERE t253a.c250_inventory_lock_id = p_inv_lock_id
            AND t253b.C250_INVENTORY_LOCK_ID = p_inv_lock_id
            AND t253a.c901_location_type     = p_location_id
	        AND t253b.C901_LOCATION_TYPE = p_location_id
            AND t253a.c5040_plant_id    = v_plant_id
            AND t253a.c504_consignment_id    = t253b.c504_consignment_id
            AND t253b.c504_consignment_id    = DECODE (p_byPart_flag, 'Y', t253b.c504_consignment_id, NVL (p_txnid,
            t253b.c504_consignment_id))
            AND t253b.c205_part_number_id = DECODE (p_byPart_flag, 'Y', NVL (p_txnid, t253b.c205_part_number_id),
            t253b.c205_part_number_id)
            AND t253b.c205_part_number_id = t205.c205_part_number_id
            AND t205.c202_project_id      = DECODE (p_proj_id, '0', t205.c202_project_id, p_proj_id)
       GROUP BY t253b.c504_consignment_id, t253b.c205_part_number_id;
    END IF;
END gm_sav_filter_insert;
/*******************************************************
* Purpose: Procedure is used to fetch Inventory Lock and
Tag info by Part with location
*******************************************************/
--
PROCEDURE gm_fch_lock_tag_by_partref (
        p_physical_audit_id IN t2603_audit_location.c2600_physical_audit_id%TYPE,
        p_location_id       IN t2603_audit_location.c901_location_id%TYPE,
        p_inv_lock_id       IN t2600_physical_audit.c250_inventory_lock_id%TYPE,
        p_cogs_lock_id      IN t2600_physical_audit.c256_cogs_lock_id%TYPE,
        p_byPart_flag       IN CHAR,
        p_out_cur OUT TYPES.cursor_type)
AS
v_company_id  t1900_company.c1900_company_id%TYPE;
 v_plant_id    t5040_plant_master.c5040_plant_id%TYPE;
 
BEGIN
SELECT GET_COMPID_FRM_CNTX(), GET_PLANTID_FRM_CNTX()
	    INTO v_company_id, v_plant_id
	 FROM DUAL;

    OPEN p_out_cur FOR SELECT ID,
    DECODE (p_byPart_flag, 'Y', '', REFID) REFID,
    detail,
    WAREHOUSE,
    beforecount,
    aftercount,
    devcount,
    devdollar,
    UnitCost,
    ExtendedCost,
    reconcileid,
    reconcilenm FROM
    (
         SELECT mylist.MY_TEMP_TXN_VALUE ID, mylist.MY_TEMP_TXN_KEY REFID, get_partnum_desc (mylist.MY_TEMP_TXN_VALUE)
            detail, get_code_name (beforecount.c901_location_type) WAREHOUSE, NVL (beforecount.before_count, 0)
            beforecount, NVL (aftercount.c2605_qty, 0) aftercount, (NVL (aftercount.c2605_qty, 0)                  - NVL (
            beforecount.before_count, 0)) devcount, (NVL (aftercount.c2605_qty, 0)                                 - NVL (
            beforecount.before_count, 0))                                                                          * NVL (
            cogs.cogs_amount, 0) devdollar, NVL (cogs.cogs_amount, 0) UnitCost, (NVL (beforecount.before_count, 0) *
            NVL (cogs.cogs_amount, 0)) ExtendedCost, reconciled.c901_reconcile_id reconcileid, reconciled.c901_code_nm
            reconcilenm
           FROM MY_TEMP_KEY_VALUE mylist, (
                 SELECT mylist.MY_TEMP_TXN_KEY, mylist.MY_TEMP_TXN_VALUE, SUM (t253b.c505_item_qty) before_count
                  , t253a.c901_location_type
                   FROM t253a_consignment_lock t253a, t253b_item_consignment_lock t253b, MY_TEMP_KEY_VALUE mylist
                  WHERE t253a.c250_inventory_lock_id = p_inv_lock_id
                    AND t253b.C250_INVENTORY_LOCK_ID = p_inv_lock_id
                    AND t253a.c901_location_type     = p_location_id
		            AND t253b.C901_LOCATION_TYPE = p_location_id
                    AND t253a.c5040_plant_id    = v_plant_id
                    AND t253a.c504_consignment_id    = t253b.c504_consignment_id
                    AND t253b.c504_consignment_id    = DECODE (p_byPart_flag, 'Y', t253b.c504_consignment_id,
                    mylist.MY_TEMP_TXN_KEY)
                    AND t253b.c205_part_number_id = mylist.MY_TEMP_TXN_VALUE
               GROUP BY mylist.MY_TEMP_TXN_KEY, mylist.MY_TEMP_TXN_VALUE, t253a.c901_location_type
            )
            beforecount, (
                 SELECT mylist.MY_TEMP_TXN_KEY, mylist.MY_TEMP_TXN_VALUE, SUM (t2605.c2605_qty) c2605_qty
                   FROM t2605_audit_tag_detail t2605, MY_TEMP_KEY_VALUE mylist
                  WHERE t2605.c2600_physical_audit_id = p_physical_audit_id
                    AND t2605.c901_location_id        = p_location_id
                    AND t2605.c901_type               = 50675 -- regular
                    AND t2605.c5040_plant_id    = v_plant_id
                    AND t2605.c2605_void_fl          IS NULL
                    AND t2605.c2605_txn_id            = DECODE (p_byPart_flag, 'Y', t2605.c2605_txn_id,
                    mylist.MY_TEMP_TXN_KEY)
                    AND t2605.C205_PART_NUMBER_ID = mylist.MY_TEMP_TXN_VALUE
               GROUP BY mylist.MY_TEMP_TXN_KEY, mylist.MY_TEMP_TXN_VALUE
            )
            aftercount, (
                 SELECT mylist.MY_TEMP_TXN_KEY, mylist.MY_TEMP_TXN_VALUE, NVL (t257.c257_cogs_amount, 0) cogs_amount
                   FROM t257_cogs_detail t257, t253b_item_consignment_lock t253b, MY_TEMP_KEY_VALUE mylist
                  WHERE t253b.c250_inventory_lock_id = p_inv_lock_id
                    AND t257.c256_cogs_lock_id       = p_cogs_lock_id
                    AND t257.c901_type               = 20450 -- Always Finished Goods
                    AND t253b.c504_consignment_id    = DECODE (p_byPart_flag, 'Y', t253b.c504_consignment_id,
                    mylist.MY_TEMP_TXN_KEY)
                    AND t253b.c205_part_number_id = t257.c205_part_number_id
                    AND t257.c205_part_number_id  = mylist.MY_TEMP_TXN_VALUE
               GROUP BY mylist.MY_TEMP_TXN_KEY, mylist.MY_TEMP_TXN_VALUE, t257.c257_cogs_amount
            )
            cogs, (
                 SELECT mylist.MY_TEMP_TXN_KEY, mylist.MY_TEMP_TXN_VALUE, t2605.c901_reconcile_id
                  , t901.c901_code_nm
                   FROM t2605_audit_tag_detail t2605, MY_TEMP_KEY_VALUE mylist, t901_code_lookup t901
                  WHERE t2605.c2600_physical_audit_id = p_physical_audit_id
                    AND t2605.c901_location_id        = p_location_id
                    AND t2605.c901_type               = 50676 -- adjustment
                    AND t901.c901_code_id             = t2605.c901_reconcile_id
                    AND t2605.c2605_void_fl          IS NULL
                    AND NVL (t2605.c2605_txn_id, 0)   = DECODE (p_byPart_flag, 'Y', NVL (t2605.c2605_txn_id, 0),
                    mylist.MY_TEMP_TXN_KEY)
                    AND t2605.C205_PART_NUMBER_ID = mylist.MY_TEMP_TXN_VALUE
                    AND t2605.c5040_plant_id    = v_plant_id
            )
            reconciled
          WHERE NVL (mylist.MY_TEMP_TXN_KEY, '-9999') = NVL (beforecount.MY_TEMP_TXN_KEY(+), '-9999')
            AND mylist.MY_TEMP_TXN_VALUE              = beforecount.MY_TEMP_TXN_VALUE(+)
            AND NVL (mylist.MY_TEMP_TXN_KEY, '-9999') = NVL (aftercount.MY_TEMP_TXN_KEY(+), '-9999')
            AND mylist.MY_TEMP_TXN_VALUE              = aftercount.MY_TEMP_TXN_VALUE(+)
            AND NVL (mylist.MY_TEMP_TXN_KEY, '-9999') = NVL (cogs.MY_TEMP_TXN_KEY(+), '-9999')
            AND mylist.MY_TEMP_TXN_VALUE              = cogs.MY_TEMP_TXN_VALUE(+)
            AND NVL (mylist.MY_TEMP_TXN_KEY, '-9999') = NVL (reconciled.MY_TEMP_TXN_KEY(+), '-9999')
            AND mylist.MY_TEMP_TXN_VALUE              = reconciled.MY_TEMP_TXN_VALUE(+)
    )
    group by DECODE (p_byPart_flag, 'Y', '', REFID),
    ID,
    detail,
    WAREHOUSE,
    beforecount,
    aftercount,
    devcount,
    devdollar,
    UnitCost,
    ExtendedCost,
    reconcileid,
    reconcilenm order by ID;
END gm_fch_lock_tag_by_partref;
/*******************************************************
* Purpose: Procedure is used to fetch Inventory Lock and
Tag info by Part
*******************************************************/
--
PROCEDURE gm_fch_lock_tag_by_part (
        p_physical_audit_id IN t2603_audit_location.c2600_physical_audit_id%TYPE,
        p_location_id       IN t2603_audit_location.c901_location_id%TYPE,
        p_inv_lock_id       IN t2600_physical_audit.c250_inventory_lock_id%TYPE,
        p_cogs_lock_id      IN t2600_physical_audit.c256_cogs_lock_id%TYPE,
        p_out_cur OUT TYPES.cursor_type)
AS
v_company_id  t1900_company.c1900_company_id%TYPE;
v_plant_id    t5040_plant_master.c5040_plant_id%TYPE;

BEGIN
SELECT GET_COMPID_FRM_CNTX(), GET_PLANTID_FRM_CNTX()
	    INTO v_company_id, v_plant_id
	 FROM DUAL;

    OPEN p_out_cur FOR SELECT t205.c205_part_number_id ID,
    NULL refid,
    t205.c205_part_num_desc detail,
    get_code_name (beforecount.c901_type) warehouse,
    NVL (beforecount.c251_qty, 0) beforecount,
    NVL (aftercount.c2605_qty, 0) aftercount,
    (NVL (aftercount.c2605_qty, 0)   - NVL (beforecount.c251_qty, 0)) devcount,
    ( (NVL (aftercount.c2605_qty, 0) - NVL (beforecount.c251_qty, 0)) * NVL (cogs.c257_cogs_amount, 0)) devdollar,
    (NVL (cogs.c257_cogs_amount, 0)) UnitCost,
    ( (NVL (beforecount.c251_qty, 0)) * NVL (cogs.c257_cogs_amount, 0)) ExtendedCost,
    reconciled.c901_reconcile_id reconcileid,
    reconciled.c901_code_nm reconcilenm FROM my_temp_list mylist,
    t205_part_number t205, (
         SELECT t251.c205_part_number_id, t251.c251_qty, t251.c901_type
           FROM t251_inventory_lock_detail t251, my_temp_list mylist
          WHERE t251.c250_inventory_lock_id = p_inv_lock_id
            AND t251.c901_type              = p_location_id
            AND t251.c205_part_number_id    = mylist.my_temp_txn_id
    )
    beforecount, (
         SELECT t2605.c205_part_number_id, SUM (t2605.c2605_qty) c2605_qty
           FROM t2605_audit_tag_detail t2605, my_temp_list mylist
          WHERE t2605.c2600_physical_audit_id = p_physical_audit_id
            AND t2605.c901_location_id        = p_location_id
            AND t2605.c901_type               = 50675 -- regular
            AND t2605.c205_part_number_id     = mylist.my_temp_txn_id
            AND t2605.c5040_plant_id    = v_plant_id
            AND t2605.c2605_void_fl          IS NULL
       GROUP BY t2605.c205_part_number_id
    )
    aftercount, (
         SELECT t257.c205_part_number_id, t257.c257_cogs_amount
           FROM t257_cogs_detail t257, my_temp_list mylist
          WHERE t257.c256_cogs_lock_id   = p_cogs_lock_id
            AND t257.c901_type           = 20450 -- Always Finished Goods
            AND t257.c205_part_number_id = mylist.my_temp_txn_id
    )
    cogs, (
         SELECT t2605.c205_part_number_id, t2605.c901_reconcile_id, t901.c901_code_nm
           FROM t2605_audit_tag_detail t2605, my_temp_list mylist, t901_code_lookup t901
          WHERE t2605.c2600_physical_audit_id = p_physical_audit_id
            AND t2605.c901_location_id        = p_location_id
            AND t2605.c901_type               = 50676 -- adjustment
            AND t901.c901_code_id             = t2605.c901_reconcile_id
            AND t2605.c205_part_number_id     = mylist.my_temp_txn_id
            AND t2605.c5040_plant_id    = v_plant_id
            AND t2605.c2605_void_fl          IS NULL
    )
    reconciled WHERE mylist.my_temp_txn_id                      = beforecount.c205_part_number_id(+) AND mylist.my_temp_txn_id =
    aftercount.c205_part_number_id(+) AND mylist.my_temp_txn_id = cogs.c205_part_number_id(+) AND mylist.my_temp_txn_id
                                                                = reconciled.c205_part_number_id(+) AND
    mylist.my_temp_txn_id                                       = t205.c205_part_number_id order by ID;
END gm_fch_lock_tag_by_part;
/*******************************************************
* Purpose: Procedure is used to fetch Inventory Lock and
Tag info by Txn ID
*******************************************************/
--
PROCEDURE gm_fch_lock_tag_by_txn (
        p_physical_audit_id IN t2603_audit_location.c2600_physical_audit_id%TYPE,
        p_location_id       IN t2603_audit_location.c901_location_id%TYPE,
        p_inv_lock_id       IN t2600_physical_audit.c250_inventory_lock_id%TYPE,
        p_cogs_lock_id      IN t2600_physical_audit.c256_cogs_lock_id%TYPE,
        p_out_cur OUT TYPES.cursor_type)
AS
v_company_id  t1900_company.c1900_company_id%TYPE;
v_plant_id    t5040_plant_master.c5040_plant_id%TYPE;

BEGIN
SELECT GET_COMPID_FRM_CNTX(), GET_PLANTID_FRM_CNTX()
	    INTO v_company_id, v_plant_id
	 FROM DUAL;

    OPEN p_out_cur FOR SELECT NULL ID,
    mylist.my_temp_txn_id refid,
    get_fch_lock_set_detail (mylist.my_temp_txn_id, p_inv_lock_id) detail,
    get_code_name (beforecount.c901_location_type) warehouse,
    NVL (beforecount.before_count, 0) beforecount,
    NVL (aftercount.c2605_qty, 0) aftercount,
    (NVL (aftercount.c2605_qty, 0) - NVL (beforecount.before_count, 0)) devcount,
    (NVL (aftercount.c2605_qty, 0) - NVL (beforecount.before_count, 0)) * NVL (cogs.cogs_amount, 0) devdollar,
    NVL (cogs.cogs_amount, 0) UnitCost,
    (NVL (beforecount.before_count, 0) * NVL (cogs.cogs_amount, 0)) ExtendedCost,
    reconciled.c901_reconcile_id reconcileid,
    reconciled.c901_code_nm reconcilenm FROM my_temp_list mylist, (
         SELECT mylist.my_temp_txn_id, 1 before_count, t253a.c901_location_type
           FROM t253a_consignment_lock t253a, my_temp_list mylist
          WHERE t253a.c250_inventory_lock_id = p_inv_lock_id
            AND t253a.c901_location_type     = p_location_id
            AND t253a.c5040_plant_id    = v_plant_id
            AND t253a.c504_void_fl          IS NULL
            AND t253a.c504_consignment_id    = mylist.my_temp_txn_id
    )
    beforecount, (
         SELECT mylist.my_temp_txn_id, SUM (t2605.c2605_qty) c2605_qty
           FROM t2605_audit_tag_detail t2605, my_temp_list mylist
          WHERE t2605.c2600_physical_audit_id = p_physical_audit_id
            AND t2605.c901_location_id        = p_location_id
            AND t2605.c901_type               = 50675 -- regular
            AND t2605.c2605_void_fl          IS NULL
            AND t2605.c2605_txn_id            = mylist.my_temp_txn_id
            AND t2605.c5040_plant_id    = v_plant_id
       GROUP BY mylist.my_temp_txn_id
    )
    aftercount, (
         SELECT mylist.my_temp_txn_id, SUM (t253b.c505_item_qty * NVL (t257.c257_cogs_amount, 0)) cogs_amount
           FROM t257_cogs_detail t257, t253b_item_consignment_lock t253b, my_temp_list mylist
          WHERE t253b.c250_inventory_lock_id = p_inv_lock_id
            AND t257.c256_cogs_lock_id       = p_cogs_lock_id
            AND t257.c901_type               = 20450 -- Always Finished Goods
            AND t253b.c504_consignment_id    = mylist.my_temp_txn_id
            AND t253b.c205_part_number_id    = t257.c205_part_number_id
       GROUP BY mylist.my_temp_txn_id
    )
    cogs, (
         SELECT mylist.my_temp_txn_id, t2605.c901_reconcile_id, t901.c901_code_nm
           FROM t2605_audit_tag_detail t2605, my_temp_list mylist, t901_code_lookup t901
          WHERE t2605.c2600_physical_audit_id = p_physical_audit_id
            AND t2605.c901_location_id        = p_location_id
            AND t2605.c901_type               = 50676 -- adjustment
            AND t901.c901_code_id             = t2605.c901_reconcile_id
            AND t2605.c2605_void_fl          IS NULL
            AND t2605.c2605_txn_id            = mylist.my_temp_txn_id
            AND t2605.c5040_plant_id    = v_plant_id 
    )
    reconciled WHERE mylist.my_temp_txn_id                 = beforecount.my_temp_txn_id(+) AND mylist.my_temp_txn_id =
    aftercount.my_temp_txn_id(+) AND mylist.my_temp_txn_id = cogs.my_temp_txn_id(+) AND mylist.my_temp_txn_id =
    reconciled.my_temp_txn_id(+) order by refid;
END gm_fch_lock_tag_by_txn;
/***********************************/
PROCEDURE gm_fch_tags (
        p_physical_audit_id  IN t2605_audit_tag_detail.c2600_physical_audit_id%TYPE,
        p_location_id        IN t2603_audit_location.c901_location_id%TYPE,
        p_part_num           IN t2605_audit_tag_detail.c205_part_number_id%TYPE,
        p_counted_by         IN t2605_audit_tag_detail.c101_counted_by%TYPE,
        p_missing_flag       IN VARCHAR2,
        p_tag_filter_token   IN VARCHAR2,
        p_from_tag_id        IN NUMBER,
        p_to_tag_id          IN NUMBER,
        p_show_void_rec_flag IN VARCHAR2,
        p_out_cur OUT TYPES.cursor_type)
AS
    /***************************************************
    create this procedure for the Inventory tag
    It will be the part of the gm_pkg_ac_patag_info
    ******************************************************/
    --
    v_type t2603_audit_location.c901_type%TYPE;
    v_inventory_id t2600_physical_audit.c250_inventory_lock_id%TYPE;
    v_part_num VARCHAR2 (20) ;
    v_temp     NUMBER;
    v_company_id  t1900_company.c1900_company_id%TYPE;
    v_plant_id    T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
    
BEGIN

select get_compid_frm_cntx(),get_plantid_frm_cntx() 
		into v_company_id,v_plant_id 
		from dual;
    --
    my_context.set_double_inlist_ctx (p_tag_filter_token) ;
    v_part_num := p_part_num || '%';
    -- If part # then execute below SQL
    -- If Transaction then execute below SQL
     SELECT t2600.c250_inventory_lock_id
       INTO v_inventory_id
       FROM t2600_physical_audit t2600
      WHERE t2600.c2600_physical_audit_id = p_physical_audit_id;
    OPEN p_out_cur FOR  
			SELECT t2605.c2605_audit_tag_id tagid, t2605.c2605_audit_tag_id tid, DECODE (t2603.c901_type, 50660, NVL (
			get_code_name (get_partnum_uom (t2605.c205_part_number_id)), ''), 50662, NVL ( get_code_name (get_partnum_uom (
			t2605.c205_part_number_id)), ''), '') puom, t2605.c205_part_number_id pnum, t2605.c2605_txn_id refid
		  , DECODE (t2603.c901_type, 50660, get_partnum_desc (t2605.c205_part_number_id), 50662, get_partnum_desc (
			t2605.c205_part_number_id), get_fch_lock_set_detail (t2605.c2605_txn_id, v_inventory_id)) txndesc,
			t901.c901_code_nm locationnm, t2605.c2605_qty qty, get_user_name (t2605.c101_counted_by) counted_by
		  , t2605.c101_checked_by checked_by, '' tagcount, t901.c902_code_nm_alt locationaltnm
		  , t2605.c901_location_id locationid, get_user_name (t2605.c2605_last_updated_by) lastupdatedby, t2605.c2605_last_updated_date lastupdateddt, DECODE (NVL (t2605.c901_location_id, 0), 20470,
			get_cs_fch_loaner_etchid (t2605.c2605_txn_id), 20473, get_cs_fch_loaner_etchid (t2605.c2605_txn_id), 20471,
			get_cs_fch_loaner_etchid (t2605.c2605_txn_id), 20472, get_cs_fch_loaner_etchid (t2605.c2605_txn_id), 20475,
			get_cs_fch_loaner_etchid (t2605.c2605_txn_id), 20476, get_cs_fch_loaner_etchid (t2605.c2605_txn_id), 20474,
			get_cs_fch_loaner_etchid (t2605.c2605_txn_id), '') etchid
		  , c5010_tag_id invtagid, gm_pkg_op_set_pick_rpt.get_loc_cd_from_id (c5052_location_id) invloccd
		   FROM t2605_audit_tag_detail t2605, t2603_audit_location t2603, t901_code_lookup t901
			-- , t205_part_number t205
		  WHERE ( (t2605.c2605_audit_tag_id   = DECODE (p_show_void_rec_flag, 'Y', c2605_audit_tag_id, - 9999)
			AND t2605.c2605_void_fl          IS NOT NULL)
			OR (t2605.c2605_audit_tag_id      = DECODE (p_show_void_rec_flag, 'N', c2605_audit_tag_id, - 9999)
			AND t2605.c2605_void_fl          IS NULL
			AND NVL (t2605.c2605_qty, - 9999) = DECODE ( p_missing_flag, 'Y', - 9999, NVL (t2605.c2605_qty, - 9999))))
			AND t2605.c901_location_id        = t2603.c901_location_id(+)
			AND t2605.c2600_physical_audit_id = t2603.c2600_physical_audit_id(+)
			AND t2605.c901_location_id        = t901.c901_code_id(+)
			--  AND t205.c205_part_number_id = t2605.c205_part_number_id
			AND NVL (t2605.c901_location_id, 0) = DECODE (p_location_id, 0, NVL (t2605.c901_location_id, 0), p_location_id)
			AND t2605.c2600_physical_audit_id   = p_physical_audit_id
			AND t2605.c5040_plant_id    = v_plant_id
			AND t2605.c901_type                 = 50675
			--AND NVL (t2605.c2605_qty, -9999) = DECODE (p_missing_flag, 'Y', -9999, NVL (t2605.c2605_qty, -9999))
			-- Only fetch the parts that are not received (Received will have a value)
			--  AND t205.c202_project_id = DECODE (p_project_id, '0', t205.c202_project_id, p_project_id)
			AND NVL (t2605.c101_counted_by, 0) = DECODE (p_counted_by, 0, NVL (t2605.c101_counted_by, 0), p_counted_by)
			AND ( NVL (t2605.c205_part_number_id, 'xxx') LIKE v_part_num
			OR (t2605.c2605_txn_id IN
			(
				 SELECT t253a.c504_consignment_id
				   FROM t253a_consignment_lock t253a
				  WHERE t253a.c250_inventory_lock_id = v_inventory_id
					AND t253a.c504_consignment_id    = DECODE (p_part_num, NULL, t253a.c504_consignment_id, p_part_num)
                    AND t253a.c5040_plant_id    = v_plant_id
					--  AND t253a.c207_set_id = DECODE (p_part_num, NULL, t253a.c207_set_id, p_part_num)
			)))
			AND ( (t2605.c2605_audit_tag_id = DECODE (instr (p_tag_filter_token, 'E', 1, 1), 1, t2605.c2605_audit_tag_id, -
			9999)
			AND t2605.c2605_audit_tag_id NOT IN
			(
				 SELECT tokenii FROM v_double_in_list WHERE token = 'E'
			))
			OR (t2605.c2605_audit_tag_id  = DECODE (instr (p_tag_filter_token, 'I', 1, 1), 1, t2605.c2605_audit_tag_id, - 9999)
			AND t2605.c2605_audit_tag_id                                                                               IN
			(
				 SELECT tokenii FROM v_double_in_list WHERE token = 'I'
			))
			OR (t2605.c2605_audit_tag_id  = DECODE (p_tag_filter_token, NULL, t2605.c2605_audit_tag_id, - 9999)
			AND t2605.c2605_audit_tag_id  = t2605.c2605_audit_tag_id))
			AND t2605.c2605_audit_tag_id >= DECODE ( p_from_tag_id, 0, t2605.c2605_audit_tag_id, p_from_tag_id)
			AND t2605.c2605_audit_tag_id <= DECODE (p_to_tag_id, 0, t2605.c2605_audit_tag_id, p_to_tag_id)
		ORDER BY t2605.c2605_audit_tag_id;
END gm_fch_tags;
--
/*******************************************************
* Description : Procedure to get the tag details for the
*    given tag id's
* Author    : VPrasath
*******************************************************/
PROCEDURE gm_fch_generated_tags (
        p_tag_ids IN VARCHAR2,
        p_out_cur OUT TYPES.cursor_type)
AS
v_company_id  t1900_company.c1900_company_id%TYPE;	
v_plant_id    t5040_plant_master.c5040_plant_id%TYPE;

BEGIN
SELECT GET_COMPID_FRM_CNTX(), GET_PLANTID_FRM_CNTX()
	    INTO v_company_id, v_plant_id
	 FROM DUAL;

    my_context.set_my_inlist_ctx (p_tag_ids) ;
    OPEN p_out_cur FOR SELECT t2605.c2605_audit_tag_id tagid,
    t2605.c205_part_number_id pnum,
    c2605_txn_id refid,
    NVL (get_code_name (get_partnum_uom (t2605.c205_part_number_id)), '') puom,
    DECODE (NVL (t2605.c901_location_id, 0), 20470, get_cs_fch_loaner_etchid (t2605.c2605_txn_id), 20473,
    get_cs_fch_loaner_etchid (t2605.c2605_txn_id), 20471, get_cs_fch_loaner_etchid (t2605.c2605_txn_id), 20472,
    get_cs_fch_loaner_etchid (t2605.c2605_txn_id), 20475, get_cs_fch_loaner_etchid (t2605.c2605_txn_id), 20476,
    get_cs_fch_loaner_etchid (t2605.c2605_txn_id), 20474, get_cs_fch_loaner_etchid (t2605.c2605_txn_id), '') etchid,
    get_partnum_desc (t2605.c205_part_number_id) txndesc,
    get_code_name (t2605.c901_location_id) locationnm,
    t2605.c2605_qty qty,
    t2605.c101_counted_by counted_by,
    '' tagcount,
    t2605.c101_checked_by checked_by,
    get_code_name_alt (t2605.c901_location_id) locationaltnm
	, c5010_tag_id invtagid, gm_pkg_op_set_pick_rpt.get_loc_cd_from_id (c5052_location_id) invloccd
	FROM t2605_audit_tag_detail t2605 
	WHERE t2605.c5040_plant_id    = v_plant_id 
	AND t2605.c2605_audit_tag_id IN
    (
         SELECT token FROM v_in_list
    )
    order by tagid;
END gm_fch_generated_tags;
--
/*******************************************************
* Description : Procedure to get the tag details
*    for the given tag id
* Author    : VPrasath
*******************************************************/
--
PROCEDURE gm_fch_tag_details (
        p_tag_id IN t2605_audit_tag_detail.c2605_audit_tag_id%TYPE,
        p_out_cur OUT TYPES.cursor_type)
AS
    v_count NUMBER;
    v_company_id  t1900_company.c1900_company_id%TYPE;
    v_plant_id    t5040_plant_master.c5040_plant_id%TYPE;
    
BEGIN
SELECT GET_COMPID_FRM_CNTX(), GET_PLANTID_FRM_CNTX()
	    INTO v_company_id, v_plant_id
	 FROM DUAL;

     SELECT COUNT ( *)
       INTO v_count
       FROM t2605_audit_tag_detail t2605
      WHERE c2605_audit_tag_id = p_tag_id
        AND c901_type          = 50675
        AND t2605.c5040_plant_id    = v_plant_id
        AND c2605_void_fl     IS NULL;
    IF (v_count                = 0) THEN
        raise_application_error ('-20881', '') ;
    END IF;
    OPEN p_out_cur FOR SELECT t2605.c2605_audit_tag_id tagid,
    t2605.c205_part_number_id pnum,
    t2605.c2605_txn_id transactionid
    -- , get_partnum_desc (NVL (t2605.c205_part_number_id, t2605.c2605_txn_id)) partdescription
    ,
    DECODE (t2605.c205_part_number_id, NULL, get_fch_set_name (t2605.c2605_txn_id), get_partnum_desc (
    t2605.c205_part_number_id)) partdescription,
    t2605.c101_counted_by audituserid,
    t2605.c901_location_id locationid,
    t2605.c2605_qty tagqty,
    get_code_name (get_partnum_uom (NVL (t2605.c205_part_number_id, t2605.c2605_txn_id))) uom,
    t2605.c2605_tag_history_fl historyfl,
    get_warehouse_type (c2600_physical_audit_id, c901_location_id) pitype FROM t2605_audit_tag_detail t2605 WHERE
    c2605_audit_tag_id = p_tag_id AND c901_type = 50675
    AND t2605.c5040_plant_id    = v_plant_id
     AND c2605_void_fl IS NULL;
END gm_fch_tag_details;
--
/*******************************************************
* Description : Procedure to get the part details
*    for the given part number
* Author    : VPrasath
*******************************************************/
--
PROCEDURE gm_fch_desc (
        p_pnum   IN t2605_audit_tag_detail.c205_part_number_id%TYPE,
        p_txn_id IN t2605_audit_tag_detail.c2605_txn_id%TYPE,
        p_flag   IN VARCHAR2,
        p_out_cur OUT TYPES.cursor_type)
AS
    v_count  NUMBER;
    v_set_nm VARCHAR2 (100) ;
    v_pnum   VARCHAR2 (100) ;
    v_pdesc t205_part_number.C205_PART_NUM_DESC%TYPE;
    v_flag VARCHAR2 (100) ;
BEGIN
    /*   SELECT COUNT (1)
    INTO v_count
    FROM t205_part_number t205
    WHERE c205_part_number_id = p_pnum;
    IF v_count                 <> 0 THEN
    v_flag                 := 'part';
    ELSE
    v_flag := 'transaction';
    END IF; */
    IF (p_flag = 'part') THEN
         SELECT COUNT (1)
           INTO v_count
           FROM t205_part_number t205
          WHERE c205_part_number_id = p_pnum;
        IF (v_count                 = 0) THEN
            raise_application_error ('-20884', '') ;
        END IF;
        OPEN p_out_cur FOR SELECT t205.c205_part_num_desc partdesc,
        get_code_name (t205.c901_uom) uom FROM t205_part_number t205 WHERE c205_part_number_id = p_pnum;
    END IF;
    IF (p_flag = 'transaction') THEN
        BEGIN
             SELECT t504.c207_set_id || ' - ' || get_set_name (t504.c207_set_id)
               INTO v_set_nm
               FROM t504_consignment t504
              WHERE t504.c504_consignment_id = p_txn_id;
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            BEGIN
                 SELECT get_partnum_desc (t408.c205_part_number_id)
                   INTO v_pnum
                   FROM t408_dhr t408
                  WHERE t408.c408_dhr_id = p_txn_id;
            EXCEPTION
            WHEN NO_DATA_FOUND THEN
                v_set_nm := '';
                v_pnum   := '';
            END;
        END;
        IF (v_set_nm IS NULL AND v_pnum IS NULL) THEN
            raise_application_error ('-20885', '') ;
        END IF;
        IF (v_set_nm IS NOT NULL) THEN
            OPEN p_out_cur FOR SELECT t504.c207_set_id || ' - ' || get_set_name (t504.c207_set_id) partdesc,
            'each' uom FROM t504_consignment t504 WHERE t504.c504_consignment_id = p_txn_id;
        END IF;
        IF (v_pnum IS NOT NULL) THEN
            OPEN p_out_cur FOR SELECT get_partnum_desc (t408.c205_part_number_id) partdesc,
            'each' uom FROM t408_dhr t408 WHERE t408.c408_dhr_id = p_txn_id;
        END IF;
    END IF;
END gm_fch_desc;
--
/*******************************************************
* Description : Procedure to get the locked transaction
*    details
* Author   : VPrasath
*******************************************************/
PROCEDURE gm_fch_locked_txn (
        p_physical_audit_id IN t2605_audit_tag_detail.c2600_physical_audit_id%TYPE,
        p_location_id       IN t2603_audit_location.c901_location_id%TYPE,
        p_txn_id            IN VARCHAR2,
        p_project_id        IN t205_part_number.c202_project_id%TYPE,
        p_str               IN VARCHAR2,
        p_untagtxnfl        IN VARCHAR2,
        p_out_cur OUT TYPES.cursor_type)
AS
    v_type t2603_audit_location.c901_type%TYPE;
    v_txn_id VARCHAR2 (20) ;
    v_all_part t205_part_number.c205_part_number_id%TYPE;
    v_rem_part t205_part_number.c205_part_number_id%TYPE;
    v_location_id t2603_audit_location.c901_location_id%TYPE;
    v_company_id  t1900_company.c1900_company_id%TYPE;
    v_plant_id    t5040_plant_master.c5040_plant_id%TYPE;
BEGIN
SELECT GET_COMPID_FRM_CNTX(), GET_PLANTID_FRM_CNTX()
	    INTO v_company_id, v_plant_id
	 FROM DUAL;
          
    v_location_id := p_location_id;
    BEGIN
         SELECT t2603.c901_type
           INTO v_type
           FROM t2603_audit_location t2603
          WHERE t2603.c901_location_id        = v_location_id
            AND t2603.c2600_physical_audit_id = p_physical_audit_id;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_location_id := 0;
    END;
    my_context.set_my_inlist_ctx (p_str) ;
     SELECT COUNT (token) INTO v_all_part FROM v_in_list;
    -- If part # then execute below SQL
    IF (v_type    = 50660 OR (v_location_id = 0 AND v_all_part > 0)) THEN
        IF p_str IS NOT NULL THEN
             SELECT COUNT (1)
               INTO v_rem_part
               FROM t205_part_number
              WHERE c205_part_number_id IN
                (
                     SELECT token FROM v_in_list
                ) ;
            IF (v_all_part <> v_rem_part) THEN
                raise_application_error ('-20886', '') ;
            END IF;
        END IF;
        IF p_untagtxnfl = 'Y' THEN
            OPEN p_out_cur FOR SELECT LOAD.pnum pnum,
            get_partnum_desc (LOAD.pnum) txndesc,
            '' locationnm,
            '' counted_by,
            '' tagcount,
            '' refid,
            LOAD.cnt - NVL (tag.cnt, 0) diff FROM t205_part_number t205, (
                 SELECT t251.c205_part_number_id pnum, t251.c901_type loc, SUM (DECODE (t251.c205_part_number_id, NULL,
                    0, 1)) cnt
                   FROM t251_inventory_lock_detail t251
                  WHERE t251.c901_type               = p_location_id
                    AND t251.c250_inventory_lock_id IN
                    (
                         SELECT t2600.c250_inventory_lock_id
                           FROM t2600_physical_audit t2600
                          WHERE t2600.c2600_physical_audit_id = p_physical_audit_id
                          AND t2600.C1900_COMPANY_ID = v_company_id
                          AND t2600.c5040_plant_id    = v_plant_id
                    )
                    AND t251.c251_qty > 0
               GROUP BY c205_part_number_id, c901_type
            )
            LOAD, (
                 SELECT t2605.c205_part_number_id pnum, t2605.c901_location_id loc, NVL (SUM (DECODE (
                    t2605.c205_part_number_id, NULL, 0, 1)), 0) cnt
                   FROM t2605_audit_tag_detail t2605
                  WHERE t2605.c901_location_id        = p_location_id
                    AND t2605.c901_type               = 50675
                    AND t2605.c2600_physical_audit_id = p_physical_audit_id
                    AND t2605.c5040_plant_id    = v_plant_id
               GROUP BY t2605.c205_part_number_id, t2605.c901_location_id
            )
            tag WHERE LOAD.pnum = tag.pnum(+) AND LOAD.pnum = t205.c205_part_number_id AND LOAD.cnt - NVL (tag.cnt, 0)
                                > 0;
        ELSE
            OPEN p_out_cur FOR
            SELECT DISTINCT t251.c205_part_number_id pnum, t205.c205_part_num_desc txndesc, '' locationnm
              , '' counted_by, '' tagcount, '' refid
               FROM t251_inventory_lock_detail t251, t2600_physical_audit t2600, t205_part_number t205
              WHERE t2600.c2600_physical_audit_id = p_physical_audit_id
                AND t2600.c250_inventory_lock_id  = t251.c250_inventory_lock_id
                AND t205.c205_part_number_id      = t251.c205_part_number_id
                AND t205.c202_project_id          = DECODE (p_project_id, '0', t205.c202_project_id, p_project_id)
                AND ('-999'                       = DECODE (v_all_part, 0, '-999', '-9999')
                AND t2600.C1900_COMPANY_ID = v_company_id
                AND t2600.c5040_plant_id    = v_plant_id
                OR t251.c205_part_number_id      IN
                (
                     SELECT token FROM v_in_list
                ))
                AND t251.c901_type           = DECODE (v_location_id, '0', t251.c901_type, p_location_id)
                AND t205.c205_part_number_id = NVL (p_txn_id, t251.c205_part_number_id)
           ORDER BY pnum;
        END IF;
    END IF;
    -- If transacation then execute below SQL
    IF (v_type          = 50661) THEN
        v_txn_id       := p_txn_id || '%';
        IF p_untagtxnfl = 'Y' THEN
            OPEN p_out_cur FOR SELECT conid refid,
            '' txndesc,
            '' locationnm,
            '' counted_by,
            '' pnum,
            '' tagcount FROM
            (
                 SELECT t253a.c504_consignment_id conid, SUM (DECODE (t253a.c504_consignment_id, NULL, 0, 1)) - SUM (
                    DECODE (t2605.c2605_txn_id, NULL, 0, 1)) diff, t253a.c901_location_type
                   FROM t253a_consignment_lock t253a, t2605_audit_tag_detail t2605
                  WHERE t253a.c504_consignment_id     = t2605.c2605_txn_id(+)
                    AND t253a.c5040_plant_id    = v_plant_id
                    AND t253a.c250_inventory_lock_id IN
                    (
                         SELECT t2600.c250_inventory_lock_id
                           FROM t2600_physical_audit t2600
                          WHERE t2600.c2600_physical_audit_id = p_physical_audit_id
                          AND t2600.C1900_COMPANY_ID = v_company_id
                          AND t2600.c5040_plant_id    = v_plant_id
                    )
                    AND t2605.c2600_physical_audit_id(+) = p_physical_audit_id
                    AND t253a.c901_location_type         = p_location_id
		            AND t2605.C901_LOCATION_ID(+)    =      p_location_id --Added By MIhir
                    AND t2605.c901_type(+)               = 50675
                    AND t253a.c5040_plant_id    = v_plant_id
                    AND t253a.c504_void_fl              IS NULL
                    AND t2605.c2605_void_fl             IS NULL
               GROUP BY t253a.c504_consignment_id, t253a.c901_location_type
               ORDER BY t253a.c504_consignment_id
            )
            WHERE diff > 0;
        ELSE
            OPEN p_out_cur FOR
            SELECT DISTINCT t253a.c504_consignment_id refid, '' txndesc, '' locationnm
              , '' counted_by, '' tagcount, '' pnum
               FROM t253a_consignment_lock t253a, t253b_item_consignment_lock t253b, t2600_physical_audit t2600
              , t205_part_number t205
              WHERE t2600.c2600_physical_audit_id = p_physical_audit_id
                AND t2600.c250_inventory_lock_id  = t253a.c250_inventory_lock_id
                AND t2600.c250_inventory_lock_id  = t253b.c250_inventory_lock_id
                AND t253a.c504_consignment_id     = t253b.c504_consignment_id
                AND t205.c205_part_number_id      = t253b.c205_part_number_id
                AND t205.c202_project_id          = DECODE (p_project_id, '0', t205.c202_project_id, p_project_id)
                AND t253a.c504_consignment_id LIKE v_txn_id
                AND t253a.c901_location_type = v_location_id
                AND t253a.c5040_plant_id    = v_plant_id
		AND t253b.C901_LOCATION_TYPE = v_location_id
           ORDER BY refid;
        END IF;
    END IF;
    -- If transacation and Part then execute below SQL
    IF (v_type          = 50662) THEN
        v_txn_id       := p_txn_id || '%';
        IF p_untagtxnfl = 'Y' THEN
            OPEN p_out_cur FOR SELECT conid refid,
            part pnum,
            get_partnum_desc (part) txndesc,
            '' locationnm,
            '' counted_by,
            '' tagcount FROM
            (
                 SELECT t253b.c504_consignment_id conid, t253b.C205_PART_NUMBER_ID part, SUM (DECODE (
                    t253b.C205_PART_NUMBER_ID, NULL, 0, 1)) - SUM (DECODE (t2605.C205_PART_NUMBER_ID, NULL, 0, 1)) diff
                    , t253a.c901_location_type
                   FROM t253b_item_consignment_lock t253b, t2605_audit_tag_detail t2605, t253a_consignment_lock t253a
                  WHERE t253b.c504_consignment_id     = t2605.c2605_txn_id(+)
                    AND t253b.C205_PART_NUMBER_ID     = t2605.C205_PART_NUMBER_ID(+)
                    AND t253b.c504_consignment_id     = t253a.c504_consignment_id
                    AND t253b.c250_inventory_lock_id IN
                    (
                         SELECT t2600.c250_inventory_lock_id
                           FROM t2600_physical_audit t2600
                          WHERE t2600.c2600_physical_audit_id = p_physical_audit_id
                          AND t2600.C1900_COMPANY_ID = v_company_id
                          AND t2600.c5040_plant_id    = v_plant_id
                    )
                    AND t253a.c250_inventory_lock_id IN
                    (
                         SELECT t2600.c250_inventory_lock_id
                           FROM t2600_physical_audit t2600
                          WHERE t2600.c2600_physical_audit_id = p_physical_audit_id
                          AND t2600.C1900_COMPANY_ID = v_company_id
                          AND t2600.c5040_plant_id    = v_plant_id
                    )
                    AND t2605.c2600_physical_audit_id(+) = p_physical_audit_id
                    AND t253a.c901_location_type         = p_location_id
		    AND t253b.C901_LOCATION_TYPE = p_location_id
		            AND t2605.C901_LOCATION_ID(+)    =      p_location_id --Added By MIhir
                    AND t2605.c901_type(+)               = 50675
                    AND t253a.c504_void_fl              IS NULL
                    AND t2605.c2605_void_fl             IS NULL
                    AND t2605.c5040_plant_id    = v_plant_id
               GROUP BY t253b.c504_consignment_id, t253a.c901_location_type, t253b.C205_PART_NUMBER_ID
               ORDER BY t253b.c504_consignment_id
            )
            WHERE diff > 0;
        ELSE
            OPEN p_out_cur FOR
            SELECT DISTINCT t253b.c205_part_number_id pnum, DECODE (v_all_part, 0, t253a.c504_consignment_id, '') refid
                , t205.c205_part_num_desc txndesc, '' locationnm, '' counted_by
              , '' tagcount
               FROM t253a_consignment_lock t253a, t253b_item_consignment_lock t253b, t2600_physical_audit t2600
              , t205_part_number t205
              WHERE t2600.c2600_physical_audit_id = p_physical_audit_id
                AND t2600.c250_inventory_lock_id  = t253a.c250_inventory_lock_id
                AND t2600.c250_inventory_lock_id  = t253b.c250_inventory_lock_id
                AND t2600.C1900_COMPANY_ID = v_company_id
                AND t2600.c5040_plant_id    = v_plant_id
                AND t253a.c504_consignment_id     = t253b.c504_consignment_id
                AND t205.c205_part_number_id      = t253b.c205_part_number_id
                AND t205.c202_project_id          = DECODE (p_project_id, '0', t205.c202_project_id, p_project_id)
                AND ('-999'                       = DECODE (v_all_part, 0, '-999', '-9999')
                OR t253b.c205_part_number_id     IN
                (
                     SELECT token FROM v_in_list
                ))
                AND t253a.c504_consignment_id LIKE v_txn_id
                AND t253a.c901_location_type = v_location_id
		AND t253b.C901_LOCATION_TYPE = v_location_id
           ORDER BY refid;
        END IF;
    END IF;
END gm_fch_locked_txn;

PROCEDURE gm_void_outstanding_tag (
        p_inputs       IN VARCHAR2,
        p_comments     IN t907_cancel_log.c907_comments%TYPE,
        p_cancelreason IN t907_cancel_log.c901_cancel_cd%TYPE,
        p_canceltype   IN t901_code_lookup.c902_code_nm_alt%TYPE,
        p_userid       IN t301_vendor.c301_last_updated_by%TYPE,
        p_out_msg OUT VARCHAR2)
AS
    v_strlen    NUMBER := NVL (LENGTH (p_inputs), 0) ;
    v_substring VARCHAR2 (4000) ;
    v_string    VARCHAR2 (10) ;
    v_flag      VARCHAR2 (10) ;
    v_physical_audit t2605_audit_tag_detail.c2600_physical_audit_id%TYPE;
    v_locationid t2603_audit_location.c901_location_id%TYPE;
    v_counted_by t2605_audit_tag_detail.c101_counted_by%TYPE;
    v_tag_range_from t2605_audit_tag_detail.c2605_audit_tag_id%TYPE;
    v_tag_range_to t2605_audit_tag_detail.c2605_audit_tag_id%TYPE;
    v_tagids VARCHAR2 (4000) ;
    v_refid VARCHAR2 (20) ;
    v_company_id  t1900_company.c1900_company_id%TYPE;
    v_plant_id    t5040_plant_master.c5040_plant_id%TYPE;
    
    CURSOR v_tag_id
    IS
         SELECT c2605_audit_tag_id tag_id
           FROM t2605_audit_tag_detail t2605
          WHERE c2600_physical_audit_id       = DECODE (v_physical_audit, NULL, c2600_physical_audit_id, v_physical_audit)
	    AND NVL(c901_location_id,999)      = DECODE (v_locationid, 0, NVL(c901_location_id,999), v_locationid)
            --AND c901_location_id              = DECODE (v_locationid, 0, c901_location_id, v_locationid)
            AND NVL (c101_counted_by, - 9999) = DECODE (v_counted_by, 0, NVL (c101_counted_by, - 9999), v_counted_by)
            AND c2605_void_fl                IS NULL
            AND c901_type                     = 50675
            AND c2605_audit_tag_id           >= DECODE (v_tag_range_from, NULL, c2605_audit_tag_id, v_tag_range_from)
            AND c2605_audit_tag_id           <= DECODE (v_tag_range_to, NULL, c2605_audit_tag_id, v_tag_range_to)
            AND t2605.c5040_plant_id    = v_plant_id
            AND c2605_qty                    IS NULL
            AND ( (c2605_audit_tag_id         = DECODE (v_flag, 'UNCHECKED', c2605_audit_tag_id, - 9999)
            AND t2605.c2605_audit_tag_id NOT                                                    IN
            (
                 SELECT token FROM v_in_list
            ))
            OR (c2605_audit_tag_id        = DECODE (v_flag, 'CHECKED', c2605_audit_tag_id, - 9999)
            AND t2605.c2605_audit_tag_id                                                  IN
            (
                 SELECT token FROM v_in_list
            ))
            OR (c2605_audit_tag_id = DECODE (v_flag, 'ALL', c2605_audit_tag_id, - 9999)
            AND c2605_audit_tag_id = c2605_audit_tag_id)) 
	    AND( NVL(C2605_txn_id,'-999') =DECODE (v_refid, NULL, NVL(C2605_txn_id,'-999'), v_refid) or NVL(c205_part_number_id,'-999')=DECODE (v_refid, NULL, NVL(c205_part_number_id,'-999'),v_refid));
            --AND( C2605_txn_id =DECODE (v_refid, NULL, C2605_txn_id, v_refid) or c205_part_number_id=DECODE (v_refid, NULL, c205_part_number_id,v_refid));
    BEGIN
	    SELECT GET_COMPID_FRM_CNTX(), GET_PLANTID_FRM_CNTX()
	    INTO v_company_id, v_plant_id
	 FROM DUAL;
	     
        IF v_strlen <= 0 THEN
            raise_application_error ('-20887', '') ; --'No TagId(s) to void.
        END IF;
        v_tagids         := SUBSTR (p_inputs, INSTR (p_inputs, '|', 1, 1)    + 1) ; --list of tagids with comma separated
        v_substring      := SUBSTR (p_inputs, 1, INSTR (p_inputs, '|')       - 1) ;
        v_flag           := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
        v_substring      := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        v_physical_audit := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
        v_substring      := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        v_locationid     := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
        v_substring      := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        v_counted_by     := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
        v_substring      := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        v_tag_range_from := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
        v_substring      := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        v_tag_range_to   := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
        v_substring      := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        v_refid   		 := v_substring;
        IF v_tagids      IS NOT NULL THEN
            my_context.set_my_inlist_ctx (v_tagids) ;
        END IF;
         
        FOR v_tag IN v_tag_id
        LOOP
            gm_pkg_common_cancel.gm_sav_cancel_log ('', v_tag.tag_id, p_comments, p_cancelreason, '', p_userid) ;
            UPDATE t2605_audit_tag_detail t2605
            SET c2605_void_fl          = 'Y', c2605_last_updated_by = p_userid, c2605_last_updated_date = CURRENT_DATE
              WHERE c2605_audit_tag_id = v_tag.tag_id;
        END LOOP;
        p_out_msg := 'All Tags have been voided successfully.';
         
    END gm_void_outstanding_tag;
    --
    FUNCTION get_fch_lock_set_detail (
            p_txn_id       IN t2605_audit_tag_detail.c2605_txn_id%TYPE,
            p_inventory_id IN t2600_physical_audit.c250_inventory_lock_id%TYPE)
        RETURN VARCHAR2
    IS
        v_set_nm VARCHAR2 (100) ;
    BEGIN
        BEGIN
             SELECT DECODE (t253a.c207_set_id, NULL, t253a.c205_part_number_id || ' - ' || t253a.c408_control_number ||
                ' - ' || get_partnum_desc (t253a.c205_part_number_id), t253a.c207_set_id || ' - ' || get_set_name (
                t253a.c207_set_id))
               INTO v_set_nm
               FROM t253a_consignment_lock t253a
              WHERE t253a.c250_inventory_lock_id = p_inventory_id
                AND t253a.c504_consignment_id    = p_txn_id;
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_set_nm := '';
        END;
        RETURN v_set_nm;
    END get_fch_lock_set_detail;
    --
FUNCTION get_fch_set_name (
        p_txn_id IN t2605_audit_tag_detail.c2605_txn_id%TYPE)
    RETURN VARCHAR2
IS
    v_set_nm VARCHAR2 (100) ;
BEGIN
    BEGIN
         SELECT t504.c207_set_id || ' - ' || get_set_name (t504.c207_set_id)
           INTO v_set_nm
           FROM t504_consignment t504
          WHERE t504.c504_consignment_id = p_txn_id;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        BEGIN
             SELECT get_partnum_desc (t408.c205_part_number_id)
               INTO v_set_nm
               FROM t408_dhr t408
              WHERE t408.c408_dhr_id = p_txn_id;
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_set_nm := '';
        END;
    END;
    RETURN v_set_nm;
END get_fch_set_name;
/*******************************************************
* Description : Procedure to get validate excel data
*    details
* Author  : Kulanthaivelu
*******************************************************/
FUNCTION get_validate_excel_data (
        p_physical_audit_id IN t2605_audit_tag_detail.c2600_physical_audit_id%TYPE,
        p_pnum              IN t205_part_number.c205_part_number_id%TYPE,
        p_loc               IN t2603_audit_location.c901_location_id%TYPE,
        p_consignee         IN t2601_audit_user.c101_user_id%TYPE,
        p_inv_location      IN t253a_consignment_lock.c504_consignment_id%TYPE)
    RETURN VARCHAR2
IS
    v_pdesc t205_part_number.c205_part_num_desc%TYPE;
    v_location_id t2603_audit_location.c901_location_id%TYPE;
    v_type t2603_audit_location.c901_type%TYPE;
    v_cnt    NUMBER;
    v_invloc VARCHAR2 (100) ;
    v_company_id  t1900_company.c1900_company_id%TYPE;
    v_plant_id    t5040_plant_master.c5040_plant_id%TYPE;
BEGIN
         SELECT GET_COMPID_FRM_CNTX(), GET_PLANTID_FRM_CNTX()
	    INTO v_company_id, v_plant_id
	 FROM DUAL;

    v_location_id := p_loc;
    v_invloc      := p_inv_location ;
    BEGIN
       
         SELECT t2603.c901_type
           INTO v_type
           FROM t2603_audit_location t2603
          WHERE t2603.c901_location_id        = v_location_id
            AND t2603.c2600_physical_audit_id = p_physical_audit_id
            AND c2603_void_fl                IS NULL;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_location_id := 0;
        v_pdesc       := '';
    END;
    IF (v_type = 50660 OR v_type = 50662) THEN
        BEGIN
            SELECT DISTINCT t205.c205_part_num_desc
               INTO v_pdesc
               FROM t251_inventory_lock_detail t251, t2600_physical_audit t2600, t205_part_number t205
              WHERE t2600.c2600_physical_audit_id = p_physical_audit_id
                AND t2600.c250_inventory_lock_id  = t251.c250_inventory_lock_id
                AND t205.c205_part_number_id      = t251.c205_part_number_id
                AND t251.c901_type                = p_loc
                AND t205.c205_part_number_id      = p_pnum
                AND t2600.C1900_COMPANY_ID = v_company_id
                AND t2600.c5040_plant_id    = v_plant_id
                AND t2600.c2600_void_fl          IS NULL;
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_pdesc  := '';
            v_invloc := '';
        END;
        IF (v_type = 50662) THEN
             SELECT COUNT (1)
               INTO v_cnt
               FROM t253a_consignment_lock t253a, t253b_item_consignment_lock t253b, t2600_physical_audit t2600
              WHERE t2600.c2600_physical_audit_id = p_physical_audit_id
                AND t2600.c250_inventory_lock_id  = t253a.c250_inventory_lock_id
                AND t2600.c250_inventory_lock_id  = t253b.c250_inventory_lock_id
                AND t253a.c504_consignment_id     = t253b.c504_consignment_id
                AND t253b.c205_part_number_id     = p_pnum
                AND t253a.c504_consignment_id     = v_invloc
                AND t253a.c901_location_type      = p_loc
                AND t253a.c5040_plant_id    = v_plant_id
		AND t253b.C901_LOCATION_TYPE = p_loc;
            IF (v_cnt                             = 0) THEN
                v_pdesc                          := '';
            END IF;
        END IF;
    ELSE
        v_pdesc := '';
    END IF;
     SELECT COUNT (1)
       INTO v_cnt
       FROM t2601_audit_user
      WHERE c2600_physical_audit_id = p_physical_audit_id
        AND c101_user_id            = p_consignee
        AND c2601_void_fl          IS NULL;
    IF (v_cnt                       = 0) THEN
        v_pdesc                    := '';
    END IF;
    RETURN v_pdesc;
END get_validate_excel_data;
END gm_pkg_ac_patag_info;
/
