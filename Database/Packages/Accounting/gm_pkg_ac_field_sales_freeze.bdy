/* Formatted on 2009/11/30 19:01 (Formatter Plus v4.8.0) */

--@"c:\database\packages\accounting\gm_pkg_ac_field_sales_freeze.bdy"

CREATE OR REPLACE PACKAGE BODY gm_pkg_ac_field_sales
IS
	/*************************************************
	 * Description : Procedure to fetch excel data
	 * Author	: Mihir
	 *************************************************/
	PROCEDURE gm_fch_excel_data (
		p_dist_id				  IN	   t2651_field_sales_details.c701_distributor_id%TYPE
	  , p_out_hospital_info 	  OUT	   TYPES.cursor_type
	  , p_out_rep_info			  OUT	   TYPES.cursor_type
	  , p_out_distributor_info	  OUT	   TYPES.cursor_type
	  , p_out_master_part_set	  OUT	   TYPES.cursor_type
	  , p_out_master_control_no   OUT	   TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_out_hospital_info
		 FOR
			 SELECT DISTINCT ac_name, ac_id
						FROM v700_territory_mapping_detail
					   WHERE d_id = p_dist_id AND ac_id IS NOT NULL
					ORDER BY ac_name;

		OPEN p_out_rep_info
		 FOR
			 SELECT DISTINCT	v700.rep_name
							 || ' - '
							 || t106.c106_add1
							 || '  '
							 || t106.c106_add2
							 || ' , '
							 || t106.c106_city
							 || ' , '
							 || get_code_name (t106.c901_state)
							 || ' - '
							 || t106.c106_zip_code rep_name
						   , t106.c106_address_id
						FROM v700_territory_mapping_detail v700, t703_sales_rep t703, t106_address t106
					   WHERE d_id = p_dist_id
						 AND ac_id IS NOT NULL
						 AND t703.c703_sales_rep_id = v700.rep_id
						 AND t106.c101_party_id = t703.c101_party_id
					ORDER BY rep_name;

		OPEN p_out_distributor_info
		 FOR
			 SELECT DISTINCT v700.d_name, v700.d_id
						FROM v700_territory_mapping_detail v700
					   WHERE d_id = p_dist_id;

		OPEN p_out_master_part_set
		 FOR
			 SELECT   t207.c205_part_number_id p_num, get_partnum_desc (t207.c205_part_number_id) pdesc
					, t207.c207_set_id set_id, t207.c207_set_nm set_desc
					, get_set_name (t207.c207_set_sales_system_id) SYSTEM
					, get_code_name_alt (t207.c901_hierarchy) HIERARCHY, NVL (SUM (t2657.c2657_qty), 0) qty
				 FROM (SELECT *
						 FROM t2657_field_sales_details_lock t2657, t2652_field_sales_lock t2652
						WHERE t2657.c2652_field_sales_lock_id = t2652.c2652_field_sales_lock_id
						  AND t2652.c701_distributor_id = p_dist_id) t2657
					, (SELECT t207.c207_set_id, t208.c205_part_number_id, t207.c207_set_nm
							, t207.c207_set_sales_system_id, t207.c901_hierarchy
						 FROM t207_set_master t207, t208_set_details t208
						WHERE t207.c207_type = 4070
						  AND t208.c208_critical_fl = 'Y'
						  AND t207.c207_set_id = t208.c207_set_id
						  AND t207.c207_void_fl IS NULL
						  AND t207.c207_obsolete_fl IS NULL) t207
				WHERE t207.c207_set_id = t2657.c207_set_id(+)
				  AND t207.c205_part_number_id = t2657.c205_part_number_id(+)
				  AND t207.c207_set_id NOT IN
										 ('900.003', '835.902', '835.901', '940.901', '938.901', '938.900', '838.010S')
			 GROUP BY t207.c205_part_number_id
					, t207.c207_set_id
					, t207.c207_set_nm
					, t207.c207_set_sales_system_id
					, t207.c901_hierarchy
			 ORDER BY NVL (TRIM (SYSTEM), 'XXXX'), set_id, HIERARCHY;

		OPEN p_out_master_control_no
		 FOR
			 SELECT get_distributor_name (t2652.c701_distributor_id) dname, t2659.c205_part_number_id
				  , get_partnum_desc (t2659.c205_part_number_id) partdesc, t2659.c2659_control_number
				  , t2659.c207_set_id, get_set_name (t2659.c207_set_id) setname, t2659.c2659_ref_id
				  , t2659.c2659_ref_date, t2659.c2659_qty
			   FROM t2659_field_sales_ref_lock t2659, t2652_field_sales_lock t2652
			  WHERE t2659.c2652_field_sales_lock_id = t2652.c2652_field_sales_lock_id
				AND t2652.c701_distributor_id = p_dist_id;
	END gm_fch_excel_data;

	/****************************************************
	 * Description : Procedure to fetch tag ids
	 * Author	: Mihir
	 *************************************************/
	PROCEDURE gm_fch_auditor_tags (
		p_dist_id		IN		 t2651_field_sales_details.c701_distributor_id%TYPE
	  , p_audit_id		IN		 t2651_field_sales_details.c2650_field_sales_audit_id%TYPE
	  , p_auditor_id	IN		 t2656_audit_entry.c2656_counted_by%TYPE
	  , p_out_tag_ids	OUT 	 TYPES.cursor_type
	)
	AS
		v_field_sales_id t2651_field_sales_details.c2651_field_sales_details_id%TYPE;
	BEGIN
		BEGIN
			SELECT c2651_field_sales_details_id
			  INTO v_field_sales_id
			  FROM t2651_field_sales_details t2651
			 WHERE t2651.c701_distributor_id = p_dist_id AND t2651.c2650_field_sales_audit_id = p_audit_id;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				raise_application_error (-20889, '');
		END;

		OPEN p_out_tag_ids
		 FOR
			 SELECT   ID, pnum, pdesc, cnum, set_id, setdesc
				 FROM (SELECT t2656.c5010_tag_id ID, t2656.c205_part_number_id pnum
							, get_partnum_desc (t2656.c205_part_number_id) pdesc, t2656.c2656_control_number cnum
							, t2656.c207_set_id set_id, get_set_name (t2656.c207_set_id) setdesc
						 FROM t2656_audit_entry t2656
						WHERE t2656.c2651_field_sales_details_id = v_field_sales_id
						  AND t2656.c2656_counted_by = p_auditor_id
					   UNION
					   SELECT t2656.c5010_tag_id ID, t2656.c205_part_number_id pnum
							, get_partnum_desc (t2656.c205_part_number_id) pdesc, t2656.c2656_control_number cnum
							, t2656.c207_set_id set_id, get_set_name (t2656.c207_set_id) set_desc
						 FROM t2656_audit_entry t2656
						WHERE t2656.c2651_field_sales_details_id = v_field_sales_id AND t2656.c2656_counted_by IS NULL)
			 ORDER BY ID;
	END gm_fch_auditor_tags;

	/****************************************************
	 * Description : Procedure to lock (MAIN)
	 * Author	: Mihir
	 *************************************************/
	PROCEDURE gm_ac_ld_lock (
		p_dist_id	 IN   t2651_field_sales_details.c701_distributor_id%TYPE
	  , p_audit_id	 IN   t2651_field_sales_details.c2650_field_sales_audit_id%TYPE
	  , p_user_id	 IN   t101_user.c101_user_id%TYPE
	  , p_inputstr	 IN   VARCHAR2
	)
	AS
		v_field_sales_id t2651_field_sales_details.c2651_field_sales_details_id%TYPE;
		v_string	   VARCHAR2 (4000) := p_inputstr;
		v_count 	   NUMBER;
	BEGIN
		SELECT COUNT (c2651_field_sales_details_id)
		  INTO v_count
		  FROM t2651_field_sales_details t2651
		 WHERE t2651.c701_distributor_id = p_dist_id AND t2651.c2650_field_sales_audit_id = p_audit_id;

		IF (v_count = 0)
		THEN
			gm_pkg_ac_ld_field_sales.gm_ac_ld_main (p_dist_id, p_audit_id, p_user_id);
		END IF;

		BEGIN
			SELECT c2651_field_sales_details_id
			  INTO v_field_sales_id
			  FROM t2651_field_sales_details t2651
			 WHERE t2651.c701_distributor_id = p_dist_id AND t2651.c2650_field_sales_audit_id = p_audit_id;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				raise_application_error (-20890, '');
		END;

		gm_pkg_ac_field_sales.gm_ac_ld_tag_team (v_field_sales_id, p_audit_id, p_user_id, v_string);
		gm_pkg_ac_ld_audit.gm_ac_ld_main (p_dist_id, p_audit_id, p_user_id);
	END gm_ac_ld_lock;

	/****************************************************
	 * Description : Procedure to insert Auditors and corresponding tag ranges
	 * Author	: Mihir
	 *************************************************/
	PROCEDURE gm_ac_ld_tag_team (
		p_field_sales_id   IN	t2651_field_sales_details.c2651_field_sales_details_id%TYPE
	  , p_audit_id		   IN	t2651_field_sales_details.c2650_field_sales_audit_id%TYPE
	  , p_user_id		   IN	t101_user.c101_user_id%TYPE
	  , p_inputstr		   IN	VARCHAR2
	)
	AS
		v_strlen	   NUMBER := NVL (LENGTH (p_inputstr), 0);
		v_string	   VARCHAR2 (4000) := p_inputstr;
		v_substring    VARCHAR2 (4000);
		v_auditor_id   t2658_audit_team.c2658_audit_user_id%TYPE;
		v_auditor_name VARCHAR2 (100);
		v_temp_auditor_name VARCHAR2 (100);
		v_start_tag_range t2658_audit_team.c2658_tag_start_range%TYPE;
		v_end_tag_range t2658_audit_team.c2658_tag_end_range%TYPE;
		v_dist_name    VARCHAR2 (100);
		v_audit_team_id t2658_audit_team.c2658_audit_team_id%TYPE;
		v_file_name    VARCHAR2 (400);
	BEGIN
		SELECT REPLACE (get_distributor_name (t2651.c701_distributor_id), ' ', NULL)
		  INTO v_dist_name
		  FROM t2651_field_sales_details t2651
		 WHERE t2651.c2651_field_sales_details_id = p_field_sales_id;

		IF v_strlen > 0
		THEN
			WHILE INSTR (v_string, '|') <> 0
			LOOP
				v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
				v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
				v_auditor_id := NULL;
				v_auditor_name := NULL;
				v_start_tag_range := NULL;
				v_end_tag_range := NULL;
				v_auditor_id := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_auditor_name := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_start_tag_range := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_end_tag_range := v_substring;

				SELECT s2658_audit_team.NEXTVAL
				  INTO v_audit_team_id
				  FROM DUAL;

				INSERT INTO t2658_audit_team
							(c2658_audit_team_id, c2651_field_sales_details_id, c2658_audit_user_id
						   , c2658_tag_start_range, c2658_tag_end_range, c2658_created_by, c2658_created_date
							)
					 VALUES (v_audit_team_id, p_field_sales_id, v_auditor_id
						   , v_start_tag_range, v_end_tag_range, p_user_id, SYSDATE
							);

				SELECT REPLACE (v_auditor_name, ' ', NULL)
				  INTO v_temp_auditor_name
				  FROM DUAL;

				v_file_name := 'Master_Audit_Data_' || v_dist_name || '_' || v_temp_auditor_name || '.xls';
				gm_pkg_upload_info.gm_sav_upload_info (v_audit_team_id, v_file_name, p_user_id, '90903');
			END LOOP;
		END IF;
	END gm_ac_ld_tag_team;

	/*************************************************
	 * Description	:This procedure is called for fetching Audit Details
	 * Author		: Pramaraj
	 *************************************************/
	PROCEDURE gm_fetch_audit_details (
		p_auditcur	 OUT   TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_auditcur
		 FOR
			 SELECT t2650.c2650_field_sales_audit_id ID, t2650.c2650_audit_name NAME
			   FROM t2650_field_sales_audit t2650
			  WHERE t2650.c2650_void_fl IS NULL;
	END gm_fetch_audit_details;

	/*************************************************
	 * Description	:This procedure is called for fetching unloked distributor Details
	 * Author		: Pramaraj
	 *************************************************/
	PROCEDURE gm_fetch_distributor (
		p_audit_id	 IN 	  t2651_field_sales_details.c2650_field_sales_audit_id%TYPE
	  , p_distcur	 OUT	  TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_distcur
		 FOR
			 SELECT   t701.c701_distributor_id distid, t701.c701_distributor_name distname
				 FROM t701_distributor t701
				WHERE t701.c701_void_fl IS NULL
				  AND t701.c701_active_fl = 'Y'
				  AND t701.c901_distributor_type IN (70100, 70101)
			 ORDER BY t701.c701_distributor_name DESC;
	END gm_fetch_distributor;

	/*************************************************
	* Description	: This procedure is called for fetching Auditor Details
	* Author		: Pramaraj
	*************************************************/
	PROCEDURE gm_fetch_auditor (
		p_audit_id	   IN		t2651_field_sales_details.c2650_field_sales_audit_id%TYPE
	  , p_auditorcur   OUT		TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_auditorcur
		 FOR
			 SELECT t2661.c101_user_id userid, get_user_name (t2661.c101_user_id) uname
			   FROM t2661_audit_users t2661
			  WHERE t2661.c2650_field_sales_audit_id = p_audit_id;
	END gm_fetch_auditor;

	/*************************************************
	 * Description	 :This procedure is called for fetching unloked distributor Details
	 * Author		  : RVaratharajan
	 *************************************************/
	PROCEDURE gm_fetch_locked_distributor (
		p_audit_id	 IN 	  t2651_field_sales_details.c2650_field_sales_audit_id%TYPE
	  , p_distcur	 OUT	  TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_distcur
		 FOR
			 SELECT   t701.c701_distributor_id distid, t701.c701_distributor_name distname
				 FROM t2651_field_sales_details t2651, t701_distributor t701
				WHERE t701.c701_distributor_id = t2651.c701_distributor_id
				  AND t2651.c2651_void_fl IS NULL
				  AND t2651.c2650_field_sales_audit_id = p_audit_id
			 ORDER BY t701.c701_distributor_name;
	END gm_fetch_locked_distributor;

	/*************************************************
	* Description	: This procedure is called for fetching uploaded Info
	* Author		: Pramaraj
	*************************************************/
	PROCEDURE gm_fetch_audit_upload_info (
		p_audit_id			 IN 	  t2651_field_sales_details.c2650_field_sales_audit_id%TYPE
	  , p_dist_id			 IN 	  t2651_field_sales_details.c701_distributor_id%TYPE
	  , p_auditor_id		 IN 	  t2658_audit_team.c2658_audit_user_id%TYPE
	  , p_ref_type			 IN 	  t903_upload_file_list.c903_ref_id%TYPE
	  , p_out_audit_upload	 OUT	  TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_out_audit_upload
		 FOR
			 SELECT   get_user_name (c2658_audit_user_id) auditor, t903.c903_upload_file_list file_id
					, t903.c903_file_name file_name, get_user_name (t903.c903_created_by) uploaded_by
					, TO_CHAR (t903.c903_created_date, 'MM/DD/YYYY') created_on
				 FROM t2658_audit_team t2658, t2651_field_sales_details t2651, t903_upload_file_list t903
				WHERE t2658.c2651_field_sales_details_id = t2651.c2651_field_sales_details_id
				  AND t903.c901_ref_type = p_ref_type
				  AND t2658.c2658_audit_user_id = DECODE (p_auditor_id, '0', t2658.c2658_audit_user_id, p_auditor_id)
				  AND t903.c903_ref_id = t2658.c2658_audit_team_id
				  AND t2651.c701_distributor_id = p_dist_id
				  AND t2651.c2650_field_sales_audit_id = p_audit_id
				  AND t903.c903_delete_fl IS NULL
				  AND t2658.c2658_void_fl IS NULL
				  AND t2651.c2651_void_fl IS NULL
			 ORDER BY c2658_audit_user_id DESC;
	END gm_fetch_audit_upload_info;

	/*************************************************
	* Description	: This procedure is called for fetching Auditor ID
	* Author		: Pramaraj
	*************************************************/
	PROCEDURE gm_fetch_audit_team_id (
		p_audit_id		 IN 	  t2651_field_sales_details.c2650_field_sales_audit_id%TYPE
	  , p_dist_id		 IN 	  t2651_field_sales_details.c701_distributor_id%TYPE
	  , p_auditor_id	 IN 	  t2658_audit_team.c2658_audit_user_id%TYPE
	  , p_out_audit_id	 OUT	  VARCHAR2
	)
	AS
	BEGIN
		SELECT t2658.c2658_audit_team_id
		  INTO p_out_audit_id
		  FROM t2651_field_sales_details t2651, t2658_audit_team t2658
		 WHERE t2651.c2650_field_sales_audit_id = p_audit_id
		   AND t2651.c701_distributor_id = p_dist_id
		   AND t2658.c2651_field_sales_details_id = t2651.c2651_field_sales_details_id
		   AND t2658.c2658_audit_user_id = p_auditor_id
		   AND t2658.c2658_void_fl IS NULL
		   AND t2651.c2651_void_fl IS NULL;
	END gm_fetch_audit_team_id;

	/*************************************************
	* Description	: This procedure is called for Upload Process
	* Author		: Pramaraj
	*************************************************/
	PROCEDURE gm_ac_ld_upload_data (
		p_dist_id		  IN	   t701_distributor.c701_distributor_id%TYPE
	  , p_audit_id		  IN	   t2650_field_sales_audit.c2650_field_sales_audit_id%TYPE
	  , p_ref_type		  IN	   t903_upload_file_list.c901_ref_type%TYPE
	  , p_file_name 	  IN	   t903_upload_file_list.c903_file_name%TYPE
	  , p_user_id		  IN	   t2656_audit_entry.c2656_created_by%TYPE
	  , p_auditor_id	  IN	   t2658_audit_team.c2658_audit_user_id%TYPE
	  , p_out_err_tag	  OUT	   VARCHAR2
	  , p_out_error_tag   OUT	   VARCHAR2
	  , p_out_file_id	  OUT	   VARCHAR2
	)
	AS
		v_tag_err	   VARCHAR2 (4000);
		v_tag_status   t5010_tag.c901_status%TYPE;
		v_field_sales_details_id t2651_field_sales_details.c2651_field_sales_details_id%TYPE;
		v_counted_by   t2656_audit_entry.c2656_counted_by%TYPE;
		--v_out_audit_id t2658_audit_team.c2658_audit_team_id%TYPE;
		v_file_id	   t903_upload_file_list.c903_upload_file_list%TYPE;
		v_ref_type	   t2656_audit_entry.c901_ref_type%TYPE;
		v_active_tag_count NUMBER;
		v_erroneous_data NUMBER;
		v_data_exists  NUMBER;
		v_tag_cnt	   NUMBER;
		v_temp_cnt	   NUMBER;
		v_out_audit_id NUMBER;

		CURSOR v_temp_tag_cur
		IS
			SELECT temp_tag_id tag_id, temp_part_id part_id, temp_part_desc part_desc
				 , temp_part_control_id part_control_id, temp_set_id set_id, temp_set_desc set_dec
				 , temp_comments comments, temp_status_id status_id, temp_sharedwith_id sharedwith_id
				 , temp_location_id location_id, temp_location_detl_id location_detl_id, temp_counted_by counted_by
				 , t_date t_date
			  FROM temp_audit_status_tag temp
			 WHERE temp.temp_dist_id = p_dist_id AND temp.temp_counted_by = p_auditor_id;
	BEGIN
		v_tag_err	:= NULL;

		SELECT COUNT (t2658.c2658_audit_team_id)
		  INTO v_out_audit_id
		  FROM t2651_field_sales_details t2651, t2658_audit_team t2658
		 WHERE t2651.c2650_field_sales_audit_id = p_audit_id
		   AND t2651.c701_distributor_id = p_dist_id
		   AND t2658.c2651_field_sales_details_id = t2651.c2651_field_sales_details_id
		   AND t2658.c2658_audit_user_id = p_auditor_id
		   AND t2658.c2658_void_fl IS NULL
		   AND t2651.c2651_void_fl IS NULL;

		IF v_out_audit_id < 1
		THEN
			raise_application_error ('-20891', '');
		END IF;

		SELECT COUNT (1)
		  INTO v_temp_cnt
		  FROM temp_audit_status_tag temp
		 WHERE temp.temp_dist_id = p_dist_id AND temp.temp_counted_by = p_auditor_id;

		IF v_temp_cnt < 1
		THEN
			raise_application_error ('-20892', '');
		END IF;

		gm_pkg_upload_info.gm_sav_upload_info (v_out_audit_id, p_file_name, p_user_id, p_ref_type);

		SELECT MAX (t903.c903_upload_file_list)
		  INTO p_out_file_id
		  FROM t903_upload_file_list t903
		 WHERE t903.c903_ref_id = v_out_audit_id AND c901_ref_type = p_ref_type AND c903_delete_fl IS NULL;

		SELECT COUNT (1)
		  INTO v_erroneous_data
		  FROM temp_audit_status_tag temp
		 WHERE (   temp.temp_tag_id IS NULL
				OR temp.temp_part_id IS NULL
				--	OR temp.temp_part_control_id IS NULL
				OR temp.temp_set_id IS NULL
				OR temp.temp_location_id IS NULL
				OR temp.temp_location_detl_id IS NULL
				OR temp.temp_dist_id IS NULL
				OR temp.temp_counted_by IS NULL
			   );

		IF v_erroneous_data > 0
		THEN
			DBMS_OUTPUT.put_line ('Check The Excel File, some of the required data is empty');
			raise_application_error ('-20893', '');
		END IF;

		BEGIN
			SELECT t2651.c2651_field_sales_details_id
			  INTO v_field_sales_details_id
			  FROM t2651_field_sales_details t2651
			 WHERE t2651.c701_distributor_id = p_dist_id
			   AND t2651.c2650_field_sales_audit_id = p_audit_id
			   AND t2651.c2651_void_fl IS NULL;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				DBMS_OUTPUT.put_line ('This distributor is not locked yet');
				raise_application_error ('-20894', '');
		END;

		FOR temp_tag IN v_temp_tag_cur
		LOOP
			BEGIN
				SELECT t5010.c901_status
				  INTO v_tag_status
				  FROM t5010_tag t5010
				 WHERE t5010.c5010_tag_id = temp_tag.tag_id AND t5010.c901_status = '51011';

				SELECT COUNT (1)
				  INTO v_tag_cnt
				  FROM t2656_audit_entry t2656
				 WHERE t2656.c2651_field_sales_details_id = v_field_sales_details_id
				   AND t2656.c5010_tag_id = temp_tag.tag_id
				   AND t2656.c2656_void_fl IS NULL;

				UPDATE t5010_tag t5010
				   SET c205_part_number_id = temp_tag.part_id
					 , c5010_control_number = temp_tag.part_control_id
					 , c207_set_id = temp_tag.set_id
					 , c901_location_type = temp_tag.location_id
					 , c5010_location_id = p_dist_id
					 , c901_status = '51012'
					 , c5010_lock_fl = 'Y'
					 , c5010_last_updated_by = p_user_id
					 , c5010_last_updated_date = SYSDATE
					 , c901_trans_type = 51000
				 WHERE t5010.c5010_tag_id = temp_tag.tag_id AND t5010.c5010_void_fl IS NULL;

				IF (v_tag_cnt < 1)
				THEN
					p_out_error_tag := p_out_error_tag || ',' || temp_tag.tag_id;
				END IF;
			EXCEPTION
				WHEN NO_DATA_FOUND
				THEN
					p_out_err_tag := p_out_err_tag || ',' || temp_tag.tag_id;
					gm_pkg_ac_tag_info.gm_sav_tag_log (temp_tag.tag_id
													 , temp_tag.part_control_id
													 , temp_tag.part_id
													 , temp_tag.set_id
													 , NULL
													 , NULL
													 , get_location_detail_id (temp_tag.location_detl_id
																			 , temp_tag.location_id
																			  )
													 , temp_tag.location_id
													 , '51012'
													 , 'Y'
													 , NULL
													 , p_user_id
													 , temp_tag.t_date
													 , null
													 , null
													 , null
													 , null
													 , null
													 , null
													 , null
													 , null
													 , null
													 , null
													  );
			END;

			IF (temp_tag.sharedwith_id IS NOT NULL)
			THEN
				v_ref_type	:= '51025';
			ELSE
				v_ref_type	:= '';
			END IF;

			UPDATE t2656_audit_entry t2656
			   SET t2656.c205_part_number_id = temp_tag.part_id
				 , t2656.c2656_control_number = temp_tag.part_control_id
				 , t2656.c207_set_id = temp_tag.set_id
				 , t2656.c2656_comments = temp_tag.comments
				 , t2656.c901_status = temp_tag.status_id
				 , t2656.c2656_ref_id = temp_tag.sharedwith_id
				 , t2656.c901_ref_type = v_ref_type
				 , t2656.c901_location_type = temp_tag.location_id
				 , t2656.c2656_location_id = temp_tag.location_detl_id
				 , t2656.c2656_counted_by = temp_tag.counted_by
				 , t2656.c2656_counted_date = temp_tag.t_date
				 , t2656.c903_upload_file_list = p_out_file_id
				 , t2656.c2656_last_updated_date = SYSDATE
				 , t2656.c2656_last_updated_by = p_user_id
				 , t2656.c2656_qty = 1
				 , t2656.c901_entry_type = '51030'
				 , t2656.c901_reconciliation_status = '51035'
			 WHERE t2656.c2651_field_sales_details_id = v_field_sales_details_id
			   AND t2656.c5010_tag_id = temp_tag.tag_id
			   AND t2656.c2656_void_fl IS NULL;

			IF SQL%ROWCOUNT = 0
			THEN
				INSERT INTO t2656_audit_entry t2656
							(t2656.c2656_audit_entry_id, t2656.c5010_tag_id, t2656.c2651_field_sales_details_id
						   , t2656.c205_part_number_id, t2656.c2656_control_number, t2656.c207_set_id
						   , t2656.c2656_comments, t2656.c901_status, t2656.c2656_ref_id, t2656.c901_ref_type
						   , t2656.c901_location_type, t2656.c2656_location_id, t2656.c2656_counted_by
						   , t2656.c2656_counted_date, t2656.c903_upload_file_list, t2656.c2656_qty
						   , t2656.c901_entry_type, t2656.c901_reconciliation_status, t2656.c2656_created_by
						   , t2656.c2656_created_date
							)
					 VALUES (s2656_audit_entry.NEXTVAL, temp_tag.tag_id, v_field_sales_details_id
						   , temp_tag.part_id, temp_tag.part_control_id, temp_tag.set_id
						   , temp_tag.comments, temp_tag.status_id, temp_tag.sharedwith_id, v_ref_type
						   , temp_tag.location_id, temp_tag.location_detl_id, temp_tag.counted_by
						   , temp_tag.t_date, p_out_file_id, 1
						   , '51030', '51035', p_user_id
						   , SYSDATE
							);
			END IF;
		END LOOP;

		UPDATE t2651_field_sales_details t2651
		   SET t2651.c901_reconciliation_status = '51035'
		 WHERE t2651.c701_distributor_id = p_dist_id
		   AND t2651.c2650_field_sales_audit_id = p_audit_id
		   AND t2651.c2651_void_fl IS NULL;
	END gm_ac_ld_upload_data;

	PROCEDURE gm_fch_audit_entry_details (
		p_audit_entry_id	   IN		t2656_audit_entry.c2656_audit_entry_id%TYPE
	  , p_audit_entry_detail   OUT		TYPES.cursor_type
	)
	AS
/*******************************************************
 * Description : Procedure to fetch Audit Entry Details
 * Author		 : Rajeshwaran Varatharajan
 *******************************************************/
	BEGIN
		OPEN p_audit_entry_detail
		 FOR
			 SELECT t2656.c5010_tag_id tagid, t2656.c205_part_number_id partnumber
				  , get_partnum_desc (t2656.c205_part_number_id) partname, c2656_control_number controlnumber
				  , t2656.c207_set_id setid, c2656_comments comments, t2656.c901_status statusid
				  , t2656.c2656_ref_id borrfrom, get_code_name (t2656.c901_location_type) loctyp
				  , t2656.c901_location_type locationid, c2656_location_id names
				  , TO_CHAR (TRUNC (c2656_counted_date), 'MM/DD/YYYY') counteddate
			   FROM t2656_audit_entry t2656
			  WHERE t2656.c2656_audit_entry_id = p_audit_entry_id;
	END gm_fch_audit_entry_details;

	/*******************************************************
		* Description : Procedure to update the audit Entry Details
		* Author	   : Rajeshwaran Varatharajan
		*******************************************************/
	PROCEDURE gm_sav_auditentrydetails (
		p_tagid 		   IN	t2656_audit_entry.c5010_tag_id%TYPE
	  , p_ptnum 		   IN	t2656_audit_entry.c205_part_number_id%TYPE
	  , p_ctrlnum		   IN	t2656_audit_entry.c2656_control_number%TYPE
	  , p_setid 		   IN	t2656_audit_entry.c207_set_id%TYPE
	  , p_comments		   IN	t2656_audit_entry.c2656_comments%TYPE
	  , p_status_id 	   IN	t2656_audit_entry.c901_status%TYPE
	  , p_distributor_id   IN	t2656_audit_entry.c2656_ref_id%TYPE
	  , p_loctype		   IN	t2656_audit_entry.c901_location_type%TYPE
	  , p_locid 		   IN	t2656_audit_entry.c2656_location_id%TYPE
	  , p_cuntdate		   IN	VARCHAR2
	  , p_userid		   IN	t2656_audit_entry.c2656_last_updated_by%TYPE
	  , p_audit_entry_id   IN	t2656_audit_entry.c2656_audit_entry_id%TYPE
	)
	AS
		cnt 		   NUMBER;
	BEGIN
		SELECT COUNT (t208.c205_part_number_id)
		  INTO cnt
		  FROM t208_set_details t208, t205d_part_attribute t205d
		 WHERE t208.c207_set_id = p_setid
		   AND t205d.c205_part_number_id = t208.c205_part_number_id
		   AND t208.c205_part_number_id = p_ptnum
		   AND t205d.c901_attribute_type = 92340   -- tagable
		   AND t205d.c205d_void_fl IS NULL
		   AND t208.c208_void_fl IS NULL;

		IF (cnt < 1)
		THEN
			raise_application_error ('-20857', '');
		END IF;

		UPDATE t2656_audit_entry
		   SET c5010_tag_id = p_tagid
			 , c205_part_number_id = p_ptnum
			 , c2656_control_number = p_ctrlnum
			 , c207_set_id = p_setid
			 , c2656_comments = p_comments
			 , c901_status = DECODE (p_status_id, 0, NULL, p_status_id)
			 , c901_ref_type = DECODE (p_distributor_id, 0, NULL, 51025)
			 , c2656_ref_id = p_distributor_id
			 , c901_location_type = p_loctype
			 , c2656_location_id = p_locid
			 , c2656_counted_date = TO_DATE (p_cuntdate, 'MM/DD/YYYY')
			 , c2656_last_updated_by = p_userid
			 , c2656_last_updated_date = SYSDATE
		 WHERE c2656_audit_entry_id = p_audit_entry_id;

		UPDATE t5010_tag t5010
		   SET t5010.c205_part_number_id = p_ptnum
			 , t5010.c207_set_id = p_setid
			 , t5010.c5010_control_number = p_ctrlnum
			 , t5010.c5010_location_id = get_location_detail_id (p_locid, p_loctype)
			 , t5010.c901_location_type = p_loctype
			 , t5010.c5010_last_updated_by = p_userid
			 , t5010.c5010_created_date = SYSDATE
		 WHERE t5010.c5010_tag_id = p_tagid AND t5010.c5010_void_fl IS NULL AND c5010_last_updated_trans_id IS NULL;
	END gm_sav_auditentrydetails;

/*******************************************************
 * Description : Procedure to fetch the addresses of the salesrep for a given distributor
 * Author		 : Brinalg
 *******************************************************/
	PROCEDURE gm_fch_rep_addresses (
		p_dist_id		   IN		t2651_field_sales_details.c701_distributor_id%TYPE
	  , p_address_detail   OUT		TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_address_detail
		 FOR
			 SELECT DISTINCT	v700.rep_name
							 || ' - '
							 || t106.c106_add1
							 || '  '
							 || t106.c106_add2
							 || ' , '
							 || t106.c106_city
							 || ' , '
							 || get_code_name (t106.c901_state)
							 || ' - '
							 || t106.c106_zip_code NAME
						   , t106.c106_address_id ID
						FROM v700_territory_mapping_detail v700, t703_sales_rep t703, t106_address t106
					   WHERE d_id = p_dist_id
						 AND ac_id IS NOT NULL
						 AND t703.c703_sales_rep_id = v700.rep_id
						 AND t106.c101_party_id = t703.c101_party_id
					 -- AND t703.c703_void_fl IS NULL	 //Void flag need to be consider
					-- AND t106.c106_void_fl IS NULL	//Void flag need to be consider , because they changed the address
			 ORDER BY		 NAME;
	END gm_fch_rep_addresses;
END gm_pkg_ac_field_sales;
/
