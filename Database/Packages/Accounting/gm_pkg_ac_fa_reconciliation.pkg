--@"c:\database\packages\accounting\gm_pkg_ac_fa_reconciliation.pkg"
CREATE OR REPLACE
PACKAGE gm_pkg_ac_fa_reconciliation
IS
    --
    /****************************************************************************
    *
    * Description : Procedure to fetch Summary report
    * Author: Jignesh Shah
    ****************************************************************************
    */
PROCEDURE gm_fch_summary_report (
        p_locked_from IN t2652_field_sales_lock.C2652_LOCKED_DATE%TYPE,
        p_locked_to   IN t2652_field_sales_lock.C2652_LOCKED_DATE%TYPE,
        p_out_audit_info OUT TYPES.cursor_type,
        p_out_fieldsales_info OUT TYPES.cursor_type) ;
    /********************************************************************
    * Description : Procedure to fetch the Approval Flagged Details
    * Author      : hreddi
    *********************************************************************/
PROCEDURE gm_fch_appr_recon_deviation (
        p_reconcil_status IN VARCHAR2,
        p_out_approval OUT TYPES.cursor_type) ;
    /********************************************************************
    * Description : Procedure to save the Approval Flagged Details
    * Author      : hreddi
    *********************************************************************/
PROCEDURE gm_sav_appr_deviation (
        p_input_str IN VARCHAR2,
        p_user_id   IN t2656_audit_entry.c2656_created_by%TYPE) ;
    /********************************************************************
    * Description : Procedure to save the reconcile
    * Author      : hreddi
    *********************************************************************/
PROCEDURE gm_sav_reconcile_deviation (
        p_input_str IN CLOB,
        p_user_id   IN t2656_audit_entry.c2656_created_by%TYPE,
        p_out_txn_ids OUT VARCHAR2) ;
    /****************************************************************************
    ****
    * Description : Procedure to fetch PDF Report (Confirmation Sheet)
    *****************************************************************************
    ****/
PROCEDURE gm_fetch_confirmation_report (
        p_audit_id IN t2652_field_sales_lock.c2650_field_sales_audit_id%TYPE,
        p_dist_id  IN t2651_field_sales_details.c701_distributor_id%TYPE,
        p_out_header_info OUT TYPES.cursor_type,
        p_out_overall_count_cur OUT TYPES.cursor_type,
        p_out_sets_missing_cur OUT TYPES.cursor_type,
        p_out_sets_extra_cur OUT TYPES.cursor_type,
        p_out_borrowed_cur OUT TYPES.cursor_type,
        p_out_sets_matching_cur OUT TYPES.cursor_type) ;
    /****************************************************************************
    ****
    * Description : Procedure to fetch Audit Header information
    *****************************************************************************
    ****/
PROCEDURE gm_fch_audit_header (
        p_audit_id IN t2652_field_sales_lock.c2650_field_sales_audit_id%TYPE,
        p_dist_id  IN t2651_field_sales_details.c701_distributor_id%TYPE,
        p_out_header_info OUT TYPES.cursor_type) ;
    /****************************************************************************
    ****
    * Description : Procedure to fetch overall count PDF Report
    *****************************************************************************
    ****/
PROCEDURE gm_fch_overall_count (
        p_audit_id IN t2652_field_sales_lock.c2650_field_sales_audit_id%TYPE,
        p_dist_id  IN t2651_field_sales_details.c701_distributor_id%TYPE,
        p_out_overall_counts_cur OUT TYPES.cursor_type) ;
    /****************************************************************************
    **********************************
    * Description : Procedure to fetch Field Sales Flagged report(Negative,
    Positive and Borrowed,Lent, Transfer)
    *****************************************************************************
    **********************************/
PROCEDURE gm_fch_field_sales_flagged_rpt (
        p_audit_id   IN t2652_field_sales_lock.c2650_field_sales_audit_id%TYPE,
        p_dist_id    IN t2651_field_sales_details.c701_distributor_id%TYPE,
        p_tag_status IN VARCHAR2,
        p_out_flagged_report OUT TYPES.cursor_type) ;
    /****************************************************************************
    ****
    * Description : Procedure to fetch Matching set + Verified Post Count set
    reports.
    *****************************************************************************
    ****/
PROCEDURE gm_fch_fs_match_ver_rpt (
        p_audit_id   IN t2652_field_sales_lock.c2650_field_sales_audit_id%TYPE,
        p_dist_id    IN t2651_field_sales_details.c701_distributor_id%TYPE,
        p_tag_status IN VARCHAR2,
        p_out_match_report OUT TYPES.cursor_type) ;
    /****************************************************************************
    ****
    * Description : Procedure to fetch Pending and Invoiced charge report
    *****************************************************************************
    ****/
PROCEDURE gm_fch_inv_pend_charge_rpt (
        p_audit_id IN t2652_field_sales_lock.c2650_field_sales_audit_id%TYPE,
        p_dist_id  IN t2651_field_sales_details.c701_distributor_id%TYPE,
        p_ref_id   IN VARCHAR2,
        p_out_report OUT TYPES.cursor_type,
        p_out_details OUT TYPES.cursor_type) ;
    /****************************************************************************
    ****
    * Description : Procedure to transfer a set to another distributor.
    * Author    : Mihir Patel
    *****************************************************************************
    ****/
PROCEDURE gm_sav_reconcile_transfer (
        p_audit_entry_id IN t2656_audit_entry.c2656_audit_entry_id%TYPE,
        p_comment        IN VARCHAR2,
        p_user_id        IN t101_user.c101_user_id%TYPE,
        p_out_txnid OUT VARCHAR2) ;
    /****************************************************************************
    ****
    * Description : Procedure to adjust (post) consignments.Call consignment
    adjustment procedures to write up/off set/
    *part
    *gm_ac_sav_cons_adj is a commom proc used from Consignment Screen as well as
    to reconcile from Field Audit.
    * Author    : Mihir Patel
    *****************************************************************************
    ****/
PROCEDURE gm_sav_consign_reconcile (
        p_audit_entry_id IN t2656_audit_entry.c2656_audit_entry_id%TYPE,
        p_comment        IN VARCHAR2,
        p_user_id        IN t101_user.c101_user_id%TYPE,
        p_out_txnid OUT VARCHAR2) ;
    /******************************************************************************
    **
    * Description : Procedure to update status of Audit/Field Sales.
    * Author    : Mihir Patel
    *******************************************************************************/
PROCEDURE gm_sav_update_status (
        p_audit_entry_id IN t2656_audit_entry.c2656_audit_entry_id%TYPE,
        p_txn_id         IN VARCHAR2,
        p_user_id        IN t2656_audit_entry.c2656_last_updated_by%TYPE) ;
    /********************************************************
    * Description : Procedure to save the Email Flag value
    *********************************************************/
PROCEDURE gm_sav_email_fl (
        p_input_str  IN VARCHAR2,
        p_email_type IN VARCHAR2) ;
    /****************************************************************************
    ****
    * Description : Procedure to fetch Pending and Invoiced charge report
    *****************************************************************************
    ****/
PROCEDURE gm_fch_reconcile_email (
        p_out_emaildet OUT TYPES.cursor_type) ;
    /****************************************************************************
    ****
    Description : Procedure to save the email type for Loaner type Tags
    *****************************************************************************
    ****/
PROCEDURE gm_sav_loaner_reconcile (
        p_audit_entry_id IN t2656_audit_entry.c2656_audit_entry_id%TYPE,
        p_user_id        IN t101_user.c101_user_id%TYPE) ;
    /***************************************************
    Description : This procedure adjust Consignemnt by creating RA and CN based
    on adjustment type.
    *********************************************************/
PROCEDURE gm_ac_sav_cons_adj (
        p_type        IN t901_code_lookup.c901_code_id%TYPE,
        p_distid      IN t701_distributor.c701_distributor_id%TYPE,
        p_part_inpstr IN VARCHAR2,
        p_setid       IN t207_set_master.c207_set_id%TYPE,
        p_tag_id      IN t5010_tag.c5010_tag_id%TYPE,
        p_comment     IN VARCHAR2,
        p_screen_type IN VARCHAR2,
        p_user_id     IN t101_user.c101_user_id%TYPE,
        p_out_txn_ids OUT VARCHAR2) ;
    /***************************************************
    Description : This procedure is used to link tags
    *********************************************************/
PROCEDURE gm_ac_sav_link_tag (
        p_tag_id      IN t5010_tag.c5010_tag_id%TYPE,
        p_setid       IN t207_set_master.c207_set_id%TYPE,
        p_pnum        IN t205_part_number.c205_part_number_id%TYPE,
        p_distid      IN t701_distributor.c701_distributor_id%TYPE,
        p_type        IN NUMBER,
        p_refid       IN VARCHAR2,
        p_screen_type IN VARCHAR2,
        p_user_id     IN t5010_tag.c5010_last_updated_by%TYPE) ;
    /***************************************************
    Description : This procedure is used to adjust set
    *********************************************************/
PROCEDURE gm_ac_sav_set_adj (
        p_type        IN t901_code_lookup.c901_code_id%TYPE,
        p_distid      IN t701_distributor.c701_distributor_id%TYPE,
        p_setid       IN t207_set_master.c207_set_id%TYPE,
        p_comment     IN VARCHAR2,
        p_screen_type IN VARCHAR2,
        p_user_id     IN t101_user.c101_user_id%TYPE,
        p_out_refid OUT VARCHAR2) ;
    /***************************************************
    Description : This procedure is used to adjust set
    *********************************************************/
PROCEDURE gm_ac_sav_part_adj (
        p_type        IN t901_code_lookup.c901_code_id%TYPE,
        p_distid      IN t701_distributor.c701_distributor_id%TYPE,
        p_part_inpstr IN VARCHAR2,
        p_comment     IN VARCHAR2,
        p_user_id     IN t101_user.c101_user_id%TYPE,
        p_out_refid OUT VARCHAR2) ;
    /***************************************************
    Description : This procedure is used validate parts
    *********************************************************/
PROCEDURE gm_ac_sav_validate_parts (
        p_inputstr IN VARCHAR2,
        p_out_tagpart OUT t208_set_details.c205_part_number_id%TYPE) ;
    /****************************************************************************
    ****
    * Description : Procedure to write-off single set for a particular
    distributor
    * Author   : Mihir Patel
    *****************************************************************************
    ****/
PROCEDURE gm_ac_sav_set_writeoff (
        p_setid   IN t207_set_master.c207_set_id%TYPE,
        p_distid  IN t2651_field_sales_details.c701_distributor_id%TYPE,
        p_user_id IN t506_returns.c506_created_by%TYPE,
        p_comment IN VARCHAR2,
        p_out_rmaid OUT t506_returns.c506_rma_id%TYPE) ;
    /****************************************************************************
    ****
    * Description : Procedure to write-off parts for a particular distributor
    * Author  : Mihir Patel
    *****************************************************************************
    ****/
PROCEDURE gm_ac_sav_part_writeoff (
        p_inputstr IN VARCHAR2,
        p_distid   IN t701_distributor.c701_distributor_id%TYPE,
        p_user_id  IN t506_returns.c506_created_by%TYPE,
        p_comment  IN VARCHAR2,
        p_out_rmaid OUT t506_returns.c506_rma_id%TYPE) ;
    /****************************************************************************
    **
    * Description : Procedure to write-up set for a particular distributor
    * Author  : Mihir Patel
    *****************************************************************************
    **/
PROCEDURE gm_ac_sav_set_writeup (
        p_setid   IN t207_set_master.c207_set_id%TYPE,
        p_distid  IN t701_distributor.c701_distributor_id%TYPE,
        p_user_id IN t504_consignment.c504_created_by%TYPE,
        p_comment IN VARCHAR2,
        p_out_conid OUT t504_consignment.c504_consignment_id%TYPE) ;
    /****************************************************************************
    **
    * Description : Procedure to write-up part for a particular distributor
    * Author  : Mihir Patel
    *****************************************************************************
    **/
PROCEDURE gm_ac_sav_part_writeup (
        p_inputstr IN VARCHAR2,
        p_distid   IN t701_distributor.c701_distributor_id%TYPE,
        p_user_id  IN t504_consignment.c504_created_by%TYPE,
        p_comment  IN VARCHAR2,
        p_out_conid OUT t504_consignment.c504_consignment_id%TYPE) ;
    /****************************************************************************
    **
    * Description : Procedure to WRITE-OFF-INACTIVE-DISTRIBUTOR
    * Author  : Mihir Patel
    *****************************************************************************
    **/
PROCEDURE gm_sav_inactive_dist_writeoff (
        p_distid t701_distributor.c701_distributor_id%TYPE,
        p_user_id IN t506_returns.c506_created_by%TYPE,
        p_comment IN VARCHAR2,
        p_out_rmaid OUT t506_returns.c506_rma_id%TYPE,
        p_out_conid OUT t504_consignment.c504_consignment_id%TYPE) ;
    /**************************************************************************
    * Description : Procedure to update the void flag in tag table
    * Author  :
    ***************************************************************************/
PROCEDURE gm_update_tag_recon (
        p_tag_id  IN t5010_tag.c5010_tag_id%TYPE,
        p_void_fl IN t5010_tag.c5010_void_fl%TYPE,
        p_user_id IN t5010_tag.c5010_last_updated_by%TYPE) ;
    /**************************************************************************
    * Description : Procedure to Reconciled the Missing Tag
    * Author  :
    ***************************************************************************/
PROCEDURE gm_sav_missing_tag_recon (
        p_audit_entry_id IN t2656_audit_entry.c2656_audit_entry_id%TYPE,
        p_user_id        IN t101_user.c101_user_id%TYPE,
        p_out_txnid OUT VARCHAR2) ;
END gm_pkg_ac_fa_reconciliation;
/