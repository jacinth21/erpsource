--@"C:\Database\Packages\Accounting\gm_pkg_ac_ar_revenue_sampling.bdy";

CREATE OR REPLACE
PACKAGE BODY gm_pkg_ac_ar_revenue_sampling
IS
/***********************************************************************
* Description : Procedure used to fetch the PO and DO details
* this procedure is called GmCustomerbean for generate PO and DO Report
************************************************************************/
PROCEDURE gm_fch_revenue_sampling_dtls(
    p_order_id IN t501_order.c501_order_id%TYPE,
    p_out OUT TYPES.cursor_type )
AS
BEGIN
	OPEN p_out FOR 
		  SELECT c901_code_id TRTYPEID,c901_code_nm TRTYPE,OREVID,ORDID,PO_DO_DTLS,PODOVAL,HIST_FL,UPBY,UDATE,CODEID,AUDID FROM t901_code_lookup t901,
		  (SELECT c5003_order_revenue_sample_dtls_id OREVID,
		    c501_order_id ORDID,
		    c901_po_dtls PO_DO_DTLS,
		    get_code_name(c901_po_dtls) PODOVAL,
		    c5003_po_history_fl HIST_FL,
		    get_user_name(NVL(c5003_po_last_updated_by,c5003_po_created_by)) UPBY,
		    NVL(c5003_po_last_updated_date,c5003_po_created_date) UDATE,
		    '53017' CODEID, --Code id for PO
		    '1241' AUDID   --Audit id  for PO
		  FROM t5003_order_revenue_sample_dtls
		  WHERE c501_order_id           = p_order_id
		  AND c5003_void_fl        IS NULL
		  ) t501 WHERE t901.c901_code_id=53017 AND t901.c901_code_id = t501.CODEID(+)
		  UNION
		  SELECT c901_code_id TRTYPEID,c901_code_nm TRTYPE,OREVID,ORDID,PO_DO_DTLS,PODOVAL,HIST_FL,UPBY,UDATE,CODEID,AUDID FROM t901_code_lookup t901,
		    (SELECT c5003_order_revenue_sample_dtls_id OREVID,
		      c501_order_id ORDID,
		      c901_do_dtls PO_DO_DTLS,
		       get_code_name(c901_do_dtls) PODOVAL,
		      c5003_do_history_fl HIST_FL,
		      get_user_name(NVL(c5003_do_last_updated_by,c5003_do_created_by)) UPBY,
		      NVL(c5003_do_last_updated_date,c5003_do_created_date) UDATE,
		      '53018' CODEID, --Code id for DO
		      '1242' AUDID--Audit id for DO
		    FROM t5003_order_revenue_sample_dtls
		    WHERE c501_order_id = p_order_id
		    AND c5003_void_fl        IS NULL
		    ) t501
		  WHERE t901.c901_code_id=53018 --PO
		  AND t901.c901_code_id  = t501.CODEID(+) ;
END gm_fch_revenue_sampling_dtls;

/********************************************************************************
* Description : Procedure used to save the PO and DO details
* This Procedure called from Gm_update_po for update/insert PO and DO Details
*********************************************************************************/
PROCEDURE gm_sav_revenue_sampling_dtls(
    p_order_id IN t501_order.c501_order_id%TYPE,
    p_po_val   IN t5003_order_revenue_sample_dtls.c901_po_dtls%TYPE,
    p_do_val   IN t5003_order_revenue_sample_dtls.c901_do_dtls%TYPE,
    p_user_id  IN t5003_order_revenue_sample_dtls.c5003_po_created_by%TYPE)
AS
BEGIN
	  UPDATE t5003_order_revenue_sample_dtls
	  Set c901_po_dtls             = p_po_val ,
	    c901_do_dtls = p_do_val,
	    c5003_po_last_updated_by   = DECODE(c901_po_dtls,p_po_val,c5003_po_last_updated_by,p_user_id),
	    c5003_po_last_updated_date = DECODE(c901_po_dtls,p_po_val,c5003_po_last_updated_date,CURRENT_DATE),
	    c5003_po_created_date      = NVL(c5003_po_created_date,DECODE(c901_po_dtls,p_po_val,c5003_po_created_date,CURRENT_DATE)),
	    c5003_po_created_by        = NVL(c5003_po_created_by,DECODE(c901_po_dtls,p_po_val,c5003_po_created_by,p_user_id)),
	    c5003_do_last_updated_by   = DECODE(c901_do_dtls,p_do_val,c5003_do_last_updated_by,p_user_id),
	    c5003_do_last_updated_date = DECODE(c901_do_dtls,p_do_val,c5003_do_last_updated_date,CURRENT_DATE),
	    c5003_do_created_by        = NVL(c5003_do_created_by,DECODE(c901_do_dtls,p_do_val,c5003_do_created_by,p_user_id)),
	    c5003_do_created_date      = NVL(c5003_do_created_date,DECODE(c901_do_dtls,p_do_val,c5003_do_created_date,CURRENT_DATE)),
	    c5003_updated_by           = p_user_id,
	    c5003_updated_date         = CURRENT_DATE
	  WHERE c501_order_id          = p_order_id
	  AND c5003_void_fl IS NULL;
  --check the update count for save the PO details
	  IF SQL%ROWCOUNT              = 0 AND (p_po_val IS NOT NULL OR p_do_val IS NOT NULL) THEN
	    INSERT
	    INTO t5003_order_revenue_sample_dtls
	      (
	        C901_Po_Dtls,
	        C901_Do_Dtls,
	        c501_order_id,
	        C5003_Po_Created_By,
	        C5003_Po_Created_Date,
	        C5003_Do_Created_By,
	        C5003_Do_Created_Date
	      )
	      VALUES
	      (
	        P_Po_Val ,
	        p_do_val ,
	        P_Order_Id ,
	        DECODE(NVL(p_po_val,'-9999'),'-9999',NULL,p_user_id) ,
	        DECODE(NVL(p_po_val,'-9999'),'-9999',NULL,CURRENT_DATE),
	        DECODE(NVL(p_do_val,'-9999'),'-9999',NULL,p_user_id) ,
	        DECODE(NVL(p_do_val,'-9999'),'-9999',NULL,CURRENT_DATE)
	      );
	  END IF;
END gm_sav_revenue_sampling_dtls;

/****************************************************************************
* Description : Procedure used to save the PO and DO details
* This Procedure called from Gm_update_po for split PO and DO Details While Edit PO 
***************************************************************************/

PROCEDURE gm_split_revenue_do_po_dtls
  (
    p_str IN VARCHAR2,
    p_po OUT t5003_order_revenue_sample_dtls.c901_po_dtls%TYPE,
    p_do OUT t5003_order_revenue_sample_dtls.c901_do_dtls%TYPE
  )
AS
	  v_string VARCHAR2
	  (
	    30000
	  )
	  := p_str;
	  v_substring   VARCHAR2 (1000);
	  v_do_po_type  VARCHAR2(20);
	  v_do_po_val   VARCHAR2(20);
	  v_rev_id      VARCHAR2(20);
	  v_strlen      NUMBER                      := NVL (LENGTH (p_str), 0);
BEGIN
	 IF v_strlen > 0
		THEN
	  WHILE INSTR (v_string, '|') <> 0
	  LOOP
		    v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
		    v_string    := SUBSTR (v_string, INSTR (v_string, '|')    + 1);
		    --
		    v_rev_id := NULL;
		    v_do_po_type := NULL;
		    v_do_po_val  := NULL;
		    --
		    v_rev_id   := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
		    v_substring  := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1);
		    v_do_po_type   := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
		    v_substring  := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1);
		    v_do_po_val    := v_substring;
	    
		    --save po and do value based on 
		    IF v_do_po_type = 53017 THEN
		      p_po       := v_do_po_val;
		    ELSIF v_do_po_type = 53018 THEN
		      p_do          := v_do_po_val;
	    END IF;
	  END LOOP;
	 END IF; 
END gm_split_revenue_do_po_dtls;

 /*******************************************************************************
  * Description : Procedure used to save the child order revenue details
  * This Procedure called from gm_pkg_ac_invoice_info.gm_sync_ord_attribute,
  * GM_SAVE_CREDIT_SALES  for update/insert the PO And DO Details for child order

  ********************************************************************************/   
PROCEDURE  gm_sav_child_order_revenue_dtls(
    p_order_id       IN t501_order.c501_order_id%TYPE,
    p_parent_ord_id  IN t501_order.c501_parent_order_id%TYPE,
    p_user_id        IN t5003_order_revenue_sample_dtls.c5003_po_created_by%TYPE )
AS
    v_po_dtls    t5003_order_revenue_sample_dtls.c901_po_dtls%TYPE;
    v_do_dtls    t5003_order_revenue_sample_dtls.c901_do_dtls%TYPE;
BEGIN
	BEGIN
		SELECT c901_po_dtls,c901_do_dtls
        	INTO  v_po_dtls,v_do_dtls
        	FROM t5003_order_revenue_sample_dtls
        	WHERE c501_order_id        = p_parent_ord_id
        	AND c5003_void_fl        IS NULL;
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
       RETURN;      
       END;
       --this procedure is used to update/insert the PO and DO details 
	gm_pkg_ac_ar_revenue_sampling.gm_sav_revenue_sampling_dtls(p_order_id,v_po_dtls,v_do_dtls,p_user_id);
END gm_sav_child_order_revenue_dtls;	
END gm_pkg_ac_ar_revenue_sampling;
/
