
--@"C:\Database\Packages\Accounting\gm_pkg_ac_invoice_tax.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_ac_invoice_tax
IS

   /***************************************************************
   * Description : Procedure to update tax info by order line items

   * Author     : Mahavishnu
   *************************************************************/
   PROCEDURE gm_sav_taxrate_by_invoice (
      p_invoice_id   IN   t501_order.c503_invoice_id%TYPE,
      p_custpo       IN   t501_order.c501_customer_po%TYPE,
      p_user_id      IN   t501_order.C501_CREATED_BY%TYPE
   )
   AS
	v_company_id        t1900_company.c1900_company_id%TYPE;
 	v_tax_decimal_fl    VARCHAR2(2) ;
 	
	CURSOR  cur_invoice is 
	SELECT t502.C502_ITEM_ORDER_ID item_ord_id
	FROM t501_ORDER t501,
 	T502_ITEM_ORDER t502
	WHERE t501.C501_ORDER_ID=t502.C501_ORDER_ID
	AND t501.C503_INVOICE_ID=p_invoice_id
	AND t501.C501_VOID_FL  IS NULL
	AND t502.C502_VOID_FL  IS NULL;
 	
 BEGIN
	 --
	 
	 SELECT C1900_COMPANY_ID INTO v_company_id FROM T503_INVOICE WHERE C503_INVOICE_ID=p_invoice_id AND C503_VOID_FL IS NULL;

	-- Rule value to ignore decimal values in tax amount
	v_tax_decimal_fl := get_rule_value_by_company (v_company_id,'IGNORE_TAX_DECIMAL', v_company_id);

	 FOR inv_cur IN cur_invoice
        
        LOOP
        	
        
        IF v_tax_decimal_fl = 'Y' THEN --To ignore decimal value in tax amount
		
		UPDATE t502_item_order
		SET C502_TAX_AMT         = TRUNC(C502_ITEM_QTY * NVL(C502_ITEM_PRICE,0) * (NVL(C502_VAT_RATE,0)/100),0)
		WHERE C502_ITEM_ORDER_ID = inv_cur.item_ord_id
		AND c502_void_fl        IS NULL;
		   
  
 	 ELSE 
  
		UPDATE t502_item_order 
		SET C502_TAX_AMT     = C502_ITEM_QTY * NVL(C502_ITEM_PRICE,0) * (NVL(C502_VAT_RATE,0)/100)
		WHERE C502_ITEM_ORDER_ID =  inv_cur.item_ord_id
		and c502_void_fl is null;
   
  	END IF;
  	
        	
        	
        END LOOP;
	
	
  	--to update total tax amount for orders
  	gm_pkg_ac_sales_tax.gm_upd_orders_tax_cost (p_invoice_id, p_user_id) ;
 
   END gm_sav_taxrate_by_invoice;
   
   /***************************************************************
   * Description : Procedure to update the VAT % for the line items
   * while issuing Credit through A/R Screen
   * Author     : Tamizh
   *************************************************************/
   
   
   PROCEDURE gm_sav_vatrate_by_part 
      (
      p_order_id     IN   t501_order.c501_order_id%TYPE,
      p_vatinputstr  IN   VARCHAR2,
      p_userid      IN   t501_order.C501_CREATED_BY%TYPE
      )
     	
   AS
       v_string	   VARCHAR2 (30000) := p_vatinputstr;
       v_substring VARCHAR2 (30000);
       v_pnum	   VARCHAR2 (20);
       v_price 	   NUMBER (15, 2);
       v_vat_rate  NUMBER (15, 2);
       v_tax_amt   NUMBER (15, 2);
       v_item_id   VARCHAR2 (50);
       v_item_ord_id VARCHAR2 (30000);
	
  BEGIN
	
	  SELECT rtrim (xmlagg (xmlelement (e, c502_item_order_id  || ',')) .extract ('//text()')) INTO v_item_ord_id
          FROM t502_item_order 
         WHERE c501_order_id = p_order_id
      ORDER BY c502_item_order_id desc;

	  WHILE INSTR (v_string, '|') <> 0
	  LOOP
	  
	     v_substring   := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
	      v_string := SUBSTR (v_string, INSTR (v_string, '|') + 1);
          v_pnum	    := NULL;
          v_price       := NULL;
          v_vat_rate    := NULL;
          v_item_id     := NULL;

          v_pnum	    := SUBSTR (v_substring,1,INSTR (v_substring, ',') - 1);
          v_substring   := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
          v_price 	    := SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
          v_substring   := SUBSTR (v_substring,INSTR (v_substring, ',') + 1);
          v_vat_rate    := v_substring;
          v_tax_amt     := ROUND(v_price*(v_vat_rate/100),0);
	
          v_item_id     := SUBSTR (v_item_ord_id,1,INSTR (v_item_ord_id, ',') - 1);
          v_item_ord_id := SUBSTR (v_item_ord_id, INSTR (v_item_ord_id, ',') + 1);

	       UPDATE T502_ITEM_ORDER T502
	        SET  c502_tax_amt  = v_tax_amt,
	        c502_vat_rate = v_vat_rate
	        WHERE c205_part_number_id = v_pnum
	        AND c502_void_fl IS NULL
	        AND c501_order_id  = p_order_id
	        AND c502_item_order_id = v_item_id;
	      
END LOOP;
  
  END gm_sav_vatrate_by_part;


/***************************************************************
* Description : Procedure used to store the avatax details to new table (request, response, error)
*
* Author     : mmuthusamy
*************************************************************/
PROCEDURE gm_sav_inv_avatax_json_dtl (
        p_invoice_id           IN t503_invoice.c503_invoice_id%TYPE,
        p_avatax_request_json  IN CLOB,
        p_avatax_response_json IN CLOB,
        p_avatax_error         IN CLOB,
        p_userid               IN t503_invoice.C503_CREATED_BY%TYPE)
AS

BEGIN
    -- to update the avatTax data to new table
     UPDATE t5032_invoice_tax_api_dtls
    SET c5032_json_request_str = p_avatax_request_json,
    	c5032_json_response_str = p_avatax_response_json,
        c5032_error_dtls       = p_avatax_error, 
        c5032_last_updated_by = p_userid,
        c5032_last_updated_date = CURRENT_DATE
      WHERE c503_invoice_id = p_invoice_id
        AND c5032_void_fl  IS NULL;
        
       -- insert the data to new table 
    IF (SQL%ROWCOUNT        = 0) THEN
         INSERT
           INTO t5032_invoice_tax_api_dtls
            (
                c503_invoice_id, c5032_json_request_str, c5032_json_response_str
              , c5032_error_dtls, c5032_last_updated_by, c5032_last_updated_date
            )
            VALUES
            (
                p_invoice_id, p_avatax_request_json, p_avatax_response_json
              , p_avatax_error, p_userid, CURRENT_DATE
            ) ;
    END IF;
    --
    
END gm_sav_inv_avatax_json_dtl;
   
	
END gm_pkg_ac_invoice_tax;
/
