/* Formatted on 2011/12/02 12:23 (Formatter Plus v4.8.0) */
--@"c:\database\packages\accounting\gm_pkg_ac_audit_reconciliation.bdy"
CREATE OR REPLACE
PACKAGE BODY gm_pkg_ac_audit_reconciliation
IS
    /********************************************************************************
    * Description : Procedure to fetch Variance Report by Set and Global Variance by Distributor
    * Author    : Pramaraj
    *********************************************************************************/
PROCEDURE gm_fch_variance_by_set (
        p_audit_id IN t2652_field_sales_lock.c2650_field_sales_audit_id%TYPE,
        p_set_id   IN t2657_field_sales_details_lock.c207_set_id%TYPE,
        p_ter_id   IN v700_territory_mapping_detail.ter_id%TYPE,
        p_out_report_info OUT TYPES.cursor_type)
AS
BEGIN
    OPEN p_out_report_info FOR SELECT NULL pline, p_set_id set_id, dist_id, dist_name, SUM (audit_qty) audit_qty, SUM (
    system_qty) system_qty, SUM (deviation_qty) deviation_qty, SUM (transfer_to) transfer_to, SUM (extra) extra, SUM (
    borrowed_from) borrowed_from, SUM (returns) returns, SUM (MISSING) MISSING, SUM (transfer_from) transfer_from, SUM
    (lend_to) lend_to, SUM (flagged_qty) flagged_qty, get_dist_unresolved_qty (p_audit_id, dist_id, p_set_id)
    unresolved_qty, DECODE (ABS (SUM (deviation_qty)), SUM (flagged_qty), 0, 1) disp_nonflagged, get_log_flag (dist_id
    || p_set_id, 1251) log_fl, (ABS (SUM (deviation_qty)) - SUM (approved_qty)) approved_qty, SUM (reconciled_qty)
    reconciled_qty FROM
    (
         SELECT v700.d_id dist_id, v700.dist_name, NVL (system_qty, 0) system_qty
          , NVL (audit_qty, 0) audit_qty, (NVL (audited_data.audit_qty, 0) - NVL (locked_data.system_qty, 0))
            deviation_qty, NVL (audited_data.transfer_to, 0) transfer_to, NVL (audited_data.extra, 0) extra
          , NVL (audited_data.borrowed_from, 0) borrowed_from, NVL (audited_data.returns, 0) returns, NVL (
            audited_data.MISSING, 0) MISSING, NVL (audited_data.transfer_from, 0) transfer_from, NVL (
            audited_data.lend_to, 0) lend_to, NVL (audited_data.approved_qty, 0) approved_qty, NVL (
            audited_data.reconciled_qty, 0) reconciled_qty, (NVL (audited_data.transfer_to, 0)   + NVL (
            audited_data.extra, 0)                                                               + NVL (
            audited_data.borrowed_from, 0)                                                       + NVL (
            audited_data.returns, 0)                                                             + NVL (
            audited_data.MISSING, 0)                                                             + NVL (
            audited_data.transfer_from, 0)                                                       + NVL (
            audited_data.lend_to, 0)) flagged_qty, (NVL (audited_data.transfer_to, 0)            + NVL (
            audited_data.extra, 0)                                                               + NVL (
            audited_data.borrowed_from, 0)) positive_flagged_qty, (NVL (audited_data.returns, 0) + NVL (
            audited_data.MISSING, 0)                                                             + NVL (
            audited_data.transfer_from, 0)                                                       + NVL (
            audited_data.lend_to, 0)) negative_flagged_qty
           FROM
            (
                SELECT DISTINCT d_id, get_distributor_name (d_id) dist_name
                   FROM v700_territory_mapping_detail
                  WHERE NVL (ter_id, - 9999) = DECODE (p_ter_id, 0, NVL (ter_id, - 9999), p_ter_id)
            )
            v700, (
                 SELECT t2652.c701_distributor_id dist_id, SUM (c2657_qty) system_qty
                   FROM t2652_field_sales_lock t2652, t2657_field_sales_details_lock t2657, (
                         SELECT t207.c207_set_id, t205.c205_part_number_id
                           FROM t207_set_master t207, t208_set_details t208, t205d_part_attribute t205d
                          , t205_part_number t205
                          WHERE t207.c207_type            = 4070
                            AND t207.c207_void_fl        IS NULL
                            AND t207.c207_set_id          = t208.c207_set_id
                            AND t208.c208_critical_fl     = 'Y'
                            AND t208.c205_part_number_id  = t205d.c205_part_number_id
                            AND t205d.c901_attribute_type = 92340
                            AND t205.c205_part_number_id  = t205d.c205_part_number_id
                    )
                    critical_part
                  WHERE t2652.c2650_field_sales_audit_id = p_audit_id
                    AND t2657.c207_set_id                = DECODE (p_set_id, '0', t2657.c207_set_id, p_set_id)
                    AND t2652.c2652_field_sales_lock_id  = t2657.c2652_field_sales_lock_id
                    AND t2657.c207_set_id                = critical_part.c207_set_id
                    AND t2657.c205_part_number_id        = critical_part.c205_part_number_id
                    AND t2652.c2652_void_fl             IS NULL
                    AND t2657.c2657_void_fl             IS NULL
               GROUP BY t2652.c701_distributor_id
            )
            locked_data, (
                 SELECT dist_id, (SUM (audit_qty)) audit_qty, SUM (transfer_to) transfer_to
                  , SUM (extra) extra, SUM (borrowed_from) borrowed_from, SUM (returns) returns
                  , SUM (MISSING) MISSING, SUM (transfer_from) transfer_from, SUM (lend_to) lend_to
                  , SUM (manual_entry) manual_entry, SUM (approved_qty) approved_qty, SUM (reconciled_qty)
                    reconciled_qty
                   FROM
                    (
                         SELECT t2651.c701_distributor_id dist_id, SUM (DECODE (t2656.c901_entry_type, 51030,
                            t2656.c2656_qty, 0)) audit_qty, SUM (DECODE (t2656.c901_ref_type, 51023, 1, 0))
                            transfer_from, SUM (DECODE (t2656.c901_ref_type, 51024, 1, 0)) extra, SUM (DECODE (
                            t2656.c901_ref_type, 51025, 1, 0)) borrowed_from, SUM (DECODE (t2656.c901_ref_type, 51029,
                            1, 0)) returns, SUM (DECODE (t2656.c901_ref_type, 51028, 1, 0)) MISSING, SUM (DECODE (
                            t2656.c901_ref_type, 51027, 1, 0)) transfer_to, SUM (DECODE (t2656.c901_ref_type, 51026, 1,
                            0)) lend_to, SUM (DECODE (t2656.c901_entry_type, 51031, 1, 0)) manual_entry, SUM (DECODE (
                            t2656.c901_reconciliation_status, 51037, 1, 0)) approved_qty, SUM (DECODE (
                            t2656.c901_reconciliation_status, 51038, 1, 0)) reconciled_qty
                           FROM t2651_field_sales_details t2651, t2656_audit_entry t2656
                          WHERE t2651.c2650_field_sales_audit_id   = p_audit_id
                            AND t2656.c207_set_id                  = DECODE (p_set_id, '0', t2656.c207_set_id, p_set_id)
                            AND t2656.c207_set_id                 IS NOT NULL
                            AND t2656.c205_part_number_id         IS NOT NULL
                            AND t2656.c2656_control_number        IS NOT NULL
                            AND t2651.c2651_field_sales_details_id = t2656.c2651_field_sales_details_id
                            AND t2656.c2656_void_fl               IS NULL
                            AND t2651.c2651_void_fl               IS NULL
                       GROUP BY t2651.c701_distributor_id
                      UNION ALL
                         SELECT t2656.c2656_ref_id dist_id, 0 audit_qty, SUM (DECODE (t2656.c901_ref_type, 51027, 1, 0)
                            ) transfer_from, SUM (DECODE (t2656.c901_ref_type, 51024, 1, 0)) extra, SUM (DECODE (
                            t2656.c901_ref_type, 51026, 1, 0)) borrowed_from, SUM (DECODE (t2656.c901_ref_type, 51029,
                            1, 0)) returns, SUM (DECODE (t2656.c901_ref_type, 51028, 1, 0)) MISSING, SUM (DECODE (
                            t2656.c901_ref_type, 51023, 1, 0)) transfer_to, SUM (DECODE (t2656.c901_ref_type, 51025, 1,
                            0)) lend_to, SUM (DECODE (t2656.c901_entry_type, 51031, 1, 0)) manual_entry, SUM (0)
                            approved_qty, SUM (DECODE (t2656.c901_reconciliation_status, 51038, 1, 0)) reconciled_qty
                           FROM t2656_audit_entry t2656, t2651_field_sales_details t2651
                          WHERE t2651.c2650_field_sales_audit_id   = p_audit_id
                            AND t2656.c207_set_id                  = DECODE (p_set_id, '0', t2656.c207_set_id, p_set_id)
                            AND t2656.c207_set_id                 IS NOT NULL
                            AND t2656.c205_part_number_id         IS NOT NULL
                            AND t2656.c2656_control_number        IS NOT NULL
                            AND t2651.c2651_field_sales_details_id = t2656.c2651_field_sales_details_id
                            AND t2656.c2656_void_fl               IS NULL
                            AND t2651.c2651_void_fl               IS NULL
                            AND t2656.c2656_ref_id                IS NOT NULL
                            AND t2656.c901_ref_type               IN (51023, 51025, 51027, 51026)
                       GROUP BY t2656.c2656_ref_id
                    )
               GROUP BY dist_id
            )
            audited_data
          WHERE v700.d_id                = locked_data.dist_id(+)
            AND v700.d_id                = audited_data.dist_id(+)
            AND (locked_data.system_qty <> 0
            OR audited_data.audit_qty   <> 0
            OR (NVL (transfer_to, 0)    <> 0)
            OR (NVL (borrowed_from, 0)  <> 0)
            OR (NVL (transfer_from, 0)  <> 0)
            OR (NVL (lend_to, 0)        <> 0))
    )
    GROUP BY dist_id, dist_name ORDER BY dist_name;
END gm_fch_variance_by_set;
/********************************************************************************
* Description : Procedure to fetch Variance Report by Dist and Global Variance by Set
* Author    : Bgupta
*********************************************************************************/
--=
PROCEDURE gm_fch_variance_by_dist (
        p_audit_id IN t2652_field_sales_lock.c2650_field_sales_audit_id%TYPE,
        p_dist_id  IN t2651_field_sales_details.c701_distributor_id%TYPE,
        p_out_report_info OUT TYPES.cursor_type)
AS
BEGIN
    OPEN p_out_report_info FOR SELECT NULL pline, p_dist_id dist_id, set_id, set_desc, pnum, pdesc, SYSTEM, TYPE, SUM (
audit_qty) audit_qty, SUM (system_qty) system_qty, SUM (deviation_qty) deviation_qty, SUM (transfer_to) transfer_to,
SUM (extra) extra, SUM (borrowed_from) borrowed_from, SUM (returns) returns, SUM (MISSING) MISSING, SUM (transfer_from)
transfer_from, SUM (lend_to) lend_to, SUM (flagged_qty) flagged_qty, get_set_unresolved_qty (p_audit_id, p_dist_id,
set_id) unresolved_qty, DECODE (ABS (SUM (deviation_qty)), SUM (flagged_qty), 0, 1) disp_nonflagged, get_log_flag (
p_dist_id || set_id, 1251) log_fl, (ABS (SUM (deviation_qty)) - SUM (approved_qty)) approved_qty, SUM (reconciled_qty)
reconciled_qty FROM
(
     SELECT t207.c205_part_number_id pnum, t207.c205_part_num_desc pdesc, t207.c207_set_id set_id
      , t207.c207_set_nm set_desc, t207.SYSTEM SYSTEM, t207.TYPE TYPE
      , NVL (audited_data.audit_qty, 0) audit_qty, NVL (locked_data.system_qty, 0) system_qty, (NVL (
        audited_data.audit_qty, 0) - NVL (locked_data.system_qty, 0)) deviation_qty, NVL (audited_data.transfer_to, 0)
        transfer_to, NVL (audited_data.extra, 0) extra, NVL (audited_data.borrowed_from, 0) borrowed_from
      , NVL (audited_data.returns, 0) returns, NVL (audited_data.MISSING, 0) MISSING, NVL (audited_data.transfer_from,
        0) transfer_from, NVL (audited_data.lend_to, 0) lend_to, NVL (audited_data.approved_qty, 0) approved_qty
      , NVL (audited_data.reconciled_qty, 0) reconciled_qty, (NVL (audited_data.transfer_to, 0) + NVL (
        audited_data.extra, 0)                                                                  + NVL (
        audited_data.borrowed_from, 0)                                                          + NVL (
        audited_data.returns, 0)                                                                + NVL (
        audited_data.MISSING, 0)                                                                + NVL (
        audited_data.transfer_from, 0)                                                          + NVL (
        audited_data.lend_to, 0)) flagged_qty, (NVL (audited_data.transfer_to, 0)               + NVL (
        audited_data.extra, 0)                                                                  + NVL (
        audited_data.borrowed_from, 0)) positive_flagged_qty, (NVL (audited_data.returns, 0)    + NVL (
        audited_data.MISSING, 0)                                                                + NVL (
        audited_data.transfer_from, 0)                                                          + NVL (
        audited_data.lend_to, 0)) negative_flagged_qty
       FROM
        (
             SELECT t207.c207_set_id, t207.c207_set_nm, t205.c205_part_number_id
              , t205.c205_part_num_desc, get_code_name_alt (t207.c901_hierarchy) TYPE, get_set_name (
                t207.c207_set_sales_system_id) SYSTEM
               FROM t207_set_master t207, t208_set_details t208, t205d_part_attribute t205d
              , t205_part_number t205
              WHERE t207.c207_type            = 4070
                AND t207.c207_void_fl        IS NULL
                AND t207.c207_set_id          = t208.c207_set_id
                AND t208.c208_critical_fl     = 'Y'
                AND t208.c205_part_number_id  = t205d.c205_part_number_id
                AND t205d.c901_attribute_type = 92340
                AND t205.c205_part_number_id  = t205d.c205_part_number_id
        )
        t207, (
             SELECT t2657.c205_part_number_id, t2657.c207_set_id, SUM (c2657_qty) system_qty
               FROM t2652_field_sales_lock t2652, t2657_field_sales_details_lock t2657, (
                     SELECT t207.c207_set_id, t205.c205_part_number_id
                       FROM t207_set_master t207, t208_set_details t208, t205d_part_attribute t205d
                      , t205_part_number t205
                      WHERE t207.c207_type            = 4070
                        AND t207.c207_void_fl        IS NULL
                        AND t207.c207_set_id          = t208.c207_set_id
                        AND t208.c208_critical_fl     = 'Y'
                        AND t208.c205_part_number_id  = t205d.c205_part_number_id
                        AND t205d.c901_attribute_type = 92340
                        AND t205.c205_part_number_id  = t205d.c205_part_number_id
                )
                critical_part
              WHERE t2652.c2650_field_sales_audit_id = p_audit_id
                AND t2652.c701_distributor_id        = NVL (p_dist_id, t2652.c701_distributor_id)
                AND t2652.c2652_field_sales_lock_id  = t2657.c2652_field_sales_lock_id
                AND t2652.c2652_void_fl             IS NULL
                AND t2657.c2657_void_fl             IS NULL
                AND t2657.c207_set_id                = critical_part.c207_set_id
                AND t2657.c205_part_number_id        = critical_part.c205_part_number_id
           GROUP BY t2657.c205_part_number_id, t2657.c207_set_id
        )
        locked_data, (
             SELECT c205_part_number_id, c207_set_id, (SUM (audit_qty)) audit_qty
              , SUM (transfer_to) transfer_to, SUM (extra) extra, SUM (borrowed_from) borrowed_from
              , SUM (returns) returns, SUM (MISSING) MISSING, SUM (transfer_from) transfer_from
              , SUM (lend_to) lend_to, SUM (manual_entry) manual_entry, SUM (approved_qty) approved_qty
              , SUM (reconciled_qty) reconciled_qty
               FROM
                (
                     SELECT t2656.c205_part_number_id, t2656.c207_set_id, SUM (DECODE (t2656.c901_entry_type, 51030,
                        t2656.c2656_qty, 0)) audit_qty, SUM (DECODE (t2656.c901_ref_type, 51023, 1, 0)) transfer_from,
                        SUM (DECODE (t2656.c901_ref_type, 51024, 1, 0)) extra, SUM (DECODE (t2656.c901_ref_type, 51025,
                        1, 0)) borrowed_from, SUM (DECODE (t2656.c901_ref_type, 51029, 1, 0)) returns, SUM (DECODE (
                        t2656.c901_ref_type, 51028, 1, 0)) MISSING, SUM (DECODE (t2656.c901_ref_type, 51027, 1, 0))
                        transfer_to, SUM (DECODE (t2656.c901_ref_type, 51026, 1, 0)) lend_to, SUM (DECODE (
                        t2656.c901_entry_type, 51031, 1, 0)) manual_entry, SUM (DECODE (
                        t2656.c901_reconciliation_status, 51037, 1, 0)) approved_qty, SUM (DECODE (
                        t2656.c901_reconciliation_status, 51038, 1, 0)) reconciled_qty
                       FROM t2651_field_sales_details t2651, t2656_audit_entry t2656
                      WHERE t2651.c2650_field_sales_audit_id   = p_audit_id
                        AND (t2651.c701_distributor_id         = NVL (p_dist_id, t2651.c701_distributor_id))
                        AND t2656.c207_set_id                 IS NOT NULL
                        AND t2656.c205_part_number_id         IS NOT NULL
                        AND t2656.c2656_control_number        IS NOT NULL
                        AND t2651.c2651_field_sales_details_id = t2656.c2651_field_sales_details_id
                        AND t2656.c2656_void_fl               IS NULL
                        AND t2651.c2651_void_fl               IS NULL
                   GROUP BY t2656.c205_part_number_id, t2656.c207_set_id
                  UNION ALL
                     SELECT t2656.c205_part_number_id, t2656.c207_set_id, 0 audit_qty
                      , SUM (DECODE (t2656.c901_ref_type, 51027, 1, 0)) transfer_from, SUM (DECODE (t2656.c901_ref_type
                        , 51024, 1, 0)) extra, SUM (DECODE (t2656.c901_ref_type, 51026, 1, 0)) borrowed_from, SUM (
                        DECODE (t2656.c901_ref_type, 51029, 1, 0)) returns, SUM (DECODE (t2656.c901_ref_type, 51028, 1,
                        0)) MISSING, SUM (DECODE (t2656.c901_ref_type, 51023, 1, 0)) transfer_to, SUM (DECODE (
                        t2656.c901_ref_type, 51025, 1, 0)) lend_to, SUM (DECODE (t2656.c901_entry_type, 51031, 1, 0))
                        manual_entry, SUM (0) approved_qty, SUM (DECODE (t2656.c901_reconciliation_status, 51038, 1, 0)
                        ) reconciled_qty
                       FROM t2651_field_sales_details t2651, t2656_audit_entry t2656
                      WHERE t2651.c2650_field_sales_audit_id   = p_audit_id
                        AND t2656.c2656_ref_id                 = NVL (p_dist_id, t2656.c2656_ref_id)
                        AND t2656.c207_set_id                 IS NOT NULL
                        AND t2656.c205_part_number_id         IS NOT NULL
                        AND t2656.c2656_control_number        IS NOT NULL
                        AND t2651.c2651_field_sales_details_id = t2656.c2651_field_sales_details_id
                        AND t2656.c2656_void_fl               IS NULL
                        AND t2651.c2651_void_fl               IS NULL
                        AND t2656.c901_ref_type               IN (51023, 51025, 51027, 51026)
                   GROUP BY t2656.c205_part_number_id, t2656.c207_set_id
                )
           GROUP BY c205_part_number_id, c207_set_id
        )
        audited_data
      WHERE t207.c207_set_id         = locked_data.c207_set_id(+)
        AND t207.c205_part_number_id = locked_data.c205_part_number_id(+)
        AND t207.c207_set_id         = audited_data.c207_set_id(+)
        AND t207.c205_part_number_id = audited_data.c205_part_number_id(+)
        AND (NVL (audit_qty, 0)     <> 0
        OR NVL (system_qty, 0)      <> 0
        OR (NVL (transfer_to, 0)    <> 0)
        OR (NVL (borrowed_from, 0)  <> 0)
        OR (NVL (transfer_from, 0)  <> 0)
        OR (NVL (lend_to, 0)        <> 0))
)
GROUP BY set_id, set_desc, pnum, pdesc, SYSTEM, TYPE ORDER BY set_id, set_desc, pnum, pdesc, SYSTEM, TYPE;
END gm_fch_variance_by_dist;
/********************************************************************************
* Description : Procedure to fetch Flag Report For Positive and Negative
* Author    : Pramaraj
*********************************************************************************/
PROCEDURE gm_fch_variance (
        p_audit_id IN t2652_field_sales_lock.c2650_field_sales_audit_id%TYPE,
        p_dist_id  IN t2651_field_sales_details.c701_distributor_id%TYPE,
        p_set_id   IN t2657_field_sales_details_lock.c207_set_id%TYPE,
        p_out_header_info OUT TYPES.cursor_type,
        p_overall_report OUT TYPES.cursor_type,
        p_audit_report OUT TYPES.cursor_type,
        p_system_report OUT TYPES.cursor_type,
        p_associated_report_positive OUT TYPES.cursor_type,
        p_associated_report_negative OUT TYPES.cursor_type,
        p_returns_cur OUT TYPES.cursor_type)
AS
    v_pnum t208_set_details.c205_part_number_id%TYPE;
BEGIN
     SELECT t208.c205_part_number_id
       INTO v_pnum
       FROM t207_set_master t207, t208_set_details t208, t205d_part_attribute t205d
      WHERE t207.c207_set_id          = p_set_id
        AND t208.c207_set_id          = t207.c207_set_id
        AND t208.c208_critical_fl     = 'Y'
        AND t205d.c205_part_number_id = t208.c205_part_number_id
        AND t205d.c901_attribute_type = 92340 -- tagable
        AND t205d.c205d_void_fl      IS NULL
        AND t208.c208_void_fl        IS NULL
        AND t207.c207_void_fl        IS NULL;
    OPEN p_out_header_info FOR SELECT t207.c207_set_nm set_desc,
    TO_CHAR (SYSDATE, GET_RULE_VALUE('DATEFMT', 'DATEFORMAT')) curdate,
    get_set_name (t207.c207_set_sales_system_id) system_name,
    get_distributor_name (p_dist_id) dist_name,
    t208.c205_part_number_id pnum,
    get_rule_value (p_set_id, 'SET - COST') set_price,
    get_partnum_desc (t208.c205_part_number_id) pdesc FROM t207_set_master t207,
    t208_set_details t208,
    t205d_part_attribute t205d WHERE t207.c207_set_id = p_set_id AND t208.c207_set_id = t207.c207_set_id AND
    t208.c208_critical_fl                             = 'Y' AND t205d.c205_part_number_id = t208.c205_part_number_id
    AND t205d.c901_attribute_type                     = 92340 -- tagable
    AND t205d.c205d_void_fl                          IS NULL AND t208.c208_void_fl IS NULL AND t207.c207_void_fl IS
    NULL;
    OPEN p_overall_report FOR SELECT NVL (audit_count, 0) audit_count,
    NVL (system_qty, 0) system_qty,
    (NVL (audit_count, 0)         - NVL (system_qty, 0)) deviation_qty,
    NVL (flagged_positive_qty, 0) + NVL (flagged_negative_qty, 0) flagged_qty,
    ABS ( ( (NVL (audit_count, 0) - NVL (system_qty, 0)) + (NVL (flagged_negative_qty, 0) - NVL (flagged_positive_qty,
    0)))) unresolved_qty FROM
    (
         SELECT get_audit_count (p_audit_id, p_dist_id, p_set_id) audit_count
           FROM DUAL
    )
    , (
         SELECT get_system_count (p_audit_id, p_dist_id, p_set_id) system_qty
           FROM DUAL
    )
    , (
         SELECT get_flagged_positive_count (p_audit_id, p_dist_id, p_set_id) flagged_positive_qty
           FROM DUAL
    )
    , (
         SELECT get_flagged_negative_count (p_audit_id, p_dist_id, p_set_id) flagged_negative_qty
           FROM DUAL
    )
    ;
    OPEN p_audit_report FOR SELECT t2656.c2656_audit_entry_id ID,
    t2656.c5010_tag_id tag_id,
    t2656.c205_part_number_id pnum,
    t2656.c2656_control_number cnum,
    t2656.c901_ref_type flag,
    t2656.c2656_ref_id reason,
    t2656.c2656_ref_details ref_details,
    get_user_name (t2656.c2656_counted_by) counted_by,
    TO_CHAR (t2656.c2656_counted_date, GET_RULE_VALUE('DATEFMT', 'DATEFORMAT')) counted_date,
    t2656.c2656_comments comments, CASE
    WHEN t2651.c901_reconciliation_status >= 51037 THEN
        51037
    ELSE
        t2656.c901_reconciliation_status
    END approve,
    get_user_name (t2656.c2656_last_updated_by) updated_by,
    TO_CHAR ( (t2656.c2656_last_updated_date), GET_RULE_VALUE('DATEFMT', 'DATEFORMAT')) updated_date,
    TO_CHAR (SYSDATE, GET_RULE_VALUE('DATEFMT', 'DATEFORMAT')) curdate,
    c901_posting_option posting_option,
    c2656_posting_comment posting_comment FROM t2651_field_sales_details t2651,
    t2656_audit_entry t2656 WHERE t2651.c701_distributor_id = p_dist_id AND t2651.c2650_field_sales_audit_id =
    p_audit_id AND t2651.c2651_field_sales_details_id       = t2656.c2651_field_sales_details_id AND t2656.c207_set_id
                                                            = p_set_id AND t2656.c901_entry_type = 51030 AND
    t2651.c2651_void_fl                                    IS NULL AND t2656.c2656_void_fl IS NULL ORDER BY tag_id,
    pnum,
    cnum;
    OPEN p_system_report FOR SELECT locked_data.lock_id ID,
    pnum,
    cnum,
    txn_type,
    txn_id,
    ref_type flag,
    reason reason,
    ref_details, CASE
    WHEN status >= 51037 THEN
        status
    ELSE
        approve
    END approve,
    TO_CHAR (ldate, GET_RULE_VALUE('DATEFMT', 'DATEFORMAT')) ldate,
    updated_by,
    updated_date,
    TO_CHAR (SYSDATE, GET_RULE_VALUE('DATEFMT', 'DATEFORMAT')) curdate,
    qty,
    posting_option,
    posting_comment FROM
    (
         SELECT t2659.c2659_field_sales_ref_lock_id lock_id, t2659.c205_part_number_id pnum, t2659.c2659_control_number
            cnum, get_code_name (t2659.c901_ref_type) txn_type, t2659.c2659_ref_id txn_id
          , t2652.c2652_locked_date ldate, t2659.c2659_qty qty, t2651.c901_reconciliation_status status
           FROM t2652_field_sales_lock t2652, t2659_field_sales_ref_lock t2659, t2651_field_sales_details t2651
          WHERE t2652.c2652_field_sales_lock_id  = t2659.c2652_field_sales_lock_id
            AND t2652.c2652_field_sales_lock_id  = t2651.c2652_field_sales_lock_id
            AND t2652.c701_distributor_id        = p_dist_id
            AND t2652.c2650_field_sales_audit_id = p_audit_id
            AND t2659.c205_part_number_id        = v_pnum
            AND t2659.c901_ref_type              = 51000
            AND t2652.c2652_void_fl             IS NULL
            AND t2659.c2659_void_fl             IS NULL
    )
    locked_data, (
         SELECT t2656.c2659_field_sales_ref_lock_id lock_id, t2656.c901_ref_type ref_type, t2656.c2656_ref_id reason
          , t2656.c2656_ref_details ref_details, CASE
                WHEN t2651.c901_reconciliation_status >= 51037
                THEN 51037
                ELSE t2656.c901_reconciliation_status
            END approve, NVL (TRIM (get_user_name (t2656.c2656_last_updated_by)), get_user_name (t2656.c2656_created_by
            )) updated_by, NVL (TO_CHAR (t2656.c2656_last_updated_date, GET_RULE_VALUE('DATEFMT', 'DATEFORMAT')), TO_CHAR (
            t2656.c2656_created_date, GET_RULE_VALUE('DATEFMT', 'DATEFORMAT'))) updated_date, c901_posting_option posting_option,
            c2656_posting_comment posting_comment
           FROM t2656_audit_entry t2656, t2651_field_sales_details t2651
          WHERE t2651.c2650_field_sales_audit_id     = p_audit_id
            AND t2651.c701_distributor_id            = p_dist_id
            AND t2656.c207_set_id                    = p_set_id
            AND t2656.c2651_field_sales_details_id   = t2651.c2651_field_sales_details_id
            AND t2656.c2656_void_fl                 IS NULL
            AND t2651.c2651_void_fl                 IS NULL
            AND t2656.c901_entry_type                = 51031
            AND t2656.c2659_field_sales_ref_lock_id IS NOT NULL
    )
    audited_data WHERE locked_data.lock_id = audited_data.lock_id(+) ORDER BY pnum,
    cnum,
    txn_type,
    txn_id;
    OPEN p_associated_report_positive FOR SELECT NVL (t2656.c5010_tag_id, t2656.c2656_ref_details) tag_id,
    t2656.c2656_control_number control_no,
    get_code_name (t901.c902_code_nm_alt) flag,
    get_distributor_name (t2651.c701_distributor_id) reason FROM t2656_audit_entry t2656,
    t2651_field_sales_details t2651,
    t901_code_lookup t901 WHERE t2656.c2651_field_sales_details_id = t2651.c2651_field_sales_details_id AND
    t2656.c2656_ref_id                                             = p_dist_id AND t2656.c207_set_id = p_set_id AND
    t2651.c2650_field_sales_audit_id                               = p_audit_id AND t2656.c2656_void_fl IS NULL AND
    t2651.c2651_void_fl                                           IS NULL AND t2656.c901_ref_type IN (51025, 51026,
    51027, 51023) AND t901.c901_code_grp                           = 'FLFPQ' AND t901.c901_code_id =
    t2656.c901_ref_type;
    OPEN p_associated_report_negative FOR SELECT NVL (t2656.c5010_tag_id, t2656.c2656_ref_details) tag_id,
    t2656.c2656_control_number control_no,
    get_code_name (t901.c902_code_nm_alt) flag,
    get_distributor_name (t2651.c701_distributor_id) reason FROM t2656_audit_entry t2656,
    t2651_field_sales_details t2651,
    t901_code_lookup t901 WHERE t2656.c2651_field_sales_details_id = t2651.c2651_field_sales_details_id AND
    t2656.c2656_ref_id                                             = p_dist_id AND t2656.c207_set_id = p_set_id AND
    t2651.c2650_field_sales_audit_id                               = p_audit_id AND t2656.c2656_void_fl IS NULL AND
    t2651.c2651_void_fl                                           IS NULL AND t2656.c901_ref_type IN (51025, 51026,
    51027, 51023) AND t901.c901_code_grp                           = 'FLFNQ' AND t901.c901_code_id =
    t2656.c901_ref_type;
    OPEN p_returns_cur FOR SELECT t2659.c2659_field_sales_ref_lock_id lock_id,
    t2659.c205_part_number_id pnum,
    t2659.c2659_control_number cnum,
    get_code_name (t2659.c901_ref_type) txn_type,
    t2659.c2659_ref_id txn_id,
    t2652.c2652_locked_date ldate,
    TO_CHAR (t506.c506_credit_date, GET_RULE_VALUE('DATEFMT', 'DATEFORMAT')) credit_date,
    t2659.c2659_qty qty FROM t2652_field_sales_lock t2652,
    t2659_field_sales_ref_lock t2659,
    t506_returns t506 WHERE t2652.c2652_field_sales_lock_id = t2659.c2652_field_sales_lock_id AND
    t2652.c701_distributor_id                               = p_dist_id AND t2652.c2650_field_sales_audit_id =
    p_audit_id AND t2659.c205_part_number_id                = v_pnum AND t2659.c901_ref_type = 51001 -- (Returns)
    AND t506.c506_rma_id                                    = t2659.c2659_ref_id AND t2652.c2652_void_fl IS NULL AND
    t2659.c2659_void_fl                                    IS NULL;
END gm_fch_variance;
/********************************************************************************
* Description : Procedure to save Flag Report For Positive and Negative
* Author    : Pramaraj
*********************************************************************************/
PROCEDURE gm_sav_flag_variance (
        p_audit_id      IN t2652_field_sales_lock.c2650_field_sales_audit_id%TYPE,
        p_dist_id       IN t2651_field_sales_details.c701_distributor_id%TYPE,
        p_set_id        IN t2657_field_sales_details_lock.c207_set_id%TYPE,
        p_inputs        IN VARCHAR2,
        p_variance_type IN VARCHAR2,
        p_user_id       IN t2656_audit_entry.c2656_last_updated_by%TYPE)
AS
    v_details_id t2651_field_sales_details.c2651_field_sales_details_id%TYPE;
    v_pnum t2656_audit_entry.c205_part_number_id%TYPE;
    v_cnum t2656_audit_entry.c2656_control_number%TYPE;
    v_strlen         NUMBER          := NVL (LENGTH (p_inputs), 0) ;
    v_string         VARCHAR2 (4000) := p_inputs;
    v_substring      VARCHAR2 (4000) ;
    v_approved_count NUMBER;
    v_id             VARCHAR2 (20) ;
    v_unresolved_qty NUMBER;
    v_resolved_count NUMBER;
    v_tag_count      NUMBER;
    v_ref_type t2656_audit_entry.c901_ref_type%TYPE;
    v_ref_id t2656_audit_entry.c2656_ref_id%TYPE;
    v_ref_details t2656_audit_entry.c2656_ref_details%TYPE;
    v_recon_status t2656_audit_entry.c901_reconciliation_status%TYPE;
    v_post_option t2656_audit_entry.c901_posting_option%TYPE;
    v_post_comment t2656_audit_entry.c2656_posting_comment%TYPE;
    v_pkey_str VARCHAR2 (4000) ;
    v_ra_count NUMBER;
    v_cnt      NUMBER;
    v_qty      NUMBER;
    v_old_ref_id t2656_audit_entry.c2656_ref_id%TYPE;
    v_old_status t2656_audit_entry.c901_reconciliation_status%TYPE;
BEGIN
    -- check already reconciled
     SELECT COUNT (1)
       INTO v_cnt
       FROM t2651_field_sales_details t2651
      WHERE t2651.c2650_field_sales_audit_id = p_audit_id
        AND t2651.c701_distributor_id        = p_dist_id
        AND t2651.c901_reconciliation_status = 51038 -- REconciled
        AND t2651.c2651_void_fl             IS NULL;
    IF v_cnt                                 > 0 THEN
        raise_application_error ('-20841', '') ;
    END IF;
     SELECT COUNT (1)
       INTO v_qty
       FROM t2651_field_sales_details t2651
      WHERE t2651.c701_distributor_id         = p_dist_id
        AND t2651.c2650_field_sales_audit_id  = p_audit_id
        AND t2651.c901_reconciliation_status IS NOT NULL
        AND t2651.c901_reconciliation_status  = 51037 -- approved
        AND t2651.c2651_void_fl              IS NULL;
    IF v_qty                                 <> 0 THEN
        raise_application_error ('-20842', '') ; --'Distributor is already Approved'
    END IF;
     SELECT t2651.c2651_field_sales_details_id
       INTO v_details_id
       FROM t2651_field_sales_details t2651
      WHERE t2651.c2650_field_sales_audit_id = p_audit_id
        AND t2651.c701_distributor_id        = p_dist_id
        AND t2651.c2651_void_fl             IS NULL FOR UPDATE;
    IF v_strlen                              > 0 THEN
        WHILE INSTR (v_string, '|')         <> 0
        LOOP
            --
            v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
            v_string    := SUBSTR (v_string, INSTR (v_string, '|')    + 1) ;
            --
            v_id           := NULL;
            v_ref_type     := NULL;
            v_ref_id       := NULL;
            v_ref_details  := NULL;
            v_recon_status := NULL;
            --
            v_id           := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            v_substring    := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
            v_ref_type     := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            v_substring    := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
            v_ref_id       := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            v_substring    := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
            v_ref_details  := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            v_substring    := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
            v_recon_status := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            v_substring    := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
            v_post_option  := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            v_substring    := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
            v_post_comment := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            v_substring    := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
            v_post_comment := v_substring;
            --v_recon_status := v_substring;
            /* ************************************************
            -- Only Execute the below condition
            -- if the value changes if not skip the validation
            -- ************************************************/
            BEGIN
                --
                IF (p_variance_type = 'positive') THEN
                     SELECT NVL (t2656.c2656_ref_id, 'XXX'), t2656.c901_reconciliation_status
                       INTO v_old_ref_id, v_old_status
                       FROM t2656_audit_entry t2656
                      WHERE t2656.c2656_audit_entry_id = v_id
                        AND t2656.c2656_void_fl       IS NULL;
                ELSE
                     SELECT NVL (t2656.c2656_ref_id, 'XXX'), t2656.c901_reconciliation_status
                       INTO v_old_ref_id, v_old_status
                       FROM t2656_audit_entry t2656
                      WHERE t2656.c2659_field_sales_ref_lock_id = v_id
                        AND t2656.c2656_void_fl                IS NULL;
                END IF;
                --
            EXCEPTION
            WHEN OTHERS THEN
                NULL;
            END;
            --
            IF (v_old_ref_id <> v_ref_id) THEN
                -- check the associated distributor is already reconciled
                 SELECT COUNT (1)
                   INTO v_cnt
                   FROM t2651_field_sales_details t2651
                  WHERE t2651.c2650_field_sales_audit_id = p_audit_id
                    AND t2651.c701_distributor_id        = v_ref_id
                    AND t2651.c901_reconciliation_status = 51038 -- REconciled
                    AND t2651.c2651_void_fl             IS NULL;
                IF v_cnt                                 > 0 THEN
                    raise_application_error ('-20843', '') ;
                END IF;
                -- check the associated distributor is already approved
                 SELECT COUNT (1)
                   INTO v_qty
                   FROM t2651_field_sales_details t2651
                  WHERE t2651.c701_distributor_id         = v_ref_id
                    AND t2651.c2650_field_sales_audit_id  = p_audit_id
                    AND t2651.c901_reconciliation_status IS NOT NULL
                    AND t2651.c901_reconciliation_status  = 51037 -- approved
                    AND t2651.c2651_void_fl              IS NULL;
                IF v_qty                                 <> 0 THEN
                    raise_application_error ('-20844', '') ; --'Distributor is already Approved'
                END IF;
                --
            END IF;
            IF p_variance_type != 'positive' THEN
                 SELECT t2659.c205_part_number_id, t2659.c2659_control_number
                   INTO v_pnum, v_cnum
                   FROM t2659_field_sales_ref_lock t2659
                  WHERE t2659.c2659_field_sales_ref_lock_id = v_id
                    AND t2659.c2659_void_fl                IS NULL;
                 SELECT COUNT (1)
                   INTO v_approved_count
                   FROM t2656_audit_entry t2656
                  WHERE t2656.c2651_field_sales_details_id  = v_details_id
                    AND t2656.c2659_field_sales_ref_lock_id = v_id
                    AND t2656.c207_set_id                   = p_set_id
                    AND t2656.c205_part_number_id           = v_pnum
                    AND t2656.c2656_control_number          = NVL (v_cnum, t2656.c2656_control_number)
                    AND t2656.c901_reconciliation_status    = 51037
                    AND t2656.c2656_void_fl                IS NULL;
                IF (v_approved_count                        > 0 AND v_post_option = '') THEN
                    raise_application_error ('-20845', '') ;
                END IF;
                 UPDATE t2656_audit_entry t2656
                SET t2656.c901_ref_type                     = v_ref_type, t2656.c2656_ref_id = v_ref_id, t2656.c2656_ref_details =
                    v_ref_details, t2656.c2656_qty          = 1, t2656.c901_reconciliation_status = v_recon_status -- Flagged
                  , t2656.c901_entry_type                   = 51031 -- Manual Entry
                  , t2656.c2656_last_updated_by             = p_user_id, t2656.c2656_last_updated_date = SYSDATE,
                    c901_posting_option                     = v_post_option, c2656_posting_comment = v_post_comment
                  WHERE t2656.c2651_field_sales_details_id  = v_details_id
                    AND t2656.c2659_field_sales_ref_lock_id = v_id
                    AND t2656.c207_set_id                   = p_set_id
                    AND t2656.c205_part_number_id           = v_pnum
                    AND t2656.c2656_control_number          = NVL (v_cnum, t2656.c2656_control_number)
                    AND t2656.c2656_void_fl                IS NULL;
                IF SQL%ROWCOUNT                             = 0 THEN
                     INSERT
                       INTO t2656_audit_entry t2656
                        (
                            t2656.c2656_audit_entry_id, t2656.c2651_field_sales_details_id, t2656.c207_set_id
                          , t2656.c205_part_number_id, t2656.c2656_control_number, t2656.c901_entry_type
                          , t2656.c901_ref_type, t2656.c2656_ref_id, t2656.c2656_ref_details
                          , t2656.c2659_field_sales_ref_lock_id, t2656.c2656_qty, t2656.c901_reconciliation_status
                          , t2656.c2656_created_by, t2656.c2656_created_date, t2656.c2656_counted_by
                          , c901_posting_option, c2656_posting_comment
                        )
                        VALUES
                        (
                            s2656_audit_entry.NEXTVAL, v_details_id, p_set_id
                          , v_pnum, v_cnum, 51031
                          , v_ref_type, v_ref_id, v_ref_details
                          , v_id, 1, v_recon_status
                          , p_user_id, SYSDATE, p_user_id
                          , v_post_option, v_post_comment
                        ) ;
                END IF;
            END IF;
            IF p_variance_type = 'positive' THEN
                 UPDATE t2656_audit_entry t2656
                SET t2656.c901_ref_type            = v_ref_type, t2656.c2656_ref_id = v_ref_id, t2656.c2656_ref_details =
                    v_ref_details, t2656.c2656_qty = 1, t2656.c901_reconciliation_status = v_recon_status -- Flagged
                  , t2656.c901_entry_type          = 51030 -- Audited
                  , t2656.c2656_last_updated_by    = p_user_id, t2656.c2656_last_updated_date = SYSDATE,
                    c901_posting_option            = v_post_option, c2656_posting_comment = v_post_comment
                  WHERE t2656.c2656_audit_entry_id = v_id
                    AND t2656.c2656_void_fl       IS NULL;
            END IF;
            -- IF v_ref_type = 51025 OR v_ref_type = 51023 OR v_ref_type = 51026 OR v_ref_type = 51027
            -- THEN
            -- SELECT  ABS (( get_audit_count (p_audit_id, v_ref_id, p_set_id)
            --      - get_system_count (p_audit_id, v_ref_id, p_set_id)
            --     )
            --     )
            --   - ABS (( get_flagged_negative_count (p_audit_id, v_ref_id, p_set_id)
            --      - get_flagged_positive_count (p_audit_id, v_ref_id, p_set_id)
            --     )
            --     )
            --    INTO v_resolved_count
            --    FROM DUAL;
            --  IF v_resolved_count < 0
            --  THEN
            --   raise_application_error ('-20192', '');
            --  END IF;
            -- END IF;
            v_pkey_str   := v_pkey_str || v_id || ',';
            IF v_ref_type = 51026 OR v_ref_type = 51027 THEN
                 SELECT COUNT (1)
                   INTO v_tag_count
                   FROM t2656_audit_entry t2656
                  WHERE t2656.c5010_tag_id = v_ref_details;
                IF v_tag_count             < 1 THEN
                    raise_application_error ('-20846', '') ;
                END IF;
            END IF;
            IF v_ref_type = 51029 THEN
                 SELECT COUNT (t506.c506_rma_id)
                   INTO v_ra_count
                   FROM t506_returns t506
                  WHERE t506.c506_rma_id         = v_ref_id
                    AND t506.c506_void_fl       IS NULL
                    AND t506.c701_distributor_id = p_dist_id
                    AND t506.c506_status_fl      = 2;
                IF v_ra_count                    < 1 THEN
                    raise_application_error ('-20847', '') ;
                END IF;
            END IF;
        END LOOP;
    END IF;
    my_context.set_my_inlist_ctx (v_pkey_str) ;
    IF p_variance_type = 'negative' THEN
         UPDATE t2656_audit_entry t2656
        SET t2656.c2656_void_fl                          = 'Y', t2656.c2656_last_updated_by = p_user_id, t2656.c2656_last_updated_date = SYSDATE
          WHERE t2656.c2651_field_sales_details_id       = v_details_id
            AND t2656.c207_set_id                        = p_set_id
            AND t2656.c2656_void_fl                     IS NULL
            AND t2656.c901_reconciliation_status        IN (51035, 51036)
            AND t2656.c2659_field_sales_ref_lock_id     IS NOT NULL
            AND t2656.c2659_field_sales_ref_lock_id NOT IN
            (
                 SELECT token FROM v_in_list WHERE token IS NOT NULL
            ) ;
    ELSE
         UPDATE t2656_audit_entry t2656
        SET t2656.c901_ref_type                    = NULL, t2656.c2656_ref_id = NULL, t2656.c2656_ref_details = NULL
          , t2656.c2656_qty                        = 1, t2656.c901_reconciliation_status = 51035 -- INITIATED
          , t2656.c901_entry_type                  = 51030 -- Audited
          , t2656.c2656_last_updated_by            = p_user_id, t2656.c2656_last_updated_date = SYSDATE
          WHERE t2656.c2651_field_sales_details_id = v_details_id
            AND t2656.c207_set_id                  = p_set_id
            AND t2656.c2656_void_fl               IS NULL
            AND t2656.c901_reconciliation_status  IN (51035, 51036)
            AND t2656.c2656_audit_entry_id NOT    IN
            (
                 SELECT token FROM v_in_list WHERE token IS NOT NULL
            ) ;
    END IF;
     SELECT (ABS ( (NVL (audit_count, 0) - NVL (system_qty, 0))) - ABS ( (NVL (flagged_negative_qty, 0) - NVL (
        flagged_positive_qty, 0))))
       INTO v_unresolved_qty
       FROM
        (
             SELECT get_audit_count (p_audit_id, p_dist_id, p_set_id) audit_count
               FROM DUAL
        )
      , (
             SELECT get_system_count (p_audit_id, p_dist_id, p_set_id) system_qty
               FROM DUAL
        )
      , (
             SELECT get_flagged_positive_count (p_audit_id, p_dist_id, p_set_id) flagged_positive_qty
               FROM DUAL
        )
      , (
             SELECT get_flagged_negative_count (p_audit_id, p_dist_id, p_set_id) flagged_negative_qty
               FROM DUAL
        ) ;
    IF (v_unresolved_qty < 0 AND v_post_option = '') THEN
        raise_application_error ('-20845', '') ;
    END IF;
END gm_sav_flag_variance;
/********************************************************************************
* Description : Procedure to fetch Uploaded Data
* Author  : Bgupta
*********************************************************************************/
PROCEDURE gm_fch_uploaded_data (
        p_audit_id     IN t2652_field_sales_lock.c2650_field_sales_audit_id%TYPE,
        p_dist_id      IN t2651_field_sales_details.c701_distributor_id%TYPE,
        p_countby      IN t2656_audit_entry.c2656_counted_by%TYPE,
        p_recon_status IN t2656_audit_entry.c901_reconciliation_status%TYPE,
        p_entry_type   IN t2656_audit_entry.c901_entry_type%TYPE,
        p_code_grp     IN t901_code_lookup.c901_code_grp%TYPE,
        p_out_report_info OUT TYPES.cursor_type)
AS
    v_code_grp t901_code_lookup.c901_code_grp%TYPE;
BEGIN
    v_code_grp     := p_code_grp;
    IF (v_code_grp IS NULL) THEN
        OPEN p_out_report_info FOR SELECT NULL pline,
        c2656_audit_entry_id auditentryid,
        t2656.c5010_tag_id tagid,
        t2656.c205_part_number_id pnum,
        get_partnum_desc (t2656.c205_part_number_id) pdesc,
        t2656.c2656_control_number cnum,
        t2656.c207_set_id setid,
        get_set_name (t2656.c207_set_id) setdesc,
        t2656.c2656_comments comments,
        get_code_name (t2656.c901_status) status,
        DECODE (t2656.c901_ref_type, 51025, get_distributor_name (t2656.c2656_ref_id)) borrowed_frm,
        get_code_name (t2656.c901_location_type) locationtype,
        gm_pkg_ac_audit_reconciliation.get_location_details (t2656.c2656_location_id, t2656.c901_location_type)
        location_detail,
        to_char(t2656.c2656_counted_date,GET_RULE_VALUE('DATEFMT', 'DATEFORMAT')) counteddt,
        get_user_name (t2656.c2656_counted_by) countedby FROM t2656_audit_entry t2656,
        t2651_field_sales_details t2651 WHERE t2651.c2651_field_sales_details_id = t2656.c2651_field_sales_details_id
        AND t2651.c2650_field_sales_audit_id                                     = p_audit_id AND
        t2651.c701_distributor_id                                                = p_dist_id AND t2656.c2656_counted_by
                                                                                 = DECODE (p_countby, '0',
        t2656.c2656_counted_by, p_countby) AND t2656.c901_entry_type             = p_entry_type AND c2656_void_fl IS
        NULL AND c2651_void_fl                                                  IS NULL AND c903_upload_file_list IS
        NOT NULL ORDER BY c2656_audit_entry_id;
    ELSE
        IF (v_code_grp  = 'MATCH') THEN
            v_code_grp := '';
        END IF;
        OPEN p_out_report_info FOR SELECT NULL pline,
        c2656_audit_entry_id auditentryid,
        DECODE (TO_CHAR (t2656.c2656_ref_id), p_dist_id, TO_CHAR (NVL (t2656.c2656_ref_details, t2656.c5010_tag_id)),
        TO_CHAR (t2656.c5010_tag_id)) tagid,
        t2656.c205_part_number_id pnum,
        get_partnum_desc (t2656.c205_part_number_id) pdesc,
        t2656.c2656_control_number cnum,
        t2656.c207_set_id setid,
        get_set_name (t2656.c207_set_id) setdesc,
        t2656.c2656_comments comments,
        get_code_name (t2656.c901_status) status,
        DECODE (t2656.c901_ref_type, 51025, get_distributor_name (t2656.c2656_ref_id), t2656.c901_ref_type,
        get_distributor_name (t2656.c2656_ref_id)) borrowed_frm,
        get_code_name (t2656.c901_location_type) locationtype,
        gm_pkg_ac_audit_reconciliation.get_location_details (t2656.c2656_location_id, t2656.c901_location_type)
        location_detail,
        to_char(t2656.c2656_counted_date,GET_RULE_VALUE('DATEFMT', 'DATEFORMAT')) counteddt,
        get_user_name (t2656.c2656_counted_by) countedby,
        DECODE (t2656.c2656_ref_id, p_dist_id, get_code_name (NVL (TRIM (get_code_name_alt (NVL (t2656.c901_ref_type, 0
        ))), 0)), get_code_name (NVL (t2656.c901_ref_type, 0))) || ' ' || DECODE (TO_CHAR (t2656.c901_ref_type),
        '51029', TO_CHAR (t2656.c2656_ref_id), '51024', ' ', '51028', '', NVL (TRIM (DECODE (t2656.c2656_ref_id,
        p_dist_id, get_distributor_name (t2651.c701_distributor_id), get_distributor_name (t2656.c2656_ref_id))), NVL (
        get_code_name (t2656.c2656_ref_id), t2656.c2656_ref_id))) || ' ' || DECODE (TO_CHAR (t2656.c901_ref_type),
        '51023', NVL (t2656.c2656_ref_details, t2656.c5010_tag_id), '51025', NVL (t2656.c2656_ref_details,
        t2656.c5010_tag_id), '51026', NVL (t2656.c2656_ref_details, t2656.c5010_tag_id), '51027', NVL (
        t2656.c2656_ref_details, t2656.c5010_tag_id), '51023', '', '51028', '', NULL, ' ', ' ' ||
        t2656.c2656_ref_details) reason FROM t2656_audit_entry t2656,
        t2651_field_sales_details t2651,
        t901_code_lookup t901 WHERE t2651.c2651_field_sales_details_id                                         = t2656.c2651_field_sales_details_id AND
        t2651.c2650_field_sales_audit_id                                                                       = p_audit_id AND (t2651.c701_distributor_id =
            p_dist_id OR t2656.c2656_ref_id                                                                    = p_dist_id) AND t2656.c2656_counted_by = DECODE
        (p_countby, '0', t2656.c2656_counted_by, p_countby) AND NVL (t2656.c901_reconciliation_status, - 9999) = DECODE
        (p_recon_status, 0, NVL (t2656.c901_reconciliation_status,                                     - 9999),
        p_recon_status) AND t2656.c901_entry_type                                 = DECODE (p_entry_type, 0, t2656.c901_entry_type, p_entry_type) AND
        c2656_void_fl                                                            IS NULL AND c2651_void_fl IS NULL AND (c903_upload_file_list IS NOT
            NULL OR t2656.c901_entry_type                                         = 51031) AND DECODE (TO_CHAR (t2656.c2656_ref_id), p_dist_id, TO_CHAR
        (get_code_name_alt (t2656.c901_ref_type)), TO_CHAR (t2656.c901_ref_type)) = TO_CHAR (t901.c901_code_id(+)) AND
        NVL (v_code_grp, '-9999')                                                 = DECODE (t901.c901_code_grp, NULL,
        '-9999', t901.c901_code_grp) ORDER BY t2656.c207_set_id;
    END IF;
END gm_fch_uploaded_data;
/********************************************************************************
* Description : Function to get location details
* Author  : Bgupta
*********************************************************************************/
--=
FUNCTION get_location_details (
        p_location_id t2656_audit_entry.c2656_location_id%TYPE,
        p_location_type t2656_audit_entry.c901_location_type%TYPE)
    RETURN VARCHAR2
IS
    v_location_details VARCHAR2 (200) ;
BEGIN
    BEGIN
        IF p_location_type = 4120 THEN -- distributor office
             SELECT c701_ship_name || ' - ' || c701_ship_add1 || ' ' || c701_ship_add2 || ' ' || c701_ship_city || ', '
                || get_code_name (c701_ship_state) || ' - ' || c701_ship_zip_code
               INTO v_location_details
               FROM t701_distributor
              WHERE c701_distributor_id = p_location_id;
        ELSIF p_location_type           = 4121 -- sales rep
            THEN
             SELECT get_party_name (c101_party_id) || ' - ' || c106_add1 || ' ' || c106_add2 || ', ' || c106_city ||
                ', ' || get_code_name (c901_state) || ' - ' || c106_zip_code
               INTO v_location_details
               FROM t106_address
              WHERE c106_address_id = p_location_id;
        ELSIF p_location_type       = 4122 -- hospital
            THEN
             SELECT get_account_name (p_location_id) INTO v_location_details FROM DUAL;
        END IF;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN '';
    END;
    RETURN v_location_details;
END get_location_details;
FUNCTION get_audit_count (
        p_audit_id IN t2652_field_sales_lock.c2650_field_sales_audit_id%TYPE,
        p_dist_id  IN t2651_field_sales_details.c701_distributor_id%TYPE,
        p_set_id   IN t2657_field_sales_details_lock.c207_set_id%TYPE)
    RETURN NUMBER
IS
    --
    /********************************************************************************************
    * Description :This function returns audit count.
    *********************************************************************************************/
    --
    v_audit_count NUMBER;
    --
BEGIN
     SELECT COUNT (1) audit_count
       INTO v_audit_count
       FROM t2651_field_sales_details t2651, t2656_audit_entry t2656
      WHERE t2651.c701_distributor_id          = p_dist_id
        AND t2651.c2650_field_sales_audit_id   = p_audit_id
        AND t2651.c2651_field_sales_details_id = t2656.c2651_field_sales_details_id
        AND t2656.c207_set_id                  = NVL (p_set_id, t2656.c207_set_id)
        AND t2651.c2651_void_fl               IS NULL
        AND t2656.c2656_void_fl               IS NULL
        AND t2656.c901_entry_type             != 51031;
    RETURN v_audit_count;
    --
EXCEPTION
WHEN NO_DATA_FOUND THEN
    RETURN 0;
    --
END get_audit_count;
FUNCTION get_system_count (
        p_audit_id IN t2652_field_sales_lock.c2650_field_sales_audit_id%TYPE,
        p_dist_id  IN t2651_field_sales_details.c701_distributor_id%TYPE,
        p_set_id   IN t2657_field_sales_details_lock.c207_set_id%TYPE)
    RETURN NUMBER
IS
    --
    /********************************************************************************************
    *Description :This function returns system count.
    *********************************************************************************************/
    --
    v_system_qty NUMBER;
    --
BEGIN
     SELECT SUM (c2657_qty) system_qty
       INTO v_system_qty
       FROM t2652_field_sales_lock t2652, t2657_field_sales_details_lock t2657
      WHERE t2652.c2652_field_sales_lock_id  = t2657.c2652_field_sales_lock_id
        AND t2652.c701_distributor_id        = p_dist_id
        AND t2652.c2650_field_sales_audit_id = p_audit_id
        AND t2657.c207_set_id                = NVL (p_set_id, t2657.c207_set_id)
        AND t2652.c2652_void_fl             IS NULL
        AND t2657.c2657_void_fl             IS NULL;
    RETURN v_system_qty;
    --
EXCEPTION
WHEN NO_DATA_FOUND THEN
    RETURN 0;
    --
END get_system_count;
FUNCTION get_flagged_positive_count (
        p_audit_id IN t2652_field_sales_lock.c2650_field_sales_audit_id%TYPE,
        p_dist_id  IN t2651_field_sales_details.c701_distributor_id%TYPE,
        p_set_id   IN t2657_field_sales_details_lock.c207_set_id%TYPE)
    RETURN NUMBER
IS
    --
    /********************************************************************************************
    * Description :This function flagged positive count
    *********************************************************************************************/
    --
    v_flagged_qty NUMBER;
    --
BEGIN
     SELECT SUM (NVL (transfer_from, 0)) + SUM (NVL (extra, 0)) + SUM (NVL (borrowed_from, 0))
       INTO v_flagged_qty
       FROM
        (
             SELECT SUM (DECODE (t2656.c901_ref_type, 51023, 1, 0)) transfer_from, SUM (DECODE (t2656.c901_ref_type,
                51024, 1, 0)) extra, SUM (DECODE (t2656.c901_ref_type, 51025, 1, 0)) borrowed_from
               FROM t2651_field_sales_details t2651, t2656_audit_entry t2656
              WHERE t2651.c2650_field_sales_audit_id   = p_audit_id
                AND (t2651.c701_distributor_id         = p_dist_id)
                AND t2656.c207_set_id                 IS NOT NULL
                AND t2656.c207_set_id                  = NVL (p_set_id, t2656.c207_set_id)
                AND t2656.c205_part_number_id         IS NOT NULL
                AND t2656.c2656_control_number        IS NOT NULL
                AND t2651.c2651_field_sales_details_id = t2656.c2651_field_sales_details_id
                AND t2656.c2656_void_fl               IS NULL
                AND t2651.c2651_void_fl               IS NULL
          UNION ALL
             SELECT SUM (DECODE (t2656.c901_ref_type, 51027, 1, 0)) transfer_from, SUM (DECODE (t2656.c901_ref_type,
                51024, 1, 0)) extra, SUM (DECODE (t2656.c901_ref_type, 51026, 1, 0)) borrowed_from
               FROM t2651_field_sales_details t2651, t2656_audit_entry t2656
              WHERE t2651.c2650_field_sales_audit_id   = p_audit_id
                AND t2656.c2656_ref_id                 = p_dist_id
                AND t2656.c207_set_id                  = NVL (p_set_id, t2656.c207_set_id)
                AND t2656.c207_set_id                 IS NOT NULL
                AND t2656.c205_part_number_id         IS NOT NULL
                AND t2656.c2656_control_number        IS NOT NULL
                AND t2651.c2651_field_sales_details_id = t2656.c2651_field_sales_details_id
                AND t2656.c2656_void_fl               IS NULL
                AND t2651.c2651_void_fl               IS NULL
                AND t2656.c901_ref_type               IN (51023, 51025, 51027, 51026)
        ) ;
    /*SELECT COUNT (1) flagged_positive_qty
    INTO v_flagged_qty
    FROM t2651_field_sales_details t2651, t2656_audit_entry t2656
    WHERE (t2651.c701_distributor_id = p_dist_id OR t2656.c2656_ref_id = p_dist_id)
    AND t2651.c2650_field_sales_audit_id = p_audit_id
    AND t2651.c2651_field_sales_details_id = t2656.c2651_field_sales_details_id
    AND t2656.c207_set_id = p_set_id
    AND t2656.c901_reconciliation_status != 51035
    AND t2656.c901_ref_type IN ('51023', '51024', '51025')
    AND t2651.c2651_void_fl IS NULL
    AND t2656.c2656_void_fl IS NULL;*/
    RETURN v_flagged_qty;
    --
EXCEPTION
WHEN NO_DATA_FOUND THEN
    RETURN 0;
    --
END get_flagged_positive_count;
--
FUNCTION get_flagged_negative_count (
        p_audit_id IN t2652_field_sales_lock.c2650_field_sales_audit_id%TYPE,
        p_dist_id  IN t2651_field_sales_details.c701_distributor_id%TYPE,
        p_set_id   IN t2657_field_sales_details_lock.c207_set_id%TYPE)
    RETURN NUMBER
IS
    --
    /********************************************************************************************
    * Description :This function flagged positive count
    *********************************************************************************************/
    --
    v_flagged_qty NUMBER;
    --
BEGIN
     SELECT SUM (NVL (returns, 0)) + SUM (NVL (MISSING, 0)) + SUM (NVL (lend_to, 0)) + SUM (NVL (transfer_to, 0))
       INTO v_flagged_qty
       FROM
        (
             SELECT SUM (DECODE (t2656.c901_ref_type, 51029, 1, 0)) returns, SUM (DECODE (t2656.c901_ref_type, 51028, 1
                , 0)) MISSING, SUM (DECODE (t2656.c901_ref_type, 51027, 1, 0)) transfer_to, SUM (DECODE (
                t2656.c901_ref_type, 51026, 1, 0)) lend_to
               FROM t2651_field_sales_details t2651, t2656_audit_entry t2656
              WHERE t2651.c2650_field_sales_audit_id   = p_audit_id
                AND t2651.c701_distributor_id          = p_dist_id
                AND t2656.c207_set_id                 IS NOT NULL
                AND t2656.c207_set_id                  = NVL (p_set_id, t2656.c207_set_id)
                AND t2656.c205_part_number_id         IS NOT NULL
                AND t2656.c2656_control_number        IS NOT NULL
                AND t2651.c2651_field_sales_details_id = t2656.c2651_field_sales_details_id
                AND t2656.c2656_void_fl               IS NULL
                AND t2651.c2651_void_fl               IS NULL
          UNION ALL
             SELECT SUM (DECODE (t2656.c901_ref_type, 51029, 1, 0)) returns, SUM (DECODE (t2656.c901_ref_type, 51028, 1
                , 0)) MISSING, SUM (DECODE (t2656.c901_ref_type, 51023, 1, 0)) transfer_to, SUM (DECODE (
                t2656.c901_ref_type, 51025, 1, 0)) lend_to
               FROM t2651_field_sales_details t2651, t2656_audit_entry t2656
              WHERE t2651.c2650_field_sales_audit_id   = p_audit_id
                AND t2656.c2656_ref_id                 = p_dist_id
                AND t2656.c207_set_id                  = NVL (p_set_id, t2656.c207_set_id)
                AND t2656.c207_set_id                 IS NOT NULL
                AND t2656.c205_part_number_id         IS NOT NULL
                AND t2656.c2656_control_number        IS NOT NULL
                AND t2651.c2651_field_sales_details_id = t2656.c2651_field_sales_details_id
                AND t2656.c2656_void_fl               IS NULL
                AND t2651.c2651_void_fl               IS NULL
                AND t2656.c901_ref_type               IN (51023, 51025, 51027, 51026)
        ) ;
    /* SELECT COUNT (1) flagged_negative_qty
    INTO v_flagged_qty
    FROM t2651_field_sales_details t2651, t2656_audit_entry t2656
    WHERE (t2651.c701_distributor_id = p_dist_id OR t2656.c2656_ref_id = p_dist_id)
    AND t2651.c2650_field_sales_audit_id = p_audit_id
    AND t2651.c2651_field_sales_details_id = t2656.c2651_field_sales_details_id
    AND t2656.c207_set_id = p_set_id
    AND t2656.c901_reconciliation_status != 51035
    AND t2656.c901_ref_type IN ('51026', '51027', '51028', '51029')
    AND t2651.c2651_void_fl IS NULL
    AND t2656.c2656_void_fl IS NULL;*/
    RETURN v_flagged_qty;
    --
EXCEPTION
WHEN NO_DATA_FOUND THEN
    RETURN 0;
    --
END get_flagged_negative_count;
/********************************************************************************
* Description : Procedure to fetch PDF Report
* Author  : pramaraj
*********************************************************************************/
PROCEDURE gm_fetch_confirmation_report (
        p_audit_id IN t2652_field_sales_lock.c2650_field_sales_audit_id%TYPE,
        p_dist_id  IN t2651_field_sales_details.c701_distributor_id%TYPE,
        p_out_header_info OUT TYPES.cursor_type,
        p_out_overall_count_cur OUT TYPES.cursor_type,
        p_out_sets_missing_cur OUT TYPES.cursor_type,
        p_out_sets_extra_cur OUT TYPES.cursor_type,
        p_out_sets_matching_cur OUT TYPES.cursor_type)
AS
    v_unresolved_qty NUMBER;
BEGIN
     SELECT get_dist_unresolved_qty (p_audit_id, p_dist_id, '0')
       INTO v_unresolved_qty
       FROM DUAL;
    IF (v_unresolved_qty != 0) THEN
        raise_application_error ('-20848', '') ;
    END IF;
    OPEN p_out_header_info FOR SELECT get_dist_bill_add (p_dist_id) address,
    get_auditor_name (p_audit_id, p_dist_id) audit_user,
    get_audit_date (p_audit_id, p_dist_id) locked_date,
    TO_CHAR (SYSDATE, GET_RULE_VALUE('DATEFMT', 'DATEFORMAT')) cur_date,
    TO_CHAR (SYSDATE + 7, GET_RULE_VALUE('DATEFMT', 'DATEFORMAT')) return_date,
    get_distributor_name (p_dist_id) dist_name FROM DUAL;
    gm_fetch_overall_count (p_audit_id, p_dist_id, p_out_overall_count_cur) ;
    gm_fch_uploaded_data (p_audit_id, p_dist_id, '0', '0', '0', 'FLFNQ', p_out_sets_missing_cur) ;
    gm_fch_uploaded_data (p_audit_id, p_dist_id, '0', '0', '0', 'FLFPQ', p_out_sets_extra_cur) ;
    gm_fch_uploaded_data (p_audit_id, p_dist_id, '0', '0', '0', 'MATCH', p_out_sets_matching_cur) ;
END gm_fetch_confirmation_report;
/********************************************************************************
* Description : Procedure to fetch overall count PDF Report
* Author   : pramaraj
*********************************************************************************/
PROCEDURE gm_fetch_overall_count (
        p_audit_id IN t2652_field_sales_lock.c2650_field_sales_audit_id%TYPE,
        p_dist_id  IN t2651_field_sales_details.c701_distributor_id%TYPE,
        p_out_overall_counts_cur OUT TYPES.cursor_type)
AS
BEGIN
    OPEN p_out_overall_counts_cur FOR SELECT NVL (SUM (DECODE (t901.c901_code_grp, 'FLFNQ', 1, 0)), 0) MISSING,
    NVL (SUM (DECODE (t901.c901_code_grp, 'FLFPQ', 1, 0)), 0) extra,
    NVL (SUM (DECODE (t901.c901_code_grp, 'FLFNQ', 0, 'FLFPQ', 0, 1)), 0) matching FROM t2656_audit_entry t2656,
    t2651_field_sales_details t2651,
    t901_code_lookup t901 WHERE t2656.c2651_field_sales_details_id = t2651.c2651_field_sales_details_id AND
    t2651.c2650_field_sales_audit_id                               = p_audit_id AND (t2651.c701_distributor_id =
        p_dist_id OR t2656.c2656_ref_id                            = p_dist_id) AND t2651.c2651_void_fl IS NULL AND
    t2656.c2656_void_fl                                           IS NULL AND DECODE (t2656.c2656_ref_id, p_dist_id,
    get_code_name_alt (t2656.c901_ref_type), t2656.c901_ref_type)  = TO_CHAR (t901.c901_code_id(+)) AND (
        t2656.c903_upload_file_list                               IS NOT NULL OR t2656.c901_entry_type = 51031) ;
END gm_fetch_overall_count;
/********************************************************************************
* Description : Function to get dist unresolved qty
* Author   : vprasath
*********************************************************************************/
FUNCTION get_dist_unresolved_qty (
        p_audit_id IN t2652_field_sales_lock.c2650_field_sales_audit_id%TYPE,
        p_dist_id  IN t2651_field_sales_details.c701_distributor_id%TYPE,
        p_set_id   IN t2657_field_sales_details_lock.c207_set_id%TYPE)
    RETURN NUMBER
IS
    v_unresolved_qty NUMBER;
BEGIN
     SELECT ABS (SUM (ABS (deviation_qty)) - ABS (SUM (net_flagged_qty)))
       INTO v_unresolved_qty
       FROM
        (
             SELECT NVL (audited_data.audit_qty, 0) audit_qty, NVL (locked_data.system_qty, 0) system_qty, (NVL (
                audited_data.audit_qty, 0) - NVL (locked_data.system_qty, 0)) deviation_qty, NVL (net_flagged_qty, 0)
                net_flagged_qty
               FROM
                (
                     SELECT t207.c207_set_id, t207.c207_set_nm, t205.c205_part_number_id
                      , t205.c205_part_num_desc, get_code_name_alt (t207.c901_hierarchy) TYPE, get_set_name (
                        t207.c207_set_sales_system_id) SYSTEM
                       FROM t207_set_master t207, t208_set_details t208, t205d_part_attribute t205d
                      , t205_part_number t205
                      WHERE t207.c207_type            = 4070
                        AND t207.c207_void_fl        IS NULL
                        AND t207.c207_set_id          = t208.c207_set_id
                        AND t208.c205_part_number_id  = t205d.c205_part_number_id
                        AND t205d.c901_attribute_type = 92340
                        AND t205.c205_part_number_id  = t205d.c205_part_number_id
                )
                t207, (
                     SELECT t2657.c205_part_number_id, t2657.c207_set_id, SUM (c2657_qty) system_qty
                       FROM t2652_field_sales_lock t2652, t2657_field_sales_details_lock t2657, (
                             SELECT t207.c207_set_id, t205.c205_part_number_id
                               FROM t207_set_master t207, t208_set_details t208, t205d_part_attribute t205d
                              , t205_part_number t205
                              WHERE t207.c207_type            = 4070
                                AND t207.c207_void_fl        IS NULL
                                AND t207.c207_set_id          = t208.c207_set_id
                                AND t208.c208_critical_fl     = 'Y'
                                AND t208.c205_part_number_id  = t205d.c205_part_number_id
                                AND t205d.c901_attribute_type = 92340
                                AND t205.c205_part_number_id  = t205d.c205_part_number_id
                        )
                        critical_part
                      WHERE t2652.c2650_field_sales_audit_id = p_audit_id
                        AND t2652.c701_distributor_id        = NVL (p_dist_id, t2652.c701_distributor_id)
                        AND t2657.c207_set_id                = DECODE (p_set_id, '0', t2657.c207_set_id, p_set_id)
                        AND t2652.c2652_field_sales_lock_id  = t2657.c2652_field_sales_lock_id
                        AND t2657.c207_set_id                = critical_part.c207_set_id
                        AND t2657.c205_part_number_id        = critical_part.c205_part_number_id
                        AND t2652.c2652_void_fl             IS NULL
                        AND t2657.c2657_void_fl             IS NULL
                   GROUP BY t2657.c205_part_number_id, t2657.c207_set_id
                )
                locked_data, (
                     SELECT c205_part_number_id, c207_set_id, (SUM (audit_qty)) audit_qty
                      , ABS (SUM (pos_qty) - SUM (neg_qty)) net_flagged_qty
                        --, ABS (SUM (net_flagged_qty)) net_flagged_qty
                       FROM
                        (
                             SELECT t2656.c205_part_number_id, t2656.c207_set_id, SUM (DECODE (t2656.c901_entry_type,
                                51030, t2656.c2656_qty, 0)) audit_qty, (SUM (DECODE (t2656.c901_ref_type, 51023, 1, 0))
                                                                                                         + SUM (DECODE (t2656.c901_ref_type, 51024, 1, 0)) + SUM (DECODE (t2656.c901_ref_type,
                                51025, 1, 0))) pos_qty, (SUM (DECODE (t2656.c901_ref_type, 51029, 1, 0)) + SUM (DECODE
                                (t2656.c901_ref_type, 51028, 1, 0))                                      + SUM (DECODE
                                (t2656.c901_ref_type, 51027, 1, 0))                                      + SUM (DECODE
                                (t2656.c901_ref_type, 51026, 1, 0))) neg_qty
                               FROM t2651_field_sales_details t2651, t2656_audit_entry t2656
                              WHERE t2651.c2650_field_sales_audit_id = p_audit_id
                                AND (t2651.c701_distributor_id       = NVL (p_dist_id, t2651.c701_distributor_id))
                                AND t2656.c207_set_id               IS NOT NULL
                                AND t2656.c207_set_id                = DECODE (p_set_id, '0', t2656.c207_set_id,
                                p_set_id)
                                AND t2656.c205_part_number_id         IS NOT NULL
                                AND t2656.c2656_control_number        IS NOT NULL
                                AND t2651.c2651_field_sales_details_id = t2656.c2651_field_sales_details_id
                                AND t2656.c2656_void_fl               IS NULL
                                AND t2651.c2651_void_fl               IS NULL
                           GROUP BY t2656.c205_part_number_id, t2656.c207_set_id
                          UNION ALL
                             SELECT t2656.c205_part_number_id, t2656.c207_set_id, 0 audit_qty
                              , (SUM (DECODE (t2656.c901_ref_type, 51027, 1, 0))                         + SUM (DECODE (t2656.c901_ref_type,
                                51024, 1, 0))                                                            + SUM (DECODE (t2656.c901_ref_type,
                                51026, 1, 0))) pos_qty, (SUM (DECODE (t2656.c901_ref_type, 51029, 1, 0)) + SUM (DECODE
                                (t2656.c901_ref_type, 51028, 1, 0))                                      + SUM (DECODE
                                (t2656.c901_ref_type, 51023, 1, 0))                                      + SUM (DECODE
                                (t2656.c901_ref_type, 51025, 1, 0))) neg_qty
                               FROM t2651_field_sales_details t2651, t2656_audit_entry t2656
                              WHERE t2651.c2650_field_sales_audit_id = p_audit_id
                                AND t2656.c2656_ref_id               = NVL (p_dist_id, t2656.c2656_ref_id)
                                AND t2656.c207_set_id                = DECODE (p_set_id, '0', t2656.c207_set_id,
                                p_set_id)
                                AND t2656.c207_set_id                 IS NOT NULL
                                AND t2656.c205_part_number_id         IS NOT NULL
                                AND t2656.c2656_control_number        IS NOT NULL
                                AND t2651.c2651_field_sales_details_id = t2656.c2651_field_sales_details_id
                                AND t2656.c2656_void_fl               IS NULL
                                AND t2651.c2651_void_fl               IS NULL
                                AND t2656.c901_ref_type               IN (51023, 51025, 51027, 51026)
                           GROUP BY t2656.c205_part_number_id, t2656.c207_set_id
                        )
                   GROUP BY c205_part_number_id, c207_set_id
                )
                audited_data
              WHERE t207.c207_set_id         = locked_data.c207_set_id(+)
                AND t207.c205_part_number_id = locked_data.c205_part_number_id(+)
                AND t207.c207_set_id         = audited_data.c207_set_id(+)
                AND t207.c205_part_number_id = audited_data.c205_part_number_id(+)
        ) ;
    RETURN v_unresolved_qty;
END get_dist_unresolved_qty;
/********************************************************************************
* Description : Function to get set unresolved qty
* Author   : vprasath
*********************************************************************************/
FUNCTION get_set_unresolved_qty (
        p_audit_id IN t2652_field_sales_lock.c2650_field_sales_audit_id%TYPE,
        p_dist_id  IN t2651_field_sales_details.c701_distributor_id%TYPE,
        p_set_id   IN t2657_field_sales_details_lock.c207_set_id%TYPE)
    RETURN NUMBER
IS
    v_unresolved_qty NUMBER;
BEGIN
     SELECT ABS (SUM (ABS (deviation_qty)) - ABS (SUM (net_flagged_qty)))
       INTO v_unresolved_qty
       FROM
        (
             SELECT v700.d_id dist_id, v700.dist_name, NVL (audited_data.audit_qty, 0) audit_qty
              , NVL (locked_data.system_qty, 0) system_qty, (NVL (audited_data.audit_qty, 0) - NVL (
                locked_data.system_qty, 0)) deviation_qty, NVL (net_flagged_qty, 0) net_flagged_qty
               FROM
                (
                    SELECT DISTINCT d_id, get_distributor_name (d_id) dist_name
                       FROM v700_territory_mapping_detail
                      WHERE NVL (ter_id, - 9999) = DECODE (0, 0, NVL (ter_id, - 9999), 0)
                )
                v700, (
                     SELECT t2652.c701_distributor_id dist_id, SUM (c2657_qty) system_qty
                       FROM t2652_field_sales_lock t2652, t2657_field_sales_details_lock t2657, (
                             SELECT t207.c207_set_id, t205.c205_part_number_id
                               FROM t207_set_master t207, t208_set_details t208, t205d_part_attribute t205d
                              , t205_part_number t205
                              WHERE t207.c207_type            = 4070
                                AND t207.c207_void_fl        IS NULL
                                AND t207.c207_set_id          = t208.c207_set_id
                                AND t208.c208_critical_fl     = 'Y'
                                AND t208.c205_part_number_id  = t205d.c205_part_number_id
                                AND t205d.c901_attribute_type = 92340
                                AND t205.c205_part_number_id  = t205d.c205_part_number_id
                        )
                        critical_part
                      WHERE t2652.c2650_field_sales_audit_id = p_audit_id
                        AND t2652.c701_distributor_id        = NVL (p_dist_id, t2652.c701_distributor_id)
                        AND t2657.c207_set_id                = DECODE (p_set_id, '0', t2657.c207_set_id, p_set_id)
                        AND t2652.c2652_field_sales_lock_id  = t2657.c2652_field_sales_lock_id
                        AND t2657.c207_set_id                = critical_part.c207_set_id
                        AND t2657.c205_part_number_id        = critical_part.c205_part_number_id
                        AND t2652.c2652_void_fl             IS NULL
                        AND t2657.c2657_void_fl             IS NULL
                   GROUP BY t2652.c701_distributor_id
                )
                locked_data, (
                     SELECT dist_id, (SUM (audit_qty)) audit_qty, ABS (SUM (pos_qty) - SUM (neg_qty)) net_flagged_qty
                        --, ABS (SUM (net_flagged_qty)) net_flagged_qty
                       FROM
                        (
                             SELECT t2651.c701_distributor_id dist_id, SUM (DECODE (t2656.c901_entry_type, 51030,
                                t2656.c2656_qty, 0)) audit_qty, (SUM (DECODE (t2656.c901_ref_type, 51023, 1, 0)) + SUM
                                (DECODE (t2656.c901_ref_type, 51024, 1, 0))                                      + SUM
                                (DECODE (t2656.c901_ref_type, 51025, 1, 0))) pos_qty, (SUM (DECODE (t2656.c901_ref_type
                                , 51029, 1, 0))                    + SUM (DECODE (t2656.c901_ref_type, 51028, 1, 0)) + SUM (DECODE (
                                t2656.c901_ref_type, 51027, 1, 0)) + SUM (DECODE (t2656.c901_ref_type, 51026, 1, 0)))
                                neg_qty
                               FROM t2651_field_sales_details t2651, t2656_audit_entry t2656
                              WHERE t2651.c2650_field_sales_audit_id = p_audit_id
                                AND t2651.c701_distributor_id        = NVL (p_dist_id, t2651.c701_distributor_id)
                                AND t2656.c207_set_id                = DECODE (p_set_id, '0', t2656.c207_set_id,
                                p_set_id)
                                AND t2656.c207_set_id                 IS NOT NULL
                                AND t2656.c205_part_number_id         IS NOT NULL
                                AND t2656.c2656_control_number        IS NOT NULL
                                AND t2651.c2651_field_sales_details_id = t2656.c2651_field_sales_details_id
                                AND t2656.c2656_void_fl               IS NULL
                                AND t2651.c2651_void_fl               IS NULL
                           GROUP BY t2651.c701_distributor_id
                          UNION ALL
                             SELECT t2656.c2656_ref_id dist_id, 0 audit_qty, (SUM (DECODE (t2656.c901_ref_type, 51027,
                                1, 0)) + SUM (DECODE (t2656.c901_ref_type, 51024, 1, 0)) + SUM (DECODE (
                                t2656.c901_ref_type, 51026, 1, 0))) pos_qty, (SUM (DECODE (t2656.c901_ref_type, 51029,
                                1, 0))                             + SUM (DECODE (t2656.c901_ref_type, 51028, 1, 0)) + SUM (DECODE (
                                t2656.c901_ref_type, 51023, 1, 0)) + SUM (DECODE (t2656.c901_ref_type, 51025, 1, 0)))
                                neg_qty
                               FROM t2656_audit_entry t2656, t2651_field_sales_details t2651
                              WHERE t2651.c2650_field_sales_audit_id = p_audit_id
                                AND t2656.c2656_ref_id               = NVL (p_dist_id, t2656.c2656_ref_id)
                                AND t2656.c207_set_id                = DECODE (p_set_id, '0', t2656.c207_set_id,
                                p_set_id)
                                AND t2656.c207_set_id                 IS NOT NULL
                                AND t2656.c205_part_number_id         IS NOT NULL
                                AND t2656.c2656_control_number        IS NOT NULL
                                AND t2651.c2651_field_sales_details_id = t2656.c2651_field_sales_details_id
                                AND t2656.c2656_void_fl               IS NULL
                                AND t2651.c2651_void_fl               IS NULL
                                AND t2656.c901_ref_type               IN (51023, 51025, 51027, 51026)
                           GROUP BY t2656.c2656_ref_id
                        )
                   GROUP BY dist_id
                )
                audited_data
              WHERE v700.d_id = locked_data.dist_id(+)
                AND v700.d_id = audited_data.dist_id(+)
        ) ;
    RETURN v_unresolved_qty;
END get_set_unresolved_qty;
/********************************************************************************
* Description : Procedure to lock the distributor
* Author    : vprasath
*********************************************************************************/
PROCEDURE gm_sav_lock_distributor (
        p_audit_id IN t2651_field_sales_details.c2650_field_sales_audit_id%TYPE,
        p_dist_id  IN t2651_field_sales_details.c701_distributor_id%TYPE,
        p_user_id  IN t2651_field_sales_details.c2651_last_updated_by%TYPE)
AS
    v_qty NUMBER;
BEGIN
     SELECT COUNT (1)
       INTO v_qty
       FROM t2651_field_sales_details t2651
      WHERE t2651.c701_distributor_id         = p_dist_id
        AND t2651.c2650_field_sales_audit_id  = p_audit_id
        AND t2651.c901_reconciliation_status IS NOT NULL
        AND t2651.c901_reconciliation_status >= 51037
        AND t2651.c2651_void_fl              IS NULL; -- approved
    IF v_qty                                 <> 0 THEN
        raise_application_error ('-20842', '') ; --'Distributor is already locked'
    END IF;
     SELECT get_dist_unresolved_qty (p_audit_id, p_dist_id, NULL)
       INTO v_qty
       FROM DUAL;
    IF v_qty <> 0 THEN
        raise_application_error ('-20849', '') ; --'Cant Lock the distributor'
    END IF;
     SELECT COUNT (1)
       INTO v_qty
       FROM t2656_audit_entry t2656, t2651_field_sales_details t2651
      WHERE t2651.c2651_field_sales_details_id = t2656.c2651_field_sales_details_id
        AND t2656.c901_ref_type                = 51028
        AND t2656.c2656_ref_id                IS NULL
        AND t2656.c2656_void_fl               IS NULL
        AND t2651.c2651_void_fl               IS NULL
        AND t2651.c2650_field_sales_audit_id   = p_audit_id
        AND t2651.c701_distributor_id          = p_dist_id;
    IF v_qty                                  <> 0 THEN
        raise_application_error ('-20850', '') ; --'Charge Details not available for some flagged Qty'
    END IF;
     UPDATE t2656_audit_entry t2656
    SET t2656.c901_reconciliation_status       = 51037 -- approved
      , c2656_last_updated_by                  = p_user_id, c2656_last_updated_date = SYSDATE
      WHERE t2656.c2651_field_sales_details_id =
        (
             SELECT t2651.c2651_field_sales_details_id
               FROM t2651_field_sales_details t2651
              WHERE t2651.c701_distributor_id        = p_dist_id
                AND t2651.c2650_field_sales_audit_id = p_audit_id
        )
        AND t2656.c2656_void_fl IS NULL;
     UPDATE t2651_field_sales_details t2651
    SET t2651.c901_reconciliation_status     = 51037 -- approved
      , c2651_last_updated_by                = p_user_id, c2651_last_updated_date = SYSDATE
      WHERE t2651.c701_distributor_id        = p_dist_id
        AND t2651.c2650_field_sales_audit_id = p_audit_id;
END gm_sav_lock_distributor;
/********************************************************************************
* Description : Procedure to check the posting validation
* Author    : pramaraj
*********************************************************************************/
PROCEDURE gm_post_validation (
        p_audit_id IN t2652_field_sales_lock.c2650_field_sales_audit_id%TYPE,
        p_dist_id  IN t2651_field_sales_details.c701_distributor_id%TYPE,
        p_user_id  IN t2656_audit_entry.c2656_last_updated_by%TYPE)
AS
    v_error_count NUMBER;
    v_count       NUMBER;
    v_field_sales_details_id t2651_field_sales_details.c2651_field_sales_details_id%TYPE;
    v_last_trans t5011_tag_log.c5011_last_updated_trans_id%TYPE;
    v_excess_id t504a_consignment_excess.c504_consignment_id%TYPE;
    v_qty t505_item_consignment.c505_item_qty%TYPE;
    v_excess_count NUMBER;
    CURSOR v_return_cur
    IS
         SELECT t2656.c207_set_id set_id, t2656.c901_ref_type ref_type, t2656.c205_part_number_id pnum
          , t2656.c5010_tag_id tagid
           FROM t2656_audit_entry t2656, t5010_tag t5010
          WHERE t2656.c5010_tag_id = t5010.c5010_tag_id
            -- AND t5010.c5010_last_updated_trans_id IS not null
            AND t2656.c2651_field_sales_details_id = v_field_sales_details_id
            AND t2656.c901_ref_type               IS NOT NULL
            AND t2656.c901_ref_type               IN (51027, 51024, 51023)
            ---transfer(to/From), Extra
            AND t2656.c2656_void_fl IS NULL
            AND t5010.c5010_void_fl IS NULL
            -- AND t5010.c901_location_type = '4120'
            AND t2656.c901_posting_option IS NULL;
BEGIN
    ----extra
     SELECT COUNT (1)
       INTO v_count
       FROM t701_distributor t701
      WHERE t701.c701_void_fl          IS NULL
        AND t701.c701_active_fl         = 'N'
        AND t701.c901_distributor_type IN (70100, 70101)
        AND t701.c701_distributor_id    = p_dist_id;
     SELECT t2651.c2651_field_sales_details_id
       INTO v_field_sales_details_id
       FROM t2651_field_sales_details t2651
      WHERE t2651.c2650_field_sales_audit_id = p_audit_id
        AND t2651.c701_distributor_id        = p_dist_id
        AND t2651.c2651_void_fl             IS NULL FOR UPDATE;
     UPDATE t2662_posting_error
    SET c2662_void_fl                    = 'Y', c2662_last_updated_by = p_user_id, c2662_last_updated_date = SYSDATE
      WHERE c2651_field_sales_details_id = v_field_sales_details_id
        AND c2662_void_fl               IS NULL;
    ----any set is already return and same set is marked as extra or Transfer (To/From)
    FOR v_return_index IN v_return_cur
    LOOP
        BEGIN
             SELECT last_updated_trans_id
               INTO v_last_trans
               FROM
                (
                     SELECT t5011.c5011_tag_txn_log_id txn_log_id, t5011.c5011_last_updated_trans_id
                        last_updated_trans_id, t5011.c5010_tag_id tag_id
                       FROM t5011_tag_log t5011, t506_returns t506
                      WHERE t5011.c5011_location_id = '52099'
                        AND t5011.c5010_tag_id      = v_return_index.tagid
                        AND t506.c506_rma_id        = t5011.c5011_last_updated_trans_id
                        AND t506.c506_void_fl      IS NULL
                   ORDER BY t5011.c5011_tag_txn_log_id
                )
              WHERE ROWNUM < 2;
             SELECT COUNT (1)
               INTO v_excess_count
               FROM t504a_consignment_excess t504a, t505_item_consignment t505
              WHERE t504a.c506_rma_id        = v_last_trans
                AND t504a.c504a_status_fl    = 1
                AND t505.c205_part_number_id = v_return_index.pnum
                AND t505.c504_consignment_id = t504a.c504_consignment_id
                AND t504a.c504a_void_fl     IS NULL
                AND t505.c505_void_fl       IS NULL;
            IF (v_excess_count               > 0) THEN
                 SELECT t504a.c504_consignment_id, t505.c505_item_qty
                   INTO v_excess_id, v_qty
                   FROM t504a_consignment_excess t504a, t505_item_consignment t505
                  WHERE t504a.c506_rma_id        = v_last_trans
                    AND t504a.c504a_status_fl    = 1
                    AND t505.c205_part_number_id = v_return_index.pnum
                    AND t505.c504_consignment_id = t504a.c504_consignment_id
                    AND t504a.c504a_void_fl     IS NULL
                    AND t505.c505_void_fl       IS NULL;
            END IF;
             INSERT
               INTO t2662_posting_error
                (
                    c2662_posting_error_id, c2651_field_sales_details_id, c207_set_id
                  , c205_part_number_id, c901_ref_type, c2662_ref_id
                  , c504_excess_id, c2662_ref_qty, c2662_created_by
                  , c2662_created_date, c901_reason
                )
                VALUES
                (
                    s2662_posting_error.NEXTVAL, v_field_sales_details_id, v_return_index.set_id
                  , v_return_index.pnum, v_return_index.ref_type, v_last_trans
                  , v_excess_id, v_qty, p_user_id
                  , SYSDATE, 52185
                ) ;
        EXCEPTION
            -- If no return dont perform any operation
        WHEN NO_DATA_FOUND THEN
            NULL;
        END;
    END LOOP;
    ---transfer validation
    gm_transfer_check (p_audit_id, p_dist_id, p_user_id) ;
    IF (v_count > 0) THEN
        gm_inactive_distributor_check (p_audit_id, p_dist_id, p_user_id) ;
    END IF;
    -- If errors then change the status to pending approval
     SELECT COUNT (1)
       INTO v_error_count
       FROM t2662_posting_error
      WHERE c2651_field_sales_details_id = v_field_sales_details_id
        AND c2662_void_fl               IS NULL;
    --
    IF (v_error_count > 0) THEN
         UPDATE t2651_field_sales_details t2651
        SET t2651.c2651_last_updated_by          = p_user_id, t2651.c2651_last_updated_date = SYSDATE,
            t2651.c901_reconciliation_status     = 51035
          WHERE t2651.c2650_field_sales_audit_id = p_audit_id
            AND t2651.c701_distributor_id        = p_dist_id
            AND t2651.c2651_void_fl             IS NULL;
    END IF;
    --
END gm_post_validation;
/********************************************************************************
* Description : Procedure will check transfers already made
* Author   : pramaraj
*********************************************************************************/
PROCEDURE gm_transfer_check (
        p_audit_id IN t2652_field_sales_lock.c2650_field_sales_audit_id%TYPE,
        p_dist_id  IN t2651_field_sales_details.c701_distributor_id%TYPE,
        p_user_id  IN t2656_audit_entry.c2656_last_updated_by%TYPE)
AS
    v_field_sales_details_id t2651_field_sales_details.c2651_field_sales_details_id%TYPE;
    v_locked_date DATE;
    v_audit_count NUMBER;
    v_ref_type t2656_audit_entry.c901_ref_type%TYPE;
    CURSOR v_transfer_cur
    IS
         SELECT t921.c920_transfer_id transfer_id, t504.c207_set_id set_id, t505.c205_part_number_id pnum
          , t505.c505_item_qty qty, t921.c901_link_type link_type
           FROM t921_transfer_detail t921, t504_consignment t504, t920_transfer t920
          , t505_item_consignment t505
          WHERE t504.c701_distributor_id = p_dist_id
            AND t921.c921_ref_id         = t504.c504_consignment_id
            -- AND t504.c207_set_id ='916.002'
            AND t920.c920_transfer_id     = t921.c920_transfer_id
            AND t920.c920_transfer_date  >= v_locked_date
            AND t920.c920_transfer_date  <= SYSDATE
            AND t921.c901_link_type       = 90360 --Consignment
            AND t505.c504_consignment_id  = t504.c504_consignment_id
            AND t505.c205_part_number_id IN
            (
                 SELECT t205d.c205_part_number_id
                   FROM t205d_part_attribute t205d
                  WHERE t205d.c901_attribute_type = 92340
            )
        AND t920.c920_void_fl IS NULL
        AND t504.c504_void_fl IS NULL
        AND t921.c921_void_fl IS NULL
        AND t505.c505_void_fl IS NULL
  UNION ALL
     SELECT t921.c920_transfer_id transfer_id, t506.c207_set_id set_id, t507.c205_part_number_id pnum
      , t507.c507_item_qty qty, t921.c901_link_type link_type
       FROM t921_transfer_detail t921, t506_returns t506, t920_transfer t920
      , t507_returns_item t507
      WHERE t506.c701_distributor_id = p_dist_id
        AND t921.c921_ref_id         = t506.c506_rma_id
        -- AND t504.c207_set_id ='916.002'
        AND t920.c920_transfer_id     = t921.c920_transfer_id
        AND t920.c920_transfer_date  >= v_locked_date
        AND t920.c920_transfer_date  <= SYSDATE
        AND t921.c901_link_type       = 90361 ----return
        AND t506.c506_rma_id          = t507.c506_rma_id
        AND t507.c205_part_number_id IN
        (
             SELECT t205d.c205_part_number_id
               FROM t205d_part_attribute t205d
              WHERE t205d.c901_attribute_type = 92340
        )
        AND t920.c920_void_fl IS NULL
        AND t506.c506_void_fl IS NULL
        AND t921.c921_void_fl IS NULL
        AND t920.c920_void_fl IS NULL;
BEGIN
     SELECT t2652.c2652_locked_date
       INTO v_locked_date
       FROM t2652_field_sales_lock t2652
      WHERE t2652.c701_distributor_id        = p_dist_id
        AND t2652.c2650_field_sales_audit_id = p_audit_id
        AND c2652_void_fl                   IS NULL;
     SELECT t2651.c2651_field_sales_details_id
       INTO v_field_sales_details_id
       FROM t2651_field_sales_details t2651
      WHERE t2651.c2650_field_sales_audit_id = p_audit_id
        AND t2651.c701_distributor_id        = p_dist_id;
    ---If transfer already done,details will be logged in the posting error table
    FOR v_transfer_index IN v_transfer_cur
    LOOP
         SELECT DECODE (v_transfer_index.link_type, 90360, 51023, 51027)
           INTO v_ref_type
           FROM DUAL;
         SELECT COUNT (1)
           INTO v_audit_count
           FROM t2656_audit_entry t2656, t2651_field_sales_details t2651
          WHERE t2656.c2651_field_sales_details_id = t2651.c2651_field_sales_details_id
            AND t2656.c205_part_number_id          = v_transfer_index.pnum
            AND t2651.c701_distributor_id          = p_dist_id
            AND t2651.c2650_field_sales_audit_id   = p_audit_id
            AND t2656.c901_ref_type                = DECODE (v_transfer_index.link_type, 90360, 51023, 51027)
            AND t2651.c2651_void_fl               IS NULL
            AND t2656.c2656_void_fl               IS NULL
            AND t2656.c901_posting_option         IS NULL;
        IF (v_audit_count                          > 0) THEN
            gm_posting_error (v_field_sales_details_id, v_transfer_index.set_id, v_transfer_index.pnum, v_ref_type,
            v_transfer_index.transfer_id, v_transfer_index.qty, p_user_id, '52183' ---Transfer Done
            ) ;
        END IF;
    END LOOP;
    --- Validation for tranfer to Inactive distributor
     INSERT
       INTO t2662_posting_error
        (
            c2662_posting_error_id, c2651_field_sales_details_id, c207_set_id
          , c205_part_number_id, c901_ref_type, c2662_created_by
          , c2662_created_date, c901_reason
        )
        (
             SELECT s2662_posting_error.NEXTVAL, t2656.c2651_field_sales_details_id, t2656.c207_set_id
              , t2656.c205_part_number_id, t2656.c901_ref_type, p_user_id
              , SYSDATE, 52184 -- inactive distributor
               FROM t2656_audit_entry t2656, t2651_field_sales_details t2651
              WHERE t2656.c2651_field_sales_details_id = t2651.c2650_field_sales_audit_id
                AND t2651.c701_distributor_id          = p_dist_id
                AND t2651.c2650_field_sales_audit_id   = p_audit_id
                AND t2656.c901_ref_type                = 51027
                AND t2656.c2656_ref_id                IN
                (
                     SELECT t701.c701_distributor_id
                       FROM t701_distributor t701
                      WHERE t701.c701_void_fl          IS NULL
                        AND (t701.c901_ext_country_id   IS NULL OR t701.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH
                        AND t701.c701_active_fl         = 'N'
                        AND t701.c901_distributor_type IN (70100, 70101)
                )
                AND t2651.c2651_void_fl       IS NULL
                AND t2656.c2656_void_fl       IS NULL
                AND t2656.c901_posting_option IS NULL
        ) ;
END gm_transfer_check;
/********************************************************************************
* Description : Procedure will check inative distributor
* Author   : pramaraj
*********************************************************************************/
PROCEDURE gm_inactive_distributor_check
    (
        p_audit_id IN t2652_field_sales_lock.c2650_field_sales_audit_id%TYPE,
        p_dist_id  IN t2651_field_sales_details.c701_distributor_id%TYPE,
        p_user_id  IN t2656_audit_entry.c2656_last_updated_by%TYPE
    )
AS
BEGIN
    ----extra
     INSERT
       INTO t2662_posting_error
        (
            c2662_posting_error_id, c2651_field_sales_details_id, c207_set_id
          , c205_part_number_id, c901_ref_type, c2662_created_by
          , c2662_created_date, c901_reason
        )
        (
             SELECT s2662_posting_error.NEXTVAL, t2656.c2651_field_sales_details_id, t2656.c207_set_id
              , t2656.c205_part_number_id, t2656.c901_ref_type, p_user_id
              , SYSDATE, '52184'
               FROM t2656_audit_entry t2656, t2651_field_sales_details t2651
              WHERE t2656.c2651_field_sales_details_id = t2651.c2651_field_sales_details_id
                AND t2651.c701_distributor_id          = p_dist_id
                AND t2651.c2650_field_sales_audit_id   = p_audit_id
                AND t2656.c901_ref_type               IN (51024, 51023)
                AND t2651.c2651_void_fl               IS NULL
                AND t2656.c2656_void_fl               IS NULL
                AND t2656.c901_posting_option         IS NULL
        ) ;
    ----Missing
     INSERT
       INTO t2662_posting_error
        (
            c2662_posting_error_id, c2651_field_sales_details_id, c207_set_id
          , c205_part_number_id, c901_ref_type, c2662_created_by
          , c2662_created_date, c901_reason
        )
        (
             SELECT s2662_posting_error.NEXTVAL, t2656.c2651_field_sales_details_id, t2656.c207_set_id
              , t2656.c205_part_number_id, t2656.c901_ref_type, p_user_id
              , SYSDATE, '52184'
               FROM t2656_audit_entry t2656, t2651_field_sales_details t2651
              WHERE t2656.c2651_field_sales_details_id = t2651.c2651_field_sales_details_id
                AND t2651.c701_distributor_id          = p_dist_id
                AND t2651.c2650_field_sales_audit_id   = p_audit_id
                AND t2656.c901_ref_type               IN (51028)
                AND t2651.c2651_void_fl               IS NULL
                AND t2656.c2656_void_fl               IS NULL
                AND c207_set_id                       IN
                (
                     SELECT t730.c207_set_id
                       FROM t730_virtual_consignment t730
                      WHERE t730.c701_distributor_id = p_dist_id
                )
                AND t2656.c901_posting_option IS NULL
        ) ;
END gm_inactive_distributor_check;
/********************************************************************************
* Description : Procedure will insert the posting Errors
* Author    : pramaraj
*********************************************************************************/
PROCEDURE gm_posting_error
    (
        p_field_sales_details_id IN t2651_field_sales_details.c2651_field_sales_details_id%TYPE,
        p_set_id                 IN t207_set_master.c207_set_id%TYPE,
        p_num                    IN t2656_audit_entry.c205_part_number_id%TYPE,
        p_ref_type               IN t2656_audit_entry.c901_ref_type%TYPE,
        p_transfer_id            IN t921_transfer_detail.c920_transfer_id%TYPE,
        p_transfer_qty           IN t2662_posting_error.c2662_ref_qty%TYPE,
        p_user_id                IN t2656_audit_entry.c2656_last_updated_by%TYPE,
        p_reason                 IN t2662_posting_error.c901_reason%TYPE
    )
AS
BEGIN
     INSERT
       INTO t2662_posting_error
        (
            c2662_posting_error_id, c2651_field_sales_details_id, c207_set_id
          , c205_part_number_id, c901_ref_type, c2662_ref_id
          , c2662_ref_qty, c2662_created_by, c2662_created_date
          , c901_reason
        )
        VALUES
        (
            s2662_posting_error.NEXTVAL, p_field_sales_details_id, p_set_id
          , p_num, p_ref_type, p_transfer_id
          , p_transfer_qty, p_user_id, SYSDATE
          , p_reason
        ) ;
END gm_posting_error;
/********************************************************************************
* Description : Procedure will pull the posting errors
* Author   : pramaraj
*********************************************************************************/
PROCEDURE gm_fech_posting_error
    (
        p_audit_id IN t2652_field_sales_lock.c2650_field_sales_audit_id%TYPE,
        p_dist_id  IN t2651_field_sales_details.c701_distributor_id%TYPE,
        p_posting_error_cur OUT TYPES.cursor_type
    )
AS
BEGIN
    OPEN p_posting_error_cur FOR SELECT get_distributor_name
    (
        t2651.c701_distributor_id
    )
    dist_name,
    t2662.c207_set_id setid,
    get_set_name
    (
        t2662.c207_set_id
    )
    setdesc,
    t2662.c205_part_number_id pnum,
    get_partnum_desc
    (
        c205_part_number_id
    )
    pdesc,
    t2662.c2662_ref_id transferid,
    get_code_name
    (
        t2662.c901_ref_type
    )
    reftype,
    get_code_name
    (
        t2662.c901_reason
    )
    reason FROM t2662_posting_error t2662,
    t2651_field_sales_details t2651 WHERE t2662.c2662_void_fl              IS NULL AND t2662.c2651_field_sales_details_id =
    t2651.c2651_field_sales_details_id AND t2651.c2650_field_sales_audit_id = p_audit_id AND t2651.c701_distributor_id
                                                                            = p_dist_id AND t2651.c2651_void_fl IS NULL
    ;
END gm_fech_posting_error;
/********************************************************************************
* Description : Procedure to post the distributor
* Author    : vprasath
*********************************************************************************/
PROCEDURE gm_sav_post_distributor
    (
        p_audit_id IN t2652_field_sales_lock.c2650_field_sales_audit_id%TYPE,
        p_dist_id  IN t2651_field_sales_details.c701_distributor_id%TYPE,
        p_user_id  IN t2656_audit_entry.c2656_last_updated_by%TYPE,
        p_out_txn_ids OUT VARCHAR2
    )
AS
    v_cnt              NUMBER;
    v_authorized_users VARCHAR2 (1000) ;
    v_txn_ids          VARCHAR2 (4000) ;
    v_from_id          VARCHAR2 (20) ;
    v_to_id            VARCHAR2 (20) ;
    v_txn_id           VARCHAR2 (20) ;
    v_user_exists      NUMBER;
    v_refid_inputstr   VARCHAR2 (4000) ;
    v_message          VARCHAR2 (1000) ;
    v_consign_id t2659_field_sales_ref_lock.c2659_ref_id%TYPE;
    v_type NUMBER := 90300;
    v_id t701_distributor.c701_distributor_id%TYPE;
    v_set_id t207_set_master.c207_set_id%TYPE;
    v_field_sales_details_id t2651_field_sales_details.c2651_field_sales_details_id%TYPE;
    v_error_count NUMBER;
    v_part_number t205_part_number.c205_part_number_id%TYPE;
    v_out_errmsg VARCHAR2 (4000) ;
    CURSOR v_dist_unresolved_cur
    IS
         SELECT t2656.c2656_audit_entry_id ID, t2656.c5010_tag_id tag_id, t2656.c2651_field_sales_details_id
            dist_details_id, t2656.c901_ref_type ref_type, t2656.c2656_ref_id ref_id
          , t2656.c2656_ref_details ref_details, t2656.c207_set_id set_id, t2656.c205_part_number_id pnum
          , t2656.c2656_control_number cnum, t2656.c2656_last_updated_date updated_date,
            t2656.c2659_field_sales_ref_lock_id ref_lock_id
           FROM t2656_audit_entry t2656, t2651_field_sales_details t2651
          WHERE t2651.c2651_field_sales_details_id = t2656.c2651_field_sales_details_id
            AND t2651.c2650_field_sales_audit_id   = p_audit_id
            AND t2651.c701_distributor_id          = p_dist_id
            AND t2656.c901_reconciliation_status   = 51037 -- Approved
            AND t2656.c2656_void_fl               IS NULL
            AND t2651.c2651_void_fl               IS NULL
            AND t2656.c901_ref_type               IS NOT NULL
            AND (t2656.c901_posting_option        <> 52180
            OR TRIM (t2656.c901_posting_option)   IS NULL) ;
    CURSOR v_out_cur_type
    IS
         SELECT t208.c205_part_number_id pnum, NVL (t208.c208_set_qty, 0) setqty, t208.c208_set_details_id mid
          , get_partnum_desc (t208.c205_part_number_id) pdesc, t208.c208_inset_fl insetfl, NVL (get_part_price (
            t208.c205_part_number_id, 'L'), 0) lprice
           FROM t208_set_details t208
          WHERE t208.c208_void_fl IS NULL
            AND t208.c207_set_id   = v_set_id
            AND t208.c208_set_qty <> 0
            AND t208.c208_inset_fl = 'Y'
       ORDER BY t208.c205_part_number_id, t208.c208_critical_fl DESC, t208.c208_inset_fl DESC;
BEGIN
     SELECT t2651.c2651_field_sales_details_id
       INTO v_field_sales_details_id
       FROM t2651_field_sales_details t2651
      WHERE t2651.c2650_field_sales_audit_id = p_audit_id
        AND t2651.c701_distributor_id        = p_dist_id FOR UPDATE;
    -- check already reconciled
     SELECT COUNT (1)
       INTO v_cnt
       FROM t2651_field_sales_details t2651
      WHERE t2651.c2650_field_sales_audit_id = p_audit_id
        AND t2651.c701_distributor_id        = p_dist_id
        AND t2651.c901_reconciliation_status = 51038 -- REconciled
        AND t2651.c2651_void_fl             IS NULL;
    IF v_cnt                                 > 0 THEN
        raise_application_error ('-20841', '') ;
    END IF;
    -- If error display error found
     SELECT COUNT (1)
       INTO v_error_count
       FROM t2662_posting_error
      WHERE c2651_field_sales_details_id = v_field_sales_details_id
        AND c2662_void_fl               IS NULL;
    IF (v_error_count                    > 0) THEN
        raise_application_error ('-20851', '') ;
    END IF;
    -- check if any records are not approved yet
     SELECT COUNT (t2656.c901_reconciliation_status)
       INTO v_cnt
       FROM t2656_audit_entry t2656, t2651_field_sales_details t2651
      WHERE t2651.c2651_field_sales_details_id = t2656.c2651_field_sales_details_id
        AND t2651.c2650_field_sales_audit_id   = p_audit_id
        AND t2651.c701_distributor_id          = p_dist_id
        AND t2656.c901_reconciliation_status   < 51037 -- Approved
        AND t2651.c2651_void_fl               IS NULL
        AND t2656.c2656_void_fl               IS NULL;
    IF v_cnt                                   > 0 THEN
        raise_application_error ('-20855', '') ;
    END IF;
    -- check if any associated records are not approved yet
     SELECT COUNT (t2656.c901_reconciliation_status)
       INTO v_cnt
       FROM t2656_audit_entry t2656, t2651_field_sales_details t2651
      WHERE t2651.c2651_field_sales_details_id = t2656.c2651_field_sales_details_id
        AND t2651.c2650_field_sales_audit_id   = p_audit_id
        AND t2656.c2656_ref_id                 = p_dist_id
        AND t2656.c901_reconciliation_status   < 51037
        AND t2651.c2651_void_fl               IS NULL
        AND t2656.c2656_void_fl               IS NULL; -- Approved
    IF v_cnt                                   > 0 THEN
        raise_application_error ('-20852', '') ;
    END IF;
    -- check if the unresolved qty is equals to zero for this distributor
     SELECT get_dist_unresolved_qty (p_audit_id, p_dist_id, NULL)
       INTO v_cnt
       FROM DUAL;
    IF v_cnt > 0 THEN
        raise_application_error ('-20853', '') ;
    END IF;
    --check the  user is authorized to perform the Posting
     SELECT get_rule_value ('FIELDAUDITRECON', 'ACCESSPERMISSION')
       INTO v_authorized_users
       FROM DUAL;
     SELECT INSTR (v_authorized_users, p_user_id, 1, 1)
       INTO v_user_exists
       FROM DUAL;
    IF v_user_exists = 0 THEN
        raise_application_error ('-20854', '') ;
    END IF;
    -- Loop the unresolved qty records, that has to be reconciled
    FOR v_tag_index IN v_dist_unresolved_cur
    LOOP
        -- Transfer From or Transfer To
        IF (v_tag_index.ref_type    = 51023 OR v_tag_index.ref_type = 51027) THEN
            IF v_tag_index.ref_type = 51023 THEN
                v_from_id          := v_tag_index.ref_id;
                v_to_id            := p_dist_id;
            ELSE
                v_from_id := p_dist_id;
                v_to_id   := v_tag_index.ref_id;
            END IF;
            -- For Initiating the transfer get the part number and qty and then prepare the input string
            v_id             := p_dist_id;
            v_set_id         := v_tag_index.set_id;
            v_refid_inputstr := '';
            FOR v_index      IN v_out_cur_type
            LOOP
                -- IF v_index.qty > 0 AND v_index.setqty > 0
                -- THEN
                IF v_index.setqty     > 0 THEN
                    v_refid_inputstr := v_refid_inputstr || v_index.pnum || '^' || v_index.setqty || '|';
                END IF;
            END LOOP;
            -- Initiate the Transfer
            gm_pkg_cs_transfer.gm_cs_sav_transfer (90300 -- Distributor to Distributor
            , 90351 -- Set Transfer
            , v_from_id, v_to_id, 90321 -- inventory transfer
            , TO_CHAR (SYSDATE, GET_RULE_VALUE('DATEFMT', 'DATEFORMAT')), p_user_id, v_tag_index.set_id, v_refid_inputstr, v_txn_id) ;
            gm_update_log (v_txn_id, 'Transfer made as a part for field audit reconciliation process', 1209, p_user_id,
            v_message) ;
            -- Accept the transfer
            --gm_pkg_cs_transfer.gm_cs_sav_accept_transfer (v_txn_id, p_user_id);
            gm_pkg_cs_transfer.gm_cs_sav_accept_transfer (v_txn_id, p_user_id, 'Y', '', 0, v_out_errmsg) ;
            gm_update_log (v_txn_id, 'Transfer Verified as a part for field audit reconciliation process', 1209,
            p_user_id, v_message) ;
            --
            -- To fetch the consignment ID for the selected transaction should have only one consignment id
             SELECT t921.c921_ref_id
               INTO v_consign_id
               FROM t921_transfer_detail t921
              WHERE t921.c920_transfer_id = v_txn_id
                AND t921.c901_link_type   = 90360;
            -- update the tag table with associated distributor
             UPDATE t5010_tag t5010
            SET t5010.c5010_location_id       = v_to_id, t5010.c5010_last_updated_by = p_user_id,
                t5010.c5010_last_updated_date = SYSDATE, t5010.c5010_last_updated_trans_id = v_consign_id,
                t5010.c901_trans_type         = 51000
              WHERE t5010.c5010_void_fl      IS NULL
                --AND t5010.c5010_last_updated_trans_id IS NULL -- ( Commented by Richard because of error (Added
                -- location id)
                AND t5010.c5010_location_id  = v_from_id
                AND t5010.c5010_tag_id       = v_tag_index.tag_id
                AND t5010.c901_location_type = 4120;
        ELSIF v_tag_index.ref_type           = 51024 THEN
            -- Extra set posting
            gm_pkg_ac_audit_reconciliation.gm_extra_set_posting (p_dist_id, v_tag_index.set_id, p_user_id, v_txn_id) ;
             SELECT COUNT (1)
               INTO v_cnt
               FROM t5010_tag t5010
              WHERE t5010.c5010_tag_id                 = v_tag_index.tag_id
                AND t5010.c5010_last_updated_trans_id IS NULL
                AND t5010.c5010_lock_fl               IS NULL
                AND t5010.c5010_void_fl               IS NULL;
            -- Update the tag with the returned txn id
            IF v_cnt > 0 THEN
                 UPDATE t5010_tag t5010
                SET c207_set_id                       = v_tag_index.set_id, c205_part_number_id = v_tag_index.pnum, c5010_control_number =
                    v_tag_index.cnum, c901_trans_type = 51000 -- Consignments
                  , c5010_last_updated_trans_id       = v_txn_id, c901_location_type = 4120 -- Distributor
                  , c5010_location_id                 = p_dist_id, c5010_lock_fl = 'Y', c901_status = 51012 -- Active
                  , c5010_last_updated_date           = SYSDATE, c5010_last_updated_by = p_user_id
                  WHERE t5010.c5010_tag_id            = v_tag_index.tag_id
                    AND t5010.c5010_void_fl          IS NULL;
            END IF;
        ELSIF v_tag_index.ref_type = 51028 THEN
            -- Missing Set Posting
             SELECT t2659.c2659_ref_id, t2659.c205_part_number_id
               INTO v_consign_id, v_part_number
               FROM t2659_field_sales_ref_lock t2659
              WHERE c2659_field_sales_ref_lock_id = v_tag_index.ref_lock_id
                AND t2659.c2659_void_fl          IS NULL;
            gm_missing_set_posting (p_dist_id, v_consign_id, v_part_number, p_user_id, v_txn_id) ;
            gm_save_charges (v_txn_id, p_dist_id, v_tag_index.ref_details, p_user_id) ;
        END IF;
         UPDATE t2656_audit_entry t2656
        SET t2656.c2656_recon_txn_id       = v_txn_id, t2656.c2656_last_updated_by = p_user_id, t2656.c2656_last_updated_date
                                           = SYSDATE, t2656.c901_reconciliation_status = 51038
          WHERE t2656.c2656_audit_entry_id = v_tag_index.ID
            AND t2656.c2656_void_fl       IS NULL;
        v_txn_ids                         := v_txn_ids || ',' || v_txn_id;
        v_txn_id                          := '';
    END LOOP;
     UPDATE t2651_field_sales_details t2651
    SET t2651.c2651_last_updated_by          = p_user_id, t2651.c2651_last_updated_date = SYSDATE,
        t2651.c901_reconciliation_status     = 51038
      WHERE t2651.c2650_field_sales_audit_id = p_audit_id
        AND t2651.c701_distributor_id        = p_dist_id
        AND t2651.c2651_void_fl             IS NULL;
    p_out_txn_ids                           := v_txn_ids;
END gm_sav_post_distributor;
/********************************************************************************
* Description : Procedure to post the extra set / write up for a distributor
* Author   : vprasath
*********************************************************************************/
PROCEDURE gm_extra_set_posting (
        p_distid  IN t701_distributor.c701_distributor_id%TYPE,
        p_setid   IN t207_set_master.c207_set_id%TYPE,
        p_user_id IN t2656_audit_entry.c2656_last_updated_by%TYPE,
        p_txn_id OUT VARCHAR2)
AS
    v_request_id t520_request.c520_request_id%TYPE;
    v_item_consigment_id t505_item_consignment.c505_item_consignment_id%TYPE;
    v_shipid t907_shipping_info.c907_shipping_id%TYPE;
	v_company_id  t1900_company.c1900_company_id%TYPE;
    v_plant_id 	  t5040_plant_master.c5040_plant_id%TYPE;	    
    CURSOR c_get_setdtl
    IS
         SELECT t208.c207_set_id ID, t208.c205_part_number_id pnum, NVL (t208.c208_set_qty, 0) qty
          , t208.c208_set_details_id mid, get_partnum_desc (t208.c205_part_number_id) pdesc, t208.c208_inset_fl insetfl
          , NVL (get_part_price (t208.c205_part_number_id, 'L'), 0) lprice
           FROM t208_set_details t208
          WHERE t208.c208_void_fl IS NULL
            AND t208.c207_set_id   = p_setid
            AND t208.c208_set_qty <> 0
            AND t208.c208_inset_fl = 'Y'
       ORDER BY t208.c205_part_number_id, t208.c208_critical_fl DESC, t208.c208_inset_fl DESC;
BEGIN
	    SELECT NVL(GET_COMPID_FRM_CNTX(),GET_RULE_VALUE('DEFAULT_COMPANY','ONEPORTAL')), NVL(GET_PLANTID_FRM_CNTX(),GET_RULE_VALUE('DEFAULT_PLANT','ONEPORTAL'))
	      INTO v_company_id, v_plant_id
	      FROM DUAL;
	      
     SELECT 'GM-RQ-' || s520_request.NEXTVAL INTO v_request_id FROM DUAL;
     INSERT
       INTO t520_request
        (
            c520_created_date, c901_request_source, c520_required_date
          , c520_request_to, c520_status_fl, c901_ship_to
          , c520_request_id, c520_created_by, c520_request_for
          , c207_set_id, c520_ship_to_id, c520_request_date
          , c901_request_by_type, c1900_company_id, c5040_plant_id
        )
        VALUES
        (
            CURRENT_DATE, 50618, TRUNC (CURRENT_DATE)
          , p_distid, 40, 4120
          , v_request_id, p_user_id, 40021
          , p_setid, p_distid, TRUNC (CURRENT_DATE)
          , 50626, v_company_id, v_plant_id
        ) ;
     SELECT get_next_consign_id ('consignment') INTO p_txn_id FROM DUAL;
     INSERT
       INTO t504_consignment
        (
            c504_verified_date, c504_delivery_carrier, c520_request_id
          , c504_consignment_id, c504_delivery_mode, c504_status_fl
          , c504_type, c504_ship_to, c504_created_by
          , c701_distributor_id, c504_ship_to_id, c504_update_inv_fl
          , c504_ship_req_fl, c207_set_id, c504_ship_date
          , c504_verify_fl, c504_created_date, c504_comments
          , c1900_company_id, c5040_plant_id
        )
        VALUES
        (
            TRUNC (CURRENT_DATE), 5040, v_request_id
          , p_txn_id, 5031, 4
          , 4110, 4120, p_user_id
          , p_distid, p_distid, '1'
          , '1', p_setid, TRUNC (CURRENT_DATE)
          , '1', CURRENT_DATE, 'Consignment Write-up for ' || get_distributor_name (p_distid)
          , v_company_id, v_plant_id
        ) ;
     SELECT s907_ship_id.NEXTVAL INTO v_shipid FROM DUAL;
     INSERT
       INTO t907_shipping_info
        (
            c907_shipping_id, c907_ref_id, c901_ship_to
          , c907_ship_to_id, c907_ship_to_dt, c901_source
          , c901_delivery_mode, c901_delivery_carrier, c907_status_fl
          , c907_created_by, c907_created_date, c907_release_dt
          , c907_shipped_dt, c907_tracking_number, c1900_company_id
          , c5040_plant_id
        )
        VALUES
        (
            v_shipid, p_txn_id, 4120
          , p_distid, TRUNC (CURRENT_DATE), 50181
          , 5031, 5040, 40
          , p_user_id, CURRENT_DATE, TRUNC (CURRENT_DATE)
          , TRUNC (CURRENT_DATE), 'Not Applicable', v_company_id
          , v_plant_id
        ) ;
    FOR rec IN c_get_setdtl
    LOOP
         SELECT s504_consign_item.NEXTVAL INTO v_item_consigment_id FROM DUAL;
         INSERT
           INTO t505_item_consignment
            (
                c505_item_price, c504_consignment_id, c505_item_consignment_id
              , c505_item_qty, c205_part_number_id, c505_control_number
            )
            VALUES
            (
                rec.lprice, p_txn_id, v_item_consigment_id
              , rec.qty, rec.pnum, 'NOC#'
            ) ;
        --Posting
        gm_save_ledger_posting (4839, CURRENT_DATE, rec.pnum, p_distid, p_txn_id, rec.qty, NVL (get_ac_cogs_value (rec.pnum,
        4904), get_ac_cogs_value (rec.pnum, 4900)), p_user_id) ;
    END LOOP;
END gm_extra_set_posting;
/********************************************************************************
* Description : Procedure to post the missing set / write off / write down
* for a distributor
* Author   : vprasath
*********************************************************************************/
PROCEDURE gm_missing_set_posting
    (
        p_dist_id        IN t2651_field_sales_details.c701_distributor_id%TYPE,
        p_consignment_id IN t504_consignment.c504_consignment_id%TYPE,
        p_partnumber     IN t205_part_number.c205_part_number_id%TYPE,
        p_user_id        IN t2656_audit_entry.c2656_last_updated_by%TYPE,
        p_txn_id OUT VARCHAR2
    )
AS
    v_comments t504_consignment.c504_comments%TYPE;
    v_distributor_id t504_consignment.c701_distributor_id%TYPE;
    v_set_id t504_consignment.c207_set_id%TYPE;
    v_consignment_id t504_consignment.c504_consignment_id%TYPE;
    v_type t506_returns.c506_type%TYPE;
    v_company_id  t1900_company.c1900_company_id%TYPE;
    v_plant_id 	  t5040_plant_master.c5040_plant_id%TYPE;	
    CURSOR v_get_item_consigment_cur
    IS
         SELECT t208.c205_part_number_id part_number_id, NVL (t208.c208_set_qty, 0) item_qty, NVL (get_part_price (
            t208.c205_part_number_id, 'L'), 0) item_price
           FROM t208_set_details t208
          WHERE t208.c208_void_fl IS NULL
            AND t208.c207_set_id   = v_set_id
            AND t208.c208_set_qty <> 0
            AND t208.c208_inset_fl = 'Y'
       ORDER BY t208.c205_part_number_id, t208.c208_critical_fl DESC, t208.c208_inset_fl DESC;

   BEGIN
	   
      SELECT get_compid_frm_cntx(), get_plantid_frm_cntx()
        INTO v_company_id, v_plant_id
        FROM DUAL;
        
     SELECT 'GM-RA-' || s506_return.NEXTVAL INTO p_txn_id FROM DUAL;
     SELECT c701_distributor_id, c207_set_id
       INTO v_distributor_id, v_set_id
       FROM t504_consignment
      WHERE c504_consignment_id = p_consignment_id;
    -- If consignment is not associated with set,
    -- get the set details from set detail
    -- If more then one set will display error -- need to decide on how to handle the same
    IF (v_set_id IS NULL) THEN
        BEGIN
             SELECT c207_set_id
               INTO v_set_id
               FROM t208_set_details t208
              WHERE t208.c205_part_number_id = p_partnumber
                AND t208.c208_critical_fl    = 'Y'
                AND t208.c208_void_fl       IS NULL;
        EXCEPTION
        WHEN TOO_MANY_ROWS THEN
            GM_RAISE_APPLICATION_ERROR('-20999','90',p_consignment_id||'#$#'||p_partnumber);
        END;
    END IF;
     SELECT DECODE ( (TRIM (v_set_id)), NULL, 3302, 3301) INTO v_type FROM DUAL;
     INSERT
       INTO t506_returns
        (
            c506_rma_id, c506_comments, c506_created_date
          , c506_type, c701_distributor_id, c506_reason
          , c506_status_fl, c506_created_by, c506_expected_date
          , c207_set_id, c506_credit_date, c506_return_date
          , c501_reprocess_date, c1900_company_id, c5040_plant_id
        )
        VALUES
        (
            p_txn_id, 'This TXN is created as a part of the Field Sales Audit Reconciliation', CURRENT_DATE
          , v_type, v_distributor_id, 3313
          , '2', p_user_id, TRUNC (CURRENT_DATE)
          , v_set_id, TRUNC (CURRENT_DATE), TRUNC (CURRENT_DATE)
          , TRUNC (CURRENT_DATE), v_company_id, v_plant_id
        ) ;
    FOR recs IN v_get_item_consigment_cur
    LOOP
         INSERT
           INTO t507_returns_item
            (
                c507_returns_item_id, c507_item_qty, c507_item_price
              , c205_part_number_id, c507_control_number, c506_rma_id
              , c507_status_fl
            )
            VALUES
            (
                s507_return_item.NEXTVAL, recs.item_qty, recs.item_price
              , recs.part_number_id, 'NOC#', p_txn_id
              , 'C'
            ) ;
        gm_save_ledger_posting (4841, CURRENT_DATE, recs.part_number_id -- part#
        , v_distributor_id -- DistributorID
        , p_txn_id -- RAID
        , recs.item_qty --QTY
        , get_ac_cogs_value (recs.part_number_id, 4904) --
        , p_user_id -- 30003
        ) ;
    END LOOP;
END gm_missing_set_posting;
/********************************************************************************
* Description : Procedure to post the extra set / write up for a distributor
* Author  : vprasath
*********************************************************************************/
PROCEDURE gm_save_charges
    (
        p_ra_id   IN t506_returns.c506_rma_id%TYPE,
        p_dist_id IN t2651_field_sales_details.c701_distributor_id%TYPE,
        p_amount  IN t2656_audit_entry.c2656_ref_details%TYPE,
        p_user_id IN t2656_audit_entry.c2656_last_updated_by%TYPE
    )
AS
    v_incident_id VARCHAR2
    (
        20
    )
    ;
BEGIN
    gm_pkg_op_loaner.gm_sav_incidents (51028, p_ra_id, 92085 -- 'Field Sales Audit'
    , 'Consignment Write Down Charge', p_user_id, SYSDATE, v_incident_id) ;
    gm_pkg_op_loaner.gm_sav_charge_details (v_incident_id, NULL, p_dist_id, NVL (p_amount, 0), 10, p_user_id) ;
END gm_save_charges;
FUNCTION get_audit_date
    (
        p_audit_id IN t2652_field_sales_lock.c2650_field_sales_audit_id%TYPE,
        p_dist_id  IN t2651_field_sales_details.c701_distributor_id%TYPE
    )
    RETURN VARCHAR2
IS
    audit_date VARCHAR2 (20) ;
BEGIN
    BEGIN
         SELECT TO_CHAR (MIN (t2656.c2656_counted_date), GET_RULE_VALUE('DATEFMT', 'DATEFORMAT'))
           INTO audit_date
           FROM t2656_audit_entry t2656, t2651_field_sales_details t2651
          WHERE (t2651.c701_distributor_id         = p_dist_id)
            AND t2651.c2650_field_sales_audit_id   = p_audit_id
            AND t2651.c2651_field_sales_details_id = t2656.c2651_field_sales_details_id
            AND t2656.c901_entry_type             != 51031;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN '';
    END;
    RETURN audit_date;
END get_audit_date;
FUNCTION get_auditor_name (
        p_audit_id IN t2652_field_sales_lock.c2650_field_sales_audit_id%TYPE,
        p_dist_id  IN t2651_field_sales_details.c701_distributor_id%TYPE)
    RETURN VARCHAR2
IS
    auditor_name VARCHAR2 (200) ;
BEGIN
    BEGIN
         SELECT get_user_name (MIN (t2656.c2656_counted_by))
           INTO auditor_name
           FROM t2656_audit_entry t2656, t2651_field_sales_details t2651
          WHERE (t2651.c701_distributor_id         = p_dist_id)
            AND t2651.c2650_field_sales_audit_id   = p_audit_id
            AND t2651.c2651_field_sales_details_id = t2656.c2651_field_sales_details_id
            AND t2656.c901_entry_type             != 51031;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN '';
    END;
    RETURN auditor_name;
END get_auditor_name;

        /******************************************************************************
        * Description : Procedure to create Excess returns. Called from above
        gm_pkg_ac_audit_reconciliation.gm_ac_sav_inactive_dist_writeoff
        * Author  : Mihir Patel
        *******************************************************************************/
    PROCEDURE gm_ac_sav_excess_returns
        (
            p_distributor_id t701_distributor.c701_distributor_id%TYPE
        )
    AS
        CURSOR cur_excess_type
        IS
             SELECT t506.c506_rma_id, t506.c506_last_updated_by, t506.c701_distributor_id
               FROM t506_returns t506
              WHERE t506.c701_distributor_id = p_distributor_id
                AND t506.c506_type           = 3306
                AND t506.c506_parent_rma_id IS NOT NULL
                AND t506.c506_void_fl       IS NULL;
    BEGIN
        FOR excess_type IN cur_excess_type
        LOOP
            --gm_procedure_log('excess txn', excess_type.c506_rma_id || ' | ' || excess_type.c506_last_updated_by || '
            -- | ' ||excess_type.c701_distributor_id);
            gm_pkg_op_excess_return.gm_op_save_excess_qty (excess_type.c506_rma_id, excess_type.c506_last_updated_by,
            excess_type.c701_distributor_id) ;
        END LOOP;
    END gm_ac_sav_excess_returns;

END gm_pkg_ac_audit_reconciliation;
/
