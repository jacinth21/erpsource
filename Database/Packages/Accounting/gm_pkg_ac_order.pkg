/* Formatted on 2011/02/17 12:50 (Formatter Plus v4.8.0) */

--@"C:\Database\Packages\Accounting\gm_pkg_ac_order.pkg";

CREATE OR REPLACE PACKAGE gm_pkg_ac_order
IS
/*******************************************************
   * Description : Procedure to update vat info

   * Author     : Arockia Prasath
   *******************************************************/
   PROCEDURE gm_sav_vatrate (
      p_invoice_id   IN   t501_order.c503_invoice_id%TYPE,
      p_custpo       IN   t501_order.c501_customer_po%TYPE,
      p_accid        IN   t501_order.c704_account_id%TYPE,
      p_type         IN   VARCHAR2
   );
   
 /*******************************************************
   * Description : Procedure to update vat info by dealer

   * Author     : Dinesh Rajavel
   *******************************************************/
   PROCEDURE gm_sav_vatrate_by_dealer (
      p_invoice_id   IN   t501_order.c503_invoice_id%TYPE,
      p_custpo       IN   t501_order.c501_customer_po%TYPE,
      p_dealerid     IN   t501_order.c101_dealer_id%TYPE
   );

/******************************************************************
* Description : Procedure to fetch the Revenue Tracking details
* Author    : Brinal
****************************************************************/
   PROCEDURE gm_fch_order_attrib_info (
      p_orderid   IN       t501_order.c501_order_id%TYPE,
      p_codegrp	  IN       t901_code_lookup.c901_code_grp%TYPE,
      p_request   OUT      TYPES.cursor_type
   );

/***************************************************************************************
* Description : Procedure to fetch the Revenue Tracking details
* Author    : Sindhu
****************************************************************/
   PROCEDURE gm_fch_order_attrib_code_info (
      p_orderid   IN       t501_order.c501_order_id%TYPE,
      p_codegrp	  IN       t901_code_lookup.c901_code_grp%TYPE,
      p_request   OUT      TYPES.cursor_type
   );
   
/******************************************************************
* Description : Procedure to save the Revenue sampling details
* Author    : Brinal
****************************************************************/
   PROCEDURE gm_sav_rev_sampling (
      p_ordid    IN   t501_order.c501_order_id%TYPE,
      p_userid   IN   t501_order.c501_last_updated_by%TYPE,
      p_str      IN   VARCHAR2,
      p_datasave IN   VARCHAR2 DEFAULT NULL
   );
   /***************************************************************************************
	    * Description : This procedure checks availability of customer po id info.
    	* Author: Velu
   ****************************************************************************************/
   PROCEDURE gm_chk_custpoid (
        p_cust_po_id IN t501_order.c501_customer_po%TYPE,
        p_message OUT VARCHAR2
	);
	
	/******************************************************************
	* Description : Procedure to fetch Invoice id.
	* Author    : velu
****************************************************************/
   PROCEDURE gm_fch_invoiceid (
      p_refid    	IN   VARCHAR2,
      p_source   	IN   VARCHAR2,
      p_acc_curr_id	IN   t704_account.c901_currency%TYPE,
      p_invoiceid   OUT  VARCHAR2
   );

/***************************************************************************************
	    * Description : This procedure checks availability of Order id info.
    	* Author: HReddi
	****************************************************************************************/

	PROCEDURE gm_chk_orderId (
        p_order_id IN t501_order.c501_order_id%TYPE,
        p_message OUT VARCHAR2
	);
	

/*
* Description : Procedure to save the VAT RATE based on the Construct and Non Construct
*     Parts for the Sales Order
* Author  :
*/
PROCEDURE gm_sav_construct_vatrate (
        p_invoice_id IN t501_order.c503_invoice_id%TYPE,
        p_custpo     IN t501_order.c501_customer_po%TYPE,
        p_accid      IN t501_order.c704_account_id%TYPE,
        p_type       IN VARCHAR2) ;
            /***************************************************************************************
	* Description : This procedure used to validate the DO.
	* Author:
	****************************************************************************************/
	PROCEDURE gm_validate_do (
	        p_order_id IN t501_order.c501_order_id%TYPE); 
        
    /***************************************************************************************
	* Description : This procedure used to update the DO document flag.
	* Author:
	****************************************************************************************/
	PROCEDURE gm_sav_do_document_fl (
	        p_order_id    IN t501_order.c501_order_id%TYPE,
	        p_document_fl IN t501_order.c501_do_doc_upload_fl%TYPE,
	        p_user_id     IN t501_order.c501_last_updated_by%TYPE) ;

	/***************************************************************************************
	* Description : This procedure used to fetch the Orders (Grand child, Child and parent orders).
	* Added the code for - PMT-28394 (Rebate management)
	* Author: mmuthusamy
	* Added new parameter p_excl_ord_type for - PMT-51170 DO summary print- Hide discount orders for rep
	****************************************************************************************/	        
	 PROCEDURE gm_fch_all_child_orders (
	 		p_order_id    IN t501_order.c501_order_id%TYPE,
	 		p_excl_ord_type IN t501_order.C901_ORDER_TYPE%TYPE,
	 		p_out_orders OUT CLOB
	 );	        
 
	 /***************************************************************************************
	* Description : This procedure used to update the vat rate for parent order.
	* Author:
	****************************************************************************************/
	 PROCEDURE gm_sav_vatrate_by_parent_order (
	        p_parent_ord_id IN t501_order.c501_order_id%TYPE,
	        p_ret_ord_id IN t501_order.c501_order_id%TYPE
 		);	
 		
 	/******************************************************************
    * Description :  Procedure to fetch the Attributes of an order
    * Author       : Raja
    ****************************************************************/
 PROCEDURE gm_fch_order_attrib_values (
      p_orderid       IN       t501_order.c501_order_id%TYPE,
      p_attribtype	  IN       t501a_order_attribute.c901_attribute_type%TYPE,
      p_request       OUT      TYPES.cursor_type);
END gm_pkg_ac_order;

/
