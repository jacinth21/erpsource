/* Formatted on 2012/22/08 17:40 (Formatter Plus v4.8.0) */
--@"c:\database\packages\accounting\gm_pkg_ac_order_rebate_txn.bdy"
CREATE OR REPLACE PACKAGE BODY gm_pkg_ac_order_rebate_txn
IS

	/****************************************************************
    * Description : to get the company and plant information from context and call main procedure.
    * Author      : Mani
    *****************************************************************/

    PROCEDURE gm_sav_rebate
    AS
    	v_company_id t1900_company.c1900_company_id%TYPE;
    	v_plant_id t5040_plant_master.c5040_plant_id%TYPE;
    	--
    	v_time_zone t901_code_lookup.c901_code_nm%TYPE;
    BEGIN
	    -- to get the company information
     SELECT get_compid_frm_cntx (), get_plantid_frm_cntx ()
     	 , get_comptimezone_frm_cntx()
       INTO v_company_id, v_plant_id
       	, v_time_zone
       FROM dual;
       
       -- to alter the time zone  
       EXECUTE IMMEDIATE 'ALTER SESSION SET TIME_ZONE='''||v_time_zone||''''; 
       
       -- to call the main procedure
       gm_pkg_ac_order_rebate_txn.gm_sav_rebate (v_company_id, v_plant_id);
	    
    END gm_sav_rebate;
    

	--
    /****************************************************************
    * Description : Main procedure - To calculate the Discount order based on Rebate management setup
    * 				1. This procedure used to Get all the Rebate active group and pass to "gm_sav_process_rebate" procedure.
    * Author      : Mani
    *****************************************************************/
	
PROCEDURE gm_sav_rebate (
	p_company_id IN t1900_company.c1900_company_id%TYPE,
	p_plant_id IN t5040_plant_master.c5040_plant_id%TYPE)
AS
    --
    CURSOR rebate_group_cur
    IS
         SELECT c901_grp_type grp_type
           FROM t7200_rebate_master
          WHERE c7200_active_fl  = 'Y'
            AND c1900_company_id = p_company_id
            AND c7200_void_fl   IS NULL
       GROUP BY c901_grp_type
       ORDER BY c901_grp_type;
    --
BEGIN
    
    --
    FOR rebate_grp IN rebate_group_cur
    LOOP
        --
        dbms_output.put_line (' Group type  ' || rebate_grp.grp_type) ;
        --
        gm_pkg_ac_order_rebate_txn.gm_sav_process_rebate (rebate_grp.grp_type, p_company_id) ;
        --
    END LOOP;
    
    -- If same account mapped to GPO and GPB then, two discount parts need to insert/update.
    -- So, rebate process Flag update temporary  - T. once processed all the rebate type we update from T to Y.
    
    -- to update the process flag
     
     UPDATE t502_item_order
    SET c502_rebate_process_fl     = 'Y'
      WHERE c502_rebate_process_fl = 'T'
        AND c502_void_fl          IS NULL
        AND c502_delete_fl        IS NULL;
        
    -- to update the non rebate orders - process flag
    
    gm_pkg_ac_order_rebate_txn.gm_update_non_rebate_orders (p_company_id) ;
    
    
END gm_sav_rebate;

    /****************************************************************
    * Description : This procedure used to Get all the Rebate master details - based on Rebate group.
    * 				1. Loop the Rebate details cursor
    * 				2. Based on group type GPO, GPB and Parent account to call separate procedure to store the account information 
    * 					a. gm_load_gpo_account
    * 					b. gm_load_grp_price_book_account
    * 					c. gm_load_parent_account
    * 
    * 				3. Based on Rebate Part type to store the parts to temp table
    * 					a. gm_sav_temp_parts
    * 
    * 				4. To create the DS order (based on account and stored parts)
    * 					a. gm_sav_discount_order
    * 
    * 				5. To delete the temp table (Accounts and Parts)
    * 					a. gm_delete_temp
    * 
    * Author      : Mani
    *****************************************************************/
	
PROCEDURE gm_sav_process_rebate (
        p_group_type IN t7200_rebate_master.c901_grp_type%TYPE,
        p_company_id IN t1900_company.c1900_company_id%TYPE)
AS
    v_part_fl  VARCHAR2 (1) ;
    v_acc_cnt  NUMBER;
    v_part_cnt NUMBER;
    --
    CURSOR rebate_dtls_cur
    IS
         SELECT c7200_rebate_id rebate_id, c901_grp_type grp_type
          , c7200_ref_id ref_id, c7200_rebate_rate rebate_rate
          , c205_reb_part_number rebate_part
           FROM t7200_rebate_master
          WHERE c7200_active_fl  = 'Y'
            AND c1900_company_id = p_company_id
            AND c901_grp_type    = p_group_type
            AND c7200_rebate_rate > 0
            AND c7200_reb_eff_dt <= TRUNC(CURRENT_DATE)
            AND c7200_reb_from_dt <= TRUNC(CURRENT_DATE)
            AND c7200_reb_to_dt   >= TRUNC(CURRENT_DATE)
            AND c7200_void_fl     IS NULL;
    --
BEGIN
	
    --1. delete the temp table
     DELETE FROM my_temp_acc_part_list;
     DELETE FROM my_temp_part_list;
    --
     DELETE FROM my_temp_key_value;
     
    -- 2. loop the rebate type and calculate the discount
    
    FOR rebate_cal IN rebate_dtls_cur
    LOOP
        --
        dbms_output.put_line ('Rebate Calculation started - ID '|| rebate_cal.rebate_id ||' Ref ID '||
        rebate_cal.ref_id) ;
        
        -- GPO
        IF p_group_type = 107460 THEN
            --
            gm_pkg_ac_order_rebate_txn.gm_load_gpo_account (rebate_cal.ref_id) ;
            -- GPB
        ELSIF p_group_type = 107461 THEN
            --
            gm_pkg_ac_order_rebate_txn.gm_load_grp_price_book_account (rebate_cal.ref_id) ;
            -- Parent Account
        ELSIF p_group_type = 107462 THEN
            --
            gm_pkg_ac_order_rebate_txn.gm_load_parent_account (rebate_cal.ref_id) ;
            --
        END IF; -- end if p_group_type
        
        
         SELECT COUNT (1) INTO v_acc_cnt FROM my_temp_acc_part_list;
         
        dbms_output.put_line (' Account Count ' || v_acc_cnt) ;
        
        --3. Save the parts to temp table
        
        gm_pkg_ac_order_rebate_txn.gm_sav_temp_parts (rebate_cal.rebate_id, v_part_fl) ;
        
        -- get the temp parts count
         SELECT COUNT (1) INTO v_part_cnt FROM my_temp_part_list;
         
        dbms_output.put_line (' Part Count ' || v_part_cnt) ;
        
        --4. save DS Orders
        
        gm_pkg_ac_order_rebate_txn.gm_sav_discount_order (rebate_cal.rebate_rate, rebate_cal.rebate_part, v_part_fl) ;
        
        -- 5 to update rebate process flag - non specific parts
        
        IF v_part_fl = 'Y'
        THEN
        	gm_pkg_ac_order_rebate_txn.gm_update_non_specific_parts_orders (p_company_id);
        END IF;
        
        --6. delete the account/Part temp
        
        gm_delete_temp (p_group_type) ;
        --
        
        dbms_output.put_line (' ') ;
        dbms_output.put_line (' ') ;
        
    END LOOP; -- end loop (rebate_cal)
    --
END gm_sav_process_rebate;


    /****************************************************************
    * Description : This procedure used to save all the account to temp table based on GPO (party).
    * 				1. To get account details from Account Affln table (T704d)
    * 				2. Store accounts to temp table (my_temp_acc_part_list)
    *  
    * Author      : Mani
    *****************************************************************/
	
PROCEDURE gm_load_gpo_account (
        p_party_id IN t7200_rebate_master.c7200_ref_id%TYPE)
AS
BEGIN
    -- Based on the GPO get all the account and save to temp table
    
     INSERT
       INTO my_temp_acc_part_list
        (
            c704_account_id
        )
      SELECT t704.c704_account_id
       FROM t704d_account_affln t704d, t704_account t704
      WHERE t704.c704_account_id = t704d.c704_account_id
      AND t704d.c101_gpo       = p_party_id
      AND t704.c704_active_fl = 'Y'
      AND (t704.c901_ext_country_id IS NULL
      	OR t704.c901_ext_country_id  IN (SELECT country_id FROM v901_country_codes))
      AND t704.c704_void_fl IS NULL	
      AND c704d_void_fl IS NULL;
        
END gm_load_gpo_account;


    /****************************************************************
    * Description : This procedure used to save all the account to temp table based on Group price book (party)
    * 				1. To get account details from  GPO account mapping table (T740)
    * 				2. Save accounts to temp table (my_temp_acc_part_list)
    * 
    * Author      : Mani
    *****************************************************************/
	
PROCEDURE gm_load_grp_price_book_account (
        p_party_id IN t7200_rebate_master.c7200_ref_id%TYPE)
AS
BEGIN
    -- Insert the Group Price Book account to temp table
    
     INSERT
       INTO my_temp_acc_part_list
        (
            c704_account_id
        )
     SELECT t704.c704_account_id
       FROM t740_gpo_account_mapping t740, t704_account t704
      WHERE  t704.c704_account_id = t740.c704_account_id
        AND t740.c101_party_id = p_party_id
      	AND t704.c704_active_fl = 'Y'
      	AND (t704.c901_ext_country_id IS NULL
      		OR  t704.c901_ext_country_id  IN (SELECT country_id FROM v901_country_codes))
      	AND t704.c704_void_fl IS NULL	
        AND t740.c740_void_fl IS NULL;
        
END gm_load_grp_price_book_account;


    /****************************************************************
    * Description : This procedure used to save all the account to temp table based on Parent Account (party)
    * 				1. To get account details from  Account table (T704)
    * 				2. Save accounts to temp table (my_temp_acc_part_list)
    * 
    * Author      : Mani
    *****************************************************************/
	
PROCEDURE gm_load_parent_account (
        p_party_id IN t7200_rebate_master.c7200_ref_id%TYPE)
AS
BEGIN
    -- Insert the Parent account to temp table
     INSERT
       INTO my_temp_acc_part_list
        (
            c704_account_id
        )
     SELECT t704.c704_account_id
       FROM t704_account t704
      WHERE t704.c101_party_id = p_party_id
      	AND t704.c704_active_fl = 'Y'
      	AND (t704.c901_ext_country_id IS NULL
      		OR  t704.c901_ext_country_id  IN (SELECT country_id FROM v901_country_codes))
        AND t704.c704_void_fl IS NULL;
        
END gm_load_parent_account;


    /****************************************************************
    * Description : This procedure used to delete the data from temp table
    * 				1. First store the Account information to another temp table (it used to update the rebate flag in T502 table)
    * 				2. Delete the temp table
    * 					a. my_temp_acc_part_list
    * 					b. my_temp_part_list
    * 
    * Author      : Mani
    *****************************************************************/
	
PROCEDURE gm_delete_temp (
        p_group_type IN t7200_rebate_master.c901_grp_type%TYPE)
AS
BEGIN
    -- to insert the current account information to temp table
    
     INSERT
       INTO my_temp_key_value
        (
            my_temp_txn_key, my_temp_txn_value
        )
     SELECT p_group_type, c704_account_id FROM my_temp_acc_part_list;
     
     
    -- delete the data
     DELETE FROM my_temp_acc_part_list;
     DELETE FROM my_temp_part_list;
    --
END gm_delete_temp;


    /****************************************************************
    * Description : This procedure used to store the parts to temp table and returns part flag Y/N - 
    * 				based on the flag to calculate the DS orders.
    * 
    * 				1. Based on Rebate id to get the  part type
    * 				2. Set the part Flag = N
    * 				3. If part type - Specific parts
    * 					a. based on Rebate id get the parts from T7201 table and save to temp table (my_temp_part_list)
    * 					b. set the part flag = Y
    * 				4. Send the part flag
    * 				 
    * Author      : Mani
    *****************************************************************/
	
PROCEDURE gm_sav_temp_parts (
        p_rebate_id IN t7200_rebate_master.c7200_rebate_id%TYPE,
        p_out_part_fl OUT VARCHAR2)
AS
    v_rebate_part_type t7200_rebate_master.c901_rebate_part_type%TYPE;
BEGIN
	
    -- get the part type
    
     SELECT c901_rebate_part_type
       INTO v_rebate_part_type
       FROM t7200_rebate_master
      WHERE C7200_rebate_id = p_rebate_id
        AND c7200_void_fl  IS NULL;
        
    -- set default part flag
    p_out_part_fl := 'N';
    
    
    -- Rebate in Part list
    IF v_rebate_part_type = 107481 THEN
        --
         INSERT INTO my_temp_part_list
            (c205_part_number_id
            )
         SELECT c205_part_number_id
           FROM t7201_rebate_parts
          WHERE c7200_rebate_id     = p_rebate_id
            AND c7201_effective_dt <= TRUNC(CURRENT_DATE)
            AND c7201_void_fl      IS NULL;
        --
        p_out_part_fl := 'Y';
    END IF; -- END v_rebate_part_type
END gm_sav_temp_parts;


    /****************************************************************
    * Description : This procedure used to update the Rebate flag/Rate.
    * 				1. Based on Order id to update process flag and rebate rate.
    * 
    * Author      : Mani
    *****************************************************************/
	
PROCEDURE gm_update_rebate_fl (
        p_order_id    IN t501_order.c501_order_id%TYPE,
        p_rebate_rate IN t7200_rebate_master.c7200_rebate_rate%TYPE,
        p_rebate_flag IN VARCHAR2)
AS
v_user_id      VARCHAR2 (10) := '30301';
BEGIN
    -- to update the rebate rate information
     UPDATE t502_item_order
    SET c7200_rebate_rate   = p_rebate_rate, c502_rebate_process_fl = p_rebate_flag
      WHERE c501_order_id   = p_order_id
        AND c502_void_fl   IS NULL
        AND c502_delete_fl IS NULL;
    
    -- to update the order information (last updated by/date)
    
        gm_pkg_ac_order_rebate_txn.gm_upd_ord_last_updated_info (p_order_id, v_user_id);
        
END gm_update_rebate_fl;


    /****************************************************************
    * Description : This procedure used to update non rebate accounts flag/rate.
    * 				So that next time job run, those records we can avoid.
    * 				1. To get all the non processed rebate orders based on below condition
    * 					a. based on rebate process flag
    * 					b. account id not in temp table (my_temp_key_value)
    * 					c. company id (selected company)
    * 				2. Loop all the orders
    * 					a. call the process flag update procedure - gm_update_rebate_fl
    * 						i. to pass process flag = N
    * 						ii. to pass rebate rate = 0
    * 
    * Author      : Mani
    *****************************************************************/
	
PROCEDURE gm_update_non_rebate_orders (
        p_company_id IN t1900_company.c1900_company_id%TYPE)
AS
    CURSOR non_rebate_orders_cur
    IS
         SELECT t501.c501_order_id ord_id
           FROM t501_order t501, t502_item_order t502
          WHERE t501.c501_order_id           = t502.c501_order_id
            AND t501.c1900_company_id        = p_company_id
            -- Only fetch current month orders
            AND t501.c501_order_date >= trunc(last_day(CURRENT_DATE)-1, 'mm') 
            AND t502.c502_rebate_process_fl IS NULL
            AND t501.c501_void_fl           IS NULL
            AND t502.c502_void_fl IS NULL
            AND t502.c502_delete_fl IS NULL
            AND NOT EXISTS
            (
                 SELECT tmp.my_temp_txn_value
                   FROM my_temp_key_value tmp
                  WHERE tmp.my_temp_txn_value = t501.c704_account_id
            )
   GROUP BY t501.c501_order_id;
   
BEGIN
    -- loop the orders and update the flag
    FOR non_rebate_orders IN non_rebate_orders_cur
    LOOP
        --
        gm_pkg_ac_order_rebate_txn.gm_update_rebate_fl (non_rebate_orders.ord_id, NULL, 'Y') ;
        --
    END LOOP;
END gm_update_non_rebate_orders;


    /***************************************************************************
     * Description : This Procedure will calculate the rebate for the Sales Orders and create a new Sales Discount order
    *  				or Update the existing Discount Order value.
    *
    * Author :
    ****************************************************************************/
	

PROCEDURE gm_sav_discount_order (
        p_rebate_rate IN t7200_rebate_master.c7200_rebate_rate%TYPE,
        p_rebate_part IN t7200_rebate_master.c205_reb_part_number%TYPE,
        p_part_fl     IN VARCHAR2)
AS

    /* Algorithm:
    * 1. Get all the Sales Orders for which the rebate is not processed.
    * 2. Certain Orders will be excluded in the fetch as they does not affect sales.
    * 3. The Account ID's and the Part Numbers will be available in the temp table, this will be set prior to calling this procedure.
    * 4. For the Order total the rebate will be applied and the value will be rounded to two decimal
    * 5. If the end result of the above calculation is 0, we do not have to proceed further.
    * 6. For the Current Order, get the Discount Order, if it does not exist new DS order will be created.
    * 7. New line item in T502 table will be created for the passed in Rebate Part Number.
    * 8. Update the Actual Sales Order rebate process fl,so this order will not be picked up again.
    *  (The flag C502_REBATE_PROCESS_FL will be reset when the Order is edited for Quantity/Price)
    *
    */

    v_order_total NUMBER (10, 2) ;
    v_reb_calc    NUMBER (10, 2) ;
    v_ds_order_id t501_order.c501_order_id%TYPE;
    v_order_id t501_order.c501_order_id%TYPE;
    v_DS_order_cnt NUMBER;
    v_user_id      VARCHAR2 (10) := '30301';
    
    CURSOR order_list_cur
    IS
         SELECT t501.c501_order_id order_id, NVL (SUM (NVL (t502.c502_item_price, 0) * NVL (t502.c502_item_qty, 0)), 0)
            order_total
           FROM t501_order t501, t502_item_order t502, my_temp_acc_part_list acc_temp
          WHERE t501.c501_order_id                      = t502.c501_order_id
          -- Only fetch current month orders
            AND t501.c501_order_date >= trunc(last_day(CURRENT_DATE)-1, 'mm')
            AND NVL (t501.c901_order_type, - 9999) NOT IN
            (
                 SELECT c906_rule_value FROM v901_order_type_grp
            )
        -- Sales Disc order type should not be fetch as this is what the proc will do.
        AND NVL (t501.c901_order_type,          - 9999) != '2535'
        AND t501.c704_account_id          = acc_temp.c704_account_id
        AND t502.c205_part_number_id           IN
        (
             SELECT t502_t.c205_part_number_id
               FROM t502_item_order t502_t
              WHERE t502_t.c501_order_id              = t501.c501_order_id
                AND (EXISTS 
                (
                     SELECT part_temp.c205_part_number_id
                       FROM my_temp_part_list part_temp
                      WHERE part_temp.c205_part_number_id = t502_t.c205_part_number_id
                      AND part_temp.c205_part_number_id IS NOT NULL
                )
                OR DECODE (p_part_fl, 'N', 1, 0) = DECODE (p_part_fl, 'N', 1, - 1))
        )
        AND t501.C501_VOID_FL            IS NULL
        AND t501.C501_DELETE_FL          IS NULL
        AND t502.C502_VOID_FL            IS NULL
        AND t502.c502_delete_fl 		 IS NULL
        AND (t502.c502_rebate_process_fl IS NULL
        OR t502.c502_rebate_process_fl         = 'T')
   GROUP BY t501.c501_order_id;
BEGIN
    FOR order_list IN order_list_cur
    LOOP
    	-- get the orders total
    	
        v_order_total := order_list.order_total;
        
        -- to calculate the rebate amount
        
        v_reb_calc := ROUND (v_order_total * NVL (p_rebate_rate, 0) / 100, 2) ;
        
        dbms_output.put_line (' OID: '||order_list.order_id||' :v_reb_calc '||v_reb_calc||' :v_order_total '||
        v_order_total || ' : Part Fl '|| p_part_fl ||' : Rebate Rate '|| p_rebate_rate) ;
        
       -- rebate amount not equal to 0 then create/update the DS order
       
        IF (v_reb_calc != 0) THEN
            v_order_id := order_list.order_id;
            gm_pkg_ac_order_rebate_txn.gm_create_discount_order (v_order_id, p_rebate_part, v_reb_calc, p_rebate_rate,
            v_user_id) ;
        END IF;
        
        -- Need to call update rebate fl.
        gm_pkg_ac_order_rebate_txn.gm_update_rebate_fl (v_order_id, p_rebate_rate, 'T') ;
    END LOOP;
END gm_sav_discount_order;


    /***************************************************************************
     * Description :  This Procedure will create/update the Discount Order 
    *  Author : 
    ****************************************************************************/
	
PROCEDURE gm_create_discount_order (
        p_order_id    IN t501_order.c501_order_id%TYPE,
        p_rebate_part IN t7200_rebate_master.c205_reb_part_number%TYPE,
        p_rebate_amt  IN t502_item_order.c502_item_price%TYPE,
        p_rebate_rate IN t502_item_order.c7200_rebate_rate%TYPE,
        p_user_id     IN t501_order.c501_created_by%TYPE
        )
AS
    v_ds_order_id t501_order.c501_order_id%TYPE;
    v_ord_id NUMBER;
    v_sign   NUMBER := - 1;
	-- PC-5352: get order amount from T502
	v_total_cost NUMBER(15,2) := 0;
BEGIN
    -- to get the DS orders
    BEGIN
         SELECT c501_order_id
           INTO v_ds_order_id
           FROM t501_order
          WHERE c501_parent_order_id          = p_order_id
          	AND NVL (c901_order_type, - 9999) = '2535'
            AND c501_void_fl                 IS NULL
            AND c501_delete_fl               IS NULL;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_ds_order_id := NULL;
    END;
    
       
        
    IF (v_ds_order_id IS NULL) THEN
    	--
         SELECT s501_order.NEXTVAL INTO v_ord_id FROM DUAL;
         --
        v_ds_order_id := 'GM' || v_ord_id || 'DS';
        
        -- based on orders to create the DS orders
        
         INSERT
           INTO t501_order
            (
                c501_order_id, c501_comments, c704_account_id
              , c501_receive_mode, c501_received_from, c703_sales_rep_id
              , c501_ship_to, c501_ship_to_id, c501_order_date
              , c501_created_by, c501_created_date, c501_status_fl
              , c501_order_date_time, c901_order_type, c501_delivery_carrier
              , c501_delivery_mode, c501_ship_cost, c501_parent_order_id
              , c1900_company_id, c5040_plant_id, c1910_division_id
              , c101_dealer_id, c501_surgery_date, c901_contract
              , c501_total_cost, c501_hold_order_process_fl, c501_hold_order_process_date
            )
         SELECT v_ds_order_id, 'Discount Order for DO - ' || p_order_id, c704_account_id
          , c501_receive_mode, c501_received_from, c703_sales_rep_id
          , c501_ship_to, c501_ship_to_id, TRUNC (CURRENT_DATE)
          , c501_created_by, CURRENT_DATE, '3'
          , CURRENT_DATE, 2535, c501_delivery_carrier
          , c501_delivery_mode, 0, p_order_id
          , c1900_company_id, c5040_plant_id, c1910_division_id
          , c101_dealer_id, c501_surgery_date, c901_contract
          , (p_rebate_amt * v_sign), 'N', CURRENT_DATE
           FROM t501_order
          WHERE C501_ORDER_ID = p_order_id;
    END IF;
    
    
    -- to update the item orders details
    
     UPDATE t502_item_order
    SET c502_item_price         = p_rebate_amt
      WHERE c501_order_id       = v_ds_order_id
        AND c205_part_number_id = p_rebate_part
        AND c502_void_fl       IS NULL
        AND c502_delete_fl     IS NULL;
        
    IF (SQL%ROWCOUNT            = 0) THEN
         INSERT
           INTO t502_item_order
            (
                c502_item_order_id, c501_order_id, c205_part_number_id
              , c502_item_qty, c502_control_number, c502_item_price
              , c901_type, c502_unit_price, c502_unit_price_adj_value
              , c502_do_unit_price, c502_adj_code_value
            )
            VALUES
            (
                s502_item_order.NEXTVAL, v_ds_order_id, p_rebate_part
              , - 1, 'NOC#', p_rebate_amt
              , 50300, p_rebate_amt, 0
              , p_rebate_amt, 0
            ) ;
            
							
    END IF;
    
	--PC-5352: Discount order amount update to T501 (Total cost)
	-- to get the amount from T502
	 BEGIN 
	 SELECT SUM (NVL (t502.c502_item_qty, 0) * NVL (t502.c502_item_price, 0))
        INTO v_total_cost
        FROM t502_item_order t502
       WHERE t502.c501_order_id = v_ds_order_id
         AND t502.c502_void_fl IS NULL
         AND t502.c502_delete_fl IS NULL;
      EXCEPTION
            WHEN NO_DATA_FOUND THEN
                v_total_cost := 0;
      END;
	
	    -- to update the Order amount
     UPDATE t501_order
    SET c501_total_cost              = v_total_cost, c501_hold_order_process_fl = 'N',
        c501_hold_order_process_date = CURRENT_DATE, c501_last_updated_by = p_user_id, c501_last_updated_date =
        CURRENT_DATE
      WHERE c501_order_id   = v_ds_order_id
        AND c501_void_fl   IS NULL
        AND c501_delete_fl IS NULL;
		
    -- Need to update rebate fl (DS orders).
    
    gm_pkg_ac_order_rebate_txn.gm_update_rebate_fl (v_ds_order_id, p_rebate_rate, 'Y') ;
    
END gm_create_discount_order;


	/***************************************************************************
     * Description :  This Procedure used to void the DS orders
     * 				1. to check any DS order available - based on passed orders.
     * 				2. If DS order available then
     * 					a. to void the orders.
     * 
    *  Author : 
    ****************************************************************************/
	
PROCEDURE gm_void_discount_order
    (
        p_parent_order_id IN t501_order.c501_order_id%TYPE,
        p_user_id         IN t501_order.c501_created_by%TYPE
    )
AS
    v_ds_count NUMBER;
BEGIN
	
    -- Check if the In Coming Parent has any DS order as it's child order, if yes, void the Child Order.
    
     SELECT COUNT (1)
       INTO v_ds_count
       FROM t501_order
      WHERE c501_parent_order_id          = p_parent_order_id
        AND NVL (c901_order_type, - 9999) = '2535'
        AND c501_void_fl                 IS NULL
        AND c501_delete_fl 				 IS NULL;
       
     --
        
    IF (v_ds_count                        > 0) THEN
    
         UPDATE t501_order
        SET c501_void_fl                      = 'Y', c501_delete_fl = 'Y'
        , c501_last_updated_by = p_user_id, c501_last_updated_date = CURRENT_DATE
          WHERE c501_parent_order_id          = p_parent_order_id
          	AND NVL (c901_order_type, - 9999) = '2535'
            AND c501_void_fl                 IS NULL
            AND c501_delete_fl 				 IS NULL;
            
    END IF;
    
END gm_void_discount_order;

	/***************************************************************************
	* Description :  This Procedure used to update last updated by/date
	*
	*  Author :
	****************************************************************************/
PROCEDURE gm_upd_ord_last_updated_info (
        p_order_id IN t501_order.c501_order_id%TYPE,
        p_user_id  IN t501_order.c501_last_updated_by%TYPE)
AS
BEGIN
    -- to update the last update information
    
     UPDATE t501_order
    SET c501_last_updated_by = p_user_id, c501_last_updated_date = CURRENT_DATE
      WHERE c501_order_id    = p_order_id;
      
END gm_upd_ord_last_updated_info;

    /****************************************************************
    * Description : This procedure used to update specific parts order - rebate flag/rate.
    * 				So that next time job run, those records we can avoid.
    * 				1. To get all the non processed rebate orders based on below condition
    * 					a. based on rebate process flag
    * 					b. account id in temp table (my_temp_key_value)
    * 					c. part number not in temp table 
    * 					d. company id (selected company)
    * 				2. Loop all the orders
    * 					a. call the process flag update procedure - gm_update_rebate_fl
    * 						i. to pass process flag = N
    * 						ii. to pass rebate rate = 0
    * 
    * Author      : Mani
    *****************************************************************/
	
PROCEDURE gm_update_non_specific_parts_orders (
        p_company_id IN t1900_company.c1900_company_id%TYPE)
AS
    CURSOR non_specific_parts_orders_cur
    IS
         SELECT t501.c501_order_id ord_id
           FROM t501_order t501, t502_item_order t502
          WHERE t501.c501_order_id           = t502.c501_order_id
            AND t501.c1900_company_id        = p_company_id
            -- Only fetch current month orders
            AND t501.c501_order_date >= trunc(last_day(CURRENT_DATE)-1, 'mm')
            AND t502.c502_rebate_process_fl IS NULL
            AND t501.c501_void_fl           IS NULL
            AND t502.c502_void_fl IS NULL
            AND t502.c502_delete_fl IS NULL
            AND EXISTS
            (
                 SELECT tmp.my_temp_txn_value
                   FROM my_temp_key_value tmp
                  WHERE tmp.my_temp_txn_value = t501.c704_account_id
            )
   GROUP BY t501.c501_order_id;
   
BEGIN
    -- loop the orders and update the flag
    FOR non_specific_parts IN non_specific_parts_orders_cur
    LOOP
        --
        gm_pkg_ac_order_rebate_txn.gm_update_rebate_fl (non_specific_parts.ord_id, NULL, 'T') ;
        --
    END LOOP;
END gm_update_non_specific_parts_orders;


END gm_pkg_ac_order_rebate_txn;
/
