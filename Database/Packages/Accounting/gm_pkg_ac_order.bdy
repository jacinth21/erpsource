* Formatted on 2011/02/21 15:41 (Formatter Plus v4.8.0) */

--@"C:\Database\Packages\Accounting\gm_pkg_ac_order.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_ac_order
IS
/*******************************************************
   * Description : Procedure to update vat info

   * Author     : Arockia Prasath
   *******************************************************/
   PROCEDURE gm_sav_vatrate (
      p_invoice_id   IN   t501_order.c503_invoice_id%TYPE,
      p_custpo       IN   t501_order.c501_customer_po%TYPE,
      p_accid        IN   t501_order.c704_account_id%TYPE,
      p_type         IN   VARCHAR2
   )
   AS
 v_acc_vat_rate  NUMBER;
 v_company_id        t1900_company.c1900_company_id%TYPE;
 v_company_vat_rate NUMBER;
 v_acc_vat_type	    T704A_ACCOUNT_ATTRIBUTE.C704A_ATTRIBUTE_VALUE%TYPE;
 v_vat_per          T502_ITEM_ORDER.C502_VAT_RATE%TYPE;
 v_part_family      T205_PART_NUMBER.C205_PRODUCT_FAMILY%TYPE;
 v_country			T704_ACCOUNT.C704_BILL_COUNTRY%TYPE;
 vat_cntry_fl		CHAR(1);

CURSOR cur_inv_det
    IS
	
	SELECT count(1),t502.c205_part_number_id part_num
                  FROM t501_order t501,t502_item_order t502
                 WHERE t501.c501_order_id = t502.c501_order_id
                   AND c501_customer_po = p_custpo
                   AND c704_account_id = p_accid
                   AND c503_invoice_id = p_invoice_id
                   AND c501_void_fl IS NULL
                   AND c502_void_fl IS NULL
                   GROUP BY t502.c205_part_number_id;

 BEGIN
	 
	 	BEGIN
			 --To Get company id from account id
			 SELECT C1900_COMPANY_ID INTO v_company_id  
		      FROM T704_ACCOUNT 
		      WHERE C704_ACCOUNT_ID= p_accid;
		    
		    -- If account id is blank then take company id from context
		 	EXCEPTION
    			WHEN NO_DATA_FOUND
      				THEN
        				SELECT get_compid_frm_cntx() 
					      INTO v_company_id  
					      FROM dual;
  		END;

    -- Get the save value of Tax Type [VAT/CST/GST] from Account Setup screen, 101170: TaxType

	SELECT TRIM(GET_ACCOUNT_ATTRB_VALUE(p_accid,'101170')) 
	INTO v_acc_vat_type 
	FROM DUAL;

	
    IF v_acc_vat_type = '102342' --Tax type : GST--102342
	THEN


	--26240115 : IGST %
	--26240116 : SGST %
	--26240117 : CGST %
 UPDATE t502_item_order
 SET  c502_igst_rate = DECODE(v_acc_vat_type, '102342',TRIM(GET_ACCOUNT_ATTRB_VALUE(p_accid,'26240115')),'')
 	, c502_cgst_rate = DECODE(v_acc_vat_type, '102342',TRIM(GET_ACCOUNT_ATTRB_VALUE(p_accid,'26240117')),'')
 	, c502_sgst_rate = DECODE(v_acc_vat_type, '102342',TRIM(GET_ACCOUNT_ATTRB_VALUE(p_accid,'26240116')),'') 
 	, c502_tcs_rate =  DECODE(v_acc_vat_type, '102342',TRIM(GET_ACCOUNT_ATTRB_VALUE(p_accid,'112020')),'') 	
 
 WHERE c501_order_id IN (
                SELECT t501.c501_order_id
                  FROM t501_order t501
                 WHERE c501_customer_po = p_custpo
                   AND c704_account_id = p_accid
                   AND c503_invoice_id = p_invoice_id
                   AND c501_void_fl IS NULL);
		   	    
	
ELSE	
    --
	SELECT 	 TRIM(GET_ACCOUNT_ATTRB_VALUE(p_accid,'101188')) INTO v_acc_vat_rate FROM DUAL;
	--
	v_company_vat_rate := get_rule_value_by_company (v_company_id,'VAT_RATE', v_company_id);
	--
	--PC-4047 Invoice Luxembourg customers in GM Belgium
	BEGIN
          SELECT c704_bill_country
          INTO v_country 
          FROM t704_account WHERE  C1900_COMPANY_ID = v_company_id and c704_account_id = p_accid;
          EXCEPTION
          WHEN OTHERS
          THEN
            v_country := NULL;
          END;
          
          	vat_cntry_fl := NVL(get_rule_value_by_company (v_country,'VAT_RATE_BY_CNTRY_FL',v_company_id),'N'); 
          
	FOR cur_inv IN cur_inv_det
	
    LOOP
	
	-- get vat rate by part attribute
	
	SELECT TRIM(GET_CODE_NAME_ALT (get_part_attr_value_by_comp(cur_inv.part_num,'200002', v_company_id))) 
	INTO v_vat_per
	FROM DUAL;
			
	-- get vat rate by part product family
		IF v_vat_per IS NULL OR vat_cntry_fl = 'Y' THEN
		
			SELECT get_partnum_product(cur_inv.part_num) INTO v_part_family FROM DUAL;
			
			--PC-4047 Invoice Luxembourg customers in GM Belgium
			IF vat_cntry_fl = 'N' THEN

			BEGIN
			
			SELECT get_rule_value_by_company(v_part_family,'VAT_RATE',v_company_id) INTO v_vat_per FROM DUAL;
			EXCEPTION
			WHEN OTHERS
			THEN
				v_vat_per := NULL;
			END;
			--PC-4047 Invoice Luxembourg customers in GM Belgium
			--Implants: 3% Instruments: 17% Frequently sold: 17% Other Product Family - not sellable (e.g. literature & graphic cases): 0% All Other product Family if applicable: 17%		
			ELSE
		
			SELECT get_rule_value (v_country||'_'||v_part_family,'VAT_RATE_BY_COUNTRY') INTO v_vat_per FROM DUAL;

			END IF;
			
		END IF;
		
		-- get vat rate by account or company
		IF v_vat_per IS NULL THEN
		
			SELECT NVL(v_acc_vat_rate , v_company_vat_rate) INTO v_vat_per FROM DUAL;
		
		END IF;
		
		UPDATE t502_item_order SET c502_vat_rate=v_vat_per 
		WHERE c501_order_id IN (
		 SELECT t501.c501_order_id
                  FROM t501_order t501
                 WHERE c501_customer_po = p_custpo
                   AND c704_account_id = p_accid
                   AND c503_invoice_id = p_invoice_id
                   AND c1900_company_id = v_company_id
                   AND c501_void_fl IS NULL
                 )
               AND c205_part_number_id = cur_inv.part_num
               AND c502_void_fl IS NULL;
	
	END LOOP;
	
 
		   	     
END IF; 
   END gm_sav_vatrate;
 
/***************************************************************
   * Description : Procedure to update vat info based on dealer

   * Author     : Dinesh Rajavel
   *************************************************************/
   PROCEDURE gm_sav_vatrate_by_dealer (
      p_invoice_id   IN   t501_order.c503_invoice_id%TYPE,
      p_custpo       IN   t501_order.c501_customer_po%TYPE,
      p_dealerid     IN   t501_order.c101_dealer_id%TYPE
   )
   AS
 v_acc_vat_rate  NUMBER;
 v_company_id        t1900_company.c1900_company_id%TYPE;
 v_company_vat_rate NUMBER;
 BEGIN
	 --
	 SELECT get_compid_frm_cntx() 
      INTO v_company_id  
      FROM dual;
      --
	--SELECT 	 TRIM(GET_ACCOUNT_ATTRB_VALUE(p_accid,'101188')) INTO v_acc_vat_rate FROM DUAL;
	--
	v_company_vat_rate := get_rule_value_by_company (v_company_id,'VAT_RATE', v_company_id);
	--
 UPDATE t502_item_order
 SET c502_vat_rate = decode(get_part_attr_value_by_comp(c205_part_number_id,'200002', v_company_id), NULL, v_company_vat_rate ,GET_CODE_NAME_ALT (get_part_attr_value_by_comp(c205_part_number_id,'200002', v_company_id)))
       WHERE c501_order_id IN (
                SELECT t501.c501_order_id
                  FROM t501_order t501
                 WHERE c501_customer_po = p_custpo
                   AND c101_dealer_id = p_dealerid
                   AND c503_invoice_id = p_invoice_id
                   AND c501_void_fl IS NULL
		   	       AND (c901_ext_country_id is NULL OR c901_ext_country_id IN (SELECT country_id FROM v901_country_codes)));-- To include ext country id for GH
   END gm_sav_vatrate_by_dealer;
 

/******************************************************************
* Description : Procedure to fetch the Attributes of an order
* Author    : Brinal
****************************************************************/
   PROCEDURE gm_fch_order_attrib_info (
      p_orderid   IN       t501_order.c501_order_id%TYPE,
      p_codegrp	  IN       t901_code_lookup.c901_code_grp%TYPE,
      p_request   OUT      TYPES.cursor_type
   )
   AS
   	v_company_id    t1900_company.c1900_company_id%TYPE;
   BEGIN
	   
	--Getting company id from context.   
    SELECT get_compid_frm_cntx()
	  INTO v_company_id 
	  FROM dual;
      OPEN p_request
       FOR
          SELECT c501a_order_attribute_id oaid,
                 t501a.c501a_attribute_value attrval,
                 t901.c901_code_nm trtype, t901.c901_code_id trtypeid,
                 DECODE (get_user_name (t501a.c501a_last_updated_by),
                         ' ', get_user_name (t501a.c501a_created_by),
                         get_user_name (t501a.c501a_last_updated_by)
                        ) upby,
                 NVL (t501a.c501a_last_updated_date,
                      c501a_created_date) udate,
                 t501a.c501_order_id ordid, t501a.c501a_history_fl hfl
            FROM t901_code_lookup t901, t501a_order_attribute t501a
           WHERE t901.c901_code_id = t501a.c901_attribute_type(+)
             AND t901.c901_code_grp = p_codegrp
             AND t901.c901_void_fl IS NULL
             AND t501a.c501a_void_fl(+) IS NULL
             AND t901.c901_active_fl = '1'
             AND t501a.c501_order_id(+) = p_orderid
             AND T901.C901_CODE_ID NOT IN 
                 ( 
                 SELECT T901D.C901_CODE_ID 
                 FROM T901D_CODE_LKP_COMPANY_EXCLN T901D, T901_CODE_LOOKUP T901 
                 WHERE T901D.C901D_VOID_FL   IS NULL 
                 AND T901D.C1900_COMPANY_ID = v_company_id
                 AND T901D.C901_CODE_ID     = T901.C901_CODE_ID 
                 AND T901.C901_CODE_GRP    = p_codegrp
                 AND T901.C901_ACTIVE_FL    = '1' 
                 AND T901.C901_VOID_FL     IS NULL 
                 )
             ORDER BY t901.c901_code_seq_no;
   END gm_fch_order_attrib_info;

/******************************************************************
* Description : Procedure to save the Revenue sampling details
* Author    : Brinal
****************************************************************/
   PROCEDURE gm_sav_rev_sampling (
      p_ordid    IN   t501_order.c501_order_id%TYPE,
      p_userid   IN   t501_order.c501_last_updated_by%TYPE,
      p_str      IN   VARCHAR2,
      p_datasave IN   VARCHAR2 DEFAULT NULL
   )
   AS
      v_strlen         NUMBER                      := NVL (LENGTH (p_str), 0);
      v_string         VARCHAR2 (30000)                              := p_str;
      v_substring      VARCHAR2 (1000);
      v_par_ord_id     t501_order.c501_order_id%TYPE;
      v_attrtype       t501a_order_attribute.c901_attribute_type%TYPE;
      v_attrval        t501a_order_attribute.c501a_attribute_value%TYPE;
      v_curr_attrval   t501a_order_attribute.c501a_attribute_value%TYPE;
      v_ordatbid       t501a_order_attribute.c501a_order_attribute_id%TYPE;
      v_count          NUMBER;
      v_company_id     t1900_company.c1900_company_id%TYPE;
      v_date_fmt        VARCHAR2(20);
      v_ack_order_flag VARCHAR2(10);
    
    BEGIN
	    --get company id , date format
        SELECT get_compdtfmt_frm_cntx(),get_compid_frm_cntx()
          INTO v_date_fmt,v_company_id
          FROM dual;
  
      --Validation
      SELECT COUNT (1)
        INTO v_count
        FROM t501_order
       WHERE c501_parent_order_id IS NULL
         AND c501_void_fl IS NULL
         AND c501_order_id = p_ordid;
         
   v_ack_order_flag := NVL(get_rule_value(v_company_id,'ACK_ORDER_DATE_VAL'),'N');
   
-- need to throw a exception if parent order id is not null
      IF v_count = 1 OR p_dataSave = 'Y' -- Price adjustment order attribute value need to insert. 
      THEN
         WHILE INSTR (v_string, '|') <> 0
         LOOP
            v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
            v_string := SUBSTR (v_string, INSTR (v_string, '|') + 1);
            --
            v_ordatbid := NULL;
            v_attrtype := NULL;
            v_attrval := NULL;
            --
            v_ordatbid :=
                         SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
            v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
            v_attrtype :=
                         SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
            v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
            v_attrval := v_substring;
            gm_pkg_cm_order_attribute.gm_sav_order_attribute (p_ordid,
                                                              v_attrtype,
                                                              v_attrval,
                                                              v_ordatbid,
                                                              p_userid
                                                             );
           --PC-1262 - ACK order PO required date update 
            --validate the Req date- if req date less than 42 days of order date ,send email      
             IF (v_attrtype = '103926' AND v_ack_order_flag ='Y' AND v_attrval IS NOT NULL) THEN
	            gm_pkg_sm_adj_txn.gm_ack_ord_reqdate_validation(p_ordid, v_attrval);
	         END IF;                      
         END LOOP;
      END IF;
   END gm_sav_rev_sampling;

   /***************************************************************************************
	    * Description : This procedure checks availability of customer po id info.
    	* Author: Velu
	****************************************************************************************/

	PROCEDURE gm_chk_custpoid (
        p_cust_po_id IN t501_order.c501_customer_po%TYPE,
        p_message OUT VARCHAR2
	)
	AS
    	v_count NUMBER;
    	v_company_id    t1900_company.c1900_company_id%TYPE;
		v_plant_id      t501_order.c5040_plant_id%TYPE;
	BEGIN
		--Getting company id from context.   
	    SELECT get_compid_frm_cntx()
		  INTO v_company_id 
		  FROM dual;
		--Getting plant id from context.
		SELECT  get_plantid_frm_cntx()
			INTO  v_plant_id
		FROM DUAL;
    --
    --PMT-4615 When validating the PO number, should only be validated should only be for the repective country orders
    	BEGIN
        	 SELECT COUNT (1)  INTO v_count   FROM t501_order
          	WHERE c501_customer_po   = p_cust_po_id
	            AND c501_delete_fl IS NULL
            	AND c501_void_fl IS NULL
            	AND (c901_ext_country_id IS NULL OR c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))
            	AND c1900_company_id = v_company_id
            	AND c5040_plant_id = v_plant_id;-- To include ext country id for GH
        	IF (v_count    = 0) THEN
	            p_message := 'AVAILABLE';
        	ELSE
	            p_message := 'DUPLICATE';
    	    END IF;
    	EXCEPTION
    	WHEN NO_DATA_FOUND THEN
	        p_message := 'AVAILABLE';
	    END;
	END gm_chk_custpoid;
	
	 
   /*******************************************************
   * Description : Procedure to Fetch Invoice Id
   * Author      : Velu
   *******************************************************/
   PROCEDURE gm_fch_invoiceid (
      p_refid    	IN   VARCHAR2,
      p_source   	IN   VARCHAR2,
      p_acc_curr_id	IN   t704_account.c901_currency%TYPE,
      p_invoiceid   OUT  VARCHAR2
   )
   AS
   	v_invoiceid VARCHAR2(50);
   	v_acc_cur_id t704_account.c901_currency%TYPE;
   	V_COMPANY_ID t1900_company.c1900_company_id%TYPE;

   BEGIN
	   SELECT get_compid_frm_cntx() INTO V_COMPANY_ID  FROM dual;
	   IF p_source = 1006651 THEN --Customer PO
	  		BEGIN
    	   		SELECT t501.c503_invoice_id, t704.c901_currency
				   INTO v_invoiceid, v_acc_cur_id
				   FROM t501_order t501, t704_account t704
				  WHERE t501.c704_account_id  = t704.c704_account_id
				    AND t501.c501_customer_po = p_refid
				    --AND t704.c901_currency    = p_acc_curr_id
				    AND t501.c501_void_fl    IS NULL
				    AND t501.c503_invoice_id IS NOT NULL;
    				EXCEPTION 
    					WHEN NO_DATA_FOUND  THEN
                  			v_invoiceid := NULL;
                  		WHEN TOO_MANY_ROWS  THEN
	                	  	GM_RAISE_APPLICATION_ERROR('-20999','339',p_refid);
        	END;
	   ELSIF p_source = 1006652 THEN -- Invoice ID
	   	  	BEGIN
	   			SELECT t503.c503_invoice_id, t704.c901_currency
				   INTO v_invoiceid, v_acc_cur_id
				   FROM t503_invoice t503, t704_account t704
				  WHERE t503.c704_account_id = t704.c704_account_id
				    AND t503.c503_invoice_id = p_refid
				    --AND t704.c901_currency   = p_acc_curr_id
				    AND t503.c503_void_fl   IS NULL;
	   				EXCEPTION 
		   			   	WHEN NO_DATA_FOUND  THEN
		   			   	BEGIN
				   			   		 SELECT t503.c503_invoice_id, T704.C901_CURRENCY
		                              INTO v_invoiceid, v_acc_cur_id
		                              FROM t503_invoice t503, t704_account t704
		                             WHERE t503.c101_dealer_id = t704.c101_dealer_id
		                               AND t503.c503_invoice_id = p_refid
		                               --AND t704.c901_currency   = p_acc_curr_id
		                               AND t503.c503_void_fl   IS NULL AND ROWNUM=1;
										EXCEPTION 
								   			   	WHEN NO_DATA_FOUND  THEN
						    	              		v_invoiceid := NULL;
						   END;
	   			END;
	   ELSIF p_source = 1006653 THEN -- Order ID
	   	  	BEGIN
				  SELECT t501.c503_invoice_id, t704.c901_currency
				   INTO v_invoiceid, v_acc_cur_id
				   FROM t501_order t501, t704_account t704
				  WHERE t501.c704_account_id = t704.c704_account_id
				    AND t501.c501_order_id   = p_refid
				    --AND t704.c901_currency   = p_acc_curr_id
				    AND c501_void_fl        IS NULL; 
	   				EXCEPTION  WHEN NO_DATA_FOUND  THEN
            			v_invoiceid := NULL;
       		END;	
       END IF;
      -- Validate the account currency based invoices
      IF  v_invoiceid IS NOT NULL AND  p_acc_curr_id <> v_acc_cur_id
      THEN
      	  GM_RAISE_APPLICATION_ERROR('-20999','340',GET_CODE_NAME_ALT (v_acc_cur_id));
      END IF;
      --
	  IF  v_invoiceid IS NULL  THEN
	       GM_RAISE_APPLICATION_ERROR('-20999','341',p_refid); 
	  END IF;
	  p_invoiceid := v_invoiceid;
   END gm_fch_invoiceid;


/*
* Description : Procedure to save the VAT RATE based on the Construct and Non Construct
*     Parts for the Sales Order
* Author  :
*/
PROCEDURE gm_sav_construct_vatrate (
        p_invoice_id IN t501_order.c503_invoice_id%TYPE,
        p_custpo     IN t501_order.c501_customer_po%TYPE,
        p_accid      IN t501_order.c704_account_id%TYPE,
        p_type       IN VARCHAR2)
AS
    v_orderid t501_order.c501_order_id%TYPE;
    v_company_id t1900_company.c1900_company_id%TYPE;
    v_con_vatrate    VARCHAR2 (20) ;
    v_noncon_vatrate VARCHAR2 (20) ;
BEGIN
    --
     SELECT get_compid_frm_cntx () INTO v_company_id FROM dual;
    --
    v_con_vatrate    := NVL (get_rule_value_by_company (v_company_id, 'CONSTRUCT_VAT', v_company_id), '0') ;
    v_noncon_vatrate := NVL (get_rule_value_by_company (v_company_id, 'NON_CONSTRUCT_VAT', v_company_id), '0') ;
    --
     UPDATE t502_item_order
    SET c502_vat_rate = (
        CASE
            WHEN C502_construct_fl = 'Y'
            THEN v_con_vatrate
            ELSE v_noncon_vatrate
        END)
      WHERE c501_order_id IN
        (
             SELECT C501_ORDER_ID
               FROM T501_ORDER
              WHERE C501_CUSTOMER_PO = p_custpo
                AND C704_ACCOUNT_ID  = p_accid
                AND C503_INVOICE_ID  = p_invoice_id
                AND C501_VOID_FL    IS NULL
        )
        AND C502_VOID_FL IS NULL;
END gm_sav_construct_vatrate;

/***************************************************************************************
	    * Description : This procedure checks availability of Order id info.
    	* Author: HReddi
	****************************************************************************************/

	PROCEDURE gm_chk_orderId (
        p_order_id IN t501_order.c501_order_id%TYPE,
        p_message OUT VARCHAR2
	)
	AS
    	v_count NUMBER;
	BEGIN
    --
    	BEGIN
        	SELECT COUNT(1) INTO p_message
        	  FROM t501_order t501 , t907_shipping_info t907 
        	 WHERE t501.c501_order_id = t907.c907_ref_id 
        	   AND t501.c501_order_id = TRIM(p_order_id)
        	   AND t907.c907_email_update_fl IS NULL 
        	   AND t501.c501_void_fl IS NULL
        	   AND t907.c907_void_fl IS NULL;        	
    	EXCEPTION
    	WHEN NO_DATA_FOUND THEN
	        p_message := 0;
	    END;
	END gm_chk_orderId;	
	
/***************************************************************************************
	    * Description : This procedure fetch revenue tracking.
    	* Author: Sindhu
	****************************************************************************************/
	
	PROCEDURE gm_fch_order_attrib_code_info (
      p_orderid   IN        t501_order.c501_order_id% TYPE ,
      p_codegrp   IN        t901_code_lookup.c901_code_grp% TYPE ,
      p_request   OUT      TYPES.cursor_type
   )
   AS
   v_company_id    t1900_company.c1900_company_id% TYPE ;
   
   BEGIN
  
--Getting company id from context.  
    SELECT get_compid_frm_cntx()
  INTO v_company_id
  FROM dual;
      OPEN p_request
       FOR
          SELECT c501a_order_attribute_id oaid,
                 t501a.c501a_attribute_value attrval,
                 get_code_name(t501a.c501a_attribute_value) attrvalnm,
                 t901.c901_code_nm trtype, t901.c901_code_id trtypeid,
                 DECODE (get_user_name (t501a.c501a_last_updated_by),
                         ' ' , get_user_name (t501a.c501a_created_by),
                         get_user_name (t501a.c501a_last_updated_by)
                        ) upby,
                 NVL (t501a.c501a_last_updated_date,
                      c501a_created_date) udate,
                 t501a.c501_order_id ordid, t501a.c501a_history_fl hfl
            FROM t901_code_lookup t901, t501a_order_attribute t501a
           WHERE t901.c901_code_id = t501a.c901_attribute_type
             AND t901.c901_code_grp = p_codegrp
             AND t901.c901_void_fl IS NULL
             AND t501a.c501a_void_fl IS NULL
             AND t901.c901_active_fl = '1'
             AND t501a.c501_order_id = p_orderid
             AND t501a.c501a_attribute_value IS NOT NULL
             AND T901.C901_CODE_ID NOT IN
                 (
                 SELECT T901D.C901_CODE_ID
                 FROM T901D_CODE_LKP_COMPANY_EXCLN T901D, T901_CODE_LOOKUP T901
                 WHERE T901D.C901D_VOID_FL   IS NULL
                 AND T901D.C1900_COMPANY_ID = v_company_id
                 AND T901D.C901_CODE_ID     = T901.C901_CODE_ID
                 AND T901.C901_CODE_GRP    = p_codegrp
                 AND T901.C901_ACTIVE_FL    = '1'
                 AND T901.C901_VOID_FL     IS NULL
                 )
             ORDER BY t901.c901_code_seq_no;
   END gm_fch_order_attrib_code_info;	
	
  /***************************************************************************************
	* Description : This procedure used to validate the DO.
	* Author:
	****************************************************************************************/
	PROCEDURE gm_validate_do (
	        p_order_id IN t501_order.c501_order_id%TYPE)
	AS
	    v_company_id t1900_company.c1900_company_id%TYPE;
	    v_ord_cnt NUMBER;
	BEGIN
	     SELECT get_compid_frm_cntx () INTO v_company_id FROM dual;
	    --
	     SELECT COUNT (1)
	       INTO v_ord_cnt
	       FROM t501_order
	      WHERE c501_order_id    = p_order_id
	        AND c1900_company_id = v_company_id
	        AND c501_void_fl    IS NULL;
	    -- validate the order count
	    IF v_ord_cnt = 0 THEN
	        raise_application_error ( - 20953, '') ;
	    END IF;
	    
	END gm_validate_do;
	
	/***************************************************************************************
	* Description : This procedure used to update the DO document flag.
	* Author:
	****************************************************************************************/
	PROCEDURE gm_sav_do_document_fl (
	        p_order_id    IN t501_order.c501_order_id%TYPE,
	        p_document_fl IN t501_order.c501_do_doc_upload_fl%TYPE,
	        p_user_id     IN t501_order.c501_last_updated_by%TYPE)
	AS
	BEGIN
	    -- to update the order DO flag
	     UPDATE t501_order
	    SET c501_do_doc_upload_fl = p_document_fl, c501_last_updated_by = p_user_id, c501_last_updated_date = sysdate
	      WHERE c501_order_id     = p_order_id;
	END gm_sav_do_document_fl;
	
	
	/***************************************************************************************
	* Description : This procedure used to fetch the Orders (Grand child, Child and parent orders).
	* Added the code for - PMT-28394 (Rebate management)
	* Author: mmuthusamy
	* Added new parameter p_excl_ord_type for - PMT-51170 DO summary print- Hide discount orders for rep
	****************************************************************************************/
	PROCEDURE gm_fch_all_child_orders (
	        p_order_id IN t501_order.c501_order_id%TYPE,
	        p_excl_ord_type IN t501_order.c901_order_type%TYPE,
	        p_out_orders OUT CLOB)
	AS
	    CURSOR tmp_ord_cur
	    IS
	         SELECT my_temp_txn_id ord_id
	           FROM my_temp_list
	          WHERE my_temp_txn_id IS NOT NULL;
	BEGIN
		
	    -- to delete the temp table
	     DELETE FROM my_temp_list;
	     
	    -- insert the order id and parent order id
	    -- To remove the delete flag (GMOUS (Ack order) driven by delete flag, if check the delete flag GMOUS ACK order not loading)
	    
	     INSERT
	       INTO my_temp_list
	        (
	            my_temp_txn_id
	        )
	     SELECT c501_order_id
	       FROM t501_order
	      WHERE (c501_order_id      = p_order_id
	        OR c501_parent_order_id = p_order_id)
	        AND NVL (c901_order_type, - 999) <> NVL (p_excl_ord_type, -111) -- to exclude mentioned order type from input parameter 
	        AND c501_void_fl       IS NULL;
	        
	        
	    -- again insert the grand child order id based on parent order id.
	        
	     INSERT
	       INTO my_temp_list
	        (
	            my_temp_txn_id
	        )
	     SELECT c501_order_id
	       FROM t501_order
	      WHERE c501_parent_order_id IN
	        (
	             SELECT my_temp_txn_id FROM my_temp_list
	        )
	        AND NVL (C901_ORDER_TYPE, - 999) <> NVL (p_excl_ord_type, -111) -- to exclude mentioned order type from input parameter
	        AND c501_void_fl   IS NULL;
	    
	    -- to get all the orders 
	      	        
	  SELECT utl_i18n.unescape_reference(RTRIM (XMLAGG (XMLELEMENT (e, '''' ||my_temp_txn_id || ''',')) .EXTRACT ('//text()'), ','))
       	INTO p_out_orders
       FROM my_temp_list;
       
	END gm_fch_all_child_orders;
	
	/***************************************************************************************
	* Description : This procedure used to fetch the parent order
	* Added the code for - PMT-36590 Issue Credit and Debit to use VAT Rate from Sales Order
	* Author: 
	****************************************************************************************/
	PROCEDURE gm_sav_vatrate_by_parent_order (
	        p_parent_ord_id IN t501_order.c501_order_id%TYPE,
	        p_ret_ord_id IN t501_order.c501_order_id%TYPE)
	AS
			v_vat_per T502_ITEM_ORDER.C502_VAT_RATE%TYPE;
			v_cgst_rate  t502_item_order.C502_CGST_RATE%TYPE;
			v_igst_rate  t502_item_order.C502_IGST_RATE%TYPE;
			v_sgst_rate  t502_item_order.C502_SGST_RATE%TYPE;
	
	  CURSOR cur_ord
	    IS
	         SELECT 
				t502.c205_part_number_id part_num
				FROM t501_order t501,
				t502_item_order t502
				WHERE t501.c501_order_id  = t502.c501_order_id
				AND t501.c501_order_id = p_ret_ord_id
				AND c501_void_fl IS NULL
				AND c502_void_fl IS NULL
				AND t501.C503_INVOICE_ID IS NOT NULL
				GROUP BY t502.c205_part_number_id;

	BEGIN
		FOR cur_ord_loop IN cur_ord
    LOOP
    	BEGIN
		SELECT NVL(c502_vat_rate,0),NVL(c502_igst_rate,0),NVL(c502_cgst_rate,0),NVL(c502_sgst_rate,0)
			INTO v_vat_per,v_igst_rate,v_cgst_rate,v_sgst_rate
			FROM 
			t502_item_order 
			WHERE c501_order_id = p_parent_ord_id
			AND c205_part_number_id = cur_ord_loop.part_num
			AND c502_void_fl IS NULL
			GROUP BY c502_vat_rate,c502_igst_rate,c502_cgst_rate,c502_sgst_rate;
		EXCEPTION
			WHEN OTHERS THEN
	        v_vat_per := 0;
	        v_igst_rate := NULL;
	        v_cgst_rate := NULL;
	        v_sgst_rate := NULL;
		END;
			UPDATE t502_item_order
				SET c502_vat_rate = v_vat_per,c502_igst_rate = v_igst_rate,c502_cgst_rate = v_cgst_rate,
				c502_sgst_rate = v_sgst_rate,c502_last_updated_date_utc = CURRENT_DATE
				WHERE c501_order_id = p_ret_ord_id
				AND c205_part_number_id  = cur_ord_loop.part_num
				AND c502_void_fl IS NULL;
END LOOP;
	END gm_sav_vatrate_by_parent_order;
	
/******************************************************************
* Description :  Procedure to fetch the Attributes of an order
* Author       : Raja
****************************************************************/
   PROCEDURE gm_fch_order_attrib_values (
      p_orderid       IN       t501_order.c501_order_id%TYPE,
      p_attribtype	  IN       t501a_order_attribute.c901_attribute_type%TYPE,
      p_request       OUT      TYPES.cursor_type
   )
   AS
   BEGIN
      OPEN p_request
       FOR         
            SELECT c501a_order_attribute_id oaid,
                   c501a_attribute_value attrval
			  FROM t501a_order_attribute
			 WHERE c901_attribute_type = p_attribtype 
			   AND c501_order_id = p_orderid 
			   AND c501a_void_fl IS NULL;
             
   END gm_fch_order_attrib_values;
	
END gm_pkg_ac_order;
/
