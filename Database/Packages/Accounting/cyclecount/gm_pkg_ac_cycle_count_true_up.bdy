--@"c:/database/packages/operations/purchasing/gm_pkg_ac_cycle_count_true_up.bdy";
CREATE OR REPLACE PACKAGE body gm_pkg_ac_cycle_count_true_up
IS
PROCEDURE gm_sav_true_up_main (
        p_company_id   IN t1900_company.c1900_company_id%TYPE,
        p_plant_cnt_id IN t5040_plant_master.c5040_plant_id%type)
AS
    v_cycle_count_id t2700_cycle_count.c2700_cycle_count_id%type;
    v_user_id      NUMBER := 30301;
    v_current_year NUMBER;
    --
    v_out_location_cur types.cursor_type;
    v_cyc_cnt_location_id t2701_cycle_count_location.c2701_cyc_cnt_location_id%TYPE;
    v_count_pattern t2701_cycle_count_location.c901_count_pattern%TYPE;
    v_lock_type t2701_cycle_count_location.C901_LOCK_TYPE%TYPE;
    --
BEGIN
    --1 to get the current year
    
     SELECT TO_CHAR (sysdate, 'YYYY')
       INTO v_current_year
       FROM dual;
       
    --2 to get the cycle count id
    
    BEGIN
         SELECT C2700_CYCLE_COUNT_ID
           INTO v_cycle_count_id
           FROM T2700_CYCLE_COUNT
          WHERE C5040_PLANT_ID         = p_plant_cnt_id
            AND C2700_CYCLE_COUNT_YEAR = v_current_year
            AND c2700_void_fl         IS NULL;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_cycle_count_id := NULL;
        raise_application_error ( - 20999, 'Cycle Count not yet setup for Year: ' || v_current_year ||' Plant: '||
        p_plant_cnt_id) ;
    END;
    
    -- 3. to delete the temp table.
    DELETE FROM my_temp_cyc_cnt_scheduler;
    
    -- 4. to fetch the cycle count location details.
    
    gm_pkg_ac_cyc_cnt_scheduler.gm_fch_cyc_cnt_location_dtls (v_cycle_count_id, v_out_location_cur) ;
    

    LOOP
        FETCH v_out_location_cur
           INTO v_cyc_cnt_location_id, v_count_pattern, v_lock_type;
        -- 5.1 to check the count pattern (ABC logic/Twice logic)
        
        -- 106818 ABC
        -- 106819 Twice Logic
        
        IF v_count_pattern = 106818 THEN
        	-- ABC logic then, call the ABC true up procedure.
        	
            gm_pkg_ac_cycle_count_true_up.gm_add_new_abc_rank (v_cycle_count_id, v_cyc_cnt_location_id, 1, 93320,
            p_company_id, p_plant_cnt_id, v_user_id) ;
            
        ELSIF v_count_pattern = 106819 THEN
        
            IF v_lock_type    = 106823 -- Location
                THEN
                -- Location true up
                gm_pkg_ac_cycle_count_true_up.gm_add_new_location (v_cycle_count_id, v_cyc_cnt_location_id, 1, 93320,
                p_company_id, p_plant_cnt_id, v_user_id) ;
                
                --
            ELSIF v_lock_type = 106824 -- Part/Project
                THEN
                 -- part # true up
                 
                gm_pkg_ac_cycle_count_true_up.gm_add_new_part_inv (v_cycle_count_id, v_cyc_cnt_location_id,
                p_company_id, p_plant_cnt_id, v_user_id) ;
                
                --
            END IF; -- end cyc_location.lock_type
        END IF; -- end cyc_location.count_pattern
    END LOOP; -- end loop cyc_location_cur
    --
END gm_sav_true_up_main;
--


PROCEDURE gm_add_new_abc_rank (
        p_cycle_count_id     IN t2700_cycle_count.c2700_cycle_count_id%TYPE,
        p_cycle_count_loc_id IN t2701_cycle_count_location.c2701_cyc_cnt_location_id%type,
        p_warehouse_id       IN t5051_inv_warehouse.c5051_inv_warehouse_id%type,
        p_location_type      IN t5052_location_master.c901_location_type%type,
        p_company_id         IN t1900_company.c1900_company_id%TYPE,
        p_plant_cnt_id t5040_plant_master.c5040_plant_id%type,
        p_user_id IN t2701_cycle_count_location.c2701_last_updated_by%TYPE)
AS
    v_default_rank VARCHAR2 (2) := 'C';
    --
    v_closed_business_day NUMBER;
    v_open_businsess_day  NUMBER;
    v_exculde_inv_location t906_rules.c906_rule_value%TYPE;
    --
    CURSOR new_abc_rank_cur
    IS
         SELECT t5053.C205_PART_NUMBER_ID part_num
           FROM t5051_inv_warehouse t5051, t5052_location_master t5052, T5053_LOCATION_PART_MAPPING t5053
          WHERE t5051.c5051_inv_warehouse_id = t5052.c5051_inv_warehouse_id
            AND t5052.C5052_LOCATION_ID      = t5053.C5052_LOCATION_ID
            AND t5051.c901_status_id         = 1 -- active
            AND t5051.c5051_inv_warehouse_id = p_warehouse_id -- fg warehouse,rw warehouse
            AND t5052.c901_status            = 93310 --active
            AND t5052.c901_location_type     = p_location_type --  bulk,pick face
            AND NOT EXISTS
            (
                 SELECT token FROM v_clob_list WHERE token = t5052.c5052_location_cd
            )
        AND NOT EXISTS
        (
             SELECT t2703.c205_part_number_id pnum
               FROM t2703_cycle_count_rank t2703
              WHERE t2703.c2700_cycle_count_id = p_cycle_count_id
                AND t2703.c2703_rank           = 'D'
                AND t2703.c205_part_number_id  = t5053.c205_part_number_id
                AND t2703.c2703_void_fl       IS NULL
        )
        AND t5052.c5052_void_fl IS NULL
        AND t5052.c5040_plant_id = p_plant_cnt_id
        
      MINUS
      
     SELECT c205_part_number_id part_num
       FROM t2703_cycle_count_rank
      WHERE c2700_cycle_count_id = p_cycle_count_id
        AND c2703_void_fl       IS NULL;
    --
BEGIN
    -- to get the exculde location from rules
    
     SELECT get_rule_value (p_plant_cnt_id, 'CYC_CNT_EXC_LOCATION')
       INTO v_exculde_inv_location
       FROM DUAL;
       
    -- set the exculde location to clob (it used the cursor)
    
    my_context.set_my_cloblist (v_exculde_inv_location) ;
    
    -- 1. loop the cursor and insert the data to Rank and temp table.
    FOR new_abc_rank IN new_abc_rank_cur
    LOOP
        -- 2. inset the data to Rank table (C)
        gm_pkg_ac_cycle_count_true_up.gm_sav_cycle_count_rank (p_cycle_count_id, p_cycle_count_loc_id,
        new_abc_rank.part_num, v_default_rank, p_user_id) ;
        
        -- 3. Insert the data to temp table
        
        gm_pkg_ac_cyc_cnt_scheduler.gm_sav_temp_scheduler (p_cycle_count_id, p_cycle_count_loc_id,
        new_abc_rank.part_num) ;
        
    END LOOP; -- end new_abc_rank_cur
    
    -- 4. to get the Closed and open bussiness days
    
    gm_pkg_ac_cyc_cnt_scheduler.gm_fch_company_working_day (p_company_id, 'CURRENT_DATE', v_closed_business_day,
    v_open_businsess_day) ;
    
    -- 5. to save the details to master table (scheduler - second semester)
    gm_pkg_ac_cycle_count_true_up.gm_sav_newly_added_data (p_cycle_count_id, p_cycle_count_loc_id,
    v_closed_business_day, v_open_businsess_day, p_user_id) ;
    --
END gm_add_new_abc_rank;
--
PROCEDURE gm_add_new_location (
        p_cycle_count_id     IN t2700_cycle_count.c2700_cycle_count_id%TYPE,
        p_cycle_count_loc_id IN t2701_cycle_count_location.c2701_cyc_cnt_location_id%type,
        p_warehouse_id       IN t5051_inv_warehouse.c5051_inv_warehouse_id%type,
        p_location_type      IN t5052_location_master.c901_location_type%type,
        p_company_id         IN t1900_company.c1900_company_id%TYPE,
        p_plant_cnt_id t5040_plant_master.c5040_plant_id%type,
        p_user_id IN t2701_cycle_count_location.c2701_last_updated_by%TYPE)
AS
    --
    v_closed_business_day NUMBER;
    v_open_businsess_day  NUMBER;
    v_exculde_inv_location t906_rules.c906_rule_value%TYPE;
    --
    CURSOR new_location_cur
    IS
         SELECT t5052.c5052_location_id location_id
           FROM t5051_inv_warehouse t5051, t5052_location_master t5052
          WHERE t5051.c5051_inv_warehouse_id = t5052.c5051_inv_warehouse_id
            AND t5051.c901_status_id         = 1 -- active
            AND t5051.c5051_inv_warehouse_id = p_warehouse_id -- fg warehouse,rw warehouse
            AND t5052.c901_status            = 93310 --active
            AND t5052.c901_location_type     = p_location_type --  bulk,pick face
            AND NOT EXISTS
            (
                 SELECT token FROM v_clob_list WHERE token = t5052.c5052_location_cd
            )
        AND t5052.c5052_void_fl IS NULL
        AND t5052.c5040_plant_id = p_plant_cnt_id
        
      MINUS
      
     SELECT c5052_location_id location_id
       FROM t2704_cycle_count_schedule
      WHERE c2700_cycle_count_id      = p_cycle_count_id
        AND c2701_cyc_cnt_location_id = p_cycle_count_loc_id
        AND c2704_void_fl            IS NULL;
BEGIN
    -- to get the exculde location from rule values
    
     SELECT get_rule_value (p_plant_cnt_id, 'CYC_CNT_EXC_LOCATION')
       INTO v_exculde_inv_location
       FROM DUAL;
       
    -- set the exclude values to context
    
    my_context.set_my_cloblist (v_exculde_inv_location) ;
    
    -- 1. loop the cursor and insert the data to temp table
    FOR new_location IN new_location_cur
    LOOP
        -- 2. Insert the data to temp table
        gm_pkg_ac_cyc_cnt_scheduler.gm_sav_temp_scheduler (p_cycle_count_id, p_cycle_count_loc_id,
        new_location.location_id) ;
        --
    END LOOP; -- end new_location
    
    -- 3. to get the closed/open bussiness days
     
    gm_pkg_ac_cyc_cnt_scheduler.gm_fch_company_working_day (p_company_id, 'CURRENT_DATE', v_closed_business_day,
    v_open_businsess_day) ;
    
    -- 4. to save the details to master table (T2704 scheduler)
    
    gm_pkg_ac_cycle_count_true_up.gm_sav_newly_added_data (p_cycle_count_id, p_cycle_count_loc_id,
    v_closed_business_day, v_open_businsess_day, p_user_id) ;
    
    --
END gm_add_new_location;
--


PROCEDURE gm_add_new_part_inv (
        p_cycle_count_id     IN t2700_cycle_count.c2700_cycle_count_id%TYPE,
        p_cycle_count_loc_id IN t2701_cycle_count_location.c2701_cyc_cnt_location_id%type,
        p_company_id         IN t1900_company.c1900_company_id%TYPE,
        p_plant_cnt_id t5040_plant_master.c5040_plant_id%type,
        p_user_id IN t2701_cycle_count_location.c2701_last_updated_by%TYPE)
AS
    v_txn_type VARCHAR2 (10) ;
    --
    v_warehouse_type t2701_cycle_count_location.c2701_cyc_cnt_location_id%type;
    v_closed_business_day NUMBER;
    v_open_businsess_day  NUMBER;
    v_include_sterile_parts t906_rules.c906_rule_value%TYPE;
    v_inculde_non_sterile_parts t906_rules.c906_rule_value%TYPE;
    v_out_inv_part_cur types.cursor_type;
    v_part_number t205c_part_qty.c205_part_number_id%TYPE;
    --
BEGIN
    -- get the location details
     SELECT c901_warehouse_location
       INTO v_warehouse_type
       FROM t2701_cycle_count_location
      WHERE c2701_cyc_cnt_location_id = p_cycle_count_loc_id
        AND c2701_void_fl            IS NULL;
        
    -- to get the inventory map id from rules
     SELECT get_rule_value (v_warehouse_type, 'CYC_CNT_INV_MAP_ID')
       INTO v_txn_type
       FROM dual;
       
    -- No rules then thrown an error message
    
    IF v_txn_type IS NULL THEN
        raise_application_error ( - 20999,
        'Rules not availabe for the inventory, Please setup the rules for this warehouse: '|| get_code_name (
        v_warehouse_type)) ;
    END IF;
    
    -- 4030 Sterile
    IF v_warehouse_type = 106805 -- QN
        THEN
        -- to get the non sterile parts
        
         SELECT get_rule_value (p_plant_cnt_id, 'CYC_CNT_INC_NON_STRE')
           INTO v_inculde_non_sterile_parts
           FROM DUAL;
           
        -- set the values to clob
        my_context.set_my_cloblist (v_inculde_non_sterile_parts) ;
        --
        
        OPEN v_out_inv_part_cur FOR
        
         SELECT t205c.c205_part_number_id pnum
		   FROM t205c_part_qty t205c, t205_part_number t205
		  WHERE t205c.c5040_plant_id               = p_plant_cnt_id
		    AND t205.c205_part_number_id           = t205c.c205_part_number_id
		    AND (NVL (t205.c205_product_class, 0) <> 4030
		    OR EXISTS
		    (
		         SELECT token FROM v_clob_list WHERE token = t205.c205_part_number_id
		    ))
		    AND t205c.c901_transaction_type        = v_txn_type
		    AND NVL (t205c.c205_available_qty, 0) <> 0
		    AND NOT EXISTS
		    (
		         SELECT t2703.c205_part_number_id pnum
		           FROM t2703_cycle_count_rank t2703
		          WHERE t2703.c2700_cycle_count_id = p_cycle_count_id
		            AND t2703.c2703_rank           = 'D'
		            AND t2703.c205_part_number_id  = t205c.c205_part_number_id
		            AND t2703.c2703_void_fl       IS NULL
		    )
		  MINUS
		 SELECT c205_part_number_id part_number
		   FROM t2704_cycle_count_schedule
		  WHERE c2700_cycle_count_id      = p_cycle_count_id
		    AND c2701_cyc_cnt_location_id = p_cycle_count_loc_id
		    AND c2704_void_fl            IS NULL;
		    
ELSIF v_warehouse_type                = 106806 -- QN (S)
    THEN
    
    -- to get the sterile parts from rules
     SELECT get_rule_value (p_plant_cnt_id, 'CYC_CNT_INC_NON_STRE')
       INTO v_include_sterile_parts
       FROM DUAL;
       
    -- set the sterile parts to clob list
    
    my_context.set_my_cloblist (v_include_sterile_parts) ;
    
    --
    OPEN v_out_inv_part_cur FOR
    
	     SELECT t205c.c205_part_number_id pnum
	   FROM t205c_part_qty t205c, t205_part_number t205
	  WHERE t205c.c5040_plant_id              = p_plant_cnt_id
	    AND t205c.c901_transaction_type       = v_txn_type
	    AND t205.c205_part_number_id          = t205c.c205_part_number_id
	    AND (NVL (t205.c205_product_class, 0) = 4030
	    OR EXISTS
	    (
	         SELECT token FROM v_clob_list WHERE token = t205.c205_part_number_id
	    ))
	    AND NVL (t205c.c205_available_qty, 0) <> 0
	    AND NOT EXISTS
	    (
	         SELECT t2703.c205_part_number_id pnum
	           FROM t2703_cycle_count_rank t2703
	          WHERE t2703.c2700_cycle_count_id = p_cycle_count_id
	            AND t2703.c2703_rank           = 'D'
	            AND t2703.c205_part_number_id  = t205c.c205_part_number_id
	            AND t2703.c2703_void_fl       IS NULL
	    )
	    
	  MINUS
	  
	 SELECT c205_part_number_id part_number
	   FROM t2704_cycle_count_schedule
	  WHERE c2700_cycle_count_id      = p_cycle_count_id
	    AND c2701_cyc_cnt_location_id = p_cycle_count_loc_id
	    AND c2704_void_fl            IS NULL;
    
ELSE
    OPEN v_out_inv_part_cur FOR
     SELECT t205c.c205_part_number_id pnum
	   FROM t205c_part_qty t205c
	  WHERE t205c.c5040_plant_id                = p_plant_cnt_id
	    AND t205c.c901_transaction_type         = v_txn_type
	    AND NVL ( t205c.c205_available_qty, 0) <> 0
	    AND NOT EXISTS
	    (
	         SELECT t2703.c205_part_number_id pnum
	           FROM t2703_cycle_count_rank t2703
	          WHERE t2703.c2700_cycle_count_id = p_cycle_count_id
	            AND t2703.c2703_rank           = 'D'
	            AND t2703.c205_part_number_id  = t205c.c205_part_number_id
	            AND t2703.c2703_void_fl       IS NULL
	    )
	  MINUS
	 SELECT c205_part_number_id part_number
	   FROM t2704_cycle_count_schedule
	  WHERE c2700_cycle_count_id      = p_cycle_count_id
	    AND c2701_cyc_cnt_location_id = p_cycle_count_loc_id
	    AND c2704_void_fl            IS NULL;
END IF;
--
LOOP
    FETCH v_out_inv_part_cur INTO v_part_number;
    
    -- 2. Insert the data to temp table
    gm_pkg_ac_cyc_cnt_scheduler.gm_sav_temp_scheduler (p_cycle_count_id, p_cycle_count_loc_id, v_part_number) ;
    
END LOOP; -- end v_out_inv_part_cur

--3. to get the closed/Open working days

gm_pkg_ac_cyc_cnt_scheduler.gm_fch_company_working_day (p_company_id, 'CURRENT_DATE', v_closed_business_day,
v_open_businsess_day) ;

-- 4. to save the details to master table (scheduler)
gm_pkg_ac_cycle_count_true_up.gm_sav_newly_added_data (p_cycle_count_id, p_cycle_count_loc_id, v_closed_business_day,
v_open_businsess_day, p_user_id) ;
--
END gm_add_new_part_inv;
--


PROCEDURE gm_sav_cycle_count_rank (
        p_cycle_count_id     IN t2700_cycle_count.c2700_cycle_count_id%TYPE,
        p_cycle_count_loc_id IN t2701_cycle_count_location.c2701_cyc_cnt_location_id%type,
        p_part_number        IN T2703_CYCLE_COUNT_RANK.C205_PART_NUMBER_ID%TYPE,
        p_rank               IN T2703_CYCLE_COUNT_RANK.c2703_rank%TYPE,
        p_user_id            IN T2703_CYCLE_COUNT_RANK.C2703_LAST_UPDATED_BY%TYPE)
AS
BEGIN
     INSERT
       INTO t2703_cycle_count_rank
        (
            c2703_cycle_count_rank_id, c2700_cycle_count_id, c2701_cyc_cnt_location_id
          , c205_part_number_id, c2703_rank, c2703_last_updated_by
          , c2703_last_updated_date
        )
        VALUES
        (
            s2703_cycle_count_rank.nextval, p_cycle_count_id, p_cycle_count_loc_id
          , p_part_number, p_rank, p_user_id
          , CURRENT_DATE
        ) ;
END gm_sav_cycle_count_rank;
--

PROCEDURE gm_sav_newly_added_data
    (
        p_cycle_count_id      IN t2700_cycle_count.c2700_cycle_count_id%TYPE,
        p_cycle_count_loc_id  IN t2701_cycle_count_location.c2701_cyc_cnt_location_id%type,
        p_closed_business_day IN NUMBER,
        p_open_business_day   IN NUMBER,
        p_user_id             IN T2703_CYCLE_COUNT_RANK.C2703_LAST_UPDATED_BY%TYPE
    )
AS
    v_last_working_date    DATE;
    v_schedule_date        DATE;
    v_last_working_day_cnt NUMBER;
    --
    v_working_day_cnt           NUMBER;
    v_remaining_working_day_cnt NUMBER;
    --
    v_lock_type t2701_cycle_count_location.c901_lock_type%TYPE;
    v_count_pattern t2701_cycle_count_location.c901_count_pattern%TYPE;
    v_newly_added_fl VARCHAR2 (2) := 'Y';
    --
    CURSOR update_scheduler_dtls_cur
    IS
         SELECT tmp.my_temp_seq_id seq_id, tmp.my_temp_cyc_cnt_location_id cc_location_id, DECODE (v_lock_type, 106823,
            tmp.my_temp_ref_id, NULL) location_id, DECODE (v_lock_type, 106822, tmp.my_temp_ref_id, v_lock_type, 106824
            , tmp.my_temp_ref_id, NULL) part_num, p_closed_business_day + ntile (p_open_business_day) over (order by
            NULL) AS num
           FROM my_temp_cyc_cnt_scheduler tmp
          WHERE my_temp_cyc_cnt_id          = p_cycle_count_id
            AND my_temp_cyc_cnt_location_id = p_cycle_count_loc_id
       ORDER BY my_temp_seq_id;
BEGIN
	
    --1. to get the location details (Lock type / count pattern)
     SELECT c901_lock_type, c901_count_pattern
       INTO v_lock_type, v_count_pattern
       FROM t2701_cycle_count_location
      WHERE c2701_cyc_cnt_location_id = p_cycle_count_loc_id;
      
    -- 2. loop the data and call the scheudler procedure
    FOR update_scheduler IN update_scheduler_dtls_cur
    LOOP
        -- based on the working days to get the date.
         SELECT cal_date
           INTO v_schedule_date
           FROM v1900_company_working_days
          WHERE working_day = update_scheduler.num;
          
        -- 3. to store the values to T2704 table
        
        gm_pkg_ac_cyc_cnt_scheduler.gm_sav_cyc_cnt_scheduler (p_cycle_count_id, p_cycle_count_loc_id,
        update_scheduler.part_num, update_scheduler.location_id, NULL, v_schedule_date, v_newly_added_fl, p_user_id) ;
        --
    END LOOP; -- end update_scheduler
    --
END gm_sav_newly_added_data;
--
END gm_pkg_ac_cycle_count_true_up;
/
