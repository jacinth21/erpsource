--@"c:\database\packages\accounting\cyclecount\gm_pkg_ac_cc_lot_txn.pkg";
CREATE OR REPLACE PACKAGE BODY gm_pkg_ac_cc_lot_txn
IS

	/**********************************************************************************************
    * Description      : Procedure used to update qty based on warehouse
    * Author           : Agilan Singaravel
    ***********************************************************************************************/
    PROCEDURE gm_upd_lot_track_data (
            p_cyc_lock_id	 IN 	t2705_cycle_count_lock.c2705_cyc_cnt_lock_id%TYPE
    )
    AS
    v_plant_id				t2700_cycle_count.c5040_plant_id%type;
    v_warehouse_location	t2701_cycle_count_location.c901_warehouse_location%type;
    v_exec_string   		VARCHAR2(4000);
    v_rule_value    		VARCHAR2(4000);
    v_warehouse_type		NUMBER;
    v_avail_qty				NUMBER;
    v_trans_id				t5060_control_number_inv.c5060_last_update_trans_id%type;
    v_trans_type			t5060_control_number_inv.c901_last_transaction_type%type;
    v_ref_id				t5060_control_number_inv.c5060_ref_id%type;
    v_ref_type				t5060_control_number_inv.c901_ref_type%type;
    v_company_id			t1900_company.c1900_company_id%type;
    v_plant					t5040_plant_master.c5040_plant_id%type;
    v_loc_tye				t5080_lot_track_inv.c901_location_type%type;
    v_loc_name				t5080_lot_track_inv.c5080_location_name%type;
    v_upd_qty				NUMBER;
    v_inhouse_qty			t413_inhouse_trans_items.c413_item_qty%type;
    v_inv_qty				t5061_control_number_inv_log.c5061_txn_qty%type;
    v_part_num				t205_part_number.c205_part_number_id%type;
    v_control_num			t5080_lot_track_inv.c2550_control_number%type;
    v_txn_qty				t5061_control_number_inv_log.c5061_txn_qty%type;
    v_txn_id				t2707_cycle_count_txn_ref.c2707_txn_id%type;
    v_iafg_id				t2707_cycle_count_txn_ref.c2707_txn_id%type;
    v_fgia_id				t2707_cycle_count_txn_ref.c2707_txn_id%type;
    v_iafg_type				NUMBER;
    v_fgia_type				NUMBER;
    v_comp_id_frmcntx       t1900_company.c1900_company_id%type;
    --Below cursor is used to fetch IAFG or FGIA based on cycle count id
    CURSOR cur_ref
    IS
    	SELECT c2707_txn_id txn_id
          FROM T2707_CYCLE_COUNT_TXN_REF
         WHERE c2705_cyc_cnt_lock_id = p_cyc_lock_id
           AND c2707_void_fl IS NULL
           AND v_warehouse_type = 90800; --FG-Bulk and FG-Pick
           
    --Below cursor is used to fetch scan qty to compare the inventory qty based on cycle count id    
    CURSOR cur_details
    IS 
    	SELECT t2708.c205_part_number_id partnum ,t2708.c2550_control_number cntlnum, t2708.c5052_location_id locid,
    	       t2708.c2708_scanned_by scanby,c5052_location_cd loccd,sum(t2708.c2708_qty) scanqty
	      FROM T2708_CYCLE_COUNT_SCANNED_LOTS t2708,t5052_LOCATION_MASTER t5052
	     WHERE t2708.c2705_cyc_cnt_lock_id = p_cyc_lock_id
	       AND t2708.c5052_location_id = t5052.c5052_location_id(+)
	       AND c2708_void_fl is NULL
	       AND c5052_void_fl(+) is null
	  GROUP BY t2708.c205_part_number_id,t2708.c2550_control_number,t2708.c5052_location_id,t2708.c2708_scanned_by,c5052_location_cd;
	      
	--Below cursor is used to fetch inhouse details based on IAFG or FGIA id
	CURSOR v_inhouse_data
	IS
        SELECT c413_trans_id transid,t413.c205_part_number_id partnum, c413_item_price itemprice,
        	   c413_status_fl statfl,c413_ref_id refid,t413.c901_ref_type reftype
          FROM T412_INHOUSE_TRANSACTIONS t412, T413_INHOUSE_TRANS_ITEMS t413
		 WHERE t413.c412_inhouse_trans_id = t412.c412_inhouse_trans_id
		   AND t413.c412_inhouse_trans_id = v_txn_id
		   AND t412.c412_void_fl IS NULL
           AND t413.c413_void_fl IS NULL;
           
    BEGIN
	    
	    SELECT get_compid_frm_cntx()  
	      INTO v_comp_id_frmcntx
	      FROM  DUAL;
    
	 BEGIN
		 --Query is select plant id and warehouse location based on cycle count id
	    SELECT t2700.c5040_plant_id, t2701.c901_warehouse_location,get_plant_parent_comp_id(t2700.c5040_plant_id)
	      INTO v_plant_id, v_warehouse_location,v_comp_id_frmcntx
	      FROM T2705_CYCLE_COUNT_LOCK t2705, T2701_CYCLE_COUNT_LOCATION t2701 , T2700_CYCLE_COUNT t2700 
	     WHERE t2701.c2701_cyc_cnt_location_id = t2705.c2701_cyc_cnt_location_id 
	       AND t2700.c2700_cycle_count_id = t2701.c2700_cycle_count_id 
	       AND t2705.c2705_cyc_cnt_lock_id = p_cyc_lock_id
	       AND t2705.c2705_void_fl is null
	       AND t2701.c2701_void_fl is null
	       AND t2700.c2700_void_fl is null
	       AND t2705.C2705_PARENT_CYC_CNT_LOCK_ID IS NOT NULL;
	 EXCEPTION WHEN OTHERS THEN
	 	v_plant_id := NULL;
	 	v_warehouse_location := NULL;
	 END;
	 
	 --Based on warehouse location we get warehouse type from rule table
     SELECT get_rule_value (v_warehouse_location, 'CYC_CNT_LOT')
	   INTO v_warehouse_type
   	   FROM DUAL;
   	   
   	 --If warehouse type is not null then only we proceed to check avaulble qty and scaned qty from t5060, t5080 table and t2078 table
   	 --and compare both qty and update in inventory table & inhouse table based on control number
   	 IF v_warehouse_type IS NOT NULL THEN
   	 	FOR lock_details IN cur_details
     	LOOP
     		IF v_plant_id = 3001 THEN --BBA
     		  BEGIN
	     		  --Query is used to fetch available qty based on part number and control number in t5060 table if plant is BBA (3001)
     			SELECT c5060_qty,c5060_last_update_trans_id,c901_last_transaction_type,
     		    	   c5060_ref_id,c901_ref_type,c1900_company_id,c5040_plant_id
     		  	  INTO v_avail_qty,v_trans_id,v_trans_type,v_ref_id,v_ref_type,v_company_id,v_plant
     		      FROM T5060_CONTROL_NUMBER_INV
     		     WHERE c205_part_number_id = lock_details.partnum
     		       AND c5060_control_number = lock_details.cntlnum
     		       --AND c5080_location_id = lock_details.locid
     		       AND c901_warehouse_type = v_warehouse_type;
     		  EXCEPTION WHEN OTHERS THEN
     		  	v_avail_qty := 0;
     		  	v_company_id := v_comp_id_frmcntx;
     		  END;
     		ELSE
     		  BEGIN
	     		   --Query is used to fetch available qty based on part number and control number in t5080 table if plant is not BBA (!=3001)
     			SELECT c5080_qty,c5080_last_update_trans_id,c901_last_transaction_type,
     		    	   c901_location_type,c5080_location_name,c1900_company_id,c5040_plant_id
     		  	  INTO v_avail_qty,v_trans_id,v_trans_type,v_loc_tye,v_loc_name,v_company_id,v_plant
     		      FROM T5080_LOT_TRACK_INV
     		     WHERE c205_part_number_id = lock_details.partnum
     		       AND c2550_control_number = lock_details.cntlnum
     		       AND nvl(c5080_location_id,-999) = nvl(lock_details.locid,-999)
     		       AND c901_warehouse_type = v_warehouse_type
     		       AND c1900_company_id = v_comp_id_frmcntx
     		       AND c5080_void_fl IS NULL;
     		  EXCEPTION WHEN OTHERS THEN
     		  	v_avail_qty := 0;
     		  	v_company_id := v_comp_id_frmcntx;
     		  END;
     	    END IF;
     		   
     	    --Here we check available qty and scaned qty and return upd_qty based on below condition.
     		 SELECT CASE 
     		   WHEN v_avail_qty < lock_details.scanqty THEN
     		   		lock_details.scanqty - v_avail_qty
     		   WHEN v_avail_qty > lock_details.scanqty THEN  --36 > 8
     		   		lock_details.scanqty - v_avail_qty   --8-36 ==> -28
     		   WHEN v_avail_qty = lock_details.scanqty THEN
     		   		0
     		   END INTO v_upd_qty
     		 FROM DUAL;
     		 
     	 BEGIN
     		 SELECT t2707.c2707_txn_id,t412.c412_type
     		   INTO v_fgia_id,v_fgia_type
               FROM T2707_CYCLE_COUNT_TXN_REF t2707,T412_INHOUSE_TRANSACTIONS t412
              WHERE t2707.c2707_txn_id = t412.c412_inhouse_trans_id
                AND c412_type = 400065 			--Shelf to Inventory Adjustment
                AND c2705_cyc_cnt_lock_id = p_cyc_lock_id
                AND c2707_void_fl IS NULL
                AND c412_void_fl is null;
          EXCEPTION WHEN OTHERS THEN
          	v_fgia_id := NULL;
          	v_fgia_type := NULL;
          END;
     		
         BEGIN 
             SELECT t2707.c2707_txn_id,t412.c412_type
     		   INTO v_iafg_id,v_iafg_type
               FROM T2707_CYCLE_COUNT_TXN_REF t2707,T412_INHOUSE_TRANSACTIONS t412
              WHERE t2707.c2707_txn_id = t412.c412_inhouse_trans_id
                AND c412_type = 400068  			--Inventory Adjustment to Shelf
                AND c2705_cyc_cnt_lock_id = p_cyc_lock_id
                AND c2707_void_fl IS NULL
                AND c412_void_fl is null;
         EXCEPTION WHEN OTHERS THEN
         	v_iafg_id := NULL;
         	v_iafg_type := NULL;
         END;
        
         --If v_upd_qty > 0 then we need to update cycle count id as last updated trans id

         v_trans_id := p_cyc_lock_id;
         v_trans_type := 106940;     --Cycle Count Adjustment
         
        --This below qury is used to get which procedure to execute based on plant id and rule grp id
        --if palnt is 3001 then gm_pkg_op_ld_lot_track.gm_sav_control_number_inv 
        --otherwise gm_pkg_op_lot_track_data.gm_upd_lot_inventory
     IF v_upd_qty <> '0' AND v_trans_id IS NOT NULL THEN
     		SELECT get_rule_value (v_plant_id, 'CYC_CNT_LOT_PRC')
	          INTO v_rule_value
   	          FROM DUAL;
   	
   	 IF v_plant_id = 3001 THEN  --BBA
   	   v_exec_string := 'BEGIN ' || v_rule_value || '(''' || lock_details.partnum || ''','''|| lock_details.cntlnum || ''','''||v_warehouse_type ||''','''||v_upd_qty||
   	   					''','''||v_trans_id||''','''||v_trans_type||''','''||v_ref_type||''','''||v_ref_id||''','''||lock_details.scanby||''','''||v_company_id||
   	   					''','||v_plant_id||'); END;';
	 ELSE
	    v_exec_string := 'BEGIN ' || v_rule_value || '(''' || lock_details.partnum || ''','''|| lock_details.cntlnum || ''','''|| v_upd_qty||''','''||v_warehouse_type||
   	   					''','''||v_trans_id||''','''||v_trans_type||''','''||lock_details.locid||''','''||v_loc_tye||''','''||lock_details.loccd||''','''||v_company_id||
   	   					''','''||v_plant_id||''','''||trunc(current_date)||'''); END;';
	 END IF;
	
	 EXECUTE IMMEDIATE (v_exec_string);
	 
	 END IF;
  --end loop;   	
--   	 END IF;
   	 	 	 
	 FOR cur_ref_dtl IN cur_ref
	 LOOP
	 	SELECT sum(C413_ITEM_QTY) 
	 	  INTO v_inhouse_qty 
	 	  FROM T412_INHOUSE_TRANSACTIONS t412, T413_INHOUSE_TRANS_ITEMS t413
         WHERE t413.c412_inhouse_trans_id = t412.c412_inhouse_trans_id 
           AND t413.c412_inhouse_trans_id = cur_ref_dtl.txn_id
           AND t413.c205_part_number_id = lock_details.partnum
           AND t412.c412_void_fl IS NULL
           AND t413.c413_void_fl IS NULL;
           
        IF v_plant_id = 3001 THEN
        	SELECT sum(t5061.c5061_new_qty) 
          	  INTO v_inv_qty 
          	  FROM T5060_CONTROL_NUMBER_INV t5060, T5061_CONTROL_NUMBER_INV_LOG t5061 
         	 WHERE t5060.c5060_control_number_inv_id = t5061.c5060_control_number_inv_id 
           	   AND t5061.c5061_last_update_trans_id = p_cyc_lock_id
           	   AND t5061.c205_part_number_id = lock_details.partnum
           	   AND t5061.c901_warehouse_type = v_warehouse_type;
        ELSE
        	SELECT sum(t5081.C5081_new_QTY) 
          	  INTO v_inv_qty 
          	  FROM T5080_LOT_TRACK_INV t5080, T5081_LOT_TRACK_INV_LOG t5081 
         	 WHERE t5081.c5080_control_number_inv_id = t5080.c5080_control_number_inv_id 
           	   AND t5081.c5081_last_update_trans_id = p_cyc_lock_id
           	   AND t5081.c205_part_number_id = lock_details.partnum
           	   AND t5081.c901_warehouse_type = v_warehouse_type
           	   AND t5080.c5080_void_fl is null
           	   AND t5081.c5081_void_fl is null;
        END IF;
           
        -- Check inhouse qty and inventory qty is its equal then update inhouse transaction as void and insert new record in t413 table baeed on transid and controlnum and partnum
        IF (v_inhouse_qty = v_inv_qty) THEN
        	v_txn_id := cur_ref_dtl.txn_id;
              FOR inhouse_data IN v_inhouse_data
              LOOP
              	IF v_plant_id = 3001 THEN
              	 BEGIN
              		SELECT t5061.c205_part_number_id,t5061.c5060_control_number, t5061.c5061_new_qty
          	  		  INTO v_part_num,v_control_num,v_txn_qty 
          	          FROM T5060_CONTROL_NUMBER_INV t5060, T5061_CONTROL_NUMBER_INV_LOG t5061 
         	         WHERE t5060.c5060_control_number_inv_id = t5061.c5060_control_number_inv_id 
           	           AND t5061.c5061_last_update_trans_id = p_cyc_lock_id
           	           AND t5061.c205_part_number_id = inhouse_data.partnum
           	           AND t5061.c5060_control_number = lock_details.cntlnum;
           	      EXCEPTION WHEN OTHERS THEN
           	      	v_txn_qty := null;
           	      END;
              	ELSE
              	BEGIN
              		SELECT t5081.c205_part_number_id,t5081.c2550_control_number, t5081.c5081_new_qty
              		  INTO v_part_num,v_control_num,v_txn_qty
              		  FROM T5080_LOT_TRACK_INV t5080, T5081_LOT_TRACK_INV_LOG t5081 
              		 WHERE t5081.c5080_control_number_inv_id = t5080.c5080_control_number_inv_id 
              		   AND t5081.c5081_last_update_trans_id = p_cyc_lock_id
					   AND t5081.c205_part_number_id = inhouse_data.partnum
					   AND t5081.c2550_control_number = lock_details.cntlnum
					   AND t5080.c5080_void_fl is null
					   AND t5081.c5081_void_fl is null;
			     EXCEPTION WHEN OTHERS THEN
			     	v_txn_qty := NULL;
			     END;
		        END IF;
					   
		        	IF v_txn_qty IS NOT NULL THEN
					   update t413_inhouse_trans_items set c413_void_fl = 'Y' where c413_trans_id = inhouse_data.transid;
					   
					   INSERT INTO t413_inhouse_trans_items (C413_TRANS_ID,C413_CONTROL_NUMBER,C413_ITEM_QTY,C413_ITEM_PRICE,C205_PART_NUMBER_ID,C412_INHOUSE_TRANS_ID,C413_STATUS_FL,C413_CREATED_DATE,C413_REF_ID,C901_REF_TYPE)
					   values (S413_INHOUSE_ITEM.nextval,v_control_num,v_txn_qty,inhouse_data.itemprice,v_part_num,cur_ref_dtl.txn_id,inhouse_data.statfl,current_date,inhouse_data.refid ,inhouse_data.reftype);
              		END IF;
				END LOOP;
        END IF;
	 END LOOP;
	 END LOOP;
	 END IF;
    END gm_upd_lot_track_data;
/**********************************************************************************************
    * Description      : Procedure used to update the physical qty to t2706_cycle_count_lock_dtls
    * Author           : tramasamy
 ***********************************************************************************************/

    PROCEDURE gm_sav_counted_qty (
        p_cycle_count_id       IN   t2706_cycle_count_lock_dtls.c2705_cyc_cnt_lock_id%TYPE,
        p_cc_lock_dtl_id       IN   t2708_cycle_count_scanned_lots.c2706_cyc_cnt_lock_dtls_id%TYPE,
        p_cycle_count_loc_id   IN   t2706_cycle_count_lock_dtls.c5052_location_id%TYPE,
        p_part_num             IN   t2706_cycle_count_lock_dtls.c205_part_number_id%TYPE,
        p_qty                  IN   t2706_cycle_count_lock_dtls.c2706_physical_qty%TYPE,
        p_lot_num              IN   t2708_cycle_count_scanned_lots.c2550_control_number%TYPE,
        p_set_id               IN   t2706_cycle_count_lock_dtls.c207_set_id%TYPE,
        p_consign_id           IN   t2706_cycle_count_lock_dtls.c504_consignment_id%TYPE,
        p_user_id              IN   t2706_cycle_count_lock_dtls.c2706_last_updated_by%TYPE,
        p_out_msg              OUT   VARCHAR2
    ) AS
    v_lock_status  t2705_cycle_count_lock.C901_LOCK_STATUS%type;
    v_counted_by t2706_cycle_count_lock_dtls.c101_counted_by%type;
    v_recount_fl t2706_cycle_count_lock_dtls.C2706_RECOUNT_FL%type;
    v_prod_material t205_part_number.C205_PRODUCT_MATERIAL%type;
	v_tissue_lot_cnt NUMBER;
	v_physical_qty t2706_cycle_count_lock_dtls.c2706_physical_qty%type;
	v_counted_dt   t2706_cycle_count_lock_dtls.c2706_counted_date%type;
	
    BEGIN
	    
	    SELECT C901_LOCK_STATUS INTO v_lock_status FROM t2705_cycle_count_lock 
	     WHERE C2705_CYC_CNT_LOCK_ID = p_cycle_count_id
	      AND C2705_VOID_FL IS NULL;
	      
	      --106827 - Open ,106830 - Recount
	      IF (v_lock_status ='106827') THEN
	         UPDATE t2705_cycle_count_lock 
	           SET C901_LOCK_STATUS = '106828' --106828 - Inprogess 
	         WHERE C2705_CYC_CNT_LOCK_ID = p_cycle_count_id
	           AND C2705_VOID_FL IS NULL;
	      END IF;
	      --get counted by and recunt flag for update
	        SELECT NVL(c101_counted_by,'-999') ,NVL(C2706_RECOUNT_FL,'N'),c2706_physical_qty,c2706_counted_date INTO v_counted_by,v_recount_fl, v_physical_qty,v_counted_dt 
	         FROM t2706_cycle_count_lock_dtls 
	        WHERE c2705_cyc_cnt_lock_id = p_cycle_count_id
              AND c2706_cyc_cnt_lock_dtls_id = p_cc_lock_dtl_id
              AND c2706_void_fl IS NULL;
              
	      --If status is recount to update the recount flag 
	     IF (v_lock_status='106830' AND v_recount_fl ='Y') THEN
	              gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (p_cc_lock_dtl_id, v_physical_qty, 1181, v_counted_by, v_counted_dt) ;   --when history icon show phsical qty value is null avoid in PC-4480 
	     
	       -- To update physical qty value as null in recount status
		      UPDATE t2706_cycle_count_lock_dtls 
			           SET c2706_physical_qty = '' ,
			               c2706_history_fl = 'Y',
			               c2706_last_updated_by = p_user_id,
		                   c2706_last_updated_date = current_date
			         WHERE c2705_cyc_cnt_lock_id = p_cycle_count_id
		              AND c2706_cyc_cnt_lock_dtls_id = p_cc_lock_dtl_id
		              AND C2706_RECOUNT_FL ='Y'
		              AND c2706_void_fl IS NULL;
	              
             IF (v_counted_by = p_user_id AND v_recount_fl ='Y') THEN
	             p_out_msg :='Recount cannot be done by the same person';
	             RETURN;
             ELSE
	             UPDATE t2706_cycle_count_lock_dtls 
		           SET C2706_RECOUNT_FL = '' ,
		               c2706_last_updated_by = p_user_id,
	                   c2706_last_updated_date = current_date
		         WHERE c2705_cyc_cnt_lock_id = p_cycle_count_id
	              AND c2706_cyc_cnt_lock_dtls_id = p_cc_lock_dtl_id
	              AND C2706_RECOUNT_FL ='Y'
	              AND c2706_void_fl IS NULL;
	              p_out_msg:='';
             END IF;
	      END IF;
	      
	    --Tissue part validation for same lot  
	       SELECT C205_PRODUCT_MATERIAL INTO v_prod_material 
		    FROM t205_part_number 
		   WHERE C205_PART_NUMBER_ID = p_part_num;
		   
		   --100845 - Tissue parts
		   IF(v_prod_material = '100845') THEN
		      SELECT COUNT(1) INTO v_tissue_lot_cnt FROM t2708_cycle_count_scanned_lots
			  WHERE c2550_control_number = p_lot_num
            AND c205_part_number_id = p_part_num
            AND c2705_cyc_cnt_lock_id = p_cycle_count_id
            AND c2706_cyc_cnt_lock_dtls_id = p_cc_lock_dtl_id
            AND c2708_void_fl IS NULL;
			
			IF v_tissue_lot_cnt > 0 THEN
			 p_out_msg :='Cannot scan same Control Number for Tissue Part';
			 RETURN;
			END IF;
			
		   END IF;
		 
		   IF(v_counted_by = p_user_id AND v_recount_fl !='Y') THEN   
		        IF ( p_set_id IS NOT NULL ) THEN
		        
		            UPDATE t2706_cycle_count_lock_dtls
		            SET
		                c2706_physical_qty = '1',    
		                c2706_last_updated_by = p_user_id,
		                c2706_last_updated_date = current_date
		            WHERE
		                c207_set_id = p_set_id
		                AND c2705_cyc_cnt_lock_id = p_cycle_count_id
		                AND c2706_cyc_cnt_lock_dtls_id = p_cc_lock_dtl_id
		                AND c504_consignment_id = p_consign_id
		                AND c2706_void_fl IS NULL;
		
		        ELSE
		            UPDATE t2706_cycle_count_lock_dtls
		            SET
		                c2706_physical_qty = nvl(c2706_physical_qty, '0') + p_qty,
		                c2706_last_updated_by = p_user_id,
		                c2706_last_updated_date = current_date
		            WHERE
		                c205_part_number_id = p_part_num
		                AND c2705_cyc_cnt_lock_id = p_cycle_count_id
		                AND c2706_cyc_cnt_lock_dtls_id = p_cc_lock_dtl_id
		                AND c2706_void_fl IS NULL;
		
		        END IF;
		        
           ELSIF (v_counted_by <> p_user_id) THEN
                IF ( p_set_id IS NOT NULL ) THEN
		        
		            UPDATE t2706_cycle_count_lock_dtls
		            SET
		                c2706_physical_qty = '1', 
		                c101_counted_by = p_user_id,
		                c2706_counted_date = CURRENT_DATE,
		                c2706_last_updated_by = p_user_id,
		                c2706_last_updated_date = current_date
		            WHERE
		                c207_set_id = p_set_id
		                AND c2705_cyc_cnt_lock_id = p_cycle_count_id
		                AND c2706_cyc_cnt_lock_dtls_id = p_cc_lock_dtl_id
		                AND c504_consignment_id = p_consign_id
		                AND c2706_void_fl IS NULL;
		
		        ELSE
		            UPDATE t2706_cycle_count_lock_dtls
		            SET
		                c2706_physical_qty = nvl(c2706_physical_qty, '0') + p_qty,
		                c101_counted_by = p_user_id,
		                c2706_counted_date = CURRENT_DATE,
		                c2706_last_updated_by = p_user_id,
		                c2706_last_updated_date = current_date
		            WHERE
		                c205_part_number_id = p_part_num
		                AND c2705_cyc_cnt_lock_id = p_cycle_count_id
		                AND c2706_cyc_cnt_lock_dtls_id = p_cc_lock_dtl_id
		                AND c2706_void_fl IS NULL;
		        END IF;
        END IF;
    END gm_sav_counted_qty;
  /**********************************************************************************************
    * Description      : Procedure used to insert or update the scanned part qty to t2708_cycle_count_scanned_lots
    * Author           : tramasamy
 ***********************************************************************************************/

    PROCEDURE gm_sav_lot_details (
        p_cc_lock_id       IN   t2708_cycle_count_scanned_lots.c2705_cyc_cnt_lock_id%TYPE,
        p_cc_lock_dtl_id   IN   t2708_cycle_count_scanned_lots.c2706_cyc_cnt_lock_dtls_id%TYPE,
        p_cc_location_id   IN   t2708_cycle_count_scanned_lots.c5052_location_id%TYPE,
        p_part_num         IN   t2708_cycle_count_scanned_lots.c205_part_number_id%TYPE,
        p_lot_num          IN   t2708_cycle_count_scanned_lots.c2550_control_number%TYPE,
        p_qty              IN   t2708_cycle_count_scanned_lots.c2708_qty%TYPE,
        p_set_id           IN   t2706_cycle_count_lock_dtls.c207_set_id%TYPE,
        p_consign_id       IN   t2708_cycle_count_scanned_lots.c504_consignment_id%TYPE,
        p_user_id          IN   t2708_cycle_count_scanned_lots.c2708_scanned_by%TYPE
    ) AS
    BEGIN
        UPDATE t2708_cycle_count_scanned_lots
        SET
            c2705_cyc_cnt_lock_id = p_cc_lock_id,
            c2706_cyc_cnt_lock_dtls_id = p_cc_lock_dtl_id,
            c5052_location_id = p_cc_location_id,
            c205_part_number_id = p_part_num,
            c2550_control_number = p_lot_num,
            c2708_qty = nvl(c2708_qty, '0') + p_qty,
            c504_consignment_id = decode(p_consign_id, '', c504_consignment_id, p_consign_id),
            c2708_last_updated_by = p_user_id,
            c2708_last_updated_date = current_date
        WHERE
            c2550_control_number = p_lot_num
            AND c205_part_number_id = p_part_num
            AND c2705_cyc_cnt_lock_id = p_cc_lock_id
            AND c2706_cyc_cnt_lock_dtls_id = p_cc_lock_dtl_id
            AND c2708_void_fl IS NULL;

        IF ( SQL%rowcount = 0 ) THEN
            INSERT INTO t2708_cycle_count_scanned_lots (
                c2708_cyc_cnt_scan_id,
                c2705_cyc_cnt_lock_id,
                c2706_cyc_cnt_lock_dtls_id,
                c5052_location_id,
                c205_part_number_id,
                c2550_control_number,
                c2708_qty,
                c2708_scanned_by,
                c2708_scanned_date,
                c504_consignment_id
            ) VALUES (
                s2708_cycle_count_scanned_lots.NEXTVAL,
                p_cc_lock_id,
                p_cc_lock_dtl_id,
                p_cc_location_id,
                p_part_num,
                p_lot_num,
                p_qty,
                p_user_id,
                current_date,
                p_consign_id
            );

        END IF;

    END gm_sav_lot_details;
  /**********************************************************************************************
    * Description      : Procedure used to update the the physicsl qty and void the scanned part 
    * Author           : tramasamy
 ***********************************************************************************************/

    PROCEDURE gm_void_lot_details (
        p_cc_lock_dtl_id   IN   t2708_cycle_count_scanned_lots.c2706_cyc_cnt_lock_dtls_id%TYPE,
        p_cc_scan_id       IN   t2708_cycle_count_scanned_lots.c2708_cyc_cnt_scan_id%TYPE,
        p_cc_lock_id       IN   t2708_cycle_count_scanned_lots.c2705_cyc_cnt_lock_id%TYPE,
        p_part_num         IN   t2708_cycle_count_scanned_lots.c205_part_number_id%TYPE,
        p_qty              IN   t2708_cycle_count_scanned_lots.c2708_qty%TYPE,
        p_set_id           IN   t2706_cycle_count_lock_dtls.c207_set_id%TYPE,
        p_consign_id       IN   t2708_cycle_count_scanned_lots.c504_consignment_id%TYPE,
        p_user_id          IN   t2708_cycle_count_scanned_lots.c2708_scanned_by%TYPE
    ) AS
        v_qty          t2706_cycle_count_lock_dtls.c2706_physical_qty%TYPE;
        v_counted_qty   t2708_cycle_count_scanned_lots.c2708_qty%TYPE;
        v_count  NUMBER;
    BEGIN
        SELECT
            ( c2706_physical_qty - p_qty )
        INTO v_qty
        FROM
            t2706_cycle_count_lock_dtls
        WHERE
            c2706_cyc_cnt_lock_dtls_id = p_cc_lock_dtl_id 
	   -- AND C5052_LOCATION_ID = p_cc_location_id 
            AND c2705_cyc_cnt_lock_id = p_cc_lock_id
            AND c2706_void_fl IS NULL;

        IF ( v_qty <= 0 ) THEN
            v_counted_qty := '0';
        ELSIF ( v_qty > 0 ) THEN
            v_counted_qty := v_qty;
        END IF;

       
        UPDATE t2708_cycle_count_scanned_lots
        SET
            c2708_void_fl = 'Y',
            c2708_last_updated_by = p_user_id,
            c2708_last_updated_date = current_date
        WHERE
            c2708_cyc_cnt_scan_id = p_cc_scan_id
            AND c205_part_number_id = p_part_num
            AND c2706_cyc_cnt_lock_dtls_id = p_cc_lock_dtl_id
            AND c2705_cyc_cnt_lock_id = p_cc_lock_id
            AND c2708_void_fl IS NULL;
            
         IF(p_set_id IS NOT NULL) THEN 
           
           SELECT COUNT(1) INTO v_count FROM t2708_cycle_count_scanned_lots
            WHERE c2706_cyc_cnt_lock_dtls_id = p_cc_lock_dtl_id
              AND c2705_cyc_cnt_lock_id = p_cc_lock_id
              AND c2708_void_fl IS NULL;
          --  if we removed all scanned parts from grid will update physical qty value as 0 for set 
		           IF v_count = 0 THEN
			           UPDATE t2706_cycle_count_lock_dtls
			              SET c2706_physical_qty = '0',
			                  c2706_last_updated_by = p_user_id,
			                  c2706_last_updated_date = current_date
					    WHERE c2706_cyc_cnt_lock_dtls_id = p_cc_lock_dtl_id
					      AND c2705_cyc_cnt_lock_id = p_cc_lock_id
					      AND c2706_void_fl IS NULL;
			         END IF;
	     ELSE
			     UPDATE t2706_cycle_count_lock_dtls
			        SET c2706_physical_qty = v_counted_qty,
			            c2706_last_updated_by = p_user_id,
			            c2706_last_updated_date = current_date
			      WHERE c2706_cyc_cnt_lock_dtls_id = p_cc_lock_dtl_id
			        AND c2705_cyc_cnt_lock_id = p_cc_lock_id
			         AND c2706_void_fl IS NULL;
	     END IF;

    END gm_void_lot_details;
 /**********************************************************************************************
   * Description      : Procedure used to update the the physical qty and void the scanned part 
   * Author           : tramasamy
 ***********************************************************************************************/

    PROCEDURE gm_save_no_part (
        p_cc_lock_id       IN   t2708_cycle_count_scanned_lots.c2705_cyc_cnt_lock_id%TYPE,
        p_cc_lock_dtl_id   IN   t2708_cycle_count_scanned_lots.c2706_cyc_cnt_lock_dtls_id%TYPE,
        p_part_num         IN   t2708_cycle_count_scanned_lots.c205_part_number_id%TYPE,
        p_qty              IN   t2708_cycle_count_scanned_lots.c2708_qty%TYPE,
        p_set_id           IN   t2706_cycle_count_lock_dtls.c207_set_id%TYPE,
        p_consign_id       IN   t2708_cycle_count_scanned_lots.c504_consignment_id%TYPE,
        p_user_id          IN   t2708_cycle_count_scanned_lots.c2708_scanned_by%TYPE,
		p_out_msg          OUT   VARCHAR2
    ) AS
    v_lock_status  t2705_cycle_count_lock.C901_LOCK_STATUS%type;
    v_counted_by t2706_cycle_count_lock_dtls.c101_counted_by%type;
    v_recount_fl t2706_cycle_count_lock_dtls.C2706_RECOUNT_FL%type;
  	     
    BEGIN
	    
	    SELECT C901_LOCK_STATUS INTO v_lock_status FROM t2705_cycle_count_lock 
	     WHERE C2705_CYC_CNT_LOCK_ID = p_cc_lock_id
	      AND C2705_VOID_FL IS NULL;
	      
	      --get counted by and recunt flag for update
	        SELECT NVL(c101_counted_by,'-999') ,NVL(C2706_RECOUNT_FL,'N') INTO v_counted_by,v_recount_fl
	         FROM t2706_cycle_count_lock_dtls 
	        WHERE c2705_cyc_cnt_lock_id = p_cc_lock_id
              AND c2706_cyc_cnt_lock_dtls_id = p_cc_lock_dtl_id
              AND c2706_void_fl IS NULL;

	      --If status is recount to update the recount flag 
	     IF (v_lock_status='106830' AND v_recount_fl ='Y') THEN
	     
             IF (v_counted_by = p_user_id AND v_recount_fl ='Y') THEN
	             p_out_msg :='Recount cannot be done by the same person';
	             RETURN;
             ELSE
	             UPDATE t2706_cycle_count_lock_dtls 
		           SET C2706_RECOUNT_FL = '' ,
		               c2706_last_updated_by = p_user_id,
	                   c2706_last_updated_date = current_date
		         WHERE c2705_cyc_cnt_lock_id = p_cc_lock_id
	              AND c2706_cyc_cnt_lock_dtls_id = p_cc_lock_dtl_id
	              AND C2706_RECOUNT_FL ='Y'
	              AND c2706_void_fl IS NULL;
	              p_out_msg:='';
             END IF;
	      END IF;
	
	       IF(v_counted_by = p_user_id AND v_recount_fl !='Y') THEN   
		    
				UPDATE t2706_cycle_count_lock_dtls
				SET
					c2706_physical_qty = p_qty,    
					c2706_last_updated_by = p_user_id,
					c2706_last_updated_date = current_date
				WHERE
					c2705_cyc_cnt_lock_id = p_cc_lock_id
					AND c2706_cyc_cnt_lock_dtls_id = p_cc_lock_dtl_id
					AND c2706_void_fl IS NULL;
						
		   ELSIF (v_counted_by <> p_user_id) THEN
   
		            UPDATE t2706_cycle_count_lock_dtls
		            SET
		                c2706_physical_qty = p_qty, 
		                c101_counted_by = p_user_id,
		                c2706_counted_date = CURRENT_DATE,
		                c2706_last_updated_by = p_user_id,
		                c2706_last_updated_date = current_date
		            WHERE
		                c2705_cyc_cnt_lock_id = p_cc_lock_id
		                AND c2706_cyc_cnt_lock_dtls_id = p_cc_lock_dtl_id 
		                AND c2706_void_fl IS NULL;
		   END IF;

        UPDATE t2708_cycle_count_scanned_lots
        SET
            c2708_void_fl = 'Y',
            c2708_last_updated_by = p_user_id,
            c2708_last_updated_date = current_date
        WHERE
            c2706_cyc_cnt_lock_dtls_id = p_cc_lock_dtl_id
            AND c2705_cyc_cnt_lock_id = p_cc_lock_id
            AND c2708_void_fl IS NULL;

    END gm_save_no_part;
END gm_pkg_ac_cc_lot_txn;
/