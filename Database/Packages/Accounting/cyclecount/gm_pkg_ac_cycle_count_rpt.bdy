--@"C:\Database\Packages\Accounting\cyclecount\gm_pkg_ac_cycle_count_rpt.bdy";
CREATE OR REPLACE PACKAGE body gm_pkg_ac_cycle_count_rpt
IS
    /**********************************************************************************************
    * description      : This procedure will return the data based on the filter criteria for the Cycle Count Dashboard.
    * author           : mmuthusamy
    ***********************************************************************************************/
PROCEDURE gm_fetch_warehouse_dtls (
        p_cycle_count_lock_id IN t2705_cycle_count_lock.c2705_cyc_cnt_lock_id%type,
        p_warehouse           IN t2701_cycle_count_location.c2701_cyc_cnt_location_id%type,
        p_fromdate            IN t2705_cycle_count_lock.c2705_lock_date%type,
        p_todate              IN t2705_cycle_count_lock.c2705_lock_date%type,
        p_status              IN t2705_cycle_count_lock.c901_lock_status%type,
        p_out_cursor OUT types.cursor_type)
AS
    v_plant_id t5040_plant_master.c5040_plant_id%type;
    v_date_fmt VARCHAR2 (20) ;
BEGIN
     SELECT get_plantid_frm_cntx (), get_compdtfmt_frm_cntx ()
       INTO v_plant_id, v_date_fmt
       FROM dual;
    --
    OPEN p_out_cursor FOR
     SELECT t2700.c2700_cycle_count_year YEAR, get_code_name (t2701.c901_warehouse_location) warehouse,
    t2705.c2705_cyc_cnt_lock_id lock_id, t2705.c2705_lock_date count_date, t2705.c2705_schedule_date schedule_date
  , get_user_name (t2705.c2705_approved_by) approved_by, t2705.c2705_approved_date approved_date, get_code_name (
    t2705.c901_lock_status) status, t2705.c2705_last_updated_by last_updated_by, TO_CHAR (t2705.c2705_last_updated_date
    , v_date_fmt ||' HH:MI:SS AM') updated_date, t2705.c901_lock_status statusid
   FROM t2705_cycle_count_lock t2705, t2700_cycle_count t2700, t2701_cycle_count_location t2701  
  WHERE t2700.c2700_cycle_count_id      = t2701.c2700_cycle_count_id
    AND t2701.c2701_cyc_cnt_location_id = t2705.c2701_cyc_cnt_location_id
    AND t2705.c2705_cyc_cnt_lock_id     = NVL (p_cycle_count_lock_id, t2705.c2705_cyc_cnt_lock_id)
    AND t2701.c2701_cyc_cnt_location_id = DECODE (p_warehouse, 0, t2701.c2701_cyc_cnt_location_id, p_warehouse)
    AND TRUNC (t2705.c2705_lock_date)  >= TRUNC (DECODE (p_fromdate, NULL, t2705.c2705_lock_date, p_fromdate))
    AND TRUNC (t2705.c2705_lock_date)  <= TRUNC (DECODE (p_todate, NULL, t2705.c2705_lock_date, p_todate))
    AND t2705.c901_lock_status          = DECODE (p_status, 0, t2705.c901_lock_status, p_status)
    AND t2700.c5040_plant_id            = v_plant_id
    AND t2700.c2700_void_fl            IS NULL
    AND t2701.c2701_void_fl            IS NULL
    AND t2705.c2705_void_fl            IS NULL
 ORDER BY warehouse asc, t2705.c2705_lock_date desc;

END gm_fetch_warehouse_dtls;
/**********************************************************************************************
* description      : procedure used to fetch the warehouse type based on plant
* author           : mmuthusamy
***********************************************************************************************/
PROCEDURE gm_fetch_warehouse_type (
        p_out_cursor OUT types.cursor_type)
AS
    v_plant_id t5040_plant_master.c5040_plant_id%type;
BEGIN
    --
     SELECT get_plantid_frm_cntx ()
       INTO v_plant_id
       FROM dual;
    --
    OPEN p_out_cursor FOR
    -- warehouse
     SELECT t2701.c2701_cyc_cnt_location_id id, get_code_name (t2701.c901_warehouse_location) warehouse
   FROM t2700_cycle_count t2700, t2701_cycle_count_location t2701
  WHERE t2700.c2700_cycle_count_id   = t2701.c2700_cycle_count_id
    AND t2700.c5040_plant_id         = v_plant_id
    AND t2700.c2700_cycle_count_year = TO_CHAR (CURRENT_DATE, 'yyyy')
    AND t2701.c2701_void_fl         IS NULL
    AND t2700.c2700_void_fl         IS NULL
 ORDER BY warehouse;
 
END gm_fetch_warehouse_type;

/**********************************************************************************************
* description      : procedure used to fetch the locked information (header and details sections)
* author           : mmuthusamy
***********************************************************************************************/
PROCEDURE gm_fch_cycle_count_record_dtls (
        p_cycle_id IN t2706_cycle_count_lock_dtls.c2705_cyc_cnt_lock_id%type,
        p_action IN VARCHAR2,
        p_out_header_dtls OUT types.cursor_type,
        p_out_locked_dtls OUT types.cursor_type)
AS
v_count_type NUMBER;
v_lot_track_fl VARCHAR2(3);
BEGIN
    gm_pkg_ac_cycle_count_rpt.gm_fch_cycle_count_header_info (p_cycle_id, p_out_header_dtls) ;
    -- to call part/set procedure
     SELECT t2701.C901_COUNT_TYPE INTO v_count_type
   FROM T2705_CYCLE_COUNT_LOCK t2705, T2701_CYCLE_COUNT_LOCATION t2701
  WHERE t2701.C2701_CYC_CNT_LOCATION_ID = t2705.C2701_CYC_CNT_LOCATION_ID
    AND t2705.C2705_CYC_CNT_LOCK_ID     = p_cycle_id
    AND t2705.C2705_VOID_FL            IS NULL
    AND t2701.C2701_VOID_FL            IS NULL;
    --
    -- PC-3495 Cycle count - Adding lots in print paperwork
	BEGIN
 	   SELECT NVL(t2705.C2705_LOT_TRACK_FL,'N') INTO v_lot_track_fl
   		 FROM T2705_CYCLE_COUNT_LOCK t2705
  		WHERE t2705.C2705_CYC_CNT_LOCK_ID	= p_cycle_id
    	  AND t2705.C2705_VOID_FL		   IS NULL;
	    EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
		v_lot_track_fl := 'N';
		
	END;
		
    IF p_action IS NOT NULL AND p_action = 'PRINT'
    THEN
    	-- PC-3495 Cycle count - Adding lots in print paperwork
    	IF v_lot_track_fl = 'Y'
    	THEN
		gm_pkg_ac_cycle_count_rpt.gm_fch_cycle_count_print_dtls_by_lot (p_cycle_id, p_out_locked_dtls) ;
     	ELSE
    	gm_pkg_ac_cycle_count_rpt.gm_fch_cycle_count_print_dtls (p_cycle_id, p_out_locked_dtls) ;
    	END IF;
    ELSE
	    IF v_count_type = 106815 THEN -- Parts
	    	gm_pkg_ac_cycle_count_rpt.gm_fch_cycle_count_locked_dtls (p_cycle_id, p_out_locked_dtls) ;
	    ELSE
			gm_pkg_ac_cycle_count_rpt.gm_fch_cycle_count_locked_set (p_cycle_id, p_out_locked_dtls) ;    
	    END IF;
	    --
    END IF;
END gm_fch_cycle_count_record_dtls;
/**********************************************************************************************
* description      : procedure used to fetch the header sections details
* author           : mmuthusamy
***********************************************************************************************/
PROCEDURE gm_fch_cycle_count_header_info (
        p_cycle_id IN t2706_cycle_count_lock_dtls.c2705_cyc_cnt_lock_id%type,
        p_out_header_dtls OUT types.cursor_type)
AS
    v_date_fmt VARCHAR2 (20) ;
BEGIN
     SELECT get_compdtfmt_frm_cntx ()
       INTO v_date_fmt
       FROM dual;
    --
    OPEN p_out_header_dtls FOR SELECT t2705.c2705_cyc_cnt_lock_id cyclecountid, get_code_name (t2701.c901_warehouse_location) warehousename,
    t2705.c2701_cyc_cnt_location_id warehouseid, TO_CHAR (t2705.c2705_lock_date, v_date_fmt ||' HH:MI:SS AM') lockdate,
    TO_CHAR (t2705.c2705_schedule_date, v_date_fmt) scheduledate, get_code_name (t2705.c901_lock_status) status,
    t2705.c901_lock_status statusid, get_user_name (t2705.c2705_approved_by) approvedby, TO_CHAR (
    t2705.c2705_approved_date, v_date_fmt) approveddate, get_user_name (t2705.c2705_last_updated_by) updatedby, TO_CHAR
    (t2705.c2705_last_updated_date, v_date_fmt) updateddate, NVL (t2706.counted_qty, 0) countedqty, NVL (
    t2706.total_cnt, 0) totalcount, t2701.c901_warehouse_location warehousetype, t2701.c901_count_type counttype
    , get_code_name_alt (t2701.c901_warehouse_location) location_code_alt_nm
  , t2701.c901_lock_type locktype, get_rule_value (t2701.c901_warehouse_location, 'CYC_CNT_INV_MAP_ID') invmapid
  , TO_CHAR (t2705.c2705_lock_date, v_date_fmt) transLogDate, t2705.C2705_LOT_TRACK_FL lotfl                                              --- t2705.C2705_LOT_TRACK_FL lotfl used show L icon added in PC-3498 - Cycle count-Changes in cycle count recording
   FROM t2705_cycle_count_lock t2705, t2701_cycle_count_location t2701, (
         SELECT t2706.c2705_cyc_cnt_lock_id lock_id, SUM (DECODE (t2706.c2706_recount_fl, 'Y',0, DECODE(t2706.c2706_physical_qty, NULL, 0, 1))) counted_qty,
            COUNT (1) total_cnt
            --into v_record_cnt
           FROM t2706_cycle_count_lock_dtls t2706
          WHERE t2706.c2705_cyc_cnt_lock_id = p_cycle_id
            AND t2706.c2706_void_fl        IS NULL
       GROUP BY t2706.c2705_cyc_cnt_lock_id
    )
    t2706
  WHERE t2701.c2701_cyc_cnt_location_id = t2705.c2701_cyc_cnt_location_id
    AND t2705.c2705_cyc_cnt_lock_id     = t2706.lock_id (+)
    AND t2705.c2705_cyc_cnt_lock_id     = p_cycle_id
    AND t2701.c2701_void_fl            IS NULL
    AND t2705.c2705_void_fl            IS NULL;
END gm_fch_cycle_count_header_info;
/**********************************************************************************************
* description      : procedure used to fetch the locked details sections
* author           : mmuthusamy
***********************************************************************************************/
PROCEDURE gm_fch_cycle_count_locked_dtls (
        p_cycle_id IN t2706_cycle_count_lock_dtls.c2705_cyc_cnt_lock_id%type,
        p_out_locked_dtls OUT types.cursor_type)
AS
v_date_fmt VARCHAR2 (20) ;
BEGIN
	--
	SELECT get_compdtfmt_frm_cntx ()
       INTO v_date_fmt
       FROM dual;
    --   
    OPEN p_out_locked_dtls FOR SELECT t2706.c205_part_number_id pnum, t205.c205_part_num_desc p_desc, t5052.c5052_location_cd location
  , t2703.c2703_rank rank, t2706.c2706_history_fl history_fl, t2706.c2706_system_qty system_qty
  , DECODE(t2705.c901_lock_status, 106830, DECODE(t2706.c2706_recount_fl,'Y', NULL, t2706.c2706_physical_qty), t2706.c2706_physical_qty) counted_qty, get_user_name (t2706.c101_counted_by) counted_by
  ,TO_CHAR (t2706.c2706_counted_date , v_date_fmt ||' HH:MI:SS AM') counted_date, c2706_cyc_cnt_lock_dtls_id lock_dtls_id, t2703.c2703_total_count total_cnt
  , NVL (t2706.c2706_physical_qty, 0) - NVL (t2706.c2706_override_qty, t2706.c2706_system_qty) variance_qty, t2706.c2706_comments comments
    -- set details
  , t2706.c504_consignment_id cn_id, t2706.c207_set_id set_id, t2706.c207_set_name set_name
  , t2706.c5010_tag_id tag_id, t2706.c504_etch_id etch_id, t2706.c2706_cn_location cn_location
  , t2706.c2706_cn_status cn_status, NVL(t2706.c2706_recount_fl,'N') recount_fl, t2706.c2706_override_qty override_qty
  ,c2706_new_part_fl part_fl
  , t2706.c2706_cycle_count_cost cc_cost
  , t2706.c2706_cycle_count_local_cost cc_local_cost
   FROM t2706_cycle_count_lock_dtls t2706, t2705_cycle_count_lock t2705, t205_part_number t205
  , t2703_cycle_count_rank t2703, t5052_location_master t5052
  WHERE t2705.c2705_cyc_cnt_lock_id = t2706.c2705_cyc_cnt_lock_id
  	AND t2706.C5052_LOCATION_ID  =  t5052.C5052_LOCATION_ID (+)
    AND t205.c205_part_number_id    = t2706.c205_part_number_id
    AND t2705.c2700_cycle_count_id = t2703.c2700_cycle_count_id (+)
    AND t205.c205_part_number_id    = t2703.C205_PART_NUMBER_ID (+)
    AND t2705.c2705_cyc_cnt_lock_id = p_cycle_id
    AND t2706.c2706_void_fl        IS NULL
    AND t2705.c2705_void_fl        IS NULL
    AND t2703.c2703_void_fl        IS NULL
    AND t5052.c5052_void_fl		   IS NULL
ORDER BY t2706.c2706_recount_fl,location, t2706.c205_part_number_id;
END gm_fch_cycle_count_locked_dtls;

/**********************************************************************************************
    * description      : procedure used to fetch the txn details associated to the approved Cycle Count for the warehouse.
    * author           : mmuthusamy
    ***********************************************************************************************/
	PROCEDURE gm_fch_txn_ref_dtls (
	        p_cycle_id IN T2706_CYCLE_COUNT_LOCK_DTLS.C2705_CYC_CNT_LOCK_ID%TYPE,
	        p_out_txn_ref_dtls OUT Types.cursor_type)
	        AS
	        v_date_fmt VARCHAR2 (20) ;
	        BEGIN
		        --
		        SELECT get_compdtfmt_frm_cntx ()
       			INTO v_date_fmt
       			FROM dual;
       			--
		        
		        OPEN p_out_txn_ref_dtls 
		        FOR SELECT C2705_CYC_CNT_LOCK_ID lock_id, C2707_TXN_ID txn_id
   , get_user_name (C2707_LAST_UPDATED_BY) updated_by, to_char(c2707_last_updated_date, v_date_fmt ||' HH:MI:SS AM') updated_date
   FROM T2707_CYCLE_COUNT_TXN_REF
   where C2705_CYC_CNT_LOCK_ID = p_cycle_id
   AND C2707_VOID_FL IS NULL;
		        
		        END gm_fch_txn_ref_dtls;

	FUNCTION get_location_id_from_cd (
	p_warehouse_id IN T5052_LOCATION_MASTER.C5051_INV_WAREHOUSE_ID%TYPE,
	p_location_cd IN T5052_LOCATION_MASTER.C5052_LOCATION_CD%TYPE,
	p_plant_id IN T5052_LOCATION_MASTER.c5040_plant_id%TYPE
	) RETURN VARCHAR2
	AS
	v_location_id t5052_location_master.c5052_location_id%TYPE;
	BEGIN
		
		SELECT C5052_LOCATION_ID 
				  INTO v_location_id 
				  FROM T5052_LOCATION_MASTER t5052 
				 WHERE t5052.c5051_inv_warehouse_id =  p_warehouse_id
				 AND t5052.c5052_location_cd  = p_location_cd
				 AND t5052.c5040_plant_id = p_plant_id
				 AND t5052.c5052_void_fl is NULL;  
			RETURN v_location_id;	 
			EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
			v_location_id := NULL;
		RETURN v_location_id;
			
		END get_location_id_from_cd;

		
		/**********************************************************************************************
* description      : procedure used to fetch the locked details sections
* author           : mmuthusamy
***********************************************************************************************/
PROCEDURE gm_fch_cycle_count_locked_set (
        p_cycle_id IN t2706_cycle_count_lock_dtls.c2705_cyc_cnt_lock_id%type,
        p_out_locked_dtls OUT types.cursor_type)
AS
v_date_fmt VARCHAR2 (20) ;
BEGIN
	--
	SELECT get_compdtfmt_frm_cntx ()
       INTO v_date_fmt
       FROM dual;
    --   
    OPEN p_out_locked_dtls FOR SELECT t2706.C504_CONSIGNMENT_ID cn_id, t2706.C207_SET_ID set_id, t2706.C207_SET_NAME set_name
  , gm_pkg_op_set_pick_rpt.GET_LOC_CD_FROM_ID (t2706.c5052_location_id) location, t2706.C504_ETCH_ID etch_id,
    t2706.C5010_TAG_ID tag_id, t2706.C2706_CN_LOCATION cn_location, t2706.C2706_CN_STATUS cn_status
  , t2706.c2706_history_fl history_fl, t2706.c2706_system_qty system_qty, DECODE (t2705.c901_lock_status, 106830,
    DECODE (t2706.c2706_recount_fl, 'Y', NULL, t2706.c2706_physical_qty), t2706.c2706_physical_qty) counted_qty,
    get_user_name (t2706.c101_counted_by) counted_by, TO_CHAR (t2706.c2706_counted_date, v_date_fmt ||' HH:MI:SS AM') counted_date, c2706_cyc_cnt_lock_dtls_id
    lock_dtls_id, NVL (t2706.c2706_physical_qty, 0) - NVL (t2706.c2706_override_qty, t2706.c2706_system_qty)
    variance_qty, t2706.c2706_comments comments, NVL (t2706.c2706_recount_fl, 'N') recount_fl
  , t2706.c2706_override_qty override_qty
   FROM t2706_cycle_count_lock_dtls t2706, t2705_cycle_count_lock t2705
  WHERE t2705.c2705_cyc_cnt_lock_id = t2706.c2705_cyc_cnt_lock_id
    AND t2705.c2705_cyc_cnt_lock_id = p_cycle_id
    AND t2706.c2706_void_fl        IS NULL
    AND t2705.c2705_void_fl        IS NULL
ORDER BY t2706.c2706_recount_fl,t2706.C504_CONSIGNMENT_ID;
END gm_fch_cycle_count_locked_set;

	/**********************************************************************************************
    * description      : procedure used to fetch the locked sheet print section
    * author           : mmuthusamy
    ***********************************************************************************************/
	PROCEDURE gm_fch_cycle_count_print_dtls (
	        p_cycle_id IN t2706_cycle_count_lock_dtls.c2705_cyc_cnt_lock_id%TYPE,
	        p_out_print_dtls OUT Types.cursor_type)
	   AS
v_date_fmt VARCHAR2 (20) ;
BEGIN
	--
	SELECT get_compdtfmt_frm_cntx ()
       INTO v_date_fmt
       FROM dual;
    --   
    OPEN p_out_print_dtls FOR SELECT t2706.c205_part_number_id pnum, t205.c205_part_num_desc p_desc, t5052.c5052_location_cd location
  , t2703.c2703_rank rank, t2706.c2706_history_fl history_fl, t2706.c2706_system_qty system_qty
  , DECODE(t2705.c901_lock_status, 106830, DECODE(t2706.c2706_recount_fl,'Y', NULL, t2706.c2706_physical_qty), t2706.c2706_physical_qty) counted_qty, get_user_name (t2706.c101_counted_by) counted_by
  ,TO_CHAR (t2706.c2706_counted_date , v_date_fmt ||' HH:MI:SS AM') counted_date, c2706_cyc_cnt_lock_dtls_id lock_dtls_id, t2703.c2703_total_count total_cnt
  , NVL (t2706.c2706_physical_qty, 0) - NVL (t2706.c2706_override_qty, t2706.c2706_system_qty) variance_qty, t2706.c2706_comments comments
    -- set details
  , t2706.c504_consignment_id cn_id, t2706.c207_set_id set_id, t2706.c207_set_name set_name
  , t2706.c5010_tag_id tag_id, t2706.c504_etch_id etch_id, t2706.c2706_cn_location cn_location
  , t2706.c2706_cn_status cn_status, NVL(t2706.c2706_recount_fl,'N') recount_fl, t2706.c2706_override_qty override_qty
   FROM t2706_cycle_count_lock_dtls t2706, t2705_cycle_count_lock t2705, t205_part_number t205
  , t2703_cycle_count_rank t2703, t5052_location_master t5052
  WHERE t2705.c2705_cyc_cnt_lock_id = t2706.c2705_cyc_cnt_lock_id
  	AND t2706.C5052_LOCATION_ID  =  t5052.C5052_LOCATION_ID (+)
    AND t205.c205_part_number_id    = t2706.c205_part_number_id
    AND t2705.c2700_cycle_count_id = t2703.c2700_cycle_count_id (+)
    AND t205.c205_part_number_id    = t2703.C205_PART_NUMBER_ID (+)
    AND t2705.c2705_cyc_cnt_lock_id = p_cycle_id
    -- Status is Recount then only fetch the Recount Data
    AND DECODE(t2705.c901_lock_status, 106830, NVL(t2706.c2706_recount_fl,'N'), 'N') = DECODE(t2705.c901_lock_status, 106830, 'Y', 'N')
    AND t2706.c2706_void_fl        IS NULL
    AND t2705.c2705_void_fl        IS NULL
    AND t2703.c2703_void_fl        IS NULL
    AND t5052.c5052_void_fl		   IS NULL
ORDER BY location, t2706.c205_part_number_id;
	--
END gm_fch_cycle_count_print_dtls;
		
/**********************************************************************************************
 * description      : procedure used to fetch Add Part Details
 * author           : pvigneshwaran
 ***********************************************************************************************/
PROCEDURE gm_fch_add_parts_dtls(
     p_cycle_count_lock_id IN t2706_cycle_count_lock_dtls.c2705_cyc_cnt_lock_id%type,
     p_out_cursor OUT types.cursor_type)
 AS               
BEGIN
    OPEN p_out_cursor FOR
    --Fetch add part details
 SELECT  t5052.c5052_location_cd location_code,t2706.C5052_LOCATION_ID location_id,RTRIM (XMLAGG (XMLELEMENT (e,decode(t2706.c2706_new_part_fl ,'Y', t2706.c205_part_number_id || ',' ,NULL))) .EXTRACT ('//text()'), ',') pnum
   FROM  t2706_cycle_count_lock_dtls t2706, t2705_cycle_count_lock t2705, t205_part_number t205,
   		 t2703_cycle_count_rank t2703, t5052_location_master t5052
  WHERE  t2705.c2705_cyc_cnt_lock_id = t2706.c2705_cyc_cnt_lock_id
    AND  t2706.C5052_LOCATION_ID     = t5052.C5052_LOCATION_ID (+)
    AND  t205.c205_part_number_id    = t2706.c205_part_number_id
    AND  t2705.c2700_cycle_count_id = t2703.c2700_cycle_count_id (+)
    AND  t205.c205_part_number_id    = t2703.C205_PART_NUMBER_ID (+)
    AND  t2705.c2705_cyc_cnt_lock_id = p_cycle_count_lock_id
    AND  t2706.c2706_void_fl        IS NULL
    AND  t2705.c2705_void_fl        IS NULL
    AND  t2703.c2703_void_fl        IS NULL
    AND  t5052.c5052_void_fl        IS NULL
 GROUP BY t5052.c5052_location_cd,t2706.C5052_LOCATION_ID
 ORDER BY location_code;
END gm_fch_add_parts_dtls;


 /**********************************************************************************************
* description      : Procedure used to fetch full details report
* author           : Mani
*  ***********************************************************************************************/        
PROCEDURE gm_fch_full_data_rpt (
    							p_warehouse_id IN T2705_CYCLE_COUNT_LOCK.C2701_CYC_CNT_LOCATION_ID%TYPE,
    							p_part_number IN T2706_CYCLE_COUNT_LOCK_DTLS.C205_PART_NUMBER_ID%TYPE,
    							p_from_date IN T2706_CYCLE_COUNT_LOCK_DTLS.C2706_COUNTED_DATE%TYPE,
    							p_to_date IN T2706_CYCLE_COUNT_LOCK_DTLS.C2706_COUNTED_DATE%TYPE,
    							p_cycle_count_lock_id IN T2705_CYCLE_COUNT_LOCK.c2705_cyc_cnt_lock_id%TYPE,
    							p_out_full_data_dtls OUT Types.cursor_type
    )
    AS
    	v_date_fmt VARCHAR2 (20) ;
    	v_plant_id   t5040_plant_master.c5040_plant_id%TYPE;
    BEGIN 
	 --
		SELECT get_compdtfmt_frm_cntx (), get_plantid_frm_cntx ()
       	INTO v_date_fmt, v_plant_id
       	FROM dual;
	    OPEN p_out_full_data_dtls
	    FOR
	    
	    SELECT t901_warehouse.c901_code_nm Location,
	    	  t2705.C2705_CYC_CNT_LOCK_ID Lock_id, 
	    	  NVL(t2706.C205_PART_NUMBER_ID,t2706.C504_CONSIGNMENT_ID) Part_Number,
	    	  t205.C205_PART_NUM_DESC p_desc,
	    	  t2706.C2706_CYCLE_COUNT_COST p_cost,   
	    	  t2706.C2706_CYCLE_COUNT_LOCAL_COST local_cost,
  			  t2706.C2706_SYSTEM_QTY System_Qty, 
  			  NVL((t2706.C2706_SYSTEM_QTY * t2706.C2706_CYCLE_COUNT_COST),0) ext_sys_cost,
  			  t2706.C2706_PHYSICAL_QTY countedqty, 
  			  NVL((t2706.C2706_PHYSICAL_QTY  * t2706.C2706_CYCLE_COUNT_COST),0) ext_cnt_cost,
    		  NVL (t2706.c2706_physical_qty, 0) - NVL (t2706.c2706_override_qty, t2706.c2706_system_qty) variance_qty,  
    		  abs(NVL (t2706.c2706_physical_qty, 0) - NVL (t2706.c2706_override_qty, t2706.c2706_system_qty)) abs_variance_qty,
    		  NVL((( NVL (t2706.c2706_physical_qty, 0) - NVL (t2706.c2706_override_qty, t2706.c2706_system_qty)) * t2706.C2706_CYCLE_COUNT_COST),0) ext_var_cost,
    		  abs(NVL((( NVL (t2706.c2706_physical_qty, 0) - NVL (t2706.c2706_override_qty, t2706.c2706_system_qty)) * t2706.C2706_CYCLE_COUNT_COST),0)) abs_ext_var_cost,
    		  --
    		  NVL((t2706.C2706_SYSTEM_QTY * t2706.C2706_CYCLE_COUNT_LOCAL_COST),0) local_ext_sys_cost,
    		  NVL((t2706.C2706_PHYSICAL_QTY  * t2706.C2706_CYCLE_COUNT_LOCAL_COST),0) local_ext_cnt_cost,
    		  NVL((( NVL (t2706.c2706_physical_qty, 0) - NVL (t2706.c2706_override_qty, t2706.c2706_system_qty)) * t2706.C2706_CYCLE_COUNT_LOCAL_COST),0) local_ext_var_cost,
    		  abs(NVL((( NVL (t2706.c2706_physical_qty, 0) - NVL (t2706.c2706_override_qty, t2706.c2706_system_qty)) * t2706.C2706_CYCLE_COUNT_LOCAL_COST),0)) abs_local_ext_var_cost,
    		  --
    		  get_code_name_alt (t2706.c901_owner_currency) owner_ccy, get_code_name_alt (t2706.C901_LOCAL_COMPANY_CURRENCY) local_ccy, 
    		  --
 			  t2706.c2706_history_fl recount_fl,   
  			  t2706.C2706_OVERRIDE_QTY override_qty ,  
   			  t2706.C2706_TXN_ID txn_id ,
  			  t2706.C2706_COMMENTS comments   ,
   			  get_user_name(t2706.C101_COUNTED_BY) counted_by  , 
   			  TO_CHAR (t2706.C2706_COUNTED_DATE, v_date_fmt) counted_date  , 
  		  	  get_user_name (t2705.C2705_APPROVED_BY) Approved_By, 
	    	  TO_CHAR (t2705.C2705_APPROVED_DATE, v_date_fmt) Approved_Date,
	    	  TO_CHAR (t2705.c2705_lock_date, v_date_fmt) lockdate	    	  
   FROM 	  T2706_CYCLE_COUNT_LOCK_DTLS t2706, 
   			  T2705_CYCLE_COUNT_LOCK t2705, 
   			  T2701_CYCLE_COUNT_LOCATION t2701,   
   			  T2700_CYCLE_COUNT t2700,
   			  T205_PART_NUMBER t205,
   			  T901_CODE_LOOKUP t901_warehouse
  WHERE 	  t2701.C2701_CYC_CNT_LOCATION_ID = t2705.C2701_CYC_CNT_LOCATION_ID   
    AND 	  t2705.C2705_CYC_CNT_LOCK_ID     = t2706.C2705_CYC_CNT_LOCK_ID   
    AND 	  t2700.C2700_CYCLE_COUNT_ID = t2701.C2700_CYCLE_COUNT_ID
    AND  	  t2706.c205_part_number_id   = t205.c205_part_number_id (+)
    AND       t2701.C901_WAREHOUSE_LOCATION = t901_warehouse.C901_CODE_ID 
    AND 	  t2705.C901_LOCK_STATUS = 106831   -- Approve
    AND       t2700.c5040_plant_id = v_plant_id
    --
    AND 	  t2705.C2701_CYC_CNT_LOCATION_ID = DECODE (p_warehouse_id, 0, t2705.C2701_CYC_CNT_LOCATION_ID,p_warehouse_id)
   AND regexp_like (NVL(t2706.c205_part_number_id, '-999'), NVL(REGEXP_REPLACE(p_part_number,'[+]','\+'),NVL(REGEXP_REPLACE(t2706.c205_part_number_id,'[+]','\+'), '-999')))
    AND 	  TRUNC (t2705.C2705_APPROVED_DATE)  >= TRUNC (DECODE (p_from_date, NULL, t2705.C2705_APPROVED_DATE, p_from_date))
    AND 	  TRUNC (t2705.C2705_APPROVED_DATE)  <= TRUNC (DECODE (p_to_date, NULL, t2705.C2705_APPROVED_DATE, p_to_date))
    AND 	  t2705.C2705_CYC_CNT_LOCK_ID = NVL (p_cycle_count_lock_id, t2705.C2705_CYC_CNT_LOCK_ID)
    --
    AND 	  t2700.C2700_VOID_FL IS NULL   
    AND 	  t2705.C2705_VOID_FL            IS NULL   
    AND       t2706.C2706_VOID_FL            IS NULL   
    AND       t2701.C2701_VOID_FL            IS NULL   
	ORDER BY Location,t2706.C205_PART_NUMBER_ID,t2706.C504_CONSIGNMENT_ID;
END gm_fch_full_data_rpt;

/**********************************************************************************************
* description      : Procedure used to fetch Rank Details List
* author           : tramasamy
*  ***********************************************************************************************/ 
PROCEDURE gm_fch_cycle_count_rank(
     p_cycle_id        IN         t2700_cycle_count.c2700_cycle_count_id%TYPE,
     p_out_rank_list      OUT         Types.cursor_type
   )
 AS               
   BEGIN
	   OPEN p_out_rank_list FOR   
	   SELECT DISTINCT T2703.C2703_RANK RANK 
	     FROM T2703_CYCLE_COUNT_RANK T2703
	     WHERE T2703.c2700_cycle_count_id = p_cycle_id
	      AND  t2703.c2703_void_fl        IS NULL
          ORDER BY RANK ASC;
	   
END gm_fch_cycle_count_rank;

/**********************************************************************************************
* description      : Procedure used to fetch Rank Report Details
* author           : tramasamy
*  ***********************************************************************************************/  
PROCEDURE gm_fch_rank_rpt(
	p_cycle_id      IN   T2703_CYCLE_COUNT_RANK.c2700_cycle_count_id%TYPE,
	p_rank          IN   T2703_CYCLE_COUNT_RANK.C2703_RANK%TYPE,
	P_part_number   IN   T2703_CYCLE_COUNT_RANK.C205_PART_NUMBER_Id%TYPE,
	p_project_id    IN   T202_PROJECT.C202_PROJECT_ID%TYPE,
	p_part_family	IN   T901_CODE_LOOKUP.C901_CODE_ID%TYPE,
    p_out_rank_rpt_dtls   OUT  Types.cursor_type
)

AS
 v_date_fmt VARCHAR2 (20);
 v_plant_id   t5040_plant_master.c5040_plant_id%TYPE;
 BEGIN
	 SELECT get_compdtfmt_frm_cntx (), get_plantid_frm_cntx ()
       	INTO v_date_fmt, v_plant_id
       	FROM dual;
      
	 OPEN p_out_rank_rpt_dtls FOR
	 SELECT T2703.C205_PART_NUMBER_ID PARTNO, T2703.C2703_RANK RANK ,t2700.c2700_CYCLE_COUNT_YEAR YEAR 
	 , get_user_name(T2703.C2703_LAST_UPDATED_BY) LASTUPDATEDBY, TO_CHAR (T2703.c2703_last_updated_date,v_date_fmt) LASTUPDATEDDATE
	 , t205.C205_PART_NUM_DESC part_desc
	 
	 , t202.C202_PROJECT_NM project_name, t202.C202_PROJECT_ID project_id
	 , t901_prod_family.C901_CODE_NM part_family
   FROM T2703_CYCLE_COUNT_RANK T2703, T2700_CYCLE_COUNT t2700, T205_PART_NUMBER t205, T202_PROJECT t202
   , T901_CODE_LOOKUP t901_prod_family
   WHERE t2700.C2700_CYCLE_COUNT_ID = t2703.C2700_CYCLE_COUNT_ID
   AND t202.C202_PROJECT_ID = t205.C202_PROJECT_ID
   AND t205.C205_PART_NUMBER_ID = T2703.C205_PART_NUMBER_ID
   AND t205.C205_PRODUCT_FAMILY = t901_prod_family.C901_CODE_ID
   --
   AND T2700.C5040_PLANT_ID   = v_plant_id
   AND t2700.C2700_CYCLE_COUNT_ID = decode(p_cycle_id, '0', t2700.C2700_CYCLE_COUNT_ID ,p_cycle_id)
   AND t2703.C2703_RANK = decode (p_rank,'0',t2703.C2703_RANK, p_rank)
   AND t202.C202_PROJECT_ID = decode (p_project_id,'0',t202.C202_PROJECT_ID, p_project_id)
   AND regexp_like (T2703.C205_PART_NUMBER_ID, NVL (REGEXP_REPLACE(p_part_number,'[+]','\+'), REGEXP_REPLACE(T2703.C205_PART_NUMBER_ID,'[+]','\+')))
   AND t901_prod_family.C901_CODE_ID = DECODE(p_part_family, '0', t901_prod_family.C901_CODE_ID, p_part_family)
   AND t2700.C2700_VOID_FL    IS NULL
  AND T2703.C2703_VOID_FL    IS NULL
  ORDER BY T2703.C2703_RANK, T2703.C205_PART_NUMBER_ID;

END gm_fch_rank_rpt;

PROCEDURE gm_fch_cycle_count_year (
    p_out_cycle_year OUT types.cursor_type
   ) 
   
   AS
   v_plant_id   t5040_plant_master.c5040_plant_id%TYPE;	
   BEGIN
	   SELECT get_plantid_frm_cntx ()
       	INTO v_plant_id
       	FROM dual;
      
       	 OPEN p_out_cycle_year FOR
	   SELECT  distinct t2700.C2700_CYCLE_COUNT_ID ID , t2700.C2700_CYCLE_COUNT_YEAR YEAR
	   FROM T2700_CYCLE_COUNT t2700 , T2701_CYCLE_COUNT_LOCATION T2701
	   WHERE t2700.C2700_CYCLE_COUNT_ID =  T2701.C2700_CYCLE_COUNT_ID
	   AND T2700.C5040_PLANT_ID   = v_plant_id
	    AND t2700.C2700_VOID_FL    IS NULL
         AND T2701.C2701_VOID_FL    IS NULL;
        
END gm_fch_cycle_count_year;

/**********************************************************************************************
* description      : Procedure used to fetch Lot Track Number
* author           : MKosalram
* ***********************************************************************************************/
PROCEDURE gm_fch_cycle_count_print_dtls_by_lot (
	        p_cycle_id IN t2706_cycle_count_lock_dtls.c2705_cyc_cnt_lock_id%TYPE,
	        p_out_print_dtls OUT Types.cursor_type)
	   AS
v_date_fmt VARCHAR2 (20);
BEGIN
	--
	SELECT get_compdtfmt_frm_cntx ()
       INTO v_date_fmt
       FROM dual;
    --   
    OPEN p_out_print_dtls FOR
		SELECT t2706.c205_part_number_id pnum
  			,t205.c205_part_num_desc p_desc
  			,t5052.c5052_location_cd location
			,T2708.C2550_CONTROL_NUMBER LOTNM
  			,DECODE(t2705.c901_lock_status, 106830,DECODE(t2706.c2706_recount_fl,'Y', NULL, T2708.C2708_QTY), T2708.C2708_QTY) counted_qty 
  	   FROM t2706_cycle_count_lock_dtls t2706
  	   		,t2705_cycle_count_lock t2705
  	   		,t205_part_number t205
  		    ,t5052_location_master t5052 
  			,t2708_cycle_count_scanned_lots T2708
   	  WHERE t2705.c2705_cyc_cnt_lock_id = t2706.c2705_cyc_cnt_lock_id
  		AND t2706.C5052_LOCATION_ID  = t5052.C5052_LOCATION_ID (+)
    	AND t205.c205_part_number_id    = t2706.c205_part_number_id
    	AND t2705.c2705_cyc_cnt_lock_id = T2708.C2705_CYC_CNT_LOCK_ID(+)
    	AND t2705.c2705_cyc_cnt_lock_id = p_cycle_id
    	AND t2706.c2706_cyc_cnt_lock_dtls_id = t2708.c2706_cyc_cnt_lock_dtls_id(+)
    	-- Status is Recount then only fetch the Recount Data
    	AND DECODE(t2705.c901_lock_status, 106830, NVL(t2706.c2706_recount_fl,'N'), 'N') = DECODE(t2705.c901_lock_status, 106830, 'Y', 'N')
    	AND NVL(t2705.C2705_LOT_TRACK_FL,'-9999') = 'Y'
    	AND t2706.c2706_void_fl        IS NULL
    	AND t2705.c2705_void_fl        IS NULL
    	AND t5052.c5052_void_fl		   IS NULL
    	AND T2708.C2708_VOID_FL(+)     IS NULL 
		ORDER BY t2706.c205_part_number_id;

END gm_fch_cycle_count_print_dtls_by_lot;

/**********************************************************************************************
* description      : Procedure used to fetch cycle count lock details physical quantity for lock details id
* author           : Raja
* ***********************************************************************************************/
PROCEDURE gm_fch_cycle_count_dtls_by_lot_qty (
	        p_cycle_id               IN  t2706_cycle_count_lock_dtls.c2705_cyc_cnt_lock_id%TYPE,
	        p_out_cycle_count_dtls  OUT CLOB)
AS
v_date_fmt VARCHAR2 (20);
BEGIN
	--
	SELECT get_compdtfmt_frm_cntx ()
       INTO v_date_fmt
       FROM dual;

             SELECT 
      JSON_ARRAYAGG (JSON_OBJECT 
                    ('p_qty' value physical_qty
                     , 'countedby' value counted_by
                     , 'counteddate' value counted_date)
	                 RETURNING CLOB) 
    	       INTO p_out_cycle_count_dtls
    	       FROM (SELECT NVL(c2706_physical_qty,'-999') physical_qty,
                            get_user_name(c101_counted_by) counted_by,                                                      
                            TO_CHAR (c2706_counted_date, v_date_fmt ||' HH:MI:SS AM') counted_date
                       FROM t2706_cycle_count_lock_dtls
                      WHERE c2706_cyc_cnt_lock_dtls_id = p_cycle_id
                        AND c2706_void_fl IS NULL);
   
          
END gm_fch_cycle_count_dtls_by_lot_qty;      

END gm_pkg_ac_cycle_count_rpt;
/