--@"c:/database/packages/operations/purchasing/gm_pkg_ac_cyc_cnt_set_schedu.bdy";
CREATE OR REPLACE PACKAGE body gm_pkg_ac_cyc_cnt_set_schedu
IS
    /*
    *
    * Set scheduler
    *
    *
    */
    /**********************************************************************************************
    * Description      : Procedure used to scheduler the consignment details
    * Author           : mmuthusamy
    ***********************************************************************************************/

PROCEDURE gm_sav_cc_set_scheudler 
AS
    v_cycle_count_id t2700_cycle_count.c2700_cycle_count_id%TYPE;
    v_current_year         NUMBER;
    v_user_id              NUMBER := 30301;
    v_working_day_cnt      NUMBER;
    v_last_working_date    DATE;
    v_last_working_day_cnt NUMBER;
    v_cnt                  NUMBER;
    --
    v_remaining_working_day_cnt NUMBER;
    v_monday_cnt                NUMBER;
    v_closed_business_day       NUMBER;
    v_open_businsess_day        NUMBER;
    --
    v_week_working_day_cnt NUMBER;
    v_schedule_from_date DATE;
    v_schedule_to_date DATE;
    v_comments	t902_log.c902_comments%TYPE;
    v_message	t902_log.c902_comments%TYPE;
    --Cycle Count - Set scheduler-PMT-27825
    v_company_id T1900_company.c1900_company_id%TYPE;
    v_plant_id t5040_plant_master.c5040_plant_id%TYPE;
    v_time_zone t901_code_lookup.c901_code_nm%TYPE;
    --
    CURSOR cyc_location_cur
    IS
        -- 106820 Daily
        -- 106821 Weekly
        
         SELECT c2701_cyc_cnt_location_id cyc_cnt_location_id, c901_warehouse_location warehouse_type,
            c901_count_pattern count_pattern, c901_lock_type lock_type, c901_count_type count_type
          , c901_count_frequency count_frequency
           FROM t2701_cycle_count_location
          WHERE c2700_cycle_count_id = v_cycle_count_id
            AND c901_lock_type       = 106825 -- Set
            AND c2701_void_fl       IS NULL
            AND c901_count_frequency = 106820 -- Daily
      UNION
     SELECT c2701_cyc_cnt_location_id cyc_cnt_location_id, c901_warehouse_location warehouse_type, c901_count_pattern
        count_pattern, c901_lock_type lock_type, c901_count_type count_type
      , c901_count_frequency count_frequency
       FROM t2701_cycle_count_location
      WHERE c2700_cycle_count_id = v_cycle_count_id
        AND c901_lock_type       = 106825 -- Set
        AND c2701_void_fl       IS NULL
        AND c901_count_frequency = 106821 -- Weekly
        AND TRUNC (CURRENT_DATE) = TRUNC (CURRENT_DATE, 'IW') ;
    --
BEGIN
	 --Cycle Count - Set scheduler-PMT-27825
	SELECT  get_compid_frm_cntx(),get_comptimezone_frm_cntx(),get_plantid_frm_cntx
			  INTO  v_company_id,v_time_zone,v_plant_id
			  FROM  DUAL;
			EXECUTE IMMEDIATE 'ALTER SESSION SET TIME_ZONE='''||v_time_zone||'''';
    --1 to set the company and plant to context
    
    gm_pkg_cor_client_context.gm_sav_client_context (v_company_id, v_plant_id) ;
    
    -- 2 to get the current year
    
     SELECT TO_CHAR (CURRENT_DATE, 'yyyy')
       INTO v_current_year
       FROM dual;
       
    -- to set the context (its used for V1900)
    
    my_context.set_my_inlist_ctx ('01/01/' || v_current_year) ;
    
    -- to get the working day count
    
     SELECT COUNT (1)
       INTO v_working_day_cnt
       FROM v1900_company_working_days
      WHERE cal_date = TRUNC (CURRENT_DATE) ;
      
    -- Checking the Monday
     SELECT COUNT (1)
       INTO v_monday_cnt
       FROM DUAL
      WHERE TRUNC (CURRENT_DATE) = TRUNC (CURRENT_DATE, 'IW') ;
    --
    
    IF v_working_day_cnt = 0 THEN
        RETURN;
    END IF;
    --
    
    --3 to get the cycle count id
    BEGIN
         SELECT c2700_cycle_count_id
           INTO v_cycle_count_id
           FROM t2700_cycle_count
          WHERE c5040_plant_id         = v_plant_id
            AND c2700_cycle_count_year = v_current_year
            AND c2700_void_fl         IS NULL;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_cycle_count_id := NULL;
        raise_application_error ( - 20999, 'Cycle Count not yet setup for Year: ' || v_current_year ||' Plant: '||
        v_plant_id) ;
    END;
    
    DBMS_OUTPUT.PUT_LINE ('v_cycle_count_id '|| v_cycle_count_id) ;
    
    --3. to get the closed/Open working days
    gm_pkg_ac_cyc_cnt_scheduler.gm_fch_company_working_day (v_company_id, 'CURRENT_DATE', v_closed_business_day,
    v_open_businsess_day) ;
    
    --
    DBMS_OUTPUT.PUT_LINE ('v_open_businsess_day '|| v_open_businsess_day) ;
    
    -- to get the Schedule from and To date (Jan to Jun or Jul to Dec)
    SELECT
    CASE
        WHEN month_number <= 6
        THEN TRUNC (CURRENT_DATE, 'YEAR')
        ELSE ADD_MONTHS (TRUNC (CURRENT_DATE, 'YEAR'), 6)
    END from_date,
    CASE
        WHEN month_number <= 6
        THEN LAST_DAY(ADD_MONTHS (TRUNC (CURRENT_DATE, 'YEAR'), 5))
        ELSE LAST_DAY(ADD_MONTHS (TRUNC (CURRENT_DATE, 'YEAR'), 11))
    END to_date    
    INTO v_schedule_from_date, v_schedule_to_date
   FROM v1900_company_working_days
  WHERE TRUNC (cal_date) = TRUNC (CURRENT_DATE) ;
  
  DBMS_OUTPUT.PUT_LINE ('v_schedule_from_date  '|| to_char(v_schedule_from_date, 'MM/dd/yyyy')) ;
  DBMS_OUTPUT.PUT_LINE ('v_schedule_to_date  '|| to_char(v_schedule_to_date, 'MM/dd/yyyy')) ;
  
    -- 4. Loop the location and call the scheduler
     
  
    FOR cyc_location IN cyc_location_cur
    LOOP
         SELECT COUNT (1)
           INTO v_cnt
           FROM t2704_cycle_count_schedule t2704
          WHERE t2704.c2700_cycle_count_id      = v_cycle_count_id
            AND t2704.c2701_cyc_cnt_location_id = cyc_location.cyc_cnt_location_id
            AND t2704.c2704_schedule_date       = TRUNC (CURRENT_DATE)
            AND t2704.c2704_void_fl            IS NULL;
            
        -- default set to 1 day (Daily)
        
        v_week_working_day_cnt := 1;
        
        -- check if any location weekly count then get all the working days count for thiss week
        
        IF cyc_location.count_frequency = 106821 THEN
             SELECT COUNT (1)
               INTO v_week_working_day_cnt
               FROM v1900_company_working_days
              WHERE WEEK_NUMBER IN
                (
                     SELECT WEEK_NUMBER
                       FROM v1900_company_working_days
                      WHERE TRUNC (cal_date) = TRUNC (CURRENT_DATE)
                ) ;
        END IF;
        --
        DBMS_OUTPUT.PUT_LINE ('Location ' || cyc_location.cyc_cnt_location_id || ' Count Frequency '|| cyc_location.count_frequency ||' v_week_working_day_cnt  '|| v_week_working_day_cnt) ;
        
        IF v_cnt                           = 0 THEN
            IF cyc_location.warehouse_type = 106810 -- WIP set
                THEN
                --
                gm_sav_wip_set_cn_scheduler (v_cycle_count_id, cyc_location.cyc_cnt_location_id, v_open_businsess_day,
                v_week_working_day_cnt, v_schedule_from_date, v_schedule_to_date, v_company_id, v_plant_id, v_user_id) ;
                --
                
            ELSIF cyc_location.warehouse_type = 106811 -- WIP Loaner
                THEN
                
                --
                
                gm_sav_wip_loaner_cn_scheduler (v_cycle_count_id, cyc_location.cyc_cnt_location_id,
                v_open_businsess_day, v_week_working_day_cnt, v_schedule_from_date, v_schedule_to_date, v_company_id, v_plant_id, v_user_id) ;
                
                --
                
            ELSIF cyc_location.warehouse_type = 106812 -- Inhouse - Set
                THEN
                
                --
                
                gm_sav_inhouse_set_scheduler (v_cycle_count_id, cyc_location.cyc_cnt_location_id, v_open_businsess_day,
                v_week_working_day_cnt, v_schedule_from_date, v_schedule_to_date, v_company_id, v_plant_id, v_user_id) ;
            
				--
				
			ELSIF cyc_location.warehouse_type = 106813 -- Built Set
                THEN
                
                --
                
                gm_sav_built_set_scheduler (v_cycle_count_id, cyc_location.cyc_cnt_location_id, v_open_businsess_day,
                v_week_working_day_cnt, v_schedule_from_date, v_schedule_to_date, v_company_id, v_plant_id, v_user_id) ;
                --
                
            END IF; -- end if cyc_location.warehouse_type
             --
        	v_comments := 'Cycle Count Set Scheduler for ' || get_plant_name (v_plant_id) || ' Plant,  Warehouse details - '|| get_code_name (cyc_location.warehouse_type) || ' and Location details - ' || cyc_location.cyc_cnt_location_id;
        
        	-- 26240486 Cycle count scheduler (comments)
        
        	gm_update_log (v_cycle_count_id, v_comments, 26240486, v_user_id, v_message);
        	
            --
        END IF; -- end v_cnt
        --
    END LOOP; -- end cyc_location cursor
    --
END gm_sav_cc_set_scheudler;
--
/**********************************************************************************************
* Description      : Procedure used to scheduler the WIP Consignment details
* Author           : mmuthusamy
***********************************************************************************************/
PROCEDURE gm_sav_wip_set_cn_scheduler (
        p_cycle_count_id        IN T2704_CYCLE_COUNT_SCHEDULE.C2700_CYCLE_COUNT_ID%TYPE,
        p_cyc_cnt_location_id   IN T2704_CYCLE_COUNT_SCHEDULE.C2701_CYC_CNT_LOCATION_ID%TYPE,
        p_remaining_working_day IN NUMBER,
        p_count_frequency       IN NUMBER,
        p_schedule_from_date IN DATE,
        p_schedule_to_date IN DATE,
        p_company_id            IN t1900_company.c1900_company_id%TYPE,
        p_plant_cnt_id          IN t5040_plant_master.c5040_plant_id%type,
        p_user_id               IN T2704_CYCLE_COUNT_SCHEDULE.c2704_last_updated_by%TYPE)
AS
    --
    CURSOR schedule_wip_set_cur
    IS
         SELECT cn_id, row_id
           FROM
            (
                 SELECT cn_id, NTILE (p_remaining_working_day) OVER (ORDER BY NULL) row_id
                   FROM
                    (
                         SELECT t504.c504_consignment_id cn_id
                           FROM t504_consignment t504
                          WHERE t504.c5040_plant_id   = p_plant_cnt_id
                            AND t504.c504_verify_fl   = '0'
                            AND t504.c504_status_fl   = 1
                            AND t504.c504_void_fl    IS NULL
							AND t504.c207_set_id     IS NOT NULL
                      MINUS
                     SELECT t2704.c504_consignment_id cn_id
                       FROM t2704_cycle_count_schedule t2704
                      WHERE t2704.c2700_cycle_count_id      = p_cycle_count_id
                        AND t2704.c2701_cyc_cnt_location_id = p_cyc_cnt_location_id
                        -- to handle the twice logic
                        AND t2704.c2704_schedule_date between p_schedule_from_date AND p_schedule_to_date
                        AND t2704.c2704_void_fl            IS NULL
                   ORDER BY cn_id
                    )
            )
          WHERE row_id BETWEEN 1 AND p_count_frequency;
        --
    BEGIN
        -- Loop the Cursor and insert the values to Scheduler table
        DBMS_OUTPUT.PUT_LINE ('WIP set load started ') ;
        
        FOR schedule_wip_set IN schedule_wip_set_cur
        LOOP
            --
            gm_pkg_ac_cyc_cnt_scheduler.gm_sav_cyc_cnt_scheduler (p_cycle_count_id, p_cyc_cnt_location_id, NULL, NULL,
            schedule_wip_set.cn_id, TRUNC(CURRENT_DATE), NULL, p_user_id) ;
        END LOOP;
        --
        DBMS_OUTPUT.PUT_LINE ('WIP set load completed ') ;
        
    END gm_sav_wip_set_cn_scheduler;
    --
    /**********************************************************************************************
    * Description      : Procedure used to scheduler the WIP Loaner details
    * Author           : mmuthusamy
    ***********************************************************************************************/
PROCEDURE gm_sav_wip_loaner_cn_scheduler (
        p_cycle_count_id        IN T2704_CYCLE_COUNT_SCHEDULE.C2700_CYCLE_COUNT_ID%TYPE,
        p_cyc_cnt_location_id   IN T2704_CYCLE_COUNT_SCHEDULE.C2701_CYC_CNT_LOCATION_ID%TYPE,
        p_remaining_working_day IN NUMBER,
        p_count_frequency       IN NUMBER,
        p_schedule_from_date IN DATE,
        p_schedule_to_date IN DATE,
        p_company_id            IN t1900_company.c1900_company_id%TYPE,
        p_plant_cnt_id          IN t5040_plant_master.c5040_plant_id%type,
        p_user_id               IN T2704_CYCLE_COUNT_SCHEDULE.c2704_last_updated_by%TYPE)
AS
    CURSOR schedule_wip_loaner_set_cur
    IS
         SELECT cn_id, row_id
           FROM
            (
                 SELECT cn_id, NTILE (p_remaining_working_day) OVER (ORDER BY NULL) row_id
                   FROM
                    (
                         SELECT t504.c504_consignment_id cn_id
                           FROM t504_consignment t504, t504a_consignment_loaner t504a
                          WHERE t504.c504_consignment_id        = t504a.c504_consignment_id
                            AND t504.c5040_plant_id           = p_plant_cnt_id
                            AND t504.c504_type                  = 4127
                            AND t504.c207_set_id               IS NOT NULL
                            AND NVL (T504a.c504a_status_fl, 0) = 30 -- WIP Loaner
                            AND t504.c504_status_fl = 4
                            AND t504.c504_verify_fl = 1
                            AND t504.c504_void_fl              IS NULL
                      MINUS
                     SELECT t2704.c504_consignment_id cn_id
                       FROM t2704_cycle_count_schedule t2704
                      WHERE t2704.c2700_cycle_count_id      = p_cycle_count_id
                        AND t2704.c2701_cyc_cnt_location_id = p_cyc_cnt_location_id
                        -- to handle the twice logic
                        AND t2704.c2704_schedule_date between p_schedule_from_date AND p_schedule_to_date
                        AND t2704.c2704_void_fl            IS NULL
                   ORDER BY cn_id
                    )
            )
          WHERE row_id BETWEEN 1 AND p_count_frequency;
        --
    BEGIN
	    
	    DBMS_OUTPUT.PUT_LINE ('WIP Loaner set started ') ;
	    
        -- Loop the Cursor and insert the values to Scheduler table
        
        FOR schedule_wip_loaner_set IN schedule_wip_loaner_set_cur
        LOOP
            --
            gm_pkg_ac_cyc_cnt_scheduler.gm_sav_cyc_cnt_scheduler (p_cycle_count_id, p_cyc_cnt_location_id, NULL, NULL,
            schedule_wip_loaner_set.cn_id, TRUNC(CURRENT_DATE), NULL, p_user_id) ;
        END LOOP;
        --
        DBMS_OUTPUT.PUT_LINE ('WIP Loaner set completed ') ;
        
    END gm_sav_wip_loaner_cn_scheduler;
    --
    /**********************************************************************************************
    * Description      : Procedure used to scheduler the In-house set details
    * Author           : mmuthusamy
    ***********************************************************************************************/
PROCEDURE gm_sav_inhouse_set_scheduler (
        p_cycle_count_id        IN T2704_CYCLE_COUNT_SCHEDULE.C2700_CYCLE_COUNT_ID%TYPE,
        p_cyc_cnt_location_id   IN T2704_CYCLE_COUNT_SCHEDULE.C2701_CYC_CNT_LOCATION_ID%TYPE,
        p_remaining_working_day IN NUMBER,
        p_count_frequency       IN NUMBER,
        p_schedule_from_date IN DATE,
        p_schedule_to_date IN DATE,
        p_company_id            IN t1900_company.c1900_company_id%TYPE,
        p_plant_cnt_id          IN t5040_plant_master.c5040_plant_id%type,
        p_user_id               IN T2704_CYCLE_COUNT_SCHEDULE.c2704_last_updated_by%TYPE)
AS
    --
    CURSOR schedule_inhouse_set_cur
    IS
         SELECT cn_id, row_id
           FROM
            (
                 SELECT cn_id, NTILE (p_remaining_working_day) OVER (ORDER BY NULL) row_id
                   FROM
                    (
                         SELECT t504.C504_CONSIGNMENT_ID cn_id
                           FROM t504_consignment t504, t504a_consignment_loaner t504a
                          WHERE t504.c504_consignment_id        = t504a.c504_consignment_id
                            AND t504.c504_type                  = 4119
                            AND t504.c504_status_fl             = '4'
                            AND t504.c504_verify_fl             = '1'
                            AND t504.c5040_plant_id           = p_plant_cnt_id
                            AND t504.c207_set_id               IS NOT NULL
                            AND NVL (t504a.c504a_status_fl, 0) NOT IN (20, 60) --Pending Returns, Inactive
                            AND t504.c504_void_fl              IS NULL
                      MINUS
                     SELECT t2704.c504_consignment_id cn_id
                       FROM t2704_cycle_count_schedule t2704
                      WHERE t2704.c2700_cycle_count_id      = p_cycle_count_id
                        AND t2704.c2701_cyc_cnt_location_id = p_cyc_cnt_location_id
                        -- to handle the twice logic
                        AND t2704.c2704_schedule_date between p_schedule_from_date AND p_schedule_to_date
                        AND t2704.c2704_void_fl            IS NULL
                   ORDER BY cn_id
                    )
            )
          WHERE row_id BETWEEN 1 AND p_count_frequency;
        --
    BEGIN
	    --
	    DBMS_OUTPUT.PUT_LINE ('In-house set started ') ;
	    
        -- Loop the Cursor and insert the values to Scheduler table
        
        FOR schedule_inhouse_set IN schedule_inhouse_set_cur
        LOOP
            --
            gm_pkg_ac_cyc_cnt_scheduler.gm_sav_cyc_cnt_scheduler (p_cycle_count_id, p_cyc_cnt_location_id, NULL, NULL,
            schedule_inhouse_set.cn_id, TRUNC(CURRENT_DATE), NULL, p_user_id) ;
        END LOOP;
        --
        DBMS_OUTPUT.PUT_LINE ('In-house set completed ') ;
        --
    END gm_sav_inhouse_set_scheduler;
    --
	
	/**********************************************************************************************
    * Description      : Procedure used to scheduler the Built set details
    * Author           : mmuthusamy
    ***********************************************************************************************/
PROCEDURE gm_sav_built_set_scheduler (
        p_cycle_count_id        IN T2704_CYCLE_COUNT_SCHEDULE.C2700_CYCLE_COUNT_ID%TYPE,
        p_cyc_cnt_location_id   IN T2704_CYCLE_COUNT_SCHEDULE.C2701_CYC_CNT_LOCATION_ID%TYPE,
        p_remaining_working_day IN NUMBER,
        p_count_frequency       IN NUMBER,
        p_schedule_from_date IN DATE,
        p_schedule_to_date IN DATE,
        p_company_id            IN t1900_company.c1900_company_id%TYPE,
        p_plant_cnt_id          IN t5040_plant_master.c5040_plant_id%type,
        p_user_id               IN T2704_CYCLE_COUNT_SCHEDULE.c2704_last_updated_by%TYPE)
AS
    --
    CURSOR schedule_built_set_cur
    IS
         SELECT cn_id, row_id
           FROM
            (
                 SELECT cn_id, NTILE (p_remaining_working_day) OVER (ORDER BY NULL) row_id
                   FROM
                    (
                           SELECT t504.c504_consignment_id cn_id
						   FROM t504_consignment t504, t906_rules t906
						  WHERE t504.c504_status_fl = t906.c906_rule_value --            IN ('2','2.20','3','3.30','3.60' ) -- 2 Built
							-- Set  /2.20 Pending Pick  /3 Pending Shipping  /3.30 Packing In Progress /3.60 Ready for Pickup
							AND t504.c504_verify_fl   = '1'
							AND t504.c504_void_fl    IS NULL
							AND t504.c207_set_id     IS NOT NULL
							AND t906.c906_rule_id     = 'PI_BUILTSETSTATUS'
							AND t906.c906_rule_grp_id = 'PI_LOCK'
							AND t906.c906_void_fl    IS NULL
							AND t504.c5040_plant_id   = p_plant_cnt_id
                      MINUS
                     SELECT t2704.c504_consignment_id cn_id
                       FROM t2704_cycle_count_schedule t2704
                      WHERE t2704.c2700_cycle_count_id      = p_cycle_count_id
                        AND t2704.c2701_cyc_cnt_location_id = p_cyc_cnt_location_id
                        -- to handle the twice logic
                        AND t2704.c2704_schedule_date between p_schedule_from_date AND p_schedule_to_date
                        AND t2704.c2704_void_fl            IS NULL
                   ORDER BY cn_id
                    )
            )
          WHERE row_id BETWEEN 1 AND p_count_frequency;
        --
    BEGIN
	    --
	    DBMS_OUTPUT.PUT_LINE ('Built set started ' || p_cycle_count_id ||' Location ID '|| p_cyc_cnt_location_id ||' Plant ID '|| p_plant_cnt_id) ;
	    
        -- Loop the Cursor and insert the values to Scheduler table
        
        FOR schedule_built_set IN schedule_built_set_cur
        LOOP
            --
			DBMS_OUTPUT.PUT_LINE (' CN ID '|| schedule_built_set.cn_id);
            gm_pkg_ac_cyc_cnt_scheduler.gm_sav_cyc_cnt_scheduler (p_cycle_count_id, p_cyc_cnt_location_id, NULL, NULL,
            schedule_built_set.cn_id, TRUNC(CURRENT_DATE), NULL, p_user_id) ;
        END LOOP;
        --
        DBMS_OUTPUT.PUT_LINE ('Build set completed ') ;
        --
    END gm_sav_built_set_scheduler;
	
END gm_pkg_ac_cyc_cnt_set_schedu;
/
