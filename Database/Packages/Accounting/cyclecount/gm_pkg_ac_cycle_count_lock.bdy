--@"c:/database/packages/operations/purchasing/gm_pkg_ac_cycle_count_lock.bdy";
CREATE OR REPLACE PACKAGE body gm_pkg_ac_cycle_count_lock
IS

/**********************************************************************************************
* description      : procedure used to get the warehouse list based on scheduler and call the lock procedure 
* author           : mmuthusamy
***********************************************************************************************/

PROCEDURE gm_lock_warehouse_main
AS
v_plant_cnt_id t5040_plant_master.c5040_plant_id%type;
v_cycle_count_id t2700_cycle_count.c2700_cycle_count_id%type;
v_time_zone t901_code_lookup.c901_code_nm%TYPE;
--
CURSOR cycle_count_lock_cur
IS

  SELECT c901_warehouse_location warehouse_location
   FROM t2701_cycle_count_location
  WHERE c2700_cycle_count_id = v_cycle_count_id
    AND c2701_void_fl       IS NULL
    AND c901_count_frequency = 106820 -- Daily
  UNION
 SELECT c901_warehouse_location warehouse_location
   FROM t2701_cycle_count_location
  WHERE c2700_cycle_count_id = v_cycle_count_id
    AND c2701_void_fl       IS NULL
    AND c901_count_frequency = 106821 -- weekly
    AND TRUNC (CURRENT_DATE)  = TRUNC (CURRENT_DATE, 'IW'); -- Monday
    
BEGIN
	 --1 to get the plant,to get the timezone
     SELECT get_plantid_frm_cntx (),get_comptimezone_frm_cntx()
     INTO v_plant_cnt_id, v_time_zone
     FROM dual;   
     EXECUTE IMMEDIATE 'ALTER SESSION SET TIME_ZONE='''||v_time_zone||''''; --Cycle Count - Set scheduler-PMT-27825
    --
    dbms_output.put_line (' Lock main plant id '|| v_plant_cnt_id) ;
    --
     -- 2 to get the cycle count id
     BEGIN
     SELECT t2700.c2700_cycle_count_id
       INTO v_cycle_count_id
       FROM t2700_cycle_count t2700
      WHERE t2700.c5040_plant_id         = v_plant_cnt_id
        AND t2700.c2700_cycle_count_year = TO_CHAR (CURRENT_DATE, 'yyyy')
        AND t2700.c2700_void_fl         IS NULL;
     EXCEPTION
            WHEN OTHERS THEN
            v_cycle_count_id := NULL;
     END;       
      -- Loop the cursor and call the Lock procedure
      FOR cycle_count_lock IN cycle_count_lock_cur
      LOOP
      	dbms_output.put_line (' Called the Lock procedure '|| cycle_count_lock.warehouse_location) ;
      	gm_lock_warehouse ( cycle_count_lock.warehouse_location, CURRENT_DATE);
      END LOOP;
      --  
	END gm_lock_warehouse_main;

    /**********************************************************************************************
    * description      : procedure used to lock the system qty - based on warehoue type
    * author           : mmuthusamy
    ***********************************************************************************************/
PROCEDURE gm_lock_warehouse (
        p_warehouse_type IN t2701_cycle_count_location.c2701_cyc_cnt_location_id%type,
        p_schedule_date  IN t2704_cycle_count_schedule.c2704_schedule_date%type)
AS
    v_cycle_count_id t2700_cycle_count.c2700_cycle_count_id%type;
    v_plant_cnt_id t5040_plant_master.c5040_plant_id%type;
    v_lock_id t2705_cycle_count_lock.c2705_cyc_cnt_lock_id%type;
    v_cycle_cnt_location_id t2705_cycle_count_lock.c2701_cyc_cnt_location_id%type;
    v_user_id       NUMBER := 30301;
    v_lock_cnt      NUMBER;
    v_lock_schedule NUMBER;
    --
    v_number_part_locked NUMBER;
    v_lock_type NUMBER;
BEGIN
    --1 to get the plant
     SELECT get_plantid_frm_cntx ()
       INTO v_plant_cnt_id
       FROM dual;
    --
    dbms_output.put_line (' plant id '|| v_plant_cnt_id) ;
    
    -- 2 to get the cycle count id
    BEGIN
     SELECT t2700.c2700_cycle_count_id
       INTO v_cycle_count_id
       FROM t2700_cycle_count t2700
      WHERE t2700.c5040_plant_id         = v_plant_cnt_id
        AND t2700.c2700_cycle_count_year = TO_CHAR (CURRENT_DATE, 'yyyy')
        AND t2700.c2700_void_fl         IS NULL;
       
    --3. to get the location id
    dbms_output.put_line (' cycle count '|| v_cycle_count_id) ;
    
     SELECT c2701_cyc_cnt_location_id, c901_lock_type
       INTO v_cycle_cnt_location_id, v_lock_type
       FROM t2701_cycle_count_location
      WHERE c2700_cycle_count_id    = v_cycle_count_id
        AND c901_warehouse_location = p_warehouse_type
        AND c2701_void_fl          IS NULL;
    EXCEPTION
            WHEN OTHERS THEN
            v_cycle_count_id := NULL;
            v_cycle_cnt_location_id := NULL;
     END;        
    -- to validate the cycle count id
    
     SELECT COUNT (1)
       INTO v_lock_cnt
       FROM t2705_cycle_count_lock
      WHERE c2700_cycle_count_id      = v_cycle_count_id
        AND c2701_cyc_cnt_location_id = v_cycle_cnt_location_id
        AND TRUNC(c2705_lock_date)    = TRUNC(CURRENT_DATE)
        AND c2705_void_fl            IS NULL;
    --
    
     SELECT COUNT (1)
       INTO v_lock_schedule
       FROM t2704_cycle_count_schedule t2704
      WHERE t2704.c2700_cycle_count_id        = v_cycle_count_id
        AND t2704.c2701_cyc_cnt_location_id   = v_cycle_cnt_location_id
        AND TRUNC (t2704.c2704_schedule_date) = TRUNC (p_schedule_date)
        AND t2704.c2704_void_fl              IS NULL;
    --
    dbms_output.put_line (' lock count '|| v_lock_cnt || ' type '|| p_warehouse_type || ' v_cycle_cnt_location_id '||
    v_cycle_cnt_location_id) ;
    
    -- if warehouse already locked then to avoid the duplicate Lock 
    -- checking the scheduler having the data then proceed.
    IF (v_lock_cnt = 0 AND v_lock_schedule <> 0) THEN
        -- to create the lock id
        gm_pkg_ac_cycle_count_lock.gm_sav_cycle_count_lock (v_cycle_count_id, v_cycle_cnt_location_id, p_schedule_date, v_user_id,
        v_lock_id) ;
        --93320 pick face
        --93336 bulk
        -- 106800 FG pick face
        IF v_lock_type = 106822 -- Part #
        THEN
	        IF (p_warehouse_type = 106800) THEN
	        	IF v_plant_cnt_id = 3001 THEN --BBA
	            	gm_pkg_ac_cycle_count_lock.gm_inv_part_bba_warehouse (v_cycle_count_id, v_cycle_cnt_location_id, v_lock_id, 1,
	            	93320, v_plant_cnt_id, p_schedule_date, v_user_id) ;
	            ELSE
	            	gm_pkg_ac_cycle_count_lock.gm_inv_part_warehouse (v_cycle_count_id, v_cycle_cnt_location_id, v_lock_id, 1,
	            	93320, v_plant_cnt_id, p_schedule_date, v_user_id) ;
	            END IF;
			END IF;
		ELSIF v_lock_type = 106823 -- Location
		THEN 
			IF (p_warehouse_type = 106800) THEN
	        
	            gm_pkg_ac_cycle_count_lock.gm_inv_location_warehouse (v_cycle_count_id, v_cycle_cnt_location_id, v_lock_id, 1,
	            93320, v_plant_cnt_id, p_schedule_date, v_user_id) ;
	            --106801 FG bulk
	        ELSIF (p_warehouse_type = 106801) THEN
	        
	            gm_pkg_ac_cycle_count_lock.gm_inv_location_warehouse (v_cycle_count_id, v_cycle_cnt_location_id, v_lock_id,
	            1, 93336, v_plant_cnt_id, p_schedule_date, v_user_id) ;
	            --106802 RW pick
	        ELSIF (p_warehouse_type = 106802) THEN
	        
	            gm_pkg_ac_cycle_count_lock.gm_inv_location_warehouse (v_cycle_count_id, v_cycle_cnt_location_id, v_lock_id,
	            2, 93320, v_plant_cnt_id, p_schedule_date, v_user_id) ;
	            
	            -- 106803 RW bulk
	        ELSIF (p_warehouse_type = 106803) THEN
	            gm_pkg_ac_cycle_count_lock.gm_inv_location_warehouse (v_cycle_count_id, v_cycle_cnt_location_id, v_lock_id,
	            2, 93336, v_plant_cnt_id, p_schedule_date, v_user_id) ;
	        END IF;    
	    ELSIF v_lock_type = 106824 -- Project
	    THEN       
	            -- 106804 repack
	            -- 106805 QN
	            -- 106806 QN (S)
	            -- 106808 bulk loose
	            -- 106809 inhouse – loose
	            -- 106807 rm
            gm_pkg_ac_cycle_count_lock.gm_inv_lock_by_project (v_cycle_count_id, v_cycle_cnt_location_id, v_lock_id, p_schedule_date,
            v_user_id) ;
	            
	            -- 106810 wip - set
	            -- 106811 wip loaner
	            -- 106812 in-house set
	    ELSIF v_lock_type = 106825 -- Set
	    THEN
	    
	        
	            gm_pkg_ac_cycle_count_lock.gm_inv_lock_by_set (v_cycle_count_id, v_cycle_cnt_location_id, v_lock_id, p_schedule_date,
	            v_user_id) ;
	    END IF; -- Lock type check        
    END IF; -- end of lock
    -- No part/Set locked then void the CC ID
     SELECT COUNT (1) INTO v_number_part_locked
   FROM t2706_cycle_count_lock_dtls
  WHERE c2705_cyc_cnt_lock_id = v_lock_id
    AND c2706_void_fl        IS NULL; 
    --
    IF v_number_part_locked = 0
    THEN
    --
    dbms_output.put_line ('Void the Lock Id '|| v_lock_id);
    --
    UPDATE t2705_cycle_count_lock
    SET c2705_void_fl          = 'Y', c2705_last_updated_by = v_user_id, c2705_last_updated_date = CURRENT_DATE
      WHERE c2705_cyc_cnt_lock_id = v_lock_id;
    -- 
    END IF;    
    
END gm_lock_warehouse;
/**********************************************************************************************
* description      : procedure used to create the cycle count lock id
* author           : mmuthusamy
***********************************************************************************************/
PROCEDURE gm_sav_cycle_count_lock (
        p_cycle_count_id IN t2700_cycle_count.c2700_cycle_count_id%type,
        p_cycle_count_loc_id         IN t2701_cycle_count_location.c2701_cyc_cnt_location_id%type,
        p_schedule_date  IN t2704_cycle_count_schedule.c2704_schedule_date%type,
        p_user_id        IN t2705_cycle_count_lock.c2705_last_updated_by%type,
        p_lock_id OUT t2705_cycle_count_lock.c2705_cyc_cnt_lock_id%type)
AS
v_company_id t1900_company.c1900_company_id%TYPE;
v_lock_prefix VARCHAR2(10);
BEGIN
    -- to get the company id 
    SELECT get_compid_frm_cntx () INTO v_company_id FROM DUAL;
    -- get the cycle count transactions pre fix (based on company level)
    SELECT GET_RULE_VALUE_BY_COMPANY ('CYC_CNT_ID_PREFIX','CYC_CNT_LOCK', v_company_id) INTO v_lock_prefix  FROM DUAL;
    --
     SELECT v_lock_prefix || s2705_cycle_count_lock.nextval INTO p_lock_id FROM dual;
    --
    dbms_output.put_line (' lock ids  '|| p_lock_id ||' p_cycle_count_id '|| p_cycle_count_id ||' p_loc_id '|| p_cycle_count_loc_id
    ) ;
    --
     INSERT
       INTO t2705_cycle_count_lock
        (
            c2705_cyc_cnt_lock_id, c2700_cycle_count_id, c2701_cyc_cnt_location_id
          , c2705_lock_date, c901_lock_status, c2705_schedule_date
          , c2705_last_updated_by, c2705_last_updated_date
   
        )
        VALUES
        (
            p_lock_id, p_cycle_count_id, p_cycle_count_loc_id
          , CURRENT_DATE, 106827, p_schedule_date
          , p_user_id, CURRENT_DATE
        
        ) ;
END gm_sav_cycle_count_lock;
/**********************************************************************************************
* description      : procedure used to insert the cycle count lock details
* author           : mmuthusamy
***********************************************************************************************/
PROCEDURE gm_sav_cycle_cnt_lock_dtls
    (
        p_cyc_cnt_lock_id IN t2706_cycle_count_lock_dtls.c2705_cyc_cnt_lock_id%type,
        p_part_number     IN t2706_cycle_count_lock_dtls.c205_part_number_id%type,
        p_location_id     IN t2706_cycle_count_lock_dtls.c5052_location_id%type,
        p_system_qty      IN t2706_cycle_count_lock_dtls.c2706_system_qty%type,
        p_cost			  IN t2706_cycle_count_lock_dtls.c2706_cycle_count_cost%TYPE,
        p_local_cost      IN t2706_cycle_count_lock_dtls.c2706_cycle_count_local_cost%TYPE,
        p_owner_currency  IN t2706_cycle_count_lock_dtls.c901_owner_currency%TYPE,
        p_local_currency  IN t2706_cycle_count_lock_dtls.c901_local_company_currency%TYPE,
        p_last_update_by  IN t2706_cycle_count_lock_dtls.c2706_last_updated_by%type
    )
AS
    v_err NUMBER;
BEGIN
    -- to handle the exception - if part #, location not available
    BEGIN
         INSERT
           INTO t2706_cycle_count_lock_dtls
            (
                c2706_cyc_cnt_lock_dtls_id, c2705_cyc_cnt_lock_id, c205_part_number_id
              , c5052_location_id, c2706_system_qty
              , c2706_last_updated_by, c2706_last_updated_date
              , c2706_cycle_count_cost, c2706_cycle_count_local_cost
              , c901_owner_currency, c901_local_company_currency
            )
            VALUES
            (
                s2706_cycle_count_lock_dtls.nextval, p_cyc_cnt_lock_id, p_part_number
              , p_location_id, NVL(p_system_qty, 0)
              , p_last_update_by, CURRENT_DATE
              , p_cost, p_local_cost
              , p_owner_currency, p_local_currency
            ) ;
    EXCEPTION
    WHEN OTHERS THEN
        v_err := 1;
    END;
END gm_sav_cycle_cnt_lock_dtls;
/**********************************************************************************************
* description      : procedure used to get the system qty from pick face based on parts and insert to lock details
table.
* author           : mmuthusamy
***********************************************************************************************/
PROCEDURE gm_inv_part_warehouse
    (
        p_cycle_count_id IN t2700_cycle_count.c2700_cycle_count_id%type,
        p_loc_id         IN t2701_cycle_count_location.c2701_cyc_cnt_location_id%type,
        p_lock_id        IN t2705_cycle_count_lock.c2705_cyc_cnt_lock_id%type,
        p_warehouse_id   IN t5051_inv_warehouse.c5051_inv_warehouse_id%type,
        p_location_type  IN t5052_location_master.c901_location_type%type,
        p_plant_id       IN t5040_plant_master.c5040_plant_id%type,
        p_schedule_date  IN t2704_cycle_count_schedule.c2704_schedule_date%type,
        p_user_id        IN t2700_cycle_count.c2700_last_updated_by%type
    )
AS
	v_costing_type VARCHAR2(20);
	--
    v_cost NUMBER;
    v_local_cost NUMBER;
	--
	v_owner_currency t2706_cycle_count_lock_dtls.c901_owner_currency%TYPE;
    v_local_currency t2706_cycle_count_lock_dtls.c901_local_company_currency%TYPE;
    --
    CURSOR inv_part_cur
    IS
         SELECT t5053.c205_part_number_id pnum, t5052.c5052_location_id location_id, t5053.c5053_curr_qty - NVL (
            alloc.qty, 0) qty, get_ac_costing_id(t5053.c205_part_number_id,v_costing_type) costing_id
           FROM t5051_inv_warehouse t5051, t5052_location_master t5052, t5053_location_part_mapping t5053
          , t205_part_number t205, my_temp_part_list my_temp, (
                 SELECT t5055.c5052_location_id, t5055.c205_part_number_id, NVL (SUM (t5055.c5055_qty), 0) qty
                   FROM t5055_control_number t5055, my_temp_part_list my_temp
                  WHERE t5055.c901_type            = 93343 -- pick transacted
                   AND my_temp.c205_part_number_id = t5055.c205_part_number_id
                    AND t5055.c5055_void_fl       IS NULL
                    AND t5055.c5055_update_inv_fl IS NULL
                    AND t5055.c5040_plant_id       = p_plant_id -- plant id
               GROUP BY t5055.c5052_location_id, t5055.c205_part_number_id
            )
        alloc
      WHERE t5051.c5051_inv_warehouse_id = t5052.c5051_inv_warehouse_id(+)
        AND t5052.c5052_location_id      = t5053.c5052_location_id(+)
        AND t205.c205_part_number_id  = my_temp.c205_part_number_id
        AND t5053.c205_part_number_id    = t205.c205_part_number_id
        AND t5053.c205_part_number_id    = alloc.c205_part_number_id (+)
        AND t5053.c5052_location_id      = alloc.c5052_location_id(+)
        AND t5051.c901_status_id         = 1 -- active
        AND t5051.c5051_inv_warehouse_id = p_warehouse_id -- fg warehouse,rw warehouse
        AND t5052.c901_status            = 93310 --active
        AND t5052.c901_location_type     = p_location_type --  bulk,pick face
        AND t5052.c5052_void_fl         IS NULL
        AND t5052.c5040_plant_id         = p_plant_id -- audubon
        AND t5053.c205_part_number_id     IS NOT NULL;
   
BEGIN
	-- to set the parts to temp table
	DELETE FROM my_temp_part_list;

	 INSERT INTO my_temp_part_list
	    (c205_part_number_id
	    )
	 SELECT c205_part_number_id
	   FROM t2704_cycle_count_schedule
	  WHERE c2700_cycle_count_id = p_cycle_count_id
	  	AND c2701_cyc_cnt_location_id = p_loc_id
	    AND c2704_schedule_date       = TRUNC (p_schedule_date)
	    AND c205_part_number_id NOT IN
	    (SELECT t2703.C205_PART_NUMBER_ID
               FROM t2703_cycle_count_rank t2703
              WHERE t2703.c2700_cycle_count_id = p_cycle_count_id
                AND t2703.c2703_rank           = 'D' -- to avoid the d rank parts to count
                AND t2703.c2703_void_fl       IS NULL)
	    AND c2704_void_fl IS NULL;
	-- Based on warehouse type - to get the costing type
	SELECT get_rule_value (c901_warehouse_location,'CYC_CNT_COSTING_MAP')
		INTO v_costing_type
	FROM t2701_cycle_count_location
	WHERE c2701_cyc_cnt_location_id = p_loc_id;
	--
    FOR inv_part IN inv_part_cur
    LOOP
    	-- based on costing ID - to get the Cost and Local cost value
    	BEGIN
			SELECT t820.c820_purchase_amt
			  , t820.c820_local_company_cost
			  , t820.c901_owner_currency
			  , t820.c901_local_company_currency
			   INTO v_cost
			  , v_local_cost
			  , v_owner_currency
			  , v_local_currency
			   FROM t820_costing t820
			  WHERE t820.c820_costing_id = inv_part.costing_id;
	   EXCEPTION WHEN NO_DATA_FOUND
	   THEN
	   		v_cost:= 0;
	   		v_local_cost:= 0;
	   		v_owner_currency := NULL;
	   		v_local_currency := NULL;
	   END;
	   --
        gm_pkg_ac_cycle_count_lock.gm_sav_cycle_cnt_lock_dtls (p_lock_id, inv_part.pnum, inv_part.location_id,
        inv_part.qty, v_cost, v_local_cost, v_owner_currency, v_local_currency, p_user_id) ;
    END LOOP;
    dbms_output.put_line (' lock completed  '|| p_lock_id) ;
    
END gm_inv_part_warehouse;

/**********************************************************************************************
* description      : procedure used to get the system qty from pick face based on parts and insert to lock details to BBA
table.
* author           : asinagravel
***********************************************************************************************/
PROCEDURE gm_inv_part_bba_warehouse
    (
        p_cycle_count_id IN t2700_cycle_count.c2700_cycle_count_id%type,
        p_loc_id         IN t2701_cycle_count_location.c2701_cyc_cnt_location_id%type,
        p_lock_id        IN t2705_cycle_count_lock.c2705_cyc_cnt_lock_id%type,
        p_warehouse_id   IN t5051_inv_warehouse.c5051_inv_warehouse_id%type,
        p_location_type  IN t5052_location_master.c901_location_type%type,
        p_plant_id       IN t5040_plant_master.c5040_plant_id%type,
        p_schedule_date  IN t2704_cycle_count_schedule.c2704_schedule_date%type,
        p_user_id        IN t2700_cycle_count.c2700_last_updated_by%type
    )
AS
	v_costing_type VARCHAR2(20);
	--
    v_cost NUMBER;
    v_local_cost NUMBER;
	--
	v_owner_currency t2706_cycle_count_lock_dtls.c901_owner_currency%TYPE;
    v_local_currency t2706_cycle_count_lock_dtls.c901_local_company_currency%TYPE;
    --
    CURSOR inv_part_cur
    IS
    
    	SELECT my_temp.c205_part_number_id pnum,c205_inshelf qty,get_ac_costing_id(my_temp.c205_part_number_id,v_costing_type) costing_id
    	  FROM v205_qty_in_stock v205, my_temp_part_list my_temp
    	 WHERE v205.c205_part_number_id = my_temp.c205_part_number_id;
    	 
BEGIN
	-- to set the parts to temp table
	DELETE FROM my_temp_part_list;

	 INSERT INTO my_temp_part_list
	    (c205_part_number_id
	    )
	 SELECT c205_part_number_id
	   FROM t2704_cycle_count_schedule
	  WHERE c2700_cycle_count_id = p_cycle_count_id
	  	AND c2701_cyc_cnt_location_id = p_loc_id
	    AND c2704_schedule_date       = TRUNC (p_schedule_date)
	    AND c205_part_number_id NOT IN
	    (SELECT t2703.C205_PART_NUMBER_ID
               FROM t2703_cycle_count_rank t2703
              WHERE t2703.c2700_cycle_count_id = p_cycle_count_id
                AND t2703.c2703_rank           = 'D' -- to avoid the d rank parts to count
                AND t2703.c2703_void_fl       IS NULL)
	    AND c2704_void_fl IS NULL;
	-- Based on warehouse type - to get the costing type
	SELECT get_rule_value (c901_warehouse_location,'CYC_CNT_COSTING_MAP')
		INTO v_costing_type
	FROM t2701_cycle_count_location
	WHERE c2701_cyc_cnt_location_id = p_loc_id;
	--
    FOR inv_part IN inv_part_cur
    LOOP
    	-- based on costing ID - to get the Cost and Local cost value
    	BEGIN
			SELECT t820.c820_purchase_amt
			  , t820.c820_local_company_cost
			  , t820.c901_owner_currency
			  , t820.c901_local_company_currency
			   INTO v_cost
			  , v_local_cost
			  , v_owner_currency
			  , v_local_currency
			   FROM t820_costing t820
			  WHERE t820.c820_costing_id = inv_part.costing_id;
	   EXCEPTION WHEN NO_DATA_FOUND
	   THEN
	   		v_cost:= 0;
	   		v_local_cost:= 0;
	   		v_owner_currency := NULL;
	   		v_local_currency := NULL;
	   END;
	   --
        gm_pkg_ac_cycle_count_lock.gm_sav_cycle_cnt_lock_dtls (p_lock_id, inv_part.pnum, null,
        inv_part.qty, v_cost, v_local_cost, v_owner_currency, v_local_currency, p_user_id) ;
    END LOOP;
    
    UPDATE T2705_CYCLE_COUNT_LOCK 
       SET c2705_lot_track_fl = 'Y'
         , c2705_split_fl = 'Y'
         , c2705_last_updated_by = p_user_id
         , c2705_last_updated_date = current_date
     WHERE c2705_cyc_cnt_lock_id = p_lock_id
       AND c2705_void_fl IS NULL;
    
    dbms_output.put_line (' lock completed  '|| p_lock_id) ;
    
END gm_inv_part_bba_warehouse;

/**********************************************************************************************
* description      : procedure used to get the system qty from pick/bulk face based on location and insert to lock
details table.
* author           : mmuthusamy
***********************************************************************************************/
PROCEDURE gm_inv_location_warehouse (
        p_cycle_count_id IN t2700_cycle_count.c2700_cycle_count_id%type,
        p_loc_id         IN t2701_cycle_count_location.c2701_cyc_cnt_location_id%type,
        p_lock_id        IN t2705_cycle_count_lock.c2705_cyc_cnt_lock_id%type,
        p_warehouse_id   IN t5051_inv_warehouse.c5051_inv_warehouse_id%type,
        p_location_type  IN t5052_location_master.c901_location_type%type,
        p_plant_id       IN t5040_plant_master.c5040_plant_id%type,
        p_schedule_date  IN t2704_cycle_count_schedule.c2704_schedule_date%type,
        p_user_id        IN t2700_cycle_count.c2700_last_updated_by%type)
AS
	v_costing_type VARCHAR2(20);
	--
    v_cost NUMBER;
    v_local_cost NUMBER;
    --
    v_owner_currency t2706_cycle_count_lock_dtls.c901_owner_currency%TYPE;
    v_local_currency t2706_cycle_count_lock_dtls.c901_local_company_currency%TYPE;
    --
    CURSOR inv_location_cur
    IS
         SELECT t5053.c205_part_number_id pnum, t5052.c5052_location_id location_id, t5053.c5053_curr_qty - NVL (
            alloc.qty, 0) qty, get_ac_costing_id(t5053.c205_part_number_id,v_costing_type) costing_id
           FROM t5051_inv_warehouse t5051, t5052_location_master t5052, t5053_location_part_mapping t5053
          , t205_part_number t205, my_temp_part_list my_temp_loc, (
                 SELECT t5055.c5052_location_id, t5055.c205_part_number_id, NVL (SUM (t5055.c5055_qty), 0) qty
                   FROM t5055_control_number t5055, my_temp_part_list my_temp_loc
                  WHERE t5055.c901_type            = 93343 -- pick transacted
                  	AND my_temp_loc.c205_part_number_id = t5055.c5052_location_id
                    AND t5055.c5055_void_fl       IS NULL
                    AND t5055.c5055_update_inv_fl IS NULL
                    AND t5055.c5040_plant_id       = p_plant_id -- plant id
               GROUP BY t5055.c5052_location_id, t5055.c205_part_number_id
            )
        alloc
      WHERE t5051.c5051_inv_warehouse_id = t5052.c5051_inv_warehouse_id(+)
      	AND my_temp_loc.c205_part_number_id = t5052.c5052_location_id 
        AND t5052.c5052_location_id      = t5053.c5052_location_id(+)
        AND t5053.c205_part_number_id    = t205.c205_part_number_id
        AND t5053.c205_part_number_id    = alloc.c205_part_number_id (+)
        AND t5053.c5052_location_id      = alloc.c5052_location_id(+)
        AND t5051.c901_status_id         = 1 -- active
        AND t5051.c5051_inv_warehouse_id = p_warehouse_id -- fg warehouse,rw warehouse
        AND t5052.c901_status            = 93310 --active
        AND t5052.c901_location_type     = p_location_type --  bulk,pick face
        AND t5052.c5052_void_fl         IS NULL
        AND t5052.c5040_plant_id         = p_plant_id -- audubon
        AND t5053.c205_part_number_id     IS NOT NULL;
   
BEGIN
	-- to set the parts to temp table
	DELETE FROM my_temp_part_list;

	 INSERT INTO my_temp_part_list
	    (c205_part_number_id
	    )
	 SELECT c5052_location_id
	   FROM t2704_cycle_count_schedule
	  WHERE c2700_cycle_count_id = p_cycle_count_id
	  	AND c2701_cyc_cnt_location_id = p_loc_id
	    AND c2704_schedule_date       = TRUNC (p_schedule_date)
	    AND c2704_void_fl IS NULL;
	-- Based on warehouse type - to get the costing type
	--    
	SELECT get_rule_value (c901_warehouse_location, 'CYC_CNT_COSTING_MAP')
		INTO v_costing_type
	FROM t2701_cycle_count_location
	WHERE c2701_cyc_cnt_location_id = p_loc_id;   
    -- to loop the cursor and insert lock details
    FOR inv_loc IN inv_location_cur
    LOOP
    	--based on costing ID - to get the Cost and Local cost value
    	BEGIN
			SELECT t820.c820_purchase_amt
			  , t820.c820_local_company_cost
			  , t820.c901_owner_currency
			  , t820.c901_local_company_currency
			   INTO v_cost
			  , v_local_cost
			  , v_owner_currency
			  , v_local_currency
			   FROM t820_costing t820
			  WHERE t820.c820_costing_id = inv_loc.costing_id;
	   EXCEPTION WHEN NO_DATA_FOUND
	   THEN
	   		v_cost:= 0;
	   		v_local_cost:= 0;
	   		v_owner_currency := NULL;
	   		v_local_currency := NULL;
	   END;
	   --
        gm_pkg_ac_cycle_count_lock.gm_sav_cycle_cnt_lock_dtls (p_lock_id, inv_loc.pnum, inv_loc.location_id,
        inv_loc.qty, v_cost, v_local_cost, v_owner_currency, v_local_currency, p_user_id) ;   
    END LOOP;
     -- to update the D rank parts to counted Qty
    UPDATE T2706_CYCLE_COUNT_LOCK_DTLS SET C2706_PHYSICAL_QTY = C2706_SYSTEM_QTY, 
    C101_COUNTED_BY = p_user_id, C2706_COUNTED_DATE = CURRENT_DATE,
    C2706_LAST_UPDATED_BY=p_user_id,C2706_LAST_UPDATED_DATE = CURRENT_DATE    
    WHERE C2705_CYC_CNT_LOCK_ID = p_lock_id
    AND C205_PART_NUMBER_ID IN
        (
             SELECT t2703.C205_PART_NUMBER_ID
               FROM t2703_cycle_count_rank t2703
              WHERE t2703.c2700_cycle_count_id = p_cycle_count_id
                AND t2703.c2703_rank           = 'D'
                AND t2703.c2703_void_fl       IS NULL
        );
    

END gm_inv_location_warehouse;
/**********************************************************************************************
* description      : procedure used to get the system qty (inventory - t205c) based on parts and insert to lock
details table.
* author           : mmuthusamy
***********************************************************************************************/
PROCEDURE gm_inv_lock_by_project (
        p_cycle_count_id IN t2700_cycle_count.c2700_cycle_count_id%type,
        p_loc_id         IN t2701_cycle_count_location.c2701_cyc_cnt_location_id%type,
        p_lock_id        IN t2705_cycle_count_lock.c2705_cyc_cnt_lock_id%type,
        p_schedule_date  IN t2704_cycle_count_schedule.c2704_schedule_date%type,
        p_user_id        IN t2700_cycle_count.c2700_last_updated_by%type)
AS
    v_pnum t205_part_number.c205_part_number_id%type;
    v_repack_qty NUMBER;
    v_bulk_qty   NUMBER;
    v_qn_qty     NUMBER;
    v_rm_qty     NUMBER;
    v_ih_qty     NUMBER;
    --
    v_warehouse_type NUMBER;
    v_out_cur types.cursor_type;
    v_rule_inv_map_id  VARCHAR2 (400) ;
    v_exculde_inv_type VARCHAR2 (4000) ;
    --
    v_owner_company_id NUMBER;
    v_cost NUMBER;
    v_local_cost NUMBER;
    v_costing_id t820_costing.c820_costing_id%TYPE;
    v_costing_type VARCHAR2 (20);
    v_owner_currency t2706_cycle_count_lock_dtls.c901_owner_currency%TYPE;
    v_local_currency t2706_cycle_count_lock_dtls.c901_local_company_currency%TYPE;
BEGIN
    -- to get the warehouse type
     SELECT c901_warehouse_location, NVL (get_rule_value (c901_warehouse_location, 'CYC_CNT_LOCK_INV'), 0)
     	, get_rule_value (c901_warehouse_location,'CYC_CNT_COSTING_MAP')
       INTO v_warehouse_type, v_rule_inv_map_id
       	, v_costing_type
       FROM t2701_cycle_count_location
      WHERE c2701_cyc_cnt_location_id = p_loc_id
        AND c2701_void_fl            IS NULL;
    --
    dbms_output.put_line (' v_rule_inv_map_id '|| v_rule_inv_map_id) ;
    
    -- exclude the inventory type
    my_context.set_my_cloblist (v_rule_inv_map_id || ',') ;
    --
     SELECT rtrim (xmlagg (xmlelement (e, c901_code_id || ',')) .extract ('//text()'), ',') ||','
       INTO v_exculde_inv_type
       FROM t901_code_lookup
      WHERE c901_code_grp     = 'INVBKT'
        AND c901_active_fl    = 1
        AND c901_code_id NOT IN
        (
             SELECT token FROM v_clob_list
        ) ;
    --
    dbms_output.put_line (' v_exculde_inv_type '|| v_exculde_inv_type) ;
    --set the exclusion so the "v205_qty_in_stock" returns data faster.
    
    my_context.set_my_inlist_ctx (v_exculde_inv_type) ;
    
    -- to fetch the parts from (t205)
    gm_fetch_part_qty_dtls (p_cycle_count_id, p_loc_id, p_lock_id, p_schedule_date, v_out_cur) ;
    
    -- loop the cursor
    LOOP
        FETCH v_out_cur
           INTO v_pnum, v_bulk_qty, v_repack_qty
          , v_rm_qty, v_qn_qty, v_ih_qty;
        IF v_pnum               IS NOT NULL THEN
        	-- Based on warehouse type - to get the costing type
        	SELECT NVL(get_ac_costing_id(v_pnum,v_costing_type),get_ac_costing_id(v_pnum,4900))  INTO v_costing_id
        	FROM DUAL;
        	--based on costing ID - to get the Cost and Local cost value
        	BEGIN
			SELECT t820.c1900_owner_company_id
			  , t820.c820_purchase_amt
			  , t820.c820_local_company_cost
			  , t820.c901_owner_currency
			  , t820.c901_local_company_currency
			   INTO v_owner_company_id
			  , v_cost
			  , v_local_cost
			  , v_owner_currency
			  , v_local_currency
			   FROM t820_costing t820
			  WHERE t820.c820_costing_id =v_costing_id;
	   EXCEPTION WHEN NO_DATA_FOUND
	   THEN
	   		v_owner_company_id := NULL;
	   		v_cost:= 0;
	   		v_local_cost:= 0;
	   		v_owner_currency := NULL;
	   		v_local_currency := NULL;
	   END;
            IF (v_warehouse_type = '106804') -- repack
                THEN
                gm_pkg_ac_cycle_count_lock.gm_sav_cycle_cnt_lock_dtls (p_lock_id, v_pnum, NULL, v_repack_qty, v_cost, v_local_cost, v_owner_currency, v_local_currency, p_user_id
                ) ;
            elsif (v_warehouse_type = '106805') -- qn
                THEN
                gm_pkg_ac_cycle_count_lock.gm_sav_cycle_cnt_lock_dtls (p_lock_id, v_pnum, NULL, v_qn_qty, v_cost, v_local_cost, v_owner_currency, v_local_currency, p_user_id) ;
            elsif (v_warehouse_type = '106806') -- qn (s)
                THEN
                gm_pkg_ac_cycle_count_lock.gm_sav_cycle_cnt_lock_dtls (p_lock_id, v_pnum, NULL, v_qn_qty, v_cost, v_local_cost, v_owner_currency, v_local_currency, p_user_id) ;
            elsif (v_warehouse_type = '106808') -- bulk loose
                THEN
                gm_pkg_ac_cycle_count_lock.gm_sav_cycle_cnt_lock_dtls (p_lock_id, v_pnum, NULL, v_bulk_qty, v_cost, v_local_cost, v_owner_currency, v_local_currency, p_user_id)
                ;
            elsif (v_warehouse_type = '106809') -- ih loose
                THEN
                gm_pkg_ac_cycle_count_lock.gm_sav_cycle_cnt_lock_dtls (p_lock_id, v_pnum, NULL, v_ih_qty, v_cost, v_local_cost, v_owner_currency, v_local_currency, p_user_id) ;
            elsif (v_warehouse_type = '106807') -- rm
                THEN
                gm_pkg_ac_cycle_count_lock.gm_sav_cycle_cnt_lock_dtls (p_lock_id, v_pnum, NULL, v_rm_qty, v_cost, v_local_cost, v_owner_currency, v_local_currency, p_user_id) ;
            END IF; -- end if warehouse type check
        END IF; -- end if part number check
        EXIT
    WHEN v_out_cur%notfound;
        v_pnum       := NULL;
        v_bulk_qty   := NULL;
        v_repack_qty := NULL;
        v_rm_qty     := NULL;
        v_qn_qty     := NULL;
        v_ih_qty     := NULL;
    END LOOP;
    --
END gm_inv_lock_by_project;
/**********************************************************************************************
* description      : procedure used to get the system qty based on part number
* author           : mmuthusamy
***********************************************************************************************/
PROCEDURE gm_fetch_part_qty_dtls (
        p_cycle_count_id IN t2700_cycle_count.c2700_cycle_count_id%type,
        p_loc_id         IN t2701_cycle_count_location.c2701_cyc_cnt_location_id%type,
        p_lock_id        IN t2705_cycle_count_lock.c2705_cyc_cnt_lock_id%type,
        p_schedule_date  IN t2704_cycle_count_schedule.c2704_schedule_date%type,
        p_out_cur OUT types.cursor_type)
AS
BEGIN
    --1 delete the temp table
     DELETE FROM my_temp_part_list;
    -- 2 insert the temp table (parts)
     INSERT
       INTO my_temp_part_list
        (
            c205_part_number_id
        )
     SELECT t2704.c205_part_number_id
       FROM t2704_cycle_count_schedule t2704
      WHERE t2704.c205_part_number_id        IS NOT NULL
        AND t2704.c2700_cycle_count_id        = p_cycle_count_id
        AND t2704.c2701_cyc_cnt_location_id   = p_loc_id
        AND TRUNC (t2704.c2704_schedule_date) = TRUNC (p_schedule_date)
        AND t2704.c2704_void_fl              IS NULL
        AND t2704.c205_part_number_id NOT    IN
        (
             SELECT t2703.C205_PART_NUMBER_ID
               FROM t2703_cycle_count_rank t2703
              WHERE t2703.c2700_cycle_count_id = p_cycle_count_id
                AND t2703.c2703_rank           = 'D'
                AND t2703.c2703_void_fl       IS NULL
        ) ;
        
    -- 3. to get the parts from v205
    OPEN p_out_cur FOR SELECT c205_part_number_id pnum,
    NVL (c205_inbulk, 0) in_bulk_qty,
    NVL (c205_repackavail, 0) repack_qty,
    NVL (rmqty, 0) raw_material_qty,
    NVL (c205_inquaran, 0) qn_qty,
    NVL (c205_ih_avail_qty, 0) ih_qty FROM v205_qty_in_stock;
END gm_fetch_part_qty_dtls;
/**********************************************************************************************
* description      : procedure used to get the system qty based on consignment/set and insert to lock details
table.
* author           : mmuthusamy
***********************************************************************************************/
PROCEDURE gm_inv_lock_by_set (
        p_cycle_count_id IN t2700_cycle_count.c2700_cycle_count_id%type,
        p_loc_id         IN t2701_cycle_count_location.c2701_cyc_cnt_location_id%type,
        p_lock_id        IN t2705_cycle_count_lock.c2705_cyc_cnt_lock_id%type,
        p_schedule_date  IN t2704_cycle_count_schedule.c2704_schedule_date%type,
        p_user_id        IN t2700_cycle_count.c2700_last_updated_by%type)
AS
    v_warehouse_type NUMBER;
BEGIN
    -- to get the warehouse type
     SELECT c901_warehouse_location
       INTO v_warehouse_type
       FROM t2701_cycle_count_location
      WHERE c2701_cyc_cnt_location_id = p_loc_id
        AND c2701_void_fl            IS NULL;
    -- 52278 wip - set
    --52279 wip – loaner
    --52280 inhouse – set
    dbms_output.put_line(' ware house type is  '|| v_warehouse_type);
    --
    IF v_warehouse_type = 106810 THEN
        gm_pkg_ac_cycle_count_lock.gm_inv_lock_wip_set (p_cycle_count_id, p_loc_id, p_lock_id, p_schedule_date,p_user_id) ;
        
    ELSIF v_warehouse_type = 106811 THEN
        gm_pkg_ac_cycle_count_lock.gm_inv_lock_wip_loaner (p_cycle_count_id, p_loc_id, p_lock_id, p_schedule_date,p_user_id) ;
        
    ELSIF v_warehouse_type = 106812 THEN
        gm_pkg_ac_cycle_count_lock.gm_inv_lock_inhouse_set (p_cycle_count_id, p_loc_id, p_lock_id, p_schedule_date,p_user_id) ;
    
	ELSIF v_warehouse_type = 106813 THEN
	-- Built Set
        gm_pkg_ac_cycle_count_lock.gm_inv_lock_built_set (p_cycle_count_id, p_loc_id, p_lock_id, p_schedule_date,p_user_id) ;
		
    END IF; -- end if warehouse type check
END gm_inv_lock_by_set;
/**********************************************************************************************
* description      : procedure used to get the system qty based on wip consignment and insert to lock details
table.
* author           : mmuthusamy
***********************************************************************************************/
PROCEDURE gm_inv_lock_wip_set (
        p_cycle_count_id IN t2700_cycle_count.c2700_cycle_count_id%type,
        p_loc_id         IN t2701_cycle_count_location.c2701_cyc_cnt_location_id%type,
        p_lock_id        IN t2705_cycle_count_lock.c2705_cyc_cnt_lock_id%type,
        p_schedule_date  IN t2704_cycle_count_schedule.c2704_schedule_date%type,
        p_user_id        IN t2700_cycle_count.c2700_last_updated_by%type)
AS
	v_plant_cnt_id t5040_plant_master.c5040_plant_id%type;
	--
	
    CURSOR wip_set_dtls_cur
    IS
     SELECT wipinv.c504_consignment_id Cn_id, DECODE (wipinv.c504_status_fl, 1, 'WIP', NULL) cn_Status, wipinv.c207_set_id
    Set_ID, get_set_name (wipinv.c207_set_id) Set_name, NVL (taggedcns.invtagid, get_tagid_from_txn_id (
    wipinv.c504_consignment_id)) tag_id, taggedcns.invlocid cn_location, (
         SELECT c5052_location_cd
           FROM t5052_location_master t5052
          WHERE t5052.c5052_location_id = taggedcns.invlocid
    )
    location_cd
   FROM
    (
         SELECT t504.c504_consignment_id, t504.c701_distributor_id, t504.c504_status_fl
          , t504.c504_void_fl, t504.c704_account_id, t504.c207_set_id
          , t504.c504_type, t504.c504_verify_fl, t504.c504_verified_date
          , t504.c504_verified_by, t504.c520_request_id, t504.c504_master_consignment_id
          , t504.c504_last_updated_by, t504.c504_last_updated_date, t504.c1900_company_id
          , t504.c5040_plant_id
           FROM t504_consignment t504
          WHERE t504.C504_CONSIGNMENT_ID IN
            (
                 SELECT t2704.C504_CONSIGNMENT_ID
                   FROM T2704_CYCLE_COUNT_SCHEDULE t2704
                  WHERE t2704.C2701_CYC_CNT_LOCATION_ID = p_loc_id
                    AND t2704.C2704_SCHEDULE_DATE       = TRUNC (p_schedule_date)
                    AND t2704.c2704_void_fl IS NULL
            )
            AND t504.c504_void_fl    IS NULL
            --AND c504_consignment_id = 'GM-CN-1143837'
    )
    wipinv, (
         SELECT T5010.c5010_last_updated_trans_id lastupdtransid, T5053.C5010_TAG_ID invtagid, t5053.c5052_location_id
            invlocid
           FROM t5010_tag t5010, t5051_inv_warehouse t5051, t5052_location_master t5052
          , t5053_location_part_mapping T5053
          WHERE t5010.c5010_tag_id           = t5053.c5010_tag_id
            AND t5051.c5051_inv_warehouse_id = t5052.c5051_inv_warehouse_id
            AND t5052.c5052_location_id      = t5053.c5052_location_id
            AND T5052.c901_location_type    IN (93501) --Consignment
            AND t5052.c5052_void_fl         IS NULL
            AND t5010.c5010_void_fl         IS NULL
            AND t5051.c901_status_id         = 1
            AND t5052.c5040_plant_id = v_plant_cnt_id
            AND t5051.c901_warehouse_type    = 90800 --FG Warehouse
    )
    taggedcns
  WHERE wipinv.c504_consignment_id = taggedcns.lastupdtransid(+);
BEGIN
	dbms_output.put_line(' called WIP set  '|| p_lock_id);
	--
	   --1 to get the plant
     SELECT get_plantid_frm_cntx ()
       INTO v_plant_cnt_id
       FROM dual;
    FOR wip_set_dtls IN wip_set_dtls_cur
    LOOP
    	gm_pkg_ac_cycle_count_lock.gm_sav_cycle_cnt_lock_cn(p_lock_id, wip_set_dtls.Cn_id, wip_set_dtls.Set_ID, wip_set_dtls.Set_name
    	, NULL, wip_set_dtls.tag_id, 'WIP Set', NULL, wip_set_dtls.cn_location, NULL, p_user_id);
    	--
    	dbms_output.put_line(' CN Details '|| wip_set_dtls.Cn_id);
    END LOOP;
END gm_inv_lock_wip_set;
/**********************************************************************************************
* description      : procedure used to get the system qty based on wip loaner and insert to lock details table.
* author           : mmuthusamy
***********************************************************************************************/
PROCEDURE gm_inv_lock_wip_loaner (
        p_cycle_count_id IN t2700_cycle_count.c2700_cycle_count_id%type,
        p_loc_id         IN t2701_cycle_count_location.c2701_cyc_cnt_location_id%type,
        p_lock_id        IN t2705_cycle_count_lock.c2705_cyc_cnt_lock_id%type,
        p_schedule_date  IN t2704_cycle_count_schedule.c2704_schedule_date%type,
        p_user_id        IN t2700_cycle_count.c2700_last_updated_by%type)
AS
    CURSOR wip_loaner_cur
    IS
    SELECT t504.c504_consignment_id Cn_id, t504.c207_set_id Set_ID, get_set_name (t504.c207_set_id) Set_name
  , t504a.c504a_etch_id etchid, gm_pkg_op_loaner.get_loaner_status (T504A.C504A_STATUS_FL) loaner_status
  , 'WIP Loaner' cn_location, t5010.C5010_TAG_ID tag_id, t5010.C205_PART_NUMBER_ID pnum
   FROM t504_consignment t504, t504a_consignment_loaner t504a
   , t5010_tag t5010
  WHERE t504.c207_set_id        IS NOT NULL
  AND t504.c504_consignment_id = t504a.c504_consignment_id (+)
  AND t504.c504_consignment_id = t5010.C5010_LAST_UPDATED_TRANS_ID (+)
    AND t504.c504_void_fl            IS NULL
    -- PBUG-2769 (to avoid the Pending Return status)
    AND NVL(t504a.c504a_status_fl, 0) <> 20
    AND t5010.c5010_void_fl IS NULL
    AND t504.C504_CONSIGNMENT_ID IN
            (
                 SELECT t2704.C504_CONSIGNMENT_ID
                   FROM T2704_CYCLE_COUNT_SCHEDULE t2704
                  WHERE t2704.C2701_CYC_CNT_LOCATION_ID = p_loc_id
                    AND t2704.C2704_SCHEDULE_DATE       = TRUNC (p_schedule_date)
                    AND t2704.c2704_void_fl IS NULL
            );
BEGIN
    FOR wip_loaner IN wip_loaner_cur
    LOOP
    	gm_pkg_ac_cycle_count_lock.gm_sav_cycle_cnt_lock_cn(p_lock_id, wip_loaner.Cn_id, wip_loaner.Set_ID, wip_loaner.Set_name
    	, wip_loaner.etchid, wip_loaner.tag_id, wip_loaner.cn_location, wip_loaner.loaner_status, NULL, wip_loaner.pnum, p_user_id);
    	--
    	dbms_output.put_line(' CN Details '|| wip_loaner.Cn_id);
    END LOOP;
END gm_inv_lock_wip_loaner;
/**********************************************************************************************
* description      : procedure used to get the system qty based on inhouse set and insert to lock details table.
* author           : mmuthusamy
***********************************************************************************************/
PROCEDURE gm_inv_lock_inhouse_set (
        p_cycle_count_id IN t2700_cycle_count.c2700_cycle_count_id%type,
        p_loc_id         IN t2701_cycle_count_location.c2701_cyc_cnt_location_id%type,
        p_lock_id        IN t2705_cycle_count_lock.c2705_cyc_cnt_lock_id%type,
        p_schedule_date  IN t2704_cycle_count_schedule.c2704_schedule_date%type,
        p_user_id        IN t2700_cycle_count.c2700_last_updated_by%type)
AS
	v_location_id t5052_location_master.c5052_location_id%TYPE;
	--
    CURSOR inhouse_set_cur
    IS
     SELECT t504.c504_consignment_id Cn_id, t504.c207_set_id Set_ID, get_set_name (t504.c207_set_id) Set_name
  , t504a.c504a_etch_id etchid, gm_pkg_op_loaner.get_loaner_status (T504A.C504A_STATUS_FL) loaner_status
  , 'Inhouse Set' cn_location, t5010.c5010_tag_id tag_id
   FROM t504_consignment t504, t504a_consignment_loaner t504a
   , t5010_tag t5010
  WHERE t504.c207_set_id        IS NOT NULL
  AND t504.c504_consignment_id = t504a.c504_consignment_id (+)
   AND t504.c504_consignment_id = t5010.c5010_last_updated_trans_id (+)
    AND t504.c504_type           = 4119
    -- PBUG-2769 (to avoid the Pending Return status)
    AND NVL(t504a.c504a_status_fl, 0) <> 20  
    AND t504.c504_void_fl            IS NULL
    AND t5010.c5010_void_fl (+) IS NULL
    AND t504.C504_CONSIGNMENT_ID IN
            (
                 SELECT t2704.C504_CONSIGNMENT_ID
                   FROM T2704_CYCLE_COUNT_SCHEDULE t2704
                  WHERE t2704.C2701_CYC_CNT_LOCATION_ID = p_loc_id
                    AND t2704.C2704_SCHEDULE_DATE       = TRUNC (p_schedule_date)
                    AND t2704.c2704_void_fl IS NULL
            );
BEGIN
      FOR inhouse_set IN inhouse_set_cur
    LOOP
    	-- based on Tag id to get the Location id
    	BEGIN
    	 SELECT c5052_location_id INTO v_location_id
		   FROM t5053_location_part_mapping
		  WHERE c5010_tag_id =  inhouse_set.tag_id; 
		EXCEPTION
    WHEN OTHERS THEN
    	v_location_id := NULL;
    END;
    	--
    	gm_pkg_ac_cycle_count_lock.gm_sav_cycle_cnt_lock_cn(p_lock_id, inhouse_set.Cn_id, inhouse_set.Set_ID, inhouse_set.Set_name
    	, inhouse_set.etchid, inhouse_set.tag_id, inhouse_set.cn_location, inhouse_set.loaner_status, v_location_id, NULL,p_user_id);
    	--
    	dbms_output.put_line(' CN Details '|| inhouse_set.Cn_id);
    END LOOP;
END gm_inv_lock_inhouse_set;

/**********************************************************************************************
* description      : procedure used to insert the cycle count lock details
* author           : mmuthusamy
***********************************************************************************************/
PROCEDURE gm_sav_cycle_cnt_lock_cn
    (
        p_cyc_cnt_lock_id IN t2706_cycle_count_lock_dtls.c2705_cyc_cnt_lock_id%type,
        p_cn_id     IN t2706_cycle_count_lock_dtls.c504_consignment_id%type,
        p_set_id     IN t2706_cycle_count_lock_dtls.c207_set_id%type,
        p_set_name      IN t2706_cycle_count_lock_dtls.C207_SET_NAME%type,
        p_etch_id  IN t2706_cycle_count_lock_dtls.C504_ETCH_ID%type,
        p_tag_id  IN t2706_cycle_count_lock_dtls.C5010_TAG_ID%type,
        p_cn_location  IN t2706_cycle_count_lock_dtls.C2706_CN_LOCATION%type,
        p_cn_status  IN t2706_cycle_count_lock_dtls.C2706_CN_STATUS%type,
        p_location_id IN t2706_cycle_count_lock_dtls.C5052_LOCATION_ID%type,
        p_part_number IN t2706_cycle_count_lock_dtls.C205_part_number_id%type,
        p_last_update_by  IN t2706_cycle_count_lock_dtls.c2706_last_updated_by%type
    )
AS
    v_err NUMBER;
BEGIN
    -- to handle the exception - if part #, location not available
    BEGIN
         INSERT
           INTO t2706_cycle_count_lock_dtls
            (
                c2706_cyc_cnt_lock_dtls_id, c2705_cyc_cnt_lock_id, c504_consignment_id
              , c207_set_id, C207_SET_NAME, C504_ETCH_ID, C5010_TAG_ID, 
              C2706_CN_LOCATION, C2706_CN_STATUS, C5052_LOCATION_ID, c2706_system_qty
              ,c205_part_number_id, c2706_last_updated_by, c2706_last_updated_date
            )
            VALUES
            (
                s2706_cycle_count_lock_dtls.nextval, p_cyc_cnt_lock_id, p_cn_id
              , p_set_id, p_set_name, p_etch_id
              , p_tag_id, p_cn_location, p_cn_status, p_location_id, 1
              , p_part_number, p_last_update_by, CURRENT_DATE
            ) ;
    EXCEPTION
    WHEN OTHERS THEN
        v_err := 1;
    END;
END gm_sav_cycle_cnt_lock_cn;

/**********************************************************************************************
* description      : procedure used to get the system qty based on built set and insert to lock details table.
* author           : mmuthusamy
***********************************************************************************************/
PROCEDURE gm_inv_lock_built_set (
        p_cycle_count_id IN t2700_cycle_count.c2700_cycle_count_id%type,
        p_loc_id         IN t2701_cycle_count_location.c2701_cyc_cnt_location_id%type,
        p_lock_id        IN t2705_cycle_count_lock.c2705_cyc_cnt_lock_id%type,
        p_schedule_date  IN t2704_cycle_count_schedule.c2704_schedule_date%type,
        p_user_id        IN t2700_cycle_count.c2700_last_updated_by%type)
AS
	v_plant_cnt_id t5040_plant_master.c5040_plant_id%type;
	--
    CURSOR built_set_cur
    IS
     SELECT bsinv.c504_consignment_id cn_id, bsinv.c207_set_id set_id, get_set_name (bsinv.c207_set_id) set_name
		   , NVL(taggedcns.invtagid,get_tagid_from_txn_id(bsinv.c504_consignment_id)) tag_id, 'Build Set' cn_location
		   , taggedcns.invlocid location_id
		   FROM
			(
				 SELECT t504.c504_consignment_id, t504.c701_distributor_id, t504.c504_status_fl
				  , t504.c504_void_fl, t504.c704_account_id, t504.c207_set_id
				  , t504.c504_type, t504.c504_verify_fl, t504.c504_verified_date
				  , t504.c504_verified_by, t504.c520_request_id, t504.c504_master_consignment_id
				  , t504.c504_last_updated_by, t504.c504_last_updated_date
				   FROM t504_consignment t504, t906_rules t906
				  WHERE t504.c504_status_fl = t906.c906_rule_value --            IN ('2','2.20','3','3.30','3.60' ) -- 2 Built
					-- Set  /2.20 Pending Pick  /3 Pending Shipping  /3.30 Packing In Progress /3.60 Ready for Pickup
					AND t504.c504_verify_fl   = '1'
					AND t504.c504_void_fl    IS NULL
					AND t504.c207_set_id     IS NOT NULL
					AND t906.c906_rule_id     = 'PI_BUILTSETSTATUS'
					AND t906.c906_rule_grp_id = 'PI_LOCK'
					AND t906.c906_void_fl    IS NULL
					AND t504.c5040_plant_id = v_plant_cnt_id
                    AND t504.C504_CONSIGNMENT_ID IN
            (
                 SELECT t2704.C504_CONSIGNMENT_ID
                   FROM T2704_CYCLE_COUNT_SCHEDULE t2704
                  WHERE t2704.C2701_CYC_CNT_LOCATION_ID = p_loc_id
                    AND t2704.C2704_SCHEDULE_DATE       = TRUNC (p_schedule_date)
                    AND t2704.c2704_void_fl IS NULL
            )
			)
			bsinv, (
				 SELECT T5010.c5010_last_updated_trans_id lastupdtransid, T5053.C5010_TAG_ID invtagid, t5053.c5052_location_id
					invlocid
				   FROM t5010_tag t5010, t5051_inv_warehouse t5051, t5052_location_master t5052
				  , t5053_location_part_mapping T5053
				  WHERE t5010.c5010_tag_id           = t5053.c5010_tag_id
					AND t5051.c5051_inv_warehouse_id = t5052.c5051_inv_warehouse_id
					AND t5052.c5052_location_id      = t5053.c5052_location_id
					AND T5052.c901_location_type    IN (93501) --Consignment
					AND t5052.c5052_void_fl         IS NULL
					AND t5010.c5010_void_fl         IS NULL
					AND t5051.c901_status_id         = 1
					AND t5051.c901_warehouse_type    = 90800 --FG Warehouse
					AND t5052.c5040_plant_id = v_plant_cnt_id
			)
			taggedcns
		  WHERE bsinv.c504_consignment_id = taggedcns.lastupdtransid(+) ;
BEGIN

 --1 to get the plant
     SELECT get_plantid_frm_cntx ()
       INTO v_plant_cnt_id
       FROM dual;
       --
	   dbms_output.put_line (' Scheduler location id  ' || p_loc_id ||' Plant Id '|| v_plant_cnt_id);
      FOR built_set IN built_set_cur
    LOOP
    	
    	--
    	gm_pkg_ac_cycle_count_lock.gm_sav_cycle_cnt_lock_cn(p_lock_id, built_set.Cn_id, built_set.Set_ID, built_set.Set_name
    	, NULL, built_set.tag_id, built_set.cn_location, NULL, built_set.location_id, NULL,p_user_id);
    	--
    	dbms_output.put_line(' CN Details '|| built_set.Cn_id);
    END LOOP;
END gm_inv_lock_built_set;

END gm_pkg_ac_cycle_count_lock;
/
