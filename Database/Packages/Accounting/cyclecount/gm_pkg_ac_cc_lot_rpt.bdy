CREATE OR REPLACE PACKAGE BODY gm_pkg_ac_cc_lot_rpt
IS

/****************************************************************
* Description : Fetch the Scanned Lot Report Details
* Author           : MKosalram
*****************************************************************/
PROCEDURE gm_fch_lot_details(
		p_cyc_cnt_lock_id	IN	t2708_cycle_count_scanned_lots.c2705_cyc_cnt_lock_id%TYPE,
		p_part_num   		IN	t205_part_number.c205_part_number_id%TYPE,
		P_consign_id 		IN	t2708_cycle_count_scanned_lots.c504_consignment_id%TYPE,
		p_location_id		IN	t2708_cycle_count_scanned_lots.c5052_location_id%TYPE,
 		p_out_str  			OUT CLOB)
 		
AS	
v_date_format 		VARCHAR2 (20);
v_cyc_cnt_lock_id	t2708_cycle_count_scanned_lots.c2705_cyc_cnt_lock_id%TYPE:= UPPER(p_cyc_cnt_lock_id);
v_pnum		 	 	t205_part_number.c205_part_number_id%TYPE:= UPPER(p_part_num);
v_consign_id 		t2708_cycle_count_scanned_lots.c504_consignment_id%TYPE:= UPPER(p_consign_id);
v_location_id 		t2708_cycle_count_scanned_lots.c5052_location_id%TYPE:= UPPER(p_location_id);
BEGIN

	  SELECT get_compdtfmt_frm_cntx()
        INTO v_date_format
        FROM dual;
        
	  SELECT JSON_ARRAYAGG(
	         JSON_OBJECT( 
		     'cyccntlockid' VALUE CYC_CNT_LOCK_ID, 
             'locationid' 	VALUE LOCATION_ID,
             'partnm' 		VALUE PART_NUMBER_ID , 
             'lotnm' 		VALUE CONTROL_NUMBER, 
             'qty' 			VALUE QTY,
             'scannedlot' 	VALUE CONTROL_NUMBER,
             'consignid' 	VALUE CONSIGNMENT_ID,
             'scannedby' 	VALUE SCANNED_BY,
             'scanneddate' 	VALUE SCANNED_DATE)
    ORDER BY CYC_CNT_LOCK_ID RETURNING CLOB) INTO p_out_str 
        FROM (
	  SELECT T2708.C2705_CYC_CNT_LOCK_ID CYC_CNT_LOCK_ID
	  		 ,T5052.C5052_LOCATION_CD LOCATION_ID
	  		 ,T2708.C205_PART_NUMBER_ID PART_NUMBER_ID
	  		 ,T2708.C2550_CONTROL_NUMBER CONTROL_NUMBER
	  		 ,T2708.C2708_QTY QTY
	  		 ,T2708.C2550_CONTROL_NUMBER CONTROL_NUMBER_UDI
	  		 ,T2708.C504_CONSIGNMENT_ID CONSIGNMENT_ID
	  		 ,get_user_name (T2708.C2708_SCANNED_BY) SCANNED_BY
	  		 ,TO_CHAR (T2708.C2708_SCANNED_DATE,v_date_format) SCANNED_DATE
        FROM t2708_CYCLE_COUNT_SCANNED_LOTS T2708, t2705_CYCLE_COUNT_LOCK t2705, t5052_LOCATION_MASTER t5052 
       WHERE T2708.C2705_CYC_CNT_LOCK_ID = t2705.C2705_CYC_CNT_LOCK_ID
         AND T2708.C2705_CYC_CNT_LOCK_ID = NVL(v_cyc_cnt_lock_id,T2708.C2705_CYC_CNT_LOCK_ID)
         AND NVL(T2708.C5052_LOCATION_ID,'-9999') = t5052.C5052_LOCATION_ID(+)
         AND REGEXP_LIKE(T2708.C205_PART_NUMBER_ID, CASE WHEN TO_CHAR(v_pnum) IS NOT NULL THEN TO_CHAR(REGEXP_REPLACE(v_pnum,'[+]','\+')) ELSE REGEXP_REPLACE(T2708.C205_PART_NUMBER_ID,'[+]','\+') END)
         AND NVL(T2708.C504_CONSIGNMENT_ID,'-9999') =  NVL(v_consign_id, NVL(T2708.C504_CONSIGNMENT_ID,'-9999'))
         AND NVL(T5052.C5052_LOCATION_CD,'-9999') =  NVL(v_location_id, NVL(T5052.C5052_LOCATION_CD,'-9999'))
         AND NVL(t2705.C2705_LOT_TRACK_FL,'-9999') = 'Y'
         AND t2705.C2705_VOID_FL IS NULL
         AND T2708.C2708_VOID_FL IS NULL);
END gm_fch_lot_details;

END gm_pkg_ac_cc_lot_rpt;
/