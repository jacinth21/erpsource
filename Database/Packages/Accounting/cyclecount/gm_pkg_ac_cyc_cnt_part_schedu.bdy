--@"c:/database/packages/operations/purchasing/gm_pkg_ac_cyc_cnt_part_schedu.bdy";
CREATE OR REPLACE PACKAGE body gm_pkg_ac_cyc_cnt_part_schedu
IS

 /**********************************************************************************************
    * Description      : Procedure used to Schedule the Part/Location to full year (Main proceduer)
    * 
    * 				   : Flow diagram update to below path - C:\Database\SQL\Accounts\cyclecount\CC_Scheduler_Flow_Diagram.zip
    * 
    * Author           : mmuthusamy
    ***********************************************************************************************/
PROCEDURE gm_sav_cc_scheduler_main (
        p_year IN t2700_cycle_count.c2700_cycle_count_year%TYPE,
        p_company_id         IN t1900_company.c1900_company_id%TYPE,
        p_plant_cnt_id IN t5040_plant_master.c5040_plant_id%type)
AS
    v_cycle_count_id t2700_cycle_count.c2700_cycle_count_id%type;
    v_user_id NUMBER := 30301;
    --
    v_out_location_cur types.cursor_type;
    v_cyc_cnt_location_id t2701_cycle_count_location.c2701_cyc_cnt_location_id%TYPE;
    v_count_pattern t2701_cycle_count_location.c901_count_pattern%TYPE;
    v_lock_type t2701_cycle_count_location.C901_LOCK_TYPE%TYPE;
    v_comments	t902_log.c902_comments%TYPE;
    v_message	t902_log.c902_comments%TYPE;
    --
BEGIN
	--
	IF (p_year IS NULL OR p_company_id IS NULL OR p_plant_cnt_id IS NULL)
	THEN
		raise_application_error ( - 20999, 'Please provide the correct information - Year, Company and Plant ID ') ;
	END IF;
	
    --1 to set the company and plant to context
     gm_pkg_cor_client_context.gm_sav_client_context(p_company_id, p_plant_cnt_id);
     -- to set the context (its used for V1900)
     
     my_context.set_my_inlist_ctx ('01/01/' || p_year) ;
     
    --2 to get the cycle count id
    BEGIN
         SELECT c2700_cycle_count_id
           INTO v_cycle_count_id
           FROM t2700_cycle_count
          WHERE c5040_plant_id         = p_plant_cnt_id
            AND c2700_cycle_count_year = p_year;
    EXCEPTION
    WHEN OTHERS THEN
        v_cycle_count_id := NULL;
        raise_application_error ( - 20999, 'Cycle Count not yet setup for Year: ' || p_year ||' Plant: '||
        p_plant_cnt_id) ;
    END;
    
    -- 3. to delete the temp table.
    DELETE FROM my_temp_cyc_cnt_scheduler;
    
    -- 4. based on Cycle count id to fetch the warehouse
    
    gm_pkg_ac_cyc_cnt_scheduler.gm_fch_cyc_cnt_location_dtls (v_cycle_count_id, v_out_location_cur) ;
    
    -- loop the cursor
    -- 5. to get all the cycle count warehouse for this plant and inset the details to temp folder.
    LOOP
        FETCH v_out_location_cur INTO v_cyc_cnt_location_id, v_count_pattern, v_lock_type;
        
        -- 5.1 to check the count pattern (ABC logic/Twice logic)
        -- 106818 ABC
        -- 106819 Twice Logic
        
        IF v_count_pattern = 106818 THEN
        	--
            gm_pkg_ac_cyc_cnt_part_schedu.gm_sav_abc_rank_scheduler (v_cycle_count_id, v_cyc_cnt_location_id,
            p_company_id, p_plant_cnt_id, v_user_id) ;
            --
        ELSIF v_count_pattern = 106819 THEN
        	--
            gm_pkg_ac_cyc_cnt_part_schedu.gm_sav_twice_logic_scheduler (v_cycle_count_id, v_cyc_cnt_location_id,
            p_company_id, p_plant_cnt_id, v_user_id) ;
            --
        END IF;
        --
        v_comments := p_year ||' Cycle Count Part Scheduler for ' || get_plant_name (p_plant_cnt_id) || ' Plant, Count Pattern details - '|| get_code_name (v_count_pattern) || ' and Location details - ' || v_cyc_cnt_location_id;
        
        -- 26240486 Cycle count scheduler (comments)
        
        gm_update_log (v_cycle_count_id, v_comments, 26240486, v_user_id, v_message); 
        
        EXIT
    WHEN v_out_location_cur%notfound;
        v_cyc_cnt_location_id := NULL;
        v_count_pattern       := NULL;
        v_lock_type := NULL;
    END LOOP; -- end loop v_out_location_cur
    --
    
END gm_sav_cc_scheduler_main;
--

/**********************************************************************************************
* Description      : Procedure used to validate the twice logic and call the Part/Location procedure
* Author           : mmuthusamy
***********************************************************************************************/
PROCEDURE gm_sav_twice_logic_scheduler (
        p_cycle_count_id     IN t2700_cycle_count.c2700_cycle_count_id%TYPE,
        p_cycle_count_loc_id IN t2701_cycle_count_location.c2701_cyc_cnt_location_id%type,
        p_company_id         IN t1900_company.c1900_company_id%TYPE,
        p_plant_cnt_id       IN t5040_plant_master.c5040_plant_id%type,
        p_user_id            IN t2701_cycle_count_location.c2701_last_updated_by%TYPE)
AS
    v_warehouse_type NUMBER;
    v_count_pattern  NUMBER;
    v_lock_type      NUMBER;
BEGIN
	
    -- Based on Location id to get the location details (warehouse and lock type)
    
     SELECT c901_warehouse_location, NVL (c901_count_pattern, 0), c901_lock_type
       INTO v_warehouse_type, v_count_pattern, v_lock_type
       FROM t2701_cycle_count_location
      WHERE c2701_cyc_cnt_location_id = p_cycle_count_loc_id
        AND c2701_void_fl            IS NULL;
        
    -- 1 to validate the warehouse is twice logic?
    
    IF v_count_pattern <> 106819 THEN
        raise_application_error ( - 20999, 'Warehouse type '|| get_code_name (v_warehouse_type) ||
        '- cannot schedule becuase not a Twice logic.') ;
    END IF;
    
    --2 to check the lock type (Part/Location)
    -- 106822 Part # / 106824 Project
    -- 106823 Location
    
    IF v_lock_type = 106824 THEN
    	--
    	
        gm_pkg_ac_cyc_cnt_part_schedu.gm_sav_part_num_scheduler (p_cycle_count_id, p_cycle_count_loc_id, p_company_id
        , p_plant_cnt_id, p_user_id) ;
        
        --
    ELSIF v_lock_type = 106822 OR v_lock_type = 106823 -- Location
        THEN
        --
        
        gm_pkg_ac_cyc_cnt_part_schedu.gm_sav_location_scheduler (p_cycle_count_id, p_cycle_count_loc_id, p_company_id
        , p_plant_cnt_id, p_user_id) ;
        
        --
    END IF;
    --
END gm_sav_twice_logic_scheduler;

--
/**********************************************************************************************
* Description      : Procedure used to update the ABC rank details to (T2704) table
* Author           : mmuthusamy
***********************************************************************************************/
PROCEDURE gm_sav_abc_rank_scheduler (
        p_cycle_count_id     IN t2700_cycle_count.c2700_cycle_count_id%TYPE,
        p_cycle_count_loc_id IN t2701_cycle_count_location.c2701_cyc_cnt_location_id%type,
        p_company_id         IN t1900_company.c1900_company_id%TYPE,
        p_plant_cnt_id IN t5040_plant_master.c5040_plant_id%type,
        p_user_id IN t2701_cycle_count_location.c2701_last_updated_by%TYPE)
AS
    v_abc_rule_val t906_rules.c906_rule_value%TYPE;
    v_rank_id VARCHAR2 (10) ;
    --
    v_schedule_date       DATE;
    v_closed_business_day NUMBER;
    v_open_businsess_day  NUMBER;
BEGIN
    -- 1. get the rule values (ABC pattern)
     SELECT get_rule_value ('ABC', 'CYC_CNT_GRP')
       INTO v_abc_rule_val
       FROM dual;
       
    -- if rule not setup - throws an error message
    
    IF v_abc_rule_val IS NULL THEN
        raise_application_error ( - 20999,
        'Rules not availabe for ABC logic pattern, Please setup the rules for this warehouse') ;
    END IF;
    
    --
    dbms_output.put_line ('v_abc_rule_val '|| v_abc_rule_val) ;
    
    -- 2. to loop the ABC pattern
    WHILE instr (v_abc_rule_val, ',') <> 0
    LOOP
        v_rank_id      := SUBSTR (v_abc_rule_val, 1, instr (v_abc_rule_val, ',') - 1) ;
        v_abc_rule_val := SUBSTR (v_abc_rule_val, instr (v_abc_rule_val, ',')    + 1) ;
        --
        dbms_output.put_line ('v_rank_id '|| v_rank_id) ;
        
        -- 2.1 to loop the parts and insert into temp table.
        
        gm_pkg_ac_cyc_cnt_part_schedu.gm_sav_abc_part_tmp_load (p_cycle_count_id, p_cycle_count_loc_id, v_rank_id) ;
        --
    END LOOP; -- end v_abc_rule_val
    --
    
    dbms_output.put_line ('before strore the values to main table ') ;
    
    -- 3. to get the company working days for full year
    
    gm_pkg_ac_cyc_cnt_scheduler.gm_fch_company_working_day (p_company_id, 'FULL_YEAR', v_closed_business_day,
    v_open_businsess_day) ;
    
    --
    dbms_output.put_line ('v_closed_business_day ' || v_closed_business_day) ;
    dbms_output.put_line ('v_open_businsess_day ' || v_open_businsess_day) ;
    --
    
    -- 4. to save the details to master table (scheduler)
    
    gm_pkg_ac_cyc_cnt_scheduler.gm_sav_spilt_scheduler (p_cycle_count_id, p_cycle_count_loc_id, v_closed_business_day, v_open_businsess_day,
    p_user_id) ;
    
    --
    dbms_output.put_line ('After strore the values to main table ') ;
    --
END gm_sav_abc_rank_scheduler;
--
/**********************************************************************************************
* Description      : Procedure used to update the Inventory part # details to scheduler (T2704) table
* Author           : mmuthusamy
***********************************************************************************************/
PROCEDURE gm_sav_part_num_scheduler (
        p_cycle_count_id     IN t2700_cycle_count.c2700_cycle_count_id%TYPE,
        p_cycle_count_loc_id IN t2701_cycle_count_location.c2701_cyc_cnt_location_id%type,
        p_company_id         IN t1900_company.c1900_company_id%TYPE,
        p_plant_cnt_id IN t5040_plant_master.c5040_plant_id%type,
        p_user_id IN t2701_cycle_count_location.c2701_last_updated_by%TYPE)
AS
    v_txn_type VARCHAR2 (10) ;
    -- old code
    v_count_pattern_cnt NUMBER;
    --
    v_working_day_cnt     NUMBER;
    v_warehouse_type      NUMBER;
    v_closed_business_day NUMBER;
    v_open_businsess_day  NUMBER;
    --
    v_tmp_location_id t2701_cycle_count_location.c2701_cyc_cnt_location_id%type;
BEGIN
    -- to get the warehouse
    
     SELECT c901_warehouse_location
       INTO v_warehouse_type
       FROM t2701_cycle_count_location
      WHERE c2701_cyc_cnt_location_id = p_cycle_count_loc_id
        AND c2701_void_fl            IS NULL;
        
    -- to get the inventory map id from rules
    
     SELECT get_rule_value (v_warehouse_type, 'CYC_CNT_INV_MAP_ID')
       INTO v_txn_type
       FROM dual;
    --
    dbms_output.put_line ('Part Number scheduler started  '|| v_txn_type || ' Warehouse '|| v_warehouse_type) ;
    
    -- 1. Rule value not available, thrown an error message.
    
    IF v_txn_type IS NULL THEN
        raise_application_error ( - 20999,
        'Rules not availabe for the inventory, Please setup the rules for this warehouse: '|| get_code_name (
        v_warehouse_type)) ;
    END IF;
    
    --2. Store the values to temp table
    --
    gm_pkg_ac_cyc_cnt_part_schedu.gm_sav_inv_part_tmp_load (p_cycle_count_id, p_cycle_count_loc_id, p_plant_cnt_id,
    v_txn_type) ;
    
    -- 3. Get the first half working days
    
    gm_pkg_ac_cyc_cnt_scheduler.gm_fch_company_working_day (p_company_id, 'FIRST_HALF', v_closed_business_day,
    v_open_businsess_day) ;
    
    
    dbms_output.put_line ('after call the Q2  ' || v_closed_business_day || ' and values '|| v_open_businsess_day) ;
    
    
    -- 4. to save the details to master table (scheduler - first half)
    gm_pkg_ac_cyc_cnt_scheduler.gm_sav_spilt_scheduler (p_cycle_count_id, p_cycle_count_loc_id, v_closed_business_day, v_open_businsess_day,
    p_user_id) ;
    
    
    --5. get the second half working days
    
    gm_pkg_ac_cyc_cnt_scheduler.gm_fch_company_working_day (p_company_id, 'SECOND_HALF', v_closed_business_day,
    v_open_businsess_day) ;
    
    dbms_output.put_line ('after call the Q4  ' || v_closed_business_day || ' and values '|| v_open_businsess_day) ;
    
    -- 6. to save the details to master table (scheduler - second half)
    
    gm_pkg_ac_cyc_cnt_scheduler.gm_sav_spilt_scheduler (p_cycle_count_id, p_cycle_count_loc_id, v_closed_business_day, v_open_businsess_day,
    p_user_id) ;
    --
    dbms_output.put_line ('Part Number complete ') ;
    --
END gm_sav_part_num_scheduler;

--
/**********************************************************************************************
* Description      : Procedure used to update the Location details to scheduler (T2704) table
* Author           : mmuthusamy
***********************************************************************************************/
PROCEDURE gm_sav_location_scheduler (
        p_cycle_count_id     IN t2700_cycle_count.c2700_cycle_count_id%TYPE,
        p_cycle_count_loc_id IN t2701_cycle_count_location.c2701_cyc_cnt_location_id%TYPE,
        p_company_id         IN t1900_company.c1900_company_id%TYPE,
        p_plant_cnt_id IN t5040_plant_master.c5040_plant_id%TYPE,
        p_user_id IN t2701_cycle_count_location.c2701_last_updated_by%TYPE)
AS
    v_count_pattern_cnt NUMBER;
    v_location_wh_id    NUMBER;
    v_location_type     NUMBER;
    v_warehouse_type    NUMBER;
    --
    v_working_day_cnt     NUMBER;
    v_schedule_date       DATE;
    v_closed_business_day NUMBER;
    v_open_businsess_day  NUMBER;
    --
BEGIN
	-- get the warehouse type
	
     SELECT c901_warehouse_location
       INTO v_warehouse_type
       FROM t2701_cycle_count_location
      WHERE c2701_cyc_cnt_location_id = p_cycle_count_loc_id
        AND c2701_void_fl            IS NULL;
        
    --1. to set the dynamic warehouse id (FG/RW) and location type (pick/bulk)
    IF (v_warehouse_type  = 106800) THEN
        v_location_wh_id := 1;
        v_location_type  := 93320;
        --106801 FG bulk
    ELSIF (v_warehouse_type = 106801) THEN
        v_location_wh_id   := 1;
        v_location_type    := 93336;
        --106802 RW pick
    ELSIF (v_warehouse_type = 106802) THEN
        v_location_wh_id   := 2;
        v_location_type    := 93320;
        --106803 RW bulk
    ELSIF (v_warehouse_type = 106803) THEN
        v_location_wh_id   := 2;
        v_location_type    := 93336;
    END IF;
    --
    
    dbms_output.put_line ('Started the Location ') ;
    
    -- 1. to store the Location details to temp table
    
    gm_pkg_ac_cyc_cnt_part_schedu.gm_sav_location_tmp_load (p_cycle_count_id, p_cycle_count_loc_id, p_plant_cnt_id,
    v_location_wh_id, v_location_type) ;
    
    --
    dbms_output.put_line ('Before Q1 start ') ;
    
    -- 2. Get the first half working days
    
    gm_pkg_ac_cyc_cnt_scheduler.gm_fch_company_working_day (p_company_id, 'FIRST_HALF', v_closed_business_day,
    v_open_businsess_day) ;
    
    
    -- 3. to save the details to master table (scheduler - first half)
    
    gm_pkg_ac_cyc_cnt_scheduler.gm_sav_spilt_scheduler (p_cycle_count_id, p_cycle_count_loc_id, v_closed_business_day, v_open_businsess_day,
    p_user_id) ;
    
    
    dbms_output.put_line ('Before Q2 start ') ;
    
    -- 4. get the second half working days
    
    gm_pkg_ac_cyc_cnt_scheduler.gm_fch_company_working_day (p_company_id, 'SECOND_HALF', v_closed_business_day,
    v_open_businsess_day) ;
    
    -- 5. to save the details to master table (scheduler - second semester)
    gm_pkg_ac_cyc_cnt_scheduler.gm_sav_spilt_scheduler (p_cycle_count_id, p_cycle_count_loc_id, v_closed_business_day, v_open_businsess_day,
    p_user_id) ;
    
    --
    dbms_output.put_line ('Completed the location ') ;
    --
END gm_sav_location_scheduler;
--
/**********************************************************************************************
* Description      : Procedure used to fetch the ABC rank details and store to temp table
* Author           : mmuthusamy
***********************************************************************************************/
PROCEDURE gm_sav_abc_part_tmp_load (
        p_cycle_count_id      IN T2704_CYCLE_COUNT_SCHEDULE.C2700_CYCLE_COUNT_ID%TYPE,
        p_cyc_cnt_location_id IN T2704_CYCLE_COUNT_SCHEDULE.C2701_CYC_CNT_LOCATION_ID%TYPE,
        p_rank                IN VARCHAR2)
AS

    CURSOR abc_rank_parts_cur
    IS
         SELECT t2703.c205_part_number_id pnum
           FROM t2703_cycle_count_rank t2703
          WHERE t2703.c2700_cycle_count_id = p_cycle_count_id
            AND t2703.c2703_rank           = p_rank
            AND t2703.c2703_void_fl       IS NULL
       ORDER BY t2703.c205_part_number_id;
       
BEGIN
    -- to insert the parts to temp table
    
    FOR abc_rank_parts IN abc_rank_parts_cur
    LOOP
    	--
        gm_pkg_ac_cyc_cnt_scheduler.gm_sav_temp_scheduler (p_cycle_count_id, p_cyc_cnt_location_id,
        abc_rank_parts.pnum) ;
        --
        
    END LOOP; -- end abc_rank_parts
    --
END gm_sav_abc_part_tmp_load;


/**********************************************************************************************
* Description      : Procedure used to fetch the Inventory parts and save to temp table
* Author           : mmuthusamy
***********************************************************************************************/
PROCEDURE gm_sav_inv_part_tmp_load (
        p_cycle_count_id     IN t2700_cycle_count.c2700_cycle_count_id%TYPE,
        p_cycle_count_loc_id IN t2701_cycle_count_location.c2701_cyc_cnt_location_id%type,
        p_plant_cnt_id       IN t5040_plant_master.c5040_plant_id%type,
        p_inv_type           IN NUMBER)
AS
    v_warehouse_type t2701_cycle_count_location.c901_warehouse_location%TYPE;
    v_out_inv_part_cur types.cursor_type;
    v_zero_qty_month NUMBER;
    v_part_number t205c_part_qty.c205_part_number_id%TYPE;
    v_include_sterile_parts t906_rules.c906_rule_value%TYPE;
    v_inculde_non_sterile_parts t906_rules.c906_rule_value%TYPE;
BEGIN
	-- get the warehouse id
	
     SELECT c901_warehouse_location, get_rule_value ('ZERO_QTY_CAL_MONTH', 'CYC_CNT_GRP')
       INTO v_warehouse_type, v_zero_qty_month
       FROM t2701_cycle_count_location
      WHERE c2701_cyc_cnt_location_id = p_cycle_count_loc_id
        AND c2701_void_fl            IS NULL;
        
     --
    -- 4030	Sterile
    
    IF v_warehouse_type = 106805 -- QN
        THEN
        -- 1. to check the include parts
        SELECT get_rule_value (p_plant_cnt_id, 'CYC_CNT_INC_NON_STRE') INTO 
        v_inculde_non_sterile_parts
        FROM DUAL;
        
        -- 2. set the parts to clob list (Used to mention the parts added to cycle count)
        
        my_context.set_my_cloblist (v_inculde_non_sterile_parts) ;
        
        -- 3. to fetch Part # from T205C
        
        OPEN v_out_inv_part_cur FOR
        
         SELECT t205c.c205_part_number_id pnum
		   FROM t205c_part_qty t205c, t205_part_number t205
		  WHERE t205c.c5040_plant_id               = p_plant_cnt_id
		    AND t205.c205_part_number_id           = t205c.c205_part_number_id
		    AND (NVL (t205.c205_product_class, 0) <> 4030
		    OR EXISTS
		    (
		         SELECT token FROM v_clob_list WHERE token = t205.c205_part_number_id
		    ))
		    AND t205c.c901_transaction_type        = p_inv_type
		    AND NVL (t205c.c205_available_qty, 0) <> 0
		    AND NOT EXISTS
		    (
		         SELECT t2703.c205_part_number_id pnum
		           FROM t2703_cycle_count_rank t2703
		          WHERE t2703.c2700_cycle_count_id = p_cycle_count_id
		            AND t2703.c2703_rank           = 'D'
		            AND t2703.c205_part_number_id  = t205c.c205_part_number_id
		            AND t2703.c2703_void_fl       IS NULL
		    )
		  UNION
		-- to get zero cost qty from last two years period
		 SELECT t205c.c205_part_number_id pnum
		   FROM t205c_part_qty t205c, t205_part_number t205
		  WHERE t205c.c5040_plant_id               = p_plant_cnt_id
		    AND t205.c205_part_number_id           = t205c.c205_part_number_id
		    AND (NVL (t205.c205_product_class, 0) <> 4030
		    OR EXISTS
		    (
		         SELECT token FROM v_clob_list WHERE token = t205.c205_part_number_id
		    ))
		    AND t205c.c901_transaction_type       = p_inv_type
		    AND NVL (t205c.c205_available_qty, 0) = 0
		    AND NOT EXISTS
		    (
		         SELECT t2703.c205_part_number_id pnum
		           FROM t2703_cycle_count_rank t2703
		          WHERE t2703.c2700_cycle_count_id = p_cycle_count_id
		            AND t2703.c2703_rank           = 'D'
		            AND t2703.c205_part_number_id  = t205c.c205_part_number_id
		            AND t2703.c2703_void_fl       IS NULL
		    )
		    AND t205c.c205c_transaction_date BETWEEN add_months (CURRENT_DATE, v_zero_qty_month) AND CURRENT_DATE
		ORDER BY pnum;
		
    ELSIF v_warehouse_type = 106806 -- QN (S)
        THEN
        --
        --
        SELECT get_rule_value (p_plant_cnt_id, 'CYC_CNT_INC_STRE') INTO 
        v_include_sterile_parts
        FROM DUAL;
        --
        my_context.set_my_cloblist (v_include_sterile_parts) ;
        --
        OPEN v_out_inv_part_cur FOR 
		  SELECT t205c.c205_part_number_id pnum
		   FROM t205c_part_qty t205c, t205_part_number t205
		  WHERE t205c.c5040_plant_id              = p_plant_cnt_id
		    AND t205c.c901_transaction_type       = p_inv_type
		    AND t205.c205_part_number_id          = t205c.c205_part_number_id
		    AND (NVL (t205.c205_product_class, 0) = 4030
		    OR EXISTS
		    (
		         SELECT token FROM v_clob_list WHERE token = t205.c205_part_number_id
		    ))
		    AND NVL (t205c.c205_available_qty, 0) <> 0
		    AND NOT EXISTS
		    (
		         SELECT t2703.c205_part_number_id pnum
		           FROM t2703_cycle_count_rank t2703
		          WHERE t2703.c2700_cycle_count_id = p_cycle_count_id
		            AND t2703.c2703_rank           = 'D'
		            AND t2703.c205_part_number_id  = t205c.c205_part_number_id
		            AND t2703.c2703_void_fl       IS NULL
		    )
		  UNION
		-- to get zero cost qty from last two years period
		 SELECT t205c.c205_part_number_id pnum
		   FROM t205c_part_qty t205c, t205_part_number t205
		  WHERE t205c.c5040_plant_id              = p_plant_cnt_id
		    AND t205c.c901_transaction_type       = p_inv_type
		    AND NVL (t205c.c205_available_qty, 0) = 0
		    AND t205.c205_part_number_id          = t205c.c205_part_number_id
		    AND (NVL (t205.c205_product_class, 0) = 4030
		    OR EXISTS
		    (
		         SELECT token FROM v_clob_list WHERE token = t205.c205_part_number_id
		    ))
		    AND NOT EXISTS
		    (
		         SELECT t2703.c205_part_number_id pnum
		           FROM t2703_cycle_count_rank t2703
		          WHERE t2703.c2700_cycle_count_id = p_cycle_count_id
		            AND t2703.c2703_rank           = 'D'
		            AND t2703.c205_part_number_id  = t205c.c205_part_number_id
		            AND t2703.c2703_void_fl       IS NULL
		    )
		    AND t205c.c205c_transaction_date BETWEEN add_months (CURRENT_DATE, v_zero_qty_month) AND CURRENT_DATE
		ORDER BY pnum;
		--
    ELSE
    
        OPEN v_out_inv_part_cur FOR
        
        SELECT t205c.c205_part_number_id pnum
	   FROM t205c_part_qty t205c
	  WHERE t205c.c5040_plant_id                = p_plant_cnt_id
	    AND t205c.c901_transaction_type         = p_inv_type
	    AND NVL ( t205c.c205_available_qty, 0) <> 0
	    AND NOT EXISTS
	    (
	         SELECT t2703.c205_part_number_id pnum
	           FROM t2703_cycle_count_rank t2703
	          WHERE t2703.c2700_cycle_count_id = p_cycle_count_id
	            AND t2703.c2703_rank           = 'D'
	            AND t2703.c205_part_number_id  = t205c.c205_part_number_id
	            AND t2703.c2703_void_fl       IS NULL
	    )
	  UNION
	-- to get zero cost qty from last two years period
	 SELECT t205c.c205_part_number_id pnum
	   FROM t205c_part_qty t205c
	  WHERE t205c.c5040_plant_id              = p_plant_cnt_id
	    AND t205c.c901_transaction_type       = p_inv_type
	    AND NVL (t205c.c205_available_qty, 0) = 0
	    AND NOT EXISTS
	    (
	         SELECT t2703.c205_part_number_id pnum
	           FROM t2703_cycle_count_rank t2703
	          WHERE t2703.c2700_cycle_count_id = p_cycle_count_id
	            AND t2703.c2703_rank           = 'D'
	            AND t2703.c205_part_number_id  = t205c.c205_part_number_id
	            AND t2703.c2703_void_fl       IS NULL
	    )
	    AND t205c.c205c_transaction_date BETWEEN add_months (CURRENT_DATE, v_zero_qty_month) AND CURRENT_DATE
	ORDER BY pnum;
    END IF;
    
    --4. Fetch the Cursor and store to temp table
    
    LOOP
        FETCH v_out_inv_part_cur INTO v_part_number;
        --
        IF v_part_number IS NOT NULL
        THEN
        	gm_pkg_ac_cyc_cnt_scheduler.gm_sav_temp_scheduler (p_cycle_count_id, p_cycle_count_loc_id, v_part_number) ;
        END IF;	
        EXIT
    WHEN v_out_inv_part_cur%notfound;
        v_part_number := NULL;
    END LOOP; -- end loop v_out_inv_part_cur
    
    --
END gm_sav_inv_part_tmp_load;
/**********************************************************************************************
* Description      : Procedure used to fetch the Location and save details to  temp table
* Author           : mmuthusamy
***********************************************************************************************/
PROCEDURE gm_sav_location_tmp_load (
        p_cycle_count_id     IN t2700_cycle_count.c2700_cycle_count_id%TYPE,
        p_cycle_count_loc_id IN t2701_cycle_count_location.c2701_cyc_cnt_location_id%type,
        p_plant_cnt_id       IN t5040_plant_master.c5040_plant_id%type,
        p_inv_warehouse_id   IN NUMBER,
        p_location_type      IN NUMBER)
AS
	--
	v_exculde_inv_location t906_rules.c906_rule_value%TYPE;
	--
    CURSOR inv_location_cur
    IS
         SELECT t5052.c5052_location_id location_id
           FROM t5051_inv_warehouse t5051, t5052_location_master t5052
          WHERE t5051.c5051_inv_warehouse_id = t5052.c5051_inv_warehouse_id(+)
            AND t5051.c901_status_id         = 1 -- active
            AND t5052.c5040_plant_id         = p_plant_cnt_id
            AND t5051.c5051_inv_warehouse_id = p_inv_warehouse_id -- fg warehouse,rw warehouse
            AND t5052.c901_location_type     = p_location_type --  bulk,pick face
            AND t5052.c901_status            = 93310 --active
            AND NOT EXISTS (
		         SELECT token FROM v_clob_list WHERE token = t5052.c5052_location_cd
            )
            AND t5052.c5052_void_fl         IS NULL
       ORDER BY t5052.c5052_location_cd;
BEGIN
	-- 1. to get the exculde location code from Rules
	
	SELECT get_rule_value (p_plant_cnt_id, 'CYC_CNT_EXC_LOCATION') INTO 
        v_exculde_inv_location
        FROM DUAL;
        
    --  2. Set the location code to Rules
      
    my_context.set_my_cloblist (v_exculde_inv_location) ;
    
    -- 3. Loop the cursor and insert to temp scheudler procedure.
    
    FOR inv_location IN inv_location_cur
    LOOP
        --
        gm_pkg_ac_cyc_cnt_scheduler.gm_sav_temp_scheduler (p_cycle_count_id, p_cycle_count_loc_id,
        inv_location.location_id) ;
    END LOOP; -- end inv_location
    --
END gm_sav_location_tmp_load;
--
END gm_pkg_ac_cyc_cnt_part_schedu;
/
