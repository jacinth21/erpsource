--@"c:\database\packages\accounting\cyclecount\gm_pkg_ac_cycle_cnt_lot.bdy";
CREATE OR REPLACE PACKAGE body gm_pkg_ac_cycle_cnt_lot
IS

/**********************************************************************************************
* description      : procedure used to get the split cycle count Parts and Sets based on scheduler and call the lock procedure 
* author           : Raja
***********************************************************************************************/
PROCEDURE gm_split_cycle_count
AS
v_lock_id   t2705_cycle_count_lock.c2705_cyc_cnt_lock_id%type;
v_plant_id  t5040_plant_master.c5040_plant_id%TYPE;

   -- Parts
	CURSOR lot_cycle_cnt_part_cur
	IS
	  SELECT t2705.C2705_CYC_CNT_LOCK_ID cyc_part_lockid,
	         t2706.C205_PART_NUMBER_ID pnumid,
	         t2705.C2700_CYCLE_COUNT_ID cyc_cnt_id,
	         t2701.C2701_CYC_CNT_LOCATION_ID cyc_cnt_loc_id,
	         t2705.C2705_SCHEDULE_DATE schedule_date,
	         t2705.C2705_APPROVED_BY approved_by 
	    FROM t2706_cycle_count_lock_dtls t2706,
	         t205_part_number t205,
	         t2705_cycle_count_lock t2705,
	         V205_STERILE_PARTS v205,
	         t2700_cycle_count t2700,
	         t2701_cycle_count_location t2701,
	         t202_project t202
	   WHERE t205.c205_part_number_id = t2706.c205_part_number_id 
	     AND t2705.c2705_cyc_cnt_lock_id =t2706.c2705_cyc_cnt_lock_id
	     AND t2706.c205_part_number_id = v205.c205_part_number_id
	     AND t202.c202_project_id = t205.c202_project_id
	     AND ((t202.c202_project_id in (SELECT C906_RULE_VALUE FROM T906_RULES WHERE C906_RULE_ID = v_plant_id AND C906_RULE_GRP_ID='CC_SPLIT_PROJECT' AND C906_VOID_FL IS NULL)
	     AND t2701.C901_WAREHOUSE_LOCATION IN (SELECT C906_RULE_VALUE FROM T906_RULES WHERE C906_RULE_ID = v_plant_id AND C906_RULE_GRP_ID='CC_SPT_PJ_WAREHOUSE' AND C906_VOID_FL IS NULL))
	     OR t2701.C901_WAREHOUSE_LOCATION IN (SELECT C906_RULE_VALUE FROM T906_RULES WHERE C906_RULE_ID = v_plant_id AND C906_RULE_GRP_ID='CC_SPLIT_WAREHOUSE' AND C906_VOID_FL IS NULL))       
	     AND t2700.c2700_cycle_count_id = t2705.c2700_cycle_count_id
	     AND t2705.c2701_cyc_cnt_location_id = t2701.c2701_cyc_cnt_location_id
	     AND t2706.c2706_void_fl  IS NULL
	     AND t2705.c2705_void_fl IS NULL
	     AND t2705.c2705_parent_cyc_cnt_lock_id IS NULL     
	     AND t2705.c901_lock_status = 106827      -- 106827  - Open
	     AND t2700.C5040_PLANT_ID = v_plant_id
	     AND t2705.C2705_split_fl IS NULL
	   --  AND t2701.C901_WAREHOUSE_LOCATION IN (SELECT C906_RULE_VALUE FROM T906_RULES WHERE C906_RULE_ID = v_plant_id AND C906_RULE_GRP_ID='CC_SPLIT_WAREHOUSE' AND C906_VOID_FL IS NULL)
	     AND TRUNC (t2705.c2705_schedule_date) = TRUNC(current_date) GROUP BY t2705.C2705_CYC_CNT_LOCK_ID ,
	         t2706.C205_PART_NUMBER_ID ,
	         t2705.C2700_CYCLE_COUNT_ID ,
	         t2701.C2701_CYC_CNT_LOCATION_ID ,
	         t2705.C2705_SCHEDULE_DATE ,
	         t2705.C2705_APPROVED_BY;    
      
	--- Sets

	CURSOR lot_cycle_cnt_set_cur
	IS
	
	  SELECT t2705.C2705_CYC_CNT_LOCK_ID cyc_set_lockid,
	         t2706.C207_SET_ID setid,
	         t2705.C2700_CYCLE_COUNT_ID cyc_set_cnt_id,
	         t2701.C2701_CYC_CNT_LOCATION_ID set_loc_id,
	         t2705.C2705_SCHEDULE_DATE set_schedule_date,
	         t2705.C2705_APPROVED_BY set_approved_by
	    FROM t2706_cycle_count_lock_dtls t2706, 
	         t207_set_master t207, 
	         t2705_cycle_count_lock t2705,
	         t2700_cycle_count t2700,
	         t2701_cycle_count_location t2701
	   WHERE t207.C207_SET_ID = t2706.C207_SET_ID 
	     AND t2705.c2705_cyc_cnt_lock_id =t2706.c2705_cyc_cnt_lock_id
	     AND t2700.c2700_cycle_count_id = t2705.c2700_cycle_count_id
	     AND t2705.c2701_cyc_cnt_location_id = t2701.c2701_cyc_cnt_location_id
	     AND t2701.C901_WAREHOUSE_LOCATION IN (SELECT C906_RULE_VALUE FROM T906_RULES WHERE C906_RULE_ID = v_plant_id AND C906_RULE_GRP_ID='CC_SPLIT_WAREHOUSE' AND C906_VOID_FL IS NULL)
         AND t207.c207_lot_track_fl = 'Y' 
         AND t2706.c2706_void_fl  IS NULL
         AND t2705.c2705_void_fl IS NULL
         AND t2705.c2705_parent_cyc_cnt_lock_id IS NULL
	     AND t2705.c901_lock_status = 106827                              -- 106827  - Open
	     AND t2700.C5040_PLANT_ID = v_plant_id
	     AND t2705.C2705_split_fl IS NULL
	     AND TRUNC (t2705.c2705_schedule_date) = TRUNC(current_date);   
 
	 BEGIN
		 
		  SELECT get_plantid_frm_cntx() 
           INTO v_plant_id 
           FROM DUAL;

	FOR cycle_cnt_part IN lot_cycle_cnt_part_cur
      LOOP
     
         --insert t2705 
      	  gm_sav_cycle_count_lock(cycle_cnt_part.cyc_part_lockid,
      	                        cycle_cnt_part.cyc_cnt_id,
      	                        cycle_cnt_part.cyc_cnt_loc_id,
      	                        cycle_cnt_part.schedule_date,
      	                        cycle_cnt_part.approved_by,
      	                        v_lock_id); 
      
      	 --  update t2706  
      	 gm_sav_cycle_cnt_lock_dtls(v_lock_id,
      	                            cycle_cnt_part.cyc_part_lockid,
      	                            cycle_cnt_part.pnumid,
      	                            NULL,
      	                            cycle_cnt_part.approved_by
      	                            );
      	 
      END LOOP;
      
    --  cycle count lock details
    /**  FOR cycle_cnt_set IN lot_cycle_cnt_set_cur
      LOOP
      	 --insert t2705 
     	 gm_sav_cycle_count_lock(cycle_cnt_set.cyc_set_lockid,
      	                        cycle_cnt_set.cyc_set_cnt_id,
      	                        cycle_cnt_set.set_loc_id,
      	                        cycle_cnt_set.set_schedule_date,
      	                        cycle_cnt_set.set_approved_by,
      	                        v_lock_id
      	                         );  
      	 --  update t2706 
     	 gm_sav_cycle_cnt_lock_dtls(v_lock_id,
      	                         cycle_cnt_set.cyc_set_lockid,
      	                         NULL,
      	                         cycle_cnt_set.setid,
      	                         cycle_cnt_set.set_approved_by
      	                         );
     END LOOP; */
      
     gm_pkg_ac_cycle_cnt_lot.gm_void_blank_cyclecount;
      
END gm_split_cycle_count;


/**********************************************************************************************
* description      : procedure used to get the split cycle count Parts and Sets based on scheduler and call the lock procedure 
* author           : Raja
***********************************************************************************************/
PROCEDURE gm_split_cycle_count(
p_schedule_dt  IN  t2705_cycle_count_lock.c2705_schedule_date%type
)
AS
v_lock_id   t2705_cycle_count_lock.c2705_cyc_cnt_lock_id%type;
v_plant_id  t5040_plant_master.c5040_plant_id%TYPE;

   -- Parts
	CURSOR lot_cycle_cnt_part_cur
	IS
	   SELECT t2705.C2705_CYC_CNT_LOCK_ID cyc_part_lockid,
	         t2706.C205_PART_NUMBER_ID pnumid,
	         t2705.C2700_CYCLE_COUNT_ID cyc_cnt_id,
	         t2701.C2701_CYC_CNT_LOCATION_ID cyc_cnt_loc_id,
	         t2705.C2705_SCHEDULE_DATE schedule_date,
	         t2705.C2705_APPROVED_BY approved_by 
	    FROM t2706_cycle_count_lock_dtls t2706,
	         t205_part_number t205,
	         t2705_cycle_count_lock t2705,
	         V205_STERILE_PARTS v205,
	         t2700_cycle_count t2700,
	         t2701_cycle_count_location t2701,
	         t202_project t202
	   WHERE t205.c205_part_number_id = t2706.c205_part_number_id 
	     AND t2705.c2705_cyc_cnt_lock_id =t2706.c2705_cyc_cnt_lock_id
	     AND t2706.c205_part_number_id = v205.c205_part_number_id
	     AND t202.c202_project_id = t205.c202_project_id
	     AND ((t202.c202_project_id in (SELECT C906_RULE_VALUE FROM T906_RULES WHERE C906_RULE_ID = v_plant_id AND C906_RULE_GRP_ID='CC_SPLIT_PROJECT' AND C906_VOID_FL IS NULL)
	     AND t2701.C901_WAREHOUSE_LOCATION IN (SELECT C906_RULE_VALUE FROM T906_RULES WHERE C906_RULE_ID = v_plant_id AND C906_RULE_GRP_ID='CC_SPT_PJ_WAREHOUSE' AND C906_VOID_FL IS NULL))
	     OR t2701.C901_WAREHOUSE_LOCATION IN (SELECT C906_RULE_VALUE FROM T906_RULES WHERE C906_RULE_ID = v_plant_id AND C906_RULE_GRP_ID='CC_SPLIT_WAREHOUSE' AND C906_VOID_FL IS NULL))       
	     AND t2700.c2700_cycle_count_id = t2705.c2700_cycle_count_id
	     AND t2705.c2701_cyc_cnt_location_id = t2701.c2701_cyc_cnt_location_id
	     AND t2706.c2706_void_fl  IS NULL
	     AND t2705.c2705_void_fl IS NULL
	     AND t2705.c2705_parent_cyc_cnt_lock_id IS NULL     
	     AND t2705.c901_lock_status = 106827      -- 106827  - Open
	     AND t2700.C5040_PLANT_ID = v_plant_id
	     AND t2705.C2705_split_fl IS NULL
	   --  AND t2701.C901_WAREHOUSE_LOCATION IN (SELECT C906_RULE_VALUE FROM T906_RULES WHERE C906_RULE_ID = v_plant_id AND C906_RULE_GRP_ID='CC_SPLIT_WAREHOUSE' AND C906_VOID_FL IS NULL)
	     AND TRUNC (t2705.c2705_schedule_date) = TRUNC(p_schedule_dt) GROUP BY t2705.C2705_CYC_CNT_LOCK_ID ,
	         t2706.C205_PART_NUMBER_ID ,
	         t2705.C2700_CYCLE_COUNT_ID ,
	         t2701.C2701_CYC_CNT_LOCATION_ID ,
	         t2705.C2705_SCHEDULE_DATE ,
	         t2705.C2705_APPROVED_BY;    
      
	--- Sets

	CURSOR lot_cycle_cnt_set_cur
	IS
	
	  SELECT t2705.C2705_CYC_CNT_LOCK_ID cyc_set_lockid,
	         t2706.C207_SET_ID setid,
	         t2705.C2700_CYCLE_COUNT_ID cyc_set_cnt_id,
	         t2701.C2701_CYC_CNT_LOCATION_ID set_loc_id,
	         t2705.C2705_SCHEDULE_DATE set_schedule_date,
	         t2705.C2705_APPROVED_BY set_approved_by
	    FROM t2706_cycle_count_lock_dtls t2706, 
	         t207_set_master t207, 
	         t2705_cycle_count_lock t2705,
	         t2700_cycle_count t2700,
	         t2701_cycle_count_location t2701
	   WHERE t207.C207_SET_ID = t2706.C207_SET_ID 
	     AND t2705.c2705_cyc_cnt_lock_id =t2706.c2705_cyc_cnt_lock_id
	     AND t2700.c2700_cycle_count_id = t2705.c2700_cycle_count_id
	     AND t2705.c2701_cyc_cnt_location_id = t2701.c2701_cyc_cnt_location_id
	     AND t2701.C901_WAREHOUSE_LOCATION IN (SELECT C906_RULE_VALUE FROM T906_RULES WHERE C906_RULE_ID = v_plant_id AND C906_RULE_GRP_ID='CC_SPLIT_WAREHOUSE' AND C906_VOID_FL IS NULL)
         AND t207.c207_lot_track_fl = 'Y' 
         AND t2706.c2706_void_fl  IS NULL
         AND t2705.c2705_void_fl IS NULL
         AND t2705.c2705_parent_cyc_cnt_lock_id IS NULL
	     AND t2705.c901_lock_status = 106827                              -- 106827  - Open
	     AND t2700.C5040_PLANT_ID = v_plant_id
	     AND t2705.C2705_split_fl IS NULL
	     AND TRUNC (t2705.c2705_schedule_date) = TRUNC(p_schedule_dt);   
 
	 BEGIN
		 
		  SELECT get_plantid_frm_cntx() 
           INTO v_plant_id 
           FROM DUAL;

	FOR cycle_cnt_part IN lot_cycle_cnt_part_cur
      LOOP
     
         --insert t2705 
      	  gm_sav_cycle_count_lock(cycle_cnt_part.cyc_part_lockid,
      	                        cycle_cnt_part.cyc_cnt_id,
      	                        cycle_cnt_part.cyc_cnt_loc_id,
      	                        cycle_cnt_part.schedule_date,
      	                        cycle_cnt_part.approved_by,
      	                        v_lock_id); 
      
      	 --  update t2706  
      	 gm_sav_cycle_cnt_lock_dtls(v_lock_id,
      	                            cycle_cnt_part.cyc_part_lockid,
      	                            cycle_cnt_part.pnumid,
      	                            NULL,
      	                            cycle_cnt_part.approved_by
      	                            );
      	 
      END LOOP;
      
    --  cycle count lock details
   /**   FOR cycle_cnt_set IN lot_cycle_cnt_set_cur
      LOOP
      	 --insert t2705 
     	 gm_sav_cycle_count_lock(cycle_cnt_set.cyc_set_lockid,
      	                        cycle_cnt_set.cyc_set_cnt_id,
      	                        cycle_cnt_set.set_loc_id,
      	                        cycle_cnt_set.set_schedule_date,
      	                        cycle_cnt_set.set_approved_by,
      	                        v_lock_id
      	                         );  
      	 --  update t2706 
     	 gm_sav_cycle_cnt_lock_dtls(v_lock_id,
      	                         cycle_cnt_set.cyc_set_lockid,
      	                         NULL,
      	                         cycle_cnt_set.setid,
      	                         cycle_cnt_set.set_approved_by
      	                         );
     END LOOP;*/
      
END gm_split_cycle_count;

/**********************************************************************************************
* description      : procedure used to create the cycle count lock id
* author           : Raja
***********************************************************************************************/
PROCEDURE gm_sav_cycle_count_lock (
        p_cyc_cnt_lockid         IN  t2705_cycle_count_lock.C2705_CYC_CNT_LOCK_ID%type,
        p_cyc_count_id           IN  t2700_cycle_count.c2700_cycle_count_id%type,
        p_cyc_count_loc_id       IN  t2701_cycle_count_location.c2701_cyc_cnt_location_id%type,
        p_schedule_date          IN  t2704_cycle_count_schedule.c2704_schedule_date%type,
        p_user_id                IN  t2705_cycle_count_lock.c2705_last_updated_by%type,
        p_lock_id                OUT t2705_cycle_count_lock.c2705_cyc_cnt_lock_id%type)
AS
v_company_id t1900_company.c1900_company_id%TYPE;
v_lock_prefix VARCHAR2(10);
v_cnt number;
v_lock_id t2705_cycle_count_lock.C2705_CYC_CNT_LOCK_ID%type;
BEGIN
    -- to get the company id 
        SELECT get_compid_frm_cntx () 
          INTO v_company_id 
          FROM DUAL;
  
    BEGIN
	    SELECT C2705_CYC_CNT_LOCK_ID
	      INTO v_lock_id  
	      FROM t2705_cycle_count_lock 
	     WHERE c2705_parent_cyc_cnt_lock_id = p_cyc_cnt_lockid 
	       AND c2705_void_fl IS NULL;
	       
	       p_lock_id := v_lock_id;
	       
	    EXCEPTION
	         WHEN NO_DATA_FOUND
			 THEN
			 v_lock_id := NULL;
	 END;

       IF v_lock_id IS NULL THEN  
	    -- get the cycle count transactions pre fix (based on company level)
	    SELECT GET_RULE_VALUE_BY_COMPANY ('CYC_CNT_ID_PREFIX','CYC_CNT_LT_LOCK', v_company_id) 
	      INTO v_lock_prefix  
	      FROM DUAL;
	    --
	    SELECT v_lock_prefix || s2705_cycle_count_lock.nextval 
	      INTO p_lock_id 
	      FROM dual;
	    --
	     INSERT
	       INTO t2705_cycle_count_lock
	        (
	            c2705_cyc_cnt_lock_id, c2705_parent_cyc_cnt_lock_id
	          , c2700_cycle_count_id, c2701_cyc_cnt_location_id
	          , c2705_lock_date, c901_lock_status, c2705_schedule_date
	          , c2705_last_updated_by, c2705_last_updated_date, c2705_lot_track_fl,C2705_SPLIT_FL
	        )
	    VALUES
	        (
	            p_lock_id, p_cyc_cnt_lockid
	          , p_cyc_count_id, p_cyc_count_loc_id
	          , CURRENT_DATE, 106827, p_schedule_date
	          , p_user_id, CURRENT_DATE, 'Y','Y'
	        ) ;
		   
  END if;
        
END gm_sav_cycle_count_lock;

/**********************************************************************************************
* description      : procedure used to update t2706_cycle_count_lock_dtls
* author           : Raja
***********************************************************************************************/
PROCEDURE gm_sav_cycle_cnt_lock_dtls
      ( 
        p_cyc_cnt_lock_id IN t2706_cycle_count_lock_dtls.C2705_CYC_CNT_LOCK_ID%type,
        p_parent_cyc_id   IN t2705_cycle_count_lock.C2705_PARENT_CYC_CNT_LOCK_ID%type,
        p_part_number     IN t2706_cycle_count_lock_dtls.C205_PART_NUMBER_ID%type,
        p_set_id          IN t2706_cycle_count_lock_dtls.C207_SET_ID%type,
        p_user_id         IN t2705_cycle_count_lock.c2705_last_updated_by%type
    )
AS
BEGIN
	
     IF p_set_id IS NOT NULL 
       THEN 
      
        UPDATE t2706_cycle_count_lock_dtls
           SET C2705_CYC_CNT_LOCK_ID = p_cyc_cnt_lock_id, 
               C2706_LAST_UPDATED_BY = p_user_id,
               C2706_LAST_UPDATED_DATE = current_date,
               C2706_LAST_UPDATED_DATE_UTC = current_date
         WHERE C207_SET_ID = p_set_id
           AND C2705_CYC_CNT_LOCK_ID = p_parent_cyc_id;   
           
    ELSE 
    
       UPDATE t2706_cycle_count_lock_dtls
           SET C2705_CYC_CNT_LOCK_ID = p_cyc_cnt_lock_id, 
               C2706_LAST_UPDATED_BY = p_user_id,
               C2706_LAST_UPDATED_DATE = current_date,
               C2706_LAST_UPDATED_DATE_UTC = current_date
         WHERE C205_PART_NUMBER_ID = p_part_number
           AND C2705_CYC_CNT_LOCK_ID = p_parent_cyc_id;
           
    END IF;

END gm_sav_cycle_cnt_lock_dtls;
/**********************************************************************************************
* description      : procedure used to check the cycle row count value 
* author           : spalanivel
***********************************************************************************************/
PROCEDURE gm_void_blank_cyclecount
AS
v_plant_id        t5040_plant_master.c5040_plant_id%TYPE;
v_cnt		      NUMBER;
v_user_id         NUMBER;
v_cyc_cnt_lock_id VARCHAR(20);

	CURSOR void_blank_cyclecount
	IS
	  SELECT t2705.c2705_cyc_cnt_lock_id  cyc_cnt_lock_id
	   -- INTO v_cyc_cnt_lock_id
	    FROM t2705_cycle_count_lock t2705,
	         t2700_cycle_count t2700
	   WHERE t2705.c2705_void_fl IS NULL
         AND t2705.c2705_parent_cyc_cnt_lock_id IS NULL
	     AND t2705.c901_lock_status = 106827                              -- 106827  - Open
	     AND t2700.C5040_PLANT_ID = v_plant_id
	     AND t2705.C2705_split_fl IS NULL
	     AND TRUNC (t2705.c2705_schedule_date) = TRUNC(current_date);
	     
	 BEGIN
	 SELECT get_plantid_frm_cntx() 
           INTO v_plant_id 
           FROM DUAL;
	v_user_id := 30301;
		 
	FOR blank_cyclecount IN void_blank_cyclecount
      LOOP
      
        SELECT count(*) 
          INTO v_cnt
          FROM t2706_cycle_count_lock_dtls t2706
         WHERE t2706.C2705_CYC_CNT_LOCK_ID = blank_cyclecount.cyc_cnt_lock_id
           AND t2706.C2706_VOID_FL IS NULL;
         
         IF (v_cnt = 0 )
         THEN 
          
             -- update the cycle count id
             UPDATE T2705_CYCLE_COUNT_LOCK
                SET C2705_VOID_FL = 'Y',
                    C2705_LAST_UPDATED_BY = v_user_id, 
                    C2705_LAST_UPDATED_DATE = SYSDATE
              WHERE C2705_CYC_CNT_LOCK_ID = blank_cyclecount.cyc_cnt_lock_id
                    AND C2705_VOID_FL IS NULL;
     
             -- to void the cycle count details
             UPDATE T2706_CYCLE_COUNT_LOCK_DTLS
                SET c2706_void_fl        = 'Y'
                    , C2706_LAST_UPDATED_BY = v_user_id
                    , C2706_LAST_UPDATED_DATE   = SYSDATE
              WHERE C2705_CYC_CNT_LOCK_ID = blank_cyclecount.cyc_cnt_lock_id
                    AND c2706_void_fl IS NULL;
        
            END IF;
      END LOOP;
          
END gm_void_blank_cyclecount;

END gm_pkg_ac_cycle_cnt_lot;
/
