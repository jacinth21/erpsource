--@"c:\database\packages\accounting\cyclecount\gm_pkg_ac_cycle_count_trans.bdy";
CREATE OR REPLACE PACKAGE body gm_pkg_ac_cycle_count_trans
IS
    /**********************************************************************************************
    * description      : procedure used to save the recorded information
    * author           : mmuthusamy
    ***********************************************************************************************/
PROCEDURE gm_sav_record_infor (
        p_cyc_lock_id    IN t2706_cycle_count_lock_dtls.c2705_cyc_cnt_lock_id%type,
	    p_input_str      IN CLOB,
	    p_record_comp_fl IN VARCHAR2,
	    p_user_id        IN t2706_cycle_count_lock_dtls.c2706_last_updated_by%type)
AS
    v_strlen NUMBER := NVL (LENGTH (p_input_str), 0) ;
    v_string CLOB   := p_input_str;
    v_substring VARCHAR2 (4000) ;
    --
    v_lock_dtls_id NUMBER;
    v_phsical_qty  NUMBER;
    --
    v_status_upd_cnt NUMBER;
    v_total_cnt   NUMBER;
    v_record_cnt  NUMBER;
    --
    v_cyc_lock_id    t2706_cycle_count_lock_dtls.c2705_cyc_cnt_lock_id%type;
    v_cyc_status_id t2705_cycle_count_lock.c901_lock_status%type;
    v_lot_track_fl t2705_cycle_count_lock.c2705_lot_track_fl%type;
BEGIN
	-- to get the lock
	 SELECT c2705_cyc_cnt_lock_id, c901_lock_status,c2705_lot_track_fl
   INTO v_cyc_lock_id, v_cyc_status_id, v_lot_track_fl
   FROM t2705_cycle_count_lock
  WHERE c2705_cyc_cnt_lock_id = p_cyc_lock_id FOR UPDATE;
  
  -- if already counted then validate
  -- 106829 counted
  IF v_cyc_status_id = 106829
  THEN
  	raise_application_error (-20999, 'Cycle Count Id '|| p_cyc_lock_id ||' already Counted ');
  END IF;
  
    -- to check the length
    IF v_strlen                      > 0 THEN
        WHILE instr (v_string, '|') <> 0
        LOOP
            v_substring    := SUBSTR (v_string, 1, instr (v_string, '|') - 1) ;
            v_string       := SUBSTR (v_string, instr (v_string, '|')    + 1) ;
            v_lock_dtls_id := NULL;
            v_phsical_qty  := NULL;
            v_lock_dtls_id := SUBSTR (v_substring, 1, instr (v_substring, '^') - 1) ;
            v_substring    := SUBSTR (v_substring, instr (v_substring, '^')    + 1) ;
            v_phsical_qty  := v_substring ;
            
            -- to get the details lock
               SELECT c2706_cyc_cnt_lock_dtls_id
			   INTO v_lock_dtls_id
			   FROM t2706_cycle_count_lock_dtls
			  WHERE c2706_cyc_cnt_lock_dtls_id = v_lock_dtls_id FOR UPDATE;
			  
            -- 1. to update the physical Qty
            gm_pkg_ac_cycle_count_trans.gm_update_physical_qty (v_lock_dtls_id, v_phsical_qty, p_user_id) ;
        END LOOP;
    END IF; -- length check
    
        --It is time consume to set counted qty zero (CC-LT-XXXX ) - PC-4480 -Counted qty changes for Sterile cycle count
         IF ( v_lot_track_fl ='Y') THEN
           UPDATE t2706_cycle_count_lock_dtls 
              SET  C2706_PHYSICAL_QTY = '0',
                   C101_COUNTED_BY = p_user_id,
                   C2706_COUNTED_DATE = CURRENT_DATE
            WHERE  C2706_PHYSICAL_QTY IS NULL 
              AND  C2705_CYC_CNT_LOCK_ID = p_cyc_lock_id ;
         END IF;
    
    
    -- 2. check the lock - current status
         SELECT COUNT (1)
           INTO v_status_upd_cnt
           FROM t2706_cycle_count_lock_dtls t2706, t2705_cycle_count_lock t2705
          WHERE t2705.c2705_cyc_cnt_lock_id = t2706.c2705_cyc_cnt_lock_id
            AND t2705.c2705_cyc_cnt_lock_id = p_cyc_lock_id
            AND t2706.c2706_physical_qty   IS NOT NULL
            AND t2705.c901_lock_status      = 106827 -- open
            AND t2705.c2705_void_fl        IS NULL
            AND t2706.c2706_void_fl        IS NULL;
            
        -- 3. based on count to update the status to in progress    
        IF (v_status_upd_cnt              <> 0) THEN
            -- 106828 in progress
            gm_pkg_ac_cycle_count_trans.gm_upd_cycle_count_status (p_cyc_lock_id, 106828, p_user_id) ;
        END IF;
        
        -- 4. get the total count (lock)
         SELECT COUNT (1)
           INTO v_total_cnt
           FROM t2706_cycle_count_lock_dtls
          WHERE c2705_cyc_cnt_lock_id = p_cyc_lock_id
            AND c2706_void_fl        IS NULL;
            
        -- 5. get the record count
         SELECT COUNT (1)
           INTO v_record_cnt
           FROM t2706_cycle_count_lock_dtls t2706
          WHERE t2706.c2705_cyc_cnt_lock_id = p_cyc_lock_id
            AND t2706.c2706_physical_qty   IS NOT NULL
            AND t2706.c2706_void_fl        IS NULL;
            
        -- 6. checking total count and record count - update the status (counted)
        IF (v_total_cnt                     = v_record_cnt AND p_record_comp_fl = 'Y') THEN
            -- 106829 counted
            gm_pkg_ac_cycle_count_trans.gm_upd_cycle_count_status (p_cyc_lock_id, 106829, p_user_id) ;
            -- to update the recount flag
            update t2706_cycle_count_lock_dtls set c2706_recount_fl = NULL
            WHERE c2705_cyc_cnt_lock_id = p_cyc_lock_id
            AND c2706_recount_fl IS NOT NULL
            AND c2706_void_fl        IS NULL;
        END IF; -- end if total count check
        
END gm_sav_record_infor;
/**********************************************************************************************
* description      : procedure used to update the phsycial qty details
* author           : mmuthusamy
***********************************************************************************************/
PROCEDURE gm_update_physical_qty (
        p_cyc_lock_dtl_id IN t2706_cycle_count_lock_dtls.c2706_cyc_cnt_lock_dtls_id%type,
        p_qty             IN t2706_cycle_count_lock_dtls.c2706_physical_qty%type,
        p_user_id         IN t2706_cycle_count_lock_dtls.c2706_last_updated_by%type)
AS
BEGIN
     UPDATE t2706_cycle_count_lock_dtls
    SET c2706_physical_qty             = p_qty, c101_counted_by = p_user_id, c2706_counted_date = CURRENT_DATE
      , c2706_last_updated_by          = p_user_id, c2706_last_updated_date = CURRENT_DATE
      WHERE c2706_cyc_cnt_lock_dtls_id = p_cyc_lock_dtl_id
        AND c2706_void_fl             IS NULL;
END gm_update_physical_qty;
/**********************************************************************************************
* description      : procedure used to update cycle count status
* author           : mmuthusamy
***********************************************************************************************/
PROCEDURE gm_upd_cycle_count_status (
        p_lock_id IN t2705_cycle_count_lock.c2705_cyc_cnt_lock_id%type,
        p_status  IN t2705_cycle_count_lock.c901_lock_status%type,
        p_user_id IN t2705_cycle_count_lock.c2705_last_updated_by%type)
AS
BEGIN
     UPDATE t2705_cycle_count_lock
    SET c901_lock_status          = p_status, c2705_last_updated_by = p_user_id, c2705_last_updated_date = CURRENT_DATE
    , c2705_approved_by = DECODE(p_status, 106831, p_user_id, c2705_approved_by), c2705_approved_date = DECODE(p_status, 106831, CURRENT_DATE, c2705_approved_date)
      WHERE c2705_cyc_cnt_lock_id = p_lock_id;
END gm_upd_cycle_count_status;

/**********************************************************************************************
    * description      : procedure used to update recount details
    * author           : mmuthusamy
    ***********************************************************************************************/        
    PROCEDURE gm_sav_recount_dtls (
            p_cyc_lock_id    IN t2706_cycle_count_lock_dtls.c2705_cyc_cnt_lock_id%type,
	        p_input_str      IN CLOB,
	        p_user_id        IN t2706_cycle_count_lock_dtls.c2706_last_updated_by%type
    )
    AS
     v_strlen NUMBER := NVL (LENGTH (p_input_str), 0) ;
    v_string CLOB   := p_input_str;
    v_substring VARCHAR2 (4000) ;
    --
    v_lock_dtls_id NUMBER;
    BEGIN
	     -- to check the length
    IF v_strlen                      > 0 THEN
        WHILE instr (v_string, '|') <> 0
        LOOP
            v_substring    := SUBSTR (v_string, 1, instr (v_string, '|') - 1) ;
            v_string       := SUBSTR (v_string, instr (v_string, '|')    + 1) ;
            v_lock_dtls_id := NULL;
            v_lock_dtls_id := v_substring;
            -- 1. to update the recount flag
            UPDATE t2706_cycle_count_lock_dtls
    SET c2706_recount_fl   = 'Y'
      , c2706_last_updated_by          = p_user_id, c2706_last_updated_date = CURRENT_DATE
      WHERE c2706_cyc_cnt_lock_dtls_id = v_lock_dtls_id
        AND c2706_void_fl             IS NULL;
        
            UPDATE t2708_cycle_count_scanned_lots                                 ---  Initiate recount Changes
               SET C2708_VOID_FL='Y' 
             WHERE C2706_CYC_CNT_LOCK_DTLS_ID = v_lock_dtls_id;       
            
        END LOOP;
        --
        -- 106830 Recount
            gm_pkg_ac_cycle_count_trans.gm_upd_cycle_count_status (p_cyc_lock_id, 106830, p_user_id) ;
        
    END IF; -- length check
	    
	    END gm_sav_recount_dtls;
	    
	     /**********************************************************************************************
    * description      : procedure used to update recount details
    * author           : mmuthusamy
    ***********************************************************************************************/        
    PROCEDURE gm_sav_override_qty_dtls (
            p_cyc_lock_id    IN t2706_cycle_count_lock_dtls.c2705_cyc_cnt_lock_id%type,
	        p_input_str      IN CLOB,
	        p_user_id        IN t2706_cycle_count_lock_dtls.c2706_last_updated_by%type
    )
    AS
      v_strlen NUMBER := NVL (LENGTH (p_input_str), 0) ;
    v_string CLOB   := p_input_str;
    v_substring VARCHAR2 (4000) ;
    --
    v_lock_dtls_id NUMBER;
    v_override_qty NUMBER;
    v_comments VARCHAR2(4000);
    BEGIN
	        -- to check the length
    IF v_strlen                      > 0 THEN
        WHILE instr (v_string, '|') <> 0
        LOOP
             v_substring    := SUBSTR (v_string, 1, instr (v_string, '|') - 1) ;
            v_string       := SUBSTR (v_string, instr (v_string, '|')    + 1) ;
            v_lock_dtls_id := NULL;
            v_override_qty  := NULL;
            v_comments := NULL;
            v_lock_dtls_id := SUBSTR (v_substring, 1, instr (v_substring, '^') - 1) ;
            v_substring    := SUBSTR (v_substring, instr (v_substring, '^')    + 1) ;
            v_override_qty  := SUBSTR (v_substring, 1, instr (v_substring, '^') - 1) ;
            v_substring    := SUBSTR (v_substring, instr (v_substring, '^')    + 1) ;
            v_comments := v_substring ;
            -- 1. to update the recount flag
            UPDATE t2706_cycle_count_lock_dtls
    SET c2706_override_qty   = v_override_qty, c2706_override_qty_fl = 'Y'
    , c2706_comments   = v_comments
      , c2706_last_updated_by          = p_user_id, c2706_last_updated_date = CURRENT_DATE
      WHERE c2706_cyc_cnt_lock_dtls_id = v_lock_dtls_id
        AND c2706_void_fl             IS NULL;
            
        END LOOP;
        
    END IF; -- length check
	    
	    END gm_sav_override_qty_dtls;
	    
	/**********************************************************************************************
    * description      : procedure used to update approval Qty
    * author           : mmuthusamy
    ***********************************************************************************************/
    PROCEDURE gm_sav_approval_qty_dtls (
            p_cyc_lock_id    IN t2706_cycle_count_lock_dtls.c2705_cyc_cnt_lock_id%type,
	        p_input_str      IN CLOB,
	        p_user_id        IN t2706_cycle_count_lock_dtls.c2706_last_updated_by%type
    )
    
    AS
    v_strlen NUMBER := NVL (LENGTH (p_input_str), 0) ;
    v_string CLOB   := p_input_str;
    v_substring VARCHAR2 (4000) ;
    --
    v_lock_dtls_id NUMBER;
    v_comments VARCHAR2(4000);
    --
    v_plus_cnt NUMBER;
    v_minus_cnt NUMBER;
    --
    v_cyc_status_id t2705_cycle_count_lock.c901_lock_status%type;
    --
    v_count_type t2701_cycle_count_location.c901_count_type%TYPE;
    v_out_err_msg VARCHAR2(4000);
    BEGIN
	    
	-- to get the lock
	 SELECT c901_lock_status
   INTO v_cyc_status_id
   FROM t2705_cycle_count_lock
  WHERE c2705_cyc_cnt_lock_id = p_cyc_lock_id FOR UPDATE;

  -- 
  IF v_cyc_status_id <> 106829
  THEN
  	raise_application_error (-20999, 'Cycle Count Id '|| p_cyc_lock_id ||' already Approved ');
  END IF;
  --
IF v_strlen                      > 0 THEN
        WHILE instr (v_string, '|') <> 0
        LOOP
             v_substring    := SUBSTR (v_string, 1, instr (v_string, '|') - 1) ;
            v_string       := SUBSTR (v_string, instr (v_string, '|')    + 1) ;
            v_lock_dtls_id := NULL;
            v_comments  := NULL;
            v_lock_dtls_id := SUBSTR (v_substring, 1, instr (v_substring, '^') - 1) ;
            v_substring    := SUBSTR (v_substring, instr (v_substring, '^')    + 1) ;
            v_comments  := v_substring ;
            -- 1. to update the Comments 
            UPDATE t2706_cycle_count_lock_dtls
    SET c2706_comments   = v_comments
      , c2706_last_updated_by          = p_user_id, c2706_last_updated_date = CURRENT_DATE
      WHERE c2706_cyc_cnt_lock_dtls_id = v_lock_dtls_id
        AND c2706_void_fl             IS NULL;
        END LOOP;
       --
        
    END IF; -- length check
--	

     -- to get the warehouse type
	     SELECT t2701.c901_count_type
	     INTO v_count_type
   FROM t2705_cycle_count_lock t2705, t2701_cycle_count_location t2701
  WHERE t2701.c2701_cyc_cnt_location_id = t2705.c2701_cyc_cnt_location_id
    AND t2705.c2705_cyc_cnt_lock_id     = p_cyc_lock_id
    AND t2705.c2705_void_fl            IS NULL
    AND t2701.c2701_void_fl            IS NULL ;

    IF v_count_type = 106815 -- Part
	THEN
		     SELECT COUNT(1) INTO v_plus_cnt
	   FROM T2706_CYCLE_COUNT_LOCK_DTLS t2706
	  WHERE C2705_CYC_CNT_LOCK_ID = p_cyc_lock_id
	  AND ( NVL (t2706.c2706_override_qty, t2706.c2706_system_qty) - c2706_physical_qty) <0 -- plus
	    AND C2706_VOID_FL        IS NULL;
	--
	 SELECT COUNT(1) INTO v_minus_cnt
	   FROM T2706_CYCLE_COUNT_LOCK_DTLS t2706
	  WHERE C2705_CYC_CNT_LOCK_ID = p_cyc_lock_id
	  AND ( NVL (t2706.c2706_override_qty, t2706.c2706_system_qty) - c2706_physical_qty) > 0 -- minus
	    AND C2706_VOID_FL        IS NULL;
		--
		IF v_plus_cnt <> 0
		THEN
		gm_create_inhouse_trans_adj (p_cyc_lock_id, 'PLUS', p_user_id);
		
		END IF;
		
		IF v_minus_cnt <> 0
		THEN
		-- validate the warehouse Qty
		gm_validate_current_inv_qty (p_cyc_lock_id, v_out_err_msg);
		--
		IF v_out_err_msg IS NOT NULL
		THEN
			v_out_err_msg := SUBSTR(v_out_err_msg, 0, NVL (LENGTH (v_out_err_msg), 0) - 2 );
			--
			raise_application_error (-20999, 'Inventory Qty not available for following Part(s): '|| v_out_err_msg);
		END IF;
		--
		gm_create_inhouse_trans_adj (p_cyc_lock_id, 'MINUS', p_user_id);
		END IF; -- end if v_minus_cnt
	    
	END IF; -- count type
	-- approved
	gm_pkg_ac_cycle_count_trans.gm_upd_cycle_count_status (p_cyc_lock_id, 106831, p_user_id) ;
 END gm_sav_approval_qty_dtls;
	  
	    
	/**********************************************************************************************
    * description      : procedure used to create the adjustment Transaction 
    * author           : mmuthusamy
    ***********************************************************************************************/
	    
	 PROCEDURE gm_create_inhouse_trans_adj(
    p_cyc_lock_id    IN t2706_cycle_count_lock_dtls.c2705_cyc_cnt_lock_id%type,
	        p_action      IN VARCHAR2,
	        p_user_id        IN t2706_cycle_count_lock_dtls.c2706_last_updated_by%type)
    AS
    v_warehouse_type NUMBER;
    v_rule_type VARCHAR2 (40);
    --
    v_inhouse_txn_id VARCHAR2 (40);
    v_inhouse_type NUMBER;
    v_input_str VARCHAR2(30000);
    v_control_input_str VARCHAR2(30000);
    v_verify_input_str  VARCHAR2(30000);
    v_out_msg VARCHAR2(4000);
    --
    v_other_input_str  VARCHAR2(30000);
    v_error_msg VARCHAR2 (4000);
    v_txn_type NUMBER;
    --
    v_count_type t2701_cycle_count_location.c901_count_type%TYPE;
    v_table_nm t906_rules.c906_rule_value%TYPE;
    v_company_id t1900_company.c1900_company_id%TYPE;
    --
    CURSOR inhouse_plus_cur
    IS
    	       SELECT t2706.c205_part_number_id pnum, NVL(c2706_physical_qty - NVL (t2706.c2706_override_qty, t2706.c2706_system_qty), 0) qty,
             NULL cnum, NVL (t2052.c2052_equity_price, 0) price, t2706.c5052_location_id location_id
       FROM t2706_cycle_count_lock_dtls t2706, t2052_part_price_mapping t2052
      WHERE c2705_cyc_cnt_lock_id = p_cyc_lock_id
      AND t2706.c205_part_number_id  = t2052.c205_part_number_id
      AND NVL(t2706.c2706_physical_qty - NVL (t2706.c2706_override_qty, t2706.c2706_system_qty), 0) > 0 -- plus
      AND T2052.c1900_company_id = v_company_id
      AND t2706.c2706_void_fl        IS NULL
      AND t2052.c2052_void_fl   IS NULL ;
    --
    CURSOR inhouse_minus_cur
    IS
    	       SELECT t2706.c205_part_number_id pnum, -1 * NVL(c2706_physical_qty - NVL (t2706.c2706_override_qty, t2706.c2706_system_qty), 0) qty,
             NULL cnum, NVL (t2052.c2052_equity_price, 0) price, t2706.c5052_location_id location_id
       FROM t2706_cycle_count_lock_dtls t2706, t2052_part_price_mapping t2052
      WHERE c2705_cyc_cnt_lock_id = p_cyc_lock_id
      AND t2706.c205_part_number_id  = t2052.c205_part_number_id
      AND NVL(t2706.c2706_physical_qty - NVL (t2706.c2706_override_qty, t2706.c2706_system_qty), 0) < 0 -- minus
      AND t2052.c1900_company_id = v_company_id
      AND t2706.c2706_void_fl        IS NULL
      AND t2052.c2052_void_fl   IS NULL ;
    
    BEGIN
	    -- to get the warehouse type
	     SELECT t2701.c901_warehouse_location, get_rule_value ( t2701.c901_warehouse_location, 'CYC_CNT_ADJ_'|| p_action), DECODE(p_action, 'PLUS', 4301, 4302)
	     , t2701.c901_count_type, get_compid_frm_cntx ()
	     INTO v_warehouse_type, v_rule_type, v_txn_type
	     , v_count_type, v_company_id
   FROM t2705_cycle_count_lock t2705, t2701_cycle_count_location t2701
  WHERE t2701.c2701_cyc_cnt_location_id = t2705.c2701_cyc_cnt_location_id
    AND t2705.c2705_cyc_cnt_lock_id     = p_cyc_lock_id
    AND t2705.c2705_void_fl            IS NULL
    AND t2701.c2701_void_fl            IS NULL ;

	    --1 to create the in house transactions id 
	    SELECT get_next_consign_id (v_rule_type), GET_RULE_VALUE(v_rule_type,'TRANS_CONTY') INTO v_inhouse_txn_id, v_inhouse_type FROM DUAL;
	    --
	    select NVL(GET_RULE_VALUE('TRANS_TABLE',v_inhouse_type), '-999') INTO v_table_nm from dual;
    	-- 2 to call the existing procedure and save the Inhouse and details
    	--
	    IF p_action = 'PLUS'
	    THEN
	    
		    FOR inhouse_plus IN inhouse_plus_cur
		    LOOP
		    	v_input_str := v_input_str || inhouse_plus.pnum || ',' || inhouse_plus.qty || ',' || inhouse_plus.cnum ||',' || inhouse_plus.price || '|';
		    	--
		    	v_control_input_str := v_control_input_str || inhouse_plus.pnum || ',' || inhouse_plus.qty || ',NOC#,,,90800|';
		    	--
		    	v_verify_input_str := v_verify_input_str ||  inhouse_plus.pnum || ',' || inhouse_plus.qty || ',NOC#,' || inhouse_plus.location_id ||',NOC#,90800|';
		    	--
		    	v_other_input_str := v_other_input_str || inhouse_plus.pnum || ',' || inhouse_plus.qty || ',NOC#,' || inhouse_plus.price ||'|';
		    END LOOP;
	    
	    ELSE
		    FOR inhouse_minus IN inhouse_minus_cur
		    LOOP
		    	v_input_str := v_input_str || inhouse_minus.pnum || ',' || inhouse_minus.qty || ',' || inhouse_minus.cnum ||',' || inhouse_minus.price || '|';
		    	--
		    	v_control_input_str := v_control_input_str || inhouse_minus.pnum || ',' || inhouse_minus.qty || ',NOC#,' || inhouse_minus.location_id ||',,90800' || '|';
		    	--
		    	v_verify_input_str := v_verify_input_str ||  inhouse_minus.pnum || ',' || inhouse_minus.qty || ',NOC#,,NOC#,90800|';
		    	--
		    	v_other_input_str := v_other_input_str || inhouse_minus.pnum || ',' || inhouse_minus.qty || ',NOC#,' || inhouse_minus.price ||'|';
		    END LOOP;
	    END IF;
	    
	    IF v_count_type = 106815 -- Part
	    THEN
	    
	    	IF v_table_nm <> 'CONSIGNMENT'
	    	THEN
	    
		    	-- Inhouse id, type, inhouse purpose, ship to , ship to id, comments, user id, input string, out msg
			    -- gm_save_inhouse_item_consign (v_inhouse_txn_id, v_inhouse_type, 106940, 1, NULL, 'Transactions created for Cycle Count', p_user_id, v_input_str, v_out_msg);
			    gm_save_inhouse_item_consign (v_inhouse_txn_id, v_inhouse_type, 106940, 01, NULL, 'Transactions created for Cycle Count: '|| p_cyc_lock_id, p_user_id, v_input_str, v_out_msg);
			    --
			    -- Location
			    IF (v_warehouse_type = 106800 OR v_warehouse_type = 106801 OR v_warehouse_type = 106802 OR v_warehouse_type = 106803)
			    THEN
			    	-- 3 to control the changes.
				    gm_pkg_op_item_control_txn.gm_sav_item_control_main (v_inhouse_txn_id, v_inhouse_type, 'on',  v_control_input_str, 93343, p_user_id);
					--				    

				    -- 4 verify the changes
				    -- 93345 Put Transacted
				    -- gm_pkg_op_item_control_txn.gm_sav_item_verify_main (Txn ID, Type, Input String, Transacted, User ID );
				    -- Input string - 124.456,1,BGN212CV,19998,BGN212CV,90800|124.945,1,VPH324ND,,VPH324ND,90800|
					    gm_pkg_op_item_control_txn.gm_sav_item_verify_main (v_inhouse_txn_id, v_inhouse_type, v_verify_input_str, 93345, p_user_id );
				    --
			    ELSE
				    --3 control
				    -- gm_ls_upd_inloanercsg ( txn id, type, purpouse, shipto , shipto id, comments, User id, input string (part, qty, Cnum, price), ref id, actions, * out msg)
				    	gm_ls_upd_inloanercsg (v_inhouse_txn_id, v_inhouse_type, NULL, NULL, NULL, NULL, p_user_id, v_other_input_str, NULL, 'ReleaseControl', v_out_msg);
				    
				    --4 verify (Raw Material transcation (RMIA)
				    IF v_table_nm = '-999'
				    THEN
					    --GM_UPDATE_REPROCESS_MATERIAL (Trans ID, Comments, Type, Verifiy By, User ID, * msg);
					    gm_update_reprocess_material (v_inhouse_txn_id, NULL, v_inhouse_type, p_user_id, p_user_id, v_out_msg);
					    --
					ELSE
					    -- verify
					    gm_ls_upd_inloanercsg (v_inhouse_txn_id, v_inhouse_type, NULL, NULL, NULL, NULL, p_user_id, NULL, NULL, 'Verify', v_out_msg);
				    END IF; -- end if inside v_table_nm
				 END IF; --  v_warehouse_type
				   
				ELSE 
					-- create the CN
					--gm_save_item_consign (CN ID, Type, Purpose, Bill to, ship to, ship to id, comments, Ship fl, User id, input string)	
					gm_save_item_consign (v_inhouse_txn_id, v_inhouse_type, 106940, NULL, NULL, p_user_id, 'Transactions created for Cycle Count: '|| p_cyc_lock_id, 1, p_user_id, v_input_str);
					
					-- contorl
					
					--gm_save_item_build (CN id, input string, Ship fl, user id)
					gm_save_item_build (v_inhouse_txn_id, v_other_input_str, 3, p_user_id);
					-- verify
					
					--gm_update_reprocess_material (Trans ID, Comments, Type, Verifiy By, User ID, * msg);
					gm_update_reprocess_material (v_inhouse_txn_id, NULL, v_inhouse_type, p_user_id, p_user_id, v_out_msg);
					
		    	END IF;-- end if outside v_table_nm
		    --
		    insert into T2707_CYCLE_COUNT_TXN_REF (C2707_CYC_CNT_TXN_REF_ID,C2705_CYC_CNT_LOCK_ID, C2707_TXN_ID,  C2707_LAST_UPDATED_BY, C2707_LAST_UPDATED_DATE)
			values (s2707_CYCLE_COUNT_TXN_REF.nextval, p_cyc_lock_id, v_inhouse_txn_id, p_user_id, CURRENT_DATE);
		--based on variance qty - to update Transaction id
			   IF p_action = 'PLUS'
			   THEN
			    UPDATE T2706_CYCLE_COUNT_LOCK_DTLS t2706
 			       SET c2706_txn_id =v_inhouse_txn_id
 			       , c2706_last_updated_by = p_user_id
 			       , c2706_last_updated_date = CURRENT_DATE
 			    WHERE c2705_cyc_cnt_lock_id = p_cyc_lock_id
      			AND NVL(t2706.c2706_physical_qty - NVL (t2706.c2706_override_qty, t2706.c2706_system_qty), 0) > 0 -- Plus
      			AND t2706.c2706_void_fl        IS NULL;
			  ELSIF p_action = 'MINUS' THEN
			    UPDATE T2706_CYCLE_COUNT_LOCK_DTLS t2706
 			       SET c2706_txn_id =v_inhouse_txn_id
 			       , c2706_last_updated_by = p_user_id
 			       , c2706_last_updated_date = CURRENT_DATE
 			    WHERE c2705_cyc_cnt_lock_id = p_cyc_lock_id
      			AND NVL(t2706.c2706_physical_qty - NVL (t2706.c2706_override_qty, t2706.c2706_system_qty), 0) < 0 -- MINUS
      			AND t2706.c2706_void_fl        IS NULL;
			   END IF;			
	    END IF; -- v_count_type
	    
	    END gm_create_inhouse_trans_adj;
	    
	/**********************************************************************************************
	* description      : procedure used validate the current Qty and variance Qty
	* If Qty not avialble then throw an error message
	* author           : mmuthusamy
	***********************************************************************************************/
	PROCEDURE gm_validate_current_inv_qty (
	        p_cyc_lock_id IN t2706_cycle_count_lock_dtls.c2705_cyc_cnt_lock_id%type,
	        p_out_err_dtls OUT VARCHAR2)
	AS
	    v_warehouse_type t2701_cycle_count_location.c901_warehouse_location%TYPE;
	    v_count_type t2701_cycle_count_location.c901_count_type%TYPE;
	    v_lock_type t2701_cycle_count_location.c901_lock_type%TYPE;
	    v_plant_cnt_id t5040_plant_master.c5040_plant_id%TYPE;
	    v_rule_inv_map_id  VARCHAR2 (400) ;
	    v_exculde_inv_type VARCHAR2 (4000) ;
	    
	BEGIN
	       
	    -- to get the warehouse type
	     SELECT t2701.c901_warehouse_location, NVL (get_rule_value (t2701.c901_warehouse_location, 'CYC_CNT_LOCK_INV'), 0),
	        t2701.c901_count_type, t2701.c901_lock_type, t2700.c5040_plant_id
	       INTO v_warehouse_type, v_rule_inv_map_id, v_count_type
	      , v_lock_type, v_plant_cnt_id
	       FROM t2705_cycle_count_lock t2705, t2701_cycle_count_location t2701
	       , t2700_cycle_count t2700
	      WHERE t2701.c2701_cyc_cnt_location_id = t2705.c2701_cyc_cnt_location_id
	      	AND t2700.c2700_cycle_count_id = t2701.c2700_cycle_count_id
	        AND t2705.c2705_cyc_cnt_lock_id     = p_cyc_lock_id
	        AND t2700.c2700_void_fl            IS NULL
	        AND t2705.c2705_void_fl            IS NULL
	        AND t2701.c2701_void_fl            IS NULL ;
	        
	    --106824 Project ID
	    -- to insert the inventory bucket exclude types
	    IF v_lock_type = 106824 THEN
	        -- exclude the inventory type
	        my_context.set_my_cloblist (v_rule_inv_map_id || ',') ;
	        
	        --
	         SELECT rtrim (xmlagg (xmlelement (e, c901_code_id || ',')) .extract ('//text()'), ',') ||','
	           INTO v_exculde_inv_type
	           FROM t901_code_lookup
	          WHERE c901_code_grp     = 'INVBKT'
	            AND c901_active_fl    = 1
	            AND c901_code_id NOT IN
	            (
	                 SELECT token FROM v_clob_list
	            ) ;
	        
	        --set the exclusion so the "v205_qty_in_stock" returns data faster.
	        
	        my_context.set_my_inlist_ctx (v_exculde_inv_type) ;
	        
	    END IF;
	    
	    
	    -- to insert the variance parts to temp table
	    --1 delete the temp table
	     DELETE
	       FROM my_temp_part_list;
	       
	    -- 2 insert the temp table (parts and variance Qty)
	     INSERT
	       INTO my_temp_part_list
	        (
	            c205_part_number_id, c205_qty
	        )
	     SELECT c205_part_number_id, NVL (t2706.c2706_physical_qty - NVL (t2706.c2706_override_qty, t2706.c2706_system_qty), 0)
	       FROM t2706_cycle_count_lock_dtls t2706
	      WHERE c2705_cyc_cnt_lock_id                                                                      = p_cyc_lock_id
	        AND NVL (t2706.c2706_physical_qty - NVL (t2706.c2706_override_qty, t2706.c2706_system_qty), 0) < 0 -- minus
	        AND C2706_VOID_FL                                                                             IS NULL;
	        
	    -- to insert the location
	    
	        delete from my_temp_list;
	        --
			INSERT INTO my_temp_list (my_temp_txn_id)
	        SELECT t2706.c5052_location_id
	       FROM t2706_cycle_count_lock_dtls t2706
	      WHERE c2705_cyc_cnt_lock_id                                                                      = p_cyc_lock_id
	        AND NVL (t2706.c2706_physical_qty - NVL (t2706.c2706_override_qty, t2706.c2706_system_qty), 0) < 0 -- minus
	        AND C2706_VOID_FL                                                                             IS NULL
	        AND t2706.c5052_location_id IS NOT NULL;

	    -- 93320 pick face
	    -- 93336 bulk
	    -- FG Pick
	    IF (v_warehouse_type = 106800) THEN
	        gm_validate_location_qty (1, 93320, v_plant_cnt_id, p_out_err_dtls) ;
	        
	        -- FG - Bulk
	    ELSIF (v_warehouse_type = 106801) THEN
	        gm_validate_location_qty (1, 93336, v_plant_cnt_id, p_out_err_dtls) ;
	        
	        -- RW - pick
	    ELSIF (v_warehouse_type = 106802) THEN
	        gm_validate_location_qty (2, 93320, v_plant_cnt_id, p_out_err_dtls) ;
	        
	        -- RW - bulk
	    ELSIF (v_warehouse_type = 106803) THEN
	        gm_validate_location_qty (2, 93336, v_plant_cnt_id, p_out_err_dtls) ;
	        
	        -- Repack
	    ELSIF (v_warehouse_type = 106804) THEN
	        gm_validate_repack_inv_qty (p_out_err_dtls) ;
	        
	        -- QN or QN S
	    ELSIF (v_warehouse_type = 106805 OR v_warehouse_type = 106806) THEN
	        gm_validate_qn_inv_qty (p_out_err_dtls) ;
	        
	        -- RM
	    ELSIF (v_warehouse_type = 106807) THEN
	        gm_validate_rm_inv_qty (p_out_err_dtls) ;
	        
	        -- Bulk loose
	    ELSIF (v_warehouse_type = 106808) THEN
	        gm_validate_bulk_loose_qty (p_out_err_dtls) ;
	        
	        --Inhouse loose
	    ELSIF (v_warehouse_type = 106809) THEN
	        gm_validate_inhouse_loose_qty (p_out_err_dtls) ;
	        
	    END IF;
	    
	END gm_validate_current_inv_qty;
	
	
	/**********************************************************************************************
	* description      : to validate the FG and RW warehouse (Location) Qty
	* author           : mmuthusamy
	***********************************************************************************************/
	PROCEDURE gm_validate_location_qty (
	        p_warehouse_id  IN t5051_inv_warehouse.c5051_inv_warehouse_id%type,
	        p_location_type IN t5052_location_master.c901_location_type%type,
	        p_plant_id      IN t5040_plant_master.c5040_plant_id%type,
	        p_out_err_dtls OUT VARCHAR2)
	AS
	    CURSOR inv_part_cur
	    IS
	         SELECT t5053.c205_part_number_id pnum, t5053.c5053_curr_qty - NVL ( alloc.qty, 0) avilable_qty,
	            my_temp.c205_qty variance_qty
	           FROM t5051_inv_warehouse t5051, t5052_location_master t5052, t5053_location_part_mapping t5053
	          , t205_part_number t205, my_temp_part_list my_temp, (
	                 SELECT t5055.c5052_location_id, t5055.c205_part_number_id, NVL (SUM (t5055.c5055_qty), 0) qty
	                   FROM t5055_control_number t5055, my_temp_part_list my_temp
	                  WHERE t5055.c901_type             = 93343 -- pick transacted
	                    AND my_temp.c205_part_number_id = t5055.c205_part_number_id
	                    AND t5055.c5055_void_fl        IS NULL
	                    AND t5055.c5055_update_inv_fl  IS NULL
	                    AND t5055.c5040_plant_id        = p_plant_id -- plant id
	               GROUP BY t5055.c5052_location_id, t5055.c205_part_number_id
	            )
	        alloc
	      WHERE t5051.c5051_inv_warehouse_id = t5052.c5051_inv_warehouse_id(+)
	        AND t5052.c5052_location_id      = t5053.c5052_location_id(+)
	        AND t205.c205_part_number_id     = my_temp.c205_part_number_id
	        AND t5053.c205_part_number_id    = t205.c205_part_number_id
	        AND EXISTS (
	        SELECT my_temp_txn_id FROM my_temp_list WHERE my_temp_txn_id = t5052.c5052_location_id
	        )
	        AND t5053.c205_part_number_id    = alloc.c205_part_number_id (+)
	        AND t5053.c5052_location_id      = alloc.c5052_location_id(+)
	        AND t5051.c901_status_id         = 1 -- active
	        AND t5051.c5051_inv_warehouse_id = p_warehouse_id -- fg warehouse,rw warehouse
	        AND t5052.c901_status            = 93310 --active
	        AND t5052.c901_location_type     = p_location_type --  bulk,pick face
	        AND t5052.c5052_void_fl         IS NULL
	        AND t5052.c5040_plant_id         = p_plant_id -- audubon
	        -- Qty not available check
	        AND (t5053.c5053_curr_qty - NVL (alloc.qty, 0)) + my_temp.c205_qty < 0 -- negative Qty
	        AND t5053.c205_part_number_id                                     IS NOT NULL;
	BEGIN
	    --
	    FOR inv_part IN inv_part_cur
	    LOOP
	        -- error parts
	        p_out_err_dtls := p_out_err_dtls || inv_part.pnum || ', ';
	    END LOOP;
	END gm_validate_location_qty;
	
	
	/**********************************************************************************************
	* description      : to validate the Repack - current inventory and variance  Qty
	* author           : mmuthusamy
	***********************************************************************************************/
	PROCEDURE gm_validate_repack_inv_qty (
	        p_out_err_dtls OUT VARCHAR2)
	AS
	    CURSOR repack_neg_parts_cur
	    IS
	         SELECT v205.c205_part_number_id pnum, NVL (c205_repackavail, 0) repack_qty, my_temp.c205_qty variance_qty
	           FROM v205_qty_in_stock v205, my_temp_part_list my_temp
	          WHERE v205.c205_part_number_id = my_temp.c205_part_number_id 
	          AND NVL (c205_repackavail, 0) + my_temp.c205_qty < 0; -- negative Qty
	BEGIN
		--
	    FOR repack_parts IN repack_neg_parts_cur
	    LOOP
	        -- error parts
	        p_out_err_dtls := p_out_err_dtls || repack_parts.pnum || ', ';
	    END LOOP;
	END gm_validate_repack_inv_qty;
	
	/**********************************************************************************************
	* description      : to validate the QN - current inventory and variance  Qty
	* author           : mmuthusamy
	***********************************************************************************************/
	PROCEDURE gm_validate_qn_inv_qty (
	        p_out_err_dtls OUT VARCHAR2)
	AS
	    CURSOR qn_neg_parts_cur
	    IS
	         SELECT v205.c205_part_number_id pnum, NVL (c205_inquaran, 0) qn_qty, my_temp.c205_qty variance_qty
	           FROM v205_qty_in_stock v205, my_temp_part_list my_temp
	          WHERE v205.c205_part_number_id = my_temp.c205_part_number_id 
	          AND NVL (c205_inquaran, 0) + my_temp.c205_qty < 0; -- negative Qty
	BEGIN
		--
	    FOR qn_parts IN qn_neg_parts_cur
	    LOOP
	        -- error parts
	        p_out_err_dtls := p_out_err_dtls || qn_parts.pnum || ', ';
	    END LOOP;
	END gm_validate_qn_inv_qty;
	
	/**********************************************************************************************
	* description      :  to validate the RM - current inventory and variance  Qty
	* author           : mmuthusamy
	***********************************************************************************************/
	PROCEDURE gm_validate_rm_inv_qty (
	        p_out_err_dtls OUT VARCHAR2)
	AS
	    CURSOR rm_neg_parts_cur
	    IS
	         SELECT v205.c205_part_number_id pnum, NVL (rmqty, 0) raw_material_qty, my_temp.c205_qty variance_qty
	           FROM v205_qty_in_stock v205, my_temp_part_list my_temp
	          WHERE v205.c205_part_number_id = my_temp.c205_part_number_id 
	          AND NVL (rmqty, 0) + my_temp.c205_qty < 0; -- negative Qty
	BEGIN
		--
	    FOR rm_parts IN rm_neg_parts_cur
	    LOOP
	        -- error parts
	        p_out_err_dtls := p_out_err_dtls || rm_parts.pnum || ', ';
	    END LOOP;
	END gm_validate_rm_inv_qty;
	
	
	/**********************************************************************************************
	* description      :  to validate the bulk loose - current inventory and variance  Qty
	* author           : mmuthusamy
	***********************************************************************************************/
	PROCEDURE gm_validate_bulk_loose_qty (
	        p_out_err_dtls OUT VARCHAR2)
	AS
	    CURSOR bulk_neg_parts_cur
	    IS
	         SELECT v205.c205_part_number_id pnum, NVL (c205_inbulk, 0) in_bulk_qty, my_temp.c205_qty variance_qty
	           FROM v205_qty_in_stock v205, my_temp_part_list my_temp
	          WHERE v205.c205_part_number_id = my_temp.c205_part_number_id 
	          AND NVL (c205_inbulk, 0) + my_temp.c205_qty < 0; -- negative Qty
	BEGIN
		--
	    FOR bulk_parts IN bulk_neg_parts_cur
	    LOOP
	        -- error parts
	        p_out_err_dtls := p_out_err_dtls || bulk_parts.pnum || ', ';
	    END LOOP;
	END gm_validate_bulk_loose_qty;
	
	
	/**********************************************************************************************
	* description      :  to validate the inhouse loose - current inventory and variance  Qty
	* author           : mmuthusamy
	***********************************************************************************************/
	PROCEDURE gm_validate_inhouse_loose_qty (
	        p_out_err_dtls OUT VARCHAR2)
	AS
	    CURSOR ih_neg_parts_cur
	    IS
	         SELECT v205.c205_part_number_id pnum, NVL (c205_ih_avail_qty, 0) ih_qty, my_temp.c205_qty variance_qty
	           FROM v205_qty_in_stock v205, my_temp_part_list my_temp
	          WHERE v205.c205_part_number_id = my_temp.c205_part_number_id 
	          AND NVL (c205_ih_avail_qty, 0) + my_temp.c205_qty < 0; -- negative Qty
	BEGIN
		--
	    FOR ih_parts IN ih_neg_parts_cur
	    LOOP
	        -- error parts
	        p_out_err_dtls := p_out_err_dtls || ih_parts.pnum || ', ';
	    END LOOP;
	END gm_validate_inhouse_loose_qty;
	
	
	
	
/********************************************************************
* Description : Procedure to save the Part Number
* Author	  : Prabhuvigneshwaran			
*************************************************************************/
PROCEDURE gm_add_new_parts (
 		p_cyc_lock_id 	IN t2706_cycle_count_lock_dtls.c2705_cyc_cnt_lock_id%type,
     	p_partinputstr 	IN CLOB,
        p_inputstr    	IN CLOB,
        p_user_id       IN t2706_cycle_count_lock_dtls.c2706_last_updated_by%type
       						)
AS
    v_location T2706_CYCLE_COUNT_LOCK_DTLS.C5052_LOCATION_ID %TYPE;
    v_string    CLOB := p_inputstr;
    v_substring VARCHAR2 (4000) ;
    v_msg_part   CLOB;
    --Invalid part
    v_msg_invalid_part   VARCHAR2 (4000);
    v_msg_prt_cmpny  VARCHAR2 (4000);
    --part number split
    v_part_substring VARCHAR2(4000);
	v_part_no   VARCHAR2(4000);
	v_part_no_str VARCHAR2(4000);
	-- bug10075
	v_lock_status T2705_CYCLE_COUNT_LOCK.C901_LOCK_STATUS%TYPE;
	v_recount_fl VARCHAR2(1);
BEGIN
	--To call Invalid part Number validation  
  gm_pkg_pd_partnumber.gm_validate_mfg_parts(p_partinputstr,v_msg_invalid_part,v_msg_prt_cmpny);
     IF v_msg_invalid_part IS NOT NULL THEN
      v_msg_part := 'The following Part(s) not Available: '|| v_msg_invalid_part || '<BR>';
     END IF;
    IF v_msg_prt_cmpny IS NOT NULL THEN
      v_msg_part :=  v_msg_part || ' Part not mapped for this company: '|| v_msg_prt_cmpny ;
    END IF;
    IF v_msg_part IS NOT null THEN
        raise_application_error(-20999 ,v_msg_part );
    END IF;
      --
    SELECT C901_LOCK_STATUS INTO v_lock_status
	FROM T2705_CYCLE_COUNT_LOCK WHERE 
	C2705_CYC_CNT_LOCK_ID = p_cyc_lock_id;
	
	IF v_lock_status = 106830 --	Recounted
	THEN
	v_recount_fl := 'Y';
	END IF;
	--
    
    	UPDATE 	T2706_CYCLE_COUNT_LOCK_DTLS
				SET C2706_VOID_FL='Y',
				    C2706_LAST_UPDATED_BY    = p_user_id,
				    C2706_LAST_UPDATED_DATE = CURRENT_DATE
				WHERE C2705_CYC_CNT_LOCK_ID= p_cyc_lock_id
				AND	 C2706_VOID_FL IS NULL                    
				AND C2706_NEW_PART_FL IS NOT NULL;
				--V_string:=location^partnumber,Partnumber1,|
        	WHILE INSTR (v_string, '|') <> 0
        		  LOOP
            			v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
            			v_string    := SUBSTR (v_string, INSTR (v_string, '|')    + 1) ;
            			v_location  := NULL;
           				v_location  := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            			v_substring := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
            			v_part_no_str   := v_substring ;
          		WHILE INSTR (v_part_no_str, ',') <> 0
          			  LOOP
           				 v_part_substring := SUBSTR (v_part_no_str, 1, INSTR (v_part_no_str, ',') - 1) ;
            			 v_part_no_str    := SUBSTR (v_part_no_str, INSTR (v_part_no_str, ',')    + 1) ;	
            			 v_part_no   := v_part_substring ;
            			 IF v_part_no IS NOT NULL THEN
            			 	gm_pkg_ac_cycle_count_trans.gm_sav_new_location_parts(p_cyc_lock_id,v_location,v_recount_fl,v_part_no,p_user_id);
						 END IF;
     			END LOOP;--part number split loop
		END LOOP;--String loop
     
END gm_add_new_parts;  
/********************************************************************
* Description : Procedure to add New location Part Number
* Author	  : Prabhuvigneshwaran			
*************************************************************************/

PROCEDURE gm_sav_new_location_parts(
		  	p_cyc_lock_id 	IN T2706_CYCLE_COUNT_LOCK_DTLS.C2705_CYC_CNT_LOCK_ID%TYPE,
		  	p_location      IN T2706_CYCLE_COUNT_LOCK_DTLS.C5052_LOCATION_ID %TYPE,
		  	p_recount_fl    IN VARCHAR2,
         	p_part_no    	IN VARCHAR2,
		 	p_user_id       IN T2706_CYCLE_COUNT_LOCK_DTLS.C2706_LAST_UPDATED_BY%TYPE							
									)
AS
BEGIN
						
							UPDATE 	T2706_CYCLE_COUNT_LOCK_DTLS
							SET 	C2706_LAST_UPDATED_BY    = p_user_id,
 									C2706_VOID_FL              = NULL,
 									C2706_NEW_PART_FL          = 'Y',
  									C2706_NEW_PART_ADDED_BY    = p_user_id,
  									C2706_NEW_PART_ADDED_DATE  = CURRENT_DATE,
  									C2706_CYCLE_COUNT_COST     = 15
  									
							WHERE 	C2705_CYC_CNT_LOCK_ID  = p_cyc_lock_id
							AND 	C5052_LOCATION_ID           = p_location
							AND 	C205_PART_NUMBER_ID      = p_part_no
							AND 	C2706_NEW_PART_FL       IS NOT NULL;
			       IF (SQL%ROWCOUNT = 0) THEN
  					  INSERT
  					  INTO T2706_CYCLE_COUNT_LOCK_DTLS
    					(
       					C2706_CYC_CNT_LOCK_DTLS_ID,
       					C2706_SYSTEM_QTY,
      					C2705_CYC_CNT_LOCK_ID,
       					C205_PART_NUMBER_ID,
      					C5052_LOCATION_ID,
       					C2706_LAST_UPDATED_BY,
       					C2706_VOID_FL ,
       					C2706_NEW_PART_FL,
       					C2706_RECOUNT_FL,
       					C2706_NEW_PART_ADDED_BY,
       					C2706_NEW_PART_ADDED_DATE
   					 	)
   				 	VALUES
   					 	(
    					S2706_CYCLE_COUNT_LOCK_DTLS.nextval,
    					'0',
    					p_cyc_lock_id,
    					p_part_no,
    					p_location,
     					p_user_id,
      					NULL,
      					'Y',
      					 p_recount_fl,
      					p_user_id,
      					CURRENT_DATE
    					) ;
				END IF;
END gm_sav_new_location_parts;


END gm_pkg_ac_cycle_count_trans;
/