--@"c:/database/packages/operations/purchasing/gm_pkg_ac_cyc_cnt_scheduler.bdy";
CREATE OR REPLACE PACKAGE body gm_pkg_ac_cyc_cnt_scheduler
IS
    --
    /**********************************************************************************************
    * Description      : Procedure used to save the scheduler data to temp table (my_temp_cyc_cnt_scheduler)
    * Author           : mmuthusamy
    ***********************************************************************************************/
PROCEDURE gm_sav_temp_scheduler (
        p_cycle_count_id     IN t2700_cycle_count.c2700_cycle_count_id%TYPE,
        p_cycle_count_loc_id IN t2701_cycle_count_location.c2701_cyc_cnt_location_id%type,
        p_ref_id             IN VARCHAR2)
AS
BEGIN
	--
     INSERT
       INTO my_temp_cyc_cnt_scheduler
        (
            my_temp_seq_id, my_temp_cyc_cnt_id, my_temp_cyc_cnt_location_id
          , my_temp_ref_id
        )
        VALUES
        (
            s_my_temp_cyc_cnt_scheduler.nextval, p_cycle_count_id, p_cycle_count_loc_id
          , p_ref_id
        ) ;
       -- 
END gm_sav_temp_scheduler;

--
/**********************************************************************************************
* Description      : Procedure used to spilt the temp table (using NTILE) and store the details to scheduler table (
T2704)
* Author           : mmuthusamy
***********************************************************************************************/

PROCEDURE gm_sav_spilt_scheduler
    (
        p_cycle_count_id      IN t2700_cycle_count.c2700_cycle_count_id%TYPE,
        p_cycle_count_loc_id  IN t2701_cycle_count_location.c2701_cyc_cnt_location_id%type,
        p_closed_business_day IN NUMBER,
        p_open_business_day   IN NUMBER,
        p_user_id             IN t2701_cycle_count_location.c2701_last_updated_by%TYPE
    )
AS

    v_lock_type t2701_cycle_count_location.c901_lock_type%TYPE;
    --
    v_schedule_date DATE;
    --
    CURSOR update_scheduler_dtls_cur
    IS
         SELECT tmp.my_temp_seq_id seq_id, tmp.my_temp_cyc_cnt_location_id cc_location_id, DECODE (v_lock_type, 106823,
            tmp.my_temp_ref_id, NULL) location_id, DECODE (v_lock_type, 106822, tmp.my_temp_ref_id, 106824,
            tmp.my_temp_ref_id, NULL) part_num, NVL (p_closed_business_day, 0) + ntile (p_open_business_day) over (
            order by NULL) AS num
           FROM my_temp_cyc_cnt_scheduler tmp
          WHERE my_temp_cyc_cnt_id          = p_cycle_count_id
            AND my_temp_cyc_cnt_location_id = p_cycle_count_loc_id
       ORDER BY my_temp_seq_id;
       
BEGIN
    --1. to get the location details (Lock type / count pattern)
     SELECT c901_lock_type
       INTO v_lock_type
       FROM t2701_cycle_count_location
      WHERE c2701_cyc_cnt_location_id = p_cycle_count_loc_id;
    -- 2. loop the data and call the scheudler procedure
    FOR update_scheduler IN update_scheduler_dtls_cur
    LOOP
        --
         SELECT cal_date
           INTO v_schedule_date
           FROM v1900_company_working_days
          WHERE WORKING_DAY = update_scheduler.num;
        --
        -- 3. to insert the data to - T2704 scheduler table.
        gm_pkg_ac_cyc_cnt_scheduler.gm_sav_cyc_cnt_scheduler (p_cycle_count_id, p_cycle_count_loc_id,
        update_scheduler.part_num, update_scheduler.location_id, NULL, v_schedule_date, NULL, p_user_id) ;
        --
    END LOOP; -- end update_scheduler
    --
END gm_sav_spilt_scheduler;


/**********************************************************************************************
* Description      : Procedure used to save the scheduler data to T2704
* Author           : mmuthusamy
***********************************************************************************************/
PROCEDURE gm_sav_cyc_cnt_scheduler (
        p_cyc_cnt_id          IN T2704_CYCLE_COUNT_SCHEDULE.C2700_CYCLE_COUNT_ID%TYPE,
        p_cyc_cnt_location_id IN T2704_CYCLE_COUNT_SCHEDULE.C2701_CYC_CNT_LOCATION_ID%TYPE,
        p_part_number         IN T2704_CYCLE_COUNT_SCHEDULE.c205_part_number_id%TYPE,
        p_location_id         IN T2704_CYCLE_COUNT_SCHEDULE.c5052_location_id%TYPE,
        p_consignment_id      IN T2704_CYCLE_COUNT_SCHEDULE.c504_consignment_id%TYPE,
        p_scheduler_date      IN DATE,
        p_newly_added_fl      IN VARCHAR2,
        p_user_id             IN T2704_CYCLE_COUNT_SCHEDULE.c2704_last_updated_by%TYPE)
AS
    v_schedule_cnt NUMBER;
BEGIN
	
     SELECT COUNT (1)
       INTO v_schedule_cnt
       FROM t2704_cycle_count_schedule
      WHERE c2700_cycle_count_id              = p_cyc_cnt_id
        AND c2701_cyc_cnt_location_id         = p_cyc_cnt_location_id
        AND NVL (c205_part_number_id, '-999') = NVL (p_part_number, '-999')
        AND NVL (c504_consignment_id, '-999') = NVL (p_consignment_id, '-999')
        AND NVL (c5052_location_id, '-999')   = NVL (p_location_id, '-999')
        AND c2704_schedule_date               = TRUNC (p_scheduler_date)
        AND c2704_void_fl                    IS NULL;
    --
    IF (v_schedule_cnt = 0) THEN
        -- to handle the exception - if part #, location not available
        BEGIN
             INSERT
               INTO t2704_cycle_count_schedule
                (
                    c2704_cyc_cnt_schedule_dtls_id, c2700_cycle_count_id, c2701_cyc_cnt_location_id
                  , c205_part_number_id, c504_consignment_id, c5052_location_id
                  , c2704_schedule_date, c2704_last_updated_by, c2704_last_updated_date
                )
                VALUES
                (
                    s2704_CYCLE_COUNT_SCHEDULE.nextval, p_cyc_cnt_id, p_cyc_cnt_location_id
                  , p_part_number, p_consignment_id, p_location_id
                  , p_scheduler_date, p_user_id, CURRENT_DATE
                ) ;
        EXCEPTION
        WHEN OTHERS THEN
        
            dbms_output.put_line (' ******************* Exception for the parts # ' || p_part_number ||' Location: '||
            p_cyc_cnt_location_id) ;
            
        END;
    END IF;
    
END gm_sav_cyc_cnt_scheduler;

--
/**********************************************************************************************
* Description      : Procedure used to fetch the cycle count location details based on Cycle count id
* Author           : mmuthusamy
***********************************************************************************************/
PROCEDURE gm_fch_cyc_cnt_location_dtls
    (
        p_cycle_count_id IN T2704_CYCLE_COUNT_SCHEDULE.C2700_CYCLE_COUNT_ID%TYPE,
        p_out_cyc_cnt_location_dtls_cur OUT types.cursor_type
    )
AS

BEGIN
	-- to get the cycle count location details
	
    OPEN p_out_cyc_cnt_location_dtls_cur FOR
     SELECT c2701_cyc_cnt_location_id cyc_cnt_location_id, c901_count_pattern count_pattern
     	, c901_lock_type lock_type
	   FROM t2701_cycle_count_location
	  WHERE c2700_cycle_count_id = p_cycle_count_id
	    -- Set we schedule the separte one - So, exclude the warehouse (Set)
	    AND c901_lock_type <> 106825 -- Set
	    AND c2701_void_fl  IS NULL
	ORDER BY c901_warehouse_location;
    --
END gm_fch_cyc_cnt_location_dtls;

/**********************************************************************************************
* Description      : Procedure used to fetch the Open/Closed business days - based on company
* Author           : mmuthusamy
***********************************************************************************************/
PROCEDURE gm_fch_company_working_day
    (
        p_company_id IN t1900_company.c1900_company_id%TYPE,
        p_action     IN VARCHAR2,
        p_out_closed_business_day OUT NUMBER,
        p_out_open_business_day OUT NUMBER
    )
AS
    v_tmp_workig_day_cnt NUMBER := 0;
    v_tmp_closed_day_cnt NUMBER := 0;
    --
    v_last_working_date DATE;
BEGIN
    -- 1. based on split type to get the number of working days
    IF p_action               = 'FULL_YEAR' THEN
    
    	-- to set the closed day
        v_tmp_closed_day_cnt := 0;
        --
         SELECT COUNT (1) INTO v_tmp_workig_day_cnt FROM v1900_company_working_days;
         --
    ELSIF p_action            = 'FIRST_HALF' THEN
    
    	-- to set the closed day
        v_tmp_closed_day_cnt := 0;
        -- to get the total working days 1 to 6 months
        
         SELECT COUNT (1)
           INTO v_tmp_workig_day_cnt
           FROM v1900_company_working_days
          WHERE month_number BETWEEN 1 AND 6;
          
        --
    ELSIF p_action = 'SECOND_HALF' THEN
    
    	-- to set the closed day
    	
         SELECT COUNT (1)
           INTO v_tmp_closed_day_cnt
           FROM v1900_company_working_days
          WHERE month_number BETWEEN 1 AND 6;
          
        -- to get the total woring days from 7 to 12 months
        
         SELECT COUNT (1)
           INTO v_tmp_workig_day_cnt
           FROM v1900_company_working_days
          WHERE month_number BETWEEN 7 AND 12;
          
          --
    ELSIF p_action = 'CURRENT_DATE' THEN
    
        --1. to get the last working day
        
         SELECT get_last_working_day (CURRENT_DATE)
           INTO v_last_working_date
           FROM dual;
           
        -- 2. to get the last working day count
        -- PMT-46615: CC Set scheduler - to handled the first day exception
        BEGIN
	         SELECT working_day
	           INTO v_tmp_closed_day_cnt
	           FROM v1900_company_working_days
	          WHERE CAL_DATE = v_last_working_date;
        EXCEPTION WHEN OTHERS
        THEN
        	v_tmp_closed_day_cnt := 0;
        END;
        
        --3. to get the working day count
        
         SELECT COUNT (1)
           INTO v_tmp_workig_day_cnt
           FROM v1900_company_working_days;
           
        --4. to calculate the remaing working days
        
        v_tmp_workig_day_cnt := v_tmp_workig_day_cnt - NVL (v_tmp_closed_day_cnt, 0) ;
        
    END IF;
    --
    p_out_closed_business_day := v_tmp_closed_day_cnt;
    p_out_open_business_day   := v_tmp_workig_day_cnt;
    --
END gm_fch_company_working_day;
--
END gm_pkg_ac_cyc_cnt_scheduler;
/
