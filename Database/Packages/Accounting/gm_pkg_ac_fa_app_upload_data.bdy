--@"C:\Database\Packages\Accounting\gm_pkg_ac_fa_app_upload_data.bdy";

create or replace
PACKAGE BODY gm_pkg_ac_fa_app_upload_data
IS
  /*************************************************
  * Description : This procedure is to get Upload Temp table for given audit and field sales
  * Author  : Yoga
  *************************************************/
PROCEDURE gm_fch_temp_upload_data(
    p_audit_id IN t2651_field_sales_details.c2650_field_sales_audit_id%TYPE,
    p_fs_id    IN t2651_field_sales_details.c701_distributor_id%TYPE,
    p_out_upload_temp OUT TYPES.cursor_type)
AS
BEGIN
  OPEN p_out_upload_temp 
    FOR 
      SELECT t2656.* 
        FROM t2656_audit_entry_temp t2656, t2651_field_sales_details t2651 
       WHERE t2656.c2651_field_sales_details_id = t2651.c2651_field_sales_details_id 
        AND t2651.c2650_field_sales_audit_id = p_audit_id 
        AND t2651.c701_distributor_id = p_fs_id 
        AND c2651_void_fl IS NULL;
END gm_fch_temp_upload_data;
/*************************************************
* Description : This procedure is called for Upload Process
* Author  : Mihir
*************************************************/
PROCEDURE gm_sav_upload_data(
    p_audit_id IN t2650_field_sales_audit.c2650_field_sales_audit_id%TYPE,
    p_user_id  IN t101_user.c101_user_id%TYPE)
AS
  --All Distributor for this Audit.
  CURSOR temp_dist_cur
  IS
    SELECT DISTINCT t2651.C2651_FIELD_SALES_DETAILS_ID fsdetid,
      temp.C2651_FIELD_SALES_DETAILS_ID tempfsdetid,
      t2650.C2650_FIELD_SALES_AUDIT_ID auditid,
      t2651.C701_DISTRIBUTOR_ID did
    FROM T2656_AUDIT_ENTRY_TEMP temp,
      t2650_field_sales_audit t2650,
      t2651_field_sales_details t2651
    WHERE t2650.C2650_FIELD_SALES_AUDIT_ID = t2651.c2650_field_sales_audit_id
    AND t2651.C2651_FIELD_SALES_DETAILS_ID = temp.C2651_FIELD_SALES_DETAILS_ID
    AND t2650.C2650_FIELD_SALES_AUDIT_ID   = p_audit_id
      -- AND t2651.C701_DISTRIBUTOR_ID = 613
    AND t2650.C2650_VOID_FL IS NULL
    AND t2651.C2651_VOID_FL IS NULL;
  ---
  v_dist_status t2651_field_sales_details.c901_reconciliation_status%TYPE;
  v_temp_cnt       NUMBER;
  v_erroneous_data NUMBER;
  v_entry_cnt      NUMBER;
  v_cnt            NUMBER := 0;
  v_audit_id t2650_field_sales_audit.C2650_FIELD_SALES_AUDIT_ID%TYPE;
  v_fsdet_cnt          NUMBER := 0;
  v_uploaded_fsdet_cnt NUMBER := 0;
  v_audit_status   NUMBER;
  v_fs_status  NUMBER;
  v_entry_status_cnt NUMBER;
BEGIN
  --Check to see if data exist in temp table
  SELECT COUNT (1)
  INTO v_temp_cnt
  FROM T2656_AUDIT_ENTRY_TEMP temp;
  --
  IF v_temp_cnt < 1 THEN
    GM_RAISE_APPLICATION_ERROR('-20999','91','');
  END IF;
  --Count the # of distinct Field sales being uploaded.
  SELECT COUNT (DISTINCT c2651_field_sales_details_id)
  INTO v_fsdet_cnt
  FROM T2656_AUDIT_ENTRY_TEMP ;
  --
  FOR temp_dist IN temp_dist_cur
  LOOP

    -- Lock audit that is being uploaded
    BEGIN --Audit is not found
      SELECT t2650.C2650_FIELD_SALES_AUDIT_ID
      INTO v_audit_id
      FROM t2650_field_sales_audit t2650
      WHERE t2650.C2650_FIELD_SALES_AUDIT_ID = p_audit_id
      AND t2650.c2650_void_fl               IS NULL FOR UPDATE;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
      v_audit_id := NULL;
    END;--Audit is not found
    --
    IF v_audit_id IS NULL THEN
      GM_RAISE_APPLICATION_ERROR('-20999','92','');
    END IF;
    -- Check the status of the field sales
    BEGIN --Audit is not found
      SELECT c901_reconciliation_status
      INTO v_dist_status
      FROM t2651_field_sales_details t2651,
        t2650_field_sales_audit t2650
      WHERE t2651.C2650_FIELD_SALES_AUDIT_ID = t2650.C2650_FIELD_SALES_AUDIT_ID
        -- AND t2651.c901_reconciliation_status <> 18524 -- Data Downloaded
      AND t2651.c701_distributor_id        = temp_dist.did
      AND t2651.C2650_FIELD_SALES_AUDIT_ID = p_audit_id
      AND t2651.c2651_void_fl             IS NULL
      AND t2650.c2650_void_fl             IS NULL FOR UPDATE;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
      v_dist_status := NULL;
    END;--Audit is not found
    --If field sales was previously uploaded then status will be Data uploaded or some other status
    SELECT COUNT (1)
    INTO v_entry_cnt
    FROM t2656_audit_entry
    WHERE c2656_void_fl             IS NULL
    AND c2651_field_sales_details_id = temp_dist.fsdetid;
    IF v_entry_cnt                   = 0 THEN
      IF v_dist_status              <> 18524 THEN --Data
        -- Downloaded
        GM_RAISE_APPLICATION_ERROR('-20999','93','');
      END IF;
      
    ELSE
    
    /*Draft 18521
      Open 18522
      Locked 18523
      Data Downloaded 18524
      Data Uploaded 18525
      Partially Approved 18526
      Partially Reconciled 18527
      Closed 18528
      Cancelled 18529
      */
      --This validation is   for those distributors that are (partially counted) and are being uploaded from  different laptop
       IF v_dist_status              <> 18524 AND v_dist_status <> 18525 AND v_dist_status <> 18526 THEN --Data Downloaded
        GM_RAISE_APPLICATION_ERROR('-20999','143',get_code_name(v_dist_status));
      END IF;
    END IF;--v_entry_cnt
    --
    IF v_dist_status IS NULL THEN
      GM_RAISE_APPLICATION_ERROR('-20999','94','');
    END IF;
    --
    --
     --If any row is reconciled for this fiels sales then throw an error
    SELECT COUNT (1)
    INTO v_entry_status_cnt
    FROM t2656_audit_entry
    WHERE c2651_field_sales_details_id = temp_dist.fsdetid
    AND NVL(C901_RECONCILIATION_STATUS,'-9999') = 51038 --Reconciled
    AND c2656_void_fl             IS NULL;
    --
    IF v_entry_status_cnt <> 0 THEN
      GM_RAISE_APPLICATION_ERROR('-20999','95','');
    END IF;
    --
    --check if critical data is empty
    SELECT COUNT (1)
    INTO v_erroneous_data
    FROM T2656_AUDIT_ENTRY_TEMP temp
    WHERE (temp.C5010_TAG_ID    IS NULL
    OR temp.C205_PART_NUMBER_ID IS NULL
      -- OR temp.temp_part_control_id IS NULL
      --OR temp.TEMP_SET_ID     IS NULL
    OR temp.C901_ENTRY_TYPE IS NULL
    OR temp.C901_TAG_STATUS IS NULL) ;
    --
    IF v_erroneous_data > 0 THEN
      GM_RAISE_APPLICATION_ERROR('-20999','96','');
    END IF;
    --verify the Field Sales being uploaded matches in uploaded temp table and permanent table
    IF temp_dist.fsdetid <> temp_dist.tempfsdetid THEN
      GM_RAISE_APPLICATION_ERROR('-20999','97','');
    END IF;
    --
    gm_sav_audit_data (temp_dist.fsdetid, p_audit_id, p_user_id) ;
     gm_sav_audit_tag_status (temp_dist.fsdetid, p_audit_id, p_user_id) ;
    --Count the # of distinct Field sales being uploaded.
    v_uploaded_fsdet_cnt := v_uploaded_fsdet_cnt + 1;
    --update the status to data uploaded
    UPDATE t2651_field_sales_details t2651
    SET t2651.c2651_last_updated_by          = p_user_id,
      t2651.c2651_last_updated_date          = SYSDATE,
      t2651.c901_reconciliation_status       = 18525 --Data Uploaded
    WHERE t2651.c2651_field_sales_details_id = temp_dist.fsdetid
    AND t2651.c2651_void_fl                 IS NULL;
  END LOOP;
  --If the # of field sales uploaded in the tmep table <> to the # in permanent table the raise error.
  IF v_fsdet_cnt <> v_uploaded_fsdet_cnt THEN
    GM_RAISE_APPLICATION_ERROR('-20999','98','');
  END IF;
  -- to get the audit status
  SELECT COUNT (1)
   INTO v_audit_status
   FROM t2650_field_sales_audit t2650
  WHERE t2650.C2650_FIELD_SALES_AUDIT_ID = p_audit_id
    AND t2650.c901_audit_status          = 18503 -- Active
    AND t2650.c2650_void_fl             IS NULL;
    -- to get the field sales status
     SELECT COUNT (1)
      INTO v_fs_status
   FROM t2651_field_sales_details t2651
  WHERE t2651.c2650_field_sales_audit_id = p_audit_id
    AND t2651.c901_reconciliation_status = 18525 -- Data Uploaded
    AND t2651.c2651_void_fl             IS NULL;
   -- to update the Main audit status as Active.  
  IF v_audit_status = 0 AND v_fs_status <> 0 THEN

  	UPDATE t2650_field_sales_audit
		SET c901_audit_status = 18503, c2650_last_updated_by = p_user_id    -- 18503 (Active)
	      , c2650_last_updated_date  = SYSDATE
	  WHERE c2650_field_sales_audit_id = p_audit_id
	    AND c2650_void_fl             IS NULL;
  END IF;
  
END gm_sav_upload_data;
/*****************************************************************************
* Description : Procedure to update /insert field sales in audit entry.
* Author: Mihir Patel
*****************************************************************************/
PROCEDURE gm_sav_audit_data(
    p_FIELD_SALES_DETAILS_ID IN t2656_audit_entry.C2651_FIELD_SALES_DETAILS_ID%TYPE,
    p_auditid                IN t2650_field_sales_audit.c2650_field_sales_audit_id%TYPE,
    p_user_id                IN t101_user.c101_user_id%TYPE)
AS

CURSOR uploaded_tag_cur IS
SELECT C2656_AUDIT_ENTRY_ID uauditentid,
        C2651_FIELD_SALES_DETAILS_ID ufsdetid,
        C5010_TAG_ID utagid ,
        C205_PART_NUMBER_ID upartid,
        C2656_CONTROL_NUMBER ucntno,
        C207_SET_ID usetid ,
        1 uqty --C2656_QTY
        ,
        C901_STATUS ustat,
        C2656_COMMENTS ucomm,
        C901_LOCATION_TYPE uloctype ,
        C2656_LOCATION_ID ulocid,
        C901_REF_TYPE ureftype,
        C2656_REF_ID urefid
        --,''  urefdet --C2656_REF_DETAILS
        ,
        C901_ENTRY_TYPE uentrytype
        -- , '' urecstat --C901_RECONCILIATION_STATUS
        ,
        C2656_COUNTED_BY ucntby,
        C2656_COUNTED_DATE ucntdate,
        '' ufsreflockid --Lock id is null because in Field Audit App. the Lock id is not consistent and Lock id are also used for matching tags.
        -- ,'' uupdlist --C903_UPLOAD_FILE_LIST
        --,'' uhistflag --C2656_HISTORY_FL
        ,C2656_VOID_FL uvdflag --C2656_VOID_FL
        ,
	
        C2656_CREATED_BY ucrby,
        C2656_CREATED_DATE ucrdate
        --,C2656_LAST_UPDATED_BY
        --,C2656_LAST_UPDATED_DATE
        ,
        C5010_RETAG_ID uretagid,
        C2656_SET_COUNT_FL usetcntfl,
        C106_ADDRESS_ID uaddid ,
        C901_TAG_STATUS utagstat
        --, C2656_FLAGGED_BY
        -- , C2656_FLAGGED_DT
        --, C2656_APPROVED_BY
        --, C2656_APPROVED_DT
        --, C2656_RECONCILED_BY
        --, C2656_RECONCILED_DT
        ,
        C2656_ADDRESS_OTHERS uaddoth
        --,C2656_RETAG_FL
        ,
        C2656_PART_COUNT_FL upartcntfl,C2659_FIELD_SALES_REF_LOCK_ID lockid
      FROM T2656_AUDIT_ENTRY_TEMP
      WHERE C2651_FIELD_SALES_DETAILS_ID = p_FIELD_SALES_DETAILS_ID;


BEGIN

 FOR uploaded_tag IN uploaded_tag_cur
  LOOP

     --if fsdetid_all not equal param error
    IF NVL (uploaded_tag.ufsdetid, '-9999') <> p_FIELD_SALES_DETAILS_ID THEN
      GM_RAISE_APPLICATION_ERROR('-20999','99','');
    END IF;

  /*Commented this code b'coz the Field Audit app does not hold this validation.
  --IF entry type is audited entry and lock id is not null then throw error
    IF uploaded_tag.uentrytype = 51030 AND  uploaded_tag.lockid IS NOT NULL THEN
      raise_application_error ('-20500', 'Entry Type is should be blank for Tag: '|| uploaded_tag.utagid) ;
    END IF;

 --IF entry type is manual entry and lock id is null then throw error
    IF uploaded_tag.uentrytype = 51031 AND  uploaded_tag.lockid IS NULL THEN
      raise_application_error ('-20500', 'Entry Type is missing for Tag: '|| uploaded_tag.utagid) ;
    END IF;*/
   
   
    --Tag quantity should always be 1.
    IF uploaded_tag.uqty <> 1 THEN
      GM_RAISE_APPLICATION_ERROR('-20999','100','');
    END IF;
    --
   
    --IF entry type not Audited or manual
    IF NVl(uploaded_tag.uentrytype,0) <>51030 AND NVl(uploaded_tag.uentrytype ,0) <>51031 THEN
      GM_RAISE_APPLICATION_ERROR('-20999','144',uploaded_tag.uentrytype);
    END IF;
    
   gm_sav_audit_entry ('', uploaded_tag.ufsdetid, uploaded_tag.utagid, uploaded_tag.upartid, uploaded_tag.ucntno, uploaded_tag.usetid, uploaded_tag.uqty, 
	 uploaded_tag.ustat, uploaded_tag.ucomm, uploaded_tag.uloctype, uploaded_tag.ulocid, uploaded_tag.ureftype, uploaded_tag.urefid, uploaded_tag.uentrytype, uploaded_tag.ucntby
	 , uploaded_tag.ucntdate, uploaded_tag.ufsreflockid,uploaded_tag.uvdflag, uploaded_tag.ucrby, uploaded_tag.ucrdate, uploaded_tag.uretagid, uploaded_tag.usetcntfl, uploaded_tag.uaddid
	 , uploaded_tag.utagstat, uploaded_tag.uaddoth, uploaded_tag.upartcntfl, p_user_id) ;
	  
  END LOOP;
END gm_sav_audit_data;


/*****************************************************************************
* Description : Procedure to update the tag status for all missing ,extra tags,matching tags.Will always contain overall validation for all the tags updated.
*This procedure should also handle scenerio : same field sales which is partially count on 2 diff. laptops and is being uploaded
* Author: Mihir Patel
*****************************************************************************/
PROCEDURE gm_sav_audit_tag_status(
    p_FIELD_SALES_DETAILS_ID IN t2656_audit_entry.C2651_FIELD_SALES_DETAILS_ID%TYPE,
    p_auditid                IN t2650_field_sales_audit.c2650_field_sales_audit_id%TYPE,
    p_user_id                IN t101_user.c101_user_id%TYPE)
AS


  --master cursor to differentiate deviation tags
  CURSOR temp_tag_cur
  IS
    SELECT tmptag.ttagid,
      tmptag.tfsreflockid,
      lockedtag.ltagid ,
      lockedtag.lfsreflockid
      --
	 ,DECODE (NVL(tmptag.tentrytype,18543),51030,DECODE(tmptag.ttagid,lockedtag.ltagid,18541,DECODE(tmptag.tentrytype,51030,18542,18542)),18543)    tagstat
	 ,NVL(tmptag.tentrytype,51031) entrytype
	 ,DECODE(NVL(tmptag.tentrytype,51031),51031,lockedtag.lfsreflockid) lockid
     ,p_FIELD_SALES_DETAILS_ID fsdetid
      --
      ,
      NVL (tmptag.tfsdetid, lockedtag.lfsdetid) fsdetid_all --for nedg. dev tags id will be in lock column ,
      -- should always match the
      -- above ID passed as parameter
      ,
      NVL (tmptag.ttagid, lockedtag.ltagid) tag_all,
      NVL (tmptag.tpartid, lockedtag.lpnum) pnum_all,
      NVL ( tmptag.tcntno, lockedtag.lcntno) cntno_all,
      NVL (tmptag.tsetid, lockedtag.lsetid) setid_all,
      NVL ( tmptag.tqty, lockedtag.lqty) qty_all
      --
      ,
      tmptag.tstat,
      tmptag.tcomm,
      tmptag.tloctype ,
      tmptag.tlocid,
      tmptag.treftype,
      tmptag.trefid
      --
      ,
      tmptag.tcntby,
      tmptag.tcntdate,
      tmptag.tcrby ,
      tmptag.tcrdate,
      tmptag.tretagid,
      tmptag.tsetcntfl ,
      tmptag.taddid,
      tmptag.taddoth,
      tmptag.tpartcntfl,
      tmptag.vdflag
      --
    FROM
      (SELECT C2656_AUDIT_ENTRY_ID tauditentid,
        C2651_FIELD_SALES_DETAILS_ID tfsdetid,
        C5010_TAG_ID ttagid ,
        C205_PART_NUMBER_ID tpartid,
        C2656_CONTROL_NUMBER tcntno,
        C207_SET_ID tsetid ,
        1 tqty --C2656_QTY
        ,
        C901_STATUS tstat,
        C2656_COMMENTS tcomm,
        C901_LOCATION_TYPE tloctype ,
        C2656_LOCATION_ID tlocid,
        C901_REF_TYPE treftype,
        C2656_REF_ID trefid
        --,''  refdet --C2656_REF_DETAILS
        ,
        C901_ENTRY_TYPE tentrytype
        -- , '' recstat --C901_RECONCILIATION_STATUS
        ,
        C2656_COUNTED_BY tcntby,
        C2656_COUNTED_DATE tcntdate,
        C2659_FIELD_SALES_REF_LOCK_ID tfsreflockid
        -- ,'' updlist --C903_UPLOAD_FILE_LIST
        --,'' histflag --C2656_HISTORY_FL
        ,C2656_VOID_FL vdflag --C2656_VOID_FL
        ,
        C2656_CREATED_BY tcrby,
        C2656_CREATED_DATE tcrdate
        --,C2656_LAST_UPDATED_BY
        --,C2656_LAST_UPDATED_DATE
        ,
        C5010_RETAG_ID tretagid,
        C2656_SET_COUNT_FL tsetcntfl,
        C106_ADDRESS_ID taddid ,
        C901_TAG_STATUS ttagstat
        --, C2656_FLAGGED_BY
        -- , C2656_FLAGGED_DT
        --, C2656_APPROVED_BY
        --, C2656_APPROVED_DT
        --, C2656_RECONCILED_BY
        --, C2656_RECONCILED_DT
        ,
        C2656_ADDRESS_OTHERS taddoth
        --,C2656_RETAG_FL
        ,
        C2656_PART_COUNT_FL tpartcntfl
	from T2656_AUDIT_ENTRY WHERE C2651_FIELD_SALES_DETAILS_ID = p_FIELD_SALES_DETAILS_ID 
      ) tmptag
  FULL OUTER JOIN
    (SELECT t2659.C5010_TAG_ID ltagid,
      t2659.C2659_FIELD_SALES_REF_LOCK_ID lfsreflockid,
      t2659.C205_PART_NUMBER_ID lpnum,
      t2651.C2651_FIELD_SALES_DETAILS_ID lfsdetid,
      t2659.C2659_QTY lqty,
      t2659.C2659_CONTROL_NUMBER lcntno,
      t2659.C207_SET_ID lsetid
    FROM t2650_field_sales_audit t2650,
      t2652_field_sales_lock t2652,
      t2651_field_sales_details t2651,
      t2659_field_sales_ref_lock t2659
    WHERE t2650.C2650_FIELD_SALES_AUDIT_ID = t2652.C2650_FIELD_SALES_AUDIT_ID
    AND t2652.C2652_FIELD_SALES_LOCK_ID    = t2659.C2652_FIELD_SALES_LOCK_ID
    AND t2650.C2650_FIELD_SALES_AUDIT_ID   = t2651.c2650_field_sales_audit_id
    AND t2651.C701_DISTRIBUTOR_ID          = t2652.C701_DISTRIBUTOR_ID
    AND t2650.C2650_FIELD_SALES_AUDIT_ID   = p_auditid
    AND t2651.C2651_FIELD_SALES_DETAILS_ID = p_FIELD_SALES_DETAILS_ID
    AND t2650.C2650_VOID_FL               IS NULL
    AND t2652.C2652_VOID_FL               IS NULL
    AND t2659.C2659_VOID_FL               IS NULL
    ) lockedtag
  ON (tmptag.ttagid = lockedtag.ltagid) ;
  --Fetch all the records that needs to be retagged.
  CURSOR retag_tag_cur
  IS
    SELECT
      c2656_audit_entry_id auditentid ,
      NVL (C2656_LAST_UPDATED_BY, C2656_CREATED_BY) userid
    FROM t2656_audit_entry
    WHERE c2656_void_fl IS NULL
    AND c2656_retag_fl  IS NULL
    AND c5010_retag_id  IS NOT NULL;
BEGIN
  FOR temp_tag IN temp_tag_cur
  LOOP
    --if fsdetid_all not equal param error
    IF NVL (temp_tag.fsdetid_all, '-9999') <> p_FIELD_SALES_DETAILS_ID THEN
      GM_RAISE_APPLICATION_ERROR('-20999','99','');
    END IF;
    --IF tag status is null
    IF NVL(temp_tag.tagstat,0) = 0 THEN
      GM_RAISE_APPLICATION_ERROR('-20999','101',temp_tag.tag_all);
    END IF;
    --IF entry type is null
    IF NVl(temp_tag.entrytype ,0) = 0 THEN
      GM_RAISE_APPLICATION_ERROR('-20999','102',temp_tag.tag_all);
    END IF;
    --
     --IF entry type not Audited or manual
    IF NVl(temp_tag.entrytype ,0) <>51030 AND NVl(temp_tag.entrytype ,0) <>51031 THEN
      GM_RAISE_APPLICATION_ERROR('-20999','103',temp_tag.tag_all);
    END IF;
    --IF part or tag is null
    IF temp_tag.pnum_all IS NULL OR temp_tag.tag_all IS NULL THEN
      GM_RAISE_APPLICATION_ERROR('-20999','104','');
    END IF;
    --IF  tag status is  Negative deviation and entry type is not Manual
    IF NVL(temp_tag.tagstat,0) = 18543 AND NVL(temp_tag.entrytype,0) <> 51031 THEN
      GM_RAISE_APPLICATION_ERROR('-20999','105',temp_tag.tag_all);
    END IF;
    --IF lock detail id is not null and tag status is not Negative deviation
    IF temp_tag.lockid IS NOT NULL AND NVL(temp_tag.tagstat,0) <> 18543 THEN
      GM_RAISE_APPLICATION_ERROR('-20999','145','');
    END IF;
    --Tag quantity should always be 1.
    IF temp_tag.qty_all <> 1 THEN
      GM_RAISE_APPLICATION_ERROR('-20999','106','');
    END IF;
    --
    gm_sav_audit_entry ('', temp_tag.fsdetid_all, temp_tag.tag_all, temp_tag.pnum_all, temp_tag.cntno_all, temp_tag.setid_all, temp_tag.qty_all, temp_tag.tstat, temp_tag.tcomm, temp_tag.tloctype, temp_tag.tlocid, temp_tag.treftype, temp_tag.trefid, temp_tag.entrytype, temp_tag.tcntby, temp_tag.tcntdate, temp_tag.lockid,temp_tag.vdflag, temp_tag.tcrby, temp_tag.tcrdate, temp_tag.tretagid, temp_tag.tsetcntfl, temp_tag.taddid, temp_tag.tagstat, temp_tag.taddoth, temp_tag.tpartcntfl, p_user_id) ;
    --v_cnt := v_cnt + 1;
    dbms_output.put_line (temp_tag.tag_all) ;
  END LOOP;
  --Retag all the tag marked as retag.
  FOR retag_tag IN retag_tag_cur
  LOOP
    gm_pkg_ac_fa_flag_deviation.gm_sav_retag (retag_tag.auditentid, retag_tag.userid) ;
  END LOOP;
END gm_sav_audit_tag_status;
/*****************************************************************************
* Description : Procedure to insert in Audit entry table
* Author: Mihir Patel
*****************************************************************************/
PROCEDURE gm_sav_audit_entry(
    p_audit_ent_id            IN t2656_audit_entry.C2656_AUDIT_ENTRY_ID%TYPE,
    p_FIELD_SALES_DETAILS_ID  IN t2656_audit_entry.C2651_FIELD_SALES_DETAILS_ID%TYPE,
    p_tag_id                  IN t2656_audit_entry.C5010_TAG_ID%TYPE,
    p_pnum                    IN t2656_audit_entry.C205_PART_NUMBER_ID%TYPE,
    p_cnt_no                  IN t2656_audit_entry.C2656_CONTROL_NUMBER%TYPE,
    p_set_id                  IN t2656_audit_entry.C207_SET_ID%TYPE,
    p_qty                     IN t2656_audit_entry.C2656_QTY%TYPE,
    p_status                  IN t2656_audit_entry.C901_STATUS%TYPE,
    p_comment                 IN t2656_audit_entry.C2656_COMMENTS%TYPE,
    p_loc_type                IN t2656_audit_entry.C901_LOCATION_TYPE%TYPE,
    p_location_id             IN t2656_audit_entry.C2656_LOCATION_ID%TYPE,
    p_ref_type                IN t2656_audit_entry.C901_REF_TYPE%TYPE,
    p_ref_id                  IN t2656_audit_entry.C2656_REF_ID%TYPE,
    p_entry_type              IN t2656_audit_entry.C901_ENTRY_TYPE%TYPE,
    p_counted_by              IN t2656_audit_entry.C2656_COUNTED_BY%TYPE,
    p_counted_date            IN t2656_audit_entry.C2656_COUNTED_DATE%TYPE,
    p_FIELD_SALES_REF_LOCK_ID IN t2656_audit_entry.C2659_FIELD_SALES_REF_LOCK_ID%TYPE,
    p_void_fl                 IN t2656_audit_entry.C2656_VOID_FL%TYPE,
    p_created_by              IN t2656_audit_entry.C2656_CREATED_BY%TYPE,
    p_created_date            IN t2656_audit_entry.C2656_CREATED_DATE%TYPE,
    p_retag_id                IN t2656_audit_entry.C5010_RETAG_ID%TYPE,
    p_setcnt_fl               IN t2656_audit_entry.C2656_SET_COUNT_FL%TYPE,
    p_add_id                  IN t2656_audit_entry.C106_ADDRESS_ID%TYPE,
    p_tag_status              IN t2656_audit_entry.C901_TAG_STATUS%TYPE,
    p_add_others              IN t2656_audit_entry.C2656_ADDRESS_OTHERS%TYPE,
    p_partcnt_fl              IN t2656_audit_entry.C2656_PART_COUNT_FL%TYPE,
    p_user_id                 IN t101_user.c101_user_id%TYPE)
AS
BEGIN
  UPDATE T2656_AUDIT_ENTRY
  SET C205_PART_NUMBER_ID         = p_pnum,
    C2656_CONTROL_NUMBER          = p_cnt_no,
    C207_SET_ID                   = p_set_id ,
    C2656_QTY                     = p_qty,
    C901_STATUS                   = p_status,
    C2656_COMMENTS                = p_comment ,
    C901_LOCATION_TYPE            = p_loc_type,
    C2656_LOCATION_ID             = p_location_id,
    C901_REF_TYPE                 = p_ref_type ,
    C2656_REF_ID                  = p_ref_id,
    C901_ENTRY_TYPE               = p_entry_type,
    C2656_COUNTED_BY              = p_counted_by ,
    C2656_COUNTED_DATE            = p_counted_date,
    C2659_FIELD_SALES_REF_LOCK_ID = p_FIELD_SALES_REF_LOCK_ID,
    C2656_VOID_FL = p_void_fl,
    --, C2656_CREATED_BY                 = p_created_by
    --, C2656_CREATED_DATE = p_created_date
    C2656_LAST_UPDATED_BY   = p_user_id,
    C2656_LAST_UPDATED_DATE = SYSDATE,
    C5010_RETAG_ID = DECODE (c2656_retag_fl, NULL, p_retag_id, c5010_retag_id), --We can retag only once ,If the retag is changed and uploaded again then the retag is not changed in entry table.
    C2656_SET_COUNT_FL             = p_setcnt_fl,
    C106_ADDRESS_ID                = p_add_id,
    C901_TAG_STATUS                = p_tag_status ,
    C2656_ADDRESS_OTHERS           = p_add_others,
    C2656_PART_COUNT_FL            = p_partcnt_fl,
    C2656_REF_DETAILS              = '' ,
    C901_RECONCILIATION_STATUS     = '',
    C2656_FLAGGED_BY               = '',
    C2656_FLAGGED_DT               = '' ,
    C2656_APPROVED_BY              = '',
    C2656_APPROVED_DT              = '',
    C901_POSTING_OPTION = '',
    C2656_POSTING_COMMENT	 = '',
	C2656_EMAIL_FL	 = '',
	C901_EMAIL_TYPE	 = ''
  WHERE C5010_TAG_ID               = p_tag_id
  AND C2651_FIELD_SALES_DETAILS_ID = p_FIELD_SALES_DETAILS_ID;
  --
  IF SQL%ROWCOUNT = 0 THEN
    INSERT
    INTO T2656_AUDIT_ENTRY
      (
        C2656_AUDIT_ENTRY_ID,
        C2651_FIELD_SALES_DETAILS_ID,
        C5010_TAG_ID ,
        C205_PART_NUMBER_ID,
        C2656_CONTROL_NUMBER,
        C207_SET_ID ,
        C2656_QTY,
        C901_STATUS,
        C2656_COMMENTS ,
        C901_LOCATION_TYPE,
        C2656_LOCATION_ID,
        C901_REF_TYPE ,
        C2656_REF_ID,
        C901_ENTRY_TYPE,
        C2656_COUNTED_BY ,
        C2656_COUNTED_DATE,
        C2659_FIELD_SALES_REF_LOCK_ID,
	    C2656_VOID_FL,
        C2656_CREATED_BY ,
        C2656_CREATED_DATE,
        C5010_RETAG_ID,
        C2656_SET_COUNT_FL ,
        C106_ADDRESS_ID,
        C901_TAG_STATUS,
        C2656_ADDRESS_OTHERS ,
        C2656_PART_COUNT_FL
      )
      VALUES
      (
        s2656_audit_entry.NEXTVAL,
        p_FIELD_SALES_DETAILS_ID,
        p_tag_id ,
        p_pnum,
        p_cnt_no,
        p_set_id ,
        p_qty,
        p_status,
        p_comment ,
        p_loc_type,
        p_location_id,
        p_ref_type ,
        p_ref_id,
        p_entry_type,
        p_counted_by ,
        p_counted_date,
        p_FIELD_SALES_REF_LOCK_ID,
     	p_void_fl,
        p_user_id ,
        SYSDATE,
        p_retag_id,
        p_setcnt_fl ,
        p_add_id,
        p_tag_status,
        p_add_others ,
        p_partcnt_fl
      ) ;
  END IF;
END gm_sav_audit_entry;
END gm_pkg_ac_fa_app_upload_data;
/