--@"C:\Database\Packages\Accounting\gm_pkg_ar_credit_hold.bdy";
CREATE OR REPLACE
PACKAGE BODY gm_pkg_ar_credit_hold
IS
/*********************************************************
* Description : This procedure is used to fetch credit hold 
*               type and sub type mapping details.
*********************************************************/
PROCEDURE gm_fch_credit_hold_dtl (
        p_credit_type    IN T704C_ACC_CREDIT_INFO.C901_TYPE%TYPE,
        p_credit_subtype IN T704C_ACC_CREDIT_INFO.C901_SUB_TYPE%TYPE,
        p_out_cur_dtls OUT TYPES.cursor_type)
AS
BEGIN
    OPEN p_out_cur_dtls FOR
     SELECT C901_TYPE CREDITTYPE, GET_CODE_NAME (C901_TYPE) CREDITTYPENM, C901_SUB_TYPE CREDITSUBTYPE
      , GET_CODE_NAME (C901_SUB_TYPE) CREDITSUBTYPENM, C704C_value CREDITVALUE, GET_CODE_NAME(C704C_value) CREDITVALUENM, C704C_message MESSAGE
       FROM T704C_ACC_CREDIT_INFO
      WHERE C901_TYPE      = p_credit_type
        AND C901_SUB_TYPE  = NVL (p_credit_subtype, C901_SUB_TYPE)
        AND C704C_VOID_FL IS NULL;
END gm_fch_credit_hold_dtl;
/*********************************************************
* Description : This procedure is used to fetch Account credit hold 
*               limit and type detail from account attribute table.
*********************************************************/
PROCEDURE gm_fch_acct_credit_hold (
        p_account_id     IN t704_account.C704_ACCOUNT_ID%TYPE,
        p_credittype_str IN VARCHAR2,
        p_out_cur_dtls  OUT TYPES.cursor_type)
AS
v_datefmt VARCHAR2(50);
v_company_id        t1900_company.c1900_company_id%TYPE;
BEGIN
	
	SELECT get_compid_frm_cntx()
	  INTO v_company_id
	  FROM dual;
	
	SELECT get_compdtfmt_frm_cntx()
	into v_datefmt from dual;
	
	my_context.set_my_inlist_ctx (p_credittype_str) ;
    OPEN p_out_cur_dtls FOR
		SELECT t704.C704_ACCOUNT_ID ACCTID, T704.C703_SALES_REP_ID REPID, t704.C704_ACCOUNT_NM ACCTNM
		     , GET_CODE_NAME (T704A.CREDITTYPE) CREDITTYPENM, NVL(get_acct_total_outstd(t704.C704_ACCOUNT_ID),'0') OUTSTD
		     --Calling get_user_name func. instead of joining table. Because, the t704a is an outer join with t704. 
		     , GET_USER_NAME(T704A.CREATEDBYID) CREATEDBY,T701.C701_DISTRIBUTOR_NAME DISTNAME
		     , NVL (get_account_curr_symb(NVL(p_account_id,t704a.C704_ACCOUNT_ID)),GET_RULE_VALUE('CURRSYMBOL','CURRSYMBOL')) ACCCURRENCY , T704A.*
		   FROM t704_account t704,t701_distributor t701,t703_sales_rep t703 , (
		         SELECT t704a.C704_ACCOUNT_ID, MAX (DECODE (C901_ATTRIBUTE_TYPE, '101182', C704A_ATTRIBUTE_VALUE)) CREDITLIMIT,
		            MAX (DECODE (C901_ATTRIBUTE_TYPE, '101184', C704A_ATTRIBUTE_VALUE)) CREDITTYPE
		            , MAX (DECODE (C901_ATTRIBUTE_TYPE, '101182', C704A_HISTORY_FL, '')) CREDITLIMITHISTFL
		            , MAX (DECODE (c901_attribute_type, '101184', c704a_history_fl, '')) CREDITTYPEHISTFL
		            , MAX (DECODE (c901_attribute_type, '101184', NVL (C704A_LAST_UPDATED_BY, C704A_CREATED_BY), '')) CREATEDBYID
		            , MAX (DECODE (c901_attribute_type, '101184', TO_char(NVL(C704A_LAST_UPDATED_DATE, C704A_CREATED_DATE), v_datefmt), '')) CREATEDDT
		           FROM T704A_ACCOUNT_ATTRIBUTE T704A,T704_ACCOUNT T704
		          WHERE t704a.C704_ACCOUNT_ID = NVL(p_account_id, t704a.C704_ACCOUNT_ID)
		            AND C704A_VOID_FL        IS NULL
		            AND C901_ATTRIBUTE_TYPE    IN (101182, 101184)
		            AND T704.C704_ACCOUNT_ID    = T704A.C704_ACCOUNT_ID
		            AND T704.C704_VOID_FL      IS NULL
  					AND T704.c1900_company_id   = v_company_id
		       GROUP BY T704A.C704_ACCOUNT_ID
		    )
		    T704A
		  WHERE T704.C704_ACCOUNT_ID  = T704A.C704_ACCOUNT_ID
		    AND T704.C704_ACCOUNT_ID = NVL (p_account_id, T704.C704_ACCOUNT_ID)
		    AND T703.C701_DISTRIBUTOR_ID = T701.C701_DISTRIBUTOR_ID
			AND T704.C703_SALES_REP_ID   = T703.C703_SALES_REP_ID
		    --query sould fetch account credit hold info and total outstanding.
		    -- eventhough account is not yet set any credit type
		    --4000844 -None.
		    AND T704A.CREDITTYPE IN (SELECT token FROM v_in_list WHERE token IS NOT NULL)
		    AND T704.C704_VOID_FL IS NULL
		    AND T704.C1900_COMPANY_ID    = v_company_id
		ORDER BY REPID,UPPER(ACCTNM);
		--there is a logic with order by repid in GmCreditHoldBean.sendCreditHoldWeeklyEmail
END gm_fch_acct_credit_hold;
 /*********************************************************
	* Description : This procedure to fetch account last payment info
	* 
	*********************************************************/
  PROCEDURE gm_fch_acct_last_payment_info(
  p_account_id IN t704_account.C704_ACCOUNT_ID%TYPE, 
  p_out_cur_dtls OUT TYPES.cursor_type)
  AS
  v_datefmt VARCHAR2(50);
  BEGIN
	  
	    v_datefmt := GET_RULE_VALUE ('DATEFMT', 'DATEFORMAT');
	  
  OPEN p_out_cur_dtls FOR
  /*
   * Below subquery getting payment info from t508 with descending order of received date.
   * main query is getting only top most payment info (last payment detail).
   */
         SELECT acctid, invid, paydt, payamt, acccurrency                              
           FROM
            (
                 SELECT T503.C704_ACCOUNT_ID acctid, T503.C503_INVOICE_ID invid, TO_CHAR(T508.C508_RECEIVED_DATE,v_datefmt) paydt
                  , T508.C508_PAYMENT_AMT payamt, NVL (get_account_curr_symb(p_account_id),GET_RULE_VALUE('CURRSYMBOL','CURRSYMBOL')) acccurrency
                   FROM T508_RECEIVABLES T508, T503_INVOICE T503
                  WHERE T503.C503_VOID_FL        IS NULL
                    AND T503.C503_INVOICE_ID      = T508.C503_INVOICE_ID
                    AND T503.C901_EXT_COUNTRY_ID IS NULL
                    AND t503.C704_ACCOUNT_ID      = p_account_id
                    AND T508.C508_RECEIVED_DATE  IS NOT NULL
               ORDER BY T508.C508_RECEIVED_DATE DESC
            )
          WHERE ROWNUM = 1;

 END gm_fch_acct_last_payment_info;
/*********************************************************
* Description : This function is used to account total outstanding amount
*               To getting Account based Ouststanding amount. Not applicable for ICT (type)
* Last Modified: Manikandan
* 				PMT-21779: DB Performance issue fix (to avoid the each invoice function call). 
*********************************************************/
FUNCTION get_acct_total_outstd (
        p_account_id        IN t704_account.C704_ACCOUNT_ID%TYPE
        )
    RETURN NUMBER
IS
    v_total NUMBER (15, 2) ;
    --

BEGIN
	
    -- to get the total amount PMT-53060
       SELECT SUM(NVL(V503_INV_AMT,0) - NVL(V503_INV_PAID,0))  INTO v_total
       FROM V503_INVOICE_REPORTS
       WHERE V503_ACCOUNT_ID = p_account_id
       AND  NVL(V503_INV_AMT,0) - NVL(V503_INV_PAID,0) <> 0
       AND  V503_STATUS_FL < 2;   --Invoice status is closed 

     
    RETURN v_total;
END get_acct_total_outstd; 

END gm_pkg_ar_credit_hold;
/