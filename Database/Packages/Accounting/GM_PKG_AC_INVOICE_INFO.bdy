	/* author  : ppandiyan
	 * Description : This function is to return whether the Tax is based on GST or not
	 Parameters 	: p_ref_id, p_type
	*/
CREATE OR REPLACE PACKAGE BODY GM_PKG_AC_INVOICE_INFO
IS
--

	FUNCTION GET_GST_START_FLAG(
		  p_ref_id 	t501_order.c501_order_id%TYPE
		, p_type	t901_code_lookup.c901_code_id%TYPE
	)
		RETURN t906_rules.c906_rule_value%TYPE
	IS

	v_ref_date		t501_order.c501_order_date%TYPE;
	v_gst_start_fl	t906_rules.c906_rule_value%TYPE;
	v_gst_start_dt	t906_rules.c906_rule_value%TYPE;
	 v_company_id  t1900_company.c1900_company_id%TYPE;

	
	BEGIN
		--getting the company id
		select get_compid_frm_cntx()
		into v_company_id
		from dual;
	
		BEGIN
			--1200: Order, 1201: Invoice, 1221: Consignment(getting the transaction created date)
			IF p_type = 1200
			THEN
				SELECT t501.c501_order_date INTO v_ref_date
					FROM t501_order t501
				WHERE t501.c501_order_id = p_ref_id
				AND t501.c1900_company_id = v_company_id
				AND t501.c501_void_fl IS NULL;
			ELSIF p_type = 1201
			THEN
				SELECT t503.c503_invoice_date INTO v_ref_date
					FROM t503_invoice t503
				WHERE t503.c503_invoice_id = p_ref_id
				AND t503.c1900_company_id = v_company_id
				AND t503.c503_void_fl IS NULL;
			ELSIF p_type = 1221
			THEN
				SELECT NVL(t504.c504_ship_date,t504.c504_created_date) INTO v_ref_date
					FROM t504_consignment t504
				WHERE t504.c504_consignment_id = p_ref_id
				AND t504.c1900_company_id = v_company_id
				AND t504.c504_void_fl IS NULL;
			END IF;
		EXCEPTION
			 WHEN NO_DATA_FOUND
			 THEN
			 v_ref_date := NULL;
		 END;
	 
		v_gst_start_dt := get_rule_value_by_company('GST_DATE','INV_FMT',v_company_id);
		-- Get the flag based for the order/invoice/consignment date by comparing the date
		 BEGIN
			SELECT
				CASE
					WHEN TRUNC (TO_DATE (v_gst_start_dt, get_company_dtfmt(v_company_id))) <= TRUNC (v_ref_date)
				    THEN 'Y'
				    ELSE 'N'
				 END GST_START_DATE_FL
				 INTO v_gst_start_fl
			FROM DUAL;
		 EXCEPTION
			 WHEN NO_DATA_FOUND
			 THEN
			 RETURN 'N';
		 END;
	 
	 RETURN v_gst_start_fl;
	END GET_GST_START_FLAG;
 
	/*******************************************************
	* Purpose: Function to get the carrier string
	*******************************************************/
	FUNCTION GET_CARRIER_STR(
		  p_invioce_id 	t501_order.c503_invoice_id%TYPE
	)
	RETURN CLOB
	IS
		v_shipcarr		CLOB;
	BEGIN
		BEGIN
      SELECT  GM_PKG_CM_SHIPPING_INFO.get_carrier(C501_ORDER_ID,50180) carrier
		  INTO v_shipcarr
		  FROM t501_order
		  WHERE c503_invoice_id               = p_invioce_id
		  AND C501_STATUS_FL                 IS NOT NULL
		  AND C501_VOID_FL                   IS NULL
		  AND C501_PARENT_ORDER_ID           IS NULL;
       EXCEPTION
			 WHEN NO_DATA_FOUND
			 THEN
			 v_shipcarr := null;
		 END;
		RETURN v_shipcarr;
		  
	END GET_CARRIER_STR;
	
	/*******************************************************
	* Purpose: Function to get the tracking number string
	*******************************************************/
	FUNCTION GET_TRACK_NUM_STR(
		  p_invioce_id 	t501_order.c503_invoice_id%TYPE
	)
	RETURN CLOB
	IS
		v_shiptrack		CLOB;
	BEGIN
		BEGIN
SELECT  GM_PKG_CM_SHIPPING_INFO.get_track_no(t501.C501_ORDER_ID,50180) tacknum
		  INTO v_shiptrack
		  FROM t501_order t501,
		    t907_shipping_info t907
		  WHERE t501.C501_ORDER_ID                 = t907.c907_ref_id
		  AND t501.c503_invoice_id                 = p_invioce_id
		  AND t501.C501_STATUS_FL                 IS NOT NULL
		  AND t501.C501_VOID_FL                   IS NULL
		  AND t907.c907_void_fl                   IS NULL
		  AND T501.C501_PARENT_ORDER_ID           IS NULL
		  AND t907.c907_tracking_number           IS NOT NULL;
      
		  EXCEPTION
			 WHEN NO_DATA_FOUND
			 THEN
			 v_shiptrack := null;
		 END;
		RETURN v_shiptrack;
		  
	END GET_TRACK_NUM_STR;
	
	/******************************************************************
	* Purpose: Procedure to get the tracking number and carrier string
	*******************************************************************/
	PROCEDURE GM_FCH_SHIP_DTLS(
		  p_invioce_id	IN 		t501_order.c503_invoice_id%TYPE
		, p_out_carr	OUT		CLOB
		, p_out_track	OUT		CLOB
	)
	AS
	BEGIN
		
		p_out_carr:= gm_pkg_ac_invoice_info.get_carrier_str(p_invioce_id);
		p_out_track:= gm_pkg_ac_invoice_info.get_track_num_str(p_invioce_id);
		
	END GM_FCH_SHIP_DTLS;
	
	/******************************************************************
	* Purpose: Procedure to sync parent order attribute
	*******************************************************************/	
	PROCEDURE gm_sync_ord_attribute(
		  v_parent_order_id 	IN t501_order.c501_order_id%TYPE
		, v_new_order_id	    IN t501_order.c501_order_id%TYPE
		, p_userid		        IN t501_order.c501_created_by%TYPE          
)
AS

	v_ordatt	   VARCHAR2(2000) :='';
	
	BEGIN	
	WITH v_txns AS
		  (SELECT ('^'
		    || C901_ATTRIBUTE_TYPE
		    || '^'
		    || C501A_ATTRIBUTE_VALUE ) trans_id
		    INTO v_ordatt
		  FROM T501a_Order_Attribute
		  WHERE C501_ORDER_ID = v_parent_order_id
		  AND C501A_VOID_FL  IS NULL
		  )
		SELECT MAX (SYS_CONNECT_BY_PATH (trans_id,'|'))
		FROM
		  (SELECT trans_id, rownum curr FROM v_txns
		  )
		  CONNECT BY CURR -1 = PRIOR CURR
		  START WITH curr    = 1; 
							v_ordatt := substr(v_ordatt,2) || '|';
			
					gm_pkg_ac_order.gm_sav_rev_sampling(v_new_order_id,p_userid, v_ordatt ,'Y');
					-- Save the child order PO and DO Details in t5003_order_revenue_sample_dtls For PMT-57180
					gm_pkg_ac_ar_revenue_sampling.gm_sav_child_order_revenue_dtls(v_new_order_id,v_parent_order_id,p_userid);							
 END gm_sync_ord_attribute;	
 
 	/******************************************************************
	* Purpose: Procedure to update hsn code for the order
	*******************************************************************/	
	PROCEDURE gm_upd_hsn_code(
		   p_txn_id 	IN t501_order.c501_order_id%TYPE
		  ,p_type	    IN  t901_code_lookup.c901_code_id%TYPE
		  ,p_user_id 	IN t501_order.c501_last_updated_by%TYPE  
)
AS
	v_company_id    t1900_company.c1900_company_id%TYPE; 
	
	CURSOR orderlog_cur
	IS
     	SELECT c501_order_id orderid
       	  FROM t501_order
      	WHERE c503_invoice_id  = p_txn_id
          	AND c503_invoice_id IS NOT NULL
          	AND c501_void_fl    IS NULL;
	BEGIN
		--getting company id
		SELECT get_compid_frm_cntx()
		  INTO v_company_id
		  FROM dual;

			IF p_type = '1201'  THEN --invoice
				  FOR order_row IN orderlog_cur
					LOOP
					--updating HSN code
						UPDATE t502_item_order
							SET c502_hsn_code   = GET_RULE_VALUE_BY_COMPANY(GET_PARTNUM_PRODUCT(C205_PART_NUMBER_ID),'HSN_GRP',v_company_id)
						WHERE c501_order_id = order_row.orderid
						AND c502_void_fl   IS NULL;
					--updating t501_order while updating item order	
					UPDATE T501_ORDER 
							SET C501_LAST_UPDATED_BY=p_user_id, C501_LAST_UPDATED_DATE=CURRENT_DATE 
						WHERE C501_ORDER_ID=order_row.orderid 
						AND C501_VOID_FL IS NULL; 
					END LOOP;
			ELSIF p_type = '1200' THEN -- orders
			--updating HSN code
					UPDATE t502_item_order
						SET c502_hsn_code   = GET_RULE_VALUE_BY_COMPANY(GET_PARTNUM_PRODUCT(C205_PART_NUMBER_ID),'HSN_GRP',v_company_id)
					WHERE c501_order_id = p_txn_id
					AND c502_void_fl   IS NULL;
				--updating t501_order while updating item order	
					UPDATE T501_ORDER 
							SET C501_LAST_UPDATED_BY=p_user_id, C501_LAST_UPDATED_DATE=CURRENT_DATE 
						WHERE C501_ORDER_ID=p_txn_id 
						AND C501_VOID_FL IS NULL; 
			END IF;
		  
							
 END gm_upd_hsn_code;	
END GM_PKG_AC_INVOICE_INFO;
/

