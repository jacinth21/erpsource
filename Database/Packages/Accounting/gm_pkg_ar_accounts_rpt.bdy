/* Formatted on 2010/11/16 11:54 (Formatter Plus v4.8.0) */
--@"C:\projects\Branches\jboss-migration\Database\Packages\Accounting\gm_pkg_ar_accounts_rpt.bdy";
CREATE OR REPLACE
PACKAGE BODY gm_pkg_ar_accounts_rpt
IS
    /***************************************************************************************************
    *Description : Procedure used to determine the email details subject, message, to, cc email id's
    *              based on the commission or final commission that is selected from the UI.
    ****************************************************************************************************/
PROCEDURE gm_fch_commsn_email (
        p_orderinput_str IN CLOB,
        p_action         IN T901_CODE_LOOKUP.c901_CODE_ID%TYPE,
        p_to_date        IN VARCHAR2,
        p_report_type	 IN VARCHAR2,
        p_txt_duedate	 IN VARCHAR2,
        p_subject   OUT VARCHAR2,
        p_message   OUT VARCHAR2,
        p_cc_email  OUT CLOB,
        p_to_email  OUT CLOB,
        p_rep_name  OUT VARCHAR2,
        p_due_date  OUT VARCHAR2,
        p_fmt_to_date  OUT VARCHAR2,
        p_fmt_due_date OUT VARCHAR2,
        p_fmt_to_month OUT VARCHAR2)
AS
    v_subject      VARCHAR2 (200) ;
    v_message      VARCHAR2 (1000) ;
    v_advp_mailids VARCHAR2 (4000) ;
    v_cc_default   VARCHAR2 (1000) ;
    v_to_date DATE;
    v_orderinput_str VARCHAR2 (4000) ;
    v_company_id 		t1900_company.c1900_company_id%TYPE;
BEGIN
    my_context.set_my_cloblist (p_orderinput_str||',') ;
    
    	SELECT get_compid_frm_cntx()
          INTO v_company_id 
          FROM DUAL;
    
       SELECT to_date (p_to_date, get_compdtfmt_frm_cntx())
       INTO v_to_date
       FROM dual;
       
	--Fetching the To Sales rep email Id's.       
    BEGIN
          SELECT RTRIM (XMLAGG (XMLELEMENT (e, email_id || ',')) .EXTRACT ('//text()'), ',') 
            INTO p_to_email  
            FROM  
                (
                 SELECT DISTINCT GET_PARTY_PRIMARY_CONTACT (T703.C101_PARTY_ID, 90452) email_id
                   FROM T703_SALES_REP T703,T501_ORDER t501, v_clob_list temp
                  WHERE t501.C703_SALES_REP_ID = T703.C703_SALES_REP_ID
                    AND t501.C501_ORDER_ID    = temp.token
                    AND temp.token IS NOT NULL
                    ORDER BY email_id
                );
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        p_to_email := NULL;
    END;
    
    --Fetching the ad, vp_mailids email Id's, based on rule value.
    --If this is defined as NO, then AD/VP will not be added in CC list. 
    BEGIN
          SELECT RTRIM (XMLAGG (XMLELEMENT (e, email_id || ',')) .EXTRACT ('//text()'), ',') 
            INTO v_advp_mailids  
            FROM  
                    (
                     SELECT DISTINCT DECODE (get_rule_value_by_company ('COPY_AD', 'COMMSN_EMAIL',v_company_id), 'YES', GET_USER_EMAILID (V700.AD_ID), '') ||
				            ','|| DECODE (get_rule_value_by_company ('COPY_VP', 'COMMSN_EMAIL',v_company_id), 'YES', GET_USER_EMAILID (V700.VP_ID), '') email_id
				       FROM V700_TERRITORY_MAPPING_DETAIL V700, T501_ORDER t501, v_clob_list temp
				      WHERE t501.C704_ACCOUNT_ID   = V700.AC_ID 
				        AND t501.C501_ORDER_ID     = temp.token
				        AND DECODE(p_action,'103281',1,'103282',1,0) = 1  --For montly(103281), Final(103282) Emails we need the this query to be fired.
				        AND temp.token IS NOT NULL
				        ORDER BY email_id
                    );
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_advp_mailids := NULL;
    END;
    --Fetching the defult mailids email Id's to be CC'd, which is defined in rules.
     --SELECT get_rule_value ('COPY', 'COMMSN_EMAIL') INTO v_cc_default FROM dual;     
     SELECT DECODE(p_action, '4000627', get_rule_value_by_company ('HELD_ORD_COPY', 'COMMSN_EMAIL',v_company_id), get_rule_value_by_company ('COPY', 'COMMSN_EMAIL',v_company_id)) --4000627 - Held Order Commission E-mail
     INTO v_cc_default FROM dual;

   
    --Fetching the rep name. when two or more reps are selected then that time need to take the distributor name. 
    IF p_report_type = '103284' THEN
         SELECT DISTINCT C101_USER_F_NAME || ' ' || C101_USER_L_NAME NAME
           INTO p_rep_name
           FROM T501_ORDER T501, T101_USER T101, v_clob_list temp
          WHERE T501.C703_SALES_REP_ID = T101.C101_USER_ID
            AND t501.C501_ORDER_ID     = temp.token
            AND p_report_type = '103284'   -- 103284- By Rep
            AND temp.token IS NOT NULL;
	END IF;
    IF p_report_type = '103285' THEN
            SELECT DISTINCT (t701.c701_distributor_name)
               INTO p_rep_name
               FROM t501_order t501, t701_distributor t701, t703_sales_rep t703
              WHERE t501.c703_sales_rep_id   = t703.c703_sales_rep_id
                AND t703.c701_distributor_id = t701.c701_distributor_id
                AND t501.c501_order_id  IN (	SELECT token FROM v_clob_list WHERE token IS NOT NULL    ) 
                AND p_report_type = '103285';   -- 103285- By Distributor
       
    END IF;
     --Fetching the default day count to add it with To date.
     SELECT NVL(p_txt_duedate ,  get_rule_value('DEFAULT_DAYS','COMMSN_EMAIL')) -- To get Due date added with To_date.
       INTO p_due_date
       FROM dual;
     --Fetching the subject, message from rules.
     SELECT get_rule_value (p_action, 'COMM_EMAIL_SUB'), get_rule_value (p_action, 'COMM_EMAIL_MSG')
       INTO v_subject, v_message
       FROM dual;

     --Fetching the due date, to month, to date in specific formats based on the input To date.  
     SELECT TO_CHAR (v_to_date, 'Month DD'), TO_CHAR (v_to_date + p_due_date, 'Day') ||','|| TO_CHAR (v_to_date +
        p_due_date, 'Month DD'), TO_CHAR (v_to_date, 'Month')
       INTO p_fmt_to_date, p_fmt_due_date, p_fmt_to_month
       FROM dual;
       
     --Replacing the subject with rep/dist name  .  
     SELECT REPLACE (REPLACE (v_subject, DECODE (p_rep_name, NULL, '-#@#NAME', '#@#NAME'), p_rep_name), '#@#MONTH',
        TRIM (UPPER (p_fmt_to_month)))
       INTO p_subject
       FROM DUAL;
     
       --Replacing the p_message with due date, to month, to date  .
     SELECT REPLACE (REPLACE (REPLACE (v_message, '#@#TODATE', p_fmt_to_date), '#@#DUEDATE', (p_fmt_due_date)),
        '#@#TOMONTH', p_fmt_to_month)
       INTO p_message
       FROM DUAL;
       
       --All the AD, VP, default mail ids are appended.  
    p_cc_email := v_advp_mailids ||','|| v_cc_default ;
END gm_fch_commsn_email;

/**********************************************************************
*Description : Procedure used to determine the CC email IDs details .
***********************************************************************/
PROCEDURE gm_fch_collector_email (
        p_orderinput_str IN CLOB,
        p_collector_email OUT CLOB)
AS
BEGIN
    my_context.set_my_cloblist (p_orderinput_str||',') ;
    
    BEGIN
	    --Fetching the Collector ID email details. 
          SELECT RTRIM (XMLAGG (XMLELEMENT (e, email_id || ',')) .EXTRACT ('//text()'), ',') 
            INTO p_collector_email  
            FROM  
                (
                     SELECT DISTINCT (t101.c101_email_id) email_id
					   FROM t501_order t501, t704a_account_attribute t704a, t101_user t101
					  , t906_rules t906, v_clob_list temp
					  WHERE t501.c704_account_id        = t704a.c704_account_id
					    AND t704a.C704A_ATTRIBUTE_VALUE = t906.c906_rule_id
					    AND t906.c906_rule_value        = TO_CHAR (t101.c101_user_id)
					    AND t704a.c901_attribute_type   = '103050' ---Collector ID attribute type
					    AND c906_rule_grp_id            = 'COLLECTOR_ID' ---Collector ID rule grp
					    AND t501.C501_ORDER_ID          = temp.token
					    AND temp.token                 IS NOT NULL
					    AND t501.c501_void_fl          IS NULL
					    AND t704a.c704a_void_fl        IS NULL
					    AND t906.c906_void_fl          IS NULL
					    AND C901_USER_STATUS            = 311  ---ACTIVE STATUS USERS
					    ORDER BY email_id
                );
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        p_collector_email := NULL;
    END;    
END gm_fch_collector_email;

/**********************************************************************
*Description : function is used to get collector id for the account
***********************************************************************/
FUNCTION get_collector_id(
        p_account_id t704_account.c704_account_id%TYPE
        )
    RETURN VARCHAR2        
AS
v_collector_id t101_user.c101_user_id%TYPE;
BEGIN
    
    BEGIN
                     SELECT t101.c101_user_id INTO v_collector_id
					   FROM t704_account t704, t704a_account_attribute t704a, t101_user t101
					  , t906_rules t906
					  WHERE t704.c704_account_id        = t704a.c704_account_id
					    AND t704a.c704a_attribute_value = t906.c906_rule_id
                        AND t704.c704_account_id 		= p_account_id
					    AND t906.c906_rule_value        = TO_CHAR (t101.c101_user_id)
					    AND t704a.c901_attribute_type   = '103050' ---Collector ID attribute type
					    AND c906_rule_grp_id            = 'COLLECTOR_ID' ---Collector ID rule grp
					    AND t704.c704_void_fl          IS NULL
					    AND t704a.c704a_void_fl        IS NULL
					    AND t906.c906_void_fl          IS NULL
					    AND C901_USER_STATUS            = 311;  ---ACTIVE STATUS USERS
					   
    EXCEPTION
    WHEN OTHERS THEN
        v_collector_id := NULL;
    END;    
    RETURN v_collector_id;
END get_collector_id;

/**********************************************************************
*Description : function is used to get Account id status
***********************************************************************/

PROCEDURE gm_check_account(
p_account_id IN t704_account.c704_account_id%TYPE,
p_flag OUT varchar2
)
AS
	v_count NUMBER;
    v_company_id 		t1900_company.c1900_company_id%TYPE;
	BEGIN
		
		SELECT get_compid_frm_cntx()
          INTO v_company_id 
          FROM DUAL;
		SELECT COUNT(1)
        INTO v_count
        FROM t704_account
        WHERE c704_account_id=p_account_id
        AND c1900_company_id=v_company_id;		
		IF (v_count = 1)
		THEN
			p_flag		:= 'Y';
		ELSE
			p_flag		:= 'N';
		END IF;	
END gm_check_account;
				       
END gm_pkg_ar_accounts_rpt;
/
