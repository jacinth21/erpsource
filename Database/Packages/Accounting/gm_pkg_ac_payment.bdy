/* Formatted on 2011/04/21 18:06 (Formatter Plus v4.8.0) */
/*
 * @"C:\Database\Packages\Accounting\gm_pkg_ac_payment.bdy";
 */

CREATE OR REPLACE PACKAGE BODY gm_pkg_ac_payment
IS
--
   PROCEDURE gm_ac_fch_invoice_list (
      p_poid                  IN       t401_purchase_order.c401_purchase_ord_id%TYPE,
      p_out_invoicelist_cur   OUT      TYPES.cursor_type
   )
   AS
   /********************************************************************
    * Description : Procedure to fetch invoice list for a Purchase Order
    * Author           : Basudev Vidyasankar
    ********************************************************************/
   BEGIN
      OPEN p_out_invoicelist_cur
       FOR
          SELECT   t406.c406_payment_id paymentid,
                   t406.c406_invoice_id invoiceid,
                   t406.c406_invoice_dt invoicedt,
                   t406.c406_payment_amount paymentamount,
                   get_user_name (t406.c406_release_by) releaseby,
                   t406.c406_release_dt releasedt,
                   DECODE
                      (t406.c406_status_fl,
                       30, gm_pkg_cm_data_download.get_batch_id
                                                        (t406.c406_payment_id,
                                                         50740
                                                        ),
                       NULL
                      ) batchid,
                   DECODE
                      (t406.c406_status_fl,
                       30, gm_pkg_cm_data_download.get_batch_dt
                                                        (t406.c406_payment_id,
                                                         50740
                                                        ),
                       NULL
                      ) batchdt,
                   t406.c401_purchase_ord_id poid,
                   get_invoice_status (c406_status_fl) pstatus
              FROM t406_payment t406
             WHERE t406.c401_purchase_ord_id = p_poid
               AND t406.c406_void_fl IS NULL
          ORDER BY invoiceid, invoicedt ASC;
   END gm_ac_fch_invoice_list;

--

   /*******************************************************
     * Description : Procedure to update a payment
     * Author     : Xun
     *******************************************************/
   PROCEDURE gm_ac_update_payment (
      p_paymentids   IN   VARCHAR2,
      p_userid       IN   t301_vendor.c301_last_updated_by%TYPE
   )
   AS
      v_strlen       NUMBER                 := NVL (LENGTH (p_paymentids), 0);
      v_string       VARCHAR2 (4000)                     := p_paymentids;
      v_status_fl    t406_payment.c406_status_fl%TYPE;
      v_payment_id   t406_payment.c406_payment_id%TYPE;
	  v_dhr_id       VARCHAR2 (20);
	  
      TYPE dhr_array IS TABLE OF t408_dhr.c408_dhr_id%TYPE;
      v_dhr_array    dhr_array;
      
   BEGIN
      IF v_strlen > 0
      THEN
         WHILE INSTR (v_string, '^') <> 0
         LOOP
            --
            v_payment_id := NULL;
            v_payment_id := SUBSTR (v_string, 1, INSTR (v_string, '^') - 1);
            v_string := SUBSTR (v_string, INSTR (v_string, '^') + 1);

            --
            SELECT     t406.c406_status_fl
                  INTO v_status_fl
                  FROM t406_payment t406
                 WHERE t406.c406_payment_id = v_payment_id
            FOR UPDATE;

            IF v_status_fl > 20
            --if any value larger than 20 , 20 =  payment released
            THEN
               raise_application_error (-20870, '');
            END IF;

            UPDATE t406_payment
               SET c406_status_fl = 30,
                   c406_last_updated_by = p_userid,
                   c406_last_updated_date = CURRENT_DATE,
                   c406_download_dt = TRUNC (CURRENT_DATE)
             WHERE c406_payment_id = v_payment_id;

            SELECT c408_dhr_id
            BULK COLLECT INTO v_dhr_array
              FROM t408_dhr t408
             WHERE t408.c406_payment_id = v_payment_id
             AND t408.c408_Void_fl IS NULL; 
             /* In existing scenario, when we void a DHR, payment id is updated to null. Here after  We going to keep payment id in voided DHR for PMT-2600.
          	  * So, whenever payment id is used to pull DHR the void flag check must required. 
              */
            IF v_dhr_array.COUNT > 0
            THEN
               FOR i IN v_dhr_array.FIRST .. v_dhr_array.LAST
               LOOP
                  v_dhr_id := v_dhr_array (i);
                  gm_pkg_op_dhr.gm_op_sav_dhr_posting (v_dhr_id,
                                                       NULL,
                                                       48140,
                                                       p_userid
                                                      );
               END LOOP;
            END IF;
         END LOOP;
      END IF;
   END gm_ac_update_payment;

/*****************************************************************************
   * Description : Function to get work order cost price
   * Parameter p_dhr_type,  80191(GM-RC-XXX) -- raw material 100.000
   * If type is RC, then wo cost should be split cost.
   * If type is regular DHR
   * 1) no RC,   wo cost should be c402_cost_price.
   * 2) with RC existed, wo cost should be c402_cost_price - c402_split_cost.
   ***************************************************************************/
   FUNCTION get_work_order_ea_cost (
      p_workorder_id   IN   t402_work_order.c402_work_order_id%TYPE,
      p_dhr_type       IN   t408_dhr.c901_type%TYPE
   )
      RETURN NUMBER
   IS
      v_cost_ea   NUMBER;
   BEGIN
      SELECT   NVL ((DECODE (p_dhr_type,
                             80191, t402.c402_split_cost,
                             DECODE (t402.c402_rm_inv_fl,
                                     'Y', t402.c402_cost_price
                                      - t402.c402_split_cost,
                                     t402.c402_cost_price
                                    )
                            )
                    ),
                    0
                   )
             / t405.c405_uom_qty
        INTO v_cost_ea
        FROM t405_vendor_pricing_details t405, t402_work_order t402
       WHERE t402.c402_work_order_id = p_workorder_id
         AND t402.c402_void_fl IS NULL
         AND t405.c405_pricing_id = t402.c405_pricing_id
         AND t405.c405_void_fl IS NULL;

      RETURN v_cost_ea;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN 0;
   END get_work_order_ea_cost;

--
            /********************************************************************************
                         * Description : Procedure to save invoice details for a Purchase Order
                         * Author           : Basudev Vidyasankar
             ********************************************************************************/
   PROCEDURE gm_update_dhr_payment_multiple (
      p_vendid          IN       t406_payment.c301_vendor_id%TYPE,
      p_paymentid       IN       t406_payment.c406_payment_id%TYPE,
      p_invoiceid       IN       t406_payment.c406_invoice_id%TYPE,
      p_invoicedt       IN       VARCHAR2,
      p_invoiceamount   IN       t406_payment.c406_payment_amount%TYPE,
      p_comments        IN       t406_payment.c406_comments%TYPE,
      p_int_comments	IN		 t406_payment.c406_internal_comments%TYPE,
      p_poid            IN       t401_purchase_order.c401_purchase_ord_id%TYPE,
      p_dhrstr          IN       VARCHAR2,
      p_addlchrgstr     IN       VARCHAR2,
      p_userid          IN       t406_payment.c406_created_by%TYPE,
      p_paymentid_out   OUT      t406_payment.c406_payment_id%TYPE
   )
   AS
      v_id             NUMBER;
      v_dhr            VARCHAR2 (20);
      v_dhr_qty        NUMBER;
      v_inv_amt        NUMBER;
      v_ext_inv_amt    NUMBER;
      v_cre_amt        NUMBER;
      v_diff_amt       NUMBER;
      
      v_chkflag        CHAR (1);
      v_strlen         NUMBER           := NVL (LENGTH (p_dhrstr), 0);
      v_string         VARCHAR2 (30000) := p_dhrstr;
      v_substring      VARCHAR2 (30000);
      v_payment        NUMBER;
      v_paymentid      NUMBER           := 0;
      v_status         NUMBER (2)       := 0;
      v_invoiceidcnt   NUMBER (2);
      v_dateformat VARCHAR2(100);
      v_company_id  t1900_company.c1900_company_id%TYPE;
   BEGIN
	   
	  -- SELECT sys_context('CLIENTCONTEXT','COMP_DATE_FMT') into v_dateformat from dual;
	  
	   select get_compid_frm_cntx(),get_compdtfmt_frm_cntx() 
	     into v_company_id,v_dateformat 
	     from dual;
      IF p_paymentid <> 0
      THEN
         SELECT     NVL (t406.c406_payment_id, 0),
                    NVL (t406.c406_status_fl, 0)
               INTO v_paymentid,
                    v_status
               FROM t406_payment t406
              WHERE t406.c406_payment_id = p_paymentid
                AND t406.c406_void_fl IS NULL
         FOR UPDATE;
      END IF;

      --
      v_payment := 0;

      --
      IF v_paymentid = 0
      THEN
         --
         SELECT s406_payment.NEXTVAL
           INTO v_id
           FROM DUAL;

         --
         INSERT INTO t406_payment
                     (c406_payment_id, c406_invoice_id, c301_vendor_id,
                      c406_entered_dt, c406_invoice_dt,
                      c406_payment_date, c406_payment_amount,
                      c406_comments, c406_internal_comments, c401_purchase_ord_id, c406_status_fl,
                      c406_created_date, c406_created_by,c1900_company_id
                     )
              VALUES (v_id, p_invoiceid, p_vendid,
                      TRUNC (CURRENT_DATE), TO_DATE (p_invoicedt, v_dateformat),
                      TO_DATE (p_invoicedt, v_dateformat), p_invoiceamount,
                      p_comments, p_int_comments, p_poid, 0,
                      CURRENT_DATE, p_userid,v_company_id
                     );

         --
         p_paymentid_out := v_id;
         v_payment := v_id;
      --
      ELSE
         IF (v_status > 10)
         THEN
            raise_application_error (-20821, '');
         --add oracle app error no. in apperror excel
         ELSE
            UPDATE t406_payment
               SET c406_invoice_id = p_invoiceid,
                   c406_invoice_dt = TO_DATE (p_invoicedt, v_dateformat),
                   c406_payment_amount = p_invoiceamount,
                   c406_comments = p_comments,
                   c406_internal_comments = p_int_comments,
                   c406_status_fl = 0
             WHERE t406_payment.c406_payment_id = v_paymentid
               AND c406_void_fl IS NULL;

            --
            v_payment := v_paymentid;
         --
         END IF;
      END IF;

      SELECT COUNT (t406.c406_invoice_id)
        INTO v_invoiceidcnt
        FROM t406_payment t406
       WHERE t406.c401_purchase_ord_id = p_poid
         AND t406.c406_invoice_id = p_invoiceid
         AND t406.c406_void_fl IS NULL;

      IF v_invoiceidcnt > 1
      THEN
         raise_application_error (-20871, '');
      END IF;

      --
      IF v_strlen > 0
      THEN
         WHILE INSTR (v_string, '|') <> 0
         LOOP
            --
            v_dhr := NULL;
            v_chkflag := NULL;
            v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
            v_string := SUBSTR (v_string, INSTR (v_string, '|') + 1);
            v_dhr := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
            v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
            v_chkflag := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
            v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
			v_inv_amt := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
			v_ext_inv_amt := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
			v_cre_amt := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
			v_diff_amt := v_substring;
			
			--
            IF (v_chkflag = '1')
            THEN
               UPDATE t408_dhr
                  SET c406_payment_id = v_payment,
                      c408_payment_fl = 'Y'
                WHERE c408_dhr_id = v_dhr;
                --
                gm_ap_sav_dhr_payment (v_dhr, v_inv_amt, v_ext_inv_amt, v_cre_amt, v_diff_amt, p_userid);
                --
            ELSE
               UPDATE t408_dhr
                  SET c406_payment_id = NULL,
                      c408_payment_fl = NULL
                WHERE c408_dhr_id = v_dhr;
            END IF;
         --
         END LOOP;
      END IF;

      --
      gm_ac_sav_invoice_addl_chrgs (v_payment, p_addlchrgstr, p_userid);
      gm_ac_update_invc_status (v_payment, '');
      --
      p_paymentid_out := v_payment;
   --
   END gm_update_dhr_payment_multiple;

--
/********************************************************************************
    * Description : Procedure to save dhr payment credit charge details
    * Author   : Jayaprasanth Gurunathan
 ********************************************************************************/
   PROCEDURE gm_ap_sav_dhr_payment (
      p_dhr_id   		IN   T4083_AP_DHR_PAYMENT.C408_DHR_ID%TYPE,
      p_inv_amt  		IN   T4083_AP_DHR_PAYMENT.C4083_INVOICE_AMOUNT%TYPE,
      p_ext_inv_amt  	IN   T4083_AP_DHR_PAYMENT.C4083_EXT_INVOICE_AMOUNT%TYPE,
      p_cre_amt  		IN   T4083_AP_DHR_PAYMENT.C4083_CREDIT_AMOUNT%TYPE,
      p_diff_amt  		IN   T4083_AP_DHR_PAYMENT.C4083_DIFFERENCE_AMOUNT%TYPE,
      p_userid   		IN   T4083_AP_DHR_PAYMENT.C4083_LAST_UPDATED_BY%TYPE
   )
   AS
   
   v_payment_id 		NUMBER;
   v_qty_received 		NUMBER;
   v_qty_on_shelf 		NUMBER;
   
   BEGIN
      --
      	SELECT c406_payment_id,
		  c408_qty_received,
		  c408_qty_on_shelf
		INTO v_payment_id,
		  v_qty_received,
		  v_qty_on_shelf
		FROM t408_dhr
		WHERE c408_dhr_id = p_dhr_id
		AND c408_void_fl is null;
      --
      
      UPDATE t4083_ap_dhr_payment
      SET c406_payment_id = v_payment_id,
      		c408_qty_received = v_qty_received,
      		c408_qty_on_shelf = v_qty_on_shelf,
	  		c4083_invoice_amount= p_inv_amt,
	  		c4083_ext_invoice_amount = p_ext_inv_amt,
	  		c4083_credit_amount= p_cre_amt,
	  		c4083_difference_amount = p_diff_amt,
	  		c4083_last_updated_by= p_userid,
      		c4083_last_updated_date=current_date
      		WHERE c408_dhr_id = p_dhr_id
      		AND c4083_void_fl is null;
      		
      	IF (SQL%ROWCOUNT = 0)
		THEN 

            INSERT INTO t4083_ap_dhr_payment
                        (c408_dhr_id, c406_payment_id, c408_qty_received, c408_qty_on_shelf, 
                        c4083_invoice_amount, c4083_ext_invoice_amount, c4083_credit_amount, c4083_difference_amount, 
                        c4083_last_updated_by,c4083_last_updated_date
                        )
                 VALUES (p_dhr_id, v_payment_id, v_qty_received, v_qty_on_shelf,
                 		p_inv_amt, p_ext_inv_amt, p_cre_amt, p_diff_amt, 
                        p_userid, CURRENT_DATE
                        );
         END IF;

   END gm_ap_sav_dhr_payment;
/********************************************************************************
    * Description : Procedure to save additional charge details for an invoice
    * Author   : Basudev Vidyasankar
 ********************************************************************************/
   PROCEDURE gm_ac_sav_invoice_addl_chrgs (
      p_paymentid   IN   t406_payment.c406_payment_id%TYPE,
      p_inputstr    IN   VARCHAR2,
      p_userid      IN   t4601_payment_addl_charge.c4601_created_by%TYPE
   )
   AS
      v_addlcharge_id        NUMBER;
      v_addl_charge_type     NUMBER;
      v_addl_charge_amount   NUMBER (15, 2);
      v_strlen               NUMBER           := NVL (LENGTH (p_inputstr), 0);
      v_string               VARCHAR2 (30000) := p_inputstr;
      v_substring            VARCHAR2 (30000);
   BEGIN
      --
      DELETE FROM t4601_payment_addl_charge
            WHERE c406_payment_id = p_paymentid;

      --
      IF v_strlen > 0
      THEN
         WHILE INSTR (v_string, '|') <> 0
         LOOP
            --
            v_addl_charge_type := NULL;
            v_addl_charge_amount := NULL;
            v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
            v_string := SUBSTR (v_string, INSTR (v_string, '|') + 1);
            v_addl_charge_type :=
               TO_NUMBER (SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1)
                         );
            v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
            v_addl_charge_amount := TO_NUMBER (v_substring);

            INSERT INTO t4601_payment_addl_charge
                        (c4601_payment_addl_charge_id, c406_payment_id,
                         c901_type, c4601_addl_charge_amount,
                         c4601_created_by, c4601_created_date
                        )
                 VALUES (s4601_payment_addl_charge.NEXTVAL, p_paymentid,
                         v_addl_charge_type, v_addl_charge_amount,
                         p_userid, CURRENT_DATE
                        );
         --
         END LOOP;
      END IF;
   END gm_ac_sav_invoice_addl_chrgs;

--
   /********************************************************************************
       * Description : Procedure to fetch additional charge details for an invoice
       * Author   : Basudev Vidyasankar
    ********************************************************************************/
   PROCEDURE gm_ac_fch_addl_charge_dtls (
      p_paymentid            IN       t406_payment.c406_invoice_id%TYPE,
      p_out_addl_chrgs_cur   OUT      TYPES.cursor_type
   )
   AS
   BEGIN
      OPEN p_out_addl_chrgs_cur
       FOR
          SELECT t4601.c901_type addltype,
                 t4601.c4601_addl_charge_amount addlamount
            FROM t4601_payment_addl_charge t4601
           WHERE t4601.c4601_void_fl IS NULL
             AND t4601.c406_payment_id = p_paymentid;
   END gm_ac_fch_addl_charge_dtls;

--
   /********************************************************************************
    * Description : Procedure to fetch invoice details for selected invoice
    * Author   : Basudev Vidyasankar
    ********************************************************************************/
   PROCEDURE gm_ac_fch_invoice_dtls (
      p_paymentid              IN       t406_payment.c406_invoice_id%TYPE,
      p_out_invoice_dtls_cur   OUT      TYPES.cursor_type
   )
   AS
   v_dateformat VARCHAR2(100);
   BEGIN
   
	   select get_compdtfmt_frm_cntx() 
	     into v_dateformat 
	     from dual;
     
      OPEN p_out_invoice_dtls_cur
       FOR
          SELECT t406.c406_invoice_id invoiceid,
                 TO_CHAR (t406.c406_invoice_dt,  NVL(v_dateformat,'mm/dd/yyyy')) invoicedt,
                 t406.c406_payment_amount pamount,
                 t406.c406_comments comments, t406.c406_internal_comments invoiceintcomments, t406.c401_purchase_ord_id poid,
                 NVL (t406.c406_status_fl, 0) pstatus,
                 c401_purchase_ord_id poid
            FROM t406_payment t406
           WHERE t406.c406_payment_id = p_paymentid;
   END gm_ac_fch_invoice_dtls;

--

   /*******************************************************
     * Description : Procedure to get A/P Postings report
     * Author     : Xun
     *******************************************************/
   PROCEDURE gm_ac_fch_ap_posting (
      p_batchid           IN       t9500_data_download_log.c9500_data_download_id%TYPE,
      p_frmdt             IN       VARCHAR2,
      p_todt              IN       VARCHAR2,
      p_out_posting_cur   OUT      TYPES.cursor_type
   )
   AS
   v_company_id  t1900_company.c1900_company_id%TYPE;
   v_dateformat VARCHAR2(100);
   BEGIN
	   
	 SELECT get_compid_frm_cntx(),get_compdtfmt_frm_cntx()
	   INTO v_company_id,v_dateformat 
	   FROM dual;
	   
      OPEN p_out_posting_cur
       FOR
          SELECT   get_rule_value (t205.c205_product_family,
                                   'A/P-Posting-Debit'
                                  ) accountid                     --ruletable
                                             ,
                   t205.c205_product_family ruleid,
                   get_code_name (t205.c205_product_family) accountname,
                   NVL (
                      SUM
                      (  t408.c408_qty_on_shelf
                       * get_currency_conversion
                            (get_vendor_currency (t408.c301_vendor_id),
                             get_comp_curr (t408.c1900_company_id),
                             t9500.c9500_download_dt,
                             NVL
                                (gm_pkg_ac_payment.get_work_order_ea_cost
                                                     (t408.c402_work_order_id,
                                                      t408.c901_type
                                                     ),
                                 0
                                )
                            )
                      ), 0) debit,
                   0 credit
              FROM t9500_data_download_log t9500,
                   t9501_data_log_detail t9501,
                   t408_dhr t408,
                   t205_part_number t205
             WHERE t9500.c9500_data_download_id =
                      DECODE (p_batchid,
                              0, t9500.c9500_data_download_id,
                              p_batchid
                             )
               AND TRUNC (t9500.c9500_download_dt) >=
                      DECODE (p_frmdt,
                              NULL, TRUNC (t9500.c9500_download_dt),
                              TO_DATE (p_frmdt, v_dateformat)
                             )
               AND TRUNC (t9500.c9500_download_dt) <=
                      DECODE (p_todt,
                              NULL, TRUNC (t9500.c9500_download_dt),
                              TO_DATE (p_todt, v_dateformat)
                             )
               AND t9501.c9500_data_download_id = t9500.c9500_data_download_id
               AND t9501.c9501_ref_id = t408.c406_payment_id
               AND t205.c205_part_number_id = t408.c205_part_number_id
               AND t9500.c1900_company_id = v_company_id
              AND t408.c408_void_fl IS NULL  
              /* In existing scenario, when we void a DHR, payment id is updated to null. Here after  We going to keep payment id in voided DHR for PMT-2600.
               * So, whenever payment id is used to pull DHR the void flag check must required. 
               */
          GROUP BY t205.c205_product_family
          UNION ALL
          SELECT   get_rule_value (TO_CHAR (t4601.c901_type),
                                   'A/P-Posting-Debit'
                                  ) accountid                     -- ruletable
                                             ,
                   TO_CHAR (t4601.c901_type) ruleid,
                   get_code_name (t4601.c901_type) accountname,
                   NVL (
                   SUM
                      (get_currency_conversion
                                    (get_vendor_currency (t406.c301_vendor_id),
                                     get_comp_curr (t406.c1900_company_id),
                                     t9500.c9500_download_dt,
                                     NVL (t4601.c4601_addl_charge_amount, 0)
                                    )
                      ), 0) debit,
                   0 credit
              FROM t9500_data_download_log t9500,
                   t9501_data_log_detail t9501,
                   t4601_payment_addl_charge t4601,
                   t406_payment t406
             WHERE t9500.c9500_data_download_id =
                      DECODE (p_batchid,
                              0, t9500.c9500_data_download_id,
                              p_batchid
                             )
               AND TRUNC (t9500.c9500_download_dt) >=
                      DECODE (p_frmdt,
                              NULL, TRUNC (t9500.c9500_download_dt),
                              TO_DATE (p_frmdt, v_dateformat)
                             )
               AND TRUNC (t9500.c9500_download_dt) <=
                      DECODE (p_todt,
                              NULL, TRUNC (t9500.c9500_download_dt),
                              TO_DATE (p_todt, v_dateformat)
                             )
               AND t9501.c9500_data_download_id = t9500.c9500_data_download_id
               AND t9501.c9501_ref_id = t4601.c406_payment_id
               AND t406.c406_payment_id = t4601.c406_payment_id
               AND t406.c1900_company_id = v_company_id
          GROUP BY t4601.c901_type
          UNION ALL
          SELECT '2000-00-00' accountid, '91310' ruleid,
                 'Accounts Payable' accountname, 0 debit,
                 NVL (
                 SUM
                    (get_currency_conversion
                                    (get_vendor_currency (t406.c301_vendor_id),
                                     get_comp_curr (t406.c1900_company_id),
                                     t9500.c9500_download_dt,
                                     NVL (t406.c406_payment_amount, 0)
                                    )
                    ), 0) credit
            FROM t9500_data_download_log t9500,
                 t9501_data_log_detail t9501,
                 t406_payment t406
           WHERE t9500.c9500_data_download_id =
                    DECODE (p_batchid,
                            0, t9500.c9500_data_download_id,
                            p_batchid
                           )
             AND TRUNC (t9500.c9500_download_dt) >=
                    DECODE (p_frmdt,
                            NULL, TRUNC (t9500.c9500_download_dt),
                            TO_DATE (p_frmdt, v_dateformat)
                           )
             AND TRUNC (t9500.c9500_download_dt) <=
                    DECODE (p_todt,
                            NULL, TRUNC (t9500.c9500_download_dt),
                            TO_DATE (p_todt, v_dateformat)
                           )
             AND t9501.c9500_data_download_id = t9500.c9500_data_download_id
             AND t9501.c9501_ref_id = t406.c406_payment_id
             AND t406.c1900_company_id = v_company_id;
   END gm_ac_fch_ap_posting;

/******************************************************************
   * Description : Function to get invoice ID
   ****************************************************************/
   FUNCTION get_invoice_id (p_payment_id IN t408_dhr.c406_payment_id%TYPE)
      RETURN VARCHAR2
   IS
      v_invoice_id   t406_payment.c406_invoice_id%TYPE;
   BEGIN
      BEGIN
         SELECT c406_invoice_id
           INTO v_invoice_id
           FROM t406_payment
          WHERE c406_payment_id = p_payment_id AND c406_void_fl IS NULL;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            RETURN '';
      END;

      RETURN v_invoice_id;
   END get_invoice_id;

/******************************************************************
   * Description : Function to get Vendor ID
   ****************************************************************/
   FUNCTION get_vendor_id (p_payment_id IN t408_dhr.c406_payment_id%TYPE)
      RETURN NUMBER
   IS
      v_vendor_id   t301_vendor.c301_vendor_id%TYPE;
   BEGIN
      BEGIN
         SELECT c301_vendor_id
           INTO v_vendor_id
           FROM t406_payment
          WHERE c406_payment_id = p_payment_id;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            RETURN 0;
      END;

      RETURN v_vendor_id;
   END get_vendor_id;

/*******************************************************
     * Description : Procedure to get A/P Postings Detail
     * Author     : Xun
     *******************************************************/
   PROCEDURE gm_ac_fch_ap_dtls (
      p_batchid           IN       t9500_data_download_log.c9500_data_download_id%TYPE,
      p_frmdt             IN       VARCHAR2,
      p_todt              IN       VARCHAR2,
      p_rule_id           IN       t906_rules.c906_rule_id%TYPE,
      p_account_id        IN       t205_part_number.c205_product_family%TYPE,
      p_out_posting_cur   OUT      TYPES.cursor_type
   )
   AS
      v_count_prod     NUMBER := 0;
      v_count_addl     NUMBER := 0;
      v_count_credit   NUMBER := 0;
      v_dateformat VARCHAR2(100);
      v_company_id t1900_company.c1900_company_id%TYPE;
   BEGIN
	   
	 SELECT get_compdtfmt_frm_cntx(),get_compid_frm_cntx()
	   INTO v_dateformat,  v_company_id
	   FROM dual;
	   
      SELECT COUNT (*)
        INTO v_count_prod
        FROM t901_code_lookup
       WHERE c901_code_grp = 'PFLY' AND c902_code_nm_alt = p_account_id;

      SELECT COUNT (*)
        INTO v_count_addl
        FROM t901_code_lookup
       WHERE c901_code_grp = 'APADDL' AND c902_code_nm_alt = p_account_id;

      SELECT COUNT (*)
        INTO v_count_credit
        FROM t901_code_lookup
       WHERE c901_code_grp = 'CRTYP' AND c902_code_nm_alt = p_account_id;

      IF (v_count_prod > 0)
      --    ('1620-00-00', '1120-00-00', '1111-00-00', '1128-00-00', '1139-00-00', '1142-00-00', '1180-00-00')
      THEN
         OPEN p_out_posting_cur
          FOR
             SELECT get_invoice_id (t408.c406_payment_id) invoiceid,
                    t408.c406_payment_id paymentid,
                    t205.c205_part_number_id partnumber,
                    get_partnum_desc (t205.c205_part_number_id) pdesc,
                    get_vendor_name (t408.c301_vendor_id) vendornm,
                    get_code_name (t205.c205_product_family) detail,
                    t408.c408_dhr_id dhrid, t408.c408_qty_on_shelf qtyacc,
                      get_currency_conversion
                         (get_vendor_currency
                                          (get_vendor_id (t408.c406_payment_id)
                                          ),
                          get_comp_curr (t408.c1900_company_id),
                          t9500.c9500_download_dt,
                          NVL
                             (get_work_order_ea_cost (t408.c402_work_order_id,
                                                      t408.c901_type
                                                     ),
                              0
                             )
                         )
                    * 1 eachcost,
                      get_currency_conversion
                         (get_vendor_currency
                                          (get_vendor_id (t408.c406_payment_id)
                                          ),
                          get_comp_curr (t408.c1900_company_id),
                          t9500.c9500_download_dt,
                          NVL
                             (  t408.c408_qty_on_shelf
                              * get_work_order_ea_cost
                                                     (t408.c402_work_order_id,
                                                      t408.c901_type
                                                     ),
                              0
                             )
                         )
                    * 1 totcost
               --  , get_work_order_ea_cost (t408.c402_work_order_id) eachcost
               -- , t408.c408_qty_on_shelf * get_work_order_ea_cost (t408.c402_work_order_id) totcost
             FROM   t9500_data_download_log t9500,
                    t9501_data_log_detail t9501,
                    t408_dhr t408,
                    t205_part_number t205
              WHERE t9500.c9500_data_download_id =
                       DECODE (p_batchid,
                               0, t9500.c9500_data_download_id,
                               p_batchid
                              )
                AND TRUNC (t9500.c9500_download_dt) >=
                       DECODE (p_frmdt,
                               NULL, TRUNC (t9500.c9500_download_dt),
                               TO_DATE (p_frmdt, v_dateformat)
                              )
                AND TRUNC (t9500.c9500_download_dt) <=
                       DECODE (p_todt,
                               NULL, TRUNC (t9500.c9500_download_dt),
                               TO_DATE (p_todt, v_dateformat)
                              )
                AND t205.c205_product_family = p_rule_id                --4051
                AND t9501.c9500_data_download_id =
                                                  t9500.c9500_data_download_id
                AND t408.c406_payment_id = t9501.c9501_ref_id
                AND t408.c205_part_number_id = t205.c205_part_number_id
                AND t408.c408_Void_fl IS NULL; 
                /* In existing scenario, when we void a DHR, payment id is updated to null. Here after  We going to keep payment id in voided DHR for PMT-2600.
                 * So, whenever payment id is used to pull DHR the void flag check must required. 
                 */
      END IF;

      IF (v_count_addl > 0)
-- p_account_id in ()-- '5004-00-00' , '1628-00-00'     ,'1621-00-00'  , '6037-05-00'
      THEN
         OPEN p_out_posting_cur
          FOR
             SELECT get_invoice_id (t4601.c406_payment_id) invoiceid,
                    t4601.c406_payment_id paymentid, '-' partnumber,
                    '-' pdesc, '-' vendornm,
                    get_code_name (t4601.c901_type) detail, '-' dhrid,
                    1 qtyacc,
                      get_currency_conversion
                         (get_vendor_currency
                                         (get_vendor_id (t4601.c406_payment_id)
                                         ),
                          get_comp_curr (v_company_id),
                          t9500.c9500_download_dt,
                          NVL (t4601.c4601_addl_charge_amount, 0)
                         )
                    * 1 eachcost,
                      get_currency_conversion
                         (get_vendor_currency
                                         (get_vendor_id (t4601.c406_payment_id)
                                         ),
                          get_comp_curr (v_company_id),
                          t9500.c9500_download_dt,
                          NVL (t4601.c4601_addl_charge_amount, 0)
                         )
                    * 1 totcost
               --  t4601.c4601_addl_charge_amount eachcost, t4601.c4601_addl_charge_amount totcost
             FROM   t9500_data_download_log t9500,
                    t9501_data_log_detail t9501,
                    t4601_payment_addl_charge t4601
              WHERE t9500.c9500_data_download_id =
                       DECODE (p_batchid,
                               0, t9500.c9500_data_download_id,
                               p_batchid
                              )
                AND TRUNC (t9500.c9500_download_dt) >=
                       DECODE (p_frmdt,
                               NULL, TRUNC (t9500.c9500_download_dt),
                               TO_DATE (p_frmdt, v_dateformat)
                              )
                AND TRUNC (t9500.c9500_download_dt) <=
                       DECODE (p_todt,
                               NULL, TRUNC (t9500.c9500_download_dt),
                               TO_DATE (p_todt, v_dateformat)
                              )
                AND t4601.c901_type = p_rule_id                        --91303
                AND t9501.c9500_data_download_id =
                                                  t9500.c9500_data_download_id
                AND t4601.c406_payment_id = TO_NUMBER (t9501.c9501_ref_id);
      END IF;

      IF (v_count_credit > 0)                                 --= '2000-00-00'
      THEN
         OPEN p_out_posting_cur
          FOR
             SELECT t406.c406_invoice_id invoiceid, '-' paymentid,
                    '-' partnumber, '-' pdesc,
                    get_vendor_name (t406.c301_vendor_id) vendornm,
                    'AP Invoice' detail, '-' dhrid, 1 qtyacc,
                      get_currency_conversion
                         (get_vendor_currency
                                          (get_vendor_id (t406.c406_payment_id)
                                          ),
                          get_comp_curr (t406.c1900_company_id),
                          t9500.c9500_download_dt,
                          NVL (t406.c406_payment_amount, 0)
                         )
                    * 1 eachcost,
                      get_currency_conversion
                         (get_vendor_currency
                                          (get_vendor_id (t406.c406_payment_id)
                                          ),
                          get_comp_curr (t406.c1900_company_id),
                          t9500.c9500_download_dt,
                          NVL (t406.c406_payment_amount, 0)
                         )
                    * 1 totcost
               --  , t406.c406_payment_amount eachcost, t406.c406_payment_amount totcost
             FROM   t9500_data_download_log t9500,
                    t9501_data_log_detail t9501,
                    t406_payment t406
              WHERE t9500.c9500_data_download_id =
                       DECODE (p_batchid,
                               0, t9500.c9500_data_download_id,
                               p_batchid
                              )
                AND TRUNC (t9500.c9500_download_dt) >=
                       DECODE (p_frmdt,
                               NULL, TRUNC (t9500.c9500_download_dt),
                               TO_DATE (p_frmdt, v_dateformat)
                              )
                AND TRUNC (t9500.c9500_download_dt) <=
                       DECODE (p_todt,
                               NULL, TRUNC (t9500.c9500_download_dt),
                               TO_DATE (p_todt, v_dateformat)
                              )
                AND t9501.c9500_data_download_id =
                                                  t9500.c9500_data_download_id
                AND t406.c406_payment_id = t9501.c9501_ref_id;
      END IF;
   END gm_ac_fch_ap_dtls;

/******************************************************************
   * Description : Function to get purchase order total amount
   ****************************************************************/
   FUNCTION get_invc_ready_payment_amount (
      p_payment_id   IN   t406_payment.c406_payment_id%TYPE
   )
      RETURN NUMBER
   IS
      v_ready_payment    NUMBER (15, 2);
      v_acc_qty_amount   NUMBER (15, 2);
      v_addl_chrg        NUMBER (15, 2);
   BEGIN
      SELECT SUM (  t408.c408_qty_on_shelf
                  * get_work_order_ea_cost (t408.c402_work_order_id,
                                            t408.c901_type
                                           )
                 )
        INTO v_acc_qty_amount
        FROM t408_dhr t408
       WHERE t408.c406_payment_id = p_payment_id
         AND t408.c408_status_fl = '4'
         AND t408.c408_dhr_id IS NOT NULL
         AND t408.c408_void_fl IS NULL;  
         /* In existing scenario, when we void a DHR, payment id is updated to null. Here after  We going to keep payment id in voided DHR for PMT-2600.
          * So, whenever payment id is used to pull DHR the void flag check must required. 
          */

      SELECT get_invc_addl_chrg (p_payment_id)
        INTO v_addl_chrg
        FROM DUAL;

      v_ready_payment := v_acc_qty_amount + NVL (v_addl_chrg, 0);
      RETURN v_ready_payment;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN 0;
   END get_invc_ready_payment_amount;

/******************************************************************
   * Description : Function to get purchase order total additional charge
   ****************************************************************/
   FUNCTION get_invc_addl_chrg (
      p_payment_id   IN   t406_payment.c406_payment_id%TYPE
   )
      RETURN NUMBER
   IS
      v_addl_chrg   NUMBER (15, 2);
   BEGIN
      SELECT SUM (t4601.c4601_addl_charge_amount)
        INTO v_addl_chrg
        FROM t4601_payment_addl_charge t4601
       WHERE t4601.c406_payment_id = p_payment_id
         AND t4601.c4601_void_fl IS NULL;

      RETURN v_addl_chrg;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN 0;
   END get_invc_addl_chrg;

/******************************************************************
   * Description : Function to get purchase order total qty received
   ****************************************************************/
   FUNCTION get_invoice_rec_qty (
      p_payment_id   IN   t406_payment.c406_payment_id%TYPE
   )
      RETURN NUMBER
   IS
      v_rec_qty   NUMBER;
   BEGIN
      SELECT SUM (t408.c408_qty_received)
        INTO v_rec_qty
        FROM t408_dhr t408
       WHERE t408.c406_payment_id = p_payment_id AND t408.c408_void_fl IS NULL;

      RETURN v_rec_qty;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN 0;
   END get_invoice_rec_qty;

/******************************************************************
   * Description : Function to get purchase order total qty on shelf
   ****************************************************************/
   FUNCTION get_invoice_acc_qty (
      p_payment_id   IN   t406_payment.c406_payment_id%TYPE
   )
      RETURN NUMBER
   IS
      v_acc_qty   NUMBER;
   BEGIN
      SELECT SUM (t408.c408_qty_on_shelf)
        INTO v_acc_qty
        FROM t408_dhr t408
       WHERE t408.c406_payment_id = p_payment_id AND t408.c408_void_fl IS NULL;

      RETURN v_acc_qty;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN 0;
   END get_invoice_acc_qty;

/******************************************************************
   * Description : Function to get purchase order ordered qty
   ****************************************************************/
   FUNCTION get_po_ordered_qty (
      p_poid   IN   t401_purchase_order.c401_purchase_ord_id%TYPE
   )
      RETURN NUMBER
   IS
      v_ord_qty   NUMBER (15, 2);
   BEGIN
      SELECT SUM (t402.c402_qty_ordered)
        INTO v_ord_qty
        FROM t402_work_order t402
       WHERE t402.c401_purchase_ord_id = p_poid AND t402.c402_void_fl IS NULL;

      RETURN v_ord_qty;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN 0;
   END get_po_ordered_qty;

/******************************************************************
   * Description : Function to get purchase order received qty
   ****************************************************************/
   FUNCTION get_po_received_qty (
      p_poid   IN   t401_purchase_order.c401_purchase_ord_id%TYPE
   )
      RETURN NUMBER
   IS
      v_rec_qty   NUMBER (15, 2);
   BEGIN
      SELECT SUM (t408.c408_qty_received)
        INTO v_rec_qty
        FROM t408_dhr t408
       WHERE t408.c401_purchase_ord_id = p_poid AND t408.c408_void_fl IS NULL;

      RETURN v_rec_qty;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN 0;
   END get_po_received_qty;

/******************************************************************
   * Description : Function to get purchase order accepted qty
   ****************************************************************/
   FUNCTION get_po_accepted_qty (
      p_poid   IN   t401_purchase_order.c401_purchase_ord_id%TYPE
   )
      RETURN NUMBER
   IS
      v_ord_qty   NUMBER (15, 2);
   BEGIN
      SELECT SUM (t408.c408_qty_on_shelf)
        INTO v_ord_qty
        FROM t408_dhr t408
       WHERE t408.c401_purchase_ord_id = p_poid AND t408.c408_void_fl IS NULL;

      --AND c406_payment_id IS NOT NULL;
      RETURN v_ord_qty;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN 0;
   END get_po_accepted_qty;

/******************************************************************
   * Description : Function to get purchase order ready payment amount
   ****************************************************************/
   FUNCTION get_po_ready_payment_amount (
      p_poid   IN   t401_purchase_order.c401_purchase_ord_id%TYPE
   )
      RETURN NUMBER
   IS
      v_po_amount   NUMBER (15, 2);
   BEGIN
      SELECT SUM (get_invc_ready_payment_amount (t406.c406_payment_id))
        INTO v_po_amount
        FROM t406_payment t406
       WHERE t406.c401_purchase_ord_id = p_poid AND t406.c406_void_fl IS NULL;

      RETURN v_po_amount;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN 0;
   END get_po_ready_payment_amount;

/*******************************************************
   * Description : Procedure to fetch ready for payment details
   * Author     : Xun Qu
   *******************************************************/
   PROCEDURE gm_ac_fch_payment_dtls (
      p_vendorid             IN       t301_vendor.c301_vendor_id%TYPE,
      p_poid                 IN       t401_purchase_order.c401_purchase_ord_id%TYPE,
      p_initiatedby          IN       VARCHAR2,
      p_initiateddate_from   IN       VARCHAR2,
      p_initiateddate_to     IN       VARCHAR2,
      p_invoice_status       IN       t406_payment.c406_status_fl%TYPE,
      p_po_list              OUT      TYPES.cursor_type,
      p_invc_list            OUT      TYPES.cursor_type
   )
   AS
   v_dateformat VARCHAR2(100);
   v_company_id  t1900_company.c1900_company_id%TYPE;
   BEGIN
	   
	  select get_compid_frm_cntx(),get_compdtfmt_frm_cntx() 
	    into v_company_id,v_dateformat 
	    from dual;
	   
      DELETE FROM my_temp_list;

      INSERT INTO my_temp_list
                  (my_temp_txn_id)
         SELECT DISTINCT t406.c401_purchase_ord_id
                    FROM t406_payment t406
                   WHERE t406.c406_status_fl = p_invoice_status       --<= 30
                     AND t406.c401_purchase_ord_id =
                            DECODE (p_poid,
                                    '', t406.c401_purchase_ord_id,
                                    p_poid
                                   )
                     AND t406.c301_vendor_id =
                            DECODE (p_vendorid,
                                    0, t406.c301_vendor_id,
                                    p_vendorid
                                   )
                     AND t406.c406_void_fl IS NULL
                     AND TRUNC (t406.c406_created_date) >=
                            DECODE (p_initiateddate_from,
                                    NULL, TRUNC (t406.c406_created_date),
                                    TO_DATE (p_initiateddate_from,
                                             v_dateformat
                                            )
                                   )
                     AND TRUNC (t406.c406_created_date) <=
                            DECODE (p_initiateddate_to,
                                    NULL, TRUNC (t406.c406_created_date),
                                    TO_DATE (p_initiateddate_to, v_dateformat)
                                   )
                     AND t406.c1900_company_id = v_company_id
                     AND t406.c406_created_by =
                            DECODE (p_initiatedby,
                                    '0', t406.c406_created_by,
                                    p_initiatedby
                                   );

      OPEN p_po_list
       FOR
          SELECT   t401.c401_purchase_ord_id purchase_order_id,
                   t401.c301_vendor_id vendorid,
                   get_vendor_name (t401.c301_vendor_id) vendor_name,
                   get_po_ordered_qty
                                    (t401.c401_purchase_ord_id)
                                                               po_ordered_qty,
                   get_po_received_qty
                                   (t401.c401_purchase_ord_id)
                                                              po_received_qty,
                   get_po_accepted_qty
                                   (t401.c401_purchase_ord_id)
                                                              po_accepted_qty,
                   t401.c401_po_total_amount po_total_amount,
                   get_po_ready_payment_amount
                              (t401.c401_purchase_ord_id)
                                                         po_ready_for_payment
              FROM t401_purchase_order t401
             WHERE t401.c401_purchase_ord_id IN (SELECT *
                                                   FROM my_temp_list)
               --= DECODE (p_poid, NULL, t401.c401_purchase_ord_id, p_poid)
               AND t401.c401_purchase_ord_id =
                      DECODE (p_poid,
                              NULL, t401.c401_purchase_ord_id,
                              p_poid
                             )
               AND t401.c301_vendor_id =
                       DECODE (p_vendorid,
                               0, t401.c301_vendor_id,
                               p_vendorid
                              )
               AND t401.c401_void_fl IS NULL
               AND t401.c1900_company_id = v_company_id
          --    AND t401.c401_payment_fl = DECODE (p_potype, '50020', 10, '50021', 30, '50022', t401.c401_payment_fl)
          ORDER BY vendor_name, t401.c401_purchase_ord_id;

      OPEN p_invc_list
       FOR
          SELECT   t406.c401_purchase_ord_id po_number,
                   get_vendor_name (t406.c301_vendor_id) vendor_name,
                   t406.c406_payment_id gm_invoice_id,
                   t406.c406_invoice_id vendor_invoice_id,
                   get_invoice_acc_qty (t406.c406_payment_id) accepted_qty,
                   get_invoice_rec_qty (t406.c406_payment_id) received_qty,
                   t406.c406_payment_amount invoice_amount,
                   get_invc_ready_payment_amount
                                      (t406.c406_payment_id)
                                                            ready_for_payment,
                   t406.c406_invoice_dt invoice_date,
                   gm_pkg_cm_data_download.get_batch_id
                                          (t406.c406_payment_id,
                                           50740
                                          ) batch_number,
                   gm_pkg_cm_data_download.get_batch_dt
                                                        (t406.c406_payment_id,
                                                         50740
                                                        ) batch_date,
                   gm_pkg_cm_data_download.get_batch_initiated_by
                                 (TO_CHAR (t406.c406_payment_id),
                                  50740
                                 ) initiated_by,
                   get_user_name (t406.c406_release_by) release_by,
                   t406.c406_release_dt release_dt,
                   t406.c406_status_fl statusfl,
                   get_invoice_status (t406.c406_status_fl) payment_status
              FROM t406_payment t406
             WHERE t406.c406_status_fl = p_invoice_status              --<= 30
               AND t406.c401_purchase_ord_id IN (SELECT *
                                                   FROM my_temp_list)
               --= DECODE (p_poid, NULL, t406.c401_purchase_ord_id, p_poid)
               AND t406.c401_purchase_ord_id =
                      DECODE (p_poid,
                              NULL, t406.c401_purchase_ord_id,
                              p_poid
                             )
               AND t406.c301_vendor_id =
                       DECODE (p_vendorid,
                               0, t406.c301_vendor_id,
                               p_vendorid
                              )
               AND t406.c406_void_fl IS NULL
               AND t406.c1900_company_id = v_company_id
          ORDER BY t406.c401_purchase_ord_id;
   END gm_ac_fch_payment_dtls;

/*******************************************************
   * Description : Procedure to release payment
   * Author     : Xun Qu
   *******************************************************/
   PROCEDURE gm_ac_sav_release_payment (
      p_paymentids   IN   VARCHAR2,
      p_userid       IN   t301_vendor.c301_last_updated_by%TYPE,
      p_release_fl   IN   VARCHAR2
   )
   AS
      v_strlen             NUMBER           := NVL (LENGTH (p_paymentids), 0);
      v_string             VARCHAR2 (4000)                    := p_paymentids;
      v_status_fl          t406_payment.c406_status_fl%TYPE;
      v_void_fl            t406_payment.c406_void_fl%TYPE;
      v_paymentid          t406_payment.c406_payment_id%TYPE;
      v_ready_payment_fl   VARCHAR2 (10);
    
      v_payment_amt        t406_payment.c406_payment_amount%TYPE;
   BEGIN
	  IF v_strlen > 0
      THEN
         WHILE INSTR (v_string, '^') <> 0
         LOOP
            --
            v_paymentid := NULL;
            v_paymentid := SUBSTR (v_string, 1, INSTR (v_string, '^') - 1);
            v_string := SUBSTR (v_string, INSTR (v_string, '^') + 1);

            SELECT     t406.c406_void_fl, t406.c406_status_fl,
                       t406.c406_payment_amount
                  INTO v_void_fl, v_status_fl,
                       v_payment_amt
                  FROM t406_payment t406
                 WHERE t406.c406_payment_id = v_paymentid
            FOR UPDATE;

            IF v_void_fl = 'Y'
            THEN
               raise_application_error (-20872, '');
            END IF;

            IF (p_release_fl = 'P' AND v_status_fl <> 10)
            THEN
               raise_application_error (-20873, '');
            END IF;
			IF (p_release_fl = 'D' AND v_status_fl <> 15)   --v_status_fl > 15
            THEN
               raise_application_error (-20873, '');
            END IF;

		BEGIN
            SELECT 'true'
              INTO v_ready_payment_fl
              FROM t406_payment t406
             WHERE t406.c406_payment_id = TO_NUMBER (v_paymentid)
               AND t406.c406_payment_amount =
                          get_invc_ready_payment_amount (t406.c406_payment_id);
		EXCEPTION
		WHEN NO_DATA_FOUND THEN
                  v_ready_payment_fl := NULL;
        END;
            IF v_ready_payment_fl IS NULL
            THEN
               raise_application_error (-20874, '');
            END IF;
			UPDATE t406_payment t406
               SET t406.c406_release_by =
                               DECODE (p_release_fl,
                                       'P', NULL,
                                       'D', p_userid
                                      ),
                   t406.c406_release_dt =
                                DECODE (p_release_fl,
                                        'P', NULL,
                                        'D', CURRENT_DATE
                                       ),
                   t406.c406_status_fl =
                      DECODE
                         (p_release_fl,
                          'P', 15,
                          'D', 20
                         ) --'P' release for payment, 'D' release for download
                          ,
                   t406.c406_last_updated_by = p_userid,
                   t406.c406_last_updated_date = CURRENT_DATE
             WHERE t406.c406_payment_id = v_paymentid;

            -- set status as downloaded for any invoice amount is 0, so it will not be in Sage download
            --Below block of code is commented due to pmt-6575 to allow user to create batch and download invoice.
            /*
            IF (p_release_fl = 'D' AND v_payment_amt = 0)
            THEN
               UPDATE t406_payment t406
                  SET t406.c406_status_fl = 30,
                      t406.c406_download_dt = TRUNC (CURRENT_DATE),
                      t406.c406_comments =
                            t406.c406_comments
                         || ', System automatically make this invoice as downloaded since its amount is 0'
                WHERE t406.c406_payment_id = v_paymentid
                  AND t406.c406_payment_amount = 0;
            END IF;
            */
         END LOOP;
      END IF;
   END gm_ac_sav_release_payment;

/************************************************************************
     * Description : Procedure to update purchase_fl to downloaded in the
     purchase order table if all invoices to this PO is downloaded
     * Author     : Xun
     *******************************************************************/
   PROCEDURE gm_ac_update_purchase (
      p_inputstr   IN   VARCHAR2,
      p_userid     IN   t301_vendor.c301_last_updated_by%TYPE
   )
   AS
      v_count          NUMBER                                   := 0;
      v_status_count   NUMBER                                   := 0;
      v_acc_qty        NUMBER                                   := 0;
      v_ord_qty        NUMBER                                   := 0;
      v_poid           t406_payment.c401_purchase_ord_id%TYPE;

      CURSOR cur_po_details
      IS
         SELECT DISTINCT c401_purchase_ord_id poid
                    FROM t406_payment
                   WHERE c406_payment_id IN (SELECT *
                                               FROM v_in_list
                                              WHERE token IS NOT NULL);
   BEGIN
      my_context.set_my_inlist_ctx (p_inputstr);

      FOR var_po_detail IN cur_po_details
      LOOP
         v_poid := var_po_detail.poid;

         SELECT SUM (t408.c408_qty_on_shelf)
           INTO v_acc_qty
           FROM t408_dhr t408
          WHERE t408.c401_purchase_ord_id = v_poid
            AND t408.c408_void_fl IS NULL
            AND c406_payment_id IS NOT NULL;

         SELECT SUM (t402.c402_qty_ordered)
           INTO v_ord_qty
           FROM t402_work_order t402
          WHERE t402.c401_purchase_ord_id = v_poid
            AND t402.c402_void_fl IS NULL;

         IF (v_acc_qty >= v_ord_qty)
         THEN
            SELECT COUNT (*)
              INTO v_count
              FROM t408_dhr
             WHERE c401_purchase_ord_id = v_poid
               AND c406_payment_id IS NULL
               AND c408_payment_fl IS NULL
               AND c408_void_fl IS NULL;

            IF (v_count = 0)
            THEN
               SELECT COUNT (*)
                 INTO v_status_count
                 FROM t406_payment
                WHERE c401_purchase_ord_id = v_poid AND c406_status_fl < 30;
            ---currently now we don't this update, will validate later
            /*       IF (v_status_count = 0)
                     THEN
                        UPDATE t401_purchase_order
                           SET c401_payment_fl = 30
                            , c401_last_updated_by = p_userid
                            , c401_last_updated_date = CURRENT_DATE
                         WHERE c401_purchase_ord_id = v_poid AND c401_void_fl IS NULL;
                     END IF; */
            END IF;
         END IF;
      END LOOP;
   END gm_ac_update_purchase;

/*******************************************************
     * Description : Procedure to display Invoice and
     associated DHR details
     * Author     : Xun
     *******************************************************/
   PROCEDURE gm_ac_fch_invc_dhr_dtls (
      p_vendorid       IN       t406_payment.c301_vendor_id%TYPE,
      p_dhrid          IN       t408_dhr.c408_dhr_id%TYPE,
      p_frmdt          IN       VARCHAR2,
      p_todt           IN       VARCHAR2,
      p_invoiceid      IN       t406_payment.c406_invoice_id%TYPE,
      p_out_invcdtls   OUT      TYPES.cursor_type
   )
   AS
   v_dateformat VARCHAR2(100);
   BEGIN
	   
	  select get_compdtfmt_frm_cntx() 
	    into v_dateformat 
	    from dual;
	    
      OPEN p_out_invcdtls
       FOR
          SELECT   t408.c408_dhr_id ID, t408.c205_part_number_id pnum,
                   get_partnum_desc (t408.c205_part_number_id) pdesc,
                   t408.c408_qty_received qtyrec,
                   t408.c408_received_date cdate,
                   t408.c408_packing_slip pslip,
                   get_vendor_sh_name (t408.c301_vendor_id) vendnm,
                   t408.c402_work_order_id workid, t408.c301_vendor_id vid,
                   NVL (t408.c408_qty_on_shelf, 0) shelfqty,
                   get_work_order_ea_cost (t402.c402_work_order_id,
                                           t408.c901_type
                                          ) COST,
                     get_work_order_ea_cost (t402.c402_work_order_id,
                                             t408.c901_type
                                            )
                   * t408.c408_qty_received reccost,
                     get_work_order_ea_cost
                                          (t402.c402_work_order_id,
                                           t408.c901_type
                                          )
                   * t408.c408_qty_on_shelf shelfcost,
                   t406.c406_invoice_id invid,
                   t406.c406_invoice_dt invdt,
                   CASE
                      WHEN t408.c408_status_fl = '1'
                         THEN 'Received'
                      WHEN t408.c408_status_fl = '2'
                         THEN 'Inspected'
                      WHEN t408.c408_status_fl = '3'
                         THEN 'Packaged'
                      WHEN t408.c408_status_fl = '4'
                         THEN 'Verified'
                   END cursts
              FROM t408_dhr t408,
                   t402_work_order t402,
                   t406_payment t406,
                   t405_vendor_pricing_details t405
             WHERE t408.c402_work_order_id = t402.c402_work_order_id
               AND t406.c406_invoice_id =
                      DECODE (p_invoiceid,
                              NULL, t406.c406_invoice_id,
                              p_invoiceid
                             )                                        --'1816'
               AND t408.c408_dhr_id =
                      DECODE (p_dhrid,
                              NULL, t408.c408_dhr_id,
                              p_dhrid
                             )                                 --'GM-DHR-6571'
               AND t408.c301_vendor_id =
                      DECODE (p_vendorid,
                              0, t408.c301_vendor_id,
                              p_vendorid
                             )                                             --7
               AND t406.c406_invoice_dt >=
                      DECODE (p_frmdt,
                              NULL, t406.c406_invoice_dt,
                              TO_DATE (p_frmdt, v_dateformat)
                             )
               AND t406.c406_invoice_dt <=
                      DECODE (p_todt,
                              NULL, t406.c406_invoice_dt,
                              TO_DATE (p_todt, v_dateformat)
                             )
               AND t402.c402_void_fl IS NULL
               AND t408.c408_void_fl IS NULL
               AND t405.c405_void_fl IS NULL
               AND t405.c405_pricing_id = t402.c405_pricing_id
               AND t408.c406_payment_id = t406.c406_payment_id
          ORDER BY ID ASC;
   END gm_ac_fch_invc_dhr_dtls;

   /*******************************************************
    * Description : Procedure to update invoice status
    * Author      : Basudev Vidyasankar
    *******************************************************/
   PROCEDURE gm_ac_update_invc_status (
      p_payment_id   IN   t406_payment.c406_payment_id%TYPE,
      p_dhr_id       IN   t408_dhr.c408_dhr_id%TYPE
   )
   AS
      v_payment_id       NUMBER;
      v_amount           NUMBER;
      v_dhr_cnt          NUMBER;
      v_invoice_amount   NUMBER;
   BEGIN
      IF (p_payment_id IS NULL)
      THEN
    
         SELECT c406_payment_id
           INTO v_payment_id
           FROM t408_dhr
          WHERE c408_dhr_id = p_dhr_id
          AND c408_void_fl IS NULL;  
          /* In existing scenario, when we void a DHR, payment id is updated to null. Here after  We going to keep payment id in voided DHR for PMT-2600.
          * So, whenever payment id is used to pull DHR the void flag check must required. 
          */
         IF v_payment_id IS NULL
         THEN
            RETURN;
         END IF;
      END IF;

      IF p_payment_id IS NOT NULL
      THEN
         v_payment_id := p_payment_id;
      END IF;

      SELECT COUNT (*)
        INTO v_dhr_cnt
        FROM t408_dhr
       WHERE c406_payment_id = v_payment_id
         AND c408_status_fl < 4
         AND c408_void_fl IS NULL;

      IF v_dhr_cnt > 0
      THEN
         RETURN;
      END IF;

      SELECT t406.c406_payment_amount
        INTO v_invoice_amount
        FROM t406_payment t406
       WHERE t406.c406_payment_id = v_payment_id AND t406.c406_void_fl IS NULL;

      v_amount := get_invc_ready_payment_amount (v_payment_id);

      IF (v_invoice_amount = v_amount)
      THEN
         UPDATE t406_payment
            SET c406_status_fl = 15,
                c406_ready_payment_dt = CURRENT_DATE
          WHERE c406_payment_id = v_payment_id AND c406_void_fl IS NULL;
      -- 10 - Ready for payment
      -- 15 - Released for payment , changed here from 10 to 15, skip the status 'Ready for payment'.
      ELSE
         UPDATE t406_payment
            SET c406_status_fl = 5
          WHERE c406_payment_id = v_payment_id AND c406_void_fl IS NULL;
      -- 5 - Invoice Incomplete
      END IF;
      		EXCEPTION WHEN NO_DATA_FOUND THEN
 		RETURN;
   END gm_ac_update_invc_status;

/*******************************************************
 * Purpose: function is used to get invoice status
   author Xun
 *******************************************************/
--
   FUNCTION get_invoice_status (
      p_statusflag   IN   t406_payment.c406_status_fl%TYPE
   )
      RETURN VARCHAR2
   IS
      v_status_name   VARCHAR2 (100);
   BEGIN
      SELECT DECODE (p_statusflag,
                     0, 'Invoice Entered',
                     5, 'Invoice Incomplete',
                     10, 'Hold',
                     15, 'Released for Payment',
                     20, 'Released for Download',
                     30, 'Downloaded'
                    )
        INTO v_status_name
        FROM DUAL;

      RETURN v_status_name;
   END get_invoice_status;

/******************************************************************
   * Description : Function to get DHR total amount
   ****************************************************************/
   FUNCTION get_dhr_amount (p_payment_id IN t406_payment.c406_payment_id%TYPE)
      RETURN NUMBER
   IS
      v_dhr_amount   NUMBER (15, 2);
   BEGIN
      SELECT SUM (  t408.c408_qty_on_shelf
                  * get_work_order_ea_cost (t408.c402_work_order_id,
                                            t408.c901_type
                                           )
                 )
        INTO v_dhr_amount
        FROM t408_dhr t408
       WHERE t408.c406_payment_id = p_payment_id
         AND t408.c408_dhr_id IS NOT NULL
         AND t408.c408_void_fl IS NULL;  
         /* In existing scenario, when we void a DHR, payment id is updated to null. Here after  We going to keep payment id in voided DHR for PMT-2600.
          * So, whenever payment id is used to pull DHR the void flag check must required. 
          */

      RETURN v_dhr_amount;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN 0;
   END get_dhr_amount;
END gm_pkg_ac_payment;
/
