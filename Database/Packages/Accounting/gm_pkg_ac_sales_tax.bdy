/* Formatted on 2009/09/17 10:48 (Formatter Plus v4.8.0) */
--@"C:\Database\Packages\Accounting\gm_pkg_ac_sales_tax.bdy";
CREATE OR REPLACE
PACKAGE BODY gm_pkg_ac_sales_tax
IS
    /*****************************************************************************************
    * Description : Procedure to fetch the order item details
    ******************************************************************************************/
PROCEDURE gm_fch_order_item_dtls (
        p_order_id IN t501_order.c501_order_id%TYPE,
        p_out_item_dtls OUT TYPES.cursor_type)
AS
v_company_id        t1900_company.c1900_company_id%TYPE;
BEGIN
	--
	 SELECT NVL (get_compid_frm_cntx (), get_rule_value ('DEFAULT_COMPANY', 'ONEPORTAL'))
	   INTO v_company_id
	   FROM dual;
      --
    OPEN p_out_item_dtls FOR SELECT t502.c502_item_order_id item_order_id, t502.C205_PART_NUMBER_ID PNUM,
    GET_PARTNUM_DESC (t502.C205_PART_NUMBER_ID) PDESC, t502.c502_item_qty QTY, NVL (GET_CUSTOM_PARTNUM (
    t501.C704_ACCOUNT_ID, t502.C205_PART_NUMBER_ID), '-9999') CUST_PNUM, NVL (GET_CUSTOM_PARTDESC (t501.C704_ACCOUNT_ID
    , t502.C205_PART_NUMBER_ID), '-9999') CUST_PDESC, t502.C502_ITEM_PRICE PRICE, t502.c901_type TYPE,
t502.C502_CONSTRUCT_FL CONFL, get_part_attr_value_by_comp (t502.C205_PART_NUMBER_ID, '200001', v_company_id) NTFTCODE,
t501.C501_SHIP_COST SCOST, t501.C501_ORDER_DATE ODT, NVL (t502.c502_vat_rate, 0) VAT, NVL (t502.C502_Control_Number, ''
) ControlNum, get_part_attribute_value (t502.C205_PART_NUMBER_ID, '101195') taxcode, get_partnum_product (
t502.C205_PART_NUMBER_ID) itemcode FROM t501_order t501, t502_item_order t502 WHERE t501.c501_order_id =
t502.c501_order_id AND t501.c501_order_id                                                              = p_order_id AND
t501.c501_delete_fl                                                                                   IS NULL AND
t501.c501_void_fl                                                                                     IS NULL AND
t502.c502_delete_fl                                                                                   IS NULL AND
t502.c502_void_fl                                                                                     IS NULL ORDER BY
t502.c205_part_number_id ;
END gm_fch_order_item_dtls;
/*****************************************************************************************
* Description : Procedure to update the VAT rate to item order table
******************************************************************************************/
PROCEDURE gm_upd_order_item_vat_rate (
        p_invoice_id IN T503_INVOICE.C503_INVOICE_ID%TYPE,
        p_inputstr   IN CLOB,
        p_user_id    IN T503_INVOICE.C503_LAST_UPDATED_BY%TYPE)
AS
    v_strlen NUMBER := NVL (LENGTH (p_inputstr), 0) ;
    v_string CLOB   := p_inputstr;
    v_substring VARCHAR2 (4000) ;
    --
    v_item_ord_id t502_item_order.c502_item_order_id%TYPE;
    v_vat_rate t502_item_order.c502_vat_rate%TYPE;
    v_tax_amt t502_item_order.c502_tax_amt%TYPE;
    
    --
BEGIN
    IF v_strlen                      > 0 THEN
        WHILE INSTR (v_string, '|') <> 0
        LOOP
            v_substring   := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
            v_string      := SUBSTR (v_string, INSTR (v_string, '|')    + 1) ;
            v_item_ord_id := NULL;
            v_vat_rate    := NULL;
            v_tax_amt	  := NULL;
            v_item_ord_id := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            v_substring   := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
            v_vat_rate    := TO_NUMBER (SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1));
            v_substring   := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
            v_tax_amt     := TO_NUMBER (v_substring) ;
            --
             UPDATE t502_item_order
            SET c502_vat_rate          = v_vat_rate, c502_tax_amt = v_tax_amt
              WHERE c502_item_order_id = v_item_ord_id;
        END LOOP;
        -- to update the tax flag
        gm_pkg_ac_sales_tax.gm_upd_inv_tax_fl (p_invoice_id, 'Y', p_user_id) ;
        -- to update orders tax cost
        gm_pkg_ac_sales_tax.gm_upd_orders_tax_cost (p_invoice_id, p_user_id) ;
    END IF;
END gm_upd_order_item_vat_rate;
/*******************************************************
* Description : Get the invoice tax flag
* Author   : Mani
*******************************************************/
FUNCTION get_invoice_tax_fl (
        p_invoice_id IN T503_INVOICE.C503_INVOICE_ID%TYPE)
    RETURN VARCHAR2
IS
    v_tax_fl VARCHAR2 (10) ;
BEGIN
    --
    BEGIN
         SELECT c503_tax_fl
           INTO v_tax_fl
           FROM t503_invoice
          WHERE c503_invoice_id = p_invoice_id;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_tax_fl := NULL;
    END;
    RETURN v_tax_fl;
END get_invoice_tax_fl;
/*******************************************************
* Description : Get the Account shipping country id
* Author   : Mani
*******************************************************/
FUNCTION get_tax_country_fl (
        p_acc_id IN T501_ORDER.C704_ACCOUNT_ID%TYPE)
    RETURN VARCHAR2
AS
    v_tax_country VARCHAR2 (10) ;
BEGIN
    --
    BEGIN
         SELECT get_rule_value_by_company (C704_SHIP_COUNTRY, 'TAX_COUNTRY', c1900_company_id)
           INTO v_tax_country
           FROM T704_ACCOUNT
          WHERE C704_ACCOUNT_ID = p_acc_id;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_tax_country := NULL;
    END;
    RETURN v_tax_country;
END get_tax_country_fl;
/*****************************************************************************************
* Description : Procedure to fetch taxable country invoice details
******************************************************************************************/
PROCEDURE gm_fch_tax_invoice_dtls (
        p_invoice_id IN t501_order.c503_invoice_id%TYPE,
        p_out_tax_dtls OUT TYPES.cursor_type)
AS
v_company_id        t1900_company.c1900_company_id%TYPE;
BEGIN
	SELECT get_compid_frm_cntx() 
      INTO v_company_id  
      FROM dual;
      
    OPEN p_out_tax_dtls FOR SELECT t503.c901_invoice_type invtype,
    get_code_name (t704.c901_company_id) companyname,
    t704.c704_ship_add1 acc_ship_add1,
    t704.c704_ship_add2 acc_ship_add2,
    t704.c704_ship_attn_to acc_ship_attn,
    t704.c704_ship_city acc_ship_city,
    t704.c704_ship_country acc_ship_country_id,
    get_code_name_alt (t704.c704_ship_country) acc_ship_country_nm,
    t704.c704_ship_state acc_ship_state_id,
    get_code_name_alt (t704.c704_ship_state) acc_ship_state_nm,
    t704.c704_ship_zip_code acc_ship_zip_code,
    t704.c704_ship_latitude acc_ship_latitude,
    t704.c704_ship_longitude acc_ship_longitude,
    TO_CHAR (t503.c503_created_date, 'YYYY-mm-dd') docdate,
    t704.c704_ship_name acc_ship_name,
    t503.c503_tax_fl tax_fl,
    GET_ACCOUNT_ATTRB_VALUE (t503.c704_account_id, 101191) valid_add_fl,
    t704.c704_account_id accid,
    t503.c503_invoice_id invid,
    --get_rule_value (t704.c704_ship_country, 'TAX_COUNTRY') taxable_country_fl,
    --Avatax should only be calculated for US only i.e Globus Medical North America
    get_rule_value_by_company(t704.c704_ship_country,'TAX_COUNTRY',v_company_id) taxable_country_fl,
    CASE
    WHEN TRUNC (to_date (get_rule_value ('TAX_DATE', 'TAX_INVOICE'), 'mm/DD/YYYY')) <= TRUNC (
        gm_pkg_ac_sales_tax.get_min_orders_date (t503.c503_invoice_id)) THEN
        'Y'
    ELSE
        'N'
    END tax_date_fl,
    DECODE (t503.c901_invoice_type, 50202, 'ReturnInvoice', 'SalesInvoice') docType,
    get_rule_value ('TAX_COMP_NM', 'TAX_INVOICE') tax_company,
    t503.c503_customer_po customer_po,
    gm_pkg_ac_sales_tax.get_parent_ord_tax_cost (t503.c503_invoice_id) parent_ord_tax_cost,
    t503.c503_void_fl inv_void_fl,
    get_rule_value_by_company(t704.c1900_company_id,'AVATAX',v_company_id) company_avatax_fl
    FROM t503_invoice t503,
    t704_account t704 WHERE t503.c503_invoice_id = p_invoice_id AND t503.c704_account_id = t704.c704_account_id AND
    t704.c704_void_fl                           IS NULL;
END gm_fch_tax_invoice_dtls;
/*******************************************************
* Description : Based on invoice get the min orders date
* Author   : Mani
*******************************************************/
FUNCTION get_min_orders_date (
        p_invoice_id IN t501_order.c503_invoice_id%TYPE)
    RETURN DATE
AS
    v_order_date t501_order.c501_order_date%TYPE;
BEGIN
    --
    BEGIN
        --
         SELECT MIN (c501_order_date)
           INTO v_order_date
           FROM t501_order
          WHERE C501_STATUS_FL                    IS NOT NULL
            AND C501_VOID_FL                      IS NULL
            AND NVL (C901_order_type, - 9999) NOT IN ('2535', '101260')
            AND NOT EXISTS
            (
                 SELECT t906.c906_rule_value
                   FROM t906_rules t906
                  WHERE t906.c906_rule_grp_id         = 'ORDTYPE'
                    AND c906_rule_id                  = 'EXCLUDE'
                    AND NVL (c901_order_type, - 9999) = t906.c906_rule_value
            )
            AND c501_order_id IN
            (
                 SELECT NVL (c501_parent_order_id, c501_order_id)
                   FROM t501_order
                  WHERE c503_invoice_id = p_invoice_id
            ) ;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_order_date := NULL;
    END;
    RETURN v_order_date;
END get_min_orders_date;
/*******************************************************
* Description : Get the code id from code group and alt
* Author   : Mani
*******************************************************/
FUNCTION get_code_id_from_code_alt (
        p_code_grp IN t901_code_lookup.c901_code_grp%TYPE,
        p_code_alt IN t901_code_lookup.c902_code_nm_alt%TYPE)
    RETURN VARCHAR2
AS
    v_code_id VARCHAR2 (20) ;
BEGIN
    --
    BEGIN
         SELECT c901_code_id
           INTO v_code_id
           FROM t901_code_lookup
          WHERE c901_code_grp    = p_code_grp
            AND c902_code_nm_alt = p_code_alt
            AND c901_active_fl   = 1
            AND c901_void_fl    IS NULL;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_code_id := NULL;
    END;
    RETURN v_code_id;
END get_code_id_from_code_alt;
/*****************************************************************************************
* Description : Procedure to update the invoice tax flag
******************************************************************************************/
PROCEDURE gm_upd_inv_tax_fl (
        p_invoice_id IN t501_order.c503_invoice_id%TYPE,
        p_flag       IN VARCHAR2,
        p_user_id    IN t503_invoice.c503_last_updated_by%TYPE)
AS
BEGIN
    --
     UPDATE t503_invoice
    SET c503_tax_fl         = p_flag, C503_LAST_UPDATED_BY = p_user_id, C503_LAST_UPDATED_DATE = SYSDATE
      WHERE c503_invoice_id = p_invoice_id;
END gm_upd_inv_tax_fl;
/*****************************************************************************************
* Description : Procedure to update orders tax cost
******************************************************************************************/
PROCEDURE gm_upd_orders_tax_cost (
        p_invoice_id IN t501_order.c503_invoice_id%TYPE,
        p_user_id    IN t501_order.c501_last_updated_by%TYPE)
AS
    v_tax_toatal NUMBER (15, 2) ;
    --
    CURSOR sales_order
    IS
         SELECT C501_ORDER_ID ID
           FROM T501_ORDER
          WHERE C503_INVOICE_ID                    = p_invoice_id
            AND C501_STATUS_FL                    IS NOT NULL
            AND c501_delete_fl                    IS NULL
            AND c501_void_fl                      IS NULL
            AND NVL (C901_order_type, - 9999) NOT IN ('2535', '101260')
            AND NOT EXISTS
            (
                 SELECT t906.c906_rule_value
                   FROM t906_rules t906
                  WHERE t906.c906_rule_grp_id         = 'ORDTYPE'
                    AND c906_rule_id                  = 'EXCLUDE'
                    AND NVL (c901_order_type, - 9999) = t906.c906_rule_value
            )
   ORDER BY ID ;
BEGIN
    FOR order_cur IN sales_order
    LOOP
         SELECT SUM(t502.c502_tax_amt)
           INTO v_tax_toatal
           FROM t501_order t501, t502_item_order t502
          WHERE t501.c501_order_id   = t502.c501_order_id
            AND t501.c501_order_id   = order_cur.ID
            AND T502.C502_DELETE_FL IS NULL
            AND T502.C502_VOID_FL   IS NULL;
        --
         UPDATE t501_order
        SET c501_tax_total_cost = v_tax_toatal, c501_last_updated_by = p_user_id, c501_last_updated_date = SYSDATE
          WHERE c501_order_id   = order_cur.ID;
        	
    END LOOP;
    --PMT25905 Save Invoice and Payment Amount
    -- In this procedure we are updating the c501_tax_total_cost column, and this column is not used in the gm_pkg_ac_invoice_txn.gm_sav_invoice_amount 
    --, this procedure call is not required,hence commenting it.
	
	-- PMT-40358 : Tax amount is blank in invoice list report.
	gm_pkg_ac_invoice_txn.gm_sav_invoice_amount(p_invoice_id,p_user_id); 
	
END gm_upd_orders_tax_cost;
/*****************************************************************************************
* Description : Procedure to void the invoices
******************************************************************************************/
PROCEDURE gm_void_tax_error_invoice (
        p_invoice_id IN t501_order.c503_invoice_id%TYPE,
        p_comments   IN VARCHAR2,
        p_user_id    IN t501_order.c501_last_updated_by%TYPE)
AS
    v_out_void_msg VARCHAR2 (4000) ;
    v_out_log_msg  VARCHAR2 (4000) ;
    v_cust_po t503_invoice.c503_customer_po%TYPE;
BEGIN
    -- Voiding the invoices
    gm_pkg_common_cancel.gm_ac_sav_void_invoice (p_invoice_id, p_user_id, v_out_void_msg) ;
    -- update the comments.
     SELECT c503_customer_po
       INTO v_cust_po
       FROM t503_invoice
      WHERE c503_invoice_id = p_invoice_id;
    -- 90196 Void Invoice
    -- 101197 Tax Error (Reason)
    -- to save the cancel log
    gm_pkg_common_cancel.gm_sav_cancel_log ('', p_invoice_id, v_cust_po ||': '||p_comments, 101197, 90196, p_user_id)
    ;
    --
END gm_void_tax_error_invoice;
/*****************************************************************************************
* Description : Procedure to roll back the return details
******************************************************************************************/
PROCEDURE gm_rollback_return (
        p_rma_id  IN t506_returns.c506_rma_id%TYPE,
        p_user_id IN t506_returns.c506_last_updated_by%TYPE)
AS
BEGIN
     UPDATE t506_returns
    SET c503_invoice_id      = NULL, c506_credit_memo_id = NULL, c506_ac_credit_dt = NULL
      , c506_last_updated_by = p_user_id, c506_last_updated_date = SYSDATE
      WHERE c506_rma_id      = p_rma_id
        AND c506_void_fl    IS NULL;
    --
     UPDATE t501_order
    SET c506_rma_id     = NULL, c501_last_updated_by = p_user_id, c501_last_updated_date = SYSDATE
      WHERE c506_rma_id = p_rma_id;
END gm_rollback_return;
/*****************************************************************************************
* Description : Procedure to update valid shipping address to temp table
******************************************************************************************/
PROCEDURE gm_upd_temp_ship_info (
        p_inputstr IN CLOB,
        p_user_id  IN T503_INVOICE.C503_LAST_UPDATED_BY%TYPE)
AS
    v_strlen NUMBER := NVL (LENGTH (p_inputstr), 0) ;
    v_string CLOB   := p_inputstr;
    v_substring VARCHAR2 (4000) ;
    --
    v_acc_id NUMBER;
    v_add1 T704_ACCOUNT.C704_SHIP_ADD1%TYPE;
    v_ship_city T704_ACCOUNT.c704_ship_city%TYPE;
    v_ship_state T704_ACCOUNT.C704_SHIP_STATE%TYPE;
    v_ship_country T704_ACCOUNT.C704_SHIP_COUNTRY%TYPE;
    v_ship_zip T704_ACCOUNT.C704_SHIP_ZIP_CODE%TYPE;
    v_flag VARCHAR2 (20) ;
BEGIN
    IF v_strlen                      > 0 THEN
        WHILE INSTR (v_string, '|') <> 0
        LOOP
            v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
            v_string    := SUBSTR (v_string, INSTR (v_string, '|')    + 1) ;
            --
            v_acc_id       := NULL;
            v_add1         := NULL;
            v_ship_city    := NULL;
            v_ship_state   := NULL;
            v_ship_country := NULL;
            v_ship_zip     := NULL;
            v_flag         := NULL;
            --
            v_acc_id       := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            v_substring    := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
            v_add1         := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            v_substring    := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
            v_ship_city    := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            v_substring    := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
            v_ship_state   := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            v_substring    := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
            v_ship_country := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            v_substring    := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
            v_ship_zip     := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            v_substring    := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
            v_flag         := v_substring;
            --
            IF v_flag = 'Y' THEN
                 UPDATE t704_account_temp
                SET c704_ship_add1               = v_add1, c704_ship_city = v_ship_city, c704_ship_state = v_ship_state
                  , c704_ship_country            = v_ship_country, c704_ship_zip_code = v_ship_zip, c704_ship_add_valid_fl =
                    v_flag, c704_last_updated_by = p_user_id, c704_last_updated_date = SYSDATE
                  WHERE c704_account_id          = v_acc_id;
            ELSE
                 UPDATE t704_account_temp
                SET c704_ship_add_valid_fl = v_flag, c704_last_updated_by = p_user_id, c704_last_updated_date = SYSDATE
                  WHERE c704_account_id    = v_acc_id;
            END IF;
        END LOOP;
    END IF;
END gm_upd_temp_ship_info;
/*****************************************************************************************
* Description : Procedure to fetch account details
******************************************************************************************/
PROCEDURE gm_fch_account_dtls (
        p_out_acc_dtls OUT TYPES.cursor_type)
AS
BEGIN
    OPEN p_out_acc_dtls FOR SELECT c704_account_id ID,
    c704_ship_add1 SHIPADD1,
    c704_ship_add2 SHIPADD2,
    c704_ship_city SHIPCITY,
    get_code_name_alt (c704_ship_country) SHIPCOUNTRY,
    get_code_name_alt (c704_ship_state) SHIPSTATE,
    c704_ship_zip_code SHIPZIP,
    get_account_attrb_value (c704_account_id, 101191) add_verified_fl FROM t704_account WHERE c704_ship_country = 1101
    AND c704_void_fl                                                                                           IS NULL;
END gm_fch_account_dtls;
/*******************************************************
* Description : Get the exemption flag from order table
* Author   : Mani
*******************************************************/
FUNCTION get_parent_ord_tax_cost (
        p_invoice_id IN t501_order.c503_invoice_id%TYPE)
    RETURN NUMBER
AS
    v_tax_cost NUMBER (14, 2) ;
BEGIN
    --
    BEGIN
         SELECT SUM (c501_tax_total_cost)
           INTO v_tax_cost
           FROM t501_order
          WHERE c501_order_id in
            (
                 SELECT c501_parent_order_id
                   FROM t501_order
                  WHERE c503_invoice_id       = p_invoice_id
                    AND c501_delete_fl       IS NULL
                    AND c501_void_fl         IS NULL
                    AND c501_parent_order_id IS NOT NULL
            )
            AND c501_delete_fl IS NULL
            AND c501_void_fl   IS NULL;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_tax_cost := NULL;
    END;
    RETURN v_tax_cost;
END get_parent_ord_tax_cost;
END gm_pkg_ac_sales_tax;
/
