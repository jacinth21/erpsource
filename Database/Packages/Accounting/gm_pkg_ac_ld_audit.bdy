/* Formatted on 2009/09/21 17:23 (Formatter Plus v4.8.0) */
--@"C:\Database\Packages\Accounting\gm_pkg_ac_ld_audit.bdy";
-- exec gm_pkg_ac_ld_audit.gm_ac_ld_main(229, 1, 303149);

CREATE OR REPLACE PACKAGE BODY gm_pkg_ac_ld_audit
IS

--
	/****************************************************************
	 * Main procedure which will call other procedures to load all the
	 * Audit Entry Details
	 ***************************************************************/
--
	PROCEDURE gm_ac_ld_main (
		p_dist_id	 IN   t701_distributor.c701_distributor_id%TYPE
	  , p_audit_id	 IN   t2650_field_sales_audit.c2650_field_sales_audit_id%TYPE
	  , p_user_id	 IN   t2656_audit_entry.c2656_created_by%TYPE
	)
	AS
		v_start_tag_range NUMBER;
		v_end_tag_range NUMBER;
		v_details_id   t2656_audit_entry.c2651_field_sales_details_id%TYPE;
		v_audit_user_id t2656_audit_entry.c2656_counted_by%TYPE;
		v_start_dt	   DATE;
		v_released_count NUMBER := 0;
		v_tag_entry_count NUMBER := 0;
		v_used_count   NUMBER := 0;
		v_error_tag_range VARCHAR2 (500);
		v_success_tag_range VARCHAR2 (500);

		CURSOR team_details_cur
		IS
			SELECT t2651.c2651_field_sales_details_id ID, t2658.c2658_audit_user_id user_id
				 , t2658.c2658_tag_start_range start_range, t2658.c2658_tag_end_range end_range
			  FROM t2651_field_sales_details t2651, t2658_audit_team t2658
			 WHERE t2651.c2651_field_sales_details_id = t2658.c2651_field_sales_details_id
			   AND t2651.c701_distributor_id = p_dist_id
			   AND t2651.c2650_field_sales_audit_id = p_audit_id
			   AND t2651.c2651_void_fl IS NULL
			   AND t2658.c2658_void_fl IS NULL;

		CURSOR tag_cur
		IS
			SELECT c5010_tag_id ID
			  FROM t5010_tag t5010
			 WHERE t5010.c5010_tag_id >= v_start_tag_range
			   AND t5010.c5010_tag_id <= v_end_tag_range
			   AND t5010.c5010_void_fl IS NULL
			   AND t5010.c901_status = 51010
			   AND t5010.c5010_lock_fl IS NULL;
	BEGIN
		--
		v_start_dt	:= SYSDATE;

		--
		FOR team_details IN team_details_cur
		LOOP
			v_start_tag_range := team_details.start_range;
			v_end_tag_range := team_details.end_range;
			v_details_id := team_details.ID;
			v_audit_user_id := team_details.user_id;

			-- Check if the tag is already released and used
			SELECT COUNT (1)
			  INTO v_used_count
			  FROM t5010_tag t5010
			 WHERE t5010.c5010_tag_id >= v_start_tag_range
			   AND t5010.c5010_tag_id <= v_end_tag_range
			   AND t5010.c5010_void_fl IS NULL
			   AND t5010.c901_status IN (51012)
			   AND t5010.c5010_lock_fl IS NULL
			   AND t5010.c205_part_number_id IS NOT NULL
			   AND t5010.c5010_control_number IS NOT NULL;

			IF v_used_count = 0
			THEN
				-- Check if the tag is already released
				SELECT COUNT (1)
				  INTO v_released_count
				  FROM t5010_tag t5010
				 WHERE t5010.c5010_tag_id >= v_start_tag_range
				   AND t5010.c5010_tag_id <= v_end_tag_range
				   AND t5010.c5010_void_fl IS NULL
				   AND t5010.c901_status IN (51011)
				   AND t5010.c5010_lock_fl IS NULL;

				-- If it is already released and it's not locked then revert the tag back to available
				IF v_released_count > 0
				THEN
					UPDATE t5010_tag t5010
					   SET c901_status = 51010	 -- Available
						 , c5010_last_updated_by = p_user_id
						 , c901_location_type = ''
						 , c5010_location_id = ''
						 , c5010_last_updated_date = SYSDATE
					 WHERE t5010.c5010_tag_id >= v_start_tag_range
					   AND t5010.c5010_tag_id <= v_end_tag_range
					   AND t5010.c5010_void_fl IS NULL
					   AND t5010.c901_status IN (51011)
					   AND t5010.c5010_lock_fl IS NULL;
				END IF;

				FOR tag IN tag_cur
				LOOP
					SELECT COUNT (1)
					  INTO v_tag_entry_count
					  FROM t2656_audit_entry t2656
					 WHERE t2656.c5010_tag_id = tag.ID
					   AND c2656_counted_by = v_audit_user_id
					   AND c2651_field_sales_details_id = v_details_id;

					IF v_tag_entry_count = 0
					THEN
						INSERT INTO t2656_audit_entry
									(c2656_audit_entry_id, c2651_field_sales_details_id, c5010_tag_id
								   , c2656_counted_by, c2656_created_by, c2656_created_date
									)
							 VALUES (s2656_audit_entry.NEXTVAL, v_details_id, tag.ID
								   , v_audit_user_id, p_user_id, SYSDATE
									);
					END IF;
				END LOOP;

				UPDATE t5010_tag t5010
				   SET c901_status = 51011	 -- Released
					 , c901_location_type = 4120
					 , c5010_location_id = p_dist_id
					 , c5010_last_updated_by = p_user_id
					 , c5010_last_updated_date = SYSDATE
				 WHERE t5010.c5010_tag_id >= v_start_tag_range
				   AND t5010.c5010_tag_id <= v_end_tag_range
				   AND t5010.c5010_void_fl IS NULL
				   AND t5010.c901_status = 51010
				   AND t5010.c5010_lock_fl IS NULL;

				v_success_tag_range := v_success_tag_range || v_start_tag_range || ' - ' || v_end_tag_range || ' , ';
			ELSE
				v_error_tag_range := v_error_tag_range || v_start_tag_range || ' - ' || v_end_tag_range || ' , ';
			END IF;
		END LOOP;

		INSERT INTO t2656_audit_entry
					(c2656_audit_entry_id, c2651_field_sales_details_id, c5010_tag_id, c205_part_number_id
				   , c2656_control_number, c207_set_id, c2656_created_by, c2656_created_date)
			(SELECT s2656_audit_entry.NEXTVAL, t2651.c2651_field_sales_details_id, t5010.c5010_tag_id
				  , t5010.c205_part_number_id, t5010.c5010_control_number, t5010.c207_set_id, p_user_id, SYSDATE
			   FROM t5010_tag t5010, t2651_field_sales_details t2651
			  WHERE t5010.c901_location_type = 5020   -- Distributor
				AND c5010_location_id = p_dist_id
				AND t2651.c701_distributor_id = c5010_location_id
				AND t2651.c2650_field_sales_audit_id = p_audit_id
				AND t5010.c901_trans_type = 51000	-- Consignment
				AND t5010.c5010_void_fl IS NULL
				AND t5010.c5010_lock_fl IS NULL);

	
		IF v_error_tag_range IS NOT NULL
		THEN
			DBMS_OUTPUT.put_line ('THE FOLLOWING TAG RANGE HAS ISSUES: ' || v_error_tag_range);
			DBMS_OUTPUT.put_line ('THE FOLLOWING TAG RANGE ARE UPDATED SUCCESSFULLY :  ' || v_success_tag_range);
		END IF;
	--
	END gm_ac_ld_main;
END gm_pkg_ac_ld_audit;
/
