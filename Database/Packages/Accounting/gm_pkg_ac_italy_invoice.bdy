/* Formatted on 2009/09/17 10:48 (Formatter Plus v4.8.0) */
-- @"C:\database\Packages\Accounting\gm_pkg_ac_italy_invoice.pkg"

CREATE OR REPLACE PACKAGE body gm_pkg_ac_italy_invoice
IS
--	
/********************************************************************
* Description : This is procedure to get vendita id from t503c table
* 				Using Invoice ID.
* Author	  : Prabhuvigneshwaran			
*************************************************************************/
FUNCTION get_vendita_for_invoice (
        p_invoiceid   IN	t503a_invoice_txn.c503_invoice_id%TYPE,
        p_compid      IN    t5034_proforma.c1900_company_id%TYPE
    ) RETURN t5034_proforma.c5034_proforma_no%TYPE IS
    	v_out_vendita_id t5034_proforma.c5034_proforma_no%TYPE;
	BEGIN
    	SELECT
    			c5034_proforma_no
  		  INTO  v_out_vendita_id
		  FROM
    			t5034_proforma
		 WHERE
    			c5034_proforma_id 
    	    IN (
    	    SELECT
            	  t5035.c5034_proforma_id
        	FROM
            	  t5034_proforma       t5034,
            	  t5035_proforma_order_map   t5035,
            	  t501_order          t501
       		 WHERE t5034.c5034_proforma_id = t5035.c5034_proforma_id
              AND  t5035.c501_order_id = t501.c501_order_id
 		       AND t501.c503_invoice_id      = p_invoiceid
 			   AND t5034.c1900_company_id    = p_compid
 			   AND  t5034.c5034_genrated_fl = 'Y'
               AND t501.c501_void_fl IS NULL
               AND t5034.c5034_void_fl IS NULL
               AND t5035.c5035_void_fl IS NULL
--        ORDER BY t5035.c5034_proforma_id DESC
				)
    		   AND ROWNUM = 1;
    		   RETURN v_out_vendita_id;
    	EXCEPTION WHEN OTHERS THEN
        RETURN '';
END get_vendita_for_invoice;
END gm_pkg_ac_italy_invoice;
/
