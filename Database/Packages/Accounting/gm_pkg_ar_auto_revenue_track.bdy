

CREATE OR REPLACE PACKAGE BODY gm_pkg_ar_auto_revenue_track
IS
/***********************************************************************
* Description : Procedure used to fetch iPad orders are not processed by
* AutoRevenue job
* Author : Mahavishnu
************************************************************************/
PROCEDURE gm_fch_revenue_pending_orders(
    p_company_id IN t1900_company.c1900_company_id%TYPE,
    p_out_ipad_ord_cur OUT TYPES.cursor_type )
AS
BEGIN
	OPEN p_out_ipad_ord_cur FOR 
		  SELECT c501_order_id ordid
			FROM t501_order 
			WHERE nvl(c901_order_type,'-999') NOT IN ( SELECT t906.c906_rule_value
					 FROM t906_rules t906 
					 	WHERE t906.c906_rule_grp_id = 'ORDTYPE' 
					 		AND c906_rule_id= 'EXCLUDE')
				AND  c501_receive_mode = 26230683  -- 26230683 iPad
				AND  c501_void_fl IS NULL
				AND  c501_revenue_track_process_fl IS NULL
				AND  c501_parent_order_id IS NULL
				AND  c1900_company_id = p_company_id;

		  
END gm_fch_revenue_pending_orders;


/***********************************************************************
* Description : Procedure used to save extract iPad DO pdf details
* Author : Mahavishnu
************************************************************************/
PROCEDURE gm_sav_scan_revenue_dtl(
	p_inputstr IN CLOB)
AS
v_json 	 json_object_t;	
v_ord_id 		t5005_ar_revenue_scan_dtls.c501_order_id%TYPE;
v_category 		t5005_ar_revenue_scan_dtls.c901_category%TYPE;
v_pat_img_fl	t5005_ar_revenue_scan_dtls.c5005_patient_sticker_img_fl%TYPE;
v_pat_valid_fl	t5005_ar_revenue_scan_dtls.c5005_patient_sticker_valid_fl%TYPE;
v_sign_fl		t5005_ar_revenue_scan_dtls.c5005_signed_fl%TYPE;
v_price_fl		t5005_ar_revenue_scan_dtls.c5005_pricing_fl%TYPE;
v_po_num		t5005_ar_revenue_scan_dtls.C5005_PO_NUMBER%TYPE;
v_po_fl			t5005_ar_revenue_scan_dtls.C5005_PO_FL%TYPE;
v_company 		t5005_ar_revenue_scan_dtls.c1900_company_id%TYPE;
v_contract_fl   t704a_account_attribute.c704a_attribute_value%TYPE;
BEGIN
	v_json   	 	:= json_object_t.parse(p_inputstr);
	v_ord_id  	 	:= v_json.get_String('ORDID');
	v_category	 	:= v_json.get_String('CATEGORY');
	v_pat_img_fl	:= v_json.get_String('PATSTICKIMG');
	v_pat_valid_fl	:= v_json.get_String('PATVALIDFL');
	v_sign_fl		:= v_json.get_String('SIGNFL');
	v_price_fl		:= v_json.get_String('PRICEFL');
	v_po_num		:= v_json.get_String('PONUM');
	v_po_fl			:= v_json.get_String('POFL');
	v_company 		:= v_json.get_String('COMPANYID');	
	
	IF v_price_fl = 'N' THEN
	
		BEGIN
					SELECT t704a.c704a_attribute_value  INTO v_contract_fl
					FROM t501_order t501,t704_account t704,t704a_account_attribute t704a
						WHERE 	t501.c704_account_id = t704.c704_account_id
							AND t704.c704_account_id = t704a.c704_account_id
							AND t704a.c901_attribute_type=903103 -- Contract
							AND t501.c501_void_fl is null
							AND t704.c704_void_fl is null
							AND t704a.c704a_void_fl is null
							AND t501.c501_order_id = v_ord_id;
					EXCEPTION WHEN NO_DATA_FOUND
					THEN
						v_contract_fl:='';
		END;
				
				IF v_contract_fl = '903100' THEN -- 903100 Contract
					v_price_fl := 'Y';
				END IF;
	
	END IF;
												
	gm_sav_extract_revenue_dtl(v_ord_id,v_category,v_pat_img_fl,v_pat_valid_fl
								,v_sign_fl,v_price_fl,v_po_num,v_po_fl
								,v_company,30301);
	
	-- Updating revenue process flag 
		gm_upd_revenue_process_fl(v_ord_id);
											
END gm_sav_scan_revenue_dtl;


/***********************************************************************
* Description : Procedure used to save/update extract iPad DO pdf details
* Author : Mahavishnu
************************************************************************/
PROCEDURE gm_sav_extract_revenue_dtl(
p_ord_id 		t5005_ar_revenue_scan_dtls.c501_order_id%TYPE,
p_category 		t5005_ar_revenue_scan_dtls.c901_category%TYPE,
p_pat_img_fl	t5005_ar_revenue_scan_dtls.c5005_patient_sticker_img_fl%TYPE,
p_pat_valid_fl	t5005_ar_revenue_scan_dtls.c5005_patient_sticker_valid_fl%TYPE,
p_sign_fl		t5005_ar_revenue_scan_dtls.c5005_signed_fl%TYPE,
p_price_fl		t5005_ar_revenue_scan_dtls.c5005_pricing_fl%TYPE,
p_po_num		t5005_ar_revenue_scan_dtls.C5005_PO_NUMBER%TYPE,
p_po_fl			t5005_ar_revenue_scan_dtls.C5005_PO_FL%TYPE,
p_company 		t5005_ar_revenue_scan_dtls.c1900_company_id%TYPE,
p_user_id       t5005_ar_revenue_scan_dtls.c5005_last_updated_by%TYPE
)
AS

BEGIN
	
	UPDATE t5005_ar_revenue_scan_dtls 
			SET c501_order_id=p_ord_id,c901_category=p_category,c5005_patient_sticker_img_fl=p_pat_img_fl,
				c5005_patient_sticker_valid_fl=p_pat_valid_fl,c5005_signed_fl=p_sign_fl,
				c5005_pricing_fl=p_price_fl,c5005_po_number=p_po_num,C5005_po_fl=p_po_fl,
				c5005_load_date = trunc(CURRENT_DATE),c1900_company_id=p_company,
				c5005_last_updated_by=p_user_id,c5005_last_updated_date=CURRENT_DATE
			 WHERE c501_order_id = p_ord_id
			 	AND c5005_void_fl IS NULL;
			 	
	   IF ( SQL%rowcount = 0 ) THEN
	   
	   	INSERT INTO t5005_ar_revenue_scan_dtls(c501_order_id,c901_category
											,c5005_patient_sticker_img_fl,c5005_patient_sticker_valid_fl
											,c5005_signed_fl,c5005_pricing_fl,c5005_po_number
											,C5005_PO_FL,c5005_load_date,c1900_company_id,c5005_last_updated_by
											,c5005_last_updated_date)
									values (p_ord_id,p_category,p_pat_img_fl,p_pat_valid_fl
											,p_sign_fl,p_price_fl,p_po_num,p_po_fl
											,trunc(CURRENT_DATE),p_company,p_user_id,CURRENT_DATE);
	   
	   END IF;
												
	
											
END gm_sav_extract_revenue_dtl;


/***********************************************************************
* Description : Procedure used to update revenue processed flag
* Author : Mahavishnu
************************************************************************/
PROCEDURE gm_upd_revenue_process_fl(
	p_order_id IN t501_order.c501_order_id%TYPE)
AS

BEGIN
	-- Updating revenue process flag 
		UPDATE t501_order set c501_revenue_track_process_fl='Y'
			WHERE (c501_order_id= p_order_id OR c501_parent_order_id = p_order_id)
 			AND c501_void_fl IS NULL;
											
END gm_upd_revenue_process_fl;


/***********************************************************************
* Description : Procedure used to save no pdf orders as unRecognized no pdf category
* Author : Mahavishnu
************************************************************************/
PROCEDURE gm_sav_no_pdf_revenue(
	 p_ord_ids   	IN  CLOB,
	 p_company_id	IN  t1900_company.c1900_company_id%TYPE)
AS

CURSOR cur_nopdf
		IS
     		SELECT token FROM v_clob_list 
					 WHERE token is not null;

BEGIN
	
	my_context.set_my_cloblist (p_ord_ids);
	
	
	FOR nopdf_cur IN cur_nopdf()
		LOOP
		
			gm_sav_extract_revenue_dtl(nopdf_cur.token,110122,'',''
								,'','','',''
								,p_company_id,30301);-- 110122 unRecognized no pdf
								
			gm_upd_revenue_process_fl(nopdf_cur.token);
		
		
	END LOOP;
	
END gm_sav_no_pdf_revenue;

/***********************************************************************
* Description : Procedure used to save DO and PO revenue recognized by system
* Author : Mahavishnu
************************************************************************/
PROCEDURE gm_sav_revenue_by_system (
		p_company_id IN t5005_ar_revenue_scan_dtls.c1900_company_id%TYPE,
		p_user_id    IN  t5005_ar_revenue_scan_dtls.c5005_last_updated_by%TYPE)
		
   AS
   v_load_date  DATE;
   
BEGIN
			
		gm_sav_po_revenue_by_system(p_company_id,p_user_id);
			
		gm_sav_do_revenue_by_system(p_company_id,p_user_id);
		
		
END gm_sav_revenue_by_system;

/***********************************************************************
* Description : Procedure used to save  PO revenue recognized by system
* Author : Mahavishnu
************************************************************************/
PROCEDURE gm_sav_po_revenue_by_system (
	p_company 		IN t501_order.c1900_company_id%TYPE,
	p_user_id    IN  t5005_ar_revenue_scan_dtls.c5005_last_updated_by%TYPE)
   AS
BEGIN
	-- 26240176 PO Only # revenue type 
		UPDATE t5005_ar_revenue_scan_dtls 
		SET c901_system_recog_po_val=26240176,c5005_last_updated_by=p_user_id,c5005_last_updated_date=CURRENT_DATE
		WHERE c5005_process_fl IS NULL
			AND c5005_void_fl IS NULL
			AND c5005_po_fl='Y'
			AND c1900_company_id = p_company;
		
END gm_sav_po_revenue_by_system;


/***********************************************************************
* Description : Procedure used to save DO revenue recognized by system
* Author : Mahavishnu
************************************************************************/
PROCEDURE gm_sav_do_revenue_by_system (
	p_company 		IN t501_order.c1900_company_id%TYPE,
	p_user_id       IN  t5005_ar_revenue_scan_dtls.c5005_last_updated_by%TYPE)
   AS
BEGIN
	
		gm_sav_signed_price_no_patstick(p_company,p_user_id);	
		
		gm_sav_signed_no_price_no_patstick(p_company,p_user_id);	
	
		gm_sav_signed_no_price_patstick(p_company,p_user_id);	
		
		gm_sav_signed_price_patstick(p_company,p_user_id);
		
		gm_sav_unsigned_price_no_patstick(p_company,p_user_id);
		
		gm_sav_unsigned_no_price_patstick(p_company,p_user_id);
		
		gm_sav_unsigned_price_patstick(p_company,p_user_id);
			
		gm_sav_unsigned_no_price_no_patstick(p_company,p_user_id);
		
END gm_sav_do_revenue_by_system;

/***********************************************************************
* Description : Procedure used to save DO revenue recognized by system
* as Signed with pricing, no patient sticker
* Author : Mahavishnu
************************************************************************/
PROCEDURE gm_sav_signed_price_no_patstick (
	p_company 		IN t501_order.c1900_company_id%TYPE,
	p_user_id       IN  t5005_ar_revenue_scan_dtls.c5005_last_updated_by%TYPE)
   AS
BEGIN
		
	--53030 Signed with pricing, no patient sticker
	UPDATE t5005_ar_revenue_scan_dtls 
		SET c901_system_recog_do_val=53030,c5005_process_fl='Y',
			c5005_last_updated_by=p_user_id,c5005_last_updated_date=CURRENT_DATE
		WHERE c5005_patient_sticker_img_fl IS NULL 
			AND c5005_signed_fl='Y'
			AND c5005_pricing_fl='Y'
			AND c5005_process_fl IS NULL
			AND c5005_void_fl IS NULL
			AND c901_category=110120  -- 110120 Recognized
			AND c1900_company_id = p_company;
	
END gm_sav_signed_price_no_patstick;

/***********************************************************************
* Description : Procedure used to save DO revenue recognized by system
* as Signed, no pricing, no patient sticker
* Author : Mahavishnu
************************************************************************/
PROCEDURE gm_sav_signed_no_price_no_patstick (
	p_company 		IN t501_order.c1900_company_id%TYPE,
	p_user_id       IN  t5005_ar_revenue_scan_dtls.c5005_last_updated_by%TYPE)
   AS
BEGIN
		--53031 Signed, no pricing, no patient sticker
	UPDATE t5005_ar_revenue_scan_dtls 
		SET c901_system_recog_do_val=53031,c5005_process_fl='Y',
			c5005_last_updated_by=p_user_id,c5005_last_updated_date=CURRENT_DATE
		WHERE c5005_patient_sticker_img_fl IS NULL 
			AND c5005_signed_fl='Y'
			AND c5005_pricing_fl='N'
			AND c5005_process_fl IS NULL
			AND c5005_void_fl IS NULL
			AND c901_category=110120  -- 110120 Recognized
			AND c1900_company_id = p_company;

	
END gm_sav_signed_no_price_no_patstick;

/***********************************************************************
* Description : Procedure used to save DO revenue recognized by system
* as Signed, no pricing, with patient sticker or info
* Author : Mahavishnu
************************************************************************/
PROCEDURE gm_sav_signed_no_price_patstick (
	p_company 		IN t501_order.c1900_company_id%TYPE,
	p_user_id       IN  t5005_ar_revenue_scan_dtls.c5005_last_updated_by%TYPE)
   AS
BEGIN
		-- 53032 Signed, no pricing, with patient sticker or info
	UPDATE t5005_ar_revenue_scan_dtls 
		SET c901_system_recog_do_val=53032,c5005_process_fl='Y',
			c5005_last_updated_by=p_user_id,c5005_last_updated_date=CURRENT_DATE
		WHERE c5005_patient_sticker_valid_fl ='Y' 
			AND c5005_signed_fl='Y'
			AND c5005_pricing_fl='N'
			AND c5005_process_fl IS NULL
			AND c5005_void_fl IS NULL
			AND c901_category=110120  -- 110120 Recognized
			AND c1900_company_id = p_company;
	
END gm_sav_signed_no_price_patstick;

/***********************************************************************
* Description : Procedure used to save DO revenue recognized by system
* as Signed with pricing &amp; patient sticker or info
* Author : Mahavishnu
************************************************************************/
PROCEDURE gm_sav_signed_price_patstick (
	p_company 		IN t501_order.c1900_company_id%TYPE,
	p_user_id       IN  t5005_ar_revenue_scan_dtls.c5005_last_updated_by%TYPE)
   AS
BEGIN

		-- 53033 Signed with pricing &amp; patient sticker or info
	UPDATE t5005_ar_revenue_scan_dtls 
		SET c901_system_recog_do_val=53033,c5005_process_fl='Y',
			c5005_last_updated_by=p_user_id,c5005_last_updated_date=CURRENT_DATE
		WHERE c5005_patient_sticker_valid_fl ='Y' 
			AND c5005_signed_fl='Y'
			AND c5005_pricing_fl='Y'
			AND c5005_process_fl IS NULL
			AND c5005_void_fl IS NULL
			AND c901_category=110120  -- 110120 Recognized
			AND c1900_company_id = p_company;

	
END gm_sav_signed_price_patstick;

/***********************************************************************
* Description : Procedure used to save DO revenue recognized by system
* as Unsigned with pricing, no patient sticker
* Author : Mahavishnu
************************************************************************/
PROCEDURE gm_sav_unsigned_price_no_patstick (
	p_company 		IN t501_order.c1900_company_id%TYPE,
	p_user_id       IN  t5005_ar_revenue_scan_dtls.c5005_last_updated_by%TYPE)
   AS
BEGIN
	
	 -- 53034 Unsigned with pricing, no patient sticker
	UPDATE t5005_ar_revenue_scan_dtls 
		SET c901_system_recog_do_val=53034,c5005_process_fl='Y',
			c5005_last_updated_by=p_user_id,c5005_last_updated_date=CURRENT_DATE
		WHERE c5005_patient_sticker_img_fl IS NULL
			AND c5005_signed_fl IS NULL
			AND c5005_pricing_fl='Y'
			AND c5005_process_fl IS NULL
			AND c5005_void_fl IS NULL
			AND c901_category=110120  -- 110120 Recognized
			AND c1900_company_id = p_company;
	
END gm_sav_unsigned_price_no_patstick;

/***********************************************************************
* Description : Procedure used to save DO revenue recognized by system
* as Unsigned, no pricing, with patient sticker or info
* Author : Mahavishnu
************************************************************************/
PROCEDURE gm_sav_unsigned_no_price_patstick (
	p_company 		IN t501_order.c1900_company_id%TYPE,
	p_user_id       IN  t5005_ar_revenue_scan_dtls.c5005_last_updated_by%TYPE)
   AS
BEGIN

	-- 53036 Unsigned, no pricing, with patient sticker or info
	UPDATE t5005_ar_revenue_scan_dtls 
		SET c901_system_recog_do_val=53036,c5005_process_fl='Y',
			c5005_last_updated_by=p_user_id,c5005_last_updated_date=CURRENT_DATE
		WHERE c5005_patient_sticker_valid_fl = 'Y'
			AND c5005_signed_fl IS NULL
			AND c5005_pricing_fl='N'
			AND c5005_process_fl IS NULL
			AND c5005_void_fl IS NULL
			AND c901_category=110120  -- 110120 Recognized
			AND c1900_company_id = p_company;
	
END gm_sav_unsigned_no_price_patstick;

/***********************************************************************
* Description : Procedure used to save DO revenue recognized by system
* as Unsigned with pricing &amp; patient sticker or info
* Author : Mahavishnu
************************************************************************/
PROCEDURE gm_sav_unsigned_price_patstick (
	p_company 		IN t501_order.c1900_company_id%TYPE,
	p_user_id       IN  t5005_ar_revenue_scan_dtls.c5005_last_updated_by%TYPE)
   AS
BEGIN

	 -- 53037 Unsigned with pricing &amp; patient sticker or info
	UPDATE t5005_ar_revenue_scan_dtls 
		SET c901_system_recog_do_val=53037,c5005_process_fl='Y',
			c5005_last_updated_by=p_user_id,c5005_last_updated_date=CURRENT_DATE
		WHERE c5005_patient_sticker_valid_fl = 'Y'
			AND c5005_signed_fl IS NULL
			AND c5005_pricing_fl='Y'
			AND c5005_process_fl IS NULL
			AND c5005_void_fl IS NULL
			AND c901_category=110120  -- 110120 Recognized
			AND c1900_company_id = p_company;
	
END gm_sav_unsigned_price_patstick;

/***********************************************************************
* Description : Procedure used to save DO revenue recognized by system
* as Unsigned, no pricing, no patient sticker
* Author : Mahavishnu
************************************************************************/
PROCEDURE gm_sav_unsigned_no_price_no_patstick (
	p_company 		IN t501_order.c1900_company_id%TYPE,
	p_user_id       IN  t5005_ar_revenue_scan_dtls.c5005_last_updated_by%TYPE)	
   AS
BEGIN
	
		--  53041 Unsigned, no pricing, no patient sticker
	UPDATE t5005_ar_revenue_scan_dtls 
		SET c901_system_recog_do_val=53041,c5005_process_fl='Y',
			c5005_last_updated_by=p_user_id,c5005_last_updated_date=CURRENT_DATE
		WHERE c5005_patient_sticker_img_fl IS NULL 
			AND c5005_signed_fl IS NULL
			AND c5005_pricing_fl='N'
			AND c5005_process_fl IS NULL
			AND c5005_void_fl IS NULL
			AND c901_category=110120  -- 110120 Recognized
			AND c1900_company_id = p_company;
	
END gm_sav_unsigned_no_price_no_patstick;

/***********************************************************************
* Description : Function to fetch revenue track aging value by load date and current date
* Author : Mahavishnu
************************************************************************/
FUNCTION get_reveue_track_aging (
	p_load_date    IN T5005_AR_REVENUE_SCAN_DTLS.C5005_LOAD_DATE%TYPE,
	p_current_date IN DATE
	) RETURN VARCHAR2
	AS
	v_aging_val t901_code_lookup.c901_code_nm%TYPE;
	v_aging_days  NUMBER;
	BEGIN
		
		SELECT trunc(p_current_date) - trunc(p_load_date) INTO v_aging_days FROM DUAL;
		
		IF v_aging_days = 0 THEN
		
			v_aging_val := 'Today';
		
		ELSIF v_aging_days= 1 THEN
		
			v_aging_val := '1 Day';
		
		ELSIF v_aging_days= 2 THEN
		
			v_aging_val := '2 Days';
		
		ELSIF v_aging_days > 2 THEN
		
			v_aging_val := '>2 Days';
		
		END IF;
			RETURN v_aging_val;
END get_reveue_track_aging;

/***********************************************************************
* Description : Procedure used to save override DO revenue value
*
* Author : Mahavishnu
************************************************************************/
PROCEDURE gm_sav_override_do_revenue (
	p_order_id 		IN t501_order.c501_order_id%TYPE,
	p_override_do 	IN t5005_ar_revenue_scan_dtls.c901_override_do_val%TYPE,
	p_user          IN t5005_ar_revenue_scan_dtls.c5005_last_updated_by%TYPE)	
   AS
BEGIN
	
		
	UPDATE t5005_ar_revenue_scan_dtls 
		SET c901_override_do_val=p_override_do,
			c5005_do_override_by=p_user,c5005_do_override_date=CURRENT_DATE,
			c5005_last_updated_by=p_user,c5005_last_updated_date=CURRENT_DATE
		WHERE c501_order_id=p_order_id
			AND c5005_void_fl IS NULL;
		
	
END gm_sav_override_do_revenue;

/***********************************************************************
* Description : Procedure used to save override  PO revenue value
*
* Author : Mahavishnu
************************************************************************/
PROCEDURE gm_sav_override_po_revenue (
	p_order_id 		IN t501_order.c501_order_id%TYPE,
	p_override_po 	IN t5005_ar_revenue_scan_dtls.c901_override_po_val%TYPE,
	p_user          IN t5005_ar_revenue_scan_dtls.c5005_last_updated_by%TYPE)	
   AS
BEGIN
		
	UPDATE t5005_ar_revenue_scan_dtls 
		SET c901_override_po_val=p_override_po,
			c5005_po_override_by=p_user,c5005_po_override_date=CURRENT_DATE,
			c5005_last_updated_by=p_user,c5005_last_updated_date=CURRENT_DATE
		WHERE c501_order_id=p_order_id
			AND c5005_void_fl IS NULL;
		
	
END gm_sav_override_po_revenue;


/***********************************************************************
* Description : Procedure used to save  revenue comments
*
* Author : Mahavishnu
************************************************************************/
PROCEDURE gm_sav_revenue_comments (
	p_order_id 		IN t501_order.c501_order_id%TYPE,
	p_comments 		IN CLOB,
	p_user          IN t5005_ar_revenue_scan_dtls.c5005_last_updated_by%TYPE)	
   AS
BEGIN
	
		
	UPDATE t5005_ar_revenue_scan_dtls 
		SET c5005_comments=p_comments,
			c5005_last_updated_by=p_user,c5005_last_updated_date=CURRENT_DATE
		WHERE c501_order_id=p_order_id
			AND c5005_void_fl IS NULL;
		
	
END gm_sav_revenue_comments;


/***********************************************************************
* Description : Procedure used to approve auto revenue by system
*
* Author : Mahavishnu
************************************************************************/
PROCEDURE gm_sav_revenue_approve (
	p_inputstr		IN CLOB,
	p_user          IN t5005_ar_revenue_scan_dtls.c5005_last_updated_by%TYPE)	
   AS
    v_message      VARCHAR2 (4000);
    v_do_codenm    t901_code_lookup.c901_code_nm%TYPE;
    v_comment      t5005_ar_revenue_scan_dtls.c5005_comments%TYPE;
   
   CURSOR cur_revenue
		IS
     		SELECT c501_order_id ordid,NVL(c901_override_do_val,c901_system_recog_do_val) rev_do,
     			   NVL(c901_override_po_val,c901_system_recog_po_val) rev_po,c5005_comments rev_comment
       		FROM t5005_ar_revenue_scan_dtls
      		WHERE c5005_ar_revenue_scan_dtls_id in (SELECT token FROM v_clob_list  WHERE token is not null)
        	AND c5005_void_fl IS NULL;
   
BEGIN
	
		my_context.set_my_cloblist (p_inputstr);
		
		
		FOR rev_cur IN cur_revenue()
		LOOP
			
			gm_pkg_ac_ar_revenue_sampling.gm_sav_revenue_sampling_dtls(rev_cur.ordid,
																	  rev_cur.rev_po,
																	  rev_cur.rev_do,
																	  p_user);
	-- Added for PC-2573 , to save DO revenue value along with user entered comments in order comments log	 												  
				BEGIN
				
					SELECT C901_code_nm INTO v_do_codenm FROM t901_code_lookup 
						WHERE c901_code_id = rev_cur.rev_do;
				
				EXCEPTION WHEN NO_DATA_FOUND
					THEN
						v_do_codenm:='';
				END;			
				
				IF nvl(dbms_lob.getlength(rev_cur.rev_comment),0)	= 0 THEN
					v_comment := v_do_codenm;
					
				ELSE
				
					v_comment :=  v_do_codenm || ' - ' || rev_cur.rev_comment;
				
				END IF;		
				
				-- Substring comments value to max length of t902_log.C902_COMMENTS(4000) column 
					v_comment := SUBSTR(v_comment,1,4000);	
									  
					gm_save_log(rev_cur.ordid,v_comment,1200,p_user,'',v_message);
					
				
				
					UPDATE t5005_ar_revenue_scan_dtls 
							SET c5005_approve_fl='Y',
							    c5005_approved_by =  p_user,
							    c5005_approved_date = CURRENT_DATE,
								c5005_last_updated_by=p_user,
								c5005_last_updated_date= CURRENT_DATE
							WHERE c501_order_id=rev_cur.ordid
						    	AND c5005_void_fl IS NULL;
															 
			
		END LOOP;
		
	
END gm_sav_revenue_approve;


END gm_pkg_ar_auto_revenue_track;
/