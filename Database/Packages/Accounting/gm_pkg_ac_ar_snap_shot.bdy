/* Formatted on 2012/22/08 17:40 (Formatter Plus v4.8.0) */
--@"c:\database\packages\accounting\gm_pkg_ac_ar_snap_shot.bdy"
CREATE OR REPLACE
PACKAGE BODY gm_pkg_ac_ar_snap_shot
IS

	/****************************************************************
	    * Description : Procedure to save the AR aging lock table (based on Company and plant - context)
	    * Author      : Mani
	    *****************************************************************/
	    --
	PROCEDURE gm_sav_ar_summary_snap_shot
	AS
	v_company_id t1900_company.c1900_company_id%TYPE;
	v_plant_id t5040_plant_master.c5040_plant_id%TYPE;
	v_time_zone t901_code_lookup.c901_code_nm%TYPE;
	--
	v_user_id NUMBER := 30301;
	BEGIN
		
		 -- to get the company information
	     SELECT get_compid_frm_cntx (), get_plantid_frm_cntx ()
	     	 , get_comptimezone_frm_cntx()
	       INTO v_company_id, v_plant_id
	       	, v_time_zone
	       FROM dual;
	       -- to alter the time zone  
	       EXECUTE IMMEDIATE 'ALTER SESSION SET TIME_ZONE='''||v_time_zone||''''; 
	       
	      --
	     gm_sav_ar_summary_snap_shot (v_company_id, CURRENT_DATE, v_user_id); 
		 --
	END gm_sav_ar_summary_snap_shot;

/****************************************************************
    * Description : Procedure to save the AR aging lock table
    * Author      : Mani
    *****************************************************************/

PROCEDURE gm_sav_ar_summary_snap_shot (
        p_company_id IN T1900_company.c1900_company_id%TYPE,
        p_lock_date IN t5030_ar_aging_lock.c5030_lock_date%TYPE,
        p_userid      IN t5030_ar_aging_lock. c5030_last_updated_by%TYPE
        )
        AS
        v_lock_id t5030_ar_aging_lock.c5030_ar_aging_lock_id%TYPE;
        v_cnt NUMBER;
        BEGIN
	        select count(1) INTO v_cnt
	        from t5030_ar_aging_lock
	        where c1900_company_id = p_company_id
	        and TRUNC(c5030_lock_date) = TRUNC(p_lock_date)
	        and c5030_void_fl IS NULL;
	        
	 IF v_cnt = 0
	 THEN       
	        
	       v_lock_id :=   s5030_ar_aging_lock.nextval;
	        -- lock
			        
		 INSERT
		   INTO t5030_ar_aging_lock
		    (
		        c5030_ar_aging_lock_id, c5030_lock_date, c1900_company_id
		      , c5030_last_updated_by, c5030_last_updated_date
		    )
		    VALUES
		    (
		        v_lock_id, p_lock_date, p_company_id
		      , p_userid, CURRENT_DATE
		    ) ;
		
		    -- 2 to call the lock details procedure
		    gm_pkg_ac_ar_snap_shot.gm_sav_ar_summary_snap_dtls (v_lock_id);
		    --
	    END IF;    
END gm_sav_ar_summary_snap_shot;

/****************************************************************
    * Description : Procedure to lock the AR aging details 
    * Author      : Mani
    *****************************************************************/
PROCEDURE gm_sav_ar_summary_snap_dtls(
p_ar_lock_id IN t5030_ar_aging_lock.c5030_ar_aging_lock_id%TYPE
) 
AS
v_company_id t1900_company.c1900_company_id%TYPE;
BEGIN

	select c1900_company_id INTO v_company_id from t5030_ar_aging_lock where c5030_ar_aging_lock_id = p_ar_lock_id for update;
	-- to insert the ar aging details
	
insert into   t5031_ar_aging_detail (
c5031_ar_aging_detail_id,
c5030_ar_aging_lock_id,
c503_invoice_id,
c704_account_id,
c704_account_nm,
c704_account_nm_en,
c503_invoice_dt,
c701_distributor_id,
c701_distributor_name,
c101_par_account_id,
c101_par_account_nm,
c101_dealer_id,
c101_dealer_nm,
c101_dealer_nm_en,
c704_acct_credit_rating,
c704_credit_rating_date,
c503_invoice_amt,
c503_inv_paid_amt,
c5031_order_amt,
c5031_ship_cost,
c5031_tax_amt,
c503_status_fl,
c901_invoice_type,
c901_invoice_source,
c901_terms_id,
c901_acc_currency_id,
c901_dealer_collector_id,
c1910_division_id,
c1910_division_name,
c5031_last_payment_rec_date,
c901_collector_id,
c901_credit_type,
c704a_credit_limit,
c5031_terms_days,
c503_customer_po,
c5031_terms_name,
c901_acc_currency_nm,
c903_pdf_file_id,
c903_rtf_file_id,
c903_csv_file_id,
c903_xml_file_id
)
  
 SELECT s5031_AR_AGING_DETAIL.nextval, p_ar_lock_id, v503_invoice_id, v503_account_id
 , v503_account_nm, v503_account_nm_en, v503_invoice_date, v503_distributor_id,
 v503_distributor_name, v503_paracct_id, v503_paracct_nm,
 v503_dealer_id, v503_dealer_nm, v503_dealer_nm_en, v503_credit_rating,
 v503_credit_rating_date, v503_inv_amt, v503_inv_paid, v503_order_amt, v503_ship_cost,
 v503_tax_amt, v503_status_fl , v503_invoice_type, v503_invoice_source,
 v503_terms_id, v503_acc_currency_id, v503_dealer_collector_id,
 v503_division_id, v503_division_name, last_payment_received_date,
 collectorid, credittype , creditlimit
 , TRIM(v503_terms_days)
 , v503_customer_po
 ,v503_terms
 , v503_acc_currency_nm
 ,v503_pdf_id --PMT25904-Store file reference
 ,v503_rtf_id 
 ,v503_csv_id
,v503_xml_id										 
   FROM v503_invoice_reports, (
         SELECT t503.C704_ACCOUNT_ID, MAX (t508.C508_RECEIVED_DATE) last_payment_received_date
           FROM t508_receivables t508, t503_invoice t503
          WHERE t508.c508_received_mode NOT IN ('5013', '5014', '5019')
            AND t503.c1900_company_id        = v_company_id
            AND t508.c503_invoice_id         = t503.c503_invoice_id
       GROUP BY t503.c704_account_id
    )
    last_payment_info, (
         SELECT t704a.c704_account_id, MAX (decode (t704a.c901_attribute_type, 103050, c704a_attribute_value)) collectorid,
            MAX (decode (t704a.c901_attribute_type, '101182', c704a_attribute_value)) creditlimit, max (decode (
            t704a.c901_attribute_type, '101184', c704a_attribute_value)) credittype
           FROM t704a_account_attribute t704a, t704_account t704
          WHERE t704.c704_account_id = t704a.c704_account_id
          AND t704.c1900_company_id = v_company_id
          AND c704a_void_fl is null
       GROUP BY t704a.c704_account_id
    )
    t704a
  WHERE v503_status_fl       < 2
    AND v503_company_id     = v_company_id
    AND v503_account_id     = t704a.c704_account_id (+)
    AND v503_account_id     = last_payment_info.c704_account_id (+);


	
	
	END gm_sav_ar_summary_snap_dtls;

	/****************************************************************
    * Description : Procedure used to fetch the locked information 
    * Author      : Mani
    *****************************************************************/
	
	PROCEDURE gm_fch_snapshot_date(
	p_out_aging_date OUT TYPES.cursor_type
	)
	AS
	v_company_id_ctx T1900_company.c1900_company_id%TYPE;
	v_company_dt_fmt VARCHAR2(20);
	BEGIN
		-- to get the company id
		SELECT get_compid_frm_cntx(), get_compdtfmt_frm_cntx ()
      	INTO v_company_id_ctx,  v_company_dt_fmt
      FROM dual;
      
      
		OPEN p_out_aging_date FOR
		 SELECT t5030.c5030_ar_aging_lock_id ID, TO_CHAR (t5030.c5030_lock_date, v_company_dt_fmt) LOCK_DATE
   FROM t5030_ar_aging_lock t5030
  WHERE c1900_company_id = v_company_id_ctx
  AND c5030_void_fl IS NULL
ORDER BY c5030_lock_date DESC;
		
		END gm_fch_snapshot_date;
	
/****************************************************************************************
 * Description        :This procedure is called to get invoice unpaid payment details
 *
 * Parameters         : p_account_id   IN Passes the Account Name
 *                   p_report_tye   IN Type of report
 *                      129  Means  1 to 29 days  3160 Means 31 to 60 Days
 *                      6190 Means 61 to 90 days 91120 Means 91 to 120 Days
 *                      121  Means Greater than 120 Days
 *
 *                   p_message      OUT Error Message IF Any
 *                   p_recordset OUT Values with AR Information
 *
 *****************************************************************************************/
		 PROCEDURE gm_fch_account_pedning_payment (
   p_ar_lock_id IN t5030_ar_aging_lock.c5030_ar_aging_lock_id%TYPE,		 
   p_account_id    IN       t501_order.c704_account_id%TYPE,
   p_report_type   IN       VARCHAR2,
   p_inv_source    IN       NUMBER,
   p_from_day	   IN       NUMBER,
   p_to_day		   IN		NUMBER,
   p_txntp         IN       VARCHAR2,
   p_div_id        IN       t1910_division.C1910_DIVISION_ID%TYPE,
   p_recordset     OUT      TYPES.cursor_type
)
AS

--
v_company_id        t1900_company.c1900_company_id%TYPE;
v_lang_id 	t1900_company.c901_language_id%TYPE;
BEGIN
	 SELECT get_compid_frm_cntx() 
      INTO v_company_id  
      FROM dual;
      
    SELECT  get_complangid_frm_cntx()
	  INTO	v_lang_id
	  FROM	dual;
	
   -- code is removed because p_from_day and p_to_day is directly coming from the screen.
   OPEN p_recordset
    FOR
--
       SELECT t5031.C503_INVOICE_ID inv_id, t5031.C503_INVOICE_DT inv_date, t5031.C503_CUSTOMER_PO customer_po
  , DECODE (t5031.C901_INVOICE_SOURCE, '26240213', t5031.C101_DEALER_ID, NVL (t5031.C704_ACCOUNT_ID,
    t5031.C701_DISTRIBUTOR_ID)) acct_id, DECODE (t5031.C901_INVOICE_SOURCE, '26240213', DECODE (v_lang_id, '103097', NVL
    (t5031.C101_DEALER_NM_EN, t5031.C101_DEALER_NM), t5031.C101_DEALER_NM), NVL (DECODE (v_lang_id, '103097', NVL (
    t5031.C704_ACCOUNT_NM_EN, t5031.C704_ACCOUNT_NM), t5031.C704_ACCOUNT_NM), t5031.C701_DISTRIBUTOR_NAME)) NAME, NVL (
    t5031.C503_INVOICE_AMT, 0) inv_amt, t5031.C503_INV_PAID_AMT payment_amt, get_log_flag (
    t5031.C503_INVOICE_ID, 1201) call_flag, get_code_name(t5031.C901_ACC_CURRENCY_ID) currency, DECODE (t5031.C901_INVOICE_SOURCE, '26240213',
    NULL, GET_ACCOUNT_ATTRB_VALUE (t5031.C704_ACCOUNT_ID, 91983)) EMAILREQ, DECODE (t5031.C901_INVOICE_SOURCE, '26240213'
    , NULL, GET_ACCOUNT_ATTRB_VALUE (t5031.C704_ACCOUNT_ID, 91984)) EVERSION,t5031.C903_PDF_FILE_ID PDFFILEID, t5031.C903_CSV_FILE_ID CSVFILEID,
    t5031.C503_INVOICE_DT INVYEAR
   FROM T5031_AR_AGING_DETAIL t5031, T5030_AR_AGING_LOCK t5030
  WHERE t5030.c5030_ar_aging_lock_id = p_ar_lock_id
  AND t5030.C5030_AR_AGING_LOCK_ID = t5031.C5030_AR_AGING_LOCK_ID
  AND DECODE (p_inv_source, '26240213', t5031.C101_DEALER_ID, '50256', t5031.C701_DISTRIBUTOR_ID, DECODE (p_txntp,
    'ACCTID', t5031.C704_ACCOUNT_ID, t5031.C101_PAR_ACCOUNT_ID)) = p_account_id
    AND t5031.C901_INVOICE_SOURCE                           = p_inv_source
    AND t5031.C503_STATUS_FL                                < 2
    AND t5030.C1900_COMPANY_ID                                    = v_company_id
    AND t5031.C1910_DIVISION_ID                             = NVL (p_div_id, t5031.C1910_DIVISION_ID)
    AND (TRUNC (t5030.C5030_LOCK_DATE) - TRUNC (t5031.C503_INVOICE_DT + DECODE (p_report_type, 'ARByDueDt', DECODE (p_from_day,
    '0', 0, c5031_terms_days), 0))) >= p_from_day -- for Duedate report curr column no need to add terms. so, that used
    -- decode here.
    AND (TRUNC (t5030.C5030_LOCK_DATE) - TRUNC (t5031.C503_INVOICE_DT + DECODE (p_report_type, 'ARByDueDt', c5031_terms_days, 0)
    )) <= p_to_day; 
END gm_fch_account_pedning_payment;

		
END gm_pkg_ac_ar_snap_shot;
/
