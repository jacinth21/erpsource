/* Formatted on 2011/01/29 02:33 (Formatter Plus v4.8.0) */
--@"c:\database\packages\accounting\gm_pkg_ac_patag_trans.bdy";
CREATE OR REPLACE
PACKAGE BODY gm_pkg_ac_patag_trans
IS
    /*******************************************************
    * Purpose: This procedure will save the tag details
    *******************************************************/
PROCEDURE gm_sav_tag_detail (
        p_physical_audit_id IN t2605_audit_tag_detail.c2605_audit_tag_id%TYPE,
        p_pnum              IN t2605_audit_tag_detail.c205_part_number_id%TYPE,
        p_txn_id            IN t2605_audit_tag_detail.c2605_txn_id%TYPE,
        p_location_id       IN t2605_audit_tag_detail.c901_location_id%TYPE,
        p_counted_by        IN t2605_audit_tag_detail.c101_counted_by%TYPE,
        p_checked_by        IN t2605_audit_tag_detail.c101_checked_by%TYPE,
        p_qty               IN t2605_audit_tag_detail.c2605_qty%TYPE,
        p_type              IN t2605_audit_tag_detail.c901_type%TYPE,
        p_reconcile_id      IN t2605_audit_tag_detail.c901_reconcile_id%TYPE,
        p_created_by        IN t2605_audit_tag_detail.c2605_created_by%TYPE,		
        p_audit_tag_id OUT t2605_audit_tag_detail.c2605_audit_tag_id%TYPE,
		p_inv_tag_id        IN t5010_tag.c5010_tag_id%TYPE DEFAULT NULL,
		p_inv_loc_id        IN t5052_location_master.c5052_location_id%TYPE DEFAULT NULL
		)
AS
	v_company_id  t1900_company.c1900_company_id%TYPE;
	v_plant_id    t5040_plant_master.c5040_plant_id%TYPE;
BEGIN
	SELECT GET_COMPID_FRM_CNTX(), GET_PLANTID_FRM_CNTX()
	    INTO v_company_id, v_plant_id
	 FROM DUAL;
	      
    gm_nextval (p_physical_audit_id, p_type, p_audit_tag_id) ;
     INSERT
       INTO t2605_audit_tag_detail
        (
            c2605_audit_tag_id, c2600_physical_audit_id, c205_part_number_id
          , c2605_txn_id, c901_location_id, c101_counted_by
          , c101_checked_by, c2605_qty, c901_type
          , c2605_created_by, c2605_created_date, c901_reconcile_id,c5010_tag_id,c5052_location_id,c5040_plant_id
        )
        VALUES
        (
            p_audit_tag_id, p_physical_audit_id, p_pnum
          , p_txn_id, p_location_id, p_counted_by
          , p_checked_by, p_qty, p_type
          , p_created_by, SYSDATE, p_reconcile_id,p_inv_tag_id,p_inv_loc_id,v_plant_id
        ) ;
END gm_sav_tag_detail;
/*******************************************************
* Purpose: This procedure will save the tag details
*******************************************************/
PROCEDURE gm_sav_tag_control
    (
        p_counted_by t2605_audit_tag_detail.c101_counted_by%TYPE,
        p_from_tag_id t2605_audit_tag_detail.c2605_audit_tag_id%TYPE,
        p_to_tag_id t2605_audit_tag_detail.c2605_audit_tag_id%TYPE,
        p_user_id t2605_audit_tag_detail.c2605_last_updated_by%TYPE,
        p_pa_id t2605_audit_tag_detail.c2600_physical_audit_id%TYPE
    )
AS
    v_count NUMBER;
    v_company_id  t1900_company.c1900_company_id%TYPE;
    v_plant_id    t5040_plant_master.c5040_plant_id%TYPE;
    
BEGIN
SELECT GET_COMPID_FRM_CNTX(), GET_PLANTID_FRM_CNTX()
	    INTO v_company_id, v_plant_id
	 FROM DUAL;

     SELECT COUNT (1)
       INTO v_count
       FROM t2605_audit_tag_detail t2605
      WHERE TO_NUMBER (c2605_audit_tag_id) >= TO_NUMBER (p_from_tag_id)
        AND TO_NUMBER (c2605_audit_tag_id) <= TO_NUMBER (p_to_tag_id)
        AND C2600_PHYSICAL_AUDIT_ID        != p_pa_id
        AND t2605.c5040_plant_id    = v_plant_id;
    IF (v_count                             > 0) THEN
        GM_RAISE_APPLICATION_ERROR('-20999','173','');
    END IF;
     UPDATE t2605_audit_tag_detail
    SET c101_counted_by                     = p_counted_by, c2605_last_updated_by = p_user_id, c2605_last_updated_date = SYSDATE
      WHERE TO_NUMBER (c2605_audit_tag_id) >= TO_NUMBER (p_from_tag_id)
        AND TO_NUMBER (c2605_audit_tag_id) <= TO_NUMBER (p_to_tag_id)
        AND c901_type                       = 50675 -- regular
        AND c5040_plant_id    = v_plant_id;
    IF (SQL%ROWCOUNT                        = 0) THEN
        raise_application_error ( - 20875, '') ;
    END IF;
END gm_sav_tag_control;
/*******************************************************
* Description : Procedure to save Reconciled Correction info
* Author    : Xun
*******************************************************/
PROCEDURE gm_sav_tag_correction (
        p_physical_audit_id IN t2603_audit_location.c2600_physical_audit_id%TYPE,
        p_location_id       IN t2603_audit_location.c901_location_id%TYPE,
        p_inputstr          IN VARCHAR2,
        p_verify_location   IN VARCHAR2,
        p_user_id           IN t2605_audit_tag_detail.c2605_created_by%TYPE)
AS
    v_strlen    NUMBER          := NVL (LENGTH (p_inputstr), 0) ;
    v_string    VARCHAR2 (4000) := p_inputstr;
    v_substring VARCHAR2 (1000) ;
    v_txnid t2605_audit_tag_detail.c2605_txn_id%TYPE;
    v_qty t2605_audit_tag_detail.c2605_qty%TYPE;
    v_reconactiontype t2605_audit_tag_detail.c901_reconcile_id%TYPE;
    v_pnum t2605_audit_tag_detail.c205_part_number_id%TYPE;
    v_location_type t2603_audit_location.c901_type%TYPE;
    v_inv_lock_id t2600_physical_audit.c250_inventory_lock_id%TYPE;
    v_audit_tag_id t2605_audit_tag_detail.c2605_audit_tag_id%TYPE;
    v_out_cur TYPES.cursor_type;
    v_ma_status t2603_audit_location.c2603_ma_status_fl%TYPE;
    v_detail t205_part_number.c205_part_num_desc%TYPE;
    v_beforcount  NUMBER;
    v_aftercount  NUMBER;
    v_devcount    NUMBER;
    v_devdollar   NUMBER;
    v_reconcilenm VARCHAR2 (3000) ;
    v_unitcost    NUMBER;
    v_extend      NUMBER;
    v_count       NUMBER;
    v_refid       VARCHAR2 (20) ;
    v_warehouse   VARCHAR2 (100) ;
    v_tags_to_verify   NUMBER;
    v_company_id  t1900_company.c1900_company_id%TYPE;	
    v_plant_id    t5040_plant_master.c5040_plant_id%TYPE;
    --
BEGIN
SELECT GET_COMPID_FRM_CNTX(), GET_PLANTID_FRM_CNTX()
	    INTO v_company_id, v_plant_id
	 FROM DUAL;
          
   SELECT COUNT (1) tag_count
       INTO v_tags_to_verify
       FROM t2605_audit_tag_detail t2605b
      WHERE t2605b.c901_location_id = p_location_id
        AND t2605b.c901_type        = 50675 -- regular
        AND t2605b.c5040_plant_id    = v_plant_id
        AND t2605b.c2605_void_fl   IS NULL
        AND t2605b.c2605_qty       IS NULL
	AND t2605b.C2600_PHYSICAL_AUDIT_ID =p_physical_audit_id ;
    IF (v_tags_to_verify            > 0) THEN
        raise_application_error ('-20876', '') ;
    END IF;
     SELECT c901_type
       INTO v_location_type
       FROM t2603_audit_location
      WHERE c2600_physical_audit_id = p_physical_audit_id
        AND c901_location_id        = p_location_id;
     SELECT c250_inventory_lock_id
       INTO v_inv_lock_id
       FROM t2600_physical_audit
      WHERE c2600_physical_audit_id = p_physical_audit_id
        AND C1900_COMPANY_ID = v_company_id
        AND c5040_plant_id    = v_plant_id
        AND c2600_void_fl          IS NULL FOR UPDATE;
    --To verify all location
    IF p_verify_location = 'Y' THEN
        gm_pkg_ac_patag_info.gm_fch_lock_tag_info (p_physical_audit_id, NULL --txn id
        , p_location_id, '0' --Project id
        , 'N' ---For records by Part# and ref Id
        , v_out_cur, v_ma_status) ;
        LOOP
            FETCH v_out_cur
               INTO v_pnum, v_refid, v_detail
              , v_warehouse, v_beforcount, v_aftercount
              , v_devcount, v_devdollar, v_unitcost
              , v_extend, v_reconactiontype, v_reconcilenm;
            EXIT
        WHEN v_out_cur%NOTFOUND;
            IF (v_devcount <> 0) THEN
                gm_pkg_ac_patag_trans.gm_sav_each_tag_correction (p_physical_audit_id, v_location_type, v_refid--Ref ID
                , v_pnum -- Part #
                , p_location_id, v_devcount, v_inv_lock_id, 50670 -- reconcile id  verified
                , p_user_id) ;
            END IF; --end of v_devcount=0
        END LOOP;
    ELSE
        IF v_strlen                      > 0 THEN
            WHILE INSTR (v_string, '|') <> 0
            LOOP
                --
                v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
                v_string    := SUBSTR (v_string, INSTR (v_string, '|')    + 1) ;
                --
                v_txnid           := NULL;
                v_qty             := NULL;
                v_reconactiontype := NULL;
                --
                v_pnum            := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
                v_substring       := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
                v_refid           := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
                v_substring       := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
                v_qty             := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
                v_substring       := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
                v_reconactiontype := v_substring;
                gm_pkg_ac_patag_trans.gm_sav_each_tag_correction (p_physical_audit_id, v_location_type, v_refid, v_pnum
                , p_location_id, v_qty, v_inv_lock_id, v_reconactiontype -- reconcile id  verified || Not Verified
                , p_user_id) ;
            END LOOP;
        END IF; --end of str length=0
    END IF; --end of verifyLocation = 'Y'
END gm_sav_tag_correction;
/*******************************************************
* Description :
* Author  :
*******************************************************/
PROCEDURE gm_sav_each_tag_correction (
        p_physical_audit_id IN t2603_audit_location.c2600_physical_audit_id%TYPE,
        p_location_type     IN t2603_audit_location.c901_type%TYPE,
        p_refid             IN VARCHAR2,
        p_pnum              IN t2605_audit_tag_detail.c205_part_number_id%TYPE,
        p_location_id       IN t2603_audit_location.c901_location_id%TYPE,
        p_qty               IN t2605_audit_tag_detail.c2605_qty%TYPE,
        p_inv_lock_id       IN t2600_physical_audit.c250_inventory_lock_id%TYPE,
        p_reconactiontype   IN t2605_audit_tag_detail.c901_reconcile_id%TYPE,
        p_user_id           IN t2605_audit_tag_detail.c2605_created_by%TYPE)
AS
    v_pnum t2605_audit_tag_detail.c205_part_number_id%TYPE;
    v_audit_tag_id t2605_audit_tag_detail.c2605_audit_tag_id%TYPE;
    v_count NUMBER;
    v_refid VARCHAR2 (20) ;
BEGIN
    v_refid            := p_refid;
    IF (p_location_type = 50661) -- for Transaction
        THEN
        v_pnum := '';
         SELECT COUNT (1)
           INTO v_count
           FROM t2605_audit_tag_detail
          WHERE c2600_physical_audit_id = p_physical_audit_id
            AND c2605_txn_id            = v_refid
            AND c901_type               = 50676
            AND c901_location_id        = p_location_id
            AND c2605_void_fl          IS NULL;
    ELSIF (p_location_type              = 50660) THEN -- For Part
        v_pnum                         := p_pnum;
        v_refid                        := '';
         SELECT COUNT (1)
           INTO v_count
           FROM t2605_audit_tag_detail
          WHERE c2600_physical_audit_id = p_physical_audit_id
            AND c205_part_number_id     = v_pnum
            AND c901_type               = 50676
            AND c901_location_id        = p_location_id
            AND c2605_void_fl          IS NULL;
    ELSIF (p_location_type              = 50662) THEN --For Part and Transaction
        v_pnum                         := p_pnum;
        v_refid                        := p_refid;
         SELECT COUNT (1)
           INTO v_count
           FROM t2605_audit_tag_detail
          WHERE c2600_physical_audit_id = p_physical_audit_id
            AND c205_part_number_id     = v_pnum
            AND c2605_txn_id            = v_refid
            AND c901_type               = 50676
            AND c901_location_id        = p_location_id
            AND c2605_void_fl          IS NULL;
    END IF;
    IF (v_count = 0) --- for reconcile not verified
        THEN
        gm_pkg_ac_patag_trans.gm_sav_tag_detail (p_physical_audit_id, v_pnum, p_refid, p_location_id, NULL --counted by
        , NULL --checked by
        , p_qty --deviation QTY
        , 50676 --type--adjustment
        , p_reconactiontype, p_user_id, v_audit_tag_id) ;
        IF (p_location_type = 50661) -- for Transaction
            THEN
            gm_pkg_ac_patag_trans.gm_sav_tag_txn_detail (p_physical_audit_id, p_refid, p_location_id, p_inv_lock_id,
            p_reconactiontype, '0' --deviation QTY
            , p_user_id) ;
        END IF;
        IF (p_location_type = 50662) -- For Part &Transaction
            THEN
            gm_pkg_ac_patag_trans.gm_sav_tag_part_txn_detail (p_physical_audit_id, p_refid, p_location_id,
            p_inv_lock_id, p_reconactiontype, v_pnum, p_qty, p_user_id) ;
        END IF;
    END IF;
END gm_sav_each_tag_correction;
/*******************************************************
* Description : Procedure to save Transaction info to table
* Author    : Xun
*******************************************************/
PROCEDURE gm_sav_tag_txn_detail (
        p_physical_audit_id IN t2603_audit_location.c2600_physical_audit_id%TYPE,
        p_txnid             IN t2605_audit_tag_detail.c2605_txn_id%TYPE,
        p_location_id       IN t2603_audit_location.c901_location_id%TYPE,
        p_inv_lock_id       IN t2600_physical_audit.c250_inventory_lock_id%TYPE,
        p_reconactiontype   IN t2605_audit_tag_detail.c901_reconcile_id%TYPE,
        --deviation QTY in this proc will be always 0 this param should not be used in this proc
        p_qty     IN t2605_audit_tag_detail.c2605_qty%TYPE,
        p_user_id IN t2605_audit_tag_detail.c2605_created_by%TYPE)
AS
    v_pnum t2605_audit_tag_detail.c205_part_number_id%TYPE;
    v_qty t2605_audit_tag_detail.c2605_qty%TYPE;
    v_audit_tag_id t2605_audit_tag_detail.c2605_audit_tag_id%TYPE;
    --
    CURSOR cur_con_details
    IS
         SELECT t253b.c205_part_number_id pnum, (t253b.c505_item_qty * - 1) qty
           FROM t253b_item_consignment_lock t253b
          WHERE t253b.c250_inventory_lock_id = p_inv_lock_id
            AND t253b.c504_consignment_id    = p_txnid;
BEGIN
    FOR var_con_detail IN cur_con_details
    LOOP
        v_pnum := var_con_detail.pnum;
        v_qty  := var_con_detail.qty;
        gm_pkg_ac_patag_trans.gm_sav_tag_ref_detail (p_physical_audit_id, p_txnid, p_location_id, p_inv_lock_id,
        p_reconactiontype, v_pnum, v_qty, p_user_id) ;
    END LOOP;
END gm_sav_tag_txn_detail;
/*******************************************************
* Description : Procedure to save Transaction and part info to table
* Author    : Xun
*******************************************************/
PROCEDURE gm_sav_tag_part_txn_detail (
        p_physical_audit_id IN t2603_audit_location.c2600_physical_audit_id%TYPE,
        p_txnid             IN t2605_audit_tag_detail.c2605_txn_id%TYPE,
        p_location_id       IN t2603_audit_location.c901_location_id%TYPE,
        p_inv_lock_id       IN t2600_physical_audit.c250_inventory_lock_id%TYPE,
        p_reconactiontype   IN t2605_audit_tag_detail.c901_reconcile_id%TYPE,
        p_pnum              IN t2605_audit_tag_detail.c205_part_number_id%TYPE,
        p_qty               IN t2605_audit_tag_detail.c2605_qty%TYPE,
        p_user_id           IN t2605_audit_tag_detail.c2605_created_by%TYPE)
AS
BEGIN
    gm_pkg_ac_patag_trans.gm_sav_tag_ref_detail (p_physical_audit_id, p_txnid, p_location_id, p_inv_lock_id,
    p_reconactiontype, p_pnum, p_qty, p_user_id) ;
END gm_sav_tag_part_txn_detail;
/*******************************************************
* Description  : Procedure to save the generated tags part and ref
* Author   : mihir
*******************************************************/
PROCEDURE gm_sav_tag_ref_detail (
        p_physical_audit_id IN t2603_audit_location.c2600_physical_audit_id%TYPE,
        p_txnid             IN t2605_audit_tag_detail.c2605_txn_id%TYPE,
        p_location_id       IN t2603_audit_location.c901_location_id%TYPE,
        p_inv_lock_id       IN t2600_physical_audit.c250_inventory_lock_id%TYPE,
        p_reconactiontype   IN t2605_audit_tag_detail.c901_reconcile_id%TYPE,
        p_pnum              IN t2605_audit_tag_detail.c205_part_number_id%TYPE,
        p_qty               IN t2605_audit_tag_detail.c2605_qty%TYPE,
        p_user_id           IN t2605_audit_tag_detail.c2605_created_by%TYPE)
AS
    v_pnum t2605_audit_tag_detail.c205_part_number_id%TYPE;
    v_qty t2605_audit_tag_detail.c2605_qty%TYPE;
    v_audit_tag_id t2605_audit_tag_detail.c2605_audit_tag_id%TYPE;
BEGIN
    v_pnum := p_pnum;
    v_qty  := p_qty;
     UPDATE t2605_audit_tag_detail
    SET c2605_qty                   = c2605_qty + v_qty, c901_reconcile_id = p_reconactiontype, c2605_last_updated_by = p_user_id
      , c2605_last_updated_date     = SYSDATE
      WHERE c2600_physical_audit_id = p_physical_audit_id
        AND c205_part_number_id     = v_pnum
        AND c901_location_id        = p_location_id
        AND c901_type               = 50676
        AND c2605_void_fl          IS NULL
        AND c2605_txn_id           IS NULL;
    IF (SQL%ROWCOUNT                = 0) THEN
        gm_pkg_ac_patag_trans.gm_sav_tag_detail (p_physical_audit_id, v_pnum, NULL, p_location_id, NULL --counted by
        , NULL --checked by
        , v_qty --deviation QTY
        , 50676 --type--adjustment
        , p_reconactiontype, p_user_id, v_audit_tag_id) ;
    END IF;
END gm_sav_tag_ref_detail;
/*******************************************************
* Description  : Procedure to save the generated tags
* Author   : VPrasath
*******************************************************/
PROCEDURE gm_sav_tags (
        p_physical_audit_id IN t2603_audit_location.c2600_physical_audit_id%TYPE,
        p_location_id       IN t2603_audit_location.c901_location_id%TYPE,
        p_inputstr          IN VARCHAR2,
        p_user_id           IN t2605_audit_tag_detail.c2605_created_by%TYPE,
        p_out_cur OUT TYPES.cursor_type)
AS
    v_location_type t2603_audit_location.c901_type%TYPE;
    v_string    VARCHAR2 (30000) := p_inputstr;
    v_substring VARCHAR2 (1000) ;
    v_pnum t2605_audit_tag_detail.c205_part_number_id%TYPE;
    v_txn_id t2605_audit_tag_detail.c2605_txn_id%TYPE;
    v_location_id t2603_audit_location.c901_location_id%TYPE;
    v_counted_by t2605_audit_tag_detail.c101_counted_by%TYPE;
    v_tag_count NUMBER := 0;
    v_part_num  VARCHAR2 (20) ;
    v_ref_id    VARCHAR2 (20) ;
    v_audit_tag_id t2605_audit_tag_detail.c2605_audit_tag_id%TYPE;
    v_tag_ids VARCHAR2 (30000) := '';
    v_company_id  t1900_company.c1900_company_id%TYPE;
    v_plant_id    T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
BEGIN
select get_compid_frm_cntx(),get_plantid_frm_cntx() 
		into v_company_id,v_plant_id 
		from dual;
    BEGIN
         SELECT c901_type
           INTO v_location_type
           FROM t2603_audit_location
          WHERE c901_location_id        = p_location_id
            AND c2600_physical_audit_id = p_physical_audit_id;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_location_type := '';
    END;
    WHILE INSTR (v_string, '|') <> 0
    LOOP
        v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
        v_string    := SUBSTR (v_string, INSTR (v_string, '|')    + 1) ;
        --
        v_part_num     := NULL;
        v_ref_id       := NULL;
        v_location_id  := NULL;
        v_counted_by   := NULL;
        v_tag_count    := 0;
        v_part_num     := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
        v_substring    := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        v_ref_id       := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
        v_substring    := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        v_location_id  := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
        v_substring    := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        v_counted_by   := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
        v_substring    := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        v_tag_count    := TO_NUMBER (v_substring) ;
        IF v_part_num   = '-' THEN
            v_part_num := '';
        END IF;
        IF v_ref_id   = '-' THEN
            v_ref_id := '';
        END IF;
        v_txn_id           := NULL;
        v_pnum             := NULL;
        v_location_type    := NULL;
        IF v_location_type IS NULL THEN
            BEGIN
                 SELECT c901_type
                   INTO v_location_type
                   FROM t2603_audit_location
                  WHERE c901_location_id        = v_location_id
                    AND c2600_physical_audit_id = p_physical_audit_id;
            EXCEPTION
            WHEN NO_DATA_FOUND THEN
                v_location_type := '';
            END;
        END IF;
        IF v_location_type    = 50660 THEN
            v_pnum           := v_part_num;
        ELSIF v_location_type = 50661 THEN
            v_txn_id         := v_ref_id;
        ELSIF v_location_type = 50662 THEN
            v_pnum           := v_part_num;
            v_txn_id         := v_ref_id;
        END IF;
        WHILE v_tag_count > 0
        LOOP
            gm_pkg_ac_patag_trans.gm_sav_tag_detail (p_physical_audit_id, v_pnum, v_txn_id, v_location_id, v_counted_by
            --counted by
            , NULL --checked by
            , NULL, 50675 --Regular
            , NULL, p_user_id, v_audit_tag_id) ;
            v_tag_ids   := v_tag_ids || v_audit_tag_id || ',';
            v_tag_count := v_tag_count - 1;
        END LOOP;
    END LOOP;
    gm_pkg_ac_patag_info.gm_fch_generated_tags (v_tag_ids, p_out_cur) ;
END gm_sav_tags;
--
/***********************************************************
* Description   : Procedure to save the entered tag detail
* Author    : VPrasath
***********************************************************/
PROCEDURE gm_sav_update_tag (
        p_tag_id      IN t2605_audit_tag_detail.c2605_audit_tag_id%TYPE,
        p_location_id IN t2605_audit_tag_detail.c901_location_id%TYPE,
        p_counted_by  IN t2605_audit_tag_detail.c101_counted_by%TYPE,
        p_pnum        IN VARCHAR2,
        p_refid       IN VARCHAR2,
        p_qty         IN t2605_audit_tag_detail.c2605_qty%TYPE,
        p_user        IN t2605_audit_tag_detail.c2605_last_updated_by%TYPE)
AS
    v_count NUMBER;
    v_status_fl t2603_audit_location.c2603_ma_status_fl%TYPE;
    v_reconcile_count NUMBER;
    v_txn_type        NUMBER;
    v_audit_id t2603_audit_location.c2600_physical_audit_id%TYPE;
    v_pnum t2605_audit_tag_detail.c205_part_number_id%TYPE;
    v_txn t2605_audit_tag_detail.c2605_txn_id%TYPE;
    v_check_pnum t2605_audit_tag_detail.c205_part_number_id%TYPE;
    v_check_txn t2605_audit_tag_detail.c2605_txn_id%TYPE;
    v_inventory_id t2600_physical_audit.c250_inventory_lock_id%TYPE;
    v_location_count    NUMBER;
    v_pi_location_count NUMBER;
    v_loc_cnt           NUMBER;
    v_warehouse_type t901_code_lookup.c901_code_id%TYPE;
    v_company_id  t1900_company.c1900_company_id%TYPE:=1000;   -- GMNA Company
    v_plant_id 	  t5040_plant_master.c5040_plant_id%TYPE:=3000;  -- Audubon Plant
BEGIN
        SELECT GET_COMPID_FRM_CNTX(), GET_PLANTID_FRM_CNTX()
	    INTO v_company_id, v_plant_id
	    FROM DUAL;
		
  v_warehouse_type := get_rule_value (p_location_id, 'PI-LOCWAREHOUSE-MAP');

     SELECT MAX (TO_NUMBER(c2600_physical_audit_id))
       INTO v_audit_id
       FROM t2600_physical_audit
      WHERE c2600_void_fl IS NULL
      AND C1900_COMPANY_ID = v_company_id
      AND c5040_plant_id    = v_plant_id;
    BEGIN
         SELECT c901_type
           INTO v_txn_type
           FROM t2603_audit_location
          WHERE c901_location_id        = p_location_id
            AND c2600_physical_audit_id = v_audit_id;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        raise_application_error ('-20877', '') ;
    END;
     SELECT t2600.c250_inventory_lock_id
       INTO v_inventory_id
       FROM t2600_physical_audit t2600
      WHERE t2600.c2600_physical_audit_id = v_audit_id;
    IF v_txn_type                         = 50660 -- Part
        THEN
        v_pnum      := p_pnum;
    ELSIF v_txn_type = 50661 -- Transaction
        THEN
        v_txn       := p_refid;
    ELSIF v_txn_type = 50662 -- Part and Transaction
        THEN
        v_pnum := p_pnum;
        v_txn  := p_refid;
    END IF;
    --IF the user entered a part number and selected a location which cant accept a part number throw an error
    IF v_txn_type = 50661 AND v_pnum IS NOT NULL THEN
        raise_application_error ('-20878', '') ;
    END IF;
    --IF the user entered a consignment and selected a location which cant accept a transaction throw an error
    IF v_txn_type = 50660 AND v_txn IS NOT NULL THEN
        raise_application_error ('-20879', '') ;
    END IF;
    --Shelf  must have part# and a Inventory location
    IF v_txn_type = 50662 AND (v_pnum IS NULL OR v_txn IS NULL) THEN
        raise_application_error ('-20880', '') ;
    END IF;
     SELECT COUNT ( *)
       INTO v_count
       FROM t2605_audit_tag_detail t2605
      WHERE c2605_audit_tag_id = p_tag_id
        AND c901_type          = 50675
        AND t2605.c5040_plant_id    = v_plant_id
        AND c2605_void_fl     IS NULL;
    IF (v_count                = 0) THEN
        raise_application_error ('-20881', '') ;
    END IF;
     SELECT c2603_ma_status_fl
       INTO v_status_fl
       FROM t2603_audit_location
      WHERE c901_location_id        = p_location_id
        AND c2600_physical_audit_id = v_audit_id FOR UPDATE;
    IF (v_status_fl                 = 'Y') THEN
        raise_application_error ('-20882', '') ;
    END IF;
     SELECT COUNT ( *)
       INTO v_reconcile_count
       FROM t2605_audit_tag_detail
      WHERE c901_type                         = 50676
        AND NVL (c205_part_number_id, '-999') = NVL (v_pnum, '-999')
        AND NVL (c2605_txn_id, '-999')        = NVL (v_txn, '-999')
        AND c901_location_id                  = p_location_id
        AND c5040_plant_id    = v_plant_id
        AND c2600_physical_audit_id           = v_audit_id;
    IF (v_reconcile_count                     > 0) THEN
        raise_application_error ('-20883', '') ;
    END IF;
    IF v_txn_type = 50662 -- Part and Transaction
        THEN
         SELECT c205_part_number_id, c2605_txn_id
           INTO v_check_pnum, v_check_txn
           FROM t2605_audit_tag_detail
          WHERE c2605_audit_tag_id = p_tag_id
          AND c5040_plant_id    = v_plant_id;
        IF v_check_pnum           IS NULL OR v_check_txn IS NULL THEN
           

		SELECT COUNT (1)
               INTO v_location_count
		FROM t5051_inv_warehouse t5051,t5052_location_master t5052, t5053_location_part_mapping t5053
		WHERE t5051.C5051_INV_WAREHOUSE_ID=t5052.C5051_INV_WAREHOUSE_ID
		AND t5052.c5052_location_id = t5053.c5052_location_id
		 AND t5051.C901_STATUS_ID =1
		AND t5052.c901_status             = 93310 --93310/Active  93202/Initiated
		AND t5052.c901_location_type     IS NOT NULL -- 93336/bulk  93320/Pick Face   NULL
		AND t5052.c5052_location_cd   = v_txn
        AND t5052.c5040_plant_id    = v_plant_id
		AND t5053.c205_part_number_id =  v_pnum
		AND t5051.C901_WAREHOUSE_TYPE = v_warehouse_type-- 56001 Restricted Warehouse  --90800 FG Warehouse
		-- AND t5053.c5053_curr_qty IS NOT NULL --'036-B-002-A-1' has 3 parts with with qty 0 and 4 row with null
		 -- part no  and null qty
		AND t5052.c5052_void_fl IS NULL;

            IF v_location_count         = 0 THEN
                raise_application_error ('-20419', '') ;
            ELSE
                 SELECT COUNT (1) INTO v_loc_cnt
                   FROM t253a_consignment_lock t253a
                  WHERE t253a.c250_inventory_lock_id = v_inventory_id
                    AND t253a.c901_location_type     = p_location_id
                    AND t253a.c5040_plant_id    = v_plant_id
                    AND t253a.c504_consignment_id    = v_txn;
                IF v_loc_cnt                         = 0 THEN
                    gm_pkg_op_ld_inventory.gm_op_sav_cons_lock (v_inventory_id, v_txn, '', NULL, '', '', '', '', '', ''
                    , '', '', '', '', '', p_location_id, '', '', NULL) ;
                END IF; -- v_loc_cnt
                 SELECT COUNT (1)
                   INTO v_pi_location_count
                   FROM t253b_item_consignment_lock
                  WHERE c250_inventory_lock_id = v_inventory_id
                    AND c504_consignment_id    = v_txn
		    AND c901_location_type     = p_location_id
                    AND c205_part_number_id    = v_pnum;
                IF v_pi_location_count         = 0 THEN
                    gm_pkg_op_ld_inventory.gm_op_sav_cons_lock_detail (v_inventory_id, v_pnum, v_txn, p_qty,p_location_id) ;
                END IF; -- v_pi_location_count
            END IF; -- check part# and inv location t5053 table
        END IF; -- pnum or txn null
    END IF; -- end txn type = 50662
     UPDATE t2605_audit_tag_detail
    SET c901_location_id        = p_location_id, c101_counted_by = p_counted_by, c205_part_number_id = v_pnum
      , c2605_txn_id            = v_txn, c2605_qty = p_qty, c2605_last_updated_by = p_user
      , c2605_last_updated_date = SYSDATE
      WHERE c2605_audit_tag_id  = p_tag_id;
END gm_sav_update_tag;
--
END gm_pkg_ac_patag_trans;
/
