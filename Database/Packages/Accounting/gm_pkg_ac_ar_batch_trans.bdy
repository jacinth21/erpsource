/* Formatted on 2009/09/17 10:48 (Formatter Plus v4.8.0) */
--@"C:\Database\Packages\Accounting\gm_pkg_ac_ar_batch_trans.bdy";
CREATE OR REPLACE
PACKAGE BODY gm_pkg_ac_ar_batch_trans
IS
    --
/******************************************************************
* Description : Procedure used to save the selected POs to batch details
****************************************************************/
PROCEDURE gm_sav_inv_batch (
        p_inputstr IN CLOB,
        p_batch_id IN T9600_BATCH.C9600_BATCH_ID%TYPE,
        p_userid   IN T9600_BATCH.C9600_CREATED_BY%TYPE
        )
AS
    v_batchid T9600_BATCH.C9600_BATCH_ID%TYPE;
    v_old_batchid T9600_BATCH.C9600_BATCH_ID%TYPE;
    v_strlen NUMBER := NVL (LENGTH (p_inputstr), 0) ;
    v_string CLOB   := p_inputstr;
    v_substring VARCHAR2 (4000) ;
    v_account_id T501_order.c704_account_id%TYPE;
    v_customer_po T501_order.c501_customer_po%TYPE;
    v_batch_type T9600_BATCH.C901_BATCH_TYPE%TYPE;
    v_batch_status T9600_BATCH.C901_BATCH_STATUS%TYPE;
    v_cnt NUMBER;
    v_inv_batch_fl BOOLEAN;
    v_dupl_po CLOB;
    v_hold_po CLOB;
    v_hold_fl_cnt NUMBER;
    v_batch_cnt NUMBER;
    v_account_name VARCHAR2(100);
    v_email_fl VARCHAR2(1);
    CURSOR cust_po_cur
    IS
          SELECT c501_order_id orderid
   			FROM t501_order
  		WHERE c704_account_id  = v_account_id
    		AND c501_customer_po = v_customer_po
    		AND NVL (c901_order_type, - 999) NOT IN
            (
                 SELECT token FROM v_in_list
            )
    		AND c501_hold_fl    IS NULL
    		AND (c901_ext_country_id IS NULL OR c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH
    		AND c503_invoice_id IS NULL
    		AND c501_void_fl    IS NULL
    		AND c501_delete_fl  IS NULL
		ORDER BY c501_order_id;
BEGIN
	-- Exclude Order type from Invoice Batch. (2518,2519,2535)
	my_context.set_my_inlist_ctx (get_rule_value ('INVBATEXLIST', 'INVBATEXORDERTYPE'));
	
	v_batchid := p_batch_id;
	--
      SELECT COUNT (1)
   		INTO v_batch_cnt
   	FROM t9600_batch
  		WHERE c9600_batch_id = p_batch_id
    	AND c9600_void_fl IS NULL;
    -- 
    IF 	v_batch_cnt = 0
    THEN
    	GM_RAISE_APPLICATION_ERROR('-20999','80','');
    END IF;	
	SELECT c901_batch_type, c901_batch_status
	 INTO v_batch_type, v_batch_status
   FROM t9600_batch
  WHERE c9600_batch_id = v_batchid
    AND c9600_void_fl IS NULL FOR UPDATE;
    --
    IF v_batch_status <> 18741
    THEN
    	GM_RAISE_APPLICATION_ERROR('-20999','74','');
    END IF;
    
    WHILE INSTR (v_string, '|') <> 0
    LOOP
        v_substring   := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
        v_string      := SUBSTR (v_string, INSTR (v_string, '|')    + 1) ;
        v_account_id  := NULL;
        v_customer_po := NULL;
        v_inv_batch_fl := FALSE;
        v_email_fl := NULL;
        --
        v_account_id  := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
        v_substring   := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        v_customer_po := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
        v_substring   := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        v_email_fl    := v_substring;
        
         -- Check the hold fl
              SELECT COUNT(1) INTO v_hold_fl_cnt
   				FROM t501_order
  			WHERE c501_customer_po = v_customer_po
    			AND c704_account_id  = v_account_id
    			AND c501_void_fl IS NULL
    			AND c501_delete_fl  IS NULL
    			AND c501_hold_fl    IS NOT NULL;
    			
    		IF	v_hold_fl_cnt <> 0
    		THEN
            	v_hold_po := v_hold_po || v_customer_po ||', ';
            END IF;	
            
            IF v_customer_po IS NULL
            THEN
            	v_account_name := get_account_name (v_account_id);
            	GM_RAISE_APPLICATION_ERROR('-20999','75',v_account_name);
            END IF;
         IF v_hold_fl_cnt = 0
         THEN
        FOR po_cur    IN cust_po_cur
        LOOP
            -- validate the order ids
             SELECT COUNT (1)
               INTO v_cnt
               FROM t9601_batch_details
              WHERE c9601_ref_id   = po_cur.orderid
                AND c901_ref_type  = v_batch_type
                AND c9601_void_fl IS NULL;
             -- to get the batch id.   
            IF v_cnt <> 0 AND v_inv_batch_fl = FALSE
            THEN
            	SELECT c9600_batch_id
               INTO v_old_batchid
               FROM t9601_batch_details
              WHERE c9601_ref_id   = po_cur.orderid
                AND c901_ref_type  = v_batch_type
                AND c9601_void_fl IS NULL;
                IF v_batchid <> v_old_batchid
                THEN
            		v_inv_batch_fl := TRUE;
            		v_dupl_po := v_dupl_po || v_customer_po ||', ';
            	END IF;	
            END IF;
            -- to update the batch details.
            gm_pkg_cm_batch.gm_sav_batch_details (v_batchid, po_cur.orderid, v_batch_type, p_userid) ;
            
            	 UPDATE t9601_batch_details
					SET c9601_email_fl = v_email_fl
					 , C9601_LAST_UPDATE_BY  = p_userid
					 , C9601_LAST_UPDATED_DATE = SYSDATE
				  WHERE c9600_batch_id = v_batchid
				  	AND c9601_ref_id = po_cur.orderid
				  	AND c901_ref_type = v_batch_type
				    AND c9601_void_fl IS NULL;
				    
        END LOOP;
        END IF; -- Hod flag check
    END LOOP;
    -- 
    IF v_dupl_po IS NOT NULL
    THEN
    -- remove the last comma.
    v_dupl_po := substr(v_dupl_po, 0, LENGTH(v_dupl_po)-2);
    -- show the error message.
    GM_RAISE_APPLICATION_ERROR('-20999','76',v_dupl_po);
    END IF;
    --
    IF v_hold_po IS NOT NULL
    THEN
    -- remove the last comma.
    v_hold_po := substr(v_hold_po, 0, LENGTH(v_hold_po)-2);
    -- show the error message.
    GM_RAISE_APPLICATION_ERROR('-20999','77',v_hold_po);
    END IF;
    -- update the batch status as (18742 - Pending Processing)
   gm_pkg_cm_batch.gm_sav_batch (v_batch_type, 18742, p_userid, v_batchid);
END gm_sav_inv_batch;
/******************************************************************
* Description : Procedure used to unlink the selected POs (batch details)
****************************************************************/
PROCEDURE gm_sav_unlink_po(
        p_inputstr       IN VARCHAR2,
        p_comments     IN t907_cancel_log.c907_comments%TYPE,
        p_cancelreason IN t907_cancel_log.c901_cancel_cd%TYPE,
        p_canceltype   IN t901_code_lookup.c902_code_nm_alt%TYPE,
        p_userid       IN T9600_BATCH.C9600_CREATED_BY%TYPE,
        p_out_msg OUT VARCHAR2)
AS
    v_strlen NUMBER := NVL (LENGTH (p_inputstr), 0) ;
    v_string CLOB   := p_inputstr;
    v_substring VARCHAR2 (4000) ;
    v_refid t9601_batch_details.c9601_ref_id%TYPE;
    v_ref_type t9601_batch_details.c901_ref_type%TYPE;
    v_cnt_dtls  NUMBER;
    v_batch_id T9600_BATCH.C9600_BATCH_ID%TYPE;
    v_account_id T501_order.c704_account_id%TYPE;
    v_customer_po T501_order.c501_customer_po%TYPE;
    v_unlink_code_id t901_code_lookup.c901_code_id%TYPE;
    v_batch_cnt NUMBER;
    v_batch_type T9600_BATCH.C901_BATCH_TYPE%TYPE;
    v_batch_status T9600_BATCH.C901_BATCH_STATUS%TYPE;
    
     CURSOR cust_po_cur
    IS
          SELECT t501.c501_order_id orderid
   			FROM t501_order t501,t9601_batch_details t9601
  		WHERE t501.c704_account_id  = v_account_id
    		AND t501.c501_customer_po = v_customer_po
    		AND t9601.c9600_batch_id = v_batch_id
    		AND t501.c501_order_id = t9601.c9601_ref_id
    		AND t501.c501_void_fl    IS NULL
    		AND t501.c501_delete_fl  IS NULL
    		AND t9601.c9601_void_fl IS NULL
    	UNION
    	  SELECT t501.c501_order_id orderid
   			FROM t501_order t501,t9601_batch_details t9601
  		WHERE t501.c101_dealer_id  = v_account_id
    		AND t501.c501_customer_po = v_customer_po
    		AND t9601.c9600_batch_id = v_batch_id
    		AND t501.c501_order_id = t9601.c9601_ref_id
    		AND t501.c501_void_fl    IS NULL
    		AND t501.c501_delete_fl  IS NULL
    		AND t9601.c9601_void_fl IS NULL
		ORDER BY orderid;
BEGIN
	
	-- to get the code id from type
     SELECT c901_code_id
       INTO v_unlink_code_id
       FROM t901_code_lookup
      WHERE c902_code_nm_alt = p_canceltype;
     -- 
    WHILE INSTR (v_string, '|') <> 0
    LOOP
        v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
        v_string    := SUBSTR (v_string, INSTR (v_string, '|')    + 1) ;
        v_account_id  := NULL;
        v_customer_po := NULL;
        --
        v_batch_id :=SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
        v_substring   := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        v_account_id  := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
        v_substring   := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        v_customer_po := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
        v_substring   := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        v_ref_type := v_substring;
        --
         FOR po_cur    IN cust_po_cur
        LOOP
        SELECT COUNT(1) INTO v_batch_cnt
       FROM t9601_batch_details
      WHERE c9601_ref_id   = po_cur.orderid
        AND c9600_batch_id = v_batch_id
        AND c901_ref_type  = v_ref_type
        AND c9601_void_fl IS NULL;
        
        IF v_batch_cnt = 0 
        THEN
        	GM_RAISE_APPLICATION_ERROR('-20999','78',v_customer_po);
        END IF;
		--
        SELECT c901_batch_type, c901_batch_status
	 INTO v_batch_type, v_batch_status
   FROM t9600_batch
  WHERE c9600_batch_id = v_batch_id
    AND c9600_void_fl IS NULL FOR UPDATE;
    --
    IF v_batch_status > 18742
    THEN
    	GM_RAISE_APPLICATION_ERROR('-20999','79','');
    END IF;
        	-- to save the cancel log
            gm_pkg_common_cancel.gm_sav_cancel_log ('', po_cur.orderid, p_comments, p_cancelreason, v_unlink_code_id, p_userid) ;
            -- to update the batch details.
            gm_pkg_cm_batch.gm_void_batch_details (po_cur.orderid,v_ref_type, p_userid) ;
        END LOOP;
    END LOOP;

    SELECT count(1)
    INTO v_cnt_dtls
   FROM t9601_batch_details
  WHERE c9600_batch_id = v_batch_id 
  AND c9601_void_fl IS NULL;
  
  IF v_cnt_dtls = 0 THEN
   -- to void the Batch
        gm_pkg_cm_batch.gm_void_batch (v_batch_id, p_userid) ;
  END IF;
  p_out_msg := ''; -- remove the success message (fix the bug - 2235)
END gm_sav_unlink_po;

/******************************************************************
    * Description : Procedure used to process the batch
    ****************************************************************/
PROCEDURE gm_sav_process_inv_batch (
        p_batch_id IN T9600_BATCH.C9600_BATCH_ID%TYPE,
        p_inputstr     IN CLOB,
        p_userid       IN T9600_BATCH.C9600_CREATED_BY%TYPE)
AS
    v_batchid T9600_BATCH.C9600_BATCH_ID%TYPE;
    v_batch_type T9600_BATCH.C901_BATCH_TYPE%TYPE;
    v_pay_term  t503_invoice.c503_terms%TYPE;
    v_inv_id CLOB;
    v_order_date DATE;
    v_all_invid  CLOB;
    v_batch_status T9600_BATCH.C901_BATCH_STATUS%TYPE;
    v_hold_po CLOB;
    v_invoiced_po CLOB;
    v_hold_fl_cnt NUMBER;
    v_inv_cnt NUMBER;
    v_batch_cnt NUMBER;
    v_cnt NUMBER;
    v_company_id        t1900_company.c1900_company_id%TYPE;
    v_monthly_comp_id   t1900_company.c1900_company_id%TYPE;
    
    CURSOR inv_batch_cur
    IS
     SELECT t9601.C9601_REF_ID ref_id,  t501.c501_customer_po po, t501.c704_account_id id
     	,get_account_name (t501.c704_account_id) name, t501.c503_invoice_id invid
     	, t9601.c9601_invoice_date invDate ,t501.c101_dealer_id dealer_id
     	,t501.c501_order_date order_date,t501.c501_surgery_date surgery_date
     	,t9600.c901_invoice_closing_date invCloseDt ,t9600.c9600_date_of_issue_from dt_issue_from,t9600.c9600_date_of_issue_to dt_issue_to
   FROM t501_order t501, t9600_batch t9600, t9601_batch_details t9601
  WHERE t9600.c9600_batch_id      = t9601.c9600_batch_id
    AND t9601.c9601_ref_id        = t501.c501_order_id
    AND t9600.c9600_batch_id      = p_batch_id
    AND (t501.c901_ext_country_id IS NULL OR t501.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH
    AND t501.c501_delete_fl      IS NULL
    AND t501.c501_void_fl        IS NULL
    AND t9600.c9600_void_fl      IS NULL
    AND t9601.c9601_void_fl      IS NULL
--Cannot group by customer PO,Account ID here bcoz we need to generate invoice ID based on FIFO barcode scan(i.e The order in which the DO barcode is scanned and added to batch by user from screen)
ORDER BY NVL(t9601.c9601_email_fl,'N'),t9601.c9601_batch_detail_id, name;

BEGIN
		
	SELECT get_compid_frm_cntx()
	  INTO v_company_id 
	  FROM dual;
	  
	SELECT get_rule_value(v_company_id,'MONTHLY_INV_COMP') 
	  INTO v_monthly_comp_id 
	  FROM DUAL;


	v_batchid := p_batch_id;
	--
	  SELECT COUNT (1)
   		INTO v_batch_cnt
   	FROM t9600_batch
  		WHERE c9600_batch_id = p_batch_id
    	AND c9600_void_fl IS NULL;
    -- 
    IF 	v_batch_cnt = 0
    THEN
    	GM_RAISE_APPLICATION_ERROR('-20999','80','');
    END IF;	
    
	SELECT c901_batch_type, c901_batch_status
	 INTO v_batch_type, v_batch_status
   FROM t9600_batch
  WHERE c9600_batch_id = v_batchid
    AND c9600_void_fl IS NULL FOR UPDATE;
    --
    IF v_batch_status <> 18742
    THEN
    	GM_RAISE_APPLICATION_ERROR('-20999','81','');
    END IF;
    
    
        -- to generate the invoice 
	    FOR v_inv_batch IN inv_batch_cur
	    LOOP   
	    --
	    v_pay_term := NULL;
	    v_order_date := NULL;
	    -- to get the account pay terms 
	  	IF v_batch_type = '18751'
	  	THEN
	    	v_pay_term := get_account_pay_term(v_inv_batch.id);
	    	
	    ELSIF v_batch_type = '18756'
	    THEN
	    	v_pay_term := get_dealer_pay_term(v_inv_batch.dealer_id);
	    END IF;
	    
	    -- Check the hold fl
              SELECT COUNT(1) INTO v_hold_fl_cnt
   				FROM t501_order
  			WHERE c501_customer_po = v_inv_batch.po
    			AND c704_account_id  = v_inv_batch.id
    			AND c501_void_fl IS NULL
    			AND c501_delete_fl  IS NULL
    			AND c501_hold_fl    IS NOT NULL;
    			
    		IF	v_hold_fl_cnt <> 0
    		THEN
            	v_hold_po := v_hold_po || v_inv_batch.po ||', ';
            END IF;	
            
            
    		IF	v_inv_batch.invid IS NOT NULL
    		THEN
            	v_invoiced_po := v_invoiced_po || v_inv_batch.po ||', ';
            END IF;		
    		
	    -- to get min order date bcoz if in a batch there are  2 different DOs with same PO then the order date should be the date from Parent order (older date)
	    -- PBUG-1430: c503_Invoice_id IS NULL condition added to avoid generating duplicate invoice for the order which is already generated in BBA.
	    -- If the company support monthly invoice batch, setting the Date of Issue To as invoice date.
	     IF v_company_id = v_monthly_comp_id
		 THEN
		 	v_order_date := v_inv_batch.dt_issue_to;
		
	     ELSE
		     SELECT MIN(c501_order_date) INTO v_order_date
		       FROM t501_order t501
		      WHERE  t501.c704_account_id =  v_inv_batch.id
		        AND t501.c501_customer_po = v_inv_batch.po
		        AND (t501.c901_ext_country_id IS NULL OR t501.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH
		        AND t501.c501_delete_fl      IS NULL
		        AND t501.c501_void_fl        IS NULL
		        AND T501.C503_INVOICE_ID IS NULL
		        AND NVL (c901_order_type, -9999) NOT IN (
                SELECT t906.c906_rule_value
                  FROM t906_rules t906
                 WHERE t906.c906_rule_grp_id = 'ORDERTYPE'
                   AND c906_rule_id = 'EXCLUDE')
		        AND t501.c501_order_id IN (  SELECT t9601.c9601_ref_id 
		                                        FROM t9601_batch_details t9601
		                                      WHERE t9601.c9600_batch_id      = v_batchid
		                                        AND t9601.c9601_void_fl      IS NULL);
		
		 END IF; 
		-- validate the hold fl
		IF v_hold_fl_cnt = 0
		THEN
			-- 50200	Regular
			-- 50255	US customers
			--Since we cannot group by in cursor we need this select.. into bcoz we want only one invoice per PO
		    SELECT COUNT(1)  INTO v_cnt
			   FROM t501_order
			  WHERE c501_order_id    = v_inv_batch.ref_id
			    AND c501_void_fl    IS NULL
			    AND c501_delete_fl  IS NULL
			    AND c503_invoice_id IS NOT NULL; 
			
			IF v_cnt = 0
			THEN
				-- Checking the invoice date as user given date.
				v_order_date := NVL(v_inv_batch.invDate, v_order_date);
				
				IF v_batch_type = '18751'
		  		THEN
		    		gm_generate_invoice(v_inv_batch.po,v_inv_batch.id,v_pay_term,v_order_date,p_userid,'ADD',50200,50255,v_inv_id,v_inv_batch.dealer_id,v_batch_type);	
		    	ELSIF v_batch_type = '18756'
		    	THEN
		    		gm_generate_invoice(v_inv_batch.po,v_inv_batch.id,v_pay_term,v_order_date,p_userid,'ADD',50200,26240213,v_inv_id,v_inv_batch.dealer_id,v_batch_type);	
		    	END IF;
				
				v_all_invid := v_all_invid || v_inv_id ||',';

			 	IF v_company_id = v_monthly_comp_id
			 	THEN 	
			 		
			 		GM_SAVE_MONTHLY_INV_ATTRIBUTE(v_batchid,v_inv_id,v_inv_batch.invCloseDt,v_inv_batch.dt_issue_from,v_inv_batch.dt_issue_to,v_inv_batch.dealer_id,v_inv_batch.id,p_userid);
				END IF;	
			
			END IF;
		END IF; --v_hold_fl_cnt
		END LOOP;

	
	 IF v_hold_po IS NOT NULL
    THEN
    -- remove the last comma.
    v_hold_po := substr(v_hold_po, 0, LENGTH(v_hold_po)-2);
    -- show the error message.
    GM_RAISE_APPLICATION_ERROR('-20999','82',v_hold_po);
    END IF;
    
     IF v_invoiced_po IS NOT NULL
    THEN
    -- remove the last comma.
    v_invoiced_po := substr(v_invoiced_po, 0, LENGTH(v_invoiced_po)-2);
    -- show the error message.
    GM_RAISE_APPLICATION_ERROR('-20999','83',v_invoiced_po);
    END IF;
	-- update the batch status as (18743 - Processing In Progress)
	 UPDATE t9600_batch
	SET c901_batch_status  = 18743
	 , c9600_released_by = p_userid
	 , c9600_released_date = SYSDATE
	 , c9600_last_update_by = p_userid
	 , c9600_last_updated_date = SYSDATE
  WHERE c9600_batch_id = v_batchid
    AND c9600_void_fl IS NULL;
    
    --update batch email required flag for batch details
    IF v_batch_type = '18751'
	THEN
    	gm_sav_update_batch_attr(p_batch_id,p_inputstr,p_userid);
	 END IF;
END gm_sav_process_inv_batch;
	/******************************************************************
    * Description : Procedure used to update batch attribute email required flag.
    ****************************************************************/
PROCEDURE gm_sav_update_batch_attr (
        p_batch_id 	   IN T9600_BATCH.C9600_BATCH_ID%TYPE,
        p_inputstr     IN CLOB,
        p_userid       IN T9600_BATCH.C9600_CREATED_BY%TYPE)
AS
    v_strlen NUMBER := NVL (LENGTH (p_inputstr), 0) ;
    v_string CLOB   := p_inputstr;
    v_substring VARCHAR2 (4000) ;
    v_batch_detail_id t9601_batch_details.c9601_batch_detail_id%TYPE;
    v_email_fl t9601_batch_details.c9601_email_fl%TYPE;
    
    CURSOR batchpos_cur (p_batch_detail_id VARCHAR2)
    IS
		SELECT  t501.c501_customer_po po, t501.c704_account_id acctid
		   FROM t501_order t501, t9600_batch t9600, t9601_batch_details t9601
		  WHERE t9600.c9600_batch_id = t9601.c9600_batch_id
		    AND t9601.c9601_ref_id   = t501.c501_order_id
		    AND t9600.c9600_batch_id = p_batch_id   
		    AND t9600.c901_batch_status   = 18743 --Processing In Progress USE this condtn
		    AND t9600.c901_batch_type     = 18751 --Invoice Batch
		    AND (t501.c901_ext_country_id IS NULL OR t501.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH
		    AND t501.c501_delete_fl      IS NULL
		    AND t501.c501_void_fl        IS NULL
		    AND t9600.c9600_void_fl      IS NULL
		    AND t9601.c9601_void_fl      IS NULL
		    AND t501.c501_hold_fl        IS NULL
		    AND t9601.c9601_email_fl  IS NOT NULL
		    --Control Flag to check that if there are more than 1 order for a PO then the cursor should not select the other orders in the PO,
		    --since in the first iteration all the order will be updated
		    AND t9601.C9601_BATCH_DETAIL_ID = p_batch_detail_id
		    AND c503_invoice_id     IS NOT NULL;

    CURSOR batchorders_cur (p_account_id VARCHAR2, p_customer_po VARCHAR2)
    IS		    
		SELECT c501_order_id orderid
		   FROM t501_order
		  WHERE c704_account_id                   = p_account_id
		    AND c501_customer_po                  = p_customer_po
		    AND NVL (c901_order_type, - 999) NOT IN
		    (
		         SELECT token FROM v_in_list
		    )
		    AND c501_hold_fl        IS NULL
		    AND (c901_ext_country_id IS NULL OR c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH
		    AND c503_invoice_id     IS NOT NULL
		    AND c501_void_fl        IS NULL
		    AND c501_delete_fl      IS NULL;
		    
BEGIN
	 my_context.set_my_inlist_ctx (get_rule_value ('INVBATEXLIST', 'INVBATEXORDERTYPE'));
	 WHILE INSTR (v_string, '|') <> 0
	    LOOP
	        v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
	        v_string    := SUBSTR (v_string, INSTR (v_string, '|')    + 1) ;
	        v_batch_detail_id  := NULL;
	        v_email_fl := NULL;
	        --
	        v_batch_detail_id :=SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
	        v_substring   := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
	        v_email_fl := v_substring;
	        FOR cur_pos IN batchpos_cur(v_batch_detail_id)
	        LOOP
		        FOR cur_order IN batchorders_cur(cur_pos.acctid, cur_pos.po)
		        LOOP
					 UPDATE t9601_batch_details
					    SET C9601_LAST_UPDATE_BY = p_userid
					      , C9601_LAST_UPDATED_DATE = SYSDATE
					      , c9601_email_fl = v_email_fl
					  WHERE c9600_batch_id   = p_batch_id
					    AND c9601_ref_id     = cur_order.orderid
					    AND c901_ref_type    = 18751 -- Invoice Batch
					    AND c9601_void_fl   IS NULL;
				END LOOP;	    
			END LOOP;	 
		END LOOP;
	
END gm_sav_update_batch_attr;

	/******************************************************************
    * Description : Procedure used to insert/ void the order based on the PO changed.
    ****************************************************************/
PROCEDURE gm_sav_batch_updated_po (
        p_order_id   IN T501_order.c501_order_id%TYPE,
        p_new_po     IN T501_order.c501_customer_po%TYPE,
        p_batch_type IN T9600_BATCH.C901_BATCH_TYPE%TYPE,
        p_userid     IN t9601_batch_details.c9601_last_update_by%TYPE)
AS
    v_old_po_cnt      NUMBER;
    v_new_po_cnt      NUMBER;
    v_batch_cnt       NUMBER;
    v_order_cnt       NUMBER;
    v_parent_cnt      NUMBER;
    v_batch_order_cnt NUMBER;
    v_old_batch_id T9600_BATCH.C9600_BATCH_ID%TYPE;
    v_batch_id T9600_BATCH.C9600_BATCH_ID%TYPE;
    v_account_id T501_order.c704_account_id%TYPE;
    v_old_po T501_order.c501_customer_po%TYPE;
    --
    CURSOR child_order_cur
    IS
         SELECT c501_order_id orderid
           FROM t501_order
          WHERE c501_parent_order_id              = p_order_id
            AND c503_invoice_id                  IS NULL
            AND NVL (c901_order_type, - 999) NOT IN
            (
                 SELECT token FROM v_in_list
            )
        AND c501_hold_fl    IS NULL
        AND c503_invoice_id IS NULL
        AND c501_void_fl    IS NULL
        AND c501_delete_fl  IS NULL ;
BEGIN
    -- Exclude Order type from Invoice Batch. (2518,2519,2535)
    my_context.set_my_inlist_ctx (get_rule_value ('INVBATEXLIST', 'INVBATEXORDERTYPE')) ;
    -- to get the existing order info
     SELECT c704_account_id, NVL (c501_customer_po, '-999')
       INTO v_account_id, v_old_po
       FROM t501_order
      WHERE c501_order_id = p_order_id
        AND c501_void_fl IS NULL;
    -- if Check the Old PO is part of the batch
     SELECT COUNT (1)
       INTO v_old_po_cnt
       FROM t501_order t501, t9601_batch_details t9601, t9600_batch t9600
      WHERE t501.c704_account_id      = v_account_id
        AND t501.c501_customer_po     = v_old_po
        AND t501.c501_order_id        = t9601.c9601_ref_id
        AND t9600.c9600_batch_id      = t9601.c9600_batch_id
        AND t9601.c9601_ref_id        = p_order_id
        AND t9601.c901_ref_type       = p_batch_type -- invoice batch
        AND t9600.c901_batch_status  NOT IN (18743,18744)
        AND (t501.c901_ext_country_id IS NULL OR t501.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH
        AND t501.c503_invoice_id     IS NULL
        AND t501.c501_delete_fl      IS NULL
        AND t501.c501_void_fl        IS NULL
        AND t9601.c9601_void_fl      IS NULL
        AND t9600.c9600_void_fl 	 IS NULL;
    IF v_old_po_cnt                  <> 0 THEN
        -- This order Part of Batch.Check PO is changed.
        IF v_old_po <> p_new_po THEN
        	-- check the order is already exist or not
                SELECT COUNT(1)
                	INTO v_parent_cnt
   			      FROM t9601_batch_details
  				WHERE c9601_ref_id   = p_order_id
    				AND c901_ref_type  = p_batch_type
    				AND c9601_void_fl IS NULL;
                -- to insert the order to batch (child order also need to add the batch, because we update PO for all
                -- the child order)
                BEGIN
                 SELECT t9601.c9600_batch_id
                   INTO v_old_batch_id
                   FROM t501_order t501, t9601_batch_details t9601, t9600_batch t9600
                  WHERE t501.c704_account_id      = v_account_id
                    AND t501.c501_customer_po     = v_old_po
                    AND t501.c501_order_id        = t9601.c9601_ref_id
                    AND t9600.c9600_batch_id      = t9601.c9600_batch_id
                    AND t9601.c9601_ref_id        = p_order_id
                    AND t9600.c901_batch_status  NOT IN (18743,18744)
                    AND t9601.c901_ref_type       = p_batch_type -- invoice batch
                    AND (t501.c901_ext_country_id IS NULL OR t501.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH
                    AND t501.c501_delete_fl      IS NULL
                    AND t501.c501_void_fl        IS NULL
                    AND t9601.c9601_void_fl      IS NULL
        			AND t9600.c9600_void_fl 	 IS NULL;
            EXCEPTION
            WHEN NO_DATA_FOUND THEN
                v_old_batch_id := NULL;
            END;
           IF v_parent_cnt <> 0
           THEN
            	-- This handle the PO (Modify and Delete - part of the batch)
            	gm_pkg_cm_batch.gm_void_batch_details (p_order_id, p_batch_type, p_userid) ;
            	
            -- check any Child order.
            FOR child_ord IN child_order_cur
            LOOP
            	-- check the order is already exist or not
                SELECT COUNT(1)
                	INTO v_order_cnt
   			      FROM t9601_batch_details
  				WHERE c9601_ref_id   = child_ord.orderid
    				AND c901_ref_type  = p_batch_type
    				AND c9601_void_fl IS NULL;
                -- to insert the order to batch (child order also need to add the batch, because we update PO for all
                -- the child order)
           IF v_order_cnt <> 0
           THEN
                -- to void all the order, (child order also part of batch, because we update PO for all the child order
                -- )
                gm_pkg_cm_batch.gm_void_batch_details (child_ord.orderid, p_batch_type, p_userid) ;
           END IF; -- v_order_cnt     
            END LOOP;
            END IF; -- v_parent_cnt
            
             SELECT COUNT (1)
               INTO v_batch_cnt
               FROM t9601_batch_details
              WHERE c9600_batch_id = v_old_batch_id
                AND c9601_void_fl IS NULL;
            IF v_batch_cnt         = 0 THEN
             -- to check the batch is voided.
             SELECT COUNT (1)
               INTO v_batch_cnt
               FROM t9600_batch
              WHERE c9600_batch_id = v_old_batch_id
                AND c9600_void_fl IS NULL;
            	IF v_batch_cnt        <> 0 THEN
                	-- to void the Batch
                	gm_pkg_cm_batch.gm_void_batch (v_old_batch_id, p_userid) ;
                END IF;	
            END IF;
        END IF;
        -- to check the POs all are void, if all POs void then void the batch
        -- to get the batch id
    END IF;
    -- IF check any new PO is mapped to the part of batch.
     SELECT COUNT (1)
       INTO v_new_po_cnt
       FROM t501_order t501, t9601_batch_details t9601, t9600_batch t9600
      WHERE t501.c704_account_id      = v_account_id
        AND t501.c501_customer_po     = p_new_po
        AND t501.c501_order_id        = t9601.c9601_ref_id
        AND t9600.c9600_batch_id      = t9601.c9600_batch_id
        AND t9601.c901_ref_type       = p_batch_type -- invoice batch
        AND t9600.c901_batch_status  NOT IN (18743,18744)
        AND t501.c503_invoice_id     IS NULL
        AND (t501.c901_ext_country_id IS NULL OR t501.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH
        AND t501.c501_delete_fl      IS NULL
        AND t501.c501_void_fl        IS NULL
        AND t9601.c9601_void_fl      IS NULL
        AND t9600.c9600_void_fl 	 IS NULL;
    IF v_new_po_cnt                  <> 0 -- new po is mapped the batch, so this same order id to be move here.
        THEN
         SELECT COUNT (1)
           INTO v_batch_order_cnt
           FROM t9601_batch_details
          WHERE c9601_ref_id   = p_order_id
            AND c901_ref_type  = p_batch_type
            AND c9601_void_fl IS NULL;
        IF v_batch_order_cnt   = 0 THEN
            -- to get the existing batch id
             SELECT t9601.c9600_batch_id
               INTO v_batch_id
               FROM t501_order t501, t9601_batch_details t9601, t9600_batch t9600
              WHERE t501.c704_account_id      = v_account_id
                AND t501.c501_customer_po     = p_new_po
                AND t501.c501_order_id        = t9601.c9601_ref_id
                AND t9600.c9600_batch_id      = t9601.c9600_batch_id
                AND t9601.c901_ref_type       = p_batch_type -- invoice batch
                AND t9600.c901_batch_status  NOT IN (18743,18744)
                AND t501.c503_invoice_id     IS NULL
                AND (t501.c901_ext_country_id IS NULL OR t501.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH
                AND t501.c501_delete_fl      IS NULL
                AND t501.c501_void_fl        IS NULL
                AND t9601.c9601_void_fl      IS NULL
        		AND t9600.c9600_void_fl 	 IS NULL
                AND ROWNUM                    = 1;
            -- to call the existing procedure to insert the new record (t9601_batch_details) table.
            gm_pkg_cm_batch.gm_sav_batch_details (v_batch_id, p_order_id, p_batch_type, p_userid) ;
            -- check any Child order.
            FOR child_ord IN child_order_cur
            LOOP
            	-- check the order is already exist or not
                SELECT COUNT(1)
                	INTO v_order_cnt
   			      FROM t9601_batch_details
  				WHERE c9601_ref_id   = child_ord.orderid
    				AND c901_ref_type  = p_batch_type
    				AND c9601_void_fl IS NULL;
                -- to insert the order to batch (child order also need to add the batch, because we update PO for all
                -- the child order)
                IF v_order_cnt = 0
                THEN
                	gm_pkg_cm_batch.gm_sav_batch_details (v_batch_id, child_ord.orderid, p_batch_type, p_userid) ;
                END IF;	
            END LOOP;
        END IF; -- v_batch_order_cnt
    END IF;
    -- to check the order count
     SELECT COUNT (1)
       INTO v_order_cnt
       FROM t501_order t501
      WHERE t501.c704_account_id      = v_account_id
        AND t501.c501_customer_po     = p_new_po
        AND t501.c503_invoice_id     IS NULL
        AND (t501.c901_ext_country_id IS NULL OR t501.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH
        AND t501.c501_delete_fl      IS NULL
        AND t501.c501_void_fl        IS NULL;
    -- to check the batch count
     SELECT COUNT (1)
       INTO v_batch_cnt
       FROM t501_order t501, t9601_batch_details t9601, t9600_batch t9600
      WHERE t501.c704_account_id      = v_account_id
        AND t501.c501_customer_po     = p_new_po
        AND t501.c501_order_id        = t9601.c9601_ref_id
        AND t9600.c9600_batch_id      = t9601.c9600_batch_id
        AND t9601.c901_ref_type       = p_batch_type -- invoice batch
        AND t9600.c901_batch_status  NOT IN (18743,18744)
        AND t501.c503_invoice_id     IS NULL
        AND (t501.c901_ext_country_id IS NULL OR t501.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH
        AND t501.c501_delete_fl      IS NULL
        AND t501.c501_void_fl        IS NULL
        AND t9601.c9601_void_fl      IS NULL
        AND t9600.c9600_void_fl 	 IS NULL;
    -- if any different then log the
    IF v_new_po_cnt <> 0 AND v_order_cnt <> v_batch_cnt THEN
        GM_SAVE_STATUS_DETAILS (p_new_po, NULL, p_userid, NULL, SYSDATE, p_batch_type, 91102) ; -- 18751 (Invoice Batch
        -- ) 91102 (Order)
    END IF;
END gm_sav_batch_updated_po;

	/******************************************************************
    * Description : Procedure used to insert/ void the order based hold/invoiced/unhold.
    ****************************************************************/     
PROCEDURE gm_sav_batch_hold_po (
        p_order_id IN T501_order.c501_order_id%TYPE,
        p_po       IN T501_order.c501_customer_po%TYPE,
        p_accid    IN t503_invoice.c704_account_id%TYPE,
        p_type     IN VARCHAR2,
        p_userid   IN t9601_batch_details.c9601_last_update_by%TYPE)
AS
    v_po_cnt    NUMBER;
    v_batch_cnt NUMBER;
    v_order_cnt NUMBER;
    v_parent_cnt NUMBER;
    v_po_hold_cnt NUMBER;
    v_batch_id T9600_BATCH.C9600_BATCH_ID%TYPE;
    CURSOR cust_po_cur
    IS
         SELECT c501_order_id orderid
           FROM t501_order
          WHERE c704_account_id                   = p_accid
            AND c501_customer_po                  = p_po
            AND NVL (c901_order_type, - 999) NOT IN
            (
                 SELECT token FROM v_in_list
            )
        AND (c901_ext_country_id IS NULL OR c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH
        AND c503_invoice_id IS NULL
        AND c501_void_fl   IS NULL
        AND c501_delete_fl IS NULL
   ORDER BY c501_order_id;
    CURSOR invoiced_cur
    IS
         SELECT c501_order_id orderid
           FROM t501_order
          WHERE c704_account_id                   = p_accid
            AND c501_customer_po                  = p_po
            AND NVL (c901_order_type, - 999) NOT IN
            (
                 SELECT token FROM v_in_list
            )
        AND (c901_ext_country_id IS NULL OR c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH
        AND c501_void_fl   IS NULL
        AND c501_delete_fl IS NULL
   ORDER BY c501_order_id;
BEGIN
    -- Exclude Order type from Invoice Batch. (2518,2519,2535)
    my_context.set_my_inlist_ctx (get_rule_value ('INVBATEXLIST', 'INVBATEXORDERTYPE')) ;
    -- if Check the PO is part of the batch
     SELECT COUNT (1)
       INTO v_po_cnt
       FROM t501_order t501, t9601_batch_details t9601, t9600_batch t9600
      WHERE t501.c704_account_id      = p_accid
        AND t501.c501_customer_po     = p_po
        AND t501.c501_order_id        = t9601.c9601_ref_id
        AND t9600.c9600_batch_id      = t9601.c9600_batch_id
        AND t9601.c901_ref_type       = 18751 -- invoice batch
        AND t9600.c901_batch_status  NOT IN (18743,18744)
        AND (t501.c901_ext_country_id IS NULL OR t501.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH
        AND t501.c501_delete_fl      IS NULL
        AND t501.c501_void_fl        IS NULL
        AND t9601.c9601_void_fl      IS NULL
        AND t9600.c9600_void_fl 	 IS NULL;
    IF v_po_cnt                      <> 0 THEN
        -- to get the batch id
        BEGIN
             SELECT t9601.c9600_batch_id
               INTO v_batch_id
               FROM t501_order t501, t9601_batch_details t9601, t9600_batch t9600
              WHERE t501.c704_account_id      = p_accid
                AND t501.c501_customer_po     = p_po
                AND t501.c501_order_id        = t9601.c9601_ref_id
                AND t9600.c9600_batch_id      = t9601.c9600_batch_id
                AND t9601.c901_ref_type       = 18751 -- invoice batch
                AND t9600.c901_batch_status  NOT IN (18743,18744)
                AND (t501.c901_ext_country_id IS NULL OR t501.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH
                AND t501.c501_delete_fl      IS NULL
                AND t501.c501_void_fl        IS NULL
                AND t9601.c9601_void_fl      IS NULL
        		AND t9600.c9600_void_fl 	 IS NULL
                AND ROWNUM                    = 1;
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_batch_id := NULL;
        END;

        IF p_type = 'HOLD' THEN
        	 -- If one order changed to hold then, we cannot process the PO.
        	 -- so void the entire PO - (order)

        	 -- check the PO - Hold cnt
                SELECT COUNT (1)
              INTO v_po_hold_cnt
   				FROM t501_order
  			WHERE c501_customer_po = p_po
    			AND c704_account_id  = p_accid
    			AND c503_invoice_id IS NULL
    			AND c501_void_fl    IS NULL
    			AND c501_hold_fl    IS NOT NULL;
    		--	
    		IF v_po_hold_cnt <> 0
            THEN
            -- loop all order and void.
            FOR child_ord IN cust_po_cur
            LOOP
            	-- check the order is already exist or not
                SELECT COUNT(1)
                	INTO v_order_cnt
   			      FROM t9601_batch_details
  				WHERE c9601_ref_id   = child_ord.orderid
    				AND c901_ref_type  = 18751
    				AND c9601_void_fl IS NULL;
    			IF v_order_cnt <> 0	
    			THEN
                	-- to void the particular POs
                	gm_pkg_cm_batch.gm_void_batch_details (child_ord.orderid, 18751, p_userid) ;
                END IF; --v_order_cnt	
            END LOOP;
            END IF; -- v_parent_cnt
        ELSIF p_type = 'REMOVE_HOLD' THEN
        	-- check the PO - Hold cnt
                SELECT COUNT (1)
              INTO v_po_hold_cnt
   				FROM t501_order
  			WHERE c501_customer_po = p_po
    			AND c704_account_id  = p_accid
    			AND c503_invoice_id IS NULL
    			AND c501_void_fl    IS NULL
    			AND c501_hold_fl    IS NOT NULL;
    		--	
    		IF v_po_hold_cnt = 0
            THEN	
            FOR child_ord IN cust_po_cur
            LOOP
                -- check the order is already exist or not
                SELECT COUNT(1)
                	INTO v_order_cnt
   			      FROM t9601_batch_details
  				WHERE c9601_ref_id   = child_ord.orderid
    				AND c901_ref_type  = 18751
    				AND c9601_void_fl IS NULL;
    				-- to insert the order to batch (child order also need to add the batch, because we update PO for all
                -- the child order)
                IF v_order_cnt = 0
                THEN
                	gm_pkg_cm_batch.gm_sav_batch_details (v_batch_id, child_ord.orderid, 18751, p_userid) ;
                END IF;	
            END LOOP;
            END IF; -- v_po_hold_cnt
        ELSIF p_type   = 'INVOICED' THEN
            FOR v_pos IN invoiced_cur
            LOOP
            	-- check the order is already exist or not
                SELECT COUNT(1)
                	INTO v_order_cnt
   			      FROM t9601_batch_details
  				WHERE c9601_ref_id   = v_pos.orderid
    				AND c901_ref_type  = 18751
    				AND c9601_void_fl IS NULL;
    			IF v_order_cnt <> 0
                THEN
                -- void the all order to the batch
                gm_pkg_cm_batch.gm_void_batch_details (v_pos.orderid, 18751, p_userid) ;
                END IF;
            END LOOP;
        END IF;
        -- to check the POs all are void, if all POs void then void the batch
         SELECT COUNT (1)
           INTO v_batch_cnt
           FROM t9601_batch_details
          WHERE c9600_batch_id = v_batch_id
            AND c9601_void_fl IS NULL;
        IF v_batch_cnt         = 0 THEN
        	 -- to check the batch is voided.
             SELECT COUNT (1)
               INTO v_batch_cnt
               FROM t9600_batch
              WHERE c9600_batch_id = v_batch_id
                AND c9600_void_fl IS NULL;
            	IF v_batch_cnt        <> 0 THEN
            		-- to void the Batch
            		gm_pkg_cm_batch.gm_void_batch (v_batch_id, p_userid) ;
            	END IF;	
        END IF;
    END IF; -- v_po_cnt
END gm_sav_batch_hold_po;
/******************************************************************
* Description : Procedure used to void the order details
****************************************************************/
PROCEDURE gm_void_batch_order_dtls (
        p_order_id IN T501_order.c501_order_id%TYPE,
        p_userid   IN t9601_batch_details.c9601_last_update_by%TYPE)
AS
    v_batch_id T9600_BATCH.C9600_BATCH_ID%TYPE;
    v_order_cnt     NUMBER;
    v_child_cnt     NUMBER;
    v_batch_dtl_cnt NUMBER;
    v_batch_cnt     NUMBER;
    --
    CURSOR child_order_cur
    IS
         SELECT c501_order_id orderid
           FROM t501_order
          WHERE c501_parent_order_id              = p_order_id
            AND NVL (c901_order_type, - 999) NOT IN
            (
                 SELECT token FROM v_in_list
            )
        AND (c901_ext_country_id IS NULL OR c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH
        AND c503_invoice_id     IS NULL
        AND c501_void_fl        IS NULL
        AND c501_delete_fl      IS NULL
   ORDER BY c501_order_id;
BEGIN
    -- Exclude Order type from Invoice Batch. (2518,2519,2535)
    my_context.set_my_inlist_ctx (get_rule_value ('INVBATEXLIST', 'INVBATEXORDERTYPE')) ;
    -- check the order is part of batch.
     SELECT COUNT (1)
       INTO v_order_cnt
       FROM t9601_batch_details
      WHERE c9601_ref_id   = p_order_id
        AND c901_ref_type  = 18751
        AND c9601_void_fl IS NULL;
	--
    IF v_order_cnt        <> 0 THEN
    	-- to get the batch id.
         SELECT c9600_batch_id
           INTO v_batch_id
           FROM t9601_batch_details
          WHERE c9601_ref_id   = p_order_id
            AND c901_ref_type  = 18751
            AND c9601_void_fl IS NULL;
        -- void the order to the batch
        gm_pkg_cm_batch.gm_void_batch_details (p_order_id, 18751, p_userid) ;
        -- check any Child order.
        FOR child_ord IN child_order_cur
        LOOP
            -- check the child order is already exist or not
             SELECT COUNT (1)
               INTO v_child_cnt
               FROM t9601_batch_details
              WHERE c9601_ref_id   = child_ord.orderid
                AND c901_ref_type  = 18751
                AND c9601_void_fl IS NULL;
            --
            IF v_child_cnt <> 0 THEN
                -- to void child order
                gm_pkg_cm_batch.gm_void_batch_details (child_ord.orderid, 18751, p_userid) ;
            END IF; -- v_child_cnt
        END LOOP;
        
        -- to check the POs all are void, if all POs void then void the batch
         SELECT COUNT (1)
           INTO v_batch_dtl_cnt
           FROM t9601_batch_details
          WHERE c9600_batch_id = v_batch_id
            AND c9601_void_fl IS NULL;
        IF v_batch_dtl_cnt     = 0 THEN
            -- to check the batch is voided.
             SELECT COUNT (1)
               INTO v_batch_cnt
               FROM t9600_batch
              WHERE c9600_batch_id = v_batch_id
                AND c9600_void_fl IS NULL;
            IF v_batch_cnt        <> 0 THEN
                -- to void the Batch
                gm_pkg_cm_batch.gm_void_batch (v_batch_id, p_userid) ;
            END IF; --v_batch_cnt
        END IF; -- v_batch_dtl_cnt
    END IF; -- v_order_cnt
END gm_void_batch_order_dtls;
	/******************************************************************
	* Description : Procedure used to entered order - save to batch
	****************************************************************/
PROCEDURE gm_sav_ord_batch_barcode_dtls (
        p_batch_id    IN T9600_BATCH.c9600_batch_id%TYPE,
        p_account_id  IN T501_order.c704_account_id%TYPE,
        p_customer_po IN T501_order.c501_customer_po%TYPE,
        p_action      IN T901_code_lookup.c901_code_id%TYPE,
        p_email_fl    IN T9601_batch_details.c9601_email_fl%TYPE,
        p_inv_date IN DATE,
        p_userid      IN T9601_batch_details.c9601_created_by%TYPE)
AS
    v_batch_type T9600_BATCH.C901_BATCH_TYPE%TYPE;
    v_batch_status T9600_BATCH.C901_BATCH_STATUS%TYPE;
    v_batch_id T9600_BATCH.c9600_batch_id%TYPE;
    v_batch_new_stat T9600_BATCH.c901_batch_status%TYPE;
    v_inprogress_cnt NUMBER;
    v_relase_cnt     NUMBER;
    v_cnt            NUMBER;
    v_batch_cnt      NUMBER;
    v_company_id    t1900_company.c1900_company_id%TYPE;
    v_current_date  DATE;
    --
    CURSOR cust_po_cur
    IS
         SELECT c501_order_id orderid
           FROM t501_order
          WHERE c704_account_id                   = p_account_id
            AND c501_customer_po                  = p_customer_po
            AND NVL (c901_order_type, - 999) NOT IN
            (
                 SELECT token FROM v_in_list
            )
        AND (c901_ext_country_id IS NULL OR c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH
        AND c503_invoice_id     IS NULL
        AND c501_void_fl        IS NULL
        AND c501_delete_fl      IS NULL
        AND C1900_COMPANY_ID   = v_company_id
   ORDER BY c501_order_id;
BEGIN
	    
	   SELECT get_compid_frm_cntx()
		 INTO v_company_id 
		 FROM dual;	
	
	-- Exclude Order type from Invoice Batch. (2518,2519,2535)
    my_context.set_my_inlist_ctx (get_rule_value ('INVBATEXLIST', 'INVBATEXORDERTYPE')) ;
    v_batch_id := p_batch_id;
    -- validate the PO
    gm_pkg_ac_ar_batch_rpt.gm_validate_batch_ord_barcode (p_account_id, p_customer_po, p_userid) ;
    --
    IF p_batch_id IS NOT NULL THEN
         SELECT COUNT (1)
           INTO v_batch_cnt
           FROM t9600_batch
          WHERE c9600_batch_id = p_batch_id
            AND c9600_void_fl IS NULL;
        --
        IF v_batch_cnt = 0 THEN
            GM_RAISE_APPLICATION_ERROR('-20999','80','');
        END IF;
         SELECT c901_batch_type, c901_batch_status
           INTO v_batch_type, v_batch_status
           FROM t9600_batch
          WHERE c9600_batch_id = p_batch_id
            AND c9600_void_fl IS NULL;
        --
    END IF;
    --
    SELECT COUNT (1)
           INTO v_inprogress_cnt
           FROM t9600_batch t9600
          WHERE t9600.c9600_initiated_by = p_userid
            AND t9600.c901_batch_type    = 18751 -- Invoice Batch
            AND t9600.c901_batch_status  = 18741 -- In progress
            AND t9600.C1900_COMPANY_ID   = v_company_id
            AND t9600.c9600_void_fl     IS NULL;
            
    IF p_action = 18853 -- Create In Progress batch and Add PO
        THEN
        -- check the batch is exist
        IF v_batch_id IS NOT NULL OR v_inprogress_cnt =1 THEN
            GM_RAISE_APPLICATION_ERROR('-20999','84',v_batch_id);
        END IF;
        -- to create the new batch id
        gm_pkg_cm_batch.gm_sav_batch (18751, 18741, p_userid, v_batch_id) ;
        -- to add the batch details.
    ELSIF p_action = 18854 -- Add PO to In Progress Batch
        THEN
        IF v_inprogress_cnt              = 0 THEN
            GM_RAISE_APPLICATION_ERROR('-20999','85','');
        END IF;
        --
        IF v_batch_status <> 18741 THEN
            GM_RAISE_APPLICATION_ERROR('-20999','86','');
        END IF;
    END IF;
    -- To validate the Invoice date
    SELECT  TRUNC(CURRENT_DATE) INTO v_current_date FROM DUAL;
    --
    IF v_current_date < p_inv_date
    THEN
    	raise_application_error (-20949, '');
    END IF;
    --
    -- to insert all orders to the batch.
    FOR po_cur IN cust_po_cur
    LOOP
        -- validate the order(exist)
         SELECT COUNT (1)
           INTO v_cnt
           FROM t9601_batch_details
          WHERE c9601_ref_id   = po_cur.orderid
            AND c901_ref_type  = 18751
            AND c9601_void_fl IS NULL;
        IF v_cnt               = 0 THEN
            -- to insert the orders to batch details.
            gm_pkg_cm_batch.gm_sav_batch_details (v_batch_id, po_cur.orderid, 18751, p_userid) ;
            
            UPDATE t9601_batch_details
			   SET c9601_email_fl = p_email_fl
			   	 , c9601_invoice_date = p_inv_date
				 , C9601_LAST_UPDATE_BY  = p_userid
				 , C9601_LAST_UPDATED_DATE = SYSDATE
			 WHERE c9600_batch_id = v_batch_id
			   AND c9601_ref_id = po_cur.orderid
			   AND c901_ref_type = 18751
			   AND c9601_void_fl IS NULL;            
            
        END IF;
    END LOOP;
    -- 
    IF p_action = 18855 -- Release Batch to Pending Processing
        THEN
        -- check the batch is exist
        IF v_batch_id IS NULL THEN
            GM_RAISE_APPLICATION_ERROR('-20999','87','');
        END IF;
        -- check the status
        IF v_batch_status <> 18741 THEN
            GM_RAISE_APPLICATION_ERROR('-20999','88',p_batch_id);
        END IF;
         SELECT COUNT (1)
           INTO v_relase_cnt
           FROM t9601_batch_details
          WHERE c9600_batch_id = p_batch_id
            AND c9601_void_fl IS NULL;
        --
        IF v_relase_cnt = 0 THEN
            GM_RAISE_APPLICATION_ERROR('-20999','89','');
        END IF;
        -- update the status as Pending Processing.
        -- 18751 invoice batch
        -- 18742 Pending Processing
        gm_pkg_cm_batch.gm_sav_batch (v_batch_type, 18742, p_userid, v_batch_id) ;
    END IF;
END gm_sav_ord_batch_barcode_dtls;
/******************************************************************
    * Description : Procedure used to save the DO print to batch
    ****************************************************************/
PROCEDURE gm_sav_process_do_summary (
        p_inputstr IN CLOB,
        p_userid       IN T9600_BATCH.C9600_CREATED_BY%TYPE,
        p_batchid  OUT T9600_BATCH.C9600_BATCH_ID%TYPE)
AS
    v_batchid T9600_BATCH.C9600_BATCH_ID%TYPE;
    v_batch_type T9600_BATCH.C901_BATCH_TYPE%TYPE;
     v_strlen NUMBER := NVL (LENGTH (p_inputstr), 0) ;
    v_string CLOB   := p_inputstr;
    v_substring VARCHAR2 (4000) ;
    v_order_id t501_order.c501_order_id%TYPE;
    v_batch_cnt NUMBER;
BEGIN
	IF v_strlen <>0 
	THEN
	-- to create the new batch #
	gm_pkg_cm_batch.gm_sav_batch(18752,18741,p_userid,v_batchid);
	--
	WHILE INSTR (v_string, '|') <> 0
    LOOP
        v_substring   := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
        v_string      := SUBSTR (v_string, INSTR (v_string, '|')    + 1) ;
        v_order_id  := NULL;
        --
        v_order_id  := v_substring;
        -- to call the existing procedure to insert the new record (t9601_batch_details) table.
            gm_pkg_cm_batch.gm_sav_batch_details (v_batchid, v_order_id, 18752, p_userid) ;
     END LOOP;
        -- batch status updated so, released by and date to be update
       UPDATE t9600_batch
		SET c901_batch_status  = 18743
	 		, c9600_released_by = p_userid
	 		, c9600_released_date = SYSDATE
	 		, c9600_last_update_by = p_userid
	 		, c9600_last_updated_date = SYSDATE
  		WHERE c9600_batch_id = v_batchid
    		AND c9600_void_fl IS NULL;
    END IF; -- v_strlen
    --
    p_batchid :=  v_batchid;
END gm_sav_process_do_summary;

/******************************************************************
* Description : Procedure used to save the PO for monthly invoice
****************************************************************/
PROCEDURE gm_update_monthly_inv_po(
	p_inputstr IN CLOB,
	p_userid IN t101_user.c101_user_id%TYPE)
AS
    v_strlen NUMBER := NVL (LENGTH (p_inputstr), 0) ;
    v_string CLOB   := p_inputstr;
    v_substring VARCHAR2 (4000) ;
    v_transid t501_order.c501_order_id%TYPE;
    v_customer_po T501_order.c501_customer_po%TYPE;
    v_order_type  T501_order.c901_order_type%TYPE;
    v_invoice_id  T501_order.c503_invoice_id%TYPE;

BEGIN

	 WHILE INSTR (v_string, '|') <> 0
    LOOP
        v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
        v_string    := SUBSTR (v_string, INSTR (v_string, '|')    + 1) ;
        v_customer_po := NULL;
        --
        v_transid 		:=SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
        v_substring   	:= SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
        v_customer_po   := v_substring;

       -- Geting order type and invoice id from order
       SELECT C901_ORDER_TYPE,C503_INVOICE_ID 
       INTO v_order_type,v_invoice_id
       FROM T501_ORDER 
       WHERE  C501_ORDER_ID = v_transid
       AND C501_VOID_FL IS NULL;

       IF v_order_type = '2524' THEN --2524 -Price adjustment order
       -- Order type is Price adjustment order , updating new customer PO and remove invoice id
       UPDATE  t501_order
	     SET  c501_customer_po = v_customer_po
	     	 ,c503_invoice_id  = ''
		     ,c501_last_updated_by = p_userid
		     ,c501_last_updated_date = CURRENT_DATE
	  	 WHERE  c501_order_id = v_transid    ;
	  	 
	  	 -- voiding  existing price adjustment order
	  	 UPDATE t503_invoice 
	  	 SET  c503_void_fl = 'Y'
	  	 	 ,c503_last_updated_by = p_userid
	  	 	 ,c503_last_updated_date = CURRENT_DATE
	  	 WHERE c503_invoice_id = v_invoice_id
	  	 AND  c503_void_fl is null;
	  	 
	  	  
       ELSE
	      
       UPDATE  t501_order
		     SET  c501_customer_po = v_customer_po
			     ,c501_last_updated_by = p_userid
			     ,c501_last_updated_date = CURRENT_DATE
		   WHERE  c501_order_id = v_transid
		     AND  c501_customer_po IS NULL
		     ;
		 END IF;
        
        
	END LOOP;
	
  END gm_update_monthly_inv_po;
END gm_pkg_ac_ar_batch_trans;
/
