create or replace
PACKAGE BODY GM_PKG_AC_FA_APP_DOWNLOAD_DATA
AS
PROCEDURE gm_fch_audit_name(
    p_audit_status IN varchar,
    p_out_audit_name OUT TYPES.cursor_type )
AS
  v_status_id VARCHAR2(500);
BEGIN
  v_status_id:= p_audit_status;
  my_context.set_my_inlist_ctx (v_status_id);
  BEGIN
    OPEN p_out_audit_name 
      FOR 
      SELECT t2650.c2650_field_sales_audit_id Audit_id, t2650.c2650_audit_name    
        FROM t2650_field_sales_audit t2650 
       WHERE t2650.c901_audit_status in 
         (SELECT token 
            FROM v_in_list)
        ORDER BY t2650.c2650_audit_name;
  END;
END gm_fch_audit_name;

PROCEDURE gm_fch_setup_data(
    p_audit_id      IN t2650_field_sales_audit.c2650_field_sales_audit_id%TYPE,
    p_audit_user_id IN T2654_AUDIT_SYNC_LOG.c2654_audit_user_id%TYPE,
    p_status        IN varchar,
    p_out_locked_data OUT TYPES.cursor_type )
AS
  v_status_id VARCHAR2(500);

BEGIN
  v_status_id:= p_status;
  my_context.set_my_inlist_ctx (v_status_id);
  
  OPEN p_out_locked_data FOR SELECT rownum S_NO,Lock_data.* FROM
  (SELECT t2650.c2650_field_sales_audit_id,
    t2651.C701_distributor_id,
    get_distributor_name (t2651.C701_distributor_id) Field_Sales ,
    t2651.C2652_FIELD_SALES_LOCK_ID
    --,t2658.c2658_audit_user_id User_id
    ,
    t101.C101_user_sh_name Auditor ,
    TO_CHAR(t2651.c2651_start_date,'MM/dd/yyyy') Audit_Start_Date ,
    TO_CHAR(t2651.c2651_end_date,'MM/dd/yyyy') Audit_End_Date ,
    SUM(t2657.c2657_tag_qty) Tag_Qty,
    SUM(t2657.c2657_ops_qty) OPs_Qty,
    SUM(t2657.c2657_qty) System_Qty ,
    -- get_code_name(t2651.c901_reconciliation_status) c901_status_id
    --,get_code_name (t2651.c901_reconciliation_status) spineIT_Status
    get_code_name(get_fa_sync_status(p_audit_id,p_audit_user_id, t2651.c2651_field_sales_details_id, p_status)) spineIT_Status -- Locked
    ,
    t2651.c2651_field_sales_details_id,
    t2651.c901_reconciliation_status
  FROM t2651_field_sales_details t2651,
    t2650_field_sales_audit t2650,
    t2658_audit_team t2658,
    t101_user t101,
    t2657_field_sales_details_lock t2657
  WHERE t2650.c2650_field_sales_audit_id = t2651.c2650_field_sales_audit_id
  AND t2651.C2652_field_sales_lock_id    = t2657.C2652_field_sales_lock_id
  AND t101.c101_user_id                  = t2658.C2658_Audit_User_Id
  AND t2650.c2650_field_sales_audit_id   = p_audit_id
  AND t2658.c2651_field_sales_details_id = t2651.c2651_field_sales_details_id
  AND t2651.c2651_void_fl               IS NULL
  AND t2650.c2650_void_fl               IS NULL
  AND t2657.c2657_void_FL               IS NULL
  GROUP BY t2650.c2650_field_sales_audit_id,
    t2651.C701_distributor_id,
    t101.C101_user_sh_name,
    t2651.c2651_start_date,
    t2651.C2652_FIELD_SALES_LOCK_ID,
    t2651.c901_reconciliation_status,
    c2651_end_date,
    t2651.c2651_field_sales_details_id,
    t2651.c901_reconciliation_status
  ) Lock_data
  --, t2658.c2658_audit_user_id, t2651.C2652_FIELD_SALES_LOCK_ID
  ;
END gm_fch_setup_data ;
--
/*******************************************************
* Description : Procedure to download t2650_field_sales_audit for given Audit Id
* Author    : Yogabalakrishnan Kannan
* Modified By :
* Modified Dt : 09/04/2012
*******************************************************/
--
-- t2650_field_sales_audit
PROCEDURE gm_fch_fs_audit(
    p_audit_id     IN t2650_field_sales_audit.c2650_field_sales_audit_id%TYPE,
    p_audit_status IN varchar2,
    p_out_fs_audit OUT TYPES.cursor_type )
AS
  v_status_id VARCHAR2(500);
BEGIN
  v_status_id:= p_audit_status;
  my_context.set_my_inlist_ctx (v_status_id);
  OPEN p_out_fs_audit 
    FOR 
    SELECT C2650_FIELD_SALES_AUDIT_ID ,C2650_AUDIT_NAME ,C2650_AUDIT_OWNER ,C2650_START_DATE ,C2650_END_DATE ,C2650_VOID_FL ,C2650_CREATED_BY ,
    C2650_CREATED_DATE ,C2650_LAST_UPDATED_BY ,C2650_LAST_UPDATED_DATE ,C901_AUDIT_STATUS
  --,C2650_LOCK_DATE
     FROM t2650_field_sales_audit 
    WHERE c901_audit_status in 
     (SELECT token 
        FROM v_in_list) --18502 'Locked' and 18503 'Downloaded'
      AND c2650_void_fl IS NULL 
      AND c2650_field_sales_audit_id = NVL(p_audit_id,c2650_field_sales_audit_id);
END gm_fch_fs_audit;
--
/*******************************************************
* Description : Procedure to download t2651_field_sales_details for given Audit Id
* Author    : Yogabalakrishnan Kannan
* Modified By :
* Modified Dt : 09/04/2012
*******************************************************/
--
--  t2651_field_sales_details
PROCEDURE gm_fch_fs_audit_details(
    p_audit_id IN t2650_field_sales_audit.c2650_field_sales_audit_id%TYPE,
    p_out_fs_audit_dtl OUT TYPES.cursor_type )
AS
BEGIN
  OPEN p_out_fs_audit_dtl FOR SELECT t2651.C2651_FIELD_SALES_DETAILS_ID ,t2651.C2651_START_DATE ,t2651.C2651_END_DATE ,t2651.C2651_VOID_FL ,t2651.C2651_CREATED_BY ,t2651.C2651_CREATED_DATE ,t2651.C2651_LAST_UPDATED_BY ,t2651.C2651_LAST_UPDATED_DATE ,t2651.C2650_FIELD_SALES_AUDIT_ID ,t2651.C701_DISTRIBUTOR_ID ,t2651.C2652_FIELD_SALES_LOCK_ID ,C901_RECONCILIATION_STATUS FROM t2651_field_sales_details t2651, t2650_field_sales_audit t2650 WHERE t2650.c2650_field_sales_audit_id = t2651.c2650_field_sales_audit_id AND t2650.c2650_field_sales_audit_id = NVL(p_audit_id,t2650.c2650_field_sales_audit_id) AND t2651.c2651_void_fl IS NULL AND t2650.c2650_void_fl IS NULL ;
END gm_fch_fs_audit_details;
--
/*******************************************************
* Description : Procedure to download t2652_field_sales_lock for given Audit Id
* Author    : Yogabalakrishnan Kannan
* Modified By :
* Modified Dt : 09/04/2012
*******************************************************/
--
--t2652_field_sales_lock
PROCEDURE gm_fch_fs_lock(
    p_audit_id IN t2650_field_sales_audit.c2650_field_sales_audit_id%TYPE,
    p_out_fs_lock OUT TYPES.cursor_type )
AS
BEGIN
  OPEN p_out_fs_lock FOR SELECT C2652_FIELD_SALES_LOCK_ID ,C2652_LOCKED_DATE ,C2652_VOID_FL ,C2652_LOCKED_BY ,C701_DISTRIBUTOR_ID ,C2650_FIELD_SALES_AUDIT_ID FROM t2652_field_sales_lock WHERE c2652_void_fl IS NULL AND c2650_field_sales_audit_id = NVL(p_audit_id,c2650_field_sales_audit_id);
END gm_fch_fs_lock;
--
/*******************************************************
* Description : Procedure to download t2657_field_sales_details_lock for given Audit Id
* Author    : Yogabalakrishnan Kannan
* Modified By :
* Modified Dt : 09/04/2012
*******************************************************/
--
--t2657_field_sales_details_lock
PROCEDURE gm_fch_fs_details_lock(
    p_audit_id IN t2650_field_sales_audit.c2650_field_sales_audit_id%TYPE,
    p_out_fs_details_lock OUT TYPES.cursor_type )
AS
BEGIN
  OPEN p_out_fs_details_lock FOR SELECT t2657.C2657_FIELD_SALES_DETAILS_LOCK ,t2657.C2657_QTY ,t2657.C205_PART_NUMBER_ID ,t2657.C2652_FIELD_SALES_LOCK_ID ,t2657.C207_SET_ID ,t2657.C2657_VOID_FL ,t2657.C2657_TAG_QTY ,t2657.C2657_OPS_QTY FROM t2657_field_sales_details_lock t2657, t2650_field_sales_audit t2650, t2652_field_sales_lock t2652 WHERE t2652.c2652_field_sales_lock_id = t2657.c2652_field_sales_lock_id AND t2650.c2650_field_sales_audit_id = t2652.c2650_field_sales_audit_id AND t2652.c2652_void_fl IS NULL AND t2657.c2657_void_fl IS NULL AND t2650.c2650_void_fl IS NULL AND t2650.c2650_field_sales_audit_id = NVL(p_audit_id,t2650.c2650_field_sales_audit_id);
END gm_fch_fs_details_lock;
--
/*******************************************************
* Description : Procedure to download t2658_audit_team for given Audit Id
* Author    : Yogabalakrishnan Kannan
* Modified By :
* Modified Dt : 09/05/2012
*******************************************************/
--
--t2658_audit_team
PROCEDURE gm_fch_audit_team(
    p_audit_id IN t2650_field_sales_audit.c2650_field_sales_audit_id%TYPE,
    p_out_audit_team OUT TYPES.cursor_type )
AS
BEGIN
  OPEN p_out_audit_team FOR SELECT t2658.C2658_AUDIT_TEAM_ID ,t2658.C2658_AUDIT_USER_ID ,t2658.C2658_TAG_START_RANGE ,t2658.C2658_TAG_END_RANGE ,t2658.C2658_VOID_FL ,t2658.C2651_FIELD_SALES_DETAILS_ID ,t2658.C2658_CREATED_BY ,t2658.C2658_CREATED_DATE ,t2658.C2658_LAST_UPDATED_BY ,t2658.C2658_LAST_UPDATED_DATE FROM t2658_audit_team t2658, t2650_field_sales_audit t2650, t2651_field_sales_details t2651 WHERE t2651.c2651_field_sales_details_id = t2658.c2651_field_sales_details_id AND t2650.c2650_field_sales_audit_id = t2651.c2650_field_sales_audit_id AND t2658.c2658_void_fl IS NULL AND t2651.c2651_void_fl IS NULL AND t2650.c2650_void_fl IS NULL AND t2650.c2650_field_sales_audit_id = NVL(p_audit_id,t2650.c2650_field_sales_audit_id);
END gm_fch_audit_team;
--
/*******************************************************
* Description : Procedure to download t2659_field_sales_ref_lock for given Audit Id
* Author    : Yogabalakrishnan Kannan
* Modified By :
* Modified Dt : 09/04/2012
*******************************************************/
--
--t2659_field_sales_ref_lock
PROCEDURE gm_fch_fs_ref_lock(
    p_audit_id IN t2650_field_sales_audit.c2650_field_sales_audit_id%TYPE,
    p_out_fs_ref_lock OUT TYPES.cursor_type )
AS
BEGIN
  OPEN p_out_fs_ref_lock FOR SELECT t2659.C2659_FIELD_SALES_REF_LOCK_ID ,t2659.C2659_CONTROL_NUMBER ,t2659.C2659_REF_ID ,t2659.C2659_REF_DATE ,t2659.C2659_QTY ,t2659.C901_REF_TYPE ,t2659.C205_PART_NUMBER_ID ,t2659.C207_SET_ID ,t2659.C2652_FIELD_SALES_LOCK_ID ,t2659.C2659_VOID_FL ,t2659.C5010_TAG_ID FROM t2659_field_sales_ref_lock t2659, t2650_field_sales_audit t2650, t2652_field_sales_lock t2652 WHERE t2652.c2652_field_sales_lock_id = t2659.c2652_field_sales_lock_id AND t2650.c2650_field_sales_audit_id = t2652.c2650_field_sales_audit_id AND t2652.c2652_void_fl IS NULL AND t2659.c2659_void_fl IS NULL AND t2650.c2650_void_fl IS NULL AND t2650.c2650_field_sales_audit_id = NVL(p_audit_id,t2650.c2650_field_sales_audit_id);
END gm_fch_fs_ref_lock;
--
/*******************************************************
* Description : Procedure to download t2663_field_sales_set_part_dtl for given Audit Id
* Author    : Yogabalakrishnan Kannan
* Modified By :
* Modified Dt : 09/04/2012
*******************************************************/
--
--t2663_field_sales_set_part_dtl
PROCEDURE gm_fch_fs_set_part_detail(
    p_audit_id IN t2650_field_sales_audit.c2650_field_sales_audit_id%TYPE,
    p_out_fs_set_part_detail OUT TYPES.cursor_type )
AS
BEGIN
  OPEN p_out_fs_set_part_detail FOR SELECT C2663_FIELD_SALES_SET_PART_ID ,C2650_FIELD_SALES_AUDIT_ID ,C2663_REF_ID ,C901_REF_TYPE ,C2663_VOID_FL ,C2663_CREATED_BY ,C2663_CREATED_DATE ,C2663_LAST_UPDATED_BY ,C2663_LAST_UPDATED_DATE FROM t2663_field_sales_set_part_dtl WHERE c2663_void_fl IS NULL AND c2650_field_sales_audit_id = NVL(p_audit_id,c2650_field_sales_audit_id);
END gm_fch_fs_set_part_detail;
-- Download Master Data
--
/*******************************************************
* Description : Procedure to download t207_set_master
* Author    : Yogabalakrishnan Kannan
* Modified By :
* Modified Dt : 09/05/2012
*******************************************************/
--
--t207_set_master
PROCEDURE gm_fch_set_master(
    p_group_id         IN t207_set_master.c901_set_grp_type%Type,
    p_create_date      IN t207_set_master.c207_created_date%type,
    p_last_update_date IN t207_set_master.c207_last_updated_date%type,
    p_out_set_master OUT TYPES.cursor_type )
AS
BEGIN
  OPEN p_out_set_master FOR SELECT C207_SET_ID ,C207_SET_NM ,C207_SET_DESC ,C202_PROJECT_ID ,C207_CREATED_DATE ,C207_CREATED_BY ,C207_LAST_UPDATED_DATE ,C207_LAST_UPDATED_BY ,C207_CATEGORY ,C207_TYPE ,C207_SKIP_RPT_FL ,C901_SET_GRP_TYPE ,C207_SEQ_NO ,C207_VOID_FL ,C207_OBSOLETE_FL ,C901_CONS_RPT_ID ,C901_VANGUARD_TYPE ,C901_STATUS_ID ,C207_REV_LVL ,C207_CORE_SET_FL ,C207_SET_SALES_SYSTEM_ID ,C901_HIERARCHY ,C901_MINSET_LOGIC ,C207_RELEASE_FOR_SALE FROM t207_set_master t207 WHERE c901_set_grp_type = NVL(p_group_id,c901_set_grp_type) AND t207.c207_void_fl IS NULL;
END gm_fch_set_master;
--
/*******************************************************
* Description : Procedure to download t208_set_details
* Author    : Yogabalakrishnan Kannan
* Modified By :
* Modified Dt : 09/05/2012
*******************************************************/
--
--t207_set_master
PROCEDURE gm_fch_set_details(
    p_group_id         IN t207_set_master.c901_set_grp_type%Type,
    p_last_update_date IN t208_set_details.c208_last_updated_date%type,
    p_out_set_details OUT TYPES.cursor_type )
AS
BEGIN
  OPEN p_out_set_details FOR SELECT t208.C208_SET_DETAILS_ID ,t208.C207_SET_ID ,t208.C205_PART_NUMBER_ID ,t208.C208_SET_QTY ,t208.C208_INSET_FL ,t208.C208_VOID_FL ,t208.C208_CRITICAL_QTY ,t208.C208_CRITICAL_TAG ,t208.C208_CRITICAL_FL ,t208.C901_CRITICAL_TYPE ,t208.C208_LAST_UPDATED_DATE ,t208.C208_LAST_UPDATED_BY ,t208.C901_UNIT_TYPE FROM t208_set_details t208, t207_set_master t207 WHERE t207.c207_set_id = t208.c207_set_id AND t207.c901_set_grp_type = NVL(p_group_id,t207.c901_set_grp_type) AND t208.c208_void_fl IS NULL;
END gm_fch_set_details;
--
/*******************************************************
* Description : Procedure to download t205_part_number
* Author    : Yogabalakrishnan Kannan
* Modified By :
* Modified Dt : 09/05/2012
*******************************************************/
--
--t205_part_number
PROCEDURE gm_fch_part_number(
    p_group_id         IN t207_set_master.c901_set_grp_type%Type,
    p_create_date      IN t207_set_master.c207_created_date%type,
    p_last_update_date IN t207_set_master.c207_last_updated_date%type,
    p_out_part_number OUT TYPES.cursor_type )
AS
BEGIN
  OPEN p_out_part_number 
    FOR 
    SELECT t205.C205_PART_NUMBER_ID ,t205.C202_PROJECT_ID ,t205.C205_PART_NUM_DESC ,t205.C205_PRODUCT_FAMILY ,t205.C205_PRODUCT_MATERIAL ,
      t205.C205_PRODUCT_CLASS ,t205.C205_TOLERANCE_LVL ,t205.C205_INSPECTION_CRITERIA ,t205.C205_MEASURING_DEV ,t205.C205_AQL ,t205.C205_REV_NUM ,
      t205.C205_PACKAGING_PRIM ,t205.C205_PACKAGING_SEC ,t205.C205_FILLER_DETAILS ,t205.C205_CREATED_DATE ,t205.C205_CREATED_BY ,t205.C205_LAST_UPDATED_DATE ,
      t205.C205_LAST_UPDATED_BY ,get_partnumber_qty(t205.C205_PART_NUMBER_ID,90800) C205_QTY_IN_STOCK ,t205.C205_EQUITY_PRICE ,t205.C205_MATERIAL_CERT_FL ,t205.C205_LOANER_PRICE ,t205.C205_INSERT_ID ,
      t205.C205_MATERIAL_SPEC ,t205.C205_MULTIPLE_PROJ_FL ,t205.C205_LIST_PRICE ,t205.C205_ACTIVE_FL ,t205.C205_PART_DRAWING ,t205.C205_COST ,t205.C205_INSPECTION_FILE_ID ,
      t205.C205_COMPLIANCE_CERT_FL ,t2052.C2052_CONSIGNMENT_PRICE ,t205.C205_SUB_ASMBLY_FL ,t205.C205_SUB_COMPONENT_FL ,t205.C205_INSPECTION_REV ,get_partnumber_qty(t205.C205_PART_NUMBER_ID,90813) C205_QTY_IN_QUARANTINE ,
      t205.C901_TYPE ,t205.C901_ACTION ,  t205.C205_LAST_UPDATE_TRANS_ID ,get_partnumber_qty(t205.C205_PART_NUMBER_ID,90814) C205_QTY_IN_BULK ,get_partnumber_qty(t205.C205_PART_NUMBER_ID,90812) C205_QTY_IN_RETURNS_HOLD ,t205.C205_TRACKING_IMPLANT_FL ,
      t205.C901_STATUS_ID ,t205.C205_RELEASE_FOR_SALE ,t205.C205_SUPPLY_FL ,t205.C205_CROSSOVER_FL ,t205.C205_HARD_TEST_FL ,t205.C901_UOM ,t205.C205_CORE_PART_FL ,
      t205.C205_MIN_ACCPT_REV_LEL ,t205.C901_RPF ,get_partnumber_qty(t205.C205_PART_NUMBER_ID,90815) C205_QTY_IN_PACKAGING ,t205.C205_COST_HISTORY_FL 
    FROM t205_part_number t205 ,t2052_part_price_mapping t2052
   WHERE exists  
    (SELECT t208.c205_part_number_id 
       FROM t208_set_details t208 , t207_set_master t207
      WHERE t207.c207_set_id = t208.c207_set_id
       AND t207.c901_set_grp_type =NVL(p_group_id,t207.c901_set_grp_type) 
       AND t208.C205_PART_NUMBER_ID =  t205.C205_PART_NUMBER_ID)
    AND t205.c205_part_number_id = t2052.c205_part_number_id(+)
    AND t2052.c2052_void_fl(+) is null
    AND t2052.c1900_company_id(+) = '1000'; --Company Id Hard Coded to 1000 Since Audit will be done only in GMNA.
  
END gm_fch_part_number;
--
/*******************************************************
* Description : Procedure to download t205d_part_attribute for given type Id
* Author    : Yogabalakrishnan Kannan
* Modified By :
* Modified Dt : 09/04/2012
*******************************************************/
--
--t205d_part_attribute
PROCEDURE gm_fch_part_attribute(
    p_Att_type_id      IN t205d_part_attribute.c901_attribute_type%TYPE,
    p_create_date      IN t205d_part_attribute.c205d_created_date%type,
    p_last_update_date IN t205d_part_attribute.c205d_last_updated_date%type,
    p_out_part_attribute OUT TYPES.cursor_type )
AS
BEGIN
  OPEN p_out_part_attribute FOR SELECT C2051_PART_ATTRIBUTE_ID ,C205D_ATTRIBUTE_VALUE ,C205D_VOID_FL ,C205D_CREATED_BY ,C205D_CREATED_DATE ,C205D_LAST_UPDATED_BY ,C205D_LAST_UPDATED_DATE ,C205_PART_NUMBER_ID ,C901_ATTRIBUTE_TYPE FROM t205d_part_attribute WHERE c205d_void_fl IS NULL AND c901_attribute_type = NVL(p_Att_type_id,c901_attribute_type);
  -- and c205d_created_date>p_create_date
  -- or c205d_last_updated_date>p_last_update_date;
END gm_fch_part_attribute;
--
/*******************************************************
* Description : Procedure to download t5010_tag
* Author    : Yogabalakrishnan Kannan
* Modified By :
* Modified Dt : 09/05/2012
*******************************************************/
--
--t5010_tag
PROCEDURE gm_fch_tag(
    p_out_tag OUT TYPES.cursor_type )
AS
BEGIN
  OPEN p_out_tag FOR SELECT C5010_TAG_ID ,C5010_CONTROL_NUMBER ,C5010_LAST_UPDATED_TRANS_ID ,C5010_LOCATION_ID ,C5010_VOID_FL ,C5010_HISTORY_FL ,C5010_LOCK_FL ,C5010_CREATED_BY ,C5010_CREATED_DATE ,C5010_LAST_UPDATED_BY ,C5010_LAST_UPDATED_DATE ,C205_PART_NUMBER_ID ,C901_TRANS_TYPE ,C207_SET_ID ,C901_LOCATION_TYPE ,C901_STATUS ,C704_ACCOUNT_ID ,C901_SUB_LOCATION_TYPE ,C5010_SUB_LOCATION_ID ,C5010_SUB_LOCATION_COMMENTS ,C106_ADDRESS_ID ,C703_SALES_REP_ID ,C901_INVENTORY_TYPE ,C5010_RETAG_ID FROM t5010_tag t5010;
END gm_fch_tag;
--
/*******************************************************
* Description : Procedure to download t701_distributor
* Author    : Yogabalakrishnan Kannan
* Modified By :
* Modified Dt : 09/05/2012
*******************************************************/
--
--t701_distributor
PROCEDURE gm_fch_distributor(
    p_out_distributor OUT TYPES.cursor_type )
AS
BEGIN
  OPEN p_out_distributor FOR SELECT C701_DISTRIBUTOR_ID ,C701_DISTRIBUTOR_NAME ,C701_DIST_SH_NAME ,C701_REGION ,C701_DISTRIBUTOR_INCHARGE ,C701_CONTACT_PERSON ,C701_BILL_ADD1 ,C701_COMMISSION_PERCENT ,C701_BILL_NAME ,C701_BILL_ADD2 ,C701_BILL_STATE ,C701_BILL_COUNTRY ,C701_BILL_CITY ,C701_BILL_ZIP_CODE ,C701_FAX ,C701_PHONE_NUMBER ,C701_CONTRACT_START_DATE ,C701_SHIP_NAME ,C701_CONTRACT_END_DATE ,C701_SHIP_ADD1 ,C701_SHIP_ADD2 ,C701_ACTIVE_FL ,C701_COMMENTS ,C701_SHIP_CITY ,C701_CREATED_DATE ,C701_SHIP_STATE ,C701_CREATED_BY ,C701_SHIP_COUNTRY ,C701_LAST_UPDATED_DATE ,C701_SHIP_ZIP_CODE ,C701_LAST_UPDATED_BY ,C701_EMAIL_ID ,C701_VOID_FL ,C901_DISTRIBUTOR_TYPE ,C101_PARTY_INTRA_MAP_ID ,C701_EXT_REF_ID ,C901_EXT_COUNTRY_ID ,C701_BILL_LATITUDE ,C701_BILL_LONGITUDE ,C701_SHIP_LATITUDE ,C701_SHIP_LONGITUDE FROM t701_distributor t701 WHERE t701.c701_void_fl IS NULL;
END gm_fch_distributor;
--
/*******************************************************
* Description : Procedure to download t703_sales_rep
* Author    : Yogabalakrishnan Kannan
* Modified By :
* Modified Dt : 09/05/2012
*******************************************************/
--
--t703_sales_rep
PROCEDURE gm_fch_sales_rep(
    p_out_sales_rep OUT TYPES.cursor_type )
AS
BEGIN
  OPEN p_out_sales_rep FOR SELECT C703_SALES_REP_ID ,C703_SALES_REP_NAME ,C701_DISTRIBUTOR_ID ,C703_PHONE_NUMBER ,C703_REP_CATEGORY ,C703_SHIP_ADD1 ,C703_SHIP_ADD2 ,C703_PAGER_NUMBER ,C703_EMAIL_ID ,C703_SHIP_CITY ,C703_SHIP_STATE ,C703_SHIP_COUNTRY ,C703_SHIP_ZIP_CODE ,C703_CREATED_DATE ,C703_CREATED_BY ,C703_LAST_UPDATED_DATE ,C703_LAST_UPDATED_BY ,C703_SALES_REP_SH_NAME ,C702_TERRITORY_ID ,C703_ACTIVE_FL ,C703_SALES_FL ,C703_VOID_FL ,C101_PARTY_ID ,C901_DESIGNATION ,C703_START_DATE ,C703_END_DATE ,C703_EXT_REF_ID ,C901_EXT_COUNTRY_ID FROM t703_sales_rep t703 WHERE t703.c703_void_fl IS NULL;
END gm_fch_sales_rep;
--
/*******************************************************
* Description : Procedure to download t704_account
* Author    : Yogabalakrishnan Kannan
* Modified By :
* Modified Dt : 09/05/2012
*******************************************************/
--
--t704_account
PROCEDURE gm_fch_account(
    p_out_account OUT TYPES.cursor_type )
AS
BEGIN
  OPEN p_out_account FOR SELECT C704_ACCOUNT_ID ,C704_ACCOUNT_NM ,C703_SALES_REP_ID ,C704_ACCOUNT_SH_NAME ,C704_CONTACT_PERSON ,C704_PHONE ,C704_FAX ,C704_BILL_ADD1 ,C704_PRICING_CATEGORY ,C704_BILL_NAME ,C704_BILL_ADD2 ,C704_BILL_CITY ,C704_BILL_STATE ,C704_BILL_COUNTRY ,C704_BILL_ZIP_CODE ,C704_PAYMENT_TERMS ,C704_INCEPTION_DATE ,C704_SHIP_NAME ,C704_ACTIVE_FL ,C704_SHIP_ADD1 ,C704_CREATED_DATE ,C704_SHIP_ADD2 ,C704_SHIP_CITY ,C704_CREATED_BY ,C704_LAST_UPDATED_DATE ,C704_SHIP_STATE ,C704_LAST_UPDATED_BY ,C704_SHIP_COUNTRY ,C704_SHIP_ZIP_CODE ,C704_COMMENTS ,C704_EMAIL_ID ,C704_PRICE_CAT_FL ,C704_VOID_FL ,C901_ACCOUNT_TYPE ,C704_CREDIT_RATING ,C704_RATING_HISTORY_FL ,C704_CREDIT_RATING_DATE ,C901_COMPANY_ID ,C704_EXT_REF_ID ,C901_EXT_COUNTRY_ID ,C704_BILL_LATITUDE ,C704_BILL_LONGITUDE ,C704_SHIP_LATITUDE ,C704_SHIP_LONGITUDE FROM t704_account t704 WHERE t704.c704_void_fl IS NULL;
END gm_fch_account;
--
/*******************************************************
* Description : Procedure to download t706_address
* Author    : Yogabalakrishnan Kannan
* Modified By :
* Modified Dt : 09/05/2012
*******************************************************/
--
--t106_address
PROCEDURE gm_fch_address(
    p_out_address OUT TYPES.cursor_type )
AS
BEGIN
  OPEN p_out_address FOR SELECT C106_ADDRESS_ID ,C106_ADD1 ,C106_ADD2 ,C106_CITY ,C901_STATE ,C901_COUNTRY ,C106_ZIP_CODE ,C106_SEQ_NO ,C106_VOID_FL ,C106_INACTIVE_FL ,C106_CREATED_DATE ,C106_CREATED_BY ,C106_LAST_UPDATED_DATE ,C101_PARTY_ID ,C106_LAST_UPDATED_BY ,C901_ADDRESS_TYPE ,C106_PRIMARY_FL ,C106_LONGITUDE ,C106_LATITUDE FROM t106_address t106 WHERE t106.c106_void_fl IS NULL;
END gm_fch_address;
--
/*******************************************************
* Description : Procedure to download t901_code_lookup
* Author    : Yogabalakrishnan Kannan
* Modified By :
* Modified Dt : 09/05/2012
*******************************************************/
--
-- t901_code_lookup
PROCEDURE gm_fch_lookup(
    p_grp_id IN VARCHAR2 ,
    p_out_lookup OUT TYPES.cursor_type )
AS
  v_code_grp_id VARCHAR2(500);
  v_token       VARCHAR2 (20);
BEGIN
  /* v_code_grp_id :=  p_grp_id;
  my_context.set_my_inlist_ctx (v_code_grp_id);
  SELECT COUNT (token)
  INTO v_incidentcnt
  FROM v_in_list vList, t901_code_lookup t901
  WHERE vList.t901_code_grp_id = t901.c901_code_grp(+);
  */
  OPEN p_out_lookup FOR SELECT C901_CODE_ID ,C901_CODE_NM ,C901_CODE_GRP ,C901_ACTIVE_FL ,C901_CODE_SEQ_NO ,C902_CODE_NM_ALT ,C901_CONTROL_TYPE ,C901_CREATED_DATE ,C901_CREATED_BY ,C901_LAST_UPDATED_DATE ,C901_LAST_UPDATED_BY ,C901_VOID_FL ,C901_LOCK_FL FROM t901_code_lookup t901 WHERE t901.c901_void_fl IS NULL;
  /*
  AND v_in_list vList, t901_code_lookup t901
  AND vList.token = t901.c901_code_grp(+);
  */
END gm_fch_lookup;
--t101_user
PROCEDURE gm_fch_user(
    p_user_id IN t101_user.c101_user_id%type,
    p_out_user OUT types.cursor_type)
AS
BEGIN
  OPEN p_out_user FOR SELECT c101_user_id ,c101_user_sh_name ,c101_user_f_name ,c101_user_l_name ,c101_email_id ,c101_dept_id ,c101_access_level_id ,c101_hire_dt ,c101_termination_date ,c101_work_phone ,c101_created_date ,c101_created_by ,c101_last_updated_date ,c101_last_updated_by ,c101_ssn_number ,c101_other_phone ,c101_ph_extn ,c101_dept_id ,c701_distributor_id ,c101_address ,c901_user_status ,c901_user_type ,c101_party_id ,c101_title ,c101_date_format FROM t101_user ;
END gm_fch_user;
PROCEDURE gm_fch_tag_details(
    p_lock_id IN t701_distributor.c701_distributor_id%TYPE,
    p_out_tag_details OUT TYPES.cursor_type )
AS
BEGIN
  OPEN p_out_tag_details FOR SELECT rownum S_NO,Lock_data.* FROM
  (SELECT t2659.c5010_tag_id Tag_id ,
    t2659.c205_part_number_id Part_Number ,
    t205.c205_part_num_desc Part_Number_Desc ,
    t2659.c2659_control_number Control_Number ,
    t2659.c2659_ref_id Ref_Id ,
    t2659.c207_set_id Set_Id ,
    DECODE (TO_CHAR (t5010.c901_location_type) , '4120', TO_CHAR (get_distributor_name (t5010.c5010_location_id)) , '4123', TO_CHAR (get_user_name (t5010.c5010_location_id)) , TO_CHAR (get_code_name (t5010.c5010_location_id)) ) Location ,
    get_code_name (t5010.c901_status) Status ,
    DECODE (TO_CHAR (t5010.c901_sub_location_type) , '50221', TO_CHAR (get_distributor_name (t5010.c5010_sub_location_id)) , TO_CHAR (get_rep_name (t5010.c5010_sub_location_id)) ) Sub_location ,
    get_code_name (t5010.c901_sub_location_type) Sub_Location_Type ,
    get_account_name (t5010.c704_account_id) Account ,
    get_code_name(t5010.c901_inventory_type) Inventory_Type
  FROM t2659_field_sales_ref_lock t2659 ,
    t5010_tag t5010 ,
    t205_part_number t205
  WHERE t2659.c2652_field_sales_lock_id = p_lock_id
  AND t205.c205_part_number_id          = t2659.c205_part_number_id
  AND t5010.c5010_tag_id (+)            = t2659.c5010_tag_id
  ) Lock_data;
END gm_fch_tag_details;
PROCEDURE gm_upd_sync_status(
    p_audit_user_id IN t101_user.c101_user_id%type,
    p_status        IN t901_code_lookup.c901_code_id%type,
    p_fs_details_id IN VARCHAR )
AS
  v_fs_details_id VARCHAR2(500);
BEGIN
  v_fs_details_id := p_fs_details_id;
  my_context.set_my_inlist_ctx (v_fs_details_id);
  UPDATE t2651_field_sales_details
  SET c901_reconciliation_status      = p_status --18524 - Data Downloaded
  WHERE c2651_field_sales_details_id IN
    (SELECT token FROM v_in_list
    );
  INSERT
  INTO T2654_AUDIT_SYNC_LOG
    (
      C2654_AUDIT_SYNC_LOG_ID,
      c2651_field_sales_details_id,
      c2654_audit_user_id,
      C901_AUDIT_STATUS,
      c2654_sync_dt
    )
  SELECT S2654_sync_log.nextval,
    token,
    p_audit_user_id,
    p_status,
    TO_CHAR(CURRENT_DATE,'DD-MON-RR')
  FROM v_in_list;
  --VALUES(p_fs_details_id,p_audit_user_id,to_char(CURRENT_DATE,'DD-MON-RR'));
END gm_upd_sync_status;
END GM_PKG_AC_FA_APP_DOWNLOAD_DATA;
/