
--@"c:\database\packages\accounting\gm_pkg_ac_fa_post_err.bdy";

create or replace PACKAGE BODY gm_pkg_ac_fa_post_err
IS

/********************************************************************************
* Description : Procedure will pull the posting errors data
*********************************************************************************/
PROCEDURE gm_fch_posting_error (
		p_audit_id IN t2652_field_sales_lock.c2650_field_sales_audit_id%TYPE,
		p_dist_id  IN t2651_field_sales_details.c701_distributor_id%TYPE,
		p_status   IN t2662_posting_error.c901_status%TYPE,
		p_posting_error_cur OUT TYPES.cursor_type )
AS
BEGIN
	OPEN p_posting_error_cur 
	FOR 
	SELECT get_distributor_name(t2651.c701_distributor_id) dist_name
		, t2662.c207_set_id setid
		, get_set_name(t2662.c207_set_id) setdesc
		, t2662.c205_part_number_id pnum
		, get_partnum_desc(t2662.c205_part_number_id) pdesc
		, t2662.c2662_ref_id transferid
		, get_code_name(t2662.c901_ref_type) reftype
		, get_code_name(t2662.c901_reason) reason
		, get_code_name(t2662.c901_status) status
		, t2656.c5010_tag_id tagid
	FROM t2662_posting_error t2662, t2651_field_sales_details t2651 ,t2656_audit_entry t2656 
	WHERE t2662.c2662_void_fl  IS NULL
	AND t2662.c2651_field_sales_details_id = t2651.c2651_field_sales_details_id
	AND t2662.c901_status = p_status
	AND t2651.c2650_field_sales_audit_id = p_audit_id
	AND t2651.c701_distributor_id = DECODE(p_dist_id,0,t2651.c701_distributor_id,p_dist_id)
	AND t2656.c2656_audit_entry_id = t2662.c2656_audit_entry_id
	AND t2651.c2651_void_fl IS NULL 
	AND t2656.c2656_void_fl IS NULL;
END gm_fch_posting_error;

	
/********************************************************************************
* Description : Procedure to check the posting validation
* Author    : arockia prasath
*********************************************************************************/
PROCEDURE gm_post_validation (
		p_audit_entry_id IN CLOB,
        p_user_id  IN t2656_audit_entry.c2656_last_updated_by%TYPE)
AS
    v_error_count NUMBER;
    v_count       NUMBER;
    v_locked_date DATE;
    v_set_id  t2656_audit_entry.c207_set_id%TYPE;
    v_ref_type t2656_audit_entry.c901_ref_type%TYPE;
    v_tagid t2656_audit_entry.c5010_tag_id%TYPE;
    v_pnum t2656_audit_entry.c205_part_number_id%TYPE;
    v_field_sales_details_id t2651_field_sales_details.c2651_field_sales_details_id%TYPE;
    v_audit_id  t2652_field_sales_lock.c2650_field_sales_audit_id%TYPE;
    v_dist_id   t2651_field_sales_details.c701_distributor_id%TYPE;
    v_last_trans t5011_tag_log.c5011_last_updated_trans_id%TYPE;
    v_qty t505_item_consignment.c505_item_qty%TYPE;
    v_excess_count NUMBER;
   	v_audit_entry_id  t2656_audit_entry.c2656_audit_entry_id%TYPE;
   	v_string    CLOB := p_audit_entry_id;
    v_substring VARCHAR2 (4000) ;
    v_return_status VARCHAR2(10);     
    v_transfer_status BOOLEAN;
    v_rma_id VARCHAR2(200);
    v_excess_id  t504a_consignment_excess.c504_consignment_id%TYPE;
    v_excess_qty  t505_item_consignment.c505_item_qty%TYPE;
    v_transfer_id  t921_transfer_detail.c920_transfer_id%TYPE;
   	v_transfer_qty  t505_item_consignment.c505_item_qty%TYPE;
BEGIN
	
   WHILE INSTR (v_string, '|') <> 0
    LOOP 
    	v_audit_entry_id     := NULL;
     	v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
        v_string    := SUBSTR (v_string, INSTR (v_string, '|')    + 1) ;
        v_audit_entry_id := v_substring ;
      
        
        --verify the records in transfer(to/From), Extra, Missing status
        SELECT COUNT(1) INTO v_count
           FROM t2656_audit_entry t2656, t2651_field_sales_details t2651 ,t2652_field_sales_lock t2652 
          WHERE t2656.c2656_audit_entry_id = v_audit_entry_id
            AND t2656.c901_ref_type               IS NOT NULL
            ---transfer(to/From), Extra, Missing, Void Tag Only
            AND t2656.c901_ref_type               IN (51027, 51024, 51023, 51028, 18558)
            AND t2651.c2651_field_sales_details_id = t2656.c2651_field_sales_details_id  
 			AND t2651.c2650_field_sales_audit_id = t2652.c2650_field_sales_audit_id 
 			AND t2652.c701_distributor_id=t2651.c701_distributor_id AND t2651.c2652_field_sales_lock_id = t2652.c2652_field_sales_lock_id 
            AND t2651.c2651_void_fl is NULL AND t2652.c2652_void_fl is NULL AND t2656.c2656_void_fl IS NULL            
            AND t2656.c901_posting_option IS NULL;
            
             --updating the existing record as closed status  
     	UPDATE t2662_posting_error 
     	    SET c901_status  = '53102', c2662_last_updated_by = p_user_id, c2662_last_updated_date = SYSDATE --53102-closed 
     	    WHERE c2656_audit_entry_id = v_audit_entry_id
        	AND c2662_void_fl               IS NULL;
        
   IF (v_count  > 0) THEN
   --fetching set id,tag id,ref type,pnum,locked date,field sales details id
    	SELECT t2656.c207_set_id , t2656.c901_ref_type , t2656.c205_part_number_id 
          , DECODE(t2656.c2656_retag_fl,'Y',NVL(t2656.c5010_retag_id,t2656.c5010_tag_id),t2656.c5010_tag_id)
          ,trunc(t2652.c2652_locked_date) , t2656.c2651_field_sales_details_id 
          , t2651.c2650_field_sales_audit_id , t2651.c701_distributor_id 
          INTO v_set_id, v_ref_type, v_pnum , v_tagid, v_locked_date, v_field_sales_details_id 
          , v_audit_id , v_dist_id 
           FROM t2656_audit_entry t2656, t2651_field_sales_details t2651 ,t2652_field_sales_lock t2652 
          WHERE t2656.c2656_audit_entry_id = v_audit_entry_id
            AND t2656.c901_ref_type               IS NOT NULL
            ---transfer(to/From), Extra, Missing, Void Tag Only
            AND t2656.c901_ref_type               IN (51027, 51024, 51023, 51028, 18558)
            AND t2651.c2651_field_sales_details_id = t2656.c2651_field_sales_details_id  
 			AND t2651.c2650_field_sales_audit_id = t2652.c2650_field_sales_audit_id 
 			AND t2652.c701_distributor_id=t2651.c701_distributor_id AND t2651.c2652_field_sales_lock_id = t2652.c2652_field_sales_lock_id 
            AND t2651.c2651_void_fl is NULL AND t2652.c2652_void_fl is NULL AND t2656.c2656_void_fl IS NULL            
            AND t2656.c901_posting_option IS NULL  FOR UPDATE;
     
        
    IF v_ref_type <>  '18558' THEN -- NOT EQUAL TO Void Tag Only
    
     	---return validation for tag
    	gm_post_tag_return_check( v_tagid , v_set_id,  v_pnum , v_locked_date ,  v_dist_id, p_user_id, v_return_status, v_rma_id);
    	
    	---transfer validation for tag
	    gm_post_tag_transfer_check (v_tagid , v_dist_id,  v_ref_type, v_locked_date , v_set_id,  v_pnum, p_user_id ,v_transfer_id ,v_transfer_qty, v_transfer_status) ;
	    
	    --we make sure tag id in return and it should not be in transfer then updating posting error as return
    	IF v_return_status = 'TRUE' AND v_transfer_status = FALSE THEN
    		gm_excess_check (v_rma_id, v_pnum ,v_excess_id ,v_excess_qty);
	 		gm_sav_posting_error (v_field_sales_details_id, v_set_id, v_pnum, v_ref_type,v_rma_id, v_excess_qty, p_user_id, '52185' ---RETURN
            ,v_audit_entry_id,v_excess_id
            ) ;	
    	END IF;
    	
    	--we make sure tag id in return and it should not be in transfer then updating posting error as return ( the tag return some other dist)
    	IF v_return_status = 'DIST' THEN
    		gm_sav_posting_error (v_field_sales_details_id, v_set_id, v_pnum, v_ref_type,v_rma_id, NULL, p_user_id, '52185' ---RETURN
            ,v_audit_entry_id,NULL
            ) ;	
    	END IF;
    	
    	--we make sure tag id in transfer and it should not be in return then updating posting error as Transfer Done
    	IF v_transfer_status = TRUE AND v_return_status = 'FALSE' THEN
    	 gm_sav_posting_error (v_field_sales_details_id, v_set_id, v_pnum, v_ref_type,v_transfer_id, v_transfer_qty, p_user_id, '52183' ---Transfer Done
            ,v_audit_entry_id,NULL
            ) ;
        END IF;
        --tag is not in transfer and return then we are checking all the open return and transfer transaction 
    	IF v_return_status = 'FALSE' AND v_transfer_status = FALSE THEN 
		  	 ---all return validation without tag
		    gm_post_return_check (v_audit_entry_id , v_tagid , v_set_id,  v_ref_type, v_pnum , v_locked_date , v_field_sales_details_id , v_dist_id, p_user_id) ;
		   
		    ---all transfer validation without tag
		    gm_post_transfer_check (v_audit_entry_id ,v_tagid , v_dist_id,  v_ref_type, v_locked_date , v_field_sales_details_id , v_set_id , v_pnum , p_user_id) ;
	    END IF;
	    
	    --loaner to consignment check 
	    gm_loaner_to_consignment_check(v_audit_entry_id, v_tagid, v_locked_date, v_set_id, v_pnum, v_ref_type,v_dist_id, v_field_sales_details_id,p_user_id);
	        
	    --inactive distributor check
	    gm_inactive_distributor_check (v_audit_entry_id, p_user_id) ;
	    
	    --incorrect transfer check
	    gm_incorrect_transfer_check  (v_audit_entry_id , v_set_id,  v_ref_type, v_pnum ,  v_field_sales_details_id , v_dist_id, p_user_id) ;
   END IF;
    
    --qty mismatch check
    gm_qty_mismatch_check  (v_audit_entry_id , v_dist_id, v_set_id,  v_ref_type, v_pnum ,  v_field_sales_details_id , p_user_id) ;
    
    -- If errors then change the status to pending approval
     SELECT COUNT (1)
       INTO v_error_count
       FROM t2662_posting_error
      WHERE c2656_audit_entry_id = v_audit_entry_id
        AND c901_status = 53101 --open
        AND c2662_void_fl               IS NULL;
    --
    IF (v_error_count > 0) THEN
         UPDATE t2656_audit_entry t2656
        SET t2656.c2656_last_updated_by          = p_user_id, t2656.c2656_last_updated_date = SYSDATE,
            t2656.c901_reconciliation_status     = 51036 -- flaged
          WHERE t2656.c2656_audit_entry_id = v_audit_entry_id
        		AND t2656.c2656_void_fl IS NULL;
    END IF;
    --
    END IF;-- v_count  > 0
    
  END LOOP;
     
END gm_post_validation;

/********************************************************************************
* Description : Procedure to check the posting return validation for tag
* Author    : arockia prasath
*********************************************************************************/
PROCEDURE gm_post_tag_return_check (
		p_tagid IN t2656_audit_entry.c5010_tag_id%TYPE,
        p_set_id IN t2656_audit_entry.c207_set_id%TYPE,
        p_pnum IN t2656_audit_entry.c205_part_number_id%TYPE,
        p_locked_date IN t2652_field_sales_lock.c2652_locked_date%TYPE,
        p_dist_id  IN t2651_field_sales_details.c701_distributor_id%TYPE,
        p_user_id  IN t2656_audit_entry.c2656_last_updated_by%TYPE,
        p_status OUT VARCHAR,
        p_rma_id OUT VARCHAR
        )
AS
    
   CURSOR v_return_cur
    IS 
    
    		 SELECT t506.c506_rma_id rma_id ,t506.c701_distributor_id dist_id ,get_distributor_name(t506.c701_distributor_id) dist_name
				   FROM t506_returns t506, t507_returns_item t507, t5011_tag_log t5011
				  WHERE EXISTS
				    (
				         SELECT t205d.c205_part_number_id
				           FROM t205d_part_attribute t205d
				          WHERE t205d.c901_attribute_type = 92340
				            AND t205d.c205_part_number_id = t507.c205_part_number_id
				    )
				    --AND t506.c701_distributor_id = p_dist_id
				    AND t507.c205_part_number_id = p_pnum
				    AND t506.c207_set_id         = p_set_id
				    AND t506.c506_rma_id         = t507.c506_rma_id
				    AND t506.c506_created_date    >= p_locked_date 
				    AND t506.c506_reason         = 3313 --Consignment Return
				    AND t506.c506_status_fl <= 2 -- <=approved pending credit
				    AND t507.c507_status_fl          IN ('R') --Return
				    AND t506.c506_void_fl       IS NULL
                    AND t5011.c5010_tag_id = p_tagid
                    AND t5011.c5011_last_updated_trans_id = t506.c506_rma_id
					ORDER BY  t506.c506_rma_id  ;
    		
	BEGIN
    
    p_status := 'FALSE';    
    ----any set is already return with same tagid and same set is marked as extra or Transfer (To/From)or missing
   FOR v_return_index IN v_return_cur
	 LOOP
	        IF p_dist_id = v_return_index.dist_id THEN
		        p_rma_id := v_return_index.rma_id;
		        p_status := 'TRUE';
		        RETURN;
	    	END IF;
	 	
	    p_rma_id := v_return_index.dist_name;
        p_status := 'DIST';
	 END LOOP;
END gm_post_tag_return_check;



/********************************************************************************
* Description : Procedure to check the posting return validation
* Author    : arockia prasath
*********************************************************************************/
PROCEDURE gm_post_return_check (
		p_audit_entry_id IN t2656_audit_entry.c2656_audit_entry_id%TYPE,
		p_tagid IN t2656_audit_entry.c5010_tag_id%TYPE,
        p_set_id IN t2656_audit_entry.c207_set_id%TYPE,
        p_ref_type IN t2656_audit_entry.c901_ref_type%TYPE,
        p_pnum IN t2656_audit_entry.c205_part_number_id%TYPE,
        p_locked_date IN t2652_field_sales_lock.c2652_locked_date%TYPE,
        p_field_sales_details_id IN t2651_field_sales_details.c2651_field_sales_details_id%TYPE,
        p_dist_id  IN t2651_field_sales_details.c701_distributor_id%TYPE,
        p_user_id  IN t2656_audit_entry.c2656_last_updated_by%TYPE)
AS
    v_last_trans t5011_tag_log.c5011_last_updated_trans_id%TYPE;
    v_excess_id t504a_consignment_excess.c504_consignment_id%TYPE;
    v_qty t505_item_consignment.c505_item_qty%TYPE;
    v_excess_count NUMBER;
    v_count       NUMBER;
    p_count NUMBER;  
    p_excess_id t504a_consignment_excess.c504_consignment_id%TYPE;
    p_qty t505_item_consignment.c505_item_qty%TYPE;
   
    CURSOR v_return_cur
    IS 
    		    SELECT t506.c506_rma_id rma_id 
				   FROM t506_returns t506, t507_returns_item t507, t5011_tag_log t5011
				  WHERE EXISTS
				    (
				         SELECT t205d.c205_part_number_id
				           FROM t205d_part_attribute t205d
				          WHERE t205d.c901_attribute_type = 92340
				            AND t205d.c205_part_number_id = t507.c205_part_number_id
				    )
				    AND t506.c701_distributor_id = p_dist_id
				    AND t507.c205_part_number_id = p_pnum
				    AND t506.c207_set_id         = p_set_id
				    AND t506.c506_rma_id         = t507.c506_rma_id
				    AND t506.c506_created_date    >= p_locked_date  
				    AND t506.c506_reason         = 3313 --Consignment Return
				    AND t506.c506_status_fl <= 2 -- <=approved pending credit
				    AND c507_status_fl          IN ('R') --Return
				    AND t506.c506_void_fl       IS NULL
                    AND t5011.c5010_tag_id IS NULL 
                    AND t5011.c5011_last_updated_trans_id(+) = t506.c506_rma_id
					ORDER BY  t506.c506_rma_id  ;
    		
	BEGIN
   
        
    ----any open return available for the set without tag id 
  
	
	  FOR v_return_index IN v_return_cur
        LOOP              
        --MISSING TAG CHECK
            gm_missing_tag_check (p_audit_entry_id,p_field_sales_details_id, p_set_id, p_pnum, p_ref_type,
            	 v_return_index.rma_id, v_qty, p_user_id, p_count
            ) ;
            
         IF (p_count = 0) THEN -- tag is not updated and its not missing tag also
         	gm_excess_check (v_return_index.rma_id, p_pnum ,p_excess_id ,p_qty);
	       	gm_sav_posting_error (p_field_sales_details_id, p_set_id, p_pnum, p_ref_type,
            v_return_index.rma_id, v_qty, p_user_id, '52185' ---RETURN
            ,p_audit_entry_id,v_excess_id
            ) ;	
         END IF;
      END LOOP;
  
END gm_post_return_check;

/********************************************************************************
* Description : Procedure to check the excess info
* Author    : arockia prasath
*********************************************************************************/
PROCEDURE gm_excess_check (
		p_rma_id IN t506_returns.c506_rma_id%TYPE,
        p_pnum IN t2656_audit_entry.c205_part_number_id%TYPE,
        p_excess_id OUT t504a_consignment_excess.c504_consignment_id%TYPE,
        p_qty OUT t505_item_consignment.c505_item_qty%TYPE
        )
AS
    v_excess_id t504a_consignment_excess.c504_consignment_id%TYPE;
    v_qty t505_item_consignment.c505_item_qty%TYPE;
    v_count       NUMBER;
BEGIN    
				v_excess_id := NULL;
          		v_qty       := NULL;
          		
          		--finding any excess available for the return
          		SELECT COUNT(1) INTO v_count 
                   FROM t504a_consignment_excess t504a, t505_item_consignment t505
                  WHERE t504a.c506_rma_id        = p_rma_id
                    AND t504a.c504a_status_fl    = 1
                    AND t505.c205_part_number_id = p_pnum
                    AND t505.c504_consignment_id = t504a.c504_consignment_id
                    AND t504a.c504a_void_fl     IS NULL
                    AND t505.c505_void_fl       IS NULL;
          		
                 IF v_count > 0 THEN 
                    SELECT t504a.c504_consignment_id, t505.c505_item_qty
                   INTO v_excess_id, v_qty
                   FROM t504a_consignment_excess t504a, t505_item_consignment t505
                  WHERE t504a.c506_rma_id        = p_rma_id
                    AND t504a.c504a_status_fl    = 1
                    AND t505.c205_part_number_id = p_pnum
                    AND t505.c504_consignment_id = t504a.c504_consignment_id
                    AND t504a.c504a_void_fl     IS NULL
                    AND t505.c505_void_fl       IS NULL;
	       		 END IF;
	       		 
	       		 p_excess_id := v_excess_id;
	       		 p_qty := v_qty;
END gm_excess_check;

/********************************************************************************
* Description : Procedure will check transfers already made for tag
* Author   : arockia prasath
*********************************************************************************/
PROCEDURE gm_post_tag_transfer_check (
       	p_tagid IN t2656_audit_entry.c5010_tag_id%TYPE,
       	p_dist_id  IN t2651_field_sales_details.c701_distributor_id%TYPE,
        p_ref_type IN t2656_audit_entry.c901_ref_type%TYPE,
        p_locked_date IN t2652_field_sales_lock.c2652_locked_date%TYPE,
        p_set_id IN t2656_audit_entry.c207_set_id%TYPE,
        p_pnum IN t2656_audit_entry.c205_part_number_id%TYPE,
        p_user_id  IN t2656_audit_entry.c2656_last_updated_by%TYPE,
        p_transfer_id OUT t921_transfer_detail.c920_transfer_id%TYPE,
   		p_transfer_qty OUT t505_item_consignment.c505_item_qty%TYPE,
        p_status OUT BOOLEAN
        )
AS
  
    --fetching all the consignment and returns belongs to distributor after the locked date with tag id
    CURSOR v_transfer_cur
    IS
        SELECT t921.c920_transfer_id transfer_id, t504.c207_set_id set_id, t505.c205_part_number_id pnum
  				, t505.c505_item_qty qty, t921.c901_link_type link_type, t504.c504_consignment_id cons_ret_id
   		FROM t921_transfer_detail t921, t504_consignment t504, t920_transfer t920, t5011_tag_log t5011
  				, T505_Item_Consignment T505
  		WHERE  EXISTS ( SELECT t205d.c205_part_number_id
           				FROM t205d_part_attribute t205d
          				WHERE t205d.c901_attribute_type = 92340
          				AND t205d.c205_part_number_id = t505.c205_part_number_id
          			   )
	    AND t504.c701_distributor_id = p_dist_id
	    AND t921.c921_ref_id         = t504.c504_consignment_id
	    AND t504.c207_set_id = p_set_id
	    AND t505.c205_part_number_id = p_pnum
	    AND t920.c920_transfer_id     = t921.c920_transfer_id
	    AND t920.c920_transfer_date  >= p_locked_date 
	    AND t921.c901_link_type       = 90360 --Consignment
	    AND t505.c504_consignment_id  = t504.c504_consignment_id
	    AND t5011.c5011_last_updated_trans_id = t921.c920_transfer_id
        AND t5011.c5010_tag_id = p_tagid 
	    AND t920.c920_void_fl IS NULL
	    AND t504.c504_void_fl IS NULL
	    AND t921.c921_void_fl IS NULL
	    AND t505.c505_void_fl IS NULL
  UNION ALL
    	SELECT t921.c920_transfer_id transfer_id, t506.c207_set_id set_id, t507.c205_part_number_id pnum
  			, t507.c507_item_qty qty, t921.c901_link_type link_type, t506.c506_rma_id cons_ret_id 
   		FROM t921_transfer_detail t921, t506_returns t506, t920_transfer t920
 			 , t507_returns_item t507, t5011_tag_log t5011
  		WHERE EXISTS ( SELECT t205d.c205_part_number_id
						FROM t205d_part_attribute t205d
          				WHERE t205d.c901_attribute_type = 92340
          				AND t205d.c205_part_number_id = t507.c205_part_number_id
    					)
    	AND t506.c701_distributor_id = p_dist_id
	    AND t921.c921_ref_id         = t506.c506_rma_id
	    AND t506.c207_set_id  = p_set_id 
	    AND t507.c205_part_number_id = p_pnum 
	    AND t920.c920_transfer_id     = t921.c920_transfer_id
	    AND t920.c920_transfer_date  >= p_locked_date 
	    AND t921.c901_link_type       = 90361 ----return
	    AND t506.c506_rma_id          = t507.c506_rma_id
        AND t5011.c5011_last_updated_trans_id = t921.c920_transfer_id
        AND t5011.c5010_tag_id = p_tagid 
        AND t920.c920_void_fl IS NULL
	    AND t506.c506_void_fl IS NULL
	    And T921.C921_Void_Fl Is NULL
	    AND t920.c920_void_fl IS NULL;
	    
	    
BEGIN
	
	p_status := FALSE;
	---If transfer already done,details will be logged in the posting error table
	FOR v_transfer_index IN v_transfer_cur
	LOOP
    
         --   90360-CONSIGNMENT   51023-TRANSFER FROM 51024-EXTRA  51027-TRANSFER TO 51028-MISSING
         IF (((v_transfer_index.link_type=90360 AND (p_ref_type=51023 OR p_ref_type=51024)) OR (v_transfer_index.link_type=90361 AND (p_ref_type=51027 OR p_ref_type=51028)))) THEN
             p_transfer_id := v_transfer_index.transfer_id;
             p_transfer_qty := v_transfer_index.qty;
             p_status := TRUE;
         END IF;  
    
	END LOOP;
    
	
END gm_post_tag_transfer_check;
/********************************************************************************
* Description : Procedure will check transfers already made
* Author   : arockia prasath
*********************************************************************************/
PROCEDURE gm_post_transfer_check (
       	p_audit_entry_id IN t2656_audit_entry.c2656_audit_entry_id%TYPE,
       	p_tagid IN t2656_audit_entry.c5010_tag_id%TYPE,
       	p_dist_id  IN t2651_field_sales_details.c701_distributor_id%TYPE,
        p_ref_type IN t2656_audit_entry.c901_ref_type%TYPE,
        p_locked_date IN t2652_field_sales_lock.c2652_locked_date%TYPE,
        p_field_sales_details_id IN t2651_field_sales_details.c2651_field_sales_details_id%TYPE,
        p_set_id IN t2656_audit_entry.c207_set_id%TYPE,
        p_pnum IN t2656_audit_entry.c205_part_number_id%TYPE,
        p_user_id  IN t2656_audit_entry.c2656_last_updated_by%TYPE)
AS
    
   
    p_count NUMBER;
    
    --fetching all the consignment and returns belongs to distributor after the locked date without tag ids
    CURSOR v_transfer_cur
    IS
        SELECT t921.c920_transfer_id transfer_id, t504.c207_set_id set_id, t505.c205_part_number_id pnum
  				, t505.c505_item_qty qty, t921.c901_link_type link_type, t504.c504_consignment_id cons_ret_id
   		FROM t921_transfer_detail t921, t504_consignment t504, t920_transfer t920, t5011_tag_log t5011
  				, T505_Item_Consignment T505
  		WHERE  EXISTS ( SELECT t205d.c205_part_number_id
           				FROM t205d_part_attribute t205d
          				WHERE t205d.c901_attribute_type = 92340
          				AND t205d.c205_part_number_id = t505.c205_part_number_id
          			   )
	    AND t504.c701_distributor_id = p_dist_id
	    AND t921.c921_ref_id         = t504.c504_consignment_id
	    AND t504.c207_set_id = p_set_id
	    AND t505.c205_part_number_id = p_pnum
	    AND t920.c920_transfer_id     = t921.c920_transfer_id
	    AND t920.c920_transfer_date  >= p_locked_date 
	    AND t921.c901_link_type       = 90360 --Consignment
	    AND t505.c504_consignment_id  = t504.c504_consignment_id
	    AND t5011.c5011_last_updated_trans_id(+) = t921.c920_transfer_id
        AND t5011.c5010_tag_id IS NULL 
	    AND t920.c920_void_fl IS NULL
	    AND t504.c504_void_fl IS NULL
	    AND t921.c921_void_fl IS NULL
	    AND t505.c505_void_fl IS NULL
  UNION ALL
    	SELECT t921.c920_transfer_id transfer_id, t506.c207_set_id set_id, t507.c205_part_number_id pnum
  			, t507.c507_item_qty qty, t921.c901_link_type link_type, t506.c506_rma_id cons_ret_id 
   		FROM t921_transfer_detail t921, t506_returns t506, t920_transfer t920
 			 , t507_returns_item t507, t5011_tag_log t5011
  		WHERE EXISTS ( SELECT t205d.c205_part_number_id
						FROM t205d_part_attribute t205d
          				WHERE t205d.c901_attribute_type = 92340
          				AND t205d.c205_part_number_id = t507.c205_part_number_id
    					)
    	AND t506.c701_distributor_id = p_dist_id
	    AND t921.c921_ref_id         = t506.c506_rma_id
	    AND t506.c207_set_id  = p_set_id 
	    AND t507.c205_part_number_id = p_pnum 
	    AND t920.c920_transfer_id     = t921.c920_transfer_id
	    AND t920.c920_transfer_date  >= p_locked_date 
	    AND t921.c901_link_type       = 90361 ----return
	    AND t506.c506_rma_id          = t507.c506_rma_id
        AND t5011.c5011_last_updated_trans_id(+) = t921.c920_transfer_id
        AND t5011.c5010_tag_id IS NULL 
        AND t920.c920_void_fl IS NULL
	    AND t506.c506_void_fl IS NULL
	    And T921.C921_Void_Fl Is NULL
	    AND t920.c920_void_fl IS NULL;
	 -- to find the open trnasfer 
	CURSOR v_open_transfer_cur
	IS
	 SELECT NVL (t5010.c5010_tag_id, '') tagid, 
   t507.c205_part_number_id pnum 
  , t5010.c5010_control_number cntlnum, t506.c506_rma_id refidrtn, DECODE (t506.c207_set_id, t5010.c207_set_id,
    t506.c207_set_id, t5010.c207_set_id) setid, t507.c507_item_qty qty, t921.c921_ref_id refid
  , t921.c920_transfer_id tfid, t920.c920_from_ref_id fromid, t920.c920_to_ref_id toid
  , t506.c506_reason, get_distributor_name ( t920.c920_to_ref_id) to_distname ,  t920.c920_status_fl statusfl
   FROM t921_transfer_detail t921, t506_returns t506, t507_returns_item t507
  , t5010_tag t5010, t920_transfer t920
  WHERE t921.c921_ref_id         = t506.c506_rma_id
    AND t920.c920_transfer_id    = t921.c920_transfer_id
    AND t506.c506_rma_id         = t507.c506_rma_id
    AND t507.c5010_tag_id        = p_tagid
    AND t506.c207_set_id         = p_set_id 
    AND t507.c205_part_number_id = p_pnum
    AND t506.c506_created_date  >= p_locked_date
    AND t506.c506_reason         = 3315 -- Transfer
    AND t507.c5010_tag_id        = t5010.c5010_tag_id(+)
    AND t507.c507_item_qty       > 0
    AND t920.c920_void_fl IS NULL
    AND t921.c921_void_fl IS NULL
    AND t506.c506_void_fl IS NULL
    order by t921.c920_transfer_id;
    
BEGIN
	
	
	    FOR v_transfer_index IN v_transfer_cur
	    LOOP   
	    	p_count := 0;
	          --MISSING TAG
	            IF v_transfer_index.link_type = 90361  THEN --RETURN ITEMS
	                 gm_missing_tag_check (p_audit_entry_id,p_field_sales_details_id, v_transfer_index.set_id, v_transfer_index.pnum, p_ref_type,
	            	 v_transfer_index.cons_ret_id, v_transfer_index.qty, p_user_id, p_count
	            	 ) ;
	            END IF;        	
	   
	       		--   90360-CONSIGNMENT   51023-TRANSFER FROM 51024-EXTRA  51027-TRANSFER TO 51028-MISSING
         		IF (p_count = 0 AND ((v_transfer_index.link_type=90360 AND (p_ref_type=51023 OR p_ref_type=51024)) OR (v_transfer_index.link_type=90361 AND (p_ref_type=51027 OR p_ref_type=51028)))) THEN
	            gm_sav_posting_error (p_field_sales_details_id, v_transfer_index.set_id, v_transfer_index.pnum, p_ref_type,
	            v_transfer_index.transfer_id, v_transfer_index.qty, p_user_id, '52183' ---Transfer Done
	            ,p_audit_entry_id,NULL
	            ) ;
	            END IF;
	        
	          
	    END LOOP;

	    IF p_ref_type=51023 -- Transfer From
	    THEN
	    FOR v_open_tran_index IN v_open_transfer_cur
	    LOOP
		    IF v_open_tran_index.toid <> p_dist_id THEN
		     gm_sav_posting_error (p_field_sales_details_id, p_set_id, p_pnum, p_ref_type,
		            v_open_tran_index.to_distname, v_open_tran_index.qty, p_user_id, '52183' -- Transfer Completed
		            ,p_audit_entry_id,NULL
		            ) ;	    
		     END IF;    
	    END LOOP;
	   END IF; 
END gm_post_transfer_check;
/********************************************************************************
* Description : Procedure will check miaaing tag
* Author   : arockia prasath
*********************************************************************************/
PROCEDURE gm_missing_tag_check (
       	p_audit_entry_id IN t2656_audit_entry.c2656_audit_entry_id%TYPE,
       	p_field_sales_details_id IN t2651_field_sales_details.c2651_field_sales_details_id%TYPE,
       	p_set_id IN t2656_audit_entry.c207_set_id%TYPE,
       	p_pnum IN t2656_audit_entry.c205_part_number_id%TYPE,
       	p_ref_type IN t2656_audit_entry.c901_ref_type%TYPE,
        p_rma_id IN t506_returns.c506_rma_id%TYPE,
        p_qty IN t507_returns_item.c507_item_qty%TYPE,
        p_user_id  IN t2656_audit_entry.c2656_last_updated_by%TYPE,
        p_count OUT NUMBER
        )
                
AS
v_count NUMBER;
BEGIN
	--VERIFYING THE TRANSACTION HAVE MISSING TAG
		 SELECT count(1) into v_count 
		   FROM t506_returns t506, t507_returns_item t507
		  WHERE t506.C506_VOID_FL            IS NULL
		    AND t507.c506_rma_id              = p_rma_id
		    AND t507.c506_rma_id              = t506.c506_rma_id
		    AND t507.c507_missing_fl          = 'Y'
		    AND t507.C507_STATUS_FL           in ('C','R')
		    AND t507.C901_MISSING_TAG_REASON IS NULL;
    
		    IF (v_count > 0) THEN
		            gm_sav_posting_error (p_field_sales_details_id, p_set_id, p_pnum, p_ref_type,
		            p_rma_id, p_qty, p_user_id, '53186' ---MISSING TAG
		            ,p_audit_entry_id,NULL
		            ) ;
		    END IF;
		    p_count := v_count;
END gm_missing_tag_check;

/********************************************************************************
* Description : Procedure will check inactive distributor 
* Author   : arockia prasath
*********************************************************************************/
PROCEDURE gm_loaner_to_consignment_check (
        p_audit_entry_id IN t2656_audit_entry.c2656_audit_entry_id%TYPE,
        p_tagid IN t2656_audit_entry.c5010_tag_id%TYPE,
        p_locked_date IN t2652_field_sales_lock.c2652_locked_date%TYPE,
        p_set_id IN t2656_audit_entry.c207_set_id%TYPE,
        p_pnum IN t2656_audit_entry.c205_part_number_id%TYPE,
        p_ref_type IN t2656_audit_entry.c901_ref_type%TYPE,
        p_dist_id  IN t2651_field_sales_details.c701_distributor_id%TYPE,
        p_field_sales_details_id IN t2651_field_sales_details.c2651_field_sales_details_id%TYPE,
        p_user_id  IN t2656_audit_entry.c2656_last_updated_by%TYPE)
AS 	
		v_count NUMBER;
BEGIN
	
		FOR v_return_cur IN (
            SELECT distinct t5011.c5011_last_updated_trans_id 
                        last_updated_trans_id
                       FROM t5011_tag_log t5011, t504_consignment t504
                      WHERE --t5011.c5011_location_id = p_dist_id AND
                            t5011.c5010_tag_id      = p_tagid
                        AND t504.c504_consignment_id = t5011.c5011_last_updated_trans_id
                        AND t504.c504_ship_date  >= p_locked_date
            			AND t504.c504_void_fl      IS NULL
                   
            )
    		LOOP
       		BEGIN        
		
	
		 ---Loanertoconsignment- verifying the consignment comes from loaner (loaner to consignment swap)
    	  SELECT    COUNT(1) INTO v_count 
            FROM t504_consignment t504,
                 t504a_consignment_loaner t504a,
                 t207_set_master t207
           WHERE t504.c504_status_fl = 4
             AND t504.c504_consignment_id IN (select distinct c504_consignment_id from t504b_loaner_swap_log 
             where c504d_ref_id=v_return_cur.last_updated_trans_id and c504d_void_fl is null) 
             AND t504a.c504_consignment_id = t504.c504_consignment_id
             AND t207.c207_set_id = t504.c207_set_id
             AND t504.c504_type = 4127 --PRODUCT LOANER
             AND t504a.c504a_status_fl = 60 --INACTIVE
             AND t207.c207_type = 4074  --LOANER
             AND t504a.c504a_void_fl IS NULL
             AND t504.c504_void_fl IS NULL
             AND c207_void_fl IS NULL;
             
            IF (v_count > 0) THEN
            gm_sav_posting_error (p_field_sales_details_id, p_set_id, p_pnum, p_ref_type,
            v_return_cur.last_updated_trans_id, NULL, p_user_id, '53189' ---Loanertoconsignment
            ,p_audit_entry_id,NULL
            ) ;
            END IF;
            
            
           END;
    	END LOOP;
END gm_loaner_to_consignment_check;

/********************************************************************************
* Description : Procedure will check inactive distributor 
* Author   : arockia prasath
*********************************************************************************/
PROCEDURE gm_inactive_distributor_check (
        p_audit_entry_id IN t2656_audit_entry.c2656_audit_entry_id%TYPE,
        p_user_id  IN t2656_audit_entry.c2656_last_updated_by%TYPE)
AS 	
BEGIN
 --- Validation for tranfer to Inactive distributor
     INSERT
       INTO t2662_posting_error
        (
            c2662_posting_error_id, c2651_field_sales_details_id, c207_set_id
          , c205_part_number_id, c901_ref_type, c2662_created_by
          , c2662_created_date, c901_reason ,c901_status ,c2656_audit_entry_id 
        )
        (
             SELECT s2662_posting_error.NEXTVAL, t2656.c2651_field_sales_details_id, t2656.c207_set_id
              , t2656.c205_part_number_id, t2656.c901_ref_type, p_user_id
              , SYSDATE, 52184 -- inactive distributor
              , 53101 --open 
              , t2656.c2656_audit_entry_id 
               FROM t2656_audit_entry t2656 
              WHERE EXISTS ( SELECT t701.c701_distributor_id
								FROM t701_distributor t701
                      			WHERE t701.c701_void_fl          IS NULL
                        		AND (t701.c901_ext_country_id   IS NULL OR t701.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH
                        		AND t701.c701_active_fl         = 'N'
                        		AND T701.C901_Distributor_Type In (70100, 70101)
                        		AND t701.c701_distributor_id = t2656.c2656_ref_id
                			) 
              	AND t2656.c2656_audit_entry_id         = p_audit_entry_id
                AND t2656.c901_ref_type                = 51027 --TRANSFER TO
                AND t2656.c2656_void_fl       IS NULL
                AND t2656.c901_posting_option IS NULL
        ) ;
END gm_inactive_distributor_check;

/********************************************************************************
* Description : Procedure will check incorrect transfer
* Author   : arockia prasath
*********************************************************************************/
PROCEDURE gm_incorrect_transfer_check (
       	p_audit_entry_id IN t2656_audit_entry.c2656_audit_entry_id%TYPE,
        p_set_id IN t2656_audit_entry.c207_set_id%TYPE,
        p_ref_type IN t2656_audit_entry.c901_ref_type%TYPE,
        p_pnum IN t2656_audit_entry.c205_part_number_id%TYPE,
        p_field_sales_details_id IN t2651_field_sales_details.c2651_field_sales_details_id%TYPE,
        p_dist_id  IN t2651_field_sales_details.c701_distributor_id%TYPE,
        p_user_id  IN t2656_audit_entry.c2656_last_updated_by%TYPE)
AS

 		v_ref_type t2656_audit_entry.c901_ref_type%TYPE;
    	v_ref_id t2656_audit_entry.c2656_ref_id	%TYPE;
    	v_count NUMBER;
BEGIN
	
	    	SELECT count(1)
           INTO v_count 
           FROM t2656_audit_entry t2656 
          WHERE t2656.c901_ref_type                IN (51023, 51027) --Transfer from / To
            AND t2656.c2656_audit_entry_id         = p_audit_entry_id 
            AND t2656.c2656_void_fl               IS NULL
            AND t2656.c901_posting_option         IS NULL;
    
   		IF v_count > 0 THEN 
         --INCORRECT TRANSFER
          SELECT c901_ref_type , c2656_ref_id
           INTO v_ref_type , v_ref_id 
           FROM t2656_audit_entry t2656 
          WHERE t2656.c901_ref_type                IN (51023, 51027) --Transfer from / To
            AND t2656.c2656_audit_entry_id         = p_audit_entry_id 
            AND t2656.c2656_void_fl               IS NULL
            AND t2656.c901_posting_option         IS NULL;
            
           --verifying whether transfer from or to same distributor 
            	IF v_ref_id IS NOT NULL AND v_ref_id = p_dist_id THEN --TRANSFER FROM,TO
             		gm_sav_posting_error (p_field_sales_details_id, p_set_id, p_pnum, p_ref_type,
            		NULL, NULL, p_user_id, '53187' --INCORRECT TRANSFER
            		,p_audit_entry_id,NULL
            		) ;
            	END IF;
         END IF;  
            
                   
           
END gm_incorrect_transfer_check;

/********************************************************************************
* Description : Procedure will check qty mismatch check
* Author   : arockia prasath
*********************************************************************************/
PROCEDURE gm_qty_mismatch_check (
        p_audit_entry_id IN t2656_audit_entry.c2656_audit_entry_id%TYPE,
        p_dist_id  IN t2651_field_sales_details.c701_distributor_id%TYPE,
        p_set_id IN t2656_audit_entry.c207_set_id%TYPE,
        p_ref_type IN t2656_audit_entry.c901_ref_type%TYPE,
        p_pnum IN t2656_audit_entry.c205_part_number_id%TYPE,
        p_field_sales_details_id IN t2651_field_sales_details.c2651_field_sales_details_id%TYPE,
        p_user_id  IN t2656_audit_entry.c2656_last_updated_by%TYPE)
AS

 		tqty NUMBER;
 		oqty NUMBER;
BEGIN
    
         IF p_ref_type = '18558' THEN --VOID TAG ONLY          
            --getting the tag qty
    	  SELECT  NVL(COUNT (t5010.C5010_TAG_ID),0) INTO tqty
             FROM t5010_tag t5010
          WHERE C5010_VOID_FL          IS NULL
            AND t5010.C5010_LOCATION_ID = TO_CHAR (p_dist_id)
            AND t5010.c205_part_number_id = p_pnum  ;
            
          --getting the operation qty  
       SELECT NVL(SUM(oqty),0) INTO oqty FROM  (  
           SELECT NVL(SUM (t731.c731_item_qty),0) oqty
           FROM t730_virtual_consignment t730, t731_virtual_consign_item t731, t205d_part_attribute t205d
           WHERE t730.c730_virtual_consignment_id = t731.c730_virtual_consignment_id
            AND t730.c701_distributor_id         = p_dist_id
            AND t731.c205_part_number_id         = t205d.c205_part_number_id
            AND t205d.c901_attribute_type        = 92340 -- taggable part
            AND t205d.c205d_attribute_value      = 'Y'
            AND t731.c205_part_number_id         = p_pnum 
            AND t205d.c205d_void_fl             IS NULL
       
       UNION
       
          SELECT NVL(SUM (t505.C505_ITEM_QTY),0) oqty
           FROM t504_consignment t504, t504a_consignment_loaner t504a, t504a_loaner_transaction t504atxn
           , t205d_part_attribute t205d, t505_item_consignment t505
           WHERE t504.c504_status_fl            = '4'
            AND t504.c504_void_fl             IS NULL
            AND t504a.c504a_void_fl           IS NULL
            AND t504.c207_set_id              IS NOT NULL
            AND t504.c504_type                 = 4127
            AND t504a.c504a_status_fl          = 20
            AND t504atxn.c504a_consigned_to_id = p_dist_id
            AND t504.c504_consignment_id       = t504a.c504_consignment_id
            AND t504.c504_consignment_id       = t504atxn.c504_consignment_id
            AND t504atxn.c504a_return_dt      IS NULL
            AND t504atxn.c504a_void_fl        IS NULL
            AND t504.c504_consignment_id       = t505.c504_consignment_id
            AND t505.c205_part_number_id       = t205d.c205_part_number_id
            AND t205d.c901_attribute_type      = 92340
            AND t505.c505_ref_id              IS NULL -- Multiple rows b'coz of LNs
            AND t205d.c205d_attribute_value   IS NOT NULL --Multiple rows
            AND t505.c505_item_qty             > 0 -- 2 taggable parts in consignment,1 with qty 1 and other with 0 qty
            AND t505.c205_part_number_id       = p_pnum );
                        
	      --verify tag qty < ops qty        
              IF tqty < oqty THEN
              gm_sav_posting_error (p_field_sales_details_id, p_set_id, p_pnum, p_ref_type,
              NULL, NULL, p_user_id, '53188' --QTY MISMATCH
              ,p_audit_entry_id,NULL
              ) ;
              END IF;
            
        END IF;--VOID TAG
           
END gm_qty_mismatch_check;
/********************************************************************************
* Description : Procedure will insert the posting Errors
* Author    : arockia prasath
*********************************************************************************/
PROCEDURE gm_sav_posting_error
    (
        p_field_sales_details_id IN t2651_field_sales_details.c2651_field_sales_details_id%TYPE,
        p_set_id                 IN t207_set_master.c207_set_id%TYPE,
        p_num                    IN t2656_audit_entry.c205_part_number_id%TYPE,
        p_ref_type               IN t2656_audit_entry.c901_ref_type%TYPE,
        p_transfer_id            IN t921_transfer_detail.c920_transfer_id%TYPE,
        p_transfer_qty           IN t2662_posting_error.c2662_ref_qty%TYPE,
        p_user_id                IN t2656_audit_entry.c2656_last_updated_by%TYPE,
        p_reason                 IN t2662_posting_error.c901_reason%TYPE,
        p_audit_entry_id         IN t2656_audit_entry.c2656_audit_entry_id%TYPE,
        p_excess_id				 IN t2662_posting_error.c504_excess_id%TYPE
    )
AS
BEGIN
	
	  INSERT
       INTO t2662_posting_error
        (
            c2662_posting_error_id, c2651_field_sales_details_id, c207_set_id
          , c205_part_number_id, c901_ref_type, c2662_ref_id
          , c2662_ref_qty, c2662_created_by, c2662_created_date
          , c901_reason ,c2656_audit_entry_id,c504_excess_id,c901_status
        )
         
        VALUES
        (
            s2662_posting_error.NEXTVAL, p_field_sales_details_id, p_set_id
          , p_num, p_ref_type, p_transfer_id
          , p_transfer_qty, p_user_id, SYSDATE
          , p_reason , p_audit_entry_id ,p_excess_id ,53101
        ) ;
END gm_sav_posting_error;
END gm_pkg_ac_fa_post_err;
/