--@"c:\database\packages\accounting\gm_pkg_ac_fa_reconciliation.bdy"
CREATE OR REPLACE
PACKAGE BODY gm_pkg_ac_fa_reconciliation
IS
    /****************************************************************************
    *
    * Description : Procedure to fetch Summary report
    * Author: Jignesh Shah
    ****************************************************************************
    */
PROCEDURE gm_fch_summary_report (
        p_locked_from IN t2652_field_sales_lock.C2652_LOCKED_DATE%TYPE,
        p_locked_to   IN t2652_field_sales_lock.C2652_LOCKED_DATE%TYPE,
        p_out_audit_info OUT TYPES.cursor_type,
        p_out_fieldsales_info OUT TYPES.cursor_type)
AS
BEGIN
    ---summary report query
    OPEN p_out_audit_info FOR -- fetch Auditor details
    SELECT t2650.c2650_field_sales_audit_id auditId, t2650.c2650_audit_name audit_name, get_code_name (
    t2650.c901_audit_status) audit_status FROM t2650_field_sales_audit t2650 WHERE t2650.c2650_field_sales_audit_id <>
    1 AND t2650.c2650_void_fl                                                                                       IS
    NULL;
    OPEN p_out_fieldsales_info FOR -- fetch distributor details
    SELECT id, distid, c2652_locked_date, get_distributor_name (distid) dist_name, get_code_name (dist_status)
    dist_status, locked_qty locked_qty, MAX (counted_date) counted_date, SUM (counted_qty) counted_qty, SUM (extra_qty)
    extra_qty, SUM (missing_qty) missing_qty, SUM (pending_charge) pending_charge FROM
    (
         SELECT t2652.c2652_locked_date, t2651.c701_distributor_id distid, t2651.c901_reconciliation_status dist_status
          , t2651.c2650_field_sales_audit_id id, MAX (t2656.c2656_counted_date) counted_date, locked_qty locked_qty
          , SUM (DECODE (t2656.c901_entry_type, '51030', 1, 0)) counted_qty, SUM (DECODE (t2656.c901_ref_type, '51024',
            1, 0)) extra_qty, SUM (DECODE (t2656.c901_ref_type, '51028', 1, 0)) missing_qty, SUM (DECODE (
            t2656.c901_ref_type, '51028', DECODE (t2656.c2656_ref_id, '51040', to_number (t2656.c2656_ref_details),
            '18566', to_number (t2656.c2656_ref_details), 0), 0)) pending_charge
           FROM t2651_field_sales_details t2651, t2656_audit_entry t2656, t2652_field_sales_lock t2652
          , t2650_field_sales_audit t2650, (
                 SELECT c2652_field_sales_lock_id, SUM (c2657_tag_qty) locked_qty
                   FROM t2657_field_sales_details_lock
                  WHERE c2657_void_fl IS NULL
               GROUP BY c2652_field_sales_lock_id
            )
            locked_details
          WHERE T2651.C2651_Field_Sales_Details_Id       = T2656.C2651_Field_Sales_Details_Id (+) -- added
            AND t2652.c2650_field_sales_audit_id         = t2651.c2650_field_sales_audit_id
            AND t2652.c701_distributor_id                = t2651.c701_distributor_id
            AND locked_details.c2652_field_sales_lock_id = t2652.c2652_field_sales_lock_id
            AND t2651.c2650_field_sales_audit_id         = t2650.c2650_field_sales_audit_id
            AND t2651.c2651_void_fl                     IS NULL
            AND t2656.c2656_void_fl                     IS NULL
            AND t2652.c2652_void_fl                     IS NULL
            AND t2650.c2650_void_fl                     IS NULL
            AND t2650.c2650_field_sales_audit_id        <> 1
            AND TRUNC (t2652.c2652_locked_date)         >= TRUNC (DECODE (p_locked_from, NULL, T2652.C2652_Locked_Date,
            p_locked_from))
            AND TRUNC (t2652.c2652_locked_date) <= TRUNC (DECODE (p_locked_to, NULL, t2652.c2652_locked_date,
            p_locked_to))
       GROUP BY t2651.c701_distributor_id, t2656.c2656_counted_date, t2651.c901_reconciliation_status
          , t2651.c2650_field_sales_audit_id, t2652.c2652_locked_date, locked_qty
    )
    GROUP BY id, distid, c2652_locked_date, dist_status, locked_qty;
END gm_fch_summary_report;
--
/**********************************************************************
* Description : Procedure to fetch the Approval Flagged Details
* Author      : hreddi
* *********************************************************************/
--=
PROCEDURE gm_fch_appr_recon_deviation (
        p_reconcil_status VARCHAR2,
        p_out_approval OUT TYPES.cursor_type)
AS
BEGIN
    OPEN p_out_approval FOR SELECT t2656.c2656_audit_entry_id id, t2656.c5010_tag_id tagid, t2650.c2650_audit_name
    auditname, get_distributor_name (t2651.c701_distributor_id) dname, t2656.c207_set_id setid,
    t2656.c205_part_number_id pnum, get_partnum_desc (t2656.c205_part_number_id) pdesc, get_code_name (
    t2656.c901_ref_type) flag, DECODE (instr (',51023,51025,51026,51027,', (','|| t2656.c901_ref_type ||',')), 0, (
    DECODE (instr (',51024,51028,18551,18552,18556,18557,18558, ', (','||t2656.c901_ref_type||',')), 0, (DECODE (
    t2656.c901_ref_type, '51029', c2656_ref_id, NULL)), get_code_name (c2656_ref_id))), get_distributor_name (
    c2656_ref_id)) reason, t2656.c2656_flagged_dt flagdt, DECODE (t2656.c901_ref_type, 51028, t2656.c2656_ref_details,
    '') charge, t2656.c5010_retag_id retagid, get_code_name (t2656.c901_posting_option) post_option,
    t2656.c2656_posting_comment post_comment, get_user_name (t2656.c2656_approved_by) app_by, t2656.c2656_approved_dt
    app_dt FROM t2650_field_sales_audit t2650, t2651_field_sales_details t2651, t2656_audit_entry t2656 WHERE
    t2650.c2650_field_sales_audit_id                                        = t2651.c2650_field_sales_audit_id AND t2651.c2651_field_sales_details_id =
    t2656.c2651_field_sales_details_id AND t2656.c901_reconciliation_status = p_reconcil_status AND
    t2650.c2650_field_sales_audit_id                                       <> 1 AND t2656.c901_tag_status <> 18541 --
    -- Matching
    AND t2650.c2650_void_fl IS NULL AND t2651.c2651_void_fl IS NULL AND t2656.c2656_void_fl IS NULL order by
    t2650.c2650_audit_name;
END gm_fch_appr_recon_deviation;
/*******************************************************************
* Description : Procedure to save the Approval Flagged Details
* Author      : hreddi
********************************************************************/
PROCEDURE gm_sav_appr_deviation (
        p_input_str IN VARCHAR2,
        p_user_id   IN t2656_audit_entry.c2656_created_by%TYPE)
AS
    v_string    VARCHAR2 (4000) := p_input_str;
    v_strlen    NUMBER          := NVL (LENGTH (p_input_str), 0) ;
    v_substring VARCHAR2 (4000) ;
    v_audit_entry_id t2656_audit_entry.c2656_audit_entry_id%TYPE;
    v_count  NUMBER;
    v_tag_id VARCHAR2 (20) ;
    v_fs_det_id t2651_field_sales_details.c2651_field_sales_details_id%TYPE;
    v_dist_status_cnt NUMBER;
    v_all_app_cnt     NUMBER;
    v_dist_recon_cnt  NUMBER;
BEGIN
    IF v_strlen                      > 0 THEN
        WHILE INSTR (v_string, '|') <> 0
        LOOP
            v_substring      := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
            v_string         := SUBSTR (v_string, INSTR (v_string, '|')    + 1) ;
            v_audit_entry_id := NULL;
            v_audit_entry_id := TO_NUMBER (v_substring) ;
            --Validate the tag
             SELECT COUNT (1)
               INTO v_count
               FROM t2656_audit_entry
              WHERE c2656_audit_entry_id       = v_audit_entry_id
                AND c901_reconciliation_status = 51036 --Flagged
                AND c2656_void_fl             IS NULL;
             SELECT t2656.c5010_tag_id, t2651.c2651_field_sales_details_id
               INTO v_tag_id, v_fs_det_id
               FROM t2651_field_sales_details t2651, t2656_audit_entry t2656
              WHERE t2651.c2651_field_sales_details_id = t2656.c2651_field_sales_details_id
                AND t2656.c2656_audit_entry_id         = v_audit_entry_id
                AND t2651.c2651_void_fl               IS NULL
                AND t2656.c2656_void_fl               IS NULL FOR UPDATE;
            IF v_count                                 = 0 THEN
                GM_RAISE_APPLICATION_ERROR('-20999','146',v_tag_id);
            END IF;
            --Update the tag
             UPDATE t2656_audit_entry
            SET c2656_approved_by = p_user_id, c2656_approved_dt = CURRENT_DATE, c901_reconciliation_status = 51037 --
                -- Approved
              , c2656_last_updated_by    = p_user_id, c2656_last_updated_date = CURRENT_DATE
              WHERE c2656_audit_entry_id = v_audit_entry_id ;
            -- fetch All Approved qty
             SELECT COUNT (1)
               INTO v_all_app_cnt
               FROM t2656_audit_entry t2656
              WHERE t2656.c2651_field_sales_details_id = v_fs_det_id
                AND t2656.c901_reconciliation_status   = 51037 -- Approved
                AND t2656.c2656_void_fl               IS NULL;
            -- Check the status of the field sales
             SELECT COUNT (1)
               INTO v_dist_status_cnt
               FROM t2651_field_sales_details t2651
              WHERE t2651.c2651_field_sales_details_id = v_fs_det_id
                AND t2651.c901_reconciliation_status   = 18526 -- Partially Approved
                AND t2651.c2651_void_fl               IS NULL;
            -- Check the status of the field sales
             SELECT COUNT (1)
               INTO v_dist_recon_cnt
               FROM t2651_field_sales_details t2651
              WHERE t2651.c2651_field_sales_details_id = v_fs_det_id
                AND t2651.c901_reconciliation_status   = 18527 -- Partially Reconciled
                AND t2651.c2651_void_fl               IS NULL;
            IF v_all_app_cnt                           > 0 AND v_dist_status_cnt = 0 AND v_dist_recon_cnt = 0 THEN
                 UPDATE t2651_field_sales_details t2651
                SET t2651.c2651_last_updated_by            = p_user_id, t2651.c2651_last_updated_date = CURRENT_DATE,
                    t2651.c901_reconciliation_status       = 18526 -- Partially Approved
                  WHERE t2651.c2651_field_sales_details_id = v_fs_det_id
                    AND t2651.c2651_void_fl               IS NULL;
            END IF;
        END LOOP;
        -- p_out_error_tags  := 'Selected Tag(s) are Successfully Approved';    --
        -- Update Status Message, disussion required
    END IF;
END gm_sav_appr_deviation;
/********************************************************************
* Description : Main procedure for field audit reconciliation
* Author      : hreddi
*********************************************************************/
PROCEDURE gm_sav_reconcile_deviation (
        p_input_str IN CLOB,
        p_user_id   IN t2656_audit_entry.c2656_created_by%TYPE,
        p_out_txn_ids OUT VARCHAR2)
AS
    v_string CLOB   := p_input_str;
    v_strlen    NUMBER := NVL (LENGTH (p_input_str), 0) ;
    v_substring VARCHAR2 (4000) ;
    v_audit_entry_id t2656_audit_entry.c2656_audit_entry_id%TYPE;
    v_count         NUMBER;
    v_error_count   NUMBER;
    v_set_build_cnt NUMBER;
    v_email_type t901_code_lookup.c901_code_id%TYPE;
    v_tag_id VARCHAR2 (40) ;
    v_set_id t207_set_master.c207_set_id%TYPE;
    v_set_type t901_code_lookup.c901_code_id%TYPE;
    v_txn_ids       VARCHAR2 (4000) ;
    v_ous_tag_count NUMBER;
    v_entry_cnt     NUMBER;
    v_comments      VARCHAR2 (4000) := 'This transaction is created as part of the Field Audit Reconciliation process.';
    v_retag_id      VARCHAR2 (40) ;
    v_retag_fl t2656_audit_entry.c2656_retag_fl%TYPE;
    v_tag_type t504_consignment.C504_TYPE%TYPE;
BEGIN
    IF v_strlen                      > 0 THEN
        WHILE INSTR (v_string, '|') <> 0
        LOOP
            v_substring      := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
            v_string         := SUBSTR (v_string, INSTR (v_string, '|')    + 1) ;
            v_audit_entry_id := NULL;
            v_audit_entry_id := TO_NUMBER (v_substring) ;
            -- Check the Posting error is closed.
             SELECT COUNT (1)
               INTO v_error_count
               FROM t2662_posting_error t2662
              WHERE t2662.c2656_audit_entry_id = v_audit_entry_id
                AND t2662.c901_status         <> 53102 --Closed
                AND c2662_void_fl             IS NULL;
            --Post only where the posting type is proceed and posting option is NULL
             SELECT COUNT (1)
               INTO v_entry_cnt
               FROM t2656_audit_entry
              WHERE c2656_audit_entry_id       = v_audit_entry_id
                AND c901_reconciliation_status = DECODE (v_error_count, 0, 51037, 51036) -- 51037-Approved,51036-
                -- Flagged
                AND c2656_void_fl             IS NULL
                AND c901_ref_type             IS NOT NULL
                AND (c901_posting_option      <> 52180
                OR TRIM (c901_posting_option) IS NULL) ;
            IF v_error_count                   = 0 AND v_entry_cnt > 0 THEN
                --Validate the tag
                 SELECT c5010_tag_id, c207_set_id, c5010_retag_id
                  , c2656_retag_fl
                   INTO v_tag_id, v_set_id, v_retag_id
                  , v_retag_fl
                   FROM t2656_audit_entry
                  WHERE c2656_audit_entry_id = v_audit_entry_id
                    AND c2656_void_fl       IS NULL FOR UPDATE;
                -- Check the status is already reconciled or not
                 SELECT COUNT (1)
                   INTO v_count
                   FROM t2656_audit_entry
                  WHERE c2656_audit_entry_id       = v_audit_entry_id
                    AND c901_reconciliation_status = 51038 --Reconciled
                    AND c2656_void_fl             IS NULL;
                IF v_count                         > 0 THEN
                    GM_RAISE_APPLICATION_ERROR('-20999','147',v_tag_id);
                END IF;
                -- Check the status is Approved
                 SELECT COUNT (1)
                   INTO v_count
                   FROM t2656_audit_entry
                  WHERE c2656_audit_entry_id        = v_audit_entry_id
                    AND c901_reconciliation_status <> 51037 --Approved
                    AND c2656_void_fl              IS NULL;
                IF v_count                          > 0 THEN
                    GM_RAISE_APPLICATION_ERROR('-20999','119',v_tag_id);
                END IF;
                -- Check tag location is available in Set Building
                 SELECT COUNT (1)
                   INTO v_set_build_cnt
                   FROM t5010_tag t5010, t2656_audit_entry t2656
                  WHERE t2656.c2656_audit_entry_id = v_audit_entry_id
                    AND t5010.c5010_tag_id         = DECODE (t2656.c2656_retag_fl, NULL, t2656.c5010_tag_id,
                    t2656.c5010_retag_id)
                    AND t5010.c901_location_type = '40033' -- Inhouse
                    AND t5010.c5010_location_id  = '52098' -- Set Building
                    AND t5010.c5010_void_fl     IS NULL
                    AND t2656.c2656_void_fl     IS NULL;
                IF v_set_build_cnt              >= 1 THEN
                    v_email_type                := 18615 ; -- Build Set (Email send CS)
                    gm_sav_email_fl (v_audit_entry_id, v_email_type) ;
                END IF;
                -- Check the tag is available in OUS Distributor
                 SELECT COUNT (1)
                   INTO v_ous_tag_count
                   FROM t2656_audit_entry t2656, t701_distributor t701
                  WHERE t2656.c2656_ref_id          = t701.c701_distributor_id
                    AND t2656.c2656_audit_entry_id  = v_audit_entry_id
                    AND t701.c901_distributor_type IN (70103, 70106, 70105, 70104)
                    AND t2656.c2656_void_fl        IS NULL
                    AND t701.c701_void_fl          IS NULL;
                IF v_ous_tag_count                 >= 1 THEN
                    v_email_type                   := 18616; --OUS Set (Email Send CS)
                    gm_sav_email_fl (v_audit_entry_id, v_email_type) ;
                END IF;
                IF v_set_build_cnt = 0 AND v_ous_tag_count = 0 THEN
                    -- Checking for the Set is belongs to Concignment or Loaner
                    -- to set the retag id to tag id
                    v_retag_id    := get_retag_id (v_audit_entry_id) ;
                    IF v_retag_id IS NOT NULL THEN
                        v_tag_id  := v_retag_id;
                    END IF;
                    v_tag_type := get_tag_type (v_tag_id) ;
                    --4127  Product loaner
                    IF v_tag_type = 4127 THEN
                        -- to send the mail to CS
                        gm_sav_loaner_reconcile (v_audit_entry_id, p_user_id) ;
                    ELSE
                        --Post only where the posting type is proceed and posting option is
                        -- NULL
                        IF v_entry_cnt > 0 THEN
                            -- to create the transaction
                            gm_sav_consign_reconcile (v_audit_entry_id, v_comments, p_user_id, v_txn_ids) ;
                            IF v_txn_ids      IS NOT NULL THEN
                                p_out_txn_ids := p_out_txn_ids ||v_txn_ids || ',';
                            END IF; -- v_txn_ids not null
                        END IF; -- v_entry_cnt
                    END IF; -- v_set_type
                END IF;
            END IF;--posting error count
            -- to update the reconcile status for each audit entry id, Adding condition for if no posting error or they
            -- marked as skip in posting option
            IF v_error_count = 0 OR v_entry_cnt = 0 THEN
                gm_sav_update_status (v_audit_entry_id, v_txn_ids, p_user_id) ;
            END IF;
        END LOOP;
    END IF;
END gm_sav_reconcile_deviation;
/******************************************************************************
**
* Description : Procedure to create the new transaction :write up/off set and part
* Author    : Mihir Patel
*******************************************************************************
**/
PROCEDURE gm_sav_consign_reconcile (
        p_audit_entry_id IN t2656_audit_entry.c2656_audit_entry_id%TYPE,
        p_comment        IN VARCHAR2,
        p_user_id        IN t101_user.c101_user_id%TYPE,
        p_out_txnid OUT VARCHAR2)
AS
    v_adj_type t906_rules.c906_rule_value%TYPE;
    v_ref_type t901_code_lookup.c901_code_id%TYPE;
    v_status t901_code_lookup.c901_code_id%TYPE;
    v_refid VARCHAR2 (40) ;
    v_dist t2651_field_sales_details.c701_distributor_id%TYPE;
    v_part t2656_audit_entry.c205_part_number_id%TYPE;
    v_setid t2656_audit_entry.c207_set_id%TYPE;
    v_tag_id    VARCHAR2 (20) ;
    v_retag_id  VARCHAR2 (40) ;
    v_out_txn   VARCHAR2 (400) ;
    v_cnt       NUMBER;
    v_out_refid VARCHAR2 (40) ;
    v_cnum t2656_audit_entry.c2656_control_number%TYPE;
    v_missing_tag NUMBER;
    v_retag_fl t2656_audit_entry.c2656_retag_fl%TYPE;
    v_part_str VARCHAR2 (2000) ;
    v_tag_cnt  NUMBER;
BEGIN
     SELECT t2656.c901_ref_type, t2656.c2656_ref_id, NVL (t2656.c901_status, 0)
      , t2656.c205_part_number_id, t2656.c207_set_id, t2656.c5010_tag_id
      , t2651.c701_distributor_id, t2656.c2656_control_number, t2656.c5010_retag_id
      , t2656.c2656_retag_fl
       INTO v_ref_type, v_refid, v_status
      , v_part, v_setid, v_tag_id
      , v_dist, v_cnum, v_retag_id
      , v_retag_fl
       FROM t2656_audit_entry t2656, t2651_field_sales_details t2651
      WHERE t2651.c2651_field_sales_details_id = t2656.c2651_field_sales_details_id
        AND t2656.c2656_audit_entry_id         = p_audit_entry_id
        AND t2656.c2656_void_fl               IS NULL
        AND t2651.c2651_void_fl               IS NULL;
    -- to set the retag id to tag id
    v_retag_id    := get_retag_id (p_audit_entry_id) ;
    IF v_retag_id IS NOT NULL THEN
        v_tag_id  := v_retag_id;
    END IF;
    -- Validate the tag id (to reconcile the tag like 'no tagX')
    IF (v_ref_type <> 18552) AND (v_ref_type <> 18558)  THEN -- UnVoidTag Only and Void Tag Only
     SELECT COUNT (1)
       INTO v_tag_cnt
       FROM t5010_tag
      WHERE c5010_tag_id   = v_tag_id
        AND c5010_void_fl IS NULL;
    -- tag id count 0 then, its 'No TagX'
    IF v_tag_cnt  = 0 THEN
        v_tag_id := NULL;
    END IF;
    END IF;
    IF v_ref_type = 51023 OR v_ref_type = 51027 -- Transfer From/To
        THEN
        gm_sav_reconcile_transfer (p_audit_entry_id, p_comment, p_user_id, v_out_txn) ;
    ELSIF v_ref_type = 51024 THEN -- Extra
        --Raise Error if the flag Reason = "Part" and c901_status= 'Complete'.
        IF v_refid = '18562' AND v_status = 51020 -- 18562 Part , 51020 - Complete
            THEN
            GM_RAISE_APPLICATION_ERROR('-20999','148','');
        END IF;
        --Raise Error if the flag Reason = "Set" and c901_status= 'Empty'
        IF v_refid = '18561' AND v_status = 51022 THEN
            GM_RAISE_APPLICATION_ERROR('-20999','149','');
        END IF;
        --Rule mapping Extra (51024)
        v_adj_type := get_rule_value (v_refid, 'FACONADJTYPE') ;
        -- ref id Set then part number should be null
        v_part_str := v_part || ',1' ||'|';
        IF v_refid  = '18561' AND v_setid IS NULL -- Set
            THEN
            --Set ID cannot be empty for consignment adjustment by set.
            GM_RAISE_APPLICATION_ERROR('-20999','124','');
        END IF;
        IF v_adj_type IS NOT NULL THEN
            gm_pkg_ac_fa_reconciliation.gm_ac_sav_cons_adj (TO_NUMBER (v_adj_type), v_dist, v_part_str, v_setid,
            v_tag_id, p_comment, '18642', p_user_id, v_out_txn) ; -- 18642 Field Audit Reconcilation (Screen)
        END IF;
    ELSIF v_ref_type = 51028 THEN
        --Rule mapping Missing(51028)
        v_adj_type := get_rule_value (v_refid, 'FACONADJTYPE') ;
        -- ref id Set then part number should be null
        v_part_str := v_part || ',1' ||'|';
        IF (v_refid = '51040' OR v_refid = '51041') AND v_setid IS NULL -- Charge Set, No Charge Set
            THEN
            --Set ID cannot be empty for consignment adjustment by set.
            GM_RAISE_APPLICATION_ERROR('-20999','123','');
        END IF;
        IF v_adj_type IS NOT NULL THEN
            gm_pkg_ac_fa_reconciliation.gm_ac_sav_cons_adj (TO_NUMBER (v_adj_type), v_dist, v_part_str, v_setid,
            v_tag_id, p_comment, '18642', p_user_id, v_out_txn) ; -- 18642 Field Audit Reconcilation (Screen)
        END IF;
    ELSIF v_ref_type = 18552 THEN --Unvoid tag Only
        gm_update_tag_recon (v_tag_id, NULL, p_user_id) ;
    ELSIF v_ref_type = 18558 THEN --Void tag Only
        gm_update_tag_recon (v_tag_id, 'Y', p_user_id) ;
    ELSIF v_ref_type = 51029 THEN --Return
         SELECT COUNT (1)
           INTO v_missing_tag
           FROM t506_returns t506, t507_returns_item t507
          WHERE t506.C506_VOID_FL            IS NULL
            AND t507.c506_rma_id              = v_refid
            AND t507.c506_rma_id              = t506.c506_rma_id
            AND t507.c507_missing_fl          = 'Y'
            AND t507.c507_status_fl           = 'C'
            AND t507.c901_missing_tag_reason IS NULL;
        IF v_missing_tag                      > 0 THEN
            gm_sav_missing_tag_recon (p_audit_entry_id, p_user_id, v_out_txn) ;
        END IF;
    END IF;
    p_out_txnid := v_out_txn;
END gm_sav_consign_reconcile;
/******************************************************************************
**
* Description : Procedure to transfer a set to another distributor.
* Author    : Mihir Patel
*******************************************************************************
**/
PROCEDURE gm_sav_reconcile_transfer (
        p_audit_entry_id IN t2656_audit_entry.c2656_audit_entry_id%TYPE,
        p_comment        IN VARCHAR2,
        p_user_id        IN t101_user.c101_user_id%TYPE,
        p_out_txnid OUT VARCHAR2)
AS
    v_accp_tf_inpustr CLOB ;
    v_refid_inputstr  VARCHAR2 (4000) ;
    v_tagtransfer_cur TYPES.cursor_type;
    v_txn_id  VARCHAR2 (40) ;
    v_message VARCHAR2 (3000) ;
    v_ref_type t901_code_lookup.c901_code_id%TYPE;
    v_refid VARCHAR2 (40) ;
    v_distid t2651_field_sales_details.c701_distributor_id%TYPE;
    v_from_id  VARCHAR2 (40) ;
    v_to_id    VARCHAR2 (40) ;
    v_tag_id   VARCHAR2 (20) ;
    v_tagid    VARCHAR2 (40) ;
    v_retag_id VARCHAR2 (40) ;
    v_tagcnt   NUMBER;
    v_set_id t207_set_master.c207_set_id%TYPE;
    v_status t901_code_lookup.c901_code_id%TYPE;
    v_cnt NUMBER;
    v_item_id t507_returns_item.c507_returns_item_id%TYPE;
    v_missing_fl t507_returns_item.c507_missing_fl%TYPE;
    v_pnum t507_returns_item.c205_part_number_id%TYPE;
    v_loc_id  VARCHAR2 (200) ;
    v_loc_num VARCHAR2 (40) ;
    v_loc_tag VARCHAR2 (400) ;
    v_cnum t5010_tag.c5010_control_number%TYPE;
    v_ref_id t506_returns.c506_rma_id%TYPE;
    v_setid t207_set_master.c207_set_id%TYPE;
    v_qty t507_returns_item.c507_item_qty%TYPE;
    v_out_errmsg VARCHAR2 (4000) ;
    v_retag_fl t2656_audit_entry.c2656_retag_fl%TYPE;
    v_tag_cnt NUMBER;
    v_dateformat varchar2(100);
    CURSOR setdet_cur
    IS
         SELECT t208.c205_part_number_id pnum, NVL (t208.c208_set_qty, 0) setqty, t208.c208_set_details_id mid
          , get_partnum_desc (t208.c205_part_number_id) pdesc, t208.c208_inset_fl insetfl, NVL (get_part_price (
            t208.c205_part_number_id, 'L'), 0) lprice
           FROM t208_set_details t208
          WHERE t208.c208_void_fl IS NULL
            AND t208.c207_set_id   = v_set_id
            AND t208.c208_set_qty <> 0
            AND t208.c208_inset_fl = 'Y'
       ORDER BY t208.c205_part_number_id, t208.c208_critical_fl DESC, t208.c208_inset_fl DESC;
    --
	BEGIN
		select NVL(get_compdtfmt_frm_cntx(),GET_RULE_VALUE('DATEFMT','DATEFORMAT')) into v_dateformat from dual;
		
     SELECT t2656.c901_ref_type, t2656.c2656_ref_id, NVL (t2656.c901_status, 0)
      , t2656.c205_part_number_id, t2656.c207_set_id, t2656.c5010_tag_id
      , t2651.c701_distributor_id, t2656.c5010_retag_id, t2656.c2656_retag_fl
       INTO v_ref_type, v_refid, v_status
      , v_pnum, v_set_id, v_tagid
      , v_distid, v_retag_id, v_retag_fl
       FROM t2656_audit_entry t2656, t2651_field_sales_details t2651
      WHERE t2651.c2651_field_sales_details_id = t2656.c2651_field_sales_details_id
        AND t2656.c2656_audit_entry_id         = p_audit_entry_id
        AND t2656.c2656_void_fl               IS NULL
        AND t2651.c2651_void_fl               IS NULL;
    -- Transfer From or Transfer To
    IF (v_ref_type     = 51023 OR v_ref_type = 51027) THEN
        IF v_ref_type  = 51023 THEN
            v_from_id := v_refid;
            v_to_id   := v_distid;
        ELSE
            v_from_id := v_distid;
            v_to_id   := v_refid;
        END IF;
    END IF;
    --Cannot do a set transfer if the set id is NULL
    IF v_set_id IS NULL THEN
        GM_RAISE_APPLICATION_ERROR('-20999','125','');
    END IF;
    -- tag id 'No Tag X' then to take the re tag id
    v_retag_id    := get_retag_id (p_audit_entry_id) ;
    IF v_retag_id IS NOT NULL THEN
        v_tagid   := v_retag_id;
    END IF;
    --if 'no tag X' and not retagged then such tag are invalid
     SELECT COUNT (1)
       INTO v_tag_cnt
       FROM t5010_tag
      WHERE c5010_tag_id   = v_tagid
        AND c5010_void_fl IS NULL;
    IF v_tag_cnt           = 0 THEN
        GM_RAISE_APPLICATION_ERROR('-20999','126','');
    END IF;
    -- For Initiating the transfer get the part number and qty and then prepare
    -- the input string
    v_refid_inputstr := '';
    FOR setdet       IN setdet_cur
    LOOP
        -- IF v_index.qty > 0 AND v_index.setqty > 0
        -- THEN
        IF setdet.setqty      > 0 THEN
            v_refid_inputstr := v_refid_inputstr || setdet.pnum || '^' || setdet.setqty || '|';
        END IF;
    END LOOP;
    -- Initiate the Transfer
    gm_pkg_cs_transfer.gm_cs_sav_transfer (90300 -- Distributor to Distributor
    , 90351 -- Set Transfer
    , v_from_id, v_to_id, 90323 -- Field Sales Audit Reconciliation
    , TO_CHAR (CURRENT_DATE, v_dateformat), p_user_id, v_set_id, v_refid_inputstr, v_txn_id) ;
    gm_update_log (v_txn_id, 'Transfer made as a part of the Field Audit Reconciliation process', 1209, p_user_id,
    v_message) ;
    --Fetch item return id so as to form the input string to be passed to
    -- existing procedure
    gm_pkg_cs_transfer.gm_cs_fch_tag_transfer (v_txn_id, 90300, v_tagtransfer_cur) ;
    BEGIN
        v_cnt := 0;
        LOOP
            FETCH v_tagtransfer_cur
               INTO v_item_id, v_tag_id, v_missing_fl
              , v_pnum, v_loc_id, v_loc_num
              , v_loc_tag, v_cnum, v_ref_id
              , v_setid, v_qty;
            EXIT
        WHEN v_tagtransfer_cur%NOTFOUND;
            v_accp_tf_inpustr := v_accp_tf_inpustr || v_item_id || '^' || v_tagid || '^' || NULL ||'^' ||v_ref_id ||
            '|';
            v_cnt := v_cnt + 1;
        END LOOP;
        CLOSE v_tagtransfer_cur;
    EXCEPTION
    WHEN OTHERS THEN
        CLOSE v_tagtransfer_cur;
        RAISE;
    END;
    IF v_cnt = 0 THEN
        GM_RAISE_APPLICATION_ERROR('-20999','150',v_txn_id);
    END IF;
    IF v_cnt > 1 THEN
        GM_RAISE_APPLICATION_ERROR('-20999','151',v_txn_id);
    END IF;
    --
    -- Accept the transfer :54011 indicates that  Tag Validation is required
    -- during transfer
    gm_pkg_cs_transfer.gm_cs_sav_accept_transfer (v_txn_id, p_user_id, 'Y', v_accp_tf_inpustr, 54011, v_out_errmsg) ;
    gm_update_log (v_txn_id, 'Transfer verified as a part of the Field Audit Reconciliation process', 1209, p_user_id,
    v_message) ;
    p_out_txnid := v_txn_id;
    --
END gm_sav_reconcile_transfer;
/******************************************************************************
**
* Description : Procedure to update status of Audit/Field Sales.Can be reflected in Audit Set up screen
* Author    : Mihir Patel
*******************************************************************************/
PROCEDURE gm_sav_update_status (
        p_audit_entry_id IN t2656_audit_entry.c2656_audit_entry_id%TYPE,
        p_txn_id         IN VARCHAR2,
        p_user_id        IN t2656_audit_entry.c2656_last_updated_by%TYPE)
AS
    v_fs_det_id t2651_field_sales_details.c2651_field_sales_details_id%TYPE;
    v_dist_status_cnt NUMBER;
    v_all_rec_cnt     NUMBER;
    v_all_dev_cnt     NUMBER;
    v_rec_status      NUMBER;
    v_distid t2651_field_sales_details.c701_distributor_id%TYPE;
    v_fs_audit_id t2650_field_sales_audit.c2650_field_sales_audit_id%TYPE;
    v_fs_close_cnt NUMBER;
    v_all_fs_cnt   NUMBER;
BEGIN
     SELECT c2651_field_sales_details_id
       INTO v_fs_det_id
       FROM t2656_audit_entry
      WHERE c2656_audit_entry_id = p_audit_entry_id
        AND c2656_void_fl       IS NULL FOR UPDATE;
     SELECT t2651.c701_distributor_id, c2650_field_sales_audit_id
       INTO v_distid, v_fs_audit_id
       FROM t2651_field_sales_details t2651
      WHERE t2651.c2651_field_sales_details_id = v_fs_det_id FOR UPDATE;
     UPDATE t2656_audit_entry t2656
    SET t2656.c2656_recon_txn_id                 = p_txn_id, t2656.c901_reconciliation_status = 51038 -- Reconciled
      , t2656.c2656_reconciled_by                = p_user_id, t2656.c2656_reconciled_dt = CURRENT_DATE, t2656.c2656_last_updated_by =
        p_user_id, t2656.c2656_last_updated_date = CURRENT_DATE
      WHERE t2656.c2656_audit_entry_id           = p_audit_entry_id
        AND t2656.c2656_void_fl                 IS NULL;
    -- Check the status of the field sales
     SELECT COUNT (1)
       INTO v_dist_status_cnt
       FROM t2651_field_sales_details t2651
      WHERE t2651.c2651_field_sales_details_id = v_fs_det_id
        AND t2651.c901_reconciliation_status   = 18527 -- Partially Reconciled
        AND t2651.c2651_void_fl               IS NULL;
    -- fetch All reconciled qty to update the status to close
     SELECT COUNT (1)
       INTO v_all_rec_cnt
       FROM t2656_audit_entry t2656
      WHERE t2656.c2651_field_sales_details_id = v_fs_det_id
        AND t2656.c901_reconciliation_status   = 51038 -- Reconciled
        AND t2656.c2656_void_fl               IS NULL;
    -- fetch All deviation qty to update the status to close
     SELECT COUNT (1)
       INTO v_all_dev_cnt
       FROM t2656_audit_entry t2656
      WHERE t2656.c901_Tag_Status             IN (18542, 18543) -- Postive dev and negative dev tags
        AND t2656.c2651_field_sales_details_id = v_fs_det_id
        AND t2656.c2656_void_fl               IS NULL;
    --If any of the record is reconciled then set the status of the field sales
    -- as Partially Reconciled.
    IF v_all_rec_cnt  > 0 AND v_dist_status_cnt = 0 THEN
        v_rec_status := 18527; --Partially Reconciled
    END IF;
    --If all the records are reconcilled then set the status of the Field Sales
    -- as Closed
    IF v_all_rec_cnt  = v_all_dev_cnt THEN
        v_rec_status := 18528;--Closed
    END IF;
     UPDATE t2651_field_sales_details t2651
    SET t2651.c2651_last_updated_by            = p_user_id, t2651.c2651_last_updated_date = CURRENT_DATE,
        t2651.c901_reconciliation_status       = NVL (v_rec_status, t2651.c901_reconciliation_status)
      WHERE t2651.c2651_field_sales_details_id = v_fs_det_id
        AND t2651.c2651_void_fl               IS NULL;
    -- to get the all field sales count
     SELECT COUNT (1)
       INTO v_all_fs_cnt
       FROM t2651_field_sales_details
      WHERE c2650_field_sales_audit_id = v_fs_audit_id
        AND c2651_void_fl             IS NULL;
    -- to get the closed field sales count
     SELECT COUNT (1)
       INTO v_fs_close_cnt
       FROM t2651_field_sales_details
      WHERE c2650_field_sales_audit_id = v_fs_audit_id
        AND c901_reconciliation_status = 18528 -- Closed
        AND c2651_void_fl             IS NULL ;
    -- closed FS and all FS equal then - audit to be closed
    IF v_all_fs_cnt = v_fs_close_cnt THEN
         UPDATE t2650_field_sales_audit
        SET c901_audit_status              = 18504, c2650_last_updated_by = p_user_id -- 18504 (Closed)
          , c2650_last_updated_date        = CURRENT_DATE, c2650_end_date = CURRENT_DATE
          WHERE c2650_field_sales_audit_id = v_fs_audit_id
            AND c2650_void_fl             IS NULL;
    END IF;
END gm_sav_update_status;
/******************************************************************************
**
Description : Procedure to save the email type for Loaner type Tags and void unvoid loaner tag
*******************************************************************************
**/
PROCEDURE gm_sav_loaner_reconcile (
        p_audit_entry_id IN t2656_audit_entry.c2656_audit_entry_id%TYPE,
        p_user_id        IN t101_user.c101_user_id%TYPE)
AS
    v_email_type_id NUMBER;
    v_email_type    VARCHAR2 (10) ;
    v_ref_type t901_code_lookup.c901_code_id%TYPE;
    v_status t901_code_lookup.c901_code_id%TYPE;
    v_refid    VARCHAR2 (40) ;
    v_tag_id   VARCHAR2 (20) ;
    v_retag_id VARCHAR2 (40) ;
    v_retag_fl t2656_audit_entry.c2656_retag_fl%TYPE;
BEGIN
     SELECT C901_REF_TYPE, C2656_REF_ID, NVL (c901_status, 0)
      , C5010_TAG_ID, c5010_retag_id, c2656_retag_fl
       INTO v_ref_type, v_refid, v_status
      , v_tag_id, v_retag_id, v_retag_fl
       FROM t2656_audit_entry
      WHERE c2656_audit_entry_id = p_audit_entry_id
        AND c2656_void_fl       IS NULL;
    /* Checking the Ref type for Transfer To or Transafer From for Flag */
    IF v_ref_type     = 51027 OR v_ref_type = 51023 THEN
        v_email_type := 18617;
        gm_sav_email_fl (p_audit_entry_id, v_email_type) ;
    END IF;
    /* Checking the Ref type  Extra for Flag */
    IF v_ref_type     = 51024 THEN
        v_email_type := 18618;
        gm_sav_email_fl (p_audit_entry_id, v_email_type) ;
    END IF;
    /* Checking the Ref type  Missing for Flag */
    IF v_ref_type     = 51028 THEN
        v_email_type := 18619;
        gm_sav_email_fl (p_audit_entry_id, v_email_type) ;
    END IF;
    --
    IF v_retag_id IS NOT NULL AND v_retag_fl IS NOT NULL THEN
        v_tag_id  := v_retag_id;
    END IF;
    /* Checking the Ref type  Unvoid tag Only /Void tag Only */
    IF v_ref_type = 18552 THEN --Unvoid tag Only
        gm_update_tag_recon (v_tag_id, NULL, p_user_id) ;
    ELSIF v_ref_type = 18558 THEN --Void tag Only
        gm_update_tag_recon (v_tag_id, 'Y', p_user_id) ;
    END IF;
END gm_sav_loaner_reconcile;
/******************************************************************************
**
* Description : Procedure to fetch Parameters for sending the Reconcile Email
*******************************************************************************
**/
PROCEDURE gm_fch_reconcile_email (
        p_out_emaildet OUT TYPES.cursor_type)
AS
BEGIN
    OPEN p_out_emaildet FOR SELECT t2656.c2656_audit_entry_id AUDITENTRYID,
    c901_email_type TEMPLATETYPE,
    get_rule_value (c901_email_type, 'FARECEMAILSUB') SUBJECT,
    get_rule_value (c901_email_type, 'FARECEMAILTOADD') EmailTo,
    get_rule_value (c901_email_type, 'FARECEMAILHEAD') EmailHeader,
    get_code_name (t701.c701_region) regionnm,
    DECODE (t2656.c2656_retag_fl, NULL, t2656.C5010_TAG_ID, t2656.c5010_retag_id) tagid,
    t2656.c207_set_id setid,
    C701_DISTRIBUTOR_NAME distname,
    t5010.C5010_LAST_UPDATED_TRANS_ID transid,
    DECODE (GM_PKG_OP_LOANER.get_cs_fch_loaner_status (t5010.C5010_LAST_UPDATED_TRANS_ID), '60', 'InActive', 'Active')
    tagstatus,
    DECODE (TO_CHAR (t5010.c901_location_type), '4120', TO_CHAR (get_distributor_name (t5010.c5010_location_id)),
    '4123', TO_CHAR (get_user_name (t5010.c5010_location_id)), TO_CHAR (get_code_name (t5010.c5010_location_id)))
    currloc,
    GM_PKG_OP_LOANER.get_loaner_etch_id (t5010.C5010_LAST_UPDATED_TRANS_ID) etchid FROM t2651_field_sales_details t2651
    ,
    t2656_audit_entry t2656,
    t5010_tag t5010,
    t701_distributor t701,
    t2652_field_sales_lock t2652 WHERE t2651.C2651_FIELD_SALES_DETAILS_ID = t2656.C2651_FIELD_SALES_DETAILS_ID AND
    t2656.C5010_TAG_ID                                                    = t5010.C5010_TAG_ID (+) AND
    t2651.C701_DISTRIBUTOR_ID                                             = t701.C701_DISTRIBUTOR_ID AND
    t2651.C2652_FIELD_SALES_LOCK_ID                                       = t2652.C2652_FIELD_SALES_LOCK_ID AND
    t2656.c2656_void_fl                                                  IS NULL AND t2651.c2651_void_fl IS NULL AND
    t5010.c5010_void_fl                                                  IS NULL AND t701.c701_void_fl IS NULL AND
    t2652.c2652_void_fl                                                  IS NULL AND t2656.c2656_email_fl IS NOT NULL
    AND t2656.c901_email_type                                            IS NOT NULL;
END gm_fch_reconcile_email;
/********************************************************
* Description : Procedure to save the Email Flag value
*********************************************************/
PROCEDURE gm_sav_email_fl (
        p_input_str  IN VARCHAR2,
        p_email_type IN VARCHAR2)
AS
BEGIN
    my_context.set_my_inlist_ctx (p_input_str) ;
     UPDATE t2656_audit_entry
    SET c2656_email_fl = DECODE (p_email_type, NULL, '', 'Y'), c901_email_type = DECODE (p_email_type, NULL,
        c901_email_type, p_email_type)
      WHERE c2656_audit_entry_id IN
        (
             SELECT token FROM v_in_list
        ) ;
END gm_sav_email_fl;
/******************************************************************************
**
* Description : Procedure to fetch PDF Report (Confirmation Sheet)
*******************************************************************************
**/
PROCEDURE gm_fetch_confirmation_report (
        p_audit_id IN t2652_field_sales_lock.c2650_field_sales_audit_id%TYPE,
        p_dist_id  IN t2651_field_sales_details.c701_distributor_id%TYPE,
        p_out_header_info OUT TYPES.cursor_type,
        p_out_overall_count_cur OUT TYPES.cursor_type,
        p_out_sets_missing_cur OUT TYPES.cursor_type,
        p_out_sets_extra_cur OUT TYPES.cursor_type,
        p_out_borrowed_cur OUT TYPES.cursor_type,
        p_out_sets_matching_cur OUT TYPES.cursor_type)
AS
    v_flagged_qty   NUMBER;
    v_cnt           NUMBER;
    v_fs_status_num NUMBER;
    v_fs_status_des VARCHAR2 (40) ;
BEGIN
     SELECT COUNT (1)
       INTO v_cnt
       FROM t2656_audit_entry T2656, t2651_field_sales_details T2651
      WHERE T2656.c2651_field_sales_details_id = t2651.c2651_field_sales_details_id
        AND t2651.c2650_field_sales_audit_id   = p_audit_id
        AND t2651.c701_distributor_id          = p_dist_id
        AND t2656.c901_tag_status             IN (18542, 18543)
        AND t2651.c2651_void_fl               IS NULL
        AND t2656.c2656_void_fl               IS NULL;
     SELECT COUNT (1)
       INTO v_flagged_qty
       FROM t2656_audit_entry T2656, t2651_field_sales_details T2651
      WHERE T2656.c2651_field_sales_details_id = t2651.c2651_field_sales_details_id
        AND t2651.c2650_field_sales_audit_id   = p_audit_id
        AND t2651.c701_distributor_id          = p_dist_id
        AND t2656.c901_tag_status             IN (18542, 18543)
        AND t2656.c901_ref_type               IS NOT NULL
        AND t2656.c2656_flagged_by 			  IS NOT NULL
        AND t2651.c2651_void_fl               IS NULL
        AND t2656.c2656_void_fl               IS NULL;
    IF (v_cnt                                 <> v_flagged_qty) THEN
        raise_application_error ('-20195', '') ;
    END IF;
     SELECT NVL (c901_reconciliation_status, 0), get_code_name (c901_reconciliation_status)
       INTO v_fs_status_num, v_fs_status_des
       FROM t2651_field_sales_details
      WHERE c2650_field_sales_audit_id = p_audit_id
        AND c701_distributor_id        = p_dist_id
        AND c2651_void_fl             IS NULL;
    -- 18525 (Data Uploaded) After the Data uploaded to generate the confirmation sheet
    IF v_fs_status_num < 18525 THEN
        GM_RAISE_APPLICATION_ERROR('-20999','152',v_fs_status_des);
    END IF;
    gm_fch_audit_header (p_audit_id, p_dist_id, p_out_header_info) ;
    gm_fch_overall_count (p_audit_id, p_dist_id, p_out_overall_count_cur) ;
    gm_fch_field_sales_flagged_rpt (p_audit_id, p_dist_id, '18543', p_out_sets_missing_cur) ;
    gm_fch_field_sales_flagged_rpt (p_audit_id, p_dist_id, '18542', p_out_sets_extra_cur) ;
    gm_fch_field_sales_flagged_rpt (p_audit_id, p_dist_id, 'BORROWED_LENT_TRANS', p_out_borrowed_cur) ;
    gm_fch_fs_match_ver_rpt (p_audit_id, p_dist_id, '18541', p_out_sets_matching_cur) ;
END gm_fetch_confirmation_report;
/******************************************************************************
**
* Description : Procedure to fetch Audit Header information
*******************************************************************************
**/
PROCEDURE gm_fch_audit_header (
        p_audit_id IN t2652_field_sales_lock.c2650_field_sales_audit_id%TYPE,
        p_dist_id  IN t2651_field_sales_details.c701_distributor_id%TYPE,
        p_out_header_info OUT TYPES.cursor_type)
AS
BEGIN
    OPEN p_out_header_info FOR SELECT get_user_name (t2658.c2658_audit_user_id) audit_user,
    get_dist_bill_add (t2651.c701_distributor_id) address,
    get_distributor_name (t2651.c701_distributor_id) dist_name,
    get_rule_value ('EMP_NAME', 'FA_LETTER') emp_name,
    get_rule_value ('EMP_DESIGN', 'FA_LETTER') emp_designation,
    get_distributor_email (p_dist_id) dist_email FROM t2651_field_sales_details t2651,
    t2658_audit_team t2658 WHERE t2651.c2651_field_sales_details_id = t2658.c2651_field_sales_details_id AND
    t2651.c2650_field_sales_audit_id                                = p_audit_id AND t2651.c701_distributor_id =
    p_dist_id AND t2651.c2651_void_fl                              IS NULL AND t2658.c2658_void_fl IS NULL;
END gm_fch_audit_header;
/******************************************************************************
**
* Description : Procedure to fetch overall count PDF Report
*******************************************************************************
**/
PROCEDURE gm_fch_overall_count (
        p_audit_id IN t2652_field_sales_lock.c2650_field_sales_audit_id%TYPE,
        p_dist_id  IN t2651_field_sales_details.c701_distributor_id%TYPE,
        p_out_overall_counts_cur OUT TYPES.cursor_type)
AS
BEGIN
    /*
    * Missing Section - Only Flagged as Missing.
    * Extra Section - Only Flagged as Extra.
    * Matching Section - Flagged as (Not a Deviation, Unvoid Tag only, Returns,
    Void Tag only, Verified Post Count, Not a Deviation)
    *                     and Matcing Tags.
    * Borrowed/Lent/Trans - Only Flagged as Borrowed From,Lend to, Trnasfer From/
    To.
    */
    OPEN p_out_overall_counts_cur FOR SELECT NVL (SUM (DECODE (t2656.c901_ref_type, 51028, 1, 0)), 0) MISSING,
    NVL (SUM (DECODE (t2656.c901_ref_type, 51024, 1, 0)), 0) extra,
    NVL (SUM (DECODE (t2656.c901_tag_status, 18541, 1, DECODE (t2656.c901_ref_type, 18551, 1, 18552, 1, 18556, 1, 18557
    , 1, 18558, 1, 51029, 1, 0))), 0) matching,
    NVL (SUM (DECODE (t2656.c901_ref_type, 51025, 1, 51023, 1, 51026, 1, 51027, 1, 0)), 0) borrowed FROM
    t2656_audit_entry t2656,
    t2651_field_sales_details t2651 WHERE t2656.c2651_field_sales_details_id = t2651.c2651_field_sales_details_id AND
    t2651.c2650_field_sales_audit_id                                         = p_audit_id AND t2651.c701_distributor_id
                                                                             = p_dist_id AND t2651.c2651_void_fl IS
    NULL AND t2656.c2656_void_fl                                            IS NULL;
END gm_fch_overall_count;
/******************************************************************************
********************************
* Description : Procedure to fetch Field Sales Flagged report(Negative,Positive
and Borrowed,Lent, Transfer)
*******************************************************************************
********************************/
PROCEDURE gm_fch_field_sales_flagged_rpt (
        p_audit_id   IN t2652_field_sales_lock.c2650_field_sales_audit_id%TYPE,
        p_dist_id    IN t2651_field_sales_details.c701_distributor_id%TYPE,
        p_tag_status IN VARCHAR2,
        p_out_flagged_report OUT TYPES.cursor_type)
AS
    v_rule_value VARCHAR2 (2000) ;
BEGIN
    -- based on the tag staus - to get the rule values (only Flagged set).
    v_rule_value := get_rule_value (p_tag_status, 'FA_PDF_FLAG_TYPE') ;
    /* For Example : Rule ID - 18543 (Negative) then Rule values (return) - 51028
    (Missing)
    *               Rule ID - 18542 (Positive) then Rule values (return) - 51024
    (Extra)
    *               Rule ID - BORROWED_LENT_TRANS (Borrowed Lent Transfer and
    Return) then Rule values (51025, 51023, 51026, 51027)
    */
    --my_context.set_my_inlist_ctx (v_rule_value);
    -- my context not work. because this procedure call mulitple time(3 time),So
    -- last rule values only store the session and result will be come wrong.
    -- So we go over the regular exp.
    OPEN p_out_flagged_report FOR SELECT TO_CHAR (t2656.c5010_tag_id) tagid,
    t2656.c205_part_number_id pnum,
    t205.c205_part_num_desc pdesc,
    t2656.c2656_control_number cnum,
    t2656.c207_set_id setid,
    t2656.c2656_comments comments,
    get_code_name (t2656.c901_location_type) systemloc FROM t2656_audit_entry t2656,
    t2651_field_sales_details t2651,
    t205_part_number t205 WHERE t2651.c2651_field_sales_details_id = t2656.c2651_field_sales_details_id AND
    t205.c205_part_number_id                                       = t2656.c205_part_number_id AND
    t2651.c2650_field_sales_audit_id                               = p_audit_id AND t2651.c701_distributor_id =
    p_dist_id AND t2656.c901_ref_type                             IN
    (
         SELECT regexp_substr (v_rule_value, '[^,]+', 1, level)
           FROM dual
            CONNECT BY regexp_substr (v_rule_value, '[^,]+', 1, level) IS NOT NULL
    )
    AND c2656_void_fl IS NULL AND c2651_void_fl IS NULL ORDER BY t2656.c207_set_id;
END gm_fch_field_sales_flagged_rpt;
/******************************************************************************
**
* Description : Procedure to fetch Matching set + Verified Post Count set
reports.
*******************************************************************************
**/
PROCEDURE gm_fch_fs_match_ver_rpt (
        p_audit_id   IN t2652_field_sales_lock.c2650_field_sales_audit_id%TYPE,
        p_dist_id    IN t2651_field_sales_details.c701_distributor_id%TYPE,
        p_tag_status IN VARCHAR2,
        p_out_match_report OUT TYPES.cursor_type)
AS
    v_rule_value VARCHAR2 (2000) ;
BEGIN
    v_rule_value := get_rule_value (p_tag_status, 'FA_PDF_FLAG_TYPE') ;
    my_context.set_my_inlist_ctx (v_rule_value) ;
    OPEN p_out_match_report FOR SELECT TO_CHAR (t2656.c5010_tag_id) tagid,
    t2656.c205_part_number_id pnum,
    t205.c205_part_num_desc pdesc,
    t2656.c2656_control_number cnum,
    t2656.c207_set_id setid,
    t2656.c2656_comments comments,
    get_code_name (t2656.c901_location_type) systemloc FROM t2656_audit_entry t2656,
    t2651_field_sales_details t2651,
    t205_part_number t205 WHERE t2651.c2651_field_sales_details_id = t2656.c2651_field_sales_details_id AND
    t205.c205_part_number_id                                       = t2656.c205_part_number_id AND
    t2651.c2650_field_sales_audit_id                               = p_audit_id AND t2651.c701_distributor_id =
    p_dist_id AND (t2656.c901_tag_status                           = p_tag_status OR t2656.c901_ref_type IN
        (
             SELECT token FROM v_in_list
        )) AND c2656_void_fl IS NULL AND c2651_void_fl IS NULL ORDER BY t2656.c207_set_id;
END gm_fch_fs_match_ver_rpt;
/******************************************************************************
**
* Description : Procedure to fetch Pending and Invoiced charge report
*******************************************************************************
**/
PROCEDURE gm_fch_inv_pend_charge_rpt (
        p_audit_id IN t2652_field_sales_lock.c2650_field_sales_audit_id%TYPE,
        p_dist_id  IN t2651_field_sales_details.c701_distributor_id%TYPE,
        p_ref_id   IN VARCHAR2,
        p_out_report OUT TYPES.cursor_type,
        p_out_details OUT TYPES.cursor_type)
AS
BEGIN
    OPEN p_out_report FOR SELECT TO_CHAR (t2656.c5010_tag_id) tagid,
    t2656.c205_part_number_id pnum,
    get_partnum_desc (t2656.c205_part_number_id) pdesc,
    t2656.c2656_control_number cnum,
    t2656.c207_set_id setid,
    get_set_name (t2656.c207_set_id) setdesc FROM t2656_audit_entry t2656,
    t2651_field_sales_details t2651 WHERE t2651.c2651_field_sales_details_id = t2656.c2651_field_sales_details_id AND
    t2651.c2650_field_sales_audit_id                                         = p_audit_id AND t2651.c701_distributor_id
                                                                             = p_dist_id AND t2656.c901_ref_type =
    51028 -- Missing
    AND t2656.c2656_ref_id IS NOT NULL AND t2656.c2656_ref_id = DECODE (p_ref_id, 'PENDING_CHARGE', t2656.c2656_ref_id,
    DECODE (t2656.c2656_ref_id, '51040', t2656.c2656_ref_id, '18566', t2656.c2656_ref_id, NULL)) --Charge Set/Part #
    AND c2656_void_fl IS NULL AND c2651_void_fl IS NULL ORDER BY t2656.c207_set_id;
    OPEN p_out_details FOR SELECT regnm,
    SUM (totalcharg) totalcharg,
    SUM (setcost) setcost FROM
    (
         SELECT get_code_name (t701.c701_region) regnm, NVL (SUM (t2656.c2656_ref_details), 0) totalcharg, NVL (SUM (
            t2656.c2656_ref_details), 0) setcost
           FROM t2656_audit_entry t2656, t2651_field_sales_details t2651, t701_distributor t701
          WHERE t2651.c2651_field_sales_details_id = t2656.c2651_field_sales_details_id
            AND t701.c701_distributor_id           = t2651.c701_distributor_id
            AND t2651.c2650_field_sales_audit_id   = p_audit_id
            AND t2651.c701_distributor_id          = p_dist_id
            AND t2656.c901_ref_type                = 51028 -- Missing
            AND t2656.c2656_ref_id                IS NOT NULL
            AND c2656_void_fl                     IS NULL
            AND c2651_void_fl                     IS NULL
       GROUP BY t701.c701_region, t2656.c2656_ref_details
    )
    group by regnm;
END gm_fch_inv_pend_charge_rpt;
/***************************************************
Description : This procedure adjust Consignemnt by creating RA and CN based on
adjustment type.
*********************************************************/
PROCEDURE gm_ac_sav_cons_adj (
        p_type        IN t901_code_lookup.c901_code_id%TYPE,
        p_distid      IN t701_distributor.c701_distributor_id%TYPE,
        p_part_inpstr IN VARCHAR2,
        p_setid       IN t207_set_master.c207_set_id%TYPE,
        p_tag_id      IN t5010_tag.c5010_tag_id%TYPE,
        p_comment     IN VARCHAR2,
        p_screen_type IN VARCHAR2,
        p_user_id     IN t101_user.c101_user_id%TYPE,
        p_out_txn_ids OUT VARCHAR2)
AS
    v_cnt       NUMBER;
    v_out_refid VARCHAR2 (30) ;
    v_out_raid  VARCHAR2 (20) ;
    v_tag_part t208_set_details.c205_part_number_id%TYPE;
    v_out_conid   VARCHAR2 (20) ;
    v_def_comment VARCHAR2 (4000) :=
    'This TXN is created as a part of the Field Sales Audit: As per the Consignment Adjustment Request Form' ;
    v_comment     VARCHAR2 (4000) := NVL (p_comment, v_def_comment) ;
    p_out_tagpart VARCHAR2 (4000) ;
    v_message     VARCHAR2 (3000) ;
    --
BEGIN
     SELECT COUNT (1)
       INTO v_cnt
       FROM t701_distributor
      WHERE c701_distributor_id = p_distid
        AND c701_void_fl       IS NULL;
    --Check for a valid distributor
    IF v_cnt = 0 THEN
        GM_RAISE_APPLICATION_ERROR('-20999','153','');
    END IF;
    IF p_screen_type = '18642' THEN -- Field Audit Recon
        -- to split the taggable part. (p_part_inpstr - '901.002,1|')
        p_out_tagpart := SUBSTR (p_part_inpstr, 1, INSTR (p_part_inpstr, ',') - 1) ;
    END IF;
    IF p_type = 101201 OR p_type = 101200 THEN
        --For write off/up sets, part shuld be null
        IF p_part_inpstr IS NOT NULL AND p_screen_type <> '18642' THEN -- 18642 (Field Audit Reconcilation)
            GM_RAISE_APPLICATION_ERROR('-20999','154','');
        END IF;
        --For Set adjustment
        gm_ac_sav_set_adj (p_type, p_distid, p_setid, p_comment, p_screen_type, p_user_id, v_out_refid) ;
        --Tag Adjust
        IF p_tag_id IS NOT NULL AND p_setid <> '0' AND p_distid <> 0 THEN
            gm_ac_sav_link_tag (p_tag_id, p_setid, p_out_tagpart, p_distid, p_type, v_out_refid, p_screen_type,
            p_user_id) ;
        END IF;
    ELSIF p_type          = 101203 OR p_type = 101202 THEN
        IF p_screen_type <> '18642' THEN -- Field Audit Reconcile
            --For write off/up part, set id should be blank
            IF p_setid <> '0' THEN -- 18642 (Field Audit Reconcilation)
                GM_RAISE_APPLICATION_ERROR('-20999','155','');
            END IF;
            --Check for Taggable part
            IF p_part_inpstr IS NOT NULL THEN
                gm_ac_sav_validate_parts (p_part_inpstr, p_out_tagpart) ;
            ELSE
                GM_RAISE_APPLICATION_ERROR('-20999','156','');
            END IF;
        END IF; --p_screen_type
        --Part Adjust
        gm_ac_sav_part_adj (p_type, p_distid, p_part_inpstr, p_comment, p_user_id, v_out_refid) ;
        --Tag Adjust
        IF p_tag_id IS NOT NULL AND p_out_tagpart IS NOT NULL AND p_distid <> 0 THEN
            gm_ac_sav_link_tag (p_tag_id, p_setid, p_out_tagpart, p_distid, p_type, v_out_refid, p_screen_type,
            p_user_id) ;
        END IF;
    ELSIF p_type     = 101204 THEN -- WRITE-OFF-INACTIVE-DIST
        IF p_tag_id IS NULL AND p_out_tagpart IS NULL AND p_distid <> 0 AND p_setid = '0' THEN
            gm_sav_inactive_dist_writeoff (p_distid, p_user_id, v_comment, v_out_raid, v_out_conid) ;
            IF v_out_raid   IS NOT NULL OR v_out_conid IS NOT NULL THEN
                v_out_refid := v_out_raid;  
                IF v_out_conid IS NOT NULL AND v_out_raid IS NOT NULL THEN
                	v_out_refid := v_out_refid || ',';
                END IF;
                v_out_refid := v_out_refid || v_out_conid;
            ELSE
                GM_RAISE_APPLICATION_ERROR('-20999','157','');
            END IF;
            -- Comments insert for RA
            IF v_out_raid IS NOT NULL THEN
                gm_update_log (v_out_raid, 'Consignment Adjustment: '|| p_comment, 1280, p_user_id, v_message) ;
            END IF;
            -- Comments insert for CN
            IF v_out_conid IS NOT NULL THEN
                gm_update_log (v_out_conid, 'Consignment Adjustment: '|| p_comment, 1280, p_user_id, v_message) ;
            END IF;
        ELSE
            GM_RAISE_APPLICATION_ERROR('-20999','158','');
        END IF;
    ELSE
        GM_RAISE_APPLICATION_ERROR('-20999','159','');
    END IF;
    IF p_type <> 101204 THEN
        gm_update_log (v_out_refid, 'Consignment Adjustment: '|| p_comment, 1280, p_user_id, v_message) ;
    END IF;
    p_out_txn_ids := v_out_refid ;
END gm_ac_sav_cons_adj;
/***************************************************
Description : This procedure is used to link tags
*********************************************************/
PROCEDURE gm_ac_sav_link_tag (
        p_tag_id      IN t5010_tag.c5010_tag_id%TYPE,
        p_setid       IN t207_set_master.c207_set_id%TYPE,
        p_pnum        IN t205_part_number.c205_part_number_id%TYPE,
        p_distid      IN t701_distributor.c701_distributor_id%TYPE,
        p_type        IN NUMBER,
        p_refid       IN VARCHAR2,
        p_screen_type IN VARCHAR2,
        p_user_id     IN t5010_tag.c5010_last_updated_by%TYPE)
AS
    v_cnt NUMBER;
    v_tag_part t208_set_details.c205_part_number_id%TYPE;
    v_cnt_tag_part NUMBER;
    v_cnt_part     NUMBER;
    v_status       NUMBER;
    v_void_fl      CHAR (1) ;
    v_plant_id     t5040_plant_master.C5040_PLANT_ID %TYPE;
    v_company_id   t1900_company.C1900_COMPANY_ID%TYPE;

    --
BEGIN
	
	SELECT get_plantid_frm_cntx()  INTO  v_plant_id   FROM DUAL;                                 
	SELECT get_compid_frm_cntx() INTO  v_company_id FROM DUAL;
		
    IF p_tag_id IS NOT NULL THEN
         SELECT COUNT (1)
           INTO v_cnt
           FROM t5010_tag t5010
          WHERE t5010.c5010_tag_id   = p_tag_id
            AND t5010.c5010_void_fl IS NULL;
        IF v_cnt                     = 0 THEN
            GM_RAISE_APPLICATION_ERROR('-20999','160','');
        END IF;
        IF p_screen_type <> '18642' --  Field Audit Reconcilation
            THEN
             SELECT c901_status INTO v_status FROM t5010_tag WHERE c5010_tag_id = p_tag_id;
            --For WRITE-UP-SET and WRITE-UP-PART tag status should be available or
            -- release.
            IF p_type        = 101200 OR p_type = 101202 THEN
                IF v_status <> 51011 AND v_status <> 51010 THEN
                    GM_RAISE_APPLICATION_ERROR('-20999','161','');
                END IF;
            END IF;
            IF v_status = 51010 THEN -- Available
                 UPDATE t5010_tag t5010
                SET t5010.c901_status = 51011, t5010.c5010_last_updated_by = p_user_id, t5010.c5010_last_updated_date =
                    CURRENT_DATE , t5010.c1900_company_id  = v_company_id , t5010.c5040_plant_id = v_plant_id
                  WHERE t5010.c5010_tag_id   = p_tag_id
                    AND c901_status          = 51010
                    AND t5010.c5010_void_fl IS NULL;
            END IF;
            IF p_setid <> '0' THEN
                BEGIN
                     SELECT t208.c205_part_number_id
                       INTO v_tag_part
                       FROM t208_set_details t208, t205d_part_attribute t205d
                      WHERE t208.c207_set_id          = p_setid
                        AND t208.c205_part_number_id  = t205d.c205_part_number_id
                        AND t208.c208_void_fl        IS NULL
                        AND t205d.c901_attribute_type = 92340
                        AND t205d.c205d_void_fl      IS NULL
                        AND t208.c208_inset_fl        = 'Y'
                        AND c205d_attribute_value     = 'Y';
                EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    v_tag_part := NULL;
                WHEN TOO_MANY_ROWS THEN
                    GM_RAISE_APPLICATION_ERROR('-20999','162','');
                END;
                IF v_tag_part IS NULL THEN
                    GM_RAISE_APPLICATION_ERROR('-20999','163','');
                END IF;
            ELSIF p_pnum IS NOT NULL THEN
                 SELECT COUNT (1)
                   INTO v_cnt_tag_part
                   FROM t205d_part_attribute t205d
                  WHERE t205d.c205_part_number_id = p_pnum
                    AND t205d.c901_attribute_type = 92340
                    AND c205d_attribute_value     = 'Y';
                IF v_cnt_tag_part                 = 0 THEN
                    GM_RAISE_APPLICATION_ERROR('-20999','164','');
                END IF;
                v_tag_part := p_pnum;
            END IF;
        ELSE -- Field Audit Reconcile
            v_tag_part := p_pnum;
        END IF; -- p_screen_type
         UPDATE t5010_tag t5010
        SET t5010.c5010_control_number                                                               = 'NOC#', t5010.c5010_last_updated_trans_id = DECODE (p_type, 101200, p_refid,
            101201, p_refid, 101202, p_refid, 101203, p_refid, NULL), t5010.c5010_location_id        = DECODE (p_type, 101201,
            '52099', 101200, p_distid, 101203, '52099', 101202, p_distid, NULL), t5010.c5010_void_fl = DECODE (p_type,
            101201, 'Y', 101203, 'Y', NULL), t5010.c5010_lock_fl                                     = 'Y',
            t5010.c205_part_number_id                                                                = v_tag_part,
            t5010.c901_trans_type                                                                    = DECODE (p_type,
            101201, 51001, 101200, 51000, 101203, 51001, 101202, 51000, NULL) --Consignment
          , t5010.c207_set_id                               = DECODE(p_setid, '0', NULL, p_setid), t5010.c901_location_type = DECODE (p_type, 101201, 40033, 101200, 4120, 101203
            , 40033, 101202, 4120, NULL), t5010.c901_status = 51012 --Active
          , t5010.c5010_last_updated_by                     = p_user_id, t5010.c5010_last_updated_date = CURRENT_DATE
          , t5010.c1900_company_id  = v_company_id 
          , t5010.c5040_plant_id = v_plant_id
          WHERE t5010.c5010_tag_id                          = p_tag_id
            AND t5010.c5010_void_fl                        IS NULL;
    END IF; -- p_tag_id
END gm_ac_sav_link_tag;
/***************************************************
Description : This procedure is used to adjust set
*********************************************************/
PROCEDURE gm_ac_sav_set_adj (
        p_type        IN t901_code_lookup.c901_code_id%TYPE,
        p_distid      IN t701_distributor.c701_distributor_id%TYPE,
        p_setid       IN t207_set_master.c207_set_id%TYPE,
        p_comment     IN VARCHAR2,
        p_screen_type IN VARCHAR2,
        p_user_id     IN t101_user.c101_user_id%TYPE,
        p_out_refid OUT VARCHAR2)
AS
    v_cnt NUMBER;
    --
BEGIN
    --For WRITE-UP-SET or WRITE-OFF-SET
    IF p_type = 101200 OR p_type = 101201 THEN
         SELECT COUNT (1)
           INTO v_cnt
           FROM t207_set_master
          WHERE c207_set_id   = p_setid
            AND c207_type     = 4070
            AND c207_void_fl IS NULL;
        --Check for a valid consignment set id
        IF v_cnt = 0 AND p_screen_type <> '18642' THEN -- Field Audit Reconcile
           GM_RAISE_APPLICATION_ERROR('-20999','165','');
        END IF;
        IF p_type = 101201 THEN -- WRITE-OFF-SET
            gm_ac_sav_set_writeoff (p_setid, p_distid, p_user_id, p_comment, p_out_refid) ;
        END IF;
        IF p_type = 101200 THEN -- WRITE-UP-SET
            gm_ac_sav_set_writeup (p_setid, p_distid, p_user_id, p_comment, p_out_refid) ;
        END IF;
    END IF;
END gm_ac_sav_set_adj;
/***************************************************
Description : This procedure is used to adjust parts
*********************************************************/
PROCEDURE gm_ac_sav_part_adj (
        p_type        IN t901_code_lookup.c901_code_id%TYPE,
        p_distid      IN t701_distributor.c701_distributor_id%TYPE,
        p_part_inpstr IN VARCHAR2,
        p_comment     IN VARCHAR2,
        p_user_id     IN t101_user.c101_user_id%TYPE,
        p_out_refid OUT VARCHAR2)
AS
    --
BEGIN
    --For WRITE-UP-PART or WRITE-OFF-PART
    IF p_type             = 101202 OR p_type = 101203 THEN
        IF p_part_inpstr IS NULL THEN
            GM_RAISE_APPLICATION_ERROR('-20999','166','');
        END IF;
        IF p_type = 101203 THEN -- WRITE-OFF-PART
            gm_ac_sav_part_writeoff (p_part_inpstr, p_distid, p_user_id, p_comment, p_out_refid) ;
        END IF;
        IF p_type = 101202 THEN -- WRITE-UP-PART
            gm_ac_sav_part_writeup (p_part_inpstr, p_distid, p_user_id, p_comment, p_out_refid) ;
        END IF;
    END IF;
END gm_ac_sav_part_adj;
/***************************************************
Description : This procedure is used validate parts
*********************************************************/
PROCEDURE gm_ac_sav_validate_parts (
        p_inputstr IN VARCHAR2,
        p_out_tagpart OUT t208_set_details.c205_part_number_id%TYPE)
AS
    v_strlen NUMBER           := NVL (LENGTH (p_inputstr), 0) ;
    v_string VARCHAR2 (30000) := p_inputstr;
    v_pnum t208_set_details.c205_part_number_id%TYPE;
    v_qty          NUMBER;
    v_cnt          NUMBER;
    v_substring    VARCHAR2 (1000) ;
    v_invalidparts VARCHAR2 (1000) ;
    v_err_fl       BOOLEAN := FALSE;
    v_tag_part t208_set_details.c205_part_number_id%TYPE;
    v_pnum_str VARCHAR2 (30000) := '';
    v_qty_str  VARCHAR2 (30000) := '';
BEGIN
    IF v_strlen   = 0 THEN
        v_err_fl := TRUE;
    END IF;
    WHILE INSTR (v_string, '|') <> 0
    LOOP
        v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
        v_string    := SUBSTR (v_string, INSTR (v_string, '|')    + 1) ;
        v_pnum      := NULL;
        v_qty       := NULL;
        v_pnum      := SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1) ;
        v_substring := SUBSTR (v_substring, INSTR (v_substring, ',')    + 1) ;
        v_qty       := TO_NUMBER (v_substring) ;
        --Used for inlist
        IF v_qty       IS NOT NULL AND v_pnum IS NOT NULL THEN
            v_pnum_str := v_pnum_str || v_pnum || ',';
            v_qty_str  := v_qty_str || v_qty || ',';
        END IF;
        --
         SELECT COUNT (1)
           INTO v_cnt
           FROM t205_part_number
          WHERE c205_part_number_id = v_pnum;
        --Invalid parts in the input
        IF v_cnt            = 0 THEN
            v_invalidparts := v_invalidparts || ',';
            v_err_fl       := TRUE;
        END IF;
        --
    END LOOP;
    my_context.set_my_inlist_ctx (v_pnum_str) ;
     SELECT COUNT (1)
       INTO v_cnt
       FROM t205d_part_attribute
      WHERE c205_part_number_id IN
        (
             SELECT token FROM v_in_list
        )
        AND c901_attribute_type   = 92340
        AND c205d_attribute_value = 'Y';
    -- more than one taggable parts in the input
    IF v_cnt      > 1 THEN
        v_err_fl := TRUE;
    ELSIF v_cnt   = 1 THEN
         SELECT c205_part_number_id
           INTO v_tag_part
           FROM t205d_part_attribute
          WHERE c205_part_number_id IN
            (
                 SELECT token FROM v_in_list
            )
            AND c901_attribute_type   = 92340
            AND c205d_attribute_value = 'Y';
    END IF;
    IF v_err_fl = TRUE THEN
        GM_RAISE_APPLICATION_ERROR('-20999','167',v_invalidparts);
    END IF;
    p_out_tagpart := v_tag_part;
END gm_ac_sav_validate_parts;
/******************************************************************************
**
* Description : Procedure to write-off single set for a particular distributor
* Author   : Mihir Patel
*******************************************************************************
**/
PROCEDURE gm_ac_sav_set_writeoff (
        p_setid   IN t207_set_master.c207_set_id%TYPE,
        p_distid  IN t2651_field_sales_details.c701_distributor_id%TYPE,
        p_user_id IN t506_returns.c506_created_by%TYPE,
        p_comment IN VARCHAR2,
        p_out_rmaid OUT t506_returns.c506_rma_id%TYPE)
AS
    v_rmaid t506_returns.c506_rma_id%TYPE;
    v_type t506_returns.c506_type%TYPE;
    v_comments VARCHAR2 (4000) := p_comment;
    v_company_id  t1900_company.c1900_company_id%TYPE;
    v_plant_id 	  t5040_plant_master.c5040_plant_id%TYPE;
    -- International posting changes.
    v_costing_id t820_costing.c820_costing_id%TYPE;
    v_owner_company_id t1900_company.c1900_company_id%TYPE;
    v_local_cost t820_costing.c820_purchase_amt%TYPE;
    v_cost t820_costing.c820_purchase_amt%TYPE;
    
    CURSOR consigment_cur
    IS
         SELECT t208.c205_part_number_id part_number_id, NVL (t208.c208_set_qty, 0) item_qty, NVL (get_part_price (
            t208.c205_part_number_id, 'E'), 0) item_price
           FROM t208_set_details t208
          WHERE t208.c208_void_fl IS NULL
            AND t208.c207_set_id   = p_setid
            AND t208.c208_set_qty <> 0
            AND t208.c208_inset_fl = 'Y'
       ORDER BY t208.c205_part_number_id, t208.c208_critical_fl DESC, t208.c208_inset_fl DESC;

   BEGIN
	   
      SELECT get_compid_frm_cntx(), get_plantid_frm_cntx()
        INTO v_company_id, v_plant_id
        FROM DUAL;
        
     SELECT 'GM-RA-' || s506_return.NEXTVAL INTO v_rmaid FROM DUAL;
    p_out_rmaid := v_rmaid;
    --DBMS_OUTPUT.put_line ('RMA_id....' || v_rmaid);
    --3301 Consignment - Sets , 3302 Consignment - Items
     SELECT DECODE ( (TRIM (p_setid)), NULL, 3302, 3301)
       INTO v_type
       FROM DUAL;
    --3313 Consignment Return
     INSERT
       INTO t506_returns
        (
            c506_rma_id, c506_comments, c506_created_date
          , c506_type, c701_distributor_id, c506_reason
          , c506_status_fl, c506_created_by, c506_expected_date
          , c207_set_id, c506_credit_date, c506_return_date
          , c501_reprocess_date, c901_txn_source
          , c1900_company_id, c5040_plant_id
        )
        VALUES
        (
            v_rmaid, v_comments, CURRENT_DATE
          , v_type, p_distid, 3313
          , '2', p_user_id, TRUNC (CURRENT_DATE)
          , p_setid, TRUNC (CURRENT_DATE), TRUNC (CURRENT_DATE)
          , CURRENT_DATE, 102680, v_company_id, v_plant_id
        ) ;
    FOR recs IN consigment_cur
    LOOP
         INSERT
           INTO t507_returns_item
            (
                c507_returns_item_id, c507_item_qty, c507_item_price
              , c205_part_number_id, c507_control_number, c506_rma_id
              , c507_status_fl
            )
            VALUES
            (
                s507_return_item.NEXTVAL, recs.item_qty, recs.item_price
              , recs.part_number_id, 'NOC#', v_rmaid
              , 'C'
            ) ;
        -- to get the costing id (4904 Sales Consignment)
		v_costing_id := get_ac_costing_id (recs.part_number_id, 4904);
		-- to get the owner information.
		BEGIN
			SELECT t820.c1900_owner_company_id
			  , t820.c820_purchase_amt
			  , t820.c820_local_company_cost
			   INTO v_owner_company_id
			  , v_cost
			  , v_local_cost
			   FROM t820_costing t820
			  WHERE t820.c820_costing_id =v_costing_id;
	   EXCEPTION WHEN NO_DATA_FOUND
	   THEN
	   		v_owner_company_id := NULL;
	   		v_cost := NULL;
	   		v_local_cost := NULL;
	   END;
	   
        gm_save_ledger_posting (4841 -- Sales Rep Consignment to Scrap
        , CURRENT_DATE, recs.part_number_id -- part#
        , p_distid -- DistributorID
        , v_rmaid -- RAID
        , recs.item_qty --QTY
        , v_cost 
        , p_user_id
        , NULL
        , v_owner_company_id
        , v_local_cost) ;
    END LOOP;
    -- update the FS warehouse Qty
    -- 4302 - Minus
    -- 102901 - Return
    -- 102908 - Return
	   gm_pkg_op_inv_field_sales_tran.gm_update_fs_inv(p_out_rmaid, 'T506', p_distid, 102901, 4302, 102908, p_user_id);
END gm_ac_sav_set_writeoff;
/******************************************************************************
**
* Description : Procedure to write-off parts for a particular distributor
* Author  : Mihir Patel
*******************************************************************************
**/
PROCEDURE gm_ac_sav_part_writeoff
    (
        p_inputstr IN VARCHAR2,
        p_distid   IN t701_distributor.c701_distributor_id%TYPE,
        p_user_id  IN t506_returns.c506_created_by%TYPE,
        p_comment  IN VARCHAR2,
        p_out_rmaid OUT t506_returns.c506_rma_id%TYPE
    )
AS
    v_rmaid t506_returns.c506_rma_id%TYPE;
    v_price t505_item_consignment.c505_item_price%TYPE;
    v_comments VARCHAR2 (4000)  := p_comment;
    v_strlen   NUMBER           := NVL (LENGTH (p_inputstr), 0) ;
    v_string   VARCHAR2 (30000) := p_inputstr;
    v_pnum t208_set_details.c205_part_number_id%TYPE;
    v_qty       NUMBER;
    v_substring VARCHAR2 (1000) ;
	v_company_id  t1900_company.c1900_company_id%TYPE;
    v_plant_id 	  t5040_plant_master.c5040_plant_id%TYPE;
    -- International posting changes.
    v_costing_id t820_costing.c820_costing_id%TYPE;
    v_owner_company_id t1900_company.c1900_company_id%TYPE;
    v_local_cost t820_costing.c820_purchase_amt%TYPE;
    v_cost t820_costing.c820_purchase_amt%TYPE;
   BEGIN
	   
      SELECT get_compid_frm_cntx(), get_plantid_frm_cntx()
        INTO v_company_id, v_plant_id
        FROM DUAL;
        
     SELECT 'GM-RA-' || s506_return.NEXTVAL INTO v_rmaid FROM DUAL;
    p_out_rmaid := v_rmaid;
    --DBMS_OUTPUT.put_line ('RMA_id....' || v_rmaid);
     INSERT
       INTO t506_returns
        (
            c506_rma_id, c506_comments, c506_created_date
          , c506_type, c701_distributor_id, c506_reason
          , c506_status_fl, c506_created_by, c506_expected_date
          , c207_set_id, c506_credit_date, c506_return_date
          , c501_reprocess_date, c901_txn_source
          , c1900_company_id, c5040_plant_id
        )
        VALUES
        (
            v_rmaid, v_comments, CURRENT_DATE
          , 3302, p_distid, 3313
          , '2', p_user_id, TRUNC (CURRENT_DATE)
          , '', TRUNC (CURRENT_DATE), TRUNC (CURRENT_DATE)
          , CURRENT_DATE, 102680
          , v_company_id, v_plant_id
        ) ;
    WHILE INSTR (v_string, '|') <> 0
    LOOP
        v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
        v_string    := SUBSTR (v_string, INSTR (v_string, '|')    + 1) ;
        v_pnum      := NULL;
        v_qty       := NULL;
        v_pnum      := SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1) ;
        v_substring := SUBSTR (v_substring, INSTR (v_substring, ',')    + 1) ;
        v_qty       := TO_NUMBER (v_substring) ;
         SELECT get_part_price (v_pnum, 'E') INTO v_price FROM DUAL;
         INSERT
           INTO t507_returns_item
            (
                c507_returns_item_id, c507_item_qty, c507_item_price
              , c205_part_number_id, c507_control_number, c506_rma_id
              , c507_status_fl
            )
            VALUES
            (
                s507_return_item.NEXTVAL, v_qty, v_price
              , v_pnum, 'NOC#', v_rmaid
              , 'C'
            ) ;
        -- to get the costing id (4904 Sales Consignment)
		v_costing_id := get_ac_costing_id (v_pnum, 4904);
		-- to get the owner information.
		BEGIN
			SELECT t820.c1900_owner_company_id
			  , t820.c820_purchase_amt
			  , t820.c820_local_company_cost
			   INTO v_owner_company_id
			  , v_cost
			  , v_local_cost
			   FROM t820_costing t820
			  WHERE t820.c820_costing_id =v_costing_id;
	   EXCEPTION WHEN NO_DATA_FOUND
	   THEN
	   		v_owner_company_id := NULL;
	   		v_cost := NULL;
	   		v_local_cost := NULL;
	   END;
	   
        gm_save_ledger_posting (4841 -- Suchi
        , CURRENT_DATE, v_pnum -- part#
        , p_distid -- DistributorID
        , v_rmaid -- RAID
        , v_qty --QTY
        , v_cost --
        , p_user_id
        , NULL
        , v_owner_company_id
        , v_local_cost) ;
    END LOOP;
    -- update the FS warehouse Qty
    -- 102901 - Return
	-- 102908 - Return
	   gm_pkg_op_inv_field_sales_tran.gm_update_fs_inv(p_out_rmaid, 'T506', p_distid, 102901, 4302, 102908, p_user_id);
END gm_ac_sav_part_writeoff;
/******************************************************************************
* Description : Procedure to write-up set for a particular distributor
* Author  : Mihir Patel
******************************************************************************
*/
PROCEDURE gm_ac_sav_set_writeup
    (
        p_setid   IN t207_set_master.c207_set_id%TYPE,
        p_distid  IN t701_distributor.c701_distributor_id%TYPE,
        p_user_id IN t504_consignment.c504_created_by%TYPE,
        p_comment IN VARCHAR2,
        p_out_conid OUT t504_consignment.c504_consignment_id%TYPE
    )
AS
    v_request_id t520_request.c520_request_id%TYPE;
    v_conid t504_consignment.c504_consignment_id%TYPE;
    v_item_consigment_id t505_item_consignment.c505_item_consignment_id%TYPE;
    v_shipid t907_shipping_info.c907_shipping_id%TYPE;
    v_comments VARCHAR2 (4000) := p_comment;
	v_company_id  t1900_company.c1900_company_id%TYPE;
    v_plant_id 	  t5040_plant_master.c5040_plant_id%TYPE;
    -- International posting changes.
    v_costing_id t820_costing.c820_costing_id%TYPE;
    v_owner_company_id t1900_company.c1900_company_id%TYPE;
    v_local_cost t820_costing.c820_purchase_amt%TYPE;
    v_cost t820_costing.c820_purchase_amt%TYPE;
    CURSOR c_get_setdtl
    IS
         SELECT t208.c207_set_id ID, t208.c205_part_number_id pnum, NVL (t208.c208_set_qty, 0) qty
          , t208.c208_set_details_id mid, get_partnum_desc (t208.c205_part_number_id) pdesc, t208.c208_inset_fl insetfl
          , NVL (get_part_price (t208.c205_part_number_id, 'C'), 0) lprice
           FROM t208_set_details t208
          WHERE t208.c208_void_fl IS NULL
            AND t208.c207_set_id   = p_setid
            AND t208.c208_set_qty <> 0
            AND t208.c208_inset_fl = 'Y'
       ORDER BY t208.c205_part_number_id, t208.c208_critical_fl DESC, t208.c208_inset_fl DESC;
BEGIN
	    SELECT NVL(GET_COMPID_FRM_CNTX(),GET_RULE_VALUE('DEFAULT_COMPANY','ONEPORTAL')), NVL(GET_PLANTID_FRM_CNTX(),GET_RULE_VALUE('DEFAULT_PLANT','ONEPORTAL'))
	      INTO v_company_id, v_plant_id
	      FROM DUAL;
     SELECT 'GM-RQ-' || s520_request.NEXTVAL INTO v_request_id FROM DUAL;
     INSERT
       INTO t520_request
        (
            c520_created_date, c901_request_source, c520_required_date
          , c520_request_to, c520_status_fl, c901_ship_to
          , c520_request_id, c520_created_by, c520_request_for
          , c207_set_id, c520_ship_to_id, c520_request_date
          , c901_request_by_type, c1900_company_id, c5040_plant_id
        )
        VALUES
        (
            CURRENT_DATE, 50618, TRUNC (CURRENT_DATE)
          , p_distid, 40, 4120
          , v_request_id, p_user_id, 40021
          , p_setid, p_distid, TRUNC (CURRENT_DATE)
          , 50626, v_company_id, v_plant_id
        ) ;
     SELECT get_next_consign_id ('consignment') INTO v_conid FROM DUAL;
    p_out_conid := v_conid;
     INSERT
       INTO t504_consignment
        (
            c504_verified_date, c504_delivery_carrier, c520_request_id
          , c504_consignment_id, c504_delivery_mode, c504_status_fl
          , c504_type, c504_ship_to, c504_created_by
          , c701_distributor_id, c504_ship_to_id, c504_update_inv_fl
          , c504_ship_req_fl, c207_set_id, c504_ship_date
          , c504_verify_fl, c504_created_date, c504_comments
          , c1900_company_id, c5040_plant_id
        )
        VALUES
        (
            TRUNC (CURRENT_DATE), 5040, v_request_id
          , v_conid, 5031, 4
          , 4110, 4120, p_user_id
          , p_distid, p_distid, '1'
          , '1', p_setid, TRUNC (CURRENT_DATE)
          , '1', CURRENT_DATE, v_comments
          , v_company_id, v_plant_id
        ) ;
     SELECT s907_ship_id.NEXTVAL INTO v_shipid FROM DUAL;
     INSERT
       INTO t907_shipping_info
        (
            c907_shipping_id, c907_ref_id, c901_ship_to
          , c907_ship_to_id, c907_ship_to_dt, c901_source
          , c901_delivery_mode, c901_delivery_carrier, c907_status_fl
          , c907_created_by, c907_created_date, c907_active_fl
          , c907_release_dt, c907_shipped_dt, c1900_company_id
          , c5040_plant_id
        )
        VALUES
        (
            v_shipid, v_conid, 4120
          , p_distid, TRUNC (CURRENT_DATE), 50181
          , 5031, 5040, 40
          , p_user_id, CURRENT_DATE, 'N'
          , TRUNC (CURRENT_DATE), TRUNC (CURRENT_DATE), v_company_id
          , v_plant_id
        ) ;
    FOR rec IN c_get_setdtl
    LOOP
         SELECT s504_consign_item.NEXTVAL INTO v_item_consigment_id FROM DUAL;
         INSERT
           INTO t505_item_consignment
            (
                c505_item_price, c504_consignment_id, c505_item_consignment_id
              , c505_item_qty, c205_part_number_id, c505_control_number
              , c901_type
            )
            VALUES
            (
                rec.lprice, v_conid, v_item_consigment_id
              , rec.qty, rec.pnum, 'NOC#'
              , 100880
            ) ;
        -- to get the costing id (4904 Sales Consignment)
		v_costing_id :=  NVL (get_ac_costing_id (rec.pnum, 4904), get_ac_costing_id (rec.pnum, 4900));
		-- to get the owner information.
		BEGIN
			SELECT t820.c1900_owner_company_id
			  , t820.c820_purchase_amt
			  , t820.c820_local_company_cost
			   INTO v_owner_company_id
			  , v_cost
			  , v_local_cost
			   FROM t820_costing t820
			  WHERE t820.c820_costing_id =v_costing_id;
	   EXCEPTION WHEN NO_DATA_FOUND
	   THEN
	   		v_owner_company_id := NULL;
	   		v_cost := NULL;
	   		v_local_cost := NULL;
	   END;    
        --Posting
        gm_save_ledger_posting (4839, CURRENT_DATE, rec.pnum, p_distid, v_conid, rec.qty, v_cost, p_user_id, NULL, v_owner_company_id, v_local_cost) ;
    END LOOP;
    -- update the FS warehouse Qty
    -- 102900 - Consignment
	-- 102907 - Consignment
	   gm_pkg_op_inv_field_sales_tran.gm_update_fs_inv(v_conid, 'T505', p_distid, 102900, 4301, 102907, p_user_id);
END gm_ac_sav_set_writeup;
/******************************************************************************
* Description : Procedure to write-up part for a particular distributor
* Author  : Mihir Patel
******************************************************************************
*/
PROCEDURE gm_ac_sav_part_writeup
    (
        p_inputstr IN VARCHAR2,
        p_distid   IN t701_distributor.c701_distributor_id%TYPE,
        p_user_id  IN t504_consignment.c504_created_by%TYPE,
        p_comment  IN VARCHAR2,
        p_out_conid OUT t504_consignment.c504_consignment_id%TYPE
    )
AS
    v_request_id t520_request.c520_request_id%TYPE;
    v_conid t504_consignment.c504_consignment_id%TYPE;
    v_item_consigment_id t505_item_consignment.c505_item_consignment_id%TYPE;
    v_shipid t907_shipping_info.c907_shipping_id%TYPE;
    v_price t505_item_consignment.c505_item_price%TYPE;
    v_comments VARCHAR2 (4000)  := p_comment;
    v_strlen   NUMBER           := NVL (LENGTH (p_inputstr), 0) ;
    v_string   VARCHAR2 (30000) := p_inputstr;
    v_pnum t208_set_details.c205_part_number_id%TYPE;
    v_qty       NUMBER;
    v_substring VARCHAR2 (1000) ;
	v_company_id  t1900_company.c1900_company_id%TYPE;
    v_plant_id 	  t5040_plant_master.c5040_plant_id%TYPE;	
     -- International posting changes.
    v_costing_id t820_costing.c820_costing_id%TYPE;
    v_owner_company_id t1900_company.c1900_company_id%TYPE;
    v_local_cost t820_costing.c820_purchase_amt%TYPE;
    v_cost t820_costing.c820_purchase_amt%TYPE;
BEGIN
	    SELECT NVL(GET_COMPID_FRM_CNTX(),GET_RULE_VALUE('DEFAULT_COMPANY','ONEPORTAL')), NVL(GET_PLANTID_FRM_CNTX(),GET_RULE_VALUE('DEFAULT_PLANT','ONEPORTAL'))
	      INTO v_company_id, v_plant_id
	      FROM DUAL;
     SELECT 'GM-RQ-' || s520_request.NEXTVAL INTO v_request_id FROM DUAL;
     INSERT
       INTO t520_request
        (
            c520_created_date, c901_request_source, c520_required_date
          , c520_request_to, c520_status_fl, c901_ship_to
          , c520_request_id, c520_created_by, c520_request_for
          , c207_set_id, c520_ship_to_id, c520_request_date
          , c901_request_by_type, c1900_company_id, c5040_plant_id
        )
        VALUES
        (
            CURRENT_DATE, 50618, TRUNC (CURRENT_DATE)
          , p_distid, 40, 4120
          , v_request_id, p_user_id, 40021
          , '', p_distid, TRUNC (CURRENT_DATE)
          , 50626, v_company_id, v_plant_id
        ) ;
     SELECT get_next_consign_id ('consignment') INTO v_conid FROM DUAL;
    p_out_conid := v_conid;
    --DBMS_OUTPUT.put_line ('CN #....' || v_consignment_id);
     INSERT
       INTO t504_consignment
        (
            c504_verified_date, c504_delivery_carrier, c520_request_id
          , c504_consignment_id, c504_delivery_mode, c504_status_fl
          , c504_type, c504_ship_to, c504_created_by
          , c701_distributor_id, c504_ship_to_id, c504_update_inv_fl
          , c504_ship_req_fl, c207_set_id, c504_ship_date
          , c504_verify_fl, c504_created_date, c504_comments
          , c1900_company_id, c5040_plant_id
        )
        VALUES
        (
            TRUNC (CURRENT_DATE), 5040, v_request_id
          , v_conid, 5031, 4
          , 4110, 4120, p_user_id
          , p_distid, p_distid, '1'
          , '1', '', TRUNC (CURRENT_DATE)
          , '1', CURRENT_DATE, v_comments
          , v_company_id, v_plant_id
        ) ;
     SELECT s907_ship_id.NEXTVAL INTO v_shipid FROM DUAL;
     INSERT
       INTO t907_shipping_info
        (
            c907_shipping_id, c907_ref_id, c901_ship_to
          , c907_ship_to_id, c907_ship_to_dt, c901_source
          , c901_delivery_mode, c901_delivery_carrier, c907_status_fl
          , c907_created_by, c907_created_date, c907_active_fl
          , c907_release_dt, c907_shipped_dt, c1900_company_id
          , c5040_plant_id
        )
        VALUES
        (
            v_shipid, v_conid, 4120
          , p_distid, TRUNC (CURRENT_DATE), 50181
          , 5031, 5040, 40
          , p_user_id, CURRENT_DATE, 'N'
          , TRUNC (CURRENT_DATE), TRUNC (CURRENT_DATE), v_company_id
          , v_plant_id
        ) ;
    WHILE INSTR (v_string, '|') <> 0
    LOOP
        v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
        v_string    := SUBSTR (v_string, INSTR (v_string, '|')    + 1) ;
        v_pnum      := NULL;
        v_qty       := NULL;
        v_pnum      := SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1) ;
        v_substring := SUBSTR (v_substring, INSTR (v_substring, ',')    + 1) ;
        v_qty       := TO_NUMBER (v_substring) ;
         SELECT s504_consign_item.NEXTVAL INTO v_item_consigment_id FROM DUAL;
         SELECT get_part_price (v_pnum, 'C') INTO v_price FROM DUAL;
         INSERT
           INTO t505_item_consignment
            (
                c505_item_price, c504_consignment_id, c505_item_consignment_id
              , c505_item_qty, c205_part_number_id, c505_control_number
              , c901_type
            )
            VALUES
            (
                v_price, v_conid, v_item_consigment_id
              , v_qty, v_pnum, 'NOC#'
              , 100880
            ) ;
         -- to get the costing id (4904 Sales Consignment)
		v_costing_id := NVL (get_ac_costing_id (v_pnum, 4904), get_ac_costing_id (v_pnum, 4900));
		-- to get the owner information.
		BEGIN
			SELECT t820.c1900_owner_company_id
			  , t820.c820_purchase_amt
			  , t820.c820_local_company_cost
			   INTO v_owner_company_id
			  , v_cost
			  , v_local_cost
			   FROM t820_costing t820
			  WHERE t820.c820_costing_id =v_costing_id;
	   EXCEPTION WHEN NO_DATA_FOUND
	   THEN
	   		v_owner_company_id := NULL;
	   		v_cost := NULL;
	   		v_local_cost := NULL;
	   END;   
        --Posting
        gm_save_ledger_posting (4839, CURRENT_DATE, v_pnum, p_distid, v_conid, v_qty, v_cost, p_user_id, NULL, v_owner_company_id, v_local_cost) ;
    END LOOP;
    -- update the FS warehouse Qty
    -- 102900 - Consignment
	-- 102907 - Consignment
	   gm_pkg_op_inv_field_sales_tran.gm_update_fs_inv(v_conid, 'T505', p_distid, 102900, 4301, 102907, p_user_id);
END gm_ac_sav_part_writeup;
/******************************************************************************
* Description : Procedure to WRITE-OFF-INACTIVE-DISTRIBUTOR
* Author  : Mihir Patel
******************************************************************************
*/
PROCEDURE gm_sav_inactive_dist_writeoff
    (
        p_distid t701_distributor.c701_distributor_id%TYPE,
        p_user_id IN t506_returns.c506_created_by%TYPE,
        p_comment IN VARCHAR2,
        p_out_rmaid OUT t506_returns.c506_rma_id%TYPE,
        p_out_conid OUT t504_consignment.c504_consignment_id%TYPE
    )
AS
    CURSOR c_get_writeoff
    IS
         SELECT pending_qty qty, price, pnum
          , 'NOC#', 'W'
           FROM
            (
                 SELECT t205.c205_part_number_id pnum, t5053.c5053_curr_qty pending_qty, NVL (
                    get_part_price (t205.c205_part_number_id, 'E'), 0) price, t205.c205_product_family
                   FROM t205_part_number t205, t5051_inv_warehouse t5051, t5052_location_master t5052, t5053_location_part_mapping t5053
                   WHERE t5051.c5051_inv_warehouse_id = t5052.c5051_inv_warehouse_id
			             AND t205.c205_part_number_id = t5053.c205_part_number_id 
			             AND t5052.c5052_location_id      = t5053.c5052_location_id
			             AND t5051.c5051_inv_warehouse_id = 3 -- FS Warehouse
			             AND t5052.c5052_ref_id           = p_distid
			             AND t5052.c901_location_type     = 4000338 --FS Location
			             AND t5052.c5052_void_fl         IS NULL
            )
          WHERE pending_qty > 0 --Create return
       ORDER BY c205_product_family, pnum;
        CURSOR c_get_writeup
        IS
             SELECT pending_qty * - 1 qty, price, pnum
              , 'NOC#', 'W'
               FROM
                (
                     SELECT t205.c205_part_number_id pnum, t5053.c5053_curr_qty pending_qty, NVL (
                        get_part_price (t205.c205_part_number_id, 'C'), 0) price, t205.c205_product_family
                       FROM t205_part_number t205, t5051_inv_warehouse t5051, t5052_location_master t5052, t5053_location_part_mapping t5053
                       WHERE t5051.c5051_inv_warehouse_id = t5052.c5051_inv_warehouse_id
			                AND t205.c205_part_number_id = t5053.c205_part_number_id 
			                AND t5052.c5052_location_id      = t5053.c5052_location_id
			                AND t5051.c5051_inv_warehouse_id = 3 -- FS Warehouse
			                AND t5052.c5052_ref_id           = p_distid
			                AND t5052.c901_location_type     = 4000338 --FS Location
			                AND t5052.c5052_void_fl         IS NULL
                )
              WHERE pending_qty < 0 --Create CN
           ORDER BY c205_product_family, pnum;
            v_rma_id  VARCHAR2 (30) ;
            v_cnt_rtn NUMBER;
            v_cnt_cn  NUMBER;
            --
            v_request_id t520_request.c520_request_id%TYPE; --
            v_conid t504_consignment.c504_consignment_id%TYPE; --
            v_item_consigment_id t505_item_consignment.c505_item_consignment_id%TYPE;
            v_shipid t907_shipping_info.c907_shipping_id%TYPE;
            v_price t505_item_consignment.c505_item_price%TYPE;
            v_comments VARCHAR2 (4000) := p_comment;
			v_company_id  t1900_company.c1900_company_id%TYPE;
		    v_plant_id 	  t5040_plant_master.c5040_plant_id%TYPE;	
		     -- International posting changes.
		    v_costing_id t820_costing.c820_costing_id%TYPE;
		    v_owner_company_id t1900_company.c1900_company_id%TYPE;
		    v_local_cost t820_costing.c820_purchase_amt%TYPE;
		    v_cost t820_costing.c820_purchase_amt%TYPE;
        BEGIN
	        SELECT NVL(GET_COMPID_FRM_CNTX(),GET_RULE_VALUE('DEFAULT_COMPANY','ONEPORTAL')), NVL(GET_PLANTID_FRM_CNTX(),GET_RULE_VALUE('DEFAULT_PLANT','ONEPORTAL'))
	          INTO v_company_id, v_plant_id
	          FROM DUAL;
	          
             SELECT COUNT (1)
               INTO v_cnt_rtn
               FROM
                (
                     SELECT t205.c205_part_number_id pnum, t5053.c5053_curr_qty pending_qty, NVL (
                        get_part_price (t205.c205_part_number_id, 'E'), 0) price, t205.c205_product_family
                       FROM t205_part_number t205, t5051_inv_warehouse t5051, t5052_location_master t5052, t5053_location_part_mapping t5053
           				WHERE t5051.c5051_inv_warehouse_id = t5052.c5051_inv_warehouse_id
			                AND t205.c205_part_number_id = t5053.c205_part_number_id 
			                AND t5052.c5052_location_id      = t5053.c5052_location_id
			                AND t5051.c5051_inv_warehouse_id = 3 -- FS Warehouse
			                AND t5052.c5052_ref_id           = p_distid
			                AND t5052.c901_location_type     = 4000338 --FS Location
			                AND t5052.c5052_void_fl         IS NULL
                )
              WHERE pending_qty > 0; -- Create RA
            IF v_cnt_rtn        > 0 THEN
                 SELECT 'GM-RA-' || s506_return.NEXTVAL INTO v_rma_id FROM DUAL;
                p_out_rmaid := v_rma_id;
                 INSERT
                   INTO t506_returns
                    (
                        c506_rma_id, c506_comments, c506_created_date
                      , c506_type, c701_distributor_id, c506_reason
                      , c506_status_fl, c506_created_by, c506_expected_date
                      , c506_credit_date, c506_return_date, c506_update_inv_fl
                      , c501_reprocess_date, c901_txn_source, c1900_company_id, c5040_plant_id
                    )
                    VALUES
                    (
                        v_rma_id, v_comments, CURRENT_DATE
                      , 3306, p_distid, 3318
                      , 2, p_user_id, TRUNC (CURRENT_DATE)
                      , TRUNC (CURRENT_DATE), TRUNC (CURRENT_DATE), 1
                      , TRUNC (CURRENT_DATE), 102680, v_company_id, v_plant_id
                    ) ;
                FOR rec IN c_get_writeoff
                LOOP
                     INSERT
                       INTO t507_returns_item
                        (
                            c507_returns_item_id, c507_item_qty, c507_item_price
                          , c205_part_number_id, c507_control_number, c506_rma_id
                          , c507_status_fl
                        )
                        VALUES
                        (
                            s507_return_item.NEXTVAL, rec.qty, rec.price
                          , rec.pnum, 'NOC#', v_rma_id
                          , 'W'
                        ) ;
                    -- to get the costing id (4904 Sales Consignment)
							v_costing_id := get_ac_costing_id (rec.pnum, 4904);
							-- to get the owner information.
							BEGIN
								SELECT t820.c1900_owner_company_id
								  , t820.c820_purchase_amt
								  , t820.c820_local_company_cost
								   INTO v_owner_company_id
								  , v_cost
								  , v_local_cost
								   FROM t820_costing t820
								  WHERE t820.c820_costing_id =v_costing_id;
						   EXCEPTION WHEN NO_DATA_FOUND
						   THEN
						   		v_owner_company_id := NULL;
						   		v_cost:= NULL;
						   		v_local_cost:= NULL;
						   END;
					--	   
                    gm_save_ledger_posting (4841 -- Sales Rep Consignment to Inv
                    -- Adjustment
                    , CURRENT_DATE, rec.pnum -- part#
                    , p_distid -- DistributorID
                    , v_rma_id -- RA ID
                    , rec.qty --QTY
                    , v_cost
                    , p_user_id
                    , NULL
                    , v_owner_company_id
                    , v_local_cost) ;
                END LOOP;
                -- update the FS warehouse Qty
                -- 102901 - Return
				-- 102908 - Return
	   		    gm_pkg_op_inv_field_sales_tran.gm_update_fs_inv(v_rma_id, 'T506', p_distid, 102901, 4302, 102908, p_user_id);

                --Create Excess Return (There is no need to create excess return b'coz
                -- excess might have already been
                -- created during Distributor closure)
                --gm_ac_sav_excess_returns (p_distid);
                -- Void Dummy Consignment
                 UPDATE t504_consignment
                SET c504_void_fl             = 'Y', c504_last_updated_by = p_user_id, c504_last_updated_date = CURRENT_DATE
                  WHERE c504_consignment_id IN
                    (
                         SELECT c504_consignment_id
                           FROM t504_consignment
                          WHERE c701_distributor_id = p_distid
                            AND c504_type           = 4129
                    ) ;
                --gm_pkg_sm_lad_consignment.gm_sm_lad_consignment;  --Need to remove
                -- when in stage/Production..(Mihir:
                -- Was Commented in the original temp Proc)
            END IF;
             SELECT COUNT (1)
               INTO v_cnt_cn
               FROM
                (
                     SELECT t205.c205_part_number_id pnum, t5053.c5053_curr_qty pending_qty, NVL (
                        get_part_price (t205.c205_part_number_id, 'C'), 0) price, t205.c205_product_family
                       FROM t205_part_number t205, t5051_inv_warehouse t5051, t5052_location_master t5052, t5053_location_part_mapping t5053
           				WHERE t5051.c5051_inv_warehouse_id = t5052.c5051_inv_warehouse_id
			                AND t205.c205_part_number_id = t5053.c205_part_number_id 
			                AND t5052.c5052_location_id      = t5053.c5052_location_id
			                AND t5051.c5051_inv_warehouse_id = 3 -- FS Warehouse
			                AND t5052.c5052_ref_id           = p_distid
			                AND t5052.c901_location_type     = 4000338 --FS Location
			                AND t5052.c5052_void_fl         IS NULL
                )
              WHERE pending_qty < 0; --Create CN
            IF v_cnt_cn         > 0 THEN
                 SELECT 'GM-RQ-' || s520_request.NEXTVAL INTO v_request_id FROM DUAL;
                 INSERT
                   INTO t520_request
                    (
                        c520_created_date, c901_request_source, c520_required_date
                      , c520_request_to, c520_status_fl, c901_ship_to
                      , c520_request_id, c520_created_by, c520_request_for
                      , c207_set_id, c520_ship_to_id, c520_request_date
                      , c901_request_by_type, c1900_company_id, c5040_plant_id
                    )
                    VALUES
                    (
                        CURRENT_DATE, 50618, TRUNC (CURRENT_DATE)
                      , p_distid, 40, 4120
                      , v_request_id, p_user_id, 40021
                      , '', p_distid, TRUNC (CURRENT_DATE)
                      , 50626, v_company_id, v_plant_id
                    ) ;
                 SELECT get_next_consign_id ('consignment') INTO v_conid FROM DUAL;
                p_out_conid := v_conid;
                --DBMS_OUTPUT.put_line ('CN #....' || v_consignment_id);
                 INSERT
                   INTO t504_consignment
                    (
                        c504_verified_date, c504_delivery_carrier, c520_request_id
                      , c504_consignment_id, c504_delivery_mode, c504_status_fl
                      , c504_type, c504_ship_to, c504_created_by
                      , c701_distributor_id, c504_ship_to_id, c504_update_inv_fl
                      , c504_ship_req_fl, c207_set_id, c504_ship_date
                      , c504_verify_fl, c504_created_date, c504_comments
                      , c1900_company_id, c5040_plant_id
                    )
                    VALUES
                    (
                        TRUNC (CURRENT_DATE), 5040, v_request_id
                      , v_conid, 5031, 4
                      , 4110, 4120, p_user_id
                      , p_distid, p_distid, '1'
                      , '1', '', TRUNC (CURRENT_DATE)
                      , '1', CURRENT_DATE, v_comments
                      , v_company_id, v_plant_id
                    ) ;
                 SELECT s907_ship_id.NEXTVAL INTO v_shipid FROM DUAL;
                 INSERT
                   INTO t907_shipping_info
                    (
                        c907_shipping_id, c907_ref_id, c901_ship_to
                      , c907_ship_to_id, c907_ship_to_dt, c901_source
                      , c901_delivery_mode, c901_delivery_carrier, c907_status_fl
                      , c907_created_by, c907_created_date, c907_active_fl
                      , c907_release_dt, c907_shipped_dt, c1900_company_id
                      , c5040_plant_id
                    )
                    VALUES
                    (
                        v_shipid, v_conid, 4120
                      , p_distid, TRUNC (CURRENT_DATE), 50181
                      , 5031, 5040, 40
                      , p_user_id, CURRENT_DATE, 'N'
                      , TRUNC (CURRENT_DATE), TRUNC (CURRENT_DATE), v_company_id
                      , v_plant_id
                    ) ;
                FOR rec IN c_get_writeup
                LOOP
                     SELECT s504_consign_item.NEXTVAL INTO v_item_consigment_id FROM DUAL;
                     INSERT
                       INTO t505_item_consignment
                        (
                            c505_item_price, c504_consignment_id, c505_item_consignment_id
                          , c505_item_qty, c205_part_number_id, c505_control_number
                          , c901_type
                        )
                        VALUES
                        (
                            rec.price, v_conid, v_item_consigment_id
                          , rec.qty, rec.pnum, 'NOC#'
                          , 100880
                        ) ;
                    -- to get the costing id (4904 Sales Consignment)
							v_costing_id := NVL (get_ac_costing_id (rec.pnum, 4904), get_ac_costing_id (rec.pnum, 4900));
							-- to get the owner information.
							BEGIN
								SELECT t820.c1900_owner_company_id
								  , t820.c820_purchase_amt
								  , t820.c820_local_company_cost
								   INTO v_owner_company_id
								  , v_cost
								  , v_local_cost
								   FROM t820_costing t820
								  WHERE t820.c820_costing_id =v_costing_id;
						   EXCEPTION WHEN NO_DATA_FOUND
						   THEN
						   		v_owner_company_id := NULL;
						   		v_cost:= NULL;
						   		v_local_cost:= NULL;
						   END;    
                    --Posting
                    gm_save_ledger_posting (4839 --Inv Adjustment To Sales Rep
                    -- Consignment
                    , CURRENT_DATE, rec.pnum, p_distid, v_conid, rec.qty, v_cost, p_user_id
                    , NULL
                    , v_owner_company_id
                    , v_local_cost) ;
                END LOOP;
                -- update the FS warehouse Qty
                -- 102900 - Consignment
				-- 102907 - Consignment
	   				gm_pkg_op_inv_field_sales_tran.gm_update_fs_inv(v_conid, 'T505', p_distid, 102900, 4301, 102907, p_user_id);
            END IF;
        END gm_sav_inactive_dist_writeoff;
        /**************************************************************************
        * Description : Procedure to update the void flag in tag table
        * Author  :
        ***************************************************************************/
    PROCEDURE gm_update_tag_recon
        (
            p_tag_id  IN t5010_tag.c5010_tag_id%TYPE,
            p_void_fl IN t5010_tag.c5010_void_fl%TYPE,
            p_user_id IN t5010_tag.c5010_last_updated_by%TYPE
        )
    AS
        v_cnt NUMBER;
    BEGIN
         SELECT COUNT (1)
           INTO v_cnt
           FROM t5010_tag t5010
          WHERE t5010.c5010_tag_id = p_tag_id;
        IF v_cnt                   = 0 THEN
            raise_application_error ('-20107', '') ;
        END IF;
         UPDATE t5010_tag
        SET c5010_void_fl    = p_void_fl, c5010_last_updated_by = p_user_id, c5010_last_updated_date = CURRENT_DATE
          WHERE c5010_tag_id = p_tag_id;
    END gm_update_tag_recon;
    /**************************************************************************
    * Description : Procedure to Reconciled the Missing Tag
    * Author  :
    ***************************************************************************/
PROCEDURE gm_sav_missing_tag_recon (
        p_audit_entry_id IN t2656_audit_entry.c2656_audit_entry_id%TYPE,
        p_user_id        IN t101_user.c101_user_id%TYPE,
        p_out_txnid OUT VARCHAR2)
AS
    v_dist t2651_field_sales_details.c701_distributor_id%TYPE;
    v_ref_type t901_code_lookup.c901_code_id%TYPE;
    v_refid VARCHAR2 (40) ;
    v_part t2656_audit_entry.c205_part_number_id%TYPE;
    v_setid t2656_audit_entry.c207_set_id%TYPE;
    v_cnum t2656_audit_entry.c2656_control_number%TYPE;
    v_qty t2656_audit_entry.c2656_qty%TYPE;
    v_input_str VARCHAR2 (2000) ;
    v_ra_type   NUMBER;
    v_tag_id    VARCHAR2 (40) ;
    v_retag_id  VARCHAR2 (40) ;
BEGIN
     SELECT t2656.c901_ref_type, t2656.c2656_ref_id, t2656.c205_part_number_id
      , t2656.c207_set_id, t2651.c701_distributor_id, t2656.c2656_control_number
      , t2656.c2656_qty, c5010_tag_id
       INTO v_ref_type, v_refid, v_part
      , v_setid, v_dist, v_cnum
      , v_qty, v_tag_id
       FROM t2656_audit_entry t2656, t2651_field_sales_details t2651
      WHERE t2651.c2651_field_sales_details_id = t2656.c2651_field_sales_details_id
        AND t2656.c2656_audit_entry_id         = p_audit_entry_id
        AND t2656.c2656_void_fl               IS NULL
        AND t2651.c2651_void_fl               IS NULL;
    -- to select the RA type.
     SELECT c506_type
       INTO v_ra_type
       FROM t506_returns
      WHERE c506_rma_id = v_refid;
    -- retag id check
    v_retag_id    := get_retag_id (p_audit_entry_id) ;
    IF v_retag_id IS NOT NULL THEN
        v_tag_id  := v_retag_id;
    END IF;
    -- Input string : RA id, type, tag, excess id, part #, Control #, reason, Set Id, Dist id, Qty
    v_input_str := v_refid ||'^' || v_ra_type ||'^' ||v_tag_id ||'^'||NULL ||'^'||v_part ||'^'||v_cnum ||'^' ||NULL ||
    '^'||v_setid ||'^'||v_dist ||'^'||v_qty||'^|';
    -- 54000 (Untagged Sets)
    gm_pkg_op_return.gm_tag_reconciliation (v_input_str, p_user_id, p_out_txnid) ;
    p_out_txnid := v_refid;
END gm_sav_missing_tag_recon;
END gm_pkg_ac_fa_reconciliation;
/