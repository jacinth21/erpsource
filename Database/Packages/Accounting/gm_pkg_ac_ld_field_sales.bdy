/* Formatted on 2009/09/22 12:38 (Formatter Plus v4.8.0) */
--@"C:\Database\Packages\Accounting\gm_pkg_ac_ld_field_sales.bdy";
--exec gm_pkg_ac_ld_field_sales.gm_ac_ld_main(29, 1, 303149);

CREATE OR REPLACE PACKAGE BODY gm_pkg_ac_ld_field_sales
IS
--
	
--
	/****************************************************************
	 * Main procedure which will call other procedures to load all the
	 * Field Sales Details
	 ***************************************************************/
--
	PROCEDURE gm_ac_ld_main (
		p_dist_id	 IN   t701_distributor.c701_distributor_id%TYPE
	  , p_audit_id	 IN   t2650_field_sales_audit.c2650_field_sales_audit_id%TYPE
	  , p_user_id	 IN   t2656_audit_entry.c2656_created_by%TYPE
	)
	AS
		v_lock_id	   t2652_field_sales_lock.c2652_field_sales_lock_id%TYPE;
		v_start_dt	   DATE;
		v_audit_count  NUMBER;
	BEGIN
		v_start_dt	:= SYSDATE;

		SELECT COUNT (1)
		  INTO v_audit_count
		  FROM t2651_field_sales_details t2651
		 WHERE t2651.c701_distributor_id = p_dist_id AND t2651.c2650_field_sales_audit_id = p_audit_id;

		IF v_audit_count > 0
		THEN
			GM_RAISE_APPLICATION_ERROR('-20999','118',p_dist_id);
		END IF;

		SELECT s2652_field_sales_lock.NEXTVAL
		  INTO v_lock_id
		  FROM DUAL;

		INSERT INTO t2652_field_sales_lock
					(c2652_field_sales_lock_id, c2650_field_sales_audit_id, c701_distributor_id, c2652_locked_date
				   , c2652_locked_by
					)
			 VALUES (v_lock_id, p_audit_id, p_dist_id, SYSDATE
				   , p_user_id
					);

		INSERT INTO t2651_field_sales_details
					(c2651_field_sales_details_id, c2650_field_sales_audit_id, c701_distributor_id
				   , c2652_field_sales_lock_id, c2651_start_date, c2651_created_by, c2651_created_date
					)
			 VALUES (s2651_field_sales_details.NEXTVAL, p_audit_id, p_dist_id
				   , v_lock_id, TRUNC (SYSDATE), p_user_id, SYSDATE
					);

		gm_ac_ld_reference (v_lock_id, p_dist_id);
		gm_ac_ld_details (v_lock_id, p_dist_id);
		gm_ac_ld_vc_details (v_lock_id, p_dist_id);
		gm_ac_ld_vc_net_details (v_lock_id, p_dist_id);
 --	
	END gm_ac_ld_main;

--
	/****************************************************************
	 * Load the reference table with all the consignments and the
	 * returns for the parts that are mapped as taggable parts
	 ***************************************************************/
--
	PROCEDURE gm_ac_ld_reference (
		p_fs_lock_id   IN	t2652_field_sales_lock.c2652_field_sales_lock_id%TYPE
	  , p_dist_id	   IN	t701_distributor.c701_distributor_id%TYPE
	)
	AS
	BEGIN
		INSERT INTO t2659_field_sales_ref_lock
					(c2659_field_sales_ref_lock_id, c2652_field_sales_lock_id, c205_part_number_id, c207_set_id
				   , c2659_control_number, c2659_qty, c2659_ref_id, c901_ref_type, c2659_ref_date)
			SELECT s2659_field_sales_ref_lock.NEXTVAL, p_fs_lock_id, p_num, set_id, cnum, qty, ref_id, ref_type
				 , ref_date
			  FROM (SELECT t505.c205_part_number_id p_num, t505.c505_control_number cnum, t504.c207_set_id set_id
						 , t505.c505_item_qty qty, t504.c504_consignment_id ref_id, t504.c504_ship_date ref_date
						 , 51000 ref_type
					  FROM t504_consignment t504, t505_item_consignment t505, t205d_part_attribute t205d
					 WHERE t504.c701_distributor_id IS NOT NULL
					   AND t504.c504_consignment_id = t505.c504_consignment_id
					   AND t505.c205_part_number_id = t205d.c205_part_number_id
					   AND t504.c504_status_fl = 4
					   AND t504.c504_type IN (4110)
					   AND TRIM (t505.c505_control_number) IS NOT NULL
					   AND c504_void_fl IS NULL
					   AND c701_distributor_id = p_dist_id
					   AND t205d.c901_attribute_type = 92340   -- taggable part
					   AND t205d.c205d_attribute_value = 'Y'
					   AND t205d.c205d_void_fl IS NULL
					UNION ALL
					SELECT t507.c205_part_number_id pnum, t507.c507_control_number cnum, t506.c207_set_id set_id
						 , t507.c507_item_qty * -1 qty, t506.c506_rma_id ref_id, t506.c506_credit_date ref_date
						 , 51001 ref_type
					  FROM t506_returns t506, t507_returns_item t507, t205d_part_attribute t205d
					 WHERE t506.c701_distributor_id IS NOT NULL
					   AND t506.c506_status_fl = 2
					   AND t506.c506_rma_id = t507.c506_rma_id
					   AND t507.c205_part_number_id = t205d.c205_part_number_id
					   AND t507.c507_status_fl IN ('C', 'W')
					   AND t506.c506_void_fl IS NULL
					   AND c701_distributor_id = p_dist_id
					   AND t205d.c901_attribute_type = 92340   -- taggable part
					   AND t205d.c205d_attribute_value = 'Y'
					   AND t205d.c205d_void_fl IS NULL);
	END gm_ac_ld_reference;

--
	/****************************************************************
	 * Load the net qty for the parts that are mapped as taggable parts
	 ***************************************************************/
--
	PROCEDURE gm_ac_ld_details (
		p_fs_lock_id   IN	t2652_field_sales_lock.c2652_field_sales_lock_id%TYPE
	  , p_dist_id	   IN	t701_distributor.c701_distributor_id%TYPE
	)
	AS
	BEGIN
		INSERT INTO t2657_field_sales_details_lock
					(c2657_field_sales_details_lock, c2652_field_sales_lock_id, c205_part_number_id, c207_set_id
				   , c2657_qty)
			(SELECT s2659_field_sales_ref_lock.NEXTVAL, p_fs_lock_id, p_num, set_id, qty
			   FROM (SELECT   t731.c205_part_number_id p_num, t730.c207_set_id set_id, SUM (t731.c731_item_qty) qty
						 FROM t730_virtual_consignment t730, t731_virtual_consign_item t731, t205d_part_attribute t205d
						WHERE t730.c730_virtual_consignment_id = t731.c730_virtual_consignment_id
						  AND t730.c701_distributor_id = p_dist_id
						  AND t731.c205_part_number_id = t205d.c205_part_number_id
						  AND t205d.c901_attribute_type = 92340   -- taggable part
						  AND t205d.c205d_attribute_value = 'Y'
						  AND t205d.c205d_void_fl IS NULL
					 GROUP BY t731.c205_part_number_id, t730.c207_set_id));
	END gm_ac_ld_details;

--
	/****************************************************************
	 * Load all the virtual consignment for the set and the item
	 ***************************************************************/
--
	PROCEDURE gm_ac_ld_vc_details (
		p_fs_lock_id   IN	t2652_field_sales_lock.c2652_field_sales_lock_id%TYPE
	  , p_dist_id	   IN	t701_distributor.c701_distributor_id%TYPE
	)
	AS
		v_virtual_consign_id t731_virtual_consign_item.c730_virtual_consignment_id%TYPE;
		v_csg_lock_id  NUMBER;

		CURSOR v_set_cursor
		IS
			SELECT t730.c730_virtual_consignment_id vc_id, t730.c207_set_id set_id, t730.c730_total_cost total_cost
				 , t730.c730_total_cogs_cost cogs_cost
			  FROM t730_virtual_consignment t730
			 WHERE t730.c701_distributor_id = p_dist_id;

		CURSOR v_item_cursor
		IS
			SELECT t731.c205_part_number_id pnum, t731.c731_item_qty qty, t731.c731_item_price price
				 , t731.c731_item_cogs_price cogs_price
			  FROM t731_virtual_consign_item t731
			 WHERE t731.c730_virtual_consignment_id = v_virtual_consign_id;
	BEGIN
		FOR set_val IN v_set_cursor
		LOOP
			SELECT s2654_consignment_set_lock.NEXTVAL
			  INTO v_csg_lock_id
			  FROM DUAL;

			INSERT INTO t2654_consignment_set_lock
						(c2654_consignment_lock_id, c2652_field_sales_lock_id, c207_set_id, c2654_total_cost
					   , c2654_cogs_cost
						)
				 VALUES (v_csg_lock_id, p_fs_lock_id, set_val.set_id, set_val.total_cost
					   , set_val.cogs_cost
						);

			v_virtual_consign_id := set_val.vc_id;

			FOR item_val IN v_item_cursor
			LOOP
				INSERT INTO t2655_consignment_item_lock
							(c2615_consignment_item_lock_id, c2654_consignment_lock_id, c205_part_number_id
						   , c2655_qty, c2655_item_price, c2655_cogs_price
							)
					 VALUES (s2655_consignment_item_lock.NEXTVAL, v_csg_lock_id, item_val.pnum
						   , item_val.qty, item_val.price, item_val.cogs_price
							);
			END LOOP;
		END LOOP;
	END gm_ac_ld_vc_details;

--
	/****************************************************************
	 * Load the net of virtual consignment for the items
	 ***************************************************************/
--
	PROCEDURE gm_ac_ld_vc_net_details (
		p_fs_lock_id   IN	t2652_field_sales_lock.c2652_field_sales_lock_id%TYPE
	  , p_dist_id	   IN	t701_distributor.c701_distributor_id%TYPE
	)
	AS
	BEGIN
		INSERT INTO t2653_field_sales_inv_lock
					(c2653_field_sales_inv_lock_id, c2652_field_sales_lock_id, c205_part_number_id, c2653_qty)
			SELECT s2653_field_sales_inv_lock.NEXTVAL, p_fs_lock_id, pnum, qty
			  FROM (SELECT	 t731.c205_part_number_id pnum, SUM (t731.c731_item_qty) qty
						FROM t730_virtual_consignment t730, t731_virtual_consign_item t731
					   WHERE t730.c730_virtual_consignment_id = t731.c730_virtual_consignment_id
						 AND t730.c701_distributor_id = p_dist_id
					GROUP BY t731.c205_part_number_id);
	END gm_ac_ld_vc_net_details;
END gm_pkg_ac_ld_field_sales;
/
