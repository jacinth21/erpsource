/* Formatted on 2009/03/03 14:24 (Formatter Plus v4.8.0) */

--@"C:\Database\Packages\Accounting\gm_pkg_ac_sage.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_ac_sage
IS
/*******************************************************
	* Description : Procedure to fetch all Vendor Info
	 by what have been entered
	* Author		 : Xun Qu
	*******************************************************/
	PROCEDURE gm_ac_fch_sage_vendor (
		p_vendorname		  IN	   t301_vendor.c301_vendor_name%TYPE
	  , p_missingvendorflag   IN	   VARCHAR2
	  , p_vendordtls		  OUT	   TYPES.cursor_type
	  
	)
	AS
	v_company_id t1900_company.c1900_company_id%TYPE;
	BEGIN
			   SELECT get_compid_frm_cntx()
			     INTO v_company_id
			    FROM dual;
	    OPEN p_vendordtls
		 FOR
			 SELECT   c301_vendor_id vendorid, c301_vendor_name vendorname, c301_sage_vendor_id sagevendorid
				 FROM t301_vendor
				WHERE c301_active_fl IS NULL
				  AND UPPER (c301_vendor_name) LIKE UPPER ('%' || TRIM (p_vendorname) || '%')
				  AND NVL (c301_sage_vendor_id, '-9999') =
														DECODE (p_missingvendorflag
															  , 'on', '-9999'
															  , c301_sage_vendor_id
															   )
                 AND C1900_COMPANY_ID = v_company_id
			 ORDER BY c301_vendor_id;
	END gm_ac_fch_sage_vendor;

/*******************************************************
	  * Description : Procedure to save all SageVendorID
	  * Author		: Xun
	  *******************************************************/
	PROCEDURE gm_ac_sav_sage_vendor (
		p_inputstr	 IN   VARCHAR2
	  , p_userid	 IN   t301_vendor.c301_last_updated_by%TYPE
	)
	AS
		v_strlen	   NUMBER := NVL (LENGTH (p_inputstr), 0);
		v_string	   VARCHAR2 (4000) := p_inputstr;
		v_substring    VARCHAR2 (1000);
		v_vendorid	   t301_vendor.c301_vendor_id%TYPE;
		v_sagevendorid t301_vendor.c301_sage_vendor_id%TYPE;
	--
	BEGIN
		IF v_strlen > 0
		THEN
			WHILE INSTR (v_string, '|') <> 0
			LOOP
				--
				v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
				v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
				--
				v_vendorid	:= NULL;
				v_sagevendorid := NULL;
				--
				v_vendorid	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_sagevendorid := v_substring;

				UPDATE t301_vendor
				   SET c301_sage_vendor_id = v_sagevendorid
					 , c301_last_updated_by = p_userid
					 , c301_last_updated_date = SYSDATE
				 WHERE c301_vendor_id = v_vendorid AND c301_active_fl IS NULL;
			END LOOP;
		END IF;
	END gm_ac_sav_sage_vendor;

/*******************************************************
	  * Description : Procedure to display Invoice details
	  * Author		: Xun
	  *******************************************************/
	PROCEDURE gm_ac_fch_download_invc_dtls (
		p_vendorid		 IN 	  t406_payment.c301_vendor_id%TYPE
	  , p_poid			 IN 	  t401_purchase_order.c401_purchase_ord_id%TYPE
	  , p_frmdt 		 IN 	  VARCHAR2
	  , p_todt			 IN 	  VARCHAR2
	  , p_invoiceid 	 IN 	  t406_payment.c406_invoice_id%TYPE
	  , p_out_invcdtls	 OUT	  TYPES.cursor_type
	)
	AS
	v_company_id t1900_company.c1900_company_id%TYPE;
	v_dateformat VARCHAR2(100);
	BEGIN
			   SELECT get_compid_frm_cntx(),NVL(get_compdtfmt_frm_cntx(),GET_RULE_VALUE('DATEFMT','DATEFORMAT'))  
			     INTO v_company_id,v_dateformat
			    FROM dual;
			    
		
		OPEN p_out_invcdtls
		 FOR
			 SELECT get_sage_vendor_id (t406.c301_vendor_id) sage_vendor_id, c406_payment_id paymentid
				  , get_vendor_name (t406.c301_vendor_id) vendor_name, t406.c406_invoice_id invoice_number
				  , TO_CHAR (t406.c406_invoice_dt, v_dateformat) invoice_dt
				  ,   get_currency_conversion (get_vendor_currency (t406.c301_vendor_id)
											 , get_comp_curr (t406.c1900_company_id)
											 , SYSDATE
											 , NVL (t406.c406_payment_amount, 0)
											  )
					* 1 invoice_amount
			   --	, t406.c406_payment_amount invoice_amount
			 FROM	t406_payment t406
			  WHERE t406.c406_status_fl = 20
				AND t406.c406_void_fl IS NULL
				AND t406.c301_vendor_id = DECODE (p_vendorid, 0, t406.c301_vendor_id, p_vendorid)
				AND t406.c406_invoice_dt >=
										   DECODE (p_frmdt
												 , NULL, t406.c406_invoice_dt
												 , TO_DATE (p_frmdt, v_dateformat)
												  )
				AND t406.c406_invoice_dt <= DECODE (p_todt, NULL, t406.c406_invoice_dt, TO_DATE (p_todt, v_dateformat))
				AND t406.c401_purchase_ord_id = DECODE (p_poid, NULL, t406.c401_purchase_ord_id, p_poid)
				AND t406.c406_invoice_id = DECODE (p_invoiceid, NULL, t406.c406_invoice_id, p_invoiceid)
				AND t406.c406_payment_id IS NOT NULL
				AND t406.C1900_COMPANY_ID = v_company_id order by vendor_name asc; 
	END gm_ac_fch_download_invc_dtls;
END gm_pkg_ac_sage;
/
