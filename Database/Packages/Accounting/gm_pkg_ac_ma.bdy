/* Formatted on 2009/08/07 17:40 (Formatter Plus v4.8.0) */
-- @"c:\database\packages\accounting\gm_pkg_ac_ma.bdy"

CREATE OR REPLACE PACKAGE BODY gm_pkg_ac_ma
IS
--
	PROCEDURE gm_ac_fch_madetails (
		p_maid			IN		 t215_part_qty_adjustment.c215_part_qty_adj_id%TYPE
	  , p_outmaheader	OUT 	 TYPES.cursor_type
	  , p_outmadetail	OUT 	 TYPES.cursor_type
	)
	AS

	/*******************************************************
	  * Description : Procedure to fetch ma details, header
	  * Author	   : Rakhi Gandhi
	  *******************************************************/
	BEGIN
	   
	
		OPEN p_outmaheader
		 FOR
			 SELECT t215.c215_part_qty_adj_id maid, t215.c215_comments madesc
				  , get_code_name (t215.c901_adjustment_source) matypeid
				  , TO_CHAR (t215.c215_created_date, 'MM/DD/YYYY') createddate
				  , get_user_name (t215.c215_created_by) createdby
				  , DECODE (c215_update_inv_fl, 'Y', 'Y', 'N') updoprqty
			   FROM t215_part_qty_adjustment t215
			  WHERE t215.c215_part_qty_adj_id = p_maid;  -- 'MAID'
			    

		OPEN p_outmadetail
		 FOR
			 SELECT t216.c205_part_number_id pnum, get_partnum_desc (t216.c205_part_number_id) partdesc
				  , NVL (t216.c216_qty_to_be_adjusted, 0) maqty, NVL (t216.c216_item_price, 0) maprice
				  , NVL (t216.c216_qty_to_be_adjusted * t216.c216_item_price, 0) extcost
				  , t216.c1900_owner_company_id owner_id, get_company_code(t216.c1900_owner_company_id) owner_cd
				  , t216.c216_local_item_price local_cost
				  , NVL (t216.c216_qty_to_be_adjusted * t216.c216_local_item_price, 0) local_ext_cost
				  , get_code_name_alt (get_comp_curr(t216.c1900_owner_company_id)) owner_curr
				  , get_code_name_alt (get_comp_curr(t215.c1900_company_id)) local_curr
				  , get_company_name(t216.c1900_owner_company_id) owner_name
			   FROM t215_part_qty_adjustment t215, t216_part_qty_adj_details t216
			  WHERE t215.c215_part_qty_adj_id = t216.c215_part_qty_adj_id 
			  AND t215.c215_part_qty_adj_id = p_maid;
	END gm_ac_fch_madetails;

--
	PROCEDURE gm_ac_sav_madetails (
		p_inputstr		 IN 	  VARCHAR2
	  , p_desc			 IN 	  t215_part_qty_adjustment.c215_comments%TYPE
	  , p_matype		 IN 	  t901_code_lookup.c901_code_id%TYPE
	  , p_fromcosttype	 IN 	  t215_part_qty_adjustment.c901_adjustment_source%TYPE
	  , p_userid		 IN 	  t215_part_qty_adjustment.c215_created_by%TYPE
	  , p_updoprqty 	 IN 	  VARCHAR2
	  , p_outmaid		 OUT	  t215_part_qty_adjustment.c215_part_qty_adj_id%TYPE
	)
	AS
		v_strlen	   NUMBER := NVL (LENGTH (p_inputstr), 0);
		v_string	   VARCHAR2 (30000) := p_inputstr;
		v_substring    VARCHAR2 (1000);
		v_seq		   NUMBER;
		v_id		   VARCHAR2 (20);
		v_pnum		   t216_part_qty_adj_details.c205_part_number_id%TYPE;
		v_price 	   t216_part_qty_adj_details.c216_item_price%TYPE;
		v_qty		   t216_part_qty_adj_details.c216_qty_to_be_adjusted%TYPE;
		v_partqty	   t820_costing.c820_qty_on_hand%TYPE;
		v_oprqty	   t205c_part_qty.c205_available_qty%TYPE;
    	v_invalid_partstr    CLOB;
    	v_company_id  t1900_company.c1900_company_id%TYPE;
		v_plant_id    t5040_plant_master.C5040_PLANT_ID %TYPE;
		--
		v_owner_company_id t1900_company.c1900_company_id%TYPE;
		v_local_price 	   t216_part_qty_adj_details.c216_item_price%TYPE;
/*******************************************************
	 * Description : Procedure to save ma details
	 * Author	: Joe P Kumar
 *******************************************************/
	BEGIN
		 select get_compid_frm_cntx(),get_plantid_frm_cntx() 
		into v_company_id,v_plant_id 
		from dual;
--
		SELECT s215_part_qty_adjustment.NEXTVAL
		  INTO v_seq
		  FROM DUAL;

		v_id		:= 'GM-MA-' || v_seq;
		p_outmaid	:= v_id;

--
		INSERT INTO t215_part_qty_adjustment
					(c215_part_qty_adj_id, c215_adj_date, c901_adjustment_source, c215_comments, c215_validated_by
				   , c215_approved_by, c215_created_by, c215_created_date, c215_update_inv_fl,c1900_company_id
					)
			 VALUES (v_id, CURRENT_DATE, p_matype, p_desc, '303029'
				   , '303009', p_userid, CURRENT_DATE, p_updoprqty,v_company_id
					);

		IF v_strlen > 0
		THEN
			WHILE INSTR (v_string, '|') <> 0
			LOOP
				--
				v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
				v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
				--
				v_pnum		:= NULL;
				v_qty		:= NULL;
				v_price 	:= NULL;
				v_owner_company_id := NULL;
				v_local_price := NULL;
				--
				v_pnum		:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_qty		:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_price 	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_owner_company_id := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_local_price := v_substring;

				-- Accounting Balance On Hand
				SELECT get_qty_onhand (v_pnum, p_fromcosttype)
				  INTO v_partqty
				  FROM DUAL;

				-- Operations Balance On Hand
				SELECT DECODE (p_fromcosttype
							 , 4900, GET_QTY_IN_INVENTORY(p_outmaid,'90800',v_pnum)
							 , 4909, get_partnumber_qty (v_pnum, 90802)
							 , 4902, (get_partnumber_qty (v_pnum, 90813) - get_qty_in_transactions_quaran (v_pnum))
							 , -9999
							  )
				  INTO v_oprqty
				  FROM t205_part_number
				 WHERE c205_part_number_id = v_pnum
				 FOR UPDATE;

				--
				--IF	   ((v_qty > v_partqty) OR (v_qty > v_oprqty)) AND (p_fromcosttype IS NOT NULL) AND (p_matype <> '48056')	-- 48056 is scrap to RM
				IF (p_updoprqty = 'Y')
				THEN
					IF	   ((v_qty > v_partqty) OR (v_qty > v_oprqty))
					   AND (p_fromcosttype IS NOT NULL)
					   AND (p_matype <> '48056')   -- 48056 is scrap to RM
					THEN
						--raise_application_error ('-20064', '');
						 v_invalid_partstr := v_invalid_partstr ||v_pnum ||', ';       -- separating invalid parts
						 GOTO skip_iteration;--CONTINUE;
					END IF;
				ELSIF	  ((NVL(v_partqty,0) - v_qty) < 0)
					  AND (p_fromcosttype IS NOT NULL)
					  AND (p_matype <> '48056')   -- 48056 is scrap to RM
				THEN
					--raise_application_error ('-20064', '');
					 v_invalid_partstr := v_invalid_partstr ||v_pnum ||', ';       -- separating invalid parts	
					 GOTO skip_iteration;--CONTINUE;		
				END IF;
				INSERT INTO t216_part_qty_adj_details
							(c216_part_qty_adj_id, c215_part_qty_adj_id, c205_part_number_id, c216_qty_to_be_adjusted
						   , c216_item_price, c1900_owner_company_id, c216_local_item_price
						   , c216_created_by, c216_created_date
							)
					 VALUES (s216_part_qty_adj_details.NEXTVAL, v_id, v_pnum, v_qty
						   , DECODE (v_price, NULL, get_inventory_cogs_value (v_pnum), v_price), v_owner_company_id, v_local_price
						   , p_userid, CURRENT_DATE
							);
			--PMT-5230: ADDED GOTO STATEMENT AND REMOVED CONTINUE BCOZ ORACLE 10G DOESNOT SUPPORT
				<<skip_iteration>>
				NULL;
			END LOOP;
		END IF;
		IF v_invalid_partstr IS NOT NULL
			THEN
			 v_invalid_partstr := SUBSTR (v_invalid_partstr, 1, (LENGTH(v_invalid_partstr)-2) ); 
			 GM_RAISE_APPLICATION_ERROR('-20999','172',v_invalid_partstr);
		END IF;  
		--	  gm_procedure_log('p_updoprqty',p_updoprqty);
		IF (p_updoprqty = 'Y')
		THEN
			gm_pkg_ac_ma.gm_update_qty_manual (v_id);
		END IF;

		gm_pkg_ac_ma.gm_save_manual_ledger_posting (v_id);
	END gm_ac_sav_madetails;

--
/*******************************************************
	 * Description : Procedure to update manual qty
	 * Author	: Joe P Kumar
 *******************************************************/
	PROCEDURE gm_update_qty_manual (
		p_maid	 IN   t215_part_qty_adjustment.c215_part_qty_adj_id%TYPE
	)
	AS
--
		CURSOR qb_cursor
		IS
			SELECT c215_part_qty_adj_id p_adj_id, c205_part_number_id p_num, c216_qty_to_be_adjusted q_adj
				 , c216_created_by created_by
			  FROM t216_part_qty_adj_details
			 WHERE c216_qty_to_be_adjusted <> 0 AND c215_part_qty_adj_id = p_maid;

--
		v_part_number  t214_transactions.c205_part_number_id%TYPE;
		v_adj_value    t214_transactions.c214_new_qty%TYPE;
		v_temp		   VARCHAR2 (20);
		v_adjsrc	   NUMBER;
		v_tableupd	   VARCHAR2 (20);
		v_sign		   NUMBER;
		v_layer 	   VARCHAR2 (20);
		v_fromtype	   VARCHAR2 (20);
--
	BEGIN
--
		SELECT c901_adjustment_source, get_rule_value (c901_adjustment_source, 'TABLE-TO-UPDATE')
			 , TO_NUMBER (get_rule_value (c901_adjustment_source, 'INV-QTY-OPERATION'))
			 , get_rule_value (c901_adjustment_source, 'INV-QTY-LAYER'), get_fromcost_type (c901_adjustment_source)
		  INTO v_adjsrc, v_tableupd
			 , v_sign
			 , v_layer, v_fromtype
		  FROM t215_part_qty_adjustment
		 WHERE c215_part_qty_adj_id = p_maid;

		--
		FOR qb_val IN qb_cursor
		LOOP
			--
			v_part_number := qb_val.p_num;
			v_adj_value := qb_val.q_adj * v_sign;

			--
			IF v_tableupd = 'T205'
			THEN
				IF v_fromtype = '4902'	 --Quarantine Inventory
				THEN
					gm_cm_sav_partqty (v_part_number,
                               v_adj_value,
                               p_maid,
                               qb_val.created_by,
                               90813, -- Quarantine  Qty
                               4303,-- Manual
                               4316 -- Manual 
                               );
				ELSE
					gm_cm_sav_partqty (v_part_number,
                               v_adj_value,
                               p_maid,
                               qb_val.created_by,
                               90800, -- FG Qty
                               4303,-- Manual
                               4316 -- Manual 
                               );
				END IF;
			ELSIF v_tableupd = 'T205C'
			THEN
				gm_cm_sav_partqty (v_part_number, v_adj_value, p_maid, qb_val.created_by, v_layer, 4302, 4316);
			END IF;
		--
		END LOOP;
	--
	END gm_update_qty_manual;

/*******************************************************
	 * Description : Procedure for ledger posting - manual
	 * Author	: Joe P Kumar
 *******************************************************/
	PROCEDURE gm_save_manual_ledger_posting (
		p_maid	 IN   t215_part_qty_adjustment.c215_part_qty_adj_id%TYPE
	)
	AS
--
		CURSOR qb_cursor
		IS
			SELECT t216.c205_part_number_id p_num, t216.c216_item_price p_price, t216.c216_qty_to_be_adjusted q_adj
				, t216.c1900_owner_company_id owner_id, t216.c216_local_item_price local_price
			  FROM t216_part_qty_adj_details t216
			 WHERE t216.c216_qty_to_be_adjusted <> 0 AND t216.c215_part_qty_adj_id = p_maid;

--
		v_source	   t215_part_qty_adjustment.c901_adjustment_source%TYPE;
		v_matype	   t901_code_lookup.c901_code_id%TYPE;
		v_date		   DATE;
		v_user		   t215_part_qty_adjustment.c215_created_by%TYPE;
		v_id		   t215_part_qty_adjustment.c215_part_qty_adj_id%TYPE;
--
	BEGIN
--
		SELECT c901_adjustment_source, c215_adj_date, c215_created_by, c215_part_qty_adj_id
		  INTO v_matype, v_date, v_user, v_id
		  FROM t215_part_qty_adjustment
		 WHERE c215_part_qty_adj_id = p_maid;

		--
		FOR qb_val IN qb_cursor
		LOOP
			  --
			--
			  /*DBMS_OUTPUT.put_line (	 v_part_number
									|| ' ** '
									|| v_adj_value
									|| ' ** '
									|| v_posting_type
									|| ' ** '
									|| qb_val.p_adj_id
								   );
				   */
			  --
			gm_save_ledger_posting (v_matype, v_date, qb_val.p_num, NULL, v_id, qb_val.q_adj, qb_val.p_price, v_user, NULL, qb_val.owner_id, qb_val.local_price);
		--
		END LOOP;
	END gm_save_manual_ledger_posting;

	--
	PROCEDURE gm_fch_ma_ref_detail (
		p_ref_id	 IN 	  t216a_part_qty_adj_ref.c216a_ref_id%TYPE
	  , p_ref_type	 IN 	  t216a_part_qty_adj_ref.c901_ref_type%TYPE
	  , p_outmaid	 OUT	  TYPES.cursor_type
	)
	AS
	/*******************************************************
	  * Description : Procedure to fetch ma ids
	  * Author	   : xun
	  *******************************************************/
	v_company_id  t1900_company.c1900_company_id%TYPE;
	v_plant_id    t5040_plant_master.C5040_PLANT_ID %TYPE;
	BEGIN
		SELECT get_compid_frm_cntx(),get_plantid_frm_cntx() 
			INTO v_company_id,v_plant_id 
		FROM dual;
		
		OPEN p_outmaid
		 FOR
			 SELECT c215_part_qty_adj_id maid
			   FROM t216a_part_qty_adj_ref
			  WHERE c216a_ref_id = p_ref_id AND c901_ref_type = p_ref_type
			  AND C1900_COMPANY_ID =v_company_id ;
	END gm_fch_ma_ref_detail;

--
	PROCEDURE gm_sav_manual_ref_detail (
		p_maid		 IN   t215_part_qty_adjustment.c215_part_qty_adj_id%TYPE
	  , p_ref_id	 IN   t216a_part_qty_adj_ref.c216a_ref_id%TYPE
	  , p_ref_type	 IN   t216a_part_qty_adj_ref.c901_ref_type%TYPE
	)
	AS
/*******************************************************
  * Description : Procedure to insert record to t216a_part_qty_adj_ref
  * Author	   : xun
  *******************************************************/
	v_company_id  t1900_company.c1900_company_id%TYPE;
	v_plant_id    t5040_plant_master.C5040_PLANT_ID %TYPE;
	BEGIN
		
		SELECT get_compid_frm_cntx(),get_plantid_frm_cntx() 
		INTO v_company_id,v_plant_id 
		FROM dual;
		
		INSERT INTO t216a_part_qty_adj_ref
					(c216a_part_qty_adj_ref_id, c215_part_qty_adj_id, c216a_ref_id, c901_ref_type,C1900_COMPANY_ID
					)
			 VALUES (s216a_part_qty_adj_ref.NEXTVAL, p_maid, p_ref_id, p_ref_type,v_company_id
					);
	END gm_sav_manual_ref_detail;

--
	PROCEDURE gm_sav_manual_qty (
		p_maid		IN	 t215_part_qty_adjustment.c215_part_qty_adj_id%TYPE
	  , p_location_id  IN   t2605_audit_tag_detail.c901_location_id%TYPE
	  , p_user_id	IN	 t2605_audit_tag_detail.c2605_last_updated_by%TYPE
	)
	AS
		v_adjsrc	   t215_part_qty_adjustment.c901_adjustment_source%TYPE;
		v_trans_type   t906_rules.c906_rule_value%TYPE;
		
		CURSOR adj_cursor
		IS
			SELECT c215_part_qty_adj_id p_adj_id, c205_part_number_id p_num, c216_qty_to_be_adjusted q_adj_qty
				 , c216_created_by created_by
			  FROM t216_part_qty_adj_details
			 WHERE c216_qty_to_be_adjusted <> 0 AND c215_part_qty_adj_id = p_maid;
	BEGIN
		SELECT	   c901_adjustment_source
			  INTO v_adjsrc
			  FROM t215_part_qty_adjustment
			 WHERE c215_part_qty_adj_id = p_maid
		FOR UPDATE;
		-- to get the transactions type
		SELECT NVL (get_rule_value (p_location_id, 'PI_INV_WARHOUSE_TYPE'), '90800')
		  INTO v_trans_type
		  FROM dual;
		--  
		FOR adj_val IN adj_cursor
		LOOP
			IF (v_adjsrc = 48159)   --4831 SOURCE IS FG to PI Adj
			THEN
				gm_cm_sav_partqty (adj_val.p_num,
                               (adj_val.q_adj_qty * -1),
                               p_maid,
                               p_user_id,
                               v_trans_type, -- FG/Returns hold  Qty
                               4302,-- minus
                               4316 --  Manual Adjustment 
                               );
			END IF;

			IF (v_adjsrc = 48161)   -- 4820 Quarantine to PI Adj
			THEN
				gm_cm_sav_partqty (adj_val.p_num,
                               (adj_val.q_adj_qty * -1),
                               p_maid,
                               p_user_id,
                               90813, -- Quarantine  Qty
                               4302,-- minus
                               4316 --  Manual Adjustment 
                               );
			END IF;

			IF (v_adjsrc = 48160)   --4832 SOURCE IS PI Adj TO FG
			THEN
				gm_cm_sav_partqty (adj_val.p_num,
                               adj_val.q_adj_qty,
                               p_maid,
                               p_user_id,
                               v_trans_type, -- FG/Returns hold  Qty
                               4301,-- PLUS
                               4316 --  Manual Adjustment 
                               );
			END IF;

			IF (v_adjsrc = 48162)   -- 4829 PI Adj to Quarantine
			THEN
				gm_cm_sav_partqty (adj_val.p_num,
                               adj_val.q_adj_qty,
                               p_maid,
                               p_user_id,
                               90813, -- Quarantine  Qty
                               4301,-- PLUS
                               4316 --  Manual Adjustment 
                               );
			END IF;

-- Added for Repack Warehouse as a part of PI 2011
      IF (v_adjsrc = 48165)   -- 4820 Repack to PI Adj
			THEN
				gm_cm_sav_partqty (adj_val.p_num,
                               (adj_val.q_adj_qty * -1),
                               p_maid,
                               p_user_id,
                               90815, -- Repack  Qty
                               4302,-- minus
                               4316 --  Manual Adjustment 
                               );
			END IF;
            
          IF (v_adjsrc = 48166)   -- 4829 PI Adj to Repack
			THEN
				gm_cm_sav_partqty (adj_val.p_num,
                               adj_val.q_adj_qty,
                               p_maid,
                               p_user_id,
                               90815, -- Repack  Qty
                               4301,-- PLUS
                               4316 --  Manual Adjustment 
                               );
			END IF;

-- Added for Inhouse Inventroy Warehouse as a part of PI 2013

	
	IF (v_adjsrc = 48218)   -- In-house Inventory to Physical Inventory Adjustment
			THEN
				/*4316   -- Manual Adjustment
				 4302	--minus
				 90816 --Inhouse Inventory Qty
				 */
				gm_cm_sav_partqty(adj_val.p_num,adj_val.q_adj_qty * -1,p_maid,p_user_id,90816,4302,4316);
			END IF;
            
          IF (v_adjsrc = 48219)   -- Physical Inventory Adjustment to In-house Inventory
			THEN
				/*4316   -- Manual Adjustment
				 4301	--plus
				 90816 --Inhouse Inventory Qty
				 */
				gm_cm_sav_partqty(adj_val.p_num,adj_val.q_adj_qty,p_maid,p_user_id,90816,4301,4316);
			END IF; 

-- Added for Restricted Warehouse as a part of PI 2013
      IF (v_adjsrc = 48220)   -- Restricted Warehouse to Physical Inventory Adjustment
			THEN
				/*4316  -- Manual Adjustment
				 4302	--minus
				 56001 --Restricted Warehouse Inventory Qty
				 */
				gm_cm_sav_partqty(adj_val.p_num,adj_val.q_adj_qty * -1,p_maid,p_user_id,56001,4302,4316);
			END IF;
            
          IF (v_adjsrc = 48221)   -- Physical Inventory Adjustment to Restricted Warehouse
			THEN
				/*4316  -- Manual Adjustment
				 4301	--plus
				 56001 --Restricted Warehouse Inventory Qty
				 */
				gm_cm_sav_partqty(adj_val.p_num,adj_val.q_adj_qty,p_maid,p_user_id,56001,4301,4316);
			END IF; 

			IF (v_adjsrc = 48164)	--SOURCE IS PI Adj TO RAWMATERIAL
			THEN
				gm_cm_sav_partqty (adj_val.p_num, adj_val.q_adj_qty, p_maid, adj_val.created_by, 90802, 4301, 4322);
			END IF;

			IF (v_adjsrc = 48163)	--SOURCE IS RAWMATERIAL TO PI Adj
			THEN
				gm_cm_sav_partqty (adj_val.p_num, adj_val.q_adj_qty * -1, p_maid, adj_val.created_by, 90802, 4302
								 , 4322);
			END IF;
		END LOOP;
	END gm_sav_manual_qty;

---
	PROCEDURE gm_sav_manual_qty_bulk (
		p_maid		IN	 t215_part_qty_adjustment.c215_part_qty_adj_id%TYPE
	  , p_user_id	IN	 t2605_audit_tag_detail.c2605_last_updated_by%TYPE
	)
	AS
		v_adjsrc	   t215_part_qty_adjustment.c901_adjustment_source%TYPE;

		CURSOR adj_cursor
		IS
			SELECT c215_part_qty_adj_id p_adj_id, c205_part_number_id p_num, c216_qty_to_be_adjusted q_adj_qty
				 , c216_created_by created_by
			  FROM t216_part_qty_adj_details
			 WHERE c216_qty_to_be_adjusted <> 0 AND c215_part_qty_adj_id = p_maid;
	BEGIN
		SELECT	   c901_adjustment_source
			  INTO v_adjsrc
			  FROM t215_part_qty_adjustment
			 WHERE c215_part_qty_adj_id = p_maid
		FOR UPDATE;

	FOR adj_val IN adj_cursor
		LOOP
			IF (v_adjsrc = 48203)   --SOURCE IS  BULK TO PI Adj   (minus)
			THEN
				gm_cm_sav_partqty (adj_val.p_num,
                               (adj_val.q_adj_qty * -1),
                               p_maid,
                               p_user_id,
                               90814, -- BULK  Qty
                               4302,-- minus
                               4316 --  Manual Adjustment 
                               );
			END IF;

			IF (v_adjsrc = 48204)   --SOURCE IS  PI Adj TO BULK (plus)
			THEN
				gm_cm_sav_partqty (adj_val.p_num,
                               adj_val.q_adj_qty,
                               p_maid,
                               p_user_id,
                               90814, -- BULK  Qty
                               4301,-- PLUS
                               4316 --  Manual Adjustment 
                               );
			END IF;
		END LOOP;
	END gm_sav_manual_qty_bulk;

--
	PROCEDURE gm_sav_ma_dxn_void (
		p_physical_audit_id   IN   t2605_audit_tag_detail.c2605_audit_tag_id%TYPE
	  , p_location_id		  IN   t2605_audit_tag_detail.c901_location_id%TYPE
	  , p_user_id			  IN   t2605_audit_tag_detail.c2605_last_updated_by%TYPE
	)
	AS
	v_company_id  t1900_company.c1900_company_id%TYPE;
	v_plant_id    t5040_plant_master.C5040_PLANT_ID %TYPE;
	
		CURSOR txn_cursor
		IS
			SELECT t2605.c2605_txn_id txn_id
			  FROM t2605_audit_tag_detail t2605
			 WHERE t2605.c901_location_id = p_location_id
			   AND t2605.c2600_physical_audit_id = p_physical_audit_id
			   AND t2605.c901_type = 50676	 ---adjustment
			   AND t2605.c5040_plant_id    = v_plant_id
			   AND t2605.c2605_txn_id IS NOT NULL
               AND t2605.c205_part_number_id IS NULL -- necessary for part & trans , if not added will double the qty to adjust
               AND t2605.c2605_void_fl is null;  ---check for void flag

	BEGIN
	select get_compid_frm_cntx(),get_plantid_frm_cntx() 
		into v_company_id,v_plant_id 
		from dual;
	
		FOR txn_val IN txn_cursor
		LOOP
			UPDATE t504_consignment
			   SET c504_void_fl = 'Y'
				 , c504_last_updated_by = p_user_id
				 , c504_last_updated_date = CURRENT_DATE
			 WHERE c504_consignment_id = txn_val.txn_id;
		END LOOP;
	END gm_sav_ma_dxn_void;
END gm_pkg_ac_ma;
/
