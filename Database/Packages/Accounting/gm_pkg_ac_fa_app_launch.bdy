create or replace
PACKAGE BODY gm_pkg_ac_fa_app_Launch
IS
--
  /****************************************************************
    * Description : Procedure to validate User
    * Author      : yogabalakrishnan
    * Modified By : 
    * Modified Dt : 09/06/2012
    *****************************************************************/

--
 PROCEDURE gm_fch_auditors_list
  (
    p_in_user_fst_letter  IN CHAR,
    p_in_user_last_name   IN VARCHAR,
    p_in_user_group_names IN VARCHAR,
    p_out_grp_map_list OUT TYPES.cursor_type,
    p_out_group_list OUT TYPES.cursor_type,
    p_out_user_list OUT TYPES.cursor_type,
    p_out_cur_user_id OUT TYPES.cursor_type
   )
AS
  v_cur_user_id  INTEGER;
  
BEGIN
  v_cur_user_id := -1;
  
  BEGIN
    SELECT t101.c101_user_id
    INTO v_cur_user_id
      FROM t101_user t101,
         t1501_group_mapping t1501,
         t1500_group t1500
     WHERE t101.c101_party_id  = t1501.c101_mapped_party_id
      AND UPPER(SUBSTR( t101.c101_user_f_name,1,1)) = UPPER(p_in_user_fst_letter)
      AND UPPER(t101.c101_user_l_name)              = UPPER(p_in_user_last_name)
      AND t1500.c1500_group_id              = t1501.c1500_group_id 
      AND UPPER(t1500.c1500_group_id)              = UPPER(p_in_user_group_names);
  
  EXCEPTION
    WHEN NO_DATA_FOUND
      THEN
        null;
  END;
  
   OPEN p_out_user_list
      FOR 
        SELECT t101.c101_user_id ,
            t101.c101_user_sh_name ,
            t101.c101_user_f_name ,
            t101.c101_user_l_name ,
            t101.c101_email_id ,
            t101.c101_dept_id ,
            t101.c101_access_level_id ,
            t101.c101_hire_dt ,
            t101.c101_termination_date ,
            t101.c101_work_phone ,
            t101.c101_created_date ,
            t101.c101_created_by ,
            t101.c101_last_updated_date ,
            t101.c101_last_updated_by ,
            t101.c101_ssn_number ,
            t101.c101_other_phone ,
            t101.c101_ph_extn ,
            t101.c701_distributor_id ,
            t101.c101_address ,
            t101.c901_user_status ,
            t101.c901_user_type ,
            t101.c101_party_id ,
            t101.c101_title ,
            t101.c101_date_format 
          FROM t101_user t101, t1500_group t1500 ,    t1501_group_mapping t1501 
         WHERE t1501.c101_mapped_party_id = t101.c101_party_id 
          AND t1501.c1500_group_id = t1500.c1500_group_id 
          AND t1501.c1500_group_id IN (p_in_user_group_names)
          AND v_cur_user_id>0;
               
        OPEN p_out_grp_map_list
      FOR
        SELECT t1501.C1501_GROUP_MAPPING_ID,
            t1501.c1500_group_id ,
            t1501.c101_mapped_party_id ,
            t1501.c1500_mapped_group_id ,
            t1501.c1501_created_by ,
            t1501.c1501_created_date ,
            t1501.c1501_last_updated_by ,
            t1501.c1501_last_updated_date ,
            t1501.c1501_void_fl 
        FROM t1501_group_mapping t1501, t101_user t101 
       WHERE t1501.c101_mapped_party_id = t101.c101_party_id
        AND t1501.c1500_group_id IN (p_in_user_group_names)
        AND v_cur_user_id > 0;
        
    OPEN p_out_group_list
      FOR 
        SELECT t1500.c1500_group_id ,
            t1500.c1500_group_nm ,
            t1500.c901_group_type ,
            t1500.c1500_void_fl ,
            t1500.c1500_last_updated_date ,
            t1500.c1500_last_updated_by ,
            t1500.c1500_group_desc ,
            t1500.c1500_created_date ,
            t1500.c1500_created_by 
          FROM  t1500_group t1500 
        WHERE t1500.c1500_group_id IN (p_in_user_group_names)
        AND v_cur_user_id > 0;
        
         OPEN p_out_cur_user_id
      FOR 
        SELECT c101_user_id 
          FROM t101_user
        WHERE c101_user_id = v_cur_user_id;
          
END gm_fch_auditors_list ;
  
END gm_pkg_ac_fa_app_Launch;
/