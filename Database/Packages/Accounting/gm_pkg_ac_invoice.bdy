/* Formatted on 2009/09/17 10:48 (Formatter Plus v4.8.0) */
-- @"C:\database\Packages\Accounting\gm_pkg_ac_invoice.bdy"
CREATE OR REPLACE
PACKAGE BODY gm_pkg_ac_invoice
IS
    --
    /******************************************************************
    * Description : Procedure to fetch invoice source
    ****************************************************************/
PROCEDURE gm_op_fch_invoice_source (
        p_invoiceid IN t503a_invoice_txn.c503_invoice_id%TYPE,
        p_recordset OUT TYPES.cursor_type)
AS
BEGIN
    OPEN p_recordset FOR SELECT c901_invoice_source invsrc, c901_invoice_type invtyp,c101_dealer_id DEALERNAME,C704_ACCOUNT_ID ACCID FROM t503_invoice WHERE
    c503_invoice_id = p_invoiceid;
END gm_op_fch_invoice_source;	
--
/*******************************************************
* Purpose: This Procedure is used to fetch
* Consignment Id From Invoice
*******************************************************/
--
PROCEDURE gm_op_fch_csg_from_invoice (
        p_invoiceid IN t503a_invoice_txn.c503_invoice_id%TYPE,
        p_refid_cur OUT t503a_invoice_txn.c503a_ref_id%TYPE)
AS
BEGIN
     SELECT c503a_ref_id
       INTO p_refid_cur
       FROM t503a_invoice_txn
      WHERE c503_invoice_id = p_invoiceid;
END gm_op_fch_csg_from_invoice;
/******************************************************************
* Description : Procedure to fetch gm_report_account_bytype
****************************************************************/
PROCEDURE gm_op_fch_account_bytype (
        p_accounttypes IN VARCHAR2,
        p_recordset OUT TYPES.cursor_type)
AS
    /*
    Description  :This procedure is called for Project Reporting
    Parameters   :p_column
    :p_recordset
    */
v_company_id        t1900_company.c1900_company_id%TYPE;
v_comp_lang_id T1900_COMPANY.C901_LANGUAGE_ID%TYPE;
BEGIN
	SELECT get_compid_frm_cntx() INTO v_company_id  FROM dual;
	SELECT get_complangid_frm_cntx() INTO  v_comp_lang_id FROM DUAL;
    my_context.set_my_inlist_ctx (p_accounttypes) ;
    OPEN p_recordset FOR SELECT c704_account_id ID,
    DECODE(v_comp_lang_id,'103097',NVL(c704_account_nm_en,c704_account_nm),c704_account_nm) NAME,
    c703_sales_rep_id repid,
    get_dist_rep_name (c703_sales_rep_id) dname,
    get_rep_name (c703_sales_rep_id) rname,
    get_account_gpo_id (c704_account_id) gpoid,
    get_distributor_id (c703_sales_rep_id) did FROM t704_account WHERE t704_account.c901_account_type IN
    (
         SELECT token FROM v_in_list
    )
    AND (c901_ext_country_id IS NULL OR c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH 
    AND C1900_COMPANY_ID =v_company_id 
    ORDER BY UPPER(NAME);
END gm_op_fch_account_bytype;
/*******************************************************
* Purpose: Function to fetch the customerPO number when InvoiceId is passed
*******************************************************/
FUNCTION get_cust_po_num (
        p_invoice_id IN t503_invoice.c503_invoice_id%TYPE)
    RETURN VARCHAR2
IS
    v_name t503_invoice.c503_customer_po%TYPE;
BEGIN
    BEGIN
         SELECT c503_customer_po
           INTO v_name
           FROM t503_invoice t503
          WHERE c503_invoice_id = p_invoice_id
            AND c503_void_fl   IS NULL;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN '';
    END;
    RETURN v_name;
END get_cust_po_num;
/*******************************************************
* Purpose: Function to fetch the InvoiceId for a given ConsignmentID
*******************************************************/
FUNCTION get_invoiceidfromconsigid (
        p_ref_id IN t504_consignment.c504_consignment_id%TYPE)
    RETURN VARCHAR2
IS
    v_name t503_invoice.c503_invoice_id%TYPE;
BEGIN
    BEGIN
         SELECT DECODE (dtype.c901_distributor_type, 70103, ict.invoiceid, fd.invoiceid) invoiceid
           INTO v_name
           FROM
            (
                 SELECT t701.c901_distributor_type, t504.c504_consignment_id
                   FROM t504_consignment t504, t701_distributor t701
                  WHERE t504.c504_consignment_id  = p_ref_id
                    AND t504.c701_distributor_id  = t701.c701_distributor_id
                    AND (t701.c901_ext_country_id IS NULL OR t701.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH
            )
            dtype, (
                 SELECT 'FD' TYPE, c503_invoice_id invoiceid, c503a_ref_id c504_consignment_id
                   FROM t503a_invoice_txn
                  WHERE c503a_ref_id  = p_ref_id
                    AND c901_ref_type = '50258'
            )
            fd, (
                 SELECT 'ICT' TYPE, t501.c503_invoice_id invoiceid, t504c.c504_consignment_id
                   FROM t501_order t501, t504c_consignment_ict_detail t504c
                  WHERE t504c.c504_consignment_id = p_ref_id
                    AND t504c.c501_order_id       = t501.c501_order_id
                    AND (t501.c901_ext_country_id IS NULL OR t501.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH
                    AND NVL(t501.C901_ORDER_TYPE,-9999) = 2533
            )
            ict
          WHERE dtype.c504_consignment_id = ict.c504_consignment_id
            AND dtype.c504_consignment_id = fd.c504_consignment_id(+);
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN '';
    END;
    RETURN v_name;
END get_invoiceidfromconsigid;
/******************************************************************
* Description : Procedure to fetch  account activity report
****************************************************************/
PROCEDURE gm_fch_activity_rpt (
        p_inv_source IN t503_invoice.c901_invoice_source%TYPE,
        p_acc_ids    IN VARCHAR2,
        p_inv_types  IN VARCHAR2,
        p_status     IN VARCHAR2,
        p_frm_dt     IN t503_invoice.c503_invoice_date%TYPE,
        p_to_dt      IN t503_invoice.c503_invoice_date%TYPE,
        p_str_opt    IN VARCHAR2,
        p_ar_currency IN t704_account.C901_CURRENCY %TYPE,
        p_recordset OUT TYPES.cursor_type,
        p_header_details OUT TYPES.cursor_type)
AS
    v_account  VARCHAR2 (400) ;
    v_bill_add VARCHAR2 (4000) ;
    v_pay_term VARCHAR2 (2000) ;
    v_company  VARCHAR2 (200) ;
    v_curr_sym VARCHAR2 (200) ;
    v_lang_id  VARCHAR2 (200) ;
    v_status   VARCHAR2 (200) ;
    v_icsadd   VARCHAR2 (4000) ;
    v_company_id  t1900_company.c1900_company_id%TYPE;
    v_division_id t1910_division.c1910_division_id%TYPE;
    v_ar_currency t704_account.C901_CURRENCY %TYPE;
    --
BEGIN
	
	SELECT  get_compid_frm_cntx()
	INTO	v_company_id
	FROM	dual;
	
	SELECT  get_complangid_frm_cntx()
	INTO	v_lang_id
	FROM	dual;
	
	v_ar_currency := p_ar_currency;
    -- to store the invoice type
    my_context.set_my_inlist_ctx (p_inv_types) ;
    -- to store the selected account
    my_context.set_my_double_inlist_ctx (p_acc_ids, '') ;
    -- to store the status in my context
    v_status      := '0,1,2,';
    IF p_status    = '1' THEN
        v_status  := '0,1,';
    ELSIF p_status = '2' THEN
        v_status  := '2,';
    END IF;
    --
    
    my_context.set_my_cloblist (v_status) ;
    -- fetch the report
    -- If the invoice source is Dealer(26240213) , then fetching the details based on dealer, else fetching based on Account 
    IF p_inv_source ='26240213'
    THEN
    
    	OPEN p_recordset FOR 
    	
    	SELECT t503.c503_invoice_id INVID,
	    t503.c503_invoice_date INVDT,
	    t503.c503_customer_po CUSTPO,
	    t101.c101_party_id ACCID,
	    DECODE (v_lang_id,'103097',NVL(c101_party_nm_en,c101_party_nm),c101_party_nm) NAME,
	    NVL (t503.C503_INV_AMT, 0) INVAMT,
	    NVL (t503.C503_INV_PYMNT_AMT, 0) INVPAID,
	    NVL (t503.C503_INV_AMT, 0) - NVL (t503.C503_INV_PYMNT_AMT, 0)  OUTAMT,
	    get_acct_curr_symb_by_dealer(t101.c101_party_id) CURRENCY,t503.c901_invoice_type INVTYPE 
	    FROM T503_INVOICE t503, t101_party t101
	    WHERE t503.c901_invoice_source = p_inv_source 
	    AND t503.c503_status_fl   IN
	    (
	         SELECT token FROM v_clob_list WHERE token IS NOT NULL
	    )
	    AND t503.c101_dealer_id IN
	    (
	         SELECT token FROM v_double_in_list WHERE token IS NOT NULL
	    )
	    AND t503.c901_invoice_type IN
	    (
	         SELECT token FROM v_in_list
	    )
	    AND TRUNC (t503.c503_invoice_date) >= p_frm_dt AND TRUNC (t503.c503_invoice_date) <= p_to_dt
	    AND t503.c1900_company_id = v_company_id
	    AND t503.c101_dealer_id = t101.c101_party_id
	    AND t101.c101_void_fl   IS NULL
	    AND t503.c503_void_fl IS NULL ORDER BY INVDT DESC;
    
    
    ELSE
    
	    OPEN p_recordset FOR 
	    
	    SELECT t503.c503_invoice_id INVID,
	    t503.c503_invoice_date INVDT,
	    t503.c503_customer_po CUSTPO,
	    NVL (t503.c704_account_id, t503.c701_distributor_id) ACCID,
	    DECODE (t503.c704_account_id, NULL, get_distributor_name (t503.c701_distributor_id), GET_ACCOUNT_NAME (
	    t503.C704_ACCOUNT_ID)) NAME,
	    NVL (t503.C503_INV_AMT, 0) INVAMT,
	    NVL (t503.C503_INV_PYMNT_AMT, 0) INVPAID,
	    NVL (t503.C503_INV_AMT, 0) - NVL (t503.C503_INV_PYMNT_AMT, 0)
	    OUTAMT,
	    DECODE (t503.C901_INVOICE_SOURCE, 50256, get_rule_value (50218, get_distributor_inter_party_id (
	    t503.c701_distributor_id)), 50253, get_rule_value (50218, get_distributor_inter_party_id (t503.c701_distributor_id)
	    ), 50257, get_rule_value (50218, get_distributor_inter_party_id (get_account_dist_id (t503.c704_account_id))),
	    get_code_name(t704.C901_CURRENCY)) CURRENCY,t503.c901_invoice_type INVTYPE FROM T503_INVOICE t503, t704_account t704 
	    WHERE t503.c901_invoice_source = p_inv_source 
	    AND t503.c503_status_fl   IN
	    (
	         SELECT token FROM v_clob_list WHERE token IS NOT NULL
	    )
	    AND DECODE (t503.c901_invoice_source, 50256, t503.c701_distributor_id, 
	    t503.c704_account_id) IN--The changes made for PMT-834 We need to pass Account Id ,since the dropdown is changed to List Accounts.
	    (
	         SELECT token FROM v_double_in_list WHERE token IS NOT NULL
	    )
	    AND t503.c901_invoice_type IN
	    (
	         SELECT token FROM v_in_list
	    )
	    AND TRUNC (t503.c503_invoice_date) >= p_frm_dt AND TRUNC (t503.c503_invoice_date) <= p_to_dt
	    AND t503.c1900_company_id = v_company_id
	    AND t503.c704_account_id = t704.c704_account_id
	    --50255=company customers only we are going filter based on currecny
	    AND DECODE (t503.c901_invoice_source, 50255,t704.C901_CURRENCY,'-999') = DECODE (t503.c901_invoice_source, 50255,v_ar_currency,'-999')
	    AND t503.c503_void_fl IS NULL ORDER BY INVDT DESC;
    
    END IF;
    
    -- if stropt is Print then only fetch the header details. otherwise return the empty cursor
    IF p_str_opt = 'Print' THEN
        -- to get the first selected account ids
         SELECT SUBSTR (p_acc_ids, INSTR (p_acc_ids, ',', - 1) + 1, LENGTH (p_acc_ids))
           INTO v_account
           FROM dual;
           -- to get the bill address, payment terms, company id and currency format
        BEGIN
	        
           
             SELECT DECODE (p_inv_source, 50253, GET_BILL_ADD (v_account), 50256, get_dist_bill_add (v_account), 26240213,GET_DEALER_BILL_ADD (v_account), GET_BILL_ADD (v_account)) BILLADD
                , DECODE(p_inv_source, 26240213, get_code_name (get_dealer_pay_term (v_account)) ,get_code_name (get_account_pay_term (v_account))) PAYTERM
                , DECODE (p_inv_source, 50253, get_compid_from_distid (v_account), 50256, get_compid_from_distid (v_account),get_ac_company_id (v_account)) companyid
                , DECODE (p_inv_source, 50256, get_rule_value (50218,get_distributor_inter_party_id (v_account)), 50253, get_rule_value (50218,
                GET_DISTRIBUTOR_INTER_PARTY_ID (GET_ACCOUNT_DIST_ID (v_account))), 50257, get_rule_value (50218,
                get_distributor_inter_party_id (get_account_dist_id (v_account))), get_comp_curr_symbol (v_company_id)) cursymbol
                , DECODE( p_inv_source,'50253',GET_RULE_VALUE ('50217',GET_DISTRIBUTOR_INTER_PARTY_ID (GET_ACCOUNT_DIST_ID (v_account))),'') icscompadd
                ,  DECODE( p_inv_source,26240213,get_rep_div_id_by_dealer(v_account),gm_pkg_sm_mapping.get_salesrep_division(get_rep_id(v_account)))
               INTO v_bill_add, v_pay_term, v_company
              , v_curr_sym,v_icsadd
              , v_division_id
               FROM dual;
               
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            IF p_inv_source = '50255' OR p_inv_source = '50257' -- US/ ICA
                THEN
                 SELECT get_bill_add (v_account), get_ac_company_id (v_account), get_comp_curr_symbol (v_company_id), get_code_name (get_account_pay_term (v_account))
                   INTO v_bill_add, v_company, v_curr_sym
                  , v_pay_term
                   FROM dual;
                   
            ELSIF  p_inv_source = '26240213'
            THEN    
                 SELECT GET_DEALER_BILL_ADD (v_account), get_comp_curr_symbol (v_company_id), get_code_name (get_dealer_pay_term (v_account))
                   INTO v_bill_add, v_curr_sym
                  , v_pay_term
                   FROM dual;
                
                
            ELSE
                 SELECT get_dist_bill_add (v_account), get_compid_from_distid (v_account), get_rule_value (50218,
                    get_distributor_inter_party_id (v_account)), get_code_name (get_account_pay_term (v_account))
                   INTO v_bill_add, v_company, v_curr_sym
                  , v_pay_term
                   FROM dual;
            END IF;
        END;
        OPEN p_header_details FOR SELECT v_bill_add billadd,
        v_icsadd icscompadd,
        v_pay_term payterm,
        v_company companyid,
        v_curr_sym cursymbol,
        SUM (NVL (t503.C503_INV_AMT, 0) - NVL (t503.C503_INV_PYMNT_AMT, 0)) TOTOSDAMT
        , v_division_id  division_id
        FROM t503_invoice t503 WHERE t503.c901_invoice_source = p_inv_source AND
        t503.c503_status_fl                                                                       IN
        (
             SELECT token FROM v_clob_list WHERE token IS NOT NULL
        )
        AND DECODE (t503.c901_invoice_source, 50256, t503.c701_distributor_id,26240213,t503.c101_dealer_id,
        t503.c704_account_id) IN
        (
             SELECT token FROM v_double_in_list WHERE token IS NOT NULL
        )
        AND t503.c901_invoice_type IN
        (
             SELECT token FROM v_in_list
        )
        AND TRUNC (t503.c503_invoice_date) >= p_frm_dt AND TRUNC (t503.c503_invoice_date) <= p_to_dt
        AND t503.c503_void_fl                  IS NULL;
    ELSE
        OPEN p_header_details FOR SELECT * FROM DUAL WHERE 1 = 0;
    END IF;
END gm_fch_activity_rpt;


/*********************************************************************************
 * Description : This procedure is called to fetch invoice id for Cash Application.
 * Author 	   : Velu
 * Parameters  : Invoice ID.
 **********************************************************************************/
PROCEDURE gm_fch_invoice (
		p_inv_id  IN   t503_invoice.c503_invoice_id%TYPE
	  , p_out	  OUT  TYPES.cursor_type
)AS
v_company_id        t1900_company.c1900_company_id%TYPE;
v_lang_id 	t1900_company.c901_language_id%TYPE;
v_date_format varchar2(20);
v_inv_src t503_invoice.c901_invoice_source%TYPE;
BEGIN
	
	SELECT c1900_company_id 
	  INTO v_company_id 
	  FROM t503_invoice
	 WHERE c503_invoice_id = p_inv_id
	   AND  c503_void_fl IS NULL;
	 
	SELECT c901_language_id
	  INTO v_lang_id
	  FROM t1900_company
	 WHERE c1900_company_id = v_company_id
	   AND c1900_void_fl     IS NULL;
	  
	SELECT get_compdtfmt_frm_cntx() 
	  INTO v_date_format 
	  from dual;
	  
	  
	SELECT c901_invoice_source
	  INTO v_inv_src
	  FROM t503_invoice
	 WHERE c503_invoice_id = p_inv_id
	   AND c503_void_fl     IS NULL;
	-- If the invoice source is Dealer(26240213) , then fetching the details based on dealer, else fetching based on Account 
	IF v_inv_src = '26240213'
	THEN
		OPEN p_out FOR
		
	SELECT inv_details.*,(inv_details.INVAMT - inv_details.AMT_REC) OUTAMT FROM (
		SELECT t503.c503_invoice_id INVID, DECODE (v_lang_id,'103097',NVL(c101_party_nm_en,c101_party_nm),c101_party_nm) ACCT_NAME, t101.c101_party_id ACC_ID  
			 , to_char(t503.c503_invoice_date,v_date_format) INV_DT, t503.c503_customer_po CUST_PO,
		 	get_acct_curr_symb_by_dealer(t101.c101_party_id) CURRENCY, NVL (t503.C503_INV_AMT, 0) INVAMT,
		 	NVL (t503.C503_INV_PYMNT_AMT, 0) AMT_REC,t503.c901_invoice_type invType,c503_status_fl statusfl,get_acct_curr_id_by_dealer(t101.c101_party_id) ARCURRID
    		, t901.c901_code_nm INV_SRC 
   		FROM t503_invoice t503, t101_party t101,t101_party_invoice_details t101p,t901_code_lookup t901
	  	WHERE t503.c503_invoice_id = p_inv_id
	  	AND t101.c101_party_id   = t503.c101_dealer_id
	  	AND t101p.c101_party_id = t101.c101_party_id
	  	AND t901.c901_code_id = t503.c901_invoice_source
	  	AND t503.C1900_COMPANY_ID  = v_company_id
    	AND t503.c503_void_fl   IS NULL
    	AND t101.c101_void_fl   IS NULL
		AND t101p.c101_void_fl IS NULL)inv_details;
	
	ELSE  
		OPEN p_out FOR
		
		SELECT inv_details.*,(inv_details.INVAMT - inv_details.AMT_REC) OUTAMT FROM (
			SELECT t503.c503_invoice_id INVID, DECODE (t503.c704_account_id, NULL, get_distributor_name (t503.c701_distributor_id), 
			DECODE(v_lang_id,'103097',NVL(C704_ACCOUNT_NM_EN,C704_ACCOUNT_NM),C704_ACCOUNT_NM)) ACCT_NAME, NVL (t503.c704_account_id, t503.c701_distributor_id) ACC_ID   
				 , to_char(t503.c503_invoice_date,v_date_format) INV_DT, t503.c503_customer_po CUST_PO,
			 	DECODE (t503.C901_INVOICE_SOURCE, 50256,get_rule_value (50218, get_distributor_inter_party_id (t503.c701_distributor_id)),
			 	50253, get_rule_value (50218,get_distributor_inter_party_id (t503.c701_distributor_id)), 50257,
			 	get_rule_value (50218,	get_distributor_inter_party_id (get_account_dist_id (t503.c704_account_id))),
			 	get_account_curr_symb(t503.c704_account_id)) CURRENCY, NVL (t503.C503_INV_AMT, 0) INVAMT,
			 	NVL (t503.C503_INV_PYMNT_AMT, 0) AMT_REC,t503.c901_invoice_type invType,c503_status_fl statusfl,t704.C901_CURRENCY ARCURRID
	    		, t901.c901_code_nm INV_SRC 
	   		FROM t503_invoice t503, T1900_COMPANY T1900, t704_account t704,t901_code_lookup t901
		  	WHERE t503.c503_invoice_id = p_inv_id
		  	AND t503.C1900_COMPANY_ID  = T1900.C1900_COMPANY_ID
		  	AND t901.c901_code_id = t503.c901_invoice_source
		  	AND t704.c704_account_id   = t503.c704_account_id
		  	AND t503.C1900_COMPANY_ID  = v_company_id
	    	AND t503.c503_void_fl   IS NULL
	    	AND t704.c704_void_fl   IS NULL)inv_details;
	END IF;
	    	
END gm_fch_invoice;

/*********************************************************************************
 * Description : This procedure is used to fetch invoice id for Issue Memo.
 * Author 	   : 
 * Parameters  : Invoice ID and Part number.
 **********************************************************************************/

PROCEDURE gm_validate_inv (
		p_inv_id   IN   t503_invoice.c503_invoice_id%TYPE
	  , p_num		  IN   t205_part_number.c205_part_number_id%TYPE
	  , p_out	 OUT	  TYPES.cursor_type
	)AS
	v_company_id        t1900_company.c1900_company_id%TYPE;
	v_lang_id 	t1900_company.c901_language_id%TYPE;
	v_inv_src t503_invoice.c901_invoice_source%TYPE;
	
	BEGIN
	
	SELECT c1900_company_id 
	  INTO v_company_id 
	  FROM t503_invoice
	 WHERE c503_invoice_id = p_inv_id
	  AND  c503_void_fl IS NULL;
	 
	SELECT c901_language_id
	  INTO v_lang_id
	  FROM t1900_company
	 WHERE c1900_company_id = v_company_id
	   AND c1900_void_fl     IS NULL;
	
		
		SELECT c901_invoice_source
	  	  INTO v_inv_src
	  	  FROM t503_invoice
	 	 WHERE c503_invoice_id = p_inv_id
	   	   AND c503_void_fl     IS NULL;
	-- If the invoice source is Dealer(26240213) , then fetching the details based on dealer, else fetching based on Account 
		
		IF v_inv_src = '26240213'
		THEN
			
			OPEN p_out FOR
				SELECT t503.c503_invoice_id INVID, t502.c205_part_number_id PartNUM, DECODE(v_lang_id,'103097',NVL(c101_party_nm_en,c101_party_nm),c101_party_nm) ACCNAME
				--BUG-6442
			  , NVL (c502_item_qty, 0) + GET_ORDER_PART_RTN_QTY(t502.C501_ORDER_ID, t502.C205_PART_NUMBER_ID, t502.C502_ITEM_PRICE, t502.c901_type) QTY
			  , t502.c502_item_price PRICE_EA
			  , t501.c101_dealer_id AccID, t503.c503_customer_po PO, t501.c501_order_id ORDERID, t502.c901_type ITYPE
			  , t503.c503_status_fl invstatus, t503.c901_invoice_type invtype, t502.c502_item_order_id itemorderid, get_acct_curr_id_by_dealer(t501.c101_dealer_id) ARCURRID 
			  , t901.c901_code_nm INV_SRC 
			   FROM t503_invoice t503, t501_order t501, t502_item_order t502, t101_party t101,t901_code_lookup t901
			  WHERE t503.c503_invoice_id     = t501.c503_invoice_id
			    AND t501.c501_order_id       = t502.c501_order_id
			    AND t101.c101_party_id       = t503.c101_dealer_id
			    AND t901.c901_code_id        = t503.c901_invoice_source
			    AND t503.c503_invoice_id     = p_inv_id
			    AND t502.c205_part_number_id = p_num
			    AND NVL(t501.C901_order_type,-9999) <> 2535  -- Sales Discount
			    AND NVL(t501.C901_order_type,-9999) <> 2529  -- Sales Adjustment
			    AND NVL (t501.c901_order_type, - 9999) NOT IN
				(
			     SELECT t906.c906_rule_value
			       FROM t906_rules t906
			      WHERE t906.c906_rule_grp_id = 'ORDTYPE'
			        AND c906_rule_id          = 'EXCLUDE'
				)
			    AND t503.c503_void_fl       IS NULL
			    AND t501.c501_void_fl       IS NULL
			    AND t502.c502_void_fl       IS NULL
			    AND t101.c101_void_fl       IS NULL
			    AND t502.C502_DELETE_FL     IS NULL
			    AND t501.C1900_COMPANY_ID = v_company_id;
				
		ELSE
			OPEN p_out FOR
				SELECT t503.c503_invoice_id INVID, t502.c205_part_number_id PartNUM, get_account_name (t501.c704_account_id) ACCNAME
				--BUG-6442
			  , NVL (c502_item_qty, 0) + GET_ORDER_PART_RTN_QTY(t502.C501_ORDER_ID, t502.C205_PART_NUMBER_ID, t502.C502_ITEM_PRICE, t502.c901_type) QTY
			  , t502.c502_item_price PRICE_EA
			  , t501.c704_account_id AccID, t503.c503_customer_po PO, t501.c501_order_id ORDERID, t502.c901_type ITYPE
			  , t503.c503_status_fl invstatus, t503.c901_invoice_type invtype, t502.c502_item_order_id itemorderid, t704.C901_CURRENCY ARCURRID 
			  , t901.c901_code_nm INV_SRC 
			   FROM t503_invoice t503, t501_order t501, t502_item_order t502, t704_account t704,t901_code_lookup t901
			  WHERE t503.c503_invoice_id     = t501.c503_invoice_id
			    AND t501.c501_order_id       = t502.c501_order_id
			    AND t901.c901_code_id        = t503.c901_invoice_source
			    AND t704.c704_account_id     = t503.c704_account_id
			    AND t503.c503_invoice_id     = p_inv_id
			    AND t502.c205_part_number_id = p_num
			    AND NVL(t501.C901_order_type,-9999) <> 2535  -- Sales Discount
			    AND NVL(t501.C901_order_type,-9999) <> 2529  -- Sales Adjustment
			    AND NVL (t501.c901_order_type, - 9999) NOT IN
				(
			     SELECT t906.c906_rule_value
			       FROM t906_rules t906
			      WHERE t906.c906_rule_grp_id = 'ORDTYPE'
			        AND c906_rule_id          = 'EXCLUDE'
				)
			    AND t503.c503_void_fl       IS NULL
			    AND t501.c501_void_fl       IS NULL
			    AND t502.c502_void_fl       IS NULL
			    AND t704.c704_void_fl       IS NULL
			    AND t502.C502_DELETE_FL     IS NULL
			    AND t501.C1900_COMPANY_ID = v_company_id;
		END IF;
END gm_validate_inv;

/*********************************************************************************
 * Description : This procedure is used to create the issue memo
 * Author 	   : 
 * Parameters  : Invoice Type, Input string, and User id.
 **********************************************************************************/
  PROCEDURE gm_sav_issue_memo_dtls (
        p_inv_type IN VARCHAR2,
        p_inputstr IN CLOB,
        p_userid   IN t503_invoice.c503_last_updated_by%TYPE,
        p_invoice_ids OUT VARCHAR2,
        p_order_ids OUT VARCHAR2)
	AS
    v_strlen NUMBER := NVL (LENGTH (p_inputstr), 0) ;
    v_string CLOB   := p_inputstr;
    v_substring VARCHAR2 (4000) ;
    --
    v_invoice_id t503_invoice.c503_invoice_id%TYPE;
    v_accid t503_invoice.c704_account_id%TYPE;
    v_part_str VARCHAR2 (4000) ;
    v_comment_str VARCHAR2 (4000);
    v_customer_po t503_invoice.c503_customer_po%TYPE;
    --
    v_new_invoice_id t503_invoice.c503_invoice_id%TYPE;
    v_new_order_id t501_order.c501_order_id%TYPE;
	BEGIN
    IF v_strlen                     <> 0 THEN
        WHILE INSTR (v_string, '!!') <> 0
        LOOP
            v_substring := SUBSTR (v_string, 1, INSTR (v_string, '!!') - 1) ;
            v_string    := SUBSTR (v_string, INSTR (v_string, '!!')    + 2) ;
            --
            v_invoice_id  := NULL;
            v_accid       := NULL;
            v_part_str    := NULL;
            v_customer_po := NULL;
            v_comment_str := NULL;
            --
            v_invoice_id  := SUBSTR (v_substring, 1, INSTR (v_substring, '[') - 1) ;
            v_substring   := SUBSTR (v_substring, INSTR (v_substring, '[')    + 1) ;
            v_accid       := SUBSTR (v_substring, 1, INSTR (v_substring, '[') - 1) ;
            v_substring   := SUBSTR (v_substring, INSTR (v_substring, '[')    + 1) ;
            v_customer_po    := SUBSTR (v_substring, 1, INSTR (v_substring, '[') - 1) ;
            v_substring   := SUBSTR (v_substring, INSTR (v_substring, '[')    + 1) ;
            v_part_str    := SUBSTR (v_substring, 1, INSTR (v_substring, '[') - 1) ; 
            v_substring   := SUBSTR (v_substring, INSTR (v_substring, '[')    + 1);
            v_comment_str := v_substring;
            --
            gm_save_credit_sales (v_invoice_id, v_accid, v_part_str, v_comment_str, p_userid, v_customer_po, p_inv_type, v_new_order_id, v_new_invoice_id) ;
            --
            p_invoice_ids := p_invoice_ids || v_new_invoice_id || ', ';
            p_order_ids   := p_order_ids || v_new_order_id || ', ';
        END LOOP;
        -- remove the last comma (Invoices)
        IF p_invoice_ids IS NOT NULL
        THEN
        	p_invoice_ids := substr(p_invoice_ids, 0, LENGTH(p_invoice_ids)-2);
        END IF;
        -- remove the last comma (Orders)
        IF p_order_ids IS NOT NULL
        THEN
        	p_order_ids := substr(p_order_ids, 0, LENGTH(p_order_ids)-2);
        END IF;
    END IF;
END gm_sav_issue_memo_dtls;

/*********************************************************************************
 * Description : This procedure is used to fetch the Invoice Header Details
 * Author 	   : HReddi 
 * Parameters  : Invoice Source, Account ID, and Status of Invoice.
 **********************************************************************************/
	
   PROCEDURE gm_fch_inv_header_dtls(
	 p_inv_source   	IN  VARCHAR2,
	 p_account_id    	IN  VARCHAR2,
	 p_status      		IN  VARCHAR2,	
	 p_out_header_dtls  OUT TYPES.cursor_type
	)
	AS
	BEGIN
		 my_context.set_my_inlist_ctx (p_status) ;
		 OPEN p_out_header_dtls FOR 
		 	SELECT DECODE (t503.c901_invoice_source, 50256, get_distributor_name (t503.c701_distributor_id)
		 	                                       , 50253,get_distributor_name (t503.c701_distributor_id)
		 	                                       , get_account_name (t503.c704_account_id)) ACCTNM
		 	     , DECODE (t503.c901_invoice_source, 50256, t503.c701_distributor_id
		 	                                       , 50253, t503.c701_distributor_id
		 	                                       , t503.c704_account_id) ACCTID
		 	     , DECODE (t503.c901_invoice_source, 50256, get_rule_value (50218, get_distributor_inter_party_id (t503.c701_distributor_id))
		 	                                       , 50253, get_rule_value (50218, get_distributor_inter_party_id (t503.c701_distributor_id))
		 	                                       , 50257, get_rule_value (50218, get_distributor_inter_party_id (get_account_dist_id (t503.c704_account_id)))
		 	                                       , get_comp_curr_symbol(t503.c1900_company_id)) CURRENCY
                 , DECODE (t503.c901_invoice_source, 50256, get_dist_bill_add (t503.c701_distributor_id)
                                                   , 50253, get_dist_bill_add (t503.c701_distributor_id)
                                                   , get_bill_add (t503.c704_account_id)) BILLADD
                 , get_code_name (get_account_pay_term (t503.c704_account_id)) PAYTERMS
                 , CURRENT_DATE REPORTDT
                 , DECODE (t503.c901_invoice_source, 50256, get_compid_from_distid (t503.c701_distributor_id)
                                                   , get_ac_company_id (t503.c704_account_id)) COMPANYID
                 , t503.c1910_division_id division_id                                  
   		      FROM t503_invoice t503
             WHERE t503.c901_invoice_source = p_inv_source
               AND DECODE (t503.c901_invoice_source, 50256, t503.c701_distributor_id
                                                    , t503.c704_account_id) = p_account_id
    		   AND t503.c503_status_fl      IN (SELECT token FROM v_in_list)
    		   AND t503.c503_void_fl        IS NULL
               AND (t503.c901_ext_country_id IS NULL OR t503.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH
          GROUP BY t503.c704_account_id, t503.c701_distributor_id, t503.c901_invoice_source, t503.c1910_division_id, t503.c1900_company_id;
	END gm_fch_inv_header_dtls;
	
	/*********************************************************************************
 * Description : This procedure is used to Update the Invoice Status as Closed.
 * Author 	   : Manikandan Rajasekaran 
 * Parameters  : Invoice Source,Invoice Id,User Id.
 **********************************************************************************/
	
   PROCEDURE gm_upd_invoice_status(
	 p_inv_source   	IN  t503_invoice.C901_INVOICE_SOURCE%TYPE,
	 p_invoice_id    	IN  t503_invoice.c503_invoice_id%TYPE,
	 p_user_id          IN t503_invoice.c503_last_updated_by%TYPE
	)
	AS
	v_invoice_amt NUMBER(15,2);
	BEGIN	
		select C503_INV_AMT into v_invoice_amt
		from t503_invoice
		where C503_INVOICE_ID = p_invoice_id;
	IF ((p_inv_source = 50255 OR p_inv_source =50253 OR p_inv_source = 26240213) AND v_invoice_amt = 0 )
	--50255 US Accounts.
	--50253 ICS Distributors.
	THEN 
		UPDATE t503_invoice  
		SET  c503_status_fl = 2 
			,C503_LAST_UPDATED_DATE =CURRENT_DATE
			,C503_LAST_UPDATED_BY = p_user_id
		WHERE c503_invoice_id = p_invoice_id; 
	END IF;
	END gm_upd_invoice_status;
	
/*********************************************************************************
* Description : This procedure is used to fetch the company invoice details.
* Author     : Manikandan
* Parameters  : Invoice Id
**********************************************************************************/
PROCEDURE gm_company_invoice_details (
        p_invoice_id IN t503_invoice.c503_invoice_id%TYPE,
        p_out_invoice_dtls OUT TYPES.cursor_type)
AS
v_company_id        t1900_company.c1900_company_id%TYPE;
v_inv_src           t503_invoice.c901_invoice_source%TYPE;
BEGIN
	--
	SELECT get_compid_frm_cntx() 
      INTO v_company_id  
      FROM dual;
      
   SELECT c901_invoice_source
	  	  INTO v_inv_src
	  	  FROM t503_invoice
	 	 WHERE c503_invoice_id = p_invoice_id
	   	   AND c503_void_fl     IS NULL;
	-- If the invoice source is Dealer(26240213) , then fetching the details based on dealer, else fetching based on Account 
		
	IF v_inv_src = '26240213'
	THEN
	
	OPEN p_out_invoice_dtls FOR
    SELECT t503.c1900_company_id company_id, get_company_code (t503.c1900_company_id) comp_code, t503.c1910_division_id division_id 
    , t1900.c1900_company_name companyname
     FROM t503_invoice t503,t1900_company t1900
     WHERE c503_invoice_id = p_invoice_id
       AND t503.c1900_company_id = t1900.c1900_company_id
       AND t1900.c1900_void_fl IS NULL
       AND c503_void_fl IS NULL;	
	
	
	ELSE
    --  
    OPEN p_out_invoice_dtls FOR
    SELECT t503.c1900_company_id company_id, get_company_code (t503.c1900_company_id) comp_code, t503.c1910_division_id division_id 
    , CASE WHEN TRUNC (to_date (get_rule_value ('TAX_DATE', 'TAX_INVOICE'), 'mm/DD/YYYY')) <= TRUNC (gm_pkg_ac_sales_tax.get_min_orders_date (t503.c503_invoice_id)) THEN 'Y' ELSE 'N' END tax_date_fl
    , get_company_name (t503.c1900_company_id) companyname
    , get_rule_value_by_company(t704.c704_ship_country,'TAX_COUNTRY',v_company_id) taxable_country_fl
    , get_account_attrb_value (t704.c704_account_id, 92000) generate_xml 
     FROM t503_invoice t503, t704_account t704
     WHERE c503_invoice_id = p_invoice_id
     AND t503.c704_account_id = t704.c704_account_id;
     
    END IF;
END gm_company_invoice_details;

	/*********************************************************************************
	* Description : This procedure is used to fetch the Italy invoice header details.
	* Author     : Manikandan
	* Parameters  : Invoice Id
	**********************************************************************************/
PROCEDURE gm_fch_italy_inv_header_dtls (
        p_invoice_id IN t503_invoice.c503_invoice_id%TYPE,
        p_out_header_dtls OUT TYPES.cursor_type)
AS
    v_company_id t1900_company.c1900_company_id%TYPE;
BEGIN
     SELECT get_compid_frm_cntx () INTO v_company_id FROM dual;
    --
    OPEN p_out_header_dtls FOR SELECT get_rule_value_by_company ('TRANS_COUNTRY_ID', 'ITALY_XML_HEADER', v_company_id)
    TRANS_COUNTRY_ID,
    get_rule_value_by_company ('TRANS_ID_CODE', 'ITALY_XML_HEADER', v_company_id) TRANS_ID_CODE,
    -- 26230663 Codice IPA
    GET_ACCOUNT_ATTRB_VALUE (t704.c704_account_id, 26230663) TRANS_ADDRESS_CODE,
    get_rule_value_by_company ('TRANS_TELE_PH', 'ITALY_XML_HEADER', v_company_id) TRANS_TELE_PH,
    get_rule_value_by_company ('TRANS_EMAIL', 'ITALY_XML_HEADER', v_company_id) TRANS_EMAIL,
    get_rule_value_by_company ('SELLER_COUNTRY_ID', 'ITALY_XML_HEADER', v_company_id) SELLER_COUNTRY_ID,
    get_rule_value_by_company ('SELLER_ID_CODE', 'ITALY_XML_HEADER', v_company_id) SELLER_ID_CODE,
    get_rule_value_by_company ('SELLER_COMPANY_NM', 'ITALY_XML_HEADER', v_company_id) SELLER_COMPANY_NM,
    get_rule_value_by_company ('SELLER_ADDRESS', 'ITALY_XML_HEADER', v_company_id) SELLER_ADDRESS,
    get_rule_value_by_company ('SELLER_POST_CODE', 'ITALY_XML_HEADER', v_company_id) SELLER_POST_CODE,
    get_rule_value_by_company ('SELLER_CITY', 'ITALY_XML_HEADER', v_company_id) SELLER_CITY,
    get_rule_value_by_company ('SELLER_PROVINCIA', 'ITALY_XML_HEADER', v_company_id) SELLER_PROVINCIA,
    get_rule_value_by_company ('ADMIN_OFFICE', 'ITALY_XML_HEADER', v_company_id) ADMIN_OFFICE,
    get_rule_value_by_company ('ADMIN_COM_REG_NO', 'ITALY_XML_HEADER', v_company_id) ADMIN_COM_REG_NO,
    get_rule_value_by_company ('ADMIN_SHARE_CAPITAL', 'ITALY_XML_HEADER', v_company_id) ADMIN_SHARE_CAPITAL,
    get_rule_value_by_company ('THIRD_PARTY_COUNTRY_ID', 'ITALY_XML_HEADER', v_company_id) THIRD_PARTY_COUNTRY_ID,
    get_rule_value_by_company ('THIRD_PARTY_ID_CODE', 'ITALY_XML_HEADER', v_company_id) THIRD_PARTY_ID_CODE,
    get_rule_value_by_company ('THIRD_PARTY_COMPANY_NM', 'ITALY_XML_HEADER', v_company_id) THIRD_PARTY_COMPANY_NM,
    get_rule_value_by_company ('ITALY_IBAN_NUMBER', 'ITALY_XML_HEADER', v_company_id) COMPANY_IBAN
    ---- account info
    ,
    get_code_name_alt (t704.C704_BILL_COUNTRY) BUYER_COUNTRY_ID,
    get_account_attrb_value (t704.c704_account_id, 101185) BUYER_ID_CODE,
    t704.C704_BILL_NAME BUYER_COMPANY_NM,
    t704.C704_BILL_ADD1 || ' ' || t704.C704_BILL_ADD2 BUYER_ADDRESS,
    t704.C704_BILL_ZIP_CODE BUYER_POST_CODE,
    t704.C704_BILL_CITY BUYER_CITY,
    get_code_name_alt (t704.C704_BILL_STATE) BUYER_PROVINCIA,
    --get account CF Number
     get_account_attrb_value (t704.c704_account_id, 107345) BUYER_CF_NUMBER, 
    current_date + get_code_name_alt (t704.c704_payment_terms) pay_terms_date,
    t503.c503_invoice_id INVID,
    t503.c903_pdf_file_id PDFFILE,
    t503.c901_invoice_type inv_type,
    GET_ACCOUNT_ATTRB_VALUE (t704.c704_account_id, 1030452951) cust_type,
    get_account_attrb_value (t704.c704_account_id, 5504)  ACC_TYPE,
    --PC-2192 View Vendita Document And Vendita Field in Xml File
    gm_pkg_ac_italy_invoice.get_vendita_for_invoice(p_invoice_id,v_company_id) VENDITAID
    FROM t503_invoice t503, t704_account t704
    WHERE t704.c704_account_id = t503.c704_account_id
    AND t503.c503_invoice_id = p_invoice_id
    AND t503.c1900_company_id = v_company_id
    AND t503.c503_void_fl IS NULL;
END gm_fch_italy_inv_header_dtls;



 	/*********************************************************************************
	* Description : This procedure is used to check if the D order already exists
	*               If Exists, increment the order suffix and return the new order id
	* Author     :  gpalani
	* Parameters  : p_order_id, p_ord_suffix, p_increment, p_new_order_id
	**********************************************************************************/
PROCEDURE gm_chk_order (
        p_order_id     IN t501_order.c501_order_id%TYPE,
        p_ord_suffix   IN VARCHAR,
        p_increment    IN NUMBER,
        p_new_order_id OUT t501_order.c501_order_id%TYPE)

AS
    v_count NUMBER := 1;
    v_temp  NUMBER;
      
BEGIN
    v_temp         := p_increment;
   
    WHILE (V_COUNT >= 1)
   
    LOOP
         SELECT COUNT (1)
           INTO V_COUNT
           FROM t501_order
          WHERE c501_order_id = p_order_id||p_ord_suffix||v_temp;
          
        IF (V_COUNT          <> 0) THEN
        
            v_temp           := v_temp + 1;
        END IF;
    END LOOP;
    p_new_order_id := p_order_id||p_ord_suffix||v_temp;
    
END gm_chk_order;

	/************************************************************************************
	* Description : This procedure is used to fetch the cust defined paperwork details - based on invoice.
	* 
	* 				Procedure created for PMT-44586: Invoice paperwork changes - Vatican city changes
	* 				This is called from - all the invoice paperwork (gmInvoiceInforServlet)
	* 
	* Author      : Vinoth
	* Parameters  : Invoice Id
	**************************************************************************************/

PROCEDURE gm_fch_cust_invoice_paperwork_dtls (
        p_invoice_id IN t503_invoice.c503_invoice_id%TYPE,
        p_out_recordset OUT TYPES.cursor_type) 
        
AS
	v_party_id t704_account.C101_PARTY_ID%TYPE;
	v_company_id t704_account.c1900_company_id%TYPE;
	v_account_currency t704_account.c901_currency%TYPE;
BEGIN
	
  BEGIN	
	-- to get the party id from account
 	SELECT t704.c101_party_id,t704.c1900_company_id , t704.c901_currency
  	  INTO v_party_id,v_company_id, v_account_currency
   	  FROM t503_invoice t503, t704_account t704
     WHERE t503.c704_account_id = t704.c704_account_id
       AND t503.c503_invoice_id = p_invoice_id
       AND t503.c503_void_fl   IS NULL;
   
    EXCEPTION WHEN NO_DATA_FOUND
    THEN
    	v_party_id := NULL;
    END;
 
    -- based on party id to get the invoice template 
  
 	 OPEN p_out_recordset FOR
	SELECT t9101.c9101_portal_template_name portal_template,t9101.c9101_pdf_template_name pdf_template
	  FROM t9102_print_template_map t9102,
		   t9101_print_template t9101
	 WHERE t9101.c9101_print_template_id = t9102.c9101_print_template_id
	   AND (t9102.c101_party_id = v_party_id OR t9102.c101_party_id IS NULL)
	   AND (t9102.c901_currency = v_account_currency OR t9102.c901_currency IS NULL)
	   AND t9102.c1900_company_id = v_company_id
	   AND t9102.c9102_void_fl IS NULL
	   AND t9101.c9101_void_fl IS NULL
	 ORDER BY t9102.c101_party_id ASC,
		   t9102.c901_currency ASC,
		   t9102.c1900_company_id ASC;
  
    
END gm_fch_cust_invoice_paperwork_dtls;

/************************************************************************************
* Description : This procedure is used to update the invoice last payment received date
* PC-3144: AR summary report timout (to capture invoice received date)
* 
* Author      : mmuthusamy
* Parameters  : Invoice Id, Payment recieved date, user id
**************************************************************************************/
PROCEDURE gm_upd_inv_last_payment_received_date (
        p_invoice_id            IN t503_invoice.c503_invoice_id%TYPE,
        p_payment_received_date IN t503_invoice.c503_last_payment_received_date%TYPE,
        p_user_id               IN t503_invoice.c503_last_updated_by%TYPE)
AS
BEGIN
	--
     UPDATE t503_invoice
    SET c503_last_payment_received_date = p_payment_received_date, c503_last_updated_by = p_user_id,
        c503_last_updated_date          = CURRENT_DATE
      WHERE c503_invoice_id             = p_invoice_id ;
      
END gm_upd_inv_last_payment_received_date;

/************************************************************************************
* Description : This procedure is used to update the dealer last payment received date
* PC-3144: AR summary report timout (to capture invoice received date)
* 
* Author      : mmuthusamy
* Parameters  : Dealer id, Payment recieved date, user id
**************************************************************************************/

PROCEDURE gm_upd_dealer_last_payment_received_date (
        p_dealer_id             IN t101_party_invoice_details.c101_party_id%TYPE,
        p_payment_received_date IN t101_party_invoice_details.c101_last_payment_received_date%TYPE,
        p_user_id               IN t101_party_invoice_details.c101_last_updated_by%TYPE)
AS
BEGIN
	--
     UPDATE t101_party_invoice_details
    SET c101_last_payment_received_date = p_payment_received_date, c101_last_updated_by = p_user_id,
        c101_last_updated_date          = CURRENT_DATE
      WHERE c101_party_id               = p_dealer_id ;
END gm_upd_dealer_last_payment_received_date;

/************************************************************************************
* Description : This procedure is used to update the Account last payment received date
* PC-3144: AR summary report timout (to capture invoice received date)
* 
* Author      : mmuthusamy
* Parameters  : Account Id, Payment recieved date, user id
**************************************************************************************/

PROCEDURE gm_upd_account_last_payment_received_date (
        p_account_id            IN t704_account.c704_account_id%TYPE,
        p_payment_received_date IN t704_account.c704_last_payment_received_date%TYPE,
        p_user_id               IN t704_account.c704_last_updated_by%TYPE)
AS
BEGIN
	--
     UPDATE t704_account
    SET c704_last_payment_received_date = p_payment_received_date, c704_last_updated_by = p_user_id,
        c704_last_updated_date          = CURRENT_DATE
      WHERE c704_account_id             = p_account_id ;
      
END gm_upd_account_last_payment_received_date;

END gm_pkg_ac_invoice;
/

 			