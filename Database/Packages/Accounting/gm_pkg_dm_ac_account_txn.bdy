/* Formatted on 2010/11/16 11:54 (Formatter Plus v4.8.0) */
--@"c:\database\packages\accounting\gm_pkg_dm_ac_account_txn.bdy";
CREATE OR REPLACE
PACKAGE BODY gm_pkg_dm_ac_account_txn
IS
    /******************************************************************
    * Description : Procedure to fetch account transaciton details DW stage to DM Operation
    ****************************************************************/
PROCEDURE gm_sav_conv_account_txn
AS
    -- variable declaration
    v_userid NUMBER := 30301;
    v_ossa_id T8900_OUS_ACCT_TRANS.C8900_OUS_ACCT_TRANS_ID%TYPE;
    v_osrt_id T8900_OUS_ACCT_TRANS.C8900_OUS_ACCT_TRANS_ID%TYPE;
    v_ossc_id T8900_OUS_ACCT_TRANS.C8900_OUS_ACCT_TRANS_ID%TYPE;
    v_osia_id T8900_OUS_ACCT_TRANS.C8900_OUS_ACCT_TRANS_ID%TYPE;
    v_osfg_id T8900_OUS_ACCT_TRANS.C8900_OUS_ACCT_TRANS_ID%TYPE;
    -- PC-3326 Brazil transaction enable
    -- To remove the cursor and based on context to get the company and plant id
   v_company_id t1900_company.c1900_company_id%TYPE;
   v_plant_id t5040_plant_master.c5040_plant_id%TYPE;
   
BEGIN
    /*
    * 1. Sales (ossa) Transaction
    * 2. Sales Returns (osrt) Transaction
    * 3. Qura to scrap (ossc) Transaction
    * 4. Inventory Adjustment (osia) Transaction
    * 5. Inventory Return (osfg) Transaction
    *
    */

	--0 to get the company and plant id from context
	 SELECT get_compid_frm_cntx (), get_plantid_frm_cntx ()
     INTO v_company_id, v_plant_id
     FROM dual;
     
        -- 1. ossa (to get the all sales order details)
        gm_sav_ous_sales_order_dtls (v_company_id, 102902, v_userid,'1000', v_ossa_id) ;
        DBMS_OUTPUT.PUT_LINE(' ** v_ossa_id '||v_ossa_id);
        gm_sav_ous_sales_order_dtls (v_company_id, 102902, v_userid,'1018', v_ossa_id) ;
        DBMS_OUTPUT.PUT_LINE(' ** v_ossa_id :1018: '||v_ossa_id);
        --2. osrt (to get the all sales returns details)
        gm_sav_ous_sales_returns_dtls (v_company_id, 102903, v_userid,'1000', v_osrt_id) ;
        --3. ossc (to get the all the Quarantine To scrap txn)
        gm_sav_ous_quar_to_scrap_dtls (v_company_id, 102904, 4115, v_userid,'1000', v_ossc_id) ;
        DBMS_OUTPUT.PUT_LINE(' ** v_ossc_id '||v_ossc_id);
        gm_sav_ous_quar_to_scrap_dtls (v_company_id, 102904, 4115, v_userid,'1018', v_ossc_id) ;
        DBMS_OUTPUT.PUT_LINE(' ** v_ossc_id :1018: '||v_ossc_id);
        --4. osia (to get the all Shelf to Inventory Adjustment txn)
        gm_sav_ous_inhouse_dtls (v_company_id, 102905, 400065, v_userid,'1000', v_osia_id) ;
        DBMS_OUTPUT.PUT_LINE(' ** v_osia_id '||v_osia_id);
        gm_sav_ous_inhouse_dtls (v_company_id, 102905, 400065, v_userid,'1018', v_osia_id) ;
        DBMS_OUTPUT.PUT_LINE(' ** v_osia_id 1018 :'||v_osia_id);
        -- 400065 Shelf to Inventory Adjustment
        --5. osfg (to get all Inventory Adjustment to Shelf txn )
        gm_sav_ous_inhouse_dtls (v_company_id, 102906, 400068, v_userid,'1000', v_osfg_id) ;--
        DBMS_OUTPUT.PUT_LINE(' ** v_osia_id 400068 :'||v_osfg_id);
        gm_sav_ous_inhouse_dtls (v_company_id, 102906, 400068, v_userid,'1018', v_osfg_id) ;
        DBMS_OUTPUT.PUT_LINE(' ** v_osia_id 400068 1018:'||v_osfg_id);
        -- 400068 Inventory Adjustment to Shelf

    -- save the Run date (t9900) (OUS Account Transactions Run time from DW stage to dm operation to USOLTP.)
    gm_pkg_ac_ous_acct_transaction.gm_sav_run_dtls (102911, SYSDATE) ; -- 102941 Account Transactions
END gm_sav_conv_account_txn;
/***********************************************************************************
*Description : Procedure to fetch all sales order details, and store to o8900 tables.
*
**************************************************************************************/
PROCEDURE gm_sav_ous_sales_order_dtls (
        p_company_id IN T1900_COMPANY.C1900_COMPANY_ID%TYPE,
        p_type      IN T8900_OUS_ACCT_TRANS.C901_TYPE%TYPE,
        p_userid    IN T501_ORDER.c501_last_updated_by%TYPE,
        p_posting_comp  IN T1900_COMPANY.C1900_COMPANY_ID%TYPE,
        p_out_txnid OUT T8900_OUS_ACCT_TRANS.C8900_OUS_ACCT_TRANS_ID%TYPE)
AS
    v_ossa_cnt NUMBER;
    v_ous_acct_trans_id T8900_OUS_ACCT_TRANS.C8900_OUS_ACCT_TRANS_ID%TYPE;
    v_order_id T501_ORDER.c501_order_id%TYPE;
    v_total_qty   NUMBER         := 0;
    v_total_price NUMBER (15, 2) := 0;
    v_ous_txn_part_count NUMBER;
    --v_run_date DATE;
    -- Sales (Order table cursor)
    CURSOR ous_order_master
    IS
         SELECT t501.c501_order_id ordid
           FROM t501_order t501,t502_item_order t502,t205_part_number t205, t202_project t202, 
           T2021_PROJECT_COMPANY_MAPPING t2021 
          WHERE 
           t202.c202_project_id = t205.c202_project_id
			and t205.c205_part_number_id = t502.c205_part_number_id
			and t501.c501_order_id = t502.c501_order_id
			and t2021.c202_project_id = t202.c202_project_id
			AND t502.C205_PART_NUMBER_ID NOT LIKE '999.%'
			and t2021.c1900_company_id=p_posting_comp
			and t2021.c2021_void_fl is null
			-- PC-3326 Brazil transaction enable
    		-- To remove the ext country and join company id condition
			and t501.c1900_company_id = p_company_id
			-- checking project company map type
			-- to avoid HQ company inculding a GMNA project
			AND t2021.C901_proj_com_map_typ = 105360
        AND t501.C501_VOID_FL IS NULL
        AND NOT EXISTS
        (
             SELECT c8902_ous_ref_id
               FROM t8902_ous_acct_trans_link
              WHERE t501.c501_order_id = c8902_ous_ref_id
                AND C1900_POSTING_COMP_ID = p_posting_comp
                AND c901_ref_type      = p_type --102902
        )
		AND t501.c501_status_fl > 0 --Need status greater than 0 since cannot use order type bcoz , first we need to exclude back orders(2525) and backorders has status 0,1,2
        AND NVL(t501.c901_order_type,-9999) NOT IN (2520,2523, 2524, 2529, 2528)
        GROUP BY t501.c501_order_id
        ;--Need NVL check bcoz there are many orders where the order type is NULL,such orders shold be bill and ship
        --AND t501.c501_shipping_date IS NOT NULL
    -- 2523 Return Order
    -- 2524 Price Adjustment
    -- 2529 Sales Adjustment
    -- 2528 Credit Memo
    CURSOR ous_ossa_cur
    IS
         SELECT t502.C205_PART_NUMBER_ID pnum, SUM (t502.C502_ITEM_QTY) qty, SUM (t502.C502_ITEM_PRICE) price
         ,t502.C502_CONTROL_NUMBER cntrlno
           FROM t501_order t501, T502_ITEM_ORDER t502, T205_PART_NUMBER T205,T2021_PROJECT_COMPANY_MAPPING t2021 
          WHERE t501.C501_ORDER_ID = t502.C501_ORDER_ID
            AND t501.c501_order_id = v_order_id
            AND t502.C205_PART_NUMBER_ID NOT LIKE '999.%'
            AND t501.C501_VOID_FL IS NULL
            AND t502.c502_void_fl IS NULL
            AND T205.C205_PART_NUMBER_ID = t502.C205_PART_NUMBER_ID
            AND t205.c202_project_id = t2021.c202_project_id
            AND t2021.c1900_company_id = p_posting_comp
			-- checking project company map type
			-- to avoid HQ company inculding a GMNA project
			AND t2021.C901_proj_com_map_typ = 105360
            AND t2021.c2021_void_fl is null
       GROUP BY t502.C205_PART_NUMBER_ID,t502.C502_CONTROL_NUMBER;
BEGIN
    -- get the total sales count
     SELECT COUNT (1)
       INTO v_ossa_cnt
       FROM t501_order t501,t502_item_order t502,t205_part_number t205, t202_project t202, 
           T2021_PROJECT_COMPANY_MAPPING t2021 
      WHERE t202.c202_project_id = t205.c202_project_id
			and t205.c205_part_number_id = t502.c205_part_number_id
			and t501.c501_order_id = t502.c501_order_id
			and t2021.c202_project_id = t202.c202_project_id
			AND t502.C205_PART_NUMBER_ID NOT LIKE '999.%'
			and t2021.c1900_company_id=p_posting_comp
			and t2021.c2021_void_fl is null
			-- PC-3326 Brazil transaction enable
    		-- To remove the ext country and join company id condition
			and t501.c1900_company_id = p_company_id
			-- checking project company map type
			-- to avoid HQ company inculding a GMNA project
			AND t2021.C901_proj_com_map_typ = 105360
        AND t501.C501_VOID_FL IS NULL
        AND NOT EXISTS
        (
             SELECT c8902_ous_ref_id
               FROM t8902_ous_acct_trans_link
              WHERE t501.c501_order_id = c8902_ous_ref_id
                AND C1900_POSTING_COMP_ID = p_posting_comp
                AND c901_ref_type      = p_type --  (Account Transactions)
        )
        AND t501.c501_status_fl > 0 --Need status greater than 0 since cannot use order type bcoz , first we need to exclude back orders(2525) and backorders has status 0,1,2
        AND NVL(t501.c901_order_type,-9999) NOT IN (2520,2523, 2524, 2529, 2528);--Need NVL check bcoz there are many orders where the order type is NULL,such orders shold be bill and ship
        --AND t501.c501_shipping_date IS NOT NULL
        DBMS_OUTPUT.PUT_LINE('>> CNT:'||v_ossa_cnt||':p_company_id:'||p_company_id||':p_type:'||p_type);
    IF v_ossa_cnt                    <> 0 THEN
        -- get the account transaction id based on the type.
        v_ous_acct_trans_id := get_ous_account_txn_id (p_type) ; -- ossa
        -- save the master txn id
        gm_pkg_ac_ous_acct_transaction.gm_sav_ous_acct_trans (v_ous_acct_trans_id, p_company_id, NULL, NULL, NULL,
        p_type, p_userid, p_posting_comp) ;
    
        FOR ousmaster IN ous_order_master
        LOOP
            v_order_id    := ousmaster.ordid;
            v_total_qty   := 0;
            v_total_price := 0;
       
            FOR oustxn    IN ous_ossa_cur
            LOOP
                -- save the order details (T8902)
                gm_pkg_ac_ous_acct_transaction.gm_sav_ous_acct_trans_item (v_ous_acct_trans_id, oustxn.pnum, oustxn.qty
                , oustxn.price, oustxn.cntrlno) ;
                -- get the total Qty and price each transaction.
                v_total_qty   := v_total_qty   + oustxn.qty;
                v_total_price := v_total_price + (oustxn.qty * oustxn.price) ;
            END LOOP;
            -- update the link table (T8093)
            gm_pkg_ac_ous_acct_transaction.gm_sav_ous_trans_link (v_order_id, v_ous_acct_trans_id, v_total_qty,
            v_total_price, p_type, p_posting_comp) ;
        END LOOP;
        
        
        
    END IF; -- v_ossa_cnt
    p_out_txnid := v_ous_acct_trans_id;
END gm_sav_ous_sales_order_dtls;
/***********************************************************************************
*Description : Procedure to fetch sales retunrs details and save to o8900 table.
*
**************************************************************************************/
PROCEDURE gm_sav_ous_sales_returns_dtls (
        p_company_id IN T1900_COMPANY.C1900_COMPANY_ID%TYPE,
        p_type      IN T8900_OUS_ACCT_TRANS.C901_TYPE%TYPE,
        p_userid    IN T506_RETURNS.c506_last_updated_by%TYPE,
        p_posting_comp  IN T1900_COMPANY.C1900_COMPANY_ID%TYPE,
        p_out_txnid OUT T8900_OUS_ACCT_TRANS.C8900_OUS_ACCT_TRANS_ID%TYPE)
AS
    v_osrt_cnt NUMBER;
    v_ous_acct_trans_id T8900_OUS_ACCT_TRANS.C8900_OUS_ACCT_TRANS_ID%TYPE;
    v_order_id t501_order.c501_order_id%TYPE;
    v_total_qty   NUMBER         := 0;
    v_total_price NUMBER (15, 2) := 0;
    v_ous_txn_part_count NUMBER;
    
    -- v_run_date DATE;
    CURSOR ous_order_cn_master
    IS
         SELECT t501.c501_order_id ordid
           FROM t501_order t501,t502_item_order t502,t205_part_number t205, t202_project t202, 
           T2021_PROJECT_COMPANY_MAPPING t2021 
          WHERE 
           t202.c202_project_id = t205.c202_project_id
			and t205.c205_part_number_id = t502.c205_part_number_id
			and t501.c501_order_id = t502.c501_order_id
			and t2021.c202_project_id = t202.c202_project_id
			AND t502.C205_PART_NUMBER_ID NOT LIKE '999.%'
			and t2021.c1900_company_id=p_posting_comp
			and t2021.c2021_void_fl is null
			-- PC-3326 Brazil transaction enable
    		-- To remove the ext country and join company id condition
			and t501.c1900_company_id = p_company_id
			-- checking project company map type
			-- to avoid HQ company inculding a GMNA project
			AND t2021.C901_proj_com_map_typ = 105360
        AND t501.C501_VOID_FL IS NULL
        AND NOT EXISTS
        (
             SELECT c8902_ous_ref_id
               FROM t8902_ous_acct_trans_link
              WHERE t501.c501_order_id = c8902_ous_ref_id
                AND C1900_POSTING_COMP_ID = p_posting_comp
                AND c901_ref_type      = p_type --  (Account Transactions)
        )
        AND NVL(t501.c901_order_type,-9999) IN (2523,  2529, 2528)
        GROUP BY t501.c501_order_id 
        ;--2524  Price Adjustment should be excluded since only the price is being adjusted and not qty
        --AND t501.c501_shipping_date IS NOT NULL
    -- 2523 Return Order
    -- 2524 Price Adjustment
    -- 2529 Sales Adjustment
    -- 2528 Credit Memo
    -- Sales Return (cursor)
    CURSOR ous_osrt_cur
    IS
         SELECT t502.C205_PART_NUMBER_ID pnum, SUM (ABS(t502.C502_ITEM_QTY)) qty, SUM (t502.C502_ITEM_PRICE) price
         ,t502.C502_CONTROL_NUMBER cntrlno
           FROM t501_order t501, T502_ITEM_ORDER t502, T205_PART_NUMBER T205,T2021_PROJECT_COMPANY_MAPPING t2021 
          WHERE t501.C501_ORDER_ID = t502.C501_ORDER_ID
            AND t501.c501_order_id = v_order_id
            AND T205.C205_PART_NUMBER_ID = t502.C205_PART_NUMBER_ID
            AND t502.C205_PART_NUMBER_ID NOT LIKE '999.%'
            AND t501.C501_VOID_FL IS NULL
            AND t502.c502_void_fl IS NULL
            and t2021.c202_project_id = t205.c202_project_id
			and t2021.c1900_company_id=p_posting_comp
			-- checking project company map type
			-- to avoid HQ company inculding a GMNA project
			AND t2021.C901_proj_com_map_typ = 105360
			and t2021.c2021_void_fl is null 
       GROUP BY t502.C205_PART_NUMBER_ID,t502.C502_CONTROL_NUMBER;
BEGIN
    -- get the sales return count
     SELECT COUNT (1)
       INTO v_osrt_cnt
       FROM t501_order t501,t502_item_order t502,t205_part_number t205, t202_project t202, 
           T2021_PROJECT_COMPANY_MAPPING t2021 
          WHERE 
           t202.c202_project_id = t205.c202_project_id
			and t205.c205_part_number_id = t502.c205_part_number_id
			and t501.c501_order_id = t502.c501_order_id
			and t2021.c202_project_id = t202.c202_project_id
			and t2021.c1900_company_id=p_posting_comp
            		AND t502.C205_PART_NUMBER_ID NOT LIKE '999.%'
			and t2021.c2021_void_fl is null
			-- PC-3326 Brazil transaction enable
    		-- To remove the ext country and join company id condition
			and t501.c1900_company_id = p_company_id
			-- checking project company map type
			-- to avoid HQ company inculding a GMNA project
			AND t2021.C901_proj_com_map_typ = 105360
        AND t501.C501_VOID_FL IS NULL
        AND NOT EXISTS
        (
             SELECT c8902_ous_ref_id
               FROM t8902_ous_acct_trans_link
              WHERE t501.c501_order_id = c8902_ous_ref_id
                AND C1900_POSTING_COMP_ID = p_posting_comp
                AND c901_ref_type      = p_type --  (Account Transactions)
        )
        AND NVL(t501.c901_order_type,-9999) IN (2523,  2529, 2528);
       -- AND t501.c501_shipping_date IS NOT NULL;
    IF v_osrt_cnt                <> 0 THEN
        -- get the account txn id
        v_ous_acct_trans_id := get_ous_account_txn_id (p_type) ; -- osrt
        -- save the master txn
        gm_pkg_ac_ous_acct_transaction.gm_sav_ous_acct_trans (v_ous_acct_trans_id, p_company_id, NULL, NULL, NULL,
        p_type, p_userid,p_posting_comp) ;
        
		FOR ousmaster IN ous_order_cn_master
        LOOP
            v_order_id    := ousmaster.ordid;
            v_total_qty   := 0;
            v_total_price := 0;
            FOR oustxn    IN ous_osrt_cur
            LOOP
                -- save the retuns details.
                gm_pkg_ac_ous_acct_transaction.gm_sav_ous_acct_trans_item (v_ous_acct_trans_id, oustxn.pnum, oustxn.qty
                , oustxn.price, oustxn.cntrlno) ;
                -- get the total Qty and price each transaction.
                v_total_qty   := v_total_qty   + oustxn.qty;
                v_total_price := v_total_price + (oustxn.qty * oustxn.price) ;
            END LOOP;
            -- update the link table.
            gm_pkg_ac_ous_acct_transaction.gm_sav_ous_trans_link (v_order_id, v_ous_acct_trans_id, v_total_qty,
            v_total_price, p_type, p_posting_comp) ;
        END LOOP;
        
        
        
    END IF; -- v_osrt_cnt
    p_out_txnid := v_ous_acct_trans_id;
END gm_sav_ous_sales_returns_dtls;
/***********************************************************************************
*Description : Procedure to fetch inhouse transaction (FGIA and IAFG) and save the o8900 table.
*
**************************************************************************************/
PROCEDURE gm_sav_ous_inhouse_dtls (
        p_company_id IN T1900_COMPANY.C1900_COMPANY_ID%TYPE,
        p_trans_type   IN T8900_OUS_ACCT_TRANS.C901_TYPE%TYPE,
        p_inhouse_type IN t412_inhouse_transactions.c412_type%TYPE,
        p_userid       IN t412_inhouse_transactions.c412_last_updated_by%TYPE,
        p_posting_comp  IN T1900_COMPANY.C1900_COMPANY_ID%TYPE,
        p_out_txnid OUT T8900_OUS_ACCT_TRANS.C8900_OUS_ACCT_TRANS_ID%TYPE)
AS
    v_cnt NUMBER;
    v_ous_acct_trans_id T8900_OUS_ACCT_TRANS.C8900_OUS_ACCT_TRANS_ID%TYPE;
    v_inhouse_id t412_inhouse_transactions.c412_inhouse_trans_id%TYPE;
    v_total_qty   NUMBER         := 0;
    v_total_price NUMBER (15, 2) := 0;
    v_ous_txn_part_count NUMBER;
    --v_run_date DATE;
    -- Qura to osscp (Inhouse table cursor)
    
    -- PC-3326 Brazil transaction enable
    -- To remove the ext country and join company id condition
    CURSOR ous_inhouse_master
    IS
         SELECT t412.c412_inhouse_trans_id id
           FROM t412_inhouse_transactions t412, T205_PART_NUMBER T205,T2021_PROJECT_COMPANY_MAPPING t2021,
            t413_inhouse_trans_items t413
          WHERE t412.c1900_company_id = p_company_id
        AND t412.c412_verify_fl = 1
        AND NOT EXISTS
        (
             SELECT c8902_ous_ref_id
               FROM t8902_ous_acct_trans_link
              WHERE t412.c412_inhouse_trans_id = c8902_ous_ref_id
                AND C1900_POSTING_COMP_ID = p_posting_comp
                AND c901_ref_type              = p_trans_type -- 102941 (Account Transactions)
        )
        AND t412.c412_type     = p_inhouse_type -- 4115 Quarantine To osscp, 400065 Shelf to Inventory
        AND t412.c412_void_fl IS NULL
        AND t412.c412_inhouse_trans_id =  t413.c412_inhouse_trans_id
        AND T205.C205_PART_NUMBER_ID = t413.C205_PART_NUMBER_ID
        AND t413.c205_part_number_id NOT LIKE '999.%'
		AND t205.c202_project_id = t2021.c202_project_id
		AND t2021.c1900_company_id = p_posting_comp
		-- checking project company map type
		-- to avoid HQ company inculding a GMNA project
		AND t2021.C901_proj_com_map_typ = 105360
		AND t2021.c2021_void_fl is null;
  
    CURSOR ous_inhouse_cur
    IS
         SELECT t413.c205_part_number_id pnum, SUM (t413.c413_item_qty) qty, SUM (t413.c413_item_price) price
         ,t413.C413_CONTROL_NUMBER cntrlno
           FROM t412_inhouse_transactions t412, t413_inhouse_trans_items t413, T205_PART_NUMBER T205,T2021_PROJECT_COMPANY_MAPPING t2021
          WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
            AND t412.c412_type             = p_inhouse_type -- 4115 Quarantine To osscp, 400065 Shelf to Inventory
            -- Adjustment
            AND t412.c412_inhouse_trans_id = v_inhouse_id
            AND t413.c205_part_number_id NOT LIKE '999.%'
            AND t412.c412_void_fl IS NULL
            AND t413.c413_void_fl IS NULL
            AND t412.c412_inhouse_trans_id =  t413.c412_inhouse_trans_id
	        AND T205.C205_PART_NUMBER_ID = t413.C205_PART_NUMBER_ID
			AND t205.c202_project_id = t2021.c202_project_id
			AND t2021.c1900_company_id = p_posting_comp
			-- checking project company map type
			-- to avoid HQ company inculding a GMNA project
			AND t2021.C901_proj_com_map_typ = 105360
			AND t2021.c2021_void_fl is null
       GROUP BY t413.c205_part_number_id, t413.C413_CONTROL_NUMBER;
BEGIN
	-- PC-3326 Brazil transaction enable
    -- To remove the ext country and join company id condition
    
    -- v_run_date := TRUNC (get_run_date (102941)) ;
     SELECT COUNT (1)
       INTO v_cnt
       FROM t412_inhouse_transactions t412, t413_inhouse_trans_items t413, T205_PART_NUMBER T205,T2021_PROJECT_COMPANY_MAPPING t2021
      WHERE t412.c1900_company_id = p_company_id
        AND t412.c412_verify_fl = 1
        AND NOT EXISTS
        (
             SELECT c8902_ous_ref_id
               FROM t8902_ous_acct_trans_link
              WHERE t412.c412_inhouse_trans_id = c8902_ous_ref_id
                AND C1900_POSTING_COMP_ID = p_posting_comp
                AND c901_ref_type              = p_trans_type -- 102941 (Account Transactions)
        )
        AND t412.c412_type     = p_inhouse_type -- 4115 Quarantine To osscp, 400065 Shelf to Inventory
            AND t413.c205_part_number_id NOT LIKE '999.%'
        AND t412.c412_void_fl IS NULL
        AND t412.c412_inhouse_trans_id =  t413.c412_inhouse_trans_id
        AND T205.C205_PART_NUMBER_ID = t413.C205_PART_NUMBER_ID
		AND t205.c202_project_id = t2021.c202_project_id
		AND t2021.c1900_company_id = p_posting_comp
		-- checking project company map type
		-- to avoid HQ company inculding a GMNA project
		AND t2021.C901_proj_com_map_typ = 105360
		AND t2021.c2021_void_fl is null
        ;
        
    IF v_cnt                  <> 0 THEN
        -- get the account txn id
        v_ous_acct_trans_id := get_ous_account_txn_id (p_trans_type) ; -- ossc
        -- save the master txn id (t8901)
        gm_pkg_ac_ous_acct_transaction.gm_sav_ous_acct_trans (v_ous_acct_trans_id, p_company_id, NULL, NULL, NULL,
        p_trans_type, p_userid, p_posting_comp) ;
        
        FOR ousmaster IN ous_inhouse_master
        LOOP
            v_inhouse_id  := ousmaster.id;
            v_total_qty   := 0;
            v_total_price := 0;
            
            FOR oustxn    IN ous_inhouse_cur
            LOOP
                -- save the inhouse details (T8902)
                gm_pkg_ac_ous_acct_transaction.gm_sav_ous_acct_trans_item (v_ous_acct_trans_id, oustxn.pnum, oustxn.qty
                , oustxn.price, oustxn.cntrlno) ;
                -- get the total Qty and price each transaction.
                v_total_qty   := v_total_qty   + oustxn.qty;
                v_total_price := v_total_price + (oustxn.qty * oustxn.price) ;
            END LOOP;
            -- update link details (T8903)
            gm_pkg_ac_ous_acct_transaction.gm_sav_ous_trans_link (v_inhouse_id, v_ous_acct_trans_id, v_total_qty,
            v_total_price, p_trans_type, p_posting_comp) ;
        END LOOP;
        
        
        
    END IF; -- v_cnt
    p_out_txnid := v_ous_acct_trans_id;
END gm_sav_ous_inhouse_dtls ;
/***********************************************************************************
*Description : Procedure to fetch Quartaine to Scap and save the o8900 table
*
**************************************************************************************/
PROCEDURE gm_sav_ous_quar_to_scrap_dtls (
        p_company_id IN T1900_COMPANY.C1900_COMPANY_ID%TYPE,
        p_trans_type IN T8900_OUS_ACCT_TRANS.C901_TYPE%TYPE,
        p_cn_type    IN t504_consignment.c504_type%TYPE,
        p_userid     IN t504_consignment.c504_last_updated_by%TYPE,
        p_posting_comp  IN T1900_COMPANY.C1900_COMPANY_ID%TYPE,
        p_out_txnid OUT T8900_OUS_ACCT_TRANS.C8900_OUS_ACCT_TRANS_ID%TYPE)
AS
    v_cnt NUMBER;
    v_ous_acct_trans_id T8900_OUS_ACCT_TRANS.C8900_OUS_ACCT_TRANS_ID%TYPE;
    v_consignment_id t504_consignment.c504_consignment_id%TYPE;
    v_total_qty   NUMBER         := 0;
    v_total_price NUMBER (15, 2) := 0;
    v_ous_txn_part_count NUMBER;
    
    -- v_run_date DATE;
    -- Qura to osscp (Inhouse table cursor)
    
    -- PC-3326 Brazil transaction enable
    -- To remove the ext country and join company id condition
    CURSOR ous_cn_master
    IS
         SELECT t504.c504_consignment_id id
           FROM t504_consignment t504,t505_item_consignment T505, T205_PART_NUMBER T205,T2021_PROJECT_COMPANY_MAPPING t2021
          WHERE t504.c1900_company_id = p_company_id
        AND t504.c504_verify_fl = 1
        AND t504.c504_status_fl = 4
        AND NOT EXISTS
        (
             SELECT c8902_ous_ref_id
               FROM t8902_ous_acct_trans_link
              WHERE t504.c504_consignment_id = c8902_ous_ref_id
                AND C1900_POSTING_COMP_ID = p_posting_comp
                AND c901_ref_type            = p_trans_type -- 102941 (Account Transactions)
        )
        AND t504.c504_type     = p_cn_type -- 4115 Quarantine To osscp
        AND t504.c504_void_fl IS NULL
            AND t505.c205_part_number_id NOT LIKE '999.%'
        AND T205.C205_PART_NUMBER_ID = t505.C205_PART_NUMBER_ID
		AND t205.c202_project_id = t2021.c202_project_id
		AND t2021.c1900_company_id = p_posting_comp
		-- checking project company map type
		-- to avoid HQ company inculding a GMNA project
		AND t2021.C901_proj_com_map_typ = 105360
		AND t2021.c2021_void_fl is null
		AND t504.c504_consignment_id = t505.c504_consignment_id
		 group by t504.c504_consignment_id
		;
		
    CURSOR ous_cn_dtls_cur
    IS
         SELECT t505.c205_part_number_id pnum, SUM (t505.c505_item_qty) qty, SUM(t505.c505_item_price) price
         ,t505.C505_CONTROL_NUMBER cntrlno
           FROM t504_consignment t504, t505_item_consignment t505, T205_PART_NUMBER T205,T2021_PROJECT_COMPANY_MAPPING t2021
          WHERE t504.c504_consignment_id = t505.c504_consignment_id
            AND t504.c504_consignment_id = v_consignment_id
            AND t505.c205_part_number_id NOT LIKE '999.%'
            AND t504.c504_type     = p_cn_type -- 4115 Quarantine To osscp
            AND t504.c504_void_fl IS NULL
            AND t505.c505_void_fl IS NULL
            AND T205.C205_PART_NUMBER_ID = t505.C205_PART_NUMBER_ID
			AND t205.c202_project_id = t2021.c202_project_id
			AND t2021.c1900_company_id = p_posting_comp
			-- checking project company map type
			-- to avoid HQ company inculding a GMNA project
			AND t2021.C901_proj_com_map_typ = 105360
			AND t2021.c2021_void_fl is null
       GROUP BY t505.c205_part_number_id, t505.C505_CONTROL_NUMBER;
BEGIN
    -- get the total count.
    
	-- PC-3326 Brazil transaction enable
    -- To remove the ext country and join company id condition
    		
     SELECT COUNT (1)
       INTO v_cnt
       FROM t504_consignment t504, t505_item_consignment t505, T205_PART_NUMBER T205,T2021_PROJECT_COMPANY_MAPPING t2021
      WHERE t504.c1900_company_id = p_company_id
        AND t504.c504_verify_fl = 1
        AND t504.c504_status_fl = 4
        AND NOT EXISTS
        (
             SELECT c8902_ous_ref_id
               FROM t8902_ous_acct_trans_link
              WHERE t504.c504_consignment_id = c8902_ous_ref_id
                AND C1900_POSTING_COMP_ID = p_posting_comp
                AND c901_ref_type            = p_trans_type -- 102941 (Account Transactions)
        )
        AND t504.c504_type     = p_cn_type -- 4115 Quarantine To osscp, 400065 Shelf to Inventory
        AND t504.c504_void_fl IS NULL
        AND t504.c504_consignment_id = t505.c504_consignment_id
        AND t505.c505_void_fl IS NULL
        AND T205.C205_PART_NUMBER_ID = t505.C205_PART_NUMBER_ID
		AND t205.c202_project_id = t2021.c202_project_id
		AND t2021.c1900_company_id = p_posting_comp
		-- checking project company map type
		-- to avoid HQ company inculding a GMNA project
		AND t2021.C901_proj_com_map_typ = 105360
		AND t2021.c2021_void_fl is null;
        
    IF v_cnt                  <> 0 THEN
    
        v_ous_acct_trans_id   := get_ous_account_txn_id (p_trans_type) ; -- ossc
        gm_pkg_ac_ous_acct_transaction.gm_sav_ous_acct_trans (v_ous_acct_trans_id, p_company_id, NULL, NULL, NULL,
        p_trans_type, p_userid, p_posting_comp) ;
       
        FOR ousmaster IN ous_cn_master
        LOOP
            v_consignment_id := ousmaster.id;
            v_total_qty      := 0;
            v_total_price    := 0;
        
            FOR oustxn       IN ous_cn_dtls_cur
            LOOP
                -- account transaction item (t8902)
                gm_pkg_ac_ous_acct_transaction.gm_sav_ous_acct_trans_item (v_ous_acct_trans_id, oustxn.pnum, oustxn.qty
                , oustxn.price, oustxn.cntrlno) ;
                -- get the total Qty and price each transaction.
                v_total_qty   := v_total_qty   + oustxn.qty;
                v_total_price := v_total_price + (oustxn.qty * oustxn.price) ;
         
            END LOOP;
         
            -- save to t8903 table
            gm_pkg_ac_ous_acct_transaction.gm_sav_ous_trans_link (v_consignment_id, v_ous_acct_trans_id, v_total_qty,
            v_total_price, p_trans_type, p_posting_comp) ;
     
         END LOOP;
        
        
    END IF; -- v_cnt
    p_out_txnid := v_ous_acct_trans_id;
END gm_sav_ous_quar_to_scrap_dtls ;
/******************************************************************
* C to C transfer Procedure
****************************************************************/
/**********************************************************************************************************
*User   : This Porcedure used for DW Stage to DM Operations.
*Description : Main Procedure to get all the Inter Company Transfer Consignment and Create a TF Transactoin.
*
***********************************************************************************************************/
PROCEDURE gm_sav_conv_transfered_cn
AS
    v_userid NUMBER := 30301; -- Manual Load
    --v_run_date DATE;
    v_country_id        VARCHAR2 (40) ;
    v_consignment_id    VARCHAR2 (40) ;
    v_ous_acct_trans_id VARCHAR2 (40) ;
    --v_type              NUMBER := 4000336; -- Dont use : OUS C to C Transfers Run time update from DW stage to dm operation to USOLTP
     v_type              NUMBER :=102923; --OUS C to C Transfers  update from DW stage to dm operation to USOLTP
    v_total_qty   NUMBER;
    v_total_price NUMBER (15, 2) := 0;
    v_trans_tag_id t8903_ous_acct_trans_tag.C8903_OUS_TRANS_TAG_ID%TYPE;
    v_set_id        VARCHAR2 (40) ;
    v_dist_id       VARCHAR2 (40) ;
    v_transfer_dist VARCHAR2 (40) ;
    --v_dist_id VARCHAR2 (40) ;
    v_cnt NUMBER;
    v_tmp_country NUMBER;
    v_ous_txn_part_count NUMBER;
    --
    -- PC-3326 Brazil transaction enable
    -- To remove the cursor and based on context to get the company and plant id
    v_company_id t1900_company.c1900_company_id%TYPE;
    v_plant_id t5040_plant_master.c5040_plant_id%TYPE; 

    -- PC-3326 Brazil transaction enable
    -- To remove the ext country and join company id condition
    		
    CURSOR ous_cn_master
    IS
         SELECT c504_consignment_id id, t504.c207_set_id setid, c701_distributor_id transfer_distid
           FROM t504_consignment t504
          WHERE t504.c1900_company_id = v_company_id
        AND t504.c504_status_fl = 5 -- Transferred
        AND t504.c504_type      = 40057 -- ICT
        AND NOT EXISTS
        (
             SELECT t8902.c8902_ous_ref_id
               FROM t8902_ous_acct_trans_link t8902
              WHERE t504.c504_consignment_id = c8902_ous_ref_id
                AND C1900_POSTING_COMP_ID = 1000
                AND t8902.c901_ref_type      = v_type -- 102923 --NEW CODE ID<Country to CountryTransfers> and not 102942 (Country to Country transfer)
        )
        AND t504.c504_void_fl IS NULL;
    CURSOR ous_cn_dtls_cur
    IS
         SELECT t505.c205_part_number_id pnum, t505.c505_item_qty qty, t505.c505_item_price price
          , t505.c505_control_number cnum
           FROM t504_consignment t504, t505_item_consignment t505
          WHERE t504.c504_consignment_id = t505.c504_consignment_id
            AND t504.c504_consignment_id = v_consignment_id
            AND t504.c504_void_fl       IS NULL
            AND t505.c505_void_fl       IS NULL;
       --GROUP BY t505.c205_part_number_id, , t505.c505_control_number;
    CURSOR ous_tag_dtls
    IS
         SELECT c5010_tag_id tagid, t5010.c205_part_number_id pnum
           FROM t5010_tag t5010
          WHERE c5010_last_updated_trans_id = v_consignment_id
            AND c5010_void_fl              IS NULL
            AND c901_status                 = 102401; -- Transferred
BEGIN
    
		--0 to get the company and plant id from context
		 SELECT get_compid_frm_cntx (), get_plantid_frm_cntx ()
	     INTO v_company_id, v_plant_id
	     FROM dual;
     
        -- get the distributor id based on the country.
        v_dist_id := get_rule_value (v_company_id ||'_1000', 'OUS_ACCOUNT_DIST_MAP') ;
        -- check the each country have the Intercompany Transfered CN.
        
        -- PC-3326 Brazil transaction enable
    	-- To remove the ext country and join company id condition
    		
         SELECT COUNT (1)
           INTO v_cnt
           FROM t504_consignment t504
          WHERE t504.c1900_company_id = v_company_id
            AND t504.c504_status_fl = 5 -- Transferred
            AND t504.c504_type      = 40057 -- ICT
            AND NOT EXISTS
            (
                 SELECT t8902.c8902_ous_ref_id
                   FROM t8902_ous_acct_trans_link t8902
                  WHERE t504.c504_consignment_id = c8902_ous_ref_id
                    AND C1900_POSTING_COMP_ID = 1000
                    AND t8902.c901_ref_type      = v_type -- 102942 (Country to Country transfer)
            )
            AND t504.c504_void_fl IS NULL;
        -- If Inter Company Transfered CN then create a temparay Transfer (t8901) and stroe to DM Operations.
        IF v_cnt          <> 0 THEN
            FOR ousmaster IN ous_cn_master
            LOOP
                v_consignment_id := ousmaster.id;
                v_set_id         := ousmaster.setid;
                --v_dist_id           := ousmaster.distid;
                v_transfer_dist := ousmaster.transfer_distid;
                -- to create a TF id (ie. TF-1-UK)
                 SELECT 'TF-' || S8900_OUS_OSSA_ACCT_TRANS.NEXTVAL 
                   INTO v_ous_acct_trans_id
                   FROM DUAL;
                -- save the account trans tables (t8901)
                gm_pkg_ac_ous_acct_transaction.gm_sav_ous_acct_trans (v_ous_acct_trans_id, v_company_id, v_set_id,
                v_transfer_dist, v_dist_id, v_type, v_userid, 1000) ;
                v_total_qty   := 0;
                v_total_price := 0;
                FOR oustxn    IN ous_cn_dtls_cur
                LOOP
                    -- save the account transaction details (t8902).
                    gm_pkg_ac_ous_acct_transaction.gm_sav_ous_acct_trans_item (v_ous_acct_trans_id, oustxn.pnum,
                    oustxn.qty, oustxn.price, oustxn.cnum) ;
                    -- get the total Qty and price each transaction.
                    v_total_qty   := v_total_qty   + oustxn.qty;
                    v_total_price := v_total_price + (oustxn.qty * oustxn.price) ;
                END LOOP;
                -- if have the tag then insert tag details (t8903)
                FOR oustag IN ous_tag_dtls
                LOOP
                    gm_pkg_ac_ous_acct_transaction.gm_sav_ous_trans_tag (v_trans_tag_id, v_ous_acct_trans_id,
                    oustag.tagid, oustag.pnum) ;
                END LOOP;
                -- insert the new record to reference table (t8902-)link.
                gm_pkg_ac_ous_acct_transaction.gm_sav_ous_trans_link (v_consignment_id, v_ous_acct_trans_id,
                v_total_qty, v_total_price, v_type, 1000) ;
            END LOOP;
            
            
        
        END IF;

    -- save the Run date (t9900) (C to C)
    -- 4000336 OUS C to C Transfers Run time update from DW stage to dm operation to USOLTP
    gm_pkg_ac_ous_acct_transaction.gm_sav_run_dtls (4000336, SYSDATE) ; 
END gm_sav_conv_transfered_cn;
-- new Proess for C- C transfer
END gm_pkg_dm_ac_account_txn;
/
