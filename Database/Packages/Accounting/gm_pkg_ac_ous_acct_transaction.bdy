/* Formatted on 2010/11/16 11:54 (Formatter Plus v4.8.0) */
--@"c:\database\packages\accounting\gm_pkg_ac_ous_acct_transaction.bdy";
CREATE OR REPLACE
PACKAGE BODY gm_pkg_ac_ous_acct_transaction
IS
    /***********************************************************************************
    *Description : Procedure to insert the master transaction details.
    *
    **************************************************************************************/
PROCEDURE gm_sav_ous_acct_trans (
        p_acct_trans_id IN OUT t8900_ous_acct_trans.C8900_OUS_ACCT_TRANS_ID%TYPE,
        p_company_id    IN t8900_ous_acct_trans.c1900_company_id%TYPE,
        p_set_id        IN t8900_ous_acct_trans.C207_SET_ID%TYPE,
        p_trans_dist    IN t8900_ous_acct_trans.C701_TRANSFER_DISTRIBUTOR_ID%TYPE,
        p_dist          IN t8900_ous_acct_trans.C701_DISTRIBUTOR_ID%TYPE,
        p_type          IN t8900_ous_acct_trans.C901_TYPE%TYPE,
        p_user_id       IN t8900_ous_acct_trans.C8900_CREATED_BY%TYPE,
        p_posting_comp  IN T1900_COMPANY.C1900_COMPANY_ID%TYPE)
AS
BEGIN
     UPDATE t8900_ous_acct_trans
    SET c1900_company_id         = p_company_id, c207_set_id = p_set_id, C701_TRANSFER_DISTRIBUTOR_ID = p_trans_dist
      , C701_DISTRIBUTOR_ID         = p_dist, C901_TYPE = p_type, C8900_LAST_UPDATED_BY = p_user_id
      , C8900_LAST_UPDATED_DATE     = SYSDATE
      WHERE C8900_OUS_ACCT_TRANS_ID = p_acct_trans_id
        AND c1900_posting_comp_id = p_posting_comp
        AND C8900_VOID_FL          IS NULL;
    IF SQL%ROWCOUNT                 = 0 THEN
         INSERT
           INTO t8900_ous_acct_trans
            (
                c8900_ous_acct_trans_id, c901_type, c1900_company_id
              , c207_set_id, C701_TRANSFER_DISTRIBUTOR_ID, C701_DISTRIBUTOR_ID
              , c8900_created_by, c8900_created_date,c1900_posting_comp_id
            )
            VALUES
            (
                p_acct_trans_id, p_type, p_company_id
              , p_set_id, p_trans_dist, p_dist
              , p_user_id, SYSDATE,p_posting_comp
            ) ;
    END IF;
END gm_sav_ous_acct_trans;
/***********************************************************************************
*Description : Procedure to insert the ous account transaction item details.
*
**************************************************************************************/
PROCEDURE gm_sav_ous_acct_trans_item
    (
        p_acct_trans_id IN t8900_ous_acct_trans.C8900_OUS_ACCT_TRANS_ID%TYPE,
        p_part_num      IN t205_part_number.C205_PART_NUMBER_ID%TYPE,
        p_item_qty      IN t8901_ous_acct_trans_item.C8901_ITEM_QTY%TYPE,
        p_item_price    IN t8901_ous_acct_trans_item.C8901_ITEM_PRICE%TYPE,
        p_control_num   IN t8901_ous_acct_trans_item.c8901_CONTROL_NUMBER%TYPE
    )
AS
    v_acct_trans_item_seq t8901_ous_acct_trans_item.c8901_ous_acct_trans_item_id%TYPE;
BEGIN
     SELECT S8901_OUS_ACCT_TRANS_ITEM.NEXTVAL INTO v_acct_trans_item_seq FROM DUAL;
     INSERT
       INTO t8901_ous_acct_trans_item
        (
            c8901_ous_acct_trans_item_id, c8900_ous_acct_trans_id, c205_part_number_id
          , c8901_CONTROL_NUMBER, c8901_item_qty, c8901_item_price
        )
        VALUES
        (
            v_acct_trans_item_seq, p_acct_trans_id, p_part_num
          , p_control_num, p_item_qty, p_item_price
        ) ;
END gm_sav_ous_acct_trans_item;
/***********************************************************************************
*Description : Procedure to insert the ous transaction link details.
*
**************************************************************************************/
PROCEDURE gm_sav_ous_trans_link
    (
        p_ref_id        IN t8902_ous_acct_trans_link.C8902_OUS_REF_ID%TYPE,
        p_acct_trans_id IN t8900_ous_acct_trans.C8900_OUS_ACCT_TRANS_ID%TYPE,
        p_total_qty     IN t8902_ous_acct_trans_link.C8902_OUS_REF_QTY%TYPE,
        p_total_price   IN t8902_ous_acct_trans_link.C8902_OUS_REF_PRICE%TYPE,
        p_ref_type      IN t8902_ous_acct_trans_link.C901_REF_TYPE%TYPE,
        p_posting_comp  IN T1900_COMPANY.C1900_COMPANY_ID%TYPE
    )
AS
    v_trans_link_seq t8902_ous_acct_trans_link.C8902_OUS_TRANS_LINK_ID%TYPE;
BEGIN
     UPDATE t8902_ous_acct_trans_link
    SET C8902_OUS_REF_QTY           = p_total_qty, C8902_OUS_REF_PRICE = p_total_price
      WHERE C8902_OUS_REF_ID        = p_ref_id
        AND C8900_OUS_ACCT_TRANS_ID = p_acct_trans_id
        AND c1900_posting_comp_id = p_posting_comp 
        AND C901_REF_TYPE           = p_ref_type;
    
        IF SQL%ROWCOUNT                 = 0 THEN
    
         SELECT S8902_OUS_TRANS_LINK.NEXTVAL INTO v_trans_link_seq FROM DUAL;
         INSERT
           INTO t8902_ous_acct_trans_link
            (
                c8902_ous_trans_link_id, c8902_ous_ref_id, c8900_ous_acct_trans_id
              , C8902_OUS_REF_QTY, C8902_OUS_REF_PRICE, C901_REF_TYPE, C1900_POSTING_COMP_ID
            )
            VALUES
            (
                v_trans_link_seq, p_ref_id, p_acct_trans_id
              , p_total_qty, p_total_price, p_ref_type,p_posting_comp
            ) ;
    END IF;
END gm_sav_ous_trans_link;
/***********************************************************************************
*Description : Procedure to insert the ous transaction tag details.
*
**************************************************************************************/
PROCEDURE gm_sav_ous_trans_tag
    (
        p_trans_tag_id IN t8903_ous_acct_trans_tag.C8903_OUS_TRANS_TAG_ID%TYPE,
        p_ous_trans_id IN t8900_ous_acct_trans.C8900_OUS_ACCT_TRANS_ID%TYPE,
        p_tag_id       IN t8903_ous_acct_trans_tag.C5010_TAG_ID%TYPE,
        p_part_num     IN t8903_ous_acct_trans_tag.C205_PART_NUMBER_ID%TYPE
    )
AS
    v_trans_tag_seq t8903_ous_acct_trans_tag.C8903_OUS_TRANS_TAG_ID%TYPE;
BEGIN
     UPDATE t8903_ous_acct_trans_tag
    SET C8900_OUS_ACCT_TRANS_ID     = p_ous_trans_id, C5010_TAG_ID = p_tag_id, C205_PART_NUMBER_ID = p_part_num
      WHERE C8900_OUS_ACCT_TRANS_ID = p_ous_trans_id
        AND C5010_TAG_ID            = p_tag_id;
    IF SQL%ROWCOUNT                 = 0 THEN
        -- get the seq
         SELECT s8903_OUS_TRANS_TAG.NEXTVAL
           INTO v_trans_tag_seq
           FROM DUAL;
         INSERT
           INTO t8903_ous_acct_trans_tag
            (
                C8903_OUS_TRANS_TAG_ID, C8900_OUS_ACCT_TRANS_ID, C5010_TAG_ID
              , C205_PART_NUMBER_ID
            )
            VALUES
            (
                v_trans_tag_seq, p_ous_trans_id, p_tag_id
              , p_part_num
            ) ;
    END IF;
END gm_sav_ous_trans_tag;
/***********************************************************************************
*Description : Procedure to insert the ous run details details.
*
**************************************************************************************/
PROCEDURE gm_sav_run_dtls
    (
        p_type        IN T9900_RUN.C901_TYPE%TYPE,
        p_last_run_dt IN T9900_RUN.C9900_LAST_RUN_DATE%TYPE
    )
AS
    v_run_seq t8903_ous_acct_trans_tag.C8903_OUS_TRANS_TAG_ID%TYPE;
BEGIN
     UPDATE T9900_RUN
    SET C9900_LAST_RUN_DATE = p_last_run_dt
      WHERE C901_TYPE       = p_type;
    IF SQL%ROWCOUNT         = 0 THEN
         SELECT S9900_RUN.NEXTVAL INTO v_run_seq FROM DUAL;
         INSERT
           INTO T9900_RUN
            (
                c9900_run_id, c901_type, c9900_last_run_date
            )
            VALUES
            (
                v_run_seq, p_type, p_last_run_dt
            ) ;
    END IF;
END gm_sav_run_dtls;
/***********************************************************************************
*Description : Procedure used to update the transaction flag in t8901 table
*
**************************************************************************************/
PROCEDURE gm_update_trans_flag
    (
        p_acct_trans_id IN t8900_ous_acct_trans.c8900_ous_acct_trans_id%TYPE,
        p_trans_fl	    IN t8900_ous_acct_trans.c8900_trans_fl%TYPE,
        p_user_id       IN t8900_ous_acct_trans.C8900_CREATED_BY%TYPE
    )
AS
BEGIN
    -- Update the Trans flag.
     UPDATE t8900_ous_acct_trans
    SET c8900_trans_fl              = p_trans_fl, c8900_last_updated_by = p_user_id, c8900_last_updated_date = SYSDATE
      WHERE c8900_ous_acct_trans_id = p_acct_trans_id
        AND c8900_void_fl          IS NULL;
END gm_update_trans_flag;
END gm_pkg_ac_ous_acct_transaction;
/
