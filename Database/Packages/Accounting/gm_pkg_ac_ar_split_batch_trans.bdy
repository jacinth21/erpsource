/* Formatted on 2009/09/17 10:48 (Formatter Plus v4.8.0) */
--@"C:\Database\Packages\Accounting\gm_pkg_ac_ar_split_batch_trans.bdy";
CREATE OR REPLACE
PACKAGE BODY gm_pkg_ac_ar_split_batch_trans
IS
    --
/******************************************************************
* Description :  This procedure is used to create multiple batch for Japan
****************************************************************/
PROCEDURE gm_split_multi_batch (
    p_batch_id     IN T9600_BATCH.C9600_BATCH_ID%TYPE,
    p_user_id      IN T9600_BATCH.C9600_CREATED_BY%TYPE,
    p_out_batch_id OUT VARCHAR2
    )
AS
CURSOR cur_batch_dtls(v_no_deal  NUMBER,v_company_id t1900_company.c1900_company_id%TYPE,v_range NUMBER)
  IS
    SELECT t501.c704_account_id accid ,
      t501.c501_order_id transid,
      t501.c501_order_date ord_dt,
      t501.c501_surgery_date trans_date ,
      t501.c101_dealer_id dealer_id ,t9601.c9601_email_fl mail_fl,
      t9601.c901_ref_type ref_type
    FROM t9600_batch t9600,
      t9601_batch_details t9601,
      t501_order t501
    WHERE t9600.c9600_void_fl   IS NULL
    AND t9601.c9601_void_fl     IS NULL
    AND t501.c501_void_fl       IS NULL
    AND t501.c501_order_id       = t9601.c9601_ref_id
    AND t9600.c9600_batch_id     = t9601.c9600_batch_id
    AND t9600.C901_BATCH_STATUS <> 18744 -- Processed
    AND t9600.c9600_batch_id     = p_batch_id
    AND t9600.C1900_COMPANY_ID   = v_company_id
    AND t501.c101_dealer_id NOT IN (SELECT c906_rule_value
    								FROM t906_rules
    								WHERE c906_rule_grp_id='JPN_BULK_DEALERS'
    								AND c1900_company_id = v_company_id
                    				AND c906_void_fl IS NULL) -- MIC , Senko Medical Instrument Mfg
    AND t501.c101_dealer_id     IN (SELECT MY_TEMP_TXN_ID
							        FROM MY_TEMP_KEY_VALUE
							        WHERE MY_TEMP_TXN_KEY IS NULL
							        AND MY_TEMP_TXN_ID IS NOT NULL
							        order by MY_TEMP_TXN_ID Offset v_range Rows Fetch Next v_no_deal Rows Only
							        );
      
  v_batch_cnt NUMBER;
  v_batchid_out  VARCHAR2(4000);
  v_batchid_out_dealer CLOB;
  v_no_deal  NUMBER;
  v_dealer_cnt       NUMBER := 0;
  v_loop_cnt  NUMBER := 0;
  v_company_id   t1900_company.c1900_company_id%TYPE;
  v_batchid varchar2(4000) := '';
  v_range_cnt  NUMBER := 0;
  v_range  NUMBER;
  v_bulk_dealer_cnt		NUMBER;
BEGIN

 SELECT get_compid_frm_cntx() 
	INTO v_company_id  
  FROM dual;

  SELECT COUNT (1)
  INTO v_batch_cnt
  FROM t9600_batch
  WHERE c9600_batch_id = p_batch_id
  AND c9600_void_fl   IS NULL;

  IF v_batch_cnt = 0 THEN
    raise_application_error(-20965,'');
  END IF;
  
  DELETE MY_TEMP_KEY_VALUE;
  gm_ins_temp_dealer(p_batch_id);
  --fetch bulk dealer count from temp table
  v_bulk_dealer_cnt := gm_ins_temp_bulk_dealer(v_company_id);

  p_out_batch_id := p_batch_id;
  --fetch all dealer count from temp table
  SELECT COUNT(1) INTO v_dealer_cnt FROM MY_TEMP_KEY_VALUE;
 
  --if one dealer, no need to split the batch id
  IF v_dealer_cnt      = 1 THEN
  	RETURN;
  END IF;
  -- get without bulk dealer count for loop
	v_dealer_cnt := v_dealer_cnt - v_bulk_dealer_cnt;

  IF v_dealer_cnt      > 0 THEN
	v_no_deal  := get_rule_value_by_company (v_company_id,'BATCH_DEALER_SLT_CNT', v_company_id); --spliting count
    v_loop_cnt := ceil(v_dealer_cnt /v_no_deal ); --returns the smallest integer value that is greater than or equal to a number.
  END IF;
  --split the batch id for bulk dealers(MIC and senko)
  gm_split_batch_bydealers(p_batch_id,p_user_id,v_company_id,v_batchid_out_dealer);

  p_out_batch_id :=  p_out_batch_id || v_batchid_out_dealer;

  -- Already we have one main batch, so we are starting loop from 2
    FOR I IN 2..v_loop_cnt
    LOOP
      gm_sav_batch(p_batch_id,v_batchid);
      --v_range is row number for needs to start the next 20 value 
      v_range:= v_range_cnt * v_no_deal;

	  FOR dealer_dtl IN CUR_BATCH_DTLS(v_no_deal,v_company_id,v_range)
	  LOOP
	  	 gm_ins_batch_details(p_batch_id,dealer_dtl.transid,dealer_dtl.mail_fl,dealer_dtl.dealer_id,dealer_dtl.ref_type,v_batchid,p_user_id);  	
	  END LOOP;
	  
	v_batchid_out :=  v_batchid_out || ', ' || v_batchid;
	--while loop the v_loop_cnt,should increase one count.because we need to fetch next 20 value from temp table.
	v_range_cnt := v_range_cnt + 1;

    END LOOP;
    p_out_batch_id:= p_out_batch_id || v_batchid_out;
END gm_split_multi_batch;

/*************************************************************************
 * Description : This procedure is insert distinct dealer ids
 *  **************************************************************************/  

PROCEDURE gm_ins_temp_dealer ( 
	p_batch_id     IN T9600_BATCH.C9600_BATCH_ID%TYPE)
AS

	BEGIN
		     INSERT INTO MY_TEMP_KEY_VALUE 
		     	(MY_TEMP_TXN_ID) 
		     SELECT distinct t501.c101_dealer_id
		  	FROM t9600_batch t9600,t9601_batch_details t9601, t501_order t501
	  		WHERE t9600.c9600_void_fl IS NULL
	  		AND t9601.c9601_void_fl IS NULL
	  		AND t501.c501_void_fl IS NULL
	  		AND t501.c501_order_id = t9601.c9601_ref_id
	  		AND  t9600.c9600_batch_id   = t9601.c9600_batch_id  
	  		AND  t9600.C901_BATCH_STATUS <> 18744 -- processed
	  		and t9600.c9600_batch_id = p_batch_id;
		      	
END gm_ins_temp_dealer;

/*************************************************************************
 * Description : This procedure is fetch bulk dealer count from temp table
 *  **************************************************************************/  

FUNCTION gm_ins_temp_bulk_dealer ( 
	p_company_id   IN T1900_COMPANY.C1900_COMPANY_ID%TYPE
	)
RETURN NUMBER
	IS
		v_bulk_dealer_cnt		NUMBER;
	BEGIN
		BEGIN
		      
		   SELECT count(DISTINCT MY_TEMP_TXN_ID) INTO v_bulk_dealer_cnt
		   FROM MY_TEMP_KEY_VALUE
           WHERE MY_TEMP_TXN_ID IN (SELECT c906_rule_value
				FROM t906_rules
				WHERE c906_rule_grp_id='JPN_BULK_DEALERS'
				AND c1900_company_id = p_company_id
                AND c906_void_fl IS NULL
                );
	
	    RETURN v_bulk_dealer_cnt;
            				
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            RETURN 0;
        END;
		

END gm_ins_temp_bulk_dealer;

/*************************************************************************
 *  Description : Procedure used to save the Splitted batch id and batch details
 *  **************************************************************************/  
PROCEDURE gm_ins_batch_details (
	p_batch_id     IN t9600_batch.c9600_batch_id%TYPE,
	p_transid      IN t501_order.c501_order_id%TYPE,
	p_mail_fl      IN t9601_batch_details.c9601_email_fl%TYPE,
	p_ref_id       IN VARCHAR2,
	p_ref_type     IN t9601_batch_details.c901_ref_type%TYPE,
	p_batch_out_id IN t9600_batch.c9600_batch_id%TYPE,
	p_user_id      IN T9600_BATCH.C9600_CREATED_BY%TYPE
	)
AS
BEGIN
   
      gm_sav_batch_details(p_batch_out_id,p_transid,p_mail_fl,p_ref_type,p_user_id);
      gm_upd_batch_dtls(p_batch_id,p_transid);
      gm_upd_temp_dtls(p_ref_id);

END gm_ins_batch_details;

/*************************************************************************
 * Description : This procedure is save batch id
 *  **************************************************************************/ 
PROCEDURE gm_sav_batch (
	p_batch_id     IN t9600_batch.c9600_batch_id%TYPE,
	p_batchid_out  OUT VARCHAR2
)
AS
 v_batchid t9600_batch.c9600_batch_id%type;
BEGIN
	
	  SELECT s9600_batch.nextval INTO v_batchid FROM DUAL;

        INSERT
        INTO t9600_batch
          (
            c9600_batch_id,c901_batch_type,c901_batch_status,c9600_created_by,c9600_created_date,c9600_initiated_by,
            c9600_initiated_date,c1900_company_id,c901_invoice_closing_date,c9600_date_of_issue_from,c9600_date_of_issue_to
          )
	        SELECT v_batchid,c901_batch_type,c901_batch_status,c9600_created_by,c9600_created_date,c9600_initiated_by ,
	        c9600_initiated_date,c1900_company_id,c901_invoice_closing_date,c9600_date_of_issue_from,c9600_date_of_issue_to
	    FROM t9600_batch
        WHERE c9600_batch_id = p_batch_id; 
        
	p_batchid_out := v_batchid;

END gm_sav_batch;

/*************************************************************************
 * Description : This procedure is save splitted batch details 
 *  **************************************************************************/ 
PROCEDURE gm_sav_batch_details (
	p_batch_id     IN t9600_batch.c9600_batch_id%TYPE,
	p_transid      IN t501_order.c501_order_id%TYPE,
	p_mail_fl      IN t9601_batch_details.c9601_email_fl%TYPE,
	p_ref_type     IN t9601_batch_details.c901_ref_type%TYPE,
	p_user_id      IN T9600_BATCH.C9600_CREATED_BY%TYPE
)
AS
 v_batch_dtls_id t9601_batch_details.c9601_batch_detail_id%type;
BEGIN
	         SELECT s9601_batch_details.nextval INTO v_batch_dtls_id FROM DUAL;
         
          INSERT
          INTO t9601_batch_details
            (
              c9601_batch_detail_id,c9600_batch_id,c9601_ref_id,c901_ref_type,c9601_created_by,c9601_created_date,
              c9601_last_update_by,c9601_last_updated_date,c9601_email_fl
            )
            VALUES
            (
              v_batch_dtls_id,p_batch_id,p_transid,p_ref_type,p_user_id,CURRENT_DATE,
              p_user_id,CURRENT_DATE,p_mail_fl
            );
END gm_sav_batch_details;

/*************************************************************************
 * Description : This procedure is splitted batchid to update the batch details table
 *  **************************************************************************/ 
PROCEDURE gm_upd_batch_dtls (
	p_batch_id     IN t9600_batch.c9600_batch_id%TYPE,
	p_transid      IN t501_order.c501_order_id%TYPE
)
AS
BEGIN
	 ---to avoid duplicates orders in multiple batch.
        UPDATE t9601_batch_details
        SET c9601_void_fl  = 'Y'
        WHERE c9601_REF_ID = p_transid
        AND c9601_void_fl IS NULL
        AND c9600_batch_id = p_batch_id;
END gm_upd_batch_dtls;

/*************************************************************************
 * Description : This procedure is update the key for temp table 
 *  **************************************************************************/ 
PROCEDURE gm_upd_temp_dtls (
	p_ref_id    IN VARCHAR2
)
AS
BEGIN
	 --- to update the dealers to whom batchs are created.
        UPDATE MY_TEMP_KEY_VALUE
        SET MY_TEMP_TXN_KEY  = 'Y'
        WHERE MY_TEMP_TXN_ID = p_ref_id
        AND MY_TEMP_TXN_KEY IS NULL;
END gm_upd_temp_dtls;

/*************************************************************************
 * Description : This procedure is splitted by bulk dealers and save the batch and batch details for the dealer
 *  **************************************************************************/
PROCEDURE gm_split_batch_bydealers ( 
	p_batch_id     IN T9600_BATCH.C9600_BATCH_ID%TYPE,
	p_user_id      IN T9600_BATCH.C9600_CREATED_BY%TYPE,
	p_company_id   IN T1900_COMPANY.C1900_COMPANY_ID%TYPE,
	p_batch_id_instr   OUT VARCHAR2
	)
AS
	CURSOR cur_dealer_dtls(v_dealer_id t501_order.c101_dealer_id%TYPE)
  IS
    SELECT t501.c704_account_id accid ,
      t501.c501_order_id transid,
      t501.c501_order_date ord_dt,
      t501.c501_surgery_date trans_date ,
      t501.c101_dealer_id dealer_id ,t9601.c9601_email_fl mail_fl,
      t9601.c901_ref_type ref_type
    FROM t9600_batch t9600,
      t9601_batch_details t9601,
      t501_order t501
    WHERE t9600.c9600_void_fl   IS NULL
    AND t9601.c9601_void_fl     IS NULL
    AND t501.c501_void_fl       IS NULL
    AND t501.c501_order_id       = t9601.c9601_ref_id
    AND t9600.c9600_batch_id     = t9601.c9600_batch_id
    AND t9600.C901_BATCH_STATUS <> 18744 -- Processed
    AND t9600.c9600_batch_id     = p_batch_id
    AND t9600.C1900_COMPANY_ID   = p_company_id
    AND t501.c101_dealer_id  IN (v_dealer_id); -- MIC , Senko Medical Instrument Mfg
    
      
      CURSOR cur_dealer_id
  IS
      SELECT c906_rule_value v_dealer_id
    FROM t906_rules
    WHERE c906_rule_id   = p_company_id
    AND c906_rule_grp_id = 'JPN_BULK_DEALERS'
    AND C1900_COMPANY_ID = p_company_id
    AND C906_VOID_FL    IS NULL ;

	v_no_deal      NUMBER;
	v_out_dealer_batchid VARCHAR(4000);
	v_bulk_dealer_fl VARCHAR(1);
BEGIN
 	
 	FOR dealer_id IN cur_dealer_id
	LOOP
	--this v_bulk_dealer_fl for creating batch id for the bulk dealer by rule value
		v_bulk_dealer_fl := 'N';
			FOR dealer_dtl IN CUR_DEALER_DTLS(dealer_id.v_dealer_id)
			LOOP
			--If flag is N ,creating new batch id otherwise batch details will updated.
				IF v_bulk_dealer_fl = 'N' THEN
					gm_sav_batch(p_batch_id,v_out_dealer_batchid);
					p_batch_id_instr := p_batch_id_instr || ', ' || v_out_dealer_batchid;
					v_bulk_dealer_fl := 'Y';
				END IF;
			    gm_sav_batch_details(v_out_dealer_batchid,dealer_dtl.transid,dealer_dtl.mail_fl,dealer_dtl.ref_type,p_user_id);
		 	    gm_upd_batch_dtls(p_batch_id,dealer_dtl.transid);
			    gm_upd_temp_dtls(dealer_dtl.dealer_id);
			END LOOP;			
	END LOOP;
	
END gm_split_batch_bydealers;

/******************************************************************
* Description :  This procedure is used to create multiple batch for PO Id's in AR project(PMT-53738) 
****************************************************************/
PROCEDURE gm_ar_split_multi_batch (
    p_batch_id     IN T9600_BATCH.C9600_BATCH_ID%TYPE,
    p_user_id      IN T9600_BATCH.C9600_CREATED_BY%TYPE,
    p_out_batch_id OUT VARCHAR2
    )
AS
	CURSOR cur_po_batch(v_no_po NUMBER,v_company_id t1900_company.c1900_company_id%TYPE,v_range NUMBER)
	IS
		SELECT t501.c704_account_id accid ,
		t501.c501_order_id transid,
		t501.c501_order_date ord_dt,
		T501.C501_Surgery_Date Trans_Date ,
		T501.C501_Customer_Po po_id ,
		t9601.c9601_email_fl mail_fl,
		t9601.c901_ref_type ref_type
		FROM t9600_batch t9600,
		t9601_batch_details t9601,
		t501_order t501
		WHERE t9600.c9600_void_fl IS NULL
		AND t9601.c9601_void_fl IS NULL
		AND t501.c501_void_fl IS NULL
		AND t501.c501_order_id= t9601.c9601_ref_id
		AND t9600.c9600_batch_id= t9601.c9600_batch_id
		AND t9600.C901_BATCH_STATUS <> 18744 -- Processed
		AND t9600.c9600_batch_id= p_batch_id
		AND T9600.C1900_Company_Id= v_company_id
		AND T501.C503_Invoice_Id IS NULL
		AND NVL (t501.c901_order_type, - 999) NOT IN
            (
                 SELECT token FROM v_in_list
            )
		AND t501.C501_Customer_Po IN
									(SELECT MY_TEMP_TXN_ID
									FROM MY_TEMP_KEY_VALUE
									WHERE MY_TEMP_TXN_KEY IS NULL
									AND MY_TEMP_TXN_ID IS NOT NULL
									ORDER BY My_Temp_Txn_Id Offset V_Range Rows
									FETCH NEXT v_no_po Rows Only
									);

      
  v_batch_cnt NUMBER;
  v_batchid_out  VARCHAR2(4000);
  v_no_po  NUMBER;
  v_cnt       NUMBER := 0;
  v_loop_cnt  NUMBER := 0;
  v_company_id   t1900_company.c1900_company_id%TYPE;
  v_batchid varchar2(4000) := '';
  v_range_cnt  NUMBER := 0;
  v_range  NUMBER;
BEGIN

	-- Exclude Order type from Invoice Batch. (2518,2519,2535)
	my_context.set_my_inlist_ctx (get_rule_value ('INVBATEXLIST', 'INVBATEXORDERTYPE'));
	
 SELECT get_compid_frm_cntx() 
	INTO v_company_id  
  FROM dual;

  SELECT COUNT (1)
  INTO v_batch_cnt
  FROM t9600_batch
  WHERE c9600_batch_id = p_batch_id
  AND c9600_void_fl   IS NULL;
  
  
  IF v_batch_cnt = 0 THEN
    raise_application_error(-20965,'');
  END IF;
  
  DELETE FROM MY_TEMP_KEY_VALUE;
  gm_insert_po_temp(p_batch_id);

  --fetch all PO's count from temp table
  SELECT COUNT(1) INTO v_cnt FROM MY_TEMP_KEY_VALUE;
 
  IF v_cnt      = 1 THEN
  	RETURN;
  END IF;

  p_out_batch_id := p_batch_id;
  IF v_cnt      > 0 THEN
	v_no_po  := get_rule_value_by_company (v_company_id,'BATCH_SLT_CNT', v_company_id); --spliting count
    v_loop_cnt := ceil(v_cnt /v_no_po ); --returns the smallest integer value that is greater than or equal to a number.
  END IF;
 
  -- Already we have one main batch, so we are starting loop from 2
    FOR I IN 2..v_loop_cnt
    LOOP
      gm_sav_batch(p_batch_id,v_batchid);
      --v_range is row number for needs to start the next 20 value 
      v_range:= v_range_cnt * v_no_po;

	  FOR po_dtl IN CUR_PO_BATCH(v_no_po,v_company_id,v_range)
	  LOOP
	  	 gm_ins_batch_details(p_batch_id,po_dtl.transid,po_dtl.mail_fl,po_dtl.po_id,po_dtl.ref_type,v_batchid,p_user_id);  	
	  END LOOP;
	  
	v_batchid_out :=  v_batchid_out || ', ' || v_batchid;
	--while loop the v_loop_cnt,should increase one count.because we need to fetch next 20 value from temp table.
	v_range_cnt := v_range_cnt + 1;

    END LOOP;
    p_out_batch_id:= p_out_batch_id || v_batchid_out;
  
END gm_ar_split_multi_batch;

/*************************************************************************
 * Description : This procedure is insert cust po ids
 *  **************************************************************************/  

PROCEDURE gm_insert_po_temp ( 
	p_batch_id    IN T9600_BATCH.C9600_BATCH_ID%TYPE)
AS

	BEGIN
		     INSERT INTO MY_TEMP_KEY_VALUE 
				(MY_TEMP_TXN_ID) 
			  SELECT T501.C501_Customer_Po
			FROM t9600_batch t9600,t9601_batch_details t9601, t501_order t501
			WHERE t9600.c9600_void_fl IS NULL
			AND t9601.c9601_void_fl IS NULL
			AND t501.c501_void_fl IS NULL
			AND t501.c503_invoice_id IS NULL
            AND t501.c501_customer_po IS NOT NULL
			AND t501.c501_order_id = t9601.c9601_ref_id
			AND t9600.c9600_batch_id= t9601.c9600_batch_id
			AND T9600.C901_Batch_Status <> 18744 -- processed
			AND T9600.C9600_Batch_Id = P_Batch_Id
			GROUP BY T501.C501_Customer_Po;
		      	
END gm_insert_po_temp;

/*************************************************************************
 * Description : This procedure to fetch the email flag for batch 
 *  **************************************************************************/  

PROCEDURE gm_fet_batch_email_fl( 
	p_batch_id    IN T9600_BATCH.C9600_BATCH_ID%TYPE,
	p_email_fl_out  OUT CLOB
	)
AS
	email_fl_str CLOB;
BEGIN

			SELECT RTRIM (XMLAGG (XMLELEMENT (e, t9601.c9601_batch_detail_id||'^'||GET_ACCOUNT_ATTRB_VALUE (t501.c704_account_id, 91983)||'|')) .EXTRACT ('//text()'), ',') 
			INTO email_fl_str
			FROM t9600_batch t9600,t9601_batch_details t9601,t501_order t501
			WHERE t9600.c9600_batch_id = t9601.c9600_batch_id
			AND t9601.C9601_REF_ID = t501.c501_order_id
			AND t9600.C9600_VOID_FL IS NULL
			AND t9601.C9601_VOID_FL IS NULL
			AND t501.c501_void_fl IS NULL
			AND t501.c503_invoice_id IS NULL
			AND t9600.c9600_batch_id = p_batch_id;
			
			p_email_fl_out := email_fl_str;
		
END gm_fet_batch_email_fl;

END gm_pkg_ac_ar_split_batch_trans;
/
