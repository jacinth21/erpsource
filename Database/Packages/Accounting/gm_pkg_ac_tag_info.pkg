/* Formatted on 2010/01/18 12:44 (Formatter Plus v4.8.0) */
--@"c:\database\packages\accounting\gm_pkg_ac_tag_info.pkg";

CREATE OR REPLACE PACKAGE gm_pkg_ac_tag_info
IS
	/*******************************************************
	* Purpose: This procedure will fetch the history of the tags
	* Author : Mihir
	*******************************************************/
	PROCEDURE gm_fch_tag_history (
		p_tagid 		IN		 t5010_tag.c5010_tag_id%TYPE
	  , p_tag_history	OUT 	 TYPES.cursor_type
	);

		/********************************************************************************
		* Description : Procedure to tag info
		* Author	 :	Xun
	********************************************************************************/
	PROCEDURE gm_fc_tag_detail (
		p_refid 	 	IN 	  t5010_tag.c5010_last_updated_trans_id%TYPE
	  , p_num		 	IN 	  t5010_tag.c205_part_number_id%TYPE
	  , p_reftype	 	IN 	  t5010_tag.c901_trans_type%TYPE
	  , p_out_cur1	 	OUT	  TYPES.cursor_type
	  , p_out_cur2	 	OUT	  TYPES.cursor_type
	  , p_intransit_fl	OUT	  VARCHAR2
	);
	
	/*******************************************************
	* Purpose: Procedure to save tag status
	* Author : Jignesh
	*******************************************************/
	PROCEDURE gm_unvoid_field_tag (
		p_txn_id   IN	t5010_tag.c5010_tag_id%TYPE
	  , p_userid   IN	t5010_tag.c5010_last_updated_by%TYPE);
        

	
	PROCEDURE gm_sav_tag_status (
		p_tag_from	IN   t5010_tag.c5010_tag_id%TYPE
  		,p_tag_to	IN   t5010_tag.c5010_tag_id%TYPE
		,p_status	IN	 t5010_tag.c901_status%TYPE
  		,p_user_id	IN   t5010_tag.c5010_created_by%TYPE
);
	
	/********************************************************************************
		* Description : Procedure to save tag history
		* Author	 : Xun
	********************************************************************************/
	PROCEDURE gm_sav_tag_log (
	    p_tagid 		  IN   t5011_tag_log.c5010_tag_id%TYPE
	  , p_control_num	  IN   t5011_tag_log.c5011_control_number%TYPE
	  , p_num		      IN   t5011_tag_log.c205_part_number_id%TYPE
	  , p_set_id		  IN   t5011_tag_log.c207_set_id%TYPE
	  , p_ref_id		  IN   t5011_tag_log.c5011_last_updated_trans_id%TYPE
	  , p_ref_type		  IN   t5011_tag_log.c901_trans_type%TYPE
	  , p_location_id	  IN   t5011_tag_log.c5011_location_id%TYPE
	  , p_location_type	  IN   t5011_tag_log.c901_location_type%TYPE
	  , p_status		  IN   t5011_tag_log.c901_status%TYPE
	  , p_lock_fl		  IN   t5011_tag_log.c5011_lock_fl%TYPE
	  , p_void_fl		  IN   t5011_tag_log.c5011_void_fl%TYPE
	  , p_created_by	  IN   t5011_tag_log.c5011_created_by%TYPE
	  , p_created_date	  IN   t5011_tag_log.c5011_created_date%TYPE
	  , p_account_id	  IN   t5011_tag_log.c704_Account_id%TYPE
	  , p_sub_location_type	    IN   t5011_tag_log.c901_sub_location_type%TYPE
	  , p_sub_location_id	    IN   t5011_tag_log.c5011_sub_location_id%TYPE
	  , p_sub_location_comments	IN   t5011_tag_log.C5011_sub_location_comments%TYPE
	  , p_addressid				IN   t5011_tag_log.c106_address_id%TYPE
	  , p_salerepid				IN   t5011_tag_log.c703_sales_rep_id%TYPE
	  , p_invtype               IN   t5010_tag.c901_inventory_type%TYPE
	  , p_retag_id              IN   t5010_tag.c5010_retag_id%TYPE
	  , p_company_id 	        IN   t5010_tag.c1900_company_id%TYPE DEFAULT NULL
	  , p_plant_id				IN   t5010_tag.c5040_plant_id%TYPE DEFAULT NULL
	  , p_missing_since         IN   t5010_tag.c5010_missing_since%TYPE
	);

	  /*******************************************************
	* Description : Procedure to save tag detail
	* Author	  : Xun
	*******************************************************/
	PROCEDURE gm_sav_tag_detail (
		p_inputs   IN		VARCHAR2
	  , p_userid   IN		t301_vendor.c301_last_updated_by%TYPE
	  , p_errmsg   OUT		VARCHAR2
	);

--
	/****************************************************************
	 * Main procedure which will populate the log table
	 ***************************************************************/
--
	PROCEDURE gm_ac_sav_audit_entry_log (
		p_audit_entry_id			IN	 t2660_audit_entry_log.c2656_audit_entry_id%TYPE
	  , p_field_sales_details_id	IN	 t2660_audit_entry_log.c2651_field_sales_details_id%TYPE
	  , p_tag_id					IN	 t2660_audit_entry_log.c5010_tag_id%TYPE
	  , p_pnum						IN	 t2660_audit_entry_log.c205_part_number_id%TYPE
	  , p_cnum						IN	 t2660_audit_entry_log.c2660_control_number%TYPE
	  , p_setid 					IN	 t2660_audit_entry_log.c207_set_id%TYPE
	  , p_qty						IN	 t2660_audit_entry_log.c2660_qty%TYPE
	  , p_status					IN	 t2660_audit_entry_log.c901_status%TYPE
	  , p_comments					IN	 t2660_audit_entry_log.c2660_comments%TYPE
	  , p_location_type 			IN	 t2660_audit_entry_log.c901_location_type%TYPE
	  , p_location_id				IN	 t2660_audit_entry_log.c2660_location_id%TYPE
	  , p_ref_type					IN	 t2660_audit_entry_log.c901_ref_type%TYPE
	  , p_counted_by				IN	 t2660_audit_entry_log.c2660_counted_by%TYPE
	  , p_counterd_date 			IN	 t2660_audit_entry_log.c2656_counted_date%TYPE
	  , p_void_fl					IN	 t2660_audit_entry_log.c2660_void_fl%TYPE
	  , p_created_by				IN	 t2660_audit_entry_log.c2660_created_by%TYPE
	  , p_created_date				IN	 t2660_audit_entry_log.c2660_created_date%TYPE
	  , p_upload_file_list			IN	 t2660_audit_entry_log.c903_upload_file_list%TYPE
	  , p_ref_id					IN	 t2660_audit_entry_log.c2660_ref_id%TYPE
	  , p_ref_detail				IN	 t2660_audit_entry_log.c2660_ref_details%TYPE
	  , p_entry_type				IN	 t2660_audit_entry_log.c901_entry_type%TYPE
	  , p_reconciliation_status 	IN	 t2660_audit_entry_log.c901_reconciliation_status%TYPE
	  , p_field_sales_ref_lock_id	IN	 t2660_audit_entry_log.c2659_field_sales_ref_lock_id%TYPE
	  , p_posting_option			IN	 t2660_audit_entry_log.c901_posting_option%TYPE
	  , p_posting_comment			IN	 t2660_audit_entry_log.c2656_posting_comment%TYPE
	  , p_retag_id					IN	 t2660_audit_entry_log.c5010_retag_id%TYPE
	  , p_set_count_fl				IN 	 t2660_audit_entry_log.c2656_set_count_fl%TYPE
	  , p_retag_fl				IN 	 t2660_audit_entry_log.c2656_retag_fl%TYPE
	  , p_part_count_fl				IN 	 t2660_audit_entry_log.c2656_part_count_fl%TYPE
	  , p_tag_status				IN 	 t2660_audit_entry_log.c901_tag_status%TYPE
	  , p_address_id				IN 	 t2660_audit_entry_log.c106_address_id%TYPE
	  , p_address_other				IN 	 t2660_audit_entry_log.c2656_address_others%TYPE
	  , p_flagged_by				IN 	 t2660_audit_entry_log.c2656_flagged_by%TYPE
	  , p_flagged_dt				IN 	 t2660_audit_entry_log.c2656_flagged_dt%TYPE
	  , p_approved_by				IN 	 t2660_audit_entry_log.c2656_approved_by%TYPE
	  , p_approved_dt				IN 	 t2660_audit_entry_log.c2656_approved_dt%TYPE
	  , p_reconciled_by				IN 	 t2660_audit_entry_log.c2656_reconciled_by%TYPE
	  , p_reconciled_dt				IN 	 t2660_audit_entry_log.c2656_reconciled_dt%TYPE
	);
--
	PROCEDURE gm_sav_changed_locations (
  	    p_updated_by		  IN   VARCHAR2	
	  , p_inputstr		  IN   VARCHAR2
	);
/*******************************************************
	* Purpose: This procedure will fetch the tag transfer details
	* Author : Ritesh
	*******************************************************/
	PROCEDURE gm_fch_edit_tag (
		p_tagid 		IN		 t5010_tag.c5010_tag_id%TYPE
	  , p_tag_dtl	OUT 	 TYPES.cursor_type
	);

/********************************************************************************
		* Description : Procedure to save tag details
		* Author	 : Ritesh
	********************************************************************************/
	PROCEDURE gm_sav_edit_tag (
        p_tagid       IN t5010_tag.c5010_tag_id%TYPE,
        p_num         IN t5010_tag.c205_part_number_id%TYPE,
        p_consignedto IN t5010_tag.C5010_LOCATION_ID%TYPE,
        p_set_id      IN t5010_tag.c207_set_id%TYPE,
        p_control_num IN t5010_tag.c5010_control_number%TYPE,
        p_status      IN t5010_tag.c901_status%TYPE,
        p_ref_id      IN t5010_tag.C5010_LAST_UPDATED_TRANS_ID%TYPE,
        p_loctype     IN t5010_tag.c901_location_type%TYPE,
        p_invtype     IN t5010_tag.c901_inventory_type%TYPE,
        p_userid      IN t5010_tag.c5010_created_by%TYPE,
        p_comments    IN t5010_tag.c5010_sub_location_comments%TYPE,
        p_missingdt   IN t5010_tag.C5010_MISSING_SINCE%TYPE,
        p_out_msg OUT VARCHAR2) ;

     /*******************************************************
	* Purpose: This procedure will fetch the history of the audit Entry tag
	* Author : Mihir
	*******************************************************/
	PROCEDURE gm_fch_fa_entry_history (
		p_audit_entry_id 		IN		 t2660_audit_entry_log.c2656_audit_entry_id%TYPE
	  , p_audit_entry_history	OUT 	 TYPES.cursor_type
	);

	
    /*******************************************************
    * Description : Procedure to fetch Tag details for given tag_id
    * Author  : Yogabalakrishnan
    *******************************************************/
    PROCEDURE gm_fch_tag_info (
        p_tag_id IN t5010_tag.c5010_tag_id%TYPE,
        p_out_loc_id OUT t5053_location_part_mapping.c5052_location_id%TYPE,
        p_out_txn_id OUT t504_consignment.c504_consignment_id%TYPE,
        p_out_txn_type OUT t504_consignment.c504_type%TYPE
    );
    /*******************************************************
    * Description : Procedure to fetch Plant Details based on the Company ID
    * Author  : HREDDI
    *******************************************************/
    PROCEDURE gm_fch_plant_dtls (
        p_company_id 		IN t5010_tag.c1900_company_id%TYPE,     
        p_out_plant_info 	OUT TYPES.cursor_type,
        p_default_plant     OUT VARCHAR
    );
     /*******************************************************
    * Description : Procedure to validate tag ID
    * Author  : ppandiyan
    *******************************************************/   
    PROCEDURE gm_validate_tag_id (
        p_tagid 	  IN	 t5010_tag.c5010_tag_id%TYPE, 
        p_tag_cnt     OUT    NUMBER
    );
    /***********************************************************************
    * Description : Procedure used to validate the Tag details
    * Author  : mmuthusamy
    *******************************************************/   
    PROCEDURE gm_validate_edit_tag (
    p_tagid       IN t5010_tag.c5010_tag_id%TYPE,
        p_num         IN t5010_tag.c205_part_number_id%TYPE,
        p_consignedto IN t5010_tag.C5010_LOCATION_ID%TYPE,
        p_set_id      IN t5010_tag.c207_set_id%TYPE,
        p_control_num IN t5010_tag.c5010_control_number%TYPE,
        p_status      IN t5010_tag.c901_status%TYPE,
        p_ref_id      IN t5010_tag.C5010_LAST_UPDATED_TRANS_ID%TYPE,
        p_loctype     IN t5010_tag.c901_location_type%TYPE
    );
    
END gm_pkg_ac_tag_info;
/
