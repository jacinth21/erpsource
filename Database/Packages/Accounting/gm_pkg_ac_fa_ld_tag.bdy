/* Formatted on 2012/22/08 17:40 (Formatter Plus v4.8.0) */
--@"c:\database\packages\accounting\gm_pkg_ac_fa_ld_tag.bdy"
CREATE OR REPLACE
PACKAGE BODY gm_pkg_ac_fa_ld_tag
IS
    --
    /**********************************************************************
    *Purpose: Procedure scheduled as job to execute the field audit load.
    **********************************************************************/
PROCEDURE gm_ac_exec_fa_tag_job(
	p_audit_id t2650_field_sales_audit.c2650_field_sales_audit_id%TYPE DEFAULT NULL)
AS
    v_error_message   VARCHAR2 (2000) ;
    v_status          VARCHAR2 (2000) ;
    v_dist_status_cnt NUMBER;
    v_austat_cnt      NUMBER;
    v_message         VARCHAR2 (4000) ;
    v_audit_id t2650_field_sales_audit.c2650_field_sales_audit_id%TYPE;
    CURSOR exec_job_cur
    IS
        SELECT DISTINCT t2651.c2650_field_sales_audit_id auditid, t2650.c2650_audit_name audit_name, NVL (
            t2650.c2650_last_updated_by, t2650.c2650_created_by) audituserid
           FROM t2650_field_sales_audit t2650, t2651_field_sales_details t2651
          WHERE t2650.c2650_field_sales_audit_id  = t2651.c2650_field_sales_audit_id
            AND t2651.c2650_field_sales_audit_id <> 1
            AND t2651.c2650_field_sales_audit_id = NVL(p_audit_id, t2651.c2650_field_sales_audit_id)
            AND t2651.c901_reconciliation_status  = 18522 -- Open
            AND t2650.c2650_void_fl              IS NULL
            AND t2651.c2651_void_fl              IS NULL;
    CURSOR cur_exec_job_detail
    IS
         SELECT t2651.c2650_field_sales_audit_id faid, t2651.c2651_field_sales_details_id fsdetid,
            t2651.c701_distributor_id distid, get_distributor_name (t2651.c701_distributor_id) dname,
            t2651.c901_reconciliation_status recstatus, NVL (C2651_LAST_UPDATED_BY, C2651_CREATED_BY) userid,
            t2652.c2652_field_sales_lock_id flockid, t2652.c701_distributor_id lockdist, get_field_sales_tag_qty (
            t2651.c701_distributor_id) tottag, get_user_name (t2658.c2658_audit_user_id) auditorname,t2650.c2650_audit_name auditname
           FROM t2650_field_sales_audit t2650, t2651_field_sales_details t2651, t2652_field_sales_lock t2652
          , t2658_audit_team t2658
          WHERE t2650.c2650_field_sales_audit_id   = t2651.c2650_field_sales_audit_id
            AND t2650.c2650_field_sales_audit_id   = v_audit_id
            AND t2651.c2650_field_sales_audit_id   = t2652.c2650_field_sales_audit_id
            AND t2652.c701_distributor_id          = t2651.c701_distributor_id
            AND t2658.c2651_field_sales_details_id = t2651.c2651_field_sales_details_id
            AND t2651.c2650_field_sales_audit_id  <> 1
            AND t2651.c901_reconciliation_status   = 18522-- 'OPEN'
            AND t2651.c2651_void_fl               IS NULL
            AND t2652.c2652_void_fl               IS NULL
            AND t2658.c2658_void_fl               IS NULL
       ORDER BY t2651.C2651_LAST_UPDATED_DATE;
BEGIN
    FOR exec_job IN exec_job_cur
    LOOP
        v_audit_id              := exec_job.auditid;
        FOR var_exec_job_detail IN cur_exec_job_detail
        LOOP
            IF var_exec_job_detail.tottag > 0 THEN
                BEGIN
                    gm_pkg_ac_fa_ld_tag.gm_ac_ld_tag_main (var_exec_job_detail.flockid, var_exec_job_detail.distid,
                    var_exec_job_detail.userid) ;
                    --Update Date t2652_field_sales_lock
                     UPDATE t2652_field_sales_lock
                    SET c2652_locked_by                = var_exec_job_detail.userid, c2652_locked_date = SYSDATE
                      WHERE c2652_field_sales_lock_id  = var_exec_job_detail.flockid
                        AND c2650_field_sales_audit_id = exec_job.auditid
                        AND c701_distributor_id        = var_exec_job_detail.distid;
                    --Update Lock Status t2651_field_sales_details  (18523- Locked)
                     UPDATE t2651_field_sales_details
                    SET c901_reconciliation_status       = 18523, c2651_last_updated_by = var_exec_job_detail.userid,
                        c2651_last_updated_date          = SYSDATE
                      WHERE c2651_field_sales_details_id = var_exec_job_detail.fsdetid
                        AND c2650_field_sales_audit_id   = exec_job.auditid
                        AND c701_distributor_id          = var_exec_job_detail.distid;
                    v_status                            := 'S';
                    v_error_message                     := 'SUCCESS';
                EXCEPTION
                WHEN OTHERS THEN
                    v_status        := 'E';
                    v_error_message := SQLERRM;--FAILED
                END;
                gm_update_log (var_exec_job_detail.fsdetid, 'Field Audit:'||var_exec_job_detail.auditname ||
                'Field Sales:' || var_exec_job_detail.dname ||'status:' || v_error_message, 1,
                var_exec_job_detail.userid, v_message) ;
            END IF;
        END LOOP;
        --Check Field Sales status
         SELECT COUNT (1)
           INTO v_dist_status_cnt
           FROM t2651_field_sales_details
          WHERE c2650_field_sales_audit_id = exec_job.auditid
            AND c901_reconciliation_status = 18523 --Locked
            AND c2651_void_fl             IS NULL;
        --Check Audit Status
         SELECT COUNT (1)
           INTO v_austat_cnt
           FROM t2650_field_sales_audit
          WHERE c2650_field_sales_audit_id = exec_job.auditid
            AND c901_audit_status          = 18501-- Draft
            AND c2650_void_fl             IS NULL;
        IF v_austat_cnt                    > 0 AND v_dist_status_cnt > 0 THEN
            --Update Audit Status (18502- Open)
             UPDATE t2650_field_sales_audit
            SET c901_audit_status              = 18502, c2650_last_updated_by = exec_job.audituserid
              , c2650_last_updated_date        = sysdate
              WHERE c2650_field_sales_audit_id = exec_job.auditid
                AND c2650_void_fl             IS NULL;
        END IF;
    END LOOP;
    --
END gm_ac_exec_fa_tag_job;
--
/****************************************************************
* Main procedure which will call other procedures to load all the
* Field Sales Details
***************************************************************/
--
PROCEDURE gm_ac_ld_tag_main (
        p_fs_lock_id IN t2652_field_sales_lock.c2652_field_sales_lock_id%TYPE,
        p_dist_id    IN t2652_field_sales_lock.c701_distributor_id%TYPE,
        p_user_id    IN t2656_audit_entry.c2656_created_by%TYPE)
AS
    v_lock_count NUMBER;
BEGIN
    --Tag Load
     SELECT COUNT (1)
       INTO v_lock_count
       FROM t2659_field_sales_ref_lock t2659
      WHERE t2659.c2652_field_sales_lock_id = p_fs_lock_id
        AND t2659.c2659_void_fl            IS NULL;
    IF v_lock_count                         > 0 THEN
        GM_RAISE_APPLICATION_ERROR('-20999','118',p_dist_id);
    END IF;
    -- ac ld tags
    gm_ac_ld_tags (p_fs_lock_id, p_dist_id) ;
    -- ac ld
    gm_ac_ld_tag_ops_details (p_fs_lock_id, p_dist_id) ;
    --Ops Vs Tag Diff
    gm_ac_ld_ops_vs_tags (p_fs_lock_id) ;
    --
END gm_ac_ld_tag_main;
--
/****************************************************************
* Load all the tag for this distributor
***************************************************************/
--
PROCEDURE gm_ac_ld_tags (
        p_fs_lock_id IN t2652_field_sales_lock.c2652_field_sales_lock_id%TYPE,
        p_dist_id    IN t701_distributor.c701_distributor_id%TYPE)
AS
    CURSOR tagcur
    IS
         SELECT t5010.c205_part_number_id p_num, t5010.c207_set_id set_id, t5010.C5010_CONTROL_NUMBER cnum
          , 1 qty, t5010.C5010_LAST_UPDATED_TRANS_ID ref_id, t5010.C901_INVENTORY_TYPE ref_type
          , t5010.C5010_LAST_UPDATED_DATE ref_date, t5010.C5010_TAG_ID tagid
           FROM t5010_tag t5010
          WHERE t5010.C5010_LOCATION_ID = TO_CHAR (p_dist_id)
            AND t5010.C5010_VOID_FL    IS NULL;
BEGIN
    FOR tag IN tagcur
    LOOP
        gm_sav_tag_details (p_fs_lock_id, tag.p_num, tag.set_id, tag.cnum, tag.qty, tag.ref_id, tag.ref_type,
        tag.ref_date, tag.tagid) ;
    END LOOP;
END gm_ac_ld_tags;
/****************************************************************
* Load  the details of tag for this distributor
***************************************************************/
--
PROCEDURE gm_sav_tag_details (
        p_fs_lock_id IN t2652_field_sales_lock.c2652_field_sales_lock_id%TYPE,
        p_num        IN t2659_field_sales_ref_lock.c205_part_number_id%TYPE,
        set_id       IN t2659_field_sales_ref_lock.c207_set_id%TYPE,
        cnum         IN t2659_field_sales_ref_lock.c2659_control_number%TYPE,
        qty          IN t2659_field_sales_ref_lock.c2659_qty%TYPE,
        ref_id       IN t2659_field_sales_ref_lock.c2659_ref_id%TYPE,
        ref_type     IN t2659_field_sales_ref_lock.c901_ref_type%TYPE,
        ref_date     IN t2659_field_sales_ref_lock.c2659_ref_date%TYPE,
        tagid        IN t2659_field_sales_ref_lock.c5010_tag_id%TYPE)
AS
BEGIN
     INSERT
       INTO t2659_field_sales_ref_lock
        (
            c2659_field_sales_ref_lock_id, c2652_field_sales_lock_id, c205_part_number_id
          , c207_set_id, c2659_control_number, c2659_qty
          , c2659_ref_id, c901_ref_type, c2659_ref_date
          , c5010_tag_id -- new column
        )
        VALUES
        (
            s2659_field_sales_ref_lock.NEXTVAL, p_fs_lock_id, p_num
          , set_id, NVL(cnum,'NOC#'), qty
          , ref_id, ref_type, ref_date
          , tagid
        ) ;
END gm_sav_tag_details;
--
/****************************************************************
* Load the net qty for the parts that are mapped as taggable parts
***************************************************************/
--
PROCEDURE gm_ac_ld_tag_ops_details
    (
        p_fs_lock_id IN t2652_field_sales_lock.c2652_field_sales_lock_id%TYPE,
        p_dist_id    IN t701_distributor.c701_distributor_id%TYPE
    )
AS
    -- Tag qty where set is not null
    CURSOR tagqty_set_cur
    IS
         SELECT t5010.c205_part_number_id pnum, t5010.c207_set_id setid, COUNT (t5010.C5010_TAG_ID) tqty
            --,t5010.C5010_LOCATION_ID
           FROM t5010_tag t5010
          WHERE C5010_VOID_FL          IS NULL
            AND t5010.C5010_LOCATION_ID = TO_CHAR (p_dist_id)
            AND t5010.c207_set_id      IS NOT NULL
       GROUP BY t5010.c205_part_number_id, t5010.c207_set_id;
    -- Tag qty where set is  null
    CURSOR tagqty_noset_cur
    IS
         SELECT t5010.c205_part_number_id pnum, t5010.c207_set_id setid, COUNT (t5010.C5010_TAG_ID) tqty
           FROM t5010_tag t5010
          WHERE C5010_VOID_FL          IS NULL
            AND t5010.C5010_LOCATION_ID = TO_CHAR (p_dist_id)
            AND t5010.c207_set_id      IS NULL
       GROUP BY t5010.c205_part_number_id, t5010.c207_set_id;
    --Operation qty for consignments not LIKE AA./UNS.
    CURSOR opsqty_notaa_consign_cur
    IS
         SELECT t731.c205_part_number_id pnum, t730.c207_set_id setid, SUM (t731.c731_item_qty) oqty
           FROM t730_virtual_consignment t730, t731_virtual_consign_item t731, t205d_part_attribute t205d,t207_set_master t207
          WHERE t730.c730_virtual_consignment_id = t731.c730_virtual_consignment_id
            AND t730.c701_distributor_id         = p_dist_id
            AND t731.c205_part_number_id         = t205d.c205_part_number_id
            AND t730.c207_set_id        = t207.c207_set_id
            AND t205d.c901_attribute_type        = 92340 -- taggable part
            AND t205d.c205d_attribute_value      = 'Y'
            AND t205d.c205d_void_fl             IS NULL
            AND t207.C207_VOID_FL IS NULL
            AND NVL(t207.c901_set_grp_type,'-9999') NOT  IN (1604) --Additional Items
            AND  NVL(t207.c901_hierarchy,'-9999') NOT IN (20703)--unassigned
       GROUP BY t731.c205_part_number_id, t730.c207_set_id;

    --Operation qty for consignments where set like AA./UNS.make the set id null to that there will not be  ops vs tags diff.
    CURSOR opsqty_aa_consign_cur
    IS
         SELECT t731.c205_part_number_id pnum, '' setid, SUM (t731.c731_item_qty) oqty
           FROM t730_virtual_consignment t730, t731_virtual_consign_item t731, t205d_part_attribute t205d,t207_set_master t207
          WHERE t730.c730_virtual_consignment_id = t731.c730_virtual_consignment_id
           AND t730.c701_distributor_id         = p_dist_id
            AND t731.c205_part_number_id         = t205d.c205_part_number_id
             AND t730.c207_set_id        = t207.c207_set_id
            AND t205d.c901_attribute_type        = 92340 -- taggable part
            AND t205d.c205d_attribute_value      = 'Y'
            AND t205d.c205d_void_fl             IS NULL
             AND t207.c207_void_fl IS NULL
            AND (t207.c901_set_grp_type   IN (1604) OR t207.c901_hierarchy  IN (20703)) -- --Additional Items/unassigned
	      GROUP BY t731.c205_part_number_id, t730.c207_set_id;
    --Operation qty for loaners
    CURSOR opsqty_loaner_cur
    IS
         SELECT t505.c205_part_number_id pnum, t504.c207_set_id setid, SUM (t505.C505_ITEM_QTY) oqty
           FROM t504_consignment t504, t504a_consignment_loaner t504a, t504a_loaner_transaction t504atxn
          , t205d_part_attribute t205d, t505_item_consignment t505
          WHERE t504.c504_status_fl            = '4'
            AND t504.c504_void_fl             IS NULL
            AND t504a.c504a_void_fl           IS NULL
            AND t504.c207_set_id              IS NOT NULL
            AND t504.c504_type                 = 4127
            AND t504a.c504a_status_fl          = 20
            AND t504atxn.c504a_consigned_to_id = p_dist_id
            AND t504.c504_consignment_id       = t504a.c504_consignment_id
            AND t504.c504_consignment_id       = t504atxn.c504_consignment_id
            AND t504atxn.c504a_return_dt      IS NULL
            AND t504atxn.c504a_void_fl        IS NULL
            AND t504.c504_consignment_id       = t505.c504_consignment_id
            AND t505.c205_part_number_id       = t205d.c205_part_number_id
            AND t205d.c901_attribute_type      = 92340
            AND t505.c505_ref_id              IS NULL -- Multiple rows b'coz of LNs
            AND t205d.c205d_attribute_value   IS NOT NULL --Multiple rows
            AND t505.c505_item_qty             > 0 -- 2 taggable parts in consignment,1 with qty 1 and other with 0 qty
       GROUP BY t505.c205_part_number_id, t504.c207_set_id;
BEGIN
    -- Tag qty where set is not null
    FOR tagqty_set IN tagqty_set_cur
    LOOP
        gm_ac_ld_tag_details (p_fs_lock_id, tagqty_set.pnum, tagqty_set.setid, tagqty_set.tqty) ;
    END LOOP;
    -- Tag qty where set is  null
    FOR tagqty_noset IN tagqty_noset_cur
    LOOP
        gm_ac_ld_tag_details (p_fs_lock_id, tagqty_noset.pnum, tagqty_noset.setid, tagqty_noset.tqty) ;
    END LOOP;
    --Operation qty for consignments not LIKE AA.
    FOR opsqty_notaa_consign IN opsqty_notaa_consign_cur
    LOOP
        gm_ac_ld_ops_details (p_fs_lock_id, opsqty_notaa_consign.pnum, opsqty_notaa_consign.setid, opsqty_notaa_consign.oqty) ;
    END LOOP;
     --Operation qty for consignments  LIKE AA.
    FOR opsqty_aa_consign IN opsqty_aa_consign_cur
    LOOP
        gm_ac_ld_ops_details (p_fs_lock_id, opsqty_aa_consign.pnum, opsqty_aa_consign.setid, opsqty_aa_consign.oqty) ;
    END LOOP;
    --Operation qty for loaners
    FOR opsqty_loaner IN opsqty_loaner_cur
    LOOP
        gm_ac_ld_ops_details (p_fs_lock_id, opsqty_loaner.pnum, opsqty_loaner.setid, opsqty_loaner.oqty) ;
    END LOOP;
    --System qty will be the qty greater of c2657_ops_qty and c2657_tag_qty
    gm_ac_ld_system_details (p_fs_lock_id) ;
END gm_ac_ld_tag_ops_details;
/****************************************************************
* Common procedure to load tag details into t2657_field_sales_details_lock
***************************************************************/
--
PROCEDURE gm_ac_ld_tag_details (
        p_fs_lock_id IN t2652_field_sales_lock.c2652_field_sales_lock_id%TYPE,
        p_num        IN t2657_field_sales_details_lock.c205_part_number_id%TYPE,
        set_id       IN t2657_field_sales_details_lock.c207_set_id%TYPE,
        tag_qty      IN t2657_field_sales_details_lock.c2657_tag_qty%TYPE)
AS
BEGIN
     INSERT
       INTO t2657_field_sales_details_lock
        (
            c2657_field_sales_details_lock, c2652_field_sales_lock_id, c205_part_number_id
          , c207_set_id, c2657_qty, c2657_tag_qty
        )
        VALUES
        (
            s2659_field_sales_ref_lock.NEXTVAL, p_fs_lock_id, p_num
          , set_id, tag_qty, tag_qty
        ) ;
END gm_ac_ld_tag_details;
/****************************************************************
* Common procedure to load Operatios details into t2657_field_sales_details_lock
***************************************************************/
--
PROCEDURE gm_ac_ld_ops_details
    (
        p_fs_lock_id IN t2652_field_sales_lock.c2652_field_sales_lock_id%TYPE,
        p_num        IN t2657_field_sales_details_lock.c205_part_number_id%TYPE,
        set_id       IN t2657_field_sales_details_lock.c207_set_id%TYPE,
        ops_qty      IN t2657_field_sales_details_lock.c2657_ops_qty%TYPE
    )
AS
BEGIN
     UPDATE t2657_field_sales_details_lock
    SET c2657_ops_qty                 = NVL(c2657_ops_qty,0)+NVL(ops_qty,0)
      WHERE c205_part_number_id       = p_num
        AND NVL(c207_set_id ,'-9999')     = NVL(set_id ,'-9999') 
        AND c2652_field_sales_lock_id = p_fs_lock_id
        AND c2657_void_fl            IS NULL;
    IF SQL%ROWCOUNT                   = 0 THEN
         INSERT
           INTO t2657_field_sales_details_lock
            (
                c2657_field_sales_details_lock, c2652_field_sales_lock_id, c205_part_number_id
              , c207_set_id, c2657_ops_qty
            )
            VALUES
            (
                s2659_field_sales_ref_lock.NEXTVAL, p_fs_lock_id, p_num
              , set_id, ops_qty
            ) ;
    END IF;
END gm_ac_ld_ops_details;
/****************************************************************
* Procedure to load system qty details into t2657_field_sales_details_lock
* System qty will be the qty greater of c2657_ops_qty and c2657_tag_qty
***************************************************************/
--
PROCEDURE gm_ac_ld_system_details
    (
        p_fs_lock_id IN t2652_field_sales_lock.c2652_field_sales_lock_id%TYPE
    )
AS
    CURSOR sysqty_cur
    IS
         SELECT C2657_FIELD_SALES_DETAILS_LOCK fsdetid, c2657_tag_qty tqty, c2657_ops_qty oqty
           FROM t2657_field_sales_details_lock
          WHERE c2652_field_sales_lock_id = p_fs_lock_id
            AND c2657_void_fl            IS NULL;
BEGIN
     UPDATE t2657_field_sales_details_lock
    SET c2657_tag_qty                 = 0
    WHERE c2652_field_sales_lock_id = p_fs_lock_id
    AND c2657_tag_qty           IS NULL
    AND c2657_void_fl            IS NULL;
    --
  UPDATE t2657_field_sales_details_lock
    SET  c2657_ops_qty = 0
    WHERE c2652_field_sales_lock_id = p_fs_lock_id
    AND c2657_ops_qty             IS NULL
    AND c2657_void_fl            IS NULL;

    FOR sysqty IN sysqty_cur
    LOOP
        IF NVL (sysqty.oqty, 0) > NVL (sysqty.tqty, 0) THEN
             UPDATE t2657_field_sales_details_lock
            SET c2657_qty                          = sysqty.oqty
              WHERE C2657_FIELD_SALES_DETAILS_LOCK = sysqty.fsdetid
                AND c2657_void_fl                 IS NULL;
        END IF;
    END LOOP;
END gm_ac_ld_system_details;
--
/****************************************************************
* Procedure to consider Ops vs tag difference
***************************************************************/
--
PROCEDURE gm_ac_ld_ops_vs_tags (
        p_fs_lock_id IN t2652_field_sales_lock.c2652_field_sales_lock_id%TYPE)
AS
    v_no_tag VARCHAR2 (20) ;
    v_cnt    NUMBER;
    CURSOR qtydiff_cur
    IS
         SELECT *
           FROM
            (
                 SELECT C2657_FIELD_SALES_DETAILS_LOCK fsdetid, c205_part_number_id pnum, c207_set_id setid
                  , NVL(c2657_ops_qty,0) - NVL(c2657_tag_qty,0) qtydiff
                   FROM t2657_field_sales_details_lock
                  WHERE c2652_field_sales_lock_id = p_fs_lock_id
                    AND c2657_void_fl            IS NULL
            )
      WHERE qtydiff > 0;
BEGIN
    --Rule value to fetch NOTAG-
    v_no_tag    := get_rule_value ('AUDIT_NOTAG', 'FIELDAUDIT') ;
    v_cnt       := 1;
    FOR qtydiff IN qtydiff_cur
    LOOP
        gm_sav_tag_details (p_fs_lock_id, qtydiff.pnum, qtydiff.setid, 'NOC#', 1, '', NULL, SYSDATE, v_no_tag||v_cnt) ;
        v_cnt := v_cnt + 1;
    END LOOP;
END gm_ac_ld_ops_vs_tags;
--
END gm_pkg_ac_fa_ld_tag;
/