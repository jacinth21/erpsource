--@"c:\database\packages\accounting\gm_pkg_ar_credit_order.bdy"
CREATE OR REPLACE
PACKAGE BODY gm_pkg_ar_credit_order
IS
	/*
	*  Description : Procedure to create entry in item order table from input string for credit return.
	*                It will call when credit qty is less than init/return qty or equal to zero.
	*/
	PROCEDURE gm_sav_partial_credit_ord_dtls (
	        p_raid     IN t506_returns.c506_rma_id%TYPE,
	        p_orderid  IN t501_order.c501_order_id%TYPE,
	        p_inputstr IN CLOB,
	        p_userid   IN t501_order.c501_created_by%TYPE,
	        p_parent_order_id 	IN	t501_order.c501_order_id%TYPE DEFAULT NULL)
	AS
	    v_string CLOB := p_inputstr;
	    v_substring VARCHAR2 (3000) ;
	    v_pnum t502_item_order.c205_part_number_id%TYPE;
	    v_qty t502_item_order.c502_item_qty%TYPE;
	    v_cnum t502_item_order.c502_control_number%TYPE;
	    v_price t502_item_order.c502_item_price%TYPE;
	    v_list_price t502_item_order.c502_list_price%TYPE;
	    v_total_price t501_order.c501_total_cost%TYPE := 0;
	    v_type t502_item_order.c901_type%TYPE;
	    v_unit_price   VARCHAR2(50);
		v_unit_price_adj   VARCHAR2(50);
		v_adj_code   VARCHAR2(50);
		v_adj_code_value 	t502_item_order.c502_adj_code_value%TYPE;
		v_item_order 		t502_item_order.c502_item_order_id%TYPE;
		v_adj_code_per		VARCHAR2(100);
		v_do_unit_price		t502_item_order.c502_do_unit_price%TYPE;
		v_acc_id			t501_order.c704_account_id%TYPE;
		v_party_id			t740_gpo_account_mapping.c101_party_id%TYPE;
		v_company_id t506_returns.c1900_company_id%TYPE;
		v_order_usage_fl  VARCHAR2(10);
		v_hsn_comp_id     t1900_company.c1900_company_id%TYPE;  
		-- new cursor for update the construct flag
		CURSOR cur_ord_dtls
		IS
     		SELECT c502_construct_fl constructfl, c205_part_number_id partnum
       		FROM t502_item_order
      		WHERE c501_order_id = p_parent_order_id
        	AND c502_void_fl IS NULL
        	AND c502_delete_fl IS NULL;
	BEGIN
		

		--
	 SELECT c1900_company_id
	   INTO v_company_id
	   FROM t506_returns
	  WHERE c506_rma_id   = p_raid
	    AND c506_void_fl IS NULL;
	    
	 SELECT get_rule_value_by_company(v_company_id,'GST_COMP',v_company_id) 
	  INTO v_hsn_comp_id 
	  FROM DUAL; 
	  
	  	  
	    WHILE INSTR (v_string, '|') <> 0
	    LOOP
	        --pnum^credit_qty^cnum^item_price^type|
	        v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|')             - 1) ;
	        v_string    := SUBSTR (v_string, INSTR (v_string, '|')                + 1) ;
	        v_pnum      := TRIM (SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1)) ;
	        v_substring := SUBSTR (v_substring, INSTR (v_substring, '^')          + 1) ;
	        v_qty       := SUBSTR (v_substring, 1, INSTR (v_substring, '^')       - 1) ;
	        v_substring := SUBSTR (v_substring, INSTR (v_substring, '^')          + 1) ;
	        v_cnum      := TRIM (SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1)) ;
	        v_substring := SUBSTR (v_substring, INSTR (v_substring, '^')          + 1) ;
	        v_price     := TRIM (SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1)) ;
	        v_substring := SUBSTR (v_substring, INSTR (v_substring, '^')          + 1) ;        
	        v_type	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
			v_unit_price	:= TO_NUMBER (SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1));
			v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
			v_unit_price_adj	:= TO_NUMBER (SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1));
			v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
			v_adj_code	:=  TO_NUMBER (v_substring);
	        
	        v_qty := v_qty * -1;
    	    v_total_price := v_total_price + (v_qty * v_price);
    	    

--    	    PC-643 :: show list price on invoices
    	    SELECT s502_item_order.NEXTVAL, TRIM(GET_PART_PRICE_LIST(v_pnum,'L')) INTO v_item_order, v_list_price
    	    FROM DUAL;
		

    	   
    	  
	         INSERT
	           INTO t502_item_order
	            (
	                c502_item_order_id, c501_order_id, c205_part_number_id
	              , c502_item_qty, c502_control_number, c502_item_price
	              , c901_type , c502_unit_price , c502_unit_price_adj_value ,c901_unit_price_adj_code, c502_list_price
	            )
	            VALUES
	            (
	                v_item_order, p_orderid, v_pnum
	              , v_qty, v_cnum, v_price
	              , v_type , v_unit_price , v_unit_price_adj , v_adj_code, v_list_price
	            ) ;
	            
	         -- Calling the below Procedure to fetch and save the Sub Component Mapping details from T2020 table to t2020a_sub_cmap_trans_details
		     gm_pkg_pd_sub_cmp_mpng.gm_sav_part_mpng_txn_dtls(p_orderid,v_pnum,p_userid);
	            
	    END LOOP;
	  --updating HSN code for the parts (Chennai/Delhi)

	    IF v_company_id = v_hsn_comp_id
				  THEN
					GM_PKG_AC_INVOICE_INFO.gm_upd_hsn_code(p_orderid,'1200',p_userid);
	
		END IF;
		
	    UPDATE t501_order
		   SET c501_total_cost = v_total_price
		   	 , c501_last_updated_by = p_userid
			 , c501_last_updated_date = SYSDATE
		 WHERE c501_order_id = p_orderid;  
		 
		 BEGIN
		 SELECT c704_account_id INTO v_acc_id
		 FROM t501_order
		 WHERE c501_order_id = p_parent_order_id
		 AND c501_void_fl IS NULL;
		  EXCEPTION WHEN NO_DATA_FOUND THEN
		      	v_acc_id := NULL;
		END;
		 BEGIN
		     SELECT t740.c101_party_id 
		     	INTO v_party_id
		     FROM t740_gpo_account_mapping t740 
		     WHERE t740.c704_account_id = v_acc_id
		     AND c740_void_fl IS NULL;
		     EXCEPTION WHEN NO_DATA_FOUND THEN
		      	v_party_id := NULL;
		END;
		 
		v_adj_code_per := gm_pkg_sm_adj_txn.get_adj_code_value(p_parent_order_id,v_pnum,v_item_order, v_adj_code);
		v_adj_code_value	:= ((NVL(v_unit_price,0) - NVL(v_unit_price_adj,0)) * NVL(v_adj_code_per,0)/100);
		-- Get the unit price using below formula, net unit price + unit adjustment value + adjustment code value
	    --v_acc_price := NVL(v_price,0) + NVL(v_unit_price_adj,0) + NVL(v_adj_code_val,0);
		v_do_unit_price := NVL(gm_pkg_sm_adj_rpt.get_account_part_unitprice(v_acc_id,v_pnum, v_party_id),0);
		
		UPDATE t502_item_order
			SET c502_do_unit_price = v_do_unit_price
				, c502_adj_code_value = v_adj_code_value
		WHERE c502_item_order_id = v_item_order;
		-- to update the construct flag 
		FOR ord_det IN cur_ord_dtls 
	    LOOP
 				UPDATE t502_item_order
				SET c502_construct_fl       = ord_det.constructfl
  				WHERE c205_part_number_id = ord_det.partnum
    			AND c501_order_id       = p_orderid
    			AND c502_void_fl IS NULL
    			AND c502_delete_fl IS NULL;
    	END LOOP;
    	-- to update usage details
		v_order_usage_fl   := get_rule_value_by_company (v_company_id, 'ORDER_USAGE', v_company_id) ;
		IF v_order_usage_fl = 'Y' THEN
		    gm_pkg_cs_usage_lot_number.gm_sav_order_info (p_orderid, NULL, p_userid) ;
		END IF;
	END gm_sav_partial_credit_ord_dtls;
END gm_pkg_ar_credit_order;
/