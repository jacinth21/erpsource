/* Formatted on 2010/11/16 11:54 (Formatter Plus v4.8.0) */
--@"c:\database\packages\accounting\gm_pkg_ac_account_transaction.bdy";
CREATE OR REPLACE
PACKAGE BODY gm_pkg_ac_account_transaction
IS
    /***********************************************************************************
    *Description : Procedure save from US OLTP db to Warehouse and Post transaction for Posting And Rollforward report.
    *
    **************************************************************************************/
PROCEDURE gm_sav_account_txn
AS
    v_trans_link_seq T8902_OUS_ACCT_TRANS_LINK.C8902_OUS_TRANS_LINK_ID%TYPE;
    v_txn_company_id     NUMBER;
    v_tmp_country NUMBER;
    v_input_str CLOB;
    v_acct_trans_id T8900_OUS_ACCT_TRANS.c8900_ous_acct_trans_id%TYPE;
    v_txn_type T8900_OUS_ACCT_TRANS.c901_type%TYPE;
    v_userid NUMBER := 30301;
    v_dist_id t701_distributor.c701_distributor_id%TYPE;
    v_post_id      VARCHAR2 (20) ;
    v_source_table VARCHAR2 (20) ;
    v_action       NUMBER;
    v_party_id     VARCHAR2 (20) ;
    v_type         NUMBER;
    v_company_id  t1900_company.c1900_company_id%TYPE:=1000;   -- GMNA Company
    v_plant_id 	  t5040_plant_master.c5040_plant_id%TYPE:=3000; --Audubon Plant
    v_us_time_zone t901_code_lookup.c901_code_nm%TYPE;
    v_us_time_format t901_code_lookup.c901_code_nm%TYPE;
    v_posting_comp t1900_company.c1900_company_id%TYPE;
   
    CURSOR cur_company_id
    IS
         SELECT c1900_company_id company_id
           FROM t8900_ous_acct_trans
          WHERE c901_type IN
            (
                 SELECT c901_code_id FROM t901_code_lookup WHERE c901_code_grp = 'FSTTY'
            )
        AND c8900_trans_fl IS NULL
        AND c8900_void_fl  IS NULL
   GROUP BY c1900_company_id;
  
   CURSOR cur_ous_master
    IS
         SELECT c8900_ous_acct_trans_id id, c901_type txntype, C1900_POSTING_COMP_ID posting_comp
           FROM T8900_OUS_ACCT_TRANS
          WHERE c1900_company_id = v_txn_company_id
          	AND c901_type IN
            (
                 SELECT c901_code_id FROM t901_code_lookup WHERE c901_code_grp = 'FSTTY'
            )
            AND c8900_trans_fl     IS NULL
            AND c8900_void_fl      IS NULL
       GROUP BY c8900_ous_acct_trans_id, c901_type, C1900_POSTING_COMP_ID;
BEGIN

-- For context set	
  SELECT GET_CODE_NAME(C901_TIMEZONE), GET_CODE_NAME(C901_DATE_FORMAT)
            INTO v_us_time_zone,v_us_time_format
        FROM t1900_company
        WHERE c1900_company_id=v_company_id;
	
	gm_pkg_cor_client_context.gm_sav_client_context(v_company_id,v_us_time_zone,v_us_time_format,v_plant_id);
		  

    FOR coun IN cur_company_id
    LOOP
        v_txn_company_id := coun.company_id;
        
        FOR acc_trans IN cur_ous_master
        LOOP
            v_acct_trans_id := acc_trans.id;
            v_txn_type      := acc_trans.txntype;
            v_posting_comp  := acc_trans.posting_comp;
            
            -- PC-3326: Brazil transacation enable - based on company id to get the dist id
            -- get Distributor id
        	v_dist_id     := get_rule_value (v_txn_company_id || '_' || v_posting_comp, 'OUS_ACCOUNT_DIST_MAP') ;
            
            DBMS_OUTPUT.PUT_LINE('v_posting_comp:'||v_posting_comp || ' v_dist_id ==> '|| v_dist_id);
            
			IF v_posting_comp IS NOT NULL
			THEN
				BEGIN
					SELECT C5040_PLANT_ID
		            INTO v_plant_id
		            FROM T5041_PLANT_COMPANY_MAPPING 
					WHERE C5041_VOID_FL IS NULL
					AND C5041_PRIMARY_FL = 'Y'
					AND C1900_COMPANY_ID = v_posting_comp;
					
					gm_pkg_cor_client_context.gm_sav_client_context(v_posting_comp,v_us_time_zone,v_us_time_format,v_plant_id);
					
				EXCEPTION WHEN OTHERS 
				THEN
					v_posting_comp := '';
				END;
			END IF;
            
            
            -- to assign the action based on the txn type.
            v_source_table  := 'FS_INV_ADJ';
            v_type          := 102910; --Adjustments
            IF v_txn_type    = 102902 THEN -- OUS Sales Order
                v_action    := 4302; -- Minus
                v_post_id   := 48224; -- I/C Item Consignment to OUS Sales
            ELSIF v_txn_type = 102903 THEN --OUS Sales Return
                v_action    := 4301; -- Plus
                v_post_id   := 48225; -- OUS Sales to I/C Item Consignment
            ELSIF v_txn_type = 102904 THEN --OUS Quar to Scrap
                v_action    := 4302; -- Minus
                v_post_id   := 48226; -- I/C Item Consignment to OUS Quarantine Scrap
            ELSIF v_txn_type = 102905 THEN -- OUS Shelf to Inventory Adjustment
                v_action    := 4302; -- Minus
                v_post_id   := 48227; -- I/C Item Consignment to OUS Inventory Adjustment
            ELSIF v_txn_type = 102906 THEN -- OUS Inventory Adjustment to Shelf
                v_action    := 4301; -- Plus
                v_post_id   := 48228; -- OUS Inventory Adjustment to I/C Item Consignment
            END IF;
            -- insert to location part mapping tables (T5053).
            gm_pkg_op_inv_field_sales_tran.gm_update_fs_inv (v_acct_trans_id, v_source_table, v_dist_id, v_txn_type,
            v_action, v_type, v_userid) ;
            -- Posting changes
            
            -- to get the party id
            -- v_party_id := get_distributor_inter_party_id (v_transfer_dist_id);
            v_party_id := v_dist_id;
            -- to call the posting procedrue
            gm_pkg_ac_account_transaction.gm_sav_account_posting (v_acct_trans_id, v_post_id, v_party_id, v_userid) ;
            
            -- to update the trans flag as Y
            gm_pkg_ac_ous_acct_transaction.gm_update_trans_flag (v_acct_trans_id, 'Y', v_userid) ;
        END LOOP;-- End Loop master acc_trans
    END LOOP; -- end loop Country
END gm_sav_account_txn;
/***********************************************************************************
*Description : Procedure used to update the Posting details.
*
**************************************************************************************/
PROCEDURE gm_sav_account_posting (
        p_ous_acc_id IN T8900_OUS_ACCT_TRANS.c8900_ous_acct_trans_id%TYPE,
        p_post_id    IN T901_CODE_LOOKUP.c901_CODE_ID%TYPE,
        p_party_id   IN t810_posting_txn.c803_period_id%TYPE,
        p_userid     IN t820_costing.c820_created_by%TYPE)
AS
 	-- International posting changes.
    v_costing_id t820_costing.c820_costing_id%TYPE;
    v_owner_company_id t1900_company.c1900_company_id%TYPE;
    v_local_cost t820_costing.c820_purchase_amt%TYPE;
    v_cost t820_costing.c820_purchase_amt%TYPE;
    --
    CURSOR cur_ous_details
    IS
         SELECT c205_part_number_id pnum, SUM (c8901_item_qty) qty,  NVL(get_ac_costing_id(c205_part_number_id,4913),get_ac_costing_id(c205_part_number_id,4900)) cost_id
           FROM t8901_ous_acct_trans_item
          WHERE c8900_ous_acct_trans_id = p_ous_acc_id
       GROUP BY c205_part_number_id,c8901_item_price;
BEGIN
    FOR acc_trans_dtls IN cur_ous_details
    LOOP
    	-- to get the costing id 
		v_costing_id := acc_trans_dtls.cost_id;
		-- to get the owner information.
		BEGIN
			SELECT t820.c1900_owner_company_id
			  , t820.c820_purchase_amt
			  , t820.c820_local_company_cost
			   INTO v_owner_company_id
			  , v_cost
			  , v_local_cost
			   FROM t820_costing t820
			  WHERE t820.c820_costing_id =v_costing_id;
	   EXCEPTION WHEN NO_DATA_FOUND
	   THEN
	   		v_owner_company_id := NULL;
	   		v_cost:= NULL;
	   		v_local_cost:= NULL;
	   END;
        -- call ledger posting procedure.
        gm_save_ledger_posting (p_post_id, CURRENT_DATE, acc_trans_dtls.pnum, p_party_id, p_ous_acc_id, acc_trans_dtls.qty,
        v_cost, p_userid, NULL, v_owner_company_id, v_local_cost) ;
    END LOOP; -- end loop Posting
END gm_sav_account_posting;
-- new Proess for C- C transfer
/***********************************************************************************
*User   : This procedure used for US OLTP Job.
*Description : Procedure used to create a transfer for Sending and Receiving country.
*
**************************************************************************************/
PROCEDURE gm_sav_transfered_cn 
AS
    v_cur_qty          NUMBER;
    v_total_qty        NUMBER;
    v_txn_company_id       NUMBER;
    v_transfer_dist_id NUMBER;
    v_transfer_cn      VARCHAR2 (40) ;
    v_dist_id          VARCHAR2 (20) ;
    v_input_str CLOB ;
    v_set_id     VARCHAR2 (40) ;
    v_out_tf_id  VARCHAR2 (40) ;
    v_out_cn_id  VARCHAR2 (40) ;
    v_out_ra_id  VARCHAR2 (40) ;
    v_ts_mode    NUMBER;
    v_message    VARCHAR2 (1000) ;
    v_out_errmsg VARCHAR2 (4000) ;
    v_tag_id t5010_tag.c5010_tag_id%TYPE;
    v_consign_id t921_transfer_detail.c921_ref_id%TYPE;
    v_post_id  VARCHAR2 (40) ;
    v_party_id VARCHAR2 (20) ;
    v_request_id t520_request.c520_request_id%TYPE;
    v_shipid t907_shipping_info.c907_shipping_id%TYPE;
    v_type       NUMBER := 102923;--102923--and not  102942 (Country to Country transfer)
    v_userid     NUMBER := 30301;
    subject      VARCHAR2 (1000) ;
    mail_body    VARCHAR2 (30000) ;
    country_msg  VARCHAR2 (32700) ;
    mail_details VARCHAR2 (30000) ;
    to_mail t906_rules.c906_rule_value%TYPE;
    v_msg        VARCHAR2 (100) ;
    v_tag_err_fl BOOLEAN;
    v_send_mail_fl BOOLEAN := FALSE;
    v_ra_id t921_transfer_detail.c921_ref_id%TYPE;
    v_err_cnt    NUMBER := 1;
    v_tag_status NUMBER;
    v_loc_type t5010_tag.c901_location_type%TYPE;
    v_tag_loc t5010_tag.c5010_location_id%TYPE;
    v_tag_pnum t5010_tag.c205_part_number_id%TYPE;
    v_tagcnt NUMBER;
    v_tag_void t5010_tag.c5010_void_fl%TYPE;
    v_void_msg        VARCHAR2 (32700) ;
    v_status_msg      VARCHAR2 (32700) ;
    v_invalid_tag_msg VARCHAR2 (32700) ;
    v_location_msg    VARCHAR2 (32700) ;
    v_table_str       VARCHAR2 (32700) ;
    v_void_str        VARCHAR2 (32700) ;
    v_status_str      VARCHAR2 (32700) ;
    v_invalid_str     VARCHAR2 (32700) ;
    v_location_str    VARCHAR2 (32700) ;
    v_company_id  t1900_company.c1900_company_id%TYPE:=1000;   -- GMNA Company
    v_plant_id 	  t5040_plant_master.c5040_plant_id%TYPE:=3000;  -- Audubon Plant
	v_us_time_zone t901_code_lookup.c901_code_nm%TYPE;
    v_us_time_format t901_code_lookup.c901_code_nm%TYPE;
	 -- International posting changes.
    v_costing_id t820_costing.c820_costing_id%TYPE;
    v_owner_company_id t1900_company.c1900_company_id%TYPE;
    v_local_cost t820_costing.c820_purchase_amt%TYPE;
    v_cost t820_costing.c820_purchase_amt%TYPE;
    
    CURSOR transfered_cur
    IS
         SELECT c1900_company_id company_id, c8900_ous_acct_trans_id txnid, c701_transfer_distributor_id
            transfer_dist, c207_set_id setid, c701_distributor_id distid
           FROM t8900_ous_acct_trans
          WHERE c901_type       = v_type 
            AND c8900_void_fl  IS NULL
            AND c8900_trans_fl IS NULL
       ORDER BY c1900_company_id;
    CURSOR transfered_dtls_cur
    IS
         SELECT c205_part_number_id pnum, c8901_item_qty qty, c8901_control_number cnum
           FROM t8901_ous_acct_trans_item
          WHERE c8900_ous_acct_trans_id = v_transfer_cn; -- v_txn_id
    CURSOR tag_cur
    IS
         SELECT t8903.c5010_tag_id tagid, t8901.c205_part_number_id pnum, t8901.c8901_control_number cnum
          , t8900.c207_set_id setid
           FROM t8900_ous_acct_trans t8900, t8901_ous_acct_trans_item t8901, t8902_ous_acct_trans_link t8902
          , t8903_ous_acct_trans_tag t8903
          WHERE t8900.c8900_ous_acct_trans_id = t8901.c8900_ous_acct_trans_id
            AND t8901.c8900_ous_acct_trans_id = t8902.c8900_ous_acct_trans_id
            AND t8900.c8900_ous_acct_trans_id = t8903.c8900_ous_acct_trans_id
            AND t8903.c205_part_number_id     = t8901.c205_part_number_id
            AND t8900.c1900_company_id     = v_txn_company_id
            AND t8900.c8900_ous_acct_trans_id = v_transfer_cn
            AND t8900.c901_type               = v_type;

 --Transfer From Posting, Returns Posting
     CURSOR return_posting_cur
    IS
         SELECT c205_part_number_id ID, SUM (c507_item_qty) qty, NVL(get_ac_costing_id(c205_part_number_id,4913),get_ac_costing_id(c205_part_number_id,4900))  cost_id
               FROM t507_returns_item
              WHERE c506_rma_id    = v_ra_id --'TFRA-87874'
           GROUP BY c205_part_number_id;

  --Transfer To Posting , Consignment Posting
    CURSOR consign_posting_cur
    IS
         SELECT c205_part_number_id ID, SUM (c505_item_qty) qty,  NVL(get_ac_costing_id(c205_part_number_id,4913),get_ac_costing_id(c205_part_number_id,4900)) cost_id
           FROM t505_item_consignment
          WHERE c504_consignment_id = v_consign_id
       GROUP BY c205_part_number_id;
    
    CURSOR return_tag_dtls_cur
    IS
         SELECT t507.c507_returns_item_id itemid, t8903.c5010_tag_id tagid, t8903.c205_part_number_id pnum
          , t507.c507_control_number control_no, t507.c506_rma_id refid, t507.c507_missing_fl missingfl
           FROM t8900_ous_acct_trans t8900, t920_transfer t920, t921_transfer_detail t921
          , t8903_ous_acct_trans_tag t8903, t507_returns_item t507
          WHERE t8900.c8900_ous_acct_trans_id = t920.c8900_ous_acct_trans_id
            AND t8900.c8900_ous_acct_trans_id = t8903.c8900_ous_acct_trans_id
            AND t920.c920_transfer_id         = t921.c920_transfer_id
            AND t8903.c205_part_number_id     = t507.c205_part_number_id
            AND t921.c921_ref_id              = t507.c506_rma_id
            --AND t8900.c8900_ous_acct_trans_id = v_transfer_cn
            AND t920.c920_transfer_id = v_out_tf_id
            --AND t8900.c901_type               = v_type
            AND t8900.c8900_void_fl IS NULL
            AND t920.c920_void_fl   IS NULL
            AND t921.c921_void_fl   IS NULL;
   BEGIN
	   -- For Context set
	    SELECT GET_CODE_NAME(C901_TIMEZONE), GET_CODE_NAME(C901_DATE_FORMAT)
            INTO v_us_time_zone,v_us_time_format
        FROM t1900_company
        WHERE c1900_company_id=v_company_id;
		  
		  gm_pkg_cor_client_context.gm_sav_client_context(v_company_id,v_us_time_zone,v_us_time_format,v_plant_id);
		  
    v_void_msg        := '<b>Following tag(s) is already voided:</b><br>';
    v_status_msg      := '<b>Following tags can only be updated when the status is Active:</b><br>';
    v_invalid_tag_msg := '<b>Following tag(s) is not valid:</b><br>';
    v_location_msg    := '<b>Following tag(s) is with the following Dist/Rep:</b><br>';
    ------------------------------------------------------------
    subject := 'Cannot update Country to Country transfer tags';
    ----------------------------------------------------------------
    v_table_str   := '<TABLE style="border: 1px solid  #676767">'
                        || '<TR><TD width=100><b>TFID</b></TD><TD width=150><b>Tag #</b></TD>'
                        || '<TD width=150><b>Invalid Tag</b></TD><TD width=100><b>Status</b></TD>'
                        || '<TD width=100><b>Void Flag</b></TD><TD width=200><b>Current Location</b></TD>'
						|| '<TD width=200><b>Transfer From</b></TD><TD width=200><b>Transfer To</b></TD></TR>';
    -- get all the C to C Transfered Transacton.
    FOR ous_trans IN transfered_cur
    LOOP
        v_txn_company_id   := ous_trans.company_id;
        v_transfer_dist_id := ous_trans.transfer_dist;
        v_transfer_cn      := ous_trans.txnid;
        v_set_id           := ous_trans.setid;
        v_dist_id          := ous_trans.distid ;
        -- set the transfer Mode.
        -- 90352 Part Transfer
        -- 90351 Set Transfer
         SELECT DECODE (v_set_id, NULL, 90352, 90351)
           INTO v_ts_mode
           FROM DUAL;
         v_input_str := null;  
         v_send_mail_fl := FALSE;
        -- to form the transfer input string.
        FOR cn_dtls IN transfered_dtls_cur
        LOOP
            -- Part #, Control # and Qty. (Control number is newly added)
            v_input_str := v_input_str || cn_dtls.pnum || '^' || cn_dtls.cnum || '^' || cn_dtls.qty || '|';
        END LOOP; --end cn_dtls
        -- create a new request.
        SELECT 'GM-RQ-' || s520_request.NEXTVAL INTO v_request_id FROM DUAL;
	     INSERT
	       INTO t520_request
	        (
	            c520_request_id, c901_request_source, c520_required_date
	          , c520_request_to, c520_status_fl, c901_ship_to
	          , c520_created_date, c520_created_by, c520_request_for
	          , c207_set_id, c520_ship_to_id, c520_request_date
	          , c901_request_by_type, c1900_company_id, c5040_plant_id
	        )
	        VALUES
	        (
	            v_request_id, 50618, TRUNC (CURRENT_DATE)
	          , v_transfer_dist_id, 40, 4120
	          , CURRENT_DATE, v_userid, 40021
	          , v_set_id, v_transfer_dist_id, TRUNC (CURRENT_DATE)
	          , 50626 -- Sales Rep
	          , v_company_id, v_plant_id
	        ) ;
        -- Initiate the Transfer
        gm_pkg_cs_transfer.gm_cs_sav_transfer (90300 -- Distributor to Distributor
        , v_ts_mode, v_dist_id, v_transfer_dist_id, 90321 -- Inventory transfer
        , TO_CHAR (CURRENT_DATE, 'MM/DD/YYYY') , v_userid, v_set_id, v_input_str, v_out_tf_id) ;
        gm_update_log (v_out_tf_id, 'Transfer made as a part of Inter Company Transfer process', 1208 --Transfer
        , v_userid, v_message) ;
        -- To fetch the consignment ID for this Transfer
         SELECT t921.c921_ref_id
           INTO v_consign_id
           FROM t921_transfer_detail t921
          WHERE t921.c920_transfer_id = v_out_tf_id
            AND t921.c901_link_type   = 90360; -- Consignment
        -- To fetch the Return ID for this Transfer
         SELECT t921.c921_ref_id
           INTO v_ra_id
           FROM t921_transfer_detail t921
          WHERE t921.c920_transfer_id = v_out_tf_id
            AND t921.c901_link_type   = 90361; -- Return    
        -- to update the Ous Account transacton ids. (t8900 - c8900_ous_acct_trans_id)
         UPDATE t920_transfer
        SET c8900_ous_acct_trans_id = v_transfer_cn, c920_last_updated_by = v_userid, c920_last_updated_date = CURRENT_DATE
          WHERE c920_transfer_id    = v_out_tf_id;
        v_input_str                := NULL;
        --Fetch item return id and form the input string (itemid^tagid^missingfl^refid|)
        FOR ra_tag_dtls IN return_tag_dtls_cur
        LOOP
            v_input_str := v_input_str || ra_tag_dtls.itemid || '^' || ra_tag_dtls.tagid || '^' ||
            ra_tag_dtls.missingfl|| '^' || ra_tag_dtls.refid || '|';
        END LOOP; --end cn_dtls
        -- Accept the transfer
        gm_pkg_cs_transfer.gm_cs_sav_accept_transfer (v_out_tf_id, v_userid, 'Y', v_input_str, 0, v_out_errmsg) ;
        gm_update_log (v_out_tf_id, 'Transfer verified as a part of Inter Company Transfer process', 1208, v_userid,
        v_message) ;
        -- to save the shipping information
         SELECT s907_ship_id.NEXTVAL INTO v_shipid FROM DUAL;
	     INSERT
	       INTO t907_shipping_info
	        (
	            c907_shipping_id, c907_ref_id, c901_ship_to
	          , c907_ship_to_id, c907_ship_to_dt, c901_source
	          , c901_delivery_mode, c901_delivery_carrier, c907_status_fl
	          , c907_created_by, c907_created_date, c907_active_fl
	          , c907_release_dt, c907_shipped_dt,c1900_company_id, c5040_plant_id
	        )
	        VALUES
	        (
	            v_shipid, v_consign_id, 4120
	          , v_transfer_dist_id, TRUNC (CURRENT_DATE), 50181
	          , 5031, 5040, 40
	          , v_userid, CURRENT_DATE, 'N'
	          , TRUNC (CURRENT_DATE), TRUNC (CURRENT_DATE),v_company_id, v_plant_id
	        ) ;    
        FOR tags                     IN tag_cur
        LOOP
            v_tag_err_fl := FALSE;
            v_tag_id     := tags.tagid;
             SELECT COUNT (1) INTO v_tagcnt FROM t5010_tag WHERE c5010_tag_id = v_tag_id;
            IF v_tagcnt        = 0 THEN
                v_tag_err_fl  := TRUE;
                v_invalid_str := v_invalid_str || '<tr><td>'||v_out_tf_id || '</td><td>'|| v_tag_id || '</td><td>Y</td><td colspan=5></td><tr>';
            ELSE
                 SELECT t5010.c901_status, t5010.c901_location_type, t5010.c5010_location_id
                  , t5010.c205_part_number_id, t5010.c5010_void_fl
                   INTO v_tag_status, v_loc_type, v_tag_loc
                  , v_tag_pnum, v_tag_void
                   FROM t5010_tag t5010
                  WHERE t5010.c5010_tag_id = v_tag_id;
                IF v_tag_void             IS NOT NULL THEN
                    v_tag_err_fl          := TRUE;
                    v_void_str            := v_void_str || '<tr><td>'||v_out_tf_id || '</td><td>'|| v_tag_id || '</td><td colspan=2></td><td>Y</td><td colspan=3></td><tr>';
                END IF;
                IF v_tag_void IS NULL AND v_tag_status != 51012 -- Active
                    THEN
                    v_tag_err_fl := TRUE;
                    v_status_str := v_status_str || '<tr><td>'||v_out_tf_id || '</td><td>'|| v_tag_id || '</td><td></td><td>'|| get_code_name(v_tag_status)||'</td><td colspan=4></td><tr>';
                END IF;
                IF v_tag_void IS NULL AND v_tag_status = 51012 AND (v_loc_type = 40033 OR v_tag_loc != v_dist_id) --
                    -- 40033 (Inhouse)
                    THEN
                    v_tag_err_fl   := TRUE;
                    v_location_str := v_location_str || '<tr><td>'||v_out_tf_id || '</td><td>'|| v_tag_id || '</td><td colspan=3></td><td>'|| SUBSTR(get_distributor_name (v_tag_loc),1,22)||'</td><td>'|| SUBSTR(get_distributor_name (v_dist_id),1,22)||'</td><td>'|| SUBSTR(get_distributor_name(v_transfer_dist_id),1,22)||'</td><tr>';
                END IF;
               -- IF v_tag_err_fl = FALSE THEN
                     UPDATE t5010_tag t5010
                    SET t5010.c5010_control_number       = tags.cnum, t5010.c5010_last_updated_trans_id = v_consign_id,
                        t5010.c5010_location_id          = v_transfer_dist_id, t5010.c5010_lock_fl = 'Y', t5010.c205_part_number_id =
                        tags.pnum, t5010.c901_trans_type = 51000, --Consignment
                        t5010.c207_set_id                = tags.setid, t5010.c901_location_type = 4120, -- Distributor
                        t5010.c901_status                = 51012, -- Active
                        t5010.c5010_last_updated_by      = v_userid, t5010.c5010_last_updated_date = CURRENT_DATE,
						C1900_COMPANY_ID				 = v_company_id,
						C5040_PLANT_ID					 = v_plant_id
                      WHERE t5010.c5010_tag_id           = v_tag_id
                        AND t5010.c5010_void_fl         IS NULL;
               -- END IF; -- end v_tag_err_fl
            END IF; -- end tag cnt fl
            IF v_tag_err_fl = TRUE
            THEN
            	v_send_mail_fl := TRUE;
            END IF; -- end if v_tag_err_fl
            						
        END LOOP; -- end tags
        -- to update the CN type as ICT (40057) otherwise Consignment Reports not full this record
         UPDATE t504_consignment
        SET c504_type               = 40057, c520_request_id = v_request_id, c504_status_fl = 4,
        c504_last_updated_by = v_userid, c504_last_updated_date = CURRENT_DATE
          WHERE c504_consignment_id = v_consign_id;
     -- Posting changes
        
        v_party_id   := v_transfer_dist_id;
	--Transfer From Posting, Returns Posting
	FOR post_val IN return_posting_cur
        LOOP
           -- to get the costing id 
			v_costing_id := post_val.cost_id;
			-- to get the owner information.
			BEGIN
				SELECT t820.c1900_owner_company_id
				  , t820.c820_purchase_amt
				  , t820.c820_local_company_cost
				   INTO v_owner_company_id
				  , v_cost
				  , v_local_cost
				   FROM t820_costing t820
				  WHERE t820.c820_costing_id =v_costing_id;
			 EXCEPTION WHEN NO_DATA_FOUND
			 THEN
			 	v_owner_company_id := NULL;
			 	v_cost:= NULL;
			 	v_local_cost:= NULL;
			 END;
			 --
			 DBMS_OUTPUT.PUT_LINE(' Trans ID:'|| v_ra_id ||' v_costing_id :'||v_costing_id ||' v_cost :' || v_cost ||' v_local_cost:'|| v_local_cost ||' v_owner_company_id :'|| v_owner_company_id);
            gm_save_ledger_posting (48229, CURRENT_DATE, post_val.ID, v_dist_id, v_ra_id, post_val.qty,
            v_cost, v_userid, NULL, v_owner_company_id, v_local_cost) ;
        END LOOP; -- end Post_val
	--Transfer To Posting , Consignment Posting
        FOR post_val IN consign_posting_cur
        LOOP
            -- to get the costing id 
			v_costing_id := post_val.cost_id;
			-- to get the owner information.
			BEGIN
				SELECT t820.c1900_owner_company_id
				  , t820.c820_purchase_amt
				  , t820.c820_local_company_cost
				   INTO v_owner_company_id
				  , v_cost
				  , v_local_cost
				   FROM t820_costing t820
				  WHERE t820.c820_costing_id =v_costing_id;
			 EXCEPTION WHEN NO_DATA_FOUND
			 THEN
			 	v_owner_company_id := NULL;
			 	v_cost:= NULL;
			 	v_local_cost:= NULL;
			 END;
			-- 
			
            gm_save_ledger_posting (48230, CURRENT_DATE, post_val.ID, v_party_id, v_consign_id, post_val.qty,
            v_cost, v_userid, NULL, v_owner_company_id, v_local_cost) ;
          
        END LOOP; -- end Post_val
       
        -- to update the trans flag as Y
        gm_pkg_ac_ous_acct_transaction.gm_update_trans_flag (v_transfer_cn, 'Y', v_userid) ;
    END LOOP; -- end ous_trans
    -- email message
    IF v_send_mail_fl = TRUE THEN
        mail_body  :=
        '<style>TD{ FONT-SIZE: 11px; COLOR: #000000; FONT-FAMILY: verdana, arial, sans-serif;}</style><font face=arial size="2">';
       
        country_msg := v_table_str || v_void_str || v_status_str || v_invalid_str || v_location_str || '</table>';
        mail_body   := mail_body || 'The following tags cannot be updated for Country to Country transfer.<br><br>'
        || country_msg;
         to_mail := get_rule_value('TRANS_TAG_ISSUE','EMAIL');
         --mail_body :=  substr(mail_body, 0, 3999);
        gm_com_send_html_email (to_mail, subject, 'test', mail_body);
    END IF; -- v_send_mail_fl
END gm_sav_transfered_cn;
END gm_pkg_ac_account_transaction;
/
