/* Formatted on 2009/09/17 10:48 (Formatter Plus v4.8.0) */
--@"C:\Database\Packages\Accounting\gm_pkg_ac_ar_batch_rpt.bdy";
CREATE OR REPLACE
PACKAGE BODY gm_pkg_ac_ar_batch_rpt
IS
    --
    /******************************************************************
    * Description : Procedure to fetch invoice source
    ****************************************************************/
PROCEDURE gm_fch_acct_pending_pos (
        p_recordset OUT TYPES.cursor_type)
AS
v_company_id    t1900_company.c1900_company_id%TYPE;
BEGIN

		SELECT get_compid_frm_cntx()
		  INTO v_company_id 
		  FROM dual;
    -- Exclude Order type from Invoice Batch. (2518,2519,2535)
	my_context.set_my_inlist_ctx (get_rule_value ('INVBATEXLIST', 'INVBATEXORDERTYPE'));
    --
    OPEN p_recordset FOR SELECT accid, accname, COUNT (totalpos) totpo, SUM (costtotalinvoice) totalinvcost,
     SUM (holdfl) holdfl, acc_currency, acc_currency_id FROM
    (
         SELECT t501.c704_account_id accid, get_account_name (t501.c704_account_id) accname, COUNT (1) totalpos
          , SUM (t501.c501_total_cost + NVL (t501.c501_ship_cost, 0)) costtotalinvoice
          , get_cust_po_hold(t501.c704_account_id,t501.c501_customer_po) holdfl
          , get_code_name(t704.c901_currency) acc_currency
          , t704.c901_currency acc_currency_id
           FROM t501_order t501, t704_account t704
          WHERE t501.c704_account_id = t704.c704_account_id
          	AND t501.c501_customer_po                 IS NOT NULL
          	AND t501.c501_order_id NOT IN(SELECT c9601_ref_id 
          							from t9601_batch_details t9601,t9600_batch t9600
          								WHERE t9600.c9600_batch_id  =t9601.c9600_batch_id
              							AND c901_ref_type=18751 
              							AND c9601_void_fl IS NULL 
              							AND t9600.C1900_COMPANY_ID = v_company_id)
            AND t501.c503_invoice_id                  IS NULL
            AND (t501.c901_ext_country_id             IS NULL  OR t501.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH
            AND t501.c501_delete_fl                   IS NULL
            AND t501.c501_void_fl                     IS NULL
            AND NVL (t501.c901_order_type, - 999) NOT IN
            (
                 SELECT token FROM v_in_list
            ) --currently Sales Discount order
            AND t501.C1900_COMPANY_ID = v_company_id
             AND NVL (c901_order_type, -9999) NOT IN (
                SELECT t906.c906_rule_value
                  FROM t906_rules t906
                 WHERE t906.c906_rule_grp_id = 'ORDTYPE'
                   AND c906_rule_id = 'EXCLUDE')
             AND NVL (t501.c901_order_type, -9999) NOT IN (
                SELECT t906.c906_rule_value
                  FROM t906_rules t906
                 WHERE t906.c906_rule_grp_id = 'ORDERTYPE'
                   AND c906_rule_id = 'EXCLUDE')
       GROUP BY t501.c501_customer_po, t501.c704_account_id, t704.c901_currency
    )
    GROUP BY accname, accid, acc_currency, acc_currency_id ORDER BY accname;
END gm_fch_acct_pending_pos;
/******************************************************************
* Description : Procedure to fetch  gm_op_fch_invoice_source
****************************************************************/
PROCEDURE gm_fch_inv_batch_det (
        p_batch_id IN T9600_BATCH.C9600_BATCH_ID%TYPE,
        p_batch_type IN T9600_BATCH.C901_BATCH_TYPE%TYPE,
        p_out_cur OUT TYPES.cursor_type)
AS
v_company_id    t1900_company.c1900_company_id%TYPE;
v_monthly_comp_id   t1900_company.c1900_company_id%TYPE;
BEGIN

	SELECT get_compid_frm_cntx()
		  INTO v_company_id 
		  FROM dual;
		  
	SELECT get_rule_value(v_company_id,'MONTHLY_INV_COMP') 
	  INTO v_monthly_comp_id 
	  FROM DUAL;
	
    -- 18742 (Pending Processing)
    -- 91983	Electronic Email
    -- 91984	Electronic File Format
    -- mdate , If the company as Monthly invoice then the mdate(Invoice Date) would be Date of Issue To else would be order date.
    OPEN p_out_cur FOR   SELECT t501.c501_customer_po po, SUM (t501.c501_total_cost) COST, SUM(NVL (t501.c501_ship_cost, 0)) SHIPCOST, get_account_name (
    t501.c704_account_id) NAME, get_account_dist_name (t501.c704_account_id) dname, DECODE (MIN (t501.c501_status_fl),
    0, 'Back Order', 1, 'Pending Control Number', 2, 'Pending Shipment', 3, 'Shipped') ordstatus, t501.c704_account_id
    ID, DECODE(v_company_id,v_monthly_comp_id,MIN(t9600.c9600_date_of_issue_to),MIN (t501.c501_order_date)) mdate, DECODE(get_cust_po_hold (t501.c704_account_id, t501.c501_customer_po),1,'Y',0,'N')  holdfl
  , MIN (t501.c501_customer_po_date) cust_po_date, t9601.c9601_email_fl BATEMAILREQ
  , GET_ACCOUNT_ATTRB_VALUE (t501.c704_account_id, 91983) EMAILREQ,
    Get_code_name (GET_ACCOUNT_ATTRB_VALUE (t501.c704_account_id, 91984)) EVERSION, t501.c503_invoice_id INVID, t9601.c9601_batch_detail_id BATCH_DETAIL_ID
    , GET_ACCOUNT_ATTRB_VALUE (t501.c704_account_id, 101191) valid_add_fl
    , gm_pkg_ac_sales_tax.get_tax_country_fl(t501.c704_account_id) tax_country_fl
    , CASE WHEN TRUNC (to_date (get_rule_value ('TAX_DATE', 'TAX_INVOICE'), 'mm/DD/YYYY')) <= TRUNC (MIN (t501.c501_order_date)) THEN 'Y'
        ELSE 'N'
    END tax_start_date, get_account_curr_symb(t501.c704_account_id) acc_curr_nm
   FROM t501_order t501, t9600_batch t9600, t9601_batch_details t9601
  WHERE t9600.c9600_batch_id      = t9601.c9600_batch_id
    AND t9601.c9601_ref_id        = t501.c501_order_id
    AND t9600.c9600_batch_id      = p_batch_id
    AND t9600.c901_batch_type     = p_batch_type
    AND NVL(t501.c503_invoice_id,'-999') = DECODE(t9600.c901_batch_status,18741,'-999',18742,'-999',t501.c503_invoice_id)
    AND (t501.c901_ext_country_id IS NULL OR t501.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH
    AND NVL (t501.c901_order_type, -9999) NOT IN (
                SELECT t906.c906_rule_value
                  FROM t906_rules t906
                 WHERE t906.c906_rule_grp_id = 'ORDTYPE'
                   AND c906_rule_id = 'EXCLUDE')
	AND NVL (t501.c901_order_type, -9999) NOT IN (
                SELECT t906.c906_rule_value
                  FROM t906_rules t906
                 WHERE t906.c906_rule_grp_id = 'ORDERTYPE'
                   AND c906_rule_id = 'EXCLUDE')
    AND t501.c501_delete_fl      IS NULL
    AND t501.c501_void_fl        IS NULL
    AND t9600.c9600_void_fl      IS NULL
    AND t9601.c9601_void_fl      IS NULL
GROUP BY t9601.c9601_batch_detail_id, t501.c501_customer_po,t501.c501_ship_cost, t501.c704_account_id, t501.c503_invoice_id, t9601.c9601_email_fl
ORDER BY NVL(t9601.c9601_email_fl,'N'),t9601.c9601_batch_detail_id, NAME, t501.c503_invoice_id;
END gm_fch_inv_batch_det;
/******************************************************************
* Description : Procedure to batch invoice detail  gm_fch_batch_inv_dtls
****************************************************************/
PROCEDURE gm_fch_batch_inv_dtls (
        p_batch_id IN T9600_BATCH.C9600_BATCH_ID%TYPE, 
        p_out_cur OUT TYPES.cursor_type)
AS
v_batch_type	t9600_batch.c901_batch_type%TYPE;
BEGIN
  BEGIN
		SELECT  c901_batch_type INTO v_batch_type 
		FROM t9600_batch 
		WHERE  c9600_batch_id = p_batch_id
		AND c9600_void_fl IS NULL;	 
	EXCEPTION WHEN OTHERS THEN
		v_batch_type := NULL;
	END;
		
		IF v_batch_type = 18751 THEN  --Invoice Batch-Account
			gm_fch_batch_account_inv_dtls(p_batch_id,p_out_cur);
		ELSIF v_batch_type = 18756 THEN --Invoice Batch-Dealer
			gm_fch_batch_dealer_inv_dtls(p_batch_id,p_out_cur);
		END IF;

END gm_fch_batch_inv_dtls;

/******************************************************************
* Description : Procedure to fetch  gm_op_fch_csv_invoice_source
****************************************************************/
PROCEDURE gm_fch_csv_invoice_dtls (
        p_invid IN t503_invoice.c503_invoice_id%TYPE,
        p_headerlist OUT TYPES.cursor_type,
        p_INV_DETAIL OUT TYPES.cursor_type)
AS   
		v_attr_value      VARCHAR2(20) ;
BEGIN
    --
     SELECT t704a.c704a_attribute_value
       INTO v_attr_value
       FROM t704a_account_attribute t704a, t503_invoice t503
      WHERE t704a.C901_ATTRIBUTE_TYPE                                                             = 91986   --CSV Template Name
        AND t704a.c704_account_id                                                                 = t503.c704_account_id
        AND t503.c503_invoice_id                                                                  = p_invid
        AND t704a.c704a_void_fl                                                                  IS NULL
        AND t503.c503_delete_fl                                                                  IS NULL
        AND t503.c503_void_fl                                                                    IS NULL ;
        
    OPEN p_headerlist FOR SELECT c906_rule_value csvheader FROM t906_rules WHERE c906_rule_grp_id = v_attr_value
    ORDER BY c906_rule_seq_id ;
    OPEN p_INV_DETAIL FOR SELECT t501.c503_invoice_id invoice_id,
    rownum invoice_line_number,
    t501.c704_account_id,
    t502.c205_part_number_id item_number,
    get_partnum_desc (t502.c205_part_number_id) description,
    t503.c503_invoice_date invoice_date,
    t502.c502_item_qty quantity,
    t502.c502_item_price unit_price,
    ROUND((t502.c502_item_qty * t502.c502_item_price),2) line_price,
    t503.c503_customer_po po_number,
    get_code_name (GET_PARTNUM_UOM (t502.c205_part_number_id)) unit_of_measure,
    NVL(c502_tax_amt, ROUND((t502.c502_vat_rate / 100 * t502.c502_item_qty * t502.c502_item_price),2)) tax,
    t704.C704_BILL_NAME bill_to_organization_Name,
    t704.C704_BILL_ADD1 ||'-'|| t704.C704_BILL_ADD2 bill_to_street_address,
    DECODE (NVL (GET_RULE_VALUE ('SWAP_ZIP_CODE', 'SWAP_ZIP_CODE'), 'N'), 'Y', t704.C704_BILL_ZIP_CODE,
    t704.C704_BILL_CITY) bill_to_city,
    GET_CODE_NAME_ALT (t704.C704_BILL_STATE) bill_to_state,
    DECODE (NVL (GET_RULE_VALUE ('SWAP_ZIP_CODE', 'SWAP_ZIP_CODE'), 'N'), 'Y', C704_BILL_CITY, C704_BILL_ZIP_CODE)
    bill_to_zip_code,
    t704.C704_SHIP_NAME ship_to_organization_Name,
    t704.C704_SHIP_ADD1 ||'-'|| t704.C704_SHIP_ADD2 ship_to_street_address,
    DECODE (NVL (GET_RULE_VALUE ('SWAP_ZIP_CODE', 'SWAP_ZIP_CODE'), 'N'), 'Y', t704.C704_SHIP_ZIP_CODE,
    t704.C704_SHIP_CITY) ship_to_city,
    GET_CODE_NAME_ALT (t704.C704_SHIP_STATE) ship_to_state,
    DECODE (NVL (GET_RULE_VALUE ('SWAP_ZIP_CODE', 'SWAP_ZIP_CODE'), 'N'), 'Y', C704_SHIP_CITY, C704_SHIP_ZIP_CODE)
    ship_to_zip_code FROM t501_order t501,
    t502_item_order t502,
    t503_invoice t503,
    t704_account t704 WHERE t501.c503_invoice_id = p_invid AND t501.c501_order_id = t502.c501_order_id AND
    t501.c503_invoice_id                         = t503.c503_invoice_id AND t704.c704_account_id = t501.c704_account_id
    AND t501.c501_void_fl                       IS NULL AND t502.c502_void_fl IS NULL AND t503.c503_void_fl IS NULL AND
    t704.c704_void_fl                           IS NULL;
END gm_fch_csv_invoice_dtls;

/******************************************************************
* Description : Procedure to check if the file is created
****************************************************************/
PROCEDURE gm_fch_file_upload (
        p_batchid IN t9600_batch.c9600_batch_id%TYPE,
        p_out OUT TYPES.cursor_type)
AS
BEGIN
    OPEN p_out FOR SELECT t9600.c9600_batch_id batchid,
    t501.c503_invoice_id invid,
    t903.c901_ref_type reftype FROM t501_order t501,
    t9600_batch t9600,
    t9601_batch_details t9601,
    t903_upload_file_list t903 WHERE t9600.c9600_batch_id = t9601.c9600_batch_id AND t9601.c9601_ref_id =
    t501.c501_order_id AND t9600.c9600_batch_id           = NVL (p_batchid, t9600.c9600_batch_id) AND
    t501.c503_invoice_id                                  = t903.c903_ref_id AND t9600.c901_batch_status = 18743 --
    -- Processing In Progress
    AND t9600.c901_batch_type     = 18751 --Invoice Batch
    AND (t501.c901_ext_country_id IS NULL OR t501.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH 
    AND t501.c501_delete_fl IS NULL AND t501.c501_void_fl IS NULL AND
    t9600.c9600_void_fl          IS NULL AND t9601.c9601_void_fl IS NULL ORDER BY t501.c503_invoice_id,
    t903.c901_ref_type;
END gm_fch_file_upload;
/******************************************************************
* Description : Procedure to check if the  log file is created
****************************************************************/
PROCEDURE gm_fch_log_detail (
        p_batchid  IN t9602_batch_log.C9600_BATCH_ID%TYPE,
        p_ref_type IN t9602_batch_log.C901_REF_TYPE%TYPE,
        p_out OUT TYPES.cursor_type)
AS
BEGIN
    OPEN p_out FOR SELECT C9600_BATCH_ID batchid,
    C9602_REF_ID invid,
    C901_ACTION_TYPE actiontype FROM t9602_batch_log WHERE C9600_BATCH_ID = NVL (p_batchid, c9600_batch_id) AND
    C901_REF_TYPE                                                         = p_ref_type order by C9602_REF_ID,
    C901_ACTION_TYPE;
END gm_fch_log_detail;
/******************************************************************
    * Description : Procedure to fetch non batch POS for the particualr account.
    ****************************************************************/
PROCEDURE gm_fch_acc_non_batch_pos (
		p_input_str	 IN	VARCHAR2,
        p_recordset OUT TYPES.cursor_type)
AS
BEGIN
    -- Exclude Order type from Invoice Batch. (2518,2519,2535)
	my_context.set_my_inlist_ctx (get_rule_value ('INVBATEXLIST', 'INVBATEXORDERTYPE'));
	--
	my_context.set_my_double_inlist_ctx (p_input_str,'');
    --
    OPEN p_recordset FOR 
    SELECT t501.*, t501.EMAILREQ BATEMAILREQ FROM
    (SELECT t501.c501_customer_po po, SUM (t501.c501_total_cost + NVL (t501.c501_ship_cost, 0)) COST,SUM(NVL(t501.c501_ship_cost, 0)) SHIPCOST, get_account_name (
    t501.c704_account_id) NAME, get_account_dist_name (t501.c704_account_id) dname, DECODE (MIN (t501.c501_status_fl),
    0, 'Back Order', 1, 'Pending Control Number', 2, 'Pending Shipment', 3, 'Shipped') ordstatus, t501.c704_account_id
    ID, MIN (t501.c501_order_date) mdate,t501.c501_surgery_date surg_date , DECODE(get_cust_po_hold (t501.c704_account_id, t501.c501_customer_po),1,'Y',0,'N')  holdfl
  , MIN (t501.c501_customer_po_date) cust_po_date, GET_ACCOUNT_ATTRB_VALUE (t501.c704_account_id, 91983) EMAILREQ,
    Get_code_name (GET_ACCOUNT_ATTRB_VALUE (t501.c704_account_id, 91984)) EVERSION
  , GET_ACCOUNT_ATTRB_VALUE (t501.c704_account_id, 101191) valid_add_fl
  , gm_pkg_ac_sales_tax.get_tax_country_fl(t501.c704_account_id) tax_country_fl 
  , CASE WHEN TRUNC (to_date (get_rule_value ('TAX_DATE', 'TAX_INVOICE'), 'mm/DD/YYYY')) <= TRUNC (MIN (t501.c501_order_date)) THEN 'Y'
        ELSE 'N'
    END tax_start_date,  '' batch_detail_id
   ,t101.C101_PARTY_NM paracctnm 
   ,t101.c101_party_id paracctid 
   , t501.c704_account_id ACCTID
   , get_account_curr_symb (t501.c704_account_id) acc_curr_nm
   FROM t501_order t501
       ,t704_account t704
       , t101_party t101
  WHERE t501.c501_order_id NOT IN
    (
         SELECT c9601_ref_id FROM t9601_batch_details WHERE c901_ref_type=18751 AND c9601_void_fl IS NULL
    )
     AND t501.c704_account_id = t704.c704_account_id
     AND t704.c101_party_id = t101.c101_party_id
    AND t501.c704_account_id IN
    (
         SELECT token FROM v_double_in_list
    )
    AND t501.c501_customer_po    IS NOT NULL
    AND t501.c503_invoice_id     IS NULL
    AND NVL (c901_order_type, - 999) NOT IN
            (
                 SELECT token FROM v_in_list
            )
    AND (t501.c901_ext_country_id IS NULL OR t501.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH
    AND t501.c501_delete_fl      IS NULL
    AND t501.c501_void_fl        IS NULL
    AND NVL (t501.c901_order_type, -9999) NOT IN (
                SELECT t906.c906_rule_value
                  FROM t906_rules t906
                 WHERE t906.c906_rule_grp_id = 'ORDERTYPE'
                   AND c906_rule_id = 'EXCLUDE')
 GROUP BY t501.c501_customer_po, t501.c704_account_id, t101.c101_party_id, t101.C101_PARTY_NM,t501.c501_surgery_date,t501.c501_ship_cost) t501
 ORDER BY NAME, po;
END gm_fch_acc_non_batch_pos;
/******************************************************************
* Description : Procedure to validate the batch and order details.
****************************************************************/
PROCEDURE gm_validate_batch_ord_barcode (
        p_account_id  IN T501_order.c704_account_id%TYPE,
        p_customer_po IN T501_order.c501_customer_po%TYPE,
        p_user_id     IN t501_order.c501_order_id%TYPE)
AS
    v_inv_id t503_invoice.c503_invoice_id%TYPE;
    v_batch_id T9600_BATCH.C9600_BATCH_ID%TYPE;
    v_inprogress_batch VARCHAR2 (4000) ;
    v_hold_cnt       NUMBER;
    v_inprogress_cnt NUMBER;
    v_cnt            NUMBER;
    v_inv_cnt        NUMBER;
    v_company_id    t1900_company.c1900_company_id%TYPE;
    CURSOR cur_inprogress_batch
    IS
         SELECT t9600.c9600_batch_id batchid
           FROM t9600_batch t9600
          WHERE t9600.c9600_initiated_by = p_user_id
            AND t9600.c901_batch_type    = 18751 -- Invoice Batch
            AND t9600.c901_batch_status  = 18741 -- In progress
            AND t9600.c9600_void_fl     IS NULL
            AND T9600.C1900_COMPANY_ID = v_company_id
       ORDER BY t9600.c9600_batch_id;
BEGIN
	
	    SELECT get_compid_frm_cntx()
		  INTO v_company_id 
		  FROM dual;
		  
    -- check the PO already invoiced.
     SELECT COUNT (1)
       INTO v_inv_cnt
       FROM t501_order
      WHERE c501_customer_po = p_customer_po
        AND c704_account_id  = p_account_id
        AND c503_invoice_id IS NULL
        AND c501_void_fl    IS NULL
        AND c501_delete_fl  IS NULL;
    IF v_inv_cnt             = 0 THEN
        GM_RAISE_APPLICATION_ERROR('-20999','2',p_customer_po);
    END IF;
    -- check the PO - Hold cnt
     SELECT COUNT (1)
       INTO v_hold_cnt
       FROM t501_order
      WHERE c501_customer_po = p_customer_po
        AND c704_account_id  = p_account_id
        AND c503_invoice_id IS NULL
        AND c501_void_fl    IS NULL
        AND c501_delete_fl  IS NULL
        AND c501_hold_fl    IS NOT NULL;
    --
    IF v_hold_cnt <> 0 THEN
        GM_RAISE_APPLICATION_ERROR('-20999','34',p_customer_po);
    END IF;
    -- if Check the PO is part of the batch
     SELECT COUNT (1)
       INTO v_cnt
       FROM t501_order t501, t9601_batch_details t9601, t9600_batch t9600
      WHERE t501.c704_account_id         = p_account_id
        AND t501.c501_customer_po        = p_customer_po
        AND t501.c501_order_id           = t9601.c9601_ref_id
        AND t9600.c9600_batch_id         = t9601.c9600_batch_id
        AND t9601.c901_ref_type          = 18751 -- invoice batch
        AND t9600.c901_batch_status NOT IN (18743, 18744)
        AND (t501.c901_ext_country_id    IS NULL OR t501.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH
        AND t501.c503_invoice_id        IS NULL
        AND t501.c501_delete_fl         IS NULL
        AND t501.c501_void_fl           IS NULL
        AND t9601.c9601_void_fl         IS NULL
        AND t9600.C1900_COMPANY_ID = v_company_id
        AND t9600.c9600_void_fl         IS NULL;
    IF v_cnt                            <> 0 THEN
        -- to get the batch id
         SELECT t9601.c9600_batch_id
           INTO v_batch_id
           FROM t501_order t501, t9601_batch_details t9601, t9600_batch t9600
          WHERE t501.c704_account_id         = p_account_id
            AND t501.c501_customer_po        = p_customer_po
            AND t501.c501_order_id           = t9601.c9601_ref_id
            AND t9600.c9600_batch_id         = t9601.c9600_batch_id
            AND t9600.c901_batch_status NOT IN (18743, 18744)
            AND t9601.c901_ref_type          = 18751 -- invoice batch
            AND (t501.c901_ext_country_id    IS NULL OR t501.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH
            AND t501.c501_delete_fl         IS NULL
            AND t501.c501_void_fl           IS NULL
            AND t9601.c9601_void_fl         IS NULL
            AND t9600.C1900_COMPANY_ID      = v_company_id
            AND t9600.c9600_void_fl         IS NULL
            AND ROWNUM                       = 1;
        --
        GM_RAISE_APPLICATION_ERROR('-20999','35',v_batch_id);
    END IF;
    --
     SELECT COUNT (1)
       INTO v_inprogress_cnt
       FROM t9600_batch t9600
      WHERE t9600.c9600_initiated_by = p_user_id
        AND t9600.c901_batch_type    = 18751 -- Invoice Batch
        AND t9600.c901_batch_status  = 18741 -- In progress
        AND t9600.C1900_COMPANY_ID = v_company_id
        AND t9600.c9600_void_fl     IS NULL;
    IF v_inprogress_cnt              > 1 THEN
        FOR inprogress_batch        IN cur_inprogress_batch
        LOOP
            v_inprogress_batch := v_inprogress_batch || inprogress_batch.batchid ||', ';
        END LOOP;
        --
        v_inprogress_batch := SUBSTR (v_inprogress_batch, 0, LENGTH (v_inprogress_batch) - 2) ;
        GM_RAISE_APPLICATION_ERROR('-20999','19',v_inprogress_batch); 
    END IF;
    --
END gm_validate_batch_ord_barcode;
/*******************************************************************************
* Description : Procedure to fetch user batch and account id, Po information
********************************************************************************/
PROCEDURE gm_fch_ord_batch_barcode_dtls (
        p_orderid IN t501_order.c501_order_id%TYPE,
        p_acc_curr_id IN t704_account.c901_currency%TYPE,
        p_user_id IN t501_order.c501_order_id%TYPE,
        p_out_rpt OUT TYPES.cursor_type)
AS
    v_batch_id T9600_BATCH.C9600_BATCH_ID%TYPE;
    v_account_id T501_order.c704_account_id%TYPE;
    v_customer_po T501_order.c501_customer_po%TYPE;
    v_inv_id t503_invoice.c503_invoice_id%TYPE;
    v_order_cnt NUMBER;
    v_company_id    t1900_company.c1900_company_id%TYPE;
    v_acc_curr_id t704_account.c901_currency%TYPE;
BEGIN
	
	 SELECT get_compid_frm_cntx()
		  INTO v_company_id 
		  FROM dual;
    --
     SELECT COUNT (1)
       INTO v_order_cnt
       FROM t501_order
      WHERE c501_order_id = p_orderid
        AND c501_void_fl IS NULL;
    --
    IF v_order_cnt = 0 THEN
    	GM_RAISE_APPLICATION_ERROR('-20999','69',p_orderid);---20997
    END IF;
    -- get the PO and account id
     SELECT t501.c704_account_id, t501.c501_customer_po, t501.c503_invoice_id
     	, t704.c901_currency
       INTO v_account_id, v_customer_po, v_inv_id
       , v_acc_curr_id
       FROM t501_order t501, t704_account t704
  		WHERE t501.c704_account_id = t704.c704_account_id
  		AND c501_order_id = p_orderid
        AND c501_void_fl IS NULL;
    -- if customer po is empty
    IF v_customer_po IS NULL THEN
        GM_RAISE_APPLICATION_ERROR('-20999','10',p_orderid);
    END IF;
     -- Validate the account currency based order
    IF p_acc_curr_id <> v_acc_curr_id
    THEN
		 GM_RAISE_APPLICATION_ERROR('-20999','70',GET_CODE_NAME_ALT (v_acc_curr_id));
    END IF;
    -- check the order is part of batch and validate the orders
    gm_pkg_ac_ar_batch_rpt.gm_validate_batch_ord_barcode (v_account_id, v_customer_po, p_user_id) ;
    -- to get the batch id
    BEGIN
         SELECT C9600_BATCH_ID
           INTO v_batch_id
           FROM t9600_batch t9600
          WHERE t9600.c9600_initiated_by = p_user_id
            AND t9600.c901_batch_type    = 18751 -- Invoice Batch
            AND t9600.c901_batch_status  = 18741 -- In progress
            AND t9600.c9600_void_fl     IS NULL
            AND t9600.C1900_COMPANY_ID=v_company_id;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_batch_id := NULL;
    END;
    -- fetch the POs, account and batch id.
    OPEN p_out_rpt FOR SELECT v_account_id ACCID,
    v_customer_po po,
    v_inv_id invid,
    v_batch_id batchid FROM DUAL;
END gm_fch_ord_batch_barcode_dtls;

/******************************************************************
* Description : Procedure to fetch the DO Summary Print data 
****************************************************************/
PROCEDURE gm_fch_do_summary_print(
        p_batch_id IN T9600_BATCH.C9600_BATCH_ID%TYPE,
        p_batch_type IN T9600_BATCH.C901_BATCH_TYPE%TYPE,
        p_out_cur OUT TYPES.cursor_type)
AS
BEGIN
    OPEN p_out_cur FOR 
	SELECT T501.C501_ORDER_ID ID, gm_pkg_op_return.get_return_reason (T501.C501_ORDER_ID, 'orders') rareason,
    get_cs_ship_email (4121, T501.c703_sales_rep_id) repemail, GET_ACCOUNT_NAME (T501.C704_ACCOUNT_ID) ANAME,
    GET_DIST_REP_NAME (T501.C703_SALES_REP_ID) REPDISTNM, GET_REP_NAME (T501.C703_SALES_REP_ID) REPNM, GET_SHIP_ADD (
    T501.C501_SHIP_TO, T501.C704_ACCOUNT_ID, T501.C703_SALES_REP_ID, T501.C501_SHIP_TO_ID) SHIPADD_OLD,
    gm_pkg_cm_shipping_info.get_ship_add (t501.c501_order_id, '50180') SHIPADD, T501.C501_ORDER_DATE ODT,
    T501.C501_CUSTOMER_PO PO, GET_BILL_ADD (T501.C704_ACCOUNT_ID) BILLADD
  , T501.C501_TOTAL_COST + NVL (T501.C501_SHIP_COST, 0) SALES, T501.C501_SHIP_COST, T501.C501_COMMENTS COMMENTS
  , GET_USER_NAME (T501.C501_CREATED_BY) UNAME, gm_pkg_cm_shipping_info.get_shipto (t501.c501_order_id, '50180') SHIPTO,
    gm_pkg_cm_shipping_info.get_shipto_id (t501.c501_order_id, '50180') SHIPTOID, gm_pkg_cm_shipping_info.get_track_no (
    t501.c501_order_id, '50180') TRACKNO, gm_pkg_cm_shipping_info.get_carrier (t501.c501_order_id, '50180') CARRIER,
    gm_pkg_cm_shipping_info.get_ship_mode (t501.c501_order_id, '50180') SHIPMODE, T501.C704_ACCOUNT_ID ACCID,
    T501.C901_ORDER_TYPE ORDERTYPE, T501.C901_REASON_TYPE REASONTYPE, T501.C501_PARENT_ORDER_ID PARENTORDID
  , T501.C503_INVOICE_ID INVOICEID, GET_CODE_NAME (T501.C901_ORDER_TYPE) ORDERTYPENAME
  , T501.C501_VOID_FL VOIDFL, T501.C501_RECEIVE_MODE RMODE
  , T501.C501_STATUS_FL SFL, T501.C501_UPDATE_INV_FL INVFL, T501.C501_RECEIVED_FROM RFROM
  , gm_pkg_op_do_process_report.get_order_status_desc (t501.C501_ORDER_ID) STATUSFL, GET_TOTAL_SALES_AMT (
    t501.c501_order_id) total_sales, T501.c703_sales_rep_id repid, get_ac_company_id (T501.C704_ACCOUNT_ID) COMPID
  , GET_TOTAL_ORD_INV_AMT (t501.c501_order_id) total_invoice, t501.c501_customer_po_date cust_po_date
  , get_account_curr_symb(t501.c704_account_id) acc_curr_nm
   FROM T501_ORDER T501, t9601_batch_details T9601
  , t9600_batch t9600
  WHERE t9600.c9600_batch_id      = t9601.c9600_batch_id
    AND t9601.c9601_ref_id        = t501.c501_order_id
    AND t9600.c9600_batch_id      = p_batch_id
    AND t9600.c901_batch_type     = p_batch_type
    AND (t501.c901_ext_country_id IS NULL OR t501.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH
    AND t501.c501_delete_fl      IS NULL
    AND t501.c501_void_fl        IS NULL
    AND t9600.c9600_void_fl      IS NULL
    AND t9601.c9601_void_fl      IS NULL
ORDER BY ANAME, t501.C501_ORDER_ID;
END gm_fch_do_summary_print;

/*****************************************************************************************
* Description : Procedure to fetch the account parameter details based on the Invoice id
* 				1. This procedured used to call - all the invoice print
* 				2. Italy invoice XML
* 				3. Invoice Edit screen  
******************************************************************************************/
PROCEDURE gm_fch_account_param_dtls (
        p_invoice_id IN t503_invoice.c503_invoice_id%TYPE,
        p_out_acc_dtls OUT TYPES.cursor_type)
AS
v_company_id        t1900_company.c1900_company_id%TYPE;
BEGIN
	--
	SELECT get_compid_frm_cntx() 
      INTO v_company_id  
      FROM dual;
    --  
    OPEN p_out_acc_dtls FOR SELECT get_ac_company_id (t503.c704_account_id) companyid
       , GET_ACCOUNT_ATTRB_VALUE (t503.c704_account_id, 91983) emailreq
       , GET_ACCOUNT_ATTRB_VALUE (t503.c704_account_id, 91984) eversion
       , GET_ACCOUNT_ATTRB_VALUE (t503.c704_account_id, 91985) accmailid
       , GET_ACCOUNT_ATTRB_VALUE (t503.c704_account_id, 91987) mpr 
       , t503.c901_invoice_type invtype
       --, DECODE(t704.c901_company_id, 100800, 'Globus Medical',get_code_name(t704.c901_company_id)) companyname
       , get_company_name (t503.c1900_company_id) companyname
       , t503.C1900_COMPANY_ID company_id, get_company_code (t503.C1900_COMPANY_ID) comp_code, c1910_division_id division_id
       , CASE WHEN TRUNC (to_date (get_rule_value ('TAX_DATE', 'TAX_INVOICE'), 'mm/DD/YYYY')) <= TRUNC (gm_pkg_ac_sales_tax.get_min_orders_date (t503.c503_invoice_id)) THEN 'Y' ELSE 'N' END tax_date_fl
       , get_rule_value_by_company(t704.c704_ship_country,'TAX_COUNTRY',v_company_id) taxable_country_fl
       , get_rule_value_by_company (t704.c1900_company_id, 'AVATAX', v_company_id) company_avatax_fl
       -- PMT-44586 Invoice changes for Vatican city (added the exclude VAT)
       , get_account_attrb_value (t503.c704_account_id, 101183) exclude_account_vat
      FROM t503_invoice t503 , t704_account t704
        WHERE t503.c503_invoice_id = p_invoice_id 
    	  AND t503.c704_account_id = t704.c704_account_id 
    	  AND t704.c704_void_fl IS NULL
        AND t503.c503_void_fl IS NULL;
END gm_fch_account_param_dtls;

/******************************************************************
* Description : Procedure to fetch dealer invoice details
****************************************************************/
PROCEDURE gm_fch_dealer_inv_batch_dtls (
        p_batch_id IN T9600_BATCH.C9600_BATCH_ID%TYPE,
        p_batch_type IN T9600_BATCH.C901_BATCH_TYPE%TYPE,
        p_out_cur OUT TYPES.cursor_type)
AS
BEGIN
	
    -- 91983	Electronic Email
    -- 91984	Electronic File Format
    OPEN p_out_cur FOR SELECT t501.c501_customer_po po, SUM (t501.c501_total_cost) COST, SUM(NVL (t501.c501_ship_cost, 0)) SHIPCOST,
    get_party_name (t501.c101_dealer_id) NAME, '' dname, '' ordstatus, t501.c101_dealer_id
    ID, batch.DT_ISSUE_TO mdate, ''  holdfl, MIN (t501.c501_customer_po_date) cust_po_date, batch.BATEMAILREQ BATEMAILREQ, 
    dealer.EMAIl_FMT EVERSION, t501.c503_invoice_id INVID
    , '' valid_add_fl, '' tax_country_fl
    , CASE WHEN TRUNC (to_date (get_rule_value ('TAX_DATE', 'TAX_INVOICE'), 'mm/DD/YYYY')) <= TRUNC (MIN (t501.c501_order_date)) THEN 'Y'
        ELSE 'N'
    END tax_start_date
    , get_acct_curr_symb_by_dealer(t501.c101_dealer_id) acc_curr_nm
   FROM t501_order t501,( SELECT t9601.c9601_ref_id,t9601.c9601_email_fl BATEMAILREQ,t9600.c9600_date_of_issue_to DT_ISSUE_TO FROM t9600_batch T9600,t9601_batch_details T9601
  WHERE t9600.c9600_batch_id = t9601.c9600_batch_id
  and t9600.c9600_batch_id = p_batch_id
  AND t9600.c901_batch_type = p_batch_type
  AND t9600.c9600_void_fl is null
  and t9601.c9601_void_fl is null)batch,(SELECT t101.c101_party_id PID,t101.c101_email_fl EMAILREQ,get_code_name(t101.c901_email_file_fmt) EMAIl_FMT 
  FROM t101_party_invoice_details t101 
  WHERE t101.c101_void_fl IS NULL)dealer
  WHERE  batch.c9601_ref_id        = t501.c501_order_id
    AND t501.c101_dealer_id = dealer.PID
    AND (t501.c901_ext_country_id IS NULL OR t501.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH
    AND NVL (t501.c901_order_type, -9999) NOT IN (
                SELECT t906.c906_rule_value
                  FROM t906_rules t906
                 WHERE t906.c906_rule_grp_id = 'ORDTYPE'
                   AND c906_rule_id = 'EXCLUDE')
	AND NVL (t501.c901_order_type, -9999) NOT IN (
                SELECT t906.c906_rule_value
                  FROM t906_rules t906
                 WHERE t906.c906_rule_grp_id = 'ORDERTYPE'
                   AND c906_rule_id = 'EXCLUDE')
    AND t501.c501_delete_fl      IS NULL
    AND t501.c501_void_fl        IS NULL
GROUP BY t501.c501_customer_po, t501.c503_invoice_id,t501.c101_dealer_id,  batch.BATEMAILREQ,batch.DT_ISSUE_TO,dealer.EMAILREQ,dealer.EMAIl_FMT 
ORDER BY t501.c503_invoice_id;

END gm_fch_dealer_inv_batch_dtls;

/******************************************************************
* Description : Procedure to batch invoice detail based on account
****************************************************************/
PROCEDURE gm_fch_batch_account_inv_dtls (
        p_batch_id IN T9600_BATCH.C9600_BATCH_ID%TYPE, 
        p_out_cur OUT TYPES.cursor_type) 
AS
v_company_id    t1900_company.c1900_company_id%TYPE;
BEGIN
	
	SELECT get_compid_frm_cntx()
		  INTO v_company_id 
		  FROM dual;
		  
	OPEN p_out_cur FOR   SELECT t9600.c9600_batch_id batchid, t501.c503_invoice_id invid, t9601.c9601_email_fl BATEMAILREQ
    , GET_ACCOUNT_ATTRB_VALUE (t501.c704_account_id, 91983) EMAILREQ,  GET_ACCOUNT_ATTRB_VALUE (t501.c704_account_id, 91984)  EVERSION
    , GET_ACCOUNT_ATTRB_VALUE (t501.c704_account_id, 91985)  accmailid , 
    get_ac_company_id (t501.c704_account_id) companyid, GET_ACCOUNT_ATTRB_VALUE (t501.c704_account_id, 91987) mpr
    , t704.c704_account_nm aname, DECODE(t704.c901_company_id, 100800, 'Globus Medical',get_code_name(t704.c901_company_id)) companyname
   FROM t501_order t501, t9600_batch t9600, t9601_batch_details t9601, t704_account t704
  WHERE t9600.c9600_batch_id      = t9601.c9600_batch_id
    AND t9601.c9601_ref_id        = t501.c501_order_id
    AND t704.c704_account_id      = t501.c704_account_id 
    AND t9600.c9600_batch_id      = NVL(p_batch_id, t9600.c9600_batch_id)
    AND t9600.c901_batch_status   = 18743 --Processing In Progress
    AND t9600.c901_batch_type     IN (18751,18756) --Invoice Batch account- dealer
    AND t501.c503_invoice_id IS NOT NULL
    AND (t501.c901_ext_country_id IS NULL OR t501.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH
    AND t501.c501_delete_fl      IS NULL
    AND t501.c501_void_fl        IS NULL
    AND t9600.c9600_void_fl      IS NULL
    AND t9601.c9601_void_fl      IS NULL  
    AND t704.c704_void_fl IS NULL 
    AND t9600.C1900_COMPANY_ID = v_company_id
GROUP BY t9601.c9601_batch_detail_id, t9600.c9600_batch_id, t501.c503_invoice_id , t501.c704_account_id, t9601.c9601_email_fl, t704.c704_account_nm, t704.c901_company_id
ORDER BY t9601.c9601_batch_detail_id, aname, t501.c503_invoice_id;
END   gm_fch_batch_account_inv_dtls;      
 
/******************************************************************
* Description : Procedure to batch invoice detail based on dealers
****************************************************************/
PROCEDURE gm_fch_batch_dealer_inv_dtls (
        p_batch_id IN T9600_BATCH.C9600_BATCH_ID%TYPE, 
        p_out_cur OUT TYPES.cursor_type) 
AS
v_company_id    t1900_company.c1900_company_id%TYPE;
BEGIN
	SELECT get_compid_frm_cntx()
		  INTO v_company_id 
		  FROM dual;
	
		  OPEN p_out_cur FOR   
			 SELECT t9600.c9600_batch_id batchid,t501.c503_invoice_id invid FROM t501_order t501, t9600_batch t9600, t9601_batch_details t9601
 			WHERE t9600.c9600_batch_id     = t9601.c9600_batch_id
		    AND t9601.c9601_ref_id         = t501.c501_order_id
		    AND t9600.c901_batch_status   = 18743 --Processing In Progress
		    AND t9600.c901_batch_type      = 18756  --Invoice Batch dealer
		    AND t501.c503_invoice_id IS NOT NULL
		    AND (t501.c901_ext_country_id IS NULL OR t501.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH
		    AND t501.c501_delete_fl      IS NULL
		    AND t501.c501_void_fl        IS NULL
		    AND t9600.c9600_void_fl      IS NULL
		    AND t9601.c9601_void_fl      IS NULL  
		     AND t9600.C1900_COMPANY_ID   = v_company_id
		    AND t9600.c9600_batch_id      = p_batch_id
		    GROUP BY t9600.c9600_batch_id,t501.c503_invoice_id;

END gm_fch_batch_dealer_inv_dtls;
END gm_pkg_ac_ar_batch_rpt;
/
