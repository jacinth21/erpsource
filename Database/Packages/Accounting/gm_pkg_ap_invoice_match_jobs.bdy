/* Created on 2018/04/20 17:58*/
--@"C:\PMT\db\Packages\Accounting\gm_pkg_ap_invoice_match_jobs.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_ap_invoice_match_jobs
IS 
/*******************************************************
	  * Description : Procedure to get all payment id for accepted quantity greater than order quantity
	  * Author		: SHANMUGAPRIYA 
 *******************************************************/
PROCEDURE gm_update_invoice_sts(
            p_company_id    IN   t1900_company.c1900_company_id%TYPE,
			p_inv_dtl       OUT	 TYPES.cursor_type,
			p_out_batchid   OUT  NUMBER,
			p_total         OUT  t406_payment.c406_payment_amount%TYPE
		 )
	AS
          v_seq_id	   	        t9500_data_download_log.c9500_data_download_id%TYPE;
          p_paymentid 	        VARCHAR2(4000);
      	  v_filename		    VARCHAR2 (200);
		  v_sage_batchid        VARCHAR2 (100);
		  v_comp_dt_fmt         VARCHAR2(20);
		  v_company_id  	    t1900_company.c1900_company_id%TYPE;
    	  v_invoice_tot_amount  t406_payment.c406_payment_amount%TYPE:=0;
    	  v_count               NUMBER:=0;

   cursor inv_match_details
   is
       
       SELECT c406_payment_id, c401_purchase_ord_id,
      	      c406_invoice_id, C406_PAYMENT_AMOUNT              
         FROM t406_payment 
        where c406_status_fl = '15' 
          and c406_payment_amount > 0
          and c406_void_fl is null 
          and c1900_company_id = p_company_id;       
  
BEGIN
	
     SELECT count(1)
       INTO v_count
       FROM (
            SELECT *			       
			  FROM t406_payment t406
			 WHERE c406_status_fl = '15' 
			   AND c406_payment_amount > 0
			   AND c406_void_fl is null 
			   AND c1900_company_id = p_company_id
			);    				
	
    IF (v_count > 0)
	THEN
			
			SELECT get_code_name(c901_date_format)
			  INTO v_comp_dt_fmt
			  FROM t1900_company 
			 WHERE c1900_company_id = p_company_id
			   AND c1900_void_fl IS NULL;
			
			OPEN p_inv_dtl  FOR
				
		    SELECT * FROM (
		    SELECT c406_payment_id ID, c401_purchase_ord_id PURID,
		           c406_invoice_id INVID, get_currency_conversion (get_vendor_currency (t406.c301_vendor_id)   
		         , 1
		         , current_date
		         , NVL (t406.c406_payment_amount, 0)) PAYAMT, get_sage_vendor_id (t406.c301_vendor_id) AVID,
		           get_vendor_name (t406.c301_vendor_id) NAM, TO_CHAR (t406.c406_invoice_dt,v_comp_dt_fmt) invoice_dt		           
		     FROM t406_payment t406
		    WHERE c406_status_fl = '15' 
		      AND c406_payment_amount > 0
		      AND c406_void_fl is null 
		      AND c1900_company_id = p_company_id) 
		    ORDER by NAM asc;		   
		
		   SELECT s9500_data_download_log.NEXTVAL
		     INTO v_seq_id
		     FROM DUAL;
		   
           p_out_batchid := v_seq_id;
		   gm_nextval ('09', 50730, v_sage_batchid);	--(refid, reftype, p_out_batchid);
		   v_sage_batchid := SUBSTR (v_sage_batchid, 2);
				  
		   SELECT TO_CHAR (CURRENT_DATE, 'yy') || v_sage_batchid || '-' || TO_CHAR (CURRENT_DATE, 'YYYYMMDD') || '.xml'
		     INTO v_filename
		     FROM DUAL;
		
		   v_sage_batchid := TO_CHAR (CURRENT_DATE, 'yy') || v_sage_batchid;		
			   
		   INSERT 
		     INTO t9500_data_download_log
				 (c9500_data_download_id, c9500_download_dt,c9500_file_name,c9500_initiated_by,c901_download_type,c9500_created_date, c9500_download_identifier,c1900_company_id)
		   VALUES 
		         (v_seq_id, CURRENT_DATE,v_filename,'30301','50740', CURRENT_DATE, v_sage_batchid,p_company_id);		         		         
						   
		  FOR order_tax IN inv_match_details		  
		  LOOP		  
		
				  gm_pkg_ap_invoice_match_jobs.gm_sav_download_dtls(order_tax.c406_payment_id,v_seq_id);                                                                                                                                      
				  p_paymentid := p_paymentid || order_tax.c406_payment_id || '^';
		
		  END LOOP;
	
		   IF p_paymentid IS NOT NULL
       	   THEN
       	   BEGIN
       				gm_pkg_ac_payment.gm_ac_update_payment (p_paymentid,'30301');
       	   	EXCEPTION WHEN OTHERS
    	   	THEN
    	        p_paymentid := NULL;    		
    	   	END;
       
      	   END IF;
		      
		    SELECT sum(get_currency_conversion (get_vendor_currency (t406.c301_vendor_id)   
		         , 1
		         , t9500.c9500_download_dt
		         , NVL (t406.c406_payment_amount, 0)))
		      INTO v_invoice_tot_amount 
		      FROM t406_payment t406, 
		      	   t9501_data_log_detail t9501, 
		      	   t9500_data_download_log t9500
		     WHERE t406.c406_payment_id         = t9501.c9501_ref_id
		       AND t406.c406_payment_amount     > 0 --This code is added for pmt-6575 to not to save the invoice into xml file.
		       AND t9500.c9500_data_download_id = t9501.c9500_data_download_id
		       AND t406.c1900_company_id        = p_company_id
		       AND t9501.c9500_data_download_id = v_seq_id; --v_seq_id
		    
		    UPDATE t9500_data_download_log 
		       SET c9500_value = v_invoice_tot_amount 
		     WHERE c9500_data_download_id = v_seq_id;
    
    END IF;
    
    p_total := v_invoice_tot_amount;   
    
END gm_update_invoice_sts;
/*******************************************************
	  * Description : Procedure to add new payment  mode
	  * Author		: SHANMUGAPRIYA
	  *******************************************************/
PROCEDURE gm_sav_download_dtls
   (
    p_paymentid IN t406_payment.c406_payment_id%TYPE,
    p_seq_id	IN t9500_data_download_log.C9500_DATA_DOWNLOAD_ID%TYPE
   )
 
AS
	    v_inputstr			     VARCHAR2(200);
        v_type					 t9500_data_download_log.c901_download_type%TYPE;
        v_inputstr	   			 VARCHAR2 (4000) := p_paymentid;
		v_filename	   			 VARCHAR2 (200);
		v_sage_batchid 			 VARCHAR2 (100);
		v_company_id  			 t1900_company.c1900_company_id%TYPE;
		p_out_batchid   		 NUMBER;
	
BEGIN
	SELECT get_compid_frm_cntx()
	INTO v_company_id 
	FROM dual;
	INSERT
	  INTO t9501_data_log_detail
		 (c9501_data_log_detail_id, c9500_data_download_id, c9501_ref_id, c901_ref_type)
	VALUES
	      (s9501_data_log_detail.NEXTVAL, p_seq_id, p_paymentid , '50740');
		
END gm_sav_download_dtls;
/*******************************************************
	  * Description : Procedure to update payment mode
	  * Author		: SHANMUGAPRIYA 
	  *******************************************************/
PROCEDURE gm_ac_update_payment
      (
        p_paymentid IN t406_payment.c406_payment_id%TYPE
       )
AS
      v_strlen       NUMBER := NVL (LENGTH (p_paymentid), 0);
      v_string       VARCHAR2 (4000) := p_paymentid;
      v_status_fl    t406_payment.c406_status_fl%TYPE;
      v_payment_id   t406_payment.c406_payment_id%TYPE;
	  v_dhr_id       VARCHAR2 (20);
	  
      TYPE dhr_array IS TABLE OF t408_dhr.c408_dhr_id%TYPE;
      v_dhr_array    dhr_array; 
BEGIN
	   SELECT t406.c406_status_fl
         INTO v_status_fl
         FROM t406_payment t406
        WHERE t406.c406_payment_id = p_paymentid
       FOR UPDATE;
       
        IF v_status_fl > 20
           --if any value larger than 20 , 20 =  payment released
        THEN
        raise_application_error (-20870, '');
        END IF;           
           
        UPDATE t406_payment
           SET c406_status_fl = '30',
               c406_last_updated_by ='30301' ,
               c406_last_updated_date = CURRENT_DATE,
               c406_download_dt = TRUNC (CURRENT_DATE)
         WHERE t406_payment.c406_payment_id = p_paymentid;
         
END gm_ac_update_payment;
		
END gm_pkg_ap_invoice_match_jobs;
/