/* Formatted on 2009/09/17 10:48 (Formatter Plus v4.8.0) */
-- @"C:\database\Packages\Accounting\gm_pkg_ac_invoice_txn.bdy"

CREATE OR REPLACE
PACKAGE BODY gm_pkg_ac_invoice_txn
IS
   /******************************************************************
   * Description : Procedure to Generate Multiple Invoice based on the Ship Address for the Order.
   ****************************************************************/
	PROCEDURE gm_sav_multiple_invoice (
	  p_custpo	   IN		t503_invoice.c503_customer_po%TYPE
  , p_accid 	   IN		t503_invoice.c704_account_id%TYPE
  , p_payterms	   IN		t503_invoice.c503_terms%TYPE
  , p_invdate	   IN		DATE
  , p_userid	   IN		t503_invoice.c503_created_by%TYPE
  , p_action	   IN		VARCHAR2
  , p_invtype	   IN		t503_invoice.c901_invoice_type%TYPE
  , p_inv_source   IN		t503_invoice.c901_invoice_source%TYPE
  , p_inv_ids	   OUT		CLOB
	)
  AS
	CURSOR order_cur
	IS
	SELECT DISTINCT --C501_ORDER_ID order_id,
	C501_SHIP_TO_ID ship_to_id
	,TO_DATE(TO_CHAR(t501.c501_shipping_date,get_rule_value('DATEFMT','DATEFORMAT')),get_rule_value('DATEFMT','DATEFORMAT')) shipping_dt
	FROM T501_ORDER T501
	WHERE  c501_void_fl    IS NULL
	AND c503_invoice_id IS  NULL
	AND TRIM(C501_CUSTOMER_PO) = TRIM(p_custpo)
	AND c704_account_id= p_accid
	AND NVL(C901_ORDER_TYPE,'-9999') NOT IN (101260)--Acknowledgement Order
	AND (c901_ext_country_id IS NULL
			 OR c901_ext_country_id  in (select country_id from v901_country_codes))
	ORDER BY shipping_dt;
	
	v_inv_ids  CLOB;
	v_shipping_dt VARCHAR2(20);
	
  BEGIN
	
	FOR v_order_cur IN order_cur
	LOOP
		--v_order_id :=v_order_cur.order_id;
		v_shipping_dt := v_order_cur.shipping_dt;
		-- check if shipping date is empty then getting the min order date 
		IF v_shipping_dt IS NULL
		THEN
			SELECT TRUNC (MIN (c501_order_date))
		   INTO v_shipping_dt
		   FROM t501_order t501
		  WHERE t501.c704_account_id               = p_accid
		    AND t501.c501_customer_po              = p_custpo
		    AND NVL (c501_ship_to_id, '-9999')     = NVL (v_order_cur.ship_to_id, NVL (c501_ship_to_id, '-9999'))
		    AND t501.c501_delete_fl               IS NULL
		    AND t501.c501_void_fl                 IS NULL
		    AND T501.c503_invoice_id              IS NULL
		    AND NVL (c901_order_type, - 9999) NOT IN
		    (
		         SELECT t906.c906_rule_value
		           FROM t906_rules t906
		          WHERE t906.c906_rule_grp_id = 'ORDERTYPE'
		            AND c906_rule_id          = 'EXCLUDE'
		    ) ;
		END IF;
		--
		gm_pkg_ac_invoice_txn.gm_sav_single_invoice(p_custpo,p_accid,p_payterms,v_shipping_dt,p_userid,p_action,p_invtype,p_inv_source,v_order_cur.ship_to_id,
				v_order_cur.shipping_dt,v_inv_ids);
		p_inv_ids := p_inv_ids||','||v_inv_ids;
	END LOOP;
	p_inv_ids := substr(p_inv_ids,2);
  END gm_sav_multiple_invoice;

	
/******************************************************************
	  * Description : Procedure to Generate Single Invoice for the Order.
	  ****************************************************************/
	PROCEDURE gm_sav_single_invoice (
		p_custpo	   IN		t503_invoice.c503_customer_po%TYPE
  , p_accid 	   IN		t503_invoice.c704_account_id%TYPE
  , p_payterms	   IN		t503_invoice.c503_terms%TYPE
  , p_invdate	   IN		DATE
  , p_userid	   IN		t503_invoice.c503_created_by%TYPE
  , p_action	   IN		VARCHAR2
  , p_invtype	   IN		t503_invoice.c901_invoice_type%TYPE
  , p_inv_source   IN		t503_invoice.c901_invoice_source%TYPE
  , p_shiptoid     IN           t501_order.C501_SHIP_TO_ID%TYPE
  , p_ship_dt	   IN		VARCHAR2
  , p_inv_id	   OUT		t503_invoice.c503_invoice_id%TYPE
	)
AS

	v_invoice_id   VARCHAR2 (20);
	v_cnt		   NUMBER;
	v_vat_fl	   VARCHAR2(5);
	v_con_vat_fl   VARCHAR2(5);	
	v_company_id   t1900_company.c1900_company_id%TYPE;
	v_division_id  t1910_division.c1910_division_id%TYPE;
	v_rep_id	   t703_sales_rep.c703_sales_rep_id%TYPE;
	v_order_date   DATE;
	v_monthly_comp_id   t1900_company.c1900_company_id%TYPE;
	v_hsn_comp_id   t1900_company.c1900_company_id%TYPE;
	v_inv_source    t503_invoice.c901_invoice_source%TYPE := p_inv_source;
	v_inv_date      DATE;
	v_distributor_id t503_invoice.c701_distributor_id%TYPE;
	
		CURSOR orderlog_cur
	IS
     	SELECT c501_order_id orderid
       	  FROM t501_order
      	WHERE c503_invoice_id  = v_invoice_id
          	AND c503_invoice_id IS NOT NULL
          	AND c501_void_fl    IS NULL;
          	
BEGIN
    	v_inv_date := p_invdate;
    	
	-- Company needs to be taken from the Account and not from Context.
	IF p_accid IS NOT NULL
	THEN
		SELECT C1900_COMPANY_ID 
	      INTO v_company_id  
	      FROM T704_ACCOUNT
	     WHERE C704_ACCOUNT_ID = p_accid;
	ELSE
	SELECT get_compid_frm_cntx() 
      INTO v_company_id  
      FROM dual;
	END IF;
     
    SELECT get_rule_value(v_company_id,'MONTHLY_INV_COMP') 
	  INTO v_monthly_comp_id 
	  FROM DUAL;
--getting the company id to perform operation only for Chennai/Delhi	
    SELECT get_rule_value_by_company(v_company_id,'GST_COMP',v_company_id) 
	  INTO v_hsn_comp_id 
	  FROM DUAL; 

	-- getting rep id
	v_rep_id := TRIM(get_rep_id(p_accid));
	-- getting the division Id
	v_division_id := gm_pkg_sm_mapping.get_salesrep_division(v_rep_id);
	
  --If invoice generate date is prior to order created date .validating the invoice date
  --If it is Monthly Invoice , then validating invoice date with surgery date  
  IF v_company_id = v_monthly_comp_id
  THEN
	         SELECT MIN(c501_surgery_date) 
	           INTO v_order_date
		       FROM t501_order t501
		      WHERE  t501.c704_account_id =  p_accid
		        AND t501.c501_customer_po = p_custpo
		        AND t501.c501_delete_fl      IS NULL
		        AND t501.c501_void_fl        IS NULL
		        AND T501.C503_INVOICE_ID IS NULL
		        AND NVL (c901_order_type, -9999) NOT IN (
               SELECT t906.c906_rule_value
                  FROM t906_rules t906
                 WHERE t906.c906_rule_grp_id = 'ORDERTYPE'
                   AND c906_rule_id = 'EXCLUDE');
   ELSE
   			SELECT MIN(c501_order_date) 
	           INTO v_order_date
		       FROM t501_order t501
		      WHERE  t501.c704_account_id =  p_accid
		        AND t501.c501_customer_po = p_custpo
		        AND t501.c501_delete_fl      IS NULL
		        AND t501.c501_void_fl        IS NULL
		        AND T501.C503_INVOICE_ID IS NULL
		        AND NVL (c901_order_type, -9999) NOT IN (
               SELECT t906.c906_rule_value
                  FROM t906_rules t906
                 WHERE t906.c906_rule_grp_id = 'ORDERTYPE'
                   AND c906_rule_id = 'EXCLUDE');
   			
   END IF;
	
	IF v_order_date > p_invdate OR CURRENT_DATE < p_invdate
	THEN
		raise_application_error (-20672, '');
	END IF;
	
	SELECT COUNT (1)
	  INTO v_cnt
	  FROM t501_order
	 WHERE c501_customer_po = p_custpo 
	   AND c704_account_id = p_accid 
	   AND c501_void_fl IS NULL
	   AND c501_hold_fl IS NOT NULL;

	IF v_cnt > 0
	THEN
		-- Error Messaage. Cannot Invoice this PO as atleast one DO is on Hold Status.
		raise_application_error (-20888, '');
	ELSE
	--to fetch the invoice source for OUS distributor order 50253
	BEGIN		
		SELECT DECODE(get_account_type(p_accid),26240482,'50253',NULL) INTO v_inv_source  --26240482 ICS Distributor
		  FROM DUAL;
	EXCEPTION
	WHEN OTHERS THEN
	  v_inv_source := p_inv_source;
	END;
	v_inv_source := NVL(v_inv_source, p_inv_source);
	--PC-2194: Display Account ID instead of Distributor ID for ICS Customer in Invoice related reports
	  	SELECT COUNT (1)
 	  	  INTO v_cnt
	      FROM T501_ORDER t501, T504C_CONSIGNMENT_ICT_DETAIL t504c
	     WHERE t501.c501_order_id = t504c.c501_order_id
           AND t501.c501_customer_po = p_custpo
	       AND t501.c704_account_id = p_accid 
	       AND c501_void_fl IS NULL 
           AND c504c_void_fl IS NULL;
		  
		  IF v_cnt = 0 THEN 
		  	v_distributor_id := null;
		  ELSE
		  -- To show the distributor id in invoice reports for ICS
			SELECT DECODE(v_inv_source,'50253',get_account_dist_id(p_accid), NULL) 
		      INTO v_distributor_id FROM DUAL;			
		  END IF;
		-- PMT-33782: Invoice sequence getting from t503c table (to avoid missing seq number)
		  
		gm_pkg_ac_invoice_txn.gm_invoice_next_seq (v_inv_source, v_company_id , v_invoice_id);
	
	
		IF p_invtype = '26240439'
		THEN
			v_invoice_id := REPLACE(v_invoice_id,'81-','INV-ST-');
		END IF;
		
		  IF NVL(get_rule_value (v_company_id,'INVOICE_CURRENT_DATE'),'N') = 'Y'  -- Check If germany then curdate
		  THEN
			v_inv_date := CURRENT_DATE;
		  END IF; 
				
		INSERT INTO t503_invoice
					(c503_invoice_id, c704_account_id, c503_invoice_date, c503_terms, c503_customer_po
				   , c503_created_by, c503_created_date, c503_status_fl, c901_invoice_type, c901_invoice_source,c1900_company_id
				   , c1910_division_id, c701_distributor_id
					)
			 VALUES (v_invoice_id, p_accid, v_inv_date, p_payterms, p_custpo
				   , p_userid, CURRENT_DATE, 0, p_invtype, v_inv_source	,v_company_id -- 50255 - generic invoice code
				   , v_division_id, v_distributor_id
					);

/************
   IF p_action = 'ADD'
   THEN
	  UPDATE t501_order
		 SET c503_invoice_id = v_invoice_id
	   WHERE c501_customer_po = p_custpo AND c704_account_id = p_accid AND c503_invoice_id IS NULL;
   END IF;
**************/
		IF p_action = 'ADD'
		THEN
			UPDATE t501_order
			   SET c503_invoice_id = v_invoice_id
			   	 , c501_last_updated_by = p_userid
				 , c501_last_updated_date = CURRENT_DATE
			 WHERE c501_customer_po = p_custpo AND c704_account_id = p_accid AND c503_invoice_id IS NULL AND (c901_ext_country_id IS NULL
			 OR c901_ext_country_id  in (select country_id from v901_country_codes))
			 AND c501_void_fl is null
			 AND NVL(C501_SHIP_TO_ID,'-9999') = NVL(p_shiptoid,NVL(C501_SHIP_TO_ID,'-9999'))
			 AND DECODE(p_ship_dt , NULL, '-999', TRUNC(c501_shipping_date)) = NVL(p_ship_dt , '-999')
			 AND NVL (c901_order_type, -9999) NOT IN (
                SELECT t906.c906_rule_value
                  FROM t906_rules t906
                 WHERE t906.c906_rule_grp_id = 'ORDERTYPE'
                   AND c906_rule_id = 'EXCLUDE')	;

             -- PMT-43914: to get the orders comments and update to Invoice table.
             
             gm_pkg_ac_invoice_txn.gm_sav_invoice_comments_from_orders (v_invoice_id, v_company_id, p_userid);

                   
			--The Closed Status update statement is moved to the below mentioned procedure,This procedure will be called when we are doing issue credit /Debit.
			--This code change is done as part of PMT-5091.
			gm_pkg_ac_invoice.gm_upd_invoice_status(p_inv_source,v_invoice_id,p_userid);

			SELECT COUNT (1)
			  INTO v_cnt
			  FROM t906_rules t906, t906_rules rul
			 WHERE UPPER (t906.c906_rule_grp_id) = 'APPLN'
			   AND UPPER (t906.c906_rule_value) = UPPER (rul.c906_rule_grp_id)
			   AND UPPER (rul.c906_rule_id) = 'VAT' 
			   AND t906.C906_VOID_FL is null ;
            
             -- to update HSN code in item order table for Chennai/Delhi
			  IF v_company_id = v_hsn_comp_id
			  THEN

				GM_PKG_AC_INVOICE_INFO.gm_upd_hsn_code(v_invoice_id,'1201',p_userid);

			  END IF;

			-- to get exclude vat rate flag 
			gm_fch_exclude_vat_fl(p_accid,v_company_id,v_vat_fl);
			
			IF v_cnt > 0
			THEN
			  IF v_vat_fl <> 'Y' 
				THEN
					v_con_vat_fl := NVL(get_rule_value (v_company_id, 'CONSTRUCT_VAT_FL'),'N');
					IF v_con_vat_fl = 'Y'  
					THEN
						gm_pkg_ac_order.gm_sav_construct_vatrate (v_invoice_id, p_custpo, p_accid, 50205);
					ELSE
						gm_pkg_ac_order.gm_sav_vatrate (v_invoice_id, p_custpo, p_accid, 50205);
					END IF;
				END IF;
			END IF;
			FOR orderlog IN orderlog_cur
			LOOP
			gm_pkg_cm_status_log.gm_sav_status_details (orderlog.orderid, '', p_userid, NULL, CURRENT_DATE, 91179, 91102);
			END LOOP;
		END IF;
	END IF;

	p_inv_id	:= v_invoice_id;
END gm_sav_single_invoice;

/******************************************************************
	  * Description : Procedure to Generate Single Invoice for the Order.
	  ****************************************************************/
	PROCEDURE gm_sav_single_dealer_invoice (
		p_custpo	   IN		t503_invoice.c503_customer_po%TYPE
  , p_accid 	   IN		t503_invoice.c704_account_id%TYPE
  , p_dealer_id    IN		t503_invoice.c101_dealer_id%TYPE
  , p_payterms	   IN		t503_invoice.c503_terms%TYPE
  , p_invdate	   IN		DATE
  , p_userid	   IN		t503_invoice.c503_created_by%TYPE
  , p_action	   IN		VARCHAR2
  , p_invtype	   IN		t503_invoice.c901_invoice_type%TYPE
  , p_inv_source   IN		t503_invoice.c901_invoice_source%TYPE
  , p_shiptoid     IN           t501_order.C501_SHIP_TO_ID%TYPE
  , p_ship_dt	   IN		VARCHAR2
  , p_inv_id	   OUT		t503_invoice.c503_invoice_id%TYPE
	)
AS

	v_invoice_id   VARCHAR2 (20);
	v_cnt		   NUMBER;
	v_vat_fl	   VARCHAR2(5);
	v_con_vat_fl   VARCHAR2(5);	
	v_company_id   t1900_company.c1900_company_id%TYPE;
	v_division_id  t1910_division.c1910_division_id%TYPE;
	v_rep_id	   t703_sales_rep.c703_sales_rep_id%TYPE;
	v_order_date   DATE;
	v_inv_date     DATE;
	
		CURSOR orderlog_cur
	IS
     	SELECT c501_order_id orderid
       	  FROM t501_order
      	WHERE c503_invoice_id  = v_invoice_id
          	AND c503_invoice_id IS NOT NULL
          	AND c501_void_fl    IS NULL;
          	
BEGIN
    	v_inv_date := p_invdate;
    	
	-- Company needs to be taken from the dealer(26230725) and not from Context.
	IF p_dealer_id IS NOT NULL
	THEN
		SELECT C1900_COMPANY_ID 
	      INTO v_company_id  
	      FROM t101_party
	     WHERE c101_party_id = p_dealer_id
	       AND c901_party_type = '26230725';
	ELSE
	SELECT get_compid_frm_cntx() 
      INTO v_company_id  
      FROM dual;
	END IF;
     

	-- getting the division Id
	v_division_id := get_rep_div_id_by_dealer(p_dealer_id);
	
--If invoice generate date is prior to surgery date .validating the invoice date
	         SELECT MIN(c501_surgery_date) 
	           INTO v_order_date
		       FROM t501_order t501
		      WHERE  t501.c101_dealer_id =  p_dealer_id
		        AND t501.c501_customer_po = p_custpo
		        AND t501.c501_delete_fl      IS NULL
		        AND t501.c501_void_fl        IS NULL
		        AND T501.C503_INVOICE_ID IS NULL
		        AND NVL (c901_order_type, -9999) NOT IN (
               SELECT t906.c906_rule_value
                  FROM t906_rules t906
                 WHERE t906.c906_rule_grp_id = 'ORDERTYPE'
                   AND c906_rule_id = 'EXCLUDE');
	
	IF v_order_date > p_invdate OR CURRENT_DATE < p_invdate
	THEN
		raise_application_error (-20672, '');
	END IF;
	
	SELECT COUNT (1)
	  INTO v_cnt
	  FROM t501_order
	 WHERE c501_customer_po = p_custpo 
	   AND c101_dealer_id = p_dealer_id 
	   AND c501_void_fl IS NULL
	   AND c501_hold_fl IS NOT NULL;

	IF v_cnt > 0
	THEN
		-- Error Messaage. Cannot Invoice this PO as atleast one DO is on Hold Status.
		raise_application_error (-20888, '');
	ELSE
				
		-- PMT-33782: Invoice sequence getting from t503c table (to avoid missing seq number)
		  
		gm_pkg_ac_invoice_txn.gm_invoice_next_seq (p_inv_source, v_company_id , v_invoice_id);
		
	 IF p_invtype = '26240439'
		THEN
			v_invoice_id := REPLACE(v_invoice_id,'81-','INV-ST-');
		END IF;
		
	IF NVL(get_rule_value (v_company_id,'INVOICE_CURRENT_DATE'),'N') = 'Y'  -- Check If germany then curdate
		THEN
			v_inv_date := CURRENT_DATE;
		END IF;
						
		INSERT INTO t503_invoice
					(c503_invoice_id, c704_account_id, c503_invoice_date, c503_terms, c503_customer_po
				   , c503_created_by, c503_created_date, c503_status_fl, c901_invoice_type, c901_invoice_source,c1900_company_id
				   , c1910_division_id,c101_dealer_id 
					)
			 VALUES (v_invoice_id, NULL, v_inv_date, p_payterms, p_custpo
				   , p_userid, CURRENT_DATE, 0, p_invtype, p_inv_source	,v_company_id -- 50255 - generic invoice code
				   , v_division_id,p_dealer_id
					);

		IF p_action = 'ADD'
		THEN
			UPDATE t501_order
			   SET c503_invoice_id = v_invoice_id
			   	 , c501_last_updated_by = p_userid
				 , c501_last_updated_date = CURRENT_DATE
			 WHERE c501_customer_po = p_custpo AND c101_dealer_id = p_dealer_id AND c503_invoice_id IS NULL AND (c901_ext_country_id IS NULL
			 OR c901_ext_country_id  in (select country_id from v901_country_codes))
			 AND c501_void_fl is null
			 AND NVL(C501_SHIP_TO_ID,'-9999') = NVL(p_shiptoid,NVL(C501_SHIP_TO_ID,'-9999'))
			 AND DECODE(p_ship_dt , NULL, '-999', TRUNC(c501_shipping_date)) = NVL(p_ship_dt , '-999')
			 AND NVL (c901_order_type, -9999) NOT IN (
                SELECT t906.c906_rule_value
                  FROM t906_rules t906
                 WHERE t906.c906_rule_grp_id = 'ORDERTYPE'
                   AND c906_rule_id = 'EXCLUDE')	;

			--The Closed Status update statement is moved to the below mentioned procedure,This procedure will be called when we are doing issue credit /Debit.
			--This code change is done as part of PMT-5091.
			gm_pkg_ac_invoice.gm_upd_invoice_status(p_inv_source,v_invoice_id,p_userid);

       		--Update VAT based on dealer.
			gm_pkg_ac_order.gm_sav_vatrate_by_dealer (v_invoice_id, p_custpo, p_dealer_id);
			
			FOR orderlog IN orderlog_cur
			LOOP
			gm_pkg_cm_status_log.gm_sav_status_details (orderlog.orderid, '', p_userid, NULL, CURRENT_DATE, 91179, 91102);
			END LOOP;
		END IF;
	END IF;

	p_inv_id	:= v_invoice_id;
END gm_sav_single_dealer_invoice;

/******************************************************************
	  * Description : Procedure to fetch exclude vat rate flag.
	  ****************************************************************/     
PROCEDURE gm_fch_exclude_vat_fl (
    p_accid 	    IN		t503_invoice.c704_account_id%TYPE
  , p_company_id	IN		t1900_company.c1900_company_id%TYPE
  , p_vat_exc_fl	    OUT		VARCHAR2
	)
AS
v_acc_att     T704A_ACCOUNT_ATTRIBUTE.C901_ATTRIBUTE_TYPE%TYPE;


BEGIN

		--If Exclude VAT given 'Y' in mapp account param screen then VAT not calculated			    
			p_vat_exc_fl := NVL(GET_ACCOUNT_ATTRB_VALUE(p_accid,'101183'),'N'); 
			
			-- Get account type is public or private
			v_acc_att:=GET_ACCOUNT_ATTRB_VALUE(p_accid,'5504');
			
			-- Rule to check account type has exclude vat rate by company
			IF get_rule_value_by_company(v_acc_att,'EXC_VAT_RATE',p_company_id) = 'Y' THEN
				
				p_vat_exc_fl :='Y';
			
			END IF;  
		
END gm_fch_exclude_vat_fl;


	/******************************************************************
	  * Description : Procedure to save invoice file reference
	  ****************************************************************/    
	
	PROCEDURE gm_sav_invoice_file_ref  (
    p_invoice_id  	    IN		t503_invoice.c503_invoice_id%TYPE
  , p_file_Type			IN		NUMBER
  , p_userid	    	IN		t503_invoice.c503_created_by%TYPE
    )
    AS
   
    V_FILE_ID NUMBER;
    BEGIN
	     V_FILE_ID := get_upload_file_list_id(p_invoice_id,p_file_Type);
						IF V_FILE_ID IS NOT NULL THEN
            	 		UPDATE T503_INVOICE SET C903_RTF_FILE_ID = DECODE(p_file_Type,'90905',V_FILE_ID,C903_RTF_FILE_ID)
							   ,C903_PDF_FILE_ID = DECODE(p_file_Type,'90906',V_FILE_ID,C903_PDF_FILE_ID)
								,C903_CSV_FILE_ID = DECODE(p_file_Type,'90907',V_FILE_ID,C903_CSV_FILE_ID)
								,C903_XML_FILE_ID = DECODE(p_file_Type,'90908',V_FILE_ID,C903_XML_FILE_ID)
								,C503_LAST_UPDATED_BY=p_userid
								,C503_LAST_UPDATED_DATE=CURRENT_DATE
								WHERE C503_INVOICE_ID = p_invoice_id;
						END IF;
     					
 END gm_sav_invoice_file_ref;
 
 	/******************************************************************
	  * Description : Save Invoice and Payment Amount
	  ****************************************************************/   
    PROCEDURE gm_sav_invoice_amount  (
    p_invoice_id  	    IN		t503_invoice.c503_invoice_id%TYPE
  , p_userid	    	IN		t503_invoice.c503_created_by%TYPE
    )
    AS
 
   V_INVOICE_AMOUNT NUMBER;     --Get the Invoice Amount by calling gm_pkg_ac_invoice_txn. GET_AC_FCH_INVOICE_AMT  
   V_INVOICE_TAX_AMOUNT NUMBER; --Get the Invoice Tax Amount by Calling gm_pkg_ac_invoice_txn. GET_AC_FCH_INVOICE_TAX_AMT
   V_INVOICE_SHIP_COST NUMBER;  --Get the Invoice Ship Cost by calling gm_pkg_ac_invoice_txn. GET_AC_FCH_INVOICE_SHIP_COST
   V_INVOICE_TOTAL_AMOUNT NUMBER;--Add the Invoice amount and tax amount to get the Invoice total amount (with tax).
   V_INVOICE_TCS_RATE NUMBER; --Get the TCS Rate for Chennai and Delhi
   V_INVOICE_TOTAL_AMOUNT_TCS NUMBER; --Add the Invoice amount and TCS amount.
	
    BEGIN
	   V_INVOICE_AMOUNT:=gm_pkg_ac_invoice_txn.GET_AC_FCH_INVOICE_AMT(p_invoice_id);
	   V_INVOICE_TAX_AMOUNT:=gm_pkg_ac_invoice_txn.GET_AC_FCH_INVOICE_TAX_AMT(p_invoice_id);
	   V_INVOICE_SHIP_COST:=gm_pkg_ac_invoice_txn.GET_AC_FCH_INVOICE_SHIP_COST(p_invoice_id);
	   V_INVOICE_TCS_RATE:= gm_pkg_ac_invoice_txn.GET_AC_FCH_INVOICE_TCS_RATE(p_invoice_id);
	   V_INVOICE_TOTAL_AMOUNT:=V_INVOICE_AMOUNT+V_INVOICE_TAX_AMOUNT;
	   V_INVOICE_TOTAL_AMOUNT_TCS:=(V_INVOICE_TOTAL_AMOUNT*V_INVOICE_TCS_RATE/100)+V_INVOICE_TOTAL_AMOUNT;
	   --Update the Invoice table and update C503_INV_AMT_WO_TAX , C503_INV_TAX_AMT, C503_INV_AMT, C503_INV_SHIP_COST
	   UPDATE T503_INVOICE SET C503_INV_AMT_WO_TAX  =  (V_INVOICE_AMOUNT-V_INVOICE_SHIP_COST)
							   ,C503_INV_TAX_AMT    =  V_INVOICE_TAX_AMOUNT
								,C503_INV_AMT       =  V_INVOICE_TOTAL_AMOUNT_TCS
								,C503_INV_SHIP_COST =  V_INVOICE_SHIP_COST
								,C503_LAST_UPDATED_BY=p_userid
								,C503_LAST_UPDATED_DATE=CURRENT_DATE
								WHERE C503_INVOICE_ID = p_invoice_id;
	END gm_sav_invoice_amount;
	
   	/***********************************************
	 * Description:Fetch TCS Tax Amount
	  Author: Vignesh Arumugam
	 ********************************************/
	
 FUNCTION GET_AC_FCH_INVOICE_TCS_RATE(
 p_inv_id   t503_invoice.c503_invoice_id%TYPE
 )
  RETURN NUMBER
 IS
 /* Description  : THIS FUNCTION RETURNS THE PAID AMOUNT FOR INVOICE
 	Parameters   : p_code_id
*/
   v_company_id        t1900_company.c1900_company_id%TYPE;
   v_tcs_rate        NUMBER(4,2);
 BEGIN     
        IF (v_company_id = 1009 OR v_company_id = 1011)
   		THEN			
   			BEGIN     
          	 SELECT NVL(T502.C502_TCS_RATE,0)
	         INTO v_tcs_rate
	         FROM t501_order t501,t502_item_order t502
	         WHERE t501.c501_order_id          = t502.c501_order_id
	         AND t501.c501_delete_fl          IS NULL
	         AND t501.c501_void_fl            IS NULL
	    --   AND NVL (c901_order_type, -9999) <> 2535 -- Exclude orders for Sales Discount,ACK order types.
	         AND  (NVL(t501.c901_order_type, -9999) NOT IN 
	         (SELECT C906_RULE_VALUE FROM T906_RULES WHERE C906_RULE_ID in ('EXC_ORDER_TYPE', 'EXCLUDE')
	         		AND C906_RULE_GRP_ID in ( 'ORDTYPE','EXC_ORDER')))
	         AND T502.C502_DELETE_FL          is null
	         AND T502.C502_VOID_FL            is null
	         AND c503_invoice_id = p_inv_id
	         Group by T502.C502_TCS_RATE;
	    
	         EXCEPTION
	         WHEN OTHERS THEN
	         v_tcs_rate := 0;
	         END;
	    ELSE
	       v_tcs_rate := 0;
	       END IF;
   
   --
   RETURN v_tcs_rate;
   EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
   RETURN 0;
END GET_AC_FCH_INVOICE_TCS_RATE;

	/***********************************************
	 * Description:Fetch invoice Tax Amount
	 ********************************************/
	
 FUNCTION get_ac_fch_invoice_tax_amt(
 p_inv_id   t503_invoice.c503_invoice_id%TYPE
 )
  RETURN NUMBER
 IS
 /* Description  : THIS FUNCTION RETURNS THE PAID AMOUNT FOR INVOICE
 	Parameters   : p_code_id
*/
   v_tax_total        NUMBER (15, 2);
   v_ship_cost        NUMBER (15, 2);
   v_invoice_source   t503_invoice.c901_invoice_source%TYPE;
   v_country_type     VARCHAR2(10);
   v_company_id        t1900_company.c1900_company_id%TYPE;
 BEGIN
 --
   		SELECT c901_invoice_source,c1900_company_id
     	INTO v_invoice_source,v_company_id
     	FROM t503_invoice
    	WHERE c503_invoice_id = p_inv_id;

   		IF (v_invoice_source != 50256 OR v_invoice_source != 50253)
   		THEN
   	
   		SELECT get_rule_value_by_company ('US_1000', 'COUNTRY',v_company_id) INTO v_country_type FROM DUAL;      
           	 SELECT NVL(SUM(c502_tax_amt
         		+ ROUND(
         		 ((NVL(NVL(T502.C502_EXT_ITEM_PRICE,T502.C502_ITEM_PRICE),0)* NVL(T502.C502_ITEM_QTY,0))* NVL(T502.C502_IGST_RATE/100,0))
          		+((NVL(NVL(T502.C502_EXT_ITEM_PRICE,T502.C502_ITEM_PRICE),0)* NVL(T502.C502_ITEM_QTY,0))* NVL(T502.C502_CGST_RATE/100,0))
         		+((NVL(NVL(T502.C502_EXT_ITEM_PRICE,T502.C502_ITEM_PRICE),0)* NVL(T502.C502_ITEM_QTY,0))* NVL(T502.C502_SGST_RATE/100,0))
           		,2)),0)
	         INTO v_tax_total
	         FROM t501_order t501,t502_item_order t502
	         WHERE t501.c501_order_id          = t502.c501_order_id
	         AND t501.c501_delete_fl          IS NULL
	         AND t501.c501_void_fl            IS NULL
	    --   AND NVL (c901_order_type, -9999) <> 2535 -- Exclude orders for Sales Discount,ACK order types.
	         AND  (NVL(t501.c901_order_type, -9999) NOT IN 
	         (SELECT C906_RULE_VALUE FROM T906_RULES WHERE C906_RULE_ID in ('EXC_ORDER_TYPE', 'EXCLUDE')
	         		AND C906_RULE_GRP_ID in ( 'ORDTYPE','EXC_ORDER')))
	         AND T502.C502_DELETE_FL          is null
	         AND T502.C502_VOID_FL            is null
	         AND c503_invoice_id = p_inv_id;

	 
     END IF;
   --
   RETURN v_tax_total;
   EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
   RETURN -1;
END get_ac_fch_invoice_tax_amt;

	/***********************************************
	 * Description:Fetch invoice Amount
	 ********************************************/
FUNCTION GET_AC_FCH_INVOICE_AMT(
   p_inv_id   t503_invoice.c503_invoice_id%TYPE
)
   RETURN NUMBER
IS
/* Description    : THIS FUNCTION RETURNS THE PAID AMOUNT FOR INVOICE
 Parameters   : p_code_id
*/
   v_total            NUMBER (15, 2);
   v_ship_cost        NUMBER (15, 2);
   v_invoice_source   t503_invoice.c901_invoice_source%TYPE;
   v_country_type     VARCHAR2(10);
   v_company_id        t1900_company.c1900_company_id%TYPE;
BEGIN
	
   --
   SELECT c901_invoice_source,c1900_company_id
     INTO v_invoice_source,v_company_id
     FROM t503_invoice
    WHERE c503_invoice_id = p_inv_id;

   IF (v_invoice_source = 50256 OR v_invoice_source = 50253)
   THEN
     	SELECT CSG_INV_AMT INTO V_TOTAL FROM (
    									SELECT SUM (  NVL (t503a.c503a_total_amt, 0)
                  						+ GM_PKG_OP_ICT.GET_CSG_SHIP_COST (T503A.C503A_REF_ID)
                 									) csg_inv_amt 
        FROM t503a_invoice_txn t503a
       	WHERE C503_INVOICE_ID = p_inv_id
    UNION
     	--code modified for PMT-30924 to get OUS distributor orders cost
     (SELECT SUM(t501.c501_total_cost + NVL(t501.c501_ship_cost, 0)) csg_inv_amt   FROM t501_order t501,
         t503_invoice t503 WHERE t501.c503_invoice_id IS NOT NULL AND t501.c501_delete_fl IS NULL 
         AND t501.c501_void_fl IS NULL          
         AND t503.c503_invoice_id = t501.c503_invoice_id 
         AND t503.c503_invoice_id = p_inv_id AND t503.c503_void_fl IS NULL 
         ) 
            )          
        WHERE csg_inv_amt is not null ;
   ELSE
   	SELECT get_rule_value_by_company ('US_1000', 'COUNTRY',v_company_id) INTO v_country_type FROM DUAL;
   		IF v_country_type = 'Y' THEN
		      SELECT SUM (NVL (c501_total_cost, 0) +NVL (c501_ship_cost, 0))
		        INTO v_total
		        FROM t501_order
		       WHERE c503_invoice_id = p_inv_id
		         AND c501_delete_fl IS NULL
		         AND c501_void_fl IS NULL
				 AND (c901_ext_country_id IS NULL OR c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH
		         AND NVL (c901_order_type, -9999) <> 2535
		         AND NVL (c901_order_type, -9999) NOT IN (
		                SELECT t906.c906_rule_value
		                  FROM t906_rules t906
		                 WHERE t906.c906_rule_grp_id = 'ORDTYPE'
		                   AND c906_rule_id = 'EXCLUDE');
        ELSE
           	 SELECT SUM(ROUND ((NVL(NVL(T502.C502_EXT_ITEM_PRICE,T502.C502_ITEM_PRICE),0) * NVL(T502.C502_ITEM_QTY,0)),2))
	         INTO v_total
	         FROM t501_order t501,t502_item_order t502
	         WHERE t501.c501_order_id          = t502.c501_order_id
	         AND t501.c501_delete_fl          IS NULL
	         AND t501.c501_void_fl            IS NULL
	    --   AND NVL (c901_order_type, -9999) <> 2535 -- Exclude orders for Sales Discount,ACK order types.
	         AND  (NVL(t501.c901_order_type, -9999) NOT IN (SELECT C906_RULE_VALUE FROM T906_RULES WHERE C906_RULE_ID='EXC_ORDER_TYPE' AND C906_RULE_GRP_ID='EXC_ORDER'))
	         AND T502.C502_DELETE_FL          is null
	         AND T502.C502_VOID_FL            is null
	         AND c503_invoice_id = p_inv_id;

	    -- Calculate the Shipping cost seperately since in the above query item order table is joined hence the duplicate shipping cost is added
	    -- PMT-21364   
	         SELECT SUM(NVL(t501.c501_ship_cost,0) )
	         INTO v_ship_cost
	          FROM t501_order t501
	          WHERE  t501.c501_delete_fl          IS NULL
	          AND t501.c501_void_fl            IS NULL
	          AND  (NVL(t501.c901_order_type, -9999) NOT IN (SELECT C906_RULE_VALUE FROM T906_RULES WHERE C906_RULE_ID='EXC_ORDER_TYPE' AND C906_RULE_GRP_ID='EXC_ORDER'))
              AND c503_invoice_id = p_inv_id;   
	         
	         v_total := v_total+v_ship_cost;
	         
         END IF;     	
         
   END IF;
   --
   	RETURN v_total;
	EXCEPTION
   	WHEN NO_DATA_FOUND
   	THEN
    RETURN -1;
END GET_AC_FCH_INVOICE_AMT;
	/***********************************************
	 * Description:Fetch invoice shipcost
	 ********************************************/
 FUNCTION GET_AC_FCH_INVOICE_SHIP_COST (
   p_inv_id   t503_invoice.c503_invoice_id%TYPE
)
   RETURN NUMBER
IS
/* Description    : THIS FUNCTION RETURNS THE PAID AMOUNT FOR INVOICE
 Parameters   : p_code_id
*/
    v_ship_cost        NUMBER (15, 2);
    v_invoice_source   t503_invoice.c901_invoice_source%TYPE;
    v_country_type     VARCHAR2(10);
    v_company_id        t1900_company.c1900_company_id%TYPE;
BEGIN
	
   --
     SELECT c901_invoice_source,c1900_company_id
     INTO v_invoice_source,v_company_id
     FROM t503_invoice
     WHERE c503_invoice_id = p_inv_id;

    IF (v_invoice_source = 50256 OR v_invoice_source = 50253)
    THEN
      				SELECT csg_ship_cost 
      				INTO v_ship_cost 
      				FROM (
    					SELECT SUM (GM_PKG_OP_ICT.GET_CSG_SHIP_COST (T503A.C503A_REF_ID)
                 					)csg_ship_cost 
        			FROM t503a_invoice_txn t503a
      				WHERE C503_INVOICE_ID = p_inv_id
    UNION
     				--code modified for PMT-30924 to get OUS distributor orders ship cost
     (SELECT SUM( NVL(t501.c501_ship_cost, 0)) v_ship_cost   FROM t501_order t501,
         t503_invoice t503 WHERE t501.c503_invoice_id IS NOT NULL AND t501.c501_delete_fl IS NULL 
         AND t501.c501_void_fl IS NULL          
         AND t503.c503_invoice_id = t501.c503_invoice_id 
         AND t503.c503_invoice_id = p_inv_id AND t503.c503_void_fl IS NULL 
         ) 
            )   
             		
          			WHERE csg_ship_cost is not null ;
   ELSE
   	SELECT get_rule_value_by_company ('US_1000', 'COUNTRY',v_company_id) INTO v_country_type FROM DUAL;
   		
   	IF v_country_type = 'Y' THEN
  		SELECT SUM ( NVL (c501_ship_cost, 0))
		        INTO v_ship_cost
		        FROM t501_order
		       WHERE 
		       C501_ORDER_ID IN (			       
			       select  t501.c501_order_id FROM t501_order t501, T502_item_order t502
			       WHERE 
			       t501.c501_order_id = t502.c501_order_id
			       and t502.c502_void_fl is null
			       AND  c503_invoice_id = p_inv_id
			         AND c501_delete_fl IS NULL
			         AND c501_void_fl IS NULL
					 AND (c901_ext_country_id IS NULL OR c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH
			         AND NVL (c901_order_type, -9999) <> 2535
			         AND NVL (c901_order_type, -9999) NOT IN (
			                SELECT t906.c906_rule_value
			                  FROM t906_rules t906
			                 WHERE t906.c906_rule_grp_id = 'ORDTYPE'
			                   AND c906_rule_id = 'EXCLUDE')		       
		       );
        ELSE
           	 SELECT SUM(NVL(t501.c501_ship_cost,0) )
	         INTO v_ship_cost
	          FROM t501_order t501
	          WHERE  C501_ORDER_ID IN (              
              	 select  t501.c501_order_id 
              	 FROM t501_order t501, T502_item_order t502
			     WHERE 
			         t501.c501_order_id = t502.c501_order_id
			         and t502.c502_void_fl is null
			       	 AND  c503_invoice_id = p_inv_id
			         AND c501_delete_fl IS NULL
			         AND c501_void_fl IS NULL
		          	AND  (NVL(t501.c901_order_type, -9999) NOT IN 
		          	(SELECT C906_RULE_VALUE FROM T906_RULES WHERE C906_RULE_ID='EXC_ORDER_TYPE' AND C906_RULE_GRP_ID='EXC_ORDER'))
	          )
              ;   
	         
         END IF;     	
         
   END IF;
   --
   RETURN v_ship_cost;
   EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      RETURN -1;
END GET_AC_FCH_INVOICE_SHIP_COST;

	/************************************************************************************************************************
		* Description : This Procedure is used to get the next invoice sequence number - based on ref id and company
		* Author      :    
		**********************************************************************************************************************/

	PROCEDURE gm_invoice_next_seq (
		 p_inv_source   IN t503_invoice.c901_invoice_source%TYPE
  		, p_company_id	   IN		t503c_invoice_sequence.c1900_company_id%TYPE
  		, p_out_invoice_id   OUT		VARCHAR2
        )
        AS
        -- Default to Customer invoice (108200)
        v_invoice_seq_source NUMBER := 108200;
        v_inv_cnt NUMBER;
        BEGIN

		-- 50256	Inter-Company Transfer ,50253 Inter-Company Sale ,1000 Globus Medical North America
		
		IF ((p_inv_source = 50256 OR p_inv_source = 50253) AND p_company_id =1000 )
		THEN
		  	-- 108201 : Distributor Invoice
		 	v_invoice_seq_source := 108201;
		END IF;
		
		-- validate the sequence configured
	    
	    BEGIN
	         SELECT c503c_invoice_prefix || TO_CHAR (c503c_invoice_currval + 1)
	           INTO p_out_invoice_id
	           FROM t503c_invoice_sequence t503c
	          WHERE t503c.c901_invoice_source  = v_invoice_seq_source
	            AND t503c.c1900_company_id = p_company_id
	            AND t503c.c503c_void_fl IS NULL
	            FOR UPDATE;
	    EXCEPTION
	    WHEN OTHERS THEN
	        raise_application_error('-20500','Invoice number cannot be generated.');
	    END;
	    
	    -- increase the current seq numbser
	    
	     UPDATE t503c_invoice_sequence
	    SET c503c_invoice_currval      = c503c_invoice_currval + 1
	      WHERE c901_invoice_source   = v_invoice_seq_source
	        AND c1900_company_id = p_company_id
	        AND c503c_void_fl IS NULL;
	     
	     -- to validate current invoice id present in T503
	     
	     SELECT count (1)
	     	INTO v_inv_cnt
	      FROM t503_invoice
	     where c503_invoice_id = p_out_invoice_id;
	     --
	     IF v_inv_cnt <> 0
	     THEN
	     	-- to get the next sequence number.
	     	
	     	gm_pkg_ac_invoice_txn.gm_invoice_next_seq (p_inv_source, p_company_id , p_out_invoice_id);

	     END IF;
	        
	   --     
	END gm_invoice_next_seq;
	
	/**********************************************************************************************************************
	* Description : This Procedure is used to get all the orders comments and stored to Invoice comments column based on
	Invoice id
	* Author      :
	**********************************************************************************************************************/
	PROCEDURE gm_sav_invoice_comments_from_orders (
	        p_invoice_id    IN  t503_invoice.c503_invoice_id%TYPE,
	        p_company_id	IN	t503_invoice.c1900_company_id%TYPE,
	        p_userid        IN  t503_invoice.c503_created_by%TYPE)
	AS
	    --
	    v_store_ord_comments_fl VARCHAR2 (2) ;
	    v_orders_comments VARCHAR2 (4000);
	    --
		v_invoice_type t503_invoice.c901_invoice_type%TYPE;
		--
	BEGIN
	    -- to handle the exception
		BEGIN
		--
		 SELECT c901_invoice_type, NVL (get_rule_value_by_company ('ORDERS_COMMENTS', 'INVOICE', c1900_company_id), 'N')
		   INTO v_invoice_type, v_store_ord_comments_fl
		   FROM t503_invoice
		  WHERE c503_invoice_id = p_invoice_id
		  AND c1900_company_id = p_company_id;
	     
	    --
	    IF v_store_ord_comments_fl = 'Y' THEN
	       --
		   IF v_invoice_type = 50200 -- Regular
		   THEN
	        -- based on invoice ids to get only parent orders comments.
	         SELECT RTRIM (XMLAGG (XMLELEMENT (e, c501_comments || '	')) .EXTRACT ('//text()'), ',')
	           INTO v_orders_comments
	           FROM t501_order
	          WHERE c503_invoice_id = p_invoice_id
	          	AND c1900_company_id = p_company_id
				AND C501_PARENT_ORDER_ID IS NULL -- only get parent orders
	            AND c501_void_fl   IS NULL
	            AND c501_comments  IS NOT NULL;
	        ELSE
				-- based on invoice ids to get the orders comments.
	         SELECT RTRIM (XMLAGG (XMLELEMENT (e, c501_comments || '	')) .EXTRACT ('//text()'), ',')
	           INTO v_orders_comments
	           FROM t501_order
	          WHERE c503_invoice_id = p_invoice_id
	          	AND c1900_company_id = p_company_id
	            AND c501_void_fl   IS NULL
	            AND c501_comments  IS NOT NULL;
			END IF;	
			
			
	        -- to update the comments to invoice table
	        -- Invoice comments accept upto 255 char - so substring to only 250 chars.
			
	        gm_update_invoice_comments (p_invoice_id, substr(v_orders_comments, 0, 250), p_userid) ;
       
			--
	    END IF;
	    --

		EXCEPTION WHEN OTHERS
		THEN
			v_orders_comments := NULL;
		END;
		--
	END gm_sav_invoice_comments_from_orders;
	        
END gm_pkg_ac_invoice_txn;
/

 			