-- @"C:\database\Packages\Accounting\gm_pkg_ar_po_trans.bdy"
CREATE OR REPLACE PACKAGE BODY gm_pkg_ar_po_trans IS
/******************************************************************************************
  * Description :  This Procedure is used to save the cancel Reason For 
  *                Reject Status Of Sales Rep Approved PO 
  * Parameters  :  p_po_detail_id,p_cancelreason,p_canceltype,p_userid
  * Author      :  Mselvamani
  *******************************************************************************************/

    PROCEDURE gm_sav_po_reject (
        p_po_detail_id   IN   t5001_order_po_dtls.c5001_order_po_dtls_id%TYPE,
        p_cancelreason   IN   t901_code_lookup.c901_code_id%TYPE,
        p_canceltype     IN   t901_code_lookup.c901_code_id%TYPE,
        p_comments       IN   t907_cancel_log.c907_comments%TYPE,
        p_userid         IN   t101_user.c101_user_id%TYPE
    ) AS

        v_ord_id    t501_order.c501_order_id%TYPE;
        v_acct_id   t5001_order_po_dtls.c704_account_id%TYPE;
        v_po_num    t501_order.c501_customer_po%TYPE;
        v_po_rev    t5003_order_revenue_sample_dtls.c901_po_dtls%TYPE;
        v_do_rev    t5003_order_revenue_sample_dtls.c901_do_dtls%TYPE;
        v_message   VARCHAR2(100);
    BEGIN
	    -- For handling Exception for Order_id and po_id
        BEGIN
	        -- select order_id,cust_po_id from table t5001_order_po_dtls
            SELECT
                c501_order_id,
                c704_account_id,
                c5001_cust_po_id
            INTO
                v_ord_id,
                v_acct_id,
                v_po_num
            FROM
                t5001_order_po_dtls
            WHERE
                c5001_order_po_dtls_id = p_po_detail_id
                AND c5001_void_fl IS NULL;
                
           --while no data found assign null value for order id and po id    

        EXCEPTION
            WHEN no_data_found THEN
                v_ord_id := NULL;
                v_po_num := NULL;
                v_acct_id :=NULL;
                return;
        END;
      -- For Handling Exception for Order Revenue Details

        BEGIN
            SELECT
                c901_po_dtls,
                c901_do_dtls
            INTO
                v_po_rev,
                v_do_rev
            FROM
                t5003_order_revenue_sample_dtls
            WHERE
                c501_order_id = v_ord_id
                AND c5003_void_fl IS NULL;
                
           --while no data found assign null value for po_dtls and do_dtls Order Revenue Details   

        EXCEPTION
            WHEN no_data_found THEN
                v_po_rev := NULL;
                v_do_rev := NULL;
        END;
        -- update the PO Status for Parent and Child Orders

        gm_upd_po_status(v_acct_id,v_po_num, 109543, p_userid);
        -- update the Order Revenue Details
        gm_update_po(v_ord_id, NULL, p_userid, '', p_comments,
                     NULL, '', '', '', v_po_rev,
                     v_do_rev, v_message);

    END gm_sav_po_reject;
    /******************************************************************************************
  * Description :  This Procedure is used to save/update the  PO Status
  *                while changing PO Number edit/Status Change from A/R
  *                PO Entry Screen
  * Parameters  :  p_po_detail_id,p_status,p_userid
  * Author      :  Mselvamani 
  *******************************************************************************************/

    PROCEDURE gm_upd_po_status (
        p_acct_id        IN   t5001_order_po_dtls.c704_account_id%TYPE,
        p_cust_po        IN   t5001_order_po_dtls.c5001_cust_po_id%TYPE,
        p_status         IN   t5001_order_po_dtls.c901_cust_po_status%TYPE,
        p_userid         IN   t101_user.c101_user_id%TYPE
    ) AS
        
    BEGIN
            
	    -- update po_details in table t5001_order_po_dtls

        UPDATE t5001_order_po_dtls
        SET
            c901_cust_po_status = p_status,
            c5001_po_status_updated_by = p_userid,
            c5001_po_status_updated_date = current_date,
            c5001_last_updated_by = p_userid,
            c5001_po_moved_discrepancy_fl = decode(p_status, 109544, 'Y', c5001_po_moved_discrepancy_fl),
            c501_po_move_discrepancy_by = decode(p_status, 109544, p_userid, c501_po_move_discrepancy_by),
            c501_po_move_discrepancy_date = decode(p_status, 109544, current_date, c501_po_move_discrepancy_date),
            c5001_last_updated_date = current_date
        WHERE
            c704_account_id = p_acct_id
            AND c5001_cust_po_id = p_cust_po
            AND c5001_void_fl IS NULL;

    END gm_upd_po_status;
    /******************************************************************************************
  * Description :  This Procedure is used to save the  PO Details and Status  Log 
  *                while change the Po Statu.s from A/R PO Entry Screen
  * Parameters  :  p_po_dtl_id,p_type,p_dtl_value,p_dtl_name,p_userid
  * Author      :  Mselvamani  
  *******************************************************************************************/

    PROCEDURE gm_sav_po_details_log (
        p_po_dtl_id   IN   t5001_order_po_dtls.c5001_order_po_dtls_id%TYPE,
        p_type        IN   t5002_order_po_dtls_log.c901_po_history_type%TYPE,
        p_dtl_value   IN   t5002_order_po_dtls_log.c5002_po_dtls_value%TYPE,
        p_dtl_name    IN   t5002_order_po_dtls_log.c5002_po_dtls_code_name%TYPE,
        p_userid      IN   t101_user.c101_user_id%TYPE
    ) AS
    BEGIN
	    --insert values in table t5002_order_po_dtls_log 
        INSERT INTO t5002_order_po_dtls_log (
            c5001_order_po_dtls_id,
            c901_po_history_type,
            c5002_po_dtls_value,
            c5002_po_dtls_code_name,
            c5002_last_updated_by,
            c5002_last_dated_date
        ) VALUES (
            p_po_dtl_id,
            p_type,
            p_dtl_value,
            p_dtl_name,
            p_userid,
            current_date
        );

    END gm_sav_po_details_log;

 /**************************************************************************************
*  Description    : This Function is Used to get total amount for Order(including child order)
*                   for A/R PO Report Entry 
*  Parameters     : p_order_id,p_cust_po
*  Author         : Mselvamani
***************************************************************************************/

    FUNCTION get_order_total_amt_by_po (
        p_order_id   t501_order.c501_order_id%TYPE,
        p_cust_po    t501_order.c501_customer_po%TYPE
    ) RETURN NUMBER IS
        v_ord_amt t501_order.c501_total_cost%TYPE;
    BEGIN
  --
        SELECT
            nvl(SUM(t502.c502_item_price * t502.c502_item_qty), 0)
        INTO v_ord_amt
        FROM
            t501_order        t501,
            t502_item_order   t502
        WHERE
            ( t501.c501_order_id = p_order_id
              OR t501.c501_parent_order_id = p_order_id )
            AND nvl(t501.c501_customer_po, '-9999') = nvl(p_cust_po, '-9999')
            AND t502.c501_order_id = t501.c501_order_id
            AND t501.c501_void_fl IS NULL
            AND t502.c502_void_fl IS NULL
            AND nvl(t501.c901_order_type, - 9999) <> 2535
            AND nvl(c901_order_type, - 9999) NOT IN (
                SELECT
                    t906.c906_rule_value
                FROM
                    t906_rules t906
                WHERE
                    t906.c906_rule_grp_id = 'ORDTYPE'
                    AND c906_rule_id = 'EXCLUDE'
                    AND c906_void_fl IS NULL
            );
  --

        RETURN v_ord_amt;
  --
    EXCEPTION
        WHEN no_data_found THEN
            RETURN 0;
    END get_order_total_amt_by_po;
/**********************************************************************************************
*  Description    : This Function is Used to get ship total for Order(including child Order)
*                   A/R PO Entry Screen
*  Parameters     : p_order_id,p_cust_po
*  Author         : Mselvamani
***********************************************************************************************/

    FUNCTION get_order_ship_amt (
        p_order_id   t501_order.c501_order_id%TYPE,
        p_cust_po    t501_order.c501_customer_po%TYPE
    ) RETURN NUMBER IS
        v_ship_amt t501_order.c501_ship_cost%TYPE;
    BEGIN
  --
        SELECT
            SUM(nvl(c501_ship_cost, 0))
        INTO v_ship_amt
        FROM
            t501_order
        WHERE
            ( c501_order_id = p_order_id
              OR c501_parent_order_id = p_order_id )
            AND nvl(c501_customer_po, '-9999') = nvl(p_cust_po, '-9999')
            AND c501_void_fl IS NULL
            AND nvl(c901_order_type, - 9999) NOT IN (
                SELECT
                    t906.c906_rule_value
                FROM
                    t906_rules t906
                WHERE
                    t906.c906_rule_grp_id = 'ORDTYPE'
                    AND c906_rule_id = 'EXCLUDE'
                    AND c906_void_fl IS NULL
            );
  --

        RETURN v_ship_amt;
  --
    EXCEPTION
        WHEN no_data_found THEN
            RETURN 0;
    END get_order_ship_amt;
    
/******************************************************************************************
 * Description :  This Function is used to get child_orders_id for Parent Order id
 *                A/R Report Entry Screen
 * Parameters  :  p_order_id,p_order_type
 * Author      :  Mselvamani
 *******************************************************************************************/

    FUNCTION get_child_orders_id (
        p_order_id     t501_order.c501_order_id%TYPE,
        p_order_type   t501_order.c901_order_type%TYPE
    ) RETURN CLOB AS
        v_orders_id CLOB;

  --
    BEGIN
        SELECT
            rtrim(xmlagg(xmlelement(e, c501_order_id || ',')).extract('//text()'), ',')
        INTO v_orders_id
        FROM
            t501_order
        WHERE
             c501_parent_order_id = p_order_id
            AND nvl(c901_order_type, - 9999) <> 2535
            AND nvl(c901_order_type, - 9999) NOT IN (
                SELECT
                    t906.c906_rule_value
                FROM
                    t906_rules t906
                WHERE
                    t906.c906_rule_grp_id = 'ORDTYPE'
                    AND c906_rule_id = 'EXCLUDE'
                    AND c906_void_fl IS NULL
            )
            AND c501_void_fl IS NULL
            AND c501_update_inv_fl IS NULL;
  --

        RETURN v_orders_id;
    END get_child_orders_id;
    /***************************************************************************************
	* Description : This procedure used to save/update the PO Status in table t5001_order_po_dtls
	*               and void the existing po_status 
	* Parameters  : p_order_id,p_account_id,p_parent_ord_id,p_old_po_id,p_new_po_id,p_po_amount,p_user_id
	* Author      : Mselvamani
	****************************************************************************************/

    PROCEDURE gm_sav_order_po_process_dtsl (
        p_order_id        IN   t501_order.c501_order_id%TYPE,
        p_account_id      IN   t501_order.c704_account_id%TYPE,
        p_parent_ord_id   IN   t501_order.c501_order_id%TYPE,
        p_old_po_id       IN   t501_order.c501_order_id%TYPE,
        p_new_po_id       IN   t501_order.c501_order_id%TYPE,
        p_po_amount       IN   t501_order.c501_po_amount%TYPE,
        p_user_id         IN   t101_user.c101_user_id%TYPE
    ) AS

        v_parent_order_id   t501_order.c501_order_id%TYPE;
        v_user_id           t101_user.c101_user_id%TYPE;
        v_updated_date      t501_order.c501_last_updated_date%TYPE;
        v_po_status         t5001_order_po_dtls.c901_cust_po_status%TYPE;
        v_po_amt            t501_order.c501_po_amount%TYPE;
        v_account_id        t501_order.c704_account_id%TYPE;
        v_cnt               NUMBER;
        v_status            t5001_order_po_dtls.c901_cust_po_status%TYPE;
    BEGIN
        SELECT
            decode(get_dept_id(p_user_id), 2005, 109541, 109540)
        INTO v_po_status
        FROM
            dual;
            -- This select is used to get status of old po id(Move to discrepancy)
	       BEGIN
	    SELECT
	        c901_cust_po_status
	    INTO v_status
	    FROM
	        t5001_order_po_dtls
	    WHERE
	        (c5001_cust_po_id IS NULL OR c5001_cust_po_id = p_old_po_id)
	        AND c501_order_id = p_order_id
	        AND c5001_void_fl IS NULL
	        AND c901_cust_po_status NOT IN (109540,109541);--(109540-Draft)(109541-Pending Approval) 
	
	EXCEPTION
	    WHEN no_data_found THEN
	        v_status := v_po_status;
	END;
  --

        SELECT
            COUNT(1)
        INTO v_cnt
        FROM
            t5001_order_po_dtls t5001
        WHERE
            t5001.c501_order_id = p_parent_ord_id
            AND t5001.c5001_cust_po_id = p_new_po_id
            AND t5001.c5001_void_fl IS NULL;
        --  update the void flag for PO Details 

        gm_void_order_po_dtls(p_account_id, p_old_po_id, p_order_id, p_user_id);
        IF v_cnt = 1 THEN
            return;
        END IF;
       IF p_new_po_id <> '-999' THEN
        --  Save/update the PO Order Details
            gm_sav_order_po_dtls(p_order_id, p_account_id, p_old_po_id, p_new_po_id, v_status,
                                 p_po_amount, p_user_id);
       END IF;                          

    END gm_sav_order_po_process_dtsl;
    
     /***************************************************************************************
	* Description : This procedure used to Save the Order Revenue Details 
	*               A/R PO Entry Screen 
	* Parameters  : p_order_id,p_account_id,p_old_po_id,p_new_po_id,p_po_status,p_po_amt,p_user_id
	* Auhtor      : Mselvamani
	****************************************************************************************/

    PROCEDURE gm_sav_order_po_dtls (
        p_order_id     IN   t5001_order_po_dtls.c501_order_id%TYPE,
        p_account_id   IN   t5001_order_po_dtls.c704_account_id%TYPE,
        p_old_po_id    IN   t5001_order_po_dtls.c5001_cust_po_id%TYPE,
        p_new_po_id    IN   t5001_order_po_dtls.c5001_cust_po_id%TYPE,
        p_po_status    IN   t5001_order_po_dtls.c901_cust_po_status%TYPE,
        p_po_amt       IN   t5001_order_po_dtls.c5001_cust_po_amt%TYPE,
        p_user_id      IN   t101_user.c101_user_id%TYPE
    ) AS
        v_po_dtls_id t5001_order_po_dtls.c5001_order_po_dtls_id%TYPE;
    BEGIN
        UPDATE t5001_order_po_dtls
        SET
            c5001_cust_po_id = p_new_po_id,
            c5001_cust_po_amt = p_po_amt,
            c901_cust_po_status = p_po_status,
            c5001_void_fl = NULL,
            c5001_last_updated_by = p_user_id,
            c5001_last_updated_date = current_date,
            c5001_cust_po_date = DECODE(NVL(p_new_po_id,'-9999'),NVL(p_old_po_id,'-9999'),c5001_cust_po_date,current_date),
            c5001_cust_po_updated_by = DECODE(NVL(p_new_po_id,'-9999'),NVL(p_old_po_id,'-9999'),c5001_cust_po_updated_by,p_user_id)
        WHERE
            c501_order_id = p_order_id;

        IF ( SQL%rowcount = 0 ) THEN
	 	
	 	-- insert values in table t5001_order_po_dtls
            INSERT INTO t5001_order_po_dtls (
                c704_account_id,
                c501_order_id,
                c5001_cust_po_id,
                c5001_cust_po_amt,
                c5001_cust_po_date,
                c5001_cust_po_updated_by,
                c901_cust_po_status,
                c5001_po_status_updated_by,
                c5001_po_status_updated_date
            ) VALUES (
                p_account_id,
                p_order_id,
                p_new_po_id,
                p_po_amt,
                current_date,
                p_user_id,
                p_po_status,
                p_user_id,
                current_date
            );

        END IF;

    END gm_sav_order_po_dtls;
 	/***************************************************************************************
	* Description : This procedure used to void the Order PO details.
	* Parameters  : p_account_id,p_old_cust_po,p_order_id,p_user_id
	* Author      : Mselvamani
	****************************************************************************************/

    PROCEDURE gm_void_order_po_dtls (
        p_account_id    IN   t704_account.c704_account_id%TYPE,
        p_old_cust_po   IN   t501_order.c501_order_id%TYPE,
        p_order_id      IN   t501_order.c501_order_id%TYPE,
        p_user_id       IN   t101_user.c101_user_id%TYPE
    ) AS
        v_po_dtls_id   t5001_order_po_dtls.c5001_order_po_dtls_id%TYPE;
        v_cnt          NUMBER;
    BEGIN
	    --update void Flag in table t5001_order_po_dtls
        UPDATE t5001_order_po_dtls
        SET
            c5001_void_fl = 'Y',
            c5001_last_updated_by = p_user_id,
            c5001_last_updated_date = current_date
        WHERE
            c501_order_id = p_order_id
            AND c5001_void_fl IS NULL;

    END gm_void_order_po_dtls;
    
    /***************************************************************************************
	* Description : This procedure used to Save/Update PO discrepancy details.
	* Parameters  : p_po_dtl_id,p_collector,p_category,p_resolution,p_user_id
	* Author      : Mselvamani
	****************************************************************************************/
    PROCEDURE gm_upd_po_discrepency_dtl (
    	p_po_dtl_id     IN   t5001_order_po_dtls.c5001_order_po_dtls_id%TYPE,
    	p_collector     IN   t5001_order_po_dtls.c901_collector_id%TYPE,
    	p_category      IN   t5001_order_po_dtls.c901_po_disc_category_type%TYPE,
    	p_resolution    IN   t5001_order_po_dtls.c901_po_disc_resolution_type%TYPE,
    	p_user_id       IN   t101_user.c101_user_id%TYPE
    ) AS
        v_acct_id   t5001_order_po_dtls.c704_account_id%TYPE;
        v_po_num    t501_order.c501_customer_po%TYPE;
        
    BEGIN
	    -- For handling Exception for acct_id and po_id
        BEGIN
	        -- select acct_id,cust_po_id from table t5001_order_po_dtls
            SELECT
                c704_account_id,
                c5001_cust_po_id
            INTO
                v_acct_id,
                v_po_num
            FROM
                t5001_order_po_dtls
            WHERE
                c5001_order_po_dtls_id = p_po_dtl_id
                AND c5001_void_fl IS NULL;
                
           --while no data found assign null value for acct_id  and po id    

        EXCEPTION
            WHEN no_data_found THEN
                v_po_num := NULL;
                v_acct_id := NULL;
                return;
        END;
         -- update po_discrepancy_details in table t5001_order_po_dtls

        UPDATE t5001_order_po_dtls
        SET
            c901_collector_id = p_collector,
            c901_po_disc_category_type = p_category,
            c901_po_disc_resolution_type = p_resolution,
            c5001_resolved_disc_by   = DECODE(c901_po_disc_resolution_type,p_resolution,c5001_resolved_disc_by,p_user_id),
	        c5001_resolved_disc_date = DECODE(c901_po_disc_resolution_type,p_resolution,c5001_resolved_disc_date,CURRENT_DATE),
            c5001_last_updated_by = p_user_id,
            c5001_last_updated_date = current_date
        WHERE
            c704_account_id = v_acct_id
            AND c5001_cust_po_id = v_po_num
            AND c5001_void_fl IS NULL;
        
   END  gm_upd_po_discrepency_dtl; 
   
    /***************************************************************************************
	* Description : This procedure used to Save/Update Remind me days for discrepancy.
	* Parameters  : p_po_dtl_id,p_rem_days,p_rem_by,p_rem_date,p_user_id
	* Author      : Mselvamani
	****************************************************************************************/
    PROCEDURE gm_upd_remind_me_date_discrepency_dtl (
    	p_po_dtl_id     IN   t5001_order_po_dtls.c5001_order_po_dtls_id%TYPE,
    	p_rem_days      IN   t5001_order_po_dtls.c5001_po_disc_remaind_days%TYPE,
    	p_rem_date      IN   t5001_order_po_dtls.c5001_po_disc_remaind_date%TYPE,
    	p_user_id       IN   t101_user.c101_user_id%TYPE
    )AS
        v_acct_id   t5001_order_po_dtls.c704_account_id%TYPE;
        v_po_num    t501_order.c501_customer_po%TYPE;
        v_rem_date  t5001_order_po_dtls.c5001_po_disc_remaind_date%TYPE;
        v_rem_days  t5001_order_po_dtls.c5001_po_disc_remaind_days%TYPE;
     BEGIN
	    v_rem_days := p_rem_days;
	    -- For handling Exception for acct_id and po_id
        BEGIN
	        -- select order_id,cust_po_id from table t5001_order_po_dtls
            SELECT
                c704_account_id,
                c5001_cust_po_id
            INTO
                v_acct_id,
                v_po_num
            FROM
                t5001_order_po_dtls
            WHERE
                c5001_order_po_dtls_id = p_po_dtl_id
                AND c5001_void_fl IS NULL;
                
           --while no data found assign null value for order id and po id    

        EXCEPTION
            WHEN no_data_found THEN
                v_po_num := NULL;
                v_acct_id :=NULL;
                return;
        END;
        
         IF p_rem_days IS NOT NULL THEN
         
            Select (current_date + p_rem_days) INTO v_rem_date  from dual;
            
            ELSE
             v_rem_date:= NULL;
          END IF;   
        
         -- update po_details in table t5001_order_po_dtls

        UPDATE t5001_order_po_dtls
        SET
            c5001_po_disc_remaind_days = p_rem_days,
            c5001_po_disc_remaind_by   = p_user_id,
            c5001_po_disc_remaind_date = v_rem_date,
            c5001_last_updated_by = p_user_id,
            c5001_last_updated_date = current_date
        WHERE
            c704_account_id = v_acct_id
            AND c5001_cust_po_id = v_po_num
            AND c5001_void_fl IS NULL;
   END gm_upd_remind_me_date_discrepency_dtl;
   
   /***************************************************************************************
	* Description : This procedure used to save line item details.
	* Parameters  : p_input_str,p_user_id
	* Author      : Ramachandiran T.S
	****************************************************************************************/
    PROCEDURE gm_save_po_discrepency_line_item (
        p_input_str             IN   CLOB,
        p_partinputstr          IN   CLOB,
        p_user_id               IN   t101_user.c101_user_id%TYPE,
        p_msg_invalid_part      OUT CLOB
    ) AS
    	v_msg_invalid_part   VARCHAR2 (4000);
    	v_msg_prt_cmpny  VARCHAR2 (4000);
        v_msg_part   CLOB;
        v_sav_line_item_obj     json_object_t;
        v_line_item_array       json_array_t;
        v_po_dtl_id 			t5004_order_po_line_item_dtls.C5001_ORDER_PO_DTLS_ID%TYPE;
        v_do_part_id			t5004_order_po_line_item_dtls.C205_DO_PART_NUMBER_ID%TYPE;
        v_do_qty				t5004_order_po_line_item_dtls.C5004_DO_QTY%TYPE;				
        v_do_price				t5004_order_po_line_item_dtls.C5004_DO_PRICE%TYPE;
        v_po_part_id			t5004_order_po_line_item_dtls.C205_PO_PART_NUMBER_ID%TYPE;
        v_po_qty				t5004_order_po_line_item_dtls.C5004_PO_QTY%TYPE;
        v_po_price				t5004_order_po_line_item_dtls.C5004_PO_PRICE%TYPE;
        v_po_disc_type			t5004_order_po_line_item_dtls.C901_PO_DISCREPANCY_TYPE%TYPE;
        v_po_status				t5004_order_po_line_item_dtls.C901_PO_RESOLUTION%TYPE;
        v_ord_line_item			t5004_order_po_line_item_dtls.C5004_ORDER_PO_LINE_ITEM_DTLS%TYPE;
        
       BEGIN
       
       --To call Invalid part Number validation  
	  	gm_pkg_pd_partnumber.gm_validate_mfg_parts(p_partinputstr,v_msg_invalid_part,v_msg_prt_cmpny);

		IF v_msg_invalid_part IS NOT NULL THEN
		      v_msg_part := 'The following Part(s) not Available: '|| v_msg_invalid_part;
		ELSE
	       
		    v_line_item_array := json_array_t(p_input_str);
		       
		    /*Loop through the line item details to get each line item from the transaction.
	         */
	
		        FOR i IN 0..v_line_item_array.get_size - 1 
		        LOOP
		        	v_sav_line_item_obj 	:=      TREAT(v_line_item_array.get(i) AS json_object_t);
		        	v_po_dtl_id 		    := 		v_sav_line_item_obj.get_string('podtlid');
		        	v_do_part_id 		    := 		v_sav_line_item_obj.get_string('dopart');
		        	v_do_qty 		        := 		v_sav_line_item_obj.get_string('doqty');
		        	v_do_price 		        := 		v_sav_line_item_obj.get_string('doprice');
		        	v_po_part_id 		    := 		v_sav_line_item_obj.get_string('popart');
		        	v_po_qty 		        := 		v_sav_line_item_obj.get_string('poqty');
		        	v_po_price 		        := 		v_sav_line_item_obj.get_string('poprice');
		        	v_po_disc_type 		    := 		v_sav_line_item_obj.get_string('podistyp');
		        	v_po_status 		    := 		v_sav_line_item_obj.get_string('postat');
		        	v_ord_line_item 		:= 		v_sav_line_item_obj.get_string('ordlinitm');
		        	
		        
		        	
		       --insert/update t5004_order_po_line_item_dtls table
		       
		        gm_pkg_ar_po_trans.gm_upd_po_line_item_dtls(v_po_dtl_id,v_do_part_id,v_do_qty,v_do_price,v_po_part_id,v_po_qty,v_po_price,v_po_disc_type,v_po_status,v_ord_line_item,p_user_id);
		        	
		        END LOOP;
		  END IF;
	       p_msg_invalid_part := v_msg_part;
	  END gm_save_po_discrepency_line_item;
	  
	/**********************************************************************************************************************************
	* Description : This procedure used to update/insert line item detail in t5004 table
	* Parameters  : p_po_detail_id,p_do_part,p_do_qty,p_do_price,p_po_part,p_po_qty,p_po_price,p_po_dis_typ,p_status,p_ord_line_itm_id
	* Author      : Ramachandiran T.S
	**********************************************************************************************************************************/
	 PROCEDURE gm_upd_po_line_item_dtls (
        p_po_detail_id      	IN   	t5004_order_po_line_item_dtls.C5001_ORDER_PO_DTLS_ID%TYPE,
        p_do_part           	IN   	t5004_order_po_line_item_dtls.C205_DO_PART_NUMBER_ID%TYPE,
        p_do_qty				IN   	t5004_order_po_line_item_dtls.C5004_DO_QTY%TYPE,	
        p_do_price				IN 		t5004_order_po_line_item_dtls.C5004_DO_PRICE%TYPE,	
        p_po_part				IN 		t5004_order_po_line_item_dtls.C205_PO_PART_NUMBER_ID%TYPE,
        p_po_qty				IN 		t5004_order_po_line_item_dtls.C5004_PO_QTY%TYPE,
        p_po_price				IN 		t5004_order_po_line_item_dtls.C5004_PO_PRICE%TYPE,
        p_po_dis_typ			IN 		t5004_order_po_line_item_dtls.C901_PO_DISCREPANCY_TYPE%TYPE,
        p_status				IN 		t5004_order_po_line_item_dtls.C901_PO_RESOLUTION%TYPE,
        p_ord_line_itm_dtls_id	IN 		t5004_order_po_line_item_dtls.C5004_ORDER_PO_LINE_ITEM_DTLS%TYPE,
        p_user_id           	IN   	t101_user.c101_user_id%TYPE
    ) AS
    
    CURSOR cur_po_dtls
    IS
    	SELECT c5001_cust_po_id, c704_account_id
    	  FROM t5001_order_po_dtls
    	 WHERE C5001_ORDER_PO_DTLS_ID = p_po_detail_id
    	   AND C5001_VOID_FL IS NULL;
    
    BEGIN
	   IF p_ord_line_itm_dtls_id = 0
	    THEN
	    	INSERT INTO t5004_order_po_line_item_dtls(
	    		C5001_ORDER_PO_DTLS_ID,
	    		C205_DO_PART_NUMBER_ID,
	    		C5004_DO_QTY,
	    		C5004_DO_PRICE,
	    		C205_PO_PART_NUMBER_ID,
	    		C5004_PO_QTY,
	    		C5004_PO_PRICE,
	    		C901_PO_DISCREPANCY_TYPE,
	    		C901_PO_RESOLUTION
	    	)VALUES(
	    		p_po_detail_id,
	    		p_do_part,
	    		p_do_qty,
	    		p_do_price,
	    		p_po_part,
	    		p_po_qty,
	    		p_po_price,
	    		p_po_dis_typ,
	    		p_status
	    	);
	    	

	    	FOR po_dtls IN cur_po_dtls
	    	LOOP
		    	UPDATE T5001_ORDER_PO_DTLS  
			    SET 
			        C5001_PO_DISC_BY_LINE_ITEM_FL = 'Y',
			        C5001_PO_DISC_RESOLVED_FL = null,
			        C5001_LAST_UPDATED_BY =	p_user_id,
			        C5001_LAST_UPDATED_DATE =	current_date
			    WHERE 
			    	  C704_ACCOUNT_ID  = po_dtls.c704_account_id
			      AND C5001_CUST_PO_ID = po_dtls.c5001_cust_po_id
			      AND C5001_VOID_FL IS NULL;
	    	
	    	END LOOP;
	    	
	    ELSE
	    	UPDATE T5004_ORDER_PO_LINE_ITEM_DTLS
        	SET	
	          	C5001_ORDER_PO_DTLS_ID		= 	p_po_detail_id,
        		C205_DO_PART_NUMBER_ID	    =	p_do_part,
	          	C5004_DO_QTY				=	p_do_qty,
		    	C5004_DO_PRICE				=	p_do_price,
		    	C205_PO_PART_NUMBER_ID		=	p_po_part,
		    	C5004_PO_QTY				=	p_po_qty,
		    	C5004_PO_PRICE				=	p_po_price,
		    	C901_PO_DISCREPANCY_TYPE	=	p_po_dis_typ,
		    	C901_PO_RESOLUTION			=	p_status,			
	            C5004_LAST_UPDATED_BY		=	p_user_id,
	            C5004_LAST_UPDATED_DATE		=	current_date
        	WHERE
	            C5004_ORDER_PO_LINE_ITEM_DTLS	=	p_ord_line_itm_dtls_id
	        AND C5004_VOID_FL IS NULL;
	        
	        
	        FOR po_dtls IN cur_po_dtls
	    	LOOP
		    	UPDATE T5001_ORDER_PO_DTLS  
			    SET 
			        C5001_PO_DISC_RESOLVED_FL = null,
			        C5001_LAST_UPDATED_BY =	p_user_id,
			        C5001_LAST_UPDATED_DATE =	current_date
			    WHERE 
			    	  C704_ACCOUNT_ID  = po_dtls.c704_account_id
			      AND C5001_CUST_PO_ID = po_dtls.c5001_cust_po_id
			      AND C5001_VOID_FL IS NULL;
	    	
	    	END LOOP;
	    	
		  END IF;
	      
	END gm_upd_po_line_item_dtls;      
	
	/**********************************************************************************************************************************
	* Description : This procedure used to update the void flag
	* Author      : Ramachandiran T.S
	**********************************************************************************************************************************/
	 PROCEDURE gm_remove_po_line_item_dtls (
        p_po_line_item_detail_id      	IN   	t5004_order_po_line_item_dtls.C5004_ORDER_PO_LINE_ITEM_DTLS%TYPE,
        p_user_id           	IN   	t101_user.c101_user_id%TYPE
    ) AS
    
    
    BEGIN
	   
	    	UPDATE t5004_order_po_line_item_dtls
        	SET	
	          	C5004_VOID_FL = 'Y',		
	            C5004_LAST_UPDATED_BY		=	p_user_id,
	            C5004_LAST_UPDATED_DATE		=	current_date
        	WHERE
	            C5004_ORDER_PO_LINE_ITEM_DTLS	=	p_po_line_item_detail_id
	        	AND C5004_VOID_FL IS NULL;
	  
	        	
	END gm_remove_po_line_item_dtls; 
END gm_pkg_ar_po_trans;
/

