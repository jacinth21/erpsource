/* Formatted on 2011/08/11 02:48 (Formatter Plus v4.8.0) */
--@"c:\database\packages\accounting\gm_pkg_ac_ld_tags.bdy"
-- exec gm_pkg_ac_ld_tags.gm_ld_main(1);
CREATE OR REPLACE
PACKAGE BODY gm_pkg_ac_ld_tags
IS
    /*******************************************************
    * Purpose: This procedure will be main procedure to
    *   load COGS information
    *******************************************************/
PROCEDURE gm_ld_main (
        p_physical_audit_id IN t2600_physical_audit.c2600_physical_audit_id%TYPE,
        p_location_id       IN t2603_audit_location.c901_location_id%TYPE,
        p_userid            IN t2605_audit_tag_detail.c2605_created_by%TYPE)
AS
    v_inventory_lock_id t2600_physical_audit.c250_inventory_lock_id%TYPE;
    v_initial_tag_load_fl t2600_physical_audit.c2600_initial_tag_load_fl%TYPE;
    v_start_time DATE;
    v_tag_id t2605_audit_tag_detail.c2605_audit_tag_id%TYPE;
    v_type t2603_audit_location.c901_type%TYPE;
    v_no_loc VARCHAR2 (200) ;
    v_company_id  t1900_company.c1900_company_id%TYPE;
	v_plant_id    t5040_plant_master.c5040_plant_id%TYPE;
	v_us_time_zone t901_code_lookup.c901_code_nm%TYPE;
   	v_us_time_format t901_code_lookup.c901_code_nm%TYPE;
    CURSOR part_tags_details_cur
    IS
         SELECT c205_part_number_id pnum, c901_type location_type, '' txn_id
           FROM t251_inventory_lock_detail t251
          WHERE c250_inventory_lock_id        = v_inventory_lock_id
            AND t251.c251_qty                 > 0
            AND t251.c901_type                = p_location_id
            AND t251.c205_part_number_id NOT IN
            (
                 SELECT c205_part_number_id
                   FROM t205d_part_attribute
                  WHERE c901_attribute_type = '92342'
                    AND c205d_void_fl      IS NULL
                    AND c1900_company_id=v_company_id
            )
   ORDER BY c205_part_number_id;
    CURSOR cons_tags_details_cur
    IS
         SELECT '' pnum, t253a.c901_location_type location_type, t253a.c504_consignment_id txn_id
		 , c5010_tag_id invtagid, c5052_location_id invlocid,gm_pkg_op_set_pick_rpt.get_loc_cd_from_id (c5052_location_id) invloccd
           FROM t253a_consignment_lock t253a
          WHERE t253a.c250_inventory_lock_id = v_inventory_lock_id
            AND t253a.c901_location_type     = p_location_id
            AND t253a.c5040_plant_id=v_plant_id
       ORDER BY invloccd,t253a.c207_set_id, t253a.c205_part_number_id, t253a.c504_consignment_id;
    CURSOR partref_tags_details_cur
    IS
         SELECT t253b.c205_part_number_id pnum, t253a.c901_location_type location_type, t253a.c504_consignment_id
            txn_id, 
            DECODE (
            --Condition added to check if the TXN ID Contains - and substring based on that.
            -- If "-" does not exist in the txn, then use 99999
            p_location_id,'20461',DECODE(instr(t253a.c504_consignment_id, '-', -1,1),0,'99999',
            substr(t253a.c504_consignment_id, instr(t253a.c504_consignment_id, '-', -1,1)+1)),
            DECODE (t253a.c504_consignment_id, v_no_loc, '999999', DECODE(instr(t253a.c504_consignment_id, '-', -1,1),0,'99999', SUBSTR (t253a.c504_consignment_id, 1, 3)))) zonel, 
            DECODE (
            p_location_id,'20461','999999',
            DECODE (t253a.c504_consignment_id, v_no_loc, '999999', DECODE(instr(t253a.c504_consignment_id, '-', -1,1),0,'99999', SUBSTR (t253a.c504_consignment_id, 5, 1)))) aisle, 
            DECODE (
            p_location_id,'20461',DECODE(instr(t253a.c504_consignment_id, '-', -1,1),0,'99999',
            substr(t253a.c504_consignment_id, instr(t253a.c504_consignment_id, '-', -1,1)+1)),
            DECODE (t253a.c504_consignment_id, v_no_loc, '999999', DECODE(instr(t253a.c504_consignment_id, '-', -1,1),0,'99999', SUBSTR (t253a.c504_consignment_id, 7, 3)))) shunit, 
            DECODE (
            p_location_id,'20461','999999',
            DECODE (t253a.c504_consignment_id, v_no_loc, '999999', DECODE(instr(t253a.c504_consignment_id, '-', -1,1),0,'99999', SUBSTR (t253a.c504_consignment_id, 11, 1)))) rowl, 
            DECODE (
            p_location_id,'20461','999999',
            DECODE (t253a.c504_consignment_id, v_no_loc, '999999', DECODE(instr(t253a.c504_consignment_id, '-', -1,1),0,'99999', SUBSTR (t253a.c504_consignment_id, 13, 10)))) bin
           FROM t253a_consignment_lock t253a, t253b_item_consignment_lock t253b
          WHERE t253a.c504_consignment_id      = t253b.c504_consignment_id
            AND t253a.c250_inventory_lock_id   = v_inventory_lock_id
            AND t253b.c250_inventory_lock_id   = v_inventory_lock_id
            AND t253a.c901_location_type       = p_location_id
	    	AND t253b.c901_location_type       = p_location_id
	    	AND t253a.c5040_plant_id=v_plant_id
	    	AND t253b.C505_ITEM_QTY <> 0 -- to avoid the 0 qty
            AND t253b.c205_part_number_id NOT IN
            (
                 SELECT c205_part_number_id
                   FROM t205d_part_attribute
                  WHERE c901_attribute_type = '92342'
                    AND c205d_void_fl      IS NULL
                    AND c1900_company_id=v_company_id
            ) --
        --AND t253a.c504_consignment_id <> 'No Location Mapped'
        --AND t253b.c205_part_number_id IN( '102.521','GMWP24')
   ORDER BY TO_NUMBER (zonel), aisle, TO_NUMBER (shunit)
      , rowl, bin, pnum;
BEGIN
    v_start_time := SYSDATE;
    v_no_loc     := get_rule_value (p_location_id, 'PINOLOCPARTS') ;
    
     SELECT t2603.c901_type
       INTO v_type
       FROM t2603_audit_location t2603
      WHERE c901_location_id              = p_location_id
        AND t2603.c2600_physical_audit_id = p_physical_audit_id;
        
     SELECT c250_inventory_lock_id,C1900_COMPANY_ID,c5040_plant_id
       INTO v_inventory_lock_id ,v_company_id,v_plant_id
       FROM t2600_physical_audit
      WHERE c2600_physical_audit_id = p_physical_audit_id;
      
      SELECT GET_CODE_NAME(C901_TIMEZONE), GET_CODE_NAME(C901_DATE_FORMAT)
	        INTO v_us_time_zone,v_us_time_format
	     FROM t1900_company
	     WHERE c1900_company_id=v_company_id;
     
     --Set Context
     gm_pkg_cor_client_context.gm_sav_client_context(v_company_id,v_us_time_zone,v_us_time_format,v_plant_id);
      
    --IF v_initial_tag_load_fl IS NULL
    --THEN
    IF v_type = 50660 -- PART NUMBER
        THEN
        FOR current_row IN part_tags_details_cur
        LOOP
            gm_pkg_ac_patag_trans.gm_sav_tag_detail (p_physical_audit_id, current_row.pnum, current_row.txn_id,
            current_row.location_type, NULL, NULL, NULL, 50675 -- Regular Tag
            , NULL, NULL, v_tag_id) ;
        END LOOP;
        --Exclude parts from count
        gm_ld_ex_count_main (p_physical_audit_id, p_location_id, v_inventory_lock_id, v_type, v_no_loc, p_userid) ;
    ELSIF v_type = 50661 -- TRANSACTION
        THEN
        FOR current_row IN cons_tags_details_cur
        LOOP
            gm_pkg_ac_patag_trans.gm_sav_tag_detail (p_physical_audit_id, current_row.pnum, current_row.txn_id,
            current_row.location_type, NULL, NULL, NULL, 50675 -- Regular Tag
            , NULL, NULL, v_tag_id,current_row.invtagid,current_row.invlocid) ;
        END LOOP;
    ELSIF v_type = 50662 -- PART and TRANSACTION
        THEN
        FOR current_row IN partref_tags_details_cur
        LOOP
            gm_pkg_ac_patag_trans.gm_sav_tag_detail (p_physical_audit_id, current_row.pnum, current_row.txn_id,
            current_row.location_type, NULL, NULL, NULL, 50675 -- Regular Tag, NULL,
            , NULL, NULL, v_tag_id) ;
        END LOOP;
        --Exclude partsfrom count
        gm_ld_ex_count_main (p_physical_audit_id, p_location_id, v_inventory_lock_id, v_type, v_no_loc, p_userid) ;
    END IF;
    --END IF;
    --UPDATE t2600_physical_audit
    --  SET c2600_initial_tag_load_fl = 'Y'
    -- WHERE c2600_physical_audit_id = p_physical_audit_id;
    gm_pkg_cm_job.gm_cm_notify_load_info (v_start_time, SYSDATE, 'S', 'SUCCESS', 'gm_pkg_ac_tags') ;
    COMMIT;
EXCEPTION
WHEN OTHERS THEN
	ROLLBACK;
    --
    gm_pkg_cm_job.gm_cm_notify_load_info (v_start_time, SYSDATE, 'E', SQLERRM, 'gm_pkg_ac_tags') ;
    COMMIT;
END gm_ld_main;
/*******************************************************
* Purpose: This proc will record the qty in lock for the part that are excluded from count
*******************************************************/
PROCEDURE gm_ld_ex_count_main (
        p_physical_audit_id IN t2600_physical_audit.c2600_physical_audit_id%TYPE,
        p_location_id       IN t2603_audit_location.c901_location_id%TYPE,
        p_inventory_lock_id IN t2600_physical_audit.c250_inventory_lock_id%TYPE,
        p_type              IN t2603_audit_location.c901_type%TYPE,
        p_no_loc            IN VARCHAR2,
        p_user_id           IN t2605_audit_tag_detail.c2605_created_by%TYPE)
AS
    v_tag_id t2605_audit_tag_detail.c2605_audit_tag_id%TYPE;
    v_company_id  t1900_company.c1900_company_id%TYPE;
	v_plant_id    t5040_plant_master.c5040_plant_id%TYPE;
    --Exclude parts cursor for type  Part .For all the warehouse that are locked by parts.
    CURSOR part_tags_ex_cnt_det_cur
    IS
         SELECT c205_part_number_id pnum, c901_type location_type, '' txn_id
          , t251.c251_qty qty
           FROM t251_inventory_lock_detail t251
          WHERE c250_inventory_lock_id    = p_inventory_lock_id
            AND t251.c251_qty             > 0
            AND t251.c901_type            = p_location_id
            AND t251.c205_part_number_id IN
            (
                 SELECT c205_part_number_id
                   FROM t205d_part_attribute
                  WHERE c901_attribute_type = '92342'
                    AND c205d_void_fl      IS NULL
                    AND c1900_company_id=v_company_id
            )
   ORDER BY c205_part_number_id;
    --Exclude parts cursor where warehouse type  is Part and Transaction (e.g .shelf Inv. Locations)
    CURSOR partref_tags_ex_cnt_det_cur
    IS
         SELECT t253b.c205_part_number_id pnum, t253a.c901_location_type location_type, t253a.c504_consignment_id
            txn_id, t253b.c505_item_qty qty, 
            DECODE (
            p_location_id,'20461',
            DECODE(instr(t253a.c504_consignment_id, '-', -1,1),0,99999,
            substr(t253a.c504_consignment_id, instr(t253a.c504_consignment_id, '-', -1,1)+1)),
            DECODE(t253a.c504_consignment_id, p_no_loc, 999999, SUBSTR (t253a.c504_consignment_id, 1, 3))) zonel, 
            DECODE (
            p_location_id,'20461','999999',
            DECODE(t253a.c504_consignment_id, p_no_loc, '999999', SUBSTR (t253a.c504_consignment_id, 5, 1))) aisle, 
            DECODE (
            p_location_id,'20461',DECODE(instr(t253a.c504_consignment_id, '-', -1,1),0,99999,
            substr(t253a.c504_consignment_id, instr(t253a.c504_consignment_id, '-', -1,1)+1)),
            DECODE(t253a.c504_consignment_id, p_no_loc, 999999, SUBSTR (t253a.c504_consignment_id, 7, 3))) shunit, 
            DECODE (
            p_location_id,'20461','999999',
            DECODE(t253a.c504_consignment_id, p_no_loc, '999999', SUBSTR (t253a.c504_consignment_id, 11, 1))) rowl, 
            DECODE (
            p_location_id,'20461','999999',
            DECODE(t253a.c504_consignment_id, p_no_loc, '999999', SUBSTR (t253a.c504_consignment_id, 13, 10))) bin
           FROM t253a_consignment_lock t253a, t253b_item_consignment_lock t253b
          WHERE t253a.c504_consignment_id    = t253b.c504_consignment_id
            AND t253a.c250_inventory_lock_id = p_inventory_lock_id
            AND t253b.c250_inventory_lock_id = p_inventory_lock_id
            AND t253a.c901_location_type     = p_location_id
            AND t253a.c5040_plant_id=v_plant_id
            AND t253b.c205_part_number_id   IN
            (
                 SELECT c205_part_number_id
                   FROM t205d_part_attribute
                  WHERE c901_attribute_type = '92342'
                    AND c205d_void_fl      IS NULL
                    AND c1900_company_id=v_company_id
            ) --
        -- AND t253a.c504_consignment_id = p_no_loc
   ORDER BY TO_NUMBER (zonel), aisle, TO_NUMBER (shunit)
      , rowl, bin, pnum;
BEGIN
	SELECT GET_COMPID_FRM_CNTX(), GET_PLANTID_FRM_CNTX()
	      INTO v_company_id, v_plant_id
	      FROM DUAL;
	      
    IF p_type = 50660 -- PART NUMBER
        THEN
        FOR current_row IN part_tags_ex_cnt_det_cur
        LOOP
            gm_pkg_ac_patag_trans.gm_sav_tag_detail (p_physical_audit_id, current_row.pnum, current_row.txn_id,
            current_row.location_type, NULL, NULL, NULL, 50675 -- Regular Tag
            , NULL, NULL, v_tag_id) ;
             UPDATE t2605_audit_tag_detail
            SET c2605_qty               = current_row.qty, c101_counted_by = p_user_id, c2605_last_updated_by = p_user_id
              , c2605_last_updated_date = SYSDATE
              WHERE c2605_audit_tag_id  = v_tag_id;
        END LOOP;
    ELSIF p_type = 50662 -- PART and TRANSACTION
        THEN
        FOR current_row IN partref_tags_ex_cnt_det_cur
        LOOP
            gm_pkg_ac_patag_trans.gm_sav_tag_detail (p_physical_audit_id, current_row.pnum, current_row.txn_id,
            current_row.location_type, NULL, NULL, NULL, 50675 -- Regular Tag, NULL,
            , NULL, NULL, v_tag_id) ;
             UPDATE t2605_audit_tag_detail
            SET c2605_qty               = current_row.qty, c101_counted_by = p_user_id, c2605_last_updated_by = p_user_id
              , c2605_last_updated_date = SYSDATE
              WHERE c2605_audit_tag_id  = v_tag_id;
        END LOOP;
    END IF;
END gm_ld_ex_count_main;
END gm_pkg_ac_ld_tags;
/
