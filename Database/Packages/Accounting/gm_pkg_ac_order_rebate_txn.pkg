/* Formatted on 2012/22/08 17:40 (Formatter Plus v4.8.0) */
--@"c:\database\packages\accounting\gm_pkg_ac_order_rebate_txn.pkg"
CREATE OR REPLACE PACKAGE gm_pkg_ac_order_rebate_txn
IS

    --
    /****************************************************************
    * Description : to get the company and plant information from context and call main procedure.
    * Author      : Mani
    *****************************************************************/

    PROCEDURE gm_sav_rebate;


	/****************************************************************
    * Description : Main procedure - To calculate the Discount order based on Rebate management setup
    * 				1. This procedure used to Get all the Rebate active group and pass to "gm_sav_process_rebate" procedure.
    * Author      : Mani
    *****************************************************************/

    PROCEDURE gm_sav_rebate (
    		p_company_id IN t1900_company.c1900_company_id%TYPE,
    		p_plant_id IN t5040_plant_master.c5040_plant_id%TYPE);
    
    
    /****************************************************************
    * Description : This procedure used to Get all the Rebate master details - based on Rebate group.
    * 				1. Loop the Rebate details cursor
    * 				2. Based on group type GPO, GPB and Parent account to call separate procedure to store the account information 
    * 					a. gm_load_gpo_account
    * 					b. gm_load_grp_price_book_account
    * 					c. gm_load_parent_account
    * 
    * 				3. Based on Rebate Part type to store the parts to temp table
    * 					a. gm_sav_temp_parts
    * 
    * 				4. To create the DS order (based on account and stored parts)
    * 					a. gm_sav_discount_order
    * 
    * 				5. To delete the temp table (Accounts and Parts)
    * 					a. gm_delete_temp
    * 
    * Author      : Mani
    *****************************************************************/
    
    PROCEDURE gm_sav_process_rebate (
            p_group_type t7200_rebate_master.c901_grp_type%TYPE,
            p_company_id IN t1900_company.c1900_company_id%TYPE) ;
            
            
    /****************************************************************
    * Description : This procedure used to save all the account to temp table based on GPO (party).
    * 				1. To get account details from Account Affln table (T704d)
    * 				2. Store accounts to temp table (my_temp_acc_part_list)
    *  
    * Author      : Mani
    *****************************************************************/
            
    PROCEDURE gm_load_gpo_account (
            p_party_id t7200_rebate_master.c7200_ref_id%TYPE) ;
            
    /****************************************************************
    * Description : This procedure used to save all the account to temp table based on Group price book (party)
    * 				1. To get account details from  GPO account mapping table (T740)
    * 				2. Save accounts to temp table (my_temp_acc_part_list)
    * 
    * Author      : Mani
    *****************************************************************/
            
    PROCEDURE gm_load_grp_price_book_account (
            p_party_id t7200_rebate_master.c7200_ref_id%TYPE) ;
            
    /****************************************************************
    * Description : This procedure used to save all the account to temp table based on Parent Account (party)
    * 				1. To get account details from  Account table (T704)
    * 				2. Save accounts to temp table (my_temp_acc_part_list)
    * 
    * Author      : Mani
    *****************************************************************/
            
    PROCEDURE gm_load_parent_account (
            p_party_id t7200_rebate_master.c7200_ref_id%TYPE) ;
            
    /****************************************************************
    * Description : This procedure used to delete the data from temp table
    * 				1. First store the Account information to another temp table (it used to update the rebate flag in T502 table)
    * 				2. Delete the temp table
    * 					a. my_temp_acc_part_list
    * 					b. my_temp_part_list
    * 
    * Author      : Mani
    *****************************************************************/
            
    PROCEDURE gm_delete_temp (
            p_group_type IN t7200_rebate_master.c901_grp_type%TYPE) ;
            
    /****************************************************************
    * Description : This procedure used to store the parts to temp table and returns part flag Y/N - 
    * 				based on the flag to calculate the DS orders.
    * 
    * 				1. Based on Rebate id to get the  part type
    * 				2. Set the part Flag = N
    * 				3. If part type - Specific parts
    * 					a. based on Rebate id get the parts from T7201 table and save to temp table (my_temp_part_list)
    * 					b. set the part flag = Y
    * 				4. Send the part flag
    * 				 
    * Author      : Mani
    *****************************************************************/
            
    PROCEDURE gm_sav_temp_parts (
            p_rebate_id IN t7200_rebate_master.c7200_rebate_id%TYPE,
            p_out_part_fl OUT VARCHAR2) ;
            
            
    /****************************************************************
    * Description : This procedure used to update the Rebate flag/Rate.
    * 				1. Based on Order id to update process flag and rebate rate.
    * 
    * Author      : Mani
    *****************************************************************/
            
    PROCEDURE gm_update_rebate_fl (
            p_order_id    IN t501_order.c501_order_id%TYPE,
            p_rebate_rate IN t7200_rebate_master.c7200_rebate_rate%TYPE,
            p_rebate_flag IN VARCHAR2) ;
            
    /****************************************************************
    * Description : This procedure used to update non rebate accounts flag/rate.
    * 				So that next time job run, those records we can avoid.
    * 				1. To get all the non processed rebate orders based on below condition
    * 					a. based on rebate process flag
    * 					b. account id not in temp table (my_temp_key_value)
    * 					c. company id (selected company)
    * 				2. Loop all the orders
    * 					a. call the process flag update procedure - gm_update_rebate_fl
    * 						i. to pass process flag = N
    * 						ii. to pass rebate rate = 0
    * 
    * Author      : Mani
    *****************************************************************/
            
    PROCEDURE gm_update_non_rebate_orders (
            p_company_id IN t1900_company.c1900_company_id%TYPE) ;
            
            
    /***************************************************************************
     * Description : This Procedure will calculate the rebate for the Sales Orders and create a new Sales Discount order
    *  				or Update the existing Discount Order value.
    *
    * Author :
    ****************************************************************************/
            
    PROCEDURE gm_sav_discount_order (
            p_rebate_rate IN t7200_rebate_master.c7200_rebate_rate%TYPE,
            p_rebate_part IN t7200_rebate_master.c205_reb_part_number%TYPE,
            p_part_fl     IN VARCHAR2) ;
           
    /***************************************************************************
     * Description :  This Procedure will create/update the Discount Order 
    *  Author : 
    ****************************************************************************/
            
    PROCEDURE gm_create_discount_order (
            p_order_id    IN t501_order.c501_order_id%TYPE,
            p_rebate_part IN t7200_rebate_master.c205_reb_part_number%TYPE,
            p_rebate_amt  IN t502_item_order.c502_item_price%TYPE,
            p_rebate_rate IN T502_ITEM_ORDER.C7200_REBATE_RATE%TYPE,
            p_user_id     IN t501_order.c501_created_by%TYPE) ;
            
	/***************************************************************************
     * Description :  This Procedure used to void the DS orders
     * 				1. to check any DS order available - based on passed orders.
     * 				2. If DS order available then
     * 					a. to void the orders.
     * 
    *  Author : 
    ****************************************************************************/            
            
    PROCEDURE gm_void_discount_order (
            p_parent_order_id IN t501_order.c501_order_id%TYPE,
            p_user_id         IN t501_order.c501_created_by%TYPE) ;

	/***************************************************************************
     * Description :  This Procedure used to update last updated by/date
     * 
     *  Author : 
    ****************************************************************************/            
            
    PROCEDURE gm_upd_ord_last_updated_info (
            p_order_id IN t501_order.c501_order_id%TYPE,
            p_user_id         IN t501_order.c501_last_updated_by%TYPE) ;

    /****************************************************************
    * Description : This procedure used to update specific parts order - rebate flag/rate.
    * 				So that next time job run, those records we can avoid.
    * 				1. To get all the non processed rebate orders based on below condition
    * 					a. based on rebate process flag
    * 					b. account id in temp table (my_temp_key_value)
    * 					c. part number not in temp table 
    * 					d. company id (selected company)
    * 				2. Loop all the orders
    * 					a. call the process flag update procedure - gm_update_rebate_fl
    * 						i. to pass process flag = N
    * 						ii. to pass rebate rate = 0
    * 
    * Author      : Mani
    *****************************************************************/
	
PROCEDURE gm_update_non_specific_parts_orders (
        p_company_id IN t1900_company.c1900_company_id%TYPE);
            
END gm_pkg_ac_order_rebate_txn;
/