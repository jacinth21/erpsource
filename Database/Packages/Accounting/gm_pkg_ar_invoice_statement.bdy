--@"C:\Database\Packages\Accounting\gm_pkg_ar_invoice_statement.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_ar_invoice_statement
IS

   /***************************************************************
   * Description : Procedure to save invoice statement

   * Author     : Mahavishnu
   *************************************************************/
   PROCEDURE gm_sav_invoice_statement (
     p_inv_close_dt    IN   t503b_invoice_attribute.C901_INVOICE_CLOSING_DATE%TYPE
 	,p_dt_issue_from   IN   t503b_invoice_attribute.C503B_DATE_OF_ISSUE_FROM%TYPE
 	,p_dt_issue_to     IN   t503b_invoice_attribute.C503B_DATE_OF_ISSUE_TO%TYPE
 	,p_dealer_str      IN   CLOB
 	,p_account_str     IN   CLOB
 	,p_inv_source      IN   t503_invoice.c901_invoice_source%TYPE
 	,p_user_id         IN   t503b_invoice_attribute.C503B_CREATED_BY%TYPE
 	,p_inv_id          OUT  t503b_invoice_attribute.C503_INVOICE_ID%TYPE
	)
	AS
	v_pay_term          t503_invoice.c503_terms%TYPE;
	v_strDealerlen      NUMBER               := NVL (LENGTH (p_dealer_str), 0);
	v_strAcclen			NUMBER               := NVL (LENGTH (p_account_str), 0);
	v_dealer_string     CLOB                        := p_dealer_str;
	v_account_string    CLOB   :=p_account_str;
	v_customer_id   	VARCHAR2 (1000);
	v_count    			NUMBER;
	v_sale_amt 			t503b_invoice_attribute.C503B_TRANSACTION_AMT%TYPE;
	v_inv_close_dt 		t503b_invoice_attribute.C901_INVOICE_CLOSING_DATE%TYPE;

	BEGIN
	
		IF v_strDealerlen > 0     	 THEN
         
     	 WHILE INSTR (v_dealer_string, '|') <> 0
         LOOP
         
         	v_customer_id := SUBSTR (v_dealer_string, 1, INSTR (v_dealer_string, '|') - 1);
            v_dealer_string := SUBSTR (v_dealer_string, INSTR (v_dealer_string, '|') + 1);
            	
	            	-- get invoice count for the dealer
	            	SELECT count(*) INTO v_count
	            	FROM t503_invoice t503,t503b_invoice_attribute t503b
	            	WHERE t503.c503_invoice_id = t503b.c503_invoice_id
	            	AND t503.c101_dealer_id = v_customer_id
	            	AND t503b.C503B_DATE_OF_ISSUE_FROM = p_dt_issue_from
	            	AND t503b.C503B_DATE_OF_ISSUE_TO = p_dt_issue_to
	            	AND t503.c503_void_fl is NULL
	            	AND t503b.c503b_void_fl is NULL;
	            	
	            	-- get dealer sales amount in the month
	            	SELECT GET_DEALER_ACCT_SALE_IN_MONTH('',v_customer_id,'',p_dt_issue_from,p_dt_issue_to,NULL)--PMT-40781 excluded orders are listing
	  				INTO v_sale_amt FROM DUAL; 
	            	
	  				-- get invoice closing date for the dealer 
	  				SELECT c901_invoice_closing_date INTO v_inv_close_dt
	  				FROM t101_party_invoice_details 
	  				WHERE c101_party_id=v_customer_id
	  				AND c101_void_fl IS NULL; 
	           
	            
	  	        -- check invoice count and sales amount are zero and invoice closing date are equal
	            IF (v_count = 0 AND v_sale_amt = 0 AND v_inv_close_dt = p_inv_close_dt) THEN
	           		 v_pay_term := get_dealer_pay_term(v_customer_id);
	           		 
	           		 -- 26240213 company dealers
	           		 -- 26240439 Invoice statement
	            	gm_pkg_ac_invoice_txn.gm_sav_single_dealer_invoice('','',v_customer_id,v_pay_term,p_dt_issue_to,p_user_id,'','26240439','26240213','','',p_inv_id);
	            	--PMT25905 Save Invoice and Payment Amount
	            	gm_pkg_ac_invoice_txn.gm_sav_invoice_amount(p_inv_id,p_user_id);
	            	GM_SAVE_MONTHLY_INV_ATTRIBUTE('',p_inv_id,p_inv_close_dt,p_dt_issue_from,p_dt_issue_to,v_customer_id,'',p_user_id);
	            	 
	            	-- update invoice status as closed
	            	UPDATE t503_invoice
        			 SET c503_status_fl = 2
       				WHERE c503_invoice_id = p_inv_id;
       				
	            END IF;
         
         END LOOP;
       END IF;
       
       
       IF v_strAcclen > 0 THEN
        
       	WHILE INSTR (v_account_string, '|') <> 0
         LOOP
         
         	v_customer_id := SUBSTR (v_account_string, 1, INSTR (v_account_string, '|') - 1);
            v_account_string := SUBSTR (v_account_string, INSTR (v_account_string, '|') + 1);
            
            	-- get invoice count for the account
	            	SELECT count(*) INTO v_count
	            	FROM t503_invoice t503,t503b_invoice_attribute t503b
	            	WHERE t503.c503_invoice_id = t503b.c503_invoice_id
	            	AND t503.c704_account_id = v_customer_id
	            	AND t503b.C503B_DATE_OF_ISSUE_FROM = p_dt_issue_from
	            	AND t503b.C503B_DATE_OF_ISSUE_TO = p_dt_issue_to
	            	AND t503.c503_void_fl is NULL
	            	AND t503b.c503b_void_fl is NULL;
	            
	            	-- get account sales amount in the month
	            	SELECT GET_DEALER_ACCT_SALE_IN_MONTH('','',v_customer_id,p_dt_issue_from,p_dt_issue_to,NULL)
	  				INTO v_sale_amt FROM DUAL; 
	  				
	  				-- get invoice closing date for the account 
	  				SELECT c704a_attribute_value  INTO v_inv_close_dt
	  				FROM t704a_account_attribute 
	  				WHERE c704_account_id=v_customer_id 
	  				AND c901_attribute_type=106202 
	  				AND c704a_void_fl is null; 
	            
	  				-- check invoice count and sales amount are zero and invoice closing date are equal
	            IF (v_count = 0 AND v_sale_amt = 0 AND v_inv_close_dt = p_inv_close_dt) THEN
	            
	            	v_pay_term := get_account_pay_term(v_customer_id);
	           	 	
	            	-- 50255 Company Customers
	           		 -- 26240439 Invoice statement
	          	 	gm_pkg_ac_invoice_txn.gm_sav_single_invoice('',v_customer_id,v_pay_term,p_dt_issue_to,p_user_id,'','26240439','50255','','',p_inv_id);
	           	 	
	           	 	GM_SAVE_MONTHLY_INV_ATTRIBUTE('',p_inv_id,p_inv_close_dt,p_dt_issue_from,p_dt_issue_to,'',v_customer_id,p_user_id);
	             	
	           	 	-- update invoice status as closed
	          	 	UPDATE t503_invoice
        			 SET c503_status_fl = 2
       				WHERE c503_invoice_id = p_inv_id;
       				
	            END IF;
            
         
         END LOOP;
       END IF;
 
   END gm_sav_invoice_statement;
	
END gm_pkg_ar_invoice_statement;
/
