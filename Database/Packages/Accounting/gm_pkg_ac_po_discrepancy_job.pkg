/* Created on 2020/07/21 17:58*/
--@"C:\PMT\Database\Packages\Accounting\gm_pkg_ac_po_discrepancy_job.bdy";

CREATE OR REPLACE PACKAGE gm_pkg_ac_po_discrepancy_job
IS

--
 /******************************************************************************************************************
		  * Description : This procedure used resolve PO Discrepancy line items based on PO Amount and Po Part details
		  * Author		: Ramachandiran T.S 
  ******************************************************************************************************************/
		PROCEDURE gm_process_po_disc_dtls;
 /******************************************************************************************************************
		  * Description : This procedure used to resolve PO Disc based on PO data comaprison and PO line item detail
		  * Author		: Ramachandiran T.S 
  ******************************************************************************************************************/
	
	PROCEDURE gm_process_po_disc_dtls(
			 p_company_id    IN   t1900_company.c1900_company_id%TYPE
			);
 /*****************************************************************************************************
		  * Description : This procedure used to fetch PO Discrepancy details based on PO Line item Flag 
		  * Author		: Ramachandiran T.S 
  ****************************************************************************************************/
	PROCEDURE gm_process_by_po(
			 p_company_id    IN   t1900_company.c1900_company_id%TYPE
			);

 /*********************************************************************************************
		  * Description : This procedure used to reconcile PO based on PO line item comparison
		  * Author		: Ramachandiran T.S 
  **********************************************************************************************/
	PROCEDURE gm_process_by_line_item(
				p_company_id    IN   t1900_company.c1900_company_id%TYPE
				);
 /*********************************************************************************************
		  * Description : This procedure used to reconcile PO based Data comparison
		  * Author		: Ramachandiran T.S 
  **********************************************************************************************/
	PROCEDURE gm_reconcile_by_po(
			 p_order_po_dtls_id    IN    t5001_order_po_dtls.c5001_order_po_dtls_id%TYPE,
			 p_user_id             IN    t101_user.c101_user_id%TYPE
			);
 /***************************************************************************************************
		  * Description : This procedure used to update PO discrepancy resolved flag based on PO details
		  * Author		: Ramachandiran T.S 
 ***************************************************************************************************/
	PROCEDURE gm_upd_po_disc_resolved(
			 p_order_po_dtls_id    IN    t5001_order_po_dtls.c5001_order_po_dtls_id%TYPE,
			 p_user_id             IN    t101_user.c101_user_id%TYPE
			);
 /*********************************************************************************************
		  * Description : This procedure used to reconcile PO based on PO line items
		  * Author		: Ramachandiran T.S 
  **********************************************************************************************/
	PROCEDURE gm_reconcile_by_line_item(
			  p_order_po_dtls_ids	IN    CLOB,
			  p_user_id             IN    t101_user.c101_user_id%TYPE
			);
 /*********************************************************************************************
		  * Description : This procedure used to save order details to temp table
		  * Author		: Ramachandiran T.S 
  **********************************************************************************************/
	PROCEDURE gm_sav_tmp_order_item_dtls;
	
 /*********************************************************************************************
		  * Description : This procedure used to update the po resolution staus in t5004 table
		  * Author		: Ramachandiran T.S 
  **********************************************************************************************/
	
	PROCEDURE  gm_upd_po_res_status(
	 			p_po_part_id     IN    t5004_order_po_line_item_dtls.C205_PO_PART_NUMBER_ID%TYPE,
	 			p_user_id        IN    t101_user.c101_user_id%TYPE
	);
	
 /***************************************************************************************************************************
		  * Description : This procedure used to update the po disc resolved flag in t5001 table based on Resolution open count
		  * Author		: Ramachandiran T.S 
  ****************************************************************************************************************************/
	
	PROCEDURE gm_upd_po_resolved_fl(
				 p_res_po_dtls_ids	    IN    CLOB,
			     p_user_id              IN    t101_user.c101_user_id%TYPE
	);
	
	
	/******************************************************************************************************
	*  Description    : This Function is Used to get count of hold order details from given order po dtls
	*  Author         : Ramachandiran T.S
	*******************************************************************************************************/

    FUNCTION get_hold_count_by_po_dtls_id RETURN NUMBER;
	
			
END gm_pkg_ac_po_discrepancy_job;
/