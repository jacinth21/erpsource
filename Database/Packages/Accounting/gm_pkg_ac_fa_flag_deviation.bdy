--@"c:\database\packages\accounting\gm_pkg_ac_fa_flag_deviation.bdy";
CREATE OR REPLACE
PACKAGE BODY gm_pkg_ac_fa_flag_deviation
IS
    /*******************************************************
    * Description : Procedure to fetch Details for VarifyUpload Report
    * Author   : Arockia Prasath
    *******************************************************/
PROCEDURE gm_fch_uploaded_data (
        p_audit_id   IN t2652_field_sales_lock.c2650_field_sales_audit_id%TYPE,
        p_dist_id    IN t2651_field_sales_details.c701_distributor_id%TYPE,
        p_countby    IN t2656_audit_entry.c2656_counted_by%TYPE,
        p_entry_type IN t2656_audit_entry.c901_entry_type%TYPE,
        p_status     IN VARCHAR,
        p_out_report_info OUT TYPES.cursor_type)
AS
BEGIN
    OPEN p_out_report_info FOR SELECT NULL pline, c2656_audit_entry_id auditentryid, t2656.c5010_tag_id tagid,
    get_distributor_name (t2651.c701_distributor_id) dist_name, t2656.c207_set_id setid, get_set_name (
    t2656.c207_set_id) setdesc, t2656.c205_part_number_id pnum, get_partnum_desc (t2656.c205_part_number_id) pdesc,
    t2656.c2656_part_count_fl part_count_fl, t2656.c2656_control_number cnum, t2656.c2656_comments comments,
    get_code_name (t2656.c901_status) status, t2656.c5010_retag_id retag, get_code_name (t2656.c901_ref_type) flagtype,
    DECODE (instr (',51023,51025,51026,51027,', (','|| t2656.c901_ref_type ||',')), 0, (DECODE (instr (
    ',51024,51028,18551,18552,18556,18557,18558, ', (','||t2656.c901_ref_type||',')), 0, (DECODE (t2656.c901_ref_type,
    '51029', c2656_ref_id, NULL)), get_code_name (c2656_ref_id))), get_distributor_name (c2656_ref_id)) flagdet,
    get_code_name (t2656.c901_location_type) ltype, t2656.c106_address_id addressid, (DECODE (c901_location_type, 6020,
    get_distributor_name (c2656_location_id), 6022, GET_REP_NAME (c2656_location_id), 6021, get_account_name (
    c2656_location_id), c2656_location_id)) location, get_code_name (t2656.c901_location_type) locationtype,
    t2656.c2656_address_others location_comments, get_code_name (v2656.addresstype) address_type, (DECODE (v2656.name,
    NULL, '', (v2656.name|| ' - ')) || v2656.address1 || ' ' || v2656.address2 || ' ' || DECODE (v2656.city, NULL, '',
    (v2656.city|| ', ')) || DECODE (v2656.state, 0, '', (get_code_name (v2656.state) || '')) || v2656.zipcode)
    location_detail, t2656.c2656_set_count_fl set_count_fl, t2656.c2656_counted_date counteddt, get_user_name (
    t2656.c2656_counted_by) countedby FROM t2656_audit_entry t2656, t2651_field_sales_details t2651,
    v2656_audit_entry_address v2656 WHERE t2651.c2651_field_sales_details_id = t2656.c2651_field_sales_details_id AND
    t2656.c2656_audit_entry_id                                               = v2656.audit_entry_id (+) AND
    t2651.c2650_field_sales_audit_id                                         = p_audit_id AND t2651.c701_distributor_id
                                                                             = DECODE (p_dist_id, '0',
    t2651.c701_distributor_id, p_dist_id) AND t2656.c2656_counted_by         = DECODE (p_countby, '0',
    t2656.c2656_counted_by, p_countby) AND t2656.c901_entry_type             = p_entry_type AND (t2656.c901_tag_status
                                                                             = DECODE (p_status, 'SUMMARY',
        t2656.c901_tag_status, p_status) OR t2656.c901_tag_status                                          IN (18541, 18542)) -- Matching , Positive
    AND (t2656.c901_ref_type                                                IS NULL OR t2656.c901_ref_type IN
        (
             SELECT t901.c901_code_id
               FROM t901_code_lookup t901
              WHERE t901.c901_code_grp IN ('FLFPQ')
                AND t901.c901_active_fl = '1'
        )
        OR t2656.c901_ref_type = DECODE (p_status, 'SUMMARY', t2656.c901_ref_type, p_status)) AND t2651.c2651_void_fl
                              IS NULL AND t2656.c2656_void_fl IS NULL ORDER BY c2656_audit_entry_id;
END gm_fch_uploaded_data;
/*******************************************************
* Description : Procedure to fetch Audit Entry Details
* Author   : Arockia Prasath
*******************************************************/
PROCEDURE gm_fch_edit_uploaded_data (
        p_audit_entry_id IN t2656_audit_entry.c2656_audit_entry_id%TYPE,
        p_audit_entry_detail OUT TYPES.cursor_type)
AS
BEGIN
    OPEN p_audit_entry_detail FOR SELECT c2656_audit_entry_id auditentryid, t2656.c5010_tag_id tagid,
    t2651.c701_distributor_id distid, get_distributor_name (t2651.c701_distributor_id) distName, t2656.c207_set_id
    setid, t2656.c205_part_number_id partnumber, t2656.c2656_control_number controlnumber, t2656.c2656_comments
    comments, t2656.c901_status statusid, t2656.c5010_retag_id reTag, t2656.c901_ref_type flagType, t2656.c2656_ref_id
    flagDetID, DECODE (instr (',51023,51025,51026,51027,', (','||t2656.c901_ref_type||',')), 0, (DECODE (instr (
    ',51024,51029,50145, ', (','||t2656.c901_ref_type||',')), 0, (DECODE (t2656.c901_ref_type, '51028', get_code_name (
    c2656_ref_id), c2656_ref_id)), c2656_ref_details)), get_distributor_name (c2656_ref_id)) flagdet,
    t2656.c106_address_id addressID, c2656_location_id locationID, t2656.c901_location_type loctyp,
    t2656.c2656_address_others locationComments, v2656.addresstype addressType, DECODE (
    t2656.c901_reconciliation_status, 51038, 'Y', NULL) reconciled, DECODE (c901_location_type, 6020, c2656_location_id
    , 6022, t2656.c106_address_id||'-'||v2656.addresstype, 6021, c2656_location_id, c2656_location_id) locationDet,
    t2656.c2656_set_count_fl setCountFL, t2656.c2656_part_count_fl partCountFL, TO_CHAR (TRUNC (
    t2656.c2656_counted_date), 'MM/DD/YYYY') counteddate, get_user_name (t2656.c2656_counted_by) countedBy,
    t2656.c901_tag_status tagStatus, t2656.c2656_ref_details flagDetails, DECODE (t2656.c207_set_id, NULL, 'N', 'Y')
    lockedsetfl, DECODE (t2656.c901_reconciliation_status, 51037, 'Y', NULL) approvedfl FROM t2656_audit_entry t2656,
    t2651_field_sales_details t2651, v2656_audit_entry_address v2656 WHERE t2651.c2651_field_sales_details_id =
    t2656.c2651_field_sales_details_id AND t2656.c2656_audit_entry_id                                         =
    v2656.audit_entry_id (+) AND t2656.c2656_audit_entry_id                                                   =
    p_audit_entry_id AND t2651.c2651_void_fl                                                                 IS NULL
    AND t2656.c2656_void_fl                                                                                  IS NULL
    ORDER BY c2656_audit_entry_id;
END gm_fch_edit_uploaded_data;
/*******************************************************
* Description : Procedure to fetch location details
* Author   : Arockia Prasath
*******************************************************/
PROCEDURE gm_fch_location_info (
        p_location_id   IN t2656_audit_entry.c2656_location_id%TYPE,
        p_location_type IN t2656_audit_entry.c901_location_type%TYPE,
        p_out_location_info OUT TYPES.cursor_type)
AS
BEGIN
    IF p_location_type = '6020' THEN -- distributor office
        OPEN p_out_location_info FOR SELECT c701_distributor_id location_id, c701_ship_add1 || ' ' || c701_ship_add2 ||
        ' ' || DECODE (c701_ship_city, NULL, '', c701_ship_city|| ', ') || DECODE (c701_ship_state, 0, '', (
        get_code_name (c701_ship_state) || ' - ')) || c701_ship_zip_code location_details FROM t701_distributor WHERE
        c701_distributor_id = p_location_id;
    ELSIF p_location_type   = '6022' THEN -- sales rep
        OPEN p_out_location_info FOR SELECT t106.c106_address_id||'-'||t106.c901_address_type location_id,
        t106.c106_add1 || ' ' || t106.c106_add2 || ', ' || t106.c106_city || ', ' || get_code_name (t106.c901_state) ||
        ' - ' || t106.c106_zip_code location_details FROM t703_sales_rep t703, t106_address t106 WHERE
        t703.c703_sales_rep_id      = p_location_id AND t106.c101_party_id = t703.c101_party_id AND t106.c106_void_fl IS
        NULL AND t703.c703_void_fl IS NULL 
        AND (t703.c901_ext_country_id IS NULL OR t703.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes));-- To include ext country id for GH
    ELSIF p_location_type           = '6021' THEN -- hospital
        OPEN p_out_location_info FOR SELECT t704.c704_account_id location_id, t704.c704_ship_add1 || '  ' ||
        t704.c704_ship_add2 || ' , ' ||t704.c704_ship_city || ', ' || get_code_name (t704.c704_ship_state) || ' - ' ||
        t704.c704_ship_zip_code location_details FROM t704_account t704 WHERE t704.c704_account_id = p_location_id AND
        t704.c704_void_fl IS NULL AND
        (t704.c901_ext_country_id IS NULL OR t704.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes));-- To include ext country id for GH
    ELSE
        OPEN p_out_location_info FOR SELECT NULL location_id, NULL location_details FROM dual;
    END IF;
END gm_fch_location_info;
/*******************************************************
* Description : Procedure to Save Data for Edit Upload
* Author   : Arockia Prasath
*******************************************************/
PROCEDURE gm_sav_edit_uploaded_data (
        p_tagid          IN t2656_audit_entry.c5010_tag_id%TYPE,
        p_ptnum          IN t2656_audit_entry.c205_part_number_id%TYPE,
        p_ctrlnum        IN t2656_audit_entry.c2656_control_number%TYPE,
        p_setid          IN t2656_audit_entry.c207_set_id%TYPE,
        p_comments       IN t2656_audit_entry.c2656_comments%TYPE,
        p_status_id      IN t2656_audit_entry.c901_status%TYPE,
        p_distributor_id IN t2656_audit_entry.c2656_ref_id%TYPE,
        p_loctype        IN t2656_audit_entry.c901_location_type%TYPE,
        p_locid          IN t2656_audit_entry.c2656_location_id%TYPE,
        p_loc_details    IN VARCHAR2,
        p_flag_id        IN t2656_audit_entry.c2656_ref_id%TYPE,
        p_flag_type      IN t2656_audit_entry.c901_ref_type%TYPE,
        p_loc_comments   IN t2656_audit_entry.c2656_address_others%TYPE,
        p_cuntdate       IN VARCHAR2,
        p_userid         IN t2656_audit_entry.c2656_last_updated_by%TYPE,
        p_audit_entry_id IN t2656_audit_entry.c2656_audit_entry_id%TYPE,
        p_locked_fl      IN CHAR,
        p_status         IN VARCHAR)
AS
    cnt       NUMBER;
    v_flagged VARCHAR2 (1) ;
    --
    v_old_flag_type t2656_audit_entry.c901_ref_type%TYPE;
    v_old_flag_reason t2656_audit_entry.c2656_ref_id%TYPE;
    v_old_flagged_by t2656_audit_entry.c2656_flagged_by%TYPE;
BEGIN
    -- Validate the part #
    IF (get_part_attribute_value (p_ptnum, 92340) != 'Y') THEN
        raise_application_error ('-20856', '') ; -- Please enter a valid taggable part number.
    END IF;
    -- Validate the set id
    IF p_setid <> '0' THEN
         SELECT COUNT (1)
           INTO cnt
           FROM t208_set_details t208, t205d_part_attribute t205d
          WHERE t208.c207_set_id            = p_setid
            AND t205d.c205_part_number_id   = t208.c205_part_number_id
            AND t208.c205_part_number_id    = p_ptnum
            AND t205d.c901_attribute_type   = 92340 -- tagable
            AND t205d.c205d_attribute_value = 'Y'
            AND t205d.c205d_void_fl        IS NULL
            AND t208.c208_void_fl          IS NULL;
        IF (cnt                             < 1) THEN
            raise_application_error ('-20857', '') ;
        END IF;
    END IF;
    IF UPPER (p_status) = 'EDITUPLOAD' THEN
        /*
        SELECT COUNT (1)
        INTO cnt
        FROM t5010_tag t5010
        WHERE t5010.c5010_tag_id = p_tagid
        AND t5010.c5010_void_fl IS NULL
        AND t5010.c5010_last_updated_trans_id IS NOT NULL;
        IF (cnt > 0)
        THEN
        raise_application_error ('-20234', '');
        END IF;
        */
         SELECT COUNT (1)
           INTO cnt
           FROM t2656_audit_entry
          WHERE c2656_audit_entry_id       = p_audit_entry_id
            AND c901_reconciliation_status = 51038--Reconciled
            AND c2656_void_fl             IS NULL;
        IF (cnt                            > 0) THEN
            raise_application_error ('-20858', '') ;--This tag was already reconciled,you cannot edit.
        END IF;
         SELECT NVL (t2656.c2656_ref_id, 'XXX'), NVL (t2656.c901_ref_type, 0), t2656.c2656_flagged_by
           INTO v_old_flag_reason, v_old_flag_type, v_old_flagged_by
           FROM t2656_audit_entry t2656
          WHERE t2656.c2656_audit_entry_id = p_audit_entry_id
            AND t2656.c2656_void_fl       IS NULL FOR UPDATE;
        -- After flagged (flag deviation), user change the flag type then to set the flagged date/by null
        -- Validate the (trasnfer from/borrowed from)
        IF v_old_flagged_by IS NOT NULL AND (v_old_flag_type <> NVL (p_flag_type, 0) OR v_old_flag_reason <> NVL (
                p_flag_id, 'XXX')) THEN
            v_flagged := 'Y';
        END IF;
         UPDATE t2656_audit_entry
        SET c205_part_number_id      = p_ptnum, c2656_control_number = p_ctrlnum, c207_set_id = DECODE (p_setid, '0', NULL,
            p_setid), c2656_comments = p_comments, c901_status = DECODE (p_status_id, 0, NULL, p_status_id)
          , c901_ref_type            = DECODE (p_flag_type, 0, NULL, p_flag_type), c2656_ref_id = DECODE (p_flag_id, 0,
            NULL, p_flag_id) --, c901_tag_status                    = DECODE (p_flag_type, 0, 18541, 18542)
          , c901_location_type                                           = p_loctype, c2656_location_id = DECODE (p_locid, '0', NULL, p_locid),
            c2656_address_others                                         = p_loc_comments, c106_address_id = DECODE (p_loctype, 6022, SUBSTR (p_loc_details, 1,
            (INSTR (p_loc_details, '-') - 1)), NULL), c2656_counted_date = TO_DATE (p_cuntdate, 'MM/DD/YYYY'),
            c2656_flagged_dt                                             = DECODE (v_flagged, 'Y', NULL,
            c2656_flagged_dt), c2656_flagged_by                          = DECODE (v_flagged, 'Y', NULL,
            c2656_flagged_by), c901_reconciliation_status                = DECODE (v_flagged, 'Y', NULL,
            c901_reconciliation_status), c2656_last_updated_by           = p_userid, c2656_last_updated_date = SYSDATE
          WHERE c2656_audit_entry_id                                     = p_audit_entry_id
            AND c2656_void_fl                                           IS NULL;
    ELSIF UPPER (p_status)                                               = 'SAVELOCKEDSET' THEN
         UPDATE t2656_audit_entry
        SET c207_set_id              = p_setid, c2656_last_updated_by = p_userid, c2656_last_updated_date = SYSDATE
          WHERE c2656_audit_entry_id = p_audit_entry_id
            AND c2656_void_fl       IS NULL;
    END IF;
    IF p_locked_fl = 'Y' THEN --LOCKED SET
        gm_sav_locked_set_data (p_tagid, p_setid, p_distributor_id, p_audit_entry_id, p_userid) ;
        -- to adjust the locked qty
       -- gm_sav_adjust_locked_set (p_audit_entry_id,p_userid);     
    END IF;
END gm_sav_edit_uploaded_data;
/*******************************************************
* Description : Procedure to update lock flag for Set
* Author   : Mihir Patel
*******************************************************/
PROCEDURE gm_sav_locked_set_data (
        p_tagid          IN t2656_audit_entry.c5010_tag_id%TYPE,
        p_setid          IN t2656_audit_entry.c207_set_id%TYPE,
        p_distributor_id IN t2656_audit_entry.c2656_ref_id%TYPE,
        p_audit_entry_id IN t2656_audit_entry.c2656_audit_entry_id%TYPE,
        p_userid         IN t2656_audit_entry.c2656_last_updated_by%TYPE)
AS
BEGIN
     UPDATE t2659_field_sales_ref_lock
    SET c207_set_id                        = p_setid
      WHERE c2659_field_sales_ref_lock_id IN
        (
             SELECT t2659.c2659_field_sales_ref_lock_id
               FROM t2656_audit_entry t2656, t2651_field_sales_details t2651, t2652_field_sales_lock t2652
              , t2659_field_sales_ref_lock t2659
              WHERE t2656.c2656_audit_entry_id         = p_audit_entry_id
                AND t2656.c2656_void_fl               IS NULL
                AND t2651.c2651_field_sales_details_id = t2656.c2651_field_sales_details_id
                AND t2651.c2651_void_fl               IS NULL
                AND t2651.c2650_field_sales_audit_id   = t2652.c2650_field_sales_audit_id
                AND t2652.c2652_void_fl               IS NULL
                AND t2652.c701_distributor_id          = p_distributor_id
                AND t2659.c2652_field_sales_lock_id    = t2652.c2652_field_sales_lock_id
                AND t2659.c2659_void_fl               IS NULL
                AND t2659.c5010_tag_id                 = p_tagid
        )
        AND c2659_void_fl IS NULL
        AND c5010_tag_id   = p_tagid;
END gm_sav_locked_set_data;
/********************************************************************************
* Description : Procedure to fetch Flag Report For Positive and Negative
*********************************************************************************/
PROCEDURE gm_fch_flag_variance (
        p_audit_id IN t2651_field_sales_details.c2650_field_sales_audit_id%TYPE,
        p_dist_id  IN t2651_field_sales_details.c701_distributor_id%TYPE,
        p_set_id   IN t2656_audit_entry.c207_set_id%TYPE,
        p_pnum     IN t2656_audit_entry.c205_part_number_id%TYPE,
        p_out_header_info OUT TYPES.cursor_type,
        p_audit_rpt OUT TYPES.cursor_type,
        p_matching_rpt OUT TYPES.cursor_type,
        p_positive_rpt OUT TYPES.cursor_type,
        p_negative_rpt OUT TYPES.cursor_type,
        p_associated_rpt_pos OUT TYPES.cursor_type,
        p_associated_rpt_neg OUT TYPES.cursor_type)
AS
    v_pnum t208_set_details.c205_part_number_id%TYPE;
    v_set_id VARCHAR2 (40) ;
BEGIN
    v_pnum    := p_pnum;
    v_set_id  := p_set_id;
    IF v_pnum IS NULL THEN
        BEGIN
            SELECT DISTINCT t2659.c205_part_number_id
               INTO v_pnum
               FROM t2659_field_sales_ref_lock t2659, t2652_field_sales_lock t2652
              WHERE t2652.c2652_field_sales_lock_id  = t2659.c2652_field_sales_lock_id
                AND t2652.c2650_field_sales_audit_id = p_audit_id
                AND t2652.c701_distributor_id        = p_dist_id
                AND t2659.c207_set_id                = p_set_id
                AND t2659.c2659_void_fl             IS NULL
                AND t2652.c2652_void_fl             IS NULL;
        EXCEPTION
        WHEN OTHERS THEN
             SELECT t208.c205_part_number_id
               INTO v_pnum
               FROM t207_set_master t207, t208_set_details t208, t205d_part_attribute t205d
              WHERE t207.c207_set_id            = p_set_id
                AND t208.c207_set_id            = t207.c207_set_id
                AND t208.c208_critical_fl       = 'Y'
                AND t205d.c205d_attribute_value = 'Y'
                AND t205d.c205_part_number_id   = t208.c205_part_number_id
                AND t205d.c901_attribute_type   = 92340 -- tagable
                AND t205d.c205d_void_fl        IS NULL
                AND t208.c208_void_fl          IS NULL
                AND t207.c207_void_fl          IS NULL;
        END;
    END IF;
    IF p_set_id  IS NULL THEN
        v_set_id := 'SET_NULL';
    END IF;
    gm_pkg_ac_fa_flag_deviation.gm_fch_header_info (p_dist_id, p_set_id, v_pnum, p_out_header_info) ;
    gm_pkg_ac_fa_variance_rpt.gm_fetch_variance_by_dist (p_audit_id, p_dist_id, NULL, NULL, p_audit_rpt, v_set_id,
    v_pnum) ;
    gm_pkg_ac_fa_flag_deviation.gm_fch_deviation_rpt (p_audit_id, p_dist_id, p_set_id, v_pnum, 18541, p_matching_rpt) ;
    gm_pkg_ac_fa_flag_deviation.gm_fch_deviation_rpt (p_audit_id, p_dist_id, p_set_id, v_pnum, 18542, p_positive_rpt) ;
    gm_pkg_ac_fa_flag_deviation.gm_fch_deviation_rpt (p_audit_id, p_dist_id, p_set_id, v_pnum, 18543, p_negative_rpt) ;
    gm_pkg_ac_fa_flag_deviation.gm_fch_associated_rpt (p_audit_id, p_dist_id, p_set_id, v_pnum, 'FLFPQ',
    p_associated_rpt_pos) ;
    gm_pkg_ac_fa_flag_deviation.gm_fch_associated_rpt (p_audit_id, p_dist_id, p_set_id, v_pnum, 'FLFNQ',
    p_associated_rpt_neg) ;
END gm_fch_flag_variance;
/********************************************************************************
* Description : Procedure to fetch part number,set and system details
*********************************************************************************/
PROCEDURE gm_fch_header_info (
        p_dist_id IN t2651_field_sales_details.c701_distributor_id%TYPE,
        p_set_id  IN t2657_field_sales_details_lock.c207_set_id%TYPE,
        p_pnum    IN t2657_field_sales_details_lock.c205_part_number_id%TYPE,
        p_out_header OUT TYPES.cursor_type)
AS
    v_set_system_id T207_SET_MASTER.c207_set_sales_system_id%TYPE;
    v_type t207_SET_MASTER.c207_type%TYPE;
BEGIN
    BEGIN
         SELECT c207_set_sales_system_id
           INTO v_set_system_id
           FROM t207_set_master
          WHERE c207_set_id = p_set_id;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_set_system_id := '';
    END;
    OPEN p_out_header FOR SELECT get_distributor_name (p_dist_id) dist_Name,
    get_partnum_desc (p_pnum) pdesc,
    get_set_name (v_set_system_id) system_name,
    get_set_name (p_set_id) set_desc,
    gm_pkg_ac_fa_flag_deviation.get_fa_set_cost (p_set_id, 11210) set_cost,
    p_pnum part_num,
    get_part_price (p_pnum, 'C') part_price FROM dual;
END gm_fch_header_info;
/********************************************************************************
* Description : Procedure to fetch Flag Report For Devaition reports
*********************************************************************************/
PROCEDURE gm_fch_deviation_rpt (
        p_audit_id   IN t2651_field_sales_details.c2650_field_sales_audit_id%TYPE,
        p_dist_id    IN t2651_field_sales_details.c701_distributor_id%TYPE,
        p_set_id     IN t2656_audit_entry.c207_set_id%TYPE,
        p_pnum       IN t2656_audit_entry.c205_part_number_id%TYPE,
        p_tag_status IN t2656_audit_entry.c901_tag_status%TYPE,
        p_out_rpt OUT TYPES.cursor_type)
AS
    v_text VARCHAR2 (50) ;
BEGIN
    OPEN p_out_rpt FOR SELECT t2656.c2656_audit_entry_id ID,
    t2656.c5010_tag_id tag_id,
    t2656.c205_part_number_id pnum,
    t2656.c2656_control_number cnum,
    t2656.c901_ref_type flag,
    t2656.c2656_ref_id reason,
    t2656.c2656_ref_details ref_details,
    t2656.c5010_retag_id retag,
    t2656.c2656_retag_fl retag_fl,
    DECODE (t2656.c901_location_type, 6020, get_distributor_name (t2656.c2656_location_id), 6022, get_rep_name (
    t2656.c2656_location_id), 6021, get_account_name (t2656.c2656_location_id)) audit_loc,
    DECODE (TO_CHAR (t5010.c901_location_type), '4120', TO_CHAR (get_distributor_name (t5010.c5010_location_id)),
    '4123', TO_CHAR (get_user_name (t5010.c5010_location_id)), TO_CHAR (get_code_name (t5010.c5010_location_id)))
    assigned,
    DECODE (t2656.c901_reconciliation_status, 51037, 'Y', 51038, 'Y', NULL) approved,
    t2656.c2656_flagged_dt fl_date,
    t2656.c2656_reconciled_dt recon_dt,
    get_user_name (t2656.c2656_counted_by) counted_by,
    t2656.c2656_counted_date counted_date,
    t2656.c2656_comments comments,
    get_user_name (t2656.c2656_last_updated_by) updated_by,
    t2656.c2656_last_updated_date updated_date,
    c901_posting_option posting_option,
    get_tag_type (NVL (get_retag_id (t2656.c2656_audit_entry_id), t2656.c5010_tag_id)) tag_type,
    c2656_posting_comment posting_comment,
    t5010.c5010_void_fl tag_void,
    get_log_flag (t2656.c2656_audit_entry_id, 1285) log_fl,
    t2656.c901_tag_status tag_status , DECODE(t5010.c5010_tag_id,NULL,'N','Y') valid_tag FROM t2651_field_sales_details t2651,
    t2656_audit_entry t2656,
    t5010_tag t5010 WHERE t2651.c2650_field_sales_audit_id = p_audit_id AND t2651.c701_distributor_id = p_dist_id AND
    t2651.c2651_field_sales_details_id                     = t2656.c2651_field_sales_details_id AND 
    DECODE(t2656.c2656_retag_fl,NULL,t2656.c5010_tag_id,t2656.c5010_retag_id) = t5010.c5010_tag_id(+) AND NVL2 (t2656.c207_set_id,
    t2656.c207_set_id, '-99999')                           = NVL (p_set_id, '-99999') AND t2656.c205_part_number_id =
    p_pnum AND t2656.c901_tag_status                       = p_tag_status AND t2651.c2651_void_fl IS NULL AND
    t2656.c2656_void_fl                                   IS NULL ORDER BY tag_id,
    pnum,
    cnum;
END gm_fch_deviation_rpt;
/********************************************************************************
* Description : Procedure to fetch associated reports
*********************************************************************************/
PROCEDURE gm_fch_associated_rpt (
        p_audit_id IN t2651_field_sales_details.c2650_field_sales_audit_id%TYPE,
        p_dist_id  IN t2651_field_sales_details.c701_distributor_id%TYPE,
        p_set_id   IN t2656_audit_entry.c207_set_id%TYPE,
        p_pnum     IN t2656_audit_entry.c205_part_number_id%TYPE,
        p_code_grp IN t901_code_lookup.c901_code_grp%TYPE,
        p_out_rpt OUT TYPES.cursor_type)
AS
BEGIN
    OPEN p_out_rpt FOR SELECT t2656.c2656_audit_entry_id ID,
    t2656.c5010_tag_id tag_id,
    t2656.c205_part_number_id pnum,
    t2656.c2656_control_number cnum,
    get_code_name (t2656.c901_ref_type) flag,
    DECODE (instr (',51023,51025,51026,51027,', (','||t2656.c901_ref_type||',')), 0, (DECODE (instr (
    ',51024,51029,50145, ', (','||t2656.c901_ref_type||',')), 0, (DECODE (t2656.c901_ref_type, '51028', get_code_name (
    c2656_ref_id), c2656_ref_id)), c2656_ref_details)), get_distributor_name (t2651.c701_distributor_id)) reason,
    t2656.c2656_ref_details ref_details FROM t2651_field_sales_details t2651,
    t2656_audit_entry t2656,
    t901_code_lookup t901,
    t2652_field_sales_lock t2652 WHERE t2651.c2651_field_sales_details_id = t2656.c2651_field_sales_details_id AND
    t2652.c2652_field_sales_lock_id                                       = t2651.c2652_field_sales_lock_id
    --AND t2651.c2650_field_sales_audit_id   = p_audit_id   -- to show the all the data
    AND t2656.c2656_ref_id                   = p_dist_id AND t2656.c207_set_id = NVL (p_set_id, t2656.c207_set_id) AND
    t2656.c205_part_number_id                = p_pnum AND t2652.c2652_locked_date BETWEEN add_months (sysdate, - 12) AND SYSDATE AND
    t2656.c901_ref_type                                                                                       IN (51025, 51026, 51027,
    51023) AND TO_CHAR (t2656.c901_ref_type) = TO_CHAR (t901.c901_code_id(+)) AND t901.c901_code_grp = p_code_grp AND
    t2651.c2651_void_fl                     IS NULL AND t2656.c2656_void_fl IS NULL ORDER BY tag_id,
    pnum,
    cnum;
END gm_fch_associated_rpt;
/********************************************************************************
* Description : Function used to fetch the set cost details
*********************************************************************************/
FUNCTION get_fa_set_cost (
        p_set_id         IN t2656_audit_entry.c207_set_id%TYPE,
        p_attribute_type IN t207c_set_attribute.c901_attribute_type%TYPE)
    RETURN VARCHAR2
IS
    v_attribute_value t207c_set_attribute.c207c_attribute_value%TYPE;
BEGIN
     SELECT t207c.c207c_attribute_value
       INTO v_attribute_value
       FROM t207c_set_attribute t207c
      WHERE t207c.c207_set_id         = p_set_id
        AND t207c.c901_attribute_type = p_attribute_type
        AND t207c.c207c_void_fl      IS NULL;
    RETURN v_attribute_value;
EXCEPTION
WHEN NO_DATA_FOUND THEN
    RETURN NULL;
    --
END get_fa_set_cost;
/********************************************************************************
* Description : Procedure to save Flag Report For Positive and Negative
*********************************************************************************/
PROCEDURE gm_sav_flag_variance (
        p_audit_id IN t2651_field_sales_details.c2650_field_sales_audit_id%TYPE,
        p_dist_id  IN t2651_field_sales_details.c701_distributor_id%TYPE,
        p_set_id   IN t2657_field_sales_details_lock.c207_set_id%TYPE,
        p_inputs   IN VARCHAR2,
        p_user_id  IN t2656_audit_entry.c2656_last_updated_by%TYPE)
AS
    v_details_id t2651_field_sales_details.c2651_field_sales_details_id%TYPE;
    v_strlen    NUMBER          := NVL (LENGTH (p_inputs), 0) ;
    v_string    VARCHAR2 (4000) := p_inputs;
    v_substring VARCHAR2 (4000) ;
    v_id        VARCHAR2 (20) ;
    v_ref_type t2656_audit_entry.c901_ref_type%TYPE;
    v_ref_id t2656_audit_entry.c2656_ref_id%TYPE;
    v_ref_details t2656_audit_entry.c2656_ref_details%TYPE;
    v_recon_status t2656_audit_entry.c901_reconciliation_status%TYPE;
    v_post_option t2656_audit_entry.c901_posting_option%TYPE;
    v_post_comment t2656_audit_entry.c2656_posting_comment%TYPE;
    v_ra_count NUMBER;
    v_cnt      NUMBER;
    v_qty      NUMBER;
    v_old_ref_id t2656_audit_entry.c2656_ref_id%TYPE;
    v_old_post_opt t2656_audit_entry.c901_posting_option%TYPE;
    v_old_ref_type t2656_audit_entry.c901_ref_type%TYPE;
    v_tag_id t2656_audit_entry.c5010_tag_id%TYPE;
    v_retag        VARCHAR2 (40) ;
    v_flag_dt      VARCHAR2 (40) ;
    v_flag_by      VARCHAR2 (20) ;
    v_new_post_opt NUMBER;
    v_new_flag_dt DATE;
    v_old_flag_dt DATE;
    v_old_post_cmts VARCHAR2 (4000) ;
    v_retag_fl t2656_audit_entry.c2656_retag_fl%TYPE;
    v_old_ref_dtls t2656_audit_entry.c2656_ref_details%TYPE;
    v_tag_type     NUMBER;
    v_old_retagid  VARCHAR2 (40) ;
    v_retaged_id   VARCHAR2 (40) ;
    v_ent_tagid    VARCHAR2 (40) ;
    v_cur_tagid    VARCHAR2 (40) ;
    v_ent_refid    VARCHAR2 (40) ;
    v_ent_refnm    VARCHAR2 (200) ;
    v_ent_distid   VARCHAR2 (40) ;
    v_ent_distnm   VARCHAR2 (200) ;
    v_curr_loc     VARCHAR2 (40) ;
    v_entry_status NUMBER;
BEGIN
    -- check already reconciled
     SELECT t2651.c2651_field_sales_details_id
       INTO v_details_id
       FROM t2651_field_sales_details t2651
      WHERE t2651.c2650_field_sales_audit_id = p_audit_id
        AND t2651.c701_distributor_id        = p_dist_id
        AND t2651.c2651_void_fl             IS NULL FOR UPDATE;
    IF v_strlen                              > 0 THEN
        WHILE INSTR (v_string, '|')         <> 0
        LOOP
            --
            v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
            v_string    := SUBSTR (v_string, INSTR (v_string, '|')    + 1) ;
            --
            v_id           := NULL;
            v_ref_type     := NULL;
            v_ref_id       := NULL;
            v_ref_details  := NULL;
            v_retag        := NULL;
            v_flag_dt      := NULL;
            v_post_option  := NULL;
            v_post_comment := NULL;
            --
            v_id               := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            v_substring        := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
            v_ref_type         := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            v_substring        := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
            v_ref_id           := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            v_substring        := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
            v_ref_details      := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            v_substring        := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
            v_retag            := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            v_substring        := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
            v_flag_dt          := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            v_substring        := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
            v_post_option      := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            v_substring        := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
            v_post_comment     := v_substring;
            IF v_flag_dt       IS NOT NULL THEN
                v_recon_status := 51036; -- Flagged
                v_flag_by      := p_user_id;
            ELSE
                v_recon_status := NULL;
                v_flag_by      := NULL;
            END IF;
             SELECT NVL (t2656.c2656_ref_id, 'XXX'), NVL (t2656.c901_posting_option, 0), NVL (t2656.c901_ref_type, 0)
              , t2656.c5010_tag_id, NVL (t2656.c2656_flagged_dt, t2656.c2656_created_date), NVL (
                t2656.c2656_posting_comment, '999'), t2656.c2656_retag_fl, NVL (t2656.c2656_ref_details, 'XX')
              , t2656.c5010_retag_id
               INTO v_old_ref_id, v_old_post_opt, v_old_ref_type
              , v_tag_id, v_old_flag_dt, v_old_post_cmts
              , v_retag_fl, v_old_ref_dtls, v_old_retagid
               FROM t2656_audit_entry t2656
              WHERE t2656.c2656_audit_entry_id = v_id
                AND t2656.c2656_void_fl       IS NULL FOR UPDATE;
            v_new_post_opt                    := NVL (v_post_option, 0) ;
            v_new_flag_dt                     := TO_DATE (v_flag_dt, get_rule_value ('DATEFMT', 'DATEFORMAT')) ;
            --
            IF (v_old_ref_type                       <> v_ref_type OR v_old_ref_id <> v_ref_id OR v_old_post_opt <> v_new_post_opt OR
                v_old_flag_dt                        <> v_new_flag_dt OR v_old_post_cmts <> NVL (v_post_comment, '999') OR v_old_ref_dtls <>
                NVL (v_ref_details, 'XX') OR v_retag IS NOT NULL) THEN
                -- check the already saved or not
                 UPDATE t2656_audit_entry t2656
                SET t2656.c901_ref_type                                  = v_ref_type, t2656.c2656_ref_id = v_ref_id, t2656.c2656_ref_details =
                    v_ref_details, t2656.c901_reconciliation_status      = v_recon_status, t2656.c901_posting_option =
                    v_post_option, t2656.c2656_posting_comment           = v_post_comment, t2656.c2656_flagged_by =
                    v_flag_by, t2656.c2656_flagged_dt                    = v_new_flag_dt, t2656.c5010_retag_id = NVL (
                    v_retag, v_old_retagid), t2656.c2656_last_updated_by = p_user_id, t2656.c2656_last_updated_date =
                    SYSDATE
                  WHERE t2656.c2651_field_sales_details_id = v_details_id
                    AND t2656.c2656_audit_entry_id         = v_id
                    AND t2656.c2656_void_fl               IS NULL;
            END IF;
            IF v_retag IS NOT NULL AND v_retag_fl IS NULL THEN
                gm_pkg_ac_fa_flag_deviation.gm_sav_retag (v_id, p_user_id) ;
            END IF;
            v_retaged_id    := get_retag_id (v_id) ;
            IF v_retaged_id IS NOT NULL THEN
                v_tag_id    := v_retaged_id;
            END IF;
            -- to get the tag type (loaner, consignment)
            v_tag_type := get_tag_type (v_tag_id) ;
            --4127  Product loaner
            IF v_ref_type = 51029 AND v_tag_type <> 4127 THEN
                 SELECT COUNT (1)
                   INTO v_ra_count
                   FROM t506_returns t506
                  WHERE t506.c506_rma_id         = v_ref_id
                    AND t506.c506_void_fl       IS NULL
                    AND t506.c701_distributor_id = p_dist_id
                    AND t506.c506_status_fl      = 2;
                IF v_ra_count                    < 1 THEN
                    raise_application_error ('-20847', '') ; --Please enter the valid RA number
                END IF;
            END IF;
            -- check the Ref type if Transfer To/from or borrowed From or Lent to then
            -- if assigned to (current location) doesnot match then raise an error
            
                 SELECT t2656.c5010_tag_id, t5010.c5010_tag_id, t2656.c2656_ref_id
                  , get_distributor_name (t2656.c2656_ref_id), t2651.c701_distributor_id, get_distributor_name (
                    t2651.c701_distributor_id), DECODE (TO_CHAR (t5010.c901_location_type), '4120', TO_CHAR (
                    t5010.c5010_location_id), '4123', TO_CHAR (t5010.c5010_location_id), TO_CHAR (
                    t5010.c5010_location_id)) currloc, NVL (t2656.c901_status, 0)
                   INTO v_ent_tagid, v_cur_tagid, v_ent_refid
                  , v_ent_refnm, v_ent_distid, v_ent_distnm
                  , v_curr_loc, v_entry_status
                   FROM t2656_audit_entry t2656, t2651_field_sales_details t2651, t5010_tag t5010
                  WHERE t2656.C2651_FIELD_SALES_DETAILS_ID                                            = t2651.C2651_FIELD_SALES_DETAILS_ID
                    AND DECODE (t2656.c2656_retag_fl, NULL, t2656.c5010_tag_id, t2656.c5010_retag_id) =
                    t5010.C5010_TAG_ID (+)
                    AND t2656.C2656_AUDIT_ENTRY_ID = v_id
                    AND t2656.c2656_void_fl       IS NULL
                    AND t2651.c2651_void_fl       IS NULL
                    AND t5010.c5010_void_fl (+)   IS NULL ;
                IF (v_ref_type                      = 51027 OR v_ref_type = 51026) AND v_post_option IS NULL --if Transfer To /lend to / Posting Options
                    THEN
                    IF NVL (v_ent_distid, '-999') <> NVL (v_curr_loc, '-999') THEN
                        GM_RAISE_APPLICATION_ERROR('-20999','107',v_tag_id||'#$#'||v_ent_distnm);
                    END IF;
                    IF NVL (v_ent_refid, '-999') = NVL (v_ent_distid, '-999') THEN
                        GM_RAISE_APPLICATION_ERROR('-20999','108',v_ent_refnm);
                    END IF;
                ELSIF v_ref_type = 51023 OR v_ref_type = 51025 --Transfer From/Borrowed from
                    THEN
                    IF NVL (v_ent_refid, '-999') <> NVL (v_curr_loc, '-999') THEN
                        GM_RAISE_APPLICATION_ERROR('-20999','109',v_tag_id||'#$#'||v_ent_refnm);
                    END IF;
                    IF NVL (v_ent_refid, '-999') = NVL (v_ent_distid, '-999') THEN
                        GM_RAISE_APPLICATION_ERROR('-20999','110',v_ent_refnm||'#$#'||v_ent_distnm);
                    END IF;
                ELSIF v_ref_type = 51024 -- Extra
                    THEN
                    --Raise Error if the flag Reason = "part" and c901_status= 'Complete'
                    IF v_ent_refid = '18562' AND v_entry_status = 51020 -- 18562 Part , 51020 - Complete
                        THEN
                        GM_RAISE_APPLICATION_ERROR('-20999','111',v_tag_id);
                    END IF;
                    --Raise Error if the flag Reason = "Set" and c901_status= 'Empty'
                    IF v_ent_refid = '18561' AND v_entry_status = 51022 THEN
                        GM_RAISE_APPLICATION_ERROR('-20999','112',v_tag_id);
                    END IF;
                    IF v_ent_refid = '18561' AND p_set_id IS NULL -- Set
                        THEN
                        --Set ID cannot be empty for consignment adjustment by set.
                        GM_RAISE_APPLICATION_ERROR('-20999','113','');
                    END IF;
                ELSIF v_ref_type    = 51028 THEN -- Missing
                    IF (v_ent_refid = '51040' OR v_ent_refid = '51041') AND p_set_id IS NULL -- Charge Set, No Charge
                        -- Set
                        THEN
                        --Set ID cannot be empty for consignment adjustment by set.
                        GM_RAISE_APPLICATION_ERROR('-20999','114','');
                    END IF;
                ELSIF v_ref_type = 18552 OR v_ref_type = 18558 THEN -- UnVOid Tag and Void Tag Only
                	 SELECT COUNT (1)
           			   INTO v_cnt
                       FROM t5010_tag t5010
                      WHERE t5010.c5010_tag_id = v_tag_id;
                   IF v_cnt                   = 0 THEN
                      GM_RAISE_APPLICATION_ERROR('-20999','115','');
                   END IF;                
                END IF; --v_ref_type (each)
            
        END LOOP;
    END IF;
END gm_sav_flag_variance;
/********************************************************************************
* Description : Procedure used to save the Retag (t5010_tag)
*********************************************************************************/
PROCEDURE gm_sav_retag (
        p_audit_entry_id IN t2656_audit_entry.c2656_audit_entry_id%TYPE,
        p_user_id        IN t2656_audit_entry.c2656_last_updated_by%TYPE)
AS
    v_cnt NUMBER;
    v_address_id t5010_tag.c106_address_id%TYPE;
    v_partnum t5010_tag.c205_part_number_id%TYPE;
    v_setid t5010_tag.c207_set_id%TYPE;
    v_control_num t5010_tag.c5010_control_number%TYPE;
    v_ref_id t5010_tag.c5010_last_updated_trans_id%TYPE;
    v_loction_id t5010_tag.c5010_location_id%TYPE;
    v_lock_fl t5010_tag.c5010_lock_fl%TYPE;
    v_sub_loc_comm t5010_tag.c5010_sub_location_comments%TYPE;
    v_sub_loc_id t5010_tag.c5010_sub_location_id%TYPE;
    v_sales_rep_id t5010_tag.c703_sales_rep_id%TYPE;
    v_account_id t5010_tag.c704_account_id%TYPE;
    v_inv_type t5010_tag.c901_inventory_type%TYPE;
    v_loction_type t5010_tag.c901_location_type%TYPE;
    v_status t5010_tag.c901_status%TYPE;
    v_sub_loc_type t5010_tag.c901_sub_location_type%TYPE;
    v_trans_type t5010_tag.c901_trans_type%TYPE;
    --
    v_a_tag_id t2656_audit_entry.c5010_tag_id%TYPE;
    v_a_retag_id t2656_audit_entry.c5010_retag_id%TYPE;
    v_a_retag_fl t2656_audit_entry.C2656_RETAG_FL%TYPE;
    v_a_pnum t5010_tag.c205_part_number_id%TYPE;
    v_a_set_id t5010_tag.c207_set_id%TYPE;
    v_a_cntno t5010_tag.c5010_control_number%TYPE;
    v_a_fsdetid t2651_field_sales_details.C2651_FIELD_SALES_DETAILS_ID%TYPE;
    v_a_dist_id t2651_field_sales_details.c701_distributor_id%TYPE;
    --
    v_no_tagid    VARCHAR2 (40) ;
    v_tag_type    NUMBER;
    v_add_comment VARCHAR2 (4000) := ' You cannot Retag the Extra Loaner Set.';
BEGIN
    -- default assign the v_no_tagid values
    v_no_tagid := 'NoTag';
    --Since the transaction Id is null, any loaner retagged will put t5010.c901_inventory_type  as consignment which is
    -- worng ,Need a solution
     SELECT c5010_tag_id, c5010_retag_id, c2656_retag_fl
      , c205_part_number_id, c207_set_id, c2656_control_number
      , c2651_field_sales_details_id fsdetid
       INTO v_a_tag_id, v_a_retag_id, v_a_retag_fl
      , v_a_pnum, v_a_set_id, v_a_cntno
      , v_a_fsdetid
       FROM t2656_audit_entry
      WHERE c2656_audit_entry_id = p_audit_entry_id
        AND c2656_void_fl       IS NULL;
    --Fetch the dist Id
     SELECT c701_distributor_id
       INTO v_a_dist_id
       FROM t2651_field_sales_details
      WHERE c2651_field_sales_details_id = v_a_fsdetid
        AND C2651_VOID_FL               IS NULL;
    IF v_a_retag_fl                     IS NULL AND v_a_retag_id IS NOT NULL THEN
        -- validate the Retag id and status. (only Released, otherwise throw an error message)
         SELECT COUNT (1)
           INTO v_cnt
           FROM t5010_tag t5010
          WHERE t5010.c5010_tag_id   = v_a_retag_id
            AND t5010.c901_status    = 51011 -- Released
            AND t5010.c5010_void_fl IS NULL;
        IF v_cnt                     = 0 THEN
            GM_RAISE_APPLICATION_ERROR('-20999','116','');
        END IF;
        -- validate the Tag id. Tag ID can be a "tag id" , "No Tag" for extra tags ,"No Tag1,2..3" for retagged as
        -- result for broken tag.
        BEGIN
            -- To select all the tag id values and transfer the retag id.
             SELECT t5010.c106_address_id, t5010.c205_part_number_id, t5010.c207_set_id
              , t5010.c5010_control_number, t5010.c5010_last_updated_trans_id, t5010.c5010_location_id
              , t5010.c5010_lock_fl, t5010.c5010_sub_location_comments, t5010.c5010_sub_location_id
              , t5010.c703_sales_rep_id, t5010.c704_account_id, t5010.c901_inventory_type
              , t5010.c901_location_type, t5010.c901_status, t5010.c901_sub_location_type
              , t5010.c901_trans_type
               INTO v_address_id, v_partnum, v_setid
              , v_control_num, v_ref_id, v_loction_id
              , v_lock_fl, v_sub_loc_comm, v_sub_loc_id
              , v_sales_rep_id, v_account_id, v_inv_type
              , v_loction_type, v_status, v_sub_loc_type
              , v_trans_type
               FROM t5010_tag t5010
              WHERE t5010.c5010_tag_id = v_a_tag_id FOR UPDATE ;
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_no_tagid := NULL;
        END;
        --The below code is need for Extra "Extra" loaners found in the field that does not have a tag,do not retag
        -- such loaners
        v_tag_type := get_tag_type (v_a_tag_id) ;
        --
        --for Extra "Extra" loaners found in the field that does not have a tag ,do not retag such loaners
        IF v_tag_type <> 4127 THEN
             UPDATE t5010_tag t5010
            SET t5010.c106_address_id                                             = DECODE (v_no_tagid, NULL, NULL, v_address_id), t5010.c205_part_number_id =
                DECODE (v_no_tagid, NULL, v_a_pnum, v_partnum), t5010.c207_set_id = DECODE (v_no_tagid, NULL,
                v_a_set_id, v_setid), t5010.c5010_control_number                  = DECODE (v_no_tagid, NULL, v_a_cntno
                , v_control_num), t5010.c5010_last_updated_trans_id               = DECODE (v_no_tagid, NULL, NULL,
                v_ref_id), t5010.c5010_location_id                                = DECODE (v_no_tagid, NULL,
                v_a_dist_id, v_loction_id), t5010.c5010_lock_fl                   = DECODE (v_no_tagid, NULL, 'Y',
                v_lock_fl), t5010.c5010_sub_location_comments                     = DECODE (v_no_tagid, NULL, 'Y',
                v_sub_loc_comm), t5010.c5010_sub_location_id                      = DECODE (v_no_tagid, NULL, NULL,
                v_sub_loc_id), t5010.c703_sales_rep_id                            = DECODE (v_no_tagid, NULL, NULL,
                v_sales_rep_id), t5010.c704_account_id                            = DECODE (v_no_tagid, NULL, NULL,
                v_account_id)
                -- Since the transaction Id is null, any loaner retagged will put t5010.c901_inventory_type  as
                -- consignment
                -- which is worng ,Any how c901_inventory_type should always be handled by existing trigger.
                -- ,t5010.c901_inventory_type                                 = DECODE (v_no_tagid, NULL, NULL,
                -- v_inv_type)
              , t5010.c901_location_type                                          = DECODE (v_no_tagid, NULL, 4120, v_loction_type), t5010.c901_status = DECODE
                (v_no_tagid, NULL, 51012, v_status), t5010.c901_sub_location_type = DECODE (v_no_tagid, NULL, NULL,
                v_sub_loc_type), t5010.c901_trans_type                            = DECODE (v_no_tagid, NULL, 51000,
                v_trans_type), t5010.c5010_last_updated_by                        = p_user_id,
                t5010.c5010_last_updated_date                                     = SYSDATE
              WHERE t5010.c5010_tag_id                                            = v_a_retag_id
                AND t5010.c5010_void_fl                                          IS NULL;
            -- to update the Void flag for Tag id
             UPDATE t5010_tag t5010
            SET t5010.c5010_retag_id          = v_a_retag_id, t5010.c5010_void_fl = 'Y', t5010.c5010_last_updated_by = p_user_id
              , t5010.c5010_last_updated_date = SYSDATE
              WHERE t5010.c5010_tag_id        = v_a_tag_id
                AND t5010.c5010_void_fl      IS NULL;
            -- to update the retag flag - in t2656_audit entry table
             UPDATE t2656_audit_entry
            SET c2656_retag_fl           = 'Y', c2656_last_updated_by = p_user_id, c2656_last_updated_date = SYSDATE
              WHERE c2656_audit_entry_id = p_audit_entry_id
                AND c2656_void_fl       IS NULL;
        ELSE
            --Cannot retag this extra Loaner Set.
             UPDATE t2656_audit_entry
            SET c2656_comments           = c2656_comments || v_add_comment, c2656_last_updated_by = p_user_id,
                c2656_last_updated_date  = SYSDATE, c5010_retag_id = NULL
              WHERE c2656_audit_entry_id = p_audit_entry_id
                AND c2656_void_fl       IS NULL;
        END IF; --v_tag_type <>4127
    ELSE
        -- Validate the retag flag (already retag or not)
        GM_RAISE_APPLICATION_ERROR('-20999','117',v_a_tag_id);
    END IF; --Retag id and retag flag check
END gm_sav_retag;

/********************************************************************************
* Description : Procedure used to adjust the locked Set id(t2657)
*********************************************************************************/
PROCEDURE gm_sav_adjust_locked_set (
        p_audit_entry_id IN t2656_audit_entry.c2656_audit_entry_id%TYPE,
        p_user_id        IN t2656_audit_entry.c2656_last_updated_by%TYPE)
AS
    v_partnum t2656_audit_entry.c205_part_number_id%TYPE;
    v_setid t2656_audit_entry.c207_set_id%TYPE;
    v_fs_lock_id t2652_field_sales_lock.c2652_field_sales_lock_id%TYPE;
    v_dist_id t2651_field_sales_details.c701_distributor_id%TYPE;
    v_fs_details_id t2657_field_sales_details_lock.c2657_field_sales_details_lock%TYPE;
    v_tag_qty NUMBER;
    v_ops_qty NUMBER;
    v_sys_qty NUMBER;
    v_count   NUMBER;
BEGIN
    -- to fetch the record (lock id, dist id, part and set id)
     SELECT t2651.c2652_field_sales_lock_id, t2651.c701_distributor_id, t2656.c205_part_number_id
      , t2656.c207_set_id
       INTO v_fs_lock_id, v_dist_id, v_partnum
      , v_setid
       FROM t2656_audit_entry t2656, t2651_field_sales_details t2651
      WHERE t2651.c2651_field_sales_details_id = t2656.c2651_field_sales_details_id
        AND t2656.c2656_audit_entry_id         = p_audit_entry_id
        AND t2651.c2651_void_fl               IS NULL
        AND t2656.c2656_void_fl               IS NULL;
    -- to get the t2657 primary key, tag qty and ops qty
    BEGIN
         SELECT c2657_field_sales_details_lock, DECODE (NVL (c2657_tag_qty, 0), 0, 0, 1),
         DECODE (NVL (c2657_ops_qty, 0), 0, 0, 1), DECODE (NVL (c2657_qty, 0), 0, 0, 1)
           INTO v_fs_details_id, v_tag_qty
           , v_ops_qty, v_sys_qty
           FROM t2657_field_sales_details_lock
          WHERE c205_part_number_id       = v_partnum
            AND c2652_field_sales_lock_id = v_fs_lock_id
            AND c207_set_id              IS NULL
            AND c2657_void_fl            IS NULL;
    EXCEPTION
    WHEN OTHERS THEN
        v_fs_details_id := NULL;
        v_tag_qty       := 0;
        v_ops_qty       := 0;
        v_sys_qty       := 0;
    END;
    -- to update the locked set and qty
     UPDATE t2657_field_sales_details_lock
    SET c207_set_id  = v_setid,
    	c2657_ops_qty = NVL (c2657_ops_qty, 0) + v_ops_qty,
    	c2657_tag_qty = NVL (c2657_tag_qty,0) + v_tag_qty,
    	c2657_qty = NVL (c2657_qty, 0) + v_sys_qty
      WHERE c205_part_number_id       = v_partnum
        AND c207_set_id               = v_setid
        AND c2652_field_sales_lock_id = v_fs_lock_id
        AND c2657_void_fl            IS NULL;
    IF SQL%ROWCOUNT                   = 0 THEN
         INSERT
           INTO t2657_field_sales_details_lock
            (
                c2657_field_sales_details_lock, c2652_field_sales_lock_id, c205_part_number_id
              , c207_set_id, c2657_ops_qty, c2657_tag_qty
              , c2657_qty
            )
            VALUES
            (
                s2659_field_sales_ref_lock.NEXTVAL, v_fs_lock_id, v_partnum
              , v_setid, v_ops_qty, v_tag_qty
              , v_sys_qty
            ) ;
    END IF;
    -- to adjust the qty as set id null.
     UPDATE t2657_field_sales_details_lock
    SET c2657_ops_qty   = NVL (c2657_ops_qty, 0) - v_ops_qty
       , c2657_tag_qty = NVL (c2657_tag_qty, 0) - v_tag_qty,
        c2657_qty   = NVL (c2657_qty, 0)     - v_sys_qty
      WHERE c2657_field_sales_details_lock = v_fs_details_id
        AND c207_set_id                   IS NULL
        AND c2657_void_fl                 IS NULL;
END gm_sav_adjust_locked_set;
--
END gm_pkg_ac_fa_flag_deviation;
/