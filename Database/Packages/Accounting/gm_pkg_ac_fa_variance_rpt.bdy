/* Formatted on 2012/28/08 23:20 (Formatter Plus v4.8.0) */
--@"c:\database\packages\accounting\gm_pkg_ac_fa_variance_rpt.bdy"
CREATE OR REPLACE 
PACKAGE BODY gm_pkg_ac_fa_variance_rpt
IS
/*********************************************************************
* Description : Procedure to fetch the returns credited info details
**********************************************************************/

	PROCEDURE gm_fch_returns_credit_dtl (
        p_dist_id  			IN  t506_returns.c701_distributor_id%TYPE,
        p_set_id   			IN  t506_returns.c207_set_id%TYPE,
        p_rtcreddtl 		OUT TYPES.cursor_type ) 
	AS
	BEGIN
		OPEN p_rtcreddtl FOR	
   	  	   SELECT t506.c506_rma_id RAID
   	  	   		, t5011.c5010_tag_id TAG_ID
   	  	   		, t506.c207_set_id SET_ID
   	  	   		, get_user_name (t506.c506_created_by) CREATEDBY
  			 	, t506.c506_created_date INTITDATE
  			 	, t506.c506_credit_date CREDITDT
    		 	, get_user_name (t507.c507_reconciled_by) RECONCILEDBY
    		 	, get_code_name (t507.c901_missing_tag_reason) Recon_Reason
    		 FROM t506_returns t506, t507_returns_item t507, t208_set_details t208, t5011_tag_log t5011
  		  	WHERE t506.c506_rma_id         = t507.c506_rma_id
		   	  AND t208.c207_set_id         = t506.c207_set_id
		      AND t507.c506_rma_id 		   = t5011.c5011_last_updated_trans_id(+)
		      AND t506.c701_distributor_id = p_dist_id		      
		      AND t208.c205_part_number_id = t507.c205_part_number_id
		      AND t506.c207_set_id         = p_set_id
		      AND t208.c208_critical_fl    = 'Y'
		      AND t506.c506_void_fl        IS NULL
		      AND t506.c506_status_fl 	   = 2  
	  	 ORDER BY t506.c506_rma_id ;	
	END gm_fch_returns_credit_dtl;
	
/*******************************************************************************************
* Description : Procedure to fetch Variance Report by Dist and Global Variance by Set
********************************************************************************************/

	PROCEDURE gm_fetch_variance_by_set (
	    p_audit_id 			IN t2652_field_sales_lock.c2650_field_sales_audit_id%TYPE,
        p_set_id   			IN t2657_field_sales_details_lock.c207_set_id%TYPE,
        p_reg_id   			IN t701_distributor.c701_region%TYPE,
        p_lock_frm_dt       IN VARCHAR2,
        p_lock_to_dt        IN VARCHAR2,
        p_out_report_info 	OUT TYPES.cursor_type) 
	AS
	v_lock_from_dt DATE;
	v_lock_to_dt DATE;
	BEGIN
		v_lock_from_dt := TO_DATE(p_lock_frm_dt,GET_RULE_VALUE('DATEFMT','DATEFORMAT'));
		v_lock_to_dt   := TO_DATE(p_lock_to_dt,GET_RULE_VALUE('DATEFMT','DATEFORMAT'));
		
		OPEN p_out_report_info FOR	
					SELECT NULL pline
					     , p_set_id set_id
					     , dist_id
  						 , dist_name
  						 , SUM (audit_qty) audit_qty
  						 , SUM (tag_qty) tag_qty  
  						 , SUM (ops_qty) ops_qty
  						 , SUM (system_qty) system_qty    
  						 --Dev    
  						 , SUM (posdev) + SUM (negdev) deviation_qty	
  						 --Positive Dev
  						 , SUM (unvoid_tag) unvoid_tag						
  						 , SUM (not_a_dev_pos) not_a_dev_pos
  						 , SUM (transfer_from) transfer_from		
  						 , SUM (borrowed_from) borrowed_from
  						 , SUM (extra) extra
  						  --Negative Dev
  						 , SUM (void_tag) void_tag						   
  						 , SUM (returns) returns
  						 , SUM (not_a_dev_neg) not_a_dev_neg
  						 , SUM (ver_post_cnt) ver_post_cnt
  						 , SUM (lend_to) lend_to
  						 , SUM (transfer_to) transfer_to
  						 , SUM (MISSING) MISSING
  						 , SUM (flagged_qty) flagged_qty  
  						 , (SUM (posdev) + SUM (negdev)) - SUM (flagged_qty) unresolved_qty
  						 , get_log_flag (p_set_id || dist_id, 1251) log_fl
  						 , SUM (approved_qty) approved_qty
  						 , SUM (reconciled_qty) reconciled_qty
  						 , DECODE (SUM (posdev)+SUM(negdev), SUM (flagged_qty), 0, 1) disp_nonflagged
                         ,(SUM (posdev)+SUM(negdev)) - SUM (approved_qty) disp_nonapproved
                         ,(SUM (posdev)+SUM(negdev)) - SUM (reconciled_qty) disp_nonReconciled
   					 FROM
   					 (
         				SELECT t701.dist_id
         					 , t701.dist_name
         					 , NVL ( audited_data.audit_qty, 0) audit_qty
          					 , NVL (locked_data.tag_qty, 0) tag_qty
          					 , NVL (locked_data.ops_qty, 0) ops_qty
          					 , NVL (locked_data.system_qty, 0) system_qty    
          					 --Positive dev
          					 , NVL (audited_data.unvoid_tag, 0) unvoid_tag			
          					 , NVL (audited_data.not_a_dev_pos, 0) not_a_dev_pos
          					 , NVL (audited_data.transfer_from, 0) transfer_from
          					 , NVL (audited_data.borrowed_from, 0) borrowed_from
          					 , NVL (audited_data.extra, 0) extra
          					 --Negative dev
          					 , NVL (audited_data.void_tag, 0) void_tag				
          					 , NVL (audited_data.returns, 0) returns
          					 , NVL (audited_data.not_a_dev_neg, 0) not_a_dev_neg
          					 , NVL (audited_data.ver_post_cnt, 0) ver_post_cnt
          					 , NVL (audited_data.lend_to, 0) lend_to
          					 , NVL (audited_data.transfer_to, 0) transfer_to
          					 , NVL (audited_data.MISSING,0) MISSING
          					 --Tag status
          					 , NVL (matching, 0) matching							
          					 , NVL (posdev, 0) posdev
          					 , NVL (negdev, 0) negdev
          					 , NVL (audited_data.approved_qty, 0) approved_qty		
          					 --Reconcile
          					 , NVL (audited_data.reconciled_qty, 0) reconciled_qty  
          					 --Flagged qty
          					 	--positive
          					 , NVL(audited_data.flagged_qty,0) flagged_qty
            							--Postivie flagged Qty
            							 , (NVL (audited_data.unvoid_tag, 0) 
            							 + NVL (audited_data.not_a_dev_pos, 0) + NVL (audited_data.transfer_from, 0	)
            							 + NVL (audited_data.borrowed_from, 0) + NVL (audited_data.extra, 0)) positive_flagged_qty	
            							--Negative flagged Qty
            			     , (+ NVL (audited_data.void_tag, 0) + NVL (audited_data.returns, 0) + NVL (audited_data.not_a_dev_neg, 0)
            			     	  + NVL (audited_data.ver_post_cnt, 0) + NVL (audited_data.lend_to, 0) + NVL (audited_data.transfer_to, 0) 
            			     	    + NVL (audited_data.MISSING, 0)) negative_flagged_qty															 
           				 FROM
           				 (
                 			SELECT t701.c701_distributor_id dist_id
                 			     , t701.c701_distributor_name dist_name
                   			  FROM t701_distributor t701 
                   			 WHERE t701.c701_region = DECODE(p_reg_id,'0',t701.c701_region,p_reg_id)
            			 )
            	   t701, (
                 			SELECT t2652.c701_distributor_id dist_id
                 			     , SUM (c2657_tag_qty) tag_qty
                 			     , SUM (c2657_ops_qty) ops_qty
                  				 , SUM (c2657_qty) system_qty
                   			  FROM t2652_field_sales_lock t2652, t2657_field_sales_details_lock t2657
                  			 WHERE t2652.c2650_field_sales_audit_id 	= DECODE (p_audit_id, '0', t2652.c2650_field_sales_audit_id, p_audit_id)
                  			   AND t2652.c2650_field_sales_audit_id 	<> 1
                  			   AND DECODE (p_set_id, '0', '-99999', t2657.c207_set_id)     = DECODE (p_set_id,'0', '-99999', p_set_id) 
                    		   AND t2652.c2652_field_sales_lock_id  	= t2657.c2652_field_sales_lock_id
                    		   AND TRUNC(t2652.c2652_locked_date)      >= TRUNC(DECODE(v_lock_from_dt, null,t2652.c2652_locked_date,v_lock_from_dt))
                               AND TRUNC(t2652.c2652_locked_date)      <= TRUNC(DECODE(v_lock_to_dt, null,t2652.c2652_locked_date,v_lock_to_dt)) 
                    		   AND t2652.c2652_void_fl             		IS NULL
                    		   AND t2657.c2657_void_fl             		IS NULL
               			  GROUP BY t2652.c701_distributor_id
            			 )
            		locked_data, (
                 			SELECT dist_id
                 			     , SUM (audit_qty) audit_qty
                 			     , SUM (unvoid_tag) unvoid_tag
                 			     , SUM (not_a_dev_pos) not_a_dev_pos
                 			     , SUM (transfer_from) transfer_from
                 			     , SUM (borrowed_from) borrowed_from
                 			     , SUM (extra) extra
                 			     , SUM (void_tag) void_tag
                 			     , SUM (returns) returns
                 			     , SUM (not_a_dev_neg) not_a_dev_neg
                 			     , SUM (ver_post_cnt) ver_post_cnt
                 			     , SUM (lend_to) lend_to
                 			     , SUM (transfer_to) transfer_to
                 			     , SUM (MISSING) MISSING
                 			     --Tag status
                 			     , SUM (matching) matching		
                 			     , SUM (posdev) posdev
                 			     , SUM (negdev) negdev
                  				 , SUM (manual_entry) manual_entry
                  				 , SUM (approved_qty) approved_qty
                  				 , SUM (reconciled_qty) reconciled_qty
                  				 , SUM(flagged_qty) flagged_qty
                   			 FROM
                   			 (
                         	SELECT t2651.c701_distributor_id dist_id
                         		 , SUM (DECODE (t2656.c901_entry_type, 51030,t2656.c2656_qty, 0)) audit_qty --audited
                          		 , SUM (DECODE (t2656.c901_ref_type, 18552, 1, 0)) unvoid_tag
                          		 , SUM (DECODE (t2656.c901_tag_status, 18542, DECODE (t2656.c901_ref_type, 18551, 1, 0), 0)) not_a_dev_pos
                          		 , SUM (DECODE (t2656.c901_ref_type, 51023, 1, 0)) transfer_from
                          		 , SUM (DECODE (t2656.c901_ref_type, 51025, 1, 0)) borrowed_from
                          		 , SUM (DECODE (t2656.c901_ref_type, 51024, 1, 0)) extra
                          		 , SUM (DECODE (t2656.c901_ref_type, 18558, 1, 0)) void_tag
                          		 , SUM (DECODE (t2656.c901_ref_type, 51029, 1, 0)) returns
                          		 , SUM (DECODE (t2656.c901_ref_type, 18557, 1, 0)) not_a_dev_neg
                          		 , SUM (DECODE (t2656.c901_ref_type, 18556, 1, 0)) ver_post_cnt
                          		 , SUM (DECODE (t2656.c901_ref_type, 51026, 1, 0)) lend_to
                          		 , SUM (DECODE (t2656.c901_ref_type, 51027, 1, 0)) transfer_to
                          		 , SUM (DECODE (t2656.c901_ref_type, 51028, 1, 0)) MISSING
                          		 , SUM (DECODE (t2656.c901_tag_status, 18541, 1, 0)) matching
                          		 , SUM (DECODE (t2656.c901_tag_status, 18542, 1, 0)) posdev
                          		 , SUM (DECODE (t2656.c901_tag_status, 18543, 1, 0)) negdev                           
                          		 , SUM (DECODE (t2656.c901_entry_type, 51031, 1, 0)) manual_entry    ---auditcnt
                          		 , SUM (DECODE (t2656.c901_reconciliation_status, 51037,1,51038, 1, 0)) approved_qty
                          		 , SUM (DECODE (t2656.c901_reconciliation_status, 51038, 1, 0)) reconciled_qty
                          		 , SUM (DECODE (t2656.c2656_flagged_by, NULL, 0, 1)) flagged_qty
                           	  FROM t2651_field_sales_details t2651, t2656_audit_entry t2656, t2652_field_sales_lock t2652
                             WHERE t2651.c2650_field_sales_audit_id 	= DECODE (p_audit_id,'0', t2651.c2650_field_sales_audit_id, p_audit_id)
                               AND t2651.c2650_field_sales_audit_id 	<> 1
                               AND t2652.c2650_field_sales_audit_id 	= t2651.c2650_field_sales_audit_id
                               AND DECODE (p_set_id, '0', '-99999', t2656.c207_set_id)     = DECODE (p_set_id,'0', '-99999', p_set_id)
                               AND t2651.c2651_field_sales_details_id 	= t2656.c2651_field_sales_details_id
                               AND t2651.c2652_field_sales_lock_id      = t2652.c2652_field_sales_lock_id
                               AND TRUNC(t2652.c2652_locked_date)      >= TRUNC(DECODE(v_lock_from_dt, null,t2652.c2652_locked_date,v_lock_from_dt))
                           	   AND TRUNC(t2652.c2652_locked_date)      <= TRUNC(DECODE(v_lock_to_dt, null,t2652.c2652_locked_date,v_lock_to_dt))
                               AND t2656.c2656_void_fl               	IS NULL
                               AND t2651.c2651_void_fl               	IS NULL
                          GROUP BY t2651.c701_distributor_id
                    		)
               			GROUP BY dist_id
            			) audited_data
          		WHERE t701.dist_id              	= locked_data.dist_id(+)
            	  AND t701.dist_id              	= audited_data.dist_id(+)
            	  AND ((NVL (audit_qty, 0) <> 0 OR NVL (system_qty, 0)<> 0))                 
    			)
		GROUP BY dist_id, dist_name
		ORDER BY dist_name;
END gm_fetch_variance_by_set;		
	
/******************************************************************************************
* Description : Procedure to fetch Variance Report by Dist and Global Variance by Set
*******************************************************************************************/

	PROCEDURE gm_fetch_variance_by_dist (
	     p_audit_id 		IN  t2652_field_sales_lock.c2650_field_sales_audit_id%TYPE,
	     p_dist_id  		IN  t2651_field_sales_details.c701_distributor_id%TYPE,
	     p_lock_frm_dt      IN  VARCHAR2,
	     p_lock_to_dt       IN  VARCHAR2,
	     p_out_report_info 	OUT TYPES.cursor_type,
	     p_set_id           IN  VARCHAR2 DEFAULT NULL,
	     p_pnum             IN  t2656_audit_entry.c205_part_number_id%TYPE DEFAULT NULL)
	AS
	v_lock_from_dt DATE;
	v_lock_to_dt DATE;
	BEGIN
		v_lock_from_dt := TO_DATE(p_lock_frm_dt,GET_RULE_VALUE('DATEFMT','DATEFORMAT'));
		v_lock_to_dt   := TO_DATE(p_lock_to_dt,GET_RULE_VALUE('DATEFMT','DATEFORMAT'));
			
    	OPEN p_out_report_info FOR
				SELECT NULL pline
						  , p_dist_id dist_id
     					  , pnum
     					  , pdesc
     					  , set_id
     					  , SYSTEM 
   						  , set_desc  
  						  , SUM (audit_qty) audit_qty
  						  , SUM (tag_qty) tag_qty
  						  , SUM (ops_qty) ops_qty
  						  , SUM (system_qty) system_qty    
  						  --Dev  
  						  , SUM (posdev)+SUM(negdev) deviation_qty   
  						  --Positive Dev
  						  , SUM (unvoid_tag) unvoid_tag
  						  , SUM (not_a_dev_pos) not_a_dev_pos
  						  , SUM (transfer_from) transfer_from
  						  , SUM (borrowed_from) borrowed_from
  						  , SUM (extra) extra    
  						  --Negative Dev
  						  , SUM (void_tag) void_tag
  						  , SUM (returns) returns
  						  , SUM (not_a_dev_neg) not_a_dev_neg
  						  , SUM (ver_post_cnt) ver_post_cnt
  						  , SUM (lend_to) lend_to
  						  , SUM (transfer_to) transfer_to
  						  , SUM (MISSING) MISSING    
   						  , SUM (flagged_qty) flagged_qty  
  						  , SUM (posdev)+SUM(negdev)-SUM (flagged_qty) unresolved_qty
  						  , get_log_flag (p_dist_id || set_id, 1251) log_fl
  						  , SUM (approved_qty) approved_qty
  						  , SUM (reconciled_qty) reconciled_qty
  						  , DECODE (SUM (posdev)+SUM(negdev), SUM (flagged_qty), 0, 1) disp_nonflagged
                          ,(SUM (posdev)+SUM(negdev)) - SUM (approved_qty) disp_nonapproved
                          ,(SUM (posdev)+SUM(negdev)) - SUM (reconciled_qty) disp_nonReconciled
   				 FROM
   				 	(
         			SELECT NVL(NVL (t207.c205_part_number_id, locked_data.c205_part_number_id),audited_data.c205_part_number_id) pnum         --Set
          			     , NVL(NVL (t207.c207_set_id, locked_data.c207_set_id ),audited_data.c207_set_id) set_id          
          				 , NVL(NVL (t207.set_desc, locked_data.set_desc),audited_data.set_desc) set_desc   -- t207.set_desc set_desc set_desc           
          				 , NVL(NVL (t207.pdesc, locked_data.pdesc),audited_data.pdesc) pdesc
          				 , t207.SYSTEM        
         				 , SUM(NVL (audited_data.audit_qty, 0)) audit_qty
         				 , SUM(NVL (locked_data.tag_qty, 0)) tag_qty
         				 , SUM(NVL (locked_data.ops_qty, 0)) ops_qty
         				 , SUM(NVL (locked_data.system_qty, 0)) system_qty            
         				 --Positive dev
          				 , SUM(NVL (audited_data.unvoid_tag, 0)) unvoid_tag
          				 , SUM(NVL (audited_data.not_a_dev_pos, 0)) not_a_dev_pos
          				 , SUM(NVL (audited_data.transfer_from, 0)) transfer_from
          				 , SUM(NVL (audited_data.borrowed_from, 0)) borrowed_from
          				 , SUM(NVL (audited_data.extra, 0)) extra       
          				 --Negative dev
          				 , SUM(NVL (audited_data.void_tag, 0)) void_tag
          				 , SUM(NVL (audited_data.returns, 0)) returns
          				 , SUM(NVL (audited_data.not_a_dev_neg, 0)) not_a_dev_neg
          				 , SUM(NVL (audited_data.ver_post_cnt, 0)) ver_post_cnt
          				 , SUM(NVL (audited_data.lend_to, 0)) lend_to
          				 , SUM(NVL (audited_data.transfer_to, 0)) transfer_to
          				 , SUM(NVL (audited_data.MISSING, 0)) MISSING        
          				 --Tag status
                   		 , SUM(NVL (matching,0)) matching
                    	 , SUM(NVL (posdev,0)) posdev
                    	 , SUM(NVL (negdev,0)) negdev  
                    	 --Reconcile
         				 , SUM(NVL (audited_data.approved_qty, 0)) approved_qty, SUM(NVL (audited_data.reconciled_qty, 0)) reconciled_qty 
         				  --Flagged qty
         				   --positive
             			 , SUM (NVL(audited_data.flagged_qty,0)) flagged_qty
                                        --Postivie flagged Qty
                         , SUM(NVL (audited_data.unvoid_tag, 0)    + NVL (audited_data.not_a_dev_pos, 0) + NVL (audited_data.transfer_from, 0) +            
            				  NVL (audited_data.borrowed_from, 0) + NVL (audited_data.extra, 0)) positive_flagged_qty            
            				  			--Negative flagged Qty
            		     , SUM(  + NVL (audited_data.void_tag, 0) + NVL (audited_data.returns, 0) 
            		            + NVL (audited_data.not_a_dev_neg, 0) + NVL (audited_data.ver_post_cnt, 0) 
            		              + NVL(audited_data.lend_to, 0) + NVL (audited_data.transfer_to, 0) + NVL (audited_data.MISSING, 0)) negative_flagged_qty         
          			 FROM
          			 (
	                 SELECT t207.c207_set_id
	                 	  , t207.c207_set_nm set_desc
	                 	  , t208.c205_part_number_id
	                 	  , t205.c205_part_num_desc pdesc
	                 	  , get_set_name (t207.c207_set_sales_system_id) SYSTEM
	                   FROM t207_set_master t207, t208_set_details t208, t205d_part_attribute t205d,t205_part_number t205
	                  WHERE t207.c207_void_fl    		IS NULL
	                    AND t208.C208_VOID_FL    		IS NULL
	                    AND t205d.c205d_void_fl  		IS NULL
	                    AND t207.c207_type       		IN (4070, 4074, 4078, 4073, 0)
	                    AND t208.c208_critical_fl 		= 'Y'
	                    AND t205d.c205d_attribute_value = 'Y'
	                    AND t205d.c901_attribute_type   = 92340 -- taggable part
	                    AND t207.c207_set_id            = t208.c207_set_id
	                    AND t208.c205_part_number_id    = t205d.c205_part_number_id
	                    AND t208.c205_part_number_id    = t205.c205_part_number_id
            		)
            		t207
	        FULL OUTER JOIN
	            	( SELECT t2657.c205_part_number_id
	            	       , t2657.c207_set_id
	            	       , get_set_name(t2657.c207_set_id) set_desc
	            	       , t205.c205_part_num_desc pdesc
	            	       , SUM (c2657_tag_qty) tag_qty
	            	       , SUM (c2657_ops_qty) ops_qty
	            	       , SUM (c2657_qty) system_qty 
	            	    FROM t2652_field_sales_lock t2652,t2657_field_sales_details_lock t2657 , t205_part_number t205
	            	   WHERE t2652.c2650_field_sales_audit_id = DECODE (p_audit_id,'0', t2652.c2650_field_sales_audit_id, p_audit_id)
	            	     AND t2652.c2650_field_sales_audit_id <> 1
	            	   	 AND TRUNC(t2652.c2652_locked_date)     >= TRUNC(DECODE(v_lock_from_dt, null,t2652.c2652_locked_date,v_lock_from_dt))
                         AND TRUNC(t2652.c2652_locked_date)     <= TRUNC(DECODE(v_lock_to_dt, null,t2652.c2652_locked_date,v_lock_to_dt))
	                     AND t2652.c701_distributor_id        = NVL (p_dist_id,  t2652.c701_distributor_id)
	                     AND t2652.c2652_field_sales_lock_id  = t2657.c2652_field_sales_lock_id 
	                     AND t2657.c205_part_number_id        = t205.c205_part_number_id
	                     AND DECODE(p_set_id,NULL,'-99999','SET_NULL',NVL2 (t2657.c207_set_id, t2657.c207_set_id, '-999'),t2657.c207_set_id) = DECODE(p_set_id,NULL,'-99999','SET_NULL','-999',p_set_id)
                         AND t2657.c205_part_number_id = NVL(p_pnum,t2657.c205_part_number_id)
	                     AND t2652.c2652_void_fl              IS NULL 
	                     AND t2657.c2657_void_fl              IS NULL 
	                GROUP BY t2657.c205_part_number_id, t2657.c207_set_id,t205.c205_part_num_desc
	                ) 
	                locked_data ON (t207.c207_set_id = locked_data.c207_set_id  AND t207.c205_part_number_id = locked_data.c205_part_number_id)
        	FULL OUTER JOIN
            		( SELECT c205_part_number_id
            		       , c207_set_id
            		       , set_desc
            		       , pdesc
            		       , SUM (audit_qty) audit_qty
            		       , SUM (unvoid_tag) unvoid_tag
            		       , SUM (not_a_dev_pos) not_a_dev_pos
                  		   , SUM (transfer_from) transfer_from
                  		   , SUM (borrowed_from) borrowed_from
                  		   , SUM (extra) extra
                  		   , SUM (void_tag) void_tag
                  		   , SUM (returns) returns
                  		   , SUM (not_a_dev_neg) not_a_dev_neg
                  		   , SUM (ver_post_cnt) ver_post_cnt
                  		   , SUM (lend_to) lend_to
                  		   , SUM (transfer_to) transfer_to
                  		   , SUM (MISSING) MISSING     
                  		   --Tag status
                    	   , SUM (matching) matching						 
                    	   , SUM (posdev) posdev
                    	   , SUM (negdev) negdev
                  		   , SUM (manual_entry) manual_entry
                  		   , SUM (approved_qty) approved_qty
                  		   , SUM (reconciled_qty) reconciled_qty 
                  		   , SUM (flagged_qty) flagged_qty
                  	   FROM
                    	( SELECT t2656.c205_part_number_id
                    	       , t2656.c207_set_id
                    	       , get_set_name(t2656.c207_set_id) set_desc
                    	       , t205.c205_part_num_desc pdesc
                    	       , SUM (DECODE (t2656.c901_entry_type, 51030,t2656.c2656_qty, 0)) audit_qty --audited
                               , SUM (DECODE (t2656.c901_ref_type, 18552, 1, 0)) unvoid_tag
                               , SUM (DECODE (t2656.c901_tag_status,18542,DECODE(t2656.c901_ref_type, 18551, 1, 0),0)) not_a_dev_pos
                               , SUM (DECODE (t2656.c901_ref_type, 51023, 1, 0)) transfer_from
                               , SUM (DECODE (t2656.c901_ref_type, 51025, 1, 0)) borrowed_from
                               , SUM (DECODE (t2656.c901_ref_type, 51024, 1, 0)) extra
                               , SUM (DECODE (t2656.c901_ref_type, 18558, 1, 0)) void_tag
                               , SUM (DECODE (t2656.c901_ref_type, 51029, 1, 0)) returns
                               , SUM (DECODE (t2656.c901_ref_type, 18557, 1, 0)) not_a_dev_neg
                               , SUM (DECODE (t2656.c901_ref_type, 18556, 1, 0)) ver_post_cnt
                               , SUM (DECODE (t2656.c901_ref_type, 51026, 1, 0)) lend_to
                               , SUM (DECODE (t2656.c901_ref_type, 51027, 1, 0)) transfer_to
                               , SUM (DECODE (t2656.c901_ref_type, 51028, 1, 0)) MISSING
                               , SUM (DECODE (t2656.c901_tag_status, 18541, 1, 0)) matching
                               , SUM (DECODE (t2656.c901_tag_status, 18542, 1, 0)) posdev
                               , SUM (DECODE (t2656.c901_tag_status, 18543, 1, 0)) negdev
                               , SUM (DECODE (t2656.c901_entry_type, 51031, 1, 0)) manual_entry
                               , SUM (DECODE (t2656.c901_reconciliation_status, 51037, 1,51038, 1, 0)) approved_qty
                               , SUM (DECODE (t2656.c901_reconciliation_status, 51038, 1, 0)) reconciled_qty 
                               , SUM (DECODE (t2656.c2656_flagged_by, NULL, 0, 1)) flagged_qty
                           FROM t2651_field_sales_details t2651, t2656_audit_entry t2656,t205_part_number t205,t2652_field_sales_lock t2652
                          WHERE t2651.c2650_field_sales_audit_id   = DECODE (p_audit_id,'0', t2651.c2650_field_sales_audit_id, p_audit_id)
                            AND t2651.c2650_field_sales_audit_id   <> 1
                            AND t2652.c2650_field_sales_audit_id   = t2651.c2650_field_sales_audit_id
                            AND (t2651.c701_distributor_id         = NVL (p_dist_id, t2651.c701_distributor_id)) 
                            AND t2651.c2651_field_sales_details_id = t2656.c2651_field_sales_details_id 
                            AND t2651.c2652_field_sales_lock_id    = t2652.c2652_field_sales_lock_id
                            AND t2656.c205_part_number_id 		   = t205.c205_part_number_id
                            AND DECODE(p_set_id,NULL,'-99999','SET_NULL',NVL2 (t2656.c207_set_id, t2656.c207_set_id, '-999'),t2656.c207_set_id) = DECODE(p_set_id,NULL,'-99999','SET_NULL','-999',p_set_id)
                            AND t2656.c205_part_number_id = NVL(p_pnum,t2656.c205_part_number_id)
                            AND TRUNC(t2652.c2652_locked_date)     >= TRUNC(DECODE(v_lock_from_dt, null,t2652.c2652_locked_date,v_lock_from_dt))
                            AND TRUNC(t2652.c2652_locked_date)     <= TRUNC(DECODE(v_lock_to_dt, null,t2652.c2652_locked_date,v_lock_to_dt))
                            AND t2656.c2656_void_fl                IS NULL 
                            AND t2651.c2651_void_fl                IS NULL
                       GROUP BY t2656.c205_part_number_id, t2656.c207_set_id,t205.c205_part_num_desc
                    ) GROUP BY c205_part_number_id, c207_set_id,pdesc,set_desc
            	) 
            	audited_data ON t207.c207_set_id = audited_data.c207_set_id AND t207.c205_part_number_id =  audited_data.c205_part_number_id
            	GROUP BY NVL (NVL (t207.c205_part_number_id, locked_data.c205_part_number_id), audited_data.c205_part_number_id)
            , NVL (NVL (t207.c207_set_id, locked_data.c207_set_id), audited_data.c207_set_id), NVL (NVL (t207.set_desc,
            locked_data.set_desc), audited_data.set_desc), NVL (NVL (t207.pdesc, locked_data.pdesc), audited_data.pdesc
            ), t207.SYSTEM
    		)
    	WHERE ( (NVL (audit_qty, 0) <> 0 OR NVL (system_qty, 0) <> 0))
	 GROUP BY set_id, pnum,set_desc,pdesc , SYSTEM
	 ORDER BY set_id, pnum;
	 
END gm_fetch_variance_by_dist;	
END gm_pkg_ac_fa_variance_rpt;
/
