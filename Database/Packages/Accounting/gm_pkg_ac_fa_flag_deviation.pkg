
--@"c:\database\packages\accounting\gm_pkg_ac_fa_flag_deviation.pkg";

CREATE OR REPLACE
PACKAGE gm_pkg_ac_fa_flag_deviation
IS

	PROCEDURE gm_fch_uploaded_data (
		p_audit_id     IN t2652_field_sales_lock.c2650_field_sales_audit_id%TYPE,
		p_dist_id      IN t2651_field_sales_details.c701_distributor_id%TYPE,
		p_countby      IN t2656_audit_entry.c2656_counted_by%TYPE,
		p_entry_type   IN t2656_audit_entry.c901_entry_type%TYPE,
		p_status	   IN VARCHAR,
		p_out_report_info OUT TYPES.cursor_type) ;
		
/*******************************************************
 * Description : Procedure to fetch Audit Entry Details
 * Author		 : Arockia Prasath
 *******************************************************/	
	
	PROCEDURE gm_fch_edit_uploaded_data (
		p_audit_entry_id	   IN		t2656_audit_entry.c2656_audit_entry_id%TYPE,
	    p_audit_entry_detail   OUT		TYPES.cursor_type);
	
	
/*******************************************************
 * Description : Procedure to fetch location details
 * Author		 : Arockia Prasath
 *******************************************************/		
	
	PROCEDURE gm_fch_location_info (
        p_location_id t2656_audit_entry.c2656_location_id%TYPE,
        p_location_type t2656_audit_entry.c901_location_type%TYPE,
    	p_out_location_info OUT TYPES.cursor_type);
    	
    	
 	PROCEDURE gm_sav_edit_uploaded_data (
		p_tagid 		   IN	t2656_audit_entry.c5010_tag_id%TYPE
	  , p_ptnum 		   IN	t2656_audit_entry.c205_part_number_id%TYPE
	  , p_ctrlnum		   IN	t2656_audit_entry.c2656_control_number%TYPE
	  , p_setid 		   IN	t2656_audit_entry.c207_set_id%TYPE
	  , p_comments		   IN	t2656_audit_entry.c2656_comments%TYPE
	  , p_status_id 	   IN	t2656_audit_entry.c901_status%TYPE
	  , p_distributor_id   IN	t2656_audit_entry.c2656_ref_id%TYPE
	  , p_loctype		   IN	t2656_audit_entry.c901_location_type%TYPE
	  , p_locid 		   IN	t2656_audit_entry.c2656_location_id%TYPE
	  , p_loc_details      IN	VARCHAR2
	  , p_flag_id          IN	t2656_audit_entry.c2656_ref_id%TYPE
	  , p_flag_type        IN	t2656_audit_entry.c901_ref_type%TYPE
	  , p_loc_comments     IN   t2656_audit_entry.c2656_address_others%TYPE
	  , p_cuntdate		   IN	VARCHAR2
	  , p_userid		   IN	t2656_audit_entry.c2656_last_updated_by%TYPE
	  , p_audit_entry_id   IN	t2656_audit_entry.c2656_audit_entry_id%TYPE
	  , p_locked_fl        IN   CHAR
	  , p_status           IN   VARCHAR
	);
	
	
	PROCEDURE gm_sav_locked_set_data (
		p_tagid 		   IN	t2656_audit_entry.c5010_tag_id%TYPE
	  , p_setid 		   IN	t2656_audit_entry.c207_set_id%TYPE
	  , p_distributor_id   IN	t2656_audit_entry.c2656_ref_id%TYPE
	  , p_audit_entry_id   IN	t2656_audit_entry.c2656_audit_entry_id%TYPE
	  , p_userid		   IN	t2656_audit_entry.c2656_last_updated_by%TYPE
	  
	);
 /********************************************************************************
    * Description : Procedure to fetch Flag Report For Positive and Negative
    *********************************************************************************/
	PROCEDURE gm_fch_flag_variance (
        p_audit_id IN t2651_field_sales_details.c2650_field_sales_audit_id%TYPE,
        p_dist_id  IN t2651_field_sales_details.c701_distributor_id%TYPE,
        p_set_id   IN t2656_audit_entry.c207_set_id%TYPE,
        p_pnum   IN t2656_audit_entry.c205_part_number_id%TYPE,
        p_out_header_info OUT TYPES.cursor_type,
        p_audit_rpt OUT TYPES.cursor_type,
        p_matching_rpt OUT TYPES.cursor_type,
        p_positive_rpt OUT TYPES.cursor_type,
        p_negative_rpt OUT TYPES.cursor_type,
        p_associated_rpt_pos OUT TYPES.cursor_type,
        p_associated_rpt_neg OUT TYPES.cursor_type);
        
  /********************************************************************************
    * Description : Procedure to fetch Part, Set and System details
    *********************************************************************************/      
   PROCEDURE gm_fch_header_info(
        p_dist_id  IN t2651_field_sales_details.c701_distributor_id%TYPE,
   		p_set_id   IN t2657_field_sales_details_lock.c207_set_id%TYPE,
	    p_pnum   IN t2657_field_sales_details_lock.c205_part_number_id%TYPE,
	    p_out_header OUT TYPES.cursor_type);
  
    /********************************************************************************
    * Description : Procedure to fetch Flag Report For Positive and Negative
    *********************************************************************************/          
        PROCEDURE gm_fch_deviation_rpt(
   		p_audit_id IN t2651_field_sales_details.c2650_field_sales_audit_id%TYPE,
        p_dist_id  IN t2651_field_sales_details.c701_distributor_id%TYPE,
        p_set_id   IN t2656_audit_entry.c207_set_id%TYPE,
        p_pnum   IN t2656_audit_entry.c205_part_number_id%TYPE,
        p_tag_status IN t2656_audit_entry.c901_tag_status%TYPE,
        p_out_rpt OUT TYPES.cursor_type);
        
     /********************************************************************************
    * Description : Procedure to fetch associated reports
    *********************************************************************************/          
         PROCEDURE gm_fch_associated_rpt(
   		p_audit_id IN t2651_field_sales_details.c2650_field_sales_audit_id%TYPE,
        p_dist_id  IN t2651_field_sales_details.c701_distributor_id%TYPE,
        p_set_id   IN t2656_audit_entry.c207_set_id%TYPE,
        p_pnum   IN t2656_audit_entry.c205_part_number_id%TYPE,
        p_code_grp     IN t901_code_lookup.c901_code_grp%TYPE,
        p_out_rpt OUT TYPES.cursor_type);
      
   /********************************************************************************
    * Description : Function used to fetch the Set cost details
    *********************************************************************************/            
    FUNCTION get_fa_set_cost (
        p_set_id   IN t2656_audit_entry.c207_set_id%TYPE,
        p_attribute_type IN t207c_set_attribute.c901_attribute_type%TYPE)
    RETURN VARCHAR2;
   
   /********************************************************************************
    * Description : Procedure used to save the flag variance details
    *********************************************************************************/            
         
    PROCEDURE gm_sav_flag_variance (
        p_audit_id      IN t2651_field_sales_details.c2650_field_sales_audit_id%TYPE,
        p_dist_id       IN t2651_field_sales_details.c701_distributor_id%TYPE,
        p_set_id        IN t2657_field_sales_details_lock.c207_set_id%TYPE,
        p_inputs        IN VARCHAR2,
        p_user_id       IN t2656_audit_entry.c2656_last_updated_by%TYPE);

   /********************************************************************************
    * Description : Procedure used to save the Retag (t5010_tag)
    *********************************************************************************/         
    PROCEDURE gm_sav_retag (
        p_audit_entry_id IN t2656_audit_entry.c2656_audit_entry_id%TYPE,
        p_user_id        IN t2656_audit_entry.c2656_last_updated_by%TYPE);    
    /********************************************************************************
	* Description : Procedure used to adjust the locked Set id(t2657)
	*********************************************************************************/
	PROCEDURE gm_sav_adjust_locked_set(
		p_audit_entry_id IN t2656_audit_entry.c2656_audit_entry_id%TYPE,
        p_user_id        IN t2656_audit_entry.c2656_last_updated_by%TYPE);
END	gm_pkg_ac_fa_flag_deviation;
/