/* Formatted on 2010/10/22 15:42 (Formatter Plus v4.8.0) */
--@"C:\Database\Packages\Accounting\gm_pkg_ac_patag_info.pkg";
CREATE OR REPLACE
PACKAGE gm_pkg_ac_patag_info
IS
    --
PROCEDURE gm_fch_lock_tag_info (
        p_physical_audit_id IN t2603_audit_location.c2600_physical_audit_id%TYPE,
        p_txnid             IN t2605_audit_tag_detail.c2605_txn_id%TYPE,
        p_location_id       IN t2603_audit_location.c901_location_id%TYPE,
        p_proj_id           IN t205_part_number.c202_project_id%TYPE,
        p_byPart_flag       IN CHAR,
        p_out_cur OUT TYPES.cursor_type,
        p_ma_status OUT t2603_audit_location.c2603_ma_status_fl%TYPE) ;
    --
    /*******************************************************
    * Purpose: Procedure is used to insert part#/transaction ID
    to my_temp_list table
    *******************************************************/
    --
PROCEDURE gm_sav_filter_insert (
        p_physical_audit_id IN t2603_audit_location.c2600_physical_audit_id%TYPE,
        p_txnid             IN t2605_audit_tag_detail.c2605_txn_id%TYPE,
        p_proj_id           IN t205_part_number.c202_project_id%TYPE,
        p_location_id       IN t2603_audit_location.c901_location_id%TYPE,
        p_location_type     IN t2603_audit_location.c901_type%TYPE,
        p_inv_lock_id       IN t2600_physical_audit.c250_inventory_lock_id%TYPE,
        p_byPart_flag       IN CHAR) ;
    /*******************************************************
    * Purpose: Procedure is used to fetch Inventory Lock and
    Tag info by Part
    *******************************************************/
    --
PROCEDURE gm_fch_lock_tag_by_partref (
        p_physical_audit_id IN t2603_audit_location.c2600_physical_audit_id%TYPE,
        p_location_id       IN t2603_audit_location.c901_location_id%TYPE,
        p_inv_lock_id       IN t2600_physical_audit.c250_inventory_lock_id%TYPE,
        p_cogs_lock_id      IN t2600_physical_audit.c256_cogs_lock_id%TYPE,
        p_byPart_flag       IN CHAR,
        p_out_cur OUT TYPES.cursor_type) ;
    /*******************************************************
    * Purpose: Procedure is used to fetch Inventory Lock and
    Tag info by Part
    *******************************************************/
    --
PROCEDURE gm_fch_lock_tag_by_part (
        p_physical_audit_id IN t2603_audit_location.c2600_physical_audit_id%TYPE,
        p_location_id       IN t2603_audit_location.c901_location_id%TYPE,
        p_inv_lock_id       IN t2600_physical_audit.c250_inventory_lock_id%TYPE,
        p_cogs_lock_id      IN t2600_physical_audit.c256_cogs_lock_id%TYPE,
        p_out_cur OUT TYPES.cursor_type) ;
    /*******************************************************
    * Purpose: Procedure is used to fetch Inventory Lock and
    Tag info by Txn ID
    *******************************************************/
    --
PROCEDURE gm_fch_lock_tag_by_txn (
        p_physical_audit_id IN t2603_audit_location.c2600_physical_audit_id%TYPE,
        p_location_id       IN t2603_audit_location.c901_location_id%TYPE,
        p_inv_lock_id       IN t2600_physical_audit.c250_inventory_lock_id%TYPE,
        p_cogs_lock_id      IN t2600_physical_audit.c256_cogs_lock_id%TYPE,
        p_out_cur OUT TYPES.cursor_type) ;
    /***************************************************
    create this procedure for the Inventory tag
    It will be the part of the gm_pkg_ac_patag_info
    -- p_missing_flag should be 'Y' or 'N'
    ******************************************************/
    --
PROCEDURE gm_fch_tags (
        p_physical_audit_id  IN t2605_audit_tag_detail.c2600_physical_audit_id%TYPE,
        p_location_id        IN t2603_audit_location.c901_location_id%TYPE,
        p_part_num           IN t2605_audit_tag_detail.c205_part_number_id%TYPE,
        p_counted_by         IN t2605_audit_tag_detail.c101_counted_by%TYPE,
        p_missing_flag       IN VARCHAR2,
        p_tag_filter_token   IN VARCHAR2,
        p_from_tag_id        IN NUMBER,
        p_to_tag_id          IN NUMBER,
        p_show_void_rec_flag IN VARCHAR2,
        p_out_cur OUT TYPES.cursor_type) ;
    --
    /*******************************************************
    * Description : Procedure to get the tag details
    *     for the given tag id
    * Author  : VPrasath
    *******************************************************/
    --
PROCEDURE gm_fch_tag_details (
        p_tag_id IN t2605_audit_tag_detail.c2605_audit_tag_id%TYPE,
        p_out_cur OUT TYPES.cursor_type) ;
    --
    /*******************************************************
    * Description : Procedure to get the part details
    *     for the given part number
    * Author  : VPrasath
    *******************************************************/
    --
PROCEDURE gm_fch_desc (
        p_pnum   IN t2605_audit_tag_detail.c205_part_number_id%TYPE,
        p_txn_id IN t2605_audit_tag_detail.c2605_txn_id%TYPE,
        p_flag   IN VARCHAR2,
        p_out_cur OUT TYPES.cursor_type) ;
    --
    /*******************************************************
    * Description : Procedure to get the tag details for the
    *     given tag id's
    * Author  : VPrasath
    *******************************************************/
PROCEDURE gm_fch_generated_tags (
        p_tag_ids IN VARCHAR2,
        p_out_cur OUT TYPES.cursor_type) ;
    --
    /*******************************************************
    * Description : Procedure to get the locked transaction
    *     details
    * Author  : VPrasath
    *******************************************************/
PROCEDURE gm_fch_locked_txn (
        p_physical_audit_id IN t2605_audit_tag_detail.c2600_physical_audit_id%TYPE,
        p_location_id       IN t2603_audit_location.c901_location_id%TYPE,
        p_txn_id            IN VARCHAR2,
        p_project_id        IN t205_part_number.c202_project_id%TYPE,
        p_str               IN VARCHAR2,
        p_untagtxnfl        IN VARCHAR2,
        p_out_cur OUT TYPES.cursor_type) ;
    --
    FUNCTION get_fch_lock_set_detail (
            p_txn_id       IN t2605_audit_tag_detail.c2605_txn_id%TYPE,
            p_inventory_id IN t2600_physical_audit.c250_inventory_lock_id%TYPE)
        RETURN VARCHAR2;
        --
    FUNCTION get_fch_set_name (
            p_txn_id IN t2605_audit_tag_detail.c2605_txn_id%TYPE)
        RETURN VARCHAR2;
        /*******************************************************
        * Description : Procedure to get validate excel data
        *     details
        * Author  : Kulanthaivelu
        *******************************************************/
    FUNCTION get_validate_excel_data (
            p_physical_audit_id IN t2605_audit_tag_detail.c2600_physical_audit_id%TYPE,
            p_pnum              IN t205_part_number.c205_part_number_id%TYPE,
            p_loc               IN t2603_audit_location.c901_location_id%TYPE,
            p_consignee         IN t2601_audit_user.c101_user_id%TYPE,
            p_inv_location      IN t253a_consignment_lock.c504_consignment_id%TYPE)
        RETURN VARCHAR2;
    PROCEDURE gm_void_outstanding_tag (
            p_inputs       IN VARCHAR2,
            p_comments     IN t907_cancel_log.c907_comments%TYPE,
            p_cancelreason IN t907_cancel_log.c901_cancel_cd%TYPE,
            p_canceltype   IN t901_code_lookup.c902_code_nm_alt%TYPE,
            p_userid       IN t301_vendor.c301_last_updated_by%TYPE,
            p_out_msg OUT VARCHAR2) ;
    END gm_pkg_ac_patag_info;
    /
