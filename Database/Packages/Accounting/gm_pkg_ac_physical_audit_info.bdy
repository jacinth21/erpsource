/* Formatted on 2010/11/16 11:54 (Formatter Plus v4.8.0) */
--@"c:\database\packages\accounting\gm_pkg_ac_physical_audit_info.bdy";
CREATE OR REPLACE
PACKAGE BODY gm_pkg_ac_physical_audit_info
IS
    --
PROCEDURE gm_fch_inventory_rpt (
        p_physical_audit_id IN t2603_audit_location.c2600_physical_audit_id%TYPE,
        p_location_id       IN t2603_audit_location.c901_location_id%TYPE,
        p_project_id        IN t205_part_number.c202_project_id%TYPE,
        p_out_cur OUT TYPES.cursor_type)
AS
    v_inv_lock_id t2600_physical_audit.c250_inventory_lock_id%TYPE;
    v_cogs_lock_id t2600_physical_audit.c256_cogs_lock_id%TYPE;
    v_company_id  t1900_company.c1900_company_id%TYPE;
    v_plant_id     t5040_plant_master.C5040_PLANT_ID %TYPE;
BEGIN
        select get_compid_frm_cntx(),get_plantid_frm_cntx() 
		into v_company_id,v_plant_id 
		from dual;

     SELECT c250_inventory_lock_id, c256_cogs_lock_id
       INTO v_inv_lock_id, v_cogs_lock_id
       FROM t2600_physical_audit
      WHERE c2600_physical_audit_id = p_physical_audit_id
        AND c1900_company_id  =  v_company_id 
        AND c5040_plant_id    = v_plant_id
        AND c2600_void_fl          IS NULL;
    OPEN p_out_cur FOR SELECT * FROM
    (
         SELECT t205.c205_part_number_id pnum, get_code_name (t251.c901_type) warehouse, t2605.c2605_txn_id ref_id
          , t205.c205_part_num_desc pdesc, NVL (t251.c251_qty, 0) lock_qty, NVL (t2605.c2605_qty, 0) adjustment_qty
          , NVL (cogs.c257_cogs_amount, 0) cogs_amt, NVL (t251.c251_qty, 0) * NVL (cogs.c257_cogs_amount, 0) lock_cosg,
            NVL (t2605.c2605_qty, 0)                                        * NVL (cogs.c257_cogs_amount, 0)
            adjustment_cosg, (NVL (t2605.c2605_qty, 0)                      + NVL (t251.c251_qty, 0)) * NVL (
            cogs.c257_cogs_amount, 0) total_cosg
           FROM t205_part_number t205, (
                 SELECT t2605.c205_part_number_id, t2605.c2605_qty c2605_qty, t2605.c2605_txn_id
                   FROM t2605_audit_tag_detail t2605
                  WHERE t2605.c2600_physical_audit_id = p_physical_audit_id
                    AND t2605.c901_type               = 50676 -- hardcoded (Only Adjustment value)
                    AND t2605.c901_location_id        = DECODE (p_location_id, 0, t2605.c901_location_id, p_location_id
                    )
                    AND t2605.c2605_void_fl IS NULL
                    AND t2605.c2605_txn_id  IS NULL
                    AND t2605.c2605_qty     IS NOT NULL
            )
            t2605, (
                 SELECT t251.c205_part_number_id, SUM (t251.c251_qty) c251_qty, t251.c901_type
                   FROM t251_inventory_lock_detail t251, t2603_audit_location t2603
                  WHERE t251.c250_inventory_lock_id   = v_inv_lock_id
                    AND t251.c901_type                = t2603.c901_location_id
                    AND t251.c901_type                = DECODE (p_location_id, 0, t251.c901_type, p_location_id)
                    AND t2603.c2600_physical_audit_id = p_physical_audit_id
                    AND t2603.c901_location_id        = DECODE (p_location_id, 0, t2603.c901_location_id, p_location_id
                    )
                    AND t2603.c901_type IN ('50660')
               GROUP BY t251.c205_part_number_id, t251.c901_type
              UNION ALL
                 SELECT t253b.c205_part_number_id, SUM (t253b.c505_item_qty), t253a.c901_location_type
                   FROM t253a_consignment_lock t253a, t253b_item_consignment_lock t253b, t2603_audit_location t2603
                  WHERE t253a.c250_inventory_lock_id  = v_inv_lock_id
                    AND t253b.C250_INVENTORY_LOCK_ID  = v_inv_lock_id
                    AND t253a.c504_consignment_id     = t253b.c504_consignment_id
                    AND t253a.c901_location_type      = t2603.c901_location_id
		    AND t253b.C901_LOCATION_TYPE = t2603.c901_location_id
		    AND t253b.c901_location_type      = DECODE (p_location_id, 0, t2603.c901_location_id, p_location_id)
                    AND t253a.c901_location_type      = DECODE (p_location_id, 0, t2603.c901_location_id, p_location_id)
                    AND t2603.c2600_physical_audit_id = p_physical_audit_id
                    AND t2603.c901_location_id        = DECODE (p_location_id, 0, t2603.c901_location_id, p_location_id
                    )
                    AND t2603.c901_type IN ('50662', '50661')
               GROUP BY t253b.c205_part_number_id, t253a.c901_location_type
            )
            t251, (
                 SELECT t257.c205_part_number_id, t257.c257_cogs_amount
                   FROM t257_cogs_detail t257
                  WHERE t257.c256_cogs_lock_id = v_cogs_lock_id
                    AND t257.c901_type         = 20450 -- Always Finished Goods
            )
            cogs
          WHERE t205.c205_part_number_id = t2605.c205_part_number_id(+)
            AND t205.c205_part_number_id = t251.c205_part_number_id(+)
            AND t205.c205_part_number_id = cogs.c205_part_number_id(+)
            AND t205.c202_project_id     = DECODE (p_project_id, '0', t205.c202_project_id, p_project_id)
    )
    WHERE lock_qty <> 0 OR adjustment_qty <> 0 order by warehouse;
END gm_fch_inventory_rpt;
---
PROCEDURE gm_fch_inv_summary_rpt (
        p_physical_audit_id IN t2603_audit_location.c2600_physical_audit_id%TYPE,
        p_detail            IN CHAR,
        p_out_cur OUT TYPES.cursor_type)
AS
    v_inv_lock_id t2600_physical_audit.c250_inventory_lock_id%TYPE;
    v_cogs_lock_id t2600_physical_audit.c256_cogs_lock_id%TYPE;
    v_out_cur TYPES.cursor_type;
    v_company_id  t1900_company.c1900_company_id%TYPE; 
    v_plant_id    t5040_plant_master.C5040_PLANT_ID %TYPE;
BEGIN
        select get_compid_frm_cntx(),get_plantid_frm_cntx() 
		into v_company_id,v_plant_id 
		from dual;

     DELETE FROM MY_TEMP_KEY_VALUE;
     SELECT c250_inventory_lock_id, c256_cogs_lock_id
       INTO v_inv_lock_id, v_cogs_lock_id
       FROM t2600_physical_audit
      WHERE c2600_physical_audit_id = p_physical_audit_id
        AND c1900_company_id  =  v_company_id
        AND c5040_plant_id    = v_plant_id
        AND c2600_void_fl          IS NULL;
    FOR v_out_cur                  IN
    (
         SELECT c901_location_id
           FROM t2603_audit_location
          WHERE c2600_physical_audit_id = p_physical_audit_id
            AND c901_type              IN ('50660')
    )
    LOOP
         INSERT INTO MY_TEMP_KEY_VALUE
            (MY_TEMP_TXN_KEY, MY_TEMP_TXN_VALUE
            )
         SELECT t2605.c205_part_number_id, TO_CHAR (v_out_cur.c901_location_id)
           FROM t2605_audit_tag_detail t2605, t205_part_number t205
          WHERE t2605.c2600_physical_audit_id  = p_physical_audit_id
            AND t2605.c205_part_number_id      = t205.c205_part_number_id
            AND t2605.c2605_qty                > 0
            AND t2605.c205_part_number_id NOT IN
            (
                 SELECT t251.c205_part_number_id
                   FROM t251_inventory_lock_detail t251, t205_part_number t205
                  WHERE t251.c250_inventory_lock_id = v_inv_lock_id
                    AND t251.c205_part_number_id    = t205.c205_part_number_id
                    AND t251.c901_type              = v_out_cur.c901_location_id
                    AND t251.c251_qty               > 0
            ) ;
    END LOOP;
    OPEN p_out_cur FOR SELECT mylist.c901_type,
    get_code_name (mylist.c901_type) warehouse,
    get_code_name_alt (mylist.c901_type) warehouse_id,
    DECODE (p_detail, 'Y', mylist.c205_part_number_id, '-9999') ref_id,
    SUM (NVL (beforecount.c251_qty, 0) * NVL (cogs.c257_cogs_amount, 0)) system_amount,
    SUM (NVL (beforecount.c251_qty, 0)) system_qty,
    SUM (NVL (aftercount.c2605_qty, 0)) count_qty,
    SUM (NVL (aftercount.c2605_qty, 0)                                          - NVL (beforecount.c251_qty, 0)) variance_qty,
    DECODE (p_detail, 'Y', '-9999', ROUND ( (SUM (NVL (aftercount.c2605_qty, 0) - NVL (beforecount.c251_qty, 0)) / SUM
    (NVL (beforecount.c251_qty, 0)))                                            * 100, 2)) variance_qty_per,
    ROUND (SUM (ABS (NVL (beforecount.c251_qty, 0)                              - NVL (aftercount.c2605_qty, 0))), 0)
    abs_qty,
    DECODE (p_detail, 'Y', '-9999', ROUND (SUM (ABS (NVL (beforecount.c251_qty, 0) - NVL (aftercount.c2605_qty, 0))) /
    SUM (NVL (beforecount.c251_qty, 0))                                            * 100, 2)) abs_qty_per,
    ROUND (SUM ( (NVL (aftercount.c2605_qty, 0)                                    - NVL (beforecount.c251_qty, 0)) *
    NVL (cogs.c257_cogs_amount, 0)), 2) variance_amount,
    DECODE (p_detail, 'Y', '-9999', ROUND ( (SUM ( (NVL (aftercount.c2605_qty, 0) - NVL (beforecount.c251_qty, 0)) *
    NVL (cogs.c257_cogs_amount, 0))                                               / SUM (NVL (beforecount.c251_qty, 0)
                                                                                  * NVL (cogs.c257_cogs_amount, 0))) *
    100, 2)) variance_amount_per,
    ROUND (SUM (ABS (NVL (beforecount.c251_qty, 0) - NVL (aftercount.c2605_qty, 0)) * NVL (cogs.c257_cogs_amount, 0)),
    2) abs_amount,
    DECODE (p_detail, 'Y', '-9999', ROUND (SUM (ABS (NVL (beforecount.c251_qty, 0) - NVL (aftercount.c2605_qty, 0)) *
    NVL (cogs.c257_cogs_amount, 0))                                                / SUM (NVL (beforecount.c251_qty, 0)
                                                                                   * NVL (cogs.c257_cogs_amount, 0)) *
    100, 2)) abs_amount_per FROM
    (
         SELECT t251.c205_part_number_id, t251.c901_type
           FROM t251_inventory_lock_detail t251, t205_part_number t205, t2603_audit_location t2603
          WHERE t251.c250_inventory_lock_id   = v_inv_lock_id
            AND t251.c205_part_number_id      = t205.c205_part_number_id
            AND t251.c901_type                = t2603.c901_location_id
            AND t2603.c2600_physical_audit_id = p_physical_audit_id
            AND t2603.c901_type              IN ('50660')
            AND t251.c251_qty                 > 0
          UNION
         SELECT MY_TEMP_TXN_KEY, to_number (MY_TEMP_TXN_VALUE) FROM MY_TEMP_KEY_VALUE
          UNION
         SELECT t253b.c205_part_number_id, t253a.c901_location_type
           FROM t253a_consignment_lock t253a, t253b_item_consignment_lock t253b, t2603_audit_location t2603
          WHERE t253a.c250_inventory_lock_id  = v_inv_lock_id
            AND t253b.C250_INVENTORY_LOCK_ID  = v_inv_lock_id--add new
            AND t253a.c504_consignment_id     = t253b.c504_consignment_id
            AND t253a.c901_location_type      = t2603.c901_location_id
	    AND t253b.C901_LOCATION_TYPE      = t2603.c901_location_id--add vew
            AND t2603.c2600_physical_audit_id = p_physical_audit_id
            AND t2603.c901_type              IN ('50662')
    )
    mylist, (
         SELECT t251.c205_part_number_id, SUM (t251.c251_qty) c251_qty, t251.c901_type
           FROM t251_inventory_lock_detail t251, t2603_audit_location t2603
          WHERE t251.c250_inventory_lock_id   = v_inv_lock_id
            AND t251.c901_type                = t2603.c901_location_id
            AND t2603.c2600_physical_audit_id = p_physical_audit_id
            AND t2603.c901_type              IN ('50660')
       GROUP BY t251.c205_part_number_id, t251.c901_type
          UNION
         SELECT t253b.c205_part_number_id, SUM (t253b.c505_item_qty), t253a.c901_location_type
           FROM t253a_consignment_lock t253a, t253b_item_consignment_lock t253b, t2603_audit_location t2603
          WHERE t253a.c250_inventory_lock_id  = v_inv_lock_id
            AND t253b.C250_INVENTORY_LOCK_ID  = v_inv_lock_id --new add
            AND t253a.c504_consignment_id     = t253b.c504_consignment_id
            AND t253a.c901_location_type      = t2603.c901_location_id
	    AND t253b.c901_location_type      = t2603.c901_location_id--new add
            AND t2603.c2600_physical_audit_id = p_physical_audit_id
            AND t2603.c901_type              IN ('50662')
       GROUP BY t253b.c205_part_number_id, t253a.c901_location_type
    )
    beforecount, (
         SELECT t2605.c205_part_number_id, SUM (t2605.c2605_qty) c2605_qty, t2605.c901_location_id
           FROM t2605_audit_tag_detail t2605
          WHERE t2605.c2600_physical_audit_id = p_physical_audit_id
            -- AND t2605.c901_location_id        = 20465
            AND t2605.c901_type      = 50675 -- regular
            AND t2605.c2605_void_fl IS NULL
       GROUP BY t2605.c205_part_number_id, t2605.c901_location_id
    )
    aftercount, (
         SELECT t257.c205_part_number_id, t257.c257_cogs_amount
           FROM t257_cogs_detail t257
          WHERE t257.c256_cogs_lock_id = v_cogs_lock_id
            AND t257.c901_type         = 20450 -- Always Finished Goods
    )
    cogs WHERE mylist.c205_part_number_id                            = beforecount.c205_part_number_id(+) AND mylist.c205_part_number_id =
    aftercount.c205_part_number_id(+) AND mylist.c205_part_number_id = cogs.c205_part_number_id(+) AND mylist.c901_type
                                                                     = beforecount.c901_type(+) AND mylist.c901_type =
    aftercount.c901_location_id(+) AND mylist.c901_type             IN
    (
         SELECT c901_location_id
           FROM t2603_audit_location
          WHERE c2600_physical_audit_id = p_physical_audit_id
            AND c901_type              IN ('50660', '50662')
    )
    AND (beforecount.c251_qty <> 0 OR aftercount.c2605_qty <> 0) group by mylist.c901_type,
    DECODE (p_detail, 'Y', mylist.c205_part_number_id, '-9999')
      UNION
     SELECT t253a.c901_location_type, get_code_name (t253a.c901_location_type) warehouse, get_code_name_alt (
        t253a.c901_location_type) warehouse_id, DECODE (p_detail, 'Y', t253a.c504_consignment_id, '-9999') ref_id,
        ROUND (SUM (NVL (t253a.before_count, 0) * NVL (cogs.cogs_amount, 0)), 2) system_amount, SUM (NVL (
        t253a.before_count, 0)) system_qty, SUM (NVL (t2605.c2605_qty, 0)) count_qty, SUM (NVL (t2605.c2605_qty, 0)
                                                                                                          - NVL (t253a.before_count, 0)) variance_qty, DECODE (p_detail, 'Y', '-9999', ROUND ( (SUM (NVL (t2605.c2605_qty
        , 0)                                                                                              - NVL (t253a.before_count, 0)) / SUM (NVL (t253a.before_count, 0))) * 100, 2)) variance_qty_per, SUM (ABS (
        NVL (t253a.before_count, 0)                                                                       - NVL (t2605.c2605_qty, 0))) abs_qty, DECODE (p_detail, 'Y', '-9999', ROUND ( (SUM
        (ABS (NVL (t253a.before_count, 0)                                                                 - NVL (t2605.c2605_qty, 0))) / SUM (NVL (t253a.before_count, 0))) * 100, 2))
        abs_qty_per, ROUND (SUM ( (NVL (t2605.c2605_qty, 0)                                            - NVL (t253a.before_count, 0)) * NVL (cogs.cogs_amount, 0))
        , 2) variance_amount, DECODE (p_detail, 'Y', '-9999', ROUND ( (SUM ( (NVL (t2605.c2605_qty, 0) - NVL (
        t253a.before_count, 0))                                                                              * NVL (
        cogs.cogs_amount, 0))                                                                             / SUM (NVL (
        t253a.before_count, 0)                                                                            * NVL (
        cogs.cogs_amount, 0)))                                                                            * 100, 2))
        variance_amount_per, ROUND (SUM (ABS (NVL (t253a.before_count, 0)                                 - NVL (
        t2605.c2605_qty, 0))                                                                              * NVL (
        cogs.cogs_amount, 0)), 2) abs_amount, DECODE (p_detail, 'Y', '-9999', ROUND ( (SUM (ABS (NVL (
        t253a.before_count, 0) - NVL (t2605.c2605_qty, 0)) * NVL (cogs.cogs_amount, 0)) / SUM (NVL (t253a.before_count,
        0)                     * NVL (cogs.cogs_amount, 0))) * 100, 2)) abs_amount_per
       FROM
        (
             SELECT t253a.c504_consignment_id, SUM (1) before_count, t253a.c901_location_type
               FROM t253a_consignment_lock t253a
              WHERE t253a.c250_inventory_lock_id = v_inv_lock_id
                AND t253a.c504_void_fl          IS NULL
           GROUP BY c504_consignment_id, t253a.c901_location_type
        )
        t253a, (
             SELECT t2605.C2605_TXN_ID, SUM (t2605.c2605_qty) c2605_qty, t2605.c901_location_id
               FROM t2605_audit_tag_detail t2605
              WHERE t2605.c2600_physical_audit_id = p_physical_audit_id
                AND t2605.c901_type               = 50675 -- hardcoded       (Only Regular value)
                --AND t2605.c901_location_id = 20450
                AND t2605.c2605_void_fl       IS NULL
                AND t2605.c2605_qty           IS NOT NULL
                AND t2605.C2605_TXN_ID        IS NOT NULL
                AND t2605.c205_part_number_id IS NULL
           GROUP BY t2605.C2605_TXN_ID, t2605.c901_location_id
        )
        t2605, (
             SELECT t253b.c504_consignment_id, SUM (t253b.c505_item_qty * NVL (t257.c257_cogs_amount, 0)) cogs_amount
               FROM t257_cogs_detail t257, t253b_item_consignment_lock t253b
              WHERE t253b.c250_inventory_lock_id = v_inv_lock_id
                AND t257.c256_cogs_lock_id       = v_cogs_lock_id
                AND t257.c901_type               = 20450 -- Always Finished Goods
                AND t253b.c205_part_number_id    = t257.c205_part_number_id
           GROUP BY t253b.c504_consignment_id
        )
        cogs
      WHERE t2605.c901_location_id(+) = t253a.c901_location_type
        AND t2605.C2605_TXN_ID(+)     = t253a.c504_consignment_id
        AND t253a.c504_consignment_id = cogs.c504_consignment_id(+)
        AND t253a.c901_location_type IN
        (
             SELECT c901_location_id
               FROM t2603_audit_location
              WHERE c2600_physical_audit_id = p_physical_audit_id
                AND c901_type              IN ('50661')
        )
   GROUP BY t253a.c901_location_type, DECODE (p_detail, 'Y', t253a.c504_consignment_id, '-9999')
   ORDER BY warehouse;
END gm_fch_inv_summary_rpt;
---
PROCEDURE gm_fch_flagged_part (
        p_physical_audit_id IN t2603_audit_location.c2600_physical_audit_id%TYPE,
        p_location_id       IN t2603_audit_location.c901_location_id%TYPE,
        p_ma_status OUT t2603_audit_location.c2603_ma_status_fl%TYPE,
        p_ma_ids OUT TYPES.cursor_type,
        p_out_cur OUT TYPES.cursor_type)
AS
    v_audit_location_id t2603_audit_location.c2603_audit_location_id%TYPE;
    v_cogs_lock_id t2600_physical_audit.c256_cogs_lock_id%TYPE;
    v_ma_status_fl t2603_audit_location.c2603_ma_status_fl%TYPE;
    v_ma_id TYPES.cursor_type;
    v_company_id  t1900_company.c1900_company_id%TYPE;
    v_plant_id    T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
BEGIN
 	    select get_compid_frm_cntx(),get_plantid_frm_cntx() 
		into v_company_id,v_plant_id 
		from dual;
     SELECT c256_cogs_lock_id
       INTO v_cogs_lock_id
       FROM t2600_physical_audit
      WHERE c2600_physical_audit_id = p_physical_audit_id
        AND c1900_company_id = v_company_id
        AND c5040_plant_id    = v_plant_id
        AND c2600_void_fl          IS NULL;
     SELECT c2603_audit_location_id, c2603_ma_status_fl
       INTO v_audit_location_id, p_ma_status
       FROM t2603_audit_location t2603
      WHERE c2600_physical_audit_id = p_physical_audit_id
        AND t2603.c901_location_id  = p_location_id;
    gm_pkg_ac_ma.gm_fch_ma_ref_detail (v_audit_location_id, 50680, p_ma_ids) ; --ref type 50680 , physical audit
    OPEN p_out_cur FOR SELECT t2605.c205_part_number_id pnum,
    t205.c205_part_num_desc pdesc,
    t2605.c2605_qty qty,
    NVL (t257.c257_cogs_amount, 0) price,
    t2605.c2605_qty * NVL (t257.c257_cogs_amount, 0) extend_cost FROM t2605_audit_tag_detail t2605,
    t205_part_number t205,t2023_part_company_mapping t2023 ,
    t257_cogs_detail t257 WHERE t2605.c2600_physical_audit_id = p_physical_audit_id
     AND t2605.c5040_plant_id    = v_plant_id
     AND t2605.c901_type = 50676 --
    -- adjustment
    AND t2605.c205_part_number_id     = t205.c205_part_number_id AND t2605.c901_location_id = p_location_id AND
    t257.c256_cogs_lock_id (+)        = v_cogs_lock_id AND t2605.c205_part_number_id IS NOT NULL AND t2605.c2605_qty IS NOT
    NULL AND t205.c205_part_number_id = t257.c205_part_number_id(+) AND t2605.c2605_void_fl IS NULL 
    AND t205.c205_part_number_id = t2023.c205_part_number_id 
	AND t2023.c2023_void_fl IS NULL
	AND t2023.c1900_company_id = v_company_id
    AND t2605.c2605_txn_id               IS NULL ; -- necessary for part & trans , if not added will double the qty to
    -- adjust
END gm_fch_flagged_part;
/*******************************************************
* Description : Procedure to Create MA during the
*     Physical Audit
* Author : Xun
*******************************************************/
PROCEDURE gm_save_ma (
        p_physical_audit_id IN t2605_audit_tag_detail.c2605_audit_tag_id%TYPE,
        p_location_id       IN t2605_audit_tag_detail.c901_location_id%TYPE,
        p_user_id           IN t2605_audit_tag_detail.c2605_last_updated_by%TYPE)
AS
    v_company_id    t1900_company.c1900_company_id%TYPE;
    v_plant_id    t5040_plant_master.C5040_PLANT_ID %TYPE;
	-- International posting changes.
    v_costing_id t820_costing.c820_costing_id%TYPE;
    v_owner_company_id t1900_company.c1900_company_id%TYPE;
    v_local_cost t820_costing.c820_purchase_amt%TYPE;
    v_cost t820_costing.c820_purchase_amt%TYPE;
    --
    
    CURSOR plus_cursor ---positive qty
    IS
         SELECT t2605.c205_part_number_id pnum, t2605.c2605_qty qty, NVL (DECODE (p_location_id, 20466, DECODE (NVL (
            get_ac_costing_id (t2605.c205_part_number_id, 4909), 0), 0, get_ac_costing_id (t2605.c205_part_number_id,
            4900), get_ac_costing_id (t2605.c205_part_number_id, 4909)), get_ac_costing_id (t2605.c205_part_number_id,
            4900)), 0) cost_id --4900 FG, 4909- MFG 20466 -RAW MATERIAL
           FROM t2605_audit_tag_detail t2605
          WHERE t2605.c2600_physical_audit_id = p_physical_audit_id
            AND t2605.c2605_void_fl          IS NULL
            AND t2605.c205_part_number_id    IS NOT NULL
            AND t2605.c901_location_id        = p_location_id
            AND t2605.c901_type               = 50676 -- adjustment
            AND NVL (t2605.c2605_qty, 0)      > 0
			AND  t2605.c5040_plant_id    = v_plant_id
            AND t2605.c2605_txn_id           IS NULL; -- necessary for part & trans , if not added  will double the qty
    -- to adjust
    --
    CURSOR minus_cursor ---negative qty
    IS
         SELECT t2605.c205_part_number_id pnum, (t2605.c2605_qty) * - 1 qty, NVL (DECODE (p_location_id, 20466, DECODE
            (NVL (get_ac_costing_id (t2605.c205_part_number_id, 4909), 0), 0, get_ac_costing_id (
            t2605.c205_part_number_id, 4900), get_ac_costing_id (t2605.c205_part_number_id, 4909)), get_ac_costing_id (
            t2605.c205_part_number_id, 4900)), 0) cost_id --4900 FG, 4909- MFG 20466 -RAW MATERIAL
           FROM t2605_audit_tag_detail t2605
          WHERE t2605.c2600_physical_audit_id = p_physical_audit_id
            AND t2605.c2605_void_fl          IS NULL
            AND t2605.c205_part_number_id    IS NOT NULL
            AND t2605.c901_location_id        = p_location_id
            AND t2605.c901_type               = 50676 -- adjustment
            AND NVL (t2605.c2605_qty, 0)      < 0
			AND  t2605.c5040_plant_id    = v_plant_id
            AND t2605.c2605_txn_id           IS NULL; -- necessary for part & trans , if not added will double the qty
    -- to adjust
    -- Select the consignments/DHRs for the built set/Manf DHR/Manf WIP resp
    CURSOR cons_cur
    IS
         SELECT t2605.c2605_txn_id cons_id
           FROM t2605_audit_tag_detail t2605
          WHERE c2600_physical_audit_id    = p_physical_audit_id
            AND t2605.c901_location_id     = p_location_id
            AND t2605.c901_type            = 50676 -- adjustment
            AND t2605.c2605_txn_id        IS NOT NULL
            AND t2605.c2605_void_fl       IS NULL
            AND t2605.c2605_qty            < 0
			AND t2605.c5040_plant_id    = v_plant_id
            AND t2605.c205_part_number_id IS NULL; -- necessary for part & trans , if not added will double the qty to
    -- adjust
    --
    v_audit_location_id t2603_audit_location.c2603_audit_location_id%TYPE;
    v_ma_status_fl t2603_audit_location.c2603_ma_status_fl%TYPE;
    v_matype_plus t901_code_lookup.c901_code_id%TYPE;
    v_matype_minus t901_code_lookup.c901_code_id%TYPE;
    v_ma_updateinv t215_part_qty_adjustment.c215_update_inv_fl%TYPE;
    v_ma_id_plus t215_part_qty_adjustment.c215_part_qty_adj_id%TYPE;
    v_ma_id_minus t215_part_qty_adjustment.c215_part_qty_adj_id%TYPE;
    v_check_count      NUMBER;
    v_tags_to_void     NUMBER;
    v_user_exists      NUMBER;
    v_authorized_users VARCHAR2 (200) ;
    v_tags_to_verify   NUMBER;
    v_warehouse_type t901_code_lookup.c901_code_id%TYPE;
    v_pos_comment VARCHAR2 (2000):= '+ve Adjustment performed as Part of Physical audit after adjusting all tags';
    v_neg_comment VARCHAR2 (2000):= '-ve Adjustment performed as Part of Physical audit after adjusting all tags';

    --
BEGIN
	SELECT NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL')),get_plantid_frm_cntx() INTO v_company_id,v_plant_id FROM dual; 
    --
    /*  v_authorized_users := get_rule_value ('SAVEMAFROMPA', 'ACCESSPERMISSION') ;
    SELECT INSTR (v_authorized_users, p_user_id, 1, 1)
    INTO v_user_exists
    FROM DUAL;
    IF v_user_exists = 0 THEN
    raise_application_error ('-20113', '') ;
    END IF;*/
     SELECT c2603_audit_location_id, c2603_ma_status_fl
       INTO v_audit_location_id, v_ma_status_fl
       FROM t2603_audit_location t2603
      WHERE c2600_physical_audit_id = p_physical_audit_id
        AND t2603.c901_location_id  = p_location_id FOR UPDATE;
    IF (v_ma_status_fl              = 'Y') THEN
        raise_application_error ('-20868', '') ;
    END IF;
     SELECT COUNT (1) tag_count
       INTO v_tags_to_verify
       FROM t2605_audit_tag_detail t2605b
      WHERE t2605b.c901_location_id = p_location_id
        AND t2605b.c901_type        = 50675 -- regular
	    AND t2605b.c5040_plant_id    = v_plant_id
        AND t2605b.c2605_void_fl   IS NULL
        AND t2605b.c2605_qty       IS NULL
	AND t2605b.C2600_PHYSICAL_AUDIT_ID = p_physical_audit_id ;
    IF (v_tags_to_verify            > 0) THEN
        raise_application_error ('-20869', '') ;
    END IF;
    v_ma_updateinv := get_rule_value (p_location_id, 'UPDATEINVTYPE') ;
    /***************************************************************
    Below code to include +ive value
    **************************************************************/
    v_matype_plus     := get_rule_value (p_location_id, 'PAUDITPLUS') ;
    IF (v_matype_plus IS NULL) THEN
        raise_application_error ('-20867', '') ;
    END IF;
    -- Master Insert
     SELECT COUNT (1)
       INTO v_check_count
       FROM t2605_audit_tag_detail t2605
      WHERE t2605.c2600_physical_audit_id = p_physical_audit_id
        AND t2605.c2605_void_fl          IS NULL
        AND t2605.c205_part_number_id    IS NOT NULL
        AND t2605.c901_location_id        = p_location_id
        AND t2605.c901_type               = 50676 -- adjustment
        AND NVL (t2605.c2605_qty, 0)      > 0
        AND t2605.c2605_txn_id           IS NULL -- necessary for part & trans , if not added will double the qty to
        -- adjust
        AND ROWNUM     = 1;
    IF (v_check_count <> 0) THEN
        gm_pkg_ac_physical_audit_info.gm_ac_sav_ma_adj (v_matype_plus,v_pos_comment, v_ma_updateinv, p_user_id,v_ma_id_plus) ;
        --
        FOR plus_val IN plus_cursor
        LOOP
        	-- to get the costing id 
			v_costing_id := plus_val.cost_id;
			-- to get the owner information.
			BEGIN
				SELECT t820.c1900_owner_company_id
				  , t820.c820_purchase_amt
				  , t820.c820_local_company_cost
				   INTO v_owner_company_id
				  , v_cost
				  , v_local_cost
				   FROM t820_costing t820
				  WHERE t820.c820_costing_id =v_costing_id;
		   EXCEPTION WHEN NO_DATA_FOUND
		   THEN
		   		v_owner_company_id := NULL;
		   		v_cost:= NULL;
		   		v_local_cost:= NULL;
		   END;
            -- gm_procedure_log ('Inside loop', 'v_ma_id_plus');
            gm_pkg_ac_physical_audit_info.gm_ac_sav_adj_detail (v_ma_id_plus, plus_val.pnum, plus_val.qty,
            v_cost, v_owner_company_id, v_local_cost, p_user_id) ;
            --
        END LOOP;
        
		gm_pkg_ac_ma.gm_save_manual_ledger_posting (v_ma_id_plus);
        gm_pkg_ac_ma.gm_sav_manual_qty (v_ma_id_plus, p_location_id, p_user_id) ;
        gm_pkg_ac_ma.gm_sav_manual_ref_detail (v_ma_id_plus, v_audit_location_id, 50680) ; --ref type 50680 , physical
        -- audit;
    END IF;
    /***************************************************************
    Below code to include -ive value
    **************************************************************/
     SELECT COUNT (1)
       INTO v_check_count
       FROM t2605_audit_tag_detail t2605
      WHERE t2605.c2600_physical_audit_id = p_physical_audit_id
        AND t2605.c2605_void_fl          IS NULL
        AND t2605.c205_part_number_id    IS NOT NULL
        AND t2605.c901_location_id        = p_location_id
        AND t2605.c901_type               = 50676 -- adjustment
        AND NVL (t2605.c2605_qty, 0)      < 0
        AND t2605.c2605_txn_id           IS NULL -- necessary for part & trans , if not added will double the qty to
        -- adjust
        AND ROWNUM         = 1;
    IF (v_check_count     <> 0) THEN
        v_matype_minus    := get_rule_value (p_location_id, 'PAUDITMINUS') ;
        IF (v_matype_plus IS NULL) THEN
            raise_application_error ('-20867', '') ;
        END IF;
        -- gm_procedure_log ('Before -ive', 'PMINIS');
        gm_pkg_ac_physical_audit_info.gm_ac_sav_ma_adj (v_matype_minus,v_neg_comment, v_ma_updateinv, p_user_id,v_ma_id_minus) ;
        FOR minus_val IN minus_cursor
        LOOP
        	-- to get the costing id 
			v_costing_id := minus_val.cost_id;
			-- to get the owner information.
			BEGIN
				SELECT t820.c1900_owner_company_id
				  , t820.c820_purchase_amt
				  , t820.c820_local_company_cost
				   INTO v_owner_company_id
				  , v_cost
				  , v_local_cost
				   FROM t820_costing t820
				  WHERE t820.c820_costing_id =v_costing_id;
		   EXCEPTION WHEN NO_DATA_FOUND
		   THEN
		   		v_owner_company_id := NULL;
		   		v_cost:= NULL;
		   		v_local_cost:= NULL;
		   END;
            -- gm_procedure_log ('Inside loop', 'v_ma_id_minus');
            gm_pkg_ac_physical_audit_info.gm_ac_sav_adj_detail (v_ma_id_minus, minus_val.pnum, minus_val.qty,
            v_cost, v_owner_company_id, v_local_cost, p_user_id) ;
            --
        END LOOP;
  		
        gm_pkg_ac_ma.gm_save_manual_ledger_posting (v_ma_id_minus);
        gm_pkg_ac_ma.gm_sav_manual_qty (v_ma_id_minus, p_location_id, p_user_id) ;
        -- If bulk location call to clear all bulk values
        -- applicable for 8-WIP, 9-TBE, 10-Loose
        IF (p_location_id = 20467 OR p_location_id = 20468 OR p_location_id = 20469) THEN
            --gm_pkg_ac_ma.gm_sav_manual_qty_bulk (v_ma_id_plus, p_user_id);
            gm_pkg_ac_ma.gm_sav_manual_qty_bulk (v_ma_id_minus, p_user_id) ;
        END IF;
        -- Below code to void all the missed transaction
        IF (p_location_id >= 20470 AND p_location_id <= 20476) THEN
            gm_pkg_ac_ma.gm_sav_ma_dxn_void (p_physical_audit_id, p_location_id, p_user_id) ;
        END IF;
        -- For Built set/Wip void the consignment, Back order the assocaited request
        -- and void all the associated open transactions
        IF (p_location_id = 20462 OR p_location_id = 20467) THEN
            FOR cons     IN cons_cur
            LOOP
                gm_pkg_op_process_request.gm_sav_void_cons (cons.cons_id, p_user_id) ;
                 INSERT
                   INTO t907_cancel_log
                    (
                        c907_cancel_log_id, c907_ref_id, c907_comments
                      , c901_cancel_cd, c901_type, c907_created_by
                      , c907_created_date, c907_cancel_date, c1900_company_id
                    )
                    VALUES
                    (
                        s907_cancel_log.NEXTVAL, cons.cons_id, 'Consignment voided while creating the MA ' ||
                        v_ma_id_minus || ' during the Physical Audit', 90257, '90202'
                      , p_user_id, CURRENT_DATE, TRUNC (CURRENT_DATE), v_company_id
                    ) ;
            END LOOP;
        END IF;
        -- For MDHR/MWIP New Location Added For PA 2010
        IF (p_location_id = 20478 OR p_location_id = 20479) THEN
            FOR cons     IN cons_cur
            LOOP
                gm_pkg_op_dhr.gm_op_void_mfg_dhrs
                (
                    cons.cons_id, p_user_id
                )
                ;
                 INSERT
                   INTO t907_cancel_log
                    (
                        c907_cancel_log_id, c907_ref_id, c907_comments
                      , c901_cancel_cd, c901_type, c907_created_by
                      , c907_created_date, c907_cancel_date, c1900_company_id
                    )
                    VALUES
                    (
                        s907_cancel_log.NEXTVAL, cons.cons_id, 'Manfufacturing DHRs/WIP voided while creating the MA '
                        || v_ma_id_minus || ' during the Physical Audit', 90257, '90956'
                      , p_user_id, CURRENT_DATE, TRUNC (CURRENT_DATE), v_company_id
                    ) ;
            END LOOP;
        END IF;
	v_warehouse_type := get_rule_value (p_location_id, 'PI-LOCWAREHOUSE-MAP');
        -- Shelf, RW  Locations Adjustment on operation side
        IF (v_warehouse_type IS NOT NULL) THEN
            gm_sav_adj_inv_loc (v_ma_id_plus, v_ma_id_minus, p_physical_audit_id, p_location_id, p_user_id) ;
        END IF;
        gm_pkg_ac_ma.gm_sav_manual_ref_detail (v_ma_id_minus, v_audit_location_id, 50680) ; --ref type 50680 , physical
        -- audit;
    END IF;
    /* End of -ive Value posting
    ********************************************************************************/
     UPDATE t2603_audit_location
    SET c2603_ma_status_fl          = 'Y', c2603_last_updated_by = p_user_id, c2603_last_updated_date = CURRENT_DATE
      WHERE c2603_audit_location_id = v_audit_location_id
        AND c2603_void_fl          IS NULL;
END gm_save_ma;
--
PROCEDURE gm_ac_sav_ma_adj (
        p_matype    IN t901_code_lookup.c901_code_id%TYPE,
        p_desc      IN t215_part_qty_adjustment.c215_comments%TYPE,
        p_updoprqty IN t215_part_qty_adjustment.c215_update_inv_fl%TYPE,
        p_user_id   IN t2605_audit_tag_detail.c2605_last_updated_by%TYPE,
        p_maid OUT t215_part_qty_adjustment.c215_part_qty_adj_id%TYPE)
AS
    v_id t215_part_qty_adjustment.c215_part_qty_adj_id%TYPE;
    v_seq NUMBER;
    v_company_id  t1900_company.c1900_company_id%TYPE;
	v_plant_id    t5040_plant_master.C5040_PLANT_ID %TYPE;
BEGIN
        select get_compid_frm_cntx(),get_plantid_frm_cntx() 
		into v_company_id,v_plant_id 
		from dual;
	
    --
     SELECT s215_part_qty_adjustment.NEXTVAL INTO v_seq FROM DUAL;
    v_id   := 'PIIA-' || v_seq;
    p_maid := v_id;
    --
     INSERT
       INTO t215_part_qty_adjustment
        (
            c215_part_qty_adj_id, c215_adj_date, c901_adjustment_source
          , c215_comments, c215_validated_by, c215_approved_by
          , c215_created_by, c215_created_date, c215_update_inv_fl
          , c1900_company_id
        )
        VALUES
        (
            v_id, CURRENT_DATE, p_matype
          , p_desc, p_user_id, p_user_id
          , p_user_id, CURRENT_DATE, p_updoprqty
          , v_company_id
        ) ;
END gm_ac_sav_ma_adj;
--
PROCEDURE gm_ac_sav_adj_detail
    (
        p_maid    IN t215_part_qty_adjustment.c215_part_qty_adj_id%TYPE,
        p_num     IN t2605_audit_tag_detail.c205_part_number_id%TYPE,
        p_qty     IN t2605_audit_tag_detail.c2605_qty%TYPE,
        p_price   IN t216_part_qty_adj_details.c216_item_price%TYPE,
        p_owner_company_id IN t216_part_qty_adj_details.c1900_owner_company_id%TYPE,
        p_local_cost IN t216_part_qty_adj_details.c216_local_item_price%TYPE,
        p_user_id IN t2605_audit_tag_detail.c2605_last_updated_by%TYPE
    )
AS

BEGIN
	--
     INSERT
       INTO t216_part_qty_adj_details
        (
            c216_part_qty_adj_id, c215_part_qty_adj_id, c205_part_number_id
          , c216_qty_to_be_adjusted, c216_item_price, c216_created_by
          , c216_created_date, c1900_owner_company_id, c216_local_item_price
        )
        VALUES
        (
            s216_part_qty_adj_details.NEXTVAL, p_maid, p_num
          , p_qty, DECODE (p_price, NULL, get_inventory_cogs_value (p_num), p_price), p_user_id
          , CURRENT_DATE, p_owner_company_id, p_local_cost
        ) ;
END gm_ac_sav_adj_detail;
--
/***********************************************************************************
*This  procedure is used to adjust Inventory locations as  part of Physical Inventory.
*
**************************************************************************************/
PROCEDURE gm_sav_adj_inv_loc
    (
        p_ma_id_plus        IN t215_part_qty_adjustment.c215_part_qty_adj_id%TYPE,
        p_ma_id_minus       IN t215_part_qty_adjustment.c215_part_qty_adj_id%TYPE,
        p_physical_audit_id IN t2605_audit_tag_detail.c2600_physical_audit_id%TYPE,
        p_locationid        IN t2605_audit_tag_detail.c901_location_id%TYPE,
        p_user_id           IN t2605_audit_tag_detail.c2605_last_updated_by%TYPE
    )
AS
     v_company_id  t1900_company.c1900_company_id%TYPE;
	 v_plant_id    t5040_plant_master.C5040_PLANT_ID %TYPE;
	 
    --Adjustments for inventory locations
    CURSOR partref_pos_cur
    IS
         SELECT t2605.c2605_txn_id refid, t2605.c205_part_number_id pnum, SUM (c2605_qty) qty
           FROM t2605_audit_tag_detail t2605
          WHERE t2605.c2600_physical_audit_id = p_physical_audit_id
            AND t2605.c2605_void_fl          IS NULL
            AND t2605.c205_part_number_id    IS NOT NULL
            AND t2605.c901_location_id        = p_locationid
            AND t2605.c901_type               = 50676 -- adjustment
            AND t2605.c2605_qty              IS NOT NULL
            AND t2605.c2605_txn_id           IS NOT NULL
            AND t2605.c2605_qty               > 0
		    AND t2605.c5040_plant_id    = v_plant_id
       GROUP BY t2605.c2605_txn_id, t2605.c205_part_number_id;
    CURSOR partref_neg_cur
    IS
         SELECT t2605.c2605_txn_id refid, t2605.c205_part_number_id pnum, SUM (c2605_qty) qty
           FROM t2605_audit_tag_detail t2605
          WHERE t2605.c2600_physical_audit_id = p_physical_audit_id
            AND t2605.c2605_void_fl          IS NULL
            AND t2605.c205_part_number_id    IS NOT NULL
            AND t2605.c901_location_id        = p_locationid
            AND t2605.c901_type               = 50676 -- adjustment
            AND t2605.c2605_qty              IS NOT NULL
            AND t2605.c2605_txn_id           IS NOT NULL
            AND t2605.c2605_qty               < 0
		    AND t2605.c5040_plant_id    = v_plant_id
       GROUP BY t2605.c2605_txn_id, t2605.c205_part_number_id;
BEGIN
        select get_compid_frm_cntx(),get_plantid_frm_cntx() 
		into v_company_id,v_plant_id 
		from dual;

    --
    FOR partref IN partref_pos_cur
    LOOP
        -- '93327' Put Away: Plus//  93326:Picks: Minus c901_last_transaction_type
        --4301 Plus
        gm_sav_adj_each_inv_loc (p_ma_id_plus, partref.refid, partref.pnum, partref.qty, '4301', p_user_id,p_locationid) ;
    END LOOP;
    FOR partref IN partref_neg_cur
    LOOP
        --4302 Minus
        gm_sav_adj_each_inv_loc (p_ma_id_minus, partref.refid, partref.pnum, partref.qty, '4302', p_user_id,p_locationid) ;
    END LOOP;
END gm_sav_adj_inv_loc;
/***********************************************************************************
*This  procedure will adjust each Inventory location as  part of Physical Inventory.
*
**************************************************************************************/
PROCEDURE gm_sav_adj_each_inv_loc (
        p_maid       IN t215_part_qty_adjustment.c215_part_qty_adj_id%TYPE,
        p_locationid IN t5053_location_part_mapping.c5052_location_id%TYPE,
        p_pnum       IN t5053_location_part_mapping.c205_part_number_id%TYPE,
        p_qty        IN t2605_audit_tag_detail.c2605_qty%TYPE,
        p_action     IN t5054_inv_location_log.c901_action%TYPE,
        p_user_id    IN t2605_audit_tag_detail.c2605_last_updated_by%TYPE,
	p_warehouseid IN t2605_audit_tag_detail.c901_location_id%TYPE)
AS
    v_cnt    NUMBER;
    v_primekey_loc   t5053_location_part_mapping.c5052_location_id%TYPE;
    v_no_loc VARCHAR2 (200) ;
    v_id t5053_location_part_mapping.c5053_location_part_map_id%TYPE;
     v_warehouse_type t901_code_lookup.c901_code_id%TYPE;
     v_company_id  t1900_company.c1900_company_id%TYPE;
	v_plant_id    t5040_plant_master.c5040_plant_id%TYPE;
  
BEGIN
SELECT GET_COMPID_FRM_CNTX(), GET_PLANTID_FRM_CNTX()
	    INTO v_company_id, v_plant_id
	 FROM DUAL;
    --
    v_warehouse_type := get_rule_value (p_warehouseid, 'PI-LOCWAREHOUSE-MAP');
    v_no_loc := get_rule_value (p_warehouseid, 'PINOLOCPARTS') ;
      SELECT COUNT (1)
       INTO v_cnt
       FROM t5051_inv_warehouse t5051,t5052_location_master t5052
      WHERE t5052.c901_status         = 93310 --93310/Active  93202/Initiated
      AND t5051.C5051_INV_WAREHOUSE_ID=t5052.C5051_INV_WAREHOUSE_ID
       AND t5051.C901_STATUS_ID =1
       AND t5051.C901_WAREHOUSE_TYPE =v_warehouse_type --DECODE (p_warehouseid,20450,90800,4000114,56001)-- 56001 Restricted Warehouse  --90800 FG Warehouse
        AND t5052.c901_location_type IS NOT NULL -- 93336/bulk  93320/Pick Face  NULL
        AND t5052.c5052_location_cd   = p_locationid
        AND t5052.c5040_plant_id    = v_plant_id
        AND t5052.c5052_void_fl      IS NULL;
    IF (v_cnt                         > 0) --No location Mapped is a fictious location for Parts(literature) will
        -- return false for this If condition.
        THEN
          BEGIN       
             SELECT t5052.c5052_location_id 
               INTO v_primekey_loc
               FROM t5051_inv_warehouse t5051,t5052_location_master t5052, t5053_location_part_mapping t5053
              WHERE t5051.C5051_INV_WAREHOUSE_ID=t5052.C5051_INV_WAREHOUSE_ID
              AND t5052.c5052_location_id = t5053.c5052_location_id
                AND t5051.C901_STATUS_ID =1
                AND t5052.c901_status             = 93310 --93310/Active  93202/Initiated
                AND t5052.c901_location_type     IS NOT NULL -- 93336/bulk  93320/Pick Face   NULL
                 AND t5052.c5052_location_cd   = p_locationid
                AND t5053.c205_part_number_id = p_pnum
                AND t5052.c5040_plant_id    = v_plant_id
               AND t5051.C901_WAREHOUSE_TYPE = v_warehouse_type--DECODE (p_warehouseid,20450,90800,4000114,56001)-- 56001 Restricted Warehouse  --90800 FG Warehouse
               -- AND t5053.c5053_curr_qty IS NOT NULL --'036-B-002-A-1' has 3 parts with with qty 0 and 4 row with null
                -- part no  and null qty
                AND t5052.c5052_void_fl IS NULL;
            EXCEPTION 
             WHEN NO_DATA_FOUND THEN
                v_primekey_loc :=NULL;
             END;
        IF (v_primekey_loc IS NULL AND p_locationid <> v_no_loc) --No location Mapped is a fictious location for
            -- Parts(literature) will return false for this If condition.
            THEN
            GM_RAISE_APPLICATION_ERROR('-20999','174',p_pnum);
        ELSE
             UPDATE t5053_location_part_mapping t5053
            SET t5053.c5053_curr_qty          = NVL(t5053.c5053_curr_qty,0) + p_qty, t5053.c5053_last_updated_by = p_user_id,
                t5053.c5053_last_updated_date = CURRENT_DATE, t5053.c5053_last_update_trans_id = p_maid,
                c901_last_transaction_type    = p_action
              WHERE t5053.c5052_location_id   = v_primekey_loc
                AND t5053.c205_part_number_id = p_pnum;
                
        END IF;
    END IF;
END gm_sav_adj_each_inv_loc;


/***********************************************************************************
*This  procedure will adjust OUS distributor net count for Physical inventory.
*
**************************************************************************************/

PROCEDURE gm_sav_adj_witeup_txn (
        p_maid	 IN   t215_part_qty_adjustment.c215_part_qty_adj_id%TYPE,
		p_company_id IN t1900_company.c1900_company_id%TYPE,
		p_plant_id IN t5040_plant_master.C5040_PLANT_ID %TYPE,
   		p_user_id    IN t2605_audit_tag_detail.c2605_last_updated_by%TYPE,
		p_comment IN VARCHAR2)
AS
    
    CURSOR adjdet_cur
    IS
         SELECT t216.c205_part_number_id pnum, t216.c216_item_price amt, t216.c216_qty_to_be_adjusted qty
			  FROM t216_part_qty_adj_details t216
			 WHERE t216.c216_qty_to_be_adjusted <> 0 AND t216.c215_part_qty_adj_id = p_maid;
		  
    
		   
	v_request_id t520_request.c520_request_id%TYPE;
    v_conid t504_consignment.c504_consignment_id%TYPE;
    v_distid t701_distributor.c701_distributor_id%TYPE;
	v_company_id    t1900_company.c1900_company_id%TYPE;
    v_plant_id    t5040_plant_master.C5040_PLANT_ID %TYPE;
    v_us_time_zone t901_code_lookup.c901_code_nm%TYPE;
   	v_us_time_format t901_code_lookup.c901_code_nm%TYPE;
BEGIN
	SELECT get_compid_frm_cntx(),get_plantid_frm_cntx() INTO v_company_id,v_plant_id FROM dual; 
	--Get distributor id based on company
     v_distid     := get_rule_value (v_company_id, 'PADISTCOMPANYMAP');
	
	   SELECT GET_CODE_NAME(C901_TIMEZONE), GET_CODE_NAME(C901_DATE_FORMAT)
	        INTO v_us_time_zone,v_us_time_format
	     FROM t1900_company
	     WHERE c1900_company_id=p_company_id;
     
     --Set Context fro US
     gm_pkg_cor_client_context.gm_sav_client_context(p_company_id,v_us_time_zone,v_us_time_format,p_plant_id);
     
     SELECT 'GM-RQ-' || s520_request.NEXTVAL INTO v_request_id FROM DUAL;
     INSERT
       INTO t520_request
        (
            c520_created_date, c901_request_source, c520_required_date
          , c520_request_to, c520_status_fl, c901_ship_to
          , c520_request_id, c520_created_by, c520_request_for
          , c207_set_id, c520_ship_to_id, c520_request_date
          , c901_request_by_type,C1900_COMPANY_ID,C5040_PLANT_ID
        )
        VALUES
        (
            CURRENT_DATE, 50618, TRUNC (CURRENT_DATE)
          , v_distid, 40, 4120
          , v_request_id, p_user_id, 40021
          , '', v_distid, TRUNC (CURRENT_DATE)
          , 50626,p_company_id,p_plant_id
        ) ;
     SELECT get_next_consign_id ('consignment') INTO v_conid FROM DUAL;
    --p_out_conid := v_conid;
    --DBMS_OUTPUT.put_line ('CN #....' || v_consignment_id);
     INSERT
       INTO t504_consignment
        (
            c504_verified_date, c504_delivery_carrier, c520_request_id
          , c504_consignment_id, c504_delivery_mode, c504_status_fl
          , c504_type, c504_ship_to, c504_created_by
          , c701_distributor_id, c504_ship_to_id, c504_update_inv_fl
          , c504_ship_req_fl, c207_set_id, c504_ship_date
          , c504_verify_fl, c504_created_date, c504_comments,C1900_COMPANY_ID,C5040_PLANT_ID
        )
        VALUES
        (
            TRUNC (CURRENT_DATE), 5040, v_request_id
          , v_conid, 5031, 4
          , 4110, 4120, p_user_id
          , v_distid, v_distid, '1'
          , '1', '', TRUNC (CURRENT_DATE)
          , '1', CURRENT_DATE, p_comment,p_company_id,p_plant_id
        ) ;
		--Insert shipping record
     INSERT
       INTO t907_shipping_info
        (
            c907_shipping_id, c907_ref_id, c901_ship_to
          , c907_ship_to_id, c907_ship_to_dt, c901_source
          , c901_delivery_mode, c901_delivery_carrier, c907_status_fl
          , c907_created_by, c907_created_date, c907_active_fl
          , c907_release_dt, c907_shipped_dt,C1900_COMPANY_ID,C5040_PLANT_ID
        )
        VALUES
        (
            s907_ship_id.NEXTVAL, v_conid, 4120
          , v_distid, TRUNC (CURRENT_DATE), 50181
          , 5031, 5040, 40
          , p_user_id, CURRENT_DATE, 'N'
          , TRUNC (CURRENT_DATE), TRUNC (CURRENT_DATE),p_company_id,p_plant_id
        ) ;
		
	FOR adjdet In adjdet_cur
	LOOP
	--100880 Consignment Adjustment
	INSERT
           INTO t505_item_consignment
            (
                c505_item_price, c504_consignment_id, c505_item_consignment_id
              , c505_item_qty, c205_part_number_id, c505_control_number
              , c901_type
            )
            VALUES
            (
                adjdet.amt, v_conid, s504_consign_item.NEXTVAL
              , adjdet.qty, adjdet.pnum, 'NOC#'
              , 100880
            ) ;
			
			--48228 --OUS Inventory Adjustment to I/C Item Consignment
		gm_save_ledger_posting (48228, CURRENT_DATE, adjdet.pnum, v_distid, v_conid, adjdet.qty, adjdet.amt, p_user_id) ;
		
	END LOOP;
	--Adjust Fiels Sales Warehouse
	 gm_pkg_op_inv_field_sales_tran.gm_update_fs_inv(v_conid, 'T505', v_distid, 102900, 4301, 102907, p_user_id);
	
	 --Reset current context
	 SELECT GET_CODE_NAME(C901_TIMEZONE), GET_CODE_NAME(C901_DATE_FORMAT)
	        INTO v_us_time_zone,v_us_time_format
	     FROM t1900_company
	     WHERE c1900_company_id=v_company_id;
     
     --Reset Context to current company
     gm_pkg_cor_client_context.gm_sav_client_context(v_company_id,v_us_time_zone,v_us_time_format,v_plant_id);
END gm_sav_adj_witeup_txn;

/***********************************************************************************
*This  procedure will adjust Ous distributor net count for Physical inventory.
*
**************************************************************************************/
PROCEDURE gm_sav_adj_writeoff_txn (
        p_maid	 IN   t215_part_qty_adjustment.c215_part_qty_adj_id%TYPE,
		p_company_id IN t1900_company.c1900_company_id%TYPE,
		p_plant_id IN t5040_plant_master.C5040_PLANT_ID %TYPE,
   		p_user_id    IN t2605_audit_tag_detail.c2605_last_updated_by%TYPE,
		p_comment IN VARCHAR2)
AS
    CURSOR adjdet_cur
    IS
          SELECT t216.c205_part_number_id pnum, t216.c216_item_price amt, t216.c216_qty_to_be_adjusted qty
			  FROM t216_part_qty_adj_details t216
			 WHERE t216.c216_qty_to_be_adjusted <> 0 AND t216.c215_part_qty_adj_id = p_maid;
		  
	v_rmaid t506_returns.c506_rma_id%TYPE;
   	v_distid t504_consignment.c701_distributor_id%TYPE;
	v_company_id    t1900_company.c1900_company_id%TYPE;
    v_plant_id    t5040_plant_master.C5040_PLANT_ID %TYPE;
	v_us_time_zone t901_code_lookup.c901_code_nm%TYPE;
   	v_us_time_format t901_code_lookup.c901_code_nm%TYPE;
BEGIN
		SELECT get_compid_frm_cntx(),get_plantid_frm_cntx()
		INTO v_company_id,v_plant_id 
		FROM dual; 
	--Get distributor id based on company
     v_distid     := get_rule_value (v_company_id, 'PADISTCOMPANYMAP');
	 
     SELECT GET_CODE_NAME(C901_TIMEZONE), GET_CODE_NAME(C901_DATE_FORMAT)
	        INTO v_us_time_zone,v_us_time_format
	     FROM t1900_company
	     WHERE c1900_company_id=p_company_id;
     
     --Set Context fro US
     gm_pkg_cor_client_context.gm_sav_client_context(p_company_id,v_us_time_zone,v_us_time_format,p_plant_id);
     
    SELECT 'GM-RA-' || s506_return.NEXTVAL INTO v_rmaid FROM DUAL;
  
     INSERT
       INTO t506_returns
        (
            c506_rma_id, c506_comments, c506_created_date
          , c506_type, c701_distributor_id, c506_reason
          , c506_status_fl, c506_created_by, c506_expected_date
          , c207_set_id, c506_credit_date, c506_return_date
          , c501_reprocess_date, c901_txn_source,C1900_COMPANY_ID,C5040_PLANT_ID
        )
        VALUES
        (
            v_rmaid, p_comment, CURRENT_DATE
          , 3302, v_distid, 3313
          , '2', p_user_id, TRUNC (CURRENT_DATE)
          , '', TRUNC (CURRENT_DATE), TRUNC (CURRENT_DATE)
          , CURRENT_DATE, 102680,p_company_id,p_plant_id
        ) ;
     
	FOR adjdet In adjdet_cur
	LOOP
	INSERT
           INTO t507_returns_item
            (
                c507_returns_item_id, c507_item_qty, c507_item_price
              , c205_part_number_id, c507_control_number, c506_rma_id
              , c507_status_fl
            )
            VALUES
            (
                s507_return_item.NEXTVAL, adjdet.qty, adjdet.amt
              , adjdet.pnum, 'NOC#', v_rmaid
              , 'C'
            ) ;
		--48227 --I/C Item Consignment to OUS Inventory Adjustment
		gm_save_ledger_posting (48227
        , CURRENT_DATE, adjdet.pnum -- part#
        , v_distid -- DistributorID
        , v_rmaid -- RAID
        , adjdet.qty --QTY
        , adjdet.amt --
        , p_user_id) ;
	END LOOP;
	
	  gm_pkg_op_inv_field_sales_tran.gm_update_fs_inv(v_rmaid, 'T506', v_distid, 102901, 4302, 102908, p_user_id);
	  
	  --Reset current context
	 SELECT GET_CODE_NAME(C901_TIMEZONE), GET_CODE_NAME(C901_DATE_FORMAT)
	        INTO v_us_time_zone,v_us_time_format
	     FROM t1900_company
	     WHERE c1900_company_id=v_company_id;
     
     --Reset Context to current company
     gm_pkg_cor_client_context.gm_sav_client_context(v_company_id,v_us_time_zone,v_us_time_format,v_plant_id);	
END gm_sav_adj_writeoff_txn;
END gm_pkg_ac_physical_audit_info;
/
