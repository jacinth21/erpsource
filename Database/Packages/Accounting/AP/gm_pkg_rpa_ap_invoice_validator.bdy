create or replace PACKAGE BODY gm_pkg_rpa_ap_invoice_validator AS
 /**********************************************************************
  Purpose: Main Procedure to call all required procedures
  Author: Andrews Stanley
  Ref:  id      Name                    Alt
        111740	Not Processed	RPAINS	
        111741	Ready for Processing	RPAINS	
        111742	Invoice Incomplete	RPAINS	5
        111743	Invoice Entered	RPAINS	0
        111744	Released for Payment	RPAINS	15
        111745	Error in Processing	RPAINS	
        111746	Hold	RPAINS	10
        111747	Released for Download	RPAINS	20
        111748	Downloaded	RPAINS	30
  **********************************************************************/
    PROCEDURE gm_rpa_apinv_lock_main AS
    BEGIN
        gm_pkg_rpa_ap_invoice_validator.gm_rpa_apinv_update_att_hdr_id;
        gm_pkg_rpa_ap_invoice_validator.gm_rpa_apinv_data_tuner;
        gm_pkg_rpa_ap_invoice_validator.gm_rpa_apinv_part_validate;
        gm_pkg_rpa_ap_invoice_validator.gm_rpa_apinv_add_charges_validator;
        EXECUTE IMMEDIATE 'TRUNCATE TABLE t4303a_rpa_invoice_details_temp';
        gm_pkg_rpa_ap_invoice_validator.gm_rpa_apinv_po_validator;
        gm_pkg_rpa_ap_invoice_validator.gm_rpa_apinv_invoice_validator;
        gm_pkg_rpa_ap_invoice_validator.gm_rpa_apinv_email_header_updater;
        gm_pkg_rpa_ap_invoice_validator.gm_rpa_apinv_invoice_status_updater;
        gm_pkg_rpa_ap_invoice_validator.gm_rpa_apinv_email_hdr_status_updater;
    END gm_rpa_apinv_lock_main;

/**********************************************************************
  Purpose: Procedure to Update Atatchment Hdr ID
  Author:  Andrews Stanley
  **********************************************************************/

    PROCEDURE gm_rpa_apinv_update_att_hdr_id AS
    BEGIN
        UPDATE t4302_rpa_invoice_hdr t4302
        SET
            c4301_attachment_hdr_id = (
                SELECT
                    c4301_attachment_hdr_id
                FROM
                    t4301_rpa_email_attachment_hdr t4301
                WHERE
                    t4302.C4300_MSGID = t4301.C4300_MSGID
                    AND T4302.C4301_FILENAME_DATE = T4301.C4301_FILE_NAME
                    AND ( t4302.c4301_attachment_hdr_id IS NULL
                          OR t4302.c4301_attachment_hdr_id = 0 )
                    AND c4301_void_fl IS NULL
            )
            WHERE
   ( t4302.c4301_attachment_hdr_id IS NULL
                          OR t4302.c4301_attachment_hdr_id = 0 );

    UPDATE t4303a_rpa_invoice_details_temp t4303a
        SET
            c4301_attachment_hdr_id = (
                SELECT
                    c4301_attachment_hdr_id
                FROM
                    t4302_rpa_invoice_hdr t4302
                WHERE
                    t4302.C4302_INVOICE_HDR_ID= t4303a.C4302_INVOICE_HDR_ID
                AND c4302_void_fl IS NULL
            );

    END gm_rpa_apinv_update_att_hdr_id;

/**********************************************************************
  Purpose: Procedure to Fine Extracted Date in Invoice Header and Invoice Details tables
  Author: Andrews Stanley
  **********************************************************************/

    PROCEDURE GM_RPA_APINV_DATA_TUNER AS

        CURSOR cur_rpa_inv_hdr_dt IS
        SELECT
            c4302_po_number,
            c4302_invoice_number,
            c4302_invoice_date,
            c4302_invoice_amount,
            c4302_invoice_hdr_id
        FROM
            t4302_rpa_invoice_hdr
		WHERE
            c4302_void_fl IS NULL
            order by c4302_invoice_hdr_id;
        
   --         c4302_invoice_status = 111740;

        v_po_number            t4302_rpa_invoice_hdr.c4302_po_number%TYPE;
        v_invoice_number       t4302_rpa_invoice_hdr.c4302_invoice_number%TYPE;
        v_invoice_date         t4302_rpa_invoice_hdr.c4302_invoice_date%TYPE;
        v_invoice_amount       t4302_rpa_invoice_hdr.c4302_invoice_amount%TYPE;
        v_invoice_hdr_id       t4302_rpa_invoice_hdr.c4302_invoice_hdr_id%TYPE;
        CURSOR cur_rpa_inv_det_tmp_dt IS
        SELECT
            c205_part_number_id,
            c4303a_qty,
            c4303a_unit_price,
            c4303a_line_amount,
            c4303a_invoice_details_id
        FROM
            t4303a_rpa_invoice_details_temp
        WHERE
            c4303a_void_fl IS NULL
            order by c4303a_invoice_details_id;

        v_part_number_id       t4303a_rpa_invoice_details_temp.c205_part_number_id%TYPE;
        v_qty                  t4303a_rpa_invoice_details_temp.c4303a_qty%TYPE;
        v_unit_price           t4303a_rpa_invoice_details_temp.c4303a_unit_price%TYPE;
        v_line_amount          t4303a_rpa_invoice_details_temp.c4303a_line_amount%TYPE;
        v_invoice_details_id   t4303a_rpa_invoice_details_temp.c4303a_invoice_details_id%TYPE;
    BEGIN
       -- Invoicer Header Data tuning
        --dbms_output.put_line('Invoice Headers Fine Tuning Started');
        FOR rpa_inv_hdr_dt IN cur_rpa_inv_hdr_dt LOOP BEGIN
            --dbms_output.put_line('Invoice Headers Fine in Loop');
            v_po_number := rpa_inv_hdr_dt.c4302_po_number;

            --dbms_output.put_line('Invoice Headers data'|| v_po_number);
            v_invoice_number := rpa_inv_hdr_dt.c4302_invoice_number;
            --dbms_output.put_line('Invoice Headers data'|| v_invoice_number);
            v_invoice_date := rpa_inv_hdr_dt.c4302_invoice_date;
            --dbms_output.put_line('Invoice Headers data'|| v_invoice_date);
            v_invoice_amount := rpa_inv_hdr_dt.c4302_invoice_amount;
            --dbms_output.put_line('Invoice Headers data'|| v_invoice_amount);
            v_invoice_hdr_id := rpa_inv_hdr_dt.c4302_invoice_hdr_id;
            --dbms_output.put_line('Invoice Headers data'|| v_invoice_hdr_id);

                 --Remove "-" ,"." from Purchase Order Number
            v_po_number := regexp_replace(v_po_number, '_.', '-');
            select Decode(instr(v_po_number,','),0,v_po_number,substr(v_po_number,1,instr(v_po_number,',')-1)) into v_po_number from dual;
            select Decode(instr(v_po_number,' '),0,v_po_number,substr(v_po_number, 1,instr(v_po_number,' ')-1)) into v_po_number from dual;
            v_po_number := regexp_replace(v_po_number, '[^0-9GMPOgmpo-]','');

                --Remove special characters from Invoice Number
            v_invoice_number := regexp_replace(v_invoice_number, '[^0-9A-Za-z]', '');
                 --dbms_output.put_line('Invoice Headers data - '|| v_invoice_number);
               -- Change Invoice Date format
            v_invoice_date := TO_DATE(TO_CHAR(v_invoice_date, 'MM/DD/YYYY'),'MM/DD/YYYY');
                 --dbms_output.put_line('Invoice Headers data - '|| v_invoice_date);
              -- Remove Special characters and spaces from Invoice Amount
            v_invoice_amount := regexp_replace(v_invoice_amount, '[^0-9.]', '');
               --dbms_output.put_line('Invoice Headers data - '|| v_invoice_amount);

			-- Replace mistakenly extracted data with dot. regexp_replace('1.318.50','.',',',instr('1.318.50','.'),1) ->  1,318.50
            if REGEXP_COUNT(v_invoice_amount,'[.]') >1 then
                v_invoice_amount:= regexp_replace(v_invoice_amount,'.',',',instr(v_invoice_amount,'.'),1);
            END IF;

            UPDATE t4302_rpa_invoice_hdr
            SET
                c4302_po_number = v_po_number,
                c4302_invoice_number = v_invoice_number,
                c4302_invoice_date = v_invoice_date,
                c4302_invoice_amount = v_invoice_amount
            WHERE
                c4302_invoice_hdr_id = v_invoice_hdr_id;
            --dbms_output.put_line('Invoice Headers Fine Tuned'|| v_invoice_hdr_id);
        EXCEPTION
            WHEN OTHERS THEN
                begin
                --dbms_output.put_line('Invoice Headers Error'|| v_invoice_hdr_id);
                gm_ins_status_info('Error in executing procedute GM_RPA_APINV_DATA_TUNER, Reference Id:Invoice Hdr-'
                                   || rpa_inv_hdr_dt.c4302_invoice_hdr_id
                                   || ',Error:'
                                   || sqlerrm);
                end;
        END;
        END LOOP;
        --dbms_output.put_line('Invoice Headers Fine Tuning Completed');

          -- Invoicer Det Data tuning
        --dbms_output.put_line('Invoice Details Fine Tuning Started');
        FOR rpa_inv_det_tmp_dt IN cur_rpa_inv_det_tmp_dt LOOP BEGIN
            v_part_number_id := rpa_inv_det_tmp_dt.c205_part_number_id;
            v_qty := rpa_inv_det_tmp_dt.c4303a_qty;
            v_unit_price := rpa_inv_det_tmp_dt.c4303a_unit_price;
            v_line_amount := rpa_inv_det_tmp_dt.c4303a_line_amount;
            v_invoice_details_id := rpa_inv_det_tmp_dt.c4303a_invoice_details_id;

            --Remove "-" ,"." from Part Number and replace "_" with "." Should be in the below order
            select replace(v_part_number_id,'GM-','') into v_part_number_id from dual;
            select replace(v_part_number_id,'-','.') into v_part_number_id from dual;
			select replace(v_part_number_id,'_','.') into v_part_number_id from dual;
            select Decode(instr(v_part_number_id,','),0,v_part_number_id,substr(v_part_number_id,1,instr(v_part_number_id,',')-1)) into v_part_number_id from dual;
            select Decode(instr(v_part_number_id,' '),0,v_part_number_id,substr(v_part_number_id, 1,instr(v_part_number_id,' ')-1)) into v_part_number_id from dual;
            v_part_number_id := regexp_replace(v_part_number_id, '[^0-9A-Za-z.]', '');


            --Remove special characters from Qty
            v_qty := to_number(regexp_replace(v_qty, '[^0-9.]', ''));

           -- Change Invoice Date format
            v_unit_price := to_number(regexp_replace(v_unit_price, '[^0-9.]', ''));

          -- Remove Special characters and spaces from Line Amount
            v_line_amount := to_number(regexp_replace(v_line_amount, '[^0-9.]', ''));
            UPDATE t4303a_rpa_invoice_details_temp
            SET
                c205_part_number_id = v_part_number_id,
                c4303a_qty = v_qty,
                c4303a_unit_price = v_unit_price,
                c4303a_line_amount = v_line_amount
            WHERE
                c4303a_invoice_details_id = v_invoice_details_id;
            --dbms_output.put_line('Invoice Headers Fine Tuned'|| v_invoice_details_id);
        EXCEPTION
            WHEN OTHERS THEN
                gm_ins_status_info('Error in executing procedute GM_RPA_APINV_DATA_TUNER, Reference Id:Invoice Details-'
                                   || rpa_inv_det_tmp_dt.c4303a_invoice_details_id
                                   || ',Error:'
                                   || sqlerrm);
        END;
        END LOOP;
    --dbms_output.put_line('Invoice Details Fine Tuning Completed');
    END gm_rpa_apinv_data_tuner;

 /**********************************************************************
  Purpose: Procedure to validate PO # and Update Vendor Details
  Author: Andrews Stanley
  **********************************************************************/

    PROCEDURE gm_rpa_apinv_po_validator AS

        CURSOR cur_rpa_inv_hdr IS
        SELECT
            c4302_po_number,
            c4302_invoice_hdr_id
        FROM
            t4302_rpa_invoice_hdr
        WHERE c4302_invoice_status NOT in ( 111744,111746,111747,111748)
          AND C4302_void_fl is null ; -- Yoga

        v_po_number        t4302_rpa_invoice_hdr.c4302_po_number%TYPE;
        v_invoice_hdr_id   t4302_rpa_invoice_hdr.c4302_invoice_hdr_id%TYPE;
        v_vendor_id        t301_vendor.c301_vendor_id%TYPE;
    BEGIN
        FOR rpa_inv_hdr IN cur_rpa_inv_hdr LOOP
            BEGIN
                v_po_number := rpa_inv_hdr.c4302_po_number;
                v_invoice_hdr_id := rpa_inv_hdr.c4302_invoice_hdr_id;
                SELECT
                    c301_vendor_id
                INTO v_vendor_id
                FROM
                    t401_purchase_order
                WHERE
                    c401_purchase_ord_id LIKE  '%' || v_po_number;

                IF v_vendor_id IS NOT NULL THEN  -- Yoga
                    UPDATE t4302_rpa_invoice_hdr
                    SET
                        c4302_line_valid = 'P'
                    WHERE
                        c4302_invoice_hdr_id = v_invoice_hdr_id;

                    UPDATE t4300_rpa_email_hdr
                    SET
                        c301_vendor_id = v_vendor_id
                    WHERE
                        c4300_msgid = (
                            SELECT
                                c4300_msgid
                            FROM
                                t4302_rpa_invoice_hdr
                            WHERE
                                c4302_invoice_hdr_id = v_invoice_hdr_id
                            AND c4302_void_fl IS NULL
                        )
                        AND c301_vendor_id IS NULL;

                    UPDATE t4300_rpa_email_hdr
                    SET
                        c301_vendor_name = get_vendor_name(v_vendor_id)
                    WHERE
                        c4300_msgid = (
                            SELECT
                                c4300_msgid
                            FROM
                                t4302_rpa_invoice_hdr
                            WHERE
                                C4302_INVOICE_HDR_ID = V_INVOICE_HDR_ID
                            AND c4302_void_fl IS NULL
                        );
                       -- AND c301_vendor_id IS NULL; yoga

                ELSE
                    UPDATE t4302_rpa_invoice_hdr
                    SET
                        c4302_line_valid = 'F'
                    WHERE
                        c4302_invoice_hdr_id = v_invoice_hdr_id;

                END IF;

            EXCEPTION
                WHEN OTHERS THEN
                    gm_ins_status_info('Error in executing procedute GM_RPA_APINV_PO_VALIDATOR, Reference Id:Invoice Hdr ID-'
                                       || v_invoice_hdr_id
                                       || ',Error:'
                                       || sqlerrm);
            END;
        END LOOP;
    END gm_rpa_apinv_po_validator;

 /**********************************************************************
  Purpose: Procedure to validate part # in Invoice Details table
  Author: Andrews Stanley
  **********************************************************************/

    PROCEDURE gm_rpa_apinv_part_validate AS

        CURSOR cur_rpa_inv_det IS
        SELECT
            t4303a.c205_part_number_id,
            t4303a.c4303a_invoice_details_id,
            t4302.c4302_po_number
        FROM
            t4303a_rpa_invoice_details_temp   t4303a,
            t4302_rpa_invoice_hdr             t4302
        WHERE
            t4303a.c4302_invoice_hdr_id = t4302.c4302_invoice_hdr_id
        AND t4303a.c4303a_void_fl IS NULL
        AND t4302.c4302_void_fl IS NULL;

        v_part_number      t4303_rpa_invoice_details.c205_part_number_id%TYPE;
        v_invoice_det_id   t4303a_rpa_invoice_details_temp.c4303a_invoice_details_id%TYPE;
        v_po_number        t4302_rpa_invoice_hdr.c4302_po_number%TYPE;
        v_count            NUMBER;
    BEGIN
        FOR rpa_inv_det IN cur_rpa_inv_det LOOP BEGIN
            v_part_number := rpa_inv_det.c205_part_number_id;
            v_invoice_det_id    :=  rpa_inv_det.c4303a_invoice_details_id;
            v_po_number := rpa_inv_det.c4302_po_number;
            SELECT
                COUNT(c205_part_number_id)
            INTO v_count
            FROM
                t402_work_order
            WHERE
                c401_purchase_ord_id LIKE '%'|| v_po_number || '%'
                AND c205_part_number_id = v_part_number;
            --dbms_output.put_line('Count:' || v_count || v_invoice_det_id);
            IF v_count > 0 THEN
            -- Update as 'L' - Part found
                UPDATE t4303a_rpa_invoice_details_temp
                SET
                    c4303a_line_status = 'L'
                WHERE
                    c4303a_invoice_details_id = v_invoice_det_id;

                INSERT INTO t4303_rpa_invoice_details (
                    c4303_invoice_details_id,
                    c4302_invoice_hdr_id,
                    c4301_attachment_hdr_id,
                    c4300_msgid,
                    c4301_attachment_id,
                    c205_part_number_id,
                    c4303_part_desc,
                    c4303_qty,
                    c4303_unit_price,
                    c4303_line_amount,
                    c4303_line_status,
                    c4303_last_updated_by,
                    c4303_last_updated_date,
                    c4303_void_fl
                )
                    SELECT
                        c4303a_invoice_details_id,
                        c4302_invoice_hdr_id,
                        c4301_attachment_hdr_id,
                        c4300_msgid,
                        c4301_attachment_id,
                        c205_part_number_id,
                        c4303a_part_desc,
                        c4303a_qty,
                        c4303a_unit_price,
                        c4303a_line_amount,
                        c4303a_line_status,
                        c4303a_last_updated_by,
                        c4303a_last_updated_date,
                        c4303a_void_fl
                    FROM
                        t4303a_rpa_invoice_details_temp
                    WHERE
                        C4303A_INVOICE_DETAILS_ID = v_invoice_det_id
                    AND c4303a_void_fl IS NULL;
          ELSE
            -- Update as 'F' - Part not found
                UPDATE t4303a_rpa_invoice_details_temp
                SET
                    c4303a_line_status = 'F'
                WHERE
                    c4303a_invoice_details_id = v_invoice_det_id;

            END IF;

        EXCEPTION
            WHEN OTHERS THEN
                --dbms_output.put_line('Error:' || sqlerrm);
                gm_ins_status_info('Error in executing procedute GM_RPA_APINV_PART_VALIDATE, Reference Id:Invoice Details ID-'
                                   || v_invoice_det_id
                                   || ',Error:'
                                   || sqlerrm);
        END;
        END LOOP;


        --dbms_output.put_line('Part Validate completed');
    END gm_rpa_apinv_part_validate;

   /**********************************************************************
  Purpose: Procedure to validate Additional Charges
  Author: Andrews Stanley
  **********************************************************************/

    PROCEDURE gm_rpa_apinv_add_charges_validator AS

        CURSOR cur_rpa_inv_det_tmp_acv IS
        SELECT
            c205_part_number_id,
            c4303a_part_desc,
            c4303a_qty,
            c4303a_unit_price,
            c4303a_line_amount,
            c4303a_invoice_details_id,
            c4302_invoice_hdr_id,
            c4300_msgid,
            c4301_attachment_id
        FROM
            t4303a_rpa_invoice_details_temp
        WHERE
            c4303a_line_status = 'F'
        AND c4303a_void_fl IS NULL;

        v_part_number_id                       t4303a_rpa_invoice_details_temp.c205_part_number_id%TYPE;
        v_part_desc                            t4303a_rpa_invoice_details_temp.c4303a_part_desc%TYPE;
        v_qty                                  t4303a_rpa_invoice_details_temp.c4303a_qty%TYPE;
        v_unit_price                           t4303a_rpa_invoice_details_temp.c4303a_unit_price%TYPE;
        v_line_amount                          t4303a_rpa_invoice_details_temp.c4303a_line_amount%TYPE;
        v_invoice_details_id                   t4303a_rpa_invoice_details_temp.c4303a_invoice_details_id%TYPE;
        v_invoice_hdr_id                       t4303a_rpa_invoice_details_temp.c4302_invoice_hdr_id%TYPE;
        v_msgid                                t4303a_rpa_invoice_details_temp.c4300_msgid%TYPE;
        v_attachment_id                        t4303a_rpa_invoice_details_temp.c4301_attachment_id%TYPE;
        v_add_count                            NUMBER;
        v_c4306_additional_charge_mapping_id   t4306_rpa_add_charge_mapping.c4306_additional_charge_mapping_id%TYPE;
    BEGIN
        FOR rpa_inv_det_tmp_acv IN cur_rpa_inv_det_tmp_acv LOOP BEGIN
            v_part_number_id := rpa_inv_det_tmp_acv.c205_part_number_id;
            v_part_desc := rpa_inv_det_tmp_acv.c4303a_part_desc;
            v_qty := rpa_inv_det_tmp_acv.c4303a_qty;
            v_unit_price := rpa_inv_det_tmp_acv.c4303a_unit_price;
            v_line_amount := rpa_inv_det_tmp_acv.c4303a_line_amount;
            v_invoice_details_id := rpa_inv_det_tmp_acv.c4303a_invoice_details_id;
            v_invoice_hdr_id := rpa_inv_det_tmp_acv.c4302_invoice_hdr_id;
            v_msgid := rpa_inv_det_tmp_acv.c4300_msgid;
            v_attachment_id := rpa_inv_det_tmp_acv.c4301_attachment_id;
            IF v_line_amount <> 0 THEN
                SELECT
                    COUNT(c4306_additional_charge_name)
                INTO v_add_count
                FROM
                    t4306_rpa_add_charge_mapping
                WHERE
                      ( upper(c4306_additional_charge_name) = upper(v_part_desc)
                      OR upper(c4306_additional_charge_name) = upper(v_part_number_id) )
                 AND c4306_void_fl IS NULL;

                IF v_add_count > 0 THEN
                    UPDATE t4303a_rpa_invoice_details_temp
                    SET
                        c4303a_line_status = 'A'
                    WHERE
                        c4303a_invoice_details_id = v_invoice_details_id;

                -- Insert into Additional charges tables

                    INSERT INTO t4305_rpa_additional_charges (
                        c4302_invoice_hdr_id,
                        c4300_msgid,
                        c4301_attachment_id,
                        c4305_additional_charge_name,
                        c4305_additional_charge_amount,
                        c4305_last_updated_date,
                        c4305_last_updated_by,
                        c4306_additional_charges_code,
                        c4306_additional_charge_mapping_id
                    )
                        SELECT
                            v_invoice_hdr_id,
                            v_msgid,
                            v_attachment_id,
                            c4306_additional_charge_name,
                            v_line_amount,
                            SYSDATE,
                            '30301',
                            c4306_additional_charge_code,
                            c4306_additional_charge_mapping_id
                        FROM
                            t4306_rpa_add_charge_mapping
                        WHERE
							( upper(c4306_additional_charge_name) = upper(v_part_number_id)
                              OR upper(c4306_additional_charge_name) = upper(v_part_desc) )
                        AND c4306_void_fl IS NULL;

                END IF;

            END IF;

        EXCEPTION
            WHEN OTHERS THEN
                gm_ins_status_info('Error in executing procedute GM_RPA_APINV_ADD_CHARGES_VALIDATOR, Reference Id:Invoice Hdr ID-'
                                   || v_invoice_hdr_id
                                   || ',Error:'
                                   || sqlerrm);
        END;
        END LOOP;


        -- Insert Records in Invoice Details temp table into Invoice details log table.
        BEGIN
          INSERT INTO t4303b_rpa_invoice_details_log (
              c4302_invoice_hdr_id,
              c4301_attachment_hdr_id,
              c4300_msgid,
              c4301_attachment_id,
              c205_part_number_id,
              c4303b_part_desc,
              c4303b_qty,
              c4303b_unit_price,
              c4303b_line_amount,
              c4303b_line_status,
              c4303b_last_updated_by,
              c4303b_last_updated_date,
              c4303b_void_fl
          )
              SELECT
                  c4302_invoice_hdr_id,
                  c4301_attachment_hdr_id,
                  c4300_msgid,
                  c4301_attachment_id,
                  c205_part_number_id,
                  c4303a_part_desc,
                  c4303a_qty,
                  c4303a_unit_price,
                  c4303a_line_amount,
                  c4303a_line_status,
                  c4303a_last_updated_by,
                  c4303a_last_updated_date,
                  c4303a_void_fl
              FROM
                  t4303a_rpa_invoice_details_temp
              WHERE c4303a_void_fl IS NULL;
            EXCEPTION
              WHEN OTHERS THEN
                  gm_ins_status_info('Error in executing procedute gm_rpa_apinv_add_charges_validator,  Failed to create  t4303b_rpa_invoice_details_log' || ',Error:'
                                     || sqlerrm);
        END;

    END gm_rpa_apinv_add_charges_validator;


 /**********************************************************************
  Purpose: Procedure to validate Invoice Header and Invoice Details tables
  Author: Andrews Stanley
  **********************************************************************/

PROCEDURE gm_rpa_apinv_invoice_validator AS

    CURSOR cur_rpa_inv_hdr_field_check IS
    SELECT
        c4302_invoice_number,
        c4302_invoice_date,
        c4302_po_number,
        c4302_invoice_amount,
        c4302_invoice_hdr_id
    FROM
        t4302_rpa_invoice_hdr t4302
    WHERE
        c4302_invoice_status = 111740
    AND c4302_void_fl IS NULL;

    v_invoice_number           t4302_rpa_invoice_hdr.c4302_invoice_number%TYPE;
    v_invoice_date             t4302_rpa_invoice_hdr.c4302_invoice_date%TYPE;
    v_po_number                t4302_rpa_invoice_hdr.c4302_po_number%TYPE;
    v_invoice_amount           t4302_rpa_invoice_hdr.c4302_invoice_amount%TYPE;
    v_invoice_hdr_id           t4302_rpa_invoice_hdr.c4302_invoice_hdr_id%TYPE;
    v_count                    NUMBER;
    CURSOR cur_rpa_inv_det_field_check IS
    SELECT
        c4303_qty,
        c4303_unit_price,
        c4303_line_amount,
        c4303_invoice_details_id
    FROM
        t4303_rpa_invoice_details t4303
    WHERE
        c4303_line_status = 'L'
    AND c4303_void_fl IS NULL;

    v_qty                      t4303_rpa_invoice_details.c4303_qty%TYPE;
    v_unit_price               t4303_rpa_invoice_details.c4303_unit_price%TYPE;
    v_invoice_details_id       t4303_rpa_invoice_details.c4303_invoice_details_id%TYPE;
    v_line_amount              t4303_rpa_invoice_details.c4303_line_amount%TYPE;
    v_line_amount_parts        t4303_rpa_invoice_details.c4303_line_amount%TYPE;
    v_line_amount_addcharges   t4303_rpa_invoice_details.c4303_line_amount%TYPE;
    v_det_count                NUMBER;
    BEGIN

     --To Check if Invoice Header Fields are missing values
    FOR rpa_inv_hdr_field_check IN cur_rpa_inv_hdr_field_check LOOP BEGIN
        v_invoice_number := rpa_inv_hdr_field_check.c4302_invoice_number;
        v_invoice_date := rpa_inv_hdr_field_check.c4302_invoice_date;
        v_po_number := rpa_inv_hdr_field_check.c4302_po_number;
        v_invoice_amount := rpa_inv_hdr_field_check.c4302_invoice_amount;
        v_invoice_hdr_id := rpa_inv_hdr_field_check.c4302_invoice_hdr_id;
        IF v_invoice_number IS NULL OR v_invoice_date IS NULL OR v_po_number IS NULL OR v_invoice_amount IS NULL THEN
            UPDATE t4302_rpa_invoice_hdr
            SET
                c4302_line_valid = 'F',
                c4302_comments = 'Invoice Validation Failed'
            WHERE
                c4302_invoice_hdr_id = v_invoice_hdr_id;

        ELSE
            UPDATE t4302_rpa_invoice_hdr
            SET
                c4302_line_valid = 'P',
                c4302_comments = 'Invoice Validated Successfully'
            WHERE
                c4302_invoice_hdr_id = v_invoice_hdr_id;

        END IF;

    EXCEPTION
        WHEN OTHERS THEN
            gm_ins_status_info('Error in executing procedute GM_RPA_APINV_INVOICE_VALIDATOR, Reference Id:Invoice Hdr ID-'
                               || v_invoice_hdr_id
                               || ',Error:'
                               || sqlerrm);
    END;
    END LOOP;


     --To Check if Invoice Detail Fields are missing values

    FOR rpa_inv_det_field_check IN cur_rpa_inv_det_field_check LOOP BEGIN
        v_qty := rpa_inv_det_field_check.c4303_qty;
        v_unit_price := rpa_inv_det_field_check.c4303_unit_price;
        v_line_amount := rpa_inv_det_field_check.c4303_line_amount;
        v_invoice_details_id := rpa_inv_det_field_check.c4303_invoice_details_id;
        IF v_qty IS NULL OR v_unit_price IS NULL OR v_unit_price IS NULL THEN
            UPDATE t4303_rpa_invoice_details
            SET
                c4303_line_status = 'F'
            WHERE
                c4303_invoice_details_id = v_invoice_details_id;

        ELSE
            UPDATE t4303_rpa_invoice_details
            SET
                c4303_line_status = 'P'
            WHERE
                c4303_invoice_details_id = v_invoice_details_id;

        END IF;

    EXCEPTION
        WHEN OTHERS THEN
            gm_ins_status_info('Error in executing procedute GM_RPA_APINV_INVOICE_VALIDATOR, Reference Id:Invoice Details ID-'
                               || v_invoice_details_id
                               || ',Error:'
                               || sqlerrm);
    END;
    END LOOP;


     --To Check if Invoice Detail Fields line amount matches with the product of Qty and Unit Price

    FOR rpa_inv_det_field_check IN cur_rpa_inv_det_field_check
    LOOP
    BEGIN
        v_qty := rpa_inv_det_field_check.c4303_qty;
        v_unit_price := rpa_inv_det_field_check.c4303_unit_price;
        v_line_amount := rpa_inv_det_field_check.c4303_line_amount;
        v_invoice_details_id := rpa_inv_det_field_check.c4303_invoice_details_id;
        IF ( v_qty * v_unit_price ) <> v_line_amount THEN
            UPDATE t4303_rpa_invoice_details
            SET
                c4303_line_status = 'F'
            WHERE
                c4303_invoice_details_id = v_invoice_details_id;

        END IF;

    EXCEPTION
        WHEN OTHERS THEN
            gm_ins_status_info('Error in executing procedute GM_RPA_APINV_INVOICE_VALIDATOR, Reference Id:Invoice Details ID-'
                               || v_invoice_details_id
                               || ',Error:'
                               || sqlerrm);
    END;
    END LOOP;



--To Check if Invoice Detail Fields sum of line amount matches with the header invoice amount
    FOR rpa_inv_hdr_field_check IN cur_rpa_inv_hdr_field_check
    LOOP
    BEGIN
        v_invoice_number := rpa_inv_hdr_field_check.c4302_invoice_number;
        v_invoice_date := rpa_inv_hdr_field_check.c4302_invoice_date;
        v_po_number := rpa_inv_hdr_field_check.c4302_po_number;
        v_invoice_amount := rpa_inv_hdr_field_check.c4302_invoice_amount;
        v_invoice_hdr_id := rpa_inv_hdr_field_check.c4302_invoice_hdr_id;
        SELECT
            SUM(c4303_line_amount)
        INTO v_line_amount_parts
        FROM
            t4303_rpa_invoice_details t4302
        WHERE
            c4302_invoice_hdr_id = v_invoice_hdr_id
        AND c4303_void_fl IS NULL;

        SELECT
            SUM(c4305_additional_charge_amount)
        INTO v_line_amount_addcharges
        FROM
            t4305_rpa_additional_charges t4305
        WHERE
            c4302_invoice_hdr_id = v_invoice_hdr_id
        AND c4305_void_fl IS NULL;

        IF ( v_line_amount_parts + v_line_amount_addcharges ) <> v_invoice_amount THEN
            UPDATE t4302_rpa_invoice_hdr
            SET
                c4302_line_valid = 'F',
                c4302_comments = 'Invoice Validation Failed'
            WHERE
                c4302_invoice_hdr_id = v_invoice_hdr_id;

        END IF;

    EXCEPTION
        WHEN OTHERS THEN
            gm_ins_status_info('Error in executing procedute GM_RPA_APINV_INVOICE_VALIDATOR, Reference Id:Invoice Details ID-'
                               || v_invoice_details_id
                               || ',Error:'
                               || sqlerrm);
    END;
    END LOOP;


        --To Check if all invoice line details are passed validation

        FOR rpa_inv_hdr_field_check IN cur_rpa_inv_hdr_field_check
        LOOP
        BEGIN
        v_invoice_number           :=  rpa_inv_hdr_field_check.c4302_invoice_number;
        v_invoice_date             :=  rpa_inv_hdr_field_check.c4302_invoice_date;
        v_po_number                :=  rpa_inv_hdr_field_check.c4302_po_number;
        v_invoice_amount           :=  rpa_inv_hdr_field_check.c4302_invoice_amount;
        v_invoice_hdr_id           :=  rpa_inv_hdr_field_check.c4302_invoice_hdr_id;

            SELECT
                COUNT(c4303_invoice_details_id)
            INTO v_det_count
            FROM
                t4303_rpa_invoice_details
            WHERE
                c4302_invoice_hdr_id = v_invoice_hdr_id
                AND c4303_line_status = 'F'
                AND c4303_void_fl IS NULL;

            IF v_det_count > 0 THEN
                UPDATE t4302_rpa_invoice_hdr
                SET
                    c4302_line_valid = 'F',
                    c4302_invoice_status = 111745,
                    c4302_comments = 'Invoice Validation Failed'
                WHERE
                    c4302_invoice_hdr_id = v_invoice_hdr_id;

            ELSE
                UPDATE t4302_rpa_invoice_hdr
                SET
                    c4302_line_valid = 'P',
                    c4302_invoice_status = 111741,
                    c4302_comments = 'Invoice Validated Successfully'
                WHERE
                    c4302_invoice_hdr_id = v_invoice_hdr_id
                    AND c4302_line_valid = 'P';

            END IF;

            SELECT
                COUNT(c4303_invoice_details_id)
            INTO v_det_count
            FROM
                t4303_rpa_invoice_details
            WHERE
                c4302_invoice_hdr_id = v_invoice_hdr_id
            AND c4303_void_fl IS NULL;

            IF v_det_count = 0 THEN
                UPDATE t4302_rpa_invoice_hdr
                SET
                    c4302_line_valid = 'F',
                    c4302_invoice_status = 111745,
                    c4302_comments = 'Invoice Validation Failed (No Line items)'
                WHERE
                    c4302_invoice_hdr_id = v_invoice_hdr_id;

            END IF;
        EXCEPTION
            WHEN OTHERS THEN
                 gm_ins_status_info('Error in executing procedute GM_RPA_APINV_INVOICE_VALIDATOR, Reference Id:Invoice Details ID-' || v_invoice_details_id || ',Error:' || sqlerrm);
        END;
        END LOOP;

    END gm_rpa_apinv_invoice_validator;


 /**********************************************************************
  Purpose: Procedure to update email header
  Author: Andrews Stanley
  **********************************************************************/

    PROCEDURE gm_rpa_apinv_email_header_updater AS

      /*  CURSOR cur_rpa_inv_hdr_upd IS
        SELECT
            c4302_line_valid,
            c4302_invoice_hdr_id,
            c4301_attachment_hdr_id
        FROM
            t4302_rpa_invoice_hdr t4302
        WHERE
            c4302_line_valid = 'P';

        v_line_valid       t4302_rpa_invoice_hdr.c4302_line_valid%TYPE;
        v_invoice_hdr_id   t4302_rpa_invoice_hdr.c4302_invoice_hdr_id%TYPE;
        v_att_hdr_id       t4302_rpa_invoice_hdr.c4301_attachment_hdr_id%TYPE;
        v_status           NUMBER;*/
    BEGIN
        UPDATE t4300_rpa_email_hdr t4300
        SET
            c4300_status = 111741,
            c4300_target_folder = 'In-Process',
            c4300_error_msg = '',
            c4300_comments = 'In-Process'
        WHERE
            c4300_msgid IN (
                SELECT
                    c4300_msgid
                FROM
                    t4302_rpa_invoice_hdr t4302
                WHERE
                    c4302_invoice_status = 111741
                AND c4302_void_fl IS NULL    
            );

        UPDATE t4300_rpa_email_hdr t4300
        SET
            c4300_status = 111722,
            c4300_target_folder = 'Others',
            c4300_error_msg = 'Error in processing',
            c4300_comments = 'Cannot Process email due to invalid data'
        WHERE
            c4300_msgid IN (
                SELECT
                    c4300_msgid
                FROM
                    t4302_rpa_invoice_hdr t4302
                WHERE
                    c4302_invoice_status = 111745
                AND c4302_void_fl IS NULL
            );

    END gm_rpa_apinv_email_header_updater;

  /**********************************************************************
  Purpose: Procedure to update Invoice Status
  Author: Andrews Stanley
  **********************************************************************/

    PROCEDURE gm_rpa_apinv_invoice_status_updater AS

        CURSOR cur_rpa_inv_hdr_field_isu IS
        SELECT
            c4302_invoice_number,
            c4302_po_number,
            c4302_invoice_hdr_id,
            c4301_attachment_hdr_id
        FROM
            t4302_rpa_invoice_hdr t4302
        WHERE
            c4302_invoice_status NOT IN (
                111748
            )
        AND c4302_void_fl IS NULL;

        v_vendor_id        t301_vendor.c301_vendor_id%TYPE;
        v_invoice_number   t4302_rpa_invoice_hdr.c4302_invoice_number%TYPE;
        v_po_number        t4302_rpa_invoice_hdr.c4302_po_number%TYPE;
        v_invoice_hdr_id   t4302_rpa_invoice_hdr.c4302_invoice_hdr_id%TYPE;
        v_att_hdr_id       T4303_RPA_INVOICE_DETAILS.C4301_ATTACHMENT_HDR_ID%TYPE;
        v_status           NUMBER;
    BEGIN

        /* Update Invoice Header Status  - 1*/
        FOR  rpa_inv_hdr_field_isu IN cur_rpa_inv_hdr_field_isu
        LOOP
        BEGIN
                v_invoice_number        :=  rpa_inv_hdr_field_isu.c4302_invoice_number;
                v_po_number             :=  rpa_inv_hdr_field_isu.c4302_po_number;
                v_invoice_hdr_id        :=  rpa_inv_hdr_field_isu.c4302_invoice_hdr_id;
                v_att_hdr_id            :=  rpa_inv_hdr_field_isu.C4301_ATTACHMENT_HDR_ID;

            select c901_code_id  INTO v_status
            from
            (SELECT
                t901.c901_code_id

            FROM
                t406_payment       t406,
                t901_code_lookup   t901,
                t901_code_lookup   t901a
            WHERE
                t406.c406_status_fl = t901.c902_code_nm_alt
                AND t901.c901_code_nm = t901a.c901_code_nm
                AND c406_invoice_id = v_invoice_number
               -- AND t406.c301_vendor_id = v_vendor_id
                AND c401_purchase_ord_id LIKE '%'|| v_po_number || '%'
                AND t901.c901_code_grp = 'RPAINS'
                AND t901a.c901_code_grp = 'INVST'
                AND t406.c406_void_fl IS NULL
                AND t901.c901_void_fl IS NULL
                AND t901a.c901_void_fl IS NULL
                  order by C406_PAYMENT_ID desc
                ) where rownum=1;

            IF v_status is not null THEN
            dbms_output.put_line('in loop: Invo' || v_invoice_number || 'v_status:' || v_status || 'Attac Id:' || v_att_hdr_id);
                UPDATE t4302_rpa_invoice_hdr
                SET
                    c4302_invoice_status = v_status
                WHERE
                    c4302_invoice_hdr_id = v_invoice_hdr_id;

                UPDATE t4301_rpa_email_attachment_hdr
                SET
                    c4301_status = v_status
                WHERE
                    c4301_attachment_hdr_id = v_att_hdr_id;
            END IF;
            --dbms_output.put_line('Email Status Updated');
        EXCEPTION
            WHEN OTHERS THEN
              --dbms_output.put_line('Error:' || sqlerrm);
                 gm_ins_status_info('Error in executing procedute GM_RPA_APINV_INVOICE_STATUS_UPDATER. Part 1, Reference Id:Invoice Header ID-' || v_invoice_hdr_id || ',Error:' || sqlerrm);
        END;
        END LOOP;

    END gm_rpa_apinv_invoice_status_updater;

  /**********************************************************************
  Purpose: Procedure to update Email Header Status
  Author: Andrews Stanley
  **********************************************************************/

    PROCEDURE gm_rpa_apinv_email_hdr_status_updater AS

        CURSOR cur_rpa_email_hdr IS
        SELECT
            c4300_msgid,
            c4300_status,
            c4300_total_attachments
        FROM
            t4300_rpa_email_hdr t4300
        WHERE
            c4300_status NOT IN (
                111723
            )
        AND c4300_void_fl IS NULL;

        v_msgid        t4300_rpa_email_hdr.c4300_msgid%TYPE;
        v_status       t4300_rpa_email_hdr.c4300_status%TYPE;
        v_tot_att      t4300_rpa_email_hdr.c4300_total_attachments%TYPE;
        v_att_count    t4300_rpa_email_hdr.c4300_total_attachments%TYPE;
        v_att_status   t4301_rpa_email_attachment_hdr.c4301_status%TYPE;
    BEGIN
        FOR rpa_email_hdr IN cur_rpa_email_hdr
        LOOP
        BEGIN

                v_msgid         :=  rpa_email_hdr.c4300_msgid;
                v_status        :=  rpa_email_hdr.c4300_status;
                v_tot_att       :=  rpa_email_hdr.c4300_total_attachments;

            SELECT
                COUNT(t4301_rpa_email_attachment_hdr.c4301_attachment_hdr_id)
            INTO v_att_count
            FROM
                t4301_rpa_email_attachment_hdr
            WHERE
                c4300_msgid = v_msgid
                AND c4301_status IN (
                    111744,
                    111747,
                    111748
                )
                AND c4301_void_fl IS NULL;

            -- Checking all are in processed status
            IF v_att_count = v_tot_att and v_att_count<>0  THEN
                UPDATE t4300_rpa_email_hdr
                SET
                    c4300_status = 111723,
                    c4300_target_folder = 'PROCESSED',
                    c4300_error_msg = '',
                    c4300_comments = 'All Invoices processed succssfully'
                WHERE
                    c4300_msgid = v_msgid;

            ELSE
                SELECT
                    COUNT(t4301_rpa_email_attachment_hdr.c4301_attachment_hdr_id)
                INTO v_att_count
                FROM
                    t4301_rpa_email_attachment_hdr
                WHERE
                    c4300_msgid = v_msgid
                    AND c4301_status IN (
                        111740
                    )
                    AND c4301_void_fl IS NULL;

                IF v_att_count = v_tot_att and  v_att_count<>0 THEN
                    UPDATE t4300_rpa_email_hdr
                    SET
                        c4300_status = 111720,
                        c4300_target_folder = 'Stage',
                        c4300_error_msg = '',
                        c4300_comments = 'Yet to be processed'
                    WHERE
                        c4300_msgid = v_msgid
                        and c4300_current_folder<>'Others';

                ELSE
                    SELECT
                        COUNT(t4301_rpa_email_attachment_hdr.c4301_attachment_hdr_id)
                    INTO v_att_count
                    FROM
                        t4301_rpa_email_attachment_hdr
                    WHERE
                        c4300_msgid = v_msgid
                        AND c4301_status IN (
                            111741
                        )
                        AND c4301_void_fl IS NULL;

                    IF v_att_count = v_tot_att  and v_att_count<>0  THEN
                        UPDATE t4300_rpa_email_hdr
                        SET
                            c4300_status = 111721,
                            c4300_target_folder = 'Stage',
                            c4300_error_msg = '',
                            c4300_comments = 'Ready for processing'
                        WHERE
                            c4300_msgid = v_msgid;

                    ELSE
                        SELECT
                            COUNT(t4301_rpa_email_attachment_hdr.c4301_attachment_hdr_id)
                        INTO v_att_count
                        FROM
                            t4301_rpa_email_attachment_hdr
                        WHERE
                            c4300_msgid = v_msgid
                            AND c4301_status IN (
                                111745
                            )
                            AND c4301_void_fl IS NULL;

                        IF v_att_count = v_tot_att  and v_att_count<>0  THEN
                            UPDATE t4300_rpa_email_hdr
                            SET
                                c4300_status = 111722,
                                c4300_target_folder = 'Others',
                                c4300_error_msg = 'Error in Processing',
                                c4300_comments = 'Cannot Process email due to invalid data'
                            WHERE
                                c4300_msgid = v_msgid;

                        ELSE
                            UPDATE t4300_rpa_email_hdr
                            SET
                            c4300_status = 111721,
                        c4300_target_folder = 'In-Process',
                        c4300_error_msg = '',
                        c4300_comments = 'In-Process'
                        WHERE
                            c4300_msgid = v_msgid
                            and c4300_current_folder<>'Others';

                    END if;
                  END if;
                END if;
              END if;


    --Check 3 weeks (mentioned in rules) older email in stage folder which are 'Not Processed' to In Complete Folder
    UPDATE t4300_rpa_email_hdr
        SET
        c4300_status = 111724, c4300_target_folder = 'In-Complete'
        WHERE
        c4300_msgid IN (
            SELECT
                c4300_msgid
            FROM
                t4300_rpa_email_hdr
            WHERE
                trunc(c4300_received_date) <= trunc(SYSDATE) - (
                    SELECT
                        c906_rule_value
                    FROM
                        t906_rules
                    WHERE
                        c906_rule_grp_id = 'RPA_EXTRACTOR'
                        AND c906_rule_id = 'EMAIL_DAYS_MOVE'
                        AND c906_void_fl IS NULL
                )
                AND upper(c4300_target_folder) = 'STAGE'
                AND c4300_status in (111720, 111722)
                AND c4300_void_fl IS NULL
    );

EXCEPTION
    WHEN OTHERS THEN
        gm_ins_status_info('Error in executing procedute GM_RPA_APINV_EMAIL_HDR_STATUS_UPDATER, Reference Id:Message ID-'
                           || v_msgid
                           || ',Error:'
                           || sqlerrm);
        end;
    end   loop;
    end   gm_rpa_apinv_email_hdr_status_updater;

    PROCEDURE gm_ins_status_info (
        p_error_msg t910_load_log_table.c910_status_details%TYPE
    ) AS
    BEGIN
        INSERT INTO t910_load_log_table (
            c910_load_log_id,
            c910_log_name,
            c910_state_dt,
            c910_end_dt,
            c910_status_details,
            c910_status_fl
        ) VALUES (
            s910_load_log.NEXTVAL,
            'GM_PKG_RPA_INVOICE_VALIDATOR',
            SYSDATE,
            SYSDATE,
            p_error_msg,
            'F'
        );

    END gm_ins_status_info;

END gm_pkg_rpa_ap_invoice_validator;
/