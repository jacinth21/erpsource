/* Formatted on 2012/03/06 16:07 (Formatter Plus v4.8.0) */
--@"c:\database\packages\accounting\gm_pkg_ac_tag_info.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_ac_tag_info
IS
	/*******************************************************
	* Purpose: This procedure will fetch the history of the tags
	* Author : Mihir
	*******************************************************/
	PROCEDURE gm_fch_tag_history (
		p_tagid 		IN		 t5010_tag.c5010_tag_id%TYPE
	  , p_tag_history	OUT 	 TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_tag_history
		 FOR
			 SELECT   t5011.c5010_tag_id tagnum, t5011.c205_part_number_id partnum
					, t5011.c5011_control_number controlnum, t5011.c5011_last_updated_trans_id refid
					, t5011.c207_set_id setid
					, DECODE (TO_CHAR (t5011.c901_location_type)
							, '4120', TO_CHAR (get_distributor_name (t5011.c5011_location_id))
							, '4123', TO_CHAR (get_user_name (t5011.c5011_location_id))
							, TO_CHAR (get_code_name (t5011.c5011_location_id))
							 ) LOCATION
					, get_user_name (t5011.c5011_created_by) createdby
					, t5011.c5011_created_date datetime, c5011_lock_fl lockfl
					, get_code_name (t5011.c901_status) status
					, t5011.c5010_missing_since missingsince
					, get_code_name (t5011.c901_sub_location_type) sublocation_type
					, DECODE (TO_CHAR (t5011.c901_sub_location_type)
							, '50221', TO_CHAR (get_distributor_name (t5011.c5011_sub_location_id))
							, TO_CHAR (get_rep_name (t5011.c5011_sub_location_id))
							 ) sublocation
					, t5011.c5011_sub_location_comments comments, get_account_name (t5011.c704_account_id) ACCOUNT
					, t5011.c5010_retag_id retagid
					, t1900.c1900_company_cd COMPANYCD
                    , t5040.c5040_plant_name PLANTID
				 FROM t5011_tag_log t5011, t1900_company t1900 , t5040_plant_master t5040
				WHERE t5011.c5011_void_fl IS NULL 
				  AND t5011.c5010_tag_id = p_tagid
				  AND t5011.c1900_company_id = t1900.c1900_company_id(+)
                  AND t5011.c5040_plant_id = t5040.c5040_plant_id(+)
                  AND t1900.c1900_void_fl IS NULL
                  AND t5040.c5040_void_fl IS NULL
			  --ORDER BY t5011.c5011_tag_txn_log_id DESC;
				ORDER BY t5011.C5011_CREATED_DATE DESC;
	END gm_fch_tag_history;

--

	/********************************************************************************
		 * Description : Procedure to fetch tag info
		 * Author	: Xun
	 ********************************************************************************/
	PROCEDURE gm_fc_tag_detail (
		p_refid 	 	IN 	  t5010_tag.c5010_last_updated_trans_id%TYPE
	  , p_num		 	IN 	  t5010_tag.c205_part_number_id%TYPE
	  , p_reftype	 	IN 	  t5010_tag.c901_trans_type%TYPE
	  , p_out_cur1	 	OUT	  TYPES.cursor_type
	  , p_out_cur2	 	OUT	  TYPES.cursor_type
	  , p_intransit_fl	OUT   VARCHAR2
	)
	AS
		v_count			NUMBER;
	
	BEGIN
		IF (p_reftype = '51000') THEN
			OPEN p_out_cur1
			 FOR
				 SELECT t5010.c5010_tag_id tagid, t5010.c205_part_number_id pnum
					  , t5010.c5010_control_number controlnum, t5010.c5010_last_updated_trans_id refid
					  , t5010.c207_set_id setid, get_code_name (t5010.c901_location_type) location_type
					  , DECODE (t5010.c901_location_type
							  , 4120, get_distributor_name (t5010.c5010_location_id)
							  , get_code_name (t5010.c5010_location_id)
							   ) LOCATION
					  , t5010.c5010_history_fl historyfl, t5010.c5010_lock_fl lockfl
					  , get_code_name (t5010.c901_sub_location_type) sublocation_type
					  , DECODE (t5010.c901_sub_location_type
							  , 50221, get_distributor_name (t5010.c5010_sub_location_id)
							  , get_rep_name (t5010.c5010_sub_location_id)
							   ) sublocation
					  , get_account_name (t5010.c704_account_id) ACCOUNT
					  , t5010.c901_status tag_status, get_code_name(t5010.c901_status) tag_status_nm
				   FROM t5010_tag t5010
				  WHERE t5010.c5010_last_updated_trans_id = p_refid
					AND t5010.c205_part_number_id IN NVL (p_num, (t5010.c205_part_number_id))
					AND t5010.c5010_void_fl IS NULL
					AND t5010.c901_status <> 51013;
		ELSE
			OPEN p_out_cur1
			 FOR
				 SELECT t5010.c5010_tag_id tagid, t5010.c205_part_number_id pnum
					  , t5010.c5010_control_number controlnum, t5010.c5010_last_updated_trans_id refid, '' missing_fl
					  , t5010.c207_set_id setid, get_code_name (t5010.c901_location_type) location_type
					  , DECODE (t5010.c901_location_type
							  , 4120, get_distributor_name (t5010.c5010_location_id)
							  , get_code_name (t5010.c5010_location_id)
							   ) LOCATION
					  , t5010.c5010_history_fl historyfl, t5010.c5010_lock_fl lockfl
					  , get_code_name (t5010.c901_sub_location_type) sublocation_type
					  , DECODE (t5010.c901_sub_location_type
							  , 50221, get_distributor_name (t5010.c5010_sub_location_id)
							  , get_rep_name (t5010.c5010_sub_location_id)
							   ) sublocation
					  , get_account_name (t5010.c704_account_id) ACCOUNT
					  , t5010.c901_status tag_status, get_code_name(t5010.c901_status) tag_status_nm
				   FROM t5010_tag t5010
				  WHERE t5010.c5010_last_updated_trans_id = p_refid
					AND t5010.c205_part_number_id IN NVL (p_num, (t5010.c205_part_number_id))
					AND t5010.c5010_void_fl IS NULL
					AND t5010.c901_status <> 51013
				 UNION ALL
				 SELECT '-' tagid, t507.c205_part_number_id pnum, t507.c507_control_number controlnum
					  , t506.c506_rma_id refid, c507_missing_fl missing_fl, t506.c207_set_id setid
					  , get_code_name (40033) location_type, get_code_name (52099) LOCATION, '' historyfl, 'Y' lockfl
					  , get_code_name (40033) sublocation_type, get_code_name (52099) sublocation, '' ACCOUNT
					  , NULL tag_status, '' tag_status_nm
				   FROM t506_returns t506, t507_returns_item t507
				  WHERE t506.c506_rma_id = t507.c506_rma_id
					AND t506.c506_rma_id = p_refid
					AND t507.c205_part_number_id = p_num
					AND t506.c506_void_fl IS NULL
					AND t507.c507_missing_fl IS NOT NULL
					AND t507.c507_status_fl <> 'Q'
					AND TRIM (t507.c507_control_number) <> 'TBE';
		END IF;

		IF (p_reftype = 51000)	 --consignment
							  THEN
			OPEN p_out_cur2
			 FOR
				 SELECT '' tagid, t505.c205_part_number_id pnum, t505.c505_control_number controlnum
					  , t505.c504_consignment_id refid, t504.c207_set_id setid, t505.c505_item_qty item_qty
					  , get_tag_count (t505.c205_part_number_id
									 , t505.c504_consignment_id
									 , t505.c505_control_number
									  ) tagcount
				   FROM t505_item_consignment t505, t504_consignment t504
				  WHERE t505.c504_consignment_id = p_refid
					AND t504.c504_consignment_id = t505.c504_consignment_id
					AND t505.c205_part_number_id IN NVL
													   (p_num
													  , (SELECT DECODE
																	(get_part_attribute_value (t505.c205_part_number_id
																							 , 92340
																							  )
																   , 'Y', t505.c205_part_number_id
																   , NULL
																	)
														   FROM DUAL)
													   )
					--	AND t504.c504_type IN (4110)
					AND t505.c505_void_fl IS NULL
					AND t504.c504_void_fl IS NULL
					AND TRIM (t505.c505_control_number) <> 'TBE';
		END IF;

		IF (p_reftype = 51001)	 --Returns
							  THEN
			OPEN p_out_cur2
			 FOR
				 SELECT '' tagid, t507.c507_returns_item_id itemid, c507_missing_fl missing_fl
					  , t507.c205_part_number_id pnum, t507.c507_control_number controlnum, t506.c506_rma_id refid
					  , t506.c207_set_id setid, t507.c507_item_qty item_qty
					  , get_tag_count (t507.c205_part_number_id, t506.c506_rma_id, t507.c507_control_number) tagcount
					  , t506.c207_set_id setid
				   FROM t506_returns t506, t507_returns_item t507
				  WHERE t506.c506_rma_id = t507.c506_rma_id
					AND t506.c506_rma_id = p_refid
					AND t507.c205_part_number_id = p_num
					AND t506.c506_void_fl IS NULL
					AND t507.c507_status_fl <> 'Q'
					AND t507.c507_missing_fl IS NULL
					AND TRIM (t507.c507_control_number) <> 'TBE';
		END IF;
			
				SELECT COUNT(c504_consignment_id) 
				  INTO v_count 
				  FROM t504e_consignment_in_trans 
				 WHERE c504_consignment_id = p_refid 
				   AND c504e_void_fl is null;
				
	IF (v_count<>0) THEN
		p_intransit_fl := 'Y';
	END IF;
		
	END gm_fc_tag_detail;

		/********************************************************************************
		* Description : Procedure to save tag history
		* 			    Its a log table (company and plant should flow from parent) 
		* Author	 : Xun
	********************************************************************************/
	PROCEDURE gm_sav_tag_log (
		p_tagid 				  IN   t5011_tag_log.c5010_tag_id%TYPE
	  , p_control_num			  IN   t5011_tag_log.c5011_control_number%TYPE
	  , p_num					  IN   t5011_tag_log.c205_part_number_id%TYPE
	  , p_set_id				  IN   t5011_tag_log.c207_set_id%TYPE
	  , p_ref_id				  IN   t5011_tag_log.c5011_last_updated_trans_id%TYPE
	  , p_ref_type				  IN   t5011_tag_log.c901_trans_type%TYPE
	  , p_location_id			  IN   t5011_tag_log.c5011_location_id%TYPE
	  , p_location_type 		  IN   t5011_tag_log.c901_location_type%TYPE
	  , p_status				  IN   t5011_tag_log.c901_status%TYPE
	  , p_lock_fl				  IN   t5011_tag_log.c5011_lock_fl%TYPE
	  , p_void_fl				  IN   t5011_tag_log.c5011_void_fl%TYPE
	  , p_created_by			  IN   t5011_tag_log.c5011_created_by%TYPE
	  , p_created_date			  IN   t5011_tag_log.c5011_created_date%TYPE
	  , p_account_id			  IN   t5011_tag_log.c704_account_id%TYPE
	  , p_sub_location_type 	  IN   t5011_tag_log.c901_sub_location_type%TYPE
	  , p_sub_location_id		  IN   t5011_tag_log.c5011_sub_location_id%TYPE
	  , p_sub_location_comments   IN   t5011_tag_log.c5011_sub_location_comments%TYPE
	  , p_addressid 			  IN   t5011_tag_log.c106_address_id%TYPE
	  , p_salerepid 			  IN   t5011_tag_log.c703_sales_rep_id%TYPE
	  , p_invtype				  IN   t5010_tag.c901_inventory_type%TYPE
	  , p_retag_id                IN   t5010_tag.c5010_retag_id%TYPE
	  , p_company_id 	          IN   t5010_tag.c1900_company_id%TYPE DEFAULT NULL
	  , p_plant_id				  IN   t5010_tag.c5040_plant_id%TYPE DEFAULT NULL
	  , p_missing_since           IN   t5010_tag.c5010_missing_since%TYPE
	)
	AS

	BEGIN
		--
		INSERT INTO t5011_tag_log
					(c5011_tag_txn_log_id, c5011_control_number, c5011_last_updated_trans_id, c5011_location_id
				   , c5011_created_by, c5011_created_date, c5010_tag_id, c205_part_number_id, c901_trans_type
				   , c901_location_type, c5011_void_fl, c207_set_id, c901_status, c5011_lock_fl, c704_account_id
				   , c901_sub_location_type, c5011_sub_location_id, c5011_sub_location_comments, c106_address_id
				   , c703_sales_rep_id, c901_inventory_type, c5010_retag_id , c1900_company_id , c5040_plant_id, c5010_missing_since)
			 VALUES (s5011_tag_log.NEXTVAL, p_control_num, p_ref_id, p_location_id
				   , p_created_by, p_created_date, p_tagid, p_num, p_ref_type
				   , p_location_type, p_void_fl, p_set_id, p_status, p_lock_fl, p_account_id
				   , p_sub_location_type, p_sub_location_id, p_sub_location_comments, p_addressid
				   , DECODE (p_sub_location_type, 4121, p_salerepid, NULL), p_invtype, p_retag_id 
				   , DECODE(p_status,'51010',null,'51011',null,'51012',p_company_id,'51013',p_company_id,'102401',p_company_id) 
				   , DECODE(p_status,'51010',null,'51011',null,'51012',p_plant_id,'51013',p_plant_id,'102401',p_plant_id),p_missing_since
					);
--
	END gm_sav_tag_log;

	/*******************************************************
	  * Description : Procedure to save tag detail
	  * Author		: Xun
	  *******************************************************/
	PROCEDURE gm_sav_tag_detail (
		p_inputs   IN		VARCHAR2
	  , p_userid   IN		t301_vendor.c301_last_updated_by%TYPE
	  , p_errmsg   OUT		VARCHAR2
	)
	AS
		v_strlen	   NUMBER := NVL (LENGTH (p_inputs), 0);
		v_string	   VARCHAR2 (4000) := p_inputs;
		v_substring    VARCHAR2 (4000);
		v_tagid 	   t5010_tag.c5010_tag_id%TYPE;
		v_control_num  t5010_tag.c5010_control_number%TYPE;
		v_partnum	   t5010_tag.c205_part_number_id%TYPE;
		v_setid 	   t5010_tag.c207_set_id%TYPE;
		v_ref_type	   t5010_tag.c901_trans_type%TYPE;
		v_ref_id	   t5010_tag.c5010_last_updated_trans_id%TYPE;
		v_loction_type t5010_tag.c901_location_type%TYPE;
		v_loction_id   t5010_tag.c5010_location_id%TYPE;
		v_item_id	   t507_returns_item.c507_returns_item_id%TYPE;
		v_cur_loc	   t5010_tag.c901_location_type%TYPE;
		v_userid	   t101_user.c101_user_id%TYPE;
		v_type		   NUMBER;
		v_tag_status   NUMBER;
		v_missing_fl   t507_returns_item.c507_missing_fl%TYPE;
		v_to_mail	   VARCHAR2 (2000);
		v_subject	   VARCHAR2 (2000);
		v_mail_body    VARCHAR2 (2000);
		v_count 	   NUMBER;
		v_cur_pnum	   t5010_tag.c205_part_number_id%TYPE;
		v_cur_cnum	   t5010_tag.c5010_control_number%TYPE;
		v_imsg		   VARCHAR2 (2000);
		v_jmsg		   VARCHAR2 (2000);
		v_kmsg		   VARCHAR2 (2000);
		v_xmsg		   VARCHAR2 (2000);
		v_ymsg		   VARCHAR2 (2000);
		v_istr		   VARCHAR2 (2000);
		v_jstr		   VARCHAR2 (2000);
		v_kstr		   VARCHAR2 (2000);
		v_xstr		   VARCHAR2 (2000);
		v_ystr		   VARCHAR2 (2000);
		v_mstr		   VARCHAR2 (2000);
		v_mmsg		   VARCHAR2 (2000);
		v_plant_id     t5040_plant_master.c5040_plant_id %TYPE;
        v_company_id   t1900_company.c1900_company_id%TYPE;

	BEGIN
		v_imsg		:= 'Following tag ID is associated to a different part or Set <br>';   -- 20219 (AppError Code)
		v_jmsg		:= 'Following tags cannot be updated when status is available or inactive <br>';   --20171
		v_kmsg		:= 'Following released tags cannot be used in returns <br>';   -- 20221
		v_xmsg		:= 'Following tag is in Inhouse.Can not be used in returns <br>';	--20216
		v_ymsg		:= 'Following tag does not exist <br>';   -- 20173
		v_mmsg		:= 'Following tags are already Consigned, cannot be used <br>';
		
		SELECT get_plantid_frm_cntx()  INTO  v_plant_id   FROM DUAL;
		SELECT get_compid_frm_cntx() INTO  v_company_id FROM DUAL;

		IF v_strlen > 0 THEN
			WHILE INSTR (v_string, '|') <> 0 LOOP
				--
				v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
				v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
				--
				v_tagid 	:= NULL;
				v_control_num := NULL;
				v_partnum	:= NULL;
				v_setid 	:= NULL;
				--	v_ref_type	:= NULL;
				v_ref_id	:= NULL;
				v_loction_type := NULL;
				v_loction_id := NULL;
				v_missing_fl := NULL;
				--
				v_tagid 	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_control_num := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_partnum	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_setid 	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_ref_type	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_ref_id	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_loction_type := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);

				IF (v_ref_type = '51001') THEN
					v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
					v_missing_fl := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
					v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
					v_item_id	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
				END IF;

				v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
				v_loction_id := v_substring;

				BEGIN
					 SELECT t5010.c901_status
				       INTO v_tag_status
				       FROM t5010_tag t5010
				      WHERE t5010.c5010_tag_id   = v_tagid
				        AND t5010.c5010_void_fl IS NULL;
				EXCEPTION
				WHEN NO_DATA_FOUND THEN
				    v_tag_status := NULL;
				END;
				IF (v_tag_status IS NOT NULL AND v_tag_status = 102401) THEN
				    raise_application_error ('-20999', 'Cannot return a transferred Tag.') ; --Cannot Return the Transfered tag
				END IF;
				
				BEGIN
					IF (v_ref_type = '51000') THEN
						v_loction_id := 52098;

						SELECT COUNT (1)
						  INTO v_count
						  FROM t504_consignment t504
						 WHERE t504.c504_status_fl = 4 AND t504.c504_consignment_id = v_ref_id;

						IF (v_count > 1) THEN
							raise_application_error ('-20210', '');
						END IF;
					END IF;

					IF (v_ref_type = '51001' AND v_missing_fl = 'Y') THEN
						UPDATE t507_returns_item t507
						   SET c507_missing_fl = 'Y'
						 WHERE t507.c507_returns_item_id = v_item_id;

						v_to_mail	:= get_rule_value ('TAG_TEAM', 'E-MAIL');
						v_subject	:= 'Missing Tag for ' || v_ref_id;
						v_mail_body := 'The tag is missing for ' || v_ref_id;
						gm_com_send_email_prc (v_to_mail, v_subject, v_mail_body);
					ELSE
						SELECT	   t5010.c901_trans_type, t5010.c901_status, t5010.c901_location_type
								 , t5010.c205_part_number_id, t5010.c5010_control_number
							  INTO v_type, v_tag_status, v_cur_loc
								 , v_cur_pnum, v_cur_cnum
							  FROM t5010_tag t5010
							 WHERE t5010.c5010_tag_id = v_tagid AND t5010.c5010_void_fl IS NULL
						FOR UPDATE;

						IF (v_cur_pnum != v_partnum) THEN
							v_istr		:=
								   v_istr
								|| 'Tag ID: '
								|| v_tagid
								|| ' Part#: '
								|| v_cur_pnum
								|| ' CNum#: '
								|| v_cur_cnum
								|| '<br>';
						END IF;

						IF v_ref_type = '51000' AND v_cur_loc = 4120 THEN
							v_mstr		:= v_mstr || 'Tag ID: ' || v_tagid || '<br>';
						END IF;

						IF (v_tag_status = 51010 OR v_tag_status = 51013) THEN
							v_jstr		:= v_jstr || 'Tag ID: ' || v_tagid || '<br>';
						END IF;

						IF (v_tag_status = 51011 AND v_ref_type = '51001') THEN
							v_kstr		:= v_kstr || 'Tag ID: ' || v_tagid || '<br>';
						END IF;

						IF (v_ref_type = '51001') THEN
							v_loction_id := 52099;

							IF (v_cur_loc = '40033') THEN
								v_xstr		:= v_xstr || 'Tag ID: ' || v_tagid || '<br>';
							END IF;
						END IF;
						
						-- If its shipped to a distributor flag to distributor location
						UPDATE t5010_tag
						   SET c5010_control_number = v_control_num
							 , c205_part_number_id = v_partnum
							 , c207_set_id = v_setid
							 , c901_trans_type = v_ref_type
							 , c5010_last_updated_trans_id = v_ref_id
							 , c901_location_type = v_loction_type
							 , c5010_location_id = v_loction_id
							 , c901_status = DECODE (v_tag_status, 51011,51012,26240343,51012,26240614,51012,c901_status)
							 , c5010_missing_since = DECODE (v_tag_status, 26240614,NULL,c5010_missing_since)
							 , c5010_last_updated_by = p_userid
							 , c5010_last_updated_date = CURRENT_DATE
							 , c1900_company_id = v_company_id
      						 , c5040_plant_id    = v_plant_id
						 WHERE c5010_tag_id = v_tagid;
					END IF;
				EXCEPTION
					WHEN NO_DATA_FOUND THEN
						v_ystr		:= v_ystr || 'Tag ID: ' || v_tagid || '<br>';
				END;
			END LOOP;

			IF v_istr IS NOT NULL THEN
				p_errmsg	:= p_errmsg || v_imsg || v_istr;
			END IF;

			IF v_jstr IS NOT NULL THEN
				p_errmsg	:= p_errmsg || v_jmsg || v_jstr;
			END IF;

			IF v_kstr IS NOT NULL THEN
				p_errmsg	:= p_errmsg || v_kmsg || v_kstr;
			END IF;

			IF v_xstr IS NOT NULL THEN
				p_errmsg	:= p_errmsg || v_xmsg || v_xstr;
			END IF;

			IF v_ystr IS NOT NULL THEN
				p_errmsg	:= p_errmsg || v_ymsg || v_ystr;
			END IF;

			IF v_mstr IS NOT NULL THEN
				p_errmsg	:= p_errmsg || v_mmsg || v_mstr;
			END IF;
		END IF;
	END gm_sav_tag_detail;

--
	/****************************************************************
	 * Main procedure which will populate the log table
	 ***************************************************************/
--
	PROCEDURE gm_ac_sav_audit_entry_log (
		p_audit_entry_id			IN	 t2660_audit_entry_log.c2656_audit_entry_id%TYPE
	  , p_field_sales_details_id	IN	 t2660_audit_entry_log.c2651_field_sales_details_id%TYPE
	  , p_tag_id					IN	 t2660_audit_entry_log.c5010_tag_id%TYPE
	  , p_pnum						IN	 t2660_audit_entry_log.c205_part_number_id%TYPE
	  , p_cnum						IN	 t2660_audit_entry_log.c2660_control_number%TYPE
	  , p_setid 					IN	 t2660_audit_entry_log.c207_set_id%TYPE
	  , p_qty						IN	 t2660_audit_entry_log.c2660_qty%TYPE
	  , p_status					IN	 t2660_audit_entry_log.c901_status%TYPE
	  , p_comments					IN	 t2660_audit_entry_log.c2660_comments%TYPE
	  , p_location_type 			IN	 t2660_audit_entry_log.c901_location_type%TYPE
	  , p_location_id				IN	 t2660_audit_entry_log.c2660_location_id%TYPE
	  , p_ref_type					IN	 t2660_audit_entry_log.c901_ref_type%TYPE
	  , p_counted_by				IN	 t2660_audit_entry_log.c2660_counted_by%TYPE
	  , p_counterd_date 			IN	 t2660_audit_entry_log.c2656_counted_date%TYPE
	  , p_void_fl					IN	 t2660_audit_entry_log.c2660_void_fl%TYPE
	  , p_created_by				IN	 t2660_audit_entry_log.c2660_created_by%TYPE
	  , p_created_date				IN	 t2660_audit_entry_log.c2660_created_date%TYPE
	  , p_upload_file_list			IN	 t2660_audit_entry_log.c903_upload_file_list%TYPE
	  , p_ref_id					IN	 t2660_audit_entry_log.c2660_ref_id%TYPE
	  , p_ref_detail				IN	 t2660_audit_entry_log.c2660_ref_details%TYPE
	  , p_entry_type				IN	 t2660_audit_entry_log.c901_entry_type%TYPE
	  , p_reconciliation_status 	IN	 t2660_audit_entry_log.c901_reconciliation_status%TYPE
	  , p_field_sales_ref_lock_id	IN	 t2660_audit_entry_log.c2659_field_sales_ref_lock_id%TYPE
	  , p_posting_option			IN	 t2660_audit_entry_log.c901_posting_option%TYPE
	  , p_posting_comment			IN	 t2660_audit_entry_log.c2656_posting_comment%TYPE
	  , p_retag_id					IN	 t2660_audit_entry_log.c5010_retag_id%TYPE
	  , p_set_count_fl				IN 	 t2660_audit_entry_log.c2656_set_count_fl%TYPE
	  , p_retag_fl				IN 	 t2660_audit_entry_log.c2656_retag_fl%TYPE
	  , p_part_count_fl				IN 	 t2660_audit_entry_log.c2656_part_count_fl%TYPE
	  , p_tag_status				IN 	 t2660_audit_entry_log.c901_tag_status%TYPE
	  , p_address_id				IN 	 t2660_audit_entry_log.c106_address_id%TYPE
	  , p_address_other				IN 	 t2660_audit_entry_log.c2656_address_others%TYPE
	  , p_flagged_by				IN 	 t2660_audit_entry_log.c2656_flagged_by%TYPE
	  , p_flagged_dt				IN 	 t2660_audit_entry_log.c2656_flagged_dt%TYPE
	  , p_approved_by				IN 	 t2660_audit_entry_log.c2656_approved_by%TYPE
	  , p_approved_dt				IN 	 t2660_audit_entry_log.c2656_approved_dt%TYPE
	  , p_reconciled_by				IN 	 t2660_audit_entry_log.c2656_reconciled_by%TYPE
	  , p_reconciled_dt				IN 	 t2660_audit_entry_log.c2656_reconciled_dt%TYPE
	)
	AS
	BEGIN
		INSERT INTO t2660_audit_entry_log
					(c2660_audit_entry_log_id, c2660_control_number, c2656_audit_entry_id, c205_part_number_id
				   , c207_set_id, c2660_void_fl, c5010_tag_id, c2660_qty, c2651_field_sales_details_id, c901_status
				   , c2660_comments, c2660_counted_by, c2660_location_id, c2656_counted_date, c2660_created_by
				   , c2660_created_date, c901_location_type, c901_ref_type, c903_upload_file_list, c2660_ref_id
				   , c2660_ref_details, c901_entry_type, c901_reconciliation_status, c2659_field_sales_ref_lock_id
				   , c901_posting_option, c2656_posting_comment,c5010_retag_id ,c2656_set_count_fl, c2656_retag_fl
				   , c2656_part_count_fl, c901_tag_status, c106_address_id, c2656_address_others
				   , c2656_flagged_by, c2656_flagged_dt, c2656_approved_by, c2656_approved_dt
				   , c2656_reconciled_by, c2656_reconciled_dt
					)
			 VALUES (s2660_audit_entry_log.NEXTVAL, p_cnum, p_audit_entry_id, p_pnum
				   , p_setid, p_void_fl, p_tag_id, p_qty, p_field_sales_details_id, p_status
				   , p_comments, p_counted_by, p_location_id, p_counterd_date, p_created_by
				   , p_created_date, p_location_type, p_ref_type, p_upload_file_list, p_ref_id
				   , p_ref_detail, p_entry_type, p_reconciliation_status, p_field_sales_ref_lock_id
				   , p_posting_option, p_posting_comment, p_retag_id, p_set_count_fl, p_retag_fl
				   , p_part_count_fl, p_tag_status, p_address_id, p_address_other
				   , p_flagged_by, p_flagged_dt, p_approved_by, p_approved_dt
				   , p_reconciled_by, p_reconciled_dt
					);
	END gm_ac_sav_audit_entry_log;

	PROCEDURE gm_sav_changed_locations (
		p_updated_by   IN	VARCHAR2
	  , p_inputstr	   IN	VARCHAR2
	)
	AS
		v_tag_id	   VARCHAR2 (100);
		v_sub_location_type NUMBER;
		v_sub_location_id VARCHAR2 (20);
		v_comments	   VARCHAR2 (1000);
		v_accid 	   VARCHAR2 (100);
		v_org_accid    VARCHAR2 (100);
		v_org_sub_location_type NUMBER;
		v_org_sub_location_id VARCHAR2 (20);
		v_tempstr	   VARCHAR2 (400);
		v_tempinputstr VARCHAR2 (32767) := p_inputstr;
	 /*******************************************************
	* Description : Procedure to save group part info
	* Author	: Xun
	*******************************************************/
	BEGIN
		WHILE INSTR (v_tempinputstr, '|') <> 0 LOOP
			--
			v_tempstr	:= SUBSTR (v_tempinputstr, 1, INSTR (v_tempinputstr, '|') - 1);
			--
			v_tag_id	:= NULL;
			v_sub_location_type := NULL;
			v_sub_location_id := NULL;
			v_accid 	:= NULL;
			--
			v_tag_id	:= SUBSTR (v_tempstr, 1, INSTR (v_tempstr, '^') - 1);
			v_tempstr	:= SUBSTR (v_tempstr, INSTR (v_tempstr, '^') + 1);
			v_sub_location_type := TO_NUMBER (SUBSTR (v_tempstr, 1, INSTR (v_tempstr, '^') - 1));
			v_tempstr	:= SUBSTR (v_tempstr, INSTR (v_tempstr, '^') + 1);
			v_sub_location_id := SUBSTR (v_tempstr, 1, INSTR (v_tempstr, '^') - 1);
			v_sub_location_id := TRIM ('D' FROM v_sub_location_id);
			v_sub_location_id := TRIM ('S' FROM v_sub_location_id);
			v_tempstr	:= SUBSTR (v_tempstr, INSTR (v_tempstr, '^') + 1);
			v_accid 	:= SUBSTR (v_tempstr, 1, INSTR (v_tempstr, '^') - 1);
			v_tempstr	:= SUBSTR (v_tempstr, INSTR (v_tempstr, '^') + 1);
			v_comments	:= v_tempstr;
			v_tempinputstr := SUBSTR (v_tempinputstr, INSTR (v_tempinputstr, '|') + 1);

			SELECT c5010_sub_location_id, c704_account_id
			  INTO v_org_sub_location_id, v_org_accid
			  FROM t5010_tag
			 WHERE c5010_tag_id = v_tag_id;

			IF	  NVL (v_org_accid, ' ') != v_accid
			   OR NVL (v_org_sub_location_id, ' ') != v_sub_location_id
			   OR v_comments IS NOT NULL THEN
				UPDATE t5010_tag
				   SET c901_sub_location_type = v_sub_location_type
					 , c5010_sub_location_id = v_sub_location_id
					 , c5010_sub_location_comments = v_comments
					 , c704_account_id = v_accid
					 , c5010_last_updated_by = p_updated_by
					 , c5010_last_updated_date = CURRENT_DATE
				 WHERE c5010_tag_id = v_tag_id;
			END IF;
		END LOOP;
	END gm_sav_changed_locations;

/*******************************************************
	* Purpose: This procedure will fetch the tag transfer details
	* Author : Ritesh
	*******************************************************/
	PROCEDURE gm_fch_edit_tag (
		p_tagid 	IN		 t5010_tag.c5010_tag_id%TYPE
	  , p_tag_dtl	OUT 	 TYPES.cursor_type
	)
	AS
	 v_dateformat 	varchar2(100);
	BEGIN
		-- to get the date format
     SELECT get_compdtfmt_frm_cntx()
       INTO v_dateformat
       FROM DUAL;
		OPEN p_tag_dtl
		 FOR
			 SELECT t5010.c5010_tag_id tagid, t5010.c205_part_number_id pnum, t5010.c207_set_id setid
				  , t5010.c5010_control_number controlnum, t5010.c901_status status
				  , get_distributor_name (t5010.c5010_location_id) currlocation, t5010.c5010_location_id consignedto
				  , t5010.c901_location_type locationtype, t5010.c5010_last_updated_trans_id refid
				  , t5010.c901_inventory_type inventorytype
				  , DECODE (t5010.c5010_void_fl, 'Y', 'disabled', NULL) strdisabled
				  , get_tag_inv_type (t5010.c5010_last_updated_trans_id) settype
				  , t5010.c5010_sub_location_comments comments
				  , get_rule_value('DISTRIBUTOR','TAGST') missingdistid
				  , nvl(t5010.c5010_LOCATION_ID,'-999') location
				  , TO_CHAR(t5010.c5010_missing_since,v_dateformat) missingDate
			   FROM t5010_tag t5010
			  WHERE t5010.c5010_tag_id = p_tagid;
	--AND t5010.c5010_void_fl IS NULL;
	END gm_fch_edit_tag;

	/********************************************************************************
			 * Description : Procedure to save tag details
			 * Author	: Ritesh
		********************************************************************************/
	PROCEDURE gm_sav_edit_tag (
		p_tagid 		IN	 t5010_tag.c5010_tag_id%TYPE
	  , p_num			IN	 t5010_tag.c205_part_number_id%TYPE
	  , p_consignedto	IN	 t5010_tag.c5010_location_id%TYPE
	  , p_set_id		IN	 t5010_tag.c207_set_id%TYPE
	  , p_control_num	IN	 t5010_tag.c5010_control_number%TYPE
	  , p_status		IN	 t5010_tag.c901_status%TYPE
	  , p_ref_id		IN	 t5010_tag.c5010_last_updated_trans_id%TYPE
	  , p_loctype		IN	 t5010_tag.c901_location_type%TYPE
	  , p_invtype		IN	 t5010_tag.c901_inventory_type%TYPE
	  , p_userid		IN	 t5010_tag.c5010_created_by%TYPE
	  , p_comments      IN   t5010_tag.c5010_sub_location_comments%TYPE
	  , p_missingdt     IN   t5010_tag.C5010_MISSING_SINCE%TYPE
	  , p_out_msg  		OUT  VARCHAR2
	
	)
	AS
		v_reftype	     NUMBER;
		v_plant_id       t5040_plant_master.c5040_plant_id%TYPE;
		v_company_id     t1900_company.c1900_company_id%TYPE;
	
	BEGIN
		-- to validate the edit tag
		gm_pkg_ac_tag_info.gm_validate_edit_tag (p_tagid , p_num, p_consignedto, p_set_id, p_control_num, p_status, p_ref_id, p_loctype);
		
		SELECT get_plantid_frm_cntx()  	INTO  v_plant_id   FROM DUAL;
		SELECT get_compid_frm_cntx() 	INTO  v_company_id FROM DUAL;

--Check for Ref ID and Set ID combination for Loaner
		SELECT get_tag_inv_type (p_ref_id)
		  INTO v_reftype
		  FROM DUAL;

-- Remove set Type validation Incase of set having LN,DS,ED at the end.
/*		IF v_reftype = 4127 AND v_settypecnt = 0 THEN
			raise_application_error ('-20529', '');
		END IF;*/

		/*IF v_reftype = 4110 AND v_settypecnt = 0 THEN
			raise_application_error ('-20530', '');
		END IF;*/
		IF (p_status = '26240614') THEN    --Missing Status
			gm_pkg_set_missing.gm_save_set_missing (p_tagid, p_missingdt, p_userid);
		ELSE
		UPDATE t5010_tag t5010
		   SET t5010.c5010_control_number = p_control_num
			 , t5010.c5010_last_updated_trans_id = p_ref_id
			 , t5010.c5010_location_id = DECODE (p_consignedto, 0, NULL, p_consignedto)
			 , t5010.c5010_lock_fl = DECODE (p_status, 51012, 'Y', NULL)
			 , t5010.c205_part_number_id = p_num
			 , t5010.c901_trans_type = DECODE (p_status, 51012, DECODE (p_consignedto, 52099, 51001, 51000), 26240343,51000, NULL)
			 , t5010.c207_set_id = DECODE (p_set_id, '0', NULL, p_set_id)
			 , t5010.c901_location_type = DECODE (p_loctype, 0, NULL, p_loctype)
			 , t5010.c901_status = p_status
			 , t5010.c901_sub_location_type = DECODE (p_status, 51011, NULL, t5010.c901_sub_location_type)
			 , t5010.c5010_sub_location_id = DECODE (p_status, 51011, NULL, t5010.c5010_sub_location_id)
			 , t5010.c703_sales_rep_id = DECODE (p_status, 51011, NULL, t5010.c703_sales_rep_id)
			 , t5010.c704_account_id = DECODE (p_status, 51011, NULL, t5010.c704_account_id)
			 , t5010.c106_address_id = DECODE (p_status, 51011, NULL, t5010.c106_address_id)
			 , t5010.c901_inventory_type = DECODE (p_status, 51011, NULL, t5010.c901_inventory_type)
			 , t5010.c5010_sub_location_comments = p_comments 
			 , t5010.c5010_last_updated_by = p_userid
			 , t5010.c5010_last_updated_date = CURRENT_DATE
			 , t5010.c1900_company_id 	= v_company_id
			 , t5010.c5040_plant_id    	= v_plant_id
		 WHERE t5010.c5010_tag_id = p_tagid AND t5010.c5010_void_fl IS NULL;
	 END IF;

	-- Validate Tag details for p_ref_id 
	   gm_pkg_op_ln_cn_swap.gm_validate_tag(p_ref_id,p_set_id);
	   
 /*
--Consigned qty cannot exceed total tag count for that Refid.
		IF v_reftype = 4110 THEN
			BEGIN
				SELECT SUM (t505.c505_item_qty)
				  INTO v_consqty
				  FROM t504_consignment t504, t505_item_consignment t505, t205d_part_attribute t205d
				 WHERE t504.c504_consignment_id = t505.c504_consignment_id
				   AND t504.c504_consignment_id = p_ref_id
				   AND t505.c205_part_number_id = t205d.c205_part_number_id
				   AND t205d.c901_attribute_type = 92340;
			-- AND t505.c205_part_number_id = p_num
			EXCEPTION
				WHEN NO_DATA_FOUND THEN
					v_consqty	:= 0;
			END;

			SELECT COUNT (t5010.c5010_tag_id)
			  INTO v_tagcnt
			  FROM t5010_tag t5010
			 WHERE c5010_last_updated_trans_id = p_ref_id;

			--AND t5010.c205_part_number_id = p_num
			IF (v_tagcnt >= v_consqty) THEN
				raise_application_error ('-20304', '');
			END IF;
		END IF;
*/
		p_out_msg := 'Tag Saved Successfully';
	END gm_sav_edit_tag;

/*******************************************************
	* Purpose: This procedure will unvoid the tag
	* Author : jshah
	*******************************************************/
	PROCEDURE gm_unvoid_field_tag (
		p_txn_id   IN	t5010_tag.c5010_tag_id%TYPE
	  , p_userid   IN	t5010_tag.c5010_last_updated_by%TYPE
	)
	AS
	BEGIN
		UPDATE t5010_tag
		   SET c5010_void_fl = NULL
			 , c5010_last_updated_by = p_userid
			 , c5010_last_updated_date = CURRENT_DATE
		 WHERE c5010_tag_id = p_txn_id AND c5010_void_fl IS NOT NULL;
	END gm_unvoid_field_tag;

/*******************************************************
	* Purpose: This procedure will save the tag status
	* Author : jshah
	*******************************************************/
	PROCEDURE gm_sav_tag_status (
		p_tag_from	 IN   t5010_tag.c5010_tag_id%TYPE
	  , p_tag_to	 IN   t5010_tag.c5010_tag_id%TYPE
	  , p_status	 IN   t5010_tag.c901_status%TYPE
	  , p_user_id	 IN   t5010_tag.c5010_created_by%TYPE
	)
	AS
		--
		CURSOR tag_cur
		IS
			SELECT t5010.c901_status status, t5010.c5010_tag_id tagid
			  FROM t5010_tag t5010
			 WHERE t5010.c5010_tag_id >= p_tag_from AND t5010.c5010_tag_id <= p_tag_to AND t5010.c5010_void_fl IS NULL;

		cur 		   tag_cur%ROWTYPE;
	BEGIN
		OPEN tag_cur;

		FETCH tag_cur
		 INTO cur;

		IF tag_cur%NOTFOUND THEN
			raise_application_error ('-20528', 'The entered Tag Range is not valid Range.');
		END IF;

		CLOSE tag_cur;

		FOR tag_val IN tag_cur LOOP
			IF p_status = 51011 THEN
				IF (tag_val.status != 51010) THEN
					raise_application_error ('-20525', 'One or more tag are not in Available status.');
				END IF;
			ELSIF p_status = 51010 THEN
				IF (tag_val.status != 51011) THEN
					raise_application_error ('-20526', 'One or more tag are not in Release status.');
				END IF;
			ELSE
				raise_application_error ('-20527', 'Please choose status as Available or Release.');
			END IF;
		END LOOP;

		UPDATE t5010_tag t5010
		   SET t5010.c901_status = p_status   --51011 released, 51010 available
			 , t5010.c5010_last_updated_by = p_user_id
			 , t5010.c5010_last_updated_date = CURRENT_DATE
		 WHERE t5010.c5010_tag_id >= p_tag_from
		   AND t5010.c5010_tag_id <= p_tag_to
		   AND c901_status = DECODE (p_status, 51011, 51010, 51010, 51011, NULL)
		   AND t5010.c5010_void_fl IS NULL;
	END gm_sav_tag_status;
	
	/*******************************************************
	* Purpose: This procedure will fetch the history of the audit Entry tag
	* Author : Mihir
	*******************************************************/
	PROCEDURE gm_fch_fa_entry_history (
		p_audit_entry_id 		IN		 t2660_audit_entry_log.c2656_audit_entry_id%TYPE
	  , p_audit_entry_history	OUT 	 TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_audit_entry_history
		 FOR
			 SELECT t2660.c2660_audit_entry_log_id logid,t2660.c2656_audit_entry_id entryid, t2660.c5010_tag_id tagnum, t2660.c205_part_number_id pnum
  , t2660.c2660_control_number cnum, t2660.c207_set_id setid, t2660.c2660_qty qty
  , get_code_name (t2660.c901_status) status, t2660.c2660_comments comments, get_code_name (t2660.c901_location_type)
    loc_type,  gm_pkg_ac_audit_reconciliation.get_location_details (t2660.c2660_location_id, t2660.c901_location_type) location_id, get_code_name (t2660.c901_ref_type) ref_type
  , DECODE (instr (',51023,51025,51026,51027,', (','||t2660.c901_ref_type
    ||',')), 0, (DECODE (instr (',51024,51028,18551,18552,18556,18557,18558, ', (','||t2660.c901_ref_type||',')), 0, (DECODE (
    t2660.c901_ref_type, '51029',t2660.c2660_ref_id,NULL)), get_code_name (t2660.c2660_ref_id))),
    get_distributor_name (t2660.c2660_ref_id))
    ref_id, t2660.c2660_ref_details refdetails, get_code_name (t2660.c901_entry_type) entry_type
  , get_code_name (t2660.c901_reconciliation_status) recon_status, get_user_name (t2660.c2660_counted_by) countedby,
    t2660.c2656_counted_date counted_date, get_user_name (t2660.c2660_created_by) created_by, TO_CHAR (t2660.c2660_created_date,get_rule_value ('DATEFMT', 'DATEFORMAT')||' HH:MI:SS')
    created_dt,get_code_name(t2660.c901_tag_status) tagstatus , t2660.c5010_retag_id retagid, t2660.c2656_address_others address_others
  , get_user_name (t2660.c2656_approved_by) approved_by, t2660.c2656_approved_dt approved_dt, get_user_name (
    t2660.c2656_flagged_by) flagged_by, t2660.c2656_flagged_dt flagged_dt, t2660.c2656_part_count_fl part_count_fl
  , t2660.c2656_posting_comment post_comment, get_user_name (t2660.c2656_reconciled_by) recon_by,
    t2660.c2656_reconciled_dt recon_dt, t2660.c2656_set_count_fl set_count_fl, t2660.c901_posting_option post_option
   FROM t2660_audit_entry_log t2660 where t2660.c2656_audit_entry_id = p_audit_entry_id order by t2660.c2660_audit_entry_log_id desc;
   
	END gm_fch_fa_entry_history;

/*******************************************************
* Description : Procedure to fetch Tag details for given tag_id
* Author  : Yogabalakrishnan
*******************************************************/
PROCEDURE gm_fch_tag_info (
        p_tag_id IN t5010_tag.c5010_tag_id%TYPE,
        p_out_loc_id OUT t5053_location_part_mapping.c5052_location_id%TYPE,
        p_out_txn_id OUT t504_consignment.c504_consignment_id%TYPE,
        p_out_txn_type OUT t504_consignment.c504_type%TYPE)
AS
    v_tag_id t5010_tag.c5010_tag_id%TYPE;
    v_plant_id t5040_plant_master.c5040_plant_id%TYPE;
    v_user_id 	NUMBER := 30301;
    v_comments	t902_log.c902_comments%TYPE;
    v_message	t902_log.c902_comments%TYPE;
    
BEGIN
SELECT  NVL(get_plantid_frm_cntx(),get_rule_value('DEFAULT_PLANT','ONEPORTAL')) INTO  v_plant_id FROM DUAL;
  SELECT t5010.c5010_tag_id, 
    t5053.c5052_location_id, 
    t5010.c5010_last_updated_trans_id, 
    get_tag_inv_type (t5010.c5010_last_updated_trans_id) 
  INTO v_tag_id, p_out_loc_id, p_out_txn_id, p_out_txn_type
  FROM t5010_tag t5010,
    t5053_location_part_mapping t5053,
    t5052_location_master t5052
   WHERE t5010.c5010_tag_id = t5053.c5010_tag_id 
    AND t5052.c5052_location_id= t5053.c5052_location_id
    AND t5010.c5010_tag_id = p_tag_id 
    AND t5010.c5010_void_fl IS NULL
    AND t5052.c5052_void_fl IS NULL
    AND t5010.c901_status = 51012 -- Active
    AND t5010.c5040_plant_id = v_plant_id
    AND t5053.c5053_curr_qty=1;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN  -- Location might not be mapped to the Tag
  BEGIN
    SELECT t5010.c5010_tag_id, 
      t5010.c5010_last_updated_trans_id, 
      get_tag_inv_type (t5010.c5010_last_updated_trans_id) 
     INTO v_tag_id, p_out_txn_id, p_out_txn_type
       FROM t5010_tag t5010
      WHERE t5010.c5010_tag_id = p_tag_id 
       AND t5010.c5010_void_fl IS NULL
       AND t5010.c901_status = 51012
       AND t5010.c5040_plant_id = v_plant_id; --- Active
     EXCEPTION
     WHEN NO_DATA_FOUND THEN
--PC-4518 Inventory device error during Pick or Putaway for certain sets     
     	v_comments:= 'Tagid: ' || p_tag_id || ' ContextPlantid: ' || get_plantid_frm_cntx() || ' plant_id: ' || v_plant_id || ' currentdate: '|| CURRENT_DATE;
		gm_update_log (p_tag_id, v_comments, 111440, v_user_id, v_message);
    BEGIN
    SELECT t5010.c5010_tag_id, 
      t5010.c5010_last_updated_trans_id, 
      get_tag_inv_type (t5010.c5010_last_updated_trans_id) 
     INTO v_tag_id, p_out_txn_id, p_out_txn_type
       FROM t5010_tag t5010
      WHERE t5010.c5010_tag_id = p_tag_id 
       AND t5010.c5010_void_fl IS NULL;
       EXCEPTION
     	WHEN NO_DATA_FOUND THEN -- Scanned Tag is invalid
        raise_application_error ( - 20501, 'Invalid Tag') ;
        END;
   END;       
END gm_fch_tag_info;

   /******************************************************************************
    * Description : Procedure to fetch Plant Details based on the Company ID
    * Author  : HREDDI
    ******************************************************************************/
    PROCEDURE gm_fch_plant_dtls (
        p_company_id 		IN t5010_tag.c1900_company_id%TYPE,     
        p_out_plant_info 	OUT TYPES.cursor_type,
        p_default_plant     OUT VARCHAR
    )
    AS
    BEGIN
	    OPEN p_out_plant_info FOR
	    	SELECT t5040.c5040_plant_id PLANTID, t5040.c5040_plant_name  plantnm
		      FROM t5041_plant_company_mapping t5041, t5040_plant_master t5040 
		     WHERE t5041.c5040_plant_id = t5040.c5040_plant_id
		       AND t5041.c1900_company_id = p_company_id
		       AND t5040.c5040_void_fl IS NULL
		       AND t5041.c5041_void_fl IS NULL;
		            
		BEGIN
		    SELECT gm_pkg_op_plant_rpt.get_default_plant_for_comp(p_company_id)
			  INTO p_default_plant
			  FROM DUAL;
		EXCEPTION WHEN NO_DATA_FOUND THEN
        		p_default_plant := '0';
   		END;		            
	END gm_fch_plant_dtls;
	   /******************************************************************************
    * Description : Procedure to validate tag ID
    * Author  : ppandiyan
    ******************************************************************************/
	PROCEDURE gm_validate_tag_id (
      	p_tagid 	  IN	   t5010_tag.c5010_tag_id%TYPE, 
        p_tag_cnt     OUT      NUMBER
    )
    AS
    v_tag_id t5010_tag.c5010_tag_id%TYPE;
    v_tagcnt NUMBER;
    
    BEGIN
	    SELECT DECODE(LENGTH(TRIM(TRANSLATE(p_tagid,'.0123456789',' '))),'',LPAD(p_tagid,7,'0'),p_tagid) INTO v_tag_id FROM DUAL;
		
	     SELECT COUNT (1) INTO v_tagcnt FROM t5010_tag WHERE c5010_tag_id = v_tag_id AND C5010_VOID_FL IS NULL;
	
	     p_tag_cnt :=  v_tagcnt;
	     
	END gm_validate_tag_id;
	
	/***********************************************************************
    * Description : Procedure used to validate the Tag details
    * Author  : mmuthusamy
    *******************************************************/   
    PROCEDURE gm_validate_edit_tag (
    p_tagid       IN t5010_tag.c5010_tag_id%TYPE,
        p_num         IN t5010_tag.c205_part_number_id%TYPE,
        p_consignedto IN t5010_tag.C5010_LOCATION_ID%TYPE,
        p_set_id      IN t5010_tag.c207_set_id%TYPE,
        p_control_num IN t5010_tag.c5010_control_number%TYPE,
        p_status      IN t5010_tag.c901_status%TYPE,
        p_ref_id      IN t5010_tag.C5010_LAST_UPDATED_TRANS_ID%TYPE,
        p_loctype     IN t5010_tag.c901_location_type%TYPE
    )
    AS
    v_cnt NUMBER;
    v_part_cnt NUMBER;
    v_set_type_cnt NUMBER;
    v_refid t5010_tag.C5010_LAST_UPDATED_TRANS_ID%TYPE;
    v_dist_type  t701_distributor.c901_distributor_type%TYPE;
    v_ship_date t504_consignment.c504_ship_date%TYPE;
    -- PMT-23439 (change tag status)
    v_tag_old_status  t5010_tag.c901_status%TYPE;
    BEGIN
	    
	    IF p_status = 51010 OR p_status = 51013 OR p_status = 102401 THEN
			raise_application_error ('-20999', 'Cannot return a transferred Tag.');
		END IF;

		IF	   p_status = 51011
		   AND (p_num IS NOT NULL
		   OR p_set_id <> '0'
		   OR p_control_num IS NOT NULL
		   OR p_ref_id IS NOT NULL
		   -- AND p_trans_type IS NOT NULL
		   OR p_loctype <> 0
		   OR p_consignedto <> 0) THEN
			raise_application_error ('-20530', '');
		END IF;

		IF	   p_status = 51012
		   AND (p_num IS NULL OR p_set_id = '0' OR p_control_num IS NULL
																		  --  OR p_trans_type IS NULL
				OR p_loctype = 0 OR p_consignedto = 0
			   ) THEN
			raise_application_error ('-20531', '');
		END IF;
-- Check if the Tag id is valid
		SELECT COUNT (1)
        INTO v_cnt
        FROM t5010_tag
       WHERE c5010_tag_id = p_tagid AND c5010_void_fl IS NULL;
       
       IF v_cnt = 0 THEN
       raise_application_error ('-20107', '');
	   END IF;
       
--Check if the Ref id is valid
		v_refid 	:= get_valid_tag_ref_id (p_ref_id);

		IF (v_refid = 0 AND p_ref_id IS NOT NULL) THEN
			raise_application_error ('-20303', '');
		END IF;

--Check for Taggable part
		IF p_num IS NOT NULL THEN
			IF (get_part_attribute_value (p_num, 92340) != 'Y') THEN
				raise_application_error ('-20305', '');
			END IF;

--Check if pnum exists in setid
		IF p_status <> '26240614' THEN
			SELECT COUNT (1)
			  INTO v_part_cnt
			  FROM t208_set_details t208
			 WHERE t208.c208_void_fl IS NULL
			   AND t208.c207_set_id = p_set_id
			   AND t208.c208_set_qty <> 0
			   AND t208.c208_inset_fl = 'Y'
			   AND t208.c205_part_number_id = p_num;

			IF (v_part_cnt = 0) THEN
				raise_application_error ('-20306', '');
			END IF;
			END IF;
		END IF;

--In-house Tag can only be consigned to Returns or Set building
		IF p_loctype = 40033 AND p_consignedto <> 52099 AND p_consignedto <> 52098 THEN
			raise_application_error ('-20532', '');
		END IF;

		SELECT COUNT (1)
		  INTO v_set_type_cnt
		  FROM t207_set_master
		 WHERE c207_type = 4074 AND c207_set_id = p_set_id;
--Validate the Ref id. If Set is Loaner then Ref id field is mandatory.	
		IF v_set_type_cnt <> 0 AND p_ref_id IS NULL THEN
		raise_application_error ('-20534', '');
		END IF;

		-- PMT-23439 : Change tag validation added
		
		BEGIN
       SELECT NVL(c901_status,'-999')
         INTO v_tag_old_status
        FROM t5010_tag
       WHERE c5010_tag_id = p_tagid;
       
       EXCEPTION
       
       WHEN OTHERS THEN
         v_tag_old_status :=NULL;
       END;

       -- Tag status other then Active - can't change to Sold status.
       -- 51012 - Active  , 26240343 - Sold
       
       IF ((v_tag_old_status <> 51012 AND p_status =  26240343 AND p_status <> v_tag_old_status))
       THEN
     
        raise_application_error ('-20061', '');

         END IF;

		-- 
		 BEGIN
	        
            SELECT t701.c901_distributor_type, c504_ship_date
            INTO v_dist_type, v_ship_date
       		 FROM t504_consignment t504, t701_distributor t701
            WHERE 
             t701.c701_distributor_id = t504.c701_distributor_id
             AND t504.c504_void_fl     IS NULL
             AND t504.c504_consignment_id=p_ref_id
             AND t701.c701_void_fl       IS NULL;
             
       EXCEPTION
       
       WHEN OTHERS THEN
          v_dist_type := NULL;
          v_ship_date := NULL;
       END;

    -- Whild update the status to sold - validate the transactions was shipped.
    -- If not shipped the transactions - can't update the status.
       
    IF p_status= 26240343 AND v_ship_date IS NULL  THEN
	-- Fetch the dist type for the CN ID provided.	
           raise_application_error ('-20951', '');
         
    END IF;
	-- Current Tag status is Sold then - can't update the status.
	-- 26240343 - Sold
	
     IF ((v_tag_old_status = 26240343 AND p_status <> 26240343)) THEN  
 
          raise_application_error ('-20962', '');

         END IF;

	    END gm_validate_edit_tag;

END gm_pkg_ac_tag_info;
/
