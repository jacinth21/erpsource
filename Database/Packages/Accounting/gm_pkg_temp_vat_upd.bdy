CREATE OR REPLACE
PACKAGE BODY gm_pkg_temp_vat_upd
IS
--
 /******************************************************************
   * Description : Procedure to fetch  gm_op_fch_invoice_source
   ****************************************************************/
	PROCEDURE gm_fch_parent_inv_date (
		p_invoiceid   IN	   t503_invoice.c503_invoice_id%TYPE
	  , p_inv_date    OUT	   VARCHAR2
	)
	AS
	BEGIN
		
		BEGIN
		SELECT to_char(c503_invoice_date,'dd/MM/yyyy')
		  INTO p_inv_date
		  FROM T503_INVOICE
		 WHERE c503_invoice_id = p_invoiceid
		   AND c503_void_fl IS NULL;
		   
		EXCEPTION WHEN OTHERS THEN
			p_inv_date := null;
		END;
		   
	END gm_fch_parent_inv_date;
END gm_pkg_temp_vat_upd;
/