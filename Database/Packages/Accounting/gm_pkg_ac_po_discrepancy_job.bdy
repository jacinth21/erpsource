/* Created on 2020/07/21 17:58*/
--@"C:\PMT\Database\Packages\Accounting\gm_pkg_ac_po_discrepancy_job.bdy";
	
CREATE OR REPLACE PACKAGE BODY gm_pkg_ac_po_discrepancy_job
IS
  	
	/******************************************************************************************************************
		  * Description : This procedure used resolve PO Discrepancy line items based on PO Amount and Po Part details
		  * Author		: Ramachandiran T.S 
   	******************************************************************************************************************/
	PROCEDURE gm_process_po_disc_dtls
	AS
		v_company_id t1900_company.c1900_company_id%TYPE;
	BEGIN
		SELECT get_compid_frm_cntx()
		  INTO v_company_id
		  FROM dual;
	--to call main procedure
		gm_pkg_ac_po_discrepancy_job.gm_process_po_disc_dtls(v_company_id);
	END gm_process_po_disc_dtls;	
	/******************************************************************************************************************
		  * Description : This procedure used to resolve PO Disc based on PO data comaprison and PO line item detail
		  * Author		: Ramachandiran T.S 
   	******************************************************************************************************************/
	
	PROCEDURE gm_process_po_disc_dtls(
			 p_company_id    IN   t1900_company.c1900_company_id%TYPE
			)
	AS
	
	BEGIN
		-- this procedure call used to reconcile PO based on data comparison
		gm_pkg_ac_po_discrepancy_job.gm_process_by_po(p_company_id);
		--this procedue call used to reconcile PO based on PO and DO line items
		gm_pkg_ac_po_discrepancy_job.gm_process_by_line_item(p_company_id);
	END gm_process_po_disc_dtls;
	
	/***************************************************************************************************
		  * Description : This procedure used to fetch PO Discrepancy details based on PO Line item Flag 
		  * Author		: Ramachandiran T.S 
	 ****************************************************************************************************/
	PROCEDURE gm_process_by_po(
			 p_company_id    IN   t1900_company.c1900_company_id%TYPE
			)
	AS
		
		CURSOR cur_line_items_by_po
	   	IS
	   		SELECT t5001.c5001_order_po_dtls_id order_po_dtls_id
	   		  FROM t5001_order_po_dtls t5001, t501_order t501
	   		 WHERE t5001.c5001_cust_po_id = t501.c501_customer_po
	   		   AND t5001.c704_account_id = t501.c704_account_id
	   		   AND t501.c501_order_id = t5001.C501_ORDER_ID
	   		   AND t501.c1900_company_id = p_company_id
	   		   AND t5001.c901_cust_po_status = 109544 -- Move to discrepancy
	   		   AND t5001.c5001_po_disc_by_line_item_fl IS NULL
	   		   AND t5001.c5001_po_disc_resolved_fl IS NULL
	   		   AND t501.c501_void_fl is NULL
	   		   AND t5001.c5001_void_fl is NULL;
	   		
	BEGIN
			  
		
			FOR po_item IN cur_line_items_by_po		  
			  LOOP
				  gm_pkg_ac_po_discrepancy_job.gm_reconcile_by_po(po_item.order_po_dtls_id,30301); --30301 user id
			  END LOOP;
		
	END gm_process_by_po;
	
	
	/*********************************************************************************************
		  * Description : This procedure used to reconcile PO based on PO line item comparison
		  * Author		: Ramachandiran T.S 
	 **********************************************************************************************/
	PROCEDURE gm_process_by_line_item(
				p_company_id    IN   t1900_company.c1900_company_id%TYPE
				)
	AS
					
			CURSOR cur_po_line_items
	   		IS
		   		SELECT t5001.c5001_cust_po_id, t5001.c704_account_id, 
  					   	RTRIM (XMLAGG (XMLELEMENT (e, c5001_order_po_dtls_id || ',')).EXTRACT ('//text()'), ',') po_dtls_ids  
                  FROM t5001_order_po_dtls t5001, t501_order t501
		   		 WHERE t5001.c5001_cust_po_id = t501.c501_customer_po
		   		   AND t5001.c704_account_id = t501.c704_account_id
                   AND t501.c501_order_id = t5001.c501_order_id
		   		   AND t501.c1900_company_id = p_company_id
		   		   AND t5001.c901_cust_po_status = 109544 -- Move to discrepancy
		   		   AND t5001.c5001_po_disc_by_line_item_fl is not null
		   		   AND t5001.c5001_po_disc_resolved_fl IS NULL
		   		   AND t501.c501_void_fl is null
		   		   AND t5001.c5001_void_fl is null
              GROUP BY t5001.c5001_cust_po_id, t5001.c704_account_id;
		  
	BEGIN   	  

			FOR po_item IN cur_po_line_items
			LOOP
						
			gm_pkg_ac_po_discrepancy_job.gm_reconcile_by_line_item(po_item.po_dtls_ids,30301); --30301 user id
			  --this procedure call used to update po disc flag in t5004 table based on part qty and price details			
			    	gm_pkg_ac_po_discrepancy_job.gm_upd_po_resolved_fl(po_item.po_dtls_ids,30301);
			
			END LOOP;
		
			
	END gm_process_by_line_item;
	/*********************************************************************************************
		  * Description : This procedure used to reconcile PO based on PO and DO amount details
		  * Author		: Ramachandiran T.S 
	 **********************************************************************************************/
	PROCEDURE gm_reconcile_by_po(
			 p_order_po_dtls_id    IN    t5001_order_po_dtls.c5001_order_po_dtls_id%TYPE,
			 p_user_id             IN    t101_user.c101_user_id%TYPE
			)
	AS
			
			v_ord_id    t501_order.c501_order_id%TYPE;
	        v_acct_id   t5001_order_po_dtls.c704_account_id%TYPE;
	        v_po_id     t501_order.c501_customer_po%TYPE;
	        v_po_amt    t501_order.c501_po_amount%TYPE;
	        v_ord_amt   t501_order.c501_total_cost%TYPE;
	        v_hold_cnt	NUMBER;
	BEGIN
			
			
		--set order po dtls id details in context 	    
	    my_context.set_my_inlist_ctx(p_order_po_dtls_id);
	    		   
	      --get hold count details by po order dtls id
	    v_hold_cnt := get_hold_count_by_po_dtls_id;		
				
				
		 IF v_hold_cnt = 0 THEN
			--fetch po,acc and po amount from t5001 table
			SELECT c5001_cust_po_id,  
				   c704_account_id,
				   c501_order_id,
				   nvl(c5001_cust_po_amt,0)
				   
		   	  INTO v_po_id,
	               v_acct_id,
		   	  	   v_ord_id,
	               v_po_amt
	                
	   		  FROM t5001_order_po_dtls
	   		 WHERE c5001_order_po_dtls_id = p_order_po_dtls_id
	   		   AND c5001_void_fl is null;
	   		   
	   		  --fetch po,acc and po amount
	   		  
	   		   v_ord_amt := gm_pkg_ar_po_trans.get_order_total_amt_by_po(v_ord_id,v_po_id);
	   		   
	   		   --check order amount and po amount to reconcile the PO
	   		   IF v_ord_amt = v_po_amt THEN
	   		   
	   		  		gm_pkg_ac_po_discrepancy_job.gm_upd_po_disc_resolved(p_order_po_dtls_id,30301);-- 30301 user id
	   		   END IF;
	   	END IF;
	   		  
	END gm_reconcile_by_po;
		
	
	/***************************************************************************************************
		  * Description : This procedure used to update PO discrepancy resolved flag based on PO details
		  * Author		: Ramachandiran T.S 
	 ***************************************************************************************************/
	PROCEDURE gm_upd_po_disc_resolved(
			 p_order_po_dtls_id    IN    t5001_order_po_dtls.c5001_order_po_dtls_id%TYPE,
			 p_user_id             IN    t101_user.c101_user_id%TYPE
			)
	AS
	BEGIN
			--update PO discrepancy resolved flag in table t5001_order_po_dtls
	        UPDATE t5001_order_po_dtls
	           SET
	               c5001_po_disc_resolved_fl = 'Y',
	               c5001_last_updated_by     = p_user_id,
	               c5001_last_updated_date   = current_date  
	        WHERE
	               c5001_order_po_dtls_id = p_order_po_dtls_id
	          AND  c5001_po_disc_resolved_fl IS NULL
	          AND  c5001_void_fl IS NULL;
	          
	END gm_upd_po_disc_resolved;
	
	
	/*********************************************************************************************
		 * Description : This procedure used to reconcile PO based on PO line items
		 * Author		: Ramachandiran T.S 
	 **********************************************************************************************/
	PROCEDURE gm_reconcile_by_line_item(
			  p_order_po_dtls_ids	IN    CLOB,
			  p_user_id             IN    t101_user.c101_user_id%TYPE
			)  	
	AS
			
	
			CURSOR cur_po_line_items_dtls
			    IS
			    	SELECT c205_po_part_number_id pnum, SUM (c5004_po_qty) qty, c5004_po_price price
   					  FROM t5004_order_po_line_item_dtls,v_in_list vlist
  					 WHERE c5001_order_po_dtls_id = vlist.token
                       AND c5004_void_fl IS NULL
                  GROUP BY c205_po_part_number_id, c5004_po_price;
			      
			v_cmp_cnt     	NUMBER;
			v_cnt         	NUMBER;
			v_hold_cnt		NUMBER;
			
	
	BEGIN
		
		--set order po dtls id details in context 	    
	    my_context.set_my_inlist_ctx(p_order_po_dtls_ids);
	      		    	    
            
	     --get hold count details by po order dtls id
	    v_hold_cnt := get_hold_count_by_po_dtls_id;
	    
	    --compare hold order details and do and po row count
	    IF v_hold_cnt = 0 THEN
	    
	    
        --to delete temp list
	     DELETE FROM MY_TEMP_KEY_VALUE;
          
        --insert po id in temp table
         gm_pkg_ac_po_discrepancy_job.gm_sav_tmp_order_item_dtls();
	  	
	     	FOR po_line_dtl IN cur_po_line_items_dtls
	     	 LOOP
	     	 
	     	 		SELECT count(1) into v_cmp_cnt
	     	 		  FROM MY_TEMP_KEY_VALUE
	     	 		 WHERE MY_TEMP_TXN_ID     = po_line_dtl.pnum
	     	 		   AND MY_TEMP_TXN_KEY    = po_line_dtl.qty
	     	 		   AND MY_TEMP_TXN_VALUE  = po_line_dtl.price;
	     	 		   	     	 		   
	     	 	IF v_cmp_cnt = 1  THEN
	     	 	-- this procedure call used to update PO resolution status
	        	gm_pkg_ac_po_discrepancy_job.gm_upd_po_res_status(po_line_dtl.pnum,30301); -- 30301 user id
	    		END IF; 
	     	 END LOOP;
	     END IF;
			
	END gm_reconcile_by_line_item;
	
	
	/*********************************************************************************************
		  * Description : This procedure used to save order details to temp table
		  * Author		: Ramachandiran T.S 
	 **********************************************************************************************/
	PROCEDURE gm_sav_tmp_order_item_dtls
	AS
	BEGIN
			--- need to check with sindhu about return result set
			INSERT INTO MY_TEMP_KEY_VALUE 
			     	(MY_TEMP_TXN_ID,MY_TEMP_TXN_KEY,MY_TEMP_TXN_VALUE) 
			     		SELECT t502.c205_part_number_id pnum, SUM (t502.c502_item_qty) qty, t502.c502_item_price price
   						  FROM t501_order t501, t502_item_order t502, t5001_order_po_dtls t5001
  						 WHERE t501.c501_order_id                                  = t502.c501_order_id
    					   AND NVL (t501.c501_parent_order_id, t501.c501_order_id) = t5001.c501_order_id
    					   AND t501.c501_customer_po = t5001.c5001_cust_po_id
    					   AND t5001.c5001_order_po_dtls_id IN
    						 (
                               SELECT TOKEN FROM V_IN_LIST
                             )
    					   AND t501.c501_void_fl   IS NULL
                           AND t502.c502_void_fl   IS NULL
                           AND t502.c502_delete_fl IS NULL
                           AND t5001.c5001_void_fl IS NULL
                      GROUP BY t502.c205_part_number_id, t502.c502_item_price;
			     	

	END gm_sav_tmp_order_item_dtls;
	
	/**********************************************************************************************************************
		  * Description : This procedure used to update the po resolution staus in t5004 table [ open to resolved status]
		  * Author		: Ramachandiran T.S 
	 ***********************************************************************************************************************/
	
	PROCEDURE  gm_upd_po_res_status(
	 			p_po_part_id     IN    t5004_order_po_line_item_dtls.C205_PO_PART_NUMBER_ID%TYPE,
	 			p_user_id        IN    t101_user.c101_user_id%TYPE
	)	
	AS
	BEGIN
		--update PO Resolutin status
				
		UPDATE t5004_order_po_line_item_dtls
           SET
               c901_po_resolution = 110181, -- 110181 Resolved Resolution type
        	   c5004_last_updated_by = p_user_id,
               c5004_last_updated_date = current_date
         WHERE c5001_order_po_dtls_id IN
    			(
                 SELECT TOKEN FROM V_IN_LIST
                )
               AND c205_po_part_number_id = p_po_part_id
               AND c5004_void_fl IS NULL;
               
	END gm_upd_po_res_status;
	
	/***************************************************************************************************************************
		  * Description : This procedure used to update the po disc resolved flag in t5001 table based on Resolution open count
		  * Author		: Ramachandiran T.S 
	 ****************************************************************************************************************************/
	
	PROCEDURE gm_upd_po_resolved_fl(
				 p_res_po_dtls_ids	    IN    CLOB,
			     p_user_id              IN    t101_user.c101_user_id%TYPE
	)
	AS
	-- cursor used to get PO dtls id's
		CURSOR cur_resolved_po_dtls_ids
		 	is
		 		SELECT token FROM v_in_list;
		 		
		 	v_open_cnt      NUMBER;
		 	v_hold_cnt		NUMBER;
		 	v_tmp_count		NUMBER;
			v_do_tot_amt	NUMBER :=0;
			v_po_tot_amt    NUMBER;
			
	
	BEGIN
			
		--set resolved po dtls id details in context 
        my_context.set_my_inlist_ctx(p_res_po_dtls_ids);
        
          --get hold count details by po order dtls id
	    v_hold_cnt := get_hold_count_by_po_dtls_id;
	    
	    
	     IF v_hold_cnt = 0 THEN
	     
	     	      -- Get DO total amount
	     		 SELECT sum(MY_TEMP_TXN_KEY * MY_TEMP_TXN_VALUE) 
	     		   INTO v_do_tot_amt 
	     		   FROM MY_TEMP_KEY_VALUE;
	     	     		   
	     		  -- Get PO total amount
	     		  SELECT nvl(c5001_cust_po_amt,0)
		   	        INTO v_po_tot_amt
	   		  	    FROM t5001_order_po_dtls
	   		 	   WHERE c5001_order_po_dtls_id IN (SELECT token FROM v_in_list)
	   		         AND c5001_void_fl is null
	   		         AND rownum = 1;

    			 
    		-- Check Open resolution count to resolve PO	 
		  		SELECT count(1) into v_open_cnt
		 		  FROM t5004_order_po_line_item_dtls t5004
		 		 WHERE t5004.c5001_order_po_dtls_id IN (SELECT token FROM v_in_list)
		 		   AND t5004.c901_po_resolution = 110180  -- 110180 - Open resolution type
		 		   AND t5004.c5004_void_fl IS NULL;
		 		   
		 		-- Check open count for line item entries and PO and DO total amount
		 		IF v_open_cnt = 0 AND v_po_tot_amt = v_do_tot_amt THEN
		 			FOR po_dtl_id IN cur_resolved_po_dtls_ids
		 			LOOP
		 				gm_pkg_ac_po_discrepancy_job.gm_upd_po_disc_resolved(po_dtl_id.token,30301); -- 30301 user id
		 			END LOOP;
		 		END IF; 	

		 END IF;		
	END gm_upd_po_resolved_fl;
		
	
/******************************************************************************************************
*  Description    : This Function is Used to get count of hold order details from given order po dtls
*  Author         : Ramachandiran T.S
*******************************************************************************************************/

    FUNCTION get_hold_count_by_po_dtls_id
    RETURN NUMBER 
    IS
        v_hold_cnt NUMBER;
    BEGIN
  --
  
       			SELECT count(1)
       			  INTO v_hold_cnt
       			  FROM t501_order t501, t5001_order_po_dtls t5001,v_in_list vlist
                 WHERE t5001.c5001_order_po_dtls_id = vlist.token
                   AND t5001.c704_account_id = t501.c704_account_id
                   AND t5001.c5001_cust_po_id = t501.c501_customer_po
                   AND t501.c501_hold_fl = 'Y'
                   AND t501.c501_void_fl is null
		   		   AND t5001.c5001_void_fl is null;         
  --

        RETURN v_hold_cnt;
  --

    END get_hold_count_by_po_dtls_id;
	
	END gm_pkg_ac_po_discrepancy_job;
	/