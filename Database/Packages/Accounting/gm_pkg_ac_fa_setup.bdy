/* Formatted on 2012/22/08 17:40 (Formatter Plus v4.8.0) */
--@"c:\database\packages\accounting\gm_pkg_ac_fa_setup.bdy"
CREATE OR REPLACE
PACKAGE BODY gm_pkg_ac_fa_setup
IS
    /****************************************************************
    * Description : Procedure to save the Set Cost as a batch
    * Author      : hreddi
    *****************************************************************/
PROCEDURE gm_ac_sav_set_costing (
        p_setinputstr IN CLOB,
        p_inputstr    IN CLOB,
        p_attr_type   IN t207c_set_attribute.c901_attribute_type%TYPE,
        p_userid      IN t207c_set_attribute. c207c_last_updated_by%TYPE,
        p_message OUT CLOB)
AS
    v_invalid_setids CLOB;
    v_attr_id t207c_set_attribute.c207c_set_attribute_id%TYPE;
    v_cost NUMBER;
    v_setid t207c_set_attribute.c207_set_id%TYPE;
    v_strlen    NUMBER           := NVL (LENGTH (p_inputstr), 0) ;
    v_string    CLOB := p_inputstr;
    v_substring VARCHAR2 (4000) ;
BEGIN
    my_context.set_my_cloblist (p_setinputstr) ;
     SELECT RTRIM (XMLAGG (XMLELEMENT (e, token || ',')) .EXTRACT ('//text()'), ',')
       INTO v_invalid_setids
       FROM v_clob_list
      WHERE TO_CHAR (token) NOT IN
        (
             SELECT C207_SET_ID FROM T207_SET_MASTER WHERE C207_VOID_FL IS NULL
        ) ;
    IF v_invalid_setids IS NOT NULL THEN
        p_message       := 'Invalid Set entries found -' || v_invalid_setids;
        RETURN;
    ELSE
        WHILE INSTR (v_string, '|') <> 0
        LOOP
            v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
            v_string    := SUBSTR (v_string, INSTR (v_string, '|')    + 1) ;
            v_setid     := NULL;
            v_cost      := NULL;
            v_setid     := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            v_substring := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
            v_cost      := v_substring ;
            GM_AC_SAV_SET_ATTRIBUTE (v_setid, p_attr_type, v_cost, p_userid) ;
        END LOOP;
        p_message := 'Cost updated successfully.';
    END IF;
END gm_ac_sav_set_costing;
/****************************************************************
* Description : Procedure to fetch the Set Cost report
* Author      : hreddi
*****************************************************************/
PROCEDURE gm_ac_fch_set_cost (
        p_attr_type IN t207c_set_attribute.c901_attribute_type%TYPE,
        p_cursor OUT TYPES.cursor_type)
AS
BEGIN
    OPEN p_cursor FOR 
    	SELECT T207C.C207_SET_ID SETID,
    		   T207.C207_SET_NM SETDES,
    		   T207C.C207C_ATTRIBUTE_VALUE COST,
    		   GET_USER_NAME (C207C_CREATED_BY) CREATEDBY,
    		   C207C_CREATED_DATE CREATEDDATE,
     		   GET_USER_NAME (C207C_LAST_UPDATED_BY) UPDATEDBY,
    		   C207C_LAST_UPDATED_DATE UPDATEDDATE,
    		   C207C_SET_HISTORY_FL HISTFL 
    	  FROM T207C_SET_ATTRIBUTE T207C, T207_SET_MASTER T207 
    	 WHERE T207C.C207C_VOID_FL IS NULL 
    	   AND T207C.C901_ATTRIBUTE_TYPE = p_attr_type --11210 (SET COST)
    	   AND T207.C207_SET_ID = T207C.C207_SET_ID 
    	   AND T207.C207_VOID_FL IS NULL 
      ORDER BY T207C.C207_SET_ID,C207C_LAST_UPDATED_DATE ;
END gm_ac_fch_set_cost;
/********************************************************************************
* Description : Procedure to save the Set Cost in T207C_SET_ATTRIBUTE table
* Author      : hreddi
*********************************************************************************/
--=
PROCEDURE gm_ac_sav_set_attribute (
        p_set_id     IN T207_SET_MASTER.C207_SET_ID%TYPE,
        p_attr_type  IN T207C_SET_ATTRIBUTE.C901_ATTRIBUTE_TYPE%TYPE,
        p_attr_value IN T207C_SET_ATTRIBUTE.C207C_ATTRIBUTE_VALUE%TYPE,
        p_user_id    IN T207C_SET_ATTRIBUTE.C207C_LAST_UPDATED_BY%TYPE)
AS
    v_attr_id T207C_SET_ATTRIBUTE.C207C_SET_ATTRIBUTE_ID%TYPE;
BEGIN
    -- UPDATING THE SET COST VALUE IN T207C_SET_ATTRIBUTE TABLE FOR PARTICULAR SET ID'S --
     UPDATE T207C_SET_ATTRIBUTE
    SET C207C_ATTRIBUTE_VALUE   = p_attr_value, C207C_LAST_UPDATED_BY = p_user_id, C207C_LAST_UPDATED_DATE = SYSDATE
      WHERE C207_SET_ID         = p_set_id
        AND C901_ATTRIBUTE_TYPE = p_attr_type
        AND C207C_VOID_FL      IS NULL;
    IF (SQL%ROWCOUNT            = 0) THEN
         SELECT s207c_set_attribute.NEXTVAL INTO v_attr_id FROM DUAL;
        --- INSERTING NEW RECORD IN T207C_SET_ATTRIBUTE TABLE AS NEW SET ID ---
         INSERT
           INTO t207c_set_attribute
            (
                c207c_set_attribute_id, c207_set_id, c207c_attribute_value
              , c901_attribute_type, c207c_created_date, c207c_created_by
            )
            VALUES
            (
                v_attr_id, p_set_id, p_attr_value
              , p_attr_type, SYSDATE, p_user_id
            ) ;
    END IF;
END gm_ac_sav_set_attribute;
/*************************************************
* Description :This procedure is called for fetching Audit Details
*************************************************/
PROCEDURE gm_fetch_audit_details
    (
        p_auditcur OUT TYPES.cursor_type
    )
AS
BEGIN
    OPEN p_auditcur FOR SELECT t2650.c2650_field_sales_audit_id ID,
    t2650.c2650_audit_name NAME FROM t2650_field_sales_audit t2650 WHERE t2650.c2650_field_sales_audit_id <> 1 AND
    t2650.c2650_void_fl                                                                                   IS NULL
    order by t2650.c2650_audit_name;
END gm_fetch_audit_details;
/*************************************************
* Description : This procedure is called for fetching auditor details
*************************************************/
PROCEDURE gm_fch_auditor_name
    (
        p_auditorcur OUT TYPES.cursor_type
    )
AS
BEGIN
    OPEN p_auditorcur FOR SELECT t2661.c101_user_id userid,
    get_user_name
    (
        t2661.c101_user_id
    )
    uname FROM t2661_audit_users t2661;
END gm_fch_auditor_name;
/*************************************************
* Description : This procedure is called for Region Details
*************************************************/
PROCEDURE gm_fch_audit_region_setup_dtls
    (
        p_region_id  IN v700_territory_mapping_detail.region_id%TYPE,
        p_auditor_id IN VARCHAR2,
        p_distcur OUT TYPES.cursor_type
    )
AS
BEGIN
    OPEN p_distcur FOR
    SELECT DISTINCT d_id id, d_name distname, get_field_sales_tag_qty
        (
            d_id
        )
        tag_qty, p_auditor_id AUDITOR
       FROM v700_territory_mapping_detail
      WHERE region_id = p_region_id
      AND D_ID            IS NOT NULL
      AND DISTTYPEID IN (70100, 70101, 70102) --Distributor, Direct Rep, Commission
   ORDER BY d_name;
END gm_fch_audit_region_setup_dtls;
/**********************************************************************
*Purpose: Procedure as fetch the audit setup details.
**********************************************************************/
PROCEDURE gm_fch_audit_setup_details
    (
        p_audit_id IN t2651_field_sales_details.c2650_field_sales_audit_id%TYPE,
        p_out_auditsetup OUT TYPES.cursor_type,
        p_out_locked_dist OUT TYPES.cursor_type,
        p_out_setids OUT VARCHAR2,
        p_out_parts OUT VARCHAR2
    )
AS
BEGIN
    OPEN p_out_auditsetup FOR SELECT c2650_field_sales_audit_id auditId,
    c2650_audit_name auditName,
    c2650_audit_owner primaryAuditorId,
    TO_CHAR
    (
        c2650_start_date, get_rule_value ('DATEFMT', 'DATEFORMAT')
    )
    auditStartDt, TO_CHAR(c2650_end_date, get_rule_value ('DATEFMT', 'DATEFORMAT')) auditEndDt,
    get_code_name
    (
        c901_audit_status
    )
    auditStatus,
    c901_audit_status auditStatusId
    FROM t2650_field_sales_audit WHERE c2650_field_sales_audit_id = p_audit_id AND c2650_void_fl IS NULL;
    OPEN p_out_locked_dist FOR SELECT t2651.c701_distributor_id id,
    get_field_sales_tag_qty
    (
        t2651.c701_distributor_id
    )
    tag_qty,
    t2658.c2658_audit_user_id auditor,
    get_code_name
    (
        t2651.c901_reconciliation_status
    )
    status_name,
    t2651.c901_reconciliation_status status,
    get_distributor_name
    (
        t2651.c701_distributor_id
    )
    dname FROM t2651_field_sales_details t2651,
    t2650_field_sales_audit t2650,
    t2658_audit_team t2658 WHERE t2650.c2650_field_sales_audit_id = t2651.c2650_field_sales_audit_id AND
    t2650.c2650_field_sales_audit_id                              = p_audit_id AND t2658.c2651_field_sales_details_id =
    t2651.c2651_field_sales_details_id AND t2650.c2650_void_fl   IS NULL order by dname;
     SELECT RTRIM (XMLAGG (XMLELEMENT (e, c2663_ref_id || ',')) .EXTRACT ('//text()'), ',')
       INTO p_out_setids
       FROM t2663_field_sales_set_part_dtl
      WHERE c2650_field_sales_audit_id = p_audit_id
        AND c901_ref_type              = 18511
        AND c2663_void_fl             IS NULL
   ORDER BY c2663_ref_id;
    IF p_audit_id IS NULL OR p_audit_id = 0 THEN
         SELECT RTRIM (XMLAGG (XMLELEMENT (e, c205_part_number_id || ',')) .EXTRACT ('//text()'), ',')
           INTO p_out_parts
           FROM t205d_part_attribute
          WHERE c901_attribute_type = 18512
            AND c205d_void_fl      IS NULL
       ORDER BY c205_part_number_id;
    ELSE
         SELECT RTRIM (XMLAGG (XMLELEMENT (e, c2663_ref_id || ',')) .EXTRACT ('//text()'), ',')
           INTO p_out_parts
           FROM t2663_field_sales_set_part_dtl
          WHERE c2650_field_sales_audit_id = p_audit_id
            AND c901_ref_type              = 18512
            AND c2663_void_fl             IS NULL
       ORDER BY c2663_ref_id;
    END IF;
END gm_fch_audit_setup_details;
/*************************************************
* Description : This procedure is called for validate the Audit name
*************************************************/
PROCEDURE gm_validate_audit_name (
        p_audit_name IN t2650_field_sales_audit.c2650_audit_name%TYPE,
        p_out_msg OUT VARCHAR2)
AS
BEGIN
     SELECT COUNT (1)
       INTO p_out_msg
       FROM t2650_field_sales_audit
      WHERE c2650_audit_name = TRIM (p_audit_name) ;
END gm_validate_audit_name;
/****************************************************
* Description : Procedure to save the field audit
*************************************************/
PROCEDURE gm_ac_sav_audit_details (
        p_audit_id        IN t2650_field_sales_audit.c2650_field_sales_audit_id%TYPE,
        p_audit_name      IN t2650_field_sales_audit.c2650_audit_name%TYPE,
        p_primary_auditor IN t2650_field_sales_audit.c2650_audit_owner%TYPE,
        p_audit_sdate     IN t2650_field_sales_audit.c2650_start_date%TYPE,
        p_dist_inputstr   IN VARCHAR2,
        p_set_inputstr    IN VARCHAR2,
        p_stropt          IN VARCHAR2,
        p_user_id         IN t2656_audit_entry.c2656_created_by%TYPE,
        p_out_auditsetup OUT TYPES.cursor_type,
        p_out_locked_dist OUT TYPES.cursor_type,
        p_out_setids OUT VARCHAR2,
        p_out_parts OUT VARCHAR2,
        p_out_invalid_set OUT VARCHAR2)
AS
    v_audit_id t2650_field_sales_audit.c2650_field_sales_audit_id%TYPE;
    v_audit_status t2650_field_sales_audit.c901_audit_status%TYPE;
BEGIN
    --
    v_audit_id := p_audit_id;
    --Proc used to save the master audit details.
    gm_pkg_ac_fa_setup.gm_sav_audit_dtls (v_audit_id, p_audit_name, p_primary_auditor, p_audit_sdate, p_user_id) ;
    --Proc used to queue the field sales details.
    gm_pkg_ac_fa_setup.gm_ac_sav_queue_field_sales (v_audit_id, p_dist_inputstr, p_stropt, p_user_id) ;
    --proc used to queue the set/part level cout
    gm_pkg_ac_fa_setup.gm_sav_set_part_setup (v_audit_id, p_set_inputstr, p_user_id, p_out_invalid_set) ;
    
    -- To call the lock procedure 
    IF p_stropt = 'LOCK' THEN
    gm_pkg_ac_fa_ld_tag.gm_ac_exec_fa_tag_job(v_audit_id);
    END IF;
    --fetch the field audit, locked field sales details
    gm_pkg_ac_fa_setup.gm_fch_audit_setup_details (v_audit_id, p_out_auditsetup, p_out_locked_dist, p_out_setids,
    p_out_parts) ;
END gm_ac_sav_audit_details;
/****************************************************
* Description : Procedure to queue the field sales details
*************************************************/
PROCEDURE gm_ac_sav_queue_field_sales (
        p_audit_id IN t2651_field_sales_details.c2650_field_sales_audit_id%TYPE,
        p_inputstr IN VARCHAR2,
        p_stropt   IN VARCHAR2,
        p_user_id  IN t101_user.c101_user_id%TYPE)
AS
    v_string    VARCHAR2 (4000) := p_inputstr;
    v_strlen    NUMBER          := NVL (LENGTH (p_inputstr), 0) ;
    v_substring VARCHAR2 (4000) ;
    v_dist_id t2658_audit_team.c2658_audit_user_id%TYPE;
    v_auditor_id VARCHAR2 (100) ;
    v_status     NUMBER;
    v_audit_date VARCHAR2 (100) ;
    v_audit_team_id t2658_audit_team.c2658_audit_team_id%TYPE;
    v_file_name VARCHAR2 (400) ;
    v_fs_count  NUMBER;
BEGIN
    IF v_strlen                      > 0 THEN
        WHILE INSTR (v_string, '|') <> 0
        LOOP
            v_substring  := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
            v_string     := SUBSTR (v_string, INSTR (v_string, '|')    + 1) ;
            v_dist_id    := NULL;
            v_auditor_id := NULL;
            v_status     := NULL;
            v_dist_id    := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            v_substring  := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
            v_auditor_id := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            v_substring  := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
            v_status     := TO_NUMBER (v_substring) ;
            IF p_stropt   = 'SAVE' AND v_status >= 18522 -- Open
                THEN
                GM_RAISE_APPLICATION_ERROR('-20999','168',get_distributor_name (v_dist_id));
            END IF;
            IF p_stropt = 'LOCK' AND v_status <> 18521 -- Draft
                THEN
                GM_RAISE_APPLICATION_ERROR('-20999','169',get_distributor_name (v_dist_id));
            END IF;
            IF (p_stropt = 'SAVE' AND (v_status IS NULL OR v_status = 18521)) -- Draft
                THEN
                v_status := 18521; -- Draft
                 SELECT COUNT (1)
                   INTO v_fs_count
                   FROM t2651_field_sales_details
                  WHERE c2650_field_sales_audit_id  = p_audit_id
                    AND c701_distributor_id         = v_dist_id
                    AND c901_reconciliation_status >= 18522 -- Open
                    AND c2651_void_fl              IS NULL ;
                IF v_fs_count                       > 0 THEN
                    GM_RAISE_APPLICATION_ERROR('-20999','168',get_distributor_name (v_dist_id));
                END IF;
                gm_pkg_ac_fa_setup.gm_ac_sav_fs_lock_dtls (v_dist_id, p_audit_id, v_auditor_id, v_status, p_stropt,
                p_user_id) ;
            ELSIF p_stropt = 'LOCK' AND v_status = 18521 --Draft
                THEN
                v_status := 18522; -- Open
                gm_pkg_ac_fa_setup.gm_ac_sav_fs_lock_dtls (v_dist_id, p_audit_id, v_auditor_id, v_status, p_stropt,
                p_user_id) ;
            END IF;
        END LOOP;
    END IF;
END gm_ac_sav_queue_field_sales;
/****************************************************************
* Proc used to save each distributor in queue
***************************************************************/
PROCEDURE gm_ac_sav_fs_lock_dtls (
        p_dist_id    IN t701_distributor.c701_distributor_id%TYPE,
        p_audit_id   IN t2650_field_sales_audit.c2650_field_sales_audit_id%TYPE,
        p_auditor_id IN t2656_audit_entry.c2656_created_by%TYPE,
        p_status     IN t2651_field_sales_details.c901_reconciliation_status%TYPE,
        p_stropt     IN VARCHAR2,
        p_user_id    IN t2656_audit_entry.c2656_created_by%TYPE)
AS
    v_fs_lock_id t2652_field_sales_lock.c2652_field_sales_lock_id%TYPE;
    v_lock_dt DATE;
BEGIN
    IF (p_stropt   = 'LOCK') THEN
        v_lock_dt := SYSDATE;
         SELECT s2652_field_sales_lock.NEXTVAL INTO v_fs_lock_id FROM DUAL;
         INSERT
           INTO t2652_field_sales_lock
            (
                c2652_field_sales_lock_id, c2650_field_sales_audit_id, c701_distributor_id
              , c2652_locked_date, c2652_locked_by
            )
            VALUES
            (
                v_fs_lock_id, p_audit_id, p_dist_id
              , v_lock_dt, p_user_id
            ) ;
    END IF;
    -- proc used to save the field sales details
    gm_pkg_ac_fa_setup.gm_sav_field_sales_dtls (p_dist_id, p_audit_id, p_status, v_fs_lock_id, p_user_id) ;
    -- proc used to save the auditor details
    gm_pkg_ac_fa_setup.gm_sav_audit_team (p_dist_id, p_audit_id, p_auditor_id, p_user_id) ;
END gm_ac_sav_fs_lock_dtls;
--
/****************************************************************
* Proc used to queue the set and part details
***************************************************************/
PROCEDURE gm_sav_set_part_setup
    (
        p_audit_id   IN t2650_field_sales_audit.c2650_field_sales_audit_id%TYPE,
        p_set_string IN VARCHAR2,
        p_user_id    IN t101_user.c101_user_id%TYPE,
        p_out_invalid_set OUT VARCHAR2
    )
AS
    v_string VARCHAR2
    (
        4000
    )
                    := p_set_string;
    v_strlen    NUMBER := NVL (LENGTH (p_set_string), 0) ;
    v_substring VARCHAR2 (4000) ;
    v_set_id    VARCHAR2 (4000) ;
    v_count     NUMBER;
    v_setid_str VARCHAR2 (4000) ;
    CURSOR set_id_cur
    IS
         SELECT token
           FROM v_in_list
          WHERE token NOT IN
            (
                 SELECT NVL (t2663.c2663_ref_id, '0')
                   FROM t2663_field_sales_set_part_dtl t2663
                  WHERE t2663.c2650_field_sales_audit_id = p_audit_id
                    AND t2663.c901_ref_type              = 18511 -- Field Audit Set level count
                    AND t2663.c2663_void_fl             IS NULL
            ) ;
    CURSOR part_numb_cur
    IS
         SELECT c205_part_number_id pnum
           FROM t205d_part_attribute
          WHERE c901_attribute_type   = 18512 -- Field Audit Part level count
            AND c205d_attribute_value = 'Y'
            AND c205d_void_fl        IS NULL;
BEGIN
    IF v_strlen                      > 0 THEN
        WHILE INSTR (v_string, ',') <> 0
        LOOP
            v_substring := SUBSTR (v_string, 1, INSTR (v_string, ',') - 1) ;
            v_string    := SUBSTR (v_string, INSTR (v_string, ',')    + 1) ;
            v_set_id    := NULL;
            v_set_id    := TRIM (v_substring) ;
             SELECT COUNT (1)
               INTO v_count
               FROM t207_set_master
              WHERE c207_set_id    = v_set_id
                AND c207_void_fl  IS NULL;
            IF v_count             = 0 THEN
                p_out_invalid_set := p_out_invalid_set || v_set_id || ',';
            ELSE
                v_setid_str := v_setid_str || v_set_id ||',';
            END IF;
        END LOOP;
    END IF;
    my_context.set_my_inlist_ctx (v_setid_str) ;
    FOR v_set IN set_id_cur
    LOOP
        IF v_set.token IS NOT NULL THEN
            gm_pkg_ac_fa_setup.gm_sav_set_dtls (p_audit_id, v_set.token, 18511, p_user_id) ;
        END IF;
    END LOOP;
     UPDATE t2663_field_sales_set_part_dtl
    SET c2663_void_fl                  = 'Y', c2663_last_updated_by = p_user_id, c2663_last_updated_date = SYSDATE
      WHERE c2650_field_sales_audit_id = p_audit_id
        AND c2663_ref_id NOT          IN
        (
             SELECT NVL (token, 0) FROM v_in_list
        )
        AND c901_ref_type  = 18511 -- Field Audit Set level count
        AND c2663_void_fl IS NULL;
    -- check the part number is entry in t2663 table
     SELECT COUNT (1)
       INTO v_count
       FROM t2663_field_sales_set_part_dtl
      WHERE c2650_field_sales_audit_id = p_audit_id
        AND c901_ref_type              = 18512 -- Field Audit Part level count
        AND c2663_void_fl             IS NULL;
    IF v_count                         = 0 THEN
        FOR v_part                    IN part_numb_cur
        LOOP
            gm_pkg_ac_fa_setup.gm_sav_part_dtls (p_audit_id, v_part.pnum, 18512, p_user_id) ;
        END LOOP;
    END IF;
END gm_sav_set_part_setup;
/****************************************************************
* Proc used to save set and part
***************************************************************/
PROCEDURE gm_ac_sav_set_part_dtls (
        p_audit_id IN t2650_field_sales_audit.c2650_field_sales_audit_id%TYPE,
        p_ref_id   IN t2663_field_sales_set_part_dtl.c2663_ref_id%TYPE,
        p_ref_type IN t2663_field_sales_set_part_dtl.c901_ref_type%TYPE,
        p_user_id  IN t101_user.c101_user_id%TYPE)
AS
    v_fs_set_dtl_id t2663_field_sales_set_part_dtl.c2663_field_sales_set_part_id%TYPE;
BEGIN
     SELECT S2663_field_sales_set_part_dtl.NEXTVAL INTO v_fs_set_dtl_id FROM dual;
     INSERT
       INTO t2663_field_sales_set_part_dtl
        (
            c2663_field_sales_set_part_id, c2650_field_sales_audit_id, c2663_ref_id
          , c901_ref_type, c2663_created_by, c2663_created_date
        )
        VALUES
        (
            v_fs_set_dtl_id, p_audit_id, p_ref_id
          , p_ref_type, p_user_id, SYSDATE
        ) ;
END gm_ac_sav_set_part_dtls;
/*******************************************************
* Purpose: This procedure will cancel the Filed audit
*******************************************************/
PROCEDURE gm_ac_cancel_audit_details
    (
        p_inputs       IN VARCHAR2,
        p_comments     IN t907_cancel_log.c907_comments%TYPE,
        p_cancelreason IN t907_cancel_log.c901_cancel_cd%TYPE,
        p_canceltype   IN t901_code_lookup.c902_code_nm_alt%TYPE,
        p_userid       IN t2651_field_sales_details.c2651_last_updated_by%TYPE,
        p_out_msg OUT VARCHAR2
    )
AS
    v_string VARCHAR2
    (
        4000
    )
                    := p_inputs;
    v_strlen    NUMBER := NVL (LENGTH (p_inputs), 0) ;
    v_substring VARCHAR2 (4000) ;
    v_dist_id t2651_field_sales_details.c701_distributor_id%TYPE;
    v_audit_id t2651_field_sales_details.c2650_field_sales_audit_id%TYPE;
    v_fa_details_id t2651_field_sales_details.c2651_field_sales_details_id%TYPE;
    v_fs_lock_id t2652_field_sales_lock.c2652_field_sales_lock_id%TYPE;
    v_count             NUMBER;
    v_dist_status_all   NUMBER;
    v_dist_status_draft NUMBER;
    v_austat_cnt        NUMBER;
BEGIN
    IF v_strlen                      > 0 THEN
        WHILE INSTR (v_string, '|') <> 0
        LOOP
            v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
            v_string    := SUBSTR (v_string, INSTR (v_string, '|')    + 1) ;
            v_dist_id   := NULL;
            v_audit_id  := NULL;
            v_audit_id  := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1) ;
            v_substring := SUBSTR (v_substring, INSTR (v_substring, '^')    + 1) ;
            v_dist_id   := v_substring ;
            -- to check the distributor reconciliation status
             SELECT COUNT (1)
               INTO v_count
               FROM t2651_field_sales_details
              WHERE c2650_field_sales_audit_id = v_audit_id
                AND c701_distributor_id        = v_dist_id
                AND c901_reconciliation_status = 18527 -- partially reconciled
                AND c2651_void_fl             IS NULL;
            IF v_count                         > 0 THEN
                GM_RAISE_APPLICATION_ERROR('-20999','170','');
            END IF;
             SELECT COUNT (1)
               INTO v_count
               FROM t2651_field_sales_details
              WHERE c2650_field_sales_audit_id = v_audit_id
                AND c701_distributor_id        = v_dist_id
                AND c2651_void_fl             IS NULL;
            IF v_count                         = 0 THEN
                GM_RAISE_APPLICATION_ERROR('-20999','171',get_distributor_name (v_dist_id));
            END IF;
            -- to save the cancel log
            gm_pkg_common_cancel.gm_sav_cancel_log ('', v_dist_id, p_comments, p_cancelreason, 55116, p_userid) ;
            -- to get the field details id
            BEGIN
                 SELECT c2651_field_sales_details_id
                   INTO v_fa_details_id
                   FROM t2651_field_sales_details
                  WHERE c2650_field_sales_audit_id = v_audit_id
                    AND c701_distributor_id        = v_dist_id
                    AND c2651_void_fl             IS NULL;
            EXCEPTION
            WHEN NO_DATA_FOUND THEN
                v_fa_details_id := NULL;
            END;
            -- to get the field lock id
            BEGIN
                 SELECT c2652_field_sales_lock_id
                   INTO v_fs_lock_id
                   FROM t2652_field_sales_lock
                  WHERE c2650_field_sales_audit_id = v_audit_id
                    AND c701_distributor_id        = v_dist_id
                    AND c2652_void_fl             IS NULL;
            EXCEPTION
            WHEN NO_DATA_FOUND THEN
                v_fs_lock_id := NULL;
            END;
            -- to void the audit entry
             UPDATE t2656_audit_entry
            SET c2656_void_fl                    = 'Y', c2656_last_updated_by = p_userid, c2656_last_updated_date = SYSDATE
              WHERE c2651_field_sales_details_id = v_fa_details_id
                AND c2656_void_fl               IS NULL;
            -- to void the audit entry log table
             UPDATE t2660_audit_entry_log
            SET c2660_void_fl                    = 'Y'
              WHERE c2651_field_sales_details_id = v_fa_details_id
                AND c2660_void_fl               IS NULL;
            -- to void the sales_details_lock
             UPDATE t2657_field_sales_details_lock
            SET c2657_void_fl                 = 'Y'
              WHERE c2652_field_sales_lock_id = v_fs_lock_id
                AND c2657_void_fl            IS NULL;
            -- to void the sales_ref_lock
             UPDATE t2659_field_sales_ref_lock
            SET c2659_void_fl                 = 'Y'
              WHERE c2652_field_sales_lock_id = v_fs_lock_id
                AND c2659_void_fl            IS NULL;
            -- to void the audit team
             UPDATE t2658_audit_team
            SET c2658_void_fl                    = 'Y', c2658_last_updated_by = p_userid, c2658_last_updated_date = SYSDATE
              WHERE c2651_field_sales_details_id = v_fa_details_id
                AND c2658_void_fl               IS NULL ;
            -- to void  t2651
             UPDATE t2651_field_sales_details
            SET c2651_void_fl                  = 'Y', c901_reconciliation_status = 18529 -- void
              , c2651_last_updated_by          = p_userid, c2651_last_updated_date = SYSDATE
              WHERE c2650_field_sales_audit_id = v_audit_id
                AND c701_distributor_id        = v_dist_id
                AND c2651_void_fl             IS NULL;
            -- to void  t2652
             UPDATE t2652_field_sales_lock
            SET c2652_void_fl                  = 'Y'
              WHERE c2650_field_sales_audit_id = v_audit_id
                AND c701_distributor_id        = v_dist_id
                AND c2652_void_fl             IS NULL;
            -- to check all distributor are void then to void the entire filed audit.
             SELECT COUNT (1)
               INTO v_count
               FROM t2651_field_sales_details
              WHERE c2650_field_sales_audit_id = v_audit_id
                AND c2651_void_fl             IS NULL;
            IF v_count                         = 0 THEN
                 UPDATE t2650_field_sales_audit
                SET c2650_void_fl                  = 'Y', c901_audit_status = 18505, -- void
                    c2650_last_updated_by          = p_userid, c2650_last_updated_date = SYSDATE
                  WHERE c2650_field_sales_audit_id = v_audit_id
                    AND c2650_void_fl             IS NULL;
            END IF; --to check the t2650 � void
        END LOOP;
        --Check Field Sales Draft status count
         SELECT COUNT (1)
           INTO v_dist_status_draft
           FROM t2651_field_sales_details
          WHERE c2650_field_sales_audit_id = v_audit_id
            AND c901_reconciliation_status = 18521 --Draft
            AND c2651_void_fl             IS NULL;
        --Check Field Sales count
         SELECT COUNT (1)
           INTO v_dist_status_all
           FROM t2651_field_sales_details
          WHERE c2650_field_sales_audit_id = v_audit_id
            AND c2651_void_fl             IS NULL;
        --Check Audit Status
         SELECT COUNT (1)
           INTO v_austat_cnt
           FROM t2650_field_sales_audit
          WHERE c2650_field_sales_audit_id = v_audit_id
            AND c901_audit_status         <> 18501-- Draft
            AND c2650_void_fl             IS NULL;
        IF v_austat_cnt                    > 0 AND v_dist_status_all = v_dist_status_draft THEN
            --Update Audit Status (18501- Draft,Locked date = null)
             UPDATE t2650_field_sales_audit
            SET c901_audit_status              = 18501, c2650_last_updated_by = p_userid
              , c2650_last_updated_date        = sysdate
              WHERE c2650_field_sales_audit_id = v_audit_id
                AND c2650_void_fl             IS NULL;
        END IF;
        p_out_msg := 'Selected Field Sales has been voided successfully';
    END IF;
END gm_ac_cancel_audit_details;
/*******************************************************************
* Description : Procedure to save the master field audit details
********************************************************************/
PROCEDURE gm_sav_audit_dtls (
        p_audit_id        IN OUT t2650_field_sales_audit.c2650_field_sales_audit_id%TYPE,
        p_audit_name      IN t2650_field_sales_audit.c2650_audit_name%TYPE,
        p_primary_auditor IN t2650_field_sales_audit.c2650_audit_owner%TYPE,
        p_audit_sdate     IN t2650_field_sales_audit.c2650_start_date%TYPE,
        p_user_id         IN t2650_field_sales_audit.c2650_created_by%TYPE)
AS
    v_audit_status t2650_field_sales_audit.c901_audit_status%TYPE;
BEGIN
    --
    v_audit_status := 18501; -- Draft
     UPDATE t2650_field_sales_audit
    SET c2650_audit_name               = p_audit_name, c2650_audit_owner = p_primary_auditor, c2650_start_date = p_audit_sdate
      , c2650_last_updated_by          = p_user_id, c2650_last_updated_date = SYSDATE
      WHERE c2650_field_sales_audit_id = p_audit_id
        AND C2650_VOID_FL             IS NULL;
    IF (SQL%ROWCOUNT                   = 0) THEN
         SELECT s2650_field_sales_audit.NEXTVAL INTO p_audit_id FROM dual;
         INSERT
           INTO t2650_field_sales_audit
            (
                c2650_field_sales_audit_id, c2650_audit_name, c2650_audit_owner
              , c2650_start_date, c2650_end_date, c2650_void_fl
              , c2650_created_by, c2650_created_date, c2650_last_updated_by
              , c2650_last_updated_date, c901_audit_status
            )
            VALUES
            (
                p_audit_id, p_audit_name, p_primary_auditor
              , p_audit_sdate, NULL, NULL
              , p_user_id, SYSDATE, NULL
              , NULL, v_audit_status
            ) ;
    END IF;
END gm_sav_audit_dtls;
/****************************************************************
* Proc used to save field sales details
***************************************************************/
PROCEDURE gm_sav_field_sales_dtls
    (
        p_dist_id    IN t701_distributor.c701_distributor_id%TYPE,
        p_audit_id   IN t2650_field_sales_audit.c2650_field_sales_audit_id%TYPE,
        p_status     IN t2651_field_sales_details.c901_reconciliation_status%TYPE,
        p_fs_lock_id IN t2652_field_sales_lock.c2652_field_sales_lock_id%TYPE,
        p_user_id    IN t2656_audit_entry.c2656_created_by%TYPE
    )
AS
    v_fs_det_id t2651_field_sales_details.c2651_field_sales_details_id%TYPE;
BEGIN
     UPDATE t2651_field_sales_details t2651
    SET t2651.c2652_field_sales_lock_id      = p_fs_lock_id, t2651.c901_reconciliation_status = p_status,
        t2651.c2651_last_updated_by          = p_user_id, t2651.c2651_last_updated_date = SYSDATE
      WHERE t2651.c701_distributor_id        = p_dist_id
        AND t2651.c2650_field_sales_audit_id = p_audit_id
        AND t2651.c2651_void_fl             IS NULL;
    IF (SQL%ROWCOUNT                         = 0) THEN
         SELECT s2651_field_sales_details.NEXTVAL INTO v_fs_det_id FROM DUAL;
         INSERT
           INTO t2651_field_sales_details
            (
                c2651_field_sales_details_id, c2650_field_sales_audit_id, c701_distributor_id
              , c2652_field_sales_lock_id, c901_reconciliation_status, c2651_start_date
              , c2651_created_by, c2651_created_date
            )
            VALUES
            (
                v_fs_det_id, p_audit_id, p_dist_id
              , p_fs_lock_id, p_status, SYSDATE
              , p_user_id, SYSDATE
            ) ;
    END IF;
END gm_sav_field_sales_dtls;
/****************************************************************
* Proc used to save auditor details
***************************************************************/
PROCEDURE gm_sav_audit_team
    (
        p_dist_id    IN t701_distributor.c701_distributor_id%TYPE,
        p_audit_id   IN t2650_field_sales_audit.c2650_field_sales_audit_id%TYPE,
        p_auditor_id IN t2656_audit_entry.c2656_created_by%TYPE,
        p_user_id    IN t2656_audit_entry.c2656_created_by%TYPE
    )
AS
    v_fs_det_id t2651_field_sales_details.c2651_field_sales_details_id%TYPE;
    v_audit_team_id t2658_audit_team.c2658_audit_team_id%TYPE;
BEGIN
    BEGIN
         SELECT t2651.c2651_field_sales_details_id
           INTO v_fs_det_id
           FROM t2651_field_sales_details t2651
          WHERE t2651.c701_distributor_id        = p_dist_id
            AND t2651.c2650_field_sales_audit_id = p_audit_id
            AND t2651.c2651_void_fl             IS NULL;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_fs_det_id := NULL;
    END;
     UPDATE t2658_audit_team
    SET c2658_audit_user_id              = p_auditor_id, c2658_last_updated_by = p_user_id, c2658_last_updated_date = SYSDATE
      WHERE c2651_field_sales_details_id = v_fs_det_id
        AND c2658_void_fl               IS NULL;
    IF (SQL%ROWCOUNT                     = 0) THEN
        --Auditors for this field sales
         SELECT s2658_audit_team.NEXTVAL
           INTO v_audit_team_id
           FROM DUAL;
         INSERT
           INTO t2658_audit_team
            (
                c2658_audit_team_id, c2651_field_sales_details_id, c2658_audit_user_id
              , c2658_created_by, c2658_created_date
            )
            VALUES
            (
                v_audit_team_id, v_fs_det_id, p_auditor_id
              , p_user_id, SYSDATE
            ) ;
    END IF;
END gm_sav_audit_team;
/****************************************************************
* Proc used to save set details
***************************************************************/
PROCEDURE gm_sav_set_dtls
    (
        p_audit_id IN t2650_field_sales_audit.c2650_field_sales_audit_id%TYPE,
        p_setid    IN t2663_field_sales_set_part_dtl.c2663_ref_id%TYPE,
        p_ref_type IN t2663_field_sales_set_part_dtl.c901_ref_type%TYPE,
        p_user_id  IN t101_user.c101_user_id%TYPE
    )
AS
BEGIN
    gm_ac_sav_set_part_dtls (p_audit_id, p_setid, p_ref_type, p_user_id) ;
END gm_sav_set_dtls;
/****************************************************************
* Proc used to save part details
***************************************************************/
PROCEDURE gm_sav_part_dtls
    (
        p_audit_id IN t2650_field_sales_audit.c2650_field_sales_audit_id%TYPE,
        p_pnum     IN t2663_field_sales_set_part_dtl.c2663_ref_id%TYPE,
        p_ref_type IN t2663_field_sales_set_part_dtl.c901_ref_type%TYPE,
        p_user_id  IN t101_user.c101_user_id%TYPE
    )
AS
BEGIN
    gm_ac_sav_set_part_dtls (p_audit_id, p_pnum, p_ref_type, p_user_id) ;
END gm_sav_part_dtls;
END gm_pkg_ac_fa_setup;
/
