/* Formatted on 2009/09/17 10:48 (Formatter Plus v4.8.0) */
--@"C:\Database\Packages\Accounting\gm_pkg_ar_process_batch.bdy";
CREATE OR REPLACE
PACKAGE BODY gm_pkg_ar_process_batch
IS
    --
/******************************************************************
* Description :  This procedure is used to create batch for Customer PO ids in AR project PMT-53738
****************************************************************/
PROCEDURE gm_add_process_batch_po_dtls (
    p_po_id        IN CLOB,
    p_company_id   IN T9600_BATCH.C1900_COMPANY_ID%TYPE,
    p_user_id      IN T9600_BATCH.C9600_CREATED_BY%TYPE,
    p_out_batch_id OUT VARCHAR2
    )
AS

CURSOR cur_cust_po_dtls
  IS
     SELECT t501.c501_order_id order_id FROM t501_order t501,t5001_order_po_dtls t5001 
			WHERE t5001.c5001_cust_po_id in ( SELECT MY_TEMP_TXN_ID FROM MY_TEMP_KEY_VALUE)
			AND t5001.c704_account_id in (SELECT MY_TEMP_TXN_KEY FROM MY_TEMP_KEY_VALUE)
			AND t501.c501_order_id = t5001.c501_order_id
			AND t5001.c5001_void_fl is null
			AND t501.c501_void_fl is null 
			AND t501.c503_invoice_id is null
			AND t501.C501_DELETE_FL is null
			AND NVL (t501.c901_order_type, - 999) NOT IN
            (
                 SELECT token FROM v_in_list
            )
			GROUP BY t501.c501_order_id;
			
v_cnt       NUMBER := 0;
v_batchid   T9600_BATCH.c9600_batch_id%TYPE;     

BEGIN

    -- Exclude Order type from Invoice Batch. (2518,2519,2535)
	my_context.set_my_inlist_ctx (get_rule_value ('INVBATEXLIST', 'INVBATEXORDERTYPE'));
	
	--set po id details in context 
    my_context.set_my_cloblist(p_po_id);
    	   
    --to delete temp list
    DELETE FROM MY_TEMP_KEY_VALUE;
    
  --insert po id in temp table
  	gm_ins_temp_po_dtls();
  
    --update status as po failed for hold po id 
    gm_validate_po_dtls(p_user_id);
    
    --delete po hold flag po id in temp table 
    gm_delete_temp_po_dtls(p_user_id);
    
     --fetch count from temp table
    SELECT COUNT(1) INTO v_cnt FROM MY_TEMP_KEY_VALUE;

    IF v_cnt <> 0 THEN
		-- update the batch status as (18742 - Pending Processing) 
		gm_pkg_cm_batch.gm_sav_batch (18751, 18742, p_user_id, v_batchid);--18751 Invoice Batch-Account

		FOR cust_po_dtl IN CUR_CUST_PO_DTLS()
		  LOOP
		  	gm_pkg_cm_batch.gm_sav_batch_details (v_batchid, cust_po_dtl.order_id, 18751, p_user_id) ;
	      END LOOP;
		  
		--update the email flag for sent the emails
	    gm_upd_batch_email_fl(v_batchid,p_user_id);
		  
		--update the status as Pending Processing
		gm_pkg_cm_batch.gm_sav_batch_status('18742',v_batchid,p_user_id);
		
		--update the status as invoice in progress 
		gm_sav_po_status(p_user_id);

    END IF;
    p_out_batch_id := v_batchid;

END gm_add_process_batch_po_dtls;

/*************************************************************************
 * Description : This procedure is insert PO ids in temp table
 *  **************************************************************************/  

PROCEDURE gm_ins_temp_po_dtls 
AS

	BEGIN
	
		INSERT INTO MY_TEMP_KEY_VALUE 
		     	(MY_TEMP_TXN_ID,MY_TEMP_TXN_KEY) 
			SELECT t5001.c5001_cust_po_id,t5001.c704_account_id FROM t5001_order_po_dtls t5001,v_clob_list cloblist
			WHERE t5001.c5001_order_po_dtls_id = cloblist.token
			AND t5001.c5001_void_fl IS NULL;
			
			
END gm_ins_temp_po_dtls;

/*************************************************************************
 * Description : This procedure is validate the hold flag for Po id's
 *  **************************************************************************/  

PROCEDURE gm_validate_po_dtls ( 
 	p_user_id      IN T9600_BATCH.C9600_CREATED_BY%TYPE)
AS
	BEGIN
		UPDATE t5001_order_po_dtls SET c901_cust_po_status=109546,c5001_last_updated_by=p_user_id,c5001_last_updated_date=CURRENT_DATE --Invoice failed-109546
		WHERE c5001_cust_po_id in (
			SELECT t5001.c5001_cust_po_id FROM t501_order t501,t5001_order_po_dtls t5001 
			WHERE t501.c501_customer_po in (SELECT MY_TEMP_TXN_ID FROM MY_TEMP_KEY_VALUE)
			AND t5001.c704_account_id in (SELECT MY_TEMP_TXN_KEY FROM MY_TEMP_KEY_VALUE)
			and t501.c501_customer_po = t5001.c5001_cust_po_id
            and t501.c704_account_id = t5001.c704_account_id
			and t501.c501_hold_fl ='Y'
			and t5001.c5001_void_fl is null
			and t501.c501_void_fl is null 
			and t501.c503_invoice_id is null
			GROUP BY t5001.c5001_cust_po_id);
END gm_validate_po_dtls;

/*************************************************************************
 * Description : This procedure is delete the hold flag po ids in temp table
 *  **************************************************************************/  

PROCEDURE gm_delete_temp_po_dtls ( 
 	p_user_id      IN T9600_BATCH.C9600_CREATED_BY%TYPE)
AS
	BEGIN
		DELETE FROM MY_TEMP_KEY_VALUE
		WHERE MY_TEMP_TXN_ID in 
			(SELECT t5001.c5001_cust_po_id FROM t501_order t501,t5001_order_po_dtls t5001 
			WHERE c5001_cust_po_id in (SELECT MY_TEMP_TXN_ID FROM MY_TEMP_KEY_VALUE)
			AND t5001.c704_account_id in (SELECT MY_TEMP_TXN_KEY FROM MY_TEMP_KEY_VALUE)
			and t501.c501_customer_po = t5001.c5001_cust_po_id
            and t501.c704_account_id = t5001.c704_account_id
			and t501.c501_hold_fl ='Y'
			and t5001.c5001_void_fl is null
			and t501.c501_void_fl is null 
			GROUP BY t5001.c5001_cust_po_id);
END gm_delete_temp_po_dtls; 

/*************************************************************************
 * Description : This procedure is used to update the po status as inprogress
 *  **************************************************************************/  

PROCEDURE gm_sav_po_status ( 
 	p_user_id      IN T9600_BATCH.C9600_CREATED_BY%TYPE)
AS
	CURSOR cur_cust_details
  	IS
		SELECT t5001.c5001_cust_po_id cust_po_id, t501.c704_account_id acc_id FROM t501_order t501,t5001_order_po_dtls t5001,MY_TEMP_KEY_VALUE temp
			WHERE t501.c501_customer_po = temp.MY_TEMP_TXN_ID
			AND t5001.c704_account_id = temp.MY_TEMP_TXN_KEY
			and t501.c501_order_id = t5001.c501_order_id
			and t501.c704_account_id = t5001.c704_account_id
			and t5001.c5001_void_fl is null
			and t501.c501_void_fl is null 
			and t501.c503_invoice_id is null
			GROUP BY t5001.c5001_cust_po_id,t501.c704_account_id;
			
	BEGIN
		FOR cust_po_dtl IN cur_cust_details()
		LOOP
			UPDATE t5001_order_po_dtls SET c901_cust_po_status='109545',c5001_last_updated_by=p_user_id,c5001_last_updated_date=CURRENT_DATE  --109545-Invoice Gen inprogress
			WHERE c5001_cust_po_id = cust_po_dtl.cust_po_id 
			AND c704_account_id = cust_po_dtl.acc_id 
			AND c5001_void_fl is null;
		END LOOP;
END gm_sav_po_status;

/*************************************************************************
 * Description : This procedure is used to update the po status as Invoiced and invoice failed for error po ids
 *  **************************************************************************/  
PROCEDURE gm_upd_batch_po_status(
	p_batch_id       IN T9600_BATCH.C9600_BATCH_ID%TYPE,
	p_Error_po_ids   IN VARCHAR2,
	p_user_id	     IN t5001_order_po_dtls.C5001_LAST_UPDATED_BY%TYPE)
AS
	v_company_id  t9600_batch.c1900_company_id%TYPE;
	CURSOR cur_custid(v_company_id t1900_company.c1900_company_id%TYPE )
  	IS
		SELECT t5001.C5001_CUST_PO_ID cust_po_id,t501.c704_account_id acc_id
			FROM t9600_batch t9600,
			t9601_batch_details t9601,
			T501_Order T501,
			T5001_Order_Po_Dtls t5001
			WHERE t9600.c9600_void_fl IS NULL
			AND t9601.c9601_void_fl IS NULL
			And T501.C501_Void_Fl Is Null
			And T501.C501_Order_Id = T5001.C501_Order_Id 
			and t5001.c5001_void_fl is null
			and t501.c503_invoice_id is not null
			AND t501.c501_order_id= t9601.c9601_ref_id
			AND t501.c704_Account_id = t5001.c704_Account_id
			AND t9600.c9600_batch_id= t9601.c9600_batch_id
			AND t9600.c9600_batch_id= p_batch_id
			And T9600.C1900_Company_Id= v_company_id
			AND NVL (T501.C901_order_type, - 9999) <> 2535
			AND NVL (c901_order_type,- 9999) NOT IN
			(
			SELECT t906.c906_rule_value
			FROM t906_rules t906
			WHERE t906.c906_rule_grp_id = 'ORDTYPE'
			And C906_Rule_Id= 'EXCLUDE'
			);
      BEGIN	
     
        --set po id details in context 
   		 my_context.set_my_cloblist(p_Error_po_ids);
    
		SELECT c1900_company_id INTO v_company_id
		FROM t9600_batch
		WHERE c9600_batch_id = p_batch_id
	    AND c9600_void_fl IS NULL;
		
		FOR cust_po IN cur_custid(v_company_id)
		LOOP
			--update the status for invoiced cus po id's
		    UPDATE t5001_order_po_dtls SET C901_CUST_PO_STATUS = '109547',C5001_LAST_UPDATED_BY=p_user_id,C5001_LAST_UPDATED_DATE=CURRENT_DATE --Invoiced
			WHERE C5001_CUST_PO_ID = cust_po.cust_po_id 
			AND c704_account_id = cust_po.acc_id
			AND c5001_void_fl is null;
	    END LOOP;
	      
	      --update the status for invoice failed cust po id's   
		   UPDATE t5001_order_po_dtls
		   SET C901_CUST_PO_STATUS = 109546,  --invoice failed
		   C5001_LAST_UPDATED_BY = p_user_id,
		   C5001_LAST_UPDATED_DATE = CURRENT_DATE
		   WHERE C5001_CUST_PO_ID in (SELECT token FROM v_clob_list where token is not null)
		   AND C901_CUST_PO_STATUS = 109545; --Invoice Gen. inprogress

END gm_upd_batch_po_status;


/*************************************************************************
 * Description : This procedure is used to get the Accounts Electronic Email status and Update into email falg column on table  
 * 			 	to send the email to batch created acccount orders 
 * Author: Sindhu 
 * PC-2077 :: Email is not received 
 *  **************************************************************************/ 
PROCEDURE gm_upd_batch_email_fl(
    p_batch_id IN T9600_BATCH.C9600_BATCH_ID%TYPE,
    p_user_id  IN t5001_order_po_dtls.C5001_LAST_UPDATED_BY%TYPE)
AS
  -- Electronic Email 91983
  CURSOR cur_batch
  IS
    SELECT t9601.C9601_BATCH_DETAIL_ID batch_dtl_id,
      GET_ACCOUNT_ATTRB_VALUE (t501.c704_account_id, 91983) email_fl
    FROM t9600_batch t9600,
      t9601_batch_details t9601,
      t501_order t501
    WHERE t9600.c9600_batch_id = t9601.c9600_batch_id
    AND t9601.C9601_REF_ID     = t501.c501_order_id
    AND t9600.c9600_void_fl   IS NULL
    AND t9601.c9601_void_fl   IS NULL
    AND t501.c501_void_fl     IS NULL
    AND t9600.c9600_batch_id   = p_batch_id;
    --
BEGIN
	--
  FOR batch_cur IN cur_batch
  LOOP
  --
    UPDATE t9601_batch_details
    SET c9601_email_fl          = batch_cur.email_fl ,
      C9601_LAST_UPDATE_BY      = p_user_id ,
      C9601_LAST_UPDATED_DATE   = CURRENT_DATE
    WHERE C9601_BATCH_DETAIL_ID = batch_cur.batch_dtl_id
    AND c9601_void_fl          IS NULL;
   --
  END LOOP;
  --
END gm_upd_batch_email_fl;


END gm_pkg_ar_process_batch;
/
