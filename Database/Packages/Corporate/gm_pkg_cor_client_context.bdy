
CREATE OR REPLACE PACKAGE BODY gm_pkg_cor_client_context
IS

		/*******************************************************
   * Description : Procedure to save client context
   *******************************************************/
   PROCEDURE gm_sav_client_context (
      p_comp_id  		IN   VARCHAR2,
      p_cmp_time_zone  	IN   VARCHAR2,
      p_cmp_date_fmt  	IN   VARCHAR2,
      p_plant_id        IN   VARCHAR2
	)   AS
   BEGIN
	   
	 gm_pkg_cor_client_context.gm_sav_client_context(p_comp_id,p_cmp_time_zone,p_cmp_date_fmt,p_plant_id,'','');
	  
   END gm_sav_client_context;


	/*******************************************************
   * Description : Procedure to save client context
   *******************************************************/
   PROCEDURE gm_sav_client_context (
      p_comp_id  		IN   VARCHAR2,
      p_cmp_time_zone  	IN   VARCHAR2,
      p_cmp_date_fmt  	IN   VARCHAR2,
      p_plant_id        IN   VARCHAR2,
      p_party_id        IN   VARCHAR2,
      p_cmp_lang_id     IN   VARCHAR2
	)   AS
   BEGIN
	   
	  my_context.set_app_double_inlist_ctx('COMPANY_ID',p_comp_id); 
	  my_context.set_app_double_inlist_ctx('COMP_TIME_ZONE',p_cmp_time_zone);
	  my_context.set_app_double_inlist_ctx('COMP_DATE_FMT',p_cmp_date_fmt); 
	  my_context.set_app_double_inlist_ctx('PLANT_ID',p_plant_id); 
	  my_context.set_app_double_inlist_ctx('PARTY_ID',p_party_id);
	  my_context.set_app_double_inlist_ctx('COMP_LANG_ID',p_cmp_lang_id);
	  
   END gm_sav_client_context;
   
   	/*******************************************************
   * Description : Procedure to save client context
   *******************************************************/
   PROCEDURE gm_sav_client_context (
      p_comp_id  		IN   VARCHAR2,
      p_plant_id        IN   VARCHAR2
	)   AS
	  v_us_time_zone t901_code_lookup.c901_code_nm%TYPE;
      v_us_time_format t901_code_lookup.c901_code_nm%TYPE;
   BEGIN
   	   
	   SELECT GET_CODE_NAME(C901_TIMEZONE), GET_CODE_NAME(C901_DATE_FORMAT)
        INTO v_us_time_zone,v_us_time_format
        FROM t1900_company
        WHERE c1900_company_id=p_comp_id;
        
	  gm_pkg_cor_client_context.gm_sav_client_context(p_comp_id,v_us_time_zone,v_us_time_format,p_plant_id);
	  
   END gm_sav_client_context;   
   
END gm_pkg_cor_client_context;
/
