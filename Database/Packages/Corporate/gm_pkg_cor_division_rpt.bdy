CREATE OR REPLACE PACKAGE BODY gm_pkg_cor_division_rpt
IS
   /*******************************************************
   * Description : Procedure to fetch Division information
   *******************************************************/

	PROCEDURE gm_fch_division (
     p_company_ID  		IN   t1911_division_company_mapping.c1900_company_id%TYPE,
     p_division_cur     OUT  TYPES.cursor_type
	)
	AS
     BEGIN
         OPEN p_division_cur FOR
      
      	     SELECT t1910.c1910_division_id division_id ,t1910.c1910_division_name division_name
			     FROM t1911_division_company_mapping t1911,t1910_division t1910
			    WHERE t1910.c1910_division_id =  t1911.c1910_division_id
			      AND t1910.c1910_void_fl IS NULL
			      AND t1911.c1911_void_fl IS NULL
			      AND t1911.c1900_company_id = p_company_ID
			      ORDER BY division_name;
      	
    END gm_fch_division;

END gm_pkg_cor_division_rpt;
/
