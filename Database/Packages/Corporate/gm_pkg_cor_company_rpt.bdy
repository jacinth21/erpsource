
CREATE OR REPLACE PACKAGE BODY gm_pkg_cor_company_rpt
IS
	/*******************************************************
   * Description : Procedure to fetch Company information
   *******************************************************/
--
	PROCEDURE gm_fch_company_info (
      p_company_id 	IN		T1900_COMPANY.C1900_COMPANY_ID%TYPE, 
      p_out  		OUT      TYPES.cursor_type
	)   AS
   BEGIN
      OPEN p_out FOR
      	SELECT C1900_COMPANY_ID COMPANYID,
		  C1900_COMPANY_CD COMPANYCD,
		  C1900_COMPANY_NAME COMPANYNM,
		  C901_TXN_CURRENCY txncurrencyid,
		  get_code_name(C901_TXN_CURRENCY) TXNCURR,
		  get_code_name(C901_REPORTING_CURRENCY) TXNRPTCURR,
		  C1900_ADDRESS_1 ADDRS1,
		  C1900_ADDRESS_2 ADDRS2,
		  C1900_CITY CITY,
		  get_code_name(C901_COUNTRY_ID) COUNTRYNM,
		  get_code_name(C901_STATE_ID) STATENM,
		  c1900_zip_code ZIPCODE,
		  get_code_name(C901_ADDRESS_FORMAT) ADDRSFMT,
		  get_code_name(C901_DATE_FORMAT) CMPDFMT,
		  get_code_name_alt(C901_DATE_FORMAT) CMPJSFMT,
		  get_code_name(C901_TIMEZONE) CMPTZONE,
		  get_code_name(C901_POSTING_REQD) POSTINGREQD,
		  get_code_name(C901_COMP_STATUS) STATUS,
		  c1900_currency_format CMPCURRFMT,
		  c1900_company_locale companylocale,
		  c901_rpt_curr_type CURRTYPE,
          c901_language_id CMPLANGID
	    FROM t1900_company 
		WHERE c1900_void_fl IS NULL
		AND C1900_COMPANY_ID = NVL(p_company_id,C1900_COMPANY_ID)
		ORDER BY NVL(C1900_COMPANY_SEQ_NO,999), C1900_COMPANY_NAME;
   END gm_fch_company_info;
   
   
   /*******************************************************
   * Description : Procedure to fetch Parent Company information
   *******************************************************/
--
	PROCEDURE gm_fch_parent_company_info (
      p_out  OUT      TYPES.cursor_type
	)   AS
   BEGIN
	   
      OPEN p_out FOR
      
      	SELECT c1900_company_id companyid,
		       c1900_company_cd companycd,
		       c1900_company_name companynm,
		       --PC-1002 - Auto Populate Company and Currency Info on Manual Adjusment
		       c901_txn_currency currencyid -- to add currency id
		  FROM t1900_company
	     WHERE c1900_parent_company_id IS NULL 
		   AND c1900_void_fl IS NULL
		   ORDER BY c1900_company_name;
		   
   END gm_fch_parent_company_info;
   /*******************************************************
   * Description : Procedure to fetch Company currency mapping
   *******************************************************/
	PROCEDURE gm_fch_comp_currency_map(
	    p_company_id IN t1900_company.c1900_company_id%TYPE,
	    p_out_cur OUT TYPES.cursor_type)
	AS
	BEGIN
		
	  OPEN p_out_cur FOR
	   SELECT t1915.C1915_COMP_CURN_MAP curr_id,
			  t1915.c1900_company_id company_id,
			  t1915.c901_code_id ID ,
			  t901.c901_code_nm NAME,
	          t901.C902_CODE_NM_ALT NAMEALT
			FROM T1915_COMPANY_CURRENCY_MAPPING t1915, t901_code_lookup t901
			WHERE c1900_company_id = p_company_id
	        AND t1915.c901_code_id = t901.c901_code_id
			AND c1915_void_fl     IS NULL
			ORDER BY NAMEALT;
		
	END gm_fch_comp_currency_map;   
	
  /*********************************************************************
   * Description : Procedure to fetch associated company mapping details
   **********************************************************************/
	PROCEDURE gm_fch_assoc_comp_mapping_dtls(
	    p_company_id IN t1900_company.c1900_company_id%TYPE,
	    p_out_cur OUT TYPES.cursor_type)
	AS
	BEGIN
		
	  OPEN p_out_cur FOR
	   SELECT T1901.C1901_Assoc_Company_Id A_COMP_ID,
		  Get_Company_Name(T1901.C1901_Assoc_Company_Id) A_COMP_NM
		FROM T1900_Company T1900,T1901_Associated_Company_Map T1901
		WHERE T1901.C1900_Company_Id = T1900.C1900_Company_Id
		AND T1901.C1900_Company_Id   = p_company_id
		AND T1900.C1900_Void_Fl     IS NULL
		AND T1901.C1901_Void_Fl     IS NULL
		ORDER BY A_COMP_NM;
		
	END gm_fch_assoc_comp_mapping_dtls;   
END gm_pkg_cor_company_rpt;
/
