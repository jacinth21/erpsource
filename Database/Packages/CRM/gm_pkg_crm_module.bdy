-- @"C:\Database\Packages\crm\gm_pkg_crm_module.bdy";

CREATE OR REPLACE
	PACKAGE BODY gm_pkg_crm_module
	IS

    /****************************************************************
    * Description 		: Get the Function Lists and the Access for the user
    * Input	Parameters	: userid, groupid (moduleid)
    * Author      		: praja
    *****************************************************************/
    
     PROCEDURE gm_crm_get_user_module (     
		p_userid			IN		t101_user.c101_user_id%type,
		p_grpnm				IN		t1500_group.c1500_group_nm%type,
		v_functionlist_cur	OUT		TYPES.cursor_type
    )AS
	    v_grpid t1500_group.c1500_group_id%type;
	    BEGIN
		    SELECT C1500_GROUP_ID into v_grpid FROM T1500_GROUP WHERE C901_GROUP_TYPE = '92265' AND C1500_GROUP_NM = p_grpnm AND C1500_VOID_FL   IS NULL;
		    OPEN v_functionlist_cur
		    FOR
		    	SELECT DISTINCT funcid, funcnm, readacc, updateacc, voidacc, seq from
				(SELECT F.C1520_FUNCTION_NM funcnm,
				  A.C1520_FUNCTION_ID funcid,
				  A.C1530_READ_ACCESS_FL readacc,
				  A.C1530_UPDATE_ACCESS_FL updateacc, 
				  A.C1530_VOID_ACCESS_FL voidacc,
				  F.C1520_SEQ_NO SEQ
				FROM T1520_FUNCTION_LIST F
				JOIN T1530_ACCESS A
				ON A.C1520_FUNCTION_ID = F.C1520_FUNCTION_ID
				WHERE A.C1530_VOID_FL IS NULL
				AND F.C1520_VOID_FL   IS NULL
				AND A.C1500_GROUP_ID  IN
				  (SELECT M.C1500_GROUP_ID
				  FROM T1501_GROUP_MAPPING M
				  WHERE M.C101_MAPPED_PARTY_ID IN
				    (SELECT C101_PARTY_ID FROM T101_USER WHERE C101_USER_ID IN (p_userid)
				    )
				  AND M.C1500_GROUP_ID IN
				    (SELECT G.C1500_MAPPED_GROUP_ID
				    FROM T1501_GROUP_MAPPING G
				    WHERE G.C1500_GROUP_ID=v_grpid
				    )
				  AND M.C1501_VOID_FL IS NULL
				  )
				AND A.C1520_FUNCTION_ID IN
				  (SELECT T1520.C1520_FUNCTION_ID
				  FROM T1530_ACCESS T1530,
				    T1500_GROUP T1500,
				    T1520_FUNCTION_LIST T1520
				  WHERE T1500.C1500_GROUP_ID  = T1530.C1500_GROUP_ID
				  AND T1530.C1520_FUNCTION_ID = T1520.C1520_FUNCTION_ID
				  AND T1500.C901_GROUP_TYPE   = '92265'
				  AND T1500.C1500_GROUP_ID    = v_grpid
				  AND T1500.C1500_VOID_FL    IS NULL
				  AND T1530.C1530_VOID_FL    IS NULL
				  AND T1520.C1520_VOID_FL    IS NULL
				  )) ORDER BY SEQ;
    

	END gm_crm_get_user_module;
	
END gm_pkg_crm_module;
/