--@"C:\projects\Branches\pmt\db\Packages\CRM\gm_pkg_crm_update.bdy";

CREATE OR REPLACE PACKAGE BODy gm_pkg_crm_update
IS
	
	
	/*******************************************************
	 *  Description :  This procedure used for web service which is used to fetch Activity Update Details 
	   for activity id.
	 *******************************************************/
	PROCEDURE gm_fch_all_activity_updates(
		p_token  			IN 		t101a_user_token.c101a_token_id%TYPE,
		p_langid			IN		NUMBER,
		p_page_no			IN		NUMBER,
	    p_uuid		        IN      t9150_device_info.c9150_device_uuid%TYPE,
		p_activitylist_cur	OUT		TYPES.CURSOR_TYPE
	) 
	AS
		v_deviceid  t9150_device_info.c9150_device_info_id%TYPE;
		v_count	    NUMBER;
	 BEGIN
		--get the device info id based on user token
		v_deviceid := get_device_id(p_token, p_uuid);
		
		
		/*below prc check the device's last updated date for Rules reftype and
		if record exist then it checks is there any Activity updated after the device updated date.
		if updated groups available then add all Contacts into v_inlist and return count of v_inlist records*/
		gm_pkg_pdpc_prodcatrpt.gm_fch_device_updates(v_deviceid,p_langid,10306170,v_count); -- 10306170 - Activity reftype
		
		IF v_count = 0 THEN
			gm_fch_activity_tosync;    
		END IF;
		gm_fch_all_activity_dtl(p_page_no,p_activitylist_cur);
		
			  --commit;
	END gm_fch_all_activity_updates;

	/*******************************************************
	   Description :  This procedure used for web service which is used to fetch Activity Update Details 
	   for activity id. 
	 *******************************************************/
	PROCEDURE gm_fch_all_activity_dtl(
		p_pageno           IN     NUMBER,
		p_activitylist_cur  OUT 	  TYPES.cursor_type
    )
    AS
    	v_page_no   NUMBER;
		v_page_size NUMBER;
	  	v_start     NUMBER;
	    v_end       NUMBER;
    BEGIN
   
		v_page_no := p_pageno;
		-- get the paging details
		gm_pkg_pdpc_prodcatrpt.gm_fch_paging_dtl('CRM','PAGESIZE',v_page_no,v_start,v_end,v_page_size);
		
		OPEN p_activitylist_cur
			FOR		
			 SELECT COUNT (1) OVER () PAGESIZE,RESULT_COUNT TOTALSIZE,V_page_no pageno, rwnum,CEIL(result_count / v_page_size) totalpages,t6630.*
			 	FROM(
					SELECT t6630.*, ROWNUM rwnum, COUNT (1) OVER () result_count
			    		FROM(
							SELECT t6630.C6630_ACTIVITY_ID actid,t6630.C901_ACTIVITY_TYPE acttype, GET_CODE_NAME(C901_ACTIVITY_TYPE) acttypenm,
								   t6630.C6630_ACTIVITY_NM actnm, NVL(temp.void_fl,t6630.c6630_void_fl) voidfl,
							       TO_CHAR(t6630.C6630_TO_DT,'MM/DD/YYYY') actenddate,t6630.c901_activity_status actstatus,
							       t6630.c6630_party_id partyid,t6630.c6630_party_nm partynm, t6630.c901_activity_category actcategory,
							       t6630.C6630_FROM_TM actstarttime, t6630.C6630_TO_TM actendtime,TO_CHAR(t6630.C6630_FROM_DT,'MM/DD/YYYY') actstartdate,
							       get_code_name(c901_activity_category) activitytype, C6630_CREATED_BY createdbyid
							FROM   t6630_activity t6630 ,my_temp_prod_cat_info temp
							WHERE  t6630.C6630_ACTIVITY_ID  = temp.ref_id
							ORDER BY t6630.C6630_ACTIVITY_NM
							)
			            t6630
			    )
			    t6630
			  WHERE RWNUM BETWEEN v_start AND v_end;
	END gm_fch_all_activity_dtl;
	
	/*****************************************************************************************************
	 * Description :  This procedure used for web service which is used to fetch ACTIVITY ID OF ALL ACTIVITIES
	 ***************************************************************************************************/
	PROCEDURE gm_fch_activity_tosync
	AS 
	BEGIN
		DELETE FROM my_temp_prod_cat_info;
		
		INSERT INTO my_temp_prod_cat_info(ref_id)
        	SELECT T6630.C6630_ACTIVITY_ID
			FROM T6630_ACTIVITY T6630
			WHERE C6630_VOID_FL IS NULL;
	END gm_fch_activity_tosync ;
	
	/*******************************************************
	 *  Description :  This procedure used for web service which is used to fetch Activity Party Update Details 
	   for activity id.
	 *******************************************************/
	PROCEDURE gm_fch_all_activityparty_updt(
		p_token  			IN 		t101a_user_token.c101a_token_id%TYPE,
		p_langid			IN		NUMBER,
		p_page_no			IN		NUMBER,
	    p_uuid		        IN      t9150_device_info.c9150_device_uuid%TYPE,
		p_activitypartylist_cur	OUT		TYPES.CURSOR_TYPE
	) 
	AS
		v_deviceid  t9150_device_info.c9150_device_info_id%TYPE;
		v_count	    NUMBER;
	 BEGIN
		--get the device info id based on user token
		v_deviceid := get_device_id(p_token, p_uuid);
		
		
		/*below prc check the device's last updated date for Rules reftype and
		if record exist then it checks is there any Activity Party data updated after the device updated date.
		if updated groups available then add all Contacts into v_inlist and return count of v_inlist records*/
		gm_pkg_pdpc_prodcatrpt.gm_fch_device_updates(v_deviceid,p_langid,10306171,v_count); -- 10306171 - Activity Party reftype
		
		IF v_count = 0 THEN
			gm_fch_activityparty_tosync;    
		END IF;
		gm_fch_all_activityparty_dtl(p_page_no,p_activitypartylist_cur);
		
			  --commit;
	END gm_fch_all_activityparty_updt;

	/*******************************************************
	   Description :  This procedure used for web service which is used to fetch Activity Party Update Details 
	   for activity id. 
	 *******************************************************/
	PROCEDURE gm_fch_all_activityparty_dtl(
		p_pageno           IN     NUMBER,
		p_activitypartylist_cur  OUT 	  TYPES.cursor_type
    )
    AS
    	v_page_no   NUMBER;
		v_page_size NUMBER;
	  	v_start     NUMBER;
	    v_end       NUMBER;
    BEGIN
   
		v_page_no := p_pageno;
		-- get the paging details
		gm_pkg_pdpc_prodcatrpt.gm_fch_paging_dtl('CRM','PAGESIZE',v_page_no,v_start,v_end,v_page_size);
		
		OPEN p_activitypartylist_cur
			FOR		
			 SELECT COUNT (1) OVER () pagesize,result_count totalsize,v_page_no pageno, rwnum,CEIL(result_count / v_page_size) totalpages,t6631.*
			 	FROM(
					SELECT t6631.*, ROWNUM rwnum, COUNT (1) OVER () result_count
			    		FROM(
							SELECT t6631.c6631_activity_party_id actpartyid, t6631.c6630_activity_id actid, t6631.c901_activity_party_role roleid,
							       NVL(temp.void_fl,t6631.c6631_void_fl) voidfl,GET_CODE_NAME(t6631.c901_activity_party_role) rolenm, 
							       t6631.c101_party_id partyid, t6631.C901_PARTY_STATUS statusid, GET_CODE_NAME(t6631.C901_PARTY_STATUS) statusnm
							FROM   t6631_activity_party_assoc t6631,my_temp_prod_cat_info temp
							WHERE  t6631.c6631_activity_party_id  = temp.ref_id
							ORDER BY t6631.c6631_activity_party_id
							)
			            t6631
			    )
			    t6631
			  WHERE RWNUM BETWEEN v_start AND v_end;
	END gm_fch_all_activityparty_dtl;
	
	/*****************************************************************************************************
	 * Description :  This procedure used for web service which is used to fetch ACTIVITY ID OF ALL ACTIVITY PARTY DATA
	 ***************************************************************************************************/
	PROCEDURE gm_fch_activityparty_tosync
	AS 
	BEGIN
		DELETE FROM my_temp_prod_cat_info;
		
		INSERT INTO my_temp_prod_cat_info(ref_id)
        	SELECT T6631.c6631_activity_party_id
			FROM t6631_activity_party_assoc t6631
			WHERE t6631.C6631_VOID_FL IS NULL;
	END gm_fch_activityparty_tosync ;
	
	/*******************************************************
	 *  Description :  This procedure used for web service which is used to fetch Activity Attribute Update Details 
	   for activity id.
	 *******************************************************/
	PROCEDURE gm_fch_all_activityattr_updt(
		p_token  			IN 		t101a_user_token.c101a_token_id%TYPE,
		p_langid			IN		NUMBER,
		p_page_no			IN		NUMBER,
	    p_uuid		        IN      t9150_device_info.c9150_device_uuid%TYPE,
		p_activityattrlist_cur	OUT		TYPES.CURSOR_TYPE
	) 
	AS
		v_deviceid  t9150_device_info.c9150_device_info_id%TYPE;
		v_count	    NUMBER;
	   
	 BEGIN
		--get the device info id based on user token
		v_deviceid := get_device_id(p_token, p_uuid);
		
		
		/*below prc check the device's last updated date for Rules reftype and
		if record exist then it checks is there any Activity Attribute updated after the device updated date.
		if updated groups available then add all Contacts into v_inlist and return count of v_inlist records*/
		gm_pkg_pdpc_prodcatrpt.gm_fch_device_updates(v_deviceid,p_langid,10306172,v_count); 
		
		IF v_count = 0 THEN
			gm_fch_activityattr_tosync;    
		END IF;
		gm_fch_all_activityattr_dtl(p_page_no,p_activityattrlist_cur);
		
			  --commit;
	END gm_fch_all_activityattr_updt;

	/*******************************************************
	   Description :  This procedure used for web service which is used to fetch Activity Attribute Update Details 
	   for activity id. 
	 *******************************************************/
	PROCEDURE gm_fch_all_activityattr_dtl(
		p_pageno           IN     NUMBER,
		p_activityattrlist_cur  OUT 	  TYPES.cursor_type
    )
    AS
    	v_page_no   NUMBER;
		v_page_size NUMBER;
	  	v_start     NUMBER;
	    v_end       NUMBER;
	    
    BEGIN
   
		v_page_no := p_pageno;
		-- get the paging details
		gm_pkg_pdpc_prodcatrpt.gm_fch_paging_dtl('CRM','PAGESIZE',v_page_no,v_start,v_end,v_page_size);
		
		OPEN p_activityattrlist_cur
			FOR		
			 SELECT COUNT (1) OVER () pagesize,result_count totalsize,v_page_no pageno, rwnum,CEIL(result_count / v_page_size) totalpages,t6632.*
			 	FROM(
					SELECT t6632.*, ROWNUM rwnum, COUNT (1) OVER () result_count
			    		FROM(
							SELECT t6632.c6632_activity_attribute_id attrid, t6632.c6630_activity_id actid, t6632.c6632_attribute_value attrval,
							       NVL(temp.void_fl,t6632.c6632_void_fl) voidfl, t6632.c6632_attribute_nm attrnm, 
							       t6632.c901_attribute_type attrtype
							FROM   t6632_activity_attribute t6632,my_temp_prod_cat_info temp
							WHERE  t6632.c6632_activity_attribute_id  = temp.ref_id
							ORDER BY t6632.c6632_activity_attribute_id
							)
			            t6632
			    )
			    t6632
			  WHERE RWNUM BETWEEN v_start AND v_end;
	END gm_fch_all_activityattr_dtl;
	
	/*****************************************************************************************************
	 * Description :  This procedure used for web service which is used to fetch ACTIVITY ID OF ALL ACTIVITY Attribute DATA
	 ***************************************************************************************************/
	PROCEDURE gm_fch_activityattr_tosync
	AS 
	BEGIN
		DELETE FROM my_temp_prod_cat_info;
		
		INSERT INTO my_temp_prod_cat_info(ref_id)
        	SELECT t6632.c6632_activity_attribute_id
			FROM t6632_activity_attribute t6632
			WHERE t6632.C6632_VOID_FL IS NULL;
	END gm_fch_activityattr_tosync ;
	
	/*******************************************************
	 *  Description :  This procedure used for web service which is used to fetch Surgeon Party Update Details 
	   for activity id.
	 *******************************************************/
	PROCEDURE gm_fch_all_surgeonparty_updt(
		p_token  			IN 		t101a_user_token.c101a_token_id%TYPE,
		p_langid			IN		NUMBER,
		p_page_no			IN		NUMBER,
	    p_uuid		        IN      t9150_device_info.c9150_device_uuid%TYPE,
		p_surgeonpartylist_cur	OUT		TYPES.CURSOR_TYPE
	) 
	AS
		v_deviceid  t9150_device_info.c9150_device_info_id%TYPE;
		v_count	    NUMBER;
	   
	 BEGIN
		--get the device info id based on user token
		v_deviceid := get_device_id(p_token, p_uuid);
		
		
		/*below prc check the device's last updated date for Rules reftype and
		if record exist then it checks is there any Activity Attribute updated after the device updated date.
		if updated groups available then add all Contacts into v_inlist and return count of v_inlist records*/
		gm_pkg_pdpc_prodcatrpt.gm_fch_device_updates(v_deviceid,p_langid,10306161,v_count); 
		
		IF v_count = 0 THEN
			gm_fch_surgeonparty_tosync;    
		END IF;
		gm_fch_all_surgeonparty_dtl(p_page_no,p_surgeonpartylist_cur);
		
			  --commit;
	END gm_fch_all_surgeonparty_updt;

	/*******************************************************
	   Description :  This procedure used for web service which is used to fetch Surgeon Party Update Details 
	   for activity id. 
	 *******************************************************/
	PROCEDURE gm_fch_all_surgeonparty_dtl(
		p_pageno           IN     NUMBER,
		p_surgeonpartylist_cur  OUT 	  TYPES.cursor_type
    )
    AS
    	v_page_no   NUMBER;
		v_page_size NUMBER;
	  	v_start     NUMBER;
	    v_end       NUMBER;
	    
    BEGIN
   
		v_page_no := p_pageno;
		-- get the paging details
		gm_pkg_pdpc_prodcatrpt.gm_fch_paging_dtl('SURGEON','PAGESIZE',v_page_no,v_start,v_end,v_page_size);
		
		OPEN p_surgeonpartylist_cur
			FOR		
			 SELECT COUNT (1) OVER () pagesize,result_count totalsize,v_page_no pageno, rwnum,CEIL(result_count / v_page_size) totalpages,T6600.*
			 	FROM(
					SELECT T6600.*, ROWNUM rwnum, COUNT (1) OVER () result_count
			    		FROM(
							SELECT T6600.C6600_SURGEON_NPI_ID npid, T6600.C101_PARTY_SURGEON_ID partyid, T6600.C6600_SURGEON_ASSISTANT_NM asstnm,
							       NVL(temp.void_fl,T6600.C6600_VOID_FL) voidfl, T6600.C6600_SURGEON_MANAGER_NM mngrnm
							FROM   T6600_PARTY_SURGEON T6600,my_temp_prod_cat_info temp
							WHERE  T6600.C6600_SURGEON_NPI_ID  = temp.ref_id
							ORDER BY T6600.C6600_SURGEON_NPI_ID
							)
			            T6600
			    )
			    T6600
			  WHERE RWNUM BETWEEN v_start AND v_end;
	END gm_fch_all_surgeonparty_dtl;
	
	/*****************************************************************************************************
	 * Description :  This procedure used for web service which is used to fetch Surgeon Party ID OF ALL Surgeon
	 ***************************************************************************************************/
	PROCEDURE gm_fch_surgeonparty_tosync
	AS 
	BEGIN
		DELETE FROM my_temp_prod_cat_info;
		
		INSERT INTO my_temp_prod_cat_info(ref_id)
        	SELECT T6600.C6600_SURGEON_NPI_ID
			FROM T6600_PARTY_SURGEON T6600
			WHERE T6600.C6600_VOID_FL IS NULL;
	END gm_fch_surgeonparty_tosync ;
	
	/*******************************************************
	 *  Description :  This procedure used for web service which is used to fetch Surgeon Attribute Update Details 
	   for activity id.
	 *******************************************************/
	PROCEDURE gm_fch_all_surgeonattr_updt(
		p_token  			IN 		t101a_user_token.c101a_token_id%TYPE,
		p_langid			IN		NUMBER,
		p_page_no			IN		NUMBER,
	    p_uuid		        IN      t9150_device_info.c9150_device_uuid%TYPE,
		p_surgeonattrlist_cur	OUT		TYPES.CURSOR_TYPE
	) 
	AS
		v_deviceid  t9150_device_info.c9150_device_info_id%TYPE;
		v_count	    NUMBER;
	   
	 BEGIN
		--get the device info id based on user token
		v_deviceid := get_device_id(p_token, p_uuid);
		
		
		/*below prc check the device's last updated date for Rules reftype and
		if record exist then it checks is there any Activity Attribute updated after the device updated date.
		if updated groups available then add all Contacts into v_inlist and return count of v_inlist records*/
		gm_pkg_pdpc_prodcatrpt.gm_fch_device_updates(v_deviceid,p_langid,10306162,v_count); 
		
		IF v_count = 0 THEN
			gm_fch_surgeonattr_tosync;    
		END IF;
		gm_fch_all_surgeonattr_dtl(p_page_no,p_surgeonattrlist_cur);
		
			  --commit;
	END gm_fch_all_surgeonattr_updt;

	/*******************************************************
	   Description :  This procedure used for web service which is used to fetch Surgeon Attribute Update Details 
	   for activity id. 
	 *******************************************************/
	PROCEDURE gm_fch_all_surgeonattr_dtl(
		p_pageno           IN     NUMBER,
		p_surgeonattrlist_cur  OUT 	  TYPES.cursor_type
    )
    AS
    	v_page_no   NUMBER;
		v_page_size NUMBER;
	  	v_start     NUMBER;
	    v_end       NUMBER;
	    v_count		NUMBER;
	    
    BEGIN
   
		v_page_no := p_pageno;
		-- get the paging details
		gm_pkg_pdpc_prodcatrpt.gm_fch_paging_dtl('SURGEON','PAGESIZE',v_page_no,v_start,v_end,v_page_size);
		
		OPEN p_surgeonattrlist_cur
			FOR		
			 SELECT COUNT (1) OVER () pagesize,result_count totalsize,v_page_no pageno, rwnum,CEIL(result_count / v_page_size) totalpages,T6600.*
			 	FROM(
					SELECT T6600.*, ROWNUM rwnum, COUNT (1) OVER () result_count
			    		FROM(
							SELECT T1012.C1012_PARTY_OTHER_INFO_ID attrid, T1012.C901_TYPE attrtype, T1012.C1012_DETAIL attrvalue,
							NVL(temp.void_fl,T1012.C1012_VOID_FL) voidfl, NVL2(LENGTH(TRIM(TRANSLATE(C1012_DETAIL, '0123456789', ' '))), 
							C1012_DETAIL, (SELECT C901_CODE_NM FROM T901_CODE_LOOKUP WHERE TO_CHAR(C901_CODE_ID) = C1012_DETAIL )) attrnm,
							C101_PARTY_ID partyid
							FROM   T1012_PARTY_OTHER_INFO T1012,my_temp_prod_cat_info temp
							WHERE  T1012.C1012_PARTY_OTHER_INFO_ID  = temp.ref_id
							ORDER BY T1012.C1012_PARTY_OTHER_INFO_ID
							)
			            T6600
			    )
			    T6600
			  WHERE RWNUM BETWEEN v_start AND v_end;
	END gm_fch_all_surgeonattr_dtl;
	
	/*****************************************************************************************************
	 * Description :  This procedure used for web service which is used to fetch Surgeon Attribute ID OF ALL Surgeon
	 ***************************************************************************************************/
	PROCEDURE gm_fch_surgeonattr_tosync
	AS 
	BEGIN
		DELETE FROM my_temp_prod_cat_info;
		
		INSERT INTO my_temp_prod_cat_info(ref_id)
        	SELECT T1012.C1012_PARTY_OTHER_INFO_ID
			FROM T1012_PARTY_OTHER_INFO T1012
			WHERE T1012.C1012_VOID_FL IS NULL;
	END gm_fch_surgeonattr_tosync ;
	
		
	/*******************************************************
	 *  Description :  This procedure used for web service which is used to fetch Surgeon Affiliation Update Details 
	   for activity id.
	 *******************************************************/
	PROCEDURE gm_fch_all_surgeonaffl_updt(
		p_token  			IN 		t101a_user_token.c101a_token_id%TYPE,
		p_langid			IN		NUMBER,
		p_page_no			IN		NUMBER,
	    p_uuid		        IN      t9150_device_info.c9150_device_uuid%TYPE,
		p_surgeonaffllist_cur	OUT		TYPES.CURSOR_TYPE
	) 
	AS
		v_deviceid  t9150_device_info.c9150_device_info_id%TYPE;
		v_count	    NUMBER;
	   
	 BEGIN
		--get the device info id based on user token
		v_deviceid := get_device_id(p_token, p_uuid);
		
		
		/*below prc check the device's last updated date for Rules reftype and
		if record exist then it checks is there any Activity Attribute updated after the device updated date.
		if updated groups available then add all Contacts into v_inlist and return count of v_inlist records*/
		gm_pkg_pdpc_prodcatrpt.gm_fch_device_updates(v_deviceid,p_langid,10306164,v_count); 
		
		IF v_count = 0 THEN
			gm_fch_surgeonaffl_tosync;    
		END IF;
		gm_fch_all_surgeonaffl_dtl(p_page_no,p_surgeonaffllist_cur);
		
			  --commit;
	END gm_fch_all_surgeonaffl_updt;

	/*******************************************************
	   Description :  This procedure used for web service which is used to fetch Surgeon Affiliation Update Details 
	   for activity id. 
	 *******************************************************/
	PROCEDURE gm_fch_all_surgeonaffl_dtl(
		p_pageno           IN     NUMBER,
		p_surgeonaffllist_cur  OUT 	  TYPES.cursor_type
    )
    AS
    	v_page_no   NUMBER;
		v_page_size NUMBER;
	  	v_start     NUMBER;
	    v_end       NUMBER;
	    
    BEGIN
   
		v_page_no := p_pageno;
		-- get the paging details
		gm_pkg_pdpc_prodcatrpt.gm_fch_paging_dtl('SURGEON','PAGESIZE',v_page_no,v_start,v_end,v_page_size);
		
		OPEN p_surgeonaffllist_cur
			FOR		
			 SELECT COUNT (1) OVER () pagesize,result_count totalsize,v_page_no pageno, rwnum,CEIL(result_count / v_page_size) totalpages,T1014.*
			 	FROM(
					SELECT T1014.*, ROWNUM rwnum, COUNT (1) OVER () result_count
			    		FROM(
							SELECT T1014.C1014_PARTY_AFFILIATION_ID afflid, T1014.C704_ACCOUNT_ID accid, T1014.C101_PARTY_ID partyid,
							NVL(temp.void_fl,T1014.C1014_VOID_FL) voidfl, T1014.C1014_ORGANIZATION_NAME accnm,
							T1014.C901_AFFILIATION_PRIORITY priority, T1014.C1014_AFFILIATION_PERCENT casepercent, T1014.C1014_CITY_NM acccity,
							GET_CODE_NAME(T1014.C901_STATE) accstate, T1014.C901_STATE accstateid, T1014.C1014_COMMENT comments
							FROM   T1014_PARTY_AFFILIATION T1014,my_temp_prod_cat_info temp
							WHERE  T1014.C1014_PARTY_AFFILIATION_ID  = temp.ref_id
							ORDER BY T1014.C1014_PARTY_AFFILIATION_ID
							)
			            T1014
			    )
			    T1014
			  WHERE RWNUM BETWEEN v_start AND v_end;
	END gm_fch_all_surgeonaffl_dtl;
	
	/*****************************************************************************************************
	 * Description :  This procedure used for web service which is used to fetch Surgeon Affiliation ID OF ALL Surgeon
	 ***************************************************************************************************/
	PROCEDURE gm_fch_surgeonaffl_tosync
	AS 
	BEGIN
		DELETE FROM my_temp_prod_cat_info;
		
		INSERT INTO my_temp_prod_cat_info(ref_id)
        	SELECT T1014.C1014_PARTY_AFFILIATION_ID
			FROM T1014_PARTY_AFFILIATION T1014
			WHERE T1014.C1014_VOID_FL IS NULL;
	END gm_fch_surgeonaffl_tosync ;
		
	/*******************************************************
	 *  Description :  This procedure used for web service which is used to fetch Surgeon Surgical Activity Update Details 
	   for activity id.
	 *******************************************************/
	PROCEDURE gm_fch_all_surgeonsurg_updt(
		p_token  			IN 		t101a_user_token.c101a_token_id%TYPE,
		p_langid			IN		NUMBER,
		p_page_no			IN		NUMBER,
	    p_uuid		        IN      t9150_device_info.c9150_device_uuid%TYPE,
		p_surgeonsurglist_cur	OUT		TYPES.CURSOR_TYPE
	) 
	AS
		v_deviceid  t9150_device_info.c9150_device_info_id%TYPE;
		v_count	    NUMBER;
	   
	 BEGIN
		--get the device info id based on user token
		v_deviceid := get_device_id(p_token, p_uuid);
		
		
		/*below prc check the device's last updated date for Rules reftype and
		if record exist then it checks is there any Activity Attribute updated after the device updated date.
		if updated groups available then add all Contacts into v_inlist and return count of v_inlist records*/
		gm_pkg_pdpc_prodcatrpt.gm_fch_device_updates(v_deviceid,p_langid,10306163,v_count); 
		
		IF v_count = 0 THEN
			gm_fch_surgeonsurg_tosync;    
		END IF;
		gm_fch_all_surgeonsurg_dtl(p_page_no,p_surgeonsurglist_cur);
		
			  --commit;
	END gm_fch_all_surgeonsurg_updt;

	/*******************************************************
	   Description :  This procedure used for web service which is used to fetch Surgeon Affiliation Update Details 
	   for activity id. 
	 *******************************************************/
	PROCEDURE gm_fch_all_surgeonsurg_dtl(
		p_pageno           IN     NUMBER,
		p_surgeonsurglist_cur  OUT 	  TYPES.cursor_type
    )
    AS
    	v_page_no   NUMBER;
		v_page_size NUMBER;
	  	v_start     NUMBER;
	    v_end       NUMBER;
	    
    BEGIN
   
		v_page_no := p_pageno;
		-- get the paging details
		gm_pkg_pdpc_prodcatrpt.gm_fch_paging_dtl('SURGEON','PAGESIZE',v_page_no,v_start,v_end,v_page_size);
		
		OPEN p_surgeonsurglist_cur
			FOR		
			 SELECT COUNT (1) OVER () pagesize,result_count totalsize,v_page_no pageno, rwnum,CEIL(result_count / v_page_size) totalpages,T6610.*
			 	FROM(
					SELECT T6610.*, ROWNUM rwnum, COUNT (1) OVER () result_count
			    		FROM(
							SELECT T6610.C901_PROCEDURE PROCEDUREID, T6610.C901_PROCEDURENM PROCEDURENM, T6610.C6610_CASES CASES,
							NVL(temp.void_fl,T6610.C6610_VOID_FL) voidfl, T6610.C207_SET_ID SYSID, T6610.C6610_NON_GLOBUS_SYSTEM SYSNM, 
							T6610.C6610_COMPANY_NM COMPANY, T6610.C6610_SURGICAL_ACTIVITY_ID SACTID, T6610.C101_PARTY_ID PARTYID, T6610.C6610_COMMENT comments
							FROM   T6610_SURGICAL_ACTIVITY T6610,my_temp_prod_cat_info temp
							WHERE  T6610.C6610_SURGICAL_ACTIVITY_ID  = temp.ref_id
							ORDER BY T6610.C6610_SURGICAL_ACTIVITY_ID
							)
			            T6610
			    )
			    T6610
			  WHERE RWNUM BETWEEN v_start AND v_end;
	END gm_fch_all_surgeonsurg_dtl;
	
	/*****************************************************************************************************
	 * Description :  This procedure used for web service which is used to fetch Surgeon Surgical Activity ID OF ALL Surgeon
	 ***************************************************************************************************/
	PROCEDURE gm_fch_surgeonsurg_tosync
	AS 
	BEGIN
		DELETE FROM my_temp_prod_cat_info;
		
		INSERT INTO my_temp_prod_cat_info(ref_id)
        	SELECT T6610.C6610_SURGICAL_ACTIVITY_ID
			FROM T6610_SURGICAL_ACTIVITY T6610
			WHERE T6610.C6610_VOID_FL IS NULL;
	END gm_fch_surgeonsurg_tosync ;
	
	/*******************************************************
	 *  Description :  This procedure used for web service which is used to fetch CRF Update Details 
	   for crf id.
	 *******************************************************/
	PROCEDURE gm_fch_all_crf_updt(
		p_token  			IN 		t101a_user_token.c101a_token_id%TYPE,
		p_langid			IN		NUMBER,
		p_page_no			IN		NUMBER,
	    p_uuid		        IN      t9150_device_info.c9150_device_uuid%TYPE,
		p_crflist_cur	OUT		TYPES.CURSOR_TYPE
	) 
	AS
		v_deviceid  t9150_device_info.c9150_device_info_id%TYPE;
		v_count	    NUMBER;
	   
	 BEGIN
		--get the device info id based on user token
		v_deviceid := get_device_id(p_token, p_uuid);
		
		/*below prc check the device's last updated date for Rules reftype and
		if record exist then it checks is there any Activity Attribute updated after the device updated date.
		if updated groups available then add all Contacts into v_inlist and return count of v_inlist records*/
		gm_pkg_pdpc_prodcatrpt.gm_fch_device_updates(v_deviceid,p_langid,10306176,v_count); 
		
		IF v_count = 0 THEN
			gm_fch_crf_tosync;    
		END IF;
		gm_fch_all_crf_dtl(p_page_no,p_crflist_cur);
		
			  --commit;
	END gm_fch_all_crf_updt;

	/*******************************************************
	   Description :  This procedure used for web service which is used to fetch CRF Update Details 
	   for crf id. 
	 *******************************************************/
	PROCEDURE gm_fch_all_crf_dtl(
		p_pageno           IN     NUMBER,
		p_crflist_cur  OUT 	  TYPES.cursor_type
    )
    AS
    	v_page_no   NUMBER;
		v_page_size NUMBER;
	  	v_start     NUMBER;
	    v_end       NUMBER;
	    
    BEGIN
   
		v_page_no := p_pageno;
		-- get the paging details
		gm_pkg_pdpc_prodcatrpt.gm_fch_paging_dtl('CRM','PAGESIZE',v_page_no,v_start,v_end,v_page_size);
		
		OPEN p_crflist_cur
			FOR		
			 SELECT COUNT (1) OVER () pagesize,result_count totalsize,v_page_no pageno, rwnum,CEIL(result_count / v_page_size) totalpages,T6605.*
			 	FROM(
					SELECT T6605.*, ROWNUM rwnum, COUNT (1) OVER () result_count
			    		FROM(
							SELECT T6605.C6605_CRF_ID crfid, T6605.C101_PARTY_ID partyid, T6605.C6630_ACTIVITY_ID actid, T6605.C901_REQUEST_TYPE reqtypeid , T6605.C6605_SERVICE_TYPE servicetyp,
						    T6605.C6605_REASON reason, T6605.C6605_SERVICE_DESCRIPTION servicedesc , T6605.C6605_HCP_QUALIFICATION hcpqualification, T6605.C6605_PREP_TIME preptime, T6605.C6605_WORK_HOURS workhrs,
						    T6605.C6605_TIME_FL timefl, T6605.C6605_AGREEMENT_FL agrfl, T6605.C6605_TRAVEL_FROM trvlfrom, T6605.C6605_TRAVEL_TO trvlto, T6605.C6605_TRAVEL_TIME trvltime, T6605.C6605_TRAVEL_MILES trvlmile,
						    T6605.C6605_HOUR_RATE hrrate, NVL(temp.void_fl,T6605.C6605_VOID_FL) voidfl, T6605.C6605_CREATED_BY createdby, T6605.C901_STATUS statusid, T6605.C6605_REQUEST_DATE reqdate, T6605.C6605_HONORARIA_STATUS honstatus, 
							T6605.C6605_HONORARIA_WORK_HRS honworkhrs, T6605.C6605_HONORARIA_PREP_HRS honprephrs, T6605.C6605_HONORARIA_COMMENT honcomment, C6605_PRIORITY_FL priorityfl  
							FROM   T6605_CRF T6605,my_temp_prod_cat_info temp
							WHERE  T6605.C6605_CRF_ID  = temp.ref_id
							ORDER BY T6605.C6605_CRF_ID
							)
			            T6605
			    )
			    T6605
			  WHERE RWNUM BETWEEN v_start AND v_end;
	END gm_fch_all_crf_dtl;
	
	/*****************************************************************************************************
	 * Description :  This procedure used for web service which is used to fetch CRF ID OF ALL CRFs
	 ***************************************************************************************************/
	PROCEDURE gm_fch_crf_tosync
	AS 
	BEGIN
		DELETE FROM my_temp_prod_cat_info;
		
		INSERT INTO my_temp_prod_cat_info(ref_id)
        	SELECT T6605.C6605_CRF_ID
			FROM T6605_CRF T6605
			WHERE T6605.C6605_VOID_FL IS NULL;
	END gm_fch_crf_tosync ;
	
	/*******************************************************
	 *  Description :  This procedure used for web service which is used to fetch CRF Approver Update Details 
	   for acr approver id.
	 *******************************************************/
	PROCEDURE gm_fch_all_crfapprv_updt(
		p_token  			IN 		t101a_user_token.c101a_token_id%TYPE,
		p_langid			IN		NUMBER,
		p_page_no			IN		NUMBER,
	    p_uuid		        IN      t9150_device_info.c9150_device_uuid%TYPE,
		p_crfapprvlist_cur	OUT		TYPES.CURSOR_TYPE
	) 
	AS
		v_deviceid  t9150_device_info.c9150_device_info_id%TYPE;
		v_count	    NUMBER;
	   
	 BEGIN
		--get the device info id based on user token
		v_deviceid := get_device_id(p_token, p_uuid);
		
		
		/*below prc check the device's last updated date for Rules reftype and
		if record exist then it checks is there any Activity Attribute updated after the device updated date.
		if updated groups available then add all Contacts into v_inlist and return count of v_inlist records*/
		gm_pkg_pdpc_prodcatrpt.gm_fch_device_updates(v_deviceid,p_langid,10306206,v_count); 
		
		IF v_count = 0 THEN
			gm_fch_crfapprv_tosync;    
		END IF;
		gm_fch_all_crfapprv_dtl(p_page_no,p_crfapprvlist_cur);
		
			  --commit;
	END gm_fch_all_crfapprv_updt;

	/*******************************************************
	   Description :  This procedure used for web service which is used to fetch CRF Approver Update Details 
	   for crf approver id. 
	 *******************************************************/
	PROCEDURE gm_fch_all_crfapprv_dtl(
		p_pageno           IN     NUMBER,
		p_crfapprvlist_cur  OUT 	  TYPES.cursor_type
    )
    AS
    	v_page_no   NUMBER;
		v_page_size NUMBER;
	  	v_start     NUMBER;
	    v_end       NUMBER;
	    
    BEGIN
   
		v_page_no := p_pageno;
		-- get the paging details
		gm_pkg_pdpc_prodcatrpt.gm_fch_paging_dtl('CRM','PAGESIZE',v_page_no,v_start,v_end,v_page_size);
		
		OPEN p_crfapprvlist_cur
			FOR		
			 SELECT COUNT (1) OVER () pagesize,result_count totalsize,v_page_no pageno, rwnum,CEIL(result_count / v_page_size) totalpages,T6605A.*
			 	FROM(
					SELECT T6605A.*, ROWNUM rwnum, COUNT (1) OVER () result_count
			    		FROM(
							SELECT T6605A.c6605a_crf_approval_id crfapprid, T6605A.c6605_crf_id CRFID,
							T6605A.c101_party_id PARTYID, T6605A.c901_status STATUSID, T6605A.c6605a_comments COMMENTS FROM T6605A_CRF_APPROVAL T6605A,
							my_temp_prod_cat_info temp
							WHERE  T6605A.c6605a_crf_approval_id  = temp.ref_id
							ORDER BY T6605A.c6605a_crf_approval_id
							)
			            T6605A
			    )
			    T6605A
			  WHERE RWNUM BETWEEN v_start AND v_end;
	END gm_fch_all_crfapprv_dtl;
	
	/*****************************************************************************************************
	 * Description :  This procedure used for web service which is used to fetch CRF Approver ID OF ALL CRF Approvals
	 ***************************************************************************************************/
	PROCEDURE gm_fch_crfapprv_tosync
	AS 
	BEGIN
		DELETE FROM my_temp_prod_cat_info;
		
		INSERT INTO my_temp_prod_cat_info(ref_id)
        	SELECT T6605A.c6605a_crf_approval_id
			FROM T6605A_CRF_APPROVAL T6605A;
	END gm_fch_crfapprv_tosync ;
	
	/*******************************************************
	 *  Description :  This procedure used for web service which is used to fetch Criteria Update Details 
	   for criteria id.
	 *******************************************************/
	PROCEDURE gm_fch_all_criteria_updt(
		p_token  			IN 		t101a_user_token.c101a_token_id%TYPE,
		p_langid			IN		NUMBER,
		p_page_no			IN		NUMBER,
	    p_uuid		        IN      t9150_device_info.c9150_device_uuid%TYPE,
		p_criterialist_cur	OUT		TYPES.CURSOR_TYPE
	) 
	AS
		v_deviceid  t9150_device_info.c9150_device_info_id%TYPE;
		v_count	    NUMBER;
	   
	 BEGIN
		--get the device info id based on user token
		v_deviceid := get_device_id(p_token, p_uuid);
		
		
		/*below prc check the device's last updated date for Rules reftype and
		if record exist then it checks is there any Activity Attribute updated after the device updated date.
		if updated groups available then add all Contacts into v_inlist and return count of v_inlist records*/
		gm_pkg_pdpc_prodcatrpt.gm_fch_device_updates(v_deviceid,p_langid,10306210,v_count); 
		
		IF v_count = 0 THEN
			gm_fch_criteria_tosync;    
		END IF;
		gm_fch_all_criteria_dtl(p_page_no,p_criterialist_cur);
		
			  --commit;
	END gm_fch_all_criteria_updt;

	/*******************************************************
	   Description :  This procedure used for web service which is used to fetch Criteria Update Details 
	   for criteria id. 
	 *******************************************************/
	PROCEDURE gm_fch_all_criteria_dtl(
		p_pageno           IN     NUMBER,
		p_criterialist_cur  OUT 	  TYPES.cursor_type
    )
    AS
    	v_page_no   NUMBER;
		v_page_size NUMBER;
	  	v_start     NUMBER;
	    v_end       NUMBER;
	    
    BEGIN
   
		v_page_no := p_pageno;
		-- get the paging details
		gm_pkg_pdpc_prodcatrpt.gm_fch_paging_dtl('CRM','PAGESIZE',v_page_no,v_start,v_end,v_page_size);
		
		OPEN p_criterialist_cur
			FOR		
			 SELECT COUNT (1) OVER () pagesize,result_count totalsize,v_page_no pageno, rwnum,CEIL(result_count / v_page_size) totalpages,T1101.*
			 	FROM(
					SELECT T1101.*, ROWNUM rwnum, COUNT (1) OVER () result_count
			    		FROM(
							SELECT T1101.C1101_CRITERIA_ID criteriaid, T1101.C1101_CRITERIA_NAME criterianm,
							T1101.C2201_MODULE_ID criteriatyp, T1101.C101_USER_ID userid,  NVL(temp.void_fl,T1101.C1101_VOID_FL) voidfl FROM T1101_CRITERIA T1101,
							my_temp_prod_cat_info temp
							WHERE  T1101.C1101_CRITERIA_ID  = temp.ref_id
							ORDER BY T1101.C1101_CRITERIA_ID
							)
			            T1101
			    )
			    T1101
			  WHERE RWNUM BETWEEN v_start AND v_end;
	END gm_fch_all_criteria_dtl;
	
	/*****************************************************************************************************
	 * Description :  This procedure used for web service which is used to fetch criteria ID OF ALL criteria
	 ***************************************************************************************************/
	PROCEDURE gm_fch_criteria_tosync
	AS 
	BEGIN
		DELETE FROM my_temp_prod_cat_info;
		
		INSERT INTO my_temp_prod_cat_info(ref_id)
        	SELECT T1101.C1101_CRITERIA_ID
			FROM T1101_CRITERIA T1101 WHERE T1101.C1101_VOID_FL IS NULL ;
	END gm_fch_criteria_tosync ;
	
	/*******************************************************
	 *  Description :  This procedure used for web service which is used to fetch Criteria Attr Update Details 
	   for criteria attr id.
	 *******************************************************/
	PROCEDURE gm_fch_all_critattr_updt(
		p_token  			IN 		t101a_user_token.c101a_token_id%TYPE,
		p_langid			IN		NUMBER,
		p_page_no			IN		NUMBER,
	    p_uuid		        IN      t9150_device_info.c9150_device_uuid%TYPE,
		p_critattrlist_cur	OUT		TYPES.CURSOR_TYPE
	) 
	AS
		v_deviceid  t9150_device_info.c9150_device_info_id%TYPE;
		v_count	    NUMBER;
	   
	 BEGIN
		--get the device info id based on user token
		v_deviceid := get_device_id(p_token, p_uuid);
		
		
		/*below prc check the device's last updated date for Rules reftype and
		if record exist then it checks is there any Activity Attribute updated after the device updated date.
		if updated groups available then add all Contacts into v_inlist and return count of v_inlist records*/
		gm_pkg_pdpc_prodcatrpt.gm_fch_device_updates(v_deviceid,p_langid,10306209,v_count); 
		
		IF v_count = 0 THEN
			gm_fch_critattr_tosync;    
		END IF;
		gm_fch_all_critattr_dtl(p_page_no,p_critattrlist_cur);
		
			  --commit;
	END gm_fch_all_critattr_updt;

	/*******************************************************
	   Description :  This procedure used for web service which is used to fetch Criteria Attr Update Details 
	   for criteria attr id. 
	 *******************************************************/
	PROCEDURE gm_fch_all_critattr_dtl(
		p_pageno           IN     NUMBER,
		p_critattrlist_cur  OUT 	  TYPES.cursor_type
    )
    AS
    	v_page_no   NUMBER;
		v_page_size NUMBER;
	  	v_start     NUMBER;
	    v_end       NUMBER;
	    
    BEGIN
   
		v_page_no := p_pageno;
		-- get the paging details
		gm_pkg_pdpc_prodcatrpt.gm_fch_paging_dtl('CRM','PAGESIZE',v_page_no,v_start,v_end,v_page_size);
		
		OPEN p_critattrlist_cur
			FOR		
			 SELECT COUNT (1) OVER () pagesize,result_count totalsize,v_page_no pageno, rwnum,CEIL(result_count / v_page_size) totalpages,T1101.*
			 	FROM(
					SELECT T1101.*, ROWNUM rwnum, COUNT (1) OVER () result_count
			    		FROM(
							SELECT T1101A.C1101A_CRITERIA_ATTRIBUTE_ID attrid, T1101A.C1101_CRITERIA_ID criteriaid, T1101A.C1101A_CRITERIA_KEY key,
							T1101A.C1101A_CRITERIA_VALUE value, NVL(temp.void_fl,T1101A.C1101A_VOID_FL) voidfl FROM T1101A_CRITERIA_ATTRIBUTE T1101A,
							my_temp_prod_cat_info temp
							WHERE  TO_CHAR(t1101A.C1101A_CRITERIA_ATTRIBUTE_ID)  = temp.ref_id
							ORDER BY T1101A.C1101A_CRITERIA_ATTRIBUTE_ID
							)
			            T1101
			    )
			    T1101
			  WHERE RWNUM BETWEEN v_start AND v_end;
	END gm_fch_all_critattr_dtl;
	
	/*****************************************************************************************************
	 * Description :  This procedure used for web service which is used to fetch criteria Attr ID OF ALL criteria attr
	 ***************************************************************************************************/
	PROCEDURE gm_fch_critattr_tosync
	AS 
	BEGIN
		DELETE FROM my_temp_prod_cat_info;
		
		INSERT INTO my_temp_prod_cat_info(ref_id)
        	SELECT T1101A.C1101A_CRITERIA_ATTRIBUTE_ID
			FROM T1101A_CRITERIA_ATTRIBUTE T1101A WHERE T1101A.C1101A_VOID_FL IS NULL ;
	END gm_fch_critattr_tosync ;
	
	/*******************************************************
	 *  Description :  This procedure used for web service which is used to fetch Project Update Details 
	   for Project id.
	 *******************************************************/
	PROCEDURE gm_fch_all_prj_updt(
		p_token  			IN 		t101a_user_token.c101a_token_id%TYPE,
		p_langid			IN		NUMBER,
		p_page_no			IN		NUMBER,
	    p_uuid		        IN      t9150_device_info.c9150_device_uuid%TYPE,
		p_prjlist_cur	    OUT		TYPES.CURSOR_TYPE
	) 
	AS
		v_deviceid  t9150_device_info.c9150_device_info_id%TYPE;
		v_count	    NUMBER;
	   
	 BEGIN
		--get the device info id based on user token
		v_deviceid := get_device_id(p_token, p_uuid);
		
		
		/*below prc check the device's last updated date for Rules reftype and
		if record exist then it checks is there any prj updated after the device updated date.
		if updated groups available then add all Contacts into v_inlist and return count of v_inlist records*/
		gm_pkg_pdpc_prodcatrpt.gm_fch_device_updates(v_deviceid,p_langid,103109,v_count); 
		
		IF v_count = 0 THEN
			gm_fch_prj_tosync;    
		END IF;
		gm_fch_all_prj_dtl(p_page_no,p_prjlist_cur);
		
			  --commit;
	END gm_fch_all_prj_updt;

	/*******************************************************
	   Description :  This procedure used for web service which is used to fetch Project Update Details 
	   for prj id. 
	 *******************************************************/
	PROCEDURE gm_fch_all_prj_dtl(
		p_pageno           IN     NUMBER,
		p_prjlist_cur 	   OUT 	  TYPES.cursor_type
    )
    AS
    	v_page_no   NUMBER;
		v_page_size NUMBER;
	  	v_start     NUMBER;
	    v_end       NUMBER;
	    
    BEGIN
   
		v_page_no := p_pageno;
		-- get the paging details
		gm_pkg_pdpc_prodcatrpt.gm_fch_paging_dtl('CRM','PAGESIZE',v_page_no,v_start,v_end,v_page_size);
		
		OPEN p_prjlist_cur
			FOR		
			 SELECT COUNT (1) OVER () pagesize,result_count totalsize,v_page_no pageno, rwnum,CEIL(result_count / v_page_size) totalpages,T202.*
			 	FROM(
					SELECT T202.*, ROWNUM rwnum, COUNT (1) OVER () result_count
			    		FROM(
							SELECT T202.C202_PROJECT_ID projectid, T202.C202_PROJECT_NM projectname,
							NVL(temp.void_fl,T202.C202_VOID_FL) voidfl FROM T202_PROJECT T202,
							my_temp_prod_cat_info temp
							WHERE  T202.C202_PROJECT_ID  = temp.ref_id
							ORDER BY T202.C202_PROJECT_ID
							)
			            T202
			    )
			    T202
			  WHERE RWNUM BETWEEN v_start AND v_end;
	END gm_fch_all_prj_dtl;
	
	/*****************************************************************************************************
	 * Description :  This procedure used for web service which is used to fetch prj ID OF ALL prj
	 ***************************************************************************************************/
	PROCEDURE gm_fch_prj_tosync
	AS 
	BEGIN
		DELETE FROM my_temp_prod_cat_info;
		
		INSERT INTO my_temp_prod_cat_info(ref_id)
        	SELECT T202.C202_PROJECT_ID
			FROM T202_PROJECT T202 WHERE T202.C202_VOID_FL IS NULL ;
	END gm_fch_prj_tosync ;
	
	/****************************************************************
    * Description : Get V700 Territory Mapping
    * Author      : cskumar
    *****************************************************************/
	
	PROCEDURE gm_fch_all_ter_map(
		p_token  			IN 		t101a_user_token.c101a_token_id%TYPE,
		p_langid			IN		NUMBER,
		p_page_no			IN		NUMBER,
		p_uuid		        IN      t9150_device_info.c9150_device_uuid%TYPE,
		p_tmlist_cur	OUT		TYPES.CURSOR_TYPE
	)
	AS
		v_deviceid  t9150_device_info.c9150_device_info_id%TYPE;
		v_count	    NUMBER;
	   
	 BEGIN
		--get the device info id based on user token
		v_deviceid := get_device_id(p_token, p_uuid);
		
		gm_fch_all_ter_map_dtl(p_page_no,p_tmlist_cur);
		
			  --commit;
	END gm_fch_all_ter_map;
	
	/*******************************************************
	   Description :  This procedure used for web service which is used to fetch V700 Details  
	 *******************************************************/
	PROCEDURE gm_fch_all_ter_map_dtl(
		p_pageno           IN     NUMBER,
		p_tmlist_cur      OUT 	  TYPES.cursor_type
    )
    AS
    	v_page_no   NUMBER;
		v_page_size NUMBER;
	  	v_start     NUMBER;
	    v_end       NUMBER;
    BEGIN
   
		v_page_no := p_pageno;
		-- get the paging details
		gm_pkg_pdpc_prodcatrpt.gm_fch_paging_dtl('SURGEON','PAGESIZE',v_page_no,v_start,v_end,v_page_size);
		
		OPEN p_tmlist_cur
			FOR		
			 SELECT COUNT (1) OVER () pagesize,result_count totalsize,v_page_no pageno, rwnum,CEIL(result_count / v_page_size) totalpages,V700.*
			 	FROM(
					SELECT V700.*, ROWNUM rwnum, COUNT (1) OVER () result_count
			    		FROM(
							select distinct ad_id ADID, ad_name ADNM,vp_name VPNM, rep_name REPNM, rep_id REPID, VP_ID VPID, TER_ID TERID,
							TER_NAME TERNM, REGION_ID  REGIONID, REGION_NAME REGIONNM, D_ID DID, D_NAME DNM, AC_ID acid, DISTTYPENM disttypenm
							from v700_territory_mapping_detail
							)
			            V700
			    )
			    V700
			  WHERE RWNUM BETWEEN v_start AND v_end;
	END gm_fch_all_ter_map_dtl;
	
	
    END gm_pkg_crm_update;
    /