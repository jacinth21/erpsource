CREATE OR REPLACE
PACKAGE BODY gm_pkg_crm_activity
IS

/********************************************************************************
* Description : Procedure to save the activity, input as JSON object.
*********************************************************************************/
        PROCEDURE gm_sav_crm_activity_json(
            p_input_string IN CLOB,
            p_user_id      IN t6630_activity.c6630_last_updated_by%TYPE,
            p_out_id OUT VARCHAR2 )
        AS
          v_json json_object_t;
          v_log_array json_array_t;
          v_log_obj json_object_t;
          v_addr_array json_array_t;
          v_addr_obj json_object_t;
          v_date_fmt VARCHAR2(20);
          v_fromdate VARCHAR2(50);
          v_enddate  VARCHAR2(50);
          v_addr_id  VARCHAR2(50);
          v_log_id   VARCHAR2(50);
          v_division VARCHAR2(50);
		  v_count NUMBER;
		  v_uptfl char(1) :='N';
		  v_party_id  t101_user.c101_party_id%type;
        BEGIN
          v_date_fmt := GET_RULE_VALUE('DATEFMT', 'DATEFORMAT');
          v_json     := json_object_t.parse(p_input_string);
          p_out_id   := v_json.get_String('actid');
          v_fromdate := v_json.get_String('actstartdate');
          v_enddate  := v_json.get_String('actenddate');
          v_division := v_json.get_String('division');
          
         SELECT  C101_PARTY_ID INTO v_party_id
	       FROM T101_USER 
	      WHERE c101_user_id = p_user_id; 
          
          --PC-4002 - Block Sales Reps from booking Physician Visit on Restricted Days
        SELECT COUNT(*) INTO v_count
		  FROM T6630_ACTIVITY T6630
		 WHERE T6630.C6630_VOID_FL                   IS NULL
		   AND T6630.C901_ACTIVITY_TYPE               =  '26240596' -- Restricted
	       AND T6630.C901_ACTIVITY_STATUS            !=  '105762' -- Cancelled
           AND (C1910_DIVISION_ID IN (v_division,'') OR C1910_DIVISION_ID IS NULL)
    	   AND T6630.C6630_ACTIVITY_ID               != NVL (p_out_id, '-999') -- Edit Activity
		   AND (((T6630.C6630_FROM_DT                <= TO_DATE(v_fromdate, v_date_fmt)
		   AND T6630.C6630_TO_DT                     >= TO_DATE(v_fromdate, v_date_fmt))
		    OR  (T6630.C6630_FROM_DT                 <= TO_DATE(v_enddate, v_date_fmt)
		   AND T6630.C6630_TO_DT                     >= TO_DATE(v_enddate, v_date_fmt)))
		    OR ((TO_DATE(v_fromdate, v_date_fmt)     <= T6630.C6630_FROM_DT
		   AND TO_DATE(v_enddate, v_date_fmt)        >= T6630.C6630_TO_DT)
		    OR (TO_DATE(v_fromdate, v_date_fmt)      <= T6630.C6630_FROM_DT
		   AND TO_DATE(v_enddate, v_date_fmt)        >= T6630.C6630_TO_DT)));
		   
		
		   v_uptfl := gm_pkg_cm_shipping_trans.get_user_security_event_status(v_party_id,'CRMADMIN-PHYSVISIT');
		   
          IF (v_count = 0 OR v_uptfl = 'Y') THEN
          gm_sav_crm_activity( p_out_id, v_json.get_String('actnm'), TO_DATE (v_fromdate,v_date_fmt),
           					   TO_DATE (v_enddate,v_date_fmt), v_json.get_String('actdesc'), 
           					   v_json.get_String('voidfl'), v_json.get_String('acttype'), 
           					   v_json.get_String('actstatus'), v_json.get_String('actcategory'), NULL, NULL, p_user_id, 
           					   v_json.get_String('partyid'), v_json.get_String('partynm'), 
           					   TO_TIMESTAMP_TZ(v_fromdate||' '||v_json.get_String('actstarttime'), 
           					   v_date_fmt||' HH24:MI TZR'), TO_TIMESTAMP_TZ(v_enddate||' '||v_json.get_String('actendtime'), 
           					   v_date_fmt||' HH24:MI TZR'), v_json.get_String('toppriority'), 
           					   v_json.get_String('otherinstr'), v_json.get_String('division'), 
           					   v_json.get_String('actnotes'), v_json.get_String('dietary'), 
           					   v_json.get_String('region'), v_json.get_String('segid'), v_json.get_String('pathlgy'), 
           					   v_json.get_String('workflow'),v_json.get_String('zone'), v_json.get_String('adminsts') );
          gm_sav_crm_activity_attr_json (p_out_id, treat(v_json.get('arrgmactivityattrvo')
        AS
          json_array_t), p_user_id);
          gm_sav_crm_activity_party_json (p_out_id, treat(v_json.get('arrgmactivitypartyvo')
        AS
          json_array_t), p_user_id);
          gm_sav_crm_log_json (p_out_id, treat(v_json.get('arrgmcrmlogvo')
        AS
          json_array_t), p_user_id);
          
        ELSE
        raise_application_error (-20999, 'We are not able to process this request. Please select another date or contact vip@globusmedical.com') ;
		END IF;
          
        END gm_sav_crm_activity_json;

/********************************************************************************
* Description : Procedure to save the product information, input as JSON array.
*********************************************************************************/

        PROCEDURE gm_sav_crm_activity_attr_json(
            p_actid      IN t6630_activity.c6630_activity_id%TYPE,
            p_attr_array IN json_array_t,
            p_user_id    IN t6630_activity.c6630_last_updated_by%TYPE )
        AS
          v_attr_obj json_object_t;
        BEGIN
          FOR i IN 0 .. p_attr_array.get_size - 1
          LOOP
            v_attr_obj := treat(p_attr_array.get(i)
          AS
            json_object_t);
            gm_sav_crm_activity_attr ( v_attr_obj.get_String('attrid'), p_actid, v_attr_obj.get_String('attrtype'), v_attr_obj.get_String('attrval'), v_attr_obj.get_String('attrnm'), v_attr_obj.get_String('voidfl'), p_user_id) ;
          END LOOP;
        END gm_sav_crm_activity_attr_json;

/********************************************************************************
* Description : Procedure to save the party information, input as JSON array.
*********************************************************************************/
        
        PROCEDURE gm_sav_crm_activity_party_json(
            p_actid            IN t6630_activity.c6630_activity_id%TYPE,
            p_party_asso_array IN json_array_t,
            p_user_id          IN t6630_activity.c6630_last_updated_by%TYPE )
        AS
          v_party_asso_obj json_object_t;
        BEGIN
          FOR i IN 0 .. p_party_asso_array.get_size - 1
          LOOP
            v_party_asso_obj := treat(p_party_asso_array.get(i)
          AS
            json_object_t);
            gm_sav_crm_activity_party ( v_party_asso_obj.get_String('actpartyid'), p_actid, v_party_asso_obj.get_String('partyid'), v_party_asso_obj.get_String('roleid'), v_party_asso_obj.get_String('statusid'), v_party_asso_obj.get_String('voidfl'), p_user_id);
          END LOOP;
        END gm_sav_crm_activity_party_json;

/********************************************************************************
* Description : Procedure to save the crm log information, input as JSON array.
*********************************************************************************/

        PROCEDURE gm_sav_crm_log_json(
            p_actid     IN t6630_activity.c6630_activity_id%TYPE,
            p_log_array IN json_array_t,
            p_user_id   IN t6630_activity.c6630_last_updated_by%TYPE )
        AS
          v_log_obj json_object_t;
          v_log_id VARCHAR2(50);
        BEGIN
          FOR i IN 0 .. p_log_array.get_size - 1
          LOOP
            v_log_obj := treat(p_log_array.get(i)
          AS
            json_object_t);
            v_log_id := v_log_obj.get_String('logid');
            gm_sav_crm_log ( v_log_id, p_actid, v_log_obj.get_String('logtypeid'), v_log_obj.get_String('logcomment'), p_user_id, v_log_obj.get_String('voidfl'));
          END LOOP;
        END gm_sav_crm_log_json;
        

/********************************************************************************
* Description : Procedure to fetch the activity information, input as activity id.
*********************************************************************************/
        PROCEDURE gm_fch_crm_activity_json(
            p_actid IN t6630_activity.c6630_activity_id%TYPE,
            p_user_id   IN t6630_activity.c6630_last_updated_by%TYPE,
            p_out_str OUT CLOB )
        AS
         v_json_out JSON_OBJECT_T;
		 v_activity VARCHAR2(4000);
		 v_activityattr VARCHAR2(4000);
		 v_activityparty VARCHAR2(4000);
		 v_activitylog VARCHAR2(4000);
        BEGIN
          SELECT JSON_OBJECT( 'dietary' VALUE C6630_Dietary, 
          'actid' VALUE C6630_Activity_Id, 
          'actnm' VALUE C6630_Activity_Nm, 
          'createdbyid' VALUE C6630_Created_By, 
          'createdby' IS Get_User_Name(C6630_Created_By), 
          'createddate' VALUE TO_CHAR(C6630_Created_Date,'mm/dd/yyyy'), 
          'actdesc' VALUE C6630_Activity_Desc, 
          'voidfl' VALUE C6630_Void_Fl, 
          'actstartdate' VALUE TO_CHAR(C6630_From_Dt,'mm/dd/yyyy'), 
          'actenddate' VALUE TO_CHAR(C6630_To_Dt,'mm/dd/yyyy'), 
          'acttype' VALUE C901_Activity_Type, 
          'acttypenm' VALUE Get_Code_Name(C901_Activity_Type), 
          'actstatus' VALUE C901_Activity_Status, 
          'actstatusnm' VALUE Get_Code_Name(C901_Activity_Status), 
          'actcategory' VALUE C901_Activity_Category,
            --'sourcetypeid' VALUE C901_Source_Type,
            'sourcetypnm' VALUE Get_Code_Name(C901_Source_Type), 
            'source' VALUE C6630_Source, 
            'partyid' VALUE C6630_Party_Id, 
            'partynm' VALUE C6630_Party_Nm , 
            'travelstatus' VALUE C6630_TRAVEL_STATUS,
			'travelcoordinator' IS Get_User_Name(C6630_TRAVEL_COORDINATOR) ,
			'notes' VALUE C6630_NOTE,
			'travelnotes' VALUE C6630_TRAVEL_NOTES,
            'actstarttime' VALUE TO_CHAR(C6630_From_Tm,'HH24:MI'), 
            'actendtime' VALUE TO_CHAR(C6630_To_Tm,'HH24:MI'), 
            'tzone' VALUE TO_CHAR(C6630_From_Tm,'TZR'), 
            'otherinstr' VALUE C6630_Other_Instrument, 
            'toppriority' VALUE C6630_Top_Priority, 
            'divisionnm' VALUE T901.C901_CODE_NM, 
            'division' VALUE C1910_Division_Id, 
            'actnotes' VALUE C6630_Note, 'region' VALUE C901_REGION, 
            'regionnm' VALUE Get_Code_Name(C901_REGION), 
            'segid' VALUE C2090_SEGMENT_ID, 
            'segnm' VALUE Get_Segment_Name(C2090_SEGMENT_ID), 
            'pathlgy' VALUE C6630_PATHOLOGY, 
            'workflow' VALUE C901_WORK_FLOW, 
            'workflownm' VALUE Get_Code_Name(C901_WORK_FLOW),
            'accessfl' VALUE get_activity_access_flag(c6630_activity_id,p_user_id),
            'zone' VALUE C901_ZONE,
			'zonenm' VALUE Get_Code_Name(C901_ZONE),
			'adminsts' VALUE C901_ADMIN_STATUS,
			'adminstsnm' VALUE Get_Code_Name(C901_ADMIN_STATUS)
            )
          INTO v_activity
          FROM T6630_ACTIVITY T6630, 
          (SELECT * FROM T901_CODE_LOOKUP WHERE  C901_CODE_GRP = 'DIVLST' AND C901_ACTIVE_FL = 1 AND C901_VOID_FL IS NULL) T901
          --T901_CODE_LOOKUP T901 -- PC-2446 to get division nm from code lookup --  PC-4002 - Block Sales Reps from booking Physician Visit on Restricted Days
          WHERE C6630_VOID_FL  IS NULL
          AND T6630.C1910_DIVISION_ID = T901.C902_CODE_NM_ALT(+)
       --   AND T901.C901_CODE_GRP = 'DIVLST' 
       --    AND T901.C901_ACTIVE_FL = 1
      --    AND T901.C901_VOID_FL IS NULL
          AND C6630_ACTIVITY_ID = p_actid;
          
          SELECT JSON_ARRAYAGG (
          	JSON_OBJECT( 'attrid' VALUE C6632_Activity_Attribute_Id , 
          	'attrval' VALUE C6632_Attribute_Value, 
          	'attid' VALUE C6632_Attribute_Id , 
          	'attrnm' VALUE C6632_Attribute_Nm, 
          	'attrtype' VALUE C901_Attribute_Type, 
          	'voidfl' VALUE C6632_Void_Fl ) ) INTO v_activityattr
            FROM T6632_Activity_Attribute
            WHERE C6632_Void_Fl  IS NULL
            AND C6630_Activity_Id = p_actid;

			SELECT JSON_ARRAYAGG ( 
			JSON_OBJECT( 'actpartyid' VALUE T6631.C6631_Activity_Party_Id,
			'partyid' VALUE T6631.C101_Party_Id, 
			'roleid' VALUE T6631.C901_Activity_Party_Role, 
			'rolenm' VALUE Get_Code_Name(T6631.C901_Activity_Party_Role), 
			'partynpid' VALUE Npid.Npid, 
			'partynm' VALUE GET_CODE_NAME(npid.salute)
              || ' '
              || T101.C101_FIRST_NM
              || ' '
              || T101.C101_MIDDLE_INITIAL
              || ' '
              || T101.C101_Last_Nm, 
            'statusid' VALUE T6631.C901_Party_Status, 
            'statusnm' VALUE Get_Code_Name(T6631.C901_Party_Status), 
            'actid' VALUE T6631.C6630_Activity_Id, 
            'partytype' VALUE T101.C901_Party_Type, 
            'surgeoncv' VALUE DECODE(T101.C901_Party_Type,7000,Get_Surgeon_Cv_Path(T6631.C101_PARTY_ID),'') ))
            INTO  v_activityparty
            FROM T6631_ACTIVITY_PARTY_ASSOC T6631,
              T101_PARTY T101 ,
              T101_USER T101U ,
              (SELECT c6600_surgeon_npi_id npid ,
                C6600_PARTY_SALUTE salute,
                c101_party_surgeon_id partyid
              FROM t6600_party_surgeon
              WHERE c6600_void_fl IS NULL
              ) npid
            WHERE T6631.C101_PARTY_ID = T101.C101_PARTY_ID(+)
            AND T101.C101_PARTY_ID    = NPID.PARTYID(+)
            AND T101.C101_PARTY_ID    = T101U.C101_PARTY_ID(+)
            AND T6631.C6631_VOID_FL  IS NULL
            AND C6630_Activity_Id     = p_actid;
            
            SELECT JSON_ARRAYAGG ( JSON_OBJECT('refid' VALUE c902_ref_id, 
	        'logid' VALUE c902_log_id, 
	        'logtypeid' VALUE c902_type,
	        'logtype' VALUE GET_CODE_NAME(c902_type), 
	        'voidfl' VALUE c902_void_fl, 
	        'logcomment' VALUE c902_comments, 
	        'updatedbyid' VALUE c902_last_updated_by, 
	        'updatedby' VALUE GET_USER_NAME(c902_last_updated_by),
			'updateddate' VALUE TO_CHAR(c902_last_updated_date,'MM/DD/YYYY')))
			INTO v_activitylog
			FROM t902_log
			WHERE c902_ref_id = p_actid
			AND c902_void_fl IS NULL
			AND c902_type    in (105184,4000891,4000892);
            
	 	v_json_out     := JSON_OBJECT_T.parse(v_activity);
		
		IF v_activityattr IS NOT NULL THEN
			v_json_out.put('arrgmactivityattrvo',JSON_ARRAY_T(v_activityattr));
		END IF;
		
		IF v_activityparty IS NOT NULL THEN
			v_json_out.put('arrgmactivitypartyvo',JSON_ARRAY_T(v_activityparty));
		END IF;
		
		IF v_activitylog IS NOT NULL THEN
			v_json_out.put('arrgmcrmlogvo',JSON_ARRAY_T(v_activitylog));
		END IF;	
		
		p_out_str := v_json_out.to_string;           
        END gm_fch_crm_activity_json;


/********************************************************************************
* Description : Procedure to save the activity information.
*********************************************************************************/

          PROCEDURE gm_sav_crm_activity(
              p_actid       IN OUT t6630_activity.c6630_activity_id%TYPE,
              p_actnm       IN t6630_activity.c6630_activity_nm%TYPE,
              p_fromdate    IN t6630_activity.c6630_from_dt%TYPE,
              p_enddate     IN t6630_activity.c6630_to_dt%TYPE,
              p_actdesc     IN t6630_activity.c6630_activity_desc%TYPE,
              p_voidfl      IN t6630_activity.c6630_void_fl%TYPE,
              p_acttype     IN t6630_activity.c901_activity_type%TYPE,
              p_actstatus   IN t6630_activity.c901_activity_status%TYPE,
              p_actcategory IN t6630_activity.c901_activity_category%TYPE,
              p_sourcetyp   IN t6630_activity.c901_source_type%TYPE,
              p_source      IN t6630_activity.c6630_source%TYPE,
              p_userid      IN t6630_activity.c6630_last_updated_by%TYPE,
              p_partyid     IN t6630_activity.c6630_party_id%TYPE,
              p_partynm     IN t6630_activity.c6630_party_nm%TYPE,
              p_fromtm      IN t6630_activity.c6630_from_tm%TYPE,
              p_totm        IN t6630_activity.c6630_to_tm%TYPE,
              p_toppriority IN t6630_activity.c6630_top_priority%TYPE,
              p_otherinstr  IN t6630_activity.c6630_other_instrument%TYPE,
              p_division    IN t6630_activity.c1910_division_id%TYPE,
              p_actnotes    IN t6630_activity.c6630_note%TYPE,
              p_actdietary  IN t6630_activity.c6630_dietary%TYPE,
              p_region      IN t6630_activity.C901_REGION%TYPE,
              p_segid       IN t6630_activity.C2090_SEGMENT_ID%TYPE,
              p_pathlgy     IN t6630_activity.C6630_PATHOLOGY%TYPE,
              p_workflow    IN t6630_activity.C901_WORK_FLOW%TYPE,
              p_zone     	IN t6630_activity.C901_ZONE%TYPE,
              p_adminsts    IN t6630_activity.C901_ADMIN_STATUS%TYPE )
          AS
          BEGIN
            UPDATE T6630_ACTIVITY
            SET C6630_ACTIVITY_NM     = p_actnm ,
              C6630_FROM_DT           = p_fromdate ,
              C6630_TO_DT             = p_enddate ,
              C6630_ACTIVITY_DESC     = p_actdesc ,
              C6630_VOID_FL           = p_voidfl ,
              C901_ACTIVITY_TYPE      = p_acttype ,
              C901_ACTIVITY_STATUS    = p_actstatus ,
              C901_ACTIVITY_CATEGORY  = p_actcategory ,
              C901_SOURCE_TYPE        = p_sourcetyp ,
              C6630_SOURCE            = p_source ,
              C6630_LAST_UPDATED_BY   = p_userid ,
              C6630_LAST_UPDATED_DATE = SYSDATE ,
              C6630_PARTY_ID          = p_partyid ,
              C6630_PARTY_NM          = p_partynm ,
              C6630_FROM_TM           = p_fromtm ,
              C6630_TO_TM             = p_totm ,
              C6630_TOP_PRIORITY      = p_toppriority ,
              C6630_OTHER_INSTRUMENT  = p_otherinstr ,
              C1910_DIVISION_ID       = p_division ,
              C6630_NOTE              = p_actnotes ,
              C6630_UPDATE_FL         = NULL ,
              C6630_DIETARY           = p_actdietary ,
              C901_REGION             = p_region ,
              C2090_SEGMENT_ID        = p_segid ,
              C6630_PATHOLOGY         = p_pathlgy ,
              C901_WORK_FLOW          = p_workflow,
              C901_ZONE         	  = p_zone ,
              C901_ADMIN_STATUS       = p_adminsts
            WHERE C6630_ACTIVITY_ID   = p_actid;
            IF (SQL%ROWCOUNT          = 0) THEN
              IF(p_actid             IS NULL) THEN
                SELECT DECODE(p_actcategory,'105272','HC-'
                  ||s6630_activity.nextval,s6630_activity.nextval)
                INTO p_actid
                FROM DUAL;
              END IF;
              INSERT
              INTO T6630_ACTIVITY
                (
                  C6630_ACTIVITY_ID,
                  C6630_ACTIVITY_NM,
                  C6630_FROM_DT,
                  C6630_TO_DT,
                  C6630_ACTIVITY_DESC,
                  C6630_VOID_FL,
                  C901_ACTIVITY_TYPE,
                  C901_ACTIVITY_STATUS,
                  C901_ACTIVITY_CATEGORY,
                  C901_SOURCE_TYPE,
                  C6630_SOURCE,
                  C6630_CREATED_BY,
                  C6630_CREATED_DATE,
                  C6630_PARTY_ID,
                  C6630_PARTY_NM,
                  C6630_FROM_TM,
                  C6630_TO_TM,
                  C6630_TOP_PRIORITY,
                  C6630_OTHER_INSTRUMENT,
                  C1910_DIVISION_ID,
                  C6630_NOTE,
                  C6630_DIETARY,
                  C901_REGION,
                  C2090_SEGMENT_ID,
                  C6630_PATHOLOGY,
              	  C901_WORK_FLOW,
              	  C901_ZONE,
              	  C901_ADMIN_STATUS
                )
                VALUES
                (
                  p_actid,
                  p_actnm,
                  p_fromdate,
                  p_enddate,
                  p_actdesc,
                  p_voidfl,
                  p_acttype,
                  p_actstatus,
                  p_actcategory,
                  p_sourcetyp,
                  p_source,
                  p_userid,
                  SYSDATE,
                  p_partyid,
                  p_partynm,
                  p_fromtm ,
                  p_totm ,
                  p_toppriority,
                  p_otherinstr,
                  p_division,
                  p_actnotes,
                  p_actdietary,
                  p_region,
                  p_segid,
                  p_pathlgy,
                  p_workflow,
                  p_zone,
          		  p_adminsts
                );
            END IF;
            IF (p_actstatus = '105762') THEN
            gm_pkg_crm_preceptorship_txn.gm_sav_hostcase_status(p_actid,p_userid);
            END IF;
          END gm_sav_crm_activity;
          
/********************************************************************************
* Description : Procedure to save the activity attribute information.
*********************************************************************************/         
                    
          PROCEDURE gm_sav_crm_activity_attr
            (
              p_attrid   IN t6632_activity_attribute.c6632_activity_attribute_id%TYPE,
              p_actid    IN t6632_activity_attribute.c6630_activity_id%TYPE,
              p_attrtype IN t6632_activity_attribute.c901_attribute_type%TYPE,
              p_attrval  IN t6632_activity_attribute.c6632_attribute_value%TYPE,
              p_attrnm   IN t6632_activity_attribute.c6632_attribute_nm%TYPE,
              p_voidfl   IN t6632_activity_attribute.c6632_void_fl%TYPE,
              p_userid   IN t6632_activity_attribute.c6632_last_updated_by%TYPE
            )
          AS
          BEGIN
            UPDATE T6632_ACTIVITY_ATTRIBUTE
            SET C901_ATTRIBUTE_TYPE           = p_attrtype ,
              c6632_attribute_value           = p_attrval ,
              C6632_ATTRIBUTE_NM              = p_attrnm ,
              C6632_VOID_FL                   = p_voidfl ,
              C6632_LAST_UPDATED_BY           = p_userid ,
              C6632_LAST_UPDATED_DATE         = SYSDATE
            WHERE C6632_ACTIVITY_ATTRIBUTE_ID = p_attrid
            AND C6630_ACTIVITY_ID             = p_actid
            AND C6632_VOID_FL                IS NULL;
            IF (SQL%ROWCOUNT                  = 0) THEN
              INSERT
              INTO T6632_ACTIVITY_ATTRIBUTE
                (
                  C6632_ACTIVITY_ATTRIBUTE_ID,
                  C6630_ACTIVITY_ID,
                  C901_ATTRIBUTE_TYPE,
                  c6632_attribute_value,
                  C6632_ATTRIBUTE_NM,
                  C6632_VOID_FL,
                  C6632_LAST_UPDATED_BY,
                  C6632_LAST_UPDATED_DATE
                )
                VALUES
                (
                  s6632_activity_attribute.nextval,
                  p_actid,
                  p_attrtype,
                  p_attrval,
                  p_attrnm,
                  p_voidfl,
                  p_userid,
                  SYSDATE
                );
            END IF;
          END gm_sav_crm_activity_attr;

/********************************************************************************
* Description : Procedure to save the activity party information.
*********************************************************************************/   

          PROCEDURE gm_sav_crm_activity_party
            (
              p_actpartyid IN t6631_activity_party_assoc.c6631_activity_party_id%TYPE,
              p_actid      IN t6631_activity_party_assoc.c6630_activity_id%TYPE,
              p_partyid    IN t6631_activity_party_assoc.c101_party_id%TYPE,
              p_roleid     IN t6631_activity_party_assoc.c901_activity_party_role%TYPE,
              p_statusid   IN t6631_activity_party_assoc.c901_party_status%TYPE,
              p_voidfl     IN t6631_activity_party_assoc.c6631_void_fl%TYPE,
              p_userid     IN t6631_activity_party_assoc.c6631_last_updated_by%TYPE
            )
          AS
          BEGIN
            UPDATE T6631_ACTIVITY_PARTY_ASSOC
            SET C101_PARTY_ID             = p_partyid ,
              C901_ACTIVITY_PARTY_ROLE    = p_roleid ,
              C901_PARTY_STATUS           = p_statusid ,
              C6631_VOID_FL               = p_voidfl ,
              C6631_LAST_UPDATED_BY       = p_userid ,
              C6631_LAST_UPDATED_DATE     = SYSDATE
            WHERE C6631_ACTIVITY_PARTY_ID = p_actpartyid
            AND C6630_ACTIVITY_ID         = p_actid
            AND C6631_VOID_FL            IS NULL;
            IF (SQL%ROWCOUNT              = 0) THEN
              INSERT
              INTO T6631_ACTIVITY_PARTY_ASSOC
                (
                  C6631_ACTIVITY_PARTY_ID,
                  C6630_ACTIVITY_ID,
                  C101_PARTY_ID,
                  C901_ACTIVITY_PARTY_ROLE,
                  C901_PARTY_STATUS,
                  C6631_VOID_FL,
                  C6631_LAST_UPDATED_BY,
                  C6631_LAST_UPDATED_DATE
                )
                VALUES
                (
                  s6631_activity_party.nextval,
                  p_actid,
                  p_partyid,
                  p_roleid,
                  p_statusid,
                  p_voidfl,
                  p_userid,
                  SYSDATE
                );
            END IF;
          END GM_SAV_CRM_ACTIVITY_PARTY;

/********************************************************************************
* Description : Procedure to save the crm log information.
*********************************************************************************/            
          
          PROCEDURE gm_sav_crm_log
            (
              p_logid      IN OUT t902_log.c902_log_id%TYPE,
              p_refid      IN t902_log.c902_ref_id%TYPE,
              p_logtypeid  IN t902_log.c902_type%TYPE,
              p_logcomment IN t902_log.c902_comments%TYPE,
              p_userid     IN t902_log.c902_created_by%TYPE,
              p_voidfl     IN t902_log.c902_void_fl%TYPE
            )
          AS
            v_company_id t1900_company.c1900_company_id%TYPE;
          BEGIN
            --
            SELECT NVL (get_compid_frm_cntx (), get_rule_value ('DEFAULT_COMPANY', 'ONEPORTAL'))
            INTO v_company_id
            FROM DUAL;
            --
            UPDATE T902_LOG
            SET C902_REF_ID          = p_refid ,
              C902_TYPE              = p_logtypeid ,
              C902_COMMENTS          = p_logcomment ,
              C902_LAST_UPDATED_BY   = p_userid ,
              C902_LAST_UPDATED_DATE = SYSDATE ,
              C902_VOID_FL           = p_voidfl
            WHERE C902_LOG_ID        = p_logid;
            IF (SQL%ROWCOUNT         = 0) THEN
              SELECT s507_log.NEXTVAL INTO p_logid FROM DUAL;
              INSERT
              INTO T902_LOG
                (
                  C902_LOG_ID,
                  C902_REF_ID,
                  C902_TYPE,
                  C902_COMMENTS,
                  C902_CREATED_BY,
                  C902_CREATED_DATE,
                  C902_LAST_UPDATED_BY,
                  C902_LAST_UPDATED_DATE,
                  C902_VOID_FL,
                  c1900_company_id
                )
                VALUES
                (
                  p_logid,
                  p_refid,
                  p_logtypeid,
                  p_logcomment,
                  p_userid,
                  SYSDATE,
                  p_userid,
                  SYSDATE,
                  p_voidfl,
                  v_company_id
                );
            END IF;
          END GM_SAV_CRM_LOG;
          
          
          PROCEDURE gm_sav_crm_update_result
            (
              p_actid  IN OUT t6630_activity.c6630_activity_id%TYPE,
              p_source IN t6630_activity.c6630_source%TYPE,
              p_userid IN t6630_activity.c6630_last_updated_by%TYPE
            )
          AS
          BEGIN
            UPDATE T6630_ACTIVITY
            SET C6630_SOURCE          = p_source ,
              C6630_LAST_UPDATED_BY   = p_userid ,
              C6630_LAST_UPDATED_DATE = SYSDATE
            WHERE C6630_ACTIVITY_ID   = p_actid;
          END gm_sav_crm_update_result;
/********************************************************************************
* Description : Procedure to update the event item id for the activity.
* Author      : Bala
*********************************************************************************/
PROCEDURE gm_sav_crm_event_item_id(
    p_actid  IN t6630_activity.c6630_activity_id%TYPE,
    p_itemid IN t6630_activity.c6630_event_item_id%TYPE,
    p_userid IN t6630_activity.c6630_last_updated_by%TYPE )
AS
BEGIN
  UPDATE T6630_ACTIVITY
  SET c6630_event_item_id   = p_itemid ,
    C6630_LAST_UPDATED_BY   = p_userid ,
    C6630_LAST_UPDATED_DATE = SYSDATE
  WHERE C6630_ACTIVITY_ID   = p_actid;
END gm_sav_crm_event_item_id;
/********************************************************************************
* Description : Procedure will call from JMS/JOB to sync updated activity from crm activity
*     and Marketing Collateral File share info into t6650 table.
* Author      : Gopi
*********************************************************************************/
PROCEDURE gm_sav_party_activity_sync(
    p_str    IN VARCHAR2,
    p_userid IN T6650_PARTY_ACTIVITY.C6650_LAST_UPDATED_BY%TYPE )
AS
BEGIN
  IF p_str = 'ACTIVITY' THEN
    gm_sav_party_act_sync_crm_act(p_userid);
  ELSIF p_str = 'FILESHARE' THEN
    gm_sav_party_act_sync_mc_share(p_userid);
  ELSIF p_str = 'CASEREQUEST' THEN
    gm_sav_party_act_sync_case_req(p_userid);
  END IF;
END gm_sav_party_activity_sync;
/********************************************************************************
* Description : Procedure will call from JMS/JOB to sync updated activity from t6630 to t6650 table.
* Author      : Gopi
*********************************************************************************/
PROCEDURE gm_sav_party_act_sync_crm_act(
    p_userid IN T6650_PARTY_ACTIVITY.C6650_LAST_UPDATED_BY%TYPE )
AS
  v_count NUMBER;
BEGIN
  --Get the count of unsynced activity records
  SELECT COUNT(1)
  INTO v_count
  FROM T6630_ACTIVITY T6630
  WHERE C6630_VOID_FL              IS NULL
  AND T6630.C901_ACTIVITY_CATEGORY IN ('105267','105265','105266','105268','105272')
  AND t6630.c6630_update_fl        IS NULL;
  /*
  105265 MERC
  105266 Physician Visit
  105267 Sales Call
  105268 Training
  */
  IF (v_count > 0) THEN
    --Delete existing records for updated activity
    DELETE
    FROM t6650_party_activity
    WHERE c901_ref_type IN ('105267','105265','105266','105268','105272')
    AND c6650_ref_id    IN
      (SELECT T6630.C6630_ACTIVITY_ID REFID
      FROM T6630_ACTIVITY T6630
      WHERE C6630_VOID_FL              IS NULL
      AND T6630.C901_ACTIVITY_CATEGORY IN ('105267','105265','105266','105268','105272')
      AND t6630.c6630_update_fl        IS NULL
      );
   --Deleting Case request detail if Host case cancelled 
    DELETE
    FROM t6650_party_activity
    WHERE c901_ref_type IN ('105273') --Case Request
    AND c6650_txn_id    IN
      (SELECT T6630.C6630_ACTIVITY_ID REFID
      FROM T6630_ACTIVITY T6630
      WHERE C6630_VOID_FL              IS NULL
      AND T6630.C901_ACTIVITY_CATEGORY IN ('105272') -- Host Case
      AND t6630.C901_Activity_Status  IN ('105762') -- Cancelled 
      AND t6630.c6630_update_fl        IS NULL
      );
      
    --Insert updated activity records
    INSERT
    INTO T6650_PARTY_ACTIVITY
      (
        C6650_PARTY_ACTIVITY_ID,
        c6650_ref_id,
        c901_ref_type,
        c101_from_party_id,
        c6650_activity_from_dt,
        c6650_activity_to_dt,
        c6650_activity_title,
        c6650_activity_detail,
        c6650_last_updated_by,
        c6650_last_updated_date
      )
    SELECT S6650_PARTY_ACTIVITY.nextval,
      T6630.C6630_ACTIVITY_ID refid,
      C901_ACTIVITY_CATEGORY reftype,
      ACTPARTY.C101_PARTY_ID frm_party_id,
      T6630.C6630_FROM_DT,
      T6630.C6630_TO_DT,
      DECODE(T6630.C901_ACTIVITY_CATEGORY,
      105267,ACTATTR.ACTIVITY_PRODUCTS,
      105272,Get_Segment_Name(T6630.C2090_Segment_Id)|| 
      NVL2(T6630.C901_Work_Flow,' '||get_code_name(T6630.C901_Work_Flow),''),
      t6630.c6630_activity_nm) title,
      DECODE(T6630.C901_ACTIVITY_CATEGORY, 105267,NVL2( t6630.c6630_from_tm, 'from '
      ||TO_CHAR(t6630.c6630_from_tm,'HH:MI AM')
      ||' to '
      || TO_CHAR(t6630.c6630_to_tm,'HH:MI AM')
      ,''),
      105272,NVL2(t6630.c6630_from_tm, 'from '
      ||To_Char(T6630.C6630_From_Tm,'HH:MI AM')
      ||' to '
      || TO_CHAR(t6630.c6630_to_tm,'HH:MI AM')
      ,'')
      ) detail,
      NVL(T6630.c6630_last_updated_by,T6630.c6630_created_by) updated_by,
      sysdate updated_dt
    FROM T6630_ACTIVITY T6630,
      ( SELECT DISTINCT C6630_ACTIVITY_ID,
        t101.C101_PARTY_ID
      FROM T6631_ACTIVITY_PARTY_ASSOC t6631,
        t101_party t101
      WHERE C6631_VOID_FL     IS NULL
      AND t6631.C101_PARTY_ID  = t101.C101_PARTY_ID
      AND t101.c901_party_type ='7000'
      ) ACTPARTY,
      (SELECT RTRIM (XMLAGG (XMLELEMENT (N, C6632_ATTRIBUTE_NM, ', ')) .EXTRACT ('//text()'), ', ') ACTIVITY_PRODUCTS ,
        C6630_ACTIVITY_ID
      FROM T6632_ACTIVITY_ATTRIBUTE
      WHERE C6632_VOID_FL IS NULL
      GROUP BY C6630_ACTIVITY_ID
      ) ACTATTR
    WHERE T6630.C6630_ACTIVITY_ID = ACTPARTY.C6630_ACTIVITY_ID(+)
    AND T6630.C6630_ACTIVITY_ID = ACTATTR.C6630_ACTIVITY_ID(+)
    AND C6630_VOID_FL                IS NULL
    AND t6630.C901_Activity_Status NOT IN ('105762') -- Cancelled 
    AND T6630.C901_ACTIVITY_CATEGORY IN ('105267','105265','105266', '105268','105272')
    AND t6630.c6630_update_fl        IS NULL;
    --Update update flag as 'Y'. It means records synced with t6650 table.
    UPDATE T6630_ACTIVITY
    SET c6630_update_fl         ='Y',
      c6630_last_updated_by     = p_userid,
      c6630_last_updated_date   =CURRENT_DATE
    WHERE C6630_VOID_FL        IS NULL
    AND C901_ACTIVITY_CATEGORY IN ('105267','105265','105266','105268','105272')
    AND c6630_update_fl        IS NULL;
  END IF;
END gm_sav_party_act_sync_crm_act;
/********************************************************************************
* Description : Procedure will call from JMS/JOB to sync updated activity from MC file share info into t6650 table.
* Author      : Gopi
*********************************************************************************/
PROCEDURE gm_sav_party_act_sync_mc_share(
    p_userid IN T6650_PARTY_ACTIVITY.C6650_LAST_UPDATED_BY%TYPE )
AS
  v_count NUMBER;
BEGIN
  --Get the count of unsynced file share records
  SELECT COUNT(1)
  INTO v_count
  FROM t1016_share T6630
  WHERE c1016_update_fl IS NULL;
  IF (v_count            > 0) THEN
    --Delete existing records for updated file share records
    DELETE
    FROM t6650_party_activity
    WHERE c901_ref_type IN ('105271')
    AND c6650_ref_id    IN
      (SELECT c1016_share_id FROM t1016_share T6630 WHERE c1016_update_fl IS NULL
      );
    --INSERT Marketing collateral file share info
    INSERT
    INTO T6650_PARTY_ACTIVITY
      (
        C6650_PARTY_ACTIVITY_ID,
        c6650_ref_id,
        c901_ref_type,
        c101_from_party_id,
        c101_to_party_id,
        c6650_activity_from_dt,
        c6650_activity_title,
        C6650_ACTIVITY_DETAIL,
        c6650_last_updated_by,
        c6650_last_updated_date
      )
    SELECT S6650_PARTY_ACTIVITY.nextval,
      t1018.c1016_share_id,
      '105271',
      p.c101_party_id,
      u.c101_party_id,
      t1016.c1016_created_date,
      get_set_name(t903.c903_ref_id),
      DECODE(T903.C901_REF_TYPE,'103114',GET_PARTNUM_DESC(T903A.c903a_attribute_value),T903.C901_FILE_TITLE) FTITLE,
      NVL(t1016.c1016_last_updated_by,t1016.c1016_created_by) updated_by,
      sysdate updated_dt
    FROM t1016_share t1016,
      t1017_share_detail t1017,
      t1018_share_file t1018,
      t903_upload_file_list t903,
      t903a_file_attribute t903a,
      t101_party p,
      t101_user u
    WHERE t1017.c1016_share_id       = t1016.c1016_share_id
    AND t1016.c1016_share_id         = t1018.c1016_share_id
    AND t1017.c901_ref_type          =103238
    AND t1016.c901_share_status      = 103229 --processed
    AND t1016.c1016_void_fl         IS NULL
    AND t1017.c1017_void_fl         IS NULL
    AND t1018.c1018_void_fl         IS NULL
    AND t1018.c903_upload_file_list  = t903.c903_upload_file_list
    AND t903.c903_upload_file_list   = t903a.c903_upload_file_list(+)
    AND t903a.c901_attribute_type(+) = 103216 -- File Title
      --AND t1017.c1017_ref_id NOT LIKE '%.%'
    AND t1017.c1017_ref_id     = p.C101_PARTY_ID
    AND p.c901_party_type      ='7000'
    AND c1016_ref_id           = u.c101_user_id
    AND t903a.c903a_void_fl   IS NULL
    AND t1016.c1016_update_fl IS NULL;
    --Update update flag as 'Y'. It means records synced with t6650 table.
    UPDATE t1016_share
    SET c1016_update_fl       = 'Y',
      c1016_last_updated_by   =p_userid,
      c1016_last_updated_date = CURRENT_DATE
    WHERE c1016_update_fl    IS NULL;
  END IF;
END gm_sav_party_act_sync_mc_share;
/********************************************************************************
* Description : Procedure will call from JMS/JOB to sync updated case request from t6660 to t6650 table.
* Author      : Gopi
*********************************************************************************/
PROCEDURE gm_sav_party_act_sync_case_req(
    p_userid IN T6650_PARTY_ACTIVITY.C6650_LAST_UPDATED_BY%TYPE )
AS
  v_count NUMBER;
BEGIN
  --Get the count of unsynced case request records
  SELECT COUNT(1)
    INTO v_count
    FROM t6660_case_request
   WHERE c6660_void_fl              IS NULL
     AND c901_type IN ('105273')
     AND c6660_update_fl        IS NULL;  

  IF (v_count > 0) THEN
  
    --Delete existing records for updated activity
    DELETE
    FROM t6650_party_activity
    WHERE c901_ref_type IN ('105273')
    AND c6650_ref_id    IN
      (SELECT c6660_case_request_id
         FROM t6660_case_request
        WHERE c6660_void_fl              IS NULL
          AND c901_type IN ('105273')
          AND c6660_update_fl        IS NULL
      );
      
    --Insert updated activity records
    INSERT
    INTO T6650_PARTY_ACTIVITY
      (
        C6650_PARTY_ACTIVITY_ID,
        c6650_ref_id,
        c6650_txn_id,
        c901_ref_type,
        c101_from_party_id,
        c6650_activity_from_dt,
        c6650_activity_to_dt,
        c6650_activity_title,
        c6650_activity_detail,
        c6650_last_updated_by,
        c6650_last_updated_date
      )
    SELECT S6650_PARTY_ACTIVITY.nextval,
	   T6660.C6660_Case_Request_Id Refid,
	   T6660.c6630_activity_id  txnid,
	  T6660.c901_type Reftype, -- case request
	  T6660.C101_Surgeon_Party_Id frm_party_id,
	  T6630.C6630_FROM_DT,
	  T6630.C6630_To_Dt,
	  Get_Segment_Name(T6630.C2090_Segment_Id) || NVL2(T6630.C901_Work_Flow,' '||get_code_name(T6630.C901_Work_Flow),'') Title,
	  DECODE(T6630.C901_ACTIVITY_CATEGORY, 105272,NVL2(
	  t6630.c6630_from_tm, 'from '
	  ||TO_CHAR(T6630.C6630_From_Tm,'HH:MI AM')
	  ||' to '
	  || TO_CHAR(t6630.c6630_to_tm,'HH:MI AM') ,'') ) detail,
	  NVL(T6660.C6660_Updated_By,T6660.C6660_Created_By) Updated_By,
	  sysdate updated_dt
	FROM T6660_Case_Request T6660,
	  T6630_Activity T6630
	WHERE T6660.C6630_Activity_Id = T6630.C6630_Activity_Id
	AND t6660.c901_status = 105761 -- Scheduled
	AND T6660.C6660_Void_Fl      IS NULL
	AND t6630.c6630_void_fl      IS NULL
	AND t6660.C901_Status NOT IN ('105765','105762') --Open, Cancelled
	AND t6660.c6660_update_fl IS NULL;
	
    --Update update flag as 'Y'. It means records synced with t6650 table.
    UPDATE t6660_case_request
       SET c6660_update_fl    = 'Y',
           c6660_updated_by   = p_userid,
           c6660_updated_dt   = CURRENT_DATE
     WHERE c6660_void_fl IS NULL
	   AND C901_type IN ('105273')
       AND c6660_update_fl IS NULL;
       
  END IF;
END gm_sav_party_act_sync_case_req;
/*********************************************************************************************************
* Description : This function is used to whether show Surgeon name in Preceptorship Calendar detail or not
*********************************************************************************************************/
FUNCTION get_activity_access_flag (
      p_act_id     IN   t6630_activity.c6630_activity_id%TYPE,
      p_user_id    IN   t101_user.c101_user_id%TYPE
)
      RETURN VARCHAR2
IS
      v_access_fl  VARCHAR2(1):= 'N';
      v_cnt 	   NUMBER;
   BEGIN
		SELECT COUNT(1) INTO v_cnt
		FROM t6630_activity t6630,
		  (SELECT t6631.c6630_activity_id
		  FROM t6631_activity_party_assoc t6631
		  WHERE t6631.c6630_activity_id     = p_act_id
		  AND t6631.c6631_void_fl          IS NULL
		  AND t6631.c101_party_id           IN( SELECT c101_party_id FROM t101_user WHERE c101_user_id =p_user_id)
		  AND t6631.c901_activity_party_role=105561 -- field sales
		  )fieldsales
		WHERE t6630.c6630_activity_id = p_act_id
		AND t6630.c6630_activity_id   =fieldsales.c6630_activity_id
		AND t6630.c6630_void_fl      IS NULL;	 
	 	 
		IF v_cnt > 0 THEN
	 		v_access_fl:='Y';
	 	END IF;

     RETURN v_access_fl;
     
     EXCEPTION WHEN OTHERS
         THEN
             RETURN 'N';
 END get_activity_access_flag;

/*********************************************************************************************************
* Description : Fetch the count of restricted Days in Activity calendar for the given dates
*********************************************************************************************************/
PROCEDURE gm_fch_restricted_flag (
      p_startdt     IN   t6630_activity.C6630_ACTIVITY_ID%TYPE,
      p_enddt       IN   t6630_activity.C6630_ACTIVITY_ID%TYPE,
      p_actid       IN   t6630_activity.C6630_ACTIVITY_ID%TYPE,
      p_out_str     OUT  VARCHAR2
)
AS
   BEGIN
        SELECT COUNT( * ) INTO P_OUT_STR
		FROM T6630_ACTIVITY T6630
		WHERE T6630.C6630_VOID_FL            IS NULL
		AND T6630.C901_ACTIVITY_TYPE         = '26240596'
	    AND T6630.C901_ACTIVITY_STATUS       != '105762'
    	AND T6630.C6630_ACTIVITY_ID          != NVL (p_actid, '-999') 
		AND(((T6630.C6630_FROM_DT            <= TO_DATE(p_startdt, 'yyyy-mm-dd')
		AND T6630.C6630_TO_DT                >= TO_DATE(p_startdt, 'yyyy-mm-dd'))
		OR(T6630.C6630_FROM_DT               <= TO_DATE(p_enddt, 'yyyy-mm-dd')
		AND T6630.C6630_TO_DT                >= TO_DATE(p_enddt, 'yyyy-mm-dd')))
		OR((TO_DATE(p_startdt, 'yyyy-mm-dd') <= T6630.C6630_FROM_DT
		AND TO_DATE(p_enddt, 'yyyy-mm-dd')   >= T6630.C6630_TO_DT)
		OR(TO_DATE(p_startdt, 'yyyy-mm-dd')  <= T6630.C6630_FROM_DT
		AND TO_DATE(p_enddt, 'yyyy-mm-dd')   >= T6630.C6630_TO_DT)) );
 END gm_fch_restricted_flag;

 
END gm_pkg_crm_activity;
/ 
