
-- @"c:\database\packages\crm\gm_pkg_cm_contact.bdy"

CREATE OR REPLACE PACKAGE BODY gm_pkg_crm_contact
IS
--
	/*******************************************************
	  * Description : Procedure to save party contact info
	  * Parameters	:		1. p_partyId
						2. p_mode
						3. p_contactType
						4. p_contactValue
						5. p_preference
						6. p_inactivefl
	  *
	  *******************************************************/
	PROCEDURE gm_sav_contactinfo (
		p_contactid 	 IN OUT   t107_contact.c107_contact_id%TYPE
	  , p_partyid		 IN 	  t101_party.c101_party_id%TYPE
	  , p_contactmode	 IN 	  t107_contact.c901_mode%TYPE
	  , p_contacttype	 IN 	  t107_contact.c107_contact_type%TYPE
	  , p_contactvalue	 IN 	  t107_contact.c107_contact_value%TYPE
	  , p_preference	 IN 	  t107_contact.c107_seq_no%TYPE
	  , p_primaryfl 	 IN 	  t107_contact.c107_primary_fl%TYPE
	  , p_inactivefl	 IN 	  t107_contact.c107_inactive_fl%TYPE
	  , p_userid		 IN 	  t101_user.c101_user_id%TYPE
	  , p_voidfl         IN       t107_contact.c107_void_fl%TYPE
	)
	AS
	-- to be deleted later
	BEGIN

		UPDATE t107_contact
		   SET c101_party_id = p_partyid
			 , c901_mode = p_contactmode
			 , c107_contact_type = p_contacttype
			 , c107_contact_value = p_contactvalue
			 , c107_seq_no = p_preference
			 , c107_primary_fl = p_primaryfl
			 , c107_inactive_fl = p_inactivefl
			 , c107_last_updated_by = p_userid
			 , c107_last_updated_date = SYSDATE
			 , c107_void_fl = p_voidfl
		 WHERE c107_contact_id = p_contactid;

		IF (SQL%ROWCOUNT = 0)
		THEN
			SELECT s107_contact.NEXTVAL
			  INTO p_contactid
			  FROM DUAL;

		
			INSERT INTO t107_contact
						(c107_contact_id, c101_party_id, c901_mode, c107_contact_type, c107_contact_value, c107_seq_no
					   , c107_primary_fl, c107_inactive_fl, c107_created_by, c107_created_date, c107_void_fl
						)
				 VALUES (p_contactid, p_partyid, p_contactmode, p_contacttype, p_contactvalue, p_preference
					   , p_primaryfl, p_inactivefl, p_userid, SYSDATE, p_voidfl
						);
		END IF;

	END gm_sav_contactinfo;

	
END gm_pkg_crm_contact;
/
