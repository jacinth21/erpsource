CREATE OR REPLACE
PACKAGE BODY GM_PKG_CRM_MERC_TXN
IS

	/*****************************************************************************************
	* Description : This Procedure is used to save MERC request JSON into appropriate table
	* 				by calling repective procedures.
	******************************************************************************************/
	PROCEDURE gm_sav_merc_dtl(
	    p_input_string IN CLOB,
	    p_user_id      IN t6670_merc.C6670_CREATED_BY%TYPE,
	    p_out_id 	   OUT VARCHAR2 )
	AS
	  v_json 	 json_object_t;
	  v_date_fmt VARCHAR2(20);
	  v_fromdate VARCHAR2(50);
	  v_enddate  VARCHAR2(50);
	BEGIN
	
	  v_date_fmt := GET_RULE_VALUE('DATEFMT', 'DATEFORMAT');
	  v_json     := json_object_t.parse(p_input_string);
	  p_out_id   := v_json.get_String('mercid');
	  v_fromdate := v_json.get_String('fromdt');
	  v_enddate  := v_json.get_String('todt');
	  
	  --CRM MERC Event master json into T6670 table
	  gm_sav_crm_merc( 
	  p_out_id, 
	  v_json.get_String('eventnm'),
	  v_json.get_String('region'),
	  v_json.get_String('type'),
	  v_json.get_String('city'),
	  v_json.get_String('state'),
	  v_json.get_String('country'),
	  v_json.get_String('status'),
	  v_json.get_String('url'),
	  v_json.get_String('opento'),
	  v_json.get_String('language'),      
	  TO_DATE(v_fromdate,v_date_fmt),
	  TO_DATE(v_enddate,v_date_fmt),
	  v_json.get_String('voidfl'),
	  p_user_id );
	  
	  -- CRM Preceporship case reqest party association json into t6671
	  gm_sav_crm_merc_attendee_json(p_out_id,treat(v_json.get('arrgmmercpartyvo') AS json_array_t),p_user_id);
	
	  
	END gm_sav_merc_dtl;
	
	/*****************************************************************************************
	* Description : This Procedure is used to update/insert into t6670 table
	******************************************************************************************/
	PROCEDURE gm_sav_crm_merc(
	    p_mercid 		IN OUT 			t6670_merc.C6670_MERC_ID%TYPE,
	    p_eventnm     	IN 				t6670_merc.C6670_MERC_NM%TYPE,
	    p_region 		IN 				t6670_merc.C901_REGION%TYPE,
	    p_type     		IN 				t6670_merc.C901_TYPE%TYPE,
	    p_city     		IN 				t6670_merc.C6670_CITY%TYPE,
	    p_state     	IN 				t6670_merc.C901_STATE%TYPE,
	    p_country     	IN 				t6670_merc.C901_COUNTRY%TYPE,
	    p_status     	IN 				t6670_merc.C901_STATUS%TYPE,
	    p_url     		IN 				t6670_merc.C6670_LINK%TYPE,
	    p_opento     	IN 				t6670_merc.C6670_OPENTO%TYPE,
	    p_language     	IN 				t6670_merc.C901_LANGUAGE%TYPE,
	    p_fromdt     	IN 				t6670_merc.C6670_FROM_DT%TYPE,
	    p_todt      	IN 				t6670_merc.C6670_TO_DT%TYPE,
        p_voidfl     	IN 				t6670_merc.C6670_VOID_FL%TYPE,
	    p_userid    	IN 				t6670_merc.C6670_CREATED_BY%TYPE)
	AS
	BEGIN
		
	  update T6670_MERC set
		C6670_MERC_NM  			= p_eventnm
		,C901_REGION 	  		= p_region
		,C901_TYPE      		= p_type
		,C6670_CITY   			= p_city
		,C901_STATE		 		= p_state
		,C901_COUNTRY      		= p_country
		,C901_STATUS    		= p_status
		,C6670_LINK 			= p_url
		,C6670_OPENTO       	= p_opento
		,C901_LANGUAGE      	= p_language
		,C6670_FROM_DT      	= p_fromdt
		,C6670_TO_DT      		= p_todt
	    ,c6670_UPDATED_BY       = p_userid
        ,C6670_VOID_FL          = p_voidfl 
	    ,c6670_UPDATED_DT       = CURRENT_DATE
		where C6670_MERC_ID = p_mercid;
	  	IF (SQL%ROWCOUNT            = 0) THEN
	  		SELECT 'GM-MERC-'|| s6660_case_request.nextval INTO p_mercid FROM DUAL; 
	    insert into T6670_MERC(
	    	 C6670_MERC_ID
			,C6670_MERC_NM
			,C901_REGION
			,C901_TYPE
			,C6670_CITY
			,C901_STATE
			,C901_COUNTRY
			,C901_STATUS
			,C6670_LINK
			,C6670_OPENTO
			,C901_LANGUAGE
			,C6670_FROM_DT
			,C6670_TO_DT
			,c6670_UPDATED_BY
	        ,c6670_UPDATED_DT
			) values (
			 p_mercid
			,p_eventnm
			,p_region
			,p_type
			,p_city
			,p_state
			,p_country
			,p_status
			,p_url
			,p_opento
			,p_language
			,p_fromdt
			,p_todt
			,p_userid
			,CURRENT_DATE
			);

	  END IF;
	END gm_sav_crm_merc;
	
	/****************************************************************
* Description : Fetch the MERC Details
*****************************************************************/
PROCEDURE  gm_fch_merc_dtl (
  p_mercid		IN 	   T6670_MERC.C6670_MERC_ID%TYPE,
  p_out_clob 	OUT  CLOB
 ) 
 AS
 v_json_out JSON_OBJECT_T;
 v_mercdtl VARCHAR2(4000);
 v_attendeeattr CLOB;
BEGIN	 
     SELECT JSON_OBJECT(
	   				'mercid' VALUE T6670.C6670_MERC_ID,
  				  	'eventnm' VALUE T6670.C6670_MERC_NM,
  				  	'typenm' VALUE get_code_name(T6670.C901_TYPE),
                    'type' VALUE T6670.C901_TYPE,
                    'fromdt' VALUE TO_CHAR(T6670.C6670_FROM_DT, 'YYYY-MM-DD'),
                    'todt' VALUE TO_CHAR(T6670.C6670_TO_DT, 'YYYY-MM-DD'),
                    'city' VALUE T6670.C6670_CITY,
                    'state' VALUE T6670.C901_STATE,
                    'language' VALUE T6670.C901_LANGUAGE,
                    'langnm' VALUE get_code_name(T6670.C901_LANGUAGE),
                    'opento' VALUE T6670.C6670_OPENTO,
                    'statenm' VALUE get_code_name(T6670.C901_STATE),
                    'region' VALUE T6670.C901_REGION,
                    'regionnm' VALUE get_code_name(T6670.C901_REGION),
                    'country' VALUE T6670.C901_COUNTRY,
                    'countrynm' VALUE get_code_name(T6670.C901_COUNTRY),
                    'url' VALUE T6670.C6670_LINK,
                    'status' VALUE T6670.C901_STATUS,
                    'statusnm' VALUE get_code_name(T6670.C901_STATUS)
  			   ) INTO v_mercdtl
  		  FROM T6670_MERC T6670 
  		  WHERE T6670.C6670_VOID_FL IS NULL
  		  AND T6670.C6670_MERC_ID = p_mercid;
  		  
  		  SELECT JSON_ARRAYAGG (
	            JSON_OBJECT(
	                'attendeeid' VALUE c6671_attendee_id ,
	                'mercid' VALUE c6670_merc_id, 
	                'partyid' VALUE c101_surgeon_party_id ,
	                'partynm' VALUE get_party_name(c101_surgeon_party_id),
	                'roleid' VALUE c901_role,
	                'statusid' VALUE c901_status,
	                'rolenm' VALUE get_code_name(c901_role),
	                'statusnm' VALUE get_code_name(C901_STATUS),
	                'voidfl' VALUE c6671_void_fl)
	                ORDER BY get_code_name(c901_role),get_party_name(c101_surgeon_party_id),c901_status RETURNING CLOB)  INTO v_attendeeattr
	    FROM T6671_merc_attendee_list 
	    WHERE C6671_VOID_FL IS NULL 
	    AND C6670_MERC_ID  = p_mercid;
	    
	v_json_out     := JSON_OBJECT_T.parse(v_mercdtl);
	
	IF v_attendeeattr IS NOT NULL THEN
		v_json_out.put('arrgmmercpartyvo',JSON_ARRAY_T(v_attendeeattr));
	END IF;
	
	p_out_clob := v_json_out.to_string;
	 	
	END gm_fch_merc_dtl;
	
	/*****************************************************************************************
	* Description : This Procedure is used to iterate merc attendee detail JSON array 
	* 				and call gm_sav_crm_merc_attendee
	******************************************************************************************/
	PROCEDURE gm_sav_crm_merc_attendee_json(
		p_mercid	 IN T6670_MERC.C6670_MERC_ID%TYPE,
		p_attr_array IN json_array_t,
		p_user_id    IN t6670_merc.C6670_CREATED_BY%TYPE)
	AS
		v_attr_obj json_object_t;
	BEGIN
		
		 FOR i IN 0 .. p_attr_array.get_size - 1
		 LOOP
		  
		    v_attr_obj := treat(p_attr_array.get(i) AS json_object_t);
		    
			gm_sav_crm_merc_attendee( v_attr_obj.get_String('attendeeid'),
									v_attr_obj.get_String('partyid'),
									p_mercid,
									v_attr_obj.get_String('roleid'),
									v_attr_obj.get_String('statusid'),
									v_attr_obj.get_String('voidfl'),
									p_user_id) ;
			
		 END LOOP;
		 
	END gm_sav_crm_merc_attendee_json;
	
	/*****************************************************************************************
	* Description : This Procedure is used to update/insert into t6671  table
	******************************************************************************************/
	PROCEDURE gm_sav_crm_merc_attendee
	  (
	    p_attendeeid    IN 		t6671_merc_attendee_list.c6671_attendee_id%TYPE,
    	p_partyid     	IN 		t6671_merc_attendee_list.c101_surgeon_party_id%TYPE,
	    p_mercid 		IN 		t6671_merc_attendee_list.c6670_merc_id%TYPE,
	    p_roleid 		IN 		t6671_merc_attendee_list.c901_role%TYPE,
    	p_statusid    	IN 		t6671_merc_attendee_list.c901_status%TYPE,
	    p_voidfl    	IN 		t6671_merc_attendee_list.c6671_void_fl%TYPE,
	    p_userid		IN 		t6671_merc_attendee_list.c6671_updated_by%TYPE
	  )
	AS
	BEGIN
	  UPDATE t6671_merc_attendee_list
	  SET c101_surgeon_party_id        = p_partyid ,
	      c6670_merc_id                = p_mercid ,
	      c901_role                	   = p_roleid ,
	      c901_status                  = p_statusid ,
	      c6671_void_fl                = p_voidfl ,
	      c6671_updated_by             = p_userid ,
	      c6671_updated_dt             = CURRENT_DATE
	    
	  WHERE c6671_attendee_id = p_attendeeid
  	  AND 	c6671_void_fl  IS NULL;
	  
	  IF (SQL%ROWCOUNT = 0) 
	  THEN
	    INSERT
	    INTO t6671_merc_attendee_list
	      (
	        c101_surgeon_party_id,
	        c6670_merc_id,
	        c901_role,
	        c901_status,
	        c6671_void_fl,
	        c6671_updated_by,
	        c6671_updated_dt
	      )
	      VALUES
	      (
	        p_partyid,
	        p_mercid,
	        p_roleid,
	        p_statusid,
	        p_voidfl,
	        p_userid,
	        CURRENT_DATE
	      );
	  END IF;
	  
	END gm_sav_crm_merc_attendee;
	
END GM_PKG_CRM_MERC_TXN;
/