CREATE OR REPLACE
PACKAGE BODY gm_pkg_crm_preceptorship_rpt
IS
/****************************************************************
* Description : Fetch the Case Request Information in Preceptorship Module
*****************************************************************/
PROCEDURE GM_FCH_PS_CASE_INFO_JSON  (
		p_casereqid				IN T6660_CASE_REQUEST.C6660_CASE_REQUEST_ID%TYPE,
 		p_out_str  			OUT CLOB
 )	
 AS	
 v_json_out JSON_OBJECT_T;
 v_casereq VARCHAR2(4000);
 v_casereqass VARCHAR2(4000);
 v_casereqdtl VARCHAR2(4000);
 v_casereqattr VARCHAR2(4000);
BEGIN

SELECT JSON_OBJECT(
'actid' VALUE C6630_ACTIVITY_ID,
'casereqid' VALUE C6660_CASE_REQUEST_ID,
'crdfl' VALUE C6660_CREDENTIAL_FL,
'createdby' IS Get_User_Name(C6660_Created_By),
'createdbyid' IS C6660_Created_By,
'createdon' VALUE TO_CHAR(C6660_CREATED_DT,'yyyy-mm-dd'),
'frmdt' VALUE TO_CHAR(C6660_FROM_DT,'yyyy-mm-dd'),
'hotlnm' VALUE C6660_HOTEL,
'hotlprice' VALUE C6660_HOTEL_PRICE,
'labadd' VALUE C6660_LAB_ADDRESS,
'labinv' VALUE C6660_LAB_INVOICE,
'type' VALUE C901_TYPE ,
'note' VALUE C6660_NOTE,
'pcmninv' VALUE C6660_PECIMEN_INVOICE,
'pcprcrf' VALUE C6660_PCRF_CRF_STATUS,
'segid' VALUE C2090_SEGMENT_ID,
'status' VALUE C901_STATUS,
'surgid' VALUE C101_SURGEON_PARTY_ID,
'survfl' VALUE C6660_SURVEY_FL,
'todt' VALUE TO_CHAR(C6660_TO_DT,'yyyy-mm-dd'),
'trfstatis' VALUE C6660_TRF_STATUS,
'trvlnm' VALUE C6660_TRAVEL,
'trvlprice' VALUE C6660_TRAVEL_PRICE,
'surgnm' VALUE GET_Party_NAME(C101_SURGEON_PARTY_ID),
'procnm' VALUE Get_Segment_Name(C2090_SEGMENT_ID),
'crfnm' VALUE Get_Code_Name(C6660_PCRF_CRF_STATUS),
'trfnm' VALUE Get_Code_Name(C6660_TRF_STATUS),
'division' VALUE C1910_DIVISION_ID ,
'workflow' VALUE C901_WORK_FLOW ,
'pathology' VALUE C6660_PATHOLOGY ,
'dvnm' VALUE Get_Division_Name(C1910_DIVISION_ID) ,
'wfnm' VALUE Get_Code_Name(C901_WORK_FLOW) ,
'statusnm' VALUE Get_Code_Name(C901_STATUS) ,
'pstype' VALUE C901_PRECEPTOR_TYPE ,
'clobj' VALUE C6660_CALL_OBJECTIVES ,
'pstypenm' VALUE Get_Code_Name(C901_PRECEPTOR_TYPE) ,
'surgeoncv' VALUE Get_Surgeon_Cv_Path(C101_SURGEON_PARTY_ID),
'region' VALUE C901_REGION ,
'regionnm' VALUE Get_Code_Name(C901_REGION),
'zone' VALUE C901_ZONE,
'zonenm' VALUE Get_Code_Name(C901_ZONE),
'adminsts' VALUE C901_ADMIN_STATUS,
'adminstsnm' VALUE Get_Code_Name(C901_ADMIN_STATUS),
'atndtyp' VALUE C901_ATTENDEE_TYPE,
'atndtypnm' VALUE Get_Code_Name(C901_ATTENDEE_TYPE),
'atndnm' VALUE C6630_ATTENDEE_NAME,
'webinarfl' VALUE C6660_WEBINAR_FL
)
	INTO v_casereq
	FROM T6660_CASE_REQUEST
	WHERE C6660_VOID_FL  IS NULL
	AND C6660_CASE_REQUEST_ID = p_casereqid; 
	
	
	SELECT JSON_ARRAYAGG (
	            JSON_OBJECT(
	                'craid' VALUE T6661.C6661_CASE_REQ_ASSOC_ID ,
	                'crid' VALUE T6661.C6660_CASE_REQUEST_ID, 
	                'role' VALUE T6661.C901_ROLE ,
	                'partyid' VALUE  T6661.C101_PARTY_ID,
	                'partynm' VALUE  GET_PARTY_NAME(T6661.C101_PARTY_ID),
			'adnm' VALUE V703.AD_NAME,
			'vpnm' VALUE V703.VP_NAME)
	                )  INTO v_casereqass
	    FROM T6661_CASE_REQUEST_ASSOC T6661, (SELECT DISTINCT REP_ID, AD_NAME, VP_NAME, REP_PARTYID  FROM  V703_REP_MAPPING_DETAIL) V703
	    WHERE T6661.C6661_VOID_FL IS NULL
      	    AND T6661.C101_PARTY_ID = V703.REP_PARTYID(+)
	    AND T6661.C6660_CASE_REQUEST_ID  = p_casereqid;
	    
	SELECT JSON_ARRAYAGG (
	            JSON_OBJECT(
	                'crdid' VALUE C6662_CASE_REQ_DETAIL_ID ,
	                'crid' VALUE C6660_CASE_REQUEST_ID, 
	                'setid' VALUE C207_SET_ID ,
	                'setnm' VALUE C6662_SET_NM )
	                )  INTO v_casereqdtl  
	    FROM T6662_CASE_REQUEST_DETAIL 
	    WHERE C6662_VOID_FL IS NULL 
	    AND C6660_CASE_REQUEST_ID  = p_casereqid;
	    
	SELECT JSON_ARRAYAGG (
	            JSON_OBJECT(
	                'attrid' VALUE C6663_case_req_attr_id ,
	                'attrnm' VALUE c6663_attribute_nm, 
	                'attrval' VALUE c6663_attribute_value ,
	                'attrtype' VALUE c901_attribute_type )
	                )  INTO v_casereqattr
	    FROM t6663_case_req_attr 
	    WHERE C6663_VOID_FL IS NULL 
	    AND C6660_CASE_REQUEST_ID  = p_casereqid;
	    
	v_json_out     := JSON_OBJECT_T.parse(v_casereq);
	
	IF v_casereqass IS NOT NULL THEN
		v_json_out.put('arrcrmpscaseassvo',JSON_ARRAY_T(v_casereqass));
	END IF;
	
	IF v_casereqdtl IS NOT NULL THEN
		v_json_out.put('arrcrmpscasereqvo',JSON_ARRAY_T(v_casereqdtl));
	END IF;
	
	IF v_casereqattr IS NOT NULL THEN
		v_json_out.put('arrcrmpscaseattrvo',JSON_ARRAY_T(v_casereqattr));
	END IF;
	
	p_out_str := v_json_out.to_string;

END GM_FCH_PS_CASE_INFO_JSON;

 /****************************************************************
* Description : Fetch the Host case List based on segment id, activity category
*****************************************************************/
PROCEDURE gm_fch_ps_lkp_host_info_json (
		p_segid				IN T6630_ACTIVITY.C2090_SEGMENT_ID%TYPE,
		p_actcategory 		IN T6630_ACTIVITY.C901_ACTIVITY_CATEGORY%TYPE,
 		p_out_str  			OUT CLOB )
 	AS	 	
BEGIN
	
	SELECT JSON_ARRAYAGG( 
		JSON_OBJECT( 
		'actid' VALUE v6630.c6630_activity_id,
		'acttype' VALUE v6630.c901_activity_type,
		'acttypenm' VALUE GET_CODE_NAME(v6630.C901_Activity_Type),
	  	'partyid' VALUE v6630.surgeonids,
	  	'partynm' VALUE v6630.surgeonnms,
	  	'segid' VALUE v6630.c2090_segment_id,
	  	'segnm' VALUE GET_SEGMENT_NAME(v6630.c2090_segment_id),
	  	'products' VALUE v6630.prodnms,
	  	'statusnm' VALUE GET_CODE_NAME(c901_activity_status),
	  	'actstartdate' VALUE TO_CHAR(v6630.c6630_from_dt,'yyyy-mm-dd') )
	ORDER BY v6630.c6630_from_dt RETURNING CLOB) INTO p_out_str
	FROM v6630_activity_dtl v6630
	WHERE v6630.C2090_Segment_Id = p_segid                  --18 --TLIF
	AND v6630.C6630_From_Dt     >= TRUNC(CURRENT_DATE) -- show future case only
	AND v6630.C901_Activity_Status IN ('105765','105766');	
	
END gm_fch_ps_lkp_host_info_json;


 /****************************************************************
* Description : Fetch the Host case List based on segment id, activity category, activity status
*****************************************************************/
PROCEDURE gm_fch_ps_lkp_case_req_json (
		p_segid				IN T6660_CASE_REQUEST.C2090_SEGMENT_ID%TYPE,
		p_actid	         	IN T6660_CASE_REQUEST.C6630_ACTIVITY_ID%TYPE,
		p_user_id    		IN t101_user.c101_user_id%TYPE,
 		p_out_str  			OUT CLOB )
 	AS	 	
BEGIN
 	SELECT JSON_ARRAYAGG(JSON_OBJECT(
	'casereqid'        VALUE v6660.C6660_CASE_REQUEST_ID,
	'actid'			   VALUE v6660.C6630_ACTIVITY_ID,
	'surgid'           VALUE v6660.C101_SURGEON_PARTY_ID, 
 	'surgnm'           VALUE GET_PARTY_NAME (v6660.C101_SURGEON_PARTY_ID), 
 	'segnm'            VALUE Get_Segment_Name(v6660.C2090_Segment_Id), 
    'products'         VALUE v6660.SETNAMES,
    'frmdt'            VALUE TO_CHAR(v6660.C6660_FROM_DT,'MM/DD/YYYY'),
    'todt'       	   VALUE TO_CHAR(v6660.C6660_TO_DT,'MM/DD/YYYY'),
    'status'		   VALUE v6660.C901_STATUS,
    'note'    		   VALUE v6660.C6660_NOTE,
    'atndtyp'    	   VALUE v6660.C901_ATTENDEE_TYPE,
	'atndnm'    	   VALUE v6660.C6630_ATTENDEE_NAME ,   
    'accessfl'		   VALUE get_casereq_access_flag(p_actid,v6660.c101_surgeon_party_id,p_user_id)
	)ORDER BY v6660.C6630_ACTIVITY_ID, TO_NUMBER(SUBSTR(v6660.C6660_CASE_REQUEST_ID,4)) RETURNING CLOB) INTO p_out_str FROM V6660_Case_Req_Dlt v6660
	WHERE v6660.C6660_VOID_FL         IS NULL
	AND v6660.C2090_Segment_Id      =  p_segid --18  
	AND (v6660.C901_STATUS          =  '105765' OR ((v6660.C901_STATUS = '105761' OR v6660.C901_STATUS = '105764') AND v6660.C6630_ACTIVITY_ID = p_actid));
	
END gm_fch_ps_lkp_case_req_json;

/****************************************************************
* Description : Fetch the Case Request Information in Preceptorship Module
*****************************************************************/
PROCEDURE gm_fch_ps_case_info(
		p_casereqid			IN T6660_CASE_REQUEST.C6660_CASE_REQUEST_ID%TYPE,
 		p_case_req  		OUT TYPES.CURSOR_TYPE,
 		P_case_req_asso		OUT TYPES.CURSOR_TYPE,
 		p_case_req_dtl		OUT TYPES.CURSOR_TYPE,
 		p_case_req_attr		OUT TYPES.CURSOR_TYPE,
 		p_case_req_file		OUT TYPES.CURSOR_TYPE
 )
 AS
 BEGIN
	 OPEN p_case_req FOR
		SELECT C6660_CASE_REQUEST_ID ACTID,
		  TO_CHAR(C6660_FROM_DT,'MM/DD/YYYY') FRMDATE,
		  TO_CHAR(C6660_TO_DT,'MM/DD/YYYY') TODATE,
		  GET_PARTY_NAME(C101_SURGEON_PARTY_ID) ACTNM ,
		  TO_CHAR(C6660_FROM_DT,'HH24:MI:SS') FRMTIME ,
		  C901_TYPE CATEGORY,
		  TO_CHAR(C6660_TO_DT,'HH24:MI:SS') TOTIME ,
		  GET_PARTY_NAME(C101_SURGEON_PARTY_ID) PARTYNM,
		  GET_SURGEON_CV_PATH(C101_SURGEON_PARTY_ID) SURGEONCV,
		  GET_SEGMENT_NAME(C2090_SEGMENT_ID) SEGNM,
		  GET_USER_NAME(C6660_CREATED_BY) ORGNM,
		  TO_CHAR(C6660_CREATED_DT,'MM/DD/YYYY') CREATEDDATE ,
		  GET_USER_EMAILID(C6660_CREATED_BY) ORGMAIL,
		  C6660_NOTE NOTE
		FROM T6660_CASE_REQUEST
		WHERE C6660_VOID_FL      IS NULL
		AND C6660_CASE_REQUEST_ID = p_casereqid;	 
	 
	 OPEN P_case_req_asso FOR
		SELECT GET_PARTY_NAME(t6661.C101_PARTY_ID) PARTYNM,
		  c101_email_id EMAILID
		FROM T6661_CASE_REQUEST_ASSOC t6661,
		  t101_user t101
		WHERE t6661.c101_party_id = t101.c101_party_id
		AND C6661_VOID_FL        IS NULL
		AND C901_ROLE             = '105561'
		AND C6660_CASE_REQUEST_ID = p_casereqid;
		
	OPEN p_case_req_dtl FOR
		SELECT C207_SET_ID SETID,
		  C6662_Set_Nm SETNM
		FROM T6662_Case_Request_Detail
		WHERE C6662_VOID_FL      IS NULL
		And C6660_Case_Request_Id = p_casereqid;
		
	OPEN p_case_req_attr FOR
		SELECT C6663_Attribute_Value ATTRVALUE,
		  C6663_Attribute_Nm ATTRNM
		FROM T6663_Case_Req_Attr
		WHERE c6663_void_fl      IS NULL
		AND C6660_Case_Request_Id = p_casereqid ;
	
	OPEN p_case_req_file FOR
		SELECT C903_UPLOAD_FILE_LIST FILEID, C903_REF_ID REFID, C903_FILE_NAME FILENAME, C901_REF_TYPE REFTYPE,
  			GET_CODE_NAME(C901_REF_TYPE) REFTYPENM, C901_TYPE TYPE, C901_REF_GRP REFGROUP, C901_FILE_TITLE FILETITLE,
  			TO_CHAR(DECODE(C903_LAST_UPDATED_DATE,NULL,C903_CREATED_DATE,C903_LAST_UPDATED_DATE),GET_RULE_VALUE('DATEFMT','DATEFORMAT')) UPDATEDDATE,
  			GET_USER_NAME(DECODE(C903_LAST_UPDATED_BY,NULL,C903_CREATED_BY,C903_LAST_UPDATED_BY)) UPDATEDBY
		FROM T903_UPLOAD_FILE_LIST
		WHERE C903_DELETE_FL IS NULL 
		AND C901_REF_GRP     IN ('107361') -- 107361 - Case Requestor uploaded Files
		AND C903_REF_ID      = p_casereqid
		ORDER BY DECODE(C903_LAST_UPDATED_DATE,NULL,C903_CREATED_DATE) DESC ;
		
END gm_fch_ps_case_info; 
/*********************************************************************************************************
* Description : This function is used to whether show Surgeon name in Preceptorship Calendar detail or not
*********************************************************************************************************/
 FUNCTION get_casereq_access_flag (
      p_act_id     IN   t6630_activity.c6630_activity_id%TYPE,
      p_party_id   IN   t101_party.c101_party_id%TYPE,
      p_user_id    IN   t101_user.c101_user_id%TYPE
)
      RETURN VARCHAR2
IS
      v_access_fl  VARCHAR2(1):= 'N';
      v_cnt 	   NUMBER;
   BEGIN
		SELECT COUNT(1) INTO v_cnt
		FROM t6660_case_request t6660,
		  t6661_case_request_assoc t6661
		WHERE t6660.c6630_activity_id   = p_act_id
		AND t6660.c101_surgeon_party_id = p_party_id
		AND t6660.c6660_case_request_id = t6661.c6660_case_request_id
		AND t6661.c101_party_id        IN
		  ( SELECT c101_party_id FROM t101_user WHERE c101_user_id = p_user_id
		  )
		AND t6661.c6661_void_fl        IS NULL
		AND t6660.c6660_void_fl        IS NULL;
	 IF v_cnt > 0 THEN
	 	v_access_fl:='Y';
	 END IF;
     RETURN v_access_fl;
     
     EXCEPTION WHEN OTHERS
         THEN
             RETURN 'N';	 
END get_casereq_access_flag;

/*********************************************************************************************************
* Description : This function is used to fetch preceptorship dash chart data
*********************************************************************************************************/
PROCEDURE  gm_fch_hostcase_chart_json (
      p_out_clob 			OUT  	CLOB
)
AS 
	  v_hostchartspine	        	CLOB;
 	  v_hostchartinr	 	 		CLOB;
 	  v_casechartspine	        	CLOB;
 	  v_casechartinr	 	 		CLOB;
 	  v_json_out 	     	 		json_object_t;
BEGIN
	SELECT JSON_ARRAYAGG ( 
     JSON_OBJECT( 
     'label' VALUE T901.C901_CODE_NM, 
     'value' VALUE NVL(T6630.COUNT,0) , 
     'link' VALUE '#crm/preceptorship/HC/2000/' || T6630.STATUSID 
     )ORDER BY T901.C901_CODE_NM ASC
     RETURNING CLOB ) 
     INTO v_hostchartspine FROM T901_CODE_LOOKUP T901, 
     (SELECT   COUNT(C6630_ACTIVITY_ID) COUNT, 
               C901_ADMIN_STATUS        STATUSID 
               FROM     T6630_ACTIVITY 
               WHERE    C901_ADMIN_STATUS IS NOT NULL 
               AND      C6630_VOID_FL IS NULL 
               AND      C1910_DIVISION_ID = '2000' 
               AND      C6630_FROM_DT >= TRUNC(CURRENT_DATE) 
               GROUP BY C901_ADMIN_STATUS)T6630 
	  WHERE  T901.C901_CODE_ID = T6630.STATUSID(+) 
	  AND    T901.C901_CODE_GRP ='PCTADS' 
      AND    T901.C901_ACTIVE_FL =1 
      AND    T901.C901_VOID_FL IS NULL;
     
     
     SELECT JSON_ARRAYAGG ( 
     JSON_OBJECT( 'label' VALUE T901.C901_CODE_NM, 
     'value' VALUE NVL(T6630.COUNT,0) , 
     'link' VALUE '#crm/preceptorship/HC/2004/' || T6630.STATUSID 
     )ORDER BY T901.C901_CODE_NM ASC
     RETURNING CLOB ) 
     INTO v_hostchartinr FROM T901_CODE_LOOKUP T901, 
     (SELECT   COUNT(C6630_ACTIVITY_ID) COUNT, 
               C901_ADMIN_STATUS        STATUSID 
               FROM     T6630_ACTIVITY 
               WHERE    C901_ADMIN_STATUS IS NOT NULL 
               AND      C6630_VOID_FL IS NULL 
               AND      C1910_DIVISION_ID = '2004' 
               AND      C6630_FROM_DT >= TRUNC(CURRENT_DATE) 
               GROUP BY C901_ADMIN_STATUS)T6630 
	  WHERE  T901.C901_CODE_ID = T6630.STATUSID(+) 
	  AND    T901.C901_CODE_GRP ='PCTADS' 
      AND    T901.C901_ACTIVE_FL =1 
      AND    T901.C901_VOID_FL IS NULL;
     
     SELECT JSON_ARRAYAGG ( 
     JSON_OBJECT( 'label' VALUE T901.C901_CODE_NM, 
     'value' VALUE NVL(T6660.COUNT,0) , 
     'link' VALUE '#crm/preceptorship/CR/2000/' || T6660.STATUSID 
     )ORDER BY T901.C901_CODE_NM ASC
     RETURNING CLOB ) 
     INTO v_casechartspine FROM T901_CODE_LOOKUP T901, 
     (SELECT   COUNT(C6660_CASE_REQUEST_ID) COUNT, 
               C901_ADMIN_STATUS        STATUSID 
               FROM     T6660_CASE_REQUEST
               WHERE    C901_ADMIN_STATUS IS NOT NULL 
               AND      C6660_VOID_FL IS NULL 
               AND      C1910_DIVISION_ID = '2000' 
               AND      C901_STATUS != '105764' -- Exclude completed status
               GROUP BY C901_ADMIN_STATUS)T6660 
	  WHERE  T901.C901_CODE_ID = T6660.STATUSID(+) 
	  AND    T901.C901_CODE_GRP ='PCTADS' 
      AND    T901.C901_ACTIVE_FL =1 
      AND    T901.C901_VOID_FL IS NULL;
     
     SELECT JSON_ARRAYAGG ( 
     JSON_OBJECT( 'label' VALUE T901.C901_CODE_NM, 
     'value' VALUE NVL(T6660.COUNT,0) , 
     'link' VALUE '#crm/preceptorship/CR/2004/' || T6660.STATUSID 
     )ORDER BY T901.C901_CODE_NM ASC
     RETURNING CLOB ) 
     INTO v_casechartinr FROM T901_CODE_LOOKUP T901, 
     (SELECT   COUNT(C6660_CASE_REQUEST_ID) COUNT, 
               C901_ADMIN_STATUS        STATUSID 
               FROM     T6660_CASE_REQUEST
               WHERE    C901_ADMIN_STATUS IS NOT NULL 
               AND      C6660_VOID_FL IS NULL 
               AND      C1910_DIVISION_ID = '2004' 
               AND      C901_STATUS != '105764' -- Exclude completed status
               GROUP BY C901_ADMIN_STATUS)T6660 
	  WHERE  T901.C901_CODE_ID = T6660.STATUSID(+) 
	  AND    T901.C901_CODE_GRP ='PCTADS' 
      AND    T901.C901_ACTIVE_FL =1 
      AND    T901.C901_VOID_FL IS NULL;
     
     v_json_out     := JSON_OBJECT_T.parse('{}');
     IF v_hostchartspine IS NOT NULL THEN
		v_json_out.put('hostspine',JSON_ARRAY_T(v_hostchartspine));
	 END IF; 
	
	 IF v_hostchartinr IS NOT NULL THEN
		v_json_out.put('hostinr',JSON_ARRAY_T(v_hostchartinr));
	 END IF; 
	 
	 IF v_casechartspine IS NOT NULL THEN
		v_json_out.put('casespine',JSON_ARRAY_T(v_casechartspine));
	 END IF; 
	
	 IF v_casechartinr IS NOT NULL THEN
		v_json_out.put('caseinr',JSON_ARRAY_T(v_casechartinr));
	 END IF; 
	
	 p_out_clob := v_json_out.to_string;

END gm_fch_hostcase_chart_json;

/*********************************************************************************************************
* Description : This function is used to fetch more filter options to the calendar
*********************************************************************************************************/
PROCEDURE gm_fch_ps_more_filter_json  (
		p_region_id     IN   t6630_activity.C6630_ACTIVITY_ID%TYPE,
		p_out_str  		OUT  CLOB
 )
 AS
	v_status	    CLOB;
	v_division	 	CLOB;
	v_zone	        CLOB;
	v_procedure	 	CLOB;
	v_workflow	 	CLOB;
	v_json_out	    json_object_t;
 BEGIN
 
   SELECT JSON_ARRAYAGG ( 
   JSON_OBJECT(
   		'id'   VALUE T901.C901_CODE_ID,
   		'name' VALUE T901.C901_CODE_NM
   )ORDER BY T901.C901_CODE_SEQ_NO,T901.C901_CODE_NM ASC RETURNING CLOB ) 
   INTO v_status FROM T901_CODE_LOOKUP T901, T901A_LOOKUP_ACCESS T901A
   WHERE T901.c901_code_grp = 'ACTSTS'
   AND T901A.C901_CODE_ID = T901.C901_CODE_ID
   AND T901A.C901_ACCESS_CODE_ID = '105272' --Preceptorship Host Case Type
   AND T901.c901_active_fl = '1'
   AND T901.c901_void_fl IS NULL;

 

   SELECT JSON_ARRAYAGG ( 
   JSON_OBJECT(
   		'id'   VALUE T901.C902_CODE_NM_ALT,
   		'name' VALUE T901.C901_CODE_NM
   )ORDER BY  T901.C901_CODE_SEQ_NO, T901.C901_CODE_NM ASC RETURNING CLOB ) 
   INTO v_division FROM T901_CODE_LOOKUP T901, T901A_LOOKUP_ACCESS T901A
   WHERE T901.c901_code_grp = 'DIVLST'
   AND T901A.C901_CODE_ID = T901.C901_CODE_ID
   AND T901A.C901_ACCESS_CODE_ID = '105272' --Preceptorship Host Case Type
   AND T901.c901_active_fl = '1'
   AND T901.c901_void_fl IS NULL;


   SELECT JSON_ARRAYAGG ( 
   JSON_OBJECT(
   		'id'   VALUE V700.ID,
   		'name' VALUE V700.NAME
   )ORDER BY V700.NAME ASC RETURNING CLOB ) 
   INTO v_zone FROM (SELECT  GP_ID ID,GP_NAME NAME FROM V700_TERRITORY_MAPPING_DETAIL 
   WHERE DIVID = p_region_id GROUP BY GP_ID,GP_NAME)V700; 


   SELECT JSON_ARRAYAGG ( 
   JSON_OBJECT(
   		'id'   VALUE T2090.C2090_SEGMENT_ID,
   		'name' VALUE T2090.C2090_SEGMENT_NAME
   )ORDER BY T2090.C2090_SEGMENT_NAME ASC RETURNING CLOB ) 
   INTO v_procedure FROM T2090_SEGMENT T2090
   WHERE c2090_void_fl IS NULL;
     

   SELECT JSON_ARRAYAGG ( 
   JSON_OBJECT(
   		'id'   VALUE T901.C901_CODE_ID,
   		'name' VALUE T901.C901_CODE_NM
   )ORDER BY T901.C901_CODE_NM ASC RETURNING CLOB ) 
   INTO v_workflow FROM T901_CODE_LOOKUP T901
   WHERE c901_code_grp = 'PRWKFL'
   AND c901_active_fl = '1'
   AND c901_void_fl IS NULL;
 
   v_json_out     := JSON_OBJECT_T.parse('{}');

   IF v_status IS NOT NULL THEN
		v_json_out.put('status',JSON_ARRAY_T(v_status));
   END IF;
   IF v_division IS NOT NULL THEN
		v_json_out.put('division',JSON_ARRAY_T(v_division));
   END IF;
   IF v_zone IS NOT NULL THEN
		v_json_out.put('zone',JSON_ARRAY_T(v_zone));
   END IF;

   IF v_procedure IS NOT NULL THEN
		v_json_out.put('procedure',JSON_ARRAY_T(v_procedure));
   END IF;

   IF v_workflow IS NOT NULL THEN
		v_json_out.put('workflow',JSON_ARRAY_T(v_workflow));
   END IF;

p_out_str := v_json_out.to_string;
	
END gm_fch_ps_more_filter_json;

END gm_pkg_crm_preceptorship_rpt;
/