-- @"c:\database\packages\crm\gm_pkg_crm_crf.bdy"


CREATE OR REPLACE PACKAGE BODY gm_pkg_crm_crf
IS
	PROCEDURE gm_crm_sav_crf (
		p_crfid					IN	OUT		t6605_crf.c6605_crf_id%type
		,p_partyid				IN			t6605_crf.c101_party_id%type
		,p_actid				IN			t6605_crf.c6630_activity_id%type
		,p_reqtype				IN			t6605_crf.c901_request_type%type
		,p_reqdate				IN			t6605_crf.c6605_request_date%type
		,p_statusid				IN			t6605_crf.c901_status%type
		,p_servicetyp			IN			t6605_crf.c6605_service_type%type
		,p_reason				IN			t6605_crf.c6605_reason%type
		,p_servicedesc			IN			t6605_crf.c6605_service_description%type
		,p_hcpqualification		IN			t6605_crf.c6605_hcp_qualification%type
		,p_preptime				IN			t6605_crf.c6605_prep_time%type
		,p_workhrs				IN			t6605_crf.c6605_work_hours%type
		,p_timefl				IN			t6605_crf.c6605_time_fl%type
		,p_agrfl				IN			t6605_crf.c6605_agreement_fl%type
		,p_trvlfrom				IN			t6605_crf.c6605_travel_from%type
		,p_trvlto				IN			t6605_crf.c6605_travel_to%type
		,p_trvltime				IN			t6605_crf.c6605_travel_time%type
		,p_trvlmile				IN			t6605_crf.c6605_travel_miles%type
		,p_hrrate				IN			t6605_crf.c6605_hour_rate%type
		,p_honstatus			IN			t6605_crf.c6605_honoraria_status%type
		,p_honworkhrs			IN			t6605_crf.c6605_honoraria_work_hrs%type
		,p_honprephrs			IN			t6605_crf.c6605_honoraria_prep_hrs%type
		,p_honcomment			IN			t6605_crf.c6605_honoraria_comment%type
		,p_voidfl				IN			t6605_crf.c6605_void_fl%type
		,p_priorityfl			IN			t6605_crf.c6605_priority_fl%type
		,p_userid				IN			t6605_crf.c6605_created_by%type
		
	) AS
	BEGIN
		UPDATE t6605_crf
		SET
			c901_request_type = p_reqtype,
			c901_status = p_statusid,
			c6605_service_type = p_servicetyp,
			c6605_reason = p_reason,
			c6605_request_date = p_reqdate,
			c6605_service_description = p_servicedesc,
			c6605_hcp_qualification = p_hcpqualification,
			c6605_prep_time = p_preptime,
			c6605_work_hours = p_workhrs,
			c6605_time_fl = p_timefl,
			c6605_agreement_fl = p_agrfl,
			c6605_travel_from = p_trvlfrom,
			c6605_travel_to = p_trvlto,
			c6605_travel_time = p_trvltime,
			c6605_travel_miles = p_trvlmile,
			c6605_hour_rate = p_hrrate,
			c6605_honoraria_status = p_honstatus,
			c6605_honoraria_work_hrs = p_honworkhrs,
			c6605_honoraria_prep_hrs = p_honprephrs,
			c6605_honoraria_comment = p_honcomment,
			c6605_void_fl = p_voidfl,
			c6605_priority_fl = p_priorityfl,
			c6605_last_updated_by = p_userid,
			c6605_last_updated_date = sysdate
		WHERE c101_party_id = p_partyid
		AND c6630_activity_id = p_actid;
		
		IF (SQL%ROWCOUNT = 0)
		THEN
			SELECT s6605_crf.NEXTVAL
			  INTO p_crfid
			  FROM DUAL;
			  
			  INSERT INTO t6605_crf (
			  c6605_crf_id,
			  c101_party_id,
			  c6630_activity_id, 
			  c901_request_type,
			  c6605_request_date,
			  c901_status,
			  c6605_service_type,
			  c6605_reason, 
			  c6605_service_description, 
			  c6605_hcp_qualification,
			  c6605_prep_time, 
			  c6605_work_hours,
			  c6605_time_fl, 
			  c6605_agreement_fl,
			  c6605_travel_from, 
			  c6605_travel_to,
			  c6605_travel_time,
			  c6605_travel_miles,
			  c6605_hour_rate, 
			  c6605_honoraria_status,
			  c6605_honoraria_work_hrs,
			  c6605_honoraria_prep_hrs,
			  c6605_honoraria_comment,
			  c6605_void_fl,
			  c6605_priority_fl,
			  c6605_created_by, 
			  c6605_created_date)
			  VALUES (p_crfid, p_partyid,
			  p_actid, p_reqtype, p_reqdate, p_statusid, 
			  p_servicetyp, p_reason, 
			  p_servicedesc, p_hcpqualification, 
			  p_preptime, p_workhrs, p_timefl, 
			  p_agrfl, p_trvlfrom, p_trvlto,
			  p_trvltime, p_trvlmile,
			  p_hrrate, p_honstatus, 
			  p_honworkhrs, p_honprephrs, 
			  p_honcomment,p_voidfl,p_priorityfl, 
			  p_userid, sysdate);
			  
		
			DECLARE                          
		        CURSOR USER_DETAILS IS
					select distinct(gm.c101_mapped_party_id) party_id  from t1501_group_mapping gm, t1530_access acc
					where gm.c1500_group_id = acc.c1500_group_id and acc.c1520_function_id = 'CRMADMIN-CRFAPPR' and gm.c101_mapped_party_id is not null and gm.c1501_void_fl is null;
		          BEGIN
		              FOR PARTY IN USER_DETAILS
		              LOOP                             
		                    BEGIN                  
		                       INSERT INTO T6605A_CRF_APPROVAL (C6605A_CRF_APPROVAL_ID, C6605_CRF_ID, C101_PARTY_ID, C901_STATUS, C6605A_COMMENTS, C6605A_CREATED_BY, C6605A_CREATED_DATE) 
			  					VALUES (S6605A_CRF_APPOVAL_ID.NEXTVAL, p_crfid, PARTY.PARTY_ID,p_statusid,NULL, p_userid, sysdate);
		                    END;
		              END LOOP;                   
		      END;
		      
		      ELSE 
		      UPDATE T6605A_CRF_APPROVAL SET C901_STATUS = p_statusid WHERE C6605_CRF_ID = p_crfid;
			  END IF;
	END gm_crm_sav_crf;
	
	
	PROCEDURE gm_crm_set_crf_appr (
		p_crfapprid				IN			t6605a_crf_approval.c6605a_crf_approval_id%type,
		p_crfid					IN			t6605a_crf_approval.c6605_crf_id%type,
		p_statusid				IN			t6605a_crf_approval.c901_status%type,
		p_comments				IN			t6605a_crf_approval.c6605a_comments%type
--		v_approvals				OUT			TYPES.cursor_type
	) AS
	
	BEGIN
		UPDATE T6605A_CRF_APPROVAL 
		SET C901_STATUS = p_statusid,
		C6605A_COMMENTS = p_comments,
		C6605A_LAST_UPDATED_BY = p_crfapprid,
		C6605A_LAST_UPDATED_DATE = sysdate
		WHERE C6605A_CRF_APPROVAL_ID = p_crfapprid;	
		
		gm_crm_set_crf_status(p_crfid);

	END gm_crm_set_crf_appr;
	
PROCEDURE gm_crm_set_crf_status (
              p_crfid                                  IN                   t6605a_crf_approval.c6605_crf_id%type
       ) AS
       v_status NUMBER;
       v_appr_cnt    NUMBER := 0;
       v_denied_cnt  NUMBER := 0;
       v_tot_appr_cnt NUMBER := 0;
       BEGIN
              v_status := 105207;-- Pending approval
              DECLARE                          
                      CURSOR v_approvals IS
                     SELECT T6605A.C6605A_CRF_APPROVAL_ID CRFAPPRID, T6605A.C6605_CRF_ID CRFID, 
                     T6605A.C101_PARTY_ID PARTYID, (T101.C101_FIRST_NM || ' ' || T101.C101_LAST_NM) PARTYNM,
                     T6605A.C901_STATUS STATUSID, GET_CODE_NAME(T6605A.C901_STATUS) STATUSNM,
                     T6605A.C6605A_COMMENTS COMMENTS
                     FROM T6605A_CRF_APPROVAL T6605A, T101_PARTY T101
                     WHERE T101.C101_PARTY_ID = T6605A.C101_PARTY_ID
                     AND T101.C101_VOID_FL IS NULL 
                     AND T6605A.C6605_CRF_ID = p_crfid;
                     
                 
                     
                     BEGIN
                     --
                      SELECT COUNT(1) INTO v_tot_appr_cnt
                  FROM t1501_group_mapping gm,
                  t1530_access acc
                  WHERE gm.c1500_group_id      = acc.c1500_group_id
                  AND acc.c1520_function_id    = 'CRMADMIN-CRFAPPR'
                  AND gm.c101_mapped_party_id IS NOT NULL
                  AND gm.c1501_void_fl        IS NULL;
                  --
                           FOR APPROVAL IN v_approvals
                           LOOP
                                  BEGIN
                                         -- 105208: Approved, 105209: Denied
                                         IF (APPROVAL.STATUSID = '105208')
                                         THEN
                                                v_appr_cnt := v_appr_cnt + 1;
                                         END IF;
                                         
                                         IF (APPROVAL.STATUSID = '105209')
                                         THEN
                                               v_denied_cnt := v_denied_cnt + 1;
                                         END IF;
                                  END;
                           END LOOP;
                           
                           IF (v_appr_cnt = v_tot_appr_cnt)
                           THEN
                                  v_status := '105208';
                           END IF;
                           IF (v_denied_cnt = v_tot_appr_cnt)
                           THEN
                                  v_status := '105209';
                           END IF;
                           
                           UPDATE t6605_crf
                           SET c901_status = v_status
                           WHERE c6605_crf_id = p_crfid;
                     END;

       END gm_crm_set_crf_status;

END gm_pkg_crm_crf;
/
