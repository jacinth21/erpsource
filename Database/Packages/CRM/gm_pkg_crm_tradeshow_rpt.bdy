CREATE OR REPLACE 
PACKAGE BODY gm_pkg_crm_tradeshow_rpt
IS
/****************************************************************
* Description : Fetch the Tradeshow Details to display in CRM Dashboard widget
*****************************************************************/
PROCEDURE  gm_fch_tradeshow_dash (
	p_input			    IN      VARCHAR2,
  	p_out_clob 			OUT  	CLOB
 ) 
AS
	v_json 	     		 json_object_t;
 	v_divisionid	     VARCHAR2(20);
BEGIN
	v_json             := json_object_t.parse(p_input);
    v_divisionid       := v_json.get_String('divisionid');	
    SELECT  JSON_ARRAYAGG(
    		JSON_OBJECT( 
					  'id'          VALUE T6700.C6700_TRADESHOW_ID,
                      'name'        VALUE T6700.C6700_TRADESHOW_NAME,
                      'fromdt'      VALUE TO_CHAR(T6700.C6700_START_DATE, 'MM/DD/YYYY'),
                      'todt'        VALUE TO_CHAR(T6700.C6700_END_DATE, 'MM/DD/YYYY'),
                      'city'        VALUE T6700.C6700_CITY,
                      'state'       VALUE T6700.C6700_STATE,
                      'country'     VALUE get_code_name(T6700.C901_COUNTRY),
                      'status'      VALUE T6700.C901_STATUS,
                      'wrkshplink'  VALUE T6700.C6700_WORKSHOP_LINK,
					  'demolink'    VALUE T6700.C6700_DEMO_LINK,
					  'notes'       VALUE T6700.C6700_NOTES,
					  'acronym'     VALUE T6700.c6700_TRADESHOW_ACRONYM,
					  'attendeecnt' VALUE (SELECT COUNT(1) FROM T6710_ATTENDEE T6710
					  WHERE T6710.C6700_TRADESHOW_ID = T6700.C6700_TRADESHOW_ID
					  AND C6710_VOID_FL IS NULL))
					  ORDER BY T6700.C6700_START_DATE  RETURNING CLOB)					  
			    INTO    p_out_clob
				FROM    T6700_TRADESHOW T6700 
				WHERE   C6700_VOID_FL IS NULL
				AND     C901_STATUS IN (SELECT C906_RULE_ID FROM T906_RULES WHERE C906_RULE_GRP_ID ='TSSTAS' AND C906_VOID_FL IS NULL)
				AND     (C901_MARKET = v_divisionid OR C901_MARKET = 111622) -- Both //PC-4591-Tradeshow Event Creation Improvement
				AND 	C6700_START_DATE >= TRUNC(CURRENT_DATE);
END gm_fch_tradeshow_dash;

/****************************************************************
* Description : Fetch the Tradeshow attendee report details
*****************************************************************/
PROCEDURE  gm_fch_tradeshow_attendee_list (
	p_input      		IN      VARCHAR2,
  	p_out_clob 			OUT     CLOB
 ) 
 AS
	 v_json 	     json_object_t;
 	 v_tradeid	     VARCHAR2(20);
 	 v_status	     VARCHAR2(20);
BEGIN	 
     v_json          := json_object_t.parse(p_input);
     v_tradeid       := v_json.get_String('tradeid');
     v_status        := v_json.get_String('status');
     SELECT   JSON_ARRAYAGG(
 				JSON_OBJECT( 
 						'firstnm' 	 VALUE T6710.C6710_FIRST_NM, 
					    'lastnm' 	 VALUE T6710.C6710_LAST_NM, 
 						'email' 	 VALUE T6710.C6710_EMAIL,
 						'phone' 	 VALUE T6710.C6710_PHONE, 
 						'company'    VALUE T6710.C6710_COMPANY,
 						'city' 	 	 VALUE T6710.C6710_CITY,
 						'state' 	 VALUE T6710.C6710_STATE,
 						'status' 	 VALUE T6700.C901_STATUS,
 						'sysnotes'   VALUE T6710.C6710_SYSTEM_NOTES, 
 						'notes'      VALUE T6710.C6710_NOTES) ORDER BY T6710.C6710_FIRST_NM, T6710.C6710_LAST_NM RETURNING CLOB)
				INTO     P_OUT_CLOB
				FROM 	 T6700_TRADESHOW T6700,T6710_ATTENDEE T6710 
				WHERE    T6700.C6700_TRADESHOW_ID = T6710.C6700_TRADESHOW_ID 
				AND      T6710.C901_STATUS = decode('109044',v_status,'109047',T6710.C901_STATUS)
				AND		 T6700.C6700_VOID_FL IS NULL 
				AND 	 T6710.C6710_VOID_FL IS NULL 
				AND 	 T6700.C6700_TRADESHOW_ID = v_tradeid;
END gm_fch_tradeshow_attendee_list;
/****************************************************************
* Description : Fetch the Tradeshow From email address
*****************************************************************/
PROCEDURE  gm_fch_rep_email_from_userid (
	p_usr_id     		IN      VARCHAR2,
  	p_frommail_id 		OUT     t107_contact.c107_contact_VALUE%TYPE
 ) 
 AS
 
 v_mail_id  t107_contact.c107_contact_VALUE%TYPE;
 
 BEGIN
	SELECT
	    nvl(TRIM(gm_pkg_cm_contact.get_contact_value(t101.c101_party_id, 90452)), nvl(t101.c101_email_id, get_rule_value('DEFAULTSHIPEMAIL'
	    , 'EMAIL')))
	INTO p_frommail_id
	FROM
	    t101_user        t101,
	    t703_sales_rep   t703
	WHERE
	    t101.C101_PARTY_ID = t703.c101_party_id (+)
	    AND t101.c101_user_id = p_usr_id
	    AND t703.c703_void_fl IS NULL;
	    
	    
		IF (INSTR (p_frommail_id, ',') <> 0)	-- If more than one mail id is fetched, get first mail id
			THEN
				v_mail_id := SUBSTR (p_frommail_id, 1, INSTR (p_frommail_id, ',') - 1);
				p_frommail_id := v_mail_id;
	 	END IF;
	 	
	 	IF (INSTR (p_frommail_id, ';') <> 0)	-- If more than one mail id is fetched, get first mail id
			THEN
				v_mail_id := SUBSTR (p_frommail_id, 1, INSTR (p_frommail_id, ';') - 1);
				p_frommail_id := v_mail_id;
	 	END IF;
	 	
 
END gm_fch_rep_email_from_userid;

END gm_pkg_crm_tradeshow_rpt;
/