CREATE OR REPLACE 
PACKAGE BODY gm_pkg_crm_quickref
IS
/****************************************************************
* Description : Fetch the Quick Reference Details to display in CRM Dashboard widget
*****************************************************************/
PROCEDURE  gm_fch_quick_ref_dash_json (
  p_out_clob OUT  CLOB
 ) 
 AS
BEGIN    
     SELECT JSON_ARRAYAGG(
     	JSON_OBJECT(
           'id' VALUE T6633.C6633_QUICK_REF_ID,
           'title' VALUE T6633.C6633_QUICK_REF_TITLE,
           'subtitle' VALUE T6633.C6633_QUICK_REF_SUBTITLE,
           'link' VALUE T6633.C6633_QUICK_REF_LINK) 
           ORDER BY T6633.C6633_SEQUENCE RETURNING CLOB) 
           INTO p_out_clob
           FROM T6633_QUICK_REFERENCE T6633
           WHERE T6633.C6633_EXPIRY_DATE >= TRUNC(CURRENT_DATE)           
           AND T6633.C6633_VOID_FL is null;

END gm_fch_quick_ref_dash_json;	
END gm_pkg_crm_quickref;
/