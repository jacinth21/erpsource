CREATE OR REPLACE
PACKAGE BODY gm_pkg_crm_surgeon
IS
  /****************************************************************
  * Description : Procedure to save Surgeon Basic data into Party Table
  * Author      :
  *****************************************************************/
PROCEDURE gm_sav_surgeoninfo(
    p_partyid       IN OUT t101_party.c101_party_id%TYPE ,
    p_firstname     IN t101_party.c101_first_nm%TYPE ,
    p_lastname      IN t101_party.c101_last_nm%TYPE ,
    p_middleinitial IN t101_party.c101_middle_initial%TYPE ,
    p_inactivefl    IN t101_party.c101_active_fl%TYPE ,
    p_companyid     IN t101_party.c1900_company_id%TYPE,
    p_userid        IN t101_party.c101_created_by%TYPE )
AS
BEGIN
  UPDATE t101_party
  SET c101_first_nm        = p_firstname,
    c101_last_nm           = p_lastname,
    c101_middle_initial    = p_middleinitial,
    c101_active_fl         = p_inactivefl,
    c1900_company_id       = p_companyid,
    c101_last_updated_by   = p_userid,
    c101_last_updated_date = CURRENT_DATE
  WHERE c101_party_id      = p_partyid;
  IF (SQL%ROWCOUNT         = 0) THEN
    SELECT s101_party.NEXTVAL INTO p_partyid FROM DUAL;
    INSERT
    INTO t101_party
      (
        c101_party_id,
        c101_last_nm,
        c101_first_nm,
        c101_middle_initial,
        c901_party_type,
        c101_active_fl,
        c1900_company_id,
        c101_created_by,
        c101_created_date
      )
      VALUES
      (
        p_partyid,
        p_lastname,
        p_firstname,
        p_middleinitial,
        '7000',
        p_inactivefl,
        p_companyid,
        p_userid,
        CURRENT_DATE
      );
  END IF;
  --inactive address for the party then only iPad addess count will match
  IF NVL(p_inactivefl,'Y') <> 'Y' THEN
    UPDATE t106_address
    SET c106_inactive_fl     = 'Y' ,
      c106_last_updated_by   = p_userid ,
      c106_last_updated_date = CURRENT_DATE
    WHERE c101_party_id      = p_partyid;
  END IF;
END gm_sav_surgeoninfo;
/****************************************************************
* Description : Procedure to save data into Party Surgeon Table
* Author      :
*****************************************************************/
PROCEDURE gm_sav_partysurgeon(
    p_partyid IN t6600_party_surgeon.c101_party_surgeon_id%TYPE,
    p_npid    IN t6600_party_surgeon.c6600_surgeon_npi_id%TYPE,
    p_asst    IN t6600_party_surgeon.c6600_surgeon_assistant_nm%TYPE,
    p_mngr    IN t6600_party_surgeon.c6600_surgeon_manager_nm%TYPE,
    p_voidfl  IN t6600_party_surgeon.c6600_void_fl%TYPE,
    p_userid  IN t6600_party_surgeon.c6600_created_by%TYPE,
    p_salute  IN t6600_party_surgeon.c6600_party_salute%TYPE )
AS
BEGIN
  UPDATE t6600_party_surgeon
  SET C6600_SURGEON_NPI_ID     = p_npid ,
    C6600_SURGEON_ASSISTANT_NM = p_asst ,
    C6600_SURGEON_MANAGER_NM   = p_mngr ,
    C6600_PARTY_SALUTE         = p_salute
  WHERE C101_PARTY_SURGEON_ID  = p_partyid
  AND C6600_VOID_FL           IS NULL;
  IF (SQL%ROWCOUNT             = 0) THEN
    INSERT
    INTO t6600_party_surgeon
      (
        C6600_SURGEON_NPI_ID,
        C101_PARTY_SURGEON_ID,
        C6600_SURGEON_ASSISTANT_NM,
        C6600_SURGEON_MANAGER_NM,
        C6600_VOID_FL,
        C6600_CREATED_BY,
        C6600_CREATED_DATE,
        C6600_LAST_UPDATED_BY,
        C6600_LAST_UPDATED_DATE,
        C6600_PARTY_SALUTE
      )
      VALUES
      (
        p_npid,
        p_partyid,
        p_asst,
        p_mngr,
        p_voidfl,
        p_userid,
        SYSDATE,
        p_userid,
        SYSDATE,
        p_salute
      );
  END IF;
END gm_sav_partysurgeon;
/****************************************************************
* Description : Save Surgeon Additional Info Like Specialty
* Author      : praja
*****************************************************************/
PROCEDURE gm_sav_crm_surgeon_atrr
  (
    p_attrid  IN OUT t1012_party_other_info.c1012_party_other_info_id%TYPE,
    p_partyid IN t1012_party_other_info.c101_party_id%TYPE,
    p_type    IN t1012_party_other_info.c901_type%TYPE,
    p_value   IN t1012_party_other_info.c1012_detail%TYPE,
    p_userid  IN t1012_party_other_info.c1012_created_by%TYPE,
    p_voidfl  IN t1012_party_other_info.c1012_void_fl%TYPE
  )
AS
  v_out t1012_party_other_info.c901_type%TYPE;
BEGIN
  IF (p_type = 11509) -- 11509 Sales Reps, Sales Rep with Surgeon mapping should go t108_party_to_party_link table
    THEN
    gm_pkg_cm_globusinfo.gm_sav_globusinfo(p_attrid, p_partyid, p_value, 'Y', p_userid, v_out);
  ELSE
    UPDATE t1012_party_other_info
    SET c101_party_id               = p_partyid ,
      c901_type                     = p_type ,
      c1012_detail                  = p_value ,
      c1012_last_updated_by         = p_userid ,
      c1012_last_updated_date       = SYSDATE ,
      c1012_void_fl                 = p_voidfl
    WHERE c1012_party_other_info_id = p_attrid;
    IF (SQL%ROWCOUNT                = 0) THEN
      SELECT s1012_party_other_info.NEXTVAL INTO p_attrid FROM DUAL;
      INSERT
      INTO t1012_party_other_info
        (
          c1012_party_other_info_id,
          c101_party_id,
          c901_type,
          c1012_detail,
          c1012_void_fl,
          c1012_created_by,
          c1012_created_date
        )
        VALUES
        (
          p_attrid,
          p_partyid,
          p_type,
          p_value,
          p_voidfl,
          p_userid,
          SYSDATE
        );
    END IF;
  END IF;
END gm_sav_crm_surgeon_atrr;
/****************************************************************
* Description : Save Surgeon Hospital Affiliation Info
* Author      : praja
*****************************************************************/
 /* PROCEDURE gm_sav_crm_surgeon_affiliation
  (
    p_partyid     IN t1014_party_affiliation.c101_party_id%TYPE,
    p_accnm       IN t1014_party_affiliation.c1014_organization_name%TYPE,
    p_priority    IN t1014_party_affiliation.c901_affiliation_priority%TYPE,
    p_casepercent IN t1014_party_affiliation.c1014_affiliation_percent%TYPE,
    p_userid      IN t1014_party_affiliation.c1014_created_by%TYPE,
    p_afflid      IN OUT t1014_party_affiliation.c1014_party_affiliation_id%TYPE,
    p_accstate    IN t1014_party_affiliation.c901_state%TYPE,
    p_acccity     IN t1014_party_affiliation.c1014_city_nm%TYPE,
    p_accid       IN t1014_party_affiliation.c704_account_id%TYPE,
    p_voidfl      IN t1014_party_affiliation.c1014_void_fl%TYPE,
    p_cmnt        IN t1014_party_affiliation.c1014_comment%TYPE
  )
AS
  v_accid VARCHAR2
  (
    20
  )
  ;
  v_exist NUMBER;
BEGIN
  v_accid := p_accid;
  v_exist := 0;
  UPDATE T1014_PARTY_AFFILIATION
  SET C901_AFFILIATION_PRIORITY    = p_priority ,
    C1014_AFFILIATION_PERCENT      = p_casepercent ,
    C1014_LAST_UPDATED_BY          = p_userid ,
    C1014_LAST_UPDATED_DATE        = SYSDATE ,
    C1014_ORGANIZATION_NAME        = p_accnm ,
    C101_PARTY_ID                  = p_partyid ,
    C901_STATE                     = p_accstate ,
    C1014_CITY_NM                  = p_acccity ,
    C704_ACCOUNT_ID                = p_accid ,
    C1014_VOID_FL                  = p_voidfl ,
    c1014_comment                  = p_cmnt
  WHERE C1014_PARTY_AFFILIATION_ID = p_afflid;
  IF (SQL%ROWCOUNT                 = 0) THEN
    SELECT S1014_PARTY_AFFILIATION.NEXTVAL INTO p_afflid FROM DUAL;
    IF(v_accid IS NULL) THEN
      -- Setting accountID for new accounts as new + T1014 primary key
      -- This way we separate affiliations without accountID and don't fetch them in surgeon profile
      v_accid := 'new' || p_afflid;
    ELSE
      SELECT COUNT(*)
      INTO v_exist
      FROM T1014_PARTY_AFFILIATION
      WHERE C101_PARTY_ID = p_partyid
      AND C704_ACCOUNT_ID = v_accid;
    END IF;
    IF(v_exist = 0) THEN
      INSERT
      INTO T1014_PARTY_AFFILIATION
        (
          C101_PARTY_ID,
          C901_AFFILIATION_PRIORITY,
          C1014_AFFILIATION_PERCENT,
          C1014_CREATED_BY,
          C1014_CREATED_DATE,
          C1014_PARTY_AFFILIATION_ID,
          C1014_ORGANIZATION_NAME,
          C901_STATE,
          C1014_CITY_NM,
          C704_ACCOUNT_ID,
          C1014_VOID_FL,
          C1014_COMMENT
        )
        VALUES
        (
          p_partyid,
          p_priority,
          p_casepercent,
          p_userid,
          SYSDATE,
          p_afflid,
          p_accnm,
          p_accstate,
          p_acccity,
          v_accid,
          p_voidfl,
         -- p_cmnt
        );
    END IF;
  END IF;
END gm_sav_crm_surgeon_affiliation; */
/****************************************************************
* Description : Save Surgeon's Surgical Activity Info
* Author      : praja
*****************************************************************/
PROCEDURE gm_sav_crm_surgeon_surgical
  (
    p_partyid     IN t6610_surgical_activity.c101_party_id%TYPE,
    p_procedure   IN t6610_surgical_activity.c901_procedure%TYPE,
    p_procedurenm IN t6610_surgical_activity.c901_procedurenm%TYPE,
    p_setid       IN t6610_surgical_activity.c207_set_id%TYPE,
    p_systemnm    IN t6610_surgical_activity.c6610_non_globus_system%TYPE,
    p_companynm   IN t6610_surgical_activity.c6610_company_nm%TYPE,
    p_cases       IN t6610_surgical_activity.c6610_cases%TYPE,
    p_userid      IN t6610_surgical_activity.c6610_last_updated_by%TYPE,
    p_sactid      IN OUT t6610_surgical_activity.c6610_surgical_activity_id%TYPE,
    p_void_fl     IN t6610_surgical_activity.c6610_void_fl%TYPE,
    p_cmnt        IN t6610_surgical_activity.c6610_comment%TYPE
  )
AS
BEGIN
  UPDATE t6610_surgical_activity
  SET c207_set_id                  = p_setid ,
    c6610_non_globus_system        = p_systemnm ,
    c6610_company_nm               = p_companynm ,
    c6610_cases                    = p_cases ,
    c6610_last_updated_by          = p_userid ,
    c6610_last_updated_date        = SYSDATE ,
    c101_party_id                  = p_partyid ,
    c901_procedure                 = p_procedure ,
    c901_procedurenm               = p_procedurenm ,
    c6610_void_fl                  = p_void_fl ,
    c6610_comment                  = p_cmnt
  WHERE c6610_surgical_activity_id = p_sactid;
  IF (SQL%ROWCOUNT                 = 0) THEN
    SELECT S6610_SURGICAL_ACTIVITY_ID.NEXTVAL INTO p_sactid FROM DUAL;
    INSERT
    INTO t6610_surgical_activity
      (
        c101_party_id,
        c901_procedure,
        c901_procedurenm,
        c207_set_id,
        c6610_non_globus_system,
        c6610_company_nm,
        c6610_cases,
        c6610_last_updated_by,
        c6610_last_updated_date,
        c6610_surgical_activity_id,
        c6610_void_fl,
        c6610_comment
      )
      VALUES
      (
        p_partyid,
        p_procedure,
        p_procedurenm,
        p_setid,
        p_systemnm,
        p_companynm,
        p_cases,
        p_userid,
        SYSDATE,
        p_sactid,
        p_void_fl,
        p_cmnt
      );
  END IF;
END gm_sav_crm_surgeon_surgical;
/****************************************************************
* Description : Fetch Surgeon's Info
* Author      : cskumar
*****************************************************************/
PROCEDURE gm_fch_surgeon_info
  (
    p_partyid IN t101_party.c101_party_id%TYPE,
    p_surgeoninfo_cur OUT TYPES.CURSOR_TYPE
  )
AS
BEGIN
  OPEN p_surgeoninfo_cur FOR SELECT T101.C101_PARTY_ID PARTYID,
  t6600.c6600_surgeon_npi_id NPID,
  T101.C101_FIRST_NM FIRSTNM,
  T101.C101_LAST_NM LASTNM,
  T101.C101_MIDDLE_INITIAL MIDINITIAL,
  T101.C1900_COMPANY_ID COMPANYID,
  GET_COMPANY_NAME
  (
    T101.C1900_COMPANY_ID
  )
  COMPANYNAME,
  GET_CODE_NAME
  (
    T6600.C6600_PARTY_SALUTE
  )
  SALUTENM,
  T6600.C6600_PARTY_SALUTE SALUTE,
  PIC_INFO.PICURL,
  PIC_INFO.PICFILEID,
  REPINFO.REPID,
  REPINFO.REPNM,
  REPINFO.ADNM,
  REPINFO.ADID,
  REPINFO.VPNM,
  get_upcoming_activity_count
  (
    p_partyid, 105265
  )
  MERCCNT,
  get_upcoming_activity_count
  (
    p_partyid, 105266
  )
  PVISITCNT,
  get_upcoming_activity_count
  (
    p_partyid, 105267
  )
  SCALLCNT FROM T101_PARTY T101,
  T6600_PARTY_SURGEON T6600,
  (SELECT C903_REF_ID REFID,
      C901_REF_GRP
      ||'/'
      || C903_REF_ID
      ||'/'
      || C903_FILE_NAME PICURL,
      C903_UPLOAD_FILE_LIST PICFILEID
    FROM T903_UPLOAD_FILE_LIST
    WHERE C903_DELETE_FL IS NULL
    AND C901_REF_TYPE IS NULL
    AND C901_REF_GRP      = '103112'
  )
  PIC_INFO,
  (SELECT t108.c101_from_party_id partyid,
      v700.ad_name adnm,
      t703.c703_sales_rep_name repnm,
      v700.vp_name vpnm,
      v700.vp_id vpid,
      v700.ad_id adid,
      t703.c703_sales_rep_id repid
    FROM t703_sales_rep t703 ,
      (SELECT DISTINCT v700.rep_id,
        v700.ad_name,
        v700.vp_name,
        v700.vp_id,
        v700.ad_id
      FROM v700_territory_mapping_detail v700
      ) v700 ,
      t108_party_to_party_link t108
    WHERE t108.c101_from_party_id = p_partyid
    AND t108.c101_to_party_id     = t703.c101_party_id
    AND t703.c703_sales_rep_id    = v700.rep_id
    AND t108.c108_primary_fl      = 'Y'
    AND t108.c108_void_fl        IS NULL
  )
  REPINFO WHERE T101.C901_PARTY_TYPE = '7000' AND T101.C101_PARTY_ID = partyid(+) AND T101.C101_PARTY_ID = T6600.C101_PARTY_SURGEON_ID(+) AND T6600.C6600_VOID_FL IS NULL AND T101.C101_VOID_FL IS NULL AND TO_CHAR
  (
    t101.c101_party_id
  )
  = PIC_INFO.REFID(+) AND (t101.c101_active_fl IS NULL OR t101.c101_active_fl = 'Y') AND t101.c101_party_id = p_partyid;
END gm_fch_surgeon_info;
/****************************************************************
* Description : Fetch Surgeon's Address Info
* Author      : cskumar
*****************************************************************/
PROCEDURE gm_fch_surgeonaddr_info
  (
    p_partyid IN t101_party.c101_party_id%TYPE,
    p_sgnaddrinfo_cur OUT TYPES.CURSOR_TYPE
  )
AS
BEGIN
  OPEN p_sgnaddrinfo_cur FOR SELECT t106.c106_bill_name BILLNAME,
  T106.C106_ADDRESS_ID ADDID,
  T106.C106_ADD1 ADD1,
  T106.C901_ADDRESS_TYPE ADDRTYP,
  GET_CODE_NAME
  (
    T106.C901_ADDRESS_TYPE
  )
  ADDRTYPNM,
  T106.C106_ADD2 ADD2,
  T106.C106_CITY CITY,
  T106.C106_PRIMARY_FL PRIMARYFL,
  NVL
  (
    t901.c902_code_nm_alt,t901.c901_code_nm
  )
  STATENM,
  T106.C901_STATE STATEID,
  GET_CODE_NAME
  (
    T106.C901_COUNTRY
  )
  COUNTRYNM,
  T106.C901_COUNTRY COUNTRYID,
  T106.C106_ZIP_CODE ZIP,
  T106.C901_ADDRESS_TYPE ADDRTYP,
  t106.c106_void_fl voidfl FROM t106_address t106,
  t901_code_lookup t901 WHERE t106.c106_void_fl IS NULL AND T106.C901_STATE = t901.c901_code_id(+) AND t106.c101_party_id = p_partyid ORDER BY PRIMARYFL,
  ADDID;
END gm_fch_surgeonaddr_info;
/****************************************************************
* Description : Fetch Surgeon's Contact Info
* Author      : cskumar
*****************************************************************/
PROCEDURE gm_fch_surgeoncnt_info
  (
    p_partyid IN t101_party.c101_party_id%TYPE,
    p_sgmcntinfo_cur OUT TYPES.CURSOR_TYPE
  )
AS
BEGIN
  OPEN p_sgmcntinfo_cur FOR SELECT C107_CONTACT_ID CONID,
  C901_MODE CONMODE,
  C107_CONTACT_TYPE CONTYPE,
  C107_CONTACT_VALUE CONVALUE,
  C107_PRIMARY_FL PRIMARYFL FROM T107_CONTACT WHERE C107_VOID_FL IS NULL AND C107_INACTIVE_FL IS NULL AND C101_PARTY_ID = p_partyid ORDER BY C107_PRIMARY_FL,
  CONID;
END gm_fch_surgeoncnt_info;
/****************************************************************
* Description : Fetch Surgeon's Attribute Info
* Author      : cskumar
*****************************************************************/
PROCEDURE gm_fch_surgeonattr_info
  (
    p_partyid IN t101_party.c101_party_id%TYPE,
    p_sgnattrinfo_cur OUT TYPES.CURSOR_TYPE
  )
AS
BEGIN
  OPEN p_sgnattrinfo_cur FOR SELECT C1012_PARTY_OTHER_INFO_ID ATTRID,
  C901_TYPE ATTRTYPE,
  C1012_DETAIL ATTRVALUE,
  NVL2
  (
    LENGTH(TRIM(TRANSLATE(C1012_DETAIL, '0123456789', ' '))),C1012_DETAIL,
    (SELECT C901_CODE_NM
    FROM T901_CODE_LOOKUP
    WHERE TO_CHAR(C901_CODE_ID) = C1012_DETAIL
    )
  )
  ATTRNM,
  C1012_VOID_FL VOIDFL FROM T1012_PARTY_OTHER_INFO WHERE C1012_VOID_FL IS NULL AND C101_PARTY_ID = p_partyid;
END gm_fch_surgeonattr_info;

/*****************************************************************************************
* Description : Fetch Surgeon's Hospital Affiliation Info, Parent information of Related Hospitals
* Author      : Ranjiths
******************************************************************************************/
PROCEDURE gm_fch_surgeonaffl_info
  (
    p_partyid IN t101_party.c101_party_id%TYPE,
    p_sgnafflinfo_cur OUT TYPES.CURSOR_TYPE
  )
AS
BEGIN
  OPEN p_sgnafflinfo_cur FOR SELECT DISTINCT(T101.C101_PARTY_NM) PARENTACCTNM,
  T704.C101_PARTY_ID ACCID,
  T101.C101_PARTY_NM ACCNM,
  T1014.C1014_CITY_NM ACCCITY,
  GET_CODE_NAME ( T1014.C901_STATE ) ACCSTATE
    FROM T1014_PARTY_AFFILIATION T1014, T704_ACCOUNT T704, T101_PARTY T101
WHERE T704.C704_VOID_FL IS NULL
AND T1014.C1014_VOID_FL IS NULL
AND T101.C101_VOID_FL IS NULL
AND T1014.C704_ACCOUNT_ID IS NOT NULL
AND T1014.C704_ACCOUNT_ID = T704.C704_ACCOUNT_ID
AND T704.C101_PARTY_ID  = T101.C101_PARTY_ID
AND T1014.C101_PARTY_ID    = p_partyid;
END gm_fch_surgeonaffl_info;

PROCEDURE gm_fch_surgeonsurg_info
  (
    p_partyid IN t101_party.c101_party_id%TYPE,
    p_sgnsurginfo_cur OUT TYPES.CURSOR_TYPE
  )
AS
BEGIN
  OPEN p_sgnsurginfo_cur FOR SELECT C901_PROCEDURE PROCEDUREID,
  C901_PROCEDURENM PROCEDURENM,
  C6610_CASES CASES,
  C207_SET_ID SYSID,
  C6610_NON_GLOBUS_SYSTEM SYSNM,
  C6610_COMPANY_NM COMPANY,
  C6610_SURGICAL_ACTIVITY_ID SACTID,
  C6610_COMMENT COMMENTS,
  get_code_name
  (
    C6610_COMMENT
  )
  COMMENTNM FROM T6610_SURGICAL_ACTIVITY WHERE C6610_VOID_FL IS NULL AND C101_PARTY_ID = p_partyid;
END gm_fch_surgeonsurg_info;
PROCEDURE gm_fch_surgeonactv_info
  (
    p_partyid IN t101_party.c101_party_id%TYPE,
    p_sgnactvinfo_cur OUT TYPES.CURSOR_TYPE
  )
AS
BEGIN
  OPEN p_sgnactvinfo_cur FOR SELECT T6630.C6630_ACTIVITY_NM ACTIVITYTITLE,
  T6630.C6630_ACTIVITY_ID ACTIVITYID,
  T6630.C6630_FROM_DT ACTIVITYDATE,
  GET_CODE_NAME
  (
    T6630.C901_ACTIVITY_CATEGORY
  )
  ACTIVITYTYPE,
  PROD.PRODUCTS ACTIVITYPRODUCTS FROM T6630_ACTIVITY T6630,
  ( SELECT DISTINCT C6630_ACTIVITY_ID,
      C101_PARTY_ID
    FROM T6631_ACTIVITY_PARTY_ASSOC
    WHERE C6631_VOID_FL IS NULL
  )
  ACTPARTY,
  (SELECT RTRIM (XMLAGG (XMLELEMENT (N, T6632.C6632_ATTRIBUTE_NM, ', ')) .EXTRACT ('//text()'), ', ') PRODUCTS,
      T6632.C6630_ACTIVITY_ID ACTID
    FROM T6632_ACTIVITY_ATTRIBUTE T6632
    WHERE T6632.C901_ATTRIBUTE_TYPE = '105582'
    GROUP BY C6630_ACTIVITY_ID
  )
  PROD WHERE ACTPARTY.C6630_ACTIVITY_ID = T6630.C6630_ACTIVITY_ID AND PROD.ACTID(+) = T6630.C6630_ACTIVITY_ID AND ACTPARTY.C101_PARTY_ID = p_partyid AND C6630_VOID_FL IS NULL AND T6630.C901_ACTIVITY_CATEGORY IN
  (
    '105267','105265','105266','105268'
  )
  AND T6630.C6630_FROM_DT >= TO_CHAR
  (
    SYSDATE
  )
  ;
END gm_fch_surgeonactv_info;
/****************************************************************
* Description : Fetch Surgeon Activity Info With Pagination
* Author      : Mahendran D
*****************************************************************/
PROCEDURE gm_fch_partyactv_info
  (
    p_partyid    IN t101_party.c101_party_id%TYPE,
    p_pagination IN t101_party.c101_party_id%TYPE,
    p_partyactv_cur OUT TYPES.CURSOR_TYPE
  )
AS
BEGIN
  OPEN p_partyactv_cur FOR SELECT * FROM
  (SELECT ROWNUM RNUM,
      TOTALSIZE,
      ACTIVITYID,
      TXNID,
      ACTIVITYDATE,
      ACTIVITYTODATE,
      ACTIVITYTYPEID,
      ACTIVITYTYPE,
      ACTIVITYTITLE,
      ACTDETAIL,
      TOPARTYID,
      FROMPARTYID    

    FROM
      (SELECT COUNT (1) OVER () TOTALSIZE,
        C6650_REF_ID ACTIVITYID,
        c6650_TXN_ID TXNID,
        C6650_ACTIVITY_FROM_DT ACTIVITYDATE,
        C6650_ACTIVITY_TO_DT ACTIVITYTODATE,
        C901_REF_TYPE ACTIVITYTYPEID,
        GET_CODE_NAME(C901_REF_TYPE) ACTIVITYTYPE,
        C6650_ACTIVITY_TITLE ACTIVITYTITLE,
        C6650_ACTIVITY_DETAIL ACTDETAIL,
        GET_PARTY_NAME(C101_TO_PARTY_ID) TOPARTYID,
        GET_USER_NAME(C6650_LAST_UPDATED_BY) FROMPARTYID
      FROM T6650_PARTY_ACTIVITY
      WHERE C6650_VOID_FL   IS NULL
      AND C101_FROM_PARTY_ID = p_partyid
      ORDER BY C6650_ACTIVITY_FROM_DT DESC
      ) T
  )
  WHERE RNUM >=
  (
    p_pagination*50
  )
  +1 AND RNUM <=
  (
    p_pagination*50
  )
  + 50;
END gm_fch_partyactv_info;
/****************************************************************
* Description : Fetch Activity Count by Activity Type and Party ID
* Author      : Mahendran D
*****************************************************************/
FUNCTION get_upcoming_activity_count
  (
    p_partyid t101_party.c101_party_id%type,
    p_activity_type t101_party.c101_party_id%type
  )
  RETURN NUMBER
IS
  v_activity_count t101_party.c101_party_id%type;
BEGIN
  --
  SELECT COUNT(1)
  INTO v_activity_count
  FROM T6630_ACTIVITY T6630,
    T6631_ACTIVITY_PARTY_ASSOC T6631
  WHERE T6630.C6630_VOID_FL       IS NULL
  AND T6630.C6630_ACTIVITY_ID      = T6631.C6630_ACTIVITY_ID
  AND T6631.C6631_VOID_FL         IS NULL
  AND T6631.C101_PARTY_ID          = p_partyid
  AND T6630.C6630_FROM_DT         >= TRUNC(SYSDATE)
  AND T6630.C901_ACTIVITY_CATEGORY = p_activity_type;
  --
  RETURN v_activity_count;
END GET_UPCOMING_ACTIVITY_COUNT;
--
/****************************************************************
* Description : Fetch surgical revenue info
* Author      : Bala
*****************************************************************/
PROCEDURE gm_fch_surgical_by_rev(
    p_partyid  IN T101_PARTY.C101_PARTY_ID%TYPE,
    p_fromdate IN T101_PARTY.C101_PARTY_ID%TYPE,
    p_todate   IN T101_PARTY.C101_PARTY_ID%TYPE,
    p_surgicalrev_cur OUT TYPES.CURSOR_TYPE )
AS
BEGIN
  OPEN p_surgicalrev_cur FOR SELECT YEARMON,
  SUM(VALUE) VALUE,
  GRPID,
  GRPNM,
  GRPID2,
  GRPNM2,
  GRPID3 FROM
  (SELECT TO_CHAR(T501.C501_ORDER_DATE ,'YYYYMM')YEARMON,
    (T502.C502_ITEM_PRICE * T502.C502_ITEM_QTY) VALUE,
    V_SYS.SEGMENT_ID GRPID,
    V_SYS.SEGMENT_NAME GRPNM,
    V_SYS.SYSTEM_ID GRPID2,
    V_SYS.SYSTEM_NM GRPNM2,
    GET_ACCOUNT_PARTYID(T6640.C704_ACCOUNT_ID) GRPID3
  FROM T6640_NPI_TRANSACTION T6640,
    T501_ORDER T501,
    T502_ITEM_ORDER T502,
    T208_SET_DETAILS T208,
    v_segment_system_map V_SYS
  WHERE T6640.C101_PARTY_SURGEON_ID     = p_partyid
  AND T6640.C6640_VALID_FL              = 'Y'
  AND T6640.C6640_REF_ID                = T501.C501_ORDER_ID
  AND T501.C501_ORDER_ID                = T502.C501_ORDER_ID
  ---AND T501.C501_STATUS_FL            = 3
  AND V_SYS.SYSTEM_ID                   = T208.C207_SET_ID
  AND T502.C205_PART_NUMBER_ID          = T208.C205_PART_NUMBER_ID
  AND T6640.C6640_VOID_FL              IS NULL
  AND T501.C501_PARENT_ORDER_ID        IS NULL
  AND T501.C501_VOID_FL                IS NULL
  AND T502.C502_VOID_FL                IS NULL
  AND T208.C208_VOID_FL				   IS NULL
  AND T502.C502_ITEM_PRICE             > 0
  AND T502.C502_ITEM_QTY               > 0
  AND T501.C501_DELETE_FL              IS NULL
  AND NVL (C901_ORDER_TYPE, -9999) NOT IN
    (SELECT C906_RULE_VALUE FROM V901_ORDER_TYPE_GRP
    )
  AND T501.C501_ORDER_DATE >= TO_DATE(p_fromdate,'YYYYMM')
  AND T501.C501_ORDER_DATE <= LAST_DAY(TO_DATE(p_todate,'YYYYMM'))
  ) GROUP BY YEARMON,
  GRPID,
  GRPNM,
  GRPID2,
  GRPNM2,
  GRPID3;
END gm_fch_surgical_by_rev;
/****************************************************************
* Description : Fetch surgical procedure info
* Author      : Bala
*****************************************************************/
PROCEDURE gm_fch_surgical_by_prc(
    p_partyid  IN T101_PARTY.C101_PARTY_ID%TYPE,
    p_fromdate IN T101_PARTY.C101_PARTY_ID%TYPE,
    p_todate   IN T101_PARTY.C101_PARTY_ID%TYPE,
    p_surgicalprc_cur OUT TYPES.CURSOR_TYPE )
AS
BEGIN
  OPEN p_surgicalprc_cur FOR SELECT ORDDATE YEARMON,
  COUNT(1) VALUE,
  SEGID GRPID,
  SEGNM GRPNM,
  SYSID GRPID2,
  SYSNM GRPNM2,
  ACCTID GRPID3 FROM
  (SELECT T6640.C6600_SURGEON_NPI_ID,
    TO_CHAR(T501.C501_ORDER_DATE,'YYYYMM') ORDDATE,
    T501.C501_ORDER_ID ORDID,
    V_SYS.SEGMENT_ID SEGID,
    V_SYS.SEGMENT_NAME SEGNM,
    V_SYS.SYSTEM_ID SYSID,
    V_SYS.SYSTEM_NM SYSNM,
    GET_ACCOUNT_PARTYID(T6640.C704_ACCOUNT_ID) ACCTID
  FROM T6640_NPI_TRANSACTION T6640,
    T501_ORDER T501,
    T501B_ORDER_BY_SYSTEM T501B,
    v_segment_system_map V_SYS
  WHERE T6640.C101_PARTY_SURGEON_ID     = p_partyid
  AND T6640.C6640_VALID_FL              = 'Y' 
  AND T6640.C6640_REF_ID                = T501.C501_ORDER_ID
  AND T501.C501_ORDER_ID                = T501B.C501_ORDER_ID
  AND T501B.C207_SYSTEM_ID              = V_SYS.SYSTEM_ID
  AND T501.C501_PARENT_ORDER_ID        IS NULL
  AND T6640.C6640_VOID_FL              IS NULL
  AND T501.C501_VOID_FL                IS NULL
  AND T501.C501_DELETE_FL              IS NULL
  AND NVL (C901_ORDER_TYPE, -9999) NOT IN
    (SELECT C906_RULE_VALUE FROM V901_ORDER_TYPE_GRP
    )
  AND T501B.C501B_VOID_FL                    IS NULL
  AND T501.C501_ORDER_DATE >= TO_DATE(p_fromdate,'YYYYMM')
  AND T501.C501_ORDER_DATE <= LAST_DAY(TO_DATE(p_todate,'YYYYMM'))
  ) GROUP BY ORDDATE,
  SYSID,
  SYSNM,
  SEGID,
  SEGNM,
  ACCTID;
END gm_fch_surgical_by_prc;


/****************************************************************
* Description : Fetch Surgeon DO counts for surgical activity calendar view
* Author      : Tmuthusamy
*****************************************************************/
PROCEDURE gm_fch_calendar_info(
    p_partyid  IN T101_PARTY.C101_PARTY_ID%TYPE,
    p_fromdate IN T101_PARTY.C101_PARTY_ID%TYPE,
    p_todate   IN T101_PARTY.C101_PARTY_ID%TYPE,
    p_surgicalclr_cur OUT TYPES.CURSOR_TYPE )
AS
BEGIN
	OPEN p_surgicalclr_cur FOR 
	SELECT NVL(T501.C501_Surgery_Date,T501.C501_Order_Date) SURGDT,
	V_SYS.SEGMENT_ID SEGID,
	V_SYS.SEGMENT_NAME SEGNM,
	V_Sys.System_Id Sysid,
	V_SYS.SYSTEM_NM SYSNM,
	COUNT(1) cnt
	FROM  T6640_NPI_TRANSACTION T6640,
	T501_ORDER T501,
	T501B_ORDER_BY_SYSTEM T501B,
	V_SEGMENT_SYSTEM_MAP V_SYS
	WHERE T6640.C101_PARTY_SURGEON_ID = p_partyid
	AND NVL(T501.C501_SURGERY_DATE,T501.C501_ORDER_DATE) >= TO_DATE(
	p_fromdate,'YYYYMM')
	AND NVL(T501.C501_SURGERY_DATE,T501.C501_ORDER_DATE) <= LAST_DAY(TO_DATE(
	p_todate,'YYYYMM'))
	AND T6640.C6640_VALID_FL = 'Y'
	AND T6640.C6640_REF_ID = T501.C501_ORDER_ID
	AND T501.C501_ORDER_ID = T501B.C501_ORDER_ID
	AND T501B.C207_SYSTEM_ID = V_SYS.SYSTEM_ID
	AND NVL (C901_ORDER_TYPE, -9999) NOT IN
	(SELECT C906_RULE_VALUE FROM V901_ORDER_TYPE_GRP
	)
	AND T501.C501_PARENT_ORDER_ID IS NULL
	AND T6640.C6640_VOID_FL IS NULL
	AND T501.C501_VOID_FL IS NULL
	AND T501.C501_DELETE_FL IS NULL
	AND T501B.C501B_VOID_FL IS NULL
	Group By
	NVL(T501.C501_Surgery_Date,T501.C501_Order_Date) ,
	V_SYS.SEGMENT_ID ,
	V_SYS.SEGMENT_NAME ,
	V_Sys.System_Id ,
	V_SYS.SYSTEM_NM
	
	UNION ALL
	
	SELECT 
	NVL(T501.C501_SURGERY_DATE,T501.C501_ORDER_DATE) SURGDT,
	0 SEGID,
	'ALL DO' SEGNM, '0' SYSID, 'ALL DO' SYSNM, Count(1) cnt FROM T6640_NPI_TRANSACTION T6640, T501_ORDER T501 WHERE T6640.C101_PARTY_SURGEON_ID = p_partyid
	AND NVL(T501.C501_SURGERY_DATE,T501.C501_ORDER_DATE) >= TO_DATE( p_fromdate,'YYYYMM')
	AND NVL(T501.C501_SURGERY_DATE,T501.C501_ORDER_DATE) <= LAST_DAY(TO_DATE( p_todate,'YYYYMM')) AND T6640.C6640_VALID_FL = 'Y' 
	AND NVL (C901_ORDER_TYPE, -9999) NOT IN (SELECT C906_RULE_VALUE FROM V901_ORDER_TYPE_GRP ) 
	AND T6640.C6640_REF_ID = T501.C501_ORDER_ID 
	AND T501.C501_PARENT_ORDER_ID IS NULL 
	AND T6640.C6640_VOID_FL IS NULL AND T501.C501_VOID_FL IS NULL 
	AND T501.C501_DELETE_FL IS NULL
	group by NVL(T501.C501_Surgery_Date,T501.C501_Order_Date);
END gm_fch_calendar_info;


/****************************************************************
    * Description : Fetch Surgeon Consultant Report info
    * Author      : RANJITH
*****************************************************************/   
    
	PROCEDURE gm_fch_projectinfo (
		p_partyid	   IN		T101_PARTY.C101_PARTY_ID%TYPE, 	 
		p_consultcur   OUT		TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_consultcur
		 FOR
            SELECT t202.c202_project_nm projectname,
              t202.c202_project_id projectid,
              t204.c901_role_id roleid,
              get_code_name(t204.c901_role_id) rolenm
            FROM t202_project t202,
              t204_project_team t204
            WHERE t204.c101_party_id = p_partyid
            AND t204.c202_project_id = t202.c202_project_id
            AND t202.c202_void_fl   IS NULL
            AND t204.c204_void_fl   IS NULL
            AND t204.c901_status_id != 92055 ORDER BY projectname;
	END gm_fch_projectinfo;
	
/****************************************************************
    * Description : Save Surgeon party status as open,in progress and active
    *               Open       -  Surgeon is doing sales with Globus
    *               In progess - Rep communication with Surgeon by VIP visit/ Marketing Collateral File Share.
    *                            But, the Surgeon is not yet doing any sales with Globus
    *               Open       - Others will be consider as Open
    * Author      : Gopinathan
*****************************************************************/   
    
	PROCEDURE gm_sav_surgeon_status (
		p_partyid	   IN		T1081_party_status.c101_party_id%TYPE, 	 
		p_status	   IN		T1081_party_status.c901_status%TYPE,
		p_userid	  IN		T1081_party_status.c1081_created_by%TYPE
	)
	AS
		v_party_status_id	T1081_party_status.C1081_party_status_id%TYPE;	
		v_status	   		T1081_party_status.c901_status%TYPE;
	BEGIN
		BEGIN
		   SELECT c901_status INTO v_status 
		     FROM T1081_party_status
		    WHERE c101_party_id = p_partyid
	  		  AND c1081_void_fl IS NULL;
	      EXCEPTION
	      WHEN NO_DATA_FOUND THEN
		    SELECT  s1081_party_status.NEXTVAL INTO v_party_status_id FROM DUAL;
		    INSERT
			  INTO T1081_party_status
			  (
			    C1081_party_status_id,
			    c101_party_id,
			    c901_status,
			    C1081_CREATED_BY,
			    C1081_CREATED_DATE
			  )VALUES
			  (
			    v_party_status_id,
			    p_partyid,
			    p_status,
			    p_userid,
			    CURRENT_DATE
			   );	        
	      END;
		
	  	/* Updating party status open,in progress and active
	  	   --400903 - Open, 400902 - In progress, 400901 - Active
	  	*/
	  		   
		  IF (v_status > p_status) THEN
			UPDATE T1081_party_status
	  		   SET c901_status           = p_status,
	               c1081_updated_by      = p_userid,
	               c1081_updated_date	 = CURRENT_DATE
	  		 WHERE c101_party_id = p_partyid
	  		   AND c1081_void_fl IS NULL;
		  END IF;
		  
	END gm_sav_surgeon_status;	

	
/****************************************************************
    * Description : This job will save Surgeon party status as open,in progress and active
    *               Active       -  Surgeon is doing sales with Globus
    *               In progess - Rep communication with Surgeon by VIP visit/ Marketing Collateral File Share.
    *                            But, the Surgeon is not yet doing any sales with Globus
    *               Open       - Others will be consider as Open
    * Author      : Gopinathan
*****************************************************************/   
    
	PROCEDURE gm_sav_surgeon_status_job (
		p_userid	  IN		T1081_party_status.c1081_created_by%TYPE DEFAULT '706310'
	)	
	AS
	
	  CURSOR cur_party_status
	  IS
		SELECT PARTYID,
		  MIN(STATUS) STATUS
		FROM
		  (
		  --ACTIVE
		  SELECT t101.c101_party_id partyid,
		    400901 STATUS
		  FROM t6645_npi_account t6645,
		    T101_PARTY t101
		  WHERE c101_party_surgeon_id    IS NOT NULL
		  AND t6645.c101_party_surgeon_id = t101.c101_party_id
		  AND c901_party_type             =7000
		  AND C101_DELETE_FL             IS NULL
		  AND (C101_ACTIVE_FL            IS NULL
		  OR C101_ACTIVE_FL               = 'Y' )
		  AND c6645_void_fl              IS NULL
		  AND c101_party_surgeon_id NOT  IN
		    (
		    -- Active Surgeons
		    SELECT c101_party_id
		    FROM T1081_party_status
		    WHERE c901_status  =400901
		    AND c1081_void_fl IS NULL
		    )
		  UNION
		  -- IN PROGRESS
		  SELECT C101_PARTY_ID partyid,
		    400902 STATUS
		  FROM T101_PARTY
		  WHERE c901_party_type =7000
		  AND C101_DELETE_FL   IS NULL
		  AND (C101_ACTIVE_FL  IS NULL
		  OR C101_ACTIVE_FL     = 'Y' )
		  AND C101_PARTY_ID    IN
		    (
		    --Activity - Physician Visit, Sales Calls
			SELECT TO_CHAR(c101_party_id)
		    FROM t6630_activity t6630, t6631_activity_party_assoc t6631
		    WHERE t6630.c6630_activity_id =  t6631.c6630_activity_id 
	        AND t6630.c6630_void_fl IS NULL
	        AND t6631.c6631_void_fl         IS NULL
		    AND c901_activity_party_role =105564
			UNION
			-- Marketing Collateral File Share
		    SELECT c1017_ref_id
		    FROM GLOBUS_APP.t1017_share_detail
		    WHERE c1017_void_fl IS NULL
		    AND c1017_ref_id    IS NOT NULL
		    )
		  AND C101_PARTY_ID NOT IN
		    (
		    -- Active, In progress Surgeons
		    SELECT c101_party_id
		    FROM T1081_party_status
		    WHERE c901_status <= 400902
		    AND c1081_void_fl IS NULL
		    )
		  UNION
		  -- OPEN
		  SELECT C101_PARTY_ID partyid,
		    400903 STATUS
		  FROM T101_PARTY
		  WHERE c901_party_type  =7000
		  AND C101_DELETE_FL    IS NULL
		  AND (C101_ACTIVE_FL   IS NULL
		  OR C101_ACTIVE_FL      = 'Y' )
		  AND C101_PARTY_ID NOT IN
		    (
		    -- Active, In progress and Open Surgeons
		    SELECT c101_party_id 
		    FROM T1081_party_status 
		    WHERE c1081_void_fl
		    IS NULL
		    )
		  )
		GROUP BY PARTYID;
		
	BEGIN
		FOR v_cur IN cur_party_status
  		LOOP
  			gm_sav_surgeon_status(v_cur.PARTYID,v_cur.STATUS,p_userid);
  		END LOOP;
		
	END gm_sav_surgeon_status_job;
	
	/****************************************************************************
* Description : Fetch Surgeon's Social media WebSite link Info like facebook,twitter
* Author      : RANJITH
*****************************************************************************/
PROCEDURE gm_fch_surgeon_socialinfo
  (
    p_partyid IN T1085_PARTY_SITES.C101_PARTY_ID%TYPE,
    p_sgnaddrinfo_cur  OUT  TYPES.CURSOR_TYPE
  )
AS
BEGIN
	OPEN p_sgnaddrinfo_cur FOR
	    SELECT C1085_FB facebook ,C1085_TWITTER twitter,C1085_LINKEDIN linkedin ,C1085_GOOGLE google,C1085_VITALS vitals,C1085_OPENPAYMENT openpayment ,C1085_WEBSITE website 
	    FROM T1085_PARTY_SITES 
	    WHERE C101_PARTY_ID = p_partyid
	     AND C1085_VOID_FL IS NULL;
	
	END gm_fch_surgeon_socialinfo;
	
	
	
/****************************************************************************
* Description : Save And Update Surgeon's Social media WebSite link Info like facebook,twitter
* Author      : RANJITH
*****************************************************************************/	
PROCEDURE gm_sav_sociallink(
	p_partyid       IN T1085_PARTY_SITES.C101_PARTY_ID%TYPE,      
	p_facebook      IN T1085_PARTY_SITES.C1085_FB%TYPE,
	p_twitter       IN T1085_PARTY_SITES.C1085_TWITTER%TYPE,
	p_google        IN T1085_PARTY_SITES.C1085_GOOGLE%TYPE,
	p_linkedin      IN T1085_PARTY_SITES.C1085_LINKEDIN%TYPE,
	p_website       IN T1085_PARTY_SITES.C1085_WEBSITE%TYPE,
	p_openpayment   IN T1085_PARTY_SITES.C1085_OPENPAYMENT%TYPE,
	p_vitals        IN T1085_PARTY_SITES.C1085_VITALS%TYPE,
	p_userid        IN T1085_PARTY_SITES.C1085_CREATED_BY%TYPE
	 
	)
	
	AS	 
	BEGIN
	UPDATE T1085_PARTY_SITES SET 
	C1085_FB           = p_facebook,
	C1085_TWITTER      = p_twitter,
	C1085_GOOGLE       = p_google,
	C1085_LINKEDIN     = p_linkedin,
	C1085_WEBSITE      = p_website,
	C1085_OPENPAYMENT  = p_openpayment,	
	C1085_VITALS       = p_vitals,
	C1085_UPDATED_BY   = p_userid,
	C1085_UPDATED_DATE = CURRENT_DATE
	
	WHERE C101_PARTY_ID =  p_partyid
	AND C1085_VOID_FL IS NULL;
	
	 IF (SQL%ROWCOUNT         = 0) THEN
	INSERT INTO T1085_PARTY_SITES (C1085_PARTY_SITES_ID,C101_PARTY_ID,C1085_FB,C1085_TWITTER,C1085_GOOGLE,C1085_LINKEDIN,C1085_WEBSITE,C1085_OPENPAYMENT,C1085_VITALS,C1085_CREATED_BY,C1085_CREATED_DATE) 
	VALUES (S1085_PARTY_SITES.nextval,p_partyid,p_facebook,p_twitter,p_google,p_linkedin,p_website,p_openpayment,p_vitals,p_userid,CURRENT_DATE);
	
	END IF;
	END gm_sav_sociallink;
--
END gm_pkg_crm_surgeon;
/