-- @"c:\database\packages\crm\gm_pkg_crm_address.bdy"


CREATE OR REPLACE PACKAGE BODY gm_pkg_crm_address
IS

	PROCEDURE gm_crm_sav_addressinfo (
		p_partyid		IN OUT	 t101_party.c101_party_id%TYPE
	  , p_addressid 	IN OUT	 t106_address.c106_address_id%TYPE
	  , p_addresstype	IN		 t106_address.c901_address_type%TYPE
	  , p_addline1		IN		 t106_address.c106_add1%TYPE
	  , p_addline2		IN		 t106_address.c106_add2%TYPE
	  , p_city			IN		 t106_address.c106_city%TYPE
	  , p_state 		IN		 t106_address.c901_state%TYPE
	  , p_country		IN		 t106_address.c901_country%TYPE
	  , p_zipcode		IN		 t106_address.c106_zip_code%TYPE
	  , p_preference	IN		 t106_address.c106_seq_no%TYPE
	  , p_userid		IN		 t106_address.c106_created_by%TYPE
	  , p_inactivefl	IN		 t106_address.c106_inactive_fl%TYPE
	  , p_primaryfl 	IN		 t106_address.c106_primary_fl%TYPE
	  , p_prefCarrier 	IN		 t106_address.c901_carrier%TYPE
	  , p_voidfl 		IN		 t106_address.c106_void_fl%TYPE
	)
	AS
		
	BEGIN
		--
		UPDATE t106_address
		   SET c901_address_type = p_addresstype
			 , c106_add1 = p_addline1
			 , c106_add2 = p_addline2
			 , c106_city = p_city
			 , c901_state = p_state
			 , c901_country = p_country
			 , c106_zip_code = p_zipcode
			 , c106_seq_no = p_preference
			 , c106_last_updated_by = p_userid
			 , c106_last_updated_date = SYSDATE
			 , c106_inactive_fl = p_inactivefl
			 , c106_primary_fl = p_primaryfl
			 , c901_carrier = p_prefCarrier     -- Updating Preferred Carrier value for a Rep -- PMT-4359
			 , c106_void_fl = p_voidfl
		 WHERE c106_address_id = p_addressid AND c101_party_id = p_partyid AND c106_void_fl IS NULL;

		IF (SQL%ROWCOUNT = 0)
		THEN
			SELECT s106_address.NEXTVAL
			  INTO p_addressid
			  FROM DUAL;

			INSERT INTO t106_address
						(c106_address_id, c101_party_id, c901_address_type, c106_add1, c106_add2, c106_city
					   , c901_state, c901_country, c106_zip_code, c106_seq_no, c106_created_by, c106_created_date
					   , c106_inactive_fl, c106_primary_fl , c901_carrier, c106_void_fl     -- Inserting Preferred Carrier value for a New Rep -- PMT-4359
						)
				 VALUES (p_addressid, p_partyid, p_addresstype, p_addline1, p_addline2, p_city
					   , p_state, p_country, p_zipcode, p_preference, p_userid, SYSDATE
					   , p_inactivefl, p_primaryfl , p_prefCarrier, p_voidfl   -- Inserting Preferred Carrier value for a New Rep -- PMT-4359
						);
		END IF;
	
	END gm_crm_sav_addressinfo;
	
	PROCEDURE gm_crm_sav_activity_addr_info (
	    p_addressid 	IN OUT	 t106_address.c106_address_id%TYPE
	  , p_addresstype	IN		 t106_address.c901_address_type%TYPE
	  , p_addline1		IN		 t106_address.c106_add1%TYPE
	  , p_addline2		IN		 t106_address.c106_add2%TYPE
	  , p_city			IN		 t106_address.c106_city%TYPE
	  , p_state 		IN		 t106_address.c901_state%TYPE
	  , p_country		IN		 t106_address.c901_country%TYPE
	  , p_zipcode		IN		 t106_address.c106_zip_code%TYPE
	  , p_userid		IN		 t106_address.c106_created_by%TYPE
	)
	AS
		
	BEGIN
		--
		UPDATE t106_address
		   SET c901_address_type = p_addresstype
			 , c106_add1 = p_addline1
			 , c106_add2 = p_addline2
			 , c106_city = p_city
			 , c901_state = p_state
			 , c901_country = p_country
			 , c106_zip_code = p_zipcode
			 , c106_last_updated_by = p_userid
			 , c106_last_updated_date = SYSDATE
		 WHERE c106_address_id = p_addressid;

		IF (SQL%ROWCOUNT = 0)
		THEN
			SELECT s106_address.NEXTVAL
			  INTO p_addressid
			  FROM DUAL;

			INSERT INTO t106_address
						(c106_address_id, c901_address_type, c106_add1, c106_add2, c106_city
					   , c901_state, c901_country, c106_zip_code, c106_created_by, c106_created_date 
						)
				 VALUES (p_addressid, p_addresstype, p_addline1, p_addline2, p_city
					   , p_state, p_country, p_zipcode, p_userid, SYSDATE  
						);
		END IF;
	
	END gm_crm_sav_activity_addr_info;
	
END gm_pkg_crm_address;
/