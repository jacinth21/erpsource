CREATE OR REPLACE
PACKAGE BODY gm_pkg_crm_preceptorship_txn
IS

	/*****************************************************************************************
	* Description : This Procedure is used to save case request JSON into appropriate table
	* 				by calling repective procedures.
	******************************************************************************************/
	PROCEDURE gm_sav_crm_case_json(
	    p_input_string IN CLOB,
	    p_user_id      IN t6660_case_request.c6660_created_by%TYPE,
	    p_out_id 	   OUT VARCHAR2 )
	AS
	  v_json 	 json_object_t;
	  v_date_fmt VARCHAR2(20);
	  v_fromdate VARCHAR2(50);
	  v_enddate  VARCHAR2(50);
	BEGIN
	
	  v_date_fmt := GET_RULE_VALUE('DATEFMT', 'DATEFORMAT');
	  v_json     := json_object_t.parse(p_input_string);
	  p_out_id   := v_json.get_String('casereqid');
	  v_fromdate := v_json.get_String('frmdt');
	  v_enddate  := v_json.get_String('todt');
	  
	  --CRM Preceporship Case request master json into t6660 table
	  gm_sav_crm_casereq( 
	  p_out_id, 
	  v_json.get_String('actid'),
	  v_json.get_String('type'),
	  v_json.get_String('crdfl'),
	  TO_DATE(v_fromdate,v_date_fmt),
	  v_json.get_String('hotlnm'),
	  v_json.get_String('hotlprice'),
	  v_json.get_String('labadd'),
	  v_json.get_String('labinv'),
	  v_json.get_String('note'),
	  v_json.get_String('pcmninv'),
	  v_json.get_String('pcprcrf'),
	  v_json.get_String('segid'),
	  v_json.get_String('status'),
	  v_json.get_String('surgid'),
	  v_json.get_String('survfl'),
	  TO_DATE(v_enddate,v_date_fmt),
	  v_json.get_String('trfstatis'),
	  v_json.get_String('trvlnm'),
	  v_json.get_String('trvlprice'),
	  v_json.get_String('division'),
	  v_json.get_String('workflow'),
	  v_json.get_String('pathology'),
	  v_json.get_String('pstype'),
	  v_json.get_String('clobj'),
	  v_json.get_String('region'),
	  v_json.get_String('zone'),
	  v_json.get_String('adminsts'),
	  v_json.get_String('atndtyp'),
	  v_json.get_String('atndnm'),
	  v_json.get_String('webinarfl'),
	  p_user_id );
	  
	   -- CRM Preceporship case reqest set details json into t6662
	  gm_sav_crm_casereq_dtl_json(p_out_id,treat(v_json.get('arrcrmpscasereqvo') AS json_array_t),p_user_id);
	  
	  -- CRM Preceporship case reqest party association json into t6661
	  gm_sav_crm_casereq_asso_json(p_out_id,treat(v_json.get('arrcrmpscaseassvo') AS json_array_t),p_user_id);
	  
	  -- CRM Preceporship case reqest Attr json into t6663
	  gm_sav_crm_casereq_attr_json(p_out_id,treat(v_json.get('arrcrmpscaseattrvo') AS json_array_t),p_user_id);
	  
	END gm_sav_crm_case_json;

	/*****************************************************************************************
	* Description : This Procedure is used to iterate case request detail JSON array 
	* 				and call gm_sav_crm_casereq_dtl
	******************************************************************************************/
	PROCEDURE gm_sav_crm_casereq_dtl_json(
		p_casereqid  IN t6660_case_request.c6660_case_request_id%TYPE,
		p_attr_array IN json_array_t,
		p_user_id    IN t6660_case_request.c6660_created_by%TYPE)
	AS
		v_casereq_attr_obj json_object_t;
	BEGIN
		 FOR i IN 0 .. p_attr_array.get_size - 1
		 LOOP
		 
		    v_casereq_attr_obj := treat(p_attr_array.get(i) AS json_object_t);
		    
			gm_sav_crm_casereq_dtl (v_casereq_attr_obj.get_String('crdid'),
									p_casereqid,
									v_casereq_attr_obj.get_String('setid'),
									v_casereq_attr_obj.get_String('setnm'),
									v_casereq_attr_obj.get_String('voidfl'),
									p_user_id );
		 END LOOP;
	END gm_sav_crm_casereq_dtl_json;
	
	/*****************************************************************************************
	* Description : This Procedure is used to iterate case request associate JSON array 
	* 				and call gm_sav_crm_casereq_asso
	******************************************************************************************/
	PROCEDURE gm_sav_crm_casereq_asso_json(
		p_casereqid  IN t6660_case_request.c6660_case_request_id%TYPE,
		p_attr_array IN json_array_t,
		p_user_id    IN t6660_case_request.c6660_created_by%TYPE)
	AS
		v_casereq_asso_obj json_object_t;
	BEGIN
		
		 FOR i IN 0 .. p_attr_array.get_size - 1
		 LOOP
		  
		    v_casereq_asso_obj := treat(p_attr_array.get(i) AS json_object_t);
		    
			gm_sav_crm_casereq_asso (v_casereq_asso_obj.get_String('craid'), 
								   p_casereqid, v_casereq_asso_obj.get_String('role'),
								   v_casereq_asso_obj.get_String('partyid'),
								   v_casereq_asso_obj.get_String('voidfl'),
								   p_user_id );
			
		 END LOOP;
		 
	END gm_sav_crm_casereq_asso_json;

	/*****************************************************************************************
	* Description : This Procedure is used to iterate case request attribute JSON array 
	* 				and call gm_sav_crm_casereq_attr
	******************************************************************************************/
	PROCEDURE gm_sav_crm_casereq_attr_json(
		p_casereqid  IN t6660_case_request.c6660_case_request_id%TYPE,
		p_attr_array IN json_array_t,
		p_user_id    IN t6660_case_request.c6660_created_by%TYPE)
	AS
		v_attr_obj json_object_t;
	BEGIN
		
		 FOR i IN 0 .. p_attr_array.get_size - 1
		 LOOP
		  
		    v_attr_obj := treat(p_attr_array.get(i) AS json_object_t);
		    
			gm_sav_crm_casereq_attr( v_attr_obj.get_String('attrid'),
									p_casereqid,
									v_attr_obj.get_String('attrtype'),
									v_attr_obj.get_String('attrval'),
									v_attr_obj.get_String('attrnm'),
									v_attr_obj.get_String('voidfl'),
									p_user_id) ;
			
		 END LOOP;
		 
	END gm_sav_crm_casereq_attr_json;
	
	/*****************************************************************************************
	* Description : This Procedure is used to update/insert into t6660  table
	******************************************************************************************/
	PROCEDURE gm_sav_crm_casereq(
	    p_casereqid 	IN OUT 			t6660_case_request.c6660_case_request_id%TYPE,
	    p_actid     	IN 				t6660_case_request.c6630_activity_id%TYPE,
	    p_type     		IN 				t6660_case_request.c901_type%TYPE,
	    p_crdfl     	IN 				t6660_case_request.c6660_credential_fl%TYPE,
	    p_frmdt     	IN 				t6660_case_request.c6660_from_dt%TYPE,
	    p_hotlnm    	IN 				t6660_case_request.c6660_hotel%TYPE,
	    p_hotlprice 	IN 				t6660_case_request.c6660_hotel_price%TYPE,
	    p_labadd    	IN 				t6660_case_request.c6660_lab_address%TYPE,
	    p_labinv    	IN 				t6660_case_request.c6660_lab_invoice%TYPE,
	    p_note      	IN 				t6660_case_request.c6660_note%TYPE,
	    p_pcmninv   	IN 				t6660_case_request.c6660_pecimen_invoice%TYPE,
	    p_pcprcrf   	IN 				t6660_case_request.c6660_pcrf_crf_status%TYPE,
	    p_segid     	IN 				t6660_case_request.c2090_segment_id%TYPE,
	    p_status    	IN 				t6660_case_request.c901_status%TYPE,
	    p_surgid    	IN 				t6660_case_request.c101_surgeon_party_id%TYPE,
	    p_survfl    	IN 				t6660_case_request.c6660_survey_fl%TYPE,
	    p_todt      	IN 				t6660_case_request.c6660_to_dt%TYPE,
	    p_trfstatis 	IN 				t6660_case_request.c6660_trf_status%TYPE,
	    p_trvlnm    	IN 				t6660_case_request.c6660_travel%TYPE,
	    p_trvlprice 	IN 				t6660_case_request.c6660_travel_price%TYPE,
	    p_division 		IN 				t6660_case_request.c1910_division_id%TYPE,
	    p_workflow 		IN 				t6660_case_request.c901_work_flow%TYPE,
	    p_pathology 	IN 				t6660_case_request.c6660_pathology%TYPE,
	    p_pstype 		IN 				t6660_case_request.c901_preceptor_type%TYPE,
	    p_clobj 		IN 				t6660_case_request.c6660_call_objectives%TYPE,
	    p_region 		IN 				t6660_case_request.c901_region%TYPE,
	    p_zone        	IN 				t6660_case_request.c901_zone%TYPE,
		p_adminsts    	IN 				t6660_case_request.c901_admin_status%TYPE ,
	    p_atndtyp 		IN 				t6660_case_request.c901_attendee_type%TYPE, 
	    p_atndnm    	IN 				t6660_case_request.c6630_attendee_name%TYPE ,
	    p_webinarfl 	IN 				t6660_case_request.c6660_webinar_fl%TYPE,	    
	    p_userid    	IN 				t6660_case_request.c6660_created_by%TYPE)
	AS
	BEGIN
		
	  UPDATE T6660_CASE_REQUEST
	  SET c6630_activity_id       = p_actid, 
	    c901_type				  = p_type,
	    c6660_credential_fl       = p_crdfl ,
	    c6660_from_dt             = p_frmdt ,
	    c6660_hotel               = p_hotlnm ,
	    c6660_hotel_price         = p_hotlprice ,
	    c6660_lab_address         = p_labadd ,
	    c6660_lab_invoice         = p_labinv ,
	    c6660_note                = p_note ,
	    c6660_pecimen_invoice     = p_pcmninv ,
	    c6660_pcrf_crf_status     = p_pcprcrf ,
	    c2090_segment_id          = p_segid ,
	    c901_status               = p_status ,
	    c101_surgeon_party_id     = p_surgid ,
	    c6660_survey_fl           = p_survfl ,
	    c6660_to_dt               = p_todt ,
	    c6660_trf_status          = p_trfstatis ,
	    c6660_travel              = p_trvlnm ,
	    c6660_travel_price        = p_trvlprice ,
		c1910_division_id		  = p_division ,
		c901_work_flow            = p_workflow ,
		c6660_pathology           = p_pathology ,
		c901_preceptor_type       = p_pstype ,
		c6660_call_objectives     = p_clobj ,
		c901_region			      = p_region ,
    	c901_zone         	  	  = p_zone ,
        c901_admin_status         = p_adminsts ,
		c901_attendee_type     	  = p_atndtyp ,
		c6630_attendee_name		  = p_atndnm ,
		c6660_webinar_fl		  = p_webinarfl ,
	    c6660_updated_by          = p_userid ,
	    c6660_updated_dt          = CURRENT_DATE,
	    c6660_update_fl           = NULL
	  WHERE c6660_case_request_id = p_casereqid
	    AND c6660_void_fl IS NULL;
	  IF (SQL%ROWCOUNT            = 0) THEN
	      SELECT 'CR-'||s6660_case_request.nextval INTO p_casereqid FROM DUAL;
	      
	    INSERT
	    INTO t6660_case_request
	      (
	        c6660_case_request_id,
	        c6630_activity_id,
	        c901_type,
	        c6660_credential_fl,
	        c6660_created_by,
	        c6660_created_dt,
	        c6660_from_dt,
	        c6660_hotel,
	        c6660_hotel_price,
	        c6660_lab_address,
	        c6660_lab_invoice,
	        c6660_note,
	        c6660_pecimen_invoice,
	        c6660_pcrf_crf_status,
	        c2090_segment_id,
	        c901_status,
	        c101_surgeon_party_id,
	        c6660_survey_fl,
	        c6660_to_dt,
	        c6660_trf_status,
	        c6660_travel,
	        c6660_travel_price,
	        c1910_division_id,
	        c901_work_flow,
	        c6660_pathology,
	        c901_preceptor_type,
	        c6660_call_objectives,
	        c901_region,
	        c901_zone,
	        c901_admin_status,
			c901_attendee_type,
	        c6630_attendee_name,
	        c6660_webinar_fl
	      )
	      VALUES
	      (
	        p_casereqid,
	        p_actid,
	        p_type,
	        p_crdfl,
	        p_userid,
	        CURRENT_DATE,
	        p_frmdt,
	        p_hotlnm,
	        p_hotlprice,
	        p_labadd,
	        p_labinv,
	        p_note,
	        p_pcmninv,
	        p_pcprcrf,
	        p_segid,
	        p_status,
	        p_surgid,
	        p_survfl,
	        p_todt,
	        p_trfstatis,
	        p_trvlnm,
	        p_trvlprice,
	        p_division,
	        p_workflow,
	        p_pathology,
	        p_pstype,
	        p_clobj,
	        p_region,
	        p_zone,
	        p_adminsts,
			p_atndtyp,
	        p_atndnm,
	        p_webinarfl
	      );
	  END IF;
	  IF (p_status = '105762') THEN
            gm_pkg_crm_preceptorship_txn.gm_sav_casereq_status(p_casereqid,p_userid);
      END IF;
	END gm_sav_crm_casereq;

	/*****************************************************************************************
	* Description : This Procedure is used to update/insert into t6662  table
	******************************************************************************************/
	PROCEDURE gm_sav_crm_casereq_dtl
	  (
	    p_crdid     IN 				t6662_case_request_detail.c6662_case_req_detail_id%TYPE,
	    p_casereqid IN 				t6662_case_request_detail.c6660_case_request_id%TYPE,
	    p_setid     IN 				t6662_case_request_detail.c207_set_id%TYPE,
	    p_setnm     IN 				t6662_case_request_detail.c6662_set_nm%TYPE,
	    p_voidfl    IN 				t6662_case_request_detail.c6662_void_fl%TYPE,
	    p_userid	IN 				t6662_case_request_detail.c6662_created_by%TYPE
	  )
	AS
	BEGIN
	  UPDATE t6662_case_request_detail
	  SET c207_set_id                = p_setid ,
	    c6662_set_nm                 = p_setnm ,
	    c6662_void_fl                = p_voidfl ,
	    c6662_updated_by             = p_userid ,
	    c6662_updated_dt             = CURRENT_DATE
	  WHERE c6662_case_req_detail_id = p_crdid
	  AND c6660_case_request_id      = p_casereqid
	  AND c6662_void_fl             IS NULL;
	  
	  IF (SQL%ROWCOUNT = 0) 
	  THEN
	    INSERT
	    INTO t6662_case_request_detail
	      (
	        c6662_case_req_detail_id,
	        c6660_case_request_id,
	        c207_set_id,
	        c6662_set_nm,
	        c6662_void_fl,
	        c6662_created_by,
	        c6662_created_dt
	      )
	      VALUES
	      (
		    s6662_case_request_detail.nextval,
	        p_casereqid,
	        p_setid,
	        p_setnm,
	        p_voidfl,
	        p_userid,
	        CURRENT_DATE
	      );
	  END IF;
	  
	END gm_sav_crm_casereq_dtl;

	/*****************************************************************************************
	* Description : This Procedure is used to update/insert into t6661  table
	******************************************************************************************/
	PROCEDURE gm_sav_crm_casereq_asso
	  (
	    p_craid     IN 				t6661_case_request_assoc.c6661_case_req_assoc_id%TYPE,
	    p_casereqid IN 				t6662_case_request_detail.c6660_case_request_id%TYPE,
	    p_role      IN 				t6661_case_request_assoc.c901_role%TYPE,
	    p_partyid   IN 				t6661_case_request_assoc.c101_party_id%TYPE,
	    p_voidfl    IN 				t6661_case_request_assoc.c6661_void_fl%TYPE,
	    p_userid    IN 				t6661_case_request_assoc.c6661_created_by%TYPE
	  )
	AS
	BEGIN
	  UPDATE t6661_case_request_assoc
	  SET c101_party_id             = p_partyid ,
	    c901_role                   = p_role ,
	    c6661_void_fl               = p_voidfl ,
	    c6661_updated_by            = p_userid ,
	    c6661_updated_dt            = CURRENT_DATE
	  WHERE C6661_CASE_REQ_ASSOC_ID = p_craid
	  AND C6660_CASE_REQUEST_ID     = p_casereqid
	  AND C6661_VOID_FL            IS NULL;
	  
	  IF (SQL%ROWCOUNT              = 0) THEN
	    INSERT
	    INTO t6661_case_request_assoc
	      (
	        c6661_case_req_assoc_id,
	        c6660_case_request_id,
	        c901_role,
	        c101_party_id,
	        c6661_created_by,
	        c6661_created_dt
	      )
	      VALUES
	      (
		    s6661_case_request_assoc.nextval,
	        p_casereqid,
	        p_role,
	        p_partyid,
	        p_userid,
	        CURRENT_DATE
	      );
	  END IF;
	END gm_sav_crm_casereq_asso;

	/********************************************************************************
	* Description : Procedure is used to update/insert into t6663  table
	*********************************************************************************/  
	PROCEDURE gm_sav_crm_casereq_attr(
	    p_attrid    IN t6663_case_req_attr.C6663_case_req_attr_id%TYPE,
	    p_casereqid IN t6663_case_req_attr.c6660_case_request_id%TYPE,
	    p_attrtype  IN t6663_case_req_attr.c901_attribute_type%TYPE,
	    p_attrval   IN t6663_case_req_attr.c6663_attribute_value%TYPE,
	    p_attrnm    IN t6663_case_req_attr.c6663_attribute_nm%TYPE,
	    p_voidfl    IN t6663_case_req_attr.c6663_void_fl%TYPE,
	    p_userid    IN t6663_case_req_attr.C6663_Updated_By%TYPE )
	AS
	BEGIN
	  UPDATE t6663_case_req_attr
	  SET c901_attribute_type      = p_attrtype ,
	    c6663_attribute_value      = p_attrval ,
	    c6663_attribute_nm         = p_attrnm ,
	    c6663_void_fl              = p_voidfl ,
	    c6663_updated_by           = p_userid ,
	    c6663_updated_dt         = CURRENT_DATE
	  WHERE C6663_case_req_attr_id = p_attrid
	  AND c6660_case_request_id    = p_casereqid
	  AND C6663_VOID_FL           IS NULL;
	  
	  IF (SQL%ROWCOUNT             = 0) THEN
	    INSERT
	    INTO t6663_case_req_attr
	      (
	        C6663_case_req_attr_id,
	        c6660_case_request_id,
	        c901_attribute_type,
	        c6663_attribute_value,
	        c6663_attribute_nm,
	        c6663_void_fl,
	        c6663_created_by,
	        c6663_created_dt
	      )
	      VALUES
	      (
	        s6663_case_req_attr.NEXTVAL,
	        p_casereqid,
	        p_attrtype,
	        p_attrval,
	        p_attrnm,
	        p_voidfl,
	        p_userid,
	        CURRENT_DATE
	      );
	  END IF;
	END gm_sav_crm_casereq_attr;
	
	/********************************************************************************
	* Description : Procedure is used to iterate JSON input to link/unlink host case into case request
	* 			    Ex. JSON is {"actid":"HC-100","linkreqs":["CR-101","CR-102"],"unlinkreqs":["CR-103","CR-104"]}
	*********************************************************************************/  
	PROCEDURE gm_sav_case_req_map_json(
	    p_input_string IN CLOB,
	    p_user_id      IN t6660_case_request.c6660_created_by%TYPE)
	AS
	  v_json 	 	JSON_OBJECT_T;
	  v_json_array  JSON_ARRAY_T;
	  v_actid	 	t6660_case_request.c6630_activity_id%TYPE;
	BEGIN
		v_json     	:= json_object_t.parse(p_input_string);
		v_actid    	:= v_json.get_String('actid');
		
		--Link host case into case request
		v_json_array	:= TREAT(v_json.get('linkreqs') AS JSON_ARRAY_T);
		
		FOR i IN 0 .. v_json_array.get_size - 1
		LOOP
		    gm_sav_case_req_map(v_actid,v_json_array.get_string(i),105761,p_user_id);--105761-'Scheduled'
	  	END LOOP;
	  	
	  	--Unlink host case into case request
		v_json_array	:= TREAT(v_json.get('unlinkreqs') AS JSON_ARRAY_T);
		
		FOR i IN 0 .. v_json_array.get_size - 1
		LOOP
		    gm_sav_case_req_map(NULL,v_json_array.get_string(i),105765,p_user_id);--105765-'Open'
	  	END LOOP;
	  	
	END gm_sav_case_req_map_json;
	
	/********************************************************************************
	* Description : Procedure is used to link/unlink host case into case request
	*********************************************************************************/  
	PROCEDURE gm_sav_case_req_map(
	  p_actid	 	IN t6660_case_request.c6630_activity_id%TYPE,
	  p_case_req_id	IN t6660_case_request.c6660_case_request_id%TYPE,
	  p_status		IN t6660_case_request.c901_status%TYPE,
	  p_user_id     IN t6660_case_request.c6660_created_by%TYPE)
	AS
	v_count       NUMBER;
	v_actid       t6660_case_request.c6630_activity_id%TYPE;
	BEGIN
		-- if p_actid is NULL then store the activity id in the variable v_actid from the table T6660_CASE_REQUEST
		SELECT c6630_activity_id v_actid INTO v_actid FROM T6660_CASE_REQUEST WHERE c6660_case_request_id = p_case_req_id;
		
		UPDATE t6660_case_request 
		   SET c6630_activity_id = p_actid,
		       c901_status       = p_status, --'Open'
		       c6660_update_fl   = NULL,
		       c6660_updated_by  = p_user_id,
		       c6660_updated_dt  = CURRENT_DATE
		 WHERE c6660_case_request_id = p_case_req_id 
		   AND c6660_void_fl IS NULL;	
		   
		   -- get count of host case activity assigned to the case request stored in v_count
		   SELECT COUNT(*) INTO v_count FROM T6660_CASE_REQUEST WHERE C6630_ACTIVITY_ID = NVL(p_actid,v_actid) ;		   
			-- v_count IS 0 then update the activity atatus as open else scheduled in T6630_Activity
		   IF v_count = 0 THEN
		    IF v_actid IS NOT NULL THEN   -- p_actid is NULL so we use v_actid
			   UPDATE t6630_activity 
			   SET C901_ACTIVITY_STATUS       = '105765', --'Open'
			       C6630_UPDATE_FL            = NULL,
			       C6630_LAST_UPDATED_BY      = p_user_id,
			       C6630_LAST_UPDATED_DATE    = CURRENT_DATE
			 WHERE C6630_ACTIVITY_ID = v_actid 
			   AND C6630_VOID_FL IS NULL;	
			 END IF;
		   ELSE		   
			   UPDATE t6630_activity 
			   SET C901_ACTIVITY_STATUS       = '105766', --'Closed'
			       C6630_UPDATE_FL            = NULL,
			       C6630_LAST_UPDATED_BY      = p_user_id,
			       C6630_LAST_UPDATED_DATE    = CURRENT_DATE
			 WHERE C6630_ACTIVITY_ID   = p_actid 
			   AND C6630_VOID_FL IS NULL;	
			   
		   END IF;
	
	END gm_sav_case_req_map;
	
/********************************************************************************
* Description : Procedure is used to cancel the host case and updating the status
*********************************************************************************/
PROCEDURE gm_sav_hostcase_status(
    p_actid   IN t6660_case_request.c6630_activity_id%TYPE,
    p_user_id IN t6660_case_request.c6660_created_by%TYPE)
AS
BEGIN
  UPDATE t6630_activity
  SET C901_ACTIVITY_STATUS  = '105762', --'Cancelled'
    C6630_LAST_UPDATED_BY   = p_user_id,
    C6630_LAST_UPDATED_DATE = CURRENT_DATE
  WHERE C6630_ACTIVITY_ID   = p_actid
  AND C6630_VOID_FL        IS NULL;
  UPDATE t6660_case_request
  SET c901_status         = '105765', --'Open'
    c6630_activity_id     = NULL,
    c6660_update_fl       = NULL,
    c6660_updated_by      = p_user_id,
    c6660_updated_dt      = CURRENT_DATE
  WHERE c6630_activity_id = p_actid
  AND c901_status        NOT IN ('105762') -- Cancelled
  AND c6660_void_fl      IS NULL;
END gm_sav_hostcase_status;

/********************************************************************************
* Description : Procedure is used to cancel the Case Request and updating the status
*********************************************************************************/
PROCEDURE gm_sav_casereq_status(
    p_case_req_id IN t6660_case_request.c6660_case_request_id%TYPE,
    p_user_id   IN t6660_case_request.c6660_created_by%TYPE)
AS
	v_count       NUMBER;
	v_actid       t6660_case_request.c6630_activity_id%TYPE;
BEGIN
  SELECT COUNT(1) INTO v_count FROM T6660_CASE_REQUEST WHERE c6630_activity_id IN
    (SELECT c6630_activity_id FROM T6660_CASE_REQUEST WHERE c6660_case_request_id = p_case_req_id AND c6660_void_fl IS NULL)
  AND c6660_void_fl IS NULL;
  
  SELECT c6630_activity_id v_actid INTO v_actid FROM T6660_CASE_REQUEST WHERE c6660_case_request_id = p_case_req_id;
  
  IF v_count = 1 THEN
    UPDATE t6630_activity
    SET C901_ACTIVITY_STATUS  = '105765', --'open'
      C6630_LAST_UPDATED_BY   = p_user_id,
      C6630_LAST_UPDATED_DATE = CURRENT_DATE
    WHERE C6630_ACTIVITY_ID  IN
      (SELECT c6630_activity_id
      FROM T6660_CASE_REQUEST
      WHERE c6660_case_request_id = p_case_req_id
      AND c6660_void_fl          IS NULL
      )
    AND C901_ACTIVITY_STATUS NOT IN ('105762') -- Cancelled
    AND C6630_VOID_FL IS NULL;
  END IF;
  
  UPDATE t6660_case_request
  SET c6630_activity_id       = NULL,
    c901_status               = '105762', --'Cancelled'
    c6660_update_fl           = NULL,
    c6660_updated_by          = p_user_id,
    c6660_updated_dt          = CURRENT_DATE
  WHERE c6660_case_request_id = p_case_req_id
  AND c6660_void_fl          IS NULL;
  
END gm_sav_casereq_status;

/********************************************************************************
* Description : Procedure is used to update the Case Request Status and update the Host Case Status
*********************************************************************************/

PROCEDURE gm_sav_ps_status_nightly_job
AS
BEGIN
  UPDATE t6660_case_request
  SET c901_status        = '105764' -- Completed 
  WHERE c6660_void_fl   IS NULL
  AND c6630_activity_id IN
    (SELECT c6630_activity_id
    FROM t6630_activity
    WHERE c901_activity_status = '105766' -- Closed
    AND c6630_to_dt          <= CURRENT_DATE
    );
    
  UPDATE t6630_activity
  SET c901_activity_status  = '105764' -- Completed
  WHERE c901_activity_status = '105765' -- Open
  AND c6630_void_fl        IS NULL
  AND c6630_to_dt          <= CURRENT_DATE;
END gm_sav_ps_status_nightly_job;

END gm_pkg_crm_preceptorship_txn;
/