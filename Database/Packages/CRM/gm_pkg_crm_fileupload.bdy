CREATE OR REPLACE
		PACKAGE BODY gm_pkg_crm_fileupload
			IS
   	   /****************************************************************
    	* Description : Procedure to save Fileupload Info
    	* Author      : tvenkateswarlu
    	*****************************************************************/
	PROCEDURE gm_sav_crm_fileupload (    
		p_fileid			IN   OUT   t903_upload_file_list.c903_upload_file_list%TYPE, 
		p_filetitle			IN	   t903_upload_file_list.c901_file_title%TYPE,
		p_filename			IN         t903_upload_file_list.c903_file_name%TYPE,
		p_refid				IN	   t903_upload_file_list.c903_ref_id%TYPE,
		p_reftype			IN         t903_upload_file_list.C901_REF_TYPE%TYPE,
		p_refgrp 			IN	   t903_upload_file_list.c901_ref_grp%TYPE,
		p_deletefl			IN         t903_upload_file_list.c903_delete_fl%TYPE,
		p_userid			IN         t903_upload_file_list.c903_created_by%TYPE
	)AS
	     
	    BEGIN
		    UPDATE T903_UPLOAD_FILE_LIST
		    SET
		    	C901_FILE_TITLE = p_filetitle,
		    	C903_FILE_NAME = p_filename,
				C903_REF_ID = p_refid,
				C901_REF_GRP = p_refgrp,
		    	C901_REF_TYPE = p_reftype,
		    	C903_DELETE_FL = p_deletefl,
		    	C903_LAST_UPDATED_DATE = SYSDATE,
		    	C903_LAST_UPDATED_BY = p_userid
		    WHERE C903_UPLOAD_FILE_LIST = p_fileid;
		    	
		   IF (SQL%ROWCOUNT = 0)
		         THEN 	
		    
		         SELECT S903_UPLOAD_FILE_LIST.NEXTVAL INTO p_fileid FROM DUAL;
		    
		         INSERT INTO T903_UPLOAD_FILE_LIST(C903_UPLOAD_FILE_LIST,C901_FILE_TITLE,C903_FILE_NAME,C903_REF_ID,C901_REF_TYPE, C901_REF_GRP, C903_DELETE_FL,C903_CREATED_BY,C903_CREATED_DATE,C903_LAST_UPDATED_BY,C903_LAST_UPDATED_DATE,C901_TYPE,C903_FILE_SIZE,C903_FILE_SEQ_NO)
		         	VALUES(p_fileid,p_filetitle,p_filename,p_refid,p_reftype,p_refgrp,p_deletefl,p_userid,SYSDATE,'','','','',''); 
		    END IF;         
		        
   END gm_sav_crm_fileupload;
	     
	END gm_pkg_crm_fileupload;
    /	 