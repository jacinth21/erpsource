CREATE OR REPLACE 
PACKAGE BODY gm_pkg_crm_merc
IS
/****************************************************************
* Description : Fetch the MERC Details to display in CRM Dashboard widget
*****************************************************************/
PROCEDURE  gm_fch_merc (
  p_input		IN 	   VARCHAR2,
  p_out_clob OUT  CLOB
 ) 
 AS
 	  v_json 	     json_object_t;
 	  v_regnid	     VARCHAR2(20);
 	  v_attendee_fl	 VARCHAR2(1);
BEGIN	 
	  v_json          := json_object_t.parse(p_input);
	  v_regnid        := v_json.get_String('regnid');
	  v_attendee_fl   := v_json.get_String('attendeefl');
	  IF (v_attendee_fl IS NULL) THEN
             SELECT JSON_ARRAYAGG(JSON_OBJECT(
	   				'id' VALUE T6670.C6670_MERC_ID,
  				  	'name' VALUE T6670.C6670_MERC_NM,
  				  	'mtype' VALUE get_code_name(T6670.C901_TYPE),
                    'mtypeid' VALUE T6670.C901_TYPE,
                    'mstatus' VALUE get_code_name(T6670.C901_STATUS),
                    'mstatusid' VALUE T6670.C901_STATUS,
                    'fromdt' VALUE TO_CHAR(T6670.C6670_FROM_DT, 'MM/DD/YYYY'),
                    'todt' VALUE TO_CHAR(T6670.C6670_TO_DT, 'MM/DD/YYYY'),
                    'city' VALUE T6670.C6670_CITY,
                    'state' VALUE T6670.C901_STATE,
                    'lang' VALUE get_code_name(T6670.C901_LANGUAGE),
                    'opento' VALUE T6670.C6670_OPENTO,
                    'statenm' VALUE NVL2(T6670.C901_STATE,get_code_name_alt(T6670.C901_STATE),get_code_name(T6670.C901_COUNTRY)),
                    'regnid' VALUE T6670.C901_REGION,
                    'link' VALUE T6670.C6670_LINK
  			   ) ORDER BY  T6670.C6670_FROM_DT RETURNING CLOB) INTO p_out_clob
  		   FROM T6670_MERC T6670 
  		  WHERE T6670.C901_REGION = v_regnid 
  		  	AND T6670.C6670_FROM_DT > TRUNC(CURRENT_DATE)  		    
  		    AND C6670_VOID_FL is null ;
  	  ELSE
	  		 SELECT JSON_ARRAYAGG(JSON_OBJECT(
		   				'id' VALUE T6670.C6670_MERC_ID,
	  				  	'name' VALUE T6670.C6670_MERC_NM,
	  				  	'mtype' VALUE get_code_name(T6670.C901_TYPE),
	                    'mtypeid' VALUE T6670.C901_TYPE,
	                    'mstatus' VALUE get_code_name(T6670.C901_STATUS),
	                    'mstatusid' VALUE T6670.C901_STATUS,
	                    'fromdt' VALUE TO_CHAR(T6670.C6670_FROM_DT, 'MM/DD/YYYY'),
	                    'todt' VALUE TO_CHAR(T6670.C6670_TO_DT, 'MM/DD/YYYY'),
	                    'city' VALUE T6670.C6670_CITY,
	                    'state' VALUE T6670.C901_STATE,
	                    'lang' VALUE get_code_name(T6670.C901_LANGUAGE),
	                    'opento' VALUE T6670.C6670_OPENTO,
	                    'statenm' VALUE NVL2(T6670.C901_STATE,get_code_name_alt(T6670.C901_STATE),get_code_name(T6670.C901_COUNTRY)),
	                    'regnid' VALUE T6670.C901_REGION,
	                    'link' VALUE T6670.C6670_LINK
	  			   ) ORDER BY  T6670.C6670_FROM_DT RETURNING CLOB) INTO p_out_clob
	  		   FROM T6670_MERC T6670 
	  		  WHERE T6670.C901_REGION = v_regnid 
	  		  	AND T6670.C6670_ATTENDEE_FL = 'Y'  		    
	  		    AND C6670_VOID_FL is null ;
  		 END IF;
	 	
	END gm_fch_merc;

/****************************************************************
* Description : Fetch the MERC Attendee Report
* Author      : MahendranD
*****************************************************************/
PROCEDURE  gm_fch_merc_attendee_list (
  p_mercid		IN 	   t6670_merc.c6670_merc_id%TYPE,
  p_out_clob    OUT    CLOB
 ) 
AS  
BEGIN	
			SELECT JSON_ARRAYAGG(JSON_OBJECT(
				   'partyid'        VALUE t101.c101_party_id,
				   'partynm'        VALUE t101.c101_party_nm,
				   'firstnm'        VALUE t101.c101_first_nm,
				   'lastnm'         VALUE t101.c101_last_nm,
				   'midinitial'     VALUE t101.c101_middle_initial,
				   'roleid'         VALUE t6671.c901_role,
				   'rolenm'         VALUE get_code_name(t6671.c901_role),
				   'statusid'       VALUE t6671.c901_status,
				   'statusnm'       VALUE get_code_name(t6671.c901_status),
				   'surgeonstausid' VALUE t1081.c901_status,
				   'surgeonstausnm' VALUE get_code_name(t1081.c901_status)
			) ORDER BY get_code_name(t6671.c901_role), t101.c101_first_nm, t101.c101_last_nm RETURNING CLOB) INTO p_out_clob
		 FROM t6670_merc t6670,
			  t6671_merc_attendee_list t6671,
			  t101_party t101, 
			  t1081_party_status t1081
		WHERE t6670.c6670_merc_id          = t6671.c6670_merc_id
		  AND t6670.c6670_void_fl          IS NULL
		  AND t6671.c6671_void_fl          IS NULL
		  AND t6670.c6670_merc_id          =  p_mercid
		  AND T6671.C101_SURGEON_PARTY_ID  = T101.C101_PARTY_ID
		  AND T6671.C101_SURGEON_PARTY_ID  = T1081.C101_PARTY_ID(+)
	      AND T1081.C1081_VOID_FL          IS NULL
		  AND T101.C101_VOID_FL            IS NULL;
	END gm_fch_merc_attendee_list;

	
END gm_pkg_crm_merc;
/