CREATE OR REPLACE PACKAGE BODY gm_pkg_crm_account_rpt
IS

/****************************************************************
* Description : Fetch the Account Surgeon Report
*****************************************************************/
PROCEDURE gm_fch_account_surgeon_rpt  (
		p_accid				IN T1014_PARTY_AFFILIATION.c704_account_id%TYPE,
 		p_out_str  			OUT CLOB
 )	
 AS	
BEGIN

SELECT JSON_ARRAYAGG(
	   JSON_OBJECT( 
		   'partyid' 	value SURGRPT.PARTYID, 
		   'firstnm' 	value SURGRPT.FIRSTNM, 
		   'lastnm' 	value SURGRPT.LASTNM, 
		   'partynm' 	value SURGRPT.FIRSTNM || ' ' ||SURGRPT.LASTNM,
		   'midinitial' value SURGRPT.MIDINITIAL, 
		   'status' 	value SURGRPT.STATUS
   		   )ORDER BY SURGRPT.FIRSTNM, SURGRPT.LASTNM  RETURNING CLOB)
   	   INTO p_out_str 
	   FROM (SELECT DISTINCT T101.C101_PARTY_ID PARTYID,
			 T101.C101_FIRST_NM FIRSTNM,
			 T101.C101_LAST_NM LASTNM,
			 T101.C101_PARTY_NM PARTYNM,
			 T101.C101_MIDDLE_INITIAL MIDINITIAL,
			 T1081.C901_STATUS STATUS
	   FROM  T1014_PARTY_AFFILIATION t1014,t101_party t101,T1081_PARTY_STATUS t1081
	   WHERE t1014.c704_account_id IN  (SELECT c704_account_id FROM t704_account WHERE C101_PARTY_ID =  p_accid)
			 AND t1014.c101_party_id  = t101.c101_party_id
			 AND t1014.c101_party_id  = T1081.C101_PARTY_ID(+)
			 AND t1081.C1081_VOID_FL IS NULL
			 AND t101.c101_void_fl   IS NULL
			 AND t1014.c1014_void_fl IS NULL  
			 GROUP BY  T101.C101_PARTY_ID,
			 T101.C101_FIRST_NM,
			 T101.C101_LAST_NM ,
			 T101.C101_PARTY_NM ,
			 T101.C101_MIDDLE_INITIAL ,
 			 T1081.C901_STATUS
 			 ) SURGRPT;
	   

END gm_fch_account_surgeon_rpt;

/****************************************************************
* Description : This method used to fetch the surgical activity by revenue
*****************************************************************/
PROCEDURE gm_fch_surgical_by_rev(
    p_accpartyid  		IN 	T101_PARTY.C101_PARTY_ID%TYPE,
    p_surgeonpartyid	IN 	T101_PARTY.C101_PARTY_ID%TYPE,
    p_fromdate    		IN 	T101_PARTY.C101_PARTY_ID%TYPE,
    p_todate      		IN 	T101_PARTY.C101_PARTY_ID%TYPE,
    p_surgicalrev_cur 	OUT CLOB )
AS	
BEGIN
SELECT JSON_ARRAYAGG(
	   JSON_OBJECT( 
		   'yearmon' 	value  TO_CHAR(T501.C501_ORDER_DATE ,'YYYYMM'), 
		   'value' 		value  SUM(T502.C502_ITEM_PRICE * T502.C502_ITEM_QTY), 
		   'grpid' 		value  V_SYS.SEGMENT_ID, 
		   'grpnm' 		value  V_SYS.SEGMENT_NAME, 
		   'grpid2' 	value  V_SYS.SYSTEM_ID,
		   'grpnm2' 	value  V_SYS.SYSTEM_NM
   		   ) RETURNING CLOB)
   	   INTO p_surgicalrev_cur
 	   FROM T501_ORDER T501,T502_ITEM_ORDER T502,T208_SET_DETAILS T208,v_segment_system_map V_SYS
 	   WHERE T501.C704_ACCOUNT_ID IN
		  ( SELECT c704_account_id FROM t704_account WHERE C101_PARTY_ID =  p_accpartyid)
		AND T501.C501_ORDER_ID                = T502.C501_ORDER_ID
		AND T501.C501_STATUS_FL               = 3
		AND V_SYS.SYSTEM_ID                   = T208.C207_SET_ID
		AND T502.C205_PART_NUMBER_ID          = T208.C205_PART_NUMBER_ID
		AND T501.C501_VOID_FL                IS NULL
		AND T502.C502_VOID_FL                IS NULL
		AND T208.C208_VOID_FL                IS NULL
		AND T501.C501_DELETE_FL              IS NULL
		AND NVL (C901_ORDER_TYPE, -9999) NOT IN
		  (SELECT C906_RULE_VALUE
		  FROM V901_ORDER_TYPE_GRP
		  )
		AND T501.C501_ORDER_DATE >= TO_DATE(p_fromdate,'YYYYMM')
		AND ( p_surgeonpartyid   IS NULL
		OR T501.C501_ORDER_ID    IN
		  (SELECT c6640_ref_id
		  FROM t6640_npi_transaction
		  WHERE c101_party_surgeon_id = p_surgeonpartyid
		  AND c6640_void_fl          IS NULL
		  AND c6640_valid_fl          ='Y'
		  ))
		AND T501.C501_ORDER_DATE <= LAST_DAY(TO_DATE(p_todate,'YYYYMM'))
		GROUP BY TO_CHAR(T501.C501_ORDER_DATE ,'YYYYMM'),
		  V_SYS.SEGMENT_ID ,
		  V_SYS.SEGMENT_NAME ,
		  V_SYS.SYSTEM_ID ,
		  V_SYS.SYSTEM_NM;
END gm_fch_surgical_by_rev;


/****************************************************************
* Description : This method used to fetch the surgical activity by procedure
*****************************************************************/
PROCEDURE gm_fch_surgical_by_prc(
    p_accpartyid  		IN 	T101_PARTY.C101_PARTY_ID%TYPE,
    p_surgeonpartyid	IN 	T101_PARTY.C101_PARTY_ID%TYPE,
    p_fromdate    		IN 	T101_PARTY.C101_PARTY_ID%TYPE,
    p_todate      		IN 	T101_PARTY.C101_PARTY_ID%TYPE,
    p_surgicalprc_cur 	OUT CLOB )
AS	
BEGIN
SELECT JSON_ARRAYAGG(
	     JSON_OBJECT( 
		   'yearmon' 	value  TO_CHAR(T501.C501_ORDER_DATE,'YYYYMM'), 
		   'value' 		value  COUNT(T501.C501_ORDER_ID), 
		   'grpid' 		value  V_SYS.SEGMENT_ID, 
		   'grpnm' 		value  V_SYS.SEGMENT_NAME, 
		   'grpid2' 	value  V_SYS.SYSTEM_ID,
		   'grpnm2' 	value  V_SYS.SYSTEM_NM
   		   ) RETURNING CLOB)
        INTO p_surgicalprc_cur
       FROM T501_ORDER T501,T501B_ORDER_BY_SYSTEM T501B,v_segment_system_map V_SYS
       WHERE t501.C704_ACCOUNT_ID IN(SELECT c704_account_id FROM t704_account  WHERE C101_PARTY_ID = p_accpartyid)
			AND ( p_surgeonpartyid IS NULL
			OR T501.C501_ORDER_ID  IN
			  (SELECT c6640_ref_id
			  FROM t6640_npi_transaction
			  WHERE c101_party_surgeon_id = p_surgeonpartyid
			  AND c6640_void_fl          IS NULL
			  AND c6640_valid_fl          ='Y'
			  ))
			AND T501.C501_ORDER_ID                = T501B.C501_ORDER_ID
			AND T501B.C207_SYSTEM_ID              = V_SYS.SYSTEM_ID
			AND T501.C501_PARENT_ORDER_ID        IS NULL
			AND T501.C501_VOID_FL                IS NULL
			AND T501.C501_DELETE_FL              IS NULL
			AND NVL (C901_ORDER_TYPE, -9999) NOT IN
			  (SELECT C906_RULE_VALUE FROM V901_ORDER_TYPE_GRP
			  )
			AND T501B.C501B_VOID_FL  IS NULL
			AND T501.C501_ORDER_DATE >= TO_DATE(p_fromdate,'YYYYMM')
			AND T501.C501_ORDER_DATE <= LAST_DAY(TO_DATE(p_todate,'YYYYMM'))
			GROUP BY TO_CHAR(T501.C501_ORDER_DATE,'YYYYMM'),
			  V_SYS.SEGMENT_ID,
			  V_SYS.SEGMENT_NAME,
			  V_SYS.SYSTEM_ID,
			  V_SYS.SYSTEM_NM;
END gm_fch_surgical_by_prc;


/****************************************************************
* Description : This method used to fetch the surgical activity system DO List
*****************************************************************/
PROCEDURE gm_fch_surgical_sys_do(
    p_accpartyid  		IN 	T101_PARTY.C101_PARTY_ID%TYPE,
    p_surgeonpartyid	IN 	T101_PARTY.C101_PARTY_ID%TYPE,
    p_fromdate    		IN 	T101_PARTY.C101_PARTY_ID%TYPE,
    p_todate      		IN 	T101_PARTY.C101_PARTY_ID%TYPE,
    p_sysid      		IN 	T208_SET_DETAILS.C207_SET_ID%TYPE,
    p_surgicalsys_cur 	OUT CLOB )
AS	
BEGIN
SELECT JSON_ARRAYAGG(
	   JSON_OBJECT( 
		   'orderid' 	value T.ORDERID, 
		   'orderdt' 	value T.ORDERDT, 
		   'partnum' 	value T.PARTNUM, 
		   'pdesc' 	  	value T205.C205_PART_NUM_DESC,
		   'qty'      	value T.QUANTITY, 
		   'price' 	  	value T.PRICE, 
		   'total'  	value (T.QUANTITY * T.PRICE),
       	   'acctid'   	value T.ACCTID
   		   )ORDER BY T.ORDERDT,T.ORDERDT,T205.C205_PART_NUM_DESC,T.QUANTITY  RETURNING CLOB)
   	   INTO p_surgicalsys_cur
 	   FROM
		  (SELECT T501.C501_ORDER_ID ORDERID,
		    TO_CHAR(T501.C501_ORDER_DATE,'MM/DD/YYYY') ORDERDT,
		    T501.C501_ORDER_DATE,
		    T501.C704_ACCOUNT_ID ACCTID,
		    T501.C703_SALES_REP_ID REPID,
		    T502.C205_PART_NUMBER_ID PARTNUM,
		    SUM(T502.C502_ITEM_QTY) QUANTITY,
		    T502.C502_ITEM_PRICE PRICE
		  FROM T501_ORDER T501,
	    	   T502_ITEM_ORDER T502,
		       T208_SET_DETAILS T208
		  WHERE T501.C704_ACCOUNT_ID IN(SELECT c704_account_id FROM t704_account WHERE C101_PARTY_ID = p_accpartyid)
		  AND ( p_surgeonpartyid IS NULL
		  OR T501.C501_ORDER_ID  IN
		    (SELECT c6640_ref_id
		    FROM t6640_npi_transaction
		    WHERE c101_party_surgeon_id = p_surgeonpartyid
		    AND c6640_void_fl          IS NULL
		    AND c6640_valid_fl          ='Y'
		    ))
		  AND T501.C501_ORDER_ID                = T502.C501_ORDER_ID
		  AND T502.C205_PART_NUMBER_ID          = T208.C205_PART_NUMBER_ID
		  AND T502.C502_VOID_FL                IS NULL
		  AND T208.C208_VOID_FL                IS NULL
		  AND T501.C501_VOID_FL                IS NULL
		  AND T501.C501_DELETE_FL              IS NULL
		  AND NVL (C901_ORDER_TYPE, -9999) NOT IN
		    (SELECT C906_RULE_VALUE FROM V901_ORDER_TYPE_GRP
		    )
		 AND T501.C501_ORDER_DATE >= TO_DATE(p_fromdate,'YYYYMM')
		  AND T501.C501_ORDER_DATE <= LAST_DAY(TO_DATE(p_todate,'YYYYMM'))
		  AND T208.C207_SET_ID     IN (p_sysid)
		  GROUP BY T501.C501_ORDER_ID,
		    T501.C501_ORDER_DATE,
		    T501.C704_ACCOUNT_ID,
		    T501.C703_SALES_REP_ID,
		    T502.C205_PART_NUMBER_ID,
		    T502.C502_ITEM_PRICE
		  ) T,
		  T205_PART_NUMBER T205
		WHERE T.PARTNUM = T205.C205_PART_NUMBER_ID;
END gm_fch_surgical_sys_do;

/****************************************************************
* Description : This method used to fetch the Account Surgical Calendar info
*****************************************************************/
PROCEDURE gm_fch_surgical_calendar_info(
   		p_partyid  			IN 	T101_PARTY.C101_PARTY_ID%TYPE,
	    p_fromdate    		IN 	T101_PARTY.C101_PARTY_ID%TYPE,
	    p_todate      		IN 	T101_PARTY.C101_PARTY_ID%TYPE,
	    p_actsurgcal_cur 	OUT CLOB )
AS	
BEGIN
SELECT JSON_ARRAYAGG( 
	   JSON_OBJECT( 
	   		'surgdt' VALUE TO_CHAR(T.SURGDT,'YYYY-MM-DD'), 
	   		'segid' VALUE T.SEGID, 
	   		'segnm' VALUE T.SEGNM, 
	   		'sysid' VALUE T.SYSID, 
	   		'sysnm' VALUE T.SYSNM, 
	   		'cnt' VALUE T.CNT ) 
	   		RETURNING CLOB) INTO p_actsurgcal_cur
	   FROM(
	   		SELECT NVL(T501.C501_SURGERY_DATE, T501.C501_ORDER_DATE) SURGDT, 
                   V_SYS.SEGMENT_ID                                  SEGID, 
                   V_SYS.SEGMENT_NAME                                SEGNM, 
                   V_SYS.SYSTEM_ID                                   SYSID, 
                   V_SYS.SYSTEM_NM                                   SYSNM, 
                   COUNT(1)                                          CNT 
            FROM   T501_ORDER T501, 
                   T501B_ORDER_BY_SYSTEM T501B, 
                   V_SEGMENT_SYSTEM_MAP V_SYS 
            WHERE  T501.C704_ACCOUNT_ID IN (SELECT C704_ACCOUNT_ID FROM   T704_ACCOUNT WHERE  C101_PARTY_ID = p_partyid) 
				   AND NVL(T501.C501_SURGERY_DATE, T501.C501_ORDER_DATE) >= TO_DATE(p_fromdate, 'YYYYMM')
            	   AND NVL(T501.C501_SURGERY_DATE, T501.C501_ORDER_DATE) <= LAST_DAY( TO_DATE(p_todate, 'YYYYMM'))
                   AND T501.C501_ORDER_ID = T501B.C501_ORDER_ID 
                   AND T501B.C207_SYSTEM_ID = V_SYS.SYSTEM_ID 
                   AND NVL (C901_ORDER_TYPE, -9999) NOT IN (SELECT C906_RULE_VALUE FROM   V901_ORDER_TYPE_GRP) 
            	   AND T501.C501_PARENT_ORDER_ID IS NULL 
                   AND T501.C501_VOID_FL IS NULL 
                   AND T501.C501_DELETE_FL IS NULL 
                   AND T501B.C501B_VOID_FL IS NULL 
 			GROUP BY NVL(T501.C501_SURGERY_DATE, T501.C501_ORDER_DATE), 
                         V_SYS.SEGMENT_ID, 
                         V_SYS.SEGMENT_NAME, 
                         V_SYS.SYSTEM_ID, 
                         V_SYS.SYSTEM_NM 
            UNION ALL 
            SELECT NVL(T501.C501_SURGERY_DATE,T501.C501_ORDER_DATE) SURGDT, 
                         0                                          SEGID, 
                         'ALL DO'                                   SEGNM, 
                         '0'                                        SYSID, 
                         'ALL DO'                                   SYSNM, 
                         COUNT(1)                                   CNT 
            FROM   T501_ORDER T501 
            WHERE  T501.C704_ACCOUNT_ID IN (SELECT C704_ACCOUNT_ID FROM   T704_ACCOUNT WHERE  C101_PARTY_ID = p_partyid ) 
                   AND NVL(T501.C501_SURGERY_DATE,T501.C501_ORDER_DATE) >= TO_DATE(p_fromdate,'YYYYMM')
                   AND NVL(T501.C501_SURGERY_DATE,T501.C501_ORDER_DATE) <= LAST_DAY(TO_DATE(p_todate,'YYYYMM'))
                   AND NVL (C901_ORDER_TYPE, -9999) NOT IN ( SELECT C906_RULE_VALUE FROM   V901_ORDER_TYPE_GRP ) 
                   AND T501.C501_PARENT_ORDER_ID IS NULL 
                   AND T501.C501_VOID_FL IS NULL 
                   AND T501.C501_DELETE_FL IS NULL 
            GROUP BY NVL(T501.C501_SURGERY_DATE,T501.C501_ORDER_DATE)) T;
END gm_fch_surgical_calendar_info;

	
END gm_pkg_crm_account_rpt;
/