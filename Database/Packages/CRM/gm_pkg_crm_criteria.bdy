CREATE OR REPLACE
		PACKAGE BODY gm_pkg_crm_criteria
			IS
   	   /****************************************************************
    	* Description : Procedure to save Criteria
    	* Author      : praja
    	*****************************************************************/
		PROCEDURE gm_sav_crm_criteria ( 
			p_criteriaid		IN OUT     t1101_criteria.c1101_criteria_id%TYPE,
	    	p_criterianm		IN         t1101_criteria.c1101_criteria_name%TYPE,
	    	p_criteriatyp		IN         t1101_criteria.c2201_module_id%TYPE,
	    	p_userid			IN         t1101_criteria.c101_user_id%TYPE,
	    	p_voidfl			IN         t1101_criteria.c1101_void_fl%TYPE
	    ) AS
	    BEGIN
		    UPDATE t1101_criteria
		    SET C1101_CRITERIA_NAME = p_criterianm
		    , C2201_MODULE_ID = p_criteriatyp
		    , C101_USER_ID = p_userid
		    , C1101_VOID_FL = p_voidfl
		    , C1101_LAST_UPDATED_BY = p_userid
		    , C1101_LAST_UPDATED_DATE = SYSDATE
		    WHERE C1101_CRITERIA_ID = p_criteriaid;
		    
		     IF (SQL%ROWCOUNT = 0)
		         THEN
		    
				 SELECT 'CR'||to_char(S1101_CRITERIA.nextval,'FM00000') INTO p_criteriaid FROM DUAL;
				 INSERT INTO t1101_criteria 
				 		(C1101_CRITERIA_ID, C1101_CRITERIA_NAME, C2201_MODULE_ID, C101_USER_ID, C1101_VOID_FL ,
				 		C1101_CREATED_BY,C1101_CREATED_DATE)
				 VALUES (p_criteriaid,p_criterianm,p_criteriatyp,p_userid,p_voidfl,p_userid,SYSDATE);
				 
			 END IF;
			 
                   
		 END gm_sav_crm_criteria;
		 
		 PROCEDURE gm_sav_crm_criteria_attr (     
			p_attrid			IN  OUT    t1101a_criteria_attribute.C1101A_CRITERIA_ATTRIBUTE_ID%TYPE, 
	    	p_key				IN         t1101a_criteria_attribute.c1101a_criteria_key%TYPE,
	    	p_value				IN         t1101a_criteria_attribute.c1101a_criteria_value%TYPE,
	    	p_voidfl			IN         t1101a_criteria_attribute.c1101a_void_fl%TYPE,
	    	p_criteriaid		IN		   t1101a_criteria_attribute.c1101_criteria_id%TYPE
	     ) AS
	     
	     BEGIN
		     UPDATE t1101a_criteria_attribute
		    SET C1101_CRITERIA_ID = p_criteriaid
		    , C1101A_CRITERIA_KEY = p_key
		    , C1101A_CRITERIA_VALUE = p_value
		    , C1101A_VOID_FL = p_voidfl
		    WHERE C1101A_CRITERIA_ATTRIBUTE_ID = p_attrid;
		    
		     IF (SQL%ROWCOUNT = 0)
		         THEN
			     	SELECT S1101A.nextval INTO p_attrid FROM DUAL;
		    
				 INSERT INTO t1101a_criteria_attribute 
				 		(C1101_CRITERIA_ID, C1101A_CRITERIA_KEY, C1101A_CRITERIA_VALUE, C1101A_VOID_FL, C1101A_CRITERIA_ATTRIBUTE_ID)
				 VALUES (p_criteriaid,p_key,p_value,p_voidfl,p_attrid);
				 
			 END IF;
			 
		END gm_sav_crm_criteria_attr;
		
		PROCEDURE gm_delete_crm_criteria_attr (     
			p_criteriaid		IN		   t1101a_criteria_attribute.c1101_criteria_id%TYPE
	     ) AS
	     
	     BEGIN
		     
		     DELETE FROM t1101a_criteria_attribute WHERE C1101_CRITERIA_ID = p_criteriaid;

		END gm_delete_crm_criteria_attr;
	     
	END gm_pkg_crm_criteria;
    /	 