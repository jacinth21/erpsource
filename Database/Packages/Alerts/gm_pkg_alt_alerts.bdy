--@"C:\database\Packages\it\GM_PKG_ALT_ALERTS.bdy";

CREATE OR REPLACE PACKAGE BODY GM_PKG_ALT_ALERTS IS
/*******************************************************************************
	Description : This procedure is used to fetch all alerts records for  Read - Unread.
********************************************************************************/
PROCEDURE gm_fch_alerts_rpt(
	p_type 		IN t901_code_lookup.C901_CODE_ID%TYPE,
	p_reason	IN t901_code_lookup.C901_CODE_ID%TYPE,
	p_actionable	IN t901_code_lookup.C901_CODE_ID%TYPE,
	p_fromdt 		IN VARCHAR2,
	p_todt 			IN VARCHAR2,
	p_userid 		IN t101_USER.c101_USER_ID%TYPE,
    p_alert_out	 	OUT Types.cursor_type
)
AS

BEGIN

	OPEN  p_alert_out 
	FOR
	    SELECT t9021.C9021_ALERT_ID ALERT_ID,T9022.C9022_ALERT_MAPPING_ID MAPID, C901_PRIORITY PRIORITY, get_code_name (C901_TYPE) ATYPE, 
        get_code_name (C901_REASON) REASON, C9021_ALERT_DATE ALERT_DATE, C9021_SUBJECT SUBJECT, 
        C9021_MESSAGE MESSAGE, get_code_name(t9022.C901_ACTIONABLE) ACTIONABLE, C9021_CREATED_BY CREATED_BY, 
        C9021_CREATED_DATE CREATED_DATE 
       	FROM T9021_alert t9021, t9022_alert_mapping t9022 
    	WHERE t9021.c9021_alert_id   = t9022.c9021_alert_id 
        AND t9021.C901_TYPE        = DECODE(p_type,0,t9021.C901_TYPE,p_type) 
        AND t9021.C901_REASON      = DECODE(p_reason,0,t9021.C901_REASON,p_reason)
        AND t9022.C901_ACTIONABLE  = DECODE(p_actionable,0,t9022.C901_ACTIONABLE,p_actionable)
   		AND t9022.c101_user_id     = p_userid 
        AND TRUNC(t9021.C9021_ALERT_DATE) >= DECODE(p_fromdt,'0',TRUNC(t9021.C9021_ALERT_DATE),TO_DATE(p_fromdt,get_rule_value('DATEFMT', 'DATEFORMAT')))
        AND TRUNC(t9021.C9021_ALERT_DATE) <= DECODE(p_todt,'0',TRUNC(t9021.C9021_ALERT_DATE),TO_DATE(p_todt, get_rule_value('DATEFMT', 'DATEFORMAT')))
        AND t9021.c9021_void_fl IS NULL 
        AND t9022.c9022_void_fl IS NULL
   ORDER BY t9022.C901_ACTIONABLE; 
END gm_fch_alerts_rpt;

/***************************************************************************************
	Description : This procedure is to fetch the details corresponding to an alert
****************************************************************************************/

 PROCEDURE gm_fch_alerts_detail(
	p_alert_id		IN T9021_alert.C9021_ALERT_ID%TYPE,
	p_userid 		IN t101_USER.c101_USER_ID%TYPE,
    p_alert_out	 	OUT Types.cursor_type
)
  AS

 BEGIN

	OPEN  p_alert_out 
	FOR

     SELECT t9021.C9021_ALERT_ID ALERT_ID, t9021.C901_PRIORITY PRIORITY, get_code_name (t9021.C901_TYPE) ATYPE, 
        get_code_name (t9021.C901_REASON) REASON, to_char(t9021.C9021_ALERT_DATE,get_rule_value('DATEFMT', 'DATEFORMAT')) ALERT_DATE, t9021.C9021_SUBJECT SUBJECT, 
        t9021.C9021_MESSAGE MESSAGE, t9022.C901_ACTIONABLE ACTIONABLE, t9021.C9021_CREATED_BY CREATED_BY, 
        t9021.C9021_CREATED_DATE CREATED_DATE 
       FROM T9021_alert t9021, t9022_alert_mapping t9022 
      WHERE t9021.C9021_ALERT_ID = p_alert_id
        AND t9021.c9021_alert_id = t9022.c9021_alert_id 
        AND t9022.c101_user_id   = p_userid
        AND t9021.c9021_void_fl IS NULL 
        AND t9022.c9022_void_fl IS NULL; 

END gm_fch_alerts_detail;

/*********************************************************************************************
	Description : This procedure will update alert Read - Unread based on select choose action.
**********************************************************************************************/

PROCEDURE gm_upd_alerts(
	p_alert_ids		IN VARCHAR2,
	p_userid 		IN t101_USER.c101_USER_ID%TYPE,
	p_actionable	IN t901_code_lookup.C901_CODE_ID%TYPE
)
  AS
	v_string VARCHAR(4000) := p_alert_ids ||',';
	v_alert_id VARCHAR2(4000);
 BEGIN
	
	WHILE INSTR (v_string, ',') <> 0
		LOOP
			v_alert_id := NULL;
	        v_alert_id := SUBSTR (v_string, 1, INSTR (v_string, ',') - 1);
            v_string := SUBSTR (v_string, INSTR (v_string, ',') + 1);
		
            UPDATE t9022_alert_mapping t9022 
				SET t9022.C901_ACTIONABLE = DECODE(p_actionable,102546,102545,102547,102544), t9022.c9022_LAST_UPDATED_DATE = SYSDATE,
				t9022.c9022_LAST_UPDATED_BY =   p_userid  -- 102546:Flag as Read, 102545:No, 102547:Flag as Unread,102544:Yes
  			WHERE t9022.C9021_ALERT_ID = v_alert_id 
    		AND t9022.c101_user_id = p_userid; 
    END LOOP;
    
END gm_upd_alerts;

/****************************************************************************************
	Description : This function will get Unread alert count 
*****************************************************************************************/

FUNCTION get_unread_alerts_cnt(
	p_userid 		IN t101_USER.c101_USER_ID%TYPE
)
RETURN NUMBER
IS
	v_count NUMBER;
BEGIN
	BEGIN
		SELECT COUNT (1) INTO v_count FROM t9022_alert_mapping
		WHERE c101_user_id = p_userid
		AND  C901_ACTIONABLE = 102544 AND c9022_void_fl IS NULL;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN 0;
	END;
	RETURN v_count;
END get_unread_alerts_cnt;

 /**************************************************************************
	Description : This procedure is to insert records of alerts in alerts and alert mapping table  
***************************************************************************/

PROCEDURE gm_sav_alerts(
	p_priority 		IN T9021_ALERT.C901_PRIORITY%TYPE,
	p_type 			IN T9021_ALERT.C901_TYPE%TYPE,
	p_reason 		IN T9021_ALERT.C901_REASON%TYPE,
	p_alertdt 		IN T9021_ALERT.C9021_ALERT_DATE%TYPE,
	p_subject 		IN T9021_ALERT.C9021_SUBJECT%TYPE,
	p_message 		IN T9021_ALERT.C9021_MESSAGE%TYPE,
	p_actionable 	IN T9022_ALERT_MAPPING.C901_ACTIONABLE%TYPE,
	p_userid 		IN T101_USER.c101_USER_ID%TYPE,
	p_users_str		IN VARCHAR2
)

  AS
	v_alert_id	  T9021_ALERT.C9021_ALERT_ID%TYPE;
	v_users_str   VARCHAR2(4000) := p_users_str;
	v_userid      T101_USER.c101_USER_ID%TYPE;
 BEGIN
	 
	 SELECT s9021_ALERT.nextval INTO v_alert_id FROM dual;
	  
 	INSERT INTO t9021_ALERT( 
        C9021_ALERT_ID, C901_PRIORITY, C901_TYPE, 
        C901_REASON, C9021_ALERT_DATE, C9021_SUBJECT, 
        C9021_MESSAGE, C9021_CREATED_BY,C9021_CREATED_DATE 
    )
    VALUES( 
        v_alert_id, p_priority, p_type, 
        p_reason, NVL (p_alertdt, SYSDATE), p_subject, 
        p_message, p_userid,SYSDATE 
    ) ; 

	WHILE INSTR (v_users_str, ',') <> 0
		LOOP
			v_userid := NULL;
			v_userid	:= SUBSTR (v_users_str, 1, INSTR (v_users_str, ',') - 1);
			v_users_str := SUBSTR (v_users_str, INSTR (v_users_str, ',') + 1);	
 		
			INSERT  INTO t9022_alert_mapping(
 				C9022_Alert_Mapping_ID, C9021_ALERT_ID, C101_USER_ID,
 				C901_ACTIONABLE,C9022_CREATED_BY, C9022_CREATED_DATE 
  			)
  			VALUES(
   				s9022_alert_mapping.nextval, v_alert_id, v_userid,
   				p_actionable, p_userid, SYSDATE
  			);
  	END LOOP;
END gm_sav_alerts;

END GM_PKG_ALT_ALERTS;
/