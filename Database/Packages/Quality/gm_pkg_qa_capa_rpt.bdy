
--@"C:\Database\Packages\Quality\gm_pkg_qa_capa_rpt.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_qa_capa_rpt
IS
/**********************************************************
	 * Description : Procedure to fetch CAPA Information
	 * Author	   :
**********************************************************/
    PROCEDURE gm_fch_capa_info (
    		  p_capa_id		      IN   T3150_CAPA.C3150_CAPA_ID%TYPE
    		 ,p_out_capa          OUT   TYPES.cursor_type
    		 ,p_out_capa_assignee OUT   TYPES.cursor_type
    )
	AS
	 	v_out_type varchar(20);
	BEGIN
		--Validating CAPA Id when clicking load
		gm_pkg_qa_com_rsr_process.gm_validate_id(UPPER(p_capa_id),'102788',v_out_type);
		
			OPEN p_out_capa
			FOR
			SELECT C3150_CAPA_ID strcapaid
				   ,C901_TYPE strmfgtype
				   ,C101_REQUESTED_BY strrequestedby
				   ,C101_ISSUED_BY strissuedby
				   ,get_party_name(C101_ISSUED_BY) strissuedbynm
				   ,C3150_NCMR_COM_REF strcomplaint
				   ,C3150_PROJECT_REF strprojectid, C901_STATUS status
				   ,GET_CODE_NAME(C901_STATUS) statusname,C3150_CAPA_REF strrefid
				   ,C3150_LOT_NUMBER STRLOTNUMS, C3150_CATAGORY strcategory
				   ,C3150_CAPA_DESC strcapadesc, C3150_PREVIOUS_CAPA strprevcapas
				   ,C901_PRIORITY_LEVEL strprtylvl
				   ,to_char(C3150_SUBMITTED_DT,get_rule_value('DATEFMT','DATEFORMAT')) strcapasubdt
			       ,to_char(C3150_RESPONSE_DT,get_rule_value('DATEFMT','DATEFORMAT')) strrespduedt
			       ,to_char(C3150_CLOSE_DT,get_rule_value('DATEFMT','DATEFORMAT')) strcloseddt
			       ,C3150_RESPONSE strcaparesp
			       ,GET_CODE_NAME(C901_TYPE) strmfgtypedesc
			       ,DECODE(C901_Status,'102770',GET_WEEKDAYSDIFF(Trunc(C3150_SUBMITTED_DT),Trunc(C3150_Last_Updated_Date)) ,'102771',GET_WEEKDAYSDIFF(Trunc(C3150_SUBMITTED_DT),Trunc(C3150_CLOSE_DT)),NULL) DAYSTOCLOSE--(changes made to get the working days from the calendar)
				   FROM T3150_CAPA 
				   WHERE UPPER (C3150_CAPA_ID) = UPPER(P_CAPA_ID) AND C3150_VOID_FL IS NULL;
			
		OPEN p_out_capa_assignee 
		FOR
			SELECT c3150_capa_id strCapaId,c101_assigned_to ID, get_user_name(c101_assigned_to) NAME 
			FROM t3151_capa_assignee 
			WHERE c3150_capa_id = p_capa_id; 
			
	END gm_fch_capa_info;

	/**********************************************************
	 * Description : Procedure to fetch CAPA Evalutor Info
	 * Author	   : Jignesh Shah
	 **********************************************************/

    PROCEDURE gm_fch_capa_eval_info (
    	p_out_eval_info		OUT   TYPES.cursor_type
    )
	AS
	BEGIN
		OPEN p_out_eval_info
		FOR
			SELECT	'20' day, t3150.c3150_capa_id capaid
					, t3150.c3150_lot_number capalotnum
					, t3150.c3150_capa_ref caparef
					, t3150.c3150_capa_desc capadesc
					, to_char(t3150.c3150_response_dt,get_rule_value('DATEFMT','DATEFORMAT')) caparesdate
					, get_user_name(t3150.c101_issued_by) capaissuedby
					, get_user_emailid(t3151.c101_assigned_to) assignedto 
				FROM t3150_capa t3150,t3151_capa_assignee t3151
				WHERE t3150.c3150_capa_id = t3151.c3150_capa_id 
				AND t3150.c901_status = 102769 
				AND t3150.c3150_void_fl IS NULL 
				AND c3150_close_dt IS NULL 
				AND GET_WEEKDAYSDIFF(t3150.c3150_submitted_dt,TRUNC(SYSDATE)) = get_rule_value('20DAYS','EMAILCAPA')  
			UNION 
			SELECT	'30' day, t3150.c3150_capa_id capaid
					, t3150.c3150_lot_number capalotnum
					, t3150.c3150_capa_ref caparef
					, t3150.c3150_capa_desc capadesc
					, to_char(t3150.c3150_response_dt,get_rule_value('DATEFMT','DATEFORMAT')) caparesdate
					, get_user_name(t3150.c101_issued_by) capaissuedby
					, get_user_emailid(t3151.c101_assigned_to) assignedto 
				FROM t3150_capa t3150,t3151_capa_assignee t3151 
				WHERE t3150.c3150_capa_id = t3151.c3150_capa_id 
				AND t3150.c901_status = 102769 
				AND t3150.c3150_void_fl IS NULL
				AND c3150_close_dt IS NULL
				AND GET_WEEKDAYSDIFF(t3150.c3150_submitted_dt,TRUNC(SYSDATE)) = get_rule_value('30DAYS','EMAILCAPA');
		
	END gm_fch_capa_eval_info;
	
	
	
END gm_pkg_qa_capa_rpt;
/