CREATE OR REPLACE PACKAGE body gm_pkg_qa_set_mapping_rpt
IS
    --
    /****************************************************************
    * Description : Procedure used to fetch set mapping bulk upload details based on set id
    * Author      :
    *****************************************************************/

PROCEDURE gm_fch_set_mapping_upload_dtls (
        p_set_id    IN t207_set_master.c207_set_id%TYPE,
        p_status_id IN T2084_SET_MAPPING_UPLOAD_DTLS.C901_STATUS%TYPE,
        p_out_upload_dtls OUT CLOB)
AS
    v_out_upload_master CLOB;
    v_out_upload_dtls CLOB;
BEGIN
	
    -- to get the master data
    gm_fch_set_map_upload_header_dtls (p_set_id, v_out_upload_master) ;
    -- to get the details
    gm_fch_set_map_upload_dtls (p_set_id, p_status_id, v_out_upload_dtls) ;
    --
    p_out_upload_dtls := v_out_upload_master ||'^^' || v_out_upload_dtls;
    
END gm_fch_set_mapping_upload_dtls;
--
/****************************************************************
* Description : Procedure used to fetch set mapping bulk upload header details.
* Author      :
*****************************************************************/
PROCEDURE gm_fch_set_map_upload_header_dtls (
        p_set_id IN t207_set_master.c207_set_id%TYPE,
        p_out_header_dtls OUT CLOB)
AS
    v_date_fmt VARCHAR2 (20) ;
BEGIN
    --
     SELECT get_compdtfmt_frm_cntx () INTO v_date_fmt FROM dual;
     --
    BEGIN
        -- to get the header details
         SELECT JSON_OBJECT ('setUploadStatus' value get_code_name (c901_status), 'setUploadUpdateBy' value
            get_user_name (c2083_last_updated_by), 'setUploadUpdatedDate' value NVL (TO_CHAR (c2083_last_updated_date,
            v_date_fmt||' HH:MI:SS AM'), ' '))
           INTO p_out_header_dtls
           FROM t2083_set_mapping_upload
          WHERE c207_set_id    = p_set_id
            AND c2083_void_fl IS NULL;
            
    EXCEPTION
    WHEN OTHERS THEN
        p_out_header_dtls := NULL;
    END;
    --
END gm_fch_set_map_upload_header_dtls;

--
/****************************************************************
* Description : Procedure used to fetch set mapping bulk upload grid details.
* Author      :
*****************************************************************/

PROCEDURE gm_fch_set_map_upload_dtls (
        p_set_id    IN t207_set_master.c207_set_id%TYPE,
        p_status_id IN T2084_SET_MAPPING_UPLOAD_DTLS.C901_STATUS%TYPE,
        p_out_set_dtls OUT CLOB)
AS
--
BEGIN
	--
    BEGIN
         SELECT JSON_ARRAYAGG (JSON_OBJECT ('setMappingId' value t2084.c2084_set_mapping_upload_dtls_id, 'setId' value
            t2084.c207_set_id, 'partNum' value t2084.c205_part_number_id, 'setQty' value t2084.c2084_set_qty,
            'criticalFl' value '', 'uploadAction' value t2084.c901_action, 'invalidPart' value c2084_invalid_part_fl,
            'invalidCriticalFl' value c2084_invalid_critical_fl)
       ORDER BY c205_part_number_id ASC RETURNING CLOB)
           INTO p_out_set_dtls
           FROM t2084_set_mapping_upload_dtls t2084
          WHERE t2084.c207_set_id = p_set_id
            AND t2084.c901_status = NVL (p_status_id, t2084.c901_status) -- 108745  Validation Failed
            AND t2084.c2084_void_fl    IS NULL;
    EXCEPTION
    WHEN OTHERS THEN
        p_out_set_dtls := NULL;
    END;
    --
END gm_fch_set_map_upload_dtls;

END gm_pkg_qa_set_mapping_rpt;
/