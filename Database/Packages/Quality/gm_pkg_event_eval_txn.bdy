CREATE OR REPLACE
PACKAGE body gm_pkg_event_eval_txn
       IS

   
    /****************************************************************
	* Description : Procedure to save the form data without pdf generation
	* Author      : Parthiban
	*****************************************************************/
   
   PROCEDURE gm_sav_event_eval
		(
		 p_partid        IN           t3106_event_eval_form.c205_part_number_id%type,
		 p_lot_number    IN           t3106_event_eval_form.c3106_lot_number%type,
		 p_qty           IN           t3106_event_eval_form.c3106_qty%type,
		 p_loaner        IN           t3106_event_eval_form.c3106_loaner_fl%type,
		 p_consignment	 IN           t3106_event_eval_form.c3106_consignment_fl%type,
		 p_sold          IN           t3106_event_eval_form.c3106_sold_fl%type,
		 p_desc          IN           t3106_event_eval_form.c3106_event_detailed_desc%type,
		 p_evnt_dt       IN           t3106_event_eval_form.c3106_event_dt%type,
		 p_evnt_id       IN           t3106_event_eval_form.c901_event_occured_id%type,
		 p_report_id     IN           t3106_event_eval_form.c901_report_cond_id%type,
		 p_surg_id       IN           t3106_event_eval_form.c901_rev_surg_id%type,
		 p_surg_reason   IN           t3106_event_eval_form.c3106_rev_surg_yes_reason%type,
		 p_adv_efct      IN           t3106_event_eval_form.c901_adv_effect_id%type,
		 p_typ_of_surg   IN           t3106_event_eval_form.c3106_type_of_surgery%type,
		 p_surg_nm       IN           t3106_event_eval_form.c3106_surgeon_nm%type,
		 p_hosp_nm       IN           t3106_event_eval_form.c3106_hospital_nm%type,
		 p_op_id         IN           t3106_event_eval_form.c901_post_op_id%type,
		 p_surg_dt       IN           t3106_event_eval_form.c3106_orig_surgery_dt%type,
		 p_prc_out_id    IN           t3106_event_eval_form.c901_proc_outcome_id%type,
		 p_replc_id      IN           t3106_event_eval_form.c901_replacement_need_id%type,
		 p_part_aval     IN           t3106_event_eval_form.c901_part_aval_eval_id%type,
		 p_ship_id       IN           t3106_event_eval_form.c901_part_shipped_id%type,
		 p_decon_id      IN           t3106_event_eval_form.c901_part_decon_id%type,
		 p_ship_dt       IN           t3106_event_eval_form.c3106_part_shipped_dt%type,
		 p_ship_crr      IN           t3106_event_eval_form.c901_ship_carrier%type,
		 p_trk_num       IN           t3106_event_eval_form.c3106_tracking_number%type,
		 p_ser_req_id    IN           t3106_event_eval_form.c901_service_req_id%type,
		 p_rep_id        IN           t3106_event_eval_form.c703_sales_rep_id%type,
		 p_cmp_id        IN           t3106_event_eval_form.c1900_company_id%type,
		 p_eval_id       IN OUT    	  t3106_event_eval_form.c3106_event_eval_form_id%type,
		 p_err_msg       OUT          VARCHAR2
		)
		
		
       AS  
       	v_eval_id  t3106_event_eval_form.c3106_event_eval_form_id%type;
      	v_count NUMBER;
	   BEGIN
		  
			p_err_msg := '';
		 IF p_desc = '' OR p_desc is null THEN
		   	p_err_msg := 'Detailed Description of Event,';
		 END IF;
		 IF p_evnt_id = '' OR p_evnt_id is null THEN
		   	p_err_msg := p_err_msg || 'Event Happen During,';
		 END IF;
		 IF p_surg_id = '' OR p_surg_id is null THEN
		   	p_err_msg := p_err_msg || 'Revision Surgery,';
		 END IF;
		 IF p_adv_efct = '' OR p_adv_efct is null THEN
		   	p_err_msg := p_err_msg || 'Adverse Effect to Patient,';		   	
		 END IF;

		 IF p_err_msg = '' OR p_err_msg is null	 THEN	   
			UPDATE  t3106_event_eval_form
			SET 
			   c205_part_number_id       = p_partid,
		       c703_sales_rep_id         = p_rep_id,
		       c3106_lot_number          = upper(p_lot_number),
		       c3106_qty                 = p_qty,
		       c3106_loaner_fl           = p_loaner,
		       c3106_consignment_fl      = p_consignment,
		       c3106_sold_fl             = p_sold,
		       c3106_event_detailed_desc = lower (p_desc),   
		       c3106_event_dt            = p_evnt_dt,
		       c901_event_occured_id     = p_evnt_id,
		       c901_report_cond_id       = p_report_id,
		       c901_rev_surg_id          = p_surg_id,
		       c3106_rev_surg_yes_reason = p_surg_reason,
		       c901_adv_effect_id        = p_adv_efct,
		       c3106_type_of_surgery     = p_typ_of_surg,
		       c3106_surgeon_nm          = p_surg_nm,
		       c3106_hospital_nm         = p_hosp_nm,
		       c901_post_op_id           = p_op_id,
		       c3106_orig_surgery_dt     = p_surg_dt,
		       c901_proc_outcome_id      = p_prc_out_id,
		       c901_replacement_need_id  = p_replc_id,
		       c901_part_aval_eval_id    = p_part_aval,
		       c901_part_shipped_id      = p_ship_id,
		       c901_part_decon_id        = p_decon_id,
		       c3106_part_shipped_dt     = p_ship_dt,
		       c901_ship_carrier         = p_ship_crr,
		       c3106_tracking_number     = p_trk_num,
		       c901_service_req_id       = p_ser_req_id,
		       c1900_company_id          = p_cmp_id,
		       c3106_last_updated_by     = p_rep_id,
		       c3106_last_updated_date   = CURRENT_DATE
		       WHERE 
		       c3106_event_eval_form_id  = p_eval_id;
       	    -- PMT-40110 :: This update should be go with Primary Key , reason that c703_sales_rep_id has been removed
       	    
	          IF (SQL%ROWCOUNT = 0)
			  THEN				
			
		      		  	SELECT S3106_EVENT_EVAL_FORM.nextval
					  	INTO v_eval_id
					  	FROM DUAL;
					  	
					  	V_EVAL_ID := 'EEF-' || V_EVAL_ID;
				 		
						INSERT INTO t3106_event_eval_form (
						c3106_event_eval_form_id, c205_part_number_id, c3106_lot_number, c3106_qty , c3106_event_detailed_desc, c3106_event_dt, c901_event_occured_id, 
						c901_report_cond_id, c901_rev_surg_id, c3106_rev_surg_yes_reason, c901_adv_effect_id, c3106_type_of_surgery, c3106_surgeon_nm, c3106_hospital_nm, c901_post_op_id, 
						c3106_orig_surgery_dt, c901_proc_outcome_id, c901_replacement_need_id, c901_part_aval_eval_id, c901_part_shipped_id, c901_part_decon_id, c3106_part_shipped_dt, 
						c901_ship_carrier, c3106_tracking_number, c901_service_req_id,c703_sales_rep_id,c1900_company_id,
						c3106_created_dt, c3106_loaner_fl, c3106_consignment_fl, c3106_sold_fl)
						VALUES (
						v_eval_id, p_partid, upper(p_lot_number), p_qty,lower(p_desc),p_evnt_dt, p_evnt_id, p_report_id, p_surg_id, p_surg_reason, p_adv_efct, p_typ_of_surg, p_surg_nm, p_hosp_nm,
						p_op_id,p_surg_dt, p_prc_out_id, p_replc_id, p_part_aval, p_ship_id, p_decon_id,p_ship_dt , p_ship_crr, p_trk_num, p_ser_req_id, p_rep_id,
						p_cmp_id, CURRENT_DATE, p_loaner, p_consignment, p_sold
						);
						
						p_eval_id := V_EVAL_ID; 
			  END IF;
			  ELSE			    
			  	p_err_msg :=  'Below Fields are Empty: ' || p_err_msg;
			  	p_err_msg := SUBSTR(p_err_msg,1,LENGTH(p_err_msg)-1);
			END IF;
		END gm_sav_event_eval;
		
	
	 /****************************************************************
	* Description : Procedure to fetch the form data to show or to create pdf
	* Author      : Parthiban
	*****************************************************************/
		
	PROCEDURE gm_fch_event_eval
(
		p_eval_id     IN     t3106_event_eval_form.c3106_event_eval_form_id%type,
		p_out_cursor  out    types.cursor_type
) AS
	
	BEGIN
		
		OPEN p_out_cursor
		FOR
		
		
		   	SELECT t205.c205_part_number_id||' '||t205.c205_part_num_desc partnm, t205.c205_part_number_id partid, t3106.c3106_lot_number lotnumber, t3106.c3106_qty qty,  t3106.c3106_loaner_fl loanerfl, t3106.c3106_consignment_fl consignmentfl, t3106.c3106_sold_fl soldfl, t3106.c3106_event_detailed_desc descr, to_char(t3106.c3106_event_dt, 'mm/dd/yyyy') evntdt,c901_event_occured_id evnthappen,c901_report_cond_id rptcond,
		    c901_rev_surg_id revsurg, t3106.c3106_rev_surg_yes_reason revsurgtxt, c901_adv_effect_id advefct, t3106.c3106_type_of_surgery typofsurg, t3106.c3106_surgeon_nm surg, t3106.c3106_hospital_nm hospital, c901_post_op_id postoprt,  to_char(t3106.c3106_orig_surgery_dt, 'mm/dd/yyyy') orgdate, c901_proc_outcome_id prcout,
		    c901_replacement_need_id replacement, c901_part_aval_eval_id partaval,c901_part_shipped_id shipped, c901_part_decon_id decontaminated,  to_char(t3106.c3106_part_shipped_dt, 'mm/dd/yyyy') dateshipped, c901_ship_carrier carrier, t3106.c3106_tracking_number trknumber,
		    c901_service_req_id fldservice, t3106.c3106_event_eval_form_id evalformid,get_rep_name (c703_sales_rep_id) repnm, get_company_name(c1900_company_id) compnm, get_rep_phone(c703_sales_rep_id) repnum,get_rep_address(c703_sales_rep_id) repaddress,to_char(t3106.c3106_pdf_created, 'mm/dd/yyyy') createddt
			FROM t205_part_number t205,t3106_event_eval_form t3106 
			WHERE  t3106.c205_part_number_id=t205.c205_part_number_id(+) 
			AND C3106_VOID_FL IS NULL AND t3106.c3106_event_eval_form_id = p_eval_id;
		    
	END  gm_fch_event_eval;
	
	 /****************************************************************
	* Description : Procedure to void the form
	* Author      : Parthiban
	*****************************************************************/
		
	PROCEDURE gm_eval_form_void
	(
		p_rep_id      IN     t3106_event_eval_form.c703_sales_rep_id%type,
	    p_eval_id     IN     t3106_event_eval_form.c3106_event_eval_form_id%type
	     
	)AS
	
	BEGIN
		
		
		UPDATE T3106_Event_Eval_Form SET C3106_VOID_FL='Y', C3106_LAST_UPDATED_BY = p_rep_id, C3106_LAST_UPDATED_DATE = CURRENT_DATE
		WHERE C703_Sales_Rep_Id = p_rep_id AND c3106_event_eval_form_id = p_eval_id;

		END gm_eval_form_void; 

		
	PROCEDURE gm_fetch_pdf
		(
		p_eval_id     IN     t3106_event_eval_form.c3106_event_eval_form_id%type,
		p_date_fmt    IN     VARCHAR2,
		p_out_cursor  OUT    types.cursor_type
		)  AS
	BEGIN
		  
		OPEN p_out_cursor
		FOR  
		
		
		   	SELECT t205.c205_part_num_desc partnm, t205.c205_part_number_id partid, t3106.c3106_lot_number lotnumber, t3106.c3106_qty qty,  t3106.c3106_loaner_fl loanerfl, t3106.c3106_consignment_fl consignmentfl, t3106.c3106_sold_fl soldfl, t3106.c3106_event_detailed_desc descr, to_char(t3106.c3106_event_dt, p_date_fmt) evntdt,c901_event_occured_id evnthappen,c901_report_cond_id rptcond,
		    c901_rev_surg_id revsurg, t3106.c3106_rev_surg_yes_reason revsurgtxt, c901_adv_effect_id advefct, t3106.c3106_type_of_surgery typofsurg, t3106.c3106_surgeon_nm surg, t3106.c3106_hospital_nm hospital, c901_post_op_id postoprt,  to_char(t3106.c3106_orig_surgery_dt, p_date_fmt) orgdate, c901_proc_outcome_id prcout,
		    c901_replacement_need_id replacement, c901_part_aval_eval_id partaval,c901_part_shipped_id shipped, c901_part_decon_id decontaminated,  to_char(t3106.c3106_part_shipped_dt, p_date_fmt) dateshipped, c901_ship_carrier carrier, t3106.c3106_tracking_number trknumber,
		    c901_service_req_id fldservice, t3106.c3106_event_eval_form_id evalformid,get_rep_name (c703_sales_rep_id) repnm, get_company_name(c1900_company_id) compnm, get_rep_phone(c703_sales_rep_id) repnum,get_rep_address(c703_sales_rep_id) repaddress,to_char(t3106.c3106_pdf_created, p_date_fmt) createddt
			FROM t205_part_number t205,t3106_event_eval_form t3106 
			WHERE  t3106.c205_part_number_id=t205.c205_part_number_id(+)  AND C3106_VOID_FL IS NULL  
			AND t3106.c3106_event_eval_form_id = p_eval_id;
		    
	END  gm_fetch_pdf; 
	
	 /****************************************************************
	* Description : Procedure to update the created date
	* Author      : Parthiban
	*****************************************************************/
	
	
	PROCEDURE gm_eval_form_date
	(
		p_eval_id     IN     t3106_event_eval_form.c3106_event_eval_form_id%type
	     
	)AS
	
	BEGIN
		
		
		UPDATE T3106_Event_Eval_Form SET C3106_PDF_CREATED = CURRENT_DATE
		WHERE  c3106_event_eval_form_id = p_eval_id;

		END gm_eval_form_date; 
		
		
		/****************************************************************
	* Description : Procedure to update the Email Sent
	* Author      : Thangasamy
	*****************************************************************/
	
	
	PROCEDURE gm_eval_email_sent
	(
		p_eval_id     IN     t3106_event_eval_form.c3106_event_eval_form_id%type
	     
	)AS
	
	BEGIN
		
		
		UPDATE T3106_Event_Eval_Form SET C3106_EMAIL_SENT = CURRENT_DATE
		WHERE  c3106_event_eval_form_id = p_eval_id;

		END gm_eval_email_sent; 
	
	
   END gm_pkg_event_eval_txn;
   /