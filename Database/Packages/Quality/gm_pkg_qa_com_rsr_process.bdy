/* Formatted on 03/19/2010 10:19 (Formatter Plus v4.8.0) */
-- @"C:\Database\Packages\Quality\gm_pkg_qa_com_rsr_process.bdy"
CREATE OR REPLACE PACKAGE BODY gm_pkg_qa_com_rsr_process
IS
	    /*******************************************************
	    * Purpose: This Procedure is used to save
	    * the upload file info
	    *******************************************************/
	    --
	PROCEDURE gm_validate_id (
		p_ref_id    IN VARCHAR2
	   ,p_type      IN VARCHAR2
	   ,p_out_type  OUT VARCHAR2
	   ,p_action     IN VARCHAR2 DEFAULT 'true'
	)
	AS
		v_ref_type VARCHAR2(20) := p_type;
		v_count 	NUMBER(5);
		v_error		VARCHAR2(20):= 'COM/RSR';
	BEGIN
		
		IF p_type = '102788' THEN -- CAPA
			SELECT COUNT(*) INTO v_count FROM T3150_CAPA WHERE UPPER(C3150_CAPA_ID) = UPPER(p_ref_id);
		ELSIF p_type = '102789' THEN -- SCAR
			SELECT COUNT(*) INTO v_count FROM T3160_SCAR WHERE UPPER(C3160_SCAR_ID) = UPPER(p_ref_id);
		ELSIF p_type = '102790'  OR p_type = '102791'  THEN -- 102790:COM / 102791:RSR
			BEGIN	
				SELECT DECODE(C901_ISSUE_TYPE,102809,102790,102810,102791) INTO v_ref_type FROM T3100_INCIDENT WHERE UPPER(C3100_INCIDENT_ID) = UPPER(p_ref_id);
			EXCEPTION
				WHEN NO_DATA_FOUND
				THEN
					v_count := 0;
			END;
		END IF;
		
		IF v_count = 0 AND p_action = 'true' THEN
			IF p_type = '102790'  OR p_type = '102791' THEN
			
			GM_RAISE_APPLICATION_ERROR('-20999','383',v_error);
				
			ELSE
			GM_RAISE_APPLICATION_ERROR('-20999','384',GET_CODE_NAME(p_type));
				
			END IF;
		END IF;
		IF  v_count = 0 AND p_action = 'false' THEN
			v_ref_type := '-20311';
		END IF;
		p_out_type := v_ref_type;
	END gm_validate_id;
	
	/******************************************************************
    * Description : Procedure to fetch the Closure Email Details
    * Author      : HReddi
	*******************************************************************/
    PROCEDURE gm_fch_closure_email (
		p_ref_id    		IN t9101_closure_email.c9101_ref_id%TYPE
	   ,p_ref_type      	IN t9101_closure_email.c901_ref_id%TYPE
	   ,p_user_id           IN t9101_closure_email.c9101_created_by%TYPE
	   ,p_closure_out	 	OUT TYPES.cursor_type
	   ,p_user_out	 		OUT TYPES.cursor_type
	   ,P_Complint_nm       OUT VARCHAR2
	)AS
	BEGIN
		               
        OPEN p_user_out FOR        
			SELECT get_user_name(c101_user_id) username
			     , c101_ph_extn userextn
			     , c101_title userdesignation
			  FROM t101_user 
			 WHERE c101_user_id = p_user_id;	
		
			 OPEN p_closure_out FOR
			SELECT c9101_closure_email_to toEmail
                 , c9101_closure_email_cc ccEmail
                 , c9101_closure_subject emailSubject
                 , c9101_closure_email_dtl emailCommnets
              FROM t9101_closure_email 
             WHERE c9101_ref_id = p_ref_id 
               AND c901_ref_id  = p_ref_type 
               AND c9101_void_fl IS NULL;
        BEGIN  
	    SELECT get_user_name(C3100_COMPLAINANT_ID) 
	      INTO P_Complint_nm 
	      FROM t3100_incident 
	     WHERE c3100_incident_id = p_ref_id
	       AND c3100_void_fl IS NULL ;
	   EXCEPTION
	     WHEN NO_DATA_FOUND THEN
	     P_Complint_nm:=NULL;
	   END;
	   
	END gm_fch_closure_email;
	
    /****************************************************************
    * Description : Procedure to save the CMP Closure Email Details
    * Author      : HReddi
    *****************************************************************/
    
	PROCEDURE gm_sav_closure_email(
    	p_ref_id		    IN   t9101_closure_email.C9101_REF_ID%TYPE
      , p_ref_type      	IN   t9101_closure_email.C901_REF_ID%TYPE
      , p_to_email    		IN   t9101_closure_email.C9101_CLOSURE_EMAIL_TO%TYPE
      , p_cc_email       	IN   t9101_closure_email.C9101_CLOSURE_EMAIL_CC%TYPE
      , p_email_subject   	IN   t9101_closure_email.C9101_CLOSURE_SUBJECT%TYPE    
      , p_email_content		IN   t9101_closure_email.C9101_CLOSURE_EMAIL_DTL%TYPE  
      , p_userid     		IN   t9101_closure_email.C9101_CREATED_BY%TYPE             
    )AS 		
    v_out_type VARCHAR2(20);
 	 BEGIN
	 		UPDATE T9101_CLOSURE_EMAIL
	 		   SET C901_REF_ID              = p_ref_type       
				 , C9101_CLOSURE_EMAIL_TO   = p_to_email
				 , C9101_CLOSURE_EMAIL_CC   = p_cc_email
				 , C9101_CLOSURE_SUBJECT    = p_email_subject
				 , C9101_CLOSURE_EMAIL_DTL  = p_email_content
				 , C9101_LAST_UPDATED_BY    = p_userid
				 , C9101_LAST_UPDATED_DATE  = SYSDATE       
	 		WHERE C9101_REF_ID 				= p_ref_id;

	 		IF (SQL%ROWCOUNT = 0) THEN
 				gm_pkg_qa_com_rsr_process.gm_validate_id(p_ref_id,p_ref_type,v_out_type);
 			
 				INSERT INTO T9101_CLOSURE_EMAIL
				    (C9101_CLOSURE_EMAIL_ID ,C9101_REF_ID,C901_REF_ID,C9101_CLOSURE_EMAIL_TO,C9101_CLOSURE_EMAIL_CC,
				     C9101_CLOSURE_SUBJECT,C9101_CLOSURE_EMAIL_DTL,C9101_CREATED_BY,C9101_CREATED_DATE
				    )
	  			VALUES(
				       S9101_closure_email.nextval,p_ref_id,p_ref_type,p_to_email,p_cc_email,
				       p_email_subject,p_email_content,p_userid,SYSDATE
			    );
			END IF;			
	END gm_sav_closure_email;
	/******************************************************************
	    * Description : Procedure to Void COM/RSR
	    * Author      : mrajasekaran
	*******************************************************************/
	
	PROCEDURE gm_upd_void_com_rsr
	(
		p_txn_id   IN T3105_INCIDENT_CLOSURE_DTL.C3100_INCIDENT_ID%TYPE,
	    p_user_id  IN T3105_INCIDENT_CLOSURE_DTL.C3105_CREATED_BY%TYPE
	)
	AS
		v_rma_id T506_RETURNS.C506_RMA_ID%TYPE;
	    v_part_num T506B_RA_INCIDENT_LINK.C205_PART_NUMBER_ID%TYPE;
	BEGIN
		SELECT C205_PART_NUMBER_ID ,C506_RMA_ID INTO v_part_num ,v_rma_id FROM T3101_INCIDENT_PART_DETAIL 
		WHERE C3100_incident_id  = p_txn_id
		AND c3101_void_fl IS NULL ;
		
		UPDATE T3100_INCIDENT
		SET 
			C3100_VOID_FL = 'Y'
			,C3100_LAST_UPDATED_BY = p_user_id
			,C3100_LAST_UPDATED_DATE = SYSDATE
		WHERE C3100_incident_id = p_txn_id
		AND c3100_void_fl IS NULL ;
		
		UPDATE T3101_INCIDENT_PART_DETAIL
		SET
			C3101_VOID_FL = 'Y'
			,C3101_LAST_UPDATED_BY = p_user_id
			,C3101_LAST_UPDATED_DATE = SYSDATE
		WHERE C3100_incident_id  = p_txn_id
		AND c3101_void_fl IS NULL ;
		
		UPDATE T3102_INCIDENT_DETAIL	
		SET
			C3102_VOID_FL = 'Y'
			,C3102_LAST_UPDATED_BY = p_user_id
			,C3102_LAST_UPDATED_DATE = SYSDATE
		WHERE C3100_incident_id  = p_txn_id
		AND c3102_void_fl IS NULL ;
		
		UPDATE T3103_INCIDENT_QA_EVAL	
		SET
			C3103_VOID_FL = 'Y'
			,C3103_LAST_UPDATED_BY = p_user_id
			,C3103_LAST_UPDATED_DATE = SYSDATE
		WHERE C3100_incident_id  = p_txn_id
		AND c3103_void_fl IS NULL ;
		
		UPDATE T3104_INCIDENT_ENG_EVAL	
		SET
			C3104_VOID_FL = 'Y'
			,C3104_LAST_UPDATED_BY = p_user_id
			,C3104_LAST_UPDATED_DATE = SYSDATE
		WHERE C3100_incident_id  = p_txn_id
		AND c3104_void_fl IS NULL ;
		
		UPDATE T3105_INCIDENT_CLOSURE_DTL	
		SET
			 C3105_VOID_FL = 'Y'
			,C3105_LAST_UPDATED_BY = p_user_id
			,C3105_LAST_UPDATED_DATE = SYSDATE
		WHERE C3100_incident_id  = p_txn_id
		AND c3105_void_fl IS NULL ;
		
		IF v_part_num IS NOT NULL AND v_rma_id IS NOT NULL THEN
		UPDATE	T506B_RA_INCIDENT_LINK
		SET
			C901_INCIDENT_LINK_STATUS = 102849 
			WHERE C205_PART_NUMBER_ID = v_part_num
			AND C506_RMA_ID = v_rma_id
			AND C901_INCIDENT_LINK_STATUS = '102850'
			AND c506B_void_fl IS NULL ;
		END IF;
		
		
	END gm_upd_void_com_rsr;
	
	/******************************************************************
	    * Description : Procedure to Reopen COM/RSR
	    * Author      : mrajasekaran
	*******************************************************************/
	PROCEDURE gm_upd_reopen_com_rsr
	(
		p_txn_id   IN T3105_INCIDENT_CLOSURE_DTL.C3100_INCIDENT_ID%TYPE,
	    p_user_id  IN T3105_INCIDENT_CLOSURE_DTL.C3105_CREATED_BY%TYPE
	)
	AS
	v_issue VARCHAR2(20);	
	v_message VARCHAR2(20);
	BEGIN
		SELECT C901_ISSUE_TYPE INTO v_issue FROM T3100_INCIDENT
		WHERE C3100_incident_id  = p_txn_id
		AND c3100_void_fl IS NULL ;
		
		UPDATE  T3100_INCIDENT
		SET
			C901_STATUS         =NULL
		   ,C3100_LAST_UPDATED_BY = p_user_id
		   ,C3100_LAST_UPDATED_DATE = SYSDATE
		WHERE C3100_incident_id  = p_txn_id 
		AND c3100_void_fl IS NULL ;
		
		UPDATE T3105_INCIDENT_CLOSURE_DTL	
		SET
			C101_CLOSED_BY  = NULL
			,C3105_CLOSED_DT  = NULL
			,C3105_LAST_UPDATED_BY = p_user_id
		   ,C3105_LAST_UPDATED_DATE = SYSDATE
		WHERE C3100_incident_id  = p_txn_id
		AND c3105_void_fl IS NULL ;
		gm_pkg_qa_com_rsr_txn.gm_upd_com_rsr_status(p_txn_id,p_user_id);
		
		IF v_issue = '102810' THEN
			gm_update_log (p_txn_id, 'RSR Reopened', 4000320, p_user_id, v_message);			
		ELSE
			gm_update_log (p_txn_id, 'COM Reopened', 4000319, p_user_id, v_message);
		END IF;
			END gm_upd_reopen_com_rsr;
		/******************************************************************
	    * Description : Procedure to Ignore COM/RSR
	    * Author      : mrajasekaran
	*******************************************************************/
	PROCEDURE gm_upd_ignore_com_rsr
	(
		p_txn_id   		IN VARCHAR2,
		p_cancel_reason IN VARCHAR2,
	    p_cancelled_id  IN NUMBER,
	    p_user_id  		IN T3105_INCIDENT_CLOSURE_DTL.C3105_CREATED_BY%TYPE
	)	
	AS
	V_RMA_ID T506_RETURNS.C506_RMA_ID%TYPE;
	v_txn_id VARCHAR2(100);
	v_string VARCHAR2(4000);
	v_part_num T506B_RA_INCIDENT_LINK.C205_PART_NUMBER_ID%TYPE;
	v_cnt NUMBER;
	BEGIN
		v_string:=p_txn_id || ',';
		
		WHILE INSTR (v_string, ',') <> 0
        LOOP
            v_txn_id := SUBSTR (v_string, 1, INSTR (v_string, ',') - 1);
            v_string := SUBSTR (v_string, INSTR (v_string, ',') + 1);
            
		 SELECT COUNT(1) INTO v_cnt
   FROM T506B_RA_INCIDENT_LINK
  WHERE c506b_ra_incident_link_id = v_txn_id
    AND c901_incident_link_status = 102849 -- Open
    AND c506b_void_fl            IS NULL;
    
    IF v_cnt = 0 THEN
    	raise_application_error (-20604, '');--Selected COM/RSR transaction not in Open status
    END IF;
  		
    	SELECT c506_rma_id, c205_part_number_id
	   		INTO V_RMA_ID, v_part_num
	   FROM T506B_RA_INCIDENT_LINK
	  	 WHERE c506b_ra_incident_link_id = v_txn_id
	    	AND c506b_void_fl IS NULL FOR UPDATE;
	    	
		IF V_RMA_ID IS NOT NULL AND v_part_num IS NOT NULL THEN
		UPDATE T506B_RA_INCIDENT_LINK
		SET 
			 C901_INCIDENT_LINK_STATUS= 102851 
			 ,C506B_LAST_UPDATED_BY = p_user_id
		     ,C506B_LAST_UPDATED_DATE = SYSDATE
		WHERE C506_RMA_ID = V_RMA_ID 
		AND   C205_PART_NUMBER_ID = v_part_num
		AND C506B_VOID_FL IS NULL;
		
		UPDATE T506_RETURNS
		SET	C506_REASON = DECODE(p_cancel_reason,'102883','3311','102884','3313',C506_REASON)
			 ,C506_LAST_UPDATED_BY = p_user_id
		     ,C506_LAST_UPDATED_DATE = SYSDATE
		WHERE C506_RMA_ID = V_RMA_ID
		 AND    C506_VOID_FL IS NULL;		
		END IF;
		
		END LOOP;
		END gm_upd_ignore_com_rsr;
		
		/******************************************************************
	    * Description : Procedure to fetch Part Not Received Information
	    * Author      : arajan
		*******************************************************************/
		PROCEDURE gm_fch_partnotreced_info
		(
			p_out_data OUT TYPES.cursor_type
		)
		AS
		BEGIN
			OPEN p_out_data FOR
				 SELECT t3101.c3100_incident_id incidentId, t3101.c506_rma_id raID
				      ,DECODE(t3100.c901_complainant_type,'102812'
				         ,(SELECT  t101.c101_first_nm FROM  t703_sales_rep t703,t101_party t101 WHERE t703.c703_sales_rep_id  = t3100.c3100_complainant_id 
				               and t101.c101_party_id=t703.c101_party_id and t703.c703_void_fl is null and t101.c101_void_fl is null)
				         ,(SELECT  C101_USER_F_NAME FROM  T101_USER WHERE  C101_USER_ID = t3100.c3100_complainant_id))compName
					  , GET_USER_EMAILID(t3100.c3100_complainant_id) compEmailId, to_char(t3100.c3100_received_dt,'MM/DD/YYYY') compRecDt
					  , t3101.c205_part_number_id partNum, t3101.c3101_lot_number lotNum
					  , t3100.c3100_detail_event_desc eventDesc, t901.c902_code_nm_alt refType
					  , t3100.c3100_created_by createdBy
				FROM t3100_incident t3100, t3101_incident_part_detail t3101, t901_code_lookup t901, t3103_incident_qa_eval t3103
				WHERE t3101.c3100_incident_id = t3100.c3100_incident_id
				AND t3101.c3100_incident_id = t3103.c3100_incident_id (+) 
                AND t3103.c3103_part_receive_dt IS NULL 
				AND t3100.c901_issue_type = t901.c901_code_id
				AND t3100.c901_status IN (102837, 4000321) -- Status : Pending Return and In Progress
				AND (t3100.c3100_received_dt BETWEEN (SYSDATE-15) AND (SYSDATE-8) --Second Monday
				OR t3100.c3100_received_dt BETWEEN (SYSDATE-22) AND (SYSDATE-15)) --Third Monday
				AND t3100.c3100_void_fl IS NULL
				AND t3101.c3101_void_fl IS NULL
				AND t3103.c3103_void_fl IS NULL 
				AND t901.c901_void_fl IS NULL;
		END gm_fch_partnotreced_info;
		
		/******************************************************************
	    * Description : Procedure to fetch Evaluator Overdue Information
	    * Author      : arajan
		*******************************************************************/
		PROCEDURE gm_fch_overdue_eval_info
		(
			p_gem_id	IN	VARCHAR2,
			p_out_data  OUT TYPES.cursor_type
		)
		AS
		BEGIN
			IF p_gem_id IS NULL THEN
				OPEN p_out_data FOR	-- to get the mail id
					SELECT GET_USER_EMAILID(t3101.c101_gem_id) gemMailId
					FROM t3100_incident t3100, t3101_incident_part_detail t3101, t3103_incident_qa_eval t3103
					WHERE t3100.c3100_incident_id = t3101.c3100_incident_id 
					AND t3100.c3100_incident_id = t3103.c3100_incident_id
					AND t3101.c3100_incident_id = t3103.c3100_incident_id
					AND t3100.c901_status = '102839' -- In Progress with Evaluator
					AND GET_WEEKDAYSDIFF(t3103.c3103_sent_to_eval_dt,TRUNC(SYSDATE)) >= 14
					AND t3100.c3100_void_fl IS NULL
					AND t3101.c3101_void_fl IS NULL
					AND t3103.c3103_void_fl IS NULL
					GROUP BY GET_USER_EMAILID(t3101.c101_gem_id);
					
			ELSE
				OPEN p_out_data FOR -- to get the details corresponding to mail id
					SELECT t3100.c3100_incident_id incidentId
						, t3101.c101_gem_id gem
						
						,(SELECT  C101_USER_F_NAME FROM  T101_USER WHERE  C101_PARTY_ID = t3101.c101_gem_id)gemname
						, TRUNC(SYSDATE - t3103.c3103_sent_to_eval_dt) dayCount
						, t901.c902_code_nm_alt refType
						, t3100.c3100_created_by createdBy
					FROM t3100_incident t3100, t3101_incident_part_detail t3101, t3103_incident_qa_eval t3103, t901_code_lookup t901
					WHERE t3100.c3100_incident_id = t3101.c3100_incident_id 
					AND t3100.c3100_incident_id = t3103.c3100_incident_id
					AND t3101.c3100_incident_id = t3103.c3100_incident_id
					AND t3100.c901_issue_type = t901.c901_code_id
					AND t3100.c901_status = '102839' -- In Progress with Evaluator
					AND GET_WEEKDAYSDIFF(t3103.c3103_sent_to_eval_dt,TRUNC(SYSDATE)) >= 14
					AND GET_USER_EMAILID(t3101.c101_gem_id) = p_gem_id
					AND t3100.c3100_void_fl IS NULL
					AND t3101.c3101_void_fl IS NULL
					AND t3103.c3103_void_fl IS NULL
					AND t901.c901_void_fl IS NULL;
			END IF;
		END gm_fch_overdue_eval_info;
		
END gm_pkg_qa_com_rsr_process;
/
