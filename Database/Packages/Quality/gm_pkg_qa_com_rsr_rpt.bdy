/* Formatted on 2012/22/08 17:40 (Formatter Plus v4.8.0) */
--@"C:\Database\Packages\Quality\gm_pkg_qa_com_rsr_rpt.bdy"
	CREATE OR REPLACE
		PACKAGE BODY gm_pkg_qa_com_rsr_rpt
			IS
   		/****************************************************************
    	 * Description : Procedure to save the Set Cost as a batch
    	 * Author      : hreddi
    	 *****************************************************************/
		PROCEDURE gm_fch_pend_com_rsr_dash (
			p_pendcomrsrdata OUT TYPES.cursor_type
		)AS
			v_company_id 		t1900_company.c1900_company_id%TYPE;
		 BEGIN
		 
		 	SELECT get_compid_frm_cntx()
          	  INTO v_company_id 
          	  FROM DUAL;
		 
		 
			 OPEN p_pendcomrsrdata FOR			 	
				SELECT t506.c506_rma_id rmaid
					 , get_code_name(t506.c506_reason) reason
					 , t506.c703_sales_rep_id repid
     				 , get_user_name(t506.c703_sales_rep_id) repname 
     				 , t506b.c205_part_number_id partid 
     				 , get_partnum_desc(t506b.c205_part_number_id) partdesc 
     				 , t506b.C506B_QTY itemqty
     				 , t506.c506_created_date createddate
     				 , t506.c506_expected_date expecteddate
     				 , DECODE(t506.c506_status_fl,0,'Pending Return',1,'Pending Return',2,'Returned','') status
     				 , GET_LOG_FLAG(t506.c506_rma_id,1216) logfl
     				 , ROUND(CURRENT_DATE - c506_created_date) Aging
     				 , t506b.c506b_ra_incident_link_id linkid
    			  FROM t506_returns t506,T506B_RA_INCIDENT_LINK t506b
    			 WHERE t506b.c506_rma_id = t506.c506_rma_id 
      			   AND t506.c506_reason IN ('26240385','3253') --3253-QA Evaluation, 26240385-Field Corrective Action
      			   AND t506b.C901_INCIDENT_LINK_STATUS = 102849 -- Open
      			   AND t506.c1900_company_id = v_company_id
                   AND t506b.C506B_VOID_FL IS NULL                   
                   AND t506.c506_void_fl IS NULL
              ORDER BY Aging DESC;
                   
		 END gm_fch_pend_com_rsr_dash;
		 
     /***************************************************************************
    	 * Description : Procedure to fetch RA Info, Part Info and REP Info
    	 * Author      : hreddi
    	 ***************************************************************************/ 
		PROCEDURE gm_fch_incident_RA_info(
        	p_ra_id	 		IN t3101_incident_part_detail.c506_rma_id%TYPE,
       	 	p_partnum_id    IN t3101_incident_part_detail.c205_part_number_id%TYPE,
       	 	p_rep_id        IN t506_returns.c703_sales_rep_id%TYPE,
        	p_data_out     	OUT TYPES.cursor_type        	
        )AS
        	v_address_id VARCHAR2(20);
        	BEGIN
	        	--Checking p_ra_id AND p_partnum_id IS NOT NULL for COM/RSR ISSUE PMT-3112
	        	IF p_ra_id IS NOT NULL AND p_partnum_id IS NOT NULL  THEN
	        		OPEN p_data_out FOR 
	        			     SELECT DISTINCT t506.c506_rma_id raid 
            					  , t506.c703_sales_rep_id repid 
            					  , t507.c205_part_number_id partNumber
            					  , t507.c507_item_qty qty
            					  , t507.c507_control_number lotNum
            					  , get_partnum_desc(t507.c205_part_number_id) partdesc 
            					  , get_project_id_from_part(t507.c205_part_number_id) projectid
            					  , t205.c205_product_family parttype
            					  , t506.c506_created_by complRecvdBy
                                  , to_char(t506.c506_created_date,get_rule_value ('DATEFMT', 'DATEFORMAT')) dtIncDate
              				   FROM t506_returns t506 , t507_returns_item t507 , t205_part_number t205
                              WHERE t506.c506_rma_id = t507.c506_rma_id
              				    AND t507.c205_part_number_id = t205.c205_part_number_id
              				    AND t507.c205_part_number_id =  p_partnum_id 
              					AND t506.c506_rma_id = p_ra_id
              					AND t506.c506_reason = 3253
              					AND t506.c506_void_fl IS NULL ; 	
	        	END IF;
	    		--Checking p_ra_id IS NULL AND p_partnum_id IS NOT NULL for COM/RSR ISSUE PMT-3112
	        	IF  p_partnum_id IS NOT NULL AND p_ra_id IS  NULL THEN
	        		OPEN p_data_out FOR 
	        			 	 SELECT t205.c205_part_number_id partnum
                                  , t205.c205_part_num_desc partdesc
                                  , get_project_id_from_part(t205.c205_part_number_id) projectid 
                                  , t205.c205_product_family parttype
                               FROM t205_part_number t205 
                              WHERE c205_part_number_id = p_partnum_id;
	        	END IF;	        	   
	  
	        	IF p_rep_id IS NOT NULL THEN
	        		SELECT get_address_id(c703_sales_rep_id) 
	        	      INTO v_address_id  
	        	      FROM t703_sales_rep 
	        	     WHERE c703_sales_rep_id = p_rep_id
	        	       AND c703_void_fl IS NULL;
	        		OPEN p_data_out FOR 
	        			 SELECT (DECODE(t106.c106_add1,NULL,NULL,t106.c106_add1||',')
	        			              || DECODE(t106.c106_add2,NULL,NULL,t106.c106_add2||',')
						              || DECODE(NVL(GET_RULE_VALUE('SWAP_ZIP_CODE','SWAP_ZIP_CODE'),'N'),'Y',t106.c106_zip_code,t106.c106_city)
						              || ' '
						              || get_code_name_alt (t106.c901_state)
						              || ' '
						              || DECODE(NVL(GET_RULE_VALUE('SWAP_ZIP_CODE','SWAP_ZIP_CODE'),'N'),'Y',t106.c106_city,t106.c106_zip_code)) ADDRESS 
	        			         , get_rep_phone(p_rep_id) PHONE 
	        			         , GET_AD_REP_ID(p_rep_id) AREADIRECT        			     
                                 , DECODE(t703.c703_rep_category,'4021',DECODE(t106.c901_country,'1101','Globus US Sales'
                                                                                                       ,'Globus ' || get_code_name(t106.c901_country))
                                                                       ,get_distributor_name(t703.c701_distributor_id)) COMPANY
             			   FROM t106_address t106,t703_sales_rep t703 
            			  WHERE t703.c703_sales_rep_id = p_rep_id 
              				AND T106.C101_PARTY_ID 	   = T703.C101_PARTY_ID
              				AND t106.c106_address_id   = v_address_id;
	        	END IF;
	        END gm_fch_incident_RA_info;
	        
	/****************************************************************
    	 * Description : Procedure to fecth the inciden COM/RSR Details
     	 * Author      : Velu
    *****************************************************************/
	PROCEDURE gm_fch_incident_info (
	 	 p_incedent_id  IN   T3100_INCIDENT.C3100_INCIDENT_ID%TYPE,
	 	 p_issue_type 	IN   T3100_INCIDENT.C901_ISSUE_TYPE%TYPE,
	 	 p_user_id      IN   T3100_INCIDENT.C3100_CREATED_BY%TYPE,
		 p_out_data 	OUT  TYPES.cursor_type
	)
	AS
		v_out_type VARCHAR(20);
	 BEGIN
			gm_pkg_qa_com_rsr_process.gm_validate_id(p_incedent_id,'102790',v_out_type);
			
		 OPEN p_out_data FOR	
			 SELECT t3101.c506_rma_id raID 
         			 , t3100.c901_status status
         			 , get_code_name(t3100.c901_status) statusnm
                     , t3100.C901_ISSUE_TYPE issueType
                     , t3100.c901_complainant_type complaint
                     , t3100.c3100_complainant_id complaintname
                     , DECODE(t3100.c901_complainant_type,'102812'
                     	,(GET_REP_NAME (t3100.c3100_complainant_id)),(GET_USER_NAME(t3100.c3100_complainant_id)))COMPLAINTFULLNAME
                     , DECODE(t3100.c901_complainant_type,'102812'
                     ,	(SELECT  t101.c101_first_nm FROM  t703_sales_rep t703,t101_party t101 WHERE t703.c703_sales_rep_id  = t3100.c3100_complainant_id 
                       		and t101.c101_party_id=t703.c101_party_id and t703.c703_void_fl is null and t101.c101_void_fl is null)
                       		,(SELECT  C101_USER_F_NAME FROM  T101_USER WHERE  C101_USER_ID = t3100.c3100_complainant_id))COMPLAINTFIRSTNAME
                     , GET_USER_EMAILID(t3100.c3100_complainant_id) EMAILID
                     , DECODE(t3100.c901_complainant_type,'102812',GET_REP_NAME(t3100.c3100_complainant_id),GET_USER_NAME(t3100.c3100_complainant_id))repName
                     , t3100.c101_ad_id areaDirector
                     , to_char(t3100.C3100_INCIDENT_DT,get_rule_value ('DATEFMT', 'DATEFORMAT')) dtIncDate
                     , t3100.C901_INCIDENT_CATEGORY incidentCat
                     , GET_CODE_NAME(t3100.C901_INCIDENT_CATEGORY) incidentCatNm
                     , t3100.C101_COMPLAINT_RECEIVED_BY complRecvdBy
                     , get_user_name(t3100.C101_COMPLAINT_RECEIVED_BY) complRecvdByName
                     , to_char(T3100.c3100_received_dt,get_rule_value ('DATEFMT', 'DATEFORMAT')) complRecvDt
                     , t3100.c901_received_via complRecvVia
                     , t3100.c101_originator originator
                     , GET_USER_NAME(t3100.c101_originator) orignName
                     , DECODE(t3100.c3100_mdr_reportable,'Y','Yes','N','No','') mdrReport
                     , DECODE(t3100.c3100_meddev_reportable,'Y','Yes','N','No','') meddevReport
                     , t3100.c3100_mdr_number mdrID
                     , t3100.c3100_meddev_number meddevID
                     , to_char(t3100.c3100_mdr_report_dt,get_rule_value ('DATEFMT', 'DATEFORMAT')) mdrReportDt
                     , to_char(t3100.c3100_meddev_report_dt,get_rule_value ('DATEFMT', 'DATEFORMAT')) meddevReportDt
                     , t3100.c3100_detail_event_desc detailDescEvent
                     , t3101.c205_part_number_id partNum
                     , t3101.c205_part_desc partDesc
                     , t3101.c202_project_id project
                     , GET_PROJECT_NAME(t3101.c202_project_id) projdesc
                     , t3101.c101_gem_id gem
                     , GET_USER_NAME(t3101.c101_gem_id) gemnm
                     , GET_USER_EMAILID(t3101.c101_gem_id) gemMailId
                     , t3101.c901_product_family type
                     , GET_CODE_NAME(t3101.c901_product_family) typenm
                     , t3101.c3101_lot_number lotNum
                     , t3101.c3101_qty quantity
                     , t3101.c901_loan_cons_type lonorConsign    
                     , DECODE(t3102.c3102_normal_wear_fl,'Y','Yes','N','No','') normalWearFl
			         , DECODE(t3102.c3102_event_surgery_fl,'Y','Yes','N','No','') eventSurgeyFl
			         , t3102.c3102_normal_wear_ds ifYesExplain
			         , t3102.c3102_type_of_surgery  typeOfSurgery
			         , DECODE(t3102.c3102_revisio_surgery_fl,'Y','Yes','N','No','') reviSurgery
			         , t3102.c3102_surgeon_nm surgeoonName
			        -- , DECODE(t3102.c3102_adverse_effect_fl,'Y','Yes','N','No','') adverseEffect
			         , t3102.c3102_hospital_nm hospitalName
			         , to_char(t3102.c3102_surgery_dt,get_rule_value ('DATEFMT', 'DATEFORMAT'))dateOfSurgery
			         , DECODE(t3102.c3102_post_opt_fl,'Y','Yes','N','No','') postOpeFlag
			         , to_char(t3102.c3102_post_opt_dt,get_rule_value ('DATEFMT', 'DATEFORMAT')) postOpeDate
			         , DECODE(t3102.c3102_event_demo_fl,'Y','Yes','N','No','') eventDemoFl
			         , DECODE(t3102.c3102_replacement_fl,'Y','Yes','N','No','') replacementFl
			         , DECODE(t3102.c3102_replacement_part_fl,'Y','Yes','N','No','') replacePartFl
			         , DECODE(t3102.c3102_part_eval_fl,'Y','Yes','N','No','') partAvailEvalFl
			         , DECODE(t3102.c3102_decontaminated_fl,'Y','Yes','N','No','') deconFl
			         , DECODE(t3102.c3102_shipped_fl,'Y','Yes','N','No','') shippedFl
			         , t3102.c3102_other_details otherDetails
			         , GET_USER_NAME(p_user_id) username
			         , to_char(t3103.C3103_PART_RECEIVE_DT,get_rule_value ('DATEFMT', 'DATEFORMAT')) partRecvDt
			         ,t3100.c3100_address address
			         ,t3100.c3100_phone_number phoneNumber
			         ,t3100.c3100_company company
			         ,t3102.C3106_TRACKING_NUMBER trackingNumber
			         ,to_char(t3102.C3106_PART_SHIPPED_DT,get_rule_value ('DATEFMT', 'DATEFORMAT'))  dateOfShipped
			         ,t3102.C901_SHIP_CARRIER carrier
			         ,DECODE(t3102.C901_SERVICE_REQ_ID,'Y','Yes','N','No','')  reqFieldService
			         ,t3102.C901_EVENT_OCCURED_ID didThisEventHappenDuring
			         ,t3102.C901_PROC_OUTCOME_ID procedureOutcome
			         ,t3102.C901_ADV_EFFECT_ID adverseEffect 
    			  FROM t3100_incident t3100,t3101_incident_part_detail t3101 , t3102_incident_detail t3102, t3103_incident_qa_eval t3103
			     WHERE t3100.c3100_incident_id = UPPER(p_incedent_id)
                   AND t3100.c3100_incident_id = t3101.c3100_incident_id
                   AND t3100.c3100_incident_id = t3102.c3100_incident_id
                   AND t3100.c3100_incident_id = t3103.c3100_incident_id(+)
                   AND T3100.C901_ISSUE_TYPE = NVL(p_issue_type,T3100.C901_ISSUE_TYPE)
                   AND t3100.c3100_void_fl IS NULL
                   AND t3101.c3101_void_fl IS NULL
                   AND t3102.c3102_void_fl IS NULL
                   AND t3103.c3103_void_fl IS NULL;
	END gm_fch_incident_info;
	
	/****************************************************************
    * Description : Procedure is to fecth the QA Evaluation Details
    * Author      : arajan
    *****************************************************************/
	PROCEDURE gm_fch_QAEvaluation (
		p_ref_id	IN		t3103_incident_qa_eval.c3100_incident_id%type,
		p_out_data	OUT		TYPES.cursor_type
	)
	AS
		
	BEGIN
		OPEN p_out_data FOR
			SELECT t3103.c3100_incident_id comrsrid, t3103.c3103_dhr_ncmr_rev dhrncmrrev
					, t3103.c3103_capa_scar_rev capascarrev, t3103.c3103_comp_review comprev
				 	, t3103.c101_reviewed_by revcompdby
				 	, to_char(t3103.c3103_review_dt,get_rule_value ('DATEFMT', 'DATEFORMAT')) dtrevcomp
				 	, to_char(t3103.c3103_part_receive_dt,get_rule_value ('DATEFMT', 'DATEFORMAT')) dtpartrec
				 	, to_char(t3103.c3103_sent_to_eval_dt,get_rule_value ('DATEFMT', 'DATEFORMAT')) dtevalsend
				 	, DECODE(t3103.c3103_deviation_dhr_fl,'Y','Yes','N','No','NA','N/A') dhrdev
				 	, DECODE(t3103.c3103_questionnaire_fl,'Y','Yes','N','No','NA','N/A') repsurgans
				 	, DECODE(t3103.c3103_proper_storage_fl,'Y','Yes','N','No','NA','N/A') storage
				 	, DECODE(t3103.c3103_package_insert_fl,'Y','Yes','N','No','NA','N/A') utilizedpac	
				 	, DECODE(t3103.c3103_market_removal_fl,'Y','Yes','N','No','NA','N/A') removalreq	
				 	, DECODE(t3103.c3103_risk_analysis_fl,'Y','Yes','N','No','NA','N/A') riskanalysis
				 	, DECODE(t3103.c3103_ncmr_scar_fl,'Y','Yes','N','No','NA','N/A') ncmrscargen
				 	, t3103.c3103_ncmr_scar_no ncmrscarid
				 	, DECODE(t3103.c3103_capa_req_fl,'Y','Yes','N','No','NA','N/A') correctiveact 
				 	, t3103.c3103_capa_no capaid, t3103.c101_qa_reviewed_by qarevcompby
				 	, to_char(t3103.c3103_qa_review_dt,get_rule_value ('DATEFMT', 'DATEFORMAT')) dtcompleted
				 	, get_user_name(t3103.c101_reviewed_by) revcompdbynm
				 	, get_user_name(t3103.c101_qa_reviewed_by) qarevcompbynm
				 	, t3100.c901_status status
			FROM t3103_incident_qa_eval t3103, t3100_incident t3100
			WHERE t3103.c3100_incident_id = p_ref_id
			AND t3103.c3100_incident_id = t3100.c3100_incident_id
			AND t3103.c3103_void_fl IS NULL
			AND t3100.c3100_void_fl IS NULL;    
			
	END gm_fch_QAEvaluation;

	
	/******************************************************************
    * Description : Procedure to fetch the details of Engineering Evaluation
    * Author      : Arajan
    **********************************************************************/
	PROCEDURE gm_fch_EnggEvaluation(
    		 p_Incident_id     IN    T3100_INCIDENT.C3100_INCIDENT_ID%TYPE
    		,p_out_scar        OUT   TYPES.cursor_type
    	)
    	AS
    	BEGIN
	    OPEN p_out_scar FOR
	    	SELECT t3104.c3100_incident_id incidentId
	    	    , to_char(t3104.c3104_evaluator_rec_dt,get_rule_value ('DATEFMT', 'DATEFORMAT')) evalRecDt
		    	, t3104.c3104_evaluation_result evalResult
		    	, t3104.c101_evaluator_id EvalId
		    	, get_user_name(t3104.c101_evaluator_id) evalname
		    	, t3104.c901_com_rsr_disp_type comRsrDispNum
		    	, GET_CODE_NAME(t3104.c901_com_rsr_disp_type) comRsrDispTyp
		    	, t3104.c901_product_disp_type PdtDispTypNum
		    	, GET_CODE_NAME(t3104.c901_product_disp_type) PdtDispTyp
		    	, t3104.c3104_capa_comments capaComments
		    	, t3104.c3104_created_by createdBy
		    	, to_char(t3104.c3104_created_date,get_rule_value ('DATEFMT', 'DATEFORMAT')) createdDt
		    	, t3104.c3104_last_updated_by updatedBy
		    	, to_char(t3104.c3104_last_updated_date,get_rule_value ('DATEFMT', 'DATEFORMAT')) updatedDt
		    	,t3100.c901_status status
                ,get_code_name(t3100.C901_STATUS) statusnm
			FROM t3104_incident_eng_eval t3104 ,t3100_incident t3100
			WHERE t3104.c3100_incident_id = p_Incident_id AND t3104.c3100_incident_id = t3100.c3100_incident_id
			AND t3104.c3104_void_fl IS NULL
			AND t3100.c3100_void_fl IS NULL;
	    END gm_fch_EnggEvaluation;
	    
	/******************************************************************
    * Description : Procedure to fetch the Closure details 
    * Author      : Arajan
    **********************************************************************/   
	PROCEDURE gm_fch_close_com_rsr (
		p_ref_id	IN		t3103_incident_qa_eval.c3100_incident_id%type,
		p_out_data	OUT		TYPES.cursor_type
	)
	AS
		
	BEGIN
		OPEN p_out_data FOR
			SELECT t3105.c3100_incident_id incidentid
				, t3105.c101_closed_by closedby
				, to_char(t3105.c3105_closed_dt,get_rule_value ('DATEFMT', 'DATEFORMAT')) closuredt
				, t3105.c3105_corrective_action prvactions
				, t3105.c3105_closure_comments comments
				, t3105.c3105_created_by createdby
				, to_char(t3105.c3105_created_date,get_rule_value ('DATEFMT', 'DATEFORMAT')) createddt
				, t3105.c3105_last_updated_by updatedby
				, to_char(t3105.c3105_last_updated_date,get_rule_value ('DATEFMT', 'DATEFORMAT')) updateddt
			FROM t3105_incident_closure_dtl t3105
			WHERE t3105.c3100_incident_id = p_ref_id
			AND t3105.c3105_void_fl is null;
	END gm_fch_close_com_rsr;
	
		
		/****************************************************************
    * Description : Procedure is to fecth the COM/RSR Details
    * Author      : elango
    *****************************************************************/
	PROCEDURE gm_fetch_close_com_rsr(
		p_comrsr_id	IN		t3105_incident_closure_dtl.C3100_INCIDENT_ID%type,
		p_out_data	OUT		TYPES.cursor_type
	)
	AS	
		v_out_type VARCHAR(20);
	 BEGIN
		OPEN p_out_data 
		FOR
			  SELECT t3105.C3100_INCIDENT_ID closeCOMRSRID,
			        t3105.C101_CLOSED_BY closedBy,
			        to_char(t3105.C3105_CLOSED_DT ,get_rule_value ('DATEFMT', 'DATEFORMAT')) closedDt,
			        t3105.C3105_CORRECTIVE_ACTION prevComments,
			        t3105.C3105_CLOSURE_COMMENTS closeCmts, 
             	    t3100.C901_STATUS status,
             	    get_code_name(t3100.C901_STATUS) statusnm,
             	    t3100.C901_ISSUE_TYPE issueType
			 FROM t3105_incident_closure_dtl t3105, t3100_incident t3100
             WHERE t3105.c3100_incident_id = t3100.c3100_incident_id
     		 AND t3105.C3100_INCIDENT_ID = p_comrsr_id
          	 AND t3100.c3100_void_fl IS NULL	
          	 AND t3105.c3105_void_fl IS NULL ;
	END gm_fetch_close_com_rsr;
END gm_pkg_qa_com_rsr_rpt;
/