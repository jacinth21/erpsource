CREATE OR REPLACE PACKAGE body gm_pkg_qa_set_mapping_txn
IS
    --
    /****************************************************************
    * Description : Procedure used to split the set mapping upload bulk data and store to master table.
    * Author      :
    *****************************************************************/
PROCEDURE gm_sav_bulk_set_list_upload (
        p_set_id    IN t2084_set_mapping_upload_dtls.c207_set_id%type,
        p_input_str IN CLOB,
        p_action    IN t2084_set_mapping_upload_dtls.c901_action%type,
        p_comments  IN VARCHAR2,
        p_user_id   IN t2084_set_mapping_upload_dtls.c2084_last_updated_by%type,
        p_out_error_dtls OUT CLOB,
        p_out_mapping_str OUT CLOB)
AS
    v_string CLOB   := p_input_str;
    v_substring VARCHAR2 (4000) ;
    --
    v_pk_id       NUMBER;
    v_pnum        VARCHAR2 (20) ;
    v_qty         NUMBER;
    v_critical_fl CHAR (1) ;
    v_set_map_cnt NUMBER;
    --
    v_critical_qty NUMBER;
    v_inset_fl     CHAR (1) ;
    v_status       NUMBER := 108746; -- Saved.
    v_set_details_id t2084_set_mapping_upload_dtls.c208_set_details_id%TYPE;
    v_critical_tag t2084_set_mapping_upload_dtls.c2084_critical_tag%TYPE;
    v_critical_type t2084_set_mapping_upload_dtls.c901_critical_type%TYPE;
    v_unit_type t2084_set_mapping_upload_dtls.c901_unit_type%TYPE;
    v_new_record_fl t2084_set_mapping_upload_dtls.c2084_new_record_fl%TYPE;
    --
BEGIN
    
    -- to update part # - to handle
     UPDATE t2084_set_mapping_upload_dtls
    SET c2084_void_fl   = 'Y'
      WHERE c207_set_id = p_set_id;
    --
    WHILE INSTR (v_string, '|') <> 0
    LOOP
        v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
        v_string    := SUBSTR (v_string, INSTR (v_string, '|')    + 1) ;
        --
        v_pk_id         := NULL;
        v_pnum          := NULL;
        v_qty           := NULL;
        v_inset_fl      := NULL;
        v_critical_fl   := NULL;
        v_critical_qty  := NULL;
        v_critical_tag  := NULL;
        v_critical_type := NULL;
        v_unit_type     := NULL;
        --
        v_pnum          := SUBSTR (v_substring, 1, INSTR (v_substring, '^')            - 1) ;
        v_substring     := SUBSTR (v_substring, INSTR (v_substring, '^')               + 1) ;
        v_qty           := TO_NUMBER (SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1)) ;
        v_substring     := SUBSTR (v_substring, INSTR (v_substring, '^')               + 1) ;
        v_inset_fl      := SUBSTR (v_substring, 1, INSTR (v_substring, '^')            - 1) ;
        v_substring     := SUBSTR (v_substring, INSTR (v_substring, '^')               + 1) ;
        v_critical_fl   := SUBSTR (v_substring, 1, INSTR (v_substring, '^')            - 1) ;
        v_substring     := SUBSTR (v_substring, INSTR (v_substring, '^')               + 1) ;
        v_critical_qty  := SUBSTR (v_substring, 1, INSTR (v_substring, '^')            - 1) ;
        v_substring     := SUBSTR (v_substring, INSTR (v_substring, '^')               + 1) ;
        v_critical_tag  := SUBSTR (v_substring, 1, INSTR (v_substring, '^')            - 1) ;
        v_substring     := SUBSTR (v_substring, INSTR (v_substring, '^')               + 1) ;
        v_critical_type := SUBSTR (v_substring, 1, INSTR (v_substring, '^')            - 1) ;
        v_substring     := SUBSTR (v_substring, INSTR (v_substring, '^')               + 1) ;
        v_unit_type     := SUBSTR (v_substring, 1, INSTR (v_substring, '^')            - 1) ;
        v_substring     := SUBSTR (v_substring, INSTR (v_substring, '^')               + 1) ;
        --
        --Part ^ Qty ^ Inset Fl ^ Critical Fl ^ Critical Qty ^ Critical Type ^ Critical Tag^ Unit Type
        -- to get the set details id
        BEGIN
             SELECT c208_set_details_id, 'U'
               INTO v_set_details_id, v_new_record_fl
               FROM t208_set_details
              WHERE c207_set_id         = p_set_id
                AND c205_part_number_id = v_pnum
                AND c208_void_fl       IS NULL;
        EXCEPTION
        WHEN OTHERS THEN
            v_set_details_id := NULL;
            v_new_record_fl  := 'I';
        END;
        --
        gm_pkg_qa_set_mapping_txn.gm_sav_set_mapping_upload_dtls (p_set_id, v_pnum, v_qty, v_critical_fl, v_critical_qty, v_inset_fl, p_action,
        v_status, v_new_record_fl, v_set_details_id, v_critical_tag, v_critical_type, v_unit_type, p_user_id) ;
        --
    END LOOP;
    --
    gm_pkg_qa_set_mapping_txn.gm_validate_part_number (p_set_id) ;
    --
    IF (p_action = 108741) -- Add/Modify
        THEN
        gm_pkg_qa_set_mapping_txn.gm_validate_critical_fl (p_set_id) ;
        gm_pkg_qa_set_mapping_txn.gm_create_set_upload_input_str_dtls (p_set_id, p_user_id, p_out_mapping_str) ;
    ELSE
        gm_pkg_qa_set_mapping_txn.gm_remove_set_mapping (p_set_id, p_comments, p_user_id, p_out_mapping_str) ;
    END IF;
    --
    IF p_out_mapping_str IS NULL THEN
        gm_pkg_qa_set_mapping_rpt.gm_fch_set_mapping_upload_dtls (p_set_id, 108745, p_out_error_dtls) ;
    END IF;
    --
END gm_sav_bulk_set_list_upload;

--
/****************************************************************
* Description : Procedure used to save the set mapping data to master table.
* Author      :
*****************************************************************/
PROCEDURE gm_sav_set_mapping_upload_dtls (
        p_set_id         IN t2084_set_mapping_upload_dtls.c207_set_id%TYPE,
        p_part_number_id IN t2084_set_mapping_upload_dtls.c205_part_number_id%TYPE,
        p_set_qty        IN t2084_set_mapping_upload_dtls.c2084_set_qty%TYPE,
        p_critical_fl    IN t2084_set_mapping_upload_dtls.c2084_critical_fl%TYPE,
        p_critical_qty   IN t2084_set_mapping_upload_dtls.c2084_critical_qty%TYPE,
        p_inset_fl       IN t2084_set_mapping_upload_dtls.c2084_inset_fl%TYPE,
        p_action         IN t2084_set_mapping_upload_dtls.c901_action%TYPE,
        p_status         IN t2084_set_mapping_upload_dtls.c901_status%TYPE,
        p_new_record_fl  IN t2084_set_mapping_upload_dtls.c2084_new_record_fl%TYPE,
        p_set_details_id IN t2084_set_mapping_upload_dtls.c208_set_details_id%TYPE,
        p_critical_tag   IN t2084_set_mapping_upload_dtls.c2084_critical_tag%TYPE,
        p_critical_type  IN t2084_set_mapping_upload_dtls.c901_critical_type%TYPE,
        p_unit_type      IN t2084_set_mapping_upload_dtls.c901_unit_type%TYPE,
        p_user_id        IN t2084_set_mapping_upload_dtls.c2084_last_updated_by%TYPE)
AS

BEGIN
    --
     UPDATE t2084_set_mapping_upload_dtls
    SET c2084_set_qty                      = p_set_qty, c2084_critical_fl = p_critical_fl, c2084_critical_qty = p_critical_qty
      , c2084_inset_fl                     = p_inset_fl, c901_action = p_action, c901_status = p_status
      , c2084_new_record_fl                = p_new_record_fl, c208_set_details_id = p_set_details_id
      , c2084_critical_tag                 = p_critical_tag, c901_critical_type = p_critical_type, c901_unit_type = p_unit_type
      , c2084_invalid_part_fl              = NULL, c2084_invalid_critical_fl = NULL, c2084_void_fl = NULL
      , c2084_last_updated_by              = p_user_id, c2084_last_updated_date = CURRENT_DATE
      WHERE C207_SET_ID                    = p_set_id
        AND C205_PART_NUMBER_ID            = p_part_number_id;
     --   
    IF (SQL%ROWCOUNT                       = 0) THEN
         INSERT
           INTO t2084_set_mapping_upload_dtls
            (
                c207_set_id, c205_part_number_id, c2084_set_qty
              , c2084_critical_fl, c2084_critical_qty, c2084_inset_fl
              , c901_action, c901_status, c2084_new_record_fl
              , c208_set_details_id, c2084_critical_tag, c901_critical_type
              , c901_unit_type, c2084_last_updated_by, c2084_last_updated_date
            )
            VALUES
            (
                p_set_id, p_part_number_id, p_set_qty
              , p_critical_fl, p_critical_qty, p_inset_fl
              , p_action, p_status, p_new_record_fl
              , p_set_details_id, p_critical_tag, p_critical_type
              , p_unit_type, p_user_id, CURRENT_DATE
            ) ;
    END IF;
    --
END gm_sav_set_mapping_upload_dtls;
--
/****************************************************************
* Description : Procedure used to validate the part number and update incorrect parts
* Author      :
*****************************************************************/
PROCEDURE gm_validate_part_number
    (
        p_set_id IN T2084_SET_MAPPING_UPLOAD_DTLS.C207_SET_ID%TYPE
    )
AS

BEGIN
	--
     UPDATE t2084_set_mapping_upload_dtls
    SET c2084_invalid_part_fl        = 'Y', c901_status = 108745 -- Validation Failed
      , c2084_new_record_fl          = NULL
      WHERE c207_set_id              = p_set_id
        AND C205_part_number_id NOT IN
        (
             SELECT t205.c205_part_number_id
			   FROM t205_part_number t205, t2084_set_mapping_upload_dtls t2084
			  WHERE t205.c205_part_number_id = t2084.c205_part_number_id
			    AND t2084.C207_SET_ID = p_set_id
			    AND t2084.c2084_void_fl     IS NULL
        )
        AND c2084_void_fl IS NULL;
      --  
END gm_validate_part_number;
--
/****************************************************************
* Description : Procedure used to validate critical flag and update incorrect data.
* Author      :
*****************************************************************/
PROCEDURE gm_validate_critical_fl (
        p_set_id IN T2084_SET_MAPPING_UPLOAD_DTLS.C207_SET_ID%TYPE)
AS
    v_current_set_critical_cnt NUMBER;
    v_new_set_critical_cnt     NUMBER;
    --
    v_critical_part_number_id t205_part_number.c205_part_number_id%TYPE;
    --
BEGIN
	--
     SELECT COUNT (1)
       INTO v_current_set_critical_cnt
       FROM t208_set_details t208
      WHERE t208.c207_set_id          = p_set_id
        AND t208.c208_critical_fl = 'Y'
        AND t208.c208_void_fl    IS NULL;
     --   
     
      BEGIN
	      SELECT c205_part_number_id
	      INTO v_critical_part_number_id
	       FROM t208_set_details t208
      WHERE t208.c207_set_id          = p_set_id
        AND t208.c208_critical_fl = 'Y'
        AND t208.c208_void_fl    IS NULL;
        --
       EXCEPTION WHEN OTHERS
       THEN
       		v_critical_part_number_id := '-999';
       END;
        
     SELECT COUNT (1)
       INTO v_new_set_critical_cnt
       FROM t2084_set_mapping_upload_dtls
     WHERE  c207_set_id                 = p_set_id
        AND c205_part_number_id <> v_critical_part_number_id
        AND c2084_critical_fl       = 'Y'
        AND c2084_void_fl          IS NULL;
     -- 
       
    IF (v_current_set_critical_cnt <> 0 AND (v_new_set_critical_cnt <> 0 OR v_new_set_critical_cnt > 1))
    THEN
    	--
         UPDATE t2084_set_mapping_upload_dtls
        SET c2084_invalid_critical_fl = 'Y', c901_status = 108745 -- Validation Failed
          , c2084_new_record_fl       = NULL
          WHERE c207_set_id           = p_set_id
            AND c2084_critical_fl     = 'Y'
            AND c2084_void_fl        IS NULL;
    END IF;
    --
END gm_validate_critical_fl;
--
/****************************************************************
* Description : Procedure used to create the set mapping input string
* Author      :
*****************************************************************/
PROCEDURE gm_create_set_upload_input_str_dtls (
        p_set_id  IN T2084_SET_MAPPING_UPLOAD_DTLS.C207_SET_ID%TYPE,
        p_user_id IN T2084_SET_MAPPING_UPLOAD_DTLS.C2084_LAST_UPDATED_BY%TYPE,
        p_out_mapping_str OUT CLOB)
AS
    v_failed_cnt NUMBER := 108745; -- Failed
    v_status     NUMBER;
BEGIN
     SELECT COUNT (1)
       INTO v_failed_cnt
       FROM t2084_set_mapping_upload_dtls
      WHERE c207_set_id        = p_set_id
        AND c901_status    = 108745 -- Validation Failed
        AND c2084_void_fl IS NULL;
    --    
    IF v_failed_cnt        = 0 THEN
        v_status          := 108747; -- Linked to Set details
        --
        gm_pkg_qa_set_mapping_txn.gm_create_set_map_inputstr (p_set_id, p_out_mapping_str) ;
        --
    END IF;
    --
    gm_pkg_qa_set_mapping_txn.gm_sav_set_mapping_upload (p_set_id, v_status, p_user_id) ;
    --
END gm_create_set_upload_input_str_dtls;
--
/****************************************************************
* Description : Procedure used to create the set mapping input string
* Author      :
*****************************************************************/
PROCEDURE gm_create_set_map_inputstr (
        p_set_id IN t2083_set_mapping_upload.c207_set_id%TYPE,
        p_out_mapping_str OUT CLOB)
AS

BEGIN
	--
	BEGIN
	--
     SELECT RTRIM (XMLAGG (XMLELEMENT (e, input_str || '|')) .EXTRACT ('//text()'), '|')
       INTO p_out_mapping_str
       FROM
        (
             SELECT c205_part_number_id ||'^' || c2084_set_qty || '^' || c2084_inset_fl || '^' || c2084_critical_fl ||
                '^' || c2084_critical_qty ||'^' || c901_critical_type ||'^' || c901_unit_type ||'^' ||
                c2084_critical_tag ||'^' || c2084_new_record_fl ||'^' ||c208_set_details_id input_str
               FROM t2084_set_mapping_upload_dtls
              WHERE c207_set_id    = p_set_id
                AND c901_status    = 108746 -- Saved
                AND c2084_void_fl IS NULL
        ) ;
     EXCEPTION WHEN OTHERS
     THEN
     	p_out_mapping_str := NULL;
     END;
     --
END gm_create_set_map_inputstr;

--
/****************************************************************
* Description : Procedure used to save/update set mapping upload data.
* Author      :
*****************************************************************/
PROCEDURE gm_sav_set_mapping_upload (
        p_set_id  IN T2083_SET_MAPPING_UPLOAD.C207_SET_ID%TYPE,
        p_status  IN T2083_SET_MAPPING_UPLOAD.C901_STATUS%TYPE,
        p_user_id IN T2083_SET_MAPPING_UPLOAD.C2083_LAST_UPDATED_BY%TYPE)
AS
BEGIN
	--
     UPDATE t2083_set_mapping_upload
    SET c901_status        = p_status
    	, c2083_last_updated_by = p_user_id
    	, c2083_last_updated_date = CURRENT_DATE
      WHERE c207_set_id    = p_set_id
        AND c2083_void_fl IS NULL;
    --    
    IF (SQL%ROWCOUNT       = 0) THEN
         INSERT
           INTO t2083_set_mapping_upload
            (
                c207_set_id, c901_status, c2083_last_updated_by
              , c2083_last_updated_date
            )
            VALUES
            (
                p_set_id, p_status, p_user_id
              , CURRENT_DATE
            ) ;
    END IF;
    --
END gm_sav_set_mapping_upload;
--
/****************************************************************
* Description : Procedure used to save set mapping bulk operations to log table
* Author      :
*****************************************************************/
PROCEDURE gm_sav_set_mapping_upload_log
    (
        p_set_id    IN t207_set_master.c207_set_id%TYPE,
        p_action_id IN t2084_set_mapping_upload_dtls.c901_status%TYPE,
        p_user_id   IN t2084_set_mapping_upload_dtls.c2084_last_updated_by%TYPE
    )
AS
    v_batch_id NUMBER;
    v_out_msg  VARCHAR2 (4000) ;
BEGIN
    -- to update the log
    -- ref id, comments, type , user id, out
    gm_update_log (p_set_id, 'Set List updated from bulk upload ('|| get_code_name (p_action_id) || ')', 1249,
    p_user_id, v_out_msg) ;
    --
     SELECT TO_CHAR (CURRENT_DATE, 'yyyyMMddhhMISS') INTO v_batch_id FROM dual;
    --
     INSERT
       INTO t2085_set_mapping_upload_dtls_log
        (
            c207_set_id, c205_part_number_id, c2085_set_qty
          , c2085_critical_fl, c2085_critical_qty, c2085_inset_fl
          , c901_action, c901_status, c2085_new_record_fl
          , c2085_batch_number, c208_set_details_id, c2085_critical_tag
          , c901_critical_type, c901_unit_type, c2085_last_updated_by
          , c2085_last_updated_date
        )
     SELECT c207_set_id, c205_part_number_id, c2084_set_qty
      , c2084_critical_fl, c2084_critical_qty, c2084_inset_fl
      , c901_action, c901_status, c2084_new_record_fl
      , v_batch_id, c208_set_details_id, c2084_critical_tag
      , c901_critical_type, c901_unit_type, p_user_id
      , CURRENT_DATE
       FROM t2084_set_mapping_upload_dtls
      WHERE c207_set_id    = p_set_id
        AND c2084_void_fl IS NULL;
    --
     DELETE FROM t2084_set_mapping_upload_dtls WHERE c207_set_id = p_set_id;
    -- 
END gm_sav_set_mapping_upload_log;

/****************************************************************
    * Description : Procedure used to remove the parts from set mapping details
    * Author      :
*****************************************************************/

PROCEDURE gm_remove_set_mapping (
        p_set_id  IN t2084_set_mapping_upload_dtls.c207_set_id%TYPE,
        p_comments  IN VARCHAR2,
        p_user_id IN t2084_set_mapping_upload_dtls.c2084_last_updated_by%TYPE,
        p_out_mapping_str OUT CLOB)
AS
	--
    v_failed_cnt NUMBER := 108745; -- Failed
    --
    v_status NUMBER;
    --
    CURSOR void_set_mapping_cur
    IS
         SELECT c208_set_details_id set_details_id
           FROM t2084_set_mapping_upload_dtls
          WHERE c207_set_id              = p_set_id
            AND c901_action          = 108742 -- Remove
            AND c208_set_details_id IS NOT NULL
            AND c2084_void_fl       IS NULL;
            
BEGIN
	--
     SELECT COUNT (1)
       INTO v_failed_cnt
       FROM t2084_set_mapping_upload_dtls
      WHERE c207_set_id        = p_set_id
        AND c901_status    = 108745
        AND c2084_void_fl IS NULL;
     --   
    IF v_failed_cnt        = 0 THEN
    	--
        v_status          := 108748; -- Linked to Set details
        --
        p_out_mapping_str := 'Void';
        --
        FOR remove_set_map IN void_set_mapping_cur
        LOOP
			-- 90898	Void Set Part Mapping
			-- 92361	Order Wrong parts

        	-- txn id, reason, cancel type, comments, user id
            gm_pkg_common_cancel.gm_cm_sav_cancelrow (remove_set_map.set_details_id, 92361, 'VSPMP', p_comments, p_user_id) ;
            --
        END LOOP;
        --
    END IF;
    --
    gm_pkg_qa_set_mapping_txn.gm_sav_set_mapping_upload (p_set_id, v_status, p_user_id) ;
    --
END gm_remove_set_mapping;


    /****************************************************************
	* Description : Procedure used for set link copy. Copying one set to another set. 
	* and voiding to set values and inserting new values comes from set values
	* Author      : Jgurunathan
	*****************************************************************/

	PROCEDURE gm_qa_sav_set_list_copy(
		p_from_set_id IN t208_set_details.c207_set_id%TYPE,
		p_to_set_id   IN t208_set_details.c207_set_id%TYPE,
		p_user_id     IN t208_set_details.c208_last_updated_by%TYPE,
		p_comments    IN VARCHAR2 )
	AS
		v_message          	VARCHAR2 (1000);
		v_error_msg    		VARCHAR2 (1000);
		v_from_set_cnt NUMBER;
		v_set_cnt NUMBER;
	BEGIN

		SELECT count(1) INTO v_set_cnt
		FROM t207_set_master
		WHERE c207_set_id IN (p_from_set_id, p_to_set_id)
		AND c207_void_fl  IS NULL;
				
		-- When the Set count is not 2, then one of the set ID passed in is incorrect.
		IF(v_set_cnt <> 2)
		THEN		
			raise_application_error ('-20999', ' Entered Set ID does not exists. Please enter valid Set ID.');
		END IF;
		
		-- Get from set id count 
		SELECT count(1) INTO v_from_set_cnt
		FROM t208_set_details
		WHERE c207_set_id = p_from_set_id
		AND c208_void_fl  IS NULL;
		
		if (v_from_set_cnt =0)
		THEN
			raise_application_error ('-20999', ' No set Details available in '||p_from_set_id||'. Please enter valid Set ID.');
		END IF;

	IF v_from_set_cnt  > 0 THEN
	 
	 -- Update the from set id 
	  UPDATE t208_set_details
	  SET c208_void_fl         = 'Y',
		c208_last_updated_by   = p_user_id,
		c208_last_updated_date = CURRENT_DATE
	  WHERE c207_set_id        = p_to_set_id
	  AND c208_void_fl        IS NULL;
	  
	 
	  INSERT
	  INTO t208_set_details
		(
		  c208_set_details_id,
		  c207_set_id,
		  c205_part_number_id,
		  c208_critical_fl,
		  c208_critical_qty,
		  c208_critical_tag,
		  c208_inset_fl,
		  c208_set_qty,
		  c901_critical_type,
		  c901_unit_type,
		  c208_last_updated_by,
		  c208_last_updated_date
		)
	  SELECT s208_setmap_id.NEXTVAL,
		p_to_set_id,
		c205_part_number_id,
		c208_critical_fl,
		c208_critical_qty,
		c208_critical_tag,
		c208_inset_fl,
		c208_set_qty,
		c901_critical_type,
		c901_unit_type,
		p_user_id,
		CURRENT_DATE
	  FROM t208_set_details
	  WHERE C207_set_id =p_from_set_id
	  AND c208_void_fl IS NULL;

			-- 1249 Quality Set Mapping comments
	  gm_update_log (p_to_set_id, p_comments, 1249, p_user_id, v_message); 
	  gm_update_log (p_to_set_id, 'Set Details Copied from:'||p_from_set_id, 1249, p_user_id, v_message);
	  
	  
	END IF;        
	  
	END gm_qa_sav_set_list_copy;
	
END gm_pkg_qa_set_mapping_txn;
/