 /* Formatted on 2010/06/25 10:23 (Formatter Plus v4.8.0) */

--@"C:\Database\Packages\Quality\gm_pkg_qa_part_process.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_qa_part_process
IS
---
/******************************************************************
    * Description : Procedure to fetch the Part Process Report Data
    * Author      : HReddi
*******************************************************************/
    
    PROCEDURE gm_fch_part_process_rpt  (
    		p_vendor		  IN   T301_VENDOR.C301_VENDOR_ID%TYPE,
          	p_system_id       IN   T207_SET_MASTER.C207_SET_ID%TYPE,           
            p_project_id      IN   T205_PART_NUMBER.C202_PROJECT_ID%TYPE,
            p_part_num		  IN   T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE,          
            p_out_partprocess OUT  TYPES.cursor_type
      )
      AS
          stmt VARCHAR2(4000);
          v_company_id 		  t1900_company.C1900_COMPANY_ID%TYPE;
      BEGIN		
		  SELECT get_compid_frm_cntx()
			 INTO v_company_id
			 FROM DUAL;
	      -- Basic Query to get the Part Process Report Columns
	      stmt := 'SELECT t205f.c205f_process_validation_id ID
						, t205f.c205_part_number_id PARTNUM
						, get_partnum_desc(t205f.c205_part_number_id) PARTDESC
						, get_code_name(t205f.c901_type) TYPE
						, t205f.c205_validated_part_number_id VALIDATIONPART
						, get_vendor_name(t205f.c301_vendor_id) VENDOR
						, t205f.c205f_last_updated_by LASTUPDATEDBY
						, NVL(t205f.c205f_last_updated_date,t205f.c205f_created_date) LASTUPDATEDDATE
						, t205f.c205f_history_fl HISTFL	
					FROM t205f_part_process_validation t205f, t2023_part_company_mapping t2023
	       			WHERE t205f.c205f_void_fl IS NULL ';
	       			
	       	-- If Part Number Filter is not Empty, then part number Filter condition is added here.		
		    IF p_part_num IS NOT NULL THEN
		    	stmt := stmt || ' AND t205f.c205_part_number_id LIKE ('''||p_part_num||'%'') ';
		    END IF;	  
		    
		    -- If Project Filter is not Empty, then beased on the Project ID gets the Part Number and part number Filter condition is added here.	
		    IF p_project_id <> '0' THEN
		    	stmt := stmt || ' AND t205f.c205_part_number_id IN (SELECT C205_PART_NUMBER_ID ID 
																	  FROM T205_PART_NUMBER 
                                                                     WHERE C202_PROJECT_ID = '''||p_project_id||'''
															UNION	SELECT C205_PART_NUMBER_ID ID 
																	  FROM T209_GENERIC_PARTNUMBER	
																	 WHERE C202_PROJECT_ID = '''||p_project_id||''')';
		    END IF;	 
		    
		    -- If Vendor Filter is not Empty, then vendor ID Filter condition is added here.	
		    IF p_vendor <> 0  THEN
		    	stmt := stmt || ' AND t205f.c301_vendor_id ='||p_vendor;
		    END IF;	 
		    
		    -- If System Filter is not Empty, then gets the part No's based on the System ID and Part Num Filter condition is added here.	
		    IF p_system_id <> 0 THEN
		    	stmt := stmt || ' AND t205f.c205_part_number_id IN (SELECT distinct t4011.c205_part_number_id 
																	  FROM t4010_group t4010, t4011_group_detail t4011 
																	 WHERE t4010.c4010_group_id = t4011.c4010_group_id 
																	   AND t4010.c4010_void_fl IS NULL
																	   AND t4010.c207_set_id='''||p_system_id||''')';
		    END IF;
		    stmt := stmt || ' AND t205f.c205_part_number_id = t2023.c205_part_number_id ';
		    stmt := stmt || ' AND t2023.c2023_void_fl IS NULL AND t2023.c1900_company_id = '''||v_company_id||''' ';
		    stmt := stmt || ' ORDER BY t205f.c205_part_number_id';
		    
		 -- opening the OUT Cursor for getting the Part Process Report with the Variable 'stmt'.
		    OPEN p_out_partprocess FOR stmt;		    
	  END gm_fch_part_process_rpt;      
END gm_pkg_qa_part_process;

/