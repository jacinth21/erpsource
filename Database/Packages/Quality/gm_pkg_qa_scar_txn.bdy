--@"C:\Database\Packages\Quality\gm_pkg_qa_scar_txn.bdy";


CREATE OR REPLACE PACKAGE BODY gm_pkg_qa_scar_txn
IS
    /****************************************************************
    * Description : Procedure to save the Scar Information as a batch
    * Author      : Manikandan
    *****************************************************************/
PROCEDURE gm_sav_scar_info(
	 p_scar_id		    IN   T3160_SCAR.C3160_SCAR_ID%TYPE
   	, p_complaint       IN   T3160_SCAR.C3160_NCMR_COM_REF%TYPE
    , p_desc            IN   T3160_SCAR.C3160_SCAR_DESC%TYPE
    , p_assigned_to	    IN   T3160_SCAR.C301_ASSIGNED_TO%TYPE
    , p_issued_by	    IN   T3160_SCAR.C101_ISSUED_BY%TYPE
    , p_prevscar	    IN   T3160_SCAR.C3160_PREVIOUS_SCAR%TYPE
    , p_assigned_date   IN   T3160_SCAR.C3160_ASSIGNED_DT%TYPE
    , p_response_date   IN   T3160_SCAR.C3160_RESPONSE_DT%TYPE                   
    , p_close_date      IN   T3160_SCAR.C3160_CLOSE_DT%TYPE
    , p_info            IN   T3160_SCAR.C3160_RESPONSE%TYPE    
    , p_user_id         IN   T3160_SCAR.C3160_CREATED_BY%TYPE
    , p_followup_fl        IN   T3160_SCAR.C3160_FOLLOW_UP_FL%TYPE
    , p_estimetdFlUpDt   IN   T3160_SCAR.C3160_FOLLOW_UP_DT%TYPE
    , p_out_scar_id     OUT  T3160_SCAR.C3160_SCAR_ID%TYPE
)
            
 AS
 	v_scar_id	 T3160_SCAR.C3160_SCAR_ID%TYPE := p_scar_id;
 	v_message VARCHAR(4000);
 	
 BEGIN
	 		UPDATE T3160_SCAR
	 		SET  
	 			 c3160_ncmr_com_ref = p_complaint
	 			,c3160_scar_desc = p_desc
	 			,c301_assigned_to = DECODE(p_assigned_to,0,NULL,p_assigned_to)
	 			,c3160_assigned_dt = p_assigned_date
	 			,c3160_response_dt =  NVL(p_response_date,get_business_day(p_assigned_date,30))
	 			,c3160_close_dt = p_close_date
	 			,c3160_response = p_info
	 			,c101_issued_by = DECODE(p_issued_by,0,NULL,p_issued_by)
	 			,C3160_previous_scar = p_prevscar
	 			,c3160_last_updated_by = p_user_id
	 			,c3160_last_updated_date = sysdate
	 			,c901_status = DECODE(p_close_date,NULL,102779,102781)
	 			,c3160_follow_up_fl = DECODE(p_followup_fl,80130,'Y',80131,'N',NULL)
	 			,c3160_follow_up_dt = p_estimetdFlUpDt 
	 		WHERE c3160_scar_id = v_scar_id;
				 
			IF (SQL%ROWCOUNT = 0)
			THEN
 				gm_pkg_qa_com_rsr_txn.gm_populate_id_by_year('11','102789',v_scar_id);

 				INSERT INTO T3160_SCAR
						(C3160_SCAR_ID, C3160_NCMR_COM_REF, 				    
					     C3160_SCAR_DESC,C101_ISSUED_BY,C3160_PREVIOUS_SCAR
					     , C301_ASSIGNED_TO, C3160_ASSIGNED_DT , C3160_RESPONSE_DT,C3160_CLOSE_DT,
					     C3160_RESPONSE,C901_STATUS,C3160_CREATED_BY,C3160_CREATED_DATE,C3160_FOLLOW_UP_FL,C3160_FOLLOW_UP_DT 
					     )
				VALUES
						(v_scar_id,p_complaint,
						 p_desc,DECODE(p_issued_by,0,NULL,p_issued_by),p_prevscar
						 ,DECODE(p_assigned_to,0,NULL,p_assigned_to),p_assigned_date, NVL(p_response_date,get_business_day(p_assigned_date,30)),p_close_date,
						 p_info,DECODE(p_close_date,NULL,102779,102781), p_user_id, SYSDATE,DECODE(p_followup_fl,80130,'Y',80131,'N',NULL),p_estimetdFlUpDt
						);
				gm_update_log (v_scar_id, 'SCAR Initiated', 4000318, p_user_id, v_message);
			END IF;
			
			IF  p_close_date IS NOT NULL THEN
				gm_update_log (v_scar_id, 'SCAR Closed', 4000318, p_user_id, v_message);
			END IF;
			
		p_out_scar_id := v_scar_id;
END gm_sav_scar_info;


	/******************************************************************
	    * Description : Procedure to cancel SCAR
	    * Author      : arajan
	*******************************************************************/
	
	PROCEDURE gm_cancel_scar
	(
		p_txn_id IN T3160_SCAR.C3160_SCAR_ID%TYPE,
	    p_user_id  IN T3160_SCAR.C3160_LAST_UPDATED_BY%TYPE
	)
	AS 
		v_message VARCHAR(4000);
	BEGIN
			
		UPDATE t3160_scar
		SET c901_status = 102780 --Canceled
			, c3160_last_updated_by = p_user_id
			, c3160_last_updated_date = SYSDATE
		WHERE c3160_scar_id = p_txn_id
		AND c3160_void_fl IS NULL;
		
		-- Below code is used to save log for cancelled SCAR
		gm_update_log (p_txn_id, 'SCAR Cancelled', 4000318, p_user_id, v_message);
		
	END gm_cancel_scar;
	/******************************************************************
	    * Description : Procedure to Reopen SCAR
	    * Author      : mrajasekaran
	*******************************************************************/
	
	PROCEDURE gm_upd_reopen_scar
	(
		p_txn_id IN T3160_SCAR.C3160_SCAR_ID%TYPE,
	    p_user_id  IN T3160_SCAR.C3160_LAST_UPDATED_BY%TYPE
	)
	AS
		v_message VARCHAR(4000);
	
	BEGIN
		UPDATE t3160_scar
		SET 
			c901_status = 102779 --Open
			,c3160_last_updated_by = p_user_id
			, c3160_last_updated_date = SYSDATE
			, c3160_close_dt = NULL			
		WHERE c3160_scar_id = p_txn_id
		AND c3160_void_fl IS NULL;
		-- Below code is used to save log for Reopen SCAR
		gm_update_log (p_txn_id, 'SCAR Reopened', 4000318, p_user_id, v_message);
		
	END gm_upd_reopen_scar;
	
END gm_pkg_qa_scar_txn; 
/