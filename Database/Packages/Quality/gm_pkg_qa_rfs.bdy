 /* Createted on 23/08/2012 for MNTTASK-6165 */

--@"C:\Database\Packages\Quality\gm_pkg_qa_rfs.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_qa_rfs
IS
---
/****************************************************************
    * Description : Procedure to save the RFS details
    * Author      : APrasath
*****************************************************************/
    
   	PROCEDURE gm_qa_sav_rfs  (
          	p_country         IN   VARCHAR2,
            p_partnos         IN   CLOB,
            p_rfs_status	  IN   NUMBER,
            p_comments		  IN   VARCHAR2,
            p_userid          IN   T205_PART_NUMBER.C205_LAST_UPDATED_BY%TYPE,
            p_attr_type       IN   T205D_PART_ATTRIBUTE.C901_ATTRIBUTE_TYPE%TYPE,
            p_message         OUT  VARCHAR2,
            p_invalid_parts   OUT  CLOB,
            p_out_comments	  OUT  VARCHAR2,
            p_out_partinfo    OUT  TYPES.cursor_type
      )
      AS
            v_invalid_parts  		 CLOB;
            v_valid_parts  		 	 CLOB;
            v_country_code  		 VARCHAR2 (100);
            v_comments		         VARCHAR2 (4000) := p_comments;
            v_string                 VARCHAR2 (4000) := p_country;
            v_substring              VARCHAR2 (1000) ;
            v_msg                    VARCHAR2 (1000) ;
            v_message                VARCHAR2 (1000) ;
            v_country			     VARCHAR2 (4000) := p_country;
            v_country_names			 VARCHAR2 (4000) ;
            v_part_count             NUMBER;
            v_action                 VARCHAR2(10) ;
            --When Updating RFS Updates needs to be fired so taking part numbers that has been changed
            cursor c_part
             is 
              select c205_part_number_id pnum
              FROM v_clob_list v_list, t205_part_number t205 
		             WHERE t205.c205_part_number_id = v_list.token  and t205.c205_active_fl = 'Y';
      BEGIN
	      	--set the partnos into clob temp table
            my_context.set_my_cloblist(p_partnos);
            
             SELECT COUNT ( *)
			   INTO v_part_count
			   FROM v_clob_list, t205_part_number
			  WHERE c205_part_number_id = token;
			  
		   IF v_part_count > 0
		   THEN
            --replace the | with , on country ids
            v_country := SUBSTR(REPLACE(v_country,'|',','),1,LENGTH(v_country)-1);
            my_context.set_my_inlist_ctx (v_country);
            --get the country names with , separator 
		            select RTRIM(XMLAGG(XMLELEMENT(e,get_code_name(token) || ',')).EXTRACT('//text()').getclobval(),',') 
		             into v_country_names from v_in_list;
                        
            --70171 - Yes , 70172 - No Codegrp - RFSYRN and  adding out messages 
            IF p_rfs_status = '70171' THEN
            	v_msg:=GET_RULE_VALUE('ADDED','RFS_MESSAGES')||'('|| v_country_names || ')' ;
            	v_message:= GET_RULE_VALUE('RELEASED','RFS_MESSAGES')||' : ' || v_country_names;
            ELSIF p_rfs_status = '70172' THEN
            	v_msg:=GET_RULE_VALUE('REMOVED','RFS_MESSAGES')||'('|| v_country_names || ')' ;
            	v_message:= GET_RULE_VALUE('UNRELEASED','RFS_MESSAGES')||' : ' || v_country_names;
            END IF;
            
            IF v_comments IS NOT NULL THEN
            	v_msg:=v_msg||' - '||v_comments;
            END IF;
            
            --to save the log details
            gm_sav_rfs_log_details(p_userid, p_rfs_status, p_attr_type, p_comments);
           
            --This procedure will load the Part Number and the Country to which the RFS Is done.
            gm_sav_part_rfs_mapping(p_userid, p_rfs_status);
            
            gm_fch_part_rfs_company_det(p_userid, p_rfs_status);
        	--iterating the country loop, it will run based on each country    
        	WHILE INSTR (v_string, '|') <> 0
        		LOOP
            	v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
            	v_string    := SUBSTR (v_string, INSTR (v_string, '|')    + 1) ;
            	v_country_code 		:= NULL;
            	v_country_code      := v_substring ;
                      
            
            IF p_rfs_status = '70171' THEN
            --if the rfs dropdown status 'Yes' then save the information
            	gm_sav_rfs_part_attr(p_attr_type, v_country_code, p_userid);
            ELSIF p_rfs_status = '70172' THEN
            --if the rfs dropdown status 'No' then void the information
            	gm_void_rfs_part_attr(p_attr_type, v_country_code, p_userid);
			END IF;

			
			END LOOP; --while
			
          END IF; -- valid part  - v_part_count
          
         	--fetching the part info
         	gm_fch_rfs_part_details(p_out_partinfo);
        
            --idenfying the invalid part nos and fetch the list of invalid part nos with , separator
		            SELECT RTRIM(XMLAGG(XMLELEMENT(e,token || ',')).EXTRACT('//text()').getclobval(),',')  
		              INTO v_invalid_parts 
		              FROM v_clob_list 
		             WHERE not exists (select c205_part_number_id from t205_part_number where c205_part_number_id = token ) ; 
            
            --assign the invalid part nos into out variable  
            IF v_invalid_parts IS NOT NULL THEN
               	p_invalid_parts := v_invalid_parts;
            ELSE
            	p_invalid_parts := 'Not Available';
            END IF;
         
         	p_out_comments := v_msg;
         	p_message      := v_message;
                    
--Updating t2001_master_data_updates for Part Updates
         	IF p_rfs_status = '70171' THEN
             	v_action := '103123';
            ELSIF p_rfs_status = '70172' THEN
            	v_action := '4000412';
			END IF;
         	
         	for v_cur in c_part
         	loop
         	 	gm_pkg_pdpc_prodcattxn.gm_sav_master_data_upd(v_cur.pnum  --4000827-->Part Attribute
							     , '4000827', v_action 
							     ,'103097','103100'
							     ,p_userid
							     ,null
							     ,'Y'
							     );
         	end loop;
         	
  	END gm_qa_sav_rfs ;

	/************************************************************************
	* Description : Procedure to save attribute value to T205D_PART_ATTRIBUTE
	*******************************************************************/
	PROCEDURE gm_sav_rfs_part_attr (
	    p_attr_type    IN		t205d_part_attribute.c901_attribute_type%TYPE
	  , p_attr_value   IN		t205d_part_attribute.c205d_attribute_value%TYPE
	  , p_user_id	   IN		t205_part_number.c205_last_updated_by%TYPE
	)
	AS
		
	BEGIN
		--update void flag=null for which are the part nos already available in t205d_part_attribute
		UPDATE t205d_part_attribute
		   SET c205d_void_fl = NULL 
		   ,C205D_UPDATE_FL = NULL--For Part Updates
			 , c205d_last_updated_date = CURRENT_DATE
			 , c205d_last_updated_by = p_user_id
		 WHERE EXISTS (SELECT token FROM v_clob_list WHERE token=  c205_part_number_id) 
		   AND c901_attribute_type = p_attr_type
		   AND c205d_attribute_value = p_attr_value
		   AND c205d_void_fl IS NOT NULL;

		
			--inserting new rocrds  for which are the part nos not available in t205d_part_attribute
			INSERT INTO t205d_part_attribute
						(c2051_part_attribute_id, c205_part_number_id, c901_attribute_type, c205d_attribute_value
					   , c205d_created_date, c205d_created_by
						)
				(SELECT s205d_part_attribute.NEXTVAL, token, p_attr_type, p_attr_value
					   , CURRENT_DATE, p_user_id FROM v_clob_list WHERE NOT EXISTS (select c205_part_number_id from t205d_part_attribute
		  				WHERE c901_attribute_type = p_attr_type AND c205d_attribute_value = p_attr_value and c205_part_number_id =token)
		  				AND EXISTS  (SELECT c205_part_number_id FROM  t205_part_number t205 WHERE token =t205.c205_part_number_id)
               );
               
               
		
	END gm_sav_rfs_part_attr;

	/************************************************************************
	* Description : Procedure to void attribute value to T205D_PART_ATTRIBUTE
	*******************************************************************/
	PROCEDURE gm_void_rfs_part_attr (
	    p_attr_type    IN		t205d_part_attribute.c901_attribute_type%TYPE
	  , p_attr_value   IN		t205d_part_attribute.c205d_attribute_value%TYPE
	  , p_user_id	   IN		t205_part_number.c205_last_updated_by%TYPE
	)
	AS
			    
    BEGIN
      
		--update void flag='Y'  which are the part nos not available in temp table
		UPDATE t205d_part_attribute
		   SET c205d_void_fl = 'Y'
		   ,C205D_UPDATE_FL = NULL--For Part Updates
			 , c205d_last_updated_date = CURRENT_DATE
			 , c205d_last_updated_by = p_user_id
		 WHERE  exists (SELECT token FROM v_clob_list where token =  c205_part_number_id) 
		   AND c901_attribute_type = p_attr_type
		   AND c205d_attribute_value = p_attr_value
		   AND c205d_void_fl IS NULL;
				
	END gm_void_rfs_part_attr;
	
	
 /*******************************************************
 * Purpose: This Procedure is used to fetch the
 * part details
 *******************************************************/
--
	PROCEDURE gm_fch_rfs_part_details (
		p_out_partinfo	 OUT	  TYPES.cursor_type
	)
	AS
	BEGIN
		--fatching partno,desc,status for which are the part nos in v_clob_list view
		OPEN p_out_partinfo
		 FOR
		 	 --PC-5470 Show Part numbers in the grid once RFS is updated
			 select c205_part_number_id part_num,regexp_replace(c205_part_num_desc,'[^ ()-/,��.0-9A-Za-z]', ' ') part_num_desc ,get_code_name(C901_STATUS_ID) status_desc,C901_STATUS_ID status,
			 get_code_name(C901_STATUS_ID) status_desc   
			 from t205_part_number where exists (
			 SELECT token FROM v_clob_list where token = c205_part_number_id);
			 
	END gm_fch_rfs_part_details;
	
	
 /*******************************************************
 * Purpose: This Procedure is used to save the
 * log details
 *******************************************************/
--
	PROCEDURE gm_sav_rfs_log_details (
		p_userid          IN   T205_PART_NUMBER.C205_LAST_UPDATED_BY%TYPE,
		p_rfs_status	  IN   NUMBER,
		p_attr_type       IN   T205D_PART_ATTRIBUTE.C901_ATTRIBUTE_TYPE%TYPE,
		p_comments		  IN   VARCHAR2
		
	)
	AS
	
		v_comments		  VARCHAR2 (4000) := p_comments;
		v_company_id t1900_company.c1900_company_id%TYPE;
	BEGIN	
		--
		SELECT get_compid_frm_cntx () INTO  v_company_id FROM DUAL;
		--
			IF v_comments IS NOT NULL THEN
            	v_comments:=' - '||v_comments;
            END IF;
			--1218(PartNum Setup) -- enter the log for each country
			
		 	--70171 - Yes , 70172 - No Codegrp - RFSYRN and  adding out messages 
            IF p_rfs_status = '70171' THEN
            
            	--fetch the country names for each for part which we are going to update or insert for relase and do the insert into log
            	INSERT INTO t902_log
								(c902_log_id, c902_ref_id, c902_comments, c902_type, c902_created_by
							   , c902_created_date, c1900_company_id) 
				select s507_log.NEXTVAL, part, msg||v_comments ,1218, p_userid, CURRENT_DATE, v_company_id  from ( 
            	select part,(GET_RULE_VALUE('ADDED','RFS_MESSAGES')||'(' || LISTAGG(country,'') within group (order by country) || ')') msg from
				(
				select clist.token part, get_code_name(list.token) country from v_clob_list clist ,v_in_list list
				where  NOT EXISTS(
				select c205_part_number_id part, c205d_attribute_value country from t205d_part_attribute t205d
				where clist.token = t205d.c205_part_number_id and list.token=t205d.c205d_attribute_value
				and c901_attribute_type=p_attr_type AND t205d.c205d_void_fl  IS NULL)
				AND EXISTS (SELECT c205_part_number_id
                             FROM t205_part_number t205
                            WHERE clist.token = t205.c205_part_number_id)
                )
			    group by part);
			
            ELSIF p_rfs_status = '70172' THEN
            
            	--fetch the country names for each for part which we are going to update or insert for relase  and do the insert into log
            	INSERT INTO t902_log
								(c902_log_id, c902_ref_id, c902_comments, c902_type, c902_created_by
							   , c902_created_date, c1900_company_id)
				select s507_log.NEXTVAL, part, msg||v_comments ,1218, p_userid, CURRENT_DATE, v_company_id from( 
            	select part,(GET_RULE_VALUE('REMOVED','RFS_MESSAGES') || '(' || LISTAGG( country,'') within group (order by country) || ')') msg from
				(
				select clist.token part, get_code_name(list.token) country from v_clob_list clist ,v_in_list list where exists
    			(SELECT c205_part_number_id part,c205d_attribute_value country   FROM t205d_part_attribute t205d   WHERE clist.token = t205d.c205_part_number_id
      				AND list.token=t205d.c205d_attribute_value   AND c901_attribute_type    = p_attr_type   AND t205d.c205d_void_fl   IS NULL)
       				AND EXISTS   (SELECT c205_part_number_id  FROM t205_part_number t205   WHERE clist.token = t205.c205_part_number_id ) 
				)group by part);
            	
            END IF;
		
	END gm_sav_rfs_log_details;
	
	/*******************************************************
 * Purpose: This Procedure is used to save the Part and the RFS Country
 * Based on the Data, system will enable the visibility of the Parts to those Companies.
 *******************************************************/
--
	PROCEDURE gm_sav_part_rfs_mapping (
		p_userid          IN   T205_PART_NUMBER.C205_LAST_UPDATED_BY%TYPE,
		p_rfs_status	  IN   NUMBER
	)
	AS
		v_count NUMBER;
	BEGIN	
		
		--70171 - Yes , 70172 - No Codegrp - RFSYRN and  adding out messages 
        IF p_rfs_status = '70171' 
        THEN
        
        	SELECT COUNT(1) INTO v_count
        	FROM 	T205H_PART_RFS_MAPPING
        	WHERE	C205_PART_NUMBER_ID IN (
			 				SELECT  part_list.token part
			 				FROM 	v_clob_list part_list 
			 									)
        	AND		 C901_RFS_COUNTRY_ID IN (
			 				SELECT  rfs_list.token part
			 				FROM 	v_in_list rfs_list 
		 										);
        	IF v_count >0 
        	THEN
        		UPDATE T205H_PART_RFS_MAPPING
		 		SET 	C205H_RFS_COMP_SYNC ='',
		 				C205H_PROJ_COMP_SYNC = '',
		 				C205H_SYSTEM_COMP_SYNC = '',
		 				C205H_UPDATED_BY = p_userid ,
		 				C205H_UPDATED_DATE = CURRENT_DATE
		 		WHERE   C205H_RFS_COMP_SYNC IS NOT NULL AND 
		 				C205_PART_NUMBER_ID IN (
			 				SELECT  part_list.token part
			 				FROM 	v_clob_list part_list 
			 									)
		 			AND C901_RFS_COUNTRY_ID IN (
			 				SELECT  rfs_list.token part
			 				FROM 	v_in_list rfs_list 
		 										)
		 			AND C205H_VOID_FL IS NULL;
	        	
	        ELSE
	        
				INSERT INTO T205H_PART_RFS_MAPPING(
							C205H_PART_RFS_LOG_ID,C205_PART_NUMBER_ID
							,C901_RFS_COUNTRY_ID,C205H_UPDATED_BY,C205H_UPDATED_DATE
							)
				SELECT 		S205H_PART_RFS_MAPPING.nextval, part_list.token part
							,rfs_list.token country,p_userid, CURRENT_DATE
				FROM 		v_clob_list part_list ,v_in_list rfs_list
					WHERE NOT EXISTS(
						SELECT  t205h.c205_part_number_id part, t205h.C901_RFS_COUNTRY_ID country 
						FROM 	T205H_PART_RFS_MAPPING t205h
						WHERE 	part_list.token = t205h.c205_part_number_id AND rfs_list.token=t205h.C901_RFS_COUNTRY_ID
						 AND 	t205h.c205h_void_fl  IS NULL
					 )
					 /*
					AND
					EXISTS (SELECT c205_part_number_id
	                             FROM t205_part_number t205
	                            WHERE clist.token = 205.c205_part_number_id)  */;
			END IF;
			
		  ELSIF p_rfs_status = '70172' 
		  THEN 
		 		UPDATE T205H_PART_RFS_MAPPING
		 		SET 	C205H_RFS_COMP_SYNC ='',
		 				C205H_PROJ_COMP_SYNC ='',
		 				C205H_SYSTEM_COMP_SYNC ='',
		 				C205H_UPDATED_BY = p_userid ,
		 				C205H_UPDATED_DATE = CURRENT_DATE
		 		WHERE   C205H_RFS_COMP_SYNC IS NOT NULL AND 
		 				C205_PART_NUMBER_ID IN (
			 				SELECT  part_list.token part
			 				FROM 	v_clob_list part_list 
			 									)
		 			AND C901_RFS_COUNTRY_ID IN (
			 				SELECT  rfs_list.token part
			 				FROM 	v_in_list rfs_list 
		 										)
		 			AND C205H_VOID_FL IS NULL;
		END IF;
		
	END gm_sav_part_rfs_mapping;
	
	
		/*******************************************************
		 * Purpose: This Procedure will be called from JMS, when a Part is RFS from Portal.
		 * Once the Part is RFS "T205H_PART_RFS_MAPPING" will be populated .
		 * Then we will have to have Visibility of the Part to the Country [ Company ].
		 * This Procedure will fetch the required data and finally insert records in T2023_PART_COMPANY_MAPPING
		 * to enable the Visibility of the Part to the Company.
		 * .
 		 *******************************************************/
--
	PROCEDURE gm_fch_part_rfs_company_det (
		p_userid          IN   T205_PART_NUMBER.C205_LAST_UPDATED_BY%TYPE,
		p_rfs_status	  IN   NUMBER
	)
	AS
	
	/* For the Parts that are RFS, get the P#, Company ID for the parts that does not exist in T2023_PART_COMPANY_MAPPING
	 * For all the parts that are matching the criteria, insert into T2023_PART_COMPANY_MAPPING to give visibility for the 
	 * parts to the Company. 
	 */
		CURSOR p_cur_part_comp IS
		
			SELECT 	C1900_COMPANY_ID compid, T205H.C205_PART_NUMBER_ID pnum ,T205H.C901_RFS_COUNTRY_ID countryid
			FROM 	T901C_RFS_COMPANY_MAPPING t901c,T205H_PART_RFS_MAPPING t205h
			WHERE 	T205H.C205H_VOID_FL IS NULL
			AND 	T901C.C901C_VOID_FL IS NULL
			AND 	T205H.C205H_RFS_COMP_SYNC IS NULL
			AND 	T205H.C901_RFS_COUNTRY_ID = T901C.C901_RFS_ID
			/*
			AND NOT EXISTS(
					SELECT C205_PART_NUMBER_ID pnum,C1900_COMPANY_ID compid 
					FROM 	T2023_PART_COMPANY_MAPPING T2023 
					WHERE-- T2023.C205_PART_NUMBER_ID='124.444'	AND 
							T2023.C205_PART_NUMBER_ID = T205H.C205_PART_NUMBER_ID
					AND 	T2023.C1900_COMPANY_ID = T901C.C1900_COMPANY_ID
					AND 	T2023.C2023_VOID_FL IS NULL
				)
				*/
			;
			
		BEGIN
			
			
				   
			FOR part_comp IN p_cur_part_comp
        	LOOP
        		gm_pkg_qa_rfs.gm_upd_part_company_map(part_comp.pnum,part_comp.compid,part_comp.countryid,p_rfs_status,p_userid );
        		
        		
        	END LOOP;
        	
	END gm_fch_part_rfs_company_det;
	
	/*******************************************************
	 * Purpose: This Procedure will be called from JMS , when ever the Part is Updated.
	 * We will get all the parts that are RFS done today.
	 * Loop through the parts and call the Part Company Visibility procedure.
	 * Author : Rajeshwaran
	 *******************************************************/
	PROCEDURE gm_sync_part_to_all_company(		
		p_userid        IN   T205_PART_NUMBER.C205_LAST_UPDATED_BY%TYPE
	)
	AS
		v_rfs_status NUMBER;
		v_created_company NUMBER;
		
		-- Get the Parts that are Modified Today, should pick voided records as well.
		CURSOR cur_part_info IS
		
			SELECT 	C205_PART_NUMBER_ID pnum,C205D_ATTRIBUTE_VALUE country_id,C205D_VOID_FL void_fl
			FROM  	T205D_PART_ATTRIBUTE 
			WHERE 	C901_ATTRIBUTE_TYPE='80180' 
			  AND 	C205D_UPDATE_FL IS NULL;

	BEGIN
		
		FOR part_info IN cur_part_info
        LOOP
        	--If its voided, the RFS Status will be No, else RFS Status = Yes.
        	SELECT DECODE(part_info.void_fl,'','70171','70172')
        	 INTO  v_rfs_status
        	 FROM  DUAL;
        	 
        	 --Get the Created Company of the part.
     BEGIN
        	 SELECT C1900_COMPANY_ID INTO v_created_company
        	   FROM 
        	   		T2023_PART_COMPANY_MAPPING 
        	   where 
        	   		C205_PART_NUMBER_ID=part_info.pnum 
        	   	AND C901_PART_MAPPING_TYPE=105360 
        	   	AND C2023_VOID_FL IS NULL;
    -- Continue is added in the below exception block to enable the JMS to continue to the next part for the Globus App Sync.
     EXCEPTION WHEN NO_DATA_FOUND THEN
     CONTINUE;
     END;
     
        	 
        	INSERT INTO MY_TEMP_PART_LIST(C205_PART_NUMBER_ID,C205_QTY) VALUES(part_info.pnum,part_info.country_id);
        	
        	gm_pkg_qa_rfs.gm_sav_part_comp_visibility(part_info.pnum,part_info.country_id,v_rfs_status,v_created_company,p_userid);
        END LOOP;
        
        /* As Part is Released / Un-released for Sale, Project and System visibility should be done as well.
         */
        /*gm_pkg_qa_rfs.gm_fch_part_proj_tosync();
         gm_pkg_qa_rfs.gm_fch_part_system_tosync();

        gm_pkg_qa_rfs.gm_sync_set_tocompany(p_userid);
        gm_pkg_qa_rfs.gm_sync_groups_tocompany(p_userid); */  
        
       --When updating RFS updates needs to be fired for IPAD  
        gm_pkg_qa_rfs.gm_sav_part_RFS_flag(p_userid);
                
        
        UPDATE t205d_part_attribute
		   SET c205d_update_fl = 'Y',
		   c205d_last_updated_by = p_userid, 
		   c205d_last_updated_date = CURRENT_DATE
   	     WHERE c901_attribute_type='80180' 
   	      AND c205_part_number_id IN (SELECT C205_PART_NUMBER_ID FROM MY_TEMP_PART_LIST)
		   AND c205d_update_fl IS NULL;
		   --
		   DELETE FROM MY_TEMP_PART_LIST;
	END gm_sync_part_to_all_company;
	
	/*******************************************************
	 * Purpose: This Procedure will be called from JMS, when a Part is RFS from Portal.
	 * Once the Part is RFS "T205H_PART_RFS_MAPPING" will be populated .
	 * Then we will have to have Visibility of the Part to the Country [ Company ].
	 * This Procedure will fetch the required data and finally insert records in T2023_PART_COMPANY_MAPPING
	 * to enable the Visibility of the Part to the Company.
	 * .
	 * Author : Rajeshwaran
	*******************************************************/
	PROCEDURE gm_sav_part_comp_visibility(
		p_partnum       IN   T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE,
		p_country_id	IN	 T205H_PART_RFS_MAPPING.C901_RFS_COUNTRY_ID%TYPE,
		p_rfs_status	IN   NUMBER,
		p_created_comp	IN   T1900_COMPANY.C1900_COMPANY_ID%TYPE,
		p_userid        IN   T205_PART_NUMBER.C205_LAST_UPDATED_BY%TYPE
	)
	AS
	
		-- Get the Company based on the RFS ID.
		CURSOR p_cur_part_comp IS
		
			SELECT 	T901c.C1900_COMPANY_ID compid
			FROM 	T901C_RFS_COMPANY_MAPPING t901c, T1900_COMPANY T1900 
			WHERE 	T901C.C901C_VOID_FL IS NULL
			AND 	T901C.C901_RFS_ID = p_country_id
			AND   	T901C.C1900_COMPANY_ID= T1900.C1900_COMPANY_ID
			AND   	T1900.C1900_PARENT_COMPANY_ID = p_created_comp
            AND 	T1900.C1900_VOID_FL IS NULL AND T1900.C901_COMP_STATUS='105320';
			
	BEGIN
		
		FOR part_comp IN p_cur_part_comp
        	LOOP
        		
        		-- Reset the Mapping ,so when parts are Un-released for sale, we will update those records first as Un-Released.
        		-- 10304547 - Unreleased.
        		 UPDATE T2023_PART_COMPANY_MAPPING
		  		SET 	C901_PART_MAPPING_TYPE = '10304547',
		  				C2023_UPDATED_DATE = CURRENT_DATE,
		  				C2023_UPDATED_BY = p_userid
		  		 WHERE 	C2023_VOID_FL IS NULL
				   AND 	C205_PART_NUMBER_ID = p_partnum
				   AND	C1900_COMPANY_ID = part_comp.compid
				   AND  C901_PART_MAPPING_TYPE = 105361;
				   --We need to void only the parts that are released, not the created type parts.
				   
				UPDATE T205H_PART_RFS_MAPPING
		 		SET 	C205H_PROJ_COMP_SYNC ='',
						C205H_SYSTEM_COMP_SYNC ='',
		 				C205H_UPDATED_BY = p_userid ,
		 				C205H_UPDATED_DATE = CURRENT_DATE
		 		WHERE  	C205_PART_NUMBER_ID = p_partnum
		          AND 	C205H_VOID_FL IS NULL;
		                
        		gm_pkg_qa_rfs.gm_sav_part_rfs_map(p_partnum,p_country_id,p_userid);
        		gm_pkg_qa_rfs.gm_upd_part_company_map(p_partnum,part_comp.compid,p_country_id,p_rfs_status,p_userid );
        		
        	END LOOP;
	
	END gm_sav_part_comp_visibility;
	
	/*******************************************************
	 * Purpose: This Procedure enabled the Company Visibility for the parts.
	 * Author : Rajeshwaran
	*******************************************************/
	
	PROCEDURE gm_upd_part_company_map(
		p_partnum       IN   T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE,
		p_company_id	IN   T1900_COMPANY.C1900_COMPANY_ID%TYPE,
		p_country_id	IN	 T205H_PART_RFS_MAPPING.C901_RFS_COUNTRY_ID%TYPE,
		p_rfs_status	IN   NUMBER,
		p_userid        IN   T205_PART_NUMBER.C205_LAST_UPDATED_BY%TYPE
	)
	AS
	BEGIN
		-- 105361 - Released.
		gm_pkg_qa_rfs.gm_sav_part_company_map(p_partnum, p_company_id, p_country_id,'105361',p_rfs_status, p_userid);
		
	
	END gm_upd_part_company_map;
	
	/*******************************************************
	 * Procedure to Update or Insert Records in Part RFS Mapping table.
	 * The Combination of the Part ID and Country is unique.
	* Author : Rajeshwaran
	*******************************************************/
	
	PROCEDURE gm_sav_part_rfs_map (
		p_partnum       IN   T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE,
		p_country_id	IN	 T205H_PART_RFS_MAPPING.C901_RFS_COUNTRY_ID%TYPE,
		p_userid        IN   T205_PART_NUMBER.C205_LAST_UPDATED_BY%TYPE
	)
	AS
	BEGIN
		
		UPDATE T205H_PART_RFS_MAPPING
 		SET 	C205H_RFS_COMP_SYNC ='',
 				C205H_PROJ_COMP_SYNC ='',
				C205H_SYSTEM_COMP_SYNC ='',
 				C205H_UPDATED_BY = p_userid ,
 				C205H_UPDATED_DATE = CURRENT_DATE
 		WHERE   C205H_RFS_COMP_SYNC IS NOT NULL AND 
 				C205_PART_NUMBER_ID = p_partnum
                AND C901_RFS_COUNTRY_ID = p_country_id
                AND C205H_VOID_FL IS NULL;
                        
		 IF (SQL%ROWCOUNT = 0)
    	 THEN
    	 
    	 	INSERT INTO T205H_PART_RFS_MAPPING(
						C205H_PART_RFS_LOG_ID,C205_PART_NUMBER_ID
						,C901_RFS_COUNTRY_ID,C205H_UPDATED_BY,C205H_UPDATED_DATE) 
			VALUES (S205H_PART_RFS_MAPPING.nextval,p_partnum,p_country_id,p_userid,CURRENT_DATE);    
    	 END IF;
    
	END gm_sav_part_rfs_map;
	
	
	/******************************************************* 
	 * This Procedure will check if the Part is available for the Passed in Company.
	 * If the Part is not available, we will Insert the Part for the Company , else we will update it.
	 * When RFS Status is set to "UnRelease" we will void the Part for the Company.
	 * Once done, we will set the Part Sync Flag .
	 * Author : Rajeshwaran
	*******************************************************/
	
	PROCEDURE gm_sav_part_company_map (
		p_partnum       IN   T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE,
		p_company_id	IN   T1900_COMPANY.C1900_COMPANY_ID%TYPE,
		p_country_id	IN	 T205H_PART_RFS_MAPPING.C901_RFS_COUNTRY_ID%TYPE,
		p_map_type		IN	 T2023_PART_COMPANY_MAPPING.C901_PART_MAPPING_TYPE%TYPE,
		p_rfs_status	IN   NUMBER,
		p_userid        IN   T205_PART_NUMBER.C205_LAST_UPDATED_BY%TYPE
	)
	AS
		v_count NUMBER;
	BEGIN
		
		IF p_rfs_status = '70171' 
        THEN 
			-- Check if the Part - Company Mapping is already available.
			SELECT 	COUNT(1)
			  INTO	v_count
			  FROM	T2023_PART_COMPANY_MAPPING T2023
			 WHERE  T2023.C205_PART_NUMBER_ID = p_partnum
			   AND	T2023.C1900_COMPANY_ID = p_company_id;
			
			-- If the Part - Company mapping is not available, then insert a record, else reset the void fl and make it active.
			IF (v_count = 0)
			THEN
				INSERT INTO T2023_PART_COMPANY_MAPPING (
							C2023_PART_COMP_MAP_ID,C205_PART_NUMBER_ID
							,C901_PART_MAPPING_TYPE
							,C1900_COMPANY_ID,C2023_UPDATED_DATE
							,C2023_UPDATED_BY)
				VALUES( 
							S2023_PART_COMPANY_MAPPING.nextval,p_partnum
							,p_map_type,p_company_id, CURRENT_DATE
							,p_userid
					   );
			ELSE
				UPDATE T2023_PART_COMPANY_MAPPING
		  		SET 	C901_PART_MAPPING_TYPE = DECODE(C901_PART_MAPPING_TYPE,105360,C901_PART_MAPPING_TYPE,p_map_type),
		  				C2023_UPDATED_DATE = CURRENT_DATE,
		  				C2023_UPDATED_BY = p_userid
		  		 WHERE 	C205_PART_NUMBER_ID = p_partnum
				   AND	C1900_COMPANY_ID = p_company_id;
				   
			END IF;
			
		 -- Unrelease from a Company.
		 -- 10304547 - UnReleased.	
		  ELSIF p_rfs_status = '70172' 
		  THEN
		  		UPDATE T2023_PART_COMPANY_MAPPING
		  		SET 	C901_PART_MAPPING_TYPE = '10304547',
		  				C2023_UPDATED_DATE = CURRENT_DATE,
		  				C2023_UPDATED_BY = p_userid
		  		 WHERE 	C2023_VOID_FL IS NULL
				   AND 	C205_PART_NUMBER_ID = p_partnum
				   AND	C1900_COMPANY_ID = p_company_id;
		  			
		 END IF;
			
		 	IF p_country_id IS NOT NULL
		 	THEN
			 	-- Once the Part - Company Mapping is set, we have to update the Sync Flag.
				UPDATE	T205H_PART_RFS_MAPPING
				   SET 	C205H_RFS_COMP_SYNC = 'Y'
					  , C205H_UPDATED_BY = p_userid  
					  , C205H_UPDATED_DATE =  CURRENT_DATE
				WHERE  C205H_VOID_FL IS NULL 
				  AND  C205H_RFS_COMP_SYNC IS NULL				
				  AND  C205_PART_NUMBER_ID = p_partnum			  
				  AND  C901_RFS_COUNTRY_ID = p_country_id;
			END IF;
			  
		
	END gm_sav_part_company_map;
	
	/******************************************************* 
	 * This Procedure will be called from JMS when ever RFS is Done for a Part.
	 * Get all the Parts for which the Project Sync fl is null.
	 * For the part get the Project and make the Project visible for the Company. 
	 * When a Part is Unreleased for Sale,we will get the Project for the Part.
	 * Check how many parts in the Project is visible for the Company.
	 * If atleast one part is visible for the Company, then the Project is also visible.
	 * If none of the parts are visible for the Company, then the Project will be removed for the Company.
	 
	 * Author : Rajeshwaran
	*******************************************************/
	
	PROCEDURE gm_fch_part_proj_tosync 
	AS
		v_user_id T205_PART_NUMBER.C205_LAST_UPDATED_BY%TYPE;
		
		CURSOR cur_proj_company
		IS
			SELECT  DISTINCT
					T901C.C1900_COMPANY_ID compid, T205.C202_PROJECT_ID projid
					, T205H.C205H_UPDATED_BY user_id , C901_RFS_COUNTRY_ID country_id
					--, T205H.C205_PART_NUMBER_ID pnum
			FROM 
					T205H_PART_RFS_MAPPING T205H , 
					T901C_RFS_COMPANY_MAPPING T901C,
					T205_PART_NUMBER T205,
					T2023_PART_COMPANY_MAPPING T2023, 
					T1900_COMPANY T1900
			WHERE 
					T205H.C901_RFS_COUNTRY_ID = T901C.C901_RFS_ID
				AND T205H.C205H_VOID_FL IS NULL
				AND T901C.C901C_VOID_FL IS NULL
				AND T205H.C205H_PROJ_COMP_SYNC IS NULL
				AND T205.C205_PART_NUMBER_ID = T205H.C205_PART_NUMBER_ID
				AND T2023.C205_PART_NUMBER_ID = T205H.C205_PART_NUMBER_ID
                AND T901C.C1900_COMPANY_ID = T1900.C1900_COMPANY_ID
                AND T1900.C1900_PARENT_COMPANY_ID = T2023.C1900_COMPANY_ID
                AND T2023.C901_PART_MAPPING_TYPE='105360'
				;
	
	BEGIN
			DELETE FROM MY_TEMP_DATA_VISIBILITY WHERE TEMP_TYPE='project';
			
			FOR proj_comp IN cur_proj_company
        	LOOP
        		gm_pkg_pd_project_trans.gm_sav_project_company_map(proj_comp.projid,proj_comp.compid,105361,proj_comp.user_id);
        		-- 105361 Released
        		
        		gm_pkg_qa_rfs.gm_chk_part_unreleased();
        		
        		INSERT INTO MY_TEMP_DATA_VISIBILITY(ID,COMPANY_ID,TEMP_TYPE,TEMP_ACTION)
        		VALUES (proj_comp.projid,proj_comp.compid,'project','released');
        			
        		v_user_id := proj_comp.user_id ;
        		
        	END LOOP;
        
        		UPDATE	T205H_PART_RFS_MAPPING
				   SET 	C205H_PROJ_COMP_SYNC = 'Y'
					  , C205H_UPDATED_BY = v_user_id 
					  , C205H_UPDATED_DATE =  CURRENT_DATE
				WHERE  C205H_VOID_FL IS NULL 
				  AND  C205H_PROJ_COMP_SYNC IS NULL				
				  AND  C205_PART_NUMBER_ID IN (SELECT C205_PART_NUMBER_ID FROM  MY_TEMP_PART_LIST)		  
				  AND  C901_RFS_COUNTRY_ID IN (SELECT C205_QTY FROM MY_TEMP_PART_LIST );
				  
				 
	END gm_fch_part_proj_tosync;

	
	/******************************************************* 
	 * For the part get the Project and make the Project visible for the Company. 
	 * When a Part is Unreleased for Sale,we will get the Project for the Part.
	 * Check how many parts in the Project is visible for the Company.
	 * If atleast one part is visible for the Company, then the Project is also visible.
	 * If none of the parts are visible for the Company, then the Project will be removed for the Company.
	 * Author : Rajeshwaran
	*******************************************************/
	 
	PROCEDURE gm_chk_part_unreleased 
	AS
		v_active_part_count NUMBER;
		v_project_id  T202_PROJECT.C202_PROJECT_ID%TYPE;
		
		/* Check how many parts are unreleased for the Company.
		 * 
		 */
		CURSOR cur_unreleased_part
		IS
			SELECT  DISTINCT
					T901C.C1900_COMPANY_ID compid,
					--T205H.C205_PART_NUMBER_ID pnum, 
					T205.C202_PROJECT_ID projid ,
					T205H.C205H_UPDATED_BY user_id
			FROM 
					T205H_PART_RFS_MAPPING T205H , 
					T901C_RFS_COMPANY_MAPPING T901C,
					T205_PART_NUMBER T205 , 
					T2023_PART_COMPANY_MAPPING T2023
			WHERE 	
					T205H.C901_RFS_COUNTRY_ID = T901C.C901_RFS_ID
			AND 	T205.C205_PART_NUMBER_ID = T205H.C205_PART_NUMBER_ID		
			AND 	T2023.C1900_COMPANY_ID = T901C.C1900_COMPANY_ID
			AND 	T2023.C205_PART_NUMBER_ID = T205H.C205_PART_NUMBER_ID
			AND 	T205H.C205H_PROJ_COMP_SYNC IS NULL
			AND 	T205H.C205H_VOID_FL IS NULL
			AND 	T901C.C901C_VOID_FL IS NULL
			AND 	T2023.C2023_VOID_FL  IS NULL
			AND     T2023.C901_PART_MAPPING_TYPE = '10304547' -- Check if the Part is Unreleased.
			;
		
	BEGIN
			FOR unreleased_part IN cur_unreleased_part
        	LOOP
        		-- Check how many parts are Available for the Company.
        		SELECT 	COUNT(1) INTO  
        				v_active_part_count
        		FROM 
						T2023_PART_COMPANY_MAPPING T2023 , T205_PART_NUMBER T205
				WHERE 	T205.C202_PROJECT_ID		=	unreleased_part.projid
                AND T205.C205_PART_NUMBER_ID = T2023.C205_PART_NUMBER_ID
                AND 	T2023.C1900_COMPANY_ID		=	unreleased_part.compid
                AND 	T2023.C901_PART_MAPPING_TYPE != '10304547'
                AND T2023.C2023_VOID_FL IS NULL;
				
				-- if no parts are avaialble for the Company, then remove the project visibility as well.
				IF v_active_part_count=0
				THEN
					gm_pkg_pd_project_trans.gm_void_project_company_map(unreleased_part.projid,unreleased_part.compid,unreleased_part.user_id);
					v_project_id := unreleased_part.projid;
					
					INSERT INTO MY_TEMP_KEY_VALUE(MY_TEMP_TXN_ID,MY_TEMP_TXN_KEY,MY_TEMP_TXN_VALUE)
        			VALUES (unreleased_part.projid,'un-released','project');
        			
        			INSERT INTO MY_TEMP_DATA_VISIBILITY(ID,COMPANY_ID,TEMP_TYPE,TEMP_ACTION)
        			VALUES (unreleased_part.projid,unreleased_part.compid,'project','un-released');
        		
        		END IF;
        		
        	END LOOP;
					
	END gm_chk_part_unreleased;
	
	/******************************************************* 
	 * This Procedure will get the System for which the Part is RFS.
	 * Get all the Companies for which the Part is RFS, and the System should
	 * be visible for all those Companies as well
	 * 
	  * Author : Rajeshwaran
	*******************************************************/
	PROCEDURE gm_fch_part_system_tosync 
	AS
		v_user_id T205_PART_NUMBER.C205_LAST_UPDATED_BY%TYPE;
		
		CURSOR cur_system_company
		IS
			 SELECT DISTINCT
					T901C.C1900_COMPANY_ID compid, T207.C207_SET_ID set_id
					, T205H.C205H_UPDATED_BY user_id , C901_RFS_COUNTRY_ID country_id
				--	, T205H.C205_PART_NUMBER_ID pnum
			FROM 
					T205H_PART_RFS_MAPPING T205H , 
					T901C_RFS_COMPANY_MAPPING T901C,
					T205_PART_NUMBER T205, 
                    T207_SET_MASTER T207,
                    T208_SET_DETAILS T208,
                    T2023_PART_COMPANY_MAPPING T2023,
                    T1900_COMPANY T1900
			WHERE 
					T205H.C901_RFS_COUNTRY_ID = T901C.C901_RFS_ID
				AND T205H.C205H_VOID_FL IS NULL
				AND T901C.C901C_VOID_FL IS NULL
				AND T205H.C205H_SYSTEM_COMP_SYNC IS NULL
				AND T205.C205_PART_NUMBER_ID = T205H.C205_PART_NUMBER_ID
                AND T207.C207_SET_ID = T208.C207_SET_ID
                AND T208.C205_PART_NUMBER_ID = T205.C205_PART_NUMBER_ID
                AND T208.C208_VOID_FL IS NULL
                AND T207.C207_VOID_FL IS NULL
                AND T207.C901_SET_GRP_TYPE=1600
                AND T901C.C1900_COMPANY_ID = T1900.C1900_COMPANY_ID
                AND T2023.C205_PART_NUMBER_ID = T205H.C205_PART_NUMBER_ID
                AND T2023.C1900_COMPANY_ID =  T1900.C1900_PARENT_COMPANY_ID
                AND T2023.C901_PART_MAPPING_TYPE='105360';
    
	BEGIN
			DELETE FROM MY_TEMP_DATA_VISIBILITY WHERE TEMP_TYPE='system';
			FOR sys_comp IN cur_system_company
        	LOOP
        		-- Enable the System to Company Mapping.
        		gm_pkg_pd_system_trans.gm_sav_system_company_map(sys_comp.set_id,sys_comp.compid,105361,sys_comp.user_id);
        		-- 105361 Released
        		
        		INSERT INTO MY_TEMP_DATA_VISIBILITY(ID,COMPANY_ID,TEMP_TYPE,TEMP_ACTION)
        		VALUES (sys_comp.set_id,sys_comp.compid,'system','released');
        			
        		-- If any Part is Un Released, then the System should not be visible for the Companies as well.
        		gm_pkg_qa_rfs.gm_chk_part_unreleased_system();
        		
        		v_user_id := sys_comp.user_id ;
        		
        	END LOOP;
        	
        	/* Moved this to new Procedure ,so not required
        	-- Sync all the Group Which belongs to the System and Map it to all Companies for which the System is available.
        	FOR cur_system IN cur_system_info
        	LOOP
        		gm_pkg_pd_group_txn.gm_sync_group_to_company('',cur_system.set_id,cur_system.compid,'105361',cur_system.user_id);
        	END LOOP;
        	*/
        	
        		UPDATE	T205H_PART_RFS_MAPPING
				   SET 	C205H_SYSTEM_COMP_SYNC = 'Y'
					  , C205H_UPDATED_BY = v_user_id 
					  , C205H_UPDATED_DATE =  CURRENT_DATE
				WHERE  C205H_VOID_FL IS NULL 
				  AND  C205H_SYSTEM_COMP_SYNC IS NULL				
				  AND  C205_PART_NUMBER_ID IN (SELECT C205_PART_NUMBER_ID FROM  MY_TEMP_PART_LIST)		  
				  AND  C901_RFS_COUNTRY_ID IN (SELECT C205_QTY FROM MY_TEMP_PART_LIST );
				  
				  DELETE FROM MY_TEMP_PART_LIST;
	END gm_fch_part_system_tosync;

	
	/******************************************************* 
	 * This Procedure will get the System for which the Part is Un-RFS.
	 * Get all the Companies for which the Part is UN-RFS, and get the count
	 * of parts that is available for the System.
	 * If there are no part available for the Country, then the System, Group 
	 * visibility should be removed.
	 * 
	  * Author : Rajeshwaran
	*******************************************************/
	
	PROCEDURE gm_chk_part_unreleased_system
	AS
		v_active_part_count NUMBER;
		
		/* Check how many parts are unreleased for the Company.
		 * 
		 */
		CURSOR cur_unreleased_part
		IS
			SELECT  DISTINCT
					T901C.C1900_COMPANY_ID compid, T207.C207_SET_ID set_id
					, T205H.C205H_UPDATED_BY user_id , C901_RFS_COUNTRY_ID country_id
					--, T205H.C205_PART_NUMBER_ID pnum
			FROM 
					T205H_PART_RFS_MAPPING T205H , 
					T901C_RFS_COMPANY_MAPPING T901C,
					T205_PART_NUMBER T205, 
                    T207_SET_MASTER T207,
                    T208_SET_DETAILS T208 ,
                    T2023_PART_COMPANY_MAPPING T2023
			WHERE 
					T205H.C901_RFS_COUNTRY_ID = T901C.C901_RFS_ID
                    AND T205.C205_PART_NUMBER_ID = T205H.C205_PART_NUMBER_ID
                AND T207.C207_SET_ID = T208.C207_SET_ID
                AND T205.C205_PART_NUMBER_ID = T205H.C205_PART_NUMBER_ID
                AND T2023.C1900_COMPANY_ID = T901C.C1900_COMPANY_ID
                AND T208.C205_PART_NUMBER_ID = T2023.C205_PART_NUMBER_ID
                AND T208.C205_PART_NUMBER_ID = T205.C205_PART_NUMBER_ID
				AND T207.C901_SET_GRP_TYPE=1600
				AND T205H.C205H_SYSTEM_COMP_SYNC IS NULL
				AND T2023.C901_PART_MAPPING_TYPE = '10304547' -- Check if the Part is Unreleased.-- Check if the Part is voided.
                AND T208.C208_VOID_FL IS NULL
                AND T207.C207_VOID_FL IS NULL
                AND T205H.C205H_VOID_FL IS NULL
				AND T901C.C901C_VOID_FL IS NULL
				AND T2023.C2023_VOID_FL  IS NULL;
	
	BEGIN
			FOR unreleased_part IN cur_unreleased_part
        	LOOP
        		-- Check how many parts are Available for the Company For the System.
        		 SELECT COUNT(1)
				   INTO v_active_part_count
				    FROM	 
						T205H_PART_RFS_MAPPING T205H ,
	                    T207_SET_MASTER T207,
	                    T2023_PART_COMPANY_MAPPING T2023 ,
	                    T208_SET_DETAILS T208
				 WHERE 
					 
                     T207.C901_SET_GRP_TYPE=1600
					AND T207.C207_SET_ID = T208.C207_SET_ID
                    AND T205H.C205_PART_NUMBER_ID =  T208.C205_PART_NUMBER_ID
                    AND T205H.C205_PART_NUMBER_ID = T2023.C205_PART_NUMBER_ID
                    AND T207.C207_SET_ID = unreleased_part.set_id
					AND T2023.C1900_COMPANY_ID = unreleased_part.compid
                    AND T2023.C901_PART_MAPPING_TYPE != '10304547' -- Check if the Part is Unreleased.
	                AND T208.C208_VOID_FL IS NULL
	                AND T207.C207_VOID_FL IS NULL
                    AND T205H.C205H_VOID_FL IS NULL
	                AND T2023.C2023_VOID_FL IS NULL;                
                
				
				-- if no parts are avaialble for the Company, then remove the project visibility as well.
				IF v_active_part_count=0
				THEN
				gm_pkg_pd_system_trans.gm_void_system_company_map(unreleased_part.set_id,unreleased_part.compid,unreleased_part.user_id);
				--gm_pkg_pd_group_txn.gm_remove_group_to_company(unreleased_part.set_id,unreleased_part.compid,unreleased_part.user_id);
				
				INSERT INTO MY_TEMP_DATA_VISIBILITY(ID,COMPANY_ID,TEMP_TYPE,TEMP_ACTION)
        		VALUES (unreleased_part.set_id,unreleased_part.compid,'system','un-released');
        		
				END IF;
        		
        	END LOOP;
			
	END gm_chk_part_unreleased_system;

	/*******************************************************
	 * Purpose: This Procedure will sync all the Sets that belongs to a Project, to
	 * all companies where the Project is visible for.
	 * When the Project is processed, we will get all the project ID in a temp table.
	 * This temp table will be looped to get all the set and will be synced to the Companies as well.
	 * 
	 * Author : Rajeshwaran
	*******************************************************/
	PROCEDURE gm_sync_set_tocompany(
		p_userid        IN   T205_PART_NUMBER.C205_LAST_UPDATED_BY%TYPE
	)
	AS
		CURSOR cur_set_tosync
		IS
			SELECT  DISTINCT
					T2021.C1900_COMPANY_ID COMPID, 
					T207.C207_SET_ID SET_ID,
					T2021.C202_PROJECT_ID PROJ_ID
			FROM
			    	T2021_PROJECT_COMPANY_MAPPING T2021, 
			    	MY_TEMP_DATA_VISIBILITY TEMP,
			    	T207_SET_MASTER T207
			WHERE
			        T2021.C202_PROJECT_ID = TEMP.ID
			AND     T2021.C2021_VOID_FL IS NULL
			AND     T207.C202_PROJECT_ID = to_char(T2021.C202_PROJECT_ID)
			AND     T207.C901_SET_GRP_TYPE = '1601'
			AND		TEMP.COMPANY_ID =  T2021.C1900_COMPANY_ID
			AND     TEMP.TEMP_TYPE = 'project'
			AND     TEMP.TEMP_ACTION = 'released'
			AND     T207.C207_VOID_FL IS NULL;

		CURSOR cur_set_toremove
		IS
			SELECT  DISTINCT
					T2021.C1900_COMPANY_ID COMPID, 
					T207.C207_SET_ID SET_ID,
					T2021.C202_PROJECT_ID PROJ_ID
			FROM
			    	T2021_PROJECT_COMPANY_MAPPING T2021, 
			    	MY_TEMP_DATA_VISIBILITY TEMP,
			    	T207_SET_MASTER T207
			WHERE
			        T2021.C202_PROJECT_ID = TEMP.ID
			        --should not include t2021.void fl check.
			AND     T207.C202_PROJECT_ID = to_char(T2021.C202_PROJECT_ID)
			AND     T207.C901_SET_GRP_TYPE = '1601'
			AND		TEMP.COMPANY_ID =  T2021.C1900_COMPANY_ID
			AND     TEMP.TEMP_TYPE = 'project'
			AND     TEMP.TEMP_ACTION = 'un-released'
			AND     T207.C207_VOID_FL IS NULL;			
	
	BEGIN
		
		FOR set_tosync IN cur_set_tosync
        	LOOP
        		gm_pkg_pd_system_trans.gm_sav_system_company_map(set_tosync.SET_ID, set_tosync.compid,
        														 105361,p_userid);
				DELETE FROM  MY_TEMP_DATA_VISIBILITY
				WHERE TEMP_TYPE = 'project'
				AND TEMP_ACTION = 'released'
				AND ID = set_tosync.PROJ_ID;
				
        	END LOOP;
		
        	
		FOR set_toremove IN cur_set_toremove
        	LOOP
        		gm_pkg_pd_system_trans.gm_void_system_company_map(set_toremove.SET_ID, set_toremove.compid,p_userid);

        		DELETE FROM  MY_TEMP_DATA_VISIBILITY
				WHERE TEMP_TYPE = 'project'
				AND TEMP_ACTION = 'un-released'
				AND ID = set_toremove.PROJ_ID;
				
        	END LOOP;
        	
	END gm_sync_set_tocompany;
	
	/*******************************************************
	 * Purpose: This Procedure will sync all the Groups that belongs to a System, to
	 * all companies where the System is visible for.
	 * When the System is processed, we will get all the System ID in a temp table.
	 * This temp table will be looped to get all the Groups and will be synced to the Companies as well.
	 * 
	 * Author : Rajeshwaran
	*******************************************************/
	PROCEDURE gm_sync_groups_tocompany(
		p_userid        IN   T205_PART_NUMBER.C205_LAST_UPDATED_BY%TYPE
	)
	AS
		CURSOR cur_group_tosync
		IS
		 SELECT DISTINCT 
		 			T207.C207_SET_ID set_id,
					T2080.C1900_COMPANY_ID compid
			FROM 
                    T2080_SET_COMPANY_MAPPING T2080,
					MY_TEMP_DATA_VISIBILITY TEMP,
                    T207_SET_MASTER T207,
                    T4010_GROUP T4010
			WHERE 
                    T207.C207_VOID_FL IS NULL
                AND T207.C901_SET_GRP_TYPE=1600
                AND T2080.C2080_VOID_FL IS NULL
                AND T2080.C207_SET_ID = T207.C207_SET_ID
                AND T4010.C207_SET_ID = T207.C207_SET_ID
                AND T4010.C4010_VOID_FL IS NULL
                AND TEMP.ID = to_char(T207.C207_SET_ID)
                AND TEMP.TEMP_TYPE = 'system'
                AND TEMP.COMPANY_ID = T2080.C1900_COMPANY_ID
                AND TEMP.TEMP_ACTION = 'released' ;
                
        CURSOR cur_group_toremove
		IS
		
		 SELECT DISTINCT 
		 			T207.C207_SET_ID set_id,
					T2080.C1900_COMPANY_ID compid
			FROM 
                    T2080_SET_COMPANY_MAPPING T2080,
					MY_TEMP_DATA_VISIBILITY TEMP,
                    T207_SET_MASTER T207,
                    T4010_GROUP T4010
			WHERE 
                    T207.C207_VOID_FL IS NULL
                AND T207.C901_SET_GRP_TYPE=1600
                --should not include T2080.void fl check.
                AND T2080.C207_SET_ID = T207.C207_SET_ID
                AND T4010.C207_SET_ID = T207.C207_SET_ID
                AND T4010.C4010_VOID_FL IS NULL
                AND TEMP.ID = to_char(T207.C207_SET_ID)
                AND TEMP.COMPANY_ID = T2080.C1900_COMPANY_ID
                AND TEMP.TEMP_TYPE = 'system'
                AND TEMP.TEMP_ACTION = 'un-released';
                
	BEGIN
		
		FOR group_tosync IN cur_group_tosync
        	LOOP
        		gm_pkg_pd_group_txn.gm_sync_group_to_company('',group_tosync.set_id,group_tosync.compid,'105361',p_userid);

        		DELETE FROM  MY_TEMP_DATA_VISIBILITY
				WHERE TEMP_TYPE = 'system'
				AND TEMP_ACTION = 'released'
				AND COMPANY_ID = group_tosync.compid
				AND ID = group_tosync.set_id;
				
        	END LOOP;
        	
			FOR group_toremove IN cur_group_toremove
        	LOOP
        	
				gm_pkg_pd_group_txn.gm_remove_group_to_company(group_toremove.set_id,group_toremove.compid,p_userid);
        	
				DELETE FROM  MY_TEMP_DATA_VISIBILITY
				WHERE TEMP_TYPE = 'system'
				AND TEMP_ACTION = 'un-released'
				AND COMPANY_ID = group_toremove.compid
				AND ID = group_toremove.set_id;
				
        	END LOOP;
		
	END gm_sync_groups_tocompany;
	
/******************************************************* 
	 * This Procedure will check if the Part is released for atleast one company.
	 * If it is released for any one company updating c205_release_for_sale in t205_part_number.
	 * If part is not evn released for any 1 company update the c205_release_for_sale to null
	 * For the purpose of Part Updates 
	 * Author : Akumar 
	*******************************************************/
	PROCEDURE gm_sav_part_RFS_flag(
		p_userid        IN   T205_PART_NUMBER.C205_LAST_UPDATED_BY%TYPE
	) 
	AS 

	BEGIN
		
		--Remove release for sales flag in t205.when rfs updated parts is not exist in active rfs part list.
		 UPDATE t205_part_number t205
		    SET c205_release_for_sale = NULL,
		        c205_last_updated_by = p_userid, 
		        c205_last_updated_date = CURRENT_DATE
		  WHERE c205_release_for_sale = 'Y'
		    AND EXISTS
		    (--updated parts
		         SELECT C205_PART_NUMBER_ID
		           FROM MY_TEMP_PART_LIST temp
		          WHERE temp.C205_PART_NUMBER_ID = t205.C205_PART_NUMBER_ID
		            AND NOT EXISTS
		            (--RFS parts query
		                 SELECT C205_PART_NUMBER_ID
		                   FROM t205d_part_attribute t205
		                  WHERE C901_ATTRIBUTE_TYPE     = '80180'
		                    AND c205d_void_fl          IS NULL
		                    AND t205.C205_PART_NUMBER_ID = temp.C205_PART_NUMBER_ID
		            )
		    );
		 
		 --Update release for sales flag in t205.when rfs updated parts is exist in active rfs part list.
		 UPDATE t205_part_number t205
		    SET c205_release_for_sale = 'Y',
		        c205_last_updated_by = p_userid, 
		        c205_last_updated_date = CURRENT_DATE
		  WHERE c205_release_for_sale IS NULL
		    AND EXISTS
		    (--updated parts
		         SELECT C205_PART_NUMBER_ID
		           FROM MY_TEMP_PART_LIST temp
		          WHERE temp.C205_PART_NUMBER_ID = t205.C205_PART_NUMBER_ID
		            AND EXISTS
		            (--RFS parts query
		                 SELECT C205_PART_NUMBER_ID
		                   FROM t205d_part_attribute t205
		                  WHERE C901_ATTRIBUTE_TYPE     = '80180'
		                    AND c205d_void_fl          IS NULL
		                    AND t205.C205_PART_NUMBER_ID = temp.C205_PART_NUMBER_ID
		            )
		    );		    
  
  END gm_sav_part_RFS_flag;
  
	/****************************************************************
	* Description : This procedure used when change the project to sync the part company map details
	* Author      : mmuthusamy
	*****************************************************************/
	PROCEDURE gm_sync_part_company_map (
	        p_partnum IN T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE,
	        p_userid  IN T205_PART_NUMBER.C205_LAST_UPDATED_BY%TYPE)
	AS
	    v_project_id T205_PART_NUMBER.c202_project_id%TYPE;
	    v_part_comp_map_cnt NUMBER;
	    --
	    CURSOR parent_project_comp_map_cur
	    IS
	         SELECT C1900_COMPANY_ID comp_id
	           FROM T2021_PROJECT_COMPANY_MAPPING
	          WHERE C202_PROJECT_ID       = v_project_id
	            AND C901_PROJ_COM_MAP_TYP = 105361
	            AND C1900_COMPANY_ID     IN
	            (
	                 SELECT c1900_company_id
	                   FROM t1900_company
	                  WHERE c1900_parent_company_id IS NULL
	                    AND c1900_void_fl           IS NULL
	            )
	        AND C2021_VOID_FL IS NULL;
	  --      
	BEGIN
		-- very the part and project
		BEGIN
	     SELECT C202_PROJECT_ID
	       INTO v_project_id
	       FROM T205_PART_NUMBER
	      WHERE C205_PART_NUMBER_ID = p_partnum;
	   EXCEPTION
      	WHEN NO_DATA_FOUND
   		THEN
   			v_project_id := NULL;
   		END;
	    --
	    FOR parent_project_comp_map IN parent_project_comp_map_cur
	    LOOP
	         SELECT COUNT (1)
	           INTO v_part_comp_map_cnt
	           FROM T2023_PART_COMPANY_MAPPING
	          WHERE C205_PART_NUMBER_ID    = p_partnum
	            AND C901_PART_MAPPING_TYPE = 105361
	            AND C1900_COMPANY_ID       = parent_project_comp_map.comp_id
	            AND c2023_void_fl         IS NULL;
	        --
	        IF v_part_comp_map_cnt = 0 THEN
	            -- to map the part # as released
	            gm_pkg_qa_rfs.gm_sav_part_company_map (p_partnum, parent_project_comp_map.comp_id, NULL, 105361, 70171,
	            p_userid) ;
	        END IF;
	    END LOOP;
	END gm_sync_part_company_map;
	/*******************************************************
	 * Purpose: This Procedure Minus the temp parts and RFS parts to validate rfs by country.
	 * Author : Jeeva
	*******************************************************/
	PROCEDURE gm_validate_rfs_by_country(
		p_country_id		IN	 T205H_PART_RFS_MAPPING.C901_RFS_COUNTRY_ID%TYPE
	)
	AS
	BEGIN
		-- To delete the temp table.		
		DELETE FROM	my_temp_part_list;
		
		INSERT INTO my_temp_part_list (c205_part_number_id)
			SELECT PART FROM TEMP_PART_QTY
			MINUS
			SELECT c205_part_number_id
			FROM t205d_part_attribute
			WHERE c205_part_number_id IN (SELECT PART FROM TEMP_PART_QTY)
			AND C205D_ATTRIBUTE_VALUE = get_mapping_countryid (to_number (p_country_id))  --
			AND c901_attribute_type = 80180
			AND c205d_void_fl IS NULL; 
	END gm_validate_rfs_by_country;	
		
END gm_pkg_qa_rfs;

/
