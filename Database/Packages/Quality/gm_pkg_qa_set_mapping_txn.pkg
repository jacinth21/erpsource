CREATE OR REPLACE PACKAGE gm_pkg_qa_set_mapping_txn
IS
    --
    /****************************************************************
    * Description : Procedure used to split the set mapping upload bulk data and store to master table.
    * Author      :
    *****************************************************************/
    PROCEDURE gm_sav_bulk_set_list_upload (
            p_set_id    IN t2084_set_mapping_upload_dtls.c207_set_id%type,
            p_input_str IN CLOB,
            p_action    IN t2084_set_mapping_upload_dtls.c901_action%type,
            p_comments  IN VARCHAR2,
            p_user_id   IN t2084_set_mapping_upload_dtls.c2084_last_updated_by%type,
            p_out_error_dtls OUT CLOB,
            p_out_mapping_str OUT CLOB) ;
    --
    /****************************************************************
    * Description : Procedure used to save the set mapping data to master table.
    * Author      :
    *****************************************************************/
    PROCEDURE gm_sav_set_mapping_upload_dtls (
            p_set_id         IN t2084_set_mapping_upload_dtls.c207_set_id%TYPE,
            p_part_number_id IN t2084_set_mapping_upload_dtls.c205_part_number_id%TYPE,
            p_set_qty        IN t2084_set_mapping_upload_dtls.c2084_set_qty%TYPE,
            p_critical_fl    IN t2084_set_mapping_upload_dtls.c2084_critical_fl%TYPE,
            p_critical_qty   IN t2084_set_mapping_upload_dtls.c2084_critical_qty%TYPE,
            p_inset_fl       IN t2084_set_mapping_upload_dtls.c2084_inset_fl%TYPE,
            p_action         IN t2084_set_mapping_upload_dtls.c901_action%TYPE,
            p_status         IN t2084_set_mapping_upload_dtls.c901_status%TYPE,
            p_new_record_fl  IN t2084_set_mapping_upload_dtls.c2084_new_record_fl%TYPE,
            p_set_details_id IN t2084_set_mapping_upload_dtls.c208_set_details_id%TYPE,
            p_critical_tag   IN t2084_set_mapping_upload_dtls.c2084_critical_tag%TYPE,
            p_critical_type  IN t2084_set_mapping_upload_dtls.c901_critical_type%TYPE,
            p_unit_type      IN t2084_set_mapping_upload_dtls.c901_unit_type%TYPE,
            p_user_id        IN t2084_set_mapping_upload_dtls.c2084_last_updated_by%TYPE) ;
    --
    /****************************************************************
    * Description : Procedure used to validate the part number and update incorrect parts
    * Author      :
    *****************************************************************/
    PROCEDURE gm_validate_part_number (
            p_set_id IN t2084_set_mapping_upload_dtls.c207_set_id%TYPE) ;
    --
    /****************************************************************
    * Description : Procedure used to validate critical flag and update incorrect data.
    * Author      :
    *****************************************************************/
    PROCEDURE gm_validate_critical_fl (
            p_set_id IN t2084_set_mapping_upload_dtls.c207_set_id%TYPE) ;
            
--
    /****************************************************************
    * Description : Procedure used to create the set mapping input string
    * Author      :
    *****************************************************************/
    PROCEDURE gm_create_set_upload_input_str_dtls (
            p_set_id  IN t2084_set_mapping_upload_dtls.c207_set_id%TYPE,
            p_user_id IN t2084_set_mapping_upload_dtls.c2084_last_updated_by%TYPE,
            p_out_mapping_str OUT CLOB) ;            
    --
    /****************************************************************
    * Description : Procedure used to create the set mapping input string
    * Author      :
    *****************************************************************/
    PROCEDURE gm_create_set_map_inputstr (
            p_set_id IN t2083_set_mapping_upload.c207_set_id%TYPE,
            p_out_mapping_str OUT CLOB) ;
    
    --
    /****************************************************************
    * Description : Procedure used to save/update set mapping upload data.
    * Author      :
    *****************************************************************/
    PROCEDURE gm_sav_set_mapping_upload (
            p_set_id  IN t2083_set_mapping_upload.c207_set_id%TYPE,
            p_status  IN t2083_set_mapping_upload.c901_status%TYPE,
            p_user_id IN t2083_set_mapping_upload.c2083_last_updated_by%TYPE) ;
    --
    /****************************************************************
    * Description : Procedure used to save set mapping bulk operations to log table
    * Author      :
    *****************************************************************/
    PROCEDURE gm_sav_set_mapping_upload_log (
            p_set_id    IN t207_set_master.c207_set_id%TYPE,
            p_action_id IN t2084_set_mapping_upload_dtls.c901_status%TYPE,
            p_user_id   IN t2084_set_mapping_upload_dtls.c2084_last_updated_by%TYPE) ;
    --
    /****************************************************************
    * Description : Procedure used to remove the parts from set mapping details
    * Author      :
    *****************************************************************/
    PROCEDURE gm_remove_set_mapping (
            p_set_id  IN t2084_set_mapping_upload_dtls.c207_set_id%TYPE,
            p_comments  IN VARCHAR2,
            p_user_id IN t2084_set_mapping_upload_dtls.c2084_last_updated_by%TYPE,
            p_out_mapping_str OUT CLOB) ;
            

    /****************************************************************
	* Description : Procedure used for set link copy. Copying one set to another set. 
	* and voiding to set values and inserting new values comes from set values
	* Author      : Jgurunathan
	*****************************************************************/

	PROCEDURE gm_qa_sav_set_list_copy(
		p_from_set_id IN t208_set_details.c207_set_id%TYPE,
		p_to_set_id   IN t208_set_details.c207_set_id%TYPE,
		p_user_id     IN t208_set_details.c208_last_updated_by%TYPE,
		p_comments    IN VARCHAR2 );
            
END gm_pkg_qa_set_mapping_txn;
/
