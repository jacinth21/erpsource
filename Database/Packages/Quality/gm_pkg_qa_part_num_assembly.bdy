/****************************************************************
 *--@C:\PMT\BitBucket_Workspace\erpGlobus\Database\Packages\Quality\gm_pkg_qa_part_num_assembly.bdy
*   Description     :  Package used to part number BOM mapping details.
*   PMT-50777       : Create BOM and sub-component Mapping.
*   Author          : rajan
*****************************************************************/

CREATE OR REPLACE PACKAGE body gm_pkg_qa_part_num_assembly
IS
   

/****************************************************************
* Description : Procedure used to fetch part mapping bulk grid details.
* Author      : rajan
*****************************************************************/
PROCEDURE gm_fch_part_assembly_part_dtls (
            p_part_number      IN T205_Part_Number.C205_PART_NUMBER_ID%TYPE, 
            p_action_type      IN T207_Set_Master.C901_SET_GRP_TYPE%TYPE,
            p_out_part_dtls   OUT CLOB)
AS
--
BEGIN
	--
         BEGIN
	        SELECT JSON_ARRAYAGG (JSON_OBJECT ('PMAPID' value T205A.C205A_PART_MAPPING_ID, 'PNUM' value  DECODE('109740',p_action_type,T205A.C205_To_Part_Number_Id,T205A.C205_From_Part_Number_Id),
	                                       'QTY' value T205A.C205A_QTY,'PDESC' value T205.C205_PART_NUM_DESC,'PTYPE' value  T205A.C901_TYPE,
	                                       'STATUSID' value T205.C901_STATUS_ID,'VOIDFLG' value T205A.C205A_VOID_FL, 'REV_NUMBER' value t205.c205_rev_num)
          ORDER BY T205.C205_Part_Number_Id RETURNING CLOB)
              INTO p_out_part_dtls
              FROM T205A_PART_MAPPING T205A,T205_Part_Number T205 
             WHERE DECODE('109740',p_action_type,T205A.C205_From_Part_Number_Id,T205A.C205_To_Part_Number_Id) = p_part_number  
               AND DECODE('109740',p_action_type,T205A.C205_To_Part_Number_Id,T205A.C205_FROM_PART_NUMBER_ID)= T205.C205_Part_Number_Id
               AND T205A.C205A_VOID_FL IS NULL;
	                 
		 EXCEPTION
		      WHEN OTHERS THEN
		        p_out_part_dtls := NULL;
		 END;
    --
END gm_fch_part_assembly_part_dtls;

/****************************************************************
* Description : Procedure used to fetch part number BOM mapping header details.
* Author      : rajan
*****************************************************************/
PROCEDURE gm_fch_part_assembly_header_dtls (
             p_part_number     IN T205_Part_Number.C205_PART_NUMBER_ID%TYPE, 
             p_out_part_dtls  OUT CLOB)
AS
--
BEGIN
	--
       BEGIN
            SELECT JSON_ARRAYAGG (JSON_OBJECT ('partDesc' value T205.C205_Part_Num_Desc, 'family' value Get_Code_Name(T205.C205_Product_Family),
                                               'prodclass' value Get_Code_Name(T205.C205_Product_Class),'setid' value NVL(T207.c207_set_id,''),
                                               -- PC-2368 BOM mapping - Remove void access rule values 
                                               'prodtyp' value T205.C205_Product_Class, 'bomcheckfl' value NVL(get_rule_value(T205.C205_Product_Family,'BOM_CRE_BY_PART_FAM'),'N'))
          ORDER BY c205_part_number_id ASC RETURNING CLOB)
              INTO p_out_part_dtls
              FROM T205_Part_Number T205 , t207_set_master t207
             WHERE T205.C205_Part_Number_Id = t207.c205_bom_part_number_id (+)
             AND T205.C205_Part_Number_Id = p_part_number
             AND t207.c207_void_fl (+) IS NULL;   

		 EXCEPTION
		      WHEN OTHERS THEN
		        p_out_part_dtls := NULL;
	   END;
    --
END gm_fch_part_assembly_header_dtls;

/****************************************************************
* Description : Procedure used to update the part mapping,   
* Author      : rajan
*****************************************************************/
PROCEDURE gm_sav_part_assembly_upload_dtls(
		   p_part_number      IN   T205A_PART_MAPPING.C205_FROM_PART_NUMBER_ID%TYPE,
		   p_part_str         IN   CLOB,
		   p_input_str        IN   CLOB,
           p_void_input_str   IN   CLOB,
           p_action           IN   T207_Set_Master.C901_SET_GRP_TYPE%TYPE,
           p_bom_fl           IN   VARCHAR2,
           p_user_id          IN   VARCHAR2,
           p_bom_id           IN   VARCHAR2 default NULL,
           p_out_bom_ids      OUT  CLOB,
           p_err_msg          OUT  CLOB)
  AS
        v_msg_invalid_part   CLOB;
        v_msg_prt_cmpny      CLOB;
        v_bom_id 	         t207_set_master.C207_SET_ID%TYPE;
  		v_bom_ids            CLOB;
  		v_out_status         VARCHAR2(1) := 'N';
BEGIN
	    p_out_bom_ids := NULL;
	
	    --To call Invalid part Number validation 
          gm_pkg_pd_partnumber.gm_validate_mfg_parts(p_part_str, v_msg_invalid_part, v_msg_prt_cmpny);
     
        IF v_msg_invalid_part IS NULL THEN
 
           gm_upd_part_assembly_dtls(p_part_number,p_input_str,p_action,p_user_id);
               
           gm_void_part_assembly_dtls(p_part_number,p_void_input_str,p_action,p_user_id);
                          
	           IF p_bom_id IS NULL AND p_bom_fl = 'Y' AND p_action = 109740 THEN   -- 109740 - part number
	           	                       
	           	 gm_sav_bom_master_dtls(p_part_number,p_user_id,v_bom_id);
	           		
	             p_out_bom_ids := v_bom_id||',';
	           	
	           ELSIF p_bom_id IS NOT NULL  THEN
	             p_out_bom_ids := p_bom_id || ',';
	           	
	           END IF;
                      
	          IF p_action = 109741 THEN      --- 109741 - Sub-component
		       	  SELECT RTRIM (XMLAGG (XMLELEMENT (e, c207_set_id || ',')) .EXTRACT ('//text()'), ',') 
		        	INTO v_bom_ids
		        	FROM t207_set_master 
		        	WHERE c901_set_grp_type=1602 and c205_bom_part_number_id in (SELECT token from v_clob_list)
		        	AND c207_void_fl is null;
	        	
	        	    p_out_bom_ids := v_bom_ids||',';
	        
	          END IF;  
	     --
	     --set flag  y for success message
             v_out_status := 'Y';
             p_err_msg := v_out_status;
	   ELSE
		   p_err_msg := v_out_status|| '##'|| v_msg_invalid_part;
	   	   	   
       END IF;
	
END gm_sav_part_assembly_upload_dtls;

/****************************************************************
* Description : Procedure used to update void grid details.
* Author      : rajan
*****************************************************************/
PROCEDURE gm_void_part_assembly_dtls(
			p_part_number IN T205A_PART_MAPPING.C205_FROM_PART_NUMBER_ID%TYPE,
			p_void_str    IN CLOB,
			p_action_type IN T205A_PART_MAPPING.C901_TYPE%TYPE,
			p_user_id     IN VARCHAR2
      )
   AS
    BEGIN
	    my_context.set_my_inlist_ctx (p_void_str);	 
	               
	       UPDATE t205a_part_mapping 
	          SET c205a_void_fl = 'Y',
	              c205a_last_updated_by = p_user_id,
	              c205a_last_updated_date = current_date
		    WHERE c205a_part_mapping_id 
		       IN (SELECT token FROM v_in_list WHERE token IS NOT NULL)
		      AND c205a_void_fl IS NULL;
		       
	       IF (p_action_type = 109741) THEN
      		UPDATE t205_part_number
			   SET c205_sub_component_fl = NULL
		           ,c205_last_updated_date = current_date
	               ,c205_last_updated_by = p_user_id
			 WHERE c205_part_number_id IN (SELECT token FROM v_in_list WHERE token IS NOT NULL);
		  END IF;

END gm_void_part_assembly_dtls;

/****************************************************************
* Description : Procedure used to update part numbers grid data details.
* Author      : rajan
*****************************************************************/
PROCEDURE gm_upd_part_assembly_dtls(
   p_part_number  IN T205A_PART_MAPPING.C205_FROM_PART_NUMBER_ID%TYPE,
   p_input_str    IN CLOB,
   p_action_type IN T205A_PART_MAPPING.C901_TYPE%TYPE,
   p_user_id     IN VARCHAR2
  )
AS
    v_strlen	        NUMBER := NVL (LENGTH (p_input_str), 0);
    v_string            CLOB := p_input_str;
    v_substring         VARCHAR2(4000);
    v_qty               NUMBER;
    v_partnumber        T205A_PART_MAPPING.C205_FROM_PART_NUMBER_ID%TYPE;
    v_type              NUMBER;
    v_cyclpnum	        VARCHAR2 (20);
    v_multipro_fl	    t205_part_number.c205_crossover_fl%TYPE;
 BEGIN
	 
	     IF (p_action_type = 109740) THEN    -- 109740 - part number 
	 --check for the input parent part as multi part or not.
			BEGIN	
				select c205_crossover_fl 
				  into v_multipro_fl
				  from t205_part_number  
				 WHERE c205_part_number_id = p_part_number;
			EXCEPTION WHEN OTHERS
			THEN
				v_multipro_fl :=null;
			END;
			
			END IF;
	
   IF v_strlen > 0
	THEN
    WHILE instr(v_string, '|') <> 0
    LOOP
    
    v_substring := substr(v_string, 1, instr(v_string, '|') - 1);

                v_string := substr(v_string, instr(v_string, '|') + 1);
				--
                v_partnumber := NULL;
                v_qty := 0;
                v_type := NULL;
                
                v_partnumber := substr(v_substring, 1, instr(v_substring, '^') - 1);

                v_substring := substr(v_substring, instr(v_substring, '^') + 1);
                
                v_qty := substr(v_substring, 1, instr(v_substring, '^') - 1);

                v_type := substr(v_substring, instr(v_substring, '^') + 1);
             
                ---   insert / update into part number mapping               
                gm_sav_part_number_mapping(p_part_number,p_action_type,v_partnumber,v_qty,v_type, p_user_id);
			   
			   IF (p_action_type = 109740) THEN   -- 109740 - part number
                   SELECT count(1) INTO v_cyclpnum FROM
											(SELECT T205A.*, CONNECT_BY_ISCYCLE CYCL
											 FROM T205A_PART_MAPPING T205A
			       START WITH C205_FROM_PART_NUMBER_ID = v_partnumber
			       CONNECT BY NOCYCLE PRIOR T205A.C205_TO_PART_NUMBER_ID = T205A.C205_FROM_PART_NUMBER_ID) TEMP
			       WHERE TEMP.CYCL > 0;

		          IF v_cyclpnum > 0 THEN
		            raise_application_error (-20175, '');    --- 20175 - One or more parts are parent parts.
		          END IF;
                 gm_upd_part_num_sub_component_fl(v_partnumber,v_multipro_fl);
	            
	           END IF;
        END LOOP;
   END IF;     
END gm_upd_part_assembly_dtls;


  /************************************************************************************************
   Description : This procedure is used to insert / update into part number mapping
   Author      : rajan
  *************************************************************************************************/
   PROCEDURE gm_sav_part_number_mapping (
        p_part_number  IN     T205A_PART_MAPPING.C205_FROM_PART_NUMBER_ID%TYPE,
        p_action_type  IN     T205A_PART_MAPPING.C901_TYPE%TYPE,
        p_vpartnumber  IN     T205A_PART_MAPPING.C205_FROM_PART_NUMBER_ID%TYPE,
        p_vqty         IN     NUMBER,
        p_vtype        IN     NUMBER,
        p_user_id	   IN     T205A_PART_MAPPING.C205A_LAST_UPDATED_BY%TYPE)
     AS
     v_rowcount	NUMBER :=0;
     BEGIN
                UPDATE T205a_Part_Mapping 
                   SET C205A_QTY = p_vqty,
                       C901_Type = p_vtype,
		               c205a_last_updated_by = p_user_id,
		               c205a_last_updated_date = current_date 
                 WHERE C205_FROM_PART_NUMBER_ID = DECODE('109740',p_action_type,p_part_number,p_vpartnumber)
                   AND C205_TO_PART_NUMBER_ID = DECODE('109740',p_action_type,p_vpartnumber,p_part_number)
                   AND C205A_VOID_FL IS NULL;
                   
                v_rowcount := SQL%ROWCOUNT;
                
                IF v_rowcount = 0 THEN   
	            INSERT 
	              INTO T205A_PART_MAPPING(C205A_PART_MAPPING_ID, 
	                                      C205_FROM_PART_NUMBER_ID, 
	                                      C205_TO_PART_NUMBER_ID, 
	                                      C205A_QTY, C901_TYPE)
			    VALUES (S205a_Part_Mapping.Nextval, DECODE('109740',p_action_type,p_part_number,p_vpartnumber),      ---  109740 - part number
			             DECODE('109740',p_action_type,p_vpartnumber,p_part_number), p_vqty, p_vtype);
			   END IF;
		
	END gm_sav_part_number_mapping;		     
        
        
   /************************************************************************************************
   Description : This procedure is used to update part number and sub component flag
   Author      : rajan
   *************************************************************************************************/
   PROCEDURE gm_upd_part_num_sub_component_fl (
 	p_vpartnumber            IN  t205a_part_mapping.c205_from_part_number_id%TYPE,
 	p_vmultipro_fl	         IN  t205_part_number.c205_crossover_fl%TYPE)

  	AS
  	v_sub_part_to_order NUMBER; 
   BEGIN
	   
	  UPDATE t205_part_number
	               SET c205_sub_component_fl = 'Y'
	                 , c205_last_updated_date = current_date
	             WHERE c205_part_number_id = p_vpartnumber;
	             
	             SELECT COUNT(1) INTO v_sub_part_to_order
	             FROM T205D_SUB_PART_TO_ORDER
	             WHERE C205D_VOID_FL IS NULL
	             AND   C205_PART_NUMBER_ID = p_vpartnumber;
	             
	             --If the Part is a sub-part to order and the parent parent part is multi-project,
	             --mark the current part also as multi-project part.
	             	             
	             IF p_vmultipro_fl IS NOT NULL AND v_sub_part_to_order >0 
	             THEN
	                 UPDATE t205_part_number
	                   SET c205_crossover_fl = 'Y'
	                     , c205_last_updated_date = current_date
	                 WHERE c205_part_number_id = p_vpartnumber;
	             END IF;
	  
	  
 END gm_upd_part_num_sub_component_fl;
 
	/****************************************************************
	* Description : Procedure used to save the BOM details.
	* Author      : rajan
	*****************************************************************/
	PROCEDURE	gm_sav_bom_master_dtls(
	 	p_part_number  IN  T205A_PART_MAPPING.C205_FROM_PART_NUMBER_ID%TYPE,
	 	p_user_id      IN  VARCHAR2,
	 	p_bom_id      OUT t207_set_master.C207_SET_ID%TYPE
		)
	 AS
		v_bom_id       t207_set_master.C207_SET_ID%TYPE;
	 	v_proj_id      t205_part_number.C202_PROJECT_ID%TYPE;
		v_part_desc    t205_part_number.C205_PART_NUM_DESC%TYPE;
		v_rev_nm       t205_part_number.C205_REV_NUM%TYPE;
		v_prod_fam     t205_part_number.C205_PRODUCT_FAMILY%TYPE;
		v_company_id   t1900_company.c1900_company_id%TYPE;
		v_proj_div_id  t1910_division.c1910_division_id%TYPE;
	 BEGIN
		 -- Replace the + symbol - PC-2368 BOM mapping - Remove void access
		 v_bom_id := 'BM.'||REPLACE(REPLACE(p_part_number,'.'),'+','P');
		 
		 BEGIN
		 
			 SELECT t202.c202_project_id, t205.c205_part_num_desc, t205.c205_rev_num, t205.c205_product_family
			 	, t202.c1910_division_id
		       INTO v_proj_id,v_part_desc,v_rev_nm,v_prod_fam
		       	, v_proj_div_id
		       FROM t205_part_number t205, t202_project t202
		      WHERE t202.c202_project_id = t205.c202_project_id
		      	AND t205.c205_part_number_id = p_part_number;
		  EXCEPTION
				WHEN NO_DATA_FOUND
				THEN
					v_proj_id := '';
					return;
			END;
					
		      SELECT get_compid_frm_cntx()
		        INTO v_company_id  
		        FROM DUAL;
		 
		-- to set the default values
		-- 4060 - Implants
		-- 4072 - Inhouse
		-- 1602 - Bill of Materials
		-- 20367 - Approved
		-- 2000 - 
		
		 -- Project id, Set id, set Name, Set Desc, Set Category,
		 -- Set Type, Set group type, User id, Action, Status, Revision, croos flag, details desc, Divsion id, lot track flag	
		 		        
		 GM_SAVE_SETMASTER(v_proj_id,v_bom_id,v_part_desc,p_part_number, '4060',
		                   '4072','1602',p_user_id,'Add','20367',v_rev_nm,'',v_part_desc, v_proj_div_id,'');

		-- after created the new BOM - status default to created. To update the status "Approved"
		 UPDATE t207_set_master
		   SET c901_status_id  = 20367
			, c207_last_updated_by = p_user_id
			, c207_last_updated_date = CURRENT_DATE
		  WHERE c207_set_id = v_bom_id;
			 
		 gm_pkg_pd_system_trans.gm_sav_system_company_map(v_bom_id,v_company_id,'105360',p_user_id);
		 
		  	  
	          
	        --2 to update the part id to (T207)
		    gm_upd_bom_part_num_id_to_set_master(p_part_number,v_bom_id,p_user_id);
	          
		      p_bom_id:= v_bom_id;
	END gm_sav_bom_master_dtls;
	
	
	
/****************************************************************
* Description : Procedure used to update the part id to (T207) 
* Author      : rajan
*****************************************************************/
PROCEDURE gm_upd_bom_part_num_id_to_set_master(
           p_part_number  IN  t205a_part_mapping.c205_from_part_number_id%TYPE,
           p_bom_id       IN  t207_set_master.c207_set_id%TYPE,
	 	   p_user_id      IN  VARCHAR2)
      AS
    BEGIN
             UPDATE t207_set_master
	            SET c205_bom_part_number_id = p_part_number, 
	                c207_last_updated_by = p_user_id, 
	                c207_last_updated_date = current_date
	          WHERE c207_set_id = p_bom_id;

END gm_upd_bom_part_num_id_to_set_master;


/****************************************************************
* Description : Procedure used to Update or Insert BOM part number mapping details.
* Author      : rajan
*****************************************************************/
PROCEDURE gm_upd_bom_part_mapping(
p_bom_ids           IN   CLOB,
p_user_id           IN  VARCHAR2
)
AS
v_bom_part  t207_set_master.C207_SET_DESC%TYPE;

	CURSOR v_bom_list
	IS
	SELECT token bomid FROM v_clob_list where token IS NOT NULL;
	
	CURSOR v_part_dtl (v_part  t207_set_master.C207_SET_DESC%TYPE)
	IS
	SELECT C205_TO_PART_NUMBER_ID part,C205A_QTY qty FROM t205a_part_mapping
		WHERE C205_FROM_PART_NUMBER_ID=v_part AND C205A_VOID_FL IS NULL;

BEGIN
	
	 my_context.set_my_cloblist (p_bom_ids);
	 
	 FOR v_bom IN v_bom_list
		LOOP
		
		SELECT c205_bom_part_number_id INTO v_bom_part FROM t207_set_master WHERE 
			C207_SET_ID=v_bom.bomid;
		
		-- to update the part number UDI parameter details.
		gm_pkg_pd_sav_udi.gm_sav_qa_parent_dmdi_param (v_bom_part, p_user_id);
		gm_pkg_pd_sav_udi.gm_sav_sub_com_bulk_parameter (v_bom_part, p_user_id);
				
		UPDATE t208_set_details 
		   SET C208_VOID_FL='Y',C208_LAST_UPDATED_DATE=CURRENT_DATE
		       ,C208_LAST_UPDATED_BY=p_user_id
		 WHERE C207_SET_ID=v_bom.bomid
		   AND c208_void_fl IS NULL
		   AND C205_PART_NUMBER_ID NOT IN (SELECT C205_TO_PART_NUMBER_ID FROM t205a_part_mapping
		 WHERE C205_FROM_PART_NUMBER_ID=v_bom_part AND C205A_VOID_FL IS NULL);
		
		FOR v_part IN v_part_dtl(v_bom_part)
		LOOP
		
			UPDATE t208_set_details 
			   SET C208_SET_QTY = v_part.qty,C208_LAST_UPDATED_DATE = CURRENT_DATE
			       ,C208_LAST_UPDATED_BY = p_user_id
			 WHERE C205_PART_NUMBER_ID = v_part.part 
			   AND C207_SET_ID=v_bom.bomid
			   AND c208_void_fl IS NULL;
									
			 IF (SQL%ROWCOUNT = 0) THEN
			INSERT 
			  INTO t208_set_details
					(c208_set_details_id, c207_set_id, c205_part_number_id, c208_set_qty, c208_inset_fl
				   , c208_critical_fl, c901_unit_type) -- add c901_unit_type column and values for this - PC-2368 BOM mapping - Remove void access
			 VALUES (s208_setmap_id.NEXTVAL, v_bom.bomid, v_part.part, v_part.qty, 'Y'
				   , 'N', 40010); -- add  code lookup '40010 - One each'  values for this - PC-2368 BOM mapping - Remove void access
    		  END IF;
    	  
		END LOOP;

		-- to update the comments
    	-- Set id , Action id, user id
    	gm_pkg_qa_set_mapping_txn.gm_sav_set_mapping_upload_log (v_bom.bomid, 108741, p_user_id);	
    	
	END LOOP;
		
end gm_upd_bom_part_mapping;
	
END gm_pkg_qa_part_num_assembly;
/