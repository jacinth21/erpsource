--@"C:\Database\Packages\Quality\gm_pkg_qa_scar_rpt.bdy";


CREATE OR REPLACE PACKAGE BODY gm_pkg_qa_scar_rpt
IS
	/**********************************************************
	 * Description : Procedure to fetch Scar Information ids
	 * Author	   : Manikandan
	 **********************************************************/

    PROCEDURE gm_fch_scar_info (
    		  p_scar_id		    IN   T3160_SCAR.C3160_SCAR_ID%TYPE
    		 ,p_out_scar        OUT   TYPES.cursor_type
    )
	AS
		v_out_type VARCHAR(4000);
	BEGIN
		
			gm_pkg_qa_com_rsr_process.gm_validate_id(UPPER(p_scar_id),'102789',v_out_type);
		
			OPEN p_out_scar
			FOR
			SELECT c3160_scar_id scarid
			     , c3160_ncmr_com_ref complaint
			     , c3160_scar_desc scardesc
			     , c301_assigned_to assignedTo
			     , to_char(c3160_assigned_dt,get_rule_value ('DATEFMT', 'DATEFORMAT')) assignedDt
			     , to_char(c3160_response_dt,get_rule_value ('DATEFMT', 'DATEFORMAT')) respDueDt
			     , to_char(c3160_close_dt,get_rule_value ('DATEFMT', 'DATEFORMAT')) closedDt
			     , c3160_response relaventinfo
			     , c901_status status
			     , get_code_name(c901_status) statusName
			     , get_vendor_name(c301_assigned_to) assignedNames
			     , c101_issued_by strIssuedBy
			     , get_user_name(c101_issued_by) strIssuedByNm
			     , c3160_previous_scar prevScar
			     , DECODE(c3160_follow_up_fl,'Y',80130,'N',80131,'0') followUpFlag
				 , to_char(c3160_follow_up_dt,get_rule_value ('DATEFMT', 'DATEFORMAT')) estimetdFlUpDt  
				 ,DECODE(C901_Status,'102780',GET_WEEKDAYSDIFF(Trunc(c3160_assigned_dt),Trunc(C3160_Last_Updated_Date)) ,'102781',GET_WEEKDAYSDIFF(Trunc(c3160_assigned_dt),Trunc(c3160_close_dt)),NULL) daysToClose--(changes made to get the working days from the calendar) 
			  FROM t3160_scar
			 WHERE UPPER(c3160_scar_id) = UPPER(p_scar_id)
			   AND c3160_void_fl IS NULL;			  
	END gm_fch_scar_info;

	/*************************************************************
	 * Description : Procedure to fetch Scar Evalutor Information
	 * Author	   : Jignesh Shah
	 *************************************************************/

    PROCEDURE gm_fch_scar_eval_info (
    	p_out_scar_eval		OUT   TYPES.cursor_type
    )
	AS
	BEGIN
		OPEN p_out_scar_eval
		FOR
			SELECT	'20' day
      				,t3160.c3160_scar_id scarid
      				,to_char(t3160.c3160_response_dt,get_rule_value ('DATEFMT', 'DATEFORMAT')) responsedt
    			FROM t3160_scar t3160 
			    WHERE t3160.c901_status = 102779
			    AND t3160.c3160_void_fl IS NULL 
			    AND c3160_close_dt IS NULL 
			    AND GET_WEEKDAYSDIFF(t3160.c3160_assigned_dt,TRUNC(SYSDATE)) = get_rule_value('20DAYS','EMAILSCAR') 
			UNION 
			SELECT	'30' day
					,t3160.c3160_scar_id 
					,to_char(t3160.c3160_response_dt,get_rule_value ('DATEFMT', 'DATEFORMAT')) responsedt
				FROM t3160_scar t3160 
				WHERE t3160.c901_status = 102779 
				AND t3160.c3160_void_fl IS NULL 
				AND c3160_close_dt IS NULL 
				AND GET_WEEKDAYSDIFF(t3160.c3160_assigned_dt,TRUNC(SYSDATE)) = get_rule_value('30DAYS','EMAILSCAR');			  
	END gm_fch_scar_eval_info;


END gm_pkg_qa_scar_rpt; 
/