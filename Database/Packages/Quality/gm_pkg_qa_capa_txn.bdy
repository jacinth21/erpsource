
--@"C:\Database\Packages\Quality\gm_pkg_qa_capa_txn.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_qa_capa_txn
IS
    /****************************************************************
    * Description : Procedure to save the CAPA INITIATION Information as a batch
    * Author      : 
    *****************************************************************/
    
PROCEDURE gm_sav_capa_info(
    		  p_capa_id		    IN   T3150_CAPA.C3150_CAPA_ID%TYPE
    		, p_type      		IN   T3150_CAPA.C901_TYPE%TYPE
    		, p_requested_by    IN   T3150_CAPA.C101_REQUESTED_BY%TYPE
    		, p_complaint       IN   T3150_CAPA.C3150_NCMR_COM_REF%TYPE
         	, p_project_ref     IN   T3150_CAPA.C3150_PROJECT_REF%TYPE    
            , p_capa_ref		IN   T3150_CAPA.C3150_CAPA_REF%TYPE  
            , p_lot_number      IN   T3150_CAPA.C3150_LOT_NUMBER%TYPE   
            , p_category        IN   T3150_CAPA.C3150_CATAGORY%TYPE   
            , p_capa_desc       IN   T3150_CAPA.C3150_CAPA_DESC%TYPE  
            , p_assigned_to 	IN	 VARCHAR
            , p_previous_capa   IN   T3150_CAPA.C3150_PREVIOUS_CAPA%TYPE
            , p_priority_level  IN   T3150_CAPA.C901_PRIORITY_LEVEL%TYPE
            , p_submitted_date  IN   T3150_CAPA.C3150_SUBMITTED_DT%TYPE    
            , p_response_date   IN   T3150_CAPA.C3150_RESPONSE_DT%TYPE                     
            , p_close_date      IN   T3150_CAPA.C3150_CLOSE_DT%TYPE   
            , p_response        IN   T3150_CAPA.C3150_RESPONSE%TYPE  
            , p_user_id			IN   T3150_CAPA.C3150_CREATED_BY%TYPE
            , p_issued_by       IN   T3150_CAPA.C101_ISSUED_BY%TYPE
            , p_out_capa_id     OUT  T3150_CAPA.C3150_CAPA_ID%TYPE  
         )
AS
 	v_capa_id T3150_CAPA.C3150_CAPA_ID%TYPE := p_capa_id;
 	v_capa_assgnid T3151_CAPA_ASSIGNEE.C3151_CAPA_ASSIGNEE_ID%TYPE;
 	v_msg varchar(4000);
 BEGIN
	 		UPDATE T3150_CAPA
	 		SET  
	 			 C901_TYPE 				 = DECODE(p_type,0,NULL,p_type) 
	 			,C101_REQUESTED_BY		 = DECODE(p_requested_by,0,NULL,p_requested_by)
	 			,C101_ISSUED_BY			 = DECODE(p_issued_by,0,NULL,p_issued_by)
	 			,C3150_NCMR_COM_REF 	 = p_complaint
	 			,C3150_PROJECT_REF       = p_project_ref
	 	   		,C901_STATUS			 = DECODE(p_close_date,NULL,102769,102771)
	 			,C3150_CAPA_REF			 = p_capa_ref
	 			,C3150_LOT_NUMBER 		 = p_lot_number
	 			,C3150_CAPA_DESC 		 = p_capa_desc  
	 			,C3150_PREVIOUS_CAPA	 = p_previous_capa
	 			,C3150_SUBMITTED_DT		 = p_submitted_date
	 			,C3150_RESPONSE_DT		 = NVL(p_response_date,get_business_day(p_submitted_date,30))
	 			,C3150_CLOSE_DT			 = p_close_date
	 			,C3150_RESPONSE 		 = p_response
	 			,C3150_last_updated_by   = p_user_id
	 			,c3150_last_updated_date = SYSDATE
	 			,c901_priority_level = DECODE(p_priority_level,0,NULL,p_priority_level)
	 			,c3150_catagory = p_category
	 		WHERE C3150_CAPA_ID = v_capa_id;

	 		IF (SQL%ROWCOUNT = 0)
				THEN
 			gm_pkg_qa_com_rsr_txn.gm_populate_id_by_year('10','102788',v_capa_id);
 		
 			INSERT INTO T3150_CAPA
				    (
				        C3150_CAPA_ID, C901_TYPE, C101_REQUESTED_BY, C101_ISSUED_BY
				      ,C3150_NCMR_COM_REF, C3150_PROJECT_REF, C901_STATUS
				      ,C3150_CAPA_REF, C3150_LOT_NUMBER
				      ,C3150_CAPA_DESC, C3150_PREVIOUS_CAPA, C3150_SUBMITTED_DT
				      ,C3150_RESPONSE_DT, C901_PRIORITY_LEVEL, C3150_CLOSE_DT
				      ,C3150_RESPONSE,  C3150_CREATED_BY
				      ,C3150_CREATED_DATE,C3150_CATAGORY
				    )
	  			VALUES
			    (
				        v_capa_id, DECODE(p_type,0,NULL,p_type)
				      ,DECODE(p_requested_by,0,NULL,p_requested_by)
				      ,DECODE(p_issued_by,0,NULL,p_issued_by)
				      ,p_complaint, p_project_ref
				      ,DECODE(p_close_date ,NULL,102769,102771)
				      ,p_capa_ref, p_lot_number
				      ,p_capa_desc, p_previous_capa, p_submitted_date
				      ,NVL(p_response_date,get_business_day(p_submitted_date,30)), DECODE(p_priority_level,0,NULL,p_priority_level), p_close_date
				      ,p_response,  p_user_id
				      ,SYSDATE,p_category
			    );
			    gm_update_log (v_capa_id, 'CAPA Initiated', '4000317', p_user_id, v_msg);
			END IF;
			IF  p_close_date IS NOT NULL THEN
				gm_update_log (v_capa_id, 'CAPA Closed', '4000317', p_user_id, v_msg);
			END IF;
			gm_pkg_qa_capa_txn.gm_sav_capa_assigned_to(v_capa_assgnid, v_capa_id, p_assigned_to);

 	p_out_capa_id := v_capa_id;
	END gm_sav_capa_info;
	
	/**********************************************************
	 * Description : Procedure to save CAPA assigned Information
	 * Author	   :
	 **********************************************************/
    PROCEDURE  gm_sav_capa_assigned_to(
    			  p_capa_assgnid	 IN	T3151_CAPA_ASSIGNEE.C3151_CAPA_ASSIGNEE_ID%TYPE
    			,p_capa_id		     IN T3150_CAPA.C3150_CAPA_ID%TYPE
    		  ,p_assigned_to	     IN	VARCHAR
    		
    )
    AS
    v_strlen    NUMBER          := NVL (LENGTH (p_assigned_to), 0) ;
    v_string    VARCHAR2 (4000) := p_assigned_to;
    v_substring VARCHAR2 (1000) ;
    v_assigned_to  NUMBER := 0;
    v_capa_assigned_id T3151_CAPA_ASSIGNEE.C3151_CAPA_ASSIGNEE_ID%TYPE;
    
BEGIN
            
	DELETE FROM T3151_CAPA_ASSIGNEE
    	WHERE C3150_CAPA_ID = p_capa_id;
    	
    IF v_strlen > 0 THEN
        WHILE INSTR (v_string, '|') <> 0
        LOOP
            v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1) ;
            v_string    := SUBSTR (v_string, INSTR (v_string, '|')    + 1) ;
            v_assigned_to     := TO_NUMBER (v_substring) ;
     
			  SELECT S1351_CAPA_ASSIGNEE.NEXTVAL
			  INTO v_capa_assigned_id
			  FROM DUAL;

			INSERT INTO T3151_CAPA_ASSIGNEE(C3151_CAPA_ASSIGNEE_ID, C3150_CAPA_ID, C101_ASSIGNED_TO)
            VALUES(v_capa_assigned_id, p_capa_id, v_assigned_to);
        END LOOP;
    END IF;
END gm_sav_capa_assigned_to;

	
	
	
	/******************************************************************
	    * Description : Procedure to cancel CAPA
	    * Author      : arajan
	*******************************************************************/
	
	PROCEDURE gm_cancel_capa
	(
		p_txn_id   IN T3150_CAPA.C3150_CAPA_ID%TYPE,
	    p_user_id  IN T3150_CAPA.C3150_LAST_UPDATED_BY%TYPE
	  
	)
	AS
		v_message VARCHAR(4000);
	BEGIN
			
		UPDATE t3150_capa
		SET c901_status = 102770, -- Canceled
			C901_TYPE = null,
			C101_REQUESTED_BY = null,
			C3150_NCMR_COM_REF = null,
			C3150_CAPA_REF= null,
			C3150_LOT_NUMBER = null,
			C3150_PROJECT_REF = null,
			C3150_CATAGORY =null,
			C3150_CAPA_DESC = null,
			C3150_PREVIOUS_CAPA = null,
			C3150_SUBMITTED_DT=null,
			C3150_RESPONSE_DT=null,
			C901_PRIORITY_LEVEL = null 
			,c3150_last_updated_by = p_user_id
			, c3150_last_updated_date = current_date
		WHERE c3150_capa_id = p_txn_id
		AND c3150_void_fl IS NULL;

		DELETE FROM T3151_CAPA_ASSIGNEE
	   		   WHERE C3150_CAPA_ID = p_txn_id;
		
		gm_update_log (p_txn_id, 'CAPA Cancelled', 4000317, p_user_id, v_message);
		
	END gm_cancel_capa;
	
END gm_pkg_qa_capa_txn;
/