/* Formatted on 2012/22/08 17:40 (Formatter Plus v4.8.0) */
--@"C:\Database\Packages\Quality\gm_pkg_qa_com_rsr_txn.bdy"
	CREATE OR REPLACE
		PACKAGE BODY gm_pkg_qa_com_rsr_txn
			IS
   	   /****************************************************************
    	* Description : Procedure to save RA Incident Link Information
    	* Author      : hreddi
    	*****************************************************************/
		PROCEDURE gm_sav_ra_incident_link (       
    		p_rma_id    IN t506_returns.C506_RMA_ID%TYPE,
    		p_part_id   IN T506B_RA_INCIDENT_LINK.C205_PART_NUMBER_ID%TYPE,
    		p_part_qty  IN T506B_RA_INCIDENT_LINK.C506B_qty%TYPE,
    	    p_userid    IN T506B_RA_INCIDENT_LINK.C506B_CREATED_BY%TYPE    	
    	)AS
		 BEGIN
			 INSERT INTO T506B_RA_INCIDENT_LINK 
			 		(C506B_RA_INCIDENT_LINK_ID,C506_RMA_ID,C901_INCIDENT_LINK_STATUS,C205_PART_NUMBER_ID
			 		,C506B_CREATED_BY,C506B_CREATED_DATE,C506B_QTY)
			 VALUES (S506B_RA_INCIDENT_LINK.NEXTVAL,p_rma_id,102849,p_part_id,p_userid,SYSDATE,p_part_qty);
			 
                   
		 END gm_sav_ra_incident_link;
		 
		/****************************************************************
		*	Description 	: THIS PROCEDURE is used to get the next val and FORMAT  it for the given ref_type and ref id
 		*	Parameters		: p_ref_id, p_ref_type
 		*	Author : Velu
		*************************************************************** */
		PROCEDURE gm_populate_id_by_year (
			p_ref_id	   IN		t908_gm_nextval.c908_ref_id%TYPE,
			p_ref_type	   IN		t908_gm_nextval.c901_ref_type%TYPE ,
			p_out_result   OUT		VARCHAR2
		)AS
			v_out_nextid VARCHAR(20);
			v_pattern_id VARCHAR(20);
			
		BEGIN	
			SELECT c908_start_pattern_id 
			  INTO v_pattern_id
		  	  FROM t908_gm_nextval t908, t901_code_lookup t901
		 	 WHERE t908.c901_pattern_format = t901.c901_code_id 
		 	   AND c908_ref_id   = p_ref_id 
		 	   AND c901_ref_type = p_ref_type;
	
			-- pass refif and reftype and it will return next val. EX: REFID:10, REFTYPE:4000417, V_OUT_NEXTID:130001
			gm_nextval (p_ref_id, p_ref_type, v_out_nextid);
			
			IF TRIM(v_pattern_id) <> to_char(SYSDATE,'YY') THEN -- If patter id is different then update current value=0, pattern id is current year.			
				UPDATE t908_gm_nextval
			   	   SET c908_currval = 0
			   	     , c908_start_pattern_id = to_char(SYSDATE,'YY')
			 	 WHERE c908_ref_id = p_ref_id 
			 	   AND c901_ref_type = p_ref_type;
			 	
			 	-- We are taking new value once update current value and pattern id.
			 	gm_nextval (p_ref_id, p_ref_type, v_out_nextid);
				v_pattern_id := to_char(SYSDATE,'YY');
		 	END IF;
		 
		 -- It will generate CAPA ids like CAPA-13-001
	 
		 SELECT get_code_name(p_ref_type)||'-'||v_pattern_id||'-' || SUBSTR (v_out_nextid, 3) INTO p_out_result  FROM DUAL;
	     
	    END gm_populate_id_by_year;
	    
	/****************************************************************
    * Description : Procedure to save Incident Information
    * Author      : hreddi
    *****************************************************************/
	PROCEDURE gm_sav_incident_info (        
	
     
     p_inputstr IN CLOB,
     p_user_id 				IN t3102_incident_detail.C3102_LAST_UPDATED_BY%TYPE,
     p_out_incident_id       OUT VARCHAR2
     
     
     
    )AS
    	v_json 	 json_object_t;
    --	v_incident_id	 t3100_incident.C3100_INCIDENT_ID%TYPE := p_incident_id;
    	v_incident_id	 t3100_incident.C3100_INCIDENT_ID%TYPE;
    	v_type           VARCHAR2(10);
    	v_ref_id         NUMBER;
    	v_log_type		 NUMBER;
    	v_message      VARCHAR2 (4000);    	
    	v_issueType   NUMBER;
    	v_complaint   NUMBER;
    	v_complaint_nm VARCHAR2(100);
    	v_status   NUMBER;
    	v_area_direct NUMBER;
    	v_inc_date  VARCHAR2(100);
    	v_inc_cat NUMBER;
    	v_compl_recv_by NUMBER;
    	v_compl_recv_dt VARCHAR2(100);
    	v_compl_recv_via NUMBER;
    	v_originator NUMBER;
    	v_mdrreportble VARCHAR2(20);
    	v_mdrreport_dt VARCHAR2(100);
    	v_mdr_id VARCHAR2(100);
    	v_meddevreport VARCHAR2(100);
    	v_meddev_id  VARCHAR2(100);
    	v_meddevreport_dt VARCHAR2(100);
    	v_details_desc_event VARCHAR2(4000);
    	v_address VARCHAR2(1000);
    	v_phone_number VARCHAR2(100);
    	v_company VARCHAR2(200);
    	v_part_number VARCHAR2(20);
    	v_project_id VARCHAR2(20);
    	v_part_desc VARCHAR2(255);
    	v_type_product NUMBER;
    	v_gem NUMBER;
    	v_lot_number VARCHAR2(255);
    	v_quantity NUMBER;
    	v_loanercon_type NUMBER;
    	v_ra_id VARCHAR2(20);
    	v_norm_wear_fl VARCHAR2(20);
    	v_explain_reason VARCHAR2(4000);
    	v_surgery_type VARCHAR2(100);
    	v_revi_surgery  VARCHAR2(20);
    	v_surgeon_nm VARCHAR2(100);
    	v_hospital VARCHAR2(100);
    	v_surgey_date VARCHAR2(100);
    	v_post_open_fl VARCHAR2(20);
    	v_post_open_date VARCHAR2(100);
    	v_event_demo_fl  VARCHAR2(20);
    	v_replace_fl VARCHAR2(20);
    	v_replace_part_fl VARCHAR2(20);
    	v_part_avail_eval_fl VARCHAR2(20);
    	v_decon_fl VARCHAR2(20);
    	v_shipped_fl VARCHAR2(20);
    	v_other_details VARCHAR2(4000);
    	v_tracking_number VARCHAR2(255);
    	v_part_shipped_dt VARCHAR2(100);
    	v_ship_carrier NUMBER;
    	--v_service_req_id NUMBER;
    	v_service_req_id VARCHAR2(20);
    	v_event_occured_id NUMBER;
    	v_adv_effect_id NUMBER;
    	v_proc_outcome_id NUMBER;

    v_date_fmt VARCHAR2 (20) ;
BEGIN
   
     SELECT get_compdtfmt_frm_cntx () INTO v_date_fmt FROM dual;
 
    	v_json     := json_object_t.parse(p_inputstr);
    	v_issueType := v_json.get_String('issueType');    	
    	v_complaint	:= v_json.get_String('complaint');    	
    	v_complaint_nm	:= v_json.get_String('complaintname');    	
    	v_status	:= v_json.get_String('status');    	
    	v_area_direct	:= v_json.get_String('areaDirector');    	
    	v_inc_date	:= v_json.get_String('dtIncDate');    	   	
    	v_inc_cat	:= v_json.get_String('incidentCat');     	    	
    	v_compl_recv_by	:= v_json.get_String('complRecvdBy');    	 
    	v_compl_recv_dt	:= v_json.get_String('complRecvDt');    	 
    	v_compl_recv_via	:= v_json.get_String('complRecvVia');    	 
    	v_originator	:= v_json.get_String('originator');    	 
    	v_mdrreportble	:= v_json.get_String('mdrReport');    	
    	v_mdrreport_dt	:= v_json.get_String('mdrReportDt');    	
    	v_mdr_id	:= v_json.get_String('mdrID');    	
    	v_meddevreport	:= v_json.get_String('meddevReport');    	
    	v_meddev_id	:= v_json.get_String('meddevID');    	
    	v_meddevreport_dt	:= v_json.get_String('meddevReportDt');    	
    	v_details_desc_event	:= v_json.get_String('detailDescEvent');    	
    	v_address	:= v_json.get_String('address');    	
    	v_phone_number	:= v_json.get_String('phoneNumber');    	
    	v_company	:= v_json.get_String('company');    	
    	v_part_number	:= v_json.get_String('partNum');    	
    	v_project_id	:= v_json.get_String('project');    	
    	v_part_desc	:= v_json.get_String('partDesc');    	
    	v_type_product	:= v_json.get_String('type');    	
    	v_gem	:= v_json.get_String('gem');    	
    	v_lot_number	:= v_json.get_String('lotNum');    	
    	v_quantity	:= v_json.get_String('quantity');    	
    	v_loanercon_type	:= v_json.get_String('lonorConsign');    	
    	v_ra_id	:= v_json.get_String('raID');    	
    	v_norm_wear_fl	:= v_json.get_String('normalWearFl');    	
    	v_explain_reason	:= v_json.get_String('ifYesExplain');    	
    	v_surgery_type	:= v_json.get_String('typeOfSurgery');    	
    	v_revi_surgery	:= v_json.get_String('reviSurgery');    	
    	v_surgeon_nm	:= v_json.get_String('surgeoonName');    	
    	v_hospital	:= v_json.get_String('hospitalName');    	
    	v_surgey_date	:= v_json.get_String('dateOfSurgery');    	
    	v_post_open_fl	:= v_json.get_String('postOpeFlag');    	
    	v_post_open_date	:= v_json.get_String('postOpeDate');    	
    	v_event_demo_fl	:= v_json.get_String('eventDemoFl');    	
    	v_replace_fl	:= v_json.get_String('replacementFl');    	
    	v_replace_part_fl	:= v_json.get_String('replacePartFl');    	
    	v_part_avail_eval_fl	:= v_json.get_String('partAvailEvalFl');    	
    	v_decon_fl	:= v_json.get_String('deconFl');    	
    	v_shipped_fl	:= v_json.get_String('shippedFl');    	
    	v_other_details	:= v_json.get_String('otherDetails');    	
        v_tracking_number	:= v_json.get_String('trackingNumber');              
    	v_part_shipped_dt	:= v_json.get_String('dateOfShipped');    	  
    	v_ship_carrier	:= v_json.get_String('carrier');    	 
    	v_service_req_id	:= v_json.get_String('reqFieldService');    	
    	v_event_occured_id	:= v_json.get_String('didThisEventHappenDuring');    	 
    	v_adv_effect_id     := v_json.get_String('adverseEffect');    	
    	v_proc_outcome_id  := v_json.get_String('procedureOutcome');    
    	v_incident_id :=  v_json.get_String('comRsrID');
    	
    	
	    UPDATE t3100_incident 
	       SET C901_ISSUE_TYPE      		=  NVL(v_issueType,C901_ISSUE_TYPE)          
			 , C901_COMPLAINANT_TYPE 		=  DECODE((v_complaint),'0',NULL,(v_complaint))               
			 , C3100_COMPLAINANT_ID     	=  DECODE(v_complaint_nm,'0',NULL,v_complaint_nm)
			 , C901_STATUS              	=   v_status              
			 , C101_AD_ID               	=  DECODE((v_area_direct),'0',NULL, (v_area_direct))        
			 , C3100_INCIDENT_DT        	=  TO_DATE( v_inc_date ,v_date_fmt)                 
			 , C901_INCIDENT_CATEGORY   	=  DECODE((v_inc_cat),'0',NULL, (v_inc_cat))                   
			 , C101_COMPLAINT_RECEIVED_BY   =  DECODE(v_compl_recv_by,'0',NULL,v_compl_recv_by)                
			 , C3100_RECEIVED_DT            =  TO_DATE( v_compl_recv_dt ,v_date_fmt)                 
			 , C901_RECEIVED_VIA            =  DECODE(v_compl_recv_via,'0',NULL,v_compl_recv_via)               
			 , C101_ORIGINATOR              =   v_originator             
			 , C3100_MDR_REPORTABLE         =  DECODE(v_mdrreportble,'Yes','Y','No','N','')          
			 , C3100_MDR_REPORT_DT          =  TO_DATE( v_mdrreport_dt ,v_date_fmt)                 
			 , C3100_MDR_NUMBER             =  v_mdr_id        
			 , C3100_MEDDEV_REPORTABLE      =  DECODE(v_meddevreport,'Yes','Y','No','N','')       
			 , C3100_MEDDEV_NUMBER          =  v_meddev_id       
			 , C3100_MEDDEV_REPORT_DT       =  TO_DATE( v_meddevreport_dt ,v_date_fmt)             
			 , C3100_DETAIL_EVENT_DESC      =  v_details_desc_event 			                           
			 , C3100_LAST_UPDATED_BY        =  p_user_id      
			 , C3100_LAST_UPDATED_DATE    	=  SYSDATE
			 , C3100_ADDRESS                =  v_address
			 , C3100_PHONE_NUMBER           =  v_phone_number
			 , C3100_COMPANY                =  v_company
		 WHERE C3100_INCIDENT_ID 			=  v_incident_id;
		 
		UPDATE t3101_incident_part_detail   
		   SET C205_PART_NUMBER_ID          =  v_part_number     
			 , C202_PROJECT_ID              =  DECODE(v_project_id,'0',NULL,v_project_id)     
			 , C205_PART_DESC               =  v_part_desc   
			 , C901_PRODUCT_FAMILY          =  DECODE((v_type_product),'0',NULL, (v_type_product))     
			 , C101_GEM_ID                  =  DECODE( (v_gem),'0',NULL, (v_gem))         
			 , C3101_LOT_NUMBER             =  v_lot_number  
			 , C3101_QTY                    =   (v_quantity)       
			 , C901_LOAN_CONS_TYPE          =  DECODE( (v_loanercon_type),'0',NULL, (v_loanercon_type))         
			 , C506_RMA_ID                  =  v_ra_id     
			 , C3101_LAST_UPDATED_BY        =  p_user_id  
			 , C3101_LAST_UPDATED_DATE      =  SYSDATE      
	     WHERE C3100_INCIDENT_ID            =  v_incident_id;
	     
	    UPDATE t3102_incident_detail
	       SET C3102_NORMAL_WEAR_FL         =  DECODE(v_norm_wear_fl,'Yes','Y','No','N','')        
			 , C3102_NORMAL_WEAR_DS         =  v_explain_reason    
			-- , C3102_EVENT_SURGERY_FL       =  DECODE(p_event_surgery_fl,'Yes','Y','No','N','')     
			 , C3102_TYPE_OF_SURGERY        =  v_surgery_type        
			 , C3102_REVISIO_SURGERY_FL     =  DECODE(v_revi_surgery,'Yes','Y','No','N','')        
			 , C3102_SURGEON_NM             =  v_surgeon_nm       
			-- , C3102_ADVERSE_EFFECT_FL      =  DECODE(p_adevrse_effect,'Yes','Y','No','N','')         
			 , C3102_HOSPITAL_NM            =  DECODE(v_hospital,'0',NULL,v_hospital)     
			 , C3102_SURGERY_DT             =  TO_DATE( v_surgey_date ,v_date_fmt)               
			 , C3102_POST_OPT_FL            =  DECODE(v_post_open_fl,'Yes','Y','No','N','')         
			 , C3102_POST_OPT_DT            =  TO_DATE( v_post_open_date ,v_date_fmt)                 
			 , C3102_EVENT_DEMO_FL          =  DECODE(v_event_demo_fl,'Yes','Y','No','N','')          
			 , C3102_REPLACEMENT_FL         =  DECODE(v_replace_fl,'Yes','Y','No','N','')        
			 , C3102_REPLACEMENT_PART_FL    =  DECODE(v_replace_part_fl,'Yes','Y','No','N','')          
			 , C3102_PART_EVAL_FL           =  DECODE(v_part_avail_eval_fl,'Yes','Y','No','N','')          
			 , C3102_DECONTAMINATED_FL      =  DECODE(v_decon_fl,'Yes','Y','No','N','')          
			 , C3102_SHIPPED_FL             =  DECODE(v_shipped_fl,'Yes','Y','No','N','')          
			 , C3102_OTHER_DETAILS          =  v_other_details      			 
			 , C3102_LAST_UPDATED_BY        =  p_user_id         
			 , C3102_LAST_UPDATED_DATE      =  SYSDATE 
			 , C3106_TRACKING_NUMBER		=  v_tracking_number
			 , C3106_PART_SHIPPED_DT		=  TO_DATE( v_part_shipped_dt ,v_date_fmt)
			 , C901_SHIP_CARRIER           	=  DECODE( (v_ship_carrier),'0',NULL, (v_ship_carrier))
			 , C901_SERVICE_REQ_ID			=  DECODE(v_service_req_id,'Yes','Y','No','N','')
			 , C901_EVENT_OCCURED_ID		=  DECODE( (v_event_occured_id),'0',NULL, (v_event_occured_id))
			 , C901_PROC_OUTCOME_ID			=  DECODE( (v_proc_outcome_id),'0',NULL,(v_proc_outcome_id))
			 , C901_ADV_EFFECT_ID			=  DECODE( (v_adv_effect_id),'0',NULL, (v_adv_effect_id))	
		 WHERE C3100_INCIDENT_ID 			= v_incident_id ;
		
		 p_out_incident_id := v_incident_id;
		
		 IF (SQL%ROWCOUNT = 0)
			THEN
				IF v_issueType = '102809' THEN
					v_type := '102790';
					v_ref_id := '12';
					v_log_type := '4000319';
				ELSIF v_issueType = '102810' THEN
					v_type := '102791';
					v_ref_id := '13';
					v_log_type := '4000320';
				END IF;
				
					gm_populate_id_by_year(v_ref_id,v_type,v_incident_id);
					
 				INSERT INTO t3100_incident
						(C3100_INCIDENT_ID,C901_ISSUE_TYPE , C901_COMPLAINANT_TYPE , C3100_COMPLAINANT_ID , C901_STATUS 
					   , C101_AD_ID, C3100_INCIDENT_DT,C901_INCIDENT_CATEGORY, C101_COMPLAINT_RECEIVED_BY , C3100_RECEIVED_DT                  
			           , C901_RECEIVED_VIA, C101_ORIGINATOR, C3100_MDR_REPORTABLE, C3100_MDR_REPORT_DT,C3100_MDR_NUMBER        
			           , C3100_MEDDEV_REPORTABLE, C3100_MEDDEV_NUMBER, C3100_MEDDEV_REPORT_DT, C3100_DETAIL_EVENT_DESC			                           
			           , C3100_CREATED_BY , C3100_CREATED_DATE,C3100_ADDRESS,C3100_PHONE_NUMBER,C3100_COMPANY 
					     )
				VALUES
						(v_incident_id, (v_issueType), DECODE(v_complaint,'0',NULL, v_complaint), DECODE(v_complaint_nm,'0',NULL,v_complaint_nm), v_status
					   , DECODE( (v_area_direct),'0',NULL, (v_area_direct)),TO_DATE( v_inc_date ,v_date_fmt),DECODE( v_inc_cat,'0',NULL, (v_inc_cat)),DECODE(v_compl_recv_by,'0',NULL,v_compl_recv_by), TO_DATE( v_compl_recv_dt ,v_date_fmt)
					   , DECODE( (v_compl_recv_via),'0',NULL, (v_compl_recv_via)),DECODE( (v_originator),'0',NULL, (v_originator)),DECODE(v_mdrreportble,'Yes','Y','No','N','') , TO_DATE( v_mdrreport_dt ,v_date_fmt),v_mdr_id
					   , DECODE(v_meddevreport,'Yes','Y','No','N','') , v_meddev_id , TO_DATE( v_meddevreport_dt ,v_date_fmt) , v_details_desc_event
					   , p_user_id , SYSDATE,v_address,v_phone_number,v_company
						);
				INSERT INTO t3101_incident_part_detail
						(C3100_INCIDENT_ID,C205_PART_NUMBER_ID , C202_PROJECT_ID , C205_PART_DESC , C901_PRODUCT_FAMILY 
					   , C101_GEM_ID, C3101_LOT_NUMBER,C3101_QTY, C901_LOAN_CONS_TYPE , C506_RMA_ID			           		                           
			           , C3101_CREATED_BY , C3101_CREATED_DATE
					     )
				VALUES
						(v_incident_id,v_part_number, DECODE(v_project_id,'0',NULL,v_project_id), v_part_desc,DECODE(v_type_product,'0',NULL,v_type_product)
					   , DECODE(v_gem,'0',NULL,v_gem),v_lot_number,(v_quantity),DECODE(v_loanercon_type,'0',NULL,v_loanercon_type), v_ra_id
					   , p_user_id , SYSDATE
						);	
				
				INSERT INTO t3102_incident_detail
						(C3100_INCIDENT_ID,C3102_NORMAL_WEAR_FL , C3102_NORMAL_WEAR_DS  , C3102_TYPE_OF_SURGERY         
			           , C3102_REVISIO_SURGERY_FL , C3102_SURGEON_NM,  C3102_HOSPITAL_NM , C3102_SURGERY_DT               
			           , C3102_POST_OPT_FL, C3102_POST_OPT_DT, C3102_EVENT_DEMO_FL, C3102_REPLACEMENT_FL, C3102_REPLACEMENT_PART_FL          
			           , C3102_PART_EVAL_FL, C3102_DECONTAMINATED_FL, C3102_SHIPPED_FL, C3102_OTHER_DETAILS, C3102_CREATED_BY, C3102_CREATED_DATE
			           , C3106_TRACKING_NUMBER, C3106_PART_SHIPPED_DT, C901_SHIP_CARRIER, C901_SERVICE_REQ_ID, C901_EVENT_OCCURED_ID
			           , C901_PROC_OUTCOME_ID,C901_ADV_EFFECT_ID     
					     )
				VALUES
						(v_incident_id,DECODE(v_norm_wear_fl,'Yes','Y','No','N',''), v_explain_reason,v_surgery_type
					   , DECODE(v_revi_surgery,'Yes','Y','No','N',''),v_surgeon_nm,DECODE(v_hospital,'0',NULL,v_hospital), TO_DATE( v_surgey_date ,v_date_fmt)
					   , DECODE(v_post_open_fl,'Yes','Y','No','N','') ,TO_DATE( v_post_open_date ,v_date_fmt) ,DECODE(v_event_demo_fl,'Yes','Y','No','N','')  ,DECODE(v_replace_fl,'Yes','Y','No','N','') ,DECODE(v_replace_part_fl,'Yes','Y','No','N','')
					   , DECODE(v_part_avail_eval_fl,'Yes','Y','No','N','') ,DECODE(v_decon_fl,'Yes','Y','No','N','')  , DECODE(v_shipped_fl,'Yes','Y','No','N','') , v_other_details
					   , p_user_id , SYSDATE, v_tracking_number,TO_DATE( v_part_shipped_dt ,v_date_fmt),DECODE(v_ship_carrier,'0',NULL,v_ship_carrier),DECODE(v_service_req_id,'Yes','Y','No','N','')
					   , DECODE(v_event_occured_id,'0',NULL,v_event_occured_id),DECODE(v_proc_outcome_id,'0',NULL,v_proc_outcome_id),DECODE(v_adv_effect_id,'0',NULL,v_adv_effect_id)
						);
				
				gm_pkg_qa_com_rsr_txn.gm_upd_com_rsr_status(v_incident_id,p_user_id);
				
				gm_save_log (v_incident_id , 'COM/RSR Initiated', v_log_type, p_user_id, NULL, v_message);
				
				IF v_ra_id IS NOT NULL AND v_incident_id IS NOT NULL THEN
					 UPDATE T506B_RA_INCIDENT_LINK 
             		    SET c901_incident_link_status = '102850'  -- COM/RSR INITIATED.
                          , c506b_last_updated_by 	  = p_user_id
                          , c506b_last_updated_date   = sysdate
                      WHERE c506_rma_id = v_ra_id
                        AND c205_part_number_id = v_part_number
                        AND c506b_void_fl IS NULL;
				END IF;
					p_out_incident_id := v_incident_id; 
			END IF;	 
	     
	    END gm_sav_incident_info;
	    
	 /**********************************************************************************************
	  *	Description 	: THIS PROCEDURE is used Update the Incident Status in t3100_incident table
 	  *	Parameters		: p_incident_id, p_user_id
 	  *	Author          : HReddi
	  **********************************************************************************************/
		PROCEDURE gm_upd_com_rsr_status (
			p_incident_id	   IN	t908_gm_nextval.c908_ref_id%TYPE,
			p_user_id 		   IN   t3102_incident_detail.C3102_LAST_UPDATED_BY%TYPE
		)AS   
			v_incident_status 		T3100_INCIDENT.C901_STATUS%TYPE;
			v_issue_type 		    T3100_INCIDENT.C901_ISSUE_TYPE%TYPE;
			v_rma_id 				VARCHAR2(20);
			v_compl_closed_dt 		DATE;	
			v_status 				T3100_INCIDENT.C901_STATUS%TYPE;
			v_return_status 		VARCHAR2(10);
			v_send_eval_date 		DATE;
			v_pervious_status		T3100_INCIDENT.C901_PREV_STATUS%TYPE;
			
	    	BEGIN
		    	
		          SELECT t3100.c901_status ,  t3101.c506_rma_id  , t3105.c3105_closed_dt , t3100.c901_issue_type , t3100.c901_prev_status 
		    	    INTO v_incident_status , v_rma_id , v_compl_closed_dt  , v_issue_type , v_pervious_status
                    FROM t3100_incident t3100 
                       , t3101_incident_part_detail t3101 
                       , t3105_incident_closure_dtl t3105
                   WHERE t3100.c3100_incident_id  = t3101.C3100_INCIDENT_ID 
                     AND t3105.c3100_incident_id(+) = t3100.c3100_incident_id
                     AND t3101.c3100_incident_id  = p_incident_id 
                     AND t3101.C3101_VOID_FL  IS NULL 
                     AND t3105.C3105_VOID_FL  IS NULL 
                     AND t3100.C3100_VOID_FL  IS NULL; 	                                     
                
                 IF v_compl_closed_dt IS NOT NULL THEN
                 	v_status := 102841;  -- CLOSED
                 ELSE                 	
                 	IF v_incident_status IS NULL AND v_pervious_status IS NULL THEN  
                 		BEGIN
	                 	 SELECT DECODE(c506_status_fl,'0','102837','1','102837','2','102838')  -- 102837 [PENDING RETURN] , 102838 [PENDING PICK]
                 		  	INTO v_status 
                 		  	FROM t506_returns
                           WHERE c506_rma_id = v_rma_id
                             AND c506_void_fl IS NULL ;  
                         EXCEPTION WHEN NO_DATA_FOUND THEN
                        	v_status := '4000321';   -- INPROGRESS
                        END;                        
                 	ELSIF v_incident_status = '102837' OR v_incident_status = '102838' OR v_incident_status = '4000321' THEN
                 	   BEGIN
                 		  SELECT DECODE(c3103_sent_to_eval_dt,NULL,v_incident_status ,102839) 
                            INTO v_status 
                            FROM T3103_INCIDENT_QA_EVAL 
                           WHERE c3100_incident_id = p_incident_id 
                             AND c3103_void_fl IS NULL;
                       EXCEPTION WHEN  NO_DATA_FOUND
                       THEN 
                          v_status := v_incident_status;  
                       END;                                	
                    ELSIF v_incident_status = '102839'  THEN
                 		BEGIN 
                 		  SELECT DECODE(c3104_evaluator_rec_dt,NULL,v_incident_status,102840)
                 		    INTO v_status
                 		    FROM t3104_incident_eng_eval 
                 		   WHERE c3100_incident_id = p_incident_id
                             AND c3104_void_fl IS NULL;
                         EXCEPTION  WHEN  NO_DATA_FOUND
                        THEN 
                          v_status := v_incident_status;  
                       END;
                    ELSE
                           v_status := NVL(v_incident_status, v_pervious_status); 
                 	END IF;
                 END IF;            
             
                 UPDATE t3100_incident 
                    SET c901_status = v_status
                      , c901_prev_status = v_incident_status
                      , c3100_last_updated_by = p_user_id 
                      , c3100_last_updated_date = SYSDATE 
                  WHERE c3100_incident_id = p_incident_id
                    AND C3100_VOID_FL IS NULL;      
           END gm_upd_com_rsr_status;
		    
		    
	    /****************************************************************
		*	Description 	: This Procedure is used to save QA Evaluation details
 		*	Author          : arajan
		*************************************************************** */
	    PROCEDURE gm_sav_QAEvaluation (       
    		p_incident_id    	IN      t3103_incident_qa_eval.c3100_incident_id%type,
    		p_dhr_ncmr_rev   	IN 		t3103_incident_qa_eval.c3103_dhr_ncmr_rev%type,
    		p_capa_scar_rev  	IN 		t3103_incident_qa_eval.c3103_capa_scar_rev%type,
    	    p_comp_rev    		IN 		t3103_incident_qa_eval.c3103_comp_review%type,
    	    p_rev_by    		IN 		t3103_incident_qa_eval.c101_reviewed_by%type,
    	    p_rev_dt    		IN 		t3103_incident_qa_eval.c3103_review_dt%type,
    		p_part_rec   		IN 		t3103_incident_qa_eval.c3103_part_receive_dt%type,
    		p_eval_send  		IN 		t3103_incident_qa_eval.c3103_sent_to_eval_dt%type,
    	    p_dhr_dev_fl    	IN 		t3103_incident_qa_eval.c3103_deviation_dhr_fl%type,
    	    p_quest_fl    		IN 		t3103_incident_qa_eval.c3103_questionnaire_fl%type,
    	    p_storage_fl    	IN 		t3103_incident_qa_eval.c3103_proper_storage_fl%type,
    	    p_pack_fl    		IN 		t3103_incident_qa_eval.c3103_package_insert_fl%type,
    	    p_market_fl    		IN 		t3103_incident_qa_eval.c3103_market_removal_fl%type,
    		p_risk_fl  			IN 		t3103_incident_qa_eval.c3103_risk_analysis_fl%type,
    		p_ncmr_scar_fl  	IN 		t3103_incident_qa_eval.c3103_ncmr_scar_fl%type,
    	    p_ncmr_scar_num    	IN 		t3103_incident_qa_eval.c3103_ncmr_scar_no%type,
    	    p_capa_req_fl    	IN 		t3103_incident_qa_eval.c3103_capa_req_fl%type,
    		p_capa_no  			IN 		t3103_incident_qa_eval.c3103_capa_no%type,
    		p_qa_rev_by  		IN 		t3103_incident_qa_eval.c101_qa_reviewed_by%type,
    	    p_qa_rev_dt    		IN 		t3103_incident_qa_eval.c3103_qa_review_dt%type,
    	    p_created_by    	IN 		t3103_incident_qa_eval.c3103_created_by%type,
    	    p_out_Fl			OUT		varchar2,
    	    p_out_eval_dt		OUT 	varchar2
    	)
    	AS
    		v_out_Fl varchar2(2) := 'N';
    		v_out_eval_dt t3103_incident_qa_eval.c3103_sent_to_eval_dt%type;
    		v_eval_send_cnt NUMBER;
		BEGIN
			
			IF (p_eval_send IS NOT NULL) 
			THEN
				BEGIN
					SELECT DECODE(C3103_SENT_TO_EVAL_DT,NULL,1,trunc (C3103_SENT_TO_EVAL_DT - p_eval_send))  dtevalsend
					INTO v_eval_send_cnt 
					FROM T3103_INCIDENT_QA_EVAL
					WHERE C3100_INCIDENT_ID = p_incident_id
					AND C3103_VOID_FL IS NULL;
				
					EXCEPTION WHEN NO_DATA_FOUND 
					THEN
	                	v_eval_send_cnt := '1';
	        	END;
	        v_out_eval_dt := p_eval_send + GET_RULE_VALUE('EVALDATE','QAEVAL');
	        p_out_eval_dt := TO_CHAR(v_out_eval_dt,get_rule_value ('DATEFMT', 'DATEFORMAT')); 
	       	END IF;  
			
			
			
			
			UPDATE T3103_INCIDENT_QA_EVAL
			SET
				 C3103_DHR_NCMR_REV			= p_dhr_ncmr_rev
				,C3103_CAPA_SCAR_REV		= p_capa_scar_rev
				,C3103_COMP_REVIEW		 	= p_comp_rev
				,C101_REVIEWED_BY			= p_rev_by
				,C3103_REVIEW_DT			= p_rev_dt
				,C3103_PART_RECEIVE_DT		= p_part_rec
				,C3103_SENT_TO_EVAL_DT		= p_eval_send
				,C3103_DEVIATION_DHR_FL		= DECODE(p_dhr_dev_fl,'Yes','Y','No','N','N/A','NA')
				,C3103_QUESTIONNAIRE_FL		= DECODE(p_quest_fl,'Yes','Y','No','N','N/A','NA')
				,C3103_PROPER_STORAGE_FL	= DECODE(p_storage_fl,'Yes','Y','No','N','N/A','NA')
				,C3103_PACKAGE_INSERT_FL	= DECODE(p_pack_fl,'Yes','Y','No','N','N/A','NA')
				,C3103_MARKET_REMOVAL_FL	= DECODE(p_market_fl,'Yes','Y','No','N','N/A','NA')
				,C3103_RISK_ANALYSIS_FL	 	= DECODE(p_risk_fl,'Yes','Y','No','N','N/A','NA')
				,C3103_NCMR_SCAR_FL			= DECODE(p_ncmr_scar_fl,'Yes','Y','No','N','N/A','NA')	
				,C3103_NCMR_SCAR_NO			= p_ncmr_scar_num
				,C3103_CAPA_REQ_FL			= DECODE(p_capa_req_fl,'Yes','Y','No','N','N/A','NA')
				,C3103_CAPA_NO			 	= p_capa_no	 
				,C101_QA_REVIEWED_BY		= p_qa_rev_by
				,C3103_QA_REVIEW_DT			= p_qa_rev_dt
				,C3103_LAST_UPDATED_BY		= p_created_by
				,C3103_LAST_UPDATED_DATE	= SYSDATE
			WHERE C3100_INCIDENT_ID = p_incident_id
			AND C3103_VOID_FL IS NULL;
			
			IF (SQL%ROWCOUNT = 0)
			THEN
			
			 INSERT INTO T3103_INCIDENT_QA_EVAL 
			 		(C3100_INCIDENT_ID, C3103_DHR_NCMR_REV, C3103_CAPA_SCAR_REV, C3103_COMP_REVIEW
			 		 ,C101_REVIEWED_BY, C3103_REVIEW_DT, C3103_PART_RECEIVE_DT, C3103_SENT_TO_EVAL_DT
			 		 ,C3103_DEVIATION_DHR_FL, C3103_QUESTIONNAIRE_FL, C3103_PROPER_STORAGE_FL
			 		 ,C3103_PACKAGE_INSERT_FL, C3103_MARKET_REMOVAL_FL, C3103_RISK_ANALYSIS_FL
			 		 ,C3103_NCMR_SCAR_FL, C3103_NCMR_SCAR_NO, C3103_CAPA_REQ_FL, C3103_CAPA_NO
			 		 ,C101_QA_REVIEWED_BY, C3103_QA_REVIEW_DT, C3103_CREATED_BY, C3103_CREATED_DATE
			 		)
			 VALUES (p_incident_id, p_dhr_ncmr_rev, p_capa_scar_rev, p_comp_rev
			 		 , p_rev_by, p_rev_dt, p_part_rec, p_eval_send
			 		 , DECODE(p_dhr_dev_fl,'Yes','Y','No','N','N/A','NA'), DECODE(p_quest_fl,'Yes','Y','No','N','N/A','NA'), DECODE(p_storage_fl,'Yes','Y','No','N','N/A','NA')
			 		 , DECODE(p_pack_fl,'Yes','Y','No','N','N/A','NA'), DECODE(p_market_fl,'Yes','Y','No','N','N/A','NA'), DECODE(p_risk_fl,'Yes','Y','No','N','N/A','NA')
			 		 , DECODE(p_ncmr_scar_fl,'Yes','Y','No','N','N/A','NA'), p_ncmr_scar_num, DECODE(p_capa_req_fl,'Yes','Y','No','N','N/A','NA'), p_capa_no
			 		 , p_qa_rev_by, p_qa_rev_dt, p_created_by, SYSDATE
			 		 );
			 		 
			END IF;	    
			
			IF (v_eval_send_cnt <> '0')
			THEN
				v_out_Fl := 'Y';				
			END IF;
			gm_pkg_qa_com_rsr_txn.gm_upd_com_rsr_status(p_incident_id,p_created_by);
			p_out_Fl := v_out_Fl;
			
		END gm_sav_QAEvaluation;
		/**********************************************************************************************
	  *	Description 	: THIS PROCEDURE is used Update the Incident Status in t3100_incident table
 	  *	Parameters		: p_incident_id, p_user_id
 	  *	Author          : HReddi
	  **********************************************************************************************/
		PROCEDURE gm_sav_EnggEvaluation(
    		 p_Incident_id   IN   T3100_INCIDENT.C3100_INCIDENT_ID%TYPE
    		,p_Date          IN   T3104_INCIDENT_ENG_EVAL.C3104_EVALUATOR_REC_DT%TYPE 
    		,p_Eval_Res      IN   T3104_INCIDENT_ENG_EVAL.C3104_EVALUATION_RESULT%TYPE
    		,p_Eval_Id       IN   T3104_INCIDENT_ENG_EVAL.C101_EVALUATOR_ID%TYPE
    		,p_Com_Rsr_Disp  IN   T3104_INCIDENT_ENG_EVAL.C901_COM_RSR_DISP_TYPE%TYPE
    		,p_Prod_Disp     IN   T3104_INCIDENT_ENG_EVAL.C901_PRODUCT_DISP_TYPE%TYPE
    		,p_Comments      IN   T3104_INCIDENT_ENG_EVAL.C3104_CAPA_COMMENTS%TYPE
    		,p_user_id       IN   T3104_INCIDENT_ENG_EVAL.C3104_CREATED_BY%TYPE
    		,p_out_Incident_id OUT T3100_INCIDENT.C3100_INCIDENT_ID%TYPE
    		
   			)
      AS
      	v_Incident_id T3100_INCIDENT.C3100_INCIDENT_ID%TYPE := p_Incident_id;
 		 BEGIN
	 		 UPDATE T3104_INCIDENT_ENG_EVAL
	      		SET
	      		 C3104_EVALUATOR_REC_DT    = p_Date
	      		,C3104_EVALUATION_RESULT   = p_Eval_Res
	      		,C101_EVALUATOR_ID         = DECODE(p_Eval_Id,0,NULL,p_Eval_Id)
	      		,C901_COM_RSR_DISP_TYPE    = DECODE(p_Com_Rsr_Disp,0,NULL,p_Com_Rsr_Disp)
	      		,C901_PRODUCT_DISP_TYPE    = DECODE(p_Prod_Disp,0,NULL,p_Prod_Disp)
	      		,C3104_CAPA_COMMENTS       = p_Comments
	      		,C3104_LAST_UPDATED_BY     = p_user_id
	 			,C3104_LAST_UPDATED_DATE   = sysdate
	 		WHERE C3100_INCIDENT_ID    = v_Incident_id;
	 		 
	 		IF (SQL%ROWCOUNT = 0)
			THEN
			
			INSERT INTO T3104_INCIDENT_ENG_EVAL
	      	                     (C3100_INCIDENT_ID
	      	                     ,C3104_EVALUATOR_REC_DT
	      	                     ,C3104_EVALUATION_RESULT
	      	                     ,C101_EVALUATOR_ID
	      	                     ,C901_COM_RSR_DISP_TYPE
	      	                     ,C901_PRODUCT_DISP_TYPE
	      	                     ,C3104_CAPA_COMMENTS
	      	                     ,C3104_CREATED_BY
	      	                     ,C3104_CREATED_DATE
 								)
	         VALUES 
	     						(v_Incident_id
	     						,p_Date
	     						,p_Eval_Res
	     						,DECODE(p_Eval_Id,0,NULL,p_Eval_Id)
	     						,DECODE(p_Com_Rsr_Disp,0,NULL,p_Com_Rsr_Disp) 
	     						,DECODE(p_Prod_Disp,0,NULL,p_Prod_Disp)
	     						,p_Comments
	     						,p_user_id
	     						, SYSDATE
	     						);
	     						
              					
              END IF;
              gm_pkg_qa_com_rsr_txn.gm_upd_com_rsr_status(v_Incident_id,p_user_id);
              
              p_out_Incident_id := v_Incident_id;
	   
	     END gm_sav_EnggEvaluation;  
	     /**********************************************************************************************
	  *	Description 	: THIS PROCEDURE is used Update the Incident Status in t3100_incident table
 	  *	Parameters		: p_incident_id, p_user_id
 	  *	Author          : HReddi
	  **********************************************************************************************/
	     PROCEDURE gm_sav_close_COMRSR (       
    		p_comrsr_id    	IN      t3105_incident_closure_dtl.c3100_incident_id%type,
    		p_closed_by   	IN 		t3105_incident_closure_dtl.c101_closed_by%type,
    		p_closed_date  	IN 		t3105_incident_closure_dtl.c3105_closed_dt%type,
    	    p_comments    	IN 		t3105_incident_closure_dtl.c3105_corrective_action%type,
    	    p_close_cmts   	IN 		t3105_incident_closure_dtl.c3105_closure_comments%type,
    	   	p_userid  		IN 		t3105_incident_closure_dtl.c3105_created_by%type,
    		p_out_Incident_id OUT   t3100_incident.c3100_incident_id%type
     	)
     	AS
     	v_comrsr_id  t3105_incident_closure_dtl.c3100_incident_id%type := p_comrsr_id;
     	v_issue      t3100_incident.c901_issue_type%type;
     	v_status     t3100_incident.c901_status%type;
     	v_message    VARCHAR2(20);
		 BEGIN
			 
			 
			  UPDATE t3105_incident_closure_dtl
				 SET C101_CLOSED_BY = p_closed_by
	                 ,C3105_CLOSED_DT =  p_closed_date
	                 ,C3105_CORRECTIVE_ACTION = p_comments
					 ,C3105_CLOSURE_COMMENTS = p_close_cmts
	                 ,C3105_LAST_UPDATED_BY = p_userid
	                 ,C3105_LAST_UPDATED_DATE = SYSDATE
	           WHERE C3100_INCIDENT_ID = v_comrsr_id 
	             AND C3105_VOID_FL IS NULL;
			
			 IF (SQL%ROWCOUNT = 0)
			 THEN
				INSERT INTO t3105_incident_closure_dtl
				(
                    C3100_INCIDENT_ID, C101_CLOSED_BY, C3105_CLOSED_DT
                  , C3105_CORRECTIVE_ACTION, C3105_CLOSURE_COMMENTS, C3105_VOID_FL
                  , C3105_CREATED_BY, C3105_CREATED_DATE
                )
                VALUES
                (
                   v_comrsr_id, p_closed_by, p_closed_date
                  , p_comments, p_close_cmts, NULL
                  , p_userid, SYSDATE
                ) ;
                
			 END IF;	 
			 gm_pkg_qa_com_rsr_txn.gm_upd_com_rsr_status(p_comrsr_id,p_userid);--To set the status
			 
			  SELECT  c901_issue_type , c901_status INTO v_issue,v_status  FROM t3100_incident
			 	WHERE c3100_incident_id  = v_comrsr_id  AND c3100_void_fl IS NULL ;
			 
			 IF v_status = '102841' THEN		--102841-closed
			    IF v_issue = '102810' THEN       --102810 - RSR
			        gm_update_log (v_comrsr_id, 'RSR Closed', 4000320, p_userid, v_message);			
		        ELSE
			        gm_update_log (v_comrsr_id, 'COM Closed', 4000319, p_userid, v_message);
			    END IF;
		     END IF;
			 p_out_Incident_id := v_comrsr_id;
			
			                   
		 END gm_sav_close_COMRSR;

		 /****************************************************************
    	* Description : Procedure to convert RA to QA evaluvation
    	* Parameters  : p_rma_id, p_user_id
    	* Author      : rdinesh
    	*****************************************************************/	
			PROCEDURE gm_upd_qa_eval(
			    p_rma_id IN t506_returns.C506_RMA_ID%TYPE,
			    p_userid IN t506b_ra_incident_link.c506b_created_by%TYPE 
			 )
			AS
			
			  v_type 		t506_returns.c506_type%TYPE;
			  v_reason 		t506_returns.c506_reason%TYPE;
			  v_count 		NUMBER;
			  v_reasonid 	t506_returns.c506_reason%TYPE;
			  v_rule_value  varchar2(20);
			  CURSOR item_cursor
			  IS
			  
			    SELECT  T507.c506_rma_id rmaid,
			      		T507.c205_part_number_id partid,
			      		SUM(T507.c507_item_qty) itemqty,
			      		T506.c506_created_by createby
			      FROM  t507_returns_item T507,
			      		t506_returns T506
			     WHERE  T507.c506_rma_id  IN (p_rma_id)
			       AND  T507.c507_status_fl IN ('Q','C')--'Q'-Initiate ,'C'-credited
			       AND  T507.c506_rma_id     = T506.c506_rma_id
			       AND  T506.c506_void_fl   IS NULL
			  GROUP BY  T507.c506_rma_id,
			      		T507.c205_part_number_id,
			      		T506.c506_created_by;
			      
			BEGIN
				
			  BEGIN
				  
			    SELECT c506_type,
			           c506_reason
			      INTO v_type,
			      	   v_reason
			      FROM t506_returns
			     WHERE c506_rma_id = p_rma_id
			       AND c506_void_fl IS NULL;
			    
			  EXCEPTION
			  WHEN NO_DATA_FOUND THEN
			  
			    v_type  := '';
			    v_reason:= '';
			    
			  END;
			  
			  --3301 - Consignment - Sets   3302 - Consignment - Items     3300 - Sales - Items   3307 - ICT returns - Sets   26240177 - ICT returns - Items
			  v_rule_value	:= NVL(get_rule_value (v_type, 'SHOW-ICT-RETRUNS'),'N');
			  IF (v_rule_value <> 'Y')
			    THEN
			    GM_RAISE_APPLICATION_ERROR('-20999','385',p_rma_id);
			   
			  END IF;

			  --For linking the RA to the COM/RSR pending dash which is already in QA Evaluvation
			  IF (v_reason = '3253') --3253-QA Evaluation
			    THEN
			    SELECT COUNT(1)
			      INTO v_count
			      FROM T506B_RA_INCIDENT_LINK
			     WHERE c506_rma_id  =p_rma_id
			       AND c506b_void_fl IS NULL;
			    
			    IF (v_count > 0) THEN
			    GM_RAISE_APPLICATION_ERROR('-20999','386',p_rma_id);  
			    END IF;
			  END IF;
		  
			  --To know the previeous reason of the RA ID
			  UPDATE t907_cancel_log
				 SET c907_comments = 'Converted from '
 			     || get_code_name(v_reason)
                 || '. '
                 || c907_comments
              WHERE c907_ref_id = p_rma_id
                AND c901_type   = 105020;

			  UPDATE t506_returns
			     SET c506_reason = 3253 --QA Evaluation
			       , c506_last_updated_by   = p_userid 
			       , c506_last_updated_date = SYSDATE
			   WHERE c506_rma_id        = p_rma_id
			     AND c506_void_fl        IS NULL;

			  BEGIN
				  
			    FOR currindex IN item_cursor
			    LOOP
			      gm_pkg_qa_com_rsr_txn.gm_sav_ra_incident_link(currindex.rmaid, currindex.partid, currindex.itemqty , currindex.createby);
			    END LOOP;
			    
			  END;
			  
			END gm_upd_qa_eval;  
			
			
		/****************************************************************
    	* Description : Procedure to convert RA to Field Corrective Action
    	* Parameters  : p_rma_id, p_user_id
    	* Author      : AGILAN
    	*****************************************************************/	
			PROCEDURE gm_upd_QA_eval_fca(
			    p_rma_id IN t506_returns.C506_RMA_ID%TYPE,
			    p_userid IN t506b_ra_incident_link.c506b_created_by%TYPE 
			 )
			AS
			
			  v_type   		t506_returns.c506_type%TYPE;
			  v_reason 		t506_returns.c506_reason%TYPE;
			  v_count  		NUMBER;
			  v_reasonid 	t506_returns.c506_reason%TYPE;
			  v_rule_value  varchar2(20);
			  CURSOR item_cursor
			  IS
			  
			    SELECT 	T507.c506_rma_id rmaid,
			      		T507.c205_part_number_id partid,
			      		SUM(T507.c507_item_qty) itemqty,
			      		T506.c506_created_by createby
			      FROM 	t507_returns_item T507,
			      		t506_returns T506
			     WHERE  T507.c506_rma_id  IN (p_rma_id)
			       AND 	T507.c507_status_fl IN ('Q','C')--'Q'-Initiate ,'C'-credited
			       AND 	T507.c506_rma_id     = T506.c506_rma_id
			       AND 	T506.c506_void_fl   IS NULL
			  GROUP BY 	T507.c506_rma_id,
			      		T507.c205_part_number_id,
			      		T506.c506_created_by;
			      
			BEGIN
				
			  BEGIN
				  
			    SELECT c506_type,
			           c506_reason
			      INTO v_type,
			           v_reason
			      FROM t506_returns
			     WHERE c506_rma_id = p_rma_id
			       AND c506_void_fl IS NULL;
			    
			  EXCEPTION
			  WHEN NO_DATA_FOUND THEN
			  
			    v_type  := '';
			    v_reason:= '';
			    
			  END;
			  
			  --3301 - Consignment - Sets   3302 - Consignment - Items     3300 - Sales - Items   3307 - ICT returns - Sets   26240177 - ICT returns - Items
			  v_rule_value	:= NVL(get_rule_value (v_type, 'SHOW-ICT-RETRUNS'),'N');
			  IF (v_rule_value <> 'Y')
			    THEN
			    GM_RAISE_APPLICATION_ERROR('-20999','385',p_rma_id);
			   
			  END IF;
			  --For linking the RA to the COM/RSR pending dash which is already in Field Corrective Action
			  IF (v_reason = '26240385') --Field Coorective Action
			    THEN

			    SELECT COUNT(1)
			    INTO v_count
			    FROM T506B_RA_INCIDENT_LINK
			    WHERE c506_rma_id  =p_rma_id
			    AND c506b_void_fl IS NULL;
			    
			    IF (v_count        > 0) THEN
			    GM_RAISE_APPLICATION_ERROR('-20999','412',p_rma_id);   
			    END IF;
			    
			  END IF;
		  
			  --To know the previeous reason of the RA ID
			  UPDATE t907_cancel_log
				 SET c907_comments = 'Converted from '
 			     || get_code_name(v_reason)
                 || '. '
                 || c907_comments
              WHERE c907_ref_id = p_rma_id
                AND c901_type   = 105020;

			  UPDATE t506_returns
			  SET c506_reason = 26240385 --Field Coorective Action
			    ,c506_last_updated_by   = p_userid 
			    ,c506_last_updated_date = SYSDATE
			  WHERE c506_rma_id        = p_rma_id
			  AND c506_void_fl        IS NULL;
			  BEGIN
				  
			    FOR currindex IN item_cursor
			    LOOP
			      gm_pkg_qa_com_rsr_txn.gm_sav_ra_incident_link(currindex.rmaid, currindex.partid, currindex.itemqty , currindex.createby);
			    END LOOP;
			    
			  END;
			  
			END gm_upd_QA_eval_fca; 
			
		END gm_pkg_qa_com_rsr_txn;
	/