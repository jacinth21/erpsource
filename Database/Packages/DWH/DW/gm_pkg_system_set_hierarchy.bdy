/* Formatted on 2010/04/30 17:51 (Formatter Plus v4.8.0) */
CREATE OR REPLACE PACKAGE BODY gm_pkg_system_set_hierarchy
IS
/*PROCEDURE CREATE_SET_HIERARCHY_DIM
AS
BEGIN

DROP TABLE SYSTEM_SET_HIERARCHY_DIM;

CREATE TABLE SYSTEM_SET_HIERARCHY_DIM
(
PARENT_SET_ID VARCHAR2(20)
,CHILD_SET_ID VARCHAR2(20)
,DEPTH NUMBER
,ISLEAF CHAR(1)
,ISROOT CHAR(1)
,HIERARCHY_DEPTH NUMBER
);

END CREATE_SET_HIERARCHY_DIM;

CREATE TABLE SYSTEM_SET_HIERARCHY_DIM
(
SYSTEM_ID VARCHAR2(20)
, LEVEL1_TYPE  VARCHAR2(20)
, LEVEL_1_SETID  VARCHAR2(20)
, LEVEL2_TYPE  VARCHAR2(20)
, LEVEL_2_SETID  VARCHAR2(20)
, LEVEL_3_TYPE	VARCHAR2(20)
, LEVEL_3_SETID  VARCHAR2(20)
, ACTUAL_SET_TYPE	VARCHAR2(20)
, ACTUAL_SET_ID  VARCHAR2(20)
);

*/
	/*PROCEDURE set_bridge_mapping
	AS
		v_system_suffix NUMBER;
		v_root_set_id  t207_set_master.c207_set_id%TYPE;
		p_parent_set_id t207_set_master.c207_set_id%TYPE;
		v_parent_set_id t207_set_master.c207_set_id%TYPE;
		v_child_set_id t207_set_master.c207_set_id%TYPE;
		v_depth 	   system_set_hierarchy_dim.DEPTH%TYPE;
		v_isleaf	   system_set_hierarchy_dim.isleaf%TYPE;
		v_isroot	   system_set_hierarchy_dim.isroot%TYPE;
		v_hierarchy    system_set_hierarchy_dim.hierarchy_level%TYPE;
		v_sysroot	   NUMBER;

		CURSOR cur_set_list_1600
		IS
			SELECT t207.c207_set_id root_set_id
			  FROM t207_set_master t207
			 WHERE t207.c901_set_grp_type = 1600 AND t207.c207_void_fl IS NULL;

		CURSOR cur_node
		IS
			SELECT node, 0 sysroot
			  FROM (SELECT	   t207a.c207_link_set_id node, connect_by_isleaf leaf
						  FROM t207a_set_link t207a
					START WITH c207_main_set_id = v_root_set_id
					CONNECT BY PRIOR t207a.c207_link_set_id = c207_main_set_id AND t207a.c901_type <> 20004) nodet
			 WHERE nodet.leaf = 0
			UNION
			SELECT v_root_set_id node, 1 sysroot
			  FROM DUAL;

		CURSOR cur_parent_child_info
		IS
			SELECT DISTINCT t207a.c207_main_set_id parent_set_id, t207a.c207_link_set_id child_set_id, LEVEL lvl
						  , DECODE (connect_by_isleaf, 1, 'Y', 'N') isleaf
					   FROM t207a_set_link t207a
				 START WITH c207_main_set_id = p_parent_set_id
				 CONNECT BY PRIOR t207a.c207_link_set_id = c207_main_set_id AND t207a.c901_type <> 20004;
	BEGIN
		FOR set_val_1600 IN cur_set_list_1600
		LOOP
			v_root_set_id := set_val_1600.root_set_id;
			set_bridge_master (v_root_set_id, v_root_set_id, 0, 'N', 'Y', 'SYSTEM');

			FOR node_list IN cur_node
			LOOP
				p_parent_set_id := node_list.node;
				v_sysroot	:= node_list.sysroot;
				v_hierarchy := get_set_hierarchy (p_parent_set_id);

				FOR set_parent_child_info IN cur_parent_child_info
				LOOP
					v_parent_set_id := p_parent_set_id;
					v_child_set_id := set_parent_child_info.child_set_id;
					v_depth 	:= set_parent_child_info.lvl;
					v_isleaf	:= set_parent_child_info.isleaf;
					v_isroot	:= 'N';
					v_hierarchy := get_set_hierarchy (v_child_set_id);
					set_bridge_master (v_parent_set_id
									 , v_child_set_id
									 , v_depth
									 , v_isleaf
									 , v_isroot
									 , get_code_name_alt (v_hierarchy)
									  );

					IF v_sysroot = 1
					THEN
						set_bridge_master (v_child_set_id, v_child_set_id, 0, 'N', 'N'
										 , get_code_name_alt (v_hierarchy));
					END IF;
				END LOOP;
			END LOOP;
		END LOOP;
	END set_bridge_mapping;

	PROCEDURE set_bridge_master (
		v_parent_set_id   system_set_hierarchy_dim.parent_set_id%TYPE
	  , v_child_set_id	  system_set_hierarchy_dim.child_set_id%TYPE
	  , v_depth 		  system_set_hierarchy_dim.DEPTH%TYPE
	  , v_isleaf		  system_set_hierarchy_dim.isleaf%TYPE
	  , v_isroot		  system_set_hierarchy_dim.isroot%TYPE
	  , v_hierarchy 	  system_set_hierarchy_dim.hierarchy_level%TYPE
	)
	AS
	BEGIN
		INSERT INTO system_set_hierarchy_dim
			 VALUES ('A_' || v_parent_set_id, 'A_' || v_child_set_id, v_parent_set_id, v_child_set_id, v_depth
				   , v_isleaf, v_isroot, v_hierarchy);
	END set_bridge_master;
*/
	FUNCTION get_set_hierarchy (
		p_set_id   t207_set_master.c207_set_id%TYPE
	)
		RETURN VARCHAR2
	IS
/*	Description 	: THIS FUNCTION RETURNS HIERARCHY OF THE GIVEN SET
 Parameters 		: p_code_id
*/
		v_hierarchy    system_set_hierarchy_dim.level1_type%TYPE;
	BEGIN
		BEGIN
			SELECT DECODE (c901_hierarchy, 20703, 'AAI', 20704, 'AIS', get_code_name_alt (c901_hierarchy))
			  INTO v_hierarchy
			  FROM t207_set_master
			 WHERE c207_set_id = p_set_id;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				RETURN NULL;
		END;

		RETURN v_hierarchy;
	END get_set_hierarchy;

	PROCEDURE load_set_hierarchy
	AS
		v_system_suffix NUMBER;
		v_root_set_id  t207_set_master.c207_set_id%TYPE;
		v_level_1_type system_set_hierarchy_dim.level1_type%TYPE;
		v_level_1_setid system_set_hierarchy_dim.level_1_setid%TYPE;
		v_level_2_type system_set_hierarchy_dim.level2_type%TYPE;
		v_level_2_setid system_set_hierarchy_dim.level_2_setid%TYPE;
		v_level_3_type system_set_hierarchy_dim.level_3_type%TYPE;
		v_level_3_setid system_set_hierarchy_dim.level_3_setid%TYPE;
		v_actual_set_type system_set_hierarchy_dim.actual_set_type%TYPE;
		v_actual_set_id system_set_hierarchy_dim.actual_set_id%TYPE;
		v_node_set_type system_set_hierarchy_dim.level1_type%TYPE;
		v_node_set_id  system_set_hierarchy_dim.level_1_setid%TYPE;
		v_node_lvl	   NUMBER;
		v_node_1_set_type system_set_hierarchy_dim.level1_type%TYPE;
		v_node_1_set_id system_set_hierarchy_dim.level_1_setid%TYPE;
		v_node_2_set_type system_set_hierarchy_dim.level2_type%TYPE;
		v_node_2_set_id system_set_hierarchy_dim.level_2_setid%TYPE;
		v_parent_setid t207_set_master.c207_set_id%TYPE;
		v_child_setid  t207_set_master.c207_set_id%TYPE;
		v_depth 	   NUMBER;

		CURSOR cur_set_list_1600
		IS
			SELECT t207.c207_set_id root_set_id
			  FROM t207_set_master t207
			 WHERE t207.c901_set_grp_type = 1600 AND t207.c207_void_fl IS NULL;

		CURSOR cur_node_list
		IS
			SELECT	 tree.child_set_id node, tree.lvl lvl
				FROM (SELECT DISTINCT t207a.c207_main_set_id parent_set_id, t207a.c207_link_set_id child_set_id
									, LEVEL lvl, DECODE (connect_by_isleaf, 1, 'Y', 'N') isleaf
								 FROM t207a_set_link t207a
						   START WITH c207_main_set_id = v_root_set_id
						   CONNECT BY PRIOR t207a.c207_link_set_id = c207_main_set_id AND t207a.c901_type <> 20004) tree
			   WHERE tree.isleaf = 'N'
			ORDER BY lvl;

		CURSOR cur_parent_child_info
		IS
			SELECT	 main.parent_set_id parent_set_id, main.child_set_id child_set_id, main.lvl lvl, main.isleaf isleaf
				FROM (SELECT DISTINCT t207a.c207_main_set_id parent_set_id, t207a.c207_link_set_id child_set_id
									, LEVEL lvl, DECODE (connect_by_isleaf, 1, 'Y', 'N') isleaf
									, t207a.c901_shared_status ss
								 FROM t207a_set_link t207a
						   START WITH c207_main_set_id = v_root_set_id
						   CONNECT BY PRIOR t207a.c207_link_set_id = c207_main_set_id AND t207a.c901_type <> 20004) main
			   WHERE NVL (main.ss, 20740) <> 20741
			ORDER BY lvl;
	BEGIN
		FOR set_val_1600 IN cur_set_list_1600
		LOOP
			v_root_set_id := set_val_1600.root_set_id;
			set_hierarchy_dim_master (v_root_set_id
									, v_level_1_type
									, v_level_1_setid
									, v_level_2_type
									, v_level_2_setid
									, v_level_3_type
									, v_level_3_setid
									, 'SYSTEM'
									, v_root_set_id
									 );

			FOR node_list IN cur_node_list
			LOOP
				v_node_set_id := node_list.node;
				v_node_lvl	:= node_list.lvl;

				IF v_node_lvl = 1
				THEN
					v_node_1_set_id := v_node_set_id;
					v_node_1_set_type := get_set_hierarchy (v_node_set_id);
				ELSIF v_node_lvl = 2
				THEN
					v_node_2_set_id := v_node_set_id;
					v_node_2_set_type := get_set_hierarchy (v_node_set_id);
				END IF;
			END LOOP;

			FOR set_parent_child_info IN cur_parent_child_info
			LOOP
				--v_parent_setid := set_parent_child_info.p_parent_set_id;
				v_child_setid := set_parent_child_info.child_set_id;
				v_actual_set_id := v_child_setid;
				v_actual_set_type := get_set_hierarchy (v_child_setid);
				v_depth 	:= set_parent_child_info.lvl;

				IF v_depth = 1
				THEN
					v_level_1_type := get_set_hierarchy (v_child_setid);
					v_level_1_setid := v_child_setid;
				ELSIF v_depth = 2
				THEN
					v_level_1_type := v_node_1_set_type;
					v_level_1_setid := v_node_1_set_id;
					v_level_2_type := get_set_hierarchy (v_child_setid);
					v_level_2_setid := v_child_setid;
				ELSIF v_depth = 3
				THEN
					v_level_1_type := v_node_1_set_type;
					v_level_1_setid := v_node_1_set_id;
					v_level_2_type := v_node_2_set_type;
					v_level_2_setid := v_node_2_set_id;
					v_level_3_type := get_set_hierarchy (v_child_setid);
					v_level_3_setid := v_child_setid;
				END IF;

				set_hierarchy_dim_master (v_root_set_id
										, v_level_1_type
										, v_level_1_setid
										, v_level_2_type
										, v_level_2_setid
										, v_level_3_type
										, v_level_3_setid
										, v_actual_set_type
										, v_actual_set_id
										 );
			END LOOP;

			v_level_1_type := NULL;
			v_level_1_setid := NULL;
			v_level_2_type := NULL;
			v_level_2_setid := NULL;
			v_level_3_type := NULL;
			v_level_3_setid := NULL;
			v_actual_set_type := NULL;
			v_actual_set_id := NULL;
		END LOOP;
	END load_set_hierarchy;

	PROCEDURE set_hierarchy_dim_master (
		p_system_id 		system_set_hierarchy_dim.system_id%TYPE
	  , p_level_1_type		system_set_hierarchy_dim.level1_type%TYPE
	  , p_level_1_setid 	system_set_hierarchy_dim.level_1_setid%TYPE
	  , p_level_2_type		system_set_hierarchy_dim.level2_type%TYPE
	  , p_level_2_setid 	system_set_hierarchy_dim.level_2_setid%TYPE
	  , p_level_3_type		system_set_hierarchy_dim.level_3_type%TYPE
	  , p_level_3_setid 	system_set_hierarchy_dim.level_3_setid%TYPE
	  , p_actual_set_type	system_set_hierarchy_dim.actual_set_type%TYPE
	  , p_actual_set_id 	system_set_hierarchy_dim.actual_set_id%TYPE
	)
	AS
	BEGIN
		INSERT INTO system_set_hierarchy_dim
			 VALUES (p_system_id, p_level_1_type, NVL (p_level_1_setid, '-1'), p_level_2_type
				   , NVL (p_level_2_setid, '-1'), p_level_3_type, NVL (p_level_3_setid, '-1'), p_actual_set_type
				   , p_actual_set_id);
	END set_hierarchy_dim_master;
END gm_pkg_system_set_hierarchy;
/
