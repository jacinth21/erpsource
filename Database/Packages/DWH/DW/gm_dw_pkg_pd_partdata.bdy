/* Formatted on 2010/05/03 10:35 (Formatter Plus v4.8.0) */
-- @"C:\database\BI\packages\gm_dw_pkg_pd_partdata.bdy";

CREATE OR REPLACE PACKAGE BODY gm_dw_pkg_pd_partdata
IS
	/*
	* Function to get the Order Planning Group Name for a part
	*/
	FUNCTION get_part_group_name (
		p_part_num	 t4011_group_detail.c205_part_number_id%TYPE
	)
		RETURN VARCHAR2
	IS
		v_group_name   t4010_group.c4010_group_nm%TYPE;
	BEGIN
		BEGIN
			SELECT t4010.c4010_group_nm
			  INTO v_group_name
			  FROM t4011_group_detail t4011, t4010_group t4010
			 WHERE t4010.c4010_group_id = t4011.c4010_group_id
			   AND t4010.c901_type = 40045
			   AND t4010.c4010_void_fl IS NULL
			   AND t4011.c901_part_pricing_type = 52080
			   AND t4011.c205_part_number_id = p_part_num
			   AND ROWNUM = 1;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				RETURN NULL;
		END;

		RETURN v_group_name;
	END get_part_group_name;

	/* Function to get the Order Planning Group Type for a part
	*/
	FUNCTION get_part_group_type (
		p_part_num	 t4011_group_detail.c205_part_number_id%TYPE
	)
		RETURN VARCHAR2
	IS
		v_group_type   t901_code_lookup.c901_code_nm%TYPE;
	BEGIN
		BEGIN
			SELECT get_code_name (t4010.c901_group_type)
			  INTO v_group_type
			  FROM t4011_group_detail t4011, t4010_group t4010
			 WHERE t4010.c4010_group_id = t4011.c4010_group_id
			   AND t4010.c901_type = 40045
			   AND t4010.c4010_void_fl IS NULL
			   AND t4011.c901_part_pricing_type = 52080
			   AND t4011.c205_part_number_id = p_part_num
			   AND ROWNUM = 1;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				RETURN NULL;
		END;

		RETURN v_group_type;
	END get_part_group_type;

	/* Function to get the Order Planning Group Type for a part
	*/
	FUNCTION get_product_part_group (
		p_part_num	 t4011_group_detail.c205_part_number_id%TYPE
	)
		RETURN VARCHAR2
	IS
		v_product_part_group t901_code_lookup.c901_code_nm%TYPE;
	BEGIN
		BEGIN
			SELECT get_code_name (t202.c901_group_id)
			  INTO v_product_part_group
			  FROM t205_part_number t205, t202_project t202
			 WHERE t205.c202_project_id = t202.c202_project_id
			   AND t202.c202_void_fl IS NULL
			   AND t205.c205_part_number_id = p_part_num;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				RETURN NULL;
		END;

		RETURN v_product_part_group;
	END get_product_part_group;

	/* Function to get the Order Planning Group Type for a part
	*/
	FUNCTION get_product_part_segment (
		p_part_num	 t4011_group_detail.c205_part_number_id%TYPE
	)
		RETURN VARCHAR2
	IS
		v_product_part_segment t901_code_lookup.c901_code_nm%TYPE;
	BEGIN
		BEGIN
			SELECT get_code_name (t202.c901_segment_id)
			  INTO v_product_part_segment
			  FROM t205_part_number t205, t202_project t202
			 WHERE t205.c202_project_id = t202.c202_project_id
			   AND t202.c202_void_fl IS NULL
			   AND t205.c205_part_number_id = p_part_num;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				RETURN NULL;
		END;

		RETURN v_product_part_segment;
	END get_product_part_segment;

	FUNCTION get_main_set_id_for_part (
		p_partnum	  t205_part_number.c205_part_number_id%TYPE
	  , p_system_id   t207_set_master.c207_set_id%TYPE
	)
		RETURN VARCHAR2
	IS
/*	Description 	: THIS FUNCTION RETURNS Master SETID GIVEN THE PedARTID
 Parameters 		: p_partnum
 Used in Business Intelligence
*/
		v_set_id	   t207_set_master.c207_set_id%TYPE;
	BEGIN
		BEGIN
			SELECT set_id
			  INTO v_set_id
			  FROM (SELECT	 t207.c207_set_id set_id
						FROM t208_set_details t208, t207_set_master t207
					   WHERE c205_part_number_id = p_partnum
						 AND t207.c901_set_grp_type = '1601'   -- hardcoding Set Report Group
						 AND t207.c207_set_id IN (SELECT v207a.c207_actual_set_id
													FROM v207a_set_consign_link v207a
												   WHERE v207a.c207_set_id = p_system_id)
--	   and t208.C208_SET_QTY > 0
						 AND t208.c207_set_id = t207.c207_set_id
						 AND t207.c207_void_fl IS NULL
						 AND t208.c208_void_fl IS NULL
						 AND t207.c207_type NOT IN ('4073')
					ORDER BY c901_hierarchy)
			 WHERE ROWNUM = 1;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				RETURN ' ';
		END;

		RETURN v_set_id;
	END get_main_set_id_for_part;
	
	/****************************************************************************************************
	 *  Description     : THIS FUNCTION RETURNS Agency Config Name give the Agency_Id
 	 *	Parameters 		: p_issue_agency_config_id
 	 *  Author			: Velu
	*****************************************************************************************************/
	FUNCTION GET_ISSUE_AGENCY_CONFIG_NM(
		p_issue_agency_config_id T1610_ISSUE_AGENCY_CONFIG.C1610_ISSUE_AGENCY_CONFIG_ID%TYPE
	)
	RETURN VARCHAR2
	IS
	v_issue_agency_config_nm VARCHAR2(100);
	BEGIN
    	 BEGIN
    	   	SELECT  C1610_ISSUE_AGENCY_CONFIG_NM
		   	INTO v_issue_agency_config_nm
		   	FROM  T1610_ISSUE_AGENCY_CONFIG
       		WHERE  C1610_ISSUE_AGENCY_CONFIG_ID = p_issue_agency_config_id;
	   	EXCEPTION
    	  WHEN NO_DATA_FOUND
   		THEN
      		RETURN ' ';
      	END;
     RETURN v_issue_agency_config_nm;
	END GET_ISSUE_AGENCY_CONFIG_NM;
 
	/****************************************************************************************************
	 *  Description     : THIS FUNCTION RETURNS Contract Process Client Name given the Account CPC Map ID
 	 *	Parameters 		: p_account_cpc_map_id
 	 *  Author			: Velu
	*****************************************************************************************************/
	FUNCTION GET_CPC_NAME(
		p_account_cpc_map_id GLOBUS_APP.T704B_ACCOUNT_CPC_MAPPING.C704B_ACCOUNT_CPC_MAP_ID%TYPE
	)
	RETURN VARCHAR2
	IS
	 v_cpc_name VARCHAR2(100);
	BEGIN
    	 BEGIN
       		SELECT  C704B_CPC_NAME
	   		INTO v_cpc_name
	   		FROM  GLOBUS_APP.T704B_ACCOUNT_CPC_MAPPING
       		WHERE  C704B_ACCOUNT_CPC_MAP_ID = p_account_cpc_map_id;
   		EXCEPTION
      		WHEN NO_DATA_FOUND
   		THEN
      		RETURN ' ';
      	END;
     RETURN v_cpc_name;
    END GET_CPC_NAME;

END gm_dw_pkg_pd_partdata;
/
