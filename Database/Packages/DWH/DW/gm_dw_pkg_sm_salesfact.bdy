/* Formatted on 2010/08/16 10:55 (Formatter Plus v4.8.0) */
-- @"C:\database\DW_BI\packages\gm_dw_pkg_sm_salesfact.bdy";

CREATE OR REPLACE PACKAGE BODY gm_dw_pkg_sm_salesfact
IS
   /*
   * Function to get the DO Id for an Order
   */
   FUNCTION get_do_id (p_order_id t501_order.c501_order_id%TYPE)
      RETURN VARCHAR2
   IS
      v_do_id   t501_order.c501_order_id%TYPE;
   BEGIN
      BEGIN
         SELECT orderlist.order_id
           INTO v_do_id
           FROM (SELECT t501.c501_order_id order_id, connect_by_isleaf leaf
                   FROM t501_order t501
                     START WITH t501.c501_order_id = p_order_id CONNECT BY NOCYCLE PRIOR t501.c501_parent_order_id = t501.c501_order_id) orderlist
          WHERE orderlist.leaf = 1;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            RETURN NULL;
      END;

      RETURN v_do_id;
   END get_do_id;

   FUNCTION get_distributor_id (
      p_rep_id   t703_sales_rep.c703_sales_rep_id%TYPE
   )
      RETURN VARCHAR2
   IS
      v_distributor_id   t703_sales_rep.c701_distributor_id%TYPE;
   BEGIN
      BEGIN
         SELECT c701_distributor_id
           INTO v_distributor_id
           FROM t703_sales_rep t703
          WHERE c703_sales_rep_id = p_rep_id;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            RETURN NULL;
      END;

      RETURN v_distributor_id;
   END get_distributor_id;

   FUNCTION get_territory_id (p_rep_id t703_sales_rep.c703_sales_rep_id%TYPE)
      RETURN VARCHAR2
   IS
      v_territory_id   t703_sales_rep.c702_territory_id%TYPE;
   BEGIN
      BEGIN
         SELECT c702_territory_id
           INTO v_territory_id
           FROM t703_sales_rep t703
          WHERE c703_sales_rep_id = p_rep_id;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            RETURN NULL;
      END;

      RETURN v_territory_id;
   END get_territory_id;

   FUNCTION get_first_order_for_acc (
      p_account_id   t501_order.c704_account_id%TYPE
   )
      RETURN VARCHAR2
   IS
      v_order_id   t501_order.c501_order_id%TYPE;
   BEGIN
      BEGIN
         SELECT t501.c501_order_id
           INTO v_order_id
           FROM t501_order t501
          WHERE t501.c501_void_fl IS NULL
            AND t501.c501_delete_fl IS NULL
            AND NVL (t501.c901_order_type, -9999) <> '2533'
            AND t501.c704_account_id = p_account_id
            AND t501.c501_order_date_time IN (
                   SELECT MIN (c501_order_date_time)
                     FROM t501_order
                    WHERE c501_void_fl IS NULL
                      AND c501_delete_fl IS NULL
                      AND NVL (c901_order_type, -9999) <> '2533'
                      AND c704_account_id = p_account_id)
            AND ROWNUM = 1;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            RETURN NULL;
      END;

      RETURN v_order_id;
   END get_first_order_for_acc;
END gm_dw_pkg_sm_salesfact;
/
