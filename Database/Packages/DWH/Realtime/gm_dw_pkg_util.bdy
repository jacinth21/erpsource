/* Formatted on 2010/08/31 17:37 (Formatter Plus v4.8.0) */
-- @"C:\database\DW_BI\packages\gm_dw_pkg_util.bdy";

CREATE OR REPLACE PACKAGE BODY globus_bi_realtime.gm_dw_pkg_util
IS
   FUNCTION add_period (date_in DATE, period_type VARCHAR2, how_many NUMBER)
      RETURN DATE
   IS
      date_out   DATE;
   BEGIN
      IF period_type = 'WEEK'
      THEN
         date_out := date_in + (how_many * 7);
      ELSIF period_type = 'MONTH'
      THEN
         date_out := ADD_MONTHS (date_in, how_many);
      ELSE
         date_out := date_in;
      END IF;

      RETURN date_out;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN date_in;
   END add_period;

   FUNCTION get_decode_answer_list_name (
      p_answer_list_nm   t605_answer_list.c605_answer_list_nm%TYPE
   )
      RETURN VARCHAR2
   IS
/*****************************************************************************
 * Description     : This function is used to clean the name of degree code &#176
 * Parameters   :c605_answer_list_nm is passed as in parameter
 ****************************************************************************/
--
      v_answer_list_nm   t605_answer_list.c605_answer_list_nm%TYPE;
--
   BEGIN
      --
      SELECT regexp_replace (p_answer_list_nm, '*&#176*', ' degree')
        INTO v_answer_list_nm
        FROM DUAL;

      --
      RETURN v_answer_list_nm;
   EXCEPTION
      WHEN OTHERS
      THEN
         --
         RETURN '';
   --
   END get_decode_answer_list_name;
END gm_dw_pkg_util;
/
