/* Formatted on 2011/06/01 14:10 (Formatter Plus v4.8.0) */

-- @"C:\Database\Packages\Sales\GM_PKG_SM_LOAD_CONSIGNMENT.BDY" ;
-- exec gm_pkg_sm_load_consignment.gm_sm_lad_consignment;

--This package similar to gm_pkg_sm_lad_consignment, This package will calculate set based on tagable part, if the set does not have tagable part then it will do manual setbuild
CREATE OR REPLACE PACKAGE BODY globus_etl.gm_pkg_sm_load_consignment
IS
--
   PROCEDURE gm_sm_lad_consignment
   AS
      --
      v_msg        VARCHAR2 (1000);
      v_start_dt   DATE;
   BEGIN
      v_start_dt := SYSDATE;

      --procedure to reload the materialized view
      -- gm_sm_load_preprocess; not required as this refresh happening in other load
      --procedure to load all the value from set to  Temp table
      gm_sm_lad_temp_consign_list;
      COMMIT;
      --
      -- Below code to back the previous day load
      gm_sm_lad_virtual_backup;
      COMMIT;
      --
      -- below code to delete all the existing load
      -- clean the previously loaded value
      gm_sm_lad_consign_clear;
      COMMIT;
      --
      -- below procedure called to load value from
      -- temp_consignment_list to ld_consignment_list
      gm_sm_lad_consign_list;
      COMMIT;
      --
      -- below procedure called to load virtual sets
      -- used to load set values
      gm_sm_lad_set_logic_main;
      COMMIT;
      --
      -- below procedure called for splitting remaining qty for set
      gm_sm_lad_set_split_logic_main;
      COMMIT;
      --below procedure called to load virtual item
      -- used to load item values
      gm_sm_lad_item_logic_main;
      COMMIT;
      --
      --below procedure called to load virtual item
      -- used to load item values
      gm_sm_lad_unass_logic_main;
      COMMIT;
      -- below proc to update the cost and the cogs value
      gm_sm_lad_update_cogs_and_cost;
      COMMIT;
      --below procedure called to load from temp table
      -- to actual table
      gm_sm_lad_temp_to_actual;
      COMMIT;
      --
      -- LOG THE STATUS
      gm_sm_lad_log_info (v_start_dt, SYSDATE, 'S', 'SUCCESS');
      COMMIT;
      --
      -- validation
      gm_sm_lad_validation;
      COMMIT;

      -- Procedure to call the Additional Inventory (AI) Quota load
      gm_sm_lad_ai_quota;
      COMMIT;

   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.put_line ('Inside gm_sm_lad_consignment posting ****');
         ROLLBACK;
         -- Final log insert statement (Error Message)
         gm_sm_lad_log_info (v_start_dt, SYSDATE, 'E', SQLERRM);
         COMMIT;
   END gm_sm_lad_consignment;

   /***********************************************************************
    * Procedure to refresh the v207a_set_consign_link materialized view
    *-***********************************************************************
   PROCEDURE gm_sm_load_preprocess
   AS
   BEGIN
   	-- Since this view is used in all the queries, we'd be using the same view to identify set mappings in this load
      dbms_mview.REFRESH ('v207a_set_consign_link');
   END gm_sm_load_preprocess; */

   /**********************************************************************************
   * Purpose: This procedure to load all the value from set to  Temp table (all value)
   **********************************************************************************/
   PROCEDURE gm_sm_lad_temp_consign_list
   AS
      v_company_id    t1900_company.c1900_company_id%TYPE;
      --code to fetch consignment and associated return (does not include excess retur)
      CURSOR set_consign_cur
      IS
         SELECT   t5052.c5052_ref_id d_id,
							t5053.c205_part_number_id p_num,
                            SUM(t5053.c5053_curr_qty) f_value,
                            Max(NVL (get_ac_cogs_value_by_company (t5053.c205_part_number_id, 4904,t5052.c1900_company_id), get_ac_cogs_value_by_company (t5053.c205_part_number_id, 4900,t5052.c1900_company_id))) as cogs_amt
						FROM t5053_location_part_mapping t5053,
							t5051_inv_warehouse t5051,
							t5052_location_master t5052
                        WHERE t5051.c5051_inv_warehouse_id = t5052.c5051_inv_warehouse_id
                        AND t5053.c5052_location_id        = t5052.c5052_location_id
						AND t5051.c5051_inv_warehouse_id   = 3
						AND t5052.c5052_void_fl IS NULL
						AND t5053.c5053_curr_qty<>0
						--AND  t5052.c5052_ref_id='689' 
						group by t5052.c5052_ref_id,
							t5053.c205_part_number_id
						order by t5052.c5052_ref_id,t5053.c205_part_number_id;
						-- sales consignment or FG Inventory cost

      v_d_id      t504_consignment.c701_distributor_id%TYPE;
      v_p_num     t505_item_consignment.c205_part_number_id%TYPE;
      v_f_value   t505_item_consignment.c505_item_qty%TYPE;
      v_icount    NUMBER;
   --
   BEGIN
/*
	  	SELECT NVL(get_compid_frm_cntx(),1000)
		  INTO v_company_id
		  FROM DUAL;
*/		  
      --
      --
      execute immediate 'TRUNCATE TABLE t7200a_ld_temp_consign_list';
	  --
      COMMIT;

      -- load consignment information
      FOR set_val IN set_consign_cur
      LOOP
         --
         v_p_num := set_val.p_num;
         v_icount := v_icount + 1;

         --
         INSERT INTO t7200a_ld_temp_consign_list
                     (C7200A_LD_TEMP_CONS_ID, c701_distributor_id,
                      c205_part_number_id, C7200A_QTY, C7200A_ITEM_PRICE,
                      C7200A_ITEM_COGS_PRICE, C7200A_TYPE
                     )
              VALUES (s7200a_ld_temp_consign_list.NEXTVAL, set_val.d_id,
                      v_p_num, set_val.f_value, 0,--set_val.list_amt
                      set_val.cogs_amt, 'C'
                     );
      --, get_part_price (v_p_num, 'E'), get_salescsg_inv_cogs_value (v_p_num)

         IF (v_icount = 100)
         THEN
            COMMIT;
            v_icount := 0;
         END IF;
      --
      END LOOP;
   END gm_sm_lad_temp_consign_list;

   /*******************************************************
   * Purpose: Procdure called to delete all the existing value
   *
   *******************************************************/
   PROCEDURE gm_sm_lad_consign_clear
   AS
   BEGIN
      --
      execute immediate 'TRUNCATE TABLE t7301t_virtual_consign_item';

      --
      execute immediate 'TRUNCATE TABLE t7200_ld_consignment_list';

      --
      execute immediate 'TRUNCATE TABLE t7300t_virtual_consignment';
   --
   END gm_sm_lad_consign_clear;

   --
   /*******************************************************
   * Purpose: Procdure called to move parts from
   *  t7200a_ld_temp_consign_list to t7200_ld_consignment_list
   *******************************************************/
   PROCEDURE gm_sm_lad_consign_list
   AS
      --
      CURSOR set_cursor
      IS
         SELECT c701_distributor_id, c205_part_number_id, C7200A_QTY,
                C7200A_ITEM_PRICE, C7200A_ITEM_COGS_PRICE, C7200A_TYPE
           FROM t7200a_ld_temp_consign_list;

      --
      v_qty   t7200a_ld_temp_consign_list.C7200A_QTY%TYPE;
   BEGIN
      --
      UPDATE t7200a_ld_temp_consign_list
         SET C7200A_UPDATE_INV_FL = 1;

      FOR set_val IN set_cursor
      LOOP
         --
         v_qty := set_val.C7200A_QTY;

         INSERT INTO t7200_ld_consignment_list
                     (C7200_LD_CONSIGNMENT_ID,
                      c701_distributor_id,
                      c205_part_number_id, C7200_QTY_RECEIVED, C7200_QTY_USED,
                      C7200_QTY_AVAILABLE, C7200_ITEM_QTY, C7200_CHANGE_TYPE,
                      C7200_ITEM_PRICE,
                      C7200_ITEM_COGS_PRICE, C7200_TYPE
                     )
              VALUES (s7200_ld_consignment_list.NEXTVAL,
                      set_val.c701_distributor_id,
                      set_val.c205_part_number_id, v_qty, 0,
                      v_qty, 0, 'C',
                      set_val.C7200A_ITEM_PRICE,
                      set_val.C7200A_ITEM_COGS_PRICE, set_val.C7200A_TYPE
                     );
         --
         --END IF;
      --
      END LOOP;
   --
   END gm_sm_lad_consign_list;

--*******************************************
--
--******************************************
   PROCEDURE gm_sm_lad_set_logic_main
   AS
   	  v_d_id                 t504_consignment.c701_distributor_id%TYPE;
      v_set_id               t207_set_master.c207_set_id%TYPE;
      v_eflag                t208_set_details.c208_critical_qty%TYPE;
      v_setmin               t7201_ld_logic_temp.C7201_SET_POSSIBLE%TYPE;
      v_set_min_logic_code   t207_set_master.c901_minset_logic%TYPE;
      --
      CURSOR distributor_cursor
      IS
         SELECT c701_distributor_id
           FROM t701_distributor;

      --WHERE c701_distributor_id = 689;  -- To be removed if moved to production
      CURSOR set_cursor
      IS
         SELECT t207.c207_set_id, NVL (t207.c207_seq_no, 9999) seq_no, t207.c207_set_nm, NVL(taggable_dtls.tag_cnt, 0) tag_cnt
		  , (
				 SELECT COUNT (1)
				   FROM t208_set_details t208
				  WHERE t208.c207_set_id      = t207.c207_set_id
					AND c208_critical_tag    IS NOT NULL
					AND t208.c208_void_fl    IS NULL
					AND t208.c208_critical_fl = 'Y'
			)
			eflag, NVL (c901_minset_logic, 20731) minsetlogic
			FROM t207_set_master t207, (SELECT c207_set_id set_id,COUNT (1) tag_cnt
            					FROM t5010_tag t5010
            					WHERE t5010.c5010_void_fl       IS NULL
            					AND c207_set_id               IS NOT NULL
            					AND t5010.C5010_LOCATION_ID        = v_d_id
						AND t5010.c901_inventory_type=10003  -- loose item consignment
            					GROUP BY c207_set_id
				)taggable_dtls
			  WHERE t207.c207_set_id IN
				(
					 SELECT c207_set_id
					   FROM t208_set_details
					  WHERE c208_void_fl    IS NULL
				)
				AND t207.c207_set_id       = taggable_dtls.set_id (+)
				AND t207.c901_set_grp_type = 1601
				--AND t207.c207_type        IN (4070, 4071, 4078)
				--AND t207.c207_set_id IN('924.905','924.902','908.903','936.902','964.903','835.901','835.901EUR','835.902','835.902EUR')
			ORDER BY tag_cnt DESC; -- this ordering is important

      --
      
      
   --
   BEGIN
      
      FOR distributor_val IN distributor_cursor
      LOOP
         --
         v_d_id := distributor_val.c701_distributor_id;

         FOR set_val IN set_cursor
         LOOP
            --
            v_set_id := set_val.c207_set_id;
            v_eflag := set_val.eflag;
            v_set_min_logic_code := set_val.minsetlogic;
              
            IF (set_val.tag_cnt<> 0)
            THEN

                -- if set has tagable part then call below procedure
                gm_sm_lad_vconsignment_tag (v_d_id, v_set_id);

           
            END IF;  --for tag_count check
                    
	            
            COMMIT;
         END LOOP;

         --
         COMMIT;
      --
      END LOOP;
   END gm_sm_lad_set_logic_main;

--*******************************************
-- This procedure loades the temp table with
--******************************************
   PROCEDURE gm_sm_lad_temp_set_logic (
      p_d_id     t504_consignment.c701_distributor_id%TYPE,
      p_set_id   t207_set_master.c207_set_id%TYPE,
      p_eflag    t208_set_details.c208_critical_qty%TYPE
   )
   AS
      --
      -- To work on either or option
      CURSOR seteither_cursor
      IS
         SELECT   c205_part_number_id,
                  NVL (c208_critical_qty, t208.c208_set_qty) sqty,
                  c208_critical_tag ctag
             FROM t208_set_details t208
            WHERE t208.c207_set_id = p_set_id
              AND t208.c208_critical_fl = 'Y'
              AND c208_critical_tag IS NOT NULL
              AND t208.c208_void_fl IS NULL
         ORDER BY ctag;

      --
      v_either            NUMBER;
      v_part_number       t205_part_number.c205_part_number_id%TYPE;
      v_set_qty           t208_set_details.c208_set_qty%TYPE;
      v_available_qty     t208_set_details.c208_set_qty%TYPE;
      v_ctag              t208_set_details.c208_critical_tag%TYPE;
      v_ptag              t208_set_details.c208_critical_tag%TYPE;
      v_insert_fl         CHAR (1)                                      := 'N';
      --
      --Previous value
      v_pre_part_number   t205_part_number.c205_part_number_id%TYPE;
      v_pre_set_qty       t208_set_details.c208_set_qty%TYPE;
      v_item_price        t7201_ld_logic_temp.c7201_item_price%TYPE;
      v_cogs_price        t7201_ld_logic_temp.c7201_item_cogs_price%TYPE;
   --
   BEGIN
      -- Code to delete previous load value from   t7201_ld_logic_temp table
      DELETE FROM t7201_ld_logic_temp;

      --
         -- Below code to dump the value to vset table
         -- loads all the available qty to frame a set does not include Tag (either or set)
         -- handled in seperate loop
      INSERT INTO t7201_ld_logic_temp
                  (c7201_ld_temp_id, c205_part_number_id, c207_set_id,
                   c7201_set_qty, c7201_qty_available, c7201_set_possible,
                   c701_distributor_id, c7201_item_price,
                   c7201_item_cogs_price)
         SELECT s7201_ld_logic_temp.NEXTVAL, t208.c205_part_number_id,
                t208.c207_set_id, NVL (c208_critical_qty, t208.c208_set_qty),
                NVL (t720.c7200_qty_available, 0),
                FLOOR (  NVL (t720.c7200_qty_available, 0)
                       / NVL (c208_critical_qty, t208.c208_set_qty)
                      ),
                p_d_id, c7200_item_price, c7200_item_cogs_price
           FROM t208_set_details t208,
                (SELECT c205_part_number_id, c7200_qty_available,
                        c701_distributor_id, c7200_item_price,
                        c7200_item_cogs_price
                   FROM t7200_ld_consignment_list
                  WHERE c701_distributor_id = p_d_id
                    AND c7200_qty_available > 0
                    AND c7200_type = 'C') t720
          WHERE t208.c207_set_id = p_set_id
            AND t208.c205_part_number_id = t720.c205_part_number_id(+)
            AND t208.c208_critical_fl = 'Y'
			AND t208.c208_void_fl IS NULL
            AND c208_critical_tag IS NULL;

      /****************************************************************
       * critical component (either or option)
       * *************************************
       *
       * Below code to find if the critical component is mapped as
       * either or option  (based on that load process will change)
       ****************************************************************/
      --
      --DBMS_OUTPUT.put_line ('p_eflag ****' || p_eflag);
      IF (p_eflag <> 0)
      THEN
         v_ptag := '!@';                    -- Default value First time check

         --
         FOR seteither_val IN seteither_cursor
         LOOP
            --
            v_part_number := seteither_val.c205_part_number_id;
            v_set_qty := seteither_val.sqty;
            v_ctag := seteither_val.ctag;

            -- if both the tag not loaded then should have a default value
            IF v_ptag != v_ctag
            THEN
               -- In the loop if the tag value does not insert value
               -- then system should insert 0 value so the set is not created
               IF (v_insert_fl = 'N' AND v_ptag != '!@')
               THEN
                  INSERT INTO t7201_ld_logic_temp
                              (c7201_ld_temp_id,
                               c205_part_number_id, c207_set_id,
                               c7201_set_qty, c7201_qty_available,
                               c7201_set_possible, c701_distributor_id,
                               c7201_item_price, c7201_item_cogs_price
                              )
                       VALUES (s7201_ld_logic_temp.NEXTVAL,
                               v_pre_part_number, p_set_id,
                               v_pre_set_qty, 0,
                               0, p_d_id,
                               0, 0
                              );
               END IF;

               v_insert_fl := 'N';
            END IF;

            IF (v_insert_fl != 'Y')
            THEN
               --
               -- Find the available qty
               -- if not skip to next record
               BEGIN
                  --
                  SELECT c7200_qty_available, c7200_item_price,
                         c7200_item_cogs_price
                    INTO v_available_qty, v_item_price,
                         v_cogs_price
                    FROM t7200_ld_consignment_list
                   WHERE c701_distributor_id = p_d_id
                     AND c205_part_number_id = v_part_number
                     AND c7200_type = 'C';
                   --
               --
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     v_available_qty := 0;
               END;

               --
               -- To insert record if the avaialble qty is more
               -- to process a set
               IF (v_available_qty >= v_set_qty)
               THEN
                  --
                  --DBMS_OUTPUT.put_line ('Inserting part****' || v_part_number);
                  INSERT INTO t7201_ld_logic_temp
                              (c7201_ld_temp_id, c205_part_number_id,
                               c207_set_id, c7201_set_qty,
                               c7201_qty_available,
                               c7201_set_possible, c701_distributor_id,
                               c7201_item_price, c7201_item_cogs_price
                              )
                       VALUES (s7201_ld_logic_temp.NEXTVAL, v_part_number,
                               p_set_id, v_set_qty,
                               v_available_qty,
                               FLOOR (v_available_qty / v_set_qty), p_d_id,
                               v_item_price, v_cogs_price
                              );

                  v_insert_fl := 'Y';
               --
               END IF;
            --
            END IF;

            --
            v_ptag := v_ctag;
            v_pre_part_number := v_part_number;
            v_pre_set_qty := v_set_qty;
         --
         END LOOP;
      END IF;
   --
   END gm_sm_lad_temp_set_logic;
   --
--*******************************************
--
--******************************************
   PROCEDURE gm_sm_lad_vset_logic (
      p_d_id           t504_consignment.c701_distributor_id%TYPE,
      p_set_id         t207_set_master.c207_set_id%TYPE,
      p_setmin   OUT   t7201_ld_logic_temp.c7201_set_possible%TYPE
   )
   AS
      --
      v_set_min_logic_code   t207_set_master.c901_minset_logic%TYPE;
   BEGIN
      --
      SELECT NVL (c901_minset_logic, 20731)
        INTO v_set_min_logic_code
        FROM t207_set_master
       WHERE c207_set_id = p_set_id;

      IF (v_set_min_logic_code = 20731)
      THEN
         SELECT NVL (MIN (c7201_set_possible), 0)
           INTO p_setmin
           FROM t7201_ld_logic_temp
          WHERE c701_distributor_id = p_d_id AND c207_set_id = p_set_id;
	  ELSIF (v_set_min_logic_code = 20730)
      THEN
         SELECT (consign.c_qty - NVL (retur.r_qty, 0)) f_value
           INTO p_setmin
           FROM (SELECT COUNT (1) c_qty
                   FROM t504_consignment t504
                  WHERE t504.c701_distributor_id IS NOT NULL
                    AND t504.c504_status_fl = 4
                    AND t504.c504_type IN (4110)
                    AND c504_void_fl IS NULL
                    AND c701_distributor_id = p_d_id
                    AND c207_set_id = p_set_id) consign,
                (SELECT COUNT (1) r_qty
                   FROM t506_returns t506
                  WHERE t506.c701_distributor_id IS NOT NULL
                    AND t506.c506_status_fl = 2
                    AND t506.c506_void_fl IS NULL
                    AND c701_distributor_id = p_d_id
                    AND c207_set_id = p_set_id) retur;
      END IF;

      IF (p_setmin <= 0)
      THEN
         RETURN;
      END IF;

      --code to add value to vconsignment
     gm_sm_lad_vconsignment_add (p_d_id, p_set_id, p_setmin);
      --code to update set used value in ld_consignment_list
      -- this wont be needed with the new changes
      /* gm_sm_lad_update_usedpart (p_d_id, p_set_id, p_setmin); */
   --
   END gm_sm_lad_vset_logic;
   --*******************************************
--
--******************************************
   PROCEDURE gm_sm_lad_vconsignment_add (
      p_d_id     t504_consignment.c701_distributor_id%TYPE,
      p_set_id   t207_set_master.c207_set_id%TYPE,
      p_setmin   t7201_ld_logic_temp.c7201_set_possible%TYPE
   )
   AS
      v_vir_consign_id   t7300t_virtual_consignment.c7300_virtual_consignment_id%TYPE;
      cur_pnum           t720_ld_consignment_list.c205_part_number_id%TYPE;
      cur_setqty         t208_set_details.c208_set_qty%TYPE;
      cur_price          t720_ld_consignment_list.c720_item_price%TYPE;
      cur_cogs           t720_ld_consignment_list.c720_item_cogs_price%TYPE;
      cur_availableqty   t208_set_details.c208_set_qty%TYPE;
      v_qtyforset        t208_set_details.c208_set_qty%TYPE;

      CURSOR cur_consign_set_details_qty
      IS
         SELECT t720.c205_part_number_id pnum, t208.c208_set_qty setqty,
                t720.c7200_item_price price, t720.c7200_item_cogs_price cogs,
                t720.c7200_qty_available availableqty
           FROM t7200_ld_consignment_list t720, t208_set_details t208
          WHERE t208.c205_part_number_id = t720.c205_part_number_id
            AND t208.c207_set_id = p_set_id                       -- '965.905'
            AND t720.c701_distributor_id = p_d_id                         -- 8
            AND t720.c7200_qty_available >
                               0
                                -- will exclude 665.502 in the fifth iteration
            AND t208.c208_inset_fl = 'Y'
            AND t208.c208_void_fl IS NULL
            AND c7200_type = 'C';
   BEGIN
      --
      FOR cntr IN 1 .. p_setmin
      LOOP
         SELECT s7300_virtual_consignment_id.NEXTVAL
           INTO v_vir_consign_id
           FROM DUAL;

         INSERT INTO t7300t_virtual_consignment
                     (c7300_virtual_consignment_id, c701_distributor_id,
                      c207_set_id, c7300_last_load_dt
                     )
              VALUES (v_vir_consign_id, p_d_id,
                      p_set_id, SYSDATE
                     );

         FOR set_val IN cur_consign_set_details_qty
         LOOP
            cur_pnum := set_val.pnum;
            cur_setqty := set_val.setqty;
            cur_price := set_val.price;
            cur_cogs := set_val.cogs;
            cur_availableqty := set_val.availableqty;

            -- If the set_qty is 3 and available qty is 1, then use 1
            -- If the set_qty is 3 and available qty is 5, then use 3
            IF (cur_availableqty - cur_setqty > 0)
            THEN
               v_qtyforset := cur_setqty;
            ELSE
               v_qtyforset := cur_availableqty;
            END IF;

            --
            -- To load the value in child table
            INSERT INTO t7301t_virtual_consign_item
                        (c7301_virtual_consign_item_id,
                         c7300_virtual_consignment_id, c205_part_number_id,
                         c7301_item_qty, c7301_item_price, c7301_item_cogs_price
                        )
                 VALUES (s7301_virtual_consign_item.NEXTVAL,
                         v_vir_consign_id, cur_pnum,
                         v_qtyforset, cur_price, cur_cogs
                        );

-- first iteration, c720_qty_used = 0 + 2; c720_qty_available = 16 - 2 for 665.500
            UPDATE t7200_ld_consignment_list
               SET c7200_qty_used = (c7200_qty_used + v_qtyforset),
                   c7200_qty_available = (c7200_qty_available - v_qtyforset)
             WHERE c701_distributor_id = p_d_id
               AND c205_part_number_id = cur_pnum
               AND c7200_type = 'C';
         END LOOP;
         --
      --
      END LOOP;
   --
   END gm_sm_lad_vconsignment_add;
--*******************************************
--
--******************************************
   PROCEDURE gm_sm_lad_vconsignment_tag (
      p_d_id     t504_consignment.c701_distributor_id%TYPE,
      p_set_id   t207_set_master.c207_set_id%TYPE
     -- p_setmin   t7201_ld_logic_temp.C7201_SET_POSSIBLE%TYPE paddy
   )
   AS
      v_vir_consign_id   t7300t_virtual_consignment.C7300_VIRTUAL_CONSIGNMENT_ID%TYPE;
      cur_pnum           t7200_ld_consignment_list.c205_part_number_id%TYPE;
      cur_setqty         t208_set_details.c208_set_qty%TYPE;
      cur_price          t7200_ld_consignment_list.C7200_ITEM_PRICE%TYPE;
      cur_cogs           t7200_ld_consignment_list.C7200_ITEM_COGS_PRICE%TYPE;
      cur_availableqty   t208_set_details.c208_set_qty%TYPE;
      v_qtyforset        t208_set_details.c208_set_qty%TYPE;

      CURSOR cur_consign_set_details_qty
      IS
         SELECT t720.c205_part_number_id pnum, t208.c208_set_qty setqty,
                t720.C7200_ITEM_PRICE price, t720.C7200_ITEM_COGS_PRICE cogs,
                t720.C7200_QTY_AVAILABLE availableqty
           FROM t7200_ld_consignment_list t720, t208_set_details t208
          WHERE t208.c205_part_number_id = t720.c205_part_number_id
            AND t208.c207_set_id = p_set_id                      -- '965.905'
            AND t720.c701_distributor_id =p_d_id                        -- 8
            AND t720.C7200_QTY_AVAILABLE >
                               0
                                -- will exclude 665.502 in the fifth iteration
            --AND t208.c208_inset_fl = 'Y'
			AND t208.c208_set_qty>0
            AND t208.c208_void_fl IS NULL
            AND C7200_TYPE = 'C';

		  CURSOR tag_count
			IS
			SELECT C5010_LOCATION_ID d_id,C207_SET_ID set_id, C5010_TAG_ID tag_id
			FROM T5010_TAG T5010
            WHERE T5010.C5010_LOCATION_ID = p_d_id
            AND  T5010.C207_SET_ID = p_set_id
            AND  T5010.C5010_VOID_FL IS NULL
			AND t5010.c901_inventory_type=10003;

   BEGIN
      --
      FOR tcntr IN tag_count
      LOOP
         SELECT s7300_virtual_consignment_id.NEXTVAL
           INTO v_vir_consign_id
           FROM DUAL;

         INSERT INTO t7300t_virtual_consignment
                     (C7300_VIRTUAL_CONSIGNMENT_ID, c701_distributor_id,
                      c207_set_id, C7300_LAST_LOAD_DT,C5010_TAG_ID
                     )
              VALUES (v_vir_consign_id, p_d_id,
                      tcntr.set_id, SYSDATE,tcntr.tag_id
                     );

         FOR set_val IN cur_consign_set_details_qty
         LOOP
            cur_pnum := set_val.pnum;
            cur_setqty := set_val.setqty;
            cur_price := set_val.price;
            cur_cogs := set_val.cogs;
            cur_availableqty := set_val.availableqty;

            -- If the set_qty is 3 and available qty is 1, then use 1
            -- If the set_qty is 3 and available qty is 5, then use 3
            IF (cur_availableqty - cur_setqty > 0)
            THEN
               v_qtyforset := cur_setqty;
            ELSE
               v_qtyforset := cur_availableqty;
            END IF;

            --
            -- To load the value in child table
            INSERT INTO t7301t_virtual_consign_item
                        (C7301_VIRTUAL_CONSIGN_ITEM_ID,
                         C7300_VIRTUAL_CONSIGNMENT_ID, c205_part_number_id,
                         C7301_ITEM_QTY, C7301_ITEM_PRICE, C7301_ITEM_COGS_PRICE
                        )
                 VALUES (s7301_virtual_consign_item.NEXTVAL,
                         v_vir_consign_id, cur_pnum,
                         v_qtyforset, cur_price, cur_cogs
                        );

-- first iteration, C7200_QTY_USED = 0 + 2; C7200_QTY_AVAILABLE = 16 - 2 for 665.500
            UPDATE t7200_ld_consignment_list
               SET C7200_QTY_USED = (C7200_QTY_USED + v_qtyforset),
                   C7200_QTY_AVAILABLE = (C7200_QTY_AVAILABLE - v_qtyforset)
             WHERE c701_distributor_id = p_d_id
               AND c205_part_number_id = cur_pnum
               AND C7200_TYPE = 'C';
         END LOOP;
         --
      --
      END LOOP;
   --
   END gm_sm_lad_vconsignment_tag;

--
--*******************************************
--
--******************************************
--
   PROCEDURE gm_sm_lad_set_split_logic_main
   AS
      CURSOR distributor_cursor
      IS
         SELECT c701_distributor_id
           FROM t701_distributor;
     -- WHERE c701_distributor_id =689;
      CURSOR set_cursor
      IS
         SELECT   c207_set_id
             FROM t207_set_master
            WHERE c901_set_grp_type = 1603
         ORDER BY c207_set_id;

      --  AND c207_set_id IN ('AI.1932', 'AI.1933', 'AI.1934', 'AI.1935', 'AI.1936', 'AI.1937');   -- To be removed

      --
      v_d_id     t504_consignment.c701_distributor_id%TYPE;
      v_set_id   t207_set_master.c207_set_id%TYPE;
   --
   BEGIN
      --
      FOR distributor_val IN distributor_cursor
      LOOP
         --
         v_d_id := distributor_val.c701_distributor_id;

         -- DBMS_OUTPUT.put_line ('Main Loop Started  ****' || v_d_id);
         FOR set_val IN set_cursor
         LOOP
            --
            v_set_id := set_val.c207_set_id;
            gm_sm_lad_temp_set_split (v_d_id, v_set_id);
         --
         END LOOP;
      --
      END LOOP;
   --
   END gm_sm_lad_set_split_logic_main;

--
--*******************************************
--
--******************************************
   PROCEDURE gm_sm_lad_temp_set_split (
      p_d_id     t504_consignment.c701_distributor_id%TYPE,
      p_set_id   t207_set_master.c207_set_id%TYPE
   )
   AS
      --
      v_finditem         NUMBER;
      v_vir_consign_id   t7300t_virtual_consignment.C7300_VIRTUAL_CONSIGNMENT_ID%TYPE;
      v_product_type     t207_set_master.c207_category%TYPE;
      v_main_set_id      t207_set_master.c207_set_id%TYPE;
      v_partnum          t205_part_number.c205_part_number_id%TYPE;
   BEGIN
      -- To find the main set id (to find unused parts)
      BEGIN
         -- this will fetch 965.905 when we send UNS.200
         SELECT c207_link_set_id
           INTO v_main_set_id
           FROM t207a_set_link t207a
          WHERE t207a.c207_main_set_id = p_set_id;
      --- add rownum =1
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            RETURN;
         WHEN OTHERS
         THEN
            DBMS_OUTPUT.put_line
                   (   'Inside gm_sm_lad_consignment posting main set id****'
                    || p_set_id
                   );
            RAISE NO_DATA_FOUND;
      END;

      -- Insert into the Virtual Consignment table
      SELECT s7300_virtual_consignment_id.NEXTVAL
        INTO v_vir_consign_id
        FROM DUAL;

      INSERT INTO t7300t_virtual_consignment
                  (C7300_VIRTUAL_CONSIGNMENT_ID, c701_distributor_id,
                   c207_set_id, C7300_LAST_LOAD_DT
                  )
           VALUES (v_vir_consign_id, p_d_id,
                   p_set_id, SYSDATE
                  );

      INSERT INTO t7301t_virtual_consign_item
                  (C7301_VIRTUAL_CONSIGN_ITEM_ID, C7300_VIRTUAL_CONSIGNMENT_ID,
                   c205_part_number_id, C7301_ITEM_QTY, C7301_ITEM_PRICE,
                   C7301_ITEM_COGS_PRICE)
         SELECT s7301_virtual_consign_item.NEXTVAL, v_vir_consign_id,
                t720.c205_part_number_id, t720.C7200_QTY_AVAILABLE,
                t720.C7200_ITEM_PRICE, t720.C7200_ITEM_COGS_PRICE
           FROM t7200_ld_consignment_list t720, t208_set_details t208
          WHERE t208.c205_part_number_id = t720.c205_part_number_id
            AND t208.c207_set_id =
                   v_main_set_id
-- this should be got from set_link table. we map AI.002 (now UNS.200) with 965.905
            AND t720.c701_distributor_id = p_d_id
            AND t720.C7200_QTY_AVAILABLE > 0
            --AND t208.c208_inset_fl = 'Y'
			AND t208.c208_set_qty>0
            AND t208.c208_void_fl IS NULL
            AND C7200_TYPE = 'C';

      UPDATE t7200_ld_consignment_list
         SET C7200_QTY_USED = (C7200_QTY_USED + C7200_QTY_AVAILABLE),
             C7200_QTY_AVAILABLE = (C7200_QTY_AVAILABLE - C7200_QTY_AVAILABLE)
       WHERE c701_distributor_id = p_d_id
         AND c205_part_number_id IN (
                SELECT t720.c205_part_number_id
                  FROM t7200_ld_consignment_list t720, t208_set_details t208
                 WHERE t208.c205_part_number_id = t720.c205_part_number_id
                   AND t208.c207_set_id =
                          v_main_set_id
         -- this should be got from set_link table. we map AI.002 with 965.905
                   AND t720.c701_distributor_id = p_d_id
                   --AND t208.c208_inset_fl = 'Y'
				   AND t208.c208_set_qty>0
                   AND t208.c208_void_fl IS NULL
                   AND t720.C7200_QTY_AVAILABLE > 0)
         AND C7200_TYPE = 'C';
   END gm_sm_lad_temp_set_split;

--***********************************************************************
--Procedure to load item into item consignment
--***********************************************************************
   PROCEDURE gm_sm_lad_item_logic_main
   AS
      --
        --
      v_d_id          t504_consignment.c701_distributor_id%TYPE;
      v_set_id        t207_set_master.c207_set_id%TYPE;
      v_main_set_id   t207_set_master.c207_set_id%TYPE;

--
		-- Select only the distributors who have atleast 1 of the parts in AA
      CURSOR distributor_cursor
      IS
         SELECT DISTINCT t701.c701_distributor_id
                    FROM t701_distributor t701,
                         t7200_ld_consignment_list t720,
                         (SELECT t208.c205_part_number_id pnum
                            FROM t208_set_details t208
                           WHERE t208.c207_set_id IN (
                                             SELECT c207_actual_set_id
                                               FROM v207a_set_consign_link v207a
                                              WHERE c207_set_id =
                                                                 v_main_set_id)
                             AND t208.c208_inset_fl = 'N'
                             AND t208.c208_void_fl IS NULL
                          UNION ALL
                          SELECT t208.c205_part_number_id
                            FROM t208_set_details t208
                           WHERE t208.c207_set_id = v_main_set_id
                             AND t208.c208_void_fl IS NULL) sys_part_list
                   WHERE t720.c701_distributor_id = t701.c701_distributor_id
                     AND t720.C7200_QTY_AVAILABLE <> 0
                     AND t720.c205_part_number_id = sys_part_list.pnum
                 --    AND t701.c701_distributor_id IN (689)
                     ORDER BY        t701.c701_distributor_id;

      CURSOR set_cursor
      IS
         SELECT   c207_set_id
--, NVL (c901_minset_logic, 20731) minsetlogic, NVL (t207.c207_seq_no, 0) seq_no
             FROM t207_set_master
            WHERE c901_set_grp_type = 1605
         --AND c207_set_id IN ('AA.003','AA.009')   -- To be removed
         ORDER BY c207_set_id;
   BEGIN
      --
      FOR set_val IN set_cursor
      LOOP
         --
         v_set_id := set_val.c207_set_id;

         BEGIN
            SELECT v207a.c207_set_id
              INTO v_main_set_id
              FROM v207a_set_consign_link v207a
             WHERE v207a.c207_actual_set_id = v_set_id;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               DBMS_OUTPUT.put_line
                    (   'Inside gm_sm_lad_temp_item_logic NO DATA FOUND ****'
                     || v_set_id
                    );
            --RETURN;
            WHEN OTHERS
            THEN
               DBMS_OUTPUT.put_line
                           (   'Inside gm_sm_lad_temp_item_logic Others ****'
                            || v_set_id
                           );
               RAISE NO_DATA_FOUND;
         END;

			-- Using the temp table instead of using IN clause for these parts. we'll join the temp table to filter the info
         DELETE FROM my_temp_list;

         INSERT INTO my_temp_list
                     (my_temp_txn_id)
            SELECT t208.c205_part_number_id
              FROM t208_set_details t208
             WHERE t208.c207_set_id IN (SELECT c207_actual_set_id
                                          FROM v207a_set_consign_link v207a
                                         WHERE c207_set_id = v_main_set_id)
               AND t208.c208_inset_fl = 'N'
               AND t208.c208_void_fl IS NULL
            UNION
            SELECT t208.c205_part_number_id
              FROM t208_set_details t208
             WHERE t208.c207_set_id = v_main_set_id
               AND t208.c208_void_fl IS NULL;

         FOR distributor_val IN distributor_cursor
         LOOP
            --
            v_d_id := distributor_val.c701_distributor_id;
            gm_sm_lad_temp_item_logic (v_d_id, v_set_id);
            --
            COMMIT;
         --
         END LOOP;
      --
      END LOOP;
--
   END gm_sm_lad_item_logic_main;

--
---***********************************************************************
--Procedure to load item into item details into item consignment set
--***********************************************************************
   PROCEDURE gm_sm_lad_temp_item_logic (
      p_d_id     t504_consignment.c701_distributor_id%TYPE,
      p_set_id   t207_set_master.c207_set_id%TYPE
   )
   AS
      --
      v_finditem         NUMBER;
      v_vir_consign_id   t7300t_virtual_consignment.C7300_VIRTUAL_CONSIGNMENT_ID%TYPE;
   BEGIN
      --
      SELECT s7300_virtual_consignment_id.NEXTVAL
        INTO v_vir_consign_id
        FROM DUAL;

      INSERT INTO t7300t_virtual_consignment
                  (C7300_VIRTUAL_CONSIGNMENT_ID, c701_distributor_id,
                   c207_set_id, C7300_LAST_LOAD_DT
                  )
           VALUES (v_vir_consign_id, p_d_id,
                   p_set_id, SYSDATE
                  );

      INSERT INTO t7301t_virtual_consign_item
                  (C7301_VIRTUAL_CONSIGN_ITEM_ID, C7300_VIRTUAL_CONSIGNMENT_ID,
                   c205_part_number_id, C7301_ITEM_QTY, C7301_ITEM_PRICE,
                   C7301_ITEM_COGS_PRICE)
         SELECT s7301_virtual_consign_item.NEXTVAL, v_vir_consign_id,
                t720.c205_part_number_id, t720.C7200_QTY_AVAILABLE,
                C7200_ITEM_PRICE, C7200_ITEM_COGS_PRICE
           FROM t7200_ld_consignment_list t720,
                my_temp_list                         --, t205_part_number t205
          WHERE t720.c701_distributor_id = p_d_id
            AND t720.C7200_QTY_AVAILABLE <> 0
            AND t720.c205_part_number_id = my_temp_txn_id;

      -- update the list table with used qty
      UPDATE t7200_ld_consignment_list
         SET C7200_QTY_USED = C7200_QTY_USED + C7200_QTY_AVAILABLE,
             C7200_QTY_AVAILABLE = 0
       WHERE c701_distributor_id = p_d_id
         AND C7200_QTY_AVAILABLE <> 0
         AND c205_part_number_id IN (SELECT my_temp_txn_id
                                       FROM my_temp_list);
   END gm_sm_lad_temp_item_logic;

--
--
--
--***********************************************************************
--Procedure to load unassigned parts to into item consignment
--***********************************************************************
   PROCEDURE gm_sm_lad_unass_logic_main
   AS
      --
      CURSOR distributor_cursor
      IS
         SELECT c701_distributor_id
           FROM t701_distributor;
          -- where c701_distributor_id = 689;

      --WHERE c701_distributor_id IN (121);
      CURSOR set_cursor
      IS
         SELECT c207_set_id
           FROM t207_set_master
          WHERE c901_set_grp_type = 1604 AND c207_set_id LIKE 'UNS.101';
                                                   -- Maps to unassigned sets

      --
      v_d_id             t504_consignment.c701_distributor_id%TYPE;
      v_set_id           t207_set_master.c207_set_id%TYPE;
      v_product_type     t205_part_number.c205_product_family%TYPE;
      v_vir_consign_id   t7300t_virtual_consignment.C7300_VIRTUAL_CONSIGNMENT_ID%TYPE;
--
   BEGIN
      --
      FOR distributor_val IN distributor_cursor
      LOOP
         --
         v_d_id := distributor_val.c701_distributor_id;

         FOR set_val IN set_cursor
         LOOP
            --
            v_set_id := set_val.c207_set_id;

            SELECT s7300_virtual_consignment_id.NEXTVAL
              INTO v_vir_consign_id
              FROM DUAL;

            --
            INSERT INTO t7300t_virtual_consignment
                        (C7300_VIRTUAL_CONSIGNMENT_ID, c701_distributor_id,
                         c207_set_id, C7300_LAST_LOAD_DT
                        )
                 VALUES (v_vir_consign_id, v_d_id,
                         v_set_id, SYSDATE
                        );

            -- Below code to dump the value to vset table
            INSERT INTO t7301t_virtual_consign_item
                        (C7301_VIRTUAL_CONSIGN_ITEM_ID,
                         C7300_VIRTUAL_CONSIGNMENT_ID, c205_part_number_id,
                         C7301_ITEM_QTY, C7301_ITEM_PRICE, C7301_ITEM_COGS_PRICE)
               SELECT s7301_virtual_consign_item.NEXTVAL, v_vir_consign_id,
                      t205.c205_part_number_id, t720.C7200_QTY_AVAILABLE,
                      C7200_ITEM_PRICE, C7200_ITEM_COGS_PRICE
                 FROM t7200_ld_consignment_list t720, t205_part_number t205
                WHERE t720.c205_part_number_id = t205.c205_part_number_id
                  AND t720.c701_distributor_id = v_d_id
                  AND t720.C7200_QTY_AVAILABLE <> 0;

            -- update the list table with used qty
            UPDATE t7200_ld_consignment_list
               SET C7200_QTY_USED = C7200_QTY_USED + C7200_QTY_AVAILABLE,
                   C7200_QTY_AVAILABLE = 0
             WHERE c701_distributor_id = v_d_id AND C7200_QTY_AVAILABLE <> 0;

            COMMIT;
         END LOOP;
      --
      END LOOP;
      --
      -- Below code to delete the unwanted records (null value records)
   -- DELETE FROM t7300t_virtual_consignment
      --   WHERE C7300_TOTAL_COST IS NULL;
--
   END gm_sm_lad_unass_logic_main;

--
--***********************************************************************
--Procedure to load error log information
--***********************************************************************
   PROCEDURE gm_sm_lad_log_info (
      p_start_dt   DATE,
      p_end_dt     DATE,
      p_type       CHAR,
      p_message    VARCHAR2
   )
   AS
      --
      subject     VARCHAR2 (100)         := 'SALES CONSIGNMENT LOAD SUCCESS ';
      mail_body   VARCHAR2 (3000);
      crlf        VARCHAR2 (2)                      := CHR (13) || CHR (10);
      to_mail     t906_rules.c906_rule_value%TYPE;
--
   BEGIN
      IF (p_type = 'E')
      THEN
         --
         subject := 'SALES CONSIGNMENT LOAD ERROR ******** ';
      --
      END IF;

      -- Final log insert statement
      INSERT INTO t910_load_log_table
                  (c910_load_log_id, c910_log_name,
                   c910_state_dt, c910_end_dt, c910_status_details,
                   c910_status_fl
                  )
           VALUES (s910_load_log.NEXTVAL, 'gm_pkg_sm_load_consignment',
                   p_start_dt, p_end_dt, p_message,
                   p_type
                  );

      --
      mail_body :=
            ' SALES CONSIGNMENT STATUS'
         || crlf
         || ' STATUS	 :- '
         || p_type
         || crlf
         || ' START Time	:- '
         || TO_CHAR (p_start_dt, 'dd/mm/yyyy hh24:mi:ss')
         || crlf
         || ' END Time :- '
         || TO_CHAR (p_end_dt, 'dd/mm/yyyy hh24:mi:ss')
         || crlf
         || ' MESSAGE '
         || p_message;
      --
      to_mail := get_rule_value ('SALESCONS', 'EMAIL');
      --DBMS_OUTPUT.put_line ('Inside gm_sm_lad_consignment posting ****' || to_mail);
      --
      gm_com_send_email_prc (to_mail, subject, mail_body);
--
   END gm_sm_lad_log_info;

----**************************************************

   ----**************************************************
   PROCEDURE gm_sm_lad_update_cogs_and_cost
--
   AS
      v_vir_consign_id   t7300t_virtual_consignment.C7300_VIRTUAL_CONSIGNMENT_ID%TYPE;

      CURSOR virtual_csg_id
      IS
         SELECT DISTINCT C7300_VIRTUAL_CONSIGNMENT_ID ID
                    FROM t7300t_virtual_consignment;
   BEGIN
-- update the total cost
      FOR vcsg_id IN virtual_csg_id
      LOOP
         v_vir_consign_id := vcsg_id.ID;

         UPDATE t7300t_virtual_consignment
            SET C7300_TOTAL_COST =
                      (SELECT SUM (C7301_ITEM_QTY * NVL (C7301_ITEM_PRICE, 0))
                         FROM t7301t_virtual_consign_item
                        WHERE C7300_VIRTUAL_CONSIGNMENT_ID = v_vir_consign_id)
          WHERE C7300_VIRTUAL_CONSIGNMENT_ID = v_vir_consign_id;

         -- update the COGS value
         UPDATE t7300t_virtual_consignment
            SET C7300_TOTAL_COGS_COST =
                   (SELECT SUM (C7301_ITEM_QTY * NVL (C7301_ITEM_COGS_PRICE, 0))
                      FROM t7301t_virtual_consign_item
                     WHERE C7300_VIRTUAL_CONSIGNMENT_ID = v_vir_consign_id)
          WHERE C7300_VIRTUAL_CONSIGNMENT_ID = v_vir_consign_id;
      END LOOP;

      -- Below code to delete the unwanted records (null value records)
      DELETE FROM t7300t_virtual_consignment
            WHERE C7300_TOTAL_COST IS NULL;
   END gm_sm_lad_update_cogs_and_cost;

--
   /*
   * Procedure to validate the load process.
   * 1. The used qty in t7200_ld_consignment_list should be 0
   * 2. SUM of (t7200_ld_consignment_list.C7200_QTY_RECEIVED * t7200_ld_consignment_list.C7200_ITEM_PRICE) should be equal to SUM (t7300_virtual_consignment.C7300_TOTAL_COST)
   * 3. check for duplicates in set_mapping
   */
--
   PROCEDURE gm_sm_lad_validation
   AS
      v_qty_gt_zero_count        NUMBER := 0;
      v_ld_list_total_value      NUMBER := 0;
      v_virtual_csg_total_cost   NUMBER := 0;
      v_cnt_duplicate            NUMBER := 0;
      v_start_dt                 DATE   := SYSDATE;
   BEGIN
      SELECT COUNT (1)
        INTO v_qty_gt_zero_count
        FROM t7200_ld_consignment_list t720
       WHERE t720.C7200_QTY_AVAILABLE > 0;

      IF v_qty_gt_zero_count > 0
      THEN
         gm_sm_lad_log_info
            (v_start_dt,
             SYSDATE,
             'E',
             'Available Qty is greater than zero. Refer gm_pkg_sm_load_consignment.gm_sm_lad_validation'
            );
      END IF;

      SELECT SUM (NVL (C7200_QTY_RECEIVED, 0) * NVL (C7200_ITEM_PRICE, 0))
        INTO v_ld_list_total_value
        FROM t7200_ld_consignment_list;

      SELECT SUM (NVL (C7300_TOTAL_COST, 0))
        INTO v_virtual_csg_total_cost
        FROM t7300_virtual_consignment;

      IF v_ld_list_total_value <> v_virtual_csg_total_cost
      THEN
         gm_sm_lad_log_info
            (v_start_dt,
             SYSDATE,
             'E',
             'Total Value from Load Consignment LIst doesnt match with Virtual Consignment. Refer gm_pkg_sm_load_consignment.gm_sm_lad_validation'
            );
      END IF;
   END gm_sm_lad_validation;

--

   --***********************************************************************
-- Procedure to load data from temp table to actual consignment table
--***********************************************************************
   PROCEDURE gm_sm_lad_temp_to_actual
   AS
   BEGIN
      DELETE FROM t7301_virtual_consign_item;

      --
      DELETE FROM t7300_virtual_consignment;

      --
      --Load data from Temp to actual table
      INSERT INTO t7300_virtual_consignment
                  (C7300_VIRTUAL_CONSIGNMENT_ID, c701_distributor_id,
                   c207_set_id, C7300_TOTAL_COST, C7300_LAST_LOAD_DT,
                   C7300_TOTAL_COGS_COST,C5010_TAG_ID)
         SELECT C7300_VIRTUAL_CONSIGNMENT_ID, c701_distributor_id,
                c207_set_id, C7300_TOTAL_COST, SYSDATE, C7300_TOTAL_COGS_COST,C5010_TAG_ID
           FROM t7300t_virtual_consignment;

      --
      --
      INSERT INTO t7301_virtual_consign_item
                  (C7301_VIRTUAL_CONSIGN_ITEM_ID, C7300_VIRTUAL_CONSIGNMENT_ID,
                   c205_part_number_id, C7301_ITEM_QTY, C7301_ITEM_PRICE,
                   C7301_ITEM_COGS_PRICE)
         SELECT C7301_VIRTUAL_CONSIGN_ITEM_ID, C7300_VIRTUAL_CONSIGNMENT_ID,
                c205_part_number_id, C7301_ITEM_QTY, C7301_ITEM_PRICE,
                C7301_ITEM_COGS_PRICE
           FROM t7301t_virtual_consign_item;
   END gm_sm_lad_temp_to_actual;

--
--***********************************************************************
-- Procedure to backup the data before loading the data into main
-- master data
--***********************************************************************
   PROCEDURE gm_sm_lad_virtual_backup
   AS
      --
      v_day   CHAR (1);
   BEGIN
      SELECT TO_CHAR (SYSDATE, 'D')
        INTO v_day
        FROM DUAL;

      --
      -- To delete the old transaction before loading the new transaction
      DELETE FROM t7301b_virtual_consign_item
            WHERE C7300_VIRTUAL_CONSIGNMENT_ID IN (
                                            SELECT C7300_VIRTUAL_CONSIGNMENT_ID
                                              FROM t7300b_virtual_consignment
                                             WHERE C7300_LOCK_FL = v_day);

      --
      --
      DELETE FROM t7300b_virtual_consignment
            WHERE C7300_LOCK_FL = v_day;

      --
      -- To load the main virtual consignment backup table
      INSERT INTO t7300b_virtual_consignment
                  (C7300B_VIRTUAL_CONSIGNMENT_ID, C7300_VIRTUAL_CONSIGNMENT_ID,
                   c701_distributor_id, c207_set_id, C7300_TOTAL_COST,
                   C7300_LAST_LOAD_DT, C7300_LOCK_FL, C7300_TOTAL_COGS_COST)
         SELECT s7300b_virtual_consignment_id.NEXTVAL,
                C7300_VIRTUAL_CONSIGNMENT_ID, c701_distributor_id, c207_set_id,
                C7300_TOTAL_COST, SYSDATE, v_day, C7300_TOTAL_COGS_COST
           FROM t7300_virtual_consignment;

      --WHERE c701_distributor_id IN (7, 8, 9);

      --
      --
      INSERT INTO t7301b_virtual_consign_item
                  (C7301_VIRTUAL_CONSIGN_ITEM_ID, C7300_VIRTUAL_CONSIGNMENT_ID,
                   c205_part_number_id, C7301_ITEM_QTY, C7301_ITEM_PRICE,
                   C7301_ITEM_COGS_PRICE)
         SELECT s7301b_virtual_consignitem_id.NEXTVAL,
                C7300_VIRTUAL_CONSIGNMENT_ID, c205_part_number_id,
                C7301_ITEM_QTY, C7301_ITEM_PRICE, C7301_ITEM_COGS_PRICE
           FROM t7301_virtual_consign_item;
--
   END gm_sm_lad_virtual_backup;

	/***********************************************************************
	* Procedure to call the Additional Inventory (AI) Quota load
	*-***********************************************************************/
	PROCEDURE gm_sm_lad_ai_quota
	AS
	BEGIN
		UPDATE t9300_job
			 SET c9300_next_date = TRUNC (SYSDATE)
		 WHERE c9300_job_nm = 'CONSIGNMENTLOAD';

		IF (SQL%ROWCOUNT = 0)
		THEN
			INSERT INTO t9300_job t9300
						(t9300.c9300_job_id, t9300.c9300_job_nm, t9300.c9300_next_date
						)
				 VALUES (s9300_job.NEXTVAL, 'CONSIGNMENTLOAD', TRUNC (SYSDATE)
						);
		END IF;

		COMMIT;
		gm_pkg_sm_lad_ai_quota.gm_sm_lad_ai_quota_main;
	END gm_sm_lad_ai_quota;
END gm_pkg_sm_load_consignment;
/