CREATE OR REPLACE VIEW v_in_list
AS
	SELECT	   TRIM (SUBSTR (txt
						   , INSTR (txt, ',', 1, LEVEL) + 1
						   , INSTR (txt, ',', 1, LEVEL + 1) - INSTR (txt, ',', 1, LEVEL) - 1
							)
					) AS token
		  FROM (SELECT ',' || SYS_CONTEXT ('acctinlist_ctx', 'txt', 4000) || ',' txt
				  FROM DUAL)
	CONNECT BY LEVEL <=
					 LENGTH (SYS_CONTEXT ('acctinlist_ctx', 'txt', 4000))
				   - LENGTH (REPLACE (SYS_CONTEXT ('acctinlist_ctx', 'txt', 4000), ',', ''))
				   + 1
/
CREATE OR REPLACE VIEW v_double_in_list
AS
	SELECT	   TRIM (SUBSTR (txt
						   , INSTR (txt, SYS_CONTEXT ('acctitoken_ctx', 'txt', 1), 1, LEVEL) + 1
						   ,   INSTR (txt, SYS_CONTEXT ('acctitoken_ctx', 'txt', 1), 1, LEVEL + 1)
							 - INSTR (txt, SYS_CONTEXT ('acctitoken_ctx', 'txt', 1), 1, LEVEL)
							 - 1
							)
					) AS token
			 , TRIM (SUBSTR (txtii
						   , INSTR (txtii, SYS_CONTEXT ('acctitoken_ctx', 'txt', 1), 1, LEVEL) + 1
						   ,   INSTR (txtii, SYS_CONTEXT ('acctitoken_ctx', 'txt', 1), 1, LEVEL + 1)
							 - INSTR (txtii, SYS_CONTEXT ('acctitoken_ctx', 'txt', 1), 1, LEVEL)
							 - 1
							)
					) AS tokenii
		  FROM (SELECT	  SYS_CONTEXT ('acctitoken_ctx', 'txt', 1)
					   || SYS_CONTEXT ('acctinlisti_ctx', 'txt', 4000)
					   || SYS_CONTEXT ('acctitoken_ctx', 'txt', 1) txt
					 ,	  SYS_CONTEXT ('acctitoken_ctx', 'txt', 1)
					   || SYS_CONTEXT ('acctinlistii_ctx', 'txt', 4000)
					   || SYS_CONTEXT ('acctitoken_ctx', 'txt', 1) txtii
				  FROM DUAL)
	CONNECT BY LEVEL <=
					 LENGTH (SYS_CONTEXT ('acctinlisti_ctx', 'txt', 4000))
				   - LENGTH (REPLACE (SYS_CONTEXT ('acctinlisti_ctx', 'txt', 4000)
									, SYS_CONTEXT ('acctitoken_ctx', 'txt', 1)
									, ''
									 )
							)
				   + 1
/
