/* Formatted on 2008/12/12 15:27 (Formatter Plus v4.8.0) */

CREATE OR REPLACE PACKAGE BODY my_context
IS
	PROCEDURE set_my_inlist_ctx (
		p_str	IN	 VARCHAR2
	)
	IS
	BEGIN
		DBMS_SESSION.set_context ('acctinlist_ctx', 'txt', p_str);
	END set_my_inlist_ctx;

	--
	PROCEDURE set_my_double_inlist_ctx (
		p_stri	  IN   VARCHAR2
	  , p_strii   IN   VARCHAR2
	  , p_token   IN VARCHAR2 DEFAULT ','	
	)
	IS
	BEGIN
		DBMS_SESSION.set_context ('acctinlisti_ctx', 'txt', p_stri);
		DBMS_SESSION.set_context ('acctinlistii_ctx', 'txt', p_strii);
		DBMS_SESSION.set_context ('acctitoken_ctx', 'txt', p_token);
	END set_my_double_inlist_ctx;

--

	PROCEDURE set_double_inlist_ctx (
		p_stri	 IN   VARCHAR2
	  , p_tokeni   IN	VARCHAR2 DEFAULT ','
	)
	IS
		v_string	   VARCHAR2 (30000) := p_stri;
		v_substring    VARCHAR2 (5000);
		v_type		   VARCHAR2 (1000);
		v_value 	   VARCHAR2 (1000);
		v_type_str	   VARCHAR2 (5000);
		v_value_str    VARCHAR2 (5000);
	BEGIN
		WHILE INSTR (v_string, '|') <> 0
		LOOP
			v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
			v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
			v_type		:= NULL;
			v_value 	:= NULL;
			v_type		:= SUBSTR (v_substring, 1, INSTR (v_substring, p_tokeni) - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, p_tokeni) + 1);
			v_value 	:= v_substring;
			v_type_str	:= v_type_str || v_type || p_tokeni;
			v_value_str := v_value_str || v_value || p_tokeni;
		END LOOP;

		DBMS_SESSION.set_context ('acctinlisti_ctx', 'txt', v_type_str);
		DBMS_SESSION.set_context ('acctinlistii_ctx', 'txt', v_value_str);
		DBMS_SESSION.set_context ('acctitoken_ctx', 'txt', p_tokeni);
	END set_double_inlist_ctx;
--
END my_context;

