/* Formatted on 2008/12/19 17:36 (Formatter Plus v4.8.0) */
CREATE OR REPLACE VIEW v_in_list
AS
	SELECT	   TRIM (SUBSTR (txt
						   , INSTR (txt, ',', 1, LEVEL) + 1
						   , INSTR (txt, ',', 1, LEVEL + 1) - INSTR (txt, ',', 1, LEVEL) - 1
							)
					) AS token
		  FROM (SELECT ',' || SYS_CONTEXT ('inlist_ctx', 'txt', 4000) || ',' txt
				  FROM DUAL)
	CONNECT BY LEVEL <=
					 LENGTH (SYS_CONTEXT ('inlist_ctx', 'txt', 4000))
				   - LENGTH (REPLACE (SYS_CONTEXT ('inlist_ctx', 'txt', 4000), ',', ''))
				   + 1
/
CREATE OR REPLACE VIEW v_double_in_list
AS
	SELECT	   TRIM (SUBSTR (txt
						   , INSTR (txt, SYS_CONTEXT ('inlisttoken_ctx', 'txt', 1), 1, LEVEL) + 1
						   ,   INSTR (txt, SYS_CONTEXT ('inlisttoken_ctx', 'txt', 1), 1, LEVEL + 1)
							 - INSTR (txt, SYS_CONTEXT ('inlisttoken_ctx', 'txt', 1), 1, LEVEL)
							 - 1
							)
					) AS token
			 , TRIM (SUBSTR (txtii
						   , INSTR (txtii, SYS_CONTEXT ('inlisttoken_ctx', 'txt', 1), 1, LEVEL) + 1
						   ,   INSTR (txtii, SYS_CONTEXT ('inlisttoken_ctx', 'txt', 1), 1, LEVEL + 1)
							 - INSTR (txtii, SYS_CONTEXT ('inlisttoken_ctx', 'txt', 1), 1, LEVEL)
							 - 1
							)
					) AS tokenii
		  FROM (SELECT	  SYS_CONTEXT ('inlisttoken_ctx', 'txt', 1)
					   || SYS_CONTEXT ('inlisti_ctx', 'txt', 4000)
					   || SYS_CONTEXT ('inlisttoken_ctx', 'txt', 1) txt
					 ,	  SYS_CONTEXT ('inlisttoken_ctx', 'txt', 1)
					   || SYS_CONTEXT ('inlistii_ctx', 'txt', 4000)
					   || SYS_CONTEXT ('inlisttoken_ctx', 'txt', 1) txtii
				  FROM DUAL)
	CONNECT BY LEVEL <=
					 LENGTH (SYS_CONTEXT ('inlisti_ctx', 'txt', 4000))
				   - LENGTH (REPLACE (SYS_CONTEXT ('inlisti_ctx', 'txt', 4000)
									, SYS_CONTEXT ('inlisttoken_ctx', 'txt', 1)
									, ''
									 )
							)
				   + 1
/
