/*
Run in DWH Globus_ETL
*/
CREATE OR REPLACE FUNCTION get_n_project_ids(
    p_group_size_n IN INTEGER )
  RETURN VARCHAR2
IS
  /*  Description     : Return comma delimited string of n projects.
  Parameters   : p_group_size_n -- The number of projects to concatenate
  */
  groupList      VARCHAR2 (6596 BYTE);
  t_group_size_n INTEGER;
BEGIN
  IF p_group_size_n > 150 THEN
    t_group_size_n := 150;
  ELSE
    t_group_size_n := p_group_size_n;
  END IF;
WITH groupList AS
  (SELECT EPl.C202_PROJECT_ID project_id
  , rownum id
  FROM ETL_PROJECT_LIST EPl
  WHERE rownum                <= t_group_size_n
  AND EPL.ETL_PROJECT_LOAD_FL IS NULL
  )
SELECT MAX (SYS_CONNECT_BY_PATH (project_id_str, ','))
INTO groupList
FROM
  (SELECT ''''
    || project_id
    || '''' project_id_str
  , ROW_NUMBER () OVER (ORDER BY id) AS curr
  FROM groupList
  )
  CONNECT BY curr - 1 = PRIOR curr
  START WITH curr     = 1;
RETURN SUBSTR (groupList, 2) ;
END get_n_project_ids;
/