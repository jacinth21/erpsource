/*
File name: get_dw_loaner_status.fnc
Created By: Aprasath
Description: This is providing loaner set status
SVN Location: @"C:\Projects\Branches\PMT\db\Functions\DWH\DW\get_dw_loaner_status.fnc";
This is created for PMT-27831 to retrieve loaner status from function 
 */
CREATE or REPLACE FUNCTION get_dw_loaner_status
  (
    p_statusflag IN t504a_consignment_loaner.c504a_status_fl%TYPE )
  RETURN VARCHAR2
IS
  v_status_name VARCHAR2 (100);
BEGIN
  SELECT DECODE (p_statusflag, 0, 'Available', 5, 'Allocated', 7, 'Pending Pick', 10, 'Pending Shipping', 13, 'Packing In Progress', 16, 'Ready For Pickup', 20, 'Pending Return', 21, 'Disputed', 22, 'Missing', 23, 'Deployed', 25, 'Pending Check', 30, 'WIP - Loaners', 40, 'Pending Verification', 50, 'Pending Process', 55, 'Pending Picture', 58, 'Pending Putaway', 60, 'Inactive', 24, 'Pending Acceptance')
  INTO v_status_name
  FROM DUAL;
  RETURN v_status_name;

END;
/