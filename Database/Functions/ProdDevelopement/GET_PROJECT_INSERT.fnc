CREATE OR REPLACE FUNCTION GET_PROJECT_INSERT
(
	p_part_id t205_part_number.c205_part_number_id%TYPE
)
RETURN VARCHAR2
IS
/**********************************************************************
Description     : This function returns the insert ID of a Part Number
Change History	: Jan15,2007	James	Changed to get Insert ID from Part
										Number Table
**********************************************************************/
v_part_ins   t205_part_number.c205_insert_id%TYPE;
BEGIN
	BEGIN
		SELECT	c205_insert_id INSID
		INTO	v_part_ins
		FROM	t205_part_number
		WHERE	c205_part_number_id = p_part_id;
	EXCEPTION
		WHEN NO_DATA_FOUND
	THEN
		RETURN ' ';
	END;
	RETURN v_part_ins;
END GET_PROJECT_INSERT;
/