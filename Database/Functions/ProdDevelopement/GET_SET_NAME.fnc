CREATE OR REPLACE FUNCTION GET_SET_NAME
(p_set_id T207_SET_MASTER.C207_SET_ID%TYPE)
RETURN VARCHAR2
IS
/*  Description     : THIS FUNCTION RETURNS CODE NAME GIVEN THE CODE_ID
 Parameters 		: p_code_id
*/
v_set_nm  T207_SET_MASTER.C207_SET_NM%TYPE;
BEGIN
     BEGIN
       SELECT C207_SET_NM
	   INTO v_set_nm
	   FROM  T207_SET_MASTER
       WHERE C207_SET_ID = p_set_id;
   EXCEPTION
      WHEN NO_DATA_FOUND
   THEN
      RETURN ' ';
      END;
     RETURN v_set_nm;
END GET_SET_NAME;
/

