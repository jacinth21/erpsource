/* Formatted on 2009/06/14 18:03 (Formatter Plus v4.8.0) */
CREATE OR REPLACE FUNCTION get_partnum_product (
	p_part_num	 t205_part_number.c205_part_number_id%TYPE
)
	RETURN NUMBER
IS
	/*	Description 	: THIS FUNCTION RETURNS CODE NAME GIVEN THE CODE_ID
	 Parameters 		: p_code_id
	*/
	v_part_product t205_part_number.c205_product_family%TYPE;
BEGIN
	BEGIN
		SELECT c205_product_family prodfamily
		  INTO v_part_product
		  FROM t205_part_number
		 WHERE c205_part_number_id = p_part_num;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN 0;
	END;

	RETURN v_part_product;
END;
