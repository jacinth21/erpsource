--"C:\Database\Functions\ProdDevelopement\get_doclassify_grp_type.fnc";

CREATE OR REPLACE FUNCTION get_doclassify_grp_type(
        p_system_id IN t9721_system_rule_type.c207_system_id%TYPE
    )
        RETURN VARCHAR2
    IS
        v_otherinfo       VARCHAR2 (500);
    BEGIN
              WITH other_info AS 
              (
 				SELECT rownum ID, t9721.C9721_SYSTEM_RULE_TYPE_ID
   				  FROM
    				(
			         SELECT C9721_SYSTEM_RULE_TYPE_ID
			           FROM t9721_system_rule_type
			          WHERE c207_system_id       = p_system_id
			            AND c9721_void_fl IS NULL
			       ORDER BY NVL (c9721_link_system_rule_type_id, c9721_system_rule_type_id),
			                c9721_system_rule_type_id
			         )t9721
				)									  
		SELECT MAX (SYS_CONNECT_BY_PATH (ID, ','))
          INTO v_otherinfo
          FROM
            (SELECT ID, ROW_NUMBER () OVER (ORDER BY id) AS curr
               FROM other_info)
          CONNECT BY curr - 1 = PRIOR curr
         START WITH curr = 1;
         
         RETURN SUBSTR (v_otherinfo,2);
       EXCEPTION
          WHEN NO_DATA_FOUND
       THEN
          RETURN '';	
    END get_doclassify_grp_type;
 /