/* Formatted on 2009/04/24 20:13 (Formatter Plus v4.8.0) */
CREATE OR REPLACE FUNCTION get_project_id_from_part (
	p_part_number	t205_part_number.c205_part_number_id%TYPE
)
	RETURN VARCHAR2
IS
/*	Description 	: THIS FUNCTION RETURNS PROJECT ID GIVEN A PART NUMBER
 Parameters 		: p_part_number
*/
	v_proj_id	   t202_project.c202_project_id%TYPE;
BEGIN
	BEGIN
		SELECT c202_project_id
		  INTO v_proj_id
		  FROM t205_part_number
		 WHERE c205_part_number_id = p_part_number;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN ' ';
	END;

	RETURN v_proj_id;
END get_project_id_from_part;
/
