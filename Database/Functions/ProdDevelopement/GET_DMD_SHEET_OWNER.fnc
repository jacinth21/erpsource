--@"C:\Database\Functions\ProdDevelopement\get_dmd_sheet_owner.fnc";
/*
 * This function will get the Demand Sheet Owner for the passed in part number. 
   The Primary group the part belong to will be identified and it will be checked in the Template in which 
   the Group is mapped to. From the template find out the Sheet owner
 */
CREATE OR REPLACE
    FUNCTION GET_DMD_SHEET_OWNER (
            p_part_num IN T4011_GROUP_DETAIL.C205_PART_NUMBER_ID%TYPE
            )
        RETURN VARCHAR2
    IS
        v_user_id VARCHAR2 (100) ;
    BEGIN
        BEGIN
            SELECT T4020.C4020_PRIMARY_USER
			INTO v_user_id
			FROM T4010_GROUP T4010,
			  T4011_GROUP_DETAIL T4011,
			  T4021_DEMAND_MAPPING T4021,
			  T4020_DEMAND_MASTER T4020
			WHERE t4010.c4010_group_id               = t4011.c4010_group_id
			AND T4010.C4010_VOID_FL                 IS NULL
			AND T4020.C4020_VOID_FL                 IS NULL
			AND T4020.C4020_INACTIVE_FL             IS NULL
			AND T4010.C901_TYPE                      = 40045 -- OP Group
			AND T4011.C205_PART_NUMBER_ID            = p_part_num
			AND T4011.C901_PART_PRICING_TYPE         = 52080 -- Primary
			AND T4020.C4020_DEMAND_MASTER_ID         = T4021.C4020_DEMAND_MASTER_ID
			AND T4021.C4021_REF_ID                   = TO_CHAR(T4011.C4010_GROUP_ID)
			AND T4020.C4020_PARENT_DEMAND_MASTER_ID IS NULL
			AND T4020.C901_DEMAND_TYPE               ='4000103' -- template
			AND T4021.C901_REF_TYPE                  ='40030'   -- group
			;
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            RETURN '';
        END;
        RETURN v_user_id;
    END GET_DMD_SHEET_OWNER;
    /
