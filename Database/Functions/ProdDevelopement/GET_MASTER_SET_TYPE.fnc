--@"C:\Database\Functions\ProdDevelopement\GET_MASTER_SET_TYPE.fnc";

CREATE OR REPLACE FUNCTION GET_MASTER_SET_TYPE
(p_set_id T207_SET_MASTER.C207_SET_ID%TYPE)
RETURN NUMBER
IS
/*  Description     : THIS FUNCTION RETURNS set type 
 Parameters 		: p_code_id 
4072	In-House
4073	Training
4074	Loaner
4077	Bone Model
4078	Demo
4127    Product Loaner  
4119    In house Loaner
*/
v_set_type  T207_SET_MASTER.c207_type%TYPE;
BEGIN
     BEGIN
       SELECT  c207_type 
	   INTO v_set_type
	   FROM  T207_SET_MASTER
       WHERE C207_SET_ID = p_set_id and C207_VOID_FL is NULL;
   EXCEPTION
      WHEN NO_DATA_FOUND
   THEN
      RETURN NULL;
      END;
     RETURN v_set_type;
END GET_MASTER_SET_TYPE;
/

