CREATE OR REPLACE FUNCTION GET_SET_SEQUENCE
(p_set_id T207_SET_MASTER.C207_SET_ID%TYPE)
RETURN NUMBER
IS
/*  Description     : THIS FUNCTION RETURNS Sequence # of a set given the Set ID
 Parameters 		: p_code_id
*/
v_set_seq  T207_SET_MASTER.C207_SEQ_NO%TYPE;
BEGIN
     BEGIN
       SELECT C207_SEQ_NO
	   INTO v_set_seq
	   FROM  T207_SET_MASTER
       WHERE C207_SET_ID = p_set_id;
   EXCEPTION
      WHEN NO_DATA_FOUND
   THEN
      RETURN 0;
      END;
     RETURN v_set_seq;
END GET_SET_SEQUENCE;
/