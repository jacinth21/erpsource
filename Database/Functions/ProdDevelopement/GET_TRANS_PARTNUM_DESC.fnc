CREATE OR REPLACE FUNCTION "GET_TRANS_PARTNUM_DESC"
(p_part_num T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE
 ,p_ref_id VARCHAR2)
RETURN VARCHAR2
IS
/*  Description     : THIS FUNCTION WILL RETURN THE TRANSLATED DESCRIPTION FOR THE PART IF THE VALUE IS AVAILABLE IN THE PART ATTRIBUTE 
*					  ELSE RETURN THE PART DESCRIPTION IN ENGLISH.  
 Parameters 		: p_partnum, p_ref_id [ refers to the transaction id].
*/
v_part_desc   T205_PART_NUMBER.C205_PART_NUM_DESC%TYPE;
v_acc_id      T704_ACCOUNT.C704_ACCOUNT_ID%TYPE;
v_sales_rep_id T703_SALES_REP.C703_SALES_REP_ID%TYPE;
v_dist_id     T701_DISTRIBUTOR.C701_DISTRIBUTOR_ID%TYPE;
v_dist_cntry_id T701_DISTRIBUTOR.C701_BILL_COUNTRY%TYPE;
v_country_cd VARCHAR2(10);
v_default_fnc VARCHAR2(1):='N';

BEGIN
     
     BEGIN
     	SELECT C704_ACCOUNT_ID INTO v_acc_id
     	FROM T501_Order
     	WHERE C501_ORDER_ID = p_ref_id
     	AND C501_VOID_FL IS NULL; 
     EXCEPTION WHEN NO_DATA_FOUND
     THEN
     	BEGIN
	     	SELECT C701_DISTRIBUTOR_ID INTO v_dist_id
	     	FROM t504_consignment
	     	WHERE C504_CONSIGNMENT_ID = p_ref_id
	     	AND C504_VOID_FL IS NULL;
	     EXCEPTION WHEN NO_DATA_FOUND
	     THEN
	     	v_default_fnc := 'Y';
	     	
	     END ;     
     END ;
     
     BEGIN
     	 IF v_acc_id IS NOT NULL
     	 THEN
	     	SELECT C703_SALES_REP_ID INTO v_sales_rep_id FROM T704_ACCOUNT WHERE C704_ACCOUNT_ID =v_acc_id;
	     END IF;
	     
	     IF v_sales_rep_id IS NOT NULL
	     THEN
	     	SELECT C701_DISTRIBUTOR_ID INTO v_dist_id FROM  T703_SALES_REP WHERE C703_SALES_REP_ID =v_sales_rep_id;
	     END IF;
	     
	     SELECT C701_BILL_COUNTRY INTO v_dist_cntry_id FROM t701_distributor WHERE C701_DISTRIBUTOR_ID = v_dist_id;
	     
	    -- SELECT C902_CODE_NM_ALT v_country_cd FROM T901_CODE_LOOKUP WHERE C901_CODE_ID = v_dist_cntry_id and C901_CODE_GRP='CNTY';
     
     EXCEPTION WHEN NO_DATA_FOUND THEN     
     	v_default_fnc := 'Y';
     END;
     
     BEGIN
     	SELECT C205D_ATTRIBUTE_VALUE INTO v_part_desc FROM T205D_PART_ATTRIBUTE
     	WHERE C205_PART_NUMBER_ID = p_part_num AND 
     	C901_ATTRIBUTE_TYPE = v_dist_cntry_id;
     
     EXCEPTION WHEN NO_DATA_FOUND THEN     
     	v_default_fnc := 'Y';
     END;
     
     IF (v_default_fnc = 'Y')
     THEN
     	SELECT get_partdesc_by_company(p_part_num) INTO v_part_desc FROM DUAL;
     END IF;
     
    
     RETURN v_part_desc;
END;
/

