--@"C:\Database\Functions\ProdDevelopement\get_link_retry_count.fnc"
CREATE OR REPLACE
    FUNCTION get_link_retry_count (
            p_share_id IN T1017_share_detail.c1016_share_id%TYPE)
        RETURN VARCHAR2
    IS
        /**********************************************************************
        * Description     : Functions returns the count of the number of times the link was retried with Egnyte
        *  Parameters  :p_share_id
        ***********************************************************************/
        v_retry_cnt T1018_share_file.C1018_link_retry_cnt%TYPE;
    BEGIN
         SELECT COUNT (1)
           INTO v_retry_cnt
           FROM T1018_share_file
          WHERE c1016_share_id                = p_share_id
            AND C1018_VOID_FL                IS NULL
            AND NVL (C1018_LINK_RETRY_CNT, 0) > 4;
        RETURN v_retry_cnt;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN NULL;
    END;
    /