CREATE OR REPLACE FUNCTION GET_PART_PRICE_LIST
(p_part_num T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE,
 p_price_type VARCHAR2)
RETURN VARCHAR2
IS
/*  Description     : This Function returns the Part Price based on the Price Type.
 Parameters 		: p_part_num , p_price_type
*/
v_price  VARCHAR2(20);
BEGIN
	 BEGIN
		 	SELECT 	GET_PART_PRICE(p_part_num,'L') 
		 	INTO	v_price
		 	FROM 	DUAL;
   EXCEPTION
      WHEN NO_DATA_FOUND
   THEN
      RETURN ' ';
      END;
     RETURN v_price;
END GET_PART_PRICE_LIST;
/

