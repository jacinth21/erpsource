--@"C:\Database\Functions\ProdDevelopement\get_doc_file_name.fnc";
CREATE OR REPLACE
    FUNCTION GET_DOC_FILE_NAME (
            p_date VARCHAR2,
            p_DocId T210_DOCUMENT.C210_DOCUMENT_ID%TYPE)
        RETURN VARCHAR2
    IS
        /*  Description     : THIS FUNCTION RETURNS CODE NAME GIVEN THE CODE_ID
        Parameters   : p_docid, p_date
        */
        v_doc_file_name VARCHAR2 (100) ;
         v_dateformat varchar2(100);
    BEGIN
        BEGIN
        select get_compdtfmt_frm_cntx() into v_dateformat from dual;
             SELECT filename
               INTO v_doc_file_name
               FROM
                (
                     SELECT c211_doc_file_name filename
                       FROM t211_document_history
                      WHERE c210_document_id     = p_DocId
                        AND trunc(c211_activated_date) <= TO_DATE (p_date, v_dateformat)
                   ORDER BY c211_activated_date DESC
                )
              WHERE ROWNUM = 1;
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            RETURN '';
        END;
        RETURN v_doc_file_name;
    END GET_DOC_FILE_NAME;
    /
