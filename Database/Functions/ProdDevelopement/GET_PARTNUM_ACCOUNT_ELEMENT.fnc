CREATE OR REPLACE FUNCTION GET_PARTNUM_ACCOUNT_ELEMENT
(
	 p_act_type			IN	t802_account_object_mapping.c901_account_type%TYPE	
	,p_partnum 			IN	t810_posting_txn.c205_part_number_id%TYPE	
)
RETURN NUMBER
IS
/*******************************************************************************  
 * Description     : This function returns part number account mapping details 
 *					  Based on Account Type and Part Number 
 * Parameters 		: p_customer_po
 *					  p_account_id
 ********************************************************************************/
--
v_account_element_id	t810_posting_txn.c801_account_element_id%TYPE;
p_fetch_flg				CHAR(1);
--
BEGIN
	--Below code to get Part, is mapped to specific account 
	--To check the Mapping @ Part Level 
	BEGIN
	--
		p_fetch_flg := 1;
		--
		SELECT	c801_account_element_id
		INTO	v_account_element_id
		FROM	t802_account_object_mapping
		WHERE	c901_account_type	= p_act_type
		AND		c802_object_id		= p_partnum;
	EXCEPTION
	WHEN NO_DATA_FOUND THEN
		p_fetch_flg := 2;
	END;
	--
	-- If Part not mapped @ Part Level 
	-- Check if the part is mapped @ Group level 
	-- like Implant, Instrunment and Graphic case 
	IF (p_fetch_flg = 2) THEN
	--
		BEGIN
		--
			SELECT	c801_account_element_id
			INTO	v_account_element_id
			FROM	t802_account_object_mapping
			WHERE	c901_account_type	= p_act_type
			AND		c802_object_id = 
				(SELECT c205_product_family FROM t205_part_number WHERE c205_part_number_id = p_partnum) ;
		--
		EXCEPTION
		WHEN NO_DATA_FOUND THEN
			p_fetch_flg := 3;
		END;
	--
	END IF;
	--
	--
	-- Below code is used for COGS and other account 
	-- fetch  the information @ Account Type level
	IF (p_fetch_flg = 3) THEN
	--
		BEGIN
		--
			SELECT	c801_account_element_id
			INTO	v_account_element_id
			FROM	t802_account_object_mapping
			WHERE	c901_account_type = p_act_type
			AND		c802_object_id IS NULL;
		EXCEPTION
		WHEN NO_DATA_FOUND THEN
			DBMS_OUTPUT.PUT_LINE('No Record found for INV Posting '  || p_act_type || ' PARTNUM * ' || p_partnum);
			RETURN NULL;
		END;
	--
	END IF;
	--
	RETURN  v_account_element_id;
EXCEPTION
WHEN NO_DATA_FOUND
THEN
--
	RETURN NULL;
--
END GET_PARTNUM_ACCOUNT_ELEMENT;
/

