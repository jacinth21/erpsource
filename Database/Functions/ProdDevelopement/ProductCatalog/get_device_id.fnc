/* Formatted on 2009/12/28 15:14 (Formatter Plus v4.8.0) */
--@"C:\Database\Functions\ProdDevelopement\ProductCatalog\get_device_id.fnc";

CREATE OR REPLACE FUNCTION get_device_id (
	p_token	T101A_USER_TOKEN.C101a_Token_Id%TYPE
  , p_uuid	t9150_device_info.c9150_device_uuid%TYPE
)
	RETURN VARCHAR2
IS
/*******************************************************
	Description     : this function returns device info id for given the token
*******************************************************/
	v_deviceid   T9150_Device_Info.C9150_Device_Uuid%TYPE;
BEGIN  
	
 SELECT t9150.C9150_Device_info_id INTO v_deviceid
   FROM T101A_USER_TOKEN t101a, T9150_Device_Info t9150
  WHERE T101a.C101a_Token_Id = p_token
    AND T9150.c9150_device_uuid = p_uuid
    AND T101a.C101_User_Id    = T9150.C101_User_Id
    AND T101a.C101a_Void_Fl  IS NULL
    AND T9150.C9150_Void_Fl  IS NULL;

	RETURN v_deviceid;
EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		RETURN '';
END get_device_id;
/