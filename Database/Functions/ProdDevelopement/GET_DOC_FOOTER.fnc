CREATE OR REPLACE FUNCTION GET_DOC_FOOTER
(
 p_date	  VARCHAR2,
 p_DocId  T210_DOCUMENT.C210_DOCUMENT_ID%TYPE)
RETURN VARCHAR2
IS
/*  Description     : THIS FUNCTION RETURNS CODE NAME GIVEN THE CODE_ID
 Parameters 		: p_code_id
*/
v_footer_desc   VARCHAR2(100);
 v_dateformat varchar2(100);
BEGIN
     BEGIN
select get_compdtfmt_frm_cntx() into v_dateformat from dual;
		SELECT DOCFOOTER INTO v_footer_desc from
		(
		  SELECT A.C210_DOCUMENT_ID || ';  Rev.' || trim(B.C211_REV) || ';  DCO# ' || B.C211_DOC_DCO_ID || '^' ||trim(B.C211_REV) || '^' || C211_ACTIVE_FL
		  DOCFOOTER
		  FROM T210_DOCUMENT A, T211_DOCUMENT_HISTORY B
		  WHERE A.C210_DOCUMENT_ID = B.C210_DOCUMENT_ID
		  AND A.C210_DOCUMENT_ID = p_DocId
		  AND TRUNC(B.C211_ACTIVATED_DATE) <= to_date(p_date,v_dateformat)
		  ORDER BY B.C211_ACTIVATED_DATE DESC
		)
		WHERE ROWNUM = 1;

   EXCEPTION
      WHEN NO_DATA_FOUND
   THEN
      RETURN ' ';
      END;
     RETURN v_footer_desc;
END GET_DOC_FOOTER;
/

