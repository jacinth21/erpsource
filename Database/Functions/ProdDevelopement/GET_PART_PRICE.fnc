CREATE OR REPLACE FUNCTION GET_PART_PRICE
(p_part_num 	T2052_PART_PRICE_MAPPING.C205_PART_NUMBER_ID%TYPE,
 p_price_type 	VARCHAR2,
 p_company_id  	T1900_COMPANY.C1900_COMPANY_ID%TYPE DEFAULT NULL)   -- Company id added for PMT-27847: Tier Price error in Process Consignment and Initiate Consignment screens
RETURN VARCHAR2
IS
/*  Description     : This Function returns the Part Price based on the Price Type and Company ID.
 Parameters 		: p_part_num , p_price_type
*/
	v_price  VARCHAR2(20);
	v_company_id T1900_COMPANY.C1900_COMPANY_ID%TYPE;
	v_default_company T1900_COMPANY.C1900_COMPANY_ID%TYPE;
BEGIN
		SELECT 	get_compid_frm_cntx()
		INTO 	v_company_id  
		FROM 	DUAL;
	v_company_id := nvl(p_company_id,v_company_id);
	
	v_default_company := NVL(get_rule_value('DEFAULT_COMPANY','ONEPORTAL'),'1000');
	
	IF v_company_id IS NULL THEN
   		v_company_id := v_default_company;
   	END IF;
   	
   	/* Table T205_PART_NUMBER is added to avoid the empty space return when there is no data found is thrown
   	 * Consider a part is manufactured and price is not set at that time if we are not joining T205_PART_NUMBER
   	 * No data will be returned at that point.
	*/
	 BEGIN
	 	IF p_price_type = 'E' THEN
	 	
		  	   SELECT 	T2052.C2052_EQUITY_PRICE
			   INTO 	v_price
	   		   FROM  	T2052_PART_PRICE_MAPPING T2052,T205_PART_NUMBER T205
       		   WHERE  	T205.C205_PART_NUMBER_ID = p_part_num
       		   AND      T205.C205_PART_NUMBER_ID = T2052.C205_PART_NUMBER_ID(+)
       		   AND 		T2052.C1900_COMPANY_ID(+) = v_company_id;
       		   
	  	  ELSIF p_price_type = 'C' THEN
	  	         		   
       		   SELECT 	T2052.C2052_CONSIGNMENT_PRICE
			   INTO 	v_price
	   		   FROM  	T2052_PART_PRICE_MAPPING T2052,T205_PART_NUMBER T205
       		   WHERE  	T205.C205_PART_NUMBER_ID = p_part_num
       		   AND      T205.C205_PART_NUMBER_ID = T2052.C205_PART_NUMBER_ID(+)
       		   AND 		T2052.C1900_COMPANY_ID(+) = v_company_id;
       		   
         	   ELSIF p_price_type = 'L' THEN
       		   
       		   SELECT 	T2052.C2052_LIST_PRICE
			   INTO 	v_price
	   		   FROM  	T2052_PART_PRICE_MAPPING T2052,T205_PART_NUMBER T205
       		   WHERE  	T205.C205_PART_NUMBER_ID = p_part_num
       		   AND      T205.C205_PART_NUMBER_ID = T2052.C205_PART_NUMBER_ID(+)
       		   AND 		T2052.C1900_COMPANY_ID(+) = v_company_id;
       		   
	  	  ELSE
       		   
       		   SELECT 	T2052.C2052_LOANER_PRICE
			   INTO 	v_price
	   		   FROM  	T2052_PART_PRICE_MAPPING T2052,T205_PART_NUMBER T205
       		   WHERE  	T205.C205_PART_NUMBER_ID = p_part_num
       		   AND      T205.C205_PART_NUMBER_ID = T2052.C205_PART_NUMBER_ID(+)
       		   AND 		T2052.C1900_COMPANY_ID(+) = v_company_id;
       		   
	 	END IF;
   EXCEPTION
      WHEN NO_DATA_FOUND
   THEN
      RETURN ' ';
      END;
     RETURN v_price;
END GET_PART_PRICE;
/

