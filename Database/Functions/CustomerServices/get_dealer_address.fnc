/* Formatted on 2011/10/26 16:20 (Formatter Plus v4.8.0) */
--@"C:\Database\Functions\CustomerServices\get_dealer_address.fnc";

CREATE OR REPLACE FUNCTION get_dealer_address (
	p_party_id   IN   t101_party.c101_party_id%TYPE
)
	RETURN VARCHAR2
IS
	v_shipadd      VARCHAR2(1000);
BEGIN
	
	SELECT c106_zip_code
                   || '<BR>&'||'nbsp;'
                   || get_code_name(c901_state) || c106_city
                   || DECODE(c106_add1,NULL,'',c106_add1 || '<BR>&'||'nbsp;')                        
                   || DECODE(c106_add2,NULL,'',c106_add2)
              INTO  v_shipadd                                                             
              FROM t101_party t101, t106_address t106
             WHERE t101.c101_party_id 	= t106.c101_party_id
               AND t101.c101_party_id = p_party_id
               AND t106.c106_primary_fl = 'Y'
               AND t106.c106_void_fl IS NULL;
               
  RETURN v_shipadd;
EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		RETURN NULL;
END get_dealer_address;
/