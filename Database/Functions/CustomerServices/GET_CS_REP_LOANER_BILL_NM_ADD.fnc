/* Formatted on 2008/12/29 15:59 (Formatter Plus v4.8.0) */
--@"C:\database\functions\CustomerServices\get_cs_rep_loaner_bill_nm_add.fnc";

CREATE OR REPLACE FUNCTION get_cs_rep_loaner_bill_nm_add (
	p_consign_id   t504_consignment.c504_consignment_id%TYPE
)
	RETURN VARCHAR2
IS
/***************************************************************************************
 * Description			 : This function returns the name of the person to whom the set
	   is loaned to.
 *
 ****************************************************************************************/
	v_id		   NUMBER;
	v_to_id 	   VARCHAR2 (20);
	v_ac_id 	   VARCHAR2 (20);
	v_rep_id	   VARCHAR2 (20);
	v_loan_add	   VARCHAR2 (1000);
	v_sales_assoc_id VARCHAR2 (20);
BEGIN
	SELECT c901_consigned_to, c504a_consigned_to_id, c704_account_id, c703_sales_rep_id, c703_ass_rep_id 
	  INTO v_id, v_to_id, v_ac_id, v_rep_id, v_sales_assoc_id
	  FROM t504a_loaner_transaction
	 WHERE c504_consignment_id = p_consign_id AND c504a_return_dt IS NULL AND c504a_void_fl IS NULL;

--
	IF v_id = 50170
	THEN
		v_loan_add	:= '&nbsp;&nbsp;D:' || get_distributor_name (v_to_id);
	ELSIF v_id = 50171
	THEN
		v_loan_add	:= get_account_name (v_to_id);
	ELSIF v_id = 50172
	THEN
		v_loan_add	:= get_user_name (v_to_id);
	END IF;
	
	IF v_ac_id IS NOT NULL
	  THEN
	    v_loan_add := v_loan_add || '<BR>' || '&nbsp;&nbsp;A:' || get_account_name (v_ac_id) ;
	END IF;
	
	IF v_rep_id IS NOT NULL
	  THEN
	    v_loan_add := v_loan_add || '<BR>' || '&nbsp;&nbsp;R:' || get_rep_name (v_rep_id);
	END IF;
	
	IF v_sales_assoc_id IS NOT NULL
	  THEN
	    v_loan_add := v_loan_add || '<BR>' || 'AR:' || get_rep_name (v_sales_assoc_id);
	END IF;
	
	RETURN v_loan_add;
EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		RETURN ' ';
END get_cs_rep_loaner_bill_nm_add;
/
