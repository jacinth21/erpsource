CREATE OR REPLACE FUNCTION get_released_qty (
       p_partnum    T205_part_number.c205_part_number_id%TYPE
  , p_order_id      t501_order.c501_order_id%TYPE
  , p_part_price    T502_item_order.c502_item_price%TYPE
)
       RETURN NUMBER
IS
       v_releaseqty NUMBER := 0;
BEGIN
       BEGIN
             SELECT NVL(SUM(t502.C502_item_qty),0) INTO v_releaseqty
                FROM t501_order t501, t502_item_order t502
               WHERE t502.c205_part_number_id = p_partnum
                 AND t502.c502_item_price     = p_part_price 
                 AND t502.c501_order_id       = t501.c501_order_id       
                 AND t501.C501_ref_id         = p_order_id
                 AND t501.c501_void_fl        IS NULL
                 AND t502.c502_void_fl        IS NULL;
       EXCEPTION
       WHEN NO_DATA_FOUND
       THEN
             v_releaseqty := 0;
       END;
RETURN v_releaseqty;
END get_released_qty;
/
