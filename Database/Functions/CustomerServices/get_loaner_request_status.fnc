/* Formatted on 2011/10/27 10:54 (Formatter Plus v4.8.0) */
--@"C:\Database\Functions\CustomerServices\get_loaner_request_status.fnc";

CREATE OR REPLACE
    FUNCTION get_loaner_request_status (
            p_prodreqid IN t526_product_request_detail.c526_product_request_detail_id%TYPE)
        RETURN VARCHAR2
    IS
        v_status VARCHAR2 (50) ;
    BEGIN
         SELECT DECODE (c526_status_fl, '5', 'Pending Approval','7', 'Pending SM Approval', '10', 'Open', '20', 'Allocated', '30', 'Closed',
            '40', 'Rejected', '13', 'Pick In Progress', '16', 'Ready For Pickup', c526_status_fl)
           INTO v_status
           FROM t526_product_request_detail
          WHERE c526_product_request_detail_id = p_prodreqid
            AND c526_void_fl                  IS NULL;
        RETURN v_status;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN ' ';
    END get_loaner_request_status;
    /
