--@"C:\Database\Functions\CustomerServices\get_open_return_fl.fnc";
/*******************************************************
* Description : Procedure to check if there's open
* return associated with order
* author  : Xun
*******************************************************/
CREATE OR REPLACE
    FUNCTION get_open_return_fl (
            p_order_id t502_item_order.c501_order_id%TYPE)
        RETURN VARCHAR2
    IS
        v_count NUMBER;
    BEGIN
         SELECT COUNT (1)
           INTO v_count
           FROM T506_RETURNS
          WHERE c501_order_id  = p_order_id 
            AND c506_status_fl = '0' --initiated
            AND c506_void_fl  IS NULL;
        IF v_count             > 0 THEN
            RETURN 'Y';
        ELSE
            RETURN '';
        END IF;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN '';
    END get_open_return_fl ;
    /