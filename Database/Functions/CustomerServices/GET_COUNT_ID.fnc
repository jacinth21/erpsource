CREATE OR REPLACE FUNCTION GET_COUNT_ID
(
	p_acc_id T502_ITEM_ORDER.C501_ORDER_ID%TYPE
)
RETURN VARCHAR2
IS
/*  
    Description : THIS FUNCTION RETURNS CODE NAME GIVEN THE CODE_ID
    Parameters	: p_code_id
*/
v_acc_nm VARCHAR2(10);
BEGIN
	SELECT	COUNT(*) INTO v_acc_nm 
	FROM 
	(
		SELECT	DISTINCT c205_part_number_id
		FROM	t502_item_order
		WHERE	c501_order_id = p_acc_id
		AND	c901_type = 50300
		AND	c502_void_fl IS NULL 
		AND	c502_delete_fl IS NULL
	);
	RETURN v_acc_nm;
END GET_COUNT_ID;
/