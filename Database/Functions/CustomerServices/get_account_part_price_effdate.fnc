/* Formatted on 2011/04/20 15:41 (Formatter Plus v4.8.0) */
--@"C:\Database\Functions\CustomerServices\get_account_part_price_effdate .fnc";

CREATE OR REPLACE FUNCTION get_account_part_price_effdate (
	p_acc_id   t704_account.c704_account_id%TYPE
  , p_num	   t705_account_pricing.c205_part_number_id%TYPE
  , p_gpoid    t705_account_pricing.c101_party_id%TYPE
)
	RETURN VARCHAR2
IS
/*
 Description  : this function returns effective date for a part for the specified Account
 Parameters  : p_acc_id
 Parameters  : p_num
*/
	v_eff_date	   VARCHAR2 (20);
BEGIN
	--
	IF p_gpoid <> '0'
	THEN
		BEGIN
			SELECT TO_CHAR (NVL (c705_last_updated_date, c705_created_date), 'mm/dd/yyyy')
			  INTO v_eff_date
			  FROM t705_account_pricing
			 WHERE c101_party_id = p_gpoid AND c205_part_number_id = p_num AND ROWNUM = 1;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				SELECT TO_CHAR (NVL (c705_last_updated_date, c705_created_date), 'mm/dd/yyyy')
				  INTO v_eff_date
				  FROM t705_account_pricing
				 WHERE c205_part_number_id = p_num AND c704_account_id = p_acc_id AND ROWNUM = 1;
		END;
	ELSE
		SELECT TO_CHAR (NVL (c705_last_updated_date, c705_created_date), 'mm/dd/yyyy')
		  INTO v_eff_date
		  FROM t705_account_pricing
		 WHERE c205_part_number_id = p_num AND c704_account_id = p_acc_id AND ROWNUM = 1;
	END IF;

	RETURN v_eff_date;
--
EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		RETURN NULL;
--
END get_account_part_price_effdate;
/
