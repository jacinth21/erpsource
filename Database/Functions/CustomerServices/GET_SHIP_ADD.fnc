--@"C:\Database\Functions\CustomerServices\GET_SHIP_ADD.fnc"

CREATE OR REPLACE FUNCTION GET_SHIP_ADD
(
 	   p_ShipTo T501_ORDER.C501_SHIP_TO%TYPE,
	   p_acc_id T704_ACCOUNT.C704_ACCOUNT_ID%TYPE,
	   p_rep_id T704_ACCOUNT.C703_SALES_REP_ID%TYPE,
	   p_ShipToId T501_ORDER.C501_SHIP_TO_ID%TYPE
)
RETURN VARCHAR2
IS
/*  Description     : THIS FUNCTION RETURNS CODE NAME GIVEN THE CODE_ID
 Parameters 		: p_code_id
*/
v_ship_add  VARCHAR2(1000);
v_val VARCHAR2(72);
v_ship_id VARCHAR2(20);
v_comp_id T1900_COMPANY.C1900_COMPANY_ID%TYPE;
v_delimiter VARCHAR2(10);
v_zip_code_fl VARCHAR2(10);
BEGIN
     BEGIN
	 v_val := GET_CODE_NAME(p_ShipTo);
	 --
	 SELECT  NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL')) INTO v_comp_id FROM DUAL;
	 -- This rule will get the Delimiter that needs to be shown in the Zip Code Section. For Country Like Germany, they do not need a delimiter.
		SELECT NVL(get_rule_value_by_company('DELIMITER','ZIP_CODE_DELIM', v_comp_id),',')
		, NVL(get_rule_value_by_company('SWAP_ZIP_CODE','SWAP_ZIP_CODE', v_comp_id),'N')
		INTO v_delimiter, v_zip_code_fl FROM DUAL;
	 --
	 IF v_val = 'Hospital'	THEN
	    BEGIN
	   		SELECT  C704_SHIP_NAME ||'<BR>&nbsp;'|| C704_SHIP_ADD1 ||'<BR>&nbsp;'|| DECODE(C704_SHIP_ADD2,NULL,'',C704_SHIP_ADD2 ||'<BR>&nbsp;')|| 
	   		DECODE(v_zip_code_fl ,'Y',C704_SHIP_ZIP_CODE,C704_SHIP_CITY)
			||v_delimiter||'&nbsp;'|| GET_CODE_NAME_ALT(C704_SHIP_STATE)	||'&nbsp;'|| 
			DECODE(v_zip_code_fl ,'Y',C704_SHIP_CITY,C704_SHIP_ZIP_CODE)
			INTO v_ship_add
			FROM  T704_ACCOUNT
			WHERE  C704_ACCOUNT_ID = p_acc_id;
		END;
	 ELSIF v_val = 'Distributor' THEN
	 	 BEGIN
	   		SELECT  C701_SHIP_NAME ||'<BR>&nbsp;'|| C701_SHIP_ADD1 ||'<BR>&nbsp;'|| DECODE(C701_SHIP_ADD2,NULL,'',C701_SHIP_ADD2 ||'<BR>&nbsp;')|| 
	   		DECODE(v_zip_code_fl ,'Y',C701_SHIP_ZIP_CODE,C701_SHIP_CITY) ||v_delimiter||'&nbsp;'
			|| GET_CODE_NAME_ALT(C701_SHIP_STATE) ||'&nbsp;'|| 
			DECODE(v_zip_code_fl ,'Y',C701_SHIP_CITY,C701_SHIP_ZIP_CODE)
			INTO v_ship_add
			FROM  T701_DISTRIBUTOR
			WHERE  C701_DISTRIBUTOR_ID = ( SELECT GET_DISTRIBUTOR_ID(p_rep_id) FROM DUAL);
		 END;
	 ELSIF v_val = 'Sales Rep' THEN
	  	 BEGIN
		 	
		 	IF p_ShipToId IS NOT NULL THEN
			   v_ship_id := p_ShipToId;
			ELSE
				v_ship_id := p_rep_id;	
			END IF;
			
		 	SELECT  C703_SALES_REP_NAME ||'<BR>&nbsp;'|| C703_SHIP_ADD1 ||'<BR>&nbsp;'|| DECODE(C703_SHIP_ADD2,NULL,'',C703_SHIP_ADD2 ||'<BR>&nbsp;') || 
		 	DECODE(v_zip_code_fl ,'Y',C703_SHIP_ZIP_CODE,C703_SHIP_CITY) ||v_delimiter||'&nbsp;'
			|| GET_CODE_NAME_ALT(C703_SHIP_STATE)	||'&nbsp;'||
			DECODE(v_zip_code_fl ,'Y',C703_SHIP_CITY,C703_SHIP_ZIP_CODE) 
			INTO v_ship_add
			FROM  T703_SALES_REP
			WHERE  C703_SALES_REP_ID = v_ship_id;
		 END;
	  END IF;
   EXCEPTION
      WHEN NO_DATA_FOUND
   THEN
      RETURN ' ';
      END;
     RETURN v_ship_add;
END GET_SHIP_ADD;
/

