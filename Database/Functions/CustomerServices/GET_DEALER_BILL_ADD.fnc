CREATE OR REPLACE FUNCTION GET_DEALER_BILL_ADD
(
	p_dealer_id t101_party.c101_party_id%TYPE
)
RETURN VARCHAR2
IS
/*  Description     : THIS FUNCTION RETURNS the Dealer Address
 	Parameters 		: p_dealer_id
*/
v_dealer_bill_add CLOB;

BEGIN
	
     BEGIN
	   		SELECT t106.c106_bill_name ||'<BR>&nbsp;'|| t106.c106_add1 ||'<BR>&nbsp;'|| t106.c106_add2 ||'<BR>&nbsp;' || 
	   			   t106.c106_city || ',' ||'&nbsp;'|| GET_CODE_NAME_ALT(t106.c901_state) ||'&nbsp;'|| 
			  	   t106.c106_zip_code || '<BR>&nbsp;' || get_code_name(t106.c901_state)
			 INTO  v_dealer_bill_add
			 FROM  t106_address t106,t101_party t101
			WHERE  t106.c101_party_id  = t101.c101_party_id
        	  AND  t106.c101_party_id  = p_dealer_id
        	  AND  t106.c106_primary_fl = 'Y'
        	  AND  t106.c106_void_fl IS NULL
        	  AND  t101.c101_void_fl IS NULL;
   EXCEPTION
      WHEN NO_DATA_FOUND
   THEN
      RETURN ' ';
      END;
     RETURN v_dealer_bill_add;
END GET_DEALER_BILL_ADD;
/

