/* Formatted on 2011/10/26 16:20 (Formatter Plus v4.8.0) */
--@"C:\Database\Functions\CustomerServices\get_order_receive_mode.fnc";

CREATE OR REPLACE FUNCTION get_order_receive_mode (
	p_ref_id   IN   t501_order.c501_order_id%TYPE
)
	RETURN VARCHAR2
IS
/*  Description     : This function is to return the receive mode of order
*/
	v_receive_mode      t501_order.c501_receive_mode%TYPE;
BEGIN
	
	SELECT c501_receive_mode
		INTO v_receive_mode
	FROM t501_order
	WHERE c501_order_id = p_ref_id
	AND c501_void_fl   IS NULL;
               
  	RETURN v_receive_mode;
EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		RETURN NULL;
END get_order_receive_mode;
/