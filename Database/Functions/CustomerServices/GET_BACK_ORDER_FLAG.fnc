/* Formatted on 2008/06/26 21:46 (Formatter Plus v4.8.0) */
CREATE OR REPLACE FUNCTION get_back_order_flag (
	p_part_num	 t502_item_order.c205_part_number_id%TYPE
)
	RETURN VARCHAR2
IS
/**********************************************************************
 * Description	   : THIS FUNCTION RETURNS 'Y' IF ITS BACK ORDER ELSE 'N'
 *	Parameters	 : p_part_num
 ***********************************************************************/
	v_flag		   CHAR (1);
	v_company_id  t1900_company.c1900_company_id%TYPE;
v_plant_id T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
BEGIN
--

	SELECT	get_compid_frm_cntx(), NVL(get_plantid_frm_cntx(),3000)
	INTO	v_company_id, v_plant_id
	FROM	dual;
	
	SELECT 'Y'
	  INTO v_flag
	  FROM t501_order t501, t502_item_order t502
	 WHERE t501.c501_order_id = t502.c501_order_id
	   AND t501.c501_status_fl = 0	 -- Maps to Back order not processed
	   AND t501.c901_order_type = 2525	 -- Maps to backorder
	   AND t502.c205_part_number_id = p_part_num
	   AND t501.c501_delete_fl IS NULL
	   AND t501.c501_void_fl IS NULL
	   AND t501.c1900_company_id = v_company_id
	   AND ROWNUM = 1;

	--
	RETURN v_flag;
--
EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		RETURN 'N';
END get_back_order_flag;
/
