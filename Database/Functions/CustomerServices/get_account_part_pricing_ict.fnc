/* Formatted on 2011/04/20 15:41 (Formatter Plus v4.8.0) */
--@"C:\Database\Functions\CustomerServices\get_account_part_pricing_ict.fnc";

CREATE OR REPLACE FUNCTION get_part_account_pricing_ict (
	p_acc_id   t704_account.c704_account_id%TYPE
  , p_num	   t705_account_pricing.c205_part_number_id%TYPE
  , p_gpoid    t705_account_pricing.c101_party_id%TYPE
)
	RETURN NUMBER
IS
/*
 Description  : this function returns price for a part for the specified Account
 Parameters  : p_acc_id
 Parameters  : p_num
*/
	v_price 	   t705_account_pricing.c705_unit_price%TYPE;
	v_acc_id	   t704_account.c704_account_id%TYPE;
	v_gpo_id	   t705_account_pricing.c101_party_id%TYPE;
	v_rowcnt	   NUMBER;
	v_group_id	   t4010_group.c4010_group_id%TYPE;
	v_count        NUMBER;
	v_ref_id       VARCHAR2(20);
	
BEGIN
	--
	IF p_gpoid <> '0'
	THEN
		BEGIN
            -- Check if the part has individual or Group Price 
			SELECT COUNT(1) INTO v_count
			FROM t7051_account_group_pricing T7051
			WHERE c7051_ref_id = p_num
			AND (c101_gpo_id = p_gpoid OR  t7051.c704_account_id = p_acc_id) 
			AND c901_ref_type = 52001 -- part
			AND c7051_void_fl is null
			AND c7051_active_fl = 'Y';
			
			v_ref_id := p_num;
			
            -- If Group Price then 
			IF v_count = 0 THEN
				SELECT t4010.c4010_group_id
					  INTO v_group_id
					  FROM t4011_group_detail t4011, t4010_group t4010
					 WHERE t4010.c4010_group_id = t4011.c4010_group_id
					   AND t4010.c4010_void_fl IS NULL
					   AND t4010.c901_type = 40045
					   AND t4011.c205_part_number_id = p_num
					   AND t4011.c901_part_pricing_type = 52080;   -- primary par
					
					v_ref_id := v_group_id;
			END IF;
			
            -- Will get the pricin from the the part or Group 
			SELECT c7051_price INTO v_price
					FROM t7051_account_group_pricing T7051
					WHERE c101_gpo_id = p_gpoid
					AND t7051.c7051_ref_id = v_ref_id
					AND c7051_void_fl IS NULL
					AND c7051_active_fl = 'Y';
			--PMT-695 - When request the price for Account and mapped in the group account, that time should load account price in new order screen.
			EXCEPTION	WHEN NO_DATA_FOUND	THEN
				BEGIN
						SELECT c7051_price
				  		INTO v_price
				  		FROM t7051_account_group_pricing t7051
				 		WHERE t7051.c704_account_id = p_acc_id
				   		AND t7051.c7051_ref_id = v_ref_id
				   		AND t7051.c7051_void_fl IS NULL
				   		AND c7051_active_fl = 'Y';
				EXCEPTION	WHEN NO_DATA_FOUND	THEN   	
					BEGIN
						  SELECT c705_unit_price
						  INTO v_price
						  FROM t705_account_pricing t705
						 WHERE c101_party_id = p_gpoid AND c205_part_number_id = p_num 
	                     AND t705.c705_void_fl IS NULL AND ROWNUM = 1;
					EXCEPTION	WHEN NO_DATA_FOUND	THEN
						SELECT c705_unit_price
						  INTO v_price
						  FROM t705_account_pricing t705
						 WHERE c205_part_number_id = p_num AND c704_account_id = p_acc_id 
	                     AND t705.c705_void_fl IS NULL AND ROWNUM = 1;
	                END;
				END;
			END;
	ELSE
		BEGIN
			--
			BEGIN
                -- Check Part Level pricing 
				SELECT c7051_price
				  INTO v_price
				  FROM t7051_account_group_pricing t7051
				 WHERE t7051.c704_account_id = p_acc_id
				   AND C901_REF_TYPE = 52001 -- PART 
				   AND t7051.c7051_ref_id = p_num
				   AND t7051.c7051_void_fl IS NULL
				   AND c7051_active_fl = 'Y';
			EXCEPTION
				WHEN NO_DATA_FOUND
				THEN
					-- Check Group level pricing 
                    SELECT t4010.c4010_group_id
					  INTO v_group_id
					  FROM t4011_group_detail t4011, t4010_group t4010
					 WHERE t4010.c4010_group_id = t4011.c4010_group_id
					   AND t4010.c4010_void_fl IS NULL
					   AND t4010.c901_type = 40045
					  -- AND t4010.c4010_visible_ac_fl IS NOT NULL
					   AND t4011.c205_part_number_id = p_num
					   AND t4011.c901_part_pricing_type = 52080;   -- primary part

					SELECT c7051_price
					  INTO v_price
					  FROM t7051_account_group_pricing t7051
					 WHERE t7051.c704_account_id = p_acc_id
					   AND C901_REF_TYPE = 52000 -- GROUP
					   AND t7051.c7051_ref_id = to_char(v_group_id)
					   AND t7051.c7051_void_fl IS NULL
					   AND c7051_active_fl = 'Y';
			END;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
                -- Account Level pricing in old table 
				SELECT c705_unit_price
				  INTO v_price
				  FROM t705_account_pricing t705
				 WHERE c205_part_number_id = p_num AND c704_account_id = p_acc_id 
                   AND t705.c705_void_fl IS NULL AND ROWNUM = 1;
		END;
	END IF;

	RETURN v_price;
--
EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		RETURN '';
--
END get_part_account_pricing_ict;
/