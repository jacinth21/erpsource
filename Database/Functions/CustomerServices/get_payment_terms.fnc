/* Formatted on 2011/10/26 16:20 (Formatter Plus v4.8.0) */
--@"C:\Database\Functions\CustomerServices\get_jp_payment_terms.fnc";
--This function is used to get Payment Due Date based on payment term of Account or Dealer
CREATE OR REPLACE FUNCTION get_payment_terms (
	p_invoice_date   IN   t503_invoice.C503_INVOICE_DATE%TYPE,
	p_payment_terms   IN   t503_invoice.C503_TERMS%TYPE
)
	RETURN DATE
IS
	v_invoicedate      t503_invoice.C503_INVOICE_DATE%TYPE;
	v_dateformat       varchar2(100);
	v_months           varchar2(20);
	v_days             varchar2(20);
	v_inv_lastday      t503_invoice.C503_INVOICE_DATE%TYPE;
	v_inv_termsdate    t503_invoice.C503_INVOICE_DATE%TYPE;
BEGIN

  --To get Days
  IF p_payment_terms IS NOT NULL THEN
	  SELECT NVL(GET_CODE_NAME_ALT(p_payment_terms),0)
	    INTO v_days
	    FROM DUAL;
	    
	   --To get Months
	  SELECT NVL(GET_CODE_CONTROL_TYPE(p_payment_terms),0)
	    INTO v_months
	    FROM DUAL;  
    
    IF p_invoice_date IS NOT NULL 
    THEN
    		--102012 = LAST DAY OF MONTH
    	IF v_days ='102012' 
    	THEN 
         	SELECT Last_Day(ADD_MONTHS(p_invoice_date,v_months))
	          INTO v_invoicedate
	          FROM DUAL;        
		ELSE 
	   		IF v_months = 0 THEN
		      SELECT (p_invoice_date + v_days)
		        INTO v_invoicedate
		        FROM DUAL;
		   ELSE
		      SELECT v_days+add_months((Last_Day(ADD_MONTHS(p_invoice_date,-1))+1),GET_CODE_CONTROL_TYPE(p_payment_terms))-1
		        INTO v_invoicedate
		        FROM DUAL;      
	       END IF;
    	END IF;
 	END IF;
 END IF;
    
  RETURN v_invoicedate;
EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		RETURN NULL;
END get_payment_terms;
/