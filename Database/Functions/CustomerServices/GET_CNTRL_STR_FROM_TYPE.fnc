/* Formatted on 2014/03/18 18:36 (Formatter Plus v4.8.0) */
--@"C:\Database\Functions\CustomerServices\get_cntrl_str_from_type.fnc";
CREATE OR REPLACE FUNCTION get_cntrl_str_from_type (
	p_cntrl_str   CLOB,
	p_type		  VARCHAR
)
	RETURN VARCHAR
IS
/*	Description 	: THIS FUNCTION RETURNS INPUT STRING FROM PASSING TYPE 
*/
	v_string		CLOB;
	v_substring     CLOB;
	v_cntrl_str		VARCHAR2 (4000);
BEGIN	
		IF INSTR(p_cntrl_str,p_type) <> 0 THEN
			v_string := p_cntrl_str;
				WHILE INSTR (v_string, '|') <> 0
	   			LOOP
					v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
	      			v_string := SUBSTR (v_string, INSTR (v_string, '|') + 1);
	      				IF INSTR(v_substring,p_type) <> 0 THEN
	      					v_cntrl_str := v_cntrl_str ||  v_substring ||'|';
	      				END IF;
	      		END LOOP;
		END IF;	
	RETURN v_cntrl_str;
END get_cntrl_str_from_type;
/