
/*************************************************************************
 * Description : This function returns the sum of Ack Order qty
 *************************************************************************/
CREATE OR REPLACE FUNCTION get_ackord_partqty (
  p_order_id	t501_order.c501_order_id%TYPE
  ,p_partnum 	T205_part_number.c205_part_number_id%TYPE
)
	RETURN NUMBER
IS
	v_ackorder_qty NUMBER := 0;
BEGIN
	BEGIN		    
	  SELECT NVL(SUM (t502.c502_item_qty),0) INTO v_ackorder_qty
	    FROM t502_item_order t502, t501_order t501
	   WHERE t502.c501_order_id = t501.c501_order_id
	     AND t502.c501_order_id IN  ( SELECT c501_ref_id 
	                                   FROM t501_order 
	                                  WHERE c501_order_id = p_order_id
	                                    AND c501_void_fl  IS NULL)
	     AND t502.c205_part_number_id = p_partnum
	     AND t501.c501_void_fl  IS NULL
		 AND t502.c502_void_fl  IS NULL;	
		    
	EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		v_ackorder_qty := 0;
	END;
RETURN nvl(v_ackorder_qty,0);
END get_ackord_partqty;
/