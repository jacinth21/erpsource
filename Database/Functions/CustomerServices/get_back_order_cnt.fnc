/**********************************************************************
 * Description       : This Function is used to get the back order count 
                       for parent order_id/Order id
 * Parameters        : p_order_id
 * Auhtor            : 
 ***********************************************************************/
CREATE OR REPLACE FUNCTION get_back_order_cnt (
    p_order_id t501_order.c501_order_id%TYPE
) RETURN NUMBER IS
    v_back_ord_cnt NUMBER;
BEGIN
--
    SELECT
        COUNT(1)
    INTO v_back_ord_cnt
    FROM
        t501_order
    WHERE
        (c501_parent_order_id = p_order_id OR c501_order_id = p_order_id)
        AND c501_void_fl IS NULL
		AND c901_order_type = 2525 -- Back Order Type
        AND c501_update_inv_fl IS NULL;

  
  --

    RETURN v_back_ord_cnt;
--
EXCEPTION
    WHEN no_data_found THEN
        RETURN 0;
END get_back_order_cnt;
/