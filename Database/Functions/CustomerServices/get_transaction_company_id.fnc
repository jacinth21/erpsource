/* Formatted on 2011/10/27 10:54 (Formatter Plus v4.8.0) */
--@"C:\Database\Functions\CustomerServices\get_transaction_company_id.fnc";

CREATE OR REPLACE
    FUNCTION get_transaction_company_id (
            p_company_id IN t1900_company.c1900_company_id%TYPE,
            p_ref_id     IN  VARCHAR2,
            p_filter 	 IN  VARCHAR2,
            p_trans_type IN VARCHAR2 )
        RETURN VARCHAR2
    IS
        v_filter     VARCHAR2(20);
        v_trans_company_id  VARCHAR2(20);
    BEGIN
        
	    v_trans_company_id := p_company_id;
	    
	    BEGIN
			SELECT GET_RULE_VALUE(p_company_id,p_filter)
	          INTO v_filter 
	         FROM DUAL;
    	EXCEPTION WHEN NO_DATA_FOUND THEN
      		v_filter := NULL;
		END;    	
    	
	    IF v_filter = 'Plant' THEN
	    
		    IF p_trans_type = 'LOANERREQUEST' THEN
		    
		    	BEGIN 
			    	SELECT NVL(c1900_company_id, p_company_id)
	                  INTO v_trans_company_id 
	                  FROM t525_product_request 
	                 WHERE c525_product_request_id = p_ref_id 
	                   AND c525_void_fl IS NULL 
	              GROUP BY c1900_company_id;
			    EXCEPTION WHEN NO_DATA_FOUND THEN
		    		BEGIN
			    		SELECT NVL(c1900_company_id, p_company_id)
	    				  INTO v_trans_company_id
	     				  FROM t526_product_request_detail 
	   					 WHERE to_char(c526_product_request_detail_id) = p_ref_id 
	       				   AND c526_void_fl IS NULL 
	  				  GROUP BY c1900_company_id;
			    	EXCEPTION WHEN NO_DATA_FOUND THEN
			    		v_trans_company_id := p_company_id;
			    	END;
			    END;
		    ELSIF p_trans_type = 'LOANERTRANSACTION' THEN
		    	BEGIN 
			    	SELECT NVL(t504a.c1900_company_id ,p_company_id)
	    			  INTO v_trans_company_id
	      			  FROM t504_consignment t504, t504a_loaner_transaction t504a
	     			 WHERE  t504.c504_consignment_id = t504a.c504_consignment_id
	       			   AND t504.c504_consignment_id = p_ref_id 
	       			   AND c504_void_fl IS NULL
	       			   AND t504a.c504a_return_dt IS NULL
	       			   AND t504a.c504a_void_fl IS NULL 
	  			  GROUP BY t504a.c1900_company_id ; 
			    EXCEPTION WHEN NO_DATA_FOUND THEN
		    		BEGIN
			    		SELECT NVL(c1900_company_id, p_company_id)  
	   					  INTO v_trans_company_id
	      				  FROM t504_consignment
	     				 WHERE c504_consignment_id = p_ref_id
	       				   AND c504_void_fl IS NULL 
	  				  GROUP BY c1900_company_id;
			    	EXCEPTION WHEN NO_DATA_FOUND THEN
			    		v_trans_company_id := p_company_id;
			    	END;
			    END;	    
		    ELSIF p_trans_type = 'ORDER' THEN
		    	BEGIN
			    	SELECT NVL(c1900_company_id, p_company_id) 
	     			  INTO v_trans_company_id
	       			  FROM t501_order 
	      			 WHERE c501_order_id = p_ref_id 
	        		   AND c501_void_fl IS NULL 
	   			  GROUP BY c1900_company_id;
			    EXCEPTION WHEN NO_DATA_FOUND THEN
			    	v_trans_company_id := p_company_id;
			    END;
			ELSIF p_trans_type = 'INHOUSE' THEN
		    	BEGIN
			    	SELECT NVL(c1900_company_id, p_company_id)
	    			  INTO v_trans_company_id
	      			  FROM t412_inhouse_transactions 
	     			 WHERE c412_inhouse_trans_id  = p_ref_id 
	       			   AND c412_void_fl IS NULL
	  			  GROUP BY c1900_company_id; 
			    EXCEPTION WHEN NO_DATA_FOUND THEN
			    	v_trans_company_id := p_company_id;
			    END;
			ELSIF p_trans_type = 'SHIPPING' THEN
		    	BEGIN
			    	SELECT NVL(c1900_company_id, p_company_id)
	     			  INTO v_trans_company_id
	       			  FROM t907_shipping_info
	      			 WHERE c907_ref_id = p_ref_id
	        		   AND c907_void_fl IS NULL
	   			  GROUP BY c1900_company_id; 
			    EXCEPTION WHEN NO_DATA_FOUND THEN
			    	v_trans_company_id := p_company_id;
			    END;
			ELSIF p_trans_type = 'DIST' THEN
		    	BEGIN
			    	SELECT NVL(c1900_company_id, p_company_id)
	    			  INTO v_trans_company_id
	     			  FROM t701_distributor 
	    			 WHERE c701_distributor_id = p_ref_id
	      			   AND c701_void_fl IS NULL 
	 			  GROUP BY c1900_company_id; 
			    EXCEPTION WHEN NO_DATA_FOUND THEN
			    	v_trans_company_id := p_company_id;
			    END;
			ELSIF p_trans_type = 'CONSIGNMENT' THEN
		    	BEGIN
			    	SELECT NVL(c1900_company_id, p_company_id)  
	   				  INTO v_trans_company_id
	      			  FROM t504_consignment
	     			 WHERE c504_consignment_id = p_ref_id
	       			   AND c504_void_fl IS NULL 
	  			  GROUP BY c1900_company_id;
			    EXCEPTION WHEN NO_DATA_FOUND THEN
			    	v_trans_company_id := p_company_id;
			    END;
			    ELSIF p_trans_type = 'RETURNS' THEN
		    	BEGIN
			    	SELECT NVL(c1900_company_id, p_company_id)  
	   				  INTO v_trans_company_id
	      			  FROM t506_returns
	     			 WHERE c506_rma_id = p_ref_id
	       			   AND c506_void_fl IS NULL 
	  			  GROUP BY c1900_company_id;
			    EXCEPTION WHEN NO_DATA_FOUND THEN
			    	v_trans_company_id := p_company_id;
			    END;
			ELSIF p_trans_type = 'CONSIGNMENTREQUEST' THEN
		    	BEGIN
			    	SELECT NVL(c1900_company_id, p_company_id) 
			       	   INTO v_trans_company_id 
			           FROM t520_request
			           WHERE c520_request_id = p_ref_id
			           AND c520_void_fl IS NULL
  			  	  GROUP BY c1900_company_id;
			    EXCEPTION WHEN NO_DATA_FOUND THEN
		    	BEGIN
			       	SELECT NVL(c1900_company_id, p_company_id)	
	      			  INTO v_trans_company_id 
			          FROM t504_consignment
			          WHERE c504_consignment_id = p_ref_id
			          AND c504_void_fl is null 
			       GROUP BY c1900_company_id;
			    EXCEPTION WHEN NO_DATA_FOUND THEN
			    	v_trans_company_id := p_company_id;
			    END;
			   END;
		    END IF;	    
	    END IF; 	    
        RETURN v_trans_company_id;
        
    EXCEPTION
    WHEN OTHERS THEN
        RETURN v_trans_company_id;       
        
END get_transaction_company_id;
/
