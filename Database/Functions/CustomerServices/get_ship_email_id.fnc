CREATE OR replace function get_ship_email_id
(  p_ship_to	           NUMBER
  ,p_ship_to_id            VARCHAR2
  ,p_mode                  VARCHAR2)
   return                  VARCHAR2
IS
    v_ship_to	   CLOB;
	v_rep_id	   VARCHAR2 (20);
	v_party_id	   VARCHAR2 (20);
	v_defaultemail VARCHAR2 (100);
BEGIN
--
	SELECT get_rule_value ('DEFAULTSHIPEMAIL', 'EMAIL')
	  INTO v_defaultemail
	  FROM DUAL;

	IF p_ship_to = 4120
	THEN
		v_ship_to	:= get_distributor_email (p_ship_to_id);
	ELSIF p_ship_to = 4121
	THEN
		v_party_id	:= TRIM (get_rep_party_id (p_ship_to_id));
		v_ship_to	:= TRIM (gm_pkg_cm_contact.get_contact_value (v_party_id, p_mode));
	ELSIF p_ship_to = 4122
	THEN
		v_rep_id	:= TRIM (get_rep_id (p_ship_to_id));
		v_party_id	:= TRIM (get_rep_party_id (v_rep_id));
		v_ship_to	:= TRIM (gm_pkg_cm_contact.get_contact_value (v_party_id, p_mode));
	ELSIF p_ship_to = 4123
	THEN
		v_ship_to	:= get_user_emailid (p_ship_to_id);
	ELSIF p_ship_to = 26240419 --26240419: Plant
	THEN
		v_ship_to	:=  get_plant_email_id (p_ship_to_id);
	END IF;

	IF v_ship_to IS NULL
	THEN
		v_ship_to	:= v_defaultemail;
	END IF;

	RETURN v_ship_to;
EXCEPTION
	WHEN OTHERS
	THEN
		RETURN v_defaultemail;
END get_ship_email_id;
/
