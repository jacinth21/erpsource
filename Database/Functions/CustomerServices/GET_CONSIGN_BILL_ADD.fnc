--@"C:\Database\Functions\CustomerServices\GET_CONSIGN_BILL_ADD.fnc"

CREATE OR REPLACE FUNCTION GET_CONSIGN_BILL_ADD
(p_consign_id T504_CONSIGNMENT.C504_CONSIGNMENT_ID%TYPE)
RETURN VARCHAR2
IS
/*  Description     : THIS FUNCTION RETURNS CODE NAME GIVEN THE CODE_ID
 Parameters 		: p_code_id
*/
v_Id VARCHAR2(20);
v_bill_add VARCHAR2(1000);
v_type T504_CONSIGNMENT.C504_TYPE%TYPE;
v_ref_id T504_CONSIGNMENT.C504_REF_ID%TYPE;
v_dist_id T504_CONSIGNMENT.C701_DISTRIBUTOR_ID%TYPE;
v_ship_to_id T504_CONSIGNMENT.C504_SHIP_TO_ID%TYPE;
v_ship_to T504_CONSIGNMENT.C504_SHIP_TO%TYPE;
v_loan_to VARCHAR2(2000);
v_plant_comp_id   t1900_company.c1900_company_id%TYPE;
v_comp_name  t906_rules.c906_rule_value%TYPE;
BEGIN
     BEGIN
	 	    SELECT C704_ACCOUNT_ID, C504_TYPE, C504_REF_ID, C701_DISTRIBUTOR_ID, C504_SHIP_TO_ID, C504_SHIP_TO 
			INTO v_Id, v_type, v_ref_id, v_dist_id, v_ship_to_id, v_ship_to
			FROM T504_CONSIGNMENT 
			WHERE C504_CONSIGNMENT_ID = p_consign_id
			AND C504_VOID_FL IS NULL;
			
			IF v_type = 9110 THEN -- IHLN
				BEGIN
					SELECT     get_user_name (t7103.c101_loan_to_id)
					     INTO  v_loan_to
					    FROM t7104_case_set_information t7104, t526_product_request_detail t526, t525_product_request t525
					      , t7103_schedule_info t7103
					    WHERE t525.c525_product_request_id = t526.c525_product_request_id
					        AND t526.c526_product_request_detail_id = t7104.c526_product_request_detail_id
					        AND t525.c525_product_request_id = TO_NUMBER(v_ref_id)
					        AND t7103.c7100_case_info_id = t7104.c7100_case_info_id
					        AND t525.c525_void_fl IS NULL
					        AND t7104.c7104_void_fl IS NULL
					        AND t526.c526_void_fl IS NULL
					        AND ROWNUM = 1;
				EXCEPTION WHEN NO_DATA_FOUND THEN
					v_loan_to := NULL;
				END;	        
			END IF;
			
			IF v_Id = '01' THEN
				--SELECT C704_ACCOUNT_NM INTO v_bill_add FROM T704_ACCOUNT WHERE C704_ACCOUNT_ID = '01';
				SELECT DECODE(v_type,9110, v_loan_to, GET_USER_NAME(C504_SHIP_TO_ID)) 
				INTO v_bill_add 
				FROM T504_CONSIGNMENT 
				WHERE C504_CONSIGNMENT_ID = p_consign_id;
				
	   		ELSIF v_Id <> '01' AND v_Id IS NOT NULL AND v_dist_id IS NULL THEN				
		     SELECT t704.c704_account_nm ||'<BR>&nbsp;'|| t704.c704_bill_add1 ||'<BR>&nbsp;'|| t704.c704_bill_add2 ||'<BR>&nbsp;' || 
               DECODE(NVL(GET_RULE_VALUE('SWAP_ZIP_CODE','SWAP_ZIP_CODE'),'N'),'Y',t704.c704_bill_zip_code,t704.C704_BILL_CITY) 
                ||',&nbsp;'|| GET_CODE_NAME_ALT(t704.c704_BILL_STATE) || '&nbsp;' ||
                DECODE(NVL(GET_RULE_VALUE('SWAP_ZIP_CODE','SWAP_ZIP_CODE'),'N'),'Y',t704.c704_bill_city,t704.c704_bill_zip_code)
               INTO v_bill_add
               FROM t504_consignment t504, t704_account t704
              WHERE t504.c704_account_id = t704.c704_account_id
                AND t504.c504_consignment_id = p_consign_id
                AND t504.c504_void_fl is null
                AND t704.c704_void_fl is null
                AND (t704.c901_ext_country_id IS NULL OR t704.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes));	
            ELSIF v_ship_to = '26240419' THEN -- ship to (26240419) is plant then get plant address
        --getting company id for the plant id
		      SELECT C1900_PARENT_COMPANY_ID
		      	INTO v_plant_comp_id
		       FROM T5040_PLANT_MASTER 
		       WHERE C5040_PLANT_ID = v_ship_to_id AND C5040_VOID_FL IS NULL AND C5040_ACTIVE_FL= 'Y';
		--getting the Company name   
		      SELECT GET_RULE_VALUE_BY_COMPANY('COMP_NAME','COMP_DETAILS',v_plant_comp_id)
		        INTO v_comp_name
		        FROM DUAL;
		       
  
       			SELECT	'&'||'nbsp;'
       			||DECODE (v_comp_name,'','',v_comp_name||'<BR>&'||'nbsp;')
         		|| C5040_PLANT_NAME
	                || '<BR>&'||'nbsp;'
	                || DECODE (C5040_ADDRESS_1,NULL,'',C5040_ADDRESS_1||'<BR>&'||'nbsp;')
	                || DECODE (C5040_ADDRESS_2,NULL,'',C5040_ADDRESS_2||'<BR>&'||'nbsp;')
	                 || DECODE (NVL (get_rule_value ('SWAP_ZIP_CODE',
	                                                'SWAP_ZIP_CODE'
	                                               ),
	                                'N'
	                               ),
	                           'Y', C5040_ZIPCODE||'<BR>&'||'nbsp;',
	                           C5040_CITY||'<BR>&'||'nbsp;'
	                          )
	                
	                || DECODE (C901_STATE_ID,NULL,'',get_code_name_alt (C901_STATE_ID)||'<BR>&'||'nbsp;') 
	             
	                || DECODE (NVL (get_rule_value ('SWAP_ZIP_CODE',
	                                                'SWAP_ZIP_CODE'
	                                               ),
	                                'N'
	                               ),
	                           'Y', C5040_CITY||'<BR>&'||'nbsp;',
	                           C5040_ZIPCODE||'<BR>&'||'nbsp;'
	                          )
	                
	
	           INTO v_bill_add
	           FROM T5040_PLANT_MASTER 
	          WHERE C5040_PLANT_ID = v_ship_to_id AND C5040_VOID_FL IS NULL AND C5040_ACTIVE_FL= 'Y';
			
          ELSE
			   SELECT A.C701_DISTRIBUTOR_NAME ||'<BR>&nbsp;'|| A.C701_BILL_ADD1 ||'<BR>&nbsp;'|| a.C701_BILL_ADD2 ||'<BR>&nbsp;'||
			   DECODE(NVL(GET_RULE_VALUE('SWAP_ZIP_CODE','SWAP_ZIP_CODE'),'N'),'Y',A.C701_BILL_ZIP_CODE,A.C701_BILL_CITY) 
			   ||',&nbsp;'|| GET_CODE_NAME_ALT(A.C701_BILL_STATE) || '&nbsp;'|| 
			   DECODE(NVL(GET_RULE_VALUE('SWAP_ZIP_CODE','SWAP_ZIP_CODE'),'N'),'Y',A.C701_BILL_CITY,A.C701_BILL_ZIP_CODE)
			   INTO v_bill_add
			   FROM T701_DISTRIBUTOR A,  T504_CONSIGNMENT B
			   WHERE B.C504_CONSIGNMENT_ID = p_consign_id AND A.C701_DISTRIBUTOR_ID = B.C701_DISTRIBUTOR_ID 
			   AND (A.c901_ext_country_id IS NULL OR A.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes));-- To include ext country id for GH

			END IF;
   EXCEPTION
      WHEN NO_DATA_FOUND
   THEN
      RETURN ' ';
      END;
     RETURN v_bill_add;
END GET_CONSIGN_BILL_ADD;
/

