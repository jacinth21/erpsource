CREATE OR REPLACE FUNCTION get_cs_loaner_recon_qty
(
 p_transid t413_inhouse_trans_items.c412_inhouse_trans_id%TYPE,
 p_pnum  t413_inhouse_trans_items.c205_part_number_id%TYPE
)
RETURN NUMBEr
IS
/***************************************************************************************
 * Description           : This function returns the Reconciled quantity for a part
 *
 ****************************************************************************************/
v_qty NUMBER;
BEGIN
--
  IF p_pnum IS NOT NULL THEN
	  SELECT	NVL(sum(C413_ITEM_QTY),0) INTO v_qty
	  FROM T413_INHOUSE_TRANS_ITEMS 
	  WHERE C412_INHOUSE_TRANS_ID = p_transid
	  AND C413_STATUS_FL <> 'Q'
	  AND C205_PART_NUMBER_ID = p_pnum
	  AND C413_VOID_FL IS NULL;
  ELSE
	  SELECT	NVL(sum(C413_ITEM_QTY),0) INTO v_qty
	  FROM T413_INHOUSE_TRANS_ITEMS 
	  WHERE C412_INHOUSE_TRANS_ID = p_transid
	  AND C413_STATUS_FL <> 'Q'
	  AND C413_VOID_FL IS NULL;
  END IF;
--
 RETURN v_qty;
 EXCEPTION
  WHEN NO_DATA_FOUND THEN
  RETURN 0;
END get_cs_loaner_recon_qty;
/