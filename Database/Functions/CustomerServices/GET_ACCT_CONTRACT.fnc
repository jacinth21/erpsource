/*	Description 	: THIS FUNCTION RETURNS contract Value for the Account
 Parameters 		: p_acc_id
*/
CREATE OR REPLACE FUNCTION get_acct_contract(
	p_acc_id   t704_account.c704_account_id%TYPE
)
	RETURN NUMBER
IS
	v_contract	 T704d_Account_Affln.c901_contract%TYPE;   
BEGIN
	BEGIN
		
		SELECT c901_contract
	     INTO v_contract 
		  FROM T704d_Account_Affln
		 WHERE c704_account_id = p_acc_id AND c704d_void_fl IS NULL;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN NULL;
	END;

	RETURN v_contract;
END get_acct_contract;
/