/* Formatted on 2009/07/29 18:36 (Formatter Plus v4.8.0) */
--@"C:\Database\Functions\CustomerServices\GET_ACCOUNT_GPO_NAME.fnc"
CREATE OR REPLACE
    FUNCTION get_account_gpo_name (
            p_gpo_id t704_account.c704_account_id%TYPE)
        RETURN VARCHAR
    IS
        v_gpo_name VARCHAR (50) ;
    BEGIN
        BEGIN
             SELECT DECODE (c101_last_nm||c101_first_nm, NULL, C101_PARTY_NM, c101_last_nm || ' ' || c101_first_nm)
               INTO v_gpo_name
               FROM t101_party
              WHERE c901_party_type = 7003
                AND c101_party_id   = p_gpo_id
                AND c101_delete_fl IS NULL;
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            RETURN '';
        END;
        RETURN v_gpo_name;
    END get_account_gpo_name;
    /
