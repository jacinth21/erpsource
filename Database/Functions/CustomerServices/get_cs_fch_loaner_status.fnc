CREATE OR REPLACE FUNCTION get_cs_fch_loaner_status
(	p_part_num T505_ITEM_CONSIGNMENT.c205_part_number_id%TYPE
   ,p_rep_id T703_SALES_REP.c703_sales_rep_id%TYPE
)
RETURN VARCHAR2
IS
/*  Description     : THIS FUNCTION RETURNS CONSIGNMENT LOANER ID if the Rep has the part
 Parameters 		: p_part_num, p_rep_id
*/
v_dist_id  T701_DISTRIBUTOR.c701_distributor_id%TYPE;
v_consign_cnt VARCHAR2(3);

BEGIN
     --
		SELECT 'Y' 
		INTO v_consign_cnt
		FROM T504_CONSIGNMENT t504, T505_ITEM_CONSIGNMENT t505, T504A_CONSIGNMENT_LOANER t504a
		WHERE t504.c504_consignment_id = t505.c504_consignment_id
		AND t504.c504_consignment_id = t504a.c504_consignment_id
		AND t504.c701_distributor_id IN  
			( SELECT C701_DISTRIBUTOR_ID 
			  FROM T703_SALES_REP t703 
			  WHERE t703.c703_sales_rep_id = p_rep_id)
		AND t505.c205_part_number_id = p_part_num
		AND t504.c504_void_fl IS NULL
		AND t505.c505_void_fl IS NULL
		AND ROWNUM = 1;
	  --
		RETURN v_consign_cnt;
      --   
   EXCEPTION
      WHEN NO_DATA_FOUND
   THEN
      RETURN 'N';
     
END get_cs_fch_loaner_status;
/


