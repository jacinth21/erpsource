CREATE OR REPLACE FUNCTION GET_COUNT_SUM
(
	p_ord_id	t502_item_order.c501_order_id%TYPE
)
RETURN NUMBER
/*  Description     : THIS FUNCTION RETURNS COUNT OF PART ORDERED FOR THE SELECTED
 *       ORDER
 *       EXCEPTION NOT REQUIRED BECAUSE SUM DOES NOT THROW NO DATA FOUND
 *       ERROR
 * Parameters   : p_ord_id ORDER ID
*/
IS
--
v_acc_nm NUMBER;
--
BEGIN
 --
	SELECT	NVL(SUM(c502_item_qty),0)
	INTO	v_acc_nm
	FROM	t502_item_order
	WHERE	c501_order_id = p_ord_id
	AND	C901_TYPE = 50300
	AND	c502_void_fl IS NULL
	AND	c502_delete_fl IS NULL;
 --
	RETURN v_acc_nm;
 --
END GET_COUNT_SUM;
/