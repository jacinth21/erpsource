/* Formatted on 2009/07/15 17:35 (Formatter Plus v4.8.0) */
CREATE OR REPLACE FUNCTION get_set_partnum_qty_return (
	p_ra_id 	 t506_returns.c506_rma_id%TYPE
  , p_part_num	 t507_returns_item.c205_part_number_id%TYPE
)
	RETURN NUMBER
IS
	/*	Description 	: THIS FUNCTION RETURNS CODE NAME GIVEN THE CODE_ID
	 Parameters 		: p_code_id
	*/
	v_qty		   t208_set_details.c208_set_qty%TYPE;
	v_fl		   t208_set_details.C207_SET_ID%TYPE;
BEGIN
	SELECT c207_set_id
	  INTO v_fl
	  FROM t506_returns
	 WHERE c506_rma_id = p_ra_id;

	IF v_fl IS NOT NULL
	THEN
		SELECT	 a.c208_set_qty qty
			INTO v_qty
			FROM t208_set_details a, t507_returns_item b, t207_set_master c, t506_returns d
		   WHERE b.c506_rma_id = p_ra_id
			 AND b.c506_rma_id = d.c506_rma_id
			 AND a.c207_set_id = c.c207_set_id
			 AND d.c207_set_id = c.c207_set_id
			 AND a.c205_part_number_id = b.c205_part_number_id
			 AND b.c205_part_number_id = p_part_num
			 AND d.c506_void_fl IS NULL
			 AND b.c507_status_fl <> 'Q'
			 AND a.c208_void_fl IS NULL
		GROUP BY a.c205_part_number_id, a.c208_set_qty;
	ELSE
		SELECT SUM (c507_item_qty)
		  INTO v_qty
		  FROM t507_returns_item
		 WHERE c205_part_number_id = p_part_num AND c506_rma_id = p_ra_id AND c507_status_fl <> 'Q';
	END IF;

	--
	RETURN v_qty;
EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		RETURN 0;
END get_set_partnum_qty_return;
/
