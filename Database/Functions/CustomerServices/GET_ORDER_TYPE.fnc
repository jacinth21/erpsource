CREATE OR REPLACE FUNCTION GET_ORDER_TYPE
(p_order_id T501_ORDER.C501_ORDER_ID%TYPE)
RETURN NUMBER
IS
/*  Description     : THIS FUNCTION RETURNS ORDER TYPE GIVEN THE ORDER_ID
 Parameters 		: p_order_id
*/
v_order_type   T501_ORDER.c901_order_type%TYPE;
BEGIN
     BEGIN
       SELECT  c901_order_type OrderType
	   INTO v_order_type
	   FROM  T501_ORDER
       WHERE C501_ORDER_ID = p_order_id
       AND C501_void_fl IS NULL;
   EXCEPTION
      WHEN NO_DATA_FOUND
   THEN
      RETURN NULL;
      END;
     RETURN v_order_type;
END;
/