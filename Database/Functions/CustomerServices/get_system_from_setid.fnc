--@"C:\Database\Functions\CustomerServices\get_system_from_setid.fnc";
CREATE OR REPLACE FUNCTION get_system_from_setid(
	p_set_id   t207_set_master.c207_set_id%TYPE
)
RETURN VARCHAR2
IS
v_sys_id	   t207_set_master.c207_set_id%TYPE;
BEGIN
	BEGIN
		SELECT  c207_set_id 
		INTO  v_sys_id
		FROM V207A_SET_CONSIGN_LINK
		WHERE c207_actual_set_id =  p_set_id
		AND ROWNUM = 1;
	EXCEPTION
	WHEN NO_DATA_FOUND THEN
	v_sys_id := '';
	END;
	RETURN v_sys_id;
END get_system_from_setid;
/