
/****************************************************************************************************
 * Description : This function returns the sum of the quantity of all the BBA order for the ACK order
 *****************************************************************************************************/
CREATE OR REPLACE FUNCTION get_order_partqty (
   p_order_id	t501_order.c501_order_id%TYPE
    ,p_partnum 	T205_part_number.c205_part_number_id%TYPE
)
	RETURN NUMBER
IS
	v_orderedqty NUMBER := 0;
	 v_ack_order_id    VARCHAR2(20);
BEGIN
	
	BEGIN
	 SELECT C501_Ref_Id
	   INTO v_ack_order_id
	   FROM t501_order
	  WHERE c501_order_id = p_order_id
	    AND c501_void_fl  IS NULL;
	EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		v_ack_order_id := '';
	END;	
	
	BEGIN
		 SELECT NVL(SUM (t502.C502_item_qty),0) INTO v_orderedqty
		   FROM t502_item_order t502, t501_order t501
		  WHERE t502.c501_order_id       = t501.c501_order_id
		    AND t502.c501_order_id      in ( SELECT c501_order_id FROM t501_order  WHERE C501_Ref_Id   = v_ack_order_id    AND c501_void_fl IS NULL)
		    AND t502.c205_part_number_id = p_partnum
		    AND t501.c501_void_fl       IS NULL
		    AND t502.c502_void_fl       IS NULL;		
		
	EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		v_orderedqty := 0;
	END;
RETURN nvl(v_orderedqty,0);
END get_order_partqty;
/