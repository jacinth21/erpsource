/* Formatted on 2009/07/29 18:36 (Formatter Plus v4.8.0) */
--@"C:\Database\Functions\CustomerServices\GET_CUST_PO_ID.fnc";
CREATE OR REPLACE FUNCTION get_cust_po_id (
	p_orderid   t501_order.c501_order_id%TYPE
)
	RETURN VARCHAR2
IS
/*	Description 	: THIS FUNCTION RETURNS CUSTOMER PO ID for the ORDER AND USED FOR FEDEXLABEL.
 Parameters 		: p_orderid
*/
	v_cust_po_id	   t501_order.c501_customer_po%TYPE;
BEGIN
	BEGIN
		SELECT c501_customer_po
		  INTO v_cust_po_id
		  FROM t501_order
		 WHERE c501_order_id = p_orderid AND c501_void_fl IS NULL;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN '';
	END;

	RETURN v_cust_po_id;
END get_cust_po_id;
/
