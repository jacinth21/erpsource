
/*************************************************************************
 * Description : This function returns the sum of order qty
 *************************************************************************/

CREATE OR REPLACE FUNCTION get_shipped_qty (
	p_partnum 	T205_part_number.c205_part_number_id%TYPE
 , p_order_id	t501_order.c501_order_id%TYPE
 , p_cons_fl		T502_item_order.C502_construct_fl%TYPE DEFAULT NULL
)
	RETURN NUMBER
IS
	v_shipqty NUMBER := 0;
BEGIN
	BEGIN
		SELECT NVL(SUM (t502.C502_item_qty),0) INTO v_shipqty
		   FROM t501_order t501, t502_item_order t502
		  WHERE t502.c205_part_number_id = p_partnum
		  	 AND NVL(t502.c502_construct_fl,'N') = NVL(p_cons_fl,'N') 
		    AND t502.c501_order_id       = t501.c501_order_id
		--    AND t501.C501_status_fl <3 
		    AND t501.C501_ref_id         = p_order_id
		    AND t501.c501_void_fl       IS NULL
		    AND t502.c502_void_fl       IS NULL;
	EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		v_shipqty := 0;
	END;
RETURN nvl(v_shipqty,0);
END get_shipped_qty;
/