/* Formatted on 2010/12/21 15:25 (Formatter Plus v4.8.0) */
--@"C:\DATABASE\Functions\CustomerServices\GET_CS_PENDSHIP_QTY.fnc";

CREATE OR REPLACE FUNCTION get_cs_pendship_qty (
	p_transid	t412_inhouse_transactions.c412_ref_id%TYPE
  , p_pnum		t413_inhouse_trans_items.c205_part_number_id%TYPE
  , p_id		t413_inhouse_trans_items.c413_ref_id%TYPE DEFAULT NULL
)
	RETURN NUMBER
IS
/***************************************************************************************
 * Description			 : This function returns the Reconciled quantity for a part
 *
 ****************************************************************************************/
	v_qty		   NUMBER;
BEGIN
--
	SELECT NVL (SUM (c413_item_qty), 0)
	  INTO v_qty
	  FROM t413_inhouse_trans_items t413, t412_inhouse_transactions t412
	 WHERE t413.c412_inhouse_trans_id = t412.c412_inhouse_trans_id
	   AND c205_part_number_id = p_pnum
	   AND t412.c412_ref_id = p_transid 
	   AND NVL(c413_ref_id,'-999') = NVL(p_id, NVL(c413_ref_id,'-999'))
	   AND t412.c412_status_fl < 3
	   AND t412.c412_void_fl IS NULL;


--
	RETURN v_qty;
EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		RETURN 0;
END get_cs_pendship_qty;
/