CREATE OR REPLACE FUNCTION get_count_dist_consign_part (
	p_id	 t504_consignment.c701_distributor_id%TYPE
  , p_pnum	 t505_item_consignment.c205_part_number_id%TYPE
)
	RETURN VARCHAR2
IS
/*	Description 	: THIS FUNCTION RETURNS CODE NAME GIVEN THE CODE_ID
 Parameters 		: p_code_id
*/
	v_cnt	NUMBER;
--
BEGIN
	SELECT	 SUM (t505.c505_item_qty)
		INTO v_cnt
		FROM t504_consignment t504, t505_item_consignment t505
	   WHERE t504.c504_consignment_id = t505.c504_consignment_id
		 AND t504.c701_distributor_id = p_id
		 AND TRIM (t505.c505_control_number) IS NOT NULL
		 AND t504.c207_set_id IS NULL
		 AND t505.c205_part_number_id = p_pnum
		 AND t504.c504_void_fl IS NULL
		 AND t504.c504_type <> 4129
	GROUP BY t505.c205_part_number_id;

	RETURN v_cnt;
END get_count_dist_consign_part;
/
