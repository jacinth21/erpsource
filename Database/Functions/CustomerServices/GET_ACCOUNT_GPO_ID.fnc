/* Formatted on 2009/07/29 18:36 (Formatter Plus v4.8.0) */
CREATE OR REPLACE FUNCTION get_account_gpo_id (
	p_acc_id   t704_account.c704_account_id%TYPE
)
	RETURN NUMBER
IS
/*	Description 	: THIS FUNCTION RETURNS CGPO ID ofor the Account
 Parameters 		: p_code_id
*/
	v_gpo_id	   t740_gpo_account_mapping.c101_party_id%TYPE;
BEGIN
	BEGIN
		SELECT c101_party_id
		  INTO v_gpo_id
		  FROM t740_gpo_account_mapping
		 WHERE c704_account_id = p_acc_id AND c740_void_fl IS NULL;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN 0;
	END;

	RETURN v_gpo_id;
END get_account_gpo_id;
/
