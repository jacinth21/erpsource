--@"C:\Database\Functions\CustomerServices\get_order_part_qty.fnc";
/****** Description : This function returns sum of the order quantity ***********/

CREATE OR REPLACE 
FUNCTION get_order_part_qty (
     p_order_id	t501_order.c501_order_id%TYPE,
	 p_partnum 	T205_part_number.c205_part_number_id%TYPE
)
	RETURN NUMBER
IS
	v_part_qty NUMBER := 0;
BEGIN
	BEGIN
		
	select sum(t502.c502_item_qty) into v_part_qty from t501_order t501,t502_item_order t502 where 
		t502.c205_part_number_id = p_partnum and 
		t501.c501_order_id = t502.c501_order_id
		and (t501.c501_order_id=p_order_id or t501.c501_parent_order_id=p_order_id) and 
		  NVL(t501.c901_order_type,-999) NOT IN (SELECT t906.c906_rule_value FROM t906_rules t906 
	     WHERE t906.c906_rule_grp_id = 'ORDTYPELOT'
	   AND c906_rule_id = 'EXCLUDE') AND
	    t502.c502_void_fl is null
		and t501.c501_void_fl IS NULL;
	EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		v_part_qty := 0;
	END;
RETURN (v_part_qty);
END get_order_part_qty;
/
