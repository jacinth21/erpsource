/* Formatted on 2009/02/23 14:33 (Formatter Plus v4.8.0) */


CREATE OR REPLACE FUNCTION get_cs_fch_loaner_etchid (
	p_consign_id   t504a_consignment_loaner.c504_consignment_id%TYPE
)
	RETURN VARCHAR2
IS
/*	Description 	: THIS FUNCTION RETURNS ETCH_ID GIVEN THE CONSIGN ID
 Parameters 		: p_consign_id
*/
	v_etch_id	   VARCHAR2 (20);
BEGIN
	BEGIN
		SELECT c504a_etch_id
		  INTO v_etch_id
		  FROM t504a_consignment_loaner
		 WHERE c504_consignment_id = p_consign_id;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN ' ';
	END;

	RETURN v_etch_id;
END get_cs_fch_loaner_etchid;
/
