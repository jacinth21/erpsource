/* Formatted on 2011/10/26 16:20 (Formatter Plus v4.8.0) */
--@"C:\Database\Functions\CustomerServices\get_dealer_payment_terms.fnc";
--This function is used to get Payment term of dealer .
CREATE OR REPLACE FUNCTION get_dealer_payment_terms (
p_dealer_id IN t101_party_invoice_details.C101_PARTY_ID%TYPE
)
	RETURN NUMBER
IS
v_paymentterms NUMBER;
BEGIN
      SELECT C901_PAYMENT_TERMS 
        INTO v_paymentterms 
        FROM t101_party_invoice_details
       WHERE c101_party_id = p_dealer_id
         AND C101_VOID_FL IS NULL;

  RETURN v_paymentterms;
EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		RETURN NULL;
END get_dealer_payment_terms;
/