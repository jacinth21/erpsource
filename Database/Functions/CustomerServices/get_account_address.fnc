/* Formatted on 2011/10/26 16:20 (Formatter Plus v4.8.0) */
--@"C:\Database\Functions\CustomerServices\get_account_address.fnc";

CREATE OR REPLACE FUNCTION get_account_address (
	p_account_id   IN   t704_account.C704_ACCOUNT_ID%TYPE
)
	RETURN VARCHAR2
IS
	v_billadd      VARCHAR2(1000);
BEGIN
	
	SELECT C704_BILL_ZIP_CODE
                   || '<BR>&'||'nbsp;'
                   || get_code_name(C704_BILL_STATE) || C704_BILL_CITY
                   || DECODE(C704_BILL_ADD1,NULL,'',C704_BILL_ADD1 || '<BR>&'||'nbsp;')                        
                   || DECODE(C704_BILL_ADD2,NULL,'',C704_BILL_ADD2)
              INTO  v_billadd                                                             
              FROM t704_account 
             WHERE C704_ACCOUNT_ID 	= p_account_id
               AND C704_VOID_FL IS NULL;
               
  RETURN v_billadd;
EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		RETURN NULL;
END get_account_address;
/