--@"C:\Database\Functions\CustomerServices\get_parent_order_id.fnc";

/*  Description     : This function return the parent order id
*       Exception when no data found - then return empty values
* Parameters   : p_ord_id
*/
CREATE OR REPLACE FUNCTION get_parent_order_id (
        p_order_id IN t501_order.c501_order_id%TYPE)
    RETURN VARCHAR2
IS
    v_parent_order_id VARCHAR2 (40) ;
BEGIN
     SELECT c501_order_id
       INTO v_parent_order_id
       FROM t501_order t501
      WHERE C501_void_fl        IS NULL
        AND connect_by_isleaf    = 1
        START WITH c501_order_id =
        (
             SELECT c501_order_id
               FROM t501_order t501
              WHERE c501_order_id = p_order_id
                AND C501_void_fl IS NULL
        )
        CONNECT BY NOCYCLE PRIOR c501_parent_order_id = c501_order_id;
    RETURN v_parent_order_id;
EXCEPTION
WHEN NO_DATA_FOUND THEN
    RETURN NULL;
END get_parent_order_id;
/
