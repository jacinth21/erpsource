CREATE OR REPLACE FUNCTION "GET_NEXT_ORDER_ID"
( p_OrderId	VARCHAR2,
  p_ordpref VARCHAR2 DEFAULT ''
)
RETURN VARCHAR2
IS
/* Description : THIS FUNCTION RETURNS CODE NAME GIVEN THE CODE_ID
 Parameters 		: p_code_id
*/
v_ord_id 		NUMBER;
v_string 		VARCHAR2(20);
v_id_string 	VARCHAR2(20);
v_OrderId 		VARCHAR2(20);
v_Cnt	 		NUMBER;
v_prefix 		VARCHAR2(20) := '';
v_company_id 	t1900_company.C1900_COMPANY_ID%TYPE;
v_def_prefix	VARCHAR2(20) := '';
BEGIN
	SELECT NVL(get_compid_frm_cntx(), get_rule_value('DEFAULT_COMPANY','ONEPORTAL')) 
      INTO v_company_id
      FROM dual;
 BEGIN
	 IF p_OrderId IS NOT NULL
	 	THEN
			SELECT COUNT(*) INTO v_Cnt FROM T501_ORDER WHERE C501_ORDER_ID = p_OrderId;
			IF v_Cnt > 0 THEN
			 	 v_OrderId := 'DUPLICATE';
			ELSE
				v_OrderId := p_OrderId;
			END IF;
	 ELSE
	 	-- Get the default order prefix if rule entry is not available
	 	SELECT DECODE(p_ordpref,'ACKORD','ACK-','GM') 
	 		INTO v_def_prefix 
	 	FROM DUAL;
	 
	 	 SELECT S501_ORDER.NEXTVAL INTO v_ord_id FROM dual;
			 IF LENGTH(v_ord_id) = 1
			 	 THEN
				 v_id_string := '0' || v_ord_id;
				 ELSE
				 v_id_string := v_ord_id;
			 END IF;
			 v_prefix := NVL(get_rule_value_by_company(p_ordpref,'ORDPREFIX',v_company_id),v_def_prefix);
			 SELECT v_prefix || v_id_string INTO v_string FROM dual;
		 	 v_OrderId := v_string;
	 END IF;
 EXCEPTION
 WHEN NO_DATA_FOUND
 THEN
 RETURN ' ';
 END;
 RETURN v_OrderId;
END GET_NEXT_ORDER_ID;
/

