CREATE OR REPLACE FUNCTION GET_CS_SHIP_NAME
(
 p_ship_to  NUMBER
 ,p_ship_to_id VARCHAR2
)
RETURN VARCHAR2
IS
/***************************************************************************************
 * Description           : This function returns the name of the person to whom the set
       is shipped to.
 *
 ****************************************************************************************/
v_ship_to VARCHAR2(255);
BEGIN
--
 IF p_ship_to = 4120 THEN
  v_ship_to := GET_DISTRIBUTOR_NAME(p_ship_to_id);
 ELSIF p_ship_to = 4121 THEN
  v_ship_to := GET_REP_NAME(p_ship_to_id);
 ELSIF p_ship_to = 4122 THEN
  v_ship_to := GET_ACCOUNT_NAME(p_ship_to_id);
 ELSIF p_ship_to = 4123 THEN
  v_ship_to := GET_USER_NAME(p_ship_to_id);
 ELSIF p_ship_to = 4000642 THEN --4000642: Shipping account
  v_ship_to := get_party_name(p_ship_to_id);
  ELSIF p_ship_to = 26240419 THEN --26240419: Plant
  v_ship_to := get_plant_name(p_ship_to_id);
 END IF;
 RETURN v_ship_to;
 EXCEPTION
  WHEN NO_DATA_FOUND THEN
  RETURN ' ';
END GET_CS_SHIP_NAME;
/

