CREATE OR REPLACE FUNCTION GET_BILL_ADD
(p_acc_id T704_ACCOUNT.C704_ACCOUNT_ID%TYPE)
RETURN VARCHAR2
IS
/*  Description     : THIS FUNCTION RETURNS CODE NAME GIVEN THE CODE_ID
 Parameters 		: p_code_id
*/
v_ship_add VARCHAR2(1000);
v_comp_id T1900_COMPANY.C1900_COMPANY_ID%TYPE;
v_delimiter VARCHAR2(10);
v_zip_code_fl VARCHAR2(10);
BEGIN
	SELECT  NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL')) INTO v_comp_id FROM DUAL;
	-- This rule will get the Delimiter that needs to be shown in the Zip Code Section. For Country Like Germany, they do not need a delimiter.
	SELECT NVL(get_rule_value_by_company('DELIMITER','ZIP_CODE_DELIM', v_comp_id),',')
	, NVL(get_rule_value_by_company('SWAP_ZIP_CODE','SWAP_ZIP_CODE', v_comp_id),'N')
	INTO v_delimiter, v_zip_code_fl FROM DUAL;
	
     BEGIN
	   		SELECT  C704_BILL_NAME ||'<BR>&nbsp;'|| C704_BILL_ADD1 ||'<BR>&nbsp;'|| C704_BILL_ADD2 ||'<BR>&nbsp;' || 
	   		DECODE(v_zip_code_fl ,'Y',C704_BILL_ZIP_CODE,C704_BILL_CITY)
			||v_delimiter||'&nbsp;'|| DECODE(C704_BILL_STATE,'1057','',GET_CODE_NAME_ALT(C704_BILL_STATE)) ||'&nbsp;'|| 
			DECODE(v_zip_code_fl ,'Y',C704_BILL_CITY,C704_BILL_ZIP_CODE)
			|| DECODE(c704_bill_country,1101,'','<BR>&nbsp;' || get_code_name(c704_bill_country))
			--When Bill Country is US,it is not required to show in the paperwork.
			INTO v_ship_add
			FROM  T704_ACCOUNT
			WHERE  C704_ACCOUNT_ID = p_acc_id;
   EXCEPTION
      WHEN NO_DATA_FOUND
   THEN
      RETURN ' ';
      END;
     RETURN v_ship_add;
END GET_BILL_ADD;
/

