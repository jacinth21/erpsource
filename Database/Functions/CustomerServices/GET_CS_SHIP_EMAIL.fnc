/* Formatted on 2009/08/31 14:48 (Formatter Plus v4.8.0) */
--@"c:\database\Functions\Customerservices\get_cs_ship_email.fnc";


CREATE OR REPLACE FUNCTION get_cs_ship_email (
	p_ship_to	   NUMBER
  , p_ship_to_id   VARCHAR2
)
	RETURN VARCHAR2
IS
/***************************************************************************************
 * Description			 : This function returns the email of the person to whom the set
		is shipped to.
 *
 ****************************************************************************************/
	v_ship_to	   CLOB;
	v_rep_id	   VARCHAR2 (20);
	v_party_id	   VARCHAR2 (20);
	v_defaultemail VARCHAR2 (100);
BEGIN
--
	SELECT get_rule_value ('DEFAULTSHIPEMAIL', 'EMAIL')
	  INTO v_defaultemail
	  FROM DUAL;

	IF p_ship_to = 4120
	THEN
		v_ship_to	:= get_distributor_email (p_ship_to_id);
	ELSIF p_ship_to = 4121
	THEN
		v_party_id	:= TRIM (get_rep_party_id (p_ship_to_id));
		v_ship_to	:= TRIM (gm_pkg_cm_contact.get_contact_value (v_party_id, 90452));
	ELSIF p_ship_to = 4122
	THEN
		v_rep_id	:= TRIM (get_rep_id (p_ship_to_id));
		v_party_id	:= TRIM (get_rep_party_id (v_rep_id));
		v_ship_to	:= TRIM (gm_pkg_cm_contact.get_contact_value (v_party_id, 90452));
	ELSIF p_ship_to = 4123
	THEN
		v_ship_to	:= get_user_emailid (p_ship_to_id);
	END IF;

	IF v_ship_to IS NULL
	THEN
		v_ship_to	:= v_defaultemail;
	END IF;

	RETURN v_ship_to;
EXCEPTION
	WHEN OTHERS
	THEN
		RETURN v_defaultemail;
END get_cs_ship_email;
/
