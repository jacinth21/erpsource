CREATE OR REPLACE FUNCTION GET_MFG_PARTNUM_INV_QTY
(
	p_part_num	T205_PART_NUMBER.c205_part_number_id%TYPE)
RETURN NUMBER
IS
/***************************************************************************************
 * Description           :	This function returns the available qty for a mfg part
 *						 
 ****************************************************************************************/
v_qty		NUMBER;
v_alloc_qty	NUMBER;
v_alloc_inhouse_qty NUMBER;
v_plant_id     T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
--
BEGIN
	SELECT  get_plantid_frm_cntx() INTO v_plant_id FROM DUAL;
	SELECT	Get_Partnumber_Qty(p_part_num,90802)
	INTO	v_qty
	FROM	T205_PART_NUMBER
	WHERE	c205_part_number_id = p_part_num;
--
	SELECT	NVL(SUM(t304.c304_item_qty),0)
	INTO	v_alloc_qty
	FROM	T303_MATERIAL_REQUEST t303, T304_MATERIAL_REQUEST_ITEM t304, t408_dhr t408
	WHERE	t304.c205_part_number_id = p_part_num
	AND		t303.c303_material_request_id = t304.c303_material_request_id
	AND		t303.c303_status_fl < 40 --2
	AND 	t303.c303_void_fl IS NULL
	AND     t408.c408_void_fl IS NULL
    AND     t303.c408_dhr_id  = t408.c408_dhr_id 
	AND     t408.c5040_plant_id = v_plant_id ;
  
  	--to calculate the Inv qty of the part , need minus all allco qty in inhouse transaction , should be the same as get_qty_in_transaction_rm
  	SELECT NVL (SUM (t413.c413_item_qty), 0)
	  INTO v_alloc_inhouse_qty
	  FROM t412_inhouse_transactions t412, t413_inhouse_trans_items t413
	 WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
	   AND t412.c412_verified_date IS NULL
	   AND t412.c412_void_fl IS NULL
	   AND t413.c205_part_number_id = p_part_num
	   AND t412.C5040_PLANT_ID = v_plant_id 
	   AND t412.c412_type IN (40051, 40052,40053);
  
--
	RETURN v_qty - v_alloc_qty - v_alloc_inhouse_qty;
--
EXCEPTION
WHEN NO_DATA_FOUND THEN
	RETURN 0;
END GET_MFG_PARTNUM_INV_QTY;
/