CREATE OR REPLACE FUNCTION Get_Mfg_Qty_To_Mfg
(
	p_mfgwo_id	T408_DHR.C408_DHR_ID%TYPE
)	
RETURN VARCHAR2
IS
/***************************************************************************************
 * Description           :	This function returns the quantity to be manufactured for a Manufacturing Work Order ID
 *						 
 ****************************************************************************************/
v_qty  T408_DHR.C408_QTY_RECEIVED%TYPE;
BEGIN
     BEGIN
		 SELECT	C408_QTY_RECEIVED INTO v_qty
		 FROM	T408_DHR
		 WHERE	C408_DHR_ID  = p_mfgwo_id;

   EXCEPTION
      WHEN NO_DATA_FOUND
   THEN
      RETURN ' ';
      END;
     RETURN v_qty;
END Get_Mfg_Qty_To_Mfg;
/
