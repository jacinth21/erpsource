/* Formatted on 2008/10/19 16:57 (Formatter Plus v4.8.0) */
--@"c:\database\Functions\Manufacturing\get_qty_in_transaction_rm.fnc";

CREATE OR REPLACE FUNCTION get_qty_in_transaction_rm (
	p_part_num	 t205_part_number.c205_part_number_id%TYPE
)
	RETURN NUMBER
IS
/***************************************************************************************
 * Description	 :	This function returns the available qty for a mfg part
 *
 ****************************************************************************************/
	v_alloc_qty    NUMBER;
	v_alloc_inhouse_qty NUMBER;
--
BEGIN
	SELECT NVL (SUM (t304.c304_item_qty), 0)
	  INTO v_alloc_qty
	  FROM t303_material_request t303, t304_material_request_item t304
	 WHERE t304.c205_part_number_id = p_part_num
	   AND t303.c303_material_request_id = t304.c303_material_request_id
	   AND t303.c303_status_fl < 40	--2
	   AND t303.c303_void_fl IS NULL; 

	   SELECT NVL (SUM (t413.c413_item_qty), 0)
	  INTO v_alloc_inhouse_qty
	  FROM t412_inhouse_transactions t412, t413_inhouse_trans_items t413
	 WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
	   AND t412.c412_verified_date IS NULL
	   AND t412.c412_void_fl IS NULL
	   AND t413.c205_part_number_id = p_part_num
	   AND t412.c412_type IN (40051, 40052,40053, 56025); -- 56025-RMRW

	
	RETURN (v_alloc_qty + v_alloc_inhouse_qty);
--
EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
RETURN 0;
END get_qty_in_transaction_rm;
/
