CREATE OR REPLACE FUNCTION GET_BOM_ID
(p_partnum T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE)
RETURN VARCHAR2
IS
/*  Description     : THIS FUNCTION RETURNS Bill Of Materials ID given the Part Number
 Parameters   : p_part_num
*/
v_bom_id   T207_SET_MASTER.C207_SET_ID%TYPE;
BEGIN
     BEGIN
       SELECT  C207_SET_ID BOMID
    INTO v_bom_id
    FROM  T207_SET_MASTER
       WHERE  C207_SET_DESC = p_partnum
      AND  C901_STATUS_ID <> 20369
      AND  C207_VOID_FL IS NULL;
   EXCEPTION
      WHEN NO_DATA_FOUND
   THEN
      RETURN ' ';
      END;
     RETURN v_bom_id;
END;
/

