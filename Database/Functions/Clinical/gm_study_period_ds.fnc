CREATE OR REPLACE FUNCTION gm_study_period_ds (
	p_study_period_id           IN       NUMBER
 
)
	RETURN VARCHAR2
IS
/*	Description 	: THIS FUNCTION RETURNS CODE ID GIVEN THE CODE NAME AND CODE GROUP
 Parameters 		: p_code_id, p_code_grp
*/
	 p_study_period_ds                VARCHAR2(50);
BEGIN
	BEGIN
		select t613.c613_study_period_ds INTO p_study_period_ds from t613_study_period t613 where t613.c613_study_period_id=p_study_period_id;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN '';
	END;

	RETURN p_study_period_ds;
END;
/