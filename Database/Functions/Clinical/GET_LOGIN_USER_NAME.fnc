/* Formatted on 2010/03/09 15:09 (Formatter Plus v4.8.0) */
CREATE OR REPLACE FUNCTION get_login_user_name (
   p_firstname   VARCHAR2,
   p_lastname    VARCHAR2
)
   RETURN VARCHAR2
IS
/*****************************************************************************
 * Description     : This function is used to return form name based on
 *       form master ID
 * Parameters   : Form ID is passed as in parameter
 ****************************************************************************/
--
   v_user_name   VARCHAR2 (20);
   v_number      NUMBER        := 1;

   CURSOR v_user_check_cur
   IS
      SELECT t102.c102_login_username user_name
        FROM t102_user_login t102
       WHERE t102.c102_login_username LIKE v_user_name || '%';
BEGIN
   SELECT SUBSTR (p_firstname, 0, v_number) || p_lastname
     INTO v_user_name
     FROM DUAL;

   FOR user_chk IN v_user_check_cur
   LOOP
      IF user_chk.user_name = v_user_name
      THEN
         v_number := v_number + 1;

         SELECT SUBSTR (p_firstname, 0, v_number) || p_lastname
           INTO v_user_name
           FROM DUAL;
      ELSE
         EXIT;
      END IF;
   END LOOP;

   RETURN v_user_name;
END get_login_user_name;
/
