--@"C:\Database\Functions\Clinical\GET_SURGICAL_INV_COUNT.fnc";
CREATE OR REPLACE FUNCTION get_surgical_inv_count (
            p_patient_ide_no  VARCHAR2
            )
        RETURN NUMBER
    IS
/***********************************************************
* Description : This function is used to get no of Surgical Intervention for patient
* Author      : Dhana Reddy
***********************************************************/
    v_total_surginv NUMBER;
BEGIN
		 SELECT COUNT (1)
           INTO v_total_surginv
           FROM t629_surgical_intervention t629,t621_patient_master t621
          WHERE t629.c621_patient_id = t621.c621_patient_id
            AND t621.c621_patient_ide_no = p_patient_ide_no
            AND t629.c629_delete_fl  = 'N'
            AND t629.c629_verify_fl  = 'Y'
            AND t621.c621_delete_fl='N';
            
        RETURN v_total_surginv;
        
END get_surgical_inv_count;
/