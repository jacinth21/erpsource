--@"C:\Database\Functions\Clinical\GET_FILLED_FORM_COUNT.fnc";
CREATE OR REPLACE FUNCTION GET_FILLED_FORM_COUNT
(
 p_study_id  v621_patient_study_list.c611_study_id%TYPE,
 p_patient_id v621_patient_study_list.c621_patient_ide_no%TYPE,
 p_study_site_id v621_patient_study_list.c614_study_site_id%TYPE,
 p_study_period v621_patient_study_list.c901_study_period%TYPE
)
RETURN VARCHAR2
IS
/*****************************************************************************
 * Description     : This function is used to return number of filled forms 
 *      
 * Parameters   : Study ID, Patient ID,SiteID,Study Period ID is passed as in parameter
 ****************************************************************************/
--
v_filled   NUMBER;
--
BEGIN
SELECT 
 SUM(DECODE(T622.C613_STUDY_PERIOD_ID,'',0,1))
 INTO V_FILLED
  FROM t622_patient_list t622,
    v621_patient_study_list v621
  WHERE v621.c621_patient_id      = t622.c621_patient_id(+)
  AND v621.c601_form_id           = t622.c601_form_id(+)
  AND v621.c613_study_period_id   = t622.c613_study_period_id(+)
  --AND (v621.c621_exp_date_start  <= SYSDATE  OR v621.c621_exp_date_final    <= SYSDATE)
  AND v621.c621_surgery_comp_fl   = 'Y'
  AND v621.c611_study_id          = p_study_id
  AND v621.c621_patient_ide_no    = p_patient_id
  AND v621.c614_study_site_id    IN DECODE (p_study_site_id,'0',v621.c614_study_site_id,'',v621.c614_study_site_id,p_study_site_id)
  AND NVL(t622.C622_DELETE_FL,'N')='N'
  and v621.c901_study_period= p_study_period;

 RETURN v_filled;
EXCEPTION
 WHEN OTHERS THEN
 --
 RETURN v_filled;
 --
END GET_FILLED_FORM_COUNT;
/

