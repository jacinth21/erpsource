
create or replace FUNCTION get_form_ds
(
 p_form_id t601_form_master.c601_form_id%TYPE
 ,p_study_id t611_clinical_study.c611_study_id%TYPE
)
RETURN VARCHAR2
IS
/*****************************************************************************
 * Description     : This function is used to return form name based on
 *       form master ID
 * Parameters   : Form ID is passed as in parameter
 ****************************************************************************/
--
v_form_ds   t612_study_form_list.c612_study_form_ds%TYPE;
--
BEGIN
 --
 SELECT  c612_study_form_ds
 INTO v_form_ds
 FROM t612_study_form_list
    WHERE c601_form_id = p_form_id AND c611_study_id=p_study_id;
 --
 RETURN v_form_ds;
EXCEPTION
 WHEN OTHERS THEN
 --
 RETURN '';
 --
END get_form_ds;
/