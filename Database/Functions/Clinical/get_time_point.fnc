CREATE OR REPLACE FUNCTION get_time_point (
   p_studyid     VARCHAR2,
   p_patientid   VARCHAR2,
   p_onsetdate   VARCHAR2
)
   RETURN VARCHAR2
IS
/*****************************************************************************
 * Description     : This function is used to return form name based on
 *       form master ID
 * Parameters   : Form ID is passed as in parameter
 ****************************************************************************/
--
   p_timepoint   VARCHAR2 (20);
   v_form_id   VARCHAR2 (20);
   v_study_period_id   VARCHAR2 (20);
--
BEGIN

    SELECT t901.c902_code_nm_alt, t901.c901_code_nm
     INTO v_form_id, v_study_period_id
     FROM t901_code_lookup t901
    WHERE t901.c901_code_id = get_rule_value (p_studyid, 'FCH_PATIENT_TRACK');

   SELECT   v621.c613_study_period_ds
       INTO p_timepoint
       FROM v621_patient_study_list v621
      WHERE v621.c611_study_id = p_studyid
        AND v621.c601_form_id IN (v_form_id)
        AND v621.c621_patient_ide_no = p_patientid
        AND v621.c621_exp_date_start <= TO_DATE (p_onsetdate, 'MM/DD/YYYY')
        AND v621.c621_exp_date_final >= TO_DATE (p_onsetdate, 'MM/DD/YYYY')
   ORDER BY v621.c613_seq_no;

   RETURN p_timepoint;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      BEGIN
         SELECT studyperiod  
          INTO p_timepoint
          FROM (     
           SELECT   v621.c613_study_period_ds studyperiod
               FROM v621_patient_study_list v621
              WHERE v621.c611_study_id = p_studyid
                AND v621.c601_form_id IN (v_form_id)
                AND v621.c621_patient_ide_no = p_patientid
                AND   v621.c621_exp_date_start
                    - TO_NUMBER
                             (get_rule_value (p_studyid,
                                              CONCAT (v621.c613_study_period_ds,
                                                      '-S'
                                                     )
                                             )
                             ) <= TO_DATE (p_onsetdate, 'MM/DD/YYYY')
                AND   v621.c621_exp_date_final
                    + TO_NUMBER
                             (get_rule_value (p_studyid,
                                              CONCAT (v621.c613_study_period_ds,
                                                      '-E'
                                                     )
                                             )
                             ) >= TO_DATE (p_onsetdate, 'MM/DD/YYYY')
           ORDER BY v621.c613_seq_no
           )
           WHERE rownum = 1;

         RETURN p_timepoint;
      END;
   WHEN OTHERS
   THEN
--
      RETURN '';
--
END get_time_point;
/
