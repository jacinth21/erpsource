--@"C:\Database\Functions\Clinical\GET_TOTAL_FORM_COUNT.fnc";
CREATE OR REPLACE FUNCTION GET_TOTAL_FORM_COUNT
(
 p_study_id  v621_patient_study_list.c611_study_id%TYPE,
 p_study_period v621_patient_study_list.c901_study_period%TYPE
)
RETURN VARCHAR2
IS
/*****************************************************************************
 * Description     : This function is used to get the total forms available for the
 *       study for the passed in study period id
 * Parameters   : Study ID , Study Period ID is passed as in parameter
 ****************************************************************************/
--
v_totalform   NUMBER;
--
BEGIN

select count(1) INTO v_totalform
from t612_study_form_list t612, t613_study_period t613 where 
t612.c612_study_list_id =t613.c612_study_list_id 
AND t613.c901_study_period=p_study_period AND t613.C613_DELETE_FL ='N'
and t612.c611_study_id=p_study_id AND t612.C612_DELETE_FL ='N';

 RETURN v_totalform;
EXCEPTION
 WHEN OTHERS THEN
 --
 RETURN v_totalform;
 --
END GET_TOTAL_FORM_COUNT;
/

