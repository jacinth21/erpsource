CREATE OR REPLACE FUNCTION get_study_name (
      p_study_id   IN   t617_site_monitor.c611_study_id%TYPE
   )
      RETURN VARCHAR
   IS
      v_study_nm   t611_clinical_study.c611_study_nm%TYPE;
   BEGIN
      SELECT c611_study_nm
        INTO v_study_nm
        FROM t611_clinical_study
       WHERE c611_study_id = p_study_id;

      RETURN v_study_nm;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN '';
   END get_study_name;
/