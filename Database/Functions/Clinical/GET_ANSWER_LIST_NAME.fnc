CREATE OR REPLACE FUNCTION get_answer_list_name
(
 p_answer_list_id t605_answer_list.c605_answer_list_id%TYPE
)
RETURN VARCHAR2
IS
/*****************************************************************************
 * Description     : This function is used to return form name based on
 *       form master ID
 * Parameters   : Form ID is passed as in parameter
 ****************************************************************************/
--
v_answer_list_nm   t605_answer_list.c605_answer_list_nm%TYPE;
--
BEGIN
 --
 SELECT  c605_answer_list_nm
 INTO v_answer_list_nm
 FROM t605_answer_list
    WHERE c605_answer_list_id = p_answer_list_id;
 --
 RETURN v_answer_list_nm;
EXCEPTION
 WHEN OTHERS THEN
 --
 RETURN '';
 --
END get_answer_list_name;
/

