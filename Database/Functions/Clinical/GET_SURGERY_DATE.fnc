CREATE OR REPLACE FUNCTION get_surgery_date
(
  p_patient_id t621_patient_master.c621_patient_ide_no%TYPE
 ,p_study_id  t621_patient_master.c611_study_id%TYPE
)
RETURN VARCHAR2
IS
/*****************************************************************************
 * Description     : This function is used to return surgery date
 *       for the selected patient
 *
 * Parameters   : Form ID is passed as in parameter
 ****************************************************************************/
--
v_surgery_dt   DATE;
--
BEGIN
 --
 SELECT  c621_surgery_date
 INTO v_surgery_dt
 FROM t621_patient_master
    WHERE c621_patient_ide_no = p_patient_id
 AND  c611_study_id  = p_study_id;
 --
 RETURN v_surgery_dt;
EXCEPTION
 WHEN OTHERS THEN
 --
 RETURN NULL;
 --
END get_surgery_date;
/

