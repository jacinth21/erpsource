/* Formatted on 2007/09/04 17:20 (Formatter Plus v4.8.0) */
--
/********************************************************************
 * Description : This function returns the prinical investigator name name for the given study site id
 * Created By Vprasath
 ********************************************************************/
--

CREATE OR REPLACE FUNCTION get_cr_fch_prinicipal_inv (
	p_id   IN	t615_site_surgeon.c614_study_site_id%TYPE
)
	RETURN VARCHAR2
IS
	v_party_name   VARCHAR2 (100);
--
BEGIN
	SELECT t101.c101_last_nm || ' ' || t101.c101_first_nm
	  INTO v_party_name
	  FROM t615_site_surgeon t615, t101_party t101
	 WHERE t615.c101_party_id = t101.c101_party_id
	   AND t615.c614_study_site_id = p_id
	   AND t615.c901_surgeon_type = '60200'
	   AND t615.c615_void_fl IS NULL
	   AND ROWNUM = 1;

	RETURN v_party_name;
EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		RETURN '';
END get_cr_fch_prinicipal_inv;
/