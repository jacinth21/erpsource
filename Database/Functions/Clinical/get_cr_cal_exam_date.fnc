/* Formatted on 2009/05/08 18:46 (Formatter Plus v4.8.0) */
---@"C:\Database\Functions\Clinical\get_cr_cal_exam_date.fnc";

CREATE OR REPLACE FUNCTION get_cr_cal_exam_date (
	p_surgery_date		  t621_patient_master.c621_surgery_date%TYPE
  , p_period_type		  t613_study_period.c901_period_type%TYPE
  , p_duration			  t613_study_period.c613_duration%TYPE
  , p_grace_period		  t613_study_period.c613_grace_period_after%TYPE
  , p_grace_period_type   t613_study_period.c901_grace_period_type%TYPE
  , p_type				  VARCHAR2
)
	RETURN DATE
IS
/* Description	: THIS FUNCTION RETURNS Patient Exam date based on the paramter
 * Parameters	: p_type holds 'S' -- Start Date , 'E' Expected Date , 'F' Final Date
 *
 */
	v_exam_date    DATE;
	v_temp_exam_date DATE;
	v_temp_val	   NUMBER (2);
	v_grace_period t613_study_period.c613_grace_period_after%TYPE;
	v_duration	   t613_study_period.c613_duration%TYPE;
BEGIN
	--
	v_duration	:= p_duration;

	IF (p_surgery_date IS NULL)
	THEN
		RETURN NULL;
	END IF;

	-- Below code to calcuate grace period (Start Visit Date)
	v_grace_period := p_grace_period;

	IF (p_type = 'S')
	THEN
		v_grace_period := p_grace_period * -1;
	END IF;

	-- if grace period is month then calculate
	IF (p_grace_period_type = 6182)   -- In Months
	THEN
		v_duration	:= p_duration + v_grace_period;
	END IF;

	-- Below code to calcualte exact exam date
	CASE
		WHEN p_period_type = 6181	-- In Days
		THEN
			v_exam_date := p_surgery_date + v_duration;
		WHEN p_period_type = 6184	-- In Wks
		THEN
			v_exam_date := p_surgery_date + (v_duration * 7);
		WHEN p_period_type = 6182	-- In Months
		THEN
			-- v_exam_date := ADD_MONTHS (p_surgery_date, p_duration);
			BEGIN
				v_exam_date := p_surgery_date + NUMTOYMINTERVAL (v_duration, 'MONTH');
			EXCEPTION
				WHEN OTHERS
				THEN
				--	v_temp_exam_date := get_add_months (p_surgery_date, v_duration);
					v_exam_date := get_add_months (p_surgery_date, v_duration);
					--
				/*	v_temp_val	:=
							   TO_NUMBER (TO_CHAR (p_surgery_date, 'DD'))
							   - TO_NUMBER (TO_CHAR (v_temp_exam_date, 'DD'));
					--
					--v_exam_date := (p_surgery_date + v_temp_val) + NUMTOYMINTERVAL (v_duration, 'MONTH');
					v_exam_date := get_add_months (p_surgery_date, v_duration) + v_temp_val;*/
			END;
		WHEN p_period_type = 6183	-- In Years
		THEN
			v_exam_date := get_add_months (p_surgery_date, (v_duration * 12));
		ELSE
			v_exam_date := p_surgery_date;
	END CASE;

	CASE
		WHEN p_grace_period_type IS NULL
		THEN
			v_exam_date := v_exam_date;
		WHEN p_grace_period_type = 6181   -- In Days
		THEN
			v_exam_date := v_exam_date + v_grace_period;
		WHEN p_grace_period_type = 6184   -- In Wks
		THEN
			v_exam_date := v_exam_date + (v_grace_period * 7);
		ELSE
			v_exam_date := v_exam_date;
	END CASE;

	--
	IF (p_type = 'F' AND p_grace_period IS NULL)
	THEN
		v_exam_date := p_surgery_date;
	END IF;

	--IF TO_NUMBER(TO_CHAR(v_exam_date,'DD'))	< TO_NUMBER(TO_CHAR(p_surgery_date,'DD'))
	--THEN
	--	v_exam_date := v_exam_date +   ( TO_NUMBER(TO_CHAR(p_surgery_date,'DD')) - TO_NUMBER(TO_CHAR(v_exam_date,'DD')) );
	--END IF;
	--
	RETURN v_exam_date;
EXCEPTION
	WHEN OTHERS
	THEN
		RETURN NULL;
END get_cr_cal_exam_date;
/
