CREATE OR REPLACE FUNCTION get_form_name
(
 p_form_id t601_form_master.c601_form_id%TYPE
)
RETURN VARCHAR2
IS
/*****************************************************************************
 * Description     : This function is used to return form name based on
 *       form master ID
 * Parameters   : Form ID is passed as in parameter
 ****************************************************************************/
--
v_form_nm   t601_form_master.c601_form_nm%TYPE;
--
BEGIN
 --
 SELECT  c601_form_nm
 INTO v_form_nm
 FROM t601_form_master
    WHERE c601_form_id = p_form_id;
 --
 RETURN v_form_nm;
EXCEPTION
 WHEN OTHERS THEN
 --
 RETURN '';
 --
END get_form_name;
/

