CREATE OR REPLACE FUNCTION get_patient_verified_by
(
 p_patient_list_id t625_patient_verify_list.c622_patient_list_id%TYPE
)
RETURN VARCHAR2
IS
/*****************************************************************************
 * Description     : This function is used to return Verfifed persion id
 *       based on patient list id
 * Parameters   : Patient List id is passed as in parameter is passed as
 *       in parameter
 ****************************************************************************/
--
v_user_nm VARCHAR2(100);
--
BEGIN
 --
 SELECT get_user_name(c101_user_id)
 INTO v_user_nm
 FROM t625_patient_verify_list
 WHERE c625_pat_verify_list_id =
   ( SELECT  max(C625_PAT_VERIFY_LIST_ID)
     FROM t625_patient_verify_list
     WHERE c622_patient_list_id = p_patient_list_id
     AND  c901_verify_level IN (6191, 6192));
 -- 6191 Verified with Correction ,  6192 Verified
 --
 RETURN v_user_nm;
EXCEPTION
 WHEN OTHERS THEN
 --
 RETURN NULL;
 --
END get_patient_verified_by;
/

