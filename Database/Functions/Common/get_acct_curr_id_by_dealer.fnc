--@"c:\database\functions\common\get_acct_curr_id_by_dealer.fnc";
/* 
 *  Description     : This function is used to get account currency symbol by dealer
 */
CREATE OR REPLACE
  FUNCTION get_acct_curr_id_by_dealer(
      p_dealer_id IN t704_account.c101_dealer_id%TYPE)
    RETURN VARCHAR2
  IS
    v_curr_sym VARCHAR2(10);
    v_count NUMBER;
  BEGIN
  	-- To avoid exact fetch error
  	SELECT COUNT(1)
  	INTO v_count
	FROM  ( SELECT DISTINCT c901_currency
	  		FROM t704_account
	  	   WHERE c101_dealer_id = p_dealer_id
	  		 AND C704_VOID_FL    IS NULL
	  	   GROUP BY c101_dealer_id,c704_account_id,c901_currency);
  
 	
	 IF v_count = 1
	 THEN 
	    SELECT distinct t704.c901_currency
	      INTO v_curr_sym
	      FROM t704_account t704
	     WHERE t704.c101_dealer_id = p_dealer_id
	       AND C704_VOID_FL IS NULL;
	 ELSE
	 	v_curr_sym := '';
	 END IF;      
           
    RETURN v_curr_sym;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    	v_curr_sym := '';  
    RETURN v_curr_sym;
  END get_acct_curr_id_by_dealer;
  /
