CREATE OR REPLACE FUNCTION GET_USER_NAME
(p_user_id T101_USER.C101_USER_ID%TYPE)
RETURN VARCHAR2
IS
/*  Description     : THIS FUNCTION RETURNS CODE NAME GIVEN THE CODE_ID
 Parameters 		: p_code_id
*/
v_user_nm VARCHAR2(100);
BEGIN
     BEGIN
       SELECT  C101_USER_F_NAME || ' ' || C101_USER_L_NAME NAME
	   INTO v_user_nm
	   FROM  T101_USER
       WHERE  C101_USER_ID = p_user_id;
   EXCEPTION
      WHEN NO_DATA_FOUND
   THEN
      RETURN ' ';
      END;
     RETURN v_user_nm;
END;
/

