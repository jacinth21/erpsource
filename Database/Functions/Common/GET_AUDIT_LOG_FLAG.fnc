--@"c:\database\functions\common\GET_AUDIT_LOG_FLAG.fnc"
CREATE OR REPLACE FUNCTION GET_AUDIT_LOG_FLAG
(  p_ref_id  t941_audit_trail_log.c941_ref_id%TYPE
 ,p_audit_trail_id  t941_audit_trail_log.c940_audit_trail_id%TYPE

)
RETURN VARCHAR2
IS
/*  Description     : THIS FUNCTION IS USED TO CHECK IF THERE IS ANY AUDIT LOG ENTRY FOR PASSED IN CRITERIA.
 */
v_flag CHAR(1);
BEGIN
--
    SELECT   'Y'
    INTO   v_flag
    FROM    t941_audit_trail_log
    WHERE  c941_ref_id = p_ref_id
    AND  c940_audit_trail_id = p_audit_trail_id
    AND  ROWNUM  = 1;
    --
 RETURN v_flag;
 --
EXCEPTION
WHEN NO_DATA_FOUND
THEN
 RETURN 'N';
END GET_AUDIT_LOG_FLAG;
/

