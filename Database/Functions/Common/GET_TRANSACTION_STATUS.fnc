    /******************************************************
	 * Description : Function will return Status Flag Name
     ******************************************************/
    /* Formatted on 2014/03/19 21:15 (Formatter Plus v4.8.0) */
    /* get_transaction_status() is created for getting the Status Name for the Status Flag Value
     * for Loaners, Consignments,In-House transactiosn and Orders by pasiing the Staus Flag value.
     * Funaction is created as part of the NSP-435.
     * */
-- @"C:\Database\Functions\Common\get_transaction_status.fnc";    
	CREATE OR REPLACE FUNCTION get_transaction_status (
		p_statusflag 	IN VARCHAR2,
    	p_trans_type    IN VARCHAR2
	)
		RETURN VARCHAR2
	IS
		v_status_name  VARCHAR2 (100);
		v_table 	   t906_rules.c906_rule_value%TYPE;
	BEGIN
		SELECT get_rule_value ('TRANS_TABLE', p_trans_type)
		  INTO v_table
		  FROM DUAL;
		  --For Loaners there is no rule value so that has been handled using the transtype.
			IF v_table = 'CONSIGNMENT' THEN
	    		   SELECT DECODE (p_statusflag,
			                      0,    'Initiated',
			                      1,    'WIP',
			                      1.10, 'Completed Set',
			                      1.20, 'Pending Putaway',
			                      2,    'Built Set/Pending Control Number',
			                      2.20, 'Pending Pick',
			                      3,    'Pending Shipping',
			                      3.30, 'Packing In Progress',
			                      3.60, 'Ready For Pickup',
			                      4,    'Completed'			                   
			                    )
			         INTO v_status_name
			         FROM DUAL;        
   			ELSIF v_table = 'ORDERS' THEN   
				   SELECT DECODE (p_statusflag,
				   				  0,    'Back Order',
				                  1,    'Pending Control Number',
				                  2,    'Pending Shipping',
				                  2.30, 'Packing In Progress',
				                  2.60, 'Ready For Pickup',	
				                  3 ,   'Completed'				                  
				                 )
				     INTO v_status_name
				     FROM DUAL; 		   
			ELSIF v_table = 'IN_HOUSE' THEN   
				   SELECT DECODE (p_statusflag,
				                  2,   'Pending Control Number',
				                  3,   'Pending Verification',
				                  3.30,'Packing In Progress',
				                  3.60,'Ready For Pickup',	
				                  4 ,  'Completed'				               
				                 )
				     INTO v_status_name
				     FROM DUAL;
			ELSIF p_trans_type = '4127' THEN   -- 4127[Consignment Loaner]
				   SELECT gm_pkg_op_loaner.get_loaner_status (p_statusflag) 
				     INTO v_status_name 
				     FROM DUAL;
   			END IF;
		RETURN v_status_name;
EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		RETURN NULL;
END get_transaction_status;
/
