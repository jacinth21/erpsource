/* Formatted on 2009/06/04 15:29 (Formatter Plus v4.8.0) */
--@"c:\Database\Functions\Common\get_workingweekdays.fnc"

CREATE OR REPLACE FUNCTION get_workingweekdays (
	p_start_date   IN	DATE
  , p_end_date	   IN	DATE
)
	/******************************************************************

	* Description		: function returns No of week days Excluding Saturday and Sunday
			------------------
		 p_start_date	: Start Date
		  p_end_date	: End Date
	****************************************************************/
RETURN NUMBER
IS
	v_days		   NUMBER;
	v_company_id        t1900_company.c1900_company_id%TYPE;
BEGIN
	--
	 SELECT NVL (get_compid_frm_cntx (), get_rule_value ('DEFAULT_COMPANY', 'ONEPORTAL'))
	   INTO v_company_id
	   FROM dual;
	--
	 SELECT COUNT (1)
	   INTO v_days
	   FROM date_dim
	  WHERE date_dim.cal_date >= p_start_date
	    AND date_dim.cal_date <= p_end_date
	    AND DAY NOT           IN ('Sat', 'Sun')
	    AND date_key_id NOT   IN
	    (
	         SELECT date_key_id
	           FROM COMPANY_HOLIDAY_DIM
	          WHERE HOLIDAY_DATE    >= p_start_date
	            AND HOLIDAY_DATE    <= p_end_date
	            AND HOLIDAY_FL       = 'Y'
	            AND C1900_COMPANY_ID = v_company_id
	    ) ; 
	--    
	RETURN v_days;
END get_workingweekdays;
/
