/* Formatted on 2009/06/02 15:33 (Formatter Plus v4.8.0)
@"C:\Database\Functions\Common\get_field_sales_net_con_qty.fnc";*/
CREATE OR REPLACE
    FUNCTION get_field_sales_net_con_qty (
            p_inv_warehouse_id IN t5051_inv_warehouse.c5051_inv_warehouse_id%TYPE,
            p_ref_id           IN t5052_location_master.c5052_ref_id%TYPE,
            p_pnum             IN t5053_location_part_mapping.c205_part_number_id%TYPE,
            p_location_type    IN t5052_location_master.c901_location_type%TYPE)
        RETURN NUMBER
    IS
        /* Description  : This function return the FS qty.
        Parameters   : p_inv_warehouse, p_ref_id, p_pnum, p_location_type
        */
        v_consign_qty NUMBER;
        v_company_id   t1900_company.c1900_company_id%TYPE;
   BEGIN
           SELECT  NVL(get_compid_frm_cntx(),get_rule_value('DEFAULT_COMPANY','ONEPORTAL')) 
                     INTO  v_company_id
                     FROM DUAL;
        BEGIN
             SELECT t5053.c5053_curr_qty
               INTO v_consign_qty
               FROM t5051_inv_warehouse t5051, t5052_location_master t5052, t5053_location_part_mapping t5053
              WHERE t5051.c5051_inv_warehouse_id = t5052.c5051_inv_warehouse_id
                AND t5052.c5052_location_id      = t5053.c5052_location_id
                AND t5051.c5051_inv_warehouse_id = p_inv_warehouse_id
                AND t5052.c5052_ref_id           = p_ref_id
                AND t5053.c205_part_number_id    = p_pnum
                AND t5052.c901_location_type     = p_location_type --'4000338'
                AND t5052.c5052_void_fl         IS NULL
                AND c1900_company_id        = v_company_id;
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_consign_qty := 0;
        END;
        RETURN v_consign_qty;
    END get_field_sales_net_con_qty;
    /