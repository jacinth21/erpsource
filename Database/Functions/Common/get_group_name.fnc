create or replace
FUNCTION              "GET_GROUP_NAME" (
	p_groupid	IN	 t4010_group.c4010_group_id%TYPE
)
	RETURN VARCHAR2
IS
	v_group_name   VARCHAR2 (100);
BEGIN
	SELECT c4010_group_nm
	  INTO v_group_name
	  FROM t4010_group
	 WHERE c4010_group_id = p_groupid;

	RETURN v_group_name;
EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		RETURN ' ';
END get_group_name;
/