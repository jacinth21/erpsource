/* Formatted on 2007/09/28 14:30 (Formatter Plus v4.8.0) */
-- @"C:\Database\Functions\Common\get_upload_file_list_id.fnc";
CREATE OR REPLACE FUNCTION get_upload_file_list_id (
	p_ref_id  t903_upload_file_list.c903_ref_id%TYPE,
	p_type           t903_upload_file_list.c901_ref_type%TYPE    
)
	RETURN NUMBER
IS
/*	Description 	: THIS FUNCTION RETURNS FILE ID GIVEN THE REF ID AND TYPE
 Parameters 		: p_acc_id
*/
	v_upload_file_id	   t903_upload_file_list.c903_upload_file_list%TYPE;
BEGIN
	BEGIN
		SELECT c903_upload_file_list
		  INTO v_upload_file_id
   			FROM t903_upload_file_list
  		WHERE  c903_ref_id     = p_ref_id
    		AND c901_ref_type   = p_type
  			AND c903_delete_fl IS NULL;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			v_upload_file_id := NULL;
		WHEN TOO_MANY_ROWS THEN
		     SELECT c903_upload_file_list
		       INTO v_upload_file_id
		       FROM
			(
			     SELECT c903_upload_file_list
			       FROM t903_upload_file_list
			      WHERE c903_ref_id     = p_ref_id
				AND c901_ref_type   = p_type
				AND c903_delete_fl IS NULL
			   ORDER BY C903_CREATED_DATE DESC
			)
		      WHERE rownum = 1;
	END;

	RETURN v_upload_file_id;
END get_upload_file_list_id;
/