--@"C:\Database\Functions\Common\get_company_code.fnc"

CREATE OR REPLACE
    FUNCTION get_company_code(
    	p_comp_id   T1900_COMPANY.C1900_COMPANY_ID%TYPE
    )
        RETURN VARCHAR2
    IS
        /* Description  : THIS FUNCTION RETURNS COMPANY code for the passed in Company ID.
        */
        v_comp_cd T1900_COMPANY.C1900_COMPANY_NAME%TYPE;
    BEGIN
        BEGIN
            SELECT C1900_COMPANY_CD  INTO v_comp_cd
            FROM T1900_COMPANY
            WHERE C1900_VOID_FL IS NULL
            AND C1900_COMPANY_ID = p_comp_id;
            
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_comp_cd := '';
        END;
        RETURN v_comp_cd;
    END get_company_code;
    /