CREATE OR REPLACE FUNCTION GET_LOG_FLAG
(  p_ref_id  t902_log.c902_ref_id%TYPE
 ,p_type  t902_log.c902_type%TYPE

)
RETURN VARCHAR2
IS
/*  Description     : THIS FUNCTION RETURNS CODE NAME GIVEN THE CODE_ID
 Parameters   : p_code_id
*/
v_flag CHAR(1);
BEGIN
--
    SELECT   'Y'
    INTO   v_flag
    FROM    t902_log
    WHERE  c902_ref_id = p_ref_id
    AND  c902_type = p_type    
    AND C902_VOID_FL IS NULL
    AND  ROWNUM  = 1;
    --
 RETURN v_flag;
 --
EXCEPTION
WHEN NO_DATA_FOUND
THEN
 RETURN 'N';
END GET_LOG_FLAG;
/

