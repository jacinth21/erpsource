/* Formatted on 2011/05/09 09:52 (Formatter Plus v4.8.0) */
--@"C:\Database\Functions\Common\get_csg_part_qty_for_dist.fnc"
--
/********************************************************************
 * Description : This function is used to get the Total Consignment
 * Qty of a part for the given distributor id.
 ********************************************************************/
--

CREATE OR REPLACE FUNCTION get_csg_part_qty_for_dist (
	p_id		IN	 t701_distributor.c701_distributor_id%TYPE
  , p_emp_id	IN	 t506_returns.c101_user_id%TYPE
  , p_part_id	IN	 t205_part_number.c205_part_number_id%TYPE
)
	RETURN NUMBER
IS
	v_consg_qty    NUMBER;
--
BEGIN
	IF (p_part_id = '')
	THEN
		RETURN 0;
	END IF;

	SELECT (NVL (consign, 0) - NVL (cons_return, 0)) pending_qty
	  INTO v_consg_qty
	  FROM t205_part_number t205
		 , (SELECT	 t505.c205_part_number_id partnum, SUM (t505.c505_item_qty) consign
				FROM t504_consignment t504, t505_item_consignment t505
			   WHERE t504.c504_consignment_id = t505.c504_consignment_id
				 -- AND t504.c701_distributor_id = p_distid
				 AND DECODE (p_id, '01', t504.c704_account_id, c701_distributor_id) = p_id
				 AND NVL (t504.c504_ship_to_id, -999) = DECODE (p_id, '01', p_emp_id, NVL (t504.c504_ship_to_id, -999))
				 AND c504_type IN (4110, 4112, 40057,9110)
				 AND t504.c504_status_fl = 4
				 AND c504_void_fl IS NULL
				 AND TRIM (t505.c505_control_number) IS NOT NULL
				 AND t505.c205_part_number_id = p_part_id
			GROUP BY t505.c205_part_number_id) cons
		 , (SELECT	 t507.c205_part_number_id partnum, SUM (c507_item_qty) cons_return
				FROM t506_returns t506, t507_returns_item t507
			   WHERE t506.c506_rma_id = t507.c506_rma_id
				 -- AND t506.c701_distributor_id = p_distid
				 AND DECODE (p_id, '01', t506.c704_account_id, t506.c701_distributor_id) = p_id
				 AND DECODE (p_id, '01', t506.c101_user_id, '01') = DECODE (p_id, '01', p_emp_id, '01')
				 --AND t506.c506_type IN (3301, 3302)
				 AND t506.c506_void_fl IS NULL
				 AND t506.c506_status_fl = 2
				 AND TRIM (t507.c507_control_number) IS NOT NULL
				 AND t507.c507_status_fl IN ('C', 'W')
				 AND t507.c205_part_number_id = p_part_id
			GROUP BY t507.c205_part_number_id) retn
	 WHERE t205.c205_part_number_id = p_part_id AND t205.c205_part_number_id = cons.partnum(+)
		   AND t205.c205_part_number_id = retn.partnum(+);

	RETURN v_consg_qty;
EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		RETURN 0;
END get_csg_part_qty_for_dist;
/