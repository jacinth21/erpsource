--@"C:\Database\Functions\Common\get_company_dtfmt.fnc"
CREATE OR REPLACE  FUNCTION get_company_dtfmt (
    p_company_id t1900_company.c1900_company_id%TYPE
)
RETURN VARCHAR2
IS
/* Description  : THIS FUNCTION RETURNS COMPANY DATE FORMAT BASED ON COMPANY 
   Parameters   : p_company_id
*/
v_comp_dt_fmt VARCHAR2(100);

    BEGIN
        BEGIN
			                
			SELECT get_code_name(c901_date_format)
			  INTO v_comp_dt_fmt
	          FROM t1900_company 
		     WHERE c1900_company_id = p_company_id
		       AND c1900_void_fl IS NULL;

        EXCEPTION
        	WHEN NO_DATA_FOUND THEN
            v_comp_dt_fmt := 'MM/dd/yyyy';
        END;
   RETURN v_comp_dt_fmt;
END get_company_dtfmt;
/
    