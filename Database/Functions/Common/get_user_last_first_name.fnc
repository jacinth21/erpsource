-- @"C:\Database\Functions\Common\get_user_last_first_name.fnc";
CREATE OR REPLACE
    FUNCTION get_user_last_first_name (
            p_user_id T101_USER.C101_USER_ID%TYPE)
        RETURN VARCHAR2
    IS
        /*  Description     : THIS FUNCTION RETURNS user last name first name
               Parameters   : p_user_id
        */
        v_user_nm VARCHAR2 (100) ;
    BEGIN
        BEGIN
             SELECT C101_USER_L_NAME || ', ' || C101_USER_F_NAME NAME
               INTO v_user_nm
               FROM T101_USER
              WHERE C101_USER_ID = p_user_id;
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            RETURN NULL;
        END;
        RETURN v_user_nm;
    END;
    /
