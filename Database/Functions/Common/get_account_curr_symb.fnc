--@"c:\database\functions\common\get_account_curr_symb.fnc";
/* 
 *  Description     : This function is used to get account currency symbol
 */
CREATE OR REPLACE
  FUNCTION get_account_curr_symb(
      p_account_id IN t704_account.c704_account_id%TYPE)
    RETURN VARCHAR2
  IS
    v_curr_sym VARCHAR2(10);
  BEGIN
    SELECT get_code_name(t704.c901_currency)
      INTO v_curr_sym
      FROM t704_account t704
     WHERE t704.c704_account_id = p_account_id
       AND C704_VOID_FL IS NULL;
    RETURN v_curr_sym;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    RETURN '';
  END get_account_curr_symb;
  /
