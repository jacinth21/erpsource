
CREATE OR REPLACE FUNCTION get_plant_email_id (
	p_plant_id	t5040_plant_master.c5040_plant_id%TYPE
)
	RETURN VARCHAR2
IS
/*	Description 	: THIS FUNCTION RETURNS PLANT EMAIL ID
 Parameters 		: p_plant_id
*/
	v_plant_email_id t5040_plant_master.c5040_plant_email_id%TYPE;
BEGIN
	BEGIN
		SELECT c5040_plant_email_id
		  INTO v_plant_email_id
		  FROM t5040_plant_master
		 WHERE c5040_plant_id = p_plant_id
		   AND c5040_void_fl IS NULL;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN '';
	END;

	RETURN v_plant_email_id;
END get_plant_email_id;
/