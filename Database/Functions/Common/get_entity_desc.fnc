/* Formatted on 2007/08/06 16:41 (Formatter Plus v4.8.0) */
--
/*******************************************************
 * Purpose: This functiond to fetch the description of an
 * entity which has a reference id and a reference type
 *******************************************************/
--

CREATE OR REPLACE FUNCTION get_entity_desc (
	p_ref_id	 IN   VARCHAR
  , p_ref_type	 IN   t4021_demand_mapping.c901_ref_type%TYPE
)
	RETURN VARCHAR
IS
	v_entity_desc  VARCHAR2 (200);
--
BEGIN
-- FOR GROUP
	IF p_ref_type = 20297
	THEN
		SELECT c4010_group_nm
		  INTO v_entity_desc
		  FROM t4010_group
		 WHERE c4010_group_id = p_ref_id;
	END IF;

-- FOR PART
	IF p_ref_type = 20295
	THEN
		SELECT get_partnum_desc (p_ref_id)
		  INTO v_entity_desc
		  FROM DUAL;
	END IF;

-- FOR SET
	IF p_ref_type = 20296
	THEN
		SELECT get_set_name (p_ref_id)
		  INTO v_entity_desc
		  FROM DUAL;
	END IF;

	RETURN v_entity_desc;
EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		RETURN '';
END get_entity_desc;
