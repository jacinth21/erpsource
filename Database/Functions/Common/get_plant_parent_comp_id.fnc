--@"C:\Database\Functions\Common\get_plant_parent_comp_id.fnc"
CREATE OR REPLACE FUNCTION get_plant_parent_comp_id (
        p_plant_id t5040_plant_master.c5040_plant_id%TYPE)
    RETURN NUMBER
IS
    /* Description  : THIS FUNCTION RETURNS PLANT NAME
    */
    v_company_id t1900_company.c1900_company_id%TYPE;
    v_context_comp_id t1900_company.c1900_company_id%TYPE; 
BEGIN
	--
	SELECT get_compid_frm_cntx () INTO v_context_comp_id from dual;
	--
	IF v_context_comp_id = 1018
	THEN
		 RETURN v_context_comp_id;
	END IF;
	--
    BEGIN
         SELECT c1900_parent_company_id
           INTO v_company_id
           FROM t5040_plant_master
          WHERE c5040_plant_id = p_plant_id;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_company_id := '';
    END;
    RETURN v_company_id;
END get_plant_parent_comp_id;
/