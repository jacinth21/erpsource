/*
 * Author		: Rajkumar
 * Description  : To fetch the Transaction details for Location Transfer 
 */
CREATE OR REPLACE FUNCTION "GET_TRANS_LOCATION_DETAIL" (
	 p_transid	 t5055_control_number.c5055_ref_id%TYPE
  ,p_type	 t5055_control_number.C901_REF_TYPE%TYPE
)
	RETURN VARCHAR2
IS
v_trans		VARCHAR2(100) := '';
BEGIN
	BEGIN
		WITH 
			v_txns 
			AS (
			SELECT MAX(TRANS_ID) trans_id FROM(
				SELECT GET_CODE_NAME(T5052.C901_LOCATION_TYPE) TRANS_ID ,T5055.C901_TYPE TYPE
				  FROM T5055_CONTROL_NUMBER T5055,T5052_LOCATION_MASTER T5052 
				 WHERE T5052.C5052_LOCATION_ID = T5055.C5052_LOCATION_ID 
					AND T5055.C901_TYPE IN ('93343','93344')
					AND T5055.C5055_REF_ID = p_transid 
					AND T5055.C901_REF_TYPE = p_type
					AND t5052.c5051_inv_warehouse_id NOT IN
				    (
				         SELECT c906_rule_value
				           FROM t906_rules
				          WHERE c906_rule_id     = 'WHID'
				            AND c906_rule_grp_id = 'RULEWH'
				            AND c906_void_fl    IS NULL
				    )--excluding FS and account warehouse
					AND T5052.C5052_VOID_FL IS NULL
					AND T5055.C5055_VOID_FL IS NULL )
			        GROUP BY TYPE
					ORDER BY TYPE 
			 )
			 SELECT MAX (SYS_CONNECT_BY_PATH (trans_id, ' to ')) INTO  v_trans
			   FROM( SELECT trans_id, rownum curr FROM v_txns)
			 CONNECT BY curr - 1 = PRIOR curr
			 START WITH curr = 1;
		v_trans := SUBSTR(v_trans, LENGTH(' to ')+1);
		v_trans := NVL(get_code_id(v_trans,'TRNLOC'),'');
	END;
			RETURN v_trans;
END;

/
