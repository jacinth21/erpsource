/* Formatted on 2009/05/08 18:24 (Formatter Plus v4.8.0) */
--- @"C:\Database\Functions\Common\get_add_months.fnc";

CREATE OR REPLACE FUNCTION get_add_months (
	p_date		 DATE
  , p_duration	 NUMBER
)
	RETURN DATE
IS
/*
Description : THIS FUNCTION adds month to the input date as specified by MONTH function in Excel.
Example:
If I/P date is 11/30/2007 and we add 3 months, excel returns 03/02/2008 while add_months of oracle returns 02/28/2008
If I/P date is 11/30/2007 and we add 2 months, excel returns 01/30/2008 while add_months of oracle returns 01/31/2008
 Parameters   : 	p_date
*/
	v_date		   DATE;
BEGIN
	SELECT TO_DATE (TO_CHAR (ADD_MONTHS (p_date, p_duration), 'MM/YYYY'), 'MM/YYYY') + EXTRACT (DAY FROM (p_date)) - 1
	  INTO v_date
	  FROM DUAL;

	RETURN v_date;
EXCEPTION
	WHEN OTHERS
	THEN
		RETURN NULL;
END;
