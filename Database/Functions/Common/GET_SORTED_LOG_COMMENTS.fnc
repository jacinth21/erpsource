

create or replace
FUNCTION              "GET_SORTED_LOG_COMMENTS" (
	p_ref_id   t902_log.c902_ref_id%TYPE
  , p_type	   t902_log.c902_type%TYPE 
  , p_code_id   IN   VARCHAR2
)
	RETURN VARCHAR2
IS
/*	Description 	: THIS FUNCTION RETURNS COMMENT BY SORTING
 Parameters   : p_code_id
*/
	v_comment	   VARCHAR2 (4000);
BEGIN

--
					SELECT c902_comments INTO v_comment
                    FROM (SELECT c902_comments 
                    FROM t902_log 
                    WHERE c902_ref_id = p_ref_id
                    AND c902_type = p_type
                    AND c902_void_fl IS NULL 
                    ORDER BY 
                    CASE p_code_id when '106981' then C902_CREATED_DATE  end ASC ,CASE p_code_id when '106980' then C902_CREATED_DATE  end DESC)
                    WHERE  ROWNUM = 1;
	--
	RETURN v_comment;
--
EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		RETURN '';
END GET_SORTED_LOG_COMMENTS;
/