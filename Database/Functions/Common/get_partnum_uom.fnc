/* Formatted on 2008/09/26 18:52 (Formatter Plus v4.8.0) */
--@c:\database\functions\common\get_partnum_uom.fnc

CREATE OR REPLACE FUNCTION get_partnum_uom (
	p_part_num	 t205_part_number.c205_part_number_id%TYPE
)
	RETURN VARCHAR2
IS
/*	Description 	: THIS FUNCTION RETURNS UOM for the given part number
 Parameters 		: p_part_num
*/
	v_part_uom	   t205_part_number.c901_uom%TYPE;
BEGIN
	BEGIN
		SELECT c901_uom
		  INTO v_part_uom
		  FROM t205_part_number
		 WHERE c205_part_number_id = p_part_num;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN '';
	END;

	RETURN v_part_uom;
END;
/
