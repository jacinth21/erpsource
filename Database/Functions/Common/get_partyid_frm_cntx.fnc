--@"C:\Database\Functions\Common\get_partyid_frm_cntx.fnc"
CREATE OR REPLACE
    FUNCTION get_partyid_frm_cntx
        RETURN VARCHAR2
    IS
        /* Description  : THIS FUNCTION RETURNS PARTY ID FROM CONTEXT
        */
        v_party_id T101_PARTY.C101_PARTY_ID%TYPE;
    BEGIN
        BEGIN
            SELECT SYS_CONTEXT('CLIENTCONTEXT', 'PARTY_ID')  INTO v_party_id
            FROM DUAL;
            
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_party_id := '';
        END;
        RETURN v_party_id;
    END get_partyid_frm_cntx;
    /