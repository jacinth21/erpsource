/* Formatted on 2007/10/11 18:05 (Formatter Plus v4.8.0) */
--
/********************************************************************
 * Description : This function formats the given number 
 * with the given precision
 * Created By Vprasath
 ********************************************************************/
--

CREATE OR REPLACE FUNCTION get_formatted_number (
 p_no    NUMBER
  , p_precision   NUMBER
)
 RETURN VARCHAR2
IS
 v_temp     VARCHAR2 (15);
 v_roundoff_char VARCHAR2 (15);
 v_append_zero  VARCHAR2 (10);
 v_return_no    VARCHAR2 (10);
--
BEGIN
 IF p_precision = 1
 THEN
  v_roundoff_char := '999999.9';
  v_append_zero := '.0';
 ELSE
  v_roundoff_char := '999999999.99';
  v_append_zero := '.00';
 END IF;

 v_temp  := TRIM(NVL (TO_CHAR (p_no, v_roundoff_char), v_append_zero));

  IF ( v_temp >= 0 AND v_temp < 1)
  THEN
  v_temp := '0' || v_temp;
 END IF;

 RETURN v_temp;
END get_formatted_number;
/
