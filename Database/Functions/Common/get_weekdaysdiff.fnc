/* Formatted on 2009/06/02 15:33 (Formatter Plus v4.8.0) */
CREATE OR REPLACE FUNCTION get_weekdaysdiff (
	d1	 IN   DATE
  , d2	 IN   DATE
)
	RETURN NUMBER
IS
	diff		   NUMBER;
BEGIN
	diff		:= get_work_days_count(TO_CHAR(d1,'MM/DD/YYYY') , TO_CHAR(d2,'MM/DD/YYYY'));
	RETURN diff;
END get_weekdaysdiff;
/