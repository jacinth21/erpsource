--@"c:\database\functions\common\get_part_size.fnc"

/********************************************************************
 * Description : This function returns the part size by  part id
 * 
 ********************************************************************/
--

CREATE OR REPLACE FUNCTION get_part_size (
	p_pnum	 			t4011_group_detail.c205_part_number_id%TYPE,
    p_control_num		t2550_part_control_number.c2550_control_number%TYPE
)
	RETURN VARCHAR2
IS
	v_size   VARCHAR2 (100);
--
BEGIN	
	 SELECT NVL(T2550.C2550_CUSTOM_SIZE, T205M.C205M_SIZE)                        /*Get part size from new label param in PMT-34056 */
	   INTO v_size
	   FROM T205M_PART_LABEL_PARAMETER T205M, T2550_PART_CONTROL_NUMBER T2550
	  WHERE T205M.C205_PART_NUMBER_ID = p_pnum       	 
		AND T205M.C2550_VOID_FL IS NULL
		AND T205M.C205_PART_NUMBER_ID = T2550.C205_PART_NUMBER_ID(+)
		AND T2550.C2550_CONTROL_NUMBER(+) = p_control_num;

     RETURN v_size;
EXCEPTION
	 WHEN NO_DATA_FOUND
	 THEN
		  RETURN '';
END get_part_size;
/