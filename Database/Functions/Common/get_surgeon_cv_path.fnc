CREATE OR REPLACE FUNCTION get_surgeon_cv_path
(p_partyid t101_party.c101_party_id%type)
RETURN VARCHAR2
IS
/*  Description     : THIS FUNCTION RETURNS SURGEON CV PATH GIVEN THE PARTY_ID
 Parameters 		: p_partyid
*/
    v_cv_path VARCHAR2(1000);
     BEGIN
		SELECT C901_REF_GRP
		  || '/'
		  || C903_REF_ID
		  || '/'
		  || utl_url.escape(C903_FILE_NAME)
		INTO v_cv_path
		FROM T903_UPLOAD_FILE_LIST
		WHERE C903_DELETE_FL IS NULL
		AND C901_REF_GRP      = '103112'
		AND C901_REF_TYPE    IN ('105220')
		AND C903_REF_ID        = TO_CHAR(p_partyid);
	 RETURN v_cv_path;
   EXCEPTION
      WHEN NO_DATA_FOUND
   THEN
      RETURN '';
    
END;

/

