--@"C:\Database\Functions\Common\get_segment_name.fnc"
CREATE OR REPLACE
    FUNCTION get_segment_name(
    	p_segment_id   T2090_SEGMENT.C2090_SEGMENT_ID%TYPE
    )
        RETURN VARCHAR2
    IS
        /* Description  : This Function Returns the Segment Name for the Passed in Segment ID.
        */
        v_segment_name T2090_SEGMENT.C2090_SEGMENT_NAME%TYPE;
    BEGIN
        BEGIN
            SELECT C2090_SEGMENT_NAME  INTO v_segment_name
            FROM T2090_SEGMENT
            WHERE C2090_VOID_FL IS NULL
            AND C2090_SEGMENT_ID = p_segment_id;
            
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_segment_name := '';
        END;
        RETURN v_segment_name;
    END get_segment_name;
    /