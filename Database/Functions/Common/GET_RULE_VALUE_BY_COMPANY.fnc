/* Formatted on 2008/10/24 19:22 (Formatter Plus v4.8.0) */
CREATE OR REPLACE FUNCTION get_rule_value_by_company(
        p_id            t906_rules.c906_rule_id%TYPE ,
        p_type          t906_rules.c906_rule_grp_id%TYPE ,
        P_company_id    t1900_company.c1900_company_id%TYPE )
    RETURN VARCHAR2
IS
    /********************************************************
    * Description : This function returns rule value
    *  based on the rule id and rule group
    * Parameters  : p_id, p_type
    ********************************************************/
    v_id t906_rules.c906_rule_value%TYPE;
BEGIN
    SELECT c906_rule_value
            INTO v_id
    FROM t906_rules
    WHERE c906_rule_id   = p_id
    AND c906_rule_grp_id = p_type
    AND C1900_COMPANY_ID =P_company_id
    AND C906_VOID_FL    IS NULL ;
    --
    RETURN v_id;
EXCEPTION
WHEN NO_DATA_FOUND THEN
    RETURN NULL;
END get_rule_value_by_company;
/