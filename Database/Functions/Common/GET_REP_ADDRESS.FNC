--@"C:\Database\Functions\Sales\GET_REP_ADDRESS.FNC"

CREATE OR REPLACE FUNCTION GET_REP_ADDRESS
(p_rep_id T703_SALES_REP.C703_SALES_REP_ID%TYPE)
RETURN T703_SALES_REP.C703_SHIP_ADD1%TYPE
IS

v_rep_add  varchar2(30000);
BEGIN
     BEGIN
     	 			  SELECT c106_add1 || c106_add2 ||' '|| c106_city ||' '|| get_code_name(c901_state) ||' '|| get_code_name(c901_country) ||' '|| c106_zip_code
	 	              INTO v_rep_add 
	 	              FROM  t106_address t106,t703_sales_rep t703 
					  WHERE   t703.c101_party_id = t106.c101_party_id AND t703.c703_active_fl ='Y' and t106.C106_PRIMARY_FL='Y' and t106.C106_VOID_FL is null AND 
					  t703.C703_VOID_FL is null AND t703.c703_sales_rep_id = p_rep_id;
		   
   EXCEPTION
      WHEN NO_DATA_FOUND
   THEN
      RETURN ' ';
      END;
      dbms_output.put_line(v_rep_add);
     RETURN v_rep_add;
END GET_REP_ADDRESS;  
/