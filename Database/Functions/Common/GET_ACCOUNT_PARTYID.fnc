/* Created on 03/27/2017 
--- @"C:\Projects\PMT\database\Functions\Common\GET_ACCOUNT_PARTYID.fnc";
-- Author : Matt B
-- Purpose : To get the party id for a given account id
*/
CREATE OR REPLACE FUNCTION GET_ACCOUNT_PARTYID (
   p_AccountId   IN T704_Account.C704_Account_id%TYPE)
   RETURN VARCHAR2
AS
   v_partyId   T704_Account.C101_Party_id%TYPE;
BEGIN
   SELECT C101_PARTY_ID
     INTO v_partyId
     FROM t704_account
    WHERE c704_account_id = p_AccountId;

   RETURN v_partyId;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      RETURN '';
END GET_ACCOUNT_PARTYID;

/

