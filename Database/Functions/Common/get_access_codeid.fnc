--Description     : THIS FUNCTION RETURNS access code id for the code id
--Parameters 	  : p_code_id , p_company_id
CREATE OR REPLACE FUNCTION get_access_codeid (
	p_code_id    t901_code_lookup.c901_code_id%TYPE,
	p_company_id T1900_COMPANY.C1900_COMPANY_ID%TYPE
)
	RETURN NUMBER
IS
	v_access_code_id   t901_code_lookup.c901_code_id%TYPE;
BEGIN
	BEGIN
		SELECT c901_access_code_id
		  INTO v_access_code_id
		  FROM t901a_lookup_access
		 WHERE c901_code_id   = p_code_id
		 AND c1900_company_id = p_company_id
		 AND ROWNUM = 1;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN NULL;
	END;
RETURN v_access_code_id;
END get_access_codeid;
/