/* Formatted on 2010/09/27 10:43 (Formatter Plus v4.8.0) */
CREATE OR REPLACE FUNCTION get_vendor_split_percentage (
   p_vend_id   t301_vendor.c301_vendor_id%TYPE
)
   RETURN NUMBER
IS
/*  Description     : THIS FUNCTION RETURNS split percentage value, GIVEN THE vendorid
 Parameters       : p_code_id
*/
   v_value   NUMBER (15, 2);
BEGIN
   SELECT c301_rm_percentage
     INTO v_value
     FROM t301_vendor t301
    WHERE t301.c301_vendor_id = p_vend_id AND t301.c301_active_fl IS NULL;

   RETURN v_value;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      RETURN NULL;
END get_vendor_split_percentage;
