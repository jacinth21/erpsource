/* Formatted on 2009/12/23 11:29 (Formatter Plus v4.8.0) */

--@"C:\DATABASE\FUNCTIONS\COMMON\get_rule_desc.fnc";


CREATE OR REPLACE FUNCTION get_rule_desc (
	p_id	 t906_rules.c906_rule_id%TYPE
  , p_type	 t906_rules.c906_rule_grp_id%TYPE
)
	RETURN VARCHAR2
IS
/********************************************************
 * Description : This function returns rule desc value
 *		based on the rule id and rule group
 * Parameters  : p_id, p_type
 ********************************************************/
	v_desc		   t906_rules.c906_rule_desc%TYPE;
BEGIN
--
	SELECT c906_rule_desc
	  INTO v_desc
	  FROM t906_rules
	 WHERE c906_rule_id = p_id AND c906_rule_grp_id = p_type;

--
	RETURN v_desc;
EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		RETURN '';
END get_rule_desc;
/
