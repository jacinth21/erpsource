--@"C:\Database\Functions\Common\get_country_id_from_distid.fnc"
CREATE OR REPLACE  FUNCTION get_country_id_from_distid (
    p_distributor_id t701_distributor.c701_distributor_id%TYPE
)
RETURN VARCHAR2
IS
/* Description  : THIS FUNCTION RETURNS COUNTRY ID
   Parameters   : p_distributor_id
*/
v_code_id t710_sales_hierarchy.c901_country_id%TYPE;

    BEGIN
        BEGIN
			                
			SELECT  t710.c901_country_id INTO v_code_id
        	FROM    t701_distributor t701
    	    	,t710_sales_hierarchy t710
	    	WHERE t701.c701_region = t710.c901_area_id
    		AND   t710.c710_active_fl = 'Y'
    		AND   t701.c701_void_fl is null
     		AND   t701.c701_distributor_id = p_distributor_id;

        EXCEPTION
        	WHEN NO_DATA_FOUND THEN
            v_code_id := NULL;
        END;
   RETURN v_code_id;
END get_country_id_from_distid;
/
    