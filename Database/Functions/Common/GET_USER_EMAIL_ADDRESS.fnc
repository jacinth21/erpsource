/* Formatted on 2009/03/07 10:42 (Formatter Plus v4.8.0) */
--@"c:\database\functions\common\GET_USER_EMAIL_ADDRESS.fnc";

CREATE OR REPLACE FUNCTION GET_USER_EMAIL_ADDRESS (
	p_user_id	t101_user.c101_user_id%TYPE
)
	RETURN VARCHAR2
IS
	v_user_nm	   t101_user.c101_email_id%TYPE;
BEGIN
	SELECT c101_email_id
	  INTO v_user_nm
	  FROM t101_user
	 WHERE c101_user_id = p_user_id
	 AND c901_user_status = 311;

	RETURN v_user_nm;
END;
/
