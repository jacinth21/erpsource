CREATE OR REPLACE FUNCTION GET_CODE_SEQ_NO
(
 p_code_id T901_CODE_LOOKUP.C901_CODE_ID%TYPE
)
RETURN NUMBER
IS
/******************************************************************
 * Description     : THIS FUNCTION RETURNS CODE SEQ NO
 * Parameters   : p_code_id
 *************************************************************/
v_code_seq   t901_code_lookup.c901_code_seq_no%TYPE;
BEGIN
 SELECT  c901_code_seq_no
 INTO v_code_seq
 FROM t901_code_lookup
    WHERE c901_code_id = p_code_id;
 --
 RETURN v_code_seq ;
 --
EXCEPTION
WHEN NO_DATA_FOUND
THEN
      RETURN NULL;
END GET_CODE_SEQ_NO;
/

