--@"C:\Database\Functions\Common\get_compid_from_distid.fnc"
CREATE OR REPLACE
    FUNCTION get_compid_from_distid (
            p_distributor_id t701_distributor.c701_distributor_id%TYPE)
        RETURN VARCHAR2
    IS
        /* Description  : THIS FUNCTION RETURNS COMPANY ID
        Parameters   : p_distributor_id
        */
        v_comp_id t704_account.c901_company_id%TYPE;
    BEGIN
        BEGIN
             SELECT NVL (c901_company_id, 100800)
               INTO v_comp_id
               FROM t710_sales_hierarchy
              WHERE c901_area_id IN
                (
                     SELECT c701_region
                       FROM t701_distributor
                      WHERE c701_distributor_id = p_distributor_id
                ) AND C710_ACTIVE_FL = 'Y' AND  C710_VOID_FL IS NULL;
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_comp_id := 100800;
        END;
        RETURN v_comp_id;
    END get_compid_from_distid;
    /