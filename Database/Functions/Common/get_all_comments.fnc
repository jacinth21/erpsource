--@C:\Database\Functions\Common\get_all_comments.fnc

create or replace FUNCTION              "GET_ALL_COMMENTS" (
    p_id	 T902_LOG.c902_ref_id%TYPE
  , p_type	 t902_LOG.c902_type%TYPE
  , p_skip_fl varchar2 DEFAULT 'Y'
)
	RETURN VARCHAR2
IS
/*	Description 	: THIS FUNCTION RETURNS CODE NAME GIVEN THE CODE_ID
 Parameters 		: p_code_id
*/
v_comments VARCHAR2(32760);	
	
	CURSOR pop_val
	IS
		SELECT TRANSLATE (C902_COMMENTS,
                  'X' || DECODE(p_skip_fl,'Y',TRANSLATE (C902_COMMENTS,
                                    '-ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.(),/\- ',
                                    '-'	
                                   ),C902_COMMENTS),
                   DECODE(p_skip_fl,'Y','X','X'||C902_COMMENTS) 
                 ) comm
		  FROM T902_LOG
		  WHERE c902_ref_id = p_id
		  AND c902_type = p_type
      	  AND c902_void_fl IS NULL;

--
BEGIN
	FOR rad_val IN pop_val
	LOOP
		v_comments := v_comments || rad_val.comm;
	END LOOP;

	RETURN v_comments;
END  get_all_comments;
/
 
 
 
