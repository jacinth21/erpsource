/* Formatted on 2011/01/10 10:39 (Formatter Plus v4.8.0) */
 --@"c:\database\functions\common\get_rfs_info.fnc";

CREATE OR REPLACE FUNCTION get_rfs_country (
	p_num		t205d_part_attribute.c205_part_number_id%TYPE
  , p_country	t205d_part_attribute.c205d_attribute_value%TYPE
)
	RETURN VARCHAR2
IS
	v_value 	   VARCHAR2 (20);
BEGIN
	SELECT c205d_attribute_value
	  INTO v_value
	  FROM t205d_part_attribute
	 WHERE c205_part_number_id = p_num AND c901_attribute_type = 80180 AND c205d_attribute_value = p_country;

	--80180, RFS
	RETURN v_value;
EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		RETURN NULL;
END get_rfs_country;
/
