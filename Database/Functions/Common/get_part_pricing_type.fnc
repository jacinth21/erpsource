/* Formatted on 2009/11/18 14:15 (Formatter Plus v4.8.0) */
--@"C:\DATABASE\Functions\Common\get_part_pricing_type.fnc"

CREATE OR REPLACE FUNCTION get_part_pricing_type (
	p_pnum	 t4011_group_detail.c205_part_number_id%TYPE
)
	RETURN NUMBER
IS
/*	Description 	: THIS FUNCTION RETURNS part pricing type in group detail table
 Parameters 		: p_pnum
*/
	v_pricing_type t4011_group_detail.c901_part_pricing_type%TYPE;
BEGIN
	BEGIN
		SELECT COUNT (1)
		  INTO v_pricing_type
		  FROM t4010_group t4010, t4011_group_detail t4011
		 WHERE t4010.c4010_group_id = t4011.c4010_group_id
		   AND t4011.c205_part_number_id = p_pnum
		   AND t4010.c901_type = 40045
		   AND t4010.c4010_void_fl IS NULL;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN 0;
	END;

	RETURN v_pricing_type;
END get_part_pricing_type;
/
