/* Formatted on 2012/02/17 12:16 (Formatter Plus v4.8.0) */
--@"C:\Database\Functions\Common\get_shipping_division_id.fnc";
/*PC-577- Fedex/UPS  account split-up for shipping transactions
 *  Description    : THIS FUNCTION RETURNS DIVISION ID based on transaction type 
 Parameters       : p_txn_id ,p_source
*/
CREATE OR REPLACE FUNCTION get_shipping_division_id (
	p_txn_id      t907_shipping_info.c907_ref_id%TYPE,
    p_source	  t907_shipping_info.c901_source%TYPE
)
	RETURN v700_territory_mapping_detail.divid%TYPE
IS
/*	Description 	: THIS FUNCTION RETURNS division id
*/
	v_rule_value	   t906_rules.c906_rule_value%TYPE;
	v_exec_string VARCHAR2 (3000) ;
	v_division_id   VARCHAR2(1000);

BEGIN
	  SELECT c906_rule_value
		INTO v_rule_value
		FROM t906_rules
		WHERE c906_rule_id = p_source AND c906_rule_grp_id = 'ACCSPLIT'
		AND C1900_COMPANY_ID IS NULL
		AND C906_VOID_FL IS NULL;

--v_exec_string := 'BEGIN'|| v_rule_value || '(' || p_txn_id ||'); END;';
--EXECUTE IMMEDIATE (v_exec_string) INTO v_division_id;

EXECUTE IMMEDIATE 'SELECT ' || v_rule_value || '('''|| p_txn_id ||''') FROM ' || 'DUAL'  INTO v_division_id;	
	
	RETURN v_division_id;
END get_shipping_division_id;
/
