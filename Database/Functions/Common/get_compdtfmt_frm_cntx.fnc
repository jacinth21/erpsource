--@"C:\Database\Functions\Common\get_compdtfmt_frm_cntx.fnc"
CREATE OR REPLACE
    FUNCTION get_compdtfmt_frm_cntx
        RETURN VARCHAR2
    IS
        /* Description  : THIS FUNCTION RETURNS COMPANY Date Format FROM CONTEXT
        */
        v_comp_dt_fmt VARCHAR2 (20);
    BEGIN
        BEGIN
            SELECT SYS_CONTEXT('CLIENTCONTEXT', 'COMP_DATE_FMT')  INTO v_comp_dt_fmt
            FROM DUAL;
            
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_comp_dt_fmt := '';
        END;
        RETURN v_comp_dt_fmt;
    END get_compdtfmt_frm_cntx;
    /