
--@"C:\Database\Functions\Common\get_comp_holiday_cnt.fnc";

CREATE OR REPLACE FUNCTION get_comp_holiday_cnt (
        p_from_date  DATE,
        p_to_date    DATE,
        p_company_id NUMBER)
    RETURN NUMBER
IS
    /*
    * Description  : This function used to returns company holiday - based on from and to date filter
    */
    v_holiday_cnt NUMBER;
BEGIN
	--
     SELECT COUNT (1)
       INTO v_holiday_cnt
       FROM company_holiday_dim
      WHERE holiday_date     > TRUNC (p_from_date)
        AND holiday_date    <= TRUNC (p_to_date)
        AND holiday_fl       = 'Y'
        AND c1900_company_id = p_company_id;
        --
    RETURN v_holiday_cnt;
END get_comp_holiday_cnt;
/