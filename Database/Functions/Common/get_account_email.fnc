--@"c:\database\functions\common\get_account_email.fnc";
CREATE OR REPLACE
    FUNCTION get_account_email (
            p_inv_id IN t503_invoice.c503_invoice_id%TYPE)
        RETURN VARCHAR2
    IS
        v_id t704a_account_attribute.c704a_attribute_value%TYPE;
    BEGIN
         SELECT t704a.c704a_attribute_value
           INTO v_id
           FROM t704a_account_attribute t704a, t503_invoice t503
          WHERE t704a.c704_account_id     = t503.c704_account_id
            AND t503.c503_invoice_id      = p_inv_id
            AND t704a.c901_attribute_type = 91985
            AND t503.c503_void_fl        IS NULL
            AND t503.c503_delete_fl      IS NULL
            AND t704a.C704A_VOID_FL      IS NULL;
        RETURN v_id;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN '';
    END;
    /
