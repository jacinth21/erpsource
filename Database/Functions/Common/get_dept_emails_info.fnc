
--@"c:\database\functions\common\get_dept_emails_info.fnc";

CREATE OR REPLACE FUNCTION get_dept_emails_info (
        p_dept_id IN t101_user.c901_dept_id%TYPE
    )
        RETURN VARCHAR2
    IS
        otherinfo       VARCHAR2 (4000);
    BEGIN
        WITH other_info AS (
            SELECT t101.C101_EMAIL_ID email_id, t101.C101_USER_ID id
              from t101_user t101
             WHERE t101.C901_DEPT_ID = p_dept_id
             AND t101.C901_USER_STATUS = 311
        )
        SELECT MAX (SYS_CONNECT_BY_PATH (email_id, ','))
          INTO otherinfo
          FROM
            (SELECT email_id, ROW_NUMBER () OVER (ORDER BY id) AS curr
               FROM other_info)
          CONNECT BY curr - 1 = PRIOR curr
         START WITH curr = 1;
         RETURN SUBSTR (otherinfo, 2) ;
    END get_dept_emails_info;
    
 /