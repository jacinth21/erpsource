-- @"C:\Database\Functions\Common\get_user_last_login.fnc";

CREATE OR REPLACE
    FUNCTION get_user_last_login (
            p_userid T101_USER.C101_USER_ID%TYPE)
        RETURN DATE
    IS
        /*  Description  : This Function Return User Last Login in Date.
            Parameters   : p_user_id
        */
        v_date DATE;
    BEGIN
        BEGIN
             SELECT MAX (C103_LOGIN_TS)
               INTO v_date
               FROM T103_USER_LOGIN_TRACK
              WHERE C101_USER_ID = p_userid;
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_date := NULL;
        END;
        RETURN v_date ;
    END get_user_last_login;
    /
