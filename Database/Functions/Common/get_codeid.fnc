/* Formatted on 2008/11/13 09:22 (Formatter Plus v4.8.0) */


CREATE OR REPLACE FUNCTION get_code_id (
	p_code_nm	 t901_code_lookup.c901_code_nm%TYPE
  , p_code_grp	 t901_code_lookup.c901_code_grp%TYPE
)
	RETURN VARCHAR2
IS
/*	Description 	: THIS FUNCTION RETURNS CODE ID GIVEN THE CODE NAME AND CODE GROUP
 Parameters 		: p_code_id, p_code_grp
*/
	v_code_id	   t901_code_lookup.c901_code_id%TYPE;
BEGIN
	BEGIN
		SELECT c901_code_id
		  INTO v_code_id
		  FROM t901_code_lookup
		 WHERE c901_code_nm = p_code_nm AND c901_code_grp = p_code_grp;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN '';
	END;

	RETURN v_code_id;
END;
/
