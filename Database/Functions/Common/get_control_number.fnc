/* Formatted on 2007/08/02 15:46 (Formatter Plus v4.8.0) */
/********************************************************************
 * Description : This function is used to get the control number for
 * the	given consignment id , part number and qty.
 ********************************************************************/
--

CREATE OR REPLACE FUNCTION get_control_number (
	p_consignment_id   IN	t504_consignment.c504_consignment_id%TYPE
  , p_part_id		   IN	t205_part_number.c205_part_number_id%TYPE
  , p_item_qty		   IN	t505_item_consignment.c505_item_qty%TYPE
)
	RETURN VARCHAR
IS
	v_cntrl_number t505_item_consignment.c505_control_number%TYPE;
	v_plant_id     t5040_plant_master.c5040_plant_id%type;
--
BEGIN

	SELECT  get_plantid_frm_cntx()
	  INTO  v_plant_id
	  FROM dual;

	IF (p_consignment_id = '')
	THEN
		RETURN '';
	END IF;

	SELECT c505_control_number
	  INTO v_cntrl_number
	  FROM t505_item_consignment t505,t504_consignment t504
	 WHERE t505.c504_consignment_id = p_consignment_id AND c205_part_number_id = p_part_id AND c505_item_qty = p_item_qty
	   AND t504.c5040_plant_id = v_plant_id
       AND t504.c504_consignment_id = t505.c504_consignment_id;

	RETURN v_cntrl_number;
EXCEPTION
	WHEN OTHERS
	THEN
		RETURN '';
END get_control_number;
/