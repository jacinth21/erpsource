--@"C:\Database\Functions\Common\get_plantid_frm_cntx.fnc"
CREATE OR REPLACE
    FUNCTION get_plantid_frm_cntx
        RETURN VARCHAR2
    IS
        /* Description  : THIS FUNCTION RETURNS PLANT ID FROM CONTEXT
        */
        v_plant_id T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
    BEGIN
        BEGIN
            SELECT SYS_CONTEXT('CLIENTCONTEXT', 'PLANT_ID')  INTO v_plant_id
            FROM DUAL;
            
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_plant_id := '';
        END;
        RETURN v_plant_id;
    END get_plantid_frm_cntx;
    /