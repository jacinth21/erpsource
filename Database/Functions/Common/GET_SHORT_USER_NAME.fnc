/**
 * @Description: This function is used to give the short name for all users
 * @Author: Rajkumar
 */
-- @"C:\Database\Functions\Common\GET_SHORT_USER_NAME.fnc";

CREATE OR REPLACE function GET_SHORT_USER_NAME(
	p_string	VARCHAR2
)
RETURN VARCHAR2
AS
v_short_name	VARCHAR2(200);
v_substring		VARCHAR(2000);
v_string		VARCHAR(2000);
BEGIN	
	v_short_name:='';
	v_string := p_string || ' ';
	WHILE INSTR (v_string, ' ') <> 0
	LOOP
		v_substring 	:= SUBSTR (v_string, 1, INSTR (v_string, ' ') - 1);
		v_string		:= SUBSTR (v_string, INSTR (v_string, ' ') + 1);
		v_short_name := v_short_name || substr(v_substring,1,1);
	END LOOP;
	RETURN v_short_name;
END;
/