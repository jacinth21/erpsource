--@"C:\Database\Functions\Common\get_compid_frm_cntx.fnc"
CREATE OR REPLACE
    FUNCTION get_compid_frm_cntx
        RETURN VARCHAR2
    IS
        /* Description  : THIS FUNCTION RETURNS COMPANY ID FROM CONTEXT
        */
        v_comp_id T1900_COMPANY.C1900_COMPANY_ID%TYPE;
    BEGIN
        BEGIN
            SELECT SYS_CONTEXT('CLIENTCONTEXT', 'COMPANY_ID')  INTO v_comp_id
            FROM DUAL;
            
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_comp_id := '';
        END;
        RETURN v_comp_id;
    END get_compid_frm_cntx;
    /