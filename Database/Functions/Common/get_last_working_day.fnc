/* Formatted on 2009/06/02 15:33 (Formatter Plus v4.8.0) */
--@"C:\Database\Functions\Common\get_last_working_day.fnc";
CREATE OR REPLACE
    FUNCTION get_last_working_day (
            p_date IN DATE)
        RETURN DATE
    IS
        /* Description  : This function return the last working day for the pass date.
           Parameters   : p_date
        */
        v_last_work_dt DATE;
        v_company_id NUMBER;
    BEGIN
    
    SELECT get_compid_frm_cntx()
		INTO   v_company_id
		FROM   DUAL;
		
        BEGIN
             SELECT cdate
               INTO v_last_work_dt
               FROM
                (
                     SELECT cal_date cdate
                       FROM date_dim
                      WHERE cal_date   >= TRUNC (p_date) - 8
                        AND cal_date    < TRUNC (p_date)
                        AND day NOT IN ('Sat','Sun')
                        AND date_key_id NOT IN (
				              	SELECT date_key_id 
				              	  FROM COMPANY_HOLIDAY_DIM 
				              	 WHERE HOLIDAY_DATE   >= TRUNC (p_date) - 8
				              	   AND HOLIDAY_DATE   < TRUNC (p_date)
				              	   AND HOLIDAY_FL = 'Y'
				              	    AND C1900_COMPANY_ID = v_company_id
								)
                   ORDER BY cal_date DESC
                )
              WHERE rownum = 1;
            --
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_last_work_dt := NULL;
        END;
        RETURN v_last_work_dt;
    END get_last_working_day;
    /