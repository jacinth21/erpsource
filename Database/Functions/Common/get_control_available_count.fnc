--@"c:\database\functions\common\get_control_available_count.fnc"

/*************************************************************************
 * Description : This function returns the Control Number in Reserve Slot
 * Author : Harinadh Reddi
 *************************************************************************/
--
CREATE OR REPLACE FUNCTION get_control_available_count (
	p_part_num     IN      VARCHAR2,
    p_control_num  IN      VARCHAR2,
    p_trans_type   IN      VARCHAR2,
    p_trans_id     IN      VARCHAR2
)
	RETURN NUMBER
IS
	v_avail_qty     NUMBER;
	v_plant_id		t5040_plant_master.c5040_plant_id%TYPE;
--
 BEGIN	 
	 
	 --Getting company id and plant id from context.   

    SELECT get_plantid_frm_cntx()
	  INTO v_plant_id
	  FROM dual;
	--103960:Open; 103961: In-Progress; 90800:Inventory Qty
	   BEGIN
		  SELECT DECODE(sign((Nvl(T5060.C5060_Qty,0) - Nvl(Other_Reserve.Qty,0)))
                           ,'-1','0',(Nvl(T5060.C5060_Qty,0) - Nvl(Other_Reserve.Qty,0))) 
            INTO v_avail_qty   
            FROM T5060_Control_Number_Inv T5060,
                 (SELECT SUM(NVL(t5062.c5062_reserved_qty,0)) qty 
                    FROM t5062_control_number_reserve t5062
                       , t5060_control_number_inv t5060
                   WHERE T5060.C5060_Control_Number_Inv_Id = T5062.C5060_Control_Number_Inv_Id  
                     AND NVL(t5062.c901_transaction_type,'-999') = decode(t5062.c901_transaction_type,NULL,'-999',p_trans_type)
                     AND T5060.C205_Part_Number_Id =  p_part_num
                     AND T5060.C901_Warehouse_Type = '90800'
                     AND NVL(t5062.c901_status,'-9999') IN ('103960','103961','-9999')                     
                     AND T5060.C5060_Control_Number = p_control_num
           			 AND T5062.C5062_Transaction_Id <> p_trans_id
           			 AND T5060.C5040_PLANT_ID = v_plant_id
           			 AND t5062.C5040_PLANT_ID = v_plant_id
                     AND T5062.C5062_Void_Fl Is Null
             ) Other_Reserve
           WHERE T5060.C205_Part_Number_Id =  p_part_num
             AND T5060.C5060_Control_Number = p_control_num
             AND T5060.C5040_PLANT_ID = v_plant_id
             AND T5060.C901_Warehouse_Type = '90800';
	   EXCEPTION
  			WHEN NO_DATA_FOUND THEN
    			v_avail_qty:= 0; 
	   END;
	RETURN v_avail_qty;
END get_control_available_count;
/