CREATE OR REPLACE FUNCTION GET_CODE_ID
(p_code_nm T901_CODE_LOOKUP.C901_CODE_NM%TYPE)
RETURN VARCHAR2
IS
/*  Description     : THIS FUNCTION RETURNS CODE NAME GIVEN THE CODE_ID
 Parameters 		: p_code_id
*/
v_code_id   T901_CODE_LOOKUP.C901_CODE_ID%TYPE;
BEGIN
     BEGIN
       SELECT  C901_CODE_ID CODEID
	   INTO v_code_id
	   FROM  T901_CODE_LOOKUP
       WHERE  C901_CODE_NM = p_code_nm;
   EXCEPTION
      WHEN NO_DATA_FOUND
   THEN
      RETURN ' ';
      END;
     RETURN v_code_id;
END;
/

