--@"c:\database\functions\common\get_rep_div_id_by_dealer.fnc";
/* 
 *  Description     : This function is used to get account currency symbol by dealer
 */
CREATE OR REPLACE
  FUNCTION get_rep_div_id_by_dealer(
      p_dealer_id IN t704_account.c101_dealer_id%TYPE)
    RETURN VARCHAR2
  IS
    v_rep_div_id t703_sales_rep.c1910_division_id%TYPE;
	v_count NUMBER;
  BEGIN
    
	-- To avoid exact fetch error
  	SELECT COUNT(1)
  	INTO v_count
	FROM  (SELECT distinct gm_pkg_sm_mapping.get_salesrep_division(C703_SALES_REP_ID)
	  		FROM t704_account
	  	   WHERE c101_dealer_id = p_dealer_id
	  		 AND C704_VOID_FL    IS NULL);
	
	IF v_count = 1
	 THEN 
    SELECT distinct gm_pkg_sm_mapping.get_salesrep_division(C703_SALES_REP_ID)
		  INTO v_rep_div_id
		  FROM T704_ACCOUNT
		  WHERE c101_dealer_id = p_dealer_id
		    AND C704_VOID_FL IS NULL;
	ELSE
		v_rep_div_id :='2000'; -- 2000 : Spine division id
	END IF;	 
   
    RETURN v_rep_div_id;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
  	v_rep_div_id := '';
    RETURN v_rep_div_id;
  END get_rep_div_id_by_dealer;
  /
