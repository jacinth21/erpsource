--@"C:\Database\Functions\Common\get_comp_curr.fnc"
CREATE OR REPLACE FUNCTION get_comp_curr (
        p_comp_id T1900_COMPANY.C1900_COMPANY_ID%TYPE)
    RETURN VARCHAR2
IS
    /* Description  : This function returns company currency id
    */
    v_comp_curr T901_CODE_LOOKUP.C901_CODE_NM%TYPE;
BEGIN
    BEGIN
         SELECT t1900.c901_txn_currency
           INTO v_comp_curr
           FROM t1900_company t1900
          WHERE t1900.c1900_company_id = p_comp_id
            AND t1900.c1900_void_fl   IS NULL;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_comp_curr := '';
    END;
    RETURN v_comp_curr;
END get_comp_curr;
/