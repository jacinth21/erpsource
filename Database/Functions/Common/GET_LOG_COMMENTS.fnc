create or replace
FUNCTION              "GET_LOG_COMMENTS" (
	p_ref_id   t902_log.c902_ref_id%TYPE
  , p_type	   t902_log.c902_type%TYPE
)
	RETURN VARCHAR2
IS
/*	Description 	: THIS FUNCTION RETURNS CODE NAME GIVEN THE CODE_ID
 Parameters   : p_code_id
*/
	v_comment	   VARCHAR2 (4000);
BEGIN
--
	SELECT c902_comments
	  INTO v_comment
	  FROM t902_log
	 WHERE c902_ref_id = p_ref_id
	   AND c902_type = p_type
	   AND ROWNUM = 1;

	--
	RETURN v_comment;
--
EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		RETURN ' ';
END get_log_comments;
/