/* Formatted on 2009/12/01 12:36 (Formatter Plus v4.8.0) */
--@"c:\database\Functions\Common\get_dist_bill_add.fnc"

CREATE OR REPLACE FUNCTION get_dist_bill_add (
	p_dist_id	IN	 t2651_field_sales_details.c701_distributor_id%TYPE
)
	RETURN VARCHAR2
IS
/*	Description 	: THIS FUNCTION RETURNS ship address for dist id
 Parameters 		: p_dist_id
*/
	v_bill_add	   VARCHAR2 (1000);
BEGIN
	BEGIN
		SELECT	  t701.c701_distributor_name
			   || '<BR>&nbsp;'
			   || t701.c701_bill_add1
			   || '<BR>&nbsp;'
			   || t701.c701_bill_add2
			   || '<BR>&nbsp;'
			   || DECODE(NVL(GET_RULE_VALUE('SWAP_ZIP_CODE','SWAP_ZIP_CODE'),'N'),'Y',t701.c701_bill_zip_code,t701.c701_bill_city)
			   || ',&nbsp;'
			   || get_code_name_alt (t701.c701_bill_state)
			   || '&nbsp;'
			   || DECODE(NVL(GET_RULE_VALUE('SWAP_ZIP_CODE','SWAP_ZIP_CODE'),'N'),'Y',t701.c701_bill_city,t701.c701_bill_zip_code)
		  INTO v_bill_add
		  FROM t701_distributor t701
		 WHERE t701.c701_distributor_id = p_dist_id;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN ' ';
	END;

	RETURN v_bill_add;
END get_dist_bill_add;
/
