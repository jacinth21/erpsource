--@"C:\Database\Functions\Common\get_company_timezone.fnc"
CREATE OR REPLACE  FUNCTION get_company_timezone (
    p_company_id t1900_company.c1900_company_id%TYPE
)
RETURN VARCHAR2
IS
/* Description  : THIS FUNCTION RETURNS THE COMPANY TIME ZONE BASED ON COMPANY 
   Parameters   : p_company_id
*/
v_comp_time_zne VARCHAR2(100);

    BEGIN
        BEGIN
			                
			SELECT get_code_name(C901_TIMEZONE)
			  INTO v_comp_time_zne
	          FROM t1900_company 
		     WHERE c1900_company_id = p_company_id
		       AND c1900_void_fl IS NULL;

        EXCEPTION
        	WHEN NO_DATA_FOUND THEN
            v_comp_time_zne := 'US/Eastern';
        END;
   RETURN v_comp_time_zne;
END get_company_timezone;
/
    