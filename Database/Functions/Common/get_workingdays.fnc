/* Formatted on 2009/08/05 17:36 (Formatter Plus v4.8.0) */
--@"c:\database\Functions\Common\get_workingdays.fnc"

CREATE OR REPLACE FUNCTION get_workingdays (
	p_type	 VARCHAR2
)
	/******************************************************************

	* Description	  : function returns No of working days
				------------------
		  p_type			:
		   Month			:  No of working days for the month
		   Quarter	  :  No of working days for the Quarter
		   Year 	  :  No of working days for the Year
		   MWC		  :  No of working days completed for Month
		   QWC		  :  No of working days completed for Month
		   YWC		  :  No of working days completed for Year
	****************************************************************/
RETURN NUMBER
IS
	v_holidays	   NUMBER;
	v_start_date   DATE;
	v_end_date	   DATE;
	v_tot_week_days NUMBER;
	diff		   NUMBER;
	v_month 	   NUMBER;
	v_year		   NUMBER;
	v_tot_working_days NUMBER;
	v_comp_working_days NUMBER;
	days NUMBER;
	v_year_number NUMBER;
	v_quarter_number NUMBER;
BEGIN
	IF (p_type = 'Month')
	THEN
		v_start_date := TRUNC (CURRENT_DATE, 'month');
		v_end_date	:= TRUNC (LAST_DAY (CURRENT_DATE));
		
		v_tot_working_days := get_workingweekdays (v_start_date, v_end_date);
		
		v_start_date := TRUNC (CURRENT_DATE, 'month');
		v_end_date	:= TRUNC (CURRENT_DATE);
		
		v_comp_working_days := get_workingweekdays (v_start_date, v_end_date);
		
		days := v_tot_working_days - v_comp_working_days;

	END IF;

	IF (p_type = 'MWC')
	THEN
		v_start_date := TRUNC (CURRENT_DATE, 'month');
		v_end_date	:= TRUNC (CURRENT_DATE);
		
		days := get_workingweekdays (v_start_date, v_end_date);
	END IF;


IF ((p_type = 'Quarter') OR  (p_type = 'QWC'))
	THEN
	
	 SELECT year_number, quarter_number
	   INTO v_year_number, v_quarter_number
	   FROM date_dim
	  WHERE year_month = TO_CHAR (CURRENT_DATE, 'yyyy/mm')
	GROUP BY year_number, quarter_number;
	--		
		SELECT MIN (cal_date), MAX (cal_date)
	   INTO v_start_date, v_end_date
	   FROM date_dim
	  WHERE year_number    = v_year_number
	    AND quarter_number = v_quarter_number
	ORDER BY cal_date;
		
	IF (p_type = 'Quarter')
	THEN		
		v_tot_working_days := get_workingweekdays (v_start_date, v_end_date);
		v_end_date	:= TRUNC (CURRENT_DATE);		
		v_comp_working_days := get_workingweekdays (v_start_date, v_end_date);
		days := v_tot_working_days - v_comp_working_days;
	END IF;

	IF (p_type = 'QWC')
	THEN
		v_end_date	:= TRUNC (CURRENT_DATE);		
		days := get_workingweekdays (v_start_date, v_end_date);
	END IF;
END IF;

	IF ((p_type = 'Year') OR (p_type = 'YWC'))
	THEN
	  SELECT MIN (cal_date), MAX (cal_date)
	   INTO v_start_date, v_end_date
	   FROM date_dim
	  WHERE year_number = TO_CHAR (CURRENT_DATE, 'yyyy')
	ORDER BY cal_date;
		
	IF (p_type = 'Year')
	THEN
		v_tot_working_days := get_workingweekdays (v_start_date, v_end_date);
		v_end_date	:= TRUNC (CURRENT_DATE);		
		v_comp_working_days := get_workingweekdays (v_start_date, v_end_date);
		days := v_tot_working_days - v_comp_working_days;
	END IF;

	IF (p_type = 'YWC')
	THEN
		v_end_date	:= TRUNC (CURRENT_DATE);		
		days := get_workingweekdays (v_start_date, v_end_date);	
	END IF;
END IF;

	RETURN days;
END get_workingdays;
/
