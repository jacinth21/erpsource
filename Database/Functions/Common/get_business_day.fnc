/* Formatted on 2009/06/02 15:33 (Formatter Plus v4.8.0) 
@"c:\database\Functions\Common\get_business_day.fnc";*/
CREATE OR REPLACE FUNCTION get_business_day (
   p_date IN DATE
 , p_no_of_days  IN NUMBER 
  )
     RETURN DATE
    IS
        /* Description  : This function return the last working day for the pass date.
           Parameters   : p_date
        */
        v_work_dt DATE;
        v_company_id NUMBER;
	BEGIN        
	
		SELECT get_compid_frm_cntx()
		INTO   v_company_id
		FROM   DUAL;
		
		SELECT cal_date
		  INTO v_work_dt
		  FROM
				( SELECT ROWNUM rnum, cal_date  
					FROM
				        (
				             SELECT cal_date
				               FROM date_dim
				              WHERE cal_date   > p_date
				              AND DAY NOT IN ('Sat','Sun')
				              AND date_key_id NOT IN (
				              	SELECT date_key_id 
				              	  FROM COMPANY_HOLIDAY_DIM 
				              	 WHERE HOLIDAY_DATE   > p_date
				              	   AND HOLIDAY_FL = 'Y'
				              	    AND C1900_COMPANY_ID = v_company_id
								)
				           ORDER BY cal_date
				         ) 
				)
		WHERE rnum =  p_no_of_days;
           
        RETURN v_work_dt;
    END get_business_day;
    /