
CREATE OR REPLACE FUNCTION get_login_user_name_by_id (
   p_user_id   T101_USER.C101_USER_ID%TYPE
)
   RETURN VARCHAR2
IS
/*****************************************************************************
 * Description     : This Function will return the Login User name for the passed in user id
  ****************************************************************************/
--
   v_user_name   t102_user_login.c102_login_username%TYPE;

BEGIN
	BEGIN
 		SELECT t102.c102_login_username 
 		INTO v_user_name
        FROM t102_user_login t102
       WHERE t102.C101_USER_ID =p_user_id;
       --No Void Flag check required as we ae checking by user id
   
  EXCEPTION WHEN NO_DATA_FOUND
  THEN
  	v_user_name := null;
  END;
  
  return v_user_name;
END get_login_user_name_by_id;
/
