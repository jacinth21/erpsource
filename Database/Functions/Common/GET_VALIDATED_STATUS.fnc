create or replace
FUNCTION GET_VALIDATED_STATUS
(p_part_num t205f_part_process_validation.c205_part_number_id%TYPE,
 p_vendor t205f_part_process_validation.c301_vendor_id%TYPE)
RETURN CHAR
IS
/*  Description     : THIS FUNCTION RETURNS VALIDATED STATUS 
 */
v_sts_fl  char(1);
BEGIN
     BEGIN
         SELECT DECODE(count(1),0,'N','Y') INTO v_sts_fl FROM  t205f_part_process_validation t205f WHERE t205f.c205_part_number_id=p_part_num 
                AND t205f.c901_type=101882 AND  t205f.c301_vendor_id=p_vendor AND t205f.c205f_void_fl is null;
   EXCEPTION
      WHEN NO_DATA_FOUND
   THEN
      RETURN 'N';
      END;
     RETURN v_sts_fl;
END GET_VALIDATED_STATUS;
/