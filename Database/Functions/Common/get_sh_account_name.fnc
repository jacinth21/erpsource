/* Formatted on 2007/09/28 14:30 (Formatter Plus v4.8.0) */
-- @"C:\Database\Functions\Common\get_sh_account_name.fnc";
CREATE OR REPLACE FUNCTION get_sh_account_name (
	p_acc_id   t704_account.c704_account_id%TYPE
)
	RETURN VARCHAR2
IS
/*	Description 	: THIS FUNCTION RETURNS ACCOUNT SHORT NAMR GIVEN THE ACC ID
 Parameters 		: p_acc_id
*/
	v_acc_nm	   t704_account.c704_account_sh_name%TYPE;
BEGIN
	BEGIN
		SELECT nvl(c704_account_sh_name,c704_account_nm)
		  INTO v_acc_nm
		  FROM t704_account
		 WHERE c704_account_id = p_acc_id;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN '';
	END;

	RETURN v_acc_nm;
END get_sh_account_name;
/