/* Formatted on 2009/05/18 12:45 (Formatter Plus v4.8.0) */
--@"c:\database\functions\common\get_party_name.fnc"

/********************************************************************
 * Description : This function returns the party name for the given party id
 * Created By Vprasath
 ********************************************************************/
--

CREATE OR REPLACE FUNCTION get_party_name (
	p_id   IN	t101_party.c101_party_id%TYPE
)
	RETURN VARCHAR2
IS
	v_party_name   VARCHAR2 (100);
	v_comp_lang_id T1900_COMPANY.C901_LANGUAGE_ID%TYPE;
--
BEGIN
SELECT NVL(get_complangid_frm_cntx(),GET_RULE_VALUE('DEFAULT_LANGUAGE','ONEPORTAL')) INTO  v_comp_lang_id FROM DUAL;
     BEGIN
	SELECT DECODE (v_comp_lang_id,'103097',NVL(C101_PARTY_NM_EN,DECODE(c101_party_nm
				 , NULL, c101_first_nm || ' ' || c101_last_nm || ' ' || c101_middle_initial
				 , c101_party_nm)
				  ),DECODE(c101_party_nm
				 , NULL, c101_first_nm || ' ' || c101_last_nm || ' ' || c101_middle_initial
				 , c101_party_nm))
			
	  INTO v_party_name
	  FROM t101_party
	 WHERE c101_party_id = p_id;


EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		RETURN '';
	END;
		RETURN v_party_name;
END get_party_name;
/
