/* Formatted on 2010/10/04 12:20 (Formatter Plus v4.8.0) */
-- @"C:\Database\Functions\Common\get_mapping_countryid.fnc";

/*	Description 	: THIS FUNCTION RETURNS country id mapped for EU countries
 Parameters 	  : p_country_id
*/

CREATE OR REPLACE FUNCTION get_mapping_countryid (
	p_country_id   t901_code_lookup.c901_code_id%TYPE
)
	RETURN NUMBER
IS
	v_country_id   NUMBER;
BEGIN
	SELECT c901_access_code_id
	  INTO v_country_id
	  FROM t901a_lookup_access
	 WHERE c901_code_id = p_country_id;

	RETURN v_country_id;
EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		RETURN 0;
END get_mapping_countryid;
/