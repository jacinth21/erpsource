/* Formatted on 2009/12/28 15:14 (Formatter Plus v4.8.0) */
--@"c:\database\functions\common\get_user_phoneno.fnc";

CREATE OR REPLACE FUNCTION get_user_phoneno (
	p_user_id	t101_user.c101_user_id%TYPE
)
	RETURN VARCHAR2
IS
	v_user_phone   t101_user.c101_work_phone%TYPE;
BEGIN
	SELECT c101_work_phone
	  INTO v_user_phone
	  FROM t101_user
	 WHERE c101_user_id = p_user_id;

	RETURN v_user_phone;
EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		RETURN '';
END;
/
