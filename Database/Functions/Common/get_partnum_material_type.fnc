/* Formatted on 2010/09/27 10:28 (Formatter Plus v4.8.0) */
CREATE OR REPLACE FUNCTION get_partnum_material_type (
   p_part_num   t205_part_number.c205_part_number_id%TYPE
)
   RETURN VARCHAR2
IS
/* Description    : THIS FUNCTION RETURNS Material Type for the given part number
 Parameters       : p_part_num
*/
   v_part_material   t205_part_number.c205_product_material%TYPE;
BEGIN
   SELECT c205_product_material
     INTO v_part_material
     FROM t205_part_number
    WHERE c205_part_number_id = p_part_num;

   RETURN v_part_material;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      RETURN '';
END get_partnum_material_type;
/
