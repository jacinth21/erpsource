CREATE OR REPLACE FUNCTION GET_NPI_ID (
	p_party_id   t6600_party_surgeon.c101_party_surgeon_id%TYPE
)
	RETURN NUMBER
IS
/*	Description 	: THIS FUNCTION RETURNS NPI IS FOR GIVEN PARTY ID
 Parameters   : p_party_id
*/
	v_npi_id	   NUMBER;
BEGIN
--
	SELECT c6600_surgeon_npi_id
	  INTO v_npi_id
	  FROM t6600_party_surgeon
	 WHERE c101_party_surgeon_id = p_party_id
	   AND c6600_void_fl is null;

	--
	RETURN v_npi_id;
--
EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		RETURN NULL;
END get_npi_id;
/