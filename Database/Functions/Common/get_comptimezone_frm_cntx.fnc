--@"C:\Database\Functions\Common\get_comptimezone_frm_cntx.fnc"
CREATE OR REPLACE
    FUNCTION get_comptimezone_frm_cntx
        RETURN VARCHAR2
    IS
        /* Description  : THIS FUNCTION RETURNS COMPANY TIME ZONE FROM CONTEXT
        */
        v_time_zone VARCHAR2 (200);
    BEGIN
        BEGIN
            SELECT SYS_CONTEXT('CLIENTCONTEXT', 'COMP_TIME_ZONE')  INTO v_time_zone
            FROM DUAL;
            
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_time_zone := '';
        END;
        RETURN v_time_zone;
    END get_comptimezone_frm_cntx;
    /