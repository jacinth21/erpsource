/* Formatted on 2009/03/07 10:42 (Formatter Plus v4.8.0) */
--@"c:\database\functions\common\get_user_emailid.fnc";

CREATE OR REPLACE FUNCTION get_user_emailid (
	p_user_id	t101_user.c101_user_id%TYPE
)
	RETURN VARCHAR2
IS
	v_user_nm	   t101_user.c101_email_id%TYPE;
BEGIN
	SELECT c101_email_id
	  INTO v_user_nm
	  FROM t101_user
	 WHERE c101_user_id = p_user_id;

	RETURN v_user_nm;
EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		-- PMT-53446: To return the default user email id
		RETURN get_rule_value('DEFAULT_USER_EMAIL','EMAIL');
END;
/
