--@"c:\database\functions\common\get_acct_region_by_dealer.fnc";
/* 
 *  Description     : This function is used to get the account region by dealer
 */
CREATE OR REPLACE
  FUNCTION get_acct_region_by_dealer(
      p_dealer_id IN t704_account.c101_dealer_id%TYPE)
    RETURN VARCHAR2
  IS
    v_region VARCHAR2(20);
  BEGIN
  	
  	 SELECT distinct v700.region_id
  	  INTO v_region
      FROM t704_account t704,v700_territory_mapping_detail v700 
     WHERE t704.c704_account_id = v700.ac_id
       AND t704.c101_dealer_id = p_dealer_id
       AND t704.c704_void_fl IS NULL;
       
    RETURN v_region;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
  	v_region := '';
    RETURN v_region;
  END get_acct_region_by_dealer;
  /
