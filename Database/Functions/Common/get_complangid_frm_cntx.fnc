--@"C:\Database\Functions\Common\get_complangid_frm_cntx.fnc"
CREATE OR REPLACE
    FUNCTION get_complangid_frm_cntx
        RETURN VARCHAR2
    IS
        /* Description  : THIS FUNCTION RETURNS Multilingual COMPANY Language FROM CONTEXT 
        */
        v_comp_lang_id T1900_COMPANY.C901_LANGUAGE_ID%TYPE;
    BEGIN
        BEGIN
            SELECT SYS_CONTEXT('CLIENTCONTEXT', 'COMP_LANG_ID')  INTO v_comp_lang_id
            FROM DUAL;
            
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_comp_lang_id := '103097';
        END;
        RETURN v_comp_lang_id;
    END get_complangid_frm_cntx;
    /