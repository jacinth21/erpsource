--@"C:\Database\Functions\Common\get_plant_name.fnc"
CREATE OR REPLACE
    FUNCTION get_plant_name(
    	P_PLANT_ID   T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE
    )
        RETURN VARCHAR2
    IS
        /* Description  : THIS FUNCTION RETURNS PLANT NAME
        */
        v_plant_name T5040_PLANT_MASTER.C5040_PLANT_NAME%TYPE;
    BEGIN
        BEGIN
            SELECT C5040_PLANT_NAME  INTO v_plant_name
            FROM T5040_PLANT_MASTER
            WHERE C5040_VOID_FL IS NULL
            AND C5040_PLANT_ID = P_PLANT_ID;
            
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_plant_name := '';
        END;
        RETURN v_plant_name;
    END get_plant_name;
    /