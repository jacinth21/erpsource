--@"C:\Database\Functions\Common\get_company_name.fnc"
CREATE OR REPLACE
    FUNCTION get_company_name(
    	p_comp_id   T1900_COMPANY.C1900_COMPANY_ID%TYPE
    )
        RETURN VARCHAR2
    IS
        /* Description  : THIS FUNCTION RETURNS COMPANY NAME
        */
        v_comp_nm T1900_COMPANY.C1900_COMPANY_NAME%TYPE;
    BEGIN
        BEGIN
            SELECT C1900_COMPANY_NAME  INTO v_comp_nm
            FROM T1900_COMPANY
            WHERE C1900_VOID_FL IS NULL
            AND C1900_COMPANY_ID = p_comp_id;
            
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_comp_nm := '';
        END;
        RETURN v_comp_nm;
    END get_company_name;
    /