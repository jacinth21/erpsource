/*
 * Author		: Rajkumar
 * Description  : To fetch the Transaction details for Location Transfer, this procedure fetch the Address Based on the Transaction 
 */
	CREATE OR REPLACE FUNCTION "GET_TRANS_LOCATION_ADDRESS" (
	 	p_transid	t5055_control_number.c5055_ref_id%TYPE
  	   ,p_type	 	t5055_control_number.C901_REF_TYPE%TYPE
	)
	RETURN VARCHAR2
	IS
	v_trans_code	VARCHAR2(20);
	v_loc_id		VARCHAR2(20);
	v_address		VARCHAR2 (1000);
	v_substring 	VARCHAR2(200);
	v_add_id		VARCHAR2(20);
	BEGIN 
		-- Fetching the Transaction Type	
		v_trans_code := NVL(GET_TRANS_LOCATION_DETAIL(p_transid,p_type),'');
		
		SELECT max(loc_id) INTO v_loc_id FROM (
		    SELECT t5055.c5052_location_id loc_id 
		          ,t5055.c901_type type
			  FROM t5055_control_number t5055,t5052_location_master t5052 
			 WHERE t5052.c5052_location_id = t5055.c5052_location_id 
			   AND t5055.c901_type IN ('93344') 
			   AND t5055.c5055_ref_id = p_transid 
			   AND t5055.C901_REF_TYPE = p_type 
			   AND t5052.c5051_inv_warehouse_id NOT IN
			    (
			         SELECT c906_rule_value
			           FROM t906_rules
			          WHERE c906_rule_id     = 'WHID'
			            AND c906_rule_grp_id = 'RULEWH'
			            AND c906_void_fl    IS NULL
			    )--excluding FS and account warehouse
			   AND t5052.c5052_void_fl IS NULL 
			   AND t5055.c5055_void_fl IS NULL
		)
		GROUP BY TYPE 
		ORDER BY TYPE;
		IF INSTR (v_loc_id, '-') <> 0
		THEN
			WHILE INSTR (v_loc_id, '-') <> 0
 				LOOP
 				v_substring := SUBSTR (v_loc_id, 1, INSTR (v_loc_id, '-') - 1);
 				v_loc_id := SUBSTR (v_loc_id, INSTR (v_loc_id, '-') + 1);
  			END LOOP;
  		END IF;
	IF v_trans_code IN ('903108','903115','903119','903122') -- For Sales Rep Address
	THEN
		BEGIN	
			SELECT C106_ADDRESS_ID INTO v_add_id 
			  FROM T106_ADDRESS t106,t703_sales_rep t703
			 WHERE t106.C101_PARTY_ID = t703.c101_party_id 
               AND t703.c703_sales_rep_id = v_loc_id 
               AND t703.c703_void_fl IS NULL
               AND t106.C106_VOID_FL IS NULL;
			EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
			v_add_id :='0';			
		END;
		v_address := GM_PKG_CM_SHIPPING_INFO.get_address('4121',v_add_id);
	ELSIF v_trans_code IN ('903109','903112','903118','903121') -- For Hospital Address
	THEN
		v_address := GM_PKG_CM_SHIPPING_INFO.get_address('4122',v_loc_id);
	ELSE
		v_address := 'Globus Med-In House';
	END IF;
		v_address := REPLACE(v_address,';','');	 	
	dbms_output.put_line('v_address'||v_address);
	RETURN v_address;
END;
/