--@"C:\Database\Functions\Common\get_comp_curr_symbol.fnc"
CREATE OR REPLACE
    FUNCTION get_comp_curr_symbol(
    	p_comp_id   T1900_COMPANY.C1900_COMPANY_ID%TYPE
    )
        RETURN VARCHAR2
        IS
        /* Description  : THIS FUNCTION RETURNS COMPANY NAME
        */
        v_comp_curr T901_CODE_LOOKUP.C901_CODE_NM%TYPE;
          BEGIN
        BEGIN
          SELECT get_code_name(t1900.c901_txn_currency) 
          INTO v_comp_curr
          FROM t1900_company t1900,t901_code_lookup t901 
          WHERE t1900.c1900_company_id= p_comp_id
        AND t1900.c901_txn_currency = t901.c901_code_id
        AND t1900.c1900_void_fl is null AND t901.c901_void_fl is null;
            
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_comp_curr := '';
        END;
        RETURN v_comp_curr;
    END get_comp_curr_symbol;
    /