--@"C:\Database\Functions\Common\get_division_name.fnc"
CREATE OR REPLACE
    FUNCTION get_division_name(
    	P_DIVISION_ID   T1910_DIVISION.C1910_DIVISION_ID%TYPE
    )
        RETURN VARCHAR2
    IS
        /* Description  : This Function Returns the Division Name for the Passed in Division ID.
        */
        v_division_name T1910_DIVISION.C1910_DIVISION_NAME%TYPE;
    BEGIN
        BEGIN
            SELECT C1910_DIVISION_NAME  INTO v_division_name
            FROM T1910_DIVISION
            WHERE C1910_VOID_FL IS NULL
            AND C1910_DIVISION_ID = P_DIVISION_ID;
            
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_division_name := '';
        END;
        RETURN v_division_name;
    END get_division_name;
    /