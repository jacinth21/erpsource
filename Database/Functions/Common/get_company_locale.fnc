--@"C:\Database\Functions\Common\get_company_locale.fnc"

CREATE OR REPLACE
    FUNCTION get_company_locale(
    	p_comp_id   T1900_COMPANY.C1900_COMPANY_ID%TYPE
    )
        RETURN VARCHAR2
    IS
        /* Description  : THIS FUNCTION RETURNS COMPANY code for the passed in Company ID.
        */
        v_comp_locale T1900_COMPANY.C1900_COMPANY_LOCALE%TYPE;
    BEGIN
        BEGIN
            SELECT c1900_company_locale  INTO v_comp_locale
            FROM T1900_COMPANY
            WHERE C1900_VOID_FL IS NULL
            AND C1900_COMPANY_ID = p_comp_id;
            
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_comp_locale := '';
        END;
        RETURN v_comp_locale;
    END  get_company_locale;
    /
    