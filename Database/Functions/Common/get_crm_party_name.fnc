/* Formatted on 2009/05/18 12:45 (Formatter Plus v4.8.0) */
--@"c:\database\functions\common\get_crm_party_name.fnc"

/********************************************************************
 * Description : This function returns the party name with salutation for the given party id
 * Created By jgurunathan
 ********************************************************************/
--

CREATE OR REPLACE FUNCTION get_crm_party_name (
	p_id   IN	t101_party.c101_party_id%TYPE
)
	RETURN VARCHAR2
IS
	v_party_name   VARCHAR2 (100);
	v_comp_lang_id T1900_COMPANY.C901_LANGUAGE_ID%TYPE;
--
BEGIN
SELECT NVL(get_complangid_frm_cntx(),GET_RULE_VALUE('DEFAULT_LANGUAGE','ONEPORTAL')) INTO  v_comp_lang_id FROM DUAL;
     BEGIN
        SELECT DECODE (v_comp_lang_id,'103097',NVL(t101.C101_PARTY_NM_EN,DECODE(t101.c101_party_nm , NULL,GET_CODE_NAME(npid.salute)
          ||' '
          || t101.c101_first_nm
          || ' '
          || t101.c101_middle_initial
          || ' '
          || t101.c101_last_nm , t101.c101_party_nm) ),DECODE(c101_party_nm , NULL,GET_CODE_NAME(npid.salute)
          ||' '
          || t101.c101_first_nm
          || ' '
          || t101.c101_middle_initial
          || ' '
          || t101.c101_last_nm , t101.c101_party_nm))
        INTO v_party_name
        FROM t101_party t101,
          (SELECT c6600_surgeon_npi_id npid ,
            c101_party_surgeon_id partyid,
            c6600_party_salute salute
          FROM t6600_party_surgeon
          WHERE c6600_void_fl IS NULL
          ) npid
        WHERE t101.c101_party_id = p_id
        AND t101.c101_party_id   = npid.partyid(+);
EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		RETURN '';
	END;
		RETURN v_party_name;
END get_crm_party_name;
/
