/* Formatted on 2010/02/18 15:29 (Formatter Plus v4.8.0) */
--"C:\Database\Functions\Sales\GET_DEMO_DATA.fnc";
CREATE OR REPLACE FUNCTION get_demo_data (
    p_reqtype IN VARCHAR2
	,p_request_id   IN	t7000_account_price_request.c7000_account_price_request_id%TYPE
  , p_groupid	   IN	t7001_group_part_pricing.c7001_ref_id%TYPE
  
)
	RETURN VARCHAR2
IS
/*
 Description  : this function returns demo price, fromdate and todate
 Parameters  : p_reqtype
 Parameters  : p_request_id,p_groupid
*/
	v_date   t7001_group_part_pricing.c7001_effective_date%TYPE;
  
BEGIN
	--
	BEGIN
		IF (p_reqtype='FromDt')
    THEN
      SELECT t7001.c7001_effective_date
          INTO v_date
        FROM t7001_group_part_pricing t7001
        WHERE c7000_account_price_request_id = p_request_id
          AND c7001_ref_id = p_groupid
          AND c7001_demo_flg  = 1;
          
         
    ELSIF (p_reqtype='ToDt')
     THEN
     
      SELECT t7001.c7001_effective_to_date
          INTO v_date
        FROM t7001_group_part_pricing t7001
        WHERE c7000_account_price_request_id = p_request_id
          AND c7001_ref_id = p_groupid
          AND c7001_demo_flg  = 1;
        
		END IF;
	END;
return TO_CHAR(v_date,'MM/DD/YYYY');
   EXCEPTION
          WHEN NO_DATA_FOUND
          THEN
           RETURN '';
--

END get_demo_data;
/
