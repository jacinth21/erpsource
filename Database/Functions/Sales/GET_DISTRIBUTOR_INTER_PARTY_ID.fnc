/* Formatted on 2008/06/20 09:46 (Formatter Plus v4.8.0) */
CREATE OR REPLACE FUNCTION get_distributor_inter_party_id (
	p_distributorid   t504_consignment.c701_distributor_id%TYPE
)
	RETURN NUMBER
IS
/*	Description 	: THIS FUNCTION RETURNS PARTYID
 Parameters 	   : p_distributorId
*/
	v_inter_party_id t701_distributor.c101_party_intra_map_id%TYPE;
BEGIN
	SELECT c101_party_intra_map_id
	  INTO v_inter_party_id
	  FROM t701_distributor t701
	 WHERE t701.c701_distributor_id = p_distributorid;

	RETURN v_inter_party_id;
EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		RETURN NULL;
END get_distributor_inter_party_id;
/
