--@"C:\Database\Functions\Sales\get_distributor_division.fnc";
/*
 * Description: This function is used to fetch the distributor divison (US or OUS)
 * Author: Rajkumar J
 * 100823 - US
 * 100824 - OUS
 */

CREATE OR REPLACE FUNCTION get_distributor_division (
   p_dist_id	  t101_user.c101_user_id%TYPE
)
	RETURN V700_TERRITORY_MAPPING_DETAIL.DIVID%TYPE
IS
	v_divid		V700_TERRITORY_MAPPING_DETAIL.DIVID%TYPE := 100823;-- US Distributor;
	v_comp_id	NUMBER;
BEGIN

	SELECT get_compid_frm_cntx () 
		 INTO v_comp_id 
		 FROM DUAL;
	BEGIN
		SELECT DISTINCT V700.DIVID INTO v_divid
	   	FROM T701_DISTRIBUTOR T701, V700_TERRITORY_MAPPING_DETAIL V700
	  		WHERE V700.D_ID                = T701.C701_DISTRIBUTOR_ID
	    	AND T701.C701_DISTRIBUTOR_ID = p_dist_id
	    	AND T701.C701_VOID_FL       IS NULL;
    EXCEPTION
    WHEN NO_DATA_FOUND
    THEN
    	v_divid := NVL(get_rule_value_by_company('DEFAULT', 'DIVISION_FILTER',v_comp_id),100823);-- By Default US Distributor
    END;
    
    RETURN v_divid;
END get_distributor_division;
/