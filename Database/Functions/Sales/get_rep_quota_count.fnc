--@"C:\Database\Functions\Sales\get_rep_quota_count.fnc"

CREATE OR REPLACE FUNCTION get_rep_quota_count
(p_rep_id t709_quota_breakup.C703_SALES_REP_ID%TYPE)
RETURN NUMBER
IS
v_count   NUMBER;
BEGIN
     BEGIN
	 	SELECT COUNT(1) 
	 	  INTO v_count
	 	  FROM t709_quota_breakup 
	 	 WHERE C703_SALES_REP_ID   = p_rep_id
	 	   AND c709_quota_breakup_amt IS NOT NULL
	 	   AND TRUNC(c709_start_date, 'YEAR') = TRUNC(SYSDATE, 'YEAR');
   EXCEPTION
      WHEN NO_DATA_FOUND
   THEN
      RETURN 0;
      END;
     RETURN v_count;
END get_rep_quota_count;
/