CREATE OR REPLACE FUNCTION GET_DIST_SET_CONSIGN_COUNT
(
   p_set_id T504_CONSIGNMENT.C207_SET_ID%TYPE
 ,p_dist_id T504_CONSIGNMENT.C701_DISTRIBUTOR_ID%TYPE
)
RETURN NUMBER
IS
/**********************************************************************************
 *  Description     : THIS FUNCTION RETURNS CONSIGNMENT COUNT INFORMATION FOR THE
 *       SELECTED SET ID AND FOR THE SELECTED DISTRIBUTOR
 *  Parameters   : p_set_id  Set ID
 *       p_dist_id  Distributor ID
 ***********************************************************************************/
 --v_acc_nm VARCHAR2(10);
 v_set_cnt   NUMBER;
 v_set_consign_cnt NUMBER;
 v_set_return_cnt NUMBER;
BEGIN
 --
 SELECT count(1)
 INTO v_set_consign_cnt
 FROM T504_CONSIGNMENT
 WHERE C207_SET_ID = p_set_id
 AND  C701_DISTRIBUTOR_ID = NVL(p_dist_id,C701_DISTRIBUTOR_ID)
 AND  C504_STATUS_FL = '4'
 AND C504_VOID_FL IS NULL;
 --
 SELECT count(1)
 INTO v_set_return_cnt
 FROM T506_RETURNS
 WHERE C207_SET_ID = p_set_id
 AND  C701_DISTRIBUTOR_ID = NVL(p_dist_id,C701_DISTRIBUTOR_ID)
 AND  C506_STATUS_FL = '2'
 AND C506_VOID_FL IS NULL;
 --
 --
 v_set_cnt := v_set_consign_cnt - v_set_return_cnt;
 RETURN v_set_cnt;
 --
EXCEPTION
WHEN NO_DATA_FOUND
THEN
 RETURN NULL;
END GET_DIST_SET_CONSIGN_COUNT;
/

