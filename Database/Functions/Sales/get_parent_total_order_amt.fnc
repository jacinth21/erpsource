/* Formatted on 2009/05/30 19:24 (Formatter Plus v4.8.0) */
CREATE OR REPLACE FUNCTION get_parent_total_order_amt (
	p_ord_id	t501_order.c501_order_id%TYPE
  , p_from_dt	DATE
  , p_to_dt 	DATE
)
	RETURN NUMBER
IS
/*	Description 	: THIS FUNCTION RETURNS CODE NAME GIVEN THE CODE_ID
 Parameters   : p_code_id
*/
	v_total 	   NUMBER (15, 2);
BEGIN
	SELECT SUM (c501_total_cost)
	  INTO v_total
	  FROM t501_order
	 WHERE (c501_order_id = p_ord_id OR c501_parent_order_id = p_ord_id)
	   AND c501_order_date >= p_from_dt
	   AND p_to_dt <= p_to_dt;

	RETURN v_total;
EXCEPTION
	WHEN OTHERS
	THEN
		RETURN 0;
END get_parent_total_order_amt;
/
