--@"C:\Database\Functions\Sales\get_rep_id_from_party_id.fnc"
/**********************************************************************
        * Description     : Functions returns rep id based on party id
        *  Parameters  :p_party_id
        ***********************************************************************/
CREATE OR REPLACE
    FUNCTION get_rep_id_from_party_id (
            p_party_id t101_user.c101_party_id%TYPE)
        RETURN VARCHAR2
    IS
        
        v_rep_id T703_SALES_REP.C703_SALES_REP_ID%TYPE;
    BEGIN
         SELECT t703.c703_sales_rep_id
           INTO v_rep_id
           FROM  T703_SALES_REP T703
          WHERE T703.C101_PARTY_ID  = p_party_id
            AND T703.C703_VOID_FL  IS NULL;
        RETURN v_rep_id;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN NULL;
    END;
/