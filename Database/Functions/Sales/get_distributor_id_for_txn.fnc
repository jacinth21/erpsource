--Get the Distributor Id for given transaction
--@"c:\database\Functions\Operations\get_distributor_id_for_txn.fnc
create or replace
FUNCTION get_distributor_id_for_txn (
   p_txn_id	  t505_item_consignment.c504_consignment_id%TYPE
)
	RETURN T701_DISTRIBUTOR.C701_DISTRIBUTOR_ID%TYPE
IS
  v_dist_id   T701_DISTRIBUTOR.C701_DISTRIBUTOR_ID%TYPE;
  BEGIN
    BEGIN
	 	  SELECT C701_DISTRIBUTOR_ID
		  INTO v_dist_id
		  FROM t504_consignment
		  WHERE c504_consignment_id = p_txn_id;
   EXCEPTION
      WHEN NO_DATA_FOUND
   THEN
      RETURN -1;
      END;
  RETURN v_dist_id;
END get_distributor_id_for_txn;
/