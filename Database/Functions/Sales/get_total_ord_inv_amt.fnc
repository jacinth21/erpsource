-- @"C:\Database\Functions\Sales\get_total_ord_inv_amt.fnc";
CREATE OR REPLACE FUNCTION get_total_ord_inv_amt (
            p_ord_id T501_ORDER.C501_ORDER_ID%TYPE)
            
        RETURN NUMBER
    IS
        /*  Description     : THIS FUNCTION RETURNS The Total Invoice Cost
        	Parameters   : Order ID
        */
        v_total NUMBER (15, 2) ;
    BEGIN
         SELECT SUM (NVL (C501_TOTAL_COST, 0)) + SUM (NVL (c501_ship_cost, 0)) + SUM (NVL (c501_tax_total_cost, 0))
           INTO v_total
           FROM T501_ORDER
          WHERE (C501_ORDER_ID                     = p_ord_id
            OR C501_PARENT_ORDER_ID                = p_ord_id)
            AND NVL (c901_order_type, '-999') NOT IN
            (
                 SELECT c906_rule_id
                   FROM t906_rules
                  WHERE c906_rule_grp_id = 'DOEXORDERTYPE'
                    AND c906_void_fl    IS NULL
            )
            AND C501_VOID_FL IS NULL;
        RETURN v_total;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN 0;
END get_total_ord_inv_amt;
/
