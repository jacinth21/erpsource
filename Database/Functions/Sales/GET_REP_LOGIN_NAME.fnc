--@"C:\Database\Functions\Sales\GET_REP_LOGIN_NAME.FNC";

CREATE OR REPLACE FUNCTION GET_REP_LOGIN_NAME
(p_rep_id T703_SALES_REP.C703_SALES_REP_ID%TYPE)
RETURN VARCHAR2
IS
/*  Description     : THIS FUNCTION RETURNS REP LOGIN NAME
 Parameters 		: p_rep_id
*/
v_rep_login_nm   t102_user_login.c102_login_username%TYPE;
BEGIN
     BEGIN
	 	  SELECT t102.c102_login_username 
		  INTO v_rep_login_nm 
   			FROM t102_user_login t102, t101_user t101, t703_sales_rep t703
  			WHERE t101.c101_party_id = t703.c101_party_id
    		AND t102.c101_user_id  = t101.c101_user_id
    		AND t703.c703_sales_rep_id = p_rep_id;
    		-- void flag not added to fetch voided record also
   EXCEPTION
      WHEN NO_DATA_FOUND
   THEN
      RETURN NULL;
      END;
     RETURN v_rep_login_nm;
END GET_REP_LOGIN_NAME;
/

