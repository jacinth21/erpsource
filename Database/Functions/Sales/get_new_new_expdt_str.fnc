--@"C:\projects\Branches\jboss-migration\Database\Functions\Sales\get_new_new_expdt_str.fnc";
CREATE OR REPLACE
    FUNCTION get_new_new_expdt_str (
            p_extn_date IN t504a_consignment_loaner.c504a_expected_return_dt%TYPE)
        RETURN VARCHAR2
    IS
        v_new_expdt_str VARCHAR2 (200) ;
        v_new_exprtdate t504a_loaner_transaction.c504a_expected_return_dt%TYPE;
        v_rule_date_fmt VARCHAR2 (20) ;
        CURSOR extend_day_details
        IS
             SELECT C901_CODE_ID CODEID, C901_CODE_NM CODENM, C902_CODE_NM_ALT CODENMALT
              , C901_CONTROL_TYPE CONTROLTYP, C901_CODE_SEQ_NO CODESEQNO
               FROM T901_CODE_LOOKUP
              WHERE C901_CODE_GRP IN ('EXTDAY')
                AND C901_ACTIVE_FL = '1'
           ORDER BY C901_CODE_SEQ_NO ;
    BEGIN
         SELECT NVL (get_rule_value ('DATEFMT', 'DATEFORMAT'), 'MM/dd/yyyy')
           INTO v_rule_date_fmt
           FROM DUAL;
        FOR extnDay_Details IN extend_day_details
        LOOP
            v_new_exprtdate     := get_new_expected_return_dt (p_extn_date, extnDay_Details.CODENMALT) ;
            IF v_new_exprtdate  IS NOT NULL THEN
                v_new_expdt_str := v_new_expdt_str || TO_CHAR (v_new_exprtdate, v_rule_date_fmt) || ',';
            END IF;
        END LOOP;
        
        IF v_new_expdt_str IS NOT NULL
        THEN
        v_new_expdt_str	:= SUBSTR (v_new_expdt_str, 1, length (v_new_expdt_str)-1);
        END IF;
        
        RETURN v_new_expdt_str;
    END get_new_new_expdt_str;
/