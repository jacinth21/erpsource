/* Formatted on 2012/02/17 12:16 (Formatter Plus v4.8.0) */
--@"C:\Database\Functions\Sales\get_division_id.fnc";

CREATE OR REPLACE FUNCTION get_division_id (
	p_accesslvl   t101_user.c101_access_level_id%TYPE
  , p_deptid	  t101_user.c901_dept_id%TYPE
  , p_user_id	  t101_user.c101_user_id%TYPE
  , p_company_id  T1900_COMPANY.C1900_COMPANY_ID%TYPE
)
	RETURN v700_territory_mapping_detail.divid%TYPE
IS
/*	Description 	: THIS FUNCTION RETURNS division id
*/
	v_div_id	   v700_territory_mapping_detail.divid%TYPE;
	v_comp_id		NUMBER;
	
BEGIN
		SELECT get_compid_frm_cntx () 
		 INTO v_comp_id 
		 FROM DUAL;
		 
	v_div_id	:= NVL(get_rule_value_by_company('DEFAULT', 'DIVISION_FILTER',p_company_id),100823);	 -- US Division

	IF p_deptid = 2005
	THEN   -- Sales
		BEGIN
			IF p_accesslvl = 1
			THEN
				SELECT DISTINCT v700.divid
						   INTO v_div_id
						   FROM v700_territory_mapping_detail v700
						  WHERE v700.rep_id = p_user_id;
			ELSIF p_accesslvl = 2
			THEN
				SELECT DISTINCT v700.divid
						   INTO v_div_id
						   FROM v700_territory_mapping_detail v700
						  WHERE v700.d_id IN (SELECT c701_distributor_id
												FROM t101_user
											   WHERE c101_user_id = p_user_id);
			ELSIF p_accesslvl = 3
			THEN
				SELECT DISTINCT v700.divid
						   INTO v_div_id
						   FROM v700_territory_mapping_detail v700
						  WHERE v700.ad_id = p_user_id;
			ELSIF p_accesslvl IN (4, 6)
			THEN
				SELECT DISTINCT v700.divid
						   INTO v_div_id
						   FROM v700_territory_mapping_detail v700
						  WHERE v700.vp_id = p_user_id;
			ELSIF p_accesslvl = 7
			THEN
				SELECT DISTINCT v700.divid
						   INTO v_div_id
						   FROM v700_territory_mapping_detail v700
						  WHERE v700.ac_id IN (SELECT c704_account_id
												 FROM t704a_account_rep_mapping
												WHERE c704a_void_fl IS NULL AND c703_sales_rep_id = p_user_id);
			END IF;
		EXCEPTION
			WHEN OTHERS
			THEN
				v_div_id	:= NVL(get_rule_value_by_company('DEFAULT', 'DIVISION_FILTER',p_company_id),100823);	 -- US Division
		END;
	END IF;

	RETURN v_div_id;
END get_division_id;
/
