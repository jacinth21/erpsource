--@"C:\Database\Functions\Sales\get_group_current_prc_cons.fnc";

CREATE OR REPLACE FUNCTION get_group_current_prc_cons
(
	  p_acc_id		T704_ACCOUNT.C704_ACCOUNT_ID%TYPE
	 ,p_gpo_id 		T101_USER.C101_PARTY_ID%TYPE
	 ,p_group_id 	T7051_ACCOUNT_GROUP_PRICING.C7051_REF_ID%TYPE

)
RETURN NUMBER
IS

/*  Description     : THIS FUNCTION RETURNS THE GROUP CURRENT PRICE
 	Parameters 		: p_group_id, p_acc_id, p_gop_id
*/
	v_current_prc T7051_ACCOUNT_GROUP_PRICING.C7051_PRICE%TYPE;
	
	BEGIN
		BEGIN
			SELECT NVL(SUM(t7004.C7004_QTY * T7051.C7051_PRICE),0)  INTO v_current_prc  
			FROM T7051_ACCOUNT_GROUP_PRICING T7051,T7004_CONSTRUCT_MAPPING T7004
			WHERE T7051.C901_REF_TYPE = '52000' --'GROUP'
      		AND T7051.C7051_REF_ID = T7004.C4010_GROUP_ID
			AND T7051.C7051_REF_ID = TO_CHAR(p_group_id)
			AND T7051.C704_ACCOUNT_ID = p_acc_id
			AND	NVL(T7051.C101_GPO_ID,'-999') = NVL(p_gpo_id,'-999')
			AND T7051.C7051_ACTIVE_FL = 'Y';
      
 	IF (v_current_prc = 0)
 	THEN
	 select MIN(c705_unit_price) INTO v_current_prc
		from 
			t705_account_pricing where c205_part_number_id in (       
				select c205_part_number_id 
						from 
							t4011_group_detail 
						where 
							c4010_group_id=p_group_id	and 
							c901_part_pricing_type = 52080) and 
					NVL(c101_party_id,'-999') = NVL(p_gpo_id,'-999') and 
					NVL(c704_account_id,'-999')=NVL(p_acc_id,'-999');
		
		SELECT SUM(t7004.C7004_QTY * v_current_prc)  INTO v_current_prc  
		FROM T7004_CONSTRUCT_MAPPING T7004
		WHERE T7004.C4010_GROUP_ID = p_group_id;
  		
	END IF;
      		RETURN v_current_prc;
      	END;
   RETURN v_current_prc;
END get_group_current_prc_cons;
/
