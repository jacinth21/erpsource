--@"C:\Database\Functions\Sales\get_rep_id_by_ass_id.fnc";
CREATE OR REPLACE FUNCTION get_rep_id_by_ass_id (
	p_rep_id   t703_sales_rep.c703_sales_rep_id%TYPE
)
	RETURN VARCHAR2
IS
v_rep_id   T704_ACCOUNT.C703_SALES_REP_ID%TYPE;
BEGIN
     BEGIN
		SELECT DISTINCT GET_REP_ID (C704_ACCOUNT_ID)
		  INTO v_rep_id
          FROM T704A_ACCOUNT_REP_MAPPING
         WHERE C703_SALES_REP_ID = p_rep_id
           AND C704A_VOID_FL IS NULL
           AND rownum            = 1;
   EXCEPTION
      WHEN NO_DATA_FOUND
   THEN
      RETURN '-999'; -- Added for PMT-49806 changes
      END;
     RETURN v_rep_id;
END get_rep_id_by_ass_id;
/
