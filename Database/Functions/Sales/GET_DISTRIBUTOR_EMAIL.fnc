/* Formatted on 2009/03/04 17:27 (Formatter Plus v4.8.0) */
CREATE OR REPLACE FUNCTION get_distributor_email (
	p_dist_id	t701_distributor.c701_distributor_id%TYPE
)
	RETURN VARCHAR2
IS
/*	Description 	: THIS FUNCTION RETURNS CODE NAME GIVEN THE CODE_ID
 Parameters 		: p_code_id
*/
	v_dist_emailid t701_distributor.c701_email_id%TYPE;
BEGIN
	BEGIN
		SELECT c701_email_id
		  INTO v_dist_emailid
		  FROM t701_distributor
		 WHERE c701_distributor_id = p_dist_id;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN '';
	END;

	RETURN v_dist_emailid;
END get_distributor_email;
