/* Formatted on 2010/05/19 17:22 (Formatter Plus v4.8.0) */
--@"C:\DATABASE\Functions\Sales\get_GRP_PART_PR_EDIT_access.fnc"

CREATE OR REPLACE FUNCTION get_grp_part_pr_edit_access (
	p_groupid	IN	 t4010_group.c4010_group_id%TYPE
  , p_partnum	IN	 VARCHAR2
  , p_userid	IN	 VARCHAR2
)
	RETURN NUMBER
IS
/*
 Description  : this function returns price for group , list, AD, VP , TripWire
 Parameters  : p_groupid
 Parameters  : p_partnum
  Parameters  : p_userid
*/
	v_access	   NUMBER (1) := 0;
	v_set_id	   VARCHAR2 (20);
	v_groupid	 NUMBER (20);
	v_part_groupid NUMBER (10);
	v_pflg		   VARCHAR2 (1);
	v_flg_id	   VARCHAR2 (1) := 'N';   -- flag for grpid =15
					  
	CURSOR findgrp_cur
	IS
		SELECT DECODE (c101_mapped_party_id, NULL, 0, 1) accessfl, t1501.c1500_group_id grpid
			 , gm_pkg_pd_group_pricing.get_publish_fl (v_groupid) flpublish
		  FROM t1501_group_mapping t1501
		 WHERE t1501.c101_mapped_party_id = p_userid AND t1501.c1500_group_id IN ('14', '15');
BEGIN
	-- When edit List/TW price in individual , should not edit in PD team. So get group id for part and check condition. 
	v_groupid := p_groupid;
	IF p_groupid IS NULL AND p_partnum IS NOT NULL THEN
		SELECT get_part_groupid (p_partnum)	  INTO v_part_groupid	  FROM DUAL;
		v_groupid := v_part_groupid;
	END IF;
	--
	BEGIN
		FOR grp_cur IN findgrp_cur
		LOOP
			IF grp_cur.grpid = '15'
			THEN
				v_flg_id	:= 'Y';
			END IF;

			IF grp_cur.flpublish = 'Y'
			THEN
				v_pflg		:= 'Y';
			END IF;

			IF grp_cur.accessfl = '1'
			THEN
				v_access	:= 1;
			END IF;
		END LOOP;

		IF v_pflg = 'Y' AND v_flg_id != 'Y'   --not pricing or sales analyst
		THEN
			RETURN 0;
		END IF;
	END;

	RETURN v_access;
EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		BEGIN
			BEGIN
				IF (p_groupid IS NULL)
				THEN
					SELECT get_part_groupid (p_partnum)
					  INTO v_part_groupid
					  FROM DUAL;
				ELSE
					v_part_groupid := p_groupid;
				END IF;

				SELECT DISTINCT DECODE (t4021.c4021_ref_id, NULL, 0, 1) accessfl
						   INTO v_access
						   FROM t4021_demand_mapping t4021
						  WHERE t4021.c4020_demand_master_id IN (
									SELECT c4020_demand_master_id
									  FROM t4020_demand_master
									 WHERE c901_demand_type = 40020
									   AND c4020_void_fl IS NULL
									   AND c4020_primary_user = p_userid)
							AND c901_ref_type = 40030
							AND c4021_ref_id = v_part_groupid;

				FOR grp_cur IN findgrp_cur
				LOOP
					IF grp_cur.grpid = '15'
					THEN
						v_flg_id	:= 'Y';
					END IF;

					IF grp_cur.flpublish = 'Y'
					THEN
						v_pflg		:= 'Y';
					END IF;

					IF grp_cur.accessfl = '1'
					THEN
						v_access	:= 1;
					END IF;
				END LOOP;

				IF v_pflg = 'Y' AND v_flg_id != 'Y'   --not pricing or sales analyst
				THEN
					RETURN 0;
				END IF;
			END;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				RETURN 0;
		END;

		RETURN v_access;
--
END get_grp_part_pr_edit_access;
/
