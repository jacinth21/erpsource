--@"C:\Database\Functions\Sales\get_party_primary_contact.fnc"
CREATE OR REPLACE
    FUNCTION get_party_primary_contact (
            p_partyid      IN t703_sales_rep.c101_party_id%TYPE,
            p_contact_mode IN t107_contact.c901_mode%TYPE)
        RETURN VARCHAR2
    IS
        /**********************************************************************
        * Description     : Functions returns primary contact based on Party ID
        *  Parameters  :p_user_id
        ***********************************************************************/
        v_contact_val t107_contact.c107_contact_value%TYPE;
    BEGIN
        BEGIN
             SELECT c107_contact_value
               INTO v_contact_val
               FROM t107_contact
              WHERE c101_party_id     = p_partyid
                AND c901_mode         = p_contact_mode --'90452'
                AND c107_primary_fl   = 'Y'
                AND C107_INACTIVE_FL IS NULL;
            RETURN v_contact_val;
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            RETURN NULL;
        WHEN TOO_MANY_ROWS THEN
            RETURN NULL;
        END;
    END get_party_primary_contact;
    /