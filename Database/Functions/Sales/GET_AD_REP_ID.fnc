/* Formatted on 2009/08/05 01:46 (Formatter Plus v4.8.0) */
CREATE OR REPLACE FUNCTION get_ad_rep_id (
	p_rep_id   t703_sales_rep.c703_sales_rep_id%TYPE
)
	RETURN VARCHAR2
IS
/*	Description 	: THIS FUNCTION RETURNS
 Parameters 		: p_code_id
*/
	v_ad_nm 	   VARCHAR2 (120);
BEGIN
	BEGIN
		SELECT c.c101_user_id
		  INTO v_ad_nm
		  FROM t703_sales_rep a, t701_distributor b, t708_region_asd_mapping c
		 WHERE c703_sales_rep_id = p_rep_id
		   AND a.c701_distributor_id = b.c701_distributor_id
		   AND b.c701_region = c.c901_region_id
		   AND c.c708_delete_fl IS NULL
		   AND c.c901_user_role_type = 8000;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN ' ';
	END;

	RETURN v_ad_nm;
END get_ad_rep_id;
/
