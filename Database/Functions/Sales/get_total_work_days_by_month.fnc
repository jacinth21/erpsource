/* Formatted on 2012/02/17 12:16 (Formatter Plus v4.8.0) */
--@"C:\Database\Functions\Sales\get_total_work_days_by_month.fnc";


/**
 * In this function, we will take the Current Month and get the total business days value.
 *	If the Inparam is passed in , then the system will calculate the Business Days for the passed in Month/Year.
 */
CREATE OR REPLACE FUNCTION get_total_work_days_by_month(p_month VARCHAR2,
	  p_company_id T1900_COMPANY.C1900_COMPANY_ID%TYPE
)
	RETURN NUMBER
IS
	v_working_days NUMBER;
	v_month DATE; 
BEGIN
	IF p_month IS NULL THEN
		v_month := CURRENT_DATE;
	ELSE
		v_month := TO_DATE (p_month, 'MM/YYYY');
	END IF;
	
	BEGIN
		SELECT COUNT (1) INTO v_working_days
		   FROM date_dim
		  WHERE date_dim.cal_date   >= TRUNC (v_month, 'Month')
		    AND date_dim.cal_date   <= last_day(v_month)
		    AND DAY NOT IN ('Sat','Sun')
		    AND date_key_id NOT IN (
		          	SELECT date_key_id
		          	  FROM COMPANY_HOLIDAY_DIM
		          	 WHERE HOLIDAY_DATE   >= TRUNC (v_month, 'Month')
		          	   AND  HOLIDAY_DATE   <= last_day(v_month)
		          	   AND HOLIDAY_FL = 'Y'
		          	    AND C1900_COMPANY_ID = p_company_id
			);
	   
	    EXCEPTION
			WHEN OTHERS
			THEN
					v_working_days	:= 0;	
	END;
	RETURN v_working_days;
END get_total_work_days_by_month;
/