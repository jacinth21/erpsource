/* Formatted on 2007/07/27 18:21 (Formatter Plus v4.8.0) */


CREATE OR REPLACE FUNCTION get_dist_set_count (
	p_set_id	t504_consignment.c207_set_id%TYPE
  , p_dist_id	t504_consignment.c701_distributor_id%TYPE
  , p_type		VARCHAR2
  , p_fromdt	VARCHAR2
  , p_todt		VARCHAR2
)
	RETURN VARCHAR2
IS
/*	Description 	: THIS FUNCTION RETURNS CODE NAME GIVEN THE CODE_ID
 Parameters 		: p_code_id
*/
--v_acc_nm VARCHAR2(10);
	v_set_cnt	   NUMBER;
	v_set_id		t504_consignment.c207_set_id%TYPE;
	v_dateformat 	varchar2(100);
BEGIN
	SELECT NVL(GET_COMPDTFMT_FRM_CNTX(),GET_RULE_VALUE('DATEFMT','DATEFORMAT')) INTO v_dateformat FROM DUAL;
	BEGIN
		IF p_type = 'DIST'
		THEN
			SELECT COUNT (1)
			  INTO v_set_cnt
			  FROM t504_consignment
			 WHERE c207_set_id = p_set_id
			   AND c701_distributor_id = p_dist_id
			   AND c504_ship_date BETWEEN TO_DATE (p_fromdt, v_dateformat) AND TO_DATE (p_todt, v_dateformat)
			   AND c504_status_fl = '4'
			   AND c504_void_fl IS NULL;
		ELSIF p_type = 'INHOUSE'
		THEN
			SELECT COUNT (1)
			  INTO v_set_cnt
			  FROM t504_consignment
			 WHERE c207_set_id = p_set_id
			   AND c504_inhouse_purpose = p_dist_id
			   AND c504_ship_date BETWEEN TO_DATE (p_fromdt, v_dateformat) AND TO_DATE (p_todt, v_dateformat)
			   AND c504_status_fl = '4'
			   AND c504_void_fl IS NULL;
		ELSIF p_type = 'DIST-RETURN'
		THEN
			IF TRIM (p_set_id) IS NULL
			THEN
				v_set_id	:= '0';
			ELSE
			    v_set_id  := p_set_id;	
			END IF;

			SELECT COUNT (1)
			  INTO v_set_cnt
			  FROM t506_returns
			 WHERE DECODE (c207_set_id, NULL, '0', c207_set_id) = v_set_id
			   AND c701_distributor_id = p_dist_id
			   AND c506_credit_date BETWEEN TO_DATE (p_fromdt, v_dateformat) AND TO_DATE (p_todt, v_dateformat)
			   AND c506_status_fl = '2'
			   AND c506_void_fl IS NULL;
		ELSIF p_type = 'INHOUSE-RETURN'
		THEN
			SELECT COUNT (1)
			  INTO v_set_cnt
			  FROM t506_returns
			 WHERE c207_set_id = p_set_id
			   AND c704_account_id = p_dist_id
			   AND c506_last_updated_date BETWEEN TO_DATE (p_fromdt, v_dateformat) AND TO_DATE (p_todt, v_dateformat)
			   AND c506_status_fl = '2'
			   AND c506_void_fl IS NULL;
		END IF;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN ' ';
	END;

	RETURN v_set_cnt;
END get_dist_set_count;
/
