CREATE OR REPLACE FUNCTION GET_AD_REGION_NAME
(
 p_ad_id  t708_region_asd_mapping.c101_user_id%TYPE
)
RETURN VARCHAR2
IS
/******************************************************************
 *  Description     : THIS FUNCTION RETURNS AD Region details
 * Parameters   : p_code_id
 *****************************************************************/
--
 v_code_nm   t901_code_lookup.c901_code_nm%TYPE;
--
BEGIN
--
 SELECT get_code_name(c901_region_id)
 INTO v_code_nm
 FROM t708_region_asd_mapping
 WHERE c101_user_id   = p_ad_id
 AND  NVL(c708_delete_fl,'N') = 'N'
 AND  ROWNUM  = 1;
 --
 RETURN v_code_nm;
--
EXCEPTION
WHEN NO_DATA_FOUND
THEN
--
      RETURN ' ';
--
END GET_AD_REGION_NAME;
/

