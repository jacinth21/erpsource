
--@"C:\Database\Functions\Sales\get_work_days_by_range.fnc";

CREATE OR REPLACE FUNCTION get_work_days_by_range(
	p_from_month VARCHAR2
	,p_to_month VARCHAR2)
	RETURN NUMBER
IS
	v_working_days NUMBER;
	v_from_month DATE; 
	v_to_month DATE;
	v_company_id NUMBER;
	
BEGIN
	v_from_month := TO_DATE (p_from_month, 'MM/DD/YYYY');
	v_to_month   := TO_DATE (p_to_month, 'MM/DD/YYYY');
	
	SELECT get_compid_frm_cntx()
			INTO   v_company_id
			FROM   DUAL;
			
	BEGIN
	 SELECT COUNT (1) INTO v_working_days
	   FROM date_dim
	  WHERE date_dim.cal_date   >= v_from_month
	    AND date_dim.cal_date   <= last_day(v_to_month)
	    AND DAY NOT IN ('Sat','Sun')
	    AND  date_key_id NOT IN (
				              	SELECT date_key_id 
				              	  FROM COMPANY_HOLIDAY_DIM 
				              	 WHERE HOLIDAY_DATE   >= v_from_month
				              	  AND HOLIDAY_DATE <= last_day(v_to_month)
				              	   AND HOLIDAY_FL = 'Y'
				              	    AND C1900_COMPANY_ID = v_company_id
								);
	    EXCEPTION
			WHEN OTHERS
			THEN
					v_working_days	:= 0;	
	END;
	RETURN v_working_days;
END get_work_days_by_range;
/