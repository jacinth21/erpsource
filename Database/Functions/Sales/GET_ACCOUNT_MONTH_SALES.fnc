CREATE OR REPLACE FUNCTION GET_ACCOUNT_MONTH_SALES
(
 	   p_acc_id T501_ORDER.C704_ACCOUNT_ID%TYPE
)
RETURN VARCHAR2
IS
/*  Description     : THIS FUNCTION RETURNS CODE NAME GIVEN THE CODE_ID
 Parameters 		: p_code_id
*/
v_total NUMBER(15,2);
v_day NUMBER;
v_date VARCHAR2(20);
BEGIN
     BEGIN
	 	 select to_char(sysdate,'dd') into v_day from dual;
		 v_day := v_day-01;
		 select to_char(sysdate-v_day,'mm/dd/yyyy') INTO v_date from dual;
		 SELECT
		 	 --SUM(C501_TOTAL_COST) + sum(decode(C501_SHIP_COST,'',0,C501_SHIP_COST)) SALES INTO v_total
			 SUM(C501_TOTAL_COST) SALES INTO v_total
		 FROM
		 	  T501_ORDER
		 WHERE
		 	 --C501_ORDER_DATE >= to_date(v_date,'mm/dd/yyyy') AND	C501_ORDER_DATE <= trunc(SYSDATE)
			 C501_ORDER_DATE BETWEEN to_date(v_date,'mm/dd/yyyy') AND trunc(SYSDATE)
		 AND
		 	 C501_VOID_FL IS NULL AND C501_DELETE_FL IS NULL
		 AND
		 	 C704_ACCOUNT_ID = p_acc_id;
   EXCEPTION
      WHEN NO_DATA_FOUND
   THEN
      RETURN ' ';
      END;
     RETURN v_total;
END GET_ACCOUNT_MONTH_SALES;
/

