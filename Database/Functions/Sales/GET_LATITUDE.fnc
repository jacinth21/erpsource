-- @"C:\Database\Functions\Sales\GET_LATITUDE.fnc";

create or replace
FUNCTION GET_LATITUDE (
p_id t703_sales_rep.c703_sales_rep_id%TYPE
,p_type varchar2
,p_lat_type varchar2
)

RETURN VARCHAR2
IS
  v_ship_lat  VARCHAR2(20);
  v_bill_lat  VARCHAR2(20);

BEGIN
  
  IF p_type='ADDR' 
  THEN 
  BEGIN
      SELECT c106_latitude 
        INTO v_ship_lat
        FROM t106_address
        WHERE c106_address_id = p_id
        AND c106_void_fl IS NULL;
        EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		v_ship_lat := NULL;
    END;
  ELSIF p_type='DIST'  
  THEN 
  BEGIN

      SELECT C701_ship_latitude,C701_bill_latitude
         INTO v_ship_lat,v_bill_lat
         FROM t701_distributor
         WHERE c701_void_fl IS NULL
         AND c701_distributor_id = p_id;
        
         EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		v_ship_lat := NULL;
		v_bill_lat := NULL;
    END;
  ELSIF p_type='ACC'
  THEN
  BEGIN
      SELECT C704_ship_latitude,C704_bill_latitude
        INTO v_ship_lat,v_bill_lat
        FROM t704_account
        WHERE c704_account_id = p_id
        AND c704_void_fl IS NULL;
        EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		v_ship_lat := NULL;
		v_bill_lat := NULL;	 
    END;
  END IF;
  IF p_lat_type = 'BILL'
  THEN
        RETURN v_bill_lat;
  ELSE 
  		RETURN v_ship_lat;
  END IF;
END get_latitude;
/