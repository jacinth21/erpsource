--@"C:\Database\Functions\Sales\get_sales_forecast_sales.fnc";

CREATE OR REPLACE FUNCTION get_sales_forecast_sales (
   p_part_number   t205_part_number.c205_part_number_id%TYPE
)
   RETURN NUMBER
IS
/********************************************************************************************
 * Description    :This function returns the sales forecast count for the selected part.
 *         Returns 3 month forecast value
 *********************************************************************************************/
   v_inventory_date   DATE;
   v_qty              NUMBER;
BEGIN
   -- To fetch active inventory lock period
   SELECT MAX (c250_lock_date)
     INTO v_inventory_date
     FROM t250_inventory_lock t250
    WHERE t250.c250_void_fl IS NULL AND t250.c901_lock_type = 20430;

   SELECT NVL (CEIL (SUM (t4042.c4042_qty)), 0)
     INTO v_qty
     FROM t4040_demand_sheet t4040, t4042_demand_sheet_detail t4042
    WHERE t4040.c4040_demand_period_dt = v_inventory_date
      AND t4040.c4040_void_fl IS NULL
      AND t4040.c901_demand_type = '40020' -- Sales
      AND t4042.c901_type IN ('50563')        -- Forecast
      AND t4040.c4040_demand_sheet_id = t4042.c4040_demand_sheet_id
      AND t4042.c4042_period BETWEEN TRUNC (SYSDATE, 'MONTH')
                                 AND TRUNC (LAST_DAY (ADD_MONTHS (SYSDATE, 1)))
      AND t4040.c4020_demand_master_id in (	
	select c4020_demand_master_id  From T4020_demand_master Where c901_demand_type = 40020 )
      AND t4042.c205_part_number_id IN (p_part_number);

   RETURN v_qty;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      RETURN 0;
--
END get_sales_forecast_sales;  
/
