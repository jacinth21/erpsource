/* Formatted on 2010/05/13 17:29 (Formatter Plus v4.8.0) */
--@"C:\DATABASE\Functions\Sales\get_part_groupid.fnc"

CREATE OR REPLACE FUNCTION get_part_groupid (
	p_partid   IN	VARCHAR2
)
	RETURN NUMBER
IS
/*
 Description  : this function returns the primary groupid for the part. w
 Parameters  : p_partid
*/
	v_groupid	   t4010_group.c4010_group_id%TYPE;
BEGIN
	--
	BEGIN
		SELECT t4011.c4010_group_id
		  INTO v_groupid
		  FROM t4011_group_detail t4011, t4010_group t4010
		 WHERE t4011.c205_part_number_id = p_partid
		   AND t4011.c901_part_pricing_type = '52080'
		   AND t4011.c4010_group_id = t4010.c4010_group_id
		   AND c901_type = 40045	-- Forecast/Demand Group
		   AND t4010.c4010_void_fl IS NULL;
	END;

	RETURN v_groupid;
EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		RETURN 0;
--
END get_part_groupid;
/
