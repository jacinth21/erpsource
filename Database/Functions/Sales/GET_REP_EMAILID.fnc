/* Formatted on 2009/03/07 10:42 (Formatter Plus v4.8.0) */
--@"C:\Database\Functions\Sales\GET_REP_EMAILID.fnc";

/*  Description     : THIS FUNCTION RETURNS SALES REP EMAIL ID FOR GIVEN REP ID
 *  Parameters 		: p_rep_id
 */
 CREATE OR REPLACE FUNCTION get_rep_emailid (
        p_rep_id T703_SALES_REP.C703_SALES_REP_ID%TYPE)
    RETURN VARCHAR2
IS
    v_rep_email T703_SALES_REP.C703_EMAIL_ID%TYPE;
BEGIN
    BEGIN
         SELECT C703_EMAIL_ID
           INTO v_rep_email
           FROM T703_SALES_REP
          WHERE C703_SALES_REP_ID = p_rep_id;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN ' ';
    END;
    RETURN v_rep_email;
END get_rep_emailid;
/