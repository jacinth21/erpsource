--@"C:\Database\Functions\Sales\GET_REP_ADDRINFO.fnc";

CREATE OR REPLACE FUNCTION GET_REP_ADDRINFO
(
 	  
	   p_rep_id t907_shipping_info.c907_ship_to_id%TYPE,
	   p_ref_id t907_shipping_info.c907_ref_id%TYPE
	  
)
RETURN t907_shipping_info.c106_address_id%TYPE
IS
/*  Description     : THIS FUNCTION RETURNS REP DETAILS
 Parameters 		: p_code_id
*/
v_rep_add_id  t907_shipping_info.c106_address_id%TYPE;

BEGIN
    
	select c106_address_id into v_rep_add_id from  t907_shipping_info where c907_ref_id=p_ref_id and c907_ship_to_id=p_rep_id and c907_void_fl is null;
                           
	
     RETURN v_rep_add_id;
END GET_REP_ADDRINFO;
/

