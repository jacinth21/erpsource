/***********************************************************************************************
-- @"C:\DATABASE\Functions\Sales\get_email_track_click_count.fnc"
 *    Description   : This function will return  clicked count for shared links(documents,images and videos)
 *    Parameters 		: p_email_sent_dtls_id,p_link_url
 *    Return Parameters  : Clicked Count
 ************************************************************************************************/
CREATE OR REPLACE FUNCTION get_email_track_click_count (
    p_email_sent_dtls_id   IN   t9164_email_event_tracker.c9163_email_sent_dtls_id%TYPE,
    p_link_url             IN   t9164_email_event_tracker.c9165_url%TYPE
) RETURN NUMBER IS

--
    v_clicked_cnt NUMBER;
BEGIN
    SELECT
        COUNT(1)
    INTO v_clicked_cnt
    FROM
        t9164_email_event_tracker
    WHERE
        c9163_email_sent_dtls_id = p_email_sent_dtls_id
        AND c9165_url = p_link_url
        AND c901_event_type = 110696 -- Click
        AND c9164_void_fl IS NULL;
--
    RETURN v_clicked_cnt;	
--
END get_email_track_click_count;
/