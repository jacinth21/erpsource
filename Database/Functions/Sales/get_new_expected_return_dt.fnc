/* Formatted on 2014/12/23 19:24 (Formatter Plus v4.8.0) */
CREATE OR REPLACE FUNCTION get_new_expected_return_dt (
	p_extn_date DATE
  , p_day_extn	NUMBER  
)
	RETURN DATE
IS
/*	Description 	: THIS FUNCTION RETURNS NEW EXPECTED RETURN DATE BASED ON THE OLD EXT DATE AND DAY OF EXTEN
 	Parameters   	: p_extn_date AND p_day_extn
*/
	    v_return_dt DATE; 
        v_rule_days  NUMBER;
        v_date DATE;
	    v_day_number NUMBER;
	    v_company_id NUMBER;
BEGIN
		
		SELECT get_compid_frm_cntx()
			INTO   v_company_id
			FROM   DUAL;
			
	     v_rule_days := p_day_extn;
         v_date := TRUNC(p_extn_date);
         
        IF (v_date < TRUNC(SYSDATE)) THEN 
            -- v_date := SYSDATE-1;
            v_date := get_last_working_day(SYSDATE);
        ELSIF (v_date = TRUNC(SYSDATE)) THEN        
            v_date := SYSDATE;
        END IF;
                  
		BEGIN
		   SELECT WEEK_DAY_NUMBER
		     INTO v_day_number
		     FROM date_dim
		    WHERE TRUNC (cal_date) = TO_DATE (v_date) 
		    AND DAY NOT IN ('Sat','Sun')
		     AND date_key_id NOT IN (
				              	SELECT date_key_id 
				              	  FROM COMPANY_HOLIDAY_DIM 
				              	 WHERE HOLIDAY_DATE  = TO_DATE (v_date)
				              	   AND HOLIDAY_FL = 'Y'
				              	    AND C1900_COMPANY_ID = v_company_id
								)
		    ;
		EXCEPTION
		WHEN NO_DATA_FOUND THEN
	       v_day_number := 0;
	    END;
	    
	    IF v_day_number NOT IN (0,1,7) THEN
	    	v_rule_days:= v_rule_days - 1;
		END IF;
	
	    BEGIN
		 SELECT caldate
		   INTO v_return_dt
		   FROM
		    (SELECT caldate, ROWNUM rnum
		       FROM
		        (SELECT cal_date caldate
		           FROM date_dim
		          WHERE TRUNC (cal_date) >= TRUNC (v_date)
		            AND TRUNC (cal_date) <= TRUNC (sysdate) + 30
		            AND DAY NOT IN ('Sat','Sun')
		             AND date_key_id NOT IN (
				              	SELECT date_key_id 
				              	  FROM COMPANY_HOLIDAY_DIM 
				              	 WHERE HOLIDAY_DATE   >= TRUNC (v_date)
				              	  AND HOLIDAY_DATE <= TRUNC (sysdate) + 30
				              	   AND HOLIDAY_FL = 'Y'
				              	    AND C1900_COMPANY_ID = v_company_id
								)
		       ORDER BY TRUNC (cal_date) ASC
		        )
		    )
		  WHERE rnum = v_rule_days +1;
	    EXCEPTION
	    WHEN NO_DATA_FOUND THEN
	        v_return_dt := NULL;
	    END;	    
	    RETURN v_return_dt;
END get_new_expected_return_dt;
/
