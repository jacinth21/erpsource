     /* Formatted on 2012/02/17 12:16 (Formatter Plus v4.8.0) */
--@"C:\Database\Functions\Sales\get_company_id_from_user_id.fnc";

CREATE OR REPLACE
    FUNCTION get_company_id_from_user_id (
        p_user_id t101_user.c101_user_id%TYPE)
        RETURN v700_territory_mapping_detail.compid%TYPE
    IS
        /* Description  : THIS FUNCTION RETURNS company id based on user ID and access and dept id
        */
        v_comp_id v700_territory_mapping_detail.compid%TYPE;
        v_deptid t101_user.c901_dept_id%TYPE;
        v_accesslvl t101_user.c101_access_level_id%TYPE;
        v_company_id t1900_company.c1900_company_id%TYPE;
    BEGIN
        BEGIN
             SELECT T101U.C101_ACCESS_LEVEL_ID, T101U.C901_DEPT_ID, gm_pkg_it_user.get_default_party_company(T101U.c101_party_id)
               INTO v_accesslvl, v_deptid, v_company_id
               FROM T101_USER T101U
              WHERE T101U.C101_USER_ID = p_user_id;
        EXCEPTION
        WHEN OTHERS THEN
            v_accesslvl := NULL; -- Globus Medical Inc
            v_deptid    := NULL;
        END;
        v_comp_id := get_company_id (v_accesslvl, v_deptid, p_user_id, v_company_id) ;
        RETURN v_comp_id;
    END get_company_id_from_user_id;
    /