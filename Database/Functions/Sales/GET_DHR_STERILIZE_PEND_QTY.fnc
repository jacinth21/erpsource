CREATE OR REPLACE FUNCTION GET_DHR_STERILIZE_PEND_QTY
(
 	   p_dhr_id T408_DHR.C408_DHR_ID%TYPE,
	   p_Qty_Rec T408_DHR.C408_QTY_ON_SHELF%TYPE
)
RETURN VARCHAR2
IS
/*  Description     : THIS FUNCTION RETURNS CODE NAME GIVEN THE CODE_ID
 Parameters 		: p_code_id
*/
v_cnt VARCHAR2(10);
v_sterilize_cnt NUMBER;

BEGIN
     BEGIN
       SELECT SUM(C402_QTY_ORDERED) INTO v_sterilize_cnt
	   FROM  T402_WORK_ORDER
       WHERE C408_DHR_ID = p_dhr_id
	   AND C402_VOID_FL IS NULL;
	   
	   IF v_sterilize_cnt IS NULL
	   	  THEN v_sterilize_cnt := 0;
	   END IF;
	   v_cnt:= p_Qty_Rec - v_sterilize_cnt;
   EXCEPTION
      WHEN NO_DATA_FOUND
   THEN
      RETURN ' ';
      END;
     RETURN v_cnt;
END GET_DHR_STERILIZE_PEND_QTY;
/

