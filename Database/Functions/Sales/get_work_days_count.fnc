/* Formatted on 2012/08/29 11:15 (Formatter Plus v4.8.0) */
--@"C:\Database\Functions\Sales\get_work_days_count.fnc";

CREATE OR REPLACE FUNCTION get_work_days_count (
   p_from_date   VARCHAR2,
   p_to_date     VARCHAR2
)
   RETURN NUMBER
IS
   v_working_days   NUMBER;
   v_days           NUMBER;
   v_from_date      DATE;
   v_to_date        DATE;
   v_date_fmt		VARCHAR2(30);
   v_company_id NUMBER;
   
BEGIN
   BEGIN
	   
	   SELECT get_compid_frm_cntx()
			INTO   v_company_id
			FROM   DUAL;
			
	  v_date_fmt := NVL(get_rule_value('DATEFMT','DATEFORMAT'),'MM/dd/yyyy');
	  
	  /* select NVL(get_compdtfmt_frm_cntx(),'MM/dd/yyyy') 
	   into v_date_fmt 
	   from dual;*/
	   
      IF TO_DATE (p_from_date, v_date_fmt) >TO_DATE (p_to_date, v_date_fmt)         
      THEN
         v_from_date := TO_DATE (p_from_date, v_date_fmt);
         v_to_date := TO_DATE (p_to_date, v_date_fmt);
      ELSE
         v_from_date := TO_DATE (p_from_date, v_date_fmt);
         v_to_date := TO_DATE (p_to_date, v_date_fmt);
      END IF;

      SELECT cal_date
        INTO v_from_date
        FROM (SELECT   cal_date
                  FROM date_dim
                 WHERE cal_date >= v_from_date
                   AND cal_date <= v_from_date + 10
                   AND DAY NOT IN ('Sat','Sun')
                    AND date_key_id NOT IN (
				              	SELECT date_key_id 
				              	  FROM COMPANY_HOLIDAY_DIM 
				              	 WHERE HOLIDAY_DATE   >= v_from_date
				              	   AND HOLIDAY_DATE   <= v_from_date + 10
				              	   AND HOLIDAY_FL = 'Y'
				              	    AND C1900_COMPANY_ID = v_company_id
								)
              ORDER BY cal_date)
       WHERE ROWNUM = 1;

      SELECT cal_date
        INTO v_to_date
        FROM (SELECT   cal_date
                  FROM date_dim
                 WHERE cal_date >= v_to_date
                   AND cal_date <= v_to_date + 10
                   AND DAY NOT IN ('Sat','Sun')
                   AND date_key_id NOT IN (
				              	SELECT date_key_id 
				              	  FROM COMPANY_HOLIDAY_DIM 
				              	 WHERE HOLIDAY_DATE   >= v_to_date
				              	   AND HOLIDAY_DATE   <= v_to_date + 10
				              	   AND HOLIDAY_FL = 'Y'
				              	    AND C1900_COMPANY_ID = v_company_id
								)
              ORDER BY cal_date)
       WHERE ROWNUM = 1;

      SELECT COUNT (1)
        INTO v_working_days
        FROM date_dim
       WHERE cal_date > TRUNC (v_from_date)
         AND cal_date <= TRUNC (v_to_date)
         AND DAY NOT IN ('Sat','Sun')
         AND date_key_id NOT IN (
			              	SELECT date_key_id 
			              	  FROM COMPANY_HOLIDAY_DIM 
			              	 WHERE HOLIDAY_DATE   > TRUNC (v_from_date)
			              	   AND HOLIDAY_DATE  <= TRUNC (v_to_date)
			              	   AND HOLIDAY_FL = 'Y'
			              	    AND C1900_COMPANY_ID = v_company_id
							);

   EXCEPTION
      WHEN OTHERS
      THEN
         v_working_days := 0;
   END;

   RETURN v_working_days;
END get_work_days_count;
/
