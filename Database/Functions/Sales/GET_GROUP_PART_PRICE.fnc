/* Formatted on 2010/02/18 15:29 (Formatter Plus v4.8.0) */
CREATE OR REPLACE FUNCTION GET_GROUP_PART_PRICE (
  p_part_number  IN	t205_part_number.c205_part_number_id%TYPE
  , p_price_type   IN	t7010_group_price_detail.c901_price_type%TYPE
  
)
	RETURN NUMBER
IS

	v_group_id	   t4010_group.c4010_group_id%TYPE;
	v_price 	   t705_account_pricing.c705_unit_price%TYPE;
	
BEGIN
	--
	BEGIN
		
		 SELECT t4010.c4010_group_id into v_group_id
					  FROM t4011_group_detail t4011, t4010_group t4010
					 WHERE t4010.c4010_group_id = t4011.c4010_group_id
					   AND t4010.c4010_void_fl IS NULL
					   AND t4010.c901_type = 40045
					   AND t4011.c205_part_number_id = p_part_number
					   AND t4011.c901_part_pricing_type = 52080;   -- primary part
		
			SELECT c7010_price into v_price
			  FROM t7010_group_price_detail t7010
			 WHERE t7010.c4010_group_id = v_group_id
			   AND t7010.c901_price_type = p_price_type
			   AND t7010.c7010_void_fl IS NULL;
	END;

	RETURN v_price;
EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
    RETURN 0;
--
END GET_GROUP_PART_PRICE;
/
