/* Formatted on 2010/02/24 13:16 (Formatter Plus v4.8.0) */
--@"C:\DATABASE\Functions\Sales\get_price_update_access.fnc"

CREATE OR REPLACE FUNCTION get_price_update_access (
	p_pricinggroup_id	IN	 t1500_group.c1500_group_id%TYPE
  , p_pricingpd_id		IN	 t1500_group.c1500_group_id%TYPE
  , p_partyid			IN	 t1501_group_mapping.c101_mapped_party_id%TYPE
)
	RETURN NUMBER
IS
/*
 Description  : this function returns the user has access for price edit or not
 Parameters  : p_group_id
 Parameters  : p_partyid
 Changing the parameter name p_userid to p_partyid and checking void flag is null
*/
	v_access	   NUMBER (1) := 0;
	v_company_id t1900_company.c1900_company_id%TYPE;
BEGIN
	--
		SELECT get_compid_frm_cntx()
    		INTO v_company_id
    		FROM DUAL;
	
	BEGIN
		SELECT DECODE (c101_mapped_party_id, NULL, 0, 1) accessfl
		  INTO v_access
		  FROM t1501_group_mapping t1501
		 WHERE t1501.c1500_group_id = p_pricinggroup_id AND t1501.c101_mapped_party_id = p_partyid  AND t1501.c1900_company_id = v_company_id AND t1501.c1501_void_fl IS NULL;
	END;

	RETURN v_access;
EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		BEGIN
			BEGIN
				SELECT DECODE (c101_mapped_party_id, NULL, 0, 1) accessfl
				  INTO v_access
				  FROM t1501_group_mapping t1501
				 WHERE t1501.c1500_group_id = p_pricingpd_id AND t1501.c101_mapped_party_id = p_partyid AND t1501.c1900_company_id = v_company_id AND t1501.c1501_void_fl IS NULL;
			END;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				RETURN 0;
		END;

		RETURN v_access;
END get_price_update_access;
/
