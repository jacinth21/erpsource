CREATE OR REPLACE FUNCTION get_lnreq_ext_status
(
 	   p_req_dtl_id t526_product_request_detail.c525_product_request_id%TYPE
)
RETURN VARCHAR2
IS
/*  Description     : THIS FUNCTION RETURNS STATUS OF LOANER EXTENSION REQUEST
    Parameters 		: p_req_dtl_id
*/
v_cnt  NUMBER;
v_status VARCHAR2(100);
BEGIN
		SELECT COUNT(1) v_cnt
		  INTO v_cnt
		  FROM t504a_loaner_transaction t504a 
		 WHERE t504a.c526_product_request_detail_id = p_req_dtl_id 
		   AND t504a.c901_status = 1901  --Pending Approval Status
		   AND t504a.c504a_void_fl is null;
   
		  IF v_cnt > 0 THEN
		     v_status := 'Y';
		  ELSE 
		 	 v_status := NULL;
		  END IF;

     RETURN v_status;
END get_lnreq_ext_status;
/

