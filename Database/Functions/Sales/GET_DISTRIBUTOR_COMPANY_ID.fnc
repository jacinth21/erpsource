/* Formatted on 2016/11/03 15:17 (Formatter Plus v4.8.8) */
CREATE OR REPLACE FUNCTION get_distributor_company_id (
    p_dist_id   t701_distributor.c701_distributor_id%TYPE
)
    RETURN NUMBER
IS
/*************************************************************************************************  
 *   Description     : Fucntion return Rcompany code for the selected distributor
 *                     Primarly created for Tag and other transaction to update company code
 *  Parameters         : p_code_id
 *************************************************************************************************/
    v_company_id   t1900_company.c1900_company_id%TYPE;
BEGIN
    SELECT c1900_company_id
      INTO v_company_id
      FROM t701_distributor
     WHERE c701_distributor_id = p_dist_id;

    --
    RETURN v_company_id;
EXCEPTION
    WHEN NO_DATA_FOUND
    THEN
        RETURN NULL;
END get_distributor_company_id;
/