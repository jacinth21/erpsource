--@"C:\Database\Functions\Sales\GET_REP_INFO.fnc";

CREATE OR REPLACE FUNCTION GET_REP_INFO
(
 	  
	   p_rep_id T704_ACCOUNT.C703_SALES_REP_ID%TYPE,
	   p_add_id t106_address.c106_address_id%TYPE
	  
)
RETURN VARCHAR2
IS
/*  Description     : THIS FUNCTION RETURNS REP DETAILS
 Parameters 		: p_code_id
*/
v_rep_add  VARCHAR2(1000);

BEGIN
     BEGIN
	SELECT	   get_party_name (t106.c101_party_id)
						   || '<br>'
						   || t106.c106_add1
						   || '<br>'
						   || t106.c106_add2
						   || '<br>'
						   || DECODE(NVL(GET_RULE_VALUE('SWAP_ZIP_CODE','SWAP_ZIP_CODE'),'N'),'Y',t106.c106_zip_code,t106.c106_city)
						   || ' '
						   || get_code_name_alt (t106.c901_state)
						   || ' '
						   || DECODE(NVL(GET_RULE_VALUE('SWAP_ZIP_CODE','SWAP_ZIP_CODE'),'N'),'Y',t106.c106_city,t106.c106_zip_code)
                           || '<br>'
                           ||'Ph:-'||t703.c703_phone_number
                           || '<br>'
                           ||'Email:-'||t703.c703_email_id 
					  INTO v_rep_add
					  FROM t106_address t106,t703_sales_rep t703 
					 WHERE  t703.c703_sales_rep_id=p_rep_id and 
                            t106.c101_party_id =t703.c101_party_id
                            and t106.c106_address_id=p_add_id;
                           
	
   EXCEPTION
      WHEN NO_DATA_FOUND
   THEN
      RETURN ' ';
      END;
     RETURN v_rep_add;
END GET_REP_INFO;
/

