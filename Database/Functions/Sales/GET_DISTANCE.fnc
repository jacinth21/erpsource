--@"C:\Database\Functions\Sales\GET_DISTANCE.fnc";
create or replace
FUNCTION GET_DISTANCE (
 lat1 t106_address.c106_latitude%type,
 lng1 t106_address.c106_longitude%type,
 lat2 t106_address.c106_latitude%type,
 lng2 t106_address.c106_longitude%typE
 )
RETURN NUMBER
IS
  v_distance  number;
BEGIN
  IF lat1 IS NOT NULL and lat2 IS NOT NULL and lng1 IS NOT NULL and lng2 IS NOT NULL
  THEN 
    BEGIN
      SELECT
       ROUND(SDO_GEOM.SDO_DISTANCE(SDO_GEOMETRY(2001,8307,SDO_POINT_TYPE(lng1,lat1, NULL),NULL,NULL), SDO_GEOMETRY(2001,8307,SDO_POINT_TYPE(lng2,lat2, NULL),NULL,NULL),500,'unit = MILE'),0) 
       INTO v_distance FROM DUAL;
       return v_distance;
    EXCEPTION WHEN NO_DATA_FOUND THEN
    return null;
    
    end;
    
    else
        return null;
    end if;
end GET_distance;
/