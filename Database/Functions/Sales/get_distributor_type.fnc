/* Formatted on 2008/08/18 14:59 (Formatter Plus v4.8.0) */
CREATE OR REPLACE FUNCTION get_distributor_type (
	p_distributorid   t504_consignment.c701_distributor_id%TYPE
)
	RETURN NUMBER
IS
/*	Description 	: THIS FUNCTION RETURNS distributor type
 Parameters 	   : p_distributorId
*/
	v_type		   t701_distributor.c901_distributor_type%TYPE;
BEGIN
	SELECT t701.c901_distributor_type
	  INTO v_type
	  FROM t701_distributor t701
	 WHERE t701.c701_distributor_id = p_distributorid;

	RETURN v_type;
EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		RETURN NULL;
END get_distributor_type;
/
