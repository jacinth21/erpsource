--@"C:\Database\Functions\Sales\get_group_current_prc.fnc";

CREATE OR REPLACE FUNCTION get_group_current_prc
(
	 p_acc_id		T704_ACCOUNT.C704_ACCOUNT_ID%TYPE
	 ,p_gpo_id 		T101_USER.C101_PARTY_ID%TYPE
	 ,p_group_id 	T7051_ACCOUNT_GROUP_PRICING.C7051_REF_ID%TYPE

)
RETURN NUMBER
IS

 v_acc_id T704_ACCOUNT.C704_ACCOUNT_ID%TYPE;

/*  Description     : THIS FUNCTION RETURNS THE GROUP CURRENT PRICE
 	Parameters 		: p_group_id, p_acc_id, p_gop_id
*/
	v_current_prc T7051_ACCOUNT_GROUP_PRICING.C7051_PRICE%TYPE;
	
	
	
	BEGIN
		SELECT DECODE(p_gpo_id,'',p_acc_id,'') INTO v_acc_id FROM DUAL;
		
		SELECT NVL (t7051.c7051_price, t705.c705_unit_price) INTO v_current_prc 
		FROM 
 		(SELECT t705.c4010_group_id grpid, t705.c705_unit_price
			FROM t705_account_pricing_by_group t705
			WHERE  ( t705.c704_account_id = NVL(v_acc_id,-999) or t705.C101_PARTY_ID =  NVL(p_gpo_id, -999)) 
			AND t705.c4010_group_id= p_group_id
			) t705,                   			
			(SELECT c704_account_id,t7051.c7051_ref_id grpid, t7051.c7051_price
	            FROM t7051_account_group_pricing t7051
	             where t7051.c7051_active_fl = 'Y'
	             and t7051.c901_ref_type = '52000'               --'GROUP'
	             AND t7051.c7051_ref_id= p_group_id
	             AND (  c704_account_id = NVL(v_acc_id,-999) or t7051.C101_GPO_ID =  NVL(p_gpo_id, -999) ) ) t7051  
	       where t705.grpid = t7051.grpid(+);
   RETURN v_current_prc;
END get_group_current_prc;
/
