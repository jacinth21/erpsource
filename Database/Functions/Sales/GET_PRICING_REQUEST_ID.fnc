/* Formatted on 2010/05/13 17:29 (Formatter Plus v4.8.0) */
--@"c:\database\functions\sales\GET_PRICING_REQUEST_ID.fnc"

	   
CREATE OR REPLACE FUNCTION GET_PRICING_REQUEST_ID (
	 p_acc_id		 IN		 t704_account.c704_account_id%TYPE
	,p_gpo_id        IN  	 t7000_account_price_request.c101_gpo_id%TYPE
	,p_req_status    IN  	 VARCHAR DEFAULT NULL
	,p_frm_swap_req  IN  	 VARCHAR DEFAULT NULL
)
	RETURN VARCHAR
IS
	v_requestid  	t7000_account_price_request.c7000_account_price_request_id%TYPE;
	v_request_stat 	VARCHAR2(20); 
	
BEGIN
	
	--
	BEGIN
		SELECT c7000_account_price_request_id into v_requestid FROM (
			SELECT c7000_account_price_request_id   FROM 
				t7000_account_price_request 
				WHERE c704_account_id = p_acc_id
                AND NVL(c101_gpo_id,'-999') = NVL(p_gpo_id,'-999')
			AND c901_request_status IN(p_req_status)
				AND  c7000_void_fl IS NULL
			ORDER BY c7000_created_date ASC) 
      WHERE ROWNUM=1;
	END;
	RETURN v_requestid;
EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
	BEGIN
		IF p_frm_swap_req IS NULL
		THEN
			SELECT c7000_account_price_request_id into v_requestid FROM (
				SELECT c7000_account_price_request_id   FROM 
					t7000_account_price_request 
					WHERE c704_account_id = p_acc_id
	                AND NVL(c101_gpo_id,'-999') = NVL(p_gpo_id,'-999')
					AND c901_request_status IN (52186,52187) -- PENDING AD APPR, PENDING VP APPR
					AND  c7000_void_fl IS NULL
				ORDER BY c7000_created_date ASC) 
	      WHERE ROWNUM=1;
      END IF;
      EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		RETURN '';
	END;
	RETURN v_requestid;
--
END GET_PRICING_REQUEST_ID;
/
