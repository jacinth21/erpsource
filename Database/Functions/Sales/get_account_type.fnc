/* Formatted on 2008/08/18 14:59 (Formatter Plus v4.8.0) */
--@"C:\Database\Functions\Sales\get_account_type.fnc";
CREATE OR REPLACE
    FUNCTION get_account_type (
            p_accountid t704_account.c704_account_id%TYPE)
        RETURN NUMBER
    IS
        /* Description  : This Function Returns Account Type
        Parameters     : p_accountid
        */
        v_type t704_account.c901_account_type%TYPE;
    BEGIN
        BEGIN
             SELECT c901_account_type
               INTO v_type
               FROM t704_account
              WHERE c704_account_id = p_accountid
                AND c704_void_fl   IS NULL;
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_type := NULL;
        END;
        --
        RETURN v_type;
    END get_account_type;
    /
