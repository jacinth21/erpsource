/* Formatted on 2012/02/17 12:16 (Formatter Plus v4.8.0) */
--@"C:\Database\Functions\Sales\get_company_id.fnc";

CREATE OR REPLACE FUNCTION get_company_id (
	p_accesslvl   t101_user.c101_access_level_id%TYPE
  , p_deptid	  t101_user.c901_dept_id%TYPE
  , p_user_id	  t101_user.c101_user_id%TYPE
  , p_company_id  t1900_company.c1900_company_id%TYPE DEFAULT NULL
)
	RETURN v700_territory_mapping_detail.compid%TYPE
IS
/*	Description 	: THIS FUNCTION RETURNS company id
*/
	v_comp_id	   v700_territory_mapping_detail.compid%TYPE;
BEGIN

	v_comp_id	:= 100800;	 -- Globus Medical Inc
	
	-- If primary company is 1001(BBA) setting more filter company as 4000643(Bone Bank Allografts)
	IF p_company_id = 1001
	THEN
		v_comp_id	:= 4000643;	 -- Bone Bank Allografts
	END IF;
					
	IF p_deptid = 2005
	THEN   -- Sales
		BEGIN
			IF p_accesslvl = 1
			THEN
				SELECT t710.c901_company_id
				  INTO v_comp_id
				  FROM t710_sales_hierarchy t710, t701_distributor t701, t703_sales_rep t703
				 WHERE t710.c901_area_id = t701.c701_region
				   AND t701.c701_distributor_id = t703.c701_distributor_id
				   AND t703.c703_sales_rep_id = p_user_id
				   AND t701.c701_void_fl is null
				   AND t703.c703_void_fl IS NULL
				   AND t710.c710_active_fl = 'Y';				
			ELSIF p_accesslvl = 2
			THEN
				SELECT t710.c901_company_id 
				  INTO v_comp_id
				  FROM t710_sales_hierarchy t710, t701_distributor t701
				 WHERE t710.c901_area_id = t701.c701_region
				   AND t701.c701_distributor_id IN (SELECT c701_distributor_id
												      FROM t101_user
											         WHERE c101_user_id = p_user_id)
				   AND t701.c701_void_fl is null
				   AND t710.c710_active_fl = 'Y';				
			ELSIF p_accesslvl = 3
			THEN
				SELECT t710.c901_company_id
				  INTO v_comp_id
				  FROM t708_region_asd_mapping t708, t710_sales_hierarchy t710
			     WHERE t708.c901_user_role_type = 8000 
				   AND t708.c901_region_id = t710.c901_area_id
				   AND t710.c710_active_fl = 'Y'   
				   AND t708.c101_user_id = p_user_id
				   AND t708.c708_delete_fl IS NULL
				   AND ROWNUM = 1;				
			ELSIF p_accesslvl IN (4, 6)
			THEN
				SELECT t710.c901_company_id
				  INTO v_comp_id
				  FROM t708_region_asd_mapping t708, t710_sales_hierarchy t710
			     WHERE t708.c901_user_role_type = 8001 
				   AND t708.c901_region_id = t710.c901_area_id
				   AND t710.c710_active_fl = 'Y'   
				   AND t708.c101_user_id = p_user_id
				   AND t708.c708_delete_fl IS NULL
				   AND ROWNUM = 1;
			ELSIF p_accesslvl = 7
			THEN
				SELECT t710.c901_company_id
				  INTO v_comp_id
				  FROM t710_sales_hierarchy t710, t701_distributor t701, t703_sales_rep t703
				 WHERE t710.c901_area_id = t701.c701_region
				   AND t701.c701_distributor_id = t703.c701_distributor_id				   
				   AND t701.c701_void_fl is null
				   AND t703.c703_void_fl IS NULL
				   AND t710.c710_active_fl = 'Y'
			       AND t703.c703_sales_rep_id IN (SELECT DISTINCT v700.rep_id
												   FROM v700_territory_mapping_detail v700
												  WHERE v700.ac_id IN (SELECT c704_account_id
																		 FROM t704a_account_rep_mapping
																		WHERE c704a_void_fl IS NULL AND c703_sales_rep_id = p_user_id))
				   AND ROWNUM = 1;
			END IF;
		EXCEPTION
			WHEN OTHERS
			THEN
				v_comp_id	:= 100800;	 -- Globus Medical Inc
				
				IF p_company_id = 1001
				THEN
					v_comp_id	:= 4000643;	 -- Bone Bank Allografts
				END IF;
		END;
	END IF;
    RETURN v_comp_id;
END get_company_id;
/
