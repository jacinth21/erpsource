CREATE OR REPLACE FUNCTION GET_TERRITORY_NAME
(
 p_terr_id  t702_territory.c702_territory_id%TYPE
)
RETURN VARCHAR2
IS
/******************************************************************
 *  Description     : THIS FUNCTION RETURNS Territory Name
 * Parameters   : p_trr_id
 *****************************************************************/
--
 v_terr_nm   t702_territory.c702_territory_name%TYPE;
--
BEGIN
--
 SELECT c702_territory_name
 INTO v_terr_nm
 FROM t702_territory
 WHERE c702_territory_id = p_terr_id;
 --
 RETURN v_terr_nm;
--
EXCEPTION
WHEN NO_DATA_FOUND
THEN
--
      RETURN ' ';
--
END GET_TERRITORY_NAME;
/

