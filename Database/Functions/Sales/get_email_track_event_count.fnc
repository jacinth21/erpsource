/***********************************************************************************************
-- @"C:\DATABASE\Functions\Sales\get_email_track_event_count.fnc"
 *    Description   : This function will return  open count for shared links(documents,images and videos)
 *    Parameters 		: p_email_sent_dtls_id,p_type
 *    Return Parameters  : Open Count
 ************************************************************************************************/
CREATE OR REPLACE FUNCTION get_email_track_event_count (
    p_email_sent_dtls_id   IN   t9164_email_event_tracker.c9163_email_sent_dtls_id%TYPE,
    p_type                 IN   t9164_email_event_tracker.c901_event_type%TYPE
) RETURN NUMBER IS

--
    v_open_cnt NUMBER;
BEGIN
	--
    SELECT
        COUNT(1)
    INTO v_open_cnt
    FROM
        t9164_email_event_tracker
    WHERE
        c9163_email_sent_dtls_id = p_email_sent_dtls_id
        AND c901_event_type = 110696 -- Click
        AND c9164_void_fl IS NULL;
 --
    RETURN v_open_cnt;
 --   
END get_email_track_event_count;
/