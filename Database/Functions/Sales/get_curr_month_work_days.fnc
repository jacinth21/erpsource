/* Formatted on 2012/02/17 12:16 (Formatter Plus v4.8.0) */
--@"C:\Database\Functions\Sales\get_curr_month_work_days.fnc";

CREATE OR REPLACE FUNCTION get_curr_month_work_days(p_month VARCHAR2)
	RETURN NUMBER
IS
	v_working_days NUMBER;
	v_month DATE; 
	v_company_id NUMBER;
	
BEGIN

	SELECT get_compid_frm_cntx()
	INTO   v_company_id
	FROM   DUAL;
			
	IF p_month IS NULL THEN
		v_month := SYSDATE;
	ELSE
		v_month := TO_DATE (p_month, 'MM/YYYY');
	END IF;
	BEGIN
	 SELECT COUNT (1) INTO v_working_days
	   FROM date_dim
	  WHERE date_dim.cal_date   >= TRUNC (v_month, 'Month')
	    AND date_dim.cal_date   <= DECODE(TRUNC (v_month, 'Month'),TRUNC (sysdate, 'Month'),TRUNC (sysdate),last_day(v_month))
	    AND DAY NOT IN ('Sat','Sun')
	    AND date_key_id NOT IN (
				              	SELECT date_key_id 
				              	  FROM COMPANY_HOLIDAY_DIM 
				              	 WHERE HOLIDAY_DATE   >= TRUNC (v_month, 'Month')
				              	   AND HOLIDAY_DATE   <= DECODE(TRUNC (v_month, 'Month'),TRUNC (sysdate, 'Month'),TRUNC (sysdate),last_day(v_month))
				              	   AND HOLIDAY_FL = 'Y'
				              	    AND C1900_COMPANY_ID = v_company_id
								);
	    EXCEPTION
			WHEN OTHERS
			THEN
					v_working_days	:= 0;	
	END;
	RETURN v_working_days;
END get_curr_month_work_days;
/