--@"C:\Database\Functions\Sales\get_rep_id_from_user_id.fnc"
CREATE OR REPLACE
    FUNCTION get_rep_id_from_user_id (
            p_user_id t101_user.c101_user_id%TYPE)
        RETURN VARCHAR2
    IS
        /**********************************************************************
        * Description     : Functions returns rep id based on user id
        *  Parameters  :p_user_id
        ***********************************************************************/
        v_rep_id T703_SALES_REP.C703_SALES_REP_ID%TYPE;
    BEGIN
         SELECT t703.c703_sales_rep_id
           INTO v_rep_id
           FROM T101_USER T101U, T101_PARTY T101, T703_SALES_REP T703
          WHERE T101U.C101_PARTY_ID = T101.C101_PARTY_ID
            AND T101.C101_PARTY_ID  = T703.C101_PARTY_ID
            AND T101U.C101_USER_ID  = p_user_id
            AND T101.C101_VOID_FL  IS NULL
            AND T703.C703_VOID_FL  IS NULL;
        RETURN v_rep_id;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN NULL;
    END;
/