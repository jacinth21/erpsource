/* Formatted on 2011/11/14 13:01 (Formatter Plus v4.8.0) */
--@"C:\Database\Functions\Sales\casemanagement\get_case_id.fnc";

/*	Description 	: THIS FUNCTION RETURNS case id by input case info id
 Parameters 		: p_caseinfo_id
*/

CREATE OR REPLACE FUNCTION get_case_id (
	p_caseinfo_id	IN	 t7100_case_information.c7100_case_info_id%TYPE
)
	RETURN VARCHAR2
IS
	v_case_id	   VARCHAR2 (20);
BEGIN
	BEGIN
		IF p_caseinfo_id IS NOT NULL
		THEN
			SELECT t7100.c7100_case_id
			  INTO v_case_id
			  FROM t7100_case_information t7100
			 WHERE t7100.c7100_case_info_id = p_caseinfo_id AND t7100.c7100_void_fl IS NULL;
		END IF;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN '';
	END;

	RETURN v_case_id;
END get_case_id;
/
