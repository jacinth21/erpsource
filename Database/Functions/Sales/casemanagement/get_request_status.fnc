/* Formatted on 2011/10/27 10:54 (Formatter Plus v4.8.0) */
--@"C:\Database\Functions\Sales\casemanagement\get_request_status.fnc";

CREATE OR REPLACE FUNCTION get_request_status (
	p_prodreqid   IN   t526_product_request_detail.c526_product_request_detail_id%TYPE
)
	RETURN VARCHAR2
IS
	v_statusfl	   t525_product_request.c525_status_fl%TYPE;
BEGIN
	SELECT c525_status_fl
	  INTO v_statusfl
	  FROM t525_product_request t525
	 WHERE t525.c525_product_request_id IN (
											SELECT c525_product_request_id
											  FROM t526_product_request_detail
											 WHERE c526_product_request_detail_id = p_prodreqid
												   AND c525_void_fl IS NULL)
	   AND t525.c525_void_fl IS NULL;

	RETURN v_statusfl;
EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		RETURN ' ';
END get_request_status;
/
