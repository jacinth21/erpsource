
--@"C:\Database\Functions\Sales\casemanagement\get_case_attr_name.fnc";

CREATE OR REPLACE FUNCTION   get_case_attr_name (
        p_code_grp IN t901_code_lookup.c901_code_grp%TYPE
      , p_case_info_id IN t7100_case_information.c7100_case_info_id%TYPE
    )
        RETURN VARCHAR2
    IS
        v_otherinfo       VARCHAR2 (200);
    BEGIN
        WITH other_info AS (
		    SELECT get_code_name(t7102.c901_attribute_type) pattr, t7102.c7102_case_attribute_id id
			  FROM t7102_case_attribute t7102 
			 WHERE t7102.c7100_case_info_id = p_case_info_id 
			   AND t7102.c7102_attribute_value = 'Y'
			   AND t7102.c7102_void_fl IS NULL
			   AND t7102.c901_attribute_type IN (SELECT c901_code_id
												   FROM t901_code_lookup
												  WHERE c901_code_grp = p_code_grp)
												  )									  
		SELECT MAX (SYS_CONNECT_BY_PATH (pattr, ','))
          INTO v_otherinfo
          FROM
            (SELECT pattr, ROW_NUMBER () OVER (ORDER BY id) AS curr
               FROM other_info)
          CONNECT BY curr - 1 = PRIOR curr
         START WITH curr = 1;
         RETURN SUBSTR (v_otherinfo, 2) ;
       EXCEPTION
          WHEN NO_DATA_FOUND
       THEN
          RETURN '';	
    END get_case_attr_name;
 /
     