/* Formatted on 2011/10/26 16:20 (Formatter Plus v4.8.0) */
--@"C:\Database\Functions\Sales\casemanagement\get_address_id.fnc";

CREATE OR REPLACE FUNCTION get_address_id (
	p_repid   IN   t703_sales_rep.c703_sales_rep_id%TYPE
)
	RETURN NUMBER
IS
	v_partyid	   t101_party.c101_party_id%TYPE;
	v_addressid    t106_address.c106_address_id%TYPE;
BEGIN
	v_partyid	:= get_rep_party_id (p_repid);

	SELECT c106_address_id
	  INTO v_addressid
	  FROM t106_address
	 WHERE c101_party_id = v_partyid AND c106_primary_fl = 'Y'
	   AND c106_void_fl IS NULL;

	RETURN v_addressid;
--AND c106_primary_fl = 'Y' AND c106_void_fl IS NULL;
EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		RETURN NULL;
END get_address_id;
/