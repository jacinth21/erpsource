/* Formatted on 2011/11/16 18:27 (Formatter Plus v4.8.0) */
--@"C:\Database\Functions\Sales\casemanagement\get_shipout_fl.fnc";

CREATE OR REPLACE FUNCTION get_shipout_fl (
	p_refid   IN   t907_shipping_info.c907_ref_id%TYPE
)
	RETURN VARCHAR2
IS
/*	Description 	: THIS FUNCTION RETURNS account for a case
 Parameters 		: p_caseid
*/
	v_shipoutcnt   NUMBER;
BEGIN
	SELECT COUNT (t907.c907_status_fl)
	  INTO v_shipoutcnt
	  FROM t907_shipping_info t907
	 WHERE t907.c907_ref_id = p_refid 
	   AND t907.c907_void_fl IS NULL 
	   AND t907.c907_status_fl = '40' 
	   AND t907.c901_source in (11385,11388);

	IF v_shipoutcnt = 1
	THEN
		RETURN 'Y';
	END IF;

	RETURN 'N';
EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		RETURN 'N';
END get_shipout_fl;
/
