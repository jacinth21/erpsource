/* Formatted on 2011/10/26 11:06 (Formatter Plus v4.8.0) */
--@"C:\Database\Functions\Sales\casemanagement\get_case_acct_id.fnc";

CREATE OR REPLACE FUNCTION get_case_acct_id (
	p_caseinfo_id	IN	 t7100_case_information.c7100_case_info_id%TYPE
  , p_casesetid   IN   t7104_case_set_information.c7104_case_set_id%TYPE
)
	RETURN VARCHAR2
IS
/*	Description 	: THIS FUNCTION RETURNS account for a case
 Parameters 		: p_caseid
*/
	v_acct_id	   t7100_case_information.c704_account_id%TYPE;
BEGIN
	BEGIN
		IF p_caseinfo_id IS NOT NULL
		THEN
			SELECT t7100.c704_account_id
			  INTO v_acct_id
			  FROM t7100_case_information t7100
			 WHERE t7100.c7100_case_info_id = p_caseinfo_id AND t7100.c7100_void_fl IS NULL;
		END IF;

		IF p_casesetid IS NOT NULL
		THEN
			SELECT t7100.c704_account_id
			  INTO v_acct_id
			  FROM t7100_case_information t7100
			 WHERE t7100.c7100_case_info_id IN (SELECT t7104.c7100_case_info_id
												  FROM t7104_case_set_information t7104
												 WHERE t7104.c7104_case_set_id = p_casesetid)
			   AND t7100.c7100_void_fl IS NULL;
		END IF;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN ' ';
	END;

	RETURN v_acct_id;
END get_case_acct_id;
/
