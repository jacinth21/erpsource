
--@"C:\Database\Functions\Sales\casemanagement\get_favouritecase_attr_name.fnc";

CREATE OR REPLACE FUNCTION   get_favouritecase_attr_name (
        p_code_grp 			IN t901_code_lookup.c901_code_grp%TYPE
      , p_favorite_case_id  IN t7120_favourite_case.c7120_favourite_case_id%TYPE
    )
        RETURN VARCHAR2
    IS
        v_otherinfo       VARCHAR2 (200);
    BEGIN
        WITH other_info AS (
		    SELECT get_code_name(t7122.c901_attribute_type) pattr, t7122.c7122_favourite_case_attr_id id
			  FROM t7122_favourite_case_attribute t7122 
			 WHERE t7122.c7120_favourite_case_id = p_favorite_case_id 
			   AND t7122.c7122_attribute_value = 'Y'
			   AND t7122.c7122_void_fl IS NULL
			   AND t7122.c901_attribute_type IN (SELECT c901_code_id
												   FROM t901_code_lookup
												  WHERE c901_code_grp = p_code_grp)
												  )									  
		SELECT MAX (SYS_CONNECT_BY_PATH (pattr, ','))
          INTO v_otherinfo
          FROM
            (SELECT pattr, ROW_NUMBER () OVER (ORDER BY id) AS curr
               FROM other_info)
          CONNECT BY curr - 1 = PRIOR curr
         START WITH curr = 1;
         RETURN SUBSTR (v_otherinfo, 2) ;
       EXCEPTION
          WHEN NO_DATA_FOUND
       THEN
          RETURN '';	
    END get_favouritecase_attr_name;
 /
     