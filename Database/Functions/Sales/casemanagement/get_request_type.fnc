/* Formatted on 2011/11/14 13:01 (Formatter Plus v4.8.0) */
--@"C:\Database\Functions\Sales\casemanagement\get_request_type.fnc";

/*	Description 	: THIS FUNCTION RETURNS t526 request type for p_product_request_id
 Parameters 		: p_product_request_id
*/

CREATE OR REPLACE FUNCTION get_request_type (
	p_product_request_id	IN	 t526_product_request_detail.c526_product_request_detail_id%TYPE
)
	RETURN VARCHAR2
IS
	v_request_type	   VARCHAR2 (20);
BEGIN
	BEGIN
		SELECT C901_REQUEST_TYPE
			  INTO v_request_type
			  FROM t526_product_request_detail 
			 WHERE c526_product_request_detail_id =  p_product_request_id
               AND c526_void_fl IS NULL ;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN '';
	END;

	RETURN v_request_type;
END get_request_type;
/
