/* Formatted on 2011/10/26 16:20 (Formatter Plus v4.8.0) */
--@"C:\Database\Functions\Sales\casemanagement\get_req_case_id.fnc";
/*This Function is used to get the case id from Request case id.
This is called when PD Approves the Request and when Mail is Sent to Respective Sales Rep.*/
CREATE OR REPLACE FUNCTION get_req_case_id (
	p_product_request_id   IN   t525_product_request.c525_product_request_id%TYPE
)
	RETURN VARCHAR2
IS
	v_case_id	   t7100_case_information.c7100_case_id%TYPE;
	
BEGIN
	

	SELECT t7100.c7100_case_id  
			  INTO v_case_id
			  FROM t7104_case_set_information t7104, t526_product_request_detail t526, t7100_case_information t7100
			 WHERE t7104.c526_product_request_detail_id = t526.c526_product_request_detail_id 
               AND t526.c525_product_request_id = p_product_request_id 
               AND t7100.c901_case_status != 19526 --  19526 - Rescheduled
               AND t7100.c7100_void_fl IS NULL                
               AND t7100.c7100_case_info_id = t7104.c7100_case_info_id
               AND t7104.c7104_void_fl IS NULL 
               AND ROWNUM=1 ;


	RETURN v_case_id;
EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		RETURN NULL;
END get_req_case_id;
/