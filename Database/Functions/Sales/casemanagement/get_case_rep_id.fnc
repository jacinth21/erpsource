/* Formatted on 2011/10/26 10:45 (Formatter Plus v4.8.0) */
--@"C:\Database\Functions\Sales\casemanagement\get_case_rep_id.fnc";

CREATE OR REPLACE FUNCTION get_case_rep_id (
	p_caseinfo_id	IN	 t7100_case_information.c7100_case_info_id%TYPE
  , p_casesetid   IN   t7104_case_set_information.c7104_case_set_id%TYPE
)
	RETURN NUMBER
IS
/*	Description 	: THIS FUNCTION RETURNS sales rep for a case
 Parameters 		: p_caseid
*/
	v_rep_id	   t7100_case_information.c703_sales_rep_id%TYPE;
BEGIN
	BEGIN
		IF p_caseinfo_id IS NOT NULL
		THEN
			SELECT t7100.c703_sales_rep_id
			  INTO v_rep_id
			  FROM t7100_case_information t7100
			 WHERE t7100.c7100_case_info_id = p_caseinfo_id AND t7100.c7100_void_fl IS NULL;
		END IF;

		IF p_casesetid IS NOT NULL
		THEN
			SELECT t7100.c703_sales_rep_id
			  INTO v_rep_id
			  FROM t7100_case_information t7100
			 WHERE t7100.c7100_case_info_id IN (SELECT t7104.c7100_case_info_id
												  FROM t7104_case_set_information t7104
												 WHERE t7104.c7104_case_set_id = p_casesetid)
			   AND t7100.c7100_void_fl IS NULL;
		END IF;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN '';
	END;

	RETURN v_rep_id;
END get_case_rep_id;
/
