/* Formatted on 2011/12/23 11:50 (Formatter Plus v4.8.0) */
--@"C:\Database\Functions\Sales\casemanagement\get_do_id.fnc";

/*	Description 	: THIS FUNCTION RETURNS DO id by input case info id
 Parameters 		: p_caseinfo_id
*/

CREATE OR REPLACE FUNCTION get_do_id (
	p_caseinfo_id	IN	 t7100_case_information.c7100_case_info_id%TYPE
)
	RETURN VARCHAR2
IS
	v_do_id 	   VARCHAR2 (30);
BEGIN
	BEGIN
		IF p_caseinfo_id IS NOT NULL
		THEN
			SELECT NVL(c501_parent_order_id , c501_order_id)
			  INTO v_do_id
			  FROM t501_order
			 WHERE c7100_case_info_id = p_caseinfo_id 
			   AND c501_void_fl IS NULL
			   AND c501_parent_order_id IS NULL;
		END IF;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN '';
	END;

	RETURN v_do_id;
END get_do_id;
/
