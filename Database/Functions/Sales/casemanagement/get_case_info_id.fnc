/* Formatted on 2011/11/14 13:01 (Formatter Plus v4.8.0) */
--@"C:\Database\Functions\Sales\casemanagement\get_case_info_id.fnc";

/*	Description 	: THIS FUNCTION RETURNS case id by input case info id
 Parameters 		: p_caseinfo_id
*/

CREATE OR REPLACE FUNCTION get_case_info_id (
	p_product_request_id	IN	 t526_product_request_detail.C525_PRODUCT_REQUEST_ID%TYPE
)
	RETURN VARCHAR2
IS
	v_case_info_id	   VARCHAR2 (20);
BEGIN
	BEGIN
		IF p_product_request_id IS NOT NULL
		THEN
			SELECT t7104.c7100_case_info_id  
			  INTO v_case_info_id
			  FROM t7104_case_set_information t7104, t526_product_request_detail t526, t7100_case_information t7100
			 WHERE t7104.c526_product_request_detail_id = t526.c526_product_request_detail_id 
               AND t526.c525_product_request_id = p_product_request_id 
               AND t7100.c901_case_status != 19526 --  19526 - Rescheduled
               AND t7100.c7100_void_fl IS NULL 
               AND t7100.c901_type = 1006504 --1006504-EVENT
               AND t7100.c7100_case_info_id = t7104.c7100_case_info_id
               AND t7104.c7104_void_fl IS NULL 
               -- AND t526.c526_void_fl IS NULL 
               AND ROWNUM=1 ;
		END IF;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN '';
	END;

	RETURN v_case_info_id;
END get_case_info_id;
/
