--@"C:\Database\Functions\Sales\casemanagement\get_case_status.fnc";
CREATE OR REPLACE FUNCTION get_case_status (
	p_caseinfoid	IN	 t7100_case_information.c7100_case_info_id%TYPE
)
	RETURN NUMBER
IS
	v_case_status  NUMBER;
BEGIN
	IF p_caseinfoid IS NOT NULL
	THEN
		SELECT c901_case_status
		  INTO v_case_status
		  FROM t7100_case_information
		 WHERE c7100_case_info_id = p_caseinfoid AND c7100_void_fl IS NULL;
	END IF;

	RETURN v_case_status;
EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		RETURN 0;
END get_case_status;
/