--@"C:\Database\Functions\Sales\casemanagement\get_type.fnc";
CREATE OR REPLACE FUNCTION get_type (
	p_caseinfoid	IN	 t7100_case_information.c7100_case_info_id%TYPE
)
	RETURN NUMBER
IS
	v_type  NUMBER;
BEGIN
	IF p_caseinfoid IS NOT NULL
	THEN
		SELECT c901_type
		  INTO v_type
		  FROM t7100_case_information
		 WHERE c7100_case_info_id = p_caseinfoid AND c7100_void_fl IS NULL;
	END IF;

	RETURN v_type;
EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		RETURN 0;
END get_type;
/