/* Formatted on 2011/12/11 18:45 (Formatter Plus v4.8.0) */

--@"C:\Database\Functions\Sales\casemanagement\get_lock_date.fnc";
/**********************************************************************
 *	Description	   : Functions returns Case Lock Date
 	
 ***********************************************************************/

CREATE OR REPLACE FUNCTION get_lock_date (
	p_surgery_date	 DATE
  , p_locktype		 VARCHAR2
)
	RETURN DATE
IS
	v_mode_value   VARCHAR2 (10);
	v_lock_date    DATE;
	v_surgery_date DATE;
	v_sys_value    VARCHAR2 (10);
BEGIN
--
	SELECT TO_CHAR (MOD (TO_CHAR (p_surgery_date, 'J'), 7) + 1)
	  INTO v_mode_value
	  FROM DUAL;

	SELECT TO_CHAR (MOD (TO_CHAR (SYSDATE, 'J'), 7) + 1)
	  INTO v_sys_value
	  FROM DUAL;

	v_lock_date := p_surgery_date + get_rule_value (v_mode_value, p_locktype);

	IF (v_sys_value = 6 AND v_lock_date < SYSDATE)
	THEN
		v_lock_date := SYSDATE + 2;
	END IF;

	IF (v_sys_value = 7 AND v_lock_date < SYSDATE)
	THEN
		v_lock_date := SYSDATE + 1;
	END IF;

	IF (v_sys_value != 7 AND v_sys_value != 6 AND v_lock_date < SYSDATE)
	THEN
		v_lock_date := SYSDATE;
	END IF;

	RETURN v_lock_date;
EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		RETURN NULL;
END get_lock_date;
/
