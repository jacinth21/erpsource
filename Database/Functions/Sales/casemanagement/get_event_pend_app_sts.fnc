--@"C:\Database\Functions\Sales\casemanagement\get_event_pend_app_sts.fnc";
CREATE OR REPLACE FUNCTION get_event_pend_app_sts (
	p_caseinfoid	IN	 t7100_case_information.c7100_case_info_id%TYPE
)
	RETURN VARCHAR2
IS
	v_case_status	VARCHAR2(1);
BEGIN
	IF p_caseinfoid IS NOT NULL
	THEN
		SELECT (CASE WHEN count(1)>0 THEN 'Y' ELSE 'N' END) 
			INTO v_case_status
			FROM  t526_product_request_detail t526, t7104_case_set_information t7104
		WHERE  t526.c526_product_request_detail_id = t7104.c526_product_request_detail_id
		AND t7104.c7100_case_info_id = p_caseinfoid
		AND t526.C526_Status_Fl		IN (5) 
		AND t7104.c7104_void_fl 	IS NULL
		AND t526.c526_void_fl   	IS NULL 
		AND TRUNC(c526_required_date) < TRUNC(sysdate);
	END IF;

	RETURN v_case_status;
EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		RETURN 0;
END get_event_pend_app_sts;
/