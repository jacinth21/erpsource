/* Formatted on 2011/10/27 10:54 (Formatter Plus v4.8.0) */
--@"C:\Database\Functions\Sales\casemanagement\get_set_pending_return.fnc";

CREATE OR REPLACE FUNCTION get_set_pending_return(
	p_case_info_id IN t7100_case_information.c7100_case_info_id%TYPE
)
	RETURN VARCHAR2
IS
	v_count NUMBER;
BEGIN

	 SELECT COUNT (1) INTO v_count
	   FROM t907_shipping_info
	  WHERE c907_ref_id IN
	    (
	         SELECT c7104_case_set_id
	           FROM t7104_case_set_information
	          WHERE c7100_case_info_id = p_case_info_id
	            AND c7104_void_fl     IS NULL
	    )
	    AND c901_source   IN (11388)
	    AND c907_status_fl = 30;
	   -- AND c907_void_fl  IS NULL; --cancel case should also show pending return value
	
    IF v_count > 0 THEN
    	RETURN 'Yes';
    ELSE
    	RETURN 'No';
    END IF;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN 'No';
END get_set_pending_return;
/