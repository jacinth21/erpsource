/* Formatted on 2011/10/27 10:54 (Formatter Plus v4.8.0) */
--@"C:\Database\Functions\Sales\casemanagement\get_pending_po.fnc";

CREATE OR REPLACE FUNCTION get_pending_po(
	p_case_info_id IN t7100_case_information.c7100_case_info_id%TYPE
)
	RETURN VARCHAR2
IS
	v_count NUMBER;
BEGIN

	 SELECT COUNT(1) 
	   INTO v_count
	   FROM t501_order
	  WHERE c7100_case_info_id = p_case_info_id
	  	AND c501_customer_po  IS NULL
	    AND c501_void_fl      IS NULL;
	
	IF v_count > 0 THEN
    	RETURN 'Yes';
    ELSE
    	RETURN 'No';
    END IF;
    
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN 'No';
END get_pending_po;
/
