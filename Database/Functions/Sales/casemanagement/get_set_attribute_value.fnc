/* Formatted on 2009/08/27 18:33 (Formatter Plus v4.8.0) */
--@"C:\DATABASE\Functions\Sales\casemanagement\get_set_attribute_value.fnc";
CREATE OR REPLACE FUNCTION get_set_attribute_value(
	p_set_id	 t207c_set_attribute.c207_set_id%TYPE
  , p_type	 	 t207c_set_attribute.c901_attribute_type%TYPE
)
	RETURN VARCHAR2
IS
	 
	v_value 	   t207c_set_attribute.c207c_attribute_value%TYPE;
BEGIN
	BEGIN
		SELECT c207c_attribute_value
		  INTO v_value
		  FROM t207c_set_attribute
		 WHERE c207_set_id = p_set_id AND c901_attribute_type = p_type;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN NULL;
	END;

	RETURN v_value;
END get_set_attribute_value;
/