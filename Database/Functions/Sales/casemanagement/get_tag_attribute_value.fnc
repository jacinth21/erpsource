/* Formatted on 2009/08/27 18:33 (Formatter Plus v4.8.0) */
--@"C:\DATABASE\Functions\Sales\casemanagement\get_tag_attribute_value.fnc";
CREATE OR REPLACE FUNCTION get_tag_attribute_value(
	p_tag_id	 t5010a_tag_attribute.c5010_tag_id%TYPE
  , p_type	 	 t5010a_tag_attribute.c901_attribute_type%TYPE
)
	RETURN VARCHAR2
IS
	 
	v_value 	   t5010a_tag_attribute.c5010a_attribute_value%TYPE;
BEGIN
	BEGIN
		SELECT c5010a_attribute_value
		  INTO v_value
		  FROM t5010a_tag_attribute
		 WHERE c5010_tag_id = p_tag_id AND c901_attribute_type = p_type;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN NULL;
	END;

	RETURN v_value;
END get_tag_attribute_value;
/