/* Formatted on 2011/10/27 10:54 (Formatter Plus v4.8.0) */
--@"C:\Database\Functions\Sales\casemanagement\get_image_count.fnc";

CREATE OR REPLACE FUNCTION get_image_count(
	p_ref_id IN t903_upload_file_list.c903_ref_id%TYPE
)
	RETURN NUMBER
IS
	v_count NUMBER;
BEGIN

	 SELECT count(1)
	   INTO v_count
	   FROM t903_upload_file_list 
	  WHERE c903_ref_id = p_ref_id
	    AND c903_delete_fl IS NULL;

    	RETURN v_count;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN 0;
END get_image_count;
/
