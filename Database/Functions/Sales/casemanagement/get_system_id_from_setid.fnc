/* Formatted on 2011/10/27 16:04 (Formatter Plus v4.8.0) */

--@"C:\Database\Functions\Sales\casemanagement\get_system_id_from_setid.fnc";

CREATE OR REPLACE FUNCTION get_system_id_from_setid (
	p_set_id   t207_set_master.c207_set_id%TYPE
)
	RETURN VARCHAR2
IS
/*	Description 	: THIS FUNCTION RETURNS SYSTEMID GIVEN THE SETID
 Parameters 		: p_setid
  */
	v_sys_id	   t207_set_master.c207_set_id%TYPE;
BEGIN
	BEGIN
		IF (SUBSTR (p_set_id, 1, 3) = 'SET')
		THEN
			SELECT t207a.c207_main_set_id
			  INTO v_sys_id
			  FROM t207a_set_link t207a
			 WHERE c207_link_set_id = (SELECT c207_main_set_id
										 FROM t207a_set_link
										WHERE c207_link_set_id = p_set_id)
			   AND t207a.c207_main_set_id LIKE '300%'
			   AND t207a.c207_main_set_id NOT LIKE '%LN%';
		ELSE
			SELECT t207a.c207_main_set_id
			  INTO v_sys_id
			  FROM t207a_set_link t207a
			 WHERE c207_link_set_id = p_set_id
			   AND t207a.c207_main_set_id LIKE '300%'
			   AND t207a.c207_main_set_id NOT LIKE '%LN%';
		END IF;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN '';
	END;

	RETURN v_sys_id;
END get_system_id_from_setid;
