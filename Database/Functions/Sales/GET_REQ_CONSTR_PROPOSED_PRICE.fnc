--@"C:\Database\Functions\Sales\GET_REQ_CONSTR_PROPOSED_PRICE.fnc";

create or replace
FUNCTION get_req_constr_proposed_price
(
	 p_request_id    T7000_ACCOUNT_PRICE_REQUEST.C7000_ACCOUNT_PRICE_REQUEST_ID%TYPE
	,p_constr_id 	t7004_construct_mapping.c7003_system_construct_id %TYPE
)
RETURN NUMBER
IS
	v_acc_id    t704_account.c704_account_id%TYPE;
	v_gpo_id 		t101_user.c101_party_id%TYPE;
	v_req_pro_price T7001_GROUP_PART_PRICING.C7001_ADJUSTED_PRICE%TYPE;
	
	BEGIN
		-- Get the price of group that is initiated and price of group(s) that are not in the request but already approved, 
		     -- this will give the price of the whole construct proposed price.
		
		SELECT c704_account_id , c101_gpo_id INTO v_acc_id , v_gpo_id
		FROM t7000_account_price_request WHERE c7000_account_price_request_id = p_request_id;
	
		SELECT DECODE(v_gpo_id,'',v_acc_id,'') INTO v_acc_id FROM DUAL;
		BEGIN
			SELECT SUM(qty * price) INTO v_req_pro_price  FROM (
				SELECT c7004_qty qty, c7001_adjusted_price price FROM
					t7004_construct_mapping t7004,t7001_group_part_pricing t7001 
					WHERE t7004.c4010_group_id = t7001.c7001_ref_id
					AND t7001.c901_ref_type = 52000 -- GROUP
					AND c7003_system_construct_id  = p_constr_id
					AND c7000_account_price_request_id = p_request_id
			UNION ALL
				SELECT c7004_qty qty ,c7051_price price
				FROM t7004_construct_mapping t7004,t7051_account_group_pricing t7051 
				WHERE t7004.c4010_group_id = t7051.c7051_ref_id
				AND t7051.c901_ref_type = 52000 -- GROUP
				AND c7003_system_construct_id= p_constr_id
				AND NVL(c704_account_id,'-999') = NVL(v_acc_id,'-999') 
				AND NVL(c101_gpo_id,'-999') = NVL(v_gpo_id,'-999')
				AND c7051_ref_id NOT IN (
					SELECT c7001_ref_id
					FROM t7004_construct_mapping t7004,t7001_group_part_pricing t7001 
					WHERE t7004.c4010_group_id = t7001.c7001_ref_id
					AND t7001.c901_ref_type = 52000 --GROUP
					AND c7003_system_construct_id = p_constr_id
					AND C7001_ADJUSTED_PRICE >0
					AND c7000_account_price_request_id = p_request_id
				)
			);
 		EXCEPTION
      	WHEN NO_DATA_FOUND
   		THEN
      		RETURN 0;
      	END;
   RETURN v_req_pro_price;
   
END get_req_constr_proposed_price;
/