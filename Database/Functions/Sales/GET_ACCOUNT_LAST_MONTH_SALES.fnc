CREATE OR REPLACE FUNCTION GET_ACCOUNT_LAST_MONTH_SALES
(
 	   p_acc_id T501_ORDER.C704_ACCOUNT_ID%TYPE
)
RETURN VARCHAR2
IS
/*  Description     : THIS FUNCTION RETURNS CODE NAME GIVEN THE CODE_ID
 Parameters 		: p_code_id
*/
v_total NUMBER(15,2);
v_Frmdate VARCHAR2(20);
v_ToDate VARCHAR2(20);
BEGIN
     BEGIN
	 	 --select to_char(sysdate-2,'mm') || '/01/' || to_char(sysdate,'yyyy') into v_Frmdate from dual;
		 --select '12/01/' || to_char(sysdate,'yyyy') into v_Frmdate from dual;
		 select to_char(last_day(add_months(sysdate,-2))+1,'mm/dd/yyyy') into v_FrmDate from dual;
		 select to_char(last_day(add_months(sysdate,-1)),'mm/dd/yyyy') into v_ToDate from dual;
		 SELECT
		 	 --SUM(C501_TOTAL_COST) + sum(decode(C501_SHIP_COST,'',0,C501_SHIP_COST)) SALES INTO v_total
			 SUM(C501_TOTAL_COST) SALES INTO v_total
		 FROM
		 	  T501_ORDER
		 WHERE
		 	  C501_ORDER_DATE BETWEEN to_date(v_FrmDate,'mm/dd/yyyy') AND to_date(v_ToDate,'mm/dd/yyyy')
		 	 --to_char(C501_ORDER_DATE,'mm/dd/yyyy') > v_Frmdate
		 --AND
		 	 --to_char(C501_ORDER_DATE,'mm/dd/yyyy') <= v_ToDate
		 AND
		 	 C501_VOID_FL IS NULL AND C501_DELETE_FL IS NULL
		 AND
		 	 C704_ACCOUNT_ID = p_acc_id;
   EXCEPTION
      WHEN NO_DATA_FOUND
   THEN
      RETURN ' ';
      END;
     RETURN v_total;
END GET_ACCOUNT_LAST_MONTH_SALES;
/

