--@"C:\Database\Functions\Sales\GET_TAG_ID.fnc";

CREATE OR REPLACE FUNCTION GET_TAG_ID
(
    p_ref_id  T5010_Tag.c5010_last_updated_trans_id%TYPE
)
RETURN VARCHAR2
IS
/*******************************************************
 * Description     : THIS FUNCTION RETURNS TAG ID
 * Parameters      : p_ref_id
 ******************************************************/
--
v_tagid  VARCHAR2 (20);
--
BEGIN
--
    SELECT t5010.c5010_tag_id
      INTO v_tagid
      FROM T5010_Tag T5010
     WHERE t5010.c5010_last_updated_trans_id = p_ref_id
       AND t5010.C5010_Void_Fl IS NULL
       AND t5010.c901_status <> 51013 and rownum = 1;
--
 RETURN v_tagid;
--
EXCEPTION
WHEN NO_DATA_FOUND
THEN
--
    RETURN ' ';
--
END GET_TAG_ID;
/
