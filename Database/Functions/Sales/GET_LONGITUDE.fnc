-- @"C:\Database\Functions\Sales\GET_LONGITUDE.fnc";

create or replace
FUNCTION GET_LONGITUDE (
p_id t703_sales_rep.c703_sales_rep_id%TYPE
,p_type varchar2
,p_lon_type varchar2
)

RETURN VARCHAR2
IS

  v_ship_lon  VARCHAR2(20);
  v_bill_lon  VARCHAR2(20);

BEGIN
  
  IF p_type='ADDR' 
  THEN 
  BEGIN
      SELECT c106_longitude 
        INTO v_ship_lon
        FROM t106_address
        WHERE c106_address_id = p_id
        AND c106_void_fl IS NULL;
        EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		v_ship_lon := NULL;
    END;
  ELSIF p_type='DIST'  
  THEN 
  BEGIN
      SELECT C701_ship_longitude,C701_bill_longitude
         INTO v_ship_lon,v_bill_lon
         FROM t701_distributor
         WHERE c701_void_fl IS NULL
         AND c701_distributor_id = p_id;
         EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		v_ship_lon := NULL;
		v_bill_lon := NULL;
    END;
  ELSIF p_type='ACC'
  THEN
  BEGIN
      SELECT C704_ship_longitude,C704_bill_longitude
        INTO v_ship_lon,v_bill_lon
        FROM t704_account
        WHERE c704_account_id = p_id
        AND c704_void_fl IS NULL;
        EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		v_ship_lon := NULL;
		v_bill_lon := NULL;	 
    END;
  END IF;   
  
  IF p_lon_type = 'BILL'
  THEN
        RETURN v_bill_lon;
  ELSE 
  		RETURN v_ship_lon;
  END IF;
END get_longitude;
/