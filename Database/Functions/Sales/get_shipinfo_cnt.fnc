/* Formatted on 2011/11/16 18:27 (Formatter Plus v4.8.0) */
--@"C:\Database\Functions\Sales\get_shipinfo_cnt.fnc";

CREATE OR REPLACE FUNCTION get_shipinfo_cnt (
	p_refid   IN   t907_shipping_info.c907_ref_id%TYPE
)
	RETURN VARCHAR2
IS
/*	Description 	: THIS FUNCTION Checks if there is a shipping record for the refid or not.
 Parameters 		: p_refid
*/
	v_shipoutcnt   NUMBER;
BEGIN
	SELECT COUNT (t907.c907_status_fl)
	  INTO v_shipoutcnt
	  FROM t907_shipping_info t907
	 WHERE t907.c907_ref_id = p_refid AND t907.c907_void_fl IS NULL;


	RETURN v_shipoutcnt;

END get_shipinfo_cnt;
/
