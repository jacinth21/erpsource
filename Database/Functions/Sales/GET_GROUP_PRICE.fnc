/* Formatted on 2010/02/18 15:29 (Formatter Plus v4.8.0) */
CREATE OR REPLACE FUNCTION get_group_price (
	p_request_id   IN	t7000_account_price_request.c7000_account_price_request_id%TYPE
  , p_groupid	   IN	t4010_group.c4010_group_id%TYPE
  , p_price_type   IN	t7010_group_price_detail.c901_price_type%TYPE
)
	RETURN NUMBER
IS
/*
 Description  : this function returns price for group , list, AD, VP , TripWire
 Parameters  : p_groupid
 Parameters  : p_price_type
*/
	v_price 	   t705_account_pricing.c705_unit_price%TYPE;
	v_pricing_id   t7002_group_pricing_detail.c7002_price%TYPE;
BEGIN
	--
	BEGIN
		--IF (p_request_id IS NULL)
		--THEN
			SELECT c7010_price
			  INTO v_price
			  FROM t7010_group_price_detail t7010
			 WHERE t7010.c4010_group_id = p_groupid
			   AND t7010.c901_price_type = p_price_type
			   AND t7010.c7010_void_fl IS NULL;
	/*	ELSE
			SELECT t7001.c7001_group_part_pricing_id
			  INTO v_pricing_id
			  FROM t7001_group_part_pricing t7001
			 WHERE t7001.c7001_ref_id = p_groupid
			   AND t7001.c7000_account_price_request_id = p_request_id
			   AND t7001.c7001_void_fl IS NULL
         and t7001.c7001_demo_flg!=1;
  
			SELECT c7002_price
			  INTO v_price
			  FROM t7002_group_pricing_detail t7002
			 WHERE t7002.c7001_group_part_pricing_id = v_pricing_id AND t7002.c901_price_type = p_price_type;
     
		END IF;
    */
	END;

	RETURN v_price;
EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
    RETURN 0;
--
END get_group_price;
/
