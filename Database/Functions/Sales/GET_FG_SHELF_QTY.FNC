--@"C:\Database\Functions\Sales\GET_FG_SHELF_QTY.FNC"
CREATE OR REPLACE
FUNCTION "GET_FG_SHELF_QTY" (
  p_part_num	 t205_part_number.c205_part_number_id%TYPE
  )
  RETURN NUMBER
  IS
  BEGIN

    RETURN get_qty_in_stock(p_part_num); -- Get the FG Qty

  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    RETURN 0;

END GET_FG_SHELF_QTY;
/