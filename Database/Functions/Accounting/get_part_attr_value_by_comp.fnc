/* Formatted on 2009/08/27 18:33 (Formatter Plus v4.8.0) */
--@"C:\DATABASE\Functions\Accounting\get_part_attr_value_by_comp.fnc";
CREATE OR REPLACE
    FUNCTION get_part_attr_value_by_comp (
            p_num t205d_part_attribute.c205_part_number_id%TYPE,
            p_type t205d_part_attribute.c901_attribute_type%TYPE,
            p_company_id    t1900_company.c1900_company_id%TYPE)
        RETURN VARCHAR2
    IS
        v_value t205d_part_attribute.c205d_attribute_value%TYPE;
    BEGIN
        --
        BEGIN
             SELECT c205d_attribute_value
               INTO v_value
               FROM t205d_part_attribute
              WHERE c205_part_number_id = p_num
                AND c901_attribute_type = p_type
                AND c1900_company_id    = p_company_id
                AND c205d_void_fl      IS NULL;
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_value := NULL;
        END;
        RETURN v_value;
    END get_part_attr_value_by_comp;
    /