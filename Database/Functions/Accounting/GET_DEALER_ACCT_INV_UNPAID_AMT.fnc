CREATE OR REPLACE FUNCTION GET_DEALER_ACCT_INV_UNPAID_AMT
(
     p_dealer_Id     t704_account.c101_dealer_id%TYPE,
     p_account_Id    t704_account.c704_account_id%TYPE,
     p_dt_issue_from t503b_invoice_attribute.c503b_date_of_issue_from%TYPE
)
RETURN NUMBER
IS
/*  Description     : THIS FUNCTION RETURNS THE UNPAID AMOUNT FOR DEALER/ACCOUNT
*/
v_total NUMBER;
BEGIN
     BEGIN
     
     IF p_dealer_Id IS NOT NULL
     THEN
     
		SELECT ROUND(NVL(SUM(NVL((INVAMT - AMTPAID),0)),0)) INTO v_total
		FROM
		  (SELECT NVL(C503_INV_AMT, 0) INVAMT,
		    NVL(C503_INV_PYMNT_AMT, 0) AMTPAID
		  FROM T503_INVOICE  
		  WHERE C503_VOID_FL     IS NULL
		  AND c101_dealer_id      = p_dealer_Id
		  AND C503_STATUS_FL     IN (0,1)
		  AND C901_INVOICE_SOURCE = 26240213
		  AND TRUNC(c503_invoice_date ) < TRUNC(p_dt_issue_from)
		  );
		  
	 ELSE
	 
	 	SELECT ROUND(NVL(SUM(NVL((INVAMT - AMTPAID),0)),0)) INTO v_total
		FROM
		  (SELECT NVL(T503.C503_INV_AMT, 0) INVAMT,
		    NVL(T503.C503_INV_PYMNT_AMT, 0) AMTPAID
		  FROM T503_INVOICE T503 ,
		    T704_ACCOUNT T704
		  WHERE T503.C503_VOID_FL     IS NULL
		  AND T503.C704_ACCOUNT_ID     = T704.C704_ACCOUNT_ID
		  AND T503.C704_ACCOUNT_ID      = p_account_Id
		  AND T503.C503_STATUS_FL     IN (0,1)
		  AND T503.C901_INVOICE_SOURCE = 50255
		  AND TRUNC(T503.c503_invoice_date ) < TRUNC(p_dt_issue_from)
		  );
	 
	 
	 END IF;
	
	
	
   EXCEPTION
      WHEN NO_DATA_FOUND
   THEN
      RETURN 0;
      END;
     RETURN v_total;
END GET_DEALER_ACCT_INV_UNPAID_AMT;
/

