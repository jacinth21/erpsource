--@"C:\Database\Functions\Accounting\GET_CUSTOM_PARTNUM.fnc";
/*  Description     : This function returns the custom part description.
 * Parameters 		: p_ref_id, p_part_num
*/
CREATE OR REPLACE FUNCTION GET_CUSTOM_PARTNUM (
 	p_ref_id   T205e_custom_part_number.c205e_ref_value%TYPE,    
	p_part_num T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE
 )
RETURN VARCHAR2
IS
    v_part_num t205e_custom_part_number.c205e_cust_part_number%TYPE;
    v_part_desc t205e_custom_part_number.c205e_cust_part_desc%TYPE;
BEGIN
    BEGIN
         SELECT     c205e_cust_part_number, c205e_cust_part_desc
               INTO v_part_num            , v_part_desc
               FROM t205e_custom_part_number
              WHERE c205_part_number_id = p_part_num
                AND c205e_ref_value = p_ref_id
                AND c205e_void_fl IS NULL;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN NULL;
    END;
RETURN v_part_num;
END;
/
