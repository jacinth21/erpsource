-- @"C:\Database\Functions\Accounting\get_run_date.fnc"
CREATE OR REPLACE
    FUNCTION get_run_date (
            p_txn_type t901_code_lookup.c901_code_id%TYPE)
        RETURN DATE
    IS
        /* Description  : THIS FUNCTION RETURNS NEXT ROLLFORWARD ID
        Parameters   : TRANS TYPE
        */
        v_run_date DATE;
        --
    BEGIN
        BEGIN
             SELECT c9900_last_run_date
               INTO v_run_date
               FROM T9900_run
              WHERE c901_type = p_txn_type;
            -- assign the values.
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_run_date := TO_DATE('01/01/2000','MM/dd/yyyy');
        END;
        RETURN v_run_date;
    END get_run_date;
    / 