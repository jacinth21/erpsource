--@"C:\Database\Functions\Accounting\get_valid_tag_ref_id.fnc";
CREATE OR REPLACE FUNCTION get_valid_tag_ref_id (
	p_txn	IN	 t5010_tag.c5010_last_updated_trans_id%TYPE
)
	RETURN NUMBER
IS
/***********************************************************************************************
 *	  Description	: This function will return transaction type based on the transaction
 ************************************************************************************************/
--
	v_txn_cnt	   t901_code_lookup.c901_code_id%TYPE;
--
BEGIN
	--
	SELECT COUNT (1)
	  INTO v_txn_cnt
	  FROM t504_consignment
	 WHERE c504_consignment_id = p_txn;
	--
	IF v_txn_cnt = 0 THEN
		SELECT COUNT (1)
		  INTO v_txn_cnt
		  FROM t506_returns
		 WHERE c506_rma_id = p_txn;
	END IF;
	IF v_txn_cnt = 0 THEN
		SELECT COUNT (1)
		  INTO v_txn_cnt
		  FROM t412_inhouse_transactions
		 WHERE c412_inhouse_trans_id = p_txn;
	END IF;
	IF v_txn_cnt = 0 THEN
		SELECT COUNT (1)
		  INTO v_txn_cnt
		  FROM t920_transfer
		 WHERE c920_transfer_id = p_txn;
	END IF;
	RETURN v_txn_cnt;
--

--
EXCEPTION
	WHEN NO_DATA_FOUND THEN
--
		RETURN 0;
--
END get_valid_tag_ref_id;
/