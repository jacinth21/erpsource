CREATE OR REPLACE FUNCTION GET_POSTING_TYPE
(
 p_posting_type t804_posting_ref.c901_posting_type%TYPE
)
RETURN VARCHAR2
IS
/***********************************************************************************************
 *  Description   : Based on the  posting id system will return posting type
 *
 *  Return Parameters  : posting_type
 ************************************************************************************************/
v_txn_post_type CHAR(1);
BEGIN
  --
     -- Check if the amount is mapped to COGS Account
     -- 4806, 4807 , 4808 ** COGS Value Posting
     -- 4806 - Actual Value TO COGS
     -- 4807 - COSG (includes transfer)
     -- 4808 - COSG Return
     IF (   (p_posting_type = 4807)
         OR (p_posting_type = 4808)
         OR (p_posting_type = 4806)
        )
     THEN
        --
        v_txn_post_type := 'C';
     --
     ELSE
        --
        v_txn_post_type  := 'A';
     --
     END IF;
 --
 RETURN v_txn_post_type;
 --
--
END GET_POSTING_TYPE;
/

