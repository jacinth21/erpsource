CREATE OR REPLACE FUNCTION qb_get_account_shipto
(
 p_qb_account_listid gm_qb_account_mapping.qb_account_listid%TYPE
)
RETURN NUMBER
IS
--
v_qb_shipto_flag gm_qb_account_mapping.qb_shipto_flag%TYPE;
v_c901_code_id  t901_code_lookup.c901_code_id%TYPE;
--
BEGIN
 --
 SELECT qb_shipto_flag
 INTO  v_qb_shipto_flag
 FROM gm_qb_account_mapping
 WHERE qb_account_listid =  p_qb_account_listid;
 --
 IF ( v_qb_shipto_flag = 'H')
 THEN
  --
  v_c901_code_id := 4122;
  --
 ELSIF ( v_qb_shipto_flag = 'R')
 THEN
  --
  v_c901_code_id := 4121;
  --
 ELSIF ( v_qb_shipto_flag = 'D')
 THEN
  --
  v_c901_code_id := 4120;
  --
 ELSIF ( v_qb_shipto_flag = 'E')
 THEN
  --
  v_c901_code_id := 4123;
  --
 ELSE
  v_c901_code_id := NULL;
 END IF;
 --
 RETURN v_c901_code_id ;
EXCEPTION
 WHEN NO_DATA_FOUND
    THEN
      RETURN NULL;
END qb_get_account_shipto;
/

