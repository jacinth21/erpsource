/* Formatted on 2008/11/26 15:16 (Formatter Plus v4.8.0) */
CREATE OR REPLACE FUNCTION get_account_opening_balance (
	p_acct_elem_type   t801_account_element.c801_account_element_id%TYPE
  , p_from_dt		   DATE
)
	RETURN NUMBER
IS
/***********************************************************************************************
 *	Description   : This function will return opening balance
 *		  for the selected period and for the selected Account Element
 *
 *	Return Parameters  : Opening Balance
 ************************************************************************************************/
--
	p_opening_balance NUMBER (15, 2);
	v_company_id_ctx  t1900_company.c1900_company_id%TYPE;
--
BEGIN
	--
	 SELECT get_compid_frm_cntx() 
      	INTO v_company_id_ctx  
      FROM dual;
      --
	SELECT NVL (SUM (v810.c810_qty * v810.c810_dr_amt) - SUM (v810.c810_qty * v810.c810_cr_amt), 0)
	  INTO p_opening_balance
	  FROM v810_posting_txn v810
	 WHERE v810.c801_account_element_id = p_acct_elem_type AND v810.c810_txn_date < p_from_dt AND c810_delete_fl  <> 'Y' 
	  AND v810.c1900_company_id=v_company_id_ctx;

	--
	RETURN p_opening_balance;
--
EXCEPTION
	WHEN OTHERS
	THEN
--
		RETURN 0;
--
END get_account_opening_balance;
/