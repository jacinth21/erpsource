/* Formatted on 2009/08/27 19:44 (Formatter Plus v4.8.0) */
--@"C:\DATABASE\Functions\Accounting\get_tag_count.fnc";

CREATE OR REPLACE FUNCTION get_tag_count (
	p_num		   t505_item_consignment.c205_part_number_id%TYPE
  , p_conid 	   t505_item_consignment.c504_consignment_id%TYPE
  , p_controlnum   t505_item_consignment.c505_control_number%TYPE
)
	RETURN NUMBER
IS
 
	v_count 	   NUMBER;
BEGIN
	BEGIN
		SELECT COUNT (1)
		  INTO v_count
		  FROM t5010_tag t5010 
		 WHERE t5010.c5010_last_updated_trans_id = p_conid
		   AND t5010.c205_part_number_id = p_num
		   AND t5010.c5010_control_number = p_controlnum
		   AND t5010.c5010_void_fl IS NULL;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN 0;
	END;

	RETURN v_count;
END get_tag_count;
/