--@"C:\Database\Functions\Accounting\get_dist_account_id.fnc"
CREATE OR REPLACE
    FUNCTION get_dist_account_id (
            p_dist_id IN t701_distributor.c701_distributor_id%TYPE)
        RETURN VARCHAR2
    IS
        /* Description  : This function returns Account id based on the distributor id
        Parameters   : p_dist_id
        */
        v_acct_id t701_distributor.c701_distributor_id%TYPE;
    BEGIN
        BEGIN
             SELECT c704_account_id
               INTO v_acct_id
               FROM t704_account
              WHERE c703_sales_rep_id IN
                (
                     SELECT c703_sales_rep_id
                       FROM t703_sales_rep
                      WHERE c701_distributor_id = p_dist_id
                )
                AND (c901_ext_country_id IS NULL OR c901_ext_country_id IN (SELECT country_id FROM v901_country_codes)) -- To include ext country id for GH
		AND c704_void_fl IS NULL;
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            RETURN NULL;
        
        WHEN TOO_MANY_ROWS THEN
            RETURN 'TOO_MANY_ROWS';
        END;
	 
        RETURN v_acct_id;
END get_dist_account_id;
/