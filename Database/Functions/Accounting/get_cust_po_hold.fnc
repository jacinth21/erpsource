/* Formatted on 2011/12/12 12:43 (Formatter Plus v4.8.0) */
--@"C:\Database\Functions\Accounting\get_cust_po_hold.fnc"
CREATE OR REPLACE
    FUNCTION get_cust_po_hold (
            p_acc_id t704_account.c704_account_id%TYPE,
            P_cust_po t501_order.c501_customer_po%TYPE)
        RETURN NUMBER
    IS
        v_cnt NUMBER;
        /* This function is used to to return the hold PO count
        * based on account ID and Customer PO
        */
    BEGIN
        BEGIN
             SELECT COUNT (1)
               INTO v_cnt
               FROM t501_order
              WHERE c704_account_id  = p_acc_id
                AND c501_customer_po = P_cust_po
                AND c503_invoice_id IS NULL
                AND c501_void_fl    IS NULL
                AND c501_delete_fl  IS NULL
                AND c501_hold_fl    IS NOT NULL;
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_cnt := 0;
        END;
        IF v_cnt  <> 0 THEN
            v_cnt := 1;
        END IF;
        RETURN v_cnt;
    END get_cust_po_hold;
    /