--@"C:\Database\Functions\Accounting\get_companyid_from_pnum.fnc"
CREATE OR REPLACE
    FUNCTION get_companyid_from_pnum (
      p_pnum IN t810_posting_txn.c205_part_number_id%TYPE)
        RETURN NUMBER
    IS
        --

        v_company_id t901_code_lookup.c901_code_id%TYPE;

        --
    BEGIN
		
	    -- PMT-39075 : To show division based company detail in UDI Review Dashboard
	    
	SELECT  NVL(GET_RULE_VALUE(T202.C1910_DIVISION_ID,'DIV_COMP_MAPPING'), 100800) into v_company_id
	FROM T202_PROJECT T202, T205_PART_NUMBER T205
	WHERE T202.C202_PROJECT_ID = T205.C202_PROJECT_ID
	AND T205.C205_PART_NUMBER_ID = p_pnum; 

        RETURN v_company_id;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        --
         
        RETURN '100800';
       
        --
    END get_companyid_from_pnum;
    /
