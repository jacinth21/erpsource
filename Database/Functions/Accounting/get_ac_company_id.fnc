/* Formatted on 2011/12/12 12:43 (Formatter Plus v4.8.0) */
--@"C:\Database\Functions\Accounting\get_ac_company_id.fnc"

CREATE OR REPLACE FUNCTION get_ac_company_id (
	p_acc_id   t704_account.c704_account_id%TYPE
)
	RETURN VARCHAR2
IS
/*	Description 	: THIS FUNCTION RETURNS COMPANY ID
 Parameters 		: p_code_id
*/
	v_comp_id	   t704_account.c901_company_id%TYPE;
BEGIN
	BEGIN
		SELECT c901_company_id
		  INTO v_comp_id
		  FROM t704_account
		 WHERE c704_account_id = p_acc_id;
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			RETURN NULL;
	END;

	RETURN v_comp_id;
END get_ac_company_id;
/