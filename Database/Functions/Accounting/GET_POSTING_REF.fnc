CREATE OR REPLACE FUNCTION GET_POSTING_REF
(
 p_tnx_type t804_posting_ref.c901_transaction_type%TYPE
)
RETURN TYPES.CURSOR_TYPE
IS
/*  Description     : THIS function will retunr the list of txn that need to posted for an selected txn
 *  Parameters   : p_code_id
 */
p_recordset TYPES.CURSOR_TYPE;
BEGIN
 OPEN p_recordset FOR
  SELECT  c901_account_type_cr
    ,c901_account_type_dr
  FROM t804_posting_ref
  WHERE c901_transaction_type = p_tnx_type;
    --
 RETURN p_recordset ;
 --
EXCEPTION
WHEN NO_DATA_FOUND
THEN
      RETURN NULL;
END GET_POSTING_REF;
/

