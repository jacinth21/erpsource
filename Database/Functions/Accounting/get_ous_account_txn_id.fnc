-- @"C:\Database\Functions\Accounting\get_ous_account_txn_id.fnc"
CREATE OR REPLACE
    FUNCTION get_ous_account_txn_id (
            p_txn_type t901_code_lookup.c901_code_id%TYPE)
        RETURN VARCHAR2
    IS
        /* Description  : THIS FUNCTION RETURNS NEXT ROLLFORWARD ID
        Parameters   : TRANS TYPE
        */
        v_txn_id       VARCHAR2 (20) ;
        v_trans_seq    VARCHAR2 (1000) ;
        v_trans_prefix VARCHAR2 (1000) ;
        --
    BEGIN
        IF p_txn_type = 102902 THEN
             SELECT S8900_OUS_OSSA_ACCT_TRANS.NEXTVAL INTO v_trans_seq FROM DUAL;
        ELSIF p_txn_type = 102903 THEN
             SELECT S8900_OUS_OSRT_ACCT_TRANS.NEXTVAL INTO v_trans_seq FROM DUAL;
        ELSIF p_txn_type = 102904 THEN
             SELECT S8900_OUS_OSSC_ACCT_TRANS.NEXTVAL INTO v_trans_seq FROM DUAL;
        ELSIF p_txn_type = 102905 THEN
             SELECT S8900_OUS_OSIA_ACCT_TRANS.NEXTVAL INTO v_trans_seq FROM DUAL;
        ELSIF p_txn_type = 102906 THEN
             SELECT S8900_OUS_OSIA_ACCT_TRANS.NEXTVAL INTO v_trans_seq FROM DUAL;
        END IF;
        IF v_trans_seq IS NOT NULL THEN
             SELECT get_code_name_alt (p_txn_type)
               INTO v_trans_prefix
               FROM DUAL;
            -- to append the sequence number
             SELECT v_trans_prefix ||v_trans_seq
               INTO v_txn_id
               FROM DUAL;
        END IF;
        -- assign the values.
        RETURN v_txn_id;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN NULL;
    END get_ous_account_txn_id;
    / 