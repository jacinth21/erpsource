CREATE OR REPLACE FUNCTION qb_get_salesrepInfo
(
 p_c704_account_id  t704_account.c704_account_id%TYPE
)
RETURN NUMBER
IS
--
v_c703_sales_rep_id  t704_account.c703_sales_rep_id%TYPE;
--
BEGIN
 --
 SELECT  c703_sales_rep_id
 INTO v_c703_sales_rep_id
 FROM t704_account
 WHERE c704_account_id = p_c704_account_id;
 --
 RETURN v_c703_sales_rep_id;
EXCEPTION
--
 WHEN NO_DATA_FOUND
 THEN
  RETURN NULL;
--
END qb_get_salesrepInfo;
/

