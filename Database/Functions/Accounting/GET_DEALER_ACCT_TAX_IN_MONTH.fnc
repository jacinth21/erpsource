CREATE OR REPLACE
  FUNCTION GET_DEALER_ACCT_TAX_IN_MONTH(
      p_dealer_Id t704_account.c101_dealer_id%TYPE,
      p_account_Id t704_account.c704_account_id%TYPE,
      p_dt_issue_from t503b_invoice_attribute.c503b_date_of_issue_from%TYPE,
      p_dt_issue_to t503b_invoice_attribute.c503b_date_of_issue_to%TYPE,
      p_vat_rate t502_item_order.C502_VAT_RATE%TYPE,
      p_inv_id 		 t503_invoice.c503_invoice_id%TYPE)
    RETURN NUMBER
  IS
    /*  Description     : THIS FUNCTION RETURNS THE SALES TAX AMMOUNT FOR DEALER/ACCOUNT
    */
    v_total NUMBER;
    v_company_id t1900_company.c1900_company_id%TYPE;
  BEGIN
    BEGIN
		
      SELECT get_compid_frm_cntx() INTO v_company_id FROM dual;
     
	  IF p_dealer_Id IS NOT NULL THEN
				SELECT SUM(total_cost)
				INTO v_total
				FROM
				  (SELECT TRUNC(t502.C502_ITEM_QTY * NVL(t502.C502_ITEM_PRICE,0) * (NVL(p_vat_rate,0)/100),0) total_cost
				  FROM t501_order t501,
				  t502_item_order t502,t503_invoice t503
				  WHERE t501.c101_dealer_id             = p_dealer_Id
				  AND t501.c501_order_id                = t502.c501_order_id
				  AND t501.c101_dealer_id 				= t503.c101_dealer_id
				  AND t501.c503_invoice_id 				= t503.c503_invoice_id
				  AND t501.c501_delete_fl              IS NULL
				  AND t501.c501_void_fl                IS NULL
				  AND t503.c503_void_fl                IS NULL
				  AND t501.c501_hold_fl                IS NULL
				  AND t502.c502_void_fl                IS NULL
				  AND NVL (t501.c901_order_type, -9999) NOT IN
					(SELECT t906.c906_rule_value
					FROM t906_rules t906
					WHERE t906.c906_rule_grp_id = 'JPN_ORDTYPE'
					AND c906_rule_id            = 'JPN_ORD_EXC'
					)
				  AND t501.c501_surgery_date <= p_dt_issue_to
				  AND t501.c503_invoice_id  = p_inv_id
				  AND TRUNC(t503.c503_invoice_date) BETWEEN TRUNC(p_dt_issue_from) AND TRUNC(p_dt_issue_to)--fetching based on the invoice date as from surgery date is removed
				  AND t501.c1900_company_id = v_company_id
				  
				  UNION ALL
				  
				  --Including price adjustment orders price in invoice attributes value
				  SELECT TRUNC(t502.C502_ITEM_QTY * NVL(t502.C502_ITEM_PRICE,0) * (NVL(p_vat_rate,0)/100),0) total_cost
				  FROM t501_order t501,
					t502_item_order t502
				  WHERE t501.c101_dealer_id = p_dealer_Id
				  AND t501.c501_order_id    = t502.c501_order_id
				  AND t501.c901_order_type  = '2524' -- 2524 price adjustment orders
				  AND TRUNC(t501.c501_order_date) BETWEEN TRUNC(p_dt_issue_from) AND TRUNC(p_dt_issue_to)
				  AND t501.c1900_company_id = v_company_id
				  AND t501.c503_invoice_id  = p_inv_id
				  AND t501.c501_delete_fl  IS NULL
				  AND t501.c501_void_fl    IS NULL
				  AND t501.c501_hold_fl    IS NULL
				  AND t502.c502_void_fl    IS NULL
				  ) ;
      ELSE
				SELECT SUM(total_cost)
				INTO v_total
				FROM
				  (SELECT TRUNC(t502.C502_ITEM_QTY * NVL(t502.C502_ITEM_PRICE,0) * (NVL(p_vat_rate,0)/100),0) total_cost
				  FROM t501_order t501,
				  t704_account t704,
				  t502_item_order t502,t503_invoice t503
				  WHERE t501.c704_account_id            = t704.c704_account_id
				  AND t501.c704_account_id 				= t503.c704_account_id
				  AND t501.c503_invoice_id				= t503.c503_invoice_id
				  AND t501.c704_account_id              = p_account_Id
				  AND t501.c501_order_id                = t502.c501_order_id
				  AND t501.c501_delete_fl              IS NULL
				  AND t501.c501_void_fl                IS NULL
				  AND t501.c501_hold_fl                IS NULL
				  AND t502.c502_void_fl                IS NULL
				  AND t503.c503_void_fl                IS NULL
				  AND NVL (t501.c901_order_type, -9999) NOT IN
					(SELECT t906.c906_rule_value
					FROM t906_rules t906
					WHERE t906.c906_rule_grp_id = 'JPN_ORDTYPE'
					AND c906_rule_id            = 'JPN_ORD_EXC'
					)
				  AND t501.c501_surgery_date <= p_dt_issue_to
				  AND TRUNC(t503.c503_invoice_date) BETWEEN TRUNC(p_dt_issue_from) AND TRUNC(p_dt_issue_to)--fetching based on the invoice date as from surgery date is removed
				  AND t501.c1900_company_id = v_company_id
				  AND t501.c503_invoice_id  = p_inv_id
				  UNION ALL
				  
				  --Including price adjustment orders price in invoice attributes value
				  SELECT TRUNC(t502.C502_ITEM_QTY * NVL(t502.C502_ITEM_PRICE,0) * (NVL(p_vat_rate,0)/100),0) total_cost
				  FROM t501_order t501,
				  t502_item_order t502
				  WHERE t501.c704_account_id = p_account_Id
				  AND t501.c501_order_id    = t502.c501_order_id
				  AND t501.c901_order_type   = '2524' -- 2524 price adjustment orders
				  AND TRUNC(t501.c501_order_date) BETWEEN TRUNC(p_dt_issue_from) AND TRUNC(p_dt_issue_to)
				  AND t501.c1900_company_id = v_company_id
				  AND t501.c503_invoice_id  = p_inv_id
				  AND t502.c502_void_fl    IS NULL
				  AND t501.c501_delete_fl  IS NULL
				  AND t501.c501_void_fl    IS NULL
				  AND t501.c501_hold_fl    IS NULL
				  );
      END IF;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN 0;
    END;
    RETURN v_total;
  END GET_DEALER_ACCT_TAX_IN_MONTH;
  /
