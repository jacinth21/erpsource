--@"C:\Database\Functions\Accounting\get_tagid_from_txn_id.fnc"

CREATE OR REPLACE FUNCTION get_tagid_from_txn_id (
	p_trans_id   IN   t5010_tag.c5010_last_updated_trans_id%TYPE
)
	RETURN VARCHAR2
IS
/*	Description 	: This function returns unique tag ID from transaction ID
 Parameters 		: Transaction ID
*/
	v_tag_id   t5010_tag.c5010_tag_id%TYPE;
BEGIN
	BEGIN
		SELECT tagid
			INTO  v_tag_id
		FROM(
			  SELECT c5010_tag_id tagid
			   FROM t5010_tag 
			  WHERE c5010_last_updated_trans_id   = p_trans_id
			   AND c5010_void_fl    IS NULL
			   ORDER BY c5010_tag_id DESC
		   )
		 WHERE ROWNUM =1;
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			v_tag_id := NULL;
	END;

	RETURN v_tag_id;
END get_tagid_from_txn_id;
/