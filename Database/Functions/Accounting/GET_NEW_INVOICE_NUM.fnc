CREATE OR REPLACE FUNCTION GET_NEW_INVOICE_NUM
(
 p_AccId T503_INVOICE.C704_ACCOUNT_ID%TYPE,
 p_CustPO T503_INVOICE.C503_CUSTOMER_PO%TYPE
)
RETURN VARCHAR2
IS
/*  Description     : THIS FUNCTION RETURNS CODE NAME GIVEN THE CODE_ID
 Parameters 		: p_code_id
*/
v_inv_id NUMBER;
v_string VARCHAR2(20);
v_id_string VARCHAR2(10);
v_rowcnt NUMBER;
BEGIN
     BEGIN
	 	  SELECT count(*) 
		  INTO v_rowcnt
		  FROM(
			 SELECT C503_INVOICE_ID 
			 FROM T501_ORDER
			 WHERE C501_CUSTOMER_PO = p_AccId
			     AND C704_ACCOUNT_ID = p_CustPO
				 AND C503_INVOICE_ID IS NOT NULL 
				 AND C501_VOID_FL IS NOT NULL 
				 AND (c901_ext_country_id IS NULL OR c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH
				 AND C501_DELETE_FL IS NOT NULL );
		 -- 
		  IF v_rowcnt = 0
		  	 THEN
				  SELECT S503_INVOICE.NEXTVAL INTO v_inv_id FROM dual;
				  IF LENGTH(v_inv_id) = 1
				  	 THEN
					 v_id_string := '0' || v_inv_id;
					 ELSE
					 v_id_string := v_inv_id;
				  END IF;
				  SELECT 'GM' || v_id_string INTO v_string FROM dual;
			ELSE
				  SELECT C503_INVOICE_ID 
				  INTO v_string 
				  FROM T503_INVOICE
				  WHERE C704_ACCOUNT_ID = p_AccId 
				  AND   C503_CUSTOMER_PO = p_CustPO
				  AND   (c901_ext_country_id IS NULL OR c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH
				  AND   C503_VOID_FL IS NULL;
		  END IF;
   EXCEPTION
      WHEN NO_DATA_FOUND
   THEN
      RETURN ' ';
      END;
     RETURN v_string;
END GET_NEW_INVOICE_NUM;
/

