/* Formatted on 2012/03/20 18:00 (Formatter Plus v4.8.0) */
CREATE OR REPLACE FUNCTION get_currency_conversion (
	p_from_currency   t907_currency_conv.c907_curr_cd_from%TYPE
  , p_to_currency	  t907_currency_conv.c907_curr_cd_to%TYPE
  , p_as_of_dt		  DATE
  , p_amount		  NUMBER
  , p_convtp		  t907_currency_conv.c901_conversion_type%TYPE DEFAULT 101104
)
	RETURN VARCHAR2
IS
/*	  Description			  :   THIS FUNCTION CONVERTS THE AMOUNT FROM CURRENCY TO CURRENCY
	  Parameters			  :   p_from_currency,
								  p_to_currency,
								  p_as_of_dt,
								  p_amount
*/
	v_average_rate NUMBER;
	v_company_id t1900_company.c1900_company_id%TYPE;
BEGIN

	-- The currency conversion value for sales is setup in the GMNA only, so getting the default company to get the conversion value
	SELECT GET_RULE_VALUE('DEFAULT_COMPANY','ONEPORTAL')
	      INTO v_company_id
	      FROM DUAL;

	IF p_from_currency = p_to_currency THEN
		RETURN p_amount;
	ELSE
		BEGIN
			SELECT NVL (TO_NUMBER (c907_curr_value), 0)
			  INTO v_average_rate
			  FROM t907_currency_conv
			 WHERE c907_curr_cd_from = p_from_currency
			   AND c907_curr_cd_to = p_to_currency
			   AND TO_DATE (TO_CHAR (c907_from_date, 'mm/dd/yyyy'), 'mm/dd/yyyy') <= TRUNC(p_as_of_dt)
			   AND TO_DATE (TO_CHAR (c907_to_date, 'mm/dd/yyyy'), 'mm/dd/yyyy') >= TRUNC(p_as_of_dt)
			   AND c901_conversion_type = p_convtp
			   AND c901_source IN (106910,106911)
			   AND c907_void_fl  IS NULL ;
		EXCEPTION
			WHEN NO_DATA_FOUND THEN
			
			BEGIN
				
				SELECT NVL (TO_NUMBER (c907_curr_value), 0)
			  INTO v_average_rate
			  FROM t907_currency_conv
			 WHERE c907_curr_cd_from = p_from_currency
			   AND c907_curr_cd_to = p_to_currency
			   AND TO_DATE (TO_CHAR (c907_from_date, 'mm/dd/yyyy'), 'mm/dd/yyyy') <= TRUNC(p_as_of_dt)
			   AND TO_DATE (TO_CHAR (c907_to_date, 'mm/dd/yyyy'), 'mm/dd/yyyy') >= TRUNC(p_as_of_dt)
			   -- PC-2500: Currency conversion function changes (to check conversion type - parameter values)
			   AND c901_conversion_type = p_convtp
			   AND c901_source =106912  -- Others
			   AND c907_void_fl  IS NULL ;
				
			EXCEPTION
			WHEN NO_DATA_FOUND THEN 
			
				
					SELECT NVL (TO_NUMBER (c907_curr_value), 0)
					  INTO v_average_rate
					  FROM t907_currency_conv
					 WHERE c907_curr_cd_from = p_from_currency
					   AND c907_curr_cd_to = p_to_currency
					   AND c901_conversion_type = p_convtp
					   AND c907_void_fl  IS NULL
					   AND TO_DATE (TO_CHAR (c907_to_date, 'mm/dd/yyyy'), 'mm/dd/yyyy') =
							   (SELECT MAX (TO_DATE (TO_CHAR (c907_to_date, 'mm/dd/yyyy'), 'mm/dd/yyyy'))
								  FROM t907_currency_conv
								 WHERE c907_curr_cd_from = p_from_currency
								   AND c907_curr_cd_to = p_to_currency
								   AND TO_DATE (TO_CHAR (c907_to_date, 'mm/dd/yyyy'), 'mm/dd/yyyy') < p_as_of_dt
								   AND c901_conversion_type = p_convtp
								   AND C1900_Company_Id = v_company_id
								   AND c907_void_fl  IS NULL);
			WHEN OTHERS THEN
				RETURN 0;
			END;
		END;

		RETURN v_average_rate * p_amount;
	END IF;
END;
/
