--@"C:\Database\Functions\Accounting\get_warehouse_type.fnc"
CREATE OR REPLACE
    FUNCTION get_warehouse_type (
            p_physical_audit_id t2603_audit_location.c2600_physical_audit_id%TYPE,
            p_location_id t2603_audit_location.c901_location_id%TYPE)
        RETURN VARCHAR2
    IS
    /**********************************************************************
 *	Description     : Functions returns warehouse type based on PI Audit Id and Warehouse(p_location_id)
 *
 * 	Parameters 	:p_physical_audit_id	p_location_id
 ***********************************************************************/
        --
        v_type t2603_audit_location.c901_type%TYPE;
        --
    BEGIN
        --
         SELECT c901_type
           INTO v_type
           FROM t2603_audit_location
          WHERE c2600_physical_audit_id = p_physical_audit_id
            AND c901_location_id        = p_location_id;
        --
        RETURN v_type;
    EXCEPTION
    WHEN OTHERS THEN
        --
        RETURN '';
        --
    END get_warehouse_type;
    /