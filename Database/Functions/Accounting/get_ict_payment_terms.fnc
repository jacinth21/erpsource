/* Formatted on 2011/12/12 12:43 (Formatter Plus v4.8.0) */
--@"C:\Database\Functions\Accounting\get_ict_payment_terms.fnc"
 /*******************************************************
 * Purpose: The below function is added for PMT-834 here we are getting 
 *          payment term for distributor from distributoe setup screen
 *******************************************************/
CREATE OR REPLACE FUNCTION get_ict_payment_terms(
	p_distid   t701_distributor.c701_distributor_id%TYPE
)
	RETURN VARCHAR2
IS

	v_terms	   t704_account.c704_payment_terms%TYPE;
	
	BEGIN
		
	  SELECT   t906.c906_rule_value 
	  INTO v_terms 
      FROM t901_code_lookup t901, t906_rules t906, t701_distributor t701
      WHERE t901.c901_code_id        = TO_NUMBER (t906.c906_rule_id)
      AND t901.c901_code_grp       = 'ICPTY'
      AND t906.c906_rule_grp_id = to_char(t701.c101_party_intra_map_id)
      AND t901.c901_code_id = 50212
      AND t901.c901_active_fl      = 1
      AND t701.c701_distributor_id = p_distid 
      AND t901.c901_void_fl IS NULL
      AND t906.c906_void_fl IS NULL
      AND t701.c701_void_fl is NULL;
      if v_terms is NULL
      THEN
      	GM_RAISE_APPLICATION_ERROR('-20999','72',GET_DISTRIBUTOR_NAME(p_distid));
      	
      END IF;
		Return v_terms;
	EXCEPTION
		WHEN NO_DATA_FOUND 
		THEN
		GM_RAISE_APPLICATION_ERROR('-20999','73',GET_DISTRIBUTOR_NAME(p_distid));
		
			
END get_ict_payment_terms;
/