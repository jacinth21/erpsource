create or replace FUNCTION get_ac_cogs_value_by_company (
    p_partnum   IN   t820_costing.c205_part_number_id%TYPE
  , p_type      IN   t820_costing.c901_costing_type%TYPE
  , p_company_id	IN t1900_company.c1900_company_id%TYPE
)
    RETURN NUMBER
IS
/***********************************************************************************************
 *    Description   : This function will return Part Costing information for a part and Inventory Type
 *    Return Parameters  : Part Pricing information
 ************************************************************************************************/
--
    v_cost         t820_costing.c820_purchase_amt%TYPE;
	v_company_parent_id  t1900_company.c1900_company_id%TYPE;
BEGIN
	BEGIN
            SELECT t5040.c1900_parent_company_id
            INTO v_company_parent_id
            FROM t5041_plant_company_mapping t5041,
                t5040_plant_master t5040
            WHERE t5040.c5040_plant_id=t5041.c5040_plant_id
			AND t5041.c5041_primary_fl='Y'
            AND t5041.c5041_void_fl  IS NULL
            AND t5040.c5040_void_fl  IS NULL
            AND t5041.c1900_company_id=p_company_id;
		 
		 EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
			v_company_parent_id:= NULL;
		END;

			BEGIN
				--
				SELECT c820_purchase_amt
				  INTO v_cost
				  FROM t820_costing
				 WHERE c820_costing_id =
						   (SELECT MIN (c820_costing_id)
							  FROM t820_costing
							 WHERE c205_part_number_id = p_partnum AND c901_costing_type = p_type
							   AND c901_status IN (4800, 4801)
							   AND C1900_COMPANY_ID= v_company_parent_id);
							   
				--
				RETURN v_cost;
		--
				EXCEPTION
					WHEN NO_DATA_FOUND
					THEN
						RETURN NULL;
			END;
EXCEPTION
WHEN NO_DATA_FOUND THEN
    RETURN NULL;	
END get_ac_cogs_value_by_company;
/