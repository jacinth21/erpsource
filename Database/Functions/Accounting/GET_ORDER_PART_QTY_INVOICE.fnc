CREATE OR REPLACE FUNCTION get_order_part_qty_invoice (
	p_order_id	 t502_item_order.c501_order_id%TYPE
  , p_part_num	 t502_item_order.c205_part_number_id%TYPE
  , p_price 	 t502_item_order.c502_item_price%TYPE
  , p_type		 t502_item_order.c901_type%TYPE
  , p_capfl 	 t502_item_order.c502_construct_fl%TYPE
  , p_ctrl_num	 t502_item_order.c502_control_number%TYPE
)
	RETURN NUMBER
IS
/*	Description 	: This Function Returns Qty For The Selected Order And Part
 *	Parameters		: p_order_id	Order ID
					  p_part_num	Part Number
 */
--
	v_qty		   NUMBER;
	v_returnqty    NUMBER;
	v_actualqty    NUMBER;
	v_invoiceid    VARCHAR2 (20);
	--
	v_return_order_cnt NUMBER;
--
BEGIN
	--
	SELECT NVL (SUM (c502_item_qty), 0) act_qty
	  INTO v_actualqty
	  FROM t501_order t501, t502_item_order t502
	 WHERE t501.c501_order_id = p_order_id
	   AND t501.c501_order_id = t502.c501_order_id
	   AND t502.c205_part_number_id = p_part_num
	   AND t502.c502_item_price = p_price
	   AND t502.c901_type = p_type
	   AND NVL(t502.c502_control_number,'-999') = DECODE(p_ctrl_num,null,'-999',p_ctrl_num) --control number will be null, before controlling the transaction
	   AND DECODE (t502.c502_construct_fl, NULL, 'N', t502.c502_construct_fl) = p_capfl;

	--
	SELECT t501.c503_invoice_id
	  INTO v_invoiceid
	  FROM t501_order t501
	 WHERE t501.c501_order_id = p_order_id;
	 
	 -- To get the Returns order mapping count
	 -- PMT-27111 - To get the RA count based on orders and child orders id 
	 	 
	 SELECT COUNT (1)
	   INTO v_return_order_cnt
	   FROM t501a_order_attribute t501a
	  WHERE t501a.c901_attribute_type = 26240469
	     AND t501a.c501_order_id      IN
		    (
		         SELECT c501_order_id
		           FROM t501_order
		          WHERE (c501_order_id      = p_order_id
		            OR c501_parent_order_id = p_order_id)
		            AND c501_void_fl       IS NULL
		            AND c501_delete_fl     IS NULL
		    )
	    AND t501a.c501a_void_fl      IS NULL;

	--26240469 Sales Adjustment Order - Mapping  
	-- To get the return QTY for the selected part
	-- and for the selected order
	
  IF v_return_order_cnt = 0
  THEN
  
	SELECT NVL (SUM (c502_item_qty), 0) ret_qty
	  INTO v_returnqty
	  FROM t501_order t501, t502_item_order t502
	 WHERE t501.c501_parent_order_id = p_order_id
	   AND t501.c501_parent_order_id <>
			   t501.c501_order_id	-- added this condition so that backorder which have the same order id as the parent order id doesnt get pulled up
	   AND t501.c501_order_id = t502.c501_order_id
	   AND t502.c205_part_number_id = p_part_num
	   AND t501.c503_invoice_id = v_invoiceid
	   AND t502.c901_type = p_type
	   AND t502.c502_item_price = p_price
	   AND NVL(t502.c502_control_number,'-999') = DECODE(p_ctrl_num,null,'-999',p_ctrl_num)
	   AND NVL (c901_order_type, 0) = 2529
	   AND t502.c502_void_fl IS NULL;

	   ELSE
	   
	   SELECT NVL (SUM (c502_item_qty), 0) ret_qty
	  INTO v_returnqty
	  FROM t501_order t501, t502_item_order t502
	 WHERE EXISTS (SELECT c501a_attribute_value 
					       FROM t501a_order_attribute t501a
					      WHERE t501a.c901_attribute_type = 26240469
					      	AND t501a.c501_order_id = p_order_id
					        AND t501a.c501a_attribute_value  = t501.c501_order_id
					        AND t501a.c501a_void_fl      IS NULL)
	   AND t501.c501_parent_order_id <>
			   t501.c501_order_id	-- added this condition so that backorder which have the same order id as the parent order id doesnt get pulled up
	   AND t501.c501_order_id = t502.c501_order_id
	   AND t502.c205_part_number_id = p_part_num
	   AND t501.c503_invoice_id = v_invoiceid
	   AND t502.c901_type = p_type
	   AND t502.c502_item_price = p_price
	   AND NVL(t502.c502_control_number,'-999') = DECODE(p_ctrl_num,null,'-999',p_ctrl_num)
	   AND NVL (c901_order_type, 0) = 2529
	   AND t502.c502_void_fl IS NULL;

	   END IF;
	--
	v_qty		:= v_returnqty + v_actualqty;
	RETURN v_qty;
EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		RETURN 0;
END get_order_part_qty_invoice;
/
