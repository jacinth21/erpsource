
--@"C:\Database\Functions\Accounting\get_tag_type.fnc";

CREATE OR REPLACE FUNCTION get_tag_type (
	p_tagid   IN   t5010_tag.c5010_tag_id%TYPE
)
	RETURN NUMBER
IS
/*	Description 	: This function returns tag type (loaner, consignment)
 Parameters 		: Tag id
*/
	v_tag_type   NUMBER;
BEGIN
	BEGIN
		   SELECT NVL(t504.c504_type,0) INTO v_tag_type
            FROM t5010_tag t5010, t504_consignment t504
              WHERE t5010.c5010_last_updated_trans_id = t504.c504_consignment_id
              	AND t5010.c5010_tag_id = p_tagid
              	AND t504.c504_void_fl IS NULL;
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			v_tag_type := 0;
	END;

	RETURN v_tag_type;
END get_tag_type;
/