/* Formatted on 2011/12/12 12:43 (Formatter Plus v4.8.0) */
--@"C:\Database\Functions\Accounting\get_dealer_pay_term.fnc"
CREATE OR REPLACE
    FUNCTION get_dealer_pay_term (
            p_delear_id t101_party.c101_party_id%TYPE)
        RETURN VARCHAR2
    IS
        /* Description  : This function returns payment term for a particular Dealer
        Parameters  : p_delear_id
        */
        v_pay_term t101_party_invoice_details.c901_payment_terms%TYPE;
    BEGIN
        BEGIN
             SELECT c901_payment_terms
               INTO v_pay_term
			   FROM t101_party_invoice_details
			  WHERE c101_party_id = p_delear_id;
			  
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            RETURN NULL;
        END;
        RETURN v_pay_term;
    END get_dealer_pay_term;
    /