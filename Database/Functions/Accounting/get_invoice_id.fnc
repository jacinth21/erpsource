CREATE OR REPLACE FUNCTION get_invoice_id
( 
p_company_id	t1900_company.c1900_company_id%TYPE,
p_inv_source    t503_invoice.c901_invoice_source%TYPE
)
RETURN VARCHAR2
IS
/* Description : This function returns invoice id based on company id
 * 				 if p_inv_source is 50256 or 50253 and if GMNA then we need distributor seq id. else GMNA sequence #
 *               Sequence prefix is based on the rule value.
 * 				 Prefix is ISD code for each country.Refer rule group INVOICE_PREFIX_MAP for ISD code prefic to invoice #
 *               Each country will have a different Invoice sequence no.
 Parameters    : p_company_id
 Parameters    : p_inv_source
*/
v_invoice_seq_id   VARCHAR2 (20);
v_seq_prefix_id VARCHAR2(20);
v_company_id  VARCHAR2(20);
v_invoice_id t503_invoice.c503_invoice_id%TYPE;
BEGIN
	v_company_id :=p_company_id;
	--50256	Inter-Company Transfer ,50253 Inter-Company Sale ,1000 Globus Medical North America
	IF ((p_inv_source = 50256 OR p_inv_source = 50253) AND v_company_id =1000 )
	THEN
	  
	 	v_company_id :=v_company_id||'DIST';
	END IF;
	
	SELECT get_rule_value_by_company(v_company_id,'INVOICE_PREFIX_MAP',p_company_id) 
		INTO v_seq_prefix_id 
	FROM DUAL;
		
	
	EXECUTE IMMEDIATE
        'SELECT  s503_invoice' || '_' || v_company_id || '.NEXTVAL from DUAL'
      INTO v_invoice_seq_id;
 		
         IF v_invoice_seq_id IS NULL
         THEN
			GM_RAISE_APPLICATION_ERROR('-20999','182','');
		END IF;
		
		v_invoice_id :=v_seq_prefix_id||v_invoice_seq_id;
         
	RETURN v_invoice_id;	
EXCEPTION
 WHEN NO_DATA_FOUND
	 THEN
		 raise_application_error('-20500','Invoice number cannot be generated.');
END get_invoice_id;
/

