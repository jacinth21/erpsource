/* Formatted on 2007/09/28 14:30 (Formatter Plus v4.8.0) */
-- @"C:\Database\Functions\Accounting\get_invoice_year.fnc";
CREATE OR REPLACE FUNCTION get_invoice_year (
	p_invoice_id  t503_invoice.c503_invoice_id%TYPE
)
	RETURN NUMBER
IS
/*	Description 	: THIS FUNCTION RETURNS INVOICE YEAR GIVEN THE INVOICE ID
 Parameters 		: p_invoice_id
*/
	v_inv_year	   VARCHAR2(20);
BEGIN
	BEGIN
		 SELECT TO_CHAR (c503_invoice_date, 'YYYY')
		 	INTO v_inv_year
   			FROM t503_invoice
  		WHERE c503_invoice_id = p_invoice_id
    		AND c503_void_fl   IS NULL;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			v_inv_year := NULL;
	END;

	RETURN v_inv_year;
END get_invoice_year;
/