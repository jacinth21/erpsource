/* Formatted on 2010/01/08 14:39 (Formatter Plus v4.8.0) */
--@"C:\Database\Functions\Accounting\GET_CREDIT_ASST_BILL_ADD.fnc"
CREATE OR REPLACE FUNCTION get_credit_asst_bill_add (
	p_ra_id   t506_returns.c506_rma_id%TYPE
)
	RETURN VARCHAR2
IS
/*	Description 	: THIS FUNCTION RETURNS CODE NAME GIVEN THE CODE_ID and it will not check for the void flag. Made for the Associated Return Report - "P" icon
 Parameters 		: p_code_id
*/
	v_type		   VARCHAR2 (20);
	v_add		   VARCHAR2 (1000);
BEGIN
	BEGIN
		SELECT c701_distributor_id
		  INTO v_type
		  FROM t506_returns
		 WHERE c506_rma_id = p_ra_id;

		IF v_type IS NOT NULL
		THEN
			SELECT	  a.c701_distributor_name
				   || '<BR>&nbsp;'
				   || a.c701_bill_add1
				   || '<BR>&nbsp;'
				   || a.c701_bill_add2
				   || '<BR>&nbsp;'
				   || DECODE(NVL(GET_RULE_VALUE('SWAP_ZIP_CODE','SWAP_ZIP_CODE'),'N'),'Y',a.c701_bill_zip_code,a.c701_bill_city)
				   || ',&nbsp;'
				   || get_code_name_alt (a.c701_bill_state)
				   || '&nbsp;'
				   || DECODE(NVL(GET_RULE_VALUE('SWAP_ZIP_CODE','SWAP_ZIP_CODE'),'N'),'Y',a.c701_bill_city,a.c701_bill_zip_code)
			  INTO v_add
			  FROM t701_distributor a, t506_returns b
			 WHERE b.c701_distributor_id = v_type
			   AND a.c701_distributor_id = b.c701_distributor_id
			   AND (a.c901_ext_country_id IS NULL OR a.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes)) -- To include ext country id for GH
			   AND b.c506_rma_id = p_ra_id;
		--AND B.C506_VOID_FL IS NULL;
		ELSE
			SELECT c704_account_id
			  INTO v_type
			  FROM t506_returns
			 WHERE c506_rma_id = p_ra_id;

			SELECT	  a.c704_bill_name
				   || '<BR>&nbsp;'
				   || a.c704_bill_add1
				   || '<BR>&nbsp;'
				   || a.c704_bill_add2
				   || '<BR>&nbsp;'
				   || DECODE(NVL(GET_RULE_VALUE('SWAP_ZIP_CODE','SWAP_ZIP_CODE'),'N'),'Y',a.c704_bill_zip_code,a.c704_bill_city)
				   || ',&nbsp;'
				   || get_code_name (a.c704_bill_state)
				   || '&nbsp;'
				   || DECODE(NVL(GET_RULE_VALUE('SWAP_ZIP_CODE','SWAP_ZIP_CODE'),'N'),'Y',a.c704_bill_city,a.c704_bill_zip_code)
			  INTO v_add
			  FROM t704_account a, t506_returns b
			 WHERE b.c704_account_id = v_type AND a.c704_account_id = b.c704_account_id AND b.c506_rma_id = p_ra_id 
			 	AND (a.c901_ext_country_id IS NULL OR a.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes));-- To include ext country id for GH
		--AND B.C506_VOID_FL IS NULL
		END IF;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN ' ';
	END;

	RETURN v_add;
END get_credit_asst_bill_add;
/
