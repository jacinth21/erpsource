create or replace
FUNCTION              "GET_GRAND_TOTAL_ORDER_AMT" 
(
     p_ord_id     T501_ORDER.C501_ORDER_ID%TYPE,
	 p_curr_type  T901_CODE_LOOKUP.C901_CODE_ID%TYPE
)
RETURN NUMBER
IS
/*  Description     : THIS FUNCTION RETURNS GRAND TOTAL FOR ORDERS
 Parameters   : p_ord_id,p_curr_type
*/
v_total NUMBER(15,2);
BEGIN

	IF p_curr_type IS NULL
     THEN
     
	 SELECT C501_TOTAL_COST INTO v_total
	  FROM T501_ORDER
	  WHERE (C501_ORDER_ID = p_ord_id OR C501_PARENT_ORDER_ID = p_ord_id)
	   AND C501_VOID_FL IS NULL ;
     
     ELSIF p_curr_type = '105460' -- Base currency type
 	 THEN
 	 
 	 	SELECT NVL(SUM(T502.C502_CORP_ITEM_PRICE * T502.C502_ITEM_QTY),0) INTO v_total
	  	  FROM T501_ORDER T501, T502_ITEM_ORDER T502
	     WHERE (T501.C501_ORDER_ID = p_ord_id OR  T501.C501_PARENT_ORDER_ID = p_ord_id)
	       AND T501.C501_ORDER_ID =  T502.C501_ORDER_ID
		   AND T501.C501_VOID_FL IS NULL 
		   AND T502.C502_VOID_FL IS NULL;
 
 
	ELSIF p_curr_type = '105461' -- Original Currency type
 	 THEN
 	 
 	 	SELECT NVL(SUM(T502.C502_ITEM_PRICE * T502.C502_ITEM_QTY),0) INTO v_total
	  	  FROM T501_ORDER T501, T502_ITEM_ORDER T502
	     WHERE (T501.C501_ORDER_ID = p_ord_id OR  T501.C501_PARENT_ORDER_ID = p_ord_id)
	       AND T501.C501_ORDER_ID =  T502.C501_ORDER_ID
 		   AND T501.C501_VOID_FL IS NULL 
		   AND T502.C502_VOID_FL IS NULL;
 
 	 END IF;
RETURN v_total;
EXCEPTION
   WHEN OTHERS
   THEN
      RETURN 0;
END GET_GRAND_TOTAL_ORDER_AMT;
/