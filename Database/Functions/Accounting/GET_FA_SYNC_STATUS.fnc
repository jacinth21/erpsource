create or replace
FUNCTION GET_FA_SYNC_STATUS(
 p_audit_id      IN   t2650_field_sales_audit.c2650_field_sales_audit_id%TYPE,
 p_audit_user_id IN  t2654_audit_sync_log.c2654_audit_user_id% TYPE,
 p_fs_det_id IN  t2651_field_sales_details.c2651_field_sales_details_id% TYPE,
 p_status IN varchar2
 )
 RETURN VARCHAR2
 IS
 v_status_code  INTEGER;
 v_count_rec      INTEGER;

BEGIN

  v_count_rec:=-1;

  BEGIN
    select count(*)
      INTO v_count_rec
        from t2654_audit_sync_log t2654 ,t2651_field_sales_details t2651
      where  UPPER(t2654.c2654_audit_user_id) = UPPER(p_audit_user_id)
       and t2651.c2651_field_sales_details_id = t2654.c2651_field_sales_details_id
       and t2654.C901_AUDIT_STATUS in (p_status)
       and t2651.c2650_field_sales_audit_id =  p_audit_id   ;

  EXCEPTION
    WHEN NO_DATA_FOUND
      THEN
        null;
  END;

  IF v_count_rec > 0 THEN
    IF p_status=18524 THEN
      v_status_code:= 18524; --Downloaded
    ELSE
      SELECT c901_reconciliation_status 
        INTO v_status_code
          FROM t2651_field_sales_details 
         WHERE c2651_field_sales_details_id = p_fs_det_id;
    END IF;
  ELSE
    IF p_status=18524 THEN
      v_status_code:= 18523; --LOCKED
    ELSE
      SELECT c901_reconciliation_status 
        INTO v_status_code
          FROM t2651_field_sales_details 
         WHERE c2651_field_sales_details_id = p_fs_det_id;
        
    END IF;

 END IF;

RETURN  v_status_code;

END GET_FA_SYNC_STATUS;
/