/* Formatted on 2011/12/12 12:43 (Formatter Plus v4.8.0) */
--@"C:\Database\Functions\Accounting\get_dealer_company_id.fnc"

CREATE OR REPLACE FUNCTION get_dealer_company_id (
	p_dealer_id   t101_party.c101_party_id%TYPE
)
	RETURN VARCHAR2
IS
/*	Description 	: THIS FUNCTION RETURNS COMPANY ID
 Parameters 		: p_code_id
*/
	v_comp_id	   t704_account.c901_company_id%TYPE;
BEGIN
	BEGIN
	
		SELECT distinct compid
		  INTO v_comp_id 
		  FROM v700_territory_mapping_detail
	     WHERE dealer_id = p_dealer_id;
	
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			RETURN NULL;
	END;

	RETURN v_comp_id;
END get_dealer_company_id;
/