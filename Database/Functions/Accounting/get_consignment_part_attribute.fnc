/* Formatted on 2009/09/08 08:31 (Formatter Plus v4.8.0) */

--@"C:\DATABASE\Functions\Accounting\get_consignment_part_attribute.fnc";

CREATE OR REPLACE FUNCTION get_consignment_part_attribute (
	p_con	t505_item_consignment.c504_consignment_id%TYPE
  , p_att	t205d_part_attribute.c901_attribute_type%TYPE
)
	RETURN VARCHAR2
IS
	v_cnt		   NUMBER;
BEGIN
	--Added void flag condition for MNTTASK-6165
	SELECT COUNT (t205d.c205d_attribute_value)
	  INTO v_cnt
	  FROM t205d_part_attribute t205d, t505_item_consignment t505
	 WHERE t205d.c205_part_number_id = t505.c205_part_number_id
	   AND t205d.c901_attribute_type = p_att
	   AND t505.c504_consignment_id = p_con
	   AND t205d.c205d_void_fl is NULL;

	IF v_cnt > 0
	THEN
		RETURN 'Y';
	ELSE
		RETURN NULL;
	END IF;
END get_consignment_part_attribute;
/
