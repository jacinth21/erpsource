CREATE OR REPLACE FUNCTION GET_POSTING_TABLE_REF
(
 p_account_type t805_posting_table_ref.c901_account_type%TYPE
)
RETURN VARCHAR2
IS
/***********************************************************************************************
 *  Description   : This function will return Table Idenfitier for
 *        the selected hospital
 *  Return Parameters  : Table Identifier name
 ************************************************************************************************/
p_table_identifier t805_posting_table_ref.c805_table_identifier%TYPE;
BEGIN
 SELECT c805_table_identifier
 INTO p_table_identifier
 FROM t805_posting_table_ref
 WHERE c901_account_type = p_account_type;
 --
 RETURN p_table_identifier ;
 --
EXCEPTION
WHEN NO_DATA_FOUND
THEN
--
      RETURN NULL;
--
END GET_POSTING_TABLE_REF;
/

