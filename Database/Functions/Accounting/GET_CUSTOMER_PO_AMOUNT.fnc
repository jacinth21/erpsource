CREATE OR REPLACE FUNCTION Get_Customer_Po_Amount
(
	 p_customer_po	t503_invoice.c503_customer_po%TYPE
	,p_account_id	t503_invoice.c704_account_id%TYPE	
)
RETURN NUMBER
IS
/*  Description     : This function returns the paid amount for invoice 
 *					  Based on customer PO number 
 * Parameters 		: p_customer_po
 *					  p_account_id
*/
v_total		NUMBER(15,2);
v_total_i	NUMBER(15,2);
v_total_ii	NUMBER(15,2);
BEGIN
--
	SELECT	SUM( a.c501_total_cost  +  NVL(a.c501_ship_cost,0) )
	INTO	v_total_i	
	FROM	t501_order a
	WHERE	a.c501_customer_pO	= p_customer_po
	AND		a.c704_account_id	= p_account_id
	AND		a.c501_parent_order_id IS NULL 
	AND		a.c501_delete_fl IS NULL 
	AND		a.c501_delete_fl IS NULL 
	AND		a.c501_void_fl	 IS NULL ;

	--
	SELECT	SUM( a.c501_total_cost  +  NVL(a.c501_ship_cost,0) )
	INTO	v_total_ii	
	FROM	t501_order a
	WHERE	a.c501_parent_order_id IN 
			(SELECT c501_order_id 
			 FROM	t501_order 
			 WHERE	c501_customer_po	= p_customer_po
			 AND	c704_account_id		= p_account_id)
	AND		a.c704_account_id	= p_account_id
	AND		a.c501_delete_fl IS NULL 
	AND		a.c501_void_fl	 IS NULL ;
	--
	RETURN (NVL(v_total_i,0) + NVL(v_total_ii,0));
--
EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
      RETURN 0;
END Get_Customer_Po_Amount;
/

