
--@"C:\Database\Functions\Accounting\get_retag_id.fnc"

CREATE OR REPLACE FUNCTION get_retag_id (
	p_audit_entry_id   IN   t2656_audit_entry.c2656_audit_entry_id%TYPE
)
	RETURN VARCHAR2
IS
/*	Description 	: This function returns Retag Id
 Parameters 		: Audit entry ID
*/
	v_retag_id   t2656_audit_entry.c5010_retag_id%TYPE;
BEGIN
	BEGIN
		  SELECT c5010_retag_id INTO  v_retag_id
           FROM t2656_audit_entry 
          WHERE c2656_audit_entry_id   = p_audit_entry_id
            AND c2656_retag_fl   IS NOT NULL 
            AND c5010_retag_id   IS NOT NULL
            AND c2656_void_fl    IS NULL;
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			v_retag_id := '';
	END;

	RETURN v_retag_id;
END get_retag_id;
/