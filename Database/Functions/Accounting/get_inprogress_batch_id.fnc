/* Formatted on 2011/12/12 12:43 (Formatter Plus v4.8.0) */
--@"C:\Database\Functions\Accounting\get_inprogress_batch_id.fnc"

CREATE OR REPLACE FUNCTION get_inprogress_batch_id (
	p_userid   t9600_batch.c9600_initiated_by%TYPE
)
	RETURN VARCHAR2
IS
/*	Description 	: THIS FUNCTION RETURNS BATCH ID
 Parameters 		: p_user_id
*/
	v_batch_id	   t9600_batch.c9600_batch_id%TYPE;
	v_inprogress_cnt NUMBER;
	v_company_id        t1900_company.c1900_company_id%TYPE;
BEGIN
	v_batch_id := NULL;
	
	 SELECT get_compid_frm_cntx() 
	   INTO v_company_id  
	   FROM dual;
	   
	
		SELECT COUNT (1)
           INTO v_inprogress_cnt
           FROM t9600_batch t9600
          WHERE t9600.c9600_initiated_by = p_userid
            AND t9600.c901_batch_type    = 18751 -- Invoice Batch
            AND t9600.c901_batch_status  = 18741 -- In progress
            AND t9600.c1900_company_id = v_company_id
            AND t9600.c9600_void_fl     IS NULL;
         -- if progress count is 1 then get the batch id otherwise return null.
         IF v_inprogress_cnt = 1
         THEN
         	SELECT c9600_batch_id
           INTO v_batch_id
           FROM t9600_batch t9600
          WHERE t9600.c9600_initiated_by = p_userid
            AND t9600.c901_batch_type    = 18751 -- Invoice Batch
            AND t9600.c901_batch_status  = 18741 -- In progress
            AND t9600.c1900_company_id = v_company_id
            AND t9600.c9600_void_fl     IS NULL;
         ELSIF v_inprogress_cnt > 1
         THEN
         	v_batch_id := -999;
         END IF;
	RETURN v_batch_id;
END get_inprogress_batch_id;
/