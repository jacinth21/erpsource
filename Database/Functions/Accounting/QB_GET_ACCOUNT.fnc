CREATE OR REPLACE FUNCTION qb_get_account
(
 p_qb_account_listid gm_qb_account_mapping.qb_account_listid%TYPE
)
RETURN VARCHAR2
IS
--
v_c704_account_id gm_qb_account_mapping.c704_account_id%TYPE;
--
BEGIN
 --
 SELECT c704_account_id
 INTO  v_c704_account_id
 FROM gm_qb_account_mapping
 WHERE qb_account_listid =  p_qb_account_listid;
 --
 RETURN v_c704_account_id ;
EXCEPTION
 WHEN NO_DATA_FOUND
    THEN
      RETURN ' ';
END qb_get_account;
/

