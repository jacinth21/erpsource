/* Formatted on 2009/02/16 15:25 (Formatter Plus v4.8.0) */
--@"C:\Database\Functions\Accounting\get_sage_vendor_id.fnc";

CREATE OR REPLACE FUNCTION get_sage_vendor_id (
	p_vend_id	IN	 t301_vendor.c301_vendor_id%TYPE
)
	RETURN VARCHAR2
IS
/*	Description 	: THIS FUNCTION RETURNS SAGE VENDOR ID GIVEN THE CODE_ID
 Parameters 		: p_code_id
*/
	v_sage_vend_id t301_vendor.c301_sage_vendor_id%TYPE;
BEGIN
	BEGIN
		SELECT c301_sage_vendor_id
		  INTO v_sage_vend_id
		  FROM t301_vendor
		 WHERE c301_vendor_id = p_vend_id;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN '';
	END;

	RETURN v_sage_vend_id;
END get_sage_vendor_id;
/
