CREATE OR REPLACE FUNCTION GET_CREDIT_MEMO_INVOICE_ID
(
	 p_parent_order_id 	t501_order.c501_parent_order_id%TYPE
	,p_type				CHAR
)
RETURN Date
IS
/**********************************************************************
 *	Description     : Functions returns Invoice ID or Invoice Date based on the
 * 					  parameter passed
 *
 * 	Parameters 		: p_parent_order_id
 * 					  p_type if I will pass Invoice ID
 *							    D will pass Invoice Date
 ***********************************************************************/
v_return Date;
BEGIN
--
	SELECT 	DECODE(p_type, 'I',c503_invoice_id, a.c503_invoice_date)
	INTO	v_return
	FROM 	t503_invoice a
	WHERE  (a.c503_customer_po  , a.c704_account_id)
			IN  (SELECT r.c501_customer_po, r.c704_account_id
        		 FROM   t501_order r
				 WHERE  r.c501_order_id = p_parent_order_id )
	AND		ROWNUM		= 1
	AND     C503_VOID_FL IS NULL;
   	--
	RETURN v_return;
	--
EXCEPTION
WHEN NO_DATA_FOUND
THEN
	RETURN NULL;
END GET_CREDIT_MEMO_INVOICE_ID;
/

