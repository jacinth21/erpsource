
--@"C:\Database\Functions\Accounting\get_field_sales_tag_qty.fnc"

CREATE OR REPLACE FUNCTION get_field_sales_tag_qty (
	p_dist_id   t701_distributor.c701_distributor_id%TYPE
)
	RETURN VARCHAR2
IS
/*	Description 	: THIS FUNCTION RETURNS tag quantity
 Parameters 		: distributor id
*/
	v_tag_id	   t5010_tag.C5010_TAG_ID%TYPE;
BEGIN
	BEGIN
		  SELECT COUNT (1) INTO v_tag_id
   			FROM t5010_tag t5010
  			WHERE t5010.c5010_location_id = p_dist_id
    		AND t5010.c5010_void_fl    IS NULL ;
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			RETURN 0;
	END;

	RETURN v_tag_id;
END get_field_sales_tag_qty;
/