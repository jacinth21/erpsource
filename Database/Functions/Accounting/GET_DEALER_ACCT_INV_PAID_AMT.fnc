CREATE OR REPLACE FUNCTION GET_DEALER_ACCT_INV_PAID_AMT
(
     p_dealer_Id     t704_account.c101_dealer_id%TYPE,
     p_account_Id    t704_account.c704_account_id%TYPE,
     p_dt_issue_from t503b_invoice_attribute.c503b_date_of_issue_from%TYPE,
     p_dt_issue_to   t503b_invoice_attribute.c503b_date_of_issue_to%TYPE  
)
RETURN NUMBER
IS
/*  Description     : THIS FUNCTION RETURNS THE PAID AMOUNT FOR DEALER/ACCOUNT
*/
v_total NUMBER;

BEGIN
     BEGIN
     
  
     IF p_dealer_Id IS NOT NULL
     THEN
     
		SELECT NVL(SUM(NVL(T508.C508_PAYMENT_AMT,0)),0) INTO v_total
		FROM T508_RECEIVABLES T508,
		  T503_INVOICE T503
		WHERE TRUNC(T508.C508_RECEIVED_DATE) BETWEEN TRUNC(p_dt_issue_from) AND TRUNC(p_dt_issue_to)
		AND T503.c503_invoice_id = T508.c503_invoice_id
		AND T503.c101_dealer_id  = p_dealer_Id
		AND T503.C503_VOID_FL   IS NULL;
		  
	 ELSE
	 
	 	SELECT NVL(SUM(NVL(T508.C508_PAYMENT_AMT,0)),0) INTO v_total
		FROM T508_RECEIVABLES T508,
		  T503_INVOICE T503,
		  T704_ACCOUNT T704
		WHERE TRUNC(T508.C508_RECEIVED_DATE) BETWEEN TRUNC(p_dt_issue_from) AND TRUNC(p_dt_issue_to)
		AND T508.C503_INVOICE_ID = T503.C503_INVOICE_ID
		AND T503.C704_ACCOUNT_ID = T704.C704_ACCOUNT_ID
		AND T503.C704_ACCOUNT_ID  = p_account_Id
		AND T503.C503_VOID_FL   IS NULL
		AND T704.C704_VOID_FL   IS NULL; 
	 
	 END IF;
	
   EXCEPTION
      WHEN NO_DATA_FOUND
   THEN
      RETURN 0;
      END;
     RETURN v_total;
END GET_DEALER_ACCT_INV_PAID_AMT;
/

