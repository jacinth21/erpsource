/* Formatted on 2007/07/24 17:42 (Formatter Plus v4.8.0) */


CREATE OR REPLACE FUNCTION get_order_return_part_qty (
	p_order_id		t502_item_order.c501_order_id%TYPE
  , p_part_num		t502_item_order.c205_part_number_id%TYPE
  , p_control_num	t502_item_order.c502_control_number%TYPE
)
	RETURN NUMBER
IS
/*	Description 	: This Function Returns Qty For The Selected Order And Part
 *	Parameters	 : p_order_id Order ID
	   p_part_num Part Number
	   p_control_num Control Number
 */
	v_qty		   NUMBER;
--
BEGIN
	--
	SELECT NVL (SUM (c507_item_qty), 0)
	  INTO v_qty
	  FROM t507_returns_item t507, t506_returns t506
	 WHERE t507.c506_rma_id = t506.c506_rma_id
	   AND t506.c501_order_id = p_order_id
	   AND t507.c205_part_number_id = p_part_num
	  -- AND t507.c507_org_control_number = p_control_num
	   AND t506.c506_void_fl IS NULL
	   AND t507.c507_status_fl <> 'Q';

	--
	RETURN v_qty;
EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		RETURN 0;
END get_order_return_part_qty;
/
