CREATE OR REPLACE
    FUNCTION get_invoice_part_tax (
            p_order_id t502_item_order.c501_order_id%TYPE,
            p_item_ord_id t502_item_order.c502_item_order_id%TYPE)
        RETURN NUMBER
    IS
        /* Description  : This Function Returns Qty For The Selected Order And Part based on adjustment code also
        * Parameters  : p_order_id Order ID
        p_item_ord_id item order id
        */
        --
        v_tax        NUMBER;
        v_return_tax NUMBER (14, 2) ;
        v_actual_tax NUMBER (14, 2) ;
        v_invoiceid t501_order.c503_invoice_id%TYPE;
        v_part_num t502_item_order.c205_part_number_id%TYPE;
        v_price t502_item_order.c502_item_price%TYPE;
        v_type t502_item_order.c901_type%TYPE;
        v_capfl t502_item_order.c502_construct_fl%TYPE;
        v_adj_code t502_item_order.C901_UNIT_PRICE_ADJ_CODE%TYPE;
        --
        v_return_order_cnt NUMBER;
    BEGIN
        BEGIN
	             SELECT c205_part_number_id, c502_item_price, c901_type
	                    , DECODE (C502_CONSTRUCT_FL, NULL, 'N', C502_CONSTRUCT_FL), C901_UNIT_PRICE_ADJ_CODE
	               INTO v_part_num, v_price, v_type
	                    , v_capfl, v_adj_code
	               FROM t502_item_order
	              WHERE c502_item_order_id  =  p_item_ord_id
	                AND c501_order_id       =  p_order_id
	                AND c502_void_fl        IS NULL;
	        EXCEPTION
	        	 WHEN NO_DATA_FOUND 
	        	   THEN
	           			v_part_num := NULL;
	                    v_price    := NULL;
	                    v_type     := NULL;
	                    v_capfl    := NULL;
	                    v_adj_code := NULL;
        END;
        --
			
		         SELECT NVL (SUM (t502.c502_tax_amt), ROUND (SUM ( (NVL (t502.c502_item_price, 0) 
		         		* NVL (t502.c502_item_qty, 0) * NVL (t502.c502_vat_rate / 100, 0))), 2)) 
		           INTO v_actual_tax
		           FROM t501_order t501, t502_item_order t502
		          WHERE t501.c501_order_id                                                 = p_order_id
		            AND t501.c501_order_id                                                 = t502.c501_order_id
		            AND t502.c205_part_number_id                                           = v_part_num
		            AND t502.c502_item_price                                               = v_price
		            AND t502.c901_type                                                     = v_type
		            AND NVL (C901_UNIT_PRICE_ADJ_CODE, '-9999')                            = NVL (v_adj_code, '-9999')
		            AND DECODE (t502.c502_construct_fl, NULL, 'N', t502.c502_construct_fl) = v_capfl;
        --
		         SELECT t501.c503_invoice_id
		           INTO v_invoiceid
		           FROM t501_order t501
		          WHERE t501.c501_order_id = p_order_id;
          
          -- To get the Returns order mapping count
          -- PMT-27111 - To get the RA count based on orders and child orders id 
          
				 SELECT COUNT (1)
			   	   INTO v_return_order_cnt
			   	   FROM t501a_order_attribute t501a
			      WHERE t501a.c901_attribute_type = 26240469
			        AND t501a.c501_order_id      IN
			            (
				         SELECT c501_order_id
				           FROM t501_order
				          WHERE (c501_order_id      =  p_order_id
				            OR c501_parent_order_id =  p_order_id)
				            AND c501_void_fl        IS NULL
				            AND c501_delete_fl      IS NULL
			    		)
			        AND t501a.c501a_void_fl      IS NULL;

        --26240469 Sales Adjustment Order - Mapping  
        -- To get the return QTY for the selected part
        -- and for the selected order
        IF v_return_order_cnt = 0
			THEN

		         SELECT NVL (SUM (t502.c502_tax_amt), NVL(ROUND (SUM ( (NVL (t502.c502_item_price, 0) 
		         		* NVL (t502.c502_item_qty, 0) * NVL (t502.c502_vat_rate / 100, 0))), 2), 0)) 
		           INTO v_return_tax
		           FROM t501_order t501, t502_item_order t502
		          WHERE t501.c501_parent_order_id = p_order_id
		            AND t501.c501_parent_order_id <> t501.c501_order_id -- added this condition so that backorder which have
		            -- the same order id as the parent order id doesnt get pulled up
		            AND t501.c501_order_id                      = t502.c501_order_id
		            AND t502.c205_part_number_id                = v_part_num
		            AND t501.c503_invoice_id                    = v_invoiceid
		            AND t502.c901_type                          = v_type
		            AND t502.c502_item_price                    = v_price
		            AND NVL (C901_UNIT_PRICE_ADJ_CODE, '-9999') = NVL (v_adj_code, '-9999')
		            AND NVL (c901_order_type, 0)                = 2529
		            AND t502.c502_void_fl IS NULL;
            
      ELSE
		      	 SELECT NVL (SUM (t502.c502_tax_amt), NVL(ROUND (SUM ( (NVL (t502.c502_item_price, 0) 
		      	 		* NVL (t502.c502_item_qty, 0) * NVL (t502.c502_vat_rate / 100, 0))), 2), 0)) 
		           INTO v_return_tax
		           FROM t501_order t501, t502_item_order t502
		          WHERE EXISTS ( SELECT c501a_attribute_value 
							       FROM t501a_order_attribute t501a
							      WHERE t501a.c901_attribute_type = 26240469
							      	AND t501a.c501_order_id = p_order_id
							        AND t501a.c501a_attribute_value  = t501.c501_order_id
							        AND t501a.c501a_void_fl      IS NULL )
		            AND t501.c501_parent_order_id <> t501.c501_order_id -- added this condition so that backorder which have
		            -- the same order id as the parent order id doesnt get pulled up
		            AND t501.c501_order_id                      = t502.c501_order_id
		            AND t502.c205_part_number_id                = v_part_num
		            AND t501.c503_invoice_id                    = v_invoiceid
		            AND t502.c901_type                          = v_type
		            AND t502.c502_item_price                    = v_price
		            AND NVL (C901_UNIT_PRICE_ADJ_CODE, '-9999') = NVL (v_adj_code, '-9999')
		            AND NVL (c901_order_type, 0)                = 2529
		            AND t502.c502_void_fl                      IS NULL;
            
       END IF;     
        --
        v_tax := v_actual_tax + v_return_tax ;
        RETURN v_tax;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN 0;
    END get_invoice_part_tax;
    /
