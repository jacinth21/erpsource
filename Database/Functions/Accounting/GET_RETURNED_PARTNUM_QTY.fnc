/* Formatted on 2007/11/29 23:59 (Formatter Plus v4.8.0) */
CREATE OR REPLACE FUNCTION get_returned_partnum_qty (
	p_ra_id   t506_returns.c506_rma_id%TYPE
)
	RETURN VARCHAR2
IS
/*	Description 	: THIS FUNCTION RETURNS CODE NAME GIVEN THE CODE_ID
 Parameters 		: p_code_id
*/
	v_qty		   t208_set_details.c208_set_qty%TYPE;
	v_fl		   VARCHAR2 (10);
BEGIN
	BEGIN
		SELECT SUM (c507_item_qty)
		  INTO v_qty
		  FROM t507_returns_item
		 WHERE c506_rma_id = p_ra_id AND c507_status_fl = 'C';
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN ' ';
	END;

	RETURN v_qty;
END get_returned_partnum_qty;
/
