/* Formatted on 2011/05/11 14:41 (Formatter Plus v4.8.0) */
--@"C:\Database\Functions\Accounting\get_account_dist_id.fnc"

CREATE OR REPLACE FUNCTION get_account_dist_id (
	p_acc_id   t704_account.c704_account_id%TYPE
)
	RETURN VARCHAR2
IS
/*	Description 	: THIS FUNCTION RETURNS distributor id based on the account id
 Parameters 		: p_acc_id
*/
	v_dist_id	   t701_distributor.c701_distributor_id%TYPE;
BEGIN
	BEGIN
		SELECT c701_distributor_id
		  INTO v_dist_id
		  FROM t701_distributor
		 WHERE c701_distributor_id = (SELECT c701_distributor_id
										FROM t703_sales_rep
									   WHERE c703_sales_rep_id = (SELECT c703_sales_rep_id
																	FROM t704_account
																   WHERE c704_account_id = p_acc_id))
										  AND (c901_ext_country_id IS NULL OR c901_ext_country_id IN (SELECT country_id FROM v901_country_codes));-- To include ext country id for GH
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN NULL;
	END;

	RETURN v_dist_id;
END get_account_dist_id;
/