/* Formatted on 2011/03/01 12:54 (Formatter Plus v4.8.0) */
CREATE OR REPLACE FUNCTION get_total_email_do_amt (
	p_ord_id   t501_order.c501_order_id%TYPE
)
	RETURN VARCHAR2
IS
/*	Description 	: THIS FUNCTION RETURNS THE TOTAL DO AMOUNT i.e(It doesnot apply the Discount Order(DS) amount.)
 Parameters 		: p_ord_id
*/
	v_total 	   NUMBER (15, 2);
BEGIN
	BEGIN
		SELECT SUM (c501_total_cost)
		  INTO v_total
		  FROM t501_order
		 WHERE (c501_order_id = p_ord_id OR c501_parent_order_id = p_ord_id)
		   AND NVL (c901_order_type, -9999) <> 2535
		   AND NVL (c901_order_type, -9999) NOT IN (
                SELECT t906.c906_rule_value
                  FROM t906_rules t906
                 WHERE t906.c906_rule_grp_id = 'ORDTYPE'
                   AND c906_rule_id = 'EXCLUDE')
		   AND c501_void_fl IS NULL;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN ' ';
	END;

	RETURN v_total;
END get_total_email_do_amt;
/
