/* Formatted on 2009/08/27 18:33 (Formatter Plus v4.8.0) */
--@"C:\DATABASE\Functions\Accounting\get_partdesc_by_company.fnc";
CREATE OR REPLACE FUNCTION get_partdesc_by_company (
	p_part_num	 	t205d_part_attribute.c205_part_number_id%TYPE	
)
	RETURN VARCHAR2
IS	 
	v_part_desc	    t2000_master_metadata.c2000_ref_desc%TYPE;
	v_company_id 	t1900_company.C1900_COMPANY_ID%TYPE;
	v_language_id   t1900_company.c901_language_id%TYPE;
BEGIN	 
	
	SELECT NVL(get_compid_frm_cntx(), get_rule_value('DEFAULT_COMPANY','ONEPORTAL')) 
      INTO v_company_id
      FROM dual;
	
     SELECT c901_language_id 
       INTO v_language_id
       FROM t1900_company t1900 
      WHERE t1900. c1900_company_id = v_company_id
        AND t1900.c1900_void_fl IS NULL;
          
    BEGIN    	
	   	SELECT TRIM(REGEXP_REPLACE(t2000.c2000_ref_desc ,'<[^>]*>',' '))
	   	  INTO v_part_desc
          FROM t2000_master_metadata t2000 
         WHERE t2000.c2000_ref_id = p_part_num
           AND c901_language_id = v_language_id
           AND c901_ref_type = 103090 --Part Number
           AND t2000.c2000_void_fl IS NULL;
       	   
    EXCEPTION WHEN NO_DATA_FOUND THEN
    
        SELECT  C205_PART_NUM_DESC PDESC
          INTO  v_part_desc
	      FROM  T205_PART_NUMBER
         WHERE  C205_PART_NUMBER_ID = p_part_num;
    END;
	RETURN v_part_desc;
END get_partdesc_by_company;
/