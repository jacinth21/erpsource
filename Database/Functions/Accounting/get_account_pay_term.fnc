/* Formatted on 2011/12/12 12:43 (Formatter Plus v4.8.0) */
--@"C:\Database\Functions\Accounting\get_account_pay_term.fnc"
CREATE OR REPLACE
    FUNCTION get_account_pay_term (
            p_acc_id t704_account.c704_account_id%TYPE)
        RETURN VARCHAR2
    IS
        /* Description  : This function returns payment term for a particular account
        Parameters  : p_acc_id
        */
        v_pay_term t704_account.C704_PAYMENT_TERMS%TYPE;
    BEGIN
        BEGIN
             SELECT C704_PAYMENT_TERMS
               INTO v_pay_term
               FROM t704_account
              WHERE c704_account_id = p_acc_id;
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            RETURN NULL;
        END;
        RETURN v_pay_term;
    END get_account_pay_term;
    /