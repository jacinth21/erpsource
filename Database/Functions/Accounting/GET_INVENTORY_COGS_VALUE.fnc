/* Formatted on 2008/02/28 12:01 (Formatter Plus v4.8.8) */
CREATE OR REPLACE FUNCTION get_inventory_cogs_value (
    p_partnum   IN   t820_costing.c205_part_number_id%TYPE
)
    RETURN NUMBER
IS
/***********************************************************************************************
 *  Description   : This function will return Part Costing information
 *        for selected part
 *  Return Parameters  : Part Pricing information
 ************************************************************************************************/
--
    v_cost         t820_costing.c820_purchase_amt%TYPE;
    v_company_id  t1900_company.c1900_company_id%TYPE;
    v_plant_id_ctx t5040_plant_master.c5040_plant_id%TYPE;
    v_costing_plant_id t5040_plant_master.c5040_plant_id%TYPE;
--
BEGIN
    --
     SELECT get_plantid_frm_cntx()
      	INTO v_plant_id_ctx
      FROM dual;
    -- to get the paranet company id
    v_company_id := get_plant_parent_comp_id(v_plant_id_ctx);
    BEGIN
		SELECT DECODE(c823_plant_level, 'Y', v_plant_id_ctx, NULL)
	    INTO v_costing_plant_id
	   FROM t823_costing_type_ref
	  WHERE c901_costing_type = 4900
	    AND c823_void_fl     IS NULL;
    --
     EXCEPTION
        WHEN NO_DATA_FOUND   
        THEN
        	v_costing_plant_id:= NULL;
        END;
	--      
    BEGIN
        --
        SELECT c820_purchase_amt
          INTO v_cost
          FROM t820_costing
         WHERE c820_costing_id =
                    (SELECT MIN (c820_costing_id)
                       FROM t820_costing
                      WHERE c205_part_number_id = p_partnum AND c901_status IN (4800, 4801) AND c901_costing_type = 4900
                       AND C1900_COMPANY_ID= v_company_id
                       AND NVL(c5040_plant_id, -999) = NVL(v_costing_plant_id, - 999));

        --
        RETURN v_cost;
--
    EXCEPTION
        WHEN NO_DATA_FOUND   -- when it is scrap
        THEN
            SELECT c820_purchase_amt
              INTO v_cost
              FROM t820_costing
             WHERE c820_costing_id =
                       (SELECT MAX (c820_costing_id)
                          FROM t820_costing
                         WHERE c205_part_number_id = p_partnum
                           AND c901_status IN (4800, 4801)
                           AND c901_costing_type = 4900
                           AND C1900_COMPANY_ID= v_company_id
                           AND NVL(c5040_plant_id, -999) = NVL(v_costing_plant_id, - 999));

            RETURN v_cost;
    --
    END;
--
EXCEPTION
    WHEN NO_DATA_FOUND
    THEN
        RETURN NULL;
--
END get_inventory_cogs_value;
/