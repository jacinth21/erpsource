/* Formatted on 2011/04/28 10:11 (Formatter Plus v4.8.0) */
CREATE OR REPLACE FUNCTION get_acct_party_id (
	p_id	 VARCHAR2
  , p_type	 VARCHAR2
)
	RETURN VARCHAR2
IS
/*	Description 	: This function returns party id based on the party type
 Parameters   : p_code_id
*/
	v_id		   VARCHAR2 (20);
BEGIN
--
	IF p_type = 'S'
	THEN
		--
		SELECT c704_account_id
		  INTO v_id
		  FROM t501_order
		 WHERE c501_order_id = p_id;
	--
	ELSIF p_type = 'CSET' OR p_type = 'CITEM' OR p_type = 'CROLL'
	THEN
		--
		SELECT c701_distributor_id
		  INTO v_id
		  FROM t504_consignment
		 WHERE c504_consignment_id = p_id;

		--
		IF v_id IS NULL
		THEN
			SELECT c704_account_id
			  INTO v_id
			  FROM t504_consignment
			 WHERE c504_consignment_id = p_id;
		END IF;
	--
	ELSIF (p_type = 'R' OR p_type = 'CR')
	THEN
		--
		SELECT NVL (c701_distributor_id, c704_account_id)
		  INTO v_id
		  FROM t506_returns
		 WHERE c506_rma_id = p_id;
	--
	ELSIF (p_type = 'ICR')
	THEN
		--
		SELECT get_rule_value (get_distributor_inter_party_id (c701_distributor_id), 'ICTACCOUNTMAP')
		  INTO v_id
		  FROM t506_returns
		 WHERE c506_rma_id = p_id;
	END IF;

	RETURN v_id;
EXCEPTION
	WHEN OTHERS
	THEN
		RETURN NULL;
END get_acct_party_id;
/
