CREATE OR REPLACE FUNCTION get_set_value (
	p_set_id	t207_set_master.c207_set_id%TYPE
	,p_type		CHAR
)
	RETURN NUMBER
IS
/*	Description 	: THIS FUNCTION RETURNS SET QTY FOR THE GIVEN PART NUMBER
 Parameters 		: p_part_id
*/
	v_set_cost	   NUMBER;
BEGIN
	--
	IF p_type = 'C'
	THEN
	SELECT sum(c208_set_qty * get_ac_cogs_value(t208.C205_PART_NUMBER_ID,4909))
		   INTO v_set_cost 
	  FROM t208_set_details t208
	 WHERE t208.c207_set_id = p_set_id 
	 AND t208.c208_void_fl IS NULL
 	 AND C208_INSET_FL = 'Y';
	END IF;
	--
	RETURN v_set_cost;

EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		RETURN 0;
END get_set_value;
/
