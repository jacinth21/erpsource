--@"C:\Database\Functions\Accounting\get_countryid_from_partyid.fnc"
CREATE OR REPLACE
    FUNCTION get_countryid_from_partyid (
      p_txntype IN t810_posting_txn.c901_account_txn_type%TYPE
     ,p_id      IN t810_posting_txn.c810_party_id%TYPE)
        RETURN NUMBER
    IS
        --
        v_country_id t901_code_lookup.c901_code_id%TYPE;
        v_cnt NUMBER;
        v_company_id_ctx  t1900_company.c1900_company_id%TYPE;
        --
    BEGIN
	
	 SELECT get_compid_frm_cntx() 
      	INTO v_company_id_ctx  
      FROM dual;
          
      --
     v_country_id := 1101;
     --1018 GMHQ
     IF v_company_id_ctx =1018
     THEN
       v_country_id := '';
     END IF;
      --
      SELECT COUNT(1) 
         INTO v_cnt
	FROM T804_POSTING_REF 
	WHERE C804_DELETE_FL = 'N'
	AND (C901_ACCOUNT_TYPE_CR = 4882 OR C901_ACCOUNT_TYPE_DR = 4882 )
	AND C901_TRANSACTION_TYPE = p_txntype;

	IF v_cnt = 1 --
	THEN 
		BEGIN
			--If Country ID India Chennai or India Delhi Then country is decoded to India
			SELECT DECODE(t906.C906_RULE_ID,102460,1103,102461,1103,t906.C906_RULE_ID)
			INTO v_country_id
			FROM T906_RULES t906
			--Rule grp ICT_CNTRY_DIST_MAP used in ETL and Ous Roll forward Transaction when apply OUS transaction in US OLTP
			 WHERE c906_RULE_GRP_ID = 'ICT_CNTRY_DIST_MAP' 
			 AND C906_RULE_VALUE = p_id -- Here Active flag condition is removed for PMT-5890 ,since OUS transaction is posting wrong country.
			 AND ROWNUM =1;
			 
		EXCEPTION
    	WHEN NO_DATA_FOUND THEN
    		--We need no data found bcoz when an MA is created the party ID is NULL and even though the account used will be 4882,but since the party ID is NULL and exception will be thrown.
    		--Hence for such 4882 transaction (MAs)we will put Country ID as US
    		v_country_id := 1101;
    		--1018 GMHQ
    		IF v_company_id_ctx =1018
     		THEN
       			v_country_id := '';
     		END IF;
    	END;
	
	END IF;

        RETURN v_country_id;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        --
        RETURN NULL;
        --
    END get_countryid_from_partyid;
    /
