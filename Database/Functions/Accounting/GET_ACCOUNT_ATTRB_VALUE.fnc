-- @"C:\Database\Functions\Accounting\GET_ACCOUNT_ATTRB_VALUE.fnc";
CREATE OR REPLACE
    FUNCTION "GET_ACCOUNT_ATTRB_VALUE" (
            p_acc_id T704_ACCOUNT.C704_ACCOUNT_ID%TYPE,
            p_attrb_type T704A_ACCOUNT_ATTRIBUTE.C901_ATTRIBUTE_TYPE%TYPE)
        RETURN VARCHAR2
    IS
        /* Description : THIS FUNCTION RETURNS ATTRIBVALUE GIVEN THE ATTRIBID
        Parameters   : p_acc_id, Attribute Type
        */
        v_acc_attrb_val T704A_ACCOUNT_ATTRIBUTE.C704A_ATTRIBUTE_VALUE%TYPE;
        v_party_id		T704_ACCOUNT.C101_PARTY_ID%TYPE;
        v_rule_carrier  VARCHAR2(10);
        v_company_id    t1900_company.c1900_company_id%TYPE;
    BEGIN
	    
	    --Getting company id from context.   
	    SELECT get_compid_frm_cntx()
		  INTO v_company_id 
		  FROM dual;
	  
	    /*
	     * 91980: FedEx Third Party Account#; 91982: Default Shipping Mode
	     * The values for third party account, shipping mode are not saving in the T704A_ACCOUNT_ATTRIBUTE table any more
	     * It is moved to the table T9080_SHIP_PARTY_PARAM. The values for those types should fetch from the table T9080_SHIP_PARTY_PARAM
	     */
	     
	    SELECT GET_RULE_VALUE_BY_COMPANY('SHIP_CARRIER','SHIP_CARR_MODE_VAL',v_company_id) INTO  v_rule_carrier FROM DUAL;
		
		IF p_attrb_type = 91980 
	    THEN
	    		--get party id from function , either be group acoount party or specific account party id
		    	v_party_id := gm_pkg_op_ship_charge.get_party_id_third_party(p_acc_id);
		ELSIF  p_attrb_type = 91982 or p_attrb_type = 105080
	    THEN
		    	v_party_id := gm_pkg_op_ship_charge.get_party_id_for_carrier (p_acc_id);
	    END IF;
		
		    IF p_attrb_type = 91980 --FedEx Third Party Account
		    THEN
		    	BEGIN
			    	SELECT C9080_THIRD_PARTY 
			    	  INTO v_acc_attrb_val
			    	  FROM T9080_SHIP_PARTY_PARAM 
			    	WHERE C101_PARTY_ID = v_party_id
			    		AND C9080_VOID_FL IS NULL;
		    	EXCEPTION
		        WHEN NO_DATA_FOUND THEN
		            RETURN '';
		        END;		        
		    ELSIF p_attrb_type = 91982 --Default Shipping Mode
		    THEN
		        BEGIN
			    	SELECT C901_SHIP_MODE 
			    	  INTO v_acc_attrb_val
			    	  FROM T9080_SHIP_PARTY_PARAM 
			    	WHERE C101_PARTY_ID = v_party_id
			    		AND C9080_VOID_FL IS NULL;
		    	EXCEPTION
		        WHEN NO_DATA_FOUND THEN
		            RETURN '';
		        END;
		    ELSIF p_attrb_type = 105080
		    THEN
		    	BEGIN
			    	SELECT DECODE(C901_SHIP_CARRIER,NULL,v_rule_carrier,C901_SHIP_CARRIER)
			    	  INTO v_acc_attrb_val
			    	  FROM T9080_SHIP_PARTY_PARAM 
			    	WHERE C101_PARTY_ID = v_party_id
			    		AND C9080_VOID_FL IS NULL;
		    	EXCEPTION
		        WHEN NO_DATA_FOUND THEN
		            v_acc_attrb_val := v_rule_carrier;
		        END;
		    END IF;
		    
	    IF v_acc_attrb_val IS NULL 
		THEN
	        BEGIN
	             SELECT C704A_ATTRIBUTE_VALUE
	               INTO v_acc_attrb_val
	               FROM T704A_ACCOUNT_ATTRIBUTE
	              WHERE C704A_VOID_FL      IS NULL
	                AND C704_ACCOUNT_ID     = p_acc_id
	                AND C901_ATTRIBUTE_TYPE = p_attrb_type;
	        EXCEPTION
	        WHEN NO_DATA_FOUND THEN
	            RETURN '';
	        END;
        END IF;
        RETURN v_acc_attrb_val;
    END GET_ACCOUNT_ATTRB_VALUE;
    /
