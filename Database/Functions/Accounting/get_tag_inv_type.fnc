/* Formatted on 2011/12/23 10:23 (Formatter Plus v4.8.0) */
--@"C:\Database\Functions\Accounting\get_tag_inv_type.fnc"

CREATE OR REPLACE FUNCTION get_tag_inv_type (
	p_txn	IN	 t5010_tag.c5010_last_updated_trans_id%TYPE
)
	RETURN NUMBER
IS
/***********************************************************************************************
 *	  Description	: This function will return transaction type based on the transaction
 ************************************************************************************************/
--
	v_txntype	   t901_code_lookup.c901_code_id%TYPE;
--
BEGIN
	--
	BEGIN
		--
		BEGIN
			--
			SELECT c504_type
			  INTO v_txntype
			  FROM t504_consignment
			 WHERE c504_consignment_id = p_txn;

			--
			RETURN v_txntype;
--
		EXCEPTION
			WHEN NO_DATA_FOUND	 --
							  THEN
				BEGIN
					SELECT c506_type
					  INTO v_txntype
					  FROM t506_returns
					 WHERE c506_rma_id = p_txn;

					RETURN v_txntype;
				EXCEPTION
					WHEN NO_DATA_FOUND	 --
									  THEN
						BEGIN
							SELECT c412_type
							  INTO v_txntype
							  FROM t412_inhouse_transactions
							 WHERE c412_inhouse_trans_id = p_txn;

							RETURN v_txntype;
						EXCEPTION
							WHEN NO_DATA_FOUND	 --
											  THEN
								SELECT c901_transfer_type
								  INTO v_txntype
								  FROM t920_transfer
								 WHERE c920_transfer_id = p_txn;

								RETURN v_txntype;
						END;
				END;
		END;
	--
	END;
--
EXCEPTION
	WHEN NO_DATA_FOUND THEN
--
		RETURN NULL;
--
END get_tag_inv_type;
/
