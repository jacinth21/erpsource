 
--@"c:\database\functions\operations\get_part_str_format";
--
/********************************************************************************************
 * Description	:This function returns the part format string part^control^expdate based on UDI.
 *********************************************************************************************/
--    
    
CREATE OR REPLACE FUNCTION get_part_str_format (
	p_udi_mrf	t2550_part_control_number.C2550_UDI_MRF%TYPE
)
	RETURN VARCHAR2
IS 
	v_string    VARCHAR (100);
--
BEGIN
        SELECT c205_part_number_id
        ||'^'
        || c2550_control_number 
        || DECODE(c2550_expiry_date, NULL,NULL, '^'|| to_char(c2550_expiry_date,'yyyy-mm')) 
      INTO v_string
      FROM t2550_part_control_number
      WHERE REPLACE(c2550_udi_mrf,'<FNC1>') = p_udi_mrf;

      
      
	RETURN v_string;
--
EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		RETURN '';
--
END get_part_str_format;
/
    