--@"c:\database\Functions\Operations\get_quantity_in_stock_byplant.fnc"
CREATE OR REPLACE FUNCTION get_quantity_in_stock_byplant (
	p_part_num	 t205_part_number.c205_part_number_id%TYPE,
	p_plant_id   t5040_plant_master.c5040_plant_id%TYPE
)
	RETURN NUMBER
IS
/* Description	  : This returns the Qty in shelf for a particular part
					Its a sql copied from v205_qty_in_stock view both sql should math all the time 
					  For easy maintenance all types are included in the view 	
   Parameters	  : p_part_num
*/
	v_shelf_qty    NUMBER;
--
BEGIN
	
	SELECT	 NVL (t205c.c205_available_qty, 0)
		   -  NVL (v205c.c205_allocated_qty, 0)			 
	  INTO v_shelf_qty
	  FROM t205c_part_qty t205c
		 , v205c_part_allocated_qty v205c
	 WHERE t205c.c205_part_number_id = p_part_num
	   AND t205c.C901_Transaction_Type  = 90800 --90800 - Inventory Qty
	   AND t205c.c205_part_number_id = v205c.c205_part_number_id(+)
	   AND v205c.c901_transaction_type(+) = '60001' -- FG Inventory
	   AND t205c.C5040_PLANT_ID 	= 	p_plant_id
	   AND t205c.C5040_PLANT_ID     =   v205c.C5040_PLANT_ID(+);
	--
	RETURN v_shelf_qty;
EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		RETURN 0;
END get_quantity_in_stock_byplant;
/
