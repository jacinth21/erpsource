/* Formatted on 2013/03/26 14:54 (Formatter Plus v4.8.8) */
--@"C:\Database\Functions\Operations\GET_QTY_IN_INVENTORY.FNC";
CREATE OR REPLACE FUNCTION GET_QTY_IN_INVENTORY (
    P_trans_id      IN   t504_consignment.C504_CONSIGNMENT_ID%TYPE,   
    p_wh_type       IN   t901_code_lookup.c901_code_id%TYPE,
    p_partnum       IN   t205c_part_qty.c205_part_number_id%TYPE
)
    RETURN NUMBER
IS
/*  Description     : This Function returns the Availability Qty for given Inventory
*/
    v_avail_qty          NUMBER;
    v_alloc_qty          NUMBER;
BEGIN
    BEGIN
	    IF p_wh_type = '90800' THEN	    -- FG  	
	    	v_avail_qty := get_qty_in_stock(p_partnum);	    
	    ELSIF p_wh_type = '90814' THEN  -- BULK
	    	v_avail_qty := get_qty_in_bulk(p_partnum); 
	    ELSIF p_wh_type = '90815' THEN  -- PACKAGING
	    	v_avail_qty := get_qty_in_packaging(p_partnum);
	    ELSIF p_wh_type = '90813' THEN  -- QUARANTINE
	    	v_avail_qty := get_qty_in_quarantine(p_partnum);
	    ELSIF p_wh_type = '90816' THEN  -- IH INVENTORY
	    	v_avail_qty := GET_QTY_IN_INHOUSEINV(p_partnum);	   
	    END IF;
    EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
            v_avail_qty := 0;
    END;
    
    BEGIN
	     SELECT NVL (SUM (alloc_qty), 0) INTO v_alloc_qty
		   FROM
		    (
		         SELECT NVL (SUM (c505_item_qty), 0) alloc_qty
		            --    INTO v_alloc_qty
		           FROM t505_item_consignment t505, t504_consignment t504
		          WHERE t505.c504_consignment_id = t504.c504_consignment_id
		            AND t505.c505_void_fl       IS NULL
		            AND t504.c504_void_fl       IS NULL
		            AND t505.c504_consignment_id = P_trans_id
		            AND t505.c205_part_number_id = p_partnum
		      UNION ALL
		         SELECT NVL (SUM (t502.c502_item_qty), 0) alloc_qty
		           FROM T502_ITEM_ORDER T502, T501_ORDER T501
		          WHERE T502.C501_ORDER_ID    = T501.C501_ORDER_ID
		            AND T502.C501_ORDER_ID    = P_trans_id
		            AND C205_PART_NUMBER_ID   = p_partnum
		            AND T502.C901_TYPE        = 50300
		            AND T501.C901_ORDER_TYPE IS NULL
		            AND T502.C502_VOID_FL    IS NULL
		            AND T501.C501_VOID_FL    IS NULL
		      UNION ALL
		         SELECT NVL (SUM (t413.c413_item_qty), 0) alloc_qty
		           FROM t412_inhouse_transactions t412, t413_inhouse_trans_items t413
		          WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
		            AND t413.c205_part_number_id   = p_partnum
		            AND t413.c412_inhouse_trans_id = P_trans_id
		            AND t412.c412_void_fl         IS NULL
		            AND t413.c413_void_fl         IS NULL
		    ) ;	    
	EXCEPTION
    WHEN NO_DATA_FOUND
    THEN
    	v_alloc_qty := 0;
	END;
    
	v_avail_qty := v_avail_qty - v_alloc_qty;
    
    RETURN v_avail_qty;
END GET_QTY_IN_INVENTORY;
/