
--@"c:\Database\Functions\Operations\get_division_from_loaner.fnc";

/*PC-577- Fedex/UPS  account split-up for shipping transactions
 *  Description    : THIS FUNCTION RETURNS DIVISION ID for Loaners
 Parameters       : p_txn_id
*/
CREATE OR REPLACE FUNCTION get_division_from_loaner(
   p_txn_id      t907_shipping_info.c907_ref_id%TYPE
)
   RETURN VARCHAR2
IS
   v_division_id   VARCHAR2(1000);
BEGIN
  SELECT compid INTO v_division_id
    FROM v700_territory_mapping_detail   v700,
         t907_shipping_info              t907,
         t525_product_request            t525
   WHERE t907.c525_product_request_id = t525.c525_product_request_id
     AND t525.c704_account_id         = v700.ac_id
     AND v700.rep_id                  = t525.c703_sales_rep_id
     AND t907.c907_ref_id             = p_txn_id --'GM-CN-138289'
     AND t907.c907_void_fl  IS NULL
     AND t525.c525_void_fl IS NULL
     AND ROWNUM = 1;
     
   RETURN v_division_id;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      RETURN '';
END get_division_from_loaner;
/


