/* Formatted on 2007/07/11 16:38 (Formatter Plus v4.8.0) */
CREATE OR REPLACE FUNCTION get_set_qty (
	p_set_id	t207_set_master.c207_set_id%TYPE
  , p_part_id	t205_part_number.c205_part_number_id%TYPE
)
	RETURN NUMBER
IS
/*	Description 	: THIS FUNCTION RETURNS SET QTY FOR THE GIVEN PART NUMBER
 Parameters 		: p_part_id
*/
	v_set_qty	   NUMBER;
BEGIN
	--
	SELECT c208_set_qty
	  INTO v_set_qty
	  FROM t208_set_details t208
	 WHERE t208.c207_set_id = p_set_id AND t208.c205_part_number_id = p_part_id AND t208.c208_void_fl IS NULL;

	--
	RETURN v_set_qty;

EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		RETURN 0;
END get_set_qty;
/
