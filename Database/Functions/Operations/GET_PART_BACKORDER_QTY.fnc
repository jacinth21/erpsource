 /***************************************************************
      * Description : Function to get backorder quantity of part
      * Author     : Rakhi Gandhi
      **********************************************************/

CREATE OR REPLACE FUNCTION GET_PART_BACKORDER_QTY
(
	p_consign_id    t504_consignment.c504_consignment_id%TYPE
      , p_part_num      t205_part_number.c205_part_number_id%TYPE
 )
 RETURN  NUMBER 
 IS
 /* Parameters: set consignment id , partid- return backorder quantity for the part
 */
        v_request_id   t520_request.c520_request_id%TYPE;
        v_bo_request_id t520_request.c520_request_id%TYPE;
	v_part_qty      NUMBER; 
    BEGIN
    	BEGIN
        -- Get MR from consignment table - Master MR ID
        SELECT c520_request_id
          INTO v_request_id
          FROM t504_consignment
         WHERE c504_consignment_id = p_consign_id;   -- FOR UPDATE;

        -- Check if parts are in backorder if so get the backorder entry
        SELECT c520_request_id
          INTO v_bo_request_id
          FROM t520_request t520
         WHERE c520_master_request_id = v_request_id AND t520.c520_status_fl = '10';   -- Backorder 10

        --get bo quantity for the part

          SELECT   t521.C521_QTY  partboqty
	  INTO     v_part_qty
	  FROM     T521_REQUEST_DETAIL  t521
	  WHERE    t521.C520_REQUEST_ID =  v_bo_request_id
		AND   t521.C205_PART_NUMBER_ID =   p_part_num ;

	 
	  EXCEPTION
          WHEN NO_DATA_FOUND
          THEN
          RETURN 0;
          END;
          RETURN v_part_qty;

    END GET_PART_BACKORDER_QTY;
   /
--