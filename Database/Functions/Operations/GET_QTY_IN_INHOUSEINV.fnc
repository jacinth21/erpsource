/* Formatted on 2013/03/26 14:54 (Formatter Plus v4.8.8) */
--@"C:\Database\Functions\Operations\GET_QTY_IN_INHOUSEINV.FNC";
CREATE OR REPLACE FUNCTION GET_QTY_IN_INHOUSEINV (
    p_partnum     IN   t205c_part_qty.c205_part_number_id%TYPE  
)
    RETURN NUMBER
IS
/*  Description     : This Function returns the Availability Qty in In-House Inventory
*/
    v_avail_qty          NUMBER;
    v_plant_id     		 T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
BEGIN
	SELECT  get_plantid_frm_cntx() INTO v_plant_id FROM DUAL;
    BEGIN 
	    SELECT	 NVL (t205c.c205_available_qty, 0)
		   -  NVL (v205c.c205_allocated_qty, 0)			 
	  INTO v_avail_qty
	  FROM t205c_part_qty t205c
		 , v205c_part_allocated_qty v205c
	 WHERE t205c.c205_part_number_id = p_partnum
	   AND t205c.C901_Transaction_Type  = 90816 -- Inhouse Inventory Qty
	   AND t205c.c205_part_number_id = v205c.c205_part_number_id(+)
	   AND v205c.c901_transaction_type(+) = '60012' -- IH Inventory
	   AND t205c.C5040_PLANT_ID 	= 	v_plant_id
	   AND t205c.C5040_PLANT_ID     =   v205c.C5040_PLANT_ID(+);    	    		 
    EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
            RETURN 0;
    END;
    RETURN v_avail_qty;
END GET_QTY_IN_INHOUSEINV;
/