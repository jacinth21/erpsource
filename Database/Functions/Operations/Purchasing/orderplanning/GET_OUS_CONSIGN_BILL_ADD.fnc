--@"C:\Database\Functions\Operations\Purchasing\orderplanning\GET_OUS_CONSIGN_BILL_ADD.fnc";

CREATE OR REPLACE FUNCTION GET_OUS_CONSIGN_BILL_ADD
(p_consign_id T504_CONSIGNMENT.C504_CONSIGNMENT_ID%TYPE)
RETURN VARCHAR2
IS
/*  Description     : THIS FUNCTION RETURNS CODE NAME GIVEN THE CODE_ID
 Parameters 		: p_cosign_id
*/
v_Id VARCHAR2(20);
v_bill_add VARCHAR2(1000);
v_type T504_CONSIGNMENT.C504_TYPE%TYPE;
v_ref_id T504_CONSIGNMENT.C504_REF_ID%TYPE;
v_loan_to VARCHAR2(2000);
BEGIN
    BEGIN
 	   SELECT C704_ACCOUNT_ID, C504_TYPE, C504_REF_ID 
		INTO v_Id, v_type, v_ref_id 
		FROM T504_CONSIGNMENT 
		WHERE C504_CONSIGNMENT_ID = p_consign_id
		AND C504_VOID_FL IS NULL;


		IF v_Id = '01' THEN
--SELECT C704_ACCOUNT_NM INTO v_bill_add FROM T704_ACCOUNT WHERE C704_ACCOUNT_ID = '01';
			SELECT DECODE(v_type,9110, v_loan_to, GET_USER_NAME(C504_SHIP_TO_ID)) 
			INTO v_bill_add 
			FROM T504_CONSIGNMENT 
			WHERE C504_CONSIGNMENT_ID = p_consign_id;
   
		ELSE
		   	SELECT A.C701_DISTRIBUTOR_NAME ||'<BR>&nbsp;'|| A.C701_BILL_ADD1 ||'<BR>&nbsp;'|| a.C701_BILL_ADD2 ||'<BR>&nbsp;'||
			   DECODE(NVL(GET_RULE_VALUE('SWAP_ZIP_CODE','SWAP_ZIP_CODE'),'N'),'Y',A.C701_BILL_ZIP_CODE,A.C701_BILL_CITY) 
			   ||',&nbsp;'|| GET_CODE_NAME_ALT(A.C701_BILL_STATE) || '&nbsp;'|| 
			   DECODE(NVL(GET_RULE_VALUE('SWAP_ZIP_CODE','SWAP_ZIP_CODE'),'N'),'Y',A.C701_BILL_CITY,A.C701_BILL_ZIP_CODE)
			   INTO v_bill_add
			   FROM T701_DISTRIBUTOR A,  T504_CONSIGNMENT B
			   WHERE B.C504_CONSIGNMENT_ID = p_consign_id AND A.C701_DISTRIBUTOR_ID = B.C701_DISTRIBUTOR_ID AND A.c901_ext_country_id IS NOT NULL ;

			END IF;
   EXCEPTION
      WHEN NO_DATA_FOUND
   THEN
      RETURN ' ';
  END;
 RETURN v_bill_add;
END GET_OUS_CONSIGN_BILL_ADD;
/

