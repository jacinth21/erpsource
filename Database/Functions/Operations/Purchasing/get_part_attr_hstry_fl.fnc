
--@"C:\Database\Functions\Operations\Purchasing\get_part_attr_hstry_fl.fnc";

CREATE OR REPLACE FUNCTION get_part_attr_hstry_fl (
	p_part_num	 t205d_part_attribute.c205_part_number_id%TYPE
  , p_attr_type	 VARCHAR
)
	RETURN VARCHAR2
IS
	 
	v_value 	   t205d_part_attribute.c205d_history_fl%TYPE;
BEGIN
	BEGIN
		SELECT C205d_History_Fl
			INTO v_value
		FROM t205d_part_attribute
		WHERE c205_part_number_id = p_part_num 
		AND c901_attribute_type = p_attr_type 
		AND c205d_void_fl is NULL
		AND c205d_history_fl IS NOT NULL;
	
		EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN 'N';
	END;

	RETURN v_value;
END get_part_attr_hstry_fl;
/