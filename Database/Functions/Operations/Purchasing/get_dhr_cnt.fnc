  /* Formatted on 2009/03/07 10:42 (Formatter Plus v4.8.0) */
--@"C:\pmt\bit\SpineIT-ERP\Database\Functions\Operations\Purchasing\get_dhr_cnt.fnc";
--Get DHR Count by using purchase order id (PC-5528 Option to Update PO Type)
CREATE OR REPLACE FUNCTION get_dhr_cnt (
 purchase_oid	 T401_PURCHASE_ORDER.C401_PURCHASE_ORD_ID%TYPE
)
	RETURN VARCHAR2
IS 
	dhrCount VARCHAR2(50);
BEGIN
	BEGIN
	 SELECT 
	    COUNT(1) INTO dhrCount
	FROM 
	    T401_PURCHASE_ORDER T401,
	    T402_WORK_ORDER t402,
	    T408_DHR T408
	WHERE
	    t402.C401_PURCHASE_ORD_ID = T401.C401_PURCHASE_ORD_ID
	    AND T408.C402_WORK_ORDER_ID     = t402.C402_WORK_ORDER_ID
	    AND T401.C401_VOID_FL          IS NULL
	    AND t402.C402_VOID_FL          IS NULL
	    AND T408.C408_VOID_FL          IS NULL
	    AND T401.C401_PURCHASE_ORD_ID   = purchase_oid
	    GROUP BY T401.C401_PURCHASE_ORD_ID;
	    
	
	
	EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
	dhrCount := ' ';
	END;
	
	RETURN dhrCount;
 END get_dhr_cnt;
/	    
    
