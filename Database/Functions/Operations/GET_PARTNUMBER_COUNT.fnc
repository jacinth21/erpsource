/* Formatted on 2007/03/05 12:24 (Formatter Plus v4.8.0) */
CREATE OR REPLACE FUNCTION get_partnumber_count (
	p_part_id	 t502_item_order.c205_part_number_id%TYPE
  , p_type		 VARCHAR2
  , p_fromdate	 VARCHAR2
  , p_todate	 VARCHAR2
)
	RETURN VARCHAR2
IS
/*	Description 	: THIS FUNCTION RETURNS CODE NAME GIVEN THE CODE_ID
 Parameters 		: p_code_id
*/
	v_item_qty	 NUMBER;
	v_cong_qty	 NUMBER;
	v_sum		 NUMBER;
BEGIN
	BEGIN
		IF p_type = 'S'
		THEN
			SELECT NVL (SUM (a.c502_item_qty), 0)
			  INTO v_sum
			  FROM t502_item_order a, t501_order b
			 WHERE a.c205_part_number_id = p_part_id
			   AND a.c501_order_id = b.c501_order_id
			   AND c501_order_date >= TO_DATE (p_fromdate, 'mm/dd/yyyy')
			   AND c501_order_date <= TO_DATE (p_todate, 'mm/dd/yyyy')
			   AND c501_delete_fl IS NULL
			   AND (c901_ext_country_id IS NULL  OR c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))-- To include ext country id for GH
			   AND c501_void_fl IS NULL;
		/*SELECT nvl(sum(C502_ITEM_QTY),0) into v_sum
		FROM T502_ITEM_ORDER
		WHERE C205_PART_NUMBER_ID = p_part_id;*/
		ELSIF p_type = 'C'
		THEN
			SELECT NVL (SUM (a.c505_item_qty), 0)
			  INTO v_sum
			  FROM t505_item_consignment a, t504_consignment b
			 WHERE a.c205_part_number_id = p_part_id
			   AND a.c504_consignment_id = b.c504_consignment_id
			   AND b.c504_ship_date BETWEEN TO_DATE (p_fromdate, 'mm/dd/yyyy') AND TO_DATE (p_todate, 'mm/dd/yyyy')
			   AND TRIM (a.c505_control_number) IS NOT NULL
			   --AND to_char(B.C504_SHIP_DATE,'mm/dd/yyyy') >= p_FromDate
			   --AND to_char(B.C504_SHIP_DATE,'mm/dd/yyyy') <= p_ToDate
			   AND b.c704_account_id IS NULL
			   AND b.c504_type <> 4129
			   AND b.c504_void_fl IS NULL;
		ELSIF p_type = 'I'
		THEN
			SELECT NVL (SUM (a.c505_item_qty), 0)
			  INTO v_sum
			  FROM t505_item_consignment a, t504_consignment b
			 WHERE a.c205_part_number_id = p_part_id
			   AND a.c504_consignment_id = b.c504_consignment_id
			   AND b.c504_ship_date BETWEEN TO_DATE (p_fromdate, 'mm/dd/yyyy') AND TO_DATE (p_todate, 'mm/dd/yyyy')
			   --AND to_char(B.C504_LAST_UPDATED_DATE,'mm/dd/yyyy') >= p_FromDate
			   --AND to_char(B.C504_LAST_UPDATED_DATE,'mm/dd/yyyy') <= p_ToDate
			   AND b.c704_account_id = '01'
			   AND b.c504_void_fl IS NULL;
		ELSE
			SELECT NVL (SUM (c502_item_qty), 0)
			  INTO v_item_qty
			  FROM t502_item_order
			 WHERE c205_part_number_id = p_part_id;

			SELECT NVL (SUM (c505_item_qty), 0)
			  INTO v_cong_qty
			  FROM t505_item_consignment
			 WHERE c205_part_number_id = p_part_id;

			v_sum					   := v_cong_qty + v_item_qty;
		END IF;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN ' ';
	END;

	RETURN v_sum;
END get_partnumber_count;
/
