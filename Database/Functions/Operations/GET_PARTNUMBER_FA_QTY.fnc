CREATE OR REPLACE FUNCTION GET_PARTNUMBER_FA_QTY
(
 	   p_PartNum  IN T402_WORK_ORDER.C205_PART_NUMBER_ID%TYPE
)
RETURN NUMBER
IS
/*  Description     : THIS FUNCTION RETURNS
 Parameters 		: p_code_id
*/

v_rec_cnt NUMBER;
BEGIN
     BEGIN

	 	  SELECT SUM(A.C505_ITEM_QTY) INTO v_rec_cnt
		  FROM T505_ITEM_CONSIGNMENT A, T504_CONSIGNMENT B
		  WHERE A.C205_PART_NUMBER_ID = p_PartNum
		  AND A.C504_CONSIGNMENT_ID = B.C504_CONSIGNMENT_ID
		  AND A.C505_CONTROL_NUMBER <> ' '
		  AND B.C504_STATUS_FL = '2'
		  AND B.C207_SET_ID IS NOT NULL
		  AND B.C504_VERIFY_FL = '1'
		  AND B.C504_VOID_FL IS NULL;


	   	  IF v_rec_cnt IS NULL
	   	  	 THEN v_rec_cnt := 0;
	   	  END IF;
   EXCEPTION
      WHEN NO_DATA_FOUND
   THEN
      RETURN ' ';
      END;
     RETURN v_rec_cnt;
END GET_PARTNUMBER_FA_QTY;
/

