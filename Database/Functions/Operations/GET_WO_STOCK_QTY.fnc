CREATE OR REPLACE FUNCTION GET_WO_STOCK_QTY
(
     p_wo_id T408_DHR.C402_WORK_ORDER_ID%TYPE
)
RETURN VARCHAR2
IS
/*  Description     : THIS FUNCTION RETURNS CODE NAME GIVEN THE CODE_ID
 Parameters   : p_code_id
*/
v_rec_cnt NUMBER;
--
BEGIN
--
     BEGIN
     --
        SELECT SUM(C408_QTY_ON_SHELF)
        INTO v_rec_cnt
  FROM  T408_DHR T408
        WHERE T408.C402_WORK_ORDER_ID = p_wo_id
        AND   T408.C408_VOID_FL IS NULL AND t408.c408_dhr_id NOT LIKE 'GM-RC%'; -- should skip RC DHR which is already verified (4)
     --
   EXCEPTION
      WHEN NO_DATA_FOUND
   THEN
      RETURN ' ';
      END;
     RETURN v_rec_cnt;
END GET_WO_STOCK_QTY;
/

