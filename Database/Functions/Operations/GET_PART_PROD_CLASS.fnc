CREATE OR REPLACE FUNCTION GET_PART_PROD_CLASS(
      p_part_num T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE
)
RETURN VARCHAR2
IS
/*  Description     : This Function returns whether the part is sterile or not
     Parameters        : p_part_num
*/
v_prod_class  VARCHAR2(20);
BEGIN
      BEGIN
            SELECT T205.C205_PRODUCT_CLASS INTO v_prod_class
            FROM T205_PART_NUMBER T205
            WHERE t205.c205_part_number_id = p_part_num;
  	  EXCEPTION
      		WHEN NO_DATA_FOUND
      		THEN
       		RETURN NULL;
      END;
     RETURN v_prod_class;
END GET_PART_PROD_CLASS;
/

