/* Formatted on 2010/12/14 10:24 (Formatter Plus v4.8.0) */
--@"C:\Database\Functions\Operations\GET_NEXT_CONSIGN_ID.fnc";

CREATE OR REPLACE FUNCTION get_next_consign_id (
	p_opt	VARCHAR2
)
	RETURN VARCHAR2
IS
/*	Description 	: THIS FUNCTION RETURNS CODE NAME GIVEN THE CODE_ID
 Parameters   : p_code_id
*/
    v_con_id       NUMBER;
    v_string       VARCHAR2 (20);
    v_id_string    VARCHAR2 (20);
    v_consignid    VARCHAR2 (20);
    v_trans_prefix VARCHAR2(20);
    v_rule_grp		VARCHAR2(20);
--
BEGIN
    SELECT s504_consign.NEXTVAL
      INTO v_con_id
      FROM DUAL;

   SELECT GET_RULE_VALUE(p_opt,'TRANS_CONTY') INTO v_rule_grp FROM DUAL;
	 	
	 	-- THIS WILL HANDLE THE SITUATION WHERE THE p_opt IS NOT AN ID FROM THE DB.
	 	IF v_rule_grp IS NULL THEN	 	
	 		v_rule_grp := p_opt;	 		
	 	END IF;
	 	
   SELECT get_rule_value('TRANS_PREFIX',v_rule_grp) INTO v_trans_prefix FROM DUAL;

    --
    IF LENGTH (v_con_id) = 1
    THEN
        v_id_string := '0' || v_con_id;
    ELSE
        v_id_string := v_con_id;
    END IF;
    
    SELECT 'GM-CN-' || v_id_string
        INTO v_string
    FROM DUAL;

 IF v_trans_prefix IS NOT NULL
    THEN
    	SELECT v_trans_prefix ||v_id_string
          INTO v_string
          FROM DUAL;
     END IF;
     
	v_consignid := v_string;
	RETURN v_consignid;
EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		RETURN '';
END get_next_consign_id;
/
