CREATE OR REPLACE FUNCTION GET_WO_WIP_QTY
(
 p_wo_id t408_dhr.c402_work_order_id%TYPE
)
RETURN NUMBER
IS
--
/********************************************************************************************
 * Description  :This function returns the Work In Progress Qty of a Work Order
 * Change Details:
 *   James Jun3,2006 Changed Formatting according to Standards
   James Jun3,2006 Changes for UOM
 *********************************************************************************************/
--
v_rec_cnt NUMBER;
--
BEGIN
 SELECT NVL(SUM(c408_shipped_qty),0)
 INTO v_rec_cnt
 FROM t408_dhr t408
 WHERE t408.c408_status_fl < 4
 AND t408.c402_work_order_id = p_wo_id
 AND t408.c408_void_fl IS NULL ;
RETURN v_rec_cnt;
--
   EXCEPTION
      WHEN NO_DATA_FOUND
   THEN
      RETURN 0;
--
END GET_WO_WIP_QTY;
/

