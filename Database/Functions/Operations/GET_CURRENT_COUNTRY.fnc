 
--@"c:\database\functions\operations\GET_CURRENT_COUNTRY";
--
/*********************************************************** 
 * Description	:This function returns the current country.
 *********************************************************************************************/
--    
    
CREATE OR REPLACE FUNCTION GET_CURRENT_COUNTRY  
	RETURN VARCHAR2
IS 
 	v_rule_id	   t906_rules.c906_rule_id%TYPE;
	v_rule_val	   t906_rules.c906_rule_value%TYPE;	
--
BEGIN
        SELECT C906_RULE_VALUE
		 INTO v_rule_id
         FROM t906_rules
   		WHERE c906_rule_id = 'CURRENT_DB' ;

      	SELECT c906_rule_value rule_value 
		INTO v_rule_val 
         FROM t906_rules
   		WHERE c906_rule_id = v_rule_id
   		 AND  c906_rule_grp_id = 'LBLCHK';
      
	RETURN v_rule_val;
--
EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		RETURN '';
--
END GET_CURRENT_COUNTRY;
/
    