--@"C:\Database\Functions\Operations\GET_QTY_ALLOCATED.fnc";

CREATE OR REPLACE FUNCTION GET_QTY_ALLOCATED
(
 p_part_num T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE
 , p_type T901_CODE_LOOKUP.C901_CODE_ID%TYPE
)
RETURN NUMBER
IS
/*  Description     : THIS FUNCTION RETURNS QTY ALLOCATED FOR THE SELECTED PART NUMBER
 *  Parameters   : PART_NUMBER
*/
v_qty NUMBER;
v_inv_trans VARCHAR2(100);
v_plant_id     T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
BEGIN
--
	SELECT  NVL(get_plantid_frm_cntx(),GET_RULE_VALUE('DEFAULT_PLANT','ONEPORTAL')) INTO v_plant_id FROM DUAL;
	SELECT GET_RULE_VALUE(p_type,'INV_TRANS') INTO v_inv_trans FROM DUAL;
	
IF v_inv_trans IS NOT NULL
THEN
my_context.set_my_inlist_ctx (v_inv_trans);

    SELECT  NVL(SUM (t505.c505_item_qty),0)
    INTO v_qty
             FROM t504_consignment t504, t505_item_consignment t505
             WHERE t504.c504_consignment_id = t505.c504_consignment_id
               AND t504.c504_status_fl < 4
               AND t504.c207_set_id IS NULL
               AND t504.c504_type IN (SELECT * FROM v_in_list)
               AND t504.c504_void_fl IS NULL
               AND t504.c504_ship_date IS NULL
               AND t504.c5040_plant_id = v_plant_id
               AND t505.c205_part_number_id = p_part_num;
 END IF;
 IF v_qty IS NULL THEN
 v_qty := 0;
 END IF;
 
RETURN v_qty;
--
EXCEPTION
WHEN NO_DATA_FOUND
THEN
      RETURN 0;
END GET_QTY_ALLOCATED;
/

