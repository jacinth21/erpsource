--@"C:\Database\Functions\Operations\GET_QTY_IN_INHOUSE.fnc"

CREATE OR REPLACE FUNCTION get_qty_in_inhouse (
	p_part_num	 t205_part_number.c205_part_number_id%TYPE
)
	RETURN NUMBER
IS
/*	Description 	: This function returns qty available in the InHouse Shelf
 *  Parameters 		: p_part_num
 */
	v_ih_qty	   NUMBER;
	v_plant_id     T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
--
BEGIN
	SELECT  get_plantid_frm_cntx() INTO v_plant_id FROM DUAL;
	BEGIN
		SELECT	NVL(t205.c205_available_qty,0)  - (  NVL (ihshelf_alloc_to_bulk, 0)) c205_inhouse_qty
		  INTO v_ih_qty
		  FROM  t205c_part_qty t205 				
			 , (SELECT	c205_part_number_id, SUM (DECODE (a.c412_type, 1006466, c413_item_qty, 0)) ihshelf_alloc_to_bulk					   
					FROM t412_inhouse_transactions a, t413_inhouse_trans_items b
				   WHERE a.c412_inhouse_trans_id = b.c412_inhouse_trans_id
					 AND a.c412_verified_date IS NULL
					 AND a.c412_void_fl IS NULL
					 AND a.c412_type IN (1006466)		-- IHBL
					 AND b.c205_part_number_id = p_part_num
					 AND a.C5040_PLANT_ID 	= v_plant_id
				GROUP BY b.c205_part_number_id) ihtxn			 
		 WHERE t205.c205_part_number_id = p_part_num		
		   AND t205.c901_transaction_type = 90816 -- In House Shelf Qty   
		   AND t205.C5040_PLANT_ID 	= v_plant_id 
		   AND t205.c205_part_number_id = ihtxn.c205_part_number_id(+);
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN 0;
	END;

	RETURN v_ih_qty;
END get_qty_in_inhouse;
/
