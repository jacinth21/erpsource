/******************************************************************
* Author      :  Anjana Rajan 
* Description : Get the total number of qty in set for the part
*****************************************************************/
--@"C:\Database\Functions\Operations\get_qty_in_set.fnc";

CREATE OR REPLACE FUNCTION get_qty_in_set (
        p_consign_id 	IN 	t504_consignment.c504_consignment_id%TYPE
	  , p_part_num	IN	t205_part_number.c205_part_number_id%TYPE)
    RETURN NUMBER
IS
    v_qtyinset NUMBER;
BEGIN
	BEGIN
		SELECT NVL(cons.qty, 0) - NVL (miss.qty, 0) INTO v_qtyinset
		FROM t205_part_number t205,
		  (SELECT t505.c205_part_number_id,
			SUM (t505.c505_item_qty) qty
		  FROM t505_item_consignment t505
		  WHERE t505.c504_consignment_id = p_consign_id
		  AND t505.c205_part_number_id   = p_part_num
		  AND t505.c505_void_fl         IS NULL
		  GROUP BY t505.c205_part_number_id
		  ) cons,
		  (SELECT c205_part_number_id,
			SUM (qty) qty
		  FROM
			(SELECT t413.c205_part_number_id,
			  DECODE (c412_type, 50151, 0, DECODE (t413.c413_status_fl, 'Q', NVL (t413.c413_item_qty, 0 ), 0 ) ) qty
			FROM t412_inhouse_transactions t412,
			  t413_inhouse_trans_items t413
			WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
			AND t412.c412_ref_id             = p_consign_id
			AND t413.c205_part_number_id     = p_part_num
			AND t412.c412_void_fl           IS NULL
			AND t413.c413_void_fl           IS NULL
			)
		  GROUP BY c205_part_number_id
		  ) miss
		WHERE t205.c205_part_number_id = cons.c205_part_number_id
		AND t205.c205_part_number_id   = miss.c205_part_number_id(+);
		RETURN v_qtyinset;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN 0;
		END;
END get_qty_in_set ;
/ 