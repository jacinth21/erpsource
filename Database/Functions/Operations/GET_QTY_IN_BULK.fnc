--@"C:\Database\Functions\Operations\GET_QTY_IN_BULK.fnc"

CREATE OR REPLACE FUNCTION get_qty_in_bulk (
	p_part_num	 t205_part_number.c205_part_number_id%TYPE
)
	RETURN NUMBER
IS
/*	Description 	: This function returns Bulk allocation qty, its a sql copied from 
					  v205_qty_in_stock view both sql should math all the time 
					  For easy maintenance all types are included in the view 	
 Parameters 		: p_code_id
*/
	v_bulk_qty	   NUMBER;
	v_plant_id     T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
--
BEGIN
	SELECT  get_plantid_frm_cntx() INTO v_plant_id FROM DUAL;
	BEGIN
		SELECT	NVL(t205c.c205_available_qty,0)  - (  NVL (v205c.c205_allocated_qty, 0) + NVL (in_wip_set, 0) + NVL (tbe_alloc, 0)				   
				 ) 
		  INTO v_bulk_qty
		  FROM t205c_part_qty t205c
			 , v205c_part_allocated_qty v205c
			 --The following existing query used to identify allocated quantity in consignment set
			 ,  (SELECT   a.c205_part_number_id
		   			 , SUM (DECODE (a.c505_control_number, 'TBE', 0, a.c505_item_qty))  in_wip_set					 
		   			 , SUM (DECODE (a.c505_control_number, 'TBE', a.c505_item_qty, 0)) tbe_alloc
				  FROM t505_item_consignment a, t504_consignment b
				 WHERE a.c504_consignment_id = b.c504_consignment_id
				   AND TRIM (a.c505_control_number) IS NOT NULL
				   AND b.c504_status_fl < '3'  --PC-4748-Tuning-T504_CONSIGNMENT
				   AND b.c207_set_id IS NOT NULL
				   AND b.c504_verify_fl = '0'
				   AND b.c504_void_fl IS NULL
				   AND a.c205_part_number_id =  p_part_num
				   AND b.C5040_PLANT_ID = v_plant_id
			  GROUP BY a.c205_part_number_id) wip
		 WHERE t205c.c205_part_number_id = p_part_num
		   AND t205c.C901_Transaction_Type  = 90814 --90814 - Bulk Qty
	  	   AND t205c.c205_part_number_id = v205c.c205_part_number_id(+) 
	       AND v205c.c901_transaction_type(+) = '60003' -- Bulk Inventory
	       AND t205c.C5040_PLANT_ID 	= 	v_plant_id
	   	   AND t205c.C5040_PLANT_ID       = v205c.C5040_PLANT_ID(+)
		   AND t205c.c205_part_number_id = wip.c205_part_number_id(+);
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN 0;
	END;

	RETURN v_bulk_qty;
END get_qty_in_bulk;
/
