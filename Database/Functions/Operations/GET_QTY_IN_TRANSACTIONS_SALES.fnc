CREATE OR REPLACE FUNCTION Get_Qty_In_Transactions_Sales
(
	p_part_num T205_PART_NUMBER.C205_PART_NUMBER_ID%TYPE
)
RETURN NUMBER
IS
/*  Description     : THIS FUNCTION RETURNS CODE NAME GIVEN THE CODE_ID
 Parameters   : p_code_id
*/
v_trans_qty NUMBER;
--
BEGIN
--
	SELECT	NVL(SUM(b.c502_item_qty),0)
	INTO	v_trans_qty
	FROM	t501_order A,
		t502_item_order B
	WHERE	a.c501_order_id = b.c501_order_id
	AND	a.c501_status_fl = 1
	AND	a.c501_void_fl IS NULL
	AND	a.c901_order_type IS NULL
	AND	b.c901_type = 50300	
	AND	b.c205_part_number_id = p_part_num;
--
	RETURN v_trans_qty;
END Get_Qty_In_Transactions_Sales;
/

