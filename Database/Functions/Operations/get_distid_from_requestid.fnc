--@"C:\Database\Functions\Operations\get_distid_from_requestid.fnc";

/*******************************************************
* Purpose: To get distid from request id for T907 table
* Author: Velu
********************************************************/

CREATE OR REPLACE FUNCTION get_distid_from_requestid (
	p_ref_id	IN	 VARCHAR2
)
	RETURN VARCHAR2
IS
	v_dist_id	  t504_consignment.c701_distributor_id%TYPE;
BEGIN
	BEGIN
		SELECT C701_DISTRIBUTOR_ID INTO v_dist_id FROM t504_consignment
			WHERE C520_REQUEST_ID = p_ref_id AND C504_void_fl IS NULL;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			BEGIN
				SELECT C701_DISTRIBUTOR_ID INTO v_dist_id FROM t504_consignment 
				WHERE c504_consignment_id = p_ref_id AND C504_void_fl IS NULL;
			EXCEPTION
				WHEN NO_DATA_FOUND
				THEN
				v_dist_id := null;
			END;
	END;
	RETURN v_dist_id;
END get_distid_from_requestid;
/
