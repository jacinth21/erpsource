-- @"C:\Database\Functions\Operations\get_txn_value.fnc";
CREATE OR REPLACE
    FUNCTION get_txn_value (
            p_refid t907_shipping_info.c907_ref_id%TYPE,
            p_source t907_shipping_info.c901_source%TYPE,
            p_type VARCHAR2)
        RETURN VARCHAR2
    IS
        v_id VARCHAR2 (20) ;
        v_refid t907_shipping_info.c907_ref_id%TYPE;
    BEGIN
        -- 50180  order ,-- 50181  Consignments , 50182  Loaners , 50183 Loaner Extn
        IF p_source    = '50180' THEN
            IF (p_type = 'REP') THEN -- order
                 SELECT c703_sales_rep_id
                   INTO v_id
                   FROM t501_order
                  WHERE c501_order_id = p_refid
                    AND c501_void_fl IS NULL;
            ELSIF (p_type             = 'ACC') THEN
                 SELECT c704_account_id
                   INTO v_id
                   FROM t501_order
                  WHERE c501_order_id = p_refid
                    AND c501_void_fl IS NULL ;
            ELSIF (p_type             = 'DIST') THEN
                 SELECT GET_DISTRIBUTOR_ID(c703_sales_rep_id)
                   INTO v_id
                   FROM t501_order
                  WHERE c501_order_id = p_refid
                    AND c501_void_fl IS NULL;
            END IF;
        ELSIF p_source = '50182' THEN -- loaners
            IF (p_type = 'REP') THEN
                BEGIN
                     SELECT t525.c703_sales_rep_id
                       INTO v_id
                       FROM t525_product_request t525, t526_product_request_detail t526
                      WHERE TO_CHAR(t526.c526_product_request_detail_id) = p_refid
                        AND t526.c525_product_request_id        = t525.c525_product_request_id
                        AND t525.c525_void_fl                  IS NULL
                        AND t526.c526_void_fl                  IS NULL;
                EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    BEGIN
                         SELECT c703_sales_rep_id
                           INTO v_id
                           FROM t504a_loaner_transaction
                          WHERE c504_consignment_id = p_refid
                            AND c504a_return_dt    IS NULL
                            AND c504a_void_fl      IS NULL;
                    EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                        v_id := '';
                    END;
                END ;
            ELSIF (p_type = 'ACC') THEN
                BEGIN
                     SELECT t525.c704_account_id
                       INTO v_id
                       FROM t525_product_request t525, t526_product_request_detail t526
                      WHERE TO_CHAR(t526.c526_product_request_detail_id) = p_refid
                        AND t526.c525_product_request_id        = t525.c525_product_request_id
                        AND t525.c525_void_fl                  IS NULL
                        AND t526.c526_void_fl                  IS NULL;
                EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    BEGIN
                         SELECT c704_account_id
                           INTO v_id
                           FROM t504a_loaner_transaction
                          WHERE c504_consignment_id = p_refid
                            AND c504a_return_dt    IS NULL
                            AND c504a_void_fl      IS NULL;
                    EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                        v_id := '';
                    END;
                END ;
            ELSIF (p_type = 'DIST') THEN
                BEGIN
                     SELECT t525.c525_request_for_id
                       INTO v_id
                       FROM t525_product_request t525, t526_product_request_detail t526
                      WHERE TO_CHAR(t526.c526_product_request_detail_id) = p_refid
                        AND t526.c525_product_request_id        = t525.c525_product_request_id
                        AND t525.c525_void_fl                  IS NULL
                        AND t526.c526_void_fl                  IS NULL;
                EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    BEGIN
                         SELECT c504a_consigned_to_id
                           INTO v_id
                           FROM t504a_loaner_transaction
                          WHERE c504_consignment_id = p_refid
                            AND c504a_return_dt    IS NULL
                            AND c504a_void_fl      IS NULL;
                    EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                        v_id := '';
                    END;
                END ;
            END IF;
        ELSIF p_source = '50183' THEN --Loaner Extn
            BEGIN
                 SELECT c504a_loaner_transaction_id
                   INTO v_refid
                   FROM t412_inhouse_transactions
                  WHERE c412_inhouse_trans_id = p_refid
                    AND c412_void_fl         IS NULL;
            EXCEPTION
            WHEN NO_DATA_FOUND THEN
                v_refid := '';
            END;
            IF (p_type = 'REP') THEN
                BEGIN
                     SELECT c703_sales_rep_id
                       INTO v_id
                       FROM t504a_loaner_transaction
                      WHERE c504a_loaner_transaction_id = v_refid
                        AND c504a_void_fl              IS NULL;
                EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    v_id := '';
                END;
            ELSIF (p_type = 'ACC') THEN
                BEGIN
                     SELECT c704_account_id
                       INTO v_id
                       FROM t504a_loaner_transaction
                      WHERE c504a_loaner_transaction_id = v_refid
                        AND c504a_void_fl              IS NULL;
                EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    v_id := '';
                END;
            ELSIF (p_type = 'DIST') THEN
                BEGIN
                     SELECT c504a_consigned_to_id
                       INTO v_id
                       FROM t504a_loaner_transaction
                      WHERE c504a_loaner_transaction_id = v_refid
                        AND c504a_void_fl              IS NULL;
                EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    v_id := '';
                END;
            END IF;
        ELSIF p_source    = '50181' THEN -- Consignments
            IF (p_type    = 'REP') THEN
                v_id     := '';
            ELSIF (p_type = 'ACC') THEN
                v_id     := '';
            ELSIF (p_type = 'DIST') THEN
                BEGIN
                     SELECT c520_request_to
                       INTO v_id
                       FROM t520_request
                      WHERE c520_request_id = p_refid
                        AND c520_void_fl   IS NULL;
                EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    BEGIN
                         SELECT c701_distributor_id
                           INTO v_id
                           FROM t504_consignment
                          WHERE c504_consignment_id = p_refid
                            AND c504_void_fl       IS NULL;
                    EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                        v_id := '';
                    END;
                END ;
            END IF;
        END IF;
        RETURN v_id;
    END get_txn_value;
    /