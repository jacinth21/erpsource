--@"C:\Database\Functions\Operations\GET_PNUM_EXPDT_BY_SLIFE.fnc";
CREATE OR REPLACE
    FUNCTION GET_PNUM_EXPDT_BY_SLIFE (
            p_part_num	t205d_part_attribute.c205_part_number_id%TYPE
          , p_type		t205d_part_attribute.c901_attribute_type%TYPE
          )
    RETURN date
    IS
        /*  Description     : THIS FUNCTION RETURNS THE EXPIRY DATE FOR THE CORRESPONDING PART NUMBER
         *	Parameters      : Part Number
         */
        v_shelf_life 	t205d_part_attribute.c205d_attribute_value%TYPE;
        v_date_fmt		t906_rules.c906_rule_value%TYPE;
        v_exp_date		DATE;
    BEGIN
	    v_date_fmt := GET_RULE_VALUE('DATEFMT','DATEFORMAT');
	    
        BEGIN
	        -- Get the corresponding SHELF LIFE from part attribute table 
             SELECT C205D_ATTRIBUTE_VALUE INTO v_shelf_life
             	FROM T205D_PART_ATTRIBUTE
				WHERE C205_PART_NUMBER_ID = p_part_num
				AND C901_ATTRIBUTE_TYPE = p_type
				AND C205D_VOID_FL is null;
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_shelf_life := NULL;
        END;
        
        IF v_shelf_life IS NOT NULL
        THEN
        -- calculate the expiry date for the part by adding the shelf life with the current month
        	SELECT to_date(TO_CHAR(ADD_MONTHS(SYSDATE,v_shelf_life),v_date_fmt),v_date_fmt)
			INTO v_exp_date
     		FROM DUAL;
        END IF;
        
        RETURN v_exp_date;
END GET_PNUM_EXPDT_BY_SLIFE;
/