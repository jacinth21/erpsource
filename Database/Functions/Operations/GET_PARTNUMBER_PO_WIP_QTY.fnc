/* Formatted on 2009/05/13 15:52 (Formatter Plus v4.8.0) */
CREATE OR REPLACE FUNCTION get_partnumber_po_wip_qty (
	p_partnum	IN	 t402_work_order.c205_part_number_id%TYPE
)
	RETURN VARCHAR2
IS
--
/********************************************************************************************
 * Description	:This function returns the WIP Qty for a Part
 * Change Details:
 *	 James Jun23,2006 : Changed Formatting according to Standards
	 James Jun23,2006 : Changes for including UOM Changes
	 James Dec21,2006 : Joined query with NCMR table to get CloseOut Qty
 *********************************************************************************************/
--
	v_cnt		   VARCHAR2 (10);
	v_rec_cnt	   NUMBER;
	v_plant_id     T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
--
BEGIN
	SELECT  get_plantid_frm_cntx() INTO v_plant_id FROM DUAL;
	
	SELECT	 NVL (SUM (t408.c408_shipped_qty), 0)
		   - NVL (SUM (t408.c408_qty_rejected), 0)
		   + NVL (SUM (t409.c409_closeout_qty), 0)
	  INTO v_rec_cnt
	  FROM t408_dhr t408, t409_ncmr t409
	 WHERE t408.c408_dhr_id = t409.c408_dhr_id(+)
	   AND t408.c408_status_fl < 4
	   AND t408.c205_part_number_id = p_partnum
	   AND t408.c408_void_fl IS NULL
	   AND t409.c409_void_fl IS NULL
	   AND t408.C5040_PLANT_ID = v_plant_id
	   AND t408.C5040_PLANT_ID = t409.C5040_PLANT_ID(+);

/*
	SELECT	NVL(SUM(c408_shipped_qty),0) - NVL(SUM(c408_qty_rejected),0)
	INTO	v_rec_cnt
	FROM	t408_dhr
	WHERE	c408_status_fl < 4
	AND c205_part_number_id = p_partnum
	AND c408_void_fl IS NULL;
*/	--
	IF v_rec_cnt IS NULL
	THEN
		v_rec_cnt	:= 0;
	END IF;

	--
	RETURN v_rec_cnt;
EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		RETURN ' ';
END get_partnumber_po_wip_qty;
/
