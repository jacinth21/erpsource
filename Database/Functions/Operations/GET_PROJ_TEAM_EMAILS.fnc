-- @"c:\database\functions\operations\get_proj_team_emails.fnc";

create or replace FUNCTION GET_PROJ_TEAM_EMAILS (
        p_projectid 	IN		 t204_project_team.c202_project_id%TYPE,
        p_vendor_id		IN		 t301_vendor.c301_vendor_id%TYPE
    )
        RETURN VARCHAR2
    IS
        otherinfo       VARCHAR2 (4000);
        v_purchasing_agent_email t101_user.c101_email_id%TYPE;
    BEGIN
        WITH other_info AS (        
             SELECT t101.c101_email_id email_id, t204.c101_party_id id 
               FROM t204_project_team t204, t101_user t101, t101_party t101p
        	  WHERE t204.c202_project_id = p_projectid
        	    AND t101p.c101_party_id = t101.c101_party_id
                AND t204.c101_party_id = t101.c101_party_id
                AND t101p.c901_party_type = 7006  --Globus Employee 
                AND t204.c901_role_id in(92042,92044) --is Project Leader Project Engineer or Purchasing agent, removed 120417 for PMT#51316
                AND t204.c901_status_id=92053 -- on Board 
                AND t204.c204_void_fl IS NULL )
            
 
       SELECT MAX (SYS_CONNECT_BY_PATH (email_id, ','))
          INTO otherinfo
          FROM
              (SELECT email_id, ROW_NUMBER () OVER (ORDER BY id) AS curr
                FROM other_info)
          CONNECT BY curr - 1 = PRIOR curr
          START WITH curr = 1;
     
      --added for PMT#51316 To Send NCMR Email notification to purchasing agent 
     BEGIN    
     
       SELECT t101.c101_email_id INTO v_purchasing_agent_email 
       	 FROM t301_vendor t301, t101_user t101
       	WHERE t301.c301_purchasing_agent_id = t101.c101_user_id
       	  AND t301.c301_vendor_id = p_vendor_id;
       	  RETURN SUBSTR (otherinfo, 2)||','|| v_purchasing_agent_email;
     EXCEPTION
     	WHEN NO_DATA_FOUND
     	THEN
			 RETURN SUBSTR (otherinfo, 2);
	END;
			
EXCEPTION
	WHEN NO_DATA_FOUND
THEN
	RETURN '';
         
END GET_PROJ_TEAM_EMAILS;
/