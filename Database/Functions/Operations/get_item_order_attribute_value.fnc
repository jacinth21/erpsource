CREATE OR REPLACE  FUNCTION get_item_order_attribute_value (
		p_orderid		IN	 t501_order.c501_order_id%TYPE,
		p_partnum	 	IN	 t502a_item_order_attribute.c205_part_number_id%TYPE,
		p_attribtype	IN	 t502a_item_order_attribute.c901_attribute_type%TYPE
	)
		RETURN VARCHAR2	
	 /*****************************************************************************
	  * Description : This function used to get item order attribut values for DO
	  * Author		: Elango
	  ****************************************************************************/
	IS
		v_item_attr_val t502a_item_order_attribute.c502a_attribute_value%TYPE;
	BEGIN
		BEGIN
			 SELECT c502a_attribute_value
			   INTO v_item_attr_val
			   FROM t502a_item_order_attribute
			  WHERE c901_attribute_type = p_attribtype 
				AND c501_order_id = p_orderid 
			 	AND c205_part_number_id = p_partnum 
			 	AND c502a_void_fl IS NULL;
	
			RETURN v_item_attr_val;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				RETURN NULL;
		END;
	END get_item_order_attribute_value;
	/