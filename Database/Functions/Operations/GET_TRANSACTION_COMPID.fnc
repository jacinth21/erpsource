-- @"C:\Database\Functions\Operations\get_transaction_compid.fnc";
CREATE OR REPLACE
    FUNCTION get_transaction_compid (
            p_refid		t907_shipping_info.c907_ref_id%TYPE,
            p_source 	t907_shipping_info.c901_source%TYPE,
            p_shipid 	t907_shipping_info.c907_shipping_id%TYPE DEFAULT NULL)
        RETURN VARCHAR2
    IS
        v_comp_id			t1900_company.c1900_company_id%TYPE;
        v_cntxt_comp_id	t1900_company.c1900_company_id%TYPE;
    BEGIN
        
	    SELECT NVL(get_compid_frm_cntx(), get_rule_value('DEFAULT_COMPANY','ONEPORTAL')) 
      		INTO v_cntxt_comp_id
      	FROM dual;
	    
	    	BEGIN
		    	SELECT c1900_company_id
		    		INTO v_comp_id
		    	FROM t907_shipping_info
				WHERE (c907_shipping_id = p_shipid
					OR (c907_ref_id = p_refid
					AND c901_source = p_source))
				AND c907_void_fl IS NULL
				GROUP BY c1900_company_id;
			EXCEPTION
                WHEN OTHERS THEN
                	v_comp_id := v_cntxt_comp_id;
            END;
	    
        RETURN v_comp_id;
    END get_transaction_compid;
    /