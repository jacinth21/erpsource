CREATE OR REPLACE FUNCTION GET_ORDER_INVOICE (
	p_inv_id   IN   t501_order.c503_invoice_id%TYPE
)
	RETURN VARCHAR2
IS
	v_order_id	   t501_order.c501_order_id%TYPE;
BEGIN
	BEGIN
		  SELECT c501_order_id 
		     INTO v_order_id
			FROM t501_order
			WHERE c503_invoice_id = p_inv_id
			AND c901_order_type   = 102080 -- OUS Distributor
			AND c501_ref_id      IS NOT NULL
			AND c501_delete_fl IS NULL
			AND c501_void_fl     IS NULL; 
		   
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			RETURN NULL;
	END;

	RETURN v_order_id;
	
END get_order_invoice;
/

