
--@"c:Database\Functions\Operations\get_division_from_FGLE.fnc";

/* PC-577- Fedex/UPS  account split-up for shipping transactions
 * Description    : THIS FUNCTION RETURNS DIVISION ID for FGLE Transactions
 Parameters       : p_txn_id
*/
CREATE OR REPLACE FUNCTION get_division_from_FGLE(
   p_txn_id      t907_shipping_info.c907_ref_id%TYPE
)
   RETURN VARCHAR2
IS
   v_division_id   VARCHAR2(1000);
BEGIN
	
  SELECT compid INTO v_division_id
    FROM v700_territory_mapping_detail   v700,
	     t907_shipping_info              t907,
	     t504a_loaner_transaction        t504a,
	     t412_inhouse_transactions       t412
   WHERE t907.c907_ref_id = t412.c412_inhouse_trans_id
     AND t504a.c504a_loaner_transaction_id = t412.c504a_loaner_transaction_id
     AND t504a.c704_account_id = v700.ac_id
     AND t504a.c703_sales_rep_id = v700.rep_id
     AND t907.c907_ref_id =p_txn_id --'FGLE-1017699';
     AND t907.c907_void_fl  IS NULL
     AND t504a.c504a_void_fl IS NULL
     AND t412.c412_void_fl IS NULL
     AND ROWNUM = 1;
      

   RETURN v_division_id;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      RETURN '';
END get_division_from_FGLE;
/


