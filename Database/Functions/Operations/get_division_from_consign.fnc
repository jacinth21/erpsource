
--@"c:Database\Functions\Operations\get_division_from_consign.fnc";

/* PC-577- Fedex/UPS  account split-up for shipping transactions
 * Description    : THIS FUNCTION RETURNS DIVISION ID for Item/Consignment Transactions
 Parameters       : p_txn_id
*/
CREATE OR REPLACE FUNCTION get_division_from_consign(
   p_txn_id      t504_consignment.c504_consignment_id%TYPE
)
   RETURN VARCHAR2
IS
   v_division_id   VARCHAR2(1000);
BEGIN
	
 SELECT compid INTO v_division_id
FROM
    (
        SELECT
            d_name,
            compid,
            COUNT(1) div_count
        FROM
            v700_territory_mapping_detail   v700,
            t504_consignment                t504,
            t907_shipping_info              t907
        WHERE
            t504.c504_consignment_id = t907.c907_ref_id
            AND t504.c701_distributor_id = v700.d_id
            AND t504.c504_consignment_id = p_txn_id --'GM-CN-3112'
            AND t504.c504_void_fl IS NULL
            AND t907.c907_void_fl  IS NULL
        GROUP BY
            d_name,
            compid
        ORDER BY
            d_name,
            div_count DESC
    ) a
WHERE
    ROWNUM = 1;

   RETURN v_division_id;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      RETURN '';
END get_division_from_consign;
/


