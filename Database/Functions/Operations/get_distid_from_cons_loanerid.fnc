--@"C:\Database\Functions\Operations\get_distid_from_cons_loanerid.fnc";

/*********************************************************************
* Description : To get dist id from consignment id
* Author      : Velu
* param		  : p_ref_id 
********************************************************************/

CREATE OR REPLACE FUNCTION get_distid_from_cons_loanerid (
	p_ref_id	IN	 t412_inhouse_transactions.c412_inhouse_trans_id%TYPE
)
	RETURN VARCHAR2
IS
	v_dist_id	t504_consignment.c701_distributor_id%TYPE;
	v_ref_id	t504_consignment.c504_consignment_id%TYPE;
BEGIN
	BEGIN
	    SELECT t504.c701_distributor_id INTO v_dist_id FROM t504a_consignment_loaner t504a,t504_consignment t504
              WHERE t504a.c504_consignment_id = t504.c504_consignment_id
              AND t504.c504_consignment_id = p_ref_id
              AND t504.c504_void_fl IS NULL
              AND t504a.c504a_void_fl IS NULL;
	EXCEPTION
		WHEN NO_DATA_FOUND
			THEN
		v_dist_id := null;
	END;	
	RETURN v_dist_id;
END get_distid_from_cons_loanerid;
/
