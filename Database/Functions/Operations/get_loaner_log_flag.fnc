CREATE OR REPLACE FUNCTION GET_LOANER_LOG_FLAG
(  p_ref_id  t902_log.c902_ref_id%TYPE
  ,p_type  t902_log.c902_type%TYPE
)
RETURN VARCHAR2
IS
/*Description     : This function returns the escalation flag 
*/
v_flag CHAR(1);
BEGIN
--
    SELECT   'Y'
    INTO   v_flag
    FROM    t526a_product_request_log
    WHERE  c526_product_request_detail_id = p_ref_id
    AND  c901_type = p_type    
    AND c526a_void_fl IS NULL
    AND  ROWNUM  = 1;
    --
    
 RETURN v_flag;
 --
EXCEPTION
WHEN NO_DATA_FOUND
THEN
 RETURN 'N';
END GET_LOANER_LOG_FLAG;
/


