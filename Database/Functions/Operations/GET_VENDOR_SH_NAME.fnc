CREATE OR REPLACE FUNCTION GET_VENDOR_SH_NAME
(p_vend_id T301_VENDOR.C301_VENDOR_ID%TYPE)
RETURN VARCHAR2
IS
/*  Description     : THIS FUNCTION RETURNS CODE NAME GIVEN THE CODE_ID
 Parameters 		: p_code_id
*/
v_vend_nm  T301_VENDOR.C301_VENDOR_NAME%TYPE;
BEGIN
     BEGIN
       SELECT C301_VENDOR_SH_NAME NM
	   INTO v_vend_nm
	   FROM  T301_VENDOR
       WHERE C301_VENDOR_ID = p_vend_id;
   EXCEPTION
      WHEN NO_DATA_FOUND
   THEN
      RETURN ' ';
      END;
     RETURN v_vend_nm;
END GET_VENDOR_SH_NAME;
/

