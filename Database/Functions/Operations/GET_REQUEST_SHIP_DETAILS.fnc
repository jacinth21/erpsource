/* Formatted on 2012/05/21 19:17 (Formatter Plus v4.8.0) */
--@"C:\Database\Functions\Operations\GET_REQUEST_SHIP_DETAILS.fnc";

CREATE OR REPLACE FUNCTION get_request_ship_details (
   p_request_id   t907_shipping_info.c907_ref_id%TYPE,
   p_action       VARCHAR2
)
   RETURN VARCHAR2
IS
   v_ship_to      t907_shipping_info.c901_ship_to%TYPE;
   v_ship_to_id   t907_shipping_info.c907_ship_to_id%TYPE;
   v_ship_email   CLOB;
BEGIN
   BEGIN
      SELECT t907.c901_ship_to, t907.c907_ship_to_id
        INTO v_ship_to, v_ship_to_id
        FROM t907_shipping_info t907
       WHERE t907.c907_ref_id = p_request_id
         AND t907.c901_source = 50184
         AND t907.c907_void_fl IS NULL;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         v_ship_to := NULL;
         v_ship_to_id := NULL;
      WHEN TOO_MANY_ROWS
      THEN
         SELECT t907.c901_ship_to, t907.c907_ship_to_id
           INTO v_ship_to, v_ship_to_id
           FROM t907_shipping_info t907
          WHERE t907.c907_ref_id = p_request_id
            AND t907.c901_source = 50184
            AND t907.c907_void_fl IS NULL
            AND ROWNUM = 1;
   END;

   IF p_action = 'MAIL'
   THEN
      v_ship_email := get_cs_ship_email (v_ship_to, v_ship_to_id);
   ELSIF p_action = 'ADD'
   THEN
      v_ship_email :=
            get_user_name (v_ship_to_id)
         || '<BR>'
         || get_rule_value ('GM-ADD', 'GM-ADD');
   ELSE
      v_ship_email := NULL;
   END IF;

   RETURN v_ship_email;
END get_request_ship_details;
/
