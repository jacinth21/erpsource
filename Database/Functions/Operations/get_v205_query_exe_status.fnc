--@"c:\database\Functions\Operations\get_qty_in_quarantine.fnc"

	/* This function is used to get the Execution status of the bucket in v205_qty_in_stock.sql file based on the code id
	 * if this function returns 0[ZERO] that bucket should execute otherwise execution will stop is it returns 1[ONE]*/

	CREATE OR REPLACE FUNCTION get_v205_query_exe_status( 
		p_query_block T901_CODE_LOOKUP.c901_code_id%TYPE 
	) 
	RETURN NUMBER 
	IS 
		v_cnt number; 
	BEGIN 
		SELECT count(1) 
		  INTO v_cnt 
	      FROM v_in_list 
	     WHERE token IS NOT NULL 
	       AND token = to_char(p_query_block); 
	RETURN v_cnt; 
	END get_v205_query_exe_status;
	/
