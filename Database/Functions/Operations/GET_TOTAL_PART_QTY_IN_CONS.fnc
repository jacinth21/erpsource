 /***************************************************************
      * Description : Function to get backorder quantity of part
			+ quantity of item in item_consignment
      * Author     : Rakhi Gandhi
      **********************************************************/

CREATE OR REPLACE FUNCTION GET_TOTAL_PART_QTY_IN_CONS
(
	p_consign_id    t504_consignment.c504_consignment_id%TYPE
      , p_part_num      t205_part_number.c205_part_number_id%TYPE
 )
 RETURN  NUMBER 
 IS
 /* Parameters: set consignment id , partid- return total quantity for the part
 */
        
	v_item_cons_qty      NUMBER; 
	v_total_qty          NUMBER; 
    BEGIN
    	BEGIN

	
	 SELECT  c505_item_qty
         INTO    v_item_cons_qty
         FROM  t505_item_consignment t505
         WHERE c504_consignment_id = p_consign_id AND c205_part_number_id = p_part_num;

	 SELECT    v_item_cons_qty   + GET_PART_BACKORDER_QTY ( p_consign_id , p_part_num )  
	 INTO      v_total_qty
	 from      dual ;
	 
	  EXCEPTION
          WHEN NO_DATA_FOUND
          THEN
          RETURN 0;
          END;
          RETURN v_total_qty;

    END GET_TOTAL_PART_QTY_IN_CONS;
   /
--