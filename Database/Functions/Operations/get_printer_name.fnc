--@C:\PC\ERP-SpineIT\Database\Functions\Operations\get_printer_name.fnc;

CREATE OR REPLACE FUNCTION get_printer_name (
	p_user_id   IN   t504p_loaner_user_print_mapping.c504p_user_id%TYPE
)
	RETURN VARCHAR2
IS
	v_printer_name	   t504p_loaner_user_print_mapping.c504p_printer_name%TYPE;
BEGIN
	BEGIN
		  SELECT c504p_printer_name
	  INTO v_printer_name
	  FROM t504p_loaner_user_print_mapping
	 WHERE c504p_user_id = p_user_id
	   AND c504p_void_fl IS NULL;
		   
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			RETURN NULL;
	END;

	RETURN v_printer_name;	
END get_printer_name;
/

