CREATE OR REPLACE FUNCTION GET_WO_UOM_TYPE_QTY
(
 p_wo_id t402_work_order.c402_work_order_id%TYPE
)
RETURN VARCHAR2
IS
--
/********************************************************************************************
 * Description  :This function returns the UOM Type and the Qty of a Work Order
 * Change Details:
   James Jun3,2006 Changes for UOM
 *********************************************************************************************/
--
v_str VARCHAR2(20);
--
BEGIN
--
 SELECT get_code_name_alt(t405.c901_uom_id) || '/' || t405.c405_uom_qty
 INTO v_str
 FROM t405_vendor_pricing_details t405, t402_work_order t402
 WHERE t402.c402_work_order_id = p_wo_id
 AND t402.c402_void_fl IS NULL
 AND t405.c405_pricing_id = t402.c405_pricing_id
 AND t405.c405_void_fl IS NULL;
--
  RETURN v_str;
  EXCEPTION
      WHEN NO_DATA_FOUND
  THEN
      RETURN '';
END GET_WO_UOM_TYPE_QTY;
/

