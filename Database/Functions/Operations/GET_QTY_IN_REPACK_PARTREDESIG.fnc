--@"c:\database\Functions\Operations\get_qty_in_repack_partredesig.fnc"
CREATE OR REPLACE FUNCTION get_qty_in_repack_partredesig (
	p_part_num	  t205_part_number.c205_part_number_id%TYPE,
	p_control_num t5060_control_number_inv.c5060_control_number%TYPE
)
	RETURN NUMBER
IS
/* Description	  : This returns the Qty in repack(Redeisgnation) for a particular part
					Its a sql copied from gm_pkg_part_redesignation.gm_fch_part_redesignation procedure. 
   Parameters	  : p_part_num
*/
	v_repack_qty    NUMBER;
	v_company_id    T1900_Company.C1900_company_id%TYPE;
	v_plant_id      T5040_plant_master.C5040_plant_id%TYPE;
--
BEGIN
	
SELECT get_compid_frm_cntx(), get_plantid_frm_cntx()
  INTO v_company_id, v_plant_id
  FROM DUAL;
	
SELECT DECODE(sign(nvl(qty,0) - nvl(reserved_qty, 0)),'-1','0',nvl(qty,0) - nvl(reserved_qty, 0)) 
  INTO v_repack_qty
  FROM
		(SELECT t5060.c5060_qty qty
		       ,t5060.c5060_control_number_inv_id
		   FROM  t5060_control_number_inv t5060 
		  WHERE t5060.c205_part_number_id = p_part_num
            AND t5060.c5060_control_number = p_control_num	 
		    AND c901_warehouse_type = '90815' --Repackage Qty Type
		    AND t5060.c5060_qty > 0
		    AND t5060.c1900_company_id = v_company_id
            AND t5060.c5040_plant_id = v_plant_id 
		 ) t5060
		,
		(SELECT sum(nvl(c5062_reserved_qty,0)) reserved_qty
		       ,t5062.c5060_control_number_inv_id
		   FROM t5060_control_number_inv t5060
			 , t5062_control_number_reserve t5062
		  WHERE t5060.c5060_control_number_inv_id = t5062.c5060_control_number_inv_id(+)
		    AND c5062_void_fl(+) is null 
		    AND t5060.c205_part_number_id =  p_part_num
		    AND t5060.c5060_control_number = p_control_num
		    
		    /*
		    Added the Status check condition as part of PMT-46898 author: gpalani
		    System should exclude closed Lot Status (103962) since the same Lot number would have 
		    came back again to re-pack so we should exclude Closed Lot Status while fetching the re-pack inventory qty.
		    Without this condition, whenever the re-pack qty becomes 1 system would not allow the user to move the last qty 
		    to FG since the query will give output as 0 qty in re-pack		    
		    */
		    
		    AND NVL(t5062.c901_status,-999) NOT IN (103962) 
		    AND t5060.c1900_company_id = t5062.c1900_company_id(+)
		    AND t5060.c5040_plant_id = t5062.c5040_plant_id(+)
		    AND t5060.c1900_company_id = v_company_id
            AND t5060.c5040_plant_id = v_plant_id
		  GROUP BY t5062.c5060_control_number_inv_id) t5062
 WHERE t5060.c5060_control_number_inv_id = t5062.c5060_control_number_inv_id(+);
	
	RETURN v_repack_qty;
EXCEPTION
	WHEN NO_DATA_FOUND
	THEN
		RETURN 0;
END get_qty_in_repack_partredesig;
/
