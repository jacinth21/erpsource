/* Formatted on 2009/03/02 15:56 (Formatter Plus v4.8.0) */
--@"C:\Database\Functions\Operations\GET_VENDOR_CURRENCY.fnc";


CREATE OR REPLACE FUNCTION get_vendor_currency (
	p_vend_id	t301_vendor.c301_vendor_id%TYPE
)
	RETURN NUMBER
IS
/*	Description 	: THIS FUNCTION RETURNS CODE NAME GIVEN THE CODE_ID
 Parameters 		: p_code_id
*/
	v_curr_id	   t301_vendor.c301_currency%TYPE;
BEGIN
	BEGIN
		SELECT c301_currency
		  INTO v_curr_id
		  FROM t301_vendor
		 WHERE c301_vendor_id = p_vend_id;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN 1;
	END;

	RETURN v_curr_id;
END;
/
