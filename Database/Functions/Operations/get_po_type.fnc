/* Formatted on 2012/04/23 10:58 (Formatter Plus v4.8.0) */
--@"C:\Database\Functions\Operations\get_po_type.fnc"

CREATE OR REPLACE FUNCTION get_po_type (
	p_dhrid   IN   t408_dhr.c408_dhr_id%TYPE
)
	RETURN NUMBER
IS
/*	Description 	: THIS FUNCTION RETURNS THE TYPE OF PO
 Parameters 		: p_dhrid
*/
	v_po_type	   t401_purchase_order.c401_type%TYPE;
BEGIN
	BEGIN
		SELECT t401.c401_type
		  INTO v_po_type
		  FROM t402_work_order t402, t401_purchase_order t401, t408_dhr t408
		 WHERE t408.c408_dhr_id = p_dhrid
		   AND t402.c402_work_order_id = t408.c402_work_order_id
		   AND t402.c401_purchase_ord_id = t401.c401_purchase_ord_id;
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			RETURN NULL;
	END;

	RETURN v_po_type;
END get_po_type;
/
