/* Formatted on 2013/03/26 14:54 (Formatter Plus v4.8.8) */
--@"C:\Database\Functions\Operations\GET_QTY_IN_INVENTORY_BY_PLANT.FNC";
CREATE OR REPLACE FUNCTION GET_QTY_IN_INVENTORY_BY_PLANT (
    P_trans_id      IN   t504_consignment.C504_CONSIGNMENT_ID%TYPE,   
    p_wh_type       IN   t901_code_lookup.c901_code_id%TYPE,
    p_partnum       IN   t205c_part_qty.c205_part_number_id%TYPE,
    p_plant_id      IN   T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE
)
    RETURN NUMBER
IS
/*  Description     : This Function returns the Availability Qty for given Inventory for the Plant SAN ANTONIO Globus Plant
*/
    v_avail_qty          NUMBER;
    v_alloc_qty          NUMBER;
BEGIN
    BEGIN
    
    SELECT	 NVL (t205c.c205_available_qty, 0)
		   -  NVL (v205c.c205_allocated_qty, 0)			 
	  	INTO v_avail_qty
	  FROM t205c_part_qty t205c
		 , v205c_part_allocated_qty v205c
	 WHERE t205c.c205_part_number_id = p_partnum
	   AND t205c.C901_Transaction_Type  = p_wh_type --90800 - Inventory Qty
	   AND t205c.c205_part_number_id = v205c.c205_part_number_id(+)
	   AND v205c.c901_transaction_type(+) = '60001' -- FG Inventory
	   AND t205c.C5040_PLANT_ID 	= 	p_plant_id
	   AND t205c.C5040_PLANT_ID     =   v205c.C5040_PLANT_ID(+);
    EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
            v_avail_qty := 0;
    END;
    
    BEGIN
	     SELECT NVL (SUM (alloc_qty), 0) INTO v_alloc_qty
		   FROM
		    (
		         SELECT NVL (SUM (c505_item_qty), 0) alloc_qty
		            --    INTO v_alloc_qty
		           FROM t505_item_consignment t505, t504_consignment t504
		          WHERE t505.c504_consignment_id = t504.c504_consignment_id
		            AND t505.c505_void_fl       IS NULL
		            AND t504.c504_void_fl       IS NULL
		            AND t505.c504_consignment_id = P_trans_id
		            AND t505.c205_part_number_id = p_partnum
		      UNION ALL
		         SELECT NVL (SUM (t502.c502_item_qty), 0) alloc_qty
		           FROM T502_ITEM_ORDER T502, T501_ORDER T501
		          WHERE T502.C501_ORDER_ID    = T501.C501_ORDER_ID
		            AND T502.C501_ORDER_ID    = P_trans_id
		            AND C205_PART_NUMBER_ID   = p_partnum
		            AND T502.C901_TYPE        = 50300
		            AND T501.C901_ORDER_TYPE IS NULL
		            AND T502.C502_VOID_FL    IS NULL
		            AND T501.C501_VOID_FL    IS NULL
		      UNION ALL
		         SELECT NVL (SUM (t413.c413_item_qty), 0) alloc_qty
		           FROM t412_inhouse_transactions t412, t413_inhouse_trans_items t413
		          WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id
		            AND t413.c205_part_number_id   = p_partnum
		            AND t413.c412_inhouse_trans_id = P_trans_id
		            AND t412.c412_void_fl         IS NULL
		            AND t413.c413_void_fl         IS NULL
		    ) ;	    
	EXCEPTION
    WHEN NO_DATA_FOUND
    THEN
    	v_alloc_qty := 0;
	END;
    
	v_avail_qty := v_avail_qty - v_alloc_qty;
    
    RETURN v_avail_qty;
END GET_QTY_IN_INVENTORY_BY_PLANT;
/