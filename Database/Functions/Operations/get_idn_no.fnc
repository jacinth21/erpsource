--@"c:\database\functions\operations\get_idn_no.fnc";

CREATE OR REPLACE FUNCTION get_idn_no(
	p_acc_id   t704_account.c704_account_id%TYPE
)
	RETURN NUMBER
is
	v_idn_no	 T704d_Account_Affln.C101_IDN%TYPE;   
BEGIN
	BEGIN
		
		SELECT C101_IDN
	     INTO v_idn_no 
		  FROM T704d_Account_Affln
		 WHERE c704_account_id = p_acc_id AND c704d_void_fl IS NULL;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN NULL;
	END;

	RETURN v_idn_no;
	
end get_idn_no;
/


