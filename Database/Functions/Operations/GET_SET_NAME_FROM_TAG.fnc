--@"C:\Database\Functions\Operations\GET_SET_NAME_FROM_TAG.fnc";
CREATE OR REPLACE FUNCTION GET_SET_NAME_FROM_TAG
(
 	   p_tag_id t5010_tag.c5010_tag_id%TYPE
)
RETURN VARCHAR2
IS
/*  Description     : THIS FUNCTION RETURNS Set ID GIVEN THE Tag id
 Parameters 		: Tag id
*/
v_set_name  VARCHAR2(100);

BEGIN
     BEGIN
		select get_set_name(c207_set_id) INTO v_set_name from t5010_tag where c5010_tag_id = p_tag_id  and c5010_void_fl IS NULL;
   EXCEPTION
      WHEN NO_DATA_FOUND
   THEN
      v_set_name := '';
      END;
     RETURN v_set_name;
END GET_SET_NAME_FROM_TAG;  
/

