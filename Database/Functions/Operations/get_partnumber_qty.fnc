/* Formatted on 2008/03/04 14:54 (Formatter Plus v4.8.8) */
CREATE OR REPLACE FUNCTION get_partnumber_qty (
    p_partnum     IN   t205c_part_qty.c205_part_number_id%TYPE
  , p_type        IN   t205c_part_qty.C901_TRANSACTION_TYPE%TYPE
)
    RETURN NUMBER
IS
/*  Description     : THIS FUNCTION RETURNS PArtnuber Qty in the T205C table
*/
    v_cnt          NUMBER;
    v_plant_id     T5040_PLANT_MASTER.C5040_PLANT_ID%TYPE;
BEGIN
	SELECT  get_plantid_frm_cntx() INTO v_plant_id FROM DUAL;
	
    BEGIN
        SELECT NVL (c205_available_qty, 0)
          INTO v_cnt
          FROM t205c_part_qty
         WHERE c205_part_number_id = p_partnum
           AND C901_TRANSACTION_TYPE = p_type
           AND C5040_PLANT_ID = v_plant_id;
    EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
            RETURN 0;
    END;

    RETURN v_cnt;
END get_partnumber_qty;
/