
--@"c:\database\functions\operations\get_dornor_number.fnc";

/* Description    : THIS FUNCTION RETURNS Donor Number by Donor ID
 Parameters       : p_donor_id
*/
create or replace
FUNCTION             get_donor_number    (
   p_donor_id   t2540_donor_master.c2540_donor_id%TYPE
)
   RETURN VARCHAR2
IS
   v_donor_number   t2540_donor_master.c2540_donor_number%TYPE;
BEGIN
   SELECT c2540_donor_number
     INTO v_donor_number
     FROM t2540_donor_master
    WHERE c2540_donor_id = p_donor_id
    AND c2540_void_fl IS NULL;

   RETURN v_donor_number;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      RETURN '';
END get_donor_number;
/